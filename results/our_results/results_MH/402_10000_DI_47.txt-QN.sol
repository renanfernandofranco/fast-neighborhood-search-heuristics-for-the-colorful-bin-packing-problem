Capicity Bin: 8256
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5765 Color: 317
Size: 2077 Color: 225
Size: 414 Color: 94

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6002 Color: 323
Size: 1970 Color: 219
Size: 284 Color: 64

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6028 Color: 325
Size: 1860 Color: 214
Size: 368 Color: 87

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6052 Color: 326
Size: 2056 Color: 222
Size: 148 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6081 Color: 327
Size: 1813 Color: 211
Size: 362 Color: 85

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 328
Size: 2068 Color: 223
Size: 104 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6085 Color: 329
Size: 1439 Color: 193
Size: 732 Color: 138

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6097 Color: 330
Size: 1801 Color: 210
Size: 358 Color: 83

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6216 Color: 334
Size: 1864 Color: 215
Size: 176 Color: 30

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6296 Color: 335
Size: 1656 Color: 204
Size: 304 Color: 73

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6324 Color: 337
Size: 1534 Color: 197
Size: 398 Color: 90

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 338
Size: 1882 Color: 216
Size: 40 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6456 Color: 343
Size: 1602 Color: 202
Size: 198 Color: 42

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 344
Size: 1256 Color: 180
Size: 540 Color: 113

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6531 Color: 346
Size: 1425 Color: 191
Size: 300 Color: 72

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6568 Color: 349
Size: 1544 Color: 198
Size: 144 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 350
Size: 1500 Color: 195
Size: 184 Color: 35

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 353
Size: 1404 Color: 190
Size: 184 Color: 36

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6682 Color: 354
Size: 1402 Color: 189
Size: 172 Color: 27

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 360
Size: 1224 Color: 178
Size: 252 Color: 55

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6861 Color: 361
Size: 1269 Color: 181
Size: 126 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6862 Color: 362
Size: 1074 Color: 171
Size: 320 Color: 77

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6863 Color: 363
Size: 1161 Color: 176
Size: 232 Color: 51

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 364
Size: 808 Color: 143
Size: 576 Color: 117

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7058 Color: 373
Size: 746 Color: 139
Size: 452 Color: 102

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7079 Color: 375
Size: 981 Color: 162
Size: 196 Color: 41

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 378
Size: 728 Color: 137
Size: 420 Color: 98

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7130 Color: 379
Size: 678 Color: 126
Size: 448 Color: 101

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 380
Size: 820 Color: 144
Size: 296 Color: 71

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7141 Color: 381
Size: 931 Color: 157
Size: 184 Color: 32

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7192 Color: 384
Size: 904 Color: 153
Size: 160 Color: 23

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 385
Size: 774 Color: 141
Size: 288 Color: 67

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7205 Color: 386
Size: 867 Color: 148
Size: 184 Color: 34

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7255 Color: 389
Size: 835 Color: 147
Size: 166 Color: 25

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 393
Size: 510 Color: 110
Size: 470 Color: 103

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7298 Color: 395
Size: 802 Color: 142
Size: 156 Color: 20

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7356 Color: 398
Size: 896 Color: 152
Size: 4 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7362 Color: 399
Size: 640 Color: 122
Size: 254 Color: 57

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7380 Color: 400
Size: 624 Color: 120
Size: 252 Color: 56

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7400 Color: 401
Size: 592 Color: 119
Size: 264 Color: 60

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7402 Color: 402
Size: 680 Color: 128
Size: 174 Color: 28

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6024 Color: 324
Size: 2051 Color: 221
Size: 180 Color: 31

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6178 Color: 333
Size: 2077 Color: 226

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6318 Color: 336
Size: 1601 Color: 201
Size: 336 Color: 80

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6729 Color: 356
Size: 942 Color: 159
Size: 584 Color: 118

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6733 Color: 357
Size: 1002 Color: 166
Size: 520 Color: 112

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6975 Color: 371
Size: 1280 Color: 185

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 7067 Color: 374
Size: 908 Color: 154
Size: 280 Color: 63

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 5542 Color: 311
Size: 2584 Color: 240
Size: 128 Color: 14

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 5712 Color: 313
Size: 2542 Color: 239

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5716 Color: 314
Size: 2114 Color: 228
Size: 424 Color: 99

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6106 Color: 331
Size: 2148 Color: 230

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6760 Color: 359
Size: 1494 Color: 194

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 7206 Color: 387
Size: 1048 Color: 168

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 6349 Color: 340
Size: 1904 Color: 218

Bin 56: 4 of cap free
Amount of items: 2
Items: 
Size: 6408 Color: 342
Size: 1844 Color: 213

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 7016 Color: 372
Size: 1236 Color: 179

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 7259 Color: 391
Size: 993 Color: 165

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 7268 Color: 392
Size: 984 Color: 163

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 7288 Color: 394
Size: 964 Color: 160

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 7320 Color: 396
Size: 932 Color: 158

Bin 62: 5 of cap free
Amount of items: 2
Items: 
Size: 6547 Color: 348
Size: 1704 Color: 206

Bin 63: 5 of cap free
Amount of items: 2
Items: 
Size: 7100 Color: 377
Size: 1151 Color: 174

Bin 64: 5 of cap free
Amount of items: 2
Items: 
Size: 7151 Color: 382
Size: 1100 Color: 173

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 7330 Color: 397
Size: 921 Color: 156

Bin 66: 6 of cap free
Amount of items: 3
Items: 
Size: 5184 Color: 301
Size: 2894 Color: 249
Size: 172 Color: 26

Bin 67: 6 of cap free
Amount of items: 3
Items: 
Size: 5496 Color: 309
Size: 2614 Color: 241
Size: 140 Color: 15

Bin 68: 6 of cap free
Amount of items: 4
Items: 
Size: 5874 Color: 322
Size: 2312 Color: 233
Size: 32 Color: 2
Size: 32 Color: 1

Bin 69: 7 of cap free
Amount of items: 7
Items: 
Size: 4132 Color: 274
Size: 888 Color: 151
Size: 878 Color: 150
Size: 877 Color: 149
Size: 834 Color: 146
Size: 320 Color: 78
Size: 320 Color: 76

Bin 70: 7 of cap free
Amount of items: 2
Items: 
Size: 4856 Color: 293
Size: 3393 Color: 258

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 6466 Color: 345
Size: 1783 Color: 209

Bin 72: 7 of cap free
Amount of items: 2
Items: 
Size: 6697 Color: 355
Size: 1552 Color: 199

Bin 73: 7 of cap free
Amount of items: 2
Items: 
Size: 7258 Color: 390
Size: 991 Color: 164

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 7172 Color: 383
Size: 1076 Color: 172

Bin 75: 9 of cap free
Amount of items: 2
Items: 
Size: 5755 Color: 316
Size: 2492 Color: 237

Bin 76: 9 of cap free
Amount of items: 2
Items: 
Size: 6535 Color: 347
Size: 1712 Color: 207

Bin 77: 9 of cap free
Amount of items: 2
Items: 
Size: 7217 Color: 388
Size: 1030 Color: 167

Bin 78: 10 of cap free
Amount of items: 2
Items: 
Size: 6574 Color: 351
Size: 1672 Color: 205

Bin 79: 10 of cap free
Amount of items: 2
Items: 
Size: 6734 Color: 358
Size: 1512 Color: 196

Bin 80: 11 of cap free
Amount of items: 2
Items: 
Size: 5405 Color: 307
Size: 2840 Color: 248

Bin 81: 11 of cap free
Amount of items: 2
Items: 
Size: 6877 Color: 365
Size: 1368 Color: 188

Bin 82: 11 of cap free
Amount of items: 2
Items: 
Size: 6972 Color: 370
Size: 1273 Color: 184

Bin 83: 12 of cap free
Amount of items: 7
Items: 
Size: 4133 Color: 275
Size: 1069 Color: 169
Size: 964 Color: 161
Size: 912 Color: 155
Size: 546 Color: 114
Size: 316 Color: 75
Size: 304 Color: 74

Bin 84: 12 of cap free
Amount of items: 3
Items: 
Size: 5160 Color: 299
Size: 2908 Color: 250
Size: 176 Color: 29

Bin 85: 12 of cap free
Amount of items: 2
Items: 
Size: 5726 Color: 315
Size: 2518 Color: 238

Bin 86: 12 of cap free
Amount of items: 2
Items: 
Size: 6632 Color: 352
Size: 1612 Color: 203

Bin 87: 12 of cap free
Amount of items: 2
Items: 
Size: 6920 Color: 366
Size: 1324 Color: 187

Bin 88: 13 of cap free
Amount of items: 4
Items: 
Size: 5769 Color: 318
Size: 2262 Color: 231
Size: 112 Color: 11
Size: 100 Color: 9

Bin 89: 14 of cap free
Amount of items: 3
Items: 
Size: 5246 Color: 304
Size: 2836 Color: 247
Size: 160 Color: 22

Bin 90: 14 of cap free
Amount of items: 2
Items: 
Size: 6971 Color: 369
Size: 1271 Color: 183

Bin 91: 14 of cap free
Amount of items: 2
Items: 
Size: 7080 Color: 376
Size: 1162 Color: 177

Bin 92: 15 of cap free
Amount of items: 4
Items: 
Size: 5800 Color: 321
Size: 2377 Color: 235
Size: 32 Color: 4
Size: 32 Color: 3

Bin 93: 15 of cap free
Amount of items: 2
Items: 
Size: 6117 Color: 332
Size: 2124 Color: 229

Bin 94: 15 of cap free
Amount of items: 2
Items: 
Size: 6940 Color: 367
Size: 1301 Color: 186

Bin 95: 16 of cap free
Amount of items: 3
Items: 
Size: 4995 Color: 296
Size: 2085 Color: 227
Size: 1160 Color: 175

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 6970 Color: 368
Size: 1270 Color: 182

Bin 97: 18 of cap free
Amount of items: 2
Items: 
Size: 6400 Color: 341
Size: 1838 Color: 212

Bin 98: 19 of cap free
Amount of items: 9
Items: 
Size: 4129 Color: 272
Size: 684 Color: 133
Size: 684 Color: 132
Size: 684 Color: 131
Size: 560 Color: 116
Size: 396 Color: 89
Size: 372 Color: 88
Size: 368 Color: 86
Size: 360 Color: 84

Bin 99: 20 of cap free
Amount of items: 3
Items: 
Size: 5122 Color: 298
Size: 2930 Color: 251
Size: 184 Color: 33

Bin 100: 20 of cap free
Amount of items: 3
Items: 
Size: 5684 Color: 312
Size: 2436 Color: 236
Size: 116 Color: 12

Bin 101: 21 of cap free
Amount of items: 2
Items: 
Size: 5011 Color: 297
Size: 3224 Color: 255

Bin 102: 23 of cap free
Amount of items: 2
Items: 
Size: 5528 Color: 310
Size: 2705 Color: 243

Bin 103: 24 of cap free
Amount of items: 4
Items: 
Size: 4137 Color: 277
Size: 2073 Color: 224
Size: 1734 Color: 208
Size: 288 Color: 69

Bin 104: 25 of cap free
Amount of items: 3
Items: 
Size: 4682 Color: 290
Size: 3357 Color: 257
Size: 192 Color: 38

Bin 105: 25 of cap free
Amount of items: 3
Items: 
Size: 5174 Color: 300
Size: 1986 Color: 220
Size: 1071 Color: 170

Bin 106: 28 of cap free
Amount of items: 3
Items: 
Size: 5268 Color: 305
Size: 2800 Color: 246
Size: 160 Color: 21

Bin 107: 28 of cap free
Amount of items: 2
Items: 
Size: 6335 Color: 339
Size: 1893 Color: 217

Bin 108: 33 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 306
Size: 2731 Color: 244
Size: 152 Color: 19

Bin 109: 33 of cap free
Amount of items: 3
Items: 
Size: 5417 Color: 308
Size: 2662 Color: 242
Size: 144 Color: 17

Bin 110: 35 of cap free
Amount of items: 4
Items: 
Size: 5781 Color: 319
Size: 2280 Color: 232
Size: 80 Color: 8
Size: 80 Color: 7

Bin 111: 36 of cap free
Amount of items: 2
Items: 
Size: 4772 Color: 292
Size: 3448 Color: 269

Bin 112: 42 of cap free
Amount of items: 5
Items: 
Size: 4136 Color: 276
Size: 1591 Color: 200
Size: 1435 Color: 192
Size: 756 Color: 140
Size: 296 Color: 70

Bin 113: 47 of cap free
Amount of items: 3
Items: 
Size: 4392 Color: 287
Size: 3609 Color: 270
Size: 208 Color: 43

Bin 114: 60 of cap free
Amount of items: 3
Items: 
Size: 5797 Color: 320
Size: 2367 Color: 234
Size: 32 Color: 5

Bin 115: 63 of cap free
Amount of items: 3
Items: 
Size: 5238 Color: 303
Size: 2791 Color: 245
Size: 164 Color: 24

Bin 116: 68 of cap free
Amount of items: 2
Items: 
Size: 5206 Color: 302
Size: 2982 Color: 252

Bin 117: 70 of cap free
Amount of items: 2
Items: 
Size: 4742 Color: 291
Size: 3444 Color: 268

Bin 118: 77 of cap free
Amount of items: 3
Items: 
Size: 4860 Color: 294
Size: 3127 Color: 253
Size: 192 Color: 37

Bin 119: 92 of cap free
Amount of items: 3
Items: 
Size: 4297 Color: 286
Size: 3659 Color: 271
Size: 208 Color: 44

Bin 120: 94 of cap free
Amount of items: 15
Items: 
Size: 680 Color: 130
Size: 680 Color: 129
Size: 680 Color: 127
Size: 672 Color: 125
Size: 672 Color: 124
Size: 670 Color: 123
Size: 630 Color: 121
Size: 560 Color: 115
Size: 448 Color: 100
Size: 416 Color: 97
Size: 416 Color: 96
Size: 416 Color: 95
Size: 414 Color: 93
Size: 408 Color: 92
Size: 400 Color: 91

Bin 121: 117 of cap free
Amount of items: 3
Items: 
Size: 4505 Color: 289
Size: 3442 Color: 267
Size: 192 Color: 39

Bin 122: 124 of cap free
Amount of items: 2
Items: 
Size: 4979 Color: 295
Size: 3153 Color: 254

Bin 123: 140 of cap free
Amount of items: 4
Items: 
Size: 4146 Color: 279
Size: 3404 Color: 259
Size: 286 Color: 65
Size: 280 Color: 62

Bin 124: 146 of cap free
Amount of items: 3
Items: 
Size: 4473 Color: 288
Size: 3441 Color: 266
Size: 196 Color: 40

Bin 125: 160 of cap free
Amount of items: 4
Items: 
Size: 4148 Color: 280
Size: 3412 Color: 260
Size: 278 Color: 61
Size: 258 Color: 59

Bin 126: 166 of cap free
Amount of items: 4
Items: 
Size: 4229 Color: 285
Size: 3437 Color: 265
Size: 212 Color: 46
Size: 212 Color: 45

Bin 127: 184 of cap free
Amount of items: 4
Items: 
Size: 4154 Color: 281
Size: 3422 Color: 261
Size: 256 Color: 58
Size: 240 Color: 54

Bin 128: 192 of cap free
Amount of items: 8
Items: 
Size: 4130 Color: 273
Size: 828 Color: 145
Size: 714 Color: 136
Size: 686 Color: 135
Size: 686 Color: 134
Size: 356 Color: 82
Size: 344 Color: 81
Size: 320 Color: 79

Bin 129: 194 of cap free
Amount of items: 4
Items: 
Size: 4164 Color: 282
Size: 3426 Color: 262
Size: 240 Color: 53
Size: 232 Color: 52

Bin 130: 196 of cap free
Amount of items: 4
Items: 
Size: 4180 Color: 283
Size: 3428 Color: 263
Size: 228 Color: 50
Size: 224 Color: 49

Bin 131: 207 of cap free
Amount of items: 4
Items: 
Size: 4185 Color: 284
Size: 3434 Color: 264
Size: 216 Color: 48
Size: 214 Color: 47

Bin 132: 216 of cap free
Amount of items: 4
Items: 
Size: 4138 Color: 278
Size: 3328 Color: 256
Size: 288 Color: 68
Size: 286 Color: 66

Bin 133: 4814 of cap free
Amount of items: 7
Items: 
Size: 512 Color: 111
Size: 508 Color: 109
Size: 500 Color: 108
Size: 496 Color: 107
Size: 480 Color: 106
Size: 474 Color: 105
Size: 472 Color: 104

Total size: 1089792
Total free space: 8256

