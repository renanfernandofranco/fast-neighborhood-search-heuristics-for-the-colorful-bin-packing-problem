Capicity Bin: 1000001
Lower Bound: 42

Bins used: 42
Amount of Colors: 2

Bin 1: 63 of cap free
Amount of items: 3
Items: 
Size: 536697 Color: 0
Size: 339305 Color: 1
Size: 123936 Color: 0

Bin 2: 114 of cap free
Amount of items: 3
Items: 
Size: 459773 Color: 0
Size: 281589 Color: 1
Size: 258525 Color: 1

Bin 3: 373 of cap free
Amount of items: 2
Items: 
Size: 506745 Color: 0
Size: 492883 Color: 1

Bin 4: 405 of cap free
Amount of items: 3
Items: 
Size: 666854 Color: 1
Size: 204943 Color: 1
Size: 127799 Color: 0

Bin 5: 451 of cap free
Amount of items: 2
Items: 
Size: 724983 Color: 1
Size: 274567 Color: 0

Bin 6: 613 of cap free
Amount of items: 3
Items: 
Size: 503847 Color: 1
Size: 303000 Color: 1
Size: 192541 Color: 0

Bin 7: 826 of cap free
Amount of items: 3
Items: 
Size: 665841 Color: 1
Size: 172723 Color: 0
Size: 160611 Color: 1

Bin 8: 911 of cap free
Amount of items: 4
Items: 
Size: 301247 Color: 1
Size: 286904 Color: 0
Size: 286755 Color: 1
Size: 124184 Color: 0

Bin 9: 1099 of cap free
Amount of items: 3
Items: 
Size: 653197 Color: 0
Size: 189179 Color: 1
Size: 156526 Color: 0

Bin 10: 1111 of cap free
Amount of items: 3
Items: 
Size: 658341 Color: 1
Size: 189315 Color: 0
Size: 151234 Color: 1

Bin 11: 1296 of cap free
Amount of items: 3
Items: 
Size: 559302 Color: 0
Size: 273919 Color: 0
Size: 165484 Color: 1

Bin 12: 1690 of cap free
Amount of items: 2
Items: 
Size: 798891 Color: 1
Size: 199420 Color: 0

Bin 13: 1706 of cap free
Amount of items: 2
Items: 
Size: 513658 Color: 1
Size: 484637 Color: 0

Bin 14: 1813 of cap free
Amount of items: 3
Items: 
Size: 617772 Color: 0
Size: 235163 Color: 1
Size: 145253 Color: 1

Bin 15: 1819 of cap free
Amount of items: 3
Items: 
Size: 361531 Color: 1
Size: 353632 Color: 0
Size: 283019 Color: 1

Bin 16: 1887 of cap free
Amount of items: 2
Items: 
Size: 539532 Color: 1
Size: 458582 Color: 0

Bin 17: 2828 of cap free
Amount of items: 3
Items: 
Size: 705520 Color: 0
Size: 149626 Color: 1
Size: 142027 Color: 1

Bin 18: 3219 of cap free
Amount of items: 2
Items: 
Size: 742621 Color: 0
Size: 254161 Color: 1

Bin 19: 3774 of cap free
Amount of items: 2
Items: 
Size: 591311 Color: 0
Size: 404916 Color: 1

Bin 20: 3784 of cap free
Amount of items: 2
Items: 
Size: 612340 Color: 1
Size: 383877 Color: 0

Bin 21: 4160 of cap free
Amount of items: 2
Items: 
Size: 605727 Color: 0
Size: 390114 Color: 1

Bin 22: 4479 of cap free
Amount of items: 2
Items: 
Size: 785992 Color: 1
Size: 209530 Color: 0

Bin 23: 4915 of cap free
Amount of items: 3
Items: 
Size: 562646 Color: 0
Size: 258097 Color: 0
Size: 174343 Color: 1

Bin 24: 5848 of cap free
Amount of items: 2
Items: 
Size: 745918 Color: 0
Size: 248235 Color: 1

Bin 25: 5913 of cap free
Amount of items: 3
Items: 
Size: 550035 Color: 1
Size: 329645 Color: 1
Size: 114408 Color: 0

Bin 26: 6554 of cap free
Amount of items: 2
Items: 
Size: 779496 Color: 0
Size: 213951 Color: 1

Bin 27: 7710 of cap free
Amount of items: 2
Items: 
Size: 586598 Color: 1
Size: 405693 Color: 0

Bin 28: 10946 of cap free
Amount of items: 2
Items: 
Size: 774502 Color: 1
Size: 214553 Color: 0

Bin 29: 12586 of cap free
Amount of items: 2
Items: 
Size: 509231 Color: 1
Size: 478184 Color: 0

Bin 30: 17491 of cap free
Amount of items: 2
Items: 
Size: 770822 Color: 0
Size: 211688 Color: 1

Bin 31: 20083 of cap free
Amount of items: 3
Items: 
Size: 552004 Color: 0
Size: 244339 Color: 1
Size: 183575 Color: 1

Bin 32: 20952 of cap free
Amount of items: 2
Items: 
Size: 505954 Color: 1
Size: 473095 Color: 0

Bin 33: 23761 of cap free
Amount of items: 2
Items: 
Size: 673990 Color: 0
Size: 302250 Color: 1

Bin 34: 31232 of cap free
Amount of items: 3
Items: 
Size: 694577 Color: 1
Size: 140333 Color: 1
Size: 133859 Color: 0

Bin 35: 34840 of cap free
Amount of items: 2
Items: 
Size: 598807 Color: 1
Size: 366354 Color: 0

Bin 36: 37131 of cap free
Amount of items: 2
Items: 
Size: 501092 Color: 1
Size: 461778 Color: 0

Bin 37: 48954 of cap free
Amount of items: 2
Items: 
Size: 683223 Color: 1
Size: 267824 Color: 0

Bin 38: 78140 of cap free
Amount of items: 2
Items: 
Size: 495276 Color: 1
Size: 426585 Color: 0

Bin 39: 87153 of cap free
Amount of items: 2
Items: 
Size: 492779 Color: 1
Size: 420069 Color: 0

Bin 40: 105142 of cap free
Amount of items: 2
Items: 
Size: 642099 Color: 1
Size: 252760 Color: 0

Bin 41: 111905 of cap free
Amount of items: 3
Items: 
Size: 490506 Color: 1
Size: 120918 Color: 0
Size: 276672 Color: 1

Bin 42: 117984 of cap free
Amount of items: 2
Items: 
Size: 510725 Color: 0
Size: 371292 Color: 1

Total size: 41172381
Total free space: 827661

