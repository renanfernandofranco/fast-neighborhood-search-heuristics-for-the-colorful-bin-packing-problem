Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 214
Size: 294 Color: 120
Size: 253 Color: 28

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 180
Size: 351 Color: 160
Size: 253 Color: 29

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 207
Size: 282 Color: 103
Size: 273 Color: 89

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 280 Color: 99
Size: 268 Color: 78
Size: 452 Color: 213

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 167
Size: 350 Color: 158
Size: 285 Color: 108

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 183
Size: 346 Color: 155
Size: 254 Color: 33

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 236
Size: 268 Color: 77
Size: 250 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 193
Size: 322 Color: 143
Size: 263 Color: 62

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 174
Size: 335 Color: 148
Size: 287 Color: 113

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 212
Size: 294 Color: 119
Size: 255 Color: 42

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 202
Size: 308 Color: 133
Size: 259 Color: 54

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 241
Size: 258 Color: 49
Size: 254 Color: 32

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 248
Size: 251 Color: 13
Size: 250 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 226
Size: 278 Color: 98
Size: 255 Color: 38

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 233
Size: 270 Color: 83
Size: 251 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 231
Size: 269 Color: 79
Size: 255 Color: 41

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 194
Size: 296 Color: 124
Size: 284 Color: 106

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 200
Size: 319 Color: 140
Size: 254 Color: 36

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 175
Size: 335 Color: 149
Size: 286 Color: 111

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 181
Size: 345 Color: 154
Size: 258 Color: 52

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 220
Size: 270 Color: 84
Size: 269 Color: 81

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 242
Size: 254 Color: 34
Size: 250 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 198
Size: 317 Color: 138
Size: 258 Color: 51

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 170
Size: 343 Color: 152
Size: 283 Color: 104

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 240
Size: 262 Color: 59
Size: 252 Color: 25

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 221
Size: 287 Color: 112
Size: 252 Color: 21

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 208
Size: 281 Color: 100
Size: 273 Color: 88

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 185
Size: 305 Color: 131
Size: 294 Color: 117

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 235
Size: 266 Color: 73
Size: 253 Color: 30

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 190
Size: 318 Color: 139
Size: 272 Color: 87

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 225
Size: 275 Color: 93
Size: 258 Color: 48

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 199
Size: 317 Color: 136
Size: 256 Color: 43

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 224
Size: 267 Color: 74
Size: 266 Color: 72

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 223
Size: 269 Color: 80
Size: 265 Color: 71

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 234
Size: 265 Color: 70
Size: 255 Color: 39

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 196
Size: 315 Color: 135
Size: 264 Color: 67

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 204
Size: 308 Color: 134
Size: 252 Color: 23

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 173
Size: 349 Color: 157
Size: 276 Color: 95

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 203
Size: 298 Color: 125
Size: 263 Color: 66

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 245
Size: 252 Color: 24
Size: 250 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 211
Size: 300 Color: 126
Size: 252 Color: 15

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 229
Size: 263 Color: 63
Size: 262 Color: 60

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 201
Size: 295 Color: 121
Size: 276 Color: 94

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 189
Size: 320 Color: 141
Size: 271 Color: 86

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 195
Size: 322 Color: 144
Size: 257 Color: 46

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 219
Size: 290 Color: 116
Size: 250 Color: 9

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 161
Size: 344 Color: 153
Size: 303 Color: 130

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 215
Size: 285 Color: 107
Size: 260 Color: 55

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 184
Size: 322 Color: 145
Size: 277 Color: 96

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 218
Size: 289 Color: 115
Size: 251 Color: 12

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 237
Size: 263 Color: 64
Size: 254 Color: 37

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 169
Size: 368 Color: 168
Size: 261 Color: 58

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 209
Size: 301 Color: 127
Size: 252 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 178
Size: 359 Color: 164
Size: 254 Color: 35

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 163
Size: 337 Color: 150
Size: 305 Color: 132

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 179
Size: 351 Color: 159
Size: 259 Color: 53

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 228
Size: 273 Color: 90
Size: 253 Color: 27

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 239
Size: 258 Color: 50
Size: 256 Color: 45

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 182
Size: 321 Color: 142
Size: 281 Color: 102

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 243
Size: 252 Color: 22
Size: 250 Color: 10

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 205
Size: 296 Color: 122
Size: 262 Color: 61

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 177
Size: 360 Color: 165
Size: 254 Color: 31

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 171
Size: 340 Color: 151
Size: 286 Color: 110

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 197
Size: 296 Color: 123
Size: 281 Color: 101

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 230
Size: 275 Color: 92
Size: 250 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 206
Size: 294 Color: 118
Size: 261 Color: 56

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 172
Size: 355 Color: 162
Size: 271 Color: 85

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 244
Size: 252 Color: 17
Size: 250 Color: 7

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 191
Size: 322 Color: 146
Size: 265 Color: 69

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 247
Size: 252 Color: 20
Size: 250 Color: 8

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 176
Size: 362 Color: 166
Size: 255 Color: 40

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 186
Size: 346 Color: 156
Size: 251 Color: 11

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 187
Size: 325 Color: 147
Size: 267 Color: 76

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 222
Size: 286 Color: 109
Size: 253 Color: 26

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 246
Size: 252 Color: 18
Size: 250 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 188
Size: 317 Color: 137
Size: 274 Color: 91

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 192
Size: 303 Color: 129
Size: 283 Color: 105

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 232
Size: 267 Color: 75
Size: 257 Color: 47

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 238
Size: 261 Color: 57
Size: 256 Color: 44

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 227
Size: 269 Color: 82
Size: 263 Color: 65

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 216
Size: 278 Color: 97
Size: 264 Color: 68

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 210
Size: 302 Color: 128
Size: 250 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 217
Size: 289 Color: 114
Size: 252 Color: 19

Total size: 83000
Total free space: 0

