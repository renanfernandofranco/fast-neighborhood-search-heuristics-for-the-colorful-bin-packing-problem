Capicity Bin: 2400
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1188 Color: 3
Size: 332 Color: 3
Size: 296 Color: 3
Size: 192 Color: 2
Size: 144 Color: 0
Size: 136 Color: 1
Size: 112 Color: 1

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1914 Color: 3
Size: 406 Color: 3
Size: 64 Color: 0
Size: 16 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1395 Color: 1
Size: 839 Color: 2
Size: 166 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 0
Size: 719 Color: 1
Size: 142 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 0
Size: 825 Color: 3
Size: 164 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 0
Size: 705 Color: 0
Size: 140 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 4
Size: 434 Color: 3
Size: 84 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 3
Size: 491 Color: 1
Size: 96 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 2
Size: 206 Color: 1
Size: 40 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 2
Size: 497 Color: 4
Size: 98 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 2
Size: 238 Color: 4
Size: 16 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1889 Color: 2
Size: 427 Color: 0
Size: 84 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 2
Size: 611 Color: 3
Size: 122 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 2
Size: 252 Color: 3
Size: 24 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 0
Size: 610 Color: 0
Size: 120 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1205 Color: 0
Size: 997 Color: 2
Size: 198 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 1
Size: 549 Color: 4
Size: 108 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 3
Size: 302 Color: 4
Size: 56 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 1
Size: 842 Color: 1
Size: 168 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 0
Size: 283 Color: 0
Size: 56 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 1
Size: 367 Color: 2
Size: 72 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 4
Size: 822 Color: 0
Size: 160 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 0
Size: 533 Color: 1
Size: 106 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1221 Color: 0
Size: 983 Color: 4
Size: 196 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2130 Color: 1
Size: 226 Color: 2
Size: 44 Color: 1

Bin 26: 0 of cap free
Amount of items: 4
Items: 
Size: 1920 Color: 0
Size: 356 Color: 2
Size: 80 Color: 2
Size: 44 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 1
Size: 331 Color: 0
Size: 64 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1857 Color: 3
Size: 453 Color: 4
Size: 90 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 1
Size: 619 Color: 2
Size: 122 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 0
Size: 802 Color: 4
Size: 160 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 0
Size: 401 Color: 3
Size: 78 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2070 Color: 4
Size: 298 Color: 1
Size: 32 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 2
Size: 678 Color: 1
Size: 132 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 0
Size: 993 Color: 2
Size: 198 Color: 0

Bin 35: 0 of cap free
Amount of items: 4
Items: 
Size: 2007 Color: 3
Size: 329 Color: 1
Size: 32 Color: 3
Size: 32 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 1
Size: 506 Color: 4
Size: 100 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 4
Size: 835 Color: 4
Size: 166 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1365 Color: 2
Size: 863 Color: 3
Size: 172 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2095 Color: 0
Size: 255 Color: 4
Size: 50 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1881 Color: 3
Size: 433 Color: 4
Size: 86 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1547 Color: 1
Size: 711 Color: 4
Size: 142 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 4
Size: 338 Color: 0
Size: 64 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1873 Color: 4
Size: 441 Color: 1
Size: 86 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 0
Size: 262 Color: 3
Size: 48 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 4
Size: 249 Color: 2
Size: 48 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 2
Size: 350 Color: 0
Size: 68 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 2
Size: 974 Color: 3
Size: 192 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 0
Size: 1002 Color: 2
Size: 196 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2041 Color: 4
Size: 301 Color: 0
Size: 58 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 3
Size: 731 Color: 2
Size: 146 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2049 Color: 3
Size: 293 Color: 4
Size: 58 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 4
Size: 631 Color: 2
Size: 126 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 2
Size: 466 Color: 3
Size: 92 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1052 Color: 1
Size: 888 Color: 3
Size: 460 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 1
Size: 554 Color: 3
Size: 108 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 0
Size: 365 Color: 0
Size: 72 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1958 Color: 4
Size: 382 Color: 3
Size: 60 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 0
Size: 605 Color: 1
Size: 120 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 0
Size: 559 Color: 4
Size: 64 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 4
Size: 242 Color: 2
Size: 48 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2057 Color: 0
Size: 287 Color: 2
Size: 56 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1937 Color: 3
Size: 387 Color: 0
Size: 76 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 3
Size: 1025 Color: 2
Size: 174 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 0
Size: 269 Color: 0
Size: 52 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 790 Color: 0
Size: 40 Color: 0

Total size: 156000
Total free space: 0

