Capicity Bin: 1001
Lower Bound: 667

Bins used: 667
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 10
Size: 296 Color: 10
Size: 251 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 3
Size: 285 Color: 19
Size: 254 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 12
Size: 266 Color: 0
Size: 258 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 10
Size: 283 Color: 19
Size: 261 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 18
Size: 333 Color: 11
Size: 267 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 10
Size: 273 Color: 9
Size: 251 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 6
Size: 294 Color: 9
Size: 256 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 8
Size: 347 Color: 18
Size: 300 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7
Size: 334 Color: 4
Size: 296 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 6
Size: 373 Color: 2
Size: 250 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 6
Size: 283 Color: 12
Size: 271 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 13
Size: 261 Color: 0
Size: 253 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 9
Size: 290 Color: 16
Size: 275 Color: 17

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 11
Size: 295 Color: 2
Size: 271 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 351 Color: 11
Size: 266 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 4
Size: 289 Color: 6
Size: 289 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 5
Size: 359 Color: 14
Size: 268 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 15
Size: 320 Color: 0
Size: 254 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 11
Size: 325 Color: 18
Size: 253 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7
Size: 321 Color: 2
Size: 282 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 2
Size: 278 Color: 1
Size: 250 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 5
Size: 288 Color: 12
Size: 267 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 8
Size: 332 Color: 19
Size: 299 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 15
Size: 336 Color: 7
Size: 284 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 288 Color: 16
Size: 259 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 5
Size: 276 Color: 13
Size: 259 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 258 Color: 16
Size: 253 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 16
Size: 326 Color: 10
Size: 296 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 2
Size: 292 Color: 16
Size: 272 Color: 11

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 17
Size: 334 Color: 13
Size: 252 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 19
Size: 262 Color: 19
Size: 259 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 11
Size: 316 Color: 17
Size: 302 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 3
Size: 313 Color: 13
Size: 275 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 15
Size: 302 Color: 16
Size: 254 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 263 Color: 4
Size: 252 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 5
Size: 355 Color: 5
Size: 251 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 10
Size: 345 Color: 16
Size: 300 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 12
Size: 341 Color: 3
Size: 319 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 3
Size: 289 Color: 6
Size: 271 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 14
Size: 317 Color: 15
Size: 284 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 19
Size: 333 Color: 3
Size: 303 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 15
Size: 337 Color: 12
Size: 301 Color: 8

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 317 Color: 5
Size: 252 Color: 14

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 16
Size: 287 Color: 0
Size: 276 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9
Size: 262 Color: 3
Size: 254 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 5
Size: 326 Color: 7
Size: 283 Color: 16

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 19
Size: 270 Color: 13
Size: 259 Color: 8

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 17
Size: 281 Color: 2
Size: 275 Color: 13

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 4
Size: 356 Color: 4
Size: 285 Color: 13

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 5
Size: 294 Color: 6
Size: 250 Color: 8

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 255 Color: 13
Size: 255 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 18
Size: 315 Color: 0
Size: 262 Color: 14

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 15
Size: 301 Color: 4
Size: 256 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 6
Size: 350 Color: 19
Size: 251 Color: 6

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 15
Size: 272 Color: 18
Size: 252 Color: 17

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 17
Size: 288 Color: 10
Size: 263 Color: 16

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 0
Size: 303 Color: 16
Size: 268 Color: 15

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 10
Size: 315 Color: 1
Size: 308 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 4
Size: 317 Color: 1
Size: 267 Color: 11

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 16
Size: 338 Color: 10
Size: 255 Color: 18

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8
Size: 345 Color: 5
Size: 250 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 14
Size: 275 Color: 19
Size: 274 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 15
Size: 308 Color: 18
Size: 283 Color: 13

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 17
Size: 317 Color: 12
Size: 263 Color: 15

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 3
Size: 271 Color: 15
Size: 254 Color: 19

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 2
Size: 310 Color: 0
Size: 254 Color: 17

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 6
Size: 336 Color: 5
Size: 271 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 15
Size: 311 Color: 5
Size: 255 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7
Size: 353 Color: 13
Size: 262 Color: 12

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 11
Size: 293 Color: 1
Size: 261 Color: 8

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 313 Color: 13
Size: 272 Color: 16

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 299 Color: 7
Size: 270 Color: 6

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 13
Size: 269 Color: 1
Size: 268 Color: 6

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 15
Size: 306 Color: 5
Size: 268 Color: 7

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 18
Size: 310 Color: 6
Size: 295 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 19
Size: 277 Color: 13
Size: 257 Color: 13

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 18
Size: 332 Color: 17
Size: 302 Color: 15

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 10
Size: 311 Color: 15
Size: 270 Color: 15

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 3
Size: 279 Color: 13
Size: 250 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 8
Size: 362 Color: 6
Size: 259 Color: 6

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 18
Size: 293 Color: 14
Size: 265 Color: 17

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 18
Size: 266 Color: 8
Size: 258 Color: 10

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 6
Size: 254 Color: 15
Size: 251 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 3
Size: 353 Color: 15
Size: 260 Color: 6

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 17
Size: 351 Color: 12
Size: 296 Color: 18

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 3
Size: 338 Color: 13
Size: 260 Color: 18

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 17
Size: 307 Color: 2
Size: 287 Color: 17

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 4
Size: 367 Color: 10
Size: 266 Color: 12

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 15
Size: 288 Color: 17
Size: 268 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 12
Size: 326 Color: 0
Size: 309 Color: 12

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 17
Size: 281 Color: 17
Size: 274 Color: 15

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 16
Size: 281 Color: 17
Size: 266 Color: 19

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 0
Size: 319 Color: 9
Size: 257 Color: 6

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 11
Size: 352 Color: 17
Size: 296 Color: 18

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 327 Color: 0
Size: 270 Color: 5

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 17
Size: 311 Color: 8
Size: 253 Color: 12

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 305 Color: 13
Size: 300 Color: 15

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 5
Size: 270 Color: 2
Size: 262 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 16
Size: 283 Color: 3
Size: 255 Color: 8

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 11
Size: 305 Color: 10
Size: 262 Color: 14

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 5
Size: 301 Color: 12
Size: 267 Color: 16

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 5
Size: 290 Color: 13
Size: 254 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 19
Size: 326 Color: 5
Size: 255 Color: 5

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 18
Size: 279 Color: 9
Size: 257 Color: 2

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 11
Size: 342 Color: 2
Size: 306 Color: 16

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 11
Size: 293 Color: 10
Size: 257 Color: 6

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6
Size: 343 Color: 6
Size: 299 Color: 19

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 16
Size: 344 Color: 14
Size: 275 Color: 8

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 6
Size: 253 Color: 19
Size: 251 Color: 11

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 14
Size: 330 Color: 10
Size: 265 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 10
Size: 282 Color: 11
Size: 279 Color: 5

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 2
Size: 269 Color: 8
Size: 263 Color: 14

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 12
Size: 295 Color: 1
Size: 261 Color: 10

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 5
Size: 331 Color: 11
Size: 290 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 18
Size: 315 Color: 13
Size: 267 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 9
Size: 340 Color: 16
Size: 256 Color: 8

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 14
Size: 369 Color: 2
Size: 259 Color: 18

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 8
Size: 324 Color: 17
Size: 323 Color: 18

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 9
Size: 333 Color: 19
Size: 290 Color: 16

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 19
Size: 284 Color: 10
Size: 263 Color: 19

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 15
Size: 339 Color: 16
Size: 279 Color: 11

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 273 Color: 14
Size: 260 Color: 9

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 18
Size: 349 Color: 2
Size: 291 Color: 10

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 333 Color: 7
Size: 310 Color: 10

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 8
Size: 366 Color: 16
Size: 255 Color: 9

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 310 Color: 13
Size: 294 Color: 9

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 18
Size: 258 Color: 14
Size: 254 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 2
Size: 253 Color: 2
Size: 252 Color: 9

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 9
Size: 341 Color: 9
Size: 274 Color: 4

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 11
Size: 334 Color: 13
Size: 251 Color: 3

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 315 Color: 8
Size: 314 Color: 18

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 18
Size: 315 Color: 3
Size: 294 Color: 19

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 15
Size: 342 Color: 3
Size: 264 Color: 5

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 10
Size: 300 Color: 17
Size: 287 Color: 10

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9
Size: 270 Color: 14
Size: 252 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 275 Color: 13
Size: 271 Color: 5

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 15
Size: 294 Color: 1
Size: 267 Color: 14

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 14
Size: 346 Color: 13
Size: 291 Color: 6

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 19
Size: 276 Color: 3
Size: 274 Color: 19

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 17
Size: 321 Color: 13
Size: 285 Color: 4

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 9
Size: 302 Color: 2
Size: 270 Color: 6

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 11
Size: 253 Color: 12
Size: 251 Color: 8

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 19
Size: 265 Color: 15
Size: 254 Color: 13

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 5
Size: 307 Color: 9
Size: 303 Color: 9

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 11
Size: 315 Color: 12
Size: 297 Color: 17

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 15
Size: 297 Color: 8
Size: 281 Color: 16

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 16
Size: 253 Color: 3
Size: 252 Color: 18

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 322 Color: 18
Size: 314 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 4
Size: 336 Color: 0
Size: 311 Color: 4

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 3
Size: 329 Color: 3
Size: 254 Color: 5

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 10
Size: 278 Color: 5
Size: 258 Color: 18

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 7
Size: 325 Color: 17
Size: 268 Color: 15

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 369 Color: 3
Size: 257 Color: 17

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 16
Size: 337 Color: 1
Size: 282 Color: 13

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 301 Color: 1
Size: 258 Color: 7

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 17
Size: 278 Color: 4
Size: 265 Color: 9

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 18
Size: 312 Color: 15
Size: 253 Color: 18

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 297 Color: 9
Size: 273 Color: 8

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 10
Size: 281 Color: 13
Size: 280 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 14
Size: 291 Color: 10
Size: 269 Color: 14

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 2
Size: 313 Color: 13
Size: 293 Color: 13

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 19
Size: 325 Color: 14
Size: 299 Color: 18

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 341 Color: 7
Size: 257 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 5
Size: 272 Color: 10
Size: 266 Color: 14

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 19
Size: 316 Color: 3
Size: 253 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 4
Size: 344 Color: 19
Size: 310 Color: 6

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 11
Size: 283 Color: 8
Size: 278 Color: 12

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 15
Size: 340 Color: 14
Size: 250 Color: 12

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 3
Size: 346 Color: 7
Size: 291 Color: 4

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 18
Size: 301 Color: 5
Size: 284 Color: 14

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 16
Size: 339 Color: 9
Size: 266 Color: 17

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 11
Size: 279 Color: 3
Size: 250 Color: 3

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 329 Color: 13
Size: 298 Color: 6

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 277 Color: 11
Size: 262 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 14
Size: 285 Color: 13
Size: 259 Color: 12

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 7
Size: 273 Color: 19
Size: 265 Color: 3

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 17
Size: 269 Color: 9
Size: 269 Color: 2

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8
Size: 301 Color: 9
Size: 280 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 9
Size: 325 Color: 2
Size: 265 Color: 2

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 13
Size: 317 Color: 8
Size: 308 Color: 6

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 337 Color: 17
Size: 282 Color: 17

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 0
Size: 273 Color: 13
Size: 262 Color: 5

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 4
Size: 309 Color: 19
Size: 254 Color: 18

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 14
Size: 332 Color: 5
Size: 321 Color: 7

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 374 Color: 14
Size: 251 Color: 12

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 12
Size: 353 Color: 16
Size: 267 Color: 11

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 8
Size: 260 Color: 15
Size: 251 Color: 15

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 14
Size: 297 Color: 19
Size: 264 Color: 7

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 16
Size: 287 Color: 0
Size: 260 Color: 5

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9
Size: 272 Color: 8
Size: 265 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 6
Size: 333 Color: 4
Size: 256 Color: 3

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 18
Size: 336 Color: 2
Size: 317 Color: 18

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 262 Color: 10
Size: 256 Color: 11

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 15
Size: 277 Color: 8
Size: 263 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 7
Size: 316 Color: 3
Size: 263 Color: 10

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 15
Size: 357 Color: 5
Size: 282 Color: 7

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 318 Color: 3
Size: 251 Color: 2

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 298 Color: 3
Size: 265 Color: 17

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 5
Size: 321 Color: 15
Size: 254 Color: 3

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 14
Size: 342 Color: 9
Size: 266 Color: 12

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 19
Size: 304 Color: 6
Size: 266 Color: 14

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 8
Size: 331 Color: 1
Size: 323 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 7
Size: 343 Color: 17
Size: 300 Color: 15

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 7
Size: 280 Color: 18
Size: 268 Color: 3

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 10
Size: 298 Color: 12
Size: 252 Color: 15

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 17
Size: 365 Color: 10
Size: 267 Color: 6

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 19
Size: 344 Color: 7
Size: 273 Color: 6

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 12
Size: 331 Color: 4
Size: 278 Color: 8

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 2
Size: 351 Color: 0
Size: 274 Color: 12

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 264 Color: 3
Size: 257 Color: 6

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 11
Size: 301 Color: 13
Size: 278 Color: 10

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 11
Size: 333 Color: 18
Size: 300 Color: 12

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 3
Size: 278 Color: 11
Size: 252 Color: 7

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 10
Size: 339 Color: 13
Size: 261 Color: 3

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 13
Size: 354 Color: 19
Size: 269 Color: 18

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 5
Size: 284 Color: 14
Size: 264 Color: 11

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 6
Size: 316 Color: 14
Size: 297 Color: 14

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 19
Size: 292 Color: 10
Size: 271 Color: 4

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 10
Size: 309 Color: 12
Size: 289 Color: 7

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 289 Color: 4
Size: 259 Color: 13

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 4
Size: 264 Color: 14
Size: 253 Color: 5

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 12
Size: 315 Color: 16
Size: 264 Color: 14

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 4
Size: 290 Color: 3
Size: 257 Color: 11

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 2
Size: 318 Color: 15
Size: 318 Color: 8

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 289 Color: 10
Size: 257 Color: 19

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 344 Color: 15
Size: 284 Color: 17

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 18
Size: 272 Color: 13
Size: 261 Color: 14

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 17
Size: 316 Color: 10
Size: 267 Color: 8

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 362 Color: 14
Size: 254 Color: 11

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 17
Size: 336 Color: 16
Size: 283 Color: 15

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 305 Color: 14
Size: 300 Color: 18

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 6
Size: 254 Color: 19
Size: 254 Color: 4

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 17
Size: 293 Color: 16
Size: 274 Color: 19

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 8
Size: 266 Color: 18
Size: 263 Color: 7

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 346 Color: 0
Size: 270 Color: 11

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 17
Size: 337 Color: 17
Size: 269 Color: 12

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 6
Size: 371 Color: 12
Size: 251 Color: 13

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 18
Size: 324 Color: 2
Size: 318 Color: 4

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 16
Size: 274 Color: 13
Size: 265 Color: 7

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 6
Size: 353 Color: 5
Size: 276 Color: 2

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 13
Size: 275 Color: 19
Size: 268 Color: 11

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 3
Size: 311 Color: 19
Size: 272 Color: 16

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 4
Size: 301 Color: 5
Size: 255 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8
Size: 327 Color: 0
Size: 263 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 362 Color: 10
Size: 257 Color: 10

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 16
Size: 328 Color: 5
Size: 327 Color: 17

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 6
Size: 334 Color: 19
Size: 262 Color: 12

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 3
Size: 337 Color: 14
Size: 320 Color: 8

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 2
Size: 318 Color: 0
Size: 310 Color: 9

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 295 Color: 13
Size: 250 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 263 Color: 1
Size: 253 Color: 11

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 355 Color: 10
Size: 273 Color: 9

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 10
Size: 288 Color: 10
Size: 259 Color: 3

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 12
Size: 347 Color: 2
Size: 282 Color: 3

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7
Size: 305 Color: 4
Size: 301 Color: 15

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 12
Size: 306 Color: 2
Size: 278 Color: 17

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 6
Size: 344 Color: 1
Size: 267 Color: 16

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 2
Size: 271 Color: 6
Size: 252 Color: 16

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 19
Size: 269 Color: 7
Size: 255 Color: 14

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 318 Color: 2
Size: 270 Color: 18

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 19
Size: 306 Color: 12
Size: 266 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 15
Size: 339 Color: 5
Size: 269 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 8
Size: 297 Color: 15
Size: 256 Color: 15

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 7
Size: 350 Color: 12
Size: 285 Color: 17

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 0
Size: 334 Color: 4
Size: 299 Color: 11

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 11
Size: 279 Color: 19
Size: 250 Color: 4

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 17
Size: 335 Color: 8
Size: 303 Color: 6

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 19
Size: 337 Color: 19
Size: 279 Color: 9

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 15
Size: 279 Color: 13
Size: 271 Color: 6

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 17
Size: 280 Color: 13
Size: 271 Color: 15

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 277 Color: 5
Size: 258 Color: 6

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 5
Size: 323 Color: 1
Size: 299 Color: 4

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 7
Size: 316 Color: 10
Size: 270 Color: 10

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 10
Size: 363 Color: 10
Size: 271 Color: 2

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 15
Size: 278 Color: 18
Size: 277 Color: 18

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 316 Color: 12
Size: 277 Color: 3

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 7
Size: 289 Color: 5
Size: 258 Color: 19

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 15
Size: 333 Color: 4
Size: 290 Color: 9

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 6
Size: 284 Color: 15
Size: 270 Color: 7

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 9
Size: 298 Color: 4
Size: 282 Color: 6

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 11
Size: 351 Color: 3
Size: 276 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 12
Size: 286 Color: 18
Size: 267 Color: 13

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 10
Size: 301 Color: 11
Size: 254 Color: 1

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 11
Size: 278 Color: 17
Size: 259 Color: 12

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 6
Size: 343 Color: 14
Size: 311 Color: 17

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 11
Size: 260 Color: 10
Size: 252 Color: 11

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 273 Color: 1
Size: 265 Color: 6

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 12
Size: 346 Color: 4
Size: 274 Color: 10

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 17
Size: 326 Color: 19
Size: 303 Color: 19

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 349 Color: 0
Size: 251 Color: 3

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 15
Size: 335 Color: 19
Size: 261 Color: 15

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 7
Size: 281 Color: 4
Size: 264 Color: 8

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 18
Size: 319 Color: 13
Size: 311 Color: 18

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 13
Size: 292 Color: 0
Size: 288 Color: 6

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 3
Size: 256 Color: 3
Size: 251 Color: 8

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 18
Size: 296 Color: 2
Size: 269 Color: 16

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 17
Size: 320 Color: 13
Size: 280 Color: 19

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 10
Size: 279 Color: 9
Size: 271 Color: 2

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 14
Size: 353 Color: 2
Size: 291 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 11
Size: 320 Color: 3
Size: 271 Color: 14

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 4
Size: 317 Color: 13
Size: 260 Color: 19

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 3
Size: 299 Color: 9
Size: 271 Color: 8

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 14
Size: 297 Color: 0
Size: 285 Color: 7

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 12
Size: 265 Color: 5
Size: 258 Color: 4

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 18
Size: 253 Color: 11
Size: 252 Color: 16

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 297 Color: 0
Size: 250 Color: 19

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 7
Size: 315 Color: 16
Size: 286 Color: 5

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 8
Size: 266 Color: 5
Size: 254 Color: 9

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 3
Size: 349 Color: 3
Size: 302 Color: 11

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 343 Color: 17
Size: 267 Color: 18

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 2
Size: 300 Color: 0
Size: 260 Color: 12

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 324 Color: 14
Size: 277 Color: 6

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 5
Size: 346 Color: 18
Size: 304 Color: 10

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 274 Color: 5
Size: 255 Color: 19

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 281 Color: 16
Size: 258 Color: 5

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1
Size: 329 Color: 11
Size: 321 Color: 12

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 344 Color: 5
Size: 275 Color: 11

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 2
Size: 310 Color: 2
Size: 284 Color: 4

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 12
Size: 333 Color: 18
Size: 333 Color: 14

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 2
Size: 278 Color: 3
Size: 277 Color: 16

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 6
Size: 343 Color: 4
Size: 254 Color: 15

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 16
Size: 255 Color: 3
Size: 250 Color: 9

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 12
Size: 297 Color: 17
Size: 255 Color: 6

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 4
Size: 288 Color: 7
Size: 273 Color: 7

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 5
Size: 323 Color: 6
Size: 259 Color: 1

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 12
Size: 312 Color: 17
Size: 298 Color: 11

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 16
Size: 316 Color: 3
Size: 293 Color: 16

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 13
Size: 331 Color: 6
Size: 277 Color: 15

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 277 Color: 9
Size: 263 Color: 7

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 11
Size: 336 Color: 10
Size: 264 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 4
Size: 330 Color: 0
Size: 277 Color: 13

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 15
Size: 317 Color: 1
Size: 300 Color: 9

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 6
Size: 288 Color: 0
Size: 277 Color: 3

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 327 Color: 9
Size: 290 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 5
Size: 273 Color: 17
Size: 260 Color: 17

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 4
Size: 346 Color: 10
Size: 276 Color: 19

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 8
Size: 272 Color: 17
Size: 256 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 3
Size: 291 Color: 9
Size: 280 Color: 8

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 13
Size: 330 Color: 16
Size: 278 Color: 8

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 12
Size: 319 Color: 9
Size: 277 Color: 2

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 19
Size: 332 Color: 16
Size: 273 Color: 12

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 5
Size: 346 Color: 9
Size: 266 Color: 15

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 8
Size: 337 Color: 18
Size: 279 Color: 2

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 11
Size: 356 Color: 3
Size: 276 Color: 14

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 18
Size: 315 Color: 9
Size: 254 Color: 5

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 19
Size: 325 Color: 18
Size: 274 Color: 6

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 262 Color: 5
Size: 253 Color: 12

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 16
Size: 333 Color: 14
Size: 250 Color: 14

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 12
Size: 338 Color: 0
Size: 302 Color: 11

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 7
Size: 307 Color: 0
Size: 286 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 18
Size: 342 Color: 2
Size: 308 Color: 17

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 303 Color: 8
Size: 252 Color: 9

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 16
Size: 332 Color: 15
Size: 250 Color: 3

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 0
Size: 322 Color: 16
Size: 310 Color: 13

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 14
Size: 347 Color: 3
Size: 269 Color: 5

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 3
Size: 292 Color: 19
Size: 257 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 295 Color: 8
Size: 250 Color: 6

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 342 Color: 4
Size: 277 Color: 5

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 18
Size: 297 Color: 11
Size: 265 Color: 19

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 7
Size: 304 Color: 8
Size: 272 Color: 10

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 15
Size: 360 Color: 18
Size: 260 Color: 13

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 13
Size: 324 Color: 18
Size: 258 Color: 15

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 354 Color: 12
Size: 251 Color: 16

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 11
Size: 258 Color: 2
Size: 250 Color: 15

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 17
Size: 301 Color: 8
Size: 283 Color: 2

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 5
Size: 281 Color: 13
Size: 258 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 16
Size: 340 Color: 0
Size: 303 Color: 19

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 6
Size: 327 Color: 4
Size: 285 Color: 19

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 16
Size: 304 Color: 12
Size: 278 Color: 18

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 347 Color: 10
Size: 252 Color: 18

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 12
Size: 262 Color: 12
Size: 262 Color: 5

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 12
Size: 293 Color: 11
Size: 265 Color: 15

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 9
Size: 336 Color: 13
Size: 271 Color: 16

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 4
Size: 292 Color: 0
Size: 282 Color: 2

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 19
Size: 291 Color: 5
Size: 290 Color: 14

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 18
Size: 332 Color: 11
Size: 301 Color: 4

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 19
Size: 277 Color: 15
Size: 253 Color: 12

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 280 Color: 12
Size: 264 Color: 19

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 15
Size: 263 Color: 11
Size: 253 Color: 14

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 16
Size: 339 Color: 19
Size: 283 Color: 2

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 18
Size: 354 Color: 18
Size: 293 Color: 14

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 14
Size: 272 Color: 8
Size: 251 Color: 14

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 17
Size: 288 Color: 5
Size: 262 Color: 13

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 18
Size: 316 Color: 10
Size: 250 Color: 13

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 13
Size: 303 Color: 14
Size: 300 Color: 12

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 2
Size: 341 Color: 10
Size: 292 Color: 8

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 11
Size: 357 Color: 15
Size: 252 Color: 10

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 4
Size: 321 Color: 2
Size: 257 Color: 17

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 16
Size: 346 Color: 14
Size: 274 Color: 5

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 7
Size: 339 Color: 18
Size: 307 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 2
Size: 322 Color: 17
Size: 311 Color: 8

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 2
Size: 266 Color: 8
Size: 259 Color: 11

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 12
Size: 283 Color: 18
Size: 276 Color: 18

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 9
Size: 313 Color: 10
Size: 261 Color: 5

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 8
Size: 310 Color: 5
Size: 308 Color: 6

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 17
Size: 320 Color: 12
Size: 278 Color: 15

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 330 Color: 1
Size: 276 Color: 18

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 2
Size: 282 Color: 0
Size: 273 Color: 5

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 355 Color: 19
Size: 253 Color: 6

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 2
Size: 294 Color: 0
Size: 250 Color: 3

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 8
Size: 278 Color: 1
Size: 250 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 8
Size: 269 Color: 8
Size: 255 Color: 10

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 6
Size: 254 Color: 4
Size: 253 Color: 0

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 19
Size: 327 Color: 4
Size: 250 Color: 8

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 17
Size: 351 Color: 14
Size: 253 Color: 6

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 275 Color: 1
Size: 252 Color: 3

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 16
Size: 256 Color: 5
Size: 251 Color: 7

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 2
Size: 294 Color: 11
Size: 279 Color: 8

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 7
Size: 323 Color: 8
Size: 315 Color: 13

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 8
Size: 315 Color: 6
Size: 287 Color: 3

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 8
Size: 291 Color: 16
Size: 265 Color: 4

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 18
Size: 331 Color: 9
Size: 265 Color: 4

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 11
Size: 262 Color: 19
Size: 251 Color: 10

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 310 Color: 14
Size: 271 Color: 3

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 11
Size: 292 Color: 6
Size: 257 Color: 13

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 6
Size: 342 Color: 6
Size: 252 Color: 5

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 10
Size: 336 Color: 16
Size: 274 Color: 4

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 10
Size: 260 Color: 6
Size: 250 Color: 12

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7
Size: 329 Color: 19
Size: 297 Color: 19

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 13
Size: 318 Color: 7
Size: 258 Color: 19

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 5
Size: 291 Color: 7
Size: 274 Color: 18

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 18
Size: 305 Color: 5
Size: 287 Color: 17

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8
Size: 315 Color: 10
Size: 252 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 2
Size: 302 Color: 12
Size: 269 Color: 12

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 5
Size: 338 Color: 8
Size: 278 Color: 8

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 14
Size: 334 Color: 7
Size: 274 Color: 12

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 16
Size: 336 Color: 7
Size: 297 Color: 0

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 9
Size: 336 Color: 2
Size: 285 Color: 5

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 14
Size: 283 Color: 0
Size: 263 Color: 14

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 7
Size: 318 Color: 18
Size: 251 Color: 5

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 337 Color: 8
Size: 276 Color: 8

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 19
Size: 349 Color: 17
Size: 254 Color: 14

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 359 Color: 6
Size: 254 Color: 1

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 11
Size: 288 Color: 17
Size: 260 Color: 15

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 11
Size: 302 Color: 9
Size: 253 Color: 16

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 10
Size: 296 Color: 0
Size: 269 Color: 8

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 300 Color: 0
Size: 290 Color: 10

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 17
Size: 255 Color: 11
Size: 252 Color: 8

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 266 Color: 18
Size: 251 Color: 4

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 9
Size: 311 Color: 4
Size: 305 Color: 3

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 0
Size: 277 Color: 3
Size: 267 Color: 7

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 7
Size: 339 Color: 1
Size: 257 Color: 7

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 10
Size: 314 Color: 16
Size: 285 Color: 1

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 17
Size: 320 Color: 18
Size: 316 Color: 4

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 10
Size: 266 Color: 0
Size: 264 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 10
Size: 343 Color: 16
Size: 293 Color: 19

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 7
Size: 304 Color: 7
Size: 296 Color: 11

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 11
Size: 335 Color: 3
Size: 278 Color: 19

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 13
Size: 318 Color: 13
Size: 265 Color: 19

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 17
Size: 312 Color: 9
Size: 285 Color: 13

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 6
Size: 340 Color: 15
Size: 272 Color: 16

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 19
Size: 336 Color: 8
Size: 316 Color: 8

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 5
Size: 282 Color: 18
Size: 250 Color: 13

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 13
Size: 352 Color: 17
Size: 263 Color: 0

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 18
Size: 333 Color: 14
Size: 297 Color: 14

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 12
Size: 352 Color: 2
Size: 285 Color: 10

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 14
Size: 319 Color: 10
Size: 285 Color: 11

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 18
Size: 329 Color: 11
Size: 257 Color: 2

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8
Size: 307 Color: 15
Size: 288 Color: 3

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 16
Size: 337 Color: 12
Size: 254 Color: 16

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 14
Size: 285 Color: 9
Size: 255 Color: 10

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 10
Size: 286 Color: 3
Size: 254 Color: 4

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 17
Size: 361 Color: 6
Size: 262 Color: 10

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8
Size: 320 Color: 16
Size: 271 Color: 15

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 18
Size: 317 Color: 10
Size: 278 Color: 14

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 7
Size: 294 Color: 15
Size: 252 Color: 3

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 5
Size: 371 Color: 3
Size: 252 Color: 11

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 18
Size: 333 Color: 6
Size: 312 Color: 14

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 2
Size: 351 Color: 17
Size: 282 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 14
Size: 263 Color: 17
Size: 259 Color: 12

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 4
Size: 317 Color: 13
Size: 250 Color: 17

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 13
Size: 306 Color: 13
Size: 268 Color: 15

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 273 Color: 13
Size: 258 Color: 18

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 19
Size: 327 Color: 12
Size: 315 Color: 4

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9
Size: 271 Color: 6
Size: 268 Color: 7

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 359 Color: 3
Size: 256 Color: 15

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 6
Size: 275 Color: 1
Size: 263 Color: 4

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 12
Size: 262 Color: 3
Size: 255 Color: 3

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 16
Size: 262 Color: 19
Size: 256 Color: 1

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 8
Size: 333 Color: 6
Size: 273 Color: 5

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 10
Size: 360 Color: 5
Size: 262 Color: 12

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 12
Size: 278 Color: 0
Size: 250 Color: 11

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 302 Color: 5
Size: 254 Color: 3

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 11
Size: 362 Color: 17
Size: 272 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 14
Size: 327 Color: 19
Size: 265 Color: 13

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 5
Size: 343 Color: 17
Size: 308 Color: 9

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 13
Size: 353 Color: 6
Size: 283 Color: 13

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 18
Size: 362 Color: 8
Size: 264 Color: 18

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 14
Size: 300 Color: 15
Size: 254 Color: 5

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 7
Size: 333 Color: 19
Size: 324 Color: 9

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 3
Size: 276 Color: 4
Size: 262 Color: 9

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 8
Size: 264 Color: 12
Size: 257 Color: 19

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 13
Size: 363 Color: 4
Size: 260 Color: 10

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 8
Size: 319 Color: 1
Size: 310 Color: 15

Bin 495: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 8
Size: 250 Color: 13
Size: 250 Color: 8
Size: 250 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 265 Color: 16
Size: 251 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 10
Size: 267 Color: 12
Size: 266 Color: 17

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 16
Size: 350 Color: 19
Size: 268 Color: 6

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 16
Size: 311 Color: 11
Size: 282 Color: 4

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 3
Size: 334 Color: 10
Size: 252 Color: 9

Bin 501: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 500 Color: 9

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 14
Size: 341 Color: 17
Size: 296 Color: 7

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 8
Size: 315 Color: 16
Size: 302 Color: 16

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 12
Size: 251 Color: 5
Size: 251 Color: 4

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 14
Size: 334 Color: 12
Size: 291 Color: 2

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 19
Size: 339 Color: 6
Size: 252 Color: 6

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 16
Size: 306 Color: 6
Size: 275 Color: 9

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 19
Size: 351 Color: 13
Size: 257 Color: 11

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 6
Size: 323 Color: 12
Size: 268 Color: 9

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 10
Size: 282 Color: 1
Size: 261 Color: 11

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 276 Color: 0
Size: 274 Color: 14

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 17
Size: 351 Color: 13
Size: 253 Color: 14

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9
Size: 256 Color: 16
Size: 253 Color: 7

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 4
Size: 331 Color: 18
Size: 331 Color: 8

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 10
Size: 309 Color: 5
Size: 270 Color: 5

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 13
Size: 311 Color: 0
Size: 252 Color: 12

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 5
Size: 257 Color: 9
Size: 250 Color: 1

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 17
Size: 277 Color: 13
Size: 273 Color: 5

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 353 Color: 7
Size: 289 Color: 16

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 5
Size: 309 Color: 5
Size: 276 Color: 14

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 276 Color: 10
Size: 254 Color: 3

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 8
Size: 336 Color: 12
Size: 322 Color: 9

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6
Size: 332 Color: 10
Size: 318 Color: 6

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 16
Size: 258 Color: 9
Size: 255 Color: 17

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 7
Size: 255 Color: 1
Size: 252 Color: 11

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 8
Size: 298 Color: 0
Size: 252 Color: 12

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 13
Size: 321 Color: 9
Size: 308 Color: 2

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 19
Size: 324 Color: 8
Size: 283 Color: 2

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 14
Size: 280 Color: 17
Size: 263 Color: 1

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 6
Size: 319 Color: 3
Size: 293 Color: 16

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 17
Size: 292 Color: 14
Size: 260 Color: 15

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 9
Size: 260 Color: 8
Size: 259 Color: 11

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 13
Size: 331 Color: 18
Size: 285 Color: 11

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 13
Size: 344 Color: 8
Size: 299 Color: 8

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 3
Size: 335 Color: 2
Size: 302 Color: 12

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 334 Color: 7
Size: 276 Color: 14

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 12
Size: 269 Color: 5
Size: 253 Color: 4

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 10
Size: 331 Color: 8
Size: 265 Color: 10

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 13
Size: 274 Color: 1
Size: 258 Color: 7

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 5
Size: 368 Color: 13
Size: 257 Color: 15

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 19
Size: 316 Color: 12
Size: 252 Color: 5

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 2
Size: 280 Color: 11
Size: 253 Color: 3

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 14
Size: 351 Color: 18
Size: 259 Color: 11

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 10
Size: 283 Color: 7
Size: 264 Color: 13

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 12
Size: 272 Color: 5
Size: 258 Color: 9

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 5
Size: 324 Color: 13
Size: 310 Color: 5

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 18
Size: 352 Color: 10
Size: 253 Color: 12

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 303 Color: 4
Size: 276 Color: 7

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 10
Size: 302 Color: 17
Size: 283 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 326 Color: 19
Size: 256 Color: 18

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 356 Color: 6
Size: 278 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 17
Size: 257 Color: 3
Size: 255 Color: 8

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 333 Color: 3
Size: 289 Color: 15

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 12
Size: 346 Color: 13
Size: 253 Color: 3

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 307 Color: 9
Size: 289 Color: 12

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 12
Size: 338 Color: 7
Size: 277 Color: 5

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 6
Size: 286 Color: 11
Size: 281 Color: 17

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 3
Size: 279 Color: 16
Size: 264 Color: 17

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 319 Color: 2
Size: 291 Color: 12

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 350 Color: 13
Size: 276 Color: 14

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 277 Color: 6
Size: 276 Color: 7

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 17
Size: 276 Color: 10
Size: 263 Color: 12

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 10
Size: 257 Color: 9
Size: 257 Color: 5

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 15
Size: 298 Color: 18
Size: 280 Color: 4

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 350 Color: 5
Size: 254 Color: 5

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 14
Size: 347 Color: 18
Size: 262 Color: 17

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 14
Size: 309 Color: 6
Size: 285 Color: 12

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 9
Size: 299 Color: 16
Size: 288 Color: 13

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 13
Size: 322 Color: 17
Size: 308 Color: 16

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 12
Size: 326 Color: 4
Size: 302 Color: 10

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 10
Size: 334 Color: 0
Size: 300 Color: 6

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 13
Size: 276 Color: 14
Size: 253 Color: 13

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 4
Size: 326 Color: 19
Size: 322 Color: 4

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 6
Size: 330 Color: 19
Size: 279 Color: 5

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 11
Size: 303 Color: 19
Size: 272 Color: 2

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 327 Color: 3
Size: 282 Color: 9

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 6
Size: 289 Color: 17
Size: 288 Color: 18

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 7
Size: 287 Color: 8
Size: 271 Color: 12

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7
Size: 347 Color: 5
Size: 269 Color: 3

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 5
Size: 310 Color: 5
Size: 281 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 5
Size: 278 Color: 3
Size: 264 Color: 19

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 14
Size: 256 Color: 2
Size: 254 Color: 7

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 6
Size: 275 Color: 17
Size: 264 Color: 16

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 3
Size: 354 Color: 9
Size: 259 Color: 6

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 19
Size: 278 Color: 12
Size: 273 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 15
Size: 354 Color: 0
Size: 261 Color: 11

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 14
Size: 339 Color: 19
Size: 301 Color: 12

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 12
Size: 345 Color: 4
Size: 274 Color: 4

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 355 Color: 11
Size: 268 Color: 9

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 4
Size: 281 Color: 4
Size: 260 Color: 3

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 9
Size: 335 Color: 12
Size: 310 Color: 4

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 12
Size: 255 Color: 15
Size: 254 Color: 5

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 11
Size: 274 Color: 16
Size: 274 Color: 12

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 317 Color: 4
Size: 282 Color: 16

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 13
Size: 334 Color: 12
Size: 252 Color: 9

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 10
Size: 356 Color: 19
Size: 287 Color: 0

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 4
Size: 348 Color: 4
Size: 288 Color: 6

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 14
Size: 325 Color: 16
Size: 250 Color: 2

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 10
Size: 319 Color: 3
Size: 310 Color: 6

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 11
Size: 320 Color: 0
Size: 250 Color: 17

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 14
Size: 367 Color: 8
Size: 258 Color: 12

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 19
Size: 316 Color: 9
Size: 250 Color: 4

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 12
Size: 279 Color: 16
Size: 266 Color: 18

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 8
Size: 280 Color: 17
Size: 268 Color: 4

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 340 Color: 6
Size: 296 Color: 17

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 15
Size: 286 Color: 18
Size: 286 Color: 10

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 4
Size: 255 Color: 14
Size: 250 Color: 13

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 14
Size: 343 Color: 2
Size: 277 Color: 19

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 12
Size: 308 Color: 15
Size: 274 Color: 19

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 12
Size: 348 Color: 9
Size: 276 Color: 8

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 10
Size: 252 Color: 11
Size: 251 Color: 6

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 7
Size: 318 Color: 8
Size: 268 Color: 6

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 17
Size: 282 Color: 5
Size: 259 Color: 11

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 3
Size: 349 Color: 16
Size: 283 Color: 1

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 19
Size: 315 Color: 1
Size: 254 Color: 14

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 10
Size: 335 Color: 2
Size: 310 Color: 11

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7
Size: 324 Color: 17
Size: 303 Color: 16

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 359 Color: 13
Size: 259 Color: 10

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 7
Size: 289 Color: 0
Size: 276 Color: 8

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 10
Size: 285 Color: 17
Size: 259 Color: 14

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 8
Size: 257 Color: 14
Size: 254 Color: 18

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 16
Size: 256 Color: 10
Size: 252 Color: 4

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 9
Size: 301 Color: 7
Size: 287 Color: 11

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 15
Size: 357 Color: 8
Size: 285 Color: 2

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 5
Size: 332 Color: 13
Size: 270 Color: 11

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 324 Color: 6
Size: 310 Color: 16

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 16
Size: 342 Color: 10
Size: 260 Color: 14

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 16
Size: 252 Color: 9
Size: 250 Color: 8

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 12
Size: 301 Color: 16
Size: 280 Color: 15

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 12
Size: 347 Color: 6
Size: 255 Color: 17

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 301 Color: 15
Size: 279 Color: 16

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 7
Size: 315 Color: 11
Size: 258 Color: 4

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 14
Size: 281 Color: 11
Size: 254 Color: 10

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 11
Size: 299 Color: 8
Size: 279 Color: 1

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 19
Size: 342 Color: 18
Size: 303 Color: 18

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 2
Size: 283 Color: 17
Size: 283 Color: 16

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 16
Size: 292 Color: 15
Size: 284 Color: 14

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 10
Size: 324 Color: 8
Size: 295 Color: 0

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 369 Color: 14
Size: 259 Color: 16

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 17
Size: 258 Color: 10
Size: 257 Color: 9

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 4
Size: 309 Color: 15
Size: 254 Color: 14

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 11
Size: 344 Color: 17
Size: 293 Color: 9

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8
Size: 309 Color: 8
Size: 287 Color: 4

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 17
Size: 331 Color: 14
Size: 304 Color: 13

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 5
Size: 297 Color: 8
Size: 256 Color: 4

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 268 Color: 10
Size: 265 Color: 15

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 14
Size: 286 Color: 2
Size: 274 Color: 2

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 10
Size: 322 Color: 19
Size: 308 Color: 15

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 7
Size: 302 Color: 0
Size: 280 Color: 1

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 8
Size: 265 Color: 10
Size: 258 Color: 13

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 15
Size: 335 Color: 11
Size: 309 Color: 4

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 5
Size: 278 Color: 3
Size: 262 Color: 7

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 0
Size: 266 Color: 17
Size: 257 Color: 4

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 15
Size: 315 Color: 16
Size: 311 Color: 16

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 12
Size: 345 Color: 3
Size: 260 Color: 19

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 5
Size: 297 Color: 14
Size: 293 Color: 19

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 12
Size: 329 Color: 19
Size: 312 Color: 18

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 345 Color: 16
Size: 273 Color: 19

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 289 Color: 7
Size: 264 Color: 4

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 4
Size: 322 Color: 12
Size: 296 Color: 1

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 8
Size: 335 Color: 8
Size: 331 Color: 16

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 19
Size: 271 Color: 8
Size: 250 Color: 13

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 2
Size: 271 Color: 18
Size: 263 Color: 6

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8
Size: 301 Color: 13
Size: 263 Color: 7

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 16
Size: 265 Color: 0
Size: 256 Color: 2

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 14
Size: 308 Color: 14
Size: 264 Color: 3

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 5
Size: 324 Color: 1
Size: 259 Color: 0

Total size: 667667
Total free space: 0

