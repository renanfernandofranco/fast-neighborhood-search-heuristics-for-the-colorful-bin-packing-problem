Capicity Bin: 5856
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 2320 Color: 259
Size: 1232 Color: 216
Size: 912 Color: 191
Size: 672 Color: 165
Size: 416 Color: 118
Size: 304 Color: 104

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 4032 Color: 310
Size: 1360 Color: 226
Size: 224 Color: 86
Size: 128 Color: 39
Size: 112 Color: 25

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 3346 Color: 291
Size: 2094 Color: 250
Size: 232 Color: 89
Size: 184 Color: 67

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4196 Color: 316
Size: 1524 Color: 232
Size: 136 Color: 46

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 4844 Color: 354
Size: 724 Color: 171
Size: 256 Color: 96
Size: 32 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5204 Color: 396
Size: 548 Color: 142
Size: 104 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5250 Color: 400
Size: 506 Color: 138
Size: 100 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4351 Color: 322
Size: 1255 Color: 220
Size: 250 Color: 93

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4468 Color: 327
Size: 1244 Color: 217
Size: 144 Color: 51

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4840 Color: 352
Size: 856 Color: 187
Size: 160 Color: 60

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4986 Color: 366
Size: 726 Color: 173
Size: 144 Color: 49

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4675 Color: 340
Size: 985 Color: 199
Size: 196 Color: 77

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4812 Color: 351
Size: 876 Color: 188
Size: 168 Color: 65

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 401
Size: 500 Color: 137
Size: 96 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5149 Color: 387
Size: 591 Color: 151
Size: 116 Color: 30

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2954 Color: 282
Size: 2422 Color: 260
Size: 480 Color: 127

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 3935 Color: 303
Size: 1601 Color: 237
Size: 320 Color: 109

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2936 Color: 276
Size: 2440 Color: 266
Size: 480 Color: 126

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4545 Color: 333
Size: 1093 Color: 207
Size: 218 Color: 83

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2941 Color: 280
Size: 2431 Color: 262
Size: 484 Color: 130

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4697 Color: 346
Size: 967 Color: 194
Size: 192 Color: 71

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5172 Color: 391
Size: 572 Color: 147
Size: 112 Color: 27

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 3212 Color: 285
Size: 2204 Color: 257
Size: 440 Color: 124

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4178 Color: 315
Size: 1402 Color: 227
Size: 276 Color: 100

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4951 Color: 362
Size: 755 Color: 177
Size: 150 Color: 56

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2937 Color: 277
Size: 2433 Color: 263
Size: 486 Color: 134

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 3664 Color: 295
Size: 2000 Color: 249
Size: 192 Color: 70

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5103 Color: 379
Size: 739 Color: 175
Size: 14 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 318
Size: 1322 Color: 224
Size: 260 Color: 97

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4842 Color: 353
Size: 846 Color: 186
Size: 168 Color: 63

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4517 Color: 331
Size: 1227 Color: 215
Size: 112 Color: 24

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2946 Color: 281
Size: 2426 Color: 261
Size: 484 Color: 129

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4561 Color: 334
Size: 1083 Color: 205
Size: 212 Color: 82

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2933 Color: 275
Size: 2437 Color: 265
Size: 486 Color: 135

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4689 Color: 344
Size: 973 Color: 196
Size: 194 Color: 74

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4165 Color: 313
Size: 1411 Color: 229
Size: 280 Color: 101

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4746 Color: 349
Size: 926 Color: 192
Size: 184 Color: 68

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5111 Color: 382
Size: 621 Color: 155
Size: 124 Color: 35

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4028 Color: 309
Size: 1188 Color: 212
Size: 640 Color: 159

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4436 Color: 325
Size: 1404 Color: 228
Size: 16 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5234 Color: 398
Size: 522 Color: 139
Size: 100 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4990 Color: 368
Size: 722 Color: 170
Size: 144 Color: 53

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 314
Size: 1480 Color: 231
Size: 208 Color: 81

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5186 Color: 393
Size: 562 Color: 145
Size: 108 Color: 20

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 4252 Color: 317
Size: 1340 Color: 225
Size: 264 Color: 98

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 3604 Color: 292
Size: 1884 Color: 248
Size: 368 Color: 117

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 395
Size: 554 Color: 143
Size: 100 Color: 15

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4284 Color: 319
Size: 1316 Color: 223
Size: 256 Color: 94

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 4506 Color: 329
Size: 1126 Color: 209
Size: 224 Color: 87

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2932 Color: 274
Size: 2444 Color: 269
Size: 480 Color: 128

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5084 Color: 378
Size: 644 Color: 160
Size: 128 Color: 40

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 3964 Color: 308
Size: 1580 Color: 233
Size: 312 Color: 106

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 3052 Color: 283
Size: 2772 Color: 271
Size: 32 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4642 Color: 338
Size: 1198 Color: 213
Size: 16 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 4955 Color: 363
Size: 751 Color: 176
Size: 150 Color: 55

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 4984 Color: 365
Size: 728 Color: 174
Size: 144 Color: 54

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 3659 Color: 293
Size: 1831 Color: 246
Size: 366 Color: 116

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 4876 Color: 355
Size: 884 Color: 190
Size: 96 Color: 10

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 3342 Color: 290
Size: 2098 Color: 251
Size: 416 Color: 120

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 4359 Color: 324
Size: 1249 Color: 218
Size: 248 Color: 90

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5122 Color: 383
Size: 614 Color: 154
Size: 120 Color: 32

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 3933 Color: 302
Size: 1603 Color: 238
Size: 320 Color: 110

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4924 Color: 361
Size: 780 Color: 179
Size: 152 Color: 57

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 4886 Color: 358
Size: 810 Color: 182
Size: 160 Color: 61

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 4082 Color: 311
Size: 1590 Color: 235
Size: 184 Color: 69

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5167 Color: 390
Size: 575 Color: 148
Size: 114 Color: 28

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 367
Size: 724 Color: 172
Size: 144 Color: 50

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5218 Color: 397
Size: 534 Color: 141
Size: 104 Color: 18

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4296 Color: 320
Size: 1304 Color: 222
Size: 256 Color: 95

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4674 Color: 339
Size: 986 Color: 200
Size: 196 Color: 76

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 3336 Color: 289
Size: 2104 Color: 254
Size: 416 Color: 119

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 4797 Color: 350
Size: 883 Color: 189
Size: 176 Color: 66

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5136 Color: 384
Size: 624 Color: 156
Size: 96 Color: 9

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 386
Size: 604 Color: 153
Size: 112 Color: 21

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 3946 Color: 305
Size: 1594 Color: 236
Size: 316 Color: 107

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 399
Size: 524 Color: 140
Size: 96 Color: 11

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 4564 Color: 335
Size: 1084 Color: 206
Size: 208 Color: 79

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 2930 Color: 273
Size: 2442 Color: 268
Size: 484 Color: 132

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 3962 Color: 307
Size: 1794 Color: 241
Size: 100 Color: 14

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5154 Color: 388
Size: 586 Color: 150
Size: 116 Color: 29

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 369
Size: 844 Color: 185
Size: 16 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 4355 Color: 323
Size: 1251 Color: 219
Size: 250 Color: 92

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5055 Color: 374
Size: 669 Color: 164
Size: 132 Color: 42

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 3116 Color: 284
Size: 2284 Color: 258
Size: 456 Color: 125

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 4922 Color: 360
Size: 782 Color: 180
Size: 152 Color: 58

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5003 Color: 370
Size: 711 Color: 169
Size: 142 Color: 48

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 5061 Color: 376
Size: 663 Color: 162
Size: 132 Color: 41

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5060 Color: 375
Size: 668 Color: 163
Size: 128 Color: 38

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 3678 Color: 298
Size: 1818 Color: 243
Size: 360 Color: 113

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 4616 Color: 337
Size: 1048 Color: 202
Size: 192 Color: 72

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5266 Color: 402
Size: 494 Color: 136
Size: 96 Color: 8

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 4885 Color: 357
Size: 811 Color: 183
Size: 160 Color: 59

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 3672 Color: 297
Size: 1832 Color: 247
Size: 352 Color: 112

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 3663 Color: 294
Size: 1829 Color: 245
Size: 364 Color: 115

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4959 Color: 364
Size: 761 Color: 178
Size: 136 Color: 45

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 332
Size: 1204 Color: 214
Size: 120 Color: 34

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 4716 Color: 347
Size: 972 Color: 195
Size: 168 Color: 64

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 2940 Color: 279
Size: 2636 Color: 270
Size: 280 Color: 102

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 359
Size: 792 Color: 181
Size: 144 Color: 52

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 3892 Color: 301
Size: 1644 Color: 240
Size: 320 Color: 111

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 372
Size: 692 Color: 167
Size: 136 Color: 44

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 4692 Color: 345
Size: 956 Color: 193
Size: 208 Color: 80

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 5062 Color: 377
Size: 662 Color: 161
Size: 132 Color: 43

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5160 Color: 389
Size: 584 Color: 149
Size: 112 Color: 26

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 4472 Color: 328
Size: 1160 Color: 210
Size: 224 Color: 85

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 3724 Color: 299
Size: 2100 Color: 252
Size: 32 Color: 5

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 5048 Color: 373
Size: 680 Color: 166
Size: 128 Color: 37

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 3324 Color: 286
Size: 2116 Color: 256
Size: 416 Color: 121

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 4681 Color: 341
Size: 981 Color: 198
Size: 194 Color: 75

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 5137 Color: 385
Size: 601 Color: 152
Size: 118 Color: 31

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5007 Color: 371
Size: 709 Color: 168
Size: 140 Color: 47

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5192 Color: 394
Size: 568 Color: 146
Size: 96 Color: 7

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 5108 Color: 381
Size: 628 Color: 158
Size: 120 Color: 33

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 4161 Color: 312
Size: 1413 Color: 230
Size: 282 Color: 103

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 2938 Color: 278
Size: 2434 Color: 264
Size: 484 Color: 131

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4446 Color: 326
Size: 1178 Color: 211
Size: 232 Color: 88

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5106 Color: 380
Size: 626 Color: 157
Size: 124 Color: 36

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 392
Size: 561 Color: 144
Size: 112 Color: 23

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 4590 Color: 336
Size: 1058 Color: 203
Size: 208 Color: 78

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 4346 Color: 321
Size: 1262 Color: 221
Size: 248 Color: 91

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 3772 Color: 300
Size: 1812 Color: 242
Size: 272 Color: 99

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 3670 Color: 296
Size: 1822 Color: 244
Size: 364 Color: 114

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 4513 Color: 330
Size: 1121 Color: 208
Size: 222 Color: 84

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 2929 Color: 272
Size: 2441 Color: 267
Size: 486 Color: 133

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 4684 Color: 342
Size: 1060 Color: 204
Size: 112 Color: 22

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 3954 Color: 306
Size: 1586 Color: 234
Size: 316 Color: 108

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 3944 Color: 304
Size: 1608 Color: 239
Size: 304 Color: 105

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 3331 Color: 287
Size: 2105 Color: 255
Size: 420 Color: 122

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 4685 Color: 343
Size: 977 Color: 197
Size: 194 Color: 73

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 4881 Color: 356
Size: 813 Color: 184
Size: 162 Color: 62

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 3333 Color: 288
Size: 2103 Color: 253
Size: 420 Color: 123

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 4728 Color: 348
Size: 1032 Color: 201
Size: 96 Color: 13

Total size: 772992
Total free space: 0

