Capicity Bin: 1001
Lower Bound: 893

Bins used: 893
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 13
Size: 299 Color: 3
Size: 294 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 15
Size: 409 Color: 4
Size: 144 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 17
Size: 408 Color: 3
Size: 121 Color: 16

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 16
Size: 482 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 7
Size: 295 Color: 18
Size: 181 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 556 Color: 17
Size: 336 Color: 8
Size: 109 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 599 Color: 15
Size: 236 Color: 7
Size: 166 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 727 Color: 19
Size: 151 Color: 13
Size: 123 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 11
Size: 133 Color: 12
Size: 107 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 770 Color: 4
Size: 131 Color: 15
Size: 100 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 7
Size: 284 Color: 13
Size: 144 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 782 Color: 7
Size: 118 Color: 12
Size: 101 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 751 Color: 8
Size: 149 Color: 18
Size: 101 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 0
Size: 283 Color: 2
Size: 183 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 19
Size: 270 Color: 3
Size: 196 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 3
Size: 175 Color: 5
Size: 148 Color: 10

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 19
Size: 157 Color: 12
Size: 112 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 11
Size: 324 Color: 18
Size: 324 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 9
Size: 413 Color: 16
Size: 150 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 778 Color: 15
Size: 119 Color: 15
Size: 104 Color: 10

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 6
Size: 242 Color: 2
Size: 149 Color: 19

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 19
Size: 204 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 537 Color: 19
Size: 283 Color: 13
Size: 181 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 1
Size: 115 Color: 11
Size: 115 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 1
Size: 251 Color: 16
Size: 161 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 599 Color: 5
Size: 224 Color: 1
Size: 178 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 7
Size: 196 Color: 9
Size: 195 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 5
Size: 253 Color: 18
Size: 151 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 18
Size: 259 Color: 10
Size: 132 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 19
Size: 174 Color: 4
Size: 135 Color: 15

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 14
Size: 130 Color: 9
Size: 100 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 1
Size: 263 Color: 3
Size: 170 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 9
Size: 258 Color: 12
Size: 133 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9
Size: 379 Color: 1
Size: 138 Color: 7

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 7
Size: 358 Color: 7
Size: 195 Color: 17

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 7
Size: 384 Color: 15
Size: 131 Color: 11

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 18
Size: 477 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 16
Size: 331 Color: 0
Size: 314 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 11
Size: 394 Color: 10
Size: 173 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 0
Size: 314 Color: 10
Size: 163 Color: 5

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 13
Size: 482 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 13
Size: 128 Color: 9
Size: 102 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 554 Color: 6
Size: 316 Color: 6
Size: 131 Color: 5

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 11
Size: 476 Color: 9

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 770 Color: 13
Size: 120 Color: 19
Size: 111 Color: 16

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 18
Size: 307 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 15
Size: 366 Color: 3
Size: 148 Color: 12

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 7
Size: 497 Color: 5

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 7
Size: 292 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 5
Size: 166 Color: 4
Size: 132 Color: 19

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 13
Size: 355 Color: 4
Size: 160 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 1
Size: 131 Color: 5
Size: 109 Color: 1

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 16
Size: 323 Color: 13

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 12
Size: 175 Color: 5
Size: 123 Color: 16

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 10
Size: 465 Color: 17

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 536 Color: 3
Size: 311 Color: 11
Size: 154 Color: 19

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 12
Size: 335 Color: 13
Size: 179 Color: 8

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 8
Size: 292 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 10
Size: 126 Color: 4
Size: 104 Color: 2

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 0
Size: 227 Color: 16

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 4
Size: 228 Color: 3

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 14
Size: 399 Color: 13

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 2
Size: 259 Color: 6

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 14
Size: 456 Color: 13

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 12
Size: 456 Color: 6

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 16
Size: 437 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 636 Color: 5
Size: 210 Color: 5
Size: 155 Color: 12

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 8
Size: 210 Color: 10

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 10
Size: 333 Color: 18
Size: 210 Color: 17

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 392 Color: 9
Size: 172 Color: 16

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 545 Color: 6
Size: 246 Color: 14
Size: 210 Color: 13

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 7
Size: 437 Color: 0
Size: 106 Color: 12

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 4
Size: 392 Color: 8
Size: 127 Color: 17

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 0
Size: 166 Color: 19
Size: 126 Color: 13

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 14
Size: 251 Color: 4
Size: 161 Color: 7

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 10
Size: 357 Color: 18
Size: 185 Color: 2

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 5
Size: 380 Color: 15

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 1
Size: 381 Color: 18

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 15
Size: 116 Color: 1
Size: 105 Color: 12

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 0
Size: 215 Color: 10

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 1
Size: 216 Color: 7

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 784 Color: 13
Size: 111 Color: 5
Size: 106 Color: 19

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 17
Size: 176 Color: 18
Size: 144 Color: 5

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 14
Size: 248 Color: 12

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 556 Color: 7
Size: 303 Color: 12
Size: 142 Color: 9

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 12
Size: 334 Color: 3
Size: 132 Color: 7

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 1
Size: 284 Color: 5
Size: 144 Color: 13

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 3
Size: 299 Color: 7

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 538 Color: 13
Size: 299 Color: 6
Size: 164 Color: 17

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 3
Size: 256 Color: 5
Size: 211 Color: 17

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 7
Size: 123 Color: 1
Size: 121 Color: 14

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 770 Color: 11
Size: 124 Color: 5
Size: 107 Color: 0

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 18
Size: 492 Color: 10

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 6
Size: 455 Color: 15

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 0
Size: 455 Color: 17

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 546 Color: 16
Size: 284 Color: 19
Size: 171 Color: 19

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 5
Size: 381 Color: 7
Size: 168 Color: 4

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 599 Color: 16
Size: 235 Color: 1
Size: 167 Color: 4

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 4
Size: 127 Color: 6
Size: 103 Color: 18

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 7
Size: 303 Color: 13

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 15
Size: 323 Color: 11

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 14
Size: 244 Color: 9

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 7
Size: 167 Color: 15
Size: 124 Color: 6

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 19
Size: 239 Color: 15

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 8
Size: 239 Color: 10

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 19
Size: 138 Color: 10
Size: 101 Color: 14

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 7
Size: 379 Color: 11
Size: 138 Color: 18

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 7
Size: 336 Color: 3
Size: 181 Color: 6

Bin 109: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 5
Size: 472 Color: 9

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 533 Color: 6
Size: 350 Color: 1
Size: 118 Color: 19

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 19
Size: 242 Color: 5
Size: 146 Color: 6

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 8
Size: 174 Color: 9
Size: 135 Color: 1

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 12
Size: 324 Color: 6

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 6
Size: 336 Color: 17
Size: 324 Color: 5

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 16
Size: 336 Color: 5
Size: 324 Color: 8

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 537 Color: 1
Size: 310 Color: 9
Size: 154 Color: 5

Bin 117: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 10
Size: 482 Color: 6

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 11
Size: 342 Color: 6
Size: 177 Color: 5

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 11
Size: 355 Color: 18
Size: 161 Color: 7

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 0
Size: 273 Color: 12

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 17
Size: 314 Color: 15
Size: 163 Color: 8

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 4
Size: 475 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 712 Color: 16
Size: 156 Color: 19
Size: 133 Color: 4

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 6
Size: 384 Color: 2
Size: 131 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 7
Size: 295 Color: 18
Size: 181 Color: 14

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 18
Size: 409 Color: 5
Size: 156 Color: 11

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 17
Size: 295 Color: 1

Bin 128: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 12
Size: 402 Color: 9

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 599 Color: 12
Size: 251 Color: 3
Size: 151 Color: 17

Bin 130: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 13
Size: 294 Color: 12

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 797 Color: 15
Size: 102 Color: 19
Size: 102 Color: 8

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 13
Size: 151 Color: 11
Size: 129 Color: 16

Bin 133: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 2
Size: 250 Color: 13

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 12
Size: 257 Color: 0

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 16
Size: 218 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 782 Color: 7
Size: 118 Color: 17
Size: 101 Color: 0

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 2
Size: 448 Color: 17

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 11
Size: 223 Color: 5

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 797 Color: 15
Size: 104 Color: 1
Size: 100 Color: 18

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 16
Size: 241 Color: 10

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 0
Size: 241 Color: 5

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 10
Size: 358 Color: 2
Size: 134 Color: 10

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 5
Size: 186 Color: 1
Size: 121 Color: 17

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 536 Color: 4
Size: 343 Color: 16
Size: 122 Color: 14

Bin 145: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 8
Size: 480 Color: 9

Bin 146: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 8
Size: 480 Color: 7

Bin 147: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 17
Size: 350 Color: 2

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 521 Color: 11
Size: 350 Color: 16
Size: 130 Color: 1

Bin 149: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 7
Size: 353 Color: 12

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 7
Size: 251 Color: 7
Size: 177 Color: 11

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 6
Size: 377 Color: 18
Size: 167 Color: 8

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 14
Size: 376 Color: 10
Size: 246 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 2
Size: 191 Color: 0
Size: 129 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 355 Color: 13
Size: 160 Color: 2

Bin 155: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 15
Size: 347 Color: 11

Bin 156: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 6
Size: 234 Color: 11

Bin 157: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 19
Size: 347 Color: 17

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 584 Color: 0
Size: 234 Color: 6
Size: 183 Color: 9

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 582 Color: 19
Size: 256 Color: 0
Size: 163 Color: 19

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 5
Size: 184 Color: 19
Size: 163 Color: 13

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 1
Size: 164 Color: 12
Size: 145 Color: 12

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 603 Color: 5
Size: 234 Color: 0
Size: 164 Color: 8

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 582 Color: 10
Size: 234 Color: 11
Size: 185 Color: 2

Bin 164: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 13
Size: 349 Color: 4

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 652 Color: 17
Size: 192 Color: 19
Size: 157 Color: 6

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 14
Size: 245 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 652 Color: 12
Size: 192 Color: 13
Size: 157 Color: 9

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 629 Color: 18
Size: 245 Color: 8
Size: 127 Color: 3

Bin 169: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 18
Size: 274 Color: 10

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 727 Color: 18
Size: 154 Color: 7
Size: 120 Color: 7

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 383 Color: 7
Size: 136 Color: 17

Bin 172: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 16
Size: 349 Color: 13

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 0
Size: 162 Color: 3
Size: 157 Color: 19

Bin 174: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 17
Size: 413 Color: 19

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 12
Size: 413 Color: 6
Size: 131 Color: 5

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 531 Color: 13
Size: 342 Color: 18
Size: 128 Color: 7

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 9
Size: 258 Color: 2
Size: 133 Color: 17

Bin 178: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 17
Size: 370 Color: 5

Bin 179: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 11
Size: 370 Color: 16

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 14
Size: 370 Color: 12
Size: 150 Color: 3

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 554 Color: 4
Size: 264 Color: 17
Size: 183 Color: 11

Bin 182: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 19
Size: 235 Color: 0

Bin 183: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 16
Size: 258 Color: 0

Bin 184: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 11
Size: 376 Color: 12

Bin 185: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 0

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 10
Size: 236 Color: 1

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 12
Size: 236 Color: 6

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 11
Size: 242 Color: 6

Bin 189: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 3
Size: 240 Color: 14

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 384 Color: 2
Size: 131 Color: 7

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 13
Size: 211 Color: 5
Size: 164 Color: 7

Bin 192: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 10
Size: 376 Color: 9

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 7
Size: 376 Color: 13
Size: 177 Color: 2

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 14
Size: 408 Color: 3
Size: 108 Color: 6

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 13
Size: 408 Color: 14
Size: 152 Color: 7

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 381 Color: 6
Size: 197 Color: 19

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 9
Size: 120 Color: 11
Size: 110 Color: 8

Bin 198: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 12
Size: 261 Color: 17

Bin 199: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 7
Size: 260 Color: 12

Bin 200: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 7
Size: 260 Color: 13

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 10
Size: 260 Color: 15
Size: 144 Color: 8

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 601 Color: 14
Size: 260 Color: 13
Size: 140 Color: 17

Bin 203: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 16
Size: 317 Color: 14

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 11
Size: 317 Color: 14

Bin 205: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 12
Size: 407 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 10
Size: 108 Color: 5
Size: 106 Color: 7

Bin 207: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 3
Size: 342 Color: 12

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 531 Color: 10
Size: 303 Color: 14
Size: 167 Color: 19

Bin 209: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 7
Size: 278 Color: 8

Bin 210: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 15
Size: 487 Color: 8

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 19
Size: 335 Color: 5
Size: 179 Color: 2

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 16
Size: 214 Color: 17

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 2
Size: 384 Color: 4

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 14
Size: 464 Color: 15

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 12
Size: 322 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 6
Size: 175 Color: 5
Size: 147 Color: 7

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 638 Color: 0
Size: 188 Color: 4
Size: 175 Color: 12

Bin 218: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 17
Size: 298 Color: 2

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 4
Size: 429 Color: 7

Bin 220: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 18
Size: 249 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 572 Color: 18
Size: 328 Color: 0
Size: 101 Color: 14

Bin 222: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 19
Size: 400 Color: 12

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 603 Color: 4
Size: 260 Color: 2
Size: 138 Color: 17

Bin 224: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 2
Size: 469 Color: 6

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 6
Size: 481 Color: 8

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 0
Size: 370 Color: 5
Size: 150 Color: 7

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 536 Color: 19
Size: 339 Color: 1
Size: 126 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 14
Size: 311 Color: 8
Size: 155 Color: 9

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 8
Size: 334 Color: 5
Size: 132 Color: 11

Bin 230: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 3
Size: 414 Color: 18

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 7
Size: 415 Color: 15

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 13
Size: 414 Color: 3

Bin 233: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 18
Size: 225 Color: 5

Bin 234: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 12
Size: 226 Color: 11

Bin 235: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 11
Size: 225 Color: 14

Bin 236: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 10
Size: 448 Color: 19

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 14
Size: 276 Color: 19
Size: 170 Color: 18

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 10
Size: 276 Color: 9
Size: 157 Color: 14

Bin 239: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 18
Size: 385 Color: 11

Bin 240: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 5
Size: 402 Color: 16

Bin 241: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 16
Size: 230 Color: 9

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 16
Size: 367 Color: 19

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 12
Size: 311 Color: 5
Size: 193 Color: 19

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 398 Color: 4

Bin 245: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 6
Size: 398 Color: 5

Bin 246: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 16
Size: 234 Color: 2

Bin 247: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 0
Size: 313 Color: 17

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 3
Size: 159 Color: 18
Size: 154 Color: 19

Bin 249: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 12
Size: 244 Color: 13

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 13
Size: 123 Color: 7
Size: 121 Color: 19

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 10
Size: 366 Color: 17

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 2
Size: 447 Color: 16

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 328 Color: 0
Size: 176 Color: 6

Bin 254: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 14
Size: 234 Color: 17

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 15
Size: 283 Color: 17

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 7
Size: 466 Color: 5

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 6
Size: 309 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 18
Size: 174 Color: 0
Size: 135 Color: 10

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 15
Size: 377 Color: 0

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 3
Size: 457 Color: 19

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 392 Color: 10
Size: 196 Color: 12

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 8
Size: 175 Color: 4
Size: 148 Color: 18

Bin 263: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 13
Size: 445 Color: 5

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 3
Size: 219 Color: 15

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 17
Size: 375 Color: 4

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 12
Size: 191 Color: 3
Size: 184 Color: 2

Bin 267: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 17
Size: 492 Color: 5

Bin 268: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 12
Size: 222 Color: 13

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 18
Size: 118 Color: 12
Size: 104 Color: 9

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 18
Size: 392 Color: 13

Bin 271: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 5
Size: 392 Color: 1

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 19
Size: 434 Color: 13

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 11
Size: 394 Color: 0

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 12
Size: 356 Color: 7

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 409 Color: 3
Size: 158 Color: 14

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 15
Size: 394 Color: 18
Size: 173 Color: 2

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 12
Size: 251 Color: 17
Size: 161 Color: 7

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 3
Size: 331 Color: 19
Size: 173 Color: 9

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 6
Size: 365 Color: 7

Bin 280: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 0
Size: 347 Color: 15

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 17
Size: 423 Color: 13
Size: 137 Color: 18

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 2
Size: 225 Color: 1

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 7
Size: 325 Color: 15
Size: 189 Color: 14

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 5
Size: 428 Color: 1

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 3
Size: 238 Color: 13

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 19
Size: 407 Color: 8

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 19
Size: 198 Color: 2
Size: 190 Color: 6

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 11
Size: 201 Color: 5
Size: 187 Color: 18

Bin 289: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 2
Size: 492 Color: 10

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 508 Color: 9
Size: 311 Color: 4
Size: 182 Color: 17

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 15
Size: 454 Color: 17

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 17
Size: 454 Color: 13

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 12
Size: 293 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 8
Size: 176 Color: 19
Size: 117 Color: 13

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 12
Size: 253 Color: 13

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 11
Size: 485 Color: 9

Bin 297: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 11
Size: 322 Color: 2

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 4
Size: 355 Color: 9
Size: 175 Color: 5

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 19
Size: 268 Color: 15

Bin 300: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 10
Size: 409 Color: 0

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 2
Size: 310 Color: 6

Bin 302: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 7
Size: 325 Color: 17

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 6
Size: 325 Color: 4

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 508 Color: 6
Size: 379 Color: 15
Size: 114 Color: 14

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 508 Color: 1
Size: 325 Color: 18
Size: 168 Color: 17

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 508 Color: 2
Size: 325 Color: 5
Size: 168 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 18
Size: 366 Color: 10
Size: 148 Color: 3

Bin 308: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 6
Size: 303 Color: 1

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 2
Size: 447 Color: 19

Bin 310: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 14
Size: 438 Color: 17

Bin 311: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 7
Size: 438 Color: 18

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 2
Size: 438 Color: 6

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 15
Size: 438 Color: 16

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 395 Color: 2

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 5
Size: 488 Color: 10

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 16
Size: 395 Color: 5
Size: 118 Color: 5

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 2
Size: 162 Color: 10
Size: 158 Color: 3

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 10
Size: 340 Color: 8

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 7
Size: 339 Color: 8

Bin 320: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 3
Size: 340 Color: 18

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 536 Color: 7
Size: 339 Color: 11
Size: 126 Color: 7

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 3
Size: 336 Color: 9

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 3
Size: 382 Color: 18

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 12
Size: 381 Color: 0
Size: 197 Color: 0

Bin 325: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 15
Size: 415 Color: 16

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 8
Size: 404 Color: 0

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 5
Size: 446 Color: 8

Bin 328: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 15
Size: 415 Color: 18

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 9
Size: 195 Color: 5
Size: 153 Color: 10

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 16
Size: 252 Color: 4
Size: 194 Color: 6

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 15
Size: 283 Color: 12
Size: 183 Color: 9

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 584 Color: 1
Size: 295 Color: 5
Size: 122 Color: 17

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 10
Size: 433 Color: 19

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 2
Size: 263 Color: 13
Size: 125 Color: 10

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 11
Size: 143 Color: 4
Size: 126 Color: 11

Bin 336: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 13
Size: 436 Color: 14

Bin 337: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 6
Size: 423 Color: 7

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 12
Size: 159 Color: 10
Size: 148 Color: 10

Bin 339: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 6
Size: 448 Color: 2

Bin 340: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 3
Size: 358 Color: 2

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 13
Size: 321 Color: 8

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 8
Size: 367 Color: 4
Size: 147 Color: 16

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 4
Size: 234 Color: 7
Size: 165 Color: 15

Bin 344: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 8
Size: 299 Color: 14

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 4
Size: 367 Color: 13
Size: 147 Color: 15

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 19
Size: 227 Color: 11

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 13
Size: 277 Color: 12

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 12
Size: 338 Color: 7

Bin 349: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 4
Size: 307 Color: 7

Bin 350: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 13
Size: 307 Color: 17

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 8
Size: 379 Color: 6

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 2
Size: 253 Color: 18
Size: 180 Color: 19

Bin 353: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 19
Size: 253 Color: 8

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 5
Size: 355 Color: 8
Size: 253 Color: 8

Bin 355: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 4
Size: 323 Color: 15

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 16
Size: 112 Color: 12
Size: 102 Color: 8

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 0
Size: 320 Color: 1

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 11
Size: 363 Color: 15

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 17
Size: 497 Color: 1

Bin 360: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 5
Size: 467 Color: 12

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 17
Size: 259 Color: 2

Bin 362: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 10
Size: 319 Color: 15

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 5
Size: 200 Color: 0

Bin 364: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 14
Size: 423 Color: 16

Bin 365: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 3
Size: 259 Color: 4

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 9
Size: 387 Color: 1

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 1
Size: 468 Color: 6

Bin 368: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 17
Size: 350 Color: 5

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 533 Color: 5
Size: 350 Color: 12
Size: 118 Color: 18

Bin 370: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 4
Size: 307 Color: 8

Bin 371: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 5
Size: 374 Color: 8

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 14
Size: 149 Color: 4
Size: 143 Color: 2

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 13
Size: 349 Color: 11

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 7
Size: 214 Color: 10

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 1
Size: 152 Color: 18
Size: 117 Color: 4

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 508 Color: 1
Size: 325 Color: 2
Size: 168 Color: 9

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 505 Color: 19
Size: 344 Color: 19
Size: 152 Color: 11

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 0
Size: 380 Color: 16
Size: 181 Color: 3

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9
Size: 326 Color: 10
Size: 187 Color: 7

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 13
Size: 129 Color: 16
Size: 110 Color: 8

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 10
Size: 206 Color: 15

Bin 382: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 210 Color: 15

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 770 Color: 12
Size: 131 Color: 18
Size: 100 Color: 16

Bin 384: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 3
Size: 266 Color: 15

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 19
Size: 489 Color: 13

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 14
Size: 381 Color: 4

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 15
Size: 205 Color: 16

Bin 388: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 2
Size: 205 Color: 8

Bin 389: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 13

Bin 390: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 5
Size: 496 Color: 19

Bin 391: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 13
Size: 496 Color: 6

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 8
Size: 489 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 727 Color: 1
Size: 144 Color: 9
Size: 130 Color: 1

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 15
Size: 294 Color: 18
Size: 105 Color: 12

Bin 395: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 14
Size: 223 Color: 3

Bin 396: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 13
Size: 266 Color: 9

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 12
Size: 210 Color: 0

Bin 398: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 14
Size: 245 Color: 18

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 13
Size: 236 Color: 3

Bin 400: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 5
Size: 499 Color: 13

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 17
Size: 217 Color: 15

Bin 402: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 2
Size: 210 Color: 19

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 8
Size: 212 Color: 0

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 5
Size: 212 Color: 19

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 18
Size: 333 Color: 18
Size: 141 Color: 3

Bin 406: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 14
Size: 474 Color: 13

Bin 407: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 6
Size: 212 Color: 4

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 8
Size: 484 Color: 16

Bin 409: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 15
Size: 440 Color: 16

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 12
Size: 252 Color: 8

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 18
Size: 253 Color: 6

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 6
Size: 212 Color: 2

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9
Size: 420 Color: 8
Size: 131 Color: 12

Bin 414: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 8
Size: 224 Color: 15

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 770 Color: 10
Size: 124 Color: 4
Size: 107 Color: 18

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 17
Size: 283 Color: 19

Bin 417: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 18
Size: 318 Color: 9

Bin 418: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 3

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 9
Size: 424 Color: 5

Bin 420: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 4
Size: 397 Color: 10

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 16
Size: 247 Color: 12

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 10
Size: 445 Color: 17

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 10
Size: 395 Color: 0

Bin 424: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 12
Size: 292 Color: 10

Bin 425: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 13
Size: 437 Color: 17

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 17
Size: 499 Color: 7

Bin 427: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 10
Size: 488 Color: 14

Bin 428: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 11
Size: 263 Color: 14

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 8
Size: 250 Color: 7

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 5
Size: 331 Color: 1

Bin 431: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 4
Size: 225 Color: 0

Bin 432: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 6
Size: 233 Color: 12

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 0
Size: 397 Color: 8

Bin 434: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 4
Size: 213 Color: 17

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 9
Size: 358 Color: 6

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 7
Size: 440 Color: 6

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 8
Size: 232 Color: 6

Bin 438: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 13
Size: 365 Color: 11

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 11
Size: 256 Color: 8

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 5
Size: 466 Color: 14

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 3
Size: 420 Color: 4

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 6
Size: 151 Color: 8
Size: 129 Color: 2

Bin 443: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 15
Size: 250 Color: 1

Bin 444: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 12
Size: 473 Color: 7

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 16
Size: 282 Color: 18

Bin 446: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 18
Size: 207 Color: 17

Bin 447: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 2
Size: 432 Color: 9

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 13
Size: 464 Color: 2

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 7
Size: 491 Color: 1

Bin 450: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 17
Size: 322 Color: 11

Bin 451: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 17
Size: 267 Color: 3

Bin 452: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 11
Size: 267 Color: 4

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 7
Size: 271 Color: 5

Bin 454: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 8
Size: 365 Color: 3

Bin 455: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 10
Size: 359 Color: 8

Bin 456: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 14
Size: 430 Color: 16

Bin 457: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 1
Size: 476 Color: 7

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 18
Size: 341 Color: 1

Bin 459: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 10
Size: 267 Color: 9

Bin 460: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 11
Size: 287 Color: 12

Bin 461: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 8
Size: 346 Color: 2

Bin 462: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 3
Size: 432 Color: 14

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 8
Size: 316 Color: 9

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 0
Size: 301 Color: 6

Bin 465: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 17
Size: 302 Color: 8

Bin 466: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 9
Size: 494 Color: 8

Bin 467: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 15
Size: 358 Color: 5

Bin 468: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 0
Size: 255 Color: 7

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 2
Size: 255 Color: 1

Bin 470: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 2
Size: 275 Color: 1

Bin 471: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 6
Size: 425 Color: 1

Bin 472: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 2
Size: 420 Color: 3

Bin 473: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 12
Size: 289 Color: 11

Bin 474: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 5
Size: 330 Color: 11

Bin 475: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 9
Size: 481 Color: 6

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 1
Size: 262 Color: 14

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 11
Size: 371 Color: 15

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 8
Size: 450 Color: 1

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 14
Size: 220 Color: 10

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 15
Size: 263 Color: 7

Bin 481: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 19
Size: 213 Color: 8

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 19
Size: 346 Color: 11

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 15
Size: 343 Color: 12

Bin 484: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 7
Size: 470 Color: 13

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 0
Size: 247 Color: 9

Bin 486: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 8
Size: 278 Color: 15

Bin 487: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 0
Size: 312 Color: 8

Bin 488: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 13
Size: 220 Color: 7

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 1
Size: 260 Color: 15

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 13
Size: 218 Color: 5

Bin 491: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 18
Size: 257 Color: 6

Bin 492: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 8
Size: 449 Color: 7

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 5
Size: 288 Color: 13

Bin 494: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 8
Size: 288 Color: 7

Bin 495: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 18
Size: 484 Color: 7

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 17
Size: 291 Color: 9

Bin 497: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 7

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 13
Size: 424 Color: 7

Bin 499: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 11
Size: 429 Color: 17

Bin 500: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 6

Bin 501: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 3
Size: 393 Color: 8

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 5
Size: 458 Color: 3

Bin 503: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 5
Size: 297 Color: 2

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 3
Size: 297 Color: 0

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 275 Color: 2

Bin 506: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 11
Size: 213 Color: 4

Bin 507: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 17
Size: 397 Color: 2

Bin 508: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 16
Size: 353 Color: 19

Bin 509: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 8
Size: 351 Color: 10

Bin 510: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 10
Size: 374 Color: 12

Bin 511: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 14
Size: 493 Color: 6

Bin 512: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 10
Size: 431 Color: 19

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 17
Size: 466 Color: 7

Bin 514: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 7
Size: 232 Color: 17

Bin 515: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 4
Size: 289 Color: 16

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 0
Size: 233 Color: 7

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 14
Size: 407 Color: 7

Bin 518: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 8
Size: 385 Color: 19

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 6
Size: 343 Color: 12

Bin 520: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 1
Size: 417 Color: 16

Bin 521: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 18
Size: 311 Color: 14

Bin 522: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 14
Size: 276 Color: 0

Bin 523: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 11
Size: 216 Color: 6

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 11
Size: 469 Color: 10

Bin 525: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 6
Size: 429 Color: 9

Bin 526: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 19
Size: 386 Color: 17

Bin 527: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 13
Size: 430 Color: 18

Bin 528: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 8
Size: 312 Color: 0

Bin 529: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 5
Size: 243 Color: 8

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 2
Size: 243 Color: 14

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 11
Size: 359 Color: 10

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 9
Size: 318 Color: 1

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 4
Size: 318 Color: 18

Bin 534: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 11
Size: 245 Color: 15

Bin 535: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 5
Size: 329 Color: 11

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 3
Size: 260 Color: 8

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 10
Size: 346 Color: 5

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 10
Size: 266 Color: 13

Bin 539: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 11
Size: 383 Color: 18

Bin 540: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 7
Size: 416 Color: 1

Bin 541: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 1
Size: 315 Color: 3

Bin 542: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 18
Size: 450 Color: 17

Bin 543: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 11
Size: 442 Color: 1

Bin 544: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 9
Size: 390 Color: 3

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 14
Size: 390 Color: 6

Bin 546: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 10
Size: 252 Color: 5

Bin 547: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 14
Size: 322 Color: 2

Bin 548: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 10
Size: 304 Color: 8

Bin 549: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 5
Size: 294 Color: 4

Bin 550: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 0
Size: 282 Color: 9

Bin 551: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 12
Size: 376 Color: 4

Bin 552: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 10
Size: 468 Color: 4

Bin 553: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 13
Size: 418 Color: 5

Bin 554: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 6
Size: 380 Color: 3

Bin 555: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 0
Size: 285 Color: 13

Bin 556: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 4
Size: 392 Color: 19

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 2
Size: 344 Color: 19

Bin 558: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 5
Size: 280 Color: 7

Bin 559: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 19
Size: 278 Color: 6

Bin 560: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 2
Size: 306 Color: 17

Bin 561: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 4
Size: 316 Color: 2

Bin 562: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 13
Size: 443 Color: 6

Bin 563: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 16
Size: 225 Color: 11

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 16
Size: 346 Color: 2

Bin 565: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 14
Size: 275 Color: 4

Bin 566: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 3
Size: 389 Color: 12

Bin 567: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 13
Size: 463 Color: 12

Bin 568: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 5
Size: 308 Color: 18

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 0
Size: 308 Color: 12

Bin 570: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 4
Size: 372 Color: 8

Bin 571: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 17
Size: 389 Color: 18

Bin 572: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 10
Size: 301 Color: 4

Bin 573: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 13
Size: 458 Color: 6

Bin 574: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 5
Size: 464 Color: 7

Bin 575: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 7
Size: 352 Color: 2

Bin 576: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 10

Bin 577: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 2
Size: 494 Color: 6

Bin 578: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 16
Size: 326 Color: 8

Bin 579: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 4
Size: 451 Color: 7

Bin 580: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 3
Size: 308 Color: 8

Bin 581: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 5
Size: 203 Color: 4

Bin 582: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 11
Size: 491 Color: 0

Bin 583: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 1
Size: 330 Color: 14

Bin 584: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 2
Size: 462 Color: 10

Bin 585: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 10
Size: 330 Color: 12

Bin 586: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 9
Size: 359 Color: 16

Bin 587: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 0
Size: 352 Color: 12

Bin 588: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 19
Size: 378 Color: 11

Bin 589: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 4
Size: 271 Color: 8

Bin 590: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 4
Size: 370 Color: 7

Bin 591: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 14
Size: 473 Color: 12

Bin 592: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 7
Size: 470 Color: 4

Bin 593: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 17
Size: 475 Color: 12

Bin 594: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 5
Size: 407 Color: 9

Bin 595: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 13
Size: 216 Color: 17

Bin 596: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 2
Size: 382 Color: 16

Bin 597: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 19
Size: 483 Color: 3

Bin 598: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 8
Size: 483 Color: 4

Bin 599: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 3
Size: 308 Color: 1

Bin 600: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 5
Size: 302 Color: 17

Bin 601: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 19
Size: 257 Color: 3

Bin 602: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 6
Size: 271 Color: 18

Bin 603: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 8
Size: 252 Color: 5

Bin 604: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 14
Size: 385 Color: 9

Bin 605: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 16
Size: 410 Color: 18

Bin 606: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 12
Size: 433 Color: 0

Bin 607: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 10
Size: 315 Color: 15

Bin 608: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 12
Size: 277 Color: 15

Bin 609: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 19
Size: 356 Color: 18

Bin 610: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 14
Size: 479 Color: 18

Bin 611: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 6
Size: 433 Color: 7

Bin 612: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 4
Size: 232 Color: 0

Bin 613: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 19
Size: 456 Color: 14

Bin 614: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 16
Size: 302 Color: 1

Bin 615: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 17
Size: 449 Color: 15

Bin 616: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 17
Size: 500 Color: 13

Bin 617: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 15
Size: 332 Color: 11

Bin 618: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 16
Size: 332 Color: 9

Bin 619: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 15
Size: 405 Color: 6

Bin 620: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 11
Size: 445 Color: 13

Bin 621: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 6
Size: 401 Color: 12

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 2
Size: 401 Color: 13

Bin 623: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 19
Size: 475 Color: 1

Bin 624: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 15
Size: 439 Color: 14

Bin 625: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 16
Size: 439 Color: 5

Bin 626: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 18
Size: 330 Color: 9

Bin 627: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 0
Size: 278 Color: 15

Bin 628: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 14
Size: 208 Color: 3

Bin 629: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 18
Size: 208 Color: 16

Bin 630: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 0
Size: 416 Color: 10

Bin 631: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 7
Size: 431 Color: 10

Bin 632: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 18
Size: 478 Color: 1

Bin 633: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 5
Size: 478 Color: 6

Bin 634: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 12
Size: 332 Color: 15

Bin 635: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 261 Color: 16

Bin 636: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 6
Size: 405 Color: 17

Bin 637: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 14
Size: 266 Color: 6

Bin 638: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 10
Size: 432 Color: 9

Bin 639: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 10
Size: 289 Color: 18

Bin 640: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 6
Size: 444 Color: 15

Bin 641: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 5
Size: 208 Color: 13

Bin 642: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 18
Size: 481 Color: 14

Bin 643: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 13
Size: 420 Color: 8

Bin 644: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 17
Size: 471 Color: 2

Bin 645: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 16
Size: 470 Color: 15

Bin 646: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 8
Size: 410 Color: 11

Bin 647: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 7
Size: 264 Color: 11

Bin 648: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 19
Size: 449 Color: 0

Bin 649: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 14
Size: 449 Color: 2

Bin 650: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 5
Size: 352 Color: 4

Bin 651: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 296 Color: 14

Bin 652: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 10
Size: 468 Color: 17

Bin 653: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 3
Size: 320 Color: 14

Bin 654: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 6
Size: 364 Color: 9

Bin 655: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 3
Size: 415 Color: 4

Bin 656: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 14
Size: 353 Color: 8

Bin 657: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 442 Color: 15

Bin 658: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 6
Size: 386 Color: 11

Bin 659: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 9
Size: 498 Color: 2

Bin 660: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 16
Size: 499 Color: 10

Bin 661: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 0
Size: 400 Color: 14

Bin 662: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 1
Size: 457 Color: 14

Bin 663: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 11
Size: 495 Color: 18

Bin 664: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 6
Size: 489 Color: 12

Bin 665: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 5
Size: 493 Color: 1

Bin 666: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 14
Size: 483 Color: 12

Bin 667: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 19
Size: 479 Color: 14

Bin 668: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 6
Size: 479 Color: 19

Bin 669: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 11
Size: 467 Color: 1

Bin 670: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 12
Size: 464 Color: 8

Bin 671: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 13
Size: 463 Color: 7

Bin 672: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 460 Color: 18

Bin 673: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 11
Size: 459 Color: 19

Bin 674: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 15
Size: 452 Color: 6

Bin 675: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 9
Size: 435 Color: 12

Bin 676: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 5
Size: 432 Color: 6

Bin 677: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 8
Size: 431 Color: 16

Bin 678: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 11
Size: 427 Color: 8

Bin 679: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 13
Size: 427 Color: 9

Bin 680: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 13
Size: 426 Color: 5

Bin 681: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 14
Size: 422 Color: 18

Bin 682: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 3
Size: 421 Color: 5

Bin 683: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 9
Size: 421 Color: 19

Bin 684: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 11
Size: 419 Color: 5

Bin 685: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 5
Size: 419 Color: 14

Bin 686: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 2
Size: 411 Color: 11

Bin 687: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 7
Size: 411 Color: 13

Bin 688: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 7
Size: 410 Color: 12

Bin 689: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 9
Size: 404 Color: 2

Bin 690: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 2
Size: 401 Color: 18

Bin 691: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 8
Size: 396 Color: 16

Bin 692: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 17
Size: 394 Color: 10

Bin 693: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 393 Color: 7

Bin 694: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 19
Size: 385 Color: 8

Bin 695: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 19
Size: 382 Color: 5

Bin 696: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 12
Size: 378 Color: 6

Bin 697: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 15

Bin 698: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 16
Size: 364 Color: 0

Bin 699: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 17
Size: 364 Color: 13

Bin 700: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 11
Size: 364 Color: 9

Bin 701: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 10
Size: 363 Color: 6

Bin 702: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 2
Size: 362 Color: 9

Bin 703: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 10

Bin 704: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 13
Size: 361 Color: 11

Bin 705: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 9
Size: 361 Color: 12

Bin 706: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 12
Size: 361 Color: 17

Bin 707: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 17
Size: 360 Color: 1

Bin 708: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 4
Size: 360 Color: 11

Bin 709: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 17
Size: 358 Color: 18

Bin 710: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 8
Size: 357 Color: 18

Bin 711: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 1
Size: 357 Color: 9

Bin 712: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 4
Size: 354 Color: 2

Bin 713: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 13
Size: 351 Color: 10

Bin 714: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 9
Size: 351 Color: 4

Bin 715: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 13
Size: 348 Color: 10

Bin 716: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 19
Size: 344 Color: 4

Bin 717: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 4
Size: 337 Color: 17

Bin 718: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 16
Size: 327 Color: 17

Bin 719: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 0
Size: 321 Color: 10

Bin 720: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 3
Size: 319 Color: 11

Bin 721: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 0
Size: 313 Color: 10

Bin 722: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 12
Size: 306 Color: 14

Bin 723: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 15
Size: 300 Color: 9

Bin 724: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 296 Color: 6

Bin 725: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 6
Size: 296 Color: 4

Bin 726: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 15
Size: 293 Color: 18

Bin 727: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 10
Size: 292 Color: 14

Bin 728: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 14
Size: 290 Color: 5

Bin 729: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 1
Size: 285 Color: 11

Bin 730: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 19
Size: 285 Color: 3

Bin 731: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 13
Size: 284 Color: 12

Bin 732: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 15
Size: 282 Color: 2

Bin 733: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 15
Size: 282 Color: 9

Bin 734: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 1
Size: 281 Color: 12

Bin 735: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 15
Size: 281 Color: 9

Bin 736: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 12
Size: 269 Color: 18

Bin 737: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 9
Size: 266 Color: 2

Bin 738: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 9
Size: 254 Color: 19

Bin 739: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 2
Size: 254 Color: 4

Bin 740: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 0
Size: 238 Color: 6

Bin 741: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 13
Size: 238 Color: 15

Bin 742: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 4
Size: 235 Color: 14

Bin 743: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 3
Size: 233 Color: 8

Bin 744: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 1
Size: 229 Color: 14

Bin 745: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 7
Size: 229 Color: 4

Bin 746: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 19
Size: 221 Color: 5

Bin 747: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 18
Size: 218 Color: 15

Bin 748: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 15
Size: 217 Color: 9

Bin 749: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 217 Color: 7

Bin 750: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 6
Size: 211 Color: 1

Bin 751: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 12
Size: 209 Color: 3

Bin 752: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 6
Size: 209 Color: 14

Bin 753: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 17
Size: 209 Color: 4

Bin 754: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 8
Size: 209 Color: 10

Bin 755: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 12
Size: 208 Color: 18

Bin 756: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 10
Size: 206 Color: 15

Bin 757: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 10
Size: 203 Color: 7

Bin 758: 1 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 17
Size: 283 Color: 0
Size: 115 Color: 2

Bin 759: 1 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 11
Size: 196 Color: 15
Size: 101 Color: 0

Bin 760: 1 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 6
Size: 295 Color: 3

Bin 761: 1 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 4
Size: 227 Color: 2

Bin 762: 1 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 8
Size: 215 Color: 16

Bin 763: 1 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 6
Size: 381 Color: 2

Bin 764: 1 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 2
Size: 273 Color: 15

Bin 765: 1 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 10
Size: 475 Color: 8

Bin 766: 1 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 256 Color: 11

Bin 767: 1 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 3
Size: 218 Color: 18

Bin 768: 1 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 11
Size: 260 Color: 12

Bin 769: 1 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 14
Size: 261 Color: 2

Bin 770: 1 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 4
Size: 249 Color: 17

Bin 771: 1 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 14
Size: 225 Color: 13

Bin 772: 1 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 5
Size: 225 Color: 16

Bin 773: 1 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 8
Size: 311 Color: 11

Bin 774: 1 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 12
Size: 246 Color: 10

Bin 775: 1 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 17
Size: 225 Color: 16

Bin 776: 1 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 18
Size: 237 Color: 13

Bin 777: 1 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 15
Size: 201 Color: 13

Bin 778: 1 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 3
Size: 201 Color: 5

Bin 779: 1 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 7
Size: 453 Color: 5

Bin 780: 1 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 12
Size: 453 Color: 19

Bin 781: 1 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 11
Size: 268 Color: 7

Bin 782: 1 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 12
Size: 339 Color: 10

Bin 783: 1 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 6
Size: 381 Color: 7

Bin 784: 1 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 8
Size: 381 Color: 14

Bin 785: 1 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 8
Size: 415 Color: 9

Bin 786: 1 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 329 Color: 0

Bin 787: 1 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 2
Size: 321 Color: 1

Bin 788: 1 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 6
Size: 415 Color: 10

Bin 789: 1 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 1
Size: 276 Color: 10

Bin 790: 1 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 12
Size: 337 Color: 4

Bin 791: 1 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 2
Size: 444 Color: 13

Bin 792: 1 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 2
Size: 423 Color: 4

Bin 793: 1 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 374 Color: 1

Bin 794: 1 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 2
Size: 270 Color: 15

Bin 795: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 14
Size: 199 Color: 5

Bin 796: 1 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 5
Size: 249 Color: 16

Bin 797: 1 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 17
Size: 286 Color: 4

Bin 798: 1 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 11
Size: 206 Color: 8

Bin 799: 1 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 1
Size: 206 Color: 3

Bin 800: 1 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 305 Color: 12

Bin 801: 1 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 6
Size: 272 Color: 17

Bin 802: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 12
Size: 199 Color: 2

Bin 803: 1 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 19
Size: 343 Color: 4

Bin 804: 1 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 12
Size: 305 Color: 14

Bin 805: 1 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 18
Size: 212 Color: 5

Bin 806: 1 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 16
Size: 206 Color: 11

Bin 807: 1 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 18
Size: 405 Color: 10

Bin 808: 1 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 12
Size: 225 Color: 14

Bin 809: 1 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 14
Size: 326 Color: 17

Bin 810: 1 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 10
Size: 461 Color: 11

Bin 811: 1 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 7
Size: 326 Color: 13

Bin 812: 1 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 7
Size: 326 Color: 17

Bin 813: 1 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 7
Size: 345 Color: 13

Bin 814: 1 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 1
Size: 473 Color: 7

Bin 815: 1 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 11
Size: 410 Color: 19

Bin 816: 1 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 3
Size: 450 Color: 9

Bin 817: 1 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 13
Size: 286 Color: 14

Bin 818: 1 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 473 Color: 9

Bin 819: 1 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 18
Size: 494 Color: 5

Bin 820: 1 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 1
Size: 326 Color: 2

Bin 821: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 13
Size: 264 Color: 1

Bin 822: 1 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 5
Size: 311 Color: 13

Bin 823: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 15
Size: 264 Color: 0

Bin 824: 1 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 19
Size: 371 Color: 16

Bin 825: 1 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 3
Size: 430 Color: 14

Bin 826: 1 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 14
Size: 386 Color: 2

Bin 827: 1 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 6
Size: 479 Color: 15

Bin 828: 1 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 5
Size: 359 Color: 7

Bin 829: 1 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 15
Size: 276 Color: 9

Bin 830: 1 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 13
Size: 212 Color: 12

Bin 831: 1 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 13
Size: 286 Color: 8

Bin 832: 1 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 3
Size: 350 Color: 16

Bin 833: 1 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 6
Size: 500 Color: 5

Bin 834: 1 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 17
Size: 473 Color: 6

Bin 835: 1 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 15
Size: 405 Color: 7

Bin 836: 1 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 10
Size: 403 Color: 17

Bin 837: 1 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 3
Size: 423 Color: 2

Bin 838: 1 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 4
Size: 494 Color: 15

Bin 839: 1 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 12
Size: 425 Color: 6

Bin 840: 1 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 0
Size: 405 Color: 15

Bin 841: 1 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 12
Size: 405 Color: 14

Bin 842: 1 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 6
Size: 395 Color: 10

Bin 843: 1 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 16
Size: 386 Color: 3

Bin 844: 1 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 4
Size: 371 Color: 16

Bin 845: 1 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 7
Size: 368 Color: 19

Bin 846: 1 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 3
Size: 368 Color: 17

Bin 847: 1 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 17
Size: 368 Color: 0

Bin 848: 1 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 12
Size: 359 Color: 19

Bin 849: 1 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 19
Size: 329 Color: 8

Bin 850: 1 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 4
Size: 254 Color: 19

Bin 851: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 2
Size: 199 Color: 3

Bin 852: 2 of cap free
Amount of items: 4
Items: 
Size: 341 Color: 12
Size: 336 Color: 7
Size: 180 Color: 2
Size: 142 Color: 0

Bin 853: 2 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 18
Size: 290 Color: 5

Bin 854: 2 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 9
Size: 246 Color: 13

Bin 855: 2 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 15
Size: 441 Color: 13

Bin 856: 2 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 6
Size: 215 Color: 14

Bin 857: 2 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 9
Size: 461 Color: 1

Bin 858: 2 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 16
Size: 461 Color: 2

Bin 859: 2 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 17
Size: 461 Color: 18

Bin 860: 2 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 9
Size: 430 Color: 16

Bin 861: 2 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 15
Size: 317 Color: 1

Bin 862: 2 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 15
Size: 278 Color: 1

Bin 863: 2 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 13
Size: 414 Color: 12

Bin 864: 2 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 7
Size: 452 Color: 17

Bin 865: 2 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 9
Size: 488 Color: 5

Bin 866: 2 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 13
Size: 389 Color: 8

Bin 867: 2 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 5
Size: 272 Color: 10

Bin 868: 2 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 15
Size: 198 Color: 9

Bin 869: 2 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 6
Size: 385 Color: 10

Bin 870: 2 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 11
Size: 430 Color: 6

Bin 871: 2 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 17
Size: 215 Color: 3

Bin 872: 2 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 8
Size: 201 Color: 18

Bin 873: 2 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 18
Size: 488 Color: 13

Bin 874: 2 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 7
Size: 441 Color: 19

Bin 875: 2 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 14
Size: 488 Color: 10

Bin 876: 3 of cap free
Amount of items: 3
Items: 
Size: 532 Color: 17
Size: 366 Color: 4
Size: 100 Color: 15

Bin 877: 3 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 2
Size: 162 Color: 15
Size: 104 Color: 1

Bin 878: 3 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 8
Size: 241 Color: 14

Bin 879: 3 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 13
Size: 409 Color: 15

Bin 880: 3 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 488 Color: 1

Bin 881: 4 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 2
Size: 227 Color: 10

Bin 882: 5 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9
Size: 303 Color: 5
Size: 195 Color: 0

Bin 883: 6 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 7
Size: 441 Color: 16

Bin 884: 8 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 8
Size: 484 Color: 15

Bin 885: 17 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 6
Size: 385 Color: 11

Bin 886: 18 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 0
Size: 251 Color: 6

Bin 887: 20 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 370 Color: 1
Size: 235 Color: 16

Bin 888: 23 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 9
Size: 196 Color: 14

Bin 889: 24 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 15
Size: 384 Color: 1
Size: 186 Color: 0

Bin 890: 26 of cap free
Amount of items: 7
Items: 
Size: 166 Color: 6
Size: 152 Color: 8
Size: 141 Color: 0
Size: 139 Color: 12
Size: 129 Color: 4
Size: 128 Color: 19
Size: 120 Color: 2

Bin 891: 30 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 6
Size: 436 Color: 12

Bin 892: 47 of cap free
Amount of items: 5
Items: 
Size: 235 Color: 15
Size: 192 Color: 10
Size: 179 Color: 3
Size: 177 Color: 1
Size: 171 Color: 3

Bin 893: 53 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 6
Size: 380 Color: 18
Size: 187 Color: 2

Total size: 893455
Total free space: 438

