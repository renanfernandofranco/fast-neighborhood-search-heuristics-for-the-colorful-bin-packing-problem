Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 174
Size: 332 Color: 148
Size: 290 Color: 102

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 181
Size: 356 Color: 161
Size: 253 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 240
Size: 273 Color: 70
Size: 251 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 231
Size: 283 Color: 96
Size: 253 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 206
Size: 324 Color: 141
Size: 253 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 199
Size: 331 Color: 147
Size: 256 Color: 29

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 225
Size: 274 Color: 74
Size: 271 Color: 65

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 205
Size: 299 Color: 115
Size: 279 Color: 84

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 227
Size: 278 Color: 82
Size: 266 Color: 56

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 211
Size: 317 Color: 133
Size: 255 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 179
Size: 359 Color: 166
Size: 257 Color: 32

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 212
Size: 294 Color: 107
Size: 274 Color: 75

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 165
Size: 356 Color: 162
Size: 285 Color: 98

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 226
Size: 294 Color: 110
Size: 251 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 182
Size: 358 Color: 164
Size: 250 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 216
Size: 283 Color: 95
Size: 275 Color: 78

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 177
Size: 324 Color: 140
Size: 294 Color: 108

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 228
Size: 280 Color: 88
Size: 261 Color: 42

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 210
Size: 313 Color: 127
Size: 261 Color: 44

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 219
Size: 296 Color: 114
Size: 255 Color: 24

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 242
Size: 261 Color: 45
Size: 256 Color: 31

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 215
Size: 293 Color: 106
Size: 270 Color: 63

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 234
Size: 276 Color: 80
Size: 257 Color: 34

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 217
Size: 282 Color: 93
Size: 274 Color: 73

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 171
Size: 358 Color: 163
Size: 278 Color: 81

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 193
Size: 305 Color: 120
Size: 289 Color: 101

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 236
Size: 272 Color: 68
Size: 255 Color: 18

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 213
Size: 303 Color: 118
Size: 264 Color: 49

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 183
Size: 353 Color: 159
Size: 253 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 200
Size: 329 Color: 145
Size: 257 Color: 35

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 180
Size: 360 Color: 167
Size: 254 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 203
Size: 320 Color: 137
Size: 260 Color: 41

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 209
Size: 319 Color: 136
Size: 257 Color: 36

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 239
Size: 268 Color: 58
Size: 256 Color: 28

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 222
Size: 280 Color: 86
Size: 269 Color: 60

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 204
Size: 311 Color: 126
Size: 269 Color: 61

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 175
Size: 361 Color: 168
Size: 261 Color: 43

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 189
Size: 326 Color: 144
Size: 271 Color: 66

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 185
Size: 334 Color: 149
Size: 270 Color: 64

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 195
Size: 326 Color: 143
Size: 264 Color: 48

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 208
Size: 314 Color: 129
Size: 262 Color: 47

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 173
Size: 362 Color: 169
Size: 264 Color: 50

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 194
Size: 314 Color: 128
Size: 280 Color: 89

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 230
Size: 278 Color: 83
Size: 260 Color: 39

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 198
Size: 306 Color: 122
Size: 282 Color: 94

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 207
Size: 316 Color: 130
Size: 261 Color: 46

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 214
Size: 302 Color: 117
Size: 265 Color: 52

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 176
Size: 356 Color: 160
Size: 264 Color: 51

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 188
Size: 318 Color: 134
Size: 281 Color: 91

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 229
Size: 284 Color: 97
Size: 255 Color: 26

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 156
Size: 345 Color: 155
Size: 309 Color: 125

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 243
Size: 259 Color: 38
Size: 255 Color: 21

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 158
Size: 352 Color: 157
Size: 295 Color: 112

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 223
Size: 275 Color: 77
Size: 272 Color: 67

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 187
Size: 307 Color: 123
Size: 294 Color: 109

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 235
Size: 273 Color: 69
Size: 255 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 186
Size: 307 Color: 124
Size: 296 Color: 113

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 246
Size: 255 Color: 23
Size: 252 Color: 6

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 220
Size: 285 Color: 99
Size: 265 Color: 55

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 218
Size: 281 Color: 92
Size: 270 Color: 62

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 245
Size: 256 Color: 27
Size: 255 Color: 25

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 184
Size: 325 Color: 142
Size: 280 Color: 85

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 201
Size: 330 Color: 146
Size: 256 Color: 30

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 202
Size: 318 Color: 135
Size: 265 Color: 54

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 192
Size: 305 Color: 121
Size: 290 Color: 103

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 224
Size: 291 Color: 104
Size: 255 Color: 20

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 241
Size: 265 Color: 53
Size: 253 Color: 11

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 221
Size: 294 Color: 111
Size: 255 Color: 22

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 154
Size: 342 Color: 153
Size: 316 Color: 131

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 191
Size: 321 Color: 139
Size: 275 Color: 79

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 232
Size: 274 Color: 72
Size: 260 Color: 40

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 172
Size: 362 Color: 170
Size: 266 Color: 57

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 196
Size: 316 Color: 132
Size: 274 Color: 71

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 247
Size: 253 Color: 14
Size: 252 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 238
Size: 268 Color: 59
Size: 258 Color: 37

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 197
Size: 300 Color: 116
Size: 289 Color: 100

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 244
Size: 257 Color: 33
Size: 255 Color: 19

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 178
Size: 338 Color: 150
Size: 280 Color: 87

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 152
Size: 340 Color: 151
Size: 320 Color: 138

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 190
Size: 305 Color: 119
Size: 292 Color: 105

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 248
Size: 252 Color: 5
Size: 251 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 237
Size: 275 Color: 76
Size: 252 Color: 7

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 233
Size: 281 Color: 90
Size: 253 Color: 12

Total size: 83000
Total free space: 0

