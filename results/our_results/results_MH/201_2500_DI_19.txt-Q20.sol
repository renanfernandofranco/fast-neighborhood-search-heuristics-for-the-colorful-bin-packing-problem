Capicity Bin: 2048
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 828 Color: 18
Size: 472 Color: 6
Size: 316 Color: 2
Size: 220 Color: 2
Size: 76 Color: 16
Size: 76 Color: 3
Size: 40 Color: 16
Size: 20 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 0
Size: 202 Color: 19
Size: 36 Color: 18

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1614 Color: 1
Size: 362 Color: 16
Size: 56 Color: 17
Size: 8 Color: 19
Size: 8 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 7
Size: 313 Color: 18
Size: 62 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1386 Color: 0
Size: 554 Color: 11
Size: 108 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 11
Size: 246 Color: 9
Size: 48 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 6
Size: 314 Color: 12
Size: 60 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 2
Size: 424 Color: 0
Size: 208 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 3
Size: 559 Color: 7
Size: 110 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 15
Size: 321 Color: 17
Size: 64 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 19
Size: 562 Color: 6
Size: 112 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 15
Size: 261 Color: 14
Size: 12 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 7
Size: 391 Color: 18
Size: 78 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 3
Size: 282 Color: 5
Size: 52 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 10
Size: 491 Color: 0
Size: 96 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 10
Size: 274 Color: 12
Size: 52 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 6
Size: 258 Color: 15
Size: 48 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1026 Color: 16
Size: 854 Color: 5
Size: 168 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 0
Size: 731 Color: 3
Size: 146 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 11
Size: 493 Color: 3
Size: 98 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 14
Size: 406 Color: 1
Size: 80 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 0
Size: 317 Color: 12
Size: 62 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 4
Size: 696 Color: 6
Size: 72 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 15
Size: 190 Color: 18
Size: 36 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1321 Color: 1
Size: 607 Color: 4
Size: 120 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 14
Size: 739 Color: 18
Size: 146 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 11
Size: 202 Color: 10
Size: 40 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 6
Size: 630 Color: 5
Size: 120 Color: 14

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 12
Size: 353 Color: 4
Size: 70 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 2
Size: 398 Color: 19
Size: 76 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 10
Size: 735 Color: 6
Size: 146 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 3
Size: 330 Color: 0
Size: 64 Color: 10

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 0
Size: 194 Color: 13
Size: 36 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1285 Color: 6
Size: 637 Color: 4
Size: 126 Color: 14

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 5
Size: 388 Color: 8
Size: 72 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 8
Size: 274 Color: 15
Size: 4 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 19
Size: 682 Color: 8
Size: 132 Color: 12

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 6
Size: 494 Color: 10
Size: 96 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 14
Size: 351 Color: 15
Size: 68 Color: 10

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 1
Size: 271 Color: 4
Size: 52 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 9
Size: 442 Color: 8
Size: 84 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 5
Size: 302 Color: 6
Size: 56 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 12
Size: 395 Color: 6
Size: 78 Color: 9

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 17
Size: 321 Color: 8
Size: 62 Color: 12

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 9
Size: 389 Color: 19
Size: 76 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 12
Size: 706 Color: 3
Size: 140 Color: 12

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1029 Color: 4
Size: 851 Color: 4
Size: 168 Color: 16

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 11
Size: 477 Color: 15
Size: 44 Color: 17

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 4
Size: 802 Color: 8
Size: 160 Color: 5

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 18
Size: 522 Color: 13
Size: 100 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 8
Size: 439 Color: 2
Size: 86 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 4
Size: 709 Color: 12
Size: 140 Color: 18

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 15
Size: 414 Color: 10
Size: 52 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 19
Size: 498 Color: 0
Size: 48 Color: 14

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 9
Size: 853 Color: 14
Size: 170 Color: 12

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 14
Size: 605 Color: 6
Size: 60 Color: 18

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 16
Size: 291 Color: 2
Size: 56 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1703 Color: 11
Size: 289 Color: 10
Size: 56 Color: 5

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 6
Size: 198 Color: 9
Size: 36 Color: 6

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 9
Size: 529 Color: 11
Size: 104 Color: 12

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 6
Size: 215 Color: 5
Size: 42 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 14
Size: 231 Color: 16
Size: 46 Color: 12

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 19
Size: 235 Color: 5
Size: 46 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 12
Size: 239 Color: 13
Size: 46 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 11
Size: 211 Color: 0
Size: 42 Color: 19

Total size: 133120
Total free space: 0

