Capicity Bin: 1996
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 1
Size: 286 Color: 1
Size: 136 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 1
Size: 509 Color: 1
Size: 108 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 1
Size: 242 Color: 1
Size: 72 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 1
Size: 330 Color: 1
Size: 144 Color: 0

Bin 5: 0 of cap free
Amount of items: 5
Items: 
Size: 1346 Color: 1
Size: 390 Color: 1
Size: 152 Color: 0
Size: 60 Color: 0
Size: 48 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1525 Color: 1
Size: 351 Color: 1
Size: 120 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 1
Size: 387 Color: 1
Size: 88 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 1
Size: 190 Color: 1
Size: 36 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 1
Size: 294 Color: 1
Size: 84 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 1
Size: 322 Color: 1
Size: 116 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 1
Size: 198 Color: 1
Size: 36 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 1
Size: 142 Color: 1
Size: 76 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1198 Color: 1
Size: 676 Color: 1
Size: 122 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 1
Size: 663 Color: 1
Size: 46 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1255 Color: 1
Size: 619 Color: 1
Size: 122 Color: 0

Bin 16: 0 of cap free
Amount of items: 4
Items: 
Size: 1252 Color: 1
Size: 542 Color: 1
Size: 132 Color: 0
Size: 70 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 1
Size: 451 Color: 1
Size: 88 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 1
Size: 449 Color: 1
Size: 164 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 1
Size: 239 Color: 1
Size: 76 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 1
Size: 550 Color: 1
Size: 56 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 1
Size: 354 Color: 1
Size: 40 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1610 Color: 1
Size: 220 Color: 0
Size: 166 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 1
Size: 186 Color: 1
Size: 36 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 1
Size: 478 Color: 1
Size: 56 Color: 0

Bin 25: 0 of cap free
Amount of items: 5
Items: 
Size: 778 Color: 1
Size: 767 Color: 1
Size: 281 Color: 1
Size: 102 Color: 0
Size: 68 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 1
Size: 446 Color: 1
Size: 64 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 1
Size: 308 Color: 0
Size: 262 Color: 1

Bin 28: 0 of cap free
Amount of items: 5
Items: 
Size: 1003 Color: 1
Size: 591 Color: 1
Size: 318 Color: 1
Size: 76 Color: 0
Size: 8 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 1
Size: 189 Color: 1
Size: 56 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1695 Color: 1
Size: 251 Color: 1
Size: 50 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 1
Size: 666 Color: 1
Size: 64 Color: 0

Bin 32: 0 of cap free
Amount of items: 5
Items: 
Size: 1141 Color: 1
Size: 610 Color: 1
Size: 197 Color: 1
Size: 32 Color: 0
Size: 16 Color: 0

Bin 33: 0 of cap free
Amount of items: 5
Items: 
Size: 999 Color: 1
Size: 460 Color: 1
Size: 397 Color: 1
Size: 88 Color: 0
Size: 52 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 1
Size: 830 Color: 1
Size: 100 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 1
Size: 246 Color: 1
Size: 36 Color: 0

Bin 36: 0 of cap free
Amount of items: 5
Items: 
Size: 804 Color: 1
Size: 717 Color: 1
Size: 303 Color: 1
Size: 164 Color: 0
Size: 8 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 1
Size: 230 Color: 1
Size: 44 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 1
Size: 401 Color: 1
Size: 78 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1607 Color: 1
Size: 211 Color: 1
Size: 178 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1145 Color: 1
Size: 831 Color: 1
Size: 20 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 1
Size: 366 Color: 1
Size: 42 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 1
Size: 410 Color: 1
Size: 10 Color: 0

Bin 43: 1 of cap free
Amount of items: 4
Items: 
Size: 1002 Color: 1
Size: 829 Color: 1
Size: 100 Color: 0
Size: 64 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1259 Color: 1
Size: 698 Color: 1
Size: 38 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 1
Size: 589 Color: 1
Size: 68 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1533 Color: 1
Size: 426 Color: 1
Size: 36 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 1
Size: 220 Color: 1
Size: 142 Color: 0

Bin 48: 1 of cap free
Amount of items: 5
Items: 
Size: 1162 Color: 1
Size: 393 Color: 1
Size: 340 Color: 1
Size: 64 Color: 0
Size: 36 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 1
Size: 734 Color: 1
Size: 60 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 1387 Color: 1
Size: 506 Color: 1
Size: 102 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 1
Size: 263 Color: 1
Size: 72 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 1
Size: 325 Color: 1
Size: 52 Color: 0

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 1
Size: 416 Color: 1
Size: 118 Color: 0

Bin 54: 3 of cap free
Amount of items: 5
Items: 
Size: 1137 Color: 1
Size: 515 Color: 1
Size: 205 Color: 1
Size: 84 Color: 0
Size: 52 Color: 0

Bin 55: 5 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 1
Size: 369 Color: 1
Size: 92 Color: 0

Bin 56: 12 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 1
Size: 615 Color: 1
Size: 78 Color: 0

Bin 57: 27 of cap free
Amount of items: 3
Items: 
Size: 1380 Color: 1
Size: 511 Color: 1
Size: 78 Color: 0

Bin 58: 28 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 1
Size: 182 Color: 1
Size: 132 Color: 0

Bin 59: 57 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 1
Size: 713 Color: 1
Size: 108 Color: 0

Bin 60: 210 of cap free
Amount of items: 1
Items: 
Size: 1786 Color: 1

Bin 61: 225 of cap free
Amount of items: 1
Items: 
Size: 1771 Color: 1

Bin 62: 235 of cap free
Amount of items: 1
Items: 
Size: 1761 Color: 1

Bin 63: 253 of cap free
Amount of items: 1
Items: 
Size: 1743 Color: 1

Bin 64: 285 of cap free
Amount of items: 1
Items: 
Size: 1711 Color: 1

Bin 65: 290 of cap free
Amount of items: 1
Items: 
Size: 1706 Color: 1

Bin 66: 350 of cap free
Amount of items: 1
Items: 
Size: 1646 Color: 1

Total size: 129740
Total free space: 1996

