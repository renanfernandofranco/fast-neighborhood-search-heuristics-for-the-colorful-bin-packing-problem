Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 308 Color: 1
Size: 256 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 255 Color: 1
Size: 253 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 329 Color: 1
Size: 263 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 300 Color: 1
Size: 268 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 264 Color: 1
Size: 258 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 279 Color: 1
Size: 266 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 252 Color: 1
Size: 251 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 314 Color: 1
Size: 274 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 267 Color: 1
Size: 253 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 280 Color: 1
Size: 270 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 354 Color: 1
Size: 264 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 282 Color: 1
Size: 279 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 296 Color: 1
Size: 275 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 334 Color: 1
Size: 264 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 250 Color: 0
Size: 269 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 314 Color: 1
Size: 284 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 282 Color: 1
Size: 274 Color: 0
Size: 444 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 315 Color: 1
Size: 253 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 263 Color: 1
Size: 255 Color: 0

Total size: 20000
Total free space: 0

