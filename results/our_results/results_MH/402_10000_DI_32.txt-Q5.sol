Capicity Bin: 8064
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4034 Color: 1
Size: 1248 Color: 2
Size: 1192 Color: 1
Size: 1140 Color: 3
Size: 450 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4045 Color: 0
Size: 3351 Color: 2
Size: 668 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5182 Color: 1
Size: 2518 Color: 4
Size: 364 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 1
Size: 2156 Color: 3
Size: 358 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 1
Size: 2148 Color: 3
Size: 160 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 1
Size: 1806 Color: 0
Size: 384 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 0
Size: 1834 Color: 1
Size: 226 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 4
Size: 1736 Color: 1
Size: 144 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6314 Color: 1
Size: 1482 Color: 2
Size: 268 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6344 Color: 4
Size: 1576 Color: 1
Size: 144 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6478 Color: 1
Size: 1218 Color: 4
Size: 368 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 4
Size: 1048 Color: 1
Size: 524 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 4
Size: 1270 Color: 4
Size: 182 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6641 Color: 4
Size: 1075 Color: 1
Size: 348 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6682 Color: 2
Size: 1058 Color: 1
Size: 324 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6697 Color: 1
Size: 1055 Color: 0
Size: 312 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6714 Color: 0
Size: 694 Color: 1
Size: 656 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 1
Size: 880 Color: 2
Size: 440 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 3
Size: 844 Color: 3
Size: 448 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6799 Color: 1
Size: 985 Color: 0
Size: 280 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 1
Size: 998 Color: 0
Size: 208 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6894 Color: 1
Size: 922 Color: 0
Size: 248 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6844 Color: 2
Size: 952 Color: 1
Size: 268 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6904 Color: 4
Size: 978 Color: 2
Size: 182 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6931 Color: 4
Size: 945 Color: 2
Size: 188 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 1
Size: 588 Color: 4
Size: 508 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6958 Color: 3
Size: 592 Color: 0
Size: 514 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7050 Color: 1
Size: 846 Color: 3
Size: 168 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7060 Color: 1
Size: 714 Color: 0
Size: 290 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 1
Size: 760 Color: 2
Size: 202 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 4
Size: 764 Color: 1
Size: 210 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7128 Color: 4
Size: 664 Color: 1
Size: 272 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 1
Size: 668 Color: 4
Size: 248 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7144 Color: 2
Size: 776 Color: 1
Size: 144 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7160 Color: 1
Size: 612 Color: 2
Size: 292 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7234 Color: 2
Size: 814 Color: 3
Size: 16 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7242 Color: 0
Size: 686 Color: 1
Size: 136 Color: 3

Bin 38: 1 of cap free
Amount of items: 8
Items: 
Size: 4033 Color: 1
Size: 1078 Color: 1
Size: 896 Color: 1
Size: 668 Color: 3
Size: 520 Color: 3
Size: 512 Color: 3
Size: 212 Color: 2
Size: 144 Color: 0

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 4042 Color: 1
Size: 3357 Color: 2
Size: 664 Color: 2

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 4579 Color: 4
Size: 3148 Color: 0
Size: 336 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5336 Color: 0
Size: 2551 Color: 4
Size: 176 Color: 1

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5753 Color: 3
Size: 2118 Color: 1
Size: 192 Color: 4

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5992 Color: 2
Size: 1759 Color: 1
Size: 312 Color: 3

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6203 Color: 1
Size: 1664 Color: 0
Size: 196 Color: 4

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6323 Color: 1
Size: 1618 Color: 4
Size: 122 Color: 4

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6391 Color: 1
Size: 1002 Color: 3
Size: 670 Color: 2

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6440 Color: 3
Size: 1395 Color: 1
Size: 228 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 2
Size: 964 Color: 1
Size: 532 Color: 4

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6775 Color: 1
Size: 868 Color: 2
Size: 420 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 3
Size: 670 Color: 1
Size: 510 Color: 4

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6971 Color: 1
Size: 896 Color: 0
Size: 196 Color: 3

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6995 Color: 4
Size: 812 Color: 4
Size: 256 Color: 1

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 2
Size: 3354 Color: 1
Size: 640 Color: 0

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 4378 Color: 3
Size: 3348 Color: 0
Size: 336 Color: 1

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 4506 Color: 0
Size: 3332 Color: 2
Size: 224 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 1
Size: 1681 Color: 3
Size: 256 Color: 0

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 6290 Color: 2
Size: 1436 Color: 1
Size: 336 Color: 0

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6347 Color: 0
Size: 1451 Color: 1
Size: 264 Color: 4

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 0
Size: 1154 Color: 4

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 4910 Color: 1
Size: 2943 Color: 2
Size: 208 Color: 4

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 4
Size: 2571 Color: 3
Size: 636 Color: 3

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 5046 Color: 1
Size: 2603 Color: 2
Size: 412 Color: 0

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 5526 Color: 4
Size: 2243 Color: 4
Size: 292 Color: 2

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 5568 Color: 4
Size: 1701 Color: 1
Size: 792 Color: 2

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 5640 Color: 1
Size: 2245 Color: 2
Size: 176 Color: 2

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 5639 Color: 2
Size: 2062 Color: 1
Size: 360 Color: 0

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 6102 Color: 3
Size: 1791 Color: 2
Size: 168 Color: 1

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 6552 Color: 2
Size: 1349 Color: 4
Size: 160 Color: 0

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 4536 Color: 2
Size: 3324 Color: 1
Size: 200 Color: 4

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 5003 Color: 2
Size: 2905 Color: 1
Size: 152 Color: 0

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 5363 Color: 1
Size: 2545 Color: 2
Size: 152 Color: 2

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 5866 Color: 1
Size: 2018 Color: 0
Size: 176 Color: 4

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 4
Size: 2280 Color: 3

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 7024 Color: 0
Size: 1020 Color: 3
Size: 16 Color: 2

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 7210 Color: 4
Size: 802 Color: 1
Size: 48 Color: 3

Bin 76: 5 of cap free
Amount of items: 5
Items: 
Size: 4037 Color: 2
Size: 1638 Color: 0
Size: 1474 Color: 1
Size: 670 Color: 0
Size: 240 Color: 0

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 5339 Color: 4
Size: 2720 Color: 3

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 5898 Color: 2
Size: 2001 Color: 1
Size: 160 Color: 3

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 6847 Color: 0
Size: 1212 Color: 4

Bin 80: 5 of cap free
Amount of items: 3
Items: 
Size: 6870 Color: 2
Size: 1141 Color: 0
Size: 48 Color: 1

Bin 81: 6 of cap free
Amount of items: 3
Items: 
Size: 4041 Color: 3
Size: 3353 Color: 0
Size: 664 Color: 1

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 4
Size: 1316 Color: 0
Size: 136 Color: 0

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 7134 Color: 3
Size: 924 Color: 4

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 4533 Color: 2
Size: 3364 Color: 4
Size: 160 Color: 3

Bin 85: 8 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 3
Size: 3428 Color: 1
Size: 576 Color: 3

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 4724 Color: 4
Size: 2206 Color: 0
Size: 1126 Color: 1

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 5070 Color: 1
Size: 2746 Color: 0
Size: 240 Color: 3

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 6944 Color: 3
Size: 1112 Color: 0

Bin 89: 10 of cap free
Amount of items: 26
Items: 
Size: 500 Color: 1
Size: 480 Color: 1
Size: 454 Color: 0
Size: 448 Color: 1
Size: 424 Color: 3
Size: 400 Color: 4
Size: 400 Color: 3
Size: 376 Color: 0
Size: 364 Color: 0
Size: 320 Color: 1
Size: 304 Color: 0
Size: 304 Color: 0
Size: 288 Color: 4
Size: 282 Color: 4
Size: 278 Color: 4
Size: 256 Color: 1
Size: 254 Color: 4
Size: 240 Color: 2
Size: 236 Color: 2
Size: 224 Color: 3
Size: 224 Color: 3
Size: 224 Color: 2
Size: 214 Color: 4
Size: 208 Color: 2
Size: 192 Color: 2
Size: 160 Color: 2

Bin 90: 10 of cap free
Amount of items: 3
Items: 
Size: 5663 Color: 2
Size: 2251 Color: 3
Size: 140 Color: 1

Bin 91: 10 of cap free
Amount of items: 3
Items: 
Size: 5915 Color: 1
Size: 1171 Color: 2
Size: 968 Color: 0

Bin 92: 11 of cap free
Amount of items: 3
Items: 
Size: 5011 Color: 1
Size: 2678 Color: 0
Size: 364 Color: 4

Bin 93: 11 of cap free
Amount of items: 3
Items: 
Size: 4941 Color: 0
Size: 2952 Color: 2
Size: 160 Color: 1

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 6126 Color: 1
Size: 1927 Color: 2

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 6181 Color: 2
Size: 1872 Color: 0

Bin 96: 11 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 0
Size: 1015 Color: 4
Size: 24 Color: 4

Bin 97: 12 of cap free
Amount of items: 3
Items: 
Size: 4226 Color: 3
Size: 3666 Color: 0
Size: 160 Color: 1

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 0
Size: 960 Color: 4

Bin 99: 13 of cap free
Amount of items: 3
Items: 
Size: 6047 Color: 4
Size: 1928 Color: 0
Size: 76 Color: 2

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 6348 Color: 2
Size: 1703 Color: 4

Bin 101: 14 of cap free
Amount of items: 3
Items: 
Size: 5418 Color: 4
Size: 2152 Color: 4
Size: 480 Color: 1

Bin 102: 15 of cap free
Amount of items: 2
Items: 
Size: 6454 Color: 4
Size: 1595 Color: 2

Bin 103: 16 of cap free
Amount of items: 3
Items: 
Size: 5484 Color: 0
Size: 1571 Color: 1
Size: 993 Color: 4

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 5646 Color: 4
Size: 2402 Color: 0

Bin 105: 16 of cap free
Amount of items: 2
Items: 
Size: 6586 Color: 1
Size: 1462 Color: 2

Bin 106: 16 of cap free
Amount of items: 2
Items: 
Size: 6964 Color: 0
Size: 1084 Color: 4

Bin 107: 17 of cap free
Amount of items: 2
Items: 
Size: 6798 Color: 2
Size: 1249 Color: 0

Bin 108: 18 of cap free
Amount of items: 2
Items: 
Size: 6774 Color: 2
Size: 1272 Color: 0

Bin 109: 20 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 4
Size: 2756 Color: 2
Size: 132 Color: 1

Bin 110: 22 of cap free
Amount of items: 2
Items: 
Size: 4968 Color: 0
Size: 3074 Color: 2

Bin 111: 22 of cap free
Amount of items: 2
Items: 
Size: 6700 Color: 4
Size: 1342 Color: 0

Bin 112: 24 of cap free
Amount of items: 3
Items: 
Size: 4979 Color: 1
Size: 2150 Color: 0
Size: 911 Color: 4

Bin 113: 25 of cap free
Amount of items: 2
Items: 
Size: 5768 Color: 3
Size: 2271 Color: 0

Bin 114: 26 of cap free
Amount of items: 2
Items: 
Size: 5072 Color: 0
Size: 2966 Color: 3

Bin 115: 28 of cap free
Amount of items: 2
Items: 
Size: 6761 Color: 3
Size: 1275 Color: 2

Bin 116: 30 of cap free
Amount of items: 2
Items: 
Size: 7028 Color: 1
Size: 1006 Color: 2

Bin 117: 33 of cap free
Amount of items: 2
Items: 
Size: 6535 Color: 3
Size: 1496 Color: 1

Bin 118: 34 of cap free
Amount of items: 3
Items: 
Size: 4050 Color: 2
Size: 3202 Color: 0
Size: 778 Color: 1

Bin 119: 38 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 4
Size: 3362 Color: 4
Size: 580 Color: 2

Bin 120: 42 of cap free
Amount of items: 2
Items: 
Size: 6298 Color: 0
Size: 1724 Color: 4

Bin 121: 46 of cap free
Amount of items: 2
Items: 
Size: 5590 Color: 0
Size: 2428 Color: 2

Bin 122: 48 of cap free
Amount of items: 2
Items: 
Size: 5935 Color: 4
Size: 2081 Color: 2

Bin 123: 48 of cap free
Amount of items: 2
Items: 
Size: 6648 Color: 2
Size: 1368 Color: 0

Bin 124: 50 of cap free
Amount of items: 2
Items: 
Size: 6188 Color: 0
Size: 1826 Color: 4

Bin 125: 52 of cap free
Amount of items: 2
Items: 
Size: 4764 Color: 2
Size: 3248 Color: 4

Bin 126: 53 of cap free
Amount of items: 2
Items: 
Size: 6447 Color: 2
Size: 1564 Color: 4

Bin 127: 53 of cap free
Amount of items: 2
Items: 
Size: 6824 Color: 4
Size: 1187 Color: 2

Bin 128: 63 of cap free
Amount of items: 2
Items: 
Size: 5371 Color: 0
Size: 2630 Color: 3

Bin 129: 68 of cap free
Amount of items: 2
Items: 
Size: 4612 Color: 3
Size: 3384 Color: 0

Bin 130: 69 of cap free
Amount of items: 3
Items: 
Size: 6023 Color: 0
Size: 1908 Color: 2
Size: 64 Color: 0

Bin 131: 118 of cap free
Amount of items: 3
Items: 
Size: 4040 Color: 0
Size: 2584 Color: 0
Size: 1322 Color: 4

Bin 132: 123 of cap free
Amount of items: 3
Items: 
Size: 4036 Color: 1
Size: 3361 Color: 0
Size: 544 Color: 3

Bin 133: 6500 of cap free
Amount of items: 8
Items: 
Size: 236 Color: 1
Size: 208 Color: 1
Size: 200 Color: 3
Size: 192 Color: 3
Size: 192 Color: 3
Size: 192 Color: 0
Size: 184 Color: 2
Size: 160 Color: 2

Total size: 1064448
Total free space: 8064

