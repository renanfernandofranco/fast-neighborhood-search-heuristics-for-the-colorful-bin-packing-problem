Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1229 Color: 0
Size: 607 Color: 4
Size: 548 Color: 3
Size: 72 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 842 Color: 1
Size: 44 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 0
Size: 742 Color: 1
Size: 68 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 1
Size: 574 Color: 0
Size: 112 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1836 Color: 3
Size: 568 Color: 4
Size: 52 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 4
Size: 478 Color: 4
Size: 88 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 3
Size: 435 Color: 1
Size: 86 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 0
Size: 354 Color: 2
Size: 128 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2003 Color: 4
Size: 389 Color: 3
Size: 64 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 2
Size: 364 Color: 2
Size: 64 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 4
Size: 280 Color: 3
Size: 142 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2074 Color: 2
Size: 226 Color: 3
Size: 156 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 3
Size: 204 Color: 4
Size: 144 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 1
Size: 272 Color: 2
Size: 60 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 0
Size: 242 Color: 0
Size: 68 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 4
Size: 164 Color: 4
Size: 112 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 2
Size: 184 Color: 4
Size: 86 Color: 3

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 2196 Color: 0
Size: 236 Color: 2
Size: 16 Color: 4
Size: 8 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 1
Size: 132 Color: 4
Size: 122 Color: 3

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 3
Size: 1021 Color: 1
Size: 204 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1500 Color: 1
Size: 863 Color: 4
Size: 92 Color: 0

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1571 Color: 2
Size: 884 Color: 3

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1660 Color: 1
Size: 715 Color: 4
Size: 80 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 3
Size: 596 Color: 3
Size: 52 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 2
Size: 530 Color: 3
Size: 104 Color: 4

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 1
Size: 425 Color: 3
Size: 108 Color: 0

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1924 Color: 0
Size: 531 Color: 2

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1931 Color: 2
Size: 292 Color: 3
Size: 232 Color: 1

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 2106 Color: 4
Size: 349 Color: 2

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 4
Size: 1142 Color: 0
Size: 76 Color: 2

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 1
Size: 938 Color: 0
Size: 48 Color: 4

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 3
Size: 705 Color: 1
Size: 36 Color: 0

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 3
Size: 621 Color: 4
Size: 104 Color: 1

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1804 Color: 3
Size: 650 Color: 2

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 3
Size: 439 Color: 1
Size: 128 Color: 4

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 1
Size: 284 Color: 0
Size: 4 Color: 1

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 2192 Color: 4
Size: 262 Color: 2

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 4
Size: 844 Color: 3
Size: 204 Color: 0

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 833 Color: 4
Size: 84 Color: 0

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1611 Color: 1
Size: 668 Color: 2
Size: 174 Color: 3

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 4
Size: 474 Color: 2
Size: 32 Color: 3

Bin 42: 3 of cap free
Amount of items: 2
Items: 
Size: 1986 Color: 2
Size: 467 Color: 1

Bin 43: 4 of cap free
Amount of items: 4
Items: 
Size: 1231 Color: 0
Size: 786 Color: 1
Size: 387 Color: 3
Size: 48 Color: 2

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 0
Size: 650 Color: 3
Size: 48 Color: 1

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 4
Size: 512 Color: 0

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 2
Size: 541 Color: 3
Size: 322 Color: 3

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 1
Size: 1022 Color: 1
Size: 64 Color: 0

Bin 48: 6 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 4
Size: 724 Color: 3
Size: 48 Color: 3

Bin 49: 6 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 3
Size: 497 Color: 4
Size: 56 Color: 1

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 1
Size: 751 Color: 4

Bin 51: 12 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 1
Size: 873 Color: 4
Size: 150 Color: 0

Bin 52: 14 of cap free
Amount of items: 4
Items: 
Size: 1334 Color: 1
Size: 1020 Color: 4
Size: 48 Color: 3
Size: 40 Color: 0

Bin 53: 17 of cap free
Amount of items: 2
Items: 
Size: 1993 Color: 3
Size: 446 Color: 1

Bin 54: 18 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 1
Size: 719 Color: 2
Size: 120 Color: 3

Bin 55: 22 of cap free
Amount of items: 16
Items: 
Size: 444 Color: 3
Size: 220 Color: 0
Size: 214 Color: 2
Size: 200 Color: 1
Size: 174 Color: 0
Size: 172 Color: 3
Size: 144 Color: 2
Size: 142 Color: 4
Size: 140 Color: 3
Size: 112 Color: 3
Size: 104 Color: 4
Size: 104 Color: 2
Size: 96 Color: 1
Size: 88 Color: 0
Size: 40 Color: 4
Size: 40 Color: 0

Bin 56: 22 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 0
Size: 840 Color: 4
Size: 356 Color: 3

Bin 57: 22 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 3
Size: 436 Color: 0
Size: 176 Color: 0

Bin 58: 23 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 1
Size: 524 Color: 3
Size: 314 Color: 4

Bin 59: 24 of cap free
Amount of items: 2
Items: 
Size: 1409 Color: 1
Size: 1023 Color: 2

Bin 60: 24 of cap free
Amount of items: 2
Items: 
Size: 1555 Color: 2
Size: 877 Color: 3

Bin 61: 25 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 4
Size: 384 Color: 1
Size: 8 Color: 4

Bin 62: 26 of cap free
Amount of items: 2
Items: 
Size: 1514 Color: 3
Size: 916 Color: 1

Bin 63: 26 of cap free
Amount of items: 2
Items: 
Size: 2036 Color: 1
Size: 394 Color: 3

Bin 64: 30 of cap free
Amount of items: 2
Items: 
Size: 1450 Color: 2
Size: 976 Color: 4

Bin 65: 30 of cap free
Amount of items: 2
Items: 
Size: 1748 Color: 0
Size: 678 Color: 2

Bin 66: 2036 of cap free
Amount of items: 5
Items: 
Size: 92 Color: 4
Size: 88 Color: 1
Size: 88 Color: 0
Size: 76 Color: 3
Size: 76 Color: 2

Total size: 159640
Total free space: 2456

