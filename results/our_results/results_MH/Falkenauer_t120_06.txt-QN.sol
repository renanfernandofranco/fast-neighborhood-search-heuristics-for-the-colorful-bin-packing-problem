Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 108
Size: 289 Color: 40
Size: 258 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 102
Size: 323 Color: 63
Size: 250 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 117
Size: 257 Color: 14
Size: 252 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 98
Size: 301 Color: 53
Size: 297 Color: 48

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 76
Size: 266 Color: 25
Size: 375 Color: 89

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 114
Size: 278 Color: 33
Size: 254 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 106
Size: 295 Color: 45
Size: 257 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 85
Size: 318 Color: 59
Size: 311 Color: 58

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 100
Size: 325 Color: 65
Size: 255 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 115
Size: 271 Color: 28
Size: 260 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 109
Size: 292 Color: 43
Size: 253 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 104
Size: 320 Color: 61
Size: 251 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 119
Size: 257 Color: 13
Size: 250 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 87
Size: 357 Color: 73
Size: 269 Color: 27

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 112
Size: 285 Color: 38
Size: 254 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 118
Size: 257 Color: 15
Size: 252 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 90
Size: 358 Color: 75
Size: 265 Color: 24

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 97
Size: 350 Color: 72
Size: 253 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 91
Size: 323 Color: 64
Size: 299 Color: 50

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 111
Size: 281 Color: 35
Size: 260 Color: 20

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 82
Size: 357 Color: 74
Size: 277 Color: 32

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 80
Size: 326 Color: 67
Size: 311 Color: 57

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 96
Size: 319 Color: 60
Size: 290 Color: 42

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 113
Size: 273 Color: 29
Size: 262 Color: 22

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 105
Size: 299 Color: 51
Size: 260 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 107
Size: 286 Color: 39
Size: 263 Color: 23

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 79
Size: 359 Color: 77
Size: 281 Color: 34

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 93
Size: 336 Color: 70
Size: 284 Color: 36

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 103
Size: 303 Color: 54
Size: 269 Color: 26

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 101
Size: 299 Color: 49
Size: 276 Color: 31

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 110
Size: 285 Color: 37
Size: 258 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 84
Size: 327 Color: 68
Size: 303 Color: 55

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 83
Size: 330 Color: 69
Size: 300 Color: 52

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 88
Size: 373 Color: 86
Size: 252 Color: 5

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 92
Size: 325 Color: 66
Size: 297 Color: 46

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 99
Size: 304 Color: 56
Size: 292 Color: 44

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 95
Size: 321 Color: 62
Size: 289 Color: 41

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 116
Size: 275 Color: 30
Size: 254 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 94
Size: 360 Color: 78
Size: 260 Color: 21

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 81
Size: 339 Color: 71
Size: 297 Color: 47

Total size: 40000
Total free space: 0

