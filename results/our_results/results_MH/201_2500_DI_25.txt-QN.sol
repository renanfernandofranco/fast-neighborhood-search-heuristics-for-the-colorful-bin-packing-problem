Capicity Bin: 2464
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1080 Color: 136
Size: 912 Color: 130
Size: 304 Color: 88
Size: 168 Color: 63

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1972 Color: 172
Size: 394 Color: 101
Size: 56 Color: 23
Size: 42 Color: 9

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 2135 Color: 189
Size: 269 Color: 80
Size: 44 Color: 11
Size: 16 Color: 1

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 2194 Color: 198
Size: 148 Color: 60
Size: 68 Color: 31
Size: 54 Color: 20

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 148
Size: 810 Color: 124
Size: 160 Color: 62

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 161
Size: 542 Color: 113
Size: 104 Color: 46

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2087 Color: 183
Size: 315 Color: 90
Size: 62 Color: 26

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 188
Size: 226 Color: 74
Size: 116 Color: 49

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2169 Color: 195
Size: 273 Color: 81
Size: 22 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 179
Size: 338 Color: 92
Size: 76 Color: 34

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 1994 Color: 173
Size: 374 Color: 98
Size: 52 Color: 17
Size: 44 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1403 Color: 146
Size: 885 Color: 128
Size: 176 Color: 65

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2215 Color: 201
Size: 209 Color: 71
Size: 40 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 154
Size: 669 Color: 119
Size: 132 Color: 55

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1969 Color: 171
Size: 413 Color: 102
Size: 82 Color: 38

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 150
Size: 871 Color: 127
Size: 50 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 140
Size: 1102 Color: 137
Size: 128 Color: 53

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 159
Size: 578 Color: 114
Size: 112 Color: 48

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 152
Size: 722 Color: 121
Size: 140 Color: 57

Bin 20: 0 of cap free
Amount of items: 4
Items: 
Size: 2098 Color: 184
Size: 302 Color: 87
Size: 32 Color: 6
Size: 32 Color: 5

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2121 Color: 187
Size: 287 Color: 85
Size: 56 Color: 22

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 174
Size: 383 Color: 99
Size: 76 Color: 35

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1905 Color: 166
Size: 467 Color: 107
Size: 92 Color: 42

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2067 Color: 182
Size: 331 Color: 91
Size: 66 Color: 28

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2150 Color: 192
Size: 262 Color: 78
Size: 52 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 156
Size: 622 Color: 117
Size: 124 Color: 52

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2045 Color: 178
Size: 351 Color: 95
Size: 68 Color: 30

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 158
Size: 591 Color: 115
Size: 116 Color: 50

Bin 29: 0 of cap free
Amount of items: 4
Items: 
Size: 1220 Color: 138
Size: 840 Color: 125
Size: 340 Color: 93
Size: 64 Color: 27

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2193 Color: 197
Size: 267 Color: 79
Size: 4 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2024 Color: 176
Size: 368 Color: 97
Size: 72 Color: 32

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 143
Size: 942 Color: 131
Size: 184 Color: 67

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 165
Size: 478 Color: 108
Size: 92 Color: 41

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2201 Color: 199
Size: 235 Color: 75
Size: 28 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2143 Color: 191
Size: 275 Color: 83
Size: 46 Color: 12

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 193
Size: 261 Color: 77
Size: 50 Color: 16

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 145
Size: 887 Color: 129
Size: 176 Color: 66

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1354 Color: 144
Size: 1062 Color: 135
Size: 48 Color: 13

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 155
Size: 662 Color: 118
Size: 128 Color: 54

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 168
Size: 425 Color: 105
Size: 84 Color: 39

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 139
Size: 1027 Color: 134
Size: 204 Color: 69

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 149
Size: 771 Color: 123
Size: 152 Color: 61

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 170
Size: 418 Color: 103
Size: 80 Color: 36

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 186
Size: 286 Color: 84
Size: 72 Color: 33

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 164
Size: 481 Color: 109
Size: 96 Color: 44

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 200
Size: 214 Color: 72
Size: 40 Color: 7

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 175
Size: 306 Color: 89
Size: 140 Color: 56

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2183 Color: 196
Size: 221 Color: 73
Size: 60 Color: 25

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1250 Color: 142
Size: 1014 Color: 132
Size: 200 Color: 68

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 167
Size: 442 Color: 106
Size: 88 Color: 40

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 181
Size: 346 Color: 94
Size: 56 Color: 21

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 177
Size: 362 Color: 96
Size: 68 Color: 29

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1866 Color: 163
Size: 502 Color: 110
Size: 96 Color: 43

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 185
Size: 301 Color: 86
Size: 60 Color: 24

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 2138 Color: 190
Size: 274 Color: 82
Size: 52 Color: 18

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 2055 Color: 180
Size: 387 Color: 100
Size: 22 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 151
Size: 762 Color: 122
Size: 148 Color: 59

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1607 Color: 153
Size: 715 Color: 120
Size: 142 Color: 58

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 157
Size: 621 Color: 116
Size: 122 Color: 51

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 194
Size: 250 Color: 76
Size: 48 Color: 14

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 169
Size: 421 Color: 104
Size: 82 Color: 37

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 147
Size: 870 Color: 126
Size: 172 Color: 64

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 160
Size: 541 Color: 112
Size: 108 Color: 47

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 162
Size: 523 Color: 111
Size: 104 Color: 45

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1235 Color: 141
Size: 1025 Color: 133
Size: 204 Color: 70

Total size: 160160
Total free space: 0

