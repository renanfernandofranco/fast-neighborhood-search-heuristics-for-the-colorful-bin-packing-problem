Capicity Bin: 8360
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4128 Color: 272
Size: 1184 Color: 179
Size: 1032 Color: 166
Size: 704 Color: 137
Size: 512 Color: 111
Size: 448 Color: 104
Size: 352 Color: 88

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5292 Color: 300
Size: 2564 Color: 243
Size: 504 Color: 109

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 6832 Color: 353
Size: 1288 Color: 186
Size: 128 Color: 11
Size: 72 Color: 3
Size: 40 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4186 Color: 276
Size: 3750 Color: 271
Size: 424 Color: 99

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4673 Color: 283
Size: 3073 Color: 257
Size: 614 Color: 127

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7353 Color: 392
Size: 841 Color: 146
Size: 166 Color: 22

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6134 Color: 322
Size: 1858 Color: 220
Size: 368 Color: 91

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5702 Color: 308
Size: 2466 Color: 238
Size: 192 Color: 38

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 330
Size: 1662 Color: 212
Size: 328 Color: 80

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6614 Color: 341
Size: 1458 Color: 199
Size: 288 Color: 71

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5179 Color: 296
Size: 2651 Color: 245
Size: 530 Color: 116

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4713 Color: 284
Size: 3041 Color: 256
Size: 606 Color: 126

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 315
Size: 2028 Color: 226
Size: 400 Color: 96

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5781 Color: 311
Size: 2181 Color: 230
Size: 398 Color: 95

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7110 Color: 372
Size: 1042 Color: 167
Size: 208 Color: 45

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5511 Color: 303
Size: 2375 Color: 237
Size: 474 Color: 108

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6505 Color: 333
Size: 1547 Color: 206
Size: 308 Color: 77

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5551 Color: 304
Size: 2341 Color: 236
Size: 468 Color: 106

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7402 Color: 397
Size: 802 Color: 142
Size: 156 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7220 Color: 379
Size: 956 Color: 160
Size: 184 Color: 31

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 365
Size: 1148 Color: 175
Size: 224 Color: 52

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 326
Size: 1714 Color: 216
Size: 340 Color: 85

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5180 Color: 297
Size: 2652 Color: 246
Size: 528 Color: 115

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4725 Color: 286
Size: 3191 Color: 260
Size: 444 Color: 103

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7388 Color: 394
Size: 812 Color: 144
Size: 160 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5155 Color: 295
Size: 2671 Color: 247
Size: 534 Color: 117

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 376
Size: 988 Color: 162
Size: 192 Color: 37

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5806 Color: 312
Size: 2130 Color: 228
Size: 424 Color: 98

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 351
Size: 1292 Color: 188
Size: 256 Color: 62

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4830 Color: 290
Size: 2942 Color: 252
Size: 588 Color: 122

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5322 Color: 301
Size: 2534 Color: 241
Size: 504 Color: 110

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7369 Color: 393
Size: 827 Color: 145
Size: 164 Color: 21

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6078 Color: 319
Size: 1902 Color: 223
Size: 380 Color: 93

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5988 Color: 317
Size: 1980 Color: 224
Size: 392 Color: 94

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 307
Size: 2220 Color: 232
Size: 440 Color: 102

Bin 36: 0 of cap free
Amount of items: 4
Items: 
Size: 6720 Color: 346
Size: 1280 Color: 185
Size: 240 Color: 57
Size: 120 Color: 9

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 325
Size: 1740 Color: 217
Size: 344 Color: 86

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6532 Color: 334
Size: 1524 Color: 205
Size: 304 Color: 76

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 3640 Color: 270
Size: 3144 Color: 259
Size: 1576 Color: 207

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7524 Color: 402
Size: 700 Color: 136
Size: 136 Color: 13

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6333 Color: 327
Size: 1691 Color: 215
Size: 336 Color: 82

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 336
Size: 1492 Color: 204
Size: 296 Color: 74

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7230 Color: 381
Size: 942 Color: 158
Size: 188 Color: 36

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 377
Size: 1108 Color: 172
Size: 64 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6228 Color: 324
Size: 1780 Color: 218
Size: 352 Color: 87

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 4496 Color: 280
Size: 3224 Color: 261
Size: 640 Color: 130

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5644 Color: 305
Size: 2516 Color: 240
Size: 200 Color: 41

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5380 Color: 302
Size: 2508 Color: 239
Size: 472 Color: 107

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5993 Color: 318
Size: 2031 Color: 227
Size: 336 Color: 83

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 344
Size: 1396 Color: 195
Size: 272 Color: 68

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4782 Color: 288
Size: 3450 Color: 264
Size: 128 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7460 Color: 399
Size: 756 Color: 140
Size: 144 Color: 15

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 294
Size: 2684 Color: 248
Size: 536 Color: 118

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 306
Size: 2262 Color: 233
Size: 452 Color: 105

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 352
Size: 1289 Color: 187
Size: 256 Color: 61

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 7255 Color: 384
Size: 921 Color: 155
Size: 184 Color: 33

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 320
Size: 1876 Color: 222
Size: 368 Color: 90

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 5286 Color: 299
Size: 2562 Color: 242
Size: 512 Color: 112

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 7222 Color: 380
Size: 950 Color: 159
Size: 188 Color: 35

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 7060 Color: 368
Size: 1084 Color: 171
Size: 216 Color: 48

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 7466 Color: 400
Size: 746 Color: 139
Size: 148 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4724 Color: 285
Size: 3036 Color: 255
Size: 600 Color: 124

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 343
Size: 1404 Color: 196
Size: 272 Color: 67

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 371
Size: 1070 Color: 168
Size: 212 Color: 47

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 7316 Color: 387
Size: 876 Color: 151
Size: 168 Color: 24

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4965 Color: 292
Size: 2831 Color: 250
Size: 564 Color: 120

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 331
Size: 1644 Color: 211
Size: 320 Color: 79

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6932 Color: 359
Size: 1196 Color: 180
Size: 232 Color: 55

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 350
Size: 1438 Color: 198
Size: 120 Color: 10

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 362
Size: 1166 Color: 177
Size: 232 Color: 54

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6945 Color: 361
Size: 1181 Color: 178
Size: 234 Color: 56

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 363
Size: 1156 Color: 176
Size: 224 Color: 51

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5187 Color: 298
Size: 2645 Color: 244
Size: 528 Color: 113

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 310
Size: 2164 Color: 229
Size: 432 Color: 100

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 7070 Color: 370
Size: 1078 Color: 169
Size: 212 Color: 46

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 7396 Color: 396
Size: 804 Color: 143
Size: 160 Color: 20

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 385
Size: 910 Color: 154
Size: 180 Color: 29

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 7406 Color: 398
Size: 798 Color: 141
Size: 156 Color: 18

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 337
Size: 1489 Color: 203
Size: 296 Color: 75

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6866 Color: 357
Size: 1246 Color: 181
Size: 248 Color: 58

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6335 Color: 328
Size: 1689 Color: 214
Size: 336 Color: 84

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 7326 Color: 388
Size: 862 Color: 150
Size: 172 Color: 27

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 4458 Color: 279
Size: 3254 Color: 262
Size: 648 Color: 131

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6834 Color: 354
Size: 1274 Color: 184
Size: 252 Color: 59

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7238 Color: 383
Size: 938 Color: 156
Size: 184 Color: 32

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6765 Color: 348
Size: 1331 Color: 191
Size: 264 Color: 64

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6546 Color: 335
Size: 1606 Color: 210
Size: 208 Color: 44

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6794 Color: 349
Size: 1306 Color: 190
Size: 260 Color: 63

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 7157 Color: 374
Size: 1003 Color: 164
Size: 200 Color: 42

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 7474 Color: 401
Size: 742 Color: 138
Size: 144 Color: 14

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 4604 Color: 282
Size: 3132 Color: 258
Size: 624 Color: 128

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 4822 Color: 289
Size: 2950 Color: 253
Size: 588 Color: 123

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 277
Size: 3484 Color: 268
Size: 688 Color: 132

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 340
Size: 1460 Color: 200
Size: 288 Color: 70

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 7166 Color: 375
Size: 998 Color: 163
Size: 196 Color: 40

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 332
Size: 1581 Color: 208
Size: 314 Color: 78

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 7063 Color: 369
Size: 1081 Color: 170
Size: 216 Color: 49

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 338
Size: 1470 Color: 202
Size: 292 Color: 73

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 281
Size: 3300 Color: 263
Size: 528 Color: 114

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 6865 Color: 356
Size: 1435 Color: 197
Size: 60 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 4182 Color: 274
Size: 3482 Color: 266
Size: 696 Color: 134

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 373
Size: 1029 Color: 165
Size: 204 Color: 43

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 4726 Color: 287
Size: 3030 Color: 254
Size: 604 Color: 125

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6842 Color: 355
Size: 1266 Color: 183
Size: 252 Color: 60

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5052 Color: 293
Size: 2764 Color: 249
Size: 544 Color: 119

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 7027 Color: 367
Size: 1111 Color: 173
Size: 222 Color: 50

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 7334 Color: 391
Size: 858 Color: 148
Size: 168 Color: 23

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 291
Size: 2922 Color: 251
Size: 584 Color: 121

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 342
Size: 1601 Color: 209
Size: 98 Color: 5

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 329
Size: 1666 Color: 213
Size: 332 Color: 81

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 4220 Color: 278
Size: 3500 Color: 269
Size: 640 Color: 129

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5885 Color: 314
Size: 2293 Color: 235
Size: 182 Color: 30

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 347
Size: 1354 Color: 192
Size: 268 Color: 65

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 4181 Color: 273
Size: 3483 Color: 267
Size: 696 Color: 133

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 6998 Color: 366
Size: 1258 Color: 182
Size: 104 Color: 7

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 5821 Color: 313
Size: 2271 Color: 234
Size: 268 Color: 66

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 7191 Color: 378
Size: 975 Color: 161
Size: 194 Color: 39

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 364
Size: 1147 Color: 174
Size: 228 Color: 53

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6877 Color: 358
Size: 1379 Color: 193
Size: 104 Color: 6

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 4183 Color: 275
Size: 3481 Color: 265
Size: 696 Color: 135

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 321
Size: 1863 Color: 221
Size: 372 Color: 92

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 7329 Color: 389
Size: 861 Color: 149
Size: 170 Color: 26

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 345
Size: 1381 Color: 194
Size: 274 Color: 69

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 309
Size: 2182 Color: 231
Size: 436 Color: 101

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 5937 Color: 316
Size: 2021 Color: 225
Size: 402 Color: 97

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 7299 Color: 386
Size: 885 Color: 152
Size: 176 Color: 28

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6175 Color: 323
Size: 1821 Color: 219
Size: 364 Color: 89

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 7333 Color: 390
Size: 857 Color: 147
Size: 170 Color: 25

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 7393 Color: 395
Size: 885 Color: 153
Size: 82 Color: 4

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6609 Color: 339
Size: 1461 Color: 201
Size: 290 Color: 72

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6937 Color: 360
Size: 1305 Color: 189
Size: 118 Color: 8

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7235 Color: 382
Size: 939 Color: 157
Size: 186 Color: 34

Total size: 1103520
Total free space: 0

