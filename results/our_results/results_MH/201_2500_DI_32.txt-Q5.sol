Capicity Bin: 2356
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1880 Color: 2
Size: 144 Color: 1
Size: 136 Color: 0
Size: 112 Color: 4
Size: 36 Color: 1
Size: 32 Color: 3
Size: 16 Color: 0

Bin 2: 0 of cap free
Amount of items: 6
Items: 
Size: 1164 Color: 4
Size: 360 Color: 0
Size: 320 Color: 2
Size: 292 Color: 1
Size: 188 Color: 4
Size: 32 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 4
Size: 429 Color: 3
Size: 84 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2075 Color: 1
Size: 235 Color: 3
Size: 46 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2070 Color: 4
Size: 242 Color: 2
Size: 44 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 0
Size: 679 Color: 1
Size: 134 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2080 Color: 4
Size: 236 Color: 3
Size: 40 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 2
Size: 649 Color: 1
Size: 128 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 2
Size: 214 Color: 2
Size: 40 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 2
Size: 226 Color: 4
Size: 44 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 1
Size: 482 Color: 2
Size: 92 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 4
Size: 454 Color: 2
Size: 88 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 2
Size: 858 Color: 1
Size: 168 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1413 Color: 3
Size: 787 Color: 1
Size: 156 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2119 Color: 0
Size: 199 Color: 1
Size: 38 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 2
Size: 951 Color: 2
Size: 88 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2071 Color: 2
Size: 255 Color: 3
Size: 30 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 2
Size: 514 Color: 3
Size: 100 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1899 Color: 3
Size: 381 Color: 2
Size: 76 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 0
Size: 742 Color: 2
Size: 144 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1827 Color: 0
Size: 499 Color: 2
Size: 30 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 2
Size: 547 Color: 2
Size: 108 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 3
Size: 350 Color: 4
Size: 68 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1182 Color: 2
Size: 982 Color: 2
Size: 192 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 3
Size: 301 Color: 3
Size: 58 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 2
Size: 297 Color: 2
Size: 58 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1518 Color: 2
Size: 702 Color: 3
Size: 136 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2083 Color: 4
Size: 229 Color: 0
Size: 44 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 3
Size: 495 Color: 0
Size: 98 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1957 Color: 3
Size: 333 Color: 3
Size: 66 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2010 Color: 2
Size: 290 Color: 4
Size: 56 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 2
Size: 667 Color: 3
Size: 122 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 4
Size: 565 Color: 2
Size: 112 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 4
Size: 475 Color: 0
Size: 78 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2099 Color: 2
Size: 215 Color: 4
Size: 42 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 2
Size: 202 Color: 1
Size: 36 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 4
Size: 933 Color: 1
Size: 186 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1366 Color: 3
Size: 826 Color: 4
Size: 164 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 1
Size: 262 Color: 1
Size: 52 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1878 Color: 1
Size: 474 Color: 1
Size: 4 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 3
Size: 241 Color: 3
Size: 12 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 4
Size: 341 Color: 0
Size: 68 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 4
Size: 638 Color: 1
Size: 124 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1377 Color: 0
Size: 817 Color: 1
Size: 162 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 3
Size: 554 Color: 2
Size: 108 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 0
Size: 930 Color: 3
Size: 184 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1185 Color: 4
Size: 977 Color: 4
Size: 194 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1179 Color: 1
Size: 981 Color: 3
Size: 196 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 2
Size: 318 Color: 3
Size: 60 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 666 Color: 2
Size: 32 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1281 Color: 0
Size: 897 Color: 4
Size: 178 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 3
Size: 390 Color: 2
Size: 76 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1397 Color: 4
Size: 801 Color: 3
Size: 158 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 2
Size: 613 Color: 4
Size: 62 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 1
Size: 283 Color: 1
Size: 56 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1032 Color: 2
Size: 876 Color: 3
Size: 448 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2047 Color: 1
Size: 259 Color: 0
Size: 50 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 4
Size: 691 Color: 2
Size: 138 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 3
Size: 459 Color: 1
Size: 90 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 2
Size: 981 Color: 3
Size: 194 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2031 Color: 0
Size: 271 Color: 2
Size: 54 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 0
Size: 231 Color: 1
Size: 46 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 3
Size: 455 Color: 2
Size: 90 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1189 Color: 1
Size: 1053 Color: 3
Size: 114 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1873 Color: 4
Size: 403 Color: 4
Size: 80 Color: 2

Total size: 153140
Total free space: 0

