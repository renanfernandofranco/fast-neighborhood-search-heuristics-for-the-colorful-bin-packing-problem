Capicity Bin: 1001
Lower Bound: 896

Bins used: 897
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 551 Color: 18
Size: 338 Color: 13
Size: 112 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 658 Color: 0
Size: 239 Color: 14
Size: 104 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 8
Size: 180 Color: 13
Size: 158 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 16
Size: 182 Color: 6
Size: 125 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 8
Size: 179 Color: 11
Size: 154 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 15
Size: 167 Color: 19
Size: 113 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 728 Color: 0
Size: 144 Color: 18
Size: 129 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 2
Size: 165 Color: 10
Size: 101 Color: 19

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 17
Size: 126 Color: 4
Size: 112 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 2
Size: 212 Color: 12
Size: 185 Color: 0

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 19
Size: 270 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 378 Color: 4
Size: 201 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 15
Size: 269 Color: 11
Size: 144 Color: 5

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 1
Size: 453 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 7
Size: 141 Color: 4
Size: 139 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 3
Size: 194 Color: 18
Size: 167 Color: 12

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 4
Size: 419 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 9
Size: 141 Color: 17
Size: 125 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 636 Color: 2
Size: 183 Color: 18
Size: 182 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 3
Size: 168 Color: 1
Size: 151 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 554 Color: 3
Size: 268 Color: 14
Size: 179 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 15
Size: 193 Color: 17
Size: 167 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 563 Color: 12
Size: 240 Color: 0
Size: 198 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 740 Color: 19
Size: 143 Color: 9
Size: 118 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 6
Size: 155 Color: 18
Size: 117 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 18
Size: 123 Color: 18
Size: 105 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 11
Size: 387 Color: 12
Size: 125 Color: 7

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 3
Size: 270 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 583 Color: 14
Size: 240 Color: 7
Size: 178 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 19
Size: 211 Color: 7
Size: 166 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 15
Size: 158 Color: 18
Size: 104 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 2
Size: 114 Color: 14
Size: 101 Color: 0

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 2
Size: 350 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 9
Size: 170 Color: 7
Size: 167 Color: 5

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 16
Size: 249 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 634 Color: 0
Size: 190 Color: 6
Size: 177 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 785 Color: 4
Size: 110 Color: 5
Size: 106 Color: 2

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 4
Size: 302 Color: 17

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 7
Size: 392 Color: 8

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 17
Size: 170 Color: 4
Size: 154 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 15
Size: 184 Color: 14
Size: 161 Color: 14

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 17
Size: 185 Color: 11
Size: 157 Color: 5

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 13
Size: 140 Color: 4
Size: 126 Color: 17

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 16
Size: 477 Color: 17

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 11
Size: 284 Color: 15

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 15
Size: 243 Color: 0
Size: 198 Color: 17

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 606 Color: 15
Size: 252 Color: 4
Size: 143 Color: 15

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 11
Size: 314 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 17
Size: 253 Color: 10
Size: 188 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 356 Color: 11
Size: 186 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 11
Size: 148 Color: 5
Size: 132 Color: 9

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 15
Size: 482 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 4
Size: 178 Color: 11
Size: 156 Color: 7

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 6
Size: 155 Color: 3
Size: 154 Color: 1

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 1
Size: 339 Color: 9

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 7
Size: 232 Color: 2
Size: 102 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 368 Color: 10
Size: 212 Color: 18

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 16
Size: 180 Color: 0
Size: 171 Color: 9

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 4
Size: 118 Color: 16
Size: 110 Color: 18

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 12
Size: 390 Color: 14

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 4
Size: 212 Color: 16
Size: 179 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 16
Size: 146 Color: 11
Size: 136 Color: 19

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 18
Size: 369 Color: 12
Size: 144 Color: 11

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 6
Size: 249 Color: 8

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 11
Size: 484 Color: 14

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 18
Size: 210 Color: 16
Size: 206 Color: 18

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 10
Size: 301 Color: 2
Size: 151 Color: 7

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 18
Size: 146 Color: 2
Size: 135 Color: 14

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 551 Color: 17
Size: 245 Color: 5
Size: 205 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 16
Size: 251 Color: 1
Size: 190 Color: 12

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 14
Size: 281 Color: 5

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 563 Color: 1
Size: 269 Color: 8
Size: 169 Color: 16

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 728 Color: 9
Size: 143 Color: 7
Size: 130 Color: 9

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 11
Size: 413 Color: 9

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 3
Size: 237 Color: 12

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 6
Size: 369 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 18
Size: 269 Color: 3
Size: 122 Color: 11

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 460 Color: 18

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 19
Size: 201 Color: 15
Size: 172 Color: 14

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 11
Size: 424 Color: 6
Size: 118 Color: 5

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 8
Size: 278 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 9
Size: 269 Color: 16
Size: 144 Color: 12

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 6
Size: 441 Color: 19

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 13
Size: 377 Color: 9

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 12
Size: 385 Color: 8

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 776 Color: 2
Size: 122 Color: 10
Size: 103 Color: 12

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 12
Size: 372 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 17
Size: 119 Color: 17
Size: 119 Color: 14

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 15
Size: 147 Color: 13
Size: 115 Color: 10

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 15
Size: 299 Color: 2
Size: 177 Color: 14

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 554 Color: 5
Size: 296 Color: 10
Size: 151 Color: 0

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 17
Size: 314 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 11
Size: 210 Color: 1
Size: 182 Color: 10

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 9
Size: 191 Color: 8
Size: 160 Color: 9

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 7
Size: 269 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 16
Size: 141 Color: 18
Size: 125 Color: 16

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 18
Size: 320 Color: 4
Size: 159 Color: 18

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 7
Size: 160 Color: 10
Size: 123 Color: 12

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 673 Color: 15
Size: 188 Color: 10
Size: 140 Color: 7

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 1
Size: 176 Color: 17
Size: 175 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 554 Color: 5
Size: 338 Color: 14
Size: 109 Color: 7

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 11
Size: 147 Color: 4
Size: 111 Color: 9

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 673 Color: 2
Size: 188 Color: 2
Size: 140 Color: 7

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 10
Size: 232 Color: 8

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 14
Size: 395 Color: 6

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 16
Size: 219 Color: 3
Size: 193 Color: 9

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 4
Size: 210 Color: 16
Size: 187 Color: 11

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 5
Size: 189 Color: 7
Size: 108 Color: 2

Bin 109: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 13
Size: 337 Color: 4

Bin 110: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 14
Size: 358 Color: 19

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 18
Size: 232 Color: 2
Size: 160 Color: 10

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 3
Size: 155 Color: 13
Size: 117 Color: 15

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 18
Size: 190 Color: 16
Size: 170 Color: 0

Bin 114: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 7
Size: 308 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 6
Size: 230 Color: 11
Size: 116 Color: 8

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 15
Size: 195 Color: 19
Size: 150 Color: 8

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 751 Color: 17
Size: 145 Color: 12
Size: 105 Color: 19

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 521 Color: 10
Size: 369 Color: 10
Size: 111 Color: 6

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 12
Size: 137 Color: 16
Size: 101 Color: 12

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 9
Size: 359 Color: 15

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 11
Size: 398 Color: 4
Size: 116 Color: 12

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 0
Size: 176 Color: 10
Size: 104 Color: 17

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 564 Color: 16
Size: 330 Color: 6
Size: 107 Color: 9

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 14
Size: 187 Color: 17
Size: 158 Color: 13

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 11
Size: 117 Color: 12
Size: 111 Color: 5

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 9
Size: 269 Color: 18
Size: 183 Color: 6

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 551 Color: 15
Size: 268 Color: 9
Size: 182 Color: 11

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 3
Size: 161 Color: 19
Size: 111 Color: 7

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 17
Size: 176 Color: 1
Size: 175 Color: 6

Bin 130: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 1
Size: 344 Color: 9

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 5
Size: 426 Color: 16
Size: 106 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 2
Size: 177 Color: 11
Size: 170 Color: 19

Bin 133: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 261 Color: 7

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 13
Size: 307 Color: 9

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 11
Size: 343 Color: 10

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 538 Color: 10
Size: 284 Color: 3
Size: 179 Color: 12

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 7
Size: 237 Color: 5

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 6
Size: 362 Color: 9

Bin 139: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 6
Size: 409 Color: 2

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 4
Size: 484 Color: 6

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 785 Color: 14
Size: 116 Color: 8
Size: 100 Color: 4

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 515 Color: 9
Size: 383 Color: 5
Size: 103 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 8
Size: 187 Color: 11
Size: 164 Color: 6

Bin 144: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 3
Size: 337 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 12
Size: 435 Color: 10
Size: 106 Color: 5

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 14
Size: 378 Color: 11
Size: 170 Color: 17

Bin 147: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 15
Size: 424 Color: 8

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 3
Size: 198 Color: 12
Size: 193 Color: 7

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 14
Size: 174 Color: 16
Size: 171 Color: 16

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 19
Size: 147 Color: 7
Size: 111 Color: 19

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 728 Color: 8
Size: 141 Color: 17
Size: 132 Color: 14

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 10
Size: 378 Color: 0
Size: 166 Color: 16

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 6
Size: 298 Color: 12
Size: 284 Color: 10

Bin 154: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 8
Size: 201 Color: 12

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 17
Size: 252 Color: 4
Size: 189 Color: 12

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 517 Color: 9
Size: 382 Color: 9
Size: 102 Color: 7

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 10
Size: 165 Color: 4
Size: 162 Color: 2

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 18
Size: 337 Color: 7

Bin 159: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 12
Size: 361 Color: 15

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 10
Size: 210 Color: 6
Size: 187 Color: 18

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 8
Size: 184 Color: 7
Size: 160 Color: 4

Bin 162: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 16
Size: 358 Color: 17

Bin 163: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 16
Size: 225 Color: 14

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 2
Size: 229 Color: 16
Size: 144 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 10
Size: 314 Color: 8
Size: 313 Color: 8

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 13
Size: 469 Color: 8

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 2
Size: 188 Color: 1
Size: 160 Color: 6

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 15
Size: 151 Color: 14
Size: 143 Color: 5

Bin 169: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 7
Size: 342 Color: 3

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 3
Size: 168 Color: 11
Size: 156 Color: 19

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 5
Size: 206 Color: 5
Size: 185 Color: 15

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 19
Size: 160 Color: 19
Size: 117 Color: 6

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 1
Size: 156 Color: 7
Size: 116 Color: 17

Bin 174: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 19
Size: 372 Color: 10

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 611 Color: 4
Size: 251 Color: 4
Size: 139 Color: 14

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 15
Size: 155 Color: 8
Size: 154 Color: 19

Bin 177: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 15
Size: 350 Color: 13

Bin 178: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 201 Color: 12

Bin 179: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 18
Size: 418 Color: 13

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 0
Size: 134 Color: 0
Size: 132 Color: 17

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 17
Size: 422 Color: 7
Size: 109 Color: 19

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 523 Color: 10
Size: 298 Color: 3
Size: 180 Color: 7

Bin 183: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 16
Size: 397 Color: 4

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 13
Size: 139 Color: 8
Size: 138 Color: 18

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 519 Color: 7
Size: 304 Color: 11
Size: 178 Color: 7

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 0
Size: 186 Color: 7
Size: 159 Color: 6

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 16
Size: 284 Color: 14

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 18
Size: 378 Color: 12

Bin 189: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 16
Size: 298 Color: 7

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 15
Size: 301 Color: 19
Size: 145 Color: 4

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 16
Size: 187 Color: 13
Size: 160 Color: 11

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 563 Color: 6
Size: 269 Color: 7
Size: 169 Color: 2

Bin 193: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 13
Size: 345 Color: 1

Bin 194: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 14
Size: 272 Color: 18

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 19
Size: 171 Color: 1
Size: 138 Color: 4

Bin 196: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 14
Size: 321 Color: 17

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 620 Color: 9
Size: 245 Color: 13
Size: 136 Color: 9

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 551 Color: 14
Size: 284 Color: 19
Size: 166 Color: 12

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 1
Size: 374 Color: 10
Size: 103 Color: 3

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 7
Size: 382 Color: 4
Size: 212 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 553 Color: 5
Size: 269 Color: 11
Size: 179 Color: 3

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 10
Size: 487 Color: 2

Bin 203: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 6

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 8
Size: 140 Color: 16
Size: 132 Color: 12

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 338 Color: 18
Size: 193 Color: 1

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 482 Color: 3

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 4
Size: 163 Color: 17
Size: 145 Color: 11

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 18
Size: 210 Color: 2
Size: 206 Color: 18

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 13
Size: 368 Color: 0
Size: 180 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 12
Size: 118 Color: 2
Size: 110 Color: 10

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 11
Size: 203 Color: 17
Size: 194 Color: 3

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 2
Size: 146 Color: 7
Size: 135 Color: 12

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 751 Color: 12
Size: 133 Color: 14
Size: 117 Color: 16

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 13
Size: 330 Color: 1
Size: 149 Color: 18

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 6
Size: 132 Color: 0
Size: 106 Color: 17

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 5
Size: 402 Color: 2
Size: 128 Color: 17

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 2
Size: 255 Color: 16
Size: 161 Color: 13

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 13
Size: 338 Color: 10
Size: 139 Color: 6

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 11
Size: 210 Color: 12

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 2
Size: 156 Color: 1
Size: 138 Color: 18

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 2
Size: 268 Color: 19
Size: 240 Color: 5

Bin 222: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 15
Size: 369 Color: 17

Bin 223: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 14
Size: 328 Color: 3

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 764 Color: 8
Size: 127 Color: 14
Size: 110 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 563 Color: 1
Size: 269 Color: 3
Size: 169 Color: 2

Bin 226: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 7
Size: 499 Color: 14

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 17
Size: 369 Color: 5

Bin 228: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 17
Size: 346 Color: 11

Bin 229: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 6
Size: 426 Color: 3

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 2
Size: 330 Color: 15
Size: 175 Color: 17

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 10
Size: 356 Color: 13

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 1
Size: 170 Color: 12
Size: 124 Color: 2

Bin 233: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 11
Size: 438 Color: 18

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 16
Size: 143 Color: 11
Size: 105 Color: 19

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 523 Color: 7
Size: 298 Color: 1
Size: 180 Color: 13

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 9
Size: 243 Color: 12
Size: 107 Color: 5

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 517 Color: 6
Size: 382 Color: 19
Size: 102 Color: 17

Bin 238: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 1
Size: 446 Color: 7

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 17
Size: 162 Color: 7
Size: 161 Color: 1

Bin 240: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 18
Size: 258 Color: 9

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 583 Color: 15
Size: 239 Color: 2
Size: 179 Color: 16

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 0
Size: 416 Color: 17

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 0
Size: 211 Color: 11
Size: 162 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 16
Size: 407 Color: 18
Size: 152 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 2
Size: 269 Color: 2
Size: 183 Color: 12

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 19
Size: 152 Color: 13
Size: 138 Color: 6

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 5
Size: 219 Color: 9
Size: 127 Color: 7

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 620 Color: 4
Size: 211 Color: 8
Size: 170 Color: 6

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 13
Size: 403 Color: 14
Size: 131 Color: 18

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 4
Size: 315 Color: 18
Size: 164 Color: 10

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 16
Size: 374 Color: 14
Size: 103 Color: 11

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 2
Size: 315 Color: 15
Size: 164 Color: 3

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 18
Size: 434 Color: 10
Size: 107 Color: 12

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 4
Size: 403 Color: 3
Size: 131 Color: 15

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 606 Color: 6
Size: 211 Color: 5
Size: 184 Color: 6

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 219 Color: 13

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8
Size: 420 Color: 9
Size: 151 Color: 18

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 11
Size: 356 Color: 14
Size: 186 Color: 17

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 9
Size: 253 Color: 10

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 5
Size: 492 Color: 18

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 420 Color: 1
Size: 151 Color: 3

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 422 Color: 18
Size: 109 Color: 1

Bin 263: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 7
Size: 283 Color: 3

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 13
Size: 314 Color: 4
Size: 313 Color: 14

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 14
Size: 452 Color: 5

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 17
Size: 468 Color: 18

Bin 267: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 272 Color: 10

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 9
Size: 401 Color: 2
Size: 166 Color: 8

Bin 269: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 16
Size: 471 Color: 5

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 13
Size: 423 Color: 6

Bin 271: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 1
Size: 218 Color: 7

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 14
Size: 255 Color: 2

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 15
Size: 237 Color: 19

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 15
Size: 211 Color: 16

Bin 275: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 13
Size: 251 Color: 1

Bin 276: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 13
Size: 222 Color: 15

Bin 277: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 10
Size: 407 Color: 12

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 1
Size: 174 Color: 10
Size: 106 Color: 11

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 9
Size: 284 Color: 7

Bin 280: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 17
Size: 461 Color: 16

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 17
Size: 337 Color: 11

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 7
Size: 335 Color: 18

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 11
Size: 496 Color: 5

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 11
Size: 340 Color: 0

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 8
Size: 374 Color: 0

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 2
Size: 201 Color: 9

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 9
Size: 332 Color: 7

Bin 288: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 5
Size: 326 Color: 12

Bin 289: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 2
Size: 318 Color: 12

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 1
Size: 294 Color: 8

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 4
Size: 321 Color: 7

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 13
Size: 466 Color: 11

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 5
Size: 205 Color: 19

Bin 294: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 15
Size: 462 Color: 17

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 5
Size: 462 Color: 0

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 2
Size: 250 Color: 17

Bin 297: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 4
Size: 321 Color: 14

Bin 298: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 10
Size: 275 Color: 18

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 19
Size: 383 Color: 3

Bin 300: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 10
Size: 234 Color: 17

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 16
Size: 482 Color: 18

Bin 302: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 5
Size: 473 Color: 3

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 0
Size: 245 Color: 19

Bin 304: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 9
Size: 322 Color: 8

Bin 305: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 18
Size: 437 Color: 8

Bin 306: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 16
Size: 306 Color: 13

Bin 307: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 9
Size: 358 Color: 11

Bin 308: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 13
Size: 442 Color: 16

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 5
Size: 454 Color: 6

Bin 310: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 1
Size: 211 Color: 10

Bin 311: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 4
Size: 311 Color: 7

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 1
Size: 205 Color: 9

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 6
Size: 219 Color: 9

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 0
Size: 329 Color: 2

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 16
Size: 216 Color: 5

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 6
Size: 267 Color: 5

Bin 317: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 9
Size: 217 Color: 10

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 18
Size: 306 Color: 15

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 4
Size: 381 Color: 16

Bin 320: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 17
Size: 475 Color: 9

Bin 321: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 6
Size: 387 Color: 15

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 2
Size: 222 Color: 14

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 477 Color: 19

Bin 324: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 2
Size: 229 Color: 0

Bin 325: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 1
Size: 487 Color: 15

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 230 Color: 11

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 19
Size: 207 Color: 7

Bin 328: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 1
Size: 311 Color: 8

Bin 329: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 9
Size: 231 Color: 4

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 14
Size: 394 Color: 7

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 13
Size: 408 Color: 5

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 10
Size: 370 Color: 16

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 256 Color: 13

Bin 334: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 18
Size: 212 Color: 6

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 8
Size: 202 Color: 5

Bin 336: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 3
Size: 227 Color: 19

Bin 337: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 18
Size: 490 Color: 17

Bin 338: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 19
Size: 482 Color: 0

Bin 339: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 10
Size: 329 Color: 18

Bin 340: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 3
Size: 329 Color: 19

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 5
Size: 213 Color: 13

Bin 342: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 4
Size: 240 Color: 19

Bin 343: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 18
Size: 298 Color: 5

Bin 344: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 8
Size: 221 Color: 17

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 18
Size: 270 Color: 10

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 14
Size: 479 Color: 5

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 7
Size: 422 Color: 9

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 9
Size: 324 Color: 3

Bin 349: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 423 Color: 19

Bin 350: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 2
Size: 431 Color: 6

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 458 Color: 17

Bin 352: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 15
Size: 458 Color: 7

Bin 353: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 5
Size: 486 Color: 0

Bin 354: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 10
Size: 398 Color: 6

Bin 355: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 4
Size: 329 Color: 17

Bin 356: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 13
Size: 459 Color: 9

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 18
Size: 467 Color: 5

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 2
Size: 216 Color: 14

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 9
Size: 430 Color: 19

Bin 360: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 5
Size: 466 Color: 15

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 9
Size: 387 Color: 2

Bin 362: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 14
Size: 245 Color: 3

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 13
Size: 219 Color: 7

Bin 364: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 7
Size: 215 Color: 3

Bin 365: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 18
Size: 231 Color: 11

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 0
Size: 239 Color: 10

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 8
Size: 466 Color: 9

Bin 368: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 10
Size: 438 Color: 14

Bin 369: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 6
Size: 258 Color: 4

Bin 370: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 4
Size: 477 Color: 15

Bin 371: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 1
Size: 220 Color: 16

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 13
Size: 492 Color: 9

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 8
Size: 240 Color: 10

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 19
Size: 480 Color: 11

Bin 375: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 6
Size: 204 Color: 8

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 10
Size: 204 Color: 19

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 5
Size: 445 Color: 16

Bin 378: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 16
Size: 471 Color: 0

Bin 379: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 0
Size: 255 Color: 1

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 18
Size: 373 Color: 17

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 6
Size: 218 Color: 7

Bin 382: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 19
Size: 474 Color: 10

Bin 383: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 8
Size: 383 Color: 1

Bin 384: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 13
Size: 258 Color: 12

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 4
Size: 269 Color: 1

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 8
Size: 389 Color: 0

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 17
Size: 289 Color: 19

Bin 388: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 0
Size: 348 Color: 13

Bin 389: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 424 Color: 15

Bin 390: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 8
Size: 435 Color: 3

Bin 391: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 5
Size: 453 Color: 4

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 18
Size: 458 Color: 0

Bin 393: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 12
Size: 368 Color: 0

Bin 394: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 17

Bin 395: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 9
Size: 412 Color: 2

Bin 396: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 13
Size: 445 Color: 17

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 5
Size: 478 Color: 14

Bin 398: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 2
Size: 384 Color: 0

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 3
Size: 267 Color: 18

Bin 400: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 17
Size: 359 Color: 15

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 9
Size: 234 Color: 3

Bin 402: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 9
Size: 274 Color: 16

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 7
Size: 274 Color: 15

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 12
Size: 408 Color: 18

Bin 405: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 9
Size: 279 Color: 8

Bin 406: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 11
Size: 468 Color: 18

Bin 407: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 4
Size: 405 Color: 10

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 12
Size: 237 Color: 18

Bin 409: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 5
Size: 466 Color: 0

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 18
Size: 227 Color: 15

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 12
Size: 435 Color: 10

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 18
Size: 408 Color: 9

Bin 413: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 5
Size: 408 Color: 15

Bin 414: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 13
Size: 335 Color: 18

Bin 415: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 9
Size: 423 Color: 2

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 14
Size: 248 Color: 17

Bin 417: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 16
Size: 308 Color: 4

Bin 418: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 2
Size: 204 Color: 10

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 12
Size: 332 Color: 7

Bin 420: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 5
Size: 355 Color: 7

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 11
Size: 388 Color: 18

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 14
Size: 388 Color: 6

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 10
Size: 211 Color: 15

Bin 424: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 5
Size: 317 Color: 16

Bin 425: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 8
Size: 350 Color: 4

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 12
Size: 298 Color: 17

Bin 427: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 1
Size: 493 Color: 7

Bin 428: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 5
Size: 459 Color: 14

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 1
Size: 404 Color: 15

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 3
Size: 483 Color: 17

Bin 431: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 10
Size: 367 Color: 18

Bin 432: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 15
Size: 465 Color: 19

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 2
Size: 353 Color: 7

Bin 434: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 14
Size: 341 Color: 6

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 17
Size: 341 Color: 10

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 15
Size: 248 Color: 17

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 4
Size: 221 Color: 6

Bin 438: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 2
Size: 427 Color: 11

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 8
Size: 342 Color: 5

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 5
Size: 222 Color: 7

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 19
Size: 359 Color: 5

Bin 442: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 9
Size: 222 Color: 13

Bin 443: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 2
Size: 350 Color: 6

Bin 444: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 9
Size: 267 Color: 12

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 3
Size: 329 Color: 14

Bin 446: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 261 Color: 1

Bin 447: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 12
Size: 411 Color: 1

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 7
Size: 478 Color: 5

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 3
Size: 296 Color: 2

Bin 450: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 5
Size: 326 Color: 4

Bin 451: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 1
Size: 454 Color: 3

Bin 452: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 440 Color: 13

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 2
Size: 440 Color: 12

Bin 454: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 15
Size: 235 Color: 13

Bin 455: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 16
Size: 235 Color: 11

Bin 456: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 9
Size: 286 Color: 10

Bin 457: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 2
Size: 286 Color: 14

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 16
Size: 363 Color: 0

Bin 459: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 8
Size: 299 Color: 12

Bin 460: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 14
Size: 315 Color: 2

Bin 461: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 1
Size: 481 Color: 7

Bin 462: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 7
Size: 363 Color: 19

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 18
Size: 379 Color: 17

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 437 Color: 14

Bin 465: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 11
Size: 379 Color: 8

Bin 466: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 19
Size: 281 Color: 5

Bin 467: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 6
Size: 285 Color: 11

Bin 468: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 8
Size: 251 Color: 13

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 12
Size: 369 Color: 0

Bin 470: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 6
Size: 463 Color: 0

Bin 471: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 2
Size: 450 Color: 6

Bin 472: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 11
Size: 410 Color: 6

Bin 473: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 12
Size: 317 Color: 14

Bin 474: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 7
Size: 283 Color: 5

Bin 475: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 292 Color: 10

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 5
Size: 292 Color: 8

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 15
Size: 472 Color: 9

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 10
Size: 333 Color: 6

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 13
Size: 351 Color: 14

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 4
Size: 469 Color: 2

Bin 481: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 4
Size: 411 Color: 18

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 13
Size: 411 Color: 17

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 18
Size: 412 Color: 3

Bin 484: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 14
Size: 487 Color: 12

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 7
Size: 322 Color: 8

Bin 486: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 12
Size: 376 Color: 3

Bin 487: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 7
Size: 357 Color: 2

Bin 488: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 18
Size: 389 Color: 3

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 12
Size: 284 Color: 17

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 17
Size: 407 Color: 2

Bin 491: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 0
Size: 346 Color: 11

Bin 492: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 5
Size: 449 Color: 4

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 18
Size: 449 Color: 13

Bin 494: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 17
Size: 257 Color: 15

Bin 495: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 13
Size: 423 Color: 2

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 19
Size: 304 Color: 6

Bin 497: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 12
Size: 378 Color: 10

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 3
Size: 265 Color: 0

Bin 499: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 16
Size: 405 Color: 18

Bin 500: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 9
Size: 490 Color: 7

Bin 501: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 19
Size: 306 Color: 11

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 16
Size: 293 Color: 15

Bin 503: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 18
Size: 364 Color: 17

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 11
Size: 457 Color: 5

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 17
Size: 344 Color: 8

Bin 506: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 16
Size: 290 Color: 9

Bin 507: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 17
Size: 279 Color: 3

Bin 508: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 2
Size: 383 Color: 7

Bin 509: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 11
Size: 360 Color: 14

Bin 510: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 3
Size: 360 Color: 5

Bin 511: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 11
Size: 275 Color: 10

Bin 512: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 17
Size: 404 Color: 8

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 8
Size: 460 Color: 10

Bin 514: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 17
Size: 348 Color: 1

Bin 515: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 18
Size: 410 Color: 17

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 17
Size: 375 Color: 5

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 17
Size: 494 Color: 5

Bin 518: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 12
Size: 449 Color: 10

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 18
Size: 472 Color: 15

Bin 520: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 406 Color: 1

Bin 521: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 8
Size: 417 Color: 18

Bin 522: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 18
Size: 231 Color: 17

Bin 523: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 18
Size: 227 Color: 2

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 12
Size: 282 Color: 1

Bin 525: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 3
Size: 465 Color: 15

Bin 526: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 6
Size: 481 Color: 13

Bin 527: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 9
Size: 402 Color: 2

Bin 528: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 12
Size: 402 Color: 15

Bin 529: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 13
Size: 300 Color: 1

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 6
Size: 300 Color: 12

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 15
Size: 347 Color: 5

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 12
Size: 289 Color: 10

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 11
Size: 263 Color: 13

Bin 534: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 5
Size: 263 Color: 16

Bin 535: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 0
Size: 299 Color: 9

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 13
Size: 279 Color: 7

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 7

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 19
Size: 335 Color: 8

Bin 539: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 7
Size: 290 Color: 9

Bin 540: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 8
Size: 481 Color: 9

Bin 541: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 449 Color: 1

Bin 542: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 19
Size: 318 Color: 11

Bin 543: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 13
Size: 411 Color: 7

Bin 544: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 11
Size: 449 Color: 5

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 17
Size: 355 Color: 14

Bin 546: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 12
Size: 273 Color: 17

Bin 547: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 5
Size: 267 Color: 10

Bin 548: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 14
Size: 451 Color: 1

Bin 549: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 12
Size: 414 Color: 11

Bin 550: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 14
Size: 226 Color: 8

Bin 551: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 7
Size: 267 Color: 1

Bin 552: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 7
Size: 455 Color: 12

Bin 553: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 11
Size: 352 Color: 15

Bin 554: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 13
Size: 259 Color: 15

Bin 555: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 2
Size: 259 Color: 10

Bin 556: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 496 Color: 12

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 0
Size: 467 Color: 17

Bin 558: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 15
Size: 310 Color: 4

Bin 559: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 15
Size: 496 Color: 19

Bin 560: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 1
Size: 355 Color: 5

Bin 561: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 9
Size: 281 Color: 12

Bin 562: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 19
Size: 394 Color: 17

Bin 563: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 8
Size: 241 Color: 14

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 16
Size: 314 Color: 2

Bin 565: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 19
Size: 275 Color: 5

Bin 566: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 12
Size: 387 Color: 10

Bin 567: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 9
Size: 435 Color: 17

Bin 568: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 19
Size: 262 Color: 10

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 6
Size: 417 Color: 18

Bin 570: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 17
Size: 341 Color: 19

Bin 571: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 19
Size: 292 Color: 4

Bin 572: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 17
Size: 226 Color: 19

Bin 573: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 10
Size: 358 Color: 7

Bin 574: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 18
Size: 493 Color: 13

Bin 575: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 10
Size: 393 Color: 7

Bin 576: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 19
Size: 332 Color: 18

Bin 577: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 13
Size: 376 Color: 12

Bin 578: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 15
Size: 380 Color: 2

Bin 579: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 15
Size: 258 Color: 16

Bin 580: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 321 Color: 10

Bin 581: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 19
Size: 392 Color: 11

Bin 582: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 343 Color: 0

Bin 583: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 19
Size: 285 Color: 18

Bin 584: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 8
Size: 483 Color: 15

Bin 585: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 11
Size: 368 Color: 19

Bin 586: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 1
Size: 323 Color: 9

Bin 587: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 13
Size: 451 Color: 18

Bin 588: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 8
Size: 268 Color: 0

Bin 589: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 5
Size: 256 Color: 10

Bin 590: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 7
Size: 394 Color: 5

Bin 591: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 12
Size: 379 Color: 5

Bin 592: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 8
Size: 379 Color: 4

Bin 593: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 3
Size: 256 Color: 0

Bin 594: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 11
Size: 415 Color: 5

Bin 595: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 3
Size: 377 Color: 10

Bin 596: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 6
Size: 325 Color: 7

Bin 597: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 10
Size: 325 Color: 1

Bin 598: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 12
Size: 230 Color: 13

Bin 599: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 15
Size: 398 Color: 10

Bin 600: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 12
Size: 398 Color: 5

Bin 601: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 10
Size: 419 Color: 15

Bin 602: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 2
Size: 367 Color: 19

Bin 603: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 14
Size: 362 Color: 4

Bin 604: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 9
Size: 492 Color: 18

Bin 605: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 19
Size: 349 Color: 10

Bin 606: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 7
Size: 434 Color: 2

Bin 607: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 15

Bin 608: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 10
Size: 290 Color: 18

Bin 609: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 13
Size: 394 Color: 18

Bin 610: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 4
Size: 394 Color: 9

Bin 611: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 19
Size: 289 Color: 13

Bin 612: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 15
Size: 207 Color: 5

Bin 613: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 16
Size: 414 Color: 9

Bin 614: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 8
Size: 303 Color: 7

Bin 615: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 8
Size: 288 Color: 15

Bin 616: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 14
Size: 406 Color: 12

Bin 617: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 12
Size: 294 Color: 2

Bin 618: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 17
Size: 314 Color: 8

Bin 619: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 3
Size: 457 Color: 1

Bin 620: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 0
Size: 395 Color: 10

Bin 621: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 8
Size: 352 Color: 9

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 2
Size: 456 Color: 19

Bin 623: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 4
Size: 465 Color: 13

Bin 624: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 18
Size: 475 Color: 15

Bin 625: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 1
Size: 368 Color: 8

Bin 626: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 5
Size: 358 Color: 0

Bin 627: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 16
Size: 495 Color: 0

Bin 628: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 7
Size: 446 Color: 12

Bin 629: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 5
Size: 436 Color: 0

Bin 630: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 19
Size: 422 Color: 5

Bin 631: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 8
Size: 322 Color: 5

Bin 632: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 0
Size: 405 Color: 10

Bin 633: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 8
Size: 500 Color: 4

Bin 634: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 3
Size: 471 Color: 6

Bin 635: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 7
Size: 219 Color: 15

Bin 636: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 7
Size: 440 Color: 15

Bin 637: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 11
Size: 310 Color: 16

Bin 638: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 5
Size: 498 Color: 16

Bin 639: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 16
Size: 498 Color: 17

Bin 640: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 17
Size: 498 Color: 5

Bin 641: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 2
Size: 494 Color: 7

Bin 642: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 14
Size: 495 Color: 10

Bin 643: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 7
Size: 496 Color: 10

Bin 644: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 5
Size: 486 Color: 7

Bin 645: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 12
Size: 485 Color: 2

Bin 646: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 13
Size: 483 Color: 8

Bin 647: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 13

Bin 648: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 3
Size: 466 Color: 16

Bin 649: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 0
Size: 462 Color: 15

Bin 650: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 0
Size: 455 Color: 19

Bin 651: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 0
Size: 455 Color: 7

Bin 652: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 19
Size: 454 Color: 2

Bin 653: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 0
Size: 454 Color: 9

Bin 654: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 11
Size: 454 Color: 2

Bin 655: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 12
Size: 454 Color: 0

Bin 656: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 16
Size: 451 Color: 13

Bin 657: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 444 Color: 4

Bin 658: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 7
Size: 444 Color: 10

Bin 659: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 16
Size: 443 Color: 1

Bin 660: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 16
Size: 442 Color: 19

Bin 661: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 8
Size: 439 Color: 16

Bin 662: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 10
Size: 436 Color: 16

Bin 663: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 12
Size: 436 Color: 6

Bin 664: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 19
Size: 432 Color: 8

Bin 665: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 7
Size: 428 Color: 9

Bin 666: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 0
Size: 421 Color: 6

Bin 667: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 9
Size: 420 Color: 19

Bin 668: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 5
Size: 406 Color: 14

Bin 669: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 13
Size: 401 Color: 2

Bin 670: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 13
Size: 400 Color: 17

Bin 671: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 9
Size: 399 Color: 5

Bin 672: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 5
Size: 399 Color: 9

Bin 673: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 12
Size: 398 Color: 3

Bin 674: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 17
Size: 396 Color: 7

Bin 675: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 10
Size: 395 Color: 6

Bin 676: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 9
Size: 395 Color: 6

Bin 677: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 16
Size: 393 Color: 15

Bin 678: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 18
Size: 388 Color: 3

Bin 679: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 18
Size: 386 Color: 7

Bin 680: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 9
Size: 386 Color: 4

Bin 681: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 18
Size: 385 Color: 12

Bin 682: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 14
Size: 384 Color: 7

Bin 683: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 11
Size: 384 Color: 6

Bin 684: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 17
Size: 380 Color: 18

Bin 685: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 14
Size: 375 Color: 2

Bin 686: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 2
Size: 375 Color: 18

Bin 687: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 17
Size: 371 Color: 7

Bin 688: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 7
Size: 371 Color: 3

Bin 689: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 14
Size: 370 Color: 9

Bin 690: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 3
Size: 370 Color: 5

Bin 691: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 16
Size: 366 Color: 5

Bin 692: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 3
Size: 366 Color: 1

Bin 693: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 1
Size: 363 Color: 4

Bin 694: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 19
Size: 358 Color: 0

Bin 695: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 10
Size: 357 Color: 19

Bin 696: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 17
Size: 354 Color: 15

Bin 697: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 17
Size: 346 Color: 6

Bin 698: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 0
Size: 341 Color: 1

Bin 699: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 15
Size: 339 Color: 17

Bin 700: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 9
Size: 336 Color: 13

Bin 701: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 0
Size: 331 Color: 6

Bin 702: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 7
Size: 329 Color: 12

Bin 703: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 19
Size: 329 Color: 16

Bin 704: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 322 Color: 8

Bin 705: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 6
Size: 320 Color: 7

Bin 706: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 11
Size: 319 Color: 19

Bin 707: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 5
Size: 320 Color: 13

Bin 708: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 16
Size: 317 Color: 8

Bin 709: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 11
Size: 316 Color: 16

Bin 710: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 16
Size: 313 Color: 12

Bin 711: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 10
Size: 312 Color: 4

Bin 712: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 6
Size: 306 Color: 10

Bin 713: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 3
Size: 305 Color: 9

Bin 714: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 15
Size: 305 Color: 4

Bin 715: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 6
Size: 305 Color: 16

Bin 716: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 297 Color: 12

Bin 717: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 296 Color: 15

Bin 718: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 14
Size: 295 Color: 18

Bin 719: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 17
Size: 291 Color: 10

Bin 720: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 18
Size: 291 Color: 10

Bin 721: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 6
Size: 290 Color: 15

Bin 722: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 5
Size: 285 Color: 16

Bin 723: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 12
Size: 282 Color: 15

Bin 724: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 18
Size: 277 Color: 13

Bin 725: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 9
Size: 277 Color: 11

Bin 726: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 275 Color: 1

Bin 727: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 16
Size: 275 Color: 12

Bin 728: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 16
Size: 274 Color: 6

Bin 729: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 10
Size: 264 Color: 8

Bin 730: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 8
Size: 263 Color: 5

Bin 731: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 18
Size: 260 Color: 0

Bin 732: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 252 Color: 16

Bin 733: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 11
Size: 248 Color: 1

Bin 734: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 18
Size: 247 Color: 11

Bin 735: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 3
Size: 247 Color: 14

Bin 736: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 18
Size: 246 Color: 6

Bin 737: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 15
Size: 246 Color: 8

Bin 738: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 244 Color: 8

Bin 739: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 14
Size: 244 Color: 17

Bin 740: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 14
Size: 241 Color: 9

Bin 741: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 3
Size: 241 Color: 17

Bin 742: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 0
Size: 239 Color: 16

Bin 743: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 12
Size: 236 Color: 11

Bin 744: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 16
Size: 236 Color: 9

Bin 745: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 9
Size: 236 Color: 15

Bin 746: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 16
Size: 232 Color: 6

Bin 747: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 18
Size: 229 Color: 1

Bin 748: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 4
Size: 227 Color: 5

Bin 749: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 18
Size: 224 Color: 19

Bin 750: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 9
Size: 223 Color: 13

Bin 751: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 10
Size: 221 Color: 9

Bin 752: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 19
Size: 220 Color: 10

Bin 753: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 220 Color: 4

Bin 754: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 11
Size: 218 Color: 0

Bin 755: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 9
Size: 216 Color: 4

Bin 756: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 6
Size: 214 Color: 2

Bin 757: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 14

Bin 758: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 12
Size: 209 Color: 11

Bin 759: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 17
Size: 204 Color: 8

Bin 760: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 4
Size: 203 Color: 18

Bin 761: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 11
Size: 203 Color: 15

Bin 762: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 18
Size: 202 Color: 8

Bin 763: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 200 Color: 4

Bin 764: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 6
Size: 200 Color: 15

Bin 765: 1 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 11
Size: 362 Color: 14
Size: 114 Color: 0

Bin 766: 1 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 16
Size: 198 Color: 1
Size: 128 Color: 14

Bin 767: 1 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 15
Size: 150 Color: 5
Size: 130 Color: 13

Bin 768: 1 of cap free
Amount of items: 3
Items: 
Size: 728 Color: 10
Size: 152 Color: 14
Size: 120 Color: 0

Bin 769: 1 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 5
Size: 320 Color: 13

Bin 770: 1 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 18
Size: 253 Color: 10

Bin 771: 1 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 10
Size: 287 Color: 11

Bin 772: 1 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 19
Size: 341 Color: 7

Bin 773: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 483 Color: 8

Bin 774: 1 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 1
Size: 424 Color: 11

Bin 775: 1 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 0
Size: 358 Color: 18

Bin 776: 1 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 11
Size: 407 Color: 14

Bin 777: 1 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 1
Size: 424 Color: 2

Bin 778: 1 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 14
Size: 424 Color: 3

Bin 779: 1 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 16
Size: 341 Color: 13

Bin 780: 1 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 12
Size: 249 Color: 7

Bin 781: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 14
Size: 483 Color: 17

Bin 782: 1 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 17
Size: 338 Color: 5

Bin 783: 1 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 11
Size: 424 Color: 4

Bin 784: 1 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 7
Size: 364 Color: 17

Bin 785: 1 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 4
Size: 230 Color: 14

Bin 786: 1 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 14
Size: 354 Color: 1

Bin 787: 1 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 8
Size: 226 Color: 11

Bin 788: 1 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 6
Size: 278 Color: 0

Bin 789: 1 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 12
Size: 299 Color: 14

Bin 790: 1 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 8
Size: 257 Color: 12

Bin 791: 1 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 6
Size: 201 Color: 12

Bin 792: 1 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 10
Size: 299 Color: 12

Bin 793: 1 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 4
Size: 357 Color: 12

Bin 794: 1 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 13
Size: 255 Color: 11

Bin 795: 1 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 4
Size: 332 Color: 8

Bin 796: 1 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 13
Size: 475 Color: 16

Bin 797: 1 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 9
Size: 241 Color: 2

Bin 798: 1 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 13
Size: 369 Color: 4

Bin 799: 1 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 7
Size: 233 Color: 10

Bin 800: 1 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 5
Size: 403 Color: 8

Bin 801: 1 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 8
Size: 220 Color: 4

Bin 802: 1 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 18
Size: 473 Color: 4

Bin 803: 1 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 15
Size: 393 Color: 13

Bin 804: 1 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 3
Size: 317 Color: 18

Bin 805: 1 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 14
Size: 464 Color: 15

Bin 806: 1 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 16
Size: 460 Color: 11

Bin 807: 1 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 493 Color: 15

Bin 808: 1 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 2
Size: 473 Color: 15

Bin 809: 1 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 3
Size: 207 Color: 19

Bin 810: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 0
Size: 427 Color: 9

Bin 811: 1 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 19
Size: 293 Color: 0

Bin 812: 1 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 17
Size: 293 Color: 16

Bin 813: 1 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 15
Size: 414 Color: 0

Bin 814: 1 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 7
Size: 203 Color: 13

Bin 815: 1 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 19
Size: 352 Color: 17

Bin 816: 1 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 6
Size: 364 Color: 3

Bin 817: 1 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 14
Size: 376 Color: 13

Bin 818: 1 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 8
Size: 293 Color: 7

Bin 819: 1 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 7
Size: 388 Color: 11

Bin 820: 1 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 17
Size: 410 Color: 14

Bin 821: 1 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 2
Size: 455 Color: 14

Bin 822: 1 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 11
Size: 457 Color: 7

Bin 823: 1 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 2
Size: 442 Color: 16

Bin 824: 1 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 9
Size: 311 Color: 8

Bin 825: 1 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 8
Size: 378 Color: 11

Bin 826: 1 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 6
Size: 414 Color: 19

Bin 827: 1 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 16
Size: 432 Color: 14

Bin 828: 1 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 0
Size: 403 Color: 16

Bin 829: 1 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 12
Size: 249 Color: 2

Bin 830: 1 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 17
Size: 445 Color: 3

Bin 831: 1 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 15
Size: 467 Color: 11

Bin 832: 1 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 5
Size: 403 Color: 8

Bin 833: 1 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 0
Size: 491 Color: 15

Bin 834: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 19
Size: 427 Color: 11

Bin 835: 1 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 6
Size: 317 Color: 3

Bin 836: 1 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 17
Size: 491 Color: 11

Bin 837: 1 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 8
Size: 332 Color: 5

Bin 838: 1 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 11
Size: 414 Color: 19

Bin 839: 1 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 10
Size: 432 Color: 17

Bin 840: 1 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 4
Size: 276 Color: 3

Bin 841: 1 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 4
Size: 489 Color: 3

Bin 842: 1 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 9
Size: 489 Color: 0

Bin 843: 1 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 6
Size: 475 Color: 11

Bin 844: 1 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 8
Size: 432 Color: 11

Bin 845: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 9
Size: 427 Color: 7

Bin 846: 1 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 7
Size: 393 Color: 15

Bin 847: 1 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 6
Size: 364 Color: 16

Bin 848: 1 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 1
Size: 311 Color: 0

Bin 849: 1 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 5
Size: 222 Color: 6

Bin 850: 1 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 19
Size: 208 Color: 13

Bin 851: 1 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 13
Size: 207 Color: 10

Bin 852: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 14
Size: 199 Color: 11

Bin 853: 2 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 14
Size: 330 Color: 19
Size: 170 Color: 13

Bin 854: 2 of cap free
Amount of items: 3
Items: 
Size: 636 Color: 5
Size: 195 Color: 14
Size: 168 Color: 18

Bin 855: 2 of cap free
Amount of items: 3
Items: 
Size: 642 Color: 1
Size: 197 Color: 19
Size: 160 Color: 11

Bin 856: 2 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 10
Size: 269 Color: 8

Bin 857: 2 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 8
Size: 235 Color: 13

Bin 858: 2 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 435 Color: 13

Bin 859: 2 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 13
Size: 338 Color: 14

Bin 860: 2 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 10
Size: 499 Color: 5

Bin 861: 2 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 19
Size: 235 Color: 15

Bin 862: 2 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 18
Size: 338 Color: 10

Bin 863: 2 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 7
Size: 301 Color: 16

Bin 864: 2 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 15
Size: 278 Color: 17

Bin 865: 2 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 7
Size: 383 Color: 5

Bin 866: 2 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 4
Size: 426 Color: 7

Bin 867: 2 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 8
Size: 402 Color: 18

Bin 868: 2 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 5
Size: 213 Color: 12

Bin 869: 2 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 15
Size: 316 Color: 2

Bin 870: 2 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 15
Size: 464 Color: 12

Bin 871: 2 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 15
Size: 454 Color: 13

Bin 872: 2 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 0
Size: 431 Color: 11

Bin 873: 2 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 15
Size: 444 Color: 16

Bin 874: 2 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 19
Size: 357 Color: 17

Bin 875: 2 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 19
Size: 444 Color: 0

Bin 876: 2 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 0
Size: 493 Color: 3

Bin 877: 3 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 369 Color: 14

Bin 878: 4 of cap free
Amount of items: 3
Items: 
Size: 658 Color: 5
Size: 206 Color: 7
Size: 133 Color: 11

Bin 879: 4 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 10
Size: 287 Color: 9

Bin 880: 8 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 6
Size: 392 Color: 2
Size: 113 Color: 13

Bin 881: 9 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 9
Size: 409 Color: 18

Bin 882: 11 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 19
Size: 334 Color: 5

Bin 883: 13 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 14
Size: 469 Color: 18

Bin 884: 13 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 8
Size: 212 Color: 9

Bin 885: 15 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 19
Size: 229 Color: 2
Size: 116 Color: 7

Bin 886: 15 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 9
Size: 338 Color: 5

Bin 887: 17 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 13
Size: 382 Color: 3
Size: 114 Color: 4

Bin 888: 17 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 7
Size: 233 Color: 13

Bin 889: 23 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 6
Size: 374 Color: 1
Size: 178 Color: 9

Bin 890: 39 of cap free
Amount of items: 6
Items: 
Size: 178 Color: 8
Size: 176 Color: 0
Size: 175 Color: 18
Size: 163 Color: 16
Size: 162 Color: 2
Size: 108 Color: 11

Bin 891: 47 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 2
Size: 186 Color: 17
Size: 109 Color: 11

Bin 892: 55 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 9
Size: 425 Color: 0

Bin 893: 60 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 7
Size: 330 Color: 2

Bin 894: 70 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 6
Size: 180 Color: 5

Bin 895: 75 of cap free
Amount of items: 3
Items: 
Size: 583 Color: 17
Size: 187 Color: 15
Size: 156 Color: 6

Bin 896: 82 of cap free
Amount of items: 3
Items: 
Size: 564 Color: 15
Size: 184 Color: 5
Size: 171 Color: 4

Bin 897: 330 of cap free
Amount of items: 5
Items: 
Size: 163 Color: 6
Size: 150 Color: 3
Size: 141 Color: 10
Size: 111 Color: 4
Size: 106 Color: 11

Total size: 896851
Total free space: 1046

