Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 258 Color: 2
Size: 250 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 363 Color: 1
Size: 252 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 3
Size: 348 Color: 3
Size: 255 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 3
Size: 298 Color: 2
Size: 285 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 4
Size: 313 Color: 3
Size: 271 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 313 Color: 4
Size: 253 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 268 Color: 4
Size: 256 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 356 Color: 4
Size: 268 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 323 Color: 1
Size: 295 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 334 Color: 3
Size: 276 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 321 Color: 2
Size: 299 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 360 Color: 3
Size: 266 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 274 Color: 3
Size: 267 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 3
Size: 297 Color: 0
Size: 290 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 2
Size: 273 Color: 1
Size: 250 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 265 Color: 2
Size: 262 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 0
Size: 274 Color: 4
Size: 250 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 0
Size: 275 Color: 2
Size: 256 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 329 Color: 3
Size: 270 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 4
Size: 281 Color: 3
Size: 281 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 3
Size: 291 Color: 3
Size: 277 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 2
Size: 362 Color: 1
Size: 269 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 259 Color: 2
Size: 257 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 4
Size: 287 Color: 2
Size: 250 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 4
Size: 309 Color: 3
Size: 269 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 4
Size: 278 Color: 1
Size: 257 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 0
Size: 316 Color: 1
Size: 269 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 313 Color: 3
Size: 310 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 2
Size: 274 Color: 4
Size: 259 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 360 Color: 4
Size: 254 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 0
Size: 334 Color: 0
Size: 310 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 356 Color: 3
Size: 261 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 3
Size: 257 Color: 4
Size: 256 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 2
Size: 291 Color: 1
Size: 265 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 323 Color: 1
Size: 271 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 324 Color: 0
Size: 302 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 269 Color: 1
Size: 255 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 347 Color: 2
Size: 270 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 2
Size: 251 Color: 1
Size: 250 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 312 Color: 2
Size: 283 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 4
Size: 336 Color: 1
Size: 296 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 315 Color: 1
Size: 307 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 323 Color: 2
Size: 261 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 263 Color: 2
Size: 257 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 357 Color: 3
Size: 252 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 3
Size: 277 Color: 1
Size: 272 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 2
Size: 254 Color: 0
Size: 250 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 268 Color: 1
Size: 257 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 4
Size: 289 Color: 3
Size: 262 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 303 Color: 1
Size: 250 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 270 Color: 2
Size: 255 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 4
Size: 294 Color: 2
Size: 275 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 3
Size: 292 Color: 2
Size: 252 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 2
Size: 280 Color: 3
Size: 262 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 335 Color: 4
Size: 276 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 0
Size: 299 Color: 4
Size: 286 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 0
Size: 278 Color: 1
Size: 259 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 324 Color: 3
Size: 294 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 4
Size: 362 Color: 2
Size: 256 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 309 Color: 2
Size: 273 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 0
Size: 254 Color: 3
Size: 250 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 3
Size: 276 Color: 4
Size: 273 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 3
Size: 301 Color: 2
Size: 294 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 319 Color: 2
Size: 250 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 3
Size: 283 Color: 1
Size: 279 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 2
Size: 356 Color: 2
Size: 254 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 326 Color: 4
Size: 300 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 4
Size: 271 Color: 4
Size: 250 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 0
Size: 334 Color: 1
Size: 325 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 353 Color: 1
Size: 268 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 255 Color: 4
Size: 250 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 335 Color: 0
Size: 251 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 312 Color: 2
Size: 304 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 256 Color: 2
Size: 255 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 330 Color: 0
Size: 262 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 338 Color: 2
Size: 278 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 260 Color: 3
Size: 252 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 295 Color: 2
Size: 259 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 300 Color: 3
Size: 267 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 3
Size: 291 Color: 1
Size: 289 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 0
Size: 347 Color: 0
Size: 304 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 2
Size: 347 Color: 3
Size: 261 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 326 Color: 3
Size: 264 Color: 2

Total size: 83000
Total free space: 0

