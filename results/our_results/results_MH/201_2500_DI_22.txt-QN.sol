Capicity Bin: 2036
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1408 Color: 158
Size: 316 Color: 97
Size: 224 Color: 78
Size: 52 Color: 26
Size: 36 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 167
Size: 389 Color: 105
Size: 76 Color: 41

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1276 Color: 148
Size: 424 Color: 109
Size: 224 Color: 79
Size: 56 Color: 32
Size: 56 Color: 29

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1814 Color: 198
Size: 198 Color: 75
Size: 16 Color: 2
Size: 8 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1645 Color: 173
Size: 327 Color: 100
Size: 64 Color: 36

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 1682 Color: 178
Size: 326 Color: 99
Size: 20 Color: 4
Size: 8 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 189
Size: 254 Color: 86
Size: 32 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 154
Size: 541 Color: 120
Size: 106 Color: 55

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 187
Size: 247 Color: 85
Size: 48 Color: 21

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 181
Size: 283 Color: 91
Size: 56 Color: 28

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 143
Size: 727 Color: 130
Size: 144 Color: 65

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 188
Size: 246 Color: 84
Size: 48 Color: 23

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 196
Size: 190 Color: 74
Size: 36 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1019 Color: 138
Size: 989 Color: 137
Size: 28 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 176
Size: 298 Color: 94
Size: 76 Color: 42

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1393 Color: 155
Size: 537 Color: 118
Size: 106 Color: 54

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 157
Size: 531 Color: 117
Size: 104 Color: 52

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1022 Color: 140
Size: 890 Color: 136
Size: 124 Color: 59

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1366 Color: 153
Size: 562 Color: 121
Size: 108 Color: 56

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 190
Size: 230 Color: 82
Size: 44 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 191
Size: 227 Color: 81
Size: 44 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 201
Size: 178 Color: 69
Size: 32 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 186
Size: 242 Color: 83
Size: 60 Color: 34

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 142
Size: 810 Color: 132
Size: 160 Color: 66

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 150
Size: 622 Color: 123
Size: 124 Color: 58

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1178 Color: 146
Size: 718 Color: 128
Size: 140 Color: 63

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 168
Size: 362 Color: 103
Size: 72 Color: 40

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 820 Color: 133
Size: 692 Color: 127
Size: 468 Color: 114
Size: 56 Color: 30

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1023 Color: 141
Size: 845 Color: 134
Size: 168 Color: 68

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1289 Color: 149
Size: 623 Color: 124
Size: 124 Color: 60

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 156
Size: 538 Color: 119
Size: 104 Color: 53

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 144
Size: 743 Color: 131
Size: 126 Color: 61

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 200
Size: 182 Color: 70
Size: 36 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 159
Size: 490 Color: 115
Size: 96 Color: 51

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 163
Size: 441 Color: 111
Size: 88 Color: 47

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 162
Size: 442 Color: 112
Size: 88 Color: 49

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 139
Size: 847 Color: 135
Size: 168 Color: 67

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 180
Size: 282 Color: 90
Size: 64 Color: 37

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 165
Size: 407 Color: 108
Size: 80 Color: 45

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 169
Size: 361 Color: 102
Size: 70 Color: 39

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 170
Size: 407 Color: 107
Size: 20 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 171
Size: 367 Color: 104
Size: 52 Color: 27

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 174
Size: 314 Color: 95
Size: 76 Color: 44

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 152
Size: 569 Color: 122
Size: 112 Color: 57

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1620 Color: 172
Size: 348 Color: 101
Size: 68 Color: 38

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 195
Size: 190 Color: 73
Size: 44 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 147
Size: 666 Color: 126
Size: 132 Color: 62

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1566 Color: 166
Size: 394 Color: 106
Size: 76 Color: 43

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1475 Color: 160
Size: 525 Color: 116
Size: 36 Color: 10

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 175
Size: 321 Color: 98
Size: 64 Color: 35

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 145
Size: 721 Color: 129
Size: 144 Color: 64

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 161
Size: 463 Color: 113
Size: 92 Color: 50

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 182
Size: 290 Color: 92
Size: 48 Color: 22

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 179
Size: 293 Color: 93
Size: 58 Color: 33

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 192
Size: 225 Color: 80
Size: 44 Color: 16

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1664 Color: 177
Size: 316 Color: 96
Size: 56 Color: 31

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 151
Size: 657 Color: 125
Size: 88 Color: 48

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 184
Size: 262 Color: 88
Size: 48 Color: 20

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 164
Size: 438 Color: 110
Size: 84 Color: 46

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 185
Size: 257 Color: 87
Size: 50 Color: 24

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1785 Color: 193
Size: 211 Color: 77
Size: 40 Color: 15

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 194
Size: 209 Color: 76
Size: 40 Color: 14

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1719 Color: 183
Size: 265 Color: 89
Size: 52 Color: 25

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 197
Size: 187 Color: 72
Size: 36 Color: 13

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 199
Size: 185 Color: 71
Size: 36 Color: 9

Total size: 132340
Total free space: 0

