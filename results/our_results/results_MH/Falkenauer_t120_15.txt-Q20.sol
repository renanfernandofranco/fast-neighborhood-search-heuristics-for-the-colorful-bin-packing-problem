Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 16
Size: 305 Color: 0
Size: 270 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 19
Size: 270 Color: 9
Size: 261 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 17
Size: 363 Color: 16
Size: 264 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 8
Size: 262 Color: 4
Size: 255 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 258 Color: 7
Size: 255 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 13
Size: 306 Color: 14
Size: 251 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9
Size: 277 Color: 15
Size: 251 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 317 Color: 0
Size: 296 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 16
Size: 306 Color: 7
Size: 294 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 12
Size: 266 Color: 11
Size: 262 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 277 Color: 5
Size: 258 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 362 Color: 15
Size: 266 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 13
Size: 354 Color: 10
Size: 260 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 11
Size: 322 Color: 8
Size: 274 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 10
Size: 274 Color: 11
Size: 263 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 5
Size: 334 Color: 16
Size: 279 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 8
Size: 362 Color: 5
Size: 252 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 18
Size: 332 Color: 13
Size: 303 Color: 9

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 4
Size: 264 Color: 0
Size: 252 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 303 Color: 10
Size: 440 Color: 18
Size: 257 Color: 16

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 280 Color: 0
Size: 267 Color: 5
Size: 453 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 13
Size: 267 Color: 11
Size: 251 Color: 16

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 19
Size: 330 Color: 6
Size: 303 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 4
Size: 271 Color: 7
Size: 250 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 292 Color: 1
Size: 282 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 322 Color: 3
Size: 270 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 19
Size: 304 Color: 11
Size: 250 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 19
Size: 296 Color: 1
Size: 271 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 17
Size: 302 Color: 13
Size: 272 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 17
Size: 363 Color: 15
Size: 274 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 19
Size: 279 Color: 7
Size: 278 Color: 16

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 6
Size: 344 Color: 15
Size: 256 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 14
Size: 337 Color: 11
Size: 285 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 16
Size: 281 Color: 3
Size: 261 Color: 16

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 11
Size: 276 Color: 15
Size: 251 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 5
Size: 307 Color: 10
Size: 250 Color: 14

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 19
Size: 322 Color: 12
Size: 256 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 7
Size: 333 Color: 6
Size: 310 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 14
Size: 320 Color: 7
Size: 269 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 287 Color: 6
Size: 267 Color: 12

Total size: 40000
Total free space: 0

