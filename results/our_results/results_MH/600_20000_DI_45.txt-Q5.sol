Capicity Bin: 16224
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8122 Color: 3
Size: 5891 Color: 1
Size: 1595 Color: 4
Size: 312 Color: 2
Size: 304 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9974 Color: 3
Size: 5930 Color: 1
Size: 320 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11162 Color: 1
Size: 4734 Color: 3
Size: 328 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11464 Color: 0
Size: 4424 Color: 2
Size: 336 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11716 Color: 0
Size: 4056 Color: 3
Size: 452 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11928 Color: 4
Size: 3976 Color: 2
Size: 320 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12036 Color: 4
Size: 3492 Color: 4
Size: 696 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 0
Size: 3764 Color: 0
Size: 276 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12478 Color: 1
Size: 3462 Color: 0
Size: 284 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 0
Size: 3400 Color: 3
Size: 176 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12746 Color: 3
Size: 3122 Color: 4
Size: 356 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12758 Color: 2
Size: 2818 Color: 0
Size: 648 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12960 Color: 3
Size: 2456 Color: 0
Size: 808 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13106 Color: 4
Size: 2578 Color: 0
Size: 540 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13132 Color: 2
Size: 2580 Color: 1
Size: 512 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 3
Size: 2132 Color: 2
Size: 688 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13454 Color: 0
Size: 2562 Color: 4
Size: 208 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 1
Size: 2048 Color: 4
Size: 632 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 0
Size: 2228 Color: 4
Size: 340 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13623 Color: 3
Size: 2061 Color: 1
Size: 540 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13734 Color: 0
Size: 2066 Color: 2
Size: 424 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 3
Size: 2052 Color: 4
Size: 420 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 1
Size: 1924 Color: 0
Size: 536 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13758 Color: 0
Size: 1672 Color: 2
Size: 794 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 2
Size: 2068 Color: 0
Size: 324 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 2
Size: 1922 Color: 4
Size: 432 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13953 Color: 0
Size: 1831 Color: 4
Size: 440 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 2
Size: 1280 Color: 2
Size: 1026 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14004 Color: 0
Size: 1176 Color: 4
Size: 1044 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14084 Color: 0
Size: 1752 Color: 3
Size: 388 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 0
Size: 1486 Color: 3
Size: 624 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14136 Color: 0
Size: 1384 Color: 4
Size: 704 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14162 Color: 0
Size: 1038 Color: 1
Size: 1024 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 0
Size: 1620 Color: 1
Size: 410 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 3
Size: 1692 Color: 2
Size: 336 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14282 Color: 3
Size: 1654 Color: 2
Size: 288 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14284 Color: 4
Size: 1364 Color: 0
Size: 576 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 4
Size: 1484 Color: 0
Size: 448 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14311 Color: 3
Size: 1501 Color: 4
Size: 412 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14320 Color: 4
Size: 1540 Color: 0
Size: 364 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14355 Color: 4
Size: 1559 Color: 2
Size: 310 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14391 Color: 4
Size: 1371 Color: 1
Size: 462 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14398 Color: 3
Size: 1478 Color: 0
Size: 348 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14407 Color: 4
Size: 1385 Color: 0
Size: 432 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14442 Color: 0
Size: 1608 Color: 4
Size: 174 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 4
Size: 1248 Color: 3
Size: 524 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14461 Color: 2
Size: 1471 Color: 0
Size: 292 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14478 Color: 3
Size: 1458 Color: 0
Size: 288 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 3
Size: 1448 Color: 0
Size: 288 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14516 Color: 0
Size: 948 Color: 3
Size: 760 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14536 Color: 1
Size: 896 Color: 3
Size: 792 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14542 Color: 4
Size: 1348 Color: 3
Size: 334 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14578 Color: 2
Size: 1374 Color: 4
Size: 272 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14596 Color: 3
Size: 1344 Color: 1
Size: 284 Color: 0

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 9157 Color: 3
Size: 6748 Color: 2
Size: 318 Color: 0

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 2
Size: 6723 Color: 3
Size: 184 Color: 4

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 2
Size: 3627 Color: 0
Size: 252 Color: 1

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 3
Size: 3109 Color: 3
Size: 370 Color: 2

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12876 Color: 1
Size: 2483 Color: 0
Size: 864 Color: 2

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12978 Color: 0
Size: 2853 Color: 4
Size: 392 Color: 3

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13154 Color: 0
Size: 1893 Color: 2
Size: 1176 Color: 2

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13245 Color: 0
Size: 2666 Color: 1
Size: 312 Color: 4

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13282 Color: 2
Size: 2031 Color: 0
Size: 910 Color: 3

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13447 Color: 3
Size: 2776 Color: 1

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13556 Color: 0
Size: 2211 Color: 2
Size: 456 Color: 4

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 0
Size: 2379 Color: 1
Size: 272 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13571 Color: 2
Size: 2356 Color: 1
Size: 296 Color: 0

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13748 Color: 0
Size: 2075 Color: 1
Size: 400 Color: 3

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13949 Color: 4
Size: 1866 Color: 0
Size: 408 Color: 1

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13994 Color: 1
Size: 1941 Color: 4
Size: 288 Color: 0

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 14125 Color: 4
Size: 1522 Color: 4
Size: 576 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 14215 Color: 3
Size: 1604 Color: 0
Size: 404 Color: 1

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 14122 Color: 1
Size: 1749 Color: 4
Size: 352 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 14563 Color: 1
Size: 1144 Color: 3
Size: 516 Color: 2

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 9528 Color: 0
Size: 5350 Color: 2
Size: 1344 Color: 3

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 9990 Color: 2
Size: 6232 Color: 3

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 13074 Color: 0
Size: 2902 Color: 1
Size: 246 Color: 1

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13426 Color: 1
Size: 2796 Color: 3

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 13510 Color: 0
Size: 2072 Color: 1
Size: 640 Color: 3

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 4
Size: 1842 Color: 2

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 10616 Color: 4
Size: 5221 Color: 0
Size: 384 Color: 3

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 10868 Color: 1
Size: 4497 Color: 3
Size: 856 Color: 0

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 0
Size: 3965 Color: 4
Size: 888 Color: 4

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 11467 Color: 1
Size: 4242 Color: 4
Size: 512 Color: 0

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 13208 Color: 1
Size: 2581 Color: 3
Size: 432 Color: 0

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 13395 Color: 1
Size: 2706 Color: 4
Size: 120 Color: 0

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 1
Size: 2315 Color: 3

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 13997 Color: 4
Size: 2224 Color: 1

Bin 89: 4 of cap free
Amount of items: 7
Items: 
Size: 8117 Color: 3
Size: 1722 Color: 0
Size: 1620 Color: 2
Size: 1548 Color: 3
Size: 1528 Color: 2
Size: 1421 Color: 4
Size: 264 Color: 4

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 8130 Color: 0
Size: 6742 Color: 2
Size: 1348 Color: 3

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 11993 Color: 4
Size: 3971 Color: 1
Size: 256 Color: 0

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 13026 Color: 0
Size: 2890 Color: 4
Size: 304 Color: 2

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 4
Size: 2212 Color: 3

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14232 Color: 3
Size: 1988 Color: 1

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 14308 Color: 3
Size: 1416 Color: 1
Size: 496 Color: 1

Bin 96: 5 of cap free
Amount of items: 3
Items: 
Size: 10753 Color: 0
Size: 5130 Color: 1
Size: 336 Color: 1

Bin 97: 5 of cap free
Amount of items: 3
Items: 
Size: 11134 Color: 1
Size: 4899 Color: 0
Size: 186 Color: 3

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 11658 Color: 1
Size: 4561 Color: 2

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 11985 Color: 3
Size: 4234 Color: 2

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 14519 Color: 4
Size: 1700 Color: 2

Bin 101: 6 of cap free
Amount of items: 18
Items: 
Size: 1180 Color: 3
Size: 1136 Color: 2
Size: 1096 Color: 0
Size: 1072 Color: 3
Size: 1024 Color: 1
Size: 992 Color: 1
Size: 984 Color: 1
Size: 944 Color: 1
Size: 928 Color: 0
Size: 912 Color: 4
Size: 900 Color: 0
Size: 898 Color: 4
Size: 860 Color: 0
Size: 844 Color: 1
Size: 840 Color: 4
Size: 744 Color: 3
Size: 584 Color: 3
Size: 280 Color: 4

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 8648 Color: 2
Size: 6754 Color: 2
Size: 816 Color: 3

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 9138 Color: 1
Size: 6760 Color: 2
Size: 320 Color: 0

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 1
Size: 6328 Color: 4
Size: 570 Color: 2

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 9908 Color: 4
Size: 5894 Color: 1
Size: 416 Color: 0

Bin 106: 6 of cap free
Amount of items: 3
Items: 
Size: 11146 Color: 4
Size: 4608 Color: 2
Size: 464 Color: 0

Bin 107: 6 of cap free
Amount of items: 3
Items: 
Size: 12434 Color: 4
Size: 3512 Color: 2
Size: 272 Color: 0

Bin 108: 6 of cap free
Amount of items: 3
Items: 
Size: 12466 Color: 2
Size: 3368 Color: 3
Size: 384 Color: 0

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 1
Size: 2598 Color: 4
Size: 304 Color: 4

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 13534 Color: 3
Size: 2684 Color: 2

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 13986 Color: 1
Size: 2232 Color: 2

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 14066 Color: 1
Size: 2152 Color: 3

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 14372 Color: 1
Size: 1846 Color: 2

Bin 114: 7 of cap free
Amount of items: 5
Items: 
Size: 8120 Color: 3
Size: 3527 Color: 2
Size: 2248 Color: 0
Size: 1442 Color: 4
Size: 880 Color: 3

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 9752 Color: 0
Size: 5921 Color: 2
Size: 544 Color: 3

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 13789 Color: 3
Size: 2428 Color: 1

Bin 117: 8 of cap free
Amount of items: 3
Items: 
Size: 8732 Color: 2
Size: 6684 Color: 0
Size: 800 Color: 2

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 9636 Color: 1
Size: 6244 Color: 0
Size: 336 Color: 2

Bin 119: 8 of cap free
Amount of items: 3
Items: 
Size: 10530 Color: 0
Size: 5400 Color: 4
Size: 286 Color: 1

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 13082 Color: 2
Size: 3134 Color: 1

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 14454 Color: 3
Size: 1762 Color: 2

Bin 122: 9 of cap free
Amount of items: 7
Items: 
Size: 8116 Color: 1
Size: 1515 Color: 2
Size: 1460 Color: 0
Size: 1428 Color: 0
Size: 1402 Color: 2
Size: 1350 Color: 2
Size: 944 Color: 4

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 4
Size: 5107 Color: 2
Size: 800 Color: 4

Bin 124: 10 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 3
Size: 1694 Color: 1
Size: 44 Color: 2

Bin 125: 11 of cap free
Amount of items: 3
Items: 
Size: 10829 Color: 3
Size: 4932 Color: 0
Size: 452 Color: 1

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 14251 Color: 0
Size: 1962 Color: 2

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 14568 Color: 2
Size: 1645 Color: 3

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 10720 Color: 4
Size: 5492 Color: 1

Bin 129: 12 of cap free
Amount of items: 3
Items: 
Size: 11057 Color: 0
Size: 4307 Color: 1
Size: 848 Color: 2

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 14499 Color: 1
Size: 1713 Color: 4

Bin 131: 13 of cap free
Amount of items: 3
Items: 
Size: 9119 Color: 1
Size: 6744 Color: 4
Size: 348 Color: 3

Bin 132: 13 of cap free
Amount of items: 3
Items: 
Size: 10065 Color: 0
Size: 5768 Color: 4
Size: 378 Color: 2

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 13127 Color: 3
Size: 3084 Color: 2

Bin 134: 13 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 3
Size: 2407 Color: 4
Size: 136 Color: 0

Bin 135: 13 of cap free
Amount of items: 2
Items: 
Size: 14423 Color: 2
Size: 1788 Color: 1

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 9154 Color: 1
Size: 7056 Color: 3

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 11742 Color: 4
Size: 4468 Color: 1

Bin 138: 14 of cap free
Amount of items: 2
Items: 
Size: 12074 Color: 0
Size: 4136 Color: 2

Bin 139: 14 of cap free
Amount of items: 2
Items: 
Size: 14498 Color: 4
Size: 1712 Color: 2

Bin 140: 16 of cap free
Amount of items: 3
Items: 
Size: 10488 Color: 4
Size: 5528 Color: 0
Size: 192 Color: 1

Bin 141: 16 of cap free
Amount of items: 3
Items: 
Size: 12716 Color: 0
Size: 2070 Color: 1
Size: 1422 Color: 4

Bin 142: 17 of cap free
Amount of items: 3
Items: 
Size: 12423 Color: 3
Size: 2600 Color: 0
Size: 1184 Color: 1

Bin 143: 17 of cap free
Amount of items: 2
Items: 
Size: 13753 Color: 1
Size: 2454 Color: 3

Bin 144: 18 of cap free
Amount of items: 3
Items: 
Size: 12142 Color: 3
Size: 3832 Color: 3
Size: 232 Color: 1

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 2
Size: 2520 Color: 3

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 14306 Color: 3
Size: 1900 Color: 4

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 14312 Color: 4
Size: 1894 Color: 3

Bin 148: 19 of cap free
Amount of items: 3
Items: 
Size: 10073 Color: 1
Size: 5764 Color: 4
Size: 368 Color: 0

Bin 149: 19 of cap free
Amount of items: 2
Items: 
Size: 11459 Color: 3
Size: 4746 Color: 4

Bin 150: 19 of cap free
Amount of items: 2
Items: 
Size: 13494 Color: 1
Size: 2711 Color: 3

Bin 151: 19 of cap free
Amount of items: 2
Items: 
Size: 13579 Color: 3
Size: 2626 Color: 2

Bin 152: 19 of cap free
Amount of items: 2
Items: 
Size: 14127 Color: 2
Size: 2078 Color: 3

Bin 153: 20 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 4
Size: 4680 Color: 1
Size: 192 Color: 0

Bin 154: 21 of cap free
Amount of items: 2
Items: 
Size: 9959 Color: 3
Size: 6244 Color: 1

Bin 155: 21 of cap free
Amount of items: 2
Items: 
Size: 12801 Color: 3
Size: 3402 Color: 1

Bin 156: 21 of cap free
Amount of items: 2
Items: 
Size: 14581 Color: 4
Size: 1622 Color: 0

Bin 157: 22 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 1
Size: 2278 Color: 4

Bin 158: 26 of cap free
Amount of items: 2
Items: 
Size: 14029 Color: 4
Size: 2169 Color: 2

Bin 159: 27 of cap free
Amount of items: 2
Items: 
Size: 12929 Color: 4
Size: 3268 Color: 2

Bin 160: 27 of cap free
Amount of items: 2
Items: 
Size: 14522 Color: 2
Size: 1675 Color: 3

Bin 161: 28 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 3
Size: 2008 Color: 1

Bin 162: 28 of cap free
Amount of items: 2
Items: 
Size: 14334 Color: 4
Size: 1862 Color: 2

Bin 163: 30 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 2
Size: 6746 Color: 1
Size: 688 Color: 4

Bin 164: 30 of cap free
Amount of items: 2
Items: 
Size: 14392 Color: 0
Size: 1802 Color: 1

Bin 165: 32 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 4
Size: 2904 Color: 1

Bin 166: 34 of cap free
Amount of items: 3
Items: 
Size: 13170 Color: 2
Size: 2924 Color: 4
Size: 96 Color: 0

Bin 167: 35 of cap free
Amount of items: 3
Items: 
Size: 9236 Color: 1
Size: 6761 Color: 0
Size: 192 Color: 4

Bin 168: 45 of cap free
Amount of items: 2
Items: 
Size: 8456 Color: 1
Size: 7723 Color: 4

Bin 169: 46 of cap free
Amount of items: 2
Items: 
Size: 10981 Color: 3
Size: 5197 Color: 1

Bin 170: 46 of cap free
Amount of items: 2
Items: 
Size: 13844 Color: 1
Size: 2334 Color: 4

Bin 171: 54 of cap free
Amount of items: 3
Items: 
Size: 10837 Color: 2
Size: 5133 Color: 1
Size: 200 Color: 3

Bin 172: 56 of cap free
Amount of items: 2
Items: 
Size: 10120 Color: 1
Size: 6048 Color: 4

Bin 173: 57 of cap free
Amount of items: 2
Items: 
Size: 12429 Color: 1
Size: 3738 Color: 3

Bin 174: 57 of cap free
Amount of items: 2
Items: 
Size: 13004 Color: 1
Size: 3163 Color: 2

Bin 175: 62 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 1
Size: 2602 Color: 3

Bin 176: 64 of cap free
Amount of items: 6
Items: 
Size: 8136 Color: 1
Size: 2242 Color: 3
Size: 1848 Color: 0
Size: 1751 Color: 3
Size: 1439 Color: 4
Size: 744 Color: 4

Bin 177: 66 of cap free
Amount of items: 2
Items: 
Size: 9396 Color: 1
Size: 6762 Color: 0

Bin 178: 66 of cap free
Amount of items: 2
Items: 
Size: 13742 Color: 1
Size: 2416 Color: 4

Bin 179: 67 of cap free
Amount of items: 2
Items: 
Size: 13895 Color: 3
Size: 2262 Color: 1

Bin 180: 72 of cap free
Amount of items: 2
Items: 
Size: 12619 Color: 4
Size: 3533 Color: 2

Bin 181: 75 of cap free
Amount of items: 3
Items: 
Size: 10070 Color: 3
Size: 4222 Color: 0
Size: 1857 Color: 2

Bin 182: 76 of cap free
Amount of items: 2
Items: 
Size: 8132 Color: 4
Size: 8016 Color: 1

Bin 183: 80 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 2
Size: 3240 Color: 4

Bin 184: 86 of cap free
Amount of items: 2
Items: 
Size: 10546 Color: 3
Size: 5592 Color: 1

Bin 185: 88 of cap free
Amount of items: 2
Items: 
Size: 14018 Color: 2
Size: 2118 Color: 1

Bin 186: 103 of cap free
Amount of items: 3
Items: 
Size: 8157 Color: 4
Size: 5906 Color: 3
Size: 2058 Color: 0

Bin 187: 103 of cap free
Amount of items: 2
Items: 
Size: 9989 Color: 4
Size: 6132 Color: 2

Bin 188: 107 of cap free
Amount of items: 2
Items: 
Size: 13112 Color: 3
Size: 3005 Color: 2

Bin 189: 110 of cap free
Amount of items: 2
Items: 
Size: 12308 Color: 3
Size: 3806 Color: 2

Bin 190: 116 of cap free
Amount of items: 2
Items: 
Size: 12024 Color: 2
Size: 4084 Color: 4

Bin 191: 126 of cap free
Amount of items: 3
Items: 
Size: 8138 Color: 3
Size: 6856 Color: 2
Size: 1104 Color: 0

Bin 192: 131 of cap free
Amount of items: 2
Items: 
Size: 12973 Color: 4
Size: 3120 Color: 1

Bin 193: 134 of cap free
Amount of items: 2
Items: 
Size: 12532 Color: 3
Size: 3558 Color: 4

Bin 194: 160 of cap free
Amount of items: 2
Items: 
Size: 11272 Color: 1
Size: 4792 Color: 2

Bin 195: 177 of cap free
Amount of items: 2
Items: 
Size: 10920 Color: 1
Size: 5127 Color: 3

Bin 196: 226 of cap free
Amount of items: 32
Items: 
Size: 784 Color: 2
Size: 706 Color: 1
Size: 680 Color: 4
Size: 672 Color: 1
Size: 624 Color: 4
Size: 608 Color: 1
Size: 584 Color: 2
Size: 576 Color: 4
Size: 576 Color: 4
Size: 552 Color: 0
Size: 532 Color: 0
Size: 516 Color: 0
Size: 508 Color: 0
Size: 496 Color: 3
Size: 488 Color: 3
Size: 488 Color: 1
Size: 480 Color: 4
Size: 480 Color: 0
Size: 464 Color: 2
Size: 464 Color: 2
Size: 450 Color: 2
Size: 440 Color: 3
Size: 434 Color: 3
Size: 412 Color: 3
Size: 408 Color: 4
Size: 408 Color: 2
Size: 392 Color: 1
Size: 384 Color: 3
Size: 376 Color: 1
Size: 364 Color: 0
Size: 348 Color: 2
Size: 304 Color: 0

Bin 197: 250 of cap free
Amount of items: 7
Items: 
Size: 8114 Color: 2
Size: 1348 Color: 1
Size: 1344 Color: 4
Size: 1344 Color: 4
Size: 1344 Color: 0
Size: 1248 Color: 3
Size: 1232 Color: 2

Bin 198: 259 of cap free
Amount of items: 3
Items: 
Size: 8113 Color: 4
Size: 7252 Color: 1
Size: 600 Color: 0

Bin 199: 11958 of cap free
Amount of items: 14
Items: 
Size: 372 Color: 4
Size: 368 Color: 1
Size: 320 Color: 2
Size: 304 Color: 3
Size: 304 Color: 3
Size: 304 Color: 0
Size: 304 Color: 0
Size: 302 Color: 0
Size: 300 Color: 3
Size: 292 Color: 2
Size: 280 Color: 3
Size: 280 Color: 2
Size: 272 Color: 4
Size: 264 Color: 4

Total size: 3212352
Total free space: 16224

