Capicity Bin: 2472
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1388 Color: 17
Size: 620 Color: 1
Size: 384 Color: 12
Size: 64 Color: 8
Size: 16 Color: 5

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1898 Color: 12
Size: 430 Color: 15
Size: 72 Color: 3
Size: 64 Color: 13
Size: 8 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 7
Size: 340 Color: 7
Size: 104 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 11
Size: 500 Color: 2
Size: 96 Color: 3

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1000 Color: 19
Size: 908 Color: 17
Size: 556 Color: 16
Size: 8 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 1704 Color: 11
Size: 504 Color: 12
Size: 176 Color: 11
Size: 88 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 2
Size: 260 Color: 14
Size: 8 Color: 8

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 10
Size: 1018 Color: 10
Size: 200 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 10
Size: 878 Color: 10
Size: 172 Color: 13

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 1732 Color: 11
Size: 412 Color: 3
Size: 272 Color: 3
Size: 56 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 15
Size: 576 Color: 16
Size: 84 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1817 Color: 9
Size: 547 Color: 10
Size: 108 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 19
Size: 840 Color: 7
Size: 80 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2214 Color: 17
Size: 218 Color: 10
Size: 40 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1636 Color: 3
Size: 700 Color: 8
Size: 136 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2082 Color: 9
Size: 326 Color: 1
Size: 64 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 5
Size: 314 Color: 5
Size: 40 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 13
Size: 1031 Color: 5
Size: 204 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 11
Size: 372 Color: 6
Size: 120 Color: 14

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1867 Color: 3
Size: 505 Color: 19
Size: 100 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 17
Size: 802 Color: 4
Size: 108 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 12
Size: 662 Color: 2
Size: 132 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 14
Size: 1022 Color: 9
Size: 204 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 0
Size: 542 Color: 5
Size: 108 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 3
Size: 726 Color: 4
Size: 144 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 16
Size: 236 Color: 14
Size: 40 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 0
Size: 272 Color: 15
Size: 92 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 1
Size: 446 Color: 6
Size: 88 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 11
Size: 585 Color: 19
Size: 116 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 14
Size: 618 Color: 10
Size: 120 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1958 Color: 6
Size: 482 Color: 0
Size: 32 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 12
Size: 643 Color: 2
Size: 128 Color: 9

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1524 Color: 19
Size: 796 Color: 8
Size: 152 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 15
Size: 1030 Color: 3
Size: 204 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 17
Size: 290 Color: 11
Size: 16 Color: 14

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2178 Color: 10
Size: 246 Color: 17
Size: 48 Color: 19

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 9
Size: 308 Color: 18
Size: 96 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 5
Size: 284 Color: 19
Size: 48 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1862 Color: 12
Size: 538 Color: 0
Size: 72 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 4
Size: 1044 Color: 14
Size: 184 Color: 12

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 10
Size: 1029 Color: 12
Size: 204 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 13
Size: 638 Color: 19
Size: 124 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 4
Size: 452 Color: 1
Size: 88 Color: 8

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 17
Size: 398 Color: 13
Size: 76 Color: 8

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 7
Size: 842 Color: 2
Size: 168 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 15
Size: 745 Color: 18
Size: 148 Color: 14

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 19
Size: 386 Color: 6
Size: 24 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 18
Size: 302 Color: 15
Size: 60 Color: 5

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 3
Size: 310 Color: 7
Size: 60 Color: 6

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 15
Size: 779 Color: 10
Size: 154 Color: 11

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 1
Size: 418 Color: 9
Size: 48 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2206 Color: 15
Size: 222 Color: 15
Size: 44 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 7
Size: 378 Color: 6
Size: 72 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1755 Color: 12
Size: 651 Color: 7
Size: 66 Color: 8

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 15
Size: 461 Color: 5
Size: 90 Color: 18

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 9
Size: 238 Color: 5
Size: 44 Color: 18

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 18
Size: 819 Color: 10
Size: 162 Color: 9

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1413 Color: 8
Size: 883 Color: 10
Size: 176 Color: 6

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 1
Size: 481 Color: 19
Size: 94 Color: 17

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 3
Size: 443 Color: 9
Size: 88 Color: 7

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1993 Color: 7
Size: 401 Color: 12
Size: 78 Color: 10

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 2
Size: 393 Color: 10
Size: 78 Color: 19

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 10
Size: 681 Color: 10
Size: 134 Color: 6

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 10
Size: 989 Color: 5
Size: 78 Color: 17

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 4
Size: 230 Color: 5
Size: 44 Color: 12

Total size: 160680
Total free space: 0

