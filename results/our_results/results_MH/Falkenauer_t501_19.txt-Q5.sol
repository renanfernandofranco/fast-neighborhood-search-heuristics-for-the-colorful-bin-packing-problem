Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 274 Color: 4
Size: 251 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 342 Color: 1
Size: 277 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 319 Color: 2
Size: 265 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 0
Size: 257 Color: 1
Size: 262 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 3
Size: 251 Color: 0
Size: 250 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 2
Size: 264 Color: 3
Size: 254 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 289 Color: 4
Size: 282 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 2
Size: 341 Color: 3
Size: 283 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 2
Size: 325 Color: 3
Size: 264 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 0
Size: 282 Color: 2
Size: 263 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 254 Color: 3
Size: 252 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 3
Size: 324 Color: 4
Size: 264 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 2
Size: 328 Color: 1
Size: 275 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 4
Size: 309 Color: 0
Size: 308 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 3
Size: 323 Color: 4
Size: 312 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 312 Color: 2
Size: 273 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 2
Size: 297 Color: 1
Size: 272 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 288 Color: 2
Size: 250 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 3
Size: 317 Color: 1
Size: 271 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 367 Color: 1
Size: 258 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 3
Size: 267 Color: 2
Size: 255 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 279 Color: 2
Size: 267 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 4
Size: 304 Color: 4
Size: 298 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 2
Size: 263 Color: 4
Size: 255 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 268 Color: 0
Size: 257 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 4
Size: 351 Color: 4
Size: 295 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 280 Color: 0
Size: 279 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 2
Size: 265 Color: 1
Size: 258 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 3
Size: 307 Color: 4
Size: 263 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 2
Size: 277 Color: 4
Size: 261 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 3
Size: 258 Color: 0
Size: 250 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 4
Size: 321 Color: 4
Size: 311 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 4
Size: 285 Color: 3
Size: 268 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 260 Color: 0
Size: 250 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 340 Color: 1
Size: 254 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 315 Color: 1
Size: 251 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 292 Color: 4
Size: 288 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 359 Color: 1
Size: 264 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 0
Size: 268 Color: 4
Size: 251 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 250 Color: 4
Size: 256 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 3
Size: 348 Color: 0
Size: 289 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 330 Color: 0
Size: 270 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 370 Color: 2
Size: 255 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 319 Color: 4
Size: 285 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 265 Color: 1
Size: 265 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 358 Color: 2
Size: 264 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 4
Size: 296 Color: 3
Size: 255 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 320 Color: 1
Size: 251 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 4
Size: 347 Color: 1
Size: 264 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 271 Color: 3
Size: 263 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 306 Color: 3
Size: 268 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 4
Size: 279 Color: 2
Size: 267 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 3
Size: 309 Color: 2
Size: 262 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 272 Color: 3
Size: 266 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 352 Color: 2
Size: 251 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 281 Color: 0
Size: 250 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 280 Color: 4
Size: 263 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 303 Color: 1
Size: 263 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 340 Color: 0
Size: 250 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 295 Color: 3
Size: 291 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 361 Color: 4
Size: 266 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 269 Color: 2
Size: 269 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 3
Size: 258 Color: 2
Size: 254 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 286 Color: 2
Size: 254 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 3
Size: 272 Color: 2
Size: 269 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 2
Size: 251 Color: 4
Size: 251 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 2
Size: 261 Color: 4
Size: 257 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 337 Color: 0
Size: 296 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 356 Color: 2
Size: 275 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 254 Color: 0
Size: 252 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 304 Color: 3
Size: 259 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 257 Color: 3
Size: 251 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 0
Size: 267 Color: 4
Size: 256 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 350 Color: 4
Size: 254 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 4
Size: 315 Color: 4
Size: 254 Color: 3

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 312 Color: 1
Size: 299 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 2
Size: 322 Color: 2
Size: 267 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 316 Color: 0
Size: 258 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 3
Size: 265 Color: 0
Size: 254 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 3
Size: 272 Color: 1
Size: 258 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 3
Size: 325 Color: 0
Size: 324 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 264 Color: 2
Size: 252 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 337 Color: 3
Size: 265 Color: 2

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 4
Size: 318 Color: 2
Size: 279 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 3
Size: 294 Color: 4
Size: 274 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 354 Color: 1
Size: 274 Color: 4

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 312 Color: 4
Size: 302 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 3
Size: 298 Color: 0
Size: 259 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 0
Size: 310 Color: 0
Size: 296 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 258 Color: 1
Size: 255 Color: 4

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 4
Size: 323 Color: 0
Size: 264 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 299 Color: 2
Size: 252 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 3
Size: 289 Color: 0
Size: 260 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 3
Size: 319 Color: 4
Size: 265 Color: 4

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 3
Size: 277 Color: 1
Size: 259 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 4
Size: 309 Color: 3
Size: 267 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 3
Size: 341 Color: 0
Size: 255 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 2
Size: 327 Color: 3
Size: 310 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 334 Color: 4
Size: 333 Color: 2
Size: 333 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 2
Size: 268 Color: 0
Size: 263 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 4
Size: 271 Color: 3
Size: 250 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 261 Color: 4
Size: 252 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 343 Color: 4
Size: 252 Color: 4

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 283 Color: 0
Size: 252 Color: 3

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 2
Size: 299 Color: 1
Size: 272 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 336 Color: 2
Size: 269 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 2
Size: 255 Color: 2
Size: 253 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 324 Color: 0
Size: 251 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 2
Size: 253 Color: 3
Size: 252 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 371 Color: 0
Size: 250 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 316 Color: 0
Size: 263 Color: 3

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 274 Color: 0
Size: 260 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 2
Size: 260 Color: 0
Size: 251 Color: 4

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 2
Size: 261 Color: 3
Size: 260 Color: 3

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 337 Color: 2
Size: 254 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 255 Color: 4
Size: 254 Color: 3

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 2
Size: 328 Color: 4
Size: 283 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 4
Size: 272 Color: 0
Size: 264 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 360 Color: 0
Size: 264 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 309 Color: 0
Size: 306 Color: 3

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 4
Size: 340 Color: 4
Size: 304 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 257 Color: 3
Size: 255 Color: 3

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 4
Size: 344 Color: 1
Size: 299 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 2
Size: 251 Color: 3
Size: 250 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 259 Color: 0
Size: 256 Color: 2

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 304 Color: 3
Size: 278 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 2
Size: 303 Color: 1
Size: 274 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 261 Color: 4
Size: 259 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 308 Color: 3
Size: 250 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 265 Color: 3
Size: 273 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 265 Color: 2
Size: 259 Color: 3

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 0
Size: 263 Color: 4
Size: 259 Color: 3

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 4
Size: 257 Color: 2
Size: 255 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 4
Size: 335 Color: 4
Size: 309 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 333 Color: 3
Size: 290 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 339 Color: 2
Size: 252 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 282 Color: 4
Size: 257 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 281 Color: 3
Size: 272 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 4
Size: 304 Color: 0
Size: 253 Color: 3

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 256 Color: 0
Size: 252 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 4
Size: 251 Color: 0
Size: 250 Color: 2

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 364 Color: 0
Size: 253 Color: 2

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 294 Color: 0
Size: 285 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 4
Size: 318 Color: 4
Size: 282 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 2
Size: 278 Color: 4
Size: 277 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 316 Color: 4
Size: 283 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 266 Color: 0
Size: 258 Color: 4

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 4
Size: 273 Color: 4
Size: 256 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 338 Color: 4
Size: 271 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 273 Color: 4
Size: 256 Color: 2

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 350 Color: 1
Size: 277 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 304 Color: 2
Size: 264 Color: 3

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 318 Color: 0
Size: 271 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 0
Size: 352 Color: 2
Size: 289 Color: 2

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 347 Color: 4
Size: 274 Color: 2

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 358 Color: 0
Size: 253 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 317 Color: 0
Size: 254 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 2
Size: 296 Color: 2
Size: 251 Color: 3

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 292 Color: 0
Size: 290 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 3
Size: 316 Color: 4
Size: 288 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 4
Size: 261 Color: 3
Size: 255 Color: 2

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 311 Color: 1
Size: 253 Color: 4

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 353 Color: 2
Size: 272 Color: 2

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 257 Color: 2
Size: 254 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 0
Size: 287 Color: 2
Size: 258 Color: 2

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 336 Color: 2
Size: 252 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 267 Color: 3
Size: 265 Color: 2

Total size: 167000
Total free space: 0

