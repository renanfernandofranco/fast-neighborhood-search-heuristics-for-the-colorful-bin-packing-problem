Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 15
Size: 291 Color: 0
Size: 255 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 14
Size: 341 Color: 5
Size: 275 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 16
Size: 267 Color: 4
Size: 256 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 17
Size: 318 Color: 16
Size: 268 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 17
Size: 295 Color: 11
Size: 261 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 16
Size: 329 Color: 17
Size: 255 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 361 Color: 18
Size: 261 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 360 Color: 5
Size: 253 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 17
Size: 322 Color: 1
Size: 255 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 330 Color: 16
Size: 273 Color: 2
Size: 397 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 18
Size: 291 Color: 2
Size: 251 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 5
Size: 350 Color: 13
Size: 266 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 12
Size: 334 Color: 1
Size: 266 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 297 Color: 19
Size: 297 Color: 16

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 13
Size: 305 Color: 17
Size: 278 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 16
Size: 354 Color: 14
Size: 290 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 15
Size: 337 Color: 11
Size: 256 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 11
Size: 321 Color: 7
Size: 280 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9
Size: 273 Color: 0
Size: 263 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 356 Color: 12
Size: 269 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 14
Size: 267 Color: 8
Size: 261 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 15
Size: 309 Color: 7
Size: 265 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 11
Size: 259 Color: 9
Size: 250 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 8
Size: 264 Color: 18
Size: 263 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 278 Color: 2
Size: 255 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 15
Size: 375 Color: 15
Size: 250 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 13
Size: 302 Color: 6
Size: 290 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 283 Color: 10
Size: 258 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9
Size: 287 Color: 15
Size: 265 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 13
Size: 290 Color: 4
Size: 270 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 3
Size: 272 Color: 10
Size: 267 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 9
Size: 315 Color: 4
Size: 329 Color: 19

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 15
Size: 356 Color: 9
Size: 274 Color: 17

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 10
Size: 313 Color: 2
Size: 283 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 7
Size: 324 Color: 6
Size: 317 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 16
Size: 314 Color: 2
Size: 273 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 12
Size: 323 Color: 6
Size: 299 Color: 11

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 16
Size: 278 Color: 5
Size: 263 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 19
Size: 355 Color: 3
Size: 254 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 8
Size: 323 Color: 7
Size: 305 Color: 18

Total size: 40000
Total free space: 0

