Capicity Bin: 2012
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1194 Color: 9
Size: 754 Color: 10
Size: 64 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 13
Size: 747 Color: 14
Size: 66 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 13
Size: 381 Color: 4
Size: 174 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 13
Size: 329 Color: 7
Size: 80 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 0
Size: 302 Color: 16
Size: 92 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 9
Size: 293 Color: 17
Size: 58 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 14
Size: 261 Color: 4
Size: 82 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 1
Size: 218 Color: 14
Size: 76 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1723 Color: 3
Size: 193 Color: 4
Size: 96 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 3
Size: 221 Color: 10
Size: 42 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 4
Size: 158 Color: 10
Size: 90 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 13
Size: 207 Color: 14
Size: 40 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1785 Color: 4
Size: 191 Color: 0
Size: 36 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 11
Size: 142 Color: 3
Size: 80 Color: 19

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1010 Color: 14
Size: 639 Color: 15
Size: 362 Color: 14

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1349 Color: 9
Size: 622 Color: 0
Size: 40 Color: 10

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1392 Color: 1
Size: 527 Color: 5
Size: 92 Color: 17

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1499 Color: 3
Size: 464 Color: 19
Size: 48 Color: 15

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 19
Size: 420 Color: 12
Size: 36 Color: 12

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 8
Size: 366 Color: 18
Size: 72 Color: 4

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1582 Color: 13
Size: 429 Color: 8

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 12
Size: 341 Color: 6
Size: 16 Color: 7

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 16
Size: 186 Color: 9
Size: 124 Color: 18

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 18
Size: 282 Color: 17

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 9
Size: 221 Color: 12
Size: 36 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 8
Size: 241 Color: 19
Size: 8 Color: 6

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1781 Color: 17
Size: 230 Color: 13

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 10
Size: 164 Color: 4
Size: 52 Color: 2

Bin 29: 2 of cap free
Amount of items: 4
Items: 
Size: 1011 Color: 19
Size: 919 Color: 16
Size: 48 Color: 11
Size: 32 Color: 17

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1111 Color: 19
Size: 839 Color: 10
Size: 60 Color: 10

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 8
Size: 367 Color: 3

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1698 Color: 18
Size: 312 Color: 8

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1807 Color: 3
Size: 203 Color: 13

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 16
Size: 463 Color: 14
Size: 104 Color: 17

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 2
Size: 331 Color: 11

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1747 Color: 16
Size: 262 Color: 1

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 14
Size: 224 Color: 11
Size: 8 Color: 17

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1326 Color: 4
Size: 682 Color: 13

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1502 Color: 19
Size: 506 Color: 10

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1798 Color: 12
Size: 210 Color: 1

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1806 Color: 0
Size: 202 Color: 11

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1522 Color: 19
Size: 485 Color: 7

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1738 Color: 16
Size: 269 Color: 0

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1770 Color: 19
Size: 237 Color: 17

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 1431 Color: 11
Size: 574 Color: 6

Bin 46: 8 of cap free
Amount of items: 3
Items: 
Size: 1110 Color: 3
Size: 838 Color: 18
Size: 56 Color: 13

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1757 Color: 15
Size: 246 Color: 8

Bin 48: 10 of cap free
Amount of items: 3
Items: 
Size: 1159 Color: 17
Size: 791 Color: 10
Size: 52 Color: 11

Bin 49: 12 of cap free
Amount of items: 3
Items: 
Size: 1381 Color: 6
Size: 411 Color: 1
Size: 208 Color: 4

Bin 50: 12 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 10
Size: 426 Color: 5

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1691 Color: 18
Size: 309 Color: 13

Bin 52: 13 of cap free
Amount of items: 2
Items: 
Size: 1521 Color: 1
Size: 478 Color: 19

Bin 53: 14 of cap free
Amount of items: 2
Items: 
Size: 1247 Color: 16
Size: 751 Color: 18

Bin 54: 16 of cap free
Amount of items: 3
Items: 
Size: 1063 Color: 12
Size: 752 Color: 6
Size: 181 Color: 8

Bin 55: 17 of cap free
Amount of items: 3
Items: 
Size: 1260 Color: 14
Size: 671 Color: 1
Size: 64 Color: 17

Bin 56: 17 of cap free
Amount of items: 2
Items: 
Size: 1311 Color: 10
Size: 684 Color: 6

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 1207 Color: 4
Size: 787 Color: 8

Bin 58: 19 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 11
Size: 643 Color: 4
Size: 84 Color: 14

Bin 59: 21 of cap free
Amount of items: 3
Items: 
Size: 1151 Color: 15
Size: 553 Color: 0
Size: 287 Color: 18

Bin 60: 21 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 12
Size: 585 Color: 13

Bin 61: 22 of cap free
Amount of items: 22
Items: 
Size: 166 Color: 18
Size: 150 Color: 15
Size: 148 Color: 11
Size: 136 Color: 12
Size: 126 Color: 11
Size: 116 Color: 10
Size: 112 Color: 2
Size: 110 Color: 6
Size: 100 Color: 13
Size: 84 Color: 11
Size: 76 Color: 6
Size: 74 Color: 0
Size: 72 Color: 9
Size: 72 Color: 8
Size: 68 Color: 19
Size: 68 Color: 1
Size: 56 Color: 18
Size: 56 Color: 16
Size: 52 Color: 17
Size: 52 Color: 4
Size: 50 Color: 9
Size: 46 Color: 4

Bin 62: 22 of cap free
Amount of items: 2
Items: 
Size: 1279 Color: 13
Size: 711 Color: 9

Bin 63: 24 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 7
Size: 189 Color: 10
Size: 182 Color: 3

Bin 64: 28 of cap free
Amount of items: 5
Items: 
Size: 1007 Color: 6
Size: 410 Color: 3
Size: 220 Color: 17
Size: 213 Color: 12
Size: 134 Color: 1

Bin 65: 29 of cap free
Amount of items: 2
Items: 
Size: 1653 Color: 19
Size: 330 Color: 2

Bin 66: 1594 of cap free
Amount of items: 11
Items: 
Size: 44 Color: 17
Size: 44 Color: 9
Size: 42 Color: 0
Size: 40 Color: 12
Size: 40 Color: 2
Size: 40 Color: 1
Size: 38 Color: 14
Size: 36 Color: 19
Size: 32 Color: 5
Size: 32 Color: 4
Size: 30 Color: 18

Total size: 130780
Total free space: 2012

