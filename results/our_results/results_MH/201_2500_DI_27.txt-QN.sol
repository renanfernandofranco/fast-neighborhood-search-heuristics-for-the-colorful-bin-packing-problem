Capicity Bin: 1940
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 1340 Color: 159
Size: 300 Color: 95
Size: 72 Color: 43
Size: 64 Color: 36
Size: 56 Color: 31
Size: 48 Color: 24
Size: 44 Color: 18
Size: 16 Color: 2

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 656 Color: 128
Size: 448 Color: 112
Size: 404 Color: 108
Size: 216 Color: 81
Size: 216 Color: 80

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 163
Size: 442 Color: 110
Size: 84 Color: 49

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 199
Size: 171 Color: 71
Size: 32 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1354 Color: 160
Size: 490 Color: 115
Size: 96 Color: 53

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 164
Size: 466 Color: 114
Size: 52 Color: 28

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1397 Color: 161
Size: 453 Color: 113
Size: 90 Color: 51

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1451 Color: 165
Size: 409 Color: 109
Size: 80 Color: 48

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1639 Color: 183
Size: 251 Color: 89
Size: 50 Color: 25

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 186
Size: 241 Color: 86
Size: 46 Color: 21

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 971 Color: 138
Size: 809 Color: 136
Size: 160 Color: 66

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1259 Color: 152
Size: 569 Color: 121
Size: 112 Color: 60

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 156
Size: 521 Color: 118
Size: 102 Color: 55

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 182
Size: 258 Color: 90
Size: 48 Color: 22

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 166
Size: 403 Color: 107
Size: 80 Color: 46

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1095 Color: 142
Size: 705 Color: 130
Size: 140 Color: 65

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 187
Size: 233 Color: 85
Size: 46 Color: 20

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 172
Size: 333 Color: 99
Size: 66 Color: 39

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1495 Color: 168
Size: 371 Color: 105
Size: 74 Color: 44

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 178
Size: 289 Color: 94
Size: 56 Color: 32

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1715 Color: 194
Size: 191 Color: 75
Size: 34 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1293 Color: 154
Size: 541 Color: 119
Size: 106 Color: 58

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 191
Size: 213 Color: 79
Size: 42 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 173
Size: 334 Color: 100
Size: 64 Color: 38

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1207 Color: 148
Size: 611 Color: 123
Size: 122 Color: 62

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 201
Size: 166 Color: 69
Size: 32 Color: 7

Bin 27: 0 of cap free
Amount of items: 4
Items: 
Size: 1647 Color: 184
Size: 245 Color: 88
Size: 40 Color: 13
Size: 8 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 155
Size: 587 Color: 122
Size: 52 Color: 29

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 177
Size: 303 Color: 96
Size: 60 Color: 33

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1648 Color: 185
Size: 244 Color: 87
Size: 48 Color: 23

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 169
Size: 366 Color: 104
Size: 72 Color: 42

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 146
Size: 646 Color: 127
Size: 128 Color: 63

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 151
Size: 626 Color: 125
Size: 96 Color: 52

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 189
Size: 221 Color: 82
Size: 42 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 174
Size: 327 Color: 98
Size: 64 Color: 37

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 180
Size: 286 Color: 93
Size: 52 Color: 27

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 197
Size: 206 Color: 76
Size: 8 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1533 Color: 171
Size: 341 Color: 101
Size: 66 Color: 40

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 974 Color: 140
Size: 806 Color: 134
Size: 160 Color: 68

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1215 Color: 149
Size: 645 Color: 126
Size: 80 Color: 47

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 190
Size: 222 Color: 83
Size: 40 Color: 14

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1098 Color: 144
Size: 738 Color: 132
Size: 104 Color: 57

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 175
Size: 346 Color: 102
Size: 24 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 973 Color: 139
Size: 807 Color: 135
Size: 160 Color: 67

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 193
Size: 207 Color: 77
Size: 40 Color: 12

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 188
Size: 227 Color: 84
Size: 44 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 181
Size: 270 Color: 91
Size: 52 Color: 26

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1333 Color: 158
Size: 507 Color: 116
Size: 100 Color: 54

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1097 Color: 143
Size: 783 Color: 133
Size: 60 Color: 34

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 192
Size: 210 Color: 78
Size: 40 Color: 11

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1282 Color: 153
Size: 550 Color: 120
Size: 108 Color: 59

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1723 Color: 196
Size: 181 Color: 73
Size: 36 Color: 10

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 176
Size: 309 Color: 97
Size: 60 Color: 35

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 170
Size: 365 Color: 103
Size: 72 Color: 41

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1121 Color: 145
Size: 683 Color: 129
Size: 136 Color: 64

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1601 Color: 179
Size: 283 Color: 92
Size: 56 Color: 30

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 162
Size: 447 Color: 111
Size: 88 Color: 50

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 167
Size: 402 Color: 106
Size: 76 Color: 45

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 147
Size: 619 Color: 124
Size: 122 Color: 61

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1058 Color: 141
Size: 838 Color: 137
Size: 44 Color: 17

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1325 Color: 157
Size: 513 Color: 117
Size: 102 Color: 56

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 200
Size: 170 Color: 70
Size: 32 Color: 5

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 198
Size: 177 Color: 72
Size: 34 Color: 9

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 195
Size: 191 Color: 74
Size: 28 Color: 4

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 1216 Color: 150
Size: 724 Color: 131

Total size: 126100
Total free space: 0

