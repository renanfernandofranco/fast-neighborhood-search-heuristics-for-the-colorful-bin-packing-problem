Capicity Bin: 1001
Lower Bound: 48

Bins used: 51
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 1
Size: 171 Color: 1
Size: 132 Color: 0

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 1
Size: 339 Color: 0

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 0
Size: 346 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 0
Size: 368 Color: 0
Size: 124 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 0
Size: 187 Color: 1
Size: 139 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 327 Color: 0
Size: 261 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 0
Size: 115 Color: 1
Size: 113 Color: 0

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 0
Size: 499 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 505 Color: 0
Size: 388 Color: 0
Size: 108 Color: 1

Bin 10: 1 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 296 Color: 1
Size: 219 Color: 0

Bin 11: 1 of cap free
Amount of items: 3
Items: 
Size: 510 Color: 0
Size: 350 Color: 0
Size: 140 Color: 1

Bin 12: 2 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 1
Size: 318 Color: 0

Bin 13: 2 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 1
Size: 219 Color: 0

Bin 14: 3 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 299 Color: 1

Bin 15: 3 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 0
Size: 393 Color: 1

Bin 16: 3 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 1
Size: 307 Color: 0

Bin 17: 3 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 0
Size: 305 Color: 1

Bin 18: 4 of cap free
Amount of items: 3
Items: 
Size: 700 Color: 1
Size: 181 Color: 0
Size: 116 Color: 1

Bin 19: 5 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 490 Color: 0

Bin 20: 6 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 332 Color: 0
Size: 298 Color: 1

Bin 21: 6 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 1
Size: 184 Color: 1
Size: 112 Color: 0

Bin 22: 8 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 0
Size: 273 Color: 1

Bin 23: 8 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 1
Size: 349 Color: 0

Bin 24: 10 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 0
Size: 376 Color: 1

Bin 25: 12 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 1
Size: 428 Color: 0

Bin 26: 12 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 237 Color: 0

Bin 27: 13 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 188 Color: 0

Bin 28: 15 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 0
Size: 481 Color: 1

Bin 29: 15 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 383 Color: 0

Bin 30: 17 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 1
Size: 459 Color: 0

Bin 31: 18 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 0
Size: 182 Color: 1
Size: 136 Color: 0

Bin 32: 24 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 0
Size: 442 Color: 1

Bin 33: 24 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 0
Size: 414 Color: 1

Bin 34: 25 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 0
Size: 314 Color: 1

Bin 35: 27 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 0
Size: 135 Color: 0
Size: 131 Color: 1

Bin 36: 31 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 1
Size: 426 Color: 0

Bin 37: 39 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 0
Size: 400 Color: 1

Bin 38: 104 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 0
Size: 243 Color: 1

Bin 39: 200 of cap free
Amount of items: 1
Items: 
Size: 801 Color: 0

Bin 40: 206 of cap free
Amount of items: 1
Items: 
Size: 795 Color: 1

Bin 41: 216 of cap free
Amount of items: 1
Items: 
Size: 785 Color: 0

Bin 42: 217 of cap free
Amount of items: 1
Items: 
Size: 784 Color: 0

Bin 43: 219 of cap free
Amount of items: 1
Items: 
Size: 782 Color: 0

Bin 44: 219 of cap free
Amount of items: 1
Items: 
Size: 782 Color: 0

Bin 45: 224 of cap free
Amount of items: 1
Items: 
Size: 777 Color: 0

Bin 46: 225 of cap free
Amount of items: 1
Items: 
Size: 776 Color: 0

Bin 47: 236 of cap free
Amount of items: 1
Items: 
Size: 765 Color: 0

Bin 48: 265 of cap free
Amount of items: 1
Items: 
Size: 736 Color: 1

Bin 49: 311 of cap free
Amount of items: 1
Items: 
Size: 690 Color: 1

Bin 50: 312 of cap free
Amount of items: 1
Items: 
Size: 689 Color: 1

Bin 51: 358 of cap free
Amount of items: 1
Items: 
Size: 643 Color: 0

Total size: 47402
Total free space: 3649

