Capicity Bin: 8136
Lower Bound: 132

Bins used: 132
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4024 Color: 2
Size: 1120 Color: 0
Size: 1008 Color: 3
Size: 656 Color: 1
Size: 520 Color: 2
Size: 448 Color: 2
Size: 360 Color: 2

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 6764 Color: 1
Size: 1148 Color: 4
Size: 104 Color: 3
Size: 80 Color: 1
Size: 40 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5289 Color: 3
Size: 2503 Color: 1
Size: 344 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 0
Size: 2060 Color: 3
Size: 408 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 2
Size: 828 Color: 3
Size: 160 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7316 Color: 1
Size: 684 Color: 4
Size: 136 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5887 Color: 2
Size: 1875 Color: 4
Size: 374 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7011 Color: 0
Size: 1047 Color: 2
Size: 78 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 4
Size: 1052 Color: 4
Size: 208 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 3
Size: 2436 Color: 3
Size: 480 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 0
Size: 1302 Color: 4
Size: 260 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4686 Color: 2
Size: 2890 Color: 1
Size: 560 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6654 Color: 4
Size: 1238 Color: 2
Size: 244 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 1
Size: 804 Color: 4
Size: 152 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 1
Size: 1419 Color: 1
Size: 282 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4086 Color: 1
Size: 3518 Color: 3
Size: 532 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 3
Size: 1124 Color: 0
Size: 216 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 2
Size: 1660 Color: 3
Size: 328 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6113 Color: 3
Size: 1687 Color: 4
Size: 336 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 3
Size: 1412 Color: 4
Size: 264 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6086 Color: 3
Size: 1710 Color: 3
Size: 340 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 1
Size: 3204 Color: 4
Size: 640 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7198 Color: 1
Size: 782 Color: 4
Size: 156 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5643 Color: 0
Size: 2079 Color: 4
Size: 414 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7030 Color: 1
Size: 922 Color: 2
Size: 184 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 4
Size: 1059 Color: 3
Size: 210 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4935 Color: 3
Size: 2669 Color: 2
Size: 532 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 4
Size: 1495 Color: 3
Size: 230 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 1
Size: 1811 Color: 3
Size: 42 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6470 Color: 3
Size: 1390 Color: 1
Size: 276 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5478 Color: 3
Size: 2218 Color: 1
Size: 440 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6945 Color: 3
Size: 993 Color: 3
Size: 198 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4126 Color: 3
Size: 3342 Color: 4
Size: 668 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4150 Color: 1
Size: 3322 Color: 4
Size: 664 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5750 Color: 1
Size: 1990 Color: 2
Size: 396 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4069 Color: 3
Size: 3391 Color: 3
Size: 676 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 2
Size: 1244 Color: 0
Size: 240 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7162 Color: 3
Size: 814 Color: 1
Size: 160 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4557 Color: 3
Size: 2983 Color: 0
Size: 596 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6227 Color: 3
Size: 1591 Color: 0
Size: 318 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7082 Color: 0
Size: 882 Color: 0
Size: 172 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7232 Color: 4
Size: 760 Color: 0
Size: 144 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 0
Size: 2347 Color: 2
Size: 422 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6969 Color: 0
Size: 1149 Color: 3
Size: 18 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5790 Color: 2
Size: 1958 Color: 3
Size: 388 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5388 Color: 0
Size: 2292 Color: 3
Size: 456 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 4
Size: 2684 Color: 2
Size: 536 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5446 Color: 0
Size: 2278 Color: 3
Size: 412 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6799 Color: 0
Size: 1115 Color: 2
Size: 222 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 0
Size: 874 Color: 2
Size: 172 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 2
Size: 2412 Color: 4
Size: 480 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6836 Color: 4
Size: 1084 Color: 2
Size: 216 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 4071 Color: 3
Size: 3389 Color: 0
Size: 676 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 4
Size: 1964 Color: 4
Size: 392 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5667 Color: 0
Size: 2059 Color: 0
Size: 410 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6926 Color: 1
Size: 1010 Color: 2
Size: 200 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 3
Size: 1498 Color: 2
Size: 296 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 2
Size: 1356 Color: 2
Size: 264 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6806 Color: 1
Size: 1110 Color: 2
Size: 220 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 3
Size: 1276 Color: 0
Size: 248 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 4949 Color: 1
Size: 2657 Color: 3
Size: 530 Color: 0

Bin 62: 0 of cap free
Amount of items: 4
Items: 
Size: 6071 Color: 1
Size: 1721 Color: 1
Size: 224 Color: 2
Size: 120 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6846 Color: 0
Size: 1078 Color: 0
Size: 212 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5627 Color: 2
Size: 2091 Color: 4
Size: 418 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6633 Color: 3
Size: 1253 Color: 1
Size: 250 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 4
Size: 2910 Color: 4
Size: 580 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5822 Color: 2
Size: 2022 Color: 4
Size: 292 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 4078 Color: 3
Size: 3382 Color: 2
Size: 676 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 2
Size: 2596 Color: 1
Size: 512 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 1
Size: 1564 Color: 3
Size: 304 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 4
Size: 1932 Color: 3
Size: 384 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7122 Color: 4
Size: 846 Color: 1
Size: 168 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 4
Size: 1278 Color: 0
Size: 252 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6097 Color: 0
Size: 1809 Color: 4
Size: 230 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 4
Size: 710 Color: 0
Size: 140 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4965 Color: 2
Size: 2643 Color: 1
Size: 528 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6510 Color: 1
Size: 1358 Color: 4
Size: 268 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 1
Size: 924 Color: 3
Size: 184 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 1
Size: 1020 Color: 2
Size: 200 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5058 Color: 1
Size: 2566 Color: 4
Size: 512 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5903 Color: 3
Size: 1861 Color: 3
Size: 372 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 7260 Color: 3
Size: 732 Color: 0
Size: 144 Color: 3

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7246 Color: 3
Size: 774 Color: 4
Size: 116 Color: 4

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 2
Size: 932 Color: 1
Size: 184 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5806 Color: 1
Size: 1942 Color: 1
Size: 388 Color: 4

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 1
Size: 1692 Color: 3
Size: 336 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 4
Size: 3052 Color: 3
Size: 608 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5355 Color: 0
Size: 2319 Color: 0
Size: 462 Color: 3

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6542 Color: 2
Size: 1478 Color: 0
Size: 116 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 4
Size: 1272 Color: 2
Size: 344 Color: 4

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6118 Color: 3
Size: 1682 Color: 3
Size: 336 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 3
Size: 2900 Color: 0
Size: 576 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6326 Color: 2
Size: 1510 Color: 2
Size: 300 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 4
Size: 1357 Color: 4
Size: 20 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 5683 Color: 3
Size: 2045 Color: 4
Size: 408 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 5406 Color: 2
Size: 2278 Color: 1
Size: 452 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 3576 Color: 1
Size: 3040 Color: 0
Size: 1520 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6886 Color: 2
Size: 1042 Color: 4
Size: 208 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5462 Color: 4
Size: 2230 Color: 2
Size: 444 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 7302 Color: 1
Size: 698 Color: 3
Size: 136 Color: 4

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6286 Color: 2
Size: 1638 Color: 0
Size: 212 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 6694 Color: 1
Size: 1202 Color: 4
Size: 240 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 7042 Color: 4
Size: 914 Color: 2
Size: 180 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5078 Color: 4
Size: 2550 Color: 0
Size: 508 Color: 4

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 6046 Color: 1
Size: 1742 Color: 3
Size: 348 Color: 3

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 6766 Color: 1
Size: 1142 Color: 2
Size: 228 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6513 Color: 3
Size: 1353 Color: 0
Size: 270 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 7220 Color: 2
Size: 764 Color: 0
Size: 152 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 2
Size: 860 Color: 3
Size: 136 Color: 3

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 1
Size: 790 Color: 4
Size: 156 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 1
Size: 722 Color: 4
Size: 144 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5371 Color: 2
Size: 2305 Color: 0
Size: 460 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6670 Color: 2
Size: 1274 Color: 3
Size: 192 Color: 2

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5118 Color: 1
Size: 2562 Color: 1
Size: 456 Color: 3

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4070 Color: 3
Size: 3390 Color: 2
Size: 676 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 4
Size: 2997 Color: 2
Size: 598 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 7264 Color: 2
Size: 728 Color: 4
Size: 144 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6073 Color: 2
Size: 1721 Color: 4
Size: 342 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 5830 Color: 0
Size: 1922 Color: 1
Size: 384 Color: 2

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6887 Color: 2
Size: 1041 Color: 1
Size: 208 Color: 3

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6579 Color: 2
Size: 1299 Color: 4
Size: 258 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 1
Size: 1468 Color: 0
Size: 288 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6251 Color: 1
Size: 1571 Color: 1
Size: 314 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 3
Size: 748 Color: 3
Size: 144 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 0
Size: 3388 Color: 2
Size: 672 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 0
Size: 1559 Color: 0
Size: 310 Color: 3

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 2
Size: 982 Color: 2
Size: 192 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 2
Size: 2260 Color: 2
Size: 448 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 4
Size: 2568 Color: 3
Size: 496 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6719 Color: 4
Size: 1203 Color: 2
Size: 214 Color: 3

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6471 Color: 1
Size: 1499 Color: 0
Size: 166 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6695 Color: 1
Size: 1247 Color: 2
Size: 194 Color: 0

Total size: 1073952
Total free space: 0

