Capicity Bin: 16608
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9188 Color: 1
Size: 6188 Color: 1
Size: 1232 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 1
Size: 4028 Color: 1
Size: 800 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 13260 Color: 1
Size: 2812 Color: 1
Size: 440 Color: 0
Size: 96 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9655 Color: 1
Size: 5955 Color: 1
Size: 998 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9467 Color: 1
Size: 5951 Color: 1
Size: 1190 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9459 Color: 1
Size: 5959 Color: 1
Size: 1190 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9140 Color: 1
Size: 6228 Color: 1
Size: 1240 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8305 Color: 1
Size: 6921 Color: 1
Size: 1382 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8308 Color: 1
Size: 7260 Color: 1
Size: 1040 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8309 Color: 1
Size: 6917 Color: 1
Size: 1382 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8312 Color: 1
Size: 6916 Color: 1
Size: 1380 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 8313 Color: 1
Size: 6913 Color: 1
Size: 1382 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8314 Color: 1
Size: 6914 Color: 1
Size: 1380 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 1
Size: 6906 Color: 1
Size: 1380 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8328 Color: 1
Size: 6904 Color: 1
Size: 1376 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8330 Color: 1
Size: 6902 Color: 1
Size: 1376 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 9470 Color: 1
Size: 5950 Color: 1
Size: 1188 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 9478 Color: 1
Size: 6144 Color: 1
Size: 986 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 9483 Color: 1
Size: 5939 Color: 1
Size: 1186 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 9486 Color: 1
Size: 5938 Color: 1
Size: 1184 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 9507 Color: 1
Size: 5919 Color: 1
Size: 1182 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 9791 Color: 1
Size: 5681 Color: 1
Size: 1136 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 10024 Color: 1
Size: 5496 Color: 1
Size: 1088 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 10442 Color: 1
Size: 5142 Color: 1
Size: 1024 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 10613 Color: 1
Size: 4997 Color: 1
Size: 998 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 1
Size: 4856 Color: 1
Size: 960 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11086 Color: 1
Size: 4546 Color: 1
Size: 976 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11116 Color: 1
Size: 4580 Color: 1
Size: 912 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 1
Size: 4604 Color: 1
Size: 850 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11344 Color: 1
Size: 4920 Color: 1
Size: 344 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11501 Color: 1
Size: 4257 Color: 1
Size: 850 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11848 Color: 1
Size: 4056 Color: 1
Size: 704 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11994 Color: 1
Size: 4042 Color: 1
Size: 572 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12081 Color: 1
Size: 3773 Color: 1
Size: 754 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12232 Color: 1
Size: 3816 Color: 1
Size: 560 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12270 Color: 1
Size: 3618 Color: 1
Size: 720 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 1
Size: 2988 Color: 1
Size: 1240 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12472 Color: 1
Size: 3656 Color: 1
Size: 480 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12519 Color: 1
Size: 3409 Color: 1
Size: 680 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12760 Color: 1
Size: 3208 Color: 1
Size: 640 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 1
Size: 3148 Color: 1
Size: 624 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12846 Color: 1
Size: 3454 Color: 1
Size: 308 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 1
Size: 3124 Color: 1
Size: 484 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13158 Color: 1
Size: 2886 Color: 1
Size: 564 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13174 Color: 1
Size: 2862 Color: 1
Size: 572 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 1
Size: 2796 Color: 1
Size: 620 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 1
Size: 2430 Color: 1
Size: 956 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13236 Color: 1
Size: 2460 Color: 1
Size: 912 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 1
Size: 2810 Color: 1
Size: 560 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13265 Color: 1
Size: 2833 Color: 1
Size: 510 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 1
Size: 3144 Color: 1
Size: 192 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 1
Size: 2856 Color: 1
Size: 476 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 1
Size: 2740 Color: 1
Size: 544 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13329 Color: 1
Size: 2733 Color: 1
Size: 546 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13345 Color: 1
Size: 2423 Color: 1
Size: 840 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13373 Color: 1
Size: 2697 Color: 1
Size: 538 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13393 Color: 1
Size: 1895 Color: 1
Size: 1320 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13432 Color: 1
Size: 2776 Color: 1
Size: 400 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13437 Color: 1
Size: 2643 Color: 1
Size: 528 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13486 Color: 1
Size: 2594 Color: 1
Size: 528 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13588 Color: 1
Size: 2606 Color: 1
Size: 414 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 1
Size: 2276 Color: 1
Size: 692 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13649 Color: 1
Size: 2703 Color: 1
Size: 256 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 1
Size: 2648 Color: 1
Size: 300 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13694 Color: 1
Size: 2418 Color: 1
Size: 496 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13710 Color: 1
Size: 2082 Color: 1
Size: 816 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 1
Size: 2405 Color: 1
Size: 480 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13724 Color: 1
Size: 2404 Color: 1
Size: 480 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 1
Size: 2288 Color: 1
Size: 520 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 1
Size: 2352 Color: 1
Size: 448 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13866 Color: 1
Size: 2286 Color: 1
Size: 456 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 1
Size: 2278 Color: 1
Size: 452 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13894 Color: 1
Size: 2262 Color: 1
Size: 452 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13901 Color: 1
Size: 2257 Color: 1
Size: 450 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 1
Size: 2228 Color: 1
Size: 440 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 1
Size: 2344 Color: 1
Size: 320 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13957 Color: 1
Size: 2099 Color: 1
Size: 552 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13958 Color: 1
Size: 2210 Color: 1
Size: 440 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 14004 Color: 1
Size: 2260 Color: 1
Size: 344 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 1
Size: 2232 Color: 1
Size: 320 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 14081 Color: 1
Size: 2107 Color: 1
Size: 420 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 14099 Color: 1
Size: 2091 Color: 1
Size: 418 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 1
Size: 2090 Color: 1
Size: 416 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 1
Size: 1710 Color: 1
Size: 784 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 1
Size: 2079 Color: 1
Size: 414 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 14119 Color: 1
Size: 2075 Color: 1
Size: 414 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 14122 Color: 1
Size: 2074 Color: 1
Size: 412 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 14127 Color: 1
Size: 1621 Color: 1
Size: 860 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 1
Size: 2180 Color: 1
Size: 264 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 14186 Color: 1
Size: 2022 Color: 1
Size: 400 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 14212 Color: 1
Size: 2004 Color: 1
Size: 392 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 14228 Color: 1
Size: 1988 Color: 1
Size: 392 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 1
Size: 2056 Color: 1
Size: 304 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 14290 Color: 1
Size: 1934 Color: 1
Size: 384 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 1
Size: 1932 Color: 1
Size: 384 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 14310 Color: 1
Size: 2126 Color: 1
Size: 172 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 14319 Color: 1
Size: 1995 Color: 1
Size: 294 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 1
Size: 1976 Color: 1
Size: 304 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 14374 Color: 1
Size: 1862 Color: 1
Size: 372 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 1
Size: 1642 Color: 1
Size: 576 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 14423 Color: 1
Size: 1821 Color: 1
Size: 364 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 1
Size: 1804 Color: 1
Size: 352 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 14470 Color: 1
Size: 1826 Color: 1
Size: 312 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 1
Size: 1768 Color: 1
Size: 352 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 1
Size: 1742 Color: 1
Size: 344 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14525 Color: 1
Size: 1737 Color: 1
Size: 346 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14556 Color: 1
Size: 1716 Color: 1
Size: 336 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14581 Color: 1
Size: 1691 Color: 1
Size: 336 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14596 Color: 1
Size: 1672 Color: 1
Size: 340 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14601 Color: 1
Size: 1673 Color: 1
Size: 334 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 1
Size: 1800 Color: 1
Size: 192 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 1
Size: 1912 Color: 1
Size: 58 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 1
Size: 1684 Color: 1
Size: 280 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14697 Color: 1
Size: 1699 Color: 1
Size: 212 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14712 Color: 1
Size: 1576 Color: 1
Size: 320 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14714 Color: 1
Size: 1582 Color: 1
Size: 312 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14727 Color: 1
Size: 1649 Color: 1
Size: 232 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 1
Size: 1558 Color: 1
Size: 322 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14740 Color: 1
Size: 1844 Color: 1
Size: 24 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14742 Color: 1
Size: 1448 Color: 1
Size: 418 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14765 Color: 1
Size: 1651 Color: 1
Size: 192 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14769 Color: 1
Size: 1691 Color: 1
Size: 148 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14773 Color: 1
Size: 1531 Color: 1
Size: 304 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14800 Color: 1
Size: 1520 Color: 1
Size: 288 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14812 Color: 1
Size: 1532 Color: 1
Size: 264 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14814 Color: 1
Size: 1498 Color: 1
Size: 296 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14817 Color: 1
Size: 1493 Color: 1
Size: 298 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14824 Color: 1
Size: 1496 Color: 1
Size: 288 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14844 Color: 1
Size: 1476 Color: 1
Size: 288 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14849 Color: 1
Size: 1467 Color: 1
Size: 292 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14886 Color: 1
Size: 1438 Color: 1
Size: 284 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 14888 Color: 1
Size: 1312 Color: 1
Size: 408 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 14908 Color: 1
Size: 1420 Color: 1
Size: 280 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14946 Color: 1
Size: 1386 Color: 1
Size: 276 Color: 0

Bin 135: 1 of cap free
Amount of items: 3
Items: 
Size: 10156 Color: 1
Size: 5763 Color: 1
Size: 688 Color: 0

Bin 136: 1 of cap free
Amount of items: 3
Items: 
Size: 10709 Color: 1
Size: 5210 Color: 1
Size: 688 Color: 0

Bin 137: 1 of cap free
Amount of items: 3
Items: 
Size: 11170 Color: 1
Size: 4301 Color: 1
Size: 1136 Color: 0

Bin 138: 1 of cap free
Amount of items: 3
Items: 
Size: 11804 Color: 1
Size: 4211 Color: 1
Size: 592 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 12883 Color: 1
Size: 3548 Color: 1
Size: 176 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 12977 Color: 1
Size: 3016 Color: 1
Size: 614 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 1
Size: 2983 Color: 1
Size: 328 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 13321 Color: 1
Size: 2822 Color: 1
Size: 464 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 13562 Color: 1
Size: 2677 Color: 1
Size: 368 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 14091 Color: 1
Size: 2068 Color: 1
Size: 448 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 14123 Color: 1
Size: 2244 Color: 1
Size: 240 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 14132 Color: 1
Size: 2211 Color: 1
Size: 264 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 14152 Color: 1
Size: 2071 Color: 1
Size: 384 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 14335 Color: 1
Size: 1644 Color: 1
Size: 628 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 14665 Color: 1
Size: 1564 Color: 1
Size: 378 Color: 0

Bin 150: 2 of cap free
Amount of items: 3
Items: 
Size: 9463 Color: 1
Size: 6103 Color: 1
Size: 1040 Color: 0

Bin 151: 2 of cap free
Amount of items: 3
Items: 
Size: 10861 Color: 1
Size: 4941 Color: 1
Size: 804 Color: 0

Bin 152: 2 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 1
Size: 4602 Color: 1
Size: 912 Color: 0

Bin 153: 2 of cap free
Amount of items: 3
Items: 
Size: 11384 Color: 1
Size: 4318 Color: 1
Size: 904 Color: 0

Bin 154: 2 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 1
Size: 3846 Color: 1
Size: 416 Color: 0

Bin 155: 2 of cap free
Amount of items: 3
Items: 
Size: 12830 Color: 1
Size: 3448 Color: 1
Size: 328 Color: 0

Bin 156: 2 of cap free
Amount of items: 3
Items: 
Size: 13397 Color: 1
Size: 2741 Color: 1
Size: 468 Color: 0

Bin 157: 2 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 1
Size: 2194 Color: 1
Size: 488 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 1
Size: 1376 Color: 0
Size: 1162 Color: 0

Bin 159: 2 of cap free
Amount of items: 2
Items: 
Size: 14558 Color: 1
Size: 2048 Color: 0

Bin 160: 3 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 1
Size: 6607 Color: 1
Size: 1190 Color: 0

Bin 161: 3 of cap free
Amount of items: 3
Items: 
Size: 10260 Color: 1
Size: 5811 Color: 1
Size: 534 Color: 0

Bin 162: 3 of cap free
Amount of items: 3
Items: 
Size: 10609 Color: 1
Size: 5292 Color: 1
Size: 704 Color: 0

Bin 163: 3 of cap free
Amount of items: 3
Items: 
Size: 11447 Color: 1
Size: 4534 Color: 1
Size: 624 Color: 0

Bin 164: 3 of cap free
Amount of items: 3
Items: 
Size: 12137 Color: 1
Size: 4004 Color: 1
Size: 464 Color: 0

Bin 165: 3 of cap free
Amount of items: 3
Items: 
Size: 12466 Color: 1
Size: 3727 Color: 1
Size: 412 Color: 0

Bin 166: 3 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 1
Size: 3150 Color: 1
Size: 890 Color: 0

Bin 167: 3 of cap free
Amount of items: 3
Items: 
Size: 12860 Color: 1
Size: 3073 Color: 1
Size: 672 Color: 0

Bin 168: 3 of cap free
Amount of items: 3
Items: 
Size: 12921 Color: 1
Size: 3138 Color: 1
Size: 546 Color: 0

Bin 169: 3 of cap free
Amount of items: 3
Items: 
Size: 13146 Color: 1
Size: 3027 Color: 1
Size: 432 Color: 0

Bin 170: 3 of cap free
Amount of items: 3
Items: 
Size: 13741 Color: 1
Size: 2496 Color: 1
Size: 368 Color: 0

Bin 171: 3 of cap free
Amount of items: 3
Items: 
Size: 13942 Color: 1
Size: 2391 Color: 1
Size: 272 Color: 0

Bin 172: 4 of cap free
Amount of items: 3
Items: 
Size: 10196 Color: 1
Size: 5688 Color: 1
Size: 720 Color: 0

Bin 173: 4 of cap free
Amount of items: 3
Items: 
Size: 10712 Color: 1
Size: 5332 Color: 1
Size: 560 Color: 0

Bin 174: 4 of cap free
Amount of items: 3
Items: 
Size: 11557 Color: 1
Size: 4791 Color: 1
Size: 256 Color: 0

Bin 175: 4 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 1
Size: 4360 Color: 1
Size: 604 Color: 0

Bin 176: 4 of cap free
Amount of items: 3
Items: 
Size: 12356 Color: 1
Size: 4152 Color: 1
Size: 96 Color: 0

Bin 177: 4 of cap free
Amount of items: 3
Items: 
Size: 12450 Color: 1
Size: 3466 Color: 1
Size: 688 Color: 0

Bin 178: 4 of cap free
Amount of items: 3
Items: 
Size: 14396 Color: 1
Size: 2136 Color: 1
Size: 72 Color: 0

Bin 179: 5 of cap free
Amount of items: 3
Items: 
Size: 9635 Color: 1
Size: 5928 Color: 1
Size: 1040 Color: 0

Bin 180: 5 of cap free
Amount of items: 3
Items: 
Size: 9496 Color: 1
Size: 6187 Color: 1
Size: 920 Color: 0

Bin 181: 5 of cap free
Amount of items: 5
Items: 
Size: 8317 Color: 1
Size: 4026 Color: 1
Size: 3168 Color: 0
Size: 804 Color: 0
Size: 288 Color: 1

Bin 182: 6 of cap free
Amount of items: 3
Items: 
Size: 9394 Color: 1
Size: 6504 Color: 1
Size: 704 Color: 0

Bin 183: 6 of cap free
Amount of items: 3
Items: 
Size: 10358 Color: 1
Size: 5380 Color: 1
Size: 864 Color: 0

Bin 184: 6 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 1
Size: 4400 Color: 1
Size: 440 Color: 0

Bin 185: 7 of cap free
Amount of items: 3
Items: 
Size: 9531 Color: 1
Size: 6014 Color: 1
Size: 1056 Color: 0

Bin 186: 7 of cap free
Amount of items: 3
Items: 
Size: 8681 Color: 1
Size: 7296 Color: 1
Size: 624 Color: 0

Bin 187: 10 of cap free
Amount of items: 3
Items: 
Size: 11778 Color: 1
Size: 3524 Color: 1
Size: 1296 Color: 0

Bin 188: 13 of cap free
Amount of items: 3
Items: 
Size: 8224 Color: 1
Size: 7603 Color: 1
Size: 768 Color: 0

Bin 189: 14 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 1
Size: 5942 Color: 1
Size: 440 Color: 0

Bin 190: 18 of cap free
Amount of items: 3
Items: 
Size: 10681 Color: 1
Size: 5001 Color: 1
Size: 908 Color: 0

Bin 191: 26 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 1
Size: 5348 Color: 1
Size: 800 Color: 0

Bin 192: 37 of cap free
Amount of items: 5
Items: 
Size: 8316 Color: 1
Size: 3105 Color: 1
Size: 2878 Color: 1
Size: 1200 Color: 0
Size: 1072 Color: 0

Bin 193: 52 of cap free
Amount of items: 3
Items: 
Size: 9124 Color: 1
Size: 6244 Color: 1
Size: 1188 Color: 0

Bin 194: 98 of cap free
Amount of items: 3
Items: 
Size: 10545 Color: 1
Size: 5431 Color: 1
Size: 534 Color: 0

Bin 195: 106 of cap free
Amount of items: 3
Items: 
Size: 9784 Color: 1
Size: 5854 Color: 1
Size: 864 Color: 0

Bin 196: 196 of cap free
Amount of items: 3
Items: 
Size: 8306 Color: 1
Size: 6922 Color: 1
Size: 1184 Color: 0

Bin 197: 347 of cap free
Amount of items: 3
Items: 
Size: 11826 Color: 1
Size: 3371 Color: 1
Size: 1064 Color: 0

Bin 198: 4807 of cap free
Amount of items: 3
Items: 
Size: 8056 Color: 1
Size: 2681 Color: 1
Size: 1064 Color: 0

Bin 199: 10738 of cap free
Amount of items: 3
Items: 
Size: 2602 Color: 1
Size: 2524 Color: 1
Size: 744 Color: 0

Total size: 3288384
Total free space: 16608

