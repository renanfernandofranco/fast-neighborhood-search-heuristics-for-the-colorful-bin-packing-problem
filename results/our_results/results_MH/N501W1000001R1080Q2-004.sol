Capicity Bin: 1000001
Lower Bound: 227

Bins used: 228
Amount of Colors: 2

Bin 1: 1 of cap free
Amount of items: 3
Items: 
Size: 738869 Color: 0
Size: 145946 Color: 0
Size: 115185 Color: 1

Bin 2: 4 of cap free
Amount of items: 2
Items: 
Size: 521707 Color: 1
Size: 478290 Color: 0

Bin 3: 6 of cap free
Amount of items: 3
Items: 
Size: 560537 Color: 1
Size: 298527 Color: 1
Size: 140931 Color: 0

Bin 4: 10 of cap free
Amount of items: 3
Items: 
Size: 537213 Color: 0
Size: 299423 Color: 1
Size: 163355 Color: 0

Bin 5: 12 of cap free
Amount of items: 3
Items: 
Size: 437065 Color: 1
Size: 415067 Color: 0
Size: 147857 Color: 0

Bin 6: 13 of cap free
Amount of items: 3
Items: 
Size: 560082 Color: 1
Size: 298066 Color: 0
Size: 141840 Color: 1

Bin 7: 13 of cap free
Amount of items: 3
Items: 
Size: 572893 Color: 0
Size: 302943 Color: 0
Size: 124152 Color: 1

Bin 8: 14 of cap free
Amount of items: 3
Items: 
Size: 558995 Color: 1
Size: 304274 Color: 1
Size: 136718 Color: 0

Bin 9: 28 of cap free
Amount of items: 2
Items: 
Size: 757722 Color: 1
Size: 242251 Color: 0

Bin 10: 31 of cap free
Amount of items: 3
Items: 
Size: 602619 Color: 1
Size: 279642 Color: 0
Size: 117709 Color: 0

Bin 11: 37 of cap free
Amount of items: 3
Items: 
Size: 756396 Color: 0
Size: 136786 Color: 1
Size: 106782 Color: 1

Bin 12: 49 of cap free
Amount of items: 3
Items: 
Size: 653596 Color: 1
Size: 184649 Color: 0
Size: 161707 Color: 1

Bin 13: 51 of cap free
Amount of items: 2
Items: 
Size: 746520 Color: 1
Size: 253430 Color: 0

Bin 14: 52 of cap free
Amount of items: 3
Items: 
Size: 573877 Color: 0
Size: 288879 Color: 0
Size: 137193 Color: 1

Bin 15: 58 of cap free
Amount of items: 2
Items: 
Size: 738903 Color: 1
Size: 261040 Color: 0

Bin 16: 78 of cap free
Amount of items: 3
Items: 
Size: 580881 Color: 0
Size: 300552 Color: 1
Size: 118490 Color: 0

Bin 17: 85 of cap free
Amount of items: 3
Items: 
Size: 572667 Color: 0
Size: 303941 Color: 1
Size: 123308 Color: 0

Bin 18: 98 of cap free
Amount of items: 3
Items: 
Size: 573664 Color: 0
Size: 262995 Color: 1
Size: 163244 Color: 0

Bin 19: 99 of cap free
Amount of items: 3
Items: 
Size: 581009 Color: 0
Size: 293440 Color: 0
Size: 125453 Color: 1

Bin 20: 104 of cap free
Amount of items: 2
Items: 
Size: 705608 Color: 1
Size: 294289 Color: 0

Bin 21: 114 of cap free
Amount of items: 2
Items: 
Size: 601825 Color: 0
Size: 398062 Color: 1

Bin 22: 119 of cap free
Amount of items: 2
Items: 
Size: 763392 Color: 0
Size: 236490 Color: 1

Bin 23: 120 of cap free
Amount of items: 3
Items: 
Size: 738641 Color: 0
Size: 134550 Color: 0
Size: 126690 Color: 1

Bin 24: 121 of cap free
Amount of items: 2
Items: 
Size: 661378 Color: 0
Size: 338502 Color: 1

Bin 25: 127 of cap free
Amount of items: 3
Items: 
Size: 603757 Color: 1
Size: 265001 Color: 0
Size: 131116 Color: 0

Bin 26: 137 of cap free
Amount of items: 2
Items: 
Size: 524851 Color: 1
Size: 475013 Color: 0

Bin 27: 138 of cap free
Amount of items: 2
Items: 
Size: 509821 Color: 0
Size: 490042 Color: 1

Bin 28: 138 of cap free
Amount of items: 2
Items: 
Size: 603395 Color: 1
Size: 396468 Color: 0

Bin 29: 169 of cap free
Amount of items: 2
Items: 
Size: 704909 Color: 1
Size: 294923 Color: 0

Bin 30: 173 of cap free
Amount of items: 3
Items: 
Size: 532421 Color: 1
Size: 304633 Color: 1
Size: 162774 Color: 0

Bin 31: 181 of cap free
Amount of items: 2
Items: 
Size: 588734 Color: 0
Size: 411086 Color: 1

Bin 32: 199 of cap free
Amount of items: 3
Items: 
Size: 577199 Color: 0
Size: 263499 Color: 0
Size: 159104 Color: 1

Bin 33: 230 of cap free
Amount of items: 2
Items: 
Size: 794457 Color: 0
Size: 205314 Color: 1

Bin 34: 232 of cap free
Amount of items: 3
Items: 
Size: 430454 Color: 1
Size: 412891 Color: 0
Size: 156424 Color: 1

Bin 35: 254 of cap free
Amount of items: 2
Items: 
Size: 500313 Color: 0
Size: 499434 Color: 1

Bin 36: 257 of cap free
Amount of items: 3
Items: 
Size: 743679 Color: 0
Size: 143124 Color: 0
Size: 112941 Color: 1

Bin 37: 258 of cap free
Amount of items: 2
Items: 
Size: 747600 Color: 0
Size: 252143 Color: 1

Bin 38: 259 of cap free
Amount of items: 2
Items: 
Size: 579889 Color: 1
Size: 419853 Color: 0

Bin 39: 275 of cap free
Amount of items: 3
Items: 
Size: 569793 Color: 0
Size: 290726 Color: 0
Size: 139207 Color: 1

Bin 40: 279 of cap free
Amount of items: 3
Items: 
Size: 738555 Color: 1
Size: 132660 Color: 0
Size: 128507 Color: 1

Bin 41: 288 of cap free
Amount of items: 2
Items: 
Size: 727689 Color: 0
Size: 272024 Color: 1

Bin 42: 291 of cap free
Amount of items: 2
Items: 
Size: 644827 Color: 0
Size: 354883 Color: 1

Bin 43: 303 of cap free
Amount of items: 3
Items: 
Size: 738235 Color: 1
Size: 141292 Color: 0
Size: 120171 Color: 1

Bin 44: 305 of cap free
Amount of items: 3
Items: 
Size: 626036 Color: 0
Size: 193216 Color: 1
Size: 180444 Color: 1

Bin 45: 354 of cap free
Amount of items: 2
Items: 
Size: 567212 Color: 0
Size: 432435 Color: 1

Bin 46: 393 of cap free
Amount of items: 2
Items: 
Size: 676508 Color: 0
Size: 323100 Color: 1

Bin 47: 404 of cap free
Amount of items: 2
Items: 
Size: 729917 Color: 0
Size: 269680 Color: 1

Bin 48: 406 of cap free
Amount of items: 2
Items: 
Size: 787552 Color: 1
Size: 212043 Color: 0

Bin 49: 415 of cap free
Amount of items: 2
Items: 
Size: 618194 Color: 0
Size: 381392 Color: 1

Bin 50: 422 of cap free
Amount of items: 3
Items: 
Size: 717872 Color: 0
Size: 141432 Color: 1
Size: 140275 Color: 0

Bin 51: 431 of cap free
Amount of items: 2
Items: 
Size: 793763 Color: 1
Size: 205807 Color: 0

Bin 52: 484 of cap free
Amount of items: 2
Items: 
Size: 723868 Color: 0
Size: 275649 Color: 1

Bin 53: 509 of cap free
Amount of items: 2
Items: 
Size: 674944 Color: 0
Size: 324548 Color: 1

Bin 54: 536 of cap free
Amount of items: 3
Items: 
Size: 510559 Color: 0
Size: 309783 Color: 1
Size: 179123 Color: 1

Bin 55: 542 of cap free
Amount of items: 2
Items: 
Size: 612317 Color: 0
Size: 387142 Color: 1

Bin 56: 543 of cap free
Amount of items: 3
Items: 
Size: 628557 Color: 0
Size: 192263 Color: 1
Size: 178638 Color: 0

Bin 57: 552 of cap free
Amount of items: 2
Items: 
Size: 517259 Color: 1
Size: 482190 Color: 0

Bin 58: 563 of cap free
Amount of items: 3
Items: 
Size: 613921 Color: 1
Size: 273727 Color: 0
Size: 111790 Color: 0

Bin 59: 566 of cap free
Amount of items: 2
Items: 
Size: 503865 Color: 0
Size: 495570 Color: 1

Bin 60: 570 of cap free
Amount of items: 2
Items: 
Size: 600822 Color: 1
Size: 398609 Color: 0

Bin 61: 587 of cap free
Amount of items: 3
Items: 
Size: 744010 Color: 1
Size: 145890 Color: 0
Size: 109514 Color: 0

Bin 62: 641 of cap free
Amount of items: 2
Items: 
Size: 569135 Color: 0
Size: 430225 Color: 1

Bin 63: 642 of cap free
Amount of items: 2
Items: 
Size: 741626 Color: 0
Size: 257733 Color: 1

Bin 64: 644 of cap free
Amount of items: 2
Items: 
Size: 539466 Color: 0
Size: 459891 Color: 1

Bin 65: 668 of cap free
Amount of items: 2
Items: 
Size: 522706 Color: 0
Size: 476627 Color: 1

Bin 66: 700 of cap free
Amount of items: 3
Items: 
Size: 639627 Color: 1
Size: 184220 Color: 0
Size: 175454 Color: 1

Bin 67: 723 of cap free
Amount of items: 2
Items: 
Size: 760282 Color: 0
Size: 238996 Color: 1

Bin 68: 747 of cap free
Amount of items: 3
Items: 
Size: 737211 Color: 1
Size: 143025 Color: 0
Size: 119018 Color: 0

Bin 69: 795 of cap free
Amount of items: 2
Items: 
Size: 713037 Color: 0
Size: 286169 Color: 1

Bin 70: 821 of cap free
Amount of items: 3
Items: 
Size: 581445 Color: 0
Size: 301763 Color: 0
Size: 115972 Color: 1

Bin 71: 866 of cap free
Amount of items: 2
Items: 
Size: 788818 Color: 1
Size: 210317 Color: 0

Bin 72: 960 of cap free
Amount of items: 2
Items: 
Size: 705346 Color: 1
Size: 293695 Color: 0

Bin 73: 1009 of cap free
Amount of items: 3
Items: 
Size: 523818 Color: 1
Size: 316049 Color: 1
Size: 159125 Color: 0

Bin 74: 1024 of cap free
Amount of items: 2
Items: 
Size: 562159 Color: 1
Size: 436818 Color: 0

Bin 75: 1046 of cap free
Amount of items: 3
Items: 
Size: 577973 Color: 0
Size: 296790 Color: 1
Size: 124192 Color: 0

Bin 76: 1057 of cap free
Amount of items: 2
Items: 
Size: 750566 Color: 1
Size: 248378 Color: 0

Bin 77: 1058 of cap free
Amount of items: 2
Items: 
Size: 554048 Color: 0
Size: 444895 Color: 1

Bin 78: 1070 of cap free
Amount of items: 2
Items: 
Size: 773654 Color: 0
Size: 225277 Color: 1

Bin 79: 1112 of cap free
Amount of items: 2
Items: 
Size: 557327 Color: 0
Size: 441562 Color: 1

Bin 80: 1137 of cap free
Amount of items: 2
Items: 
Size: 665004 Color: 1
Size: 333860 Color: 0

Bin 81: 1153 of cap free
Amount of items: 2
Items: 
Size: 614536 Color: 1
Size: 384312 Color: 0

Bin 82: 1169 of cap free
Amount of items: 2
Items: 
Size: 658323 Color: 1
Size: 340509 Color: 0

Bin 83: 1206 of cap free
Amount of items: 2
Items: 
Size: 711982 Color: 1
Size: 286813 Color: 0

Bin 84: 1212 of cap free
Amount of items: 2
Items: 
Size: 634867 Color: 0
Size: 363922 Color: 1

Bin 85: 1222 of cap free
Amount of items: 2
Items: 
Size: 770479 Color: 1
Size: 228300 Color: 0

Bin 86: 1256 of cap free
Amount of items: 2
Items: 
Size: 502797 Color: 1
Size: 495948 Color: 0

Bin 87: 1272 of cap free
Amount of items: 2
Items: 
Size: 572950 Color: 1
Size: 425779 Color: 0

Bin 88: 1281 of cap free
Amount of items: 2
Items: 
Size: 749742 Color: 0
Size: 248978 Color: 1

Bin 89: 1330 of cap free
Amount of items: 2
Items: 
Size: 512750 Color: 1
Size: 485921 Color: 0

Bin 90: 1349 of cap free
Amount of items: 2
Items: 
Size: 727920 Color: 1
Size: 270732 Color: 0

Bin 91: 1353 of cap free
Amount of items: 2
Items: 
Size: 794604 Color: 1
Size: 204044 Color: 0

Bin 92: 1364 of cap free
Amount of items: 2
Items: 
Size: 673866 Color: 0
Size: 324771 Color: 1

Bin 93: 1404 of cap free
Amount of items: 2
Items: 
Size: 674618 Color: 1
Size: 323979 Color: 0

Bin 94: 1415 of cap free
Amount of items: 2
Items: 
Size: 692275 Color: 1
Size: 306311 Color: 0

Bin 95: 1513 of cap free
Amount of items: 2
Items: 
Size: 757462 Color: 0
Size: 241026 Color: 1

Bin 96: 1541 of cap free
Amount of items: 2
Items: 
Size: 589387 Color: 0
Size: 409073 Color: 1

Bin 97: 1729 of cap free
Amount of items: 2
Items: 
Size: 762818 Color: 0
Size: 235454 Color: 1

Bin 98: 1755 of cap free
Amount of items: 3
Items: 
Size: 711182 Color: 0
Size: 154061 Color: 1
Size: 133003 Color: 0

Bin 99: 1812 of cap free
Amount of items: 2
Items: 
Size: 639933 Color: 0
Size: 358256 Color: 1

Bin 100: 1813 of cap free
Amount of items: 3
Items: 
Size: 581385 Color: 0
Size: 265610 Color: 0
Size: 151193 Color: 1

Bin 101: 1876 of cap free
Amount of items: 2
Items: 
Size: 732407 Color: 0
Size: 265718 Color: 1

Bin 102: 1883 of cap free
Amount of items: 2
Items: 
Size: 668306 Color: 1
Size: 329812 Color: 0

Bin 103: 1892 of cap free
Amount of items: 2
Items: 
Size: 547085 Color: 0
Size: 451024 Color: 1

Bin 104: 1921 of cap free
Amount of items: 2
Items: 
Size: 781576 Color: 0
Size: 216504 Color: 1

Bin 105: 2081 of cap free
Amount of items: 2
Items: 
Size: 546080 Color: 1
Size: 451840 Color: 0

Bin 106: 2139 of cap free
Amount of items: 2
Items: 
Size: 596539 Color: 1
Size: 401323 Color: 0

Bin 107: 2272 of cap free
Amount of items: 2
Items: 
Size: 513688 Color: 0
Size: 484041 Color: 1

Bin 108: 2306 of cap free
Amount of items: 2
Items: 
Size: 550441 Color: 0
Size: 447254 Color: 1

Bin 109: 2322 of cap free
Amount of items: 2
Items: 
Size: 568925 Color: 1
Size: 428754 Color: 0

Bin 110: 2324 of cap free
Amount of items: 2
Items: 
Size: 612465 Color: 0
Size: 385212 Color: 1

Bin 111: 2327 of cap free
Amount of items: 2
Items: 
Size: 728648 Color: 0
Size: 269026 Color: 1

Bin 112: 2334 of cap free
Amount of items: 2
Items: 
Size: 696904 Color: 1
Size: 300763 Color: 0

Bin 113: 2373 of cap free
Amount of items: 2
Items: 
Size: 608524 Color: 0
Size: 389104 Color: 1

Bin 114: 2438 of cap free
Amount of items: 2
Items: 
Size: 587913 Color: 0
Size: 409650 Color: 1

Bin 115: 2503 of cap free
Amount of items: 2
Items: 
Size: 599460 Color: 0
Size: 398038 Color: 1

Bin 116: 2584 of cap free
Amount of items: 2
Items: 
Size: 782509 Color: 1
Size: 214908 Color: 0

Bin 117: 2709 of cap free
Amount of items: 2
Items: 
Size: 534493 Color: 0
Size: 462799 Color: 1

Bin 118: 2927 of cap free
Amount of items: 2
Items: 
Size: 742466 Color: 1
Size: 254608 Color: 0

Bin 119: 2930 of cap free
Amount of items: 2
Items: 
Size: 709942 Color: 0
Size: 287129 Color: 1

Bin 120: 2961 of cap free
Amount of items: 2
Items: 
Size: 505669 Color: 1
Size: 491371 Color: 0

Bin 121: 3057 of cap free
Amount of items: 2
Items: 
Size: 751558 Color: 0
Size: 245386 Color: 1

Bin 122: 3121 of cap free
Amount of items: 2
Items: 
Size: 798788 Color: 1
Size: 198092 Color: 0

Bin 123: 3165 of cap free
Amount of items: 2
Items: 
Size: 552275 Color: 0
Size: 444561 Color: 1

Bin 124: 3301 of cap free
Amount of items: 2
Items: 
Size: 515994 Color: 0
Size: 480706 Color: 1

Bin 125: 3338 of cap free
Amount of items: 2
Items: 
Size: 532574 Color: 0
Size: 464089 Color: 1

Bin 126: 3345 of cap free
Amount of items: 2
Items: 
Size: 616655 Color: 0
Size: 380001 Color: 1

Bin 127: 3364 of cap free
Amount of items: 2
Items: 
Size: 651371 Color: 0
Size: 345266 Color: 1

Bin 128: 3366 of cap free
Amount of items: 2
Items: 
Size: 760134 Color: 1
Size: 236501 Color: 0

Bin 129: 3489 of cap free
Amount of items: 2
Items: 
Size: 716186 Color: 1
Size: 280326 Color: 0

Bin 130: 3569 of cap free
Amount of items: 3
Items: 
Size: 433105 Color: 1
Size: 294888 Color: 0
Size: 268439 Color: 0

Bin 131: 3601 of cap free
Amount of items: 3
Items: 
Size: 660365 Color: 1
Size: 169754 Color: 1
Size: 166281 Color: 0

Bin 132: 3743 of cap free
Amount of items: 2
Items: 
Size: 611513 Color: 0
Size: 384745 Color: 1

Bin 133: 3766 of cap free
Amount of items: 2
Items: 
Size: 592126 Color: 0
Size: 404109 Color: 1

Bin 134: 3801 of cap free
Amount of items: 2
Items: 
Size: 539053 Color: 1
Size: 457147 Color: 0

Bin 135: 3815 of cap free
Amount of items: 2
Items: 
Size: 589814 Color: 1
Size: 406372 Color: 0

Bin 136: 3881 of cap free
Amount of items: 2
Items: 
Size: 700164 Color: 1
Size: 295956 Color: 0

Bin 137: 4000 of cap free
Amount of items: 2
Items: 
Size: 542437 Color: 0
Size: 453564 Color: 1

Bin 138: 4112 of cap free
Amount of items: 2
Items: 
Size: 596964 Color: 0
Size: 398925 Color: 1

Bin 139: 4147 of cap free
Amount of items: 2
Items: 
Size: 589939 Color: 1
Size: 405915 Color: 0

Bin 140: 4201 of cap free
Amount of items: 2
Items: 
Size: 607718 Color: 1
Size: 388082 Color: 0

Bin 141: 4257 of cap free
Amount of items: 2
Items: 
Size: 544765 Color: 0
Size: 450979 Color: 1

Bin 142: 4325 of cap free
Amount of items: 2
Items: 
Size: 754063 Color: 1
Size: 241613 Color: 0

Bin 143: 4328 of cap free
Amount of items: 2
Items: 
Size: 716645 Color: 1
Size: 279028 Color: 0

Bin 144: 4648 of cap free
Amount of items: 2
Items: 
Size: 524700 Color: 0
Size: 470653 Color: 1

Bin 145: 4665 of cap free
Amount of items: 2
Items: 
Size: 510676 Color: 1
Size: 484660 Color: 0

Bin 146: 4731 of cap free
Amount of items: 2
Items: 
Size: 730441 Color: 0
Size: 264829 Color: 1

Bin 147: 4839 of cap free
Amount of items: 2
Items: 
Size: 657636 Color: 1
Size: 337526 Color: 0

Bin 148: 4848 of cap free
Amount of items: 2
Items: 
Size: 701716 Color: 0
Size: 293437 Color: 1

Bin 149: 4852 of cap free
Amount of items: 2
Items: 
Size: 621454 Color: 1
Size: 373695 Color: 0

Bin 150: 4870 of cap free
Amount of items: 2
Items: 
Size: 792098 Color: 1
Size: 203033 Color: 0

Bin 151: 4893 of cap free
Amount of items: 2
Items: 
Size: 754861 Color: 1
Size: 240247 Color: 0

Bin 152: 5096 of cap free
Amount of items: 2
Items: 
Size: 687555 Color: 0
Size: 307350 Color: 1

Bin 153: 5120 of cap free
Amount of items: 2
Items: 
Size: 647197 Color: 1
Size: 347684 Color: 0

Bin 154: 5168 of cap free
Amount of items: 2
Items: 
Size: 743295 Color: 0
Size: 251538 Color: 1

Bin 155: 5172 of cap free
Amount of items: 2
Items: 
Size: 517835 Color: 1
Size: 476994 Color: 0

Bin 156: 5229 of cap free
Amount of items: 2
Items: 
Size: 683106 Color: 1
Size: 311666 Color: 0

Bin 157: 5407 of cap free
Amount of items: 2
Items: 
Size: 628490 Color: 1
Size: 366104 Color: 0

Bin 158: 5408 of cap free
Amount of items: 2
Items: 
Size: 653938 Color: 1
Size: 340655 Color: 0

Bin 159: 5480 of cap free
Amount of items: 2
Items: 
Size: 569445 Color: 1
Size: 425076 Color: 0

Bin 160: 5549 of cap free
Amount of items: 2
Items: 
Size: 760079 Color: 1
Size: 234373 Color: 0

Bin 161: 5569 of cap free
Amount of items: 2
Items: 
Size: 663900 Color: 0
Size: 330532 Color: 1

Bin 162: 5695 of cap free
Amount of items: 2
Items: 
Size: 500814 Color: 0
Size: 493492 Color: 1

Bin 163: 5867 of cap free
Amount of items: 2
Items: 
Size: 677016 Color: 1
Size: 317118 Color: 0

Bin 164: 5871 of cap free
Amount of items: 2
Items: 
Size: 776409 Color: 1
Size: 217721 Color: 0

Bin 165: 5873 of cap free
Amount of items: 2
Items: 
Size: 580666 Color: 1
Size: 413462 Color: 0

Bin 166: 5920 of cap free
Amount of items: 2
Items: 
Size: 679721 Color: 0
Size: 314360 Color: 1

Bin 167: 5959 of cap free
Amount of items: 2
Items: 
Size: 518099 Color: 0
Size: 475943 Color: 1

Bin 168: 6042 of cap free
Amount of items: 2
Items: 
Size: 646632 Color: 1
Size: 347327 Color: 0

Bin 169: 6059 of cap free
Amount of items: 2
Items: 
Size: 753875 Color: 1
Size: 240067 Color: 0

Bin 170: 6326 of cap free
Amount of items: 2
Items: 
Size: 580548 Color: 1
Size: 413127 Color: 0

Bin 171: 6345 of cap free
Amount of items: 2
Items: 
Size: 627205 Color: 1
Size: 366451 Color: 0

Bin 172: 6480 of cap free
Amount of items: 2
Items: 
Size: 624325 Color: 0
Size: 369196 Color: 1

Bin 173: 6560 of cap free
Amount of items: 2
Items: 
Size: 692531 Color: 1
Size: 300910 Color: 0

Bin 174: 6661 of cap free
Amount of items: 2
Items: 
Size: 668734 Color: 1
Size: 324606 Color: 0

Bin 175: 6768 of cap free
Amount of items: 2
Items: 
Size: 741151 Color: 1
Size: 252082 Color: 0

Bin 176: 7581 of cap free
Amount of items: 2
Items: 
Size: 655440 Color: 0
Size: 336980 Color: 1

Bin 177: 7791 of cap free
Amount of items: 2
Items: 
Size: 635509 Color: 0
Size: 356701 Color: 1

Bin 178: 8034 of cap free
Amount of items: 2
Items: 
Size: 648721 Color: 0
Size: 343246 Color: 1

Bin 179: 8079 of cap free
Amount of items: 2
Items: 
Size: 534980 Color: 1
Size: 456942 Color: 0

Bin 180: 8436 of cap free
Amount of items: 2
Items: 
Size: 605324 Color: 1
Size: 386241 Color: 0

Bin 181: 8539 of cap free
Amount of items: 2
Items: 
Size: 528180 Color: 1
Size: 463282 Color: 0

Bin 182: 8616 of cap free
Amount of items: 2
Items: 
Size: 665339 Color: 0
Size: 326046 Color: 1

Bin 183: 8999 of cap free
Amount of items: 2
Items: 
Size: 522026 Color: 0
Size: 468976 Color: 1

Bin 184: 9094 of cap free
Amount of items: 2
Items: 
Size: 605845 Color: 0
Size: 385062 Color: 1

Bin 185: 9515 of cap free
Amount of items: 2
Items: 
Size: 661871 Color: 0
Size: 328615 Color: 1

Bin 186: 9820 of cap free
Amount of items: 2
Items: 
Size: 591714 Color: 0
Size: 398467 Color: 1

Bin 187: 10312 of cap free
Amount of items: 2
Items: 
Size: 605106 Color: 0
Size: 384583 Color: 1

Bin 188: 10602 of cap free
Amount of items: 2
Items: 
Size: 759614 Color: 0
Size: 229785 Color: 1

Bin 189: 10880 of cap free
Amount of items: 2
Items: 
Size: 623640 Color: 1
Size: 365481 Color: 0

Bin 190: 11113 of cap free
Amount of items: 2
Items: 
Size: 568694 Color: 1
Size: 420194 Color: 0

Bin 191: 11383 of cap free
Amount of items: 2
Items: 
Size: 500757 Color: 0
Size: 487861 Color: 1

Bin 192: 11491 of cap free
Amount of items: 2
Items: 
Size: 680059 Color: 1
Size: 308451 Color: 0

Bin 193: 11961 of cap free
Amount of items: 2
Items: 
Size: 622987 Color: 0
Size: 365053 Color: 1

Bin 194: 12158 of cap free
Amount of items: 2
Items: 
Size: 753214 Color: 1
Size: 234629 Color: 0

Bin 195: 12220 of cap free
Amount of items: 2
Items: 
Size: 566961 Color: 1
Size: 420820 Color: 0

Bin 196: 12499 of cap free
Amount of items: 2
Items: 
Size: 564555 Color: 1
Size: 422947 Color: 0

Bin 197: 12924 of cap free
Amount of items: 2
Items: 
Size: 790398 Color: 1
Size: 196679 Color: 0

Bin 198: 12969 of cap free
Amount of items: 2
Items: 
Size: 787524 Color: 0
Size: 199508 Color: 1

Bin 199: 13212 of cap free
Amount of items: 2
Items: 
Size: 588810 Color: 1
Size: 397979 Color: 0

Bin 200: 13409 of cap free
Amount of items: 2
Items: 
Size: 542174 Color: 0
Size: 444418 Color: 1

Bin 201: 13452 of cap free
Amount of items: 2
Items: 
Size: 675946 Color: 0
Size: 310603 Color: 1

Bin 202: 13474 of cap free
Amount of items: 2
Items: 
Size: 563195 Color: 1
Size: 423332 Color: 0

Bin 203: 13756 of cap free
Amount of items: 2
Items: 
Size: 721752 Color: 0
Size: 264493 Color: 1

Bin 204: 16388 of cap free
Amount of items: 2
Items: 
Size: 541566 Color: 0
Size: 442047 Color: 1

Bin 205: 16516 of cap free
Amount of items: 2
Items: 
Size: 768788 Color: 1
Size: 214697 Color: 0

Bin 206: 18210 of cap free
Amount of items: 2
Items: 
Size: 790310 Color: 1
Size: 191481 Color: 0

Bin 207: 18370 of cap free
Amount of items: 2
Items: 
Size: 783843 Color: 0
Size: 197788 Color: 1

Bin 208: 18514 of cap free
Amount of items: 3
Items: 
Size: 518646 Color: 0
Size: 316160 Color: 1
Size: 146681 Color: 0

Bin 209: 19623 of cap free
Amount of items: 2
Items: 
Size: 616608 Color: 1
Size: 363770 Color: 0

Bin 210: 19951 of cap free
Amount of items: 3
Items: 
Size: 503025 Color: 0
Size: 290675 Color: 0
Size: 186350 Color: 1

Bin 211: 20544 of cap free
Amount of items: 2
Items: 
Size: 531907 Color: 1
Size: 447550 Color: 0

Bin 212: 21741 of cap free
Amount of items: 2
Items: 
Size: 581742 Color: 0
Size: 396518 Color: 1

Bin 213: 22423 of cap free
Amount of items: 2
Items: 
Size: 713897 Color: 0
Size: 263681 Color: 1

Bin 214: 24117 of cap free
Amount of items: 2
Items: 
Size: 561792 Color: 1
Size: 414092 Color: 0

Bin 215: 24738 of cap free
Amount of items: 2
Items: 
Size: 788263 Color: 1
Size: 187000 Color: 0

Bin 216: 25634 of cap free
Amount of items: 2
Items: 
Size: 782177 Color: 0
Size: 192190 Color: 1

Bin 217: 27364 of cap free
Amount of items: 2
Items: 
Size: 640609 Color: 1
Size: 332028 Color: 0

Bin 218: 29478 of cap free
Amount of items: 2
Items: 
Size: 785758 Color: 1
Size: 184765 Color: 0

Bin 219: 30398 of cap free
Amount of items: 2
Items: 
Size: 668250 Color: 1
Size: 301353 Color: 0

Bin 220: 31345 of cap free
Amount of items: 2
Items: 
Size: 531750 Color: 0
Size: 436906 Color: 1

Bin 221: 31769 of cap free
Amount of items: 2
Items: 
Size: 523201 Color: 1
Size: 445031 Color: 0

Bin 222: 34636 of cap free
Amount of items: 3
Items: 
Size: 500896 Color: 0
Size: 299939 Color: 1
Size: 164530 Color: 1

Bin 223: 39537 of cap free
Amount of items: 2
Items: 
Size: 522257 Color: 0
Size: 438207 Color: 1

Bin 224: 40997 of cap free
Amount of items: 4
Items: 
Size: 470892 Color: 0
Size: 181499 Color: 1
Size: 174658 Color: 1
Size: 131955 Color: 0

Bin 225: 44148 of cap free
Amount of items: 2
Items: 
Size: 770571 Color: 0
Size: 185282 Color: 1

Bin 226: 49519 of cap free
Amount of items: 2
Items: 
Size: 509362 Color: 0
Size: 441120 Color: 1

Bin 227: 50143 of cap free
Amount of items: 2
Items: 
Size: 647621 Color: 0
Size: 302237 Color: 1

Bin 228: 50591 of cap free
Amount of items: 3
Items: 
Size: 430894 Color: 0
Size: 352834 Color: 0
Size: 165682 Color: 1

Total size: 226602095
Total free space: 1398133

