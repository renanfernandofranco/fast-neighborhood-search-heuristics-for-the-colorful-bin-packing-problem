Capicity Bin: 1000001
Lower Bound: 229

Bins used: 230
Amount of Colors: 5

Bin 1: 2 of cap free
Amount of items: 3
Items: 
Size: 705750 Color: 4
Size: 157554 Color: 0
Size: 136695 Color: 3

Bin 2: 3 of cap free
Amount of items: 3
Items: 
Size: 595779 Color: 0
Size: 216569 Color: 0
Size: 187650 Color: 1

Bin 3: 8 of cap free
Amount of items: 2
Items: 
Size: 571262 Color: 4
Size: 428731 Color: 3

Bin 4: 14 of cap free
Amount of items: 3
Items: 
Size: 715836 Color: 3
Size: 170024 Color: 4
Size: 114127 Color: 2

Bin 5: 15 of cap free
Amount of items: 3
Items: 
Size: 610792 Color: 1
Size: 205081 Color: 2
Size: 184113 Color: 1

Bin 6: 26 of cap free
Amount of items: 3
Items: 
Size: 692537 Color: 0
Size: 187602 Color: 1
Size: 119836 Color: 1

Bin 7: 29 of cap free
Amount of items: 3
Items: 
Size: 616639 Color: 0
Size: 195618 Color: 2
Size: 187715 Color: 4

Bin 8: 29 of cap free
Amount of items: 3
Items: 
Size: 710026 Color: 0
Size: 171861 Color: 1
Size: 118085 Color: 3

Bin 9: 34 of cap free
Amount of items: 2
Items: 
Size: 698103 Color: 0
Size: 301864 Color: 2

Bin 10: 36 of cap free
Amount of items: 2
Items: 
Size: 794084 Color: 1
Size: 205881 Color: 3

Bin 11: 45 of cap free
Amount of items: 3
Items: 
Size: 587665 Color: 2
Size: 208854 Color: 0
Size: 203437 Color: 1

Bin 12: 51 of cap free
Amount of items: 3
Items: 
Size: 672256 Color: 1
Size: 184647 Color: 0
Size: 143047 Color: 4

Bin 13: 54 of cap free
Amount of items: 2
Items: 
Size: 672871 Color: 4
Size: 327076 Color: 0

Bin 14: 59 of cap free
Amount of items: 2
Items: 
Size: 630341 Color: 0
Size: 369601 Color: 3

Bin 15: 63 of cap free
Amount of items: 2
Items: 
Size: 776983 Color: 3
Size: 222955 Color: 0

Bin 16: 69 of cap free
Amount of items: 3
Items: 
Size: 573852 Color: 3
Size: 270137 Color: 0
Size: 155943 Color: 1

Bin 17: 70 of cap free
Amount of items: 3
Items: 
Size: 565380 Color: 3
Size: 270189 Color: 4
Size: 164362 Color: 3

Bin 18: 70 of cap free
Amount of items: 2
Items: 
Size: 785732 Color: 3
Size: 214199 Color: 2

Bin 19: 72 of cap free
Amount of items: 2
Items: 
Size: 585962 Color: 1
Size: 413967 Color: 0

Bin 20: 78 of cap free
Amount of items: 2
Items: 
Size: 503486 Color: 3
Size: 496437 Color: 4

Bin 21: 82 of cap free
Amount of items: 3
Items: 
Size: 674162 Color: 0
Size: 174733 Color: 0
Size: 151024 Color: 4

Bin 22: 84 of cap free
Amount of items: 2
Items: 
Size: 552875 Color: 1
Size: 447042 Color: 4

Bin 23: 94 of cap free
Amount of items: 3
Items: 
Size: 566514 Color: 3
Size: 253585 Color: 0
Size: 179808 Color: 2

Bin 24: 94 of cap free
Amount of items: 3
Items: 
Size: 354166 Color: 3
Size: 349201 Color: 0
Size: 296540 Color: 1

Bin 25: 114 of cap free
Amount of items: 2
Items: 
Size: 719850 Color: 0
Size: 280037 Color: 4

Bin 26: 118 of cap free
Amount of items: 2
Items: 
Size: 781389 Color: 4
Size: 218494 Color: 2

Bin 27: 125 of cap free
Amount of items: 3
Items: 
Size: 480357 Color: 0
Size: 356587 Color: 4
Size: 162932 Color: 0

Bin 28: 127 of cap free
Amount of items: 3
Items: 
Size: 676054 Color: 2
Size: 175000 Color: 0
Size: 148820 Color: 2

Bin 29: 128 of cap free
Amount of items: 2
Items: 
Size: 570719 Color: 3
Size: 429154 Color: 2

Bin 30: 135 of cap free
Amount of items: 3
Items: 
Size: 439765 Color: 1
Size: 366692 Color: 4
Size: 193409 Color: 2

Bin 31: 137 of cap free
Amount of items: 3
Items: 
Size: 572978 Color: 1
Size: 253040 Color: 3
Size: 173846 Color: 0

Bin 32: 141 of cap free
Amount of items: 3
Items: 
Size: 468919 Color: 1
Size: 352997 Color: 4
Size: 177944 Color: 0

Bin 33: 142 of cap free
Amount of items: 3
Items: 
Size: 734742 Color: 1
Size: 152419 Color: 0
Size: 112698 Color: 1

Bin 34: 146 of cap free
Amount of items: 2
Items: 
Size: 645083 Color: 2
Size: 354772 Color: 3

Bin 35: 147 of cap free
Amount of items: 2
Items: 
Size: 709712 Color: 1
Size: 290142 Color: 4

Bin 36: 161 of cap free
Amount of items: 3
Items: 
Size: 567708 Color: 3
Size: 269951 Color: 0
Size: 162181 Color: 3

Bin 37: 167 of cap free
Amount of items: 2
Items: 
Size: 573242 Color: 0
Size: 426592 Color: 4

Bin 38: 168 of cap free
Amount of items: 3
Items: 
Size: 663285 Color: 3
Size: 178180 Color: 3
Size: 158368 Color: 1

Bin 39: 197 of cap free
Amount of items: 2
Items: 
Size: 582267 Color: 0
Size: 417537 Color: 4

Bin 40: 206 of cap free
Amount of items: 2
Items: 
Size: 614690 Color: 2
Size: 385105 Color: 0

Bin 41: 209 of cap free
Amount of items: 2
Items: 
Size: 768992 Color: 3
Size: 230800 Color: 4

Bin 42: 216 of cap free
Amount of items: 2
Items: 
Size: 759512 Color: 1
Size: 240273 Color: 4

Bin 43: 252 of cap free
Amount of items: 2
Items: 
Size: 662129 Color: 3
Size: 337620 Color: 0

Bin 44: 256 of cap free
Amount of items: 2
Items: 
Size: 636185 Color: 2
Size: 363560 Color: 0

Bin 45: 260 of cap free
Amount of items: 2
Items: 
Size: 716511 Color: 4
Size: 283230 Color: 1

Bin 46: 271 of cap free
Amount of items: 3
Items: 
Size: 747565 Color: 4
Size: 150600 Color: 1
Size: 101565 Color: 1

Bin 47: 293 of cap free
Amount of items: 2
Items: 
Size: 521037 Color: 4
Size: 478671 Color: 1

Bin 48: 293 of cap free
Amount of items: 2
Items: 
Size: 647026 Color: 3
Size: 352682 Color: 1

Bin 49: 301 of cap free
Amount of items: 2
Items: 
Size: 519838 Color: 3
Size: 479862 Color: 1

Bin 50: 306 of cap free
Amount of items: 2
Items: 
Size: 542305 Color: 2
Size: 457390 Color: 1

Bin 51: 319 of cap free
Amount of items: 2
Items: 
Size: 693915 Color: 2
Size: 305767 Color: 0

Bin 52: 340 of cap free
Amount of items: 2
Items: 
Size: 648125 Color: 0
Size: 351536 Color: 3

Bin 53: 341 of cap free
Amount of items: 2
Items: 
Size: 575747 Color: 3
Size: 423913 Color: 4

Bin 54: 355 of cap free
Amount of items: 2
Items: 
Size: 768940 Color: 1
Size: 230706 Color: 2

Bin 55: 368 of cap free
Amount of items: 3
Items: 
Size: 588166 Color: 2
Size: 265393 Color: 1
Size: 146074 Color: 4

Bin 56: 377 of cap free
Amount of items: 3
Items: 
Size: 748754 Color: 3
Size: 145051 Color: 4
Size: 105819 Color: 4

Bin 57: 390 of cap free
Amount of items: 3
Items: 
Size: 736754 Color: 1
Size: 132198 Color: 1
Size: 130659 Color: 3

Bin 58: 399 of cap free
Amount of items: 2
Items: 
Size: 644336 Color: 1
Size: 355266 Color: 2

Bin 59: 413 of cap free
Amount of items: 2
Items: 
Size: 680790 Color: 3
Size: 318798 Color: 4

Bin 60: 419 of cap free
Amount of items: 2
Items: 
Size: 721630 Color: 3
Size: 277952 Color: 2

Bin 61: 422 of cap free
Amount of items: 3
Items: 
Size: 500934 Color: 1
Size: 328253 Color: 0
Size: 170392 Color: 4

Bin 62: 428 of cap free
Amount of items: 3
Items: 
Size: 433123 Color: 3
Size: 361065 Color: 0
Size: 205385 Color: 0

Bin 63: 428 of cap free
Amount of items: 2
Items: 
Size: 737571 Color: 1
Size: 262002 Color: 3

Bin 64: 434 of cap free
Amount of items: 3
Items: 
Size: 703635 Color: 1
Size: 154497 Color: 3
Size: 141435 Color: 4

Bin 65: 445 of cap free
Amount of items: 2
Items: 
Size: 773897 Color: 0
Size: 225659 Color: 4

Bin 66: 449 of cap free
Amount of items: 2
Items: 
Size: 684891 Color: 3
Size: 314661 Color: 4

Bin 67: 461 of cap free
Amount of items: 2
Items: 
Size: 714554 Color: 4
Size: 284986 Color: 1

Bin 68: 525 of cap free
Amount of items: 2
Items: 
Size: 645128 Color: 3
Size: 354348 Color: 1

Bin 69: 528 of cap free
Amount of items: 2
Items: 
Size: 771793 Color: 1
Size: 227680 Color: 0

Bin 70: 533 of cap free
Amount of items: 2
Items: 
Size: 791802 Color: 0
Size: 207666 Color: 3

Bin 71: 537 of cap free
Amount of items: 2
Items: 
Size: 627249 Color: 0
Size: 372215 Color: 1

Bin 72: 561 of cap free
Amount of items: 3
Items: 
Size: 590747 Color: 3
Size: 211197 Color: 2
Size: 197496 Color: 1

Bin 73: 572 of cap free
Amount of items: 2
Items: 
Size: 681995 Color: 0
Size: 317434 Color: 2

Bin 74: 578 of cap free
Amount of items: 2
Items: 
Size: 588161 Color: 2
Size: 411262 Color: 1

Bin 75: 599 of cap free
Amount of items: 2
Items: 
Size: 772231 Color: 4
Size: 227171 Color: 1

Bin 76: 618 of cap free
Amount of items: 2
Items: 
Size: 539458 Color: 3
Size: 459925 Color: 4

Bin 77: 648 of cap free
Amount of items: 3
Items: 
Size: 735897 Color: 0
Size: 147438 Color: 1
Size: 116018 Color: 4

Bin 78: 662 of cap free
Amount of items: 2
Items: 
Size: 549258 Color: 2
Size: 450081 Color: 4

Bin 79: 682 of cap free
Amount of items: 2
Items: 
Size: 593121 Color: 0
Size: 406198 Color: 4

Bin 80: 691 of cap free
Amount of items: 2
Items: 
Size: 775895 Color: 0
Size: 223415 Color: 4

Bin 81: 704 of cap free
Amount of items: 2
Items: 
Size: 553068 Color: 1
Size: 446229 Color: 2

Bin 82: 722 of cap free
Amount of items: 2
Items: 
Size: 549104 Color: 3
Size: 450175 Color: 2

Bin 83: 754 of cap free
Amount of items: 2
Items: 
Size: 591765 Color: 1
Size: 407482 Color: 4

Bin 84: 789 of cap free
Amount of items: 2
Items: 
Size: 678454 Color: 3
Size: 320758 Color: 2

Bin 85: 796 of cap free
Amount of items: 2
Items: 
Size: 690656 Color: 1
Size: 308549 Color: 3

Bin 86: 802 of cap free
Amount of items: 2
Items: 
Size: 757398 Color: 2
Size: 241801 Color: 1

Bin 87: 803 of cap free
Amount of items: 2
Items: 
Size: 652497 Color: 1
Size: 346701 Color: 4

Bin 88: 819 of cap free
Amount of items: 2
Items: 
Size: 543808 Color: 4
Size: 455374 Color: 3

Bin 89: 829 of cap free
Amount of items: 2
Items: 
Size: 694993 Color: 2
Size: 304179 Color: 0

Bin 90: 837 of cap free
Amount of items: 2
Items: 
Size: 530034 Color: 4
Size: 469130 Color: 2

Bin 91: 848 of cap free
Amount of items: 2
Items: 
Size: 553797 Color: 4
Size: 445356 Color: 2

Bin 92: 865 of cap free
Amount of items: 2
Items: 
Size: 649992 Color: 2
Size: 349144 Color: 0

Bin 93: 879 of cap free
Amount of items: 2
Items: 
Size: 715331 Color: 0
Size: 283791 Color: 3

Bin 94: 886 of cap free
Amount of items: 2
Items: 
Size: 756295 Color: 0
Size: 242820 Color: 1

Bin 95: 950 of cap free
Amount of items: 2
Items: 
Size: 730441 Color: 4
Size: 268610 Color: 1

Bin 96: 977 of cap free
Amount of items: 2
Items: 
Size: 630430 Color: 3
Size: 368594 Color: 0

Bin 97: 998 of cap free
Amount of items: 3
Items: 
Size: 500753 Color: 1
Size: 358515 Color: 1
Size: 139735 Color: 2

Bin 98: 1038 of cap free
Amount of items: 2
Items: 
Size: 700187 Color: 1
Size: 298776 Color: 4

Bin 99: 1077 of cap free
Amount of items: 2
Items: 
Size: 764869 Color: 3
Size: 234055 Color: 4

Bin 100: 1082 of cap free
Amount of items: 2
Items: 
Size: 599609 Color: 1
Size: 399310 Color: 4

Bin 101: 1094 of cap free
Amount of items: 2
Items: 
Size: 575487 Color: 4
Size: 423420 Color: 3

Bin 102: 1110 of cap free
Amount of items: 3
Items: 
Size: 611942 Color: 2
Size: 195249 Color: 1
Size: 191700 Color: 2

Bin 103: 1112 of cap free
Amount of items: 2
Items: 
Size: 649080 Color: 0
Size: 349809 Color: 4

Bin 104: 1115 of cap free
Amount of items: 2
Items: 
Size: 515093 Color: 4
Size: 483793 Color: 2

Bin 105: 1124 of cap free
Amount of items: 3
Items: 
Size: 749876 Color: 0
Size: 124534 Color: 0
Size: 124467 Color: 2

Bin 106: 1133 of cap free
Amount of items: 2
Items: 
Size: 776611 Color: 3
Size: 222257 Color: 4

Bin 107: 1207 of cap free
Amount of items: 2
Items: 
Size: 621001 Color: 0
Size: 377793 Color: 1

Bin 108: 1223 of cap free
Amount of items: 3
Items: 
Size: 590020 Color: 2
Size: 214467 Color: 2
Size: 194291 Color: 4

Bin 109: 1228 of cap free
Amount of items: 2
Items: 
Size: 582838 Color: 1
Size: 415935 Color: 2

Bin 110: 1230 of cap free
Amount of items: 2
Items: 
Size: 519699 Color: 0
Size: 479072 Color: 1

Bin 111: 1233 of cap free
Amount of items: 2
Items: 
Size: 606019 Color: 3
Size: 392749 Color: 4

Bin 112: 1257 of cap free
Amount of items: 2
Items: 
Size: 620843 Color: 2
Size: 377901 Color: 4

Bin 113: 1258 of cap free
Amount of items: 2
Items: 
Size: 643711 Color: 4
Size: 355032 Color: 0

Bin 114: 1276 of cap free
Amount of items: 2
Items: 
Size: 532991 Color: 0
Size: 465734 Color: 4

Bin 115: 1297 of cap free
Amount of items: 2
Items: 
Size: 777904 Color: 0
Size: 220800 Color: 3

Bin 116: 1298 of cap free
Amount of items: 2
Items: 
Size: 653724 Color: 3
Size: 344979 Color: 0

Bin 117: 1298 of cap free
Amount of items: 2
Items: 
Size: 526288 Color: 0
Size: 472415 Color: 2

Bin 118: 1317 of cap free
Amount of items: 2
Items: 
Size: 638271 Color: 0
Size: 360413 Color: 1

Bin 119: 1332 of cap free
Amount of items: 2
Items: 
Size: 670995 Color: 4
Size: 327674 Color: 1

Bin 120: 1388 of cap free
Amount of items: 2
Items: 
Size: 659837 Color: 2
Size: 338776 Color: 0

Bin 121: 1399 of cap free
Amount of items: 2
Items: 
Size: 568684 Color: 4
Size: 429918 Color: 3

Bin 122: 1429 of cap free
Amount of items: 2
Items: 
Size: 674798 Color: 4
Size: 323774 Color: 3

Bin 123: 1431 of cap free
Amount of items: 2
Items: 
Size: 773044 Color: 1
Size: 225526 Color: 3

Bin 124: 1444 of cap free
Amount of items: 2
Items: 
Size: 780438 Color: 3
Size: 218119 Color: 1

Bin 125: 1461 of cap free
Amount of items: 2
Items: 
Size: 752470 Color: 2
Size: 246070 Color: 3

Bin 126: 1467 of cap free
Amount of items: 2
Items: 
Size: 594891 Color: 3
Size: 403643 Color: 1

Bin 127: 1545 of cap free
Amount of items: 2
Items: 
Size: 730454 Color: 2
Size: 268002 Color: 0

Bin 128: 1550 of cap free
Amount of items: 2
Items: 
Size: 646100 Color: 4
Size: 352351 Color: 3

Bin 129: 1602 of cap free
Amount of items: 2
Items: 
Size: 532194 Color: 3
Size: 466205 Color: 0

Bin 130: 1610 of cap free
Amount of items: 2
Items: 
Size: 545725 Color: 2
Size: 452666 Color: 1

Bin 131: 1614 of cap free
Amount of items: 2
Items: 
Size: 769309 Color: 3
Size: 229078 Color: 1

Bin 132: 1650 of cap free
Amount of items: 2
Items: 
Size: 740604 Color: 2
Size: 257747 Color: 1

Bin 133: 1658 of cap free
Amount of items: 2
Items: 
Size: 730545 Color: 1
Size: 267798 Color: 3

Bin 134: 1659 of cap free
Amount of items: 2
Items: 
Size: 584157 Color: 0
Size: 414185 Color: 1

Bin 135: 1670 of cap free
Amount of items: 2
Items: 
Size: 730399 Color: 1
Size: 267932 Color: 4

Bin 136: 1693 of cap free
Amount of items: 2
Items: 
Size: 788097 Color: 0
Size: 210211 Color: 4

Bin 137: 1699 of cap free
Amount of items: 3
Items: 
Size: 481024 Color: 0
Size: 343076 Color: 3
Size: 174202 Color: 2

Bin 138: 1708 of cap free
Amount of items: 2
Items: 
Size: 507484 Color: 1
Size: 490809 Color: 4

Bin 139: 1738 of cap free
Amount of items: 3
Items: 
Size: 752941 Color: 4
Size: 142301 Color: 1
Size: 103021 Color: 2

Bin 140: 1749 of cap free
Amount of items: 2
Items: 
Size: 723064 Color: 3
Size: 275188 Color: 0

Bin 141: 1892 of cap free
Amount of items: 2
Items: 
Size: 686103 Color: 0
Size: 312006 Color: 4

Bin 142: 1931 of cap free
Amount of items: 2
Items: 
Size: 659539 Color: 3
Size: 338531 Color: 2

Bin 143: 1950 of cap free
Amount of items: 2
Items: 
Size: 717153 Color: 1
Size: 280898 Color: 2

Bin 144: 1979 of cap free
Amount of items: 2
Items: 
Size: 720445 Color: 1
Size: 277577 Color: 2

Bin 145: 1986 of cap free
Amount of items: 2
Items: 
Size: 577971 Color: 4
Size: 420044 Color: 3

Bin 146: 2004 of cap free
Amount of items: 2
Items: 
Size: 700626 Color: 0
Size: 297371 Color: 2

Bin 147: 2032 of cap free
Amount of items: 2
Items: 
Size: 616653 Color: 2
Size: 381316 Color: 0

Bin 148: 2056 of cap free
Amount of items: 2
Items: 
Size: 604923 Color: 4
Size: 393022 Color: 1

Bin 149: 2089 of cap free
Amount of items: 2
Items: 
Size: 663189 Color: 2
Size: 334723 Color: 3

Bin 150: 2112 of cap free
Amount of items: 2
Items: 
Size: 758456 Color: 0
Size: 239433 Color: 1

Bin 151: 2171 of cap free
Amount of items: 2
Items: 
Size: 510000 Color: 3
Size: 487830 Color: 2

Bin 152: 2186 of cap free
Amount of items: 3
Items: 
Size: 704803 Color: 3
Size: 173480 Color: 2
Size: 119532 Color: 3

Bin 153: 2249 of cap free
Amount of items: 2
Items: 
Size: 679506 Color: 0
Size: 318246 Color: 1

Bin 154: 2271 of cap free
Amount of items: 2
Items: 
Size: 667017 Color: 0
Size: 330713 Color: 4

Bin 155: 2309 of cap free
Amount of items: 2
Items: 
Size: 620563 Color: 4
Size: 377129 Color: 2

Bin 156: 2337 of cap free
Amount of items: 2
Items: 
Size: 759887 Color: 1
Size: 237777 Color: 4

Bin 157: 2386 of cap free
Amount of items: 2
Items: 
Size: 510545 Color: 1
Size: 487070 Color: 0

Bin 158: 2400 of cap free
Amount of items: 2
Items: 
Size: 632603 Color: 2
Size: 364998 Color: 1

Bin 159: 2411 of cap free
Amount of items: 2
Items: 
Size: 736706 Color: 3
Size: 260884 Color: 1

Bin 160: 2441 of cap free
Amount of items: 2
Items: 
Size: 521111 Color: 0
Size: 476449 Color: 2

Bin 161: 2513 of cap free
Amount of items: 2
Items: 
Size: 504981 Color: 0
Size: 492507 Color: 2

Bin 162: 2517 of cap free
Amount of items: 2
Items: 
Size: 576440 Color: 1
Size: 421044 Color: 4

Bin 163: 2523 of cap free
Amount of items: 2
Items: 
Size: 602710 Color: 4
Size: 394768 Color: 3

Bin 164: 2538 of cap free
Amount of items: 2
Items: 
Size: 540285 Color: 0
Size: 457178 Color: 2

Bin 165: 2562 of cap free
Amount of items: 2
Items: 
Size: 677327 Color: 0
Size: 320112 Color: 4

Bin 166: 2627 of cap free
Amount of items: 2
Items: 
Size: 522078 Color: 1
Size: 475296 Color: 2

Bin 167: 2629 of cap free
Amount of items: 2
Items: 
Size: 696208 Color: 1
Size: 301164 Color: 4

Bin 168: 2689 of cap free
Amount of items: 2
Items: 
Size: 590559 Color: 1
Size: 406753 Color: 4

Bin 169: 2705 of cap free
Amount of items: 2
Items: 
Size: 596271 Color: 3
Size: 401025 Color: 4

Bin 170: 2741 of cap free
Amount of items: 2
Items: 
Size: 509732 Color: 4
Size: 487528 Color: 1

Bin 171: 2753 of cap free
Amount of items: 2
Items: 
Size: 570213 Color: 4
Size: 427035 Color: 2

Bin 172: 2763 of cap free
Amount of items: 2
Items: 
Size: 579853 Color: 2
Size: 417385 Color: 4

Bin 173: 2912 of cap free
Amount of items: 2
Items: 
Size: 719831 Color: 4
Size: 277258 Color: 1

Bin 174: 3052 of cap free
Amount of items: 2
Items: 
Size: 712202 Color: 4
Size: 284747 Color: 0

Bin 175: 3078 of cap free
Amount of items: 2
Items: 
Size: 524140 Color: 2
Size: 472783 Color: 3

Bin 176: 3255 of cap free
Amount of items: 2
Items: 
Size: 540375 Color: 4
Size: 456371 Color: 3

Bin 177: 3357 of cap free
Amount of items: 2
Items: 
Size: 582811 Color: 3
Size: 413833 Color: 1

Bin 178: 3385 of cap free
Amount of items: 2
Items: 
Size: 748402 Color: 1
Size: 248214 Color: 0

Bin 179: 3400 of cap free
Amount of items: 2
Items: 
Size: 779806 Color: 0
Size: 216795 Color: 2

Bin 180: 3520 of cap free
Amount of items: 2
Items: 
Size: 548184 Color: 2
Size: 448297 Color: 0

Bin 181: 3646 of cap free
Amount of items: 2
Items: 
Size: 594306 Color: 2
Size: 402049 Color: 0

Bin 182: 3686 of cap free
Amount of items: 2
Items: 
Size: 663887 Color: 4
Size: 332428 Color: 2

Bin 183: 3703 of cap free
Amount of items: 2
Items: 
Size: 638389 Color: 1
Size: 357909 Color: 0

Bin 184: 4040 of cap free
Amount of items: 2
Items: 
Size: 519972 Color: 2
Size: 475989 Color: 3

Bin 185: 4091 of cap free
Amount of items: 2
Items: 
Size: 797250 Color: 3
Size: 198660 Color: 2

Bin 186: 4152 of cap free
Amount of items: 2
Items: 
Size: 664991 Color: 3
Size: 330858 Color: 4

Bin 187: 4214 of cap free
Amount of items: 2
Items: 
Size: 739464 Color: 3
Size: 256323 Color: 1

Bin 188: 4460 of cap free
Amount of items: 2
Items: 
Size: 632944 Color: 4
Size: 362597 Color: 2

Bin 189: 4517 of cap free
Amount of items: 2
Items: 
Size: 720085 Color: 3
Size: 275399 Color: 0

Bin 190: 4520 of cap free
Amount of items: 2
Items: 
Size: 771128 Color: 1
Size: 224353 Color: 3

Bin 191: 4608 of cap free
Amount of items: 2
Items: 
Size: 696653 Color: 4
Size: 298740 Color: 0

Bin 192: 4617 of cap free
Amount of items: 2
Items: 
Size: 782077 Color: 0
Size: 213307 Color: 2

Bin 193: 4721 of cap free
Amount of items: 2
Items: 
Size: 603170 Color: 0
Size: 392110 Color: 4

Bin 194: 4855 of cap free
Amount of items: 2
Items: 
Size: 719207 Color: 1
Size: 275939 Color: 3

Bin 195: 5181 of cap free
Amount of items: 2
Items: 
Size: 739731 Color: 1
Size: 255089 Color: 3

Bin 196: 5289 of cap free
Amount of items: 2
Items: 
Size: 567252 Color: 0
Size: 427460 Color: 4

Bin 197: 5422 of cap free
Amount of items: 2
Items: 
Size: 768500 Color: 4
Size: 226079 Color: 1

Bin 198: 5550 of cap free
Amount of items: 2
Items: 
Size: 772281 Color: 2
Size: 222170 Color: 1

Bin 199: 5584 of cap free
Amount of items: 2
Items: 
Size: 594287 Color: 2
Size: 400130 Color: 4

Bin 200: 5852 of cap free
Amount of items: 2
Items: 
Size: 574661 Color: 1
Size: 419488 Color: 4

Bin 201: 5858 of cap free
Amount of items: 2
Items: 
Size: 543173 Color: 4
Size: 450970 Color: 2

Bin 202: 5959 of cap free
Amount of items: 2
Items: 
Size: 531376 Color: 2
Size: 462666 Color: 1

Bin 203: 6056 of cap free
Amount of items: 2
Items: 
Size: 531719 Color: 3
Size: 462226 Color: 0

Bin 204: 6328 of cap free
Amount of items: 2
Items: 
Size: 664275 Color: 4
Size: 329398 Color: 2

Bin 205: 6339 of cap free
Amount of items: 2
Items: 
Size: 556861 Color: 1
Size: 436801 Color: 3

Bin 206: 7019 of cap free
Amount of items: 2
Items: 
Size: 555750 Color: 1
Size: 437232 Color: 3

Bin 207: 7647 of cap free
Amount of items: 2
Items: 
Size: 616014 Color: 1
Size: 376340 Color: 3

Bin 208: 7712 of cap free
Amount of items: 2
Items: 
Size: 653642 Color: 3
Size: 338647 Color: 1

Bin 209: 7784 of cap free
Amount of items: 2
Items: 
Size: 759360 Color: 1
Size: 232857 Color: 0

Bin 210: 7956 of cap free
Amount of items: 2
Items: 
Size: 738650 Color: 1
Size: 253395 Color: 2

Bin 211: 8312 of cap free
Amount of items: 2
Items: 
Size: 758508 Color: 4
Size: 233181 Color: 2

Bin 212: 8831 of cap free
Amount of items: 2
Items: 
Size: 504984 Color: 2
Size: 486186 Color: 1

Bin 213: 9100 of cap free
Amount of items: 2
Items: 
Size: 757387 Color: 3
Size: 233514 Color: 0

Bin 214: 9401 of cap free
Amount of items: 2
Items: 
Size: 720094 Color: 2
Size: 270506 Color: 1

Bin 215: 9781 of cap free
Amount of items: 2
Items: 
Size: 756027 Color: 2
Size: 234193 Color: 3

Bin 216: 10329 of cap free
Amount of items: 2
Items: 
Size: 508742 Color: 2
Size: 480930 Color: 3

Bin 217: 14519 of cap free
Amount of items: 2
Items: 
Size: 779661 Color: 4
Size: 205821 Color: 0

Bin 218: 14996 of cap free
Amount of items: 2
Items: 
Size: 594102 Color: 4
Size: 390903 Color: 3

Bin 219: 15200 of cap free
Amount of items: 2
Items: 
Size: 795584 Color: 1
Size: 189217 Color: 2

Bin 220: 15339 of cap free
Amount of items: 2
Items: 
Size: 611686 Color: 3
Size: 372976 Color: 4

Bin 221: 18979 of cap free
Amount of items: 2
Items: 
Size: 590974 Color: 4
Size: 390048 Color: 1

Bin 222: 40504 of cap free
Amount of items: 3
Items: 
Size: 687624 Color: 4
Size: 166065 Color: 4
Size: 105808 Color: 1

Bin 223: 57769 of cap free
Amount of items: 3
Items: 
Size: 591452 Color: 3
Size: 185238 Color: 1
Size: 165542 Color: 0

Bin 224: 67604 of cap free
Amount of items: 2
Items: 
Size: 665715 Color: 4
Size: 266682 Color: 1

Bin 225: 73351 of cap free
Amount of items: 2
Items: 
Size: 717082 Color: 2
Size: 209568 Color: 0

Bin 226: 94179 of cap free
Amount of items: 2
Items: 
Size: 571074 Color: 0
Size: 334748 Color: 2

Bin 227: 94208 of cap free
Amount of items: 2
Items: 
Size: 464242 Color: 2
Size: 441551 Color: 3

Bin 228: 121244 of cap free
Amount of items: 3
Items: 
Size: 562332 Color: 1
Size: 159235 Color: 4
Size: 157190 Color: 3

Bin 229: 124627 of cap free
Amount of items: 2
Items: 
Size: 568431 Color: 1
Size: 306943 Color: 3

Bin 230: 132802 of cap free
Amount of items: 2
Items: 
Size: 499517 Color: 2
Size: 367682 Color: 1

Total size: 228702598
Total free space: 1297632

