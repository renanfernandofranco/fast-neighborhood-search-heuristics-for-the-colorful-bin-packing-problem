Capicity Bin: 2048
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1165 Color: 143
Size: 631 Color: 123
Size: 168 Color: 66
Size: 76 Color: 35
Size: 8 Color: 2

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1438 Color: 160
Size: 510 Color: 115
Size: 64 Color: 28
Size: 36 Color: 9

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1280 Color: 148
Size: 482 Color: 112
Size: 190 Color: 70
Size: 96 Color: 46

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 151
Size: 737 Color: 131
Size: 20 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 178
Size: 342 Color: 95
Size: 68 Color: 30

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 200
Size: 186 Color: 69
Size: 40 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 182
Size: 314 Color: 90
Size: 60 Color: 26

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1027 Color: 140
Size: 851 Color: 135
Size: 170 Color: 68

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 168
Size: 429 Color: 105
Size: 84 Color: 41

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1385 Color: 155
Size: 553 Color: 119
Size: 110 Color: 52

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 164
Size: 446 Color: 109
Size: 88 Color: 44

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 174
Size: 435 Color: 108
Size: 20 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 185
Size: 281 Color: 87
Size: 56 Color: 22

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 183
Size: 318 Color: 92
Size: 48 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 170
Size: 390 Color: 102
Size: 76 Color: 39

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 201
Size: 146 Color: 63
Size: 76 Color: 38

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 1398 Color: 158
Size: 424 Color: 104
Size: 126 Color: 59
Size: 100 Color: 48

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 150
Size: 635 Color: 124
Size: 126 Color: 57

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 192
Size: 246 Color: 79
Size: 48 Color: 15

Bin 20: 0 of cap free
Amount of items: 4
Items: 
Size: 828 Color: 133
Size: 696 Color: 127
Size: 316 Color: 91
Size: 208 Color: 74

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 184
Size: 303 Color: 88
Size: 60 Color: 25

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1026 Color: 139
Size: 802 Color: 132
Size: 220 Color: 76

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 147
Size: 682 Color: 126
Size: 132 Color: 60

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 172
Size: 388 Color: 101
Size: 72 Color: 33

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1031 Color: 141
Size: 849 Color: 134
Size: 168 Color: 67

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1463 Color: 161
Size: 489 Color: 114
Size: 96 Color: 47

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 171
Size: 385 Color: 100
Size: 76 Color: 36

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 177
Size: 351 Color: 96
Size: 68 Color: 31

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 199
Size: 194 Color: 71
Size: 36 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 197
Size: 202 Color: 73
Size: 36 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 173
Size: 382 Color: 99
Size: 76 Color: 40

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1169 Color: 144
Size: 733 Color: 130
Size: 146 Color: 64

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1173 Color: 145
Size: 731 Color: 129
Size: 144 Color: 62

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 159
Size: 472 Color: 111
Size: 160 Color: 65

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 175
Size: 378 Color: 98
Size: 72 Color: 34

Bin 36: 0 of cap free
Amount of items: 4
Items: 
Size: 1646 Color: 179
Size: 338 Color: 94
Size: 56 Color: 23
Size: 8 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 157
Size: 550 Color: 117
Size: 108 Color: 51

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1283 Color: 149
Size: 639 Color: 125
Size: 126 Color: 58

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 169
Size: 398 Color: 103
Size: 76 Color: 37

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1319 Color: 154
Size: 609 Color: 120
Size: 120 Color: 54

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 163
Size: 542 Color: 116
Size: 36 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 138
Size: 903 Color: 137
Size: 120 Color: 55

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 198
Size: 198 Color: 72
Size: 36 Color: 8

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 146
Size: 706 Color: 128
Size: 140 Color: 61

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 167
Size: 430 Color: 106
Size: 84 Color: 42

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 152
Size: 626 Color: 122
Size: 124 Color: 56

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 142
Size: 854 Color: 136
Size: 108 Color: 50

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 189
Size: 262 Color: 81
Size: 48 Color: 14

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 162
Size: 487 Color: 113
Size: 96 Color: 45

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 193
Size: 242 Color: 78
Size: 48 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 190
Size: 258 Color: 80
Size: 48 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 180
Size: 325 Color: 93
Size: 64 Color: 29

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 181
Size: 313 Color: 89
Size: 62 Color: 27

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 153
Size: 614 Color: 121
Size: 120 Color: 53

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 188
Size: 263 Color: 82
Size: 52 Color: 19

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 165
Size: 463 Color: 110
Size: 58 Color: 24

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1621 Color: 176
Size: 357 Color: 97
Size: 70 Color: 32

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 191
Size: 275 Color: 85
Size: 30 Color: 5

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 187
Size: 273 Color: 84
Size: 54 Color: 21

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 186
Size: 281 Color: 86
Size: 54 Color: 20

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 156
Size: 551 Color: 118
Size: 108 Color: 49

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 166
Size: 431 Color: 107
Size: 86 Color: 43

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 194
Size: 241 Color: 77
Size: 46 Color: 13

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 195
Size: 265 Color: 83
Size: 2 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 196
Size: 213 Color: 75
Size: 42 Color: 12

Total size: 133120
Total free space: 0

