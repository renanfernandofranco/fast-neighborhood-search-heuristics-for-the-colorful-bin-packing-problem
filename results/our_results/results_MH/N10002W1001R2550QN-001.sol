Capicity Bin: 1001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 10002

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9515
Size: 275 Color: 2570
Size: 256 Color: 936

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7676
Size: 343 Color: 6148
Size: 269 Color: 2086

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9183
Size: 292 Color: 3732
Size: 257 Color: 989

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9771
Size: 265 Color: 1674
Size: 250 Color: 102

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7091
Size: 351 Color: 6446
Size: 281 Color: 2987

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8872
Size: 291 Color: 3663
Size: 274 Color: 2527

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8515
Size: 296 Color: 3945
Size: 286 Color: 3314

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8110
Size: 335 Color: 5822
Size: 262 Color: 1474

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8827
Size: 303 Color: 4349
Size: 264 Color: 1631

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9466
Size: 284 Color: 3219
Size: 251 Color: 197

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8868
Size: 314 Color: 4900
Size: 251 Color: 224

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9802
Size: 258 Color: 1058
Size: 256 Color: 932

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8920
Size: 285 Color: 3258
Size: 278 Color: 2839

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9075
Size: 283 Color: 3167
Size: 272 Color: 2302

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8046
Size: 345 Color: 6234
Size: 254 Color: 619

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9909
Size: 254 Color: 615
Size: 251 Color: 331

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7464
Size: 346 Color: 6283
Size: 274 Color: 2456

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8757
Size: 287 Color: 3420
Size: 284 Color: 3233

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7151
Size: 333 Color: 5752
Size: 297 Color: 4020

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7265
Size: 319 Color: 5129
Size: 308 Color: 4600

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9793
Size: 264 Color: 1595
Size: 250 Color: 128

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6730
Size: 359 Color: 6709
Size: 283 Color: 3136

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7913
Size: 313 Color: 4859
Size: 291 Color: 3641

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7266
Size: 327 Color: 5507
Size: 300 Color: 4163

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8836
Size: 300 Color: 4192
Size: 267 Color: 1863

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9423
Size: 273 Color: 2429
Size: 264 Color: 1645

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9042
Size: 285 Color: 3272
Size: 272 Color: 2322

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8947
Size: 296 Color: 3941
Size: 265 Color: 1677

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 6217
Size: 334 Color: 5807
Size: 322 Color: 5278

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7748
Size: 342 Color: 6091
Size: 268 Color: 2006

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9534
Size: 277 Color: 2774
Size: 253 Color: 552

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9712
Size: 266 Color: 1824
Size: 254 Color: 646

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8288
Size: 330 Color: 5653
Size: 260 Color: 1329

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8314
Size: 321 Color: 5236
Size: 268 Color: 2040

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8461
Size: 319 Color: 5125
Size: 265 Color: 1716

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6415
Size: 350 Color: 6413
Size: 301 Color: 4250

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7828
Size: 313 Color: 4848
Size: 294 Color: 3823

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6976
Size: 318 Color: 5100
Size: 317 Color: 5055

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7871
Size: 328 Color: 5543
Size: 278 Color: 2834

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9959
Size: 252 Color: 357
Size: 250 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8957
Size: 308 Color: 4581
Size: 253 Color: 493

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8214
Size: 331 Color: 5680
Size: 262 Color: 1431

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8667
Size: 323 Color: 5315
Size: 252 Color: 453

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7761
Size: 342 Color: 6107
Size: 267 Color: 1858

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7956
Size: 316 Color: 5002
Size: 287 Color: 3389

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8765
Size: 286 Color: 3331
Size: 284 Color: 3206

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9087
Size: 279 Color: 2906
Size: 275 Color: 2589

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8623
Size: 322 Color: 5292
Size: 255 Color: 844

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7494
Size: 311 Color: 4760
Size: 308 Color: 4606

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7485
Size: 362 Color: 6810
Size: 257 Color: 957

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6980
Size: 329 Color: 5580
Size: 306 Color: 4466

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6946
Size: 320 Color: 5172
Size: 316 Color: 4996

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8347
Size: 325 Color: 5400
Size: 263 Color: 1547

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8004
Size: 332 Color: 5695
Size: 269 Color: 2058

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 9716
Size: 261 Color: 1393
Size: 258 Color: 1134

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8871
Size: 290 Color: 3571
Size: 275 Color: 2595

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7762
Size: 321 Color: 5221
Size: 288 Color: 3458

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6938
Size: 334 Color: 5817
Size: 302 Color: 4324

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9852
Size: 255 Color: 841
Size: 255 Color: 765

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9442
Size: 276 Color: 2691
Size: 260 Color: 1318

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6420
Size: 350 Color: 6419
Size: 301 Color: 4208

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7829
Size: 319 Color: 5135
Size: 288 Color: 3438

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7840
Size: 306 Color: 4498
Size: 301 Color: 4225

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8396
Size: 306 Color: 4480
Size: 280 Color: 2915

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9552
Size: 272 Color: 2352
Size: 257 Color: 966

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8170
Size: 337 Color: 5889
Size: 257 Color: 1024

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8244
Size: 324 Color: 5359
Size: 268 Color: 2039

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 9979
Size: 251 Color: 293
Size: 250 Color: 82

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7918
Size: 326 Color: 5478
Size: 278 Color: 2838

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7932
Size: 326 Color: 5467
Size: 278 Color: 2820

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 6997
Size: 332 Color: 5729
Size: 302 Color: 4323

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9634
Size: 266 Color: 1815
Size: 259 Color: 1206

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9669
Size: 261 Color: 1419
Size: 261 Color: 1344

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8343
Size: 294 Color: 3846
Size: 294 Color: 3831

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8457
Size: 307 Color: 4532
Size: 277 Color: 2709

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9224
Size: 279 Color: 2901
Size: 268 Color: 1991

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9773
Size: 262 Color: 1450
Size: 253 Color: 510

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9955
Size: 252 Color: 339
Size: 250 Color: 148

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9934
Size: 252 Color: 459
Size: 252 Color: 337

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9147
Size: 299 Color: 4150
Size: 252 Color: 356

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7746
Size: 342 Color: 6103
Size: 268 Color: 1968

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9565
Size: 269 Color: 2085
Size: 260 Color: 1317

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7378
Size: 347 Color: 6296
Size: 276 Color: 2634

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9023
Size: 294 Color: 3811
Size: 264 Color: 1603

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8411
Size: 295 Color: 3897
Size: 291 Color: 3643

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7705
Size: 314 Color: 4876
Size: 297 Color: 4027

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9020
Size: 290 Color: 3622
Size: 268 Color: 1999

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8285
Size: 339 Color: 5993
Size: 251 Color: 270

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7381
Size: 347 Color: 6302
Size: 276 Color: 2643

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8386
Size: 316 Color: 4995
Size: 271 Color: 2271

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8169
Size: 300 Color: 4187
Size: 294 Color: 3842

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9009
Size: 299 Color: 4142
Size: 260 Color: 1272

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8326
Size: 339 Color: 5992
Size: 250 Color: 129

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9647
Size: 265 Color: 1747
Size: 259 Color: 1235

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7985
Size: 338 Color: 5931
Size: 264 Color: 1632

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8313
Size: 339 Color: 5979
Size: 250 Color: 51

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8354
Size: 324 Color: 5370
Size: 264 Color: 1658

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9005
Size: 290 Color: 3587
Size: 269 Color: 2057

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7615
Size: 308 Color: 4609
Size: 306 Color: 4493

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9859
Size: 257 Color: 990
Size: 252 Color: 450

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8845
Size: 308 Color: 4562
Size: 258 Color: 1136

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9375
Size: 277 Color: 2698
Size: 262 Color: 1463

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9497
Size: 269 Color: 2065
Size: 263 Color: 1528

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7632
Size: 357 Color: 6638
Size: 257 Color: 1009

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7032
Size: 342 Color: 6090
Size: 291 Color: 3629

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7907
Size: 324 Color: 5377
Size: 281 Color: 2978

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7201
Size: 349 Color: 6390
Size: 280 Color: 2927

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8433
Size: 316 Color: 4986
Size: 269 Color: 2098

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7491
Size: 362 Color: 6820
Size: 257 Color: 973

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7187
Size: 318 Color: 5087
Size: 311 Color: 4747

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7358
Size: 343 Color: 6159
Size: 281 Color: 2979

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7603
Size: 328 Color: 5565
Size: 287 Color: 3424

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7398
Size: 311 Color: 4757
Size: 311 Color: 4746

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9101
Size: 294 Color: 3852
Size: 259 Color: 1197

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8337
Size: 320 Color: 5177
Size: 268 Color: 2053

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9540
Size: 266 Color: 1808
Size: 264 Color: 1608

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7565
Size: 344 Color: 6196
Size: 272 Color: 2311

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7383
Size: 312 Color: 4811
Size: 311 Color: 4759

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6981
Size: 318 Color: 5066
Size: 317 Color: 5051

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7738
Size: 340 Color: 6034
Size: 270 Color: 2190

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8738
Size: 321 Color: 5211
Size: 251 Color: 295

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9839
Size: 257 Color: 984
Size: 253 Color: 593

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6627
Size: 356 Color: 6620
Size: 289 Color: 3530

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6815
Size: 349 Color: 6400
Size: 290 Color: 3599

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9357
Size: 287 Color: 3426
Size: 253 Color: 531

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8003
Size: 309 Color: 4613
Size: 292 Color: 3696

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8573
Size: 306 Color: 4473
Size: 273 Color: 2402

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6881
Size: 326 Color: 5441
Size: 312 Color: 4793

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8438
Size: 332 Color: 5709
Size: 253 Color: 540

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6975
Size: 318 Color: 5102
Size: 317 Color: 5017

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6778
Size: 337 Color: 5898
Size: 303 Color: 4364

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7810
Size: 341 Color: 6045
Size: 267 Color: 1926

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8594
Size: 312 Color: 4799
Size: 266 Color: 1794

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7236
Size: 349 Color: 6375
Size: 279 Color: 2879

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7477
Size: 313 Color: 4826
Size: 306 Color: 4494

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9244
Size: 282 Color: 3093
Size: 264 Color: 1594

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 9988
Size: 250 Color: 38
Size: 250 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9903
Size: 254 Color: 688
Size: 252 Color: 442

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9021
Size: 305 Color: 4420
Size: 253 Color: 575

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6834
Size: 362 Color: 6823
Size: 277 Color: 2770

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8217
Size: 331 Color: 5693
Size: 262 Color: 1500

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8913
Size: 285 Color: 3283
Size: 278 Color: 2812

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9885
Size: 255 Color: 807
Size: 252 Color: 446

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8238
Size: 336 Color: 5854
Size: 256 Color: 867

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9697
Size: 261 Color: 1425
Size: 260 Color: 1304

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6873
Size: 328 Color: 5559
Size: 310 Color: 4679

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7283
Size: 349 Color: 6408
Size: 277 Color: 2714

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9868
Size: 254 Color: 717
Size: 254 Color: 664

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9707
Size: 270 Color: 2156
Size: 250 Color: 12

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8278
Size: 296 Color: 3937
Size: 294 Color: 3860

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8502
Size: 318 Color: 5099
Size: 264 Color: 1653

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9938
Size: 254 Color: 639
Size: 250 Color: 160

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6837
Size: 330 Color: 5630
Size: 309 Color: 4617

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8504
Size: 291 Color: 3668
Size: 291 Color: 3647

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7987
Size: 322 Color: 5267
Size: 280 Color: 2925

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8887
Size: 285 Color: 3256
Size: 279 Color: 2902

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8760
Size: 311 Color: 4750
Size: 259 Color: 1181

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9058
Size: 305 Color: 4432
Size: 251 Color: 269

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7625
Size: 318 Color: 5062
Size: 296 Color: 3948

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8494
Size: 292 Color: 3718
Size: 291 Color: 3657

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8864
Size: 284 Color: 3225
Size: 281 Color: 2972

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6688
Size: 358 Color: 6664
Size: 285 Color: 3285

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8949
Size: 306 Color: 4500
Size: 255 Color: 831

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7995
Size: 315 Color: 4950
Size: 287 Color: 3375

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6862
Size: 326 Color: 5464
Size: 312 Color: 4800

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8489
Size: 307 Color: 4546
Size: 276 Color: 2676

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8803
Size: 316 Color: 4963
Size: 253 Color: 596

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7643
Size: 326 Color: 5471
Size: 287 Color: 3361

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9076
Size: 301 Color: 4232
Size: 254 Color: 642

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6642
Size: 343 Color: 6126
Size: 301 Color: 4268

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7893
Size: 319 Color: 5122
Size: 286 Color: 3333

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9435
Size: 283 Color: 3156
Size: 253 Color: 555

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7647
Size: 343 Color: 6152
Size: 270 Color: 2182

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7694
Size: 343 Color: 6131
Size: 269 Color: 2124

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7188
Size: 349 Color: 6382
Size: 280 Color: 2923

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7367
Size: 327 Color: 5501
Size: 297 Color: 3984

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6860
Size: 328 Color: 5538
Size: 310 Color: 4719

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6737
Size: 359 Color: 6734
Size: 283 Color: 3122

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7362
Size: 323 Color: 5307
Size: 301 Color: 4243

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9503
Size: 266 Color: 1792
Size: 266 Color: 1767

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9606
Size: 265 Color: 1704
Size: 261 Color: 1377

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7020
Size: 319 Color: 5118
Size: 315 Color: 4949

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7346
Size: 348 Color: 6354
Size: 277 Color: 2703

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7463
Size: 346 Color: 6288
Size: 274 Color: 2455

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7960
Size: 337 Color: 5915
Size: 266 Color: 1801

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8149
Size: 331 Color: 5670
Size: 264 Color: 1652

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8374
Size: 306 Color: 4467
Size: 281 Color: 3017

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7690
Size: 343 Color: 6157
Size: 269 Color: 2070

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6445
Size: 350 Color: 6430
Size: 301 Color: 4223

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7256
Size: 349 Color: 6376
Size: 278 Color: 2793

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7922
Size: 325 Color: 5412
Size: 279 Color: 2865

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9213
Size: 288 Color: 3451
Size: 260 Color: 1310

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9294
Size: 294 Color: 3864
Size: 250 Color: 120

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7115
Size: 350 Color: 6442
Size: 281 Color: 3008

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9350
Size: 274 Color: 2496
Size: 267 Color: 1855

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7385
Size: 347 Color: 6294
Size: 276 Color: 2662

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7541
Size: 327 Color: 5483
Size: 290 Color: 3591

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8713
Size: 313 Color: 4863
Size: 260 Color: 1255

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8932
Size: 290 Color: 3610
Size: 272 Color: 2305

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7110
Size: 370 Color: 7106
Size: 261 Color: 1373

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7598
Size: 344 Color: 6204
Size: 271 Color: 2243

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9317
Size: 285 Color: 3280
Size: 258 Color: 1138

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9222
Size: 275 Color: 2562
Size: 272 Color: 2300

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6678
Size: 358 Color: 6660
Size: 285 Color: 3310

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8302
Size: 339 Color: 5967
Size: 250 Color: 25

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8109
Size: 334 Color: 5789
Size: 263 Color: 1540

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7289
Size: 315 Color: 4923
Size: 311 Color: 4781

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8922
Size: 300 Color: 4169
Size: 263 Color: 1566

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7446
Size: 367 Color: 7010
Size: 254 Color: 607

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9038
Size: 285 Color: 3309
Size: 272 Color: 2330

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9671
Size: 271 Color: 2237
Size: 251 Color: 292

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8100
Size: 335 Color: 5821
Size: 262 Color: 1466

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8666
Size: 306 Color: 4481
Size: 269 Color: 2130

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7712
Size: 323 Color: 5340
Size: 288 Color: 3472

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8636
Size: 296 Color: 3936
Size: 281 Color: 3034

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8740
Size: 302 Color: 4288
Size: 270 Color: 2161

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7849
Size: 315 Color: 4926
Size: 292 Color: 3720

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7940
Size: 353 Color: 6511
Size: 250 Color: 168

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7697
Size: 338 Color: 5946
Size: 273 Color: 2407

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7747
Size: 342 Color: 6102
Size: 268 Color: 2028

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7204
Size: 317 Color: 5049
Size: 312 Color: 4818

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9620
Size: 265 Color: 1745
Size: 261 Color: 1413

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7231
Size: 327 Color: 5490
Size: 301 Color: 4204

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6621
Size: 356 Color: 6604
Size: 289 Color: 3504

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9160
Size: 285 Color: 3294
Size: 265 Color: 1737

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7580
Size: 344 Color: 6209
Size: 272 Color: 2284

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7409
Size: 331 Color: 5692
Size: 291 Color: 3631

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8527
Size: 321 Color: 5232
Size: 260 Color: 1254

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8818
Size: 306 Color: 4474
Size: 262 Color: 1479

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9406
Size: 273 Color: 2434
Size: 265 Color: 1661

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7169
Size: 328 Color: 5549
Size: 302 Color: 4273

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7579
Size: 344 Color: 6174
Size: 272 Color: 2314

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6943
Size: 318 Color: 5106
Size: 318 Color: 5075

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8011
Size: 308 Color: 4587
Size: 293 Color: 3795

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7361
Size: 348 Color: 6370
Size: 276 Color: 2641

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9525
Size: 274 Color: 2492
Size: 257 Color: 1000

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6554
Size: 354 Color: 6547
Size: 293 Color: 3794

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7097
Size: 325 Color: 5402
Size: 307 Color: 4526

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9725
Size: 263 Color: 1546
Size: 255 Color: 759

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6606
Size: 356 Color: 6605
Size: 289 Color: 3516

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6838
Size: 330 Color: 5654
Size: 309 Color: 4638

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9896
Size: 253 Color: 581
Size: 253 Color: 481

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8516
Size: 295 Color: 3920
Size: 287 Color: 3419

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8816
Size: 317 Color: 5046
Size: 251 Color: 212

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7009
Size: 349 Color: 6373
Size: 285 Color: 3281

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7077
Size: 335 Color: 5819
Size: 297 Color: 4006

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8155
Size: 341 Color: 6085
Size: 254 Color: 625

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7809
Size: 341 Color: 6072
Size: 267 Color: 1913

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7556
Size: 324 Color: 5349
Size: 292 Color: 3708

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9383
Size: 277 Color: 2740
Size: 262 Color: 1461

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 5896
Size: 333 Color: 5761
Size: 331 Color: 5684

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7240
Size: 349 Color: 6378
Size: 279 Color: 2884

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7017
Size: 321 Color: 5233
Size: 313 Color: 4830

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8522
Size: 291 Color: 3639
Size: 291 Color: 3635

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9286
Size: 284 Color: 3185
Size: 260 Color: 1322

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7787
Size: 341 Color: 6048
Size: 267 Color: 1947

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7977
Size: 322 Color: 5255
Size: 280 Color: 2931

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9670
Size: 271 Color: 2239
Size: 251 Color: 265

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8711
Size: 288 Color: 3464
Size: 285 Color: 3297

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7212
Size: 341 Color: 6079
Size: 288 Color: 3486

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8253
Size: 330 Color: 5655
Size: 261 Color: 1359

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6692
Size: 358 Color: 6673
Size: 285 Color: 3275

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8056
Size: 333 Color: 5748
Size: 266 Color: 1787

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9501
Size: 270 Color: 2158
Size: 262 Color: 1494

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8680
Size: 288 Color: 3434
Size: 287 Color: 3384

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8431
Size: 306 Color: 4484
Size: 279 Color: 2891

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8058
Size: 312 Color: 4788
Size: 287 Color: 3402

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8853
Size: 315 Color: 4938
Size: 251 Color: 291

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8256
Size: 304 Color: 4411
Size: 287 Color: 3410

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7778
Size: 358 Color: 6662
Size: 251 Color: 300

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7193
Size: 363 Color: 6870
Size: 266 Color: 1806

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8823
Size: 307 Color: 4543
Size: 260 Color: 1249

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8383
Size: 323 Color: 5337
Size: 264 Color: 1581

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9354
Size: 271 Color: 2223
Size: 269 Color: 2105

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9120
Size: 290 Color: 3612
Size: 262 Color: 1430

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7145
Size: 325 Color: 5416
Size: 305 Color: 4449

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8036
Size: 333 Color: 5746
Size: 267 Color: 1882

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8179
Size: 337 Color: 5907
Size: 257 Color: 1006

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7825
Size: 308 Color: 4601
Size: 299 Color: 4118

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8634
Size: 296 Color: 3951
Size: 281 Color: 3009

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8592
Size: 326 Color: 5472
Size: 252 Color: 411

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9187
Size: 281 Color: 3014
Size: 268 Color: 1956

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7589
Size: 318 Color: 5095
Size: 297 Color: 4017

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7232
Size: 332 Color: 5712
Size: 296 Color: 3976

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7045
Size: 368 Color: 7037
Size: 265 Color: 1701

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8538
Size: 330 Color: 5647
Size: 251 Color: 222

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9333
Size: 284 Color: 3180
Size: 258 Color: 1137

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8484
Size: 301 Color: 4252
Size: 282 Color: 3042

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8467
Size: 332 Color: 5702
Size: 251 Color: 264

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6519
Size: 353 Color: 6513
Size: 295 Color: 3889

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9837
Size: 256 Color: 898
Size: 255 Color: 748

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6521
Size: 353 Color: 6512
Size: 295 Color: 3917

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9037
Size: 305 Color: 4458
Size: 252 Color: 428

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7552
Size: 315 Color: 4948
Size: 302 Color: 4284

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8792
Size: 287 Color: 3371
Size: 282 Color: 3045

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9890
Size: 255 Color: 745
Size: 252 Color: 373

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6935
Size: 320 Color: 5198
Size: 316 Color: 4984

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9507
Size: 266 Color: 1813
Size: 266 Color: 1800

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8530
Size: 321 Color: 5201
Size: 260 Color: 1319

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9465
Size: 280 Color: 2926
Size: 255 Color: 731

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9481
Size: 274 Color: 2447
Size: 260 Color: 1258

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6872
Size: 328 Color: 5532
Size: 310 Color: 4709

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7413
Size: 365 Color: 6924
Size: 257 Color: 1020

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9616
Size: 266 Color: 1793
Size: 260 Color: 1307

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9781
Size: 259 Color: 1241
Size: 256 Color: 943

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9739
Size: 260 Color: 1309
Size: 257 Color: 1018

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7405
Size: 354 Color: 6545
Size: 268 Color: 1966

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9330
Size: 276 Color: 2617
Size: 267 Color: 1907

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6527
Size: 353 Color: 6510
Size: 295 Color: 3874

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8963
Size: 295 Color: 3888
Size: 266 Color: 1812

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8828
Size: 298 Color: 4052
Size: 269 Color: 2120

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9731
Size: 259 Color: 1155
Size: 259 Color: 1151

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9625
Size: 270 Color: 2189
Size: 255 Color: 751

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9071
Size: 298 Color: 4041
Size: 257 Color: 1051

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7671
Size: 343 Color: 6153
Size: 269 Color: 2075

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8619
Size: 304 Color: 4409
Size: 273 Color: 2370

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6859
Size: 329 Color: 5587
Size: 309 Color: 4668

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 9721
Size: 260 Color: 1251
Size: 259 Color: 1199

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9026
Size: 305 Color: 4437
Size: 253 Color: 500

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6800
Size: 339 Color: 5964
Size: 301 Color: 4220

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8180
Size: 305 Color: 4454
Size: 289 Color: 3558

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7207
Size: 322 Color: 5260
Size: 307 Color: 4510

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9322
Size: 279 Color: 2851
Size: 264 Color: 1654

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9631
Size: 268 Color: 2013
Size: 257 Color: 987

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7749
Size: 309 Color: 4676
Size: 301 Color: 4202

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7319
Size: 348 Color: 6342
Size: 277 Color: 2741

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6879
Size: 326 Color: 5466
Size: 312 Color: 4806

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 9990
Size: 250 Color: 104
Size: 250 Color: 93

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7051
Size: 340 Color: 6006
Size: 293 Color: 3803

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8325
Size: 330 Color: 5656
Size: 259 Color: 1228

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 5985
Size: 335 Color: 5837
Size: 327 Color: 5502

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7238
Size: 369 Color: 7075
Size: 259 Color: 1239

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 9977
Size: 251 Color: 247
Size: 250 Color: 64

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8197
Size: 334 Color: 5802
Size: 259 Color: 1183

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6560
Size: 354 Color: 6542
Size: 293 Color: 3754

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9674
Size: 262 Color: 1459
Size: 260 Color: 1284

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8114
Size: 300 Color: 4162
Size: 296 Color: 3938

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8885
Size: 297 Color: 3993
Size: 267 Color: 1937

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7312
Size: 348 Color: 6352
Size: 277 Color: 2756

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8223
Size: 340 Color: 6009
Size: 252 Color: 343

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9800
Size: 263 Color: 1526
Size: 251 Color: 321

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8005
Size: 309 Color: 4627
Size: 292 Color: 3726

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6746
Size: 358 Color: 6672
Size: 283 Color: 3115

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9762
Size: 259 Color: 1190
Size: 257 Color: 1056

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6458
Size: 351 Color: 6455
Size: 299 Color: 4101

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6887
Size: 328 Color: 5540
Size: 310 Color: 4696

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9344
Size: 277 Color: 2702
Size: 264 Color: 1612

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8930
Size: 300 Color: 4174
Size: 262 Color: 1464

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8697
Size: 292 Color: 3699
Size: 282 Color: 3076

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7260
Size: 349 Color: 6388
Size: 278 Color: 2786

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9203
Size: 288 Color: 3445
Size: 260 Color: 1312

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7096
Size: 344 Color: 6173
Size: 288 Color: 3448

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7708
Size: 321 Color: 5200
Size: 290 Color: 3594

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7798
Size: 341 Color: 6042
Size: 267 Color: 1939

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9554
Size: 269 Color: 2131
Size: 260 Color: 1286

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8869
Size: 289 Color: 3507
Size: 276 Color: 2659

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8933
Size: 312 Color: 4801
Size: 250 Color: 123

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6659
Size: 337 Color: 5887
Size: 306 Color: 4477

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9914
Size: 254 Color: 636
Size: 251 Color: 205

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9362
Size: 271 Color: 2246
Size: 269 Color: 2100

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7480
Size: 346 Color: 6286
Size: 273 Color: 2399

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7131
Size: 327 Color: 5524
Size: 304 Color: 4369

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8994
Size: 282 Color: 3064
Size: 277 Color: 2695

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6768
Size: 360 Color: 6767
Size: 281 Color: 2992

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6883
Size: 350 Color: 6412
Size: 288 Color: 3489

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8025
Size: 301 Color: 4246
Size: 300 Color: 4175

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 9996
Size: 500 Color: 9983

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9895
Size: 253 Color: 553
Size: 253 Color: 536

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8837
Size: 308 Color: 4583
Size: 259 Color: 1185

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9142
Size: 288 Color: 3465
Size: 263 Color: 1506

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9227
Size: 275 Color: 2598
Size: 272 Color: 2307

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9292
Size: 279 Color: 2895
Size: 265 Color: 1740

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7861
Size: 316 Color: 4972
Size: 290 Color: 3597

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9215
Size: 274 Color: 2488
Size: 274 Color: 2444

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6901
Size: 324 Color: 5351
Size: 313 Color: 4836

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7016
Size: 350 Color: 6416
Size: 284 Color: 3226

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9577
Size: 269 Color: 2066
Size: 259 Color: 1163

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7347
Size: 361 Color: 6788
Size: 263 Color: 1575

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8204
Size: 336 Color: 5872
Size: 257 Color: 994

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8589
Size: 290 Color: 3589
Size: 288 Color: 3470

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9586
Size: 272 Color: 2290
Size: 255 Color: 744

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8349
Size: 333 Color: 5751
Size: 255 Color: 752

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7159
Size: 353 Color: 6534
Size: 277 Color: 2772

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9546
Size: 279 Color: 2887
Size: 251 Color: 240

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9923
Size: 255 Color: 821
Size: 250 Color: 71

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6477
Size: 351 Color: 6462
Size: 299 Color: 4115

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6991
Size: 319 Color: 5137
Size: 316 Color: 4961

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7719
Size: 343 Color: 6168
Size: 268 Color: 1992

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9798
Size: 261 Color: 1384
Size: 253 Color: 542

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9212
Size: 295 Color: 3898
Size: 253 Color: 522

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8158
Size: 303 Color: 4355
Size: 292 Color: 3731

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7755
Size: 324 Color: 5363
Size: 285 Color: 3251

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6761
Size: 360 Color: 6740
Size: 281 Color: 3022

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8596
Size: 308 Color: 4580
Size: 270 Color: 2164

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9659
Size: 271 Color: 2248
Size: 252 Color: 462

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9198
Size: 277 Color: 2739
Size: 271 Color: 2212

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8558
Size: 302 Color: 4277
Size: 278 Color: 2790

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7085
Size: 325 Color: 5404
Size: 307 Color: 4554

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7957
Size: 323 Color: 5303
Size: 280 Color: 2917

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7419
Size: 346 Color: 6257
Size: 276 Color: 2645

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9355
Size: 276 Color: 2615
Size: 264 Color: 1579

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7540
Size: 317 Color: 5008
Size: 300 Color: 4186

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6897
Size: 322 Color: 5265
Size: 315 Color: 4935

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7344
Size: 371 Color: 7178
Size: 254 Color: 698

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7983
Size: 337 Color: 5902
Size: 265 Color: 1732

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7531
Size: 345 Color: 6220
Size: 273 Color: 2432

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9853
Size: 257 Color: 1054
Size: 252 Color: 345

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7292
Size: 316 Color: 4974
Size: 310 Color: 4690

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8229
Size: 340 Color: 6020
Size: 252 Color: 384

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8590
Size: 308 Color: 4576
Size: 270 Color: 2191

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6655
Size: 357 Color: 6649
Size: 287 Color: 3398

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9600
Size: 275 Color: 2539
Size: 252 Color: 380

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9061
Size: 278 Color: 2805
Size: 278 Color: 2788

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8267
Size: 340 Color: 6019
Size: 250 Color: 167

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9305
Size: 278 Color: 2818
Size: 266 Color: 1822

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7867
Size: 326 Color: 5475
Size: 280 Color: 2932

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9267
Size: 288 Color: 3432
Size: 258 Color: 1067

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9856
Size: 255 Color: 762
Size: 254 Color: 681

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8392
Size: 301 Color: 4219
Size: 285 Color: 3266

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7982
Size: 313 Color: 4858
Size: 289 Color: 3562

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9900
Size: 254 Color: 687
Size: 252 Color: 443

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9378
Size: 284 Color: 3241
Size: 255 Color: 737

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8534
Size: 304 Color: 4395
Size: 277 Color: 2768

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9377
Size: 287 Color: 3423
Size: 252 Color: 362

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8318
Size: 310 Color: 4697
Size: 279 Color: 2868

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7056
Size: 346 Color: 6290
Size: 287 Color: 3388

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9593
Size: 276 Color: 2639
Size: 251 Color: 237

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9455
Size: 268 Color: 2041
Size: 267 Color: 1917

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9046
Size: 278 Color: 2835
Size: 278 Color: 2828

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8133
Size: 336 Color: 5841
Size: 260 Color: 1243

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8557
Size: 321 Color: 5217
Size: 259 Color: 1195

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6529
Size: 353 Color: 6525
Size: 295 Color: 3921

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7080
Size: 335 Color: 5826
Size: 297 Color: 4022

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7926
Size: 305 Color: 4436
Size: 299 Color: 4129

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7299
Size: 329 Color: 5592
Size: 297 Color: 4008

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8294
Size: 339 Color: 5995
Size: 250 Color: 191

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9767
Size: 258 Color: 1091
Size: 257 Color: 963

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9438
Size: 280 Color: 2967
Size: 256 Color: 935

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8486
Size: 316 Color: 4977
Size: 267 Color: 1949

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9257
Size: 283 Color: 3135
Size: 263 Color: 1520

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7186
Size: 328 Color: 5570
Size: 301 Color: 4228

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7209
Size: 349 Color: 6401
Size: 280 Color: 2956

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7343
Size: 348 Color: 6332
Size: 277 Color: 2694

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9328
Size: 292 Color: 3704
Size: 251 Color: 288

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9594
Size: 274 Color: 2469
Size: 253 Color: 475

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6635
Size: 357 Color: 6633
Size: 287 Color: 3366

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8758
Size: 315 Color: 4919
Size: 256 Color: 904

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8321
Size: 311 Color: 4775
Size: 278 Color: 2808

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9287
Size: 293 Color: 3791
Size: 251 Color: 319

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8950
Size: 309 Color: 4620
Size: 252 Color: 456

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8563
Size: 324 Color: 5360
Size: 256 Color: 879

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8687
Size: 322 Color: 5294
Size: 252 Color: 422

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7637
Size: 321 Color: 5215
Size: 293 Color: 3780

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8737
Size: 319 Color: 5141
Size: 253 Color: 546

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8476
Size: 333 Color: 5769
Size: 250 Color: 24

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7476
Size: 328 Color: 5546
Size: 292 Color: 3702

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8929
Size: 298 Color: 4084
Size: 264 Color: 1636

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8298
Size: 320 Color: 5169
Size: 269 Color: 2080

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9218
Size: 295 Color: 3907
Size: 253 Color: 470

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 9717
Size: 263 Color: 1514
Size: 256 Color: 907

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8307
Size: 339 Color: 5968
Size: 250 Color: 87

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7526
Size: 345 Color: 6215
Size: 273 Color: 2424

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7970
Size: 337 Color: 5909
Size: 265 Color: 1686

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9240
Size: 284 Color: 3217
Size: 263 Color: 1534

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8925
Size: 292 Color: 3703
Size: 270 Color: 2201

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8664
Size: 316 Color: 4987
Size: 260 Color: 1339

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8660
Size: 311 Color: 4767
Size: 265 Color: 1712

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9550
Size: 273 Color: 2425
Size: 257 Color: 975

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9911
Size: 255 Color: 727
Size: 250 Color: 180

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6491
Size: 352 Color: 6486
Size: 297 Color: 4009

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7804
Size: 341 Color: 6050
Size: 267 Color: 1871

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8021
Size: 320 Color: 5194
Size: 281 Color: 3028

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8510
Size: 310 Color: 4713
Size: 272 Color: 2350

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7135
Size: 327 Color: 5528
Size: 304 Color: 4377

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9054
Size: 303 Color: 4353
Size: 253 Color: 592

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6670
Size: 358 Color: 6661
Size: 285 Color: 3305

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8766
Size: 320 Color: 5170
Size: 250 Color: 101

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8786
Size: 319 Color: 5142
Size: 250 Color: 8

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8521
Size: 309 Color: 4661
Size: 273 Color: 2414

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9557
Size: 271 Color: 2224
Size: 258 Color: 1135

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7740
Size: 342 Color: 6105
Size: 268 Color: 1972

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8779
Size: 314 Color: 4903
Size: 255 Color: 736

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8038
Size: 349 Color: 6384
Size: 251 Color: 202

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7399
Size: 311 Color: 4779
Size: 311 Color: 4778

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9296
Size: 276 Color: 2612
Size: 268 Color: 2015

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7917
Size: 314 Color: 4907
Size: 290 Color: 3573

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7757
Size: 309 Color: 4652
Size: 300 Color: 4181

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9863
Size: 259 Color: 1238
Size: 250 Color: 67

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6828
Size: 330 Color: 5660
Size: 309 Color: 4616

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7954
Size: 348 Color: 6340
Size: 255 Color: 761

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6723
Size: 359 Color: 6720
Size: 283 Color: 3145

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7566
Size: 320 Color: 5197
Size: 296 Color: 3954

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8499
Size: 317 Color: 5009
Size: 265 Color: 1752

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7375
Size: 329 Color: 5593
Size: 294 Color: 3838

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8171
Size: 300 Color: 4166
Size: 294 Color: 3822

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9146
Size: 283 Color: 3144
Size: 268 Color: 2030

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9701
Size: 270 Color: 2138
Size: 250 Color: 4

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9754
Size: 259 Color: 1223
Size: 257 Color: 1041

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8368
Size: 333 Color: 5762
Size: 254 Color: 669

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7140
Size: 352 Color: 6505
Size: 278 Color: 2781

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8588
Size: 315 Color: 4952
Size: 263 Color: 1503

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6854
Size: 328 Color: 5573
Size: 310 Color: 4694

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7889
Size: 327 Color: 5505
Size: 278 Color: 2782

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6452
Size: 345 Color: 6218
Size: 305 Color: 4434

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6902
Size: 331 Color: 5672
Size: 306 Color: 4472

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8391
Size: 314 Color: 4892
Size: 272 Color: 2319

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6921
Size: 323 Color: 5345
Size: 314 Color: 4913

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7975
Size: 318 Color: 5067
Size: 284 Color: 3179

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6760
Size: 360 Color: 6747
Size: 281 Color: 3031

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9081
Size: 297 Color: 4035
Size: 257 Color: 1047

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9044
Size: 286 Color: 3334
Size: 271 Color: 2225

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8942
Size: 285 Color: 3288
Size: 277 Color: 2760

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9759
Size: 263 Color: 1570
Size: 253 Color: 554

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8362
Size: 297 Color: 3992
Size: 290 Color: 3616

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7127
Size: 370 Color: 7120
Size: 261 Color: 1375

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8271
Size: 338 Color: 5958
Size: 252 Color: 447

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8830
Size: 317 Color: 5028
Size: 250 Color: 146

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8541
Size: 291 Color: 3662
Size: 290 Color: 3581

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8597
Size: 301 Color: 4253
Size: 277 Color: 2755

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8159
Size: 334 Color: 5779
Size: 261 Color: 1408

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7962
Size: 349 Color: 6406
Size: 254 Color: 712

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7455
Size: 346 Color: 6253
Size: 274 Color: 2475

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9028
Size: 308 Color: 4611
Size: 250 Color: 112

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6941
Size: 320 Color: 5164
Size: 316 Color: 4965

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7461
Size: 346 Color: 6289
Size: 274 Color: 2489

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8346
Size: 325 Color: 5411
Size: 263 Color: 1543

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8628
Size: 293 Color: 3765
Size: 284 Color: 3230

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6971
Size: 318 Color: 5079
Size: 317 Color: 5031

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7898
Size: 323 Color: 5334
Size: 282 Color: 3063

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7196
Size: 323 Color: 5322
Size: 306 Color: 4492

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7507
Size: 345 Color: 6214
Size: 273 Color: 2431

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9461
Size: 275 Color: 2599
Size: 260 Color: 1273

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9871
Size: 257 Color: 1023
Size: 251 Color: 312

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9440
Size: 271 Color: 2242
Size: 265 Color: 1744

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6686
Size: 347 Color: 6295
Size: 296 Color: 3978

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7050
Size: 339 Color: 5987
Size: 294 Color: 3841

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 6132
Size: 343 Color: 6130
Size: 315 Color: 4940

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6934
Size: 364 Color: 6898
Size: 272 Color: 2320

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 6995
Size: 319 Color: 5147
Size: 315 Color: 4929

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8024
Size: 310 Color: 4729
Size: 291 Color: 3689

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9775
Size: 259 Color: 1204
Size: 256 Color: 882

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9664
Size: 265 Color: 1683
Size: 258 Color: 1089

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9040
Size: 301 Color: 4245
Size: 256 Color: 884

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6952
Size: 351 Color: 6460
Size: 285 Color: 3313

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9108
Size: 281 Color: 2997
Size: 272 Color: 2296

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7658
Size: 362 Color: 6813
Size: 251 Color: 294

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7064
Size: 335 Color: 5825
Size: 297 Color: 4029

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8876
Size: 290 Color: 3618
Size: 275 Color: 2579

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9732
Size: 262 Color: 1447
Size: 256 Color: 905

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9062
Size: 281 Color: 2984
Size: 275 Color: 2586

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7704
Size: 309 Color: 4630
Size: 302 Color: 4303

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8875
Size: 303 Color: 4334
Size: 262 Color: 1499

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7847
Size: 318 Color: 5074
Size: 289 Color: 3529

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8470
Size: 316 Color: 4985
Size: 267 Color: 1920

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9520
Size: 273 Color: 2390
Size: 258 Color: 1069

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9334
Size: 292 Color: 3695
Size: 250 Color: 43

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8843
Size: 296 Color: 3940
Size: 270 Color: 2183

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6773
Size: 360 Color: 6763
Size: 281 Color: 2977

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9535
Size: 268 Color: 1987
Size: 262 Color: 1473

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9710
Size: 266 Color: 1771
Size: 254 Color: 612

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6978
Size: 339 Color: 5990
Size: 296 Color: 3928

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8064
Size: 334 Color: 5784
Size: 264 Color: 1593

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8890
Size: 311 Color: 4749
Size: 253 Color: 514

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7163
Size: 350 Color: 6414
Size: 280 Color: 2910

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9034
Size: 298 Color: 4095
Size: 259 Color: 1213

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7665
Size: 326 Color: 5469
Size: 287 Color: 3390

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9967
Size: 252 Color: 445
Size: 250 Color: 95

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7581
Size: 329 Color: 5582
Size: 287 Color: 3368

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7848
Size: 316 Color: 4999
Size: 291 Color: 3645

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9541
Size: 274 Color: 2477
Size: 256 Color: 865

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9473
Size: 283 Color: 3123
Size: 251 Color: 284

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8863
Size: 298 Color: 4043
Size: 267 Color: 1942

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6751
Size: 360 Color: 6744
Size: 281 Color: 2976

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9555
Size: 265 Color: 1705
Size: 264 Color: 1599

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7595
Size: 344 Color: 6192
Size: 271 Color: 2270

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8224
Size: 336 Color: 5842
Size: 256 Color: 950

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8512
Size: 308 Color: 4568
Size: 274 Color: 2507

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7564
Size: 344 Color: 6191
Size: 272 Color: 2299

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9821
Size: 259 Color: 1198
Size: 253 Color: 595

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9791
Size: 258 Color: 1110
Size: 256 Color: 945

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7854
Size: 326 Color: 5463
Size: 281 Color: 3018

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8289
Size: 338 Color: 5963
Size: 252 Color: 383

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8802
Size: 290 Color: 3615
Size: 279 Color: 2889

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8553
Size: 299 Color: 4132
Size: 281 Color: 2995

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7641
Size: 311 Color: 4737
Size: 302 Color: 4306

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6937
Size: 321 Color: 5227
Size: 315 Color: 4942

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8618
Size: 322 Color: 5281
Size: 255 Color: 820

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6559
Size: 354 Color: 6551
Size: 293 Color: 3805

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 6999
Size: 338 Color: 5935
Size: 296 Color: 3975

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7022
Size: 321 Color: 5247
Size: 313 Color: 4834

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8163
Size: 336 Color: 5867
Size: 259 Color: 1186

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9532
Size: 278 Color: 2798
Size: 253 Color: 489

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8305
Size: 334 Color: 5812
Size: 255 Color: 852

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9916
Size: 255 Color: 757
Size: 250 Color: 28

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6588
Size: 355 Color: 6567
Size: 291 Color: 3640

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9621
Size: 272 Color: 2337
Size: 254 Color: 693

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8951
Size: 310 Color: 4731
Size: 251 Color: 286

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7116
Size: 365 Color: 6954
Size: 266 Color: 1786

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9031
Size: 295 Color: 3881
Size: 262 Color: 1486

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7668
Size: 316 Color: 4997
Size: 297 Color: 3997

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8076
Size: 343 Color: 6147
Size: 255 Color: 839

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7839
Size: 318 Color: 5104
Size: 289 Color: 3533

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6496
Size: 352 Color: 6485
Size: 297 Color: 3999

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8070
Size: 344 Color: 6194
Size: 254 Color: 631

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6647
Size: 357 Color: 6630
Size: 287 Color: 3363

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9066
Size: 303 Color: 4354
Size: 253 Color: 479

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9256
Size: 286 Color: 3323
Size: 260 Color: 1268

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7134
Size: 350 Color: 6444
Size: 281 Color: 2993

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8519
Size: 329 Color: 5588
Size: 253 Color: 544

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9660
Size: 271 Color: 2275
Size: 252 Color: 416

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9971
Size: 252 Color: 423
Size: 250 Color: 48

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8164
Size: 340 Color: 6008
Size: 255 Color: 849

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6985
Size: 318 Color: 5097
Size: 317 Color: 5027

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7950
Size: 323 Color: 5320
Size: 280 Color: 2941

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9228
Size: 287 Color: 3421
Size: 260 Color: 1314

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9622
Size: 265 Color: 1718
Size: 261 Color: 1369

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8902
Size: 297 Color: 4018
Size: 266 Color: 1835

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7386
Size: 324 Color: 5358
Size: 299 Color: 4124

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7575
Size: 344 Color: 6195
Size: 272 Color: 2301

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9162
Size: 284 Color: 3239
Size: 266 Color: 1830

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9510
Size: 268 Color: 1974
Size: 264 Color: 1650

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8662
Size: 324 Color: 5397
Size: 252 Color: 336

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8284
Size: 338 Color: 5959
Size: 252 Color: 360

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8749
Size: 296 Color: 3974
Size: 275 Color: 2533

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7609
Size: 344 Color: 6179
Size: 271 Color: 2230

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8079
Size: 310 Color: 4722
Size: 288 Color: 3463

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6988
Size: 319 Color: 5131
Size: 316 Color: 4976

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9281
Size: 274 Color: 2509
Size: 271 Color: 2263

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9489
Size: 272 Color: 2297
Size: 261 Color: 1410

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8160
Size: 314 Color: 4878
Size: 281 Color: 3010

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8299
Size: 331 Color: 5674
Size: 258 Color: 1086

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6858
Size: 326 Color: 5440
Size: 312 Color: 4802

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8026
Size: 333 Color: 5735
Size: 267 Color: 1861

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9833
Size: 260 Color: 1305
Size: 251 Color: 314

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6964
Size: 319 Color: 5123
Size: 316 Color: 5003

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7475
Size: 346 Color: 6272
Size: 274 Color: 2462

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9024
Size: 305 Color: 4443
Size: 253 Color: 602

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7802
Size: 318 Color: 5105
Size: 290 Color: 3613

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8989
Size: 283 Color: 3143
Size: 276 Color: 2651

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9831
Size: 260 Color: 1265
Size: 251 Color: 242

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7483
Size: 318 Color: 5068
Size: 301 Color: 4255

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9964
Size: 251 Color: 323
Size: 251 Color: 198

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6890
Size: 362 Color: 6843
Size: 276 Color: 2683

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7162
Size: 344 Color: 6201
Size: 286 Color: 3349

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9899
Size: 253 Color: 594
Size: 253 Color: 576

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7527
Size: 353 Color: 6523
Size: 265 Color: 1690

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9327
Size: 272 Color: 2304
Size: 271 Color: 2247

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9610
Size: 272 Color: 2310
Size: 254 Color: 679

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7881
Size: 333 Color: 5730
Size: 272 Color: 2283

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8259
Size: 340 Color: 6028
Size: 251 Color: 246

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7103
Size: 327 Color: 5513
Size: 304 Color: 4386

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7089
Size: 325 Color: 5401
Size: 307 Color: 4527

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8847
Size: 303 Color: 4351
Size: 263 Color: 1563

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7788
Size: 344 Color: 6183
Size: 264 Color: 1597

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7990
Size: 334 Color: 5810
Size: 268 Color: 1965

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 9722
Size: 269 Color: 2096
Size: 250 Color: 162

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9840
Size: 259 Color: 1167
Size: 251 Color: 200

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8290
Size: 330 Color: 5640
Size: 260 Color: 1256

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9504
Size: 275 Color: 2536
Size: 257 Color: 959

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7359
Size: 323 Color: 5300
Size: 301 Color: 4211

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8637
Size: 293 Color: 3758
Size: 283 Color: 3111

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8017
Size: 327 Color: 5514
Size: 274 Color: 2497

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7191
Size: 327 Color: 5487
Size: 302 Color: 4304

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9335
Size: 277 Color: 2769
Size: 265 Color: 1734

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6848
Size: 351 Color: 6483
Size: 287 Color: 3374

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8677
Size: 288 Color: 3498
Size: 287 Color: 3395

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8123
Size: 341 Color: 6084
Size: 255 Color: 812

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7660
Size: 343 Color: 6145
Size: 270 Color: 2137

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7234
Size: 349 Color: 6407
Size: 279 Color: 2904

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9704
Size: 262 Color: 1470
Size: 258 Color: 1074

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9278
Size: 282 Color: 3055
Size: 263 Color: 1565

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9472
Size: 269 Color: 2112
Size: 265 Color: 1703

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7815
Size: 341 Color: 6068
Size: 267 Color: 1944

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7756
Size: 341 Color: 6064
Size: 268 Color: 1954

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9015
Size: 307 Color: 4539
Size: 251 Color: 325

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9369
Size: 276 Color: 2687
Size: 264 Color: 1643

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9112
Size: 284 Color: 3203
Size: 269 Color: 2127

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7645
Size: 362 Color: 6832
Size: 251 Color: 317

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8981
Size: 283 Color: 3150
Size: 277 Color: 2707

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7936
Size: 311 Color: 4753
Size: 293 Color: 3774

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8207
Size: 337 Color: 5901
Size: 256 Color: 860

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8008
Size: 308 Color: 4598
Size: 293 Color: 3800

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7468
Size: 367 Color: 7007
Size: 253 Color: 519

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9558
Size: 275 Color: 2549
Size: 254 Color: 647

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8061
Size: 333 Color: 5766
Size: 266 Color: 1797

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7971
Size: 336 Color: 5860
Size: 266 Color: 1773

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7522
Size: 328 Color: 5561
Size: 290 Color: 3611

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8382
Size: 299 Color: 4121
Size: 288 Color: 3496

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9692
Size: 262 Color: 1502
Size: 259 Color: 1173

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6950
Size: 349 Color: 6410
Size: 287 Color: 3386

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8972
Size: 306 Color: 4501
Size: 254 Color: 662

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6797
Size: 336 Color: 5864
Size: 304 Color: 4384

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6628
Size: 332 Color: 5717
Size: 312 Color: 4814

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7497
Size: 346 Color: 6256
Size: 273 Color: 2373

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6644
Size: 357 Color: 6634
Size: 287 Color: 3373

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7634
Size: 326 Color: 5445
Size: 288 Color: 3460

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9232
Size: 282 Color: 3054
Size: 265 Color: 1714

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9778
Size: 263 Color: 1574
Size: 252 Color: 397

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9293
Size: 292 Color: 3723
Size: 252 Color: 341

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7258
Size: 315 Color: 4915
Size: 312 Color: 4794

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8165
Size: 343 Color: 6161
Size: 251 Color: 256

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8776
Size: 319 Color: 5149
Size: 251 Color: 234

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9416
Size: 283 Color: 3140
Size: 255 Color: 847

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7946
Size: 315 Color: 4951
Size: 288 Color: 3433

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9478
Size: 273 Color: 2372
Size: 261 Color: 1343

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7972
Size: 343 Color: 6120
Size: 259 Color: 1170

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9678
Size: 261 Color: 1405
Size: 261 Color: 1383

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9970
Size: 252 Color: 427
Size: 250 Color: 7

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9490
Size: 280 Color: 2930
Size: 253 Color: 564

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7528
Size: 345 Color: 6230
Size: 273 Color: 2415

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7218
Size: 364 Color: 6912
Size: 264 Color: 1606

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9105
Size: 278 Color: 2797
Size: 275 Color: 2532

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8746
Size: 308 Color: 4597
Size: 263 Color: 1541

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7567
Size: 344 Color: 6181
Size: 272 Color: 2339

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8616
Size: 318 Color: 5092
Size: 259 Color: 1219

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6469
Size: 351 Color: 6447
Size: 299 Color: 4147

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9878
Size: 254 Color: 710
Size: 254 Color: 670

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9177
Size: 277 Color: 2718
Size: 272 Color: 2340

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8600
Size: 312 Color: 4809
Size: 266 Color: 1766

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9098
Size: 298 Color: 4094
Size: 256 Color: 938

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8468
Size: 328 Color: 5547
Size: 255 Color: 788

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9348
Size: 285 Color: 3264
Size: 256 Color: 899

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7868
Size: 321 Color: 5213
Size: 285 Color: 3273

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7784
Size: 305 Color: 4457
Size: 304 Color: 4406

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8227
Size: 330 Color: 5646
Size: 262 Color: 1453

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9588
Size: 270 Color: 2147
Size: 257 Color: 1043

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7031
Size: 322 Color: 5279
Size: 311 Color: 4738

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8973
Size: 284 Color: 3215
Size: 276 Color: 2674

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8745
Size: 303 Color: 4362
Size: 268 Color: 2016

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8889
Size: 307 Color: 4545
Size: 257 Color: 991

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6615
Size: 356 Color: 6594
Size: 289 Color: 3545

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7728
Size: 342 Color: 6096
Size: 268 Color: 1994

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8578
Size: 319 Color: 5127
Size: 260 Color: 1252

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6758
Size: 360 Color: 6750
Size: 281 Color: 3004

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6983
Size: 318 Color: 5080
Size: 317 Color: 5025

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7733
Size: 342 Color: 6088
Size: 268 Color: 1975

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8648
Size: 312 Color: 4787
Size: 264 Color: 1590

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9794
Size: 262 Color: 1487
Size: 252 Color: 361

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8812
Size: 307 Color: 4517
Size: 261 Color: 1342

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7124
Size: 327 Color: 5496
Size: 304 Color: 4383

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9522
Size: 280 Color: 2958
Size: 251 Color: 271

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7076
Size: 325 Color: 5431
Size: 307 Color: 4537

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7417
Size: 346 Color: 6247
Size: 276 Color: 2614

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8428
Size: 298 Color: 4083
Size: 287 Color: 3369

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8936
Size: 305 Color: 4422
Size: 257 Color: 972

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8035
Size: 332 Color: 5723
Size: 268 Color: 2005

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8577
Size: 310 Color: 4711
Size: 269 Color: 2078

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6632
Size: 322 Color: 5298
Size: 322 Color: 5254

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8741
Size: 315 Color: 4945
Size: 257 Color: 986

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9389
Size: 272 Color: 2348
Size: 267 Color: 1892

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9649
Size: 274 Color: 2529
Size: 250 Color: 53

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7824
Size: 325 Color: 5435
Size: 282 Color: 3053

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9591
Size: 276 Color: 2637
Size: 251 Color: 287

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7109
Size: 327 Color: 5518
Size: 304 Color: 4387

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 7996
Size: 331 Color: 5663
Size: 270 Color: 2205

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9694
Size: 266 Color: 1765
Size: 255 Color: 843

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9437
Size: 281 Color: 3020
Size: 255 Color: 753

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9893
Size: 253 Color: 583
Size: 253 Color: 571

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8480
Size: 295 Color: 3916
Size: 288 Color: 3473

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7316
Size: 348 Color: 6353
Size: 277 Color: 2747

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9393
Size: 276 Color: 2682
Size: 262 Color: 1484

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7852
Size: 315 Color: 4943
Size: 292 Color: 3717

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8148
Size: 339 Color: 5965
Size: 256 Color: 886

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8264
Size: 340 Color: 6000
Size: 251 Color: 243

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7626
Size: 329 Color: 5609
Size: 285 Color: 3265

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8292
Size: 302 Color: 4280
Size: 288 Color: 3480

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9303
Size: 286 Color: 3350
Size: 258 Color: 1066

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9347
Size: 282 Color: 3100
Size: 259 Color: 1231

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9635
Size: 273 Color: 2418
Size: 252 Color: 438

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8477
Size: 325 Color: 5421
Size: 258 Color: 1064

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8893
Size: 290 Color: 3566
Size: 274 Color: 2448

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6809
Size: 351 Color: 6481
Size: 288 Color: 3468

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6736
Size: 359 Color: 6705
Size: 283 Color: 3172

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8263
Size: 337 Color: 5926
Size: 254 Color: 633

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6818
Size: 340 Color: 6012
Size: 299 Color: 4105

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8552
Size: 291 Color: 3628
Size: 289 Color: 3518

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9561
Size: 275 Color: 2581
Size: 254 Color: 716

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6601
Size: 356 Color: 6596
Size: 289 Color: 3536

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6565
Size: 354 Color: 6552
Size: 293 Color: 3751

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9266
Size: 285 Color: 3250
Size: 261 Color: 1358

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7340
Size: 336 Color: 5876
Size: 289 Color: 3540

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8488
Size: 316 Color: 4989
Size: 267 Color: 1915

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7168
Size: 371 Color: 7167
Size: 259 Color: 1193

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9138
Size: 298 Color: 4050
Size: 253 Color: 524

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6702
Size: 358 Color: 6675
Size: 285 Color: 3290

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8631
Size: 326 Color: 5449
Size: 251 Color: 302

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9010
Size: 282 Color: 3073
Size: 276 Color: 2655

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6563
Size: 354 Color: 6543
Size: 293 Color: 3747

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7879
Size: 309 Color: 4641
Size: 296 Color: 3932

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8554
Size: 325 Color: 5430
Size: 255 Color: 793

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7627
Size: 322 Color: 5291
Size: 292 Color: 3691

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6811
Size: 358 Color: 6684
Size: 281 Color: 3007

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8060
Size: 334 Color: 5770
Size: 265 Color: 1722

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7155
Size: 333 Color: 5753
Size: 297 Color: 4013

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6631
Size: 357 Color: 6629
Size: 287 Color: 3393

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6940
Size: 364 Color: 6909
Size: 272 Color: 2349

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8147
Size: 336 Color: 5868
Size: 259 Color: 1161

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7679
Size: 310 Color: 4730
Size: 302 Color: 4312

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7973
Size: 342 Color: 6114
Size: 260 Color: 1323

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9262
Size: 288 Color: 3474
Size: 258 Color: 1100

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8441
Size: 331 Color: 5671
Size: 254 Color: 628

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9202
Size: 274 Color: 2515
Size: 274 Color: 2451

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9157
Size: 295 Color: 3886
Size: 255 Color: 756

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9181
Size: 282 Color: 3050
Size: 267 Color: 1911

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9056
Size: 279 Color: 2848
Size: 277 Color: 2759

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8881
Size: 301 Color: 4249
Size: 263 Color: 1517

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7584
Size: 365 Color: 6948
Size: 250 Color: 10

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7546
Size: 324 Color: 5353
Size: 293 Color: 3798

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8705
Size: 311 Color: 4756
Size: 262 Color: 1482

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9017
Size: 283 Color: 3141
Size: 275 Color: 2543

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6807
Size: 330 Color: 5659
Size: 309 Color: 4650

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8912
Size: 297 Color: 3982
Size: 266 Color: 1809

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6868
Size: 356 Color: 6623
Size: 282 Color: 3107

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7094
Size: 325 Color: 5434
Size: 307 Color: 4541

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7328
Size: 348 Color: 6368
Size: 277 Color: 2700

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8218
Size: 332 Color: 5708
Size: 260 Color: 1289

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8569
Size: 301 Color: 4213
Size: 279 Color: 2867

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8693
Size: 323 Color: 5333
Size: 251 Color: 235

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8910
Size: 283 Color: 3152
Size: 280 Color: 2951

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6524
Size: 353 Color: 6520
Size: 295 Color: 3873

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7213
Size: 319 Color: 5110
Size: 310 Color: 4705

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7194
Size: 317 Color: 5023
Size: 312 Color: 4807

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 6994
Size: 321 Color: 5224
Size: 313 Color: 4840

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8839
Size: 310 Color: 4733
Size: 257 Color: 981

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8704
Size: 316 Color: 4964
Size: 257 Color: 1034

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9104
Size: 303 Color: 4368
Size: 250 Color: 145

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9884
Size: 257 Color: 996
Size: 250 Color: 164

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7937
Size: 328 Color: 5533
Size: 275 Color: 2545

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8725
Size: 299 Color: 4122
Size: 273 Color: 2380

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8769
Size: 313 Color: 4845
Size: 257 Color: 958

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8696
Size: 296 Color: 3947
Size: 278 Color: 2800

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7084
Size: 332 Color: 5721
Size: 300 Color: 4188

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7779
Size: 341 Color: 6044
Size: 268 Color: 2031

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9059
Size: 298 Color: 4097
Size: 258 Color: 1129

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7894
Size: 342 Color: 6092
Size: 263 Color: 1516

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7047
Size: 335 Color: 5829
Size: 298 Color: 4051

Bin 837: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 304
Size: 250 Color: 194
Size: 250 Color: 137
Size: 250 Color: 90

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6725
Size: 359 Color: 6721
Size: 283 Color: 3114

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8022
Size: 309 Color: 4669
Size: 292 Color: 3711

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9539
Size: 269 Color: 2087
Size: 261 Color: 1409

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9410
Size: 282 Color: 3103
Size: 256 Color: 942

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9602
Size: 275 Color: 2596
Size: 252 Color: 451

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7797
Size: 341 Color: 6061
Size: 267 Color: 1901

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9888
Size: 254 Color: 678
Size: 253 Color: 600

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8273
Size: 339 Color: 5991
Size: 251 Color: 211

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8310
Size: 339 Color: 5972
Size: 250 Color: 106

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 5980
Size: 333 Color: 5747
Size: 329 Color: 5606

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7324
Size: 348 Color: 6347
Size: 277 Color: 2699

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9760
Size: 260 Color: 1246
Size: 256 Color: 891

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9874
Size: 254 Color: 697
Size: 254 Color: 632

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8062
Size: 333 Color: 5755
Size: 266 Color: 1846

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8607
Size: 297 Color: 3991
Size: 281 Color: 3030

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7534
Size: 322 Color: 5282
Size: 295 Color: 3923

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6556
Size: 353 Color: 6531
Size: 294 Color: 3825

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6558
Size: 354 Color: 6553
Size: 293 Color: 3771

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9100
Size: 283 Color: 3127
Size: 271 Color: 2276

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7015
Size: 321 Color: 5214
Size: 313 Color: 4835

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8617
Size: 308 Color: 4608
Size: 269 Color: 2093

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9338
Size: 271 Color: 2249
Size: 271 Color: 2222

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8248
Size: 340 Color: 6039
Size: 252 Color: 401

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7354
Size: 317 Color: 5045
Size: 307 Color: 4520

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8921
Size: 298 Color: 4093
Size: 265 Color: 1679

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 6992
Size: 352 Color: 6504
Size: 282 Color: 3091

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7563
Size: 344 Color: 6189
Size: 272 Color: 2351

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7150
Size: 328 Color: 5529
Size: 302 Color: 4326

Bin 866: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 204
Size: 250 Color: 140
Size: 250 Color: 58
Size: 250 Color: 40

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9141
Size: 295 Color: 3891
Size: 256 Color: 864

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6785
Size: 358 Color: 6682
Size: 282 Color: 3084

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7605
Size: 328 Color: 5545
Size: 287 Color: 3429

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6691
Size: 344 Color: 6200
Size: 299 Color: 4131

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9169
Size: 283 Color: 3174
Size: 267 Color: 1893

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9196
Size: 284 Color: 3207
Size: 265 Color: 1697

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8178
Size: 307 Color: 4509
Size: 287 Color: 3427

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6537
Size: 326 Color: 5470
Size: 322 Color: 5266

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8009
Size: 322 Color: 5297
Size: 279 Color: 2872

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7511
Size: 329 Color: 5610
Size: 289 Color: 3523

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8387
Size: 295 Color: 3882
Size: 292 Color: 3727

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9163
Size: 275 Color: 2583
Size: 275 Color: 2573

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8545
Size: 295 Color: 3908
Size: 285 Color: 3286

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9210
Size: 293 Color: 3772
Size: 255 Color: 854

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9823
Size: 256 Color: 887
Size: 256 Color: 857

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6698
Size: 358 Color: 6680
Size: 285 Color: 3261

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6693
Size: 358 Color: 6669
Size: 285 Color: 3307

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7729
Size: 358 Color: 6666
Size: 252 Color: 400

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7252
Size: 361 Color: 6799
Size: 266 Color: 1844

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7735
Size: 330 Color: 5638
Size: 280 Color: 2929

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7586
Size: 329 Color: 5614
Size: 286 Color: 3326

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7992
Size: 347 Color: 6310
Size: 255 Color: 776

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8434
Size: 326 Color: 5452
Size: 259 Color: 1237

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8353
Size: 331 Color: 5694
Size: 257 Color: 1042

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8511
Size: 325 Color: 5399
Size: 257 Color: 1016

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8630
Size: 309 Color: 4628
Size: 268 Color: 1969

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8846
Size: 308 Color: 4574
Size: 258 Color: 1063

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8712
Size: 320 Color: 5184
Size: 253 Color: 494

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8907
Size: 284 Color: 3237
Size: 279 Color: 2852

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 6222
Size: 336 Color: 5878
Size: 320 Color: 5173

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9402
Size: 270 Color: 2184
Size: 268 Color: 2049

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9086
Size: 288 Color: 3449
Size: 266 Color: 1810

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9477
Size: 283 Color: 3134
Size: 251 Color: 241

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9313
Size: 285 Color: 3267
Size: 258 Color: 1096

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9587
Size: 268 Color: 1976
Size: 259 Color: 1240

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9451
Size: 277 Color: 2733
Size: 258 Color: 1062

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9825
Size: 258 Color: 1132
Size: 254 Color: 714

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9867
Size: 255 Color: 842
Size: 253 Color: 558

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9947
Size: 253 Color: 590
Size: 250 Color: 60

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9854
Size: 255 Color: 758
Size: 254 Color: 711

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9573
Size: 269 Color: 2074
Size: 259 Color: 1188

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8698
Size: 287 Color: 3403
Size: 287 Color: 3364

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8276
Size: 331 Color: 5685
Size: 259 Color: 1160

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8442
Size: 295 Color: 3927
Size: 290 Color: 3574

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7876
Size: 324 Color: 5387
Size: 282 Color: 3051

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7822
Size: 321 Color: 5212
Size: 286 Color: 3327

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9381
Size: 273 Color: 2403
Size: 266 Color: 1818

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8774
Size: 302 Color: 4309
Size: 268 Color: 2023

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9589
Size: 269 Color: 2083
Size: 258 Color: 1102

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8694
Size: 321 Color: 5229
Size: 253 Color: 511

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6771
Size: 360 Color: 6769
Size: 281 Color: 3001

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9527
Size: 280 Color: 2908
Size: 251 Color: 221

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7933
Size: 344 Color: 6197
Size: 260 Color: 1324

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7925
Size: 316 Color: 4994
Size: 288 Color: 3491

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7928
Size: 310 Color: 4718
Size: 294 Color: 3830

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7688
Size: 362 Color: 6814
Size: 250 Color: 187

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9171
Size: 293 Color: 3766
Size: 257 Color: 961

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8720
Size: 322 Color: 5259
Size: 250 Color: 45

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8023
Size: 346 Color: 6248
Size: 255 Color: 818

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8409
Size: 332 Color: 5699
Size: 254 Color: 654

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9820
Size: 260 Color: 1333
Size: 252 Color: 430

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8311
Size: 339 Color: 5976
Size: 250 Color: 68

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9851
Size: 260 Color: 1337
Size: 250 Color: 147

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9519
Size: 275 Color: 2537
Size: 256 Color: 873

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9679
Size: 265 Color: 1749
Size: 257 Color: 979

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8205
Size: 331 Color: 5679
Size: 262 Color: 1429

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8306
Size: 339 Color: 5978
Size: 250 Color: 2

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9248
Size: 289 Color: 3517
Size: 257 Color: 1004

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7792
Size: 341 Color: 6065
Size: 267 Color: 1895

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8540
Size: 330 Color: 5632
Size: 251 Color: 333

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8010
Size: 319 Color: 5130
Size: 282 Color: 3052

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8226
Size: 340 Color: 6021
Size: 252 Color: 344

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8506
Size: 310 Color: 4704
Size: 272 Color: 2325

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9186
Size: 291 Color: 3684
Size: 258 Color: 1080

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9197
Size: 276 Color: 2661
Size: 272 Color: 2291

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9805
Size: 258 Color: 1142
Size: 255 Color: 836

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8877
Size: 298 Color: 4078
Size: 267 Color: 1875

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7654
Size: 343 Color: 6164
Size: 270 Color: 2155

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7621
Size: 308 Color: 4586
Size: 306 Color: 4499

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8727
Size: 307 Color: 4553
Size: 265 Color: 1696

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7521
Size: 345 Color: 6237
Size: 273 Color: 2383

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8571
Size: 291 Color: 3667
Size: 289 Color: 3521

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6855
Size: 328 Color: 5550
Size: 310 Color: 4714

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9414
Size: 282 Color: 3067
Size: 256 Color: 889

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8243
Size: 340 Color: 6025
Size: 252 Color: 348

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8739
Size: 292 Color: 3725
Size: 280 Color: 2960

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7736
Size: 342 Color: 6087
Size: 268 Color: 1958

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7043
Size: 336 Color: 5844
Size: 297 Color: 4026

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8012
Size: 324 Color: 5379
Size: 277 Color: 2750

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6766
Size: 360 Color: 6739
Size: 281 Color: 2989

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8701
Size: 303 Color: 4366
Size: 270 Color: 2203

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9448
Size: 282 Color: 3089
Size: 254 Color: 641

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7482
Size: 364 Color: 6893
Size: 255 Color: 732

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9352
Size: 274 Color: 2517
Size: 267 Color: 1925

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8987
Size: 301 Color: 4261
Size: 259 Color: 1169

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9427
Size: 270 Color: 2159
Size: 267 Color: 1851

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8732
Size: 302 Color: 4283
Size: 270 Color: 2185

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6592
Size: 355 Color: 6587
Size: 291 Color: 3649

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9932
Size: 253 Color: 488
Size: 251 Color: 228

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7166
Size: 371 Color: 7149
Size: 259 Color: 1224

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8858
Size: 298 Color: 4055
Size: 268 Color: 1979

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7100
Size: 351 Color: 6479
Size: 281 Color: 3005

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9744
Size: 261 Color: 1414
Size: 256 Color: 861

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8778
Size: 308 Color: 4590
Size: 262 Color: 1496

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9698
Size: 263 Color: 1572
Size: 258 Color: 1097

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9673
Size: 264 Color: 1617
Size: 258 Color: 1076

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8320
Size: 339 Color: 5996
Size: 250 Color: 73

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6700
Size: 358 Color: 6677
Size: 285 Color: 3253

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9363
Size: 272 Color: 2344
Size: 268 Color: 1995

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 6996
Size: 320 Color: 5159
Size: 314 Color: 4870

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6765
Size: 360 Color: 6752
Size: 281 Color: 3023

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7246
Size: 318 Color: 5090
Size: 309 Color: 4674

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8684
Size: 323 Color: 5347
Size: 251 Color: 324

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7604
Size: 344 Color: 6203
Size: 271 Color: 2274

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7966
Size: 321 Color: 5220
Size: 282 Color: 3086

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9743
Size: 259 Color: 1212
Size: 258 Color: 1073

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9499
Size: 273 Color: 2392
Size: 259 Color: 1179

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8503
Size: 323 Color: 5302
Size: 259 Color: 1172

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9418
Size: 278 Color: 2811
Size: 259 Color: 1220

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7165
Size: 316 Color: 4981
Size: 314 Color: 4882

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8633
Size: 322 Color: 5293
Size: 255 Color: 772

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6833
Size: 351 Color: 6478
Size: 288 Color: 3485

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7215
Size: 330 Color: 5643
Size: 298 Color: 4046

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 6396
Size: 349 Color: 6379
Size: 303 Color: 4331

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9380
Size: 272 Color: 2323
Size: 267 Color: 1933

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8819
Size: 296 Color: 3956
Size: 272 Color: 2282

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9724
Size: 267 Color: 1869
Size: 251 Color: 308

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8113
Size: 335 Color: 5838
Size: 261 Color: 1340

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9193
Size: 283 Color: 3112
Size: 266 Color: 1777

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6969
Size: 319 Color: 5112
Size: 316 Color: 4975

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6804
Size: 358 Color: 6679
Size: 282 Color: 3061

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9201
Size: 281 Color: 2981
Size: 267 Color: 1898

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7179
Size: 371 Color: 7176
Size: 259 Color: 1152

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8423
Size: 301 Color: 4271
Size: 284 Color: 3213

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9642
Size: 271 Color: 2241
Size: 253 Color: 496

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9346
Size: 276 Color: 2638
Size: 265 Color: 1710

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7027
Size: 332 Color: 5698
Size: 301 Color: 4251

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8878
Size: 292 Color: 3736
Size: 273 Color: 2401

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6473
Size: 351 Color: 6468
Size: 299 Color: 4127

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9238
Size: 294 Color: 3815
Size: 253 Color: 486

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9799
Size: 257 Color: 1048
Size: 257 Color: 965

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9458
Size: 282 Color: 3077
Size: 253 Color: 515

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9460
Size: 285 Color: 3298
Size: 250 Color: 65

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9241
Size: 280 Color: 2962
Size: 267 Color: 1927

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8308
Size: 332 Color: 5727
Size: 257 Color: 1021

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8649
Size: 322 Color: 5295
Size: 254 Color: 683

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8054
Size: 341 Color: 6060
Size: 258 Color: 1106

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9901
Size: 254 Color: 691
Size: 252 Color: 367

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8602
Size: 327 Color: 5522
Size: 251 Color: 251

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6886
Size: 329 Color: 5596
Size: 309 Color: 4666

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8605
Size: 305 Color: 4441
Size: 273 Color: 2413

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7525
Size: 345 Color: 6216
Size: 273 Color: 2371

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6432
Size: 328 Color: 5530
Size: 323 Color: 5342

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7782
Size: 359 Color: 6716
Size: 250 Color: 119

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9708
Size: 265 Color: 1709
Size: 255 Color: 824

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9948
Size: 252 Color: 463
Size: 251 Color: 326

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8277
Size: 330 Color: 5636
Size: 260 Color: 1302

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8118
Size: 335 Color: 5832
Size: 261 Color: 1394

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8407
Size: 298 Color: 4066
Size: 288 Color: 3437

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7294
Size: 325 Color: 5418
Size: 301 Color: 4270

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8542
Size: 329 Color: 5590
Size: 252 Color: 439

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8962
Size: 300 Color: 4159
Size: 261 Color: 1382

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8487
Size: 321 Color: 5202
Size: 262 Color: 1457

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7365
Size: 374 Color: 7271
Size: 250 Color: 175

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8067
Size: 334 Color: 5791
Size: 264 Color: 1607

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7450
Size: 346 Color: 6292
Size: 275 Color: 2555

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7290
Size: 314 Color: 4895
Size: 312 Color: 4785

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7752
Size: 342 Color: 6119
Size: 268 Color: 2045

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8297
Size: 339 Color: 5969
Size: 250 Color: 54

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8620
Size: 314 Color: 4883
Size: 263 Color: 1569

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7457
Size: 346 Color: 6285
Size: 274 Color: 2463

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9065
Size: 306 Color: 4464
Size: 250 Color: 79

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7806
Size: 341 Color: 6066
Size: 267 Color: 1916

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7426
Size: 347 Color: 6304
Size: 275 Color: 2558

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8280
Size: 339 Color: 5984
Size: 251 Color: 320

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6944
Size: 362 Color: 6825
Size: 274 Color: 2522

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6792
Size: 335 Color: 5839
Size: 305 Color: 4435

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7707
Size: 343 Color: 6141
Size: 268 Color: 2033

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6880
Size: 328 Color: 5558
Size: 310 Color: 4701

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9530
Size: 268 Color: 2047
Size: 263 Color: 1557

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7287
Size: 368 Color: 7024
Size: 258 Color: 1083

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8090
Size: 334 Color: 5774
Size: 263 Color: 1554

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9641
Size: 266 Color: 1788
Size: 258 Color: 1113

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9509
Size: 280 Color: 2954
Size: 252 Color: 436

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9960
Size: 252 Color: 417
Size: 250 Color: 121

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7161
Size: 324 Color: 5374
Size: 306 Color: 4478

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8296
Size: 295 Color: 3910
Size: 294 Color: 3813

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8524
Size: 330 Color: 5635
Size: 251 Color: 255

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8344
Size: 294 Color: 3867
Size: 294 Color: 3839

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9308
Size: 274 Color: 2491
Size: 269 Color: 2106

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7471
Size: 346 Color: 6274
Size: 274 Color: 2450

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6733
Size: 359 Color: 6724
Size: 283 Color: 3139

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7942
Size: 323 Color: 5305
Size: 280 Color: 2961

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9862
Size: 259 Color: 1174
Size: 250 Color: 15

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8015
Size: 346 Color: 6254
Size: 255 Color: 855

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9968
Size: 252 Color: 371
Size: 250 Color: 159

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9810
Size: 257 Color: 1044
Size: 256 Color: 920

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9770
Size: 263 Color: 1529
Size: 252 Color: 347

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9508
Size: 281 Color: 2996
Size: 251 Color: 280

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9749
Size: 259 Color: 1194
Size: 258 Color: 1146

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 7998
Size: 329 Color: 5597
Size: 272 Color: 2285

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7291
Size: 328 Color: 5539
Size: 298 Color: 4087

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9803
Size: 263 Color: 1522
Size: 250 Color: 18

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 9995
Size: 250 Color: 52
Size: 250 Color: 37

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9063
Size: 288 Color: 3484
Size: 268 Color: 2024

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7444
Size: 346 Color: 6281
Size: 275 Color: 2601

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8397
Size: 299 Color: 4136
Size: 287 Color: 3376

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7955
Size: 315 Color: 4931
Size: 288 Color: 3479

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9514
Size: 272 Color: 2295
Size: 259 Color: 1202

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7734
Size: 342 Color: 6094
Size: 268 Color: 2035

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7451
Size: 346 Color: 6260
Size: 275 Color: 2535

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9155
Size: 279 Color: 2864
Size: 271 Color: 2219

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9261
Size: 292 Color: 3719
Size: 254 Color: 616

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8873
Size: 298 Color: 4077
Size: 267 Color: 1902

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6958
Size: 338 Color: 5942
Size: 298 Color: 4074

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9614
Size: 263 Color: 1568
Size: 263 Color: 1513

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7670
Size: 343 Color: 6137
Size: 269 Color: 2090

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7286
Size: 328 Color: 5548
Size: 298 Color: 4099

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9433
Size: 269 Color: 2077
Size: 268 Color: 2008

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7554
Size: 318 Color: 5108
Size: 299 Color: 4125

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9865
Size: 259 Color: 1236
Size: 250 Color: 109

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9099
Size: 300 Color: 4168
Size: 254 Color: 620

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9683
Size: 264 Color: 1619
Size: 258 Color: 1140

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8892
Size: 295 Color: 3925
Size: 269 Color: 2108

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7737
Size: 342 Color: 6098
Size: 268 Color: 2000

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 10000
Size: 250 Color: 127
Size: 250 Color: 126

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8053
Size: 337 Color: 5890
Size: 262 Color: 1491

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7739
Size: 342 Color: 6101
Size: 268 Color: 1953

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8111
Size: 335 Color: 5830
Size: 262 Color: 1501

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9206
Size: 278 Color: 2799
Size: 270 Color: 2187

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6777
Size: 360 Color: 6755
Size: 281 Color: 2980

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8474
Size: 307 Color: 4551
Size: 276 Color: 2613

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9136
Size: 299 Color: 4100
Size: 252 Color: 354

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9343
Size: 284 Color: 3195
Size: 257 Color: 977

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7433
Size: 366 Color: 6962
Size: 255 Color: 817

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8625
Size: 291 Color: 3637
Size: 286 Color: 3351

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7144
Size: 336 Color: 5877
Size: 294 Color: 3857

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8125
Size: 331 Color: 5689
Size: 265 Color: 1687

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9567
Size: 270 Color: 2178
Size: 258 Color: 1122

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7930
Size: 350 Color: 6427
Size: 254 Color: 671

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8014
Size: 307 Color: 4556
Size: 294 Color: 3810

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9006
Size: 301 Color: 4231
Size: 258 Color: 1114

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7516
Size: 329 Color: 5591
Size: 289 Color: 3528

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7963
Size: 323 Color: 5314
Size: 280 Color: 2950

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9126
Size: 276 Color: 2657
Size: 276 Color: 2653

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9788
Size: 263 Color: 1544
Size: 251 Color: 226

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9730
Size: 267 Color: 1899
Size: 251 Color: 305

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9689
Size: 270 Color: 2177
Size: 251 Color: 330

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9156
Size: 285 Color: 3247
Size: 265 Color: 1735

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9972
Size: 252 Color: 469
Size: 250 Color: 125

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8231
Size: 338 Color: 5938
Size: 254 Color: 719

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7373
Size: 347 Color: 6299
Size: 276 Color: 2627

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8729
Size: 320 Color: 5174
Size: 252 Color: 398

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8908
Size: 310 Color: 4703
Size: 253 Color: 572

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9407
Size: 269 Color: 2126
Size: 269 Color: 2067

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8624
Size: 308 Color: 4578
Size: 269 Color: 2132

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9665
Size: 265 Color: 1694
Size: 258 Color: 1088

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8495
Size: 330 Color: 5631
Size: 252 Color: 387

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9953
Size: 252 Color: 346
Size: 251 Color: 220

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6757
Size: 358 Color: 6697
Size: 283 Color: 3128

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8219
Size: 338 Color: 5929
Size: 254 Color: 613

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9841
Size: 257 Color: 1046
Size: 253 Color: 507

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8200
Size: 330 Color: 5628
Size: 263 Color: 1521

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7442
Size: 346 Color: 6284
Size: 275 Color: 2551

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9876
Size: 254 Color: 709
Size: 254 Color: 651

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6690
Size: 358 Color: 6685
Size: 285 Color: 3270

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8874
Size: 285 Color: 3246
Size: 280 Color: 2933

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8855
Size: 296 Color: 3946
Size: 270 Color: 2204

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8565
Size: 290 Color: 3600
Size: 290 Color: 3582

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7611
Size: 320 Color: 5183
Size: 294 Color: 3869

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8398
Size: 331 Color: 5661
Size: 255 Color: 816

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8367
Size: 305 Color: 4446
Size: 282 Color: 3102

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7544
Size: 362 Color: 6812
Size: 255 Color: 840

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8463
Size: 293 Color: 3799
Size: 291 Color: 3669

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9612
Size: 275 Color: 2580
Size: 251 Color: 306

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8953
Size: 288 Color: 3452
Size: 273 Color: 2439

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9879
Size: 257 Color: 1011
Size: 251 Color: 259

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7071
Size: 329 Color: 5607
Size: 303 Color: 4337

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8097
Size: 336 Color: 5871
Size: 261 Color: 1406

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8137
Size: 336 Color: 5850
Size: 260 Color: 1336

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8518
Size: 330 Color: 5648
Size: 252 Color: 396

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7072
Size: 324 Color: 5354
Size: 308 Color: 4570

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8670
Size: 288 Color: 3454
Size: 287 Color: 3411

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7730
Size: 342 Color: 6089
Size: 268 Color: 1980

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6779
Size: 338 Color: 5960
Size: 302 Color: 4281

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8755
Size: 299 Color: 4139
Size: 272 Color: 2354

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7374
Size: 320 Color: 5187
Size: 303 Color: 4333

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8857
Size: 298 Color: 4067
Size: 268 Color: 2004

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9297
Size: 291 Color: 3634
Size: 253 Color: 563

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9898
Size: 254 Color: 630
Size: 252 Color: 424

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8548
Size: 293 Color: 3783
Size: 287 Color: 3413

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9107
Size: 285 Color: 3255
Size: 268 Color: 2042

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8900
Size: 309 Color: 4678
Size: 255 Color: 770

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9180
Size: 275 Color: 2576
Size: 274 Color: 2461

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8451
Size: 320 Color: 5162
Size: 264 Color: 1656

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7765
Size: 312 Color: 4816
Size: 297 Color: 3989

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9741
Size: 261 Color: 1411
Size: 256 Color: 869

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8841
Size: 314 Color: 4912
Size: 253 Color: 483

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7790
Size: 341 Color: 6047
Size: 267 Color: 1897

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9471
Size: 279 Color: 2894
Size: 255 Color: 725

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8471
Size: 332 Color: 5701
Size: 251 Color: 274

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9161
Size: 295 Color: 3909
Size: 255 Color: 747

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9434
Size: 270 Color: 2163
Size: 267 Color: 1931

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9817
Size: 257 Color: 968
Size: 255 Color: 738

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9253
Size: 281 Color: 3016
Size: 265 Color: 1695

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8891
Size: 285 Color: 3308
Size: 279 Color: 2843

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7853
Size: 314 Color: 4905
Size: 293 Color: 3738

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6865
Size: 326 Color: 5450
Size: 312 Color: 4798

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9658
Size: 272 Color: 2329
Size: 251 Color: 272

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9601
Size: 270 Color: 2207
Size: 257 Color: 995

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7274
Size: 349 Color: 6387
Size: 278 Color: 2785

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9907
Size: 253 Color: 585
Size: 253 Color: 495

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8445
Size: 311 Color: 4764
Size: 273 Color: 2405

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8175
Size: 337 Color: 5885
Size: 257 Color: 1019

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9703
Size: 270 Color: 2206
Size: 250 Color: 144

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8406
Size: 301 Color: 4269
Size: 285 Color: 3291

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9687
Size: 262 Color: 1443
Size: 259 Color: 1208

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7594
Size: 364 Color: 6910
Size: 251 Color: 332

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6651
Size: 357 Color: 6645
Size: 287 Color: 3422

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8150
Size: 340 Color: 6002
Size: 255 Color: 809

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9963
Size: 251 Color: 290
Size: 251 Color: 239

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9426
Size: 281 Color: 2994
Size: 256 Color: 885

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8685
Size: 299 Color: 4128
Size: 275 Color: 2585

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9391
Size: 284 Color: 3214
Size: 255 Color: 783

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9526
Size: 274 Color: 2526
Size: 257 Color: 1015

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8513
Size: 321 Color: 5238
Size: 261 Color: 1389

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8915
Size: 283 Color: 3175
Size: 280 Color: 2942

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8013
Size: 313 Color: 4847
Size: 288 Color: 3440

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7813
Size: 341 Color: 6062
Size: 267 Color: 1874

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6903
Size: 323 Color: 5319
Size: 314 Color: 4886

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9643
Size: 265 Color: 1723
Size: 259 Color: 1165

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8611
Size: 291 Color: 3653
Size: 287 Color: 3414

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7911
Size: 310 Color: 4725
Size: 295 Color: 3922

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9316
Size: 277 Color: 2743
Size: 266 Color: 1840

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7063
Size: 337 Color: 5912
Size: 296 Color: 3965

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9709
Size: 261 Color: 1423
Size: 259 Color: 1218

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8130
Size: 300 Color: 4161
Size: 296 Color: 3933

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7279
Size: 318 Color: 5088
Size: 308 Color: 4610

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6974
Size: 337 Color: 5918
Size: 298 Color: 4064

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7832
Size: 306 Color: 4468
Size: 301 Color: 4234

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8091
Size: 301 Color: 4222
Size: 296 Color: 3980

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7296
Size: 329 Color: 5581
Size: 297 Color: 3985

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9207
Size: 292 Color: 3707
Size: 256 Color: 908

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8121
Size: 337 Color: 5911
Size: 259 Color: 1207

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9199
Size: 297 Color: 4037
Size: 251 Color: 219

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 10001
Size: 250 Color: 136
Size: 250 Color: 76

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9954
Size: 252 Color: 342
Size: 250 Color: 86

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6927
Size: 341 Color: 6086
Size: 295 Color: 3919

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7949
Size: 348 Color: 6364
Size: 255 Color: 768

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8222
Size: 340 Color: 6024
Size: 252 Color: 338

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9629
Size: 268 Color: 1998
Size: 257 Color: 1028

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8735
Size: 287 Color: 3425
Size: 285 Color: 3312

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8586
Size: 310 Color: 4728
Size: 268 Color: 2032

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9846
Size: 260 Color: 1334
Size: 250 Color: 57

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7608
Size: 321 Color: 5203
Size: 294 Color: 3847

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6863
Size: 351 Color: 6459
Size: 287 Color: 3379

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7781
Size: 317 Color: 5058
Size: 292 Color: 3722

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6457
Size: 351 Color: 6451
Size: 299 Color: 4102

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8610
Size: 302 Color: 4299
Size: 276 Color: 2633

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9498
Size: 268 Color: 1997
Size: 264 Color: 1624

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9908
Size: 256 Color: 954
Size: 250 Color: 142

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7273
Size: 323 Color: 5335
Size: 304 Color: 4398

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8771
Size: 320 Color: 5167
Size: 250 Color: 100

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9318
Size: 284 Color: 3205
Size: 259 Color: 1177

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9926
Size: 253 Color: 502
Size: 251 Color: 277

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9904
Size: 254 Color: 629
Size: 252 Color: 359

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8533
Size: 293 Color: 3784
Size: 288 Color: 3499

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8832
Size: 316 Color: 4992
Size: 251 Color: 208

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6920
Size: 343 Color: 6133
Size: 294 Color: 3862

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9166
Size: 284 Color: 3208
Size: 266 Color: 1781

Bin 1237: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 322
Size: 250 Color: 185
Size: 250 Color: 170
Size: 250 Color: 14

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7469
Size: 346 Color: 6262
Size: 274 Color: 2457

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8888
Size: 297 Color: 4031
Size: 267 Color: 1873

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8001
Size: 308 Color: 4584
Size: 293 Color: 3764

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7325
Size: 348 Color: 6345
Size: 277 Color: 2704

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6776
Size: 360 Color: 6745
Size: 281 Color: 2971

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7147
Size: 328 Color: 5555
Size: 302 Color: 4302

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7458
Size: 346 Color: 6270
Size: 274 Color: 2499

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7323
Size: 348 Color: 6350
Size: 277 Color: 2737

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8233
Size: 338 Color: 5949
Size: 254 Color: 614

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6885
Size: 328 Color: 5564
Size: 310 Color: 4682

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9090
Size: 284 Color: 3209
Size: 270 Color: 2179

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7731
Size: 342 Color: 6115
Size: 268 Color: 1990

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9735
Size: 265 Color: 1676
Size: 252 Color: 405

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7473
Size: 346 Color: 6258
Size: 274 Color: 2495

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8098
Size: 334 Color: 5776
Size: 263 Color: 1535

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8412
Size: 293 Color: 3778
Size: 293 Color: 3744

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9516
Size: 278 Color: 2776
Size: 253 Color: 506

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8140
Size: 335 Color: 5828
Size: 261 Color: 1407

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7830
Size: 326 Color: 5455
Size: 281 Color: 2990

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9757
Size: 260 Color: 1331
Size: 256 Color: 871

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8192
Size: 305 Color: 4439
Size: 288 Color: 3453

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8181
Size: 332 Color: 5719
Size: 262 Color: 1472

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9897
Size: 253 Color: 560
Size: 253 Color: 541

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8952
Size: 302 Color: 4296
Size: 259 Color: 1217

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9446
Size: 283 Color: 3119
Size: 253 Color: 559

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8212
Size: 305 Color: 4418
Size: 288 Color: 3492

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7288
Size: 328 Color: 5575
Size: 298 Color: 4068

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8136
Size: 298 Color: 4092
Size: 298 Color: 4061

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6930
Size: 365 Color: 6928
Size: 271 Color: 2235

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9388
Size: 276 Color: 2671
Size: 263 Color: 1542

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9943
Size: 252 Color: 455
Size: 251 Color: 232

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8609
Size: 291 Color: 3659
Size: 287 Color: 3396

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8309
Size: 327 Color: 5494
Size: 262 Color: 1478

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9835
Size: 259 Color: 1162
Size: 252 Color: 457

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8427
Size: 309 Color: 4643
Size: 276 Color: 2686

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8840
Size: 284 Color: 3184
Size: 283 Color: 3163

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8867
Size: 313 Color: 4831
Size: 252 Color: 452

Bin 1275: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 248
Size: 250 Color: 169
Size: 250 Color: 61
Size: 250 Color: 23

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8152
Size: 334 Color: 5813
Size: 261 Color: 1365

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8673
Size: 305 Color: 4447
Size: 270 Color: 2168

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9946
Size: 253 Color: 523
Size: 250 Color: 157

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8154
Size: 340 Color: 6014
Size: 255 Color: 802

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8514
Size: 308 Color: 4560
Size: 274 Color: 2460

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8807
Size: 288 Color: 3439
Size: 280 Color: 2943

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9219
Size: 295 Color: 3906
Size: 253 Color: 503

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9252
Size: 281 Color: 2998
Size: 265 Color: 1717

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8785
Size: 302 Color: 4282
Size: 267 Color: 1929

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7573
Size: 326 Color: 5468
Size: 290 Color: 3592

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9269
Size: 276 Color: 2680
Size: 269 Color: 2104

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9842
Size: 255 Color: 755
Size: 255 Color: 729

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9544
Size: 278 Color: 2803
Size: 252 Color: 409

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9384
Size: 274 Color: 2516
Size: 265 Color: 1757

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9824
Size: 257 Color: 1049
Size: 255 Color: 773

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9392
Size: 282 Color: 3049
Size: 256 Color: 952

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9656
Size: 264 Color: 1615
Size: 260 Color: 1277

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9850
Size: 255 Color: 851
Size: 255 Color: 766

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9109
Size: 295 Color: 3871
Size: 258 Color: 1092

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8237
Size: 338 Color: 5928
Size: 254 Color: 634

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8763
Size: 302 Color: 4313
Size: 268 Color: 1970

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7059
Size: 368 Color: 7026
Size: 265 Color: 1726

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7726
Size: 342 Color: 6112
Size: 268 Color: 1964

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9578
Size: 277 Color: 2705
Size: 251 Color: 334

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9425
Size: 278 Color: 2836
Size: 259 Color: 1221

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7896
Size: 323 Color: 5323
Size: 282 Color: 3041

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7021
Size: 332 Color: 5704
Size: 302 Color: 4315

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9860
Size: 258 Color: 1115
Size: 251 Color: 318

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7432
Size: 347 Color: 6326
Size: 274 Color: 2484

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8291
Size: 340 Color: 6037
Size: 250 Color: 63

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9814
Size: 259 Color: 1189
Size: 253 Color: 490

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8580
Size: 323 Color: 5343
Size: 256 Color: 877

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8404
Size: 299 Color: 4135
Size: 287 Color: 3408

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9630
Size: 269 Color: 2081
Size: 256 Color: 951

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9617
Size: 276 Color: 2658
Size: 250 Color: 171

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9638
Size: 266 Color: 1762
Size: 258 Color: 1130

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8568
Size: 324 Color: 5356
Size: 256 Color: 900

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9304
Size: 283 Color: 3138
Size: 261 Color: 1422

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7200
Size: 329 Color: 5604
Size: 300 Color: 4177

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9467
Size: 281 Color: 2988
Size: 253 Color: 480

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9609
Size: 270 Color: 2195
Size: 256 Color: 919

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8283
Size: 339 Color: 5988
Size: 251 Color: 217

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8498
Size: 311 Color: 4766
Size: 271 Color: 2238

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6822
Size: 330 Color: 5623
Size: 309 Color: 4654

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7939
Size: 337 Color: 5883
Size: 266 Color: 1804

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7826
Size: 320 Color: 5188
Size: 287 Color: 3412

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9592
Size: 264 Color: 1634
Size: 263 Color: 1549

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9243
Size: 287 Color: 3377
Size: 259 Color: 1150

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8820
Size: 306 Color: 4490
Size: 262 Color: 1477

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8970
Size: 285 Color: 3299
Size: 275 Color: 2607

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6506
Size: 352 Color: 6498
Size: 297 Color: 4007

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9628
Size: 265 Color: 1727
Size: 260 Color: 1250

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9939
Size: 253 Color: 589
Size: 251 Color: 327

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8030
Size: 345 Color: 6245
Size: 255 Color: 740

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7416
Size: 347 Color: 6305
Size: 275 Color: 2608

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9094
Size: 278 Color: 2826
Size: 276 Color: 2650

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8083
Size: 336 Color: 5849
Size: 262 Color: 1492

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7472
Size: 346 Color: 6269
Size: 274 Color: 2466

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8078
Size: 334 Color: 5800
Size: 264 Color: 1589

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9668
Size: 273 Color: 2422
Size: 250 Color: 166

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8709
Size: 317 Color: 5012
Size: 256 Color: 856

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8242
Size: 338 Color: 5930
Size: 254 Color: 624

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8958
Size: 294 Color: 3849
Size: 267 Color: 1866

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 9718
Size: 261 Color: 1368
Size: 258 Color: 1068

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9605
Size: 273 Color: 2375
Size: 254 Color: 606

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6508
Size: 352 Color: 6502
Size: 297 Color: 4005

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8080
Size: 303 Color: 4346
Size: 295 Color: 3902

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9302
Size: 280 Color: 2914
Size: 264 Color: 1601

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9815
Size: 257 Color: 967
Size: 255 Color: 733

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9761
Size: 261 Color: 1349
Size: 255 Color: 805

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9727
Size: 260 Color: 1285
Size: 258 Color: 1128

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8189
Size: 337 Color: 5897
Size: 257 Color: 998

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7591
Size: 314 Color: 4901
Size: 301 Color: 4212

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8767
Size: 288 Color: 3490
Size: 282 Color: 3106

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8982
Size: 282 Color: 3059
Size: 278 Color: 2801

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8245
Size: 340 Color: 6017
Size: 252 Color: 369

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8894
Size: 283 Color: 3162
Size: 281 Color: 3012

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7725
Size: 342 Color: 6099
Size: 268 Color: 2050

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7866
Size: 316 Color: 4978
Size: 290 Color: 3598

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7827
Size: 314 Color: 4884
Size: 293 Color: 3748

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8040
Size: 334 Color: 5796
Size: 266 Color: 1784

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8579
Size: 291 Color: 3665
Size: 288 Color: 3443

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9370
Size: 286 Color: 3357
Size: 254 Color: 661

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9445
Size: 285 Color: 3268
Size: 251 Color: 285

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9411
Size: 288 Color: 3475
Size: 250 Color: 92

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9892
Size: 254 Color: 649
Size: 253 Color: 509

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6786
Size: 337 Color: 5924
Size: 303 Color: 4336

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7397
Size: 328 Color: 5567
Size: 294 Color: 3837

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8345
Size: 333 Color: 5740
Size: 255 Color: 800

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8640
Size: 325 Color: 5429
Size: 251 Color: 278

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8073
Size: 334 Color: 5787
Size: 264 Color: 1592

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9894
Size: 256 Color: 914
Size: 250 Color: 141

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9234
Size: 288 Color: 3466
Size: 259 Color: 1176

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8279
Size: 338 Color: 5961
Size: 252 Color: 377

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9688
Size: 268 Color: 2051
Size: 253 Color: 550

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7384
Size: 331 Color: 5667
Size: 292 Color: 3710

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9699
Size: 263 Color: 1523
Size: 258 Color: 1111

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8961
Size: 284 Color: 3227
Size: 277 Color: 2713

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8882
Size: 313 Color: 4854
Size: 251 Color: 268

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9568
Size: 272 Color: 2356
Size: 256 Color: 897

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8971
Size: 292 Color: 3701
Size: 268 Color: 1971

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8604
Size: 324 Color: 5393
Size: 254 Color: 657

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7126
Size: 370 Color: 7113
Size: 261 Color: 1427

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7363
Size: 365 Color: 6939
Size: 259 Color: 1233

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9125
Size: 292 Color: 3705
Size: 260 Color: 1327

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8028
Size: 344 Color: 6207
Size: 256 Color: 934

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7744
Size: 342 Color: 6117
Size: 268 Color: 2029

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7411
Size: 368 Color: 7025
Size: 254 Color: 721

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7370
Size: 347 Color: 6311
Size: 276 Color: 2622

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7617
Size: 328 Color: 5572
Size: 286 Color: 3328

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7435
Size: 311 Color: 4777
Size: 310 Color: 4734

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9776
Size: 260 Color: 1288
Size: 255 Color: 822

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6915
Size: 324 Color: 5368
Size: 313 Color: 4867

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6774
Size: 360 Color: 6756
Size: 281 Color: 2974

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9910
Size: 255 Color: 835
Size: 250 Color: 108

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7042
Size: 335 Color: 5823
Size: 298 Color: 4088

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8707
Size: 293 Color: 3779
Size: 280 Color: 2913

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8601
Size: 320 Color: 5160
Size: 258 Color: 1099

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9581
Size: 276 Color: 2647
Size: 252 Color: 404

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7428
Size: 314 Color: 4898
Size: 308 Color: 4595

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8927
Size: 307 Color: 4524
Size: 255 Color: 760

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9491
Size: 282 Color: 3069
Size: 251 Color: 229

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8781
Size: 290 Color: 3619
Size: 279 Color: 2875

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7976
Size: 324 Color: 5366
Size: 278 Color: 2809

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8453
Size: 317 Color: 5044
Size: 267 Color: 1852

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7431
Size: 346 Color: 6267
Size: 275 Color: 2540

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8156
Size: 340 Color: 6026
Size: 255 Color: 829

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7281
Size: 317 Color: 5035
Size: 309 Color: 4635

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8796
Size: 298 Color: 4056
Size: 271 Color: 2245

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8316
Size: 334 Color: 5786
Size: 255 Color: 808

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8177
Size: 332 Color: 5703
Size: 262 Color: 1446

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6831
Size: 330 Color: 5645
Size: 309 Color: 4655

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9190
Size: 295 Color: 3926
Size: 254 Color: 676

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7884
Size: 312 Color: 4817
Size: 293 Color: 3777

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8375
Size: 316 Color: 4988
Size: 271 Color: 2278

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7672
Size: 343 Color: 6136
Size: 269 Color: 2068

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8870
Size: 307 Color: 4521
Size: 258 Color: 1077

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9585
Size: 277 Color: 2738
Size: 251 Color: 213

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9580
Size: 274 Color: 2464
Size: 254 Color: 623

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8824
Size: 293 Color: 3801
Size: 274 Color: 2465

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9777
Size: 262 Color: 1498
Size: 253 Color: 528

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9944
Size: 253 Color: 484
Size: 250 Color: 84

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9419
Size: 282 Color: 3108
Size: 255 Color: 774

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9396
Size: 274 Color: 2510
Size: 264 Color: 1640

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8656
Size: 305 Color: 4445
Size: 271 Color: 2232

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9966
Size: 251 Color: 329
Size: 251 Color: 281

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8425
Size: 331 Color: 5681
Size: 254 Color: 696

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9813
Size: 260 Color: 1269
Size: 253 Color: 473

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8108
Size: 334 Color: 5778
Size: 263 Color: 1576

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9220
Size: 296 Color: 3930
Size: 252 Color: 415

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9611
Size: 274 Color: 2453
Size: 252 Color: 461

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6914
Size: 323 Color: 5308
Size: 314 Color: 4897

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7377
Size: 325 Color: 5409
Size: 298 Color: 4069

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7994
Size: 337 Color: 5904
Size: 265 Color: 1673

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9933
Size: 254 Color: 674
Size: 250 Color: 117

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6522
Size: 353 Color: 6515
Size: 295 Color: 3915

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8143
Size: 336 Color: 5853
Size: 260 Color: 1283

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 7997
Size: 331 Color: 5662
Size: 270 Color: 2157

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8988
Size: 290 Color: 3617
Size: 270 Color: 2194

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7923
Size: 324 Color: 5364
Size: 280 Color: 2921

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9702
Size: 260 Color: 1338
Size: 260 Color: 1244

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8644
Size: 323 Color: 5346
Size: 253 Color: 567

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6957
Size: 365 Color: 6956
Size: 271 Color: 2272

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8904
Size: 300 Color: 4194
Size: 263 Color: 1537

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6984
Size: 333 Color: 5744
Size: 302 Color: 4278

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9742
Size: 265 Color: 1743
Size: 252 Color: 355

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9233
Size: 276 Color: 2666
Size: 271 Color: 2259

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6827
Size: 330 Color: 5633
Size: 309 Color: 4614

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8924
Size: 298 Color: 4091
Size: 265 Color: 1689

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9921
Size: 254 Color: 713
Size: 251 Color: 282

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8790
Size: 299 Color: 4149
Size: 270 Color: 2210

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8975
Size: 310 Color: 4712
Size: 250 Color: 132

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6913
Size: 324 Color: 5361
Size: 313 Color: 4839

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9496
Size: 277 Color: 2749
Size: 255 Color: 827

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9339
Size: 283 Color: 3158
Size: 259 Color: 1191

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8194
Size: 300 Color: 4151
Size: 293 Color: 3807

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9398
Size: 279 Color: 2859
Size: 259 Color: 1211

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9623
Size: 270 Color: 2144
Size: 256 Color: 918

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7547
Size: 312 Color: 4824
Size: 305 Color: 4456

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7612
Size: 364 Color: 6922
Size: 250 Color: 70

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7687
Size: 343 Color: 6163
Size: 269 Color: 2113

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7941
Size: 321 Color: 5219
Size: 282 Color: 3065

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7401
Size: 317 Color: 5047
Size: 305 Color: 4419

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8821
Size: 294 Color: 3828
Size: 273 Color: 2416

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9245
Size: 278 Color: 2819
Size: 268 Color: 1983

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7001
Size: 320 Color: 5176
Size: 314 Color: 4875

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8252
Size: 340 Color: 6010
Size: 251 Color: 283

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7295
Size: 318 Color: 5091
Size: 308 Color: 4591

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9925
Size: 255 Color: 750
Size: 250 Color: 46

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7493
Size: 345 Color: 6225
Size: 274 Color: 2493

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9474
Size: 284 Color: 3222
Size: 250 Color: 41

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9011
Size: 295 Color: 3877
Size: 263 Color: 1558

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8974
Size: 284 Color: 3240
Size: 276 Color: 2628

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6970
Size: 319 Color: 5114
Size: 316 Color: 4962

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8437
Size: 311 Color: 4742
Size: 274 Color: 2454

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9951
Size: 252 Color: 349
Size: 251 Color: 301

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9789
Size: 257 Color: 1035
Size: 257 Color: 974

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6929
Size: 320 Color: 5193
Size: 316 Color: 4971

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8039
Size: 300 Color: 4198
Size: 300 Color: 4196

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8906
Size: 304 Color: 4408
Size: 259 Color: 1214

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9018
Size: 288 Color: 3469
Size: 270 Color: 2169

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7974
Size: 322 Color: 5290
Size: 280 Color: 2918

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9007
Size: 306 Color: 4465
Size: 253 Color: 474

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7074
Size: 351 Color: 6461
Size: 281 Color: 3033

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7633
Size: 320 Color: 5152
Size: 294 Color: 3865

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9468
Size: 273 Color: 2366
Size: 261 Color: 1396

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7958
Size: 317 Color: 5040
Size: 286 Color: 3315

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8043
Size: 333 Color: 5756
Size: 266 Color: 1836

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8866
Size: 297 Color: 4001
Size: 268 Color: 1957

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7952
Size: 317 Color: 5014
Size: 286 Color: 3318

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9332
Size: 292 Color: 3737
Size: 250 Color: 56

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9413
Size: 272 Color: 2324
Size: 266 Color: 1785

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8475
Size: 329 Color: 5621
Size: 254 Color: 635

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8172
Size: 336 Color: 5863
Size: 258 Color: 1104

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8333
Size: 300 Color: 4176
Size: 288 Color: 3447

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9545
Size: 275 Color: 2574
Size: 255 Color: 794

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8196
Size: 337 Color: 5905
Size: 256 Color: 906

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9436
Size: 284 Color: 3189
Size: 252 Color: 389

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7693
Size: 358 Color: 6699
Size: 254 Color: 665

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6495
Size: 352 Color: 6493
Size: 297 Color: 4019

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7062
Size: 368 Color: 7034
Size: 265 Color: 1664

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7160
Size: 332 Color: 5697
Size: 298 Color: 4038

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7459
Size: 346 Color: 6280
Size: 274 Color: 2503

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7805
Size: 323 Color: 5311
Size: 285 Color: 3249

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9260
Size: 294 Color: 3821
Size: 252 Color: 335

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 9999
Size: 250 Color: 195
Size: 250 Color: 163

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9713
Size: 262 Color: 1438
Size: 258 Color: 1131

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7512
Size: 345 Color: 6224
Size: 273 Color: 2406

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9691
Size: 266 Color: 1838
Size: 255 Color: 853

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8358
Size: 299 Color: 4104
Size: 289 Color: 3511

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9450
Size: 278 Color: 2821
Size: 258 Color: 1121

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7517
Size: 345 Color: 6229
Size: 273 Color: 2374

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8190
Size: 300 Color: 4164
Size: 294 Color: 3868

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8884
Size: 311 Color: 4773
Size: 253 Color: 530

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8432
Size: 325 Color: 5407
Size: 260 Color: 1247

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9405
Size: 284 Color: 3196
Size: 254 Color: 618

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9493
Size: 268 Color: 2043
Size: 265 Color: 1739

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9082
Size: 288 Color: 3497
Size: 266 Color: 1805

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8000
Size: 310 Color: 4687
Size: 291 Color: 3680

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8639
Size: 288 Color: 3495
Size: 288 Color: 3436

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 9991
Size: 250 Color: 97
Size: 250 Color: 26

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7843
Size: 325 Color: 5417
Size: 282 Color: 3085

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9360
Size: 288 Color: 3481
Size: 252 Color: 388

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7439
Size: 346 Color: 6271
Size: 275 Color: 2553

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8191
Size: 337 Color: 5921
Size: 256 Color: 944

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7337
Size: 348 Color: 6369
Size: 277 Color: 2721

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8027
Size: 341 Color: 6055
Size: 259 Color: 1171

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9373
Size: 282 Color: 3092
Size: 257 Color: 982

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7758
Size: 341 Color: 6046
Size: 268 Color: 1993

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9131
Size: 294 Color: 3866
Size: 258 Color: 1079

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7453
Size: 347 Color: 6316
Size: 274 Color: 2502

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7139
Size: 322 Color: 5274
Size: 309 Color: 4640

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9887
Size: 254 Color: 680
Size: 253 Color: 529

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8124
Size: 336 Color: 5880
Size: 260 Color: 1270

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9168
Size: 293 Color: 3740
Size: 257 Color: 999

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7470
Size: 346 Color: 6261
Size: 274 Color: 2482

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7052
Size: 332 Color: 5725
Size: 301 Color: 4203

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9961
Size: 252 Color: 374
Size: 250 Color: 66

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7700
Size: 323 Color: 5321
Size: 288 Color: 3431

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9084
Size: 296 Color: 3970
Size: 258 Color: 1090

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7953
Size: 337 Color: 5895
Size: 266 Color: 1776

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7873
Size: 322 Color: 5280
Size: 284 Color: 3198

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6866
Size: 328 Color: 5531
Size: 310 Color: 4716

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9385
Size: 274 Color: 2494
Size: 265 Color: 1724

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9073
Size: 297 Color: 4034
Size: 258 Color: 1072

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7119
Size: 326 Color: 5457
Size: 305 Color: 4459

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8151
Size: 340 Color: 6003
Size: 255 Color: 784

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7227
Size: 327 Color: 5504
Size: 301 Color: 4215

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9648
Size: 271 Color: 2236
Size: 253 Color: 537

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8791
Size: 319 Color: 5121
Size: 250 Color: 115

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7880
Size: 310 Color: 4683
Size: 295 Color: 3913

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6657
Size: 357 Color: 6637
Size: 287 Color: 3428

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9415
Size: 284 Color: 3182
Size: 254 Color: 682

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9681
Size: 268 Color: 1967
Size: 254 Color: 660

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7745
Size: 342 Color: 6108
Size: 268 Color: 1985

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7206
Size: 329 Color: 5616
Size: 300 Color: 4189

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7403
Size: 363 Color: 6857
Size: 259 Color: 1203

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9518
Size: 274 Color: 2511
Size: 257 Color: 1025

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8462
Size: 293 Color: 3797
Size: 291 Color: 3679

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7711
Size: 343 Color: 6144
Size: 268 Color: 1960

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8206
Size: 331 Color: 5688
Size: 262 Color: 1493

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9604
Size: 265 Color: 1684
Size: 262 Color: 1444

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9705
Size: 270 Color: 2152
Size: 250 Color: 6

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9748
Size: 259 Color: 1200
Size: 258 Color: 1071

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6904
Size: 322 Color: 5256
Size: 315 Color: 4946

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8020
Size: 309 Color: 4664
Size: 292 Color: 3694

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7208
Size: 332 Color: 5722
Size: 297 Color: 4016

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9952
Size: 253 Color: 487
Size: 250 Color: 179

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7092
Size: 329 Color: 5600
Size: 303 Color: 4345

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8202
Size: 330 Color: 5650
Size: 263 Color: 1533

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9088
Size: 288 Color: 3442
Size: 266 Color: 1775

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6802
Size: 335 Color: 5840
Size: 305 Color: 4427

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8526
Size: 321 Color: 5239
Size: 260 Color: 1299

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7078
Size: 324 Color: 5362
Size: 308 Color: 4585

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7329
Size: 348 Color: 6337
Size: 277 Color: 2751

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8134
Size: 335 Color: 5833
Size: 261 Color: 1355

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9528
Size: 280 Color: 2919
Size: 251 Color: 252

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8380
Size: 337 Color: 5894
Size: 250 Color: 161

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8983
Size: 298 Color: 4076
Size: 262 Color: 1445

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9019
Size: 302 Color: 4327
Size: 256 Color: 909

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8446
Size: 303 Color: 4356
Size: 281 Color: 3019

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9417
Size: 284 Color: 3190
Size: 253 Color: 513

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9055
Size: 294 Color: 3816
Size: 262 Color: 1458

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9596
Size: 266 Color: 1789
Size: 261 Color: 1356

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7175
Size: 324 Color: 5376
Size: 306 Color: 4497

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7300
Size: 332 Color: 5700
Size: 294 Color: 3819

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8945
Size: 300 Color: 4154
Size: 262 Color: 1481

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8198
Size: 333 Color: 5731
Size: 260 Color: 1260

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8071
Size: 334 Color: 5793
Size: 264 Color: 1586

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9143
Size: 276 Color: 2640
Size: 275 Color: 2538

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8621
Size: 304 Color: 4393
Size: 273 Color: 2438

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8544
Size: 298 Color: 4054
Size: 283 Color: 3147

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9049
Size: 297 Color: 4011
Size: 259 Color: 1192

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7993
Size: 330 Color: 5652
Size: 272 Color: 2316

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9345
Size: 287 Color: 3370
Size: 254 Color: 667

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8728
Size: 318 Color: 5082
Size: 254 Color: 659

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9368
Size: 284 Color: 3210
Size: 256 Color: 893

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8576
Size: 329 Color: 5598
Size: 250 Color: 17

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8122
Size: 331 Color: 5687
Size: 265 Color: 1715

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8037
Size: 318 Color: 5064
Size: 282 Color: 3066

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8220
Size: 337 Color: 5925
Size: 255 Color: 735

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7986
Size: 322 Color: 5285
Size: 280 Color: 2965

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8319
Size: 339 Color: 5981
Size: 250 Color: 42

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8483
Size: 329 Color: 5578
Size: 254 Color: 643

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7703
Size: 325 Color: 5436
Size: 286 Color: 3322

Bin 1601: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 233
Size: 250 Color: 143
Size: 250 Color: 44
Size: 250 Color: 35

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9399
Size: 282 Color: 3079
Size: 256 Color: 922

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8682
Size: 307 Color: 4519
Size: 268 Color: 2019

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7969
Size: 315 Color: 4921
Size: 287 Color: 3406

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9599
Size: 275 Color: 2534
Size: 252 Color: 444

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8281
Size: 338 Color: 5937
Size: 252 Color: 364

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6782
Size: 338 Color: 5933
Size: 302 Color: 4298

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8389
Size: 316 Color: 4968
Size: 270 Color: 2171

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9132
Size: 302 Color: 4305
Size: 250 Color: 74

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7651
Size: 343 Color: 6129
Size: 270 Color: 2150

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8331
Size: 339 Color: 5971
Size: 250 Color: 181

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8176
Size: 337 Color: 5910
Size: 257 Color: 1012

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6803
Size: 336 Color: 5861
Size: 304 Color: 4380

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8718
Size: 309 Color: 4637
Size: 264 Color: 1649

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9654
Size: 263 Color: 1571
Size: 261 Color: 1387

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7799
Size: 341 Color: 6078
Size: 267 Color: 1924

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7073
Size: 326 Color: 5460
Size: 306 Color: 4462

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9111
Size: 299 Color: 4107
Size: 254 Color: 686

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8635
Size: 314 Color: 4888
Size: 263 Color: 1552

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8770
Size: 316 Color: 4959
Size: 254 Color: 627

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8215
Size: 333 Color: 5759
Size: 260 Color: 1295

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6532
Size: 353 Color: 6518
Size: 295 Color: 3883

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8166
Size: 330 Color: 5622
Size: 264 Color: 1602

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9002
Size: 284 Color: 3223
Size: 275 Color: 2564

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8646
Size: 313 Color: 4844
Size: 263 Color: 1539

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7137
Size: 370 Color: 7136
Size: 261 Color: 1370

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6864
Size: 329 Color: 5599
Size: 309 Color: 4663

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7284
Size: 317 Color: 5053
Size: 309 Color: 4657

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7070
Size: 324 Color: 5378
Size: 308 Color: 4593

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9795
Size: 258 Color: 1101
Size: 256 Color: 926

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8626
Size: 296 Color: 3966
Size: 281 Color: 3013

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9270
Size: 288 Color: 3455
Size: 257 Color: 1033

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9487
Size: 275 Color: 2588
Size: 258 Color: 1109

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6658
Size: 357 Color: 6648
Size: 287 Color: 3381

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9188
Size: 279 Color: 2893
Size: 270 Color: 2197

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7920
Size: 304 Color: 4374
Size: 300 Color: 4201

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9331
Size: 290 Color: 3575
Size: 252 Color: 375

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9969
Size: 251 Color: 311
Size: 251 Color: 297

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9074
Size: 296 Color: 3931
Size: 259 Color: 1168

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7407
Size: 325 Color: 5398
Size: 297 Color: 4023

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7838
Size: 323 Color: 5301
Size: 284 Color: 3186

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9583
Size: 267 Color: 1943
Size: 261 Color: 1364

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7004
Size: 332 Color: 5724
Size: 302 Color: 4320

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7055
Size: 332 Color: 5718
Size: 301 Color: 4205

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8145
Size: 336 Color: 5856
Size: 260 Color: 1261

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7142
Size: 330 Color: 5658
Size: 300 Color: 4158

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6586
Size: 355 Color: 6579
Size: 291 Color: 3632

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7014
Size: 321 Color: 5204
Size: 313 Color: 4862

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7794
Size: 341 Color: 6070
Size: 267 Color: 1853

Bin 1650: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 9987
Size: 500 Color: 9981

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7901
Size: 311 Color: 4748
Size: 294 Color: 3835

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8422
Size: 331 Color: 5666
Size: 254 Color: 658

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8574
Size: 311 Color: 4736
Size: 268 Color: 2017

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9239
Size: 291 Color: 3683
Size: 256 Color: 883

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6961
Size: 318 Color: 5096
Size: 317 Color: 5011

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7984
Size: 309 Color: 4673
Size: 293 Color: 3793

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6616
Size: 355 Color: 6577
Size: 290 Color: 3606

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8599
Size: 314 Color: 4911
Size: 264 Color: 1621

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9816
Size: 262 Color: 1439
Size: 250 Color: 3

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9902
Size: 256 Color: 956
Size: 250 Color: 158

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6665
Size: 333 Color: 5758
Size: 310 Color: 4695

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8385
Size: 337 Color: 5900
Size: 250 Color: 176

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7883
Size: 314 Color: 4894
Size: 291 Color: 3675

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6845
Size: 328 Color: 5551
Size: 310 Color: 4715

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7000
Size: 332 Color: 5713
Size: 302 Color: 4300

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6821
Size: 330 Color: 5624
Size: 309 Color: 4624

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7980
Size: 336 Color: 5879
Size: 266 Color: 1807

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8092
Size: 335 Color: 5820
Size: 262 Color: 1488

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8323
Size: 339 Color: 5977
Size: 250 Color: 113

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8032
Size: 336 Color: 5870
Size: 264 Color: 1587

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8721
Size: 304 Color: 4390
Size: 268 Color: 2054

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9542
Size: 274 Color: 2506
Size: 256 Color: 863

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9226
Size: 282 Color: 3105
Size: 265 Color: 1733

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9089
Size: 288 Color: 3476
Size: 266 Color: 1828

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9598
Size: 275 Color: 2590
Size: 252 Color: 403

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6759
Size: 360 Color: 6742
Size: 281 Color: 2986

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9323
Size: 279 Color: 2881
Size: 264 Color: 1580

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9517
Size: 276 Color: 2664
Size: 255 Color: 724

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9653
Size: 274 Color: 2525
Size: 250 Color: 99

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8033
Size: 331 Color: 5690
Size: 269 Color: 2122

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9307
Size: 291 Color: 3673
Size: 253 Color: 557

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7770
Size: 357 Color: 6652
Size: 252 Color: 414

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7066
Size: 333 Color: 5768
Size: 299 Color: 4133

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8031
Size: 333 Color: 5765
Size: 267 Color: 1932

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9942
Size: 252 Color: 368
Size: 251 Color: 254

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8899
Size: 310 Color: 4727
Size: 254 Color: 609

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9733
Size: 268 Color: 2038
Size: 250 Color: 153

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7058
Size: 336 Color: 5873
Size: 297 Color: 3994

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8536
Size: 299 Color: 4144
Size: 282 Color: 3097

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8675
Size: 317 Color: 5054
Size: 258 Color: 1095

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9728
Size: 267 Color: 1946
Size: 251 Color: 307

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8210
Size: 337 Color: 5916
Size: 256 Color: 941

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9102
Size: 292 Color: 3697
Size: 261 Color: 1346

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7254
Size: 327 Color: 5520
Size: 300 Color: 4193

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8049
Size: 334 Color: 5814
Size: 265 Color: 1755

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8643
Size: 291 Color: 3633
Size: 285 Color: 3303

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8979
Size: 307 Color: 4507
Size: 253 Color: 543

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7681
Size: 343 Color: 6150
Size: 269 Color: 2121

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9873
Size: 255 Color: 739
Size: 253 Color: 527

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8560
Size: 290 Color: 3570
Size: 290 Color: 3567

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6497
Size: 352 Color: 6489
Size: 297 Color: 4032

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8865
Size: 291 Color: 3682
Size: 274 Color: 2480

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7400
Size: 368 Color: 7053
Size: 254 Color: 722

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6712
Size: 350 Color: 6431
Size: 292 Color: 3734

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8582
Size: 291 Color: 3652
Size: 288 Color: 3487

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7869
Size: 313 Color: 4842
Size: 293 Color: 3788

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8614
Size: 324 Color: 5371
Size: 253 Color: 517

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7887
Size: 306 Color: 4491
Size: 299 Color: 4126

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9827
Size: 256 Color: 953
Size: 255 Color: 769

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8730
Size: 295 Color: 3876
Size: 277 Color: 2722

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8860
Size: 316 Color: 4982
Size: 250 Color: 96

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8094
Size: 334 Color: 5795
Size: 263 Color: 1556

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7978
Size: 314 Color: 4880
Size: 288 Color: 3444

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9779
Size: 258 Color: 1147
Size: 257 Color: 1045

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7099
Size: 325 Color: 5420
Size: 307 Color: 4557

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6842
Size: 330 Color: 5637
Size: 309 Color: 4651

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9479
Size: 272 Color: 2308
Size: 262 Color: 1456

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8184
Size: 334 Color: 5801
Size: 260 Color: 1262

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9872
Size: 255 Color: 806
Size: 253 Color: 577

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7530
Size: 345 Color: 6246
Size: 273 Color: 2426

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8173
Size: 337 Color: 5888
Size: 257 Color: 997

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7130
Size: 327 Color: 5484
Size: 304 Color: 4394

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8089
Size: 336 Color: 5851
Size: 261 Color: 1361

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8119
Size: 331 Color: 5682
Size: 265 Color: 1707

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7657
Size: 363 Color: 6847
Size: 250 Color: 98

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8132
Size: 303 Color: 4332
Size: 293 Color: 3790

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6907
Size: 323 Color: 5341
Size: 314 Color: 4871

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9092
Size: 280 Color: 2957
Size: 274 Color: 2512

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7903
Size: 321 Color: 5206
Size: 284 Color: 3193

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8232
Size: 338 Color: 5936
Size: 254 Color: 611

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9052
Size: 288 Color: 3461
Size: 268 Color: 2046

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9492
Size: 267 Color: 1905
Size: 266 Color: 1760

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9906
Size: 253 Color: 547
Size: 253 Color: 492

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9714
Size: 261 Color: 1417
Size: 259 Color: 1159

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9295
Size: 274 Color: 2471
Size: 270 Color: 2181

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9390
Size: 279 Color: 2890
Size: 260 Color: 1257

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7132
Size: 370 Color: 7105
Size: 261 Color: 1381

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8103
Size: 334 Color: 5803
Size: 263 Color: 1518

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7304
Size: 319 Color: 5126
Size: 307 Color: 4544

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9242
Size: 292 Color: 3690
Size: 255 Color: 850

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8041
Size: 340 Color: 6027
Size: 260 Color: 1332

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8606
Size: 325 Color: 5427
Size: 253 Color: 574

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8168
Size: 330 Color: 5641
Size: 264 Color: 1623

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9858
Size: 257 Color: 962
Size: 252 Color: 365

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7842
Size: 305 Color: 4455
Size: 302 Color: 4276

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7002
Size: 319 Color: 5143
Size: 315 Color: 4922

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9505
Size: 273 Color: 2395
Size: 259 Color: 1187

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7456
Size: 346 Color: 6293
Size: 274 Color: 2470

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8421
Size: 321 Color: 5245
Size: 264 Color: 1628

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8174
Size: 334 Color: 5809
Size: 260 Color: 1274

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8052
Size: 347 Color: 6322
Size: 252 Color: 392

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7440
Size: 346 Color: 6250
Size: 275 Color: 2546

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7353
Size: 322 Color: 5296
Size: 302 Color: 4307

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7326
Size: 331 Color: 5691
Size: 294 Color: 3836

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8657
Size: 300 Color: 4191
Size: 276 Color: 2669

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7011
Size: 321 Color: 5207
Size: 313 Color: 4827

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6819
Size: 332 Color: 5705
Size: 307 Color: 4522

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7550
Size: 311 Color: 4780
Size: 306 Color: 4483

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6791
Size: 356 Color: 6600
Size: 284 Color: 3181

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8562
Size: 293 Color: 3755
Size: 287 Color: 3415

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9114
Size: 282 Color: 3068
Size: 271 Color: 2233

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8126
Size: 331 Color: 5677
Size: 265 Color: 1663

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8042
Size: 334 Color: 5773
Size: 265 Color: 1713

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9250
Size: 274 Color: 2467
Size: 272 Color: 2336

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8157
Size: 333 Color: 5764
Size: 262 Color: 1465

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6882
Size: 322 Color: 5275
Size: 316 Color: 4954

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7686
Size: 343 Color: 6165
Size: 269 Color: 2061

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7402
Size: 318 Color: 5098
Size: 304 Color: 4405

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8086
Size: 334 Color: 5808
Size: 264 Color: 1591

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6839
Size: 330 Color: 5627
Size: 309 Color: 4626

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8641
Size: 297 Color: 3988
Size: 279 Color: 2869

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7618
Size: 313 Color: 4838
Size: 301 Color: 4260

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7701
Size: 311 Color: 4762
Size: 300 Color: 4157

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6735
Size: 359 Color: 6703
Size: 283 Color: 3176

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9277
Size: 277 Color: 2725
Size: 268 Color: 1962

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9962
Size: 251 Color: 315
Size: 251 Color: 276

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9891
Size: 255 Color: 763
Size: 252 Color: 437

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8286
Size: 340 Color: 6031
Size: 250 Color: 135

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9928
Size: 252 Color: 399
Size: 252 Color: 351

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7924
Size: 328 Color: 5535
Size: 276 Color: 2660

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9051
Size: 280 Color: 2963
Size: 276 Color: 2652

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8269
Size: 340 Color: 6035
Size: 250 Color: 139

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7759
Size: 329 Color: 5586
Size: 280 Color: 2922

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7430
Size: 346 Color: 6252
Size: 275 Color: 2606

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7515
Size: 345 Color: 6241
Size: 273 Color: 2362

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 5824
Size: 333 Color: 5767
Size: 333 Color: 5760

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7599
Size: 364 Color: 6900
Size: 251 Color: 289

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6951
Size: 364 Color: 6923
Size: 272 Color: 2346

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8129
Size: 306 Color: 4471
Size: 290 Color: 3580

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7395
Size: 347 Color: 6315
Size: 275 Color: 2571

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7961
Size: 315 Color: 4953
Size: 288 Color: 3500

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7250
Size: 332 Color: 5696
Size: 295 Color: 3893

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9195
Size: 297 Color: 4033
Size: 252 Color: 467

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9366
Size: 271 Color: 2264
Size: 269 Color: 2089

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9513
Size: 269 Color: 2069
Size: 263 Color: 1561

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7858
Size: 313 Color: 4856
Size: 293 Color: 3757

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7817
Size: 341 Color: 6056
Size: 267 Color: 1883

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9686
Size: 269 Color: 2119
Size: 252 Color: 432

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7408
Size: 372 Color: 7195
Size: 250 Color: 182

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9766
Size: 260 Color: 1301
Size: 255 Color: 771

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7833
Size: 325 Color: 5419
Size: 282 Color: 3072

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7628
Size: 309 Color: 4633
Size: 305 Color: 4438

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7506
Size: 345 Color: 6238
Size: 273 Color: 2443

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6574
Size: 355 Color: 6566
Size: 291 Color: 3638

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9877
Size: 258 Color: 1118
Size: 250 Color: 152

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9864
Size: 257 Color: 1039
Size: 252 Color: 466

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7981
Size: 322 Color: 5251
Size: 280 Color: 2916

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7851
Size: 329 Color: 5585
Size: 278 Color: 2778

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6805
Size: 345 Color: 6223
Size: 294 Color: 3826

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9607
Size: 266 Color: 1842
Size: 260 Color: 1290

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9349
Size: 273 Color: 2398
Size: 268 Color: 1951

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8478
Size: 328 Color: 5541
Size: 255 Color: 746

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7572
Size: 344 Color: 6186
Size: 272 Color: 2355

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8997
Size: 309 Color: 4671
Size: 250 Color: 49

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8960
Size: 292 Color: 3735
Size: 269 Color: 2109

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9619
Size: 276 Color: 2690
Size: 250 Color: 19

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7276
Size: 322 Color: 5273
Size: 305 Color: 4417

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9116
Size: 301 Color: 4257
Size: 252 Color: 420

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7568
Size: 344 Color: 6184
Size: 272 Color: 2328

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9004
Size: 303 Color: 4367
Size: 256 Color: 903

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7153
Size: 324 Color: 5396
Size: 306 Color: 4487

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7909
Size: 318 Color: 5072
Size: 287 Color: 3397

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7285
Size: 325 Color: 5426
Size: 301 Color: 4244

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6840
Size: 337 Color: 5906
Size: 302 Color: 4314

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9822
Size: 259 Color: 1180
Size: 253 Color: 505

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8201
Size: 331 Color: 5675
Size: 262 Color: 1483

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6589
Size: 355 Color: 6575
Size: 291 Color: 3666

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8591
Size: 292 Color: 3724
Size: 286 Color: 3354

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7509
Size: 345 Color: 6240
Size: 273 Color: 2441

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7623
Size: 328 Color: 5542
Size: 286 Color: 3319

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8959
Size: 281 Color: 3025
Size: 280 Color: 2964

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9068
Size: 285 Color: 3252
Size: 270 Color: 2198

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8861
Size: 292 Color: 3729
Size: 274 Color: 2478

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9457
Size: 272 Color: 2326
Size: 263 Color: 1530

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9083
Size: 290 Color: 3603
Size: 264 Color: 1600

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8048
Size: 332 Color: 5716
Size: 267 Color: 1922

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9576
Size: 265 Color: 1671
Size: 263 Color: 1532

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9645
Size: 274 Color: 2452
Size: 250 Color: 85

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7979
Size: 337 Color: 5920
Size: 265 Color: 1667

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9462
Size: 279 Color: 2883
Size: 256 Color: 870

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8854
Size: 292 Color: 3721
Size: 274 Color: 2500

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7857
Size: 328 Color: 5556
Size: 278 Color: 2796

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8946
Size: 282 Color: 3088
Size: 280 Color: 2944

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8348
Size: 306 Color: 4503
Size: 282 Color: 3070

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 6363
Size: 340 Color: 6029
Size: 313 Color: 4829

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9279
Size: 274 Color: 2481
Size: 271 Color: 2265

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9078
Size: 278 Color: 2813
Size: 277 Color: 2763

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8007
Size: 327 Color: 5506
Size: 274 Color: 2446

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8301
Size: 313 Color: 4833
Size: 276 Color: 2685

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9130
Size: 278 Color: 2807
Size: 274 Color: 2468

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7795
Size: 313 Color: 4865
Size: 295 Color: 3911

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7648
Size: 343 Color: 6122
Size: 270 Color: 2141

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6437
Size: 350 Color: 6411
Size: 301 Color: 4242

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8622
Size: 298 Color: 4065
Size: 279 Color: 2857

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9151
Size: 285 Color: 3306
Size: 266 Color: 1823

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8105
Size: 334 Color: 5818
Size: 263 Color: 1504

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8247
Size: 340 Color: 6018
Size: 252 Color: 378

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9225
Size: 296 Color: 3960
Size: 251 Color: 215

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8135
Size: 331 Color: 5686
Size: 265 Color: 1711

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7454
Size: 346 Color: 6277
Size: 274 Color: 2483

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8045
Size: 333 Color: 5738
Size: 266 Color: 1802

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8065
Size: 334 Color: 5798
Size: 264 Color: 1585

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9790
Size: 264 Color: 1638
Size: 250 Color: 155

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8330
Size: 339 Color: 5986
Size: 250 Color: 88

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6889
Size: 363 Color: 6877
Size: 275 Color: 2591

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7885
Size: 327 Color: 5499
Size: 278 Color: 2840

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7593
Size: 314 Color: 4872
Size: 301 Color: 4229

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8529
Size: 311 Color: 4761
Size: 270 Color: 2174

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8069
Size: 334 Color: 5772
Size: 264 Color: 1633

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8131
Size: 331 Color: 5683
Size: 265 Color: 1736

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8458
Size: 329 Color: 5615
Size: 255 Color: 791

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7951
Size: 317 Color: 5026
Size: 286 Color: 3324

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8706
Size: 289 Color: 3512
Size: 284 Color: 3191

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7192
Size: 333 Color: 5732
Size: 296 Color: 3964

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7921
Size: 310 Color: 4726
Size: 294 Color: 3853

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7699
Size: 329 Color: 5613
Size: 282 Color: 3047

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7510
Size: 345 Color: 6221
Size: 273 Color: 2409

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7036
Size: 333 Color: 5750
Size: 300 Color: 4190

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8006
Size: 331 Color: 5669
Size: 270 Color: 2167

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7171
Size: 350 Color: 6438
Size: 280 Color: 2946

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7237
Size: 368 Color: 7029
Size: 260 Color: 1267

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7535
Size: 319 Color: 5116
Size: 298 Color: 4079

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6906
Size: 323 Color: 5317
Size: 314 Color: 4896

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7988
Size: 336 Color: 5865
Size: 266 Color: 1764

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9695
Size: 266 Color: 1820
Size: 255 Color: 782

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9811
Size: 258 Color: 1105
Size: 255 Color: 815

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9475
Size: 275 Color: 2582
Size: 259 Color: 1222

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8426
Size: 333 Color: 5736
Size: 252 Color: 460

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8115
Size: 336 Color: 5857
Size: 260 Color: 1326

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8254
Size: 338 Color: 5940
Size: 253 Color: 539

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8772
Size: 285 Color: 3302
Size: 285 Color: 3289

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7006
Size: 320 Color: 5178
Size: 314 Color: 4904

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8969
Size: 301 Color: 4227
Size: 259 Color: 1178

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8390
Size: 330 Color: 5625
Size: 256 Color: 917

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8430
Size: 309 Color: 4621
Size: 276 Color: 2621

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7492
Size: 344 Color: 6177
Size: 275 Color: 2565

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8733
Size: 290 Color: 3609
Size: 282 Color: 3074

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8581
Size: 296 Color: 3969
Size: 283 Color: 3177

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6917
Size: 358 Color: 6681
Size: 279 Color: 2876

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9014
Size: 280 Color: 2969
Size: 278 Color: 2783

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7777
Size: 333 Color: 5733
Size: 276 Color: 2630

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8801
Size: 298 Color: 4081
Size: 271 Color: 2267

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8613
Size: 292 Color: 3709
Size: 285 Color: 3259

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8141
Size: 303 Color: 4338
Size: 293 Color: 3767

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7248
Size: 320 Color: 5151
Size: 307 Color: 4531

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8322
Size: 339 Color: 5974
Size: 250 Color: 55

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8167
Size: 337 Color: 5913
Size: 257 Color: 980

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9372
Size: 282 Color: 3101
Size: 257 Color: 1053

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9447
Size: 284 Color: 3224
Size: 252 Color: 435

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9404
Size: 284 Color: 3245
Size: 254 Color: 672

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9016
Size: 282 Color: 3096
Size: 276 Color: 2625

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9205
Size: 275 Color: 2547
Size: 273 Color: 2389

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8523
Size: 291 Color: 3654
Size: 290 Color: 3569

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9124
Size: 293 Color: 3741
Size: 259 Color: 1175

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6533
Size: 353 Color: 6526
Size: 295 Color: 3912

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6851
Size: 326 Color: 5446
Size: 312 Color: 4782

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8750
Size: 317 Color: 5042
Size: 254 Color: 715

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7944
Size: 317 Color: 5024
Size: 286 Color: 3337

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9927
Size: 253 Color: 599
Size: 251 Color: 313

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9746
Size: 267 Color: 1876
Size: 250 Color: 177

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6817
Size: 330 Color: 5634
Size: 309 Color: 4646

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7133
Size: 370 Color: 7122
Size: 261 Color: 1385

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8911
Size: 293 Color: 3782
Size: 270 Color: 2146

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6650
Size: 357 Color: 6636
Size: 287 Color: 3391

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7819
Size: 341 Color: 6076
Size: 267 Color: 1896

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7938
Size: 315 Color: 4932
Size: 288 Color: 3450

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8208
Size: 336 Color: 5882
Size: 257 Color: 970

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6789
Size: 360 Color: 6775
Size: 280 Color: 2937

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8044
Size: 334 Color: 5777
Size: 265 Color: 1681

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9395
Size: 283 Color: 3137
Size: 255 Color: 742

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7780
Size: 315 Color: 4917
Size: 294 Color: 3840

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8762
Size: 304 Color: 4413
Size: 266 Color: 1829

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7943
Size: 337 Color: 5884
Size: 266 Color: 1774

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7989
Size: 337 Color: 5923
Size: 265 Color: 1706

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8901
Size: 310 Color: 4735
Size: 253 Color: 499

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9254
Size: 292 Color: 3712
Size: 254 Color: 610

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7751
Size: 342 Color: 6106
Size: 268 Color: 1952

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9849
Size: 257 Color: 985
Size: 253 Color: 556

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7713
Size: 357 Color: 6646
Size: 254 Color: 708

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7905
Size: 312 Color: 4805
Size: 293 Color: 3786

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8051
Size: 336 Color: 5869
Size: 263 Color: 1567

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7900
Size: 321 Color: 5235
Size: 284 Color: 3183

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8496
Size: 313 Color: 4853
Size: 269 Color: 2076

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7389
Size: 347 Color: 6331
Size: 276 Color: 2646

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8378
Size: 331 Color: 5664
Size: 256 Color: 901

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7054
Size: 367 Color: 6993
Size: 266 Color: 1799

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9139
Size: 294 Color: 3812
Size: 257 Color: 964

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8944
Size: 307 Color: 4536
Size: 255 Color: 837

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8117
Size: 336 Color: 5866
Size: 260 Color: 1259

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8084
Size: 342 Color: 6118
Size: 256 Color: 929

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8063
Size: 333 Color: 5757
Size: 266 Color: 1769

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7908
Size: 322 Color: 5249
Size: 283 Color: 3155

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6713
Size: 359 Color: 6704
Size: 283 Color: 3117

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9918
Size: 255 Color: 795
Size: 250 Color: 183

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8472
Size: 317 Color: 5034
Size: 266 Color: 1834

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9185
Size: 276 Color: 2626
Size: 273 Color: 2358

Bin 1957: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 328
Size: 250 Color: 130
Size: 250 Color: 39
Size: 250 Color: 36

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7474
Size: 365 Color: 6947
Size: 255 Color: 830

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8418
Size: 325 Color: 5405
Size: 260 Color: 1263

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9551
Size: 265 Color: 1680
Size: 265 Color: 1672

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7683
Size: 343 Color: 6167
Size: 269 Color: 2063

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9956
Size: 252 Color: 465
Size: 250 Color: 186

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8583
Size: 294 Color: 3861
Size: 285 Color: 3296

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6986
Size: 332 Color: 5714
Size: 303 Color: 4363

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9764
Size: 261 Color: 1378
Size: 255 Color: 796

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8107
Size: 300 Color: 4180
Size: 297 Color: 3983

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7897
Size: 317 Color: 5029
Size: 288 Color: 3441

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8627
Size: 324 Color: 5391
Size: 253 Color: 548

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9237
Size: 296 Color: 3981
Size: 251 Color: 257

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7915
Size: 322 Color: 5289
Size: 282 Color: 3104

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7298
Size: 321 Color: 5228
Size: 305 Color: 4429

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8400
Size: 309 Color: 4660
Size: 277 Color: 2712

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9069
Size: 298 Color: 4045
Size: 257 Color: 1026

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6972
Size: 318 Color: 5076
Size: 317 Color: 5037

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8629
Size: 293 Color: 3787
Size: 284 Color: 3197

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7251
Size: 323 Color: 5318
Size: 304 Color: 4397

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9915
Size: 253 Color: 582
Size: 252 Color: 413

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8805
Size: 286 Color: 3345
Size: 282 Color: 3058

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8388
Size: 294 Color: 3858
Size: 292 Color: 3730

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9957
Size: 252 Color: 421
Size: 250 Color: 193

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8817
Size: 286 Color: 3347
Size: 282 Color: 3060

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7689
Size: 361 Color: 6790
Size: 251 Color: 310

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9626
Size: 271 Color: 2262
Size: 254 Color: 652

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6728
Size: 359 Color: 6707
Size: 283 Color: 3164

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8539
Size: 311 Color: 4769
Size: 270 Color: 2166

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8645
Size: 307 Color: 4504
Size: 269 Color: 2060

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7808
Size: 317 Color: 5043
Size: 291 Color: 3688

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6891
Size: 363 Color: 6849
Size: 275 Color: 2531

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6540
Size: 343 Color: 6149
Size: 304 Color: 4407

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8351
Size: 294 Color: 3856
Size: 294 Color: 3820

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9430
Size: 273 Color: 2437
Size: 264 Color: 1651

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7498
Size: 314 Color: 4885
Size: 305 Color: 4428

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7727
Size: 342 Color: 6093
Size: 268 Color: 2009

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9783
Size: 263 Color: 1573
Size: 252 Color: 382

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8752
Size: 295 Color: 3924
Size: 276 Color: 2667

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9855
Size: 257 Color: 1005
Size: 252 Color: 464

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9027
Size: 300 Color: 4156
Size: 258 Color: 1141

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7914
Size: 303 Color: 4359
Size: 301 Color: 4256

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9263
Size: 288 Color: 3462
Size: 258 Color: 1103

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8700
Size: 321 Color: 5243
Size: 252 Color: 419

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7551
Size: 313 Color: 4851
Size: 304 Color: 4399

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7831
Size: 317 Color: 5015
Size: 290 Color: 3604

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7101
Size: 324 Color: 5380
Size: 308 Color: 4572

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7087
Size: 338 Color: 5953
Size: 294 Color: 3850

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9003
Size: 282 Color: 3095
Size: 277 Color: 2773

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8465
Size: 299 Color: 4138
Size: 285 Color: 3260

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9449
Size: 282 Color: 3043
Size: 254 Color: 690

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9575
Size: 276 Color: 2678
Size: 252 Color: 393

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6835
Size: 362 Color: 6806
Size: 277 Color: 2761

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7224
Size: 349 Color: 6380
Size: 279 Color: 2854

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9077
Size: 291 Color: 3685
Size: 264 Color: 1626

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8402
Size: 297 Color: 4003
Size: 289 Color: 3560

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7039
Size: 332 Color: 5726
Size: 301 Color: 4258

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9797
Size: 263 Color: 1545
Size: 251 Color: 249

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6764
Size: 351 Color: 6456
Size: 290 Color: 3623

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9765
Size: 265 Color: 1721
Size: 250 Color: 34

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9154
Size: 287 Color: 3392
Size: 263 Color: 1559

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8851
Size: 284 Color: 3243
Size: 282 Color: 3087

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8234
Size: 338 Color: 5956
Size: 254 Color: 685

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9288
Size: 275 Color: 2587
Size: 269 Color: 2072

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7314
Size: 348 Color: 6348
Size: 277 Color: 2708

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6701
Size: 358 Color: 6668
Size: 285 Color: 3279

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9431
Size: 284 Color: 3202
Size: 253 Color: 561

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9917
Size: 253 Color: 588
Size: 252 Color: 372

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9883
Size: 256 Color: 946
Size: 251 Color: 263

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8849
Size: 300 Color: 4171
Size: 266 Color: 1778

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6696
Size: 358 Color: 6687
Size: 285 Color: 3257

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 7999
Size: 331 Color: 5678
Size: 270 Color: 2176

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9886
Size: 257 Color: 1057
Size: 250 Color: 9

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9922
Size: 255 Color: 728
Size: 250 Color: 172

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7859
Size: 329 Color: 5617
Size: 277 Color: 2754

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8780
Size: 301 Color: 4237
Size: 268 Color: 2001

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9838
Size: 258 Color: 1065
Size: 252 Color: 468

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8082
Size: 334 Color: 5792
Size: 264 Color: 1659

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7174
Size: 320 Color: 5153
Size: 310 Color: 4721

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8926
Size: 302 Color: 4291
Size: 260 Color: 1293

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9096
Size: 301 Color: 4214
Size: 253 Color: 545

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8751
Size: 306 Color: 4475
Size: 265 Color: 1728

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9229
Size: 287 Color: 3360
Size: 260 Color: 1294

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9209
Size: 277 Color: 2765
Size: 271 Color: 2266

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8834
Size: 298 Color: 4089
Size: 269 Color: 2088

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8497
Size: 323 Color: 5331
Size: 259 Color: 1210

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8379
Size: 334 Color: 5816
Size: 253 Color: 471

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7379
Size: 347 Color: 6298
Size: 276 Color: 2620

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8182
Size: 337 Color: 5891
Size: 257 Color: 988

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7691
Size: 326 Color: 5465
Size: 286 Color: 3359

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6869
Size: 326 Color: 5459
Size: 312 Color: 4804

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7152
Size: 317 Color: 5013
Size: 313 Color: 4855

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9870
Size: 254 Color: 673
Size: 254 Color: 650

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9439
Size: 274 Color: 2508
Size: 262 Color: 1433

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7561
Size: 362 Color: 6816
Size: 254 Color: 656

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8085
Size: 334 Color: 5781
Size: 264 Color: 1584

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7173
Size: 320 Color: 5182
Size: 310 Color: 4681

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9937
Size: 253 Color: 598
Size: 251 Color: 231

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9845
Size: 255 Color: 803
Size: 255 Color: 801

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 9985
Size: 251 Color: 267
Size: 250 Color: 20

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9080
Size: 297 Color: 4036
Size: 257 Color: 1030

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8686
Size: 287 Color: 3380
Size: 287 Color: 3372

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8692
Size: 291 Color: 3672
Size: 283 Color: 3118

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8300
Size: 339 Color: 5997
Size: 250 Color: 150

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6966
Size: 351 Color: 6471
Size: 284 Color: 3235

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9935
Size: 254 Color: 720
Size: 250 Color: 149

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8274
Size: 339 Color: 5989
Size: 251 Color: 236

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6844
Size: 329 Color: 5618
Size: 309 Color: 4631

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7013
Size: 340 Color: 6036
Size: 294 Color: 3863

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6674
Size: 358 Color: 6663
Size: 285 Color: 3274

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8047
Size: 333 Color: 5742
Size: 266 Color: 1848

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9085
Size: 289 Color: 3547
Size: 265 Color: 1719

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7967
Size: 321 Color: 5231
Size: 282 Color: 3044

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8002
Size: 313 Color: 4866
Size: 288 Color: 3488

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8188
Size: 333 Color: 5734
Size: 261 Color: 1402

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7158
Size: 333 Color: 5739
Size: 297 Color: 4012

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8106
Size: 332 Color: 5706
Size: 265 Color: 1746

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8138
Size: 336 Color: 5874
Size: 260 Color: 1308

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8128
Size: 336 Color: 5843
Size: 260 Color: 1279

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8239
Size: 338 Color: 5951
Size: 254 Color: 684

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8225
Size: 336 Color: 5875
Size: 256 Color: 939

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8240
Size: 330 Color: 5651
Size: 262 Color: 1462

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8258
Size: 338 Color: 5945
Size: 253 Color: 472

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8250
Size: 338 Color: 5947
Size: 253 Color: 476

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8249
Size: 338 Color: 5943
Size: 253 Color: 526

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8251
Size: 338 Color: 5952
Size: 253 Color: 591

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8265
Size: 338 Color: 5954
Size: 253 Color: 549

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8255
Size: 338 Color: 5955
Size: 253 Color: 533

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8266
Size: 338 Color: 5934
Size: 253 Color: 485

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8260
Size: 338 Color: 5957
Size: 253 Color: 603

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8104
Size: 327 Color: 5527
Size: 270 Color: 2200

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8101
Size: 335 Color: 5834
Size: 262 Color: 1455

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8139
Size: 335 Color: 5836
Size: 261 Color: 1367

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8127
Size: 336 Color: 5852
Size: 260 Color: 1248

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8186
Size: 337 Color: 5927
Size: 257 Color: 1003

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8199
Size: 337 Color: 5917
Size: 256 Color: 880

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8213
Size: 300 Color: 4199
Size: 293 Color: 3792

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8203
Size: 337 Color: 5892
Size: 256 Color: 876

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8193
Size: 330 Color: 5626
Size: 263 Color: 1538

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7991
Size: 322 Color: 5257
Size: 280 Color: 2966

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8059
Size: 334 Color: 5783
Size: 265 Color: 1742

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8087
Size: 334 Color: 5775
Size: 264 Color: 1647

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8066
Size: 334 Color: 5806
Size: 264 Color: 1622

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8068
Size: 334 Color: 5785
Size: 264 Color: 1660

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8075
Size: 334 Color: 5790
Size: 264 Color: 1598

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8099
Size: 334 Color: 5805
Size: 263 Color: 1511

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8093
Size: 334 Color: 5782
Size: 263 Color: 1551

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9374
Size: 277 Color: 2732
Size: 262 Color: 1452

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8096
Size: 334 Color: 5804
Size: 263 Color: 1510

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8095
Size: 334 Color: 5780
Size: 263 Color: 1531

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7964
Size: 323 Color: 5312
Size: 280 Color: 2940

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8102
Size: 335 Color: 5827
Size: 262 Color: 1485

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8144
Size: 336 Color: 5855
Size: 260 Color: 1253

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 6279
Size: 337 Color: 5908
Size: 318 Color: 5085

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8691
Size: 302 Color: 4319
Size: 272 Color: 2288

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8153
Size: 336 Color: 5847
Size: 259 Color: 1182

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8112
Size: 336 Color: 5858
Size: 261 Color: 1363

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8088
Size: 336 Color: 5862
Size: 261 Color: 1371

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9276
Size: 284 Color: 3199
Size: 261 Color: 1345

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8055
Size: 333 Color: 5737
Size: 266 Color: 1833

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8050
Size: 333 Color: 5743
Size: 266 Color: 1796

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8057
Size: 334 Color: 5771
Size: 265 Color: 1685

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8074
Size: 334 Color: 5797
Size: 264 Color: 1611

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8072
Size: 334 Color: 5815
Size: 264 Color: 1583

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8077
Size: 334 Color: 5794
Size: 264 Color: 1655

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8142
Size: 300 Color: 4152
Size: 296 Color: 3952

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8018
Size: 336 Color: 5859
Size: 265 Color: 1720

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8016
Size: 320 Color: 5161
Size: 281 Color: 2983

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8334
Size: 333 Color: 5745
Size: 255 Color: 797

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8216
Size: 331 Color: 5673
Size: 262 Color: 1480

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 7968
Size: 337 Color: 5886
Size: 265 Color: 1691

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8195
Size: 331 Color: 5665
Size: 262 Color: 1442

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8211
Size: 331 Color: 5676
Size: 262 Color: 1441

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8120
Size: 331 Color: 5668
Size: 265 Color: 1741

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7959
Size: 337 Color: 5919
Size: 266 Color: 1831

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7948
Size: 337 Color: 5893
Size: 266 Color: 1845

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7947
Size: 337 Color: 5903
Size: 266 Color: 1817

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7666
Size: 342 Color: 6111
Size: 271 Color: 2273

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7219
Size: 328 Color: 5574
Size: 300 Color: 4179

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7222
Size: 327 Color: 5515
Size: 301 Color: 4209

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7226
Size: 327 Color: 5509
Size: 301 Color: 4207

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7245
Size: 327 Color: 5497
Size: 301 Color: 4266

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7230
Size: 327 Color: 5493
Size: 301 Color: 4216

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7229
Size: 327 Color: 5491
Size: 301 Color: 4238

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7220
Size: 327 Color: 5485
Size: 301 Color: 4241

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7253
Size: 329 Color: 5605
Size: 298 Color: 4049

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7965
Size: 323 Color: 5310
Size: 280 Color: 2939

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7125
Size: 327 Color: 5525
Size: 304 Color: 4385

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7129
Size: 327 Color: 5516
Size: 304 Color: 4404

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7138
Size: 327 Color: 5508
Size: 304 Color: 4402

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7180
Size: 371 Color: 7141
Size: 259 Color: 1166

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7049
Size: 351 Color: 6453
Size: 282 Color: 3062

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7148
Size: 364 Color: 6894
Size: 266 Color: 1839

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7082
Size: 323 Color: 5326
Size: 309 Color: 4612

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7067
Size: 325 Color: 5424
Size: 307 Color: 4547

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7118
Size: 370 Color: 7104
Size: 261 Color: 1391

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7117
Size: 370 Color: 7102
Size: 261 Color: 1380

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7128
Size: 326 Color: 5479
Size: 305 Color: 4421

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7112
Size: 327 Color: 5486
Size: 304 Color: 4371

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7107
Size: 327 Color: 5503
Size: 304 Color: 4415

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7108
Size: 327 Color: 5495
Size: 304 Color: 4410

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7111
Size: 327 Color: 5500
Size: 304 Color: 4391

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7177
Size: 327 Color: 5511
Size: 303 Color: 4360

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7060
Size: 322 Color: 5288
Size: 311 Color: 4745

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7044
Size: 322 Color: 5262
Size: 311 Color: 4768

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7057
Size: 336 Color: 5848
Size: 297 Color: 4010

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7030
Size: 323 Color: 5325
Size: 310 Color: 4693

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7046
Size: 323 Color: 5316
Size: 310 Color: 4692

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7033
Size: 323 Color: 5313
Size: 310 Color: 4685

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7038
Size: 368 Color: 7035
Size: 265 Color: 1682

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7041
Size: 368 Color: 7023
Size: 265 Color: 1731

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7040
Size: 323 Color: 5327
Size: 310 Color: 4706

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7069
Size: 324 Color: 5392
Size: 308 Color: 4563

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7083
Size: 324 Color: 5372
Size: 308 Color: 4588

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7095
Size: 324 Color: 5365
Size: 308 Color: 4589

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7088
Size: 324 Color: 5352
Size: 308 Color: 4569

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7093
Size: 324 Color: 5381
Size: 308 Color: 4607

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7068
Size: 324 Color: 5394
Size: 308 Color: 4565

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7221
Size: 369 Color: 7098
Size: 259 Color: 1149

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7090
Size: 324 Color: 5382
Size: 308 Color: 4575

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7114
Size: 324 Color: 5386
Size: 307 Color: 4515

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7086
Size: 325 Color: 5410
Size: 307 Color: 4535

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7079
Size: 325 Color: 5403
Size: 307 Color: 4533

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7065
Size: 325 Color: 5406
Size: 307 Color: 4508

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7081
Size: 325 Color: 5408
Size: 307 Color: 4542

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7003
Size: 320 Color: 5180
Size: 314 Color: 4869

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7008
Size: 320 Color: 5155
Size: 314 Color: 4906

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7018
Size: 320 Color: 5156
Size: 314 Color: 4890

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7577
Size: 336 Color: 5881
Size: 280 Color: 2968

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7012
Size: 321 Color: 5218
Size: 313 Color: 4861

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7061
Size: 321 Color: 5223
Size: 312 Color: 4823

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7028
Size: 321 Color: 5244
Size: 312 Color: 4791

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6973
Size: 319 Color: 5119
Size: 316 Color: 4973

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6965
Size: 319 Color: 5113
Size: 316 Color: 4983

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6977
Size: 319 Color: 5124
Size: 316 Color: 5001

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6967
Size: 319 Color: 5115
Size: 316 Color: 4960

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7019
Size: 367 Color: 7005
Size: 267 Color: 1860

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9572
Size: 267 Color: 1903
Size: 261 Color: 1398

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 6998
Size: 321 Color: 5242
Size: 313 Color: 4843

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6936
Size: 320 Color: 5158
Size: 316 Color: 4966

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6953
Size: 320 Color: 5157
Size: 316 Color: 4980

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7123
Size: 365 Color: 6945
Size: 266 Color: 1790

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6955
Size: 329 Color: 5602
Size: 307 Color: 4518

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7154
Size: 336 Color: 5846
Size: 294 Color: 3870

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7121
Size: 365 Color: 6949
Size: 266 Color: 1763

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6959
Size: 329 Color: 5584
Size: 307 Color: 4540

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6931
Size: 329 Color: 5595
Size: 307 Color: 4512

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6968
Size: 318 Color: 5069
Size: 317 Color: 5010

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6979
Size: 318 Color: 5071
Size: 317 Color: 5060

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6990
Size: 318 Color: 5094
Size: 317 Color: 5048

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6989
Size: 351 Color: 6448
Size: 284 Color: 3201

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6982
Size: 318 Color: 5073
Size: 317 Color: 5016

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6987
Size: 318 Color: 5063
Size: 317 Color: 5052

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6960
Size: 318 Color: 5078
Size: 317 Color: 5006

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6963
Size: 318 Color: 5084
Size: 317 Color: 5032

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6919
Size: 324 Color: 5367
Size: 313 Color: 4846

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6916
Size: 322 Color: 5276
Size: 315 Color: 4930

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6905
Size: 322 Color: 5253
Size: 315 Color: 4925

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6895
Size: 322 Color: 5277
Size: 315 Color: 4924

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6925
Size: 321 Color: 5209
Size: 315 Color: 4933

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6933
Size: 321 Color: 5234
Size: 315 Color: 4934

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6926
Size: 320 Color: 5171
Size: 316 Color: 4998

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6932
Size: 322 Color: 5272
Size: 314 Color: 4902

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6942
Size: 320 Color: 5175
Size: 316 Color: 4993

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6861
Size: 329 Color: 5620
Size: 309 Color: 4662

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6853
Size: 329 Color: 5611
Size: 309 Color: 4634

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6871
Size: 329 Color: 5601
Size: 309 Color: 4672

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6876
Size: 329 Color: 5579
Size: 309 Color: 4629

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6918
Size: 323 Color: 5339
Size: 314 Color: 4891

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6911
Size: 323 Color: 5332
Size: 314 Color: 4879

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6899
Size: 323 Color: 5324
Size: 314 Color: 4874

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9184
Size: 290 Color: 3577
Size: 259 Color: 1148

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6908
Size: 322 Color: 5271
Size: 315 Color: 4920

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6836
Size: 337 Color: 5899
Size: 302 Color: 4316

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6824
Size: 330 Color: 5644
Size: 309 Color: 4619

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6829
Size: 330 Color: 5639
Size: 309 Color: 4653

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6878
Size: 328 Color: 5544
Size: 310 Color: 4684

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6888
Size: 328 Color: 5536
Size: 310 Color: 4686

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6875
Size: 328 Color: 5560
Size: 310 Color: 4707

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6856
Size: 326 Color: 5448
Size: 312 Color: 4783

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6846
Size: 326 Color: 5443
Size: 312 Color: 4808

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6850
Size: 326 Color: 5456
Size: 312 Color: 4796

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6826
Size: 330 Color: 5649
Size: 309 Color: 4659

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6830
Size: 330 Color: 5642
Size: 309 Color: 4647

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6841
Size: 360 Color: 6770
Size: 279 Color: 2874

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6808
Size: 330 Color: 5629
Size: 309 Color: 4636

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6852
Size: 351 Color: 6475
Size: 287 Color: 3401

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6867
Size: 328 Color: 5554
Size: 310 Color: 4688

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6892
Size: 328 Color: 5534
Size: 310 Color: 4699

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6874
Size: 328 Color: 5566
Size: 310 Color: 4689

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6793
Size: 338 Color: 5932
Size: 302 Color: 4289

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6795
Size: 361 Color: 6783
Size: 279 Color: 2846

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6801
Size: 338 Color: 5941
Size: 302 Color: 4294

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6780
Size: 356 Color: 6614
Size: 284 Color: 3220

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6794
Size: 338 Color: 5944
Size: 302 Color: 4286

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6781
Size: 338 Color: 5939
Size: 302 Color: 4287

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6798
Size: 338 Color: 5950
Size: 302 Color: 4295

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6787
Size: 356 Color: 6625
Size: 284 Color: 3204

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6796
Size: 337 Color: 5914
Size: 303 Color: 4344

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8261
Size: 305 Color: 4444
Size: 286 Color: 3358

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6784
Size: 337 Color: 5922
Size: 303 Color: 4339

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6753
Size: 360 Color: 6741
Size: 281 Color: 2975

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6762
Size: 360 Color: 6743
Size: 281 Color: 3027

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6749
Size: 360 Color: 6748
Size: 281 Color: 3037

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6772
Size: 360 Color: 6754
Size: 281 Color: 2991

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6729
Size: 359 Color: 6719
Size: 283 Color: 3168

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6718
Size: 359 Color: 6706
Size: 283 Color: 3153

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6722
Size: 359 Color: 6711
Size: 283 Color: 3113

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6727
Size: 359 Color: 6708
Size: 283 Color: 3133

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6726
Size: 359 Color: 6714
Size: 283 Color: 3129

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6738
Size: 359 Color: 6732
Size: 283 Color: 3171

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6731
Size: 359 Color: 6717
Size: 283 Color: 3149

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6715
Size: 359 Color: 6710
Size: 283 Color: 3161

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6689
Size: 358 Color: 6683
Size: 285 Color: 3295

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6896
Size: 358 Color: 6695
Size: 279 Color: 2898

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6676
Size: 358 Color: 6671
Size: 285 Color: 3263

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6694
Size: 358 Color: 6667
Size: 285 Color: 3248

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6641
Size: 357 Color: 6639
Size: 287 Color: 3405

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6656
Size: 357 Color: 6654
Size: 287 Color: 3385

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6643
Size: 357 Color: 6640
Size: 287 Color: 3418

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6653
Size: 356 Color: 6624
Size: 288 Color: 3494

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6612
Size: 356 Color: 6608
Size: 289 Color: 3538

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6603
Size: 356 Color: 6598
Size: 289 Color: 3524

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6622
Size: 356 Color: 6607
Size: 289 Color: 3550

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6613
Size: 356 Color: 6595
Size: 289 Color: 3561

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6618
Size: 356 Color: 6609
Size: 289 Color: 3526

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6626
Size: 356 Color: 6597
Size: 289 Color: 3554

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6610
Size: 356 Color: 6599
Size: 289 Color: 3506

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6619
Size: 356 Color: 6611
Size: 289 Color: 3520

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6617
Size: 356 Color: 6602
Size: 289 Color: 3543

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7916
Size: 302 Color: 4293
Size: 302 Color: 4290

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6581
Size: 355 Color: 6571
Size: 291 Color: 3660

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6576
Size: 355 Color: 6570
Size: 291 Color: 3648

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6593
Size: 355 Color: 6573
Size: 291 Color: 3677

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6583
Size: 355 Color: 6572
Size: 291 Color: 3630

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6584
Size: 355 Color: 6578
Size: 291 Color: 3636

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6580
Size: 355 Color: 6568
Size: 291 Color: 3642

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6591
Size: 355 Color: 6585
Size: 291 Color: 3655

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6590
Size: 355 Color: 6569
Size: 291 Color: 3650

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6582
Size: 354 Color: 6564
Size: 292 Color: 3706

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6546
Size: 354 Color: 6544
Size: 293 Color: 3770

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6562
Size: 354 Color: 6539
Size: 293 Color: 3806

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6555
Size: 354 Color: 6548
Size: 293 Color: 3759

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6561
Size: 354 Color: 6549
Size: 293 Color: 3760

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6550
Size: 354 Color: 6538
Size: 293 Color: 3745

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6557
Size: 354 Color: 6541
Size: 293 Color: 3749

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6536
Size: 353 Color: 6516
Size: 295 Color: 3872

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6530
Size: 353 Color: 6517
Size: 295 Color: 3879

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6535
Size: 353 Color: 6528
Size: 295 Color: 3914

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6514
Size: 352 Color: 6507
Size: 296 Color: 3955

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6501
Size: 352 Color: 6488
Size: 297 Color: 4021

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6492
Size: 352 Color: 6484
Size: 297 Color: 3998

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6503
Size: 352 Color: 6499
Size: 297 Color: 3987

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6500
Size: 352 Color: 6487
Size: 297 Color: 3986

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6509
Size: 352 Color: 6490
Size: 297 Color: 3996

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6494
Size: 351 Color: 6465
Size: 298 Color: 4059

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6480
Size: 351 Color: 6474
Size: 299 Color: 4106

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6482
Size: 351 Color: 6466
Size: 299 Color: 4110

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6463
Size: 351 Color: 6450
Size: 299 Color: 4143

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6472
Size: 351 Color: 6454
Size: 299 Color: 4134

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6470
Size: 351 Color: 6467
Size: 299 Color: 4148

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6476
Size: 351 Color: 6464
Size: 299 Color: 4108

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8585
Size: 316 Color: 4979
Size: 263 Color: 1560

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7935
Size: 324 Color: 5355
Size: 280 Color: 2959

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6449
Size: 350 Color: 6443
Size: 300 Color: 4197

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6440
Size: 350 Color: 6436
Size: 301 Color: 4206

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6435
Size: 350 Color: 6433
Size: 301 Color: 4235

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6441
Size: 350 Color: 6439
Size: 301 Color: 4247

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6434
Size: 350 Color: 6423
Size: 301 Color: 4248

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6428
Size: 350 Color: 6424
Size: 301 Color: 4217

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6422
Size: 350 Color: 6421
Size: 301 Color: 4210

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6426
Size: 350 Color: 6425
Size: 301 Color: 4239

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6418
Size: 350 Color: 6417
Size: 301 Color: 4265

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7172
Size: 326 Color: 5461
Size: 304 Color: 4401

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7156
Size: 315 Color: 4941
Size: 315 Color: 4918

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7146
Size: 318 Color: 5070
Size: 312 Color: 4825

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7157
Size: 320 Color: 5196
Size: 310 Color: 4700

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7164
Size: 316 Color: 4957
Size: 314 Color: 4873

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7170
Size: 319 Color: 5139
Size: 311 Color: 4752

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7143
Size: 316 Color: 4990
Size: 314 Color: 4914

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7203
Size: 320 Color: 5154
Size: 309 Color: 4639

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7198
Size: 327 Color: 5517
Size: 302 Color: 4321

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7190
Size: 316 Color: 4967
Size: 313 Color: 4868

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7205
Size: 322 Color: 5286
Size: 307 Color: 4538

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7199
Size: 328 Color: 5552
Size: 301 Color: 4224

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7197
Size: 327 Color: 5498
Size: 302 Color: 4310

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7181
Size: 321 Color: 5226
Size: 308 Color: 4603

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7202
Size: 323 Color: 5299
Size: 306 Color: 4469

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7210
Size: 328 Color: 5553
Size: 301 Color: 4240

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7211
Size: 315 Color: 4927
Size: 314 Color: 4887

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7183
Size: 318 Color: 5077
Size: 311 Color: 4776

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7185
Size: 320 Color: 5195
Size: 309 Color: 4625

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7184
Size: 317 Color: 5022
Size: 312 Color: 4784

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7225
Size: 349 Color: 6377
Size: 279 Color: 2877

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7239
Size: 349 Color: 6403
Size: 279 Color: 2873

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7242
Size: 349 Color: 6371
Size: 279 Color: 2866

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7228
Size: 349 Color: 6385
Size: 279 Color: 2886

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7243
Size: 349 Color: 6374
Size: 279 Color: 2849

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7217
Size: 349 Color: 6383
Size: 279 Color: 2896

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7223
Size: 349 Color: 6386
Size: 279 Color: 2862

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7244
Size: 349 Color: 6392
Size: 279 Color: 2861

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7235
Size: 349 Color: 6389
Size: 279 Color: 2860

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7233
Size: 349 Color: 6399
Size: 279 Color: 2856

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7214
Size: 349 Color: 6391
Size: 279 Color: 2841

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7216
Size: 349 Color: 6404
Size: 279 Color: 2870

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7241
Size: 349 Color: 6395
Size: 279 Color: 2850

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7261
Size: 323 Color: 5338
Size: 304 Color: 4376

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7269
Size: 325 Color: 5422
Size: 302 Color: 4297

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7255
Size: 315 Color: 4937
Size: 312 Color: 4810

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7275
Size: 326 Color: 5453
Size: 301 Color: 4254

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7249
Size: 328 Color: 5562
Size: 299 Color: 4141

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7257
Size: 315 Color: 4944
Size: 312 Color: 4812

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7270
Size: 322 Color: 5270
Size: 305 Color: 4430

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7262
Size: 314 Color: 4881
Size: 313 Color: 4837

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7259
Size: 319 Color: 5132
Size: 308 Color: 4561

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7263
Size: 349 Color: 6398
Size: 278 Color: 2827

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7247
Size: 349 Color: 6372
Size: 278 Color: 2791

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7268
Size: 349 Color: 6402
Size: 278 Color: 2831

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7264
Size: 349 Color: 6394
Size: 278 Color: 2784

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7272
Size: 349 Color: 6393
Size: 278 Color: 2789

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7267
Size: 349 Color: 6381
Size: 278 Color: 2777

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7302
Size: 321 Color: 5230
Size: 305 Color: 4431

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7278
Size: 329 Color: 5583
Size: 297 Color: 4015

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7277
Size: 319 Color: 5148
Size: 307 Color: 4505

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7303
Size: 326 Color: 5454
Size: 300 Color: 4153

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7307
Size: 322 Color: 5287
Size: 304 Color: 4379

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7305
Size: 319 Color: 5133
Size: 307 Color: 4511

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7293
Size: 324 Color: 5350
Size: 302 Color: 4274

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7297
Size: 324 Color: 5383
Size: 302 Color: 4279

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7308
Size: 317 Color: 5007
Size: 309 Color: 4615

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7306
Size: 327 Color: 5510
Size: 299 Color: 4123

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7282
Size: 317 Color: 5041
Size: 309 Color: 4645

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7280
Size: 349 Color: 6397
Size: 277 Color: 2735

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7301
Size: 349 Color: 6409
Size: 277 Color: 2744

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7339
Size: 326 Color: 5458
Size: 299 Color: 4119

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7315
Size: 348 Color: 6333
Size: 277 Color: 2734

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7317
Size: 348 Color: 6366
Size: 277 Color: 2731

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7368
Size: 372 Color: 7189
Size: 251 Color: 266

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7311
Size: 348 Color: 6365
Size: 277 Color: 2762

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7327
Size: 348 Color: 6355
Size: 277 Color: 2724

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7332
Size: 348 Color: 6338
Size: 277 Color: 2758

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7313
Size: 348 Color: 6367
Size: 277 Color: 2726

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7330
Size: 348 Color: 6349
Size: 277 Color: 2742

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7320
Size: 348 Color: 6336
Size: 277 Color: 2729

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7341
Size: 348 Color: 6346
Size: 277 Color: 2730

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7342
Size: 348 Color: 6359
Size: 277 Color: 2697

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7309
Size: 348 Color: 6335
Size: 277 Color: 2753

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7333
Size: 347 Color: 6314
Size: 278 Color: 2779

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7338
Size: 348 Color: 6360
Size: 277 Color: 2766

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7335
Size: 348 Color: 6358
Size: 277 Color: 2720

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7322
Size: 348 Color: 6361
Size: 277 Color: 2745

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7345
Size: 348 Color: 6351
Size: 277 Color: 2696

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7321
Size: 348 Color: 6356
Size: 277 Color: 2771

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7310
Size: 348 Color: 6362
Size: 277 Color: 2706

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7331
Size: 348 Color: 6341
Size: 277 Color: 2715

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7318
Size: 348 Color: 6334
Size: 277 Color: 2711

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7334
Size: 348 Color: 6344
Size: 277 Color: 2767

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7336
Size: 348 Color: 6343
Size: 277 Color: 2727

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7360
Size: 321 Color: 5246
Size: 303 Color: 4348

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7182
Size: 323 Color: 5306
Size: 306 Color: 4488

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7348
Size: 318 Color: 5089
Size: 306 Color: 4485

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7350
Size: 326 Color: 5438
Size: 298 Color: 4072

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7356
Size: 320 Color: 5179
Size: 304 Color: 4375

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7366
Size: 323 Color: 5330
Size: 301 Color: 4259

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9931
Size: 253 Color: 518
Size: 251 Color: 230

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7352
Size: 328 Color: 5537
Size: 296 Color: 3929

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7351
Size: 328 Color: 5557
Size: 296 Color: 3961

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7364
Size: 317 Color: 5030
Size: 307 Color: 4516

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7945
Size: 317 Color: 5036
Size: 286 Color: 3317

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7355
Size: 328 Color: 5563
Size: 296 Color: 3950

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7048
Size: 323 Color: 5328
Size: 310 Color: 4680

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7357
Size: 316 Color: 4970
Size: 308 Color: 4564

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7349
Size: 348 Color: 6357
Size: 276 Color: 2654

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7382
Size: 328 Color: 5568
Size: 295 Color: 3875

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7371
Size: 325 Color: 5428
Size: 298 Color: 4060

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7387
Size: 347 Color: 6323
Size: 276 Color: 2648

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7372
Size: 347 Color: 6318
Size: 276 Color: 2642

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7390
Size: 347 Color: 6329
Size: 276 Color: 2632

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7380
Size: 347 Color: 6300
Size: 276 Color: 2681

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7376
Size: 347 Color: 6319
Size: 276 Color: 2656

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7391
Size: 347 Color: 6308
Size: 276 Color: 2670

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7388
Size: 347 Color: 6330
Size: 276 Color: 2672

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7369
Size: 347 Color: 6309
Size: 276 Color: 2675

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7404
Size: 313 Color: 4841
Size: 309 Color: 4665

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7406
Size: 315 Color: 4947
Size: 307 Color: 4555

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7393
Size: 313 Color: 4850
Size: 309 Color: 4656

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7414
Size: 321 Color: 5237
Size: 301 Color: 4263

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7410
Size: 324 Color: 5385
Size: 298 Color: 4063

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7412
Size: 323 Color: 5309
Size: 299 Color: 4114

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7420
Size: 321 Color: 5210
Size: 301 Color: 4233

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7423
Size: 347 Color: 6303
Size: 275 Color: 2604

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7422
Size: 347 Color: 6328
Size: 275 Color: 2567

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7394
Size: 347 Color: 6324
Size: 275 Color: 2554

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7424
Size: 347 Color: 6297
Size: 275 Color: 2560

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7418
Size: 347 Color: 6313
Size: 275 Color: 2572

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7415
Size: 347 Color: 6320
Size: 275 Color: 2556

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7396
Size: 347 Color: 6307
Size: 275 Color: 2593

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7421
Size: 347 Color: 6325
Size: 275 Color: 2552

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7425
Size: 347 Color: 6317
Size: 275 Color: 2550

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7437
Size: 346 Color: 6251
Size: 275 Color: 2559

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7445
Size: 346 Color: 6278
Size: 275 Color: 2584

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7443
Size: 346 Color: 6259
Size: 275 Color: 2544

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7436
Size: 346 Color: 6276
Size: 275 Color: 2541

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7447
Size: 346 Color: 6249
Size: 275 Color: 2569

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7452
Size: 346 Color: 6265
Size: 275 Color: 2577

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7449
Size: 346 Color: 6264
Size: 275 Color: 2578

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7429
Size: 347 Color: 6321
Size: 274 Color: 2459

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7448
Size: 347 Color: 6306
Size: 274 Color: 2458

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7441
Size: 347 Color: 6301
Size: 274 Color: 2501

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7438
Size: 347 Color: 6327
Size: 274 Color: 2524

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7434
Size: 347 Color: 6312
Size: 274 Color: 2472

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7462
Size: 346 Color: 6263
Size: 274 Color: 2476

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7892
Size: 314 Color: 4877
Size: 291 Color: 3661

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7466
Size: 346 Color: 6282
Size: 274 Color: 2520

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9494
Size: 274 Color: 2474
Size: 259 Color: 1205

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7460
Size: 346 Color: 6287
Size: 274 Color: 2485

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7465
Size: 346 Color: 6268
Size: 274 Color: 2521

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7467
Size: 346 Color: 6275
Size: 274 Color: 2473

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7478
Size: 321 Color: 5205
Size: 298 Color: 4057

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7502
Size: 320 Color: 5186
Size: 299 Color: 4117

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7496
Size: 321 Color: 5241
Size: 298 Color: 4073

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7481
Size: 311 Color: 4751
Size: 308 Color: 4605

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7500
Size: 324 Color: 5357
Size: 295 Color: 3895

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7489
Size: 321 Color: 5240
Size: 298 Color: 4062

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7488
Size: 310 Color: 4717
Size: 309 Color: 4649

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7499
Size: 317 Color: 5019
Size: 302 Color: 4275

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7486
Size: 345 Color: 6210
Size: 274 Color: 2528

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7487
Size: 345 Color: 6226
Size: 274 Color: 2449

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7495
Size: 345 Color: 6243
Size: 274 Color: 2505

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7484
Size: 345 Color: 6227
Size: 274 Color: 2445

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7490
Size: 346 Color: 6255
Size: 273 Color: 2433

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7501
Size: 346 Color: 6291
Size: 273 Color: 2435

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7479
Size: 346 Color: 6266
Size: 273 Color: 2365

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7513
Size: 345 Color: 6213
Size: 273 Color: 2417

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7508
Size: 345 Color: 6242
Size: 273 Color: 2396

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7529
Size: 345 Color: 6235
Size: 273 Color: 2387

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9429
Size: 270 Color: 2173
Size: 267 Color: 1945

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7518
Size: 345 Color: 6232
Size: 273 Color: 2385

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7519
Size: 345 Color: 6233
Size: 273 Color: 2397

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7523
Size: 345 Color: 6212
Size: 273 Color: 2420

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7504
Size: 345 Color: 6236
Size: 273 Color: 2391

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7514
Size: 345 Color: 6219
Size: 273 Color: 2368

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7520
Size: 345 Color: 6211
Size: 273 Color: 2367

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7505
Size: 345 Color: 6244
Size: 273 Color: 2412

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7524
Size: 345 Color: 6231
Size: 273 Color: 2378

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7538
Size: 319 Color: 5128
Size: 298 Color: 4070

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7533
Size: 328 Color: 5569
Size: 289 Color: 3542

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7532
Size: 322 Color: 5263
Size: 295 Color: 3892

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7542
Size: 310 Color: 4710
Size: 307 Color: 4550

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7539
Size: 318 Color: 5081
Size: 299 Color: 4130

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7553
Size: 309 Color: 4648
Size: 308 Color: 4579

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7545
Size: 316 Color: 4991
Size: 301 Color: 4264

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7549
Size: 322 Color: 5269
Size: 295 Color: 3900

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7537
Size: 313 Color: 4849
Size: 304 Color: 4370

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7555
Size: 327 Color: 5492
Size: 290 Color: 3595

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7543
Size: 319 Color: 5136
Size: 298 Color: 4039

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7536
Size: 312 Color: 4786
Size: 305 Color: 4448

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7548
Size: 345 Color: 6239
Size: 272 Color: 2347

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7582
Size: 323 Color: 5336
Size: 293 Color: 3776

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7583
Size: 310 Color: 4708
Size: 306 Color: 4461

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7557
Size: 311 Color: 4741
Size: 305 Color: 4450

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7559
Size: 344 Color: 6172
Size: 272 Color: 2309

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7562
Size: 344 Color: 6175
Size: 272 Color: 2343

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7578
Size: 344 Color: 6182
Size: 272 Color: 2313

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7570
Size: 344 Color: 6187
Size: 272 Color: 2312

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7558
Size: 344 Color: 6180
Size: 272 Color: 2321

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7569
Size: 344 Color: 6190
Size: 272 Color: 2317

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7576
Size: 344 Color: 6188
Size: 272 Color: 2279

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7574
Size: 344 Color: 6185
Size: 272 Color: 2293

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7560
Size: 344 Color: 6193
Size: 272 Color: 2294

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7571
Size: 344 Color: 6176
Size: 272 Color: 2327

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8690
Size: 298 Color: 4080
Size: 276 Color: 2644

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7600
Size: 317 Color: 5020
Size: 298 Color: 4071

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7602
Size: 317 Color: 5056
Size: 298 Color: 4040

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7590
Size: 312 Color: 4792
Size: 303 Color: 4357

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7592
Size: 327 Color: 5519
Size: 288 Color: 3459

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7607
Size: 311 Color: 4758
Size: 304 Color: 4400

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9828
Size: 261 Color: 1348
Size: 250 Color: 30

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7597
Size: 344 Color: 6198
Size: 271 Color: 2226

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7588
Size: 344 Color: 6199
Size: 271 Color: 2231

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7596
Size: 344 Color: 6178
Size: 271 Color: 2216

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7585
Size: 344 Color: 6202
Size: 271 Color: 2244

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9655
Size: 264 Color: 1639
Size: 260 Color: 1311

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7601
Size: 344 Color: 6206
Size: 271 Color: 2261

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7587
Size: 344 Color: 6208
Size: 271 Color: 2240

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7606
Size: 344 Color: 6205
Size: 271 Color: 2214

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7638
Size: 312 Color: 4820
Size: 302 Color: 4292

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7636
Size: 329 Color: 5577
Size: 285 Color: 3287

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7624
Size: 329 Color: 5594
Size: 285 Color: 3311

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7639
Size: 327 Color: 5521
Size: 287 Color: 3400

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7622
Size: 308 Color: 4592
Size: 306 Color: 4496

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7635
Size: 311 Color: 4771
Size: 303 Color: 4352

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7614
Size: 326 Color: 5477
Size: 288 Color: 3483

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7616
Size: 309 Color: 4642
Size: 305 Color: 4416

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7619
Size: 325 Color: 5423
Size: 289 Color: 3556

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7620
Size: 318 Color: 5103
Size: 296 Color: 3949

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7613
Size: 311 Color: 4739
Size: 303 Color: 4328

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7610
Size: 320 Color: 5163
Size: 294 Color: 3834

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7630
Size: 309 Color: 4622
Size: 305 Color: 4453

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7629
Size: 321 Color: 5222
Size: 293 Color: 3762

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7631
Size: 343 Color: 6155
Size: 271 Color: 2260

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7667
Size: 320 Color: 5181
Size: 293 Color: 3742

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7659
Size: 320 Color: 5189
Size: 293 Color: 3781

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7642
Size: 324 Color: 5389
Size: 289 Color: 3565

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7653
Size: 309 Color: 4670
Size: 304 Color: 4382

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7644
Size: 343 Color: 6170
Size: 270 Color: 2154

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7664
Size: 343 Color: 6171
Size: 270 Color: 2153

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7663
Size: 343 Color: 6140
Size: 270 Color: 2162

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7652
Size: 343 Color: 6142
Size: 270 Color: 2209

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9801
Size: 258 Color: 1108
Size: 256 Color: 925

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7649
Size: 343 Color: 6146
Size: 270 Color: 2142

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7640
Size: 343 Color: 6166
Size: 270 Color: 2192

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7656
Size: 343 Color: 6158
Size: 270 Color: 2186

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7662
Size: 343 Color: 6124
Size: 270 Color: 2208

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7646
Size: 343 Color: 6139
Size: 270 Color: 2199

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7661
Size: 343 Color: 6169
Size: 270 Color: 2145

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7650
Size: 343 Color: 6123
Size: 270 Color: 2143

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7655
Size: 343 Color: 6143
Size: 270 Color: 2149

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7675
Size: 326 Color: 5474
Size: 286 Color: 3356

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7673
Size: 320 Color: 5165
Size: 292 Color: 3693

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7678
Size: 322 Color: 5283
Size: 290 Color: 3583

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7685
Size: 343 Color: 6127
Size: 269 Color: 2097

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7682
Size: 343 Color: 6151
Size: 269 Color: 2111

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7669
Size: 343 Color: 6154
Size: 269 Color: 2133

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7692
Size: 343 Color: 6128
Size: 269 Color: 2092

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7680
Size: 343 Color: 6156
Size: 269 Color: 2062

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7677
Size: 343 Color: 6121
Size: 269 Color: 2107

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7674
Size: 343 Color: 6162
Size: 269 Color: 2114

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7684
Size: 343 Color: 6160
Size: 269 Color: 2071

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7722
Size: 311 Color: 4755
Size: 300 Color: 4170

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7696
Size: 308 Color: 4573
Size: 303 Color: 4361

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7721
Size: 307 Color: 4530
Size: 304 Color: 4403

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7702
Size: 315 Color: 4916
Size: 296 Color: 3939

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7718
Size: 322 Color: 5261
Size: 289 Color: 3510

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7720
Size: 324 Color: 5390
Size: 287 Color: 3409

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7715
Size: 317 Color: 5057
Size: 294 Color: 3818

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7695
Size: 309 Color: 4623
Size: 302 Color: 4272

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7723
Size: 326 Color: 5437
Size: 285 Color: 3254

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7716
Size: 314 Color: 4889
Size: 297 Color: 3995

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7706
Size: 312 Color: 4789
Size: 299 Color: 4137

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7710
Size: 310 Color: 4724
Size: 301 Color: 4226

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7709
Size: 342 Color: 6113
Size: 269 Color: 2102

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7724
Size: 343 Color: 6135
Size: 268 Color: 2020

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7714
Size: 343 Color: 6134
Size: 268 Color: 1973

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7698
Size: 343 Color: 6138
Size: 268 Color: 1996

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7717
Size: 343 Color: 6125
Size: 268 Color: 2026

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7750
Size: 342 Color: 6116
Size: 268 Color: 2010

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7741
Size: 342 Color: 6104
Size: 268 Color: 2003

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7753
Size: 342 Color: 6095
Size: 268 Color: 2044

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7754
Size: 342 Color: 6109
Size: 268 Color: 2011

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7743
Size: 342 Color: 6097
Size: 268 Color: 1978

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7732
Size: 342 Color: 6110
Size: 268 Color: 2055

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7767
Size: 321 Color: 5216
Size: 288 Color: 3471

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7776
Size: 315 Color: 4939
Size: 294 Color: 3814

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7774
Size: 311 Color: 4744
Size: 298 Color: 4058

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7769
Size: 313 Color: 4828
Size: 296 Color: 3963

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7785
Size: 319 Color: 5138
Size: 290 Color: 3584

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7772
Size: 317 Color: 5059
Size: 292 Color: 3715

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7760
Size: 322 Color: 5252
Size: 287 Color: 3367

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7766
Size: 320 Color: 5191
Size: 289 Color: 3535

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7768
Size: 341 Color: 6051
Size: 268 Color: 2022

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7771
Size: 341 Color: 6041
Size: 268 Color: 2002

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7773
Size: 341 Color: 6071
Size: 268 Color: 1963

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7775
Size: 341 Color: 6075
Size: 268 Color: 2052

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7783
Size: 341 Color: 6067
Size: 268 Color: 2018

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7764
Size: 341 Color: 6040
Size: 268 Color: 2036

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7763
Size: 341 Color: 6069
Size: 268 Color: 1984

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7818
Size: 341 Color: 6053
Size: 267 Color: 1912

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7742
Size: 319 Color: 5134
Size: 291 Color: 3646

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7820
Size: 341 Color: 6059
Size: 267 Color: 1849

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7796
Size: 341 Color: 6057
Size: 267 Color: 1935

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7814
Size: 341 Color: 6082
Size: 267 Color: 1938

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7821
Size: 341 Color: 6074
Size: 267 Color: 1884

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7812
Size: 341 Color: 6052
Size: 267 Color: 1928

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7793
Size: 341 Color: 6073
Size: 267 Color: 1940

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7800
Size: 341 Color: 6043
Size: 267 Color: 1880

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7816
Size: 341 Color: 6054
Size: 267 Color: 1867

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7811
Size: 341 Color: 6083
Size: 267 Color: 1877

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7803
Size: 341 Color: 6049
Size: 267 Color: 1862

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7791
Size: 341 Color: 6080
Size: 267 Color: 1850

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7801
Size: 341 Color: 6081
Size: 267 Color: 1941

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7789
Size: 341 Color: 6077
Size: 267 Color: 1930

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7786
Size: 341 Color: 6058
Size: 267 Color: 1881

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7807
Size: 341 Color: 6063
Size: 267 Color: 1914

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7850
Size: 318 Color: 5109
Size: 289 Color: 3525

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7845
Size: 308 Color: 4596
Size: 299 Color: 4145

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7823
Size: 314 Color: 4893
Size: 293 Color: 3785

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7834
Size: 307 Color: 4506
Size: 300 Color: 4184

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7835
Size: 326 Color: 5473
Size: 281 Color: 3006

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7855
Size: 313 Color: 4852
Size: 294 Color: 3827

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7844
Size: 307 Color: 4548
Size: 300 Color: 4195

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7841
Size: 317 Color: 5033
Size: 290 Color: 3608

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7837
Size: 320 Color: 5192
Size: 287 Color: 3378

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7836
Size: 328 Color: 5576
Size: 279 Color: 2871

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7846
Size: 318 Color: 5093
Size: 289 Color: 3505

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7863
Size: 308 Color: 4599
Size: 298 Color: 4090

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9920
Size: 253 Color: 569
Size: 252 Color: 433

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7874
Size: 319 Color: 5111
Size: 287 Color: 3417

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7878
Size: 311 Color: 4754
Size: 295 Color: 3890

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7864
Size: 329 Color: 5608
Size: 277 Color: 2710

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7875
Size: 324 Color: 5369
Size: 282 Color: 3057

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7872
Size: 303 Color: 4340
Size: 303 Color: 4330

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7862
Size: 323 Color: 5304
Size: 283 Color: 3142

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7427
Size: 346 Color: 6273
Size: 276 Color: 2663

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7870
Size: 310 Color: 4691
Size: 296 Color: 3935

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7856
Size: 317 Color: 5039
Size: 289 Color: 3559

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7865
Size: 309 Color: 4667
Size: 297 Color: 4028

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7877
Size: 303 Color: 4347
Size: 303 Color: 4341

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7910
Size: 317 Color: 5005
Size: 288 Color: 3477

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7895
Size: 307 Color: 4513
Size: 298 Color: 4053

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7882
Size: 317 Color: 5021
Size: 288 Color: 3482

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7904
Size: 311 Color: 4770
Size: 294 Color: 3833

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7891
Size: 307 Color: 4549
Size: 298 Color: 4098

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7906
Size: 312 Color: 4803
Size: 293 Color: 3768

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7902
Size: 309 Color: 4632
Size: 296 Color: 3959

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7899
Size: 322 Color: 5258
Size: 283 Color: 3116

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7890
Size: 312 Color: 4815
Size: 293 Color: 3743

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8672
Size: 291 Color: 3627
Size: 284 Color: 3200

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7888
Size: 327 Color: 5489
Size: 278 Color: 2802

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7919
Size: 308 Color: 4566
Size: 296 Color: 3943

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7929
Size: 322 Color: 5264
Size: 282 Color: 3048

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7927
Size: 308 Color: 4604
Size: 296 Color: 3957

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7934
Size: 303 Color: 4365
Size: 301 Color: 4262

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7931
Size: 326 Color: 5482
Size: 278 Color: 2806

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7912
Size: 317 Color: 5018
Size: 287 Color: 3362

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8116
Size: 340 Color: 5998
Size: 256 Color: 881

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8146
Size: 340 Color: 6005
Size: 255 Color: 726

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8161
Size: 340 Color: 6004
Size: 255 Color: 834

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8162
Size: 340 Color: 6023
Size: 255 Color: 775

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8183
Size: 340 Color: 6015
Size: 254 Color: 663

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8236
Size: 340 Color: 6001
Size: 252 Color: 390

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8246
Size: 332 Color: 5710
Size: 260 Color: 1275

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8221
Size: 340 Color: 6013
Size: 252 Color: 441

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8241
Size: 340 Color: 6007
Size: 252 Color: 358

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8230
Size: 340 Color: 6016
Size: 252 Color: 431

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8228
Size: 340 Color: 5999
Size: 252 Color: 381

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8235
Size: 340 Color: 6011
Size: 252 Color: 418

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8257
Size: 340 Color: 6022
Size: 251 Color: 238

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8262
Size: 340 Color: 6030
Size: 251 Color: 296

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8272
Size: 339 Color: 5966
Size: 251 Color: 309

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8287
Size: 339 Color: 5973
Size: 251 Color: 245

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8282
Size: 339 Color: 5994
Size: 251 Color: 199

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8293
Size: 340 Color: 6032
Size: 250 Color: 72

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8270
Size: 340 Color: 6033
Size: 250 Color: 178

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8268
Size: 340 Color: 6038
Size: 250 Color: 62

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8328
Size: 339 Color: 5982
Size: 250 Color: 89

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8327
Size: 339 Color: 5983
Size: 250 Color: 107

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8317
Size: 339 Color: 5975
Size: 250 Color: 184

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8312
Size: 339 Color: 5970
Size: 250 Color: 80

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8324
Size: 322 Color: 5268
Size: 267 Color: 1919

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8315
Size: 317 Color: 5061
Size: 272 Color: 2345

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8295
Size: 325 Color: 5413
Size: 264 Color: 1582

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8304
Size: 326 Color: 5442
Size: 263 Color: 1524

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8303
Size: 308 Color: 4602
Size: 281 Color: 3035

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8340
Size: 325 Color: 5425
Size: 263 Color: 1508

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8342
Size: 322 Color: 5250
Size: 266 Color: 1782

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8335
Size: 336 Color: 5845
Size: 252 Color: 440

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 6339
Size: 338 Color: 5948
Size: 315 Color: 4936

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8352
Size: 319 Color: 5140
Size: 269 Color: 2079

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8332
Size: 300 Color: 4200
Size: 288 Color: 3501

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8338
Size: 327 Color: 5523
Size: 261 Color: 1341

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8356
Size: 332 Color: 5720
Size: 256 Color: 921

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8350
Size: 327 Color: 5526
Size: 261 Color: 1401

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8355
Size: 326 Color: 5481
Size: 262 Color: 1434

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8341
Size: 327 Color: 5488
Size: 261 Color: 1388

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8339
Size: 324 Color: 5375
Size: 264 Color: 1630

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8357
Size: 305 Color: 4442
Size: 283 Color: 3120

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8381
Size: 303 Color: 4342
Size: 284 Color: 3192

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8365
Size: 303 Color: 4358
Size: 284 Color: 3231

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8372
Size: 323 Color: 5329
Size: 264 Color: 1605

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8366
Size: 307 Color: 4525
Size: 280 Color: 2935

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8373
Size: 303 Color: 4350
Size: 284 Color: 3216

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8384
Size: 308 Color: 4567
Size: 279 Color: 2882

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8360
Size: 313 Color: 4832
Size: 274 Color: 2504

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9536
Size: 266 Color: 1803
Size: 264 Color: 1578

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8371
Size: 314 Color: 4899
Size: 273 Color: 2404

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8377
Size: 327 Color: 5512
Size: 260 Color: 1297

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8361
Size: 302 Color: 4308
Size: 285 Color: 3276

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8364
Size: 294 Color: 3859
Size: 293 Color: 3808

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8370
Size: 304 Color: 4381
Size: 283 Color: 3157

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8363
Size: 329 Color: 5619
Size: 258 Color: 1126

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8369
Size: 324 Color: 5388
Size: 263 Color: 1562

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8376
Size: 325 Color: 5433
Size: 262 Color: 1451

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8359
Size: 320 Color: 5166
Size: 267 Color: 1890

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8399
Size: 310 Color: 4723
Size: 276 Color: 2619

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8394
Size: 332 Color: 5707
Size: 254 Color: 653

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8401
Size: 301 Color: 4218
Size: 285 Color: 3277

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8414
Size: 329 Color: 5612
Size: 257 Color: 1029

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8405
Size: 297 Color: 4004
Size: 289 Color: 3553

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8416
Size: 318 Color: 5086
Size: 268 Color: 2021

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8395
Size: 312 Color: 4797
Size: 274 Color: 2518

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8413
Size: 320 Color: 5185
Size: 266 Color: 1783

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8393
Size: 300 Color: 4173
Size: 286 Color: 3335

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8403
Size: 332 Color: 5715
Size: 254 Color: 655

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8410
Size: 298 Color: 4082
Size: 288 Color: 3478

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8415
Size: 308 Color: 4582
Size: 278 Color: 2825

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8417
Size: 329 Color: 5603
Size: 257 Color: 1022

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8408
Size: 311 Color: 4772
Size: 275 Color: 2594

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8436
Size: 332 Color: 5728
Size: 253 Color: 501

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8419
Size: 335 Color: 5831
Size: 250 Color: 47

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8429
Size: 305 Color: 4423
Size: 280 Color: 2952

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8420
Size: 316 Color: 4956
Size: 269 Color: 2095

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8440
Size: 300 Color: 4160
Size: 285 Color: 3284

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8435
Size: 325 Color: 5415
Size: 260 Color: 1321

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8439
Size: 305 Color: 4452
Size: 280 Color: 2947

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8424
Size: 305 Color: 4433
Size: 280 Color: 2912

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8464
Size: 294 Color: 3845
Size: 290 Color: 3605

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8459
Size: 332 Color: 5711
Size: 252 Color: 385

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8443
Size: 325 Color: 5414
Size: 259 Color: 1157

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8460
Size: 314 Color: 4909
Size: 270 Color: 2139

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8444
Size: 333 Color: 5754
Size: 251 Color: 244

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8447
Size: 302 Color: 4318
Size: 282 Color: 3094

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8448
Size: 294 Color: 3843
Size: 290 Color: 3588

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8455
Size: 319 Color: 5144
Size: 265 Color: 1665

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8275
Size: 338 Color: 5962
Size: 252 Color: 407

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8452
Size: 316 Color: 5000
Size: 268 Color: 1981

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8449
Size: 297 Color: 3990
Size: 287 Color: 3399

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8454
Size: 293 Color: 3769
Size: 291 Color: 3658

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8456
Size: 293 Color: 3775
Size: 291 Color: 3678

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8481
Size: 293 Color: 3773
Size: 290 Color: 3624

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8491
Size: 293 Color: 3761
Size: 290 Color: 3578

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8466
Size: 305 Color: 4424
Size: 278 Color: 2810

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8482
Size: 302 Color: 4325
Size: 281 Color: 3003

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8473
Size: 299 Color: 4112
Size: 284 Color: 3188

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8493
Size: 312 Color: 4819
Size: 271 Color: 2258

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8469
Size: 304 Color: 4372
Size: 279 Color: 2844

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8485
Size: 304 Color: 4396
Size: 279 Color: 2847

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8492
Size: 312 Color: 4795
Size: 271 Color: 2255

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8479
Size: 294 Color: 3855
Size: 289 Color: 3534

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8508
Size: 328 Color: 5571
Size: 254 Color: 621

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8501
Size: 296 Color: 3968
Size: 286 Color: 3320

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8507
Size: 326 Color: 5480
Size: 256 Color: 923

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8520
Size: 301 Color: 4236
Size: 281 Color: 3015

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8509
Size: 311 Color: 4765
Size: 271 Color: 2221

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8505
Size: 312 Color: 4822
Size: 270 Color: 2148

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8500
Size: 296 Color: 3979
Size: 286 Color: 3355

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8535
Size: 326 Color: 5447
Size: 255 Color: 804

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8528
Size: 292 Color: 3728
Size: 289 Color: 3557

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8532
Size: 317 Color: 5050
Size: 264 Color: 1637

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8537
Size: 321 Color: 5208
Size: 260 Color: 1287

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8543
Size: 325 Color: 5432
Size: 256 Color: 930

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8531
Size: 324 Color: 5373
Size: 257 Color: 971

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8525
Size: 298 Color: 4096
Size: 283 Color: 3151

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8567
Size: 295 Color: 3918
Size: 285 Color: 3262

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8551
Size: 299 Color: 4109
Size: 281 Color: 2982

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8561
Size: 307 Color: 4552
Size: 273 Color: 2393

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8490
Size: 316 Color: 4958
Size: 267 Color: 1868

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8546
Size: 295 Color: 3885
Size: 285 Color: 3292

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8556
Size: 313 Color: 4860
Size: 267 Color: 1891

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8566
Size: 329 Color: 5589
Size: 251 Color: 299

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8559
Size: 319 Color: 5145
Size: 261 Color: 1399

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8549
Size: 313 Color: 4864
Size: 267 Color: 1908

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8550
Size: 312 Color: 4821
Size: 268 Color: 2007

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8564
Size: 321 Color: 5225
Size: 259 Color: 1216

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8547
Size: 319 Color: 5146
Size: 261 Color: 1376

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8555
Size: 311 Color: 4743
Size: 269 Color: 2103

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8584
Size: 290 Color: 3601
Size: 289 Color: 3522

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8572
Size: 324 Color: 5384
Size: 255 Color: 826

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8575
Size: 309 Color: 4644
Size: 270 Color: 2165

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8603
Size: 300 Color: 4167
Size: 278 Color: 2837

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8598
Size: 296 Color: 3934
Size: 282 Color: 3082

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8612
Size: 295 Color: 3884
Size: 283 Color: 3132

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8608
Size: 326 Color: 5451
Size: 252 Color: 429

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8329
Size: 335 Color: 5835
Size: 254 Color: 622

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8595
Size: 323 Color: 5344
Size: 255 Color: 819

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8615
Size: 290 Color: 3572
Size: 287 Color: 3382

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8632
Size: 289 Color: 3546
Size: 288 Color: 3467

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8663
Size: 291 Color: 3651
Size: 285 Color: 3269

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8642
Size: 324 Color: 5395
Size: 252 Color: 340

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8652
Size: 326 Color: 5476
Size: 250 Color: 83

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8659
Size: 311 Color: 4774
Size: 265 Color: 1748

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8651
Size: 316 Color: 4955
Size: 260 Color: 1335

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8638
Size: 326 Color: 5462
Size: 250 Color: 134

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8655
Size: 326 Color: 5439
Size: 250 Color: 13

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8653
Size: 294 Color: 3817
Size: 282 Color: 3098

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8647
Size: 296 Color: 3944
Size: 280 Color: 2953

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8661
Size: 305 Color: 4460
Size: 271 Color: 2211

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8650
Size: 290 Color: 3596
Size: 286 Color: 3325

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8654
Size: 320 Color: 5199
Size: 256 Color: 931

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8658
Size: 307 Color: 4534
Size: 269 Color: 2116

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8674
Size: 298 Color: 4047
Size: 277 Color: 2723

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9857
Size: 255 Color: 778
Size: 254 Color: 700

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8679
Size: 323 Color: 5348
Size: 252 Color: 425

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9456
Size: 268 Color: 1977
Size: 267 Color: 1859

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8669
Size: 302 Color: 4317
Size: 273 Color: 2388

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9408
Size: 284 Color: 3218
Size: 254 Color: 704

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8681
Size: 319 Color: 5150
Size: 256 Color: 928

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8665
Size: 291 Color: 3681
Size: 284 Color: 3194

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8668
Size: 289 Color: 3539
Size: 286 Color: 3346

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8671
Size: 319 Color: 5120
Size: 256 Color: 895

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8678
Size: 320 Color: 5190
Size: 255 Color: 780

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8695
Size: 290 Color: 3590
Size: 284 Color: 3236

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8688
Size: 291 Color: 3625
Size: 283 Color: 3170

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8689
Size: 318 Color: 5107
Size: 256 Color: 894

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8593
Size: 306 Color: 4463
Size: 272 Color: 2335

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9255
Size: 273 Color: 2428
Size: 273 Color: 2376

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8683
Size: 298 Color: 4086
Size: 276 Color: 2636

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8703
Size: 306 Color: 4482
Size: 267 Color: 1887

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8715
Size: 312 Color: 4813
Size: 261 Color: 1416

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8699
Size: 296 Color: 3962
Size: 277 Color: 2757

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8716
Size: 308 Color: 4571
Size: 265 Color: 1750

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8708
Size: 287 Color: 3407
Size: 286 Color: 3341

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8717
Size: 293 Color: 3763
Size: 280 Color: 2909

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8702
Size: 300 Color: 4183
Size: 273 Color: 2363

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8710
Size: 306 Color: 4476
Size: 267 Color: 1878

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8744
Size: 309 Color: 4675
Size: 263 Color: 1509

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8734
Size: 294 Color: 3824
Size: 278 Color: 2822

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8742
Size: 289 Color: 3564
Size: 283 Color: 3148

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8723
Size: 310 Color: 4702
Size: 262 Color: 1467

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8736
Size: 297 Color: 4024
Size: 275 Color: 2611

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8722
Size: 304 Color: 4378
Size: 268 Color: 2025

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8726
Size: 291 Color: 3656
Size: 281 Color: 2999

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8731
Size: 321 Color: 5248
Size: 251 Color: 225

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8724
Size: 313 Color: 4857
Size: 259 Color: 1232

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8719
Size: 316 Color: 5004
Size: 256 Color: 866

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8747
Size: 295 Color: 3899
Size: 276 Color: 2616

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8753
Size: 308 Color: 4594
Size: 263 Color: 1555

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8754
Size: 302 Color: 4301
Size: 269 Color: 2110

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8748
Size: 305 Color: 4440
Size: 266 Color: 1819

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8756
Size: 304 Color: 4373
Size: 267 Color: 1904

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8761
Size: 318 Color: 5101
Size: 252 Color: 449

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8775
Size: 318 Color: 5065
Size: 252 Color: 454

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8773
Size: 319 Color: 5117
Size: 251 Color: 216

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8768
Size: 314 Color: 4908
Size: 256 Color: 858

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8759
Size: 314 Color: 4910
Size: 256 Color: 913

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8777
Size: 304 Color: 4412
Size: 266 Color: 1795

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8764
Size: 303 Color: 4343
Size: 267 Color: 1936

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8783
Size: 286 Color: 3343
Size: 283 Color: 3109

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8794
Size: 296 Color: 3971
Size: 273 Color: 2419

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8209
Size: 315 Color: 4928
Size: 278 Color: 2833

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8787
Size: 304 Color: 4388
Size: 265 Color: 1692

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8800
Size: 307 Color: 4529
Size: 262 Color: 1476

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8799
Size: 296 Color: 3973
Size: 273 Color: 2359

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8795
Size: 286 Color: 3353
Size: 283 Color: 3160

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8798
Size: 286 Color: 3336
Size: 283 Color: 3125

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8782
Size: 310 Color: 4698
Size: 259 Color: 1242

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8784
Size: 298 Color: 4048
Size: 271 Color: 2217

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8789
Size: 297 Color: 4014
Size: 272 Color: 2292

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8797
Size: 293 Color: 3789
Size: 276 Color: 2688

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8793
Size: 291 Color: 3664
Size: 278 Color: 2814

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8788
Size: 294 Color: 3848
Size: 275 Color: 2592

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8811
Size: 296 Color: 3967
Size: 272 Color: 2281

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8814
Size: 289 Color: 3519
Size: 279 Color: 2903

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8808
Size: 290 Color: 3586
Size: 278 Color: 2823

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8809
Size: 308 Color: 4577
Size: 260 Color: 1281

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8804
Size: 293 Color: 3746
Size: 275 Color: 2530

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8813
Size: 288 Color: 3493
Size: 280 Color: 2945

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8810
Size: 304 Color: 4389
Size: 264 Color: 1625

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8806
Size: 300 Color: 4155
Size: 268 Color: 1955

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8815
Size: 318 Color: 5083
Size: 250 Color: 31

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9787
Size: 262 Color: 1469
Size: 252 Color: 434

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9829
Size: 256 Color: 862
Size: 255 Color: 814

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8829
Size: 296 Color: 3977
Size: 271 Color: 2250

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8833
Size: 306 Color: 4495
Size: 261 Color: 1390

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8835
Size: 301 Color: 4267
Size: 266 Color: 1770

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8825
Size: 287 Color: 3404
Size: 280 Color: 2955

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8838
Size: 300 Color: 4185
Size: 267 Color: 1894

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8826
Size: 298 Color: 4085
Size: 269 Color: 2082

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8842
Size: 284 Color: 3228
Size: 283 Color: 3110

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8822
Size: 291 Color: 3626
Size: 276 Color: 2631

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8831
Size: 290 Color: 3568
Size: 277 Color: 2693

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8848
Size: 287 Color: 3416
Size: 279 Color: 2855

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8852
Size: 294 Color: 3851
Size: 272 Color: 2334

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8859
Size: 286 Color: 3338
Size: 280 Color: 2911

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8856
Size: 310 Color: 4732
Size: 256 Color: 902

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8844
Size: 287 Color: 3387
Size: 279 Color: 2880

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8850
Size: 283 Color: 3173
Size: 283 Color: 3146

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8862
Size: 289 Color: 3527
Size: 276 Color: 2629

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8886
Size: 299 Color: 4111
Size: 265 Color: 1729

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8883
Size: 306 Color: 4479
Size: 258 Color: 1124

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8895
Size: 284 Color: 3232
Size: 280 Color: 2949

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8517
Size: 299 Color: 4116
Size: 283 Color: 3159

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8880
Size: 295 Color: 3878
Size: 269 Color: 2091

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8879
Size: 290 Color: 3576
Size: 274 Color: 2498

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8896
Size: 302 Color: 4311
Size: 262 Color: 1471

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8898
Size: 311 Color: 4763
Size: 253 Color: 580

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8897
Size: 295 Color: 3901
Size: 269 Color: 2115

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8909
Size: 298 Color: 4075
Size: 265 Color: 1751

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8918
Size: 297 Color: 4030
Size: 266 Color: 1843

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8914
Size: 290 Color: 3593
Size: 273 Color: 2377

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8917
Size: 286 Color: 3344
Size: 277 Color: 2728

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8919
Size: 306 Color: 4486
Size: 257 Color: 1037

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8916
Size: 282 Color: 3081
Size: 281 Color: 3021

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8905
Size: 296 Color: 3942
Size: 267 Color: 1870

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8903
Size: 299 Color: 4120
Size: 264 Color: 1657

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8923
Size: 307 Color: 4558
Size: 256 Color: 892

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8931
Size: 309 Color: 4677
Size: 253 Color: 566

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8943
Size: 291 Color: 3687
Size: 271 Color: 2252

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8934
Size: 302 Color: 4322
Size: 260 Color: 1306

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8937
Size: 310 Color: 4720
Size: 252 Color: 402

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8941
Size: 289 Color: 3551
Size: 273 Color: 2421

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8185
Size: 334 Color: 5788
Size: 260 Color: 1264

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8940
Size: 290 Color: 3579
Size: 272 Color: 2289

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8928
Size: 292 Color: 3733
Size: 270 Color: 2172

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8935
Size: 295 Color: 3905
Size: 267 Color: 1906

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8938
Size: 289 Color: 3513
Size: 273 Color: 2410

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8939
Size: 286 Color: 3330
Size: 276 Color: 2649

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8956
Size: 306 Color: 4502
Size: 255 Color: 764

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8954
Size: 282 Color: 3090
Size: 279 Color: 2897

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8948
Size: 295 Color: 3903
Size: 266 Color: 1759

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8966
Size: 303 Color: 4335
Size: 258 Color: 1060

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8965
Size: 297 Color: 4002
Size: 264 Color: 1635

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8964
Size: 284 Color: 3187
Size: 277 Color: 2719

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8955
Size: 292 Color: 3700
Size: 269 Color: 2118

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8968
Size: 282 Color: 3078
Size: 278 Color: 2804

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8985
Size: 288 Color: 3457
Size: 272 Color: 2332

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8986
Size: 284 Color: 3242
Size: 276 Color: 2624

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8967
Size: 293 Color: 3804
Size: 267 Color: 1864

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8984
Size: 290 Color: 3602
Size: 270 Color: 2175

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8978
Size: 302 Color: 4285
Size: 258 Color: 1116

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8980
Size: 293 Color: 3739
Size: 267 Color: 1921

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9310
Size: 286 Color: 3342
Size: 257 Color: 983

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8998
Size: 295 Color: 3896
Size: 264 Color: 1588

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9001
Size: 307 Color: 4514
Size: 252 Color: 370

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8990
Size: 296 Color: 3972
Size: 263 Color: 1536

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9000
Size: 288 Color: 3435
Size: 271 Color: 2253

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8999
Size: 306 Color: 4489
Size: 253 Color: 497

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9008
Size: 298 Color: 4044
Size: 261 Color: 1362

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8992
Size: 295 Color: 3880
Size: 264 Color: 1604

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8991
Size: 287 Color: 3394
Size: 272 Color: 2331

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8993
Size: 286 Color: 3339
Size: 273 Color: 2423

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8996
Size: 294 Color: 3854
Size: 265 Color: 1700

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9012
Size: 283 Color: 3165
Size: 275 Color: 2557

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9013
Size: 291 Color: 3676
Size: 267 Color: 1888

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9022
Size: 304 Color: 4414
Size: 254 Color: 608

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9025
Size: 282 Color: 3083
Size: 276 Color: 2692

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9039
Size: 307 Color: 4559
Size: 250 Color: 122

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9036
Size: 296 Color: 3958
Size: 261 Color: 1395

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9032
Size: 279 Color: 2888
Size: 278 Color: 2795

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9045
Size: 285 Color: 3282
Size: 272 Color: 2315

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9043
Size: 293 Color: 3802
Size: 264 Color: 1596

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9029
Size: 305 Color: 4425
Size: 252 Color: 366

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9030
Size: 293 Color: 3796
Size: 264 Color: 1642

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9033
Size: 293 Color: 3750
Size: 264 Color: 1620

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9041
Size: 307 Color: 4528
Size: 250 Color: 75

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9488
Size: 279 Color: 2892
Size: 254 Color: 701

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9064
Size: 289 Color: 3548
Size: 267 Color: 1886

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9106
Size: 288 Color: 3456
Size: 265 Color: 1758

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9057
Size: 278 Color: 2815
Size: 278 Color: 2794

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9048
Size: 281 Color: 3002
Size: 275 Color: 2610

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9047
Size: 291 Color: 3671
Size: 265 Color: 1753

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9053
Size: 280 Color: 2924
Size: 276 Color: 2684

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9060
Size: 279 Color: 2885
Size: 277 Color: 2716

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9050
Size: 295 Color: 3904
Size: 261 Color: 1374

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9079
Size: 300 Color: 4178
Size: 255 Color: 848

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9072
Size: 299 Color: 4103
Size: 256 Color: 874

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9067
Size: 305 Color: 4426
Size: 250 Color: 110

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9070
Size: 280 Color: 2938
Size: 275 Color: 2561

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7503
Size: 333 Color: 5763
Size: 286 Color: 3321

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9093
Size: 278 Color: 2824
Size: 276 Color: 2618

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9091
Size: 299 Color: 4146
Size: 255 Color: 786

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9095
Size: 287 Color: 3365
Size: 267 Color: 1923

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9097
Size: 304 Color: 4392
Size: 250 Color: 33

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9866
Size: 258 Color: 1082
Size: 250 Color: 173

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9117
Size: 289 Color: 3509
Size: 264 Color: 1627

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9110
Size: 300 Color: 4165
Size: 253 Color: 562

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9103
Size: 286 Color: 3352
Size: 267 Color: 1934

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9113
Size: 281 Color: 2973
Size: 272 Color: 2342

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9118
Size: 282 Color: 3040
Size: 271 Color: 2228

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9115
Size: 282 Color: 3080
Size: 271 Color: 2220

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9119
Size: 286 Color: 3329
Size: 266 Color: 1798

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9127
Size: 276 Color: 2673
Size: 276 Color: 2623

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9122
Size: 289 Color: 3514
Size: 263 Color: 1548

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9129
Size: 282 Color: 3056
Size: 270 Color: 2193

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9128
Size: 300 Color: 4172
Size: 252 Color: 448

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9531
Size: 266 Color: 1772
Size: 265 Color: 1670

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9123
Size: 283 Color: 3124
Size: 269 Color: 2084

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8450
Size: 322 Color: 5284
Size: 262 Color: 1489

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9121
Size: 290 Color: 3607
Size: 262 Color: 1468

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9140
Size: 284 Color: 3212
Size: 267 Color: 1879

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9137
Size: 286 Color: 3340
Size: 265 Color: 1699

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9149
Size: 286 Color: 3348
Size: 265 Color: 1688

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9145
Size: 281 Color: 3026
Size: 270 Color: 2202

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9144
Size: 292 Color: 3713
Size: 259 Color: 1226

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9133
Size: 289 Color: 3531
Size: 262 Color: 1440

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9134
Size: 293 Color: 3752
Size: 258 Color: 1094

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9919
Size: 255 Color: 787
Size: 250 Color: 94

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9150
Size: 285 Color: 3271
Size: 266 Color: 1768

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9148
Size: 297 Color: 4000
Size: 254 Color: 645

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9152
Size: 300 Color: 4182
Size: 251 Color: 218

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9170
Size: 298 Color: 4042
Size: 252 Color: 394

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9172
Size: 284 Color: 3211
Size: 266 Color: 1779

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9153
Size: 278 Color: 2829
Size: 272 Color: 2338

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9167
Size: 283 Color: 3121
Size: 267 Color: 1918

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9158
Size: 291 Color: 3686
Size: 259 Color: 1209

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9164
Size: 292 Color: 3692
Size: 258 Color: 1098

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9165
Size: 276 Color: 2668
Size: 274 Color: 2490

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9175
Size: 294 Color: 3829
Size: 256 Color: 910

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9176
Size: 285 Color: 3304
Size: 265 Color: 1725

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9159
Size: 290 Color: 3621
Size: 260 Color: 1266

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9174
Size: 282 Color: 3046
Size: 268 Color: 1986

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9173
Size: 277 Color: 2775
Size: 273 Color: 2379

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9178
Size: 295 Color: 3894
Size: 254 Color: 668

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9182
Size: 296 Color: 3953
Size: 253 Color: 516

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9192
Size: 283 Color: 3154
Size: 266 Color: 1814

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9189
Size: 293 Color: 3753
Size: 256 Color: 859

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9179
Size: 299 Color: 4113
Size: 250 Color: 29

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9191
Size: 289 Color: 3563
Size: 260 Color: 1313

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9194
Size: 276 Color: 2679
Size: 273 Color: 2361

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9646
Size: 274 Color: 2479
Size: 250 Color: 189

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9200
Size: 285 Color: 3278
Size: 263 Color: 1564

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9214
Size: 293 Color: 3809
Size: 255 Color: 798

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 9974
Size: 251 Color: 214
Size: 250 Color: 1

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9216
Size: 279 Color: 2853
Size: 269 Color: 2064

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9208
Size: 280 Color: 2934
Size: 268 Color: 1959

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9204
Size: 277 Color: 2717
Size: 271 Color: 2218

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9217
Size: 275 Color: 2602
Size: 273 Color: 2427

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9211
Size: 276 Color: 2635
Size: 272 Color: 2303

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8570
Size: 301 Color: 4221
Size: 279 Color: 2858

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9235
Size: 291 Color: 3674
Size: 256 Color: 911

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9230
Size: 283 Color: 3130
Size: 264 Color: 1618

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7886
Size: 311 Color: 4740
Size: 294 Color: 3832

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9221
Size: 291 Color: 3644
Size: 256 Color: 949

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9236
Size: 291 Color: 3670
Size: 256 Color: 912

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9223
Size: 289 Color: 3502
Size: 258 Color: 1145

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9231
Size: 289 Color: 3532
Size: 258 Color: 1078

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9264
Size: 279 Color: 2900
Size: 267 Color: 1865

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9246
Size: 281 Color: 3036
Size: 265 Color: 1678

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9268
Size: 289 Color: 3544
Size: 257 Color: 1014

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9249
Size: 275 Color: 2563
Size: 271 Color: 2227

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9247
Size: 289 Color: 3555
Size: 257 Color: 992

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9259
Size: 289 Color: 3552
Size: 257 Color: 978

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9265
Size: 279 Color: 2845
Size: 267 Color: 1948

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9251
Size: 283 Color: 3166
Size: 263 Color: 1519

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9280
Size: 284 Color: 3229
Size: 261 Color: 1392

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9273
Size: 288 Color: 3446
Size: 257 Color: 1002

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9275
Size: 289 Color: 3503
Size: 256 Color: 916

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9274
Size: 284 Color: 3221
Size: 261 Color: 1353

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9272
Size: 287 Color: 3383
Size: 258 Color: 1061

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9271
Size: 275 Color: 2566
Size: 270 Color: 2180

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9291
Size: 293 Color: 3756
Size: 251 Color: 273

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9282
Size: 282 Color: 3039
Size: 262 Color: 1460

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9284
Size: 276 Color: 2677
Size: 268 Color: 1961

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9299
Size: 283 Color: 3169
Size: 261 Color: 1366

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9300
Size: 275 Color: 2542
Size: 269 Color: 2125

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6429
Size: 345 Color: 6228
Size: 306 Color: 4470

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9290
Size: 279 Color: 2899
Size: 265 Color: 1693

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9301
Size: 280 Color: 2928
Size: 264 Color: 1613

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9298
Size: 294 Color: 3844
Size: 250 Color: 196

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9306
Size: 289 Color: 3537
Size: 255 Color: 723

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9285
Size: 284 Color: 3244
Size: 260 Color: 1282

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9283
Size: 292 Color: 3714
Size: 252 Color: 353

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9289
Size: 289 Color: 3515
Size: 255 Color: 828

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9314
Size: 275 Color: 2568
Size: 268 Color: 2037

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9312
Size: 273 Color: 2436
Size: 270 Color: 2140

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9311
Size: 278 Color: 2832
Size: 265 Color: 1675

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9326
Size: 276 Color: 2689
Size: 267 Color: 1872

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9321
Size: 278 Color: 2830
Size: 265 Color: 1668

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9315
Size: 277 Color: 2748
Size: 266 Color: 1761

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9319
Size: 281 Color: 3000
Size: 262 Color: 1475

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9325
Size: 290 Color: 3620
Size: 253 Color: 482

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9309
Size: 284 Color: 3238
Size: 259 Color: 1184

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9329
Size: 283 Color: 3131
Size: 260 Color: 1298

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9320
Size: 273 Color: 2369
Size: 270 Color: 2188

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9324
Size: 290 Color: 3585
Size: 253 Color: 570

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9341
Size: 273 Color: 2442
Size: 269 Color: 2117

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9337
Size: 282 Color: 3071
Size: 260 Color: 1296

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9336
Size: 282 Color: 3099
Size: 260 Color: 1316

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9342
Size: 285 Color: 3293
Size: 257 Color: 1013

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9340
Size: 283 Color: 3126
Size: 259 Color: 1225

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9351
Size: 286 Color: 3316
Size: 255 Color: 846

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9359
Size: 279 Color: 2863
Size: 261 Color: 1426

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9361
Size: 289 Color: 3508
Size: 251 Color: 258

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9371
Size: 281 Color: 2985
Size: 259 Color: 1227

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9364
Size: 285 Color: 3300
Size: 255 Color: 790

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9356
Size: 271 Color: 2277
Size: 269 Color: 2094

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9358
Size: 286 Color: 3332
Size: 254 Color: 626

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9353
Size: 290 Color: 3614
Size: 250 Color: 133

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8019
Size: 309 Color: 4658
Size: 292 Color: 3698

Bin 3126: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9386
Size: 277 Color: 2752
Size: 262 Color: 1495

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9376
Size: 282 Color: 3075
Size: 257 Color: 1001

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9387
Size: 280 Color: 2936
Size: 259 Color: 1201

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9382
Size: 272 Color: 2341
Size: 267 Color: 1885

Bin 3130: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9379
Size: 289 Color: 3541
Size: 250 Color: 151

Bin 3131: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9397
Size: 277 Color: 2736
Size: 261 Color: 1404

Bin 3132: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9401
Size: 281 Color: 3032
Size: 257 Color: 1027

Bin 3133: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8187
Size: 334 Color: 5811
Size: 260 Color: 1280

Bin 3134: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9403
Size: 287 Color: 3430
Size: 251 Color: 261

Bin 3135: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9409
Size: 279 Color: 2878
Size: 259 Color: 1215

Bin 3136: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9394
Size: 277 Color: 2746
Size: 261 Color: 1420

Bin 3137: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9400
Size: 275 Color: 2597
Size: 263 Color: 1577

Bin 3138: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9412
Size: 281 Color: 3029
Size: 257 Color: 1031

Bin 3139: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9424
Size: 273 Color: 2384
Size: 264 Color: 1644

Bin 3140: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7860
Size: 333 Color: 5741
Size: 273 Color: 2386

Bin 3141: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9421
Size: 271 Color: 2256
Size: 266 Color: 1780

Bin 3142: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9432
Size: 274 Color: 2513
Size: 263 Color: 1550

Bin 3143: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9428
Size: 278 Color: 2816
Size: 259 Color: 1196

Bin 3144: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9420
Size: 284 Color: 3234
Size: 253 Color: 534

Bin 3145: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9422
Size: 279 Color: 2842
Size: 258 Color: 1123

Bin 3146: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9444
Size: 271 Color: 2229
Size: 265 Color: 1738

Bin 3147: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9443
Size: 281 Color: 2970
Size: 255 Color: 741

Bin 3148: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9441
Size: 273 Color: 2394
Size: 263 Color: 1505

Bin 3149: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9452
Size: 269 Color: 2128
Size: 266 Color: 1837

Bin 3150: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9453
Size: 269 Color: 2129
Size: 266 Color: 1832

Bin 3151: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9463
Size: 278 Color: 2787
Size: 257 Color: 976

Bin 3152: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9459
Size: 275 Color: 2548
Size: 260 Color: 1320

Bin 3153: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9454
Size: 283 Color: 3178
Size: 252 Color: 379

Bin 3154: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9464
Size: 270 Color: 2160
Size: 265 Color: 1708

Bin 3155: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9470
Size: 274 Color: 2519
Size: 260 Color: 1276

Bin 3156: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9469
Size: 275 Color: 2603
Size: 259 Color: 1158

Bin 3157: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9482
Size: 273 Color: 2360
Size: 261 Color: 1418

Bin 3158: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9483
Size: 281 Color: 3024
Size: 253 Color: 532

Bin 3159: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9476
Size: 277 Color: 2764
Size: 257 Color: 1010

Bin 3160: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9484
Size: 278 Color: 2817
Size: 256 Color: 890

Bin 3161: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9480
Size: 273 Color: 2381
Size: 261 Color: 1354

Bin 3162: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9495
Size: 271 Color: 2234
Size: 262 Color: 1437

Bin 3163: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9485
Size: 269 Color: 2123
Size: 264 Color: 1646

Bin 3164: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6884
Size: 326 Color: 5444
Size: 312 Color: 4790

Bin 3165: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9486
Size: 282 Color: 3038
Size: 251 Color: 253

Bin 3166: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9512
Size: 277 Color: 2701
Size: 255 Color: 832

Bin 3167: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9506
Size: 281 Color: 3011
Size: 251 Color: 203

Bin 3168: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8714
Size: 297 Color: 4025
Size: 276 Color: 2665

Bin 3169: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9500
Size: 275 Color: 2600
Size: 257 Color: 1008

Bin 3170: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9502
Size: 279 Color: 2905
Size: 253 Color: 538

Bin 3171: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9511
Size: 271 Color: 2215
Size: 261 Color: 1352

Bin 3172: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9524
Size: 269 Color: 2099
Size: 262 Color: 1449

Bin 3173: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9533
Size: 267 Color: 1857
Size: 264 Color: 1648

Bin 3174: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9523
Size: 268 Color: 2056
Size: 263 Color: 1507

Bin 3175: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9521
Size: 271 Color: 2254
Size: 260 Color: 1315

Bin 3176: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9529
Size: 273 Color: 2400
Size: 258 Color: 1143

Bin 3177: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9537
Size: 272 Color: 2280
Size: 258 Color: 1087

Bin 3178: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9549
Size: 274 Color: 2487
Size: 256 Color: 927

Bin 3179: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9538
Size: 280 Color: 2948
Size: 250 Color: 78

Bin 3180: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9543
Size: 266 Color: 1791
Size: 264 Color: 1629

Bin 3181: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9548
Size: 266 Color: 1821
Size: 264 Color: 1641

Bin 3182: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9547
Size: 275 Color: 2605
Size: 255 Color: 734

Bin 3183: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9553
Size: 275 Color: 2575
Size: 254 Color: 666

Bin 3184: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9560
Size: 271 Color: 2213
Size: 258 Color: 1107

Bin 3185: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9556
Size: 273 Color: 2382
Size: 256 Color: 878

Bin 3186: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9563
Size: 269 Color: 2059
Size: 260 Color: 1330

Bin 3187: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9258
Size: 273 Color: 2430
Size: 273 Color: 2411

Bin 3188: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9564
Size: 267 Color: 1909
Size: 262 Color: 1432

Bin 3189: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9562
Size: 268 Color: 2048
Size: 261 Color: 1403

Bin 3190: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9559
Size: 271 Color: 2268
Size: 258 Color: 1059

Bin 3191: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9675
Size: 263 Color: 1525
Size: 259 Color: 1154

Bin 3192: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9566
Size: 272 Color: 2318
Size: 256 Color: 948

Bin 3193: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9365
Size: 285 Color: 3301
Size: 255 Color: 743

Bin 3194: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9571
Size: 278 Color: 2792
Size: 250 Color: 111

Bin 3195: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9579
Size: 272 Color: 2286
Size: 256 Color: 955

Bin 3196: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9574
Size: 264 Color: 1614
Size: 264 Color: 1610

Bin 3197: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9570
Size: 274 Color: 2514
Size: 254 Color: 617

Bin 3198: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9569
Size: 278 Color: 2780
Size: 250 Color: 5

Bin 3199: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9582
Size: 265 Color: 1730
Size: 263 Color: 1527

Bin 3200: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9584
Size: 274 Color: 2523
Size: 254 Color: 692

Bin 3201: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9595
Size: 272 Color: 2333
Size: 255 Color: 813

Bin 3202: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9590
Size: 268 Color: 2014
Size: 259 Color: 1229

Bin 3203: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9603
Size: 269 Color: 2073
Size: 258 Color: 1093

Bin 3204: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9597
Size: 275 Color: 2609
Size: 252 Color: 406

Bin 3205: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9624
Size: 270 Color: 2196
Size: 256 Color: 888

Bin 3206: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9618
Size: 266 Color: 1827
Size: 260 Color: 1291

Bin 3207: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9615
Size: 265 Color: 1756
Size: 261 Color: 1415

Bin 3208: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9613
Size: 273 Color: 2357
Size: 253 Color: 504

Bin 3209: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9608
Size: 273 Color: 2440
Size: 253 Color: 584

Bin 3210: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9367
Size: 279 Color: 2907
Size: 261 Color: 1357

Bin 3211: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9627
Size: 272 Color: 2298
Size: 253 Color: 477

Bin 3212: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9633
Size: 271 Color: 2269
Size: 254 Color: 637

Bin 3213: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9632
Size: 272 Color: 2306
Size: 253 Color: 551

Bin 3214: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8976
Size: 307 Color: 4523
Size: 253 Color: 568

Bin 3215: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9636
Size: 267 Color: 1854
Size: 258 Color: 1127

Bin 3216: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9639
Size: 272 Color: 2353
Size: 252 Color: 376

Bin 3217: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9644
Size: 269 Color: 2134
Size: 255 Color: 777

Bin 3218: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9640
Size: 273 Color: 2364
Size: 251 Color: 227

Bin 3219: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9651
Size: 263 Color: 1553
Size: 261 Color: 1347

Bin 3220: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9637
Size: 268 Color: 1982
Size: 256 Color: 933

Bin 3221: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9650
Size: 271 Color: 2257
Size: 253 Color: 597

Bin 3222: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 9652
Size: 266 Color: 1811
Size: 258 Color: 1133

Bin 3223: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9663
Size: 266 Color: 1825
Size: 257 Color: 1032

Bin 3224: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9657
Size: 269 Color: 2136
Size: 254 Color: 695

Bin 3225: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9666
Size: 273 Color: 2408
Size: 250 Color: 27

Bin 3226: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9662
Size: 266 Color: 1841
Size: 257 Color: 1040

Bin 3227: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9661
Size: 267 Color: 1856
Size: 256 Color: 915

Bin 3228: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9667
Size: 269 Color: 2101
Size: 254 Color: 699

Bin 3229: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9672
Size: 268 Color: 1988
Size: 254 Color: 648

Bin 3230: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9680
Size: 267 Color: 1910
Size: 255 Color: 785

Bin 3231: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9676
Size: 272 Color: 2287
Size: 250 Color: 188

Bin 3232: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9682
Size: 265 Color: 1702
Size: 257 Color: 1007

Bin 3233: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9700
Size: 269 Color: 2135
Size: 252 Color: 410

Bin 3234: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9684
Size: 268 Color: 2027
Size: 253 Color: 525

Bin 3235: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9696
Size: 271 Color: 2251
Size: 250 Color: 81

Bin 3236: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9693
Size: 268 Color: 1950
Size: 253 Color: 508

Bin 3237: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9690
Size: 262 Color: 1436
Size: 259 Color: 1230

Bin 3238: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9715
Size: 270 Color: 2151
Size: 250 Color: 77

Bin 3239: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9706
Size: 263 Color: 1515
Size: 257 Color: 969

Bin 3240: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9711
Size: 270 Color: 2170
Size: 250 Color: 59

Bin 3241: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 9720
Size: 268 Color: 2012
Size: 251 Color: 303

Bin 3242: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 9719
Size: 266 Color: 1816
Size: 253 Color: 521

Bin 3243: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9729
Size: 261 Color: 1412
Size: 257 Color: 1036

Bin 3244: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9734
Size: 263 Color: 1512
Size: 255 Color: 845

Bin 3245: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9726
Size: 261 Color: 1351
Size: 257 Color: 960

Bin 3246: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9723
Size: 261 Color: 1360
Size: 257 Color: 993

Bin 3247: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9745
Size: 259 Color: 1153
Size: 258 Color: 1120

Bin 3248: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9737
Size: 261 Color: 1421
Size: 256 Color: 924

Bin 3249: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9738
Size: 260 Color: 1271
Size: 257 Color: 1038

Bin 3250: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9740
Size: 262 Color: 1454
Size: 255 Color: 792

Bin 3251: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9736
Size: 267 Color: 1889
Size: 250 Color: 91

Bin 3252: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9747
Size: 259 Color: 1234
Size: 258 Color: 1084

Bin 3253: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9756
Size: 261 Color: 1379
Size: 255 Color: 789

Bin 3254: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9758
Size: 261 Color: 1400
Size: 255 Color: 754

Bin 3255: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9752
Size: 258 Color: 1117
Size: 258 Color: 1085

Bin 3256: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9753
Size: 266 Color: 1826
Size: 250 Color: 11

Bin 3257: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9751
Size: 258 Color: 1144
Size: 258 Color: 1139

Bin 3258: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9755
Size: 266 Color: 1847
Size: 250 Color: 103

Bin 3259: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9750
Size: 265 Color: 1666
Size: 251 Color: 210

Bin 3260: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9774
Size: 265 Color: 1698
Size: 250 Color: 118

Bin 3261: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9772
Size: 265 Color: 1669
Size: 250 Color: 50

Bin 3262: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9782
Size: 260 Color: 1325
Size: 255 Color: 838

Bin 3263: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9769
Size: 262 Color: 1490
Size: 253 Color: 512

Bin 3264: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9780
Size: 264 Color: 1616
Size: 251 Color: 201

Bin 3265: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9768
Size: 258 Color: 1112
Size: 257 Color: 1055

Bin 3266: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9785
Size: 261 Color: 1424
Size: 253 Color: 520

Bin 3267: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9792
Size: 260 Color: 1245
Size: 254 Color: 702

Bin 3268: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9796
Size: 261 Color: 1428
Size: 253 Color: 491

Bin 3269: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9786
Size: 259 Color: 1156
Size: 255 Color: 799

Bin 3270: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9784
Size: 262 Color: 1497
Size: 252 Color: 458

Bin 3271: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9135
Size: 299 Color: 4140
Size: 252 Color: 412

Bin 3272: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9804
Size: 260 Color: 1278
Size: 253 Color: 565

Bin 3273: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9807
Size: 258 Color: 1075
Size: 255 Color: 781

Bin 3274: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9812
Size: 262 Color: 1448
Size: 251 Color: 279

Bin 3275: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9808
Size: 258 Color: 1125
Size: 255 Color: 833

Bin 3276: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9809
Size: 260 Color: 1292
Size: 253 Color: 579

Bin 3277: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9806
Size: 262 Color: 1435
Size: 251 Color: 207

Bin 3278: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9818
Size: 258 Color: 1070
Size: 254 Color: 705

Bin 3279: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8081
Size: 334 Color: 5799
Size: 264 Color: 1609

Bin 3280: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9826
Size: 257 Color: 1050
Size: 255 Color: 811

Bin 3281: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9819
Size: 256 Color: 947
Size: 256 Color: 896

Bin 3282: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9834
Size: 260 Color: 1328
Size: 251 Color: 209

Bin 3283: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9832
Size: 261 Color: 1372
Size: 250 Color: 105

Bin 3284: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8676
Size: 316 Color: 4969
Size: 259 Color: 1164

Bin 3285: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9830
Size: 257 Color: 1017
Size: 254 Color: 707

Bin 3286: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9836
Size: 261 Color: 1397
Size: 250 Color: 22

Bin 3287: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9847
Size: 258 Color: 1119
Size: 252 Color: 391

Bin 3288: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9848
Size: 260 Color: 1300
Size: 250 Color: 192

Bin 3289: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9844
Size: 256 Color: 875
Size: 254 Color: 644

Bin 3290: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9843
Size: 260 Color: 1303
Size: 250 Color: 156

Bin 3291: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9861
Size: 255 Color: 810
Size: 254 Color: 677

Bin 3292: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9869
Size: 258 Color: 1081
Size: 250 Color: 190

Bin 3293: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8034
Size: 349 Color: 6405
Size: 251 Color: 298

Bin 3294: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 9875
Size: 256 Color: 937
Size: 252 Color: 352

Bin 3295: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9880
Size: 254 Color: 706
Size: 253 Color: 573

Bin 3296: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9889
Size: 255 Color: 823
Size: 252 Color: 395

Bin 3297: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9881
Size: 254 Color: 689
Size: 253 Color: 478

Bin 3298: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9882
Size: 256 Color: 940
Size: 251 Color: 275

Bin 3299: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9905
Size: 256 Color: 868
Size: 250 Color: 174

Bin 3300: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9677
Size: 261 Color: 1386
Size: 261 Color: 1350

Bin 3301: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9912
Size: 255 Color: 730
Size: 250 Color: 131

Bin 3302: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9924
Size: 254 Color: 718
Size: 251 Color: 223

Bin 3303: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9913
Size: 255 Color: 825
Size: 250 Color: 116

Bin 3304: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9929
Size: 253 Color: 535
Size: 251 Color: 250

Bin 3305: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9930
Size: 254 Color: 638
Size: 250 Color: 154

Bin 3306: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9685
Size: 265 Color: 1662
Size: 256 Color: 872

Bin 3307: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9936
Size: 253 Color: 604
Size: 251 Color: 316

Bin 3308: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9945
Size: 252 Color: 350
Size: 251 Color: 260

Bin 3309: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9941
Size: 253 Color: 601
Size: 250 Color: 124

Bin 3310: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9949
Size: 253 Color: 587
Size: 250 Color: 69

Bin 3311: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9950
Size: 253 Color: 586
Size: 250 Color: 32

Bin 3312: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9940
Size: 253 Color: 605
Size: 250 Color: 114

Bin 3313: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9973
Size: 252 Color: 386
Size: 250 Color: 138

Bin 3314: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9965
Size: 252 Color: 363
Size: 250 Color: 21

Bin 3315: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 8743
Size: 320 Color: 5168
Size: 252 Color: 408

Bin 3316: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9958
Size: 252 Color: 426
Size: 250 Color: 165

Bin 3317: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 9998
Size: 500 Color: 9980

Bin 3318: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 9993
Size: 500 Color: 9984

Bin 3319: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8977
Size: 292 Color: 3716
Size: 268 Color: 1989

Bin 3320: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 9997
Size: 500 Color: 9976

Bin 3321: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 9989
Size: 500 Color: 9975

Bin 3322: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 9994
Size: 500 Color: 9978

Bin 3323: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 9992
Size: 500 Color: 9982

Bin 3324: 1 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9035
Size: 301 Color: 4230
Size: 255 Color: 767

Bin 3325: 1 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7392
Size: 317 Color: 5038
Size: 305 Color: 4451

Bin 3326: 1 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8587
Size: 309 Color: 4618
Size: 268 Color: 2034

Bin 3327: 1 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8995
Size: 303 Color: 4329
Size: 255 Color: 749

Bin 3328: 1 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8029
Size: 342 Color: 6100
Size: 257 Color: 1052

Bin 3329: 4 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8336
Size: 295 Color: 3887
Size: 289 Color: 3549

Bin 3330: 7 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9763
Size: 255 Color: 779
Size: 254 Color: 703

Bin 3331: 58 of cap free
Amount of items: 3
Items: 
Size: 333 Color: 5749
Size: 330 Color: 5657
Size: 280 Color: 2920

Bin 3332: 195 of cap free
Amount of items: 3
Items: 
Size: 274 Color: 2486
Size: 267 Color: 1900
Size: 265 Color: 1754

Bin 3333: 239 of cap free
Amount of items: 3
Items: 
Size: 254 Color: 694
Size: 254 Color: 675
Size: 254 Color: 640

Bin 3334: 244 of cap free
Amount of items: 3
Items: 
Size: 253 Color: 578
Size: 253 Color: 498
Size: 251 Color: 206

Bin 3335: 249 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 9986
Size: 251 Color: 262

Total size: 3337334
Total free space: 1001

