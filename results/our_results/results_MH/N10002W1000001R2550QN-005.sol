Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 10002

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 376301 Color: 7328
Size: 345485 Color: 6266
Size: 278215 Color: 2788

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 380740 Color: 7472
Size: 343725 Color: 6206
Size: 275536 Color: 2577

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 361588 Color: 6866
Size: 324885 Color: 5447
Size: 313528 Color: 4887

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 373748 Color: 7252
Size: 346182 Color: 6291
Size: 280071 Color: 2936

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 383114 Color: 7539
Size: 342927 Color: 6173
Size: 273960 Color: 2463

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 378690 Color: 7400
Size: 339901 Color: 6060
Size: 281410 Color: 3020

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 381256 Color: 7491
Size: 367997 Color: 7077
Size: 250748 Color: 122

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 385622 Color: 7605
Size: 341839 Color: 6130
Size: 272540 Color: 2342

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 355856 Color: 6683
Size: 346788 Color: 6317
Size: 297357 Color: 4025

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 385434 Color: 7600
Size: 341989 Color: 6136
Size: 272578 Color: 2346

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 350592 Color: 6474
Size: 328176 Color: 5593
Size: 321233 Color: 5289

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 382803 Color: 7534
Size: 353503 Color: 6608
Size: 263695 Color: 1605

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 401468 Color: 8045
Size: 326469 Color: 5518
Size: 272064 Color: 2297

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 371821 Color: 7199
Size: 352414 Color: 6567
Size: 275766 Color: 2593

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 379846 Color: 7438
Size: 344276 Color: 6228
Size: 275879 Color: 2604

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 375342 Color: 7297
Size: 349337 Color: 6424
Size: 275322 Color: 2567

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 385485 Color: 7602
Size: 339648 Color: 6045
Size: 274868 Color: 2529

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 391898 Color: 7791
Size: 339206 Color: 6027
Size: 268897 Color: 2045

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 393511 Color: 7848
Size: 338496 Color: 6005
Size: 267994 Color: 1974

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 373893 Color: 7255
Size: 317657 Color: 5097
Size: 308451 Color: 4617

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 382507 Color: 7529
Size: 343294 Color: 6187
Size: 274200 Color: 2482

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 405586 Color: 8152
Size: 326218 Color: 5508
Size: 268197 Color: 1991

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 386767 Color: 7641
Size: 340700 Color: 6087
Size: 272534 Color: 2340

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 403538 Color: 8093
Size: 339887 Color: 6059
Size: 256576 Color: 894

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 355483 Color: 6669
Size: 350163 Color: 6458
Size: 294355 Color: 3838

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 405382 Color: 8143
Size: 317641 Color: 5096
Size: 276978 Color: 2697

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 385925 Color: 7618
Size: 351430 Color: 6520
Size: 262646 Color: 1489

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 392655 Color: 7819
Size: 335734 Color: 5882
Size: 271612 Color: 2252

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 386469 Color: 7633
Size: 358861 Color: 6781
Size: 254671 Color: 668

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 380431 Color: 7461
Size: 356330 Color: 6693
Size: 263240 Color: 1560

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 401667 Color: 8050
Size: 338188 Color: 5991
Size: 260146 Color: 1269

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 371112 Color: 7181
Size: 346718 Color: 6314
Size: 282171 Color: 3076

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 407052 Color: 8202
Size: 340108 Color: 6066
Size: 252841 Color: 414

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 387575 Color: 7660
Size: 345129 Color: 6250
Size: 267297 Color: 1915

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 388814 Color: 7702
Size: 338866 Color: 6014
Size: 272321 Color: 2321

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 389594 Color: 7719
Size: 358348 Color: 6764
Size: 252059 Color: 318

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 371739 Color: 7197
Size: 339882 Color: 6058
Size: 288380 Color: 3497

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 367916 Color: 7074
Size: 365947 Color: 7018
Size: 266138 Color: 1814

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 412055 Color: 8321
Size: 337856 Color: 5976
Size: 250090 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 406413 Color: 8173
Size: 339184 Color: 6026
Size: 254404 Color: 633

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 337509 Color: 5955
Size: 334195 Color: 5829
Size: 328297 Color: 5599

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 385768 Color: 7615
Size: 341728 Color: 6125
Size: 272505 Color: 2336

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 350925 Color: 6496
Size: 336143 Color: 5904
Size: 312933 Color: 4860

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 411142 Color: 8300
Size: 327569 Color: 5567
Size: 261290 Color: 1382

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 368994 Color: 7117
Size: 347043 Color: 6331
Size: 283964 Color: 3217

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 369154 Color: 7123
Size: 346965 Color: 6328
Size: 283882 Color: 3210

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 391619 Color: 7781
Size: 339406 Color: 6033
Size: 268976 Color: 2051

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 410647 Color: 8286
Size: 338490 Color: 6004
Size: 250864 Color: 150

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 372418 Color: 7225
Size: 344848 Color: 6242
Size: 282735 Color: 3124

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 365369 Color: 7000
Size: 329874 Color: 5655
Size: 304758 Color: 4435

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 366105 Color: 7025
Size: 360859 Color: 6839
Size: 273037 Color: 2389

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 398552 Color: 7972
Size: 328215 Color: 5594
Size: 273234 Color: 2406

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 393445 Color: 7845
Size: 338512 Color: 6006
Size: 268044 Color: 1978

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 361431 Color: 6857
Size: 351521 Color: 6526
Size: 287049 Color: 3427

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 384410 Color: 7578
Size: 342423 Color: 6153
Size: 273168 Color: 2400

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 393339 Color: 7842
Size: 337647 Color: 5963
Size: 269015 Color: 2057

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 387350 Color: 7653
Size: 340576 Color: 6084
Size: 272075 Color: 2299

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 411402 Color: 8305
Size: 332550 Color: 5761
Size: 256049 Color: 829

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 409612 Color: 8255
Size: 330129 Color: 5662
Size: 260260 Color: 1281

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 402473 Color: 8072
Size: 332897 Color: 5773
Size: 264631 Color: 1690

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 370090 Color: 7146
Size: 347693 Color: 6355
Size: 282218 Color: 3088

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 383765 Color: 7565
Size: 352214 Color: 6557
Size: 264022 Color: 1631

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 402891 Color: 8084
Size: 341420 Color: 6116
Size: 255690 Color: 794

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 376973 Color: 7350
Size: 362261 Color: 6883
Size: 260767 Color: 1336

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 385480 Color: 7601
Size: 331262 Color: 5704
Size: 283259 Color: 3158

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 384542 Color: 7582
Size: 360362 Color: 6822
Size: 255097 Color: 721

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 391627 Color: 7782
Size: 356947 Color: 6715
Size: 251427 Color: 241

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 387384 Color: 7655
Size: 340909 Color: 6099
Size: 271708 Color: 2260

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 374767 Color: 7285
Size: 358718 Color: 6775
Size: 266516 Color: 1855

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 380322 Color: 7458
Size: 343852 Color: 6213
Size: 275827 Color: 2601

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 387617 Color: 7663
Size: 340931 Color: 6101
Size: 271453 Color: 2241

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 363666 Color: 6937
Size: 347123 Color: 6338
Size: 289212 Color: 3546

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 412085 Color: 8323
Size: 337344 Color: 5948
Size: 250572 Color: 92

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 361366 Color: 6855
Size: 337260 Color: 5944
Size: 301375 Color: 4234

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 380993 Color: 7478
Size: 343634 Color: 6204
Size: 275374 Color: 2571

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 411957 Color: 8319
Size: 326349 Color: 5513
Size: 261695 Color: 1422

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 386699 Color: 7638
Size: 341226 Color: 6111
Size: 272076 Color: 2300

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 405188 Color: 8138
Size: 330640 Color: 5686
Size: 264173 Color: 1649

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 380438 Color: 7462
Size: 338489 Color: 6003
Size: 281074 Color: 3000

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 399668 Color: 8000
Size: 337651 Color: 5964
Size: 262682 Color: 1492

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 377554 Color: 7364
Size: 349744 Color: 6437
Size: 272703 Color: 2356

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 380340 Color: 7459
Size: 342630 Color: 6161
Size: 277031 Color: 2699

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 386083 Color: 7624
Size: 341579 Color: 6119
Size: 272339 Color: 2324

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 395682 Color: 7908
Size: 336986 Color: 5935
Size: 267333 Color: 1916

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 394110 Color: 7867
Size: 338141 Color: 5987
Size: 267750 Color: 1959

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 403753 Color: 8100
Size: 346248 Color: 6296
Size: 250000 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 382716 Color: 7531
Size: 343142 Color: 6185
Size: 274143 Color: 2479

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 381913 Color: 7516
Size: 344034 Color: 6219
Size: 274054 Color: 2470

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 386694 Color: 7637
Size: 341094 Color: 6106
Size: 272213 Color: 2313

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 411708 Color: 8313
Size: 337359 Color: 5949
Size: 250934 Color: 164

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 411661 Color: 8310
Size: 332282 Color: 5747
Size: 256058 Color: 833

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 403938 Color: 8108
Size: 325967 Color: 5499
Size: 270096 Color: 2132

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 364584 Color: 6965
Size: 340707 Color: 6088
Size: 294710 Color: 3860

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 384149 Color: 7572
Size: 352805 Color: 6583
Size: 263047 Color: 1536

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 402042 Color: 8059
Size: 323560 Color: 5404
Size: 274399 Color: 2503

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 409720 Color: 8259
Size: 336324 Color: 5915
Size: 253957 Color: 566

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 393046 Color: 7834
Size: 345417 Color: 6260
Size: 261538 Color: 1407

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 411048 Color: 8297
Size: 336983 Color: 5934
Size: 251970 Color: 304

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 395395 Color: 7900
Size: 349143 Color: 6416
Size: 255463 Color: 762

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 412200 Color: 8324
Size: 333835 Color: 5811
Size: 253966 Color: 567

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 411113 Color: 8299
Size: 327161 Color: 5547
Size: 261727 Color: 1424

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 405214 Color: 8140
Size: 337878 Color: 5977
Size: 256909 Color: 937

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 409736 Color: 8260
Size: 333953 Color: 5817
Size: 256312 Color: 864

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 410935 Color: 8293
Size: 325341 Color: 5465
Size: 263725 Color: 1607

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 400342 Color: 8019
Size: 347107 Color: 6335
Size: 252552 Color: 375

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 410247 Color: 8273
Size: 328092 Color: 5588
Size: 261662 Color: 1421

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 385692 Color: 7610
Size: 359609 Color: 6799
Size: 254700 Color: 673

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 385128 Color: 7591
Size: 363828 Color: 6941
Size: 251045 Color: 183

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 405113 Color: 8136
Size: 338282 Color: 5997
Size: 256606 Color: 897

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 409523 Color: 8254
Size: 335646 Color: 5879
Size: 254832 Color: 690

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 410351 Color: 8278
Size: 336908 Color: 5930
Size: 252742 Color: 400

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 401480 Color: 8046
Size: 338153 Color: 5988
Size: 260368 Color: 1297

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 406441 Color: 8176
Size: 326182 Color: 5503
Size: 267378 Color: 1922

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 408596 Color: 8233
Size: 336225 Color: 5912
Size: 255180 Color: 733

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 401916 Color: 8054
Size: 342239 Color: 6146
Size: 255846 Color: 808

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 399548 Color: 7996
Size: 344644 Color: 6236
Size: 255809 Color: 803

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 407928 Color: 8223
Size: 335811 Color: 5885
Size: 256262 Color: 856

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 409811 Color: 8262
Size: 332597 Color: 5764
Size: 257593 Color: 1001

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 394280 Color: 7872
Size: 345760 Color: 6276
Size: 259961 Color: 1246

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 405577 Color: 8151
Size: 332676 Color: 5767
Size: 261748 Color: 1427

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 410843 Color: 8291
Size: 332314 Color: 5750
Size: 256844 Color: 928

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 365324 Color: 6998
Size: 318355 Color: 5133
Size: 316322 Color: 5034

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 407396 Color: 8211
Size: 332551 Color: 5762
Size: 260054 Color: 1258

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 410129 Color: 8270
Size: 333907 Color: 5815
Size: 255965 Color: 820

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 405225 Color: 8141
Size: 332636 Color: 5766
Size: 262140 Color: 1457

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 405672 Color: 8154
Size: 335143 Color: 5853
Size: 259186 Color: 1164

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 407337 Color: 8209
Size: 332450 Color: 5754
Size: 260214 Color: 1276

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 400766 Color: 8028
Size: 343630 Color: 6203
Size: 255605 Color: 787

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 404697 Color: 8127
Size: 341188 Color: 6109
Size: 254116 Color: 594

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 405880 Color: 8157
Size: 333047 Color: 5779
Size: 261074 Color: 1369

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 388864 Color: 7706
Size: 352409 Color: 6566
Size: 258728 Color: 1119

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 407395 Color: 8210
Size: 334284 Color: 5833
Size: 258322 Color: 1079

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 408522 Color: 8231
Size: 333219 Color: 5788
Size: 258260 Color: 1066

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 407611 Color: 8216
Size: 334727 Color: 5844
Size: 257663 Color: 1005

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 405042 Color: 8134
Size: 332383 Color: 5752
Size: 262576 Color: 1484

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 405734 Color: 8156
Size: 333037 Color: 5778
Size: 261230 Color: 1379

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 406327 Color: 8171
Size: 335624 Color: 5877
Size: 258050 Color: 1043

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 407205 Color: 8207
Size: 336676 Color: 5925
Size: 256120 Color: 839

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 403752 Color: 8099
Size: 336222 Color: 5910
Size: 260027 Color: 1254

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 406422 Color: 8174
Size: 333399 Color: 5793
Size: 260180 Color: 1273

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 409685 Color: 8258
Size: 334074 Color: 5825
Size: 256242 Color: 854

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 370703 Color: 7167
Size: 366379 Color: 7034
Size: 262919 Color: 1518

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 387994 Color: 7674
Size: 355299 Color: 6660
Size: 256708 Color: 907

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 370464 Color: 7159
Size: 365207 Color: 6989
Size: 264330 Color: 1664

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 409475 Color: 8253
Size: 333679 Color: 5806
Size: 256847 Color: 930

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 404608 Color: 8124
Size: 332218 Color: 5744
Size: 263175 Color: 1553

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 406671 Color: 8186
Size: 336531 Color: 5923
Size: 256799 Color: 922

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 400188 Color: 8013
Size: 343094 Color: 6181
Size: 256719 Color: 909

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 402082 Color: 8061
Size: 333498 Color: 5796
Size: 264421 Color: 1672

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 351293 Color: 6512
Size: 332841 Color: 5771
Size: 315867 Color: 5001

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 406809 Color: 8195
Size: 332481 Color: 5756
Size: 260711 Color: 1327

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 406450 Color: 8177
Size: 333508 Color: 5797
Size: 260043 Color: 1257

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 411807 Color: 8315
Size: 332037 Color: 5732
Size: 256157 Color: 846

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 404306 Color: 8116
Size: 332133 Color: 5738
Size: 263562 Color: 1592

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 403902 Color: 8107
Size: 331901 Color: 5725
Size: 264198 Color: 1652

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 403944 Color: 8109
Size: 331929 Color: 5728
Size: 264128 Color: 1645

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 403536 Color: 8092
Size: 330616 Color: 5685
Size: 265849 Color: 1791

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 401972 Color: 8057
Size: 333699 Color: 5808
Size: 264330 Color: 1665

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 403571 Color: 8095
Size: 334998 Color: 5847
Size: 261432 Color: 1401

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 403495 Color: 8091
Size: 343479 Color: 6196
Size: 253027 Color: 443

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 411700 Color: 8312
Size: 294659 Color: 3858
Size: 293642 Color: 3801

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 402387 Color: 8070
Size: 332968 Color: 5775
Size: 264646 Color: 1691

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 403602 Color: 8097
Size: 341725 Color: 6124
Size: 254674 Color: 669

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 388508 Color: 7692
Size: 358746 Color: 6777
Size: 252747 Color: 401

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 379533 Color: 7430
Size: 359935 Color: 6809
Size: 260533 Color: 1314

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 402563 Color: 8074
Size: 330794 Color: 5689
Size: 266644 Color: 1865

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 402961 Color: 8085
Size: 335233 Color: 5858
Size: 261807 Color: 1435

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 402748 Color: 8080
Size: 330927 Color: 5696
Size: 266326 Color: 1834

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 406646 Color: 8183
Size: 330660 Color: 5687
Size: 262695 Color: 1494

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 393111 Color: 7838
Size: 352879 Color: 6586
Size: 254011 Color: 574

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 406546 Color: 8180
Size: 330482 Color: 5679
Size: 262973 Color: 1526

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 402137 Color: 8063
Size: 337581 Color: 5960
Size: 260283 Color: 1287

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 401995 Color: 8058
Size: 330375 Color: 5672
Size: 267631 Color: 1948

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 401759 Color: 8052
Size: 330209 Color: 5666
Size: 268033 Color: 1977

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 406435 Color: 8175
Size: 330140 Color: 5663
Size: 263426 Color: 1577

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 384573 Color: 7583
Size: 357090 Color: 6720
Size: 258338 Color: 1083

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 406559 Color: 8181
Size: 329892 Color: 5656
Size: 263550 Color: 1589

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 401404 Color: 8042
Size: 329290 Color: 5632
Size: 269307 Color: 2080

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 405059 Color: 8135
Size: 329142 Color: 5626
Size: 265800 Color: 1788

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 400352 Color: 8020
Size: 335342 Color: 5863
Size: 264307 Color: 1660

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 400354 Color: 8021
Size: 339999 Color: 6063
Size: 259648 Color: 1215

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 400313 Color: 8018
Size: 335375 Color: 5866
Size: 264313 Color: 1661

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 407672 Color: 8219
Size: 327206 Color: 5552
Size: 265123 Color: 1729

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 365208 Color: 6990
Size: 320592 Color: 5255
Size: 314201 Color: 4917

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 410159 Color: 8271
Size: 328724 Color: 5613
Size: 261118 Color: 1373

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 374089 Color: 7262
Size: 361546 Color: 6862
Size: 264366 Color: 1667

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 371929 Color: 7208
Size: 348250 Color: 6374
Size: 279822 Color: 2915

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 399661 Color: 7999
Size: 335419 Color: 5868
Size: 264921 Color: 1715

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 398871 Color: 7981
Size: 328066 Color: 5584
Size: 273064 Color: 2392

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 398864 Color: 7980
Size: 328019 Color: 5583
Size: 273118 Color: 2395

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 399431 Color: 7994
Size: 328336 Color: 5602
Size: 272234 Color: 2316

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 404319 Color: 8118
Size: 343805 Color: 6210
Size: 251877 Color: 294

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 383966 Color: 7568
Size: 349746 Color: 6438
Size: 266289 Color: 1831

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 365086 Color: 6982
Size: 349629 Color: 6431
Size: 285286 Color: 3318

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 351609 Color: 6531
Size: 326801 Color: 5534
Size: 321591 Color: 5302

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 364795 Color: 6974
Size: 324986 Color: 5451
Size: 310220 Color: 4731

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 364810 Color: 6976
Size: 325382 Color: 5471
Size: 309809 Color: 4710

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 261788 Color: 1433
Size: 400286 Color: 8016
Size: 337927 Color: 5979

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 336031 Color: 5895
Size: 333544 Color: 5800
Size: 330426 Color: 5675

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 374995 Color: 7288
Size: 345462 Color: 6265
Size: 279544 Color: 2887

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 390065 Color: 7730
Size: 357208 Color: 6724
Size: 252728 Color: 397

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 374328 Color: 7271
Size: 322975 Color: 5378
Size: 302698 Color: 4307

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 410960 Color: 8294
Size: 338251 Color: 5993
Size: 250790 Color: 130

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 374468 Color: 7273
Size: 345848 Color: 6280
Size: 279685 Color: 2902

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 405130 Color: 8137
Size: 308073 Color: 4603
Size: 286798 Color: 3417

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 373181 Color: 7238
Size: 326547 Color: 5523
Size: 300273 Color: 4182

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 372153 Color: 7216
Size: 354951 Color: 6653
Size: 272897 Color: 2377

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 390280 Color: 7737
Size: 345654 Color: 6271
Size: 264067 Color: 1639

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 405689 Color: 8155
Size: 343090 Color: 6180
Size: 251222 Color: 204

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 382605 Color: 7530
Size: 345530 Color: 6268
Size: 271866 Color: 2279

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 386406 Color: 7631
Size: 315748 Color: 4993
Size: 297847 Color: 4046

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 372312 Color: 7220
Size: 368961 Color: 7115
Size: 258728 Color: 1120

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 364314 Color: 6957
Size: 328359 Color: 5604
Size: 307328 Color: 4567

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 368396 Color: 7095
Size: 333961 Color: 5820
Size: 297644 Color: 4036

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 348681 Color: 6394
Size: 328220 Color: 5596
Size: 323100 Color: 5386

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 370752 Color: 7169
Size: 325115 Color: 5455
Size: 304134 Color: 4399

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 347663 Color: 6353
Size: 347595 Color: 6350
Size: 304743 Color: 4433

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 375258 Color: 7293
Size: 349838 Color: 6442
Size: 274905 Color: 2533

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 362342 Color: 6885
Size: 329686 Color: 5648
Size: 307973 Color: 4595

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 368494 Color: 7101
Size: 368344 Color: 7090
Size: 263163 Color: 1551

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 368476 Color: 7100
Size: 346523 Color: 6307
Size: 285002 Color: 3295

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 370663 Color: 7165
Size: 319855 Color: 5219
Size: 309483 Color: 4694

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 368293 Color: 7087
Size: 347248 Color: 6340
Size: 284460 Color: 3255

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 368089 Color: 7080
Size: 346160 Color: 6290
Size: 285752 Color: 3344

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 368091 Color: 7081
Size: 322099 Color: 5337
Size: 309811 Color: 4711

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 399026 Color: 7986
Size: 349135 Color: 6415
Size: 251840 Color: 288

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 363889 Color: 6944
Size: 330300 Color: 5667
Size: 305812 Color: 4490

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 368747 Color: 7108
Size: 321434 Color: 5297
Size: 309820 Color: 4713

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 369059 Color: 7119
Size: 321215 Color: 5288
Size: 309727 Color: 4705

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 367789 Color: 7069
Size: 350672 Color: 6478
Size: 281540 Color: 3027

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 402802 Color: 8081
Size: 323254 Color: 5394
Size: 273945 Color: 2461

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 363870 Color: 6942
Size: 331362 Color: 5708
Size: 304769 Color: 4437

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 366329 Color: 7033
Size: 321864 Color: 5320
Size: 311808 Color: 4810

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 366388 Color: 7036
Size: 322168 Color: 5339
Size: 311445 Color: 4794

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 365031 Color: 6981
Size: 362816 Color: 6908
Size: 272154 Color: 2308

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 366118 Color: 7026
Size: 327441 Color: 5560
Size: 306442 Color: 4524

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 366013 Color: 7021
Size: 352731 Color: 6580
Size: 281257 Color: 3011

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 365973 Color: 7019
Size: 349674 Color: 6433
Size: 284354 Color: 3245

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 365874 Color: 7016
Size: 317964 Color: 5110
Size: 316163 Color: 5019

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 365572 Color: 7009
Size: 318729 Color: 5161
Size: 315700 Color: 4989

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 365631 Color: 7011
Size: 318430 Color: 5143
Size: 315940 Color: 5006

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 349531 Color: 6429
Size: 346841 Color: 6320
Size: 303629 Color: 4369

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 363210 Color: 6926
Size: 346873 Color: 6321
Size: 289918 Color: 3584

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 363146 Color: 6924
Size: 332904 Color: 5774
Size: 303951 Color: 4391

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 363236 Color: 6927
Size: 347044 Color: 6332
Size: 289721 Color: 3574

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 362795 Color: 6905
Size: 349846 Color: 6444
Size: 287360 Color: 3447

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 362726 Color: 6902
Size: 345817 Color: 6279
Size: 291458 Color: 3680

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 362585 Color: 6898
Size: 345757 Color: 6275
Size: 291659 Color: 3695

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 362565 Color: 6896
Size: 353889 Color: 6624
Size: 283547 Color: 3179

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 393470 Color: 7847
Size: 336047 Color: 5898
Size: 270484 Color: 2162

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 362191 Color: 6880
Size: 337772 Color: 5970
Size: 300038 Color: 4167

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 362286 Color: 6884
Size: 345206 Color: 6252
Size: 292509 Color: 3749

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 384273 Color: 7577
Size: 362055 Color: 6875
Size: 253673 Color: 524

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 362086 Color: 6876
Size: 361864 Color: 6870
Size: 276051 Color: 2619

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 383680 Color: 7561
Size: 361712 Color: 6867
Size: 254609 Color: 659

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 361443 Color: 6859
Size: 361324 Color: 6853
Size: 277234 Color: 2716

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 399168 Color: 7990
Size: 329931 Color: 5658
Size: 270902 Color: 2197

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 375617 Color: 7307
Size: 373581 Color: 7244
Size: 250803 Color: 132

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 353046 Color: 6594
Size: 351336 Color: 6514
Size: 295619 Color: 3911

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 362718 Color: 6900
Size: 360750 Color: 6835
Size: 276533 Color: 2657

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 360661 Color: 6831
Size: 343121 Color: 6184
Size: 296219 Color: 3962

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 361438 Color: 6858
Size: 359727 Color: 6803
Size: 278836 Color: 2834

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 360485 Color: 6823
Size: 356698 Color: 6704
Size: 282818 Color: 3130

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 368054 Color: 7078
Size: 360300 Color: 6818
Size: 271647 Color: 2256

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 360288 Color: 6817
Size: 359566 Color: 6797
Size: 280147 Color: 2942

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 400787 Color: 8029
Size: 330814 Color: 5691
Size: 268400 Color: 2005

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 360102 Color: 6812
Size: 359630 Color: 6801
Size: 280269 Color: 2949

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 358951 Color: 6784
Size: 350039 Color: 6453
Size: 291011 Color: 3650

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 384600 Color: 7584
Size: 345521 Color: 6267
Size: 269880 Color: 2116

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 366791 Color: 7046
Size: 350216 Color: 6459
Size: 282994 Color: 3142

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 358492 Color: 6770
Size: 358477 Color: 6768
Size: 283032 Color: 3144

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 358351 Color: 6766
Size: 356828 Color: 6711
Size: 284822 Color: 3285

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 358254 Color: 6760
Size: 347285 Color: 6343
Size: 294462 Color: 3846

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 358093 Color: 6753
Size: 358062 Color: 6752
Size: 283846 Color: 3205

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 390001 Color: 7728
Size: 357781 Color: 6743
Size: 252219 Color: 336

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 357644 Color: 6738
Size: 357556 Color: 6736
Size: 284801 Color: 3284

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 372317 Color: 7221
Size: 350943 Color: 6499
Size: 276741 Color: 2678

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 358203 Color: 6758
Size: 355733 Color: 6677
Size: 286065 Color: 3361

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 356824 Color: 6710
Size: 356753 Color: 6707
Size: 286424 Color: 3385

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 409911 Color: 8266
Size: 331429 Color: 5709
Size: 258661 Color: 1112

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 369528 Color: 7133
Size: 356101 Color: 6689
Size: 274372 Color: 2499

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 357215 Color: 6725
Size: 355501 Color: 6670
Size: 287285 Color: 3442

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 355773 Color: 6681
Size: 355762 Color: 6679
Size: 288466 Color: 3502

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 355580 Color: 6675
Size: 355530 Color: 6672
Size: 288891 Color: 3528

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 359725 Color: 6802
Size: 351649 Color: 6532
Size: 288627 Color: 3513

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 355389 Color: 6664
Size: 355386 Color: 6663
Size: 289226 Color: 3547

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 355767 Color: 6680
Size: 355432 Color: 6666
Size: 288802 Color: 3524

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 355168 Color: 6657
Size: 345751 Color: 6274
Size: 299082 Color: 4117

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 355198 Color: 6658
Size: 354782 Color: 6649
Size: 290021 Color: 3590

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 354074 Color: 6633
Size: 354027 Color: 6629
Size: 291900 Color: 3714

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 372064 Color: 7213
Size: 335874 Color: 5890
Size: 292063 Color: 3726

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 353379 Color: 6605
Size: 351662 Color: 6533
Size: 294960 Color: 3876

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 352131 Color: 6552
Size: 351211 Color: 6509
Size: 296659 Color: 3989

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 367072 Color: 7054
Size: 351777 Color: 6539
Size: 281152 Color: 3006

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 361045 Color: 6847
Size: 342521 Color: 6157
Size: 296435 Color: 3973

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 411229 Color: 8303
Size: 333954 Color: 5818
Size: 254818 Color: 687

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 354301 Color: 6640
Size: 351373 Color: 6516
Size: 294327 Color: 3835

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 363959 Color: 6948
Size: 355737 Color: 6678
Size: 280305 Color: 2956

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 351017 Color: 6502
Size: 347846 Color: 6360
Size: 301138 Color: 4224

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 351417 Color: 6518
Size: 350684 Color: 6479
Size: 297900 Color: 4048

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 395239 Color: 7896
Size: 347753 Color: 6357
Size: 257009 Color: 946

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 374180 Color: 7266
Size: 350659 Color: 6477
Size: 275162 Color: 2551

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 350876 Color: 6494
Size: 350747 Color: 6483
Size: 298378 Color: 4078

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 350904 Color: 6495
Size: 350859 Color: 6492
Size: 298238 Color: 4069

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 350756 Color: 6485
Size: 350706 Color: 6480
Size: 298539 Color: 4088

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 350797 Color: 6488
Size: 350543 Color: 6472
Size: 298661 Color: 4096

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 357255 Color: 6729
Size: 343916 Color: 6215
Size: 298830 Color: 4104

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 352564 Color: 6572
Size: 348813 Color: 6399
Size: 298624 Color: 4090

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 353543 Color: 6612
Size: 348080 Color: 6366
Size: 298378 Color: 4077

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 352727 Color: 6578
Size: 348080 Color: 6367
Size: 299194 Color: 4125

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 352723 Color: 6577
Size: 349066 Color: 6414
Size: 298212 Color: 4067

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 353860 Color: 6622
Size: 352395 Color: 6565
Size: 293746 Color: 3806

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 353186 Color: 6599
Size: 353146 Color: 6595
Size: 293669 Color: 3804

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 358140 Color: 6756
Size: 347977 Color: 6365
Size: 293884 Color: 3812

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 370513 Color: 7160
Size: 353025 Color: 6592
Size: 276463 Color: 2650

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 351156 Color: 6505
Size: 336317 Color: 5914
Size: 312528 Color: 4841

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 399126 Color: 7987
Size: 348171 Color: 6370
Size: 252704 Color: 395

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 410383 Color: 8279
Size: 322873 Color: 5374
Size: 266745 Color: 1872

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 360568 Color: 6827
Size: 348791 Color: 6397
Size: 290642 Color: 3625

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 361772 Color: 6868
Size: 347227 Color: 6339
Size: 291002 Color: 3647

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 371837 Color: 7201
Size: 350317 Color: 6467
Size: 277847 Color: 2761

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 376297 Color: 7327
Size: 350279 Color: 6466
Size: 273425 Color: 2417

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 362527 Color: 6892
Size: 350000 Color: 6452
Size: 287474 Color: 3454

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 368332 Color: 7089
Size: 331572 Color: 5716
Size: 300097 Color: 4172

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 362731 Color: 6904
Size: 349838 Color: 6443
Size: 287432 Color: 3452

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 369309 Color: 7127
Size: 343838 Color: 6212
Size: 286854 Color: 3419

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 363785 Color: 6940
Size: 349490 Color: 6427
Size: 286726 Color: 3409

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 362871 Color: 6909
Size: 350400 Color: 6468
Size: 286730 Color: 3410

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 365295 Color: 6996
Size: 348082 Color: 6368
Size: 286624 Color: 3400

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 364787 Color: 6972
Size: 349004 Color: 6409
Size: 286210 Color: 3371

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 384745 Color: 7586
Size: 328916 Color: 5619
Size: 286340 Color: 3381

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 348988 Color: 6407
Size: 348927 Color: 6403
Size: 302086 Color: 4276

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 348875 Color: 6401
Size: 348595 Color: 6389
Size: 302531 Color: 4302

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 348810 Color: 6398
Size: 347902 Color: 6363
Size: 303289 Color: 4355

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 348887 Color: 6402
Size: 348611 Color: 6390
Size: 302503 Color: 4300

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 366380 Color: 7035
Size: 319382 Color: 5189
Size: 314239 Color: 4919

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 366618 Color: 7042
Size: 318426 Color: 5141
Size: 314957 Color: 4953

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 366678 Color: 7043
Size: 318784 Color: 5162
Size: 314539 Color: 4938

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 366912 Color: 7048
Size: 317278 Color: 5076
Size: 315811 Color: 4996

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 367042 Color: 7051
Size: 316610 Color: 5049
Size: 316349 Color: 5037

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 367056 Color: 7053
Size: 319820 Color: 5217
Size: 313125 Color: 4872

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 367094 Color: 7055
Size: 320005 Color: 5226
Size: 312902 Color: 4859

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 367124 Color: 7058
Size: 320182 Color: 5236
Size: 312695 Color: 4848

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 367146 Color: 7060
Size: 317050 Color: 5066
Size: 315805 Color: 4995

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 367300 Color: 7063
Size: 318546 Color: 5151
Size: 314155 Color: 4913

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 367381 Color: 7064
Size: 319149 Color: 5176
Size: 313471 Color: 4885

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 369139 Color: 7120
Size: 346924 Color: 6324
Size: 283938 Color: 3216

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 369358 Color: 7128
Size: 317214 Color: 5069
Size: 313429 Color: 4884

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 369484 Color: 7131
Size: 322862 Color: 5373
Size: 307655 Color: 4578

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 369520 Color: 7132
Size: 315940 Color: 5007
Size: 314541 Color: 4939

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 369693 Color: 7136
Size: 315554 Color: 4980
Size: 314754 Color: 4947

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 369718 Color: 7137
Size: 315988 Color: 5014
Size: 314295 Color: 4921

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 369737 Color: 7138
Size: 318117 Color: 5119
Size: 312147 Color: 4828

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 369740 Color: 7139
Size: 322338 Color: 5346
Size: 307923 Color: 4590

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 369897 Color: 7140
Size: 316778 Color: 5058
Size: 313326 Color: 4879

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 369905 Color: 7141
Size: 318110 Color: 5118
Size: 311986 Color: 4822

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 369908 Color: 7142
Size: 322086 Color: 5336
Size: 308007 Color: 4596

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 369931 Color: 7143
Size: 319808 Color: 5216
Size: 310262 Color: 4736

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 369938 Color: 7144
Size: 323955 Color: 5414
Size: 306108 Color: 4509

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 369989 Color: 7145
Size: 316764 Color: 5057
Size: 313248 Color: 4874

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 370124 Color: 7148
Size: 321252 Color: 5291
Size: 308625 Color: 4630

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 370159 Color: 7149
Size: 319385 Color: 5190
Size: 310457 Color: 4749

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 370162 Color: 7150
Size: 320652 Color: 5256
Size: 309187 Color: 4667

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 370188 Color: 7151
Size: 320904 Color: 5272
Size: 308909 Color: 4645

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 370272 Color: 7153
Size: 317789 Color: 5101
Size: 311940 Color: 4819

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 371053 Color: 7177
Size: 315857 Color: 4999
Size: 313091 Color: 4870

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 371060 Color: 7178
Size: 315102 Color: 4966
Size: 313839 Color: 4897

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 371108 Color: 7180
Size: 316631 Color: 5050
Size: 312262 Color: 4833

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 371157 Color: 7182
Size: 323106 Color: 5387
Size: 305738 Color: 4484

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 371184 Color: 7183
Size: 319473 Color: 5195
Size: 309344 Color: 4679

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 371207 Color: 7184
Size: 314709 Color: 4945
Size: 314085 Color: 4911

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 371235 Color: 7185
Size: 319830 Color: 5218
Size: 308936 Color: 4646

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 371267 Color: 7186
Size: 315085 Color: 4962
Size: 313649 Color: 4892

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 371276 Color: 7187
Size: 317718 Color: 5099
Size: 311007 Color: 4774

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 371327 Color: 7188
Size: 318444 Color: 5145
Size: 310230 Color: 4732

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 371477 Color: 7189
Size: 316963 Color: 5063
Size: 311561 Color: 4799

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 371497 Color: 7190
Size: 324907 Color: 5448
Size: 303597 Color: 4368

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 371500 Color: 7191
Size: 318520 Color: 5148
Size: 309981 Color: 4718

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 371516 Color: 7192
Size: 315958 Color: 5011
Size: 312527 Color: 4840

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 371522 Color: 7193
Size: 322810 Color: 5372
Size: 305669 Color: 4483

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 371903 Color: 7206
Size: 346685 Color: 6312
Size: 281413 Color: 3021

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 371920 Color: 7207
Size: 316548 Color: 5046
Size: 311533 Color: 4796

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 371971 Color: 7209
Size: 320841 Color: 5268
Size: 307189 Color: 4562

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 371979 Color: 7210
Size: 316590 Color: 5048
Size: 311432 Color: 4793

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 371995 Color: 7211
Size: 325255 Color: 5460
Size: 302751 Color: 4311

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 372035 Color: 7212
Size: 316318 Color: 5032
Size: 311648 Color: 4802

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 372341 Color: 7222
Size: 321260 Color: 5292
Size: 306400 Color: 4520

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 372342 Color: 7223
Size: 315098 Color: 4965
Size: 312561 Color: 4842

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 372378 Color: 7224
Size: 315704 Color: 4990
Size: 311919 Color: 4817

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 372697 Color: 7231
Size: 346364 Color: 6303
Size: 280940 Color: 2988

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 373085 Color: 7234
Size: 314159 Color: 4914
Size: 312757 Color: 4855

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 373117 Color: 7235
Size: 315634 Color: 4985
Size: 311250 Color: 4782

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 373122 Color: 7236
Size: 324626 Color: 5435
Size: 302253 Color: 4288

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 373165 Color: 7237
Size: 324011 Color: 5417
Size: 302825 Color: 4315

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 373195 Color: 7239
Size: 316666 Color: 5053
Size: 310140 Color: 4729

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 373351 Color: 7241
Size: 346206 Color: 6292
Size: 280444 Color: 2970

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 373938 Color: 7256
Size: 314043 Color: 4907
Size: 312020 Color: 4824

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 373944 Color: 7257
Size: 318621 Color: 5154
Size: 307436 Color: 4570

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 373961 Color: 7258
Size: 320172 Color: 5234
Size: 305868 Color: 4495

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 374002 Color: 7259
Size: 320564 Color: 5254
Size: 305435 Color: 4471

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 374145 Color: 7264
Size: 346037 Color: 6286
Size: 279819 Color: 2914

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 374147 Color: 7265
Size: 318118 Color: 5121
Size: 307736 Color: 4583

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 374323 Color: 7270
Size: 322464 Color: 5350
Size: 303214 Color: 4352

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 374559 Color: 7277
Size: 323229 Color: 5392
Size: 302213 Color: 4285

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 374566 Color: 7278
Size: 320758 Color: 5264
Size: 304677 Color: 4427

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 374590 Color: 7279
Size: 319891 Color: 5221
Size: 305520 Color: 4477

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 374611 Color: 7280
Size: 316229 Color: 5026
Size: 309161 Color: 4664

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 374613 Color: 7281
Size: 326247 Color: 5509
Size: 299141 Color: 4122

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 374627 Color: 7282
Size: 321978 Color: 5330
Size: 303396 Color: 4359

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 375126 Color: 7291
Size: 312965 Color: 4862
Size: 311910 Color: 4816

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 375448 Color: 7299
Size: 345798 Color: 6278
Size: 278755 Color: 2827

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 375471 Color: 7300
Size: 318684 Color: 5160
Size: 305846 Color: 4494

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 375523 Color: 7302
Size: 316745 Color: 5056
Size: 307733 Color: 4582

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 375529 Color: 7303
Size: 316235 Color: 5027
Size: 308237 Color: 4611

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 375560 Color: 7304
Size: 322596 Color: 5356
Size: 301845 Color: 4260

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 375566 Color: 7305
Size: 320666 Color: 5257
Size: 303769 Color: 4377

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 375598 Color: 7306
Size: 319380 Color: 5188
Size: 305023 Color: 4450

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 375664 Color: 7308
Size: 318387 Color: 5137
Size: 305950 Color: 4500

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 375678 Color: 7309
Size: 325691 Color: 5485
Size: 298632 Color: 4091

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 375926 Color: 7315
Size: 313302 Color: 4877
Size: 310773 Color: 4764

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 375967 Color: 7316
Size: 320485 Color: 5247
Size: 303549 Color: 4365

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 375992 Color: 7318
Size: 326489 Color: 5521
Size: 297520 Color: 4031

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 376012 Color: 7319
Size: 327522 Color: 5564
Size: 296467 Color: 3976

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 376063 Color: 7320
Size: 325567 Color: 5479
Size: 298371 Color: 4075

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 376128 Color: 7322
Size: 320733 Color: 5261
Size: 303140 Color: 4346

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 376142 Color: 7323
Size: 321858 Color: 5319
Size: 302001 Color: 4268

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 376171 Color: 7324
Size: 313875 Color: 4899
Size: 309955 Color: 4717

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 376188 Color: 7325
Size: 323810 Color: 5408
Size: 300003 Color: 4165

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 376362 Color: 7329
Size: 315366 Color: 4973
Size: 308273 Color: 4612

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 376370 Color: 7330
Size: 322068 Color: 5333
Size: 301563 Color: 4242

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 376381 Color: 7331
Size: 322800 Color: 5368
Size: 300820 Color: 4205

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 376403 Color: 7332
Size: 314397 Color: 4928
Size: 309201 Color: 4669

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 376422 Color: 7334
Size: 316640 Color: 5052
Size: 306939 Color: 4551

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 376462 Color: 7336
Size: 326948 Color: 5540
Size: 296591 Color: 3983

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 376511 Color: 7337
Size: 326175 Color: 5502
Size: 297315 Color: 4022

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 376526 Color: 7338
Size: 320762 Color: 5265
Size: 302713 Color: 4309

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 376627 Color: 7342
Size: 321595 Color: 5303
Size: 301779 Color: 4257

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 376633 Color: 7343
Size: 325597 Color: 5480
Size: 297771 Color: 4044

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 376726 Color: 7344
Size: 322127 Color: 5338
Size: 301148 Color: 4226

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 376750 Color: 7345
Size: 312695 Color: 4849
Size: 310556 Color: 4756

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 376888 Color: 7347
Size: 323247 Color: 5393
Size: 299866 Color: 4155

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 377128 Color: 7352
Size: 345312 Color: 6257
Size: 277561 Color: 2738

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 377208 Color: 7354
Size: 313942 Color: 4903
Size: 308851 Color: 4640

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 377221 Color: 7355
Size: 320745 Color: 5262
Size: 302035 Color: 4274

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 377277 Color: 7356
Size: 324236 Color: 5422
Size: 298488 Color: 4086

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 377360 Color: 7358
Size: 313478 Color: 4886
Size: 309163 Color: 4665

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 377439 Color: 7359
Size: 317405 Color: 5084
Size: 305157 Color: 4458

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 377457 Color: 7360
Size: 312238 Color: 4832
Size: 310306 Color: 4738

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 377488 Color: 7361
Size: 321407 Color: 5296
Size: 301106 Color: 4221

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 377643 Color: 7365
Size: 323489 Color: 5402
Size: 298869 Color: 4107

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 377666 Color: 7366
Size: 323202 Color: 5390
Size: 299133 Color: 4121

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 377677 Color: 7367
Size: 326313 Color: 5510
Size: 296011 Color: 3949

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 377701 Color: 7368
Size: 317499 Color: 5088
Size: 304801 Color: 4439

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 377739 Color: 7369
Size: 324988 Color: 5452
Size: 297274 Color: 4021

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 377768 Color: 7370
Size: 316211 Color: 5024
Size: 306022 Color: 4505

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 377769 Color: 7371
Size: 321808 Color: 5315
Size: 300424 Color: 4186

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 377794 Color: 7372
Size: 320182 Color: 5237
Size: 302025 Color: 4271

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 377856 Color: 7374
Size: 316264 Color: 5029
Size: 305881 Color: 4496

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 377986 Color: 7377
Size: 319670 Color: 5208
Size: 302345 Color: 4292

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 378043 Color: 7379
Size: 317269 Color: 5075
Size: 304689 Color: 4429

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 378093 Color: 7381
Size: 315344 Color: 4972
Size: 306564 Color: 4530

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 378288 Color: 7384
Size: 323293 Color: 5396
Size: 298420 Color: 4081

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 378325 Color: 7385
Size: 325320 Color: 5463
Size: 296356 Color: 3967

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 378363 Color: 7387
Size: 317834 Color: 5103
Size: 303804 Color: 4379

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 378381 Color: 7388
Size: 318059 Color: 5117
Size: 303561 Color: 4366

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 378383 Color: 7389
Size: 312182 Color: 4830
Size: 309436 Color: 4688

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 378386 Color: 7390
Size: 318634 Color: 5155
Size: 302981 Color: 4330

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 378420 Color: 7391
Size: 319363 Color: 5186
Size: 302218 Color: 4286

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 378449 Color: 7392
Size: 320176 Color: 5235
Size: 301376 Color: 4235

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 378451 Color: 7393
Size: 325937 Color: 5496
Size: 295613 Color: 3910

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 378541 Color: 7396
Size: 324811 Color: 5443
Size: 296649 Color: 3988

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 378548 Color: 7397
Size: 322191 Color: 5340
Size: 299262 Color: 4127

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 378792 Color: 7402
Size: 315313 Color: 4970
Size: 305896 Color: 4498

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 378797 Color: 7403
Size: 319057 Color: 5172
Size: 302147 Color: 4280

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 378840 Color: 7404
Size: 312652 Color: 4846
Size: 308509 Color: 4622

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 378857 Color: 7405
Size: 310697 Color: 4761
Size: 310447 Color: 4748

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 378869 Color: 7407
Size: 311709 Color: 4807
Size: 309423 Color: 4686

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 378917 Color: 7408
Size: 322513 Color: 5353
Size: 298571 Color: 4089

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 378941 Color: 7409
Size: 312851 Color: 4857
Size: 308209 Color: 4609

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 378981 Color: 7411
Size: 310738 Color: 4762
Size: 310282 Color: 4737

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 378983 Color: 7412
Size: 326555 Color: 5525
Size: 294463 Color: 3847

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 379174 Color: 7416
Size: 325604 Color: 5481
Size: 295223 Color: 3893

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 379206 Color: 7417
Size: 318207 Color: 5126
Size: 302588 Color: 4305

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 379252 Color: 7418
Size: 322774 Color: 5365
Size: 297975 Color: 4051

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 379256 Color: 7419
Size: 324261 Color: 5423
Size: 296484 Color: 3978

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 379264 Color: 7420
Size: 320546 Color: 5253
Size: 300191 Color: 4178

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 379274 Color: 7421
Size: 311447 Color: 4795
Size: 309280 Color: 4677

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 379277 Color: 7422
Size: 325360 Color: 5469
Size: 295364 Color: 3900

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 379294 Color: 7423
Size: 318669 Color: 5158
Size: 302038 Color: 4275

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 379318 Color: 7425
Size: 326340 Color: 5512
Size: 294343 Color: 3836

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 379357 Color: 7426
Size: 318160 Color: 5124
Size: 302484 Color: 4297

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 379459 Color: 7428
Size: 311266 Color: 4783
Size: 309276 Color: 4675

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 379490 Color: 7429
Size: 311556 Color: 4797
Size: 308955 Color: 4649

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 379596 Color: 7431
Size: 319255 Color: 5182
Size: 301150 Color: 4227

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 379702 Color: 7434
Size: 325940 Color: 5497
Size: 294359 Color: 3839

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 379706 Color: 7435
Size: 324786 Color: 5440
Size: 295509 Color: 3903

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 379813 Color: 7437
Size: 324458 Color: 5426
Size: 295730 Color: 3925

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 379857 Color: 7439
Size: 315521 Color: 4978
Size: 304623 Color: 4425

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 379874 Color: 7440
Size: 324466 Color: 5428
Size: 295661 Color: 3915

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 379887 Color: 7441
Size: 313044 Color: 4868
Size: 307070 Color: 4557

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 379893 Color: 7442
Size: 316935 Color: 5062
Size: 303173 Color: 4349

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 379938 Color: 7444
Size: 344095 Color: 6222
Size: 275968 Color: 2613

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 380037 Color: 7446
Size: 317256 Color: 5073
Size: 302708 Color: 4308

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 380066 Color: 7448
Size: 313262 Color: 4876
Size: 306673 Color: 4541

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 380075 Color: 7449
Size: 319753 Color: 5214
Size: 300173 Color: 4175

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 380094 Color: 7451
Size: 312738 Color: 4853
Size: 307169 Color: 4561

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 380132 Color: 7452
Size: 321869 Color: 5321
Size: 298000 Color: 4054

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 380146 Color: 7453
Size: 317636 Color: 5095
Size: 302219 Color: 4287

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 380164 Color: 7454
Size: 321710 Color: 5308
Size: 298127 Color: 4064

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 380217 Color: 7456
Size: 324104 Color: 5419
Size: 295680 Color: 3919

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 380264 Color: 7457
Size: 323978 Color: 5416
Size: 295759 Color: 3927

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 380424 Color: 7460
Size: 327507 Color: 5563
Size: 292070 Color: 3727

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 380596 Color: 7466
Size: 311893 Color: 4814
Size: 307512 Color: 4573

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 380619 Color: 7467
Size: 310121 Color: 4727
Size: 309261 Color: 4674

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 380684 Color: 7469
Size: 312695 Color: 4850
Size: 306622 Color: 4537

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 380706 Color: 7470
Size: 325898 Color: 5493
Size: 293397 Color: 3788

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 380721 Color: 7471
Size: 320530 Color: 5250
Size: 298750 Color: 4100

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 380899 Color: 7475
Size: 343736 Color: 6208
Size: 275366 Color: 2570

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 380947 Color: 7476
Size: 310870 Color: 4768
Size: 308184 Color: 4608

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 380975 Color: 7477
Size: 311005 Color: 4773
Size: 308021 Color: 4598

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 381066 Color: 7479
Size: 323007 Color: 5379
Size: 295928 Color: 3944

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 381105 Color: 7481
Size: 319375 Color: 5187
Size: 299521 Color: 4142

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 381122 Color: 7483
Size: 324787 Color: 5441
Size: 294092 Color: 3822

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 381168 Color: 7485
Size: 322323 Color: 5344
Size: 296510 Color: 3979

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 381170 Color: 7486
Size: 317938 Color: 5109
Size: 300893 Color: 4208

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 381174 Color: 7487
Size: 313711 Color: 4893
Size: 305116 Color: 4457

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 381192 Color: 7488
Size: 318873 Color: 5164
Size: 299936 Color: 4161

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 381308 Color: 7492
Size: 318339 Color: 5130
Size: 300354 Color: 4185

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 381339 Color: 7493
Size: 321639 Color: 5304
Size: 297023 Color: 4006

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 381350 Color: 7494
Size: 309777 Color: 4708
Size: 308874 Color: 4641

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 381369 Color: 7495
Size: 316212 Color: 5025
Size: 302420 Color: 4296

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 381402 Color: 7496
Size: 313561 Color: 4888
Size: 305038 Color: 4452

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 381411 Color: 7497
Size: 318117 Color: 5120
Size: 300473 Color: 4189

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 381474 Color: 7498
Size: 310091 Color: 4724
Size: 308436 Color: 4616

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 381483 Color: 7499
Size: 322540 Color: 5354
Size: 295978 Color: 3946

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 381529 Color: 7501
Size: 309710 Color: 4704
Size: 308762 Color: 4636

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 381630 Color: 7502
Size: 318990 Color: 5170
Size: 299381 Color: 4136

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 381633 Color: 7503
Size: 315373 Color: 4974
Size: 302995 Color: 4332

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 381694 Color: 7504
Size: 327228 Color: 5554
Size: 291079 Color: 3652

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 381742 Color: 7505
Size: 315862 Color: 5000
Size: 302397 Color: 4294

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 381778 Color: 7506
Size: 322434 Color: 5349
Size: 295789 Color: 3931

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 381790 Color: 7507
Size: 321914 Color: 5325
Size: 296297 Color: 3966

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 381797 Color: 7508
Size: 322076 Color: 5335
Size: 296128 Color: 3957

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 381816 Color: 7509
Size: 317252 Color: 5071
Size: 300933 Color: 4213

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 381825 Color: 7510
Size: 315122 Color: 4967
Size: 303054 Color: 4339

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 381836 Color: 7511
Size: 320363 Color: 5245
Size: 297802 Color: 4045

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 381870 Color: 7512
Size: 313934 Color: 4902
Size: 304197 Color: 4403

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 383064 Color: 7536
Size: 314447 Color: 4934
Size: 302490 Color: 4299

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 383087 Color: 7537
Size: 326768 Color: 5532
Size: 290146 Color: 3599

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 383101 Color: 7538
Size: 315591 Color: 4982
Size: 301309 Color: 4230

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 383124 Color: 7540
Size: 311930 Color: 4818
Size: 304947 Color: 4447

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 383193 Color: 7541
Size: 321142 Color: 5284
Size: 295666 Color: 3917

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 383216 Color: 7543
Size: 342857 Color: 6171
Size: 273928 Color: 2458

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 383327 Color: 7546
Size: 310428 Color: 4746
Size: 306246 Color: 4514

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 383366 Color: 7547
Size: 321569 Color: 5300
Size: 295066 Color: 3883

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 383409 Color: 7549
Size: 315739 Color: 4992
Size: 300853 Color: 4206

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 383420 Color: 7550
Size: 312280 Color: 4834
Size: 304301 Color: 4409

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 383429 Color: 7551
Size: 315999 Color: 5015
Size: 300573 Color: 4195

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 383459 Color: 7553
Size: 311379 Color: 4791
Size: 305163 Color: 4460

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 383567 Color: 7554
Size: 317317 Color: 5078
Size: 299117 Color: 4119

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 383569 Color: 7555
Size: 314070 Color: 4909
Size: 302362 Color: 4293

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 383602 Color: 7556
Size: 316443 Color: 5042
Size: 299956 Color: 4163

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 383618 Color: 7557
Size: 326934 Color: 5538
Size: 289449 Color: 3562

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 383621 Color: 7558
Size: 315081 Color: 4961
Size: 301299 Color: 4229

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 383634 Color: 7559
Size: 313356 Color: 4881
Size: 303011 Color: 4335

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 383660 Color: 7560
Size: 324489 Color: 5432
Size: 291852 Color: 3711

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 383806 Color: 7566
Size: 342703 Color: 6163
Size: 273492 Color: 2424

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 383931 Color: 7567
Size: 321026 Color: 5278
Size: 295044 Color: 3881

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 384068 Color: 7569
Size: 324948 Color: 5450
Size: 290985 Color: 3646

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 384649 Color: 7585
Size: 342323 Color: 6149
Size: 273029 Color: 2387

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 385269 Color: 7593
Size: 324564 Color: 5433
Size: 290168 Color: 3601

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 385313 Color: 7594
Size: 310811 Color: 4766
Size: 303877 Color: 4385

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 385315 Color: 7595
Size: 316207 Color: 5022
Size: 298479 Color: 4084

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 385330 Color: 7596
Size: 319931 Color: 5225
Size: 294740 Color: 3863

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 385334 Color: 7597
Size: 327583 Color: 5568
Size: 287084 Color: 3432

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 385572 Color: 7604
Size: 318152 Color: 5122
Size: 296277 Color: 3964

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 385644 Color: 7606
Size: 311587 Color: 4800
Size: 302770 Color: 4313

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 385661 Color: 7607
Size: 315683 Color: 4988
Size: 298657 Color: 4095

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 385671 Color: 7608
Size: 315504 Color: 4977
Size: 298826 Color: 4103

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 385683 Color: 7609
Size: 315947 Color: 5010
Size: 298371 Color: 4076

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 385710 Color: 7611
Size: 309220 Color: 4672
Size: 305071 Color: 4453

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 385718 Color: 7612
Size: 322330 Color: 5345
Size: 291953 Color: 3717

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 386052 Color: 7620
Size: 317973 Color: 5112
Size: 295976 Color: 3945

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 386063 Color: 7621
Size: 309202 Color: 4670
Size: 304736 Color: 4431

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 386072 Color: 7622
Size: 308534 Color: 4626
Size: 305395 Color: 4469

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 386076 Color: 7623
Size: 327643 Color: 5570
Size: 286282 Color: 3378

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 386226 Color: 7628
Size: 321825 Color: 5316
Size: 291950 Color: 3716

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 386235 Color: 7629
Size: 314383 Color: 4926
Size: 299383 Color: 4137

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 386570 Color: 7635
Size: 318162 Color: 5125
Size: 295269 Color: 3896

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 387131 Color: 7647
Size: 341054 Color: 6105
Size: 271816 Color: 2270

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 387306 Color: 7649
Size: 313041 Color: 4866
Size: 299654 Color: 4147

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 387320 Color: 7650
Size: 317320 Color: 5079
Size: 295361 Color: 3899

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 387322 Color: 7651
Size: 321844 Color: 5318
Size: 290835 Color: 3637

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 387322 Color: 7652
Size: 312957 Color: 4861
Size: 299722 Color: 4149

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 387493 Color: 7657
Size: 306769 Color: 4546
Size: 305739 Color: 4485

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 387508 Color: 7658
Size: 309011 Color: 4651
Size: 303482 Color: 4362

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 387509 Color: 7659
Size: 325348 Color: 5466
Size: 287144 Color: 3439

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 387610 Color: 7661
Size: 307493 Color: 4572
Size: 304898 Color: 4444

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 387768 Color: 7666
Size: 340763 Color: 6090
Size: 271470 Color: 2245

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 387782 Color: 7667
Size: 307695 Color: 4580
Size: 304524 Color: 4423

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 387858 Color: 7669
Size: 321932 Color: 5328
Size: 290211 Color: 3603

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 387875 Color: 7670
Size: 327231 Color: 5555
Size: 284895 Color: 3288

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 387957 Color: 7671
Size: 315598 Color: 4983
Size: 296446 Color: 3974

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 388097 Color: 7675
Size: 309772 Color: 4707
Size: 302132 Color: 4279

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 388163 Color: 7676
Size: 326784 Color: 5533
Size: 285054 Color: 3299

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 388236 Color: 7679
Size: 318005 Color: 5116
Size: 293760 Color: 3807

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 388356 Color: 7683
Size: 340506 Color: 6082
Size: 271139 Color: 2215

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 388373 Color: 7684
Size: 340490 Color: 6080
Size: 271138 Color: 2214

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 388398 Color: 7685
Size: 308523 Color: 4625
Size: 303080 Color: 4342

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 388475 Color: 7687
Size: 322807 Color: 5371
Size: 288719 Color: 3517

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 388476 Color: 7688
Size: 307619 Color: 4576
Size: 303906 Color: 4389

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 388479 Color: 7689
Size: 308230 Color: 4610
Size: 303292 Color: 4356

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 388480 Color: 7690
Size: 325315 Color: 5462
Size: 286206 Color: 3369

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 388491 Color: 7691
Size: 322755 Color: 5364
Size: 288755 Color: 3519

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 388583 Color: 7694
Size: 317812 Color: 5102
Size: 293606 Color: 3796

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 388633 Color: 7695
Size: 315836 Color: 4997
Size: 295532 Color: 3905

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 388674 Color: 7696
Size: 310549 Color: 4755
Size: 300778 Color: 4203

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 388755 Color: 7697
Size: 305981 Color: 4502
Size: 305265 Color: 4464

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 388762 Color: 7698
Size: 308279 Color: 4613
Size: 302960 Color: 4329

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 388766 Color: 7699
Size: 306470 Color: 4525
Size: 304765 Color: 4436

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 388770 Color: 7700
Size: 311906 Color: 4815
Size: 299325 Color: 4132

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 388842 Color: 7704
Size: 340260 Color: 6074
Size: 270899 Color: 2196

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 389025 Color: 7707
Size: 306666 Color: 4540
Size: 304310 Color: 4411

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 389066 Color: 7709
Size: 309159 Color: 4663
Size: 301776 Color: 4256

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 389067 Color: 7710
Size: 319119 Color: 5173
Size: 291815 Color: 3709

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 389799 Color: 7721
Size: 308313 Color: 4614
Size: 301889 Color: 4261

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 389804 Color: 7722
Size: 316696 Color: 5054
Size: 293501 Color: 3789

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 389838 Color: 7723
Size: 318377 Color: 5134
Size: 291786 Color: 3706

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 389934 Color: 7726
Size: 309259 Color: 4673
Size: 300808 Color: 4204

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 389954 Color: 7727
Size: 309982 Color: 4719
Size: 300065 Color: 4170

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 390018 Color: 7729
Size: 339878 Color: 6056
Size: 270105 Color: 2135

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 390143 Color: 7732
Size: 307699 Color: 4581
Size: 302159 Color: 4281

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 390203 Color: 7733
Size: 308824 Color: 4637
Size: 300974 Color: 4217

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 390210 Color: 7734
Size: 320544 Color: 5252
Size: 289247 Color: 3551

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 390241 Color: 7735
Size: 305375 Color: 4468
Size: 304385 Color: 4413

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 390382 Color: 7738
Size: 307084 Color: 4558
Size: 302535 Color: 4303

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 390383 Color: 7739
Size: 325060 Color: 5454
Size: 284558 Color: 3265

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 390614 Color: 7743
Size: 318887 Color: 5165
Size: 290500 Color: 3621

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 390627 Color: 7744
Size: 319143 Color: 5174
Size: 290231 Color: 3605

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 390646 Color: 7745
Size: 305821 Color: 4493
Size: 303534 Color: 4364

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 390668 Color: 7746
Size: 339711 Color: 6047
Size: 269622 Color: 2100

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 390700 Color: 7747
Size: 322608 Color: 5357
Size: 286693 Color: 3406

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 390704 Color: 7748
Size: 315712 Color: 4991
Size: 293585 Color: 3793

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 390715 Color: 7749
Size: 311038 Color: 4776
Size: 298248 Color: 4070

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 390725 Color: 7750
Size: 320520 Color: 5249
Size: 288756 Color: 3520

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 390744 Color: 7751
Size: 317247 Color: 5070
Size: 292010 Color: 3723

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 390756 Color: 7752
Size: 324826 Color: 5444
Size: 284419 Color: 3253

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 390777 Color: 7753
Size: 315598 Color: 4984
Size: 293626 Color: 3799

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 390792 Color: 7754
Size: 308517 Color: 4624
Size: 300692 Color: 4200

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 390872 Color: 7757
Size: 322577 Color: 5355
Size: 286552 Color: 3393

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 390886 Color: 7758
Size: 325416 Color: 5473
Size: 283699 Color: 3195

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 390896 Color: 7759
Size: 325167 Color: 5458
Size: 283938 Color: 3215

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 390923 Color: 7760
Size: 311332 Color: 4786
Size: 297746 Color: 4042

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 390963 Color: 7761
Size: 314259 Color: 4920
Size: 294779 Color: 3864

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 391091 Color: 7765
Size: 313182 Color: 4873
Size: 295728 Color: 3924

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 391096 Color: 7766
Size: 322300 Color: 5343
Size: 286605 Color: 3397

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 391123 Color: 7767
Size: 306703 Color: 4542
Size: 302175 Color: 4282

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 391146 Color: 7768
Size: 313751 Color: 4896
Size: 295104 Color: 3886

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 391148 Color: 7769
Size: 321299 Color: 5293
Size: 287554 Color: 3459

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 391610 Color: 7779
Size: 312639 Color: 4845
Size: 295752 Color: 3926

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 391764 Color: 7784
Size: 312981 Color: 4864
Size: 295256 Color: 3895

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 391770 Color: 7785
Size: 305468 Color: 4472
Size: 302763 Color: 4312

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 391811 Color: 7787
Size: 319805 Color: 5215
Size: 288385 Color: 3498

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 391850 Color: 7788
Size: 308357 Color: 4615
Size: 299794 Color: 4152

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 391874 Color: 7789
Size: 314326 Color: 4923
Size: 293801 Color: 3810

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 391971 Color: 7794
Size: 320965 Color: 5274
Size: 287065 Color: 3430

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 391984 Color: 7795
Size: 326193 Color: 5506
Size: 281824 Color: 3056

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 392020 Color: 7796
Size: 322776 Color: 5366
Size: 285205 Color: 3313

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 392052 Color: 7797
Size: 316191 Color: 5021
Size: 291758 Color: 3705

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 392056 Color: 7798
Size: 310405 Color: 4743
Size: 297540 Color: 4032

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 392066 Color: 7799
Size: 339059 Color: 6022
Size: 268876 Color: 2040

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 392070 Color: 7800
Size: 320206 Color: 5240
Size: 287725 Color: 3465

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 392328 Color: 7806
Size: 313347 Color: 4880
Size: 294326 Color: 3833

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 392359 Color: 7807
Size: 318634 Color: 5156
Size: 289008 Color: 3535

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 392365 Color: 7808
Size: 312316 Color: 4836
Size: 295320 Color: 3898

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 392392 Color: 7809
Size: 315871 Color: 5002
Size: 291738 Color: 3703

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 392423 Color: 7811
Size: 312861 Color: 4858
Size: 294717 Color: 3861

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 392450 Color: 7812
Size: 321784 Color: 5314
Size: 285767 Color: 3346

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 392455 Color: 7813
Size: 306158 Color: 4511
Size: 301388 Color: 4236

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 392456 Color: 7814
Size: 309639 Color: 4700
Size: 297906 Color: 4049

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 392489 Color: 7815
Size: 315538 Color: 4979
Size: 291974 Color: 3719

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 392495 Color: 7816
Size: 315923 Color: 5005
Size: 291583 Color: 3690

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 392578 Color: 7818
Size: 318154 Color: 5123
Size: 289269 Color: 3554

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 392677 Color: 7820
Size: 323019 Color: 5381
Size: 284305 Color: 3241

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 392685 Color: 7821
Size: 305412 Color: 4470
Size: 301904 Color: 4262

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 392738 Color: 7823
Size: 325566 Color: 5478
Size: 281697 Color: 3039

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 392746 Color: 7824
Size: 310251 Color: 4734
Size: 297004 Color: 4001

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 392834 Color: 7826
Size: 311383 Color: 4792
Size: 295784 Color: 3929

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 392881 Color: 7827
Size: 310091 Color: 4725
Size: 297029 Color: 4007

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 392882 Color: 7828
Size: 327171 Color: 5548
Size: 279948 Color: 2923

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 392915 Color: 7829
Size: 314692 Color: 4944
Size: 292394 Color: 3740

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 393001 Color: 7830
Size: 318380 Color: 5135
Size: 288620 Color: 3512

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 393024 Color: 7831
Size: 314201 Color: 4918
Size: 292776 Color: 3761

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 393031 Color: 7832
Size: 326573 Color: 5526
Size: 280397 Color: 2966

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 393037 Color: 7833
Size: 319609 Color: 5203
Size: 287355 Color: 3446

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 393067 Color: 7835
Size: 310063 Color: 4723
Size: 296871 Color: 3998

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 393075 Color: 7836
Size: 305595 Color: 4481
Size: 301331 Color: 4231

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 393211 Color: 7839
Size: 318960 Color: 5169
Size: 287830 Color: 3471

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 393277 Color: 7840
Size: 325120 Color: 5456
Size: 281604 Color: 3031

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 393396 Color: 7843
Size: 304099 Color: 4397
Size: 302506 Color: 4301

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 393596 Color: 7850
Size: 314005 Color: 4904
Size: 292400 Color: 3742

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 393603 Color: 7851
Size: 317016 Color: 5064
Size: 289382 Color: 3558

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 393666 Color: 7853
Size: 307428 Color: 4569
Size: 298907 Color: 4109

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 393694 Color: 7854
Size: 308554 Color: 4627
Size: 297753 Color: 4043

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 393716 Color: 7855
Size: 315640 Color: 4986
Size: 290645 Color: 3626

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 393719 Color: 7856
Size: 320052 Color: 5229
Size: 286230 Color: 3373

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 393721 Color: 7857
Size: 303165 Color: 4348
Size: 303115 Color: 4344

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 393790 Color: 7859
Size: 307243 Color: 4564
Size: 298968 Color: 4112

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 393898 Color: 7862
Size: 326358 Color: 5514
Size: 279745 Color: 2908

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 394041 Color: 7864
Size: 315946 Color: 5009
Size: 290014 Color: 3589

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 394552 Color: 7876
Size: 320998 Color: 5275
Size: 284451 Color: 3254

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 394590 Color: 7877
Size: 304475 Color: 4421
Size: 300936 Color: 4214

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 394600 Color: 7878
Size: 317360 Color: 5082
Size: 288041 Color: 3482

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 394625 Color: 7879
Size: 314195 Color: 4915
Size: 291181 Color: 3661

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 394770 Color: 7882
Size: 323059 Color: 5384
Size: 282172 Color: 3077

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 394932 Color: 7887
Size: 316801 Color: 5059
Size: 288268 Color: 3493

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 394981 Color: 7888
Size: 320688 Color: 5258
Size: 284332 Color: 3244

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 395120 Color: 7892
Size: 337839 Color: 5974
Size: 267042 Color: 1889

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 395184 Color: 7894
Size: 323010 Color: 5380
Size: 281807 Color: 3055

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 395362 Color: 7898
Size: 306404 Color: 4521
Size: 298235 Color: 4068

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 395364 Color: 7899
Size: 319558 Color: 5200
Size: 285079 Color: 3300

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 395424 Color: 7901
Size: 321404 Color: 5295
Size: 283173 Color: 3151

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 395513 Color: 7904
Size: 322746 Color: 5363
Size: 281742 Color: 3047

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 395606 Color: 7906
Size: 308890 Color: 4644
Size: 295505 Color: 3902

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 396144 Color: 7917
Size: 313108 Color: 4871
Size: 290749 Color: 3633

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 396150 Color: 7918
Size: 324138 Color: 5421
Size: 279713 Color: 2905

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 396217 Color: 7919
Size: 309459 Color: 4692
Size: 294325 Color: 3832

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 396263 Color: 7920
Size: 308711 Color: 4633
Size: 295027 Color: 3879

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 396348 Color: 7921
Size: 309218 Color: 4671
Size: 294435 Color: 3843

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 396394 Color: 7922
Size: 324878 Color: 5446
Size: 278729 Color: 2825

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 396446 Color: 7923
Size: 325761 Color: 5487
Size: 277794 Color: 2758

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 396464 Color: 7924
Size: 309922 Color: 4716
Size: 293615 Color: 3798

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 396547 Color: 7925
Size: 315306 Color: 4969
Size: 288148 Color: 3487

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 396663 Color: 7926
Size: 307491 Color: 4571
Size: 295847 Color: 3938

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 396677 Color: 7927
Size: 313027 Color: 4865
Size: 290297 Color: 3608

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 396786 Color: 7928
Size: 317913 Color: 5107
Size: 285302 Color: 3320

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 396798 Color: 7929
Size: 303021 Color: 4336
Size: 300182 Color: 4177

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 396822 Color: 7930
Size: 303858 Color: 4383
Size: 299321 Color: 4130

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 396849 Color: 7931
Size: 319534 Color: 5198
Size: 283618 Color: 3186

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 396866 Color: 7932
Size: 306068 Color: 4508
Size: 297067 Color: 4010

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 396879 Color: 7933
Size: 305487 Color: 4473
Size: 297635 Color: 4035

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 396897 Color: 7934
Size: 305934 Color: 4499
Size: 297170 Color: 4016

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 396999 Color: 7935
Size: 318638 Color: 5157
Size: 284364 Color: 3246

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 397032 Color: 7936
Size: 306579 Color: 4533
Size: 296390 Color: 3970

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 397064 Color: 7937
Size: 323356 Color: 5397
Size: 279581 Color: 2891

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 397082 Color: 7938
Size: 323363 Color: 5398
Size: 279556 Color: 2888

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 397111 Color: 7939
Size: 311004 Color: 4772
Size: 291886 Color: 3713

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 397265 Color: 7940
Size: 311060 Color: 4777
Size: 291676 Color: 3697

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 397438 Color: 7941
Size: 309603 Color: 4698
Size: 292960 Color: 3770

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 397447 Color: 7942
Size: 322070 Color: 5334
Size: 280484 Color: 2971

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 397468 Color: 7943
Size: 325417 Color: 5474
Size: 277116 Color: 2707

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 397499 Color: 7944
Size: 312638 Color: 4844
Size: 289864 Color: 3581

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 397518 Color: 7945
Size: 307234 Color: 4563
Size: 295249 Color: 3894

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 397525 Color: 7946
Size: 323442 Color: 5400
Size: 279034 Color: 2851

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 397535 Color: 7947
Size: 325610 Color: 5482
Size: 276856 Color: 2684

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 397545 Color: 7948
Size: 311350 Color: 4787
Size: 291106 Color: 3655

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 397623 Color: 7949
Size: 327172 Color: 5549
Size: 275206 Color: 2556

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 397704 Color: 7950
Size: 305502 Color: 4474
Size: 296795 Color: 3995

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 397705 Color: 7951
Size: 304265 Color: 4408
Size: 298031 Color: 4056

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 397739 Color: 7952
Size: 302008 Color: 4269
Size: 300254 Color: 4181

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 397744 Color: 7953
Size: 327059 Color: 5542
Size: 275198 Color: 2554

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 397757 Color: 7954
Size: 301970 Color: 4267
Size: 300274 Color: 4183

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 397802 Color: 7955
Size: 314461 Color: 4935
Size: 287738 Color: 3466

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 397847 Color: 7956
Size: 302995 Color: 4333
Size: 299159 Color: 4123

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 397891 Color: 7957
Size: 303997 Color: 4393
Size: 298113 Color: 4061

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 397931 Color: 7958
Size: 306025 Color: 4506
Size: 296045 Color: 3951

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 397958 Color: 7959
Size: 305781 Color: 4488
Size: 296262 Color: 3963

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 398019 Color: 7960
Size: 326184 Color: 5504
Size: 275798 Color: 2598

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 398077 Color: 7961
Size: 324862 Color: 5445
Size: 277062 Color: 2702

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 398115 Color: 7962
Size: 323207 Color: 5391
Size: 278679 Color: 2822

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 398119 Color: 7963
Size: 319501 Color: 5197
Size: 282381 Color: 3101

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 398258 Color: 7964
Size: 316138 Color: 5017
Size: 285605 Color: 3333

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 398260 Color: 7965
Size: 307557 Color: 4574
Size: 294184 Color: 3825

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 398305 Color: 7966
Size: 325634 Color: 5483
Size: 276062 Color: 2620

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 398350 Color: 7967
Size: 317633 Color: 5094
Size: 284018 Color: 3222

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 398385 Color: 7968
Size: 316455 Color: 5043
Size: 285161 Color: 3309

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 398440 Color: 7969
Size: 317715 Color: 5098
Size: 283846 Color: 3206

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 398475 Color: 7970
Size: 308477 Color: 4619
Size: 293049 Color: 3774

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 398476 Color: 7971
Size: 319231 Color: 5180
Size: 282294 Color: 3096

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 412463 Color: 8334
Size: 310408 Color: 4745
Size: 277130 Color: 2708

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 412508 Color: 8335
Size: 297678 Color: 4037
Size: 289815 Color: 3579

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 412628 Color: 8336
Size: 335071 Color: 5849
Size: 252302 Color: 344

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 412680 Color: 8337
Size: 323849 Color: 5409
Size: 263472 Color: 1586

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 412734 Color: 8338
Size: 319909 Color: 5224
Size: 267358 Color: 1920

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 412781 Color: 8339
Size: 304682 Color: 4428
Size: 282538 Color: 3114

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 412813 Color: 8340
Size: 318398 Color: 5138
Size: 268790 Color: 2031

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 412814 Color: 8341
Size: 329293 Color: 5633
Size: 257894 Color: 1028

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 412823 Color: 8342
Size: 317323 Color: 5081
Size: 269855 Color: 2115

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 412835 Color: 8343
Size: 293605 Color: 3795
Size: 293561 Color: 3792

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 412862 Color: 8344
Size: 295624 Color: 3912
Size: 291515 Color: 3686

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 412976 Color: 8345
Size: 332482 Color: 5757
Size: 254543 Color: 649

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 412981 Color: 8346
Size: 316734 Color: 5055
Size: 270286 Color: 2145

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 413015 Color: 8347
Size: 327724 Color: 5573
Size: 259262 Color: 1177

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 413020 Color: 8348
Size: 294454 Color: 3844
Size: 292527 Color: 3750

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 413028 Color: 8349
Size: 311848 Color: 4812
Size: 275125 Color: 2549

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 413070 Color: 8350
Size: 329400 Color: 5638
Size: 257531 Color: 992

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 413149 Color: 8351
Size: 321669 Color: 5305
Size: 265183 Color: 1736

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 413187 Color: 8352
Size: 310621 Color: 4759
Size: 276193 Color: 2630

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 413245 Color: 8353
Size: 308577 Color: 4628
Size: 278179 Color: 2785

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 413248 Color: 8354
Size: 328126 Color: 5591
Size: 258627 Color: 1104

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 413255 Color: 8355
Size: 330848 Color: 5692
Size: 255898 Color: 816

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 413257 Color: 8356
Size: 320021 Color: 5228
Size: 266723 Color: 1870

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 413295 Color: 8357
Size: 329520 Color: 5641
Size: 257186 Color: 966

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 413315 Color: 8358
Size: 325748 Color: 5486
Size: 260938 Color: 1352

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 413376 Color: 8359
Size: 324751 Color: 5439
Size: 261874 Color: 1443

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 413406 Color: 8360
Size: 334678 Color: 5842
Size: 251917 Color: 298

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 413413 Color: 8361
Size: 326704 Color: 5528
Size: 259884 Color: 1237

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 413418 Color: 8362
Size: 324462 Color: 5427
Size: 262121 Color: 1455

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 413465 Color: 8364
Size: 323516 Color: 5403
Size: 263020 Color: 1534

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 413465 Color: 8363
Size: 318383 Color: 5136
Size: 268153 Color: 1989

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 413510 Color: 8365
Size: 330436 Color: 5676
Size: 256055 Color: 830

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 413517 Color: 8366
Size: 321015 Color: 5277
Size: 265469 Color: 1761

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 413522 Color: 8367
Size: 325041 Color: 5453
Size: 261438 Color: 1402

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 413545 Color: 8368
Size: 302844 Color: 4317
Size: 283612 Color: 3185

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 413566 Color: 8369
Size: 323023 Color: 5382
Size: 263412 Color: 1576

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 413569 Color: 8370
Size: 321186 Color: 5286
Size: 265246 Color: 1741

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 413570 Color: 8371
Size: 309405 Color: 4683
Size: 277026 Color: 2698

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 413612 Color: 8372
Size: 331445 Color: 5710
Size: 254944 Color: 705

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 413680 Color: 8373
Size: 326938 Color: 5539
Size: 259383 Color: 1187

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 413746 Color: 8374
Size: 306954 Color: 4552
Size: 279301 Color: 2870

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 413807 Color: 8375
Size: 293390 Color: 3786
Size: 292804 Color: 3763

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 413869 Color: 8376
Size: 295661 Color: 3916
Size: 290471 Color: 3619

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 413895 Color: 8377
Size: 294544 Color: 3851
Size: 291562 Color: 3688

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 413938 Color: 8378
Size: 317629 Color: 5093
Size: 268434 Color: 2009

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 413971 Color: 8379
Size: 310862 Color: 4767
Size: 275168 Color: 2552

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 414032 Color: 8380
Size: 296481 Color: 3977
Size: 289488 Color: 3564

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 414051 Color: 8381
Size: 308087 Color: 4604
Size: 277863 Color: 2762

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 414097 Color: 8382
Size: 309419 Color: 4685
Size: 276485 Color: 2653

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 414185 Color: 8383
Size: 335370 Color: 5865
Size: 250446 Color: 73

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 414199 Color: 8384
Size: 305816 Color: 4492
Size: 279986 Color: 2926

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 414289 Color: 8385
Size: 321920 Color: 5326
Size: 263792 Color: 1609

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 414502 Color: 8386
Size: 312966 Color: 4863
Size: 272533 Color: 2339

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 414730 Color: 8387
Size: 314315 Color: 4922
Size: 270956 Color: 2202

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 414768 Color: 8388
Size: 329207 Color: 5628
Size: 256026 Color: 827

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 414886 Color: 8389
Size: 309818 Color: 4712
Size: 275297 Color: 2566

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 414955 Color: 8390
Size: 298470 Color: 4083
Size: 286576 Color: 3395

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 414975 Color: 8391
Size: 301550 Color: 4240
Size: 283476 Color: 3173

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 414992 Color: 8392
Size: 299017 Color: 4114
Size: 285992 Color: 3356

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 415097 Color: 8393
Size: 321679 Color: 5306
Size: 263225 Color: 1558

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 415131 Color: 8394
Size: 316287 Color: 5031
Size: 268583 Color: 2017

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 415199 Color: 8395
Size: 316538 Color: 5044
Size: 268264 Color: 1997

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 415208 Color: 8396
Size: 319314 Color: 5184
Size: 265479 Color: 1762

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 415302 Color: 8397
Size: 330881 Color: 5694
Size: 253818 Color: 547

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 415308 Color: 8398
Size: 332504 Color: 5760
Size: 252189 Color: 331

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 415311 Color: 8399
Size: 306592 Color: 4534
Size: 278098 Color: 2782

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 415373 Color: 8400
Size: 309081 Color: 4654
Size: 275547 Color: 2578

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 415432 Color: 8401
Size: 325859 Color: 5492
Size: 258710 Color: 1118

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 415481 Color: 8402
Size: 304873 Color: 4443
Size: 279647 Color: 2900

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 415508 Color: 8403
Size: 322501 Color: 5351
Size: 261992 Color: 1449

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 415798 Color: 8404
Size: 294161 Color: 3824
Size: 290042 Color: 3591

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 415804 Color: 8405
Size: 293871 Color: 3811
Size: 290326 Color: 3610

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 415934 Color: 8406
Size: 316548 Color: 5045
Size: 267519 Color: 1940

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 415985 Color: 8407
Size: 330015 Color: 5660
Size: 254001 Color: 573

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 416036 Color: 8408
Size: 310488 Color: 4752
Size: 273477 Color: 2421

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 416049 Color: 8409
Size: 309103 Color: 4656
Size: 274849 Color: 2526

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 416076 Color: 8410
Size: 305083 Color: 4454
Size: 278842 Color: 2835

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 416078 Color: 8411
Size: 319423 Color: 5193
Size: 264500 Color: 1680

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 416091 Color: 8412
Size: 311948 Color: 4820
Size: 271962 Color: 2288

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 416168 Color: 8413
Size: 301620 Color: 4244
Size: 282213 Color: 3087

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 416288 Color: 8414
Size: 295007 Color: 3878
Size: 288706 Color: 3516

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 416349 Color: 8415
Size: 329226 Color: 5630
Size: 254426 Color: 636

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 416362 Color: 8416
Size: 303196 Color: 4351
Size: 280443 Color: 2969

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 416442 Color: 8417
Size: 294596 Color: 3854
Size: 288963 Color: 3532

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 416447 Color: 8418
Size: 306397 Color: 4519
Size: 277157 Color: 2711

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 416451 Color: 8419
Size: 310802 Color: 4765
Size: 272748 Color: 2360

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 416455 Color: 8420
Size: 295894 Color: 3941
Size: 287652 Color: 3463

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 416471 Color: 8421
Size: 317736 Color: 5100
Size: 265794 Color: 1787

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 416526 Color: 8422
Size: 298119 Color: 4062
Size: 285356 Color: 3322

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 416740 Color: 8423
Size: 292742 Color: 3759
Size: 290519 Color: 3622

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 416769 Color: 8424
Size: 325392 Color: 5472
Size: 257840 Color: 1021

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 416779 Color: 8425
Size: 310035 Color: 4721
Size: 273187 Color: 2401

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 416817 Color: 8426
Size: 324487 Color: 5431
Size: 258697 Color: 1116

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 416898 Color: 8427
Size: 293716 Color: 3805
Size: 289387 Color: 3559

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 416958 Color: 8428
Size: 308142 Color: 4605
Size: 274901 Color: 2531

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 416966 Color: 8429
Size: 310914 Color: 4770
Size: 272121 Color: 2303

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 416975 Color: 8430
Size: 329842 Color: 5653
Size: 253184 Color: 464

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 417001 Color: 8431
Size: 296202 Color: 3960
Size: 286798 Color: 3416

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 417166 Color: 8432
Size: 310250 Color: 4733
Size: 272585 Color: 2347

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 417187 Color: 8433
Size: 319653 Color: 5206
Size: 263161 Color: 1550

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 417211 Color: 8434
Size: 299428 Color: 4139
Size: 283362 Color: 3168

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 417244 Color: 8435
Size: 321883 Color: 5323
Size: 260874 Color: 1347

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 417246 Color: 8436
Size: 323790 Color: 5407
Size: 258965 Color: 1151

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 417291 Color: 8437
Size: 327814 Color: 5576
Size: 254896 Color: 698

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 417319 Color: 8438
Size: 325916 Color: 5495
Size: 256766 Color: 916

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 417336 Color: 8439
Size: 332598 Color: 5765
Size: 250067 Color: 10

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 417378 Color: 8440
Size: 312098 Color: 4827
Size: 270525 Color: 2168

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 417390 Color: 8441
Size: 298037 Color: 4057
Size: 284574 Color: 3268

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 417396 Color: 8442
Size: 308731 Color: 4635
Size: 273874 Color: 2454

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 417399 Color: 8443
Size: 303897 Color: 4388
Size: 278705 Color: 2823

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 417431 Color: 8444
Size: 328377 Color: 5605
Size: 254193 Color: 607

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 417683 Color: 8445
Size: 291646 Color: 3693
Size: 290672 Color: 3627

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 417738 Color: 8446
Size: 292614 Color: 3754
Size: 289649 Color: 3572

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 417782 Color: 8447
Size: 322642 Color: 5358
Size: 259577 Color: 1209

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 417804 Color: 8448
Size: 307775 Color: 4586
Size: 274422 Color: 2504

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 417809 Color: 8449
Size: 303871 Color: 4384
Size: 278321 Color: 2792

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 417906 Color: 8450
Size: 300878 Color: 4207
Size: 281217 Color: 3008

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 417919 Color: 8451
Size: 313855 Color: 4898
Size: 268227 Color: 1994

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 417964 Color: 8452
Size: 303179 Color: 4350
Size: 278858 Color: 2836

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 418004 Color: 8453
Size: 322804 Color: 5370
Size: 259193 Color: 1166

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 418114 Color: 8454
Size: 295219 Color: 3892
Size: 286668 Color: 3403

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 418207 Color: 8455
Size: 326803 Color: 5535
Size: 254991 Color: 709

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 418278 Color: 8456
Size: 318340 Color: 5131
Size: 263383 Color: 1573

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 418299 Color: 8457
Size: 317256 Color: 5072
Size: 264446 Color: 1676

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 418303 Color: 8458
Size: 306751 Color: 4545
Size: 274947 Color: 2538

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 418312 Color: 8459
Size: 305792 Color: 4489
Size: 275897 Color: 2605

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 418375 Color: 8460
Size: 326727 Color: 5530
Size: 254899 Color: 699

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 418395 Color: 8461
Size: 294992 Color: 3877
Size: 286614 Color: 3398

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 418430 Color: 8462
Size: 321329 Color: 5294
Size: 260242 Color: 1279

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 418464 Color: 8463
Size: 321080 Color: 5279
Size: 260457 Color: 1306

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 418485 Color: 8464
Size: 315091 Color: 4963
Size: 266425 Color: 1848

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 418534 Color: 8465
Size: 306224 Color: 4513
Size: 275243 Color: 2561

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 418540 Color: 8466
Size: 298964 Color: 4111
Size: 282497 Color: 3111

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 418670 Color: 8467
Size: 295209 Color: 3891
Size: 286122 Color: 3362

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 418675 Color: 8468
Size: 299007 Color: 4113
Size: 282319 Color: 3098

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 418690 Color: 8469
Size: 318511 Color: 5146
Size: 262800 Color: 1507

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 418713 Color: 8470
Size: 317566 Color: 5090
Size: 263722 Color: 1606

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 418868 Color: 8471
Size: 313065 Color: 4869
Size: 268068 Color: 1981

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 418889 Color: 8472
Size: 291571 Color: 3689
Size: 289541 Color: 3567

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 418923 Color: 8473
Size: 292905 Color: 3767
Size: 288173 Color: 3490

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 418989 Color: 8474
Size: 291809 Color: 3708
Size: 289203 Color: 3545

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 419015 Color: 8475
Size: 314402 Color: 4930
Size: 266584 Color: 1861

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 419017 Color: 8476
Size: 297694 Color: 4038
Size: 283290 Color: 3161

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 419019 Color: 8477
Size: 310443 Color: 4747
Size: 270539 Color: 2170

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 419032 Color: 8478
Size: 326493 Color: 5522
Size: 254476 Color: 642

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 419129 Color: 8479
Size: 317308 Color: 5077
Size: 263564 Color: 1593

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 419202 Color: 8480
Size: 294353 Color: 3837
Size: 286446 Color: 3386

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 419259 Color: 8481
Size: 296861 Color: 3997
Size: 283881 Color: 3209

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 419300 Color: 8482
Size: 304959 Color: 4448
Size: 275742 Color: 2591

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 419316 Color: 8483
Size: 307932 Color: 4591
Size: 272753 Color: 2362

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 419321 Color: 8484
Size: 319579 Color: 5201
Size: 261101 Color: 1371

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 419349 Color: 8485
Size: 301562 Color: 4241
Size: 279090 Color: 2858

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 419387 Color: 8486
Size: 302635 Color: 4306
Size: 277979 Color: 2774

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 419395 Color: 8487
Size: 328986 Color: 5623
Size: 251620 Color: 263

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 419492 Color: 8488
Size: 306260 Color: 4515
Size: 274249 Color: 2488

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 419500 Color: 8489
Size: 314007 Color: 4905
Size: 266494 Color: 1854

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 419520 Color: 8490
Size: 312624 Color: 4843
Size: 267857 Color: 1966

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 419581 Color: 8491
Size: 326389 Color: 5515
Size: 254031 Color: 580

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 419769 Color: 8492
Size: 327026 Color: 5541
Size: 253206 Color: 469

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 419787 Color: 8493
Size: 296707 Color: 3991
Size: 283507 Color: 3174

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 419844 Color: 8494
Size: 298111 Color: 4060
Size: 282046 Color: 3074

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 419847 Color: 8495
Size: 326649 Color: 5527
Size: 253505 Color: 504

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 419909 Color: 8496
Size: 311355 Color: 4788
Size: 268737 Color: 2025

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 419992 Color: 8497
Size: 326205 Color: 5507
Size: 253804 Color: 545

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 420027 Color: 8498
Size: 314375 Color: 4925
Size: 265599 Color: 1775

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 420050 Color: 8499
Size: 296610 Color: 3986
Size: 283341 Color: 3165

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 420148 Color: 8500
Size: 304942 Color: 4445
Size: 274911 Color: 2534

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 420254 Color: 8501
Size: 302176 Color: 4283
Size: 277571 Color: 2741

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 420301 Color: 8502
Size: 306901 Color: 4550
Size: 272799 Color: 2369

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 420305 Color: 8503
Size: 326550 Color: 5524
Size: 253146 Color: 459

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 420317 Color: 8504
Size: 305753 Color: 4486
Size: 273931 Color: 2459

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 420325 Color: 8505
Size: 292666 Color: 3756
Size: 287010 Color: 3425

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 420340 Color: 8506
Size: 308014 Color: 4597
Size: 271647 Color: 2255

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 420368 Color: 8507
Size: 298167 Color: 4065
Size: 281466 Color: 3024

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 420417 Color: 8508
Size: 290600 Color: 3624
Size: 288984 Color: 3533

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 420426 Color: 8509
Size: 316635 Color: 5051
Size: 262940 Color: 1521

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 420473 Color: 8510
Size: 325271 Color: 5461
Size: 254257 Color: 618

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 420527 Color: 8511
Size: 306616 Color: 4536
Size: 272858 Color: 2371

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 420533 Color: 8512
Size: 290203 Color: 3602
Size: 289265 Color: 3553

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 420572 Color: 8513
Size: 322196 Color: 5341
Size: 257233 Color: 972

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 420631 Color: 8514
Size: 327275 Color: 5556
Size: 252095 Color: 320

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 420698 Color: 8515
Size: 310258 Color: 4735
Size: 269045 Color: 2061

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 420702 Color: 8516
Size: 301933 Color: 4264
Size: 277366 Color: 2725

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 420759 Color: 8517
Size: 319535 Color: 5199
Size: 259707 Color: 1219

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 420824 Color: 8518
Size: 294943 Color: 3875
Size: 284234 Color: 3235

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 420846 Color: 8519
Size: 320723 Color: 5260
Size: 258432 Color: 1092

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 420955 Color: 8520
Size: 319401 Color: 5192
Size: 259645 Color: 1214

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 421043 Color: 8521
Size: 302095 Color: 4278
Size: 276863 Color: 2685

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 421051 Color: 8522
Size: 302568 Color: 4304
Size: 276382 Color: 2644

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 421104 Color: 8523
Size: 325480 Color: 5475
Size: 253417 Color: 497

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 421164 Color: 8524
Size: 309123 Color: 4660
Size: 269714 Color: 2106

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 421250 Color: 8525
Size: 307168 Color: 4560
Size: 271583 Color: 2251

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 421302 Color: 8526
Size: 309670 Color: 4703
Size: 269029 Color: 2059

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 421328 Color: 8527
Size: 322511 Color: 5352
Size: 256162 Color: 848

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 421354 Color: 8528
Size: 300175 Color: 4176
Size: 278472 Color: 2808

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 421376 Color: 8529
Size: 315674 Color: 4987
Size: 262951 Color: 1523

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 421424 Color: 8530
Size: 306781 Color: 4547
Size: 271796 Color: 2267

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 421504 Color: 8531
Size: 316423 Color: 5041
Size: 262074 Color: 1451

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 421523 Color: 8532
Size: 291632 Color: 3692
Size: 286846 Color: 3418

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 421532 Color: 8533
Size: 297965 Color: 4050
Size: 280504 Color: 2974

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 421541 Color: 8534
Size: 324386 Color: 5424
Size: 254074 Color: 586

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 421615 Color: 8535
Size: 307578 Color: 4575
Size: 270808 Color: 2187

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 421616 Color: 8536
Size: 295916 Color: 3943
Size: 282469 Color: 3109

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 421733 Color: 8537
Size: 299947 Color: 4162
Size: 278321 Color: 2793

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 421744 Color: 8538
Size: 310333 Color: 4740
Size: 267924 Color: 1969

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 421755 Color: 8539
Size: 328076 Color: 5586
Size: 250170 Color: 32

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 421758 Color: 8540
Size: 296291 Color: 3965
Size: 281952 Color: 3066

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 421760 Color: 8541
Size: 292401 Color: 3743
Size: 285840 Color: 3351

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 421762 Color: 8542
Size: 313620 Color: 4889
Size: 264619 Color: 1686

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 421768 Color: 8543
Size: 290402 Color: 3616
Size: 287831 Color: 3472

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 421810 Color: 8544
Size: 319606 Color: 5202
Size: 258585 Color: 1103

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 421820 Color: 8545
Size: 304154 Color: 4400
Size: 274027 Color: 2468

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 421842 Color: 8546
Size: 315394 Color: 4976
Size: 262765 Color: 1503

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 421893 Color: 8547
Size: 325376 Color: 5470
Size: 252732 Color: 399

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 421954 Color: 8548
Size: 321124 Color: 5282
Size: 256923 Color: 940

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 421957 Color: 8549
Size: 302019 Color: 4270
Size: 276025 Color: 2616

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 422013 Color: 8550
Size: 316246 Color: 5028
Size: 261742 Color: 1426

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 422014 Color: 8551
Size: 320845 Color: 5269
Size: 257142 Color: 961

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 422023 Color: 8552
Size: 318314 Color: 5129
Size: 259664 Color: 1216

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 422030 Color: 8553
Size: 291296 Color: 3668
Size: 286675 Color: 3404

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 422084 Color: 8554
Size: 291683 Color: 3699
Size: 286234 Color: 3374

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 422122 Color: 8555
Size: 326473 Color: 5519
Size: 251406 Color: 238

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 422197 Color: 8556
Size: 296681 Color: 3990
Size: 281123 Color: 3003

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 422244 Color: 8557
Size: 294220 Color: 3829
Size: 283537 Color: 3177

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 422266 Color: 8558
Size: 304491 Color: 4422
Size: 273244 Color: 2408

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 422331 Color: 8559
Size: 291005 Color: 3648
Size: 286665 Color: 3401

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 422403 Color: 8560
Size: 308064 Color: 4601
Size: 269534 Color: 2094

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 422418 Color: 8561
Size: 317577 Color: 5092
Size: 260006 Color: 1252

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 422501 Color: 8562
Size: 290755 Color: 3635
Size: 286745 Color: 3412

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 422525 Color: 8563
Size: 293218 Color: 3782
Size: 284258 Color: 3237

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 422627 Color: 8564
Size: 315797 Color: 4994
Size: 261577 Color: 1414

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 422768 Color: 8565
Size: 326188 Color: 5505
Size: 251045 Color: 184

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 422773 Color: 8566
Size: 322690 Color: 5360
Size: 254538 Color: 648

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 422828 Color: 8567
Size: 314628 Color: 4941
Size: 262545 Color: 1481

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 422884 Color: 8568
Size: 303028 Color: 4337
Size: 274089 Color: 2473

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 422910 Color: 8569
Size: 307933 Color: 4592
Size: 269158 Color: 2071

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 422932 Color: 8570
Size: 306337 Color: 4517
Size: 270732 Color: 2180

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 423044 Color: 8571
Size: 314417 Color: 4932
Size: 262540 Color: 1480

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 423045 Color: 8572
Size: 295991 Color: 3948
Size: 280965 Color: 2989

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 423066 Color: 8573
Size: 317511 Color: 5089
Size: 259424 Color: 1191

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 423078 Color: 8574
Size: 310537 Color: 4754
Size: 266386 Color: 1840

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 423104 Color: 8575
Size: 289959 Color: 3586
Size: 286938 Color: 3423

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 423236 Color: 8576
Size: 320358 Color: 5244
Size: 256407 Color: 878

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 423266 Color: 8577
Size: 314807 Color: 4949
Size: 261928 Color: 1445

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 423302 Color: 8578
Size: 304213 Color: 4404
Size: 272486 Color: 2335

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 423352 Color: 8579
Size: 321690 Color: 5307
Size: 254959 Color: 707

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 423352 Color: 8580
Size: 294460 Color: 3845
Size: 282189 Color: 3080

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 423375 Color: 8581
Size: 317052 Color: 5067
Size: 259574 Color: 1208

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 423414 Color: 8582
Size: 306749 Color: 4544
Size: 269838 Color: 2114

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 423437 Color: 8583
Size: 314751 Color: 4946
Size: 261813 Color: 1436

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 423437 Color: 8584
Size: 325837 Color: 5489
Size: 250727 Color: 119

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 423463 Color: 8585
Size: 310315 Color: 4739
Size: 266223 Color: 1823

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 423503 Color: 8586
Size: 322784 Color: 5367
Size: 253714 Color: 531

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 423513 Color: 8587
Size: 295027 Color: 3880
Size: 281461 Color: 3023

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 423527 Color: 8588
Size: 291726 Color: 3701
Size: 284748 Color: 3279

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 423609 Color: 8589
Size: 297109 Color: 4014
Size: 279283 Color: 2869

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 423636 Color: 8590
Size: 318429 Color: 5142
Size: 257936 Color: 1032

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 423727 Color: 8591
Size: 309326 Color: 4678
Size: 266948 Color: 1884

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 423765 Color: 8592
Size: 298683 Color: 4098
Size: 277553 Color: 2736

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 423791 Color: 8593
Size: 311220 Color: 4781
Size: 264990 Color: 1722

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 423792 Color: 8594
Size: 303764 Color: 4376
Size: 272445 Color: 2332

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 423862 Color: 8595
Size: 325487 Color: 5476
Size: 250652 Color: 104

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 423887 Color: 8596
Size: 309031 Color: 4653
Size: 267083 Color: 1895

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 423889 Color: 8597
Size: 295539 Color: 3906
Size: 280573 Color: 2975

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 423899 Color: 8598
Size: 321749 Color: 5311
Size: 254353 Color: 627

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 423920 Color: 8599
Size: 304195 Color: 4402
Size: 271886 Color: 2281

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 424070 Color: 8600
Size: 312326 Color: 4837
Size: 263605 Color: 1597

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 424078 Color: 8601
Size: 304557 Color: 4424
Size: 271366 Color: 2236

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 424142 Color: 8602
Size: 301727 Color: 4252
Size: 274132 Color: 2477

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 424182 Color: 8603
Size: 301719 Color: 4250
Size: 274100 Color: 2475

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 424200 Color: 8604
Size: 302025 Color: 4272
Size: 273776 Color: 2444

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 424203 Color: 8605
Size: 292136 Color: 3729
Size: 283662 Color: 3191

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 424212 Color: 8606
Size: 303897 Color: 4387
Size: 271892 Color: 2282

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 424347 Color: 8607
Size: 308503 Color: 4621
Size: 267151 Color: 1902

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 424357 Color: 8608
Size: 294631 Color: 3856
Size: 281013 Color: 2993

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 424498 Color: 8609
Size: 296529 Color: 3980
Size: 278974 Color: 2845

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 424561 Color: 8610
Size: 316564 Color: 5047
Size: 258876 Color: 1140

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 424587 Color: 8611
Size: 322414 Color: 5348
Size: 253000 Color: 439

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 424601 Color: 8612
Size: 301829 Color: 4259
Size: 273571 Color: 2430

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 424620 Color: 8613
Size: 315908 Color: 5004
Size: 259473 Color: 1195

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 424668 Color: 8614
Size: 317907 Color: 5106
Size: 257426 Color: 981

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 424690 Color: 8615
Size: 298015 Color: 4055
Size: 277296 Color: 2721

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 424699 Color: 8616
Size: 319180 Color: 5179
Size: 256122 Color: 840

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 424707 Color: 8617
Size: 318910 Color: 5166
Size: 256384 Color: 874

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 424882 Color: 8618
Size: 314046 Color: 4908
Size: 261073 Color: 1368

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 424918 Color: 8619
Size: 314990 Color: 4956
Size: 260093 Color: 1265

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 424939 Color: 8620
Size: 321176 Color: 5285
Size: 253886 Color: 556

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 424990 Color: 8621
Size: 308578 Color: 4629
Size: 266433 Color: 1849

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 425022 Color: 8622
Size: 306494 Color: 4528
Size: 268485 Color: 2012

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 425028 Color: 8623
Size: 319146 Color: 5175
Size: 255827 Color: 807

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 425070 Color: 8624
Size: 321207 Color: 5287
Size: 253724 Color: 534

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 425186 Color: 8625
Size: 318678 Color: 5159
Size: 256137 Color: 841

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 425315 Color: 8626
Size: 307737 Color: 4584
Size: 266949 Color: 1885

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 425349 Color: 8627
Size: 290940 Color: 3644
Size: 283712 Color: 3196

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 425376 Color: 8628
Size: 296709 Color: 3992
Size: 277916 Color: 2769

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 425388 Color: 8629
Size: 319627 Color: 5205
Size: 254986 Color: 708

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 425396 Color: 8630
Size: 291482 Color: 3682
Size: 283123 Color: 3148

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 425428 Color: 8631
Size: 311104 Color: 4779
Size: 263469 Color: 1584

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 425442 Color: 8632
Size: 294579 Color: 3852
Size: 279980 Color: 2925

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 425497 Color: 8633
Size: 295832 Color: 3936
Size: 278672 Color: 2821

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 425502 Color: 8634
Size: 290870 Color: 3641
Size: 283629 Color: 3188

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 425516 Color: 8635
Size: 306477 Color: 4526
Size: 268008 Color: 1975

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 425525 Color: 8636
Size: 313746 Color: 4895
Size: 260730 Color: 1332

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 425541 Color: 8637
Size: 320484 Color: 5246
Size: 253976 Color: 571

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 425557 Color: 8638
Size: 290903 Color: 3642
Size: 283541 Color: 3178

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 425607 Color: 8639
Size: 320145 Color: 5233
Size: 254249 Color: 615

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 425648 Color: 8640
Size: 309109 Color: 4657
Size: 265244 Color: 1739

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 425706 Color: 8641
Size: 295683 Color: 3920
Size: 278612 Color: 2817

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 425844 Color: 8642
Size: 303877 Color: 4386
Size: 270280 Color: 2144

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 425893 Color: 8643
Size: 306817 Color: 4549
Size: 267291 Color: 1914

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 425918 Color: 8644
Size: 291836 Color: 3710
Size: 282247 Color: 3092

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 425918 Color: 8645
Size: 321013 Color: 5276
Size: 253070 Color: 448

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 425926 Color: 8646
Size: 310476 Color: 4750
Size: 263599 Color: 1596

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 425931 Color: 8647
Size: 309380 Color: 4680
Size: 264690 Color: 1695

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 425942 Color: 8648
Size: 287579 Color: 3460
Size: 286480 Color: 3388

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 426069 Color: 8649
Size: 309874 Color: 4714
Size: 264058 Color: 1637

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 426080 Color: 8650
Size: 300684 Color: 4199
Size: 273237 Color: 2407

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 426192 Color: 8651
Size: 321935 Color: 5329
Size: 251874 Color: 293

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 426222 Color: 8652
Size: 323748 Color: 5406
Size: 250031 Color: 5

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 426271 Color: 8653
Size: 305236 Color: 4461
Size: 268494 Color: 2013

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 426312 Color: 8654
Size: 320814 Color: 5267
Size: 252875 Color: 419

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 426401 Color: 8655
Size: 305252 Color: 4463
Size: 268348 Color: 2001

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 426506 Color: 8656
Size: 322025 Color: 5331
Size: 251470 Color: 243

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 426521 Color: 8657
Size: 310536 Color: 4753
Size: 262944 Color: 1522

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 426555 Color: 8658
Size: 309154 Color: 4662
Size: 264292 Color: 1658

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 426570 Color: 8659
Size: 321094 Color: 5280
Size: 252337 Color: 349

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 426576 Color: 8660
Size: 296452 Color: 3975
Size: 276973 Color: 2696

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 426579 Color: 8661
Size: 322801 Color: 5369
Size: 250621 Color: 100

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 426608 Color: 8662
Size: 312658 Color: 4847
Size: 260735 Color: 1334

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 426715 Color: 8663
Size: 321776 Color: 5312
Size: 251510 Color: 249

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 426743 Color: 8664
Size: 319350 Color: 5185
Size: 253908 Color: 559

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 426765 Color: 8665
Size: 291262 Color: 3666
Size: 281974 Color: 3071

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 426774 Color: 8666
Size: 290780 Color: 3636
Size: 282447 Color: 3108

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 426827 Color: 8667
Size: 321909 Color: 5324
Size: 251265 Color: 213

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 426828 Color: 8668
Size: 315341 Color: 4971
Size: 257832 Color: 1020

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 426880 Color: 8669
Size: 298645 Color: 4093
Size: 274476 Color: 2511

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 426924 Color: 8670
Size: 299924 Color: 4159
Size: 273153 Color: 2399

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 426926 Color: 8671
Size: 298534 Color: 4087
Size: 274541 Color: 2514

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 426954 Color: 8672
Size: 286557 Color: 3394
Size: 286490 Color: 3389

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 427057 Color: 8673
Size: 313395 Color: 4882
Size: 259549 Color: 1205

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 427128 Color: 8674
Size: 303130 Color: 4345
Size: 269743 Color: 2108

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 427148 Color: 8675
Size: 314899 Color: 4951
Size: 257954 Color: 1033

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 427219 Color: 8676
Size: 294881 Color: 3869
Size: 277901 Color: 2766

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 427410 Color: 8677
Size: 305304 Color: 4467
Size: 267287 Color: 1913

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 427414 Color: 8678
Size: 320006 Color: 5227
Size: 252581 Color: 380

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 427441 Color: 8679
Size: 309577 Color: 4697
Size: 262983 Color: 1529

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 427451 Color: 8680
Size: 306126 Color: 4510
Size: 266424 Color: 1847

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 427499 Color: 8681
Size: 318608 Color: 5153
Size: 253894 Color: 557

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 427500 Color: 8682
Size: 315072 Color: 4959
Size: 257429 Color: 982

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 427505 Color: 8683
Size: 291993 Color: 3721
Size: 280503 Color: 2973

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 427560 Color: 8684
Size: 291425 Color: 3679
Size: 281016 Color: 2994

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 427627 Color: 8685
Size: 301293 Color: 4228
Size: 271081 Color: 2211

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 427634 Color: 8686
Size: 290157 Color: 3600
Size: 282210 Color: 3086

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 427635 Color: 8687
Size: 291079 Color: 3653
Size: 281287 Color: 3013

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 427650 Color: 8688
Size: 312430 Color: 4839
Size: 259921 Color: 1244

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 427735 Color: 8689
Size: 300906 Color: 4210
Size: 271360 Color: 2235

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 427762 Color: 8690
Size: 311361 Color: 4789
Size: 260878 Color: 1348

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 427770 Color: 8691
Size: 317860 Color: 5105
Size: 254371 Color: 629

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 427836 Color: 8692
Size: 297121 Color: 4015
Size: 275044 Color: 2543

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 427858 Color: 8693
Size: 302090 Color: 4277
Size: 270053 Color: 2125

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 427960 Color: 8694
Size: 286930 Color: 3422
Size: 285111 Color: 3304

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 428000 Color: 8695
Size: 319903 Color: 5222
Size: 252098 Color: 321

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 428228 Color: 8696
Size: 290049 Color: 3592
Size: 281724 Color: 3044

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 428284 Color: 8697
Size: 286678 Color: 3405
Size: 285039 Color: 3298

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 428557 Color: 8698
Size: 308682 Color: 4632
Size: 262762 Color: 1502

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 428707 Color: 8699
Size: 314501 Color: 4937
Size: 256793 Color: 920

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 428939 Color: 8700
Size: 294504 Color: 3850
Size: 276558 Color: 2663

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 428993 Color: 8701
Size: 285983 Color: 3354
Size: 285025 Color: 3297

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 428995 Color: 8702
Size: 310627 Color: 4760
Size: 260379 Color: 1299

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 429059 Color: 8703
Size: 298954 Color: 4110
Size: 271988 Color: 2292

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 429060 Color: 8705
Size: 314664 Color: 4942
Size: 256277 Color: 859

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 429060 Color: 8704
Size: 319042 Color: 5171
Size: 251899 Color: 297

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 429077 Color: 8706
Size: 293628 Color: 3800
Size: 277296 Color: 2722

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 429129 Color: 8707
Size: 300672 Color: 4198
Size: 270200 Color: 2138

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 429160 Color: 8708
Size: 300022 Color: 4166
Size: 270819 Color: 2188

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 429247 Color: 8709
Size: 299059 Color: 4116
Size: 271695 Color: 2258

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 429322 Color: 8710
Size: 301686 Color: 4248
Size: 268993 Color: 2053

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 429371 Color: 8711
Size: 315976 Color: 5012
Size: 254654 Color: 665

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 429422 Color: 8712
Size: 300612 Color: 4196
Size: 269967 Color: 2122

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 429456 Color: 8713
Size: 301944 Color: 4265
Size: 268601 Color: 2018

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 429496 Color: 8714
Size: 311949 Color: 4821
Size: 258556 Color: 1100

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 429497 Color: 8715
Size: 294327 Color: 3834
Size: 276177 Color: 2629

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 429526 Color: 8716
Size: 302929 Color: 4325
Size: 267546 Color: 1941

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 429543 Color: 8717
Size: 307808 Color: 4589
Size: 262650 Color: 1490

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 429629 Color: 8718
Size: 302897 Color: 4321
Size: 267475 Color: 1935

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 429642 Color: 8719
Size: 287298 Color: 3444
Size: 283061 Color: 3145

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 429792 Color: 8720
Size: 287791 Color: 3469
Size: 282418 Color: 3105

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 429834 Color: 8721
Size: 288503 Color: 3503
Size: 281664 Color: 3036

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 430030 Color: 8722
Size: 313896 Color: 4900
Size: 256075 Color: 836

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 430081 Color: 8723
Size: 309647 Color: 4701
Size: 260273 Color: 1283

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 430120 Color: 8724
Size: 299866 Color: 4156
Size: 270015 Color: 2124

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 430182 Color: 8725
Size: 304257 Color: 4407
Size: 265562 Color: 1768

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 430200 Color: 8726
Size: 293885 Color: 3813
Size: 275916 Color: 2606

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 430234 Color: 8727
Size: 286342 Color: 3382
Size: 283425 Color: 3170

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 430235 Color: 8728
Size: 309445 Color: 4691
Size: 260321 Color: 1291

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 430247 Color: 8729
Size: 300511 Color: 4191
Size: 269243 Color: 2077

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 430291 Color: 8730
Size: 310594 Color: 4757
Size: 259116 Color: 1158

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 430348 Color: 8731
Size: 308479 Color: 4620
Size: 261174 Color: 1377

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 430399 Color: 8732
Size: 289329 Color: 3556
Size: 280273 Color: 2951

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 430446 Color: 8733
Size: 285143 Color: 3307
Size: 284412 Color: 3252

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 430482 Color: 8734
Size: 304811 Color: 4440
Size: 264708 Color: 1696

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 430516 Color: 8735
Size: 311700 Color: 4806
Size: 257785 Color: 1015

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 430626 Color: 8736
Size: 299823 Color: 4153
Size: 269552 Color: 2096

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 430630 Color: 8737
Size: 319166 Color: 5178
Size: 250205 Color: 42

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 430646 Color: 8738
Size: 316366 Color: 5038
Size: 252989 Color: 437

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 430658 Color: 8739
Size: 317999 Color: 5115
Size: 251344 Color: 229

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 430685 Color: 8740
Size: 294893 Color: 3873
Size: 274423 Color: 2505

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 430712 Color: 8741
Size: 302949 Color: 4328
Size: 266340 Color: 1836

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 430738 Color: 8742
Size: 288147 Color: 3486
Size: 281116 Color: 3002

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 430766 Color: 8743
Size: 287299 Color: 3445
Size: 281936 Color: 3063

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 430804 Color: 8744
Size: 309093 Color: 4655
Size: 260104 Color: 1267

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 430813 Color: 8745
Size: 290082 Color: 3595
Size: 279106 Color: 2860

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 430815 Color: 8746
Size: 310487 Color: 4751
Size: 258699 Color: 1117

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 430823 Color: 8747
Size: 287062 Color: 3429
Size: 282116 Color: 3075

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 430852 Color: 8748
Size: 316163 Color: 5020
Size: 252986 Color: 436

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 430899 Color: 8749
Size: 315069 Color: 4958
Size: 254033 Color: 582

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 430922 Color: 8750
Size: 307934 Color: 4594
Size: 261145 Color: 1375

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 430939 Color: 8751
Size: 309439 Color: 4690
Size: 259623 Color: 1212

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 430967 Color: 8752
Size: 287979 Color: 3479
Size: 281055 Color: 2998

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 430984 Color: 8753
Size: 284632 Color: 3273
Size: 284385 Color: 3248

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 431053 Color: 8754
Size: 313621 Color: 4891
Size: 255327 Color: 748

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 431069 Color: 8755
Size: 291253 Color: 3665
Size: 277679 Color: 2749

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 431105 Color: 8756
Size: 299083 Color: 4118
Size: 269813 Color: 2112

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 431110 Color: 8757
Size: 292627 Color: 3755
Size: 276264 Color: 2634

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 431127 Color: 8758
Size: 303294 Color: 4357
Size: 265580 Color: 1773

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 431130 Color: 8759
Size: 301766 Color: 4255
Size: 267105 Color: 1898

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 431244 Color: 8760
Size: 312176 Color: 4829
Size: 256581 Color: 895

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 431257 Color: 8761
Size: 302878 Color: 4319
Size: 265866 Color: 1792

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 431266 Color: 8762
Size: 299127 Color: 4120
Size: 269608 Color: 2099

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 431319 Color: 8763
Size: 310138 Color: 4728
Size: 258544 Color: 1099

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 431357 Color: 8764
Size: 303002 Color: 4334
Size: 265642 Color: 1777

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 431386 Color: 8765
Size: 313261 Color: 4875
Size: 255354 Color: 753

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 431465 Color: 8766
Size: 304712 Color: 4430
Size: 263824 Color: 1611

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 431482 Color: 8767
Size: 291995 Color: 3722
Size: 276524 Color: 2656

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 431519 Color: 8768
Size: 297065 Color: 4009
Size: 271417 Color: 2240

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 431544 Color: 8769
Size: 293023 Color: 3773
Size: 275434 Color: 2573

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 431581 Color: 8770
Size: 285422 Color: 3327
Size: 282998 Color: 3143

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 431603 Color: 8771
Size: 312732 Color: 4852
Size: 255666 Color: 792

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 431635 Color: 8772
Size: 300616 Color: 4197
Size: 267750 Color: 1960

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 431704 Color: 8773
Size: 307308 Color: 4566
Size: 260989 Color: 1360

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 431734 Color: 8774
Size: 308454 Color: 4618
Size: 259813 Color: 1230

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 431788 Color: 8775
Size: 285102 Color: 3302
Size: 283111 Color: 3146

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 431798 Color: 8776
Size: 314092 Color: 4912
Size: 254111 Color: 591

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 431808 Color: 8777
Size: 302944 Color: 4326
Size: 265249 Color: 1742

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 431875 Color: 8778
Size: 314944 Color: 4952
Size: 253182 Color: 463

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 431876 Color: 8779
Size: 317473 Color: 5086
Size: 250652 Color: 105

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 431938 Color: 8780
Size: 317574 Color: 5091
Size: 250489 Color: 80

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 431984 Color: 8781
Size: 314691 Color: 4943
Size: 253326 Color: 487

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 432005 Color: 8782
Size: 309437 Color: 4689
Size: 258559 Color: 1101

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 432083 Color: 8783
Size: 303144 Color: 4347
Size: 264774 Color: 1699

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 432106 Color: 8784
Size: 303823 Color: 4381
Size: 264072 Color: 1640

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 432165 Color: 8785
Size: 306422 Color: 4522
Size: 261414 Color: 1400

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 432172 Color: 8786
Size: 291228 Color: 3664
Size: 276601 Color: 2667

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 432233 Color: 8787
Size: 303717 Color: 4374
Size: 264051 Color: 1636

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 432239 Color: 8788
Size: 317090 Color: 5068
Size: 250672 Color: 107

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 432248 Color: 8789
Size: 301816 Color: 4258
Size: 265937 Color: 1798

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 432249 Color: 8790
Size: 312700 Color: 4851
Size: 255052 Color: 717

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 432398 Color: 8791
Size: 299315 Color: 4129
Size: 268288 Color: 2000

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 432399 Color: 8792
Size: 300215 Color: 4180
Size: 267387 Color: 1925

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 432437 Color: 8793
Size: 284922 Color: 3291
Size: 282642 Color: 3119

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 432457 Color: 8794
Size: 291680 Color: 3698
Size: 275864 Color: 2603

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 432467 Color: 8795
Size: 309659 Color: 4702
Size: 257875 Color: 1026

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 432481 Color: 8796
Size: 291047 Color: 3651
Size: 276473 Color: 2652

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 432507 Color: 8797
Size: 301594 Color: 4243
Size: 265900 Color: 1796

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 432536 Color: 8798
Size: 304468 Color: 4420
Size: 262997 Color: 1532

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 432545 Color: 8799
Size: 300901 Color: 4209
Size: 266555 Color: 1859

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 432574 Color: 8800
Size: 283733 Color: 3200
Size: 283694 Color: 3193

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 432586 Color: 8801
Size: 292485 Color: 3746
Size: 274930 Color: 2537

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 432679 Color: 8802
Size: 301138 Color: 4223
Size: 266184 Color: 1819

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 432732 Color: 8803
Size: 287231 Color: 3441
Size: 280038 Color: 2934

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 432733 Color: 8804
Size: 308941 Color: 4647
Size: 258327 Color: 1081

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 432874 Color: 8805
Size: 295310 Color: 3897
Size: 271817 Color: 2271

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 432893 Color: 8806
Size: 296390 Color: 3971
Size: 270718 Color: 2179

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 432898 Color: 8807
Size: 314873 Color: 4950
Size: 252230 Color: 338

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 432902 Color: 8808
Size: 289730 Color: 3575
Size: 277369 Color: 2726

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 432999 Color: 8809
Size: 292832 Color: 3765
Size: 274170 Color: 2480

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 433014 Color: 8810
Size: 294862 Color: 3867
Size: 272125 Color: 2305

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 433058 Color: 8811
Size: 292480 Color: 3745
Size: 274463 Color: 2509

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 433120 Color: 8812
Size: 284698 Color: 3277
Size: 282183 Color: 3078

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 433186 Color: 8813
Size: 292368 Color: 3739
Size: 274447 Color: 2508

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 433239 Color: 8814
Size: 286588 Color: 3396
Size: 280174 Color: 2945

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 433263 Color: 8815
Size: 297086 Color: 4012
Size: 269652 Color: 2102

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 433316 Color: 8816
Size: 299018 Color: 4115
Size: 267667 Color: 1949

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 433321 Color: 8817
Size: 286409 Color: 3383
Size: 280271 Color: 2950

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 433328 Color: 8818
Size: 301632 Color: 4245
Size: 265041 Color: 1724

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 433356 Color: 8819
Size: 289246 Color: 3550
Size: 277399 Color: 2728

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 433437 Color: 8820
Size: 286245 Color: 3375
Size: 280319 Color: 2957

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 433444 Color: 8821
Size: 289605 Color: 3569
Size: 276952 Color: 2695

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 433464 Color: 8822
Size: 285546 Color: 3331
Size: 280991 Color: 2992

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 433569 Color: 8823
Size: 300447 Color: 4188
Size: 265985 Color: 1802

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 433584 Color: 8824
Size: 304437 Color: 4417
Size: 261980 Color: 1448

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 433604 Color: 8825
Size: 305988 Color: 4503
Size: 260409 Color: 1300

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 433740 Color: 8826
Size: 288616 Color: 3511
Size: 277645 Color: 2746

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 433748 Color: 8827
Size: 285674 Color: 3339
Size: 280579 Color: 2976

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 433753 Color: 8828
Size: 301750 Color: 4254
Size: 264498 Color: 1679

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 433768 Color: 8829
Size: 294883 Color: 3870
Size: 271350 Color: 2234

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 433864 Color: 8830
Size: 315097 Color: 4964
Size: 251040 Color: 182

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 433952 Color: 8831
Size: 283713 Color: 3197
Size: 282336 Color: 3099

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 433966 Color: 8832
Size: 297246 Color: 4019
Size: 268789 Color: 2030

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 434018 Color: 8833
Size: 297077 Color: 4011
Size: 268906 Color: 2046

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 434038 Color: 8834
Size: 284058 Color: 3223
Size: 281905 Color: 3061

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 434056 Color: 8835
Size: 307807 Color: 4588
Size: 258138 Color: 1052

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 434104 Color: 8836
Size: 311266 Color: 4784
Size: 254631 Color: 661

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 434111 Color: 8837
Size: 295785 Color: 3930
Size: 270105 Color: 2134

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 434124 Color: 8838
Size: 304055 Color: 4395
Size: 261822 Color: 1437

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 434192 Color: 8839
Size: 303100 Color: 4343
Size: 262709 Color: 1497

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 434303 Color: 8840
Size: 304000 Color: 4394
Size: 261698 Color: 1423

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 434336 Color: 8841
Size: 309380 Color: 4681
Size: 256285 Color: 860

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 434343 Color: 8842
Size: 314348 Color: 4924
Size: 251310 Color: 222

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 434408 Color: 8843
Size: 298860 Color: 4106
Size: 266733 Color: 1871

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 434411 Color: 8844
Size: 312739 Color: 4854
Size: 252851 Color: 417

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 434439 Color: 8845
Size: 288430 Color: 3501
Size: 277132 Color: 2709

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 434474 Color: 8846
Size: 309017 Color: 4652
Size: 256510 Color: 886

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 434509 Color: 8847
Size: 296018 Color: 3950
Size: 269474 Color: 2089

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 434549 Color: 8848
Size: 294819 Color: 3865
Size: 270633 Color: 2176

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 434565 Color: 8849
Size: 293612 Color: 3797
Size: 271824 Color: 2272

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 434681 Color: 8850
Size: 299205 Color: 4126
Size: 266115 Color: 1812

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 434757 Color: 8851
Size: 284824 Color: 3286
Size: 280420 Color: 2967

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 434764 Color: 8852
Size: 286143 Color: 3364
Size: 279094 Color: 2859

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 434770 Color: 8853
Size: 287393 Color: 3450
Size: 277838 Color: 2760

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 434855 Color: 8854
Size: 286530 Color: 3391
Size: 278616 Color: 2818

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 434862 Color: 8855
Size: 295809 Color: 3933
Size: 269330 Color: 2082

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 434884 Color: 8856
Size: 287365 Color: 3448
Size: 277752 Color: 2755

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 434946 Color: 8857
Size: 295774 Color: 3928
Size: 269281 Color: 2079

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 434960 Color: 8858
Size: 313718 Color: 4894
Size: 251323 Color: 225

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 434994 Color: 8859
Size: 287100 Color: 3433
Size: 277907 Color: 2767

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 435070 Color: 8860
Size: 309424 Color: 4687
Size: 255507 Color: 768

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 435107 Color: 8861
Size: 304168 Color: 4401
Size: 260726 Color: 1331

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 435114 Color: 8863
Size: 299597 Color: 4144
Size: 265290 Color: 1747

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 435114 Color: 8862
Size: 307678 Color: 4579
Size: 257209 Color: 969

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 435148 Color: 8864
Size: 285102 Color: 3301
Size: 279751 Color: 2909

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 435296 Color: 8865
Size: 282755 Color: 3125
Size: 281950 Color: 3065

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 435382 Color: 8866
Size: 303590 Color: 4367
Size: 261029 Color: 1364

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 435383 Color: 8867
Size: 303664 Color: 4371
Size: 260954 Color: 1353

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 435520 Color: 8868
Size: 293970 Color: 3819
Size: 270511 Color: 2165

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 435522 Color: 8869
Size: 301723 Color: 4251
Size: 262756 Color: 1500

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 435531 Color: 8870
Size: 290864 Color: 3640
Size: 273606 Color: 2433

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 435550 Color: 8871
Size: 284513 Color: 3260
Size: 279938 Color: 2921

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 435574 Color: 8872
Size: 299183 Color: 4124
Size: 265244 Color: 1740

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 435617 Color: 8873
Size: 282868 Color: 3132
Size: 281516 Color: 3026

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 435665 Color: 8874
Size: 292214 Color: 3733
Size: 272122 Color: 2304

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 435728 Color: 8875
Size: 298203 Color: 4066
Size: 266070 Color: 1808

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 435766 Color: 8876
Size: 307363 Color: 4568
Size: 256872 Color: 933

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 435780 Color: 8877
Size: 285359 Color: 3323
Size: 278862 Color: 2837

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 435798 Color: 8878
Size: 282246 Color: 3091
Size: 281957 Color: 3067

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 435831 Color: 8879
Size: 289963 Color: 3587
Size: 274207 Color: 2484

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 436052 Color: 8880
Size: 311068 Color: 4778
Size: 252881 Color: 422

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 436056 Color: 8881
Size: 291389 Color: 3675
Size: 272556 Color: 2345

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 436129 Color: 8882
Size: 290539 Color: 3623
Size: 273333 Color: 2413

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 436163 Color: 8883
Size: 293360 Color: 3784
Size: 270478 Color: 2161

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 436179 Color: 8884
Size: 309491 Color: 4695
Size: 254331 Color: 623

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 436221 Color: 8885
Size: 307039 Color: 4556
Size: 256741 Color: 911

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 436286 Color: 8886
Size: 285471 Color: 3329
Size: 278244 Color: 2790

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 436310 Color: 8887
Size: 293139 Color: 3777
Size: 270552 Color: 2171

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 436364 Color: 8888
Size: 297013 Color: 4004
Size: 266624 Color: 1864

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 436370 Color: 8889
Size: 287497 Color: 3456
Size: 276134 Color: 2624

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 436375 Color: 8890
Size: 299733 Color: 4150
Size: 263893 Color: 1619

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 436413 Color: 8891
Size: 312025 Color: 4825
Size: 251563 Color: 254

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 436422 Color: 8892
Size: 307283 Color: 4565
Size: 256296 Color: 863

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 436435 Color: 8893
Size: 304834 Color: 4442
Size: 258732 Color: 1122

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 436448 Color: 8894
Size: 310119 Color: 4726
Size: 253434 Color: 498

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 436468 Color: 8895
Size: 295817 Color: 3934
Size: 267716 Color: 1956

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 436492 Color: 8896
Size: 284318 Color: 3243
Size: 279191 Color: 2866

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 436568 Color: 8897
Size: 297009 Color: 4003
Size: 266424 Color: 1846

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 436691 Color: 8898
Size: 289240 Color: 3549
Size: 274070 Color: 2472

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 436697 Color: 8899
Size: 291298 Color: 3669
Size: 272006 Color: 2294

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 436715 Color: 8900
Size: 284379 Color: 3247
Size: 278907 Color: 2842

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 436752 Color: 8901
Size: 295581 Color: 3908
Size: 267668 Color: 1950

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 436902 Color: 8902
Size: 296408 Color: 3972
Size: 266691 Color: 1868

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 436932 Color: 8903
Size: 308847 Color: 4638
Size: 254222 Color: 612

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 436952 Color: 8904
Size: 289582 Color: 3568
Size: 273467 Color: 2420

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 436960 Color: 8905
Size: 311650 Color: 4804
Size: 251391 Color: 236

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 436981 Color: 8906
Size: 291732 Color: 3702
Size: 271288 Color: 2231

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 436994 Color: 8907
Size: 297444 Color: 4026
Size: 265563 Color: 1769

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 437019 Color: 8908
Size: 303076 Color: 4341
Size: 259906 Color: 1243

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 437019 Color: 8909
Size: 308959 Color: 4650
Size: 254023 Color: 577

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 437131 Color: 8910
Size: 284475 Color: 3258
Size: 278395 Color: 2798

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 437235 Color: 8911
Size: 291751 Color: 3704
Size: 271015 Color: 2205

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 437284 Color: 8912
Size: 281922 Color: 3062
Size: 280795 Color: 2984

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 437335 Color: 8913
Size: 300915 Color: 4211
Size: 261751 Color: 1428

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 437356 Color: 8914
Size: 302032 Color: 4273
Size: 260613 Color: 1323

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 437397 Color: 8915
Size: 300073 Color: 4171
Size: 262531 Color: 1478

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 437421 Color: 8916
Size: 290381 Color: 3615
Size: 272199 Color: 2312

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 437441 Color: 8917
Size: 311690 Color: 4805
Size: 250870 Color: 153

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 437443 Color: 8918
Size: 283582 Color: 3181
Size: 278976 Color: 2846

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 437452 Color: 8919
Size: 311297 Color: 4785
Size: 251252 Color: 211

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 437470 Color: 8920
Size: 299277 Color: 4128
Size: 263254 Color: 1563

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 437522 Color: 8921
Size: 288241 Color: 3492
Size: 274238 Color: 2487

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 437565 Color: 8922
Size: 284548 Color: 3264
Size: 277888 Color: 2764

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 437603 Color: 8923
Size: 306015 Color: 4504
Size: 256383 Color: 872

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 437622 Color: 8924
Size: 282358 Color: 3100
Size: 280021 Color: 2932

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 437666 Color: 8925
Size: 296605 Color: 3985
Size: 265730 Color: 1782

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 437679 Color: 8926
Size: 299460 Color: 4141
Size: 262862 Color: 1511

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 437691 Color: 8927
Size: 297257 Color: 4020
Size: 265053 Color: 1725

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 437802 Color: 8928
Size: 288394 Color: 3499
Size: 273805 Color: 2447

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 437901 Color: 8929
Size: 309189 Color: 4668
Size: 252911 Color: 427

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 437908 Color: 8930
Size: 300533 Color: 4194
Size: 261560 Color: 1410

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 437941 Color: 8931
Size: 309118 Color: 4659
Size: 252942 Color: 433

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 438019 Color: 8932
Size: 295702 Color: 3921
Size: 266280 Color: 1828

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 438027 Color: 8933
Size: 292186 Color: 3732
Size: 269788 Color: 2111

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 438054 Color: 8934
Size: 297705 Color: 4039
Size: 264242 Color: 1655

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 438059 Color: 8935
Size: 303043 Color: 4338
Size: 258899 Color: 1142

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 438091 Color: 8936
Size: 289140 Color: 3537
Size: 272770 Color: 2365

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 438191 Color: 8937
Size: 307622 Color: 4577
Size: 254188 Color: 606

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 438298 Color: 8938
Size: 307017 Color: 4554
Size: 254686 Color: 670

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 438376 Color: 8939
Size: 291393 Color: 3676
Size: 270232 Color: 2141

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 438377 Color: 8940
Size: 284677 Color: 3276
Size: 276947 Color: 2694

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 438407 Color: 8941
Size: 300145 Color: 4173
Size: 261449 Color: 1404

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 438418 Color: 8942
Size: 308727 Color: 4634
Size: 252856 Color: 418

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 438440 Color: 8943
Size: 295611 Color: 3909
Size: 265950 Color: 1800

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 438450 Color: 8944
Size: 301129 Color: 4222
Size: 260422 Color: 1303

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 438562 Color: 8945
Size: 288809 Color: 3526
Size: 272630 Color: 2352

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 438574 Color: 8946
Size: 305270 Color: 4465
Size: 256157 Color: 845

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 438657 Color: 8947
Size: 291597 Color: 3691
Size: 269747 Color: 2109

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 438680 Color: 8948
Size: 297509 Color: 4030
Size: 263812 Color: 1610

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 438702 Color: 8949
Size: 303741 Color: 4375
Size: 257558 Color: 996

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 438840 Color: 8950
Size: 308035 Color: 4599
Size: 253126 Color: 456

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 438939 Color: 8951
Size: 301685 Color: 4247
Size: 259377 Color: 1186

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 438985 Color: 8952
Size: 292172 Color: 3731
Size: 268844 Color: 2038

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 439148 Color: 8953
Size: 289400 Color: 3560
Size: 271453 Color: 2242

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 439182 Color: 8954
Size: 309612 Color: 4699
Size: 251207 Color: 203

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 439189 Color: 8955
Size: 285162 Color: 3310
Size: 275650 Color: 2585

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 439257 Color: 8956
Size: 281677 Color: 3038
Size: 279067 Color: 2857

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 439267 Color: 8957
Size: 284261 Color: 3238
Size: 276473 Color: 2651

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 439341 Color: 8958
Size: 289745 Color: 3576
Size: 270915 Color: 2198

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 439391 Color: 8959
Size: 305584 Color: 4480
Size: 255026 Color: 714

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 439392 Color: 8960
Size: 305602 Color: 4482
Size: 255007 Color: 712

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 439401 Color: 8961
Size: 304787 Color: 4438
Size: 255813 Color: 804

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 439423 Color: 8962
Size: 294637 Color: 3857
Size: 265941 Color: 1799

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 439429 Color: 8963
Size: 303668 Color: 4372
Size: 256904 Color: 936

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 439492 Color: 8964
Size: 303458 Color: 4360
Size: 257051 Color: 954

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 439518 Color: 8965
Size: 284659 Color: 3275
Size: 275824 Color: 2600

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 439570 Color: 8966
Size: 302279 Color: 4289
Size: 258152 Color: 1056

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 439729 Color: 8967
Size: 281149 Color: 3005
Size: 279123 Color: 2861

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 439731 Color: 8968
Size: 289936 Color: 3585
Size: 270334 Color: 2152

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 439860 Color: 8969
Size: 302894 Color: 4320
Size: 257247 Color: 973

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 439939 Color: 8970
Size: 283307 Color: 3164
Size: 276755 Color: 2679

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 439974 Color: 8971
Size: 290270 Color: 3607
Size: 269757 Color: 2110

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 440042 Color: 8972
Size: 294885 Color: 3871
Size: 265074 Color: 1726

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 440069 Color: 8973
Size: 281383 Color: 3016
Size: 278549 Color: 2815

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 440084 Color: 8974
Size: 295883 Color: 3940
Size: 264034 Color: 1633

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 440154 Color: 8975
Size: 289754 Color: 3577
Size: 270093 Color: 2130

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 440174 Color: 8976
Size: 300957 Color: 4216
Size: 258870 Color: 1139

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 440226 Color: 8977
Size: 295123 Color: 3887
Size: 264652 Color: 1692

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 440239 Color: 8978
Size: 282194 Color: 3083
Size: 277568 Color: 2740

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 440266 Color: 8979
Size: 288567 Color: 3506
Size: 271168 Color: 2222

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 440315 Color: 8980
Size: 281699 Color: 3041
Size: 277987 Color: 2775

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 440359 Color: 8981
Size: 308516 Color: 4623
Size: 251126 Color: 196

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 440416 Color: 8982
Size: 282734 Color: 3123
Size: 276851 Color: 2683

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 440419 Color: 8983
Size: 285207 Color: 3314
Size: 274375 Color: 2501

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 440425 Color: 8984
Size: 309464 Color: 4693
Size: 250112 Color: 16

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 440513 Color: 8985
Size: 304744 Color: 4434
Size: 254744 Color: 677

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 440525 Color: 8986
Size: 284185 Color: 3231
Size: 275291 Color: 2564

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 440535 Color: 8987
Size: 305514 Color: 4475
Size: 253952 Color: 564

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 440623 Color: 8988
Size: 282775 Color: 3126
Size: 276603 Color: 2668

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 440636 Color: 8989
Size: 293085 Color: 3775
Size: 266280 Color: 1829

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 440668 Color: 8990
Size: 304461 Color: 4419
Size: 254872 Color: 694

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 440722 Color: 8991
Size: 283244 Color: 3157
Size: 276035 Color: 2618

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 440775 Color: 8992
Size: 293960 Color: 3817
Size: 265266 Color: 1745

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 440822 Color: 8993
Size: 280760 Color: 2982
Size: 278419 Color: 2802

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 440836 Color: 8994
Size: 292784 Color: 3762
Size: 266381 Color: 1838

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 440916 Color: 8995
Size: 290083 Color: 3596
Size: 269002 Color: 2055

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 440959 Color: 8996
Size: 301947 Color: 4266
Size: 257095 Color: 957

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 440977 Color: 8997
Size: 299325 Color: 4131
Size: 259699 Color: 1217

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 441025 Color: 8998
Size: 299376 Color: 4135
Size: 259600 Color: 1210

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 441069 Color: 8999
Size: 292932 Color: 3769
Size: 266000 Color: 1804

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 441084 Color: 9000
Size: 299684 Color: 4148
Size: 259233 Color: 1172

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 441122 Color: 9001
Size: 297331 Color: 4024
Size: 261548 Color: 1409

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 441172 Color: 9002
Size: 298341 Color: 4072
Size: 260488 Color: 1308

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 441196 Color: 9003
Size: 305090 Color: 4455
Size: 253715 Color: 532

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 441243 Color: 9004
Size: 304997 Color: 4449
Size: 253761 Color: 541

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 441243 Color: 9005
Size: 295410 Color: 3901
Size: 263348 Color: 1569

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 441275 Color: 9006
Size: 282227 Color: 3089
Size: 276499 Color: 2655

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 441329 Color: 9007
Size: 284532 Color: 3263
Size: 274140 Color: 2478

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 441424 Color: 9008
Size: 290845 Color: 3638
Size: 267732 Color: 1957

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 441435 Color: 9009
Size: 301532 Color: 4238
Size: 257034 Color: 951

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 441460 Color: 9010
Size: 304741 Color: 4432
Size: 253800 Color: 544

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 441547 Color: 9011
Size: 299639 Color: 4146
Size: 258815 Color: 1130

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 441586 Color: 9012
Size: 300931 Color: 4212
Size: 257484 Color: 988

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 441606 Color: 9013
Size: 300777 Color: 4202
Size: 257618 Color: 1003

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 441635 Color: 9014
Size: 291301 Color: 3670
Size: 267065 Color: 1893

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 441651 Color: 9015
Size: 292595 Color: 3752
Size: 265755 Color: 1784

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 441703 Color: 9016
Size: 286037 Color: 3358
Size: 272261 Color: 2318

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 441711 Color: 9017
Size: 296103 Color: 3955
Size: 262187 Color: 1462

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 441769 Color: 9019
Size: 282382 Color: 3102
Size: 275850 Color: 2602

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 441769 Color: 9018
Size: 292019 Color: 3724
Size: 266213 Color: 1822

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 441771 Color: 9020
Size: 305541 Color: 4478
Size: 252689 Color: 393

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 441787 Color: 9021
Size: 306660 Color: 4538
Size: 251554 Color: 252

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 441815 Color: 9022
Size: 283516 Color: 3175
Size: 274670 Color: 2517

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 441935 Color: 9023
Size: 288933 Color: 3531
Size: 269133 Color: 2068

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 441967 Color: 9024
Size: 296955 Color: 4000
Size: 261079 Color: 1370

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 441972 Color: 9025
Size: 302918 Color: 4322
Size: 255111 Color: 724

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 441988 Color: 9026
Size: 282417 Color: 3104
Size: 275596 Color: 2580

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 442038 Color: 9027
Size: 290365 Color: 3613
Size: 267598 Color: 1945

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 442108 Color: 9028
Size: 299841 Color: 4154
Size: 258052 Color: 1044

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 442151 Color: 9029
Size: 302811 Color: 4314
Size: 255039 Color: 716

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 442174 Color: 9030
Size: 288593 Color: 3508
Size: 269234 Color: 2075

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 442198 Color: 9031
Size: 298102 Color: 4059
Size: 259701 Color: 1218

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 442226 Color: 9032
Size: 281960 Color: 3069
Size: 275815 Color: 2599

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 442227 Color: 9033
Size: 300526 Color: 4192
Size: 257248 Color: 974

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 442245 Color: 9034
Size: 306372 Color: 4518
Size: 251384 Color: 234

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 442353 Color: 9035
Size: 298689 Color: 4099
Size: 258959 Color: 1148

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 442400 Color: 9036
Size: 288801 Color: 3523
Size: 268800 Color: 2032

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 442447 Color: 9037
Size: 291988 Color: 3720
Size: 265566 Color: 1770

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 442519 Color: 9038
Size: 306192 Color: 4512
Size: 251290 Color: 219

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 442524 Color: 9039
Size: 282583 Color: 3118
Size: 274894 Color: 2530

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 442583 Color: 9040
Size: 291870 Color: 3712
Size: 265548 Color: 1765

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 442589 Color: 9041
Size: 306964 Color: 4553
Size: 250448 Color: 74

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 442605 Color: 9042
Size: 295089 Color: 3885
Size: 262307 Color: 1467

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 442642 Color: 9043
Size: 278892 Color: 2839
Size: 278467 Color: 2807

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 442681 Color: 9044
Size: 291397 Color: 3677
Size: 265923 Color: 1797

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 442715 Color: 9045
Size: 284504 Color: 3259
Size: 272782 Color: 2367

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 442780 Color: 9046
Size: 301741 Color: 4253
Size: 255480 Color: 764

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 442793 Color: 9047
Size: 284315 Color: 3242
Size: 272893 Color: 2376

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 442810 Color: 9048
Size: 296774 Color: 3994
Size: 260417 Color: 1301

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 442823 Color: 9049
Size: 303526 Color: 4363
Size: 253652 Color: 522

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 442850 Color: 9050
Size: 284403 Color: 3250
Size: 272748 Color: 2361

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 442922 Color: 9051
Size: 281716 Color: 3043
Size: 275363 Color: 2569

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 442951 Color: 9052
Size: 282698 Color: 3120
Size: 274352 Color: 2497

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 443016 Color: 9053
Size: 292884 Color: 3766
Size: 264101 Color: 1642

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 443017 Color: 9054
Size: 295822 Color: 3935
Size: 261162 Color: 1376

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 443041 Color: 9055
Size: 306067 Color: 4507
Size: 250893 Color: 159

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 443087 Color: 9056
Size: 303810 Color: 4380
Size: 253104 Color: 452

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 443089 Color: 9057
Size: 280370 Color: 2963
Size: 276542 Color: 2658

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 443100 Color: 9058
Size: 280344 Color: 2958
Size: 276557 Color: 2662

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 443142 Color: 9059
Size: 290310 Color: 3609
Size: 266549 Color: 1858

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 443144 Color: 9060
Size: 292032 Color: 3725
Size: 264825 Color: 1705

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 443244 Color: 9061
Size: 304249 Color: 4406
Size: 252508 Color: 370

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 443266 Color: 9062
Size: 286665 Color: 3402
Size: 270070 Color: 2126

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 443369 Color: 9063
Size: 305559 Color: 4479
Size: 251073 Color: 188

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 443373 Color: 9064
Size: 301033 Color: 4220
Size: 255595 Color: 786

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 443412 Color: 9065
Size: 280035 Color: 2933
Size: 276554 Color: 2661

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 443602 Color: 9066
Size: 278333 Color: 2795
Size: 278066 Color: 2780

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 443620 Color: 9067
Size: 296891 Color: 3999
Size: 259490 Color: 1198

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 443638 Color: 9068
Size: 280778 Color: 2983
Size: 275585 Color: 2579

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 443685 Color: 9069
Size: 285434 Color: 3328
Size: 270882 Color: 2194

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 443693 Color: 9070
Size: 287835 Color: 3473
Size: 268473 Color: 2011

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 443695 Color: 9071
Size: 280005 Color: 2928
Size: 276301 Color: 2638

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 443767 Color: 9072
Size: 279179 Color: 2864
Size: 277055 Color: 2701

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 443772 Color: 9073
Size: 302739 Color: 4310
Size: 253490 Color: 501

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 443784 Color: 9074
Size: 278491 Color: 2810
Size: 277726 Color: 2752

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 443810 Color: 9075
Size: 279615 Color: 2893
Size: 276576 Color: 2665

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 443830 Color: 9076
Size: 293196 Color: 3780
Size: 262975 Color: 1528

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 443846 Color: 9077
Size: 293518 Color: 3791
Size: 262637 Color: 1487

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 443881 Color: 9078
Size: 297720 Color: 4041
Size: 258400 Color: 1090

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 443913 Color: 9079
Size: 300757 Color: 4201
Size: 255331 Color: 751

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 444018 Color: 9080
Size: 297005 Color: 4002
Size: 258978 Color: 1153

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 444080 Color: 9081
Size: 299362 Color: 4134
Size: 256559 Color: 893

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 444199 Color: 9082
Size: 299446 Color: 4140
Size: 256356 Color: 870

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 444228 Color: 9083
Size: 303338 Color: 4358
Size: 252435 Color: 364

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 444270 Color: 9084
Size: 285226 Color: 3317
Size: 270505 Color: 2164

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 444344 Color: 9085
Size: 296596 Color: 3984
Size: 259061 Color: 1157

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 444349 Color: 9086
Size: 279877 Color: 2917
Size: 275775 Color: 2596

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 444451 Color: 9087
Size: 290114 Color: 3598
Size: 265436 Color: 1758

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 444466 Color: 9088
Size: 292141 Color: 3730
Size: 263394 Color: 1575

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 444519 Color: 9089
Size: 305302 Color: 4466
Size: 250180 Color: 38

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 444622 Color: 9090
Size: 277974 Color: 2773
Size: 277405 Color: 2730

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 444632 Color: 9091
Size: 291388 Color: 3674
Size: 263981 Color: 1626

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 444672 Color: 9092
Size: 281843 Color: 3058
Size: 273486 Color: 2423

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 444698 Color: 9093
Size: 291179 Color: 3660
Size: 264124 Color: 1644

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 444742 Color: 9094
Size: 299745 Color: 4151
Size: 255514 Color: 769

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 444822 Color: 9095
Size: 278906 Color: 2841
Size: 276273 Color: 2635

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 444845 Color: 9096
Size: 282964 Color: 3140
Size: 272192 Color: 2311

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 444857 Color: 9097
Size: 295629 Color: 3913
Size: 259515 Color: 1201

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 444863 Color: 9098
Size: 279190 Color: 2865
Size: 275948 Color: 2610

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 445083 Color: 9099
Size: 299973 Color: 4164
Size: 254945 Color: 706

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 445139 Color: 9100
Size: 284767 Color: 3281
Size: 270095 Color: 2131

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 445153 Color: 9101
Size: 289901 Color: 3583
Size: 264947 Color: 1717

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 445206 Color: 9102
Size: 284474 Color: 3257
Size: 270321 Color: 2149

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 445225 Color: 9103
Size: 278808 Color: 2832
Size: 275968 Color: 2612

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 445308 Color: 9104
Size: 297461 Color: 4028
Size: 257232 Color: 971

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 445345 Color: 9105
Size: 295126 Color: 3888
Size: 259530 Color: 1202

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 445371 Color: 9106
Size: 297710 Color: 4040
Size: 256920 Color: 939

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 445436 Color: 9107
Size: 286207 Color: 3370
Size: 268358 Color: 2002

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 445458 Color: 9108
Size: 304090 Color: 4396
Size: 250453 Color: 75

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 445478 Color: 9109
Size: 288333 Color: 3494
Size: 266190 Color: 1820

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 445486 Color: 9110
Size: 300353 Color: 4184
Size: 254162 Color: 603

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 445501 Color: 9111
Size: 287444 Color: 3453
Size: 267056 Color: 1890

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 445663 Color: 9112
Size: 304333 Color: 4412
Size: 250005 Color: 3

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 445687 Color: 9113
Size: 290752 Color: 3634
Size: 263562 Color: 1591

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 445716 Color: 9114
Size: 300200 Color: 4179
Size: 254085 Color: 588

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 445724 Color: 9115
Size: 284207 Color: 3233
Size: 270070 Color: 2127

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 445746 Color: 9117
Size: 282419 Color: 3106
Size: 271836 Color: 2275

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 445746 Color: 9116
Size: 277498 Color: 2733
Size: 276757 Color: 2680

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 445766 Color: 9118
Size: 290944 Color: 3645
Size: 263291 Color: 1566

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 445802 Color: 9119
Size: 301000 Color: 4218
Size: 253199 Color: 467

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 445941 Color: 9120
Size: 283456 Color: 3172
Size: 270604 Color: 2175

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 445960 Color: 9121
Size: 300487 Color: 4190
Size: 253554 Color: 508

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 446034 Color: 9122
Size: 301665 Color: 4246
Size: 252302 Color: 343

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 446066 Color: 9123
Size: 279630 Color: 2897
Size: 274305 Color: 2493

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 446093 Color: 9124
Size: 278995 Color: 2847
Size: 274913 Color: 2535

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 446095 Color: 9125
Size: 279643 Color: 2899
Size: 274263 Color: 2489

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 446264 Color: 9126
Size: 283209 Color: 3154
Size: 270528 Color: 2169

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 446276 Color: 9127
Size: 284276 Color: 3239
Size: 269449 Color: 2088

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 446316 Color: 9128
Size: 295724 Color: 3923
Size: 257961 Color: 1034

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 446452 Color: 9129
Size: 279421 Color: 2878
Size: 274128 Color: 2476

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 446497 Color: 9130
Size: 284524 Color: 3261
Size: 268980 Color: 2052

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 446502 Color: 9131
Size: 277067 Color: 2704
Size: 276432 Color: 2648

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 446657 Color: 9132
Size: 288920 Color: 3530
Size: 264424 Color: 1673

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 446737 Color: 9133
Size: 280148 Color: 2943
Size: 273116 Color: 2394

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 446780 Color: 9134
Size: 278443 Color: 2804
Size: 274778 Color: 2523

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 446794 Color: 9135
Size: 292398 Color: 3741
Size: 260809 Color: 1339

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 446831 Color: 9136
Size: 300949 Color: 4215
Size: 252221 Color: 337

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 446912 Color: 9137
Size: 287202 Color: 3440
Size: 265887 Color: 1794

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 447037 Color: 9138
Size: 280827 Color: 2986
Size: 272137 Color: 2306

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 447059 Color: 9139
Size: 286521 Color: 3390
Size: 266421 Color: 1844

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 447088 Color: 9140
Size: 277567 Color: 2739
Size: 275346 Color: 2568

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 447159 Color: 9141
Size: 285767 Color: 3347
Size: 267075 Color: 1894

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 447188 Color: 9142
Size: 285134 Color: 3306
Size: 267679 Color: 1951

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 447230 Color: 9143
Size: 299895 Color: 4158
Size: 252876 Color: 420

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 447281 Color: 9144
Size: 284568 Color: 3267
Size: 268152 Color: 1988

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 447291 Color: 9145
Size: 287888 Color: 3476
Size: 264822 Color: 1704

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 447314 Color: 9146
Size: 297997 Color: 4053
Size: 254690 Color: 671

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 447452 Color: 9147
Size: 281565 Color: 3030
Size: 270984 Color: 2203

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 447490 Color: 9148
Size: 289262 Color: 3552
Size: 263249 Color: 1562

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 447501 Color: 9149
Size: 288558 Color: 3504
Size: 263942 Color: 1623

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 447628 Color: 9150
Size: 298407 Color: 4080
Size: 253966 Color: 568

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 447902 Color: 9151
Size: 284621 Color: 3271
Size: 267478 Color: 1936

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 447958 Color: 9152
Size: 279063 Color: 2855
Size: 272980 Color: 2384

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 448073 Color: 9153
Size: 293654 Color: 3802
Size: 258274 Color: 1071

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 448158 Color: 9154
Size: 295978 Color: 3947
Size: 255865 Color: 810

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 448161 Color: 9155
Size: 291946 Color: 3715
Size: 259894 Color: 1238

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 448165 Color: 9156
Size: 294720 Color: 3862
Size: 257116 Color: 959

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 448201 Color: 9157
Size: 297088 Color: 4013
Size: 254712 Color: 674

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 448218 Color: 9158
Size: 285373 Color: 3325
Size: 266410 Color: 1843

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 448229 Color: 9159
Size: 282830 Color: 3131
Size: 268942 Color: 2049

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 448272 Color: 9160
Size: 296065 Color: 3952
Size: 255664 Color: 791

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 448287 Color: 9161
Size: 297978 Color: 4052
Size: 253736 Color: 538

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 448342 Color: 9162
Size: 275945 Color: 2609
Size: 275714 Color: 2589

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 448401 Color: 9163
Size: 298834 Color: 4105
Size: 252766 Color: 404

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 448527 Color: 9164
Size: 292819 Color: 3764
Size: 258655 Color: 1111

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 448555 Color: 9165
Size: 294401 Color: 3841
Size: 257045 Color: 953

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 448606 Color: 9166
Size: 299524 Color: 4143
Size: 251871 Color: 291

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 448707 Color: 9167
Size: 294466 Color: 3848
Size: 256828 Color: 925

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 448871 Color: 9168
Size: 284408 Color: 3251
Size: 266722 Color: 1869

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 448891 Color: 9169
Size: 282869 Color: 3133
Size: 268241 Color: 1996

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 448897 Color: 9170
Size: 277896 Color: 2765
Size: 273208 Color: 2403

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 448898 Color: 9171
Size: 283724 Color: 3199
Size: 267379 Color: 1924

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 448934 Color: 9172
Size: 287123 Color: 3437
Size: 263944 Color: 1624

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 449288 Color: 9173
Size: 278041 Color: 2778
Size: 272672 Color: 2354

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 449443 Color: 9174
Size: 287940 Color: 3478
Size: 262618 Color: 1485

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 449463 Color: 9175
Size: 291182 Color: 3662
Size: 259356 Color: 1185

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 449465 Color: 9176
Size: 283289 Color: 3160
Size: 267247 Color: 1910

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 449514 Color: 9177
Size: 277750 Color: 2754
Size: 272737 Color: 2358

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 449599 Color: 9178
Size: 294829 Color: 3866
Size: 255573 Color: 783

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 449664 Color: 9179
Size: 283123 Color: 3147
Size: 267214 Color: 1905

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 449734 Color: 9180
Size: 284657 Color: 3274
Size: 265610 Color: 1776

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 449765 Color: 9181
Size: 282316 Color: 3097
Size: 267920 Color: 1968

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 449775 Color: 9182
Size: 277195 Color: 2714
Size: 273031 Color: 2388

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 449854 Color: 9183
Size: 284886 Color: 3287
Size: 265261 Color: 1744

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 449900 Color: 9184
Size: 288583 Color: 3507
Size: 261518 Color: 1405

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 450006 Color: 9185
Size: 281796 Color: 3052
Size: 268199 Color: 1992

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 450013 Color: 9186
Size: 291496 Color: 3684
Size: 258492 Color: 1094

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 450027 Color: 9187
Size: 275770 Color: 2595
Size: 274204 Color: 2483

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 450035 Color: 9188
Size: 286760 Color: 3413
Size: 263206 Color: 1555

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 450107 Color: 9189
Size: 296117 Color: 3956
Size: 253777 Color: 542

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 450136 Color: 9190
Size: 285768 Color: 3348
Size: 264097 Color: 1641

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 450353 Color: 9191
Size: 287112 Color: 3435
Size: 262536 Color: 1479

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 450606 Color: 9192
Size: 283124 Color: 3149
Size: 266271 Color: 1826

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 450719 Color: 9193
Size: 288566 Color: 3505
Size: 260716 Color: 1330

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 450770 Color: 9194
Size: 295138 Color: 3889
Size: 254093 Color: 590

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 450776 Color: 9195
Size: 294433 Color: 3842
Size: 254792 Color: 683

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 450872 Color: 9196
Size: 289195 Color: 3544
Size: 259934 Color: 1245

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 450876 Color: 9197
Size: 277936 Color: 2771
Size: 271189 Color: 2223

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 450886 Color: 9199
Size: 279630 Color: 2896
Size: 269485 Color: 2090

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 450886 Color: 9198
Size: 292320 Color: 3736
Size: 256795 Color: 921

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 450936 Color: 9200
Size: 296562 Color: 3982
Size: 252503 Color: 368

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 450972 Color: 9201
Size: 294371 Color: 3840
Size: 254658 Color: 666

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 451045 Color: 9202
Size: 278203 Color: 2786
Size: 270753 Color: 2183

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 451112 Color: 9203
Size: 281980 Color: 3072
Size: 266909 Color: 1880

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 451116 Color: 9204
Size: 290498 Color: 3620
Size: 258387 Color: 1085

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 451195 Color: 9205
Size: 298680 Color: 4097
Size: 250126 Color: 22

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 451294 Color: 9206
Size: 285643 Color: 3335
Size: 263064 Color: 1538

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 451404 Color: 9207
Size: 276241 Color: 2632
Size: 272356 Color: 2325

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 451416 Color: 9208
Size: 289037 Color: 3536
Size: 259548 Color: 1204

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 451532 Color: 9209
Size: 276697 Color: 2675
Size: 271772 Color: 2265

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 451608 Color: 9210
Size: 290063 Color: 3593
Size: 258330 Color: 1082

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 451623 Color: 9211
Size: 287795 Color: 3470
Size: 260583 Color: 1320

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 451633 Color: 9212
Size: 286615 Color: 3399
Size: 261753 Color: 1429

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 451686 Color: 9213
Size: 294591 Color: 3853
Size: 253724 Color: 535

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 451715 Color: 9214
Size: 274439 Color: 2506
Size: 273847 Color: 2452

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 451719 Color: 9215
Size: 277292 Color: 2720
Size: 270990 Color: 2204

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 451872 Color: 9216
Size: 294224 Color: 3830
Size: 253905 Color: 558

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 451971 Color: 9217
Size: 276885 Color: 2690
Size: 271145 Color: 2216

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 452010 Color: 9218
Size: 287497 Color: 3457
Size: 260494 Color: 1311

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 452017 Color: 9219
Size: 279533 Color: 2885
Size: 268451 Color: 2010

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 452062 Color: 9220
Size: 294207 Color: 3827
Size: 253732 Color: 537

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 452085 Color: 9221
Size: 293379 Color: 3785
Size: 254537 Color: 646

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 452120 Color: 9222
Size: 295068 Color: 3884
Size: 252813 Color: 411

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 452132 Color: 9223
Size: 277175 Color: 2712
Size: 270694 Color: 2178

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 452133 Color: 9224
Size: 279926 Color: 2920
Size: 267942 Color: 1970

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 452146 Color: 9225
Size: 296205 Color: 3961
Size: 251650 Color: 266

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 452246 Color: 9226
Size: 276408 Color: 2646
Size: 271347 Color: 2233

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 452250 Color: 9227
Size: 274736 Color: 2521
Size: 273015 Color: 2386

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 452406 Color: 9228
Size: 276135 Color: 2625
Size: 271460 Color: 2243

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 452470 Color: 9229
Size: 292605 Color: 3753
Size: 254926 Color: 704

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 452489 Color: 9230
Size: 276351 Color: 2641
Size: 271161 Color: 2220

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 452490 Color: 9231
Size: 282197 Color: 3084
Size: 265314 Color: 1750

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 452535 Color: 9232
Size: 291649 Color: 3694
Size: 255817 Color: 805

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 452550 Color: 9233
Size: 285672 Color: 3338
Size: 261779 Color: 1431

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 452814 Color: 9234
Size: 286720 Color: 3408
Size: 260467 Color: 1307

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 452880 Color: 9235
Size: 287033 Color: 3426
Size: 260088 Color: 1264

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 452974 Color: 9236
Size: 274963 Color: 2539
Size: 272064 Color: 2298

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 453014 Color: 9237
Size: 288699 Color: 3515
Size: 258288 Color: 1074

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 453090 Color: 9238
Size: 279762 Color: 2911
Size: 267149 Color: 1901

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 453092 Color: 9239
Size: 283286 Color: 3159
Size: 263623 Color: 1601

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 453104 Color: 9240
Size: 280720 Color: 2980
Size: 266177 Color: 1818

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 453124 Color: 9241
Size: 290701 Color: 3628
Size: 256176 Color: 852

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 453132 Color: 9242
Size: 294687 Color: 3859
Size: 252182 Color: 327

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 453181 Color: 9243
Size: 285475 Color: 3330
Size: 261345 Color: 1390

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 453222 Color: 9244
Size: 288098 Color: 3484
Size: 258681 Color: 1114

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 453246 Color: 9245
Size: 293968 Color: 3818
Size: 252787 Color: 406

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 453335 Color: 9246
Size: 274856 Color: 2528
Size: 271810 Color: 2269

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 453424 Color: 9247
Size: 289837 Color: 3580
Size: 256740 Color: 910

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 453444 Color: 9248
Size: 291674 Color: 3696
Size: 254883 Color: 695

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 453519 Color: 9249
Size: 287297 Color: 3443
Size: 259185 Color: 1163

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 453569 Color: 9250
Size: 281453 Color: 3022
Size: 264979 Color: 1721

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 453587 Color: 9251
Size: 278339 Color: 2797
Size: 268075 Color: 1983

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 453724 Color: 9252
Size: 280711 Color: 2979
Size: 265566 Color: 1771

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 453759 Color: 9253
Size: 286180 Color: 3367
Size: 260062 Color: 1259

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 453786 Color: 9254
Size: 292931 Color: 3768
Size: 253284 Color: 482

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 453955 Color: 9255
Size: 278311 Color: 2791
Size: 267735 Color: 1958

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 453972 Color: 9256
Size: 280156 Color: 2944
Size: 265873 Color: 1793

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 453992 Color: 9257
Size: 287121 Color: 3436
Size: 258888 Color: 1141

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 454315 Color: 9258
Size: 284301 Color: 3240
Size: 261385 Color: 1395

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 454330 Color: 9259
Size: 273655 Color: 2436
Size: 272016 Color: 2296

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 454464 Color: 9260
Size: 286694 Color: 3407
Size: 258843 Color: 1134

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 454506 Color: 9261
Size: 276349 Color: 2640
Size: 269146 Color: 2069

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 454508 Color: 9262
Size: 285212 Color: 3315
Size: 260281 Color: 1286

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 454644 Color: 9263
Size: 281359 Color: 3014
Size: 263998 Color: 1629

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 454689 Color: 9264
Size: 287388 Color: 3449
Size: 257924 Color: 1030

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 454702 Color: 9265
Size: 294217 Color: 3828
Size: 251082 Color: 189

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 454725 Color: 9266
Size: 277671 Color: 2747
Size: 267605 Color: 1946

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 454734 Color: 9267
Size: 284163 Color: 3229
Size: 261104 Color: 1372

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 454804 Color: 9268
Size: 291135 Color: 3657
Size: 254062 Color: 583

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 454814 Color: 9269
Size: 273392 Color: 2416
Size: 271795 Color: 2266

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 454871 Color: 9270
Size: 280811 Color: 2985
Size: 264319 Color: 1662

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 454876 Color: 9271
Size: 290708 Color: 3629
Size: 254417 Color: 635

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 454901 Color: 9272
Size: 289152 Color: 3539
Size: 255948 Color: 819

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 454906 Color: 9273
Size: 287399 Color: 3451
Size: 257696 Color: 1009

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 454943 Color: 9275
Size: 276218 Color: 2631
Size: 268840 Color: 2037

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 454943 Color: 9274
Size: 277720 Color: 2751
Size: 267338 Color: 1917

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 454985 Color: 9276
Size: 282921 Color: 3139
Size: 262095 Color: 1454

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 455033 Color: 9277
Size: 284787 Color: 3283
Size: 260181 Color: 1274

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 455063 Color: 9278
Size: 279802 Color: 2912
Size: 265136 Color: 1731

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 455141 Color: 9279
Size: 280134 Color: 2940
Size: 264726 Color: 1698

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 455145 Color: 9280
Size: 286124 Color: 3363
Size: 258732 Color: 1123

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 455162 Color: 9281
Size: 286789 Color: 3415
Size: 258050 Color: 1042

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 455171 Color: 9282
Size: 290452 Color: 3617
Size: 254378 Color: 630

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 455340 Color: 9283
Size: 276138 Color: 2626
Size: 268523 Color: 2015

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 455393 Color: 9284
Size: 285707 Color: 3341
Size: 258901 Color: 1143

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 455394 Color: 9285
Size: 277175 Color: 2713
Size: 267432 Color: 1931

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 455493 Color: 9286
Size: 278412 Color: 2801
Size: 266096 Color: 1809

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 455497 Color: 9287
Size: 284600 Color: 3269
Size: 259904 Color: 1242

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 455504 Color: 9288
Size: 284766 Color: 3280
Size: 259731 Color: 1222

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 455514 Color: 9289
Size: 288146 Color: 3485
Size: 256341 Color: 868

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 455652 Color: 9290
Size: 289175 Color: 3543
Size: 255174 Color: 731

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 455662 Color: 9291
Size: 281650 Color: 3033
Size: 262689 Color: 1493

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 455734 Color: 9292
Size: 273862 Color: 2453
Size: 270405 Color: 2156

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 455744 Color: 9293
Size: 292759 Color: 3760
Size: 251498 Color: 245

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 455798 Color: 9294
Size: 280215 Color: 2948
Size: 263988 Color: 1627

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 455898 Color: 9295
Size: 292422 Color: 3744
Size: 251681 Color: 272

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 455899 Color: 9296
Size: 293201 Color: 3781
Size: 250901 Color: 160

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 455944 Color: 9297
Size: 273502 Color: 2427
Size: 270555 Color: 2172

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 455967 Color: 9298
Size: 290736 Color: 3632
Size: 253298 Color: 483

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 456164 Color: 9299
Size: 277538 Color: 2735
Size: 266299 Color: 1832

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 456218 Color: 9300
Size: 280911 Color: 2987
Size: 262872 Color: 1512

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 456250 Color: 9301
Size: 275665 Color: 2586
Size: 268086 Color: 1985

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 456337 Color: 9302
Size: 273061 Color: 2391
Size: 270603 Color: 2174

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 456376 Color: 9303
Size: 279558 Color: 2889
Size: 264067 Color: 1638

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 456526 Color: 9304
Size: 279026 Color: 2850
Size: 264449 Color: 1677

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 456540 Color: 9305
Size: 293154 Color: 3778
Size: 250307 Color: 55

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 456658 Color: 9306
Size: 274716 Color: 2520
Size: 268627 Color: 2021

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 456671 Color: 9307
Size: 289682 Color: 3573
Size: 253648 Color: 521

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 456723 Color: 9308
Size: 286330 Color: 3380
Size: 256948 Color: 943

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 456763 Color: 9309
Size: 293114 Color: 3776
Size: 250124 Color: 20

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 456764 Color: 9310
Size: 282732 Color: 3122
Size: 260505 Color: 1312

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 456781 Color: 9311
Size: 277774 Color: 2757
Size: 265446 Color: 1760

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 456805 Color: 9312
Size: 283935 Color: 3213
Size: 259261 Color: 1176

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 456850 Color: 9313
Size: 290078 Color: 3594
Size: 253073 Color: 449

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 456895 Color: 9314
Size: 279636 Color: 2898
Size: 263470 Color: 1585

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 456960 Color: 9315
Size: 282792 Color: 3128
Size: 260249 Color: 1280

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 457004 Color: 9316
Size: 290267 Color: 3606
Size: 252730 Color: 398

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 457006 Color: 9317
Size: 283654 Color: 3190
Size: 259341 Color: 1184

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 457039 Color: 9318
Size: 287854 Color: 3474
Size: 255108 Color: 723

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 457051 Color: 9319
Size: 274794 Color: 2524
Size: 268156 Color: 1990

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 457141 Color: 9320
Size: 273902 Color: 2457
Size: 268958 Color: 2050

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 457202 Color: 9321
Size: 272871 Color: 2373
Size: 269928 Color: 2118

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 457236 Color: 9322
Size: 277205 Color: 2715
Size: 265560 Color: 1767

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 457257 Color: 9323
Size: 275292 Color: 2565
Size: 267452 Color: 1932

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 457367 Color: 9325
Size: 278775 Color: 2829
Size: 263859 Color: 1615

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 457367 Color: 9324
Size: 279361 Color: 2873
Size: 263273 Color: 1565

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 457403 Color: 9326
Size: 284470 Color: 3256
Size: 258128 Color: 1050

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 457462 Color: 9327
Size: 280202 Color: 2947
Size: 262337 Color: 1468

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 457468 Color: 9328
Size: 279647 Color: 2901
Size: 262886 Color: 1513

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 457570 Color: 9329
Size: 281041 Color: 2996
Size: 261390 Color: 1397

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 457580 Color: 9330
Size: 289758 Color: 3578
Size: 252663 Color: 390

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 457606 Color: 9331
Size: 281085 Color: 3001
Size: 261310 Color: 1386

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 457619 Color: 9332
Size: 283848 Color: 3207
Size: 258534 Color: 1097

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 457733 Color: 9333
Size: 279346 Color: 2872
Size: 262922 Color: 1519

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 457872 Color: 9334
Size: 279434 Color: 2879
Size: 262695 Color: 1495

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 457907 Color: 9335
Size: 286419 Color: 3384
Size: 255675 Color: 793

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 457999 Color: 9336
Size: 271716 Color: 2261
Size: 270286 Color: 2146

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 458149 Color: 9337
Size: 288338 Color: 3495
Size: 253514 Color: 505

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 458155 Color: 9338
Size: 290925 Color: 3643
Size: 250921 Color: 162

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 458250 Color: 9339
Size: 287663 Color: 3464
Size: 254088 Color: 589

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 458272 Color: 9340
Size: 283533 Color: 3176
Size: 258196 Color: 1060

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 458279 Color: 9341
Size: 288766 Color: 3521
Size: 252956 Color: 435

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 458285 Color: 9342
Size: 291156 Color: 3658
Size: 250560 Color: 90

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 458296 Color: 9343
Size: 289140 Color: 3538
Size: 252565 Color: 376

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 458309 Color: 9344
Size: 289448 Color: 3561
Size: 252244 Color: 340

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 458319 Color: 9345
Size: 272973 Color: 2383
Size: 268709 Color: 2024

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 458368 Color: 9346
Size: 281766 Color: 3049
Size: 259867 Color: 1235

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 458440 Color: 9347
Size: 289535 Color: 3566
Size: 252026 Color: 314

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 458493 Color: 9348
Size: 282193 Color: 3082
Size: 259315 Color: 1182

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 458498 Color: 9349
Size: 282541 Color: 3115
Size: 258962 Color: 1150

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 458534 Color: 9350
Size: 281393 Color: 3019
Size: 260074 Color: 1260

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 458585 Color: 9351
Size: 285104 Color: 3303
Size: 256312 Color: 865

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 458590 Color: 9352
Size: 290110 Color: 3597
Size: 251301 Color: 221

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 458609 Color: 9353
Size: 282880 Color: 3136
Size: 258512 Color: 1095

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 458619 Color: 9354
Size: 276547 Color: 2659
Size: 264835 Color: 1707

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 458638 Color: 9355
Size: 273754 Color: 2443
Size: 267609 Color: 1947

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 458659 Color: 9356
Size: 275235 Color: 2560
Size: 266107 Color: 1811

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 458664 Color: 9357
Size: 272244 Color: 2317
Size: 269093 Color: 2066

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 458678 Color: 9358
Size: 280344 Color: 2959
Size: 260979 Color: 1355

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 458774 Color: 9359
Size: 283975 Color: 3221
Size: 257252 Color: 975

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 458831 Color: 9360
Size: 286772 Color: 3414
Size: 254398 Color: 631

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 458847 Color: 9361
Size: 285985 Color: 3355
Size: 255169 Color: 730

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 458923 Color: 9362
Size: 284747 Color: 3278
Size: 256331 Color: 867

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 458964 Color: 9363
Size: 283232 Color: 3155
Size: 257805 Color: 1018

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 459018 Color: 9364
Size: 276356 Color: 2642
Size: 264627 Color: 1688

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 459060 Color: 9365
Size: 286473 Color: 3387
Size: 254468 Color: 640

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 459097 Color: 9366
Size: 274441 Color: 2507
Size: 266463 Color: 1851

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 459112 Color: 9367
Size: 278721 Color: 2824
Size: 262168 Color: 1458

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 459131 Color: 9368
Size: 271478 Color: 2246
Size: 269392 Color: 2085

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 459147 Color: 9369
Size: 279322 Color: 2871
Size: 261532 Color: 1406

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 459148 Color: 9370
Size: 288806 Color: 3525
Size: 252047 Color: 316

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 459458 Color: 9371
Size: 271733 Color: 2263
Size: 268810 Color: 2034

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 459483 Color: 9372
Size: 281608 Color: 3032
Size: 258910 Color: 1144

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 459737 Color: 9373
Size: 272776 Color: 2366
Size: 267488 Color: 1938

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 459774 Color: 9374
Size: 278781 Color: 2831
Size: 261446 Color: 1403

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 459808 Color: 9375
Size: 286219 Color: 3372
Size: 253974 Color: 569

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 459820 Color: 9376
Size: 284913 Color: 3290
Size: 255268 Color: 742

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 459824 Color: 9377
Size: 270949 Color: 2201
Size: 269228 Color: 2074

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 459827 Color: 9378
Size: 275140 Color: 2550
Size: 265034 Color: 1723

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 459866 Color: 9379
Size: 285678 Color: 3340
Size: 254457 Color: 639

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 460010 Color: 9380
Size: 285403 Color: 3326
Size: 254588 Color: 656

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 460055 Color: 9381
Size: 287070 Color: 3431
Size: 252876 Color: 421

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 460069 Color: 9382
Size: 287123 Color: 3438
Size: 252809 Color: 408

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 460119 Color: 9383
Size: 280013 Color: 2930
Size: 259869 Color: 1236

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 460120 Color: 9384
Size: 276670 Color: 2674
Size: 263211 Color: 1556

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 460272 Color: 9385
Size: 276868 Color: 2687
Size: 262861 Color: 1510

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 460308 Color: 9386
Size: 276869 Color: 2688
Size: 262824 Color: 1508

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 460388 Color: 9387
Size: 278050 Color: 2779
Size: 261563 Color: 1412

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 460420 Color: 9388
Size: 282879 Color: 3135
Size: 256702 Color: 906

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 460517 Color: 9389
Size: 283968 Color: 3219
Size: 255516 Color: 772

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 460568 Color: 9390
Size: 283294 Color: 3162
Size: 256139 Color: 842

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 460570 Color: 9391
Size: 284133 Color: 3226
Size: 255298 Color: 745

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 460636 Color: 9392
Size: 283860 Color: 3208
Size: 255505 Color: 767

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 460659 Color: 9393
Size: 279375 Color: 2874
Size: 259967 Color: 1247

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 460759 Color: 9394
Size: 278333 Color: 2796
Size: 260909 Color: 1350

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 460791 Color: 9395
Size: 281707 Color: 3042
Size: 257503 Color: 989

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 460899 Color: 9396
Size: 275481 Color: 2574
Size: 263621 Color: 1600

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 460999 Color: 9397
Size: 280005 Color: 2929
Size: 258997 Color: 1156

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 461082 Color: 9398
Size: 274542 Color: 2515
Size: 264377 Color: 1670

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 461084 Color: 9399
Size: 273735 Color: 2441
Size: 265182 Color: 1735

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 461189 Color: 9400
Size: 275068 Color: 2545
Size: 263744 Color: 1608

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 461195 Color: 9401
Size: 285796 Color: 3349
Size: 253010 Color: 441

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 461261 Color: 9402
Size: 288379 Color: 3496
Size: 250361 Color: 62

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 461269 Color: 9403
Size: 277400 Color: 2729
Size: 261332 Color: 1388

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 461285 Color: 9404
Size: 281542 Color: 3029
Size: 257174 Color: 965

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 461388 Color: 9405
Size: 288612 Color: 3510
Size: 250001 Color: 1

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 461451 Color: 9406
Size: 273735 Color: 2442
Size: 264815 Color: 1703

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 461457 Color: 9407
Size: 285858 Color: 3352
Size: 252686 Color: 392

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 461476 Color: 9408
Size: 271388 Color: 2239
Size: 267137 Color: 1900

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 461494 Color: 9409
Size: 275055 Color: 2544
Size: 263452 Color: 1580

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 461504 Color: 9410
Size: 282970 Color: 3141
Size: 255527 Color: 773

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 461683 Color: 9411
Size: 273823 Color: 2451
Size: 264495 Color: 1678

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 461719 Color: 9412
Size: 284566 Color: 3266
Size: 253716 Color: 533

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 461742 Color: 9413
Size: 286277 Color: 3376
Size: 251982 Color: 306

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 461772 Color: 9414
Size: 281957 Color: 3068
Size: 256272 Color: 857

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 461858 Color: 9415
Size: 278656 Color: 2820
Size: 259487 Color: 1197

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 461964 Color: 9416
Size: 282786 Color: 3127
Size: 255251 Color: 741

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 461967 Color: 9417
Size: 277739 Color: 2753
Size: 260295 Color: 1289

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 461994 Color: 9418
Size: 279485 Color: 2881
Size: 258522 Color: 1096

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 462042 Color: 9419
Size: 276398 Color: 2645
Size: 261561 Color: 1411

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 462058 Color: 9420
Size: 279201 Color: 2867
Size: 258742 Color: 1124

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 462077 Color: 9421
Size: 281699 Color: 3040
Size: 256225 Color: 853

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 462094 Color: 9422
Size: 270514 Color: 2166
Size: 267393 Color: 1926

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 462130 Color: 9423
Size: 280296 Color: 2955
Size: 257575 Color: 998

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 462199 Color: 9424
Size: 279386 Color: 2876
Size: 258416 Color: 1091

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 462236 Color: 9425
Size: 281727 Color: 3045
Size: 256038 Color: 828

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 462305 Color: 9426
Size: 282487 Color: 3110
Size: 255209 Color: 736

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 462400 Color: 9427
Size: 269038 Color: 2060
Size: 268563 Color: 2016

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 462452 Color: 9428
Size: 273995 Color: 2466
Size: 263554 Color: 1590

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 462605 Color: 9429
Size: 277619 Color: 2745
Size: 259777 Color: 1228

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 462610 Color: 9430
Size: 279458 Color: 2880
Size: 257933 Color: 1031

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 462611 Color: 9431
Size: 282573 Color: 3117
Size: 254817 Color: 686

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 462689 Color: 9432
Size: 270634 Color: 2177
Size: 266678 Color: 1867

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 462755 Color: 9433
Size: 272617 Color: 2350
Size: 264629 Color: 1689

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 462816 Color: 9434
Size: 277672 Color: 2748
Size: 259513 Color: 1200

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 462839 Color: 9435
Size: 283629 Color: 3187
Size: 253533 Color: 506

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 462848 Color: 9436
Size: 275768 Color: 2594
Size: 261385 Color: 1394

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 462871 Color: 9437
Size: 278458 Color: 2805
Size: 258672 Color: 1113

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 462874 Color: 9438
Size: 282901 Color: 3138
Size: 254226 Color: 613

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 462919 Color: 9439
Size: 279558 Color: 2890
Size: 257524 Color: 991

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 462952 Color: 9440
Size: 274580 Color: 2516
Size: 262469 Color: 1475

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 462994 Color: 9441
Size: 278772 Color: 2828
Size: 258235 Color: 1063

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 463047 Color: 9442
Size: 279380 Color: 2875
Size: 257574 Color: 997

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 463112 Color: 9443
Size: 283200 Color: 3153
Size: 253689 Color: 527

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 463124 Color: 9444
Size: 272838 Color: 2370
Size: 264039 Color: 1634

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 463145 Color: 9445
Size: 283608 Color: 3184
Size: 253248 Color: 477

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 463160 Color: 9446
Size: 272468 Color: 2333
Size: 264373 Color: 1668

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 463166 Color: 9447
Size: 278780 Color: 2830
Size: 258055 Color: 1045

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 463204 Color: 9448
Size: 286055 Color: 3359
Size: 250742 Color: 121

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 463207 Color: 9449
Size: 284137 Color: 3227
Size: 252657 Color: 389

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 463209 Color: 9450
Size: 281668 Color: 3037
Size: 255124 Color: 725

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 463212 Color: 9451
Size: 284395 Color: 3249
Size: 252394 Color: 357

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 463222 Color: 9452
Size: 277574 Color: 2742
Size: 259205 Color: 1168

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 463249 Color: 9453
Size: 272909 Color: 2378
Size: 263843 Color: 1613

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 463384 Color: 9454
Size: 281803 Color: 3054
Size: 254814 Color: 685

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 463433 Color: 9455
Size: 273124 Color: 2396
Size: 263444 Color: 1578

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 463460 Color: 9456
Size: 280040 Color: 2935
Size: 256501 Color: 885

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 463464 Color: 9457
Size: 275969 Color: 2614
Size: 260568 Color: 1318

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 463489 Color: 9458
Size: 284629 Color: 3272
Size: 251883 Color: 295

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 463621 Color: 9459
Size: 281902 Color: 3060
Size: 254478 Color: 643

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 463642 Color: 9460
Size: 269824 Color: 2113
Size: 266535 Color: 1857

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 463687 Color: 9461
Size: 276286 Color: 2636
Size: 260028 Color: 1255

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 463700 Color: 9462
Size: 271045 Color: 2208
Size: 265256 Color: 1743

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 463752 Color: 9463
Size: 283193 Color: 3152
Size: 253056 Color: 446

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 463766 Color: 9464
Size: 271636 Color: 2254
Size: 264599 Color: 1685

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 463879 Color: 9465
Size: 276869 Color: 2689
Size: 259253 Color: 1175

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 464054 Color: 9466
Size: 271900 Color: 2283
Size: 264047 Color: 1635

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 464196 Color: 9467
Size: 283913 Color: 3211
Size: 251892 Color: 296

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 464224 Color: 9468
Size: 284604 Color: 3270
Size: 251173 Color: 201

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 464257 Color: 9469
Size: 278867 Color: 2838
Size: 256877 Color: 934

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 464294 Color: 9470
Size: 279266 Color: 2868
Size: 256441 Color: 881

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 464302 Color: 9471
Size: 273645 Color: 2435
Size: 262054 Color: 1450

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 464412 Color: 9472
Size: 285199 Color: 3312
Size: 250390 Color: 68

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 464434 Color: 9473
Size: 276302 Color: 2639
Size: 259265 Color: 1178

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 464479 Color: 9474
Size: 282416 Color: 3103
Size: 253106 Color: 453

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 464495 Color: 9475
Size: 283694 Color: 3194
Size: 251812 Color: 286

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 464496 Color: 9476
Size: 283306 Color: 3163
Size: 252199 Color: 333

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 464519 Color: 9477
Size: 270386 Color: 2154
Size: 265096 Color: 1727

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 464581 Color: 9478
Size: 276141 Color: 2627
Size: 259279 Color: 1180

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 464585 Color: 9479
Size: 275081 Color: 2546
Size: 260335 Color: 1292

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 464610 Color: 9480
Size: 275949 Color: 2611
Size: 259442 Color: 1192

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 464676 Color: 9481
Size: 269643 Color: 2101
Size: 265682 Color: 1778

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 464804 Color: 9482
Size: 282268 Color: 3094
Size: 252929 Color: 432

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 464816 Color: 9483
Size: 272757 Color: 2363
Size: 262428 Color: 1473

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 464825 Color: 9484
Size: 271972 Color: 2291
Size: 263204 Color: 1554

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 464851 Color: 9485
Size: 278507 Color: 2812
Size: 256643 Color: 900

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 464919 Color: 9486
Size: 280290 Color: 2953
Size: 254792 Color: 682

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 464972 Color: 9487
Size: 281771 Color: 3050
Size: 253258 Color: 479

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 464990 Color: 9488
Size: 278513 Color: 2813
Size: 256498 Color: 883

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 464999 Color: 9489
Size: 284150 Color: 3228
Size: 250852 Color: 144

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 465054 Color: 9490
Size: 283605 Color: 3183
Size: 251342 Color: 228

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 465399 Color: 9491
Size: 276891 Color: 2691
Size: 257711 Color: 1012

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 465441 Color: 9492
Size: 274473 Color: 2510
Size: 260087 Color: 1262

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 465455 Color: 9493
Size: 278488 Color: 2809
Size: 256058 Color: 832

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 465480 Color: 9494
Size: 272739 Color: 2359
Size: 261782 Color: 1432

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 465678 Color: 9495
Size: 281744 Color: 3048
Size: 252579 Color: 378

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 465710 Color: 9496
Size: 273588 Color: 2432
Size: 260703 Color: 1326

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 465867 Color: 9497
Size: 276251 Color: 2633
Size: 257883 Color: 1027

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 465925 Color: 9498
Size: 282511 Color: 3113
Size: 251565 Color: 255

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 465937 Color: 9499
Size: 277245 Color: 2717
Size: 256819 Color: 924

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 466071 Color: 9500
Size: 270082 Color: 2129
Size: 263848 Color: 1614

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 466149 Color: 9501
Size: 270391 Color: 2155
Size: 263461 Color: 1582

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 466297 Color: 9502
Size: 275787 Color: 2597
Size: 257917 Color: 1029

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 466310 Color: 9503
Size: 281063 Color: 2999
Size: 252628 Color: 386

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 466311 Color: 9504
Size: 280291 Color: 2954
Size: 253399 Color: 496

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 466332 Color: 9505
Size: 267780 Color: 1963
Size: 265889 Color: 1795

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 466351 Color: 9506
Size: 281729 Color: 3046
Size: 251921 Color: 299

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 466448 Color: 9507
Size: 277267 Color: 2719
Size: 256286 Color: 861

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 466477 Color: 9508
Size: 269932 Color: 2119
Size: 263592 Color: 1595

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 466506 Color: 9509
Size: 267284 Color: 1912
Size: 266211 Color: 1821

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 466511 Color: 9510
Size: 273717 Color: 2438
Size: 259773 Color: 1225

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 466586 Color: 9511
Size: 282187 Color: 3079
Size: 251228 Color: 207

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 466693 Color: 9512
Size: 282198 Color: 3085
Size: 251110 Color: 192

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 466702 Color: 9513
Size: 278897 Color: 2840
Size: 254402 Color: 632

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 466775 Color: 9514
Size: 280345 Color: 2960
Size: 252881 Color: 423

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 466868 Color: 9515
Size: 277443 Color: 2731
Size: 255690 Color: 795

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 466869 Color: 9516
Size: 274993 Color: 2540
Size: 258139 Color: 1053

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 466996 Color: 9517
Size: 270455 Color: 2159
Size: 262550 Color: 1482

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 467023 Color: 9518
Size: 279065 Color: 2856
Size: 253913 Color: 560

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 467043 Color: 9519
Size: 271338 Color: 2232
Size: 261620 Color: 1418

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 467089 Color: 9520
Size: 275203 Color: 2555
Size: 257709 Color: 1011

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 467095 Color: 9521
Size: 268740 Color: 2028
Size: 264166 Color: 1647

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 467223 Color: 9522
Size: 270079 Color: 2128
Size: 262699 Color: 1496

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 467247 Color: 9523
Size: 274776 Color: 2522
Size: 257978 Color: 1035

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 467259 Color: 9524
Size: 272881 Color: 2374
Size: 259861 Color: 1233

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 467262 Color: 9525
Size: 280345 Color: 2961
Size: 252394 Color: 358

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 467295 Color: 9526
Size: 274052 Color: 2469
Size: 258654 Color: 1110

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 467332 Color: 9527
Size: 280386 Color: 2965
Size: 252283 Color: 342

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 467450 Color: 9528
Size: 280136 Color: 2941
Size: 252415 Color: 361

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 467473 Color: 9529
Size: 268846 Color: 2039
Size: 263682 Color: 1604

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 467478 Color: 9530
Size: 279046 Color: 2852
Size: 253477 Color: 499

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 467671 Color: 9531
Size: 267416 Color: 1928
Size: 264914 Color: 1714

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 467762 Color: 9532
Size: 272338 Color: 2323
Size: 259901 Color: 1240

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 467827 Color: 9533
Size: 274509 Color: 2513
Size: 257665 Color: 1006

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 467897 Color: 9534
Size: 269356 Color: 2083
Size: 262748 Color: 1499

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 467899 Color: 9535
Size: 281657 Color: 3034
Size: 250445 Color: 72

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 468067 Color: 9536
Size: 268782 Color: 2029
Size: 263152 Color: 1547

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 468104 Color: 9537
Size: 270514 Color: 2167
Size: 261383 Color: 1393

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 468110 Color: 9538
Size: 271157 Color: 2217
Size: 260734 Color: 1333

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 468139 Color: 9539
Size: 272544 Color: 2343
Size: 259318 Color: 1183

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 468331 Color: 9540
Size: 271860 Color: 2278
Size: 259810 Color: 1229

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 468416 Color: 9541
Size: 272406 Color: 2330
Size: 259179 Color: 1162

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 468492 Color: 9542
Size: 273315 Color: 2412
Size: 258194 Color: 1059

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 468495 Color: 9543
Size: 273812 Color: 2448
Size: 257694 Color: 1008

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 468556 Color: 9544
Size: 273204 Color: 2402
Size: 258241 Color: 1064

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 468582 Color: 9545
Size: 272691 Color: 2355
Size: 258728 Color: 1121

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 468633 Color: 9546
Size: 276632 Color: 2670
Size: 254736 Color: 676

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 468645 Color: 9547
Size: 280430 Color: 2968
Size: 250926 Color: 163

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 468682 Color: 9548
Size: 266478 Color: 1853
Size: 264841 Color: 1708

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 468739 Color: 9549
Size: 266144 Color: 1815
Size: 265118 Color: 1728

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 468850 Color: 9550
Size: 273812 Color: 2449
Size: 257339 Color: 976

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 468934 Color: 9551
Size: 274308 Color: 2494
Size: 256759 Color: 914

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 468948 Color: 9552
Size: 268883 Color: 2042
Size: 262170 Color: 1459

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 469007 Color: 9553
Size: 270852 Color: 2191
Size: 260142 Color: 1268

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 469182 Color: 9554
Size: 265954 Color: 1801
Size: 264865 Color: 1710

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 469235 Color: 9555
Size: 277085 Color: 2705
Size: 253681 Color: 526

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 469268 Color: 9556
Size: 277907 Color: 2768
Size: 252826 Color: 412

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 469311 Color: 9557
Size: 265423 Color: 1757
Size: 265267 Color: 1746

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 469373 Color: 9558
Size: 275113 Color: 2548
Size: 255515 Color: 771

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 469398 Color: 9559
Size: 273552 Color: 2429
Size: 257051 Color: 955

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 469399 Color: 9560
Size: 278079 Color: 2781
Size: 252523 Color: 371

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 469409 Color: 9561
Size: 277875 Color: 2763
Size: 252717 Color: 396

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 469440 Color: 9562
Size: 266055 Color: 1807
Size: 264506 Color: 1681

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 469480 Color: 9563
Size: 270768 Color: 2185
Size: 259753 Color: 1224

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 469517 Color: 9564
Size: 276559 Color: 2664
Size: 253925 Color: 561

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 469586 Color: 9565
Size: 280382 Color: 2964
Size: 250033 Color: 6

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 469749 Color: 9566
Size: 273466 Color: 2419
Size: 256786 Color: 919

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 469775 Color: 9567
Size: 276864 Color: 2686
Size: 253362 Color: 491

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 469823 Color: 9568
Size: 271251 Color: 2228
Size: 258927 Color: 1146

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 469963 Color: 9569
Size: 273485 Color: 2422
Size: 256553 Color: 891

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 469975 Color: 9570
Size: 269045 Color: 2062
Size: 260981 Color: 1357

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 470018 Color: 9571
Size: 275717 Color: 2590
Size: 254266 Color: 619

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 470061 Color: 9572
Size: 267002 Color: 1887
Size: 262938 Color: 1520

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 470100 Color: 9573
Size: 269547 Color: 2095
Size: 260354 Color: 1296

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 470127 Color: 9574
Size: 273716 Color: 2437
Size: 256158 Color: 847

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 470176 Color: 9575
Size: 271136 Color: 2213
Size: 258689 Color: 1115

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 470327 Color: 9576
Size: 267273 Color: 1911
Size: 262401 Color: 1472

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 470347 Color: 9577
Size: 271022 Color: 2206
Size: 258632 Color: 1105

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 470366 Color: 9578
Size: 266462 Color: 1850
Size: 263173 Color: 1552

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 470373 Color: 9579
Size: 276604 Color: 2669
Size: 253024 Color: 442

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 470392 Color: 9580
Size: 271463 Color: 2244
Size: 258146 Color: 1054

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 470436 Color: 9581
Size: 265553 Color: 1766
Size: 264012 Color: 1630

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 470438 Color: 9582
Size: 277932 Color: 2770
Size: 251631 Color: 264

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 470471 Color: 9583
Size: 279157 Color: 2863
Size: 250373 Color: 64

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 470477 Color: 9584
Size: 271210 Color: 2224
Size: 258314 Color: 1077

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 470486 Color: 9585
Size: 266271 Color: 1827
Size: 263244 Color: 1561

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 470558 Color: 9586
Size: 274347 Color: 2496
Size: 255096 Color: 720

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 470622 Color: 9587
Size: 277370 Color: 2727
Size: 252009 Color: 311

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 470657 Color: 9588
Size: 275229 Color: 2558
Size: 254115 Color: 593

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 470667 Color: 9589
Size: 270365 Color: 2153
Size: 258969 Color: 1152

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 470694 Color: 9590
Size: 278324 Color: 2794
Size: 250983 Color: 169

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 470706 Color: 9591
Size: 277577 Color: 2743
Size: 251718 Color: 275

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 470955 Color: 9592
Size: 270858 Color: 2192
Size: 258188 Color: 1058

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 470955 Color: 9593
Size: 265184 Color: 1737
Size: 263862 Color: 1616

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 470972 Color: 9594
Size: 273464 Color: 2418
Size: 255565 Color: 779

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 470975 Color: 9595
Size: 274384 Color: 2502
Size: 254642 Color: 664

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 471058 Color: 9596
Size: 273941 Color: 2460
Size: 255002 Color: 711

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 471071 Color: 9597
Size: 272011 Color: 2295
Size: 256919 Color: 938

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 471076 Color: 9598
Size: 272397 Color: 2329
Size: 256528 Color: 888

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 471196 Color: 9599
Size: 267462 Color: 1934
Size: 261343 Color: 1389

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 471238 Color: 9600
Size: 276738 Color: 2677
Size: 252025 Color: 313

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 471243 Color: 9601
Size: 269949 Color: 2120
Size: 258809 Color: 1127

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 471246 Color: 9602
Size: 276357 Color: 2643
Size: 252398 Color: 359

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 471371 Color: 9603
Size: 272757 Color: 2364
Size: 255873 Color: 813

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 471388 Color: 9604
Size: 276492 Color: 2654
Size: 252121 Color: 323

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 471404 Color: 9605
Size: 272304 Color: 2320
Size: 256293 Color: 862

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 471466 Color: 9606
Size: 266947 Color: 1883
Size: 261588 Color: 1415

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 471475 Color: 9607
Size: 264847 Color: 1709
Size: 263679 Color: 1603

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 471493 Color: 9608
Size: 265403 Color: 1755
Size: 263105 Color: 1540

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 471507 Color: 9609
Size: 276131 Color: 2623
Size: 252363 Color: 352

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 471533 Color: 9610
Size: 275672 Color: 2588
Size: 252796 Color: 407

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 471595 Color: 9611
Size: 275084 Color: 2547
Size: 253322 Color: 486

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 471731 Color: 9612
Size: 270208 Color: 2139
Size: 258062 Color: 1046

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 471833 Color: 9613
Size: 272390 Color: 2328
Size: 255778 Color: 801

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 471844 Color: 9614
Size: 274093 Color: 2474
Size: 254064 Color: 584

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 471849 Color: 9615
Size: 277456 Color: 2732
Size: 250696 Color: 113

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 471902 Color: 9616
Size: 268270 Color: 1998
Size: 259829 Color: 1232

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 471904 Color: 9617
Size: 272224 Color: 2315
Size: 255873 Color: 814

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 472102 Color: 9618
Size: 272366 Color: 2326
Size: 255533 Color: 776

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 472210 Color: 9619
Size: 271076 Color: 2210
Size: 256715 Color: 908

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 472325 Color: 9620
Size: 275036 Color: 2542
Size: 252640 Color: 387

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 472359 Color: 9621
Size: 274336 Color: 2495
Size: 253306 Color: 485

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 472365 Color: 9622
Size: 277249 Color: 2718
Size: 250387 Color: 67

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 472372 Color: 9623
Size: 264655 Color: 1694
Size: 262974 Color: 1527

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 472395 Color: 9624
Size: 267400 Color: 1927
Size: 260206 Color: 1275

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 472397 Color: 9625
Size: 277063 Color: 2703
Size: 250541 Color: 85

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 472407 Color: 9626
Size: 264440 Color: 1675
Size: 263154 Color: 1548

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 472443 Color: 9627
Size: 265200 Color: 1738
Size: 262358 Color: 1470

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 472453 Color: 9628
Size: 274853 Color: 2527
Size: 252695 Color: 394

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 472521 Color: 9629
Size: 264356 Color: 1666
Size: 263124 Color: 1543

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 472617 Color: 9630
Size: 273817 Color: 2450
Size: 253567 Color: 510

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 472653 Color: 9631
Size: 275404 Color: 2572
Size: 251944 Color: 302

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 472661 Color: 9632
Size: 272865 Color: 2372
Size: 254475 Color: 641

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 472725 Color: 9633
Size: 271877 Color: 2280
Size: 255399 Color: 754

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 472726 Color: 9634
Size: 271833 Color: 2274
Size: 255442 Color: 757

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 472811 Color: 9635
Size: 266621 Color: 1863
Size: 260569 Color: 1319

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 472816 Color: 9636
Size: 271717 Color: 2262
Size: 255468 Color: 763

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 472907 Color: 9637
Size: 272959 Color: 2382
Size: 254135 Color: 598

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 472915 Color: 9638
Size: 271768 Color: 2264
Size: 255318 Color: 747

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 472980 Color: 9639
Size: 272883 Color: 2375
Size: 254138 Color: 600

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 473161 Color: 9640
Size: 270488 Color: 2163
Size: 256352 Color: 869

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 473444 Color: 9641
Size: 267695 Color: 1953
Size: 258862 Color: 1138

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 473622 Color: 9642
Size: 267982 Color: 1972
Size: 258397 Color: 1087

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 473737 Color: 9643
Size: 267508 Color: 1939
Size: 258756 Color: 1125

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 473751 Color: 9644
Size: 272990 Color: 2385
Size: 253260 Color: 480

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 473760 Color: 9645
Size: 268211 Color: 1993
Size: 258030 Color: 1040

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 473788 Color: 9646
Size: 268387 Color: 2004
Size: 257826 Color: 1019

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 473887 Color: 9647
Size: 267479 Color: 1937
Size: 258635 Color: 1106

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 473899 Color: 9648
Size: 272531 Color: 2338
Size: 253571 Color: 512

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 473930 Color: 9649
Size: 264948 Color: 1718
Size: 261123 Color: 1374

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 473974 Color: 9650
Size: 273215 Color: 2405
Size: 252812 Color: 409

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 474063 Color: 9651
Size: 267369 Color: 1921
Size: 258569 Color: 1102

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 474131 Color: 9652
Size: 271847 Color: 2276
Size: 254023 Color: 578

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 474252 Color: 9653
Size: 274806 Color: 2525
Size: 250943 Color: 166

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 474439 Color: 9654
Size: 268927 Color: 2048
Size: 256635 Color: 899

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 474485 Color: 9655
Size: 273732 Color: 2439
Size: 251784 Color: 284

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 474552 Color: 9656
Size: 274682 Color: 2518
Size: 250767 Color: 126

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 474802 Color: 9657
Size: 263627 Color: 1602
Size: 261572 Color: 1413

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 474973 Color: 9658
Size: 271221 Color: 2227
Size: 253807 Color: 546

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 474993 Color: 9659
Size: 264192 Color: 1650
Size: 260816 Color: 1340

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 475133 Color: 9660
Size: 267455 Color: 1933
Size: 257413 Color: 980

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 475176 Color: 9661
Size: 269924 Color: 2117
Size: 254901 Color: 700

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 475193 Color: 9662
Size: 273962 Color: 2464
Size: 250846 Color: 140

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 475333 Color: 9663
Size: 271370 Color: 2237
Size: 253298 Color: 484

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 475445 Color: 9664
Size: 264835 Color: 1706
Size: 259721 Color: 1220

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 475668 Color: 9665
Size: 265687 Color: 1779
Size: 258646 Color: 1108

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 475699 Color: 9666
Size: 267378 Color: 1923
Size: 256924 Color: 941

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 476025 Color: 9667
Size: 265145 Color: 1733
Size: 258831 Color: 1133

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 476033 Color: 9668
Size: 267131 Color: 1899
Size: 256837 Color: 927

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 476110 Color: 9669
Size: 269600 Color: 2098
Size: 254291 Color: 622

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 476142 Color: 9670
Size: 273734 Color: 2440
Size: 250125 Color: 21

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 476291 Color: 9671
Size: 265411 Color: 1756
Size: 258299 Color: 1075

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 476527 Color: 9673
Size: 269256 Color: 2078
Size: 254218 Color: 611

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 476527 Color: 9672
Size: 265767 Color: 1785
Size: 257707 Color: 1010

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 476555 Color: 9674
Size: 269573 Color: 2097
Size: 253873 Color: 554

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 476623 Color: 9675
Size: 272598 Color: 2349
Size: 250780 Color: 129

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 476652 Color: 9676
Size: 271929 Color: 2287
Size: 251420 Color: 240

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 476716 Color: 9677
Size: 272476 Color: 2334
Size: 250809 Color: 134

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 476717 Color: 9678
Size: 271994 Color: 2293
Size: 251290 Color: 220

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 476797 Color: 9679
Size: 268604 Color: 2019
Size: 254600 Color: 658

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 476846 Color: 9680
Size: 261794 Color: 1434
Size: 261361 Color: 1391

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 476860 Color: 9681
Size: 267697 Color: 1954
Size: 255444 Color: 759

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 477017 Color: 9682
Size: 266970 Color: 1886
Size: 256014 Color: 825

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 477091 Color: 9683
Size: 264266 Color: 1656
Size: 258644 Color: 1107

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 477166 Color: 9684
Size: 261823 Color: 1438
Size: 261012 Color: 1361

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 477210 Color: 9685
Size: 269950 Color: 2121
Size: 252841 Color: 413

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 477263 Color: 9686
Size: 264563 Color: 1682
Size: 258175 Color: 1057

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 477319 Color: 9687
Size: 265141 Color: 1732
Size: 257541 Color: 993

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 477338 Color: 9688
Size: 261388 Color: 1396
Size: 261275 Color: 1381

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 477401 Color: 9689
Size: 268028 Color: 1976
Size: 254572 Color: 651

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 477427 Color: 9690
Size: 263612 Color: 1598
Size: 258962 Color: 1149

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 477514 Color: 9691
Size: 270826 Color: 2190
Size: 251661 Color: 267

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 477553 Color: 9692
Size: 271695 Color: 2259
Size: 250753 Color: 123

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 477579 Color: 9693
Size: 270262 Color: 2142
Size: 252160 Color: 326

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 477727 Color: 9694
Size: 264196 Color: 1651
Size: 258078 Color: 1047

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 477764 Color: 9695
Size: 268414 Color: 2008
Size: 253823 Color: 548

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 477789 Color: 9696
Size: 262983 Color: 1530
Size: 259229 Color: 1171

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 477836 Color: 9697
Size: 263355 Color: 1571
Size: 258810 Color: 1128

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 477867 Color: 9698
Size: 266937 Color: 1882
Size: 255197 Color: 734

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 477883 Color: 9699
Size: 265575 Color: 1772
Size: 256543 Color: 890

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 477887 Color: 9700
Size: 267762 Color: 1961
Size: 254352 Color: 626

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 477894 Color: 9701
Size: 261760 Color: 1430
Size: 260347 Color: 1294

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 477896 Color: 9702
Size: 265362 Color: 1753
Size: 256743 Color: 912

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 477946 Color: 9703
Size: 268377 Color: 2003
Size: 253678 Color: 525

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 477995 Color: 9704
Size: 267423 Color: 1930
Size: 254583 Color: 655

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 477995 Color: 9705
Size: 271830 Color: 2273
Size: 250176 Color: 36

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 478038 Color: 9706
Size: 267093 Color: 1896
Size: 254870 Color: 693

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 478106 Color: 9707
Size: 266329 Color: 1835
Size: 255566 Color: 780

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 478226 Color: 9708
Size: 263125 Color: 1544
Size: 258650 Color: 1109

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 478297 Color: 9709
Size: 268048 Color: 1979
Size: 253656 Color: 523

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 478334 Color: 9710
Size: 268071 Color: 1982
Size: 253596 Color: 515

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 478342 Color: 9711
Size: 262465 Color: 1474
Size: 259194 Color: 1167

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 478432 Color: 9712
Size: 270199 Color: 2137
Size: 251370 Color: 231

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 478571 Color: 9713
Size: 261331 Color: 1387
Size: 260099 Color: 1266

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 478594 Color: 9714
Size: 270466 Color: 2160
Size: 250941 Color: 165

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 478719 Color: 9715
Size: 262090 Color: 1453
Size: 259192 Color: 1165

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 478762 Color: 9716
Size: 265346 Color: 1751
Size: 255893 Color: 815

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 478819 Color: 9717
Size: 269676 Color: 2103
Size: 251506 Color: 248

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 478858 Color: 9718
Size: 262226 Color: 1463
Size: 258917 Color: 1145

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 478925 Color: 9719
Size: 263620 Color: 1599
Size: 257456 Color: 985

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 478925 Color: 9720
Size: 264911 Color: 1713
Size: 256165 Color: 850

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 479146 Color: 9721
Size: 270318 Color: 2148
Size: 250537 Color: 83

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 479213 Color: 9722
Size: 261296 Color: 1384
Size: 259492 Color: 1199

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 479393 Color: 9723
Size: 270305 Color: 2147
Size: 250303 Color: 53

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 479395 Color: 9724
Size: 262394 Color: 1471
Size: 258212 Color: 1062

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 479654 Color: 9725
Size: 269518 Color: 2092
Size: 250829 Color: 137

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 479660 Color: 9726
Size: 265132 Color: 1730
Size: 255209 Color: 735

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 479721 Color: 9727
Size: 268613 Color: 2020
Size: 251667 Color: 270

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 479733 Color: 9728
Size: 260866 Color: 1346
Size: 259402 Color: 1189

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 479742 Color: 9729
Size: 265482 Color: 1763
Size: 254777 Color: 679

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 479875 Color: 9730
Size: 263104 Color: 1539
Size: 257022 Color: 949

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 479897 Color: 9731
Size: 266757 Color: 1874
Size: 253347 Color: 490

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 479998 Color: 9732
Size: 269111 Color: 2067
Size: 250892 Color: 157

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 480085 Color: 9733
Size: 262770 Color: 1505
Size: 257146 Color: 963

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 480100 Color: 9734
Size: 262897 Color: 1515
Size: 257004 Color: 945

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 480139 Color: 9735
Size: 268836 Color: 2035
Size: 251026 Color: 180

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 480159 Color: 9736
Size: 262994 Color: 1531
Size: 256848 Color: 932

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 480376 Color: 9737
Size: 269239 Color: 2076
Size: 250386 Color: 66

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 480400 Color: 9738
Size: 267232 Color: 1907
Size: 252369 Color: 353

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 480420 Color: 9739
Size: 266238 Color: 1824
Size: 253343 Color: 489

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 480433 Color: 9740
Size: 262133 Color: 1456
Size: 257435 Color: 984

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 480442 Color: 9741
Size: 261295 Color: 1383
Size: 258264 Color: 1069

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 480487 Color: 9742
Size: 261382 Color: 1392
Size: 258132 Color: 1051

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 480534 Color: 9743
Size: 266102 Color: 1810
Size: 253365 Color: 492

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 480572 Color: 9744
Size: 260281 Color: 1285
Size: 259148 Color: 1160

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 480819 Color: 9745
Size: 259722 Color: 1221
Size: 259460 Color: 1193

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 480873 Color: 9746
Size: 260769 Color: 1337
Size: 258359 Color: 1084

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 480890 Color: 9747
Size: 264976 Color: 1720
Size: 254135 Color: 599

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 480899 Color: 9748
Size: 259560 Color: 1207
Size: 259542 Color: 1203

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 481094 Color: 9749
Size: 268234 Color: 1995
Size: 250673 Color: 108

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 481157 Color: 9750
Size: 260520 Color: 1313
Size: 258324 Color: 1080

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 481262 Color: 9751
Size: 263293 Color: 1567
Size: 255446 Color: 760

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 481282 Color: 9752
Size: 261974 Color: 1447
Size: 256745 Color: 913

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 481284 Color: 9753
Size: 261065 Color: 1367
Size: 257652 Color: 1004

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 481435 Color: 9754
Size: 262182 Color: 1460
Size: 256384 Color: 873

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 481565 Color: 9755
Size: 266582 Color: 1860
Size: 251854 Color: 289

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 481576 Color: 9756
Size: 260032 Color: 1256
Size: 258393 Color: 1086

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 481583 Color: 9757
Size: 261826 Color: 1439
Size: 256592 Color: 896

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 481752 Color: 9758
Size: 259987 Color: 1249
Size: 258262 Color: 1068

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 481801 Color: 9759
Size: 262480 Color: 1476
Size: 255720 Color: 799

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 481903 Color: 9760
Size: 259253 Color: 1174
Size: 258845 Color: 1135

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 482015 Color: 9761
Size: 260435 Color: 1304
Size: 257551 Color: 994

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 482020 Color: 9762
Size: 261544 Color: 1408
Size: 256437 Color: 880

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 482115 Color: 9763
Size: 260370 Color: 1298
Size: 257516 Color: 990

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 482290 Color: 9764
Size: 262255 Color: 1465
Size: 255456 Color: 761

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 482291 Color: 9765
Size: 259174 Color: 1161
Size: 258536 Color: 1098

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 482335 Color: 9766
Size: 261647 Color: 1420
Size: 256019 Color: 826

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 482335 Color: 9767
Size: 259420 Color: 1190
Size: 258246 Color: 1065

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 482366 Color: 9768
Size: 266385 Color: 1839
Size: 251250 Color: 210

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 482369 Color: 9769
Size: 266127 Color: 1813
Size: 251505 Color: 247

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 482412 Color: 9770
Size: 261841 Color: 1441
Size: 255748 Color: 800

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 482528 Color: 9771
Size: 263362 Color: 1572
Size: 254111 Color: 592

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 482600 Color: 9772
Size: 259140 Color: 1159
Size: 258261 Color: 1067

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 482647 Color: 9773
Size: 259897 Color: 1239
Size: 257457 Color: 986

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 482664 Color: 9774
Size: 266521 Color: 1856
Size: 250816 Color: 136

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 482672 Color: 9775
Size: 265735 Color: 1783
Size: 251594 Color: 257

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 482879 Color: 9776
Size: 260292 Color: 1288
Size: 256830 Color: 926

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 483073 Color: 9777
Size: 263392 Color: 1574
Size: 253536 Color: 507

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 483360 Color: 9778
Size: 262620 Color: 1486
Size: 254021 Color: 576

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 483381 Color: 9779
Size: 260560 Color: 1317
Size: 256060 Color: 834

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 483577 Color: 9780
Size: 266422 Color: 1845
Size: 250002 Color: 2

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 483578 Color: 9781
Size: 265588 Color: 1774
Size: 250835 Color: 138

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 483685 Color: 9782
Size: 260609 Color: 1322
Size: 255707 Color: 796

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 483730 Color: 9783
Size: 261829 Color: 1440
Size: 254442 Color: 637

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 483872 Color: 9784
Size: 262960 Color: 1524
Size: 253169 Color: 462

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 483931 Color: 9785
Size: 262345 Color: 1469
Size: 253725 Color: 536

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 484081 Color: 9786
Size: 258758 Color: 1126
Size: 257162 Color: 964

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 484108 Color: 9787
Size: 260840 Color: 1343
Size: 255053 Color: 718

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 484235 Color: 9788
Size: 262644 Color: 1488
Size: 253122 Color: 455

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 484441 Color: 9789
Size: 263120 Color: 1541
Size: 252440 Color: 365

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 484554 Color: 9790
Size: 264577 Color: 1683
Size: 250870 Color: 154

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 484579 Color: 9791
Size: 258278 Color: 1072
Size: 257144 Color: 962

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 484724 Color: 9792
Size: 261945 Color: 1446
Size: 253332 Color: 488

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 484873 Color: 9793
Size: 259776 Color: 1227
Size: 255352 Color: 752

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 484954 Color: 9794
Size: 264429 Color: 1674
Size: 250618 Color: 98

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 485028 Color: 9795
Size: 257862 Color: 1023
Size: 257111 Color: 958

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 485178 Color: 9796
Size: 259814 Color: 1231
Size: 255009 Color: 713

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 485229 Color: 9797
Size: 263546 Color: 1588
Size: 251226 Color: 205

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 485461 Color: 9798
Size: 260937 Color: 1351
Size: 253603 Color: 516

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 485809 Color: 9799
Size: 263871 Color: 1617
Size: 250321 Color: 58

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 485811 Color: 9800
Size: 260712 Color: 1328
Size: 253478 Color: 500

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 485927 Color: 9801
Size: 263953 Color: 1625
Size: 250121 Color: 19

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 485940 Color: 9802
Size: 261391 Color: 1398
Size: 252670 Color: 391

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 485997 Color: 9803
Size: 261408 Color: 1399
Size: 252596 Color: 383

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 486031 Color: 9804
Size: 258934 Color: 1147
Size: 255036 Color: 715

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 486152 Color: 9805
Size: 263120 Color: 1542
Size: 250729 Color: 120

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 486270 Color: 9806
Size: 262712 Color: 1498
Size: 251019 Color: 177

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 486311 Color: 9807
Size: 261298 Color: 1385
Size: 252392 Color: 356

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 486314 Color: 9808
Size: 258447 Color: 1093
Size: 255240 Color: 740

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 486348 Color: 9809
Size: 257866 Color: 1025
Size: 255787 Color: 802

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 486459 Color: 9810
Size: 258122 Color: 1048
Size: 255420 Color: 756

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 486614 Color: 9811
Size: 261205 Color: 1378
Size: 252182 Color: 328

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 486662 Color: 9812
Size: 262909 Color: 1516
Size: 250430 Color: 71

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 486830 Color: 9813
Size: 256765 Color: 915
Size: 256406 Color: 877

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 486899 Color: 9814
Size: 259239 Color: 1173
Size: 253863 Color: 552

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 486939 Color: 9815
Size: 258281 Color: 1073
Size: 254781 Color: 680

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 486953 Color: 9816
Size: 256885 Color: 935
Size: 256163 Color: 849

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 487079 Color: 9817
Size: 262504 Color: 1477
Size: 250418 Color: 69

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 487104 Color: 9818
Size: 257674 Color: 1007
Size: 255223 Color: 739

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 487133 Color: 9819
Size: 260745 Color: 1335
Size: 252123 Color: 324

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 487178 Color: 9820
Size: 262765 Color: 1504
Size: 250058 Color: 8

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 487235 Color: 9821
Size: 257117 Color: 960
Size: 255649 Color: 790

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 487609 Color: 9822
Size: 259394 Color: 1188
Size: 252998 Color: 438

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 487628 Color: 9823
Size: 260002 Color: 1251
Size: 252371 Color: 354

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 487803 Color: 9824
Size: 260589 Color: 1321
Size: 251609 Color: 260

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 488132 Color: 9825
Size: 258304 Color: 1076
Size: 253565 Color: 509

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 488134 Color: 9826
Size: 261019 Color: 1362
Size: 250848 Color: 143

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 488233 Color: 9827
Size: 257600 Color: 1002
Size: 254168 Color: 604

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 488331 Color: 9828
Size: 257738 Color: 1013
Size: 253932 Color: 563

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 488389 Color: 9829
Size: 257864 Color: 1024
Size: 253748 Color: 539

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 488719 Color: 9830
Size: 260964 Color: 1354
Size: 250318 Color: 57

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 488787 Color: 9831
Size: 257578 Color: 999
Size: 253636 Color: 520

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 488833 Color: 9832
Size: 255627 Color: 788
Size: 255541 Color: 777

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 488866 Color: 9833
Size: 259902 Color: 1241
Size: 251233 Color: 208

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 488926 Color: 9834
Size: 255855 Color: 809
Size: 255220 Color: 738

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 488931 Color: 9835
Size: 255941 Color: 818
Size: 255129 Color: 726

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 488955 Color: 9836
Size: 260849 Color: 1344
Size: 250197 Color: 40

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 488963 Color: 9837
Size: 257771 Color: 1014
Size: 253267 Color: 481

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 489087 Color: 9838
Size: 259776 Color: 1226
Size: 251138 Color: 198

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 489115 Color: 9839
Size: 259222 Color: 1170
Size: 251664 Color: 268

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 489118 Color: 9840
Size: 258035 Color: 1041
Size: 252848 Color: 416

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 489155 Color: 9841
Size: 260676 Color: 1325
Size: 250170 Color: 33

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 489172 Color: 9842
Size: 260024 Color: 1253
Size: 250805 Color: 133

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 489205 Color: 9843
Size: 257226 Color: 970
Size: 253570 Color: 511

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 489266 Color: 9844
Size: 258989 Color: 1155
Size: 251746 Color: 280

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 489267 Color: 9845
Size: 258147 Color: 1055
Size: 252587 Color: 381

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 489535 Color: 9846
Size: 255974 Color: 822
Size: 254492 Color: 644

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 489602 Color: 9847
Size: 257393 Color: 979
Size: 253006 Color: 440

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 489622 Color: 9848
Size: 260232 Color: 1278
Size: 250147 Color: 25

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 489658 Color: 9849
Size: 259487 Color: 1196
Size: 250856 Color: 147

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 489677 Color: 9850
Size: 257855 Color: 1022
Size: 252469 Color: 366

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 489843 Color: 9851
Size: 256094 Color: 838
Size: 254064 Color: 585

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 489920 Color: 9852
Size: 259279 Color: 1181
Size: 250802 Color: 131

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 489976 Color: 9853
Size: 256768 Color: 917
Size: 253257 Color: 478

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 490080 Color: 9854
Size: 257995 Color: 1038
Size: 251926 Color: 300

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 490092 Color: 9856
Size: 258207 Color: 1061
Size: 251702 Color: 274

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 490092 Color: 9855
Size: 259220 Color: 1169
Size: 250689 Color: 111

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 490137 Color: 9857
Size: 255287 Color: 744
Size: 254577 Color: 652

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 490261 Color: 9858
Size: 259558 Color: 1206
Size: 250182 Color: 39

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 490287 Color: 9859
Size: 257989 Color: 1037
Size: 251725 Color: 276

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 490376 Color: 9860
Size: 257095 Color: 956
Size: 252530 Color: 372

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 490562 Color: 9862
Size: 254991 Color: 710
Size: 254448 Color: 638

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 490562 Color: 9861
Size: 255572 Color: 782
Size: 253867 Color: 553

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 490594 Color: 9863
Size: 258821 Color: 1132
Size: 250586 Color: 94

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 490597 Color: 9864
Size: 258849 Color: 1137
Size: 250555 Color: 89

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 490676 Color: 9865
Size: 257790 Color: 1016
Size: 251535 Color: 251

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 490767 Color: 9866
Size: 256330 Color: 866
Size: 252904 Color: 426

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 490789 Color: 9867
Size: 257010 Color: 947
Size: 252202 Color: 334

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 490794 Color: 9868
Size: 257435 Color: 983
Size: 251772 Color: 283

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 490831 Color: 9869
Size: 256665 Color: 904
Size: 252505 Color: 369

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 490911 Color: 9870
Size: 254597 Color: 657
Size: 254493 Color: 645

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 490966 Color: 9871
Size: 256498 Color: 884
Size: 252537 Color: 374

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 491016 Color: 9872
Size: 254786 Color: 681
Size: 254199 Color: 608

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 491033 Color: 9873
Size: 254893 Color: 697
Size: 254075 Color: 587

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 491129 Color: 9875
Size: 257979 Color: 1036
Size: 250893 Color: 158

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 491129 Color: 9874
Size: 258399 Color: 1089
Size: 250473 Color: 78

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 491158 Color: 9876
Size: 256847 Color: 929
Size: 251996 Color: 309

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 491219 Color: 9877
Size: 256810 Color: 923
Size: 251972 Color: 305

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 491245 Color: 9878
Size: 254551 Color: 650
Size: 254205 Color: 609

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 491303 Color: 9879
Size: 257371 Color: 977
Size: 251327 Color: 227

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 491307 Color: 9880
Size: 255592 Color: 785
Size: 253102 Color: 451

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 491331 Color: 9881
Size: 256652 Color: 902
Size: 252018 Color: 312

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 491528 Color: 9882
Size: 257461 Color: 987
Size: 251012 Color: 175

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 491636 Color: 9883
Size: 255131 Color: 727
Size: 253234 Color: 475

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 491668 Color: 9884
Size: 254208 Color: 610
Size: 254125 Color: 596

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 491742 Color: 9885
Size: 255305 Color: 746
Size: 252954 Color: 434

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 491758 Color: 9886
Size: 256516 Color: 887
Size: 251727 Color: 277

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 491840 Color: 9887
Size: 254276 Color: 620
Size: 253885 Color: 555

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 491851 Color: 9888
Size: 255504 Color: 766
Size: 252646 Color: 388

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 491939 Color: 9889
Size: 257387 Color: 978
Size: 250675 Color: 109

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 491955 Color: 9890
Size: 257207 Color: 968
Size: 250839 Color: 139

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 492174 Color: 9891
Size: 254639 Color: 663
Size: 253188 Color: 465

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 492187 Color: 9892
Size: 256430 Color: 879
Size: 251384 Color: 233

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 492193 Color: 9893
Size: 254888 Color: 696
Size: 252920 Color: 429

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 492214 Color: 9894
Size: 256151 Color: 844
Size: 251636 Color: 265

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 492386 Color: 9895
Size: 256558 Color: 892
Size: 251057 Color: 186

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 492437 Color: 9896
Size: 253855 Color: 551
Size: 253709 Color: 529

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 492498 Color: 9897
Size: 256656 Color: 903
Size: 250847 Color: 141

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 492617 Color: 9898
Size: 254256 Color: 617
Size: 253128 Color: 457

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 492681 Color: 9899
Size: 254284 Color: 621
Size: 253036 Color: 444

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 492756 Color: 9900
Size: 256393 Color: 875
Size: 250852 Color: 145

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 492891 Color: 9901
Size: 254726 Color: 675
Size: 252384 Color: 355

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 492942 Color: 9902
Size: 255328 Color: 749
Size: 251731 Color: 278

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 492983 Color: 9903
Size: 254664 Color: 667
Size: 252354 Color: 351

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 493132 Color: 9904
Size: 255544 Color: 778
Size: 251325 Color: 226

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 493219 Color: 9905
Size: 254908 Color: 701
Size: 251874 Color: 292

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 493279 Color: 9906
Size: 253575 Color: 513
Size: 253147 Color: 460

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 493325 Color: 9907
Size: 254348 Color: 625
Size: 252328 Color: 347

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 493409 Color: 9908
Size: 254253 Color: 616
Size: 252339 Color: 350

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 493430 Color: 9909
Size: 254152 Color: 602
Size: 252419 Color: 362

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 493578 Color: 9910
Size: 256244 Color: 855
Size: 250179 Color: 37

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 493601 Color: 9911
Size: 253790 Color: 543
Size: 252610 Color: 384

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 493622 Color: 9912
Size: 255514 Color: 770
Size: 250865 Color: 151

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 493689 Color: 9913
Size: 255328 Color: 750
Size: 250984 Color: 170

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 493715 Color: 9914
Size: 255570 Color: 781
Size: 250716 Color: 118

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 493750 Color: 9915
Size: 256082 Color: 837
Size: 250169 Color: 31

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 493810 Color: 9916
Size: 254918 Color: 703
Size: 251273 Color: 216

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 493852 Color: 9917
Size: 255084 Color: 719
Size: 251065 Color: 187

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 493871 Color: 9918
Size: 253234 Color: 476
Size: 252896 Color: 424

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 493914 Color: 9919
Size: 254698 Color: 672
Size: 251389 Color: 235

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 493978 Color: 9920
Size: 254755 Color: 678
Size: 251268 Color: 214

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 493985 Color: 9921
Size: 254032 Color: 581
Size: 251984 Color: 307

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 494016 Color: 9922
Size: 255819 Color: 806
Size: 250166 Color: 30

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 494081 Color: 9923
Size: 254124 Color: 595
Size: 251796 Color: 285

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 494144 Color: 9924
Size: 255574 Color: 784
Size: 250283 Color: 51

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 494245 Color: 9925
Size: 254638 Color: 662
Size: 251118 Color: 195

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 494324 Color: 9926
Size: 255529 Color: 774
Size: 250148 Color: 26

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 494421 Color: 9927
Size: 254915 Color: 702
Size: 250665 Color: 106

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 494475 Color: 9928
Size: 254845 Color: 691
Size: 250681 Color: 110

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 494511 Color: 9929
Size: 255215 Color: 737
Size: 250275 Color: 50

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 494557 Color: 9930
Size: 254579 Color: 653
Size: 250865 Color: 152

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 494564 Color: 9931
Size: 255176 Color: 732
Size: 250261 Color: 48

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 494838 Color: 9932
Size: 253137 Color: 458
Size: 252026 Color: 315

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 494890 Color: 9933
Size: 253494 Color: 502
Size: 251617 Color: 262

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 495047 Color: 9934
Size: 254862 Color: 692
Size: 250092 Color: 15

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 495052 Color: 9935
Size: 253630 Color: 518
Size: 251319 Color: 224

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 495070 Color: 9936
Size: 254581 Color: 654
Size: 250350 Color: 61

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 495083 Color: 9937
Size: 252591 Color: 382
Size: 252327 Color: 346

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 495093 Color: 9938
Size: 253232 Color: 472
Size: 251676 Color: 271

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 495181 Color: 9939
Size: 253217 Color: 470
Size: 251603 Color: 259

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 495282 Color: 9940
Size: 253222 Color: 471
Size: 251497 Color: 244

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 495290 Color: 9941
Size: 253749 Color: 540
Size: 250962 Color: 168

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 495327 Color: 9942
Size: 254369 Color: 628
Size: 250305 Color: 54

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 495420 Color: 9943
Size: 253975 Color: 570
Size: 250606 Color: 95

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 495473 Color: 9944
Size: 253831 Color: 549
Size: 250697 Color: 114

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 495636 Color: 9945
Size: 254337 Color: 624
Size: 250028 Color: 4

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 495643 Color: 9946
Size: 253498 Color: 503
Size: 250860 Color: 148

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 495677 Color: 9947
Size: 252920 Color: 428
Size: 251404 Color: 237

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 495682 Color: 9948
Size: 254017 Color: 575
Size: 250302 Color: 52

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 495714 Color: 9949
Size: 253191 Color: 466
Size: 251096 Color: 190

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 495736 Color: 9950
Size: 253712 Color: 530
Size: 250553 Color: 88

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 495766 Color: 9951
Size: 253233 Color: 474
Size: 251002 Color: 173

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 495805 Color: 9952
Size: 253382 Color: 494
Size: 250814 Color: 135

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 495935 Color: 9953
Size: 252066 Color: 319
Size: 252000 Color: 310

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 496065 Color: 9954
Size: 252928 Color: 431
Size: 251008 Color: 174

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 496092 Color: 9955
Size: 252622 Color: 385
Size: 251287 Color: 218

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 496194 Color: 9956
Size: 252812 Color: 410
Size: 250995 Color: 172

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 496273 Color: 9957
Size: 253578 Color: 514
Size: 250150 Color: 27

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 496323 Color: 9958
Size: 253117 Color: 454
Size: 250561 Color: 91

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 496337 Color: 9959
Size: 253043 Color: 445
Size: 250621 Color: 99

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 496462 Color: 9960
Size: 252898 Color: 425
Size: 250641 Color: 102

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 496570 Color: 9961
Size: 253068 Color: 447
Size: 250363 Color: 63

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 496661 Color: 9962
Size: 252424 Color: 363
Size: 250916 Color: 161

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 496773 Color: 9963
Size: 252531 Color: 373
Size: 250697 Color: 115

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 496794 Color: 9964
Size: 251609 Color: 261
Size: 251598 Color: 258

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 496874 Color: 9965
Size: 252237 Color: 339
Size: 250890 Color: 156

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 496900 Color: 9966
Size: 251957 Color: 303
Size: 251144 Color: 199

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 496941 Color: 9967
Size: 252411 Color: 360
Size: 250649 Color: 103

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 496978 Color: 9968
Size: 251828 Color: 287
Size: 251195 Color: 202

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 497035 Color: 9969
Size: 251689 Color: 273
Size: 251277 Color: 217

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 497124 Color: 9970
Size: 251762 Color: 281
Size: 251115 Color: 194

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 497170 Color: 9971
Size: 252491 Color: 367
Size: 250340 Color: 59

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 497186 Color: 9972
Size: 252199 Color: 332
Size: 250616 Color: 97

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 497199 Color: 9973
Size: 251666 Color: 269
Size: 251136 Color: 197

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 497224 Color: 9974
Size: 252576 Color: 377
Size: 250201 Color: 41

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 497320 Color: 9975
Size: 252253 Color: 341
Size: 250428 Color: 70

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 497539 Color: 9976
Size: 251236 Color: 209
Size: 251226 Color: 206

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 497563 Color: 9977
Size: 251382 Color: 232
Size: 251056 Color: 185

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 497667 Color: 9978
Size: 252184 Color: 329
Size: 250150 Color: 28

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 497670 Color: 9979
Size: 251571 Color: 256
Size: 250760 Color: 124

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 497711 Color: 9980
Size: 252049 Color: 317
Size: 250241 Color: 47

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 497792 Color: 9981
Size: 251361 Color: 230
Size: 250848 Color: 142

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 497796 Color: 9982
Size: 251500 Color: 246
Size: 250705 Color: 116

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 497832 Color: 9983
Size: 251943 Color: 301
Size: 250226 Color: 45

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 498044 Color: 9984
Size: 251413 Color: 239
Size: 250544 Color: 86

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 498263 Color: 9985
Size: 251161 Color: 200
Size: 250577 Color: 93

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 498407 Color: 9986
Size: 250988 Color: 171
Size: 250606 Color: 96

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 498431 Color: 9987
Size: 251035 Color: 181
Size: 250535 Color: 82

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 498512 Color: 9988
Size: 251255 Color: 212
Size: 250234 Color: 46

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 498809 Color: 9989
Size: 251104 Color: 191
Size: 250088 Color: 12

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 498815 Color: 9990
Size: 250694 Color: 112
Size: 250492 Color: 81

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 498871 Color: 9991
Size: 251017 Color: 176
Size: 250113 Color: 17

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 498966 Color: 9992
Size: 250549 Color: 87
Size: 250486 Color: 79

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 499034 Color: 9993
Size: 250853 Color: 146
Size: 250114 Color: 18

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 499058 Color: 9994
Size: 250632 Color: 101
Size: 250311 Color: 56

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 499081 Color: 9995
Size: 250774 Color: 128
Size: 250146 Color: 24

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 499196 Color: 9996
Size: 250716 Color: 117
Size: 250089 Color: 13

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 499201 Color: 9997
Size: 250537 Color: 84
Size: 250263 Color: 49

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 499381 Color: 9998
Size: 250457 Color: 76
Size: 250163 Color: 29

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 499582 Color: 9999
Size: 250341 Color: 60
Size: 250078 Color: 11

Bin 2446: 1 of cap free
Amount of items: 3
Items: 
Size: 366763 Color: 7045
Size: 316909 Color: 5061
Size: 316328 Color: 5036

Bin 2447: 1 of cap free
Amount of items: 3
Items: 
Size: 366915 Color: 7049
Size: 318539 Color: 5150
Size: 314546 Color: 4940

Bin 2448: 1 of cap free
Amount of items: 3
Items: 
Size: 366969 Color: 7050
Size: 320200 Color: 5239
Size: 312831 Color: 4856

Bin 2449: 1 of cap free
Amount of items: 3
Items: 
Size: 367054 Color: 7052
Size: 317935 Color: 5108
Size: 315011 Color: 4957

Bin 2450: 1 of cap free
Amount of items: 3
Items: 
Size: 367098 Color: 7057
Size: 318517 Color: 5147
Size: 314385 Color: 4927

Bin 2451: 1 of cap free
Amount of items: 3
Items: 
Size: 367233 Color: 7061
Size: 320537 Color: 5251
Size: 312230 Color: 4831

Bin 2452: 1 of cap free
Amount of items: 3
Items: 
Size: 369142 Color: 7122
Size: 346936 Color: 6326
Size: 283922 Color: 3212

Bin 2453: 1 of cap free
Amount of items: 3
Items: 
Size: 370111 Color: 7147
Size: 321238 Color: 5290
Size: 308651 Color: 4631

Bin 2454: 1 of cap free
Amount of items: 3
Items: 
Size: 370199 Color: 7152
Size: 319396 Color: 5191
Size: 310405 Color: 4742

Bin 2455: 1 of cap free
Amount of items: 3
Items: 
Size: 375973 Color: 7317
Size: 345624 Color: 6269
Size: 278403 Color: 2800

Bin 2456: 1 of cap free
Amount of items: 3
Items: 
Size: 376419 Color: 7333
Size: 345453 Color: 6263
Size: 278128 Color: 2784

Bin 2457: 1 of cap free
Amount of items: 3
Items: 
Size: 377867 Color: 7375
Size: 345024 Color: 6247
Size: 277109 Color: 2706

Bin 2458: 1 of cap free
Amount of items: 3
Items: 
Size: 378750 Color: 7401
Size: 344611 Color: 6235
Size: 276639 Color: 2671

Bin 2459: 1 of cap free
Amount of items: 3
Items: 
Size: 379147 Color: 7415
Size: 344395 Color: 6232
Size: 276458 Color: 2649

Bin 2460: 1 of cap free
Amount of items: 3
Items: 
Size: 379313 Color: 7424
Size: 344256 Color: 6226
Size: 276431 Color: 2647

Bin 2461: 1 of cap free
Amount of items: 3
Items: 
Size: 390260 Color: 7736
Size: 339746 Color: 6049
Size: 269994 Color: 2123

Bin 2462: 1 of cap free
Amount of items: 3
Items: 
Size: 392401 Color: 7810
Size: 338957 Color: 6018
Size: 268642 Color: 2022

Bin 2463: 1 of cap free
Amount of items: 3
Items: 
Size: 394387 Color: 7873
Size: 338259 Color: 5995
Size: 267354 Color: 1919

Bin 2464: 1 of cap free
Amount of items: 3
Items: 
Size: 412227 Color: 8325
Size: 337562 Color: 5959
Size: 250211 Color: 44

Bin 2465: 1 of cap free
Amount of items: 3
Items: 
Size: 394787 Color: 7885
Size: 337983 Color: 5981
Size: 267230 Color: 1906

Bin 2466: 1 of cap free
Amount of items: 3
Items: 
Size: 364152 Color: 6955
Size: 347049 Color: 6333
Size: 288799 Color: 3522

Bin 2467: 1 of cap free
Amount of items: 3
Items: 
Size: 366494 Color: 7037
Size: 318425 Color: 5140
Size: 315081 Color: 4960

Bin 2468: 1 of cap free
Amount of items: 3
Items: 
Size: 410998 Color: 8295
Size: 318411 Color: 5139
Size: 270591 Color: 2173

Bin 2469: 1 of cap free
Amount of items: 3
Items: 
Size: 379922 Color: 7443
Size: 344061 Color: 6221
Size: 276017 Color: 2615

Bin 2470: 1 of cap free
Amount of items: 3
Items: 
Size: 380083 Color: 7450
Size: 343368 Color: 6193
Size: 276549 Color: 2660

Bin 2471: 1 of cap free
Amount of items: 3
Items: 
Size: 354716 Color: 6647
Size: 352943 Color: 6589
Size: 292341 Color: 3738

Bin 2472: 1 of cap free
Amount of items: 3
Items: 
Size: 368352 Color: 7092
Size: 347807 Color: 6359
Size: 283841 Color: 3204

Bin 2473: 1 of cap free
Amount of items: 3
Items: 
Size: 407439 Color: 8212
Size: 339360 Color: 6032
Size: 253201 Color: 468

Bin 2474: 1 of cap free
Amount of items: 3
Items: 
Size: 363706 Color: 6938
Size: 340783 Color: 6091
Size: 295511 Color: 3904

Bin 2475: 1 of cap free
Amount of items: 3
Items: 
Size: 371010 Color: 7176
Size: 346727 Color: 6315
Size: 282263 Color: 3093

Bin 2476: 1 of cap free
Amount of items: 3
Items: 
Size: 385923 Color: 7617
Size: 341748 Color: 6127
Size: 272329 Color: 2322

Bin 2477: 1 of cap free
Amount of items: 3
Items: 
Size: 391181 Color: 7773
Size: 339606 Color: 6042
Size: 269213 Color: 2073

Bin 2478: 1 of cap free
Amount of items: 3
Items: 
Size: 384447 Color: 7580
Size: 342425 Color: 6154
Size: 273128 Color: 2397

Bin 2479: 1 of cap free
Amount of items: 3
Items: 
Size: 372066 Color: 7214
Size: 338940 Color: 6016
Size: 288994 Color: 3534

Bin 2480: 1 of cap free
Amount of items: 3
Items: 
Size: 365224 Color: 6992
Size: 350540 Color: 6471
Size: 284236 Color: 3236

Bin 2481: 1 of cap free
Amount of items: 3
Items: 
Size: 395453 Color: 7903
Size: 336979 Color: 5933
Size: 267568 Color: 1942

Bin 2482: 1 of cap free
Amount of items: 3
Items: 
Size: 410570 Color: 8284
Size: 333986 Color: 5821
Size: 255444 Color: 758

Bin 2483: 1 of cap free
Amount of items: 3
Items: 
Size: 382115 Color: 7519
Size: 343580 Color: 6200
Size: 274305 Color: 2492

Bin 2484: 1 of cap free
Amount of items: 3
Items: 
Size: 373032 Color: 7233
Size: 354019 Color: 6627
Size: 272949 Color: 2380

Bin 2485: 1 of cap free
Amount of items: 3
Items: 
Size: 365388 Color: 7001
Size: 342274 Color: 6147
Size: 292338 Color: 3737

Bin 2486: 1 of cap free
Amount of items: 3
Items: 
Size: 383201 Color: 7542
Size: 342833 Color: 6170
Size: 273966 Color: 2465

Bin 2487: 1 of cap free
Amount of items: 3
Items: 
Size: 374258 Color: 7268
Size: 322879 Color: 5375
Size: 302863 Color: 4318

Bin 2488: 1 of cap free
Amount of items: 3
Items: 
Size: 398992 Color: 7984
Size: 338036 Color: 5984
Size: 262972 Color: 1525

Bin 2489: 1 of cap free
Amount of items: 3
Items: 
Size: 368474 Color: 7099
Size: 321778 Color: 5313
Size: 309748 Color: 4706

Bin 2490: 1 of cap free
Amount of items: 3
Items: 
Size: 369555 Color: 7134
Size: 346782 Color: 6316
Size: 283663 Color: 3192

Bin 2491: 1 of cap free
Amount of items: 3
Items: 
Size: 393432 Color: 7844
Size: 338517 Color: 6007
Size: 268051 Color: 1980

Bin 2492: 1 of cap free
Amount of items: 3
Items: 
Size: 374552 Color: 7275
Size: 351490 Color: 6523
Size: 273958 Color: 2462

Bin 2493: 1 of cap free
Amount of items: 3
Items: 
Size: 354866 Color: 6650
Size: 350933 Color: 6498
Size: 294201 Color: 3826

Bin 2494: 1 of cap free
Amount of items: 3
Items: 
Size: 412427 Color: 8332
Size: 337538 Color: 5956
Size: 250035 Color: 7

Bin 2495: 1 of cap free
Amount of items: 3
Items: 
Size: 406517 Color: 8179
Size: 324467 Color: 5429
Size: 269016 Color: 2058

Bin 2496: 1 of cap free
Amount of items: 3
Items: 
Size: 387960 Color: 7672
Size: 340756 Color: 6089
Size: 271284 Color: 2230

Bin 2497: 1 of cap free
Amount of items: 3
Items: 
Size: 376753 Color: 7346
Size: 343624 Color: 6202
Size: 279623 Color: 2894

Bin 2498: 1 of cap free
Amount of items: 3
Items: 
Size: 387615 Color: 7662
Size: 341169 Color: 6108
Size: 271216 Color: 2226

Bin 2499: 1 of cap free
Amount of items: 3
Items: 
Size: 383710 Color: 7562
Size: 342708 Color: 6164
Size: 273582 Color: 2431

Bin 2500: 1 of cap free
Amount of items: 3
Items: 
Size: 387424 Color: 7656
Size: 340902 Color: 6097
Size: 271674 Color: 2257

Bin 2501: 1 of cap free
Amount of items: 3
Items: 
Size: 411743 Color: 8314
Size: 325965 Color: 5498
Size: 262292 Color: 1466

Bin 2502: 1 of cap free
Amount of items: 3
Items: 
Size: 389278 Color: 7714
Size: 339984 Color: 6062
Size: 270738 Color: 2181

Bin 2503: 1 of cap free
Amount of items: 3
Items: 
Size: 363565 Color: 6936
Size: 347115 Color: 6337
Size: 289320 Color: 3555

Bin 2504: 1 of cap free
Amount of items: 3
Items: 
Size: 363525 Color: 6934
Size: 347109 Color: 6336
Size: 289366 Color: 3557

Bin 2505: 1 of cap free
Amount of items: 3
Items: 
Size: 390530 Color: 7741
Size: 330454 Color: 5678
Size: 279016 Color: 2848

Bin 2506: 1 of cap free
Amount of items: 3
Items: 
Size: 382424 Color: 7525
Size: 343296 Color: 6188
Size: 274280 Color: 2490

Bin 2507: 1 of cap free
Amount of items: 3
Items: 
Size: 411809 Color: 8316
Size: 337303 Color: 5945
Size: 250888 Color: 155

Bin 2508: 1 of cap free
Amount of items: 3
Items: 
Size: 384523 Color: 7581
Size: 342168 Color: 6142
Size: 273309 Color: 2411

Bin 2509: 1 of cap free
Amount of items: 3
Items: 
Size: 382477 Color: 7528
Size: 337820 Color: 5972
Size: 279703 Color: 2904

Bin 2510: 1 of cap free
Amount of items: 3
Items: 
Size: 411096 Color: 8298
Size: 336914 Color: 5931
Size: 251990 Color: 308

Bin 2511: 1 of cap free
Amount of items: 3
Items: 
Size: 412324 Color: 8327
Size: 333683 Color: 5807
Size: 253993 Color: 572

Bin 2512: 1 of cap free
Amount of items: 3
Items: 
Size: 409417 Color: 8251
Size: 335481 Color: 5872
Size: 255102 Color: 722

Bin 2513: 1 of cap free
Amount of items: 3
Items: 
Size: 410687 Color: 8289
Size: 337106 Color: 5938
Size: 252207 Color: 335

Bin 2514: 1 of cap free
Amount of items: 3
Items: 
Size: 407111 Color: 8203
Size: 319739 Color: 5212
Size: 273150 Color: 2398

Bin 2515: 1 of cap free
Amount of items: 3
Items: 
Size: 409022 Color: 8243
Size: 335446 Color: 5870
Size: 255532 Color: 775

Bin 2516: 1 of cap free
Amount of items: 3
Items: 
Size: 408848 Color: 8241
Size: 336327 Color: 5918
Size: 254825 Color: 689

Bin 2517: 1 of cap free
Amount of items: 3
Items: 
Size: 350869 Color: 6493
Size: 345029 Color: 6248
Size: 304102 Color: 4398

Bin 2518: 1 of cap free
Amount of items: 3
Items: 
Size: 408600 Color: 8234
Size: 335233 Color: 5859
Size: 256167 Color: 851

Bin 2519: 1 of cap free
Amount of items: 3
Items: 
Size: 405445 Color: 8146
Size: 316328 Color: 5035
Size: 278227 Color: 2789

Bin 2520: 1 of cap free
Amount of items: 3
Items: 
Size: 405542 Color: 8149
Size: 332836 Color: 5770
Size: 261622 Color: 1419

Bin 2521: 1 of cap free
Amount of items: 3
Items: 
Size: 405403 Color: 8144
Size: 334596 Color: 5841
Size: 260001 Color: 1250

Bin 2522: 1 of cap free
Amount of items: 3
Items: 
Size: 405902 Color: 8160
Size: 333112 Color: 5783
Size: 260986 Color: 1358

Bin 2523: 1 of cap free
Amount of items: 3
Items: 
Size: 404654 Color: 8125
Size: 339354 Color: 6031
Size: 255992 Color: 824

Bin 2524: 1 of cap free
Amount of items: 3
Items: 
Size: 405888 Color: 8159
Size: 333077 Color: 5780
Size: 261035 Color: 1365

Bin 2525: 1 of cap free
Amount of items: 3
Items: 
Size: 404954 Color: 8132
Size: 332490 Color: 5759
Size: 262556 Color: 1483

Bin 2526: 1 of cap free
Amount of items: 3
Items: 
Size: 406133 Color: 8165
Size: 335547 Color: 5875
Size: 258320 Color: 1078

Bin 2527: 1 of cap free
Amount of items: 3
Items: 
Size: 402607 Color: 8075
Size: 337127 Color: 5939
Size: 260266 Color: 1282

Bin 2528: 1 of cap free
Amount of items: 3
Items: 
Size: 406850 Color: 8196
Size: 336505 Color: 5922
Size: 256645 Color: 901

Bin 2529: 1 of cap free
Amount of items: 3
Items: 
Size: 401463 Color: 8044
Size: 345458 Color: 6264
Size: 253079 Color: 450

Bin 2530: 1 of cap free
Amount of items: 3
Items: 
Size: 382324 Color: 7523
Size: 332312 Color: 5749
Size: 285364 Color: 3324

Bin 2531: 1 of cap free
Amount of items: 3
Items: 
Size: 403857 Color: 8103
Size: 331840 Color: 5723
Size: 264303 Color: 1659

Bin 2532: 1 of cap free
Amount of items: 3
Items: 
Size: 406890 Color: 8198
Size: 331508 Color: 5712
Size: 261602 Color: 1416

Bin 2533: 1 of cap free
Amount of items: 3
Items: 
Size: 354477 Color: 6643
Size: 327182 Color: 5550
Size: 318341 Color: 5132

Bin 2534: 1 of cap free
Amount of items: 3
Items: 
Size: 408997 Color: 8242
Size: 330205 Color: 5665
Size: 260798 Color: 1338

Bin 2535: 1 of cap free
Amount of items: 3
Items: 
Size: 404834 Color: 8129
Size: 329444 Color: 5639
Size: 265722 Color: 1781

Bin 2536: 1 of cap free
Amount of items: 3
Items: 
Size: 402841 Color: 8082
Size: 330990 Color: 5698
Size: 266169 Color: 1817

Bin 2537: 1 of cap free
Amount of items: 3
Items: 
Size: 403026 Color: 8086
Size: 329736 Color: 5650
Size: 267238 Color: 1908

Bin 2538: 1 of cap free
Amount of items: 3
Items: 
Size: 402347 Color: 8069
Size: 330596 Color: 5683
Size: 267057 Color: 1891

Bin 2539: 1 of cap free
Amount of items: 3
Items: 
Size: 402327 Color: 8068
Size: 330489 Color: 5681
Size: 267184 Color: 1903

Bin 2540: 1 of cap free
Amount of items: 3
Items: 
Size: 347499 Color: 6347
Size: 331597 Color: 5718
Size: 320904 Color: 5271

Bin 2541: 1 of cap free
Amount of items: 3
Items: 
Size: 406475 Color: 8178
Size: 330311 Color: 5668
Size: 263214 Color: 1557

Bin 2542: 1 of cap free
Amount of items: 3
Items: 
Size: 354687 Color: 6646
Size: 330354 Color: 5671
Size: 314959 Color: 4954

Bin 2543: 1 of cap free
Amount of items: 3
Items: 
Size: 401459 Color: 8043
Size: 335088 Color: 5850
Size: 263453 Color: 1581

Bin 2544: 1 of cap free
Amount of items: 3
Items: 
Size: 401093 Color: 8035
Size: 333096 Color: 5782
Size: 265811 Color: 1790

Bin 2545: 1 of cap free
Amount of items: 3
Items: 
Size: 408284 Color: 8229
Size: 327693 Color: 5571
Size: 264023 Color: 1632

Bin 2546: 1 of cap free
Amount of items: 3
Items: 
Size: 400876 Color: 8031
Size: 334411 Color: 5836
Size: 264713 Color: 1697

Bin 2547: 1 of cap free
Amount of items: 3
Items: 
Size: 400557 Color: 8023
Size: 333724 Color: 5810
Size: 265719 Color: 1780

Bin 2548: 1 of cap free
Amount of items: 3
Items: 
Size: 399843 Color: 8006
Size: 342571 Color: 6159
Size: 257586 Color: 1000

Bin 2549: 1 of cap free
Amount of items: 3
Items: 
Size: 399681 Color: 8001
Size: 335430 Color: 5869
Size: 264889 Color: 1711

Bin 2550: 1 of cap free
Amount of items: 3
Items: 
Size: 399584 Color: 7997
Size: 328322 Color: 5601
Size: 272094 Color: 2301

Bin 2551: 1 of cap free
Amount of items: 3
Items: 
Size: 398975 Color: 7983
Size: 328074 Color: 5585
Size: 272951 Color: 2381

Bin 2552: 1 of cap free
Amount of items: 3
Items: 
Size: 385729 Color: 7613
Size: 342416 Color: 6152
Size: 271855 Color: 2277

Bin 2553: 1 of cap free
Amount of items: 3
Items: 
Size: 374516 Color: 7274
Size: 333253 Color: 5791
Size: 292231 Color: 3734

Bin 2554: 1 of cap free
Amount of items: 3
Items: 
Size: 372167 Color: 7217
Size: 346356 Color: 6302
Size: 281477 Color: 3025

Bin 2555: 1 of cap free
Amount of items: 3
Items: 
Size: 365997 Color: 7020
Size: 352340 Color: 6561
Size: 281663 Color: 3035

Bin 2556: 1 of cap free
Amount of items: 3
Items: 
Size: 370830 Color: 7172
Size: 360670 Color: 6833
Size: 268500 Color: 2014

Bin 2557: 1 of cap free
Amount of items: 3
Items: 
Size: 340039 Color: 6065
Size: 335904 Color: 5891
Size: 324057 Color: 5418

Bin 2558: 1 of cap free
Amount of items: 3
Items: 
Size: 368865 Color: 7113
Size: 324105 Color: 5420
Size: 307030 Color: 4555

Bin 2559: 1 of cap free
Amount of items: 3
Items: 
Size: 368857 Color: 7112
Size: 346945 Color: 6327
Size: 284198 Color: 3232

Bin 2560: 1 of cap free
Amount of items: 3
Items: 
Size: 400620 Color: 8024
Size: 314416 Color: 4931
Size: 284964 Color: 3292

Bin 2561: 1 of cap free
Amount of items: 3
Items: 
Size: 368982 Color: 7116
Size: 324439 Color: 5425
Size: 306579 Color: 4532

Bin 2562: 1 of cap free
Amount of items: 3
Items: 
Size: 368546 Color: 7102
Size: 346487 Color: 6306
Size: 284967 Color: 3293

Bin 2563: 1 of cap free
Amount of items: 3
Items: 
Size: 368435 Color: 7097
Size: 346389 Color: 6305
Size: 285176 Color: 3311

Bin 2564: 1 of cap free
Amount of items: 3
Items: 
Size: 368471 Color: 7098
Size: 322377 Color: 5347
Size: 309152 Color: 4661

Bin 2565: 1 of cap free
Amount of items: 3
Items: 
Size: 409160 Color: 8245
Size: 312347 Color: 4838
Size: 278493 Color: 2811

Bin 2566: 1 of cap free
Amount of items: 3
Items: 
Size: 366212 Color: 7029
Size: 319716 Color: 5210
Size: 314072 Color: 4910

Bin 2567: 1 of cap free
Amount of items: 3
Items: 
Size: 366070 Color: 7023
Size: 329490 Color: 5640
Size: 304440 Color: 4418

Bin 2568: 1 of cap free
Amount of items: 3
Items: 
Size: 381916 Color: 7517
Size: 348772 Color: 6395
Size: 269312 Color: 2081

Bin 2569: 1 of cap free
Amount of items: 3
Items: 
Size: 393982 Color: 7863
Size: 317965 Color: 5111
Size: 288053 Color: 3483

Bin 2570: 1 of cap free
Amount of items: 3
Items: 
Size: 365784 Color: 7014
Size: 353508 Color: 6610
Size: 280708 Color: 2978

Bin 2571: 1 of cap free
Amount of items: 3
Items: 
Size: 365443 Color: 7003
Size: 365404 Color: 7002
Size: 269153 Color: 2070

Bin 2572: 1 of cap free
Amount of items: 3
Items: 
Size: 381195 Color: 7489
Size: 333226 Color: 5789
Size: 285579 Color: 3332

Bin 2573: 1 of cap free
Amount of items: 3
Items: 
Size: 363248 Color: 6929
Size: 340382 Color: 6076
Size: 296370 Color: 3968

Bin 2574: 1 of cap free
Amount of items: 3
Items: 
Size: 363101 Color: 6922
Size: 333201 Color: 5786
Size: 303698 Color: 4373

Bin 2575: 1 of cap free
Amount of items: 3
Items: 
Size: 375089 Color: 7290
Size: 321928 Color: 5327
Size: 302983 Color: 4331

Bin 2576: 1 of cap free
Amount of items: 3
Items: 
Size: 363060 Color: 6916
Size: 333289 Color: 5792
Size: 303651 Color: 4370

Bin 2577: 1 of cap free
Amount of items: 3
Items: 
Size: 363206 Color: 6925
Size: 332487 Color: 5758
Size: 304307 Color: 4410

Bin 2578: 1 of cap free
Amount of items: 3
Items: 
Size: 376536 Color: 7339
Size: 362816 Color: 6907
Size: 260648 Color: 1324

Bin 2579: 1 of cap free
Amount of items: 3
Items: 
Size: 362577 Color: 6897
Size: 345863 Color: 6281
Size: 291560 Color: 3687

Bin 2580: 1 of cap free
Amount of items: 3
Items: 
Size: 362206 Color: 6881
Size: 337746 Color: 5969
Size: 300048 Color: 4168

Bin 2581: 1 of cap free
Amount of items: 3
Items: 
Size: 361827 Color: 6869
Size: 344375 Color: 6230
Size: 293798 Color: 3809

Bin 2582: 1 of cap free
Amount of items: 3
Items: 
Size: 361355 Color: 6854
Size: 344145 Color: 6224
Size: 294500 Color: 3849

Bin 2583: 1 of cap free
Amount of items: 3
Items: 
Size: 361418 Color: 6856
Size: 360970 Color: 6845
Size: 277612 Color: 2744

Bin 2584: 1 of cap free
Amount of items: 3
Items: 
Size: 367928 Color: 7075
Size: 335999 Color: 5893
Size: 296073 Color: 3953

Bin 2585: 1 of cap free
Amount of items: 3
Items: 
Size: 378008 Color: 7378
Size: 359903 Color: 6807
Size: 262089 Color: 1452

Bin 2586: 1 of cap free
Amount of items: 3
Items: 
Size: 360519 Color: 6826
Size: 355697 Color: 6676
Size: 283784 Color: 3202

Bin 2587: 1 of cap free
Amount of items: 3
Items: 
Size: 365522 Color: 7005
Size: 361941 Color: 6873
Size: 272537 Color: 2341

Bin 2588: 1 of cap free
Amount of items: 3
Items: 
Size: 358291 Color: 6762
Size: 358132 Color: 6755
Size: 283577 Color: 3180

Bin 2589: 1 of cap free
Amount of items: 3
Items: 
Size: 358149 Color: 6757
Size: 358027 Color: 6750
Size: 283824 Color: 3203

Bin 2590: 1 of cap free
Amount of items: 3
Items: 
Size: 363967 Color: 6949
Size: 346582 Color: 6309
Size: 289451 Color: 3563

Bin 2591: 1 of cap free
Amount of items: 3
Items: 
Size: 357027 Color: 6718
Size: 356979 Color: 6716
Size: 285994 Color: 3357

Bin 2592: 1 of cap free
Amount of items: 3
Items: 
Size: 357244 Color: 6727
Size: 356475 Color: 6697
Size: 286281 Color: 3377

Bin 2593: 1 of cap free
Amount of items: 3
Items: 
Size: 342100 Color: 6140
Size: 330809 Color: 5690
Size: 327091 Color: 5544

Bin 2594: 1 of cap free
Amount of items: 3
Items: 
Size: 389590 Color: 7718
Size: 330447 Color: 5677
Size: 279963 Color: 2924

Bin 2595: 1 of cap free
Amount of items: 3
Items: 
Size: 358648 Color: 6774
Size: 350253 Color: 6465
Size: 291099 Color: 3654

Bin 2596: 1 of cap free
Amount of items: 3
Items: 
Size: 354481 Color: 6644
Size: 354031 Color: 6630
Size: 291488 Color: 3683

Bin 2597: 1 of cap free
Amount of items: 3
Items: 
Size: 354045 Color: 6632
Size: 353994 Color: 6626
Size: 291961 Color: 3718

Bin 2598: 1 of cap free
Amount of items: 3
Items: 
Size: 359613 Color: 6800
Size: 347891 Color: 6362
Size: 292496 Color: 3747

Bin 2599: 1 of cap free
Amount of items: 3
Items: 
Size: 370583 Color: 7162
Size: 349892 Color: 6447
Size: 279525 Color: 2882

Bin 2600: 1 of cap free
Amount of items: 3
Items: 
Size: 356861 Color: 6714
Size: 352427 Color: 6568
Size: 290712 Color: 3630

Bin 2601: 1 of cap free
Amount of items: 3
Items: 
Size: 374753 Color: 7284
Size: 352209 Color: 6556
Size: 273038 Color: 2390

Bin 2602: 1 of cap free
Amount of items: 3
Items: 
Size: 351847 Color: 6541
Size: 351769 Color: 6538
Size: 296384 Color: 3969

Bin 2603: 1 of cap free
Amount of items: 3
Items: 
Size: 351537 Color: 6528
Size: 351422 Color: 6519
Size: 297041 Color: 4008

Bin 2604: 1 of cap free
Amount of items: 3
Items: 
Size: 365181 Color: 6986
Size: 351414 Color: 6517
Size: 283405 Color: 3169

Bin 2605: 1 of cap free
Amount of items: 3
Items: 
Size: 350763 Color: 6486
Size: 350754 Color: 6484
Size: 298483 Color: 4085

Bin 2606: 1 of cap free
Amount of items: 3
Items: 
Size: 381085 Color: 7480
Size: 338182 Color: 5990
Size: 280733 Color: 2981

Bin 2607: 1 of cap free
Amount of items: 3
Items: 
Size: 358482 Color: 6769
Size: 347588 Color: 6349
Size: 293930 Color: 3815

Bin 2608: 1 of cap free
Amount of items: 3
Items: 
Size: 353275 Color: 6604
Size: 347313 Color: 6345
Size: 299412 Color: 4138

Bin 2609: 1 of cap free
Amount of items: 3
Items: 
Size: 358127 Color: 6754
Size: 353267 Color: 6602
Size: 288606 Color: 3509

Bin 2610: 1 of cap free
Amount of items: 3
Items: 
Size: 366614 Color: 7041
Size: 350232 Color: 6461
Size: 283154 Color: 3150

Bin 2611: 1 of cap free
Amount of items: 3
Items: 
Size: 349929 Color: 6449
Size: 349914 Color: 6448
Size: 300157 Color: 4174

Bin 2612: 1 of cap free
Amount of items: 3
Items: 
Size: 387670 Color: 7665
Size: 325327 Color: 5464
Size: 287003 Color: 3424

Bin 2613: 1 of cap free
Amount of items: 3
Items: 
Size: 349759 Color: 6439
Size: 349236 Color: 6421
Size: 301005 Color: 4219

Bin 2614: 1 of cap free
Amount of items: 3
Items: 
Size: 364796 Color: 6975
Size: 349020 Color: 6410
Size: 286184 Color: 3368

Bin 2615: 2 of cap free
Amount of items: 3
Items: 
Size: 380672 Color: 7468
Size: 343792 Color: 6209
Size: 275535 Color: 2576

Bin 2616: 2 of cap free
Amount of items: 3
Items: 
Size: 366882 Color: 7047
Size: 317975 Color: 5114
Size: 315142 Color: 4968

Bin 2617: 2 of cap free
Amount of items: 3
Items: 
Size: 367094 Color: 7056
Size: 319284 Color: 5183
Size: 313621 Color: 4890

Bin 2618: 2 of cap free
Amount of items: 3
Items: 
Size: 367140 Color: 7059
Size: 317485 Color: 5087
Size: 315374 Color: 4975

Bin 2619: 2 of cap free
Amount of items: 3
Items: 
Size: 373255 Color: 7240
Size: 346248 Color: 6297
Size: 280496 Color: 2972

Bin 2620: 2 of cap free
Amount of items: 3
Items: 
Size: 377354 Color: 7357
Size: 345126 Color: 6249
Size: 277519 Color: 2734

Bin 2621: 2 of cap free
Amount of items: 3
Items: 
Size: 380051 Color: 7447
Size: 344031 Color: 6218
Size: 275917 Color: 2607

Bin 2622: 2 of cap free
Amount of items: 3
Items: 
Size: 380767 Color: 7473
Size: 343702 Color: 6205
Size: 275530 Color: 2575

Bin 2623: 2 of cap free
Amount of items: 3
Items: 
Size: 382424 Color: 7526
Size: 343346 Color: 6191
Size: 274229 Color: 2486

Bin 2624: 2 of cap free
Amount of items: 3
Items: 
Size: 384087 Color: 7571
Size: 342567 Color: 6158
Size: 273345 Color: 2414

Bin 2625: 2 of cap free
Amount of items: 3
Items: 
Size: 411190 Color: 8301
Size: 317320 Color: 5080
Size: 271489 Color: 2248

Bin 2626: 2 of cap free
Amount of items: 3
Items: 
Size: 391946 Color: 7793
Size: 339177 Color: 6025
Size: 268876 Color: 2041

Bin 2627: 2 of cap free
Amount of items: 3
Items: 
Size: 393303 Color: 7841
Size: 338585 Color: 6009
Size: 268111 Color: 1986

Bin 2628: 2 of cap free
Amount of items: 3
Items: 
Size: 393576 Color: 7849
Size: 338448 Color: 6001
Size: 267975 Color: 1971

Bin 2629: 2 of cap free
Amount of items: 3
Items: 
Size: 394233 Color: 7869
Size: 338179 Color: 5989
Size: 267587 Color: 1943

Bin 2630: 2 of cap free
Amount of items: 3
Items: 
Size: 377515 Color: 7363
Size: 345135 Color: 6251
Size: 277349 Color: 2724

Bin 2631: 2 of cap free
Amount of items: 3
Items: 
Size: 394263 Color: 7871
Size: 339450 Color: 6035
Size: 266286 Color: 1830

Bin 2632: 2 of cap free
Amount of items: 3
Items: 
Size: 386151 Color: 7627
Size: 323635 Color: 5405
Size: 290213 Color: 3604

Bin 2633: 2 of cap free
Amount of items: 3
Items: 
Size: 390115 Color: 7731
Size: 339784 Color: 6051
Size: 270100 Color: 2133

Bin 2634: 2 of cap free
Amount of items: 3
Items: 
Size: 394071 Color: 7866
Size: 342773 Color: 6167
Size: 263155 Color: 1549

Bin 2635: 2 of cap free
Amount of items: 3
Items: 
Size: 391570 Color: 7778
Size: 336011 Color: 5894
Size: 272418 Color: 2331

Bin 2636: 2 of cap free
Amount of items: 3
Items: 
Size: 395792 Color: 7910
Size: 337555 Color: 5958
Size: 266652 Color: 1866

Bin 2637: 2 of cap free
Amount of items: 3
Items: 
Size: 383312 Color: 7545
Size: 342793 Color: 6168
Size: 273894 Color: 2456

Bin 2638: 2 of cap free
Amount of items: 3
Items: 
Size: 371089 Color: 7179
Size: 346676 Color: 6311
Size: 282234 Color: 3090

Bin 2639: 2 of cap free
Amount of items: 3
Items: 
Size: 353175 Color: 6597
Size: 340219 Color: 6072
Size: 306605 Color: 4535

Bin 2640: 2 of cap free
Amount of items: 3
Items: 
Size: 387979 Color: 7673
Size: 340640 Color: 6086
Size: 271380 Color: 2238

Bin 2641: 2 of cap free
Amount of items: 3
Items: 
Size: 391723 Color: 7783
Size: 339276 Color: 6028
Size: 269000 Color: 2054

Bin 2642: 2 of cap free
Amount of items: 3
Items: 
Size: 376974 Color: 7351
Size: 361127 Color: 6848
Size: 261898 Color: 1444

Bin 2643: 2 of cap free
Amount of items: 3
Items: 
Size: 385217 Color: 7592
Size: 342060 Color: 6137
Size: 272722 Color: 2357

Bin 2644: 2 of cap free
Amount of items: 3
Items: 
Size: 386932 Color: 7643
Size: 341257 Color: 6112
Size: 271810 Color: 2268

Bin 2645: 2 of cap free
Amount of items: 3
Items: 
Size: 385338 Color: 7598
Size: 354241 Color: 6637
Size: 260420 Color: 1302

Bin 2646: 2 of cap free
Amount of items: 3
Items: 
Size: 389521 Color: 7717
Size: 329251 Color: 5631
Size: 281227 Color: 3010

Bin 2647: 2 of cap free
Amount of items: 3
Items: 
Size: 393454 Color: 7846
Size: 331266 Color: 5705
Size: 275279 Color: 2562

Bin 2648: 2 of cap free
Amount of items: 3
Items: 
Size: 386792 Color: 7642
Size: 333676 Color: 5805
Size: 279531 Color: 2884

Bin 2649: 2 of cap free
Amount of items: 3
Items: 
Size: 386718 Color: 7640
Size: 341372 Color: 6113
Size: 271909 Color: 2284

Bin 2650: 2 of cap free
Amount of items: 3
Items: 
Size: 391221 Color: 7774
Size: 335280 Color: 5860
Size: 273498 Color: 2425

Bin 2651: 2 of cap free
Amount of items: 3
Items: 
Size: 388820 Color: 7703
Size: 340236 Color: 6073
Size: 270943 Color: 2200

Bin 2652: 2 of cap free
Amount of items: 3
Items: 
Size: 395862 Color: 7913
Size: 337741 Color: 5968
Size: 266396 Color: 1842

Bin 2653: 2 of cap free
Amount of items: 3
Items: 
Size: 368809 Color: 7110
Size: 359920 Color: 6808
Size: 271270 Color: 2229

Bin 2654: 2 of cap free
Amount of items: 3
Items: 
Size: 384229 Color: 7575
Size: 351778 Color: 6540
Size: 263992 Color: 1628

Bin 2655: 2 of cap free
Amount of items: 3
Items: 
Size: 391168 Color: 7770
Size: 339423 Color: 6034
Size: 269408 Color: 2086

Bin 2656: 2 of cap free
Amount of items: 3
Items: 
Size: 382059 Color: 7518
Size: 343015 Color: 6176
Size: 274925 Color: 2536

Bin 2657: 2 of cap free
Amount of items: 3
Items: 
Size: 384075 Color: 7570
Size: 336033 Color: 5897
Size: 279891 Color: 2918

Bin 2658: 2 of cap free
Amount of items: 3
Items: 
Size: 381913 Color: 7515
Size: 323945 Color: 5412
Size: 294141 Color: 3823

Bin 2659: 2 of cap free
Amount of items: 3
Items: 
Size: 384242 Color: 7576
Size: 342122 Color: 6141
Size: 273635 Color: 2434

Bin 2660: 2 of cap free
Amount of items: 3
Items: 
Size: 410649 Color: 8287
Size: 337605 Color: 5961
Size: 251745 Color: 279

Bin 2661: 2 of cap free
Amount of items: 3
Items: 
Size: 410264 Color: 8274
Size: 335810 Color: 5884
Size: 253925 Color: 562

Bin 2662: 2 of cap free
Amount of items: 3
Items: 
Size: 351527 Color: 6527
Size: 326761 Color: 5531
Size: 321711 Color: 5309

Bin 2663: 2 of cap free
Amount of items: 3
Items: 
Size: 410384 Color: 8280
Size: 335488 Color: 5873
Size: 254127 Color: 597

Bin 2664: 2 of cap free
Amount of items: 3
Items: 
Size: 410288 Color: 8275
Size: 336956 Color: 5932
Size: 252755 Color: 402

Bin 2665: 2 of cap free
Amount of items: 3
Items: 
Size: 341573 Color: 6118
Size: 330321 Color: 5669
Size: 328105 Color: 5589

Bin 2666: 2 of cap free
Amount of items: 3
Items: 
Size: 410033 Color: 8269
Size: 336733 Color: 5927
Size: 253233 Color: 473

Bin 2667: 2 of cap free
Amount of items: 3
Items: 
Size: 404283 Color: 8115
Size: 320486 Color: 5248
Size: 275230 Color: 2559

Bin 2668: 2 of cap free
Amount of items: 3
Items: 
Size: 409370 Color: 8249
Size: 336091 Color: 5902
Size: 254538 Color: 647

Bin 2669: 2 of cap free
Amount of items: 3
Items: 
Size: 408627 Color: 8235
Size: 335224 Color: 5857
Size: 256148 Color: 843

Bin 2670: 2 of cap free
Amount of items: 3
Items: 
Size: 389283 Color: 7715
Size: 349673 Color: 6432
Size: 261043 Color: 1366

Bin 2671: 2 of cap free
Amount of items: 3
Items: 
Size: 407941 Color: 8225
Size: 336075 Color: 5901
Size: 255983 Color: 823

Bin 2672: 2 of cap free
Amount of items: 3
Items: 
Size: 407930 Color: 8224
Size: 336201 Color: 5907
Size: 255868 Color: 812

Bin 2673: 2 of cap free
Amount of items: 3
Items: 
Size: 404153 Color: 8112
Size: 325525 Color: 5477
Size: 270321 Color: 2150

Bin 2674: 2 of cap free
Amount of items: 3
Items: 
Size: 406293 Color: 8169
Size: 334436 Color: 5838
Size: 259270 Color: 1179

Bin 2675: 2 of cap free
Amount of items: 3
Items: 
Size: 412382 Color: 8329
Size: 327452 Color: 5562
Size: 260165 Color: 1270

Bin 2676: 2 of cap free
Amount of items: 3
Items: 
Size: 405902 Color: 8161
Size: 335286 Color: 5861
Size: 258811 Color: 1129

Bin 2677: 2 of cap free
Amount of items: 3
Items: 
Size: 406147 Color: 8166
Size: 335579 Color: 5876
Size: 258273 Color: 1070

Bin 2678: 2 of cap free
Amount of items: 3
Items: 
Size: 401540 Color: 8047
Size: 340901 Color: 6096
Size: 257558 Color: 995

Bin 2679: 2 of cap free
Amount of items: 3
Items: 
Size: 405993 Color: 8164
Size: 333168 Color: 5785
Size: 260838 Color: 1342

Bin 2680: 2 of cap free
Amount of items: 3
Items: 
Size: 405927 Color: 8162
Size: 333092 Color: 5781
Size: 260980 Color: 1356

Bin 2681: 2 of cap free
Amount of items: 3
Items: 
Size: 389140 Color: 7713
Size: 334220 Color: 5830
Size: 276639 Color: 2672

Bin 2682: 2 of cap free
Amount of items: 3
Items: 
Size: 404724 Color: 8128
Size: 332264 Color: 5746
Size: 263011 Color: 1533

Bin 2683: 2 of cap free
Amount of items: 3
Items: 
Size: 403560 Color: 8094
Size: 340470 Color: 6079
Size: 255969 Color: 821

Bin 2684: 2 of cap free
Amount of items: 3
Items: 
Size: 406653 Color: 8184
Size: 336499 Color: 5921
Size: 256847 Color: 931

Bin 2685: 2 of cap free
Amount of items: 3
Items: 
Size: 401924 Color: 8055
Size: 341041 Color: 6104
Size: 257034 Color: 950

Bin 2686: 2 of cap free
Amount of items: 3
Items: 
Size: 404309 Color: 8117
Size: 332100 Color: 5736
Size: 263590 Color: 1594

Bin 2687: 2 of cap free
Amount of items: 3
Items: 
Size: 404329 Color: 8119
Size: 335135 Color: 5852
Size: 260535 Color: 1315

Bin 2688: 2 of cap free
Amount of items: 3
Items: 
Size: 409876 Color: 8263
Size: 331999 Color: 5731
Size: 258124 Color: 1049

Bin 2689: 2 of cap free
Amount of items: 3
Items: 
Size: 396054 Color: 7916
Size: 350098 Color: 6456
Size: 253847 Color: 550

Bin 2690: 2 of cap free
Amount of items: 3
Items: 
Size: 402516 Color: 8073
Size: 330731 Color: 5688
Size: 266752 Color: 1873

Bin 2691: 2 of cap free
Amount of items: 3
Items: 
Size: 392691 Color: 7822
Size: 346972 Color: 6329
Size: 260336 Color: 1293

Bin 2692: 2 of cap free
Amount of items: 3
Items: 
Size: 406639 Color: 8182
Size: 330123 Color: 5661
Size: 263237 Color: 1559

Bin 2693: 2 of cap free
Amount of items: 3
Items: 
Size: 401181 Color: 8038
Size: 334983 Color: 5846
Size: 263835 Color: 1612

Bin 2694: 2 of cap free
Amount of items: 3
Items: 
Size: 401189 Color: 8039
Size: 329802 Color: 5652
Size: 269008 Color: 2056

Bin 2695: 2 of cap free
Amount of items: 3
Items: 
Size: 388528 Color: 7693
Size: 357847 Color: 6744
Size: 253624 Color: 517

Bin 2696: 2 of cap free
Amount of items: 3
Items: 
Size: 408832 Color: 8240
Size: 319246 Color: 5181
Size: 271921 Color: 2286

Bin 2697: 2 of cap free
Amount of items: 3
Items: 
Size: 400752 Color: 8027
Size: 333204 Color: 5787
Size: 266043 Color: 1805

Bin 2698: 2 of cap free
Amount of items: 3
Items: 
Size: 402050 Color: 8060
Size: 333716 Color: 5809
Size: 264233 Color: 1654

Bin 2699: 2 of cap free
Amount of items: 3
Items: 
Size: 406742 Color: 8190
Size: 328463 Color: 5607
Size: 264794 Color: 1700

Bin 2700: 2 of cap free
Amount of items: 3
Items: 
Size: 395534 Color: 7905
Size: 352907 Color: 6587
Size: 251558 Color: 253

Bin 2701: 2 of cap free
Amount of items: 3
Items: 
Size: 398847 Color: 7979
Size: 348382 Color: 6381
Size: 252770 Color: 405

Bin 2702: 2 of cap free
Amount of items: 3
Items: 
Size: 398833 Color: 7978
Size: 327872 Color: 5578
Size: 273294 Color: 2410

Bin 2703: 2 of cap free
Amount of items: 3
Items: 
Size: 365027 Color: 6980
Size: 323086 Color: 5385
Size: 311886 Color: 4813

Bin 2704: 2 of cap free
Amount of items: 3
Items: 
Size: 365170 Color: 6985
Size: 323271 Color: 5395
Size: 311558 Color: 4798

Bin 2705: 2 of cap free
Amount of items: 3
Items: 
Size: 348952 Color: 6404
Size: 339849 Color: 6054
Size: 311198 Color: 4780

Bin 2706: 2 of cap free
Amount of items: 3
Items: 
Size: 364621 Color: 6967
Size: 325844 Color: 5491
Size: 309534 Color: 4696

Bin 2707: 2 of cap free
Amount of items: 3
Items: 
Size: 375312 Color: 7295
Size: 345668 Color: 6272
Size: 279019 Color: 2849

Bin 2708: 2 of cap free
Amount of items: 3
Items: 
Size: 375233 Color: 7292
Size: 327564 Color: 5566
Size: 297202 Color: 4018

Bin 2709: 2 of cap free
Amount of items: 3
Items: 
Size: 375477 Color: 7301
Size: 345996 Color: 6285
Size: 278526 Color: 2814

Bin 2710: 2 of cap free
Amount of items: 3
Items: 
Size: 383375 Color: 7548
Size: 365861 Color: 7015
Size: 250763 Color: 125

Bin 2711: 2 of cap free
Amount of items: 3
Items: 
Size: 371670 Color: 7195
Size: 348612 Color: 6391
Size: 279717 Color: 2906

Bin 2712: 2 of cap free
Amount of items: 3
Items: 
Size: 370306 Color: 7154
Size: 339343 Color: 6030
Size: 290350 Color: 3611

Bin 2713: 2 of cap free
Amount of items: 3
Items: 
Size: 370407 Color: 7156
Size: 346158 Color: 6289
Size: 283434 Color: 3171

Bin 2714: 2 of cap free
Amount of items: 3
Items: 
Size: 368786 Color: 7109
Size: 327999 Color: 5582
Size: 303214 Color: 4353

Bin 2715: 2 of cap free
Amount of items: 3
Items: 
Size: 368637 Color: 7104
Size: 365923 Color: 7017
Size: 265439 Color: 1759

Bin 2716: 2 of cap free
Amount of items: 3
Items: 
Size: 368314 Color: 7088
Size: 321882 Color: 5322
Size: 309803 Color: 4709

Bin 2717: 2 of cap free
Amount of items: 3
Items: 
Size: 367552 Color: 7068
Size: 367474 Color: 7066
Size: 264973 Color: 1719

Bin 2718: 2 of cap free
Amount of items: 3
Items: 
Size: 363886 Color: 6943
Size: 331081 Color: 5700
Size: 305032 Color: 4451

Bin 2719: 2 of cap free
Amount of items: 3
Items: 
Size: 366553 Color: 7039
Size: 366524 Color: 7038
Size: 266922 Color: 1881

Bin 2720: 2 of cap free
Amount of items: 3
Items: 
Size: 401970 Color: 8056
Size: 320064 Color: 5231
Size: 277965 Color: 2772

Bin 2721: 2 of cap free
Amount of items: 3
Items: 
Size: 375713 Color: 7311
Size: 372522 Color: 7228
Size: 251764 Color: 282

Bin 2722: 2 of cap free
Amount of items: 3
Items: 
Size: 362524 Color: 6891
Size: 345359 Color: 6258
Size: 292116 Color: 3728

Bin 2723: 2 of cap free
Amount of items: 3
Items: 
Size: 362529 Color: 6893
Size: 336095 Color: 5903
Size: 301375 Color: 4233

Bin 2724: 2 of cap free
Amount of items: 3
Items: 
Size: 376228 Color: 7326
Size: 339802 Color: 6052
Size: 283969 Color: 3220

Bin 2725: 2 of cap free
Amount of items: 3
Items: 
Size: 362184 Color: 6879
Size: 344813 Color: 6241
Size: 293002 Color: 3772

Bin 2726: 2 of cap free
Amount of items: 3
Items: 
Size: 364379 Color: 6959
Size: 358790 Color: 6779
Size: 276830 Color: 2682

Bin 2727: 2 of cap free
Amount of items: 3
Items: 
Size: 370684 Color: 7166
Size: 368291 Color: 7086
Size: 261024 Color: 1363

Bin 2728: 2 of cap free
Amount of items: 3
Items: 
Size: 389117 Color: 7712
Size: 345721 Color: 6273
Size: 265161 Color: 1734

Bin 2729: 2 of cap free
Amount of items: 3
Items: 
Size: 360923 Color: 6843
Size: 360635 Color: 6828
Size: 278441 Color: 2803

Bin 2730: 2 of cap free
Amount of items: 3
Items: 
Size: 360304 Color: 6820
Size: 360303 Color: 6819
Size: 279392 Color: 2877

Bin 2731: 2 of cap free
Amount of items: 3
Items: 
Size: 359001 Color: 6787
Size: 358978 Color: 6785
Size: 282020 Color: 3073

Bin 2732: 2 of cap free
Amount of items: 3
Items: 
Size: 363911 Color: 6946
Size: 352738 Color: 6582
Size: 283350 Color: 3166

Bin 2733: 2 of cap free
Amount of items: 3
Items: 
Size: 357922 Color: 6748
Size: 357892 Color: 6745
Size: 284185 Color: 3230

Bin 2734: 2 of cap free
Amount of items: 3
Items: 
Size: 357749 Color: 6741
Size: 357719 Color: 6740
Size: 284531 Color: 3262

Bin 2735: 2 of cap free
Amount of items: 3
Items: 
Size: 357058 Color: 6719
Size: 356798 Color: 6709
Size: 286143 Color: 3365

Bin 2736: 2 of cap free
Amount of items: 3
Items: 
Size: 356665 Color: 6703
Size: 356600 Color: 6702
Size: 286734 Color: 3411

Bin 2737: 2 of cap free
Amount of items: 3
Items: 
Size: 355549 Color: 6674
Size: 355535 Color: 6673
Size: 288915 Color: 3529

Bin 2738: 2 of cap free
Amount of items: 3
Items: 
Size: 355833 Color: 6682
Size: 355420 Color: 6665
Size: 288746 Color: 3518

Bin 2739: 2 of cap free
Amount of items: 3
Items: 
Size: 354357 Color: 6642
Size: 354264 Color: 6638
Size: 291378 Color: 3673

Bin 2740: 2 of cap free
Amount of items: 3
Items: 
Size: 353680 Color: 6618
Size: 353587 Color: 6614
Size: 292732 Color: 3758

Bin 2741: 2 of cap free
Amount of items: 3
Items: 
Size: 366613 Color: 7040
Size: 351995 Color: 6546
Size: 281391 Color: 3018

Bin 2742: 2 of cap free
Amount of items: 3
Items: 
Size: 357349 Color: 6731
Size: 348341 Color: 6378
Size: 294309 Color: 3831

Bin 2743: 2 of cap free
Amount of items: 3
Items: 
Size: 355857 Color: 6684
Size: 352811 Color: 6584
Size: 291331 Color: 3671

Bin 2744: 2 of cap free
Amount of items: 3
Items: 
Size: 363307 Color: 6931
Size: 352969 Color: 6590
Size: 283723 Color: 3198

Bin 2745: 2 of cap free
Amount of items: 3
Items: 
Size: 362726 Color: 6903
Size: 347263 Color: 6342
Size: 290010 Color: 3588

Bin 2746: 2 of cap free
Amount of items: 3
Items: 
Size: 364543 Color: 6961
Size: 347299 Color: 6344
Size: 288157 Color: 3488

Bin 2747: 2 of cap free
Amount of items: 3
Items: 
Size: 351756 Color: 6537
Size: 348351 Color: 6380
Size: 299892 Color: 4157

Bin 2748: 2 of cap free
Amount of items: 3
Items: 
Size: 349869 Color: 6446
Size: 349684 Color: 6435
Size: 300446 Color: 4187

Bin 2749: 2 of cap free
Amount of items: 3
Items: 
Size: 371891 Color: 7204
Size: 348568 Color: 6387
Size: 279540 Color: 2886

Bin 2750: 3 of cap free
Amount of items: 3
Items: 
Size: 366747 Color: 7044
Size: 317377 Color: 5083
Size: 315874 Color: 5003

Bin 2751: 3 of cap free
Amount of items: 3
Items: 
Size: 367255 Color: 7062
Size: 316801 Color: 5060
Size: 315942 Color: 5008

Bin 2752: 3 of cap free
Amount of items: 3
Items: 
Size: 367426 Color: 7065
Size: 320274 Color: 5243
Size: 312298 Color: 4835

Bin 2753: 3 of cap free
Amount of items: 3
Items: 
Size: 394642 Color: 7880
Size: 322922 Color: 5376
Size: 282434 Color: 3107

Bin 2754: 3 of cap free
Amount of items: 3
Items: 
Size: 377189 Color: 7353
Size: 345250 Color: 6253
Size: 277559 Color: 2737

Bin 2755: 3 of cap free
Amount of items: 3
Items: 
Size: 380439 Color: 7463
Size: 343924 Color: 6216
Size: 275635 Color: 2583

Bin 2756: 3 of cap free
Amount of items: 3
Items: 
Size: 380561 Color: 7465
Size: 343820 Color: 6211
Size: 275617 Color: 2581

Bin 2757: 3 of cap free
Amount of items: 3
Items: 
Size: 388340 Color: 7682
Size: 340500 Color: 6081
Size: 271158 Color: 2218

Bin 2758: 3 of cap free
Amount of items: 3
Items: 
Size: 390389 Color: 7740
Size: 339882 Color: 6057
Size: 269727 Color: 2107

Bin 2759: 3 of cap free
Amount of items: 3
Items: 
Size: 391482 Color: 7777
Size: 339458 Color: 6036
Size: 269058 Color: 2063

Bin 2760: 3 of cap free
Amount of items: 3
Items: 
Size: 394409 Color: 7874
Size: 338245 Color: 5992
Size: 267344 Color: 1918

Bin 2761: 3 of cap free
Amount of items: 3
Items: 
Size: 395198 Color: 7895
Size: 334060 Color: 5823
Size: 270740 Color: 2182

Bin 2762: 3 of cap free
Amount of items: 3
Items: 
Size: 373613 Color: 7246
Size: 372692 Color: 7230
Size: 253693 Color: 528

Bin 2763: 3 of cap free
Amount of items: 3
Items: 
Size: 384917 Color: 7589
Size: 333886 Color: 5814
Size: 281195 Color: 3007

Bin 2764: 3 of cap free
Amount of items: 3
Items: 
Size: 390570 Color: 7742
Size: 339737 Color: 6048
Size: 269691 Color: 2105

Bin 2765: 3 of cap free
Amount of items: 3
Items: 
Size: 372654 Color: 7229
Size: 346323 Color: 6300
Size: 281021 Color: 2995

Bin 2766: 3 of cap free
Amount of items: 3
Items: 
Size: 379423 Color: 7427
Size: 344280 Color: 6229
Size: 276295 Color: 2637

Bin 2767: 3 of cap free
Amount of items: 3
Items: 
Size: 378474 Color: 7395
Size: 344729 Color: 6239
Size: 276795 Color: 2681

Bin 2768: 3 of cap free
Amount of items: 3
Items: 
Size: 391023 Color: 7764
Size: 336326 Color: 5917
Size: 272649 Color: 2353

Bin 2769: 3 of cap free
Amount of items: 3
Items: 
Size: 376576 Color: 7341
Size: 353202 Color: 6601
Size: 270220 Color: 2140

Bin 2770: 3 of cap free
Amount of items: 3
Items: 
Size: 385760 Color: 7614
Size: 348245 Color: 6371
Size: 265993 Color: 1803

Bin 2771: 3 of cap free
Amount of items: 3
Items: 
Size: 412388 Color: 8330
Size: 337477 Color: 5954
Size: 250133 Color: 23

Bin 2772: 3 of cap free
Amount of items: 3
Items: 
Size: 362360 Color: 6886
Size: 348479 Color: 6385
Size: 289159 Color: 3540

Bin 2773: 3 of cap free
Amount of items: 3
Items: 
Size: 378146 Color: 7382
Size: 351717 Color: 6536
Size: 270135 Color: 2136

Bin 2774: 3 of cap free
Amount of items: 3
Items: 
Size: 389323 Color: 7716
Size: 340262 Color: 6075
Size: 270413 Color: 2157

Bin 2775: 3 of cap free
Amount of items: 3
Items: 
Size: 393089 Color: 7837
Size: 334281 Color: 5832
Size: 272628 Color: 2351

Bin 2776: 3 of cap free
Amount of items: 3
Items: 
Size: 381220 Color: 7490
Size: 343875 Color: 6214
Size: 274903 Color: 2532

Bin 2777: 3 of cap free
Amount of items: 3
Items: 
Size: 376928 Color: 7349
Size: 345310 Color: 6256
Size: 277760 Color: 2756

Bin 2778: 3 of cap free
Amount of items: 3
Items: 
Size: 390802 Color: 7755
Size: 339675 Color: 6046
Size: 269521 Color: 2093

Bin 2779: 3 of cap free
Amount of items: 3
Items: 
Size: 385926 Color: 7619
Size: 341926 Color: 6131
Size: 272146 Color: 2307

Bin 2780: 3 of cap free
Amount of items: 3
Items: 
Size: 387350 Color: 7654
Size: 333520 Color: 5798
Size: 279128 Color: 2862

Bin 2781: 3 of cap free
Amount of items: 3
Items: 
Size: 386131 Color: 7625
Size: 348574 Color: 6388
Size: 265293 Color: 1748

Bin 2782: 3 of cap free
Amount of items: 3
Items: 
Size: 373606 Color: 7245
Size: 354772 Color: 6648
Size: 271620 Color: 2253

Bin 2783: 3 of cap free
Amount of items: 3
Items: 
Size: 387635 Color: 7664
Size: 340785 Color: 6092
Size: 271578 Color: 2250

Bin 2784: 3 of cap free
Amount of items: 3
Items: 
Size: 388279 Color: 7680
Size: 347094 Color: 6334
Size: 264625 Color: 1687

Bin 2785: 3 of cap free
Amount of items: 3
Items: 
Size: 394051 Color: 7865
Size: 338136 Color: 5986
Size: 267811 Color: 1965

Bin 2786: 3 of cap free
Amount of items: 3
Items: 
Size: 408317 Color: 8230
Size: 340818 Color: 6095
Size: 250863 Color: 149

Bin 2787: 3 of cap free
Amount of items: 3
Items: 
Size: 411220 Color: 8302
Size: 337259 Color: 5943
Size: 251519 Color: 250

Bin 2788: 3 of cap free
Amount of items: 3
Items: 
Size: 409971 Color: 8267
Size: 335843 Color: 5887
Size: 254184 Color: 605

Bin 2789: 3 of cap free
Amount of items: 3
Items: 
Size: 410658 Color: 8288
Size: 327094 Color: 5545
Size: 262246 Color: 1464

Bin 2790: 3 of cap free
Amount of items: 3
Items: 
Size: 408143 Color: 8227
Size: 336367 Color: 5919
Size: 255488 Color: 765

Bin 2791: 3 of cap free
Amount of items: 3
Items: 
Size: 365314 Color: 6997
Size: 318282 Color: 5128
Size: 316402 Color: 5040

Bin 2792: 3 of cap free
Amount of items: 3
Items: 
Size: 407671 Color: 8218
Size: 336055 Color: 5900
Size: 256272 Color: 858

Bin 2793: 3 of cap free
Amount of items: 3
Items: 
Size: 408759 Color: 8237
Size: 335174 Color: 5855
Size: 256065 Color: 835

Bin 2794: 3 of cap free
Amount of items: 3
Items: 
Size: 409354 Color: 8247
Size: 329658 Color: 5647
Size: 260986 Color: 1359

Bin 2795: 3 of cap free
Amount of items: 3
Items: 
Size: 348615 Color: 6392
Size: 332457 Color: 5755
Size: 318926 Color: 5168

Bin 2796: 3 of cap free
Amount of items: 3
Items: 
Size: 407121 Color: 8204
Size: 335681 Color: 5881
Size: 257196 Color: 967

Bin 2797: 3 of cap free
Amount of items: 3
Items: 
Size: 401145 Color: 8037
Size: 333474 Color: 5794
Size: 265379 Color: 1754

Bin 2798: 3 of cap free
Amount of items: 3
Items: 
Size: 349954 Color: 6450
Size: 333935 Color: 5816
Size: 316109 Color: 5016

Bin 2799: 3 of cap free
Amount of items: 3
Items: 
Size: 409462 Color: 8252
Size: 333847 Color: 5812
Size: 256689 Color: 905

Bin 2800: 3 of cap free
Amount of items: 3
Items: 
Size: 404379 Color: 8120
Size: 335125 Color: 5851
Size: 260494 Color: 1310

Bin 2801: 3 of cap free
Amount of items: 3
Items: 
Size: 403959 Color: 8110
Size: 331921 Color: 5727
Size: 264118 Color: 1643

Bin 2802: 3 of cap free
Amount of items: 3
Items: 
Size: 403787 Color: 8101
Size: 331629 Color: 5719
Size: 264582 Color: 1684

Bin 2803: 3 of cap free
Amount of items: 3
Items: 
Size: 402610 Color: 8076
Size: 335773 Color: 5883
Size: 261615 Color: 1417

Bin 2804: 3 of cap free
Amount of items: 3
Items: 
Size: 409091 Color: 8244
Size: 330604 Color: 5684
Size: 260303 Color: 1290

Bin 2805: 3 of cap free
Amount of items: 3
Items: 
Size: 374046 Color: 7260
Size: 362812 Color: 6906
Size: 263140 Color: 1545

Bin 2806: 3 of cap free
Amount of items: 3
Items: 
Size: 400288 Color: 8017
Size: 335386 Color: 5867
Size: 264324 Color: 1663

Bin 2807: 3 of cap free
Amount of items: 3
Items: 
Size: 400078 Color: 8009
Size: 328883 Color: 5617
Size: 271037 Color: 2207

Bin 2808: 3 of cap free
Amount of items: 3
Items: 
Size: 400258 Color: 8015
Size: 328932 Color: 5620
Size: 270808 Color: 2186

Bin 2809: 3 of cap free
Amount of items: 3
Items: 
Size: 406672 Color: 8187
Size: 328938 Color: 5621
Size: 264388 Color: 1671

Bin 2810: 3 of cap free
Amount of items: 3
Items: 
Size: 399021 Color: 7985
Size: 327430 Color: 5559
Size: 273547 Color: 2428

Bin 2811: 3 of cap free
Amount of items: 3
Items: 
Size: 365134 Color: 6984
Size: 323045 Color: 5383
Size: 311819 Color: 4811

Bin 2812: 3 of cap free
Amount of items: 3
Items: 
Size: 394994 Color: 7890
Size: 323874 Color: 5410
Size: 281130 Color: 3004

Bin 2813: 3 of cap free
Amount of items: 3
Items: 
Size: 364651 Color: 6968
Size: 327198 Color: 5551
Size: 308149 Color: 4606

Bin 2814: 3 of cap free
Amount of items: 3
Items: 
Size: 372244 Color: 7219
Size: 346212 Color: 6293
Size: 281542 Color: 3028

Bin 2815: 3 of cap free
Amount of items: 3
Items: 
Size: 371820 Color: 7198
Size: 343064 Color: 6178
Size: 285114 Color: 3305

Bin 2816: 3 of cap free
Amount of items: 3
Items: 
Size: 371847 Color: 7202
Size: 342997 Color: 6175
Size: 285154 Color: 3308

Bin 2817: 3 of cap free
Amount of items: 3
Items: 
Size: 372127 Color: 7215
Size: 344121 Color: 6223
Size: 283750 Color: 3201

Bin 2818: 3 of cap free
Amount of items: 3
Items: 
Size: 364110 Color: 6952
Size: 329394 Color: 5637
Size: 306494 Color: 4527

Bin 2819: 3 of cap free
Amount of items: 3
Items: 
Size: 367982 Color: 7076
Size: 323949 Color: 5413
Size: 308067 Color: 4602

Bin 2820: 3 of cap free
Amount of items: 3
Items: 
Size: 369184 Color: 7124
Size: 327344 Color: 5557
Size: 303470 Color: 4361

Bin 2821: 3 of cap free
Amount of items: 3
Items: 
Size: 367853 Color: 7073
Size: 321110 Color: 5281
Size: 311035 Color: 4775

Bin 2822: 3 of cap free
Amount of items: 3
Items: 
Size: 357550 Color: 6735
Size: 322728 Color: 5361
Size: 319720 Color: 5211

Bin 2823: 3 of cap free
Amount of items: 3
Items: 
Size: 368359 Color: 7094
Size: 319908 Color: 5223
Size: 311731 Color: 4808

Bin 2824: 3 of cap free
Amount of items: 3
Items: 
Size: 365515 Color: 7004
Size: 318272 Color: 5127
Size: 316211 Color: 5023

Bin 2825: 3 of cap free
Amount of items: 3
Items: 
Size: 363482 Color: 6932
Size: 332082 Color: 5735
Size: 304434 Color: 4416

Bin 2826: 3 of cap free
Amount of items: 3
Items: 
Size: 362560 Color: 6895
Size: 362438 Color: 6888
Size: 275000 Color: 2541

Bin 2827: 3 of cap free
Amount of items: 3
Items: 
Size: 361578 Color: 6864
Size: 344477 Color: 6233
Size: 293943 Color: 3816

Bin 2828: 3 of cap free
Amount of items: 3
Items: 
Size: 360211 Color: 6814
Size: 346825 Color: 6319
Size: 292962 Color: 3771

Bin 2829: 3 of cap free
Amount of items: 3
Items: 
Size: 359054 Color: 6788
Size: 358982 Color: 6786
Size: 281962 Color: 3070

Bin 2830: 3 of cap free
Amount of items: 3
Items: 
Size: 388178 Color: 7677
Size: 329630 Color: 5646
Size: 282190 Color: 3081

Bin 2831: 3 of cap free
Amount of items: 3
Items: 
Size: 408805 Color: 8239
Size: 302309 Color: 4290
Size: 288884 Color: 3527

Bin 2832: 3 of cap free
Amount of items: 3
Items: 
Size: 369395 Color: 7129
Size: 347248 Color: 6341
Size: 283355 Color: 3167

Bin 2833: 3 of cap free
Amount of items: 3
Items: 
Size: 357531 Color: 6734
Size: 357248 Color: 6728
Size: 285219 Color: 3316

Bin 2834: 3 of cap free
Amount of items: 3
Items: 
Size: 357153 Color: 6723
Size: 357121 Color: 6722
Size: 285724 Color: 3343

Bin 2835: 3 of cap free
Amount of items: 3
Items: 
Size: 366191 Color: 7028
Size: 347907 Color: 6364
Size: 285900 Color: 3353

Bin 2836: 3 of cap free
Amount of items: 3
Items: 
Size: 356252 Color: 6692
Size: 356122 Color: 6690
Size: 287624 Color: 3461

Bin 2837: 3 of cap free
Amount of items: 3
Items: 
Size: 363969 Color: 6950
Size: 347866 Color: 6361
Size: 288163 Color: 3489

Bin 2838: 3 of cap free
Amount of items: 3
Items: 
Size: 357223 Color: 6726
Size: 351372 Color: 6515
Size: 291403 Color: 3678

Bin 2839: 3 of cap free
Amount of items: 3
Items: 
Size: 353887 Color: 6623
Size: 348258 Color: 6375
Size: 297853 Color: 4047

Bin 2840: 3 of cap free
Amount of items: 3
Items: 
Size: 354241 Color: 6636
Size: 351969 Color: 6544
Size: 293788 Color: 3808

Bin 2841: 3 of cap free
Amount of items: 3
Items: 
Size: 352126 Color: 6551
Size: 350400 Color: 6469
Size: 297472 Color: 4029

Bin 2842: 3 of cap free
Amount of items: 3
Items: 
Size: 351295 Color: 6513
Size: 351141 Color: 6504
Size: 297562 Color: 4033

Bin 2843: 3 of cap free
Amount of items: 3
Items: 
Size: 352989 Color: 6591
Size: 348249 Color: 6372
Size: 298760 Color: 4101

Bin 2844: 3 of cap free
Amount of items: 3
Items: 
Size: 353933 Color: 6625
Size: 352045 Color: 6549
Size: 294020 Color: 3821

Bin 2845: 3 of cap free
Amount of items: 3
Items: 
Size: 353175 Color: 6598
Size: 353168 Color: 6596
Size: 293655 Color: 3803

Bin 2846: 3 of cap free
Amount of items: 3
Items: 
Size: 358928 Color: 6783
Size: 347754 Color: 6358
Size: 293316 Color: 3783

Bin 2847: 3 of cap free
Amount of items: 3
Items: 
Size: 347599 Color: 6351
Size: 347453 Color: 6346
Size: 304946 Color: 4446

Bin 2848: 3 of cap free
Amount of items: 3
Items: 
Size: 363092 Color: 6920
Size: 349852 Color: 6445
Size: 287054 Color: 3428

Bin 2849: 3 of cap free
Amount of items: 3
Items: 
Size: 349778 Color: 6440
Size: 349692 Color: 6436
Size: 300528 Color: 4193

Bin 2850: 3 of cap free
Amount of items: 3
Items: 
Size: 360338 Color: 6821
Size: 352550 Color: 6571
Size: 287110 Color: 3434

Bin 2851: 3 of cap free
Amount of items: 3
Items: 
Size: 365571 Color: 7008
Size: 348664 Color: 6393
Size: 285763 Color: 3345

Bin 2852: 3 of cap free
Amount of items: 3
Items: 
Size: 350217 Color: 6460
Size: 348417 Color: 6383
Size: 301364 Color: 4232

Bin 2853: 4 of cap free
Amount of items: 3
Items: 
Size: 405436 Color: 8145
Size: 327972 Color: 5581
Size: 266589 Color: 1862

Bin 2854: 4 of cap free
Amount of items: 3
Items: 
Size: 376919 Color: 7348
Size: 345257 Color: 6254
Size: 277821 Color: 2759

Bin 2855: 4 of cap free
Amount of items: 3
Items: 
Size: 378619 Color: 7399
Size: 344710 Color: 6238
Size: 276668 Color: 2673

Bin 2856: 4 of cap free
Amount of items: 3
Items: 
Size: 378859 Color: 7406
Size: 344537 Color: 6234
Size: 276601 Color: 2666

Bin 2857: 4 of cap free
Amount of items: 3
Items: 
Size: 353408 Color: 6607
Size: 338548 Color: 6008
Size: 308041 Color: 4600

Bin 2858: 4 of cap free
Amount of items: 3
Items: 
Size: 374447 Color: 7272
Size: 346931 Color: 6325
Size: 278619 Color: 2819

Bin 2859: 4 of cap free
Amount of items: 3
Items: 
Size: 380201 Color: 7455
Size: 311989 Color: 4823
Size: 307807 Color: 4587

Bin 2860: 4 of cap free
Amount of items: 3
Items: 
Size: 381105 Color: 7482
Size: 343604 Color: 6201
Size: 275288 Color: 2563

Bin 2861: 4 of cap free
Amount of items: 3
Items: 
Size: 383052 Color: 7535
Size: 319626 Color: 5204
Size: 297319 Color: 4023

Bin 2862: 4 of cap free
Amount of items: 3
Items: 
Size: 385874 Color: 7616
Size: 341737 Color: 6126
Size: 272386 Color: 2327

Bin 2863: 4 of cap free
Amount of items: 3
Items: 
Size: 390802 Color: 7756
Size: 345286 Color: 6255
Size: 263909 Color: 1620

Bin 2864: 4 of cap free
Amount of items: 3
Items: 
Size: 394715 Color: 7881
Size: 338969 Color: 6019
Size: 266313 Color: 1833

Bin 2865: 4 of cap free
Amount of items: 3
Items: 
Size: 395130 Color: 7893
Size: 326467 Color: 5517
Size: 278400 Color: 2799

Bin 2866: 4 of cap free
Amount of items: 3
Items: 
Size: 398772 Color: 7977
Size: 326852 Color: 5536
Size: 274373 Color: 2500

Bin 2867: 4 of cap free
Amount of items: 3
Items: 
Size: 391387 Color: 7775
Size: 339518 Color: 6038
Size: 269092 Color: 2065

Bin 2868: 4 of cap free
Amount of items: 3
Items: 
Size: 412073 Color: 8322
Size: 327750 Color: 5574
Size: 260174 Color: 1272

Bin 2869: 4 of cap free
Amount of items: 3
Items: 
Size: 382738 Color: 7532
Size: 343069 Color: 6179
Size: 274190 Color: 2481

Bin 2870: 4 of cap free
Amount of items: 3
Items: 
Size: 387032 Color: 7645
Size: 349514 Color: 6428
Size: 263451 Color: 1579

Bin 2871: 4 of cap free
Amount of items: 3
Items: 
Size: 399432 Color: 7995
Size: 327220 Color: 5553
Size: 273345 Color: 2415

Bin 2872: 4 of cap free
Amount of items: 3
Items: 
Size: 373731 Color: 7250
Size: 373507 Color: 7242
Size: 252759 Color: 403

Bin 2873: 4 of cap free
Amount of items: 3
Items: 
Size: 399752 Color: 8003
Size: 345442 Color: 6262
Size: 254803 Color: 684

Bin 2874: 4 of cap free
Amount of items: 3
Items: 
Size: 409986 Color: 8268
Size: 337681 Color: 5966
Size: 252330 Color: 348

Bin 2875: 4 of cap free
Amount of items: 3
Items: 
Size: 407916 Color: 8222
Size: 336214 Color: 5909
Size: 255867 Color: 811

Bin 2876: 4 of cap free
Amount of items: 3
Items: 
Size: 405530 Color: 8148
Size: 318825 Color: 5163
Size: 275642 Color: 2584

Bin 2877: 4 of cap free
Amount of items: 3
Items: 
Size: 358723 Color: 6776
Size: 349987 Color: 6451
Size: 291287 Color: 3667

Bin 2878: 4 of cap free
Amount of items: 3
Items: 
Size: 407131 Color: 8205
Size: 334047 Color: 5822
Size: 258819 Color: 1131

Bin 2879: 4 of cap free
Amount of items: 3
Items: 
Size: 375320 Color: 7296
Size: 368139 Color: 7082
Size: 256538 Color: 889

Bin 2880: 4 of cap free
Amount of items: 3
Items: 
Size: 408683 Color: 8236
Size: 334531 Color: 5839
Size: 256783 Color: 918

Bin 2881: 4 of cap free
Amount of items: 3
Items: 
Size: 403168 Color: 8088
Size: 331524 Color: 5714
Size: 265305 Color: 1749

Bin 2882: 4 of cap free
Amount of items: 3
Items: 
Size: 402259 Color: 8066
Size: 329600 Color: 5643
Size: 268138 Color: 1987

Bin 2883: 4 of cap free
Amount of items: 3
Items: 
Size: 402095 Color: 8062
Size: 330483 Color: 5680
Size: 267419 Color: 1929

Bin 2884: 4 of cap free
Amount of items: 3
Items: 
Size: 401386 Color: 8041
Size: 329871 Color: 5654
Size: 268740 Color: 2027

Bin 2885: 4 of cap free
Amount of items: 3
Items: 
Size: 394793 Color: 7886
Size: 320192 Color: 5238
Size: 285012 Color: 3296

Bin 2886: 4 of cap free
Amount of items: 3
Items: 
Size: 400393 Color: 8022
Size: 329166 Color: 5627
Size: 270438 Color: 2158

Bin 2887: 4 of cap free
Amount of items: 3
Items: 
Size: 405623 Color: 8153
Size: 328877 Color: 5616
Size: 265497 Color: 1764

Bin 2888: 4 of cap free
Amount of items: 3
Items: 
Size: 406689 Color: 8188
Size: 328500 Color: 5608
Size: 264808 Color: 1702

Bin 2889: 4 of cap free
Amount of items: 3
Items: 
Size: 399216 Color: 7991
Size: 328236 Color: 5598
Size: 272545 Color: 2344

Bin 2890: 4 of cap free
Amount of items: 3
Items: 
Size: 398670 Color: 7975
Size: 327826 Color: 5577
Size: 273501 Color: 2426

Bin 2891: 4 of cap free
Amount of items: 3
Items: 
Size: 364826 Color: 6977
Size: 325159 Color: 5457
Size: 310012 Color: 4720

Bin 2892: 4 of cap free
Amount of items: 3
Items: 
Size: 365340 Color: 6999
Size: 325246 Color: 5459
Size: 309411 Color: 4684

Bin 2893: 4 of cap free
Amount of items: 3
Items: 
Size: 374046 Color: 7261
Size: 346354 Color: 6301
Size: 279597 Color: 2892

Bin 2894: 4 of cap free
Amount of items: 3
Items: 
Size: 374557 Color: 7276
Size: 323112 Color: 5388
Size: 302328 Color: 4291

Bin 2895: 4 of cap free
Amount of items: 3
Items: 
Size: 370341 Color: 7155
Size: 325660 Color: 5484
Size: 303996 Color: 4392

Bin 2896: 4 of cap free
Amount of items: 3
Items: 
Size: 370425 Color: 7157
Size: 324475 Color: 5430
Size: 305097 Color: 4456

Bin 2897: 4 of cap free
Amount of items: 3
Items: 
Size: 369293 Color: 7126
Size: 323956 Color: 5415
Size: 306748 Color: 4543

Bin 2898: 4 of cap free
Amount of items: 3
Items: 
Size: 368820 Color: 7111
Size: 324741 Color: 5437
Size: 306436 Color: 4523

Bin 2899: 4 of cap free
Amount of items: 3
Items: 
Size: 349167 Color: 6417
Size: 341947 Color: 6134
Size: 308883 Color: 4643

Bin 2900: 4 of cap free
Amount of items: 3
Items: 
Size: 368605 Color: 7103
Size: 365230 Color: 6993
Size: 266162 Color: 1816

Bin 2901: 4 of cap free
Amount of items: 3
Items: 
Size: 362148 Color: 6877
Size: 359266 Color: 6790
Size: 278583 Color: 2816

Bin 2902: 4 of cap free
Amount of items: 3
Items: 
Size: 369141 Color: 7121
Size: 321576 Color: 5301
Size: 309280 Color: 4676

Bin 2903: 4 of cap free
Amount of items: 3
Items: 
Size: 368285 Color: 7085
Size: 321830 Color: 5317
Size: 309882 Color: 4715

Bin 2904: 4 of cap free
Amount of items: 3
Items: 
Size: 381139 Color: 7484
Size: 367839 Color: 7072
Size: 251019 Color: 178

Bin 2905: 4 of cap free
Amount of items: 3
Items: 
Size: 358505 Color: 6771
Size: 331279 Color: 5707
Size: 310213 Color: 4730

Bin 2906: 4 of cap free
Amount of items: 3
Items: 
Size: 366041 Color: 7022
Size: 319472 Color: 5194
Size: 314484 Color: 4936

Bin 2907: 4 of cap free
Amount of items: 3
Items: 
Size: 360654 Color: 6830
Size: 322963 Color: 5377
Size: 316380 Color: 5039

Bin 2908: 4 of cap free
Amount of items: 3
Items: 
Size: 363052 Color: 6915
Size: 346222 Color: 6294
Size: 290723 Color: 3631

Bin 2909: 4 of cap free
Amount of items: 3
Items: 
Size: 363083 Color: 6917
Size: 360887 Color: 6842
Size: 276027 Color: 2617

Bin 2910: 4 of cap free
Amount of items: 3
Items: 
Size: 350248 Color: 6463
Size: 350112 Color: 6457
Size: 299637 Color: 4145

Bin 2911: 4 of cap free
Amount of items: 3
Items: 
Size: 367811 Color: 7070
Size: 331045 Color: 5699
Size: 301141 Color: 4225

Bin 2912: 4 of cap free
Amount of items: 3
Items: 
Size: 374656 Color: 7283
Size: 349180 Color: 6418
Size: 276161 Color: 2628

Bin 2913: 4 of cap free
Amount of items: 3
Items: 
Size: 360767 Color: 6836
Size: 343355 Color: 6192
Size: 295875 Color: 3939

Bin 2914: 4 of cap free
Amount of items: 3
Items: 
Size: 359528 Color: 6795
Size: 359482 Color: 6794
Size: 280987 Color: 2991

Bin 2915: 4 of cap free
Amount of items: 3
Items: 
Size: 358420 Color: 6767
Size: 358338 Color: 6763
Size: 283239 Color: 3156

Bin 2916: 4 of cap free
Amount of items: 3
Items: 
Size: 357907 Color: 6746
Size: 356374 Color: 6694
Size: 285716 Color: 3342

Bin 2917: 4 of cap free
Amount of items: 3
Items: 
Size: 357353 Color: 6732
Size: 357306 Color: 6730
Size: 285338 Color: 3321

Bin 2918: 4 of cap free
Amount of items: 3
Items: 
Size: 356851 Color: 6713
Size: 356834 Color: 6712
Size: 286312 Color: 3379

Bin 2919: 4 of cap free
Amount of items: 3
Items: 
Size: 356047 Color: 6688
Size: 356036 Color: 6687
Size: 287914 Color: 3477

Bin 2920: 4 of cap free
Amount of items: 3
Items: 
Size: 387790 Color: 7668
Size: 326909 Color: 5537
Size: 285298 Color: 3319

Bin 2921: 4 of cap free
Amount of items: 3
Items: 
Size: 355310 Color: 6661
Size: 354314 Color: 6641
Size: 290373 Color: 3614

Bin 2922: 4 of cap free
Amount of items: 3
Items: 
Size: 355107 Color: 6655
Size: 354037 Color: 6631
Size: 290853 Color: 3639

Bin 2923: 4 of cap free
Amount of items: 3
Items: 
Size: 352718 Color: 6576
Size: 333875 Color: 5813
Size: 313404 Color: 4883

Bin 2924: 4 of cap free
Amount of items: 3
Items: 
Size: 352573 Color: 6573
Size: 348777 Color: 6396
Size: 298647 Color: 4094

Bin 2925: 4 of cap free
Amount of items: 3
Items: 
Size: 349338 Color: 6425
Size: 349242 Color: 6422
Size: 301417 Color: 4237

Bin 2926: 4 of cap free
Amount of items: 3
Items: 
Size: 348822 Color: 6400
Size: 348249 Color: 6373
Size: 302926 Color: 4323

Bin 2927: 4 of cap free
Amount of items: 3
Items: 
Size: 363534 Color: 6935
Size: 333637 Color: 5802
Size: 302826 Color: 4316

Bin 2928: 5 of cap free
Amount of items: 3
Items: 
Size: 366254 Color: 7031
Size: 317421 Color: 5085
Size: 316321 Color: 5033

Bin 2929: 5 of cap free
Amount of items: 3
Items: 
Size: 382438 Color: 7527
Size: 343346 Color: 6190
Size: 274212 Color: 2485

Bin 2930: 5 of cap free
Amount of items: 3
Items: 
Size: 386376 Color: 7630
Size: 341429 Color: 6117
Size: 272191 Color: 2310

Bin 2931: 5 of cap free
Amount of items: 3
Items: 
Size: 391780 Color: 7786
Size: 339299 Color: 6029
Size: 268917 Color: 2047

Bin 2932: 5 of cap free
Amount of items: 3
Items: 
Size: 393640 Color: 7852
Size: 338437 Color: 6000
Size: 267919 Color: 1967

Bin 2933: 5 of cap free
Amount of items: 3
Items: 
Size: 393809 Color: 7860
Size: 338391 Color: 5998
Size: 267796 Color: 1964

Bin 2934: 5 of cap free
Amount of items: 3
Items: 
Size: 374291 Color: 7269
Size: 345984 Color: 6284
Size: 279721 Color: 2907

Bin 2935: 5 of cap free
Amount of items: 3
Items: 
Size: 412003 Color: 8320
Size: 337226 Color: 5942
Size: 250767 Color: 127

Bin 2936: 5 of cap free
Amount of items: 3
Items: 
Size: 389710 Color: 7720
Size: 339959 Color: 6061
Size: 270327 Color: 2151

Bin 2937: 5 of cap free
Amount of items: 3
Items: 
Size: 373746 Color: 7251
Size: 346233 Color: 6295
Size: 280017 Color: 2931

Bin 2938: 5 of cap free
Amount of items: 3
Items: 
Size: 369282 Color: 7125
Size: 352589 Color: 6574
Size: 278125 Color: 2783

Bin 2939: 5 of cap free
Amount of items: 3
Items: 
Size: 359358 Color: 6792
Size: 338448 Color: 6002
Size: 302190 Color: 4284

Bin 2940: 5 of cap free
Amount of items: 3
Items: 
Size: 380545 Color: 7464
Size: 339575 Color: 6040
Size: 279876 Color: 2916

Bin 2941: 5 of cap free
Amount of items: 3
Items: 
Size: 384421 Color: 7579
Size: 362996 Color: 6914
Size: 252579 Color: 379

Bin 2942: 5 of cap free
Amount of items: 3
Items: 
Size: 391611 Color: 7780
Size: 339583 Color: 6041
Size: 268802 Color: 2033

Bin 2943: 5 of cap free
Amount of items: 3
Items: 
Size: 383737 Color: 7564
Size: 335207 Color: 5856
Size: 281052 Color: 2997

Bin 2944: 5 of cap free
Amount of items: 3
Items: 
Size: 388856 Color: 7705
Size: 340210 Color: 6071
Size: 270930 Color: 2199

Bin 2945: 5 of cap free
Amount of items: 3
Items: 
Size: 395042 Color: 7891
Size: 337893 Color: 5978
Size: 267061 Color: 1892

Bin 2946: 5 of cap free
Amount of items: 3
Items: 
Size: 375058 Color: 7289
Size: 343106 Color: 6183
Size: 281832 Color: 3057

Bin 2947: 5 of cap free
Amount of items: 3
Items: 
Size: 388776 Color: 7701
Size: 340135 Color: 6068
Size: 271085 Color: 2212

Bin 2948: 5 of cap free
Amount of items: 3
Items: 
Size: 398929 Color: 7982
Size: 338272 Color: 5996
Size: 262795 Color: 1506

Bin 2949: 5 of cap free
Amount of items: 3
Items: 
Size: 409876 Color: 8264
Size: 338252 Color: 5994
Size: 251868 Color: 290

Bin 2950: 5 of cap free
Amount of items: 3
Items: 
Size: 410901 Color: 8292
Size: 336990 Color: 5936
Size: 252105 Color: 322

Bin 2951: 5 of cap free
Amount of items: 3
Items: 
Size: 411549 Color: 8306
Size: 327888 Color: 5579
Size: 260559 Color: 1316

Bin 2952: 5 of cap free
Amount of items: 3
Items: 
Size: 407853 Color: 8221
Size: 336430 Color: 5920
Size: 255713 Color: 798

Bin 2953: 5 of cap free
Amount of items: 3
Items: 
Size: 410349 Color: 8277
Size: 335036 Color: 5848
Size: 254611 Color: 660

Bin 2954: 5 of cap free
Amount of items: 3
Items: 
Size: 407455 Color: 8213
Size: 332190 Color: 5741
Size: 260351 Color: 1295

Bin 2955: 5 of cap free
Amount of items: 3
Items: 
Size: 406760 Color: 8192
Size: 336764 Color: 5929
Size: 256472 Color: 882

Bin 2956: 5 of cap free
Amount of items: 3
Items: 
Size: 406967 Color: 8199
Size: 336646 Color: 5924
Size: 256383 Color: 871

Bin 2957: 5 of cap free
Amount of items: 3
Items: 
Size: 401209 Color: 8040
Size: 342181 Color: 6143
Size: 256606 Color: 898

Bin 2958: 5 of cap free
Amount of items: 3
Items: 
Size: 395611 Color: 7907
Size: 354209 Color: 6635
Size: 250176 Color: 35

Bin 2959: 5 of cap free
Amount of items: 3
Items: 
Size: 404463 Color: 8123
Size: 332222 Color: 5745
Size: 263311 Color: 1568

Bin 2960: 5 of cap free
Amount of items: 3
Items: 
Size: 403113 Color: 8087
Size: 345940 Color: 6282
Size: 250943 Color: 167

Bin 2961: 5 of cap free
Amount of items: 3
Items: 
Size: 402719 Color: 8078
Size: 330929 Color: 5697
Size: 266348 Color: 1837

Bin 2962: 5 of cap free
Amount of items: 3
Items: 
Size: 406772 Color: 8193
Size: 329757 Color: 5651
Size: 263467 Color: 1583

Bin 2963: 5 of cap free
Amount of items: 3
Items: 
Size: 382160 Color: 7520
Size: 335338 Color: 5862
Size: 282498 Color: 3112

Bin 2964: 5 of cap free
Amount of items: 3
Items: 
Size: 399951 Color: 8007
Size: 344763 Color: 6240
Size: 255282 Color: 743

Bin 2965: 5 of cap free
Amount of items: 3
Items: 
Size: 365128 Color: 6983
Size: 326018 Color: 5500
Size: 308850 Color: 4639

Bin 2966: 5 of cap free
Amount of items: 3
Items: 
Size: 375287 Color: 7294
Size: 343450 Color: 6195
Size: 281259 Color: 3012

Bin 2967: 5 of cap free
Amount of items: 3
Items: 
Size: 371825 Color: 7200
Size: 346370 Color: 6304
Size: 281801 Color: 3053

Bin 2968: 5 of cap free
Amount of items: 3
Items: 
Size: 370801 Color: 7171
Size: 325357 Color: 5468
Size: 303838 Color: 4382

Bin 2969: 5 of cap free
Amount of items: 3
Items: 
Size: 370657 Color: 7164
Size: 324936 Color: 5449
Size: 304403 Color: 4415

Bin 2970: 5 of cap free
Amount of items: 3
Items: 
Size: 394195 Color: 7868
Size: 310900 Color: 4769
Size: 294901 Color: 3874

Bin 2971: 5 of cap free
Amount of items: 3
Items: 
Size: 365583 Color: 7010
Size: 318430 Color: 5144
Size: 315983 Color: 5013

Bin 2972: 5 of cap free
Amount of items: 3
Items: 
Size: 362934 Color: 6912
Size: 329295 Color: 5634
Size: 307767 Color: 4585

Bin 2973: 5 of cap free
Amount of items: 3
Items: 
Size: 358587 Color: 6773
Size: 358537 Color: 6772
Size: 282872 Color: 3134

Bin 2974: 5 of cap free
Amount of items: 3
Items: 
Size: 356740 Color: 6706
Size: 356710 Color: 6705
Size: 286546 Color: 3392

Bin 2975: 5 of cap free
Amount of items: 3
Items: 
Size: 355453 Color: 6667
Size: 355370 Color: 6662
Size: 289173 Color: 3542

Bin 2976: 5 of cap free
Amount of items: 3
Items: 
Size: 358268 Color: 6761
Size: 351267 Color: 6511
Size: 290461 Color: 3618

Bin 2977: 5 of cap free
Amount of items: 3
Items: 
Size: 384860 Color: 7588
Size: 323432 Color: 5399
Size: 291704 Color: 3700

Bin 2978: 5 of cap free
Amount of items: 3
Items: 
Size: 352379 Color: 6563
Size: 352050 Color: 6550
Size: 295567 Color: 3907

Bin 2979: 5 of cap free
Amount of items: 3
Items: 
Size: 352183 Color: 6554
Size: 352159 Color: 6553
Size: 295654 Color: 3914

Bin 2980: 5 of cap free
Amount of items: 3
Items: 
Size: 351705 Color: 6535
Size: 351670 Color: 6534
Size: 296621 Color: 3987

Bin 2981: 5 of cap free
Amount of items: 3
Items: 
Size: 369037 Color: 7118
Size: 350962 Color: 6500
Size: 279997 Color: 2927

Bin 2982: 5 of cap free
Amount of items: 3
Items: 
Size: 360698 Color: 6834
Size: 348141 Color: 6369
Size: 291157 Color: 3659

Bin 2983: 5 of cap free
Amount of items: 3
Items: 
Size: 349059 Color: 6412
Size: 349028 Color: 6411
Size: 301909 Color: 4263

Bin 2984: 6 of cap free
Amount of items: 3
Items: 
Size: 383443 Color: 7552
Size: 327389 Color: 5558
Size: 289163 Color: 3541

Bin 2985: 6 of cap free
Amount of items: 3
Items: 
Size: 374923 Color: 7286
Size: 344969 Color: 6245
Size: 280103 Color: 2937

Bin 2986: 6 of cap free
Amount of items: 3
Items: 
Size: 370924 Color: 7174
Size: 346794 Color: 6318
Size: 282277 Color: 3095

Bin 2987: 6 of cap free
Amount of items: 3
Items: 
Size: 382367 Color: 7524
Size: 343345 Color: 6189
Size: 274283 Color: 2491

Bin 2988: 6 of cap free
Amount of items: 3
Items: 
Size: 386472 Color: 7634
Size: 341410 Color: 6115
Size: 272113 Color: 2302

Bin 2989: 6 of cap free
Amount of items: 3
Items: 
Size: 395891 Color: 7914
Size: 337708 Color: 5967
Size: 266396 Color: 1841

Bin 2990: 6 of cap free
Amount of items: 3
Items: 
Size: 412457 Color: 8333
Size: 337474 Color: 5952
Size: 250064 Color: 9

Bin 2991: 6 of cap free
Amount of items: 3
Items: 
Size: 394776 Color: 7883
Size: 337979 Color: 5980
Size: 267240 Color: 1909

Bin 2992: 6 of cap free
Amount of items: 3
Items: 
Size: 338001 Color: 5982
Size: 335664 Color: 5880
Size: 326330 Color: 5511

Bin 2993: 6 of cap free
Amount of items: 3
Items: 
Size: 358047 Color: 6751
Size: 340408 Color: 6077
Size: 301540 Color: 4239

Bin 2994: 6 of cap free
Amount of items: 3
Items: 
Size: 384156 Color: 7573
Size: 361885 Color: 6872
Size: 253954 Color: 565

Bin 2995: 6 of cap free
Amount of items: 3
Items: 
Size: 380852 Color: 7474
Size: 351156 Color: 6507
Size: 267987 Color: 1973

Bin 2996: 6 of cap free
Amount of items: 3
Items: 
Size: 382255 Color: 7522
Size: 343962 Color: 6217
Size: 273778 Color: 2445

Bin 2997: 6 of cap free
Amount of items: 3
Items: 
Size: 375777 Color: 7312
Size: 328117 Color: 5590
Size: 296101 Color: 3954

Bin 2998: 6 of cap free
Amount of items: 3
Items: 
Size: 379014 Color: 7413
Size: 324804 Color: 5442
Size: 296177 Color: 3959

Bin 2999: 6 of cap free
Amount of items: 3
Items: 
Size: 381900 Color: 7514
Size: 336225 Color: 5911
Size: 281870 Color: 3059

Bin 3000: 6 of cap free
Amount of items: 3
Items: 
Size: 411852 Color: 8318
Size: 328506 Color: 5609
Size: 259637 Color: 1213

Bin 3001: 6 of cap free
Amount of items: 3
Items: 
Size: 406226 Color: 8167
Size: 326059 Color: 5501
Size: 267710 Color: 1955

Bin 3002: 6 of cap free
Amount of items: 3
Items: 
Size: 407805 Color: 8220
Size: 338039 Color: 5985
Size: 254151 Color: 601

Bin 3003: 6 of cap free
Amount of items: 3
Items: 
Size: 411598 Color: 8308
Size: 333230 Color: 5790
Size: 255167 Color: 729

Bin 3004: 6 of cap free
Amount of items: 3
Items: 
Size: 406749 Color: 8191
Size: 328314 Color: 5600
Size: 264932 Color: 1716

Bin 3005: 6 of cap free
Amount of items: 3
Items: 
Size: 410468 Color: 8282
Size: 332565 Color: 5763
Size: 256962 Color: 944

Bin 3006: 6 of cap free
Amount of items: 3
Items: 
Size: 408031 Color: 8226
Size: 332979 Color: 5776
Size: 258985 Color: 1154

Bin 3007: 6 of cap free
Amount of items: 3
Items: 
Size: 406265 Color: 8168
Size: 333014 Color: 5777
Size: 260716 Color: 1329

Bin 3008: 6 of cap free
Amount of items: 3
Items: 
Size: 404959 Color: 8133
Size: 334542 Color: 5840
Size: 260494 Color: 1309

Bin 3009: 6 of cap free
Amount of items: 3
Items: 
Size: 407041 Color: 8201
Size: 334106 Color: 5827
Size: 258848 Color: 1136

Bin 3010: 6 of cap free
Amount of items: 3
Items: 
Size: 410443 Color: 8281
Size: 298446 Color: 4082
Size: 291106 Color: 3656

Bin 3011: 6 of cap free
Amount of items: 3
Items: 
Size: 404183 Color: 8113
Size: 331538 Color: 5715
Size: 264274 Color: 1657

Bin 3012: 6 of cap free
Amount of items: 3
Items: 
Size: 375799 Color: 7313
Size: 329317 Color: 5635
Size: 294879 Color: 3868

Bin 3013: 6 of cap free
Amount of items: 3
Items: 
Size: 353505 Color: 6609
Size: 337097 Color: 5937
Size: 309393 Color: 4682

Bin 3014: 6 of cap free
Amount of items: 3
Items: 
Size: 364657 Color: 6969
Size: 349684 Color: 6434
Size: 285654 Color: 3336

Bin 3015: 6 of cap free
Amount of items: 3
Items: 
Size: 364598 Color: 6966
Size: 354022 Color: 6628
Size: 281375 Color: 3015

Bin 3016: 6 of cap free
Amount of items: 3
Items: 
Size: 402468 Color: 8071
Size: 346256 Color: 6298
Size: 251271 Color: 215

Bin 3017: 6 of cap free
Amount of items: 3
Items: 
Size: 363891 Color: 6945
Size: 330865 Color: 5693
Size: 305239 Color: 4462

Bin 3018: 6 of cap free
Amount of items: 3
Items: 
Size: 368192 Color: 7084
Size: 346137 Color: 6287
Size: 285666 Color: 3337

Bin 3019: 6 of cap free
Amount of items: 3
Items: 
Size: 411689 Color: 8311
Size: 327442 Color: 5561
Size: 260864 Color: 1345

Bin 3020: 6 of cap free
Amount of items: 3
Items: 
Size: 383712 Color: 7563
Size: 362647 Color: 6899
Size: 253636 Color: 519

Bin 3021: 6 of cap free
Amount of items: 3
Items: 
Size: 360944 Color: 6844
Size: 341594 Color: 6120
Size: 297457 Color: 4027

Bin 3022: 6 of cap free
Amount of items: 3
Items: 
Size: 360665 Color: 6832
Size: 359572 Color: 6798
Size: 279758 Color: 2910

Bin 3023: 6 of cap free
Amount of items: 3
Items: 
Size: 355138 Color: 6656
Size: 354978 Color: 6654
Size: 289879 Color: 3582

Bin 3024: 6 of cap free
Amount of items: 3
Items: 
Size: 353821 Color: 6621
Size: 325902 Color: 5494
Size: 320272 Color: 5242

Bin 3025: 6 of cap free
Amount of items: 3
Items: 
Size: 361871 Color: 6871
Size: 344955 Color: 6244
Size: 293169 Color: 3779

Bin 3026: 6 of cap free
Amount of items: 3
Items: 
Size: 352207 Color: 6555
Size: 351240 Color: 6510
Size: 296548 Color: 3981

Bin 3027: 6 of cap free
Amount of items: 3
Items: 
Size: 353592 Color: 6615
Size: 351517 Color: 6524
Size: 294886 Color: 3872

Bin 3028: 6 of cap free
Amount of items: 3
Items: 
Size: 365246 Color: 6994
Size: 351095 Color: 6503
Size: 283654 Color: 3189

Bin 3029: 6 of cap free
Amount of items: 3
Items: 
Size: 351206 Color: 6508
Size: 350405 Color: 6470
Size: 298384 Color: 4079

Bin 3030: 6 of cap free
Amount of items: 3
Items: 
Size: 348494 Color: 6386
Size: 348441 Color: 6384
Size: 303060 Color: 4340

Bin 3031: 7 of cap free
Amount of items: 3
Items: 
Size: 375401 Color: 7298
Size: 345781 Color: 6277
Size: 278812 Color: 2833

Bin 3032: 7 of cap free
Amount of items: 3
Items: 
Size: 376544 Color: 7340
Size: 345431 Color: 6261
Size: 278019 Color: 2776

Bin 3033: 7 of cap free
Amount of items: 3
Items: 
Size: 388202 Color: 7678
Size: 340579 Color: 6085
Size: 271213 Color: 2225

Bin 3034: 7 of cap free
Amount of items: 3
Items: 
Size: 395859 Color: 7912
Size: 337664 Color: 5965
Size: 266471 Color: 1852

Bin 3035: 7 of cap free
Amount of items: 3
Items: 
Size: 412362 Color: 8328
Size: 337425 Color: 5950
Size: 250207 Color: 43

Bin 3036: 7 of cap free
Amount of items: 3
Items: 
Size: 388427 Color: 7686
Size: 329629 Color: 5645
Size: 281938 Color: 3064

Bin 3037: 7 of cap free
Amount of items: 3
Items: 
Size: 378333 Color: 7386
Size: 361582 Color: 6865
Size: 260079 Color: 1261

Bin 3038: 7 of cap free
Amount of items: 3
Items: 
Size: 399140 Color: 7988
Size: 323945 Color: 5411
Size: 276909 Color: 2692

Bin 3039: 7 of cap free
Amount of items: 3
Items: 
Size: 373676 Color: 7248
Size: 362385 Color: 6887
Size: 263933 Color: 1622

Bin 3040: 7 of cap free
Amount of items: 3
Items: 
Size: 379078 Color: 7414
Size: 328598 Color: 5610
Size: 292318 Color: 3735

Bin 3041: 7 of cap free
Amount of items: 3
Items: 
Size: 382239 Color: 7521
Size: 342086 Color: 6139
Size: 275669 Color: 2587

Bin 3042: 7 of cap free
Amount of items: 3
Items: 
Size: 409784 Color: 8261
Size: 336186 Color: 5906
Size: 254024 Color: 579

Bin 3043: 7 of cap free
Amount of items: 3
Items: 
Size: 408181 Color: 8228
Size: 336165 Color: 5905
Size: 255648 Color: 789

Bin 3044: 7 of cap free
Amount of items: 3
Items: 
Size: 407629 Color: 8217
Size: 328874 Color: 5615
Size: 263491 Color: 1587

Bin 3045: 7 of cap free
Amount of items: 3
Items: 
Size: 404943 Color: 8131
Size: 332289 Color: 5748
Size: 262762 Color: 1501

Bin 3046: 7 of cap free
Amount of items: 3
Items: 
Size: 365259 Color: 6995
Size: 319157 Color: 5177
Size: 315578 Color: 4981

Bin 3047: 7 of cap free
Amount of items: 3
Items: 
Size: 406704 Color: 8189
Size: 330379 Color: 5673
Size: 262911 Color: 1517

Bin 3048: 7 of cap free
Amount of items: 3
Items: 
Size: 406394 Color: 8172
Size: 329725 Color: 5649
Size: 263875 Color: 1618

Bin 3049: 7 of cap free
Amount of items: 3
Items: 
Size: 389848 Color: 7724
Size: 345973 Color: 6283
Size: 264173 Color: 1648

Bin 3050: 7 of cap free
Amount of items: 3
Items: 
Size: 399639 Color: 7998
Size: 328392 Color: 5606
Size: 271963 Color: 2289

Bin 3051: 7 of cap free
Amount of items: 3
Items: 
Size: 365198 Color: 6987
Size: 321487 Color: 5298
Size: 313309 Color: 4878

Bin 3052: 7 of cap free
Amount of items: 3
Items: 
Size: 364754 Color: 6971
Size: 317973 Color: 5113
Size: 317267 Color: 5074

Bin 3053: 7 of cap free
Amount of items: 3
Items: 
Size: 348999 Color: 6408
Size: 340002 Color: 6064
Size: 310993 Color: 4771

Bin 3054: 7 of cap free
Amount of items: 3
Items: 
Size: 400097 Color: 8010
Size: 328357 Color: 5603
Size: 271540 Color: 2249

Bin 3055: 7 of cap free
Amount of items: 3
Items: 
Size: 359776 Color: 6804
Size: 335541 Color: 5874
Size: 304677 Color: 4426

Bin 3056: 7 of cap free
Amount of items: 3
Items: 
Size: 370626 Color: 7163
Size: 344387 Color: 6231
Size: 284981 Color: 3294

Bin 3057: 7 of cap free
Amount of items: 3
Items: 
Size: 364229 Color: 6956
Size: 328957 Color: 5622
Size: 306808 Color: 4548

Bin 3058: 7 of cap free
Amount of items: 3
Items: 
Size: 367524 Color: 7067
Size: 320718 Color: 5259
Size: 311752 Color: 4809

Bin 3059: 7 of cap free
Amount of items: 3
Items: 
Size: 363932 Color: 6947
Size: 330900 Color: 5695
Size: 305162 Color: 4459

Bin 3060: 7 of cap free
Amount of items: 3
Items: 
Size: 363091 Color: 6919
Size: 346547 Color: 6308
Size: 290356 Color: 3612

Bin 3061: 7 of cap free
Amount of items: 3
Items: 
Size: 362889 Color: 6910
Size: 334179 Color: 5828
Size: 302926 Color: 4324

Bin 3062: 7 of cap free
Amount of items: 3
Items: 
Size: 362981 Color: 6913
Size: 362504 Color: 6890
Size: 274509 Color: 2512

Bin 3063: 7 of cap free
Amount of items: 3
Items: 
Size: 362224 Color: 6882
Size: 337841 Color: 5975
Size: 299929 Color: 4160

Bin 3064: 7 of cap free
Amount of items: 3
Items: 
Size: 362170 Color: 6878
Size: 337773 Color: 5971
Size: 300051 Color: 4169

Bin 3065: 7 of cap free
Amount of items: 3
Items: 
Size: 361470 Color: 6860
Size: 360494 Color: 6824
Size: 278030 Color: 2777

Bin 3066: 7 of cap free
Amount of items: 3
Items: 
Size: 360774 Color: 6837
Size: 342468 Color: 6155
Size: 296752 Color: 3993

Bin 3067: 7 of cap free
Amount of items: 3
Items: 
Size: 360069 Color: 6811
Size: 359985 Color: 6810
Size: 279940 Color: 2922

Bin 3068: 7 of cap free
Amount of items: 3
Items: 
Size: 363488 Color: 6933
Size: 351608 Color: 6530
Size: 284898 Color: 3289

Bin 3069: 7 of cap free
Amount of items: 3
Items: 
Size: 352609 Color: 6575
Size: 352330 Color: 6560
Size: 295055 Color: 3882

Bin 3070: 7 of cap free
Amount of items: 3
Items: 
Size: 352260 Color: 6558
Size: 352017 Color: 6547
Size: 295717 Color: 3922

Bin 3071: 7 of cap free
Amount of items: 3
Items: 
Size: 351981 Color: 6545
Size: 351156 Color: 6506
Size: 296857 Color: 3996

Bin 3072: 7 of cap free
Amount of items: 3
Items: 
Size: 374994 Color: 7287
Size: 333535 Color: 5799
Size: 291465 Color: 3681

Bin 3073: 8 of cap free
Amount of items: 3
Items: 
Size: 399687 Color: 8002
Size: 337647 Color: 5962
Size: 262659 Color: 1491

Bin 3074: 8 of cap free
Amount of items: 3
Items: 
Size: 395809 Color: 7911
Size: 340917 Color: 6100
Size: 263267 Color: 1564

Bin 3075: 8 of cap free
Amount of items: 3
Items: 
Size: 391888 Color: 7790
Size: 338720 Color: 6012
Size: 269385 Color: 2084

Bin 3076: 8 of cap free
Amount of items: 3
Items: 
Size: 387059 Color: 7646
Size: 341013 Color: 6102
Size: 271921 Color: 2285

Bin 3077: 8 of cap free
Amount of items: 3
Items: 
Size: 405314 Color: 8142
Size: 336680 Color: 5926
Size: 257999 Color: 1039

Bin 3078: 8 of cap free
Amount of items: 3
Items: 
Size: 372485 Color: 7227
Size: 342725 Color: 6165
Size: 284783 Color: 3282

Bin 3079: 8 of cap free
Amount of items: 3
Items: 
Size: 381887 Color: 7513
Size: 342342 Color: 6150
Size: 275764 Color: 2592

Bin 3080: 8 of cap free
Amount of items: 3
Items: 
Size: 336312 Color: 5913
Size: 332446 Color: 5753
Size: 331235 Color: 5703

Bin 3081: 8 of cap free
Amount of items: 3
Items: 
Size: 348985 Color: 6406
Size: 331269 Color: 5706
Size: 319739 Color: 5213

Bin 3082: 8 of cap free
Amount of items: 3
Items: 
Size: 409886 Color: 8265
Size: 335867 Color: 5889
Size: 254240 Color: 614

Bin 3083: 8 of cap free
Amount of items: 3
Items: 
Size: 402213 Color: 8064
Size: 342645 Color: 6162
Size: 255135 Color: 728

Bin 3084: 8 of cap free
Amount of items: 3
Items: 
Size: 339621 Color: 6044
Size: 332139 Color: 5739
Size: 328233 Color: 5597

Bin 3085: 8 of cap free
Amount of items: 3
Items: 
Size: 404018 Color: 8111
Size: 335156 Color: 5854
Size: 260819 Color: 1341

Bin 3086: 8 of cap free
Amount of items: 3
Items: 
Size: 402854 Color: 8083
Size: 330379 Color: 5674
Size: 266760 Color: 1875

Bin 3087: 8 of cap free
Amount of items: 3
Items: 
Size: 401630 Color: 8049
Size: 322739 Color: 5362
Size: 275624 Color: 2582

Bin 3088: 8 of cap free
Amount of items: 3
Items: 
Size: 401141 Color: 8036
Size: 329348 Color: 5636
Size: 269504 Color: 2091

Bin 3089: 8 of cap free
Amount of items: 3
Items: 
Size: 370789 Color: 7170
Size: 341225 Color: 6110
Size: 287979 Color: 3480

Bin 3090: 8 of cap free
Amount of items: 3
Items: 
Size: 370444 Color: 7158
Size: 362444 Color: 6889
Size: 267105 Color: 1897

Bin 3091: 8 of cap free
Amount of items: 3
Items: 
Size: 363099 Color: 6921
Size: 330350 Color: 5670
Size: 306544 Color: 4529

Bin 3092: 8 of cap free
Amount of items: 3
Items: 
Size: 361044 Color: 6846
Size: 359889 Color: 6806
Size: 279060 Color: 2854

Bin 3093: 8 of cap free
Amount of items: 3
Items: 
Size: 356239 Color: 6691
Size: 355989 Color: 6686
Size: 287765 Color: 3468

Bin 3094: 8 of cap free
Amount of items: 3
Items: 
Size: 391172 Color: 7772
Size: 317023 Color: 5065
Size: 291798 Color: 3707

Bin 3095: 8 of cap free
Amount of items: 3
Items: 
Size: 352483 Color: 6569
Size: 351595 Color: 6529
Size: 295915 Color: 3942

Bin 3096: 8 of cap free
Amount of items: 3
Items: 
Size: 357774 Color: 6742
Size: 348315 Color: 6377
Size: 293904 Color: 3814

Bin 3097: 8 of cap free
Amount of items: 3
Items: 
Size: 353545 Color: 6613
Size: 352862 Color: 6585
Size: 293586 Color: 3794

Bin 3098: 8 of cap free
Amount of items: 3
Items: 
Size: 365569 Color: 7007
Size: 343055 Color: 6177
Size: 291369 Color: 3672

Bin 3099: 9 of cap free
Amount of items: 3
Items: 
Size: 352387 Color: 6564
Size: 341036 Color: 6103
Size: 306569 Color: 4531

Bin 3100: 9 of cap free
Amount of items: 3
Items: 
Size: 391020 Color: 7763
Size: 343171 Color: 6186
Size: 265801 Color: 1789

Bin 3101: 9 of cap free
Amount of items: 3
Items: 
Size: 411319 Color: 8304
Size: 335828 Color: 5886
Size: 252845 Color: 415

Bin 3102: 9 of cap free
Amount of items: 3
Items: 
Size: 389026 Color: 7708
Size: 340143 Color: 6069
Size: 270823 Color: 2189

Bin 3103: 9 of cap free
Amount of items: 3
Items: 
Size: 373766 Color: 7254
Size: 328156 Color: 5592
Size: 298070 Color: 4058

Bin 3104: 9 of cap free
Amount of items: 3
Items: 
Size: 406660 Color: 8185
Size: 342872 Color: 6172
Size: 250460 Color: 77

Bin 3105: 9 of cap free
Amount of items: 3
Items: 
Size: 398665 Color: 7974
Size: 318523 Color: 5149
Size: 282804 Color: 3129

Bin 3106: 9 of cap free
Amount of items: 3
Items: 
Size: 370555 Color: 7161
Size: 359161 Color: 6789
Size: 270276 Color: 2143

Bin 3107: 9 of cap free
Amount of items: 3
Items: 
Size: 377505 Color: 7362
Size: 343546 Color: 6198
Size: 278941 Color: 2843

Bin 3108: 9 of cap free
Amount of items: 3
Items: 
Size: 378050 Color: 7380
Size: 341662 Color: 6122
Size: 280280 Color: 2952

Bin 3109: 9 of cap free
Amount of items: 3
Items: 
Size: 400252 Color: 8014
Size: 333957 Color: 5819
Size: 265783 Color: 1786

Bin 3110: 9 of cap free
Amount of items: 3
Items: 
Size: 409612 Color: 8256
Size: 337453 Color: 5951
Size: 252927 Color: 430

Bin 3111: 9 of cap free
Amount of items: 3
Items: 
Size: 405557 Color: 8150
Size: 327590 Color: 5569
Size: 266845 Color: 1878

Bin 3112: 9 of cap free
Amount of items: 3
Items: 
Size: 405474 Color: 8147
Size: 332335 Color: 5751
Size: 262183 Color: 1461

Bin 3113: 9 of cap free
Amount of items: 3
Items: 
Size: 406306 Color: 8170
Size: 334072 Color: 5824
Size: 259614 Color: 1211

Bin 3114: 9 of cap free
Amount of items: 3
Items: 
Size: 409365 Color: 8248
Size: 333616 Color: 5801
Size: 257011 Color: 948

Bin 3115: 9 of cap free
Amount of items: 3
Items: 
Size: 403900 Color: 8106
Size: 331883 Color: 5724
Size: 264209 Color: 1653

Bin 3116: 9 of cap free
Amount of items: 3
Items: 
Size: 403571 Color: 8096
Size: 331511 Color: 5713
Size: 264910 Color: 1712

Bin 3117: 9 of cap free
Amount of items: 3
Items: 
Size: 342065 Color: 6138
Size: 335630 Color: 5878
Size: 322297 Color: 5342

Bin 3118: 9 of cap free
Amount of items: 3
Items: 
Size: 364906 Color: 6978
Size: 323456 Color: 5401
Size: 311630 Color: 4801

Bin 3119: 9 of cap free
Amount of items: 3
Items: 
Size: 363271 Color: 6930
Size: 332785 Color: 5769
Size: 303936 Color: 4390

Bin 3120: 9 of cap free
Amount of items: 3
Items: 
Size: 368359 Color: 7093
Size: 354287 Color: 6639
Size: 277346 Color: 2723

Bin 3121: 9 of cap free
Amount of items: 3
Items: 
Size: 350046 Color: 6454
Size: 332107 Color: 5737
Size: 317839 Color: 5104

Bin 3122: 9 of cap free
Amount of items: 3
Items: 
Size: 354080 Color: 6634
Size: 353405 Color: 6606
Size: 292507 Color: 3748

Bin 3123: 9 of cap free
Amount of items: 3
Items: 
Size: 360113 Color: 6813
Size: 343729 Color: 6207
Size: 296150 Color: 3958

Bin 3124: 9 of cap free
Amount of items: 3
Items: 
Size: 357637 Color: 6737
Size: 347725 Color: 6356
Size: 294630 Color: 3855

Bin 3125: 9 of cap free
Amount of items: 3
Items: 
Size: 352377 Color: 6562
Size: 341803 Color: 6129
Size: 305812 Color: 4491

Bin 3126: 9 of cap free
Amount of items: 3
Items: 
Size: 358209 Color: 6759
Size: 348387 Color: 6382
Size: 293396 Color: 3787

Bin 3127: 9 of cap free
Amount of items: 3
Items: 
Size: 363245 Color: 6928
Size: 349199 Color: 6419
Size: 287548 Color: 3458

Bin 3128: 9 of cap free
Amount of items: 3
Items: 
Size: 399265 Color: 7992
Size: 349274 Color: 6423
Size: 251453 Color: 242

Bin 3129: 10 of cap free
Amount of items: 3
Items: 
Size: 393831 Color: 7861
Size: 338391 Color: 5999
Size: 267769 Color: 1962

Bin 3130: 10 of cap free
Amount of items: 3
Items: 
Size: 373712 Color: 7249
Size: 346156 Color: 6288
Size: 280123 Color: 2939

Bin 3131: 10 of cap free
Amount of items: 3
Items: 
Size: 384229 Color: 7574
Size: 342493 Color: 6156
Size: 273269 Color: 2409

Bin 3132: 10 of cap free
Amount of items: 3
Items: 
Size: 389115 Color: 7711
Size: 340113 Color: 6067
Size: 270763 Color: 2184

Bin 3133: 10 of cap free
Amount of items: 3
Items: 
Size: 410645 Color: 8285
Size: 335949 Color: 5892
Size: 253397 Color: 495

Bin 3134: 10 of cap free
Amount of items: 3
Items: 
Size: 394500 Color: 7875
Size: 331138 Color: 5701
Size: 274353 Color: 2498

Bin 3135: 10 of cap free
Amount of items: 3
Items: 
Size: 374249 Color: 7267
Size: 320225 Color: 5241
Size: 305517 Color: 4476

Bin 3136: 10 of cap free
Amount of items: 3
Items: 
Size: 402741 Color: 8079
Size: 329562 Color: 5642
Size: 267688 Color: 1952

Bin 3137: 10 of cap free
Amount of items: 3
Items: 
Size: 374138 Color: 7263
Size: 366103 Color: 7024
Size: 259750 Color: 1223

Bin 3138: 10 of cap free
Amount of items: 3
Items: 
Size: 403208 Color: 8090
Size: 318569 Color: 5152
Size: 278214 Color: 2787

Bin 3139: 10 of cap free
Amount of items: 3
Items: 
Size: 371604 Color: 7194
Size: 340175 Color: 6070
Size: 288212 Color: 3491

Bin 3140: 10 of cap free
Amount of items: 3
Items: 
Size: 366238 Color: 7030
Size: 319855 Color: 5220
Size: 313898 Color: 4901

Bin 3141: 10 of cap free
Amount of items: 3
Items: 
Size: 365756 Color: 7012
Size: 319480 Color: 5196
Size: 314755 Color: 4948

Bin 3142: 10 of cap free
Amount of items: 3
Items: 
Size: 361541 Color: 6861
Size: 358762 Color: 6778
Size: 279688 Color: 2903

Bin 3143: 10 of cap free
Amount of items: 3
Items: 
Size: 356564 Color: 6699
Size: 356519 Color: 6698
Size: 286908 Color: 3421

Bin 3144: 10 of cap free
Amount of items: 3
Items: 
Size: 350823 Color: 6490
Size: 350802 Color: 6489
Size: 298366 Color: 4074

Bin 3145: 10 of cap free
Amount of items: 3
Items: 
Size: 372419 Color: 7226
Size: 346890 Color: 6322
Size: 280682 Color: 2977

Bin 3146: 10 of cap free
Amount of items: 3
Items: 
Size: 361148 Color: 6849
Size: 337143 Color: 5940
Size: 301700 Color: 4249

Bin 3147: 11 of cap free
Amount of items: 3
Items: 
Size: 395253 Color: 7897
Size: 337838 Color: 5973
Size: 266899 Color: 1879

Bin 3148: 11 of cap free
Amount of items: 3
Items: 
Size: 392191 Color: 7803
Size: 339061 Color: 6023
Size: 268738 Color: 2026

Bin 3149: 11 of cap free
Amount of items: 3
Items: 
Size: 391943 Color: 7792
Size: 339157 Color: 6024
Size: 268890 Color: 2043

Bin 3150: 11 of cap free
Amount of items: 3
Items: 
Size: 400829 Color: 8030
Size: 328091 Color: 5587
Size: 271070 Color: 2209

Bin 3151: 11 of cap free
Amount of items: 3
Items: 
Size: 394246 Color: 7870
Size: 338951 Color: 6017
Size: 266793 Color: 1876

Bin 3152: 11 of cap free
Amount of items: 3
Items: 
Size: 392106 Color: 7801
Size: 338994 Color: 6021
Size: 268890 Color: 2044

Bin 3153: 11 of cap free
Amount of items: 3
Items: 
Size: 409644 Color: 8257
Size: 326416 Color: 5516
Size: 263930 Color: 1621

Bin 3154: 11 of cap free
Amount of items: 3
Items: 
Size: 404459 Color: 8122
Size: 332177 Color: 5740
Size: 263354 Color: 1570

Bin 3155: 11 of cap free
Amount of items: 3
Items: 
Size: 375813 Color: 7314
Size: 371010 Color: 7175
Size: 253167 Color: 461

Bin 3156: 11 of cap free
Amount of items: 3
Items: 
Size: 364794 Color: 6973
Size: 324592 Color: 5434
Size: 310604 Color: 4758

Bin 3157: 11 of cap free
Amount of items: 3
Items: 
Size: 368738 Color: 7107
Size: 347018 Color: 6330
Size: 284234 Color: 3234

Bin 3158: 11 of cap free
Amount of items: 3
Items: 
Size: 368086 Color: 7079
Size: 321496 Color: 5299
Size: 310408 Color: 4744

Bin 3159: 11 of cap free
Amount of items: 3
Items: 
Size: 395444 Color: 7902
Size: 313044 Color: 4867
Size: 291502 Color: 3685

Bin 3160: 11 of cap free
Amount of items: 3
Items: 
Size: 360868 Color: 6840
Size: 341944 Color: 6133
Size: 297178 Color: 4017

Bin 3161: 11 of cap free
Amount of items: 3
Items: 
Size: 353655 Color: 6617
Size: 353651 Color: 6616
Size: 292684 Color: 3757

Bin 3162: 11 of cap free
Amount of items: 3
Items: 
Size: 368904 Color: 7114
Size: 339868 Color: 6055
Size: 291218 Color: 3663

Bin 3163: 11 of cap free
Amount of items: 3
Items: 
Size: 351007 Color: 6501
Size: 350734 Color: 6482
Size: 298249 Color: 4071

Bin 3164: 12 of cap free
Amount of items: 3
Items: 
Size: 403869 Color: 8104
Size: 320908 Color: 5273
Size: 275212 Color: 2557

Bin 3165: 12 of cap free
Amount of items: 3
Items: 
Size: 395791 Color: 7909
Size: 337184 Color: 5941
Size: 267014 Color: 1888

Bin 3166: 12 of cap free
Amount of items: 3
Items: 
Size: 386419 Color: 7632
Size: 339766 Color: 6050
Size: 273804 Color: 2446

Bin 3167: 12 of cap free
Amount of items: 3
Items: 
Size: 403898 Color: 8105
Size: 331717 Color: 5722
Size: 264374 Color: 1669

Bin 3168: 12 of cap free
Amount of items: 3
Items: 
Size: 362539 Color: 6894
Size: 327061 Color: 5543
Size: 310389 Color: 4741

Bin 3169: 12 of cap free
Amount of items: 3
Items: 
Size: 401733 Color: 8051
Size: 331447 Color: 5711
Size: 266809 Color: 1877

Bin 3170: 12 of cap free
Amount of items: 3
Items: 
Size: 364558 Color: 6964
Size: 326486 Color: 5520
Size: 308945 Color: 4648

Bin 3171: 12 of cap free
Amount of items: 3
Items: 
Size: 373546 Color: 7243
Size: 327107 Color: 5546
Size: 299336 Color: 4133

Bin 3172: 12 of cap free
Amount of items: 3
Items: 
Size: 360518 Color: 6825
Size: 356581 Color: 6701
Size: 282890 Color: 3137

Bin 3173: 12 of cap free
Amount of items: 3
Items: 
Size: 358822 Color: 6780
Size: 332050 Color: 5734
Size: 309117 Color: 4658

Bin 3174: 12 of cap free
Amount of items: 3
Items: 
Size: 373623 Color: 7247
Size: 357658 Color: 6739
Size: 268708 Color: 2023

Bin 3175: 13 of cap free
Amount of items: 3
Items: 
Size: 411569 Color: 8307
Size: 337308 Color: 5946
Size: 251111 Color: 193

Bin 3176: 13 of cap free
Amount of items: 3
Items: 
Size: 409179 Color: 8246
Size: 329070 Color: 5625
Size: 261739 Color: 1425

Bin 3177: 13 of cap free
Amount of items: 3
Items: 
Size: 400885 Color: 8032
Size: 338658 Color: 6010
Size: 260445 Color: 1305

Bin 3178: 13 of cap free
Amount of items: 3
Items: 
Size: 405212 Color: 8139
Size: 334688 Color: 5843
Size: 260088 Color: 1263

Bin 3179: 13 of cap free
Amount of items: 3
Items: 
Size: 407040 Color: 8200
Size: 339568 Color: 6039
Size: 253380 Color: 493

Bin 3180: 13 of cap free
Amount of items: 3
Items: 
Size: 365206 Color: 6988
Size: 320745 Color: 5263
Size: 314037 Color: 4906

Bin 3181: 13 of cap free
Amount of items: 3
Items: 
Size: 398556 Color: 7973
Size: 328219 Color: 5595
Size: 273213 Color: 2404

Bin 3182: 13 of cap free
Amount of items: 3
Items: 
Size: 386702 Color: 7639
Size: 344875 Color: 6243
Size: 268411 Color: 2007

Bin 3183: 13 of cap free
Amount of items: 3
Items: 
Size: 371735 Color: 7196
Size: 325838 Color: 5490
Size: 302415 Color: 4295

Bin 3184: 13 of cap free
Amount of items: 3
Items: 
Size: 410781 Color: 8290
Size: 312056 Color: 4826
Size: 277151 Color: 2710

Bin 3185: 13 of cap free
Amount of items: 3
Items: 
Size: 366326 Color: 7032
Size: 327772 Color: 5575
Size: 305890 Color: 4497

Bin 3186: 13 of cap free
Amount of items: 3
Items: 
Size: 387225 Color: 7648
Size: 349618 Color: 6430
Size: 263145 Color: 1546

Bin 3187: 13 of cap free
Amount of items: 3
Items: 
Size: 360223 Color: 6815
Size: 359407 Color: 6793
Size: 280358 Color: 2962

Bin 3188: 13 of cap free
Amount of items: 3
Items: 
Size: 355936 Color: 6685
Size: 353042 Color: 6593
Size: 291010 Color: 3649

Bin 3189: 13 of cap free
Amount of items: 3
Items: 
Size: 368424 Color: 7096
Size: 345387 Color: 6259
Size: 286177 Color: 3366

Bin 3190: 14 of cap free
Amount of items: 3
Items: 
Size: 369638 Color: 7135
Size: 316150 Color: 5018
Size: 314199 Color: 4916

Bin 3191: 14 of cap free
Amount of items: 3
Items: 
Size: 384999 Color: 7590
Size: 342200 Color: 6144
Size: 272788 Color: 2368

Bin 3192: 14 of cap free
Amount of items: 3
Items: 
Size: 344057 Color: 6220
Size: 329214 Color: 5629
Size: 326716 Color: 5529

Bin 3193: 14 of cap free
Amount of items: 3
Items: 
Size: 408550 Color: 8232
Size: 336032 Color: 5896
Size: 255405 Color: 755

Bin 3194: 14 of cap free
Amount of items: 3
Items: 
Size: 404218 Color: 8114
Size: 332747 Color: 5768
Size: 263022 Color: 1535

Bin 3195: 14 of cap free
Amount of items: 3
Items: 
Size: 340808 Color: 6094
Size: 334433 Color: 5837
Size: 324746 Color: 5438

Bin 3196: 14 of cap free
Amount of items: 3
Items: 
Size: 364034 Color: 6951
Size: 329981 Color: 5659
Size: 305972 Color: 4501

Bin 3197: 14 of cap free
Amount of items: 3
Items: 
Size: 367834 Color: 7071
Size: 320791 Color: 5266
Size: 311362 Color: 4790

Bin 3198: 14 of cap free
Amount of items: 3
Items: 
Size: 357367 Color: 6733
Size: 356786 Color: 6708
Size: 285834 Color: 3350

Bin 3199: 14 of cap free
Amount of items: 3
Items: 
Size: 355479 Color: 6668
Size: 354893 Color: 6651
Size: 289615 Color: 3571

Bin 3200: 14 of cap free
Amount of items: 3
Items: 
Size: 387008 Color: 7644
Size: 353513 Color: 6611
Size: 259466 Color: 1194

Bin 3201: 14 of cap free
Amount of items: 3
Items: 
Size: 361984 Color: 6874
Size: 350253 Color: 6464
Size: 287750 Color: 3467

Bin 3202: 15 of cap free
Amount of items: 3
Items: 
Size: 384759 Color: 7587
Size: 342317 Color: 6148
Size: 272910 Color: 2379

Bin 3203: 15 of cap free
Amount of items: 3
Items: 
Size: 394786 Color: 7884
Size: 338008 Color: 5983
Size: 267192 Color: 1904

Bin 3204: 15 of cap free
Amount of items: 3
Items: 
Size: 348344 Color: 6379
Size: 331962 Color: 5730
Size: 319680 Color: 5209

Bin 3205: 15 of cap free
Amount of items: 3
Items: 
Size: 401825 Color: 8053
Size: 342225 Color: 6145
Size: 255936 Color: 817

Bin 3206: 15 of cap free
Amount of items: 3
Items: 
Size: 409386 Color: 8250
Size: 333667 Color: 5804
Size: 256933 Color: 942

Bin 3207: 15 of cap free
Amount of items: 3
Items: 
Size: 403666 Color: 8098
Size: 331666 Color: 5721
Size: 264654 Color: 1693

Bin 3208: 15 of cap free
Amount of items: 3
Items: 
Size: 392328 Color: 7805
Size: 319659 Color: 5207
Size: 287999 Color: 3481

Bin 3209: 15 of cap free
Amount of items: 3
Items: 
Size: 364557 Color: 6963
Size: 355509 Color: 6671
Size: 279920 Color: 2919

Bin 3210: 15 of cap free
Amount of items: 3
Items: 
Size: 350843 Color: 6491
Size: 350780 Color: 6487
Size: 298363 Color: 4073

Bin 3211: 15 of cap free
Amount of items: 3
Items: 
Size: 376425 Color: 7335
Size: 347638 Color: 6352
Size: 275923 Color: 2608

Bin 3212: 16 of cap free
Amount of items: 3
Items: 
Size: 379607 Color: 7432
Size: 344259 Color: 6227
Size: 276119 Color: 2622

Bin 3213: 16 of cap free
Amount of items: 3
Items: 
Size: 399772 Color: 8004
Size: 349830 Color: 6441
Size: 250383 Color: 65

Bin 3214: 16 of cap free
Amount of items: 3
Items: 
Size: 407224 Color: 8208
Size: 334363 Color: 5834
Size: 258398 Color: 1088

Bin 3215: 16 of cap free
Amount of items: 3
Items: 
Size: 407180 Color: 8206
Size: 331574 Color: 5717
Size: 261231 Color: 1380

Bin 3216: 16 of cap free
Amount of items: 3
Items: 
Size: 346913 Color: 6323
Size: 331945 Color: 5729
Size: 321127 Color: 5283

Bin 3217: 16 of cap free
Amount of items: 3
Items: 
Size: 371901 Color: 7205
Size: 362725 Color: 6901
Size: 265359 Color: 1752

Bin 3218: 17 of cap free
Amount of items: 3
Items: 
Size: 392154 Color: 7802
Size: 338993 Color: 6020
Size: 268837 Color: 2036

Bin 3219: 17 of cap free
Amount of items: 3
Items: 
Size: 400633 Color: 8025
Size: 320888 Color: 5270
Size: 278463 Color: 2806

Bin 3220: 17 of cap free
Amount of items: 3
Items: 
Size: 406799 Color: 8194
Size: 337474 Color: 5953
Size: 255711 Color: 797

Bin 3221: 17 of cap free
Amount of items: 3
Items: 
Size: 365222 Color: 6991
Size: 318923 Color: 5167
Size: 315839 Color: 4998

Bin 3222: 17 of cap free
Amount of items: 3
Items: 
Size: 361307 Color: 6852
Size: 339802 Color: 6053
Size: 298875 Color: 4108

Bin 3223: 17 of cap free
Amount of items: 3
Items: 
Size: 364435 Color: 6960
Size: 356579 Color: 6700
Size: 278970 Color: 2844

Bin 3224: 18 of cap free
Amount of items: 3
Items: 
Size: 403187 Color: 8089
Size: 330538 Color: 5682
Size: 266258 Color: 1825

Bin 3225: 18 of cap free
Amount of items: 3
Items: 
Size: 351517 Color: 6525
Size: 333493 Color: 5795
Size: 314973 Color: 4955

Bin 3226: 18 of cap free
Amount of items: 3
Items: 
Size: 407539 Color: 8215
Size: 336047 Color: 5899
Size: 256397 Color: 876

Bin 3227: 18 of cap free
Amount of items: 3
Items: 
Size: 412294 Color: 8326
Size: 329897 Color: 5657
Size: 257792 Color: 1017

Bin 3228: 18 of cap free
Amount of items: 3
Items: 
Size: 400056 Color: 8008
Size: 328763 Color: 5614
Size: 271164 Color: 2221

Bin 3229: 19 of cap free
Amount of items: 3
Items: 
Size: 352936 Color: 6588
Size: 351895 Color: 6543
Size: 295151 Color: 3890

Bin 3230: 20 of cap free
Amount of items: 3
Items: 
Size: 360653 Color: 6829
Size: 358349 Color: 6765
Size: 280979 Color: 2990

Bin 3231: 20 of cap free
Amount of items: 3
Items: 
Size: 405946 Color: 8163
Size: 333146 Color: 5784
Size: 260889 Color: 1349

Bin 3232: 20 of cap free
Amount of items: 3
Items: 
Size: 364149 Color: 6953
Size: 328717 Color: 5612
Size: 307115 Color: 4559

Bin 3233: 20 of cap free
Amount of items: 3
Items: 
Size: 353273 Color: 6603
Size: 353201 Color: 6600
Size: 293507 Color: 3790

Bin 3234: 21 of cap free
Amount of items: 3
Items: 
Size: 385414 Color: 7599
Size: 341974 Color: 6135
Size: 272592 Color: 2348

Bin 3235: 21 of cap free
Amount of items: 3
Items: 
Size: 391171 Color: 7771
Size: 339614 Color: 6043
Size: 269195 Color: 2072

Bin 3236: 21 of cap free
Amount of items: 3
Items: 
Size: 385500 Color: 7603
Size: 340469 Color: 6078
Size: 274011 Color: 2467

Bin 3237: 21 of cap free
Amount of items: 3
Items: 
Size: 400937 Color: 8033
Size: 329627 Color: 5644
Size: 269416 Color: 2087

Bin 3238: 22 of cap free
Amount of items: 3
Items: 
Size: 342371 Color: 6151
Size: 337546 Color: 5957
Size: 320062 Color: 5230

Bin 3239: 22 of cap free
Amount of items: 3
Items: 
Size: 378467 Color: 7394
Size: 333643 Color: 5803
Size: 287869 Color: 3475

Bin 3240: 22 of cap free
Amount of items: 3
Items: 
Size: 406857 Color: 8197
Size: 332891 Color: 5772
Size: 260231 Color: 1277

Bin 3241: 22 of cap free
Amount of items: 3
Items: 
Size: 370834 Color: 7173
Size: 325353 Color: 5467
Size: 303792 Color: 4378

Bin 3242: 22 of cap free
Amount of items: 3
Items: 
Size: 386613 Color: 7636
Size: 361219 Color: 6851
Size: 252147 Color: 325

Bin 3243: 23 of cap free
Amount of items: 3
Items: 
Size: 365780 Color: 7013
Size: 327915 Color: 5580
Size: 306283 Color: 4516

Bin 3244: 23 of cap free
Amount of items: 3
Items: 
Size: 379658 Color: 7433
Size: 344203 Color: 6225
Size: 276117 Color: 2621

Bin 3245: 23 of cap free
Amount of items: 3
Items: 
Size: 390988 Color: 7762
Size: 332045 Color: 5733
Size: 276945 Color: 2693

Bin 3246: 23 of cap free
Amount of items: 3
Items: 
Size: 368681 Color: 7106
Size: 323126 Color: 5389
Size: 308171 Color: 4607

Bin 3247: 23 of cap free
Amount of items: 3
Items: 
Size: 363090 Color: 6918
Size: 334400 Color: 5835
Size: 302488 Color: 4298

Bin 3248: 23 of cap free
Amount of items: 3
Items: 
Size: 360784 Color: 6838
Size: 343401 Color: 6194
Size: 295793 Color: 3932

Bin 3249: 23 of cap free
Amount of items: 3
Items: 
Size: 350733 Color: 6481
Size: 350602 Color: 6475
Size: 298643 Color: 4092

Bin 3250: 24 of cap free
Amount of items: 3
Items: 
Size: 388310 Color: 7681
Size: 340509 Color: 6083
Size: 271158 Color: 2219

Bin 3251: 24 of cap free
Amount of items: 3
Items: 
Size: 369421 Color: 7130
Size: 350928 Color: 6497
Size: 279628 Color: 2895

Bin 3252: 24 of cap free
Amount of items: 3
Items: 
Size: 401009 Color: 8034
Size: 346647 Color: 6310
Size: 252321 Color: 345

Bin 3253: 24 of cap free
Amount of items: 3
Items: 
Size: 400187 Color: 8012
Size: 328901 Color: 5618
Size: 270889 Color: 2195

Bin 3254: 25 of cap free
Amount of items: 3
Items: 
Size: 411654 Color: 8309
Size: 314440 Color: 4933
Size: 273882 Color: 2455

Bin 3255: 25 of cap free
Amount of items: 3
Items: 
Size: 345647 Color: 6270
Size: 343572 Color: 6199
Size: 310757 Color: 4763

Bin 3256: 25 of cap free
Amount of items: 3
Items: 
Size: 363773 Color: 6939
Size: 346686 Color: 6313
Size: 289517 Color: 3565

Bin 3257: 25 of cap free
Amount of items: 3
Items: 
Size: 362931 Color: 6911
Size: 334099 Color: 5826
Size: 302946 Color: 4327

Bin 3258: 25 of cap free
Amount of items: 3
Items: 
Size: 368646 Color: 7105
Size: 341719 Color: 6123
Size: 289611 Color: 3570

Bin 3259: 26 of cap free
Amount of items: 3
Items: 
Size: 368351 Color: 7091
Size: 347687 Color: 6354
Size: 283937 Color: 3214

Bin 3260: 26 of cap free
Amount of items: 3
Items: 
Size: 402311 Color: 8067
Size: 334837 Color: 5845
Size: 262827 Color: 1509

Bin 3261: 26 of cap free
Amount of items: 3
Items: 
Size: 401547 Color: 8048
Size: 330147 Color: 5664
Size: 268281 Color: 1999

Bin 3262: 26 of cap free
Amount of items: 3
Items: 
Size: 359283 Color: 6791
Size: 357104 Color: 6721
Size: 283588 Color: 3182

Bin 3263: 26 of cap free
Amount of items: 3
Items: 
Size: 351480 Color: 6522
Size: 351472 Color: 6521
Size: 297023 Color: 4005

Bin 3264: 27 of cap free
Amount of items: 3
Items: 
Size: 377928 Color: 7376
Size: 344994 Color: 6246
Size: 277052 Color: 2700

Bin 3265: 28 of cap free
Amount of items: 3
Items: 
Size: 381489 Color: 7500
Size: 340799 Color: 6093
Size: 277685 Color: 2750

Bin 3266: 28 of cap free
Amount of items: 3
Items: 
Size: 379759 Color: 7436
Size: 360242 Color: 6816
Size: 259972 Color: 1248

Bin 3267: 28 of cap free
Amount of items: 3
Items: 
Size: 398708 Color: 7976
Size: 350240 Color: 6462
Size: 251025 Color: 179

Bin 3268: 29 of cap free
Amount of items: 3
Items: 
Size: 341097 Color: 6107
Size: 336212 Color: 5908
Size: 322663 Color: 5359

Bin 3269: 29 of cap free
Amount of items: 3
Items: 
Size: 344669 Color: 6237
Size: 340905 Color: 6098
Size: 314398 Color: 4929

Bin 3270: 29 of cap free
Amount of items: 3
Items: 
Size: 350645 Color: 6476
Size: 350549 Color: 6473
Size: 298778 Color: 4102

Bin 3271: 29 of cap free
Amount of items: 3
Items: 
Size: 361215 Color: 6850
Size: 350094 Color: 6455
Size: 288663 Color: 3514

Bin 3272: 30 of cap free
Amount of items: 3
Items: 
Size: 404446 Color: 8121
Size: 335360 Color: 5864
Size: 260165 Color: 1271

Bin 3273: 30 of cap free
Amount of items: 3
Items: 
Size: 399798 Color: 8005
Size: 328685 Color: 5611
Size: 271488 Color: 2247

Bin 3274: 30 of cap free
Amount of items: 3
Items: 
Size: 364685 Color: 6970
Size: 352737 Color: 6581
Size: 282549 Color: 3116

Bin 3275: 31 of cap free
Amount of items: 3
Items: 
Size: 378241 Color: 7383
Size: 341614 Color: 6121
Size: 280115 Color: 2938

Bin 3276: 31 of cap free
Amount of items: 3
Items: 
Size: 357026 Color: 6717
Size: 354531 Color: 6645
Size: 288413 Color: 3500

Bin 3277: 33 of cap free
Amount of items: 3
Items: 
Size: 353706 Color: 6619
Size: 352269 Color: 6559
Size: 293993 Color: 3820

Bin 3278: 34 of cap free
Amount of items: 3
Items: 
Size: 393722 Color: 7858
Size: 334274 Color: 5831
Size: 271971 Color: 2290

Bin 3279: 34 of cap free
Amount of items: 3
Items: 
Size: 399314 Color: 7993
Size: 335853 Color: 5888
Size: 264800 Color: 1701

Bin 3280: 36 of cap free
Amount of items: 3
Items: 
Size: 382798 Color: 7533
Size: 343098 Color: 6182
Size: 274069 Color: 2471

Bin 3281: 36 of cap free
Amount of items: 3
Items: 
Size: 378962 Color: 7410
Size: 364946 Color: 6979
Size: 256057 Color: 831

Bin 3282: 36 of cap free
Amount of items: 3
Items: 
Size: 366128 Color: 7027
Size: 361572 Color: 6863
Size: 272265 Color: 2319

Bin 3283: 37 of cap free
Amount of items: 3
Items: 
Size: 392255 Color: 7804
Size: 322067 Color: 5332
Size: 285642 Color: 3334

Bin 3284: 37 of cap free
Amount of items: 3
Items: 
Size: 402252 Color: 8065
Size: 311648 Color: 4803
Size: 286064 Color: 3360

Bin 3285: 38 of cap free
Amount of items: 3
Items: 
Size: 375703 Color: 7310
Size: 347530 Color: 6348
Size: 276730 Color: 2676

Bin 3286: 38 of cap free
Amount of items: 3
Items: 
Size: 392755 Color: 7825
Size: 338802 Color: 6013
Size: 268406 Color: 2006

Bin 3287: 38 of cap free
Amount of items: 3
Items: 
Size: 402696 Color: 8077
Size: 331217 Color: 5702
Size: 266050 Color: 1806

Bin 3288: 39 of cap free
Amount of items: 3
Items: 
Size: 410243 Color: 8272
Size: 305753 Color: 4487
Size: 283966 Color: 3218

Bin 3289: 44 of cap free
Amount of items: 3
Items: 
Size: 411042 Color: 8296
Size: 329053 Color: 5624
Size: 259862 Color: 1234

Bin 3290: 44 of cap free
Amount of items: 3
Items: 
Size: 376073 Color: 7321
Size: 325764 Color: 5488
Size: 298120 Color: 4063

Bin 3291: 45 of cap free
Amount of items: 3
Items: 
Size: 411827 Color: 8317
Size: 307934 Color: 4593
Size: 280195 Color: 2946

Bin 3292: 48 of cap free
Amount of items: 3
Items: 
Size: 400719 Color: 8026
Size: 331640 Color: 5720
Size: 267594 Color: 1944

Bin 3293: 51 of cap free
Amount of items: 3
Items: 
Size: 407469 Color: 8214
Size: 332208 Color: 5743
Size: 260273 Color: 1284

Bin 3294: 53 of cap free
Amount of items: 3
Items: 
Size: 405887 Color: 8158
Size: 342744 Color: 6166
Size: 251317 Color: 223

Bin 3295: 55 of cap free
Amount of items: 3
Items: 
Size: 373757 Color: 7253
Size: 343480 Color: 6197
Size: 282709 Color: 3121

Bin 3296: 56 of cap free
Amount of items: 3
Items: 
Size: 404687 Color: 8126
Size: 332206 Color: 5742
Size: 263052 Color: 1537

Bin 3297: 56 of cap free
Amount of items: 3
Items: 
Size: 400105 Color: 8011
Size: 342798 Color: 6169
Size: 257042 Color: 952

Bin 3298: 56 of cap free
Amount of items: 3
Items: 
Size: 359849 Color: 6805
Size: 358878 Color: 6782
Size: 281218 Color: 3009

Bin 3299: 62 of cap free
Amount of items: 3
Items: 
Size: 410568 Color: 8283
Size: 316277 Color: 5030
Size: 273094 Color: 2393

Bin 3300: 62 of cap free
Amount of items: 3
Items: 
Size: 380010 Color: 7445
Size: 349065 Color: 6413
Size: 270864 Color: 2193

Bin 3301: 65 of cap free
Amount of items: 3
Items: 
Size: 378617 Color: 7398
Size: 342575 Color: 6160
Size: 278744 Color: 2826

Bin 3302: 65 of cap free
Amount of items: 3
Items: 
Size: 391392 Color: 7776
Size: 339478 Color: 6037
Size: 269066 Color: 2064

Bin 3303: 65 of cap free
Amount of items: 3
Items: 
Size: 357940 Color: 6749
Size: 357913 Color: 6747
Size: 284083 Color: 3225

Bin 3304: 73 of cap free
Amount of items: 3
Items: 
Size: 412421 Color: 8331
Size: 337331 Color: 5947
Size: 250176 Color: 34

Bin 3305: 74 of cap free
Amount of items: 3
Items: 
Size: 359552 Color: 6796
Size: 352728 Color: 6579
Size: 287647 Color: 3462

Bin 3306: 76 of cap free
Amount of items: 3
Items: 
Size: 383217 Color: 7544
Size: 310044 Color: 4722
Size: 306664 Color: 4539

Bin 3307: 78 of cap free
Amount of items: 3
Items: 
Size: 372201 Color: 7218
Size: 352539 Color: 6570
Size: 275183 Color: 2553

Bin 3308: 83 of cap free
Amount of items: 3
Items: 
Size: 392510 Color: 7817
Size: 355220 Color: 6659
Size: 252188 Color: 330

Bin 3309: 84 of cap free
Amount of items: 3
Items: 
Size: 408773 Color: 8238
Size: 336325 Color: 5916
Size: 254819 Color: 688

Bin 3310: 86 of cap free
Amount of items: 3
Items: 
Size: 368153 Color: 7083
Size: 327524 Color: 5565
Size: 304238 Color: 4405

Bin 3311: 89 of cap free
Amount of items: 3
Items: 
Size: 348967 Color: 6405
Size: 341767 Color: 6128
Size: 309178 Color: 4666

Bin 3312: 92 of cap free
Amount of items: 3
Items: 
Size: 364324 Color: 6958
Size: 360885 Color: 6841
Size: 274700 Color: 2519

Bin 3313: 94 of cap free
Amount of items: 3
Items: 
Size: 389886 Color: 7725
Size: 341939 Color: 6132
Size: 268082 Color: 1984

Bin 3314: 103 of cap free
Amount of items: 2
Items: 
Size: 499999 Color: 10001
Size: 499899 Color: 10000

Bin 3315: 104 of cap free
Amount of items: 3
Items: 
Size: 371878 Color: 7203
Size: 335454 Color: 5871
Size: 292565 Color: 3751

Bin 3316: 109 of cap free
Amount of items: 3
Items: 
Size: 365534 Color: 7006
Size: 338688 Color: 6011
Size: 295670 Color: 3918

Bin 3317: 109 of cap free
Amount of items: 3
Items: 
Size: 403834 Color: 8102
Size: 331912 Color: 5726
Size: 264146 Color: 1646

Bin 3318: 137 of cap free
Amount of items: 3
Items: 
Size: 341400 Color: 6114
Size: 336746 Color: 5928
Size: 321718 Color: 5310

Bin 3319: 143 of cap free
Amount of items: 3
Items: 
Size: 363146 Color: 6923
Size: 354937 Color: 6652
Size: 281775 Color: 3051

Bin 3320: 148 of cap free
Amount of items: 3
Items: 
Size: 410312 Color: 8276
Size: 327693 Color: 5572
Size: 261848 Color: 1442

Bin 3321: 158 of cap free
Amount of items: 3
Items: 
Size: 386144 Color: 7626
Size: 308881 Color: 4642
Size: 304818 Color: 4441

Bin 3322: 161 of cap free
Amount of items: 3
Items: 
Size: 377803 Color: 7373
Size: 342984 Color: 6174
Size: 279053 Color: 2853

Bin 3323: 199 of cap free
Amount of items: 3
Items: 
Size: 356473 Color: 6696
Size: 356455 Color: 6695
Size: 286874 Color: 3420

Bin 3324: 265 of cap free
Amount of items: 3
Items: 
Size: 370718 Color: 7168
Size: 324632 Color: 5436
Size: 304386 Color: 4414

Bin 3325: 266 of cap free
Amount of items: 3
Items: 
Size: 395908 Color: 7915
Size: 349412 Color: 6426
Size: 254415 Color: 634

Bin 3326: 266 of cap free
Amount of items: 3
Items: 
Size: 352042 Color: 6548
Size: 351851 Color: 6542
Size: 295842 Color: 3937

Bin 3327: 333 of cap free
Amount of items: 3
Items: 
Size: 364149 Color: 6954
Size: 346286 Color: 6299
Size: 289233 Color: 3548

Bin 3328: 334 of cap free
Amount of items: 3
Items: 
Size: 353778 Color: 6620
Size: 348299 Color: 6376
Size: 297590 Color: 4034

Bin 3329: 675 of cap free
Amount of items: 3
Items: 
Size: 372907 Color: 7232
Size: 338928 Color: 6015
Size: 287491 Color: 3455

Bin 3330: 2138 of cap free
Amount of items: 3
Items: 
Size: 364556 Color: 6962
Size: 349230 Color: 6420
Size: 284077 Color: 3224

Bin 3331: 10526 of cap free
Amount of items: 3
Items: 
Size: 404863 Color: 8130
Size: 303222 Color: 4354
Size: 281390 Color: 3017

Bin 3332: 52669 of cap free
Amount of items: 3
Items: 
Size: 394992 Color: 7889
Size: 279811 Color: 2913
Size: 272529 Color: 2337

Bin 3333: 185900 of cap free
Amount of items: 3
Items: 
Size: 272222 Color: 2314
Size: 272191 Color: 2309
Size: 269688 Color: 2104

Bin 3334: 337941 of cap free
Amount of items: 2
Items: 
Size: 399165 Color: 7989
Size: 262895 Color: 1514

Bin 3335: 400359 of cap free
Amount of items: 2
Items: 
Size: 279529 Color: 2883
Size: 320113 Color: 5232

Total size: 3334003334
Total free space: 1000001

