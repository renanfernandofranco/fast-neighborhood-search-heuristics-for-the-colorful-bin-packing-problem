Capicity Bin: 13904
Lower Bound: 198

Bins used: 198
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 12014 Color: 1
Size: 1578 Color: 1
Size: 312 Color: 0

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 6880 Color: 1
Size: 1856 Color: 1
Size: 1696 Color: 1
Size: 1088 Color: 1
Size: 896 Color: 0
Size: 816 Color: 0
Size: 672 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 1
Size: 1272 Color: 1
Size: 240 Color: 0

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 10008 Color: 1
Size: 3256 Color: 1
Size: 304 Color: 0
Size: 176 Color: 0
Size: 160 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10328 Color: 1
Size: 2984 Color: 1
Size: 592 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12228 Color: 1
Size: 1404 Color: 1
Size: 272 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12198 Color: 1
Size: 1422 Color: 1
Size: 284 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11517 Color: 1
Size: 1991 Color: 1
Size: 396 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11674 Color: 1
Size: 2110 Color: 1
Size: 120 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12510 Color: 1
Size: 1162 Color: 1
Size: 232 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 9886 Color: 1
Size: 3350 Color: 1
Size: 668 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10743 Color: 1
Size: 2635 Color: 1
Size: 526 Color: 0

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 12044 Color: 1
Size: 1556 Color: 1
Size: 208 Color: 0
Size: 96 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6964 Color: 1
Size: 6180 Color: 1
Size: 760 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8084 Color: 1
Size: 4852 Color: 1
Size: 968 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11757 Color: 1
Size: 1791 Color: 1
Size: 356 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7811 Color: 1
Size: 5079 Color: 1
Size: 1014 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11958 Color: 1
Size: 1622 Color: 1
Size: 324 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7875 Color: 1
Size: 5025 Color: 1
Size: 1004 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12421 Color: 1
Size: 1237 Color: 1
Size: 246 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7867 Color: 1
Size: 5031 Color: 1
Size: 1006 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11262 Color: 1
Size: 2202 Color: 1
Size: 440 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11506 Color: 1
Size: 2002 Color: 1
Size: 396 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 10370 Color: 1
Size: 2946 Color: 1
Size: 588 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11983 Color: 1
Size: 1601 Color: 1
Size: 320 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 10796 Color: 1
Size: 2596 Color: 1
Size: 512 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 1
Size: 1982 Color: 1
Size: 392 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 1
Size: 4792 Color: 1
Size: 944 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 9581 Color: 1
Size: 3603 Color: 1
Size: 720 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12502 Color: 1
Size: 1214 Color: 1
Size: 188 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 1
Size: 1464 Color: 1
Size: 288 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11290 Color: 1
Size: 2182 Color: 1
Size: 432 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11663 Color: 1
Size: 1869 Color: 1
Size: 372 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6953 Color: 1
Size: 5793 Color: 1
Size: 1158 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12075 Color: 1
Size: 1529 Color: 1
Size: 300 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11464 Color: 1
Size: 2040 Color: 1
Size: 400 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12381 Color: 1
Size: 1271 Color: 1
Size: 252 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 1
Size: 1332 Color: 1
Size: 264 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 9820 Color: 1
Size: 3404 Color: 1
Size: 680 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11105 Color: 1
Size: 2333 Color: 1
Size: 466 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 11773 Color: 1
Size: 1777 Color: 1
Size: 354 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 11933 Color: 1
Size: 1643 Color: 1
Size: 328 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 1
Size: 2394 Color: 1
Size: 476 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 11396 Color: 1
Size: 2092 Color: 1
Size: 416 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12246 Color: 1
Size: 1382 Color: 1
Size: 276 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 11893 Color: 1
Size: 1677 Color: 1
Size: 334 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 8539 Color: 1
Size: 4759 Color: 1
Size: 606 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12134 Color: 1
Size: 1478 Color: 1
Size: 292 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 1
Size: 5782 Color: 1
Size: 1156 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 1
Size: 3364 Color: 1
Size: 672 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 1
Size: 3276 Color: 1
Size: 320 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 8667 Color: 1
Size: 4365 Color: 1
Size: 872 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 1
Size: 1384 Color: 1
Size: 272 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12342 Color: 1
Size: 1302 Color: 1
Size: 260 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 11804 Color: 1
Size: 1756 Color: 1
Size: 344 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 9958 Color: 1
Size: 3290 Color: 1
Size: 656 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12084 Color: 1
Size: 1524 Color: 1
Size: 296 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 10709 Color: 1
Size: 2989 Color: 1
Size: 206 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 7814 Color: 1
Size: 5078 Color: 1
Size: 1012 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 12132 Color: 1
Size: 1484 Color: 1
Size: 288 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 11944 Color: 1
Size: 1640 Color: 1
Size: 320 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 9672 Color: 1
Size: 3704 Color: 1
Size: 528 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 1
Size: 1716 Color: 1
Size: 336 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 12309 Color: 1
Size: 1331 Color: 1
Size: 264 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 12328 Color: 1
Size: 1320 Color: 1
Size: 256 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6144 Color: 1
Size: 5136 Color: 1
Size: 2624 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 10269 Color: 1
Size: 3031 Color: 1
Size: 604 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 9238 Color: 1
Size: 3890 Color: 1
Size: 776 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 10479 Color: 1
Size: 2855 Color: 1
Size: 570 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 10418 Color: 1
Size: 2906 Color: 1
Size: 580 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 11790 Color: 1
Size: 1762 Color: 1
Size: 352 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 12147 Color: 1
Size: 1465 Color: 1
Size: 292 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 11623 Color: 1
Size: 1901 Color: 1
Size: 380 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 11990 Color: 1
Size: 1598 Color: 1
Size: 316 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 12078 Color: 1
Size: 1522 Color: 1
Size: 304 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 1
Size: 1258 Color: 1
Size: 248 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 11880 Color: 1
Size: 1768 Color: 1
Size: 256 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 12390 Color: 1
Size: 1262 Color: 1
Size: 252 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 10762 Color: 1
Size: 2622 Color: 1
Size: 520 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 1
Size: 1378 Color: 1
Size: 272 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 10519 Color: 1
Size: 2821 Color: 1
Size: 564 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 12361 Color: 1
Size: 1287 Color: 1
Size: 256 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 10366 Color: 1
Size: 3122 Color: 1
Size: 416 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 8542 Color: 1
Size: 4470 Color: 1
Size: 892 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 9902 Color: 1
Size: 3730 Color: 1
Size: 272 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 1
Size: 4488 Color: 1
Size: 896 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 1
Size: 5796 Color: 1
Size: 1152 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6954 Color: 1
Size: 5942 Color: 1
Size: 1008 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 10436 Color: 1
Size: 2892 Color: 1
Size: 576 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 1
Size: 3942 Color: 1
Size: 788 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 8606 Color: 1
Size: 4418 Color: 1
Size: 880 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 10600 Color: 1
Size: 2760 Color: 1
Size: 544 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 11735 Color: 1
Size: 1809 Color: 1
Size: 360 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 11076 Color: 1
Size: 2364 Color: 1
Size: 464 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 7916 Color: 1
Size: 4996 Color: 1
Size: 992 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 8824 Color: 1
Size: 4248 Color: 1
Size: 832 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 9206 Color: 1
Size: 3918 Color: 1
Size: 780 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 9064 Color: 1
Size: 4040 Color: 1
Size: 800 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 12484 Color: 1
Size: 1188 Color: 1
Size: 232 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 11998 Color: 1
Size: 1590 Color: 1
Size: 316 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 11749 Color: 1
Size: 1797 Color: 1
Size: 358 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 11613 Color: 1
Size: 1911 Color: 1
Size: 380 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 10904 Color: 1
Size: 2504 Color: 1
Size: 496 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 1
Size: 1432 Color: 1
Size: 272 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 12006 Color: 1
Size: 1582 Color: 1
Size: 316 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 10696 Color: 1
Size: 2680 Color: 1
Size: 528 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 1
Size: 3384 Color: 1
Size: 672 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 9734 Color: 1
Size: 3478 Color: 1
Size: 692 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 11576 Color: 1
Size: 1944 Color: 1
Size: 384 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 12190 Color: 1
Size: 1430 Color: 1
Size: 284 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 12399 Color: 1
Size: 1255 Color: 1
Size: 250 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 1
Size: 1624 Color: 1
Size: 256 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 12356 Color: 1
Size: 1292 Color: 1
Size: 256 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 11066 Color: 1
Size: 2366 Color: 1
Size: 472 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 1
Size: 5784 Color: 1
Size: 1152 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 12281 Color: 1
Size: 1353 Color: 1
Size: 270 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 12334 Color: 1
Size: 1310 Color: 1
Size: 260 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 1
Size: 3064 Color: 1
Size: 608 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 1
Size: 2360 Color: 1
Size: 464 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 11950 Color: 1
Size: 1630 Color: 1
Size: 324 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 1
Size: 1236 Color: 1
Size: 240 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 12025 Color: 1
Size: 1567 Color: 1
Size: 312 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 1
Size: 2032 Color: 1
Size: 400 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 11668 Color: 1
Size: 1868 Color: 1
Size: 368 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 12238 Color: 1
Size: 1390 Color: 1
Size: 276 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 11454 Color: 1
Size: 2042 Color: 1
Size: 408 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 9715 Color: 1
Size: 3491 Color: 1
Size: 698 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 8924 Color: 1
Size: 4156 Color: 1
Size: 824 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 11088 Color: 1
Size: 2176 Color: 1
Size: 640 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 1
Size: 1896 Color: 1
Size: 368 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 11894 Color: 1
Size: 1678 Color: 1
Size: 332 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 1
Size: 1176 Color: 1
Size: 224 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 11474 Color: 1
Size: 2026 Color: 1
Size: 404 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 1
Size: 1832 Color: 1
Size: 352 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 9766 Color: 1
Size: 3818 Color: 1
Size: 320 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 11256 Color: 1
Size: 2216 Color: 1
Size: 432 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 9556 Color: 1
Size: 3628 Color: 1
Size: 720 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 10348 Color: 1
Size: 2964 Color: 1
Size: 592 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 11620 Color: 1
Size: 1908 Color: 1
Size: 376 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 1
Size: 3892 Color: 1
Size: 416 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 11750 Color: 1
Size: 1798 Color: 1
Size: 356 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 12236 Color: 1
Size: 1396 Color: 1
Size: 272 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 10748 Color: 1
Size: 2636 Color: 1
Size: 520 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 1
Size: 4494 Color: 1
Size: 896 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 10814 Color: 1
Size: 2578 Color: 1
Size: 512 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 11229 Color: 1
Size: 2231 Color: 1
Size: 444 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 11348 Color: 1
Size: 2132 Color: 1
Size: 424 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 12437 Color: 1
Size: 1223 Color: 1
Size: 244 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 1
Size: 3816 Color: 1
Size: 752 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 1
Size: 3608 Color: 1
Size: 720 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 10410 Color: 1
Size: 3018 Color: 1
Size: 476 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 12407 Color: 1
Size: 1249 Color: 1
Size: 248 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 1
Size: 5628 Color: 1
Size: 1120 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 6958 Color: 1
Size: 5790 Color: 1
Size: 1156 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 11730 Color: 1
Size: 1814 Color: 1
Size: 360 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 11161 Color: 1
Size: 2455 Color: 1
Size: 288 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 7992 Color: 1
Size: 4936 Color: 1
Size: 976 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 12454 Color: 1
Size: 1210 Color: 1
Size: 240 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 9871 Color: 1
Size: 3361 Color: 1
Size: 672 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 12468 Color: 1
Size: 1204 Color: 1
Size: 232 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 11025 Color: 1
Size: 2401 Color: 1
Size: 478 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 11956 Color: 1
Size: 1628 Color: 1
Size: 320 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 1
Size: 2894 Color: 1
Size: 576 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 10302 Color: 1
Size: 3498 Color: 1
Size: 104 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 9894 Color: 1
Size: 3342 Color: 1
Size: 668 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 8574 Color: 1
Size: 4654 Color: 1
Size: 676 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 12139 Color: 1
Size: 1471 Color: 1
Size: 294 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 11116 Color: 1
Size: 2324 Color: 1
Size: 464 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 9643 Color: 1
Size: 3903 Color: 1
Size: 358 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 7160 Color: 1
Size: 5624 Color: 1
Size: 1120 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 8212 Color: 1
Size: 4748 Color: 1
Size: 944 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 12171 Color: 1
Size: 1445 Color: 1
Size: 288 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 12189 Color: 1
Size: 1431 Color: 1
Size: 284 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 9702 Color: 1
Size: 4034 Color: 1
Size: 168 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 11333 Color: 1
Size: 2143 Color: 1
Size: 428 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 12315 Color: 1
Size: 1325 Color: 1
Size: 264 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 8164 Color: 1
Size: 4788 Color: 1
Size: 952 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 6974 Color: 1
Size: 5778 Color: 1
Size: 1152 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 7320 Color: 1
Size: 5496 Color: 1
Size: 1088 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 12374 Color: 1
Size: 1278 Color: 1
Size: 252 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 8972 Color: 1
Size: 4116 Color: 1
Size: 816 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 9125 Color: 1
Size: 3983 Color: 1
Size: 796 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 12069 Color: 1
Size: 1531 Color: 1
Size: 304 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 12229 Color: 1
Size: 1397 Color: 1
Size: 278 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 12237 Color: 1
Size: 1587 Color: 1
Size: 80 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 9787 Color: 1
Size: 3431 Color: 1
Size: 686 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 11077 Color: 1
Size: 2357 Color: 1
Size: 470 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 7771 Color: 1
Size: 5111 Color: 1
Size: 1022 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 7846 Color: 1
Size: 5050 Color: 1
Size: 1008 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 8579 Color: 1
Size: 4439 Color: 1
Size: 886 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 6957 Color: 1
Size: 5791 Color: 1
Size: 1156 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 7843 Color: 1
Size: 5051 Color: 1
Size: 1010 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 9199 Color: 1
Size: 3921 Color: 1
Size: 784 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 10203 Color: 1
Size: 3085 Color: 1
Size: 616 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 10871 Color: 1
Size: 2529 Color: 1
Size: 504 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 11351 Color: 1
Size: 2129 Color: 1
Size: 424 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 11461 Color: 1
Size: 2037 Color: 1
Size: 406 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 11813 Color: 1
Size: 1743 Color: 1
Size: 348 Color: 0

Total size: 2752992
Total free space: 0

