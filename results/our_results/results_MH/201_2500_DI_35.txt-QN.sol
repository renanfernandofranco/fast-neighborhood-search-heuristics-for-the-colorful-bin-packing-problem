Capicity Bin: 2400
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1188 Color: 138
Size: 332 Color: 88
Size: 296 Color: 82
Size: 192 Color: 65
Size: 144 Color: 54
Size: 136 Color: 50
Size: 112 Color: 43

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1914 Color: 176
Size: 406 Color: 97
Size: 32 Color: 5
Size: 32 Color: 4
Size: 16 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2007 Color: 186
Size: 329 Color: 86
Size: 64 Color: 22

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 184
Size: 338 Color: 89
Size: 64 Color: 21

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 159
Size: 619 Color: 115
Size: 122 Color: 47

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 150
Size: 822 Color: 124
Size: 160 Color: 57

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1958 Color: 180
Size: 382 Color: 94
Size: 60 Color: 20

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 170
Size: 466 Color: 104
Size: 92 Color: 36

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 181
Size: 367 Color: 93
Size: 72 Color: 27

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 144
Size: 974 Color: 131
Size: 192 Color: 64

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 185
Size: 331 Color: 87
Size: 64 Color: 23

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1052 Color: 137
Size: 888 Color: 130
Size: 460 Color: 103

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1873 Color: 172
Size: 441 Color: 101
Size: 86 Color: 34

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2041 Color: 187
Size: 301 Color: 84
Size: 58 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 196
Size: 249 Color: 74
Size: 48 Color: 10

Bin 16: 0 of cap free
Amount of items: 4
Items: 
Size: 1920 Color: 177
Size: 356 Color: 91
Size: 80 Color: 30
Size: 44 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1221 Color: 143
Size: 983 Color: 132
Size: 196 Color: 67

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 178
Size: 401 Color: 96
Size: 78 Color: 29

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1857 Color: 171
Size: 453 Color: 102
Size: 90 Color: 35

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1365 Color: 145
Size: 863 Color: 129
Size: 172 Color: 62

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 197
Size: 242 Color: 73
Size: 48 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 155
Size: 705 Color: 118
Size: 140 Color: 51

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 152
Size: 731 Color: 121
Size: 146 Color: 55

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 200
Size: 238 Color: 72
Size: 16 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 142
Size: 993 Color: 133
Size: 198 Color: 68

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 148
Size: 835 Color: 126
Size: 166 Color: 60

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2070 Color: 192
Size: 298 Color: 83
Size: 32 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 194
Size: 262 Color: 77
Size: 48 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 183
Size: 350 Color: 90
Size: 68 Color: 25

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1547 Color: 154
Size: 711 Color: 119
Size: 142 Color: 52

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 166
Size: 559 Color: 111
Size: 64 Color: 24

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 151
Size: 802 Color: 123
Size: 160 Color: 56

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 165
Size: 533 Color: 108
Size: 106 Color: 40

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2057 Color: 190
Size: 287 Color: 80
Size: 56 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 162
Size: 605 Color: 112
Size: 120 Color: 44

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 201
Size: 206 Color: 70
Size: 40 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 158
Size: 631 Color: 116
Size: 126 Color: 48

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 169
Size: 491 Color: 105
Size: 96 Color: 37

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1889 Color: 175
Size: 427 Color: 98
Size: 84 Color: 31

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 188
Size: 302 Color: 85
Size: 56 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 198
Size: 252 Color: 75
Size: 24 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2095 Color: 195
Size: 255 Color: 76
Size: 50 Color: 13

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 149
Size: 825 Color: 125
Size: 164 Color: 58

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2130 Color: 199
Size: 226 Color: 71
Size: 44 Color: 9

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 174
Size: 434 Color: 100
Size: 84 Color: 32

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 160
Size: 611 Color: 114
Size: 122 Color: 46

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1881 Color: 173
Size: 433 Color: 99
Size: 86 Color: 33

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 156
Size: 790 Color: 122
Size: 40 Color: 7

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1937 Color: 179
Size: 387 Color: 95
Size: 76 Color: 28

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 168
Size: 497 Color: 106
Size: 98 Color: 38

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2049 Color: 189
Size: 293 Color: 81
Size: 58 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 146
Size: 842 Color: 128
Size: 168 Color: 61

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 139
Size: 1025 Color: 136
Size: 174 Color: 63

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 153
Size: 719 Color: 120
Size: 142 Color: 53

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 182
Size: 365 Color: 92
Size: 72 Color: 26

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 167
Size: 506 Color: 107
Size: 100 Color: 39

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1205 Color: 141
Size: 997 Color: 134
Size: 198 Color: 69

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1395 Color: 147
Size: 839 Color: 127
Size: 166 Color: 59

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 140
Size: 1002 Color: 135
Size: 196 Color: 66

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 191
Size: 283 Color: 79
Size: 56 Color: 17

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 161
Size: 610 Color: 113
Size: 120 Color: 45

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 157
Size: 678 Color: 117
Size: 132 Color: 49

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 193
Size: 269 Color: 78
Size: 52 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 163
Size: 554 Color: 110
Size: 108 Color: 41

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 164
Size: 549 Color: 109
Size: 108 Color: 42

Total size: 156000
Total free space: 0

