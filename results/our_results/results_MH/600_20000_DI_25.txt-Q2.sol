Capicity Bin: 16000
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 11
Items: 
Size: 8004 Color: 0
Size: 984 Color: 0
Size: 976 Color: 0
Size: 942 Color: 1
Size: 936 Color: 0
Size: 912 Color: 0
Size: 908 Color: 1
Size: 850 Color: 1
Size: 788 Color: 1
Size: 428 Color: 1
Size: 272 Color: 0

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 8002 Color: 1
Size: 1192 Color: 0
Size: 1136 Color: 1
Size: 1080 Color: 0
Size: 1058 Color: 0
Size: 994 Color: 1
Size: 988 Color: 1
Size: 898 Color: 0
Size: 652 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8936 Color: 0
Size: 6536 Color: 1
Size: 528 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 1
Size: 4538 Color: 1
Size: 370 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11624 Color: 0
Size: 2684 Color: 1
Size: 1692 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12078 Color: 1
Size: 3432 Color: 1
Size: 490 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 0
Size: 2968 Color: 0
Size: 864 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 1
Size: 3056 Color: 0
Size: 312 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12738 Color: 0
Size: 2444 Color: 0
Size: 818 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 2744 Color: 1
Size: 352 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 1
Size: 2488 Color: 1
Size: 536 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13253 Color: 0
Size: 2291 Color: 0
Size: 456 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13300 Color: 1
Size: 1368 Color: 0
Size: 1332 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 1
Size: 2302 Color: 0
Size: 396 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13308 Color: 1
Size: 1548 Color: 0
Size: 1144 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 0
Size: 1370 Color: 1
Size: 1116 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 0
Size: 1766 Color: 0
Size: 616 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13670 Color: 1
Size: 1394 Color: 1
Size: 936 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13954 Color: 1
Size: 1302 Color: 0
Size: 744 Color: 0

Bin 20: 0 of cap free
Amount of items: 4
Items: 
Size: 14068 Color: 0
Size: 1772 Color: 1
Size: 124 Color: 1
Size: 36 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14214 Color: 1
Size: 1444 Color: 0
Size: 342 Color: 0

Bin 22: 1 of cap free
Amount of items: 13
Items: 
Size: 8003 Color: 0
Size: 816 Color: 0
Size: 784 Color: 0
Size: 776 Color: 0
Size: 764 Color: 0
Size: 736 Color: 1
Size: 736 Color: 0
Size: 732 Color: 1
Size: 720 Color: 1
Size: 696 Color: 1
Size: 628 Color: 1
Size: 320 Color: 1
Size: 288 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 8408 Color: 0
Size: 6503 Color: 1
Size: 1088 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 8814 Color: 0
Size: 6665 Color: 0
Size: 520 Color: 1

Bin 25: 1 of cap free
Amount of items: 5
Items: 
Size: 9972 Color: 1
Size: 1816 Color: 0
Size: 1807 Color: 0
Size: 1404 Color: 0
Size: 1000 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 9986 Color: 0
Size: 5725 Color: 1
Size: 288 Color: 0

Bin 27: 1 of cap free
Amount of items: 5
Items: 
Size: 10340 Color: 0
Size: 2715 Color: 1
Size: 2356 Color: 0
Size: 296 Color: 0
Size: 292 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 1
Size: 4497 Color: 1
Size: 710 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 12101 Color: 1
Size: 2970 Color: 0
Size: 928 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 12187 Color: 1
Size: 2964 Color: 0
Size: 848 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 13101 Color: 0
Size: 1512 Color: 0
Size: 1386 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 13117 Color: 1
Size: 1582 Color: 0
Size: 1300 Color: 1

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 13492 Color: 0
Size: 2147 Color: 0
Size: 360 Color: 1

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 13793 Color: 1
Size: 2206 Color: 0

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 14140 Color: 1
Size: 1859 Color: 0

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 10504 Color: 1
Size: 5494 Color: 0

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 0
Size: 4550 Color: 0
Size: 352 Color: 1

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 12338 Color: 0
Size: 2724 Color: 1
Size: 936 Color: 0

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 12514 Color: 0
Size: 3484 Color: 1

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 12698 Color: 0
Size: 3300 Color: 1

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 12850 Color: 0
Size: 1684 Color: 0
Size: 1464 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 12986 Color: 0
Size: 2708 Color: 1
Size: 304 Color: 0

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 1
Size: 2452 Color: 0
Size: 580 Color: 1

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 13365 Color: 1
Size: 2633 Color: 0

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 13944 Color: 1
Size: 2054 Color: 0

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 14050 Color: 1
Size: 1948 Color: 0

Bin 47: 3 of cap free
Amount of items: 7
Items: 
Size: 8012 Color: 0
Size: 1604 Color: 0
Size: 1573 Color: 0
Size: 1328 Color: 1
Size: 1328 Color: 1
Size: 1152 Color: 0
Size: 1000 Color: 1

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 10392 Color: 1
Size: 4981 Color: 1
Size: 624 Color: 0

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 12685 Color: 0
Size: 3084 Color: 0
Size: 228 Color: 1

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 13053 Color: 1
Size: 2252 Color: 0
Size: 692 Color: 0

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 1
Size: 2069 Color: 0

Bin 52: 3 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 1
Size: 1877 Color: 0

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 8197 Color: 0
Size: 7459 Color: 0
Size: 340 Color: 1

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 10356 Color: 1
Size: 5048 Color: 1
Size: 592 Color: 0

Bin 55: 4 of cap free
Amount of items: 2
Items: 
Size: 11288 Color: 0
Size: 4708 Color: 1

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 11652 Color: 0
Size: 3928 Color: 1
Size: 416 Color: 1

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 12102 Color: 1
Size: 3894 Color: 0

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 13060 Color: 0
Size: 2936 Color: 1

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 13274 Color: 1
Size: 2722 Color: 0

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 13354 Color: 0
Size: 2642 Color: 1

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 13370 Color: 1
Size: 2626 Color: 0

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 14066 Color: 1
Size: 1930 Color: 0

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 14324 Color: 0
Size: 1672 Color: 1

Bin 64: 5 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 0
Size: 3555 Color: 1

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 1
Size: 3251 Color: 0

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 0
Size: 1356 Color: 1
Size: 992 Color: 0

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 13833 Color: 0
Size: 2162 Color: 1

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 14083 Color: 1
Size: 1912 Color: 0

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 14125 Color: 1
Size: 1870 Color: 0

Bin 70: 6 of cap free
Amount of items: 3
Items: 
Size: 8010 Color: 1
Size: 7656 Color: 1
Size: 328 Color: 0

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 12230 Color: 0
Size: 3764 Color: 1

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 13624 Color: 1
Size: 2370 Color: 0

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 1
Size: 2274 Color: 0

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 14148 Color: 1
Size: 1846 Color: 0

Bin 75: 7 of cap free
Amount of items: 3
Items: 
Size: 9502 Color: 1
Size: 6099 Color: 0
Size: 392 Color: 1

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 10025 Color: 0
Size: 5968 Color: 1

Bin 77: 7 of cap free
Amount of items: 3
Items: 
Size: 11081 Color: 0
Size: 3446 Color: 1
Size: 1466 Color: 0

Bin 78: 7 of cap free
Amount of items: 2
Items: 
Size: 12743 Color: 1
Size: 3250 Color: 0

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 9016 Color: 0
Size: 6976 Color: 1

Bin 80: 8 of cap free
Amount of items: 3
Items: 
Size: 10542 Color: 0
Size: 4938 Color: 1
Size: 512 Color: 0

Bin 81: 8 of cap free
Amount of items: 3
Items: 
Size: 11534 Color: 1
Size: 4226 Color: 0
Size: 232 Color: 1

Bin 82: 8 of cap free
Amount of items: 3
Items: 
Size: 11580 Color: 0
Size: 4092 Color: 1
Size: 320 Color: 1

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 0
Size: 2763 Color: 1
Size: 224 Color: 0

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 0
Size: 2754 Color: 1

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 13900 Color: 1
Size: 2092 Color: 0

Bin 86: 9 of cap free
Amount of items: 11
Items: 
Size: 8001 Color: 1
Size: 904 Color: 0
Size: 844 Color: 0
Size: 824 Color: 0
Size: 816 Color: 0
Size: 800 Color: 0
Size: 788 Color: 1
Size: 782 Color: 1
Size: 760 Color: 1
Size: 752 Color: 1
Size: 720 Color: 1

Bin 87: 9 of cap free
Amount of items: 2
Items: 
Size: 13980 Color: 1
Size: 2011 Color: 0

Bin 88: 10 of cap free
Amount of items: 3
Items: 
Size: 9432 Color: 0
Size: 6070 Color: 1
Size: 488 Color: 0

Bin 89: 10 of cap free
Amount of items: 3
Items: 
Size: 10341 Color: 0
Size: 5297 Color: 1
Size: 352 Color: 0

Bin 90: 10 of cap free
Amount of items: 2
Items: 
Size: 13758 Color: 0
Size: 2232 Color: 1

Bin 91: 11 of cap free
Amount of items: 2
Items: 
Size: 10897 Color: 0
Size: 5092 Color: 1

Bin 92: 11 of cap free
Amount of items: 2
Items: 
Size: 12890 Color: 0
Size: 3099 Color: 1

Bin 93: 11 of cap free
Amount of items: 2
Items: 
Size: 13997 Color: 0
Size: 1992 Color: 1

Bin 94: 11 of cap free
Amount of items: 4
Items: 
Size: 14024 Color: 0
Size: 1713 Color: 1
Size: 140 Color: 1
Size: 112 Color: 0

Bin 95: 12 of cap free
Amount of items: 3
Items: 
Size: 8830 Color: 0
Size: 6660 Color: 1
Size: 498 Color: 1

Bin 96: 12 of cap free
Amount of items: 3
Items: 
Size: 11512 Color: 0
Size: 4088 Color: 1
Size: 388 Color: 0

Bin 97: 12 of cap free
Amount of items: 3
Items: 
Size: 11668 Color: 0
Size: 3752 Color: 1
Size: 568 Color: 0

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 11735 Color: 1
Size: 4253 Color: 0

Bin 99: 12 of cap free
Amount of items: 2
Items: 
Size: 13538 Color: 0
Size: 2450 Color: 1

Bin 100: 12 of cap free
Amount of items: 4
Items: 
Size: 13882 Color: 1
Size: 1942 Color: 0
Size: 112 Color: 1
Size: 52 Color: 0

Bin 101: 12 of cap free
Amount of items: 2
Items: 
Size: 13972 Color: 0
Size: 2016 Color: 1

Bin 102: 12 of cap free
Amount of items: 2
Items: 
Size: 14268 Color: 1
Size: 1720 Color: 0

Bin 103: 12 of cap free
Amount of items: 2
Items: 
Size: 14338 Color: 1
Size: 1650 Color: 0

Bin 104: 13 of cap free
Amount of items: 3
Items: 
Size: 11828 Color: 1
Size: 3823 Color: 1
Size: 336 Color: 0

Bin 105: 13 of cap free
Amount of items: 2
Items: 
Size: 13587 Color: 1
Size: 2400 Color: 0

Bin 106: 14 of cap free
Amount of items: 2
Items: 
Size: 11036 Color: 0
Size: 4950 Color: 1

Bin 107: 14 of cap free
Amount of items: 2
Items: 
Size: 13668 Color: 1
Size: 2318 Color: 0

Bin 108: 14 of cap free
Amount of items: 2
Items: 
Size: 14360 Color: 1
Size: 1626 Color: 0

Bin 109: 15 of cap free
Amount of items: 3
Items: 
Size: 12740 Color: 0
Size: 3045 Color: 1
Size: 200 Color: 1

Bin 110: 16 of cap free
Amount of items: 2
Items: 
Size: 12022 Color: 1
Size: 3962 Color: 0

Bin 111: 16 of cap free
Amount of items: 2
Items: 
Size: 12300 Color: 0
Size: 3684 Color: 1

Bin 112: 16 of cap free
Amount of items: 2
Items: 
Size: 14248 Color: 1
Size: 1736 Color: 0

Bin 113: 16 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 0
Size: 1784 Color: 1

Bin 114: 16 of cap free
Amount of items: 2
Items: 
Size: 14346 Color: 0
Size: 1638 Color: 1

Bin 115: 17 of cap free
Amount of items: 2
Items: 
Size: 11266 Color: 1
Size: 4717 Color: 0

Bin 116: 17 of cap free
Amount of items: 2
Items: 
Size: 13909 Color: 0
Size: 2074 Color: 1

Bin 117: 17 of cap free
Amount of items: 2
Items: 
Size: 14022 Color: 1
Size: 1961 Color: 0

Bin 118: 17 of cap free
Amount of items: 2
Items: 
Size: 14242 Color: 0
Size: 1741 Color: 1

Bin 119: 18 of cap free
Amount of items: 2
Items: 
Size: 11330 Color: 1
Size: 4652 Color: 0

Bin 120: 18 of cap free
Amount of items: 2
Items: 
Size: 12260 Color: 1
Size: 3722 Color: 0

Bin 121: 18 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 1
Size: 3270 Color: 0

Bin 122: 18 of cap free
Amount of items: 2
Items: 
Size: 13076 Color: 0
Size: 2906 Color: 1

Bin 123: 19 of cap free
Amount of items: 2
Items: 
Size: 13564 Color: 0
Size: 2417 Color: 1

Bin 124: 19 of cap free
Amount of items: 2
Items: 
Size: 13945 Color: 0
Size: 2036 Color: 1

Bin 125: 20 of cap free
Amount of items: 2
Items: 
Size: 13788 Color: 1
Size: 2192 Color: 0

Bin 126: 21 of cap free
Amount of items: 2
Items: 
Size: 12800 Color: 0
Size: 3179 Color: 1

Bin 127: 22 of cap free
Amount of items: 3
Items: 
Size: 11413 Color: 0
Size: 3469 Color: 1
Size: 1096 Color: 0

Bin 128: 22 of cap free
Amount of items: 2
Items: 
Size: 13448 Color: 0
Size: 2530 Color: 1

Bin 129: 23 of cap free
Amount of items: 2
Items: 
Size: 13777 Color: 0
Size: 2200 Color: 1

Bin 130: 24 of cap free
Amount of items: 2
Items: 
Size: 8040 Color: 1
Size: 7936 Color: 0

Bin 131: 24 of cap free
Amount of items: 3
Items: 
Size: 9988 Color: 1
Size: 5404 Color: 1
Size: 584 Color: 0

Bin 132: 24 of cap free
Amount of items: 2
Items: 
Size: 10558 Color: 1
Size: 5418 Color: 0

Bin 133: 24 of cap free
Amount of items: 2
Items: 
Size: 12834 Color: 1
Size: 3142 Color: 0

Bin 134: 24 of cap free
Amount of items: 2
Items: 
Size: 14118 Color: 1
Size: 1858 Color: 0

Bin 135: 25 of cap free
Amount of items: 4
Items: 
Size: 11586 Color: 0
Size: 4101 Color: 1
Size: 192 Color: 1
Size: 96 Color: 0

Bin 136: 25 of cap free
Amount of items: 2
Items: 
Size: 13062 Color: 1
Size: 2913 Color: 0

Bin 137: 26 of cap free
Amount of items: 2
Items: 
Size: 11250 Color: 0
Size: 4724 Color: 1

Bin 138: 28 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 0
Size: 2584 Color: 1

Bin 139: 29 of cap free
Amount of items: 2
Items: 
Size: 13774 Color: 0
Size: 2197 Color: 1

Bin 140: 30 of cap free
Amount of items: 2
Items: 
Size: 14113 Color: 0
Size: 1857 Color: 1

Bin 141: 30 of cap free
Amount of items: 2
Items: 
Size: 14358 Color: 1
Size: 1612 Color: 0

Bin 142: 31 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 1
Size: 2457 Color: 0
Size: 176 Color: 0

Bin 143: 32 of cap free
Amount of items: 3
Items: 
Size: 11839 Color: 1
Size: 3921 Color: 0
Size: 208 Color: 0

Bin 144: 32 of cap free
Amount of items: 2
Items: 
Size: 13832 Color: 0
Size: 2136 Color: 1

Bin 145: 36 of cap free
Amount of items: 2
Items: 
Size: 10825 Color: 0
Size: 5139 Color: 1

Bin 146: 36 of cap free
Amount of items: 2
Items: 
Size: 12756 Color: 0
Size: 3208 Color: 1

Bin 147: 38 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 1
Size: 3682 Color: 0

Bin 148: 38 of cap free
Amount of items: 2
Items: 
Size: 12348 Color: 1
Size: 3614 Color: 0

Bin 149: 40 of cap free
Amount of items: 2
Items: 
Size: 10946 Color: 1
Size: 5014 Color: 0

Bin 150: 41 of cap free
Amount of items: 2
Items: 
Size: 12847 Color: 0
Size: 3112 Color: 1

Bin 151: 41 of cap free
Amount of items: 2
Items: 
Size: 14106 Color: 1
Size: 1853 Color: 0

Bin 152: 47 of cap free
Amount of items: 2
Items: 
Size: 13773 Color: 1
Size: 2180 Color: 0

Bin 153: 48 of cap free
Amount of items: 6
Items: 
Size: 8185 Color: 1
Size: 1718 Color: 0
Size: 1671 Color: 0
Size: 1556 Color: 1
Size: 1490 Color: 1
Size: 1332 Color: 0

Bin 154: 50 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 0
Size: 1570 Color: 1

Bin 155: 51 of cap free
Amount of items: 2
Items: 
Size: 12283 Color: 0
Size: 3666 Color: 1

Bin 156: 54 of cap free
Amount of items: 2
Items: 
Size: 11602 Color: 1
Size: 4344 Color: 0

Bin 157: 56 of cap free
Amount of items: 2
Items: 
Size: 11780 Color: 0
Size: 4164 Color: 1

Bin 158: 58 of cap free
Amount of items: 2
Items: 
Size: 10930 Color: 1
Size: 5012 Color: 0

Bin 159: 58 of cap free
Amount of items: 2
Items: 
Size: 13398 Color: 1
Size: 2544 Color: 0

Bin 160: 59 of cap free
Amount of items: 2
Items: 
Size: 10605 Color: 1
Size: 5336 Color: 0

Bin 161: 60 of cap free
Amount of items: 2
Items: 
Size: 11896 Color: 1
Size: 4044 Color: 0

Bin 162: 61 of cap free
Amount of items: 2
Items: 
Size: 13425 Color: 0
Size: 2514 Color: 1

Bin 163: 62 of cap free
Amount of items: 2
Items: 
Size: 9960 Color: 0
Size: 5978 Color: 1

Bin 164: 70 of cap free
Amount of items: 2
Items: 
Size: 13942 Color: 1
Size: 1988 Color: 0

Bin 165: 77 of cap free
Amount of items: 2
Items: 
Size: 9410 Color: 1
Size: 6513 Color: 0

Bin 166: 78 of cap free
Amount of items: 2
Items: 
Size: 13519 Color: 0
Size: 2403 Color: 1

Bin 167: 80 of cap free
Amount of items: 2
Items: 
Size: 14076 Color: 0
Size: 1844 Color: 1

Bin 168: 82 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 1
Size: 3130 Color: 0

Bin 169: 92 of cap free
Amount of items: 2
Items: 
Size: 13714 Color: 0
Size: 2194 Color: 1

Bin 170: 96 of cap free
Amount of items: 2
Items: 
Size: 13749 Color: 1
Size: 2155 Color: 0

Bin 171: 98 of cap free
Amount of items: 2
Items: 
Size: 12246 Color: 1
Size: 3656 Color: 0

Bin 172: 100 of cap free
Amount of items: 2
Items: 
Size: 10068 Color: 0
Size: 5832 Color: 1

Bin 173: 100 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 1
Size: 5480 Color: 0

Bin 174: 104 of cap free
Amount of items: 2
Items: 
Size: 11666 Color: 1
Size: 4230 Color: 0

Bin 175: 107 of cap free
Amount of items: 2
Items: 
Size: 14330 Color: 0
Size: 1563 Color: 1

Bin 176: 108 of cap free
Amount of items: 2
Items: 
Size: 12574 Color: 0
Size: 3318 Color: 1

Bin 177: 114 of cap free
Amount of items: 2
Items: 
Size: 13158 Color: 1
Size: 2728 Color: 0

Bin 178: 119 of cap free
Amount of items: 2
Items: 
Size: 11297 Color: 0
Size: 4584 Color: 1

Bin 179: 122 of cap free
Amount of items: 2
Items: 
Size: 12444 Color: 1
Size: 3434 Color: 0

Bin 180: 126 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 0
Size: 2858 Color: 1

Bin 181: 127 of cap free
Amount of items: 2
Items: 
Size: 9545 Color: 1
Size: 6328 Color: 0

Bin 182: 129 of cap free
Amount of items: 2
Items: 
Size: 12347 Color: 0
Size: 3524 Color: 1

Bin 183: 135 of cap free
Amount of items: 2
Items: 
Size: 13368 Color: 1
Size: 2497 Color: 0

Bin 184: 138 of cap free
Amount of items: 2
Items: 
Size: 13876 Color: 0
Size: 1986 Color: 1

Bin 185: 144 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 0
Size: 2170 Color: 1

Bin 186: 161 of cap free
Amount of items: 2
Items: 
Size: 12611 Color: 1
Size: 3228 Color: 0

Bin 187: 166 of cap free
Amount of items: 2
Items: 
Size: 10062 Color: 0
Size: 5772 Color: 1

Bin 188: 168 of cap free
Amount of items: 2
Items: 
Size: 9164 Color: 0
Size: 6668 Color: 1

Bin 189: 188 of cap free
Amount of items: 2
Items: 
Size: 11866 Color: 1
Size: 3946 Color: 0

Bin 190: 193 of cap free
Amount of items: 2
Items: 
Size: 9141 Color: 0
Size: 6666 Color: 1

Bin 191: 198 of cap free
Amount of items: 37
Items: 
Size: 524 Color: 0
Size: 512 Color: 0
Size: 504 Color: 0
Size: 500 Color: 0
Size: 482 Color: 0
Size: 480 Color: 1
Size: 480 Color: 0
Size: 476 Color: 1
Size: 472 Color: 1
Size: 460 Color: 1
Size: 452 Color: 1
Size: 448 Color: 0
Size: 440 Color: 1
Size: 438 Color: 0
Size: 436 Color: 0
Size: 432 Color: 1
Size: 432 Color: 0
Size: 432 Color: 0
Size: 432 Color: 0
Size: 432 Color: 0
Size: 416 Color: 1
Size: 412 Color: 1
Size: 412 Color: 1
Size: 408 Color: 1
Size: 402 Color: 1
Size: 400 Color: 1
Size: 384 Color: 1
Size: 384 Color: 0
Size: 384 Color: 0
Size: 380 Color: 1
Size: 374 Color: 0
Size: 372 Color: 1
Size: 370 Color: 1
Size: 368 Color: 1
Size: 368 Color: 0
Size: 368 Color: 0
Size: 336 Color: 0

Bin 192: 205 of cap free
Amount of items: 2
Items: 
Size: 9131 Color: 0
Size: 6664 Color: 1

Bin 193: 206 of cap free
Amount of items: 27
Items: 
Size: 732 Color: 0
Size: 688 Color: 0
Size: 688 Color: 0
Size: 672 Color: 0
Size: 660 Color: 0
Size: 648 Color: 0
Size: 648 Color: 0
Size: 634 Color: 0
Size: 624 Color: 1
Size: 618 Color: 1
Size: 608 Color: 1
Size: 608 Color: 1
Size: 592 Color: 1
Size: 552 Color: 1
Size: 548 Color: 0
Size: 544 Color: 0
Size: 542 Color: 0
Size: 540 Color: 1
Size: 540 Color: 0
Size: 536 Color: 0
Size: 528 Color: 1
Size: 528 Color: 1
Size: 528 Color: 0
Size: 524 Color: 1
Size: 496 Color: 1
Size: 488 Color: 1
Size: 480 Color: 1

Bin 194: 208 of cap free
Amount of items: 2
Items: 
Size: 9084 Color: 1
Size: 6708 Color: 0

Bin 195: 222 of cap free
Amount of items: 2
Items: 
Size: 9645 Color: 0
Size: 6133 Color: 1

Bin 196: 222 of cap free
Amount of items: 2
Items: 
Size: 10078 Color: 1
Size: 5700 Color: 0

Bin 197: 258 of cap free
Amount of items: 7
Items: 
Size: 8008 Color: 1
Size: 1382 Color: 0
Size: 1328 Color: 0
Size: 1328 Color: 0
Size: 1280 Color: 0
Size: 1264 Color: 1
Size: 1152 Color: 1

Bin 198: 262 of cap free
Amount of items: 2
Items: 
Size: 9076 Color: 0
Size: 6662 Color: 1

Bin 199: 8764 of cap free
Amount of items: 24
Items: 
Size: 352 Color: 1
Size: 336 Color: 1
Size: 336 Color: 1
Size: 336 Color: 0
Size: 332 Color: 1
Size: 324 Color: 0
Size: 322 Color: 0
Size: 314 Color: 1
Size: 312 Color: 0
Size: 312 Color: 0
Size: 304 Color: 1
Size: 304 Color: 0
Size: 304 Color: 0
Size: 296 Color: 0
Size: 288 Color: 1
Size: 288 Color: 1
Size: 276 Color: 0
Size: 276 Color: 0
Size: 272 Color: 1
Size: 272 Color: 1
Size: 272 Color: 1
Size: 272 Color: 0
Size: 272 Color: 0
Size: 264 Color: 1

Total size: 3168000
Total free space: 16000

