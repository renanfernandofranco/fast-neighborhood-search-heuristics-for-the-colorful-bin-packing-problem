Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 2
Size: 261 Color: 4
Size: 250 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 274 Color: 2
Size: 352 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 254 Color: 1
Size: 253 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 329 Color: 0
Size: 267 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 366 Color: 4
Size: 252 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 353 Color: 1
Size: 258 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 367 Color: 1
Size: 263 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 333 Color: 0
Size: 289 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 3
Size: 270 Color: 0
Size: 251 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 3
Size: 300 Color: 1
Size: 297 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 330 Color: 1
Size: 299 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 299 Color: 2
Size: 276 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1
Size: 343 Color: 0
Size: 307 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 4
Size: 259 Color: 2
Size: 252 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 334 Color: 0
Size: 290 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 335 Color: 3
Size: 285 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 3
Size: 315 Color: 0
Size: 301 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 4
Size: 268 Color: 1
Size: 260 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 3
Size: 299 Color: 4
Size: 270 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 3
Size: 283 Color: 0
Size: 250 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 2
Size: 358 Color: 0
Size: 257 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 4
Size: 276 Color: 1
Size: 261 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 324 Color: 2
Size: 271 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 294 Color: 4
Size: 288 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 3
Size: 251 Color: 0
Size: 250 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 0
Size: 263 Color: 1
Size: 259 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 329 Color: 3
Size: 268 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 320 Color: 1
Size: 304 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 314 Color: 3
Size: 312 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 4
Size: 283 Color: 3
Size: 254 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 4
Size: 343 Color: 1
Size: 301 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 4
Size: 295 Color: 1
Size: 255 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 3
Size: 304 Color: 2
Size: 258 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 3
Size: 365 Color: 2
Size: 251 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 3
Size: 315 Color: 2
Size: 281 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 363 Color: 0
Size: 260 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 328 Color: 1
Size: 265 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 3
Size: 260 Color: 1
Size: 252 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 320 Color: 3
Size: 280 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 263 Color: 3
Size: 253 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 357 Color: 1
Size: 262 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 4
Size: 268 Color: 0
Size: 252 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 279 Color: 3
Size: 258 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 4
Size: 293 Color: 0
Size: 274 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 305 Color: 1
Size: 255 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 362 Color: 1
Size: 273 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 312 Color: 4
Size: 300 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 2
Size: 282 Color: 3
Size: 273 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 350 Color: 2
Size: 269 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 3
Size: 275 Color: 3
Size: 259 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 323 Color: 2
Size: 278 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 4
Size: 326 Color: 1
Size: 252 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 289 Color: 0
Size: 273 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 4
Size: 293 Color: 1
Size: 264 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 1
Size: 336 Color: 0
Size: 319 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 276 Color: 1
Size: 253 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 313 Color: 0
Size: 308 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 307 Color: 1
Size: 274 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 3
Size: 295 Color: 4
Size: 252 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 3
Size: 263 Color: 4
Size: 250 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 300 Color: 3
Size: 275 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 317 Color: 4
Size: 270 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 3
Size: 330 Color: 1
Size: 310 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 2
Size: 279 Color: 3
Size: 260 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 2
Size: 321 Color: 4
Size: 285 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 338 Color: 4
Size: 276 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 3
Size: 335 Color: 1
Size: 254 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 0
Size: 294 Color: 3
Size: 273 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 3
Size: 352 Color: 4
Size: 287 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 353 Color: 1
Size: 265 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 310 Color: 2
Size: 255 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 297 Color: 0
Size: 283 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 0
Size: 313 Color: 4
Size: 262 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 312 Color: 0
Size: 276 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 257 Color: 0
Size: 252 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 3
Size: 346 Color: 4
Size: 291 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 2
Size: 309 Color: 0
Size: 297 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 2
Size: 252 Color: 2
Size: 250 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 307 Color: 4
Size: 279 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 2
Size: 286 Color: 2
Size: 267 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 2
Size: 297 Color: 0
Size: 259 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 259 Color: 4
Size: 252 Color: 3

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 4
Size: 342 Color: 0
Size: 279 Color: 3

Total size: 83000
Total free space: 0

