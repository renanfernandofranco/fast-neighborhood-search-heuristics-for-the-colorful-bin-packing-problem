Capicity Bin: 1864
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1218 Color: 155
Size: 542 Color: 119
Size: 68 Color: 36
Size: 36 Color: 12

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1498 Color: 177
Size: 274 Color: 91
Size: 48 Color: 23
Size: 44 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1538 Color: 181
Size: 306 Color: 96
Size: 20 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 193
Size: 194 Color: 77
Size: 36 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 161
Size: 446 Color: 114
Size: 88 Color: 48

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 187
Size: 202 Color: 79
Size: 68 Color: 35

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 166
Size: 398 Color: 108
Size: 76 Color: 42

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 156
Size: 529 Color: 118
Size: 104 Color: 53

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 199
Size: 194 Color: 76
Size: 12 Color: 3

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 1292 Color: 160
Size: 288 Color: 92
Size: 152 Color: 66
Size: 132 Color: 60

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 752 Color: 132
Size: 632 Color: 126
Size: 428 Color: 111
Size: 52 Color: 25

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1447 Color: 173
Size: 349 Color: 100
Size: 68 Color: 37

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 934 Color: 139
Size: 778 Color: 136
Size: 144 Color: 63
Size: 8 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 152
Size: 602 Color: 121
Size: 116 Color: 55

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1519 Color: 180
Size: 289 Color: 93
Size: 56 Color: 27

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 174
Size: 330 Color: 99
Size: 64 Color: 33

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 185
Size: 246 Color: 87
Size: 48 Color: 24

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1286 Color: 159
Size: 482 Color: 115
Size: 96 Color: 49

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 146
Size: 694 Color: 128
Size: 136 Color: 61

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 198
Size: 182 Color: 72
Size: 32 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1636 Color: 194
Size: 196 Color: 78
Size: 32 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 191
Size: 226 Color: 85
Size: 12 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 167
Size: 382 Color: 106
Size: 76 Color: 43

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 147
Size: 738 Color: 131
Size: 60 Color: 30

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1551 Color: 182
Size: 261 Color: 90
Size: 52 Color: 26

Bin 26: 0 of cap free
Amount of items: 4
Items: 
Size: 1168 Color: 153
Size: 388 Color: 107
Size: 204 Color: 80
Size: 104 Color: 52

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 175
Size: 314 Color: 98
Size: 60 Color: 31

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1566 Color: 184
Size: 250 Color: 88
Size: 48 Color: 22

Bin 29: 0 of cap free
Amount of items: 4
Items: 
Size: 982 Color: 144
Size: 666 Color: 127
Size: 208 Color: 82
Size: 8 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 164
Size: 425 Color: 110
Size: 84 Color: 45

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1565 Color: 183
Size: 251 Color: 89
Size: 48 Color: 21

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1358 Color: 165
Size: 422 Color: 109
Size: 84 Color: 44

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1257 Color: 158
Size: 507 Color: 116
Size: 100 Color: 50

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 200
Size: 163 Color: 70
Size: 32 Color: 7

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 188
Size: 221 Color: 84
Size: 44 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 170
Size: 370 Color: 103
Size: 72 Color: 40

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 178
Size: 302 Color: 95
Size: 56 Color: 28

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1637 Color: 195
Size: 191 Color: 74
Size: 36 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 989 Color: 145
Size: 731 Color: 129
Size: 144 Color: 62

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1642 Color: 196
Size: 186 Color: 73
Size: 36 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 169
Size: 380 Color: 105
Size: 68 Color: 34

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 937 Color: 140
Size: 773 Color: 134
Size: 154 Color: 67

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 981 Color: 143
Size: 737 Color: 130
Size: 146 Color: 64

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1347 Color: 163
Size: 431 Color: 112
Size: 86 Color: 46

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 933 Color: 138
Size: 777 Color: 135
Size: 154 Color: 68

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 176
Size: 311 Color: 97
Size: 62 Color: 32

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1123 Color: 149
Size: 619 Color: 124
Size: 122 Color: 57

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1443 Color: 172
Size: 351 Color: 101
Size: 70 Color: 38

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1511 Color: 179
Size: 295 Color: 94
Size: 58 Color: 29

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 186
Size: 239 Color: 86
Size: 46 Color: 20

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1135 Color: 151
Size: 609 Color: 122
Size: 120 Color: 56

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1249 Color: 157
Size: 513 Color: 117
Size: 102 Color: 51

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 197
Size: 181 Color: 71
Size: 34 Color: 10

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 154
Size: 554 Color: 120
Size: 108 Color: 54

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1109 Color: 148
Size: 631 Color: 125
Size: 124 Color: 59

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 961 Color: 142
Size: 753 Color: 133
Size: 150 Color: 65

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 168
Size: 375 Color: 104
Size: 74 Color: 41

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 941 Color: 141
Size: 879 Color: 137
Size: 44 Color: 18

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1131 Color: 150
Size: 611 Color: 123
Size: 122 Color: 58

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 201
Size: 157 Color: 69
Size: 30 Color: 5

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1619 Color: 189
Size: 205 Color: 81
Size: 40 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1621 Color: 190
Size: 213 Color: 83
Size: 30 Color: 6

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1333 Color: 162
Size: 443 Color: 113
Size: 88 Color: 47

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1435 Color: 171
Size: 359 Color: 102
Size: 70 Color: 39

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 192
Size: 193 Color: 75
Size: 38 Color: 15

Total size: 121160
Total free space: 0

