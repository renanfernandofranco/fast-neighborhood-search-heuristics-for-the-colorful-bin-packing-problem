Capicity Bin: 1976
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 1368 Color: 1
Size: 408 Color: 1
Size: 68 Color: 5
Size: 48 Color: 2
Size: 44 Color: 5
Size: 40 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1236 Color: 12
Size: 456 Color: 2
Size: 216 Color: 12
Size: 68 Color: 14

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1667 Color: 3
Size: 259 Color: 0
Size: 42 Color: 1
Size: 8 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1395 Color: 1
Size: 485 Color: 18
Size: 96 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1075 Color: 8
Size: 751 Color: 10
Size: 150 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1023 Color: 9
Size: 795 Color: 4
Size: 158 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 2
Size: 262 Color: 10
Size: 48 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1103 Color: 19
Size: 729 Color: 11
Size: 144 Color: 5

Bin 9: 0 of cap free
Amount of items: 4
Items: 
Size: 1713 Color: 6
Size: 227 Color: 17
Size: 20 Color: 12
Size: 16 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 18
Size: 323 Color: 15
Size: 64 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 17
Size: 346 Color: 8
Size: 68 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1611 Color: 8
Size: 305 Color: 18
Size: 60 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 11
Size: 542 Color: 6
Size: 108 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1524 Color: 5
Size: 380 Color: 17
Size: 72 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 3
Size: 502 Color: 1
Size: 60 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 17
Size: 598 Color: 14
Size: 116 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 9
Size: 249 Color: 3
Size: 48 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1700 Color: 6
Size: 236 Color: 18
Size: 40 Color: 5

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 0
Size: 334 Color: 15
Size: 64 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 11
Size: 233 Color: 15
Size: 46 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 9
Size: 307 Color: 13
Size: 42 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 12
Size: 311 Color: 11
Size: 62 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 11
Size: 274 Color: 4
Size: 52 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 11
Size: 467 Color: 3
Size: 92 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1635 Color: 19
Size: 317 Color: 13
Size: 24 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 5
Size: 266 Color: 19
Size: 52 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 14
Size: 246 Color: 16
Size: 48 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 16
Size: 391 Color: 16
Size: 78 Color: 18

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 5
Size: 319 Color: 19
Size: 62 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 7
Size: 718 Color: 17
Size: 140 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 4
Size: 430 Color: 3
Size: 84 Color: 10

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1275 Color: 10
Size: 585 Color: 9
Size: 116 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 9
Size: 170 Color: 4
Size: 32 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1610 Color: 5
Size: 306 Color: 8
Size: 60 Color: 2

Bin 35: 0 of cap free
Amount of items: 4
Items: 
Size: 800 Color: 9
Size: 668 Color: 18
Size: 304 Color: 14
Size: 204 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 991 Color: 19
Size: 899 Color: 10
Size: 86 Color: 14

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 4
Size: 282 Color: 4
Size: 56 Color: 10

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1499 Color: 19
Size: 399 Color: 1
Size: 78 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 2
Size: 395 Color: 13
Size: 78 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 990 Color: 17
Size: 822 Color: 16
Size: 164 Color: 12

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 18
Size: 166 Color: 11
Size: 32 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1705 Color: 2
Size: 221 Color: 4
Size: 50 Color: 9

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 6
Size: 481 Color: 13
Size: 94 Color: 7

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 0
Size: 362 Color: 17
Size: 72 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 18
Size: 427 Color: 18
Size: 84 Color: 6

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 989 Color: 17
Size: 823 Color: 7
Size: 164 Color: 9

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 9
Size: 402 Color: 7
Size: 80 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 19
Size: 218 Color: 17
Size: 40 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 17
Size: 210 Color: 15
Size: 40 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1158 Color: 2
Size: 682 Color: 13
Size: 136 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 0
Size: 361 Color: 18
Size: 72 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 19
Size: 206 Color: 8
Size: 32 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 2
Size: 502 Color: 3
Size: 100 Color: 7

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1341 Color: 18
Size: 531 Color: 4
Size: 104 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 3
Size: 462 Color: 16
Size: 92 Color: 16

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1511 Color: 15
Size: 389 Color: 8
Size: 76 Color: 13

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1189 Color: 13
Size: 657 Color: 8
Size: 130 Color: 12

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 18
Size: 651 Color: 0
Size: 128 Color: 15

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1313 Color: 3
Size: 553 Color: 12
Size: 110 Color: 14

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1318 Color: 8
Size: 550 Color: 4
Size: 108 Color: 18

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 12
Size: 253 Color: 8
Size: 50 Color: 13

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 3
Size: 213 Color: 4
Size: 42 Color: 15

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1333 Color: 2
Size: 537 Color: 17
Size: 106 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1054 Color: 4
Size: 866 Color: 12
Size: 56 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 6
Size: 241 Color: 1
Size: 46 Color: 19

Total size: 128440
Total free space: 0

