Capicity Bin: 1001
Lower Bound: 216

Bins used: 216
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 519 Color: 0
Size: 314 Color: 19
Size: 168 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 11
Size: 225 Color: 8
Size: 144 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 660 Color: 14
Size: 232 Color: 17
Size: 109 Color: 7

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 17
Size: 370 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 384 Color: 10
Size: 148 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 18
Size: 220 Color: 9
Size: 171 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 6
Size: 225 Color: 2
Size: 144 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 644 Color: 13
Size: 232 Color: 2
Size: 125 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 519 Color: 16
Size: 303 Color: 18
Size: 179 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 596 Color: 7
Size: 259 Color: 3
Size: 146 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 599 Color: 9
Size: 302 Color: 1
Size: 100 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 2
Size: 328 Color: 9
Size: 107 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 511 Color: 16
Size: 303 Color: 18
Size: 187 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 642 Color: 1
Size: 217 Color: 1
Size: 142 Color: 19

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 18
Size: 357 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 10
Size: 305 Color: 12
Size: 147 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 12
Size: 178 Color: 1
Size: 115 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 19
Size: 281 Color: 4
Size: 154 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 5
Size: 253 Color: 3
Size: 109 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 512 Color: 7
Size: 312 Color: 4
Size: 177 Color: 5

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 8
Size: 431 Color: 19
Size: 102 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 644 Color: 4
Size: 250 Color: 6
Size: 107 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 521 Color: 1
Size: 260 Color: 14
Size: 220 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 2
Size: 250 Color: 9
Size: 242 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 586 Color: 1
Size: 268 Color: 13
Size: 147 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 17
Size: 430 Color: 6
Size: 108 Color: 18

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 365 Color: 9
Size: 216 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 523 Color: 17
Size: 314 Color: 13
Size: 164 Color: 17

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 13
Size: 454 Color: 12

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 520 Color: 18
Size: 324 Color: 19
Size: 157 Color: 7

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 7
Size: 247 Color: 5

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 559 Color: 15
Size: 306 Color: 0
Size: 136 Color: 19

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 561 Color: 14
Size: 319 Color: 6
Size: 121 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 8
Size: 156 Color: 9
Size: 102 Color: 7

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 553 Color: 6
Size: 269 Color: 1
Size: 179 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 14
Size: 217 Color: 19
Size: 125 Color: 9

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 5
Size: 388 Color: 6
Size: 159 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 13
Size: 182 Color: 18
Size: 179 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 511 Color: 19
Size: 324 Color: 15
Size: 166 Color: 16

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 16
Size: 470 Color: 7

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 627 Color: 8
Size: 229 Color: 11
Size: 145 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 517 Color: 15
Size: 384 Color: 2
Size: 100 Color: 5

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 2
Size: 220 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 1
Size: 185 Color: 11
Size: 107 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 559 Color: 10
Size: 303 Color: 3
Size: 139 Color: 8

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 6
Size: 195 Color: 10
Size: 175 Color: 7

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 3
Size: 318 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 548 Color: 0
Size: 294 Color: 14
Size: 159 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 548 Color: 2
Size: 317 Color: 8
Size: 136 Color: 6

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 596 Color: 12
Size: 216 Color: 1
Size: 189 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 13
Size: 351 Color: 2
Size: 182 Color: 12

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 7
Size: 456 Color: 18

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 5
Size: 294 Color: 8

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 1
Size: 319 Color: 3
Size: 122 Color: 7

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 6
Size: 336 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 519 Color: 14
Size: 301 Color: 5
Size: 181 Color: 15

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 19
Size: 467 Color: 13

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 565 Color: 18
Size: 305 Color: 3
Size: 131 Color: 13

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 7
Size: 328 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 12
Size: 317 Color: 19
Size: 183 Color: 9

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 3
Size: 301 Color: 10
Size: 151 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 7
Size: 286 Color: 14
Size: 247 Color: 12

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 601 Color: 17
Size: 279 Color: 11
Size: 121 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 15
Size: 193 Color: 5
Size: 151 Color: 5

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 643 Color: 1
Size: 242 Color: 5
Size: 116 Color: 13

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 658 Color: 15
Size: 189 Color: 4
Size: 154 Color: 9

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 2
Size: 143 Color: 11
Size: 134 Color: 4

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 10
Size: 364 Color: 6

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 3
Size: 430 Color: 17

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9
Size: 406 Color: 15
Size: 128 Color: 11

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 7
Size: 216 Color: 0
Size: 130 Color: 10

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 11
Size: 406 Color: 9
Size: 151 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 11
Size: 148 Color: 17
Size: 132 Color: 5

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 795 Color: 6
Size: 103 Color: 2
Size: 103 Color: 0

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 13
Size: 337 Color: 5

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 19
Size: 330 Color: 4

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 10
Size: 231 Color: 6

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 9
Size: 478 Color: 2

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 11
Size: 412 Color: 6

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 3
Size: 281 Color: 8

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 12
Size: 228 Color: 9

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 11
Size: 361 Color: 5

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 6
Size: 234 Color: 16

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 9
Size: 205 Color: 5

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 7
Size: 228 Color: 18

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 17
Size: 276 Color: 9

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 16
Size: 308 Color: 0

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 18
Size: 407 Color: 13

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 10
Size: 204 Color: 8

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 0
Size: 477 Color: 14

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 17
Size: 350 Color: 12

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 11
Size: 307 Color: 16

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 0
Size: 290 Color: 13

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 7
Size: 203 Color: 4

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 5
Size: 331 Color: 4

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 19
Size: 352 Color: 8

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 19
Size: 393 Color: 6

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 204 Color: 13

Bin 99: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 9
Size: 409 Color: 8

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 8
Size: 347 Color: 15

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 2
Size: 347 Color: 5

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 0
Size: 491 Color: 1

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 3
Size: 456 Color: 18

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 3
Size: 288 Color: 14

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 7
Size: 476 Color: 13

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 0
Size: 425 Color: 14

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 0
Size: 499 Color: 9

Bin 108: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 485 Color: 7

Bin 109: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 12
Size: 476 Color: 2

Bin 110: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 5
Size: 476 Color: 3

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 14
Size: 479 Color: 9

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 3
Size: 465 Color: 0

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 0
Size: 471 Color: 11

Bin 114: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 7
Size: 443 Color: 15

Bin 115: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 18
Size: 439 Color: 6

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 11
Size: 436 Color: 7

Bin 117: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 10
Size: 428 Color: 15

Bin 118: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 16
Size: 423 Color: 2

Bin 119: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 10
Size: 422 Color: 2

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 4
Size: 417 Color: 11

Bin 121: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 19
Size: 414 Color: 12

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 2
Size: 398 Color: 11

Bin 123: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 10
Size: 397 Color: 0

Bin 124: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 19
Size: 388 Color: 6

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 14
Size: 383 Color: 16

Bin 126: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 2
Size: 381 Color: 1

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 13
Size: 376 Color: 11

Bin 128: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 8
Size: 375 Color: 11

Bin 129: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 16
Size: 348 Color: 18

Bin 130: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 8
Size: 342 Color: 14

Bin 131: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 3
Size: 335 Color: 15

Bin 132: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 15
Size: 333 Color: 14

Bin 133: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 17
Size: 323 Color: 11

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 19
Size: 310 Color: 16

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 12
Size: 295 Color: 19

Bin 136: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 7
Size: 295 Color: 1

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 16
Size: 291 Color: 12

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 2
Size: 264 Color: 1

Bin 139: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 12
Size: 255 Color: 11

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 6
Size: 252 Color: 14

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 13
Size: 251 Color: 2

Bin 142: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 6
Size: 223 Color: 5

Bin 143: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 10
Size: 222 Color: 7

Bin 144: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 8
Size: 221 Color: 18

Bin 145: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 8
Size: 204 Color: 14

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 519 Color: 5
Size: 241 Color: 18
Size: 240 Color: 10

Bin 147: 1 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 8
Size: 455 Color: 17

Bin 148: 1 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 19
Size: 373 Color: 3

Bin 149: 1 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 13
Size: 420 Color: 19

Bin 150: 1 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 9
Size: 364 Color: 4

Bin 151: 1 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 18
Size: 345 Color: 19

Bin 152: 1 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 10
Size: 255 Color: 9

Bin 153: 1 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 6
Size: 407 Color: 12

Bin 154: 1 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 16
Size: 408 Color: 7

Bin 155: 1 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 3
Size: 282 Color: 10

Bin 156: 1 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 14
Size: 283 Color: 2

Bin 157: 1 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 2
Size: 202 Color: 15

Bin 158: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 14
Size: 367 Color: 4

Bin 159: 1 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 5
Size: 248 Color: 13

Bin 160: 1 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 5
Size: 218 Color: 11

Bin 161: 1 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 17
Size: 205 Color: 5

Bin 162: 1 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 7
Size: 412 Color: 15

Bin 163: 1 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 4
Size: 326 Color: 16

Bin 164: 1 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 7
Size: 243 Color: 0

Bin 165: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 9
Size: 483 Color: 2

Bin 166: 1 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 13
Size: 496 Color: 19

Bin 167: 1 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 17
Size: 460 Color: 7

Bin 168: 1 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 15
Size: 433 Color: 6

Bin 169: 1 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 0
Size: 385 Color: 19

Bin 170: 1 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 3
Size: 377 Color: 7

Bin 171: 1 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 3
Size: 348 Color: 0

Bin 172: 1 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 18
Size: 326 Color: 2

Bin 173: 1 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 9
Size: 315 Color: 8

Bin 174: 1 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 8
Size: 299 Color: 10

Bin 175: 1 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 14
Size: 296 Color: 19

Bin 176: 1 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 4
Size: 271 Color: 2

Bin 177: 1 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 6
Size: 262 Color: 11

Bin 178: 1 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 2
Size: 238 Color: 15

Bin 179: 2 of cap free
Amount of items: 3
Items: 
Size: 627 Color: 0
Size: 190 Color: 16
Size: 182 Color: 10

Bin 180: 2 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 11
Size: 319 Color: 0

Bin 181: 2 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 13
Size: 256 Color: 7

Bin 182: 2 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 11
Size: 273 Color: 18

Bin 183: 2 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 6
Size: 234 Color: 13

Bin 184: 2 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 0
Size: 411 Color: 14

Bin 185: 2 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 18
Size: 411 Color: 1

Bin 186: 2 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 12
Size: 286 Color: 0

Bin 187: 2 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 19
Size: 325 Color: 14

Bin 188: 2 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 446 Color: 10

Bin 189: 2 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 2
Size: 243 Color: 7

Bin 190: 2 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 4
Size: 306 Color: 12

Bin 191: 2 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 393 Color: 13

Bin 192: 2 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 366 Color: 18

Bin 193: 2 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 0
Size: 493 Color: 4

Bin 194: 2 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 10
Size: 486 Color: 13

Bin 195: 2 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 16
Size: 432 Color: 18

Bin 196: 2 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 14
Size: 427 Color: 10

Bin 197: 2 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 5
Size: 416 Color: 16

Bin 198: 2 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 12
Size: 377 Color: 7

Bin 199: 2 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 5
Size: 353 Color: 16

Bin 200: 2 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 4
Size: 296 Color: 3

Bin 201: 2 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 7
Size: 222 Color: 14

Bin 202: 2 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 2
Size: 212 Color: 1

Bin 203: 3 of cap free
Amount of items: 3
Items: 
Size: 521 Color: 18
Size: 364 Color: 14
Size: 113 Color: 5

Bin 204: 3 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 12
Size: 318 Color: 3
Size: 156 Color: 6

Bin 205: 3 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 14
Size: 172 Color: 4
Size: 118 Color: 2

Bin 206: 3 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 17
Size: 333 Color: 4

Bin 207: 3 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 12
Size: 455 Color: 0

Bin 208: 3 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 4
Size: 273 Color: 3

Bin 209: 3 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 0
Size: 445 Color: 15

Bin 210: 3 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 15
Size: 471 Color: 17

Bin 211: 3 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 437 Color: 6

Bin 212: 3 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 7
Size: 376 Color: 8

Bin 213: 4 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 5
Size: 387 Color: 2
Size: 140 Color: 8

Bin 214: 9 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 17
Size: 352 Color: 0
Size: 185 Color: 10

Bin 215: 14 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 3
Size: 181 Color: 10
Size: 151 Color: 16

Bin 216: 20 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 5
Size: 318 Color: 14
Size: 267 Color: 8

Total size: 216058
Total free space: 158

