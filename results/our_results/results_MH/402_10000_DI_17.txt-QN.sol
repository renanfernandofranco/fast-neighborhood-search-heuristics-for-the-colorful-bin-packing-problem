Capicity Bin: 8264
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 6236 Color: 318
Size: 1862 Color: 228
Size: 166 Color: 29

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6251 Color: 320
Size: 1679 Color: 222
Size: 334 Color: 89

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 321
Size: 1671 Color: 221
Size: 334 Color: 90

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6414 Color: 325
Size: 1692 Color: 224
Size: 158 Color: 23

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6436 Color: 326
Size: 1548 Color: 217
Size: 280 Color: 78

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6446 Color: 327
Size: 1278 Color: 197
Size: 540 Color: 120

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6611 Color: 334
Size: 1233 Color: 194
Size: 420 Color: 105

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 338
Size: 1372 Color: 205
Size: 226 Color: 59

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 339
Size: 1116 Color: 184
Size: 468 Color: 109

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6765 Color: 345
Size: 1305 Color: 200
Size: 194 Color: 47

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6774 Color: 346
Size: 1114 Color: 183
Size: 376 Color: 99

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6785 Color: 347
Size: 1227 Color: 193
Size: 252 Color: 69

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6907 Color: 353
Size: 1111 Color: 182
Size: 246 Color: 65

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6963 Color: 356
Size: 1085 Color: 180
Size: 216 Color: 56

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7002 Color: 359
Size: 790 Color: 148
Size: 472 Color: 110

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7039 Color: 362
Size: 977 Color: 172
Size: 248 Color: 68

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 363
Size: 802 Color: 150
Size: 416 Color: 102

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7069 Color: 364
Size: 1131 Color: 187
Size: 64 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 366
Size: 700 Color: 136
Size: 474 Color: 112

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7154 Color: 372
Size: 598 Color: 127
Size: 512 Color: 118

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7157 Color: 373
Size: 923 Color: 165
Size: 184 Color: 39

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 375
Size: 542 Color: 122
Size: 542 Color: 121

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 376
Size: 826 Color: 153
Size: 256 Color: 70

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7257 Color: 381
Size: 821 Color: 152
Size: 186 Color: 43

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7289 Color: 385
Size: 813 Color: 151
Size: 162 Color: 26

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7309 Color: 388
Size: 783 Color: 146
Size: 172 Color: 31

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7364 Color: 394
Size: 684 Color: 133
Size: 216 Color: 55

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7365 Color: 395
Size: 751 Color: 143
Size: 148 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7388 Color: 397
Size: 724 Color: 139
Size: 152 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7412 Color: 400
Size: 680 Color: 130
Size: 172 Color: 32

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7428 Color: 402
Size: 684 Color: 131
Size: 152 Color: 19

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 5403 Color: 297
Size: 2860 Color: 255

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 6212 Color: 317
Size: 1871 Color: 230
Size: 180 Color: 38

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 6267 Color: 322
Size: 1996 Color: 232

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 324
Size: 1665 Color: 220
Size: 186 Color: 42

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6451 Color: 328
Size: 1788 Color: 226
Size: 24 Color: 0

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 6580 Color: 332
Size: 1683 Color: 223

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6605 Color: 333
Size: 1450 Color: 210
Size: 208 Color: 54

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 336
Size: 1505 Color: 212
Size: 138 Color: 10

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 344
Size: 1318 Color: 201
Size: 184 Color: 40

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6793 Color: 348
Size: 1470 Color: 211

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 350
Size: 1397 Color: 208

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6902 Color: 352
Size: 1361 Color: 204

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 7281 Color: 384
Size: 982 Color: 173

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 7306 Color: 387
Size: 957 Color: 170

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 7322 Color: 389
Size: 941 Color: 169

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 5850 Color: 309
Size: 2412 Color: 244

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 6619 Color: 335
Size: 1383 Color: 207
Size: 260 Color: 71

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 6686 Color: 340
Size: 1576 Color: 218

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6883 Color: 351
Size: 1379 Color: 206

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6978 Color: 358
Size: 1284 Color: 198

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 7020 Color: 361
Size: 1242 Color: 195

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 7082 Color: 365
Size: 1180 Color: 192

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 7164 Color: 374
Size: 1098 Color: 181

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 7298 Color: 386
Size: 964 Color: 171

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 7325 Color: 390
Size: 937 Color: 168

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 7354 Color: 392
Size: 908 Color: 164

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 7394 Color: 398
Size: 868 Color: 160

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 7404 Color: 399
Size: 858 Color: 159

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 7418 Color: 401
Size: 844 Color: 156

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 5876 Color: 310
Size: 2385 Color: 243

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 6502 Color: 330
Size: 1511 Color: 213
Size: 248 Color: 67

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 6719 Color: 341
Size: 1542 Color: 216

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 7137 Color: 370
Size: 1124 Color: 186

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 7359 Color: 393
Size: 902 Color: 163

Bin 66: 4 of cap free
Amount of items: 5
Items: 
Size: 4158 Color: 279
Size: 3438 Color: 265
Size: 308 Color: 86
Size: 180 Color: 37
Size: 176 Color: 36

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 5156 Color: 293
Size: 3104 Color: 258

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 5757 Color: 308
Size: 2367 Color: 240
Size: 136 Color: 7

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 7141 Color: 371
Size: 1119 Color: 185

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 7260 Color: 382
Size: 1000 Color: 175

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 7274 Color: 383
Size: 986 Color: 174

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 6190 Color: 316
Size: 1865 Color: 229
Size: 204 Color: 53

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6245 Color: 319
Size: 2014 Color: 233

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 6970 Color: 357
Size: 1289 Color: 199

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 7093 Color: 367
Size: 1166 Color: 190

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 7108 Color: 368
Size: 1151 Color: 189

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 7238 Color: 380
Size: 1021 Color: 177

Bin 78: 6 of cap free
Amount of items: 5
Items: 
Size: 4150 Color: 278
Size: 3430 Color: 264
Size: 304 Color: 85
Size: 190 Color: 44
Size: 184 Color: 41

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 4836 Color: 287
Size: 3422 Color: 263

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 5749 Color: 307
Size: 2373 Color: 241
Size: 136 Color: 8

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6734 Color: 343
Size: 1524 Color: 215

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6924 Color: 354
Size: 1334 Color: 203

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 7214 Color: 378
Size: 1044 Color: 178

Bin 84: 6 of cap free
Amount of items: 2
Items: 
Size: 7380 Color: 396
Size: 878 Color: 161

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 4673 Color: 283
Size: 3584 Color: 270

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 306
Size: 2381 Color: 242
Size: 136 Color: 9

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 6459 Color: 329
Size: 1798 Color: 227

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 281
Size: 3764 Color: 271
Size: 176 Color: 33

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 4714 Color: 285
Size: 3542 Color: 269

Bin 90: 8 of cap free
Amount of items: 4
Items: 
Size: 6030 Color: 314
Size: 2114 Color: 237
Size: 56 Color: 3
Size: 56 Color: 2

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 331
Size: 1730 Color: 225

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 6852 Color: 349
Size: 1404 Color: 209

Bin 93: 8 of cap free
Amount of items: 2
Items: 
Size: 7330 Color: 391
Size: 926 Color: 167

Bin 94: 9 of cap free
Amount of items: 5
Items: 
Size: 4262 Color: 280
Size: 3441 Color: 266
Size: 200 Color: 52
Size: 176 Color: 35
Size: 176 Color: 34

Bin 95: 9 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 315
Size: 2097 Color: 235
Size: 48 Color: 1

Bin 96: 9 of cap free
Amount of items: 2
Items: 
Size: 7117 Color: 369
Size: 1138 Color: 188

Bin 97: 9 of cap free
Amount of items: 2
Items: 
Size: 7201 Color: 377
Size: 1054 Color: 179

Bin 98: 10 of cap free
Amount of items: 3
Items: 
Size: 5532 Color: 301
Size: 2578 Color: 248
Size: 144 Color: 15

Bin 99: 10 of cap free
Amount of items: 2
Items: 
Size: 6648 Color: 337
Size: 1606 Color: 219

Bin 100: 10 of cap free
Amount of items: 2
Items: 
Size: 7236 Color: 379
Size: 1018 Color: 176

Bin 101: 12 of cap free
Amount of items: 2
Items: 
Size: 7004 Color: 360
Size: 1248 Color: 196

Bin 102: 13 of cap free
Amount of items: 2
Items: 
Size: 6931 Color: 355
Size: 1320 Color: 202

Bin 103: 14 of cap free
Amount of items: 2
Items: 
Size: 6732 Color: 342
Size: 1518 Color: 214

Bin 104: 15 of cap free
Amount of items: 9
Items: 
Size: 4133 Color: 273
Size: 726 Color: 140
Size: 716 Color: 138
Size: 704 Color: 137
Size: 688 Color: 135
Size: 688 Color: 134
Size: 200 Color: 51
Size: 198 Color: 50
Size: 196 Color: 49

Bin 105: 16 of cap free
Amount of items: 7
Items: 
Size: 4135 Color: 275
Size: 854 Color: 157
Size: 841 Color: 155
Size: 841 Color: 154
Size: 797 Color: 149
Size: 588 Color: 126
Size: 192 Color: 46

Bin 106: 22 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 304
Size: 2422 Color: 245
Size: 144 Color: 12

Bin 107: 26 of cap free
Amount of items: 2
Items: 
Size: 5996 Color: 311
Size: 2242 Color: 238

Bin 108: 28 of cap free
Amount of items: 2
Items: 
Size: 4938 Color: 288
Size: 3298 Color: 261

Bin 109: 28 of cap free
Amount of items: 3
Items: 
Size: 5574 Color: 303
Size: 2518 Color: 247
Size: 144 Color: 13

Bin 110: 30 of cap free
Amount of items: 3
Items: 
Size: 5362 Color: 296
Size: 2716 Color: 252
Size: 156 Color: 20

Bin 111: 32 of cap free
Amount of items: 3
Items: 
Size: 4730 Color: 286
Size: 3338 Color: 262
Size: 164 Color: 28

Bin 112: 34 of cap free
Amount of items: 2
Items: 
Size: 6338 Color: 323
Size: 1892 Color: 231

Bin 113: 42 of cap free
Amount of items: 3
Items: 
Size: 6019 Color: 312
Size: 2091 Color: 234
Size: 112 Color: 6

Bin 114: 50 of cap free
Amount of items: 2
Items: 
Size: 4142 Color: 277
Size: 4072 Color: 272

Bin 115: 53 of cap free
Amount of items: 3
Items: 
Size: 6027 Color: 313
Size: 2108 Color: 236
Size: 76 Color: 5

Bin 116: 62 of cap free
Amount of items: 3
Items: 
Size: 5566 Color: 302
Size: 2492 Color: 246
Size: 144 Color: 14

Bin 117: 67 of cap free
Amount of items: 4
Items: 
Size: 5174 Color: 294
Size: 2707 Color: 250
Size: 160 Color: 24
Size: 156 Color: 22

Bin 118: 73 of cap free
Amount of items: 2
Items: 
Size: 5417 Color: 299
Size: 2774 Color: 254

Bin 119: 91 of cap free
Amount of items: 6
Items: 
Size: 4140 Color: 276
Size: 1176 Color: 191
Size: 924 Color: 166
Size: 887 Color: 162
Size: 854 Color: 158
Size: 192 Color: 45

Bin 120: 94 of cap free
Amount of items: 3
Items: 
Size: 5017 Color: 292
Size: 2993 Color: 257
Size: 160 Color: 25

Bin 121: 99 of cap free
Amount of items: 3
Items: 
Size: 5425 Color: 300
Size: 2596 Color: 249
Size: 144 Color: 16

Bin 122: 106 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 305
Size: 2284 Color: 239
Size: 144 Color: 11

Bin 123: 119 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 295
Size: 2713 Color: 251
Size: 156 Color: 21

Bin 124: 128 of cap free
Amount of items: 3
Items: 
Size: 5012 Color: 291
Size: 2962 Color: 256
Size: 162 Color: 27

Bin 125: 131 of cap free
Amount of items: 2
Items: 
Size: 5009 Color: 290
Size: 3124 Color: 260

Bin 126: 134 of cap free
Amount of items: 2
Items: 
Size: 5409 Color: 298
Size: 2721 Color: 253

Bin 127: 137 of cap free
Amount of items: 3
Items: 
Size: 4516 Color: 282
Size: 3443 Color: 267
Size: 168 Color: 30

Bin 128: 138 of cap free
Amount of items: 7
Items: 
Size: 4134 Color: 274
Size: 786 Color: 147
Size: 782 Color: 145
Size: 756 Color: 144
Size: 740 Color: 142
Size: 732 Color: 141
Size: 196 Color: 48

Bin 129: 139 of cap free
Amount of items: 2
Items: 
Size: 4681 Color: 284
Size: 3444 Color: 268

Bin 130: 148 of cap free
Amount of items: 2
Items: 
Size: 5001 Color: 289
Size: 3115 Color: 259

Bin 131: 190 of cap free
Amount of items: 24
Items: 
Size: 448 Color: 108
Size: 448 Color: 107
Size: 448 Color: 106
Size: 418 Color: 104
Size: 416 Color: 103
Size: 400 Color: 101
Size: 392 Color: 100
Size: 376 Color: 98
Size: 374 Color: 97
Size: 372 Color: 96
Size: 372 Color: 95
Size: 356 Color: 94
Size: 344 Color: 93
Size: 336 Color: 92
Size: 276 Color: 77
Size: 274 Color: 76
Size: 272 Color: 75
Size: 264 Color: 74
Size: 264 Color: 73
Size: 264 Color: 72
Size: 248 Color: 66
Size: 244 Color: 64
Size: 236 Color: 63
Size: 232 Color: 62

Bin 132: 214 of cap free
Amount of items: 17
Items: 
Size: 684 Color: 132
Size: 664 Color: 129
Size: 624 Color: 128
Size: 588 Color: 125
Size: 568 Color: 124
Size: 552 Color: 123
Size: 536 Color: 119
Size: 512 Color: 117
Size: 496 Color: 116
Size: 488 Color: 115
Size: 480 Color: 114
Size: 476 Color: 113
Size: 474 Color: 111
Size: 232 Color: 61
Size: 230 Color: 60
Size: 224 Color: 58
Size: 222 Color: 57

Bin 133: 5490 of cap free
Amount of items: 9
Items: 
Size: 336 Color: 91
Size: 332 Color: 88
Size: 320 Color: 87
Size: 304 Color: 84
Size: 302 Color: 83
Size: 300 Color: 82
Size: 300 Color: 81
Size: 292 Color: 80
Size: 288 Color: 79

Total size: 1090848
Total free space: 8264

