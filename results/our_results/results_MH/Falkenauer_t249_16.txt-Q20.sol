Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 3
Size: 312 Color: 3
Size: 264 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 7
Size: 252 Color: 8
Size: 250 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 10
Size: 311 Color: 3
Size: 273 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 17
Size: 254 Color: 12
Size: 251 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 18
Size: 295 Color: 1
Size: 260 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9
Size: 253 Color: 7
Size: 252 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 14
Size: 281 Color: 13
Size: 259 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 19
Size: 310 Color: 6
Size: 259 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 15
Size: 303 Color: 19
Size: 302 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 18
Size: 293 Color: 3
Size: 273 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 9
Size: 354 Color: 2
Size: 264 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 9
Size: 301 Color: 8
Size: 296 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 330 Color: 14
Size: 288 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 10
Size: 309 Color: 8
Size: 277 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 0
Size: 266 Color: 14
Size: 258 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 17
Size: 262 Color: 7
Size: 256 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 8
Size: 254 Color: 14
Size: 250 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8
Size: 331 Color: 5
Size: 257 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 7
Size: 279 Color: 18
Size: 280 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 19
Size: 259 Color: 10
Size: 254 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 11
Size: 333 Color: 19
Size: 263 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 8
Size: 276 Color: 17
Size: 276 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 18
Size: 317 Color: 5
Size: 269 Color: 10

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 5
Size: 336 Color: 11
Size: 275 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 5
Size: 304 Color: 7
Size: 266 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 19
Size: 300 Color: 14
Size: 253 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7
Size: 328 Color: 0
Size: 296 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 7
Size: 274 Color: 9
Size: 255 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 6
Size: 276 Color: 19
Size: 251 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 8
Size: 337 Color: 18
Size: 284 Color: 15

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 4
Size: 264 Color: 2
Size: 256 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 6
Size: 342 Color: 6
Size: 261 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 18
Size: 356 Color: 9
Size: 264 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 17
Size: 277 Color: 4
Size: 253 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 12
Size: 294 Color: 16
Size: 269 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 9
Size: 294 Color: 16
Size: 268 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 332 Color: 11
Size: 254 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 8
Size: 261 Color: 1
Size: 258 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 6
Size: 316 Color: 8
Size: 273 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 19
Size: 294 Color: 8
Size: 271 Color: 14

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 2
Size: 256 Color: 13
Size: 251 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 12
Size: 341 Color: 17
Size: 250 Color: 7

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 15
Size: 324 Color: 5
Size: 286 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 17
Size: 329 Color: 18
Size: 294 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 16
Size: 317 Color: 17
Size: 254 Color: 8

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 19
Size: 307 Color: 4
Size: 261 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 7
Size: 265 Color: 19
Size: 258 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 365 Color: 1
Size: 254 Color: 14

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 13
Size: 324 Color: 4
Size: 298 Color: 18

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8
Size: 304 Color: 4
Size: 287 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 10
Size: 283 Color: 18
Size: 263 Color: 10

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 2
Size: 304 Color: 4
Size: 285 Color: 9

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 15
Size: 343 Color: 12
Size: 263 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 16
Size: 308 Color: 18
Size: 253 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 14
Size: 297 Color: 2
Size: 271 Color: 9

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 8
Size: 321 Color: 17
Size: 290 Color: 8

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 293 Color: 4
Size: 287 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 2
Size: 274 Color: 14
Size: 250 Color: 8

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 10
Size: 357 Color: 0
Size: 258 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 0
Size: 285 Color: 17
Size: 252 Color: 16

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 17
Size: 361 Color: 3
Size: 257 Color: 11

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 282 Color: 12
Size: 276 Color: 12

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 303 Color: 8
Size: 280 Color: 8
Size: 417 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 17
Size: 298 Color: 8
Size: 277 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 19
Size: 267 Color: 2
Size: 266 Color: 9

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 9
Size: 308 Color: 7
Size: 255 Color: 13

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 6
Size: 318 Color: 17
Size: 290 Color: 18

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 15
Size: 362 Color: 10
Size: 273 Color: 11

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 15
Size: 272 Color: 6
Size: 261 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 13
Size: 352 Color: 0
Size: 287 Color: 8

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 341 Color: 19
Size: 267 Color: 5

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 15
Size: 298 Color: 12
Size: 250 Color: 8

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9
Size: 279 Color: 15
Size: 260 Color: 12

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 12
Size: 360 Color: 4
Size: 258 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 6
Size: 352 Color: 19
Size: 267 Color: 7

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 19
Size: 294 Color: 16
Size: 258 Color: 19

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 256 Color: 11
Size: 254 Color: 12

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 19
Size: 351 Color: 19
Size: 269 Color: 8

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 14
Size: 271 Color: 17
Size: 263 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 13
Size: 291 Color: 8
Size: 290 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 14
Size: 287 Color: 16
Size: 267 Color: 12

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 12
Size: 285 Color: 0
Size: 263 Color: 11

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 293 Color: 19
Size: 250 Color: 0
Size: 457 Color: 1

Total size: 83000
Total free space: 0

