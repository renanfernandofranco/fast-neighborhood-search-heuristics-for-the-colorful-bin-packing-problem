Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 116
Size: 258 Color: 21
Size: 252 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 107
Size: 302 Color: 53
Size: 252 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 88
Size: 329 Color: 66
Size: 285 Color: 48

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 102
Size: 292 Color: 51
Size: 275 Color: 40

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 91
Size: 334 Color: 68
Size: 270 Color: 33

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 90
Size: 339 Color: 71
Size: 273 Color: 37

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 119
Size: 253 Color: 9
Size: 250 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 98
Size: 328 Color: 65
Size: 250 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 105
Size: 306 Color: 56
Size: 252 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 113
Size: 262 Color: 23
Size: 254 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 118
Size: 254 Color: 15
Size: 250 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 117
Size: 256 Color: 17
Size: 251 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 79
Size: 361 Color: 78
Size: 276 Color: 43

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 93
Size: 347 Color: 73
Size: 250 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 89
Size: 335 Color: 69
Size: 278 Color: 46

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 80
Size: 342 Color: 72
Size: 294 Color: 52

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 106
Size: 290 Color: 50
Size: 265 Color: 27

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 101
Size: 310 Color: 60
Size: 263 Color: 26

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 111
Size: 276 Color: 44
Size: 254 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 97
Size: 308 Color: 58
Size: 273 Color: 36

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 96
Size: 318 Color: 63
Size: 268 Color: 30

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 85
Size: 373 Color: 83
Size: 252 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 103
Size: 306 Color: 57
Size: 258 Color: 20

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 110
Size: 276 Color: 45
Size: 262 Color: 25

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 95
Size: 315 Color: 62
Size: 275 Color: 41

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 84
Size: 357 Color: 77
Size: 269 Color: 31

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 86
Size: 372 Color: 81
Size: 253 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 100
Size: 308 Color: 59
Size: 266 Color: 28

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 115
Size: 257 Color: 18
Size: 253 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 112
Size: 270 Color: 32
Size: 258 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 108
Size: 274 Color: 39
Size: 280 Color: 47

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 82
Size: 352 Color: 75
Size: 276 Color: 42

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 99
Size: 323 Color: 64
Size: 254 Color: 13

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 92
Size: 330 Color: 67
Size: 268 Color: 29

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 94
Size: 305 Color: 55
Size: 287 Color: 49

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 114
Size: 260 Color: 22
Size: 255 Color: 16

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 109
Size: 272 Color: 35
Size: 270 Color: 34

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 104
Size: 302 Color: 54
Size: 262 Color: 24

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 76
Size: 336 Color: 70
Size: 312 Color: 61

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 87
Size: 349 Color: 74
Size: 274 Color: 38

Total size: 40000
Total free space: 0

