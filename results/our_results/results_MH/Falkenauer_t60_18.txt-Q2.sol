Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 366 Color: 0
Size: 254 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 324 Color: 0
Size: 277 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 0
Size: 295 Color: 0
Size: 281 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 370 Color: 1
Size: 253 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 319 Color: 1
Size: 261 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 291 Color: 0
Size: 256 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 255 Color: 0
Size: 250 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 361 Color: 1
Size: 275 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 255 Color: 1
Size: 252 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 254 Color: 1
Size: 275 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 0
Size: 331 Color: 1
Size: 296 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 273 Color: 0
Size: 261 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 315 Color: 0
Size: 251 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 256 Color: 0
Size: 252 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 304 Color: 1
Size: 253 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 0
Size: 352 Color: 1
Size: 290 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 337 Color: 0
Size: 278 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 290 Color: 1
Size: 271 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 270 Color: 1
Size: 251 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 347 Color: 1
Size: 276 Color: 1

Total size: 20000
Total free space: 0

