Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 330 Color: 4
Size: 394 Color: 2
Size: 277 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 3
Size: 298 Color: 4
Size: 268 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 296 Color: 2
Size: 440 Color: 3
Size: 265 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 288 Color: 0
Size: 284 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 323 Color: 2
Size: 275 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 327 Color: 2
Size: 254 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 3
Size: 275 Color: 0
Size: 250 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 0
Size: 337 Color: 1
Size: 296 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 0
Size: 334 Color: 4
Size: 319 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 0
Size: 315 Color: 3
Size: 253 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 4
Size: 314 Color: 2
Size: 259 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 326 Color: 2
Size: 264 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 4
Size: 250 Color: 2
Size: 250 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 0
Size: 327 Color: 2
Size: 327 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 0
Size: 289 Color: 1
Size: 266 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 4
Size: 298 Color: 0
Size: 270 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 344 Color: 4
Size: 296 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 298 Color: 4
Size: 297 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 0
Size: 334 Color: 4
Size: 327 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 330 Color: 1
Size: 268 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 318 Color: 0
Size: 278 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 296 Color: 3
Size: 293 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 348 Color: 4
Size: 282 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 4
Size: 354 Color: 2
Size: 258 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 333 Color: 1
Size: 273 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 0
Size: 343 Color: 1
Size: 264 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 4
Size: 270 Color: 4
Size: 257 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 339 Color: 2
Size: 266 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 4
Size: 333 Color: 2
Size: 317 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 2
Size: 355 Color: 4
Size: 256 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 4
Size: 331 Color: 2
Size: 316 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 265 Color: 2
Size: 251 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 345 Color: 0
Size: 270 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 3
Size: 321 Color: 4
Size: 284 Color: 2

Total size: 34034
Total free space: 0

