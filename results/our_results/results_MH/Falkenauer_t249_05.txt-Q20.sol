Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 4
Size: 313 Color: 14
Size: 265 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 259 Color: 8
Size: 252 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 8
Size: 346 Color: 10
Size: 270 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 11
Size: 250 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 299 Color: 16
Size: 270 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 11
Size: 350 Color: 0
Size: 297 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 17
Size: 297 Color: 19
Size: 265 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 19
Size: 312 Color: 6
Size: 294 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 11
Size: 262 Color: 11
Size: 260 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 17
Size: 304 Color: 8
Size: 276 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 4
Size: 334 Color: 8
Size: 263 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 19
Size: 297 Color: 12
Size: 278 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 342 Color: 19
Size: 279 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 10
Size: 308 Color: 18
Size: 259 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 10
Size: 283 Color: 2
Size: 254 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 9
Size: 301 Color: 19
Size: 299 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 301 Color: 1
Size: 252 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 18
Size: 326 Color: 9
Size: 275 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 18
Size: 259 Color: 3
Size: 257 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 10
Size: 312 Color: 15
Size: 274 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 16
Size: 371 Color: 8
Size: 250 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 5
Size: 293 Color: 15
Size: 288 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 17
Size: 274 Color: 11
Size: 254 Color: 10

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 260 Color: 12
Size: 251 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7
Size: 352 Color: 19
Size: 271 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 15
Size: 257 Color: 1
Size: 252 Color: 18

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 312 Color: 15
Size: 299 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 13
Size: 258 Color: 9
Size: 253 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 16
Size: 279 Color: 2
Size: 268 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 14
Size: 320 Color: 15
Size: 255 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 16
Size: 252 Color: 13
Size: 250 Color: 5

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 11
Size: 281 Color: 12
Size: 274 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 17
Size: 336 Color: 4
Size: 304 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 19
Size: 314 Color: 7
Size: 283 Color: 10

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 18
Size: 345 Color: 11
Size: 267 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 4
Size: 321 Color: 14
Size: 297 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 10
Size: 374 Color: 9
Size: 252 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 13
Size: 273 Color: 17
Size: 264 Color: 13

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 13
Size: 287 Color: 9
Size: 252 Color: 7

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 16
Size: 335 Color: 15
Size: 307 Color: 13

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 3
Size: 267 Color: 6
Size: 254 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8
Size: 319 Color: 4
Size: 269 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 15
Size: 310 Color: 18
Size: 285 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 12
Size: 333 Color: 17
Size: 286 Color: 14

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 10
Size: 352 Color: 0
Size: 282 Color: 13

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 15
Size: 280 Color: 7
Size: 276 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 6
Size: 268 Color: 14
Size: 252 Color: 19

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 3
Size: 343 Color: 13
Size: 307 Color: 12

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 11
Size: 300 Color: 12
Size: 260 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 9
Size: 313 Color: 6
Size: 276 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 335 Color: 4
Size: 289 Color: 7

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 19
Size: 275 Color: 8
Size: 258 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9
Size: 276 Color: 9
Size: 261 Color: 17

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 19
Size: 329 Color: 6
Size: 258 Color: 14

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 14
Size: 330 Color: 4
Size: 290 Color: 17

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 6
Size: 291 Color: 9
Size: 259 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 6
Size: 255 Color: 4
Size: 252 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 9
Size: 305 Color: 5
Size: 270 Color: 5

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 9
Size: 353 Color: 5
Size: 253 Color: 18

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 12
Size: 307 Color: 16
Size: 289 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 14
Size: 260 Color: 2
Size: 252 Color: 6

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6
Size: 324 Color: 2
Size: 315 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 8
Size: 273 Color: 7
Size: 261 Color: 17

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 12
Size: 363 Color: 19
Size: 259 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 320 Color: 4
Size: 317 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 362 Color: 16
Size: 273 Color: 5

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 14
Size: 295 Color: 19
Size: 262 Color: 16

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 0
Size: 283 Color: 18
Size: 279 Color: 6

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 315 Color: 5
Size: 309 Color: 19

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 263 Color: 5
Size: 250 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 10
Size: 356 Color: 12
Size: 263 Color: 9

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 6
Size: 329 Color: 4
Size: 253 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 6
Size: 338 Color: 18
Size: 276 Color: 15

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 17
Size: 310 Color: 3
Size: 255 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 279 Color: 13
Size: 250 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 15
Size: 367 Color: 15
Size: 263 Color: 13

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 12
Size: 330 Color: 18
Size: 285 Color: 17

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 13
Size: 343 Color: 13
Size: 300 Color: 18

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 8
Size: 323 Color: 19
Size: 295 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 9
Size: 328 Color: 16
Size: 268 Color: 9

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 10
Size: 300 Color: 17
Size: 293 Color: 18

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 14
Size: 365 Color: 19
Size: 251 Color: 9

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 15
Size: 294 Color: 8
Size: 273 Color: 10

Total size: 83000
Total free space: 0

