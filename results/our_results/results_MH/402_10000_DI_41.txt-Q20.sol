Capicity Bin: 8080
Lower Bound: 132

Bins used: 132
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3264 Color: 3
Size: 1680 Color: 11
Size: 1248 Color: 18
Size: 896 Color: 13
Size: 656 Color: 12
Size: 304 Color: 9
Size: 32 Color: 13

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 4312 Color: 10
Size: 3368 Color: 10
Size: 288 Color: 19
Size: 80 Color: 16
Size: 32 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6788 Color: 5
Size: 1084 Color: 0
Size: 208 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 13
Size: 748 Color: 8
Size: 144 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5768 Color: 8
Size: 1928 Color: 10
Size: 384 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 10
Size: 1192 Color: 19
Size: 64 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5192 Color: 19
Size: 2408 Color: 12
Size: 480 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5556 Color: 8
Size: 2108 Color: 4
Size: 416 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 8
Size: 1436 Color: 0
Size: 216 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 11
Size: 1112 Color: 15
Size: 128 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 12
Size: 1182 Color: 8
Size: 232 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5594 Color: 4
Size: 2074 Color: 0
Size: 412 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6427 Color: 13
Size: 1379 Color: 17
Size: 274 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6583 Color: 14
Size: 1249 Color: 13
Size: 248 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6218 Color: 19
Size: 1554 Color: 14
Size: 308 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 19
Size: 2444 Color: 5
Size: 480 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 1
Size: 3348 Color: 0
Size: 664 Color: 19

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4042 Color: 13
Size: 3366 Color: 16
Size: 672 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4642 Color: 17
Size: 2866 Color: 16
Size: 572 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5154 Color: 14
Size: 2442 Color: 2
Size: 484 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 3
Size: 900 Color: 6
Size: 176 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 8
Size: 1484 Color: 13
Size: 288 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6568 Color: 10
Size: 1272 Color: 16
Size: 240 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6551 Color: 1
Size: 1275 Color: 19
Size: 254 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4056 Color: 19
Size: 3144 Color: 12
Size: 880 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4267 Color: 15
Size: 3179 Color: 15
Size: 634 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4744 Color: 0
Size: 2792 Color: 15
Size: 544 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5160 Color: 3
Size: 2792 Color: 11
Size: 128 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5624 Color: 15
Size: 2056 Color: 2
Size: 400 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 11
Size: 1419 Color: 9
Size: 282 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 10
Size: 820 Color: 12
Size: 152 Color: 16

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6056 Color: 10
Size: 1688 Color: 16
Size: 336 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4676 Color: 0
Size: 2844 Color: 1
Size: 560 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 6
Size: 3332 Color: 19
Size: 664 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5124 Color: 9
Size: 2468 Color: 8
Size: 488 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 5
Size: 1404 Color: 14
Size: 280 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5577 Color: 12
Size: 2087 Color: 18
Size: 416 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6825 Color: 3
Size: 1047 Color: 4
Size: 208 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6365 Color: 19
Size: 1431 Color: 0
Size: 284 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5483 Color: 9
Size: 2165 Color: 9
Size: 432 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7272 Color: 11
Size: 680 Color: 8
Size: 128 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 15
Size: 2132 Color: 7
Size: 424 Color: 17

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7238 Color: 15
Size: 702 Color: 6
Size: 140 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6239 Color: 7
Size: 1535 Color: 6
Size: 306 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6486 Color: 8
Size: 1330 Color: 19
Size: 264 Color: 6

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7075 Color: 18
Size: 839 Color: 14
Size: 166 Color: 7

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6092 Color: 0
Size: 1660 Color: 13
Size: 328 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6255 Color: 0
Size: 1521 Color: 18
Size: 304 Color: 18

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 4658 Color: 8
Size: 2854 Color: 0
Size: 568 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 7088 Color: 15
Size: 848 Color: 3
Size: 144 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5950 Color: 17
Size: 1778 Color: 10
Size: 352 Color: 7

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 4
Size: 1876 Color: 15
Size: 368 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 7210 Color: 19
Size: 726 Color: 6
Size: 144 Color: 12

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 4
Size: 1142 Color: 2
Size: 228 Color: 5

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 4041 Color: 3
Size: 3367 Color: 15
Size: 672 Color: 18

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 6
Size: 1542 Color: 8
Size: 304 Color: 14

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6894 Color: 7
Size: 990 Color: 19
Size: 196 Color: 11

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6747 Color: 16
Size: 1201 Color: 3
Size: 132 Color: 6

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 11
Size: 1391 Color: 3
Size: 278 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 7224 Color: 6
Size: 728 Color: 13
Size: 128 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5057 Color: 1
Size: 2521 Color: 11
Size: 502 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5610 Color: 8
Size: 2062 Color: 1
Size: 408 Color: 6

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 14
Size: 980 Color: 2
Size: 88 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5600 Color: 3
Size: 1856 Color: 14
Size: 624 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 4046 Color: 13
Size: 3362 Color: 10
Size: 672 Color: 6

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5448 Color: 2
Size: 2200 Color: 1
Size: 432 Color: 7

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 1
Size: 3364 Color: 17
Size: 664 Color: 18

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6909 Color: 18
Size: 1043 Color: 13
Size: 128 Color: 10

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 7041 Color: 15
Size: 867 Color: 8
Size: 172 Color: 7

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4833 Color: 19
Size: 2843 Color: 7
Size: 404 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 9
Size: 1852 Color: 9
Size: 368 Color: 11

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6124 Color: 2
Size: 1812 Color: 19
Size: 144 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6696 Color: 0
Size: 1160 Color: 2
Size: 224 Color: 18

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6910 Color: 14
Size: 978 Color: 13
Size: 192 Color: 6

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 18
Size: 1604 Color: 16
Size: 320 Color: 13

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 15
Size: 862 Color: 18
Size: 172 Color: 19

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7016 Color: 4
Size: 888 Color: 19
Size: 176 Color: 8

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6965 Color: 0
Size: 931 Color: 1
Size: 184 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 1
Size: 1324 Color: 6
Size: 264 Color: 12

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6943 Color: 11
Size: 949 Color: 4
Size: 188 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5959 Color: 19
Size: 1769 Color: 12
Size: 352 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 3
Size: 1436 Color: 11
Size: 280 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6376 Color: 4
Size: 1432 Color: 19
Size: 272 Color: 11

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 7025 Color: 15
Size: 881 Color: 18
Size: 174 Color: 19

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5751 Color: 4
Size: 1941 Color: 8
Size: 388 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 3
Size: 850 Color: 18
Size: 168 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 5735 Color: 19
Size: 1955 Color: 19
Size: 390 Color: 15

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 7158 Color: 1
Size: 770 Color: 2
Size: 152 Color: 17

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 13
Size: 1307 Color: 4
Size: 238 Color: 18

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 1
Size: 1400 Color: 18
Size: 256 Color: 7

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 4100 Color: 0
Size: 3324 Color: 2
Size: 656 Color: 9

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5100 Color: 2
Size: 2484 Color: 15
Size: 496 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6443 Color: 5
Size: 1365 Color: 16
Size: 272 Color: 19

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5142 Color: 15
Size: 2450 Color: 12
Size: 488 Color: 12

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 7080 Color: 17
Size: 840 Color: 14
Size: 160 Color: 12

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 4455 Color: 13
Size: 3021 Color: 2
Size: 604 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 14
Size: 1727 Color: 19
Size: 344 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 1
Size: 690 Color: 11
Size: 136 Color: 16

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5217 Color: 14
Size: 2387 Color: 1
Size: 476 Color: 6

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 9
Size: 886 Color: 2
Size: 176 Color: 5

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6878 Color: 19
Size: 1006 Color: 2
Size: 196 Color: 12

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 4
Size: 1300 Color: 19
Size: 256 Color: 19

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 14
Size: 1358 Color: 8
Size: 268 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 7
Size: 1130 Color: 6
Size: 224 Color: 11

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 7023 Color: 16
Size: 881 Color: 3
Size: 176 Color: 14

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 4043 Color: 9
Size: 3771 Color: 1
Size: 266 Color: 12

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 4888 Color: 15
Size: 2664 Color: 13
Size: 528 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 7030 Color: 12
Size: 878 Color: 9
Size: 172 Color: 13

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 7
Size: 1591 Color: 8
Size: 316 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6168 Color: 2
Size: 1608 Color: 2
Size: 304 Color: 11

Bin 111: 0 of cap free
Amount of items: 4
Items: 
Size: 5072 Color: 2
Size: 2736 Color: 4
Size: 208 Color: 13
Size: 64 Color: 15

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6678 Color: 19
Size: 1386 Color: 11
Size: 16 Color: 13

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6470 Color: 8
Size: 1342 Color: 15
Size: 268 Color: 9

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5934 Color: 16
Size: 1790 Color: 11
Size: 356 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5186 Color: 19
Size: 2414 Color: 18
Size: 480 Color: 7

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 5161 Color: 17
Size: 2433 Color: 14
Size: 486 Color: 6

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 6
Size: 1092 Color: 19
Size: 216 Color: 9

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 4644 Color: 11
Size: 2868 Color: 6
Size: 568 Color: 8

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 14
Size: 952 Color: 12
Size: 176 Color: 12

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 5928 Color: 14
Size: 1800 Color: 14
Size: 352 Color: 12

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4044 Color: 3
Size: 3364 Color: 6
Size: 672 Color: 7

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5113 Color: 13
Size: 2473 Color: 9
Size: 494 Color: 19

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6001 Color: 15
Size: 1733 Color: 18
Size: 346 Color: 13

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6803 Color: 18
Size: 1065 Color: 19
Size: 212 Color: 18

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 4721 Color: 11
Size: 2801 Color: 18
Size: 558 Color: 12

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6697 Color: 13
Size: 1153 Color: 8
Size: 230 Color: 13

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 19
Size: 1061 Color: 4
Size: 210 Color: 9

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 8
Size: 1396 Color: 16
Size: 16 Color: 16

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 6681 Color: 4
Size: 1201 Color: 1
Size: 198 Color: 4

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 18
Size: 933 Color: 3
Size: 186 Color: 16

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 5
Size: 1147 Color: 8
Size: 228 Color: 10

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 5170 Color: 5
Size: 2426 Color: 4
Size: 484 Color: 3

Total size: 1066560
Total free space: 0

