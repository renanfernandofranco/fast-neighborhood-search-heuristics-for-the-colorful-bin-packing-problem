Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1238 Color: 142
Size: 874 Color: 127
Size: 272 Color: 74
Size: 64 Color: 24
Size: 8 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1688 Color: 163
Size: 576 Color: 107
Size: 104 Color: 44
Size: 48 Color: 13
Size: 40 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 145
Size: 1028 Color: 137
Size: 32 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 199
Size: 204 Color: 68
Size: 72 Color: 25

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1444 Color: 149
Size: 840 Color: 123
Size: 172 Color: 62

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 198
Size: 254 Color: 73
Size: 32 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1804 Color: 169
Size: 380 Color: 84
Size: 272 Color: 75

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 138
Size: 1023 Color: 136
Size: 204 Color: 66

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 195
Size: 294 Color: 77
Size: 56 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 154
Size: 750 Color: 119
Size: 148 Color: 55

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 200
Size: 220 Color: 70
Size: 40 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 167
Size: 571 Color: 106
Size: 112 Color: 46

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1443 Color: 148
Size: 845 Color: 125
Size: 168 Color: 59

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 1410 Color: 146
Size: 1022 Color: 135
Size: 16 Color: 4
Size: 8 Color: 2

Bin 15: 0 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 139
Size: 1018 Color: 132
Size: 120 Color: 49
Size: 88 Color: 33

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2052 Color: 190
Size: 340 Color: 81
Size: 64 Color: 22

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 150
Size: 919 Color: 129
Size: 16 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 168
Size: 618 Color: 110
Size: 40 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 156
Size: 724 Color: 116
Size: 144 Color: 54

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 191
Size: 338 Color: 80
Size: 64 Color: 23

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 201
Size: 206 Color: 69
Size: 40 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2004 Color: 188
Size: 284 Color: 76
Size: 168 Color: 60

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 157
Size: 814 Color: 122
Size: 52 Color: 15

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 984 Color: 131
Size: 844 Color: 124
Size: 628 Color: 111

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1923 Color: 180
Size: 445 Color: 93
Size: 88 Color: 32

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1874 Color: 174
Size: 486 Color: 98
Size: 96 Color: 38

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 175
Size: 484 Color: 97
Size: 88 Color: 34

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 141
Size: 1020 Color: 133
Size: 200 Color: 64

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1948 Color: 183
Size: 428 Color: 90
Size: 80 Color: 29

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 147
Size: 846 Color: 126
Size: 168 Color: 61

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 170
Size: 538 Color: 104
Size: 104 Color: 45

Bin 32: 0 of cap free
Amount of items: 4
Items: 
Size: 1536 Color: 151
Size: 504 Color: 99
Size: 384 Color: 86
Size: 32 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 176
Size: 530 Color: 103
Size: 32 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 197
Size: 250 Color: 72
Size: 48 Color: 14

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 162
Size: 647 Color: 112
Size: 128 Color: 50

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 165
Size: 614 Color: 109
Size: 120 Color: 48

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 140
Size: 1021 Color: 134
Size: 204 Color: 67

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 196
Size: 236 Color: 71
Size: 96 Color: 40

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1708 Color: 164
Size: 548 Color: 105
Size: 200 Color: 65

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2092 Color: 194
Size: 308 Color: 79
Size: 56 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 189
Size: 378 Color: 83
Size: 72 Color: 26

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 193
Size: 306 Color: 78
Size: 60 Color: 21

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 153
Size: 753 Color: 120
Size: 150 Color: 58

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 143
Size: 891 Color: 128
Size: 176 Color: 63

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 160
Size: 661 Color: 113
Size: 130 Color: 51

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 159
Size: 665 Color: 114
Size: 132 Color: 52

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 181
Size: 438 Color: 92
Size: 84 Color: 31

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 172
Size: 511 Color: 101
Size: 102 Color: 43

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1967 Color: 184
Size: 409 Color: 88
Size: 80 Color: 28

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 186
Size: 406 Color: 87
Size: 52 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 158
Size: 690 Color: 115
Size: 136 Color: 53

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 182
Size: 430 Color: 91
Size: 84 Color: 30

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1907 Color: 178
Size: 459 Color: 95
Size: 90 Color: 36

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 166
Size: 577 Color: 108
Size: 114 Color: 47

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1391 Color: 144
Size: 969 Color: 130
Size: 96 Color: 39

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 161
Size: 731 Color: 117
Size: 52 Color: 16

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 155
Size: 747 Color: 118
Size: 148 Color: 56

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 2074 Color: 192
Size: 374 Color: 82
Size: 8 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1545 Color: 152
Size: 761 Color: 121
Size: 150 Color: 57

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 187
Size: 381 Color: 85
Size: 74 Color: 27

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 185
Size: 414 Color: 89
Size: 60 Color: 20

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1851 Color: 173
Size: 505 Color: 100
Size: 100 Color: 41

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1835 Color: 171
Size: 519 Color: 102
Size: 102 Color: 42

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1915 Color: 179
Size: 451 Color: 94
Size: 90 Color: 35

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1901 Color: 177
Size: 463 Color: 96
Size: 92 Color: 37

Total size: 159640
Total free space: 0

