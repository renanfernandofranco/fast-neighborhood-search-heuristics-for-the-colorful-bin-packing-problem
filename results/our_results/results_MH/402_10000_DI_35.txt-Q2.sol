Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 4070 Color: 1
Size: 932 Color: 0
Size: 790 Color: 0
Size: 710 Color: 0
Size: 672 Color: 1
Size: 596 Color: 1
Size: 230 Color: 1
Size: 136 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 1
Size: 3388 Color: 0
Size: 456 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4965 Color: 0
Size: 2503 Color: 1
Size: 668 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 1
Size: 2568 Color: 1
Size: 348 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5406 Color: 1
Size: 2562 Color: 0
Size: 168 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 1
Size: 2260 Color: 1
Size: 208 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 0
Size: 1495 Color: 1
Size: 374 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6251 Color: 1
Size: 1687 Color: 0
Size: 198 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 0
Size: 1811 Color: 0
Size: 42 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6286 Color: 0
Size: 1510 Color: 1
Size: 340 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6471 Color: 1
Size: 1499 Color: 1
Size: 166 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 0
Size: 1202 Color: 0
Size: 360 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 1
Size: 1358 Color: 0
Size: 172 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6579 Color: 0
Size: 1357 Color: 0
Size: 200 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 1
Size: 1276 Color: 0
Size: 208 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6670 Color: 0
Size: 1244 Color: 0
Size: 222 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 0
Size: 1059 Color: 1
Size: 318 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6846 Color: 0
Size: 1110 Color: 0
Size: 180 Color: 1

Bin 19: 0 of cap free
Amount of items: 4
Items: 
Size: 6916 Color: 0
Size: 1124 Color: 1
Size: 78 Color: 0
Size: 18 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7030 Color: 0
Size: 764 Color: 1
Size: 342 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7042 Color: 0
Size: 676 Color: 1
Size: 418 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7122 Color: 0
Size: 722 Color: 1
Size: 292 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 1
Size: 804 Color: 0
Size: 184 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7162 Color: 0
Size: 512 Color: 1
Size: 462 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7220 Color: 0
Size: 580 Color: 1
Size: 336 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 0
Size: 748 Color: 1
Size: 144 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7198 Color: 1
Size: 528 Color: 0
Size: 410 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7246 Color: 0
Size: 774 Color: 0
Size: 116 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7260 Color: 0
Size: 480 Color: 1
Size: 396 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 5371 Color: 0
Size: 2550 Color: 0
Size: 214 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 1
Size: 2091 Color: 0
Size: 224 Color: 1

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 5830 Color: 1
Size: 2305 Color: 0

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 6113 Color: 1
Size: 2022 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 6227 Color: 1
Size: 1520 Color: 1
Size: 388 Color: 0

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 6326 Color: 1
Size: 1809 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6513 Color: 1
Size: 982 Color: 0
Size: 640 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6719 Color: 1
Size: 1008 Color: 0
Size: 408 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6799 Color: 0
Size: 728 Color: 1
Size: 608 Color: 1

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6836 Color: 1
Size: 1299 Color: 0

Bin 40: 2 of cap free
Amount of items: 19
Items: 
Size: 532 Color: 0
Size: 530 Color: 0
Size: 512 Color: 0
Size: 496 Color: 0
Size: 460 Color: 1
Size: 456 Color: 0
Size: 452 Color: 1
Size: 448 Color: 1
Size: 448 Color: 0
Size: 444 Color: 1
Size: 440 Color: 1
Size: 422 Color: 1
Size: 414 Color: 1
Size: 412 Color: 0
Size: 408 Color: 1
Size: 392 Color: 1
Size: 384 Color: 0
Size: 300 Color: 0
Size: 184 Color: 1

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 5244 Color: 0
Size: 2890 Color: 1

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 7082 Color: 1
Size: 1052 Color: 0

Bin 43: 3 of cap free
Amount of items: 10
Items: 
Size: 4069 Color: 1
Size: 656 Color: 0
Size: 576 Color: 0
Size: 536 Color: 0
Size: 532 Color: 0
Size: 520 Color: 1
Size: 508 Color: 1
Size: 480 Color: 1
Size: 216 Color: 0
Size: 40 Color: 1

Bin 44: 3 of cap free
Amount of items: 2
Items: 
Size: 4557 Color: 1
Size: 3576 Color: 0

Bin 45: 3 of cap free
Amount of items: 4
Items: 
Size: 5355 Color: 1
Size: 2566 Color: 0
Size: 192 Color: 0
Size: 20 Color: 1

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 5750 Color: 1
Size: 2079 Color: 0
Size: 304 Color: 0

Bin 47: 3 of cap free
Amount of items: 2
Items: 
Size: 5903 Color: 1
Size: 2230 Color: 0

Bin 48: 3 of cap free
Amount of items: 2
Items: 
Size: 6073 Color: 1
Size: 2060 Color: 0

Bin 49: 3 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 1
Size: 993 Color: 0

Bin 50: 4 of cap free
Amount of items: 5
Items: 
Size: 4071 Color: 1
Size: 1272 Color: 0
Size: 1047 Color: 1
Size: 1010 Color: 0
Size: 732 Color: 1

Bin 51: 4 of cap free
Amount of items: 2
Items: 
Size: 6411 Color: 1
Size: 1721 Color: 0

Bin 52: 4 of cap free
Amount of items: 2
Items: 
Size: 6654 Color: 0
Size: 1478 Color: 1

Bin 53: 4 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 1
Size: 1042 Color: 0

Bin 54: 5 of cap free
Amount of items: 3
Items: 
Size: 5388 Color: 1
Size: 2059 Color: 0
Size: 684 Color: 1

Bin 55: 5 of cap free
Amount of items: 2
Items: 
Size: 5462 Color: 0
Size: 2669 Color: 1

Bin 56: 5 of cap free
Amount of items: 3
Items: 
Size: 6046 Color: 1
Size: 1875 Color: 0
Size: 210 Color: 0

Bin 57: 5 of cap free
Amount of items: 2
Items: 
Size: 6086 Color: 0
Size: 2045 Color: 1

Bin 58: 6 of cap free
Amount of items: 4
Items: 
Size: 4476 Color: 0
Size: 3382 Color: 1
Size: 156 Color: 1
Size: 116 Color: 0

Bin 59: 6 of cap free
Amount of items: 3
Items: 
Size: 4935 Color: 1
Size: 2983 Color: 0
Size: 212 Color: 1

Bin 60: 6 of cap free
Amount of items: 2
Items: 
Size: 5078 Color: 1
Size: 3052 Color: 0

Bin 61: 6 of cap free
Amount of items: 2
Items: 
Size: 5446 Color: 1
Size: 2684 Color: 0

Bin 62: 6 of cap free
Amount of items: 3
Items: 
Size: 6542 Color: 1
Size: 924 Color: 0
Size: 664 Color: 1

Bin 63: 6 of cap free
Amount of items: 2
Items: 
Size: 7270 Color: 0
Size: 860 Color: 1

Bin 64: 6 of cap free
Amount of items: 2
Items: 
Size: 7302 Color: 0
Size: 828 Color: 1

Bin 65: 7 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 1
Size: 1861 Color: 0

Bin 66: 7 of cap free
Amount of items: 2
Items: 
Size: 6876 Color: 1
Size: 1253 Color: 0

Bin 67: 7 of cap free
Amount of items: 2
Items: 
Size: 6926 Color: 1
Size: 1203 Color: 0

Bin 68: 8 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 1
Size: 2900 Color: 0
Size: 200 Color: 0

Bin 69: 9 of cap free
Amount of items: 3
Items: 
Size: 4949 Color: 1
Size: 2910 Color: 0
Size: 268 Color: 1

Bin 70: 9 of cap free
Amount of items: 3
Items: 
Size: 5289 Color: 1
Size: 1964 Color: 0
Size: 874 Color: 0

Bin 71: 9 of cap free
Amount of items: 2
Items: 
Size: 6435 Color: 0
Size: 1692 Color: 1

Bin 72: 10 of cap free
Amount of items: 3
Items: 
Size: 4086 Color: 0
Size: 3342 Color: 0
Size: 698 Color: 1

Bin 73: 10 of cap free
Amount of items: 4
Items: 
Size: 4916 Color: 1
Size: 1682 Color: 0
Size: 1356 Color: 0
Size: 172 Color: 1

Bin 74: 10 of cap free
Amount of items: 3
Items: 
Size: 6886 Color: 0
Size: 1120 Color: 1
Size: 120 Color: 1

Bin 75: 10 of cap free
Amount of items: 2
Items: 
Size: 7011 Color: 1
Size: 1115 Color: 0

Bin 76: 11 of cap free
Amount of items: 3
Items: 
Size: 5643 Color: 1
Size: 1922 Color: 0
Size: 560 Color: 0

Bin 77: 11 of cap free
Amount of items: 2
Items: 
Size: 5806 Color: 0
Size: 2319 Color: 1

Bin 78: 11 of cap free
Amount of items: 3
Items: 
Size: 6071 Color: 0
Size: 1710 Color: 1
Size: 344 Color: 1

Bin 79: 11 of cap free
Amount of items: 2
Items: 
Size: 6887 Color: 0
Size: 1238 Color: 1

Bin 80: 12 of cap free
Amount of items: 3
Items: 
Size: 4126 Color: 1
Size: 3322 Color: 0
Size: 676 Color: 1

Bin 81: 14 of cap free
Amount of items: 2
Items: 
Size: 6380 Color: 0
Size: 1742 Color: 1

Bin 82: 15 of cap free
Amount of items: 2
Items: 
Size: 5478 Color: 1
Size: 2643 Color: 0

Bin 83: 16 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 0
Size: 1660 Color: 1

Bin 84: 17 of cap free
Amount of items: 2
Items: 
Size: 6766 Color: 0
Size: 1353 Color: 1

Bin 85: 18 of cap free
Amount of items: 2
Items: 
Size: 6969 Color: 1
Size: 1149 Color: 0

Bin 86: 21 of cap free
Amount of items: 3
Items: 
Size: 5058 Color: 0
Size: 1559 Color: 1
Size: 1498 Color: 1

Bin 87: 21 of cap free
Amount of items: 2
Items: 
Size: 5118 Color: 0
Size: 2997 Color: 1

Bin 88: 22 of cap free
Amount of items: 3
Items: 
Size: 4150 Color: 0
Size: 3204 Color: 1
Size: 760 Color: 0

Bin 89: 22 of cap free
Amount of items: 2
Items: 
Size: 5822 Color: 0
Size: 2292 Color: 1

Bin 90: 22 of cap free
Amount of items: 2
Items: 
Size: 6867 Color: 1
Size: 1247 Color: 0

Bin 91: 22 of cap free
Amount of items: 2
Items: 
Size: 7232 Color: 1
Size: 882 Color: 0

Bin 92: 23 of cap free
Amount of items: 2
Items: 
Size: 6694 Color: 1
Size: 1419 Color: 0

Bin 93: 24 of cap free
Amount of items: 2
Items: 
Size: 5072 Color: 1
Size: 3040 Color: 0

Bin 94: 24 of cap free
Amount of items: 2
Items: 
Size: 7190 Color: 0
Size: 922 Color: 1

Bin 95: 26 of cap free
Amount of items: 2
Items: 
Size: 6962 Color: 1
Size: 1148 Color: 0

Bin 96: 26 of cap free
Amount of items: 2
Items: 
Size: 7264 Color: 0
Size: 846 Color: 1

Bin 97: 28 of cap free
Amount of items: 2
Items: 
Size: 6470 Color: 1
Size: 1638 Color: 0

Bin 98: 28 of cap free
Amount of items: 2
Items: 
Size: 6806 Color: 0
Size: 1302 Color: 1

Bin 99: 29 of cap free
Amount of items: 2
Items: 
Size: 6516 Color: 0
Size: 1591 Color: 1

Bin 100: 29 of cap free
Amount of items: 2
Items: 
Size: 6695 Color: 0
Size: 1412 Color: 1

Bin 101: 30 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 0
Size: 1958 Color: 1

Bin 102: 30 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 1
Size: 1390 Color: 0
Size: 104 Color: 1

Bin 103: 30 of cap free
Amount of items: 2
Items: 
Size: 7028 Color: 1
Size: 1078 Color: 0

Bin 104: 31 of cap free
Amount of items: 2
Items: 
Size: 5887 Color: 1
Size: 2218 Color: 0

Bin 105: 32 of cap free
Amount of items: 2
Items: 
Size: 7020 Color: 0
Size: 1084 Color: 1

Bin 106: 34 of cap free
Amount of items: 2
Items: 
Size: 4078 Color: 0
Size: 4024 Color: 1

Bin 107: 35 of cap free
Amount of items: 2
Items: 
Size: 6633 Color: 1
Size: 1468 Color: 0

Bin 108: 36 of cap free
Amount of items: 2
Items: 
Size: 7286 Color: 1
Size: 814 Color: 0

Bin 109: 38 of cap free
Amount of items: 29
Items: 
Size: 388 Color: 1
Size: 384 Color: 1
Size: 372 Color: 0
Size: 344 Color: 0
Size: 336 Color: 1
Size: 336 Color: 0
Size: 328 Color: 1
Size: 314 Color: 0
Size: 310 Color: 1
Size: 296 Color: 0
Size: 288 Color: 1
Size: 282 Color: 1
Size: 276 Color: 0
Size: 270 Color: 0
Size: 264 Color: 1
Size: 264 Color: 1
Size: 260 Color: 0
Size: 258 Color: 1
Size: 252 Color: 0
Size: 250 Color: 1
Size: 248 Color: 1
Size: 244 Color: 1
Size: 240 Color: 0
Size: 240 Color: 0
Size: 230 Color: 0
Size: 228 Color: 1
Size: 220 Color: 1
Size: 216 Color: 0
Size: 160 Color: 0

Bin 110: 38 of cap free
Amount of items: 2
Items: 
Size: 6108 Color: 1
Size: 1990 Color: 0

Bin 111: 38 of cap free
Amount of items: 2
Items: 
Size: 7316 Color: 0
Size: 782 Color: 1

Bin 112: 41 of cap free
Amount of items: 2
Items: 
Size: 5683 Color: 0
Size: 2412 Color: 1

Bin 113: 42 of cap free
Amount of items: 3
Items: 
Size: 5667 Color: 0
Size: 2347 Color: 1
Size: 80 Color: 1

Bin 114: 42 of cap free
Amount of items: 2
Items: 
Size: 7180 Color: 0
Size: 914 Color: 1

Bin 115: 49 of cap free
Amount of items: 6
Items: 
Size: 4076 Color: 0
Size: 1041 Color: 0
Size: 1020 Color: 0
Size: 676 Color: 1
Size: 676 Color: 1
Size: 598 Color: 1

Bin 116: 49 of cap free
Amount of items: 2
Items: 
Size: 6945 Color: 0
Size: 1142 Color: 1

Bin 117: 52 of cap free
Amount of items: 2
Items: 
Size: 6520 Color: 1
Size: 1564 Color: 0

Bin 118: 55 of cap free
Amount of items: 2
Items: 
Size: 6510 Color: 0
Size: 1571 Color: 1

Bin 119: 59 of cap free
Amount of items: 2
Items: 
Size: 4686 Color: 1
Size: 3391 Color: 0

Bin 120: 62 of cap free
Amount of items: 2
Items: 
Size: 6796 Color: 1
Size: 1278 Color: 0

Bin 121: 68 of cap free
Amount of items: 2
Items: 
Size: 5790 Color: 0
Size: 2278 Color: 1

Bin 122: 73 of cap free
Amount of items: 2
Items: 
Size: 5627 Color: 1
Size: 2436 Color: 0

Bin 123: 73 of cap free
Amount of items: 2
Items: 
Size: 6342 Color: 1
Size: 1721 Color: 0

Bin 124: 76 of cap free
Amount of items: 2
Items: 
Size: 6118 Color: 0
Size: 1942 Color: 1

Bin 125: 77 of cap free
Amount of items: 2
Items: 
Size: 4541 Color: 0
Size: 3518 Color: 1

Bin 126: 78 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 1
Size: 2278 Color: 0

Bin 127: 86 of cap free
Amount of items: 2
Items: 
Size: 4660 Color: 1
Size: 3390 Color: 0

Bin 128: 98 of cap free
Amount of items: 2
Items: 
Size: 6764 Color: 0
Size: 1274 Color: 1

Bin 129: 101 of cap free
Amount of items: 2
Items: 
Size: 4646 Color: 1
Size: 3389 Color: 0

Bin 130: 107 of cap free
Amount of items: 2
Items: 
Size: 6097 Color: 0
Size: 1932 Color: 1

Bin 131: 112 of cap free
Amount of items: 2
Items: 
Size: 5367 Color: 0
Size: 2657 Color: 1

Bin 132: 112 of cap free
Amount of items: 2
Items: 
Size: 5428 Color: 1
Size: 2596 Color: 0

Bin 133: 5538 of cap free
Amount of items: 16
Items: 
Size: 212 Color: 1
Size: 208 Color: 1
Size: 194 Color: 1
Size: 192 Color: 1
Size: 184 Color: 1
Size: 160 Color: 0
Size: 156 Color: 0
Size: 152 Color: 1
Size: 152 Color: 0
Size: 144 Color: 1
Size: 144 Color: 1
Size: 144 Color: 0
Size: 144 Color: 0
Size: 140 Color: 0
Size: 136 Color: 0
Size: 136 Color: 0

Total size: 1073952
Total free space: 8136

