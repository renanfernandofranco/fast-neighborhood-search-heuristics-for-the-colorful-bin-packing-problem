Capicity Bin: 7928
Lower Bound: 132

Bins used: 132
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 6320 Color: 3
Size: 616 Color: 4
Size: 504 Color: 2
Size: 384 Color: 0
Size: 104 Color: 4

Bin 2: 0 of cap free
Amount of items: 8
Items: 
Size: 3904 Color: 3
Size: 1208 Color: 3
Size: 1080 Color: 4
Size: 984 Color: 4
Size: 456 Color: 0
Size: 128 Color: 3
Size: 112 Color: 4
Size: 56 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5831 Color: 0
Size: 1833 Color: 2
Size: 264 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4917 Color: 2
Size: 2511 Color: 4
Size: 500 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6414 Color: 1
Size: 1262 Color: 4
Size: 252 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7100 Color: 2
Size: 692 Color: 2
Size: 136 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 3
Size: 1110 Color: 2
Size: 220 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6743 Color: 1
Size: 989 Color: 2
Size: 196 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6989 Color: 4
Size: 783 Color: 0
Size: 156 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 0
Size: 1436 Color: 4
Size: 32 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5591 Color: 1
Size: 1949 Color: 3
Size: 388 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 4
Size: 2004 Color: 3
Size: 400 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6399 Color: 4
Size: 1425 Color: 1
Size: 104 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6982 Color: 3
Size: 790 Color: 3
Size: 156 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6406 Color: 2
Size: 1310 Color: 0
Size: 212 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6847 Color: 2
Size: 901 Color: 1
Size: 180 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5263 Color: 4
Size: 2221 Color: 3
Size: 444 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6746 Color: 0
Size: 986 Color: 4
Size: 196 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6533 Color: 4
Size: 1163 Color: 2
Size: 232 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6383 Color: 2
Size: 1429 Color: 0
Size: 116 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6675 Color: 0
Size: 1045 Color: 1
Size: 208 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 0
Size: 1018 Color: 0
Size: 200 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5010 Color: 2
Size: 2434 Color: 0
Size: 484 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5766 Color: 0
Size: 1802 Color: 1
Size: 360 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 0
Size: 3156 Color: 3
Size: 624 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6190 Color: 4
Size: 1450 Color: 1
Size: 288 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6037 Color: 1
Size: 1577 Color: 0
Size: 314 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 3974 Color: 1
Size: 3366 Color: 4
Size: 588 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6896 Color: 1
Size: 872 Color: 2
Size: 160 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6951 Color: 4
Size: 815 Color: 2
Size: 162 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 2
Size: 2422 Color: 0
Size: 480 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4636 Color: 3
Size: 2748 Color: 4
Size: 544 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7007 Color: 4
Size: 769 Color: 0
Size: 152 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 1
Size: 948 Color: 3
Size: 184 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6991 Color: 2
Size: 781 Color: 0
Size: 156 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5587 Color: 4
Size: 1951 Color: 1
Size: 390 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 4
Size: 1796 Color: 2
Size: 352 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7058 Color: 4
Size: 758 Color: 0
Size: 112 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 4
Size: 852 Color: 2
Size: 168 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6496 Color: 2
Size: 1208 Color: 3
Size: 224 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6845 Color: 0
Size: 903 Color: 0
Size: 180 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5188 Color: 0
Size: 2284 Color: 2
Size: 456 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5277 Color: 2
Size: 2211 Color: 2
Size: 440 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6917 Color: 0
Size: 855 Color: 4
Size: 156 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 4036 Color: 2
Size: 3244 Color: 2
Size: 648 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 2
Size: 702 Color: 0
Size: 136 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6770 Color: 0
Size: 966 Color: 0
Size: 192 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5686 Color: 0
Size: 1870 Color: 0
Size: 372 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 4
Size: 1414 Color: 3
Size: 280 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6659 Color: 0
Size: 1059 Color: 3
Size: 210 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 7126 Color: 0
Size: 670 Color: 2
Size: 132 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 1
Size: 3052 Color: 0
Size: 224 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 4
Size: 738 Color: 0
Size: 144 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6804 Color: 2
Size: 940 Color: 3
Size: 184 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6476 Color: 0
Size: 1212 Color: 0
Size: 240 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6243 Color: 3
Size: 1405 Color: 4
Size: 280 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4477 Color: 3
Size: 2877 Color: 3
Size: 574 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6614 Color: 0
Size: 1098 Color: 1
Size: 216 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6842 Color: 0
Size: 906 Color: 2
Size: 180 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 0
Size: 994 Color: 4
Size: 196 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 7118 Color: 0
Size: 678 Color: 0
Size: 132 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5148 Color: 1
Size: 2324 Color: 4
Size: 456 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6636 Color: 1
Size: 1084 Color: 0
Size: 208 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6404 Color: 0
Size: 1276 Color: 1
Size: 248 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5269 Color: 3
Size: 2217 Color: 3
Size: 442 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4092 Color: 0
Size: 3204 Color: 3
Size: 632 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6973 Color: 1
Size: 797 Color: 2
Size: 158 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 0
Size: 822 Color: 1
Size: 160 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6887 Color: 1
Size: 869 Color: 0
Size: 172 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 0
Size: 2364 Color: 0
Size: 96 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 4
Size: 1052 Color: 0
Size: 208 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6045 Color: 0
Size: 1571 Color: 2
Size: 312 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6042 Color: 4
Size: 1574 Color: 2
Size: 312 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6749 Color: 1
Size: 1023 Color: 3
Size: 156 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 4700 Color: 3
Size: 2692 Color: 0
Size: 536 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4903 Color: 3
Size: 2545 Color: 1
Size: 480 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 4
Size: 1822 Color: 2
Size: 364 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 2
Size: 1145 Color: 3
Size: 228 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6231 Color: 2
Size: 1415 Color: 2
Size: 282 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 4909 Color: 1
Size: 2517 Color: 0
Size: 502 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6431 Color: 1
Size: 1249 Color: 3
Size: 248 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6358 Color: 3
Size: 1310 Color: 1
Size: 260 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6558 Color: 1
Size: 1142 Color: 2
Size: 228 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5955 Color: 4
Size: 1645 Color: 4
Size: 328 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 4
Size: 2364 Color: 2
Size: 168 Color: 4

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 4756 Color: 3
Size: 2644 Color: 2
Size: 528 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4526 Color: 1
Size: 2838 Color: 3
Size: 564 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6663 Color: 4
Size: 1055 Color: 4
Size: 210 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 5620 Color: 2
Size: 2068 Color: 0
Size: 240 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 3
Size: 874 Color: 0
Size: 172 Color: 2

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 4812 Color: 0
Size: 2604 Color: 3
Size: 512 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 2
Size: 762 Color: 1
Size: 152 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 1
Size: 756 Color: 0
Size: 144 Color: 2

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 3
Size: 1380 Color: 1
Size: 272 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6430 Color: 1
Size: 1250 Color: 0
Size: 248 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6180 Color: 1
Size: 1460 Color: 4
Size: 288 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 3472 Color: 1
Size: 2936 Color: 2
Size: 1520 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6996 Color: 1
Size: 780 Color: 3
Size: 152 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 3972 Color: 2
Size: 3300 Color: 3
Size: 656 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 6006 Color: 3
Size: 1618 Color: 1
Size: 304 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 1
Size: 862 Color: 2
Size: 168 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 3990 Color: 2
Size: 3282 Color: 3
Size: 656 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6235 Color: 4
Size: 1411 Color: 3
Size: 282 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5386 Color: 2
Size: 2122 Color: 0
Size: 420 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5426 Color: 2
Size: 2086 Color: 2
Size: 416 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5018 Color: 4
Size: 2426 Color: 3
Size: 484 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 4
Size: 1838 Color: 4
Size: 364 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5884 Color: 0
Size: 1708 Color: 2
Size: 336 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6890 Color: 3
Size: 866 Color: 3
Size: 172 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 7005 Color: 1
Size: 771 Color: 2
Size: 152 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 4
Size: 1278 Color: 2
Size: 252 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5734 Color: 3
Size: 1862 Color: 1
Size: 332 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 4970 Color: 1
Size: 2466 Color: 2
Size: 492 Color: 4

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6044 Color: 3
Size: 1572 Color: 1
Size: 312 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4566 Color: 0
Size: 2802 Color: 4
Size: 560 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6375 Color: 1
Size: 1295 Color: 3
Size: 258 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6643 Color: 4
Size: 1071 Color: 2
Size: 214 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 5569 Color: 3
Size: 1967 Color: 1
Size: 392 Color: 4

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 3
Size: 3302 Color: 3
Size: 660 Color: 2

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6933 Color: 0
Size: 831 Color: 2
Size: 164 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6211 Color: 2
Size: 1431 Color: 3
Size: 286 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 4485 Color: 3
Size: 2871 Color: 4
Size: 572 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6203 Color: 4
Size: 1439 Color: 3
Size: 286 Color: 4

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 3967 Color: 4
Size: 3301 Color: 1
Size: 660 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6919 Color: 3
Size: 857 Color: 2
Size: 152 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6799 Color: 1
Size: 941 Color: 4
Size: 188 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6423 Color: 4
Size: 1255 Color: 4
Size: 250 Color: 3

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 5837 Color: 2
Size: 1743 Color: 3
Size: 348 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 6797 Color: 3
Size: 943 Color: 3
Size: 188 Color: 2

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 3965 Color: 0
Size: 3303 Color: 1
Size: 660 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6885 Color: 3
Size: 871 Color: 1
Size: 172 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6539 Color: 0
Size: 1159 Color: 1
Size: 230 Color: 2

Total size: 1046496
Total free space: 0

