Capicity Bin: 2472
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1596 Color: 0
Size: 428 Color: 1
Size: 384 Color: 0
Size: 64 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1000 Color: 1
Size: 840 Color: 1
Size: 576 Color: 0
Size: 48 Color: 0
Size: 8 Color: 1

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 2188 Color: 0
Size: 200 Color: 0
Size: 76 Color: 1
Size: 8 Color: 1

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1552 Color: 1
Size: 504 Color: 0
Size: 272 Color: 0
Size: 144 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 1
Size: 887 Color: 1
Size: 176 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1916 Color: 1
Size: 548 Color: 1
Size: 8 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 0
Size: 622 Color: 1
Size: 124 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 1704 Color: 0
Size: 732 Color: 1
Size: 20 Color: 0
Size: 16 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 0
Size: 647 Color: 0
Size: 52 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 0
Size: 581 Color: 0
Size: 116 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2007 Color: 0
Size: 401 Color: 0
Size: 64 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 1
Size: 956 Color: 0
Size: 272 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 0
Size: 662 Color: 0
Size: 132 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1906 Color: 1
Size: 474 Color: 1
Size: 92 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1879 Color: 0
Size: 495 Color: 0
Size: 98 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 1
Size: 230 Color: 1
Size: 44 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 1
Size: 252 Color: 0
Size: 112 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1971 Color: 0
Size: 419 Color: 1
Size: 82 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2057 Color: 1
Size: 397 Color: 0
Size: 18 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2127 Color: 1
Size: 289 Color: 1
Size: 56 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 0
Size: 885 Color: 1
Size: 176 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 1
Size: 482 Color: 1
Size: 92 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1476 Color: 0
Size: 836 Color: 0
Size: 160 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 1
Size: 567 Color: 0
Size: 112 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 0
Size: 402 Color: 0
Size: 76 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2026 Color: 0
Size: 374 Color: 0
Size: 72 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 667 Color: 1
Size: 132 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1951 Color: 1
Size: 435 Color: 0
Size: 86 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 1
Size: 767 Color: 0
Size: 152 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 1030 Color: 0
Size: 204 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 0
Size: 842 Color: 1
Size: 168 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 0
Size: 1028 Color: 1
Size: 40 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 0
Size: 1022 Color: 1
Size: 204 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2162 Color: 0
Size: 262 Color: 0
Size: 48 Color: 1

Bin 35: 0 of cap free
Amount of items: 4
Items: 
Size: 1964 Color: 1
Size: 324 Color: 1
Size: 96 Color: 0
Size: 88 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2084 Color: 0
Size: 308 Color: 0
Size: 80 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 1
Size: 459 Color: 0
Size: 34 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 0
Size: 244 Color: 1
Size: 56 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2097 Color: 0
Size: 313 Color: 0
Size: 62 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 1
Size: 389 Color: 1
Size: 32 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 1
Size: 1031 Color: 0
Size: 204 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 1
Size: 1018 Color: 1
Size: 200 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 0
Size: 878 Color: 0
Size: 172 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2004 Color: 0
Size: 396 Color: 1
Size: 72 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 0
Size: 343 Color: 1
Size: 68 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 0
Size: 702 Color: 1
Size: 52 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1700 Color: 0
Size: 644 Color: 0
Size: 128 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2142 Color: 1
Size: 278 Color: 0
Size: 52 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 1
Size: 1029 Color: 0
Size: 204 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2066 Color: 1
Size: 342 Color: 0
Size: 64 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 0
Size: 267 Color: 1
Size: 52 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1788 Color: 0
Size: 572 Color: 0
Size: 112 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 1
Size: 244 Color: 0
Size: 48 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 0
Size: 489 Color: 0
Size: 96 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1838 Color: 1
Size: 530 Color: 1
Size: 104 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 0
Size: 726 Color: 0
Size: 144 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 1
Size: 762 Color: 0
Size: 148 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 0
Size: 665 Color: 0
Size: 132 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 0
Size: 765 Color: 0
Size: 152 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 0
Size: 366 Color: 1
Size: 20 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1766 Color: 0
Size: 590 Color: 0
Size: 116 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1859 Color: 0
Size: 511 Color: 1
Size: 102 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1860 Color: 0
Size: 516 Color: 1
Size: 96 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 1
Size: 434 Color: 0
Size: 84 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 0
Size: 298 Color: 0
Size: 56 Color: 1

Total size: 160680
Total free space: 0

