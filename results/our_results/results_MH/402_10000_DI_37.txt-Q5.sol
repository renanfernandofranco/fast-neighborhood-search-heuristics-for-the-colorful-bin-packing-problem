Capicity Bin: 8080
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4258 Color: 0
Size: 3186 Color: 1
Size: 636 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4266 Color: 4
Size: 3364 Color: 2
Size: 450 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5420 Color: 4
Size: 2220 Color: 0
Size: 440 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5425 Color: 4
Size: 2213 Color: 0
Size: 442 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5909 Color: 0
Size: 1811 Color: 0
Size: 360 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5938 Color: 4
Size: 1786 Color: 3
Size: 356 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 4
Size: 2012 Color: 2
Size: 88 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5999 Color: 4
Size: 1571 Color: 1
Size: 510 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6188 Color: 4
Size: 1764 Color: 1
Size: 128 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6202 Color: 1
Size: 1558 Color: 4
Size: 320 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 0
Size: 1132 Color: 4
Size: 672 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 4
Size: 1580 Color: 3
Size: 136 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 4
Size: 1533 Color: 0
Size: 168 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 3
Size: 1420 Color: 2
Size: 280 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6485 Color: 4
Size: 1245 Color: 1
Size: 350 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6564 Color: 4
Size: 1268 Color: 0
Size: 248 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 3
Size: 1118 Color: 4
Size: 352 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 2
Size: 1240 Color: 1
Size: 220 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6676 Color: 4
Size: 1220 Color: 3
Size: 184 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6729 Color: 4
Size: 1071 Color: 3
Size: 280 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 2
Size: 684 Color: 0
Size: 672 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6797 Color: 4
Size: 971 Color: 2
Size: 312 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 4
Size: 1025 Color: 2
Size: 240 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 4
Size: 1002 Color: 2
Size: 258 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 4
Size: 822 Color: 1
Size: 376 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6890 Color: 4
Size: 684 Color: 3
Size: 506 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6893 Color: 4
Size: 1031 Color: 2
Size: 156 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 4
Size: 942 Color: 3
Size: 192 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 4
Size: 1044 Color: 1
Size: 48 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 2
Size: 724 Color: 2
Size: 416 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7013 Color: 4
Size: 891 Color: 0
Size: 176 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 4
Size: 672 Color: 1
Size: 364 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7038 Color: 2
Size: 528 Color: 1
Size: 514 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 3
Size: 754 Color: 2
Size: 280 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7094 Color: 2
Size: 544 Color: 4
Size: 442 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 2
Size: 762 Color: 4
Size: 216 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7178 Color: 3
Size: 702 Color: 2
Size: 200 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7220 Color: 0
Size: 636 Color: 2
Size: 224 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7226 Color: 4
Size: 582 Color: 1
Size: 272 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7242 Color: 1
Size: 590 Color: 4
Size: 248 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7268 Color: 3
Size: 636 Color: 1
Size: 176 Color: 4

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4533 Color: 4
Size: 3202 Color: 3
Size: 344 Color: 2

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 4557 Color: 4
Size: 3362 Color: 0
Size: 160 Color: 2

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5330 Color: 4
Size: 2537 Color: 3
Size: 212 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 2
Size: 2531 Color: 2
Size: 264 Color: 3

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 4
Size: 1949 Color: 3
Size: 256 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 1
Size: 1141 Color: 4
Size: 512 Color: 3

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6464 Color: 2
Size: 1271 Color: 0
Size: 344 Color: 4

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6531 Color: 4
Size: 1436 Color: 2
Size: 112 Color: 2

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6587 Color: 2
Size: 1052 Color: 0
Size: 440 Color: 4

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6600 Color: 1
Size: 1169 Color: 4
Size: 310 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6721 Color: 0
Size: 862 Color: 1
Size: 496 Color: 4

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6917 Color: 1
Size: 706 Color: 4
Size: 456 Color: 3

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 4802 Color: 0
Size: 3004 Color: 2
Size: 272 Color: 0

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5693 Color: 3
Size: 2241 Color: 1
Size: 144 Color: 3

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 6007 Color: 0
Size: 1775 Color: 1
Size: 296 Color: 4

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 6195 Color: 2
Size: 1735 Color: 4
Size: 148 Color: 1

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6262 Color: 3
Size: 1102 Color: 4
Size: 714 Color: 1

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 7260 Color: 1
Size: 818 Color: 3

Bin 60: 3 of cap free
Amount of items: 13
Items: 
Size: 965 Color: 4
Size: 956 Color: 4
Size: 946 Color: 4
Size: 868 Color: 2
Size: 672 Color: 4
Size: 648 Color: 0
Size: 586 Color: 4
Size: 504 Color: 0
Size: 496 Color: 1
Size: 432 Color: 2
Size: 424 Color: 1
Size: 376 Color: 1
Size: 204 Color: 1

Bin 61: 3 of cap free
Amount of items: 4
Items: 
Size: 4042 Color: 0
Size: 2557 Color: 0
Size: 1234 Color: 3
Size: 244 Color: 4

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 2
Size: 2937 Color: 3
Size: 136 Color: 4

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 5037 Color: 0
Size: 2900 Color: 4
Size: 140 Color: 3

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 5369 Color: 3
Size: 2572 Color: 0
Size: 136 Color: 4

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 4
Size: 1755 Color: 0
Size: 632 Color: 2

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 5946 Color: 2
Size: 1991 Color: 2
Size: 140 Color: 4

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 5975 Color: 0
Size: 1794 Color: 1
Size: 308 Color: 4

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 2
Size: 1551 Color: 3

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 6851 Color: 2
Size: 1226 Color: 1

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 4
Size: 3194 Color: 2
Size: 632 Color: 0

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 5013 Color: 1
Size: 2911 Color: 4
Size: 152 Color: 3

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 6219 Color: 2
Size: 1545 Color: 3
Size: 312 Color: 4

Bin 73: 4 of cap free
Amount of items: 4
Items: 
Size: 6579 Color: 2
Size: 1369 Color: 1
Size: 72 Color: 4
Size: 56 Color: 0

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 6618 Color: 0
Size: 1458 Color: 3

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 5572 Color: 0
Size: 2207 Color: 2
Size: 296 Color: 4

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 0
Size: 2294 Color: 1

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 6823 Color: 2
Size: 1251 Color: 1

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 7166 Color: 0
Size: 908 Color: 3

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 7204 Color: 1
Size: 870 Color: 2

Bin 80: 7 of cap free
Amount of items: 3
Items: 
Size: 4788 Color: 0
Size: 2917 Color: 1
Size: 368 Color: 2

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 4989 Color: 3
Size: 2738 Color: 2
Size: 346 Color: 3

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 3
Size: 2261 Color: 3
Size: 144 Color: 4

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 4044 Color: 1
Size: 3742 Color: 1
Size: 286 Color: 2

Bin 84: 9 of cap free
Amount of items: 3
Items: 
Size: 5045 Color: 3
Size: 2662 Color: 0
Size: 364 Color: 4

Bin 85: 9 of cap free
Amount of items: 2
Items: 
Size: 6938 Color: 0
Size: 1133 Color: 1

Bin 86: 10 of cap free
Amount of items: 3
Items: 
Size: 5417 Color: 4
Size: 2221 Color: 0
Size: 432 Color: 3

Bin 87: 10 of cap free
Amount of items: 2
Items: 
Size: 5890 Color: 0
Size: 2180 Color: 1

Bin 88: 10 of cap free
Amount of items: 2
Items: 
Size: 7234 Color: 2
Size: 836 Color: 0

Bin 89: 11 of cap free
Amount of items: 3
Items: 
Size: 5433 Color: 3
Size: 2532 Color: 4
Size: 104 Color: 0

Bin 90: 11 of cap free
Amount of items: 3
Items: 
Size: 6355 Color: 2
Size: 1650 Color: 3
Size: 64 Color: 1

Bin 91: 13 of cap free
Amount of items: 4
Items: 
Size: 4043 Color: 3
Size: 3182 Color: 0
Size: 592 Color: 2
Size: 250 Color: 3

Bin 92: 13 of cap free
Amount of items: 2
Items: 
Size: 6439 Color: 3
Size: 1628 Color: 0

Bin 93: 14 of cap free
Amount of items: 3
Items: 
Size: 4890 Color: 3
Size: 2922 Color: 4
Size: 254 Color: 1

Bin 94: 14 of cap free
Amount of items: 2
Items: 
Size: 6954 Color: 1
Size: 1112 Color: 3

Bin 95: 15 of cap free
Amount of items: 6
Items: 
Size: 4041 Color: 2
Size: 1222 Color: 0
Size: 1098 Color: 0
Size: 1032 Color: 2
Size: 504 Color: 1
Size: 168 Color: 4

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 6766 Color: 0
Size: 1298 Color: 3

Bin 97: 17 of cap free
Amount of items: 2
Items: 
Size: 6334 Color: 3
Size: 1729 Color: 1

Bin 98: 17 of cap free
Amount of items: 2
Items: 
Size: 6555 Color: 3
Size: 1508 Color: 2

Bin 99: 19 of cap free
Amount of items: 2
Items: 
Size: 5951 Color: 3
Size: 2110 Color: 1

Bin 100: 19 of cap free
Amount of items: 2
Items: 
Size: 6679 Color: 2
Size: 1382 Color: 0

Bin 101: 20 of cap free
Amount of items: 25
Items: 
Size: 544 Color: 4
Size: 544 Color: 4
Size: 512 Color: 4
Size: 416 Color: 2
Size: 400 Color: 4
Size: 396 Color: 2
Size: 388 Color: 2
Size: 356 Color: 4
Size: 356 Color: 2
Size: 354 Color: 0
Size: 344 Color: 4
Size: 318 Color: 2
Size: 314 Color: 2
Size: 308 Color: 0
Size: 288 Color: 1
Size: 244 Color: 4
Size: 240 Color: 2
Size: 240 Color: 1
Size: 232 Color: 0
Size: 226 Color: 3
Size: 226 Color: 1
Size: 210 Color: 1
Size: 208 Color: 3
Size: 208 Color: 3
Size: 188 Color: 1

Bin 102: 24 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 4
Size: 1756 Color: 0
Size: 464 Color: 2

Bin 103: 24 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 1
Size: 1924 Color: 0

Bin 104: 24 of cap free
Amount of items: 2
Items: 
Size: 6214 Color: 0
Size: 1842 Color: 3

Bin 105: 25 of cap free
Amount of items: 3
Items: 
Size: 5743 Color: 2
Size: 1580 Color: 4
Size: 732 Color: 0

Bin 106: 26 of cap free
Amount of items: 2
Items: 
Size: 4046 Color: 2
Size: 4008 Color: 1

Bin 107: 26 of cap free
Amount of items: 3
Items: 
Size: 4156 Color: 1
Size: 2332 Color: 3
Size: 1566 Color: 2

Bin 108: 26 of cap free
Amount of items: 2
Items: 
Size: 5972 Color: 1
Size: 2082 Color: 2

Bin 109: 27 of cap free
Amount of items: 2
Items: 
Size: 6227 Color: 3
Size: 1826 Color: 2

Bin 110: 28 of cap free
Amount of items: 2
Items: 
Size: 5044 Color: 1
Size: 3008 Color: 0

Bin 111: 29 of cap free
Amount of items: 2
Items: 
Size: 4684 Color: 2
Size: 3367 Color: 4

Bin 112: 29 of cap free
Amount of items: 2
Items: 
Size: 6996 Color: 3
Size: 1055 Color: 0

Bin 113: 30 of cap free
Amount of items: 2
Items: 
Size: 6923 Color: 2
Size: 1127 Color: 3

Bin 114: 31 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 0
Size: 1291 Color: 3

Bin 115: 32 of cap free
Amount of items: 2
Items: 
Size: 6876 Color: 2
Size: 1172 Color: 1

Bin 116: 33 of cap free
Amount of items: 2
Items: 
Size: 6171 Color: 2
Size: 1876 Color: 0

Bin 117: 35 of cap free
Amount of items: 2
Items: 
Size: 5468 Color: 0
Size: 2577 Color: 1

Bin 118: 36 of cap free
Amount of items: 2
Items: 
Size: 6713 Color: 0
Size: 1331 Color: 3

Bin 119: 39 of cap free
Amount of items: 2
Items: 
Size: 6602 Color: 0
Size: 1439 Color: 3

Bin 120: 42 of cap free
Amount of items: 2
Items: 
Size: 7084 Color: 2
Size: 954 Color: 1

Bin 121: 45 of cap free
Amount of items: 3
Items: 
Size: 4242 Color: 3
Size: 2744 Color: 3
Size: 1049 Color: 2

Bin 122: 47 of cap free
Amount of items: 3
Items: 
Size: 4589 Color: 3
Size: 3276 Color: 0
Size: 168 Color: 4

Bin 123: 50 of cap free
Amount of items: 4
Items: 
Size: 4798 Color: 3
Size: 1782 Color: 4
Size: 1004 Color: 4
Size: 446 Color: 2

Bin 124: 52 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 4
Size: 3204 Color: 3
Size: 580 Color: 1

Bin 125: 53 of cap free
Amount of items: 2
Items: 
Size: 6436 Color: 2
Size: 1591 Color: 0

Bin 126: 58 of cap free
Amount of items: 2
Items: 
Size: 5930 Color: 0
Size: 2092 Color: 1

Bin 127: 68 of cap free
Amount of items: 2
Items: 
Size: 4484 Color: 1
Size: 3528 Color: 4

Bin 128: 86 of cap free
Amount of items: 2
Items: 
Size: 5246 Color: 1
Size: 2748 Color: 0

Bin 129: 96 of cap free
Amount of items: 2
Items: 
Size: 5582 Color: 3
Size: 2402 Color: 0

Bin 130: 123 of cap free
Amount of items: 2
Items: 
Size: 5393 Color: 0
Size: 2564 Color: 1

Bin 131: 127 of cap free
Amount of items: 2
Items: 
Size: 4996 Color: 1
Size: 2957 Color: 3

Bin 132: 134 of cap free
Amount of items: 2
Items: 
Size: 4581 Color: 4
Size: 3365 Color: 3

Bin 133: 6196 of cap free
Amount of items: 10
Items: 
Size: 232 Color: 2
Size: 224 Color: 2
Size: 196 Color: 3
Size: 192 Color: 0
Size: 188 Color: 3
Size: 184 Color: 1
Size: 172 Color: 1
Size: 172 Color: 0
Size: 164 Color: 3
Size: 160 Color: 1

Total size: 1066560
Total free space: 8080

