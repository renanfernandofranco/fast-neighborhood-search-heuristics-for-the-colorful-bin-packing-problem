Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 102

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 70
Size: 348 Color: 66
Size: 296 Color: 29

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 90
Size: 338 Color: 56
Size: 250 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 81
Size: 362 Color: 75
Size: 263 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 96
Size: 291 Color: 26
Size: 266 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 93
Size: 316 Color: 42
Size: 263 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 58
Size: 335 Color: 54
Size: 326 Color: 51

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 97
Size: 279 Color: 18
Size: 267 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 98
Size: 279 Color: 19
Size: 263 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 88
Size: 339 Color: 57
Size: 265 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 74
Size: 344 Color: 62
Size: 296 Color: 28

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 71
Size: 323 Color: 48
Size: 320 Color: 44

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 87
Size: 303 Color: 34
Size: 302 Color: 33

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 82
Size: 306 Color: 37
Size: 314 Color: 41

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 77
Size: 343 Color: 60
Size: 289 Color: 24

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 95
Size: 307 Color: 38
Size: 254 Color: 5

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 78
Size: 323 Color: 49
Size: 305 Color: 36

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 84
Size: 349 Color: 67
Size: 264 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 92
Size: 322 Color: 46
Size: 262 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 99
Size: 284 Color: 22
Size: 252 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 86
Size: 333 Color: 53
Size: 277 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 101
Size: 258 Color: 6
Size: 250 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 69
Size: 347 Color: 64
Size: 301 Color: 31

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 59
Size: 337 Color: 55
Size: 323 Color: 47

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 100
Size: 284 Color: 21
Size: 252 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 85
Size: 361 Color: 73
Size: 251 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 89
Size: 304 Color: 35
Size: 286 Color: 23

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 76
Size: 321 Color: 45
Size: 317 Color: 43

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 91
Size: 311 Color: 40
Size: 277 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 68
Size: 343 Color: 61
Size: 308 Color: 39

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 72
Size: 347 Color: 65
Size: 295 Color: 27

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 79
Size: 325 Color: 50
Size: 301 Color: 32

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 80
Size: 345 Color: 63
Size: 280 Color: 20

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 94
Size: 301 Color: 30
Size: 278 Color: 17

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 83
Size: 326 Color: 52
Size: 290 Color: 25

Total size: 34034
Total free space: 0

