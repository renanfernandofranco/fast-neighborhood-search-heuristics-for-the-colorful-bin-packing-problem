Capicity Bin: 6240
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4792 Color: 1
Size: 1336 Color: 1
Size: 112 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 4920 Color: 1
Size: 936 Color: 1
Size: 224 Color: 1
Size: 80 Color: 0
Size: 80 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 5192 Color: 1
Size: 952 Color: 1
Size: 64 Color: 0
Size: 32 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 5112 Color: 1
Size: 688 Color: 1
Size: 296 Color: 0
Size: 144 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 4156 Color: 1
Size: 1492 Color: 1
Size: 336 Color: 0
Size: 256 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5240 Color: 1
Size: 776 Color: 1
Size: 224 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4648 Color: 1
Size: 1208 Color: 1
Size: 384 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 1
Size: 1148 Color: 1
Size: 224 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 3720 Color: 1
Size: 2408 Color: 1
Size: 112 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4452 Color: 1
Size: 1740 Color: 1
Size: 48 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5480 Color: 1
Size: 416 Color: 1
Size: 344 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4700 Color: 1
Size: 1284 Color: 1
Size: 256 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5232 Color: 1
Size: 848 Color: 1
Size: 160 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5312 Color: 1
Size: 688 Color: 1
Size: 240 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 1
Size: 580 Color: 1
Size: 112 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 3588 Color: 1
Size: 2212 Color: 1
Size: 440 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4908 Color: 1
Size: 1284 Color: 1
Size: 48 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4220 Color: 1
Size: 1684 Color: 1
Size: 336 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 3132 Color: 1
Size: 2604 Color: 1
Size: 504 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 1
Size: 729 Color: 1
Size: 144 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5384 Color: 1
Size: 712 Color: 1
Size: 144 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 3148 Color: 1
Size: 2580 Color: 1
Size: 512 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 3124 Color: 1
Size: 2604 Color: 1
Size: 512 Color: 0

Bin 24: 0 of cap free
Amount of items: 4
Items: 
Size: 3896 Color: 1
Size: 1960 Color: 1
Size: 224 Color: 0
Size: 160 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 3352 Color: 1
Size: 2104 Color: 1
Size: 784 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5436 Color: 1
Size: 676 Color: 1
Size: 128 Color: 0

Bin 27: 0 of cap free
Amount of items: 4
Items: 
Size: 4040 Color: 1
Size: 1280 Color: 1
Size: 888 Color: 0
Size: 32 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5416 Color: 1
Size: 648 Color: 1
Size: 176 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4456 Color: 1
Size: 1496 Color: 1
Size: 288 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 1
Size: 1254 Color: 1
Size: 224 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5608 Color: 1
Size: 600 Color: 1
Size: 32 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5544 Color: 1
Size: 584 Color: 1
Size: 112 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 3128 Color: 1
Size: 2648 Color: 1
Size: 464 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 1
Size: 881 Color: 1
Size: 176 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 1
Size: 1532 Color: 1
Size: 176 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4598 Color: 1
Size: 1370 Color: 1
Size: 272 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4320 Color: 1
Size: 1440 Color: 1
Size: 480 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5016 Color: 1
Size: 1032 Color: 1
Size: 192 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 1
Size: 924 Color: 1
Size: 176 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5604 Color: 1
Size: 532 Color: 1
Size: 104 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 1
Size: 1804 Color: 1
Size: 360 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4940 Color: 1
Size: 1084 Color: 1
Size: 216 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 1
Size: 1112 Color: 1
Size: 960 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5321 Color: 1
Size: 767 Color: 1
Size: 152 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5300 Color: 1
Size: 788 Color: 1
Size: 152 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5572 Color: 1
Size: 564 Color: 1
Size: 104 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 3353 Color: 1
Size: 2407 Color: 1
Size: 480 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 1
Size: 534 Color: 1
Size: 104 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 1
Size: 652 Color: 1
Size: 128 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 1
Size: 764 Color: 1
Size: 144 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4846 Color: 1
Size: 1162 Color: 1
Size: 232 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 4914 Color: 1
Size: 1106 Color: 1
Size: 220 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 1
Size: 900 Color: 1
Size: 176 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 1
Size: 1014 Color: 1
Size: 200 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 3980 Color: 1
Size: 1884 Color: 1
Size: 376 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 4930 Color: 1
Size: 1094 Color: 1
Size: 216 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 1
Size: 1060 Color: 1
Size: 208 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 3620 Color: 1
Size: 2188 Color: 1
Size: 432 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5103 Color: 1
Size: 949 Color: 1
Size: 188 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5094 Color: 1
Size: 958 Color: 1
Size: 188 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5336 Color: 1
Size: 840 Color: 1
Size: 64 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4392 Color: 1
Size: 1544 Color: 1
Size: 304 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4407 Color: 1
Size: 1529 Color: 1
Size: 304 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 4012 Color: 1
Size: 1996 Color: 1
Size: 232 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 1
Size: 1324 Color: 1
Size: 256 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 2528 Color: 1
Size: 1976 Color: 1
Size: 1736 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5062 Color: 1
Size: 982 Color: 1
Size: 196 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 1
Size: 578 Color: 1
Size: 112 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 1
Size: 582 Color: 1
Size: 72 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4390 Color: 1
Size: 1662 Color: 1
Size: 188 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5358 Color: 1
Size: 810 Color: 1
Size: 72 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 5254 Color: 1
Size: 822 Color: 1
Size: 164 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 4258 Color: 1
Size: 1654 Color: 1
Size: 328 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 3734 Color: 1
Size: 2090 Color: 1
Size: 416 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 4323 Color: 1
Size: 1725 Color: 1
Size: 192 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4843 Color: 1
Size: 1201 Color: 1
Size: 196 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5518 Color: 1
Size: 602 Color: 1
Size: 120 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5326 Color: 1
Size: 762 Color: 1
Size: 152 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 5486 Color: 1
Size: 630 Color: 1
Size: 124 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5229 Color: 1
Size: 843 Color: 1
Size: 168 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5294 Color: 1
Size: 790 Color: 1
Size: 156 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5442 Color: 1
Size: 666 Color: 1
Size: 132 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5222 Color: 1
Size: 934 Color: 1
Size: 84 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5157 Color: 1
Size: 903 Color: 1
Size: 180 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 4714 Color: 1
Size: 1274 Color: 1
Size: 252 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5126 Color: 1
Size: 930 Color: 1
Size: 184 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 3970 Color: 1
Size: 1894 Color: 1
Size: 376 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 1
Size: 785 Color: 1
Size: 156 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4094 Color: 1
Size: 1966 Color: 1
Size: 180 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 3122 Color: 1
Size: 2602 Color: 1
Size: 516 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 3394 Color: 1
Size: 2374 Color: 1
Size: 472 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 3130 Color: 1
Size: 2594 Color: 1
Size: 516 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 3138 Color: 1
Size: 2586 Color: 1
Size: 516 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 1
Size: 1417 Color: 1
Size: 282 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4307 Color: 1
Size: 1611 Color: 1
Size: 322 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 5422 Color: 1
Size: 738 Color: 1
Size: 80 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 5419 Color: 1
Size: 797 Color: 1
Size: 24 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 4293 Color: 1
Size: 1623 Color: 1
Size: 324 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 4779 Color: 1
Size: 1219 Color: 1
Size: 242 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 4731 Color: 1
Size: 1259 Color: 1
Size: 250 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 4518 Color: 1
Size: 1438 Color: 1
Size: 284 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 3321 Color: 1
Size: 2433 Color: 1
Size: 486 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 4035 Color: 1
Size: 1839 Color: 1
Size: 366 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 3121 Color: 1
Size: 2601 Color: 1
Size: 518 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 4747 Color: 1
Size: 1353 Color: 1
Size: 140 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5110 Color: 1
Size: 942 Color: 1
Size: 188 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 3725 Color: 1
Size: 2097 Color: 1
Size: 418 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5267 Color: 1
Size: 811 Color: 1
Size: 162 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 3341 Color: 1
Size: 2417 Color: 1
Size: 482 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 5053 Color: 1
Size: 991 Color: 1
Size: 196 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 3146 Color: 1
Size: 2858 Color: 1
Size: 236 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 3741 Color: 1
Size: 2083 Color: 1
Size: 416 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 3920 Color: 1
Size: 2112 Color: 1
Size: 208 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 4947 Color: 1
Size: 1079 Color: 1
Size: 214 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4795 Color: 1
Size: 1327 Color: 1
Size: 118 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 5190 Color: 1
Size: 982 Color: 1
Size: 68 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 4126 Color: 1
Size: 1762 Color: 1
Size: 352 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 5390 Color: 1
Size: 710 Color: 1
Size: 140 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 3125 Color: 1
Size: 2811 Color: 1
Size: 304 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 4051 Color: 1
Size: 1825 Color: 1
Size: 364 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4951 Color: 1
Size: 1127 Color: 1
Size: 162 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 4983 Color: 1
Size: 1049 Color: 1
Size: 208 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 3337 Color: 1
Size: 2421 Color: 1
Size: 482 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 3662 Color: 1
Size: 2150 Color: 1
Size: 428 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 4422 Color: 1
Size: 1518 Color: 1
Size: 300 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 4573 Color: 1
Size: 1391 Color: 1
Size: 276 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 4715 Color: 1
Size: 1271 Color: 1
Size: 254 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 4735 Color: 1
Size: 1255 Color: 1
Size: 250 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 4811 Color: 1
Size: 1191 Color: 1
Size: 238 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5251 Color: 1
Size: 911 Color: 1
Size: 78 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 5403 Color: 1
Size: 699 Color: 1
Size: 138 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 5435 Color: 1
Size: 691 Color: 1
Size: 114 Color: 0

Total size: 823680
Total free space: 0

