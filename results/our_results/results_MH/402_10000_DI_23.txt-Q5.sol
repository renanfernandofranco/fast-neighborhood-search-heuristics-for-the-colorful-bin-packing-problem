Capicity Bin: 5856
Lower Bound: 132

Bins used: 132
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 2320 Color: 2
Size: 1480 Color: 0
Size: 1232 Color: 1
Size: 792 Color: 4
Size: 32 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 4252 Color: 3
Size: 1188 Color: 3
Size: 224 Color: 1
Size: 192 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 3933 Color: 2
Size: 1603 Color: 2
Size: 208 Color: 4
Size: 112 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 4196 Color: 2
Size: 1340 Color: 4
Size: 184 Color: 3
Size: 136 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2936 Color: 3
Size: 2440 Color: 4
Size: 480 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3346 Color: 1
Size: 2094 Color: 2
Size: 416 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4924 Color: 0
Size: 780 Color: 3
Size: 152 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4684 Color: 2
Size: 1060 Color: 3
Size: 112 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 3964 Color: 4
Size: 1580 Color: 2
Size: 312 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4697 Color: 2
Size: 967 Color: 2
Size: 192 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5172 Color: 3
Size: 572 Color: 3
Size: 112 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 3664 Color: 2
Size: 2000 Color: 4
Size: 192 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 0
Size: 844 Color: 2
Size: 16 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4840 Color: 4
Size: 856 Color: 0
Size: 160 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4468 Color: 2
Size: 1244 Color: 2
Size: 144 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5048 Color: 0
Size: 680 Color: 3
Size: 128 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 3944 Color: 1
Size: 1608 Color: 3
Size: 304 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 0
Size: 524 Color: 3
Size: 96 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5084 Color: 4
Size: 644 Color: 2
Size: 128 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4346 Color: 0
Size: 1262 Color: 3
Size: 248 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 1
Size: 672 Color: 0
Size: 264 Color: 1

Bin 22: 0 of cap free
Amount of items: 4
Items: 
Size: 4616 Color: 3
Size: 912 Color: 2
Size: 232 Color: 4
Size: 96 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4986 Color: 2
Size: 726 Color: 1
Size: 144 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4876 Color: 1
Size: 724 Color: 0
Size: 256 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5266 Color: 4
Size: 494 Color: 4
Size: 96 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4716 Color: 0
Size: 972 Color: 1
Size: 168 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5060 Color: 3
Size: 668 Color: 2
Size: 128 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 4082 Color: 3
Size: 1590 Color: 2
Size: 184 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4028 Color: 3
Size: 1524 Color: 4
Size: 304 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 4
Size: 1048 Color: 3
Size: 640 Color: 3

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 4032 Color: 0
Size: 1360 Color: 1
Size: 320 Color: 3
Size: 144 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 2
Size: 724 Color: 3
Size: 144 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4296 Color: 2
Size: 1304 Color: 1
Size: 256 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4728 Color: 3
Size: 1032 Color: 2
Size: 96 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 1
Size: 500 Color: 4
Size: 96 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4692 Color: 3
Size: 956 Color: 3
Size: 208 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4844 Color: 1
Size: 884 Color: 3
Size: 128 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 1
Size: 1204 Color: 3
Size: 120 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4436 Color: 1
Size: 1404 Color: 1
Size: 16 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 3324 Color: 4
Size: 2116 Color: 0
Size: 416 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4359 Color: 0
Size: 1249 Color: 3
Size: 248 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 3724 Color: 4
Size: 2100 Color: 4
Size: 32 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 4746 Color: 2
Size: 926 Color: 0
Size: 184 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 4590 Color: 3
Size: 1058 Color: 4
Size: 208 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 0
Size: 692 Color: 1
Size: 136 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 3052 Color: 2
Size: 2772 Color: 4
Size: 32 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 4506 Color: 3
Size: 1126 Color: 1
Size: 224 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4990 Color: 0
Size: 722 Color: 1
Size: 144 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 4284 Color: 1
Size: 1316 Color: 1
Size: 256 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 4922 Color: 0
Size: 782 Color: 4
Size: 152 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4564 Color: 0
Size: 1084 Color: 2
Size: 208 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2932 Color: 3
Size: 2444 Color: 1
Size: 480 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 4885 Color: 0
Size: 811 Color: 4
Size: 160 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4472 Color: 3
Size: 1160 Color: 0
Size: 224 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 4812 Color: 2
Size: 876 Color: 0
Size: 168 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 4797 Color: 1
Size: 883 Color: 0
Size: 176 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4642 Color: 0
Size: 1198 Color: 3
Size: 16 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 5192 Color: 3
Size: 568 Color: 4
Size: 96 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5160 Color: 4
Size: 584 Color: 0
Size: 112 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 4842 Color: 4
Size: 846 Color: 2
Size: 168 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2954 Color: 0
Size: 2422 Color: 2
Size: 480 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 3892 Color: 2
Size: 1644 Color: 1
Size: 320 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5122 Color: 1
Size: 614 Color: 2
Size: 120 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5136 Color: 1
Size: 624 Color: 1
Size: 96 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 4984 Color: 2
Size: 728 Color: 1
Size: 144 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 3342 Color: 2
Size: 2098 Color: 1
Size: 416 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 3116 Color: 2
Size: 2284 Color: 1
Size: 456 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 3772 Color: 0
Size: 1812 Color: 0
Size: 272 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4517 Color: 0
Size: 1227 Color: 0
Size: 112 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 3604 Color: 3
Size: 1884 Color: 0
Size: 368 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 4959 Color: 3
Size: 761 Color: 4
Size: 136 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 3672 Color: 0
Size: 1832 Color: 1
Size: 352 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 4681 Color: 2
Size: 981 Color: 4
Size: 194 Color: 3

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 4
Size: 604 Color: 4
Size: 112 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 3670 Color: 4
Size: 1822 Color: 4
Size: 364 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 2940 Color: 1
Size: 2636 Color: 1
Size: 280 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 4951 Color: 2
Size: 755 Color: 1
Size: 150 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 4178 Color: 1
Size: 1402 Color: 3
Size: 276 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 2938 Color: 2
Size: 2434 Color: 2
Size: 484 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 4881 Color: 1
Size: 813 Color: 4
Size: 162 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5108 Color: 2
Size: 628 Color: 1
Size: 120 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 2937 Color: 1
Size: 2433 Color: 1
Size: 486 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 4513 Color: 1
Size: 1121 Color: 3
Size: 222 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5003 Color: 4
Size: 711 Color: 1
Size: 142 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5204 Color: 3
Size: 548 Color: 2
Size: 104 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 3331 Color: 4
Size: 2105 Color: 1
Size: 420 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4561 Color: 2
Size: 1083 Color: 2
Size: 212 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 4351 Color: 2
Size: 1255 Color: 1
Size: 250 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 3935 Color: 0
Size: 1601 Color: 3
Size: 320 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 3678 Color: 4
Size: 1818 Color: 2
Size: 360 Color: 4

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 3336 Color: 0
Size: 2104 Color: 4
Size: 416 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 4545 Color: 4
Size: 1093 Color: 2
Size: 218 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 4674 Color: 3
Size: 986 Color: 2
Size: 196 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5218 Color: 3
Size: 534 Color: 3
Size: 104 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 3946 Color: 3
Size: 1594 Color: 0
Size: 316 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 3659 Color: 2
Size: 1831 Color: 2
Size: 366 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 5007 Color: 2
Size: 709 Color: 3
Size: 140 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 5061 Color: 2
Size: 663 Color: 4
Size: 132 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5137 Color: 4
Size: 601 Color: 0
Size: 118 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 5055 Color: 1
Size: 669 Color: 2
Size: 132 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 4446 Color: 2
Size: 1178 Color: 1
Size: 232 Color: 4

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5149 Color: 1
Size: 591 Color: 0
Size: 116 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 5103 Color: 1
Size: 739 Color: 1
Size: 14 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5234 Color: 0
Size: 522 Color: 4
Size: 100 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 4689 Color: 1
Size: 973 Color: 0
Size: 194 Color: 4

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 0
Size: 1322 Color: 0
Size: 260 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 2946 Color: 1
Size: 2426 Color: 2
Size: 484 Color: 2

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 4355 Color: 3
Size: 1251 Color: 4
Size: 250 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 5106 Color: 2
Size: 626 Color: 0
Size: 124 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 4
Size: 554 Color: 2
Size: 100 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5167 Color: 4
Size: 575 Color: 0
Size: 114 Color: 2

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 2933 Color: 3
Size: 2437 Color: 4
Size: 486 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 5154 Color: 3
Size: 586 Color: 1
Size: 116 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5250 Color: 1
Size: 506 Color: 2
Size: 100 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5111 Color: 3
Size: 621 Color: 3
Size: 124 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 3962 Color: 3
Size: 1794 Color: 4
Size: 100 Color: 2

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5186 Color: 2
Size: 562 Color: 4
Size: 108 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 3663 Color: 0
Size: 1829 Color: 4
Size: 364 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 4165 Color: 3
Size: 1411 Color: 3
Size: 280 Color: 2

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 3954 Color: 0
Size: 1586 Color: 3
Size: 316 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 2930 Color: 4
Size: 2442 Color: 2
Size: 484 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 3333 Color: 0
Size: 2103 Color: 4
Size: 420 Color: 3

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 2941 Color: 2
Size: 2431 Color: 1
Size: 484 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 4675 Color: 1
Size: 985 Color: 4
Size: 196 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 3212 Color: 1
Size: 2204 Color: 3
Size: 440 Color: 4

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 4955 Color: 3
Size: 751 Color: 3
Size: 150 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 4161 Color: 3
Size: 1413 Color: 1
Size: 282 Color: 2

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 2929 Color: 4
Size: 2441 Color: 2
Size: 486 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 4685 Color: 3
Size: 977 Color: 1
Size: 194 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 4886 Color: 4
Size: 810 Color: 0
Size: 160 Color: 2

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 5062 Color: 0
Size: 662 Color: 2
Size: 132 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 2
Size: 561 Color: 3
Size: 112 Color: 3

Total size: 772992
Total free space: 0

