Capicity Bin: 2464
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 1696 Color: 161
Size: 384 Color: 93
Size: 168 Color: 58
Size: 88 Color: 35
Size: 64 Color: 20
Size: 48 Color: 9
Size: 8 Color: 1
Size: 8 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 198
Size: 244 Color: 72
Size: 40 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 200
Size: 236 Color: 70
Size: 40 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 144
Size: 892 Color: 130
Size: 176 Color: 62

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 186
Size: 332 Color: 86
Size: 64 Color: 21

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 197
Size: 244 Color: 73
Size: 48 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 173
Size: 458 Color: 100
Size: 88 Color: 36

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 160
Size: 662 Color: 113
Size: 128 Color: 50

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 194
Size: 252 Color: 76
Size: 48 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 171
Size: 484 Color: 102
Size: 96 Color: 39

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 1450 Color: 149
Size: 846 Color: 124
Size: 96 Color: 38
Size: 72 Color: 28

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 196
Size: 246 Color: 74
Size: 48 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2167 Color: 195
Size: 249 Color: 75
Size: 48 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 148
Size: 874 Color: 126
Size: 172 Color: 59

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 146
Size: 882 Color: 128
Size: 176 Color: 61

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 188
Size: 308 Color: 83
Size: 56 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 138
Size: 1027 Color: 135
Size: 204 Color: 68

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 139
Size: 1026 Color: 134
Size: 204 Color: 67

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 163
Size: 734 Color: 117
Size: 12 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 158
Size: 684 Color: 114
Size: 136 Color: 52

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 179
Size: 387 Color: 94
Size: 76 Color: 31

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 175
Size: 452 Color: 99
Size: 72 Color: 30

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 168
Size: 540 Color: 106
Size: 104 Color: 42

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 147
Size: 882 Color: 127
Size: 172 Color: 60

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 184
Size: 342 Color: 88
Size: 68 Color: 23

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 187
Size: 314 Color: 84
Size: 60 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 181
Size: 372 Color: 91
Size: 72 Color: 27

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 178
Size: 404 Color: 95
Size: 72 Color: 26

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2151 Color: 193
Size: 261 Color: 77
Size: 52 Color: 14

Bin 30: 0 of cap free
Amount of items: 5
Items: 
Size: 840 Color: 123
Size: 576 Color: 109
Size: 504 Color: 104
Size: 272 Color: 79
Size: 272 Color: 78

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1235 Color: 140
Size: 1093 Color: 137
Size: 136 Color: 53

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 201
Size: 226 Color: 69
Size: 44 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 141
Size: 1028 Color: 136
Size: 200 Color: 65

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 152
Size: 771 Color: 119
Size: 152 Color: 56

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 151
Size: 780 Color: 120
Size: 152 Color: 57

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2045 Color: 183
Size: 351 Color: 89
Size: 68 Color: 24

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 2182 Color: 199
Size: 238 Color: 71
Size: 44 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 157
Size: 685 Color: 115
Size: 136 Color: 51

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 177
Size: 414 Color: 97
Size: 80 Color: 32

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2123 Color: 192
Size: 315 Color: 85
Size: 26 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1862 Color: 170
Size: 502 Color: 103
Size: 100 Color: 40

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 154
Size: 758 Color: 118
Size: 148 Color: 55

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 156
Size: 722 Color: 116
Size: 144 Color: 54

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 143
Size: 941 Color: 132
Size: 188 Color: 64

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 153
Size: 920 Color: 131

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 191
Size: 290 Color: 80
Size: 56 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 159
Size: 789 Color: 122
Size: 18 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1969 Color: 176
Size: 413 Color: 96
Size: 82 Color: 33

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 180
Size: 378 Color: 92
Size: 72 Color: 25

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 167
Size: 558 Color: 107
Size: 108 Color: 44

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1487 Color: 150
Size: 869 Color: 125
Size: 108 Color: 43

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 142
Size: 1022 Color: 133
Size: 200 Color: 66

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 166
Size: 561 Color: 108
Size: 110 Color: 45

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 145
Size: 887 Color: 129
Size: 176 Color: 63

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1937 Color: 174
Size: 441 Color: 98
Size: 86 Color: 34

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 164
Size: 617 Color: 111
Size: 122 Color: 48

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2115 Color: 190
Size: 291 Color: 81
Size: 58 Color: 17

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 165
Size: 604 Color: 110
Size: 120 Color: 46

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 172
Size: 473 Color: 101
Size: 94 Color: 37

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2107 Color: 189
Size: 299 Color: 82
Size: 58 Color: 18

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 162
Size: 630 Color: 112
Size: 124 Color: 49

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 155
Size: 785 Color: 121
Size: 120 Color: 47

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2063 Color: 185
Size: 335 Color: 87
Size: 66 Color: 22

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 169
Size: 513 Color: 105
Size: 102 Color: 41

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 182
Size: 365 Color: 90
Size: 72 Color: 29

Total size: 160160
Total free space: 0

