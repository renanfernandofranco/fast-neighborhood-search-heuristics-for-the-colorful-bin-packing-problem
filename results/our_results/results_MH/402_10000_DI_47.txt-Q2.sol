Capicity Bin: 8256
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 29
Items: 
Size: 416 Color: 1
Size: 408 Color: 1
Size: 398 Color: 1
Size: 372 Color: 1
Size: 368 Color: 1
Size: 368 Color: 1
Size: 356 Color: 1
Size: 344 Color: 1
Size: 320 Color: 1
Size: 304 Color: 1
Size: 304 Color: 1
Size: 300 Color: 0
Size: 288 Color: 1
Size: 288 Color: 0
Size: 286 Color: 0
Size: 284 Color: 1
Size: 280 Color: 0
Size: 254 Color: 0
Size: 252 Color: 0
Size: 252 Color: 0
Size: 232 Color: 0
Size: 232 Color: 0
Size: 224 Color: 0
Size: 212 Color: 0
Size: 208 Color: 0
Size: 196 Color: 0
Size: 192 Color: 1
Size: 192 Color: 0
Size: 126 Color: 0

Bin 2: 0 of cap free
Amount of items: 8
Items: 
Size: 4136 Color: 1
Size: 904 Color: 1
Size: 756 Color: 1
Size: 686 Color: 0
Size: 684 Color: 0
Size: 684 Color: 0
Size: 258 Color: 1
Size: 148 Color: 0

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 4133 Color: 0
Size: 2051 Color: 1
Size: 1236 Color: 0
Size: 540 Color: 0
Size: 296 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 1
Size: 3428 Color: 1
Size: 680 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4154 Color: 0
Size: 3422 Color: 1
Size: 680 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4180 Color: 1
Size: 3404 Color: 0
Size: 672 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4229 Color: 0
Size: 3357 Color: 0
Size: 670 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5160 Color: 1
Size: 2930 Color: 0
Size: 166 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5206 Color: 1
Size: 2894 Color: 0
Size: 156 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5496 Color: 0
Size: 2584 Color: 1
Size: 176 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5769 Color: 1
Size: 2073 Color: 1
Size: 414 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6052 Color: 1
Size: 1402 Color: 1
Size: 802 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6456 Color: 0
Size: 1656 Color: 0
Size: 144 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6531 Color: 0
Size: 1439 Color: 1
Size: 286 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 0
Size: 1161 Color: 0
Size: 560 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6632 Color: 0
Size: 1224 Color: 1
Size: 400 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6760 Color: 0
Size: 1256 Color: 0
Size: 240 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6877 Color: 0
Size: 1151 Color: 0
Size: 228 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 0
Size: 686 Color: 0
Size: 630 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 1
Size: 888 Color: 1
Size: 396 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7016 Color: 1
Size: 728 Color: 0
Size: 512 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7058 Color: 1
Size: 878 Color: 1
Size: 320 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 1
Size: 732 Color: 0
Size: 416 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7206 Color: 0
Size: 714 Color: 1
Size: 336 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7192 Color: 1
Size: 592 Color: 0
Size: 472 Color: 1

Bin 26: 0 of cap free
Amount of items: 4
Items: 
Size: 7400 Color: 0
Size: 820 Color: 1
Size: 32 Color: 1
Size: 4 Color: 0

Bin 27: 1 of cap free
Amount of items: 12
Items: 
Size: 4129 Color: 1
Size: 500 Color: 0
Size: 480 Color: 1
Size: 470 Color: 1
Size: 448 Color: 1
Size: 420 Color: 1
Size: 362 Color: 0
Size: 358 Color: 0
Size: 320 Color: 0
Size: 316 Color: 0
Size: 240 Color: 0
Size: 212 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 4146 Color: 1
Size: 3437 Color: 1
Size: 672 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 4138 Color: 0
Size: 3609 Color: 1
Size: 508 Color: 0

Bin 30: 1 of cap free
Amount of items: 4
Items: 
Size: 4505 Color: 1
Size: 3426 Color: 0
Size: 164 Color: 0
Size: 160 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 5184 Color: 1
Size: 2791 Color: 0
Size: 280 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 5246 Color: 1
Size: 2731 Color: 0
Size: 278 Color: 1

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 5417 Color: 0
Size: 2542 Color: 1
Size: 296 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 5797 Color: 0
Size: 2262 Color: 0
Size: 196 Color: 1

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 6178 Color: 0
Size: 2077 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 7067 Color: 0
Size: 774 Color: 1
Size: 414 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 7217 Color: 0
Size: 678 Color: 0
Size: 360 Color: 1

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 5174 Color: 0
Size: 2908 Color: 0
Size: 172 Color: 1

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 5238 Color: 0
Size: 2800 Color: 0
Size: 216 Color: 1

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 1
Size: 1162 Color: 0
Size: 424 Color: 0

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 0
Size: 2705 Color: 1
Size: 208 Color: 0

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 5712 Color: 1
Size: 2367 Color: 0
Size: 174 Color: 1

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 5781 Color: 0
Size: 2056 Color: 1
Size: 416 Color: 0

Bin 44: 3 of cap free
Amount of items: 2
Items: 
Size: 6349 Color: 1
Size: 1904 Color: 0

Bin 45: 3 of cap free
Amount of items: 4
Items: 
Size: 7079 Color: 0
Size: 1030 Color: 1
Size: 112 Color: 1
Size: 32 Color: 0

Bin 46: 3 of cap free
Amount of items: 2
Items: 
Size: 7205 Color: 0
Size: 1048 Color: 1

Bin 47: 4 of cap free
Amount of items: 8
Items: 
Size: 4132 Color: 1
Size: 684 Color: 1
Size: 680 Color: 0
Size: 680 Color: 0
Size: 560 Color: 0
Size: 546 Color: 0
Size: 496 Color: 1
Size: 474 Color: 1

Bin 48: 4 of cap free
Amount of items: 3
Items: 
Size: 4392 Color: 1
Size: 3412 Color: 0
Size: 448 Color: 0

Bin 49: 4 of cap free
Amount of items: 3
Items: 
Size: 5684 Color: 1
Size: 2280 Color: 0
Size: 288 Color: 1

Bin 50: 4 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 0
Size: 2312 Color: 1
Size: 214 Color: 0

Bin 51: 4 of cap free
Amount of items: 2
Items: 
Size: 6408 Color: 1
Size: 1844 Color: 0

Bin 52: 4 of cap free
Amount of items: 2
Items: 
Size: 7268 Color: 0
Size: 984 Color: 1

Bin 53: 4 of cap free
Amount of items: 2
Items: 
Size: 7288 Color: 0
Size: 964 Color: 1

Bin 54: 4 of cap free
Amount of items: 2
Items: 
Size: 7356 Color: 0
Size: 896 Color: 1

Bin 55: 5 of cap free
Amount of items: 5
Items: 
Size: 4137 Color: 1
Size: 1534 Color: 1
Size: 1404 Color: 1
Size: 912 Color: 0
Size: 264 Color: 0

Bin 56: 5 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 0
Size: 1801 Color: 1
Size: 576 Color: 1

Bin 57: 5 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 0
Size: 1813 Color: 1
Size: 104 Color: 1

Bin 58: 5 of cap free
Amount of items: 2
Items: 
Size: 6547 Color: 0
Size: 1704 Color: 1

Bin 59: 5 of cap free
Amount of items: 2
Items: 
Size: 7151 Color: 0
Size: 1100 Color: 1

Bin 60: 5 of cap free
Amount of items: 2
Items: 
Size: 7320 Color: 1
Size: 931 Color: 0

Bin 61: 5 of cap free
Amount of items: 2
Items: 
Size: 7330 Color: 1
Size: 921 Color: 0

Bin 62: 6 of cap free
Amount of items: 3
Items: 
Size: 4185 Color: 0
Size: 3441 Color: 1
Size: 624 Color: 0

Bin 63: 6 of cap free
Amount of items: 2
Items: 
Size: 5268 Color: 0
Size: 2982 Color: 1

Bin 64: 6 of cap free
Amount of items: 3
Items: 
Size: 6318 Color: 0
Size: 1612 Color: 0
Size: 320 Color: 1

Bin 65: 6 of cap free
Amount of items: 2
Items: 
Size: 6970 Color: 1
Size: 1280 Color: 0

Bin 66: 6 of cap free
Amount of items: 4
Items: 
Size: 7140 Color: 1
Size: 942 Color: 0
Size: 128 Color: 1
Size: 40 Color: 0

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 7259 Color: 1
Size: 991 Color: 0

Bin 68: 7 of cap free
Amount of items: 3
Items: 
Size: 4297 Color: 1
Size: 3442 Color: 1
Size: 510 Color: 0

Bin 69: 7 of cap free
Amount of items: 2
Items: 
Size: 4856 Color: 0
Size: 3393 Color: 1

Bin 70: 7 of cap free
Amount of items: 2
Items: 
Size: 5122 Color: 0
Size: 3127 Color: 1

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 6466 Color: 0
Size: 1783 Color: 1

Bin 72: 8 of cap free
Amount of items: 2
Items: 
Size: 6975 Color: 0
Size: 1273 Color: 1

Bin 73: 8 of cap free
Amount of items: 2
Items: 
Size: 7172 Color: 0
Size: 1076 Color: 1

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 7255 Color: 0
Size: 993 Color: 1

Bin 75: 9 of cap free
Amount of items: 2
Items: 
Size: 5755 Color: 0
Size: 2492 Color: 1

Bin 76: 9 of cap free
Amount of items: 2
Items: 
Size: 7380 Color: 1
Size: 867 Color: 0

Bin 77: 10 of cap free
Amount of items: 4
Items: 
Size: 6084 Color: 1
Size: 1970 Color: 0
Size: 160 Color: 1
Size: 32 Color: 0

Bin 78: 10 of cap free
Amount of items: 2
Items: 
Size: 6734 Color: 1
Size: 1512 Color: 0

Bin 79: 10 of cap free
Amount of items: 3
Items: 
Size: 6861 Color: 0
Size: 1269 Color: 1
Size: 116 Color: 1

Bin 80: 11 of cap free
Amount of items: 3
Items: 
Size: 6081 Color: 1
Size: 1712 Color: 0
Size: 452 Color: 0

Bin 81: 12 of cap free
Amount of items: 3
Items: 
Size: 6002 Color: 1
Size: 1602 Color: 0
Size: 640 Color: 0

Bin 82: 12 of cap free
Amount of items: 4
Items: 
Size: 6862 Color: 1
Size: 1270 Color: 0
Size: 80 Color: 1
Size: 32 Color: 0

Bin 83: 12 of cap free
Amount of items: 3
Items: 
Size: 6863 Color: 1
Size: 1301 Color: 0
Size: 80 Color: 1

Bin 84: 12 of cap free
Amount of items: 2
Items: 
Size: 6920 Color: 1
Size: 1324 Color: 0

Bin 85: 14 of cap free
Amount of items: 2
Items: 
Size: 6971 Color: 0
Size: 1271 Color: 1

Bin 86: 15 of cap free
Amount of items: 2
Items: 
Size: 5405 Color: 0
Size: 2836 Color: 1

Bin 87: 15 of cap free
Amount of items: 2
Items: 
Size: 6117 Color: 0
Size: 2124 Color: 1

Bin 88: 15 of cap free
Amount of items: 2
Items: 
Size: 6697 Color: 1
Size: 1544 Color: 0

Bin 89: 16 of cap free
Amount of items: 2
Items: 
Size: 6568 Color: 1
Size: 1672 Color: 0

Bin 90: 16 of cap free
Amount of items: 2
Items: 
Size: 6872 Color: 1
Size: 1368 Color: 0

Bin 91: 16 of cap free
Amount of items: 2
Items: 
Size: 7080 Color: 0
Size: 1160 Color: 1

Bin 92: 16 of cap free
Amount of items: 2
Items: 
Size: 7276 Color: 0
Size: 964 Color: 1

Bin 93: 17 of cap free
Amount of items: 2
Items: 
Size: 7258 Color: 1
Size: 981 Color: 0

Bin 94: 17 of cap free
Amount of items: 2
Items: 
Size: 7362 Color: 1
Size: 877 Color: 0

Bin 95: 18 of cap free
Amount of items: 3
Items: 
Size: 4164 Color: 1
Size: 3328 Color: 1
Size: 746 Color: 0

Bin 96: 18 of cap free
Amount of items: 2
Items: 
Size: 6400 Color: 0
Size: 1838 Color: 1

Bin 97: 20 of cap free
Amount of items: 2
Items: 
Size: 5800 Color: 1
Size: 2436 Color: 0

Bin 98: 20 of cap free
Amount of items: 2
Items: 
Size: 7402 Color: 0
Size: 834 Color: 1

Bin 99: 21 of cap free
Amount of items: 2
Items: 
Size: 5011 Color: 1
Size: 3224 Color: 0

Bin 100: 22 of cap free
Amount of items: 2
Items: 
Size: 5716 Color: 1
Size: 2518 Color: 0

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 6682 Color: 0
Size: 1552 Color: 1

Bin 102: 23 of cap free
Amount of items: 2
Items: 
Size: 6085 Color: 0
Size: 2148 Color: 1

Bin 103: 26 of cap free
Amount of items: 2
Items: 
Size: 7298 Color: 0
Size: 932 Color: 1

Bin 104: 29 of cap free
Amount of items: 3
Items: 
Size: 5528 Color: 1
Size: 1864 Color: 0
Size: 835 Color: 0

Bin 105: 29 of cap free
Amount of items: 2
Items: 
Size: 6733 Color: 1
Size: 1494 Color: 0

Bin 106: 36 of cap free
Amount of items: 2
Items: 
Size: 4772 Color: 0
Size: 3448 Color: 1

Bin 107: 36 of cap free
Amount of items: 3
Items: 
Size: 4860 Color: 0
Size: 2840 Color: 0
Size: 520 Color: 1

Bin 108: 36 of cap free
Amount of items: 2
Items: 
Size: 6106 Color: 1
Size: 2114 Color: 0

Bin 109: 39 of cap free
Amount of items: 2
Items: 
Size: 6324 Color: 1
Size: 1893 Color: 0

Bin 110: 41 of cap free
Amount of items: 2
Items: 
Size: 6780 Color: 1
Size: 1435 Color: 0

Bin 111: 41 of cap free
Amount of items: 2
Items: 
Size: 7141 Color: 1
Size: 1074 Color: 0

Bin 112: 52 of cap free
Amount of items: 2
Items: 
Size: 5542 Color: 1
Size: 2662 Color: 0

Bin 113: 54 of cap free
Amount of items: 2
Items: 
Size: 6216 Color: 1
Size: 1986 Color: 0

Bin 114: 55 of cap free
Amount of items: 3
Items: 
Size: 7100 Color: 0
Size: 1069 Color: 1
Size: 32 Color: 0

Bin 115: 55 of cap free
Amount of items: 2
Items: 
Size: 7130 Color: 0
Size: 1071 Color: 1

Bin 116: 60 of cap free
Amount of items: 2
Items: 
Size: 7194 Color: 1
Size: 1002 Color: 0

Bin 117: 61 of cap free
Amount of items: 2
Items: 
Size: 6335 Color: 0
Size: 1860 Color: 1

Bin 118: 62 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 1
Size: 1734 Color: 0

Bin 119: 63 of cap free
Amount of items: 3
Items: 
Size: 4995 Color: 0
Size: 2614 Color: 1
Size: 584 Color: 1

Bin 120: 70 of cap free
Amount of items: 2
Items: 
Size: 4742 Color: 0
Size: 3444 Color: 1

Bin 121: 78 of cap free
Amount of items: 2
Items: 
Size: 6296 Color: 1
Size: 1882 Color: 0

Bin 122: 81 of cap free
Amount of items: 2
Items: 
Size: 6574 Color: 0
Size: 1601 Color: 1

Bin 123: 82 of cap free
Amount of items: 5
Items: 
Size: 4130 Color: 0
Size: 1500 Color: 1
Size: 908 Color: 1
Size: 828 Color: 0
Size: 808 Color: 0

Bin 124: 91 of cap free
Amount of items: 2
Items: 
Size: 6097 Color: 1
Size: 2068 Color: 0

Bin 125: 93 of cap free
Amount of items: 2
Items: 
Size: 6572 Color: 0
Size: 1591 Color: 1

Bin 126: 102 of cap free
Amount of items: 2
Items: 
Size: 6729 Color: 1
Size: 1425 Color: 0

Bin 127: 114 of cap free
Amount of items: 2
Items: 
Size: 5765 Color: 1
Size: 2377 Color: 0

Bin 128: 124 of cap free
Amount of items: 2
Items: 
Size: 4473 Color: 0
Size: 3659 Color: 1

Bin 129: 124 of cap free
Amount of items: 2
Items: 
Size: 4979 Color: 1
Size: 3153 Color: 0

Bin 130: 140 of cap free
Amount of items: 2
Items: 
Size: 4682 Color: 1
Size: 3434 Color: 0

Bin 131: 143 of cap free
Amount of items: 2
Items: 
Size: 6028 Color: 0
Size: 2085 Color: 1

Bin 132: 155 of cap free
Amount of items: 2
Items: 
Size: 6024 Color: 0
Size: 2077 Color: 1

Bin 133: 5466 of cap free
Amount of items: 16
Items: 
Size: 256 Color: 1
Size: 198 Color: 1
Size: 192 Color: 0
Size: 184 Color: 1
Size: 184 Color: 1
Size: 184 Color: 1
Size: 184 Color: 0
Size: 184 Color: 0
Size: 180 Color: 1
Size: 176 Color: 0
Size: 172 Color: 1
Size: 160 Color: 1
Size: 152 Color: 0
Size: 144 Color: 0
Size: 140 Color: 0
Size: 100 Color: 0

Total size: 1089792
Total free space: 8256

