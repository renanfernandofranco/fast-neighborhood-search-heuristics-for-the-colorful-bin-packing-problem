Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 83
Size: 365 Color: 78
Size: 265 Color: 28

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 108
Size: 285 Color: 46
Size: 272 Color: 32

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 115
Size: 273 Color: 35
Size: 252 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 84
Size: 321 Color: 63
Size: 303 Color: 58

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 86
Size: 358 Color: 74
Size: 265 Color: 27

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 104
Size: 316 Color: 60
Size: 250 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 94
Size: 337 Color: 69
Size: 259 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 105
Size: 290 Color: 50
Size: 273 Color: 34

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 90
Size: 361 Color: 76
Size: 252 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 95
Size: 329 Color: 66
Size: 267 Color: 29

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 113
Size: 272 Color: 33
Size: 261 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 87
Size: 359 Color: 75
Size: 260 Color: 18

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 73
Size: 352 Color: 72
Size: 293 Color: 52

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 93
Size: 330 Color: 68
Size: 267 Color: 30

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 114
Size: 250 Color: 1
Size: 278 Color: 38

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 106
Size: 294 Color: 53
Size: 264 Color: 25

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 103
Size: 287 Color: 48
Size: 281 Color: 41

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 100
Size: 298 Color: 55
Size: 275 Color: 37

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 262 Color: 22
Size: 254 Color: 8
Size: 484 Color: 116

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 119
Size: 255 Color: 9
Size: 253 Color: 6

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 107
Size: 294 Color: 54
Size: 264 Color: 24

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 77
Size: 337 Color: 70
Size: 299 Color: 57

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 109
Size: 284 Color: 43
Size: 270 Color: 31

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 85
Size: 367 Color: 79
Size: 256 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 110
Size: 279 Color: 39
Size: 274 Color: 36

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 111
Size: 282 Color: 42
Size: 264 Color: 23

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 118
Size: 259 Color: 14
Size: 251 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 88
Size: 368 Color: 80
Size: 251 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 112
Size: 279 Color: 40
Size: 260 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 97
Size: 329 Color: 67
Size: 257 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 91
Size: 317 Color: 61
Size: 290 Color: 49

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 89
Size: 319 Color: 62
Size: 298 Color: 56

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 96
Size: 323 Color: 64
Size: 265 Color: 26

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 101
Size: 310 Color: 59
Size: 262 Color: 21

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 117
Size: 260 Color: 16
Size: 255 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 92
Size: 349 Color: 71
Size: 254 Color: 7

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 82
Size: 369 Color: 81
Size: 262 Color: 20

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 99
Size: 293 Color: 51
Size: 285 Color: 47

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 102
Size: 285 Color: 45
Size: 284 Color: 44

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 98
Size: 324 Color: 65
Size: 257 Color: 13

Total size: 40000
Total free space: 0

