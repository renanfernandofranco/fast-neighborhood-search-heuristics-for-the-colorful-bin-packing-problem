Capicity Bin: 16432
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 12500 Color: 1
Size: 3284 Color: 1
Size: 648 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11325 Color: 1
Size: 4257 Color: 1
Size: 850 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10210 Color: 1
Size: 5186 Color: 1
Size: 1036 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10264 Color: 1
Size: 5168 Color: 1
Size: 1000 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9548 Color: 1
Size: 5684 Color: 1
Size: 1200 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 8218 Color: 1
Size: 6846 Color: 1
Size: 1368 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 8217 Color: 1
Size: 6847 Color: 1
Size: 1368 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 8128 Color: 1
Size: 4920 Color: 1
Size: 3088 Color: 0
Size: 296 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8220 Color: 1
Size: 6844 Color: 1
Size: 1368 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8222 Color: 1
Size: 6842 Color: 1
Size: 1368 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8232 Color: 1
Size: 6840 Color: 1
Size: 1360 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 8236 Color: 1
Size: 6836 Color: 1
Size: 1360 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 9310 Color: 1
Size: 5938 Color: 1
Size: 1184 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 1
Size: 5932 Color: 1
Size: 1184 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 1
Size: 6040 Color: 1
Size: 1056 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 10101 Color: 1
Size: 5277 Color: 1
Size: 1054 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 10117 Color: 1
Size: 5263 Color: 1
Size: 1052 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 1
Size: 5032 Color: 1
Size: 992 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 10484 Color: 1
Size: 4924 Color: 1
Size: 1024 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 10516 Color: 1
Size: 4932 Color: 1
Size: 984 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 1
Size: 5144 Color: 1
Size: 640 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 10725 Color: 1
Size: 4757 Color: 1
Size: 950 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 10837 Color: 1
Size: 4663 Color: 1
Size: 932 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 10966 Color: 1
Size: 4558 Color: 1
Size: 908 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 10974 Color: 1
Size: 4550 Color: 1
Size: 908 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 10982 Color: 1
Size: 4542 Color: 1
Size: 908 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 1
Size: 4344 Color: 1
Size: 864 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11236 Color: 1
Size: 4332 Color: 1
Size: 864 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11324 Color: 1
Size: 4284 Color: 1
Size: 824 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11336 Color: 1
Size: 4248 Color: 1
Size: 848 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11576 Color: 1
Size: 4056 Color: 1
Size: 800 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11610 Color: 1
Size: 3608 Color: 1
Size: 1214 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11622 Color: 1
Size: 4010 Color: 1
Size: 800 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11857 Color: 1
Size: 3813 Color: 1
Size: 762 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 1
Size: 3780 Color: 1
Size: 752 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11953 Color: 1
Size: 3733 Color: 1
Size: 746 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11969 Color: 1
Size: 3721 Color: 1
Size: 742 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 1
Size: 3724 Color: 1
Size: 604 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12105 Color: 1
Size: 3607 Color: 1
Size: 720 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12138 Color: 1
Size: 3582 Color: 1
Size: 712 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 1
Size: 3528 Color: 1
Size: 704 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 1
Size: 3960 Color: 1
Size: 256 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12419 Color: 1
Size: 3345 Color: 1
Size: 668 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12436 Color: 1
Size: 3332 Color: 1
Size: 664 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 1
Size: 3288 Color: 1
Size: 688 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12477 Color: 1
Size: 3297 Color: 1
Size: 658 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 1
Size: 3528 Color: 1
Size: 400 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12532 Color: 1
Size: 3252 Color: 1
Size: 648 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 1
Size: 3244 Color: 1
Size: 640 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12574 Color: 1
Size: 3218 Color: 1
Size: 640 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 1
Size: 3080 Color: 1
Size: 608 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12801 Color: 1
Size: 3027 Color: 1
Size: 604 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12856 Color: 1
Size: 3014 Color: 1
Size: 562 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 1
Size: 2932 Color: 1
Size: 584 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12930 Color: 1
Size: 2922 Color: 1
Size: 580 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12938 Color: 1
Size: 2914 Color: 1
Size: 580 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 1
Size: 2326 Color: 1
Size: 1144 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 1
Size: 2884 Color: 1
Size: 568 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13012 Color: 1
Size: 2844 Color: 1
Size: 576 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13057 Color: 1
Size: 3025 Color: 1
Size: 350 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13113 Color: 1
Size: 2767 Color: 1
Size: 552 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 1
Size: 2604 Color: 1
Size: 512 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 1
Size: 2680 Color: 1
Size: 384 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 1
Size: 2516 Color: 1
Size: 544 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 1
Size: 2284 Color: 1
Size: 744 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13420 Color: 1
Size: 2556 Color: 1
Size: 456 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13422 Color: 1
Size: 2510 Color: 1
Size: 500 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13438 Color: 1
Size: 2754 Color: 1
Size: 240 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13440 Color: 1
Size: 2512 Color: 1
Size: 480 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 1
Size: 2488 Color: 1
Size: 480 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13487 Color: 1
Size: 2441 Color: 1
Size: 504 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13534 Color: 1
Size: 2160 Color: 1
Size: 738 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13644 Color: 1
Size: 2324 Color: 1
Size: 464 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 1
Size: 2232 Color: 1
Size: 544 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13669 Color: 1
Size: 2303 Color: 1
Size: 460 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13699 Color: 1
Size: 2317 Color: 1
Size: 416 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 1
Size: 2216 Color: 1
Size: 516 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13711 Color: 1
Size: 2005 Color: 1
Size: 716 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13734 Color: 1
Size: 2250 Color: 1
Size: 448 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13784 Color: 1
Size: 2328 Color: 1
Size: 320 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13797 Color: 1
Size: 2075 Color: 1
Size: 560 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 1
Size: 2098 Color: 1
Size: 518 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 1
Size: 2532 Color: 1
Size: 64 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 13845 Color: 1
Size: 2143 Color: 1
Size: 444 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13861 Color: 1
Size: 2157 Color: 1
Size: 414 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 1
Size: 2084 Color: 1
Size: 430 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 1
Size: 2016 Color: 1
Size: 488 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13943 Color: 1
Size: 2041 Color: 1
Size: 448 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13983 Color: 1
Size: 1805 Color: 1
Size: 644 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 1
Size: 1896 Color: 1
Size: 548 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 1
Size: 1986 Color: 1
Size: 396 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 1
Size: 2088 Color: 1
Size: 288 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 14058 Color: 1
Size: 1922 Color: 1
Size: 452 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 14076 Color: 1
Size: 2044 Color: 1
Size: 312 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14089 Color: 1
Size: 1953 Color: 1
Size: 390 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 1
Size: 1964 Color: 1
Size: 328 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 14153 Color: 1
Size: 1901 Color: 1
Size: 378 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 14166 Color: 1
Size: 1890 Color: 1
Size: 376 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 1948 Color: 1
Size: 316 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14170 Color: 1
Size: 1886 Color: 1
Size: 376 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 14188 Color: 1
Size: 1816 Color: 1
Size: 428 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 14221 Color: 1
Size: 1843 Color: 1
Size: 368 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 14263 Color: 1
Size: 1761 Color: 1
Size: 408 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 14264 Color: 1
Size: 1798 Color: 1
Size: 370 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 14278 Color: 1
Size: 1770 Color: 1
Size: 384 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14310 Color: 1
Size: 1702 Color: 1
Size: 420 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14321 Color: 1
Size: 1607 Color: 1
Size: 504 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14340 Color: 1
Size: 1684 Color: 1
Size: 408 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14398 Color: 1
Size: 1992 Color: 1
Size: 42 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14408 Color: 1
Size: 1982 Color: 1
Size: 42 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14412 Color: 1
Size: 1652 Color: 1
Size: 368 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14436 Color: 1
Size: 1668 Color: 1
Size: 328 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14449 Color: 1
Size: 1653 Color: 1
Size: 330 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 1
Size: 1542 Color: 1
Size: 438 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14456 Color: 1
Size: 1656 Color: 1
Size: 320 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14466 Color: 1
Size: 1642 Color: 1
Size: 324 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14485 Color: 1
Size: 1623 Color: 1
Size: 324 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14498 Color: 1
Size: 1598 Color: 1
Size: 336 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14500 Color: 1
Size: 1612 Color: 1
Size: 320 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14518 Color: 1
Size: 1614 Color: 1
Size: 300 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14548 Color: 1
Size: 1528 Color: 1
Size: 356 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 1
Size: 1524 Color: 1
Size: 340 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14574 Color: 1
Size: 1650 Color: 1
Size: 208 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14582 Color: 1
Size: 1550 Color: 1
Size: 300 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 1
Size: 1264 Color: 1
Size: 568 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14605 Color: 1
Size: 1721 Color: 1
Size: 106 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 1
Size: 1468 Color: 1
Size: 352 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14621 Color: 1
Size: 1511 Color: 1
Size: 300 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14622 Color: 1
Size: 1502 Color: 1
Size: 308 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14630 Color: 1
Size: 1442 Color: 1
Size: 360 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 1
Size: 1570 Color: 1
Size: 224 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 14643 Color: 1
Size: 1491 Color: 1
Size: 298 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 14654 Color: 1
Size: 1510 Color: 1
Size: 268 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14660 Color: 1
Size: 1484 Color: 1
Size: 288 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 14663 Color: 1
Size: 1741 Color: 1
Size: 28 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 14676 Color: 1
Size: 1448 Color: 1
Size: 308 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 1
Size: 1572 Color: 1
Size: 164 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 14702 Color: 1
Size: 1498 Color: 1
Size: 232 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 14762 Color: 1
Size: 1394 Color: 1
Size: 276 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 14774 Color: 1
Size: 1382 Color: 1
Size: 276 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 14786 Color: 1
Size: 1374 Color: 1
Size: 272 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 1
Size: 5983 Color: 1
Size: 344 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 10218 Color: 1
Size: 5357 Color: 1
Size: 856 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 11421 Color: 1
Size: 4530 Color: 1
Size: 480 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 11972 Color: 1
Size: 4131 Color: 1
Size: 328 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 12946 Color: 1
Size: 2501 Color: 1
Size: 984 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 12968 Color: 1
Size: 2431 Color: 1
Size: 1032 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 13073 Color: 1
Size: 2778 Color: 1
Size: 580 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 13154 Color: 1
Size: 2781 Color: 1
Size: 496 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 13317 Color: 1
Size: 2746 Color: 1
Size: 368 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 13503 Color: 1
Size: 2592 Color: 1
Size: 336 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 13517 Color: 1
Size: 2418 Color: 1
Size: 496 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 13642 Color: 1
Size: 2597 Color: 1
Size: 192 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 13826 Color: 1
Size: 2269 Color: 1
Size: 336 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 13910 Color: 1
Size: 2037 Color: 1
Size: 484 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 1
Size: 2043 Color: 1
Size: 448 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 14025 Color: 1
Size: 2102 Color: 1
Size: 304 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 14073 Color: 1
Size: 2174 Color: 1
Size: 184 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 14267 Color: 1
Size: 1876 Color: 1
Size: 288 Color: 0

Bin 160: 1 of cap free
Amount of items: 3
Items: 
Size: 14385 Color: 1
Size: 1198 Color: 0
Size: 848 Color: 0

Bin 161: 1 of cap free
Amount of items: 3
Items: 
Size: 14407 Color: 1
Size: 1560 Color: 1
Size: 464 Color: 0

Bin 162: 1 of cap free
Amount of items: 3
Items: 
Size: 14454 Color: 1
Size: 1689 Color: 1
Size: 288 Color: 0

Bin 163: 1 of cap free
Amount of items: 3
Items: 
Size: 14557 Color: 1
Size: 1748 Color: 1
Size: 126 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 12803 Color: 1
Size: 3283 Color: 1
Size: 344 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 10536 Color: 1
Size: 4824 Color: 1
Size: 1070 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 10821 Color: 1
Size: 4873 Color: 1
Size: 736 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 11688 Color: 1
Size: 4022 Color: 1
Size: 720 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 11940 Color: 1
Size: 3586 Color: 1
Size: 904 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 12493 Color: 1
Size: 2801 Color: 1
Size: 1136 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 12566 Color: 1
Size: 2888 Color: 1
Size: 976 Color: 0

Bin 171: 2 of cap free
Amount of items: 3
Items: 
Size: 13028 Color: 1
Size: 2906 Color: 1
Size: 496 Color: 0

Bin 172: 2 of cap free
Amount of items: 3
Items: 
Size: 13072 Color: 1
Size: 2524 Color: 1
Size: 834 Color: 0

Bin 173: 2 of cap free
Amount of items: 3
Items: 
Size: 13138 Color: 1
Size: 2734 Color: 1
Size: 558 Color: 0

Bin 174: 2 of cap free
Amount of items: 3
Items: 
Size: 13470 Color: 1
Size: 2568 Color: 1
Size: 392 Color: 0

Bin 175: 2 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 1
Size: 1688 Color: 1
Size: 352 Color: 0

Bin 176: 3 of cap free
Amount of items: 3
Items: 
Size: 9141 Color: 1
Size: 6008 Color: 1
Size: 1280 Color: 0

Bin 177: 3 of cap free
Amount of items: 3
Items: 
Size: 9237 Color: 1
Size: 6408 Color: 1
Size: 784 Color: 0

Bin 178: 3 of cap free
Amount of items: 3
Items: 
Size: 10240 Color: 1
Size: 6077 Color: 1
Size: 112 Color: 0

Bin 179: 3 of cap free
Amount of items: 3
Items: 
Size: 11292 Color: 1
Size: 4177 Color: 1
Size: 960 Color: 0

Bin 180: 3 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 1
Size: 2813 Color: 1
Size: 392 Color: 0

Bin 181: 4 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 1
Size: 6808 Color: 1
Size: 296 Color: 0

Bin 182: 4 of cap free
Amount of items: 3
Items: 
Size: 11000 Color: 1
Size: 5012 Color: 1
Size: 416 Color: 0

Bin 183: 5 of cap free
Amount of items: 3
Items: 
Size: 12009 Color: 1
Size: 3222 Color: 1
Size: 1196 Color: 0

Bin 184: 5 of cap free
Amount of items: 4
Items: 
Size: 8219 Color: 1
Size: 7280 Color: 1
Size: 816 Color: 0
Size: 112 Color: 0

Bin 185: 5 of cap free
Amount of items: 5
Items: 
Size: 8280 Color: 1
Size: 2852 Color: 1
Size: 2583 Color: 1
Size: 1368 Color: 0
Size: 1344 Color: 0

Bin 186: 6 of cap free
Amount of items: 3
Items: 
Size: 9306 Color: 1
Size: 6064 Color: 1
Size: 1056 Color: 0

Bin 187: 6 of cap free
Amount of items: 3
Items: 
Size: 12130 Color: 1
Size: 3320 Color: 1
Size: 976 Color: 0

Bin 188: 8 of cap free
Amount of items: 3
Items: 
Size: 10420 Color: 1
Size: 4964 Color: 1
Size: 1040 Color: 0

Bin 189: 12 of cap free
Amount of items: 3
Items: 
Size: 10998 Color: 1
Size: 5182 Color: 1
Size: 240 Color: 0

Bin 190: 20 of cap free
Amount of items: 3
Items: 
Size: 10532 Color: 1
Size: 4888 Color: 1
Size: 992 Color: 0

Bin 191: 23 of cap free
Amount of items: 3
Items: 
Size: 9612 Color: 1
Size: 5997 Color: 1
Size: 800 Color: 0

Bin 192: 23 of cap free
Amount of items: 3
Items: 
Size: 11477 Color: 1
Size: 3748 Color: 1
Size: 1184 Color: 0

Bin 193: 30 of cap free
Amount of items: 3
Items: 
Size: 9253 Color: 1
Size: 6845 Color: 1
Size: 304 Color: 0

Bin 194: 31 of cap free
Amount of items: 3
Items: 
Size: 10005 Color: 1
Size: 5740 Color: 1
Size: 656 Color: 0

Bin 195: 52 of cap free
Amount of items: 3
Items: 
Size: 8744 Color: 1
Size: 6612 Color: 1
Size: 1024 Color: 0

Bin 196: 246 of cap free
Amount of items: 3
Items: 
Size: 13333 Color: 1
Size: 2197 Color: 1
Size: 656 Color: 0

Bin 197: 738 of cap free
Amount of items: 3
Items: 
Size: 9224 Color: 1
Size: 5942 Color: 1
Size: 528 Color: 0

Bin 198: 6452 of cap free
Amount of items: 3
Items: 
Size: 5288 Color: 1
Size: 4260 Color: 1
Size: 432 Color: 0

Bin 199: 8701 of cap free
Amount of items: 3
Items: 
Size: 3979 Color: 1
Size: 3320 Color: 1
Size: 432 Color: 0

Total size: 3253536
Total free space: 16432

