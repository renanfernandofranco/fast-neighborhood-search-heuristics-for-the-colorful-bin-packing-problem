Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 251 Color: 7
Size: 250 Color: 12
Size: 499 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 312 Color: 6
Size: 280 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 370 Color: 3
Size: 255 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 6
Size: 329 Color: 9
Size: 276 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 19
Size: 251 Color: 2
Size: 251 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 5
Size: 356 Color: 18
Size: 281 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 11
Size: 371 Color: 0
Size: 258 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 2
Size: 296 Color: 7
Size: 269 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 17
Size: 274 Color: 18
Size: 263 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 342 Color: 11
Size: 277 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 319 Color: 7
Size: 256 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 19
Size: 275 Color: 1
Size: 302 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 16
Size: 340 Color: 1
Size: 285 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8
Size: 312 Color: 10
Size: 263 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 267 Color: 11
Size: 256 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 335 Color: 3
Size: 296 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 15
Size: 257 Color: 18
Size: 253 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 19
Size: 350 Color: 19
Size: 253 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 9
Size: 316 Color: 16
Size: 274 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9
Size: 289 Color: 12
Size: 269 Color: 16

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 10
Size: 286 Color: 9
Size: 267 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 13
Size: 264 Color: 19
Size: 257 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 8
Size: 354 Color: 7
Size: 259 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 10
Size: 282 Color: 8
Size: 258 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 18
Size: 352 Color: 1
Size: 272 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 15
Size: 298 Color: 19
Size: 263 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 7
Size: 264 Color: 3
Size: 252 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 11
Size: 274 Color: 17
Size: 267 Color: 15

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 13
Size: 250 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 10
Size: 370 Color: 6
Size: 260 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 12
Size: 338 Color: 18
Size: 280 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 366 Color: 11
Size: 252 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 331 Color: 18
Size: 266 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 11
Size: 306 Color: 5
Size: 270 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 12
Size: 278 Color: 0
Size: 274 Color: 17

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 17
Size: 314 Color: 3
Size: 256 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 9
Size: 311 Color: 3
Size: 280 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 15
Size: 264 Color: 0
Size: 257 Color: 7

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 8
Size: 313 Color: 19
Size: 288 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 12
Size: 284 Color: 16
Size: 260 Color: 13

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 13
Size: 268 Color: 11
Size: 261 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 13
Size: 316 Color: 16
Size: 292 Color: 8

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 2
Size: 285 Color: 8
Size: 253 Color: 6

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 11
Size: 251 Color: 1
Size: 250 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 325 Color: 19
Size: 254 Color: 18

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 18
Size: 263 Color: 17
Size: 250 Color: 9

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 11
Size: 267 Color: 15
Size: 259 Color: 11

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 4
Size: 292 Color: 18
Size: 288 Color: 11

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 7
Size: 310 Color: 18
Size: 264 Color: 7

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 6
Size: 342 Color: 16
Size: 281 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 10
Size: 361 Color: 3
Size: 273 Color: 16

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 15
Size: 346 Color: 0
Size: 253 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 14
Size: 302 Color: 16
Size: 269 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 15
Size: 369 Color: 18
Size: 251 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 283 Color: 11
Size: 254 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 18
Size: 330 Color: 19
Size: 298 Color: 14

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 10
Size: 320 Color: 7
Size: 252 Color: 11

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 359 Color: 6
Size: 264 Color: 11

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 15
Size: 331 Color: 0
Size: 294 Color: 18

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 2
Size: 326 Color: 12
Size: 309 Color: 9

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 11
Size: 282 Color: 1
Size: 268 Color: 7

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 10
Size: 359 Color: 5
Size: 261 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 9
Size: 300 Color: 11
Size: 274 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 353 Color: 16
Size: 252 Color: 16

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 13
Size: 316 Color: 14
Size: 275 Color: 14

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 14
Size: 360 Color: 0
Size: 258 Color: 10

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 261 Color: 14
Size: 254 Color: 18

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 17
Size: 293 Color: 17
Size: 260 Color: 16

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 11
Size: 273 Color: 19
Size: 261 Color: 13

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 4
Size: 323 Color: 5
Size: 283 Color: 7

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 7
Size: 343 Color: 1
Size: 297 Color: 12

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 12
Size: 283 Color: 14
Size: 268 Color: 15

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 8
Size: 331 Color: 1
Size: 280 Color: 11

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 19
Size: 326 Color: 5
Size: 264 Color: 7

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 15
Size: 278 Color: 0
Size: 276 Color: 6

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 12
Size: 282 Color: 13
Size: 266 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 17
Size: 256 Color: 13
Size: 251 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 17
Size: 340 Color: 8
Size: 280 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 8
Size: 276 Color: 0
Size: 270 Color: 10

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 15
Size: 307 Color: 11
Size: 289 Color: 11

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 315 Color: 19
Size: 288 Color: 13

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 16
Size: 330 Color: 19
Size: 330 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 16
Size: 326 Color: 10
Size: 280 Color: 12

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 10
Size: 306 Color: 0
Size: 254 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 18
Size: 283 Color: 17
Size: 256 Color: 14

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 254 Color: 5
Size: 253 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 301 Color: 13
Size: 296 Color: 9

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 335 Color: 9
Size: 267 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 16
Size: 371 Color: 10
Size: 255 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 335 Color: 6
Size: 281 Color: 14

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 13
Size: 258 Color: 3
Size: 255 Color: 15

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 17
Size: 282 Color: 16
Size: 263 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 15
Size: 265 Color: 13
Size: 254 Color: 19

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 18
Size: 283 Color: 7
Size: 252 Color: 16

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 6
Size: 324 Color: 13
Size: 279 Color: 11

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 8
Size: 290 Color: 19
Size: 266 Color: 13

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 13
Size: 258 Color: 1
Size: 256 Color: 9

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 19
Size: 294 Color: 11
Size: 268 Color: 19

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 17
Size: 274 Color: 16
Size: 272 Color: 19

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 341 Color: 16
Size: 294 Color: 6

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 17
Size: 315 Color: 4
Size: 263 Color: 15

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 19
Size: 285 Color: 3
Size: 255 Color: 10

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 14
Size: 291 Color: 6
Size: 277 Color: 15

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 14
Size: 258 Color: 13
Size: 256 Color: 6

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 17
Size: 265 Color: 0
Size: 264 Color: 14

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 319 Color: 17
Size: 269 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 17
Size: 262 Color: 1
Size: 253 Color: 17

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 13
Size: 341 Color: 1
Size: 261 Color: 17

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 7
Size: 259 Color: 17
Size: 253 Color: 15

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 18
Size: 343 Color: 2
Size: 294 Color: 8

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 15
Size: 332 Color: 15
Size: 317 Color: 7

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 15
Size: 288 Color: 1
Size: 284 Color: 2

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 8
Size: 261 Color: 7
Size: 254 Color: 17

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 5
Size: 261 Color: 9
Size: 253 Color: 9

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 19
Size: 337 Color: 18
Size: 295 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 8
Size: 284 Color: 10
Size: 263 Color: 17

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 360 Color: 11
Size: 255 Color: 16

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 16
Size: 312 Color: 8
Size: 308 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 19
Size: 298 Color: 4
Size: 280 Color: 15

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 12
Size: 283 Color: 0
Size: 281 Color: 5

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 13
Size: 269 Color: 5
Size: 266 Color: 19

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 10
Size: 253 Color: 15
Size: 252 Color: 14

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 19
Size: 273 Color: 11
Size: 257 Color: 17

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 9
Size: 328 Color: 10
Size: 326 Color: 10

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 12
Size: 343 Color: 3
Size: 282 Color: 6

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 6
Size: 325 Color: 5
Size: 260 Color: 15

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 7
Size: 304 Color: 17
Size: 295 Color: 15

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 10
Size: 352 Color: 3
Size: 291 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 12
Size: 320 Color: 6
Size: 258 Color: 11

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 3
Size: 326 Color: 7
Size: 252 Color: 18

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 18
Size: 313 Color: 11
Size: 291 Color: 3

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 323 Color: 14
Size: 286 Color: 16

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 330 Color: 10
Size: 287 Color: 6

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 12
Size: 289 Color: 13
Size: 255 Color: 17

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 270 Color: 11
Size: 264 Color: 19

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 256 Color: 10
Size: 253 Color: 17

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 11
Size: 267 Color: 19
Size: 250 Color: 19

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 10
Size: 319 Color: 14
Size: 280 Color: 14

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 9
Size: 341 Color: 0
Size: 318 Color: 12

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9
Size: 275 Color: 6
Size: 264 Color: 18

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 11
Size: 288 Color: 14
Size: 257 Color: 8

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 7
Size: 298 Color: 12
Size: 285 Color: 16

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 10
Size: 344 Color: 13
Size: 285 Color: 7

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 9
Size: 330 Color: 7
Size: 258 Color: 17

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9
Size: 277 Color: 7
Size: 250 Color: 5

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 16
Size: 364 Color: 10
Size: 255 Color: 18

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 316 Color: 15
Size: 290 Color: 11

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 12
Size: 318 Color: 15
Size: 274 Color: 17

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 307 Color: 2
Size: 265 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 10
Size: 351 Color: 2
Size: 293 Color: 10

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 12
Size: 353 Color: 19
Size: 252 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 15
Size: 306 Color: 0
Size: 266 Color: 18

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 318 Color: 6
Size: 257 Color: 2

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 12
Size: 360 Color: 11
Size: 250 Color: 7

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 13
Size: 278 Color: 7
Size: 257 Color: 11

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 15
Size: 299 Color: 19
Size: 283 Color: 3

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 5
Size: 318 Color: 0
Size: 265 Color: 17

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 342 Color: 7
Size: 279 Color: 10

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 11
Size: 266 Color: 15
Size: 264 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 17
Size: 259 Color: 3
Size: 255 Color: 12

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 338 Color: 3
Size: 259 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 320 Color: 0
Size: 253 Color: 2

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 333 Color: 0
Size: 268 Color: 2

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 6
Size: 368 Color: 10
Size: 260 Color: 12

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 312 Color: 16
Size: 282 Color: 12

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7
Size: 350 Color: 4
Size: 265 Color: 10

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 18
Size: 351 Color: 8
Size: 256 Color: 8

Total size: 167000
Total free space: 0

