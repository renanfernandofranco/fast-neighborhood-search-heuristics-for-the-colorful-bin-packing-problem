Capicity Bin: 1000001
Lower Bound: 222

Bins used: 222
Amount of Colors: 20

Bin 1: 1 of cap free
Amount of items: 2
Items: 
Size: 721734 Color: 14
Size: 278266 Color: 6

Bin 2: 3 of cap free
Amount of items: 3
Items: 
Size: 625298 Color: 19
Size: 252293 Color: 10
Size: 122407 Color: 9

Bin 3: 5 of cap free
Amount of items: 3
Items: 
Size: 658358 Color: 19
Size: 222595 Color: 14
Size: 119043 Color: 2

Bin 4: 5 of cap free
Amount of items: 3
Items: 
Size: 658335 Color: 13
Size: 218591 Color: 15
Size: 123070 Color: 14

Bin 5: 6 of cap free
Amount of items: 3
Items: 
Size: 686006 Color: 14
Size: 167520 Color: 14
Size: 146469 Color: 7

Bin 6: 6 of cap free
Amount of items: 2
Items: 
Size: 622295 Color: 17
Size: 377700 Color: 9

Bin 7: 7 of cap free
Amount of items: 3
Items: 
Size: 468167 Color: 2
Size: 385223 Color: 14
Size: 146604 Color: 14

Bin 8: 8 of cap free
Amount of items: 3
Items: 
Size: 650314 Color: 15
Size: 242121 Color: 12
Size: 107558 Color: 4

Bin 9: 13 of cap free
Amount of items: 3
Items: 
Size: 475793 Color: 8
Size: 385308 Color: 4
Size: 138887 Color: 4

Bin 10: 15 of cap free
Amount of items: 3
Items: 
Size: 722577 Color: 7
Size: 145730 Color: 1
Size: 131679 Color: 13

Bin 11: 19 of cap free
Amount of items: 3
Items: 
Size: 685488 Color: 5
Size: 167621 Color: 8
Size: 146873 Color: 0

Bin 12: 19 of cap free
Amount of items: 3
Items: 
Size: 476568 Color: 18
Size: 363546 Color: 17
Size: 159868 Color: 9

Bin 13: 23 of cap free
Amount of items: 3
Items: 
Size: 460287 Color: 1
Size: 389014 Color: 2
Size: 150677 Color: 7

Bin 14: 28 of cap free
Amount of items: 3
Items: 
Size: 787638 Color: 3
Size: 106204 Color: 7
Size: 106131 Color: 9

Bin 15: 48 of cap free
Amount of items: 3
Items: 
Size: 469554 Color: 2
Size: 398995 Color: 15
Size: 131404 Color: 2

Bin 16: 56 of cap free
Amount of items: 3
Items: 
Size: 666436 Color: 14
Size: 184741 Color: 6
Size: 148768 Color: 4

Bin 17: 56 of cap free
Amount of items: 2
Items: 
Size: 664459 Color: 13
Size: 335486 Color: 14

Bin 18: 57 of cap free
Amount of items: 2
Items: 
Size: 533000 Color: 6
Size: 466944 Color: 3

Bin 19: 62 of cap free
Amount of items: 3
Items: 
Size: 739317 Color: 13
Size: 135378 Color: 7
Size: 125244 Color: 16

Bin 20: 65 of cap free
Amount of items: 2
Items: 
Size: 718243 Color: 1
Size: 281693 Color: 12

Bin 21: 66 of cap free
Amount of items: 3
Items: 
Size: 558169 Color: 9
Size: 265228 Color: 7
Size: 176538 Color: 6

Bin 22: 68 of cap free
Amount of items: 3
Items: 
Size: 482355 Color: 1
Size: 363243 Color: 17
Size: 154335 Color: 11

Bin 23: 71 of cap free
Amount of items: 3
Items: 
Size: 472390 Color: 11
Size: 413642 Color: 13
Size: 113898 Color: 10

Bin 24: 74 of cap free
Amount of items: 3
Items: 
Size: 469093 Color: 9
Size: 350003 Color: 14
Size: 180831 Color: 17

Bin 25: 76 of cap free
Amount of items: 3
Items: 
Size: 601799 Color: 16
Size: 243203 Color: 7
Size: 154923 Color: 10

Bin 26: 85 of cap free
Amount of items: 3
Items: 
Size: 554771 Color: 15
Size: 319272 Color: 4
Size: 125873 Color: 19

Bin 27: 87 of cap free
Amount of items: 3
Items: 
Size: 587219 Color: 7
Size: 244242 Color: 3
Size: 168453 Color: 8

Bin 28: 89 of cap free
Amount of items: 3
Items: 
Size: 682522 Color: 16
Size: 160807 Color: 17
Size: 156583 Color: 12

Bin 29: 92 of cap free
Amount of items: 2
Items: 
Size: 705822 Color: 2
Size: 294087 Color: 1

Bin 30: 95 of cap free
Amount of items: 2
Items: 
Size: 596773 Color: 12
Size: 403133 Color: 14

Bin 31: 97 of cap free
Amount of items: 2
Items: 
Size: 743134 Color: 19
Size: 256770 Color: 8

Bin 32: 97 of cap free
Amount of items: 2
Items: 
Size: 625693 Color: 14
Size: 374211 Color: 9

Bin 33: 98 of cap free
Amount of items: 3
Items: 
Size: 624698 Color: 3
Size: 241507 Color: 14
Size: 133698 Color: 19

Bin 34: 99 of cap free
Amount of items: 3
Items: 
Size: 696079 Color: 8
Size: 166610 Color: 2
Size: 137213 Color: 4

Bin 35: 103 of cap free
Amount of items: 3
Items: 
Size: 712414 Color: 5
Size: 154147 Color: 14
Size: 133337 Color: 5

Bin 36: 108 of cap free
Amount of items: 2
Items: 
Size: 764897 Color: 4
Size: 234996 Color: 12

Bin 37: 109 of cap free
Amount of items: 2
Items: 
Size: 775080 Color: 8
Size: 224812 Color: 14

Bin 38: 110 of cap free
Amount of items: 3
Items: 
Size: 570246 Color: 18
Size: 261749 Color: 2
Size: 167896 Color: 17

Bin 39: 110 of cap free
Amount of items: 3
Items: 
Size: 712175 Color: 7
Size: 150731 Color: 7
Size: 136985 Color: 1

Bin 40: 112 of cap free
Amount of items: 3
Items: 
Size: 569206 Color: 12
Size: 256663 Color: 19
Size: 174020 Color: 6

Bin 41: 146 of cap free
Amount of items: 2
Items: 
Size: 709920 Color: 18
Size: 289935 Color: 12

Bin 42: 152 of cap free
Amount of items: 3
Items: 
Size: 702181 Color: 11
Size: 162597 Color: 19
Size: 135071 Color: 5

Bin 43: 154 of cap free
Amount of items: 2
Items: 
Size: 532771 Color: 19
Size: 467076 Color: 5

Bin 44: 171 of cap free
Amount of items: 2
Items: 
Size: 604196 Color: 5
Size: 395634 Color: 17

Bin 45: 173 of cap free
Amount of items: 3
Items: 
Size: 555945 Color: 3
Size: 304287 Color: 3
Size: 139596 Color: 14

Bin 46: 178 of cap free
Amount of items: 2
Items: 
Size: 532562 Color: 10
Size: 467261 Color: 2

Bin 47: 180 of cap free
Amount of items: 2
Items: 
Size: 545044 Color: 13
Size: 454777 Color: 15

Bin 48: 182 of cap free
Amount of items: 3
Items: 
Size: 682900 Color: 3
Size: 181273 Color: 19
Size: 135646 Color: 12

Bin 49: 182 of cap free
Amount of items: 3
Items: 
Size: 471371 Color: 7
Size: 383604 Color: 3
Size: 144844 Color: 17

Bin 50: 193 of cap free
Amount of items: 2
Items: 
Size: 782256 Color: 12
Size: 217552 Color: 15

Bin 51: 196 of cap free
Amount of items: 3
Items: 
Size: 586653 Color: 18
Size: 247321 Color: 4
Size: 165831 Color: 8

Bin 52: 196 of cap free
Amount of items: 2
Items: 
Size: 710909 Color: 9
Size: 288896 Color: 15

Bin 53: 198 of cap free
Amount of items: 3
Items: 
Size: 625490 Color: 1
Size: 240416 Color: 13
Size: 133897 Color: 11

Bin 54: 203 of cap free
Amount of items: 2
Items: 
Size: 631289 Color: 14
Size: 368509 Color: 10

Bin 55: 218 of cap free
Amount of items: 2
Items: 
Size: 705259 Color: 14
Size: 294524 Color: 4

Bin 56: 222 of cap free
Amount of items: 3
Items: 
Size: 697475 Color: 9
Size: 176125 Color: 18
Size: 126179 Color: 5

Bin 57: 235 of cap free
Amount of items: 3
Items: 
Size: 566075 Color: 10
Size: 261020 Color: 4
Size: 172671 Color: 5

Bin 58: 235 of cap free
Amount of items: 3
Items: 
Size: 559490 Color: 13
Size: 318611 Color: 9
Size: 121665 Color: 10

Bin 59: 253 of cap free
Amount of items: 3
Items: 
Size: 590670 Color: 11
Size: 261456 Color: 0
Size: 147622 Color: 16

Bin 60: 254 of cap free
Amount of items: 2
Items: 
Size: 570197 Color: 10
Size: 429550 Color: 8

Bin 61: 257 of cap free
Amount of items: 2
Items: 
Size: 574106 Color: 9
Size: 425638 Color: 17

Bin 62: 290 of cap free
Amount of items: 3
Items: 
Size: 569673 Color: 9
Size: 242345 Color: 1
Size: 187693 Color: 8

Bin 63: 310 of cap free
Amount of items: 2
Items: 
Size: 562291 Color: 11
Size: 437400 Color: 17

Bin 64: 319 of cap free
Amount of items: 3
Items: 
Size: 670177 Color: 10
Size: 174611 Color: 1
Size: 154894 Color: 3

Bin 65: 326 of cap free
Amount of items: 2
Items: 
Size: 503493 Color: 8
Size: 496182 Color: 12

Bin 66: 338 of cap free
Amount of items: 3
Items: 
Size: 791773 Color: 4
Size: 107883 Color: 18
Size: 100007 Color: 6

Bin 67: 342 of cap free
Amount of items: 3
Items: 
Size: 564508 Color: 10
Size: 279466 Color: 15
Size: 155685 Color: 15

Bin 68: 360 of cap free
Amount of items: 3
Items: 
Size: 651840 Color: 19
Size: 233434 Color: 4
Size: 114367 Color: 5

Bin 69: 368 of cap free
Amount of items: 3
Items: 
Size: 471853 Color: 14
Size: 385111 Color: 17
Size: 142669 Color: 0

Bin 70: 374 of cap free
Amount of items: 2
Items: 
Size: 778655 Color: 19
Size: 220972 Color: 17

Bin 71: 392 of cap free
Amount of items: 2
Items: 
Size: 652469 Color: 2
Size: 347140 Color: 11

Bin 72: 393 of cap free
Amount of items: 2
Items: 
Size: 725074 Color: 0
Size: 274534 Color: 19

Bin 73: 417 of cap free
Amount of items: 3
Items: 
Size: 666466 Color: 8
Size: 228281 Color: 11
Size: 104837 Color: 16

Bin 74: 421 of cap free
Amount of items: 2
Items: 
Size: 561313 Color: 13
Size: 438267 Color: 16

Bin 75: 425 of cap free
Amount of items: 2
Items: 
Size: 777294 Color: 14
Size: 222282 Color: 5

Bin 76: 432 of cap free
Amount of items: 2
Items: 
Size: 638107 Color: 17
Size: 361462 Color: 18

Bin 77: 433 of cap free
Amount of items: 3
Items: 
Size: 682965 Color: 13
Size: 185839 Color: 6
Size: 130764 Color: 0

Bin 78: 442 of cap free
Amount of items: 2
Items: 
Size: 659248 Color: 6
Size: 340311 Color: 4

Bin 79: 451 of cap free
Amount of items: 2
Items: 
Size: 758820 Color: 16
Size: 240730 Color: 13

Bin 80: 476 of cap free
Amount of items: 2
Items: 
Size: 695289 Color: 0
Size: 304236 Color: 3

Bin 81: 483 of cap free
Amount of items: 3
Items: 
Size: 708976 Color: 18
Size: 164577 Color: 1
Size: 125965 Color: 8

Bin 82: 548 of cap free
Amount of items: 2
Items: 
Size: 610653 Color: 10
Size: 388800 Color: 9

Bin 83: 583 of cap free
Amount of items: 2
Items: 
Size: 762378 Color: 11
Size: 237040 Color: 9

Bin 84: 588 of cap free
Amount of items: 2
Items: 
Size: 579139 Color: 8
Size: 420274 Color: 1

Bin 85: 588 of cap free
Amount of items: 2
Items: 
Size: 541829 Color: 19
Size: 457584 Color: 4

Bin 86: 600 of cap free
Amount of items: 3
Items: 
Size: 794160 Color: 16
Size: 103722 Color: 2
Size: 101519 Color: 5

Bin 87: 606 of cap free
Amount of items: 2
Items: 
Size: 768589 Color: 17
Size: 230806 Color: 8

Bin 88: 620 of cap free
Amount of items: 2
Items: 
Size: 524619 Color: 1
Size: 474762 Color: 17

Bin 89: 631 of cap free
Amount of items: 3
Items: 
Size: 548635 Color: 17
Size: 229858 Color: 11
Size: 220877 Color: 12

Bin 90: 647 of cap free
Amount of items: 2
Items: 
Size: 639393 Color: 1
Size: 359961 Color: 5

Bin 91: 662 of cap free
Amount of items: 2
Items: 
Size: 514550 Color: 19
Size: 484789 Color: 1

Bin 92: 677 of cap free
Amount of items: 2
Items: 
Size: 537310 Color: 0
Size: 462014 Color: 14

Bin 93: 681 of cap free
Amount of items: 2
Items: 
Size: 546528 Color: 16
Size: 452792 Color: 14

Bin 94: 690 of cap free
Amount of items: 2
Items: 
Size: 576095 Color: 15
Size: 423216 Color: 5

Bin 95: 700 of cap free
Amount of items: 2
Items: 
Size: 507241 Color: 6
Size: 492060 Color: 18

Bin 96: 705 of cap free
Amount of items: 2
Items: 
Size: 673166 Color: 0
Size: 326130 Color: 18

Bin 97: 740 of cap free
Amount of items: 2
Items: 
Size: 581835 Color: 12
Size: 417426 Color: 5

Bin 98: 755 of cap free
Amount of items: 2
Items: 
Size: 772954 Color: 8
Size: 226292 Color: 4

Bin 99: 810 of cap free
Amount of items: 2
Items: 
Size: 612248 Color: 0
Size: 386943 Color: 7

Bin 100: 834 of cap free
Amount of items: 2
Items: 
Size: 735298 Color: 9
Size: 263869 Color: 6

Bin 101: 843 of cap free
Amount of items: 2
Items: 
Size: 550392 Color: 17
Size: 448766 Color: 5

Bin 102: 860 of cap free
Amount of items: 2
Items: 
Size: 541426 Color: 0
Size: 457715 Color: 9

Bin 103: 867 of cap free
Amount of items: 3
Items: 
Size: 774923 Color: 5
Size: 112145 Color: 10
Size: 112066 Color: 3

Bin 104: 871 of cap free
Amount of items: 2
Items: 
Size: 621317 Color: 6
Size: 377813 Color: 11

Bin 105: 948 of cap free
Amount of items: 2
Items: 
Size: 718720 Color: 4
Size: 280333 Color: 19

Bin 106: 955 of cap free
Amount of items: 2
Items: 
Size: 788296 Color: 6
Size: 210750 Color: 10

Bin 107: 957 of cap free
Amount of items: 2
Items: 
Size: 658267 Color: 16
Size: 340777 Color: 6

Bin 108: 978 of cap free
Amount of items: 2
Items: 
Size: 710763 Color: 16
Size: 288260 Color: 17

Bin 109: 984 of cap free
Amount of items: 2
Items: 
Size: 599849 Color: 7
Size: 399168 Color: 19

Bin 110: 1013 of cap free
Amount of items: 2
Items: 
Size: 605933 Color: 0
Size: 393055 Color: 2

Bin 111: 1058 of cap free
Amount of items: 2
Items: 
Size: 761757 Color: 17
Size: 237186 Color: 9

Bin 112: 1091 of cap free
Amount of items: 2
Items: 
Size: 706316 Color: 6
Size: 292594 Color: 8

Bin 113: 1149 of cap free
Amount of items: 2
Items: 
Size: 521304 Color: 3
Size: 477548 Color: 0

Bin 114: 1217 of cap free
Amount of items: 2
Items: 
Size: 625796 Color: 15
Size: 372988 Color: 18

Bin 115: 1235 of cap free
Amount of items: 2
Items: 
Size: 758793 Color: 17
Size: 239973 Color: 19

Bin 116: 1249 of cap free
Amount of items: 2
Items: 
Size: 502542 Color: 2
Size: 496210 Color: 18

Bin 117: 1250 of cap free
Amount of items: 2
Items: 
Size: 733896 Color: 0
Size: 264855 Color: 3

Bin 118: 1401 of cap free
Amount of items: 2
Items: 
Size: 741364 Color: 14
Size: 257236 Color: 12

Bin 119: 1407 of cap free
Amount of items: 2
Items: 
Size: 581977 Color: 1
Size: 416617 Color: 18

Bin 120: 1417 of cap free
Amount of items: 2
Items: 
Size: 689104 Color: 5
Size: 309480 Color: 16

Bin 121: 1515 of cap free
Amount of items: 2
Items: 
Size: 630643 Color: 6
Size: 367843 Color: 9

Bin 122: 1523 of cap free
Amount of items: 2
Items: 
Size: 795923 Color: 0
Size: 202555 Color: 8

Bin 123: 1556 of cap free
Amount of items: 2
Items: 
Size: 739866 Color: 4
Size: 258579 Color: 6

Bin 124: 1578 of cap free
Amount of items: 2
Items: 
Size: 779269 Color: 11
Size: 219154 Color: 7

Bin 125: 1619 of cap free
Amount of items: 2
Items: 
Size: 595704 Color: 1
Size: 402678 Color: 8

Bin 126: 1622 of cap free
Amount of items: 2
Items: 
Size: 717873 Color: 10
Size: 280506 Color: 7

Bin 127: 1658 of cap free
Amount of items: 2
Items: 
Size: 660830 Color: 12
Size: 337513 Color: 11

Bin 128: 1665 of cap free
Amount of items: 2
Items: 
Size: 770700 Color: 16
Size: 227636 Color: 1

Bin 129: 1688 of cap free
Amount of items: 2
Items: 
Size: 621889 Color: 11
Size: 376424 Color: 13

Bin 130: 1706 of cap free
Amount of items: 2
Items: 
Size: 692928 Color: 14
Size: 305367 Color: 11

Bin 131: 1711 of cap free
Amount of items: 2
Items: 
Size: 774124 Color: 12
Size: 224166 Color: 6

Bin 132: 1790 of cap free
Amount of items: 2
Items: 
Size: 540977 Color: 8
Size: 457234 Color: 4

Bin 133: 1852 of cap free
Amount of items: 2
Items: 
Size: 561231 Color: 13
Size: 436918 Color: 11

Bin 134: 1946 of cap free
Amount of items: 2
Items: 
Size: 691372 Color: 17
Size: 306683 Color: 13

Bin 135: 1960 of cap free
Amount of items: 2
Items: 
Size: 761582 Color: 17
Size: 236459 Color: 12

Bin 136: 1962 of cap free
Amount of items: 2
Items: 
Size: 727108 Color: 2
Size: 270931 Color: 15

Bin 137: 1967 of cap free
Amount of items: 2
Items: 
Size: 781206 Color: 17
Size: 216828 Color: 6

Bin 138: 1978 of cap free
Amount of items: 2
Items: 
Size: 594091 Color: 16
Size: 403932 Color: 5

Bin 139: 2008 of cap free
Amount of items: 2
Items: 
Size: 687808 Color: 13
Size: 310185 Color: 1

Bin 140: 2068 of cap free
Amount of items: 2
Items: 
Size: 702495 Color: 19
Size: 295438 Color: 4

Bin 141: 2084 of cap free
Amount of items: 2
Items: 
Size: 663593 Color: 13
Size: 334324 Color: 5

Bin 142: 2086 of cap free
Amount of items: 2
Items: 
Size: 552406 Color: 8
Size: 445509 Color: 18

Bin 143: 2236 of cap free
Amount of items: 3
Items: 
Size: 683526 Color: 17
Size: 159676 Color: 10
Size: 154563 Color: 19

Bin 144: 2236 of cap free
Amount of items: 2
Items: 
Size: 687598 Color: 12
Size: 310167 Color: 6

Bin 145: 2245 of cap free
Amount of items: 2
Items: 
Size: 749961 Color: 14
Size: 247795 Color: 13

Bin 146: 2307 of cap free
Amount of items: 2
Items: 
Size: 672101 Color: 10
Size: 325593 Color: 2

Bin 147: 2360 of cap free
Amount of items: 2
Items: 
Size: 536289 Color: 9
Size: 461352 Color: 1

Bin 148: 2379 of cap free
Amount of items: 2
Items: 
Size: 549045 Color: 8
Size: 448577 Color: 7

Bin 149: 2381 of cap free
Amount of items: 3
Items: 
Size: 784940 Color: 15
Size: 111162 Color: 10
Size: 101518 Color: 11

Bin 150: 2382 of cap free
Amount of items: 2
Items: 
Size: 770145 Color: 1
Size: 227474 Color: 0

Bin 151: 2442 of cap free
Amount of items: 2
Items: 
Size: 739461 Color: 12
Size: 258098 Color: 9

Bin 152: 2442 of cap free
Amount of items: 2
Items: 
Size: 744584 Color: 4
Size: 252975 Color: 12

Bin 153: 2447 of cap free
Amount of items: 2
Items: 
Size: 743530 Color: 1
Size: 254024 Color: 13

Bin 154: 2471 of cap free
Amount of items: 2
Items: 
Size: 625794 Color: 11
Size: 371736 Color: 1

Bin 155: 2503 of cap free
Amount of items: 2
Items: 
Size: 705744 Color: 13
Size: 291754 Color: 5

Bin 156: 2504 of cap free
Amount of items: 2
Items: 
Size: 504785 Color: 18
Size: 492712 Color: 3

Bin 157: 2521 of cap free
Amount of items: 2
Items: 
Size: 698926 Color: 11
Size: 298554 Color: 14

Bin 158: 2550 of cap free
Amount of items: 2
Items: 
Size: 501416 Color: 15
Size: 496035 Color: 8

Bin 159: 2610 of cap free
Amount of items: 3
Items: 
Size: 709604 Color: 8
Size: 149627 Color: 7
Size: 138160 Color: 0

Bin 160: 2617 of cap free
Amount of items: 2
Items: 
Size: 604054 Color: 17
Size: 393330 Color: 1

Bin 161: 2644 of cap free
Amount of items: 2
Items: 
Size: 617766 Color: 7
Size: 379591 Color: 18

Bin 162: 2683 of cap free
Amount of items: 2
Items: 
Size: 519789 Color: 15
Size: 477529 Color: 4

Bin 163: 2699 of cap free
Amount of items: 2
Items: 
Size: 627205 Color: 19
Size: 370097 Color: 8

Bin 164: 2702 of cap free
Amount of items: 2
Items: 
Size: 602439 Color: 3
Size: 394860 Color: 4

Bin 165: 2717 of cap free
Amount of items: 2
Items: 
Size: 771048 Color: 6
Size: 226236 Color: 0

Bin 166: 2750 of cap free
Amount of items: 2
Items: 
Size: 798194 Color: 8
Size: 199057 Color: 11

Bin 167: 2834 of cap free
Amount of items: 2
Items: 
Size: 505464 Color: 18
Size: 491703 Color: 17

Bin 168: 2961 of cap free
Amount of items: 2
Items: 
Size: 531879 Color: 9
Size: 465161 Color: 5

Bin 169: 2972 of cap free
Amount of items: 2
Items: 
Size: 541101 Color: 16
Size: 455928 Color: 9

Bin 170: 3005 of cap free
Amount of items: 2
Items: 
Size: 630740 Color: 2
Size: 366256 Color: 7

Bin 171: 3016 of cap free
Amount of items: 2
Items: 
Size: 618819 Color: 13
Size: 378166 Color: 3

Bin 172: 3091 of cap free
Amount of items: 2
Items: 
Size: 578590 Color: 11
Size: 418320 Color: 8

Bin 173: 3099 of cap free
Amount of items: 2
Items: 
Size: 673068 Color: 11
Size: 323834 Color: 0

Bin 174: 3165 of cap free
Amount of items: 2
Items: 
Size: 675273 Color: 5
Size: 321563 Color: 10

Bin 175: 3166 of cap free
Amount of items: 2
Items: 
Size: 574896 Color: 3
Size: 421939 Color: 14

Bin 176: 3223 of cap free
Amount of items: 2
Items: 
Size: 508889 Color: 18
Size: 487889 Color: 4

Bin 177: 3495 of cap free
Amount of items: 2
Items: 
Size: 784208 Color: 1
Size: 212298 Color: 15

Bin 178: 3515 of cap free
Amount of items: 2
Items: 
Size: 660408 Color: 1
Size: 336078 Color: 18

Bin 179: 3542 of cap free
Amount of items: 2
Items: 
Size: 766632 Color: 19
Size: 229827 Color: 1

Bin 180: 3560 of cap free
Amount of items: 2
Items: 
Size: 535779 Color: 15
Size: 460662 Color: 2

Bin 181: 3768 of cap free
Amount of items: 2
Items: 
Size: 690324 Color: 10
Size: 305909 Color: 18

Bin 182: 3870 of cap free
Amount of items: 2
Items: 
Size: 616655 Color: 15
Size: 379476 Color: 16

Bin 183: 3918 of cap free
Amount of items: 2
Items: 
Size: 687354 Color: 13
Size: 308729 Color: 0

Bin 184: 3970 of cap free
Amount of items: 2
Items: 
Size: 592645 Color: 7
Size: 403386 Color: 6

Bin 185: 3989 of cap free
Amount of items: 2
Items: 
Size: 548204 Color: 0
Size: 447808 Color: 17

Bin 186: 4002 of cap free
Amount of items: 2
Items: 
Size: 531405 Color: 2
Size: 464594 Color: 15

Bin 187: 4239 of cap free
Amount of items: 2
Items: 
Size: 743444 Color: 6
Size: 252318 Color: 3

Bin 188: 4400 of cap free
Amount of items: 2
Items: 
Size: 713674 Color: 8
Size: 281927 Color: 7

Bin 189: 4471 of cap free
Amount of items: 2
Items: 
Size: 569598 Color: 16
Size: 425932 Color: 11

Bin 190: 4514 of cap free
Amount of items: 2
Items: 
Size: 797758 Color: 17
Size: 197729 Color: 3

Bin 191: 4525 of cap free
Amount of items: 2
Items: 
Size: 748265 Color: 6
Size: 247211 Color: 1

Bin 192: 4558 of cap free
Amount of items: 2
Items: 
Size: 550008 Color: 1
Size: 445435 Color: 8

Bin 193: 4752 of cap free
Amount of items: 2
Items: 
Size: 713589 Color: 4
Size: 281660 Color: 7

Bin 194: 4926 of cap free
Amount of items: 2
Items: 
Size: 760596 Color: 4
Size: 234479 Color: 18

Bin 195: 5102 of cap free
Amount of items: 2
Items: 
Size: 539718 Color: 3
Size: 455181 Color: 16

Bin 196: 5349 of cap free
Amount of items: 2
Items: 
Size: 674870 Color: 11
Size: 319782 Color: 8

Bin 197: 5492 of cap free
Amount of items: 2
Items: 
Size: 643053 Color: 8
Size: 351456 Color: 18

Bin 198: 5657 of cap free
Amount of items: 2
Items: 
Size: 760482 Color: 6
Size: 233862 Color: 14

Bin 199: 5712 of cap free
Amount of items: 2
Items: 
Size: 579284 Color: 3
Size: 415005 Color: 18

Bin 200: 6117 of cap free
Amount of items: 2
Items: 
Size: 702152 Color: 9
Size: 291732 Color: 18

Bin 201: 6387 of cap free
Amount of items: 2
Items: 
Size: 797980 Color: 6
Size: 195634 Color: 0

Bin 202: 6419 of cap free
Amount of items: 2
Items: 
Size: 592011 Color: 5
Size: 401571 Color: 15

Bin 203: 6561 of cap free
Amount of items: 2
Items: 
Size: 601708 Color: 12
Size: 391732 Color: 4

Bin 204: 6694 of cap free
Amount of items: 2
Items: 
Size: 674089 Color: 18
Size: 319218 Color: 9

Bin 205: 6717 of cap free
Amount of items: 2
Items: 
Size: 592359 Color: 7
Size: 400925 Color: 8

Bin 206: 6722 of cap free
Amount of items: 4
Items: 
Size: 471733 Color: 10
Size: 243348 Color: 19
Size: 148968 Color: 12
Size: 129230 Color: 5

Bin 207: 6905 of cap free
Amount of items: 2
Items: 
Size: 501564 Color: 10
Size: 491532 Color: 16

Bin 208: 7209 of cap free
Amount of items: 2
Items: 
Size: 641992 Color: 2
Size: 350800 Color: 14

Bin 209: 7506 of cap free
Amount of items: 2
Items: 
Size: 518006 Color: 11
Size: 474489 Color: 4

Bin 210: 9574 of cap free
Amount of items: 2
Items: 
Size: 498901 Color: 14
Size: 491526 Color: 1

Bin 211: 10367 of cap free
Amount of items: 2
Items: 
Size: 575112 Color: 10
Size: 414522 Color: 5

Bin 212: 10873 of cap free
Amount of items: 2
Items: 
Size: 574305 Color: 3
Size: 414823 Color: 5

Bin 213: 12607 of cap free
Amount of items: 2
Items: 
Size: 724149 Color: 18
Size: 263245 Color: 16

Bin 214: 12712 of cap free
Amount of items: 2
Items: 
Size: 795094 Color: 17
Size: 192195 Color: 6

Bin 215: 13385 of cap free
Amount of items: 3
Items: 
Size: 660104 Color: 7
Size: 167543 Color: 4
Size: 158969 Color: 10

Bin 216: 16012 of cap free
Amount of items: 2
Items: 
Size: 789695 Color: 13
Size: 194294 Color: 10

Bin 217: 16755 of cap free
Amount of items: 2
Items: 
Size: 790974 Color: 14
Size: 192272 Color: 0

Bin 218: 21922 of cap free
Amount of items: 2
Items: 
Size: 792941 Color: 16
Size: 185138 Color: 14

Bin 219: 22497 of cap free
Amount of items: 2
Items: 
Size: 489964 Color: 1
Size: 487540 Color: 6

Bin 220: 22646 of cap free
Amount of items: 2
Items: 
Size: 491380 Color: 15
Size: 485975 Color: 11

Bin 221: 28254 of cap free
Amount of items: 2
Items: 
Size: 486179 Color: 3
Size: 485568 Color: 18

Bin 222: 73627 of cap free
Amount of items: 2
Items: 
Size: 591275 Color: 8
Size: 335099 Color: 7

Total size: 221381793
Total free space: 618429

