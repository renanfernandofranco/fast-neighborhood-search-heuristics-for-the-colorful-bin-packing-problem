Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 304 Color: 0
Size: 254 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 3
Size: 280 Color: 1
Size: 275 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 351 Color: 3
Size: 258 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 298 Color: 3
Size: 276 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 3
Size: 304 Color: 2
Size: 253 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 359 Color: 1
Size: 259 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 319 Color: 1
Size: 252 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 0
Size: 369 Color: 0
Size: 262 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 359 Color: 1
Size: 261 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 343 Color: 3
Size: 294 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 2
Size: 289 Color: 3
Size: 258 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 4
Size: 273 Color: 1
Size: 259 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 319 Color: 3
Size: 304 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 3
Size: 288 Color: 3
Size: 261 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 316 Color: 0
Size: 308 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 263 Color: 1
Size: 254 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 337 Color: 0
Size: 266 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 2
Size: 307 Color: 3
Size: 276 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 332 Color: 3
Size: 256 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 364 Color: 0
Size: 263 Color: 2

Total size: 20000
Total free space: 0

