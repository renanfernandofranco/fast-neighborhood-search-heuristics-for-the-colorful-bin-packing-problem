Capicity Bin: 2032
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 820 Color: 133
Size: 754 Color: 129
Size: 420 Color: 111
Size: 38 Color: 10

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1334 Color: 153
Size: 472 Color: 115
Size: 186 Color: 71
Size: 40 Color: 15

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1130 Color: 144
Size: 758 Color: 131
Size: 56 Color: 32
Size: 48 Color: 26
Size: 40 Color: 17

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1301 Color: 152
Size: 559 Color: 120
Size: 120 Color: 59
Size: 52 Color: 30

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1810 Color: 199
Size: 210 Color: 80
Size: 8 Color: 1
Size: 4 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 157
Size: 518 Color: 117
Size: 100 Color: 56

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 178
Size: 291 Color: 94
Size: 56 Color: 33

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 169
Size: 390 Color: 107
Size: 24 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1501 Color: 162
Size: 443 Color: 112
Size: 88 Color: 51

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 190
Size: 206 Color: 78
Size: 44 Color: 20

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 167
Size: 371 Color: 105
Size: 74 Color: 44

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1127 Color: 143
Size: 755 Color: 130
Size: 150 Color: 65

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1759 Color: 188
Size: 229 Color: 84
Size: 44 Color: 22

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 175
Size: 294 Color: 96
Size: 80 Color: 49

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 196
Size: 193 Color: 74
Size: 38 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 174
Size: 317 Color: 99
Size: 62 Color: 37

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 145
Size: 721 Color: 128
Size: 144 Color: 63

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 187
Size: 230 Color: 85
Size: 44 Color: 21

Bin 19: 0 of cap free
Amount of items: 4
Items: 
Size: 1400 Color: 156
Size: 316 Color: 98
Size: 240 Color: 87
Size: 76 Color: 45

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 170
Size: 363 Color: 103
Size: 40 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1245 Color: 148
Size: 657 Color: 124
Size: 130 Color: 62

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 147
Size: 662 Color: 125
Size: 128 Color: 61

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 165
Size: 391 Color: 108
Size: 78 Color: 46

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 193
Size: 148 Color: 64
Size: 94 Color: 55

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1017 Color: 138
Size: 937 Color: 137
Size: 78 Color: 48

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 189
Size: 197 Color: 75
Size: 64 Color: 39

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1441 Color: 158
Size: 553 Color: 119
Size: 38 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 177
Size: 314 Color: 97
Size: 36 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 166
Size: 418 Color: 110
Size: 36 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 191
Size: 209 Color: 79
Size: 40 Color: 18

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 140
Size: 843 Color: 134
Size: 168 Color: 68

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1825 Color: 201
Size: 173 Color: 69
Size: 34 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1709 Color: 181
Size: 271 Color: 91
Size: 52 Color: 31

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 197
Size: 191 Color: 73
Size: 36 Color: 7

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1222 Color: 146
Size: 582 Color: 121
Size: 228 Color: 82

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 168
Size: 369 Color: 104
Size: 72 Color: 42

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1281 Color: 151
Size: 673 Color: 126
Size: 78 Color: 47

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 172
Size: 325 Color: 101
Size: 64 Color: 40

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1744 Color: 185
Size: 228 Color: 83
Size: 60 Color: 35

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 194
Size: 201 Color: 76
Size: 40 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1702 Color: 179
Size: 278 Color: 93
Size: 52 Color: 29

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1747 Color: 186
Size: 239 Color: 86
Size: 46 Color: 23

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 150
Size: 680 Color: 127
Size: 72 Color: 43

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 184
Size: 251 Color: 88
Size: 50 Color: 28

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 183
Size: 255 Color: 89
Size: 50 Color: 27

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 173
Size: 321 Color: 100
Size: 62 Color: 38

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 182
Size: 262 Color: 90
Size: 48 Color: 25

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 159
Size: 481 Color: 116
Size: 94 Color: 54

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 161
Size: 461 Color: 113
Size: 90 Color: 52

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 155
Size: 541 Color: 118
Size: 108 Color: 57

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 176
Size: 293 Color: 95
Size: 58 Color: 34

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 171
Size: 338 Color: 102
Size: 64 Color: 41

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 163
Size: 382 Color: 106
Size: 116 Color: 58

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 200
Size: 177 Color: 70
Size: 34 Color: 5

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 154
Size: 611 Color: 122
Size: 42 Color: 19

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 195
Size: 219 Color: 81
Size: 16 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1708 Color: 180
Size: 276 Color: 92
Size: 48 Color: 24

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 198
Size: 190 Color: 72
Size: 36 Color: 6

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1018 Color: 139
Size: 846 Color: 135
Size: 168 Color: 67

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 160
Size: 462 Color: 114
Size: 92 Color: 53

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 164
Size: 411 Color: 109
Size: 82 Color: 50

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1265 Color: 149
Size: 641 Color: 123
Size: 126 Color: 60

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1079 Color: 141
Size: 795 Color: 132
Size: 158 Color: 66

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 192
Size: 206 Color: 77
Size: 40 Color: 14

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1126 Color: 142
Size: 846 Color: 136
Size: 60 Color: 36

Total size: 132080
Total free space: 0

