Capicity Bin: 2048
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 828 Color: 134
Size: 696 Color: 127
Size: 316 Color: 93
Size: 76 Color: 42
Size: 56 Color: 25
Size: 40 Color: 10
Size: 20 Color: 4
Size: 8 Color: 2
Size: 8 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 198
Size: 202 Color: 72
Size: 36 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 145
Size: 709 Color: 129
Size: 140 Color: 60

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 143
Size: 735 Color: 131
Size: 146 Color: 63

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 185
Size: 282 Color: 87
Size: 52 Color: 20

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 180
Size: 313 Color: 91
Size: 62 Color: 32

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 164
Size: 439 Color: 109
Size: 86 Color: 47

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 178
Size: 321 Color: 96
Size: 62 Color: 30

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1029 Color: 140
Size: 851 Color: 135
Size: 168 Color: 66

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 158
Size: 522 Color: 117
Size: 100 Color: 51

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 168
Size: 395 Color: 104
Size: 78 Color: 43

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1386 Color: 155
Size: 554 Color: 119
Size: 108 Color: 53

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 161
Size: 491 Color: 113
Size: 96 Color: 49

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 162
Size: 498 Color: 116
Size: 48 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 182
Size: 302 Color: 90
Size: 56 Color: 26

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 173
Size: 362 Color: 100
Size: 72 Color: 37

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 177
Size: 321 Color: 95
Size: 64 Color: 34

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 197
Size: 202 Color: 73
Size: 40 Color: 9

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 154
Size: 605 Color: 122
Size: 60 Color: 28

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 172
Size: 388 Color: 101
Size: 72 Color: 38

Bin 21: 0 of cap free
Amount of items: 4
Items: 
Size: 1280 Color: 148
Size: 472 Color: 111
Size: 220 Color: 77
Size: 76 Color: 40

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 138
Size: 853 Color: 136
Size: 170 Color: 68

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 199
Size: 198 Color: 71
Size: 36 Color: 8

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 170
Size: 414 Color: 107
Size: 52 Color: 22

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 186
Size: 274 Color: 86
Size: 52 Color: 23

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1285 Color: 149
Size: 637 Color: 125
Size: 126 Color: 58

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 150
Size: 630 Color: 124
Size: 120 Color: 56

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 181
Size: 314 Color: 92
Size: 60 Color: 29

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 152
Size: 562 Color: 121
Size: 112 Color: 55

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 167
Size: 398 Color: 105
Size: 76 Color: 39

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 192
Size: 274 Color: 85
Size: 4 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 147
Size: 682 Color: 126
Size: 132 Color: 59

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 174
Size: 353 Color: 99
Size: 70 Color: 36

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 176
Size: 330 Color: 97
Size: 64 Color: 33

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 179
Size: 317 Color: 94
Size: 62 Color: 31

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1321 Color: 151
Size: 607 Color: 123
Size: 120 Color: 57

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 201
Size: 190 Color: 69
Size: 36 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 189
Size: 246 Color: 81
Size: 48 Color: 19

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 194
Size: 261 Color: 83
Size: 12 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 169
Size: 391 Color: 103
Size: 78 Color: 44

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 183
Size: 291 Color: 89
Size: 56 Color: 24

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 188
Size: 258 Color: 82
Size: 48 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 146
Size: 706 Color: 128
Size: 140 Color: 61

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 157
Size: 424 Color: 108
Size: 208 Color: 74

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 153
Size: 559 Color: 120
Size: 110 Color: 54

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1026 Color: 139
Size: 854 Color: 137
Size: 168 Color: 67

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 200
Size: 194 Color: 70
Size: 36 Color: 7

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 163
Size: 442 Color: 110
Size: 84 Color: 46

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 166
Size: 406 Color: 106
Size: 80 Color: 45

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1703 Color: 184
Size: 289 Color: 88
Size: 56 Color: 27

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 187
Size: 271 Color: 84
Size: 52 Color: 21

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 171
Size: 389 Color: 102
Size: 76 Color: 41

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 141
Size: 802 Color: 133
Size: 160 Color: 65

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 156
Size: 529 Color: 118
Size: 104 Color: 52

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 160
Size: 494 Color: 115
Size: 96 Color: 48

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 159
Size: 493 Color: 114
Size: 98 Color: 50

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 165
Size: 477 Color: 112
Size: 44 Color: 13

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 175
Size: 351 Color: 98
Size: 68 Color: 35

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 190
Size: 239 Color: 80
Size: 46 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 144
Size: 731 Color: 130
Size: 146 Color: 62

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 193
Size: 231 Color: 78
Size: 46 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 191
Size: 235 Color: 79
Size: 46 Color: 14

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 142
Size: 739 Color: 132
Size: 146 Color: 64

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 195
Size: 215 Color: 76
Size: 42 Color: 12

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 196
Size: 211 Color: 75
Size: 42 Color: 11

Total size: 133120
Total free space: 0

