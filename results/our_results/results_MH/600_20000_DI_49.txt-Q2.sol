Capicity Bin: 14912
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 11
Items: 
Size: 7462 Color: 1
Size: 876 Color: 1
Size: 832 Color: 1
Size: 816 Color: 1
Size: 816 Color: 1
Size: 802 Color: 0
Size: 800 Color: 0
Size: 792 Color: 0
Size: 774 Color: 0
Size: 626 Color: 0
Size: 316 Color: 1

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 7460 Color: 0
Size: 1096 Color: 0
Size: 1072 Color: 0
Size: 1072 Color: 0
Size: 992 Color: 1
Size: 976 Color: 1
Size: 976 Color: 1
Size: 768 Color: 0
Size: 500 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7528 Color: 1
Size: 6980 Color: 1
Size: 404 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8272 Color: 0
Size: 6168 Color: 0
Size: 472 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8784 Color: 1
Size: 5712 Color: 0
Size: 416 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 8844 Color: 1
Size: 5764 Color: 0
Size: 304 Color: 1

Bin 7: 0 of cap free
Amount of items: 5
Items: 
Size: 9051 Color: 0
Size: 1597 Color: 0
Size: 1522 Color: 1
Size: 1448 Color: 1
Size: 1294 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 9240 Color: 0
Size: 5336 Color: 1
Size: 336 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 9944 Color: 1
Size: 4680 Color: 0
Size: 288 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10142 Color: 1
Size: 4524 Color: 0
Size: 246 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10516 Color: 1
Size: 4040 Color: 0
Size: 356 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 0
Size: 3920 Color: 0
Size: 344 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11440 Color: 0
Size: 2896 Color: 1
Size: 576 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11416 Color: 1
Size: 3124 Color: 1
Size: 372 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11628 Color: 1
Size: 2850 Color: 0
Size: 434 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12158 Color: 0
Size: 1514 Color: 1
Size: 1240 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12314 Color: 0
Size: 2166 Color: 1
Size: 432 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 0
Size: 1492 Color: 0
Size: 1096 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12365 Color: 0
Size: 2217 Color: 0
Size: 330 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12580 Color: 0
Size: 1464 Color: 0
Size: 868 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 0
Size: 1744 Color: 0
Size: 424 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 0
Size: 1604 Color: 1
Size: 528 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12832 Color: 0
Size: 1302 Color: 0
Size: 778 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 1
Size: 1368 Color: 0
Size: 672 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12867 Color: 0
Size: 1661 Color: 0
Size: 384 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13083 Color: 0
Size: 1493 Color: 1
Size: 336 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 0
Size: 976 Color: 1
Size: 786 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 0
Size: 1380 Color: 0
Size: 300 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13234 Color: 0
Size: 1402 Color: 1
Size: 276 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 1
Size: 1240 Color: 0
Size: 384 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13348 Color: 1
Size: 1308 Color: 0
Size: 256 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 10194 Color: 0
Size: 4453 Color: 0
Size: 264 Color: 1

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 10193 Color: 1
Size: 4088 Color: 0
Size: 630 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 10233 Color: 0
Size: 4084 Color: 0
Size: 594 Color: 1

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 10297 Color: 0
Size: 4342 Color: 1
Size: 272 Color: 1

Bin 36: 1 of cap free
Amount of items: 2
Items: 
Size: 11943 Color: 0
Size: 2968 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 12203 Color: 0
Size: 2436 Color: 0
Size: 272 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 12329 Color: 1
Size: 2298 Color: 0
Size: 284 Color: 1

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 12457 Color: 0
Size: 2454 Color: 1

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 12599 Color: 0
Size: 2312 Color: 1

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 12751 Color: 1
Size: 2160 Color: 0

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 12803 Color: 1
Size: 2108 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 12977 Color: 0
Size: 1232 Color: 0
Size: 702 Color: 1

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 10281 Color: 0
Size: 3933 Color: 0
Size: 696 Color: 1

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 10304 Color: 0
Size: 4394 Color: 0
Size: 212 Color: 1

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 10960 Color: 0
Size: 2664 Color: 1
Size: 1286 Color: 1

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 11118 Color: 1
Size: 3312 Color: 1
Size: 480 Color: 0

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 11970 Color: 0
Size: 2940 Color: 1

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 12008 Color: 0
Size: 2642 Color: 1
Size: 260 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 12080 Color: 1
Size: 1456 Color: 0
Size: 1374 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 12086 Color: 1
Size: 1816 Color: 0
Size: 1008 Color: 1

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 1
Size: 2256 Color: 0
Size: 546 Color: 0

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 12562 Color: 1
Size: 2348 Color: 0

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 1
Size: 1786 Color: 0

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 0
Size: 1734 Color: 1

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 0
Size: 1586 Color: 1

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 8327 Color: 1
Size: 6214 Color: 0
Size: 368 Color: 0

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 10224 Color: 0
Size: 3901 Color: 0
Size: 784 Color: 1

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 11784 Color: 0
Size: 2733 Color: 1
Size: 392 Color: 0

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 12003 Color: 1
Size: 1664 Color: 0
Size: 1242 Color: 1

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 12188 Color: 1
Size: 2345 Color: 0
Size: 376 Color: 1

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 12434 Color: 0
Size: 2475 Color: 1

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 13034 Color: 0
Size: 1875 Color: 1

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 13133 Color: 1
Size: 1776 Color: 0

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 13253 Color: 1
Size: 1656 Color: 0

Bin 66: 4 of cap free
Amount of items: 3
Items: 
Size: 7996 Color: 0
Size: 6016 Color: 1
Size: 896 Color: 1

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 9053 Color: 0
Size: 5487 Color: 0
Size: 368 Color: 1

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 12352 Color: 0
Size: 2556 Color: 1

Bin 69: 5 of cap free
Amount of items: 3
Items: 
Size: 9697 Color: 0
Size: 4890 Color: 0
Size: 320 Color: 1

Bin 70: 5 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 1
Size: 3517 Color: 0
Size: 226 Color: 1

Bin 71: 5 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 1
Size: 2123 Color: 0

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 12481 Color: 1
Size: 2425 Color: 0

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 13201 Color: 0
Size: 1705 Color: 1

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 0
Size: 1682 Color: 1

Bin 75: 6 of cap free
Amount of items: 3
Items: 
Size: 13362 Color: 1
Size: 1512 Color: 0
Size: 32 Color: 0

Bin 76: 7 of cap free
Amount of items: 16
Items: 
Size: 7457 Color: 1
Size: 544 Color: 1
Size: 536 Color: 1
Size: 530 Color: 0
Size: 528 Color: 1
Size: 524 Color: 0
Size: 524 Color: 0
Size: 516 Color: 1
Size: 512 Color: 1
Size: 512 Color: 0
Size: 504 Color: 1
Size: 494 Color: 0
Size: 488 Color: 0
Size: 468 Color: 0
Size: 448 Color: 0
Size: 320 Color: 1

Bin 77: 7 of cap free
Amount of items: 12
Items: 
Size: 7461 Color: 1
Size: 816 Color: 1
Size: 770 Color: 1
Size: 768 Color: 1
Size: 728 Color: 0
Size: 720 Color: 1
Size: 704 Color: 0
Size: 692 Color: 0
Size: 640 Color: 0
Size: 634 Color: 0
Size: 632 Color: 0
Size: 340 Color: 1

Bin 78: 7 of cap free
Amount of items: 7
Items: 
Size: 7472 Color: 0
Size: 1424 Color: 1
Size: 1416 Color: 0
Size: 1383 Color: 0
Size: 1322 Color: 1
Size: 1216 Color: 1
Size: 672 Color: 0

Bin 79: 7 of cap free
Amount of items: 2
Items: 
Size: 10558 Color: 0
Size: 4347 Color: 1

Bin 80: 7 of cap free
Amount of items: 2
Items: 
Size: 11692 Color: 1
Size: 3213 Color: 0

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 11810 Color: 0
Size: 2907 Color: 1
Size: 188 Color: 1

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 12568 Color: 1
Size: 2337 Color: 0

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 12680 Color: 1
Size: 2225 Color: 0

Bin 84: 8 of cap free
Amount of items: 3
Items: 
Size: 8326 Color: 1
Size: 6204 Color: 1
Size: 374 Color: 0

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 11023 Color: 0
Size: 3881 Color: 1

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 11057 Color: 1
Size: 3847 Color: 0

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 11368 Color: 0
Size: 3536 Color: 1

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 12243 Color: 1
Size: 2661 Color: 0

Bin 89: 8 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 0
Size: 2424 Color: 1
Size: 110 Color: 1

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 13010 Color: 1
Size: 1894 Color: 0

Bin 91: 9 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 1
Size: 6213 Color: 0
Size: 368 Color: 0

Bin 92: 9 of cap free
Amount of items: 3
Items: 
Size: 9046 Color: 1
Size: 5489 Color: 0
Size: 368 Color: 1

Bin 93: 9 of cap free
Amount of items: 2
Items: 
Size: 12627 Color: 0
Size: 2276 Color: 1

Bin 94: 9 of cap free
Amount of items: 2
Items: 
Size: 12663 Color: 0
Size: 2240 Color: 1

Bin 95: 10 of cap free
Amount of items: 2
Items: 
Size: 10008 Color: 1
Size: 4894 Color: 0

Bin 96: 10 of cap free
Amount of items: 2
Items: 
Size: 11721 Color: 1
Size: 3181 Color: 0

Bin 97: 10 of cap free
Amount of items: 2
Items: 
Size: 12894 Color: 0
Size: 2008 Color: 1

Bin 98: 10 of cap free
Amount of items: 2
Items: 
Size: 13260 Color: 1
Size: 1642 Color: 0

Bin 99: 10 of cap free
Amount of items: 2
Items: 
Size: 13354 Color: 0
Size: 1548 Color: 1

Bin 100: 11 of cap free
Amount of items: 2
Items: 
Size: 11341 Color: 0
Size: 3560 Color: 1

Bin 101: 11 of cap free
Amount of items: 2
Items: 
Size: 12155 Color: 1
Size: 2746 Color: 0

Bin 102: 11 of cap free
Amount of items: 2
Items: 
Size: 12834 Color: 1
Size: 2067 Color: 0

Bin 103: 11 of cap free
Amount of items: 2
Items: 
Size: 13121 Color: 0
Size: 1780 Color: 1

Bin 104: 12 of cap free
Amount of items: 3
Items: 
Size: 7476 Color: 0
Size: 6184 Color: 1
Size: 1240 Color: 0

Bin 105: 13 of cap free
Amount of items: 3
Items: 
Size: 11161 Color: 0
Size: 3482 Color: 1
Size: 256 Color: 0

Bin 106: 13 of cap free
Amount of items: 2
Items: 
Size: 13057 Color: 0
Size: 1842 Color: 1

Bin 107: 14 of cap free
Amount of items: 3
Items: 
Size: 11097 Color: 1
Size: 3161 Color: 0
Size: 640 Color: 0

Bin 108: 14 of cap free
Amount of items: 2
Items: 
Size: 11980 Color: 1
Size: 2918 Color: 0

Bin 109: 14 of cap free
Amount of items: 2
Items: 
Size: 12706 Color: 0
Size: 2192 Color: 1

Bin 110: 14 of cap free
Amount of items: 2
Items: 
Size: 12936 Color: 1
Size: 1962 Color: 0

Bin 111: 15 of cap free
Amount of items: 3
Items: 
Size: 8329 Color: 1
Size: 6216 Color: 0
Size: 352 Color: 1

Bin 112: 15 of cap free
Amount of items: 2
Items: 
Size: 10012 Color: 0
Size: 4885 Color: 1

Bin 113: 15 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 0
Size: 1757 Color: 1
Size: 152 Color: 1

Bin 114: 16 of cap free
Amount of items: 2
Items: 
Size: 13060 Color: 1
Size: 1836 Color: 0

Bin 115: 16 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 1
Size: 1664 Color: 0
Size: 32 Color: 1

Bin 116: 16 of cap free
Amount of items: 2
Items: 
Size: 13330 Color: 1
Size: 1566 Color: 0

Bin 117: 17 of cap free
Amount of items: 2
Items: 
Size: 11909 Color: 1
Size: 2986 Color: 0

Bin 118: 17 of cap free
Amount of items: 2
Items: 
Size: 13370 Color: 1
Size: 1525 Color: 0

Bin 119: 18 of cap free
Amount of items: 2
Items: 
Size: 12946 Color: 0
Size: 1948 Color: 1

Bin 120: 20 of cap free
Amount of items: 2
Items: 
Size: 12770 Color: 1
Size: 2122 Color: 0

Bin 121: 20 of cap free
Amount of items: 2
Items: 
Size: 13188 Color: 0
Size: 1704 Color: 1

Bin 122: 21 of cap free
Amount of items: 2
Items: 
Size: 12260 Color: 1
Size: 2631 Color: 0

Bin 123: 21 of cap free
Amount of items: 2
Items: 
Size: 12305 Color: 1
Size: 2586 Color: 0

Bin 124: 21 of cap free
Amount of items: 2
Items: 
Size: 12388 Color: 1
Size: 2503 Color: 0

Bin 125: 22 of cap free
Amount of items: 2
Items: 
Size: 12930 Color: 1
Size: 1960 Color: 0

Bin 126: 22 of cap free
Amount of items: 2
Items: 
Size: 13214 Color: 0
Size: 1676 Color: 1

Bin 127: 23 of cap free
Amount of items: 3
Items: 
Size: 10733 Color: 0
Size: 3978 Color: 1
Size: 178 Color: 1

Bin 128: 24 of cap free
Amount of items: 2
Items: 
Size: 10704 Color: 1
Size: 4184 Color: 0

Bin 129: 24 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 0
Size: 2736 Color: 1

Bin 130: 25 of cap free
Amount of items: 2
Items: 
Size: 11746 Color: 0
Size: 3141 Color: 1

Bin 131: 25 of cap free
Amount of items: 2
Items: 
Size: 11874 Color: 0
Size: 3013 Color: 1

Bin 132: 25 of cap free
Amount of items: 2
Items: 
Size: 13086 Color: 0
Size: 1801 Color: 1

Bin 133: 27 of cap free
Amount of items: 2
Items: 
Size: 12997 Color: 0
Size: 1888 Color: 1

Bin 134: 28 of cap free
Amount of items: 2
Items: 
Size: 11428 Color: 1
Size: 3456 Color: 0

Bin 135: 28 of cap free
Amount of items: 2
Items: 
Size: 12200 Color: 0
Size: 2684 Color: 1

Bin 136: 28 of cap free
Amount of items: 3
Items: 
Size: 12304 Color: 1
Size: 2444 Color: 0
Size: 136 Color: 1

Bin 137: 29 of cap free
Amount of items: 3
Items: 
Size: 13266 Color: 0
Size: 1607 Color: 1
Size: 10 Color: 0

Bin 138: 30 of cap free
Amount of items: 2
Items: 
Size: 11720 Color: 1
Size: 3162 Color: 0

Bin 139: 30 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 0
Size: 1654 Color: 1
Size: 68 Color: 1

Bin 140: 32 of cap free
Amount of items: 2
Items: 
Size: 11960 Color: 0
Size: 2920 Color: 1

Bin 141: 32 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 1
Size: 2264 Color: 0

Bin 142: 33 of cap free
Amount of items: 4
Items: 
Size: 9896 Color: 0
Size: 2299 Color: 1
Size: 2020 Color: 0
Size: 664 Color: 1

Bin 143: 35 of cap free
Amount of items: 2
Items: 
Size: 11016 Color: 1
Size: 3861 Color: 0

Bin 144: 36 of cap free
Amount of items: 2
Items: 
Size: 9492 Color: 0
Size: 5384 Color: 1

Bin 145: 36 of cap free
Amount of items: 2
Items: 
Size: 12492 Color: 0
Size: 2384 Color: 1

Bin 146: 37 of cap free
Amount of items: 2
Items: 
Size: 12848 Color: 1
Size: 2027 Color: 0

Bin 147: 37 of cap free
Amount of items: 2
Items: 
Size: 12908 Color: 0
Size: 1967 Color: 1

Bin 148: 38 of cap free
Amount of items: 3
Items: 
Size: 10584 Color: 1
Size: 4098 Color: 0
Size: 192 Color: 0

Bin 149: 38 of cap free
Amount of items: 2
Items: 
Size: 10602 Color: 1
Size: 4272 Color: 0

Bin 150: 38 of cap free
Amount of items: 2
Items: 
Size: 11633 Color: 1
Size: 3241 Color: 0

Bin 151: 38 of cap free
Amount of items: 2
Items: 
Size: 11618 Color: 0
Size: 3256 Color: 1

Bin 152: 38 of cap free
Amount of items: 2
Items: 
Size: 12808 Color: 0
Size: 2066 Color: 1

Bin 153: 40 of cap free
Amount of items: 3
Items: 
Size: 10868 Color: 0
Size: 2908 Color: 0
Size: 1096 Color: 1

Bin 154: 40 of cap free
Amount of items: 2
Items: 
Size: 11664 Color: 0
Size: 3208 Color: 1

Bin 155: 40 of cap free
Amount of items: 2
Items: 
Size: 12660 Color: 0
Size: 2212 Color: 1

Bin 156: 41 of cap free
Amount of items: 2
Items: 
Size: 13112 Color: 0
Size: 1759 Color: 1

Bin 157: 42 of cap free
Amount of items: 2
Items: 
Size: 9376 Color: 0
Size: 5494 Color: 1

Bin 158: 43 of cap free
Amount of items: 2
Items: 
Size: 12716 Color: 1
Size: 2153 Color: 0

Bin 159: 43 of cap free
Amount of items: 2
Items: 
Size: 12985 Color: 1
Size: 1884 Color: 0

Bin 160: 44 of cap free
Amount of items: 9
Items: 
Size: 7464 Color: 1
Size: 1056 Color: 0
Size: 976 Color: 1
Size: 944 Color: 0
Size: 928 Color: 1
Size: 928 Color: 1
Size: 876 Color: 0
Size: 864 Color: 0
Size: 832 Color: 0

Bin 161: 45 of cap free
Amount of items: 2
Items: 
Size: 13098 Color: 1
Size: 1769 Color: 0

Bin 162: 50 of cap free
Amount of items: 2
Items: 
Size: 12504 Color: 1
Size: 2358 Color: 0

Bin 163: 55 of cap free
Amount of items: 3
Items: 
Size: 11121 Color: 1
Size: 3608 Color: 0
Size: 128 Color: 0

Bin 164: 55 of cap free
Amount of items: 2
Items: 
Size: 12553 Color: 0
Size: 2304 Color: 1

Bin 165: 56 of cap free
Amount of items: 2
Items: 
Size: 12240 Color: 1
Size: 2616 Color: 0

Bin 166: 57 of cap free
Amount of items: 2
Items: 
Size: 11330 Color: 0
Size: 3525 Color: 1

Bin 167: 62 of cap free
Amount of items: 2
Items: 
Size: 9360 Color: 0
Size: 5490 Color: 1

Bin 168: 63 of cap free
Amount of items: 2
Items: 
Size: 12921 Color: 1
Size: 1928 Color: 0

Bin 169: 67 of cap free
Amount of items: 2
Items: 
Size: 10693 Color: 1
Size: 4152 Color: 0

Bin 170: 70 of cap free
Amount of items: 2
Items: 
Size: 9706 Color: 1
Size: 5136 Color: 0

Bin 171: 72 of cap free
Amount of items: 2
Items: 
Size: 10906 Color: 1
Size: 3934 Color: 0

Bin 172: 72 of cap free
Amount of items: 2
Items: 
Size: 12368 Color: 1
Size: 2472 Color: 0

Bin 173: 76 of cap free
Amount of items: 4
Items: 
Size: 9657 Color: 1
Size: 1864 Color: 0
Size: 1768 Color: 0
Size: 1547 Color: 1

Bin 174: 79 of cap free
Amount of items: 2
Items: 
Size: 10452 Color: 0
Size: 4381 Color: 1

Bin 175: 80 of cap free
Amount of items: 2
Items: 
Size: 9808 Color: 1
Size: 5024 Color: 0

Bin 176: 81 of cap free
Amount of items: 2
Items: 
Size: 9948 Color: 0
Size: 4883 Color: 1

Bin 177: 82 of cap free
Amount of items: 2
Items: 
Size: 10738 Color: 0
Size: 4092 Color: 1

Bin 178: 83 of cap free
Amount of items: 2
Items: 
Size: 11757 Color: 1
Size: 3072 Color: 0

Bin 179: 83 of cap free
Amount of items: 2
Items: 
Size: 11852 Color: 1
Size: 2977 Color: 0

Bin 180: 89 of cap free
Amount of items: 2
Items: 
Size: 9655 Color: 0
Size: 5168 Color: 1

Bin 181: 94 of cap free
Amount of items: 2
Items: 
Size: 11064 Color: 0
Size: 3754 Color: 1

Bin 182: 97 of cap free
Amount of items: 2
Items: 
Size: 12642 Color: 0
Size: 2173 Color: 1

Bin 183: 99 of cap free
Amount of items: 2
Items: 
Size: 11145 Color: 1
Size: 3668 Color: 0

Bin 184: 104 of cap free
Amount of items: 2
Items: 
Size: 11466 Color: 0
Size: 3342 Color: 1

Bin 185: 111 of cap free
Amount of items: 5
Items: 
Size: 9042 Color: 0
Size: 1470 Color: 0
Size: 1444 Color: 1
Size: 1427 Color: 1
Size: 1418 Color: 0

Bin 186: 112 of cap free
Amount of items: 2
Items: 
Size: 10072 Color: 1
Size: 4728 Color: 0

Bin 187: 115 of cap free
Amount of items: 2
Items: 
Size: 11425 Color: 1
Size: 3372 Color: 0

Bin 188: 131 of cap free
Amount of items: 2
Items: 
Size: 10257 Color: 1
Size: 4524 Color: 0

Bin 189: 132 of cap free
Amount of items: 2
Items: 
Size: 11297 Color: 0
Size: 3483 Color: 1

Bin 190: 134 of cap free
Amount of items: 37
Items: 
Size: 488 Color: 1
Size: 484 Color: 1
Size: 480 Color: 1
Size: 480 Color: 1
Size: 458 Color: 1
Size: 456 Color: 1
Size: 456 Color: 1
Size: 448 Color: 1
Size: 448 Color: 1
Size: 448 Color: 0
Size: 444 Color: 0
Size: 440 Color: 1
Size: 440 Color: 1
Size: 430 Color: 0
Size: 420 Color: 1
Size: 416 Color: 0
Size: 416 Color: 0
Size: 412 Color: 0
Size: 400 Color: 1
Size: 400 Color: 1
Size: 388 Color: 1
Size: 384 Color: 0
Size: 384 Color: 0
Size: 364 Color: 0
Size: 360 Color: 0
Size: 360 Color: 0
Size: 352 Color: 0
Size: 352 Color: 0
Size: 350 Color: 0
Size: 344 Color: 0
Size: 336 Color: 1
Size: 328 Color: 1
Size: 328 Color: 0
Size: 324 Color: 0
Size: 320 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0

Bin 191: 136 of cap free
Amount of items: 2
Items: 
Size: 9236 Color: 1
Size: 5540 Color: 0

Bin 192: 164 of cap free
Amount of items: 7
Items: 
Size: 7496 Color: 1
Size: 1324 Color: 0
Size: 1232 Color: 1
Size: 1232 Color: 0
Size: 1216 Color: 1
Size: 1152 Color: 1
Size: 1096 Color: 0

Bin 193: 181 of cap free
Amount of items: 2
Items: 
Size: 8520 Color: 0
Size: 6211 Color: 1

Bin 194: 210 of cap free
Amount of items: 2
Items: 
Size: 9642 Color: 0
Size: 5060 Color: 1

Bin 195: 220 of cap free
Amount of items: 2
Items: 
Size: 8468 Color: 1
Size: 6224 Color: 0

Bin 196: 236 of cap free
Amount of items: 2
Items: 
Size: 9304 Color: 0
Size: 5372 Color: 1

Bin 197: 246 of cap free
Amount of items: 13
Items: 
Size: 7458 Color: 1
Size: 648 Color: 1
Size: 642 Color: 1
Size: 640 Color: 1
Size: 624 Color: 0
Size: 602 Color: 0
Size: 600 Color: 0
Size: 596 Color: 1
Size: 580 Color: 0
Size: 576 Color: 0
Size: 576 Color: 0
Size: 576 Color: 0
Size: 548 Color: 1

Bin 198: 246 of cap free
Amount of items: 2
Items: 
Size: 8456 Color: 0
Size: 6210 Color: 1

Bin 199: 8876 of cap free
Amount of items: 21
Items: 
Size: 320 Color: 0
Size: 320 Color: 0
Size: 318 Color: 1
Size: 312 Color: 0
Size: 308 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 298 Color: 1
Size: 296 Color: 1
Size: 292 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0
Size: 280 Color: 1
Size: 280 Color: 0
Size: 276 Color: 0
Size: 272 Color: 0
Size: 256 Color: 1
Size: 256 Color: 1
Size: 256 Color: 1
Size: 256 Color: 1
Size: 256 Color: 0

Total size: 2952576
Total free space: 14912

