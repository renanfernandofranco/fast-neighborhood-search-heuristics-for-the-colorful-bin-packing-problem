Capicity Bin: 2020
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1025 Color: 4
Size: 831 Color: 12
Size: 92 Color: 14
Size: 72 Color: 6

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1505 Color: 15
Size: 375 Color: 12
Size: 88 Color: 7
Size: 52 Color: 15

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1501 Color: 0
Size: 439 Color: 19
Size: 44 Color: 4
Size: 36 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 6
Size: 202 Color: 2
Size: 60 Color: 2

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1590 Color: 8
Size: 274 Color: 6
Size: 88 Color: 5
Size: 68 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 4
Size: 290 Color: 18
Size: 56 Color: 11

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 910 Color: 14
Size: 654 Color: 18
Size: 416 Color: 5
Size: 40 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 7
Size: 406 Color: 10
Size: 224 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1114 Color: 7
Size: 820 Color: 10
Size: 86 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1769 Color: 4
Size: 211 Color: 9
Size: 40 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 7
Size: 414 Color: 17
Size: 148 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1315 Color: 2
Size: 589 Color: 14
Size: 116 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 5
Size: 164 Color: 15
Size: 74 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 2
Size: 383 Color: 5
Size: 76 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 7
Size: 578 Color: 1
Size: 112 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 19
Size: 433 Color: 17
Size: 96 Color: 11

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 1396 Color: 4
Size: 468 Color: 4
Size: 128 Color: 7
Size: 28 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1311 Color: 5
Size: 591 Color: 18
Size: 118 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 11
Size: 833 Color: 10
Size: 166 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 10
Size: 310 Color: 12
Size: 60 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 18
Size: 189 Color: 12
Size: 36 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 6
Size: 218 Color: 10
Size: 40 Color: 2

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 1571 Color: 16
Size: 441 Color: 7
Size: 4 Color: 10
Size: 4 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1182 Color: 11
Size: 702 Color: 17
Size: 136 Color: 12

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 5
Size: 249 Color: 16
Size: 4 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 19
Size: 200 Color: 1
Size: 40 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 3
Size: 309 Color: 8
Size: 60 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 11
Size: 334 Color: 5
Size: 64 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 0
Size: 242 Color: 19
Size: 44 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 1
Size: 731 Color: 15
Size: 118 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1497 Color: 11
Size: 437 Color: 16
Size: 86 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 6
Size: 222 Color: 11
Size: 104 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1293 Color: 2
Size: 607 Color: 15
Size: 120 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 16
Size: 378 Color: 10
Size: 72 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1017 Color: 15
Size: 837 Color: 10
Size: 166 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1268 Color: 5
Size: 676 Color: 1
Size: 76 Color: 9

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 4
Size: 462 Color: 16
Size: 24 Color: 7

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 3
Size: 379 Color: 8
Size: 74 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 18
Size: 245 Color: 6
Size: 48 Color: 12

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 13
Size: 182 Color: 14
Size: 32 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 3
Size: 311 Color: 7
Size: 60 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 16
Size: 503 Color: 3
Size: 100 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 0
Size: 526 Color: 8
Size: 24 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1011 Color: 10
Size: 841 Color: 13
Size: 168 Color: 12

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 0
Size: 349 Color: 10
Size: 68 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 15
Size: 231 Color: 13
Size: 44 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 5
Size: 164 Color: 5
Size: 44 Color: 16

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 8
Size: 470 Color: 17
Size: 312 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1014 Color: 13
Size: 826 Color: 6
Size: 180 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 17
Size: 711 Color: 11
Size: 142 Color: 8

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1030 Color: 12
Size: 758 Color: 7
Size: 232 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1195 Color: 13
Size: 689 Color: 9
Size: 136 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 8
Size: 273 Color: 15
Size: 54 Color: 17

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 17
Size: 267 Color: 8
Size: 52 Color: 17

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1013 Color: 7
Size: 841 Color: 3
Size: 166 Color: 9

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 7
Size: 351 Color: 16
Size: 70 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 11
Size: 237 Color: 9
Size: 46 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 2
Size: 359 Color: 8
Size: 70 Color: 12

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 9
Size: 277 Color: 8
Size: 54 Color: 12

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 15
Size: 273 Color: 16
Size: 50 Color: 6

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 15
Size: 233 Color: 15
Size: 46 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 12
Size: 241 Color: 19
Size: 46 Color: 6

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 7
Size: 227 Color: 6
Size: 2 Color: 19

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 0
Size: 509 Color: 4
Size: 100 Color: 10

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 10
Size: 355 Color: 2
Size: 70 Color: 17

Total size: 131300
Total free space: 0

