Capicity Bin: 15744
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 7898 Color: 3
Size: 6542 Color: 2
Size: 1304 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 7932 Color: 4
Size: 6516 Color: 2
Size: 1296 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8988 Color: 1
Size: 5636 Color: 4
Size: 1120 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9096 Color: 4
Size: 5344 Color: 4
Size: 1304 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9296 Color: 0
Size: 5392 Color: 4
Size: 1056 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 3
Size: 5686 Color: 1
Size: 738 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11432 Color: 0
Size: 3208 Color: 3
Size: 1104 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11327 Color: 3
Size: 2409 Color: 4
Size: 2008 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 0
Size: 3608 Color: 3
Size: 664 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11888 Color: 3
Size: 3272 Color: 0
Size: 584 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11944 Color: 0
Size: 3146 Color: 3
Size: 654 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11976 Color: 0
Size: 3176 Color: 2
Size: 592 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 0
Size: 3120 Color: 4
Size: 584 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 4
Size: 3122 Color: 0
Size: 620 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12285 Color: 0
Size: 3107 Color: 4
Size: 352 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 0
Size: 2768 Color: 2
Size: 652 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 4
Size: 3076 Color: 3
Size: 292 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12400 Color: 2
Size: 2800 Color: 4
Size: 544 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12432 Color: 3
Size: 2772 Color: 2
Size: 540 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12682 Color: 3
Size: 2554 Color: 0
Size: 508 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 3
Size: 2432 Color: 1
Size: 568 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 2
Size: 2536 Color: 3
Size: 432 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12946 Color: 0
Size: 2334 Color: 4
Size: 464 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 1
Size: 2488 Color: 0
Size: 332 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13108 Color: 4
Size: 1708 Color: 0
Size: 928 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13124 Color: 3
Size: 1760 Color: 3
Size: 860 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 0
Size: 1978 Color: 1
Size: 616 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 4
Size: 2188 Color: 0
Size: 284 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 3
Size: 1296 Color: 0
Size: 1132 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 1
Size: 2096 Color: 2
Size: 312 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13340 Color: 4
Size: 1280 Color: 3
Size: 1124 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13374 Color: 3
Size: 1894 Color: 4
Size: 476 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13401 Color: 4
Size: 1953 Color: 2
Size: 390 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13442 Color: 0
Size: 1438 Color: 1
Size: 864 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 1
Size: 2004 Color: 0
Size: 276 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13476 Color: 4
Size: 1136 Color: 0
Size: 1132 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 4
Size: 1896 Color: 4
Size: 288 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13602 Color: 3
Size: 1774 Color: 1
Size: 368 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13582 Color: 2
Size: 1682 Color: 3
Size: 480 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 1
Size: 1448 Color: 2
Size: 628 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 4
Size: 1468 Color: 2
Size: 576 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13730 Color: 1
Size: 1358 Color: 3
Size: 656 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 2
Size: 1384 Color: 4
Size: 640 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 1
Size: 1424 Color: 3
Size: 544 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 3
Size: 1572 Color: 2
Size: 356 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13936 Color: 2
Size: 1520 Color: 1
Size: 288 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13886 Color: 0
Size: 1730 Color: 2
Size: 128 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 2
Size: 1336 Color: 4
Size: 464 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13978 Color: 2
Size: 1382 Color: 0
Size: 384 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13990 Color: 2
Size: 1558 Color: 1
Size: 196 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14000 Color: 3
Size: 1456 Color: 0
Size: 288 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14022 Color: 3
Size: 984 Color: 3
Size: 738 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14064 Color: 0
Size: 1648 Color: 3
Size: 32 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14088 Color: 2
Size: 1368 Color: 1
Size: 288 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 0
Size: 1152 Color: 4
Size: 474 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14128 Color: 4
Size: 1304 Color: 2
Size: 312 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14156 Color: 3
Size: 1124 Color: 2
Size: 464 Color: 4

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 7884 Color: 4
Size: 6551 Color: 1
Size: 1308 Color: 3

Bin 59: 1 of cap free
Amount of items: 4
Items: 
Size: 7890 Color: 1
Size: 6557 Color: 2
Size: 960 Color: 3
Size: 336 Color: 1

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11211 Color: 0
Size: 4276 Color: 1
Size: 256 Color: 1

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 11184 Color: 2
Size: 4295 Color: 3
Size: 264 Color: 3

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 11805 Color: 0
Size: 2354 Color: 2
Size: 1584 Color: 1

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11815 Color: 1
Size: 3632 Color: 0
Size: 296 Color: 2

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11831 Color: 2
Size: 3688 Color: 1
Size: 224 Color: 0

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12613 Color: 4
Size: 2570 Color: 1
Size: 560 Color: 2

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12925 Color: 0
Size: 2394 Color: 4
Size: 424 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 1
Size: 2351 Color: 0
Size: 360 Color: 2

Bin 68: 2 of cap free
Amount of items: 9
Items: 
Size: 7876 Color: 4
Size: 1360 Color: 0
Size: 1310 Color: 3
Size: 1308 Color: 4
Size: 1308 Color: 3
Size: 976 Color: 1
Size: 736 Color: 1
Size: 480 Color: 2
Size: 388 Color: 1

Bin 69: 2 of cap free
Amount of items: 7
Items: 
Size: 7880 Color: 0
Size: 1922 Color: 3
Size: 1922 Color: 2
Size: 1764 Color: 3
Size: 1474 Color: 4
Size: 400 Color: 1
Size: 380 Color: 4

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 7916 Color: 2
Size: 6546 Color: 0
Size: 1280 Color: 2

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 8996 Color: 3
Size: 5622 Color: 0
Size: 1124 Color: 2

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 10606 Color: 2
Size: 4336 Color: 0
Size: 800 Color: 3

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 11694 Color: 1
Size: 4048 Color: 4

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 12034 Color: 1
Size: 3308 Color: 0
Size: 400 Color: 4

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 12212 Color: 4
Size: 3186 Color: 0
Size: 344 Color: 2

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 12500 Color: 1
Size: 3242 Color: 3

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 1
Size: 2590 Color: 0
Size: 520 Color: 4

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 12690 Color: 3
Size: 2652 Color: 4
Size: 400 Color: 0

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 12914 Color: 0
Size: 2072 Color: 2
Size: 756 Color: 4

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 1
Size: 1304 Color: 4
Size: 1200 Color: 0

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 3
Size: 1548 Color: 2
Size: 576 Color: 0

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 1
Size: 1056 Color: 3
Size: 832 Color: 2

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 14152 Color: 1
Size: 1578 Color: 4
Size: 12 Color: 1

Bin 84: 3 of cap free
Amount of items: 9
Items: 
Size: 7873 Color: 0
Size: 1308 Color: 0
Size: 1120 Color: 2
Size: 1120 Color: 2
Size: 976 Color: 4
Size: 976 Color: 2
Size: 960 Color: 2
Size: 736 Color: 4
Size: 672 Color: 1

Bin 85: 3 of cap free
Amount of items: 4
Items: 
Size: 7912 Color: 4
Size: 6561 Color: 0
Size: 980 Color: 3
Size: 288 Color: 4

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 9851 Color: 1
Size: 5638 Color: 3
Size: 252 Color: 4

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 9855 Color: 2
Size: 5630 Color: 1
Size: 256 Color: 2

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 11201 Color: 0
Size: 4284 Color: 2
Size: 256 Color: 4

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 0
Size: 4185 Color: 4
Size: 328 Color: 2

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 11954 Color: 4
Size: 3787 Color: 1

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 12224 Color: 4
Size: 3261 Color: 0
Size: 256 Color: 2

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 0
Size: 2377 Color: 3
Size: 800 Color: 2

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 13149 Color: 4
Size: 2040 Color: 3
Size: 552 Color: 0

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 9876 Color: 0
Size: 5544 Color: 1
Size: 320 Color: 4

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 11662 Color: 4
Size: 3542 Color: 2
Size: 536 Color: 2

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 12906 Color: 2
Size: 2482 Color: 4
Size: 352 Color: 0

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 1
Size: 1936 Color: 4

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 13872 Color: 4
Size: 1868 Color: 1

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 0
Size: 1752 Color: 1

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 3
Size: 1732 Color: 4

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 14084 Color: 2
Size: 1632 Color: 1
Size: 24 Color: 0

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 0
Size: 1620 Color: 3

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 11182 Color: 4
Size: 4285 Color: 0
Size: 272 Color: 1

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 11315 Color: 0
Size: 4088 Color: 3
Size: 336 Color: 2

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 12235 Color: 3
Size: 3504 Color: 2

Bin 106: 6 of cap free
Amount of items: 4
Items: 
Size: 7888 Color: 0
Size: 6554 Color: 2
Size: 848 Color: 3
Size: 448 Color: 3

Bin 107: 6 of cap free
Amount of items: 3
Items: 
Size: 11498 Color: 2
Size: 3824 Color: 0
Size: 416 Color: 1

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 12855 Color: 1
Size: 2883 Color: 2

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 2
Size: 2434 Color: 1

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 13474 Color: 4
Size: 2264 Color: 2

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 13710 Color: 0
Size: 2028 Color: 4

Bin 112: 8 of cap free
Amount of items: 3
Items: 
Size: 9840 Color: 4
Size: 5628 Color: 1
Size: 268 Color: 4

Bin 113: 8 of cap free
Amount of items: 3
Items: 
Size: 10574 Color: 3
Size: 2980 Color: 0
Size: 2182 Color: 4

Bin 114: 8 of cap free
Amount of items: 3
Items: 
Size: 10896 Color: 3
Size: 4200 Color: 0
Size: 640 Color: 2

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 11732 Color: 3
Size: 3492 Color: 0
Size: 512 Color: 2

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 12922 Color: 1
Size: 2814 Color: 3

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13232 Color: 2
Size: 2504 Color: 3

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 13824 Color: 4
Size: 1912 Color: 1

Bin 119: 9 of cap free
Amount of items: 3
Items: 
Size: 10587 Color: 4
Size: 4892 Color: 1
Size: 256 Color: 3

Bin 120: 9 of cap free
Amount of items: 3
Items: 
Size: 10603 Color: 4
Size: 4664 Color: 0
Size: 468 Color: 2

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 9846 Color: 2
Size: 5888 Color: 4

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 10546 Color: 0
Size: 4900 Color: 4
Size: 288 Color: 0

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 13322 Color: 1
Size: 2412 Color: 4

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 13570 Color: 3
Size: 2163 Color: 4

Bin 125: 13 of cap free
Amount of items: 3
Items: 
Size: 8971 Color: 1
Size: 6456 Color: 1
Size: 304 Color: 2

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 12382 Color: 2
Size: 3348 Color: 3

Bin 127: 14 of cap free
Amount of items: 2
Items: 
Size: 13130 Color: 1
Size: 2600 Color: 2

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 13123 Color: 2
Size: 2607 Color: 3

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 14086 Color: 3
Size: 1644 Color: 0

Bin 130: 15 of cap free
Amount of items: 7
Items: 
Size: 7877 Color: 2
Size: 1728 Color: 0
Size: 1688 Color: 3
Size: 1512 Color: 2
Size: 1310 Color: 4
Size: 1310 Color: 1
Size: 304 Color: 0

Bin 131: 15 of cap free
Amount of items: 3
Items: 
Size: 11311 Color: 0
Size: 3094 Color: 3
Size: 1324 Color: 1

Bin 132: 17 of cap free
Amount of items: 2
Items: 
Size: 13315 Color: 2
Size: 2412 Color: 4

Bin 133: 18 of cap free
Amount of items: 2
Items: 
Size: 12874 Color: 3
Size: 2852 Color: 1

Bin 134: 18 of cap free
Amount of items: 3
Items: 
Size: 13860 Color: 1
Size: 1802 Color: 3
Size: 64 Color: 4

Bin 135: 19 of cap free
Amount of items: 4
Items: 
Size: 7885 Color: 1
Size: 6556 Color: 2
Size: 980 Color: 0
Size: 304 Color: 0

Bin 136: 20 of cap free
Amount of items: 3
Items: 
Size: 9976 Color: 0
Size: 4890 Color: 1
Size: 858 Color: 3

Bin 137: 20 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 2
Size: 2576 Color: 3

Bin 138: 20 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 4
Size: 2164 Color: 2
Size: 160 Color: 1

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 3
Size: 1832 Color: 1

Bin 140: 21 of cap free
Amount of items: 2
Items: 
Size: 13772 Color: 3
Size: 1951 Color: 0

Bin 141: 22 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 2
Size: 4282 Color: 4
Size: 528 Color: 2

Bin 142: 22 of cap free
Amount of items: 3
Items: 
Size: 13636 Color: 3
Size: 2022 Color: 0
Size: 64 Color: 3

Bin 143: 27 of cap free
Amount of items: 3
Items: 
Size: 10152 Color: 0
Size: 4909 Color: 1
Size: 656 Color: 4

Bin 144: 28 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 1
Size: 3616 Color: 4
Size: 320 Color: 0

Bin 145: 28 of cap free
Amount of items: 2
Items: 
Size: 13008 Color: 1
Size: 2708 Color: 2

Bin 146: 29 of cap free
Amount of items: 2
Items: 
Size: 12571 Color: 3
Size: 3144 Color: 1

Bin 147: 30 of cap free
Amount of items: 2
Items: 
Size: 11748 Color: 2
Size: 3966 Color: 4

Bin 148: 30 of cap free
Amount of items: 2
Items: 
Size: 11912 Color: 3
Size: 3802 Color: 2

Bin 149: 30 of cap free
Amount of items: 2
Items: 
Size: 12172 Color: 2
Size: 3542 Color: 1

Bin 150: 31 of cap free
Amount of items: 2
Items: 
Size: 12617 Color: 0
Size: 3096 Color: 2

Bin 151: 31 of cap free
Amount of items: 2
Items: 
Size: 13688 Color: 3
Size: 2025 Color: 4

Bin 152: 32 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 4
Size: 2288 Color: 3

Bin 153: 33 of cap free
Amount of items: 2
Items: 
Size: 12016 Color: 1
Size: 3695 Color: 2

Bin 154: 36 of cap free
Amount of items: 2
Items: 
Size: 13932 Color: 3
Size: 1776 Color: 0

Bin 155: 38 of cap free
Amount of items: 29
Items: 
Size: 832 Color: 0
Size: 752 Color: 0
Size: 704 Color: 1
Size: 654 Color: 2
Size: 624 Color: 4
Size: 624 Color: 1
Size: 608 Color: 4
Size: 608 Color: 2
Size: 604 Color: 1
Size: 584 Color: 2
Size: 580 Color: 4
Size: 576 Color: 3
Size: 560 Color: 3
Size: 560 Color: 3
Size: 528 Color: 0
Size: 512 Color: 0
Size: 504 Color: 3
Size: 496 Color: 4
Size: 496 Color: 4
Size: 480 Color: 3
Size: 468 Color: 4
Size: 468 Color: 2
Size: 448 Color: 0
Size: 436 Color: 1
Size: 432 Color: 3
Size: 432 Color: 1
Size: 432 Color: 1
Size: 416 Color: 3
Size: 288 Color: 1

Bin 156: 38 of cap free
Amount of items: 2
Items: 
Size: 13616 Color: 1
Size: 2090 Color: 3

Bin 157: 43 of cap free
Amount of items: 3
Items: 
Size: 8945 Color: 2
Size: 5368 Color: 0
Size: 1388 Color: 1

Bin 158: 44 of cap free
Amount of items: 2
Items: 
Size: 12752 Color: 4
Size: 2948 Color: 1

Bin 159: 49 of cap free
Amount of items: 2
Items: 
Size: 12420 Color: 4
Size: 3275 Color: 1

Bin 160: 49 of cap free
Amount of items: 2
Items: 
Size: 12893 Color: 3
Size: 2802 Color: 1

Bin 161: 50 of cap free
Amount of items: 2
Items: 
Size: 11922 Color: 2
Size: 3772 Color: 3

Bin 162: 51 of cap free
Amount of items: 2
Items: 
Size: 13048 Color: 4
Size: 2645 Color: 1

Bin 163: 51 of cap free
Amount of items: 2
Items: 
Size: 13508 Color: 2
Size: 2185 Color: 1

Bin 164: 58 of cap free
Amount of items: 3
Items: 
Size: 8990 Color: 0
Size: 5936 Color: 2
Size: 760 Color: 1

Bin 165: 67 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 1
Size: 3271 Color: 2
Size: 1786 Color: 3

Bin 166: 70 of cap free
Amount of items: 2
Items: 
Size: 9006 Color: 4
Size: 6668 Color: 3

Bin 167: 71 of cap free
Amount of items: 5
Items: 
Size: 7881 Color: 0
Size: 2362 Color: 0
Size: 2022 Color: 1
Size: 1892 Color: 4
Size: 1516 Color: 3

Bin 168: 76 of cap free
Amount of items: 3
Items: 
Size: 8922 Color: 0
Size: 5618 Color: 0
Size: 1128 Color: 3

Bin 169: 78 of cap free
Amount of items: 2
Items: 
Size: 12370 Color: 2
Size: 3296 Color: 4

Bin 170: 80 of cap free
Amount of items: 2
Items: 
Size: 10856 Color: 0
Size: 4808 Color: 3

Bin 171: 83 of cap free
Amount of items: 2
Items: 
Size: 11970 Color: 3
Size: 3691 Color: 2

Bin 172: 84 of cap free
Amount of items: 2
Items: 
Size: 12852 Color: 2
Size: 2808 Color: 3

Bin 173: 88 of cap free
Amount of items: 4
Items: 
Size: 7882 Color: 1
Size: 3378 Color: 0
Size: 2356 Color: 3
Size: 2040 Color: 2

Bin 174: 88 of cap free
Amount of items: 2
Items: 
Size: 10712 Color: 3
Size: 4944 Color: 2

Bin 175: 88 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 1
Size: 2944 Color: 2

Bin 176: 90 of cap free
Amount of items: 2
Items: 
Size: 11320 Color: 4
Size: 4334 Color: 3

Bin 177: 91 of cap free
Amount of items: 2
Items: 
Size: 13405 Color: 4
Size: 2248 Color: 1

Bin 178: 108 of cap free
Amount of items: 11
Items: 
Size: 7874 Color: 2
Size: 944 Color: 2
Size: 858 Color: 0
Size: 856 Color: 2
Size: 856 Color: 2
Size: 856 Color: 0
Size: 752 Color: 4
Size: 736 Color: 1
Size: 704 Color: 1
Size: 624 Color: 3
Size: 576 Color: 3

Bin 179: 148 of cap free
Amount of items: 2
Items: 
Size: 11832 Color: 2
Size: 3764 Color: 3

Bin 180: 157 of cap free
Amount of items: 2
Items: 
Size: 12662 Color: 3
Size: 2925 Color: 1

Bin 181: 171 of cap free
Amount of items: 3
Items: 
Size: 7900 Color: 4
Size: 6553 Color: 1
Size: 1120 Color: 2

Bin 182: 184 of cap free
Amount of items: 2
Items: 
Size: 8998 Color: 2
Size: 6562 Color: 3

Bin 183: 186 of cap free
Amount of items: 3
Items: 
Size: 8008 Color: 2
Size: 6568 Color: 4
Size: 982 Color: 3

Bin 184: 186 of cap free
Amount of items: 2
Items: 
Size: 8982 Color: 1
Size: 6576 Color: 4

Bin 185: 194 of cap free
Amount of items: 2
Items: 
Size: 9878 Color: 2
Size: 5672 Color: 4

Bin 186: 200 of cap free
Amount of items: 2
Items: 
Size: 8656 Color: 1
Size: 6888 Color: 0

Bin 187: 209 of cap free
Amount of items: 2
Items: 
Size: 9868 Color: 3
Size: 5667 Color: 0

Bin 188: 209 of cap free
Amount of items: 2
Items: 
Size: 10604 Color: 0
Size: 4931 Color: 1

Bin 189: 214 of cap free
Amount of items: 2
Items: 
Size: 11220 Color: 2
Size: 4310 Color: 1

Bin 190: 217 of cap free
Amount of items: 2
Items: 
Size: 12244 Color: 1
Size: 3283 Color: 4

Bin 191: 226 of cap free
Amount of items: 2
Items: 
Size: 9856 Color: 1
Size: 5662 Color: 0

Bin 192: 242 of cap free
Amount of items: 2
Items: 
Size: 10591 Color: 1
Size: 4911 Color: 4

Bin 193: 244 of cap free
Amount of items: 2
Items: 
Size: 11819 Color: 4
Size: 3681 Color: 1

Bin 194: 250 of cap free
Amount of items: 2
Items: 
Size: 10576 Color: 2
Size: 4918 Color: 1

Bin 195: 251 of cap free
Amount of items: 2
Items: 
Size: 11194 Color: 1
Size: 4299 Color: 4

Bin 196: 252 of cap free
Amount of items: 2
Items: 
Size: 8952 Color: 2
Size: 6540 Color: 3

Bin 197: 270 of cap free
Amount of items: 2
Items: 
Size: 8950 Color: 0
Size: 6524 Color: 3

Bin 198: 270 of cap free
Amount of items: 2
Items: 
Size: 9829 Color: 1
Size: 5645 Color: 0

Bin 199: 8876 of cap free
Amount of items: 18
Items: 
Size: 448 Color: 0
Size: 448 Color: 0
Size: 416 Color: 4
Size: 416 Color: 4
Size: 404 Color: 4
Size: 400 Color: 4
Size: 400 Color: 4
Size: 400 Color: 1
Size: 392 Color: 0
Size: 376 Color: 3
Size: 376 Color: 2
Size: 368 Color: 2
Size: 356 Color: 3
Size: 352 Color: 3
Size: 352 Color: 0
Size: 348 Color: 3
Size: 344 Color: 1
Size: 272 Color: 1

Total size: 3117312
Total free space: 15744

