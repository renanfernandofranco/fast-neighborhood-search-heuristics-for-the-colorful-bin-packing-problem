Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 11
Size: 279 Color: 11
Size: 256 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 330 Color: 14
Size: 297 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 267 Color: 14
Size: 255 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8
Size: 318 Color: 15
Size: 260 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 264 Color: 8
Size: 260 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 2
Size: 370 Color: 19
Size: 260 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 329 Color: 14
Size: 293 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 6
Size: 314 Color: 16
Size: 279 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 4
Size: 264 Color: 10
Size: 256 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 17
Size: 353 Color: 19
Size: 281 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 19
Size: 362 Color: 10
Size: 273 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 6
Size: 296 Color: 14
Size: 250 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 17
Size: 307 Color: 6
Size: 281 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 6
Size: 326 Color: 16
Size: 268 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 12
Size: 327 Color: 17
Size: 263 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 5
Size: 365 Color: 3
Size: 255 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 324 Color: 19
Size: 266 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 17
Size: 261 Color: 17
Size: 252 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 15
Size: 322 Color: 17
Size: 286 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 12
Size: 303 Color: 14
Size: 265 Color: 1

Total size: 20000
Total free space: 0

