Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 347 Color: 6
Size: 259 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 345 Color: 18
Size: 281 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 19
Size: 350 Color: 15
Size: 271 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 0
Size: 255 Color: 12
Size: 250 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 8
Size: 286 Color: 4
Size: 262 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 7
Size: 350 Color: 14
Size: 292 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 14
Size: 261 Color: 2
Size: 255 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 342 Color: 12
Size: 264 Color: 15

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 5
Size: 336 Color: 3
Size: 291 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 4
Size: 337 Color: 13
Size: 288 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 4
Size: 277 Color: 19
Size: 259 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 13
Size: 342 Color: 4
Size: 285 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 5
Size: 307 Color: 5
Size: 250 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 12
Size: 266 Color: 4
Size: 254 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 8
Size: 352 Color: 12
Size: 274 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 18
Size: 307 Color: 12
Size: 292 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6
Size: 350 Color: 15
Size: 293 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 302 Color: 1
Size: 265 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 11
Size: 296 Color: 5
Size: 260 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9
Size: 266 Color: 3
Size: 254 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 15
Size: 342 Color: 19
Size: 292 Color: 9

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 14
Size: 359 Color: 0
Size: 281 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 3
Size: 280 Color: 12
Size: 261 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 0
Size: 357 Color: 7
Size: 281 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 6
Size: 311 Color: 0
Size: 269 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 355 Color: 4
Size: 281 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 16
Size: 371 Color: 5
Size: 250 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 0
Size: 336 Color: 0
Size: 307 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 0
Size: 252 Color: 5
Size: 250 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 19
Size: 335 Color: 16
Size: 287 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 8
Size: 307 Color: 11
Size: 298 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 5
Size: 364 Color: 4
Size: 250 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 11
Size: 329 Color: 10
Size: 312 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 10
Size: 327 Color: 9
Size: 289 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 13
Size: 316 Color: 16
Size: 280 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 15
Size: 350 Color: 7
Size: 281 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 13
Size: 312 Color: 3
Size: 254 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 9
Size: 335 Color: 17
Size: 310 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 290 Color: 7
Size: 251 Color: 5

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 5
Size: 296 Color: 17
Size: 295 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 19
Size: 304 Color: 17
Size: 286 Color: 13

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 10
Size: 324 Color: 9
Size: 270 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 9
Size: 327 Color: 4
Size: 273 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 4
Size: 301 Color: 4
Size: 280 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7
Size: 328 Color: 14
Size: 299 Color: 8

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 314 Color: 16
Size: 286 Color: 6

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 4
Size: 303 Color: 14
Size: 253 Color: 19

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 5
Size: 327 Color: 8
Size: 306 Color: 15

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 4
Size: 281 Color: 7
Size: 278 Color: 18

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 14
Size: 288 Color: 0
Size: 257 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9
Size: 256 Color: 9
Size: 254 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 310 Color: 4
Size: 294 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 10
Size: 311 Color: 14
Size: 260 Color: 14

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 17
Size: 269 Color: 0
Size: 253 Color: 17

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 3
Size: 326 Color: 10
Size: 256 Color: 11

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 13
Size: 267 Color: 6
Size: 258 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 12
Size: 279 Color: 13
Size: 264 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 11
Size: 350 Color: 15
Size: 286 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 12
Size: 327 Color: 11
Size: 319 Color: 12

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 5
Size: 332 Color: 5
Size: 253 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 371 Color: 10
Size: 253 Color: 10

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 18
Size: 355 Color: 11
Size: 260 Color: 14

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 266 Color: 13
Size: 261 Color: 5

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 18
Size: 322 Color: 15
Size: 283 Color: 10

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 18
Size: 323 Color: 15
Size: 323 Color: 8

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 12
Size: 286 Color: 2
Size: 260 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 16
Size: 270 Color: 11
Size: 254 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 13
Size: 339 Color: 11
Size: 251 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 14
Size: 304 Color: 9
Size: 261 Color: 9

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 0
Size: 271 Color: 15
Size: 254 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 17
Size: 307 Color: 6
Size: 302 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 13
Size: 300 Color: 7
Size: 254 Color: 12

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 298 Color: 8
Size: 281 Color: 17

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 14
Size: 320 Color: 14
Size: 310 Color: 6

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 6
Size: 333 Color: 2
Size: 332 Color: 7

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 5
Size: 299 Color: 14
Size: 268 Color: 9

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 5
Size: 354 Color: 17
Size: 271 Color: 11

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 9
Size: 348 Color: 5
Size: 270 Color: 10

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7
Size: 327 Color: 0
Size: 302 Color: 15

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 329 Color: 17
Size: 274 Color: 8

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 16
Size: 343 Color: 7
Size: 253 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 9
Size: 321 Color: 17
Size: 250 Color: 19

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 5
Size: 296 Color: 0
Size: 269 Color: 4

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 295 Color: 10
Size: 261 Color: 15

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7
Size: 351 Color: 13
Size: 271 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 314 Color: 17
Size: 294 Color: 2

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 14
Size: 325 Color: 1
Size: 286 Color: 9

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 6
Size: 336 Color: 18
Size: 289 Color: 8

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 18
Size: 321 Color: 15
Size: 291 Color: 8

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 356 Color: 14
Size: 262 Color: 18

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 2
Size: 290 Color: 6
Size: 262 Color: 11

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 5
Size: 259 Color: 4
Size: 255 Color: 17

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 327 Color: 11
Size: 256 Color: 14

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 9
Size: 313 Color: 1
Size: 307 Color: 6

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 314 Color: 3
Size: 253 Color: 2

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 12
Size: 331 Color: 17
Size: 250 Color: 7

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 300 Color: 17
Size: 267 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 11
Size: 363 Color: 7
Size: 257 Color: 12

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 10
Size: 258 Color: 9
Size: 252 Color: 14

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 10
Size: 277 Color: 1
Size: 270 Color: 16

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 15
Size: 283 Color: 5
Size: 269 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 19
Size: 254 Color: 17
Size: 250 Color: 19

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 12
Size: 353 Color: 8
Size: 285 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 345 Color: 17
Size: 266 Color: 17

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 14
Size: 267 Color: 1
Size: 252 Color: 18

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 8
Size: 360 Color: 4
Size: 253 Color: 6

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 12
Size: 323 Color: 0
Size: 274 Color: 17

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 18
Size: 306 Color: 19
Size: 303 Color: 14

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 13
Size: 291 Color: 15
Size: 252 Color: 16

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 321 Color: 12
Size: 301 Color: 11

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 14
Size: 275 Color: 4
Size: 270 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 4
Size: 357 Color: 18
Size: 262 Color: 2

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 14
Size: 289 Color: 16
Size: 272 Color: 18

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 4
Size: 313 Color: 8
Size: 288 Color: 18

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 7
Size: 314 Color: 12
Size: 267 Color: 18

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 16
Size: 262 Color: 1
Size: 254 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 4
Size: 276 Color: 5
Size: 272 Color: 13

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 10
Size: 302 Color: 12
Size: 279 Color: 8

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 18
Size: 328 Color: 2
Size: 251 Color: 3

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8
Size: 332 Color: 18
Size: 263 Color: 19

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 7
Size: 298 Color: 17
Size: 274 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 14
Size: 282 Color: 4
Size: 250 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 14
Size: 315 Color: 13
Size: 254 Color: 10

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 17
Size: 289 Color: 1
Size: 267 Color: 14

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 277 Color: 7
Size: 273 Color: 13

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 17
Size: 299 Color: 9
Size: 267 Color: 5

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 290 Color: 16
Size: 282 Color: 5

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 19
Size: 318 Color: 14
Size: 304 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 19
Size: 280 Color: 5
Size: 257 Color: 8

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 326 Color: 11
Size: 292 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 19
Size: 324 Color: 13
Size: 304 Color: 18

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 14
Size: 250 Color: 8

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8
Size: 286 Color: 15
Size: 277 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 7
Size: 345 Color: 13
Size: 252 Color: 13

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 4
Size: 263 Color: 2
Size: 256 Color: 4

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 330 Color: 12
Size: 255 Color: 10

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 10
Size: 319 Color: 3
Size: 275 Color: 7

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 1
Size: 343 Color: 15
Size: 313 Color: 17

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 19
Size: 342 Color: 5
Size: 311 Color: 2

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 5
Size: 279 Color: 7
Size: 260 Color: 10

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 19
Size: 338 Color: 17
Size: 307 Color: 12

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 13
Size: 349 Color: 3
Size: 280 Color: 2

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 13
Size: 373 Color: 18
Size: 253 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7
Size: 366 Color: 5
Size: 256 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7
Size: 323 Color: 3
Size: 297 Color: 18

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 14
Size: 338 Color: 8
Size: 280 Color: 16

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 335 Color: 9
Size: 280 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 323 Color: 15
Size: 287 Color: 5

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 19
Size: 311 Color: 1
Size: 298 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 10
Size: 340 Color: 17
Size: 266 Color: 4

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7
Size: 312 Color: 12
Size: 293 Color: 2

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 14
Size: 326 Color: 1
Size: 278 Color: 3

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 12
Size: 321 Color: 10
Size: 282 Color: 8

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 4
Size: 316 Color: 2
Size: 283 Color: 15

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 9
Size: 316 Color: 15
Size: 277 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 6
Size: 332 Color: 4
Size: 250 Color: 3

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 10
Size: 307 Color: 8
Size: 269 Color: 6

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 16
Size: 295 Color: 14
Size: 280 Color: 10

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 2
Size: 298 Color: 5
Size: 273 Color: 11

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 309 Color: 13
Size: 259 Color: 17

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 4
Size: 301 Color: 6
Size: 251 Color: 5

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 14
Size: 294 Color: 10
Size: 259 Color: 12

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 7
Size: 289 Color: 18
Size: 260 Color: 13

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 16
Size: 274 Color: 15
Size: 268 Color: 18

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 349 Color: 0
Size: 295 Color: 4

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 14
Size: 278 Color: 9
Size: 259 Color: 12

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 7
Size: 269 Color: 6
Size: 250 Color: 1

Total size: 167167
Total free space: 0

