Capicity Bin: 6528
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 2432 Color: 259
Size: 2232 Color: 245
Size: 904 Color: 176
Size: 680 Color: 152
Size: 120 Color: 21
Size: 112 Color: 14
Size: 48 Color: 4

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 5232 Color: 352
Size: 800 Color: 166
Size: 408 Color: 109
Size: 88 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4622 Color: 315
Size: 1590 Color: 226
Size: 316 Color: 95

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4967 Color: 327
Size: 1517 Color: 221
Size: 44 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3265 Color: 273
Size: 2721 Color: 268
Size: 542 Color: 136

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3722 Color: 285
Size: 2530 Color: 260
Size: 276 Color: 86

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4412 Color: 309
Size: 1764 Color: 232
Size: 352 Color: 102

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5628 Color: 380
Size: 756 Color: 159
Size: 144 Color: 32

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 391
Size: 644 Color: 149
Size: 120 Color: 20

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4929 Color: 326
Size: 1407 Color: 216
Size: 192 Color: 55

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5119 Color: 339
Size: 1175 Color: 201
Size: 234 Color: 72

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5046 Color: 335
Size: 1238 Color: 206
Size: 244 Color: 77

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4204 Color: 302
Size: 2212 Color: 242
Size: 112 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4500 Color: 311
Size: 1692 Color: 230
Size: 336 Color: 100

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5381 Color: 359
Size: 957 Color: 183
Size: 190 Color: 54

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 3282 Color: 280
Size: 2706 Color: 263
Size: 540 Color: 131

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5322 Color: 356
Size: 1006 Color: 186
Size: 200 Color: 58

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 3460 Color: 282
Size: 2564 Color: 261
Size: 504 Color: 128

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5147 Color: 343
Size: 1151 Color: 196
Size: 230 Color: 69

Bin 20: 0 of cap free
Amount of items: 4
Items: 
Size: 3831 Color: 296
Size: 2249 Color: 246
Size: 312 Color: 93
Size: 136 Color: 28

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4171 Color: 301
Size: 1965 Color: 238
Size: 392 Color: 108

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 3775 Color: 290
Size: 2295 Color: 252
Size: 458 Color: 122

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 3274 Color: 278
Size: 2714 Color: 266
Size: 540 Color: 132

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 333
Size: 1254 Color: 207
Size: 248 Color: 78

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5604 Color: 377
Size: 772 Color: 161
Size: 152 Color: 36

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5409 Color: 364
Size: 933 Color: 178
Size: 186 Color: 53

Bin 27: 0 of cap free
Amount of items: 4
Items: 
Size: 3738 Color: 286
Size: 2326 Color: 256
Size: 440 Color: 114
Size: 24 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5413 Color: 365
Size: 931 Color: 177
Size: 184 Color: 49

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 354
Size: 1220 Color: 205
Size: 24 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 305
Size: 1954 Color: 237
Size: 324 Color: 97

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 382
Size: 791 Color: 163
Size: 78 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4322 Color: 306
Size: 1842 Color: 235
Size: 364 Color: 106

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5814 Color: 396
Size: 618 Color: 145
Size: 96 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 362
Size: 940 Color: 180
Size: 184 Color: 48

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4455 Color: 310
Size: 1749 Color: 231
Size: 324 Color: 98

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5791 Color: 394
Size: 615 Color: 144
Size: 122 Color: 22

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 348
Size: 1131 Color: 192
Size: 226 Color: 66

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5366 Color: 358
Size: 970 Color: 184
Size: 192 Color: 56

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 345
Size: 1148 Color: 195
Size: 224 Color: 65

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 329
Size: 1284 Color: 210
Size: 256 Color: 81

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5650 Color: 381
Size: 734 Color: 157
Size: 144 Color: 33

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 299
Size: 2138 Color: 240
Size: 424 Color: 111

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 3860 Color: 297
Size: 2228 Color: 244
Size: 440 Color: 113

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 3819 Color: 293
Size: 2259 Color: 249
Size: 450 Color: 117

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5798 Color: 395
Size: 610 Color: 143
Size: 120 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5712 Color: 387
Size: 464 Color: 125
Size: 352 Color: 103

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 4741 Color: 321
Size: 1491 Color: 219
Size: 296 Color: 88

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5531 Color: 371
Size: 831 Color: 169
Size: 166 Color: 42

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 3823 Color: 294
Size: 2255 Color: 248
Size: 450 Color: 118

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 3273 Color: 277
Size: 2713 Color: 265
Size: 542 Color: 137

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5500 Color: 369
Size: 860 Color: 171
Size: 168 Color: 43

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 3266 Color: 274
Size: 2722 Color: 269
Size: 540 Color: 134

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 389
Size: 660 Color: 151
Size: 128 Color: 26

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4226 Color: 303
Size: 2214 Color: 243
Size: 88 Color: 10

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 4995 Color: 330
Size: 1279 Color: 209
Size: 254 Color: 80

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 372
Size: 818 Color: 168
Size: 160 Color: 39

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 317
Size: 1570 Color: 224
Size: 312 Color: 92

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 4346 Color: 308
Size: 1822 Color: 233
Size: 360 Color: 104

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 349
Size: 1121 Color: 191
Size: 224 Color: 64

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5099 Color: 338
Size: 1191 Color: 202
Size: 238 Color: 75

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5143 Color: 342
Size: 1155 Color: 197
Size: 230 Color: 70

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 3268 Color: 275
Size: 2724 Color: 270
Size: 536 Color: 130

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 355
Size: 1025 Color: 187
Size: 204 Color: 59

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 3751 Color: 288
Size: 2315 Color: 254
Size: 462 Color: 123

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5692 Color: 385
Size: 700 Color: 154
Size: 136 Color: 27

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 3676 Color: 283
Size: 2380 Color: 258
Size: 472 Color: 127

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5489 Color: 368
Size: 867 Color: 172
Size: 172 Color: 44

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 3277 Color: 279
Size: 2711 Color: 264
Size: 540 Color: 133

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4636 Color: 316
Size: 1580 Color: 225
Size: 312 Color: 94

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 324
Size: 1388 Color: 214
Size: 272 Color: 84

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5167 Color: 347
Size: 1135 Color: 193
Size: 226 Color: 67

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 3799 Color: 292
Size: 2275 Color: 250
Size: 454 Color: 119

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 4971 Color: 328
Size: 1299 Color: 211
Size: 258 Color: 82

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5670 Color: 383
Size: 718 Color: 156
Size: 140 Color: 31

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 4999 Color: 331
Size: 1275 Color: 208
Size: 254 Color: 79

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5526 Color: 370
Size: 838 Color: 170
Size: 164 Color: 41

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5850 Color: 400
Size: 566 Color: 140
Size: 112 Color: 13

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5139 Color: 341
Size: 1159 Color: 199
Size: 230 Color: 71

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 3290 Color: 281
Size: 2702 Color: 262
Size: 536 Color: 129

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 307
Size: 1828 Color: 234
Size: 360 Color: 105

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 322
Size: 1484 Color: 218
Size: 296 Color: 87

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 4582 Color: 314
Size: 1622 Color: 227
Size: 324 Color: 96

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5095 Color: 337
Size: 1195 Color: 203
Size: 238 Color: 74

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 3894 Color: 298
Size: 2198 Color: 241
Size: 436 Color: 112

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5626 Color: 379
Size: 754 Color: 158
Size: 148 Color: 34

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 4548 Color: 313
Size: 1652 Color: 228
Size: 328 Color: 99

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 3795 Color: 291
Size: 2279 Color: 251
Size: 454 Color: 120

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5342 Color: 357
Size: 990 Color: 185
Size: 196 Color: 57

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 5206 Color: 350
Size: 1102 Color: 190
Size: 220 Color: 62

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 4737 Color: 320
Size: 1493 Color: 220
Size: 298 Color: 89

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5148 Color: 344
Size: 1156 Color: 198
Size: 224 Color: 63

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 3827 Color: 295
Size: 2251 Color: 247
Size: 450 Color: 116

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5815 Color: 397
Size: 649 Color: 150
Size: 64 Color: 5

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5027 Color: 334
Size: 1325 Color: 212
Size: 176 Color: 46

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 5757 Color: 390
Size: 643 Color: 148
Size: 128 Color: 25

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 5070 Color: 336
Size: 1218 Color: 204
Size: 240 Color: 76

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 401
Size: 564 Color: 139
Size: 112 Color: 15

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 3709 Color: 284
Size: 2351 Color: 257
Size: 468 Color: 126

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 3747 Color: 287
Size: 2319 Color: 255
Size: 462 Color: 124

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 5777 Color: 392
Size: 627 Color: 147
Size: 124 Color: 24

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 5003 Color: 332
Size: 1437 Color: 217
Size: 88 Color: 8

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 351
Size: 1082 Color: 189
Size: 216 Color: 61

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 5694 Color: 386
Size: 698 Color: 153
Size: 136 Color: 29

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 4878 Color: 325
Size: 1378 Color: 213
Size: 272 Color: 83

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5555 Color: 373
Size: 811 Color: 167
Size: 162 Color: 40

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5615 Color: 378
Size: 761 Color: 160
Size: 152 Color: 35

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 360
Size: 948 Color: 182
Size: 184 Color: 50

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 353
Size: 1044 Color: 188
Size: 208 Color: 60

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 4060 Color: 300
Size: 2060 Color: 239
Size: 408 Color: 110

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 340
Size: 1171 Color: 200
Size: 234 Color: 73

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 3224 Color: 272
Size: 2856 Color: 271
Size: 448 Color: 115

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 3771 Color: 289
Size: 2299 Color: 253
Size: 458 Color: 121

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 367
Size: 894 Color: 174
Size: 176 Color: 47

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5854 Color: 402
Size: 562 Color: 138
Size: 112 Color: 12

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4509 Color: 312
Size: 1683 Color: 229
Size: 336 Color: 101

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 318
Size: 1564 Color: 223
Size: 304 Color: 90

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 4237 Color: 304
Size: 1911 Color: 236
Size: 380 Color: 107

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 3269 Color: 276
Size: 2717 Color: 267
Size: 542 Color: 135

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 388
Size: 794 Color: 164
Size: 4 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 5454 Color: 366
Size: 898 Color: 175
Size: 176 Color: 45

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 323
Size: 1398 Color: 215
Size: 276 Color: 85

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5401 Color: 361
Size: 941 Color: 181
Size: 186 Color: 51

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5579 Color: 375
Size: 873 Color: 173
Size: 76 Color: 6

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 5575 Color: 374
Size: 795 Color: 165
Size: 158 Color: 38

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 5163 Color: 346
Size: 1139 Color: 194
Size: 226 Color: 68

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5683 Color: 384
Size: 705 Color: 155
Size: 140 Color: 30

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 5839 Color: 398
Size: 575 Color: 142
Size: 114 Color: 17

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 399
Size: 571 Color: 141
Size: 114 Color: 18

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5405 Color: 363
Size: 937 Color: 179
Size: 186 Color: 52

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 4671 Color: 319
Size: 1549 Color: 222
Size: 308 Color: 91

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 5787 Color: 393
Size: 619 Color: 146
Size: 122 Color: 23

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 5599 Color: 376
Size: 775 Color: 162
Size: 154 Color: 37

Total size: 861696
Total free space: 0

