Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 102

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 75
Size: 339 Color: 57
Size: 288 Color: 33

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 92
Size: 297 Color: 38
Size: 261 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 101
Size: 255 Color: 6
Size: 250 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 99
Size: 282 Color: 28
Size: 255 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 82
Size: 311 Color: 45
Size: 305 Color: 41

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 91
Size: 286 Color: 31
Size: 277 Color: 22

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 81
Size: 329 Color: 55
Size: 290 Color: 36

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 95
Size: 277 Color: 23
Size: 275 Color: 21

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 76
Size: 354 Color: 65
Size: 272 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 83
Size: 340 Color: 59
Size: 273 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 80
Size: 341 Color: 62
Size: 278 Color: 26

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 88
Size: 313 Color: 48
Size: 269 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 100
Size: 253 Color: 3
Size: 253 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 90
Size: 317 Color: 50
Size: 255 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 94
Size: 279 Color: 27
Size: 274 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 85
Size: 308 Color: 42
Size: 285 Color: 30

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 87
Size: 309 Color: 43
Size: 278 Color: 25

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 71
Size: 320 Color: 52
Size: 317 Color: 49

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 96
Size: 275 Color: 20
Size: 274 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 84
Size: 331 Color: 56
Size: 278 Color: 24

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 93
Size: 288 Color: 32
Size: 267 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 78
Size: 312 Color: 46
Size: 310 Color: 44

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 97
Size: 282 Color: 29
Size: 266 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 77
Size: 370 Color: 73
Size: 254 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 68
Size: 341 Color: 61
Size: 305 Color: 40

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 89
Size: 323 Color: 54
Size: 252 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 72
Size: 341 Color: 60
Size: 293 Color: 37

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 79
Size: 354 Color: 66
Size: 267 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 70
Size: 320 Color: 53
Size: 319 Color: 51

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 86
Size: 312 Color: 47
Size: 275 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 74
Size: 339 Color: 58
Size: 289 Color: 35

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 98
Size: 272 Color: 14
Size: 271 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 69
Size: 351 Color: 64
Size: 289 Color: 34

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 67
Size: 349 Color: 63
Size: 297 Color: 39

Total size: 34034
Total free space: 0

