Capicity Bin: 7568
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 3736 Color: 272
Size: 1144 Color: 195
Size: 944 Color: 174
Size: 600 Color: 130
Size: 464 Color: 115
Size: 424 Color: 111
Size: 112 Color: 9
Size: 96 Color: 6
Size: 48 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6754 Color: 393
Size: 682 Color: 146
Size: 132 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5798 Color: 324
Size: 1478 Color: 217
Size: 292 Color: 89

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6041 Color: 335
Size: 1273 Color: 206
Size: 254 Color: 78

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6582 Color: 376
Size: 822 Color: 162
Size: 164 Color: 42

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5095 Color: 300
Size: 2061 Color: 241
Size: 412 Color: 109

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4299 Color: 281
Size: 2725 Color: 261
Size: 544 Color: 128

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4322 Color: 284
Size: 2706 Color: 257
Size: 540 Color: 126

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 322
Size: 1492 Color: 219
Size: 296 Color: 90

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6810 Color: 402
Size: 634 Color: 137
Size: 124 Color: 12

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6324 Color: 350
Size: 1044 Color: 190
Size: 200 Color: 56

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5990 Color: 330
Size: 1318 Color: 211
Size: 260 Color: 82

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 305
Size: 1860 Color: 236
Size: 368 Color: 103

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 362
Size: 988 Color: 180
Size: 120 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 3786 Color: 274
Size: 3154 Color: 267
Size: 628 Color: 135

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6188 Color: 344
Size: 1156 Color: 199
Size: 224 Color: 67

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 372
Size: 836 Color: 167
Size: 160 Color: 39

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6145 Color: 339
Size: 1187 Color: 203
Size: 236 Color: 73

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6621 Color: 378
Size: 791 Color: 160
Size: 156 Color: 38

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6474 Color: 363
Size: 1046 Color: 191
Size: 48 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6663 Color: 383
Size: 755 Color: 155
Size: 150 Color: 33

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5162 Color: 302
Size: 2006 Color: 239
Size: 400 Color: 106

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4332 Color: 286
Size: 2700 Color: 256
Size: 536 Color: 124

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 365
Size: 892 Color: 172
Size: 176 Color: 48

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6427 Color: 359
Size: 951 Color: 176
Size: 190 Color: 54

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 285
Size: 2710 Color: 258
Size: 528 Color: 123

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4729 Color: 291
Size: 2367 Color: 251
Size: 472 Color: 119

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5806 Color: 325
Size: 1502 Color: 220
Size: 260 Color: 83

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6338 Color: 353
Size: 1026 Color: 185
Size: 204 Color: 59

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6757 Color: 394
Size: 677 Color: 145
Size: 134 Color: 23

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 331
Size: 1316 Color: 210
Size: 256 Color: 81

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 364
Size: 1022 Color: 183
Size: 64 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5758 Color: 321
Size: 1510 Color: 221
Size: 300 Color: 91

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6174 Color: 341
Size: 1162 Color: 201
Size: 232 Color: 71

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 354
Size: 1028 Color: 186
Size: 200 Color: 58

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 298
Size: 2066 Color: 242
Size: 412 Color: 108

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4306 Color: 283
Size: 2722 Color: 259
Size: 540 Color: 125

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6187 Color: 343
Size: 1151 Color: 197
Size: 230 Color: 70

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 384
Size: 828 Color: 163
Size: 72 Color: 5

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6458 Color: 361
Size: 962 Color: 179
Size: 148 Color: 31

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6329 Color: 352
Size: 1033 Color: 188
Size: 206 Color: 61

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6697 Color: 387
Size: 727 Color: 152
Size: 144 Color: 28

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5083 Color: 297
Size: 2071 Color: 243
Size: 414 Color: 110

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 310
Size: 1942 Color: 237
Size: 168 Color: 46

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6622 Color: 379
Size: 790 Color: 159
Size: 156 Color: 36

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6372 Color: 357
Size: 1004 Color: 181
Size: 192 Color: 55

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 356
Size: 1006 Color: 182
Size: 200 Color: 57

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5796 Color: 323
Size: 1484 Color: 218
Size: 288 Color: 88

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 3789 Color: 276
Size: 3151 Color: 265
Size: 628 Color: 134

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6743 Color: 392
Size: 689 Color: 147
Size: 136 Color: 24

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6022 Color: 333
Size: 1290 Color: 207
Size: 256 Color: 79

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6341 Color: 355
Size: 1023 Color: 184
Size: 204 Color: 60

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5628 Color: 315
Size: 1620 Color: 225
Size: 320 Color: 95

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 280
Size: 2746 Color: 262
Size: 548 Color: 129

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 388
Size: 724 Color: 151
Size: 144 Color: 27

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 304
Size: 1964 Color: 238
Size: 384 Color: 105

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 320
Size: 1794 Color: 232
Size: 48 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6786 Color: 400
Size: 654 Color: 139
Size: 128 Color: 15

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 395
Size: 673 Color: 142
Size: 134 Color: 22

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6179 Color: 342
Size: 1159 Color: 200
Size: 230 Color: 69

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5838 Color: 326
Size: 1442 Color: 215
Size: 288 Color: 87

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4758 Color: 292
Size: 2342 Color: 249
Size: 468 Color: 118

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5643 Color: 317
Size: 1793 Color: 231
Size: 132 Color: 19

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5381 Color: 306
Size: 1823 Color: 235
Size: 364 Color: 102

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 312
Size: 1708 Color: 228
Size: 336 Color: 97

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6638 Color: 381
Size: 778 Color: 157
Size: 152 Color: 35

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6573 Color: 373
Size: 831 Color: 166
Size: 164 Color: 44

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 4722 Color: 289
Size: 2374 Color: 253
Size: 472 Color: 120

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6325 Color: 351
Size: 1037 Color: 189
Size: 206 Color: 62

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6596 Color: 377
Size: 812 Color: 161
Size: 160 Color: 40

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 382
Size: 764 Color: 156
Size: 152 Color: 34

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6773 Color: 399
Size: 663 Color: 140
Size: 132 Color: 17

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 4852 Color: 296
Size: 2268 Color: 245
Size: 448 Color: 112

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 3304 Color: 270
Size: 2816 Color: 263
Size: 1448 Color: 216

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5522 Color: 311
Size: 1706 Color: 227
Size: 340 Color: 98

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 389
Size: 721 Color: 150
Size: 144 Color: 29

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6431 Color: 360
Size: 949 Color: 175
Size: 188 Color: 53

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 368
Size: 886 Color: 169
Size: 176 Color: 51

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 4788 Color: 294
Size: 2324 Color: 248
Size: 456 Color: 114

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 374
Size: 830 Color: 165
Size: 164 Color: 43

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 358
Size: 954 Color: 177
Size: 188 Color: 52

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5180 Color: 303
Size: 2148 Color: 244
Size: 240 Color: 76

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6671 Color: 385
Size: 749 Color: 154
Size: 148 Color: 32

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 3796 Color: 278
Size: 3676 Color: 271
Size: 96 Color: 7

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 370
Size: 907 Color: 173
Size: 100 Color: 8

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6191 Color: 345
Size: 1149 Color: 196
Size: 228 Color: 68

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6788 Color: 401
Size: 652 Color: 138
Size: 128 Color: 13

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5399 Color: 308
Size: 1809 Color: 233
Size: 360 Color: 100

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 5091 Color: 299
Size: 2357 Color: 250
Size: 120 Color: 10

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 340
Size: 1164 Color: 202
Size: 232 Color: 72

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 4782 Color: 293
Size: 2322 Color: 247
Size: 464 Color: 116

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6770 Color: 398
Size: 666 Color: 141
Size: 132 Color: 21

Bin 93: 0 of cap free
Amount of items: 4
Items: 
Size: 6040 Color: 334
Size: 1032 Color: 187
Size: 368 Color: 104
Size: 128 Color: 16

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6503 Color: 367
Size: 889 Color: 170
Size: 176 Color: 50

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4301 Color: 282
Size: 2723 Color: 260
Size: 544 Color: 127

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 380
Size: 789 Color: 158
Size: 156 Color: 37

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 3794 Color: 277
Size: 3158 Color: 269
Size: 616 Color: 132

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 5875 Color: 328
Size: 1411 Color: 213
Size: 282 Color: 85

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 295
Size: 2308 Color: 246
Size: 456 Color: 113

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 4727 Color: 290
Size: 2369 Color: 252
Size: 472 Color: 121

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 3785 Color: 273
Size: 3153 Color: 266
Size: 630 Color: 136

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 396
Size: 674 Color: 143
Size: 132 Color: 20

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 4380 Color: 287
Size: 2660 Color: 255
Size: 528 Color: 122

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 4696 Color: 288
Size: 2408 Color: 254
Size: 464 Color: 117

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5637 Color: 316
Size: 1611 Color: 224
Size: 320 Color: 94

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5138 Color: 301
Size: 2026 Color: 240
Size: 404 Color: 107

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 347
Size: 1082 Color: 193
Size: 216 Color: 65

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6563 Color: 371
Size: 957 Color: 178
Size: 48 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 332
Size: 1308 Color: 209
Size: 256 Color: 80

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6142 Color: 338
Size: 1190 Color: 204
Size: 236 Color: 74

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6192 Color: 346
Size: 1152 Color: 198
Size: 224 Color: 66

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 314
Size: 1654 Color: 226
Size: 328 Color: 96

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6725 Color: 391
Size: 703 Color: 148
Size: 140 Color: 26

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 397
Size: 676 Color: 144
Size: 128 Color: 14

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 390
Size: 706 Color: 149
Size: 140 Color: 25

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6114 Color: 337
Size: 1214 Color: 205
Size: 240 Color: 75

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 279
Size: 3052 Color: 264
Size: 608 Color: 131

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 5391 Color: 307
Size: 1815 Color: 234
Size: 362 Color: 101

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 349
Size: 1057 Color: 192
Size: 210 Color: 63

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 5426 Color: 309
Size: 1786 Color: 229
Size: 356 Color: 99

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 348
Size: 1126 Color: 194
Size: 168 Color: 45

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 369
Size: 870 Color: 168
Size: 172 Color: 47

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5538 Color: 313
Size: 1786 Color: 230
Size: 244 Color: 77

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 5867 Color: 327
Size: 1419 Color: 214
Size: 282 Color: 86

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 318
Size: 1591 Color: 223
Size: 318 Color: 93

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 375
Size: 829 Color: 164
Size: 164 Color: 41

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 5667 Color: 319
Size: 1585 Color: 222
Size: 316 Color: 92

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6049 Color: 336
Size: 1307 Color: 208
Size: 212 Color: 64

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5879 Color: 329
Size: 1409 Color: 212
Size: 280 Color: 84

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6501 Color: 366
Size: 891 Color: 171
Size: 176 Color: 49

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6686 Color: 386
Size: 738 Color: 153
Size: 144 Color: 30

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 3788 Color: 275
Size: 3156 Color: 268
Size: 624 Color: 133

Total size: 998976
Total free space: 0

