Capicity Bin: 1000001
Lower Bound: 48

Bins used: 49
Amount of Colors: 20

Bin 1: 20 of cap free
Amount of items: 2
Items: 
Size: 686574 Color: 3
Size: 313407 Color: 17

Bin 2: 28 of cap free
Amount of items: 3
Items: 
Size: 748804 Color: 19
Size: 131188 Color: 7
Size: 119981 Color: 14

Bin 3: 208 of cap free
Amount of items: 3
Items: 
Size: 699276 Color: 1
Size: 192188 Color: 18
Size: 108329 Color: 3

Bin 4: 271 of cap free
Amount of items: 3
Items: 
Size: 508617 Color: 10
Size: 289769 Color: 13
Size: 201344 Color: 13

Bin 5: 350 of cap free
Amount of items: 3
Items: 
Size: 563577 Color: 1
Size: 292659 Color: 15
Size: 143415 Color: 13

Bin 6: 560 of cap free
Amount of items: 3
Items: 
Size: 734252 Color: 15
Size: 153297 Color: 11
Size: 111892 Color: 8

Bin 7: 591 of cap free
Amount of items: 2
Items: 
Size: 668830 Color: 12
Size: 330580 Color: 11

Bin 8: 629 of cap free
Amount of items: 3
Items: 
Size: 735512 Color: 15
Size: 140905 Color: 15
Size: 122955 Color: 6

Bin 9: 1134 of cap free
Amount of items: 2
Items: 
Size: 554199 Color: 7
Size: 444668 Color: 19

Bin 10: 1171 of cap free
Amount of items: 2
Items: 
Size: 559109 Color: 10
Size: 439721 Color: 7

Bin 11: 1503 of cap free
Amount of items: 2
Items: 
Size: 636010 Color: 16
Size: 362488 Color: 18

Bin 12: 1869 of cap free
Amount of items: 2
Items: 
Size: 592364 Color: 8
Size: 405768 Color: 0

Bin 13: 1975 of cap free
Amount of items: 2
Items: 
Size: 715735 Color: 9
Size: 282291 Color: 0

Bin 14: 2054 of cap free
Amount of items: 3
Items: 
Size: 553891 Color: 19
Size: 245013 Color: 11
Size: 199043 Color: 17

Bin 15: 2347 of cap free
Amount of items: 2
Items: 
Size: 783038 Color: 7
Size: 214616 Color: 18

Bin 16: 2797 of cap free
Amount of items: 2
Items: 
Size: 526789 Color: 1
Size: 470415 Color: 0

Bin 17: 3176 of cap free
Amount of items: 2
Items: 
Size: 640552 Color: 4
Size: 356273 Color: 2

Bin 18: 3214 of cap free
Amount of items: 2
Items: 
Size: 655439 Color: 2
Size: 341348 Color: 16

Bin 19: 3715 of cap free
Amount of items: 2
Items: 
Size: 521883 Color: 7
Size: 474403 Color: 16

Bin 20: 3751 of cap free
Amount of items: 2
Items: 
Size: 607628 Color: 18
Size: 388622 Color: 12

Bin 21: 4735 of cap free
Amount of items: 2
Items: 
Size: 544633 Color: 2
Size: 450633 Color: 10

Bin 22: 5382 of cap free
Amount of items: 2
Items: 
Size: 746349 Color: 18
Size: 248270 Color: 3

Bin 23: 5768 of cap free
Amount of items: 2
Items: 
Size: 780386 Color: 10
Size: 213847 Color: 11

Bin 24: 6708 of cap free
Amount of items: 2
Items: 
Size: 501490 Color: 13
Size: 491803 Color: 16

Bin 25: 6900 of cap free
Amount of items: 2
Items: 
Size: 712525 Color: 8
Size: 280576 Color: 5

Bin 26: 9031 of cap free
Amount of items: 2
Items: 
Size: 779026 Color: 14
Size: 211944 Color: 7

Bin 27: 9124 of cap free
Amount of items: 2
Items: 
Size: 568418 Color: 5
Size: 422459 Color: 16

Bin 28: 9446 of cap free
Amount of items: 2
Items: 
Size: 589253 Color: 16
Size: 401302 Color: 0

Bin 29: 11767 of cap free
Amount of items: 2
Items: 
Size: 666828 Color: 16
Size: 321406 Color: 17

Bin 30: 13371 of cap free
Amount of items: 2
Items: 
Size: 756663 Color: 15
Size: 229967 Color: 6

Bin 31: 23552 of cap free
Amount of items: 2
Items: 
Size: 790509 Color: 5
Size: 185940 Color: 12

Bin 32: 23600 of cap free
Amount of items: 2
Items: 
Size: 695833 Color: 6
Size: 280568 Color: 16

Bin 33: 25359 of cap free
Amount of items: 2
Items: 
Size: 619295 Color: 17
Size: 355347 Color: 5

Bin 34: 26035 of cap free
Amount of items: 2
Items: 
Size: 789722 Color: 8
Size: 184244 Color: 10

Bin 35: 27269 of cap free
Amount of items: 2
Items: 
Size: 694201 Color: 2
Size: 278531 Color: 7

Bin 36: 28406 of cap free
Amount of items: 2
Items: 
Size: 743680 Color: 10
Size: 227915 Color: 15

Bin 37: 30017 of cap free
Amount of items: 2
Items: 
Size: 615387 Color: 6
Size: 354597 Color: 9

Bin 38: 31770 of cap free
Amount of items: 2
Items: 
Size: 563065 Color: 10
Size: 405166 Color: 16

Bin 39: 37121 of cap free
Amount of items: 2
Items: 
Size: 652885 Color: 6
Size: 309995 Color: 9

Bin 40: 37513 of cap free
Amount of items: 2
Items: 
Size: 513622 Color: 8
Size: 448866 Color: 13

Bin 41: 45878 of cap free
Amount of items: 2
Items: 
Size: 600737 Color: 4
Size: 353386 Color: 17

Bin 42: 46487 of cap free
Amount of items: 2
Items: 
Size: 777991 Color: 1
Size: 175523 Color: 13

Bin 43: 48206 of cap free
Amount of items: 2
Items: 
Size: 559330 Color: 7
Size: 392465 Color: 4

Bin 44: 70621 of cap free
Amount of items: 2
Items: 
Size: 660358 Color: 12
Size: 269022 Color: 6

Bin 45: 116313 of cap free
Amount of items: 2
Items: 
Size: 743548 Color: 17
Size: 140140 Color: 7

Bin 46: 120022 of cap free
Amount of items: 2
Items: 
Size: 741548 Color: 8
Size: 138431 Color: 3

Bin 47: 259429 of cap free
Amount of items: 1
Items: 
Size: 740572 Color: 16

Bin 48: 266327 of cap free
Amount of items: 1
Items: 
Size: 733674 Color: 13

Bin 49: 268062 of cap free
Amount of items: 1
Items: 
Size: 731939 Color: 1

Total size: 47354467
Total free space: 1645582

