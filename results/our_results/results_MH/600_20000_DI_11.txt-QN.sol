Capicity Bin: 14480
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 10364 Color: 459
Size: 3220 Color: 336
Size: 896 Color: 174

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10600 Color: 468
Size: 2856 Color: 317
Size: 1024 Color: 185

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 469
Size: 2000 Color: 273
Size: 1860 Color: 265

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10915 Color: 480
Size: 3031 Color: 330
Size: 534 Color: 118

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11150 Color: 487
Size: 2778 Color: 315
Size: 552 Color: 123

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 488
Size: 1906 Color: 271
Size: 1420 Color: 222

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11160 Color: 489
Size: 3016 Color: 329
Size: 304 Color: 48

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 494
Size: 2776 Color: 314
Size: 400 Color: 84

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11379 Color: 497
Size: 1901 Color: 269
Size: 1200 Color: 196

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11568 Color: 504
Size: 2264 Color: 292
Size: 648 Color: 142

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 509
Size: 1902 Color: 270
Size: 858 Color: 171

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 513
Size: 2078 Color: 281
Size: 640 Color: 139

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11768 Color: 515
Size: 2508 Color: 305
Size: 204 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11770 Color: 516
Size: 2262 Color: 291
Size: 448 Color: 101

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11860 Color: 519
Size: 2312 Color: 296
Size: 308 Color: 52

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11927 Color: 520
Size: 2261 Color: 290
Size: 292 Color: 42

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11985 Color: 521
Size: 1571 Color: 244
Size: 924 Color: 181

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11989 Color: 522
Size: 2077 Color: 280
Size: 414 Color: 91

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11990 Color: 523
Size: 2018 Color: 276
Size: 472 Color: 108

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12060 Color: 527
Size: 2188 Color: 285
Size: 232 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12186 Color: 532
Size: 1442 Color: 223
Size: 852 Color: 169

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 537
Size: 1404 Color: 220
Size: 832 Color: 168

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12305 Color: 540
Size: 1813 Color: 263
Size: 362 Color: 72

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12312 Color: 541
Size: 1672 Color: 253
Size: 496 Color: 113

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 542
Size: 1816 Color: 264
Size: 320 Color: 57

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12354 Color: 544
Size: 1662 Color: 252
Size: 464 Color: 107

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12436 Color: 548
Size: 1484 Color: 231
Size: 560 Color: 125

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 550
Size: 1592 Color: 246
Size: 400 Color: 87

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12490 Color: 551
Size: 1542 Color: 240
Size: 448 Color: 102

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12502 Color: 552
Size: 1384 Color: 219
Size: 594 Color: 133

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 554
Size: 1448 Color: 224
Size: 496 Color: 114

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 555
Size: 1204 Color: 200
Size: 728 Color: 158

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 557
Size: 1612 Color: 248
Size: 284 Color: 36

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 558
Size: 1406 Color: 221
Size: 480 Color: 111

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12630 Color: 563
Size: 1530 Color: 239
Size: 320 Color: 58

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 564
Size: 1152 Color: 193
Size: 682 Color: 152

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 565
Size: 1260 Color: 207
Size: 552 Color: 124

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12680 Color: 566
Size: 1206 Color: 202
Size: 594 Color: 131

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12707 Color: 570
Size: 1479 Color: 230
Size: 294 Color: 43

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12716 Color: 571
Size: 1500 Color: 233
Size: 264 Color: 27

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12742 Color: 574
Size: 1478 Color: 229
Size: 260 Color: 26

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12754 Color: 576
Size: 1038 Color: 186
Size: 688 Color: 153

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12768 Color: 577
Size: 912 Color: 180
Size: 800 Color: 166

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12786 Color: 580
Size: 1502 Color: 234
Size: 192 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12794 Color: 581
Size: 1342 Color: 215
Size: 344 Color: 66

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12796 Color: 582
Size: 1168 Color: 194
Size: 516 Color: 116

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12822 Color: 583
Size: 1310 Color: 212
Size: 348 Color: 67

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12824 Color: 584
Size: 1288 Color: 208
Size: 368 Color: 76

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12892 Color: 588
Size: 1292 Color: 209
Size: 296 Color: 46

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12918 Color: 591
Size: 1302 Color: 210
Size: 260 Color: 24

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12926 Color: 593
Size: 1228 Color: 205
Size: 326 Color: 60

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 595
Size: 1214 Color: 203
Size: 314 Color: 55

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13012 Color: 598
Size: 1192 Color: 195
Size: 276 Color: 32

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13026 Color: 600
Size: 1040 Color: 188
Size: 414 Color: 92

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 10460 Color: 464
Size: 2971 Color: 324
Size: 1048 Color: 190

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 10476 Color: 466
Size: 3763 Color: 355
Size: 240 Color: 14

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 10911 Color: 479
Size: 3502 Color: 349
Size: 66 Color: 0

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 11052 Color: 484
Size: 3351 Color: 341
Size: 76 Color: 1

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 11516 Color: 502
Size: 2585 Color: 307
Size: 378 Color: 79

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11573 Color: 505
Size: 2522 Color: 306
Size: 384 Color: 83

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 11751 Color: 511
Size: 2012 Color: 275
Size: 716 Color: 157

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 11767 Color: 514
Size: 2344 Color: 297
Size: 368 Color: 75

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 12183 Color: 531
Size: 2008 Color: 274
Size: 288 Color: 37

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 12248 Color: 538
Size: 2231 Color: 288

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 12350 Color: 543
Size: 2129 Color: 284

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12519 Color: 553
Size: 1324 Color: 214
Size: 636 Color: 138

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12627 Color: 562
Size: 1514 Color: 237
Size: 338 Color: 64

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 12780 Color: 579
Size: 1699 Color: 254

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 12988 Color: 597
Size: 1491 Color: 232

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 10266 Color: 455
Size: 4212 Color: 363

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 12595 Color: 559
Size: 1883 Color: 268

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 12614 Color: 561
Size: 1864 Color: 266

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 12700 Color: 569
Size: 1778 Color: 260

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 12727 Color: 573
Size: 1751 Color: 258

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 12770 Color: 578
Size: 1708 Color: 255

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 12828 Color: 585
Size: 1650 Color: 251

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 12920 Color: 592
Size: 1558 Color: 242

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 12972 Color: 596
Size: 1506 Color: 235

Bin 79: 3 of cap free
Amount of items: 7
Items: 
Size: 7245 Color: 408
Size: 1450 Color: 225
Size: 1382 Color: 218
Size: 1380 Color: 217
Size: 1364 Color: 216
Size: 1304 Color: 211
Size: 352 Color: 70

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 10459 Color: 463
Size: 2502 Color: 304
Size: 1516 Color: 238

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 11132 Color: 486
Size: 3345 Color: 340

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 11283 Color: 493
Size: 3194 Color: 334

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 11486 Color: 501
Size: 2991 Color: 328

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 11560 Color: 503
Size: 2051 Color: 278
Size: 866 Color: 172

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 11992 Color: 524
Size: 2485 Color: 303

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 12202 Color: 535
Size: 2275 Color: 295

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 12563 Color: 556
Size: 1914 Color: 272

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 12932 Color: 594
Size: 1545 Color: 241

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 599
Size: 1461 Color: 227

Bin 90: 4 of cap free
Amount of items: 5
Items: 
Size: 7416 Color: 414
Size: 6028 Color: 397
Size: 384 Color: 82
Size: 328 Color: 61
Size: 320 Color: 59

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 8997 Color: 430
Size: 5191 Color: 382
Size: 288 Color: 40

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 9965 Color: 451
Size: 4271 Color: 365
Size: 240 Color: 18

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 10435 Color: 461
Size: 3801 Color: 356
Size: 240 Color: 15

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 10996 Color: 482
Size: 3480 Color: 347

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 11828 Color: 518
Size: 2648 Color: 308

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 12388 Color: 546
Size: 2088 Color: 283

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 12684 Color: 567
Size: 1792 Color: 262

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 575
Size: 1732 Color: 256

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 12852 Color: 586
Size: 1624 Color: 249

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 12902 Color: 589
Size: 1574 Color: 245

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 10451 Color: 462
Size: 4024 Color: 361

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 11064 Color: 485
Size: 3411 Color: 345

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 11267 Color: 492
Size: 3208 Color: 335

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 11796 Color: 517
Size: 2679 Color: 309

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 12068 Color: 529
Size: 2407 Color: 300

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 12201 Color: 534
Size: 2274 Color: 294

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 12691 Color: 568
Size: 1784 Color: 261

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 11734 Color: 510
Size: 2740 Color: 311

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 11998 Color: 525
Size: 2476 Color: 302

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 12404 Color: 547
Size: 2070 Color: 279

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 12726 Color: 572
Size: 1748 Color: 257

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 12910 Color: 590
Size: 1564 Color: 243

Bin 113: 7 of cap free
Amount of items: 11
Items: 
Size: 7241 Color: 405
Size: 928 Color: 183
Size: 928 Color: 182
Size: 908 Color: 179
Size: 904 Color: 178
Size: 904 Color: 177
Size: 904 Color: 176
Size: 620 Color: 137
Size: 380 Color: 81
Size: 380 Color: 80
Size: 376 Color: 78

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 11754 Color: 512
Size: 2719 Color: 310

Bin 115: 7 of cap free
Amount of items: 2
Items: 
Size: 12874 Color: 587
Size: 1599 Color: 247

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 11036 Color: 483
Size: 3436 Color: 346

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 11196 Color: 490
Size: 3276 Color: 338

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 11596 Color: 507
Size: 2876 Color: 319

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 12604 Color: 560
Size: 1868 Color: 267

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 9038 Color: 433
Size: 5144 Color: 381
Size: 288 Color: 39

Bin 121: 11 of cap free
Amount of items: 2
Items: 
Size: 11363 Color: 496
Size: 3106 Color: 333

Bin 122: 12 of cap free
Amount of items: 3
Items: 
Size: 9283 Color: 437
Size: 4905 Color: 380
Size: 280 Color: 34

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 508
Size: 2796 Color: 316

Bin 124: 13 of cap free
Amount of items: 3
Items: 
Size: 10274 Color: 456
Size: 2969 Color: 323
Size: 1224 Color: 204

Bin 125: 14 of cap free
Amount of items: 5
Items: 
Size: 7258 Color: 412
Size: 6024 Color: 395
Size: 488 Color: 112
Size: 352 Color: 68
Size: 344 Color: 65

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 9812 Color: 447
Size: 4654 Color: 378

Bin 127: 14 of cap free
Amount of items: 2
Items: 
Size: 10170 Color: 454
Size: 4296 Color: 367

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 12062 Color: 528
Size: 2404 Color: 299

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 12088 Color: 530
Size: 2378 Color: 298

Bin 130: 15 of cap free
Amount of items: 2
Items: 
Size: 11482 Color: 500
Size: 2983 Color: 327

Bin 131: 15 of cap free
Amount of items: 2
Items: 
Size: 12221 Color: 536
Size: 2244 Color: 289

Bin 132: 16 of cap free
Amount of items: 3
Items: 
Size: 8178 Color: 417
Size: 5974 Color: 394
Size: 312 Color: 54

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 12252 Color: 539
Size: 2212 Color: 287

Bin 134: 17 of cap free
Amount of items: 3
Items: 
Size: 9924 Color: 450
Size: 4291 Color: 366
Size: 248 Color: 19

Bin 135: 17 of cap free
Amount of items: 2
Items: 
Size: 12443 Color: 549
Size: 2020 Color: 277

Bin 136: 18 of cap free
Amount of items: 3
Items: 
Size: 9706 Color: 443
Size: 4500 Color: 370
Size: 256 Color: 21

Bin 137: 18 of cap free
Amount of items: 3
Items: 
Size: 10282 Color: 457
Size: 3940 Color: 359
Size: 240 Color: 17

Bin 138: 18 of cap free
Amount of items: 2
Items: 
Size: 12381 Color: 545
Size: 2081 Color: 282

Bin 139: 19 of cap free
Amount of items: 2
Items: 
Size: 11359 Color: 495
Size: 3102 Color: 332

Bin 140: 19 of cap free
Amount of items: 2
Items: 
Size: 12021 Color: 526
Size: 2440 Color: 301

Bin 141: 20 of cap free
Amount of items: 2
Items: 
Size: 9084 Color: 435
Size: 5376 Color: 388

Bin 142: 20 of cap free
Amount of items: 3
Items: 
Size: 10762 Color: 474
Size: 3506 Color: 350
Size: 192 Color: 8

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 12194 Color: 533
Size: 2266 Color: 293

Bin 144: 21 of cap free
Amount of items: 4
Items: 
Size: 8228 Color: 421
Size: 5611 Color: 390
Size: 312 Color: 53
Size: 308 Color: 51

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 11219 Color: 491
Size: 3240 Color: 337

Bin 146: 22 of cap free
Amount of items: 5
Items: 
Size: 7314 Color: 413
Size: 6026 Color: 396
Size: 454 Color: 106
Size: 336 Color: 63
Size: 328 Color: 62

Bin 147: 22 of cap free
Amount of items: 3
Items: 
Size: 9614 Color: 441
Size: 4588 Color: 376
Size: 256 Color: 23

Bin 148: 22 of cap free
Amount of items: 3
Items: 
Size: 10650 Color: 471
Size: 3594 Color: 352
Size: 214 Color: 11

Bin 149: 23 of cap free
Amount of items: 2
Items: 
Size: 11478 Color: 499
Size: 2979 Color: 326

Bin 150: 24 of cap free
Amount of items: 3
Items: 
Size: 10754 Color: 473
Size: 3502 Color: 348
Size: 200 Color: 9

Bin 151: 25 of cap free
Amount of items: 3
Items: 
Size: 8251 Color: 422
Size: 5896 Color: 393
Size: 308 Color: 50

Bin 152: 26 of cap free
Amount of items: 4
Items: 
Size: 9359 Color: 440
Size: 4571 Color: 375
Size: 264 Color: 28
Size: 260 Color: 25

Bin 153: 26 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 442
Size: 4542 Color: 374
Size: 256 Color: 22

Bin 154: 26 of cap free
Amount of items: 3
Items: 
Size: 10556 Color: 467
Size: 3658 Color: 353
Size: 240 Color: 13

Bin 155: 27 of cap free
Amount of items: 2
Items: 
Size: 11593 Color: 506
Size: 2860 Color: 318

Bin 156: 29 of cap free
Amount of items: 2
Items: 
Size: 11476 Color: 498
Size: 2975 Color: 325

Bin 157: 30 of cap free
Amount of items: 3
Items: 
Size: 9752 Color: 445
Size: 2924 Color: 322
Size: 1774 Color: 259

Bin 158: 30 of cap free
Amount of items: 2
Items: 
Size: 9828 Color: 448
Size: 4622 Color: 377

Bin 159: 30 of cap free
Amount of items: 2
Items: 
Size: 10746 Color: 472
Size: 3704 Color: 354

Bin 160: 31 of cap free
Amount of items: 2
Items: 
Size: 10467 Color: 465
Size: 3982 Color: 360

Bin 161: 32 of cap free
Amount of items: 2
Items: 
Size: 10040 Color: 452
Size: 4408 Color: 369

Bin 162: 35 of cap free
Amount of items: 2
Items: 
Size: 10387 Color: 460
Size: 4058 Color: 362

Bin 163: 36 of cap free
Amount of items: 3
Items: 
Size: 10312 Color: 458
Size: 3892 Color: 358
Size: 240 Color: 16

Bin 164: 40 of cap free
Amount of items: 3
Items: 
Size: 8934 Color: 429
Size: 5218 Color: 384
Size: 288 Color: 41

Bin 165: 42 of cap free
Amount of items: 3
Items: 
Size: 8595 Color: 425
Size: 2922 Color: 321
Size: 2921 Color: 320

Bin 166: 42 of cap free
Amount of items: 4
Items: 
Size: 10907 Color: 478
Size: 3371 Color: 344
Size: 80 Color: 3
Size: 80 Color: 2

Bin 167: 44 of cap free
Amount of items: 9
Items: 
Size: 7242 Color: 406
Size: 1200 Color: 197
Size: 1136 Color: 192
Size: 1054 Color: 191
Size: 1040 Color: 189
Size: 1040 Color: 187
Size: 980 Color: 184
Size: 376 Color: 77
Size: 368 Color: 74

Bin 168: 44 of cap free
Amount of items: 2
Items: 
Size: 9756 Color: 446
Size: 4680 Color: 379

Bin 169: 44 of cap free
Amount of items: 3
Items: 
Size: 9919 Color: 449
Size: 4269 Color: 364
Size: 248 Color: 20

Bin 170: 44 of cap free
Amount of items: 2
Items: 
Size: 10632 Color: 470
Size: 3804 Color: 357

Bin 171: 47 of cap free
Amount of items: 2
Items: 
Size: 10919 Color: 481
Size: 3514 Color: 351

Bin 172: 49 of cap free
Amount of items: 4
Items: 
Size: 9355 Color: 439
Size: 4540 Color: 373
Size: 272 Color: 30
Size: 264 Color: 29

Bin 173: 55 of cap free
Amount of items: 2
Items: 
Size: 10094 Color: 453
Size: 4331 Color: 368

Bin 174: 59 of cap free
Amount of items: 4
Items: 
Size: 9331 Color: 438
Size: 4538 Color: 372
Size: 280 Color: 33
Size: 272 Color: 31

Bin 175: 66 of cap free
Amount of items: 3
Items: 
Size: 8872 Color: 427
Size: 2774 Color: 313
Size: 2768 Color: 312

Bin 176: 74 of cap free
Amount of items: 3
Items: 
Size: 8898 Color: 428
Size: 5212 Color: 383
Size: 296 Color: 44

Bin 177: 76 of cap free
Amount of items: 3
Items: 
Size: 9732 Color: 444
Size: 3037 Color: 331
Size: 1635 Color: 250

Bin 178: 88 of cap free
Amount of items: 2
Items: 
Size: 7256 Color: 411
Size: 7136 Color: 404

Bin 179: 88 of cap free
Amount of items: 3
Items: 
Size: 8632 Color: 426
Size: 5464 Color: 389
Size: 296 Color: 45

Bin 180: 92 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 476
Size: 3356 Color: 342
Size: 160 Color: 5

Bin 181: 105 of cap free
Amount of items: 3
Items: 
Size: 10843 Color: 475
Size: 3340 Color: 339
Size: 192 Color: 6

Bin 182: 134 of cap free
Amount of items: 3
Items: 
Size: 10891 Color: 477
Size: 3359 Color: 343
Size: 96 Color: 4

Bin 183: 147 of cap free
Amount of items: 3
Items: 
Size: 8331 Color: 424
Size: 5704 Color: 392
Size: 298 Color: 47

Bin 184: 157 of cap free
Amount of items: 2
Items: 
Size: 9052 Color: 434
Size: 5271 Color: 387

Bin 185: 186 of cap free
Amount of items: 3
Items: 
Size: 7640 Color: 415
Size: 6336 Color: 402
Size: 318 Color: 56

Bin 186: 190 of cap free
Amount of items: 2
Items: 
Size: 9036 Color: 432
Size: 5254 Color: 386

Bin 187: 196 of cap free
Amount of items: 4
Items: 
Size: 9192 Color: 436
Size: 4524 Color: 371
Size: 288 Color: 38
Size: 280 Color: 35

Bin 188: 206 of cap free
Amount of items: 26
Items: 
Size: 672 Color: 149
Size: 670 Color: 148
Size: 670 Color: 147
Size: 668 Color: 146
Size: 666 Color: 145
Size: 664 Color: 144
Size: 664 Color: 143
Size: 640 Color: 141
Size: 640 Color: 140
Size: 616 Color: 136
Size: 606 Color: 135
Size: 598 Color: 134
Size: 594 Color: 132
Size: 592 Color: 130
Size: 480 Color: 109
Size: 452 Color: 105
Size: 452 Color: 104
Size: 452 Color: 103
Size: 448 Color: 100
Size: 440 Color: 99
Size: 440 Color: 98
Size: 440 Color: 97
Size: 432 Color: 96
Size: 432 Color: 95
Size: 424 Color: 94
Size: 422 Color: 93

Bin 189: 208 of cap free
Amount of items: 21
Items: 
Size: 896 Color: 175
Size: 880 Color: 173
Size: 854 Color: 170
Size: 808 Color: 167
Size: 792 Color: 165
Size: 784 Color: 164
Size: 776 Color: 163
Size: 760 Color: 162
Size: 752 Color: 161
Size: 752 Color: 160
Size: 736 Color: 159
Size: 700 Color: 156
Size: 700 Color: 155
Size: 696 Color: 154
Size: 680 Color: 151
Size: 674 Color: 150
Size: 412 Color: 90
Size: 412 Color: 89
Size: 408 Color: 88
Size: 400 Color: 86
Size: 400 Color: 85

Bin 190: 222 of cap free
Amount of items: 2
Items: 
Size: 8222 Color: 420
Size: 6036 Color: 401

Bin 191: 223 of cap free
Amount of items: 3
Items: 
Size: 8312 Color: 423
Size: 5641 Color: 391
Size: 304 Color: 49

Bin 192: 226 of cap free
Amount of items: 2
Items: 
Size: 8220 Color: 419
Size: 6034 Color: 400

Bin 193: 230 of cap free
Amount of items: 2
Items: 
Size: 9030 Color: 431
Size: 5220 Color: 385

Bin 194: 242 of cap free
Amount of items: 6
Items: 
Size: 7250 Color: 409
Size: 2192 Color: 286
Size: 1512 Color: 236
Size: 1476 Color: 228
Size: 1456 Color: 226
Size: 352 Color: 69

Bin 195: 244 of cap free
Amount of items: 2
Items: 
Size: 8203 Color: 418
Size: 6033 Color: 399

Bin 196: 294 of cap free
Amount of items: 2
Items: 
Size: 7252 Color: 410
Size: 6934 Color: 403

Bin 197: 294 of cap free
Amount of items: 2
Items: 
Size: 8155 Color: 416
Size: 6031 Color: 398

Bin 198: 346 of cap free
Amount of items: 8
Items: 
Size: 7244 Color: 407
Size: 1318 Color: 213
Size: 1244 Color: 206
Size: 1204 Color: 201
Size: 1204 Color: 199
Size: 1200 Color: 198
Size: 368 Color: 73
Size: 352 Color: 71

Bin 199: 8494 of cap free
Amount of items: 11
Items: 
Size: 592 Color: 129
Size: 568 Color: 128
Size: 568 Color: 127
Size: 560 Color: 126
Size: 552 Color: 122
Size: 544 Color: 121
Size: 544 Color: 120
Size: 542 Color: 119
Size: 528 Color: 117
Size: 508 Color: 115
Size: 480 Color: 110

Total size: 2867040
Total free space: 14480

