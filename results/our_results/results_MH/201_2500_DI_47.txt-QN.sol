Capicity Bin: 1976
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1368 Color: 157
Size: 502 Color: 116
Size: 50 Color: 21
Size: 40 Color: 9
Size: 16 Color: 1

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1774 Color: 200
Size: 100 Color: 52
Size: 56 Color: 25
Size: 46 Color: 15

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1650 Color: 184
Size: 170 Color: 70
Size: 108 Color: 55
Size: 48 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1333 Color: 155
Size: 537 Color: 119
Size: 106 Color: 54

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 174
Size: 346 Color: 99
Size: 68 Color: 34

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1075 Color: 143
Size: 751 Color: 131
Size: 150 Color: 65

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 163
Size: 462 Color: 112
Size: 92 Color: 48

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1524 Color: 171
Size: 380 Color: 102
Size: 72 Color: 38

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 158
Size: 550 Color: 121
Size: 52 Color: 24

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 160
Size: 481 Color: 114
Size: 94 Color: 50

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 175
Size: 282 Color: 89
Size: 116 Color: 59

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 145
Size: 718 Color: 129
Size: 140 Color: 63

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 197
Size: 213 Color: 74
Size: 42 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 989 Color: 138
Size: 823 Color: 135
Size: 164 Color: 67

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 172
Size: 362 Color: 101
Size: 72 Color: 37

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1700 Color: 193
Size: 236 Color: 80
Size: 40 Color: 10

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 177
Size: 319 Color: 96
Size: 62 Color: 31

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 161
Size: 502 Color: 117
Size: 60 Color: 27

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 178
Size: 311 Color: 94
Size: 62 Color: 30

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1611 Color: 180
Size: 305 Color: 91
Size: 60 Color: 28

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1705 Color: 194
Size: 227 Color: 78
Size: 44 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 181
Size: 307 Color: 93
Size: 42 Color: 13

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 800 Color: 133
Size: 668 Color: 127
Size: 304 Color: 90
Size: 204 Color: 71

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 190
Size: 246 Color: 82
Size: 48 Color: 19

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 192
Size: 259 Color: 85
Size: 20 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 165
Size: 427 Color: 109
Size: 84 Color: 45

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1635 Color: 182
Size: 317 Color: 95
Size: 24 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 188
Size: 253 Color: 84
Size: 50 Color: 22

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1023 Color: 141
Size: 795 Color: 132
Size: 158 Color: 66

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 198
Size: 210 Color: 73
Size: 40 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 186
Size: 262 Color: 86
Size: 48 Color: 17

Bin 32: 0 of cap free
Amount of items: 4
Items: 
Size: 1511 Color: 170
Size: 389 Color: 103
Size: 68 Color: 35
Size: 8 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1189 Color: 147
Size: 657 Color: 126
Size: 130 Color: 61

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1054 Color: 142
Size: 866 Color: 136
Size: 56 Color: 26

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 173
Size: 361 Color: 100
Size: 72 Color: 39

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 990 Color: 139
Size: 822 Color: 134
Size: 164 Color: 68

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1341 Color: 156
Size: 531 Color: 118
Size: 104 Color: 53

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 195
Size: 221 Color: 77
Size: 42 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 150
Size: 408 Color: 108
Size: 306 Color: 92

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 168
Size: 395 Color: 105
Size: 78 Color: 41

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 187
Size: 233 Color: 79
Size: 76 Color: 40

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1395 Color: 159
Size: 485 Color: 115
Size: 96 Color: 51

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 169
Size: 391 Color: 104
Size: 78 Color: 42

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 154
Size: 542 Color: 120
Size: 108 Color: 56

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 201
Size: 166 Color: 69
Size: 32 Color: 6

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1275 Color: 151
Size: 585 Color: 123
Size: 116 Color: 58

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 196
Size: 218 Color: 76
Size: 40 Color: 7

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 148
Size: 651 Color: 125
Size: 128 Color: 60

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1318 Color: 153
Size: 598 Color: 124
Size: 60 Color: 29

Bin 50: 0 of cap free
Amount of items: 4
Items: 
Size: 1236 Color: 149
Size: 456 Color: 111
Size: 216 Color: 75
Size: 68 Color: 36

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 191
Size: 241 Color: 81
Size: 46 Color: 16

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 176
Size: 323 Color: 97
Size: 64 Color: 32

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1610 Color: 179
Size: 334 Color: 98
Size: 32 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 162
Size: 467 Color: 113
Size: 92 Color: 49

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 183
Size: 274 Color: 88
Size: 64 Color: 33

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1313 Color: 152
Size: 553 Color: 122
Size: 110 Color: 57

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 164
Size: 430 Color: 110
Size: 84 Color: 46

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 166
Size: 402 Color: 107
Size: 80 Color: 44

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 991 Color: 140
Size: 899 Color: 137
Size: 86 Color: 47

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 185
Size: 266 Color: 87
Size: 52 Color: 23

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1499 Color: 167
Size: 399 Color: 106
Size: 78 Color: 43

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1158 Color: 146
Size: 682 Color: 128
Size: 136 Color: 62

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1103 Color: 144
Size: 729 Color: 130
Size: 144 Color: 64

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 189
Size: 249 Color: 83
Size: 48 Color: 20

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 199
Size: 206 Color: 72
Size: 32 Color: 5

Total size: 128440
Total free space: 0

