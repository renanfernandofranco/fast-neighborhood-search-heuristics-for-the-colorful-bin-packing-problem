Capicity Bin: 1000001
Lower Bound: 4512

Bins used: 4514
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 715233 Color: 0
Size: 160750 Color: 1
Size: 124018 Color: 0

Bin 2: 0 of cap free
Amount of items: 6
Items: 
Size: 170464 Color: 0
Size: 169314 Color: 0
Size: 169231 Color: 0
Size: 165479 Color: 1
Size: 162977 Color: 1
Size: 162536 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 429335 Color: 1
Size: 406811 Color: 0
Size: 163855 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 702509 Color: 1
Size: 163110 Color: 0
Size: 134382 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 677057 Color: 0
Size: 184293 Color: 1
Size: 138651 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 500358 Color: 0
Size: 397004 Color: 1
Size: 102639 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 614295 Color: 0
Size: 222727 Color: 1
Size: 162979 Color: 0

Bin 8: 0 of cap free
Amount of items: 5
Items: 
Size: 244885 Color: 0
Size: 204257 Color: 0
Size: 198552 Color: 1
Size: 195513 Color: 1
Size: 156794 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 576311 Color: 0
Size: 226093 Color: 0
Size: 197597 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 726984 Color: 0
Size: 159247 Color: 0
Size: 113770 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 428531 Color: 1
Size: 318034 Color: 1
Size: 253436 Color: 0

Bin 12: 0 of cap free
Amount of items: 5
Items: 
Size: 412780 Color: 1
Size: 181031 Color: 0
Size: 161489 Color: 0
Size: 139588 Color: 1
Size: 105113 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 547087 Color: 1
Size: 296614 Color: 0
Size: 156300 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 785743 Color: 0
Size: 108122 Color: 1
Size: 106136 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 735451 Color: 1
Size: 153134 Color: 1
Size: 111416 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 713479 Color: 0
Size: 145385 Color: 1
Size: 141137 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 627498 Color: 1
Size: 210737 Color: 0
Size: 161766 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 613277 Color: 1
Size: 286237 Color: 0
Size: 100487 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 753429 Color: 0
Size: 134183 Color: 1
Size: 112389 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 716098 Color: 0
Size: 153456 Color: 1
Size: 130447 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 490580 Color: 0
Size: 391061 Color: 1
Size: 118360 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 782102 Color: 1
Size: 115129 Color: 0
Size: 102770 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 741174 Color: 0
Size: 151640 Color: 1
Size: 107187 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 571091 Color: 0
Size: 269859 Color: 1
Size: 159051 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 642486 Color: 1
Size: 210502 Color: 1
Size: 147013 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 742638 Color: 1
Size: 137260 Color: 0
Size: 120103 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 714515 Color: 1
Size: 154022 Color: 1
Size: 131464 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 680062 Color: 1
Size: 161531 Color: 0
Size: 158408 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 590522 Color: 0
Size: 306312 Color: 1
Size: 103167 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 596779 Color: 0
Size: 300212 Color: 1
Size: 103010 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 761067 Color: 0
Size: 132831 Color: 1
Size: 106103 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 769695 Color: 0
Size: 128027 Color: 1
Size: 102279 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 572704 Color: 0
Size: 325792 Color: 1
Size: 101505 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 580157 Color: 0
Size: 317832 Color: 1
Size: 102012 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 747587 Color: 1
Size: 127989 Color: 1
Size: 124425 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 477592 Color: 0
Size: 398890 Color: 1
Size: 123519 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 566423 Color: 1
Size: 317391 Color: 0
Size: 116187 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 791992 Color: 0
Size: 105655 Color: 0
Size: 102354 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 451647 Color: 1
Size: 432804 Color: 0
Size: 115550 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 555381 Color: 1
Size: 335360 Color: 0
Size: 109260 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 785724 Color: 1
Size: 108943 Color: 1
Size: 105334 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 792881 Color: 0
Size: 105353 Color: 0
Size: 101767 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 458949 Color: 0
Size: 431005 Color: 1
Size: 110047 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 757754 Color: 0
Size: 136225 Color: 1
Size: 106022 Color: 1

Bin 45: 0 of cap free
Amount of items: 6
Items: 
Size: 281957 Color: 0
Size: 170823 Color: 0
Size: 170339 Color: 0
Size: 165684 Color: 1
Size: 106399 Color: 1
Size: 104799 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 525765 Color: 0
Size: 372560 Color: 1
Size: 101676 Color: 1

Bin 47: 0 of cap free
Amount of items: 7
Items: 
Size: 156335 Color: 1
Size: 150651 Color: 1
Size: 150592 Color: 1
Size: 146268 Color: 0
Size: 146266 Color: 0
Size: 132473 Color: 0
Size: 117416 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 576257 Color: 0
Size: 236340 Color: 1
Size: 187404 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 556976 Color: 1
Size: 336218 Color: 0
Size: 106807 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 588317 Color: 0
Size: 311080 Color: 1
Size: 100604 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 722421 Color: 1
Size: 172866 Color: 0
Size: 104714 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 739653 Color: 0
Size: 154839 Color: 1
Size: 105509 Color: 1

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 626932 Color: 0
Size: 373069 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 469199 Color: 0
Size: 429800 Color: 1
Size: 101002 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 791082 Color: 0
Size: 106265 Color: 1
Size: 102654 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 458668 Color: 0
Size: 434472 Color: 1
Size: 106861 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 682427 Color: 1
Size: 202563 Color: 0
Size: 115011 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 445249 Color: 0
Size: 429129 Color: 1
Size: 125623 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 695410 Color: 1
Size: 168055 Color: 0
Size: 136536 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 737250 Color: 1
Size: 150617 Color: 0
Size: 112134 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 778122 Color: 1
Size: 120868 Color: 0
Size: 101011 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 744718 Color: 1
Size: 151794 Color: 1
Size: 103489 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 768915 Color: 1
Size: 130492 Color: 0
Size: 100594 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 528172 Color: 0
Size: 322340 Color: 1
Size: 149489 Color: 0

Bin 65: 0 of cap free
Amount of items: 6
Items: 
Size: 179668 Color: 1
Size: 177912 Color: 0
Size: 175072 Color: 1
Size: 174683 Color: 1
Size: 174491 Color: 0
Size: 118175 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 755303 Color: 1
Size: 133887 Color: 0
Size: 110811 Color: 1

Bin 67: 0 of cap free
Amount of items: 7
Items: 
Size: 157340 Color: 1
Size: 155569 Color: 0
Size: 152958 Color: 0
Size: 152089 Color: 0
Size: 150799 Color: 1
Size: 116401 Color: 0
Size: 114845 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 462876 Color: 0
Size: 428813 Color: 1
Size: 108312 Color: 0

Bin 69: 0 of cap free
Amount of items: 7
Items: 
Size: 148441 Color: 1
Size: 148195 Color: 1
Size: 147329 Color: 1
Size: 147290 Color: 1
Size: 142959 Color: 0
Size: 142356 Color: 0
Size: 123431 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 496828 Color: 0
Size: 382274 Color: 1
Size: 120899 Color: 1

Bin 71: 0 of cap free
Amount of items: 6
Items: 
Size: 174851 Color: 0
Size: 174664 Color: 0
Size: 173433 Color: 1
Size: 172264 Color: 1
Size: 171131 Color: 1
Size: 133658 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 791680 Color: 0
Size: 106814 Color: 0
Size: 101507 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 595345 Color: 1
Size: 294461 Color: 0
Size: 110195 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 726797 Color: 0
Size: 167232 Color: 1
Size: 105972 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 781082 Color: 0
Size: 116019 Color: 1
Size: 102900 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 730154 Color: 1
Size: 144488 Color: 0
Size: 125359 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 722971 Color: 0
Size: 175054 Color: 1
Size: 101976 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 654179 Color: 0
Size: 215703 Color: 1
Size: 130119 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 646698 Color: 1
Size: 210451 Color: 0
Size: 142852 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 749715 Color: 1
Size: 143462 Color: 0
Size: 106824 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 459957 Color: 0
Size: 431132 Color: 1
Size: 108912 Color: 1

Bin 82: 0 of cap free
Amount of items: 8
Items: 
Size: 130997 Color: 1
Size: 130214 Color: 1
Size: 129851 Color: 1
Size: 128909 Color: 1
Size: 125548 Color: 0
Size: 124653 Color: 0
Size: 122297 Color: 0
Size: 107532 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 688895 Color: 0
Size: 178933 Color: 1
Size: 132173 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 750628 Color: 1
Size: 143791 Color: 0
Size: 105582 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 655693 Color: 0
Size: 180612 Color: 0
Size: 163696 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 722965 Color: 0
Size: 176015 Color: 1
Size: 101021 Color: 0

Bin 87: 0 of cap free
Amount of items: 6
Items: 
Size: 176692 Color: 0
Size: 176044 Color: 0
Size: 174621 Color: 1
Size: 174318 Color: 1
Size: 174298 Color: 1
Size: 124028 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 743016 Color: 1
Size: 155913 Color: 0
Size: 101072 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 714712 Color: 1
Size: 170839 Color: 0
Size: 114450 Color: 0

Bin 90: 0 of cap free
Amount of items: 7
Items: 
Size: 160727 Color: 1
Size: 142982 Color: 1
Size: 140844 Color: 1
Size: 139740 Color: 0
Size: 139547 Color: 0
Size: 138665 Color: 0
Size: 137496 Color: 0

Bin 91: 0 of cap free
Amount of items: 7
Items: 
Size: 164254 Color: 0
Size: 140597 Color: 0
Size: 140561 Color: 1
Size: 140049 Color: 1
Size: 139855 Color: 1
Size: 137459 Color: 0
Size: 137226 Color: 0

Bin 92: 0 of cap free
Amount of items: 6
Items: 
Size: 231134 Color: 0
Size: 157972 Color: 0
Size: 156408 Color: 1
Size: 156002 Color: 1
Size: 150301 Color: 1
Size: 148184 Color: 0

Bin 93: 0 of cap free
Amount of items: 6
Items: 
Size: 230923 Color: 0
Size: 193169 Color: 0
Size: 176849 Color: 1
Size: 155689 Color: 1
Size: 137696 Color: 1
Size: 105675 Color: 0

Bin 94: 0 of cap free
Amount of items: 5
Items: 
Size: 245600 Color: 1
Size: 189474 Color: 0
Size: 189372 Color: 0
Size: 187778 Color: 1
Size: 187777 Color: 0

Bin 95: 0 of cap free
Amount of items: 6
Items: 
Size: 317509 Color: 0
Size: 184792 Color: 1
Size: 163034 Color: 1
Size: 115361 Color: 1
Size: 109904 Color: 0
Size: 109401 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 787172 Color: 0
Size: 112630 Color: 1
Size: 100199 Color: 0

Bin 97: 0 of cap free
Amount of items: 5
Items: 
Size: 234570 Color: 1
Size: 223685 Color: 1
Size: 209034 Color: 0
Size: 182951 Color: 1
Size: 149761 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 435182 Color: 1
Size: 406920 Color: 0
Size: 157899 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 456855 Color: 1
Size: 435325 Color: 1
Size: 107821 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 458438 Color: 0
Size: 430257 Color: 1
Size: 111306 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 481736 Color: 0
Size: 408789 Color: 0
Size: 109476 Color: 1

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 502007 Color: 0
Size: 497994 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 502269 Color: 0
Size: 300497 Color: 1
Size: 197235 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 504550 Color: 0
Size: 372891 Color: 0
Size: 122560 Color: 1

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 508954 Color: 0
Size: 491047 Color: 1

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 511213 Color: 1
Size: 488788 Color: 0

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 517416 Color: 1
Size: 482585 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 522548 Color: 1
Size: 284773 Color: 1
Size: 192680 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 522609 Color: 0
Size: 377242 Color: 1
Size: 100150 Color: 0

Bin 110: 0 of cap free
Amount of items: 2
Items: 
Size: 523770 Color: 1
Size: 476231 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 524266 Color: 0
Size: 267415 Color: 1
Size: 208320 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 526647 Color: 0
Size: 356202 Color: 0
Size: 117152 Color: 1

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 536880 Color: 0
Size: 463121 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 539400 Color: 0
Size: 280244 Color: 1
Size: 180357 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 551115 Color: 0
Size: 317666 Color: 1
Size: 131220 Color: 0

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 555687 Color: 0
Size: 444314 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 556252 Color: 1
Size: 306862 Color: 1
Size: 136887 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 557377 Color: 1
Size: 277758 Color: 0
Size: 164866 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 565721 Color: 1
Size: 268278 Color: 0
Size: 166002 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 572564 Color: 1
Size: 306859 Color: 1
Size: 120578 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 574834 Color: 1
Size: 220848 Color: 1
Size: 204319 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 575978 Color: 1
Size: 230495 Color: 0
Size: 193528 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 576096 Color: 0
Size: 233738 Color: 0
Size: 190167 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 577042 Color: 0
Size: 223583 Color: 1
Size: 199376 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 577507 Color: 1
Size: 244459 Color: 1
Size: 178035 Color: 0

Bin 126: 0 of cap free
Amount of items: 2
Items: 
Size: 580406 Color: 1
Size: 419595 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 581532 Color: 1
Size: 280747 Color: 0
Size: 137722 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 588282 Color: 0
Size: 220011 Color: 1
Size: 191708 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 589792 Color: 0
Size: 301071 Color: 1
Size: 109138 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 590084 Color: 1
Size: 223858 Color: 1
Size: 186059 Color: 0

Bin 131: 0 of cap free
Amount of items: 2
Items: 
Size: 590467 Color: 1
Size: 409534 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 594200 Color: 0
Size: 209043 Color: 1
Size: 196758 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 594540 Color: 0
Size: 268147 Color: 0
Size: 137314 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 594575 Color: 0
Size: 250710 Color: 1
Size: 154716 Color: 0

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 597552 Color: 1
Size: 402449 Color: 0

Bin 136: 0 of cap free
Amount of items: 2
Items: 
Size: 598315 Color: 0
Size: 401686 Color: 1

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 599483 Color: 0
Size: 400518 Color: 1

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 600177 Color: 1
Size: 399824 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 601904 Color: 0
Size: 245889 Color: 1
Size: 152208 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 602961 Color: 1
Size: 250808 Color: 1
Size: 146232 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 603175 Color: 1
Size: 280060 Color: 1
Size: 116766 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 608248 Color: 1
Size: 281234 Color: 0
Size: 110519 Color: 0

Bin 143: 0 of cap free
Amount of items: 2
Items: 
Size: 609021 Color: 1
Size: 390980 Color: 0

Bin 144: 0 of cap free
Amount of items: 2
Items: 
Size: 609950 Color: 1
Size: 390051 Color: 0

Bin 145: 0 of cap free
Amount of items: 2
Items: 
Size: 611394 Color: 0
Size: 388607 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 612012 Color: 1
Size: 209899 Color: 0
Size: 178090 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 612035 Color: 1
Size: 194280 Color: 1
Size: 193686 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 612141 Color: 0
Size: 195230 Color: 0
Size: 192630 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 613372 Color: 0
Size: 244614 Color: 1
Size: 142015 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 613395 Color: 1
Size: 209032 Color: 0
Size: 177574 Color: 1

Bin 151: 0 of cap free
Amount of items: 2
Items: 
Size: 614287 Color: 0
Size: 385714 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 620086 Color: 1
Size: 195321 Color: 0
Size: 184594 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 623277 Color: 0
Size: 188899 Color: 0
Size: 187825 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 626481 Color: 0
Size: 207210 Color: 0
Size: 166310 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 628166 Color: 0
Size: 223247 Color: 1
Size: 148588 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 629217 Color: 0
Size: 261187 Color: 1
Size: 109597 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 629085 Color: 1
Size: 197847 Color: 1
Size: 173069 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 629233 Color: 0
Size: 191600 Color: 1
Size: 179168 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 631414 Color: 0
Size: 220368 Color: 1
Size: 148219 Color: 0

Bin 160: 0 of cap free
Amount of items: 2
Items: 
Size: 633479 Color: 1
Size: 366522 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 635197 Color: 1
Size: 206776 Color: 0
Size: 158028 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 635224 Color: 0
Size: 194303 Color: 0
Size: 170474 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 635373 Color: 0
Size: 193322 Color: 0
Size: 171306 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 636619 Color: 0
Size: 182395 Color: 0
Size: 180987 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 636584 Color: 1
Size: 222665 Color: 1
Size: 140752 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 637833 Color: 0
Size: 195702 Color: 1
Size: 166466 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 637834 Color: 1
Size: 189989 Color: 0
Size: 172178 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 637865 Color: 0
Size: 195846 Color: 0
Size: 166290 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 639830 Color: 0
Size: 183008 Color: 1
Size: 177163 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 639999 Color: 1
Size: 246318 Color: 0
Size: 113684 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 640174 Color: 1
Size: 206645 Color: 0
Size: 153182 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 640283 Color: 0
Size: 185763 Color: 1
Size: 173955 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 640327 Color: 0
Size: 194088 Color: 1
Size: 165586 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 640404 Color: 0
Size: 187024 Color: 1
Size: 172573 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 641523 Color: 1
Size: 184392 Color: 1
Size: 174086 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 641429 Color: 0
Size: 185640 Color: 1
Size: 172932 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 642475 Color: 0
Size: 197313 Color: 0
Size: 160213 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 642977 Color: 0
Size: 196300 Color: 0
Size: 160724 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 643188 Color: 0
Size: 197064 Color: 1
Size: 159749 Color: 1

Bin 180: 0 of cap free
Amount of items: 2
Items: 
Size: 643365 Color: 1
Size: 356636 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 644736 Color: 0
Size: 181078 Color: 0
Size: 174187 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 645203 Color: 1
Size: 209914 Color: 0
Size: 144884 Color: 1

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 645282 Color: 1
Size: 198849 Color: 0
Size: 155870 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 645392 Color: 1
Size: 205072 Color: 0
Size: 149537 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 645411 Color: 1
Size: 201034 Color: 0
Size: 153556 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 646003 Color: 0
Size: 189178 Color: 0
Size: 164820 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 647356 Color: 0
Size: 192666 Color: 0
Size: 159979 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 648504 Color: 1
Size: 181558 Color: 0
Size: 169939 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 649007 Color: 0
Size: 192517 Color: 0
Size: 158477 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 649093 Color: 0
Size: 175819 Color: 1
Size: 175089 Color: 1

Bin 191: 0 of cap free
Amount of items: 2
Items: 
Size: 649221 Color: 1
Size: 350780 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 649687 Color: 0
Size: 190426 Color: 1
Size: 159888 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 650042 Color: 1
Size: 181195 Color: 1
Size: 168764 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 650626 Color: 1
Size: 178164 Color: 0
Size: 171211 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 650622 Color: 0
Size: 182040 Color: 1
Size: 167339 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 650663 Color: 1
Size: 176425 Color: 1
Size: 172913 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 650781 Color: 0
Size: 184285 Color: 1
Size: 164935 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 651014 Color: 1
Size: 176385 Color: 0
Size: 172602 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 650947 Color: 0
Size: 190179 Color: 0
Size: 158875 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 651039 Color: 1
Size: 198463 Color: 0
Size: 150499 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 652019 Color: 1
Size: 175106 Color: 0
Size: 172876 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 652553 Color: 1
Size: 191275 Color: 1
Size: 156173 Color: 0

Bin 203: 0 of cap free
Amount of items: 2
Items: 
Size: 653420 Color: 0
Size: 346581 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 653605 Color: 0
Size: 194522 Color: 0
Size: 151874 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 653904 Color: 1
Size: 175557 Color: 0
Size: 170540 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 654146 Color: 0
Size: 197363 Color: 1
Size: 148492 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 654309 Color: 1
Size: 177770 Color: 1
Size: 167922 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 654417 Color: 1
Size: 176666 Color: 0
Size: 168918 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 654778 Color: 1
Size: 204181 Color: 0
Size: 141042 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 654870 Color: 1
Size: 186420 Color: 0
Size: 158711 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 654928 Color: 0
Size: 175484 Color: 0
Size: 169589 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 655077 Color: 1
Size: 197013 Color: 1
Size: 147911 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 658836 Color: 0
Size: 203894 Color: 0
Size: 137271 Color: 1

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 660619 Color: 1
Size: 339382 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 661243 Color: 1
Size: 233769 Color: 0
Size: 104989 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 661332 Color: 0
Size: 176491 Color: 1
Size: 162178 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 661972 Color: 1
Size: 189697 Color: 0
Size: 148332 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 664423 Color: 0
Size: 192798 Color: 0
Size: 142780 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 664547 Color: 1
Size: 193986 Color: 0
Size: 141468 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 667379 Color: 0
Size: 186837 Color: 1
Size: 145785 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 669863 Color: 0
Size: 171221 Color: 1
Size: 158917 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 670146 Color: 1
Size: 166315 Color: 1
Size: 163540 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 670245 Color: 1
Size: 186557 Color: 0
Size: 143199 Color: 0

Bin 224: 0 of cap free
Amount of items: 2
Items: 
Size: 670425 Color: 1
Size: 329576 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 670494 Color: 1
Size: 206003 Color: 0
Size: 123504 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 670654 Color: 1
Size: 176455 Color: 0
Size: 152892 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 670681 Color: 1
Size: 172233 Color: 1
Size: 157087 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 671210 Color: 0
Size: 174615 Color: 0
Size: 154176 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 671196 Color: 1
Size: 173642 Color: 0
Size: 155163 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 671216 Color: 0
Size: 173726 Color: 1
Size: 155059 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 671238 Color: 1
Size: 203942 Color: 0
Size: 124821 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 671578 Color: 0
Size: 183715 Color: 1
Size: 144708 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 671606 Color: 1
Size: 172073 Color: 0
Size: 156322 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 671637 Color: 0
Size: 198278 Color: 0
Size: 130086 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 671932 Color: 1
Size: 176353 Color: 0
Size: 151716 Color: 1

Bin 236: 0 of cap free
Amount of items: 2
Items: 
Size: 672760 Color: 0
Size: 327241 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 672921 Color: 1
Size: 181529 Color: 1
Size: 145551 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 673264 Color: 1
Size: 166562 Color: 0
Size: 160175 Color: 1

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 673264 Color: 1
Size: 183241 Color: 0
Size: 143496 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 673293 Color: 1
Size: 163764 Color: 1
Size: 162944 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 673317 Color: 1
Size: 180647 Color: 0
Size: 146037 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 673401 Color: 0
Size: 171833 Color: 1
Size: 154767 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 673398 Color: 1
Size: 170423 Color: 0
Size: 156180 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 674258 Color: 0
Size: 181303 Color: 0
Size: 144440 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 674391 Color: 1
Size: 170276 Color: 0
Size: 155334 Color: 1

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 674324 Color: 0
Size: 197181 Color: 1
Size: 128496 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 674489 Color: 1
Size: 210468 Color: 1
Size: 115044 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 674533 Color: 0
Size: 187852 Color: 1
Size: 137616 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 675913 Color: 1
Size: 165982 Color: 1
Size: 158106 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 675997 Color: 1
Size: 209988 Color: 0
Size: 114016 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 676064 Color: 1
Size: 179995 Color: 0
Size: 143942 Color: 1

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 677019 Color: 1
Size: 184731 Color: 0
Size: 138251 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 677673 Color: 1
Size: 181286 Color: 0
Size: 141042 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 677761 Color: 1
Size: 210120 Color: 0
Size: 112120 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 682592 Color: 0
Size: 190146 Color: 0
Size: 127263 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 683152 Color: 0
Size: 199135 Color: 0
Size: 117714 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 684224 Color: 1
Size: 157918 Color: 0
Size: 157859 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 684268 Color: 1
Size: 192975 Color: 0
Size: 122758 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 684341 Color: 0
Size: 169039 Color: 1
Size: 146621 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 684464 Color: 1
Size: 198260 Color: 0
Size: 117277 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 685307 Color: 1
Size: 170829 Color: 0
Size: 143865 Color: 0

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 687438 Color: 1
Size: 312563 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 688452 Color: 1
Size: 188289 Color: 0
Size: 123260 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 688662 Color: 1
Size: 193551 Color: 1
Size: 117788 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 690759 Color: 1
Size: 170212 Color: 0
Size: 139030 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 691285 Color: 1
Size: 164917 Color: 0
Size: 143799 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 691406 Color: 1
Size: 163788 Color: 1
Size: 144807 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 691523 Color: 1
Size: 193117 Color: 0
Size: 115361 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 691731 Color: 1
Size: 181175 Color: 0
Size: 127095 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 692079 Color: 1
Size: 167647 Color: 1
Size: 140275 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 692085 Color: 1
Size: 192676 Color: 0
Size: 115240 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 693776 Color: 0
Size: 159060 Color: 0
Size: 147165 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 697102 Color: 0
Size: 162934 Color: 0
Size: 139965 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 699857 Color: 0
Size: 159770 Color: 1
Size: 140374 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 699922 Color: 1
Size: 194158 Color: 0
Size: 105921 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 699907 Color: 0
Size: 183455 Color: 0
Size: 116639 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 700002 Color: 1
Size: 178923 Color: 0
Size: 121076 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 700140 Color: 0
Size: 195548 Color: 0
Size: 104313 Color: 1

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 700575 Color: 1
Size: 299426 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 702374 Color: 0
Size: 173318 Color: 0
Size: 124309 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 702707 Color: 1
Size: 186133 Color: 1
Size: 111161 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 702659 Color: 0
Size: 167464 Color: 0
Size: 129878 Color: 1

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 702734 Color: 1
Size: 184744 Color: 0
Size: 112523 Color: 1

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 702724 Color: 0
Size: 171511 Color: 0
Size: 125766 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 702936 Color: 1
Size: 154716 Color: 1
Size: 142349 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 702891 Color: 0
Size: 175261 Color: 1
Size: 121849 Color: 0

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 703110 Color: 0
Size: 296891 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 705079 Color: 1
Size: 156173 Color: 0
Size: 138749 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 705043 Color: 0
Size: 170780 Color: 1
Size: 124178 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 705321 Color: 1
Size: 151609 Color: 0
Size: 143071 Color: 1

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 705330 Color: 0
Size: 151987 Color: 0
Size: 142684 Color: 1

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 707381 Color: 1
Size: 164324 Color: 0
Size: 128296 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 707447 Color: 0
Size: 169115 Color: 0
Size: 123439 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 707399 Color: 1
Size: 151114 Color: 1
Size: 141488 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 707586 Color: 0
Size: 152647 Color: 0
Size: 139768 Color: 1

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 710162 Color: 1
Size: 148624 Color: 0
Size: 141215 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 712527 Color: 0
Size: 167236 Color: 0
Size: 120238 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 712571 Color: 1
Size: 153016 Color: 0
Size: 134414 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 714959 Color: 0
Size: 143447 Color: 0
Size: 141595 Color: 1

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 715026 Color: 1
Size: 153144 Color: 1
Size: 131831 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 715471 Color: 1
Size: 167651 Color: 1
Size: 116879 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 715574 Color: 1
Size: 181015 Color: 0
Size: 103412 Color: 0

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 716962 Color: 1
Size: 283039 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 719798 Color: 1
Size: 150969 Color: 1
Size: 129234 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 719961 Color: 0
Size: 170131 Color: 0
Size: 109909 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 720803 Color: 0
Size: 143039 Color: 0
Size: 136159 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 720959 Color: 0
Size: 147652 Color: 1
Size: 131390 Color: 1

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 721320 Color: 1
Size: 173002 Color: 1
Size: 105679 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 721319 Color: 0
Size: 168537 Color: 0
Size: 110145 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 722052 Color: 1
Size: 143280 Color: 1
Size: 134669 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 722061 Color: 0
Size: 159340 Color: 0
Size: 118600 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 723467 Color: 0
Size: 158319 Color: 0
Size: 118215 Color: 1

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 723518 Color: 0
Size: 153539 Color: 1
Size: 122944 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 726348 Color: 0
Size: 163904 Color: 0
Size: 109749 Color: 1

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 726544 Color: 1
Size: 165113 Color: 1
Size: 108344 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 726632 Color: 1
Size: 144763 Color: 0
Size: 128606 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 731999 Color: 1
Size: 140833 Color: 0
Size: 127169 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 731913 Color: 0
Size: 142749 Color: 1
Size: 125339 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 732088 Color: 1
Size: 136119 Color: 0
Size: 131794 Color: 1

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 732225 Color: 0
Size: 151815 Color: 1
Size: 115961 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 732239 Color: 1
Size: 148541 Color: 0
Size: 119221 Color: 1

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 732285 Color: 1
Size: 157416 Color: 1
Size: 110300 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 732975 Color: 0
Size: 160390 Color: 1
Size: 106636 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 733068 Color: 1
Size: 159403 Color: 1
Size: 107530 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 733150 Color: 1
Size: 141636 Color: 1
Size: 125215 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 733178 Color: 1
Size: 151430 Color: 0
Size: 115393 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 734364 Color: 0
Size: 144855 Color: 0
Size: 120782 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 734325 Color: 1
Size: 158275 Color: 1
Size: 107401 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 734767 Color: 0
Size: 136907 Color: 0
Size: 128327 Color: 1

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 735242 Color: 1
Size: 143447 Color: 0
Size: 121312 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 735598 Color: 1
Size: 141414 Color: 0
Size: 122989 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 735761 Color: 1
Size: 142944 Color: 0
Size: 121296 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 735996 Color: 1
Size: 149332 Color: 0
Size: 114673 Color: 1

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 736305 Color: 0
Size: 157958 Color: 0
Size: 105738 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 736427 Color: 1
Size: 143501 Color: 1
Size: 120073 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 736504 Color: 0
Size: 139945 Color: 0
Size: 123552 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 736458 Color: 1
Size: 133354 Color: 0
Size: 130189 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 737226 Color: 0
Size: 158004 Color: 0
Size: 104771 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 737540 Color: 1
Size: 153158 Color: 0
Size: 109303 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 738019 Color: 1
Size: 153709 Color: 1
Size: 108273 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 738034 Color: 1
Size: 137764 Color: 0
Size: 124203 Color: 0

Bin 342: 0 of cap free
Amount of items: 2
Items: 
Size: 740003 Color: 0
Size: 259998 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 743037 Color: 1
Size: 136399 Color: 0
Size: 120565 Color: 1

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 743193 Color: 0
Size: 131614 Color: 0
Size: 125194 Color: 1

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 745425 Color: 0
Size: 142729 Color: 0
Size: 111847 Color: 1

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 745709 Color: 0
Size: 142934 Color: 1
Size: 111358 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 745802 Color: 0
Size: 142663 Color: 0
Size: 111536 Color: 1

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 745807 Color: 0
Size: 132498 Color: 1
Size: 121696 Color: 1

Bin 349: 0 of cap free
Amount of items: 2
Items: 
Size: 746099 Color: 1
Size: 253902 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 746445 Color: 1
Size: 137139 Color: 0
Size: 116417 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 746705 Color: 1
Size: 149308 Color: 0
Size: 103988 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 751765 Color: 0
Size: 135393 Color: 1
Size: 112843 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 751766 Color: 0
Size: 128234 Color: 1
Size: 120001 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 751811 Color: 1
Size: 141182 Color: 1
Size: 107008 Color: 0

Bin 355: 0 of cap free
Amount of items: 2
Items: 
Size: 752847 Color: 0
Size: 247154 Color: 1

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 753876 Color: 0
Size: 129409 Color: 0
Size: 116716 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 755093 Color: 0
Size: 123592 Color: 1
Size: 121316 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 757318 Color: 0
Size: 128314 Color: 0
Size: 114369 Color: 1

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 757422 Color: 0
Size: 135943 Color: 1
Size: 106636 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 757525 Color: 1
Size: 129538 Color: 1
Size: 112938 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 757789 Color: 1
Size: 131512 Color: 0
Size: 110700 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 757748 Color: 0
Size: 130480 Color: 0
Size: 111773 Color: 1

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 757883 Color: 1
Size: 242118 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 758345 Color: 0
Size: 121911 Color: 1
Size: 119745 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 759200 Color: 0
Size: 138368 Color: 0
Size: 102433 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 759274 Color: 1
Size: 121758 Color: 1
Size: 118969 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 760002 Color: 1
Size: 122638 Color: 0
Size: 117361 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 760624 Color: 1
Size: 126186 Color: 1
Size: 113191 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 760737 Color: 1
Size: 120261 Color: 0
Size: 119003 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 760797 Color: 1
Size: 124984 Color: 0
Size: 114220 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 761746 Color: 1
Size: 123233 Color: 1
Size: 115022 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 762419 Color: 0
Size: 126692 Color: 1
Size: 110890 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 765180 Color: 0
Size: 131008 Color: 0
Size: 103813 Color: 1

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 766220 Color: 0
Size: 233781 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 766932 Color: 0
Size: 125404 Color: 1
Size: 107665 Color: 1

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 768004 Color: 1
Size: 130495 Color: 0
Size: 101502 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 769548 Color: 0
Size: 127495 Color: 1
Size: 102958 Color: 1

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 769677 Color: 0
Size: 121410 Color: 1
Size: 108914 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 769602 Color: 1
Size: 122103 Color: 0
Size: 108296 Color: 1

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 770582 Color: 0
Size: 124262 Color: 0
Size: 105157 Color: 1

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 770869 Color: 0
Size: 116562 Color: 1
Size: 112570 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 770928 Color: 0
Size: 125783 Color: 1
Size: 103290 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 771287 Color: 0
Size: 119588 Color: 0
Size: 109126 Color: 1

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 773829 Color: 0
Size: 123475 Color: 0
Size: 102697 Color: 1

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 773847 Color: 0
Size: 120680 Color: 1
Size: 105474 Color: 1

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 773895 Color: 0
Size: 121238 Color: 0
Size: 104868 Color: 1

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 774241 Color: 1
Size: 115833 Color: 0
Size: 109927 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 775108 Color: 1
Size: 114010 Color: 0
Size: 110883 Color: 1

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 775328 Color: 1
Size: 117716 Color: 0
Size: 106957 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 775347 Color: 1
Size: 116896 Color: 0
Size: 107758 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 778652 Color: 0
Size: 116994 Color: 1
Size: 104355 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 778733 Color: 1
Size: 113472 Color: 0
Size: 107796 Color: 1

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 778901 Color: 0
Size: 111943 Color: 1
Size: 109157 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 778945 Color: 1
Size: 111098 Color: 0
Size: 109958 Color: 1

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 779218 Color: 1
Size: 112355 Color: 0
Size: 108428 Color: 1

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 779366 Color: 0
Size: 114753 Color: 0
Size: 105882 Color: 1

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 779389 Color: 1
Size: 113682 Color: 0
Size: 106930 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 779482 Color: 1
Size: 114538 Color: 0
Size: 105981 Color: 0

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 781099 Color: 1
Size: 218902 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 782839 Color: 0
Size: 110810 Color: 0
Size: 106352 Color: 1

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 778614 Color: 1
Size: 112697 Color: 1
Size: 108689 Color: 0

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 737489 Color: 1
Size: 132979 Color: 0
Size: 129532 Color: 1

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 770786 Color: 1
Size: 119688 Color: 0
Size: 109526 Color: 1

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 775496 Color: 1
Size: 120305 Color: 0
Size: 104199 Color: 1

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 723540 Color: 1
Size: 164117 Color: 0
Size: 112343 Color: 1

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 729887 Color: 0
Size: 145768 Color: 1
Size: 124345 Color: 0

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 570937 Color: 1
Size: 268758 Color: 0
Size: 160305 Color: 0

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 428487 Color: 1
Size: 379994 Color: 0
Size: 191519 Color: 0

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 499943 Color: 0
Size: 380134 Color: 1
Size: 119923 Color: 1

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 588375 Color: 0
Size: 311426 Color: 1
Size: 100199 Color: 0

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 695346 Color: 1
Size: 162503 Color: 0
Size: 142151 Color: 0

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 777047 Color: 1
Size: 114476 Color: 0
Size: 108477 Color: 0

Bin 413: 1 of cap free
Amount of items: 2
Items: 
Size: 545485 Color: 0
Size: 454515 Color: 1

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 490068 Color: 0
Size: 397132 Color: 1
Size: 112800 Color: 0

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 753857 Color: 1
Size: 125703 Color: 0
Size: 120440 Color: 1

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 577407 Color: 1
Size: 269160 Color: 0
Size: 153433 Color: 1

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 653611 Color: 0
Size: 176108 Color: 1
Size: 170281 Color: 1

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 775605 Color: 0
Size: 124333 Color: 1
Size: 100062 Color: 1

Bin 419: 1 of cap free
Amount of items: 2
Items: 
Size: 594692 Color: 1
Size: 405308 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 502450 Color: 0
Size: 381642 Color: 1
Size: 115908 Color: 1

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 760529 Color: 0
Size: 138459 Color: 1
Size: 101012 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 792404 Color: 0
Size: 106911 Color: 1
Size: 100685 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 549345 Color: 1
Size: 317186 Color: 1
Size: 133469 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 652697 Color: 1
Size: 193612 Color: 0
Size: 153691 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 568953 Color: 0
Size: 321356 Color: 1
Size: 109691 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 557838 Color: 1
Size: 340029 Color: 0
Size: 102133 Color: 1

Bin 427: 1 of cap free
Amount of items: 7
Items: 
Size: 149829 Color: 1
Size: 149089 Color: 1
Size: 148926 Color: 1
Size: 145984 Color: 0
Size: 145925 Color: 0
Size: 143583 Color: 1
Size: 116664 Color: 0

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 780728 Color: 0
Size: 112074 Color: 1
Size: 107198 Color: 0

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 740010 Color: 1
Size: 157356 Color: 0
Size: 102634 Color: 0

Bin 430: 1 of cap free
Amount of items: 5
Items: 
Size: 243073 Color: 1
Size: 242905 Color: 1
Size: 209460 Color: 0
Size: 154380 Color: 1
Size: 150182 Color: 0

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 775133 Color: 0
Size: 116231 Color: 1
Size: 108636 Color: 0

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 450935 Color: 1
Size: 442838 Color: 0
Size: 106227 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 451973 Color: 1
Size: 396974 Color: 0
Size: 151053 Color: 0

Bin 434: 1 of cap free
Amount of items: 7
Items: 
Size: 144569 Color: 1
Size: 144569 Color: 1
Size: 143760 Color: 1
Size: 142103 Color: 0
Size: 142003 Color: 0
Size: 141774 Color: 0
Size: 141222 Color: 1

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 692104 Color: 1
Size: 205028 Color: 0
Size: 102868 Color: 1

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 790942 Color: 1
Size: 105169 Color: 1
Size: 103889 Color: 0

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 602971 Color: 0
Size: 262324 Color: 1
Size: 134705 Color: 1

Bin 438: 1 of cap free
Amount of items: 6
Items: 
Size: 286220 Color: 1
Size: 154209 Color: 1
Size: 150684 Color: 1
Size: 147135 Color: 0
Size: 146308 Color: 0
Size: 115444 Color: 0

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 734574 Color: 1
Size: 157898 Color: 0
Size: 107528 Color: 1

Bin 440: 1 of cap free
Amount of items: 6
Items: 
Size: 173502 Color: 0
Size: 173240 Color: 0
Size: 169727 Color: 1
Size: 169052 Color: 1
Size: 169042 Color: 1
Size: 145437 Color: 0

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 788921 Color: 0
Size: 109966 Color: 0
Size: 101113 Color: 1

Bin 442: 1 of cap free
Amount of items: 5
Items: 
Size: 272325 Color: 1
Size: 183264 Color: 0
Size: 182963 Color: 0
Size: 180765 Color: 1
Size: 180683 Color: 0

Bin 443: 1 of cap free
Amount of items: 5
Items: 
Size: 204656 Color: 0
Size: 204582 Color: 0
Size: 198744 Color: 1
Size: 197102 Color: 0
Size: 194916 Color: 1

Bin 444: 1 of cap free
Amount of items: 5
Items: 
Size: 223200 Color: 1
Size: 223184 Color: 1
Size: 209020 Color: 0
Size: 195411 Color: 1
Size: 149185 Color: 0

Bin 445: 1 of cap free
Amount of items: 5
Items: 
Size: 236255 Color: 1
Size: 234586 Color: 1
Size: 209447 Color: 0
Size: 170350 Color: 1
Size: 149362 Color: 0

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 435271 Color: 1
Size: 431811 Color: 0
Size: 132918 Color: 0

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 435495 Color: 1
Size: 407475 Color: 0
Size: 157030 Color: 1

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 476548 Color: 0
Size: 373609 Color: 0
Size: 149843 Color: 1

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 476572 Color: 0
Size: 377731 Color: 0
Size: 145697 Color: 1

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 481662 Color: 0
Size: 379338 Color: 0
Size: 139000 Color: 1

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 481806 Color: 1
Size: 389865 Color: 0
Size: 128329 Color: 1

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 486130 Color: 0
Size: 364224 Color: 0
Size: 149646 Color: 1

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 497209 Color: 0
Size: 367631 Color: 0
Size: 135160 Color: 1

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 498910 Color: 0
Size: 367631 Color: 0
Size: 133459 Color: 1

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 499992 Color: 1
Size: 392022 Color: 0
Size: 107986 Color: 1

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 500211 Color: 1
Size: 363530 Color: 0
Size: 136259 Color: 1

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 500541 Color: 0
Size: 298698 Color: 1
Size: 200761 Color: 0

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 501514 Color: 0
Size: 381245 Color: 1
Size: 117241 Color: 0

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 502587 Color: 0
Size: 380246 Color: 1
Size: 117167 Color: 0

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 502756 Color: 0
Size: 376849 Color: 1
Size: 120395 Color: 0

Bin 461: 1 of cap free
Amount of items: 2
Items: 
Size: 505852 Color: 1
Size: 494148 Color: 0

Bin 462: 1 of cap free
Amount of items: 2
Items: 
Size: 508699 Color: 0
Size: 491301 Color: 1

Bin 463: 1 of cap free
Amount of items: 2
Items: 
Size: 511155 Color: 0
Size: 488845 Color: 1

Bin 464: 1 of cap free
Amount of items: 2
Items: 
Size: 511359 Color: 0
Size: 488641 Color: 1

Bin 465: 1 of cap free
Amount of items: 2
Items: 
Size: 512999 Color: 1
Size: 487001 Color: 0

Bin 466: 1 of cap free
Amount of items: 2
Items: 
Size: 531955 Color: 0
Size: 468045 Color: 1

Bin 467: 1 of cap free
Amount of items: 2
Items: 
Size: 549070 Color: 1
Size: 450930 Color: 0

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 549313 Color: 1
Size: 280271 Color: 1
Size: 170416 Color: 0

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 549269 Color: 0
Size: 326875 Color: 1
Size: 123856 Color: 0

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 556242 Color: 1
Size: 301158 Color: 1
Size: 142600 Color: 0

Bin 471: 1 of cap free
Amount of items: 2
Items: 
Size: 559577 Color: 0
Size: 440423 Color: 1

Bin 472: 1 of cap free
Amount of items: 2
Items: 
Size: 569112 Color: 0
Size: 430888 Color: 1

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 574812 Color: 0
Size: 217716 Color: 1
Size: 207472 Color: 0

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 580088 Color: 0
Size: 268602 Color: 1
Size: 151310 Color: 0

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 580250 Color: 0
Size: 297366 Color: 1
Size: 122384 Color: 0

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 580801 Color: 0
Size: 241033 Color: 0
Size: 178166 Color: 1

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 587970 Color: 1
Size: 241111 Color: 0
Size: 170919 Color: 1

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 588080 Color: 1
Size: 220673 Color: 1
Size: 191247 Color: 0

Bin 479: 1 of cap free
Amount of items: 3
Items: 
Size: 588305 Color: 0
Size: 279629 Color: 0
Size: 132066 Color: 1

Bin 480: 1 of cap free
Amount of items: 3
Items: 
Size: 588286 Color: 1
Size: 268259 Color: 0
Size: 143455 Color: 1

Bin 481: 1 of cap free
Amount of items: 3
Items: 
Size: 588587 Color: 0
Size: 240893 Color: 0
Size: 170520 Color: 1

Bin 482: 1 of cap free
Amount of items: 3
Items: 
Size: 590140 Color: 1
Size: 289965 Color: 0
Size: 119895 Color: 1

Bin 483: 1 of cap free
Amount of items: 2
Items: 
Size: 591843 Color: 1
Size: 408157 Color: 0

Bin 484: 1 of cap free
Amount of items: 3
Items: 
Size: 593461 Color: 0
Size: 207463 Color: 0
Size: 199076 Color: 1

Bin 485: 1 of cap free
Amount of items: 3
Items: 
Size: 593618 Color: 0
Size: 280144 Color: 0
Size: 126238 Color: 1

Bin 486: 1 of cap free
Amount of items: 2
Items: 
Size: 596366 Color: 0
Size: 403634 Color: 1

Bin 487: 1 of cap free
Amount of items: 3
Items: 
Size: 596403 Color: 0
Size: 205083 Color: 0
Size: 198514 Color: 1

Bin 488: 1 of cap free
Amount of items: 3
Items: 
Size: 599368 Color: 1
Size: 248305 Color: 1
Size: 152327 Color: 0

Bin 489: 1 of cap free
Amount of items: 3
Items: 
Size: 603179 Color: 0
Size: 268990 Color: 0
Size: 127831 Color: 1

Bin 490: 1 of cap free
Amount of items: 3
Items: 
Size: 603269 Color: 0
Size: 287439 Color: 0
Size: 109292 Color: 1

Bin 491: 1 of cap free
Amount of items: 3
Items: 
Size: 612133 Color: 0
Size: 233739 Color: 0
Size: 154128 Color: 1

Bin 492: 1 of cap free
Amount of items: 2
Items: 
Size: 612908 Color: 1
Size: 387092 Color: 0

Bin 493: 1 of cap free
Amount of items: 2
Items: 
Size: 614154 Color: 1
Size: 385846 Color: 0

Bin 494: 1 of cap free
Amount of items: 2
Items: 
Size: 626436 Color: 1
Size: 373564 Color: 0

Bin 495: 1 of cap free
Amount of items: 3
Items: 
Size: 628405 Color: 0
Size: 191773 Color: 1
Size: 179822 Color: 1

Bin 496: 1 of cap free
Amount of items: 3
Items: 
Size: 628595 Color: 0
Size: 189820 Color: 1
Size: 181585 Color: 1

Bin 497: 1 of cap free
Amount of items: 3
Items: 
Size: 629077 Color: 0
Size: 189914 Color: 1
Size: 181009 Color: 0

Bin 498: 1 of cap free
Amount of items: 3
Items: 
Size: 629200 Color: 0
Size: 198337 Color: 0
Size: 172463 Color: 1

Bin 499: 1 of cap free
Amount of items: 3
Items: 
Size: 629077 Color: 1
Size: 261163 Color: 1
Size: 109760 Color: 0

Bin 500: 1 of cap free
Amount of items: 3
Items: 
Size: 635331 Color: 1
Size: 203400 Color: 0
Size: 161269 Color: 1

Bin 501: 1 of cap free
Amount of items: 3
Items: 
Size: 636806 Color: 1
Size: 183390 Color: 0
Size: 179804 Color: 1

Bin 502: 1 of cap free
Amount of items: 3
Items: 
Size: 636975 Color: 1
Size: 203463 Color: 0
Size: 159562 Color: 0

Bin 503: 1 of cap free
Amount of items: 3
Items: 
Size: 637791 Color: 1
Size: 182520 Color: 1
Size: 179689 Color: 0

Bin 504: 1 of cap free
Amount of items: 3
Items: 
Size: 639966 Color: 0
Size: 193038 Color: 1
Size: 166996 Color: 0

Bin 505: 1 of cap free
Amount of items: 3
Items: 
Size: 640221 Color: 0
Size: 198626 Color: 1
Size: 161153 Color: 0

Bin 506: 1 of cap free
Amount of items: 3
Items: 
Size: 640436 Color: 0
Size: 219656 Color: 1
Size: 139908 Color: 1

Bin 507: 1 of cap free
Amount of items: 2
Items: 
Size: 641656 Color: 0
Size: 358344 Color: 1

Bin 508: 1 of cap free
Amount of items: 3
Items: 
Size: 645929 Color: 1
Size: 190706 Color: 0
Size: 163365 Color: 1

Bin 509: 1 of cap free
Amount of items: 3
Items: 
Size: 648681 Color: 0
Size: 191099 Color: 1
Size: 160220 Color: 1

Bin 510: 1 of cap free
Amount of items: 3
Items: 
Size: 648996 Color: 0
Size: 229851 Color: 0
Size: 121153 Color: 1

Bin 511: 1 of cap free
Amount of items: 3
Items: 
Size: 650286 Color: 1
Size: 181928 Color: 0
Size: 167786 Color: 0

Bin 512: 1 of cap free
Amount of items: 3
Items: 
Size: 650608 Color: 1
Size: 192507 Color: 1
Size: 156885 Color: 0

Bin 513: 1 of cap free
Amount of items: 3
Items: 
Size: 653795 Color: 0
Size: 196308 Color: 0
Size: 149897 Color: 1

Bin 514: 1 of cap free
Amount of items: 3
Items: 
Size: 654021 Color: 0
Size: 179472 Color: 0
Size: 166507 Color: 1

Bin 515: 1 of cap free
Amount of items: 3
Items: 
Size: 656054 Color: 1
Size: 186263 Color: 0
Size: 157683 Color: 0

Bin 516: 1 of cap free
Amount of items: 2
Items: 
Size: 660507 Color: 0
Size: 339493 Color: 1

Bin 517: 1 of cap free
Amount of items: 3
Items: 
Size: 664690 Color: 1
Size: 179027 Color: 0
Size: 156283 Color: 0

Bin 518: 1 of cap free
Amount of items: 2
Items: 
Size: 665105 Color: 0
Size: 334895 Color: 1

Bin 519: 1 of cap free
Amount of items: 3
Items: 
Size: 670697 Color: 1
Size: 165685 Color: 0
Size: 163618 Color: 0

Bin 520: 1 of cap free
Amount of items: 3
Items: 
Size: 673036 Color: 1
Size: 166245 Color: 0
Size: 160719 Color: 0

Bin 521: 1 of cap free
Amount of items: 3
Items: 
Size: 676062 Color: 1
Size: 191018 Color: 0
Size: 132920 Color: 0

Bin 522: 1 of cap free
Amount of items: 3
Items: 
Size: 677217 Color: 1
Size: 170211 Color: 0
Size: 152572 Color: 0

Bin 523: 1 of cap free
Amount of items: 3
Items: 
Size: 680146 Color: 0
Size: 173953 Color: 1
Size: 145901 Color: 0

Bin 524: 1 of cap free
Amount of items: 2
Items: 
Size: 680852 Color: 1
Size: 319148 Color: 0

Bin 525: 1 of cap free
Amount of items: 3
Items: 
Size: 682756 Color: 1
Size: 159774 Color: 1
Size: 157470 Color: 0

Bin 526: 1 of cap free
Amount of items: 3
Items: 
Size: 690781 Color: 1
Size: 161588 Color: 0
Size: 147631 Color: 1

Bin 527: 1 of cap free
Amount of items: 3
Items: 
Size: 691704 Color: 1
Size: 164609 Color: 1
Size: 143687 Color: 0

Bin 528: 1 of cap free
Amount of items: 2
Items: 
Size: 700664 Color: 0
Size: 299336 Color: 1

Bin 529: 1 of cap free
Amount of items: 3
Items: 
Size: 704641 Color: 1
Size: 172732 Color: 0
Size: 122627 Color: 0

Bin 530: 1 of cap free
Amount of items: 3
Items: 
Size: 708225 Color: 1
Size: 187423 Color: 1
Size: 104352 Color: 0

Bin 531: 1 of cap free
Amount of items: 3
Items: 
Size: 708227 Color: 0
Size: 152623 Color: 1
Size: 139150 Color: 0

Bin 532: 1 of cap free
Amount of items: 3
Items: 
Size: 709850 Color: 0
Size: 181050 Color: 0
Size: 109100 Color: 1

Bin 533: 1 of cap free
Amount of items: 3
Items: 
Size: 710070 Color: 1
Size: 163198 Color: 1
Size: 126732 Color: 0

Bin 534: 1 of cap free
Amount of items: 3
Items: 
Size: 712588 Color: 0
Size: 144111 Color: 1
Size: 143301 Color: 0

Bin 535: 1 of cap free
Amount of items: 2
Items: 
Size: 715512 Color: 0
Size: 284488 Color: 1

Bin 536: 1 of cap free
Amount of items: 3
Items: 
Size: 721248 Color: 1
Size: 166510 Color: 0
Size: 112242 Color: 0

Bin 537: 1 of cap free
Amount of items: 2
Items: 
Size: 722237 Color: 0
Size: 277763 Color: 1

Bin 538: 1 of cap free
Amount of items: 2
Items: 
Size: 731459 Color: 1
Size: 268541 Color: 0

Bin 539: 1 of cap free
Amount of items: 3
Items: 
Size: 732307 Color: 0
Size: 155006 Color: 0
Size: 112687 Color: 1

Bin 540: 1 of cap free
Amount of items: 3
Items: 
Size: 734803 Color: 1
Size: 157657 Color: 1
Size: 107540 Color: 0

Bin 541: 1 of cap free
Amount of items: 3
Items: 
Size: 735249 Color: 1
Size: 149206 Color: 0
Size: 115545 Color: 0

Bin 542: 1 of cap free
Amount of items: 3
Items: 
Size: 735361 Color: 1
Size: 157140 Color: 1
Size: 107499 Color: 0

Bin 543: 1 of cap free
Amount of items: 2
Items: 
Size: 740851 Color: 1
Size: 259149 Color: 0

Bin 544: 1 of cap free
Amount of items: 3
Items: 
Size: 743031 Color: 0
Size: 137691 Color: 1
Size: 119278 Color: 0

Bin 545: 1 of cap free
Amount of items: 3
Items: 
Size: 747609 Color: 1
Size: 142194 Color: 1
Size: 110197 Color: 0

Bin 546: 1 of cap free
Amount of items: 2
Items: 
Size: 748284 Color: 0
Size: 251716 Color: 1

Bin 547: 1 of cap free
Amount of items: 3
Items: 
Size: 748551 Color: 0
Size: 135180 Color: 0
Size: 116269 Color: 1

Bin 548: 1 of cap free
Amount of items: 2
Items: 
Size: 750161 Color: 1
Size: 249839 Color: 0

Bin 549: 1 of cap free
Amount of items: 3
Items: 
Size: 754741 Color: 1
Size: 135562 Color: 0
Size: 109697 Color: 0

Bin 550: 1 of cap free
Amount of items: 3
Items: 
Size: 757542 Color: 0
Size: 125413 Color: 1
Size: 117045 Color: 0

Bin 551: 1 of cap free
Amount of items: 3
Items: 
Size: 757574 Color: 0
Size: 128093 Color: 0
Size: 114333 Color: 1

Bin 552: 1 of cap free
Amount of items: 3
Items: 
Size: 761822 Color: 1
Size: 128212 Color: 0
Size: 109966 Color: 1

Bin 553: 1 of cap free
Amount of items: 3
Items: 
Size: 761716 Color: 0
Size: 126810 Color: 0
Size: 111474 Color: 1

Bin 554: 1 of cap free
Amount of items: 3
Items: 
Size: 761879 Color: 1
Size: 125877 Color: 1
Size: 112244 Color: 0

Bin 555: 1 of cap free
Amount of items: 2
Items: 
Size: 764654 Color: 1
Size: 235346 Color: 0

Bin 556: 1 of cap free
Amount of items: 2
Items: 
Size: 770509 Color: 0
Size: 229491 Color: 1

Bin 557: 1 of cap free
Amount of items: 2
Items: 
Size: 771332 Color: 0
Size: 228668 Color: 1

Bin 558: 1 of cap free
Amount of items: 3
Items: 
Size: 774036 Color: 1
Size: 118405 Color: 0
Size: 107559 Color: 1

Bin 559: 1 of cap free
Amount of items: 2
Items: 
Size: 777681 Color: 0
Size: 222319 Color: 1

Bin 560: 1 of cap free
Amount of items: 2
Items: 
Size: 777858 Color: 1
Size: 222142 Color: 0

Bin 561: 1 of cap free
Amount of items: 3
Items: 
Size: 779116 Color: 0
Size: 112463 Color: 0
Size: 108421 Color: 1

Bin 562: 1 of cap free
Amount of items: 3
Items: 
Size: 779304 Color: 1
Size: 115099 Color: 1
Size: 105597 Color: 0

Bin 563: 2 of cap free
Amount of items: 3
Items: 
Size: 792597 Color: 0
Size: 103883 Color: 1
Size: 103519 Color: 0

Bin 564: 2 of cap free
Amount of items: 3
Items: 
Size: 497758 Color: 0
Size: 382104 Color: 1
Size: 120137 Color: 1

Bin 565: 2 of cap free
Amount of items: 3
Items: 
Size: 742938 Color: 1
Size: 149187 Color: 0
Size: 107874 Color: 1

Bin 566: 2 of cap free
Amount of items: 3
Items: 
Size: 739584 Color: 1
Size: 130578 Color: 0
Size: 129837 Color: 0

Bin 567: 2 of cap free
Amount of items: 3
Items: 
Size: 526532 Color: 0
Size: 357073 Color: 0
Size: 116394 Color: 1

Bin 568: 2 of cap free
Amount of items: 3
Items: 
Size: 792544 Color: 1
Size: 105341 Color: 0
Size: 102114 Color: 1

Bin 569: 2 of cap free
Amount of items: 3
Items: 
Size: 726672 Color: 0
Size: 151494 Color: 1
Size: 121833 Color: 0

Bin 570: 2 of cap free
Amount of items: 3
Items: 
Size: 702438 Color: 1
Size: 185166 Color: 0
Size: 112395 Color: 1

Bin 571: 2 of cap free
Amount of items: 3
Items: 
Size: 688845 Color: 1
Size: 186174 Color: 0
Size: 124980 Color: 0

Bin 572: 2 of cap free
Amount of items: 3
Items: 
Size: 462347 Color: 0
Size: 432639 Color: 1
Size: 105013 Color: 0

Bin 573: 2 of cap free
Amount of items: 3
Items: 
Size: 733807 Color: 0
Size: 141826 Color: 1
Size: 124366 Color: 1

Bin 574: 2 of cap free
Amount of items: 3
Items: 
Size: 599486 Color: 1
Size: 268872 Color: 0
Size: 131641 Color: 0

Bin 575: 2 of cap free
Amount of items: 3
Items: 
Size: 497924 Color: 0
Size: 373668 Color: 0
Size: 128407 Color: 1

Bin 576: 2 of cap free
Amount of items: 6
Items: 
Size: 258703 Color: 1
Size: 151821 Color: 1
Size: 150881 Color: 1
Size: 149524 Color: 0
Size: 148878 Color: 0
Size: 140192 Color: 0

Bin 577: 2 of cap free
Amount of items: 7
Items: 
Size: 158092 Color: 1
Size: 157076 Color: 0
Size: 156858 Color: 0
Size: 155535 Color: 1
Size: 137552 Color: 1
Size: 119167 Color: 0
Size: 115719 Color: 0

Bin 578: 2 of cap free
Amount of items: 3
Items: 
Size: 686613 Color: 0
Size: 173491 Color: 1
Size: 139895 Color: 1

Bin 579: 2 of cap free
Amount of items: 5
Items: 
Size: 264596 Color: 1
Size: 184849 Color: 0
Size: 184686 Color: 0
Size: 183718 Color: 0
Size: 182150 Color: 1

Bin 580: 2 of cap free
Amount of items: 5
Items: 
Size: 217628 Color: 1
Size: 205919 Color: 0
Size: 205592 Color: 0
Size: 191183 Color: 1
Size: 179677 Color: 0

Bin 581: 2 of cap free
Amount of items: 3
Items: 
Size: 458591 Color: 0
Size: 408609 Color: 0
Size: 132799 Color: 1

Bin 582: 2 of cap free
Amount of items: 3
Items: 
Size: 497597 Color: 0
Size: 389818 Color: 0
Size: 112584 Color: 1

Bin 583: 2 of cap free
Amount of items: 3
Items: 
Size: 499243 Color: 0
Size: 373479 Color: 0
Size: 127277 Color: 1

Bin 584: 2 of cap free
Amount of items: 3
Items: 
Size: 499970 Color: 1
Size: 340578 Color: 1
Size: 159451 Color: 0

Bin 585: 2 of cap free
Amount of items: 2
Items: 
Size: 500440 Color: 0
Size: 499559 Color: 1

Bin 586: 2 of cap free
Amount of items: 2
Items: 
Size: 504601 Color: 0
Size: 495398 Color: 1

Bin 587: 2 of cap free
Amount of items: 2
Items: 
Size: 516474 Color: 1
Size: 483525 Color: 0

Bin 588: 2 of cap free
Amount of items: 3
Items: 
Size: 524458 Color: 1
Size: 324960 Color: 1
Size: 150581 Color: 0

Bin 589: 2 of cap free
Amount of items: 3
Items: 
Size: 525356 Color: 0
Size: 317562 Color: 1
Size: 157081 Color: 0

Bin 590: 2 of cap free
Amount of items: 3
Items: 
Size: 526161 Color: 0
Size: 340096 Color: 1
Size: 133742 Color: 1

Bin 591: 2 of cap free
Amount of items: 3
Items: 
Size: 536385 Color: 0
Size: 320232 Color: 1
Size: 143382 Color: 0

Bin 592: 2 of cap free
Amount of items: 2
Items: 
Size: 540238 Color: 1
Size: 459761 Color: 0

Bin 593: 2 of cap free
Amount of items: 2
Items: 
Size: 543651 Color: 1
Size: 456348 Color: 0

Bin 594: 2 of cap free
Amount of items: 3
Items: 
Size: 543445 Color: 0
Size: 317845 Color: 1
Size: 138709 Color: 0

Bin 595: 2 of cap free
Amount of items: 2
Items: 
Size: 567644 Color: 0
Size: 432355 Color: 1

Bin 596: 2 of cap free
Amount of items: 3
Items: 
Size: 574911 Color: 0
Size: 267122 Color: 1
Size: 157966 Color: 0

Bin 597: 2 of cap free
Amount of items: 3
Items: 
Size: 575205 Color: 0
Size: 284534 Color: 1
Size: 140260 Color: 1

Bin 598: 2 of cap free
Amount of items: 3
Items: 
Size: 577385 Color: 1
Size: 318114 Color: 1
Size: 104500 Color: 0

Bin 599: 2 of cap free
Amount of items: 2
Items: 
Size: 577989 Color: 0
Size: 422010 Color: 1

Bin 600: 2 of cap free
Amount of items: 3
Items: 
Size: 580147 Color: 1
Size: 240797 Color: 0
Size: 179055 Color: 1

Bin 601: 2 of cap free
Amount of items: 2
Items: 
Size: 581092 Color: 1
Size: 418907 Color: 0

Bin 602: 2 of cap free
Amount of items: 3
Items: 
Size: 588386 Color: 1
Size: 267820 Color: 1
Size: 143793 Color: 0

Bin 603: 2 of cap free
Amount of items: 3
Items: 
Size: 591903 Color: 0
Size: 267798 Color: 1
Size: 140298 Color: 0

Bin 604: 2 of cap free
Amount of items: 3
Items: 
Size: 593549 Color: 0
Size: 210491 Color: 1
Size: 195959 Color: 1

Bin 605: 2 of cap free
Amount of items: 2
Items: 
Size: 599112 Color: 0
Size: 400887 Color: 1

Bin 606: 2 of cap free
Amount of items: 3
Items: 
Size: 603309 Color: 0
Size: 268777 Color: 0
Size: 127913 Color: 1

Bin 607: 2 of cap free
Amount of items: 2
Items: 
Size: 606793 Color: 1
Size: 393206 Color: 0

Bin 608: 2 of cap free
Amount of items: 3
Items: 
Size: 611999 Color: 1
Size: 203487 Color: 0
Size: 184513 Color: 0

Bin 609: 2 of cap free
Amount of items: 2
Items: 
Size: 612841 Color: 0
Size: 387158 Color: 1

Bin 610: 2 of cap free
Amount of items: 2
Items: 
Size: 620417 Color: 1
Size: 379582 Color: 0

Bin 611: 2 of cap free
Amount of items: 3
Items: 
Size: 628529 Color: 0
Size: 217629 Color: 1
Size: 153841 Color: 0

Bin 612: 2 of cap free
Amount of items: 3
Items: 
Size: 634326 Color: 1
Size: 188203 Color: 0
Size: 177470 Color: 0

Bin 613: 2 of cap free
Amount of items: 2
Items: 
Size: 645750 Color: 1
Size: 354249 Color: 0

Bin 614: 2 of cap free
Amount of items: 2
Items: 
Size: 647967 Color: 0
Size: 352032 Color: 1

Bin 615: 2 of cap free
Amount of items: 3
Items: 
Size: 653944 Color: 0
Size: 193763 Color: 1
Size: 152292 Color: 1

Bin 616: 2 of cap free
Amount of items: 2
Items: 
Size: 658816 Color: 1
Size: 341183 Color: 0

Bin 617: 2 of cap free
Amount of items: 2
Items: 
Size: 660054 Color: 1
Size: 339945 Color: 0

Bin 618: 2 of cap free
Amount of items: 2
Items: 
Size: 662812 Color: 1
Size: 337187 Color: 0

Bin 619: 2 of cap free
Amount of items: 2
Items: 
Size: 663738 Color: 1
Size: 336261 Color: 0

Bin 620: 2 of cap free
Amount of items: 2
Items: 
Size: 684690 Color: 1
Size: 315309 Color: 0

Bin 621: 2 of cap free
Amount of items: 2
Items: 
Size: 693400 Color: 1
Size: 306599 Color: 0

Bin 622: 2 of cap free
Amount of items: 2
Items: 
Size: 697064 Color: 1
Size: 302935 Color: 0

Bin 623: 2 of cap free
Amount of items: 2
Items: 
Size: 707736 Color: 1
Size: 292263 Color: 0

Bin 624: 2 of cap free
Amount of items: 2
Items: 
Size: 712098 Color: 0
Size: 287901 Color: 1

Bin 625: 2 of cap free
Amount of items: 2
Items: 
Size: 714825 Color: 0
Size: 285174 Color: 1

Bin 626: 2 of cap free
Amount of items: 2
Items: 
Size: 716242 Color: 0
Size: 283757 Color: 1

Bin 627: 2 of cap free
Amount of items: 2
Items: 
Size: 719628 Color: 1
Size: 280371 Color: 0

Bin 628: 2 of cap free
Amount of items: 2
Items: 
Size: 727543 Color: 1
Size: 272456 Color: 0

Bin 629: 2 of cap free
Amount of items: 3
Items: 
Size: 731874 Color: 0
Size: 141370 Color: 0
Size: 126755 Color: 1

Bin 630: 2 of cap free
Amount of items: 2
Items: 
Size: 732073 Color: 1
Size: 267926 Color: 0

Bin 631: 2 of cap free
Amount of items: 2
Items: 
Size: 734418 Color: 1
Size: 265581 Color: 0

Bin 632: 2 of cap free
Amount of items: 3
Items: 
Size: 745809 Color: 0
Size: 132512 Color: 1
Size: 121678 Color: 0

Bin 633: 2 of cap free
Amount of items: 3
Items: 
Size: 760652 Color: 1
Size: 125212 Color: 0
Size: 114135 Color: 0

Bin 634: 2 of cap free
Amount of items: 3
Items: 
Size: 761633 Color: 0
Size: 126948 Color: 0
Size: 111418 Color: 1

Bin 635: 2 of cap free
Amount of items: 2
Items: 
Size: 762029 Color: 1
Size: 237970 Color: 0

Bin 636: 2 of cap free
Amount of items: 2
Items: 
Size: 763438 Color: 1
Size: 236561 Color: 0

Bin 637: 2 of cap free
Amount of items: 2
Items: 
Size: 764634 Color: 0
Size: 235365 Color: 1

Bin 638: 2 of cap free
Amount of items: 2
Items: 
Size: 787263 Color: 1
Size: 212736 Color: 0

Bin 639: 2 of cap free
Amount of items: 2
Items: 
Size: 791042 Color: 1
Size: 208957 Color: 0

Bin 640: 2 of cap free
Amount of items: 2
Items: 
Size: 799695 Color: 0
Size: 200304 Color: 1

Bin 641: 3 of cap free
Amount of items: 3
Items: 
Size: 556704 Color: 1
Size: 232165 Color: 0
Size: 211129 Color: 1

Bin 642: 3 of cap free
Amount of items: 3
Items: 
Size: 435044 Color: 1
Size: 429477 Color: 0
Size: 135477 Color: 0

Bin 643: 3 of cap free
Amount of items: 3
Items: 
Size: 766866 Color: 1
Size: 119349 Color: 0
Size: 113783 Color: 0

Bin 644: 3 of cap free
Amount of items: 3
Items: 
Size: 735163 Color: 1
Size: 147302 Color: 0
Size: 117533 Color: 0

Bin 645: 3 of cap free
Amount of items: 3
Items: 
Size: 738109 Color: 1
Size: 136050 Color: 1
Size: 125839 Color: 0

Bin 646: 3 of cap free
Amount of items: 3
Items: 
Size: 571639 Color: 1
Size: 280730 Color: 0
Size: 147629 Color: 1

Bin 647: 3 of cap free
Amount of items: 7
Items: 
Size: 271457 Color: 1
Size: 134258 Color: 0
Size: 129195 Color: 1
Size: 128286 Color: 1
Size: 113529 Color: 0
Size: 111832 Color: 0
Size: 111441 Color: 1

Bin 648: 3 of cap free
Amount of items: 3
Items: 
Size: 500606 Color: 0
Size: 371901 Color: 1
Size: 127491 Color: 1

Bin 649: 3 of cap free
Amount of items: 3
Items: 
Size: 501813 Color: 1
Size: 363684 Color: 0
Size: 134501 Color: 1

Bin 650: 3 of cap free
Amount of items: 2
Items: 
Size: 779127 Color: 1
Size: 220871 Color: 0

Bin 651: 3 of cap free
Amount of items: 5
Items: 
Size: 246904 Color: 0
Size: 200137 Color: 0
Size: 197785 Color: 1
Size: 197782 Color: 1
Size: 157390 Color: 1

Bin 652: 3 of cap free
Amount of items: 3
Items: 
Size: 590080 Color: 0
Size: 216442 Color: 0
Size: 193476 Color: 1

Bin 653: 3 of cap free
Amount of items: 3
Items: 
Size: 593707 Color: 0
Size: 220547 Color: 1
Size: 185744 Color: 1

Bin 654: 3 of cap free
Amount of items: 6
Items: 
Size: 173246 Color: 0
Size: 171284 Color: 1
Size: 170860 Color: 0
Size: 169054 Color: 1
Size: 169046 Color: 1
Size: 146508 Color: 0

Bin 655: 3 of cap free
Amount of items: 3
Items: 
Size: 749364 Color: 0
Size: 130403 Color: 0
Size: 120231 Color: 1

Bin 656: 3 of cap free
Amount of items: 5
Items: 
Size: 219311 Color: 1
Size: 219173 Color: 1
Size: 207403 Color: 0
Size: 183606 Color: 0
Size: 170505 Color: 1

Bin 657: 3 of cap free
Amount of items: 3
Items: 
Size: 429460 Color: 0
Size: 392260 Color: 0
Size: 178278 Color: 1

Bin 658: 3 of cap free
Amount of items: 3
Items: 
Size: 458576 Color: 0
Size: 408628 Color: 0
Size: 132794 Color: 1

Bin 659: 3 of cap free
Amount of items: 3
Items: 
Size: 478937 Color: 0
Size: 373088 Color: 0
Size: 147973 Color: 1

Bin 660: 3 of cap free
Amount of items: 3
Items: 
Size: 481094 Color: 0
Size: 387541 Color: 0
Size: 131363 Color: 1

Bin 661: 3 of cap free
Amount of items: 3
Items: 
Size: 481165 Color: 1
Size: 324114 Color: 1
Size: 194719 Color: 0

Bin 662: 3 of cap free
Amount of items: 3
Items: 
Size: 486604 Color: 0
Size: 408039 Color: 0
Size: 105355 Color: 1

Bin 663: 3 of cap free
Amount of items: 3
Items: 
Size: 497164 Color: 0
Size: 387852 Color: 0
Size: 114982 Color: 1

Bin 664: 3 of cap free
Amount of items: 3
Items: 
Size: 499220 Color: 0
Size: 321210 Color: 1
Size: 179568 Color: 1

Bin 665: 3 of cap free
Amount of items: 3
Items: 
Size: 499994 Color: 0
Size: 324513 Color: 1
Size: 175491 Color: 1

Bin 666: 3 of cap free
Amount of items: 3
Items: 
Size: 504217 Color: 0
Size: 364936 Color: 1
Size: 130845 Color: 0

Bin 667: 3 of cap free
Amount of items: 2
Items: 
Size: 513716 Color: 1
Size: 486282 Color: 0

Bin 668: 3 of cap free
Amount of items: 2
Items: 
Size: 514338 Color: 0
Size: 485660 Color: 1

Bin 669: 3 of cap free
Amount of items: 2
Items: 
Size: 524060 Color: 1
Size: 475938 Color: 0

Bin 670: 3 of cap free
Amount of items: 3
Items: 
Size: 524893 Color: 1
Size: 331962 Color: 1
Size: 143143 Color: 0

Bin 671: 3 of cap free
Amount of items: 3
Items: 
Size: 524733 Color: 0
Size: 356682 Color: 0
Size: 118583 Color: 1

Bin 672: 3 of cap free
Amount of items: 2
Items: 
Size: 532235 Color: 1
Size: 467763 Color: 0

Bin 673: 3 of cap free
Amount of items: 2
Items: 
Size: 549573 Color: 1
Size: 450425 Color: 0

Bin 674: 3 of cap free
Amount of items: 3
Items: 
Size: 556285 Color: 0
Size: 280314 Color: 1
Size: 163399 Color: 0

Bin 675: 3 of cap free
Amount of items: 3
Items: 
Size: 556570 Color: 1
Size: 321111 Color: 1
Size: 122317 Color: 0

Bin 676: 3 of cap free
Amount of items: 2
Items: 
Size: 557857 Color: 1
Size: 442141 Color: 0

Bin 677: 3 of cap free
Amount of items: 2
Items: 
Size: 558031 Color: 1
Size: 441967 Color: 0

Bin 678: 3 of cap free
Amount of items: 2
Items: 
Size: 566015 Color: 0
Size: 433983 Color: 1

Bin 679: 3 of cap free
Amount of items: 2
Items: 
Size: 585793 Color: 0
Size: 414205 Color: 1

Bin 680: 3 of cap free
Amount of items: 3
Items: 
Size: 602953 Color: 1
Size: 219643 Color: 1
Size: 177402 Color: 0

Bin 681: 3 of cap free
Amount of items: 2
Items: 
Size: 607513 Color: 0
Size: 392485 Color: 1

Bin 682: 3 of cap free
Amount of items: 3
Items: 
Size: 611918 Color: 0
Size: 220471 Color: 1
Size: 167609 Color: 0

Bin 683: 3 of cap free
Amount of items: 3
Items: 
Size: 614088 Color: 1
Size: 195710 Color: 0
Size: 190200 Color: 0

Bin 684: 3 of cap free
Amount of items: 2
Items: 
Size: 615547 Color: 1
Size: 384451 Color: 0

Bin 685: 3 of cap free
Amount of items: 3
Items: 
Size: 616263 Color: 1
Size: 203950 Color: 0
Size: 179785 Color: 0

Bin 686: 3 of cap free
Amount of items: 3
Items: 
Size: 627186 Color: 1
Size: 196243 Color: 0
Size: 176569 Color: 0

Bin 687: 3 of cap free
Amount of items: 3
Items: 
Size: 636778 Color: 0
Size: 194608 Color: 1
Size: 168612 Color: 0

Bin 688: 3 of cap free
Amount of items: 2
Items: 
Size: 638543 Color: 1
Size: 361455 Color: 0

Bin 689: 3 of cap free
Amount of items: 3
Items: 
Size: 639257 Color: 1
Size: 199440 Color: 0
Size: 161301 Color: 0

Bin 690: 3 of cap free
Amount of items: 2
Items: 
Size: 639587 Color: 0
Size: 360411 Color: 1

Bin 691: 3 of cap free
Amount of items: 3
Items: 
Size: 640236 Color: 0
Size: 193783 Color: 1
Size: 165979 Color: 1

Bin 692: 3 of cap free
Amount of items: 3
Items: 
Size: 641446 Color: 1
Size: 192744 Color: 0
Size: 165808 Color: 0

Bin 693: 3 of cap free
Amount of items: 2
Items: 
Size: 647761 Color: 0
Size: 352237 Color: 1

Bin 694: 3 of cap free
Amount of items: 2
Items: 
Size: 651852 Color: 1
Size: 348146 Color: 0

Bin 695: 3 of cap free
Amount of items: 2
Items: 
Size: 664220 Color: 1
Size: 335778 Color: 0

Bin 696: 3 of cap free
Amount of items: 2
Items: 
Size: 668591 Color: 1
Size: 331407 Color: 0

Bin 697: 3 of cap free
Amount of items: 2
Items: 
Size: 669643 Color: 0
Size: 330355 Color: 1

Bin 698: 3 of cap free
Amount of items: 2
Items: 
Size: 670080 Color: 1
Size: 329918 Color: 0

Bin 699: 3 of cap free
Amount of items: 3
Items: 
Size: 672144 Color: 1
Size: 180815 Color: 0
Size: 147039 Color: 0

Bin 700: 3 of cap free
Amount of items: 2
Items: 
Size: 681380 Color: 0
Size: 318618 Color: 1

Bin 701: 3 of cap free
Amount of items: 2
Items: 
Size: 683875 Color: 1
Size: 316123 Color: 0

Bin 702: 3 of cap free
Amount of items: 2
Items: 
Size: 686714 Color: 0
Size: 313284 Color: 1

Bin 703: 3 of cap free
Amount of items: 2
Items: 
Size: 692815 Color: 1
Size: 307183 Color: 0

Bin 704: 3 of cap free
Amount of items: 2
Items: 
Size: 693792 Color: 1
Size: 306206 Color: 0

Bin 705: 3 of cap free
Amount of items: 2
Items: 
Size: 702110 Color: 0
Size: 297888 Color: 1

Bin 706: 3 of cap free
Amount of items: 2
Items: 
Size: 726645 Color: 0
Size: 273353 Color: 1

Bin 707: 3 of cap free
Amount of items: 2
Items: 
Size: 735366 Color: 0
Size: 264632 Color: 1

Bin 708: 3 of cap free
Amount of items: 2
Items: 
Size: 745872 Color: 0
Size: 254126 Color: 1

Bin 709: 3 of cap free
Amount of items: 3
Items: 
Size: 751798 Color: 0
Size: 132607 Color: 0
Size: 115593 Color: 1

Bin 710: 3 of cap free
Amount of items: 2
Items: 
Size: 759838 Color: 1
Size: 240160 Color: 0

Bin 711: 3 of cap free
Amount of items: 3
Items: 
Size: 771356 Color: 1
Size: 123123 Color: 1
Size: 105519 Color: 0

Bin 712: 3 of cap free
Amount of items: 2
Items: 
Size: 783567 Color: 0
Size: 216431 Color: 1

Bin 713: 3 of cap free
Amount of items: 2
Items: 
Size: 793851 Color: 1
Size: 206147 Color: 0

Bin 714: 4 of cap free
Amount of items: 5
Items: 
Size: 227838 Color: 0
Size: 194690 Color: 0
Size: 192631 Color: 1
Size: 192544 Color: 1
Size: 192294 Color: 1

Bin 715: 4 of cap free
Amount of items: 5
Items: 
Size: 250626 Color: 1
Size: 188671 Color: 0
Size: 188586 Color: 0
Size: 186192 Color: 1
Size: 185922 Color: 1

Bin 716: 4 of cap free
Amount of items: 3
Items: 
Size: 444065 Color: 0
Size: 429223 Color: 1
Size: 126709 Color: 0

Bin 717: 4 of cap free
Amount of items: 3
Items: 
Size: 584415 Color: 0
Size: 280247 Color: 1
Size: 135335 Color: 0

Bin 718: 4 of cap free
Amount of items: 3
Items: 
Size: 462870 Color: 0
Size: 387722 Color: 0
Size: 149405 Color: 1

Bin 719: 4 of cap free
Amount of items: 3
Items: 
Size: 499380 Color: 0
Size: 380376 Color: 1
Size: 120241 Color: 1

Bin 720: 4 of cap free
Amount of items: 3
Items: 
Size: 499925 Color: 0
Size: 364309 Color: 0
Size: 135763 Color: 1

Bin 721: 4 of cap free
Amount of items: 3
Items: 
Size: 524737 Color: 0
Size: 306878 Color: 1
Size: 168382 Color: 0

Bin 722: 4 of cap free
Amount of items: 3
Items: 
Size: 408844 Color: 0
Size: 346460 Color: 1
Size: 244693 Color: 1

Bin 723: 4 of cap free
Amount of items: 5
Items: 
Size: 219040 Color: 1
Size: 219036 Color: 1
Size: 205990 Color: 0
Size: 203404 Color: 0
Size: 152527 Color: 0

Bin 724: 4 of cap free
Amount of items: 3
Items: 
Size: 429440 Color: 0
Size: 411625 Color: 0
Size: 158932 Color: 1

Bin 725: 4 of cap free
Amount of items: 3
Items: 
Size: 452107 Color: 1
Size: 377986 Color: 0
Size: 169904 Color: 1

Bin 726: 4 of cap free
Amount of items: 3
Items: 
Size: 456640 Color: 1
Size: 373055 Color: 0
Size: 170302 Color: 0

Bin 727: 4 of cap free
Amount of items: 3
Items: 
Size: 458260 Color: 0
Size: 408746 Color: 0
Size: 132991 Color: 1

Bin 728: 4 of cap free
Amount of items: 3
Items: 
Size: 462593 Color: 0
Size: 387174 Color: 0
Size: 150230 Color: 1

Bin 729: 4 of cap free
Amount of items: 3
Items: 
Size: 502686 Color: 0
Size: 394338 Color: 0
Size: 102973 Color: 1

Bin 730: 4 of cap free
Amount of items: 3
Items: 
Size: 504177 Color: 0
Size: 372993 Color: 1
Size: 122827 Color: 1

Bin 731: 4 of cap free
Amount of items: 3
Items: 
Size: 504256 Color: 1
Size: 382839 Color: 1
Size: 112902 Color: 0

Bin 732: 4 of cap free
Amount of items: 3
Items: 
Size: 504270 Color: 1
Size: 379153 Color: 1
Size: 116574 Color: 0

Bin 733: 4 of cap free
Amount of items: 2
Items: 
Size: 509763 Color: 1
Size: 490234 Color: 0

Bin 734: 4 of cap free
Amount of items: 2
Items: 
Size: 521146 Color: 1
Size: 478851 Color: 0

Bin 735: 4 of cap free
Amount of items: 3
Items: 
Size: 522581 Color: 0
Size: 298012 Color: 1
Size: 179404 Color: 0

Bin 736: 4 of cap free
Amount of items: 3
Items: 
Size: 522638 Color: 1
Size: 321493 Color: 1
Size: 155866 Color: 0

Bin 737: 4 of cap free
Amount of items: 2
Items: 
Size: 522851 Color: 1
Size: 477146 Color: 0

Bin 738: 4 of cap free
Amount of items: 2
Items: 
Size: 533535 Color: 0
Size: 466462 Color: 1

Bin 739: 4 of cap free
Amount of items: 3
Items: 
Size: 549195 Color: 0
Size: 273396 Color: 1
Size: 177406 Color: 0

Bin 740: 4 of cap free
Amount of items: 3
Items: 
Size: 556125 Color: 0
Size: 306654 Color: 1
Size: 137218 Color: 0

Bin 741: 4 of cap free
Amount of items: 3
Items: 
Size: 556387 Color: 0
Size: 284503 Color: 1
Size: 159107 Color: 0

Bin 742: 4 of cap free
Amount of items: 2
Items: 
Size: 566373 Color: 0
Size: 433624 Color: 1

Bin 743: 4 of cap free
Amount of items: 2
Items: 
Size: 575547 Color: 1
Size: 424450 Color: 0

Bin 744: 4 of cap free
Amount of items: 2
Items: 
Size: 584244 Color: 1
Size: 415753 Color: 0

Bin 745: 4 of cap free
Amount of items: 3
Items: 
Size: 594882 Color: 1
Size: 229971 Color: 0
Size: 175144 Color: 0

Bin 746: 4 of cap free
Amount of items: 2
Items: 
Size: 621976 Color: 1
Size: 378021 Color: 0

Bin 747: 4 of cap free
Amount of items: 3
Items: 
Size: 640220 Color: 0
Size: 200970 Color: 0
Size: 158807 Color: 1

Bin 748: 4 of cap free
Amount of items: 2
Items: 
Size: 670528 Color: 0
Size: 329469 Color: 1

Bin 749: 4 of cap free
Amount of items: 2
Items: 
Size: 680855 Color: 0
Size: 319142 Color: 1

Bin 750: 4 of cap free
Amount of items: 2
Items: 
Size: 710818 Color: 1
Size: 289179 Color: 0

Bin 751: 4 of cap free
Amount of items: 2
Items: 
Size: 787196 Color: 0
Size: 212801 Color: 1

Bin 752: 5 of cap free
Amount of items: 3
Items: 
Size: 759038 Color: 1
Size: 138784 Color: 0
Size: 102174 Color: 0

Bin 753: 5 of cap free
Amount of items: 3
Items: 
Size: 522524 Color: 0
Size: 357017 Color: 0
Size: 120455 Color: 1

Bin 754: 5 of cap free
Amount of items: 2
Items: 
Size: 594268 Color: 1
Size: 405728 Color: 0

Bin 755: 5 of cap free
Amount of items: 3
Items: 
Size: 545884 Color: 0
Size: 319889 Color: 1
Size: 134223 Color: 0

Bin 756: 5 of cap free
Amount of items: 3
Items: 
Size: 702401 Color: 1
Size: 171563 Color: 0
Size: 126032 Color: 0

Bin 757: 5 of cap free
Amount of items: 5
Items: 
Size: 276298 Color: 1
Size: 182699 Color: 0
Size: 182223 Color: 0
Size: 179459 Color: 1
Size: 179317 Color: 1

Bin 758: 5 of cap free
Amount of items: 3
Items: 
Size: 457242 Color: 1
Size: 429436 Color: 1
Size: 113318 Color: 0

Bin 759: 5 of cap free
Amount of items: 3
Items: 
Size: 480366 Color: 0
Size: 407085 Color: 0
Size: 112545 Color: 1

Bin 760: 5 of cap free
Amount of items: 3
Items: 
Size: 499587 Color: 0
Size: 322207 Color: 1
Size: 178202 Color: 1

Bin 761: 5 of cap free
Amount of items: 3
Items: 
Size: 500048 Color: 0
Size: 388514 Color: 0
Size: 111434 Color: 1

Bin 762: 5 of cap free
Amount of items: 3
Items: 
Size: 504101 Color: 0
Size: 322325 Color: 1
Size: 173570 Color: 0

Bin 763: 5 of cap free
Amount of items: 3
Items: 
Size: 506392 Color: 0
Size: 332865 Color: 1
Size: 160739 Color: 0

Bin 764: 5 of cap free
Amount of items: 2
Items: 
Size: 520319 Color: 0
Size: 479677 Color: 1

Bin 765: 5 of cap free
Amount of items: 2
Items: 
Size: 525829 Color: 1
Size: 474167 Color: 0

Bin 766: 5 of cap free
Amount of items: 3
Items: 
Size: 526006 Color: 0
Size: 358104 Color: 0
Size: 115886 Color: 1

Bin 767: 5 of cap free
Amount of items: 3
Items: 
Size: 526632 Color: 0
Size: 324157 Color: 1
Size: 149207 Color: 1

Bin 768: 5 of cap free
Amount of items: 2
Items: 
Size: 530538 Color: 0
Size: 469458 Color: 1

Bin 769: 5 of cap free
Amount of items: 2
Items: 
Size: 537735 Color: 0
Size: 462261 Color: 1

Bin 770: 5 of cap free
Amount of items: 2
Items: 
Size: 553286 Color: 1
Size: 446710 Color: 0

Bin 771: 5 of cap free
Amount of items: 2
Items: 
Size: 557526 Color: 0
Size: 442470 Color: 1

Bin 772: 5 of cap free
Amount of items: 2
Items: 
Size: 574191 Color: 0
Size: 425805 Color: 1

Bin 773: 5 of cap free
Amount of items: 3
Items: 
Size: 576984 Color: 0
Size: 297449 Color: 1
Size: 125563 Color: 0

Bin 774: 5 of cap free
Amount of items: 2
Items: 
Size: 580722 Color: 0
Size: 419274 Color: 1

Bin 775: 5 of cap free
Amount of items: 2
Items: 
Size: 584693 Color: 0
Size: 415303 Color: 1

Bin 776: 5 of cap free
Amount of items: 2
Items: 
Size: 586014 Color: 1
Size: 413982 Color: 0

Bin 777: 5 of cap free
Amount of items: 2
Items: 
Size: 597794 Color: 1
Size: 402202 Color: 0

Bin 778: 5 of cap free
Amount of items: 3
Items: 
Size: 605240 Color: 0
Size: 220497 Color: 1
Size: 174259 Color: 0

Bin 779: 5 of cap free
Amount of items: 2
Items: 
Size: 620106 Color: 1
Size: 379890 Color: 0

Bin 780: 5 of cap free
Amount of items: 3
Items: 
Size: 629101 Color: 0
Size: 187763 Color: 1
Size: 183132 Color: 1

Bin 781: 5 of cap free
Amount of items: 2
Items: 
Size: 637513 Color: 1
Size: 362483 Color: 0

Bin 782: 5 of cap free
Amount of items: 2
Items: 
Size: 644726 Color: 0
Size: 355270 Color: 1

Bin 783: 5 of cap free
Amount of items: 3
Items: 
Size: 649002 Color: 0
Size: 189743 Color: 1
Size: 161251 Color: 1

Bin 784: 5 of cap free
Amount of items: 2
Items: 
Size: 666122 Color: 1
Size: 333874 Color: 0

Bin 785: 5 of cap free
Amount of items: 2
Items: 
Size: 675402 Color: 1
Size: 324594 Color: 0

Bin 786: 5 of cap free
Amount of items: 2
Items: 
Size: 675695 Color: 0
Size: 324301 Color: 1

Bin 787: 5 of cap free
Amount of items: 2
Items: 
Size: 699245 Color: 1
Size: 300751 Color: 0

Bin 788: 5 of cap free
Amount of items: 2
Items: 
Size: 701974 Color: 0
Size: 298022 Color: 1

Bin 789: 5 of cap free
Amount of items: 2
Items: 
Size: 703500 Color: 0
Size: 296496 Color: 1

Bin 790: 5 of cap free
Amount of items: 2
Items: 
Size: 707706 Color: 0
Size: 292290 Color: 1

Bin 791: 5 of cap free
Amount of items: 2
Items: 
Size: 719701 Color: 1
Size: 280295 Color: 0

Bin 792: 5 of cap free
Amount of items: 2
Items: 
Size: 720804 Color: 1
Size: 279192 Color: 0

Bin 793: 5 of cap free
Amount of items: 3
Items: 
Size: 755185 Color: 1
Size: 134539 Color: 0
Size: 110272 Color: 0

Bin 794: 5 of cap free
Amount of items: 2
Items: 
Size: 755198 Color: 0
Size: 244798 Color: 1

Bin 795: 5 of cap free
Amount of items: 2
Items: 
Size: 782610 Color: 0
Size: 217386 Color: 1

Bin 796: 5 of cap free
Amount of items: 2
Items: 
Size: 785684 Color: 0
Size: 214312 Color: 1

Bin 797: 6 of cap free
Amount of items: 3
Items: 
Size: 447368 Color: 0
Size: 392911 Color: 1
Size: 159716 Color: 1

Bin 798: 6 of cap free
Amount of items: 2
Items: 
Size: 760412 Color: 1
Size: 239583 Color: 0

Bin 799: 6 of cap free
Amount of items: 3
Items: 
Size: 577755 Color: 1
Size: 278490 Color: 0
Size: 143750 Color: 1

Bin 800: 6 of cap free
Amount of items: 3
Items: 
Size: 502421 Color: 0
Size: 382100 Color: 1
Size: 115474 Color: 1

Bin 801: 6 of cap free
Amount of items: 5
Items: 
Size: 220901 Color: 1
Size: 220049 Color: 1
Size: 208378 Color: 0
Size: 206949 Color: 0
Size: 143718 Color: 0

Bin 802: 6 of cap free
Amount of items: 3
Items: 
Size: 477087 Color: 0
Size: 374642 Color: 1
Size: 148266 Color: 1

Bin 803: 6 of cap free
Amount of items: 5
Items: 
Size: 285671 Color: 0
Size: 180800 Color: 0
Size: 180718 Color: 1
Size: 176600 Color: 1
Size: 176206 Color: 1

Bin 804: 6 of cap free
Amount of items: 3
Items: 
Size: 486490 Color: 0
Size: 397149 Color: 0
Size: 116356 Color: 1

Bin 805: 6 of cap free
Amount of items: 3
Items: 
Size: 499213 Color: 0
Size: 367605 Color: 0
Size: 133177 Color: 1

Bin 806: 6 of cap free
Amount of items: 2
Items: 
Size: 519352 Color: 0
Size: 480643 Color: 1

Bin 807: 6 of cap free
Amount of items: 2
Items: 
Size: 537565 Color: 1
Size: 462430 Color: 0

Bin 808: 6 of cap free
Amount of items: 2
Items: 
Size: 557994 Color: 0
Size: 442001 Color: 1

Bin 809: 6 of cap free
Amount of items: 2
Items: 
Size: 561360 Color: 0
Size: 438635 Color: 1

Bin 810: 6 of cap free
Amount of items: 3
Items: 
Size: 571328 Color: 0
Size: 255223 Color: 1
Size: 173444 Color: 0

Bin 811: 6 of cap free
Amount of items: 3
Items: 
Size: 580640 Color: 0
Size: 236025 Color: 1
Size: 183330 Color: 1

Bin 812: 6 of cap free
Amount of items: 2
Items: 
Size: 602433 Color: 1
Size: 397562 Color: 0

Bin 813: 6 of cap free
Amount of items: 2
Items: 
Size: 615924 Color: 0
Size: 384071 Color: 1

Bin 814: 6 of cap free
Amount of items: 2
Items: 
Size: 616800 Color: 0
Size: 383195 Color: 1

Bin 815: 6 of cap free
Amount of items: 2
Items: 
Size: 621844 Color: 0
Size: 378151 Color: 1

Bin 816: 6 of cap free
Amount of items: 2
Items: 
Size: 623492 Color: 0
Size: 376503 Color: 1

Bin 817: 6 of cap free
Amount of items: 2
Items: 
Size: 633554 Color: 0
Size: 366441 Color: 1

Bin 818: 6 of cap free
Amount of items: 2
Items: 
Size: 648252 Color: 0
Size: 351743 Color: 1

Bin 819: 6 of cap free
Amount of items: 2
Items: 
Size: 663595 Color: 0
Size: 336400 Color: 1

Bin 820: 6 of cap free
Amount of items: 2
Items: 
Size: 683800 Color: 0
Size: 316195 Color: 1

Bin 821: 6 of cap free
Amount of items: 2
Items: 
Size: 691884 Color: 1
Size: 308111 Color: 0

Bin 822: 6 of cap free
Amount of items: 2
Items: 
Size: 712474 Color: 0
Size: 287521 Color: 1

Bin 823: 6 of cap free
Amount of items: 2
Items: 
Size: 742808 Color: 0
Size: 257187 Color: 1

Bin 824: 6 of cap free
Amount of items: 2
Items: 
Size: 752100 Color: 1
Size: 247895 Color: 0

Bin 825: 6 of cap free
Amount of items: 2
Items: 
Size: 771522 Color: 0
Size: 228473 Color: 1

Bin 826: 7 of cap free
Amount of items: 3
Items: 
Size: 735069 Color: 0
Size: 139593 Color: 1
Size: 125332 Color: 1

Bin 827: 7 of cap free
Amount of items: 3
Items: 
Size: 571332 Color: 1
Size: 243831 Color: 0
Size: 184831 Color: 0

Bin 828: 7 of cap free
Amount of items: 3
Items: 
Size: 500005 Color: 0
Size: 379265 Color: 1
Size: 120724 Color: 0

Bin 829: 7 of cap free
Amount of items: 2
Items: 
Size: 569823 Color: 0
Size: 430171 Color: 1

Bin 830: 7 of cap free
Amount of items: 3
Items: 
Size: 558870 Color: 0
Size: 267133 Color: 1
Size: 173991 Color: 0

Bin 831: 7 of cap free
Amount of items: 5
Items: 
Size: 259590 Color: 1
Size: 253943 Color: 0
Size: 173315 Color: 0
Size: 157763 Color: 1
Size: 155383 Color: 1

Bin 832: 7 of cap free
Amount of items: 3
Items: 
Size: 612150 Color: 1
Size: 250390 Color: 1
Size: 137454 Color: 0

Bin 833: 7 of cap free
Amount of items: 3
Items: 
Size: 480921 Color: 0
Size: 380376 Color: 1
Size: 138697 Color: 1

Bin 834: 7 of cap free
Amount of items: 5
Items: 
Size: 234519 Color: 0
Size: 193530 Color: 0
Size: 192191 Color: 0
Size: 189884 Color: 1
Size: 189870 Color: 1

Bin 835: 7 of cap free
Amount of items: 3
Items: 
Size: 431159 Color: 0
Size: 430588 Color: 0
Size: 138247 Color: 1

Bin 836: 7 of cap free
Amount of items: 3
Items: 
Size: 444700 Color: 0
Size: 405137 Color: 0
Size: 150157 Color: 1

Bin 837: 7 of cap free
Amount of items: 3
Items: 
Size: 480432 Color: 0
Size: 373274 Color: 0
Size: 146288 Color: 1

Bin 838: 7 of cap free
Amount of items: 3
Items: 
Size: 481777 Color: 1
Size: 379954 Color: 0
Size: 138263 Color: 1

Bin 839: 7 of cap free
Amount of items: 3
Items: 
Size: 481810 Color: 1
Size: 379358 Color: 0
Size: 138826 Color: 1

Bin 840: 7 of cap free
Amount of items: 3
Items: 
Size: 497260 Color: 0
Size: 377986 Color: 0
Size: 124748 Color: 1

Bin 841: 7 of cap free
Amount of items: 3
Items: 
Size: 500037 Color: 0
Size: 340548 Color: 1
Size: 159409 Color: 0

Bin 842: 7 of cap free
Amount of items: 3
Items: 
Size: 500270 Color: 0
Size: 376133 Color: 0
Size: 123591 Color: 1

Bin 843: 7 of cap free
Amount of items: 2
Items: 
Size: 508704 Color: 1
Size: 491290 Color: 0

Bin 844: 7 of cap free
Amount of items: 2
Items: 
Size: 513382 Color: 1
Size: 486612 Color: 0

Bin 845: 7 of cap free
Amount of items: 3
Items: 
Size: 515343 Color: 0
Size: 356279 Color: 0
Size: 128372 Color: 1

Bin 846: 7 of cap free
Amount of items: 2
Items: 
Size: 526511 Color: 0
Size: 473483 Color: 1

Bin 847: 7 of cap free
Amount of items: 2
Items: 
Size: 553186 Color: 0
Size: 446808 Color: 1

Bin 848: 7 of cap free
Amount of items: 2
Items: 
Size: 559801 Color: 1
Size: 440193 Color: 0

Bin 849: 7 of cap free
Amount of items: 2
Items: 
Size: 587050 Color: 1
Size: 412944 Color: 0

Bin 850: 7 of cap free
Amount of items: 2
Items: 
Size: 591081 Color: 1
Size: 408913 Color: 0

Bin 851: 7 of cap free
Amount of items: 3
Items: 
Size: 599308 Color: 0
Size: 220293 Color: 1
Size: 180393 Color: 0

Bin 852: 7 of cap free
Amount of items: 2
Items: 
Size: 601048 Color: 1
Size: 398946 Color: 0

Bin 853: 7 of cap free
Amount of items: 2
Items: 
Size: 601981 Color: 1
Size: 398013 Color: 0

Bin 854: 7 of cap free
Amount of items: 2
Items: 
Size: 611098 Color: 1
Size: 388896 Color: 0

Bin 855: 7 of cap free
Amount of items: 2
Items: 
Size: 614874 Color: 0
Size: 385120 Color: 1

Bin 856: 7 of cap free
Amount of items: 2
Items: 
Size: 632292 Color: 1
Size: 367702 Color: 0

Bin 857: 7 of cap free
Amount of items: 2
Items: 
Size: 637006 Color: 0
Size: 362988 Color: 1

Bin 858: 7 of cap free
Amount of items: 2
Items: 
Size: 684722 Color: 0
Size: 315272 Color: 1

Bin 859: 7 of cap free
Amount of items: 2
Items: 
Size: 686711 Color: 0
Size: 313283 Color: 1

Bin 860: 7 of cap free
Amount of items: 2
Items: 
Size: 687446 Color: 1
Size: 312548 Color: 0

Bin 861: 7 of cap free
Amount of items: 2
Items: 
Size: 689099 Color: 0
Size: 310895 Color: 1

Bin 862: 7 of cap free
Amount of items: 2
Items: 
Size: 690897 Color: 0
Size: 309097 Color: 1

Bin 863: 7 of cap free
Amount of items: 2
Items: 
Size: 691480 Color: 0
Size: 308514 Color: 1

Bin 864: 7 of cap free
Amount of items: 2
Items: 
Size: 693245 Color: 0
Size: 306749 Color: 1

Bin 865: 7 of cap free
Amount of items: 2
Items: 
Size: 699649 Color: 0
Size: 300345 Color: 1

Bin 866: 7 of cap free
Amount of items: 2
Items: 
Size: 716776 Color: 1
Size: 283218 Color: 0

Bin 867: 7 of cap free
Amount of items: 2
Items: 
Size: 747631 Color: 1
Size: 252363 Color: 0

Bin 868: 7 of cap free
Amount of items: 2
Items: 
Size: 749705 Color: 1
Size: 250289 Color: 0

Bin 869: 7 of cap free
Amount of items: 2
Items: 
Size: 760632 Color: 0
Size: 239362 Color: 1

Bin 870: 7 of cap free
Amount of items: 2
Items: 
Size: 772819 Color: 0
Size: 227175 Color: 1

Bin 871: 7 of cap free
Amount of items: 2
Items: 
Size: 779176 Color: 1
Size: 220818 Color: 0

Bin 872: 7 of cap free
Amount of items: 2
Items: 
Size: 786195 Color: 1
Size: 213799 Color: 0

Bin 873: 7 of cap free
Amount of items: 2
Items: 
Size: 791764 Color: 1
Size: 208230 Color: 0

Bin 874: 8 of cap free
Amount of items: 3
Items: 
Size: 486582 Color: 0
Size: 381519 Color: 1
Size: 131892 Color: 1

Bin 875: 8 of cap free
Amount of items: 5
Items: 
Size: 203644 Color: 0
Size: 200318 Color: 0
Size: 199327 Color: 0
Size: 198562 Color: 1
Size: 198142 Color: 1

Bin 876: 8 of cap free
Amount of items: 2
Items: 
Size: 557836 Color: 1
Size: 442157 Color: 0

Bin 877: 8 of cap free
Amount of items: 3
Items: 
Size: 460032 Color: 0
Size: 413694 Color: 0
Size: 126267 Color: 1

Bin 878: 8 of cap free
Amount of items: 3
Items: 
Size: 511645 Color: 0
Size: 339384 Color: 1
Size: 148964 Color: 0

Bin 879: 8 of cap free
Amount of items: 2
Items: 
Size: 512607 Color: 1
Size: 487386 Color: 0

Bin 880: 8 of cap free
Amount of items: 3
Items: 
Size: 522480 Color: 1
Size: 317454 Color: 1
Size: 160059 Color: 0

Bin 881: 8 of cap free
Amount of items: 3
Items: 
Size: 522689 Color: 1
Size: 327207 Color: 1
Size: 150097 Color: 0

Bin 882: 8 of cap free
Amount of items: 2
Items: 
Size: 540001 Color: 0
Size: 459992 Color: 1

Bin 883: 8 of cap free
Amount of items: 2
Items: 
Size: 544640 Color: 1
Size: 455353 Color: 0

Bin 884: 8 of cap free
Amount of items: 2
Items: 
Size: 547352 Color: 0
Size: 452641 Color: 1

Bin 885: 8 of cap free
Amount of items: 2
Items: 
Size: 551073 Color: 0
Size: 448920 Color: 1

Bin 886: 8 of cap free
Amount of items: 2
Items: 
Size: 551985 Color: 0
Size: 448008 Color: 1

Bin 887: 8 of cap free
Amount of items: 2
Items: 
Size: 554531 Color: 0
Size: 445462 Color: 1

Bin 888: 8 of cap free
Amount of items: 3
Items: 
Size: 580150 Color: 0
Size: 236115 Color: 1
Size: 183728 Color: 0

Bin 889: 8 of cap free
Amount of items: 3
Items: 
Size: 580811 Color: 0
Size: 243093 Color: 1
Size: 176089 Color: 1

Bin 890: 8 of cap free
Amount of items: 3
Items: 
Size: 589499 Color: 0
Size: 224495 Color: 1
Size: 185999 Color: 0

Bin 891: 8 of cap free
Amount of items: 2
Items: 
Size: 595320 Color: 1
Size: 404673 Color: 0

Bin 892: 8 of cap free
Amount of items: 2
Items: 
Size: 603591 Color: 1
Size: 396402 Color: 0

Bin 893: 8 of cap free
Amount of items: 2
Items: 
Size: 624610 Color: 0
Size: 375383 Color: 1

Bin 894: 8 of cap free
Amount of items: 2
Items: 
Size: 630473 Color: 1
Size: 369520 Color: 0

Bin 895: 8 of cap free
Amount of items: 2
Items: 
Size: 632772 Color: 1
Size: 367221 Color: 0

Bin 896: 8 of cap free
Amount of items: 2
Items: 
Size: 692596 Color: 0
Size: 307397 Color: 1

Bin 897: 8 of cap free
Amount of items: 2
Items: 
Size: 692754 Color: 0
Size: 307239 Color: 1

Bin 898: 8 of cap free
Amount of items: 2
Items: 
Size: 698661 Color: 1
Size: 301332 Color: 0

Bin 899: 8 of cap free
Amount of items: 2
Items: 
Size: 700544 Color: 0
Size: 299449 Color: 1

Bin 900: 8 of cap free
Amount of items: 2
Items: 
Size: 701327 Color: 0
Size: 298666 Color: 1

Bin 901: 8 of cap free
Amount of items: 2
Items: 
Size: 715744 Color: 0
Size: 284249 Color: 1

Bin 902: 8 of cap free
Amount of items: 2
Items: 
Size: 717899 Color: 0
Size: 282094 Color: 1

Bin 903: 8 of cap free
Amount of items: 2
Items: 
Size: 720790 Color: 0
Size: 279203 Color: 1

Bin 904: 8 of cap free
Amount of items: 2
Items: 
Size: 722902 Color: 1
Size: 277091 Color: 0

Bin 905: 8 of cap free
Amount of items: 2
Items: 
Size: 733149 Color: 1
Size: 266844 Color: 0

Bin 906: 8 of cap free
Amount of items: 2
Items: 
Size: 734411 Color: 0
Size: 265582 Color: 1

Bin 907: 8 of cap free
Amount of items: 2
Items: 
Size: 751983 Color: 1
Size: 248010 Color: 0

Bin 908: 9 of cap free
Amount of items: 3
Items: 
Size: 530048 Color: 1
Size: 360902 Color: 0
Size: 109042 Color: 0

Bin 909: 9 of cap free
Amount of items: 3
Items: 
Size: 593423 Color: 1
Size: 234492 Color: 1
Size: 172077 Color: 0

Bin 910: 9 of cap free
Amount of items: 3
Items: 
Size: 477109 Color: 0
Size: 387136 Color: 0
Size: 135747 Color: 1

Bin 911: 9 of cap free
Amount of items: 3
Items: 
Size: 495031 Color: 0
Size: 374044 Color: 0
Size: 130917 Color: 1

Bin 912: 9 of cap free
Amount of items: 3
Items: 
Size: 499085 Color: 0
Size: 324424 Color: 1
Size: 176483 Color: 0

Bin 913: 9 of cap free
Amount of items: 3
Items: 
Size: 499896 Color: 0
Size: 332795 Color: 1
Size: 167301 Color: 0

Bin 914: 9 of cap free
Amount of items: 2
Items: 
Size: 521842 Color: 0
Size: 478150 Color: 1

Bin 915: 9 of cap free
Amount of items: 3
Items: 
Size: 525351 Color: 1
Size: 331318 Color: 1
Size: 143323 Color: 0

Bin 916: 9 of cap free
Amount of items: 2
Items: 
Size: 531071 Color: 1
Size: 468921 Color: 0

Bin 917: 9 of cap free
Amount of items: 2
Items: 
Size: 547992 Color: 0
Size: 452000 Color: 1

Bin 918: 9 of cap free
Amount of items: 3
Items: 
Size: 556576 Color: 1
Size: 321377 Color: 1
Size: 122039 Color: 0

Bin 919: 9 of cap free
Amount of items: 2
Items: 
Size: 557069 Color: 1
Size: 442923 Color: 0

Bin 920: 9 of cap free
Amount of items: 2
Items: 
Size: 561684 Color: 0
Size: 438308 Color: 1

Bin 921: 9 of cap free
Amount of items: 2
Items: 
Size: 569541 Color: 0
Size: 430451 Color: 1

Bin 922: 9 of cap free
Amount of items: 2
Items: 
Size: 570286 Color: 0
Size: 429706 Color: 1

Bin 923: 9 of cap free
Amount of items: 2
Items: 
Size: 588016 Color: 0
Size: 411976 Color: 1

Bin 924: 9 of cap free
Amount of items: 2
Items: 
Size: 592187 Color: 1
Size: 407805 Color: 0

Bin 925: 9 of cap free
Amount of items: 2
Items: 
Size: 592905 Color: 0
Size: 407087 Color: 1

Bin 926: 9 of cap free
Amount of items: 2
Items: 
Size: 594050 Color: 0
Size: 405942 Color: 1

Bin 927: 9 of cap free
Amount of items: 2
Items: 
Size: 655378 Color: 0
Size: 344614 Color: 1

Bin 928: 9 of cap free
Amount of items: 2
Items: 
Size: 663075 Color: 1
Size: 336917 Color: 0

Bin 929: 9 of cap free
Amount of items: 2
Items: 
Size: 674953 Color: 0
Size: 325039 Color: 1

Bin 930: 9 of cap free
Amount of items: 2
Items: 
Size: 675083 Color: 1
Size: 324909 Color: 0

Bin 931: 9 of cap free
Amount of items: 2
Items: 
Size: 683923 Color: 1
Size: 316069 Color: 0

Bin 932: 9 of cap free
Amount of items: 2
Items: 
Size: 686665 Color: 1
Size: 313327 Color: 0

Bin 933: 9 of cap free
Amount of items: 2
Items: 
Size: 689417 Color: 1
Size: 310575 Color: 0

Bin 934: 9 of cap free
Amount of items: 2
Items: 
Size: 706476 Color: 1
Size: 293516 Color: 0

Bin 935: 9 of cap free
Amount of items: 2
Items: 
Size: 708105 Color: 1
Size: 291887 Color: 0

Bin 936: 9 of cap free
Amount of items: 2
Items: 
Size: 708948 Color: 1
Size: 291044 Color: 0

Bin 937: 9 of cap free
Amount of items: 2
Items: 
Size: 738942 Color: 0
Size: 261050 Color: 1

Bin 938: 9 of cap free
Amount of items: 2
Items: 
Size: 740101 Color: 1
Size: 259891 Color: 0

Bin 939: 9 of cap free
Amount of items: 2
Items: 
Size: 752399 Color: 0
Size: 247593 Color: 1

Bin 940: 9 of cap free
Amount of items: 2
Items: 
Size: 766441 Color: 0
Size: 233551 Color: 1

Bin 941: 9 of cap free
Amount of items: 2
Items: 
Size: 782836 Color: 0
Size: 217156 Color: 1

Bin 942: 9 of cap free
Amount of items: 2
Items: 
Size: 787075 Color: 1
Size: 212917 Color: 0

Bin 943: 9 of cap free
Amount of items: 2
Items: 
Size: 789353 Color: 0
Size: 210639 Color: 1

Bin 944: 10 of cap free
Amount of items: 3
Items: 
Size: 739849 Color: 0
Size: 153023 Color: 1
Size: 107119 Color: 1

Bin 945: 10 of cap free
Amount of items: 3
Items: 
Size: 555873 Color: 0
Size: 300433 Color: 1
Size: 143685 Color: 0

Bin 946: 10 of cap free
Amount of items: 3
Items: 
Size: 422965 Color: 0
Size: 389512 Color: 0
Size: 187514 Color: 1

Bin 947: 10 of cap free
Amount of items: 2
Items: 
Size: 504858 Color: 0
Size: 495133 Color: 1

Bin 948: 10 of cap free
Amount of items: 2
Items: 
Size: 518288 Color: 1
Size: 481703 Color: 0

Bin 949: 10 of cap free
Amount of items: 2
Items: 
Size: 521741 Color: 1
Size: 478250 Color: 0

Bin 950: 10 of cap free
Amount of items: 2
Items: 
Size: 527473 Color: 0
Size: 472518 Color: 1

Bin 951: 10 of cap free
Amount of items: 2
Items: 
Size: 534374 Color: 1
Size: 465617 Color: 0

Bin 952: 10 of cap free
Amount of items: 2
Items: 
Size: 540093 Color: 1
Size: 459898 Color: 0

Bin 953: 10 of cap free
Amount of items: 2
Items: 
Size: 544577 Color: 1
Size: 455414 Color: 0

Bin 954: 10 of cap free
Amount of items: 2
Items: 
Size: 560419 Color: 1
Size: 439572 Color: 0

Bin 955: 10 of cap free
Amount of items: 2
Items: 
Size: 565158 Color: 0
Size: 434833 Color: 1

Bin 956: 10 of cap free
Amount of items: 2
Items: 
Size: 572723 Color: 1
Size: 427268 Color: 0

Bin 957: 10 of cap free
Amount of items: 3
Items: 
Size: 590005 Color: 0
Size: 246162 Color: 0
Size: 163824 Color: 1

Bin 958: 10 of cap free
Amount of items: 2
Items: 
Size: 592996 Color: 1
Size: 406995 Color: 0

Bin 959: 10 of cap free
Amount of items: 2
Items: 
Size: 606486 Color: 0
Size: 393505 Color: 1

Bin 960: 10 of cap free
Amount of items: 2
Items: 
Size: 613543 Color: 0
Size: 386448 Color: 1

Bin 961: 10 of cap free
Amount of items: 2
Items: 
Size: 613662 Color: 1
Size: 386329 Color: 0

Bin 962: 10 of cap free
Amount of items: 2
Items: 
Size: 615068 Color: 1
Size: 384923 Color: 0

Bin 963: 10 of cap free
Amount of items: 2
Items: 
Size: 617307 Color: 0
Size: 382684 Color: 1

Bin 964: 10 of cap free
Amount of items: 2
Items: 
Size: 632080 Color: 1
Size: 367911 Color: 0

Bin 965: 10 of cap free
Amount of items: 2
Items: 
Size: 646418 Color: 0
Size: 353573 Color: 1

Bin 966: 10 of cap free
Amount of items: 2
Items: 
Size: 651529 Color: 0
Size: 348462 Color: 1

Bin 967: 10 of cap free
Amount of items: 2
Items: 
Size: 652001 Color: 0
Size: 347990 Color: 1

Bin 968: 10 of cap free
Amount of items: 2
Items: 
Size: 656115 Color: 1
Size: 343876 Color: 0

Bin 969: 10 of cap free
Amount of items: 2
Items: 
Size: 677470 Color: 0
Size: 322521 Color: 1

Bin 970: 10 of cap free
Amount of items: 2
Items: 
Size: 685522 Color: 1
Size: 314469 Color: 0

Bin 971: 10 of cap free
Amount of items: 2
Items: 
Size: 687911 Color: 1
Size: 312080 Color: 0

Bin 972: 10 of cap free
Amount of items: 2
Items: 
Size: 695947 Color: 0
Size: 304044 Color: 1

Bin 973: 10 of cap free
Amount of items: 2
Items: 
Size: 703089 Color: 1
Size: 296902 Color: 0

Bin 974: 10 of cap free
Amount of items: 2
Items: 
Size: 720340 Color: 0
Size: 279651 Color: 1

Bin 975: 10 of cap free
Amount of items: 2
Items: 
Size: 748213 Color: 1
Size: 251778 Color: 0

Bin 976: 10 of cap free
Amount of items: 2
Items: 
Size: 758930 Color: 1
Size: 241061 Color: 0

Bin 977: 10 of cap free
Amount of items: 2
Items: 
Size: 766728 Color: 1
Size: 233263 Color: 0

Bin 978: 10 of cap free
Amount of items: 2
Items: 
Size: 767004 Color: 1
Size: 232987 Color: 0

Bin 979: 10 of cap free
Amount of items: 2
Items: 
Size: 773762 Color: 0
Size: 226229 Color: 1

Bin 980: 11 of cap free
Amount of items: 3
Items: 
Size: 574788 Color: 0
Size: 285145 Color: 1
Size: 140057 Color: 1

Bin 981: 11 of cap free
Amount of items: 3
Items: 
Size: 590018 Color: 1
Size: 213701 Color: 0
Size: 196271 Color: 0

Bin 982: 11 of cap free
Amount of items: 3
Items: 
Size: 548908 Color: 0
Size: 317296 Color: 1
Size: 133786 Color: 0

Bin 983: 11 of cap free
Amount of items: 3
Items: 
Size: 652152 Color: 0
Size: 213299 Color: 0
Size: 134539 Color: 1

Bin 984: 11 of cap free
Amount of items: 3
Items: 
Size: 451133 Color: 1
Size: 432294 Color: 0
Size: 116563 Color: 0

Bin 985: 11 of cap free
Amount of items: 2
Items: 
Size: 503807 Color: 0
Size: 496183 Color: 1

Bin 986: 11 of cap free
Amount of items: 2
Items: 
Size: 506770 Color: 0
Size: 493220 Color: 1

Bin 987: 11 of cap free
Amount of items: 2
Items: 
Size: 509217 Color: 1
Size: 490773 Color: 0

Bin 988: 11 of cap free
Amount of items: 2
Items: 
Size: 520261 Color: 0
Size: 479729 Color: 1

Bin 989: 11 of cap free
Amount of items: 2
Items: 
Size: 551769 Color: 0
Size: 448221 Color: 1

Bin 990: 11 of cap free
Amount of items: 2
Items: 
Size: 555239 Color: 1
Size: 444751 Color: 0

Bin 991: 11 of cap free
Amount of items: 2
Items: 
Size: 556639 Color: 0
Size: 443351 Color: 1

Bin 992: 11 of cap free
Amount of items: 2
Items: 
Size: 558387 Color: 0
Size: 441603 Color: 1

Bin 993: 11 of cap free
Amount of items: 3
Items: 
Size: 575216 Color: 0
Size: 253310 Color: 0
Size: 171464 Color: 1

Bin 994: 11 of cap free
Amount of items: 2
Items: 
Size: 585587 Color: 1
Size: 414403 Color: 0

Bin 995: 11 of cap free
Amount of items: 2
Items: 
Size: 592064 Color: 0
Size: 407926 Color: 1

Bin 996: 11 of cap free
Amount of items: 2
Items: 
Size: 593147 Color: 1
Size: 406843 Color: 0

Bin 997: 11 of cap free
Amount of items: 2
Items: 
Size: 599514 Color: 1
Size: 400476 Color: 0

Bin 998: 11 of cap free
Amount of items: 2
Items: 
Size: 599546 Color: 1
Size: 400444 Color: 0

Bin 999: 11 of cap free
Amount of items: 2
Items: 
Size: 606869 Color: 0
Size: 393121 Color: 1

Bin 1000: 11 of cap free
Amount of items: 2
Items: 
Size: 613703 Color: 1
Size: 386287 Color: 0

Bin 1001: 11 of cap free
Amount of items: 2
Items: 
Size: 630474 Color: 0
Size: 369516 Color: 1

Bin 1002: 11 of cap free
Amount of items: 2
Items: 
Size: 654420 Color: 0
Size: 345570 Color: 1

Bin 1003: 11 of cap free
Amount of items: 2
Items: 
Size: 665903 Color: 1
Size: 334087 Color: 0

Bin 1004: 11 of cap free
Amount of items: 2
Items: 
Size: 668047 Color: 1
Size: 331943 Color: 0

Bin 1005: 11 of cap free
Amount of items: 2
Items: 
Size: 678104 Color: 1
Size: 321886 Color: 0

Bin 1006: 11 of cap free
Amount of items: 2
Items: 
Size: 681236 Color: 0
Size: 318754 Color: 1

Bin 1007: 11 of cap free
Amount of items: 2
Items: 
Size: 708836 Color: 0
Size: 291154 Color: 1

Bin 1008: 11 of cap free
Amount of items: 2
Items: 
Size: 713328 Color: 1
Size: 286662 Color: 0

Bin 1009: 11 of cap free
Amount of items: 2
Items: 
Size: 718731 Color: 1
Size: 281259 Color: 0

Bin 1010: 11 of cap free
Amount of items: 2
Items: 
Size: 741240 Color: 0
Size: 258750 Color: 1

Bin 1011: 11 of cap free
Amount of items: 2
Items: 
Size: 743089 Color: 1
Size: 256901 Color: 0

Bin 1012: 11 of cap free
Amount of items: 2
Items: 
Size: 746640 Color: 0
Size: 253350 Color: 1

Bin 1013: 11 of cap free
Amount of items: 2
Items: 
Size: 759761 Color: 1
Size: 240229 Color: 0

Bin 1014: 12 of cap free
Amount of items: 2
Items: 
Size: 574430 Color: 1
Size: 425559 Color: 0

Bin 1015: 12 of cap free
Amount of items: 3
Items: 
Size: 479304 Color: 0
Size: 396448 Color: 0
Size: 124237 Color: 1

Bin 1016: 12 of cap free
Amount of items: 3
Items: 
Size: 486208 Color: 0
Size: 374318 Color: 1
Size: 139463 Color: 1

Bin 1017: 12 of cap free
Amount of items: 3
Items: 
Size: 508919 Color: 0
Size: 332975 Color: 1
Size: 158095 Color: 0

Bin 1018: 12 of cap free
Amount of items: 2
Items: 
Size: 521963 Color: 1
Size: 478026 Color: 0

Bin 1019: 12 of cap free
Amount of items: 2
Items: 
Size: 523862 Color: 1
Size: 476127 Color: 0

Bin 1020: 12 of cap free
Amount of items: 2
Items: 
Size: 543239 Color: 1
Size: 456750 Color: 0

Bin 1021: 12 of cap free
Amount of items: 2
Items: 
Size: 570028 Color: 1
Size: 429961 Color: 0

Bin 1022: 12 of cap free
Amount of items: 2
Items: 
Size: 581232 Color: 0
Size: 418757 Color: 1

Bin 1023: 12 of cap free
Amount of items: 2
Items: 
Size: 597711 Color: 1
Size: 402278 Color: 0

Bin 1024: 12 of cap free
Amount of items: 2
Items: 
Size: 599944 Color: 0
Size: 400045 Color: 1

Bin 1025: 12 of cap free
Amount of items: 2
Items: 
Size: 640945 Color: 1
Size: 359044 Color: 0

Bin 1026: 12 of cap free
Amount of items: 2
Items: 
Size: 646586 Color: 1
Size: 353403 Color: 0

Bin 1027: 12 of cap free
Amount of items: 2
Items: 
Size: 658281 Color: 0
Size: 341708 Color: 1

Bin 1028: 12 of cap free
Amount of items: 2
Items: 
Size: 660571 Color: 1
Size: 339418 Color: 0

Bin 1029: 12 of cap free
Amount of items: 2
Items: 
Size: 668659 Color: 1
Size: 331330 Color: 0

Bin 1030: 12 of cap free
Amount of items: 2
Items: 
Size: 684935 Color: 0
Size: 315054 Color: 1

Bin 1031: 12 of cap free
Amount of items: 2
Items: 
Size: 706497 Color: 1
Size: 293492 Color: 0

Bin 1032: 12 of cap free
Amount of items: 2
Items: 
Size: 707935 Color: 0
Size: 292054 Color: 1

Bin 1033: 12 of cap free
Amount of items: 2
Items: 
Size: 710998 Color: 1
Size: 288991 Color: 0

Bin 1034: 12 of cap free
Amount of items: 2
Items: 
Size: 719215 Color: 0
Size: 280774 Color: 1

Bin 1035: 12 of cap free
Amount of items: 2
Items: 
Size: 727710 Color: 0
Size: 272279 Color: 1

Bin 1036: 12 of cap free
Amount of items: 2
Items: 
Size: 746850 Color: 1
Size: 253139 Color: 0

Bin 1037: 12 of cap free
Amount of items: 2
Items: 
Size: 747821 Color: 0
Size: 252168 Color: 1

Bin 1038: 12 of cap free
Amount of items: 2
Items: 
Size: 753982 Color: 0
Size: 246007 Color: 1

Bin 1039: 12 of cap free
Amount of items: 2
Items: 
Size: 755925 Color: 1
Size: 244064 Color: 0

Bin 1040: 12 of cap free
Amount of items: 2
Items: 
Size: 762149 Color: 1
Size: 237840 Color: 0

Bin 1041: 13 of cap free
Amount of items: 2
Items: 
Size: 678451 Color: 0
Size: 321537 Color: 1

Bin 1042: 13 of cap free
Amount of items: 3
Items: 
Size: 408693 Color: 0
Size: 407820 Color: 0
Size: 183475 Color: 1

Bin 1043: 13 of cap free
Amount of items: 3
Items: 
Size: 432530 Color: 1
Size: 377578 Color: 0
Size: 189880 Color: 0

Bin 1044: 13 of cap free
Amount of items: 2
Items: 
Size: 514941 Color: 0
Size: 485047 Color: 1

Bin 1045: 13 of cap free
Amount of items: 3
Items: 
Size: 522579 Color: 1
Size: 298711 Color: 1
Size: 178698 Color: 0

Bin 1046: 13 of cap free
Amount of items: 2
Items: 
Size: 524887 Color: 0
Size: 475101 Color: 1

Bin 1047: 13 of cap free
Amount of items: 2
Items: 
Size: 529880 Color: 1
Size: 470108 Color: 0

Bin 1048: 13 of cap free
Amount of items: 3
Items: 
Size: 549093 Color: 1
Size: 324119 Color: 1
Size: 126776 Color: 0

Bin 1049: 13 of cap free
Amount of items: 2
Items: 
Size: 562971 Color: 0
Size: 437017 Color: 1

Bin 1050: 13 of cap free
Amount of items: 2
Items: 
Size: 563012 Color: 0
Size: 436976 Color: 1

Bin 1051: 13 of cap free
Amount of items: 2
Items: 
Size: 586076 Color: 1
Size: 413912 Color: 0

Bin 1052: 13 of cap free
Amount of items: 2
Items: 
Size: 597322 Color: 1
Size: 402666 Color: 0

Bin 1053: 13 of cap free
Amount of items: 2
Items: 
Size: 617782 Color: 0
Size: 382206 Color: 1

Bin 1054: 13 of cap free
Amount of items: 2
Items: 
Size: 630514 Color: 1
Size: 369474 Color: 0

Bin 1055: 13 of cap free
Amount of items: 2
Items: 
Size: 652858 Color: 0
Size: 347130 Color: 1

Bin 1056: 13 of cap free
Amount of items: 2
Items: 
Size: 653129 Color: 1
Size: 346859 Color: 0

Bin 1057: 13 of cap free
Amount of items: 2
Items: 
Size: 701884 Color: 0
Size: 298104 Color: 1

Bin 1058: 13 of cap free
Amount of items: 2
Items: 
Size: 744166 Color: 0
Size: 255822 Color: 1

Bin 1059: 13 of cap free
Amount of items: 2
Items: 
Size: 768961 Color: 1
Size: 231027 Color: 0

Bin 1060: 13 of cap free
Amount of items: 2
Items: 
Size: 790959 Color: 0
Size: 209029 Color: 1

Bin 1061: 13 of cap free
Amount of items: 2
Items: 
Size: 799561 Color: 0
Size: 200427 Color: 1

Bin 1062: 14 of cap free
Amount of items: 5
Items: 
Size: 208479 Color: 0
Size: 199303 Color: 0
Size: 198474 Color: 0
Size: 196880 Color: 1
Size: 196851 Color: 1

Bin 1063: 14 of cap free
Amount of items: 3
Items: 
Size: 504555 Color: 0
Size: 300763 Color: 1
Size: 194669 Color: 0

Bin 1064: 14 of cap free
Amount of items: 3
Items: 
Size: 571668 Color: 1
Size: 277661 Color: 0
Size: 150658 Color: 0

Bin 1065: 14 of cap free
Amount of items: 3
Items: 
Size: 445076 Color: 0
Size: 396740 Color: 0
Size: 158171 Color: 1

Bin 1066: 14 of cap free
Amount of items: 3
Items: 
Size: 469051 Color: 0
Size: 379284 Color: 0
Size: 151652 Color: 1

Bin 1067: 14 of cap free
Amount of items: 2
Items: 
Size: 507307 Color: 1
Size: 492680 Color: 0

Bin 1068: 14 of cap free
Amount of items: 2
Items: 
Size: 509836 Color: 0
Size: 490151 Color: 1

Bin 1069: 14 of cap free
Amount of items: 2
Items: 
Size: 530028 Color: 0
Size: 469959 Color: 1

Bin 1070: 14 of cap free
Amount of items: 2
Items: 
Size: 539540 Color: 1
Size: 460447 Color: 0

Bin 1071: 14 of cap free
Amount of items: 2
Items: 
Size: 545413 Color: 0
Size: 454574 Color: 1

Bin 1072: 14 of cap free
Amount of items: 2
Items: 
Size: 549433 Color: 0
Size: 450554 Color: 1

Bin 1073: 14 of cap free
Amount of items: 2
Items: 
Size: 551284 Color: 0
Size: 448703 Color: 1

Bin 1074: 14 of cap free
Amount of items: 2
Items: 
Size: 562316 Color: 0
Size: 437671 Color: 1

Bin 1075: 14 of cap free
Amount of items: 3
Items: 
Size: 580146 Color: 0
Size: 250804 Color: 1
Size: 169037 Color: 0

Bin 1076: 14 of cap free
Amount of items: 2
Items: 
Size: 618661 Color: 0
Size: 381326 Color: 1

Bin 1077: 14 of cap free
Amount of items: 2
Items: 
Size: 632467 Color: 0
Size: 367520 Color: 1

Bin 1078: 14 of cap free
Amount of items: 2
Items: 
Size: 633215 Color: 1
Size: 366772 Color: 0

Bin 1079: 14 of cap free
Amount of items: 2
Items: 
Size: 643443 Color: 0
Size: 356544 Color: 1

Bin 1080: 14 of cap free
Amount of items: 2
Items: 
Size: 655019 Color: 1
Size: 344968 Color: 0

Bin 1081: 14 of cap free
Amount of items: 2
Items: 
Size: 664547 Color: 0
Size: 335440 Color: 1

Bin 1082: 14 of cap free
Amount of items: 2
Items: 
Size: 667270 Color: 1
Size: 332717 Color: 0

Bin 1083: 14 of cap free
Amount of items: 2
Items: 
Size: 669477 Color: 1
Size: 330510 Color: 0

Bin 1084: 14 of cap free
Amount of items: 2
Items: 
Size: 694941 Color: 0
Size: 305046 Color: 1

Bin 1085: 14 of cap free
Amount of items: 2
Items: 
Size: 704842 Color: 0
Size: 295145 Color: 1

Bin 1086: 14 of cap free
Amount of items: 2
Items: 
Size: 710517 Color: 1
Size: 289470 Color: 0

Bin 1087: 14 of cap free
Amount of items: 2
Items: 
Size: 712065 Color: 0
Size: 287922 Color: 1

Bin 1088: 14 of cap free
Amount of items: 2
Items: 
Size: 720182 Color: 0
Size: 279805 Color: 1

Bin 1089: 14 of cap free
Amount of items: 2
Items: 
Size: 720597 Color: 1
Size: 279390 Color: 0

Bin 1090: 14 of cap free
Amount of items: 2
Items: 
Size: 739221 Color: 1
Size: 260766 Color: 0

Bin 1091: 14 of cap free
Amount of items: 2
Items: 
Size: 780107 Color: 0
Size: 219880 Color: 1

Bin 1092: 15 of cap free
Amount of items: 3
Items: 
Size: 431045 Color: 0
Size: 429242 Color: 1
Size: 139699 Color: 1

Bin 1093: 15 of cap free
Amount of items: 5
Items: 
Size: 231769 Color: 1
Size: 193644 Color: 0
Size: 193575 Color: 0
Size: 190801 Color: 1
Size: 190197 Color: 1

Bin 1094: 15 of cap free
Amount of items: 3
Items: 
Size: 422983 Color: 0
Size: 407744 Color: 0
Size: 169259 Color: 1

Bin 1095: 15 of cap free
Amount of items: 3
Items: 
Size: 430749 Color: 0
Size: 410375 Color: 0
Size: 158862 Color: 1

Bin 1096: 15 of cap free
Amount of items: 2
Items: 
Size: 526596 Color: 1
Size: 473390 Color: 0

Bin 1097: 15 of cap free
Amount of items: 2
Items: 
Size: 541198 Color: 0
Size: 458788 Color: 1

Bin 1098: 15 of cap free
Amount of items: 2
Items: 
Size: 541512 Color: 1
Size: 458474 Color: 0

Bin 1099: 15 of cap free
Amount of items: 2
Items: 
Size: 544691 Color: 0
Size: 455295 Color: 1

Bin 1100: 15 of cap free
Amount of items: 2
Items: 
Size: 545473 Color: 0
Size: 454513 Color: 1

Bin 1101: 15 of cap free
Amount of items: 2
Items: 
Size: 546105 Color: 0
Size: 453881 Color: 1

Bin 1102: 15 of cap free
Amount of items: 2
Items: 
Size: 576990 Color: 1
Size: 422996 Color: 0

Bin 1103: 15 of cap free
Amount of items: 3
Items: 
Size: 579286 Color: 1
Size: 280030 Color: 0
Size: 140670 Color: 0

Bin 1104: 15 of cap free
Amount of items: 2
Items: 
Size: 582214 Color: 1
Size: 417772 Color: 0

Bin 1105: 15 of cap free
Amount of items: 2
Items: 
Size: 610778 Color: 0
Size: 389208 Color: 1

Bin 1106: 15 of cap free
Amount of items: 2
Items: 
Size: 623443 Color: 0
Size: 376543 Color: 1

Bin 1107: 15 of cap free
Amount of items: 2
Items: 
Size: 633984 Color: 0
Size: 366002 Color: 1

Bin 1108: 15 of cap free
Amount of items: 2
Items: 
Size: 652477 Color: 1
Size: 347509 Color: 0

Bin 1109: 15 of cap free
Amount of items: 2
Items: 
Size: 657842 Color: 1
Size: 342144 Color: 0

Bin 1110: 15 of cap free
Amount of items: 2
Items: 
Size: 662558 Color: 1
Size: 337428 Color: 0

Bin 1111: 15 of cap free
Amount of items: 2
Items: 
Size: 681426 Color: 1
Size: 318560 Color: 0

Bin 1112: 15 of cap free
Amount of items: 2
Items: 
Size: 699326 Color: 1
Size: 300660 Color: 0

Bin 1113: 15 of cap free
Amount of items: 2
Items: 
Size: 704475 Color: 0
Size: 295511 Color: 1

Bin 1114: 15 of cap free
Amount of items: 2
Items: 
Size: 753635 Color: 0
Size: 246351 Color: 1

Bin 1115: 15 of cap free
Amount of items: 2
Items: 
Size: 765313 Color: 0
Size: 234673 Color: 1

Bin 1116: 15 of cap free
Amount of items: 2
Items: 
Size: 793281 Color: 0
Size: 206705 Color: 1

Bin 1117: 16 of cap free
Amount of items: 2
Items: 
Size: 794136 Color: 1
Size: 205849 Color: 0

Bin 1118: 16 of cap free
Amount of items: 3
Items: 
Size: 626891 Color: 0
Size: 188393 Color: 1
Size: 184701 Color: 1

Bin 1119: 16 of cap free
Amount of items: 5
Items: 
Size: 313254 Color: 1
Size: 174139 Color: 0
Size: 173866 Color: 0
Size: 169435 Color: 1
Size: 169291 Color: 1

Bin 1120: 16 of cap free
Amount of items: 2
Items: 
Size: 515895 Color: 0
Size: 484090 Color: 1

Bin 1121: 16 of cap free
Amount of items: 2
Items: 
Size: 558888 Color: 1
Size: 441097 Color: 0

Bin 1122: 16 of cap free
Amount of items: 2
Items: 
Size: 561117 Color: 1
Size: 438868 Color: 0

Bin 1123: 16 of cap free
Amount of items: 2
Items: 
Size: 570986 Color: 1
Size: 428999 Color: 0

Bin 1124: 16 of cap free
Amount of items: 3
Items: 
Size: 574652 Color: 1
Size: 242949 Color: 1
Size: 182384 Color: 0

Bin 1125: 16 of cap free
Amount of items: 2
Items: 
Size: 578417 Color: 0
Size: 421568 Color: 1

Bin 1126: 16 of cap free
Amount of items: 2
Items: 
Size: 578924 Color: 0
Size: 421061 Color: 1

Bin 1127: 16 of cap free
Amount of items: 2
Items: 
Size: 581744 Color: 0
Size: 418241 Color: 1

Bin 1128: 16 of cap free
Amount of items: 2
Items: 
Size: 591528 Color: 0
Size: 408457 Color: 1

Bin 1129: 16 of cap free
Amount of items: 2
Items: 
Size: 609625 Color: 1
Size: 390360 Color: 0

Bin 1130: 16 of cap free
Amount of items: 2
Items: 
Size: 612321 Color: 0
Size: 387664 Color: 1

Bin 1131: 16 of cap free
Amount of items: 2
Items: 
Size: 623327 Color: 0
Size: 376658 Color: 1

Bin 1132: 16 of cap free
Amount of items: 2
Items: 
Size: 624601 Color: 1
Size: 375384 Color: 0

Bin 1133: 16 of cap free
Amount of items: 2
Items: 
Size: 640608 Color: 0
Size: 359377 Color: 1

Bin 1134: 16 of cap free
Amount of items: 2
Items: 
Size: 649357 Color: 0
Size: 350628 Color: 1

Bin 1135: 16 of cap free
Amount of items: 2
Items: 
Size: 666731 Color: 1
Size: 333254 Color: 0

Bin 1136: 16 of cap free
Amount of items: 2
Items: 
Size: 699159 Color: 1
Size: 300826 Color: 0

Bin 1137: 16 of cap free
Amount of items: 2
Items: 
Size: 731624 Color: 1
Size: 268361 Color: 0

Bin 1138: 16 of cap free
Amount of items: 2
Items: 
Size: 761413 Color: 1
Size: 238572 Color: 0

Bin 1139: 16 of cap free
Amount of items: 2
Items: 
Size: 762830 Color: 1
Size: 237155 Color: 0

Bin 1140: 16 of cap free
Amount of items: 2
Items: 
Size: 763451 Color: 1
Size: 236534 Color: 0

Bin 1141: 17 of cap free
Amount of items: 2
Items: 
Size: 538851 Color: 1
Size: 461133 Color: 0

Bin 1142: 17 of cap free
Amount of items: 3
Items: 
Size: 447093 Color: 0
Size: 429907 Color: 1
Size: 122984 Color: 1

Bin 1143: 17 of cap free
Amount of items: 2
Items: 
Size: 786278 Color: 1
Size: 213706 Color: 0

Bin 1144: 17 of cap free
Amount of items: 3
Items: 
Size: 432578 Color: 1
Size: 373581 Color: 0
Size: 193825 Color: 1

Bin 1145: 17 of cap free
Amount of items: 3
Items: 
Size: 524619 Color: 0
Size: 267792 Color: 1
Size: 207573 Color: 0

Bin 1146: 17 of cap free
Amount of items: 2
Items: 
Size: 534727 Color: 0
Size: 465257 Color: 1

Bin 1147: 17 of cap free
Amount of items: 2
Items: 
Size: 584459 Color: 0
Size: 415525 Color: 1

Bin 1148: 17 of cap free
Amount of items: 2
Items: 
Size: 601217 Color: 1
Size: 398767 Color: 0

Bin 1149: 17 of cap free
Amount of items: 2
Items: 
Size: 603913 Color: 1
Size: 396071 Color: 0

Bin 1150: 17 of cap free
Amount of items: 2
Items: 
Size: 630664 Color: 0
Size: 369320 Color: 1

Bin 1151: 17 of cap free
Amount of items: 2
Items: 
Size: 641260 Color: 1
Size: 358724 Color: 0

Bin 1152: 17 of cap free
Amount of items: 2
Items: 
Size: 664975 Color: 1
Size: 335009 Color: 0

Bin 1153: 17 of cap free
Amount of items: 2
Items: 
Size: 673632 Color: 0
Size: 326352 Color: 1

Bin 1154: 17 of cap free
Amount of items: 2
Items: 
Size: 701128 Color: 1
Size: 298856 Color: 0

Bin 1155: 17 of cap free
Amount of items: 2
Items: 
Size: 705237 Color: 0
Size: 294747 Color: 1

Bin 1156: 17 of cap free
Amount of items: 2
Items: 
Size: 736670 Color: 0
Size: 263314 Color: 1

Bin 1157: 17 of cap free
Amount of items: 2
Items: 
Size: 743430 Color: 0
Size: 256554 Color: 1

Bin 1158: 17 of cap free
Amount of items: 2
Items: 
Size: 751464 Color: 1
Size: 248520 Color: 0

Bin 1159: 17 of cap free
Amount of items: 2
Items: 
Size: 759649 Color: 0
Size: 240335 Color: 1

Bin 1160: 18 of cap free
Amount of items: 5
Items: 
Size: 241940 Color: 1
Size: 191410 Color: 1
Size: 189669 Color: 0
Size: 189479 Color: 0
Size: 187485 Color: 0

Bin 1161: 18 of cap free
Amount of items: 2
Items: 
Size: 522343 Color: 0
Size: 477640 Color: 1

Bin 1162: 18 of cap free
Amount of items: 2
Items: 
Size: 526744 Color: 0
Size: 473239 Color: 1

Bin 1163: 18 of cap free
Amount of items: 2
Items: 
Size: 538773 Color: 0
Size: 461210 Color: 1

Bin 1164: 18 of cap free
Amount of items: 2
Items: 
Size: 538880 Color: 1
Size: 461103 Color: 0

Bin 1165: 18 of cap free
Amount of items: 2
Items: 
Size: 539029 Color: 1
Size: 460954 Color: 0

Bin 1166: 18 of cap free
Amount of items: 2
Items: 
Size: 550693 Color: 1
Size: 449290 Color: 0

Bin 1167: 18 of cap free
Amount of items: 2
Items: 
Size: 579377 Color: 0
Size: 420606 Color: 1

Bin 1168: 18 of cap free
Amount of items: 2
Items: 
Size: 580882 Color: 0
Size: 419101 Color: 1

Bin 1169: 18 of cap free
Amount of items: 2
Items: 
Size: 581859 Color: 1
Size: 418124 Color: 0

Bin 1170: 18 of cap free
Amount of items: 2
Items: 
Size: 588931 Color: 1
Size: 411052 Color: 0

Bin 1171: 18 of cap free
Amount of items: 2
Items: 
Size: 591378 Color: 0
Size: 408605 Color: 1

Bin 1172: 18 of cap free
Amount of items: 2
Items: 
Size: 600283 Color: 1
Size: 399700 Color: 0

Bin 1173: 18 of cap free
Amount of items: 2
Items: 
Size: 625103 Color: 1
Size: 374880 Color: 0

Bin 1174: 18 of cap free
Amount of items: 2
Items: 
Size: 636591 Color: 0
Size: 363392 Color: 1

Bin 1175: 18 of cap free
Amount of items: 2
Items: 
Size: 638774 Color: 0
Size: 361209 Color: 1

Bin 1176: 18 of cap free
Amount of items: 2
Items: 
Size: 658404 Color: 0
Size: 341579 Color: 1

Bin 1177: 18 of cap free
Amount of items: 2
Items: 
Size: 662097 Color: 0
Size: 337886 Color: 1

Bin 1178: 18 of cap free
Amount of items: 2
Items: 
Size: 691561 Color: 0
Size: 308422 Color: 1

Bin 1179: 18 of cap free
Amount of items: 2
Items: 
Size: 704528 Color: 1
Size: 295455 Color: 0

Bin 1180: 18 of cap free
Amount of items: 2
Items: 
Size: 707031 Color: 0
Size: 292952 Color: 1

Bin 1181: 18 of cap free
Amount of items: 2
Items: 
Size: 709408 Color: 0
Size: 290575 Color: 1

Bin 1182: 18 of cap free
Amount of items: 2
Items: 
Size: 727883 Color: 1
Size: 272100 Color: 0

Bin 1183: 18 of cap free
Amount of items: 2
Items: 
Size: 742121 Color: 1
Size: 257862 Color: 0

Bin 1184: 18 of cap free
Amount of items: 2
Items: 
Size: 767655 Color: 1
Size: 232328 Color: 0

Bin 1185: 19 of cap free
Amount of items: 3
Items: 
Size: 444026 Color: 0
Size: 429001 Color: 1
Size: 126955 Color: 1

Bin 1186: 19 of cap free
Amount of items: 2
Items: 
Size: 502717 Color: 0
Size: 497265 Color: 1

Bin 1187: 19 of cap free
Amount of items: 2
Items: 
Size: 509507 Color: 0
Size: 490475 Color: 1

Bin 1188: 19 of cap free
Amount of items: 3
Items: 
Size: 518304 Color: 0
Size: 318118 Color: 1
Size: 163560 Color: 0

Bin 1189: 19 of cap free
Amount of items: 2
Items: 
Size: 522377 Color: 0
Size: 477605 Color: 1

Bin 1190: 19 of cap free
Amount of items: 2
Items: 
Size: 527263 Color: 0
Size: 472719 Color: 1

Bin 1191: 19 of cap free
Amount of items: 2
Items: 
Size: 529422 Color: 1
Size: 470560 Color: 0

Bin 1192: 19 of cap free
Amount of items: 2
Items: 
Size: 534064 Color: 0
Size: 465918 Color: 1

Bin 1193: 19 of cap free
Amount of items: 2
Items: 
Size: 546530 Color: 1
Size: 453452 Color: 0

Bin 1194: 19 of cap free
Amount of items: 2
Items: 
Size: 563031 Color: 1
Size: 436951 Color: 0

Bin 1195: 19 of cap free
Amount of items: 2
Items: 
Size: 563325 Color: 0
Size: 436657 Color: 1

Bin 1196: 19 of cap free
Amount of items: 2
Items: 
Size: 605468 Color: 1
Size: 394514 Color: 0

Bin 1197: 19 of cap free
Amount of items: 2
Items: 
Size: 618334 Color: 0
Size: 381648 Color: 1

Bin 1198: 19 of cap free
Amount of items: 2
Items: 
Size: 620900 Color: 1
Size: 379082 Color: 0

Bin 1199: 19 of cap free
Amount of items: 2
Items: 
Size: 635570 Color: 1
Size: 364412 Color: 0

Bin 1200: 19 of cap free
Amount of items: 2
Items: 
Size: 637914 Color: 0
Size: 362068 Color: 1

Bin 1201: 19 of cap free
Amount of items: 2
Items: 
Size: 675017 Color: 1
Size: 324965 Color: 0

Bin 1202: 19 of cap free
Amount of items: 2
Items: 
Size: 695013 Color: 0
Size: 304969 Color: 1

Bin 1203: 19 of cap free
Amount of items: 2
Items: 
Size: 716642 Color: 1
Size: 283340 Color: 0

Bin 1204: 19 of cap free
Amount of items: 2
Items: 
Size: 732447 Color: 1
Size: 267535 Color: 0

Bin 1205: 19 of cap free
Amount of items: 2
Items: 
Size: 751240 Color: 0
Size: 248742 Color: 1

Bin 1206: 19 of cap free
Amount of items: 2
Items: 
Size: 756857 Color: 0
Size: 243125 Color: 1

Bin 1207: 19 of cap free
Amount of items: 2
Items: 
Size: 762577 Color: 1
Size: 237405 Color: 0

Bin 1208: 19 of cap free
Amount of items: 2
Items: 
Size: 765338 Color: 1
Size: 234644 Color: 0

Bin 1209: 19 of cap free
Amount of items: 2
Items: 
Size: 796838 Color: 0
Size: 203144 Color: 1

Bin 1210: 20 of cap free
Amount of items: 5
Items: 
Size: 304757 Color: 0
Size: 180713 Color: 1
Size: 172956 Color: 0
Size: 172856 Color: 0
Size: 168699 Color: 1

Bin 1211: 20 of cap free
Amount of items: 2
Items: 
Size: 500360 Color: 1
Size: 499621 Color: 0

Bin 1212: 20 of cap free
Amount of items: 2
Items: 
Size: 517781 Color: 0
Size: 482200 Color: 1

Bin 1213: 20 of cap free
Amount of items: 2
Items: 
Size: 524062 Color: 0
Size: 475919 Color: 1

Bin 1214: 20 of cap free
Amount of items: 2
Items: 
Size: 544713 Color: 1
Size: 455268 Color: 0

Bin 1215: 20 of cap free
Amount of items: 2
Items: 
Size: 573302 Color: 1
Size: 426679 Color: 0

Bin 1216: 20 of cap free
Amount of items: 2
Items: 
Size: 575476 Color: 1
Size: 424505 Color: 0

Bin 1217: 20 of cap free
Amount of items: 2
Items: 
Size: 618045 Color: 0
Size: 381936 Color: 1

Bin 1218: 20 of cap free
Amount of items: 2
Items: 
Size: 632685 Color: 1
Size: 367296 Color: 0

Bin 1219: 20 of cap free
Amount of items: 2
Items: 
Size: 651494 Color: 1
Size: 348487 Color: 0

Bin 1220: 20 of cap free
Amount of items: 2
Items: 
Size: 659584 Color: 0
Size: 340397 Color: 1

Bin 1221: 20 of cap free
Amount of items: 2
Items: 
Size: 666832 Color: 1
Size: 333149 Color: 0

Bin 1222: 20 of cap free
Amount of items: 2
Items: 
Size: 667137 Color: 0
Size: 332844 Color: 1

Bin 1223: 20 of cap free
Amount of items: 2
Items: 
Size: 693997 Color: 0
Size: 305984 Color: 1

Bin 1224: 20 of cap free
Amount of items: 2
Items: 
Size: 704357 Color: 1
Size: 295624 Color: 0

Bin 1225: 20 of cap free
Amount of items: 2
Items: 
Size: 705725 Color: 1
Size: 294256 Color: 0

Bin 1226: 20 of cap free
Amount of items: 2
Items: 
Size: 724393 Color: 0
Size: 275588 Color: 1

Bin 1227: 20 of cap free
Amount of items: 2
Items: 
Size: 745689 Color: 1
Size: 254292 Color: 0

Bin 1228: 20 of cap free
Amount of items: 2
Items: 
Size: 764272 Color: 1
Size: 235709 Color: 0

Bin 1229: 20 of cap free
Amount of items: 2
Items: 
Size: 765294 Color: 1
Size: 234687 Color: 0

Bin 1230: 20 of cap free
Amount of items: 2
Items: 
Size: 768560 Color: 0
Size: 231421 Color: 1

Bin 1231: 20 of cap free
Amount of items: 2
Items: 
Size: 780780 Color: 0
Size: 219201 Color: 1

Bin 1232: 20 of cap free
Amount of items: 2
Items: 
Size: 783653 Color: 0
Size: 216328 Color: 1

Bin 1233: 21 of cap free
Amount of items: 3
Items: 
Size: 695728 Color: 0
Size: 171573 Color: 1
Size: 132679 Color: 0

Bin 1234: 21 of cap free
Amount of items: 3
Items: 
Size: 429042 Color: 1
Size: 367985 Color: 0
Size: 202953 Color: 1

Bin 1235: 21 of cap free
Amount of items: 3
Items: 
Size: 481320 Color: 0
Size: 412734 Color: 0
Size: 105926 Color: 1

Bin 1236: 21 of cap free
Amount of items: 5
Items: 
Size: 205311 Color: 0
Size: 205067 Color: 0
Size: 199082 Color: 1
Size: 195417 Color: 1
Size: 195103 Color: 1

Bin 1237: 21 of cap free
Amount of items: 3
Items: 
Size: 458877 Color: 0
Size: 411410 Color: 0
Size: 129693 Color: 1

Bin 1238: 21 of cap free
Amount of items: 2
Items: 
Size: 533012 Color: 0
Size: 466968 Color: 1

Bin 1239: 21 of cap free
Amount of items: 2
Items: 
Size: 581193 Color: 0
Size: 418787 Color: 1

Bin 1240: 21 of cap free
Amount of items: 2
Items: 
Size: 599515 Color: 0
Size: 400465 Color: 1

Bin 1241: 21 of cap free
Amount of items: 2
Items: 
Size: 630322 Color: 1
Size: 369658 Color: 0

Bin 1242: 21 of cap free
Amount of items: 2
Items: 
Size: 632677 Color: 0
Size: 367303 Color: 1

Bin 1243: 21 of cap free
Amount of items: 2
Items: 
Size: 652877 Color: 1
Size: 347103 Color: 0

Bin 1244: 21 of cap free
Amount of items: 2
Items: 
Size: 658030 Color: 1
Size: 341950 Color: 0

Bin 1245: 21 of cap free
Amount of items: 2
Items: 
Size: 664569 Color: 0
Size: 335411 Color: 1

Bin 1246: 21 of cap free
Amount of items: 2
Items: 
Size: 672075 Color: 0
Size: 327905 Color: 1

Bin 1247: 21 of cap free
Amount of items: 2
Items: 
Size: 726344 Color: 1
Size: 273636 Color: 0

Bin 1248: 21 of cap free
Amount of items: 2
Items: 
Size: 744253 Color: 1
Size: 255727 Color: 0

Bin 1249: 21 of cap free
Amount of items: 2
Items: 
Size: 760806 Color: 1
Size: 239174 Color: 0

Bin 1250: 22 of cap free
Amount of items: 3
Items: 
Size: 380362 Color: 1
Size: 352690 Color: 0
Size: 266927 Color: 0

Bin 1251: 22 of cap free
Amount of items: 3
Items: 
Size: 497207 Color: 0
Size: 382855 Color: 1
Size: 119917 Color: 1

Bin 1252: 22 of cap free
Amount of items: 2
Items: 
Size: 508709 Color: 0
Size: 491270 Color: 1

Bin 1253: 22 of cap free
Amount of items: 2
Items: 
Size: 524826 Color: 0
Size: 475153 Color: 1

Bin 1254: 22 of cap free
Amount of items: 2
Items: 
Size: 539432 Color: 0
Size: 460547 Color: 1

Bin 1255: 22 of cap free
Amount of items: 2
Items: 
Size: 555507 Color: 1
Size: 444472 Color: 0

Bin 1256: 22 of cap free
Amount of items: 3
Items: 
Size: 559141 Color: 1
Size: 306836 Color: 1
Size: 134002 Color: 0

Bin 1257: 22 of cap free
Amount of items: 2
Items: 
Size: 562567 Color: 0
Size: 437412 Color: 1

Bin 1258: 22 of cap free
Amount of items: 2
Items: 
Size: 565393 Color: 0
Size: 434586 Color: 1

Bin 1259: 22 of cap free
Amount of items: 2
Items: 
Size: 592004 Color: 1
Size: 407975 Color: 0

Bin 1260: 22 of cap free
Amount of items: 2
Items: 
Size: 592304 Color: 0
Size: 407675 Color: 1

Bin 1261: 22 of cap free
Amount of items: 2
Items: 
Size: 593705 Color: 1
Size: 406274 Color: 0

Bin 1262: 22 of cap free
Amount of items: 2
Items: 
Size: 601349 Color: 1
Size: 398630 Color: 0

Bin 1263: 22 of cap free
Amount of items: 2
Items: 
Size: 614487 Color: 0
Size: 385492 Color: 1

Bin 1264: 22 of cap free
Amount of items: 2
Items: 
Size: 624604 Color: 0
Size: 375375 Color: 1

Bin 1265: 22 of cap free
Amount of items: 2
Items: 
Size: 631238 Color: 1
Size: 368741 Color: 0

Bin 1266: 22 of cap free
Amount of items: 2
Items: 
Size: 656695 Color: 0
Size: 343284 Color: 1

Bin 1267: 22 of cap free
Amount of items: 2
Items: 
Size: 657183 Color: 0
Size: 342796 Color: 1

Bin 1268: 22 of cap free
Amount of items: 2
Items: 
Size: 668885 Color: 0
Size: 331094 Color: 1

Bin 1269: 22 of cap free
Amount of items: 2
Items: 
Size: 708020 Color: 0
Size: 291959 Color: 1

Bin 1270: 22 of cap free
Amount of items: 2
Items: 
Size: 718807 Color: 0
Size: 281172 Color: 1

Bin 1271: 22 of cap free
Amount of items: 2
Items: 
Size: 723903 Color: 0
Size: 276076 Color: 1

Bin 1272: 22 of cap free
Amount of items: 2
Items: 
Size: 729714 Color: 1
Size: 270265 Color: 0

Bin 1273: 22 of cap free
Amount of items: 2
Items: 
Size: 745769 Color: 0
Size: 254210 Color: 1

Bin 1274: 22 of cap free
Amount of items: 2
Items: 
Size: 784967 Color: 1
Size: 215012 Color: 0

Bin 1275: 22 of cap free
Amount of items: 2
Items: 
Size: 792150 Color: 1
Size: 207829 Color: 0

Bin 1276: 23 of cap free
Amount of items: 2
Items: 
Size: 574864 Color: 1
Size: 425114 Color: 0

Bin 1277: 23 of cap free
Amount of items: 5
Items: 
Size: 259462 Color: 0
Size: 186120 Color: 0
Size: 186040 Color: 0
Size: 184264 Color: 1
Size: 184092 Color: 1

Bin 1278: 23 of cap free
Amount of items: 2
Items: 
Size: 770950 Color: 1
Size: 229028 Color: 0

Bin 1279: 23 of cap free
Amount of items: 2
Items: 
Size: 505243 Color: 1
Size: 494735 Color: 0

Bin 1280: 23 of cap free
Amount of items: 2
Items: 
Size: 532147 Color: 1
Size: 467831 Color: 0

Bin 1281: 23 of cap free
Amount of items: 2
Items: 
Size: 538950 Color: 0
Size: 461028 Color: 1

Bin 1282: 23 of cap free
Amount of items: 2
Items: 
Size: 545423 Color: 1
Size: 454555 Color: 0

Bin 1283: 23 of cap free
Amount of items: 2
Items: 
Size: 590397 Color: 0
Size: 409581 Color: 1

Bin 1284: 23 of cap free
Amount of items: 2
Items: 
Size: 590511 Color: 1
Size: 409467 Color: 0

Bin 1285: 23 of cap free
Amount of items: 2
Items: 
Size: 610497 Color: 1
Size: 389481 Color: 0

Bin 1286: 23 of cap free
Amount of items: 2
Items: 
Size: 614714 Color: 1
Size: 385264 Color: 0

Bin 1287: 23 of cap free
Amount of items: 2
Items: 
Size: 642966 Color: 0
Size: 357012 Color: 1

Bin 1288: 23 of cap free
Amount of items: 2
Items: 
Size: 645763 Color: 0
Size: 354215 Color: 1

Bin 1289: 23 of cap free
Amount of items: 2
Items: 
Size: 659078 Color: 1
Size: 340900 Color: 0

Bin 1290: 23 of cap free
Amount of items: 2
Items: 
Size: 694944 Color: 1
Size: 305034 Color: 0

Bin 1291: 23 of cap free
Amount of items: 2
Items: 
Size: 698211 Color: 1
Size: 301767 Color: 0

Bin 1292: 23 of cap free
Amount of items: 2
Items: 
Size: 706096 Color: 0
Size: 293882 Color: 1

Bin 1293: 23 of cap free
Amount of items: 2
Items: 
Size: 722406 Color: 0
Size: 277572 Color: 1

Bin 1294: 23 of cap free
Amount of items: 2
Items: 
Size: 730850 Color: 0
Size: 269128 Color: 1

Bin 1295: 23 of cap free
Amount of items: 2
Items: 
Size: 758226 Color: 0
Size: 241752 Color: 1

Bin 1296: 23 of cap free
Amount of items: 2
Items: 
Size: 793658 Color: 0
Size: 206320 Color: 1

Bin 1297: 24 of cap free
Amount of items: 3
Items: 
Size: 559341 Color: 1
Size: 278617 Color: 0
Size: 162019 Color: 1

Bin 1298: 24 of cap free
Amount of items: 6
Items: 
Size: 169548 Color: 0
Size: 169423 Color: 0
Size: 169360 Color: 0
Size: 165532 Color: 1
Size: 164400 Color: 1
Size: 161714 Color: 1

Bin 1299: 24 of cap free
Amount of items: 3
Items: 
Size: 446875 Color: 0
Size: 396629 Color: 0
Size: 156473 Color: 1

Bin 1300: 24 of cap free
Amount of items: 2
Items: 
Size: 501045 Color: 0
Size: 498932 Color: 1

Bin 1301: 24 of cap free
Amount of items: 2
Items: 
Size: 507697 Color: 1
Size: 492280 Color: 0

Bin 1302: 24 of cap free
Amount of items: 2
Items: 
Size: 513728 Color: 0
Size: 486249 Color: 1

Bin 1303: 24 of cap free
Amount of items: 2
Items: 
Size: 536879 Color: 1
Size: 463098 Color: 0

Bin 1304: 24 of cap free
Amount of items: 2
Items: 
Size: 552180 Color: 0
Size: 447797 Color: 1

Bin 1305: 24 of cap free
Amount of items: 2
Items: 
Size: 578554 Color: 1
Size: 421423 Color: 0

Bin 1306: 24 of cap free
Amount of items: 2
Items: 
Size: 592608 Color: 1
Size: 407369 Color: 0

Bin 1307: 24 of cap free
Amount of items: 2
Items: 
Size: 605909 Color: 1
Size: 394068 Color: 0

Bin 1308: 24 of cap free
Amount of items: 2
Items: 
Size: 607333 Color: 1
Size: 392644 Color: 0

Bin 1309: 24 of cap free
Amount of items: 2
Items: 
Size: 620558 Color: 1
Size: 379419 Color: 0

Bin 1310: 24 of cap free
Amount of items: 2
Items: 
Size: 636797 Color: 1
Size: 363180 Color: 0

Bin 1311: 24 of cap free
Amount of items: 2
Items: 
Size: 647468 Color: 0
Size: 352509 Color: 1

Bin 1312: 24 of cap free
Amount of items: 2
Items: 
Size: 697199 Color: 0
Size: 302778 Color: 1

Bin 1313: 24 of cap free
Amount of items: 2
Items: 
Size: 697465 Color: 0
Size: 302512 Color: 1

Bin 1314: 24 of cap free
Amount of items: 2
Items: 
Size: 704613 Color: 1
Size: 295364 Color: 0

Bin 1315: 24 of cap free
Amount of items: 2
Items: 
Size: 709652 Color: 0
Size: 290325 Color: 1

Bin 1316: 24 of cap free
Amount of items: 2
Items: 
Size: 753572 Color: 0
Size: 246405 Color: 1

Bin 1317: 24 of cap free
Amount of items: 2
Items: 
Size: 777404 Color: 1
Size: 222573 Color: 0

Bin 1318: 24 of cap free
Amount of items: 2
Items: 
Size: 783275 Color: 1
Size: 216702 Color: 0

Bin 1319: 25 of cap free
Amount of items: 2
Items: 
Size: 723693 Color: 0
Size: 276283 Color: 1

Bin 1320: 25 of cap free
Amount of items: 2
Items: 
Size: 503310 Color: 1
Size: 496666 Color: 0

Bin 1321: 25 of cap free
Amount of items: 2
Items: 
Size: 503413 Color: 1
Size: 496563 Color: 0

Bin 1322: 25 of cap free
Amount of items: 2
Items: 
Size: 508785 Color: 1
Size: 491191 Color: 0

Bin 1323: 25 of cap free
Amount of items: 2
Items: 
Size: 515391 Color: 1
Size: 484585 Color: 0

Bin 1324: 25 of cap free
Amount of items: 2
Items: 
Size: 518678 Color: 0
Size: 481298 Color: 1

Bin 1325: 25 of cap free
Amount of items: 2
Items: 
Size: 576259 Color: 1
Size: 423717 Color: 0

Bin 1326: 25 of cap free
Amount of items: 2
Items: 
Size: 598433 Color: 0
Size: 401543 Color: 1

Bin 1327: 25 of cap free
Amount of items: 2
Items: 
Size: 600871 Color: 0
Size: 399105 Color: 1

Bin 1328: 25 of cap free
Amount of items: 2
Items: 
Size: 606531 Color: 0
Size: 393445 Color: 1

Bin 1329: 25 of cap free
Amount of items: 2
Items: 
Size: 606767 Color: 1
Size: 393209 Color: 0

Bin 1330: 25 of cap free
Amount of items: 2
Items: 
Size: 631180 Color: 0
Size: 368796 Color: 1

Bin 1331: 25 of cap free
Amount of items: 2
Items: 
Size: 636059 Color: 0
Size: 363917 Color: 1

Bin 1332: 25 of cap free
Amount of items: 2
Items: 
Size: 659157 Color: 0
Size: 340819 Color: 1

Bin 1333: 25 of cap free
Amount of items: 2
Items: 
Size: 663796 Color: 1
Size: 336180 Color: 0

Bin 1334: 25 of cap free
Amount of items: 2
Items: 
Size: 686966 Color: 1
Size: 313010 Color: 0

Bin 1335: 25 of cap free
Amount of items: 2
Items: 
Size: 717221 Color: 1
Size: 282755 Color: 0

Bin 1336: 25 of cap free
Amount of items: 2
Items: 
Size: 722494 Color: 1
Size: 277482 Color: 0

Bin 1337: 25 of cap free
Amount of items: 2
Items: 
Size: 723827 Color: 0
Size: 276149 Color: 1

Bin 1338: 25 of cap free
Amount of items: 2
Items: 
Size: 724481 Color: 1
Size: 275495 Color: 0

Bin 1339: 25 of cap free
Amount of items: 2
Items: 
Size: 730805 Color: 0
Size: 269171 Color: 1

Bin 1340: 25 of cap free
Amount of items: 2
Items: 
Size: 777707 Color: 1
Size: 222269 Color: 0

Bin 1341: 25 of cap free
Amount of items: 2
Items: 
Size: 797053 Color: 0
Size: 202923 Color: 1

Bin 1342: 26 of cap free
Amount of items: 2
Items: 
Size: 512701 Color: 1
Size: 487274 Color: 0

Bin 1343: 26 of cap free
Amount of items: 2
Items: 
Size: 514530 Color: 1
Size: 485445 Color: 0

Bin 1344: 26 of cap free
Amount of items: 2
Items: 
Size: 517704 Color: 1
Size: 482271 Color: 0

Bin 1345: 26 of cap free
Amount of items: 2
Items: 
Size: 527575 Color: 0
Size: 472400 Color: 1

Bin 1346: 26 of cap free
Amount of items: 2
Items: 
Size: 546442 Color: 0
Size: 453533 Color: 1

Bin 1347: 26 of cap free
Amount of items: 2
Items: 
Size: 583808 Color: 1
Size: 416167 Color: 0

Bin 1348: 26 of cap free
Amount of items: 2
Items: 
Size: 631253 Color: 0
Size: 368722 Color: 1

Bin 1349: 26 of cap free
Amount of items: 2
Items: 
Size: 632513 Color: 1
Size: 367462 Color: 0

Bin 1350: 26 of cap free
Amount of items: 2
Items: 
Size: 640109 Color: 0
Size: 359866 Color: 1

Bin 1351: 26 of cap free
Amount of items: 2
Items: 
Size: 679096 Color: 0
Size: 320879 Color: 1

Bin 1352: 26 of cap free
Amount of items: 2
Items: 
Size: 689344 Color: 0
Size: 310631 Color: 1

Bin 1353: 26 of cap free
Amount of items: 2
Items: 
Size: 709948 Color: 1
Size: 290027 Color: 0

Bin 1354: 26 of cap free
Amount of items: 2
Items: 
Size: 710895 Color: 1
Size: 289080 Color: 0

Bin 1355: 26 of cap free
Amount of items: 2
Items: 
Size: 744443 Color: 1
Size: 255532 Color: 0

Bin 1356: 26 of cap free
Amount of items: 2
Items: 
Size: 745283 Color: 0
Size: 254692 Color: 1

Bin 1357: 26 of cap free
Amount of items: 2
Items: 
Size: 746079 Color: 1
Size: 253896 Color: 0

Bin 1358: 26 of cap free
Amount of items: 2
Items: 
Size: 780470 Color: 0
Size: 219505 Color: 1

Bin 1359: 26 of cap free
Amount of items: 2
Items: 
Size: 788499 Color: 1
Size: 211476 Color: 0

Bin 1360: 27 of cap free
Amount of items: 2
Items: 
Size: 692778 Color: 0
Size: 307196 Color: 1

Bin 1361: 27 of cap free
Amount of items: 2
Items: 
Size: 680023 Color: 1
Size: 319951 Color: 0

Bin 1362: 27 of cap free
Amount of items: 2
Items: 
Size: 521120 Color: 0
Size: 478854 Color: 1

Bin 1363: 27 of cap free
Amount of items: 2
Items: 
Size: 530503 Color: 0
Size: 469471 Color: 1

Bin 1364: 27 of cap free
Amount of items: 2
Items: 
Size: 532147 Color: 1
Size: 467827 Color: 0

Bin 1365: 27 of cap free
Amount of items: 2
Items: 
Size: 569869 Color: 0
Size: 430105 Color: 1

Bin 1366: 27 of cap free
Amount of items: 2
Items: 
Size: 570004 Color: 0
Size: 429970 Color: 1

Bin 1367: 27 of cap free
Amount of items: 2
Items: 
Size: 593279 Color: 0
Size: 406695 Color: 1

Bin 1368: 27 of cap free
Amount of items: 2
Items: 
Size: 601751 Color: 0
Size: 398223 Color: 1

Bin 1369: 27 of cap free
Amount of items: 2
Items: 
Size: 615653 Color: 0
Size: 384321 Color: 1

Bin 1370: 27 of cap free
Amount of items: 2
Items: 
Size: 623952 Color: 1
Size: 376022 Color: 0

Bin 1371: 27 of cap free
Amount of items: 2
Items: 
Size: 642095 Color: 0
Size: 357879 Color: 1

Bin 1372: 27 of cap free
Amount of items: 2
Items: 
Size: 646557 Color: 0
Size: 353417 Color: 1

Bin 1373: 27 of cap free
Amount of items: 2
Items: 
Size: 674073 Color: 1
Size: 325901 Color: 0

Bin 1374: 27 of cap free
Amount of items: 2
Items: 
Size: 683644 Color: 1
Size: 316330 Color: 0

Bin 1375: 27 of cap free
Amount of items: 2
Items: 
Size: 687408 Color: 1
Size: 312566 Color: 0

Bin 1376: 27 of cap free
Amount of items: 2
Items: 
Size: 695504 Color: 0
Size: 304470 Color: 1

Bin 1377: 27 of cap free
Amount of items: 2
Items: 
Size: 730964 Color: 0
Size: 269010 Color: 1

Bin 1378: 27 of cap free
Amount of items: 2
Items: 
Size: 743353 Color: 1
Size: 256621 Color: 0

Bin 1379: 27 of cap free
Amount of items: 2
Items: 
Size: 744770 Color: 1
Size: 255204 Color: 0

Bin 1380: 27 of cap free
Amount of items: 2
Items: 
Size: 744981 Color: 0
Size: 254993 Color: 1

Bin 1381: 27 of cap free
Amount of items: 2
Items: 
Size: 752713 Color: 0
Size: 247261 Color: 1

Bin 1382: 27 of cap free
Amount of items: 2
Items: 
Size: 770230 Color: 1
Size: 229744 Color: 0

Bin 1383: 27 of cap free
Amount of items: 2
Items: 
Size: 770719 Color: 1
Size: 229255 Color: 0

Bin 1384: 27 of cap free
Amount of items: 2
Items: 
Size: 781200 Color: 0
Size: 218774 Color: 1

Bin 1385: 27 of cap free
Amount of items: 2
Items: 
Size: 793942 Color: 1
Size: 206032 Color: 0

Bin 1386: 28 of cap free
Amount of items: 2
Items: 
Size: 692474 Color: 1
Size: 307499 Color: 0

Bin 1387: 28 of cap free
Amount of items: 5
Items: 
Size: 260482 Color: 1
Size: 223279 Color: 1
Size: 209136 Color: 0
Size: 153872 Color: 1
Size: 153204 Color: 0

Bin 1388: 28 of cap free
Amount of items: 2
Items: 
Size: 795502 Color: 1
Size: 204471 Color: 0

Bin 1389: 28 of cap free
Amount of items: 2
Items: 
Size: 500773 Color: 0
Size: 499200 Color: 1

Bin 1390: 28 of cap free
Amount of items: 2
Items: 
Size: 504183 Color: 1
Size: 495790 Color: 0

Bin 1391: 28 of cap free
Amount of items: 2
Items: 
Size: 522355 Color: 1
Size: 477618 Color: 0

Bin 1392: 28 of cap free
Amount of items: 2
Items: 
Size: 539929 Color: 0
Size: 460044 Color: 1

Bin 1393: 28 of cap free
Amount of items: 2
Items: 
Size: 554136 Color: 0
Size: 445837 Color: 1

Bin 1394: 28 of cap free
Amount of items: 2
Items: 
Size: 567298 Color: 1
Size: 432675 Color: 0

Bin 1395: 28 of cap free
Amount of items: 2
Items: 
Size: 577647 Color: 0
Size: 422326 Color: 1

Bin 1396: 28 of cap free
Amount of items: 2
Items: 
Size: 578794 Color: 0
Size: 421179 Color: 1

Bin 1397: 28 of cap free
Amount of items: 2
Items: 
Size: 585789 Color: 1
Size: 414184 Color: 0

Bin 1398: 28 of cap free
Amount of items: 2
Items: 
Size: 604783 Color: 1
Size: 395190 Color: 0

Bin 1399: 28 of cap free
Amount of items: 2
Items: 
Size: 614688 Color: 0
Size: 385285 Color: 1

Bin 1400: 28 of cap free
Amount of items: 2
Items: 
Size: 630193 Color: 1
Size: 369780 Color: 0

Bin 1401: 28 of cap free
Amount of items: 2
Items: 
Size: 647534 Color: 1
Size: 352439 Color: 0

Bin 1402: 28 of cap free
Amount of items: 2
Items: 
Size: 704435 Color: 1
Size: 295538 Color: 0

Bin 1403: 28 of cap free
Amount of items: 2
Items: 
Size: 714103 Color: 1
Size: 285870 Color: 0

Bin 1404: 28 of cap free
Amount of items: 2
Items: 
Size: 733581 Color: 1
Size: 266392 Color: 0

Bin 1405: 28 of cap free
Amount of items: 2
Items: 
Size: 789382 Color: 0
Size: 210591 Color: 1

Bin 1406: 28 of cap free
Amount of items: 2
Items: 
Size: 793101 Color: 1
Size: 206872 Color: 0

Bin 1407: 29 of cap free
Amount of items: 2
Items: 
Size: 589108 Color: 0
Size: 410864 Color: 1

Bin 1408: 29 of cap free
Amount of items: 3
Items: 
Size: 477362 Color: 0
Size: 407873 Color: 0
Size: 114737 Color: 1

Bin 1409: 29 of cap free
Amount of items: 2
Items: 
Size: 526002 Color: 1
Size: 473970 Color: 0

Bin 1410: 29 of cap free
Amount of items: 2
Items: 
Size: 526675 Color: 0
Size: 473297 Color: 1

Bin 1411: 29 of cap free
Amount of items: 2
Items: 
Size: 539251 Color: 1
Size: 460721 Color: 0

Bin 1412: 29 of cap free
Amount of items: 2
Items: 
Size: 540939 Color: 1
Size: 459033 Color: 0

Bin 1413: 29 of cap free
Amount of items: 2
Items: 
Size: 551362 Color: 0
Size: 448610 Color: 1

Bin 1414: 29 of cap free
Amount of items: 2
Items: 
Size: 561226 Color: 1
Size: 438746 Color: 0

Bin 1415: 29 of cap free
Amount of items: 2
Items: 
Size: 566357 Color: 1
Size: 433615 Color: 0

Bin 1416: 29 of cap free
Amount of items: 2
Items: 
Size: 573464 Color: 0
Size: 426508 Color: 1

Bin 1417: 29 of cap free
Amount of items: 2
Items: 
Size: 613670 Color: 0
Size: 386302 Color: 1

Bin 1418: 29 of cap free
Amount of items: 2
Items: 
Size: 616570 Color: 0
Size: 383402 Color: 1

Bin 1419: 29 of cap free
Amount of items: 2
Items: 
Size: 619781 Color: 1
Size: 380191 Color: 0

Bin 1420: 29 of cap free
Amount of items: 2
Items: 
Size: 625527 Color: 0
Size: 374445 Color: 1

Bin 1421: 29 of cap free
Amount of items: 2
Items: 
Size: 651283 Color: 1
Size: 348689 Color: 0

Bin 1422: 29 of cap free
Amount of items: 2
Items: 
Size: 656802 Color: 1
Size: 343170 Color: 0

Bin 1423: 29 of cap free
Amount of items: 2
Items: 
Size: 656853 Color: 1
Size: 343119 Color: 0

Bin 1424: 29 of cap free
Amount of items: 2
Items: 
Size: 660257 Color: 1
Size: 339715 Color: 0

Bin 1425: 29 of cap free
Amount of items: 2
Items: 
Size: 660929 Color: 1
Size: 339043 Color: 0

Bin 1426: 29 of cap free
Amount of items: 2
Items: 
Size: 668130 Color: 1
Size: 331842 Color: 0

Bin 1427: 29 of cap free
Amount of items: 2
Items: 
Size: 683198 Color: 0
Size: 316774 Color: 1

Bin 1428: 29 of cap free
Amount of items: 2
Items: 
Size: 702809 Color: 0
Size: 297163 Color: 1

Bin 1429: 29 of cap free
Amount of items: 2
Items: 
Size: 729212 Color: 1
Size: 270760 Color: 0

Bin 1430: 29 of cap free
Amount of items: 2
Items: 
Size: 739302 Color: 1
Size: 260670 Color: 0

Bin 1431: 29 of cap free
Amount of items: 2
Items: 
Size: 778023 Color: 1
Size: 221949 Color: 0

Bin 1432: 30 of cap free
Amount of items: 3
Items: 
Size: 707264 Color: 1
Size: 187750 Color: 1
Size: 104957 Color: 0

Bin 1433: 30 of cap free
Amount of items: 2
Items: 
Size: 512790 Color: 1
Size: 487181 Color: 0

Bin 1434: 30 of cap free
Amount of items: 2
Items: 
Size: 526885 Color: 0
Size: 473086 Color: 1

Bin 1435: 30 of cap free
Amount of items: 2
Items: 
Size: 537003 Color: 0
Size: 462968 Color: 1

Bin 1436: 30 of cap free
Amount of items: 2
Items: 
Size: 543084 Color: 0
Size: 456887 Color: 1

Bin 1437: 30 of cap free
Amount of items: 2
Items: 
Size: 548677 Color: 1
Size: 451294 Color: 0

Bin 1438: 30 of cap free
Amount of items: 2
Items: 
Size: 556717 Color: 0
Size: 443254 Color: 1

Bin 1439: 30 of cap free
Amount of items: 2
Items: 
Size: 567510 Color: 1
Size: 432461 Color: 0

Bin 1440: 30 of cap free
Amount of items: 2
Items: 
Size: 581481 Color: 1
Size: 418490 Color: 0

Bin 1441: 30 of cap free
Amount of items: 2
Items: 
Size: 632377 Color: 0
Size: 367594 Color: 1

Bin 1442: 30 of cap free
Amount of items: 2
Items: 
Size: 640373 Color: 0
Size: 359598 Color: 1

Bin 1443: 30 of cap free
Amount of items: 2
Items: 
Size: 680458 Color: 0
Size: 319513 Color: 1

Bin 1444: 30 of cap free
Amount of items: 2
Items: 
Size: 680892 Color: 0
Size: 319079 Color: 1

Bin 1445: 30 of cap free
Amount of items: 2
Items: 
Size: 680945 Color: 0
Size: 319026 Color: 1

Bin 1446: 30 of cap free
Amount of items: 2
Items: 
Size: 686871 Color: 1
Size: 313100 Color: 0

Bin 1447: 30 of cap free
Amount of items: 2
Items: 
Size: 694657 Color: 0
Size: 305314 Color: 1

Bin 1448: 30 of cap free
Amount of items: 2
Items: 
Size: 701609 Color: 1
Size: 298362 Color: 0

Bin 1449: 30 of cap free
Amount of items: 2
Items: 
Size: 704686 Color: 0
Size: 295285 Color: 1

Bin 1450: 30 of cap free
Amount of items: 2
Items: 
Size: 726213 Color: 1
Size: 273758 Color: 0

Bin 1451: 30 of cap free
Amount of items: 2
Items: 
Size: 763491 Color: 0
Size: 236480 Color: 1

Bin 1452: 30 of cap free
Amount of items: 2
Items: 
Size: 796019 Color: 1
Size: 203952 Color: 0

Bin 1453: 31 of cap free
Amount of items: 2
Items: 
Size: 570071 Color: 0
Size: 429899 Color: 1

Bin 1454: 31 of cap free
Amount of items: 2
Items: 
Size: 718363 Color: 1
Size: 281607 Color: 0

Bin 1455: 31 of cap free
Amount of items: 5
Items: 
Size: 281862 Color: 0
Size: 182155 Color: 0
Size: 181062 Color: 0
Size: 177453 Color: 1
Size: 177438 Color: 1

Bin 1456: 31 of cap free
Amount of items: 2
Items: 
Size: 500986 Color: 1
Size: 498984 Color: 0

Bin 1457: 31 of cap free
Amount of items: 2
Items: 
Size: 518789 Color: 1
Size: 481181 Color: 0

Bin 1458: 31 of cap free
Amount of items: 2
Items: 
Size: 521028 Color: 1
Size: 478942 Color: 0

Bin 1459: 31 of cap free
Amount of items: 2
Items: 
Size: 550541 Color: 1
Size: 449429 Color: 0

Bin 1460: 31 of cap free
Amount of items: 2
Items: 
Size: 555994 Color: 1
Size: 443976 Color: 0

Bin 1461: 31 of cap free
Amount of items: 2
Items: 
Size: 569963 Color: 1
Size: 430007 Color: 0

Bin 1462: 31 of cap free
Amount of items: 2
Items: 
Size: 575591 Color: 0
Size: 424379 Color: 1

Bin 1463: 31 of cap free
Amount of items: 2
Items: 
Size: 594716 Color: 0
Size: 405254 Color: 1

Bin 1464: 31 of cap free
Amount of items: 2
Items: 
Size: 618362 Color: 0
Size: 381608 Color: 1

Bin 1465: 31 of cap free
Amount of items: 2
Items: 
Size: 645997 Color: 1
Size: 353973 Color: 0

Bin 1466: 31 of cap free
Amount of items: 2
Items: 
Size: 655682 Color: 1
Size: 344288 Color: 0

Bin 1467: 31 of cap free
Amount of items: 2
Items: 
Size: 685350 Color: 0
Size: 314620 Color: 1

Bin 1468: 31 of cap free
Amount of items: 2
Items: 
Size: 690299 Color: 0
Size: 309671 Color: 1

Bin 1469: 31 of cap free
Amount of items: 2
Items: 
Size: 731030 Color: 1
Size: 268940 Color: 0

Bin 1470: 31 of cap free
Amount of items: 2
Items: 
Size: 734887 Color: 1
Size: 265083 Color: 0

Bin 1471: 31 of cap free
Amount of items: 2
Items: 
Size: 749918 Color: 0
Size: 250052 Color: 1

Bin 1472: 31 of cap free
Amount of items: 2
Items: 
Size: 772588 Color: 1
Size: 227382 Color: 0

Bin 1473: 31 of cap free
Amount of items: 2
Items: 
Size: 792114 Color: 1
Size: 207856 Color: 0

Bin 1474: 31 of cap free
Amount of items: 2
Items: 
Size: 797493 Color: 0
Size: 202477 Color: 1

Bin 1475: 32 of cap free
Amount of items: 2
Items: 
Size: 596522 Color: 0
Size: 403447 Color: 1

Bin 1476: 32 of cap free
Amount of items: 2
Items: 
Size: 528543 Color: 0
Size: 471426 Color: 1

Bin 1477: 32 of cap free
Amount of items: 2
Items: 
Size: 556398 Color: 1
Size: 443571 Color: 0

Bin 1478: 32 of cap free
Amount of items: 2
Items: 
Size: 560670 Color: 1
Size: 439299 Color: 0

Bin 1479: 32 of cap free
Amount of items: 2
Items: 
Size: 568736 Color: 1
Size: 431233 Color: 0

Bin 1480: 32 of cap free
Amount of items: 2
Items: 
Size: 577099 Color: 0
Size: 422870 Color: 1

Bin 1481: 32 of cap free
Amount of items: 2
Items: 
Size: 581825 Color: 1
Size: 418144 Color: 0

Bin 1482: 32 of cap free
Amount of items: 2
Items: 
Size: 624997 Color: 0
Size: 374972 Color: 1

Bin 1483: 32 of cap free
Amount of items: 2
Items: 
Size: 667219 Color: 0
Size: 332750 Color: 1

Bin 1484: 32 of cap free
Amount of items: 2
Items: 
Size: 674848 Color: 0
Size: 325121 Color: 1

Bin 1485: 32 of cap free
Amount of items: 2
Items: 
Size: 687447 Color: 0
Size: 312522 Color: 1

Bin 1486: 32 of cap free
Amount of items: 2
Items: 
Size: 688105 Color: 1
Size: 311864 Color: 0

Bin 1487: 32 of cap free
Amount of items: 2
Items: 
Size: 695885 Color: 0
Size: 304084 Color: 1

Bin 1488: 32 of cap free
Amount of items: 2
Items: 
Size: 730416 Color: 1
Size: 269553 Color: 0

Bin 1489: 32 of cap free
Amount of items: 2
Items: 
Size: 764213 Color: 1
Size: 235756 Color: 0

Bin 1490: 32 of cap free
Amount of items: 2
Items: 
Size: 765220 Color: 0
Size: 234749 Color: 1

Bin 1491: 32 of cap free
Amount of items: 2
Items: 
Size: 765908 Color: 0
Size: 234061 Color: 1

Bin 1492: 32 of cap free
Amount of items: 2
Items: 
Size: 767050 Color: 0
Size: 232919 Color: 1

Bin 1493: 32 of cap free
Amount of items: 2
Items: 
Size: 769931 Color: 0
Size: 230038 Color: 1

Bin 1494: 32 of cap free
Amount of items: 2
Items: 
Size: 774379 Color: 0
Size: 225590 Color: 1

Bin 1495: 32 of cap free
Amount of items: 2
Items: 
Size: 780229 Color: 1
Size: 219740 Color: 0

Bin 1496: 32 of cap free
Amount of items: 2
Items: 
Size: 783809 Color: 0
Size: 216160 Color: 1

Bin 1497: 33 of cap free
Amount of items: 2
Items: 
Size: 506398 Color: 1
Size: 493570 Color: 0

Bin 1498: 33 of cap free
Amount of items: 2
Items: 
Size: 508323 Color: 0
Size: 491645 Color: 1

Bin 1499: 33 of cap free
Amount of items: 2
Items: 
Size: 516225 Color: 0
Size: 483743 Color: 1

Bin 1500: 33 of cap free
Amount of items: 2
Items: 
Size: 532703 Color: 1
Size: 467265 Color: 0

Bin 1501: 33 of cap free
Amount of items: 2
Items: 
Size: 541800 Color: 0
Size: 458168 Color: 1

Bin 1502: 33 of cap free
Amount of items: 2
Items: 
Size: 556580 Color: 1
Size: 443388 Color: 0

Bin 1503: 33 of cap free
Amount of items: 2
Items: 
Size: 571949 Color: 0
Size: 428019 Color: 1

Bin 1504: 33 of cap free
Amount of items: 2
Items: 
Size: 584196 Color: 0
Size: 415772 Color: 1

Bin 1505: 33 of cap free
Amount of items: 2
Items: 
Size: 593657 Color: 1
Size: 406311 Color: 0

Bin 1506: 33 of cap free
Amount of items: 2
Items: 
Size: 594971 Color: 0
Size: 404997 Color: 1

Bin 1507: 33 of cap free
Amount of items: 2
Items: 
Size: 597197 Color: 1
Size: 402771 Color: 0

Bin 1508: 33 of cap free
Amount of items: 2
Items: 
Size: 628607 Color: 0
Size: 371361 Color: 1

Bin 1509: 33 of cap free
Amount of items: 2
Items: 
Size: 664983 Color: 0
Size: 334985 Color: 1

Bin 1510: 33 of cap free
Amount of items: 2
Items: 
Size: 696031 Color: 0
Size: 303937 Color: 1

Bin 1511: 33 of cap free
Amount of items: 2
Items: 
Size: 736266 Color: 1
Size: 263702 Color: 0

Bin 1512: 33 of cap free
Amount of items: 2
Items: 
Size: 764901 Color: 1
Size: 235067 Color: 0

Bin 1513: 33 of cap free
Amount of items: 2
Items: 
Size: 799974 Color: 1
Size: 199994 Color: 0

Bin 1514: 34 of cap free
Amount of items: 3
Items: 
Size: 435942 Color: 1
Size: 411868 Color: 0
Size: 152157 Color: 0

Bin 1515: 34 of cap free
Amount of items: 2
Items: 
Size: 510058 Color: 1
Size: 489909 Color: 0

Bin 1516: 34 of cap free
Amount of items: 2
Items: 
Size: 522637 Color: 0
Size: 477330 Color: 1

Bin 1517: 34 of cap free
Amount of items: 2
Items: 
Size: 529074 Color: 1
Size: 470893 Color: 0

Bin 1518: 34 of cap free
Amount of items: 2
Items: 
Size: 534440 Color: 1
Size: 465527 Color: 0

Bin 1519: 34 of cap free
Amount of items: 2
Items: 
Size: 572260 Color: 0
Size: 427707 Color: 1

Bin 1520: 34 of cap free
Amount of items: 2
Items: 
Size: 572403 Color: 1
Size: 427564 Color: 0

Bin 1521: 34 of cap free
Amount of items: 2
Items: 
Size: 580358 Color: 1
Size: 419609 Color: 0

Bin 1522: 34 of cap free
Amount of items: 2
Items: 
Size: 592983 Color: 1
Size: 406984 Color: 0

Bin 1523: 34 of cap free
Amount of items: 2
Items: 
Size: 595364 Color: 0
Size: 404603 Color: 1

Bin 1524: 34 of cap free
Amount of items: 2
Items: 
Size: 621138 Color: 1
Size: 378829 Color: 0

Bin 1525: 34 of cap free
Amount of items: 2
Items: 
Size: 629811 Color: 1
Size: 370156 Color: 0

Bin 1526: 34 of cap free
Amount of items: 2
Items: 
Size: 658380 Color: 1
Size: 341587 Color: 0

Bin 1527: 34 of cap free
Amount of items: 2
Items: 
Size: 664046 Color: 0
Size: 335921 Color: 1

Bin 1528: 34 of cap free
Amount of items: 2
Items: 
Size: 713222 Color: 0
Size: 286745 Color: 1

Bin 1529: 34 of cap free
Amount of items: 2
Items: 
Size: 720096 Color: 1
Size: 279871 Color: 0

Bin 1530: 34 of cap free
Amount of items: 2
Items: 
Size: 722714 Color: 1
Size: 277253 Color: 0

Bin 1531: 34 of cap free
Amount of items: 2
Items: 
Size: 724253 Color: 0
Size: 275714 Color: 1

Bin 1532: 34 of cap free
Amount of items: 2
Items: 
Size: 742472 Color: 0
Size: 257495 Color: 1

Bin 1533: 34 of cap free
Amount of items: 2
Items: 
Size: 775953 Color: 1
Size: 224014 Color: 0

Bin 1534: 34 of cap free
Amount of items: 2
Items: 
Size: 781299 Color: 1
Size: 218668 Color: 0

Bin 1535: 34 of cap free
Amount of items: 2
Items: 
Size: 781344 Color: 1
Size: 218623 Color: 0

Bin 1536: 35 of cap free
Amount of items: 3
Items: 
Size: 477450 Color: 0
Size: 374906 Color: 1
Size: 147610 Color: 1

Bin 1537: 35 of cap free
Amount of items: 2
Items: 
Size: 507930 Color: 0
Size: 492036 Color: 1

Bin 1538: 35 of cap free
Amount of items: 2
Items: 
Size: 536826 Color: 1
Size: 463140 Color: 0

Bin 1539: 35 of cap free
Amount of items: 2
Items: 
Size: 547686 Color: 0
Size: 452280 Color: 1

Bin 1540: 35 of cap free
Amount of items: 2
Items: 
Size: 562964 Color: 0
Size: 437002 Color: 1

Bin 1541: 35 of cap free
Amount of items: 2
Items: 
Size: 588866 Color: 0
Size: 411100 Color: 1

Bin 1542: 35 of cap free
Amount of items: 2
Items: 
Size: 609773 Color: 0
Size: 390193 Color: 1

Bin 1543: 35 of cap free
Amount of items: 2
Items: 
Size: 621558 Color: 1
Size: 378408 Color: 0

Bin 1544: 35 of cap free
Amount of items: 2
Items: 
Size: 623234 Color: 1
Size: 376732 Color: 0

Bin 1545: 35 of cap free
Amount of items: 2
Items: 
Size: 649725 Color: 0
Size: 350241 Color: 1

Bin 1546: 35 of cap free
Amount of items: 2
Items: 
Size: 661600 Color: 1
Size: 338366 Color: 0

Bin 1547: 35 of cap free
Amount of items: 2
Items: 
Size: 669849 Color: 1
Size: 330117 Color: 0

Bin 1548: 35 of cap free
Amount of items: 2
Items: 
Size: 674646 Color: 0
Size: 325320 Color: 1

Bin 1549: 35 of cap free
Amount of items: 2
Items: 
Size: 676774 Color: 0
Size: 323192 Color: 1

Bin 1550: 35 of cap free
Amount of items: 2
Items: 
Size: 705892 Color: 1
Size: 294074 Color: 0

Bin 1551: 35 of cap free
Amount of items: 2
Items: 
Size: 712325 Color: 1
Size: 287641 Color: 0

Bin 1552: 35 of cap free
Amount of items: 2
Items: 
Size: 712488 Color: 0
Size: 287478 Color: 1

Bin 1553: 35 of cap free
Amount of items: 2
Items: 
Size: 753712 Color: 0
Size: 246254 Color: 1

Bin 1554: 36 of cap free
Amount of items: 2
Items: 
Size: 706393 Color: 0
Size: 293572 Color: 1

Bin 1555: 36 of cap free
Amount of items: 2
Items: 
Size: 546169 Color: 1
Size: 453796 Color: 0

Bin 1556: 36 of cap free
Amount of items: 2
Items: 
Size: 561407 Color: 0
Size: 438558 Color: 1

Bin 1557: 36 of cap free
Amount of items: 2
Items: 
Size: 571244 Color: 1
Size: 428721 Color: 0

Bin 1558: 36 of cap free
Amount of items: 2
Items: 
Size: 589882 Color: 0
Size: 410083 Color: 1

Bin 1559: 36 of cap free
Amount of items: 2
Items: 
Size: 606899 Color: 1
Size: 393066 Color: 0

Bin 1560: 36 of cap free
Amount of items: 2
Items: 
Size: 611543 Color: 1
Size: 388422 Color: 0

Bin 1561: 36 of cap free
Amount of items: 2
Items: 
Size: 615584 Color: 1
Size: 384381 Color: 0

Bin 1562: 36 of cap free
Amount of items: 2
Items: 
Size: 617337 Color: 0
Size: 382628 Color: 1

Bin 1563: 36 of cap free
Amount of items: 2
Items: 
Size: 658340 Color: 1
Size: 341625 Color: 0

Bin 1564: 36 of cap free
Amount of items: 2
Items: 
Size: 659828 Color: 0
Size: 340137 Color: 1

Bin 1565: 36 of cap free
Amount of items: 2
Items: 
Size: 664525 Color: 1
Size: 335440 Color: 0

Bin 1566: 36 of cap free
Amount of items: 2
Items: 
Size: 702048 Color: 0
Size: 297917 Color: 1

Bin 1567: 36 of cap free
Amount of items: 2
Items: 
Size: 708731 Color: 1
Size: 291234 Color: 0

Bin 1568: 36 of cap free
Amount of items: 2
Items: 
Size: 740131 Color: 1
Size: 259834 Color: 0

Bin 1569: 36 of cap free
Amount of items: 2
Items: 
Size: 785472 Color: 0
Size: 214493 Color: 1

Bin 1570: 36 of cap free
Amount of items: 2
Items: 
Size: 786641 Color: 1
Size: 213324 Color: 0

Bin 1571: 37 of cap free
Amount of items: 5
Items: 
Size: 223737 Color: 0
Size: 196603 Color: 0
Size: 195682 Color: 1
Size: 195564 Color: 1
Size: 188378 Color: 1

Bin 1572: 37 of cap free
Amount of items: 2
Items: 
Size: 579742 Color: 1
Size: 420222 Color: 0

Bin 1573: 37 of cap free
Amount of items: 2
Items: 
Size: 509931 Color: 0
Size: 490033 Color: 1

Bin 1574: 37 of cap free
Amount of items: 2
Items: 
Size: 521382 Color: 1
Size: 478582 Color: 0

Bin 1575: 37 of cap free
Amount of items: 2
Items: 
Size: 531060 Color: 1
Size: 468904 Color: 0

Bin 1576: 37 of cap free
Amount of items: 2
Items: 
Size: 541934 Color: 0
Size: 458030 Color: 1

Bin 1577: 37 of cap free
Amount of items: 2
Items: 
Size: 543083 Color: 0
Size: 456881 Color: 1

Bin 1578: 37 of cap free
Amount of items: 2
Items: 
Size: 558662 Color: 0
Size: 441302 Color: 1

Bin 1579: 37 of cap free
Amount of items: 2
Items: 
Size: 585842 Color: 0
Size: 414122 Color: 1

Bin 1580: 37 of cap free
Amount of items: 2
Items: 
Size: 591164 Color: 1
Size: 408800 Color: 0

Bin 1581: 37 of cap free
Amount of items: 2
Items: 
Size: 597977 Color: 1
Size: 401987 Color: 0

Bin 1582: 37 of cap free
Amount of items: 2
Items: 
Size: 616957 Color: 1
Size: 383007 Color: 0

Bin 1583: 37 of cap free
Amount of items: 2
Items: 
Size: 622106 Color: 0
Size: 377858 Color: 1

Bin 1584: 37 of cap free
Amount of items: 2
Items: 
Size: 623871 Color: 0
Size: 376093 Color: 1

Bin 1585: 37 of cap free
Amount of items: 2
Items: 
Size: 636010 Color: 1
Size: 363954 Color: 0

Bin 1586: 37 of cap free
Amount of items: 2
Items: 
Size: 659099 Color: 0
Size: 340865 Color: 1

Bin 1587: 37 of cap free
Amount of items: 2
Items: 
Size: 663034 Color: 1
Size: 336930 Color: 0

Bin 1588: 37 of cap free
Amount of items: 2
Items: 
Size: 668090 Color: 0
Size: 331874 Color: 1

Bin 1589: 37 of cap free
Amount of items: 2
Items: 
Size: 668880 Color: 0
Size: 331084 Color: 1

Bin 1590: 37 of cap free
Amount of items: 2
Items: 
Size: 728795 Color: 0
Size: 271169 Color: 1

Bin 1591: 37 of cap free
Amount of items: 2
Items: 
Size: 781634 Color: 1
Size: 218330 Color: 0

Bin 1592: 37 of cap free
Amount of items: 2
Items: 
Size: 794756 Color: 1
Size: 205208 Color: 0

Bin 1593: 38 of cap free
Amount of items: 3
Items: 
Size: 477188 Color: 0
Size: 388803 Color: 0
Size: 133972 Color: 1

Bin 1594: 38 of cap free
Amount of items: 6
Items: 
Size: 187246 Color: 0
Size: 174481 Color: 0
Size: 171366 Color: 1
Size: 169030 Color: 1
Size: 162247 Color: 1
Size: 135593 Color: 0

Bin 1595: 38 of cap free
Amount of items: 2
Items: 
Size: 526940 Color: 0
Size: 473023 Color: 1

Bin 1596: 38 of cap free
Amount of items: 2
Items: 
Size: 556031 Color: 1
Size: 443932 Color: 0

Bin 1597: 38 of cap free
Amount of items: 2
Items: 
Size: 587964 Color: 0
Size: 411999 Color: 1

Bin 1598: 38 of cap free
Amount of items: 2
Items: 
Size: 596314 Color: 0
Size: 403649 Color: 1

Bin 1599: 38 of cap free
Amount of items: 2
Items: 
Size: 608138 Color: 0
Size: 391825 Color: 1

Bin 1600: 38 of cap free
Amount of items: 2
Items: 
Size: 634462 Color: 0
Size: 365501 Color: 1

Bin 1601: 38 of cap free
Amount of items: 2
Items: 
Size: 634604 Color: 0
Size: 365359 Color: 1

Bin 1602: 38 of cap free
Amount of items: 2
Items: 
Size: 649499 Color: 1
Size: 350464 Color: 0

Bin 1603: 38 of cap free
Amount of items: 2
Items: 
Size: 735246 Color: 0
Size: 264717 Color: 1

Bin 1604: 38 of cap free
Amount of items: 2
Items: 
Size: 753867 Color: 0
Size: 246096 Color: 1

Bin 1605: 38 of cap free
Amount of items: 2
Items: 
Size: 759746 Color: 0
Size: 240217 Color: 1

Bin 1606: 39 of cap free
Amount of items: 2
Items: 
Size: 508195 Color: 1
Size: 491767 Color: 0

Bin 1607: 39 of cap free
Amount of items: 2
Items: 
Size: 515417 Color: 0
Size: 484545 Color: 1

Bin 1608: 39 of cap free
Amount of items: 2
Items: 
Size: 521467 Color: 1
Size: 478495 Color: 0

Bin 1609: 39 of cap free
Amount of items: 2
Items: 
Size: 547100 Color: 1
Size: 452862 Color: 0

Bin 1610: 39 of cap free
Amount of items: 2
Items: 
Size: 580444 Color: 1
Size: 419518 Color: 0

Bin 1611: 39 of cap free
Amount of items: 2
Items: 
Size: 582844 Color: 1
Size: 417118 Color: 0

Bin 1612: 39 of cap free
Amount of items: 2
Items: 
Size: 585197 Color: 0
Size: 414765 Color: 1

Bin 1613: 39 of cap free
Amount of items: 2
Items: 
Size: 614870 Color: 0
Size: 385092 Color: 1

Bin 1614: 39 of cap free
Amount of items: 2
Items: 
Size: 636521 Color: 1
Size: 363441 Color: 0

Bin 1615: 39 of cap free
Amount of items: 2
Items: 
Size: 650439 Color: 0
Size: 349523 Color: 1

Bin 1616: 39 of cap free
Amount of items: 2
Items: 
Size: 654282 Color: 1
Size: 345680 Color: 0

Bin 1617: 39 of cap free
Amount of items: 2
Items: 
Size: 669675 Color: 1
Size: 330287 Color: 0

Bin 1618: 39 of cap free
Amount of items: 2
Items: 
Size: 677243 Color: 1
Size: 322719 Color: 0

Bin 1619: 39 of cap free
Amount of items: 2
Items: 
Size: 679135 Color: 1
Size: 320827 Color: 0

Bin 1620: 39 of cap free
Amount of items: 2
Items: 
Size: 685748 Color: 1
Size: 314214 Color: 0

Bin 1621: 39 of cap free
Amount of items: 2
Items: 
Size: 716894 Color: 0
Size: 283068 Color: 1

Bin 1622: 39 of cap free
Amount of items: 2
Items: 
Size: 724825 Color: 0
Size: 275137 Color: 1

Bin 1623: 39 of cap free
Amount of items: 2
Items: 
Size: 733647 Color: 0
Size: 266315 Color: 1

Bin 1624: 39 of cap free
Amount of items: 2
Items: 
Size: 738129 Color: 0
Size: 261833 Color: 1

Bin 1625: 39 of cap free
Amount of items: 2
Items: 
Size: 781256 Color: 0
Size: 218706 Color: 1

Bin 1626: 39 of cap free
Amount of items: 2
Items: 
Size: 793099 Color: 0
Size: 206863 Color: 1

Bin 1627: 39 of cap free
Amount of items: 2
Items: 
Size: 798182 Color: 1
Size: 201780 Color: 0

Bin 1628: 40 of cap free
Amount of items: 2
Items: 
Size: 637035 Color: 0
Size: 362926 Color: 1

Bin 1629: 40 of cap free
Amount of items: 3
Items: 
Size: 459320 Color: 0
Size: 372910 Color: 0
Size: 167731 Color: 1

Bin 1630: 40 of cap free
Amount of items: 2
Items: 
Size: 575376 Color: 0
Size: 424585 Color: 1

Bin 1631: 40 of cap free
Amount of items: 2
Items: 
Size: 585238 Color: 1
Size: 414723 Color: 0

Bin 1632: 40 of cap free
Amount of items: 2
Items: 
Size: 588399 Color: 1
Size: 411562 Color: 0

Bin 1633: 40 of cap free
Amount of items: 2
Items: 
Size: 591735 Color: 1
Size: 408226 Color: 0

Bin 1634: 40 of cap free
Amount of items: 2
Items: 
Size: 597333 Color: 0
Size: 402628 Color: 1

Bin 1635: 40 of cap free
Amount of items: 2
Items: 
Size: 629594 Color: 0
Size: 370367 Color: 1

Bin 1636: 40 of cap free
Amount of items: 2
Items: 
Size: 637220 Color: 0
Size: 362741 Color: 1

Bin 1637: 40 of cap free
Amount of items: 2
Items: 
Size: 637687 Color: 0
Size: 362274 Color: 1

Bin 1638: 40 of cap free
Amount of items: 2
Items: 
Size: 646123 Color: 1
Size: 353838 Color: 0

Bin 1639: 40 of cap free
Amount of items: 2
Items: 
Size: 651597 Color: 1
Size: 348364 Color: 0

Bin 1640: 40 of cap free
Amount of items: 2
Items: 
Size: 687570 Color: 1
Size: 312391 Color: 0

Bin 1641: 40 of cap free
Amount of items: 2
Items: 
Size: 747910 Color: 0
Size: 252051 Color: 1

Bin 1642: 40 of cap free
Amount of items: 2
Items: 
Size: 785007 Color: 0
Size: 214954 Color: 1

Bin 1643: 41 of cap free
Amount of items: 2
Items: 
Size: 708366 Color: 1
Size: 291594 Color: 0

Bin 1644: 41 of cap free
Amount of items: 3
Items: 
Size: 413603 Color: 0
Size: 412464 Color: 0
Size: 173893 Color: 1

Bin 1645: 41 of cap free
Amount of items: 3
Items: 
Size: 430219 Color: 0
Size: 374319 Color: 0
Size: 195422 Color: 1

Bin 1646: 41 of cap free
Amount of items: 2
Items: 
Size: 532944 Color: 1
Size: 467016 Color: 0

Bin 1647: 41 of cap free
Amount of items: 2
Items: 
Size: 544022 Color: 1
Size: 455938 Color: 0

Bin 1648: 41 of cap free
Amount of items: 2
Items: 
Size: 586701 Color: 0
Size: 413259 Color: 1

Bin 1649: 41 of cap free
Amount of items: 2
Items: 
Size: 601288 Color: 0
Size: 398672 Color: 1

Bin 1650: 41 of cap free
Amount of items: 2
Items: 
Size: 619300 Color: 0
Size: 380660 Color: 1

Bin 1651: 41 of cap free
Amount of items: 2
Items: 
Size: 638516 Color: 1
Size: 361444 Color: 0

Bin 1652: 41 of cap free
Amount of items: 2
Items: 
Size: 676178 Color: 0
Size: 323782 Color: 1

Bin 1653: 41 of cap free
Amount of items: 2
Items: 
Size: 688356 Color: 1
Size: 311604 Color: 0

Bin 1654: 41 of cap free
Amount of items: 2
Items: 
Size: 721137 Color: 1
Size: 278823 Color: 0

Bin 1655: 41 of cap free
Amount of items: 2
Items: 
Size: 752271 Color: 0
Size: 247689 Color: 1

Bin 1656: 41 of cap free
Amount of items: 2
Items: 
Size: 770447 Color: 0
Size: 229513 Color: 1

Bin 1657: 42 of cap free
Amount of items: 2
Items: 
Size: 739649 Color: 0
Size: 260310 Color: 1

Bin 1658: 42 of cap free
Amount of items: 2
Items: 
Size: 513441 Color: 1
Size: 486518 Color: 0

Bin 1659: 42 of cap free
Amount of items: 2
Items: 
Size: 549556 Color: 1
Size: 450403 Color: 0

Bin 1660: 42 of cap free
Amount of items: 2
Items: 
Size: 551487 Color: 1
Size: 448472 Color: 0

Bin 1661: 42 of cap free
Amount of items: 2
Items: 
Size: 562764 Color: 1
Size: 437195 Color: 0

Bin 1662: 42 of cap free
Amount of items: 2
Items: 
Size: 562884 Color: 1
Size: 437075 Color: 0

Bin 1663: 42 of cap free
Amount of items: 2
Items: 
Size: 596387 Color: 1
Size: 403572 Color: 0

Bin 1664: 42 of cap free
Amount of items: 2
Items: 
Size: 622942 Color: 0
Size: 377017 Color: 1

Bin 1665: 42 of cap free
Amount of items: 2
Items: 
Size: 626181 Color: 0
Size: 373778 Color: 1

Bin 1666: 42 of cap free
Amount of items: 2
Items: 
Size: 634784 Color: 1
Size: 365175 Color: 0

Bin 1667: 42 of cap free
Amount of items: 2
Items: 
Size: 673642 Color: 1
Size: 326317 Color: 0

Bin 1668: 42 of cap free
Amount of items: 2
Items: 
Size: 711975 Color: 1
Size: 287984 Color: 0

Bin 1669: 42 of cap free
Amount of items: 2
Items: 
Size: 722591 Color: 0
Size: 277368 Color: 1

Bin 1670: 42 of cap free
Amount of items: 2
Items: 
Size: 728416 Color: 0
Size: 271543 Color: 1

Bin 1671: 42 of cap free
Amount of items: 2
Items: 
Size: 741604 Color: 0
Size: 258355 Color: 1

Bin 1672: 42 of cap free
Amount of items: 2
Items: 
Size: 749860 Color: 0
Size: 250099 Color: 1

Bin 1673: 42 of cap free
Amount of items: 2
Items: 
Size: 750239 Color: 1
Size: 249720 Color: 0

Bin 1674: 42 of cap free
Amount of items: 2
Items: 
Size: 789701 Color: 0
Size: 210258 Color: 1

Bin 1675: 43 of cap free
Amount of items: 5
Items: 
Size: 279084 Color: 1
Size: 187468 Color: 0
Size: 184622 Color: 1
Size: 184502 Color: 1
Size: 164282 Color: 0

Bin 1676: 43 of cap free
Amount of items: 2
Items: 
Size: 526182 Color: 0
Size: 473776 Color: 1

Bin 1677: 43 of cap free
Amount of items: 2
Items: 
Size: 577223 Color: 0
Size: 422735 Color: 1

Bin 1678: 43 of cap free
Amount of items: 2
Items: 
Size: 583527 Color: 0
Size: 416431 Color: 1

Bin 1679: 43 of cap free
Amount of items: 2
Items: 
Size: 669650 Color: 0
Size: 330308 Color: 1

Bin 1680: 43 of cap free
Amount of items: 2
Items: 
Size: 689091 Color: 0
Size: 310867 Color: 1

Bin 1681: 43 of cap free
Amount of items: 2
Items: 
Size: 714103 Color: 1
Size: 285855 Color: 0

Bin 1682: 43 of cap free
Amount of items: 2
Items: 
Size: 717767 Color: 1
Size: 282191 Color: 0

Bin 1683: 43 of cap free
Amount of items: 2
Items: 
Size: 746007 Color: 1
Size: 253951 Color: 0

Bin 1684: 43 of cap free
Amount of items: 2
Items: 
Size: 750377 Color: 1
Size: 249581 Color: 0

Bin 1685: 43 of cap free
Amount of items: 2
Items: 
Size: 752092 Color: 0
Size: 247866 Color: 1

Bin 1686: 43 of cap free
Amount of items: 2
Items: 
Size: 771598 Color: 1
Size: 228360 Color: 0

Bin 1687: 43 of cap free
Amount of items: 2
Items: 
Size: 772250 Color: 1
Size: 227708 Color: 0

Bin 1688: 43 of cap free
Amount of items: 2
Items: 
Size: 780298 Color: 1
Size: 219660 Color: 0

Bin 1689: 43 of cap free
Amount of items: 2
Items: 
Size: 786094 Color: 1
Size: 213864 Color: 0

Bin 1690: 43 of cap free
Amount of items: 2
Items: 
Size: 789961 Color: 1
Size: 209997 Color: 0

Bin 1691: 44 of cap free
Amount of items: 2
Items: 
Size: 534532 Color: 0
Size: 465425 Color: 1

Bin 1692: 44 of cap free
Amount of items: 2
Items: 
Size: 539858 Color: 1
Size: 460099 Color: 0

Bin 1693: 44 of cap free
Amount of items: 2
Items: 
Size: 548410 Color: 0
Size: 451547 Color: 1

Bin 1694: 44 of cap free
Amount of items: 3
Items: 
Size: 549039 Color: 1
Size: 279982 Color: 0
Size: 170936 Color: 0

Bin 1695: 44 of cap free
Amount of items: 2
Items: 
Size: 558517 Color: 0
Size: 441440 Color: 1

Bin 1696: 44 of cap free
Amount of items: 2
Items: 
Size: 561213 Color: 1
Size: 438744 Color: 0

Bin 1697: 44 of cap free
Amount of items: 2
Items: 
Size: 564324 Color: 0
Size: 435633 Color: 1

Bin 1698: 44 of cap free
Amount of items: 2
Items: 
Size: 571860 Color: 1
Size: 428097 Color: 0

Bin 1699: 44 of cap free
Amount of items: 2
Items: 
Size: 574694 Color: 0
Size: 425263 Color: 1

Bin 1700: 44 of cap free
Amount of items: 2
Items: 
Size: 642894 Color: 0
Size: 357063 Color: 1

Bin 1701: 44 of cap free
Amount of items: 2
Items: 
Size: 666066 Color: 0
Size: 333891 Color: 1

Bin 1702: 44 of cap free
Amount of items: 2
Items: 
Size: 669090 Color: 0
Size: 330867 Color: 1

Bin 1703: 44 of cap free
Amount of items: 2
Items: 
Size: 682112 Color: 1
Size: 317845 Color: 0

Bin 1704: 44 of cap free
Amount of items: 2
Items: 
Size: 706911 Color: 1
Size: 293046 Color: 0

Bin 1705: 44 of cap free
Amount of items: 2
Items: 
Size: 713467 Color: 1
Size: 286490 Color: 0

Bin 1706: 44 of cap free
Amount of items: 2
Items: 
Size: 752641 Color: 0
Size: 247316 Color: 1

Bin 1707: 44 of cap free
Amount of items: 2
Items: 
Size: 766030 Color: 0
Size: 233927 Color: 1

Bin 1708: 45 of cap free
Amount of items: 3
Items: 
Size: 459812 Color: 0
Size: 364975 Color: 1
Size: 175169 Color: 1

Bin 1709: 45 of cap free
Amount of items: 2
Items: 
Size: 763638 Color: 0
Size: 236318 Color: 1

Bin 1710: 45 of cap free
Amount of items: 2
Items: 
Size: 535813 Color: 1
Size: 464143 Color: 0

Bin 1711: 45 of cap free
Amount of items: 2
Items: 
Size: 546032 Color: 1
Size: 453924 Color: 0

Bin 1712: 45 of cap free
Amount of items: 2
Items: 
Size: 560656 Color: 0
Size: 439300 Color: 1

Bin 1713: 45 of cap free
Amount of items: 2
Items: 
Size: 563995 Color: 1
Size: 435961 Color: 0

Bin 1714: 45 of cap free
Amount of items: 2
Items: 
Size: 613019 Color: 1
Size: 386937 Color: 0

Bin 1715: 45 of cap free
Amount of items: 2
Items: 
Size: 641581 Color: 0
Size: 358375 Color: 1

Bin 1716: 45 of cap free
Amount of items: 2
Items: 
Size: 662958 Color: 0
Size: 336998 Color: 1

Bin 1717: 45 of cap free
Amount of items: 2
Items: 
Size: 678557 Color: 0
Size: 321399 Color: 1

Bin 1718: 45 of cap free
Amount of items: 2
Items: 
Size: 701720 Color: 0
Size: 298236 Color: 1

Bin 1719: 45 of cap free
Amount of items: 2
Items: 
Size: 748032 Color: 0
Size: 251924 Color: 1

Bin 1720: 45 of cap free
Amount of items: 2
Items: 
Size: 799597 Color: 0
Size: 200359 Color: 1

Bin 1721: 46 of cap free
Amount of items: 3
Items: 
Size: 477253 Color: 0
Size: 340613 Color: 1
Size: 182089 Color: 1

Bin 1722: 46 of cap free
Amount of items: 2
Items: 
Size: 602652 Color: 0
Size: 397303 Color: 1

Bin 1723: 46 of cap free
Amount of items: 2
Items: 
Size: 554463 Color: 1
Size: 445492 Color: 0

Bin 1724: 46 of cap free
Amount of items: 2
Items: 
Size: 517085 Color: 0
Size: 482870 Color: 1

Bin 1725: 46 of cap free
Amount of items: 2
Items: 
Size: 532418 Color: 0
Size: 467537 Color: 1

Bin 1726: 46 of cap free
Amount of items: 2
Items: 
Size: 533369 Color: 1
Size: 466586 Color: 0

Bin 1727: 46 of cap free
Amount of items: 2
Items: 
Size: 551093 Color: 0
Size: 448862 Color: 1

Bin 1728: 46 of cap free
Amount of items: 2
Items: 
Size: 588212 Color: 0
Size: 411743 Color: 1

Bin 1729: 46 of cap free
Amount of items: 2
Items: 
Size: 647384 Color: 0
Size: 352571 Color: 1

Bin 1730: 46 of cap free
Amount of items: 2
Items: 
Size: 673772 Color: 1
Size: 326183 Color: 0

Bin 1731: 46 of cap free
Amount of items: 2
Items: 
Size: 676774 Color: 0
Size: 323181 Color: 1

Bin 1732: 46 of cap free
Amount of items: 2
Items: 
Size: 677860 Color: 1
Size: 322095 Color: 0

Bin 1733: 46 of cap free
Amount of items: 2
Items: 
Size: 685559 Color: 1
Size: 314396 Color: 0

Bin 1734: 46 of cap free
Amount of items: 2
Items: 
Size: 688581 Color: 0
Size: 311374 Color: 1

Bin 1735: 46 of cap free
Amount of items: 2
Items: 
Size: 724165 Color: 1
Size: 275790 Color: 0

Bin 1736: 46 of cap free
Amount of items: 2
Items: 
Size: 731400 Color: 1
Size: 268555 Color: 0

Bin 1737: 46 of cap free
Amount of items: 2
Items: 
Size: 776532 Color: 1
Size: 223423 Color: 0

Bin 1738: 46 of cap free
Amount of items: 2
Items: 
Size: 783488 Color: 0
Size: 216467 Color: 1

Bin 1739: 47 of cap free
Amount of items: 2
Items: 
Size: 782056 Color: 1
Size: 217898 Color: 0

Bin 1740: 47 of cap free
Amount of items: 2
Items: 
Size: 629015 Color: 1
Size: 370939 Color: 0

Bin 1741: 47 of cap free
Amount of items: 2
Items: 
Size: 503821 Color: 1
Size: 496133 Color: 0

Bin 1742: 47 of cap free
Amount of items: 2
Items: 
Size: 529512 Color: 1
Size: 470442 Color: 0

Bin 1743: 47 of cap free
Amount of items: 2
Items: 
Size: 532757 Color: 1
Size: 467197 Color: 0

Bin 1744: 47 of cap free
Amount of items: 2
Items: 
Size: 573846 Color: 1
Size: 426108 Color: 0

Bin 1745: 47 of cap free
Amount of items: 2
Items: 
Size: 585007 Color: 1
Size: 414947 Color: 0

Bin 1746: 47 of cap free
Amount of items: 2
Items: 
Size: 593993 Color: 1
Size: 405961 Color: 0

Bin 1747: 47 of cap free
Amount of items: 2
Items: 
Size: 654467 Color: 0
Size: 345487 Color: 1

Bin 1748: 47 of cap free
Amount of items: 2
Items: 
Size: 661425 Color: 0
Size: 338529 Color: 1

Bin 1749: 47 of cap free
Amount of items: 2
Items: 
Size: 674534 Color: 0
Size: 325420 Color: 1

Bin 1750: 47 of cap free
Amount of items: 2
Items: 
Size: 719127 Color: 0
Size: 280827 Color: 1

Bin 1751: 47 of cap free
Amount of items: 2
Items: 
Size: 765203 Color: 1
Size: 234751 Color: 0

Bin 1752: 48 of cap free
Amount of items: 2
Items: 
Size: 642478 Color: 1
Size: 357475 Color: 0

Bin 1753: 48 of cap free
Amount of items: 2
Items: 
Size: 788420 Color: 0
Size: 211533 Color: 1

Bin 1754: 48 of cap free
Amount of items: 2
Items: 
Size: 513880 Color: 1
Size: 486073 Color: 0

Bin 1755: 48 of cap free
Amount of items: 2
Items: 
Size: 515545 Color: 0
Size: 484408 Color: 1

Bin 1756: 48 of cap free
Amount of items: 2
Items: 
Size: 531553 Color: 1
Size: 468400 Color: 0

Bin 1757: 48 of cap free
Amount of items: 2
Items: 
Size: 531936 Color: 1
Size: 468017 Color: 0

Bin 1758: 48 of cap free
Amount of items: 2
Items: 
Size: 557307 Color: 0
Size: 442646 Color: 1

Bin 1759: 48 of cap free
Amount of items: 2
Items: 
Size: 581270 Color: 0
Size: 418683 Color: 1

Bin 1760: 48 of cap free
Amount of items: 2
Items: 
Size: 590242 Color: 0
Size: 409711 Color: 1

Bin 1761: 48 of cap free
Amount of items: 2
Items: 
Size: 620248 Color: 1
Size: 379705 Color: 0

Bin 1762: 48 of cap free
Amount of items: 2
Items: 
Size: 622750 Color: 1
Size: 377203 Color: 0

Bin 1763: 48 of cap free
Amount of items: 2
Items: 
Size: 652971 Color: 0
Size: 346982 Color: 1

Bin 1764: 48 of cap free
Amount of items: 2
Items: 
Size: 660444 Color: 1
Size: 339509 Color: 0

Bin 1765: 48 of cap free
Amount of items: 2
Items: 
Size: 695772 Color: 0
Size: 304181 Color: 1

Bin 1766: 48 of cap free
Amount of items: 2
Items: 
Size: 720902 Color: 1
Size: 279051 Color: 0

Bin 1767: 48 of cap free
Amount of items: 2
Items: 
Size: 722691 Color: 0
Size: 277262 Color: 1

Bin 1768: 48 of cap free
Amount of items: 2
Items: 
Size: 755215 Color: 1
Size: 244738 Color: 0

Bin 1769: 48 of cap free
Amount of items: 2
Items: 
Size: 794199 Color: 0
Size: 205754 Color: 1

Bin 1770: 49 of cap free
Amount of items: 2
Items: 
Size: 568911 Color: 1
Size: 431041 Color: 0

Bin 1771: 49 of cap free
Amount of items: 3
Items: 
Size: 767780 Color: 0
Size: 128904 Color: 0
Size: 103268 Color: 1

Bin 1772: 49 of cap free
Amount of items: 5
Items: 
Size: 225275 Color: 1
Size: 194535 Color: 0
Size: 194221 Color: 0
Size: 193906 Color: 0
Size: 192015 Color: 1

Bin 1773: 49 of cap free
Amount of items: 2
Items: 
Size: 547039 Color: 0
Size: 452913 Color: 1

Bin 1774: 49 of cap free
Amount of items: 2
Items: 
Size: 560451 Color: 1
Size: 439501 Color: 0

Bin 1775: 49 of cap free
Amount of items: 2
Items: 
Size: 563997 Color: 0
Size: 435955 Color: 1

Bin 1776: 49 of cap free
Amount of items: 2
Items: 
Size: 576757 Color: 0
Size: 423195 Color: 1

Bin 1777: 49 of cap free
Amount of items: 2
Items: 
Size: 616089 Color: 0
Size: 383863 Color: 1

Bin 1778: 49 of cap free
Amount of items: 2
Items: 
Size: 643896 Color: 1
Size: 356056 Color: 0

Bin 1779: 49 of cap free
Amount of items: 2
Items: 
Size: 733034 Color: 0
Size: 266918 Color: 1

Bin 1780: 49 of cap free
Amount of items: 2
Items: 
Size: 739679 Color: 1
Size: 260273 Color: 0

Bin 1781: 49 of cap free
Amount of items: 2
Items: 
Size: 744433 Color: 1
Size: 255519 Color: 0

Bin 1782: 49 of cap free
Amount of items: 2
Items: 
Size: 757838 Color: 1
Size: 242114 Color: 0

Bin 1783: 49 of cap free
Amount of items: 2
Items: 
Size: 760716 Color: 1
Size: 239236 Color: 0

Bin 1784: 49 of cap free
Amount of items: 2
Items: 
Size: 772732 Color: 0
Size: 227220 Color: 1

Bin 1785: 50 of cap free
Amount of items: 3
Items: 
Size: 476987 Color: 0
Size: 388995 Color: 1
Size: 133969 Color: 1

Bin 1786: 50 of cap free
Amount of items: 2
Items: 
Size: 631174 Color: 0
Size: 368777 Color: 1

Bin 1787: 50 of cap free
Amount of items: 5
Items: 
Size: 262539 Color: 1
Size: 185092 Color: 0
Size: 184970 Color: 0
Size: 183972 Color: 1
Size: 183378 Color: 1

Bin 1788: 50 of cap free
Amount of items: 2
Items: 
Size: 506184 Color: 0
Size: 493767 Color: 1

Bin 1789: 50 of cap free
Amount of items: 2
Items: 
Size: 507100 Color: 1
Size: 492851 Color: 0

Bin 1790: 50 of cap free
Amount of items: 2
Items: 
Size: 517287 Color: 1
Size: 482664 Color: 0

Bin 1791: 50 of cap free
Amount of items: 2
Items: 
Size: 518363 Color: 1
Size: 481588 Color: 0

Bin 1792: 50 of cap free
Amount of items: 2
Items: 
Size: 531385 Color: 1
Size: 468566 Color: 0

Bin 1793: 50 of cap free
Amount of items: 2
Items: 
Size: 534594 Color: 1
Size: 465357 Color: 0

Bin 1794: 50 of cap free
Amount of items: 2
Items: 
Size: 558280 Color: 1
Size: 441671 Color: 0

Bin 1795: 50 of cap free
Amount of items: 2
Items: 
Size: 561959 Color: 1
Size: 437992 Color: 0

Bin 1796: 50 of cap free
Amount of items: 2
Items: 
Size: 608214 Color: 0
Size: 391737 Color: 1

Bin 1797: 50 of cap free
Amount of items: 2
Items: 
Size: 653634 Color: 0
Size: 346317 Color: 1

Bin 1798: 50 of cap free
Amount of items: 2
Items: 
Size: 677131 Color: 0
Size: 322820 Color: 1

Bin 1799: 50 of cap free
Amount of items: 2
Items: 
Size: 678139 Color: 1
Size: 321812 Color: 0

Bin 1800: 50 of cap free
Amount of items: 2
Items: 
Size: 701490 Color: 1
Size: 298461 Color: 0

Bin 1801: 50 of cap free
Amount of items: 2
Items: 
Size: 724830 Color: 1
Size: 275121 Color: 0

Bin 1802: 50 of cap free
Amount of items: 2
Items: 
Size: 737546 Color: 0
Size: 262405 Color: 1

Bin 1803: 50 of cap free
Amount of items: 2
Items: 
Size: 755627 Color: 0
Size: 244324 Color: 1

Bin 1804: 50 of cap free
Amount of items: 2
Items: 
Size: 767082 Color: 1
Size: 232869 Color: 0

Bin 1805: 51 of cap free
Amount of items: 2
Items: 
Size: 519054 Color: 1
Size: 480896 Color: 0

Bin 1806: 51 of cap free
Amount of items: 2
Items: 
Size: 527383 Color: 0
Size: 472567 Color: 1

Bin 1807: 51 of cap free
Amount of items: 2
Items: 
Size: 537557 Color: 1
Size: 462393 Color: 0

Bin 1808: 51 of cap free
Amount of items: 2
Items: 
Size: 551422 Color: 1
Size: 448528 Color: 0

Bin 1809: 51 of cap free
Amount of items: 2
Items: 
Size: 569354 Color: 1
Size: 430596 Color: 0

Bin 1810: 51 of cap free
Amount of items: 2
Items: 
Size: 584825 Color: 1
Size: 415125 Color: 0

Bin 1811: 51 of cap free
Amount of items: 2
Items: 
Size: 646909 Color: 0
Size: 353041 Color: 1

Bin 1812: 51 of cap free
Amount of items: 2
Items: 
Size: 653783 Color: 0
Size: 346167 Color: 1

Bin 1813: 51 of cap free
Amount of items: 2
Items: 
Size: 660696 Color: 0
Size: 339254 Color: 1

Bin 1814: 51 of cap free
Amount of items: 2
Items: 
Size: 666845 Color: 0
Size: 333105 Color: 1

Bin 1815: 51 of cap free
Amount of items: 2
Items: 
Size: 682189 Color: 1
Size: 317761 Color: 0

Bin 1816: 51 of cap free
Amount of items: 2
Items: 
Size: 697179 Color: 0
Size: 302771 Color: 1

Bin 1817: 51 of cap free
Amount of items: 2
Items: 
Size: 713888 Color: 0
Size: 286062 Color: 1

Bin 1818: 51 of cap free
Amount of items: 2
Items: 
Size: 737704 Color: 0
Size: 262246 Color: 1

Bin 1819: 51 of cap free
Amount of items: 2
Items: 
Size: 766297 Color: 1
Size: 233653 Color: 0

Bin 1820: 52 of cap free
Amount of items: 2
Items: 
Size: 788064 Color: 0
Size: 211885 Color: 1

Bin 1821: 52 of cap free
Amount of items: 2
Items: 
Size: 517974 Color: 0
Size: 481975 Color: 1

Bin 1822: 52 of cap free
Amount of items: 2
Items: 
Size: 512267 Color: 1
Size: 487682 Color: 0

Bin 1823: 52 of cap free
Amount of items: 2
Items: 
Size: 512372 Color: 1
Size: 487577 Color: 0

Bin 1824: 52 of cap free
Amount of items: 2
Items: 
Size: 522130 Color: 1
Size: 477819 Color: 0

Bin 1825: 52 of cap free
Amount of items: 2
Items: 
Size: 565773 Color: 0
Size: 434176 Color: 1

Bin 1826: 52 of cap free
Amount of items: 2
Items: 
Size: 567151 Color: 0
Size: 432798 Color: 1

Bin 1827: 52 of cap free
Amount of items: 2
Items: 
Size: 570566 Color: 0
Size: 429383 Color: 1

Bin 1828: 52 of cap free
Amount of items: 2
Items: 
Size: 581602 Color: 0
Size: 418347 Color: 1

Bin 1829: 52 of cap free
Amount of items: 2
Items: 
Size: 587599 Color: 0
Size: 412350 Color: 1

Bin 1830: 52 of cap free
Amount of items: 2
Items: 
Size: 614221 Color: 0
Size: 385728 Color: 1

Bin 1831: 52 of cap free
Amount of items: 2
Items: 
Size: 652183 Color: 1
Size: 347766 Color: 0

Bin 1832: 52 of cap free
Amount of items: 2
Items: 
Size: 657252 Color: 0
Size: 342697 Color: 1

Bin 1833: 52 of cap free
Amount of items: 2
Items: 
Size: 729689 Color: 1
Size: 270260 Color: 0

Bin 1834: 52 of cap free
Amount of items: 2
Items: 
Size: 740973 Color: 1
Size: 258976 Color: 0

Bin 1835: 52 of cap free
Amount of items: 2
Items: 
Size: 757894 Color: 0
Size: 242055 Color: 1

Bin 1836: 52 of cap free
Amount of items: 2
Items: 
Size: 761192 Color: 0
Size: 238757 Color: 1

Bin 1837: 52 of cap free
Amount of items: 2
Items: 
Size: 767096 Color: 0
Size: 232853 Color: 1

Bin 1838: 52 of cap free
Amount of items: 2
Items: 
Size: 796113 Color: 0
Size: 203836 Color: 1

Bin 1839: 53 of cap free
Amount of items: 3
Items: 
Size: 556767 Color: 1
Size: 290440 Color: 0
Size: 152741 Color: 0

Bin 1840: 53 of cap free
Amount of items: 3
Items: 
Size: 727471 Color: 1
Size: 141412 Color: 1
Size: 131065 Color: 0

Bin 1841: 53 of cap free
Amount of items: 2
Items: 
Size: 541091 Color: 0
Size: 458857 Color: 1

Bin 1842: 53 of cap free
Amount of items: 2
Items: 
Size: 566897 Color: 1
Size: 433051 Color: 0

Bin 1843: 53 of cap free
Amount of items: 2
Items: 
Size: 577630 Color: 0
Size: 422318 Color: 1

Bin 1844: 53 of cap free
Amount of items: 2
Items: 
Size: 620329 Color: 0
Size: 379619 Color: 1

Bin 1845: 53 of cap free
Amount of items: 2
Items: 
Size: 649989 Color: 0
Size: 349959 Color: 1

Bin 1846: 53 of cap free
Amount of items: 2
Items: 
Size: 650752 Color: 0
Size: 349196 Color: 1

Bin 1847: 53 of cap free
Amount of items: 2
Items: 
Size: 680752 Color: 0
Size: 319196 Color: 1

Bin 1848: 53 of cap free
Amount of items: 2
Items: 
Size: 706723 Color: 1
Size: 293225 Color: 0

Bin 1849: 53 of cap free
Amount of items: 2
Items: 
Size: 730064 Color: 0
Size: 269884 Color: 1

Bin 1850: 53 of cap free
Amount of items: 2
Items: 
Size: 737033 Color: 0
Size: 262915 Color: 1

Bin 1851: 53 of cap free
Amount of items: 2
Items: 
Size: 737421 Color: 1
Size: 262527 Color: 0

Bin 1852: 54 of cap free
Amount of items: 2
Items: 
Size: 779978 Color: 1
Size: 219969 Color: 0

Bin 1853: 54 of cap free
Amount of items: 2
Items: 
Size: 516805 Color: 1
Size: 483142 Color: 0

Bin 1854: 54 of cap free
Amount of items: 2
Items: 
Size: 544876 Color: 0
Size: 455071 Color: 1

Bin 1855: 54 of cap free
Amount of items: 2
Items: 
Size: 584459 Color: 0
Size: 415488 Color: 1

Bin 1856: 54 of cap free
Amount of items: 2
Items: 
Size: 608251 Color: 1
Size: 391696 Color: 0

Bin 1857: 54 of cap free
Amount of items: 2
Items: 
Size: 696265 Color: 1
Size: 303682 Color: 0

Bin 1858: 54 of cap free
Amount of items: 2
Items: 
Size: 714446 Color: 1
Size: 285501 Color: 0

Bin 1859: 54 of cap free
Amount of items: 2
Items: 
Size: 714563 Color: 0
Size: 285384 Color: 1

Bin 1860: 54 of cap free
Amount of items: 2
Items: 
Size: 797953 Color: 1
Size: 201994 Color: 0

Bin 1861: 55 of cap free
Amount of items: 3
Items: 
Size: 444203 Color: 0
Size: 431508 Color: 1
Size: 124235 Color: 1

Bin 1862: 55 of cap free
Amount of items: 2
Items: 
Size: 780830 Color: 0
Size: 219116 Color: 1

Bin 1863: 55 of cap free
Amount of items: 5
Items: 
Size: 272145 Color: 1
Size: 182845 Color: 0
Size: 182732 Color: 0
Size: 182671 Color: 0
Size: 179553 Color: 1

Bin 1864: 55 of cap free
Amount of items: 2
Items: 
Size: 563008 Color: 0
Size: 436938 Color: 1

Bin 1865: 55 of cap free
Amount of items: 2
Items: 
Size: 571973 Color: 1
Size: 427973 Color: 0

Bin 1866: 55 of cap free
Amount of items: 2
Items: 
Size: 573598 Color: 1
Size: 426348 Color: 0

Bin 1867: 55 of cap free
Amount of items: 2
Items: 
Size: 579792 Color: 0
Size: 420154 Color: 1

Bin 1868: 55 of cap free
Amount of items: 2
Items: 
Size: 613646 Color: 0
Size: 386300 Color: 1

Bin 1869: 55 of cap free
Amount of items: 2
Items: 
Size: 659062 Color: 1
Size: 340884 Color: 0

Bin 1870: 55 of cap free
Amount of items: 2
Items: 
Size: 698873 Color: 1
Size: 301073 Color: 0

Bin 1871: 55 of cap free
Amount of items: 2
Items: 
Size: 750289 Color: 1
Size: 249657 Color: 0

Bin 1872: 55 of cap free
Amount of items: 2
Items: 
Size: 771953 Color: 0
Size: 227993 Color: 1

Bin 1873: 56 of cap free
Amount of items: 2
Items: 
Size: 510166 Color: 0
Size: 489779 Color: 1

Bin 1874: 56 of cap free
Amount of items: 2
Items: 
Size: 531355 Color: 0
Size: 468590 Color: 1

Bin 1875: 56 of cap free
Amount of items: 2
Items: 
Size: 545901 Color: 1
Size: 454044 Color: 0

Bin 1876: 56 of cap free
Amount of items: 2
Items: 
Size: 563507 Color: 1
Size: 436438 Color: 0

Bin 1877: 56 of cap free
Amount of items: 2
Items: 
Size: 568714 Color: 1
Size: 431231 Color: 0

Bin 1878: 56 of cap free
Amount of items: 2
Items: 
Size: 587604 Color: 1
Size: 412341 Color: 0

Bin 1879: 56 of cap free
Amount of items: 2
Items: 
Size: 595706 Color: 0
Size: 404239 Color: 1

Bin 1880: 56 of cap free
Amount of items: 2
Items: 
Size: 612285 Color: 0
Size: 387660 Color: 1

Bin 1881: 56 of cap free
Amount of items: 2
Items: 
Size: 612378 Color: 1
Size: 387567 Color: 0

Bin 1882: 56 of cap free
Amount of items: 2
Items: 
Size: 655227 Color: 1
Size: 344718 Color: 0

Bin 1883: 56 of cap free
Amount of items: 2
Items: 
Size: 683980 Color: 1
Size: 315965 Color: 0

Bin 1884: 56 of cap free
Amount of items: 2
Items: 
Size: 700607 Color: 0
Size: 299338 Color: 1

Bin 1885: 56 of cap free
Amount of items: 2
Items: 
Size: 710562 Color: 0
Size: 289383 Color: 1

Bin 1886: 56 of cap free
Amount of items: 2
Items: 
Size: 711861 Color: 0
Size: 288084 Color: 1

Bin 1887: 56 of cap free
Amount of items: 2
Items: 
Size: 712249 Color: 0
Size: 287696 Color: 1

Bin 1888: 56 of cap free
Amount of items: 2
Items: 
Size: 741527 Color: 1
Size: 258418 Color: 0

Bin 1889: 56 of cap free
Amount of items: 2
Items: 
Size: 764211 Color: 1
Size: 235734 Color: 0

Bin 1890: 56 of cap free
Amount of items: 2
Items: 
Size: 792667 Color: 0
Size: 207278 Color: 1

Bin 1891: 57 of cap free
Amount of items: 5
Items: 
Size: 303614 Color: 0
Size: 255181 Color: 0
Size: 152796 Color: 1
Size: 150986 Color: 0
Size: 137367 Color: 1

Bin 1892: 57 of cap free
Amount of items: 2
Items: 
Size: 534167 Color: 1
Size: 465777 Color: 0

Bin 1893: 57 of cap free
Amount of items: 2
Items: 
Size: 540284 Color: 0
Size: 459660 Color: 1

Bin 1894: 57 of cap free
Amount of items: 2
Items: 
Size: 542805 Color: 1
Size: 457139 Color: 0

Bin 1895: 57 of cap free
Amount of items: 2
Items: 
Size: 564684 Color: 1
Size: 435260 Color: 0

Bin 1896: 57 of cap free
Amount of items: 2
Items: 
Size: 618019 Color: 1
Size: 381925 Color: 0

Bin 1897: 57 of cap free
Amount of items: 2
Items: 
Size: 632605 Color: 0
Size: 367339 Color: 1

Bin 1898: 57 of cap free
Amount of items: 2
Items: 
Size: 634823 Color: 0
Size: 365121 Color: 1

Bin 1899: 57 of cap free
Amount of items: 2
Items: 
Size: 641891 Color: 0
Size: 358053 Color: 1

Bin 1900: 57 of cap free
Amount of items: 2
Items: 
Size: 657706 Color: 0
Size: 342238 Color: 1

Bin 1901: 57 of cap free
Amount of items: 2
Items: 
Size: 680226 Color: 1
Size: 319718 Color: 0

Bin 1902: 57 of cap free
Amount of items: 2
Items: 
Size: 680619 Color: 0
Size: 319325 Color: 1

Bin 1903: 57 of cap free
Amount of items: 2
Items: 
Size: 693013 Color: 1
Size: 306931 Color: 0

Bin 1904: 57 of cap free
Amount of items: 2
Items: 
Size: 768332 Color: 0
Size: 231612 Color: 1

Bin 1905: 57 of cap free
Amount of items: 2
Items: 
Size: 777691 Color: 0
Size: 222253 Color: 1

Bin 1906: 58 of cap free
Amount of items: 2
Items: 
Size: 731249 Color: 0
Size: 268694 Color: 1

Bin 1907: 58 of cap free
Amount of items: 3
Items: 
Size: 459637 Color: 0
Size: 438374 Color: 1
Size: 101932 Color: 1

Bin 1908: 58 of cap free
Amount of items: 2
Items: 
Size: 590091 Color: 0
Size: 409852 Color: 1

Bin 1909: 58 of cap free
Amount of items: 3
Items: 
Size: 429407 Color: 0
Size: 301984 Color: 1
Size: 268552 Color: 1

Bin 1910: 58 of cap free
Amount of items: 2
Items: 
Size: 532851 Color: 1
Size: 467092 Color: 0

Bin 1911: 58 of cap free
Amount of items: 2
Items: 
Size: 541326 Color: 0
Size: 458617 Color: 1

Bin 1912: 58 of cap free
Amount of items: 2
Items: 
Size: 550116 Color: 1
Size: 449827 Color: 0

Bin 1913: 58 of cap free
Amount of items: 2
Items: 
Size: 556496 Color: 1
Size: 443447 Color: 0

Bin 1914: 58 of cap free
Amount of items: 2
Items: 
Size: 572602 Color: 0
Size: 427341 Color: 1

Bin 1915: 58 of cap free
Amount of items: 2
Items: 
Size: 611470 Color: 0
Size: 388473 Color: 1

Bin 1916: 58 of cap free
Amount of items: 2
Items: 
Size: 613566 Color: 0
Size: 386377 Color: 1

Bin 1917: 58 of cap free
Amount of items: 2
Items: 
Size: 631925 Color: 0
Size: 368018 Color: 1

Bin 1918: 58 of cap free
Amount of items: 2
Items: 
Size: 660230 Color: 1
Size: 339713 Color: 0

Bin 1919: 58 of cap free
Amount of items: 2
Items: 
Size: 714766 Color: 1
Size: 285177 Color: 0

Bin 1920: 58 of cap free
Amount of items: 2
Items: 
Size: 715632 Color: 0
Size: 284311 Color: 1

Bin 1921: 58 of cap free
Amount of items: 2
Items: 
Size: 731598 Color: 1
Size: 268345 Color: 0

Bin 1922: 58 of cap free
Amount of items: 2
Items: 
Size: 744939 Color: 1
Size: 255004 Color: 0

Bin 1923: 59 of cap free
Amount of items: 2
Items: 
Size: 535093 Color: 1
Size: 464849 Color: 0

Bin 1924: 59 of cap free
Amount of items: 2
Items: 
Size: 546678 Color: 1
Size: 453264 Color: 0

Bin 1925: 59 of cap free
Amount of items: 2
Items: 
Size: 560648 Color: 0
Size: 439294 Color: 1

Bin 1926: 59 of cap free
Amount of items: 2
Items: 
Size: 564964 Color: 0
Size: 434978 Color: 1

Bin 1927: 59 of cap free
Amount of items: 2
Items: 
Size: 610999 Color: 1
Size: 388943 Color: 0

Bin 1928: 59 of cap free
Amount of items: 2
Items: 
Size: 616662 Color: 0
Size: 383280 Color: 1

Bin 1929: 59 of cap free
Amount of items: 2
Items: 
Size: 630891 Color: 0
Size: 369051 Color: 1

Bin 1930: 59 of cap free
Amount of items: 2
Items: 
Size: 659276 Color: 0
Size: 340666 Color: 1

Bin 1931: 59 of cap free
Amount of items: 2
Items: 
Size: 682208 Color: 0
Size: 317734 Color: 1

Bin 1932: 59 of cap free
Amount of items: 2
Items: 
Size: 694445 Color: 1
Size: 305497 Color: 0

Bin 1933: 59 of cap free
Amount of items: 2
Items: 
Size: 725219 Color: 1
Size: 274723 Color: 0

Bin 1934: 59 of cap free
Amount of items: 2
Items: 
Size: 739439 Color: 1
Size: 260503 Color: 0

Bin 1935: 60 of cap free
Amount of items: 2
Items: 
Size: 506278 Color: 1
Size: 493663 Color: 0

Bin 1936: 60 of cap free
Amount of items: 2
Items: 
Size: 512554 Color: 0
Size: 487387 Color: 1

Bin 1937: 60 of cap free
Amount of items: 2
Items: 
Size: 517667 Color: 0
Size: 482274 Color: 1

Bin 1938: 60 of cap free
Amount of items: 2
Items: 
Size: 530818 Color: 0
Size: 469123 Color: 1

Bin 1939: 60 of cap free
Amount of items: 2
Items: 
Size: 567759 Color: 1
Size: 432182 Color: 0

Bin 1940: 60 of cap free
Amount of items: 2
Items: 
Size: 590031 Color: 1
Size: 409910 Color: 0

Bin 1941: 60 of cap free
Amount of items: 2
Items: 
Size: 631400 Color: 1
Size: 368541 Color: 0

Bin 1942: 60 of cap free
Amount of items: 2
Items: 
Size: 633393 Color: 0
Size: 366548 Color: 1

Bin 1943: 60 of cap free
Amount of items: 2
Items: 
Size: 636783 Color: 1
Size: 363158 Color: 0

Bin 1944: 60 of cap free
Amount of items: 2
Items: 
Size: 656691 Color: 1
Size: 343250 Color: 0

Bin 1945: 60 of cap free
Amount of items: 2
Items: 
Size: 657375 Color: 1
Size: 342566 Color: 0

Bin 1946: 60 of cap free
Amount of items: 2
Items: 
Size: 663244 Color: 1
Size: 336697 Color: 0

Bin 1947: 60 of cap free
Amount of items: 2
Items: 
Size: 669818 Color: 0
Size: 330123 Color: 1

Bin 1948: 60 of cap free
Amount of items: 2
Items: 
Size: 700309 Color: 0
Size: 299632 Color: 1

Bin 1949: 60 of cap free
Amount of items: 2
Items: 
Size: 726713 Color: 1
Size: 273228 Color: 0

Bin 1950: 60 of cap free
Amount of items: 2
Items: 
Size: 737346 Color: 1
Size: 262595 Color: 0

Bin 1951: 60 of cap free
Amount of items: 2
Items: 
Size: 790328 Color: 0
Size: 209613 Color: 1

Bin 1952: 61 of cap free
Amount of items: 2
Items: 
Size: 503602 Color: 1
Size: 496338 Color: 0

Bin 1953: 61 of cap free
Amount of items: 2
Items: 
Size: 505208 Color: 1
Size: 494732 Color: 0

Bin 1954: 61 of cap free
Amount of items: 2
Items: 
Size: 518935 Color: 1
Size: 481005 Color: 0

Bin 1955: 61 of cap free
Amount of items: 2
Items: 
Size: 553120 Color: 0
Size: 446820 Color: 1

Bin 1956: 61 of cap free
Amount of items: 2
Items: 
Size: 593560 Color: 1
Size: 406380 Color: 0

Bin 1957: 61 of cap free
Amount of items: 2
Items: 
Size: 631342 Color: 0
Size: 368598 Color: 1

Bin 1958: 61 of cap free
Amount of items: 2
Items: 
Size: 639287 Color: 1
Size: 360653 Color: 0

Bin 1959: 61 of cap free
Amount of items: 2
Items: 
Size: 641629 Color: 0
Size: 358311 Color: 1

Bin 1960: 61 of cap free
Amount of items: 2
Items: 
Size: 655736 Color: 1
Size: 344204 Color: 0

Bin 1961: 61 of cap free
Amount of items: 2
Items: 
Size: 683807 Color: 0
Size: 316133 Color: 1

Bin 1962: 61 of cap free
Amount of items: 2
Items: 
Size: 689783 Color: 0
Size: 310157 Color: 1

Bin 1963: 61 of cap free
Amount of items: 2
Items: 
Size: 698697 Color: 0
Size: 301243 Color: 1

Bin 1964: 61 of cap free
Amount of items: 2
Items: 
Size: 705272 Color: 1
Size: 294668 Color: 0

Bin 1965: 61 of cap free
Amount of items: 2
Items: 
Size: 715332 Color: 1
Size: 284608 Color: 0

Bin 1966: 61 of cap free
Amount of items: 2
Items: 
Size: 776601 Color: 0
Size: 223339 Color: 1

Bin 1967: 61 of cap free
Amount of items: 2
Items: 
Size: 790505 Color: 0
Size: 209435 Color: 1

Bin 1968: 62 of cap free
Amount of items: 2
Items: 
Size: 547449 Color: 1
Size: 452490 Color: 0

Bin 1969: 62 of cap free
Amount of items: 2
Items: 
Size: 550333 Color: 1
Size: 449606 Color: 0

Bin 1970: 62 of cap free
Amount of items: 2
Items: 
Size: 603468 Color: 1
Size: 396471 Color: 0

Bin 1971: 62 of cap free
Amount of items: 2
Items: 
Size: 622537 Color: 0
Size: 377402 Color: 1

Bin 1972: 62 of cap free
Amount of items: 2
Items: 
Size: 672690 Color: 0
Size: 327249 Color: 1

Bin 1973: 62 of cap free
Amount of items: 2
Items: 
Size: 688558 Color: 1
Size: 311381 Color: 0

Bin 1974: 62 of cap free
Amount of items: 2
Items: 
Size: 733409 Color: 1
Size: 266530 Color: 0

Bin 1975: 62 of cap free
Amount of items: 2
Items: 
Size: 750913 Color: 0
Size: 249026 Color: 1

Bin 1976: 62 of cap free
Amount of items: 2
Items: 
Size: 775050 Color: 1
Size: 224889 Color: 0

Bin 1977: 62 of cap free
Amount of items: 2
Items: 
Size: 798369 Color: 0
Size: 201570 Color: 1

Bin 1978: 63 of cap free
Amount of items: 2
Items: 
Size: 570412 Color: 0
Size: 429526 Color: 1

Bin 1979: 63 of cap free
Amount of items: 2
Items: 
Size: 506604 Color: 0
Size: 493334 Color: 1

Bin 1980: 63 of cap free
Amount of items: 2
Items: 
Size: 520086 Color: 0
Size: 479852 Color: 1

Bin 1981: 63 of cap free
Amount of items: 2
Items: 
Size: 542028 Color: 1
Size: 457910 Color: 0

Bin 1982: 63 of cap free
Amount of items: 2
Items: 
Size: 542853 Color: 0
Size: 457085 Color: 1

Bin 1983: 63 of cap free
Amount of items: 2
Items: 
Size: 577359 Color: 0
Size: 422579 Color: 1

Bin 1984: 63 of cap free
Amount of items: 2
Items: 
Size: 595200 Color: 0
Size: 404738 Color: 1

Bin 1985: 63 of cap free
Amount of items: 2
Items: 
Size: 615804 Color: 0
Size: 384134 Color: 1

Bin 1986: 63 of cap free
Amount of items: 2
Items: 
Size: 616420 Color: 1
Size: 383518 Color: 0

Bin 1987: 63 of cap free
Amount of items: 2
Items: 
Size: 638037 Color: 1
Size: 361901 Color: 0

Bin 1988: 63 of cap free
Amount of items: 2
Items: 
Size: 647701 Color: 1
Size: 352237 Color: 0

Bin 1989: 63 of cap free
Amount of items: 2
Items: 
Size: 655185 Color: 0
Size: 344753 Color: 1

Bin 1990: 63 of cap free
Amount of items: 2
Items: 
Size: 658884 Color: 0
Size: 341054 Color: 1

Bin 1991: 63 of cap free
Amount of items: 2
Items: 
Size: 663880 Color: 0
Size: 336058 Color: 1

Bin 1992: 63 of cap free
Amount of items: 2
Items: 
Size: 726057 Color: 1
Size: 273881 Color: 0

Bin 1993: 63 of cap free
Amount of items: 2
Items: 
Size: 756127 Color: 0
Size: 243811 Color: 1

Bin 1994: 63 of cap free
Amount of items: 2
Items: 
Size: 756515 Color: 1
Size: 243423 Color: 0

Bin 1995: 63 of cap free
Amount of items: 2
Items: 
Size: 786504 Color: 0
Size: 213434 Color: 1

Bin 1996: 64 of cap free
Amount of items: 2
Items: 
Size: 774801 Color: 1
Size: 225136 Color: 0

Bin 1997: 64 of cap free
Amount of items: 2
Items: 
Size: 559325 Color: 1
Size: 440612 Color: 0

Bin 1998: 64 of cap free
Amount of items: 2
Items: 
Size: 755693 Color: 0
Size: 244244 Color: 1

Bin 1999: 64 of cap free
Amount of items: 2
Items: 
Size: 503057 Color: 1
Size: 496880 Color: 0

Bin 2000: 64 of cap free
Amount of items: 2
Items: 
Size: 514901 Color: 0
Size: 485036 Color: 1

Bin 2001: 64 of cap free
Amount of items: 2
Items: 
Size: 523250 Color: 1
Size: 476687 Color: 0

Bin 2002: 64 of cap free
Amount of items: 2
Items: 
Size: 540467 Color: 1
Size: 459470 Color: 0

Bin 2003: 64 of cap free
Amount of items: 2
Items: 
Size: 623651 Color: 0
Size: 376286 Color: 1

Bin 2004: 64 of cap free
Amount of items: 2
Items: 
Size: 674195 Color: 1
Size: 325742 Color: 0

Bin 2005: 64 of cap free
Amount of items: 2
Items: 
Size: 713305 Color: 1
Size: 286632 Color: 0

Bin 2006: 64 of cap free
Amount of items: 2
Items: 
Size: 748954 Color: 1
Size: 250983 Color: 0

Bin 2007: 64 of cap free
Amount of items: 2
Items: 
Size: 777207 Color: 0
Size: 222730 Color: 1

Bin 2008: 64 of cap free
Amount of items: 2
Items: 
Size: 791905 Color: 1
Size: 208032 Color: 0

Bin 2009: 64 of cap free
Amount of items: 2
Items: 
Size: 797557 Color: 1
Size: 202380 Color: 0

Bin 2010: 65 of cap free
Amount of items: 3
Items: 
Size: 575488 Color: 0
Size: 306738 Color: 1
Size: 117710 Color: 0

Bin 2011: 65 of cap free
Amount of items: 2
Items: 
Size: 507526 Color: 0
Size: 492410 Color: 1

Bin 2012: 65 of cap free
Amount of items: 2
Items: 
Size: 509633 Color: 1
Size: 490303 Color: 0

Bin 2013: 65 of cap free
Amount of items: 2
Items: 
Size: 548784 Color: 1
Size: 451152 Color: 0

Bin 2014: 65 of cap free
Amount of items: 2
Items: 
Size: 564251 Color: 0
Size: 435685 Color: 1

Bin 2015: 65 of cap free
Amount of items: 2
Items: 
Size: 589951 Color: 1
Size: 409985 Color: 0

Bin 2016: 65 of cap free
Amount of items: 2
Items: 
Size: 597695 Color: 1
Size: 402241 Color: 0

Bin 2017: 65 of cap free
Amount of items: 2
Items: 
Size: 638739 Color: 1
Size: 361197 Color: 0

Bin 2018: 65 of cap free
Amount of items: 2
Items: 
Size: 709727 Color: 1
Size: 290209 Color: 0

Bin 2019: 65 of cap free
Amount of items: 2
Items: 
Size: 723863 Color: 1
Size: 276073 Color: 0

Bin 2020: 66 of cap free
Amount of items: 3
Items: 
Size: 574299 Color: 1
Size: 269590 Color: 0
Size: 156046 Color: 0

Bin 2021: 66 of cap free
Amount of items: 3
Items: 
Size: 604815 Color: 0
Size: 277587 Color: 0
Size: 117533 Color: 1

Bin 2022: 66 of cap free
Amount of items: 2
Items: 
Size: 525619 Color: 1
Size: 474316 Color: 0

Bin 2023: 66 of cap free
Amount of items: 2
Items: 
Size: 573275 Color: 1
Size: 426660 Color: 0

Bin 2024: 66 of cap free
Amount of items: 2
Items: 
Size: 669914 Color: 0
Size: 330021 Color: 1

Bin 2025: 66 of cap free
Amount of items: 2
Items: 
Size: 683121 Color: 0
Size: 316814 Color: 1

Bin 2026: 66 of cap free
Amount of items: 2
Items: 
Size: 691216 Color: 1
Size: 308719 Color: 0

Bin 2027: 66 of cap free
Amount of items: 2
Items: 
Size: 716005 Color: 1
Size: 283930 Color: 0

Bin 2028: 66 of cap free
Amount of items: 2
Items: 
Size: 724389 Color: 0
Size: 275546 Color: 1

Bin 2029: 66 of cap free
Amount of items: 2
Items: 
Size: 732786 Color: 1
Size: 267149 Color: 0

Bin 2030: 66 of cap free
Amount of items: 2
Items: 
Size: 746614 Color: 0
Size: 253321 Color: 1

Bin 2031: 66 of cap free
Amount of items: 2
Items: 
Size: 779003 Color: 1
Size: 220932 Color: 0

Bin 2032: 67 of cap free
Amount of items: 2
Items: 
Size: 677853 Color: 0
Size: 322081 Color: 1

Bin 2033: 67 of cap free
Amount of items: 2
Items: 
Size: 551713 Color: 1
Size: 448221 Color: 0

Bin 2034: 67 of cap free
Amount of items: 2
Items: 
Size: 549823 Color: 0
Size: 450111 Color: 1

Bin 2035: 67 of cap free
Amount of items: 2
Items: 
Size: 576482 Color: 1
Size: 423452 Color: 0

Bin 2036: 67 of cap free
Amount of items: 2
Items: 
Size: 588407 Color: 0
Size: 411527 Color: 1

Bin 2037: 67 of cap free
Amount of items: 2
Items: 
Size: 601657 Color: 1
Size: 398277 Color: 0

Bin 2038: 67 of cap free
Amount of items: 2
Items: 
Size: 610172 Color: 0
Size: 389762 Color: 1

Bin 2039: 67 of cap free
Amount of items: 2
Items: 
Size: 611138 Color: 0
Size: 388796 Color: 1

Bin 2040: 67 of cap free
Amount of items: 2
Items: 
Size: 614575 Color: 1
Size: 385359 Color: 0

Bin 2041: 67 of cap free
Amount of items: 2
Items: 
Size: 640029 Color: 0
Size: 359905 Color: 1

Bin 2042: 67 of cap free
Amount of items: 2
Items: 
Size: 662691 Color: 0
Size: 337243 Color: 1

Bin 2043: 67 of cap free
Amount of items: 2
Items: 
Size: 787957 Color: 0
Size: 211977 Color: 1

Bin 2044: 67 of cap free
Amount of items: 2
Items: 
Size: 796174 Color: 0
Size: 203760 Color: 1

Bin 2045: 68 of cap free
Amount of items: 2
Items: 
Size: 555318 Color: 1
Size: 444615 Color: 0

Bin 2046: 68 of cap free
Amount of items: 2
Items: 
Size: 563451 Color: 0
Size: 436482 Color: 1

Bin 2047: 68 of cap free
Amount of items: 2
Items: 
Size: 578222 Color: 0
Size: 421711 Color: 1

Bin 2048: 68 of cap free
Amount of items: 2
Items: 
Size: 583346 Color: 1
Size: 416587 Color: 0

Bin 2049: 68 of cap free
Amount of items: 2
Items: 
Size: 608334 Color: 1
Size: 391599 Color: 0

Bin 2050: 68 of cap free
Amount of items: 2
Items: 
Size: 611748 Color: 0
Size: 388185 Color: 1

Bin 2051: 68 of cap free
Amount of items: 2
Items: 
Size: 651832 Color: 1
Size: 348101 Color: 0

Bin 2052: 68 of cap free
Amount of items: 2
Items: 
Size: 671469 Color: 0
Size: 328464 Color: 1

Bin 2053: 68 of cap free
Amount of items: 2
Items: 
Size: 698338 Color: 1
Size: 301595 Color: 0

Bin 2054: 68 of cap free
Amount of items: 2
Items: 
Size: 710870 Color: 1
Size: 289063 Color: 0

Bin 2055: 68 of cap free
Amount of items: 2
Items: 
Size: 774135 Color: 0
Size: 225798 Color: 1

Bin 2056: 68 of cap free
Amount of items: 2
Items: 
Size: 784891 Color: 0
Size: 215042 Color: 1

Bin 2057: 69 of cap free
Amount of items: 2
Items: 
Size: 520706 Color: 0
Size: 479226 Color: 1

Bin 2058: 69 of cap free
Amount of items: 5
Items: 
Size: 303904 Color: 0
Size: 199381 Color: 1
Size: 167837 Color: 0
Size: 167244 Color: 0
Size: 161566 Color: 1

Bin 2059: 69 of cap free
Amount of items: 2
Items: 
Size: 508952 Color: 0
Size: 490980 Color: 1

Bin 2060: 69 of cap free
Amount of items: 2
Items: 
Size: 525329 Color: 1
Size: 474603 Color: 0

Bin 2061: 69 of cap free
Amount of items: 2
Items: 
Size: 558729 Color: 1
Size: 441203 Color: 0

Bin 2062: 69 of cap free
Amount of items: 2
Items: 
Size: 569536 Color: 0
Size: 430396 Color: 1

Bin 2063: 69 of cap free
Amount of items: 2
Items: 
Size: 678339 Color: 1
Size: 321593 Color: 0

Bin 2064: 69 of cap free
Amount of items: 2
Items: 
Size: 693558 Color: 1
Size: 306374 Color: 0

Bin 2065: 69 of cap free
Amount of items: 2
Items: 
Size: 712759 Color: 1
Size: 287173 Color: 0

Bin 2066: 69 of cap free
Amount of items: 2
Items: 
Size: 721897 Color: 0
Size: 278035 Color: 1

Bin 2067: 69 of cap free
Amount of items: 2
Items: 
Size: 726469 Color: 1
Size: 273463 Color: 0

Bin 2068: 69 of cap free
Amount of items: 2
Items: 
Size: 729490 Color: 0
Size: 270442 Color: 1

Bin 2069: 69 of cap free
Amount of items: 2
Items: 
Size: 784667 Color: 1
Size: 215265 Color: 0

Bin 2070: 70 of cap free
Amount of items: 3
Items: 
Size: 738724 Color: 1
Size: 134395 Color: 1
Size: 126812 Color: 0

Bin 2071: 70 of cap free
Amount of items: 2
Items: 
Size: 508175 Color: 1
Size: 491756 Color: 0

Bin 2072: 70 of cap free
Amount of items: 2
Items: 
Size: 517750 Color: 0
Size: 482181 Color: 1

Bin 2073: 70 of cap free
Amount of items: 2
Items: 
Size: 577264 Color: 1
Size: 422667 Color: 0

Bin 2074: 70 of cap free
Amount of items: 2
Items: 
Size: 640911 Color: 0
Size: 359020 Color: 1

Bin 2075: 70 of cap free
Amount of items: 2
Items: 
Size: 666645 Color: 1
Size: 333286 Color: 0

Bin 2076: 70 of cap free
Amount of items: 2
Items: 
Size: 675318 Color: 1
Size: 324613 Color: 0

Bin 2077: 70 of cap free
Amount of items: 2
Items: 
Size: 762808 Color: 1
Size: 237123 Color: 0

Bin 2078: 70 of cap free
Amount of items: 2
Items: 
Size: 771115 Color: 0
Size: 228816 Color: 1

Bin 2079: 70 of cap free
Amount of items: 2
Items: 
Size: 776428 Color: 1
Size: 223503 Color: 0

Bin 2080: 70 of cap free
Amount of items: 2
Items: 
Size: 795543 Color: 0
Size: 204388 Color: 1

Bin 2081: 71 of cap free
Amount of items: 2
Items: 
Size: 782826 Color: 1
Size: 217104 Color: 0

Bin 2082: 71 of cap free
Amount of items: 2
Items: 
Size: 567059 Color: 1
Size: 432871 Color: 0

Bin 2083: 71 of cap free
Amount of items: 2
Items: 
Size: 598214 Color: 1
Size: 401716 Color: 0

Bin 2084: 71 of cap free
Amount of items: 2
Items: 
Size: 623981 Color: 0
Size: 375949 Color: 1

Bin 2085: 71 of cap free
Amount of items: 2
Items: 
Size: 647145 Color: 1
Size: 352785 Color: 0

Bin 2086: 71 of cap free
Amount of items: 2
Items: 
Size: 661273 Color: 1
Size: 338657 Color: 0

Bin 2087: 71 of cap free
Amount of items: 2
Items: 
Size: 668512 Color: 0
Size: 331418 Color: 1

Bin 2088: 71 of cap free
Amount of items: 2
Items: 
Size: 673159 Color: 1
Size: 326771 Color: 0

Bin 2089: 71 of cap free
Amount of items: 2
Items: 
Size: 685176 Color: 0
Size: 314754 Color: 1

Bin 2090: 71 of cap free
Amount of items: 2
Items: 
Size: 690881 Color: 0
Size: 309049 Color: 1

Bin 2091: 71 of cap free
Amount of items: 2
Items: 
Size: 702546 Color: 1
Size: 297384 Color: 0

Bin 2092: 71 of cap free
Amount of items: 2
Items: 
Size: 737797 Color: 0
Size: 262133 Color: 1

Bin 2093: 71 of cap free
Amount of items: 2
Items: 
Size: 742401 Color: 1
Size: 257529 Color: 0

Bin 2094: 71 of cap free
Amount of items: 2
Items: 
Size: 799278 Color: 1
Size: 200652 Color: 0

Bin 2095: 72 of cap free
Amount of items: 6
Items: 
Size: 190890 Color: 0
Size: 166421 Color: 0
Size: 165078 Color: 0
Size: 160235 Color: 1
Size: 160087 Color: 1
Size: 157218 Color: 1

Bin 2096: 72 of cap free
Amount of items: 2
Items: 
Size: 526038 Color: 1
Size: 473891 Color: 0

Bin 2097: 72 of cap free
Amount of items: 2
Items: 
Size: 534633 Color: 0
Size: 465296 Color: 1

Bin 2098: 72 of cap free
Amount of items: 2
Items: 
Size: 558862 Color: 1
Size: 441067 Color: 0

Bin 2099: 72 of cap free
Amount of items: 2
Items: 
Size: 559924 Color: 0
Size: 440005 Color: 1

Bin 2100: 72 of cap free
Amount of items: 2
Items: 
Size: 568328 Color: 1
Size: 431601 Color: 0

Bin 2101: 72 of cap free
Amount of items: 2
Items: 
Size: 581805 Color: 1
Size: 418124 Color: 0

Bin 2102: 72 of cap free
Amount of items: 2
Items: 
Size: 618574 Color: 1
Size: 381355 Color: 0

Bin 2103: 72 of cap free
Amount of items: 2
Items: 
Size: 626666 Color: 0
Size: 373263 Color: 1

Bin 2104: 72 of cap free
Amount of items: 2
Items: 
Size: 665068 Color: 1
Size: 334861 Color: 0

Bin 2105: 72 of cap free
Amount of items: 2
Items: 
Size: 698563 Color: 1
Size: 301366 Color: 0

Bin 2106: 72 of cap free
Amount of items: 2
Items: 
Size: 725871 Color: 1
Size: 274058 Color: 0

Bin 2107: 72 of cap free
Amount of items: 2
Items: 
Size: 737796 Color: 0
Size: 262133 Color: 1

Bin 2108: 72 of cap free
Amount of items: 2
Items: 
Size: 785727 Color: 1
Size: 214202 Color: 0

Bin 2109: 73 of cap free
Amount of items: 2
Items: 
Size: 571403 Color: 1
Size: 428525 Color: 0

Bin 2110: 73 of cap free
Amount of items: 2
Items: 
Size: 618933 Color: 1
Size: 380995 Color: 0

Bin 2111: 73 of cap free
Amount of items: 2
Items: 
Size: 704427 Color: 1
Size: 295501 Color: 0

Bin 2112: 73 of cap free
Amount of items: 2
Items: 
Size: 731470 Color: 0
Size: 268458 Color: 1

Bin 2113: 73 of cap free
Amount of items: 2
Items: 
Size: 756875 Color: 1
Size: 243053 Color: 0

Bin 2114: 74 of cap free
Amount of items: 2
Items: 
Size: 537287 Color: 1
Size: 462640 Color: 0

Bin 2115: 74 of cap free
Amount of items: 2
Items: 
Size: 570691 Color: 0
Size: 429236 Color: 1

Bin 2116: 74 of cap free
Amount of items: 2
Items: 
Size: 520101 Color: 1
Size: 479826 Color: 0

Bin 2117: 74 of cap free
Amount of items: 2
Items: 
Size: 536273 Color: 0
Size: 463654 Color: 1

Bin 2118: 74 of cap free
Amount of items: 2
Items: 
Size: 682979 Color: 1
Size: 316948 Color: 0

Bin 2119: 74 of cap free
Amount of items: 2
Items: 
Size: 689060 Color: 0
Size: 310867 Color: 1

Bin 2120: 74 of cap free
Amount of items: 2
Items: 
Size: 699815 Color: 0
Size: 300112 Color: 1

Bin 2121: 74 of cap free
Amount of items: 2
Items: 
Size: 701565 Color: 0
Size: 298362 Color: 1

Bin 2122: 74 of cap free
Amount of items: 2
Items: 
Size: 705616 Color: 0
Size: 294311 Color: 1

Bin 2123: 74 of cap free
Amount of items: 2
Items: 
Size: 722854 Color: 1
Size: 277073 Color: 0

Bin 2124: 74 of cap free
Amount of items: 2
Items: 
Size: 727996 Color: 0
Size: 271931 Color: 1

Bin 2125: 74 of cap free
Amount of items: 2
Items: 
Size: 743831 Color: 0
Size: 256096 Color: 1

Bin 2126: 74 of cap free
Amount of items: 2
Items: 
Size: 745261 Color: 0
Size: 254666 Color: 1

Bin 2127: 74 of cap free
Amount of items: 2
Items: 
Size: 773351 Color: 1
Size: 226576 Color: 0

Bin 2128: 74 of cap free
Amount of items: 2
Items: 
Size: 781947 Color: 1
Size: 217980 Color: 0

Bin 2129: 75 of cap free
Amount of items: 5
Items: 
Size: 218080 Color: 0
Size: 197316 Color: 0
Size: 196529 Color: 1
Size: 196500 Color: 1
Size: 191501 Color: 0

Bin 2130: 75 of cap free
Amount of items: 2
Items: 
Size: 510503 Color: 0
Size: 489423 Color: 1

Bin 2131: 75 of cap free
Amount of items: 2
Items: 
Size: 595428 Color: 1
Size: 404498 Color: 0

Bin 2132: 75 of cap free
Amount of items: 2
Items: 
Size: 607811 Color: 0
Size: 392115 Color: 1

Bin 2133: 75 of cap free
Amount of items: 2
Items: 
Size: 616337 Color: 1
Size: 383589 Color: 0

Bin 2134: 75 of cap free
Amount of items: 2
Items: 
Size: 647978 Color: 0
Size: 351948 Color: 1

Bin 2135: 75 of cap free
Amount of items: 2
Items: 
Size: 658732 Color: 1
Size: 341194 Color: 0

Bin 2136: 75 of cap free
Amount of items: 2
Items: 
Size: 698323 Color: 0
Size: 301603 Color: 1

Bin 2137: 75 of cap free
Amount of items: 2
Items: 
Size: 706235 Color: 0
Size: 293691 Color: 1

Bin 2138: 75 of cap free
Amount of items: 2
Items: 
Size: 758376 Color: 1
Size: 241550 Color: 0

Bin 2139: 75 of cap free
Amount of items: 2
Items: 
Size: 795158 Color: 0
Size: 204768 Color: 1

Bin 2140: 76 of cap free
Amount of items: 2
Items: 
Size: 523974 Color: 1
Size: 475951 Color: 0

Bin 2141: 76 of cap free
Amount of items: 2
Items: 
Size: 531110 Color: 1
Size: 468815 Color: 0

Bin 2142: 76 of cap free
Amount of items: 2
Items: 
Size: 551355 Color: 0
Size: 448570 Color: 1

Bin 2143: 76 of cap free
Amount of items: 2
Items: 
Size: 602703 Color: 0
Size: 397222 Color: 1

Bin 2144: 76 of cap free
Amount of items: 2
Items: 
Size: 606274 Color: 1
Size: 393651 Color: 0

Bin 2145: 76 of cap free
Amount of items: 2
Items: 
Size: 634212 Color: 1
Size: 365713 Color: 0

Bin 2146: 76 of cap free
Amount of items: 2
Items: 
Size: 683190 Color: 0
Size: 316735 Color: 1

Bin 2147: 76 of cap free
Amount of items: 2
Items: 
Size: 696802 Color: 0
Size: 303123 Color: 1

Bin 2148: 76 of cap free
Amount of items: 2
Items: 
Size: 711239 Color: 0
Size: 288686 Color: 1

Bin 2149: 76 of cap free
Amount of items: 2
Items: 
Size: 722701 Color: 1
Size: 277224 Color: 0

Bin 2150: 76 of cap free
Amount of items: 2
Items: 
Size: 763879 Color: 1
Size: 236046 Color: 0

Bin 2151: 76 of cap free
Amount of items: 2
Items: 
Size: 786137 Color: 0
Size: 213788 Color: 1

Bin 2152: 76 of cap free
Amount of items: 2
Items: 
Size: 791524 Color: 1
Size: 208401 Color: 0

Bin 2153: 77 of cap free
Amount of items: 2
Items: 
Size: 580350 Color: 1
Size: 419574 Color: 0

Bin 2154: 77 of cap free
Amount of items: 2
Items: 
Size: 624162 Color: 1
Size: 375762 Color: 0

Bin 2155: 77 of cap free
Amount of items: 2
Items: 
Size: 685635 Color: 1
Size: 314289 Color: 0

Bin 2156: 77 of cap free
Amount of items: 2
Items: 
Size: 696671 Color: 0
Size: 303253 Color: 1

Bin 2157: 77 of cap free
Amount of items: 2
Items: 
Size: 703718 Color: 0
Size: 296206 Color: 1

Bin 2158: 77 of cap free
Amount of items: 2
Items: 
Size: 720198 Color: 0
Size: 279726 Color: 1

Bin 2159: 77 of cap free
Amount of items: 2
Items: 
Size: 751616 Color: 1
Size: 248308 Color: 0

Bin 2160: 78 of cap free
Amount of items: 3
Items: 
Size: 444828 Color: 0
Size: 429474 Color: 1
Size: 125621 Color: 1

Bin 2161: 78 of cap free
Amount of items: 3
Items: 
Size: 505605 Color: 1
Size: 374792 Color: 1
Size: 119526 Color: 0

Bin 2162: 78 of cap free
Amount of items: 2
Items: 
Size: 505459 Color: 0
Size: 494464 Color: 1

Bin 2163: 78 of cap free
Amount of items: 2
Items: 
Size: 510796 Color: 0
Size: 489127 Color: 1

Bin 2164: 78 of cap free
Amount of items: 2
Items: 
Size: 533351 Color: 1
Size: 466572 Color: 0

Bin 2165: 78 of cap free
Amount of items: 2
Items: 
Size: 634890 Color: 0
Size: 365033 Color: 1

Bin 2166: 78 of cap free
Amount of items: 2
Items: 
Size: 640020 Color: 0
Size: 359903 Color: 1

Bin 2167: 78 of cap free
Amount of items: 2
Items: 
Size: 645918 Color: 0
Size: 354005 Color: 1

Bin 2168: 78 of cap free
Amount of items: 2
Items: 
Size: 651785 Color: 0
Size: 348138 Color: 1

Bin 2169: 78 of cap free
Amount of items: 2
Items: 
Size: 683692 Color: 0
Size: 316231 Color: 1

Bin 2170: 78 of cap free
Amount of items: 2
Items: 
Size: 724732 Color: 0
Size: 275191 Color: 1

Bin 2171: 78 of cap free
Amount of items: 2
Items: 
Size: 724924 Color: 0
Size: 274999 Color: 1

Bin 2172: 79 of cap free
Amount of items: 2
Items: 
Size: 695564 Color: 0
Size: 304358 Color: 1

Bin 2173: 79 of cap free
Amount of items: 2
Items: 
Size: 758226 Color: 0
Size: 241696 Color: 1

Bin 2174: 79 of cap free
Amount of items: 2
Items: 
Size: 538162 Color: 0
Size: 461760 Color: 1

Bin 2175: 79 of cap free
Amount of items: 2
Items: 
Size: 543508 Color: 0
Size: 456414 Color: 1

Bin 2176: 79 of cap free
Amount of items: 2
Items: 
Size: 601954 Color: 0
Size: 397968 Color: 1

Bin 2177: 79 of cap free
Amount of items: 2
Items: 
Size: 630375 Color: 1
Size: 369547 Color: 0

Bin 2178: 79 of cap free
Amount of items: 2
Items: 
Size: 660293 Color: 0
Size: 339629 Color: 1

Bin 2179: 79 of cap free
Amount of items: 2
Items: 
Size: 676514 Color: 0
Size: 323408 Color: 1

Bin 2180: 79 of cap free
Amount of items: 2
Items: 
Size: 783541 Color: 0
Size: 216381 Color: 1

Bin 2181: 80 of cap free
Amount of items: 2
Items: 
Size: 620188 Color: 0
Size: 379733 Color: 1

Bin 2182: 80 of cap free
Amount of items: 2
Items: 
Size: 615904 Color: 1
Size: 384017 Color: 0

Bin 2183: 80 of cap free
Amount of items: 2
Items: 
Size: 658599 Color: 1
Size: 341322 Color: 0

Bin 2184: 80 of cap free
Amount of items: 2
Items: 
Size: 697382 Color: 0
Size: 302539 Color: 1

Bin 2185: 80 of cap free
Amount of items: 2
Items: 
Size: 717955 Color: 1
Size: 281966 Color: 0

Bin 2186: 80 of cap free
Amount of items: 2
Items: 
Size: 722148 Color: 0
Size: 277773 Color: 1

Bin 2187: 80 of cap free
Amount of items: 2
Items: 
Size: 746302 Color: 0
Size: 253619 Color: 1

Bin 2188: 81 of cap free
Amount of items: 2
Items: 
Size: 555066 Color: 1
Size: 444854 Color: 0

Bin 2189: 81 of cap free
Amount of items: 2
Items: 
Size: 506393 Color: 1
Size: 493527 Color: 0

Bin 2190: 81 of cap free
Amount of items: 2
Items: 
Size: 524969 Color: 1
Size: 474951 Color: 0

Bin 2191: 81 of cap free
Amount of items: 2
Items: 
Size: 546557 Color: 1
Size: 453363 Color: 0

Bin 2192: 81 of cap free
Amount of items: 2
Items: 
Size: 605072 Color: 0
Size: 394848 Color: 1

Bin 2193: 81 of cap free
Amount of items: 2
Items: 
Size: 637501 Color: 1
Size: 362419 Color: 0

Bin 2194: 81 of cap free
Amount of items: 2
Items: 
Size: 652736 Color: 1
Size: 347184 Color: 0

Bin 2195: 81 of cap free
Amount of items: 2
Items: 
Size: 667438 Color: 0
Size: 332482 Color: 1

Bin 2196: 81 of cap free
Amount of items: 2
Items: 
Size: 674756 Color: 0
Size: 325164 Color: 1

Bin 2197: 81 of cap free
Amount of items: 2
Items: 
Size: 684850 Color: 1
Size: 315070 Color: 0

Bin 2198: 81 of cap free
Amount of items: 2
Items: 
Size: 710194 Color: 0
Size: 289726 Color: 1

Bin 2199: 81 of cap free
Amount of items: 2
Items: 
Size: 717352 Color: 1
Size: 282568 Color: 0

Bin 2200: 82 of cap free
Amount of items: 2
Items: 
Size: 518641 Color: 0
Size: 481278 Color: 1

Bin 2201: 82 of cap free
Amount of items: 2
Items: 
Size: 586848 Color: 0
Size: 413071 Color: 1

Bin 2202: 82 of cap free
Amount of items: 2
Items: 
Size: 622524 Color: 1
Size: 377395 Color: 0

Bin 2203: 82 of cap free
Amount of items: 2
Items: 
Size: 661813 Color: 1
Size: 338106 Color: 0

Bin 2204: 82 of cap free
Amount of items: 2
Items: 
Size: 679872 Color: 1
Size: 320047 Color: 0

Bin 2205: 82 of cap free
Amount of items: 2
Items: 
Size: 684950 Color: 0
Size: 314969 Color: 1

Bin 2206: 82 of cap free
Amount of items: 2
Items: 
Size: 706474 Color: 0
Size: 293445 Color: 1

Bin 2207: 82 of cap free
Amount of items: 2
Items: 
Size: 783172 Color: 1
Size: 216747 Color: 0

Bin 2208: 82 of cap free
Amount of items: 2
Items: 
Size: 791241 Color: 1
Size: 208678 Color: 0

Bin 2209: 83 of cap free
Amount of items: 2
Items: 
Size: 789566 Color: 0
Size: 210352 Color: 1

Bin 2210: 83 of cap free
Amount of items: 2
Items: 
Size: 594200 Color: 1
Size: 405718 Color: 0

Bin 2211: 83 of cap free
Amount of items: 2
Items: 
Size: 544065 Color: 0
Size: 455853 Color: 1

Bin 2212: 83 of cap free
Amount of items: 2
Items: 
Size: 566389 Color: 0
Size: 433529 Color: 1

Bin 2213: 83 of cap free
Amount of items: 2
Items: 
Size: 574025 Color: 0
Size: 425893 Color: 1

Bin 2214: 83 of cap free
Amount of items: 2
Items: 
Size: 598885 Color: 1
Size: 401033 Color: 0

Bin 2215: 83 of cap free
Amount of items: 2
Items: 
Size: 632672 Color: 0
Size: 367246 Color: 1

Bin 2216: 83 of cap free
Amount of items: 2
Items: 
Size: 703184 Color: 1
Size: 296734 Color: 0

Bin 2217: 83 of cap free
Amount of items: 2
Items: 
Size: 740273 Color: 1
Size: 259645 Color: 0

Bin 2218: 83 of cap free
Amount of items: 2
Items: 
Size: 755866 Color: 1
Size: 244052 Color: 0

Bin 2219: 83 of cap free
Amount of items: 2
Items: 
Size: 757314 Color: 0
Size: 242604 Color: 1

Bin 2220: 83 of cap free
Amount of items: 2
Items: 
Size: 784662 Color: 1
Size: 215256 Color: 0

Bin 2221: 84 of cap free
Amount of items: 2
Items: 
Size: 761659 Color: 0
Size: 238258 Color: 1

Bin 2222: 84 of cap free
Amount of items: 2
Items: 
Size: 501889 Color: 1
Size: 498028 Color: 0

Bin 2223: 84 of cap free
Amount of items: 2
Items: 
Size: 538394 Color: 1
Size: 461523 Color: 0

Bin 2224: 84 of cap free
Amount of items: 2
Items: 
Size: 632740 Color: 1
Size: 367177 Color: 0

Bin 2225: 84 of cap free
Amount of items: 2
Items: 
Size: 633629 Color: 1
Size: 366288 Color: 0

Bin 2226: 84 of cap free
Amount of items: 2
Items: 
Size: 655873 Color: 0
Size: 344044 Color: 1

Bin 2227: 84 of cap free
Amount of items: 2
Items: 
Size: 660773 Color: 0
Size: 339144 Color: 1

Bin 2228: 84 of cap free
Amount of items: 2
Items: 
Size: 681413 Color: 1
Size: 318504 Color: 0

Bin 2229: 84 of cap free
Amount of items: 2
Items: 
Size: 709929 Color: 1
Size: 289988 Color: 0

Bin 2230: 84 of cap free
Amount of items: 2
Items: 
Size: 771800 Color: 1
Size: 228117 Color: 0

Bin 2231: 84 of cap free
Amount of items: 2
Items: 
Size: 777080 Color: 0
Size: 222837 Color: 1

Bin 2232: 85 of cap free
Amount of items: 2
Items: 
Size: 500299 Color: 0
Size: 499617 Color: 1

Bin 2233: 85 of cap free
Amount of items: 2
Items: 
Size: 569441 Color: 1
Size: 430475 Color: 0

Bin 2234: 85 of cap free
Amount of items: 2
Items: 
Size: 607300 Color: 0
Size: 392616 Color: 1

Bin 2235: 85 of cap free
Amount of items: 2
Items: 
Size: 615348 Color: 0
Size: 384568 Color: 1

Bin 2236: 85 of cap free
Amount of items: 2
Items: 
Size: 627312 Color: 0
Size: 372604 Color: 1

Bin 2237: 85 of cap free
Amount of items: 2
Items: 
Size: 633181 Color: 0
Size: 366735 Color: 1

Bin 2238: 85 of cap free
Amount of items: 2
Items: 
Size: 635186 Color: 1
Size: 364730 Color: 0

Bin 2239: 85 of cap free
Amount of items: 2
Items: 
Size: 639495 Color: 0
Size: 360421 Color: 1

Bin 2240: 85 of cap free
Amount of items: 2
Items: 
Size: 642704 Color: 1
Size: 357212 Color: 0

Bin 2241: 85 of cap free
Amount of items: 2
Items: 
Size: 647139 Color: 1
Size: 352777 Color: 0

Bin 2242: 85 of cap free
Amount of items: 2
Items: 
Size: 650580 Color: 0
Size: 349336 Color: 1

Bin 2243: 85 of cap free
Amount of items: 2
Items: 
Size: 746387 Color: 0
Size: 253529 Color: 1

Bin 2244: 85 of cap free
Amount of items: 2
Items: 
Size: 772203 Color: 0
Size: 227713 Color: 1

Bin 2245: 85 of cap free
Amount of items: 2
Items: 
Size: 799442 Color: 1
Size: 200474 Color: 0

Bin 2246: 86 of cap free
Amount of items: 3
Items: 
Size: 557698 Color: 1
Size: 317922 Color: 0
Size: 124295 Color: 1

Bin 2247: 86 of cap free
Amount of items: 2
Items: 
Size: 519218 Color: 1
Size: 480697 Color: 0

Bin 2248: 86 of cap free
Amount of items: 2
Items: 
Size: 581029 Color: 1
Size: 418886 Color: 0

Bin 2249: 86 of cap free
Amount of items: 2
Items: 
Size: 629947 Color: 1
Size: 369968 Color: 0

Bin 2250: 86 of cap free
Amount of items: 2
Items: 
Size: 636064 Color: 1
Size: 363851 Color: 0

Bin 2251: 86 of cap free
Amount of items: 2
Items: 
Size: 637327 Color: 0
Size: 362588 Color: 1

Bin 2252: 86 of cap free
Amount of items: 2
Items: 
Size: 656746 Color: 0
Size: 343169 Color: 1

Bin 2253: 86 of cap free
Amount of items: 2
Items: 
Size: 662401 Color: 1
Size: 337514 Color: 0

Bin 2254: 86 of cap free
Amount of items: 2
Items: 
Size: 748035 Color: 1
Size: 251880 Color: 0

Bin 2255: 86 of cap free
Amount of items: 2
Items: 
Size: 774803 Color: 0
Size: 225112 Color: 1

Bin 2256: 87 of cap free
Amount of items: 2
Items: 
Size: 550053 Color: 0
Size: 449861 Color: 1

Bin 2257: 87 of cap free
Amount of items: 2
Items: 
Size: 577180 Color: 0
Size: 422734 Color: 1

Bin 2258: 87 of cap free
Amount of items: 2
Items: 
Size: 616943 Color: 1
Size: 382971 Color: 0

Bin 2259: 87 of cap free
Amount of items: 2
Items: 
Size: 685172 Color: 0
Size: 314742 Color: 1

Bin 2260: 87 of cap free
Amount of items: 2
Items: 
Size: 726296 Color: 1
Size: 273618 Color: 0

Bin 2261: 87 of cap free
Amount of items: 2
Items: 
Size: 794853 Color: 0
Size: 205061 Color: 1

Bin 2262: 88 of cap free
Amount of items: 2
Items: 
Size: 505665 Color: 0
Size: 494248 Color: 1

Bin 2263: 88 of cap free
Amount of items: 2
Items: 
Size: 513531 Color: 1
Size: 486382 Color: 0

Bin 2264: 88 of cap free
Amount of items: 2
Items: 
Size: 572031 Color: 1
Size: 427882 Color: 0

Bin 2265: 88 of cap free
Amount of items: 2
Items: 
Size: 646921 Color: 1
Size: 352992 Color: 0

Bin 2266: 88 of cap free
Amount of items: 2
Items: 
Size: 670357 Color: 1
Size: 329556 Color: 0

Bin 2267: 89 of cap free
Amount of items: 2
Items: 
Size: 736642 Color: 0
Size: 263270 Color: 1

Bin 2268: 89 of cap free
Amount of items: 2
Items: 
Size: 510074 Color: 0
Size: 489838 Color: 1

Bin 2269: 89 of cap free
Amount of items: 2
Items: 
Size: 566225 Color: 0
Size: 433687 Color: 1

Bin 2270: 89 of cap free
Amount of items: 2
Items: 
Size: 610903 Color: 0
Size: 389009 Color: 1

Bin 2271: 89 of cap free
Amount of items: 2
Items: 
Size: 677213 Color: 0
Size: 322699 Color: 1

Bin 2272: 89 of cap free
Amount of items: 2
Items: 
Size: 699695 Color: 1
Size: 300217 Color: 0

Bin 2273: 89 of cap free
Amount of items: 2
Items: 
Size: 754901 Color: 1
Size: 245011 Color: 0

Bin 2274: 89 of cap free
Amount of items: 2
Items: 
Size: 763077 Color: 0
Size: 236835 Color: 1

Bin 2275: 90 of cap free
Amount of items: 2
Items: 
Size: 546946 Color: 0
Size: 452965 Color: 1

Bin 2276: 90 of cap free
Amount of items: 2
Items: 
Size: 647239 Color: 0
Size: 352672 Color: 1

Bin 2277: 90 of cap free
Amount of items: 2
Items: 
Size: 656834 Color: 1
Size: 343077 Color: 0

Bin 2278: 90 of cap free
Amount of items: 2
Items: 
Size: 688752 Color: 0
Size: 311159 Color: 1

Bin 2279: 90 of cap free
Amount of items: 2
Items: 
Size: 719515 Color: 1
Size: 280396 Color: 0

Bin 2280: 90 of cap free
Amount of items: 2
Items: 
Size: 732388 Color: 0
Size: 267523 Color: 1

Bin 2281: 90 of cap free
Amount of items: 2
Items: 
Size: 773072 Color: 1
Size: 226839 Color: 0

Bin 2282: 91 of cap free
Amount of items: 2
Items: 
Size: 534762 Color: 0
Size: 465148 Color: 1

Bin 2283: 91 of cap free
Amount of items: 2
Items: 
Size: 601249 Color: 0
Size: 398661 Color: 1

Bin 2284: 91 of cap free
Amount of items: 2
Items: 
Size: 616560 Color: 0
Size: 383350 Color: 1

Bin 2285: 91 of cap free
Amount of items: 2
Items: 
Size: 626237 Color: 1
Size: 373673 Color: 0

Bin 2286: 91 of cap free
Amount of items: 2
Items: 
Size: 653872 Color: 1
Size: 346038 Color: 0

Bin 2287: 91 of cap free
Amount of items: 2
Items: 
Size: 666657 Color: 0
Size: 333253 Color: 1

Bin 2288: 91 of cap free
Amount of items: 2
Items: 
Size: 666657 Color: 0
Size: 333253 Color: 1

Bin 2289: 91 of cap free
Amount of items: 2
Items: 
Size: 690846 Color: 1
Size: 309064 Color: 0

Bin 2290: 91 of cap free
Amount of items: 2
Items: 
Size: 752245 Color: 1
Size: 247665 Color: 0

Bin 2291: 92 of cap free
Amount of items: 2
Items: 
Size: 522186 Color: 0
Size: 477723 Color: 1

Bin 2292: 92 of cap free
Amount of items: 2
Items: 
Size: 646535 Color: 0
Size: 353374 Color: 1

Bin 2293: 92 of cap free
Amount of items: 2
Items: 
Size: 653783 Color: 0
Size: 346126 Color: 1

Bin 2294: 92 of cap free
Amount of items: 2
Items: 
Size: 688938 Color: 0
Size: 310971 Color: 1

Bin 2295: 92 of cap free
Amount of items: 2
Items: 
Size: 691749 Color: 0
Size: 308160 Color: 1

Bin 2296: 92 of cap free
Amount of items: 2
Items: 
Size: 733161 Color: 0
Size: 266748 Color: 1

Bin 2297: 92 of cap free
Amount of items: 2
Items: 
Size: 778990 Color: 1
Size: 220919 Color: 0

Bin 2298: 92 of cap free
Amount of items: 2
Items: 
Size: 793677 Color: 1
Size: 206232 Color: 0

Bin 2299: 93 of cap free
Amount of items: 2
Items: 
Size: 507882 Color: 0
Size: 492026 Color: 1

Bin 2300: 93 of cap free
Amount of items: 2
Items: 
Size: 531912 Color: 1
Size: 467996 Color: 0

Bin 2301: 93 of cap free
Amount of items: 2
Items: 
Size: 591612 Color: 1
Size: 408296 Color: 0

Bin 2302: 93 of cap free
Amount of items: 2
Items: 
Size: 597699 Color: 0
Size: 402209 Color: 1

Bin 2303: 93 of cap free
Amount of items: 2
Items: 
Size: 603583 Color: 0
Size: 396325 Color: 1

Bin 2304: 93 of cap free
Amount of items: 2
Items: 
Size: 646438 Color: 0
Size: 353470 Color: 1

Bin 2305: 93 of cap free
Amount of items: 2
Items: 
Size: 663439 Color: 1
Size: 336469 Color: 0

Bin 2306: 93 of cap free
Amount of items: 2
Items: 
Size: 667873 Color: 0
Size: 332035 Color: 1

Bin 2307: 93 of cap free
Amount of items: 2
Items: 
Size: 692355 Color: 1
Size: 307553 Color: 0

Bin 2308: 93 of cap free
Amount of items: 2
Items: 
Size: 713261 Color: 0
Size: 286647 Color: 1

Bin 2309: 93 of cap free
Amount of items: 2
Items: 
Size: 772845 Color: 0
Size: 227063 Color: 1

Bin 2310: 94 of cap free
Amount of items: 2
Items: 
Size: 508039 Color: 0
Size: 491868 Color: 1

Bin 2311: 94 of cap free
Amount of items: 2
Items: 
Size: 552163 Color: 0
Size: 447744 Color: 1

Bin 2312: 94 of cap free
Amount of items: 2
Items: 
Size: 566122 Color: 0
Size: 433785 Color: 1

Bin 2313: 94 of cap free
Amount of items: 2
Items: 
Size: 566567 Color: 0
Size: 433340 Color: 1

Bin 2314: 94 of cap free
Amount of items: 2
Items: 
Size: 624283 Color: 0
Size: 375624 Color: 1

Bin 2315: 94 of cap free
Amount of items: 2
Items: 
Size: 632922 Color: 1
Size: 366985 Color: 0

Bin 2316: 94 of cap free
Amount of items: 2
Items: 
Size: 652505 Color: 1
Size: 347402 Color: 0

Bin 2317: 94 of cap free
Amount of items: 2
Items: 
Size: 688346 Color: 1
Size: 311561 Color: 0

Bin 2318: 94 of cap free
Amount of items: 2
Items: 
Size: 701708 Color: 1
Size: 298199 Color: 0

Bin 2319: 94 of cap free
Amount of items: 2
Items: 
Size: 798686 Color: 0
Size: 201221 Color: 1

Bin 2320: 95 of cap free
Amount of items: 2
Items: 
Size: 516663 Color: 0
Size: 483243 Color: 1

Bin 2321: 95 of cap free
Amount of items: 2
Items: 
Size: 547678 Color: 1
Size: 452228 Color: 0

Bin 2322: 95 of cap free
Amount of items: 2
Items: 
Size: 548106 Color: 1
Size: 451800 Color: 0

Bin 2323: 95 of cap free
Amount of items: 2
Items: 
Size: 584877 Color: 0
Size: 415029 Color: 1

Bin 2324: 95 of cap free
Amount of items: 2
Items: 
Size: 585518 Color: 1
Size: 414388 Color: 0

Bin 2325: 95 of cap free
Amount of items: 2
Items: 
Size: 627400 Color: 1
Size: 372506 Color: 0

Bin 2326: 95 of cap free
Amount of items: 2
Items: 
Size: 680698 Color: 1
Size: 319208 Color: 0

Bin 2327: 95 of cap free
Amount of items: 2
Items: 
Size: 700144 Color: 0
Size: 299762 Color: 1

Bin 2328: 95 of cap free
Amount of items: 2
Items: 
Size: 721033 Color: 0
Size: 278873 Color: 1

Bin 2329: 95 of cap free
Amount of items: 2
Items: 
Size: 722752 Color: 0
Size: 277154 Color: 1

Bin 2330: 95 of cap free
Amount of items: 2
Items: 
Size: 762844 Color: 0
Size: 237062 Color: 1

Bin 2331: 95 of cap free
Amount of items: 2
Items: 
Size: 794545 Color: 0
Size: 205361 Color: 1

Bin 2332: 95 of cap free
Amount of items: 2
Items: 
Size: 799715 Color: 1
Size: 200191 Color: 0

Bin 2333: 96 of cap free
Amount of items: 2
Items: 
Size: 605854 Color: 0
Size: 394051 Color: 1

Bin 2334: 96 of cap free
Amount of items: 2
Items: 
Size: 765439 Color: 0
Size: 234466 Color: 1

Bin 2335: 96 of cap free
Amount of items: 2
Items: 
Size: 571239 Color: 0
Size: 428666 Color: 1

Bin 2336: 96 of cap free
Amount of items: 2
Items: 
Size: 610963 Color: 1
Size: 388942 Color: 0

Bin 2337: 96 of cap free
Amount of items: 2
Items: 
Size: 669808 Color: 1
Size: 330097 Color: 0

Bin 2338: 96 of cap free
Amount of items: 2
Items: 
Size: 744556 Color: 0
Size: 255349 Color: 1

Bin 2339: 96 of cap free
Amount of items: 2
Items: 
Size: 760928 Color: 0
Size: 238977 Color: 1

Bin 2340: 96 of cap free
Amount of items: 2
Items: 
Size: 766931 Color: 1
Size: 232974 Color: 0

Bin 2341: 97 of cap free
Amount of items: 2
Items: 
Size: 581333 Color: 1
Size: 418571 Color: 0

Bin 2342: 97 of cap free
Amount of items: 2
Items: 
Size: 646891 Color: 0
Size: 353013 Color: 1

Bin 2343: 97 of cap free
Amount of items: 2
Items: 
Size: 656569 Color: 0
Size: 343335 Color: 1

Bin 2344: 97 of cap free
Amount of items: 2
Items: 
Size: 743473 Color: 0
Size: 256431 Color: 1

Bin 2345: 98 of cap free
Amount of items: 2
Items: 
Size: 529446 Color: 0
Size: 470457 Color: 1

Bin 2346: 98 of cap free
Amount of items: 2
Items: 
Size: 551654 Color: 0
Size: 448249 Color: 1

Bin 2347: 98 of cap free
Amount of items: 2
Items: 
Size: 565371 Color: 0
Size: 434532 Color: 1

Bin 2348: 98 of cap free
Amount of items: 2
Items: 
Size: 595668 Color: 0
Size: 404235 Color: 1

Bin 2349: 98 of cap free
Amount of items: 2
Items: 
Size: 595949 Color: 1
Size: 403954 Color: 0

Bin 2350: 98 of cap free
Amount of items: 2
Items: 
Size: 638221 Color: 1
Size: 361682 Color: 0

Bin 2351: 98 of cap free
Amount of items: 2
Items: 
Size: 656431 Color: 0
Size: 343472 Color: 1

Bin 2352: 98 of cap free
Amount of items: 2
Items: 
Size: 670814 Color: 0
Size: 329089 Color: 1

Bin 2353: 98 of cap free
Amount of items: 2
Items: 
Size: 704309 Color: 0
Size: 295594 Color: 1

Bin 2354: 99 of cap free
Amount of items: 2
Items: 
Size: 510531 Color: 1
Size: 489371 Color: 0

Bin 2355: 99 of cap free
Amount of items: 2
Items: 
Size: 543821 Color: 0
Size: 456081 Color: 1

Bin 2356: 99 of cap free
Amount of items: 2
Items: 
Size: 634627 Color: 1
Size: 365275 Color: 0

Bin 2357: 99 of cap free
Amount of items: 2
Items: 
Size: 669607 Color: 0
Size: 330295 Color: 1

Bin 2358: 100 of cap free
Amount of items: 2
Items: 
Size: 589105 Color: 0
Size: 410796 Color: 1

Bin 2359: 100 of cap free
Amount of items: 2
Items: 
Size: 507195 Color: 1
Size: 492706 Color: 0

Bin 2360: 100 of cap free
Amount of items: 2
Items: 
Size: 507837 Color: 1
Size: 492064 Color: 0

Bin 2361: 100 of cap free
Amount of items: 2
Items: 
Size: 524209 Color: 1
Size: 475692 Color: 0

Bin 2362: 100 of cap free
Amount of items: 2
Items: 
Size: 590181 Color: 1
Size: 409720 Color: 0

Bin 2363: 100 of cap free
Amount of items: 2
Items: 
Size: 608410 Color: 0
Size: 391491 Color: 1

Bin 2364: 100 of cap free
Amount of items: 2
Items: 
Size: 710290 Color: 0
Size: 289611 Color: 1

Bin 2365: 100 of cap free
Amount of items: 2
Items: 
Size: 763470 Color: 0
Size: 236431 Color: 1

Bin 2366: 100 of cap free
Amount of items: 2
Items: 
Size: 799114 Color: 1
Size: 200787 Color: 0

Bin 2367: 101 of cap free
Amount of items: 2
Items: 
Size: 534482 Color: 0
Size: 465418 Color: 1

Bin 2368: 101 of cap free
Amount of items: 2
Items: 
Size: 545157 Color: 1
Size: 454743 Color: 0

Bin 2369: 101 of cap free
Amount of items: 2
Items: 
Size: 555305 Color: 0
Size: 444595 Color: 1

Bin 2370: 101 of cap free
Amount of items: 2
Items: 
Size: 559976 Color: 1
Size: 439924 Color: 0

Bin 2371: 101 of cap free
Amount of items: 2
Items: 
Size: 592636 Color: 1
Size: 407264 Color: 0

Bin 2372: 101 of cap free
Amount of items: 2
Items: 
Size: 670258 Color: 0
Size: 329642 Color: 1

Bin 2373: 101 of cap free
Amount of items: 2
Items: 
Size: 675615 Color: 0
Size: 324285 Color: 1

Bin 2374: 101 of cap free
Amount of items: 2
Items: 
Size: 703187 Color: 0
Size: 296713 Color: 1

Bin 2375: 101 of cap free
Amount of items: 2
Items: 
Size: 711740 Color: 1
Size: 288160 Color: 0

Bin 2376: 101 of cap free
Amount of items: 2
Items: 
Size: 742767 Color: 0
Size: 257133 Color: 1

Bin 2377: 101 of cap free
Amount of items: 2
Items: 
Size: 788775 Color: 1
Size: 211125 Color: 0

Bin 2378: 101 of cap free
Amount of items: 2
Items: 
Size: 789989 Color: 0
Size: 209911 Color: 1

Bin 2379: 102 of cap free
Amount of items: 2
Items: 
Size: 772535 Color: 1
Size: 227364 Color: 0

Bin 2380: 102 of cap free
Amount of items: 2
Items: 
Size: 581791 Color: 0
Size: 418108 Color: 1

Bin 2381: 102 of cap free
Amount of items: 2
Items: 
Size: 625358 Color: 1
Size: 374541 Color: 0

Bin 2382: 102 of cap free
Amount of items: 2
Items: 
Size: 638658 Color: 0
Size: 361241 Color: 1

Bin 2383: 102 of cap free
Amount of items: 2
Items: 
Size: 679333 Color: 1
Size: 320566 Color: 0

Bin 2384: 102 of cap free
Amount of items: 2
Items: 
Size: 690115 Color: 0
Size: 309784 Color: 1

Bin 2385: 102 of cap free
Amount of items: 2
Items: 
Size: 716557 Color: 0
Size: 283342 Color: 1

Bin 2386: 102 of cap free
Amount of items: 2
Items: 
Size: 751281 Color: 0
Size: 248618 Color: 1

Bin 2387: 102 of cap free
Amount of items: 2
Items: 
Size: 779309 Color: 1
Size: 220590 Color: 0

Bin 2388: 102 of cap free
Amount of items: 2
Items: 
Size: 783859 Color: 0
Size: 216040 Color: 1

Bin 2389: 103 of cap free
Amount of items: 2
Items: 
Size: 500189 Color: 1
Size: 499709 Color: 0

Bin 2390: 103 of cap free
Amount of items: 2
Items: 
Size: 521447 Color: 0
Size: 478451 Color: 1

Bin 2391: 103 of cap free
Amount of items: 2
Items: 
Size: 587113 Color: 1
Size: 412785 Color: 0

Bin 2392: 103 of cap free
Amount of items: 2
Items: 
Size: 593873 Color: 0
Size: 406025 Color: 1

Bin 2393: 103 of cap free
Amount of items: 2
Items: 
Size: 607598 Color: 0
Size: 392300 Color: 1

Bin 2394: 103 of cap free
Amount of items: 2
Items: 
Size: 628476 Color: 0
Size: 371422 Color: 1

Bin 2395: 103 of cap free
Amount of items: 2
Items: 
Size: 663088 Color: 0
Size: 336810 Color: 1

Bin 2396: 103 of cap free
Amount of items: 2
Items: 
Size: 677464 Color: 0
Size: 322434 Color: 1

Bin 2397: 103 of cap free
Amount of items: 2
Items: 
Size: 726320 Color: 0
Size: 273578 Color: 1

Bin 2398: 103 of cap free
Amount of items: 2
Items: 
Size: 762408 Color: 1
Size: 237490 Color: 0

Bin 2399: 103 of cap free
Amount of items: 2
Items: 
Size: 762789 Color: 1
Size: 237109 Color: 0

Bin 2400: 103 of cap free
Amount of items: 2
Items: 
Size: 780125 Color: 0
Size: 219773 Color: 1

Bin 2401: 103 of cap free
Amount of items: 2
Items: 
Size: 796184 Color: 1
Size: 203714 Color: 0

Bin 2402: 104 of cap free
Amount of items: 2
Items: 
Size: 752964 Color: 1
Size: 246933 Color: 0

Bin 2403: 104 of cap free
Amount of items: 2
Items: 
Size: 586498 Color: 0
Size: 413399 Color: 1

Bin 2404: 104 of cap free
Amount of items: 2
Items: 
Size: 545807 Color: 0
Size: 454090 Color: 1

Bin 2405: 104 of cap free
Amount of items: 2
Items: 
Size: 711621 Color: 1
Size: 288276 Color: 0

Bin 2406: 104 of cap free
Amount of items: 2
Items: 
Size: 676639 Color: 1
Size: 323258 Color: 0

Bin 2407: 104 of cap free
Amount of items: 2
Items: 
Size: 508460 Color: 0
Size: 491437 Color: 1

Bin 2408: 104 of cap free
Amount of items: 2
Items: 
Size: 628187 Color: 1
Size: 371710 Color: 0

Bin 2409: 104 of cap free
Amount of items: 2
Items: 
Size: 646755 Color: 1
Size: 353142 Color: 0

Bin 2410: 104 of cap free
Amount of items: 2
Items: 
Size: 647961 Color: 0
Size: 351936 Color: 1

Bin 2411: 104 of cap free
Amount of items: 2
Items: 
Size: 689173 Color: 1
Size: 310724 Color: 0

Bin 2412: 104 of cap free
Amount of items: 2
Items: 
Size: 700478 Color: 1
Size: 299419 Color: 0

Bin 2413: 104 of cap free
Amount of items: 2
Items: 
Size: 796694 Color: 0
Size: 203203 Color: 1

Bin 2414: 105 of cap free
Amount of items: 2
Items: 
Size: 523464 Color: 0
Size: 476432 Color: 1

Bin 2415: 105 of cap free
Amount of items: 2
Items: 
Size: 558127 Color: 0
Size: 441769 Color: 1

Bin 2416: 105 of cap free
Amount of items: 2
Items: 
Size: 630124 Color: 0
Size: 369772 Color: 1

Bin 2417: 105 of cap free
Amount of items: 2
Items: 
Size: 785928 Color: 1
Size: 213968 Color: 0

Bin 2418: 106 of cap free
Amount of items: 3
Items: 
Size: 489990 Color: 0
Size: 375510 Color: 1
Size: 134395 Color: 1

Bin 2419: 106 of cap free
Amount of items: 2
Items: 
Size: 620062 Color: 1
Size: 379833 Color: 0

Bin 2420: 106 of cap free
Amount of items: 2
Items: 
Size: 547427 Color: 1
Size: 452468 Color: 0

Bin 2421: 106 of cap free
Amount of items: 2
Items: 
Size: 635837 Color: 1
Size: 364058 Color: 0

Bin 2422: 106 of cap free
Amount of items: 2
Items: 
Size: 724096 Color: 0
Size: 275799 Color: 1

Bin 2423: 106 of cap free
Amount of items: 2
Items: 
Size: 736973 Color: 1
Size: 262922 Color: 0

Bin 2424: 106 of cap free
Amount of items: 2
Items: 
Size: 741461 Color: 0
Size: 258434 Color: 1

Bin 2425: 107 of cap free
Amount of items: 2
Items: 
Size: 770098 Color: 0
Size: 229796 Color: 1

Bin 2426: 107 of cap free
Amount of items: 2
Items: 
Size: 535524 Color: 0
Size: 464370 Color: 1

Bin 2427: 107 of cap free
Amount of items: 2
Items: 
Size: 543188 Color: 1
Size: 456706 Color: 0

Bin 2428: 107 of cap free
Amount of items: 2
Items: 
Size: 546802 Color: 1
Size: 453092 Color: 0

Bin 2429: 107 of cap free
Amount of items: 2
Items: 
Size: 600428 Color: 1
Size: 399466 Color: 0

Bin 2430: 107 of cap free
Amount of items: 2
Items: 
Size: 642583 Color: 0
Size: 357311 Color: 1

Bin 2431: 107 of cap free
Amount of items: 2
Items: 
Size: 667432 Color: 0
Size: 332462 Color: 1

Bin 2432: 107 of cap free
Amount of items: 2
Items: 
Size: 668278 Color: 0
Size: 331616 Color: 1

Bin 2433: 107 of cap free
Amount of items: 2
Items: 
Size: 683584 Color: 0
Size: 316310 Color: 1

Bin 2434: 107 of cap free
Amount of items: 2
Items: 
Size: 686073 Color: 1
Size: 313821 Color: 0

Bin 2435: 107 of cap free
Amount of items: 2
Items: 
Size: 700704 Color: 0
Size: 299190 Color: 1

Bin 2436: 108 of cap free
Amount of items: 2
Items: 
Size: 653760 Color: 1
Size: 346133 Color: 0

Bin 2437: 108 of cap free
Amount of items: 2
Items: 
Size: 548622 Color: 0
Size: 451271 Color: 1

Bin 2438: 108 of cap free
Amount of items: 2
Items: 
Size: 564898 Color: 1
Size: 434995 Color: 0

Bin 2439: 108 of cap free
Amount of items: 2
Items: 
Size: 585384 Color: 0
Size: 414509 Color: 1

Bin 2440: 108 of cap free
Amount of items: 2
Items: 
Size: 590943 Color: 1
Size: 408950 Color: 0

Bin 2441: 108 of cap free
Amount of items: 2
Items: 
Size: 606525 Color: 1
Size: 393368 Color: 0

Bin 2442: 108 of cap free
Amount of items: 2
Items: 
Size: 609351 Color: 0
Size: 390542 Color: 1

Bin 2443: 108 of cap free
Amount of items: 2
Items: 
Size: 681725 Color: 1
Size: 318168 Color: 0

Bin 2444: 108 of cap free
Amount of items: 2
Items: 
Size: 684123 Color: 0
Size: 315770 Color: 1

Bin 2445: 108 of cap free
Amount of items: 2
Items: 
Size: 732760 Color: 1
Size: 267133 Color: 0

Bin 2446: 109 of cap free
Amount of items: 2
Items: 
Size: 792000 Color: 0
Size: 207892 Color: 1

Bin 2447: 109 of cap free
Amount of items: 2
Items: 
Size: 517452 Color: 1
Size: 482440 Color: 0

Bin 2448: 109 of cap free
Amount of items: 2
Items: 
Size: 570936 Color: 1
Size: 428956 Color: 0

Bin 2449: 109 of cap free
Amount of items: 2
Items: 
Size: 588081 Color: 0
Size: 411811 Color: 1

Bin 2450: 109 of cap free
Amount of items: 2
Items: 
Size: 637374 Color: 1
Size: 362518 Color: 0

Bin 2451: 109 of cap free
Amount of items: 2
Items: 
Size: 672467 Color: 0
Size: 327425 Color: 1

Bin 2452: 110 of cap free
Amount of items: 2
Items: 
Size: 556833 Color: 1
Size: 443058 Color: 0

Bin 2453: 110 of cap free
Amount of items: 2
Items: 
Size: 620970 Color: 1
Size: 378921 Color: 0

Bin 2454: 110 of cap free
Amount of items: 2
Items: 
Size: 685700 Color: 0
Size: 314191 Color: 1

Bin 2455: 110 of cap free
Amount of items: 2
Items: 
Size: 764549 Color: 1
Size: 235342 Color: 0

Bin 2456: 111 of cap free
Amount of items: 2
Items: 
Size: 760484 Color: 0
Size: 239406 Color: 1

Bin 2457: 111 of cap free
Amount of items: 2
Items: 
Size: 716414 Color: 1
Size: 283476 Color: 0

Bin 2458: 111 of cap free
Amount of items: 2
Items: 
Size: 519625 Color: 0
Size: 480265 Color: 1

Bin 2459: 111 of cap free
Amount of items: 2
Items: 
Size: 531420 Color: 0
Size: 468470 Color: 1

Bin 2460: 111 of cap free
Amount of items: 2
Items: 
Size: 630960 Color: 0
Size: 368930 Color: 1

Bin 2461: 111 of cap free
Amount of items: 2
Items: 
Size: 656166 Color: 0
Size: 343724 Color: 1

Bin 2462: 111 of cap free
Amount of items: 2
Items: 
Size: 698140 Color: 0
Size: 301750 Color: 1

Bin 2463: 111 of cap free
Amount of items: 2
Items: 
Size: 717419 Color: 0
Size: 282471 Color: 1

Bin 2464: 111 of cap free
Amount of items: 2
Items: 
Size: 739410 Color: 1
Size: 260480 Color: 0

Bin 2465: 111 of cap free
Amount of items: 2
Items: 
Size: 765191 Color: 0
Size: 234699 Color: 1

Bin 2466: 111 of cap free
Amount of items: 2
Items: 
Size: 781604 Color: 1
Size: 218286 Color: 0

Bin 2467: 111 of cap free
Amount of items: 2
Items: 
Size: 788940 Color: 1
Size: 210950 Color: 0

Bin 2468: 112 of cap free
Amount of items: 2
Items: 
Size: 765575 Color: 0
Size: 234314 Color: 1

Bin 2469: 112 of cap free
Amount of items: 2
Items: 
Size: 538929 Color: 0
Size: 460960 Color: 1

Bin 2470: 112 of cap free
Amount of items: 2
Items: 
Size: 540140 Color: 0
Size: 459749 Color: 1

Bin 2471: 112 of cap free
Amount of items: 2
Items: 
Size: 562726 Color: 1
Size: 437163 Color: 0

Bin 2472: 112 of cap free
Amount of items: 2
Items: 
Size: 568200 Color: 0
Size: 431689 Color: 1

Bin 2473: 112 of cap free
Amount of items: 2
Items: 
Size: 572496 Color: 1
Size: 427393 Color: 0

Bin 2474: 112 of cap free
Amount of items: 2
Items: 
Size: 585809 Color: 0
Size: 414080 Color: 1

Bin 2475: 112 of cap free
Amount of items: 2
Items: 
Size: 588387 Color: 0
Size: 411502 Color: 1

Bin 2476: 112 of cap free
Amount of items: 2
Items: 
Size: 646093 Color: 0
Size: 353796 Color: 1

Bin 2477: 112 of cap free
Amount of items: 2
Items: 
Size: 672544 Color: 1
Size: 327345 Color: 0

Bin 2478: 112 of cap free
Amount of items: 2
Items: 
Size: 682033 Color: 0
Size: 317856 Color: 1

Bin 2479: 112 of cap free
Amount of items: 2
Items: 
Size: 692586 Color: 1
Size: 307303 Color: 0

Bin 2480: 112 of cap free
Amount of items: 2
Items: 
Size: 703381 Color: 0
Size: 296508 Color: 1

Bin 2481: 112 of cap free
Amount of items: 2
Items: 
Size: 706513 Color: 1
Size: 293376 Color: 0

Bin 2482: 113 of cap free
Amount of items: 2
Items: 
Size: 515893 Color: 1
Size: 483995 Color: 0

Bin 2483: 113 of cap free
Amount of items: 2
Items: 
Size: 551288 Color: 1
Size: 448600 Color: 0

Bin 2484: 113 of cap free
Amount of items: 2
Items: 
Size: 604491 Color: 0
Size: 395397 Color: 1

Bin 2485: 113 of cap free
Amount of items: 2
Items: 
Size: 733292 Color: 0
Size: 266596 Color: 1

Bin 2486: 114 of cap free
Amount of items: 2
Items: 
Size: 516503 Color: 1
Size: 483384 Color: 0

Bin 2487: 114 of cap free
Amount of items: 2
Items: 
Size: 553892 Color: 0
Size: 445995 Color: 1

Bin 2488: 114 of cap free
Amount of items: 2
Items: 
Size: 568726 Color: 0
Size: 431161 Color: 1

Bin 2489: 114 of cap free
Amount of items: 2
Items: 
Size: 633366 Color: 0
Size: 366521 Color: 1

Bin 2490: 114 of cap free
Amount of items: 2
Items: 
Size: 645764 Color: 1
Size: 354123 Color: 0

Bin 2491: 114 of cap free
Amount of items: 2
Items: 
Size: 698287 Color: 0
Size: 301600 Color: 1

Bin 2492: 114 of cap free
Amount of items: 2
Items: 
Size: 731023 Color: 0
Size: 268864 Color: 1

Bin 2493: 114 of cap free
Amount of items: 2
Items: 
Size: 763409 Color: 1
Size: 236478 Color: 0

Bin 2494: 115 of cap free
Amount of items: 3
Items: 
Size: 712884 Color: 1
Size: 185345 Color: 1
Size: 101657 Color: 0

Bin 2495: 115 of cap free
Amount of items: 2
Items: 
Size: 797718 Color: 0
Size: 202168 Color: 1

Bin 2496: 115 of cap free
Amount of items: 2
Items: 
Size: 521568 Color: 1
Size: 478318 Color: 0

Bin 2497: 115 of cap free
Amount of items: 2
Items: 
Size: 527767 Color: 0
Size: 472119 Color: 1

Bin 2498: 115 of cap free
Amount of items: 2
Items: 
Size: 627091 Color: 0
Size: 372795 Color: 1

Bin 2499: 115 of cap free
Amount of items: 2
Items: 
Size: 664837 Color: 0
Size: 335049 Color: 1

Bin 2500: 115 of cap free
Amount of items: 2
Items: 
Size: 665603 Color: 1
Size: 334283 Color: 0

Bin 2501: 115 of cap free
Amount of items: 2
Items: 
Size: 713538 Color: 1
Size: 286348 Color: 0

Bin 2502: 115 of cap free
Amount of items: 2
Items: 
Size: 753142 Color: 0
Size: 246744 Color: 1

Bin 2503: 116 of cap free
Amount of items: 2
Items: 
Size: 748176 Color: 0
Size: 251709 Color: 1

Bin 2504: 116 of cap free
Amount of items: 2
Items: 
Size: 619887 Color: 1
Size: 379998 Color: 0

Bin 2505: 116 of cap free
Amount of items: 2
Items: 
Size: 624547 Color: 0
Size: 375338 Color: 1

Bin 2506: 116 of cap free
Amount of items: 2
Items: 
Size: 710167 Color: 1
Size: 289718 Color: 0

Bin 2507: 117 of cap free
Amount of items: 2
Items: 
Size: 766351 Color: 1
Size: 233533 Color: 0

Bin 2508: 117 of cap free
Amount of items: 2
Items: 
Size: 678645 Color: 1
Size: 321239 Color: 0

Bin 2509: 117 of cap free
Amount of items: 2
Items: 
Size: 531503 Color: 1
Size: 468381 Color: 0

Bin 2510: 117 of cap free
Amount of items: 2
Items: 
Size: 555149 Color: 0
Size: 444735 Color: 1

Bin 2511: 117 of cap free
Amount of items: 2
Items: 
Size: 581433 Color: 0
Size: 418451 Color: 1

Bin 2512: 117 of cap free
Amount of items: 2
Items: 
Size: 583514 Color: 0
Size: 416370 Color: 1

Bin 2513: 117 of cap free
Amount of items: 2
Items: 
Size: 589843 Color: 0
Size: 410041 Color: 1

Bin 2514: 117 of cap free
Amount of items: 2
Items: 
Size: 601248 Color: 0
Size: 398636 Color: 1

Bin 2515: 117 of cap free
Amount of items: 2
Items: 
Size: 652170 Color: 1
Size: 347714 Color: 0

Bin 2516: 117 of cap free
Amount of items: 2
Items: 
Size: 656486 Color: 1
Size: 343398 Color: 0

Bin 2517: 117 of cap free
Amount of items: 2
Items: 
Size: 718521 Color: 0
Size: 281363 Color: 1

Bin 2518: 117 of cap free
Amount of items: 2
Items: 
Size: 798138 Color: 1
Size: 201746 Color: 0

Bin 2519: 118 of cap free
Amount of items: 2
Items: 
Size: 526415 Color: 0
Size: 473468 Color: 1

Bin 2520: 118 of cap free
Amount of items: 2
Items: 
Size: 505757 Color: 1
Size: 494126 Color: 0

Bin 2521: 118 of cap free
Amount of items: 2
Items: 
Size: 536138 Color: 0
Size: 463745 Color: 1

Bin 2522: 118 of cap free
Amount of items: 2
Items: 
Size: 551210 Color: 0
Size: 448673 Color: 1

Bin 2523: 118 of cap free
Amount of items: 2
Items: 
Size: 572574 Color: 0
Size: 427309 Color: 1

Bin 2524: 118 of cap free
Amount of items: 2
Items: 
Size: 641971 Color: 0
Size: 357912 Color: 1

Bin 2525: 118 of cap free
Amount of items: 2
Items: 
Size: 660381 Color: 0
Size: 339502 Color: 1

Bin 2526: 118 of cap free
Amount of items: 2
Items: 
Size: 664305 Color: 1
Size: 335578 Color: 0

Bin 2527: 118 of cap free
Amount of items: 2
Items: 
Size: 680246 Color: 0
Size: 319637 Color: 1

Bin 2528: 118 of cap free
Amount of items: 2
Items: 
Size: 727567 Color: 1
Size: 272316 Color: 0

Bin 2529: 118 of cap free
Amount of items: 2
Items: 
Size: 777317 Color: 1
Size: 222566 Color: 0

Bin 2530: 119 of cap free
Amount of items: 2
Items: 
Size: 596925 Color: 1
Size: 402957 Color: 0

Bin 2531: 119 of cap free
Amount of items: 2
Items: 
Size: 611847 Color: 1
Size: 388035 Color: 0

Bin 2532: 119 of cap free
Amount of items: 2
Items: 
Size: 628878 Color: 1
Size: 371004 Color: 0

Bin 2533: 119 of cap free
Amount of items: 2
Items: 
Size: 681510 Color: 1
Size: 318372 Color: 0

Bin 2534: 119 of cap free
Amount of items: 2
Items: 
Size: 684605 Color: 1
Size: 315277 Color: 0

Bin 2535: 119 of cap free
Amount of items: 2
Items: 
Size: 702302 Color: 0
Size: 297580 Color: 1

Bin 2536: 119 of cap free
Amount of items: 2
Items: 
Size: 712096 Color: 1
Size: 287786 Color: 0

Bin 2537: 119 of cap free
Amount of items: 2
Items: 
Size: 769087 Color: 1
Size: 230795 Color: 0

Bin 2538: 120 of cap free
Amount of items: 2
Items: 
Size: 561541 Color: 1
Size: 438340 Color: 0

Bin 2539: 120 of cap free
Amount of items: 2
Items: 
Size: 620047 Color: 0
Size: 379834 Color: 1

Bin 2540: 120 of cap free
Amount of items: 2
Items: 
Size: 668500 Color: 1
Size: 331381 Color: 0

Bin 2541: 120 of cap free
Amount of items: 2
Items: 
Size: 731627 Color: 0
Size: 268254 Color: 1

Bin 2542: 120 of cap free
Amount of items: 2
Items: 
Size: 769864 Color: 1
Size: 230017 Color: 0

Bin 2543: 120 of cap free
Amount of items: 2
Items: 
Size: 788970 Color: 0
Size: 210911 Color: 1

Bin 2544: 120 of cap free
Amount of items: 2
Items: 
Size: 789293 Color: 0
Size: 210588 Color: 1

Bin 2545: 121 of cap free
Amount of items: 2
Items: 
Size: 524465 Color: 0
Size: 475415 Color: 1

Bin 2546: 121 of cap free
Amount of items: 2
Items: 
Size: 695362 Color: 1
Size: 304518 Color: 0

Bin 2547: 121 of cap free
Amount of items: 2
Items: 
Size: 542002 Color: 0
Size: 457878 Color: 1

Bin 2548: 121 of cap free
Amount of items: 2
Items: 
Size: 546349 Color: 0
Size: 453531 Color: 1

Bin 2549: 121 of cap free
Amount of items: 2
Items: 
Size: 567476 Color: 0
Size: 432404 Color: 1

Bin 2550: 121 of cap free
Amount of items: 2
Items: 
Size: 584586 Color: 1
Size: 415294 Color: 0

Bin 2551: 121 of cap free
Amount of items: 2
Items: 
Size: 590694 Color: 0
Size: 409186 Color: 1

Bin 2552: 121 of cap free
Amount of items: 2
Items: 
Size: 627212 Color: 1
Size: 372668 Color: 0

Bin 2553: 121 of cap free
Amount of items: 2
Items: 
Size: 659029 Color: 1
Size: 340851 Color: 0

Bin 2554: 121 of cap free
Amount of items: 2
Items: 
Size: 679569 Color: 1
Size: 320311 Color: 0

Bin 2555: 121 of cap free
Amount of items: 2
Items: 
Size: 723453 Color: 0
Size: 276427 Color: 1

Bin 2556: 121 of cap free
Amount of items: 2
Items: 
Size: 744545 Color: 0
Size: 255335 Color: 1

Bin 2557: 122 of cap free
Amount of items: 2
Items: 
Size: 511450 Color: 0
Size: 488429 Color: 1

Bin 2558: 122 of cap free
Amount of items: 2
Items: 
Size: 572250 Color: 1
Size: 427629 Color: 0

Bin 2559: 122 of cap free
Amount of items: 2
Items: 
Size: 638161 Color: 0
Size: 361718 Color: 1

Bin 2560: 122 of cap free
Amount of items: 2
Items: 
Size: 658583 Color: 1
Size: 341296 Color: 0

Bin 2561: 122 of cap free
Amount of items: 2
Items: 
Size: 686557 Color: 1
Size: 313322 Color: 0

Bin 2562: 122 of cap free
Amount of items: 2
Items: 
Size: 780849 Color: 1
Size: 219030 Color: 0

Bin 2563: 123 of cap free
Amount of items: 2
Items: 
Size: 505855 Color: 0
Size: 494023 Color: 1

Bin 2564: 123 of cap free
Amount of items: 2
Items: 
Size: 514527 Color: 0
Size: 485351 Color: 1

Bin 2565: 123 of cap free
Amount of items: 2
Items: 
Size: 543212 Color: 0
Size: 456666 Color: 1

Bin 2566: 123 of cap free
Amount of items: 2
Items: 
Size: 558222 Color: 1
Size: 441656 Color: 0

Bin 2567: 123 of cap free
Amount of items: 2
Items: 
Size: 668090 Color: 0
Size: 331788 Color: 1

Bin 2568: 123 of cap free
Amount of items: 2
Items: 
Size: 714578 Color: 1
Size: 285300 Color: 0

Bin 2569: 123 of cap free
Amount of items: 2
Items: 
Size: 757588 Color: 0
Size: 242290 Color: 1

Bin 2570: 123 of cap free
Amount of items: 2
Items: 
Size: 761477 Color: 0
Size: 238401 Color: 1

Bin 2571: 124 of cap free
Amount of items: 2
Items: 
Size: 509736 Color: 1
Size: 490141 Color: 0

Bin 2572: 124 of cap free
Amount of items: 2
Items: 
Size: 544141 Color: 1
Size: 455736 Color: 0

Bin 2573: 124 of cap free
Amount of items: 2
Items: 
Size: 558710 Color: 0
Size: 441167 Color: 1

Bin 2574: 124 of cap free
Amount of items: 2
Items: 
Size: 568193 Color: 0
Size: 431684 Color: 1

Bin 2575: 124 of cap free
Amount of items: 2
Items: 
Size: 596274 Color: 0
Size: 403603 Color: 1

Bin 2576: 124 of cap free
Amount of items: 2
Items: 
Size: 615581 Color: 0
Size: 384296 Color: 1

Bin 2577: 124 of cap free
Amount of items: 2
Items: 
Size: 635474 Color: 0
Size: 364403 Color: 1

Bin 2578: 124 of cap free
Amount of items: 2
Items: 
Size: 701446 Color: 1
Size: 298431 Color: 0

Bin 2579: 124 of cap free
Amount of items: 2
Items: 
Size: 715673 Color: 1
Size: 284204 Color: 0

Bin 2580: 124 of cap free
Amount of items: 2
Items: 
Size: 717927 Color: 0
Size: 281950 Color: 1

Bin 2581: 124 of cap free
Amount of items: 2
Items: 
Size: 764178 Color: 0
Size: 235699 Color: 1

Bin 2582: 125 of cap free
Amount of items: 2
Items: 
Size: 528815 Color: 0
Size: 471061 Color: 1

Bin 2583: 125 of cap free
Amount of items: 2
Items: 
Size: 589237 Color: 1
Size: 410639 Color: 0

Bin 2584: 125 of cap free
Amount of items: 2
Items: 
Size: 617562 Color: 1
Size: 382314 Color: 0

Bin 2585: 125 of cap free
Amount of items: 2
Items: 
Size: 640253 Color: 1
Size: 359623 Color: 0

Bin 2586: 125 of cap free
Amount of items: 2
Items: 
Size: 724995 Color: 1
Size: 274881 Color: 0

Bin 2587: 125 of cap free
Amount of items: 2
Items: 
Size: 751767 Color: 1
Size: 248109 Color: 0

Bin 2588: 125 of cap free
Amount of items: 2
Items: 
Size: 777557 Color: 1
Size: 222319 Color: 0

Bin 2589: 126 of cap free
Amount of items: 2
Items: 
Size: 767776 Color: 0
Size: 232099 Color: 1

Bin 2590: 126 of cap free
Amount of items: 2
Items: 
Size: 502205 Color: 0
Size: 497670 Color: 1

Bin 2591: 126 of cap free
Amount of items: 2
Items: 
Size: 535996 Color: 1
Size: 463879 Color: 0

Bin 2592: 126 of cap free
Amount of items: 2
Items: 
Size: 601448 Color: 0
Size: 398427 Color: 1

Bin 2593: 126 of cap free
Amount of items: 2
Items: 
Size: 606144 Color: 0
Size: 393731 Color: 1

Bin 2594: 126 of cap free
Amount of items: 2
Items: 
Size: 609065 Color: 0
Size: 390810 Color: 1

Bin 2595: 126 of cap free
Amount of items: 2
Items: 
Size: 668993 Color: 1
Size: 330882 Color: 0

Bin 2596: 126 of cap free
Amount of items: 2
Items: 
Size: 696262 Color: 0
Size: 303613 Color: 1

Bin 2597: 126 of cap free
Amount of items: 2
Items: 
Size: 730191 Color: 1
Size: 269684 Color: 0

Bin 2598: 126 of cap free
Amount of items: 2
Items: 
Size: 754886 Color: 1
Size: 244989 Color: 0

Bin 2599: 126 of cap free
Amount of items: 2
Items: 
Size: 787370 Color: 1
Size: 212505 Color: 0

Bin 2600: 126 of cap free
Amount of items: 2
Items: 
Size: 789657 Color: 1
Size: 210218 Color: 0

Bin 2601: 127 of cap free
Amount of items: 2
Items: 
Size: 592354 Color: 1
Size: 407520 Color: 0

Bin 2602: 127 of cap free
Amount of items: 2
Items: 
Size: 625491 Color: 0
Size: 374383 Color: 1

Bin 2603: 127 of cap free
Amount of items: 2
Items: 
Size: 713856 Color: 1
Size: 286018 Color: 0

Bin 2604: 128 of cap free
Amount of items: 2
Items: 
Size: 792767 Color: 0
Size: 207106 Color: 1

Bin 2605: 128 of cap free
Amount of items: 2
Items: 
Size: 635470 Color: 1
Size: 364403 Color: 0

Bin 2606: 128 of cap free
Amount of items: 2
Items: 
Size: 778521 Color: 0
Size: 221352 Color: 1

Bin 2607: 129 of cap free
Amount of items: 2
Items: 
Size: 703550 Color: 1
Size: 296322 Color: 0

Bin 2608: 129 of cap free
Amount of items: 2
Items: 
Size: 704080 Color: 0
Size: 295792 Color: 1

Bin 2609: 130 of cap free
Amount of items: 2
Items: 
Size: 569095 Color: 1
Size: 430776 Color: 0

Bin 2610: 130 of cap free
Amount of items: 2
Items: 
Size: 612820 Color: 0
Size: 387051 Color: 1

Bin 2611: 130 of cap free
Amount of items: 2
Items: 
Size: 639280 Color: 1
Size: 360591 Color: 0

Bin 2612: 130 of cap free
Amount of items: 2
Items: 
Size: 643545 Color: 1
Size: 356326 Color: 0

Bin 2613: 130 of cap free
Amount of items: 2
Items: 
Size: 667581 Color: 1
Size: 332290 Color: 0

Bin 2614: 130 of cap free
Amount of items: 2
Items: 
Size: 679440 Color: 0
Size: 320431 Color: 1

Bin 2615: 130 of cap free
Amount of items: 2
Items: 
Size: 703010 Color: 0
Size: 296861 Color: 1

Bin 2616: 130 of cap free
Amount of items: 2
Items: 
Size: 710276 Color: 0
Size: 289595 Color: 1

Bin 2617: 130 of cap free
Amount of items: 2
Items: 
Size: 743310 Color: 1
Size: 256561 Color: 0

Bin 2618: 130 of cap free
Amount of items: 2
Items: 
Size: 759289 Color: 1
Size: 240582 Color: 0

Bin 2619: 130 of cap free
Amount of items: 2
Items: 
Size: 760003 Color: 0
Size: 239868 Color: 1

Bin 2620: 131 of cap free
Amount of items: 3
Items: 
Size: 469318 Color: 0
Size: 397046 Color: 1
Size: 133506 Color: 1

Bin 2621: 131 of cap free
Amount of items: 2
Items: 
Size: 533955 Color: 0
Size: 465915 Color: 1

Bin 2622: 131 of cap free
Amount of items: 2
Items: 
Size: 565892 Color: 0
Size: 433978 Color: 1

Bin 2623: 131 of cap free
Amount of items: 2
Items: 
Size: 682619 Color: 1
Size: 317251 Color: 0

Bin 2624: 131 of cap free
Amount of items: 2
Items: 
Size: 730614 Color: 1
Size: 269256 Color: 0

Bin 2625: 131 of cap free
Amount of items: 2
Items: 
Size: 750425 Color: 0
Size: 249445 Color: 1

Bin 2626: 131 of cap free
Amount of items: 2
Items: 
Size: 771400 Color: 1
Size: 228470 Color: 0

Bin 2627: 132 of cap free
Amount of items: 2
Items: 
Size: 714698 Color: 0
Size: 285171 Color: 1

Bin 2628: 132 of cap free
Amount of items: 2
Items: 
Size: 536416 Color: 1
Size: 463453 Color: 0

Bin 2629: 132 of cap free
Amount of items: 2
Items: 
Size: 542484 Color: 1
Size: 457385 Color: 0

Bin 2630: 132 of cap free
Amount of items: 2
Items: 
Size: 554915 Color: 0
Size: 444954 Color: 1

Bin 2631: 132 of cap free
Amount of items: 2
Items: 
Size: 592750 Color: 1
Size: 407119 Color: 0

Bin 2632: 132 of cap free
Amount of items: 2
Items: 
Size: 595613 Color: 1
Size: 404256 Color: 0

Bin 2633: 132 of cap free
Amount of items: 2
Items: 
Size: 614266 Color: 1
Size: 385603 Color: 0

Bin 2634: 132 of cap free
Amount of items: 2
Items: 
Size: 650407 Color: 1
Size: 349462 Color: 0

Bin 2635: 132 of cap free
Amount of items: 2
Items: 
Size: 693814 Color: 1
Size: 306055 Color: 0

Bin 2636: 132 of cap free
Amount of items: 2
Items: 
Size: 700992 Color: 0
Size: 298877 Color: 1

Bin 2637: 132 of cap free
Amount of items: 2
Items: 
Size: 707204 Color: 1
Size: 292665 Color: 0

Bin 2638: 132 of cap free
Amount of items: 2
Items: 
Size: 708924 Color: 1
Size: 290945 Color: 0

Bin 2639: 132 of cap free
Amount of items: 2
Items: 
Size: 711672 Color: 0
Size: 288197 Color: 1

Bin 2640: 133 of cap free
Amount of items: 2
Items: 
Size: 678636 Color: 1
Size: 321232 Color: 0

Bin 2641: 133 of cap free
Amount of items: 2
Items: 
Size: 795825 Color: 1
Size: 204043 Color: 0

Bin 2642: 133 of cap free
Amount of items: 2
Items: 
Size: 539730 Color: 0
Size: 460138 Color: 1

Bin 2643: 133 of cap free
Amount of items: 2
Items: 
Size: 636416 Color: 0
Size: 363452 Color: 1

Bin 2644: 133 of cap free
Amount of items: 2
Items: 
Size: 641251 Color: 0
Size: 358617 Color: 1

Bin 2645: 133 of cap free
Amount of items: 2
Items: 
Size: 702850 Color: 1
Size: 297018 Color: 0

Bin 2646: 133 of cap free
Amount of items: 2
Items: 
Size: 743813 Color: 1
Size: 256055 Color: 0

Bin 2647: 133 of cap free
Amount of items: 2
Items: 
Size: 789124 Color: 1
Size: 210744 Color: 0

Bin 2648: 134 of cap free
Amount of items: 3
Items: 
Size: 499179 Color: 0
Size: 377909 Color: 1
Size: 122779 Color: 1

Bin 2649: 134 of cap free
Amount of items: 2
Items: 
Size: 512888 Color: 0
Size: 486979 Color: 1

Bin 2650: 134 of cap free
Amount of items: 2
Items: 
Size: 640732 Color: 1
Size: 359135 Color: 0

Bin 2651: 134 of cap free
Amount of items: 2
Items: 
Size: 645286 Color: 0
Size: 354581 Color: 1

Bin 2652: 134 of cap free
Amount of items: 2
Items: 
Size: 652964 Color: 0
Size: 346903 Color: 1

Bin 2653: 134 of cap free
Amount of items: 2
Items: 
Size: 700162 Color: 1
Size: 299705 Color: 0

Bin 2654: 134 of cap free
Amount of items: 2
Items: 
Size: 726310 Color: 0
Size: 273557 Color: 1

Bin 2655: 134 of cap free
Amount of items: 2
Items: 
Size: 764314 Color: 1
Size: 235553 Color: 0

Bin 2656: 135 of cap free
Amount of items: 2
Items: 
Size: 511043 Color: 0
Size: 488823 Color: 1

Bin 2657: 135 of cap free
Amount of items: 2
Items: 
Size: 627661 Color: 0
Size: 372205 Color: 1

Bin 2658: 135 of cap free
Amount of items: 2
Items: 
Size: 666014 Color: 1
Size: 333852 Color: 0

Bin 2659: 135 of cap free
Amount of items: 2
Items: 
Size: 678310 Color: 1
Size: 321556 Color: 0

Bin 2660: 135 of cap free
Amount of items: 2
Items: 
Size: 720535 Color: 0
Size: 279331 Color: 1

Bin 2661: 135 of cap free
Amount of items: 2
Items: 
Size: 766356 Color: 0
Size: 233510 Color: 1

Bin 2662: 136 of cap free
Amount of items: 2
Items: 
Size: 508246 Color: 1
Size: 491619 Color: 0

Bin 2663: 136 of cap free
Amount of items: 2
Items: 
Size: 614511 Color: 1
Size: 385354 Color: 0

Bin 2664: 136 of cap free
Amount of items: 2
Items: 
Size: 661669 Color: 0
Size: 338196 Color: 1

Bin 2665: 136 of cap free
Amount of items: 2
Items: 
Size: 676842 Color: 1
Size: 323023 Color: 0

Bin 2666: 136 of cap free
Amount of items: 2
Items: 
Size: 717495 Color: 1
Size: 282370 Color: 0

Bin 2667: 136 of cap free
Amount of items: 2
Items: 
Size: 728789 Color: 0
Size: 271076 Color: 1

Bin 2668: 137 of cap free
Amount of items: 2
Items: 
Size: 587066 Color: 0
Size: 412798 Color: 1

Bin 2669: 137 of cap free
Amount of items: 2
Items: 
Size: 734568 Color: 1
Size: 265296 Color: 0

Bin 2670: 137 of cap free
Amount of items: 2
Items: 
Size: 510899 Color: 1
Size: 488965 Color: 0

Bin 2671: 137 of cap free
Amount of items: 2
Items: 
Size: 532104 Color: 1
Size: 467760 Color: 0

Bin 2672: 137 of cap free
Amount of items: 2
Items: 
Size: 575643 Color: 1
Size: 424221 Color: 0

Bin 2673: 137 of cap free
Amount of items: 2
Items: 
Size: 578945 Color: 0
Size: 420919 Color: 1

Bin 2674: 137 of cap free
Amount of items: 2
Items: 
Size: 666833 Color: 0
Size: 333031 Color: 1

Bin 2675: 137 of cap free
Amount of items: 2
Items: 
Size: 704592 Color: 1
Size: 295272 Color: 0

Bin 2676: 137 of cap free
Amount of items: 2
Items: 
Size: 716530 Color: 0
Size: 283334 Color: 1

Bin 2677: 137 of cap free
Amount of items: 2
Items: 
Size: 762266 Color: 1
Size: 237598 Color: 0

Bin 2678: 138 of cap free
Amount of items: 2
Items: 
Size: 546752 Color: 0
Size: 453111 Color: 1

Bin 2679: 138 of cap free
Amount of items: 2
Items: 
Size: 677386 Color: 1
Size: 322477 Color: 0

Bin 2680: 138 of cap free
Amount of items: 2
Items: 
Size: 788959 Color: 0
Size: 210904 Color: 1

Bin 2681: 139 of cap free
Amount of items: 2
Items: 
Size: 723595 Color: 1
Size: 276267 Color: 0

Bin 2682: 139 of cap free
Amount of items: 2
Items: 
Size: 542831 Color: 0
Size: 457031 Color: 1

Bin 2683: 139 of cap free
Amount of items: 2
Items: 
Size: 561155 Color: 1
Size: 438707 Color: 0

Bin 2684: 139 of cap free
Amount of items: 2
Items: 
Size: 652710 Color: 0
Size: 347152 Color: 1

Bin 2685: 139 of cap free
Amount of items: 2
Items: 
Size: 697618 Color: 1
Size: 302244 Color: 0

Bin 2686: 139 of cap free
Amount of items: 2
Items: 
Size: 722645 Color: 1
Size: 277217 Color: 0

Bin 2687: 140 of cap free
Amount of items: 2
Items: 
Size: 536597 Color: 0
Size: 463264 Color: 1

Bin 2688: 140 of cap free
Amount of items: 2
Items: 
Size: 543528 Color: 1
Size: 456333 Color: 0

Bin 2689: 140 of cap free
Amount of items: 2
Items: 
Size: 607018 Color: 1
Size: 392843 Color: 0

Bin 2690: 140 of cap free
Amount of items: 2
Items: 
Size: 668687 Color: 1
Size: 331174 Color: 0

Bin 2691: 140 of cap free
Amount of items: 2
Items: 
Size: 686758 Color: 0
Size: 313103 Color: 1

Bin 2692: 140 of cap free
Amount of items: 2
Items: 
Size: 690552 Color: 1
Size: 309309 Color: 0

Bin 2693: 141 of cap free
Amount of items: 2
Items: 
Size: 515677 Color: 0
Size: 484183 Color: 1

Bin 2694: 141 of cap free
Amount of items: 2
Items: 
Size: 535916 Color: 0
Size: 463944 Color: 1

Bin 2695: 141 of cap free
Amount of items: 2
Items: 
Size: 540140 Color: 0
Size: 459720 Color: 1

Bin 2696: 141 of cap free
Amount of items: 2
Items: 
Size: 592350 Color: 1
Size: 407510 Color: 0

Bin 2697: 141 of cap free
Amount of items: 2
Items: 
Size: 608729 Color: 1
Size: 391131 Color: 0

Bin 2698: 141 of cap free
Amount of items: 2
Items: 
Size: 676382 Color: 1
Size: 323478 Color: 0

Bin 2699: 141 of cap free
Amount of items: 2
Items: 
Size: 750674 Color: 0
Size: 249186 Color: 1

Bin 2700: 141 of cap free
Amount of items: 2
Items: 
Size: 795032 Color: 1
Size: 204828 Color: 0

Bin 2701: 142 of cap free
Amount of items: 2
Items: 
Size: 590019 Color: 1
Size: 409840 Color: 0

Bin 2702: 142 of cap free
Amount of items: 2
Items: 
Size: 756068 Color: 1
Size: 243791 Color: 0

Bin 2703: 142 of cap free
Amount of items: 2
Items: 
Size: 578478 Color: 1
Size: 421381 Color: 0

Bin 2704: 142 of cap free
Amount of items: 2
Items: 
Size: 602252 Color: 0
Size: 397607 Color: 1

Bin 2705: 142 of cap free
Amount of items: 2
Items: 
Size: 667184 Color: 1
Size: 332675 Color: 0

Bin 2706: 142 of cap free
Amount of items: 2
Items: 
Size: 669649 Color: 1
Size: 330210 Color: 0

Bin 2707: 142 of cap free
Amount of items: 2
Items: 
Size: 674898 Color: 1
Size: 324961 Color: 0

Bin 2708: 142 of cap free
Amount of items: 2
Items: 
Size: 699037 Color: 1
Size: 300822 Color: 0

Bin 2709: 143 of cap free
Amount of items: 2
Items: 
Size: 511991 Color: 1
Size: 487867 Color: 0

Bin 2710: 143 of cap free
Amount of items: 2
Items: 
Size: 519747 Color: 1
Size: 480111 Color: 0

Bin 2711: 143 of cap free
Amount of items: 2
Items: 
Size: 538702 Color: 1
Size: 461156 Color: 0

Bin 2712: 143 of cap free
Amount of items: 2
Items: 
Size: 549597 Color: 0
Size: 450261 Color: 1

Bin 2713: 143 of cap free
Amount of items: 2
Items: 
Size: 556295 Color: 1
Size: 443563 Color: 0

Bin 2714: 143 of cap free
Amount of items: 2
Items: 
Size: 615894 Color: 1
Size: 383964 Color: 0

Bin 2715: 143 of cap free
Amount of items: 2
Items: 
Size: 623973 Color: 0
Size: 375885 Color: 1

Bin 2716: 143 of cap free
Amount of items: 2
Items: 
Size: 663333 Color: 0
Size: 336525 Color: 1

Bin 2717: 143 of cap free
Amount of items: 2
Items: 
Size: 713229 Color: 1
Size: 286629 Color: 0

Bin 2718: 143 of cap free
Amount of items: 2
Items: 
Size: 773670 Color: 0
Size: 226188 Color: 1

Bin 2719: 143 of cap free
Amount of items: 2
Items: 
Size: 778417 Color: 1
Size: 221441 Color: 0

Bin 2720: 144 of cap free
Amount of items: 2
Items: 
Size: 623055 Color: 1
Size: 376802 Color: 0

Bin 2721: 144 of cap free
Amount of items: 2
Items: 
Size: 635464 Color: 0
Size: 364393 Color: 1

Bin 2722: 144 of cap free
Amount of items: 2
Items: 
Size: 677199 Color: 0
Size: 322658 Color: 1

Bin 2723: 144 of cap free
Amount of items: 2
Items: 
Size: 687045 Color: 1
Size: 312812 Color: 0

Bin 2724: 144 of cap free
Amount of items: 2
Items: 
Size: 741962 Color: 0
Size: 257895 Color: 1

Bin 2725: 145 of cap free
Amount of items: 2
Items: 
Size: 623998 Color: 1
Size: 375858 Color: 0

Bin 2726: 145 of cap free
Amount of items: 2
Items: 
Size: 624355 Color: 1
Size: 375501 Color: 0

Bin 2727: 145 of cap free
Amount of items: 2
Items: 
Size: 625524 Color: 1
Size: 374332 Color: 0

Bin 2728: 146 of cap free
Amount of items: 2
Items: 
Size: 757982 Color: 0
Size: 241873 Color: 1

Bin 2729: 146 of cap free
Amount of items: 2
Items: 
Size: 515006 Color: 1
Size: 484849 Color: 0

Bin 2730: 146 of cap free
Amount of items: 2
Items: 
Size: 570633 Color: 1
Size: 429222 Color: 0

Bin 2731: 146 of cap free
Amount of items: 2
Items: 
Size: 628825 Color: 0
Size: 371030 Color: 1

Bin 2732: 146 of cap free
Amount of items: 2
Items: 
Size: 661239 Color: 1
Size: 338616 Color: 0

Bin 2733: 146 of cap free
Amount of items: 2
Items: 
Size: 696991 Color: 0
Size: 302864 Color: 1

Bin 2734: 147 of cap free
Amount of items: 2
Items: 
Size: 608613 Color: 0
Size: 391241 Color: 1

Bin 2735: 147 of cap free
Amount of items: 2
Items: 
Size: 542095 Color: 1
Size: 457759 Color: 0

Bin 2736: 147 of cap free
Amount of items: 2
Items: 
Size: 586115 Color: 1
Size: 413739 Color: 0

Bin 2737: 147 of cap free
Amount of items: 2
Items: 
Size: 600342 Color: 0
Size: 399512 Color: 1

Bin 2738: 147 of cap free
Amount of items: 2
Items: 
Size: 679150 Color: 0
Size: 320704 Color: 1

Bin 2739: 147 of cap free
Amount of items: 2
Items: 
Size: 701128 Color: 0
Size: 298726 Color: 1

Bin 2740: 147 of cap free
Amount of items: 2
Items: 
Size: 719682 Color: 1
Size: 280172 Color: 0

Bin 2741: 147 of cap free
Amount of items: 2
Items: 
Size: 756603 Color: 0
Size: 243251 Color: 1

Bin 2742: 148 of cap free
Amount of items: 2
Items: 
Size: 583183 Color: 0
Size: 416670 Color: 1

Bin 2743: 148 of cap free
Amount of items: 2
Items: 
Size: 604707 Color: 1
Size: 395146 Color: 0

Bin 2744: 148 of cap free
Amount of items: 2
Items: 
Size: 612639 Color: 1
Size: 387214 Color: 0

Bin 2745: 148 of cap free
Amount of items: 2
Items: 
Size: 704723 Color: 0
Size: 295130 Color: 1

Bin 2746: 148 of cap free
Amount of items: 2
Items: 
Size: 796025 Color: 0
Size: 203828 Color: 1

Bin 2747: 149 of cap free
Amount of items: 2
Items: 
Size: 566641 Color: 1
Size: 433211 Color: 0

Bin 2748: 149 of cap free
Amount of items: 2
Items: 
Size: 584196 Color: 1
Size: 415656 Color: 0

Bin 2749: 150 of cap free
Amount of items: 2
Items: 
Size: 607898 Color: 0
Size: 391953 Color: 1

Bin 2750: 150 of cap free
Amount of items: 2
Items: 
Size: 727729 Color: 0
Size: 272122 Color: 1

Bin 2751: 150 of cap free
Amount of items: 2
Items: 
Size: 517311 Color: 0
Size: 482540 Color: 1

Bin 2752: 150 of cap free
Amount of items: 2
Items: 
Size: 594883 Color: 1
Size: 404968 Color: 0

Bin 2753: 150 of cap free
Amount of items: 2
Items: 
Size: 633093 Color: 1
Size: 366758 Color: 0

Bin 2754: 150 of cap free
Amount of items: 2
Items: 
Size: 721165 Color: 0
Size: 278686 Color: 1

Bin 2755: 151 of cap free
Amount of items: 2
Items: 
Size: 517919 Color: 0
Size: 481931 Color: 1

Bin 2756: 151 of cap free
Amount of items: 2
Items: 
Size: 569854 Color: 1
Size: 429996 Color: 0

Bin 2757: 151 of cap free
Amount of items: 2
Items: 
Size: 602124 Color: 1
Size: 397726 Color: 0

Bin 2758: 151 of cap free
Amount of items: 2
Items: 
Size: 616886 Color: 1
Size: 382964 Color: 0

Bin 2759: 151 of cap free
Amount of items: 2
Items: 
Size: 645033 Color: 0
Size: 354817 Color: 1

Bin 2760: 151 of cap free
Amount of items: 2
Items: 
Size: 652311 Color: 0
Size: 347539 Color: 1

Bin 2761: 152 of cap free
Amount of items: 2
Items: 
Size: 550764 Color: 0
Size: 449085 Color: 1

Bin 2762: 152 of cap free
Amount of items: 2
Items: 
Size: 554089 Color: 0
Size: 445760 Color: 1

Bin 2763: 152 of cap free
Amount of items: 2
Items: 
Size: 721164 Color: 0
Size: 278685 Color: 1

Bin 2764: 152 of cap free
Amount of items: 2
Items: 
Size: 732562 Color: 1
Size: 267287 Color: 0

Bin 2765: 152 of cap free
Amount of items: 2
Items: 
Size: 774974 Color: 0
Size: 224875 Color: 1

Bin 2766: 152 of cap free
Amount of items: 2
Items: 
Size: 786831 Color: 0
Size: 213018 Color: 1

Bin 2767: 153 of cap free
Amount of items: 3
Items: 
Size: 479103 Color: 0
Size: 376858 Color: 1
Size: 143887 Color: 1

Bin 2768: 153 of cap free
Amount of items: 2
Items: 
Size: 539515 Color: 1
Size: 460333 Color: 0

Bin 2769: 153 of cap free
Amount of items: 2
Items: 
Size: 559569 Color: 1
Size: 440279 Color: 0

Bin 2770: 153 of cap free
Amount of items: 2
Items: 
Size: 619012 Color: 1
Size: 380836 Color: 0

Bin 2771: 153 of cap free
Amount of items: 2
Items: 
Size: 701889 Color: 1
Size: 297959 Color: 0

Bin 2772: 154 of cap free
Amount of items: 2
Items: 
Size: 671366 Color: 1
Size: 328481 Color: 0

Bin 2773: 154 of cap free
Amount of items: 2
Items: 
Size: 683366 Color: 1
Size: 316481 Color: 0

Bin 2774: 154 of cap free
Amount of items: 2
Items: 
Size: 750236 Color: 0
Size: 249611 Color: 1

Bin 2775: 154 of cap free
Amount of items: 2
Items: 
Size: 754703 Color: 0
Size: 245144 Color: 1

Bin 2776: 155 of cap free
Amount of items: 2
Items: 
Size: 506184 Color: 0
Size: 493662 Color: 1

Bin 2777: 155 of cap free
Amount of items: 2
Items: 
Size: 515990 Color: 0
Size: 483856 Color: 1

Bin 2778: 155 of cap free
Amount of items: 2
Items: 
Size: 547485 Color: 0
Size: 452361 Color: 1

Bin 2779: 155 of cap free
Amount of items: 2
Items: 
Size: 567228 Color: 1
Size: 432618 Color: 0

Bin 2780: 155 of cap free
Amount of items: 2
Items: 
Size: 626115 Color: 0
Size: 373731 Color: 1

Bin 2781: 155 of cap free
Amount of items: 2
Items: 
Size: 688632 Color: 1
Size: 311214 Color: 0

Bin 2782: 155 of cap free
Amount of items: 2
Items: 
Size: 725972 Color: 0
Size: 273874 Color: 1

Bin 2783: 155 of cap free
Amount of items: 2
Items: 
Size: 790556 Color: 1
Size: 209290 Color: 0

Bin 2784: 155 of cap free
Amount of items: 2
Items: 
Size: 794736 Color: 1
Size: 205110 Color: 0

Bin 2785: 156 of cap free
Amount of items: 2
Items: 
Size: 561948 Color: 1
Size: 437897 Color: 0

Bin 2786: 156 of cap free
Amount of items: 2
Items: 
Size: 771100 Color: 1
Size: 228745 Color: 0

Bin 2787: 156 of cap free
Amount of items: 2
Items: 
Size: 794160 Color: 1
Size: 205685 Color: 0

Bin 2788: 157 of cap free
Amount of items: 2
Items: 
Size: 650804 Color: 1
Size: 349040 Color: 0

Bin 2789: 157 of cap free
Amount of items: 2
Items: 
Size: 790820 Color: 1
Size: 209024 Color: 0

Bin 2790: 157 of cap free
Amount of items: 2
Items: 
Size: 517273 Color: 1
Size: 482571 Color: 0

Bin 2791: 157 of cap free
Amount of items: 2
Items: 
Size: 653734 Color: 0
Size: 346110 Color: 1

Bin 2792: 157 of cap free
Amount of items: 2
Items: 
Size: 713506 Color: 0
Size: 286338 Color: 1

Bin 2793: 157 of cap free
Amount of items: 2
Items: 
Size: 725580 Color: 1
Size: 274264 Color: 0

Bin 2794: 157 of cap free
Amount of items: 2
Items: 
Size: 734046 Color: 0
Size: 265798 Color: 1

Bin 2795: 158 of cap free
Amount of items: 2
Items: 
Size: 740498 Color: 1
Size: 259345 Color: 0

Bin 2796: 158 of cap free
Amount of items: 2
Items: 
Size: 558488 Color: 0
Size: 441355 Color: 1

Bin 2797: 158 of cap free
Amount of items: 2
Items: 
Size: 740807 Color: 0
Size: 259036 Color: 1

Bin 2798: 159 of cap free
Amount of items: 2
Items: 
Size: 784169 Color: 0
Size: 215673 Color: 1

Bin 2799: 159 of cap free
Amount of items: 2
Items: 
Size: 503935 Color: 1
Size: 495907 Color: 0

Bin 2800: 159 of cap free
Amount of items: 2
Items: 
Size: 568179 Color: 0
Size: 431663 Color: 1

Bin 2801: 159 of cap free
Amount of items: 2
Items: 
Size: 673854 Color: 1
Size: 325988 Color: 0

Bin 2802: 159 of cap free
Amount of items: 2
Items: 
Size: 752502 Color: 1
Size: 247340 Color: 0

Bin 2803: 160 of cap free
Amount of items: 2
Items: 
Size: 614365 Color: 0
Size: 385476 Color: 1

Bin 2804: 160 of cap free
Amount of items: 2
Items: 
Size: 791012 Color: 1
Size: 208829 Color: 0

Bin 2805: 160 of cap free
Amount of items: 2
Items: 
Size: 507360 Color: 1
Size: 492481 Color: 0

Bin 2806: 160 of cap free
Amount of items: 2
Items: 
Size: 535052 Color: 0
Size: 464789 Color: 1

Bin 2807: 160 of cap free
Amount of items: 2
Items: 
Size: 611461 Color: 0
Size: 388380 Color: 1

Bin 2808: 160 of cap free
Amount of items: 2
Items: 
Size: 617329 Color: 1
Size: 382512 Color: 0

Bin 2809: 160 of cap free
Amount of items: 2
Items: 
Size: 640409 Color: 1
Size: 359432 Color: 0

Bin 2810: 160 of cap free
Amount of items: 2
Items: 
Size: 691786 Color: 1
Size: 308055 Color: 0

Bin 2811: 160 of cap free
Amount of items: 2
Items: 
Size: 706829 Color: 0
Size: 293012 Color: 1

Bin 2812: 160 of cap free
Amount of items: 2
Items: 
Size: 734641 Color: 0
Size: 265200 Color: 1

Bin 2813: 161 of cap free
Amount of items: 2
Items: 
Size: 745946 Color: 1
Size: 253894 Color: 0

Bin 2814: 161 of cap free
Amount of items: 2
Items: 
Size: 574646 Color: 1
Size: 425194 Color: 0

Bin 2815: 161 of cap free
Amount of items: 2
Items: 
Size: 689668 Color: 1
Size: 310172 Color: 0

Bin 2816: 161 of cap free
Amount of items: 2
Items: 
Size: 715290 Color: 1
Size: 284550 Color: 0

Bin 2817: 161 of cap free
Amount of items: 2
Items: 
Size: 770481 Color: 1
Size: 229359 Color: 0

Bin 2818: 162 of cap free
Amount of items: 2
Items: 
Size: 563711 Color: 0
Size: 436128 Color: 1

Bin 2819: 162 of cap free
Amount of items: 2
Items: 
Size: 586773 Color: 0
Size: 413066 Color: 1

Bin 2820: 162 of cap free
Amount of items: 2
Items: 
Size: 683945 Color: 1
Size: 315894 Color: 0

Bin 2821: 162 of cap free
Amount of items: 2
Items: 
Size: 708537 Color: 0
Size: 291302 Color: 1

Bin 2822: 162 of cap free
Amount of items: 2
Items: 
Size: 712272 Color: 1
Size: 287567 Color: 0

Bin 2823: 162 of cap free
Amount of items: 2
Items: 
Size: 723257 Color: 1
Size: 276582 Color: 0

Bin 2824: 162 of cap free
Amount of items: 2
Items: 
Size: 741426 Color: 1
Size: 258413 Color: 0

Bin 2825: 162 of cap free
Amount of items: 2
Items: 
Size: 766606 Color: 1
Size: 233233 Color: 0

Bin 2826: 162 of cap free
Amount of items: 2
Items: 
Size: 770210 Color: 1
Size: 229629 Color: 0

Bin 2827: 162 of cap free
Amount of items: 2
Items: 
Size: 799873 Color: 1
Size: 199966 Color: 0

Bin 2828: 163 of cap free
Amount of items: 2
Items: 
Size: 761609 Color: 0
Size: 238229 Color: 1

Bin 2829: 163 of cap free
Amount of items: 2
Items: 
Size: 540807 Color: 0
Size: 459031 Color: 1

Bin 2830: 163 of cap free
Amount of items: 2
Items: 
Size: 544801 Color: 0
Size: 455037 Color: 1

Bin 2831: 163 of cap free
Amount of items: 2
Items: 
Size: 577563 Color: 0
Size: 422275 Color: 1

Bin 2832: 163 of cap free
Amount of items: 2
Items: 
Size: 665814 Color: 0
Size: 334024 Color: 1

Bin 2833: 163 of cap free
Amount of items: 2
Items: 
Size: 754493 Color: 1
Size: 245345 Color: 0

Bin 2834: 164 of cap free
Amount of items: 2
Items: 
Size: 560788 Color: 0
Size: 439049 Color: 1

Bin 2835: 164 of cap free
Amount of items: 2
Items: 
Size: 720029 Color: 1
Size: 279808 Color: 0

Bin 2836: 165 of cap free
Amount of items: 2
Items: 
Size: 552760 Color: 1
Size: 447076 Color: 0

Bin 2837: 165 of cap free
Amount of items: 2
Items: 
Size: 500640 Color: 0
Size: 499196 Color: 1

Bin 2838: 165 of cap free
Amount of items: 2
Items: 
Size: 577501 Color: 1
Size: 422335 Color: 0

Bin 2839: 165 of cap free
Amount of items: 2
Items: 
Size: 616321 Color: 1
Size: 383515 Color: 0

Bin 2840: 165 of cap free
Amount of items: 2
Items: 
Size: 632837 Color: 0
Size: 366999 Color: 1

Bin 2841: 166 of cap free
Amount of items: 2
Items: 
Size: 533431 Color: 0
Size: 466404 Color: 1

Bin 2842: 166 of cap free
Amount of items: 2
Items: 
Size: 534509 Color: 1
Size: 465326 Color: 0

Bin 2843: 166 of cap free
Amount of items: 2
Items: 
Size: 639424 Color: 0
Size: 360411 Color: 1

Bin 2844: 166 of cap free
Amount of items: 2
Items: 
Size: 702064 Color: 1
Size: 297771 Color: 0

Bin 2845: 166 of cap free
Amount of items: 2
Items: 
Size: 775889 Color: 1
Size: 223946 Color: 0

Bin 2846: 167 of cap free
Amount of items: 2
Items: 
Size: 542338 Color: 0
Size: 457496 Color: 1

Bin 2847: 167 of cap free
Amount of items: 2
Items: 
Size: 662644 Color: 0
Size: 337190 Color: 1

Bin 2848: 168 of cap free
Amount of items: 2
Items: 
Size: 552397 Color: 0
Size: 447436 Color: 1

Bin 2849: 168 of cap free
Amount of items: 2
Items: 
Size: 585774 Color: 0
Size: 414059 Color: 1

Bin 2850: 168 of cap free
Amount of items: 2
Items: 
Size: 675281 Color: 1
Size: 324552 Color: 0

Bin 2851: 168 of cap free
Amount of items: 2
Items: 
Size: 748757 Color: 1
Size: 251076 Color: 0

Bin 2852: 168 of cap free
Amount of items: 2
Items: 
Size: 775030 Color: 1
Size: 224803 Color: 0

Bin 2853: 168 of cap free
Amount of items: 2
Items: 
Size: 777434 Color: 0
Size: 222399 Color: 1

Bin 2854: 169 of cap free
Amount of items: 2
Items: 
Size: 575469 Color: 0
Size: 424363 Color: 1

Bin 2855: 169 of cap free
Amount of items: 2
Items: 
Size: 524891 Color: 1
Size: 474941 Color: 0

Bin 2856: 169 of cap free
Amount of items: 2
Items: 
Size: 548986 Color: 1
Size: 450846 Color: 0

Bin 2857: 169 of cap free
Amount of items: 2
Items: 
Size: 583170 Color: 1
Size: 416662 Color: 0

Bin 2858: 169 of cap free
Amount of items: 2
Items: 
Size: 669717 Color: 0
Size: 330115 Color: 1

Bin 2859: 169 of cap free
Amount of items: 2
Items: 
Size: 719102 Color: 0
Size: 280730 Color: 1

Bin 2860: 170 of cap free
Amount of items: 2
Items: 
Size: 619846 Color: 1
Size: 379985 Color: 0

Bin 2861: 170 of cap free
Amount of items: 2
Items: 
Size: 784764 Color: 1
Size: 215067 Color: 0

Bin 2862: 170 of cap free
Amount of items: 2
Items: 
Size: 747015 Color: 1
Size: 252816 Color: 0

Bin 2863: 170 of cap free
Amount of items: 2
Items: 
Size: 556831 Color: 1
Size: 443000 Color: 0

Bin 2864: 170 of cap free
Amount of items: 2
Items: 
Size: 548581 Color: 0
Size: 451250 Color: 1

Bin 2865: 170 of cap free
Amount of items: 2
Items: 
Size: 581889 Color: 1
Size: 417942 Color: 0

Bin 2866: 170 of cap free
Amount of items: 2
Items: 
Size: 718958 Color: 1
Size: 280873 Color: 0

Bin 2867: 170 of cap free
Amount of items: 2
Items: 
Size: 757186 Color: 1
Size: 242645 Color: 0

Bin 2868: 170 of cap free
Amount of items: 2
Items: 
Size: 768038 Color: 0
Size: 231793 Color: 1

Bin 2869: 171 of cap free
Amount of items: 2
Items: 
Size: 729984 Color: 0
Size: 269846 Color: 1

Bin 2870: 171 of cap free
Amount of items: 2
Items: 
Size: 662733 Color: 1
Size: 337097 Color: 0

Bin 2871: 171 of cap free
Amount of items: 2
Items: 
Size: 683101 Color: 0
Size: 316729 Color: 1

Bin 2872: 171 of cap free
Amount of items: 2
Items: 
Size: 781544 Color: 1
Size: 218286 Color: 0

Bin 2873: 172 of cap free
Amount of items: 2
Items: 
Size: 605085 Color: 1
Size: 394744 Color: 0

Bin 2874: 172 of cap free
Amount of items: 2
Items: 
Size: 670228 Color: 0
Size: 329601 Color: 1

Bin 2875: 172 of cap free
Amount of items: 2
Items: 
Size: 691737 Color: 0
Size: 308092 Color: 1

Bin 2876: 173 of cap free
Amount of items: 2
Items: 
Size: 788887 Color: 1
Size: 210941 Color: 0

Bin 2877: 173 of cap free
Amount of items: 2
Items: 
Size: 523213 Color: 1
Size: 476615 Color: 0

Bin 2878: 173 of cap free
Amount of items: 2
Items: 
Size: 514292 Color: 0
Size: 485536 Color: 1

Bin 2879: 173 of cap free
Amount of items: 2
Items: 
Size: 665989 Color: 0
Size: 333839 Color: 1

Bin 2880: 173 of cap free
Amount of items: 2
Items: 
Size: 693733 Color: 0
Size: 306095 Color: 1

Bin 2881: 173 of cap free
Amount of items: 2
Items: 
Size: 744345 Color: 0
Size: 255483 Color: 1

Bin 2882: 173 of cap free
Amount of items: 2
Items: 
Size: 782570 Color: 1
Size: 217258 Color: 0

Bin 2883: 173 of cap free
Amount of items: 2
Items: 
Size: 793655 Color: 1
Size: 206173 Color: 0

Bin 2884: 174 of cap free
Amount of items: 2
Items: 
Size: 512079 Color: 0
Size: 487748 Color: 1

Bin 2885: 174 of cap free
Amount of items: 2
Items: 
Size: 594652 Color: 0
Size: 405175 Color: 1

Bin 2886: 174 of cap free
Amount of items: 2
Items: 
Size: 735742 Color: 1
Size: 264085 Color: 0

Bin 2887: 174 of cap free
Amount of items: 2
Items: 
Size: 761448 Color: 1
Size: 238379 Color: 0

Bin 2888: 175 of cap free
Amount of items: 2
Items: 
Size: 735241 Color: 0
Size: 264585 Color: 1

Bin 2889: 175 of cap free
Amount of items: 2
Items: 
Size: 548389 Color: 1
Size: 451437 Color: 0

Bin 2890: 175 of cap free
Amount of items: 2
Items: 
Size: 615268 Color: 1
Size: 384558 Color: 0

Bin 2891: 175 of cap free
Amount of items: 2
Items: 
Size: 690082 Color: 1
Size: 309744 Color: 0

Bin 2892: 175 of cap free
Amount of items: 2
Items: 
Size: 710679 Color: 1
Size: 289147 Color: 0

Bin 2893: 176 of cap free
Amount of items: 2
Items: 
Size: 774753 Color: 1
Size: 225072 Color: 0

Bin 2894: 176 of cap free
Amount of items: 2
Items: 
Size: 594140 Color: 1
Size: 405685 Color: 0

Bin 2895: 176 of cap free
Amount of items: 2
Items: 
Size: 678309 Color: 1
Size: 321516 Color: 0

Bin 2896: 176 of cap free
Amount of items: 2
Items: 
Size: 685300 Color: 1
Size: 314525 Color: 0

Bin 2897: 177 of cap free
Amount of items: 2
Items: 
Size: 596462 Color: 0
Size: 403362 Color: 1

Bin 2898: 177 of cap free
Amount of items: 2
Items: 
Size: 503587 Color: 0
Size: 496237 Color: 1

Bin 2899: 177 of cap free
Amount of items: 2
Items: 
Size: 562706 Color: 1
Size: 437118 Color: 0

Bin 2900: 177 of cap free
Amount of items: 2
Items: 
Size: 570239 Color: 1
Size: 429585 Color: 0

Bin 2901: 177 of cap free
Amount of items: 2
Items: 
Size: 679872 Color: 0
Size: 319952 Color: 1

Bin 2902: 177 of cap free
Amount of items: 2
Items: 
Size: 702824 Color: 1
Size: 297000 Color: 0

Bin 2903: 177 of cap free
Amount of items: 2
Items: 
Size: 787547 Color: 1
Size: 212277 Color: 0

Bin 2904: 178 of cap free
Amount of items: 2
Items: 
Size: 509327 Color: 0
Size: 490496 Color: 1

Bin 2905: 178 of cap free
Amount of items: 2
Items: 
Size: 590652 Color: 0
Size: 409171 Color: 1

Bin 2906: 178 of cap free
Amount of items: 2
Items: 
Size: 652432 Color: 1
Size: 347391 Color: 0

Bin 2907: 178 of cap free
Amount of items: 2
Items: 
Size: 668862 Color: 0
Size: 330961 Color: 1

Bin 2908: 178 of cap free
Amount of items: 2
Items: 
Size: 794115 Color: 0
Size: 205708 Color: 1

Bin 2909: 179 of cap free
Amount of items: 2
Items: 
Size: 673812 Color: 0
Size: 326010 Color: 1

Bin 2910: 179 of cap free
Amount of items: 2
Items: 
Size: 772973 Color: 0
Size: 226849 Color: 1

Bin 2911: 179 of cap free
Amount of items: 2
Items: 
Size: 513213 Color: 0
Size: 486609 Color: 1

Bin 2912: 179 of cap free
Amount of items: 2
Items: 
Size: 554618 Color: 0
Size: 445204 Color: 1

Bin 2913: 179 of cap free
Amount of items: 2
Items: 
Size: 563166 Color: 0
Size: 436656 Color: 1

Bin 2914: 179 of cap free
Amount of items: 2
Items: 
Size: 657606 Color: 0
Size: 342216 Color: 1

Bin 2915: 179 of cap free
Amount of items: 2
Items: 
Size: 657798 Color: 1
Size: 342024 Color: 0

Bin 2916: 179 of cap free
Amount of items: 2
Items: 
Size: 748456 Color: 0
Size: 251366 Color: 1

Bin 2917: 180 of cap free
Amount of items: 2
Items: 
Size: 678848 Color: 1
Size: 320973 Color: 0

Bin 2918: 180 of cap free
Amount of items: 2
Items: 
Size: 722279 Color: 1
Size: 277542 Color: 0

Bin 2919: 180 of cap free
Amount of items: 2
Items: 
Size: 504934 Color: 0
Size: 494887 Color: 1

Bin 2920: 180 of cap free
Amount of items: 2
Items: 
Size: 528388 Color: 1
Size: 471433 Color: 0

Bin 2921: 180 of cap free
Amount of items: 2
Items: 
Size: 687221 Color: 0
Size: 312600 Color: 1

Bin 2922: 181 of cap free
Amount of items: 2
Items: 
Size: 770044 Color: 0
Size: 229776 Color: 1

Bin 2923: 181 of cap free
Amount of items: 2
Items: 
Size: 510049 Color: 0
Size: 489771 Color: 1

Bin 2924: 181 of cap free
Amount of items: 2
Items: 
Size: 516499 Color: 1
Size: 483321 Color: 0

Bin 2925: 181 of cap free
Amount of items: 2
Items: 
Size: 654559 Color: 1
Size: 345261 Color: 0

Bin 2926: 181 of cap free
Amount of items: 2
Items: 
Size: 687755 Color: 1
Size: 312065 Color: 0

Bin 2927: 181 of cap free
Amount of items: 2
Items: 
Size: 689140 Color: 1
Size: 310680 Color: 0

Bin 2928: 181 of cap free
Amount of items: 2
Items: 
Size: 691213 Color: 1
Size: 308607 Color: 0

Bin 2929: 182 of cap free
Amount of items: 2
Items: 
Size: 714889 Color: 0
Size: 284930 Color: 1

Bin 2930: 183 of cap free
Amount of items: 2
Items: 
Size: 603981 Color: 0
Size: 395837 Color: 1

Bin 2931: 183 of cap free
Amount of items: 2
Items: 
Size: 612998 Color: 1
Size: 386820 Color: 0

Bin 2932: 183 of cap free
Amount of items: 2
Items: 
Size: 719654 Color: 1
Size: 280164 Color: 0

Bin 2933: 184 of cap free
Amount of items: 2
Items: 
Size: 594378 Color: 1
Size: 405439 Color: 0

Bin 2934: 184 of cap free
Amount of items: 2
Items: 
Size: 558093 Color: 0
Size: 441724 Color: 1

Bin 2935: 184 of cap free
Amount of items: 2
Items: 
Size: 600734 Color: 0
Size: 399083 Color: 1

Bin 2936: 185 of cap free
Amount of items: 2
Items: 
Size: 757113 Color: 0
Size: 242703 Color: 1

Bin 2937: 185 of cap free
Amount of items: 2
Items: 
Size: 784212 Color: 1
Size: 215604 Color: 0

Bin 2938: 186 of cap free
Amount of items: 2
Items: 
Size: 561038 Color: 0
Size: 438777 Color: 1

Bin 2939: 186 of cap free
Amount of items: 2
Items: 
Size: 642816 Color: 0
Size: 356999 Color: 1

Bin 2940: 186 of cap free
Amount of items: 2
Items: 
Size: 734032 Color: 0
Size: 265783 Color: 1

Bin 2941: 187 of cap free
Amount of items: 5
Items: 
Size: 281952 Color: 0
Size: 199380 Color: 0
Size: 181141 Color: 1
Size: 171701 Color: 0
Size: 165640 Color: 1

Bin 2942: 187 of cap free
Amount of items: 6
Items: 
Size: 177076 Color: 0
Size: 174928 Color: 0
Size: 174214 Color: 1
Size: 171477 Color: 1
Size: 169777 Color: 1
Size: 132342 Color: 0

Bin 2943: 187 of cap free
Amount of items: 2
Items: 
Size: 520375 Color: 1
Size: 479439 Color: 0

Bin 2944: 187 of cap free
Amount of items: 2
Items: 
Size: 784141 Color: 0
Size: 215673 Color: 1

Bin 2945: 188 of cap free
Amount of items: 2
Items: 
Size: 523597 Color: 0
Size: 476216 Color: 1

Bin 2946: 188 of cap free
Amount of items: 2
Items: 
Size: 610232 Color: 1
Size: 389581 Color: 0

Bin 2947: 188 of cap free
Amount of items: 2
Items: 
Size: 624694 Color: 0
Size: 375119 Color: 1

Bin 2948: 188 of cap free
Amount of items: 2
Items: 
Size: 705041 Color: 1
Size: 294772 Color: 0

Bin 2949: 188 of cap free
Amount of items: 2
Items: 
Size: 767543 Color: 0
Size: 232270 Color: 1

Bin 2950: 189 of cap free
Amount of items: 5
Items: 
Size: 235222 Color: 1
Size: 192077 Color: 0
Size: 191964 Color: 0
Size: 191674 Color: 0
Size: 188875 Color: 1

Bin 2951: 189 of cap free
Amount of items: 2
Items: 
Size: 698116 Color: 1
Size: 301696 Color: 0

Bin 2952: 189 of cap free
Amount of items: 2
Items: 
Size: 710848 Color: 0
Size: 288964 Color: 1

Bin 2953: 190 of cap free
Amount of items: 2
Items: 
Size: 631394 Color: 1
Size: 368417 Color: 0

Bin 2954: 190 of cap free
Amount of items: 2
Items: 
Size: 650360 Color: 1
Size: 349451 Color: 0

Bin 2955: 190 of cap free
Amount of items: 2
Items: 
Size: 676822 Color: 1
Size: 322989 Color: 0

Bin 2956: 191 of cap free
Amount of items: 2
Items: 
Size: 639628 Color: 1
Size: 360182 Color: 0

Bin 2957: 191 of cap free
Amount of items: 2
Items: 
Size: 568664 Color: 0
Size: 431146 Color: 1

Bin 2958: 191 of cap free
Amount of items: 2
Items: 
Size: 625749 Color: 1
Size: 374061 Color: 0

Bin 2959: 191 of cap free
Amount of items: 2
Items: 
Size: 651590 Color: 0
Size: 348220 Color: 1

Bin 2960: 191 of cap free
Amount of items: 2
Items: 
Size: 659070 Color: 0
Size: 340740 Color: 1

Bin 2961: 191 of cap free
Amount of items: 2
Items: 
Size: 747350 Color: 0
Size: 252460 Color: 1

Bin 2962: 192 of cap free
Amount of items: 2
Items: 
Size: 590360 Color: 0
Size: 409449 Color: 1

Bin 2963: 192 of cap free
Amount of items: 2
Items: 
Size: 599829 Color: 1
Size: 399980 Color: 0

Bin 2964: 192 of cap free
Amount of items: 2
Items: 
Size: 741794 Color: 1
Size: 258015 Color: 0

Bin 2965: 192 of cap free
Amount of items: 2
Items: 
Size: 796119 Color: 1
Size: 203690 Color: 0

Bin 2966: 193 of cap free
Amount of items: 2
Items: 
Size: 585673 Color: 1
Size: 414135 Color: 0

Bin 2967: 193 of cap free
Amount of items: 2
Items: 
Size: 624987 Color: 1
Size: 374821 Color: 0

Bin 2968: 193 of cap free
Amount of items: 2
Items: 
Size: 659977 Color: 0
Size: 339831 Color: 1

Bin 2969: 193 of cap free
Amount of items: 2
Items: 
Size: 730327 Color: 0
Size: 269481 Color: 1

Bin 2970: 194 of cap free
Amount of items: 2
Items: 
Size: 755618 Color: 0
Size: 244189 Color: 1

Bin 2971: 194 of cap free
Amount of items: 2
Items: 
Size: 757978 Color: 0
Size: 241829 Color: 1

Bin 2972: 194 of cap free
Amount of items: 2
Items: 
Size: 562414 Color: 0
Size: 437393 Color: 1

Bin 2973: 194 of cap free
Amount of items: 2
Items: 
Size: 578491 Color: 0
Size: 421316 Color: 1

Bin 2974: 194 of cap free
Amount of items: 2
Items: 
Size: 653734 Color: 0
Size: 346073 Color: 1

Bin 2975: 195 of cap free
Amount of items: 2
Items: 
Size: 525495 Color: 1
Size: 474311 Color: 0

Bin 2976: 195 of cap free
Amount of items: 2
Items: 
Size: 535498 Color: 0
Size: 464308 Color: 1

Bin 2977: 195 of cap free
Amount of items: 2
Items: 
Size: 561100 Color: 1
Size: 438706 Color: 0

Bin 2978: 195 of cap free
Amount of items: 2
Items: 
Size: 633301 Color: 1
Size: 366505 Color: 0

Bin 2979: 195 of cap free
Amount of items: 2
Items: 
Size: 639969 Color: 0
Size: 359837 Color: 1

Bin 2980: 195 of cap free
Amount of items: 2
Items: 
Size: 675682 Color: 1
Size: 324124 Color: 0

Bin 2981: 196 of cap free
Amount of items: 2
Items: 
Size: 566118 Color: 0
Size: 433687 Color: 1

Bin 2982: 196 of cap free
Amount of items: 2
Items: 
Size: 666826 Color: 0
Size: 332979 Color: 1

Bin 2983: 196 of cap free
Amount of items: 2
Items: 
Size: 688656 Color: 0
Size: 311149 Color: 1

Bin 2984: 197 of cap free
Amount of items: 2
Items: 
Size: 548979 Color: 0
Size: 450825 Color: 1

Bin 2985: 197 of cap free
Amount of items: 2
Items: 
Size: 512877 Color: 0
Size: 486927 Color: 1

Bin 2986: 197 of cap free
Amount of items: 5
Items: 
Size: 342771 Color: 1
Size: 188889 Color: 1
Size: 177966 Color: 0
Size: 175621 Color: 1
Size: 114557 Color: 0

Bin 2987: 198 of cap free
Amount of items: 2
Items: 
Size: 678242 Color: 0
Size: 321561 Color: 1

Bin 2988: 198 of cap free
Amount of items: 2
Items: 
Size: 565364 Color: 0
Size: 434439 Color: 1

Bin 2989: 198 of cap free
Amount of items: 2
Items: 
Size: 531448 Color: 1
Size: 468355 Color: 0

Bin 2990: 198 of cap free
Amount of items: 2
Items: 
Size: 606683 Color: 0
Size: 393120 Color: 1

Bin 2991: 198 of cap free
Amount of items: 2
Items: 
Size: 641952 Color: 0
Size: 357851 Color: 1

Bin 2992: 198 of cap free
Amount of items: 2
Items: 
Size: 709372 Color: 0
Size: 290431 Color: 1

Bin 2993: 198 of cap free
Amount of items: 2
Items: 
Size: 743027 Color: 0
Size: 256776 Color: 1

Bin 2994: 199 of cap free
Amount of items: 2
Items: 
Size: 500285 Color: 0
Size: 499517 Color: 1

Bin 2995: 199 of cap free
Amount of items: 2
Items: 
Size: 693429 Color: 1
Size: 306373 Color: 0

Bin 2996: 199 of cap free
Amount of items: 2
Items: 
Size: 707858 Color: 0
Size: 291944 Color: 1

Bin 2997: 199 of cap free
Amount of items: 2
Items: 
Size: 754669 Color: 0
Size: 245133 Color: 1

Bin 2998: 200 of cap free
Amount of items: 2
Items: 
Size: 695085 Color: 0
Size: 304716 Color: 1

Bin 2999: 200 of cap free
Amount of items: 2
Items: 
Size: 569429 Color: 0
Size: 430372 Color: 1

Bin 3000: 200 of cap free
Amount of items: 2
Items: 
Size: 637956 Color: 1
Size: 361845 Color: 0

Bin 3001: 201 of cap free
Amount of items: 2
Items: 
Size: 776876 Color: 1
Size: 222924 Color: 0

Bin 3002: 201 of cap free
Amount of items: 2
Items: 
Size: 620215 Color: 1
Size: 379585 Color: 0

Bin 3003: 201 of cap free
Amount of items: 2
Items: 
Size: 706824 Color: 0
Size: 292976 Color: 1

Bin 3004: 202 of cap free
Amount of items: 2
Items: 
Size: 706231 Color: 0
Size: 293568 Color: 1

Bin 3005: 202 of cap free
Amount of items: 2
Items: 
Size: 731141 Color: 0
Size: 268658 Color: 1

Bin 3006: 202 of cap free
Amount of items: 2
Items: 
Size: 521427 Color: 0
Size: 478372 Color: 1

Bin 3007: 202 of cap free
Amount of items: 2
Items: 
Size: 765738 Color: 1
Size: 234061 Color: 0

Bin 3008: 202 of cap free
Amount of items: 2
Items: 
Size: 771928 Color: 1
Size: 227871 Color: 0

Bin 3009: 203 of cap free
Amount of items: 2
Items: 
Size: 629529 Color: 1
Size: 370269 Color: 0

Bin 3010: 203 of cap free
Amount of items: 2
Items: 
Size: 660670 Color: 0
Size: 339128 Color: 1

Bin 3011: 203 of cap free
Amount of items: 2
Items: 
Size: 662138 Color: 0
Size: 337660 Color: 1

Bin 3012: 203 of cap free
Amount of items: 2
Items: 
Size: 774435 Color: 1
Size: 225363 Color: 0

Bin 3013: 204 of cap free
Amount of items: 2
Items: 
Size: 503137 Color: 0
Size: 496660 Color: 1

Bin 3014: 204 of cap free
Amount of items: 2
Items: 
Size: 544327 Color: 1
Size: 455470 Color: 0

Bin 3015: 204 of cap free
Amount of items: 2
Items: 
Size: 667802 Color: 1
Size: 331995 Color: 0

Bin 3016: 204 of cap free
Amount of items: 2
Items: 
Size: 759870 Color: 1
Size: 239927 Color: 0

Bin 3017: 204 of cap free
Amount of items: 2
Items: 
Size: 785753 Color: 0
Size: 214044 Color: 1

Bin 3018: 205 of cap free
Amount of items: 2
Items: 
Size: 537527 Color: 1
Size: 462269 Color: 0

Bin 3019: 205 of cap free
Amount of items: 2
Items: 
Size: 550487 Color: 0
Size: 449309 Color: 1

Bin 3020: 205 of cap free
Amount of items: 2
Items: 
Size: 631834 Color: 0
Size: 367962 Color: 1

Bin 3021: 205 of cap free
Amount of items: 2
Items: 
Size: 748744 Color: 1
Size: 251052 Color: 0

Bin 3022: 205 of cap free
Amount of items: 2
Items: 
Size: 799421 Color: 1
Size: 200375 Color: 0

Bin 3023: 206 of cap free
Amount of items: 2
Items: 
Size: 634398 Color: 1
Size: 365397 Color: 0

Bin 3024: 206 of cap free
Amount of items: 2
Items: 
Size: 758992 Color: 0
Size: 240803 Color: 1

Bin 3025: 207 of cap free
Amount of items: 2
Items: 
Size: 573141 Color: 1
Size: 426653 Color: 0

Bin 3026: 207 of cap free
Amount of items: 2
Items: 
Size: 605475 Color: 0
Size: 394319 Color: 1

Bin 3027: 208 of cap free
Amount of items: 2
Items: 
Size: 739731 Color: 0
Size: 260062 Color: 1

Bin 3028: 208 of cap free
Amount of items: 2
Items: 
Size: 532482 Color: 1
Size: 467311 Color: 0

Bin 3029: 208 of cap free
Amount of items: 2
Items: 
Size: 535050 Color: 0
Size: 464743 Color: 1

Bin 3030: 208 of cap free
Amount of items: 2
Items: 
Size: 573554 Color: 0
Size: 426239 Color: 1

Bin 3031: 208 of cap free
Amount of items: 2
Items: 
Size: 743978 Color: 1
Size: 255815 Color: 0

Bin 3032: 208 of cap free
Amount of items: 2
Items: 
Size: 744960 Color: 0
Size: 254833 Color: 1

Bin 3033: 208 of cap free
Amount of items: 2
Items: 
Size: 755324 Color: 1
Size: 244469 Color: 0

Bin 3034: 209 of cap free
Amount of items: 2
Items: 
Size: 590935 Color: 1
Size: 408857 Color: 0

Bin 3035: 209 of cap free
Amount of items: 2
Items: 
Size: 591652 Color: 0
Size: 408140 Color: 1

Bin 3036: 209 of cap free
Amount of items: 2
Items: 
Size: 639010 Color: 0
Size: 360782 Color: 1

Bin 3037: 209 of cap free
Amount of items: 2
Items: 
Size: 694316 Color: 0
Size: 305476 Color: 1

Bin 3038: 209 of cap free
Amount of items: 2
Items: 
Size: 737680 Color: 0
Size: 262112 Color: 1

Bin 3039: 210 of cap free
Amount of items: 2
Items: 
Size: 644980 Color: 0
Size: 354811 Color: 1

Bin 3040: 210 of cap free
Amount of items: 2
Items: 
Size: 649203 Color: 0
Size: 350588 Color: 1

Bin 3041: 211 of cap free
Amount of items: 2
Items: 
Size: 511393 Color: 0
Size: 488397 Color: 1

Bin 3042: 211 of cap free
Amount of items: 2
Items: 
Size: 550228 Color: 0
Size: 449562 Color: 1

Bin 3043: 211 of cap free
Amount of items: 2
Items: 
Size: 680166 Color: 0
Size: 319624 Color: 1

Bin 3044: 211 of cap free
Amount of items: 2
Items: 
Size: 763404 Color: 1
Size: 236386 Color: 0

Bin 3045: 212 of cap free
Amount of items: 2
Items: 
Size: 602602 Color: 1
Size: 397187 Color: 0

Bin 3046: 213 of cap free
Amount of items: 2
Items: 
Size: 517532 Color: 0
Size: 482256 Color: 1

Bin 3047: 213 of cap free
Amount of items: 2
Items: 
Size: 543902 Color: 1
Size: 455886 Color: 0

Bin 3048: 213 of cap free
Amount of items: 2
Items: 
Size: 665176 Color: 1
Size: 334612 Color: 0

Bin 3049: 213 of cap free
Amount of items: 2
Items: 
Size: 687812 Color: 0
Size: 311976 Color: 1

Bin 3050: 214 of cap free
Amount of items: 2
Items: 
Size: 764574 Color: 0
Size: 235213 Color: 1

Bin 3051: 214 of cap free
Amount of items: 3
Items: 
Size: 476561 Color: 0
Size: 376777 Color: 1
Size: 146449 Color: 1

Bin 3052: 214 of cap free
Amount of items: 2
Items: 
Size: 601107 Color: 1
Size: 398680 Color: 0

Bin 3053: 214 of cap free
Amount of items: 2
Items: 
Size: 664258 Color: 1
Size: 335529 Color: 0

Bin 3054: 214 of cap free
Amount of items: 2
Items: 
Size: 670708 Color: 0
Size: 329079 Color: 1

Bin 3055: 214 of cap free
Amount of items: 2
Items: 
Size: 763059 Color: 0
Size: 236728 Color: 1

Bin 3056: 214 of cap free
Amount of items: 2
Items: 
Size: 783829 Color: 1
Size: 215958 Color: 0

Bin 3057: 215 of cap free
Amount of items: 2
Items: 
Size: 556260 Color: 1
Size: 443526 Color: 0

Bin 3058: 215 of cap free
Amount of items: 2
Items: 
Size: 584147 Color: 0
Size: 415639 Color: 1

Bin 3059: 215 of cap free
Amount of items: 2
Items: 
Size: 612274 Color: 0
Size: 387512 Color: 1

Bin 3060: 215 of cap free
Amount of items: 2
Items: 
Size: 637950 Color: 1
Size: 361836 Color: 0

Bin 3061: 216 of cap free
Amount of items: 5
Items: 
Size: 317576 Color: 0
Size: 198097 Color: 0
Size: 187563 Color: 0
Size: 185218 Color: 1
Size: 111331 Color: 1

Bin 3062: 216 of cap free
Amount of items: 2
Items: 
Size: 577719 Color: 1
Size: 422066 Color: 0

Bin 3063: 216 of cap free
Amount of items: 2
Items: 
Size: 530984 Color: 1
Size: 468801 Color: 0

Bin 3064: 216 of cap free
Amount of items: 2
Items: 
Size: 644800 Color: 1
Size: 354985 Color: 0

Bin 3065: 216 of cap free
Amount of items: 2
Items: 
Size: 760093 Color: 1
Size: 239692 Color: 0

Bin 3066: 217 of cap free
Amount of items: 2
Items: 
Size: 514895 Color: 0
Size: 484889 Color: 1

Bin 3067: 217 of cap free
Amount of items: 2
Items: 
Size: 535641 Color: 1
Size: 464143 Color: 0

Bin 3068: 217 of cap free
Amount of items: 2
Items: 
Size: 550602 Color: 1
Size: 449182 Color: 0

Bin 3069: 218 of cap free
Amount of items: 2
Items: 
Size: 528407 Color: 0
Size: 471376 Color: 1

Bin 3070: 218 of cap free
Amount of items: 2
Items: 
Size: 589662 Color: 1
Size: 410121 Color: 0

Bin 3071: 218 of cap free
Amount of items: 2
Items: 
Size: 736753 Color: 1
Size: 263030 Color: 0

Bin 3072: 218 of cap free
Amount of items: 2
Items: 
Size: 783137 Color: 1
Size: 216646 Color: 0

Bin 3073: 219 of cap free
Amount of items: 2
Items: 
Size: 641520 Color: 1
Size: 358262 Color: 0

Bin 3074: 220 of cap free
Amount of items: 2
Items: 
Size: 560330 Color: 1
Size: 439451 Color: 0

Bin 3075: 220 of cap free
Amount of items: 2
Items: 
Size: 637910 Color: 0
Size: 361871 Color: 1

Bin 3076: 220 of cap free
Amount of items: 2
Items: 
Size: 689253 Color: 0
Size: 310528 Color: 1

Bin 3077: 221 of cap free
Amount of items: 2
Items: 
Size: 633731 Color: 1
Size: 366049 Color: 0

Bin 3078: 221 of cap free
Amount of items: 2
Items: 
Size: 589103 Color: 0
Size: 410677 Color: 1

Bin 3079: 221 of cap free
Amount of items: 2
Items: 
Size: 602358 Color: 1
Size: 397422 Color: 0

Bin 3080: 221 of cap free
Amount of items: 2
Items: 
Size: 687497 Color: 1
Size: 312283 Color: 0

Bin 3081: 221 of cap free
Amount of items: 2
Items: 
Size: 718675 Color: 0
Size: 281105 Color: 1

Bin 3082: 221 of cap free
Amount of items: 2
Items: 
Size: 721473 Color: 0
Size: 278307 Color: 1

Bin 3083: 222 of cap free
Amount of items: 2
Items: 
Size: 674084 Color: 0
Size: 325695 Color: 1

Bin 3084: 222 of cap free
Amount of items: 2
Items: 
Size: 700857 Color: 1
Size: 298922 Color: 0

Bin 3085: 222 of cap free
Amount of items: 2
Items: 
Size: 738889 Color: 0
Size: 260890 Color: 1

Bin 3086: 223 of cap free
Amount of items: 2
Items: 
Size: 692910 Color: 1
Size: 306868 Color: 0

Bin 3087: 223 of cap free
Amount of items: 2
Items: 
Size: 550001 Color: 1
Size: 449777 Color: 0

Bin 3088: 223 of cap free
Amount of items: 2
Items: 
Size: 557356 Color: 1
Size: 442422 Color: 0

Bin 3089: 223 of cap free
Amount of items: 2
Items: 
Size: 727208 Color: 0
Size: 272570 Color: 1

Bin 3090: 224 of cap free
Amount of items: 2
Items: 
Size: 756763 Color: 1
Size: 243014 Color: 0

Bin 3091: 224 of cap free
Amount of items: 2
Items: 
Size: 564452 Color: 1
Size: 435325 Color: 0

Bin 3092: 224 of cap free
Amount of items: 2
Items: 
Size: 724076 Color: 0
Size: 275701 Color: 1

Bin 3093: 224 of cap free
Amount of items: 2
Items: 
Size: 735740 Color: 1
Size: 264037 Color: 0

Bin 3094: 224 of cap free
Amount of items: 2
Items: 
Size: 775831 Color: 1
Size: 223946 Color: 0

Bin 3095: 225 of cap free
Amount of items: 2
Items: 
Size: 676392 Color: 0
Size: 323384 Color: 1

Bin 3096: 225 of cap free
Amount of items: 2
Items: 
Size: 699812 Color: 0
Size: 299964 Color: 1

Bin 3097: 225 of cap free
Amount of items: 2
Items: 
Size: 708485 Color: 0
Size: 291291 Color: 1

Bin 3098: 226 of cap free
Amount of items: 2
Items: 
Size: 507726 Color: 1
Size: 492049 Color: 0

Bin 3099: 226 of cap free
Amount of items: 2
Items: 
Size: 525491 Color: 1
Size: 474284 Color: 0

Bin 3100: 226 of cap free
Amount of items: 2
Items: 
Size: 571081 Color: 1
Size: 428694 Color: 0

Bin 3101: 226 of cap free
Amount of items: 2
Items: 
Size: 599435 Color: 0
Size: 400340 Color: 1

Bin 3102: 226 of cap free
Amount of items: 2
Items: 
Size: 672886 Color: 0
Size: 326889 Color: 1

Bin 3103: 226 of cap free
Amount of items: 2
Items: 
Size: 676324 Color: 1
Size: 323451 Color: 0

Bin 3104: 227 of cap free
Amount of items: 2
Items: 
Size: 625996 Color: 1
Size: 373778 Color: 0

Bin 3105: 227 of cap free
Amount of items: 2
Items: 
Size: 628153 Color: 0
Size: 371621 Color: 1

Bin 3106: 227 of cap free
Amount of items: 2
Items: 
Size: 749290 Color: 1
Size: 250484 Color: 0

Bin 3107: 228 of cap free
Amount of items: 2
Items: 
Size: 557513 Color: 0
Size: 442260 Color: 1

Bin 3108: 229 of cap free
Amount of items: 2
Items: 
Size: 582541 Color: 1
Size: 417231 Color: 0

Bin 3109: 229 of cap free
Amount of items: 2
Items: 
Size: 628470 Color: 1
Size: 371302 Color: 0

Bin 3110: 229 of cap free
Amount of items: 2
Items: 
Size: 651719 Color: 1
Size: 348053 Color: 0

Bin 3111: 229 of cap free
Amount of items: 2
Items: 
Size: 684794 Color: 1
Size: 314978 Color: 0

Bin 3112: 229 of cap free
Amount of items: 2
Items: 
Size: 712603 Color: 0
Size: 287169 Color: 1

Bin 3113: 230 of cap free
Amount of items: 2
Items: 
Size: 594088 Color: 1
Size: 405683 Color: 0

Bin 3114: 230 of cap free
Amount of items: 2
Items: 
Size: 679526 Color: 1
Size: 320245 Color: 0

Bin 3115: 230 of cap free
Amount of items: 2
Items: 
Size: 751984 Color: 0
Size: 247787 Color: 1

Bin 3116: 230 of cap free
Amount of items: 2
Items: 
Size: 783151 Color: 0
Size: 216620 Color: 1

Bin 3117: 230 of cap free
Amount of items: 2
Items: 
Size: 791753 Color: 0
Size: 208018 Color: 1

Bin 3118: 231 of cap free
Amount of items: 2
Items: 
Size: 561935 Color: 1
Size: 437835 Color: 0

Bin 3119: 231 of cap free
Amount of items: 2
Items: 
Size: 694787 Color: 1
Size: 304983 Color: 0

Bin 3120: 232 of cap free
Amount of items: 2
Items: 
Size: 555750 Color: 0
Size: 444019 Color: 1

Bin 3121: 232 of cap free
Amount of items: 2
Items: 
Size: 648179 Color: 0
Size: 351590 Color: 1

Bin 3122: 232 of cap free
Amount of items: 2
Items: 
Size: 729580 Color: 0
Size: 270189 Color: 1

Bin 3123: 234 of cap free
Amount of items: 2
Items: 
Size: 519672 Color: 1
Size: 480095 Color: 0

Bin 3124: 234 of cap free
Amount of items: 2
Items: 
Size: 594879 Color: 0
Size: 404888 Color: 1

Bin 3125: 235 of cap free
Amount of items: 2
Items: 
Size: 518372 Color: 0
Size: 481394 Color: 1

Bin 3126: 236 of cap free
Amount of items: 2
Items: 
Size: 772132 Color: 0
Size: 227633 Color: 1

Bin 3127: 236 of cap free
Amount of items: 2
Items: 
Size: 791437 Color: 1
Size: 208328 Color: 0

Bin 3128: 236 of cap free
Amount of items: 2
Items: 
Size: 506148 Color: 0
Size: 493617 Color: 1

Bin 3129: 236 of cap free
Amount of items: 2
Items: 
Size: 582888 Color: 0
Size: 416877 Color: 1

Bin 3130: 236 of cap free
Amount of items: 2
Items: 
Size: 648705 Color: 0
Size: 351060 Color: 1

Bin 3131: 236 of cap free
Amount of items: 2
Items: 
Size: 767951 Color: 1
Size: 231814 Color: 0

Bin 3132: 237 of cap free
Amount of items: 2
Items: 
Size: 501423 Color: 1
Size: 498341 Color: 0

Bin 3133: 237 of cap free
Amount of items: 2
Items: 
Size: 649860 Color: 1
Size: 349904 Color: 0

Bin 3134: 238 of cap free
Amount of items: 2
Items: 
Size: 698538 Color: 0
Size: 301225 Color: 1

Bin 3135: 238 of cap free
Amount of items: 2
Items: 
Size: 750600 Color: 0
Size: 249163 Color: 1

Bin 3136: 239 of cap free
Amount of items: 2
Items: 
Size: 609046 Color: 0
Size: 390716 Color: 1

Bin 3137: 239 of cap free
Amount of items: 2
Items: 
Size: 764867 Color: 0
Size: 234895 Color: 1

Bin 3138: 240 of cap free
Amount of items: 2
Items: 
Size: 773255 Color: 1
Size: 226506 Color: 0

Bin 3139: 241 of cap free
Amount of items: 2
Items: 
Size: 767279 Color: 1
Size: 232481 Color: 0

Bin 3140: 241 of cap free
Amount of items: 2
Items: 
Size: 595366 Color: 1
Size: 404394 Color: 0

Bin 3141: 241 of cap free
Amount of items: 2
Items: 
Size: 713201 Color: 0
Size: 286559 Color: 1

Bin 3142: 242 of cap free
Amount of items: 2
Items: 
Size: 622165 Color: 0
Size: 377594 Color: 1

Bin 3143: 242 of cap free
Amount of items: 2
Items: 
Size: 647864 Color: 0
Size: 351895 Color: 1

Bin 3144: 242 of cap free
Amount of items: 2
Items: 
Size: 786997 Color: 1
Size: 212762 Color: 0

Bin 3145: 243 of cap free
Amount of items: 2
Items: 
Size: 554587 Color: 1
Size: 445171 Color: 0

Bin 3146: 243 of cap free
Amount of items: 2
Items: 
Size: 614266 Color: 1
Size: 385492 Color: 0

Bin 3147: 243 of cap free
Amount of items: 2
Items: 
Size: 650359 Color: 1
Size: 349399 Color: 0

Bin 3148: 244 of cap free
Amount of items: 2
Items: 
Size: 720749 Color: 1
Size: 279008 Color: 0

Bin 3149: 245 of cap free
Amount of items: 2
Items: 
Size: 628813 Color: 0
Size: 370943 Color: 1

Bin 3150: 245 of cap free
Amount of items: 2
Items: 
Size: 644609 Color: 0
Size: 355147 Color: 1

Bin 3151: 245 of cap free
Amount of items: 2
Items: 
Size: 756430 Color: 1
Size: 243326 Color: 0

Bin 3152: 245 of cap free
Amount of items: 2
Items: 
Size: 783804 Color: 0
Size: 215952 Color: 1

Bin 3153: 246 of cap free
Amount of items: 2
Items: 
Size: 573802 Color: 1
Size: 425953 Color: 0

Bin 3154: 246 of cap free
Amount of items: 2
Items: 
Size: 738009 Color: 0
Size: 261746 Color: 1

Bin 3155: 246 of cap free
Amount of items: 2
Items: 
Size: 750187 Color: 1
Size: 249568 Color: 0

Bin 3156: 247 of cap free
Amount of items: 2
Items: 
Size: 580338 Color: 1
Size: 419416 Color: 0

Bin 3157: 247 of cap free
Amount of items: 2
Items: 
Size: 592556 Color: 0
Size: 407198 Color: 1

Bin 3158: 248 of cap free
Amount of items: 2
Items: 
Size: 629280 Color: 0
Size: 370473 Color: 1

Bin 3159: 248 of cap free
Amount of items: 2
Items: 
Size: 698065 Color: 1
Size: 301688 Color: 0

Bin 3160: 249 of cap free
Amount of items: 2
Items: 
Size: 765291 Color: 1
Size: 234461 Color: 0

Bin 3161: 249 of cap free
Amount of items: 2
Items: 
Size: 605746 Color: 1
Size: 394006 Color: 0

Bin 3162: 249 of cap free
Amount of items: 2
Items: 
Size: 586688 Color: 0
Size: 413064 Color: 1

Bin 3163: 249 of cap free
Amount of items: 2
Items: 
Size: 703620 Color: 0
Size: 296132 Color: 1

Bin 3164: 249 of cap free
Amount of items: 2
Items: 
Size: 712612 Color: 1
Size: 287140 Color: 0

Bin 3165: 249 of cap free
Amount of items: 2
Items: 
Size: 738390 Color: 1
Size: 261362 Color: 0

Bin 3166: 250 of cap free
Amount of items: 2
Items: 
Size: 568587 Color: 1
Size: 431164 Color: 0

Bin 3167: 250 of cap free
Amount of items: 2
Items: 
Size: 623047 Color: 1
Size: 376704 Color: 0

Bin 3168: 250 of cap free
Amount of items: 2
Items: 
Size: 746562 Color: 0
Size: 253189 Color: 1

Bin 3169: 250 of cap free
Amount of items: 2
Items: 
Size: 775763 Color: 0
Size: 223988 Color: 1

Bin 3170: 251 of cap free
Amount of items: 2
Items: 
Size: 587863 Color: 1
Size: 411887 Color: 0

Bin 3171: 251 of cap free
Amount of items: 2
Items: 
Size: 700440 Color: 1
Size: 299310 Color: 0

Bin 3172: 252 of cap free
Amount of items: 2
Items: 
Size: 699228 Color: 1
Size: 300521 Color: 0

Bin 3173: 253 of cap free
Amount of items: 2
Items: 
Size: 570792 Color: 1
Size: 428956 Color: 0

Bin 3174: 253 of cap free
Amount of items: 2
Items: 
Size: 548314 Color: 1
Size: 451434 Color: 0

Bin 3175: 253 of cap free
Amount of items: 2
Items: 
Size: 740724 Color: 0
Size: 259024 Color: 1

Bin 3176: 253 of cap free
Amount of items: 2
Items: 
Size: 785381 Color: 1
Size: 214367 Color: 0

Bin 3177: 253 of cap free
Amount of items: 2
Items: 
Size: 794997 Color: 1
Size: 204751 Color: 0

Bin 3178: 254 of cap free
Amount of items: 2
Items: 
Size: 654662 Color: 0
Size: 345085 Color: 1

Bin 3179: 255 of cap free
Amount of items: 2
Items: 
Size: 523916 Color: 1
Size: 475830 Color: 0

Bin 3180: 255 of cap free
Amount of items: 2
Items: 
Size: 566887 Color: 1
Size: 432859 Color: 0

Bin 3181: 255 of cap free
Amount of items: 2
Items: 
Size: 608715 Color: 1
Size: 391031 Color: 0

Bin 3182: 256 of cap free
Amount of items: 2
Items: 
Size: 516035 Color: 1
Size: 483710 Color: 0

Bin 3183: 257 of cap free
Amount of items: 2
Items: 
Size: 631806 Color: 1
Size: 367938 Color: 0

Bin 3184: 257 of cap free
Amount of items: 2
Items: 
Size: 501535 Color: 0
Size: 498209 Color: 1

Bin 3185: 257 of cap free
Amount of items: 2
Items: 
Size: 716112 Color: 1
Size: 283632 Color: 0

Bin 3186: 257 of cap free
Amount of items: 2
Items: 
Size: 799912 Color: 0
Size: 199832 Color: 1

Bin 3187: 258 of cap free
Amount of items: 2
Items: 
Size: 757967 Color: 1
Size: 241776 Color: 0

Bin 3188: 259 of cap free
Amount of items: 2
Items: 
Size: 672219 Color: 1
Size: 327523 Color: 0

Bin 3189: 259 of cap free
Amount of items: 2
Items: 
Size: 754906 Color: 0
Size: 244836 Color: 1

Bin 3190: 260 of cap free
Amount of items: 2
Items: 
Size: 729158 Color: 1
Size: 270583 Color: 0

Bin 3191: 260 of cap free
Amount of items: 2
Items: 
Size: 769407 Color: 0
Size: 230334 Color: 1

Bin 3192: 261 of cap free
Amount of items: 2
Items: 
Size: 681311 Color: 0
Size: 318429 Color: 1

Bin 3193: 262 of cap free
Amount of items: 2
Items: 
Size: 720145 Color: 0
Size: 279594 Color: 1

Bin 3194: 263 of cap free
Amount of items: 2
Items: 
Size: 503443 Color: 1
Size: 496295 Color: 0

Bin 3195: 263 of cap free
Amount of items: 2
Items: 
Size: 644262 Color: 1
Size: 355476 Color: 0

Bin 3196: 265 of cap free
Amount of items: 2
Items: 
Size: 587489 Color: 0
Size: 412247 Color: 1

Bin 3197: 265 of cap free
Amount of items: 2
Items: 
Size: 625732 Color: 0
Size: 374004 Color: 1

Bin 3198: 266 of cap free
Amount of items: 2
Items: 
Size: 713805 Color: 1
Size: 285930 Color: 0

Bin 3199: 266 of cap free
Amount of items: 2
Items: 
Size: 748640 Color: 0
Size: 251095 Color: 1

Bin 3200: 267 of cap free
Amount of items: 2
Items: 
Size: 718375 Color: 0
Size: 281359 Color: 1

Bin 3201: 268 of cap free
Amount of items: 2
Items: 
Size: 636998 Color: 0
Size: 362735 Color: 1

Bin 3202: 268 of cap free
Amount of items: 2
Items: 
Size: 554588 Color: 0
Size: 445145 Color: 1

Bin 3203: 268 of cap free
Amount of items: 2
Items: 
Size: 598333 Color: 0
Size: 401400 Color: 1

Bin 3204: 269 of cap free
Amount of items: 2
Items: 
Size: 506892 Color: 1
Size: 492840 Color: 0

Bin 3205: 269 of cap free
Amount of items: 2
Items: 
Size: 538962 Color: 1
Size: 460770 Color: 0

Bin 3206: 269 of cap free
Amount of items: 2
Items: 
Size: 613730 Color: 1
Size: 386002 Color: 0

Bin 3207: 269 of cap free
Amount of items: 2
Items: 
Size: 720742 Color: 1
Size: 278990 Color: 0

Bin 3208: 271 of cap free
Amount of items: 2
Items: 
Size: 521781 Color: 0
Size: 477949 Color: 1

Bin 3209: 271 of cap free
Amount of items: 2
Items: 
Size: 523899 Color: 0
Size: 475831 Color: 1

Bin 3210: 271 of cap free
Amount of items: 2
Items: 
Size: 626476 Color: 0
Size: 373254 Color: 1

Bin 3211: 271 of cap free
Amount of items: 2
Items: 
Size: 765697 Color: 1
Size: 234033 Color: 0

Bin 3212: 271 of cap free
Amount of items: 2
Items: 
Size: 768653 Color: 0
Size: 231077 Color: 1

Bin 3213: 272 of cap free
Amount of items: 2
Items: 
Size: 618905 Color: 0
Size: 380824 Color: 1

Bin 3214: 272 of cap free
Amount of items: 2
Items: 
Size: 647381 Color: 1
Size: 352348 Color: 0

Bin 3215: 272 of cap free
Amount of items: 2
Items: 
Size: 718892 Color: 1
Size: 280837 Color: 0

Bin 3216: 272 of cap free
Amount of items: 2
Items: 
Size: 781704 Color: 0
Size: 218025 Color: 1

Bin 3217: 273 of cap free
Amount of items: 2
Items: 
Size: 612238 Color: 0
Size: 387490 Color: 1

Bin 3218: 273 of cap free
Amount of items: 2
Items: 
Size: 742001 Color: 1
Size: 257727 Color: 0

Bin 3219: 273 of cap free
Amount of items: 2
Items: 
Size: 762783 Color: 1
Size: 236945 Color: 0

Bin 3220: 274 of cap free
Amount of items: 2
Items: 
Size: 729907 Color: 0
Size: 269820 Color: 1

Bin 3221: 274 of cap free
Amount of items: 2
Items: 
Size: 510344 Color: 0
Size: 489383 Color: 1

Bin 3222: 274 of cap free
Amount of items: 2
Items: 
Size: 535606 Color: 1
Size: 464121 Color: 0

Bin 3223: 274 of cap free
Amount of items: 2
Items: 
Size: 544756 Color: 0
Size: 454971 Color: 1

Bin 3224: 276 of cap free
Amount of items: 2
Items: 
Size: 649486 Color: 0
Size: 350239 Color: 1

Bin 3225: 276 of cap free
Amount of items: 2
Items: 
Size: 791723 Color: 0
Size: 208002 Color: 1

Bin 3226: 276 of cap free
Amount of items: 2
Items: 
Size: 648025 Color: 1
Size: 351700 Color: 0

Bin 3227: 276 of cap free
Amount of items: 2
Items: 
Size: 683325 Color: 1
Size: 316400 Color: 0

Bin 3228: 276 of cap free
Amount of items: 2
Items: 
Size: 743975 Color: 1
Size: 255750 Color: 0

Bin 3229: 276 of cap free
Amount of items: 2
Items: 
Size: 782496 Color: 1
Size: 217229 Color: 0

Bin 3230: 277 of cap free
Amount of items: 2
Items: 
Size: 745303 Color: 1
Size: 254421 Color: 0

Bin 3231: 277 of cap free
Amount of items: 2
Items: 
Size: 540758 Color: 1
Size: 458966 Color: 0

Bin 3232: 277 of cap free
Amount of items: 2
Items: 
Size: 551264 Color: 1
Size: 448460 Color: 0

Bin 3233: 277 of cap free
Amount of items: 2
Items: 
Size: 574536 Color: 1
Size: 425188 Color: 0

Bin 3234: 277 of cap free
Amount of items: 2
Items: 
Size: 617861 Color: 1
Size: 381863 Color: 0

Bin 3235: 277 of cap free
Amount of items: 2
Items: 
Size: 638650 Color: 0
Size: 361074 Color: 1

Bin 3236: 277 of cap free
Amount of items: 2
Items: 
Size: 759917 Color: 0
Size: 239807 Color: 1

Bin 3237: 279 of cap free
Amount of items: 2
Items: 
Size: 614942 Color: 0
Size: 384780 Color: 1

Bin 3238: 279 of cap free
Amount of items: 2
Items: 
Size: 643648 Color: 0
Size: 356074 Color: 1

Bin 3239: 280 of cap free
Amount of items: 2
Items: 
Size: 668845 Color: 1
Size: 330876 Color: 0

Bin 3240: 281 of cap free
Amount of items: 2
Items: 
Size: 688536 Color: 1
Size: 311184 Color: 0

Bin 3241: 281 of cap free
Amount of items: 2
Items: 
Size: 694338 Color: 1
Size: 305382 Color: 0

Bin 3242: 281 of cap free
Amount of items: 2
Items: 
Size: 722366 Color: 0
Size: 277354 Color: 1

Bin 3243: 282 of cap free
Amount of items: 2
Items: 
Size: 645760 Color: 1
Size: 353959 Color: 0

Bin 3244: 283 of cap free
Amount of items: 2
Items: 
Size: 615467 Color: 0
Size: 384251 Color: 1

Bin 3245: 283 of cap free
Amount of items: 2
Items: 
Size: 656676 Color: 1
Size: 343042 Color: 0

Bin 3246: 284 of cap free
Amount of items: 2
Items: 
Size: 512255 Color: 1
Size: 487462 Color: 0

Bin 3247: 284 of cap free
Amount of items: 2
Items: 
Size: 630239 Color: 0
Size: 369478 Color: 1

Bin 3248: 284 of cap free
Amount of items: 2
Items: 
Size: 694711 Color: 0
Size: 305006 Color: 1

Bin 3249: 285 of cap free
Amount of items: 2
Items: 
Size: 737346 Color: 0
Size: 262370 Color: 1

Bin 3250: 285 of cap free
Amount of items: 2
Items: 
Size: 527487 Color: 1
Size: 472229 Color: 0

Bin 3251: 285 of cap free
Amount of items: 2
Items: 
Size: 770294 Color: 0
Size: 229422 Color: 1

Bin 3252: 286 of cap free
Amount of items: 2
Items: 
Size: 508439 Color: 1
Size: 491276 Color: 0

Bin 3253: 286 of cap free
Amount of items: 2
Items: 
Size: 645591 Color: 0
Size: 354124 Color: 1

Bin 3254: 287 of cap free
Amount of items: 2
Items: 
Size: 620967 Color: 1
Size: 378747 Color: 0

Bin 3255: 287 of cap free
Amount of items: 2
Items: 
Size: 716091 Color: 1
Size: 283623 Color: 0

Bin 3256: 288 of cap free
Amount of items: 2
Items: 
Size: 571121 Color: 0
Size: 428592 Color: 1

Bin 3257: 288 of cap free
Amount of items: 2
Items: 
Size: 549648 Color: 1
Size: 450065 Color: 0

Bin 3258: 288 of cap free
Amount of items: 2
Items: 
Size: 624905 Color: 1
Size: 374808 Color: 0

Bin 3259: 288 of cap free
Amount of items: 2
Items: 
Size: 688181 Color: 0
Size: 311532 Color: 1

Bin 3260: 289 of cap free
Amount of items: 2
Items: 
Size: 526996 Color: 1
Size: 472716 Color: 0

Bin 3261: 289 of cap free
Amount of items: 2
Items: 
Size: 648174 Color: 0
Size: 351538 Color: 1

Bin 3262: 289 of cap free
Amount of items: 2
Items: 
Size: 662952 Color: 1
Size: 336760 Color: 0

Bin 3263: 289 of cap free
Amount of items: 2
Items: 
Size: 685527 Color: 0
Size: 314185 Color: 1

Bin 3264: 290 of cap free
Amount of items: 2
Items: 
Size: 597556 Color: 1
Size: 402155 Color: 0

Bin 3265: 290 of cap free
Amount of items: 2
Items: 
Size: 798997 Color: 0
Size: 200714 Color: 1

Bin 3266: 291 of cap free
Amount of items: 2
Items: 
Size: 749797 Color: 1
Size: 249913 Color: 0

Bin 3267: 292 of cap free
Amount of items: 2
Items: 
Size: 614354 Color: 0
Size: 385355 Color: 1

Bin 3268: 292 of cap free
Amount of items: 2
Items: 
Size: 545853 Color: 1
Size: 453856 Color: 0

Bin 3269: 292 of cap free
Amount of items: 2
Items: 
Size: 593021 Color: 0
Size: 406688 Color: 1

Bin 3270: 292 of cap free
Amount of items: 2
Items: 
Size: 706494 Color: 1
Size: 293215 Color: 0

Bin 3271: 293 of cap free
Amount of items: 2
Items: 
Size: 520060 Color: 1
Size: 479648 Color: 0

Bin 3272: 293 of cap free
Amount of items: 2
Items: 
Size: 609670 Color: 0
Size: 390038 Color: 1

Bin 3273: 294 of cap free
Amount of items: 2
Items: 
Size: 588913 Color: 1
Size: 410794 Color: 0

Bin 3274: 294 of cap free
Amount of items: 2
Items: 
Size: 691773 Color: 1
Size: 307934 Color: 0

Bin 3275: 295 of cap free
Amount of items: 2
Items: 
Size: 532541 Color: 0
Size: 467165 Color: 1

Bin 3276: 295 of cap free
Amount of items: 2
Items: 
Size: 561757 Color: 0
Size: 437949 Color: 1

Bin 3277: 295 of cap free
Amount of items: 2
Items: 
Size: 596887 Color: 0
Size: 402819 Color: 1

Bin 3278: 295 of cap free
Amount of items: 2
Items: 
Size: 611741 Color: 1
Size: 387965 Color: 0

Bin 3279: 295 of cap free
Amount of items: 2
Items: 
Size: 623521 Color: 0
Size: 376185 Color: 1

Bin 3280: 296 of cap free
Amount of items: 2
Items: 
Size: 507326 Color: 0
Size: 492379 Color: 1

Bin 3281: 296 of cap free
Amount of items: 2
Items: 
Size: 621038 Color: 0
Size: 378667 Color: 1

Bin 3282: 297 of cap free
Amount of items: 2
Items: 
Size: 529727 Color: 1
Size: 469977 Color: 0

Bin 3283: 297 of cap free
Amount of items: 2
Items: 
Size: 745352 Color: 0
Size: 254352 Color: 1

Bin 3284: 298 of cap free
Amount of items: 2
Items: 
Size: 676697 Color: 0
Size: 323006 Color: 1

Bin 3285: 298 of cap free
Amount of items: 2
Items: 
Size: 733780 Color: 1
Size: 265923 Color: 0

Bin 3286: 299 of cap free
Amount of items: 2
Items: 
Size: 507324 Color: 1
Size: 492378 Color: 0

Bin 3287: 299 of cap free
Amount of items: 2
Items: 
Size: 585646 Color: 0
Size: 414056 Color: 1

Bin 3288: 299 of cap free
Amount of items: 2
Items: 
Size: 765107 Color: 0
Size: 234595 Color: 1

Bin 3289: 300 of cap free
Amount of items: 2
Items: 
Size: 604978 Color: 1
Size: 394723 Color: 0

Bin 3290: 300 of cap free
Amount of items: 2
Items: 
Size: 511341 Color: 0
Size: 488360 Color: 1

Bin 3291: 301 of cap free
Amount of items: 2
Items: 
Size: 590418 Color: 1
Size: 409282 Color: 0

Bin 3292: 301 of cap free
Amount of items: 2
Items: 
Size: 786687 Color: 1
Size: 213013 Color: 0

Bin 3293: 301 of cap free
Amount of items: 2
Items: 
Size: 506789 Color: 0
Size: 492911 Color: 1

Bin 3294: 301 of cap free
Amount of items: 2
Items: 
Size: 533805 Color: 0
Size: 465895 Color: 1

Bin 3295: 301 of cap free
Amount of items: 2
Items: 
Size: 724651 Color: 1
Size: 275049 Color: 0

Bin 3296: 302 of cap free
Amount of items: 2
Items: 
Size: 725497 Color: 1
Size: 274202 Color: 0

Bin 3297: 303 of cap free
Amount of items: 2
Items: 
Size: 653032 Color: 1
Size: 346666 Color: 0

Bin 3298: 304 of cap free
Amount of items: 2
Items: 
Size: 505385 Color: 1
Size: 494312 Color: 0

Bin 3299: 304 of cap free
Amount of items: 2
Items: 
Size: 695832 Color: 1
Size: 303865 Color: 0

Bin 3300: 305 of cap free
Amount of items: 2
Items: 
Size: 520509 Color: 0
Size: 479187 Color: 1

Bin 3301: 305 of cap free
Amount of items: 2
Items: 
Size: 639594 Color: 1
Size: 360102 Color: 0

Bin 3302: 305 of cap free
Amount of items: 2
Items: 
Size: 615760 Color: 1
Size: 383936 Color: 0

Bin 3303: 305 of cap free
Amount of items: 2
Items: 
Size: 701408 Color: 0
Size: 298288 Color: 1

Bin 3304: 306 of cap free
Amount of items: 2
Items: 
Size: 517257 Color: 1
Size: 482438 Color: 0

Bin 3305: 306 of cap free
Amount of items: 2
Items: 
Size: 616319 Color: 1
Size: 383376 Color: 0

Bin 3306: 306 of cap free
Amount of items: 2
Items: 
Size: 648003 Color: 1
Size: 351692 Color: 0

Bin 3307: 307 of cap free
Amount of items: 2
Items: 
Size: 545473 Color: 0
Size: 454221 Color: 1

Bin 3308: 307 of cap free
Amount of items: 2
Items: 
Size: 550533 Color: 1
Size: 449161 Color: 0

Bin 3309: 307 of cap free
Amount of items: 2
Items: 
Size: 561511 Color: 1
Size: 438183 Color: 0

Bin 3310: 307 of cap free
Amount of items: 2
Items: 
Size: 677060 Color: 1
Size: 322634 Color: 0

Bin 3311: 307 of cap free
Amount of items: 2
Items: 
Size: 707052 Color: 0
Size: 292642 Color: 1

Bin 3312: 307 of cap free
Amount of items: 2
Items: 
Size: 758926 Color: 0
Size: 240768 Color: 1

Bin 3313: 308 of cap free
Amount of items: 2
Items: 
Size: 734501 Color: 0
Size: 265192 Color: 1

Bin 3314: 308 of cap free
Amount of items: 2
Items: 
Size: 527262 Color: 0
Size: 472431 Color: 1

Bin 3315: 308 of cap free
Amount of items: 2
Items: 
Size: 586662 Color: 1
Size: 413031 Color: 0

Bin 3316: 308 of cap free
Amount of items: 2
Items: 
Size: 593472 Color: 1
Size: 406221 Color: 0

Bin 3317: 308 of cap free
Amount of items: 2
Items: 
Size: 633266 Color: 1
Size: 366427 Color: 0

Bin 3318: 308 of cap free
Amount of items: 2
Items: 
Size: 683027 Color: 0
Size: 316666 Color: 1

Bin 3319: 309 of cap free
Amount of items: 2
Items: 
Size: 633884 Color: 0
Size: 365808 Color: 1

Bin 3320: 310 of cap free
Amount of items: 2
Items: 
Size: 622795 Color: 0
Size: 376896 Color: 1

Bin 3321: 310 of cap free
Amount of items: 2
Items: 
Size: 609524 Color: 1
Size: 390167 Color: 0

Bin 3322: 310 of cap free
Amount of items: 2
Items: 
Size: 738878 Color: 0
Size: 260813 Color: 1

Bin 3323: 311 of cap free
Amount of items: 2
Items: 
Size: 715832 Color: 0
Size: 283858 Color: 1

Bin 3324: 311 of cap free
Amount of items: 2
Items: 
Size: 508259 Color: 0
Size: 491431 Color: 1

Bin 3325: 311 of cap free
Amount of items: 2
Items: 
Size: 596287 Color: 1
Size: 403403 Color: 0

Bin 3326: 311 of cap free
Amount of items: 2
Items: 
Size: 718858 Color: 1
Size: 280832 Color: 0

Bin 3327: 311 of cap free
Amount of items: 2
Items: 
Size: 748952 Color: 1
Size: 250738 Color: 0

Bin 3328: 312 of cap free
Amount of items: 2
Items: 
Size: 737647 Color: 0
Size: 262042 Color: 1

Bin 3329: 312 of cap free
Amount of items: 2
Items: 
Size: 675139 Color: 1
Size: 324550 Color: 0

Bin 3330: 313 of cap free
Amount of items: 2
Items: 
Size: 559771 Color: 0
Size: 439917 Color: 1

Bin 3331: 313 of cap free
Amount of items: 2
Items: 
Size: 670776 Color: 1
Size: 328912 Color: 0

Bin 3332: 314 of cap free
Amount of items: 2
Items: 
Size: 708461 Color: 0
Size: 291226 Color: 1

Bin 3333: 315 of cap free
Amount of items: 2
Items: 
Size: 515223 Color: 1
Size: 484463 Color: 0

Bin 3334: 315 of cap free
Amount of items: 2
Items: 
Size: 571915 Color: 0
Size: 427771 Color: 1

Bin 3335: 316 of cap free
Amount of items: 2
Items: 
Size: 599021 Color: 1
Size: 400664 Color: 0

Bin 3336: 316 of cap free
Amount of items: 2
Items: 
Size: 613787 Color: 0
Size: 385898 Color: 1

Bin 3337: 316 of cap free
Amount of items: 2
Items: 
Size: 697537 Color: 0
Size: 302148 Color: 1

Bin 3338: 316 of cap free
Amount of items: 2
Items: 
Size: 726891 Color: 1
Size: 272794 Color: 0

Bin 3339: 317 of cap free
Amount of items: 2
Items: 
Size: 654537 Color: 1
Size: 345147 Color: 0

Bin 3340: 318 of cap free
Amount of items: 2
Items: 
Size: 548635 Color: 1
Size: 451048 Color: 0

Bin 3341: 319 of cap free
Amount of items: 2
Items: 
Size: 530758 Color: 0
Size: 468924 Color: 1

Bin 3342: 319 of cap free
Amount of items: 2
Items: 
Size: 740698 Color: 0
Size: 258984 Color: 1

Bin 3343: 320 of cap free
Amount of items: 2
Items: 
Size: 548469 Color: 0
Size: 451212 Color: 1

Bin 3344: 321 of cap free
Amount of items: 2
Items: 
Size: 659170 Color: 1
Size: 340510 Color: 0

Bin 3345: 321 of cap free
Amount of items: 2
Items: 
Size: 689030 Color: 1
Size: 310650 Color: 0

Bin 3346: 321 of cap free
Amount of items: 2
Items: 
Size: 741841 Color: 0
Size: 257839 Color: 1

Bin 3347: 322 of cap free
Amount of items: 2
Items: 
Size: 570131 Color: 1
Size: 429548 Color: 0

Bin 3348: 322 of cap free
Amount of items: 2
Items: 
Size: 618532 Color: 1
Size: 381147 Color: 0

Bin 3349: 323 of cap free
Amount of items: 2
Items: 
Size: 710434 Color: 0
Size: 289244 Color: 1

Bin 3350: 324 of cap free
Amount of items: 2
Items: 
Size: 755884 Color: 0
Size: 243793 Color: 1

Bin 3351: 324 of cap free
Amount of items: 2
Items: 
Size: 620188 Color: 1
Size: 379489 Color: 0

Bin 3352: 325 of cap free
Amount of items: 2
Items: 
Size: 757066 Color: 1
Size: 242610 Color: 0

Bin 3353: 326 of cap free
Amount of items: 2
Items: 
Size: 658008 Color: 0
Size: 341667 Color: 1

Bin 3354: 326 of cap free
Amount of items: 2
Items: 
Size: 671382 Color: 0
Size: 328293 Color: 1

Bin 3355: 326 of cap free
Amount of items: 2
Items: 
Size: 747301 Color: 0
Size: 252374 Color: 1

Bin 3356: 327 of cap free
Amount of items: 3
Items: 
Size: 441766 Color: 1
Size: 402074 Color: 1
Size: 155834 Color: 0

Bin 3357: 327 of cap free
Amount of items: 2
Items: 
Size: 537358 Color: 0
Size: 462316 Color: 1

Bin 3358: 327 of cap free
Amount of items: 2
Items: 
Size: 600732 Color: 1
Size: 398942 Color: 0

Bin 3359: 327 of cap free
Amount of items: 2
Items: 
Size: 756523 Color: 0
Size: 243151 Color: 1

Bin 3360: 328 of cap free
Amount of items: 2
Items: 
Size: 789098 Color: 0
Size: 210575 Color: 1

Bin 3361: 328 of cap free
Amount of items: 2
Items: 
Size: 664363 Color: 0
Size: 335310 Color: 1

Bin 3362: 329 of cap free
Amount of items: 2
Items: 
Size: 532820 Color: 1
Size: 466852 Color: 0

Bin 3363: 329 of cap free
Amount of items: 2
Items: 
Size: 600655 Color: 0
Size: 399017 Color: 1

Bin 3364: 329 of cap free
Amount of items: 2
Items: 
Size: 608080 Color: 1
Size: 391592 Color: 0

Bin 3365: 329 of cap free
Amount of items: 2
Items: 
Size: 700949 Color: 0
Size: 298723 Color: 1

Bin 3366: 330 of cap free
Amount of items: 2
Items: 
Size: 792734 Color: 0
Size: 206937 Color: 1

Bin 3367: 331 of cap free
Amount of items: 2
Items: 
Size: 541221 Color: 0
Size: 458449 Color: 1

Bin 3368: 331 of cap free
Amount of items: 2
Items: 
Size: 797253 Color: 0
Size: 202417 Color: 1

Bin 3369: 332 of cap free
Amount of items: 2
Items: 
Size: 517662 Color: 1
Size: 482007 Color: 0

Bin 3370: 332 of cap free
Amount of items: 2
Items: 
Size: 716663 Color: 1
Size: 283006 Color: 0

Bin 3371: 333 of cap free
Amount of items: 2
Items: 
Size: 635961 Color: 1
Size: 363707 Color: 0

Bin 3372: 333 of cap free
Amount of items: 2
Items: 
Size: 510767 Color: 1
Size: 488901 Color: 0

Bin 3373: 333 of cap free
Amount of items: 2
Items: 
Size: 776974 Color: 0
Size: 222694 Color: 1

Bin 3374: 333 of cap free
Amount of items: 2
Items: 
Size: 639916 Color: 1
Size: 359752 Color: 0

Bin 3375: 333 of cap free
Amount of items: 2
Items: 
Size: 754555 Color: 0
Size: 245113 Color: 1

Bin 3376: 333 of cap free
Amount of items: 2
Items: 
Size: 759285 Color: 1
Size: 240383 Color: 0

Bin 3377: 333 of cap free
Amount of items: 2
Items: 
Size: 764717 Color: 1
Size: 234951 Color: 0

Bin 3378: 334 of cap free
Amount of items: 2
Items: 
Size: 752753 Color: 1
Size: 246914 Color: 0

Bin 3379: 334 of cap free
Amount of items: 2
Items: 
Size: 663918 Color: 1
Size: 335749 Color: 0

Bin 3380: 335 of cap free
Amount of items: 2
Items: 
Size: 610534 Color: 1
Size: 389132 Color: 0

Bin 3381: 335 of cap free
Amount of items: 2
Items: 
Size: 683912 Color: 1
Size: 315754 Color: 0

Bin 3382: 335 of cap free
Amount of items: 2
Items: 
Size: 697174 Color: 0
Size: 302492 Color: 1

Bin 3383: 335 of cap free
Amount of items: 2
Items: 
Size: 728647 Color: 0
Size: 271019 Color: 1

Bin 3384: 338 of cap free
Amount of items: 2
Items: 
Size: 538098 Color: 0
Size: 461565 Color: 1

Bin 3385: 338 of cap free
Amount of items: 2
Items: 
Size: 677779 Color: 1
Size: 321884 Color: 0

Bin 3386: 338 of cap free
Amount of items: 2
Items: 
Size: 746136 Color: 0
Size: 253527 Color: 1

Bin 3387: 339 of cap free
Amount of items: 2
Items: 
Size: 607232 Color: 0
Size: 392430 Color: 1

Bin 3388: 339 of cap free
Amount of items: 2
Items: 
Size: 707930 Color: 1
Size: 291732 Color: 0

Bin 3389: 340 of cap free
Amount of items: 2
Items: 
Size: 794653 Color: 0
Size: 205008 Color: 1

Bin 3390: 340 of cap free
Amount of items: 2
Items: 
Size: 581064 Color: 0
Size: 418597 Color: 1

Bin 3391: 340 of cap free
Amount of items: 2
Items: 
Size: 592560 Color: 1
Size: 407101 Color: 0

Bin 3392: 340 of cap free
Amount of items: 2
Items: 
Size: 699526 Color: 1
Size: 300135 Color: 0

Bin 3393: 341 of cap free
Amount of items: 3
Items: 
Size: 395928 Color: 0
Size: 394537 Color: 0
Size: 209195 Color: 1

Bin 3394: 342 of cap free
Amount of items: 2
Items: 
Size: 797676 Color: 0
Size: 201983 Color: 1

Bin 3395: 342 of cap free
Amount of items: 2
Items: 
Size: 597194 Color: 1
Size: 402465 Color: 0

Bin 3396: 342 of cap free
Amount of items: 2
Items: 
Size: 607179 Color: 1
Size: 392480 Color: 0

Bin 3397: 342 of cap free
Amount of items: 2
Items: 
Size: 650684 Color: 0
Size: 348975 Color: 1

Bin 3398: 343 of cap free
Amount of items: 2
Items: 
Size: 503883 Color: 1
Size: 495775 Color: 0

Bin 3399: 343 of cap free
Amount of items: 2
Items: 
Size: 578463 Color: 1
Size: 421195 Color: 0

Bin 3400: 344 of cap free
Amount of items: 2
Items: 
Size: 512763 Color: 0
Size: 486894 Color: 1

Bin 3401: 344 of cap free
Amount of items: 2
Items: 
Size: 728365 Color: 1
Size: 271292 Color: 0

Bin 3402: 344 of cap free
Amount of items: 2
Items: 
Size: 768238 Color: 0
Size: 231419 Color: 1

Bin 3403: 345 of cap free
Amount of items: 2
Items: 
Size: 673241 Color: 0
Size: 326415 Color: 1

Bin 3404: 346 of cap free
Amount of items: 2
Items: 
Size: 503364 Color: 1
Size: 496291 Color: 0

Bin 3405: 346 of cap free
Amount of items: 2
Items: 
Size: 751604 Color: 1
Size: 248051 Color: 0

Bin 3406: 347 of cap free
Amount of items: 2
Items: 
Size: 608604 Color: 0
Size: 391050 Color: 1

Bin 3407: 347 of cap free
Amount of items: 2
Items: 
Size: 755643 Color: 1
Size: 244011 Color: 0

Bin 3408: 348 of cap free
Amount of items: 2
Items: 
Size: 736637 Color: 0
Size: 263016 Color: 1

Bin 3409: 348 of cap free
Amount of items: 2
Items: 
Size: 691246 Color: 0
Size: 308407 Color: 1

Bin 3410: 349 of cap free
Amount of items: 2
Items: 
Size: 584371 Color: 0
Size: 415281 Color: 1

Bin 3411: 349 of cap free
Amount of items: 2
Items: 
Size: 538272 Color: 1
Size: 461380 Color: 0

Bin 3412: 349 of cap free
Amount of items: 2
Items: 
Size: 596250 Color: 1
Size: 403402 Color: 0

Bin 3413: 349 of cap free
Amount of items: 2
Items: 
Size: 746139 Color: 1
Size: 253513 Color: 0

Bin 3414: 351 of cap free
Amount of items: 2
Items: 
Size: 552313 Color: 0
Size: 447337 Color: 1

Bin 3415: 351 of cap free
Amount of items: 2
Items: 
Size: 598275 Color: 0
Size: 401375 Color: 1

Bin 3416: 352 of cap free
Amount of items: 2
Items: 
Size: 638212 Color: 1
Size: 361437 Color: 0

Bin 3417: 353 of cap free
Amount of items: 2
Items: 
Size: 715792 Color: 0
Size: 283856 Color: 1

Bin 3418: 353 of cap free
Amount of items: 2
Items: 
Size: 718301 Color: 1
Size: 281347 Color: 0

Bin 3419: 353 of cap free
Amount of items: 2
Items: 
Size: 508242 Color: 0
Size: 491406 Color: 1

Bin 3420: 353 of cap free
Amount of items: 2
Items: 
Size: 633608 Color: 1
Size: 366040 Color: 0

Bin 3421: 353 of cap free
Amount of items: 2
Items: 
Size: 709247 Color: 0
Size: 290401 Color: 1

Bin 3422: 353 of cap free
Amount of items: 2
Items: 
Size: 733731 Color: 1
Size: 265917 Color: 0

Bin 3423: 354 of cap free
Amount of items: 2
Items: 
Size: 619260 Color: 1
Size: 380387 Color: 0

Bin 3424: 354 of cap free
Amount of items: 2
Items: 
Size: 634334 Color: 0
Size: 365313 Color: 1

Bin 3425: 354 of cap free
Amount of items: 2
Items: 
Size: 640710 Color: 0
Size: 358937 Color: 1

Bin 3426: 355 of cap free
Amount of items: 2
Items: 
Size: 501485 Color: 0
Size: 498161 Color: 1

Bin 3427: 355 of cap free
Amount of items: 2
Items: 
Size: 591010 Color: 0
Size: 408636 Color: 1

Bin 3428: 355 of cap free
Amount of items: 2
Items: 
Size: 716478 Color: 0
Size: 283168 Color: 1

Bin 3429: 356 of cap free
Amount of items: 2
Items: 
Size: 566270 Color: 1
Size: 433375 Color: 0

Bin 3430: 356 of cap free
Amount of items: 2
Items: 
Size: 505382 Color: 1
Size: 494263 Color: 0

Bin 3431: 356 of cap free
Amount of items: 2
Items: 
Size: 733275 Color: 0
Size: 266370 Color: 1

Bin 3432: 357 of cap free
Amount of items: 2
Items: 
Size: 783706 Color: 1
Size: 215938 Color: 0

Bin 3433: 358 of cap free
Amount of items: 2
Items: 
Size: 711610 Color: 1
Size: 288033 Color: 0

Bin 3434: 359 of cap free
Amount of items: 2
Items: 
Size: 649785 Color: 1
Size: 349857 Color: 0

Bin 3435: 359 of cap free
Amount of items: 2
Items: 
Size: 733267 Color: 1
Size: 266375 Color: 0

Bin 3436: 359 of cap free
Amount of items: 2
Items: 
Size: 795966 Color: 0
Size: 203676 Color: 1

Bin 3437: 360 of cap free
Amount of items: 2
Items: 
Size: 622944 Color: 1
Size: 376697 Color: 0

Bin 3438: 360 of cap free
Amount of items: 2
Items: 
Size: 676309 Color: 0
Size: 323332 Color: 1

Bin 3439: 360 of cap free
Amount of items: 2
Items: 
Size: 781673 Color: 0
Size: 217968 Color: 1

Bin 3440: 361 of cap free
Amount of items: 2
Items: 
Size: 518273 Color: 0
Size: 481367 Color: 1

Bin 3441: 361 of cap free
Amount of items: 2
Items: 
Size: 529405 Color: 0
Size: 470235 Color: 1

Bin 3442: 362 of cap free
Amount of items: 2
Items: 
Size: 770983 Color: 1
Size: 228656 Color: 0

Bin 3443: 363 of cap free
Amount of items: 2
Items: 
Size: 568019 Color: 0
Size: 431619 Color: 1

Bin 3444: 364 of cap free
Amount of items: 2
Items: 
Size: 553525 Color: 1
Size: 446112 Color: 0

Bin 3445: 364 of cap free
Amount of items: 2
Items: 
Size: 609509 Color: 1
Size: 390128 Color: 0

Bin 3446: 365 of cap free
Amount of items: 2
Items: 
Size: 538923 Color: 1
Size: 460713 Color: 0

Bin 3447: 366 of cap free
Amount of items: 2
Items: 
Size: 705381 Color: 0
Size: 294254 Color: 1

Bin 3448: 366 of cap free
Amount of items: 2
Items: 
Size: 793475 Color: 1
Size: 206160 Color: 0

Bin 3449: 367 of cap free
Amount of items: 2
Items: 
Size: 702656 Color: 1
Size: 296978 Color: 0

Bin 3450: 368 of cap free
Amount of items: 2
Items: 
Size: 601821 Color: 0
Size: 397812 Color: 1

Bin 3451: 368 of cap free
Amount of items: 2
Items: 
Size: 696420 Color: 1
Size: 303213 Color: 0

Bin 3452: 369 of cap free
Amount of items: 2
Items: 
Size: 763754 Color: 1
Size: 235878 Color: 0

Bin 3453: 370 of cap free
Amount of items: 2
Items: 
Size: 531277 Color: 0
Size: 468354 Color: 1

Bin 3454: 372 of cap free
Amount of items: 2
Items: 
Size: 674643 Color: 0
Size: 324986 Color: 1

Bin 3455: 373 of cap free
Amount of items: 2
Items: 
Size: 596236 Color: 1
Size: 403392 Color: 0

Bin 3456: 374 of cap free
Amount of items: 2
Items: 
Size: 739570 Color: 1
Size: 260057 Color: 0

Bin 3457: 374 of cap free
Amount of items: 2
Items: 
Size: 672152 Color: 1
Size: 327475 Color: 0

Bin 3458: 375 of cap free
Amount of items: 2
Items: 
Size: 593460 Color: 1
Size: 406166 Color: 0

Bin 3459: 375 of cap free
Amount of items: 2
Items: 
Size: 685048 Color: 0
Size: 314578 Color: 1

Bin 3460: 375 of cap free
Amount of items: 2
Items: 
Size: 689655 Color: 1
Size: 309971 Color: 0

Bin 3461: 375 of cap free
Amount of items: 2
Items: 
Size: 743300 Color: 1
Size: 256326 Color: 0

Bin 3462: 376 of cap free
Amount of items: 2
Items: 
Size: 786777 Color: 0
Size: 212848 Color: 1

Bin 3463: 378 of cap free
Amount of items: 2
Items: 
Size: 522984 Color: 0
Size: 476639 Color: 1

Bin 3464: 378 of cap free
Amount of items: 2
Items: 
Size: 633817 Color: 0
Size: 365806 Color: 1

Bin 3465: 379 of cap free
Amount of items: 2
Items: 
Size: 567177 Color: 1
Size: 432445 Color: 0

Bin 3466: 380 of cap free
Amount of items: 2
Items: 
Size: 598260 Color: 0
Size: 401361 Color: 1

Bin 3467: 380 of cap free
Amount of items: 2
Items: 
Size: 751028 Color: 1
Size: 248593 Color: 0

Bin 3468: 381 of cap free
Amount of items: 2
Items: 
Size: 574001 Color: 0
Size: 425619 Color: 1

Bin 3469: 381 of cap free
Amount of items: 2
Items: 
Size: 591571 Color: 1
Size: 408049 Color: 0

Bin 3470: 381 of cap free
Amount of items: 2
Items: 
Size: 720721 Color: 1
Size: 278899 Color: 0

Bin 3471: 381 of cap free
Amount of items: 2
Items: 
Size: 774315 Color: 1
Size: 225305 Color: 0

Bin 3472: 382 of cap free
Amount of items: 2
Items: 
Size: 775742 Color: 1
Size: 223877 Color: 0

Bin 3473: 383 of cap free
Amount of items: 2
Items: 
Size: 514227 Color: 1
Size: 485391 Color: 0

Bin 3474: 383 of cap free
Amount of items: 2
Items: 
Size: 583570 Color: 1
Size: 416048 Color: 0

Bin 3475: 383 of cap free
Amount of items: 2
Items: 
Size: 729541 Color: 1
Size: 270077 Color: 0

Bin 3476: 384 of cap free
Amount of items: 2
Items: 
Size: 648158 Color: 0
Size: 351459 Color: 1

Bin 3477: 384 of cap free
Amount of items: 2
Items: 
Size: 749787 Color: 0
Size: 249830 Color: 1

Bin 3478: 385 of cap free
Amount of items: 2
Items: 
Size: 620047 Color: 0
Size: 379569 Color: 1

Bin 3479: 385 of cap free
Amount of items: 2
Items: 
Size: 627477 Color: 0
Size: 372139 Color: 1

Bin 3480: 385 of cap free
Amount of items: 2
Items: 
Size: 668275 Color: 0
Size: 331341 Color: 1

Bin 3481: 386 of cap free
Amount of items: 2
Items: 
Size: 596870 Color: 0
Size: 402745 Color: 1

Bin 3482: 388 of cap free
Amount of items: 2
Items: 
Size: 604920 Color: 1
Size: 394693 Color: 0

Bin 3483: 388 of cap free
Amount of items: 2
Items: 
Size: 629275 Color: 0
Size: 370338 Color: 1

Bin 3484: 389 of cap free
Amount of items: 2
Items: 
Size: 625713 Color: 0
Size: 373899 Color: 1

Bin 3485: 389 of cap free
Amount of items: 2
Items: 
Size: 710856 Color: 1
Size: 288756 Color: 0

Bin 3486: 389 of cap free
Amount of items: 2
Items: 
Size: 763298 Color: 1
Size: 236314 Color: 0

Bin 3487: 390 of cap free
Amount of items: 2
Items: 
Size: 527855 Color: 1
Size: 471756 Color: 0

Bin 3488: 390 of cap free
Amount of items: 2
Items: 
Size: 578933 Color: 1
Size: 420678 Color: 0

Bin 3489: 390 of cap free
Amount of items: 2
Items: 
Size: 784411 Color: 0
Size: 215200 Color: 1

Bin 3490: 391 of cap free
Amount of items: 2
Items: 
Size: 583073 Color: 1
Size: 416537 Color: 0

Bin 3491: 391 of cap free
Amount of items: 2
Items: 
Size: 621001 Color: 0
Size: 378609 Color: 1

Bin 3492: 392 of cap free
Amount of items: 2
Items: 
Size: 608560 Color: 0
Size: 391049 Color: 1

Bin 3493: 392 of cap free
Amount of items: 2
Items: 
Size: 700721 Color: 1
Size: 298888 Color: 0

Bin 3494: 392 of cap free
Amount of items: 2
Items: 
Size: 747869 Color: 1
Size: 251740 Color: 0

Bin 3495: 393 of cap free
Amount of items: 2
Items: 
Size: 575362 Color: 0
Size: 424246 Color: 1

Bin 3496: 393 of cap free
Amount of items: 2
Items: 
Size: 606255 Color: 1
Size: 393353 Color: 0

Bin 3497: 394 of cap free
Amount of items: 2
Items: 
Size: 533227 Color: 0
Size: 466380 Color: 1

Bin 3498: 394 of cap free
Amount of items: 2
Items: 
Size: 777431 Color: 0
Size: 222176 Color: 1

Bin 3499: 395 of cap free
Amount of items: 2
Items: 
Size: 753314 Color: 0
Size: 246292 Color: 1

Bin 3500: 396 of cap free
Amount of items: 2
Items: 
Size: 527959 Color: 0
Size: 471646 Color: 1

Bin 3501: 399 of cap free
Amount of items: 2
Items: 
Size: 515202 Color: 1
Size: 484400 Color: 0

Bin 3502: 399 of cap free
Amount of items: 2
Items: 
Size: 741796 Color: 0
Size: 257806 Color: 1

Bin 3503: 400 of cap free
Amount of items: 2
Items: 
Size: 581762 Color: 0
Size: 417839 Color: 1

Bin 3504: 401 of cap free
Amount of items: 2
Items: 
Size: 693690 Color: 1
Size: 305910 Color: 0

Bin 3505: 401 of cap free
Amount of items: 2
Items: 
Size: 566085 Color: 0
Size: 433515 Color: 1

Bin 3506: 401 of cap free
Amount of items: 2
Items: 
Size: 576491 Color: 0
Size: 423109 Color: 1

Bin 3507: 401 of cap free
Amount of items: 2
Items: 
Size: 661625 Color: 0
Size: 337975 Color: 1

Bin 3508: 402 of cap free
Amount of items: 2
Items: 
Size: 564449 Color: 1
Size: 435150 Color: 0

Bin 3509: 402 of cap free
Amount of items: 2
Items: 
Size: 566825 Color: 0
Size: 432774 Color: 1

Bin 3510: 402 of cap free
Amount of items: 2
Items: 
Size: 613675 Color: 1
Size: 385924 Color: 0

Bin 3511: 402 of cap free
Amount of items: 2
Items: 
Size: 671375 Color: 0
Size: 328224 Color: 1

Bin 3512: 404 of cap free
Amount of items: 2
Items: 
Size: 667154 Color: 1
Size: 332443 Color: 0

Bin 3513: 405 of cap free
Amount of items: 2
Items: 
Size: 572246 Color: 1
Size: 427350 Color: 0

Bin 3514: 405 of cap free
Amount of items: 2
Items: 
Size: 691006 Color: 1
Size: 308590 Color: 0

Bin 3515: 406 of cap free
Amount of items: 2
Items: 
Size: 765627 Color: 1
Size: 233968 Color: 0

Bin 3516: 406 of cap free
Amount of items: 2
Items: 
Size: 551945 Color: 1
Size: 447650 Color: 0

Bin 3517: 408 of cap free
Amount of items: 2
Items: 
Size: 573994 Color: 0
Size: 425599 Color: 1

Bin 3518: 409 of cap free
Amount of items: 2
Items: 
Size: 656930 Color: 0
Size: 342662 Color: 1

Bin 3519: 411 of cap free
Amount of items: 2
Items: 
Size: 653180 Color: 0
Size: 346410 Color: 1

Bin 3520: 411 of cap free
Amount of items: 2
Items: 
Size: 657414 Color: 0
Size: 342176 Color: 1

Bin 3521: 414 of cap free
Amount of items: 2
Items: 
Size: 525307 Color: 1
Size: 474280 Color: 0

Bin 3522: 414 of cap free
Amount of items: 2
Items: 
Size: 694282 Color: 1
Size: 305305 Color: 0

Bin 3523: 415 of cap free
Amount of items: 2
Items: 
Size: 563437 Color: 1
Size: 436149 Color: 0

Bin 3524: 415 of cap free
Amount of items: 2
Items: 
Size: 571785 Color: 1
Size: 427801 Color: 0

Bin 3525: 416 of cap free
Amount of items: 2
Items: 
Size: 517659 Color: 1
Size: 481926 Color: 0

Bin 3526: 418 of cap free
Amount of items: 2
Items: 
Size: 706464 Color: 1
Size: 293119 Color: 0

Bin 3527: 420 of cap free
Amount of items: 2
Items: 
Size: 618793 Color: 0
Size: 380788 Color: 1

Bin 3528: 420 of cap free
Amount of items: 2
Items: 
Size: 521742 Color: 0
Size: 477839 Color: 1

Bin 3529: 420 of cap free
Amount of items: 2
Items: 
Size: 529161 Color: 1
Size: 470420 Color: 0

Bin 3530: 422 of cap free
Amount of items: 2
Items: 
Size: 605273 Color: 0
Size: 394306 Color: 1

Bin 3531: 422 of cap free
Amount of items: 2
Items: 
Size: 688974 Color: 1
Size: 310605 Color: 0

Bin 3532: 423 of cap free
Amount of items: 2
Items: 
Size: 572923 Color: 0
Size: 426655 Color: 1

Bin 3533: 425 of cap free
Amount of items: 2
Items: 
Size: 752465 Color: 0
Size: 247111 Color: 1

Bin 3534: 426 of cap free
Amount of items: 2
Items: 
Size: 724969 Color: 1
Size: 274606 Color: 0

Bin 3535: 427 of cap free
Amount of items: 2
Items: 
Size: 552302 Color: 0
Size: 447272 Color: 1

Bin 3536: 428 of cap free
Amount of items: 2
Items: 
Size: 573523 Color: 0
Size: 426050 Color: 1

Bin 3537: 430 of cap free
Amount of items: 2
Items: 
Size: 534854 Color: 1
Size: 464717 Color: 0

Bin 3538: 430 of cap free
Amount of items: 2
Items: 
Size: 637157 Color: 1
Size: 362414 Color: 0

Bin 3539: 431 of cap free
Amount of items: 2
Items: 
Size: 531218 Color: 1
Size: 468352 Color: 0

Bin 3540: 431 of cap free
Amount of items: 2
Items: 
Size: 534476 Color: 0
Size: 465094 Color: 1

Bin 3541: 431 of cap free
Amount of items: 2
Items: 
Size: 787620 Color: 0
Size: 211950 Color: 1

Bin 3542: 432 of cap free
Amount of items: 2
Items: 
Size: 591649 Color: 0
Size: 407920 Color: 1

Bin 3543: 432 of cap free
Amount of items: 2
Items: 
Size: 644143 Color: 1
Size: 355426 Color: 0

Bin 3544: 432 of cap free
Amount of items: 2
Items: 
Size: 724531 Color: 1
Size: 275038 Color: 0

Bin 3545: 434 of cap free
Amount of items: 2
Items: 
Size: 662593 Color: 0
Size: 336974 Color: 1

Bin 3546: 435 of cap free
Amount of items: 2
Items: 
Size: 592976 Color: 0
Size: 406590 Color: 1

Bin 3547: 435 of cap free
Amount of items: 2
Items: 
Size: 615768 Color: 0
Size: 383798 Color: 1

Bin 3548: 436 of cap free
Amount of items: 2
Items: 
Size: 734483 Color: 0
Size: 265082 Color: 1

Bin 3549: 437 of cap free
Amount of items: 2
Items: 
Size: 604887 Color: 1
Size: 394677 Color: 0

Bin 3550: 437 of cap free
Amount of items: 2
Items: 
Size: 608572 Color: 1
Size: 390992 Color: 0

Bin 3551: 438 of cap free
Amount of items: 2
Items: 
Size: 539200 Color: 0
Size: 460363 Color: 1

Bin 3552: 439 of cap free
Amount of items: 2
Items: 
Size: 562680 Color: 0
Size: 436882 Color: 1

Bin 3553: 439 of cap free
Amount of items: 2
Items: 
Size: 704146 Color: 1
Size: 295416 Color: 0

Bin 3554: 440 of cap free
Amount of items: 2
Items: 
Size: 776145 Color: 1
Size: 223416 Color: 0

Bin 3555: 440 of cap free
Amount of items: 2
Items: 
Size: 571913 Color: 0
Size: 427648 Color: 1

Bin 3556: 441 of cap free
Amount of items: 2
Items: 
Size: 580115 Color: 0
Size: 419445 Color: 1

Bin 3557: 441 of cap free
Amount of items: 2
Items: 
Size: 590820 Color: 1
Size: 408740 Color: 0

Bin 3558: 441 of cap free
Amount of items: 2
Items: 
Size: 620956 Color: 0
Size: 378604 Color: 1

Bin 3559: 441 of cap free
Amount of items: 2
Items: 
Size: 673605 Color: 1
Size: 325955 Color: 0

Bin 3560: 442 of cap free
Amount of items: 2
Items: 
Size: 772784 Color: 0
Size: 226775 Color: 1

Bin 3561: 443 of cap free
Amount of items: 2
Items: 
Size: 787887 Color: 1
Size: 211671 Color: 0

Bin 3562: 444 of cap free
Amount of items: 2
Items: 
Size: 544698 Color: 1
Size: 454859 Color: 0

Bin 3563: 444 of cap free
Amount of items: 2
Items: 
Size: 772721 Color: 1
Size: 226836 Color: 0

Bin 3564: 445 of cap free
Amount of items: 2
Items: 
Size: 558029 Color: 0
Size: 441527 Color: 1

Bin 3565: 445 of cap free
Amount of items: 2
Items: 
Size: 721436 Color: 0
Size: 278120 Color: 1

Bin 3566: 447 of cap free
Amount of items: 2
Items: 
Size: 700429 Color: 0
Size: 299125 Color: 1

Bin 3567: 447 of cap free
Amount of items: 2
Items: 
Size: 777478 Color: 1
Size: 222076 Color: 0

Bin 3568: 450 of cap free
Amount of items: 2
Items: 
Size: 614838 Color: 0
Size: 384713 Color: 1

Bin 3569: 450 of cap free
Amount of items: 2
Items: 
Size: 644142 Color: 1
Size: 355409 Color: 0

Bin 3570: 450 of cap free
Amount of items: 2
Items: 
Size: 654605 Color: 0
Size: 344946 Color: 1

Bin 3571: 450 of cap free
Amount of items: 2
Items: 
Size: 665998 Color: 1
Size: 333553 Color: 0

Bin 3572: 451 of cap free
Amount of items: 2
Items: 
Size: 554077 Color: 1
Size: 445473 Color: 0

Bin 3573: 451 of cap free
Amount of items: 2
Items: 
Size: 648332 Color: 1
Size: 351218 Color: 0

Bin 3574: 452 of cap free
Amount of items: 2
Items: 
Size: 648135 Color: 0
Size: 351414 Color: 1

Bin 3575: 454 of cap free
Amount of items: 2
Items: 
Size: 627451 Color: 0
Size: 372096 Color: 1

Bin 3576: 455 of cap free
Amount of items: 2
Items: 
Size: 501425 Color: 0
Size: 498121 Color: 1

Bin 3577: 455 of cap free
Amount of items: 2
Items: 
Size: 515849 Color: 1
Size: 483697 Color: 0

Bin 3578: 455 of cap free
Amount of items: 2
Items: 
Size: 658553 Color: 0
Size: 340993 Color: 1

Bin 3579: 457 of cap free
Amount of items: 2
Items: 
Size: 617299 Color: 0
Size: 382245 Color: 1

Bin 3580: 459 of cap free
Amount of items: 2
Items: 
Size: 531223 Color: 0
Size: 468319 Color: 1

Bin 3581: 460 of cap free
Amount of items: 2
Items: 
Size: 679301 Color: 1
Size: 320240 Color: 0

Bin 3582: 460 of cap free
Amount of items: 2
Items: 
Size: 501300 Color: 1
Size: 498241 Color: 0

Bin 3583: 462 of cap free
Amount of items: 2
Items: 
Size: 606233 Color: 1
Size: 393306 Color: 0

Bin 3584: 463 of cap free
Amount of items: 2
Items: 
Size: 606577 Color: 0
Size: 392961 Color: 1

Bin 3585: 465 of cap free
Amount of items: 2
Items: 
Size: 503299 Color: 1
Size: 496237 Color: 0

Bin 3586: 465 of cap free
Amount of items: 2
Items: 
Size: 515499 Color: 0
Size: 484037 Color: 1

Bin 3587: 465 of cap free
Amount of items: 2
Items: 
Size: 527851 Color: 1
Size: 471685 Color: 0

Bin 3588: 465 of cap free
Amount of items: 2
Items: 
Size: 599274 Color: 0
Size: 400262 Color: 1

Bin 3589: 466 of cap free
Amount of items: 2
Items: 
Size: 548384 Color: 0
Size: 451151 Color: 1

Bin 3590: 466 of cap free
Amount of items: 2
Items: 
Size: 746532 Color: 0
Size: 253003 Color: 1

Bin 3591: 467 of cap free
Amount of items: 2
Items: 
Size: 558016 Color: 0
Size: 441518 Color: 1

Bin 3592: 470 of cap free
Amount of items: 2
Items: 
Size: 628603 Color: 0
Size: 370928 Color: 1

Bin 3593: 470 of cap free
Amount of items: 2
Items: 
Size: 744217 Color: 0
Size: 255314 Color: 1

Bin 3594: 471 of cap free
Amount of items: 2
Items: 
Size: 582509 Color: 1
Size: 417021 Color: 0

Bin 3595: 472 of cap free
Amount of items: 2
Items: 
Size: 682763 Color: 1
Size: 316766 Color: 0

Bin 3596: 472 of cap free
Amount of items: 2
Items: 
Size: 663379 Color: 1
Size: 336150 Color: 0

Bin 3597: 472 of cap free
Amount of items: 2
Items: 
Size: 595319 Color: 1
Size: 404210 Color: 0

Bin 3598: 473 of cap free
Amount of items: 2
Items: 
Size: 616214 Color: 1
Size: 383314 Color: 0

Bin 3599: 473 of cap free
Amount of items: 2
Items: 
Size: 631641 Color: 0
Size: 367887 Color: 1

Bin 3600: 474 of cap free
Amount of items: 2
Items: 
Size: 536089 Color: 0
Size: 463438 Color: 1

Bin 3601: 474 of cap free
Amount of items: 2
Items: 
Size: 664314 Color: 0
Size: 335213 Color: 1

Bin 3602: 477 of cap free
Amount of items: 2
Items: 
Size: 555734 Color: 0
Size: 443790 Color: 1

Bin 3603: 477 of cap free
Amount of items: 2
Items: 
Size: 525599 Color: 0
Size: 473925 Color: 1

Bin 3604: 477 of cap free
Amount of items: 2
Items: 
Size: 551886 Color: 1
Size: 447638 Color: 0

Bin 3605: 477 of cap free
Amount of items: 2
Items: 
Size: 731580 Color: 0
Size: 267944 Color: 1

Bin 3606: 478 of cap free
Amount of items: 2
Items: 
Size: 766562 Color: 1
Size: 232961 Color: 0

Bin 3607: 478 of cap free
Amount of items: 2
Items: 
Size: 513131 Color: 0
Size: 486392 Color: 1

Bin 3608: 479 of cap free
Amount of items: 2
Items: 
Size: 507259 Color: 0
Size: 492263 Color: 1

Bin 3609: 479 of cap free
Amount of items: 2
Items: 
Size: 620964 Color: 1
Size: 378558 Color: 0

Bin 3610: 479 of cap free
Amount of items: 2
Items: 
Size: 736642 Color: 1
Size: 262880 Color: 0

Bin 3611: 481 of cap free
Amount of items: 2
Items: 
Size: 692750 Color: 1
Size: 306770 Color: 0

Bin 3612: 481 of cap free
Amount of items: 2
Items: 
Size: 601154 Color: 0
Size: 398366 Color: 1

Bin 3613: 482 of cap free
Amount of items: 2
Items: 
Size: 596224 Color: 1
Size: 403295 Color: 0

Bin 3614: 482 of cap free
Amount of items: 2
Items: 
Size: 745179 Color: 1
Size: 254340 Color: 0

Bin 3615: 483 of cap free
Amount of items: 2
Items: 
Size: 544052 Color: 0
Size: 455466 Color: 1

Bin 3616: 484 of cap free
Amount of items: 2
Items: 
Size: 624843 Color: 1
Size: 374674 Color: 0

Bin 3617: 484 of cap free
Amount of items: 2
Items: 
Size: 798968 Color: 0
Size: 200549 Color: 1

Bin 3618: 487 of cap free
Amount of items: 2
Items: 
Size: 727202 Color: 0
Size: 272312 Color: 1

Bin 3619: 487 of cap free
Amount of items: 2
Items: 
Size: 560121 Color: 0
Size: 439393 Color: 1

Bin 3620: 487 of cap free
Amount of items: 2
Items: 
Size: 688950 Color: 1
Size: 310564 Color: 0

Bin 3621: 488 of cap free
Amount of items: 2
Items: 
Size: 531877 Color: 0
Size: 467636 Color: 1

Bin 3622: 489 of cap free
Amount of items: 2
Items: 
Size: 564380 Color: 1
Size: 435132 Color: 0

Bin 3623: 491 of cap free
Amount of items: 2
Items: 
Size: 571732 Color: 1
Size: 427778 Color: 0

Bin 3624: 491 of cap free
Amount of items: 2
Items: 
Size: 694209 Color: 1
Size: 305301 Color: 0

Bin 3625: 491 of cap free
Amount of items: 2
Items: 
Size: 733223 Color: 1
Size: 266287 Color: 0

Bin 3626: 493 of cap free
Amount of items: 2
Items: 
Size: 746526 Color: 0
Size: 252982 Color: 1

Bin 3627: 494 of cap free
Amount of items: 2
Items: 
Size: 579180 Color: 0
Size: 420327 Color: 1

Bin 3628: 494 of cap free
Amount of items: 2
Items: 
Size: 593860 Color: 0
Size: 405647 Color: 1

Bin 3629: 494 of cap free
Amount of items: 2
Items: 
Size: 662583 Color: 0
Size: 336924 Color: 1

Bin 3630: 495 of cap free
Amount of items: 2
Items: 
Size: 576297 Color: 1
Size: 423209 Color: 0

Bin 3631: 495 of cap free
Amount of items: 2
Items: 
Size: 702040 Color: 0
Size: 297466 Color: 1

Bin 3632: 495 of cap free
Amount of items: 2
Items: 
Size: 724025 Color: 0
Size: 275481 Color: 1

Bin 3633: 497 of cap free
Amount of items: 2
Items: 
Size: 760850 Color: 0
Size: 238654 Color: 1

Bin 3634: 498 of cap free
Amount of items: 2
Items: 
Size: 514152 Color: 1
Size: 485351 Color: 0

Bin 3635: 500 of cap free
Amount of items: 2
Items: 
Size: 618427 Color: 1
Size: 381074 Color: 0

Bin 3636: 500 of cap free
Amount of items: 2
Items: 
Size: 673604 Color: 1
Size: 325897 Color: 0

Bin 3637: 502 of cap free
Amount of items: 2
Items: 
Size: 747864 Color: 1
Size: 251635 Color: 0

Bin 3638: 504 of cap free
Amount of items: 2
Items: 
Size: 619976 Color: 0
Size: 379521 Color: 1

Bin 3639: 504 of cap free
Amount of items: 2
Items: 
Size: 601138 Color: 0
Size: 398359 Color: 1

Bin 3640: 506 of cap free
Amount of items: 2
Items: 
Size: 512164 Color: 1
Size: 487331 Color: 0

Bin 3641: 508 of cap free
Amount of items: 2
Items: 
Size: 569155 Color: 0
Size: 430338 Color: 1

Bin 3642: 508 of cap free
Amount of items: 2
Items: 
Size: 733715 Color: 1
Size: 265778 Color: 0

Bin 3643: 509 of cap free
Amount of items: 2
Items: 
Size: 786700 Color: 0
Size: 212792 Color: 1

Bin 3644: 511 of cap free
Amount of items: 2
Items: 
Size: 645759 Color: 1
Size: 353731 Color: 0

Bin 3645: 511 of cap free
Amount of items: 2
Items: 
Size: 797831 Color: 1
Size: 201659 Color: 0

Bin 3646: 512 of cap free
Amount of items: 2
Items: 
Size: 536158 Color: 1
Size: 463331 Color: 0

Bin 3647: 512 of cap free
Amount of items: 2
Items: 
Size: 661231 Color: 1
Size: 338258 Color: 0

Bin 3648: 513 of cap free
Amount of items: 2
Items: 
Size: 606217 Color: 1
Size: 393271 Color: 0

Bin 3649: 513 of cap free
Amount of items: 2
Items: 
Size: 562689 Color: 1
Size: 436799 Color: 0

Bin 3650: 514 of cap free
Amount of items: 2
Items: 
Size: 705923 Color: 0
Size: 293564 Color: 1

Bin 3651: 517 of cap free
Amount of items: 2
Items: 
Size: 574400 Color: 1
Size: 425084 Color: 0

Bin 3652: 521 of cap free
Amount of items: 2
Items: 
Size: 511794 Color: 0
Size: 487686 Color: 1

Bin 3653: 522 of cap free
Amount of items: 2
Items: 
Size: 504238 Color: 1
Size: 495241 Color: 0

Bin 3654: 522 of cap free
Amount of items: 2
Items: 
Size: 758791 Color: 0
Size: 240688 Color: 1

Bin 3655: 523 of cap free
Amount of items: 2
Items: 
Size: 682854 Color: 0
Size: 316624 Color: 1

Bin 3656: 525 of cap free
Amount of items: 2
Items: 
Size: 539199 Color: 0
Size: 460277 Color: 1

Bin 3657: 525 of cap free
Amount of items: 2
Items: 
Size: 648328 Color: 1
Size: 351148 Color: 0

Bin 3658: 527 of cap free
Amount of items: 2
Items: 
Size: 501419 Color: 0
Size: 498055 Color: 1

Bin 3659: 528 of cap free
Amount of items: 2
Items: 
Size: 751887 Color: 0
Size: 247586 Color: 1

Bin 3660: 529 of cap free
Amount of items: 2
Items: 
Size: 587712 Color: 1
Size: 411760 Color: 0

Bin 3661: 530 of cap free
Amount of items: 2
Items: 
Size: 586436 Color: 0
Size: 413035 Color: 1

Bin 3662: 530 of cap free
Amount of items: 2
Items: 
Size: 634297 Color: 1
Size: 365174 Color: 0

Bin 3663: 530 of cap free
Amount of items: 2
Items: 
Size: 762714 Color: 1
Size: 236757 Color: 0

Bin 3664: 531 of cap free
Amount of items: 2
Items: 
Size: 605176 Color: 0
Size: 394294 Color: 1

Bin 3665: 531 of cap free
Amount of items: 2
Items: 
Size: 721572 Color: 1
Size: 277898 Color: 0

Bin 3666: 534 of cap free
Amount of items: 2
Items: 
Size: 633026 Color: 0
Size: 366441 Color: 1

Bin 3667: 534 of cap free
Amount of items: 2
Items: 
Size: 700692 Color: 1
Size: 298775 Color: 0

Bin 3668: 535 of cap free
Amount of items: 3
Items: 
Size: 395370 Color: 0
Size: 321817 Color: 1
Size: 282279 Color: 0

Bin 3669: 535 of cap free
Amount of items: 2
Items: 
Size: 550970 Color: 0
Size: 448496 Color: 1

Bin 3670: 535 of cap free
Amount of items: 2
Items: 
Size: 578915 Color: 1
Size: 420551 Color: 0

Bin 3671: 535 of cap free
Amount of items: 2
Items: 
Size: 703355 Color: 0
Size: 296111 Color: 1

Bin 3672: 535 of cap free
Amount of items: 2
Items: 
Size: 791688 Color: 1
Size: 207778 Color: 0

Bin 3673: 536 of cap free
Amount of items: 2
Items: 
Size: 781108 Color: 0
Size: 218357 Color: 1

Bin 3674: 536 of cap free
Amount of items: 2
Items: 
Size: 509036 Color: 0
Size: 490429 Color: 1

Bin 3675: 538 of cap free
Amount of items: 2
Items: 
Size: 711549 Color: 1
Size: 287914 Color: 0

Bin 3676: 539 of cap free
Amount of items: 2
Items: 
Size: 603714 Color: 0
Size: 395748 Color: 1

Bin 3677: 539 of cap free
Amount of items: 2
Items: 
Size: 549466 Color: 0
Size: 449996 Color: 1

Bin 3678: 541 of cap free
Amount of items: 2
Items: 
Size: 545465 Color: 0
Size: 453995 Color: 1

Bin 3679: 541 of cap free
Amount of items: 2
Items: 
Size: 552271 Color: 0
Size: 447189 Color: 1

Bin 3680: 541 of cap free
Amount of items: 2
Items: 
Size: 686950 Color: 1
Size: 312510 Color: 0

Bin 3681: 542 of cap free
Amount of items: 2
Items: 
Size: 520471 Color: 0
Size: 478988 Color: 1

Bin 3682: 544 of cap free
Amount of items: 2
Items: 
Size: 571835 Color: 0
Size: 427622 Color: 1

Bin 3683: 545 of cap free
Amount of items: 2
Items: 
Size: 791580 Color: 0
Size: 207876 Color: 1

Bin 3684: 546 of cap free
Amount of items: 2
Items: 
Size: 503230 Color: 1
Size: 496225 Color: 0

Bin 3685: 547 of cap free
Amount of items: 2
Items: 
Size: 574923 Color: 1
Size: 424531 Color: 0

Bin 3686: 549 of cap free
Amount of items: 2
Items: 
Size: 589017 Color: 0
Size: 410435 Color: 1

Bin 3687: 552 of cap free
Amount of items: 2
Items: 
Size: 592866 Color: 0
Size: 406583 Color: 1

Bin 3688: 554 of cap free
Amount of items: 2
Items: 
Size: 710506 Color: 0
Size: 288941 Color: 1

Bin 3689: 554 of cap free
Amount of items: 2
Items: 
Size: 636136 Color: 0
Size: 363311 Color: 1

Bin 3690: 556 of cap free
Amount of items: 2
Items: 
Size: 506067 Color: 1
Size: 493378 Color: 0

Bin 3691: 556 of cap free
Amount of items: 2
Items: 
Size: 680656 Color: 1
Size: 318789 Color: 0

Bin 3692: 557 of cap free
Amount of items: 2
Items: 
Size: 548027 Color: 1
Size: 451417 Color: 0

Bin 3693: 563 of cap free
Amount of items: 2
Items: 
Size: 717220 Color: 0
Size: 282218 Color: 1

Bin 3694: 564 of cap free
Amount of items: 2
Items: 
Size: 689861 Color: 0
Size: 309576 Color: 1

Bin 3695: 567 of cap free
Amount of items: 2
Items: 
Size: 653475 Color: 1
Size: 345959 Color: 0

Bin 3696: 567 of cap free
Amount of items: 2
Items: 
Size: 627425 Color: 0
Size: 372009 Color: 1

Bin 3697: 568 of cap free
Amount of items: 2
Items: 
Size: 514268 Color: 0
Size: 485165 Color: 1

Bin 3698: 569 of cap free
Amount of items: 2
Items: 
Size: 616222 Color: 0
Size: 383210 Color: 1

Bin 3699: 569 of cap free
Amount of items: 2
Items: 
Size: 758789 Color: 0
Size: 240643 Color: 1

Bin 3700: 571 of cap free
Amount of items: 2
Items: 
Size: 669562 Color: 0
Size: 329868 Color: 1

Bin 3701: 573 of cap free
Amount of items: 2
Items: 
Size: 672120 Color: 1
Size: 327308 Color: 0

Bin 3702: 573 of cap free
Amount of items: 2
Items: 
Size: 709159 Color: 0
Size: 290269 Color: 1

Bin 3703: 574 of cap free
Amount of items: 2
Items: 
Size: 598130 Color: 0
Size: 401297 Color: 1

Bin 3704: 574 of cap free
Amount of items: 2
Items: 
Size: 508182 Color: 0
Size: 491245 Color: 1

Bin 3705: 575 of cap free
Amount of items: 2
Items: 
Size: 739403 Color: 1
Size: 260023 Color: 0

Bin 3706: 575 of cap free
Amount of items: 2
Items: 
Size: 533758 Color: 0
Size: 465668 Color: 1

Bin 3707: 576 of cap free
Amount of items: 2
Items: 
Size: 762814 Color: 0
Size: 236611 Color: 1

Bin 3708: 578 of cap free
Amount of items: 2
Items: 
Size: 787539 Color: 0
Size: 211884 Color: 1

Bin 3709: 578 of cap free
Amount of items: 2
Items: 
Size: 569107 Color: 0
Size: 430316 Color: 1

Bin 3710: 579 of cap free
Amount of items: 2
Items: 
Size: 547388 Color: 1
Size: 452034 Color: 0

Bin 3711: 581 of cap free
Amount of items: 2
Items: 
Size: 641214 Color: 1
Size: 358206 Color: 0

Bin 3712: 582 of cap free
Amount of items: 2
Items: 
Size: 609428 Color: 1
Size: 389991 Color: 0

Bin 3713: 585 of cap free
Amount of items: 6
Items: 
Size: 224651 Color: 1
Size: 161808 Color: 0
Size: 160754 Color: 0
Size: 160690 Color: 1
Size: 151620 Color: 0
Size: 139893 Color: 1

Bin 3714: 585 of cap free
Amount of items: 2
Items: 
Size: 586388 Color: 0
Size: 413028 Color: 1

Bin 3715: 586 of cap free
Amount of items: 2
Items: 
Size: 790092 Color: 0
Size: 209323 Color: 1

Bin 3716: 587 of cap free
Amount of items: 2
Items: 
Size: 544005 Color: 0
Size: 455409 Color: 1

Bin 3717: 587 of cap free
Amount of items: 2
Items: 
Size: 680986 Color: 0
Size: 318428 Color: 1

Bin 3718: 588 of cap free
Amount of items: 2
Items: 
Size: 583550 Color: 1
Size: 415863 Color: 0

Bin 3719: 589 of cap free
Amount of items: 2
Items: 
Size: 665868 Color: 1
Size: 333544 Color: 0

Bin 3720: 590 of cap free
Amount of items: 2
Items: 
Size: 661485 Color: 0
Size: 337926 Color: 1

Bin 3721: 591 of cap free
Amount of items: 2
Items: 
Size: 521371 Color: 1
Size: 478039 Color: 0

Bin 3722: 592 of cap free
Amount of items: 2
Items: 
Size: 582824 Color: 0
Size: 416585 Color: 1

Bin 3723: 596 of cap free
Amount of items: 2
Items: 
Size: 750825 Color: 1
Size: 248580 Color: 0

Bin 3724: 598 of cap free
Amount of items: 2
Items: 
Size: 746899 Color: 1
Size: 252504 Color: 0

Bin 3725: 601 of cap free
Amount of items: 2
Items: 
Size: 622862 Color: 1
Size: 376538 Color: 0

Bin 3726: 602 of cap free
Amount of items: 2
Items: 
Size: 679078 Color: 0
Size: 320321 Color: 1

Bin 3727: 602 of cap free
Amount of items: 2
Items: 
Size: 706985 Color: 1
Size: 292414 Color: 0

Bin 3728: 603 of cap free
Amount of items: 2
Items: 
Size: 665621 Color: 0
Size: 333777 Color: 1

Bin 3729: 605 of cap free
Amount of items: 2
Items: 
Size: 787736 Color: 1
Size: 211660 Color: 0

Bin 3730: 606 of cap free
Amount of items: 2
Items: 
Size: 598802 Color: 1
Size: 400593 Color: 0

Bin 3731: 607 of cap free
Amount of items: 2
Items: 
Size: 664219 Color: 0
Size: 335175 Color: 1

Bin 3732: 607 of cap free
Amount of items: 2
Items: 
Size: 754424 Color: 1
Size: 244970 Color: 0

Bin 3733: 608 of cap free
Amount of items: 3
Items: 
Size: 734519 Color: 0
Size: 150704 Color: 1
Size: 114170 Color: 0

Bin 3734: 608 of cap free
Amount of items: 2
Items: 
Size: 784590 Color: 1
Size: 214803 Color: 0

Bin 3735: 608 of cap free
Amount of items: 2
Items: 
Size: 735052 Color: 0
Size: 264341 Color: 1

Bin 3736: 609 of cap free
Amount of items: 2
Items: 
Size: 530603 Color: 0
Size: 468789 Color: 1

Bin 3737: 610 of cap free
Amount of items: 2
Items: 
Size: 618758 Color: 0
Size: 380633 Color: 1

Bin 3738: 610 of cap free
Amount of items: 2
Items: 
Size: 616144 Color: 1
Size: 383247 Color: 0

Bin 3739: 610 of cap free
Amount of items: 2
Items: 
Size: 642511 Color: 0
Size: 356880 Color: 1

Bin 3740: 612 of cap free
Amount of items: 2
Items: 
Size: 544649 Color: 1
Size: 454740 Color: 0

Bin 3741: 612 of cap free
Amount of items: 2
Items: 
Size: 620849 Color: 1
Size: 378540 Color: 0

Bin 3742: 615 of cap free
Amount of items: 2
Items: 
Size: 795753 Color: 0
Size: 203633 Color: 1

Bin 3743: 616 of cap free
Amount of items: 2
Items: 
Size: 759633 Color: 0
Size: 239752 Color: 1

Bin 3744: 618 of cap free
Amount of items: 2
Items: 
Size: 538673 Color: 1
Size: 460710 Color: 0

Bin 3745: 618 of cap free
Amount of items: 2
Items: 
Size: 658425 Color: 0
Size: 340958 Color: 1

Bin 3746: 619 of cap free
Amount of items: 2
Items: 
Size: 596196 Color: 1
Size: 403186 Color: 0

Bin 3747: 621 of cap free
Amount of items: 2
Items: 
Size: 515724 Color: 1
Size: 483656 Color: 0

Bin 3748: 622 of cap free
Amount of items: 2
Items: 
Size: 799782 Color: 0
Size: 199597 Color: 1

Bin 3749: 622 of cap free
Amount of items: 2
Items: 
Size: 581034 Color: 0
Size: 418345 Color: 1

Bin 3750: 624 of cap free
Amount of items: 6
Items: 
Size: 174055 Color: 0
Size: 173505 Color: 0
Size: 171946 Color: 1
Size: 170220 Color: 1
Size: 170167 Color: 1
Size: 139484 Color: 0

Bin 3751: 625 of cap free
Amount of items: 2
Items: 
Size: 740171 Color: 1
Size: 259205 Color: 0

Bin 3752: 627 of cap free
Amount of items: 2
Items: 
Size: 629419 Color: 1
Size: 369955 Color: 0

Bin 3753: 628 of cap free
Amount of items: 2
Items: 
Size: 555696 Color: 0
Size: 443677 Color: 1

Bin 3754: 628 of cap free
Amount of items: 2
Items: 
Size: 611053 Color: 0
Size: 388320 Color: 1

Bin 3755: 628 of cap free
Amount of items: 2
Items: 
Size: 708328 Color: 0
Size: 291045 Color: 1

Bin 3756: 630 of cap free
Amount of items: 2
Items: 
Size: 630732 Color: 1
Size: 368639 Color: 0

Bin 3757: 630 of cap free
Amount of items: 2
Items: 
Size: 745114 Color: 1
Size: 254257 Color: 0

Bin 3758: 633 of cap free
Amount of items: 2
Items: 
Size: 580801 Color: 1
Size: 418567 Color: 0

Bin 3759: 633 of cap free
Amount of items: 2
Items: 
Size: 665141 Color: 1
Size: 334227 Color: 0

Bin 3760: 634 of cap free
Amount of items: 3
Items: 
Size: 576046 Color: 0
Size: 257680 Color: 0
Size: 165641 Color: 1

Bin 3761: 636 of cap free
Amount of items: 2
Items: 
Size: 559540 Color: 1
Size: 439825 Color: 0

Bin 3762: 636 of cap free
Amount of items: 2
Items: 
Size: 791544 Color: 0
Size: 207821 Color: 1

Bin 3763: 636 of cap free
Amount of items: 2
Items: 
Size: 658883 Color: 1
Size: 340482 Color: 0

Bin 3764: 638 of cap free
Amount of items: 2
Items: 
Size: 692705 Color: 1
Size: 306658 Color: 0

Bin 3765: 638 of cap free
Amount of items: 2
Items: 
Size: 652289 Color: 0
Size: 347074 Color: 1

Bin 3766: 639 of cap free
Amount of items: 2
Items: 
Size: 606123 Color: 1
Size: 393239 Color: 0

Bin 3767: 639 of cap free
Amount of items: 2
Items: 
Size: 541047 Color: 0
Size: 458315 Color: 1

Bin 3768: 640 of cap free
Amount of items: 2
Items: 
Size: 785708 Color: 0
Size: 213653 Color: 1

Bin 3769: 640 of cap free
Amount of items: 2
Items: 
Size: 786631 Color: 0
Size: 212730 Color: 1

Bin 3770: 640 of cap free
Amount of items: 2
Items: 
Size: 685299 Color: 1
Size: 314062 Color: 0

Bin 3771: 643 of cap free
Amount of items: 2
Items: 
Size: 588939 Color: 0
Size: 410419 Color: 1

Bin 3772: 644 of cap free
Amount of items: 2
Items: 
Size: 761088 Color: 1
Size: 238269 Color: 0

Bin 3773: 644 of cap free
Amount of items: 2
Items: 
Size: 644036 Color: 1
Size: 355321 Color: 0

Bin 3774: 644 of cap free
Amount of items: 2
Items: 
Size: 691678 Color: 0
Size: 307679 Color: 1

Bin 3775: 648 of cap free
Amount of items: 2
Items: 
Size: 603704 Color: 0
Size: 395649 Color: 1

Bin 3776: 654 of cap free
Amount of items: 2
Items: 
Size: 671292 Color: 0
Size: 328055 Color: 1

Bin 3777: 655 of cap free
Amount of items: 2
Items: 
Size: 516510 Color: 0
Size: 482836 Color: 1

Bin 3778: 655 of cap free
Amount of items: 2
Items: 
Size: 545628 Color: 1
Size: 453718 Color: 0

Bin 3779: 656 of cap free
Amount of items: 2
Items: 
Size: 582788 Color: 0
Size: 416557 Color: 1

Bin 3780: 656 of cap free
Amount of items: 2
Items: 
Size: 750901 Color: 0
Size: 248444 Color: 1

Bin 3781: 657 of cap free
Amount of items: 2
Items: 
Size: 596663 Color: 0
Size: 402681 Color: 1

Bin 3782: 657 of cap free
Amount of items: 2
Items: 
Size: 781532 Color: 1
Size: 217812 Color: 0

Bin 3783: 659 of cap free
Amount of items: 2
Items: 
Size: 638596 Color: 1
Size: 360746 Color: 0

Bin 3784: 660 of cap free
Amount of items: 2
Items: 
Size: 613534 Color: 0
Size: 385807 Color: 1

Bin 3785: 665 of cap free
Amount of items: 2
Items: 
Size: 519959 Color: 1
Size: 479377 Color: 0

Bin 3786: 665 of cap free
Amount of items: 2
Items: 
Size: 581531 Color: 1
Size: 417805 Color: 0

Bin 3787: 666 of cap free
Amount of items: 2
Items: 
Size: 540082 Color: 0
Size: 459253 Color: 1

Bin 3788: 669 of cap free
Amount of items: 2
Items: 
Size: 544005 Color: 0
Size: 455327 Color: 1

Bin 3789: 669 of cap free
Amount of items: 2
Items: 
Size: 631501 Color: 0
Size: 367831 Color: 1

Bin 3790: 669 of cap free
Amount of items: 2
Items: 
Size: 638573 Color: 0
Size: 360759 Color: 1

Bin 3791: 673 of cap free
Amount of items: 2
Items: 
Size: 691654 Color: 0
Size: 307674 Color: 1

Bin 3792: 677 of cap free
Amount of items: 2
Items: 
Size: 777261 Color: 1
Size: 222063 Color: 0

Bin 3793: 678 of cap free
Amount of items: 2
Items: 
Size: 739968 Color: 1
Size: 259355 Color: 0

Bin 3794: 678 of cap free
Amount of items: 2
Items: 
Size: 665552 Color: 0
Size: 333771 Color: 1

Bin 3795: 680 of cap free
Amount of items: 2
Items: 
Size: 730228 Color: 0
Size: 269093 Color: 1

Bin 3796: 681 of cap free
Amount of items: 2
Items: 
Size: 679062 Color: 0
Size: 320258 Color: 1

Bin 3797: 682 of cap free
Amount of items: 2
Items: 
Size: 641142 Color: 1
Size: 358177 Color: 0

Bin 3798: 683 of cap free
Amount of items: 2
Items: 
Size: 642210 Color: 1
Size: 357108 Color: 0

Bin 3799: 683 of cap free
Amount of items: 2
Items: 
Size: 760708 Color: 0
Size: 238610 Color: 1

Bin 3800: 684 of cap free
Amount of items: 2
Items: 
Size: 526784 Color: 1
Size: 472533 Color: 0

Bin 3801: 684 of cap free
Amount of items: 2
Items: 
Size: 778100 Color: 0
Size: 221217 Color: 1

Bin 3802: 686 of cap free
Amount of items: 2
Items: 
Size: 514947 Color: 1
Size: 484368 Color: 0

Bin 3803: 686 of cap free
Amount of items: 2
Items: 
Size: 752430 Color: 0
Size: 246885 Color: 1

Bin 3804: 689 of cap free
Amount of items: 2
Items: 
Size: 662540 Color: 0
Size: 336772 Color: 1

Bin 3805: 689 of cap free
Amount of items: 2
Items: 
Size: 697988 Color: 1
Size: 301324 Color: 0

Bin 3806: 690 of cap free
Amount of items: 2
Items: 
Size: 779830 Color: 1
Size: 219481 Color: 0

Bin 3807: 690 of cap free
Amount of items: 2
Items: 
Size: 668493 Color: 1
Size: 330818 Color: 0

Bin 3808: 692 of cap free
Amount of items: 2
Items: 
Size: 523570 Color: 0
Size: 475739 Color: 1

Bin 3809: 692 of cap free
Amount of items: 2
Items: 
Size: 534156 Color: 1
Size: 465153 Color: 0

Bin 3810: 695 of cap free
Amount of items: 2
Items: 
Size: 788882 Color: 1
Size: 210424 Color: 0

Bin 3811: 697 of cap free
Amount of items: 2
Items: 
Size: 696253 Color: 1
Size: 303051 Color: 0

Bin 3812: 700 of cap free
Amount of items: 2
Items: 
Size: 690792 Color: 1
Size: 308509 Color: 0

Bin 3813: 701 of cap free
Amount of items: 2
Items: 
Size: 630015 Color: 0
Size: 369285 Color: 1

Bin 3814: 703 of cap free
Amount of items: 2
Items: 
Size: 762725 Color: 0
Size: 236573 Color: 1

Bin 3815: 705 of cap free
Amount of items: 2
Items: 
Size: 582749 Color: 0
Size: 416547 Color: 1

Bin 3816: 706 of cap free
Amount of items: 2
Items: 
Size: 663902 Color: 1
Size: 335393 Color: 0

Bin 3817: 709 of cap free
Amount of items: 2
Items: 
Size: 797640 Color: 1
Size: 201652 Color: 0

Bin 3818: 711 of cap free
Amount of items: 2
Items: 
Size: 607050 Color: 0
Size: 392240 Color: 1

Bin 3819: 716 of cap free
Amount of items: 2
Items: 
Size: 653424 Color: 1
Size: 345861 Color: 0

Bin 3820: 717 of cap free
Amount of items: 2
Items: 
Size: 571829 Color: 0
Size: 427455 Color: 1

Bin 3821: 720 of cap free
Amount of items: 2
Items: 
Size: 771681 Color: 0
Size: 227600 Color: 1

Bin 3822: 720 of cap free
Amount of items: 2
Items: 
Size: 542005 Color: 1
Size: 457276 Color: 0

Bin 3823: 720 of cap free
Amount of items: 2
Items: 
Size: 725441 Color: 1
Size: 273840 Color: 0

Bin 3824: 721 of cap free
Amount of items: 3
Items: 
Size: 775683 Color: 1
Size: 119567 Color: 0
Size: 104030 Color: 1

Bin 3825: 721 of cap free
Amount of items: 2
Items: 
Size: 759566 Color: 0
Size: 239714 Color: 1

Bin 3826: 722 of cap free
Amount of items: 2
Items: 
Size: 526989 Color: 0
Size: 472290 Color: 1

Bin 3827: 722 of cap free
Amount of items: 2
Items: 
Size: 580752 Color: 1
Size: 418527 Color: 0

Bin 3828: 722 of cap free
Amount of items: 2
Items: 
Size: 661357 Color: 0
Size: 337922 Color: 1

Bin 3829: 724 of cap free
Amount of items: 2
Items: 
Size: 551799 Color: 1
Size: 447478 Color: 0

Bin 3830: 724 of cap free
Amount of items: 2
Items: 
Size: 584856 Color: 0
Size: 414421 Color: 1

Bin 3831: 726 of cap free
Amount of items: 2
Items: 
Size: 706903 Color: 1
Size: 292372 Color: 0

Bin 3832: 727 of cap free
Amount of items: 2
Items: 
Size: 510409 Color: 1
Size: 488865 Color: 0

Bin 3833: 730 of cap free
Amount of items: 2
Items: 
Size: 750825 Color: 1
Size: 248446 Color: 0

Bin 3834: 731 of cap free
Amount of items: 2
Items: 
Size: 753124 Color: 0
Size: 246146 Color: 1

Bin 3835: 737 of cap free
Amount of items: 2
Items: 
Size: 789965 Color: 0
Size: 209299 Color: 1

Bin 3836: 738 of cap free
Amount of items: 2
Items: 
Size: 705891 Color: 0
Size: 293372 Color: 1

Bin 3837: 738 of cap free
Amount of items: 2
Items: 
Size: 505321 Color: 0
Size: 493942 Color: 1

Bin 3838: 738 of cap free
Amount of items: 2
Items: 
Size: 620775 Color: 1
Size: 378488 Color: 0

Bin 3839: 740 of cap free
Amount of items: 2
Items: 
Size: 762525 Color: 1
Size: 236736 Color: 0

Bin 3840: 741 of cap free
Amount of items: 2
Items: 
Size: 708327 Color: 0
Size: 290933 Color: 1

Bin 3841: 742 of cap free
Amount of items: 2
Items: 
Size: 568985 Color: 0
Size: 430274 Color: 1

Bin 3842: 743 of cap free
Amount of items: 2
Items: 
Size: 704104 Color: 1
Size: 295154 Color: 0

Bin 3843: 744 of cap free
Amount of items: 2
Items: 
Size: 535840 Color: 0
Size: 463417 Color: 1

Bin 3844: 744 of cap free
Amount of items: 2
Items: 
Size: 665828 Color: 1
Size: 333429 Color: 0

Bin 3845: 745 of cap free
Amount of items: 2
Items: 
Size: 655984 Color: 0
Size: 343272 Color: 1

Bin 3846: 749 of cap free
Amount of items: 2
Items: 
Size: 557134 Color: 0
Size: 442118 Color: 1

Bin 3847: 749 of cap free
Amount of items: 2
Items: 
Size: 597169 Color: 1
Size: 402083 Color: 0

Bin 3848: 750 of cap free
Amount of items: 2
Items: 
Size: 562663 Color: 0
Size: 436588 Color: 1

Bin 3849: 751 of cap free
Amount of items: 2
Items: 
Size: 774253 Color: 1
Size: 224997 Color: 0

Bin 3850: 752 of cap free
Amount of items: 2
Items: 
Size: 787727 Color: 1
Size: 211522 Color: 0

Bin 3851: 753 of cap free
Amount of items: 2
Items: 
Size: 674748 Color: 1
Size: 324500 Color: 0

Bin 3852: 759 of cap free
Amount of items: 2
Items: 
Size: 744906 Color: 0
Size: 254336 Color: 1

Bin 3853: 762 of cap free
Amount of items: 2
Items: 
Size: 662511 Color: 0
Size: 336728 Color: 1

Bin 3854: 763 of cap free
Amount of items: 2
Items: 
Size: 717062 Color: 0
Size: 282176 Color: 1

Bin 3855: 764 of cap free
Amount of items: 2
Items: 
Size: 752342 Color: 1
Size: 246895 Color: 0

Bin 3856: 766 of cap free
Amount of items: 2
Items: 
Size: 726676 Color: 1
Size: 272559 Color: 0

Bin 3857: 767 of cap free
Amount of items: 2
Items: 
Size: 624709 Color: 1
Size: 374525 Color: 0

Bin 3858: 774 of cap free
Amount of items: 2
Items: 
Size: 510405 Color: 1
Size: 488822 Color: 0

Bin 3859: 774 of cap free
Amount of items: 2
Items: 
Size: 774465 Color: 0
Size: 224762 Color: 1

Bin 3860: 777 of cap free
Amount of items: 2
Items: 
Size: 728240 Color: 0
Size: 270984 Color: 1

Bin 3861: 780 of cap free
Amount of items: 2
Items: 
Size: 520331 Color: 0
Size: 478890 Color: 1

Bin 3862: 784 of cap free
Amount of items: 2
Items: 
Size: 723541 Color: 1
Size: 275676 Color: 0

Bin 3863: 785 of cap free
Amount of items: 2
Items: 
Size: 754443 Color: 0
Size: 244773 Color: 1

Bin 3864: 785 of cap free
Amount of items: 2
Items: 
Size: 591610 Color: 0
Size: 407606 Color: 1

Bin 3865: 785 of cap free
Amount of items: 2
Items: 
Size: 501385 Color: 0
Size: 497831 Color: 1

Bin 3866: 786 of cap free
Amount of items: 2
Items: 
Size: 521216 Color: 1
Size: 477999 Color: 0

Bin 3867: 786 of cap free
Amount of items: 2
Items: 
Size: 661231 Color: 1
Size: 337984 Color: 0

Bin 3868: 787 of cap free
Amount of items: 2
Items: 
Size: 698723 Color: 1
Size: 300491 Color: 0

Bin 3869: 788 of cap free
Amount of items: 2
Items: 
Size: 546590 Color: 0
Size: 452623 Color: 1

Bin 3870: 788 of cap free
Amount of items: 2
Items: 
Size: 747831 Color: 1
Size: 251382 Color: 0

Bin 3871: 789 of cap free
Amount of items: 2
Items: 
Size: 649455 Color: 1
Size: 349757 Color: 0

Bin 3872: 793 of cap free
Amount of items: 2
Items: 
Size: 673483 Color: 1
Size: 325725 Color: 0

Bin 3873: 794 of cap free
Amount of items: 2
Items: 
Size: 557102 Color: 0
Size: 442105 Color: 1

Bin 3874: 795 of cap free
Amount of items: 2
Items: 
Size: 785617 Color: 0
Size: 213589 Color: 1

Bin 3875: 797 of cap free
Amount of items: 2
Items: 
Size: 506861 Color: 1
Size: 492343 Color: 0

Bin 3876: 797 of cap free
Amount of items: 2
Items: 
Size: 683541 Color: 0
Size: 315663 Color: 1

Bin 3877: 798 of cap free
Amount of items: 2
Items: 
Size: 755559 Color: 0
Size: 243644 Color: 1

Bin 3878: 799 of cap free
Amount of items: 2
Items: 
Size: 708503 Color: 1
Size: 290699 Color: 0

Bin 3879: 805 of cap free
Amount of items: 2
Items: 
Size: 623394 Color: 0
Size: 375802 Color: 1

Bin 3880: 806 of cap free
Amount of items: 2
Items: 
Size: 728014 Color: 1
Size: 271181 Color: 0

Bin 3881: 807 of cap free
Amount of items: 2
Items: 
Size: 718115 Color: 0
Size: 281079 Color: 1

Bin 3882: 808 of cap free
Amount of items: 2
Items: 
Size: 788692 Color: 1
Size: 210501 Color: 0

Bin 3883: 810 of cap free
Amount of items: 2
Items: 
Size: 720592 Color: 1
Size: 278599 Color: 0

Bin 3884: 811 of cap free
Amount of items: 2
Items: 
Size: 704046 Color: 1
Size: 295144 Color: 0

Bin 3885: 815 of cap free
Amount of items: 2
Items: 
Size: 727077 Color: 0
Size: 272109 Color: 1

Bin 3886: 817 of cap free
Amount of items: 2
Items: 
Size: 768548 Color: 1
Size: 230636 Color: 0

Bin 3887: 820 of cap free
Amount of items: 2
Items: 
Size: 578900 Color: 1
Size: 420281 Color: 0

Bin 3888: 821 of cap free
Amount of items: 2
Items: 
Size: 618375 Color: 1
Size: 380805 Color: 0

Bin 3889: 825 of cap free
Amount of items: 2
Items: 
Size: 750761 Color: 1
Size: 248415 Color: 0

Bin 3890: 826 of cap free
Amount of items: 2
Items: 
Size: 760912 Color: 1
Size: 238263 Color: 0

Bin 3891: 826 of cap free
Amount of items: 2
Items: 
Size: 600668 Color: 1
Size: 398507 Color: 0

Bin 3892: 829 of cap free
Amount of items: 2
Items: 
Size: 754297 Color: 1
Size: 244875 Color: 0

Bin 3893: 833 of cap free
Amount of items: 2
Items: 
Size: 669364 Color: 0
Size: 329804 Color: 1

Bin 3894: 834 of cap free
Amount of items: 2
Items: 
Size: 714386 Color: 0
Size: 284781 Color: 1

Bin 3895: 835 of cap free
Amount of items: 2
Items: 
Size: 765765 Color: 0
Size: 233401 Color: 1

Bin 3896: 837 of cap free
Amount of items: 2
Items: 
Size: 704044 Color: 0
Size: 295120 Color: 1

Bin 3897: 838 of cap free
Amount of items: 2
Items: 
Size: 533634 Color: 0
Size: 465529 Color: 1

Bin 3898: 838 of cap free
Amount of items: 2
Items: 
Size: 588605 Color: 1
Size: 410558 Color: 0

Bin 3899: 839 of cap free
Amount of items: 2
Items: 
Size: 646350 Color: 0
Size: 352812 Color: 1

Bin 3900: 844 of cap free
Amount of items: 2
Items: 
Size: 653345 Color: 1
Size: 345812 Color: 0

Bin 3901: 844 of cap free
Amount of items: 2
Items: 
Size: 710609 Color: 1
Size: 288548 Color: 0

Bin 3902: 847 of cap free
Amount of items: 2
Items: 
Size: 506842 Color: 1
Size: 492312 Color: 0

Bin 3903: 848 of cap free
Amount of items: 2
Items: 
Size: 577901 Color: 0
Size: 421252 Color: 1

Bin 3904: 851 of cap free
Amount of items: 2
Items: 
Size: 793251 Color: 0
Size: 205899 Color: 1

Bin 3905: 851 of cap free
Amount of items: 2
Items: 
Size: 557067 Color: 0
Size: 442083 Color: 1

Bin 3906: 851 of cap free
Amount of items: 2
Items: 
Size: 598962 Color: 0
Size: 400188 Color: 1

Bin 3907: 855 of cap free
Amount of items: 2
Items: 
Size: 516442 Color: 0
Size: 482704 Color: 1

Bin 3908: 856 of cap free
Amount of items: 2
Items: 
Size: 785291 Color: 1
Size: 213854 Color: 0

Bin 3909: 856 of cap free
Amount of items: 2
Items: 
Size: 554569 Color: 0
Size: 444576 Color: 1

Bin 3910: 858 of cap free
Amount of items: 2
Items: 
Size: 672099 Color: 1
Size: 327044 Color: 0

Bin 3911: 859 of cap free
Amount of items: 2
Items: 
Size: 631494 Color: 0
Size: 367648 Color: 1

Bin 3912: 861 of cap free
Amount of items: 2
Items: 
Size: 623386 Color: 0
Size: 375754 Color: 1

Bin 3913: 861 of cap free
Amount of items: 2
Items: 
Size: 674713 Color: 1
Size: 324427 Color: 0

Bin 3914: 863 of cap free
Amount of items: 2
Items: 
Size: 571709 Color: 0
Size: 427429 Color: 1

Bin 3915: 864 of cap free
Amount of items: 2
Items: 
Size: 697833 Color: 1
Size: 301304 Color: 0

Bin 3916: 867 of cap free
Amount of items: 2
Items: 
Size: 791378 Color: 0
Size: 207756 Color: 1

Bin 3917: 870 of cap free
Amount of items: 2
Items: 
Size: 520293 Color: 0
Size: 478838 Color: 1

Bin 3918: 870 of cap free
Amount of items: 2
Items: 
Size: 683528 Color: 0
Size: 315603 Color: 1

Bin 3919: 875 of cap free
Amount of items: 2
Items: 
Size: 512762 Color: 0
Size: 486364 Color: 1

Bin 3920: 876 of cap free
Amount of items: 2
Items: 
Size: 620849 Color: 0
Size: 378276 Color: 1

Bin 3921: 877 of cap free
Amount of items: 2
Items: 
Size: 501202 Color: 1
Size: 497922 Color: 0

Bin 3922: 880 of cap free
Amount of items: 2
Items: 
Size: 645576 Color: 1
Size: 353545 Color: 0

Bin 3923: 882 of cap free
Amount of items: 2
Items: 
Size: 744913 Color: 1
Size: 254206 Color: 0

Bin 3924: 883 of cap free
Amount of items: 2
Items: 
Size: 654206 Color: 0
Size: 344912 Color: 1

Bin 3925: 885 of cap free
Amount of items: 2
Items: 
Size: 797146 Color: 0
Size: 201970 Color: 1

Bin 3926: 887 of cap free
Amount of items: 2
Items: 
Size: 635964 Color: 0
Size: 363150 Color: 1

Bin 3927: 895 of cap free
Amount of items: 2
Items: 
Size: 785551 Color: 0
Size: 213555 Color: 1

Bin 3928: 897 of cap free
Amount of items: 2
Items: 
Size: 704022 Color: 1
Size: 295082 Color: 0

Bin 3929: 898 of cap free
Amount of items: 2
Items: 
Size: 529323 Color: 0
Size: 469780 Color: 1

Bin 3930: 898 of cap free
Amount of items: 2
Items: 
Size: 671057 Color: 0
Size: 328046 Color: 1

Bin 3931: 901 of cap free
Amount of items: 2
Items: 
Size: 768025 Color: 0
Size: 231075 Color: 1

Bin 3932: 906 of cap free
Amount of items: 2
Items: 
Size: 688027 Color: 0
Size: 311068 Color: 1

Bin 3933: 906 of cap free
Amount of items: 2
Items: 
Size: 545595 Color: 1
Size: 453500 Color: 0

Bin 3934: 910 of cap free
Amount of items: 2
Items: 
Size: 774373 Color: 0
Size: 224718 Color: 1

Bin 3935: 910 of cap free
Amount of items: 2
Items: 
Size: 628181 Color: 1
Size: 370910 Color: 0

Bin 3936: 914 of cap free
Amount of items: 2
Items: 
Size: 511989 Color: 1
Size: 487098 Color: 0

Bin 3937: 917 of cap free
Amount of items: 2
Items: 
Size: 656325 Color: 1
Size: 342759 Color: 0

Bin 3938: 919 of cap free
Amount of items: 2
Items: 
Size: 775741 Color: 1
Size: 223341 Color: 0

Bin 3939: 921 of cap free
Amount of items: 2
Items: 
Size: 759692 Color: 1
Size: 239388 Color: 0

Bin 3940: 922 of cap free
Amount of items: 2
Items: 
Size: 645555 Color: 1
Size: 353524 Color: 0

Bin 3941: 923 of cap free
Amount of items: 2
Items: 
Size: 514749 Color: 1
Size: 484329 Color: 0

Bin 3942: 927 of cap free
Amount of items: 3
Items: 
Size: 394403 Color: 0
Size: 387011 Color: 0
Size: 217660 Color: 1

Bin 3943: 930 of cap free
Amount of items: 2
Items: 
Size: 631440 Color: 0
Size: 367631 Color: 1

Bin 3944: 935 of cap free
Amount of items: 2
Items: 
Size: 635960 Color: 0
Size: 363106 Color: 1

Bin 3945: 936 of cap free
Amount of items: 2
Items: 
Size: 785530 Color: 0
Size: 213535 Color: 1

Bin 3946: 936 of cap free
Amount of items: 2
Items: 
Size: 593904 Color: 1
Size: 405161 Color: 0

Bin 3947: 937 of cap free
Amount of items: 2
Items: 
Size: 546500 Color: 0
Size: 452564 Color: 1

Bin 3948: 939 of cap free
Amount of items: 2
Items: 
Size: 654154 Color: 0
Size: 344908 Color: 1

Bin 3949: 940 of cap free
Amount of items: 2
Items: 
Size: 652228 Color: 0
Size: 346833 Color: 1

Bin 3950: 949 of cap free
Amount of items: 2
Items: 
Size: 549548 Color: 1
Size: 449504 Color: 0

Bin 3951: 951 of cap free
Amount of items: 2
Items: 
Size: 507826 Color: 0
Size: 491224 Color: 1

Bin 3952: 952 of cap free
Amount of items: 2
Items: 
Size: 779647 Color: 1
Size: 219402 Color: 0

Bin 3953: 956 of cap free
Amount of items: 2
Items: 
Size: 523829 Color: 1
Size: 475216 Color: 0

Bin 3954: 956 of cap free
Amount of items: 2
Items: 
Size: 529304 Color: 0
Size: 469741 Color: 1

Bin 3955: 958 of cap free
Amount of items: 2
Items: 
Size: 647914 Color: 1
Size: 351129 Color: 0

Bin 3956: 961 of cap free
Amount of items: 2
Items: 
Size: 747753 Color: 1
Size: 251287 Color: 0

Bin 3957: 964 of cap free
Amount of items: 2
Items: 
Size: 548013 Color: 1
Size: 451024 Color: 0

Bin 3958: 968 of cap free
Amount of items: 2
Items: 
Size: 638406 Color: 0
Size: 360627 Color: 1

Bin 3959: 980 of cap free
Amount of items: 2
Items: 
Size: 680398 Color: 1
Size: 318623 Color: 0

Bin 3960: 980 of cap free
Amount of items: 2
Items: 
Size: 716086 Color: 1
Size: 282935 Color: 0

Bin 3961: 981 of cap free
Amount of items: 2
Items: 
Size: 777241 Color: 1
Size: 221779 Color: 0

Bin 3962: 981 of cap free
Amount of items: 2
Items: 
Size: 564104 Color: 1
Size: 434916 Color: 0

Bin 3963: 982 of cap free
Amount of items: 2
Items: 
Size: 598842 Color: 0
Size: 400177 Color: 1

Bin 3964: 982 of cap free
Amount of items: 2
Items: 
Size: 686770 Color: 1
Size: 312249 Color: 0

Bin 3965: 983 of cap free
Amount of items: 2
Items: 
Size: 521325 Color: 0
Size: 477693 Color: 1

Bin 3966: 985 of cap free
Amount of items: 2
Items: 
Size: 727905 Color: 1
Size: 271111 Color: 0

Bin 3967: 986 of cap free
Amount of items: 2
Items: 
Size: 630730 Color: 1
Size: 368285 Color: 0

Bin 3968: 987 of cap free
Amount of items: 2
Items: 
Size: 743717 Color: 0
Size: 255297 Color: 1

Bin 3969: 989 of cap free
Amount of items: 2
Items: 
Size: 520283 Color: 0
Size: 478729 Color: 1

Bin 3970: 990 of cap free
Amount of items: 2
Items: 
Size: 735009 Color: 0
Size: 264002 Color: 1

Bin 3971: 992 of cap free
Amount of items: 2
Items: 
Size: 562310 Color: 1
Size: 436699 Color: 0

Bin 3972: 994 of cap free
Amount of items: 2
Items: 
Size: 584437 Color: 1
Size: 414570 Color: 0

Bin 3973: 994 of cap free
Amount of items: 2
Items: 
Size: 629960 Color: 0
Size: 369047 Color: 1

Bin 3974: 996 of cap free
Amount of items: 2
Items: 
Size: 517181 Color: 1
Size: 481824 Color: 0

Bin 3975: 997 of cap free
Amount of items: 2
Items: 
Size: 622639 Color: 1
Size: 376365 Color: 0

Bin 3976: 998 of cap free
Amount of items: 2
Items: 
Size: 549516 Color: 1
Size: 449487 Color: 0

Bin 3977: 1001 of cap free
Amount of items: 2
Items: 
Size: 710165 Color: 0
Size: 288835 Color: 1

Bin 3978: 1003 of cap free
Amount of items: 2
Items: 
Size: 539048 Color: 0
Size: 459950 Color: 1

Bin 3979: 1004 of cap free
Amount of items: 2
Items: 
Size: 651160 Color: 1
Size: 347837 Color: 0

Bin 3980: 1004 of cap free
Amount of items: 2
Items: 
Size: 762442 Color: 0
Size: 236555 Color: 1

Bin 3981: 1013 of cap free
Amount of items: 2
Items: 
Size: 512685 Color: 0
Size: 486303 Color: 1

Bin 3982: 1014 of cap free
Amount of items: 2
Items: 
Size: 511942 Color: 1
Size: 487045 Color: 0

Bin 3983: 1014 of cap free
Amount of items: 2
Items: 
Size: 577899 Color: 0
Size: 421088 Color: 1

Bin 3984: 1015 of cap free
Amount of items: 2
Items: 
Size: 604723 Color: 0
Size: 394263 Color: 1

Bin 3985: 1015 of cap free
Amount of items: 2
Items: 
Size: 535735 Color: 0
Size: 463251 Color: 1

Bin 3986: 1018 of cap free
Amount of items: 2
Items: 
Size: 789781 Color: 0
Size: 209202 Color: 1

Bin 3987: 1018 of cap free
Amount of items: 2
Items: 
Size: 529024 Color: 1
Size: 469959 Color: 0

Bin 3988: 1021 of cap free
Amount of items: 2
Items: 
Size: 586239 Color: 0
Size: 412741 Color: 1

Bin 3989: 1027 of cap free
Amount of items: 2
Items: 
Size: 777208 Color: 1
Size: 221766 Color: 0

Bin 3990: 1027 of cap free
Amount of items: 2
Items: 
Size: 586576 Color: 1
Size: 412398 Color: 0

Bin 3991: 1031 of cap free
Amount of items: 2
Items: 
Size: 714362 Color: 0
Size: 284608 Color: 1

Bin 3992: 1031 of cap free
Amount of items: 2
Items: 
Size: 598830 Color: 0
Size: 400140 Color: 1

Bin 3993: 1036 of cap free
Amount of items: 2
Items: 
Size: 737236 Color: 0
Size: 261729 Color: 1

Bin 3994: 1037 of cap free
Amount of items: 2
Items: 
Size: 771496 Color: 0
Size: 227468 Color: 1

Bin 3995: 1037 of cap free
Amount of items: 2
Items: 
Size: 644202 Color: 0
Size: 354762 Color: 1

Bin 3996: 1042 of cap free
Amount of items: 2
Items: 
Size: 628135 Color: 1
Size: 370824 Color: 0

Bin 3997: 1044 of cap free
Amount of items: 2
Items: 
Size: 630673 Color: 1
Size: 368284 Color: 0

Bin 3998: 1045 of cap free
Amount of items: 2
Items: 
Size: 553891 Color: 1
Size: 445065 Color: 0

Bin 3999: 1046 of cap free
Amount of items: 2
Items: 
Size: 729865 Color: 0
Size: 269090 Color: 1

Bin 4000: 1047 of cap free
Amount of items: 2
Items: 
Size: 723980 Color: 0
Size: 274974 Color: 1

Bin 4001: 1053 of cap free
Amount of items: 2
Items: 
Size: 674562 Color: 1
Size: 324386 Color: 0

Bin 4002: 1054 of cap free
Amount of items: 2
Items: 
Size: 510247 Color: 1
Size: 488700 Color: 0

Bin 4003: 1056 of cap free
Amount of items: 2
Items: 
Size: 700306 Color: 1
Size: 298639 Color: 0

Bin 4004: 1059 of cap free
Amount of items: 2
Items: 
Size: 545196 Color: 0
Size: 453746 Color: 1

Bin 4005: 1064 of cap free
Amount of items: 2
Items: 
Size: 631312 Color: 0
Size: 367625 Color: 1

Bin 4006: 1065 of cap free
Amount of items: 2
Items: 
Size: 588679 Color: 0
Size: 410257 Color: 1

Bin 4007: 1076 of cap free
Amount of items: 2
Items: 
Size: 644195 Color: 0
Size: 354730 Color: 1

Bin 4008: 1077 of cap free
Amount of items: 2
Items: 
Size: 567884 Color: 0
Size: 431040 Color: 1

Bin 4009: 1077 of cap free
Amount of items: 2
Items: 
Size: 765052 Color: 1
Size: 233872 Color: 0

Bin 4010: 1079 of cap free
Amount of items: 2
Items: 
Size: 650259 Color: 0
Size: 348663 Color: 1

Bin 4011: 1082 of cap free
Amount of items: 2
Items: 
Size: 557976 Color: 0
Size: 440943 Color: 1

Bin 4012: 1086 of cap free
Amount of items: 2
Items: 
Size: 770014 Color: 0
Size: 228901 Color: 1

Bin 4013: 1090 of cap free
Amount of items: 2
Items: 
Size: 712881 Color: 0
Size: 286030 Color: 1

Bin 4014: 1090 of cap free
Amount of items: 2
Items: 
Size: 768397 Color: 1
Size: 230514 Color: 0

Bin 4015: 1091 of cap free
Amount of items: 2
Items: 
Size: 623346 Color: 0
Size: 375564 Color: 1

Bin 4016: 1095 of cap free
Amount of items: 2
Items: 
Size: 668365 Color: 1
Size: 330541 Color: 0

Bin 4017: 1095 of cap free
Amount of items: 2
Items: 
Size: 689626 Color: 1
Size: 309280 Color: 0

Bin 4018: 1096 of cap free
Amount of items: 2
Items: 
Size: 598819 Color: 0
Size: 400086 Color: 1

Bin 4019: 1096 of cap free
Amount of items: 2
Items: 
Size: 613605 Color: 1
Size: 385300 Color: 0

Bin 4020: 1098 of cap free
Amount of items: 2
Items: 
Size: 683508 Color: 0
Size: 315395 Color: 1

Bin 4021: 1101 of cap free
Amount of items: 2
Items: 
Size: 797083 Color: 0
Size: 201817 Color: 1

Bin 4022: 1103 of cap free
Amount of items: 2
Items: 
Size: 658486 Color: 1
Size: 340412 Color: 0

Bin 4023: 1104 of cap free
Amount of items: 2
Items: 
Size: 680520 Color: 0
Size: 318377 Color: 1

Bin 4024: 1105 of cap free
Amount of items: 2
Items: 
Size: 620757 Color: 1
Size: 378139 Color: 0

Bin 4025: 1108 of cap free
Amount of items: 2
Items: 
Size: 559505 Color: 0
Size: 439388 Color: 1

Bin 4026: 1110 of cap free
Amount of items: 3
Items: 
Size: 447345 Color: 0
Size: 394021 Color: 0
Size: 157525 Color: 1

Bin 4027: 1110 of cap free
Amount of items: 2
Items: 
Size: 726800 Color: 0
Size: 272091 Color: 1

Bin 4028: 1111 of cap free
Amount of items: 2
Items: 
Size: 606880 Color: 1
Size: 392010 Color: 0

Bin 4029: 1112 of cap free
Amount of items: 2
Items: 
Size: 740006 Color: 1
Size: 258883 Color: 0

Bin 4030: 1112 of cap free
Amount of items: 2
Items: 
Size: 771425 Color: 0
Size: 227464 Color: 1

Bin 4031: 1120 of cap free
Amount of items: 2
Items: 
Size: 777904 Color: 0
Size: 220977 Color: 1

Bin 4032: 1126 of cap free
Amount of items: 2
Items: 
Size: 645533 Color: 1
Size: 353342 Color: 0

Bin 4033: 1128 of cap free
Amount of items: 2
Items: 
Size: 656136 Color: 1
Size: 342737 Color: 0

Bin 4034: 1129 of cap free
Amount of items: 2
Items: 
Size: 617047 Color: 1
Size: 381825 Color: 0

Bin 4035: 1130 of cap free
Amount of items: 2
Items: 
Size: 511885 Color: 1
Size: 486986 Color: 0

Bin 4036: 1135 of cap free
Amount of items: 2
Items: 
Size: 664199 Color: 0
Size: 334667 Color: 1

Bin 4037: 1140 of cap free
Amount of items: 2
Items: 
Size: 727882 Color: 1
Size: 270979 Color: 0

Bin 4038: 1149 of cap free
Amount of items: 2
Items: 
Size: 583045 Color: 1
Size: 415807 Color: 0

Bin 4039: 1150 of cap free
Amount of items: 2
Items: 
Size: 647437 Color: 0
Size: 351414 Color: 1

Bin 4040: 1151 of cap free
Amount of items: 2
Items: 
Size: 600386 Color: 1
Size: 398464 Color: 0

Bin 4041: 1151 of cap free
Amount of items: 2
Items: 
Size: 632860 Color: 1
Size: 365990 Color: 0

Bin 4042: 1154 of cap free
Amount of items: 2
Items: 
Size: 797201 Color: 1
Size: 201646 Color: 0

Bin 4043: 1155 of cap free
Amount of items: 2
Items: 
Size: 532007 Color: 1
Size: 466839 Color: 0

Bin 4044: 1157 of cap free
Amount of items: 2
Items: 
Size: 562263 Color: 0
Size: 436581 Color: 1

Bin 4045: 1166 of cap free
Amount of items: 2
Items: 
Size: 650174 Color: 0
Size: 348661 Color: 1

Bin 4046: 1167 of cap free
Amount of items: 2
Items: 
Size: 600544 Color: 0
Size: 398290 Color: 1

Bin 4047: 1170 of cap free
Amount of items: 2
Items: 
Size: 756341 Color: 1
Size: 242490 Color: 0

Bin 4048: 1176 of cap free
Amount of items: 2
Items: 
Size: 595632 Color: 0
Size: 403193 Color: 1

Bin 4049: 1177 of cap free
Amount of items: 2
Items: 
Size: 692552 Color: 0
Size: 306272 Color: 1

Bin 4050: 1182 of cap free
Amount of items: 2
Items: 
Size: 598754 Color: 0
Size: 400065 Color: 1

Bin 4051: 1194 of cap free
Amount of items: 2
Items: 
Size: 613080 Color: 0
Size: 385727 Color: 1

Bin 4052: 1194 of cap free
Amount of items: 2
Items: 
Size: 717846 Color: 0
Size: 280961 Color: 1

Bin 4053: 1195 of cap free
Amount of items: 2
Items: 
Size: 797177 Color: 1
Size: 201629 Color: 0

Bin 4054: 1200 of cap free
Amount of items: 2
Items: 
Size: 622500 Color: 1
Size: 376301 Color: 0

Bin 4055: 1203 of cap free
Amount of items: 2
Items: 
Size: 526647 Color: 1
Size: 472151 Color: 0

Bin 4056: 1206 of cap free
Amount of items: 2
Items: 
Size: 723894 Color: 0
Size: 274901 Color: 1

Bin 4057: 1207 of cap free
Amount of items: 2
Items: 
Size: 664158 Color: 0
Size: 334636 Color: 1

Bin 4058: 1208 of cap free
Amount of items: 2
Items: 
Size: 608033 Color: 1
Size: 390760 Color: 0

Bin 4059: 1211 of cap free
Amount of items: 2
Items: 
Size: 634180 Color: 1
Size: 364610 Color: 0

Bin 4060: 1212 of cap free
Amount of items: 2
Items: 
Size: 772385 Color: 1
Size: 226404 Color: 0

Bin 4061: 1215 of cap free
Amount of items: 2
Items: 
Size: 652978 Color: 1
Size: 345808 Color: 0

Bin 4062: 1216 of cap free
Amount of items: 2
Items: 
Size: 695768 Color: 1
Size: 303017 Color: 0

Bin 4063: 1216 of cap free
Amount of items: 2
Items: 
Size: 608031 Color: 1
Size: 390754 Color: 0

Bin 4064: 1219 of cap free
Amount of items: 2
Items: 
Size: 685064 Color: 1
Size: 313718 Color: 0

Bin 4065: 1222 of cap free
Amount of items: 2
Items: 
Size: 680518 Color: 0
Size: 318261 Color: 1

Bin 4066: 1232 of cap free
Amount of items: 2
Items: 
Size: 560647 Color: 1
Size: 438122 Color: 0

Bin 4067: 1238 of cap free
Amount of items: 2
Items: 
Size: 684730 Color: 0
Size: 314033 Color: 1

Bin 4068: 1241 of cap free
Amount of items: 2
Items: 
Size: 683469 Color: 0
Size: 315291 Color: 1

Bin 4069: 1245 of cap free
Amount of items: 2
Items: 
Size: 676149 Color: 0
Size: 322607 Color: 1

Bin 4070: 1247 of cap free
Amount of items: 2
Items: 
Size: 586176 Color: 0
Size: 412578 Color: 1

Bin 4071: 1251 of cap free
Amount of items: 2
Items: 
Size: 734971 Color: 0
Size: 263779 Color: 1

Bin 4072: 1255 of cap free
Amount of items: 2
Items: 
Size: 669222 Color: 0
Size: 329524 Color: 1

Bin 4073: 1263 of cap free
Amount of items: 2
Items: 
Size: 656025 Color: 1
Size: 342713 Color: 0

Bin 4074: 1267 of cap free
Amount of items: 2
Items: 
Size: 526661 Color: 0
Size: 472073 Color: 1

Bin 4075: 1272 of cap free
Amount of items: 2
Items: 
Size: 715908 Color: 1
Size: 282821 Color: 0

Bin 4076: 1275 of cap free
Amount of items: 2
Items: 
Size: 613437 Color: 1
Size: 385289 Color: 0

Bin 4077: 1276 of cap free
Amount of items: 2
Items: 
Size: 541848 Color: 1
Size: 456877 Color: 0

Bin 4078: 1283 of cap free
Amount of items: 2
Items: 
Size: 512442 Color: 0
Size: 486276 Color: 1

Bin 4079: 1283 of cap free
Amount of items: 2
Items: 
Size: 670980 Color: 0
Size: 327738 Color: 1

Bin 4080: 1284 of cap free
Amount of items: 2
Items: 
Size: 785336 Color: 0
Size: 213381 Color: 1

Bin 4081: 1284 of cap free
Amount of items: 2
Items: 
Size: 511774 Color: 1
Size: 486943 Color: 0

Bin 4082: 1287 of cap free
Amount of items: 2
Items: 
Size: 516978 Color: 1
Size: 481736 Color: 0

Bin 4083: 1288 of cap free
Amount of items: 2
Items: 
Size: 510040 Color: 1
Size: 488673 Color: 0

Bin 4084: 1290 of cap free
Amount of items: 2
Items: 
Size: 760313 Color: 0
Size: 238398 Color: 1

Bin 4085: 1292 of cap free
Amount of items: 2
Items: 
Size: 501093 Color: 0
Size: 497616 Color: 1

Bin 4086: 1292 of cap free
Amount of items: 2
Items: 
Size: 710099 Color: 0
Size: 288610 Color: 1

Bin 4087: 1301 of cap free
Amount of items: 2
Items: 
Size: 554255 Color: 0
Size: 444445 Color: 1

Bin 4088: 1303 of cap free
Amount of items: 2
Items: 
Size: 756309 Color: 1
Size: 242389 Color: 0

Bin 4089: 1305 of cap free
Amount of items: 2
Items: 
Size: 686716 Color: 1
Size: 311980 Color: 0

Bin 4090: 1306 of cap free
Amount of items: 2
Items: 
Size: 750749 Color: 1
Size: 247946 Color: 0

Bin 4091: 1313 of cap free
Amount of items: 2
Items: 
Size: 781504 Color: 1
Size: 217184 Color: 0

Bin 4092: 1313 of cap free
Amount of items: 2
Items: 
Size: 737197 Color: 0
Size: 261491 Color: 1

Bin 4093: 1314 of cap free
Amount of items: 2
Items: 
Size: 511004 Color: 0
Size: 487683 Color: 1

Bin 4094: 1318 of cap free
Amount of items: 2
Items: 
Size: 644138 Color: 0
Size: 354545 Color: 1

Bin 4095: 1320 of cap free
Amount of items: 2
Items: 
Size: 734970 Color: 0
Size: 263711 Color: 1

Bin 4096: 1334 of cap free
Amount of items: 2
Items: 
Size: 511747 Color: 1
Size: 486920 Color: 0

Bin 4097: 1336 of cap free
Amount of items: 2
Items: 
Size: 647808 Color: 1
Size: 350857 Color: 0

Bin 4098: 1343 of cap free
Amount of items: 2
Items: 
Size: 696593 Color: 0
Size: 302065 Color: 1

Bin 4099: 1356 of cap free
Amount of items: 2
Items: 
Size: 635958 Color: 0
Size: 362687 Color: 1

Bin 4100: 1361 of cap free
Amount of items: 2
Items: 
Size: 593374 Color: 1
Size: 405266 Color: 0

Bin 4101: 1363 of cap free
Amount of items: 2
Items: 
Size: 506717 Color: 1
Size: 491921 Color: 0

Bin 4102: 1374 of cap free
Amount of items: 2
Items: 
Size: 523731 Color: 1
Size: 474896 Color: 0

Bin 4103: 1376 of cap free
Amount of items: 2
Items: 
Size: 550160 Color: 0
Size: 448465 Color: 1

Bin 4104: 1388 of cap free
Amount of items: 2
Items: 
Size: 694063 Color: 0
Size: 304550 Color: 1

Bin 4105: 1393 of cap free
Amount of items: 2
Items: 
Size: 535401 Color: 1
Size: 463207 Color: 0

Bin 4106: 1399 of cap free
Amount of items: 2
Items: 
Size: 700029 Color: 1
Size: 298573 Color: 0

Bin 4107: 1400 of cap free
Amount of items: 2
Items: 
Size: 743323 Color: 0
Size: 255278 Color: 1

Bin 4108: 1403 of cap free
Amount of items: 2
Items: 
Size: 695660 Color: 1
Size: 302938 Color: 0

Bin 4109: 1403 of cap free
Amount of items: 2
Items: 
Size: 562251 Color: 0
Size: 436347 Color: 1

Bin 4110: 1410 of cap free
Amount of items: 2
Items: 
Size: 769855 Color: 0
Size: 228736 Color: 1

Bin 4111: 1413 of cap free
Amount of items: 2
Items: 
Size: 766283 Color: 1
Size: 232305 Color: 0

Bin 4112: 1414 of cap free
Amount of items: 2
Items: 
Size: 551183 Color: 1
Size: 447404 Color: 0

Bin 4113: 1414 of cap free
Amount of items: 2
Items: 
Size: 622446 Color: 1
Size: 376141 Color: 0

Bin 4114: 1414 of cap free
Amount of items: 2
Items: 
Size: 644106 Color: 0
Size: 354481 Color: 1

Bin 4115: 1421 of cap free
Amount of items: 2
Items: 
Size: 506672 Color: 1
Size: 491908 Color: 0

Bin 4116: 1422 of cap free
Amount of items: 2
Items: 
Size: 708321 Color: 0
Size: 290258 Color: 1

Bin 4117: 1423 of cap free
Amount of items: 2
Items: 
Size: 574103 Color: 1
Size: 424475 Color: 0

Bin 4118: 1423 of cap free
Amount of items: 2
Items: 
Size: 608096 Color: 0
Size: 390482 Color: 1

Bin 4119: 1429 of cap free
Amount of items: 2
Items: 
Size: 680383 Color: 0
Size: 318189 Color: 1

Bin 4120: 1450 of cap free
Amount of items: 2
Items: 
Size: 750680 Color: 1
Size: 247871 Color: 0

Bin 4121: 1451 of cap free
Amount of items: 2
Items: 
Size: 545327 Color: 1
Size: 453223 Color: 0

Bin 4122: 1452 of cap free
Amount of items: 2
Items: 
Size: 566216 Color: 1
Size: 432333 Color: 0

Bin 4123: 1455 of cap free
Amount of items: 2
Items: 
Size: 511697 Color: 1
Size: 486849 Color: 0

Bin 4124: 1455 of cap free
Amount of items: 2
Items: 
Size: 703531 Color: 1
Size: 295015 Color: 0

Bin 4125: 1457 of cap free
Amount of items: 2
Items: 
Size: 772167 Color: 1
Size: 226377 Color: 0

Bin 4126: 1460 of cap free
Amount of items: 2
Items: 
Size: 796529 Color: 0
Size: 202012 Color: 1

Bin 4127: 1462 of cap free
Amount of items: 2
Items: 
Size: 767737 Color: 0
Size: 230802 Color: 1

Bin 4128: 1464 of cap free
Amount of items: 2
Items: 
Size: 506664 Color: 1
Size: 491873 Color: 0

Bin 4129: 1468 of cap free
Amount of items: 2
Items: 
Size: 783638 Color: 0
Size: 214895 Color: 1

Bin 4130: 1470 of cap free
Amount of items: 2
Items: 
Size: 537955 Color: 1
Size: 460576 Color: 0

Bin 4131: 1472 of cap free
Amount of items: 2
Items: 
Size: 669042 Color: 0
Size: 329487 Color: 1

Bin 4132: 1477 of cap free
Amount of items: 2
Items: 
Size: 706218 Color: 1
Size: 292306 Color: 0

Bin 4133: 1479 of cap free
Amount of items: 2
Items: 
Size: 688048 Color: 0
Size: 310474 Color: 1

Bin 4134: 1480 of cap free
Amount of items: 2
Items: 
Size: 726738 Color: 0
Size: 271783 Color: 1

Bin 4135: 1493 of cap free
Amount of items: 2
Items: 
Size: 670923 Color: 0
Size: 327585 Color: 1

Bin 4136: 1496 of cap free
Amount of items: 2
Items: 
Size: 649900 Color: 0
Size: 348605 Color: 1

Bin 4137: 1503 of cap free
Amount of items: 2
Items: 
Size: 779235 Color: 0
Size: 219263 Color: 1

Bin 4138: 1508 of cap free
Amount of items: 2
Items: 
Size: 537953 Color: 1
Size: 460540 Color: 0

Bin 4139: 1510 of cap free
Amount of items: 2
Items: 
Size: 580914 Color: 0
Size: 417577 Color: 1

Bin 4140: 1510 of cap free
Amount of items: 2
Items: 
Size: 706204 Color: 1
Size: 292287 Color: 0

Bin 4141: 1515 of cap free
Amount of items: 2
Items: 
Size: 533885 Color: 1
Size: 464601 Color: 0

Bin 4142: 1517 of cap free
Amount of items: 2
Items: 
Size: 590937 Color: 0
Size: 407547 Color: 1

Bin 4143: 1522 of cap free
Amount of items: 2
Items: 
Size: 740689 Color: 0
Size: 257790 Color: 1

Bin 4144: 1525 of cap free
Amount of items: 2
Items: 
Size: 743153 Color: 1
Size: 255323 Color: 0

Bin 4145: 1528 of cap free
Amount of items: 2
Items: 
Size: 606523 Color: 0
Size: 391950 Color: 1

Bin 4146: 1532 of cap free
Amount of items: 2
Items: 
Size: 723729 Color: 0
Size: 274740 Color: 1

Bin 4147: 1533 of cap free
Amount of items: 2
Items: 
Size: 616831 Color: 1
Size: 381637 Color: 0

Bin 4148: 1534 of cap free
Amount of items: 2
Items: 
Size: 797172 Color: 1
Size: 201295 Color: 0

Bin 4149: 1534 of cap free
Amount of items: 2
Items: 
Size: 632495 Color: 1
Size: 365972 Color: 0

Bin 4150: 1535 of cap free
Amount of items: 2
Items: 
Size: 760298 Color: 0
Size: 238168 Color: 1

Bin 4151: 1537 of cap free
Amount of items: 2
Items: 
Size: 635940 Color: 0
Size: 362524 Color: 1

Bin 4152: 1537 of cap free
Amount of items: 2
Items: 
Size: 559451 Color: 0
Size: 439013 Color: 1

Bin 4153: 1538 of cap free
Amount of items: 2
Items: 
Size: 779575 Color: 1
Size: 218888 Color: 0

Bin 4154: 1546 of cap free
Amount of items: 2
Items: 
Size: 737152 Color: 0
Size: 261303 Color: 1

Bin 4155: 1553 of cap free
Amount of items: 2
Items: 
Size: 578370 Color: 1
Size: 420078 Color: 0

Bin 4156: 1554 of cap free
Amount of items: 2
Items: 
Size: 517999 Color: 0
Size: 480448 Color: 1

Bin 4157: 1554 of cap free
Amount of items: 2
Items: 
Size: 776714 Color: 1
Size: 221733 Color: 0

Bin 4158: 1559 of cap free
Amount of items: 2
Items: 
Size: 596893 Color: 1
Size: 401549 Color: 0

Bin 4159: 1561 of cap free
Amount of items: 2
Items: 
Size: 554007 Color: 0
Size: 444433 Color: 1

Bin 4160: 1563 of cap free
Amount of items: 2
Items: 
Size: 798892 Color: 0
Size: 199546 Color: 1

Bin 4161: 1564 of cap free
Amount of items: 2
Items: 
Size: 658319 Color: 1
Size: 340118 Color: 0

Bin 4162: 1565 of cap free
Amount of items: 2
Items: 
Size: 773828 Color: 0
Size: 224608 Color: 1

Bin 4163: 1568 of cap free
Amount of items: 2
Items: 
Size: 781459 Color: 1
Size: 216974 Color: 0

Bin 4164: 1582 of cap free
Amount of items: 2
Items: 
Size: 647673 Color: 1
Size: 350746 Color: 0

Bin 4165: 1591 of cap free
Amount of items: 2
Items: 
Size: 590916 Color: 0
Size: 407494 Color: 1

Bin 4166: 1598 of cap free
Amount of items: 2
Items: 
Size: 665019 Color: 1
Size: 333384 Color: 0

Bin 4167: 1605 of cap free
Amount of items: 2
Items: 
Size: 580833 Color: 0
Size: 417563 Color: 1

Bin 4168: 1606 of cap free
Amount of items: 2
Items: 
Size: 737148 Color: 0
Size: 261247 Color: 1

Bin 4169: 1606 of cap free
Amount of items: 2
Items: 
Size: 559402 Color: 0
Size: 438993 Color: 1

Bin 4170: 1609 of cap free
Amount of items: 2
Items: 
Size: 551032 Color: 1
Size: 447360 Color: 0

Bin 4171: 1612 of cap free
Amount of items: 2
Items: 
Size: 686478 Color: 1
Size: 311911 Color: 0

Bin 4172: 1628 of cap free
Amount of items: 2
Items: 
Size: 586195 Color: 0
Size: 412178 Color: 1

Bin 4173: 1633 of cap free
Amount of items: 2
Items: 
Size: 783653 Color: 1
Size: 214715 Color: 0

Bin 4174: 1650 of cap free
Amount of items: 2
Items: 
Size: 590876 Color: 0
Size: 407475 Color: 1

Bin 4175: 1652 of cap free
Amount of items: 2
Items: 
Size: 560236 Color: 1
Size: 438113 Color: 0

Bin 4176: 1655 of cap free
Amount of items: 2
Items: 
Size: 684745 Color: 1
Size: 313601 Color: 0

Bin 4177: 1668 of cap free
Amount of items: 2
Items: 
Size: 783481 Color: 0
Size: 214852 Color: 1

Bin 4178: 1671 of cap free
Amount of items: 2
Items: 
Size: 635879 Color: 0
Size: 362451 Color: 1

Bin 4179: 1673 of cap free
Amount of items: 2
Items: 
Size: 679834 Color: 1
Size: 318494 Color: 0

Bin 4180: 1675 of cap free
Amount of items: 2
Items: 
Size: 545292 Color: 1
Size: 453034 Color: 0

Bin 4181: 1675 of cap free
Amount of items: 2
Items: 
Size: 712382 Color: 0
Size: 285944 Color: 1

Bin 4182: 1684 of cap free
Amount of items: 2
Items: 
Size: 503202 Color: 1
Size: 495115 Color: 0

Bin 4183: 1694 of cap free
Amount of items: 2
Items: 
Size: 729328 Color: 0
Size: 268979 Color: 1

Bin 4184: 1701 of cap free
Amount of items: 2
Items: 
Size: 545268 Color: 1
Size: 453032 Color: 0

Bin 4185: 1712 of cap free
Amount of items: 2
Items: 
Size: 668851 Color: 0
Size: 329438 Color: 1

Bin 4186: 1746 of cap free
Amount of items: 2
Items: 
Size: 797037 Color: 1
Size: 201218 Color: 0

Bin 4187: 1747 of cap free
Amount of items: 2
Items: 
Size: 563425 Color: 1
Size: 434829 Color: 0

Bin 4188: 1750 of cap free
Amount of items: 2
Items: 
Size: 600500 Color: 0
Size: 397751 Color: 1

Bin 4189: 1752 of cap free
Amount of items: 2
Items: 
Size: 562137 Color: 0
Size: 436112 Color: 1

Bin 4190: 1772 of cap free
Amount of items: 2
Items: 
Size: 574066 Color: 1
Size: 424163 Color: 0

Bin 4191: 1776 of cap free
Amount of items: 2
Items: 
Size: 767461 Color: 0
Size: 230764 Color: 1

Bin 4192: 1795 of cap free
Amount of items: 2
Items: 
Size: 562113 Color: 0
Size: 436093 Color: 1

Bin 4193: 1810 of cap free
Amount of items: 2
Items: 
Size: 687407 Color: 0
Size: 310784 Color: 1

Bin 4194: 1819 of cap free
Amount of items: 2
Items: 
Size: 712327 Color: 0
Size: 285855 Color: 1

Bin 4195: 1828 of cap free
Amount of items: 2
Items: 
Size: 695534 Color: 1
Size: 302639 Color: 0

Bin 4196: 1839 of cap free
Amount of items: 2
Items: 
Size: 684585 Color: 1
Size: 313577 Color: 0

Bin 4197: 1840 of cap free
Amount of items: 2
Items: 
Size: 578345 Color: 1
Size: 419816 Color: 0

Bin 4198: 1848 of cap free
Amount of items: 2
Items: 
Size: 796394 Color: 0
Size: 201759 Color: 1

Bin 4199: 1854 of cap free
Amount of items: 2
Items: 
Size: 724409 Color: 1
Size: 273738 Color: 0

Bin 4200: 1856 of cap free
Amount of items: 2
Items: 
Size: 745795 Color: 0
Size: 252350 Color: 1

Bin 4201: 1857 of cap free
Amount of items: 2
Items: 
Size: 599776 Color: 1
Size: 398368 Color: 0

Bin 4202: 1860 of cap free
Amount of items: 2
Items: 
Size: 538916 Color: 0
Size: 459225 Color: 1

Bin 4203: 1866 of cap free
Amount of items: 2
Items: 
Size: 667723 Color: 1
Size: 330412 Color: 0

Bin 4204: 1872 of cap free
Amount of items: 2
Items: 
Size: 749757 Color: 0
Size: 248372 Color: 1

Bin 4205: 1885 of cap free
Amount of items: 2
Items: 
Size: 747730 Color: 1
Size: 250386 Color: 0

Bin 4206: 1889 of cap free
Amount of items: 2
Items: 
Size: 723628 Color: 0
Size: 274484 Color: 1

Bin 4207: 1890 of cap free
Amount of items: 2
Items: 
Size: 779520 Color: 1
Size: 218591 Color: 0

Bin 4208: 1891 of cap free
Amount of items: 2
Items: 
Size: 595630 Color: 0
Size: 402480 Color: 1

Bin 4209: 1892 of cap free
Amount of items: 2
Items: 
Size: 686431 Color: 1
Size: 311678 Color: 0

Bin 4210: 1897 of cap free
Amount of items: 3
Items: 
Size: 593966 Color: 1
Size: 246795 Color: 0
Size: 157343 Color: 1

Bin 4211: 1902 of cap free
Amount of items: 2
Items: 
Size: 696211 Color: 0
Size: 301888 Color: 1

Bin 4212: 1921 of cap free
Amount of items: 2
Items: 
Size: 703449 Color: 1
Size: 294631 Color: 0

Bin 4213: 1926 of cap free
Amount of items: 2
Items: 
Size: 735413 Color: 1
Size: 262662 Color: 0

Bin 4214: 1927 of cap free
Amount of items: 2
Items: 
Size: 507795 Color: 0
Size: 490279 Color: 1

Bin 4215: 1930 of cap free
Amount of items: 3
Items: 
Size: 548953 Color: 0
Size: 261168 Color: 0
Size: 187950 Color: 1

Bin 4216: 1932 of cap free
Amount of items: 2
Items: 
Size: 578271 Color: 1
Size: 419798 Color: 0

Bin 4217: 1934 of cap free
Amount of items: 2
Items: 
Size: 526642 Color: 1
Size: 471425 Color: 0

Bin 4218: 1937 of cap free
Amount of items: 2
Items: 
Size: 595571 Color: 0
Size: 402493 Color: 1

Bin 4219: 1955 of cap free
Amount of items: 2
Items: 
Size: 655599 Color: 1
Size: 342447 Color: 0

Bin 4220: 1957 of cap free
Amount of items: 2
Items: 
Size: 767381 Color: 0
Size: 230663 Color: 1

Bin 4221: 1964 of cap free
Amount of items: 2
Items: 
Size: 739280 Color: 1
Size: 258757 Color: 0

Bin 4222: 1968 of cap free
Amount of items: 2
Items: 
Size: 796918 Color: 1
Size: 201115 Color: 0

Bin 4223: 1972 of cap free
Amount of items: 2
Items: 
Size: 798890 Color: 1
Size: 199139 Color: 0

Bin 4224: 1989 of cap free
Amount of items: 2
Items: 
Size: 578252 Color: 1
Size: 419760 Color: 0

Bin 4225: 1992 of cap free
Amount of items: 3
Items: 
Size: 771753 Color: 1
Size: 117441 Color: 1
Size: 108815 Color: 0

Bin 4226: 2016 of cap free
Amount of items: 2
Items: 
Size: 742744 Color: 0
Size: 255241 Color: 1

Bin 4227: 2022 of cap free
Amount of items: 2
Items: 
Size: 771761 Color: 1
Size: 226218 Color: 0

Bin 4228: 2028 of cap free
Amount of items: 2
Items: 
Size: 747656 Color: 1
Size: 250317 Color: 0

Bin 4229: 2033 of cap free
Amount of items: 2
Items: 
Size: 658308 Color: 1
Size: 339660 Color: 0

Bin 4230: 2034 of cap free
Amount of items: 2
Items: 
Size: 798822 Color: 0
Size: 199145 Color: 1

Bin 4231: 2041 of cap free
Amount of items: 2
Items: 
Size: 511335 Color: 1
Size: 486625 Color: 0

Bin 4232: 2059 of cap free
Amount of items: 2
Items: 
Size: 703430 Color: 1
Size: 294512 Color: 0

Bin 4233: 2063 of cap free
Amount of items: 2
Items: 
Size: 538722 Color: 0
Size: 459216 Color: 1

Bin 4234: 2064 of cap free
Amount of items: 2
Items: 
Size: 664700 Color: 1
Size: 333237 Color: 0

Bin 4235: 2072 of cap free
Amount of items: 2
Items: 
Size: 547311 Color: 1
Size: 450618 Color: 0

Bin 4236: 2077 of cap free
Amount of items: 2
Items: 
Size: 649819 Color: 0
Size: 348105 Color: 1

Bin 4237: 2078 of cap free
Amount of items: 2
Items: 
Size: 560215 Color: 1
Size: 437708 Color: 0

Bin 4238: 2083 of cap free
Amount of items: 2
Items: 
Size: 696202 Color: 0
Size: 301716 Color: 1

Bin 4239: 2089 of cap free
Amount of items: 2
Items: 
Size: 583902 Color: 0
Size: 414010 Color: 1

Bin 4240: 2089 of cap free
Amount of items: 2
Items: 
Size: 635694 Color: 0
Size: 362218 Color: 1

Bin 4241: 2098 of cap free
Amount of items: 2
Items: 
Size: 614778 Color: 0
Size: 383125 Color: 1

Bin 4242: 2099 of cap free
Amount of items: 2
Items: 
Size: 580256 Color: 1
Size: 417646 Color: 0

Bin 4243: 2106 of cap free
Amount of items: 2
Items: 
Size: 655490 Color: 1
Size: 342405 Color: 0

Bin 4244: 2109 of cap free
Amount of items: 2
Items: 
Size: 603697 Color: 0
Size: 394195 Color: 1

Bin 4245: 2113 of cap free
Amount of items: 2
Items: 
Size: 612546 Color: 0
Size: 385342 Color: 1

Bin 4246: 2119 of cap free
Amount of items: 2
Items: 
Size: 537813 Color: 1
Size: 460069 Color: 0

Bin 4247: 2119 of cap free
Amount of items: 2
Items: 
Size: 556956 Color: 0
Size: 440926 Color: 1

Bin 4248: 2123 of cap free
Amount of items: 2
Items: 
Size: 547306 Color: 1
Size: 450572 Color: 0

Bin 4249: 2132 of cap free
Amount of items: 2
Items: 
Size: 661210 Color: 1
Size: 336659 Color: 0

Bin 4250: 2137 of cap free
Amount of items: 2
Items: 
Size: 506022 Color: 1
Size: 491842 Color: 0

Bin 4251: 2150 of cap free
Amount of items: 2
Items: 
Size: 614764 Color: 0
Size: 383087 Color: 1

Bin 4252: 2153 of cap free
Amount of items: 2
Items: 
Size: 767189 Color: 0
Size: 230659 Color: 1

Bin 4253: 2155 of cap free
Amount of items: 2
Items: 
Size: 712157 Color: 0
Size: 285689 Color: 1

Bin 4254: 2159 of cap free
Amount of items: 2
Items: 
Size: 628084 Color: 1
Size: 369758 Color: 0

Bin 4255: 2169 of cap free
Amount of items: 2
Items: 
Size: 655465 Color: 1
Size: 342367 Color: 0

Bin 4256: 2171 of cap free
Amount of items: 2
Items: 
Size: 773465 Color: 0
Size: 224365 Color: 1

Bin 4257: 2174 of cap free
Amount of items: 2
Items: 
Size: 655808 Color: 0
Size: 342019 Color: 1

Bin 4258: 2189 of cap free
Amount of items: 2
Items: 
Size: 755572 Color: 1
Size: 242240 Color: 0

Bin 4259: 2205 of cap free
Amount of items: 2
Items: 
Size: 751817 Color: 0
Size: 245979 Color: 1

Bin 4260: 2219 of cap free
Amount of items: 2
Items: 
Size: 526382 Color: 1
Size: 471400 Color: 0

Bin 4261: 2230 of cap free
Amount of items: 2
Items: 
Size: 577508 Color: 0
Size: 420263 Color: 1

Bin 4262: 2232 of cap free
Amount of items: 2
Items: 
Size: 723390 Color: 0
Size: 274379 Color: 1

Bin 4263: 2242 of cap free
Amount of items: 2
Items: 
Size: 583867 Color: 0
Size: 413892 Color: 1

Bin 4264: 2255 of cap free
Amount of items: 2
Items: 
Size: 692469 Color: 1
Size: 305277 Color: 0

Bin 4265: 2261 of cap free
Amount of items: 2
Items: 
Size: 664144 Color: 0
Size: 333596 Color: 1

Bin 4266: 2266 of cap free
Amount of items: 2
Items: 
Size: 727740 Color: 1
Size: 269995 Color: 0

Bin 4267: 2273 of cap free
Amount of items: 2
Items: 
Size: 755630 Color: 1
Size: 242098 Color: 0

Bin 4268: 2299 of cap free
Amount of items: 2
Items: 
Size: 664107 Color: 0
Size: 333595 Color: 1

Bin 4269: 2301 of cap free
Amount of items: 2
Items: 
Size: 696138 Color: 0
Size: 301562 Color: 1

Bin 4270: 2308 of cap free
Amount of items: 2
Items: 
Size: 577443 Color: 0
Size: 420250 Color: 1

Bin 4271: 2309 of cap free
Amount of items: 2
Items: 
Size: 649771 Color: 0
Size: 347921 Color: 1

Bin 4272: 2312 of cap free
Amount of items: 2
Items: 
Size: 553186 Color: 1
Size: 444503 Color: 0

Bin 4273: 2329 of cap free
Amount of items: 2
Items: 
Size: 647112 Color: 1
Size: 350560 Color: 0

Bin 4274: 2333 of cap free
Amount of items: 2
Items: 
Size: 700007 Color: 1
Size: 297661 Color: 0

Bin 4275: 2357 of cap free
Amount of items: 2
Items: 
Size: 742408 Color: 0
Size: 255236 Color: 1

Bin 4276: 2362 of cap free
Amount of items: 2
Items: 
Size: 532943 Color: 0
Size: 464696 Color: 1

Bin 4277: 2369 of cap free
Amount of items: 2
Items: 
Size: 522767 Color: 0
Size: 474865 Color: 1

Bin 4278: 2385 of cap free
Amount of items: 2
Items: 
Size: 776825 Color: 0
Size: 220791 Color: 1

Bin 4279: 2391 of cap free
Amount of items: 2
Items: 
Size: 655375 Color: 1
Size: 342235 Color: 0

Bin 4280: 2399 of cap free
Amount of items: 2
Items: 
Size: 599531 Color: 1
Size: 398071 Color: 0

Bin 4281: 2412 of cap free
Amount of items: 2
Items: 
Size: 727735 Color: 1
Size: 269854 Color: 0

Bin 4282: 2427 of cap free
Amount of items: 2
Items: 
Size: 595453 Color: 0
Size: 402121 Color: 1

Bin 4283: 2433 of cap free
Amount of items: 2
Items: 
Size: 526371 Color: 1
Size: 471197 Color: 0

Bin 4284: 2445 of cap free
Amount of items: 2
Items: 
Size: 717018 Color: 0
Size: 280538 Color: 1

Bin 4285: 2445 of cap free
Amount of items: 2
Items: 
Size: 600063 Color: 0
Size: 397493 Color: 1

Bin 4286: 2448 of cap free
Amount of items: 2
Items: 
Size: 567805 Color: 0
Size: 429748 Color: 1

Bin 4287: 2456 of cap free
Amount of items: 2
Items: 
Size: 732322 Color: 1
Size: 265223 Color: 0

Bin 4288: 2460 of cap free
Amount of items: 2
Items: 
Size: 538520 Color: 0
Size: 459021 Color: 1

Bin 4289: 2463 of cap free
Amount of items: 2
Items: 
Size: 599502 Color: 1
Size: 398036 Color: 0

Bin 4290: 2466 of cap free
Amount of items: 2
Items: 
Size: 751646 Color: 0
Size: 245889 Color: 1

Bin 4291: 2467 of cap free
Amount of items: 2
Items: 
Size: 545176 Color: 0
Size: 452358 Color: 1

Bin 4292: 2502 of cap free
Amount of items: 2
Items: 
Size: 739176 Color: 1
Size: 258323 Color: 0

Bin 4293: 2517 of cap free
Amount of items: 2
Items: 
Size: 590326 Color: 0
Size: 407158 Color: 1

Bin 4294: 2520 of cap free
Amount of items: 2
Items: 
Size: 733975 Color: 0
Size: 263506 Color: 1

Bin 4295: 2522 of cap free
Amount of items: 2
Items: 
Size: 603474 Color: 0
Size: 394005 Color: 1

Bin 4296: 2522 of cap free
Amount of items: 2
Items: 
Size: 513950 Color: 1
Size: 483529 Color: 0

Bin 4297: 2549 of cap free
Amount of items: 2
Items: 
Size: 596081 Color: 1
Size: 401371 Color: 0

Bin 4298: 2553 of cap free
Amount of items: 2
Items: 
Size: 595435 Color: 0
Size: 402013 Color: 1

Bin 4299: 2553 of cap free
Amount of items: 2
Items: 
Size: 646057 Color: 0
Size: 351391 Color: 1

Bin 4300: 2561 of cap free
Amount of items: 2
Items: 
Size: 529226 Color: 0
Size: 468214 Color: 1

Bin 4301: 2564 of cap free
Amount of items: 2
Items: 
Size: 627989 Color: 1
Size: 369448 Color: 0

Bin 4302: 2576 of cap free
Amount of items: 2
Items: 
Size: 621951 Color: 0
Size: 375474 Color: 1

Bin 4303: 2583 of cap free
Amount of items: 2
Items: 
Size: 599486 Color: 1
Size: 397932 Color: 0

Bin 4304: 2585 of cap free
Amount of items: 2
Items: 
Size: 687151 Color: 0
Size: 310265 Color: 1

Bin 4305: 2598 of cap free
Amount of items: 2
Items: 
Size: 759558 Color: 0
Size: 237845 Color: 1

Bin 4306: 2625 of cap free
Amount of items: 2
Items: 
Size: 507237 Color: 0
Size: 490139 Color: 1

Bin 4307: 2637 of cap free
Amount of items: 2
Items: 
Size: 549323 Color: 0
Size: 448041 Color: 1

Bin 4308: 2646 of cap free
Amount of items: 2
Items: 
Size: 529177 Color: 0
Size: 468178 Color: 1

Bin 4309: 2651 of cap free
Amount of items: 2
Items: 
Size: 629801 Color: 0
Size: 367549 Color: 1

Bin 4310: 2657 of cap free
Amount of items: 2
Items: 
Size: 627946 Color: 1
Size: 369398 Color: 0

Bin 4311: 2662 of cap free
Amount of items: 2
Items: 
Size: 716873 Color: 0
Size: 280466 Color: 1

Bin 4312: 2669 of cap free
Amount of items: 2
Items: 
Size: 529175 Color: 0
Size: 468157 Color: 1

Bin 4313: 2689 of cap free
Amount of items: 2
Items: 
Size: 745715 Color: 0
Size: 251597 Color: 1

Bin 4314: 2691 of cap free
Amount of items: 2
Items: 
Size: 577279 Color: 0
Size: 420031 Color: 1

Bin 4315: 2704 of cap free
Amount of items: 2
Items: 
Size: 782473 Color: 0
Size: 214824 Color: 1

Bin 4316: 2705 of cap free
Amount of items: 2
Items: 
Size: 711649 Color: 0
Size: 285647 Color: 1

Bin 4317: 2715 of cap free
Amount of items: 2
Items: 
Size: 553709 Color: 0
Size: 443577 Color: 1

Bin 4318: 2721 of cap free
Amount of items: 2
Items: 
Size: 506010 Color: 1
Size: 491270 Color: 0

Bin 4319: 2725 of cap free
Amount of items: 2
Items: 
Size: 595296 Color: 0
Size: 401980 Color: 1

Bin 4320: 2729 of cap free
Amount of items: 2
Items: 
Size: 627895 Color: 1
Size: 369377 Color: 0

Bin 4321: 2748 of cap free
Amount of items: 2
Items: 
Size: 655634 Color: 0
Size: 341619 Color: 1

Bin 4322: 2749 of cap free
Amount of items: 2
Items: 
Size: 629799 Color: 0
Size: 367453 Color: 1

Bin 4323: 2769 of cap free
Amount of items: 2
Items: 
Size: 755566 Color: 1
Size: 241666 Color: 0

Bin 4324: 2774 of cap free
Amount of items: 2
Items: 
Size: 637892 Color: 1
Size: 359335 Color: 0

Bin 4325: 2794 of cap free
Amount of items: 2
Items: 
Size: 658167 Color: 1
Size: 339040 Color: 0

Bin 4326: 2825 of cap free
Amount of items: 2
Items: 
Size: 785302 Color: 0
Size: 211874 Color: 1

Bin 4327: 2829 of cap free
Amount of items: 2
Items: 
Size: 695999 Color: 0
Size: 301173 Color: 1

Bin 4328: 2844 of cap free
Amount of items: 2
Items: 
Size: 529013 Color: 0
Size: 468144 Color: 1

Bin 4329: 2846 of cap free
Amount of items: 2
Items: 
Size: 798804 Color: 0
Size: 198351 Color: 1

Bin 4330: 2847 of cap free
Amount of items: 2
Items: 
Size: 505988 Color: 1
Size: 491166 Color: 0

Bin 4331: 2849 of cap free
Amount of items: 2
Items: 
Size: 722916 Color: 0
Size: 274236 Color: 1

Bin 4332: 2884 of cap free
Amount of items: 2
Items: 
Size: 510881 Color: 0
Size: 486236 Color: 1

Bin 4333: 2906 of cap free
Amount of items: 2
Items: 
Size: 785291 Color: 0
Size: 211804 Color: 1

Bin 4334: 2929 of cap free
Amount of items: 2
Items: 
Size: 763270 Color: 1
Size: 233802 Color: 0

Bin 4335: 2946 of cap free
Amount of items: 2
Items: 
Size: 667070 Color: 1
Size: 329985 Color: 0

Bin 4336: 2969 of cap free
Amount of items: 2
Items: 
Size: 637628 Color: 1
Size: 359404 Color: 0

Bin 4337: 2978 of cap free
Amount of items: 3
Items: 
Size: 423770 Color: 0
Size: 381803 Color: 1
Size: 191450 Color: 0

Bin 4338: 2987 of cap free
Amount of items: 2
Items: 
Size: 714833 Color: 1
Size: 282181 Color: 0

Bin 4339: 2990 of cap free
Amount of items: 2
Items: 
Size: 702621 Color: 1
Size: 294390 Color: 0

Bin 4340: 2998 of cap free
Amount of items: 2
Items: 
Size: 505932 Color: 1
Size: 491071 Color: 0

Bin 4341: 3017 of cap free
Amount of items: 2
Items: 
Size: 533805 Color: 1
Size: 463179 Color: 0

Bin 4342: 3021 of cap free
Amount of items: 2
Items: 
Size: 499542 Color: 1
Size: 497438 Color: 0

Bin 4343: 3024 of cap free
Amount of items: 2
Items: 
Size: 576997 Color: 0
Size: 419980 Color: 1

Bin 4344: 3063 of cap free
Amount of items: 2
Items: 
Size: 679205 Color: 1
Size: 317733 Color: 0

Bin 4345: 3082 of cap free
Amount of items: 2
Items: 
Size: 553537 Color: 0
Size: 443382 Color: 1

Bin 4346: 3088 of cap free
Amount of items: 2
Items: 
Size: 775494 Color: 1
Size: 221419 Color: 0

Bin 4347: 3096 of cap free
Amount of items: 2
Items: 
Size: 666923 Color: 1
Size: 329982 Color: 0

Bin 4348: 3106 of cap free
Amount of items: 2
Items: 
Size: 663543 Color: 0
Size: 333352 Color: 1

Bin 4349: 3122 of cap free
Amount of items: 2
Items: 
Size: 525817 Color: 1
Size: 471062 Color: 0

Bin 4350: 3123 of cap free
Amount of items: 2
Items: 
Size: 742797 Color: 1
Size: 254081 Color: 0

Bin 4351: 3136 of cap free
Amount of items: 2
Items: 
Size: 572825 Color: 1
Size: 424040 Color: 0

Bin 4352: 3144 of cap free
Amount of items: 2
Items: 
Size: 706166 Color: 1
Size: 290691 Color: 0

Bin 4353: 3150 of cap free
Amount of items: 2
Items: 
Size: 708267 Color: 0
Size: 288584 Color: 1

Bin 4354: 3240 of cap free
Amount of items: 2
Items: 
Size: 714828 Color: 1
Size: 281933 Color: 0

Bin 4355: 3247 of cap free
Amount of items: 2
Items: 
Size: 679082 Color: 1
Size: 317672 Color: 0

Bin 4356: 3272 of cap free
Amount of items: 2
Items: 
Size: 506747 Color: 0
Size: 489982 Color: 1

Bin 4357: 3275 of cap free
Amount of items: 2
Items: 
Size: 723177 Color: 1
Size: 273549 Color: 0

Bin 4358: 3279 of cap free
Amount of items: 2
Items: 
Size: 708233 Color: 0
Size: 288489 Color: 1

Bin 4359: 3288 of cap free
Amount of items: 2
Items: 
Size: 663505 Color: 0
Size: 333208 Color: 1

Bin 4360: 3308 of cap free
Amount of items: 2
Items: 
Size: 722866 Color: 0
Size: 273827 Color: 1

Bin 4361: 3341 of cap free
Amount of items: 2
Items: 
Size: 533630 Color: 1
Size: 463030 Color: 0

Bin 4362: 3403 of cap free
Amount of items: 2
Items: 
Size: 716201 Color: 0
Size: 280397 Color: 1

Bin 4363: 3442 of cap free
Amount of items: 2
Items: 
Size: 541822 Color: 1
Size: 454737 Color: 0

Bin 4364: 3462 of cap free
Amount of items: 2
Items: 
Size: 706105 Color: 1
Size: 290434 Color: 0

Bin 4365: 3488 of cap free
Amount of items: 2
Items: 
Size: 729332 Color: 0
Size: 267181 Color: 1

Bin 4366: 3511 of cap free
Amount of items: 2
Items: 
Size: 682852 Color: 0
Size: 313638 Color: 1

Bin 4367: 3534 of cap free
Amount of items: 2
Items: 
Size: 582470 Color: 1
Size: 413997 Color: 0

Bin 4368: 3540 of cap free
Amount of items: 2
Items: 
Size: 770425 Color: 1
Size: 226036 Color: 0

Bin 4369: 3600 of cap free
Amount of items: 2
Items: 
Size: 722681 Color: 0
Size: 273720 Color: 1

Bin 4370: 3616 of cap free
Amount of items: 2
Items: 
Size: 576464 Color: 0
Size: 419921 Color: 1

Bin 4371: 3621 of cap free
Amount of items: 2
Items: 
Size: 595078 Color: 1
Size: 401302 Color: 0

Bin 4372: 3640 of cap free
Amount of items: 2
Items: 
Size: 606446 Color: 0
Size: 389915 Color: 1

Bin 4373: 3647 of cap free
Amount of items: 2
Items: 
Size: 606337 Color: 0
Size: 390017 Color: 1

Bin 4374: 3700 of cap free
Amount of items: 2
Items: 
Size: 758627 Color: 0
Size: 237674 Color: 1

Bin 4375: 3724 of cap free
Amount of items: 2
Items: 
Size: 637154 Color: 1
Size: 359123 Color: 0

Bin 4376: 3734 of cap free
Amount of items: 2
Items: 
Size: 506573 Color: 0
Size: 489694 Color: 1

Bin 4377: 3747 of cap free
Amount of items: 2
Items: 
Size: 615465 Color: 1
Size: 380789 Color: 0

Bin 4378: 3777 of cap free
Amount of items: 2
Items: 
Size: 537988 Color: 0
Size: 458236 Color: 1

Bin 4379: 3788 of cap free
Amount of items: 2
Items: 
Size: 547096 Color: 1
Size: 449117 Color: 0

Bin 4380: 3790 of cap free
Amount of items: 2
Items: 
Size: 528743 Color: 0
Size: 467468 Color: 1

Bin 4381: 3818 of cap free
Amount of items: 2
Items: 
Size: 598750 Color: 0
Size: 397433 Color: 1

Bin 4382: 3819 of cap free
Amount of items: 2
Items: 
Size: 655524 Color: 0
Size: 340658 Color: 1

Bin 4383: 3863 of cap free
Amount of items: 2
Items: 
Size: 668795 Color: 0
Size: 327343 Color: 1

Bin 4384: 3869 of cap free
Amount of items: 2
Items: 
Size: 598739 Color: 0
Size: 397393 Color: 1

Bin 4385: 3873 of cap free
Amount of items: 2
Items: 
Size: 506456 Color: 0
Size: 489672 Color: 1

Bin 4386: 3880 of cap free
Amount of items: 2
Items: 
Size: 543800 Color: 0
Size: 452321 Color: 1

Bin 4387: 3927 of cap free
Amount of items: 2
Items: 
Size: 627817 Color: 1
Size: 368257 Color: 0

Bin 4388: 3935 of cap free
Amount of items: 2
Items: 
Size: 629793 Color: 0
Size: 366273 Color: 1

Bin 4389: 3967 of cap free
Amount of items: 2
Items: 
Size: 505329 Color: 1
Size: 490705 Color: 0

Bin 4390: 3989 of cap free
Amount of items: 2
Items: 
Size: 505314 Color: 1
Size: 490698 Color: 0

Bin 4391: 4023 of cap free
Amount of items: 2
Items: 
Size: 655339 Color: 0
Size: 340639 Color: 1

Bin 4392: 4031 of cap free
Amount of items: 2
Items: 
Size: 673603 Color: 0
Size: 322367 Color: 1

Bin 4393: 4061 of cap free
Amount of items: 2
Items: 
Size: 559302 Color: 1
Size: 436638 Color: 0

Bin 4394: 4068 of cap free
Amount of items: 2
Items: 
Size: 543790 Color: 0
Size: 452143 Color: 1

Bin 4395: 4101 of cap free
Amount of items: 2
Items: 
Size: 606366 Color: 0
Size: 389534 Color: 1

Bin 4396: 4126 of cap free
Amount of items: 2
Items: 
Size: 583721 Color: 0
Size: 412154 Color: 1

Bin 4397: 4185 of cap free
Amount of items: 2
Items: 
Size: 578209 Color: 1
Size: 417607 Color: 0

Bin 4398: 4216 of cap free
Amount of items: 2
Items: 
Size: 583686 Color: 0
Size: 412099 Color: 1

Bin 4399: 4237 of cap free
Amount of items: 2
Items: 
Size: 615008 Color: 1
Size: 380756 Color: 0

Bin 4400: 4246 of cap free
Amount of items: 2
Items: 
Size: 537883 Color: 0
Size: 457872 Color: 1

Bin 4401: 4247 of cap free
Amount of items: 3
Items: 
Size: 638917 Color: 1
Size: 232267 Color: 0
Size: 124570 Color: 0

Bin 4402: 4291 of cap free
Amount of items: 2
Items: 
Size: 744835 Color: 0
Size: 250875 Color: 1

Bin 4403: 4354 of cap free
Amount of items: 2
Items: 
Size: 627812 Color: 1
Size: 367835 Color: 0

Bin 4404: 4373 of cap free
Amount of items: 2
Items: 
Size: 571530 Color: 1
Size: 424098 Color: 0

Bin 4405: 4381 of cap free
Amount of items: 2
Items: 
Size: 525799 Color: 1
Size: 469821 Color: 0

Bin 4406: 4396 of cap free
Amount of items: 2
Items: 
Size: 571488 Color: 1
Size: 424117 Color: 0

Bin 4407: 4417 of cap free
Amount of items: 2
Items: 
Size: 537777 Color: 0
Size: 457807 Color: 1

Bin 4408: 4533 of cap free
Amount of items: 2
Items: 
Size: 629667 Color: 0
Size: 365801 Color: 1

Bin 4409: 4672 of cap free
Amount of items: 3
Items: 
Size: 467107 Color: 1
Size: 350359 Color: 1
Size: 177863 Color: 0

Bin 4410: 4724 of cap free
Amount of items: 2
Items: 
Size: 583468 Color: 0
Size: 411809 Color: 1

Bin 4411: 4737 of cap free
Amount of items: 2
Items: 
Size: 778778 Color: 1
Size: 216486 Color: 0

Bin 4412: 4766 of cap free
Amount of items: 2
Items: 
Size: 612237 Color: 0
Size: 382998 Color: 1

Bin 4413: 4781 of cap free
Amount of items: 2
Items: 
Size: 798804 Color: 0
Size: 196416 Color: 1

Bin 4414: 4811 of cap free
Amount of items: 2
Items: 
Size: 583463 Color: 0
Size: 411727 Color: 1

Bin 4415: 4820 of cap free
Amount of items: 2
Items: 
Size: 586539 Color: 1
Size: 408642 Color: 0

Bin 4416: 4824 of cap free
Amount of items: 2
Items: 
Size: 754270 Color: 1
Size: 240907 Color: 0

Bin 4417: 4852 of cap free
Amount of items: 2
Items: 
Size: 588632 Color: 0
Size: 406517 Color: 1

Bin 4418: 4871 of cap free
Amount of items: 2
Items: 
Size: 614817 Color: 1
Size: 380313 Color: 0

Bin 4419: 4928 of cap free
Amount of items: 2
Items: 
Size: 612144 Color: 0
Size: 382929 Color: 1

Bin 4420: 4956 of cap free
Amount of items: 2
Items: 
Size: 614763 Color: 1
Size: 380282 Color: 0

Bin 4421: 4983 of cap free
Amount of items: 2
Items: 
Size: 598067 Color: 0
Size: 396951 Color: 1

Bin 4422: 4996 of cap free
Amount of items: 2
Items: 
Size: 655353 Color: 1
Size: 339652 Color: 0

Bin 4423: 5162 of cap free
Amount of items: 2
Items: 
Size: 525146 Color: 1
Size: 469693 Color: 0

Bin 4424: 5169 of cap free
Amount of items: 2
Items: 
Size: 541817 Color: 1
Size: 453015 Color: 0

Bin 4425: 5304 of cap free
Amount of items: 2
Items: 
Size: 531827 Color: 1
Size: 462870 Color: 0

Bin 4426: 5329 of cap free
Amount of items: 2
Items: 
Size: 505310 Color: 0
Size: 489362 Color: 1

Bin 4427: 5343 of cap free
Amount of items: 2
Items: 
Size: 783392 Color: 0
Size: 211266 Color: 1

Bin 4428: 5348 of cap free
Amount of items: 2
Items: 
Size: 537269 Color: 0
Size: 457384 Color: 1

Bin 4429: 5415 of cap free
Amount of items: 2
Items: 
Size: 733916 Color: 0
Size: 260670 Color: 1

Bin 4430: 5497 of cap free
Amount of items: 3
Items: 
Size: 395632 Color: 0
Size: 355272 Color: 0
Size: 243600 Color: 1

Bin 4431: 5504 of cap free
Amount of items: 2
Items: 
Size: 606318 Color: 0
Size: 388179 Color: 1

Bin 4432: 5821 of cap free
Amount of items: 2
Items: 
Size: 655294 Color: 1
Size: 338886 Color: 0

Bin 4433: 5838 of cap free
Amount of items: 2
Items: 
Size: 649438 Color: 0
Size: 344725 Color: 1

Bin 4434: 6080 of cap free
Amount of items: 2
Items: 
Size: 796778 Color: 1
Size: 197143 Color: 0

Bin 4435: 6081 of cap free
Amount of items: 2
Items: 
Size: 553105 Color: 0
Size: 440815 Color: 1

Bin 4436: 6209 of cap free
Amount of items: 2
Items: 
Size: 553104 Color: 0
Size: 440688 Color: 1

Bin 4437: 6251 of cap free
Amount of items: 2
Items: 
Size: 593193 Color: 1
Size: 400557 Color: 0

Bin 4438: 6422 of cap free
Amount of items: 3
Items: 
Size: 720525 Color: 1
Size: 155329 Color: 1
Size: 117725 Color: 0

Bin 4439: 6426 of cap free
Amount of items: 2
Items: 
Size: 613391 Color: 1
Size: 380184 Color: 0

Bin 4440: 6465 of cap free
Amount of items: 2
Items: 
Size: 613353 Color: 1
Size: 380183 Color: 0

Bin 4441: 6470 of cap free
Amount of items: 2
Items: 
Size: 502907 Color: 1
Size: 490624 Color: 0

Bin 4442: 6486 of cap free
Amount of items: 2
Items: 
Size: 720089 Color: 0
Size: 273426 Color: 1

Bin 4443: 6770 of cap free
Amount of items: 2
Items: 
Size: 523633 Color: 1
Size: 469598 Color: 0

Bin 4444: 7095 of cap free
Amount of items: 2
Items: 
Size: 558083 Color: 1
Size: 434823 Color: 0

Bin 4445: 7317 of cap free
Amount of items: 2
Items: 
Size: 603312 Color: 0
Size: 389372 Color: 1

Bin 4446: 7420 of cap free
Amount of items: 2
Items: 
Size: 558091 Color: 1
Size: 434490 Color: 0

Bin 4447: 7601 of cap free
Amount of items: 2
Items: 
Size: 714503 Color: 1
Size: 277897 Color: 0

Bin 4448: 7618 of cap free
Amount of items: 2
Items: 
Size: 557948 Color: 1
Size: 434435 Color: 0

Bin 4449: 7713 of cap free
Amount of items: 2
Items: 
Size: 714505 Color: 1
Size: 277783 Color: 0

Bin 4450: 7809 of cap free
Amount of items: 2
Items: 
Size: 738859 Color: 1
Size: 253333 Color: 0

Bin 4451: 7827 of cap free
Amount of items: 2
Items: 
Size: 557981 Color: 1
Size: 434193 Color: 0

Bin 4452: 8105 of cap free
Amount of items: 2
Items: 
Size: 773501 Color: 0
Size: 218395 Color: 1

Bin 4453: 8120 of cap free
Amount of items: 2
Items: 
Size: 528642 Color: 0
Size: 463239 Color: 1

Bin 4454: 8125 of cap free
Amount of items: 3
Items: 
Size: 502999 Color: 0
Size: 283874 Color: 1
Size: 205003 Color: 1

Bin 4455: 8170 of cap free
Amount of items: 2
Items: 
Size: 553116 Color: 0
Size: 438715 Color: 1

Bin 4456: 8185 of cap free
Amount of items: 2
Items: 
Size: 553064 Color: 0
Size: 438752 Color: 1

Bin 4457: 8362 of cap free
Amount of items: 2
Items: 
Size: 795615 Color: 1
Size: 196024 Color: 0

Bin 4458: 8476 of cap free
Amount of items: 2
Items: 
Size: 553012 Color: 0
Size: 438513 Color: 1

Bin 4459: 8483 of cap free
Amount of items: 2
Items: 
Size: 733731 Color: 0
Size: 257787 Color: 1

Bin 4460: 8511 of cap free
Amount of items: 2
Items: 
Size: 687168 Color: 0
Size: 304322 Color: 1

Bin 4461: 8549 of cap free
Amount of items: 2
Items: 
Size: 733086 Color: 1
Size: 258366 Color: 0

Bin 4462: 8688 of cap free
Amount of items: 2
Items: 
Size: 636496 Color: 1
Size: 354817 Color: 0

Bin 4463: 8760 of cap free
Amount of items: 2
Items: 
Size: 547055 Color: 1
Size: 444186 Color: 0

Bin 4464: 8891 of cap free
Amount of items: 2
Items: 
Size: 504890 Color: 0
Size: 486220 Color: 1

Bin 4465: 8917 of cap free
Amount of items: 2
Items: 
Size: 504871 Color: 0
Size: 486213 Color: 1

Bin 4466: 8998 of cap free
Amount of items: 2
Items: 
Size: 553062 Color: 0
Size: 437941 Color: 1

Bin 4467: 9220 of cap free
Amount of items: 2
Items: 
Size: 504572 Color: 0
Size: 486209 Color: 1

Bin 4468: 9269 of cap free
Amount of items: 2
Items: 
Size: 504558 Color: 0
Size: 486174 Color: 1

Bin 4469: 9358 of cap free
Amount of items: 2
Items: 
Size: 754330 Color: 0
Size: 236313 Color: 1

Bin 4470: 9446 of cap free
Amount of items: 2
Items: 
Size: 655208 Color: 1
Size: 335347 Color: 0

Bin 4471: 9454 of cap free
Amount of items: 2
Items: 
Size: 795430 Color: 1
Size: 195117 Color: 0

Bin 4472: 9613 of cap free
Amount of items: 2
Items: 
Size: 712866 Color: 1
Size: 277522 Color: 0

Bin 4473: 10667 of cap free
Amount of items: 2
Items: 
Size: 503418 Color: 0
Size: 485916 Color: 1

Bin 4474: 11244 of cap free
Amount of items: 2
Items: 
Size: 796497 Color: 0
Size: 192260 Color: 1

Bin 4475: 11436 of cap free
Amount of items: 2
Items: 
Size: 503415 Color: 0
Size: 485150 Color: 1

Bin 4476: 12037 of cap free
Amount of items: 2
Items: 
Size: 794957 Color: 1
Size: 193007 Color: 0

Bin 4477: 12092 of cap free
Amount of items: 2
Items: 
Size: 503120 Color: 0
Size: 484789 Color: 1

Bin 4478: 12166 of cap free
Amount of items: 2
Items: 
Size: 764347 Color: 0
Size: 223488 Color: 1

Bin 4479: 12673 of cap free
Amount of items: 2
Items: 
Size: 770462 Color: 1
Size: 216866 Color: 0

Bin 4480: 13246 of cap free
Amount of items: 2
Items: 
Size: 795749 Color: 0
Size: 191006 Color: 1

Bin 4481: 13323 of cap free
Amount of items: 2
Items: 
Size: 798893 Color: 1
Size: 187785 Color: 0

Bin 4482: 13603 of cap free
Amount of items: 2
Items: 
Size: 577944 Color: 1
Size: 408454 Color: 0

Bin 4483: 13882 of cap free
Amount of items: 2
Items: 
Size: 523494 Color: 1
Size: 462625 Color: 0

Bin 4484: 13930 of cap free
Amount of items: 2
Items: 
Size: 603163 Color: 0
Size: 382908 Color: 1

Bin 4485: 14850 of cap free
Amount of items: 2
Items: 
Size: 522329 Color: 0
Size: 462822 Color: 1

Bin 4486: 15484 of cap free
Amount of items: 2
Items: 
Size: 770670 Color: 1
Size: 213847 Color: 0

Bin 4487: 15704 of cap free
Amount of items: 2
Items: 
Size: 795716 Color: 0
Size: 188581 Color: 1

Bin 4488: 16257 of cap free
Amount of items: 2
Items: 
Size: 795377 Color: 0
Size: 188367 Color: 1

Bin 4489: 16397 of cap free
Amount of items: 2
Items: 
Size: 795329 Color: 0
Size: 188275 Color: 1

Bin 4490: 17021 of cap free
Amount of items: 2
Items: 
Size: 520195 Color: 0
Size: 462785 Color: 1

Bin 4491: 17823 of cap free
Amount of items: 2
Items: 
Size: 552854 Color: 0
Size: 429324 Color: 1

Bin 4492: 17861 of cap free
Amount of items: 2
Items: 
Size: 553005 Color: 0
Size: 429135 Color: 1

Bin 4493: 18475 of cap free
Amount of items: 2
Items: 
Size: 792607 Color: 1
Size: 188919 Color: 0

Bin 4494: 18726 of cap free
Amount of items: 2
Items: 
Size: 499387 Color: 0
Size: 481888 Color: 1

Bin 4495: 19422 of cap free
Amount of items: 2
Items: 
Size: 791390 Color: 1
Size: 189189 Color: 0

Bin 4496: 20260 of cap free
Amount of items: 2
Items: 
Size: 792482 Color: 0
Size: 187259 Color: 1

Bin 4497: 20851 of cap free
Amount of items: 2
Items: 
Size: 498936 Color: 0
Size: 480214 Color: 1

Bin 4498: 20913 of cap free
Amount of items: 2
Items: 
Size: 792368 Color: 0
Size: 186720 Color: 1

Bin 4499: 21558 of cap free
Amount of items: 2
Items: 
Size: 571392 Color: 1
Size: 407051 Color: 0

Bin 4500: 22538 of cap free
Amount of items: 2
Items: 
Size: 520187 Color: 0
Size: 457276 Color: 1

Bin 4501: 23264 of cap free
Amount of items: 3
Items: 
Size: 479271 Color: 1
Size: 392433 Color: 0
Size: 105033 Color: 0

Bin 4502: 24122 of cap free
Amount of items: 2
Items: 
Size: 787512 Color: 1
Size: 188367 Color: 0

Bin 4503: 24332 of cap free
Amount of items: 5
Items: 
Size: 195888 Color: 0
Size: 195649 Color: 0
Size: 195244 Color: 1
Size: 195038 Color: 1
Size: 193850 Color: 1

Bin 4504: 24576 of cap free
Amount of items: 2
Items: 
Size: 786494 Color: 1
Size: 188931 Color: 0

Bin 4505: 24928 of cap free
Amount of items: 2
Items: 
Size: 791292 Color: 0
Size: 183781 Color: 1

Bin 4506: 26712 of cap free
Amount of items: 5
Items: 
Size: 195626 Color: 0
Size: 195228 Color: 0
Size: 195172 Color: 0
Size: 193705 Color: 1
Size: 193558 Color: 1

Bin 4507: 29472 of cap free
Amount of items: 2
Items: 
Size: 788615 Color: 1
Size: 181914 Color: 0

Bin 4508: 32831 of cap free
Amount of items: 3
Items: 
Size: 555630 Color: 1
Size: 260090 Color: 1
Size: 151450 Color: 0

Bin 4509: 34130 of cap free
Amount of items: 2
Items: 
Size: 791189 Color: 0
Size: 174682 Color: 1

Bin 4510: 39222 of cap free
Amount of items: 2
Items: 
Size: 582640 Color: 0
Size: 378139 Color: 1

Bin 4511: 40409 of cap free
Amount of items: 2
Items: 
Size: 790991 Color: 0
Size: 168601 Color: 1

Bin 4512: 46343 of cap free
Amount of items: 2
Items: 
Size: 556691 Color: 1
Size: 396967 Color: 0

Bin 4513: 56360 of cap free
Amount of items: 2
Items: 
Size: 758173 Color: 0
Size: 185468 Color: 1

Bin 4514: 61032 of cap free
Amount of items: 2
Items: 
Size: 767859 Color: 1
Size: 171110 Color: 0

Total size: 4511154048
Total free space: 2850466

