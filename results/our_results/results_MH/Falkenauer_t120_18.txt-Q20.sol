Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 14
Size: 306 Color: 15
Size: 286 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 14
Size: 292 Color: 5
Size: 255 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 11
Size: 267 Color: 17
Size: 256 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 17
Size: 317 Color: 0
Size: 276 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 12
Size: 366 Color: 18
Size: 264 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 11
Size: 268 Color: 2
Size: 256 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 14
Size: 286 Color: 13
Size: 256 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 10
Size: 259 Color: 12
Size: 253 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 348 Color: 12
Size: 267 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 6
Size: 251 Color: 15
Size: 250 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 11
Size: 272 Color: 18
Size: 259 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 16
Size: 275 Color: 0
Size: 264 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 323 Color: 16
Size: 286 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 4
Size: 254 Color: 6
Size: 251 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 308 Color: 11
Size: 268 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 255 Color: 11
Size: 252 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 0
Size: 334 Color: 11
Size: 327 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 8
Size: 258 Color: 17
Size: 254 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 8
Size: 282 Color: 19
Size: 261 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 4
Size: 323 Color: 18
Size: 304 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 15
Size: 290 Color: 2
Size: 282 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 14
Size: 297 Color: 3
Size: 259 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 338 Color: 7
Size: 262 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 13
Size: 318 Color: 6
Size: 313 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 284 Color: 1
Size: 250 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 13
Size: 327 Color: 18
Size: 280 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 18
Size: 355 Color: 13
Size: 250 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 11
Size: 340 Color: 2
Size: 314 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 17
Size: 278 Color: 18
Size: 312 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 268 Color: 17
Size: 259 Color: 2
Size: 473 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 15
Size: 317 Color: 11
Size: 265 Color: 16

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 295 Color: 11
Size: 287 Color: 10

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 13
Size: 295 Color: 6
Size: 250 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 300 Color: 8
Size: 292 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 5
Size: 300 Color: 18
Size: 262 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 19
Size: 304 Color: 0
Size: 298 Color: 13

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 15
Size: 255 Color: 9
Size: 250 Color: 17

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 4
Size: 329 Color: 7
Size: 250 Color: 19

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 5
Size: 282 Color: 7
Size: 258 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 16
Size: 309 Color: 2
Size: 274 Color: 0

Total size: 40000
Total free space: 0

