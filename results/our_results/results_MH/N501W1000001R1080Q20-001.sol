Capicity Bin: 1000001
Lower Bound: 226

Bins used: 227
Amount of Colors: 20

Bin 1: 2 of cap free
Amount of items: 3
Items: 
Size: 725576 Color: 13
Size: 173529 Color: 4
Size: 100894 Color: 19

Bin 2: 3 of cap free
Amount of items: 3
Items: 
Size: 528091 Color: 1
Size: 361974 Color: 8
Size: 109933 Color: 16

Bin 3: 3 of cap free
Amount of items: 3
Items: 
Size: 707968 Color: 9
Size: 175933 Color: 19
Size: 116097 Color: 8

Bin 4: 5 of cap free
Amount of items: 2
Items: 
Size: 551606 Color: 0
Size: 448390 Color: 1

Bin 5: 6 of cap free
Amount of items: 3
Items: 
Size: 485454 Color: 8
Size: 382883 Color: 13
Size: 131658 Color: 14

Bin 6: 7 of cap free
Amount of items: 3
Items: 
Size: 727851 Color: 5
Size: 144458 Color: 15
Size: 127685 Color: 1

Bin 7: 8 of cap free
Amount of items: 3
Items: 
Size: 520566 Color: 12
Size: 330404 Color: 1
Size: 149023 Color: 5

Bin 8: 9 of cap free
Amount of items: 3
Items: 
Size: 524183 Color: 17
Size: 358130 Color: 3
Size: 117679 Color: 9

Bin 9: 11 of cap free
Amount of items: 3
Items: 
Size: 558254 Color: 15
Size: 276414 Color: 16
Size: 165322 Color: 18

Bin 10: 15 of cap free
Amount of items: 2
Items: 
Size: 670875 Color: 15
Size: 329111 Color: 16

Bin 11: 19 of cap free
Amount of items: 3
Items: 
Size: 536523 Color: 13
Size: 358694 Color: 19
Size: 104765 Color: 17

Bin 12: 25 of cap free
Amount of items: 3
Items: 
Size: 725252 Color: 14
Size: 138454 Color: 5
Size: 136270 Color: 9

Bin 13: 26 of cap free
Amount of items: 2
Items: 
Size: 759682 Color: 14
Size: 240293 Color: 9

Bin 14: 31 of cap free
Amount of items: 2
Items: 
Size: 520492 Color: 0
Size: 479478 Color: 10

Bin 15: 32 of cap free
Amount of items: 3
Items: 
Size: 766302 Color: 2
Size: 117071 Color: 17
Size: 116596 Color: 13

Bin 16: 34 of cap free
Amount of items: 3
Items: 
Size: 519045 Color: 10
Size: 332358 Color: 13
Size: 148564 Color: 9

Bin 17: 36 of cap free
Amount of items: 3
Items: 
Size: 624288 Color: 0
Size: 195721 Color: 19
Size: 179956 Color: 2

Bin 18: 42 of cap free
Amount of items: 3
Items: 
Size: 689488 Color: 0
Size: 187508 Color: 0
Size: 122963 Color: 14

Bin 19: 44 of cap free
Amount of items: 2
Items: 
Size: 700796 Color: 1
Size: 299161 Color: 16

Bin 20: 51 of cap free
Amount of items: 3
Items: 
Size: 643850 Color: 5
Size: 178096 Color: 9
Size: 178004 Color: 18

Bin 21: 54 of cap free
Amount of items: 3
Items: 
Size: 417499 Color: 19
Size: 409115 Color: 19
Size: 173333 Color: 16

Bin 22: 55 of cap free
Amount of items: 3
Items: 
Size: 528141 Color: 12
Size: 331226 Color: 8
Size: 140579 Color: 5

Bin 23: 58 of cap free
Amount of items: 2
Items: 
Size: 653440 Color: 5
Size: 346503 Color: 1

Bin 24: 60 of cap free
Amount of items: 3
Items: 
Size: 727174 Color: 16
Size: 165759 Color: 15
Size: 107008 Color: 6

Bin 25: 61 of cap free
Amount of items: 2
Items: 
Size: 527320 Color: 16
Size: 472620 Color: 12

Bin 26: 67 of cap free
Amount of items: 3
Items: 
Size: 528449 Color: 19
Size: 368472 Color: 9
Size: 103013 Color: 2

Bin 27: 69 of cap free
Amount of items: 3
Items: 
Size: 626365 Color: 5
Size: 189589 Color: 10
Size: 183978 Color: 9

Bin 28: 80 of cap free
Amount of items: 2
Items: 
Size: 710842 Color: 5
Size: 289079 Color: 16

Bin 29: 82 of cap free
Amount of items: 3
Items: 
Size: 646076 Color: 16
Size: 182725 Color: 13
Size: 171118 Color: 7

Bin 30: 85 of cap free
Amount of items: 3
Items: 
Size: 528320 Color: 16
Size: 329998 Color: 7
Size: 141598 Color: 18

Bin 31: 86 of cap free
Amount of items: 2
Items: 
Size: 659078 Color: 15
Size: 340837 Color: 7

Bin 32: 103 of cap free
Amount of items: 2
Items: 
Size: 501273 Color: 15
Size: 498625 Color: 2

Bin 33: 149 of cap free
Amount of items: 3
Items: 
Size: 751075 Color: 7
Size: 146474 Color: 0
Size: 102303 Color: 2

Bin 34: 150 of cap free
Amount of items: 2
Items: 
Size: 546091 Color: 7
Size: 453760 Color: 15

Bin 35: 166 of cap free
Amount of items: 2
Items: 
Size: 771690 Color: 18
Size: 228145 Color: 3

Bin 36: 167 of cap free
Amount of items: 2
Items: 
Size: 617770 Color: 16
Size: 382064 Color: 7

Bin 37: 170 of cap free
Amount of items: 3
Items: 
Size: 691022 Color: 6
Size: 163173 Color: 1
Size: 145636 Color: 6

Bin 38: 170 of cap free
Amount of items: 2
Items: 
Size: 634246 Color: 14
Size: 365585 Color: 0

Bin 39: 176 of cap free
Amount of items: 2
Items: 
Size: 550020 Color: 16
Size: 449805 Color: 12

Bin 40: 182 of cap free
Amount of items: 2
Items: 
Size: 756684 Color: 8
Size: 243135 Color: 5

Bin 41: 190 of cap free
Amount of items: 3
Items: 
Size: 686197 Color: 9
Size: 181559 Color: 11
Size: 132055 Color: 1

Bin 42: 193 of cap free
Amount of items: 3
Items: 
Size: 698313 Color: 19
Size: 182924 Color: 15
Size: 118571 Color: 1

Bin 43: 199 of cap free
Amount of items: 2
Items: 
Size: 618989 Color: 15
Size: 380813 Color: 6

Bin 44: 202 of cap free
Amount of items: 3
Items: 
Size: 618599 Color: 10
Size: 196326 Color: 5
Size: 184874 Color: 11

Bin 45: 204 of cap free
Amount of items: 3
Items: 
Size: 727551 Color: 9
Size: 139978 Color: 4
Size: 132268 Color: 0

Bin 46: 213 of cap free
Amount of items: 3
Items: 
Size: 416373 Color: 0
Size: 412701 Color: 8
Size: 170714 Color: 8

Bin 47: 216 of cap free
Amount of items: 3
Items: 
Size: 658747 Color: 19
Size: 182261 Color: 7
Size: 158777 Color: 12

Bin 48: 230 of cap free
Amount of items: 2
Items: 
Size: 732749 Color: 8
Size: 267022 Color: 2

Bin 49: 233 of cap free
Amount of items: 2
Items: 
Size: 525658 Color: 6
Size: 474110 Color: 3

Bin 50: 248 of cap free
Amount of items: 2
Items: 
Size: 569688 Color: 15
Size: 430065 Color: 14

Bin 51: 255 of cap free
Amount of items: 3
Items: 
Size: 686985 Color: 14
Size: 159578 Color: 8
Size: 153183 Color: 10

Bin 52: 256 of cap free
Amount of items: 3
Items: 
Size: 503203 Color: 1
Size: 343258 Color: 8
Size: 153284 Color: 7

Bin 53: 265 of cap free
Amount of items: 2
Items: 
Size: 558467 Color: 4
Size: 441269 Color: 2

Bin 54: 268 of cap free
Amount of items: 3
Items: 
Size: 644967 Color: 1
Size: 182750 Color: 16
Size: 172016 Color: 13

Bin 55: 275 of cap free
Amount of items: 2
Items: 
Size: 638190 Color: 8
Size: 361536 Color: 3

Bin 56: 286 of cap free
Amount of items: 3
Items: 
Size: 784894 Color: 4
Size: 114317 Color: 6
Size: 100504 Color: 8

Bin 57: 290 of cap free
Amount of items: 2
Items: 
Size: 602563 Color: 15
Size: 397148 Color: 12

Bin 58: 293 of cap free
Amount of items: 3
Items: 
Size: 502036 Color: 11
Size: 385475 Color: 19
Size: 112197 Color: 14

Bin 59: 297 of cap free
Amount of items: 2
Items: 
Size: 771883 Color: 0
Size: 227821 Color: 4

Bin 60: 308 of cap free
Amount of items: 2
Items: 
Size: 734174 Color: 18
Size: 265519 Color: 15

Bin 61: 325 of cap free
Amount of items: 2
Items: 
Size: 523162 Color: 3
Size: 476514 Color: 8

Bin 62: 330 of cap free
Amount of items: 3
Items: 
Size: 559880 Color: 10
Size: 221961 Color: 14
Size: 217830 Color: 14

Bin 63: 330 of cap free
Amount of items: 2
Items: 
Size: 658267 Color: 1
Size: 341404 Color: 10

Bin 64: 337 of cap free
Amount of items: 2
Items: 
Size: 601442 Color: 13
Size: 398222 Color: 17

Bin 65: 339 of cap free
Amount of items: 3
Items: 
Size: 523633 Color: 12
Size: 357690 Color: 4
Size: 118339 Color: 1

Bin 66: 343 of cap free
Amount of items: 2
Items: 
Size: 559154 Color: 17
Size: 440504 Color: 15

Bin 67: 347 of cap free
Amount of items: 2
Items: 
Size: 768938 Color: 19
Size: 230716 Color: 17

Bin 68: 350 of cap free
Amount of items: 2
Items: 
Size: 555619 Color: 5
Size: 444032 Color: 12

Bin 69: 368 of cap free
Amount of items: 3
Items: 
Size: 671674 Color: 3
Size: 165231 Color: 4
Size: 162728 Color: 13

Bin 70: 389 of cap free
Amount of items: 2
Items: 
Size: 564086 Color: 5
Size: 435526 Color: 13

Bin 71: 397 of cap free
Amount of items: 2
Items: 
Size: 594289 Color: 2
Size: 405315 Color: 13

Bin 72: 404 of cap free
Amount of items: 2
Items: 
Size: 764542 Color: 18
Size: 235055 Color: 0

Bin 73: 416 of cap free
Amount of items: 3
Items: 
Size: 451291 Color: 1
Size: 359891 Color: 5
Size: 188403 Color: 15

Bin 74: 438 of cap free
Amount of items: 2
Items: 
Size: 531224 Color: 3
Size: 468339 Color: 10

Bin 75: 441 of cap free
Amount of items: 2
Items: 
Size: 534470 Color: 0
Size: 465090 Color: 1

Bin 76: 443 of cap free
Amount of items: 2
Items: 
Size: 600372 Color: 15
Size: 399186 Color: 18

Bin 77: 467 of cap free
Amount of items: 2
Items: 
Size: 742853 Color: 11
Size: 256681 Color: 6

Bin 78: 510 of cap free
Amount of items: 3
Items: 
Size: 416382 Color: 3
Size: 397560 Color: 9
Size: 185549 Color: 8

Bin 79: 511 of cap free
Amount of items: 2
Items: 
Size: 512972 Color: 5
Size: 486518 Color: 8

Bin 80: 520 of cap free
Amount of items: 2
Items: 
Size: 661655 Color: 10
Size: 337826 Color: 15

Bin 81: 531 of cap free
Amount of items: 2
Items: 
Size: 645576 Color: 8
Size: 353894 Color: 16

Bin 82: 541 of cap free
Amount of items: 2
Items: 
Size: 511553 Color: 10
Size: 487907 Color: 16

Bin 83: 561 of cap free
Amount of items: 2
Items: 
Size: 767000 Color: 8
Size: 232440 Color: 1

Bin 84: 567 of cap free
Amount of items: 2
Items: 
Size: 614739 Color: 5
Size: 384695 Color: 7

Bin 85: 572 of cap free
Amount of items: 2
Items: 
Size: 753211 Color: 2
Size: 246218 Color: 6

Bin 86: 573 of cap free
Amount of items: 3
Items: 
Size: 688454 Color: 0
Size: 166073 Color: 11
Size: 144901 Color: 10

Bin 87: 574 of cap free
Amount of items: 2
Items: 
Size: 772895 Color: 1
Size: 226532 Color: 11

Bin 88: 586 of cap free
Amount of items: 2
Items: 
Size: 656353 Color: 14
Size: 343062 Color: 3

Bin 89: 590 of cap free
Amount of items: 2
Items: 
Size: 707805 Color: 9
Size: 291606 Color: 0

Bin 90: 595 of cap free
Amount of items: 2
Items: 
Size: 527186 Color: 14
Size: 472220 Color: 10

Bin 91: 606 of cap free
Amount of items: 2
Items: 
Size: 777297 Color: 0
Size: 222098 Color: 17

Bin 92: 608 of cap free
Amount of items: 2
Items: 
Size: 603116 Color: 5
Size: 396277 Color: 10

Bin 93: 630 of cap free
Amount of items: 2
Items: 
Size: 789630 Color: 4
Size: 209741 Color: 14

Bin 94: 640 of cap free
Amount of items: 2
Items: 
Size: 694255 Color: 6
Size: 305106 Color: 13

Bin 95: 644 of cap free
Amount of items: 3
Items: 
Size: 697754 Color: 11
Size: 167141 Color: 10
Size: 134462 Color: 14

Bin 96: 644 of cap free
Amount of items: 2
Items: 
Size: 723494 Color: 1
Size: 275863 Color: 18

Bin 97: 668 of cap free
Amount of items: 2
Items: 
Size: 599798 Color: 14
Size: 399535 Color: 12

Bin 98: 672 of cap free
Amount of items: 2
Items: 
Size: 773955 Color: 5
Size: 225374 Color: 18

Bin 99: 686 of cap free
Amount of items: 2
Items: 
Size: 687347 Color: 15
Size: 311968 Color: 2

Bin 100: 713 of cap free
Amount of items: 2
Items: 
Size: 619909 Color: 16
Size: 379379 Color: 2

Bin 101: 737 of cap free
Amount of items: 2
Items: 
Size: 707590 Color: 7
Size: 291674 Color: 4

Bin 102: 765 of cap free
Amount of items: 2
Items: 
Size: 775841 Color: 15
Size: 223395 Color: 9

Bin 103: 777 of cap free
Amount of items: 2
Items: 
Size: 693183 Color: 10
Size: 306041 Color: 5

Bin 104: 787 of cap free
Amount of items: 2
Items: 
Size: 565641 Color: 2
Size: 433573 Color: 9

Bin 105: 834 of cap free
Amount of items: 2
Items: 
Size: 696326 Color: 9
Size: 302841 Color: 7

Bin 106: 838 of cap free
Amount of items: 2
Items: 
Size: 499765 Color: 8
Size: 499398 Color: 10

Bin 107: 871 of cap free
Amount of items: 3
Items: 
Size: 560931 Color: 9
Size: 275737 Color: 18
Size: 162462 Color: 16

Bin 108: 878 of cap free
Amount of items: 2
Items: 
Size: 621121 Color: 3
Size: 378002 Color: 4

Bin 109: 887 of cap free
Amount of items: 2
Items: 
Size: 562958 Color: 17
Size: 436156 Color: 16

Bin 110: 895 of cap free
Amount of items: 2
Items: 
Size: 662657 Color: 14
Size: 336449 Color: 0

Bin 111: 900 of cap free
Amount of items: 2
Items: 
Size: 514634 Color: 14
Size: 484467 Color: 6

Bin 112: 968 of cap free
Amount of items: 2
Items: 
Size: 735544 Color: 17
Size: 263489 Color: 15

Bin 113: 974 of cap free
Amount of items: 2
Items: 
Size: 585808 Color: 17
Size: 413219 Color: 0

Bin 114: 982 of cap free
Amount of items: 2
Items: 
Size: 629380 Color: 16
Size: 369639 Color: 15

Bin 115: 1029 of cap free
Amount of items: 2
Items: 
Size: 513002 Color: 17
Size: 485970 Color: 15

Bin 116: 1079 of cap free
Amount of items: 2
Items: 
Size: 746920 Color: 13
Size: 252002 Color: 19

Bin 117: 1086 of cap free
Amount of items: 2
Items: 
Size: 683445 Color: 4
Size: 315470 Color: 5

Bin 118: 1101 of cap free
Amount of items: 2
Items: 
Size: 528758 Color: 1
Size: 470142 Color: 13

Bin 119: 1171 of cap free
Amount of items: 2
Items: 
Size: 729712 Color: 18
Size: 269118 Color: 10

Bin 120: 1172 of cap free
Amount of items: 2
Items: 
Size: 738293 Color: 10
Size: 260536 Color: 17

Bin 121: 1212 of cap free
Amount of items: 2
Items: 
Size: 606892 Color: 5
Size: 391897 Color: 12

Bin 122: 1229 of cap free
Amount of items: 2
Items: 
Size: 539050 Color: 5
Size: 459722 Color: 16

Bin 123: 1269 of cap free
Amount of items: 2
Items: 
Size: 553342 Color: 9
Size: 445390 Color: 7

Bin 124: 1300 of cap free
Amount of items: 2
Items: 
Size: 733941 Color: 18
Size: 264760 Color: 5

Bin 125: 1310 of cap free
Amount of items: 2
Items: 
Size: 653868 Color: 1
Size: 344823 Color: 8

Bin 126: 1322 of cap free
Amount of items: 2
Items: 
Size: 627610 Color: 12
Size: 371069 Color: 0

Bin 127: 1333 of cap free
Amount of items: 3
Items: 
Size: 658852 Color: 8
Size: 175301 Color: 14
Size: 164515 Color: 8

Bin 128: 1352 of cap free
Amount of items: 2
Items: 
Size: 747248 Color: 3
Size: 251401 Color: 12

Bin 129: 1387 of cap free
Amount of items: 2
Items: 
Size: 701072 Color: 8
Size: 297542 Color: 18

Bin 130: 1410 of cap free
Amount of items: 2
Items: 
Size: 597961 Color: 18
Size: 400630 Color: 11

Bin 131: 1429 of cap free
Amount of items: 2
Items: 
Size: 742101 Color: 2
Size: 256471 Color: 7

Bin 132: 1444 of cap free
Amount of items: 2
Items: 
Size: 778775 Color: 4
Size: 219782 Color: 6

Bin 133: 1454 of cap free
Amount of items: 2
Items: 
Size: 614195 Color: 5
Size: 384352 Color: 11

Bin 134: 1462 of cap free
Amount of items: 2
Items: 
Size: 660079 Color: 18
Size: 338460 Color: 13

Bin 135: 1494 of cap free
Amount of items: 2
Items: 
Size: 553160 Color: 13
Size: 445347 Color: 18

Bin 136: 1525 of cap free
Amount of items: 2
Items: 
Size: 794669 Color: 15
Size: 203807 Color: 13

Bin 137: 1550 of cap free
Amount of items: 2
Items: 
Size: 732175 Color: 8
Size: 266276 Color: 19

Bin 138: 1553 of cap free
Amount of items: 2
Items: 
Size: 743599 Color: 19
Size: 254849 Color: 0

Bin 139: 1564 of cap free
Amount of items: 2
Items: 
Size: 553220 Color: 10
Size: 445217 Color: 8

Bin 140: 1592 of cap free
Amount of items: 2
Items: 
Size: 756907 Color: 2
Size: 241502 Color: 0

Bin 141: 1599 of cap free
Amount of items: 2
Items: 
Size: 585380 Color: 1
Size: 413022 Color: 0

Bin 142: 1617 of cap free
Amount of items: 2
Items: 
Size: 757555 Color: 12
Size: 240829 Color: 0

Bin 143: 1649 of cap free
Amount of items: 2
Items: 
Size: 571678 Color: 16
Size: 426674 Color: 4

Bin 144: 1671 of cap free
Amount of items: 2
Items: 
Size: 501795 Color: 4
Size: 496535 Color: 14

Bin 145: 1762 of cap free
Amount of items: 2
Items: 
Size: 699656 Color: 15
Size: 298583 Color: 1

Bin 146: 1779 of cap free
Amount of items: 2
Items: 
Size: 763297 Color: 13
Size: 234925 Color: 9

Bin 147: 1847 of cap free
Amount of items: 2
Items: 
Size: 707555 Color: 9
Size: 290599 Color: 1

Bin 148: 1861 of cap free
Amount of items: 2
Items: 
Size: 561875 Color: 15
Size: 436265 Color: 17

Bin 149: 1876 of cap free
Amount of items: 2
Items: 
Size: 608691 Color: 0
Size: 389434 Color: 12

Bin 150: 1915 of cap free
Amount of items: 2
Items: 
Size: 733321 Color: 14
Size: 264765 Color: 17

Bin 151: 1931 of cap free
Amount of items: 2
Items: 
Size: 699323 Color: 18
Size: 298747 Color: 1

Bin 152: 1979 of cap free
Amount of items: 2
Items: 
Size: 664924 Color: 2
Size: 333098 Color: 11

Bin 153: 2001 of cap free
Amount of items: 2
Items: 
Size: 648094 Color: 6
Size: 349906 Color: 1

Bin 154: 2031 of cap free
Amount of items: 2
Items: 
Size: 634787 Color: 8
Size: 363183 Color: 2

Bin 155: 2138 of cap free
Amount of items: 2
Items: 
Size: 529926 Color: 3
Size: 467937 Color: 11

Bin 156: 2194 of cap free
Amount of items: 3
Items: 
Size: 755324 Color: 15
Size: 140186 Color: 2
Size: 102297 Color: 14

Bin 157: 2195 of cap free
Amount of items: 2
Items: 
Size: 525331 Color: 18
Size: 472475 Color: 3

Bin 158: 2302 of cap free
Amount of items: 2
Items: 
Size: 675540 Color: 14
Size: 322159 Color: 19

Bin 159: 2335 of cap free
Amount of items: 2
Items: 
Size: 530869 Color: 6
Size: 466797 Color: 18

Bin 160: 2339 of cap free
Amount of items: 2
Items: 
Size: 701530 Color: 13
Size: 296132 Color: 6

Bin 161: 2379 of cap free
Amount of items: 2
Items: 
Size: 762880 Color: 1
Size: 234742 Color: 13

Bin 162: 2527 of cap free
Amount of items: 2
Items: 
Size: 593016 Color: 8
Size: 404458 Color: 16

Bin 163: 2549 of cap free
Amount of items: 2
Items: 
Size: 681517 Color: 17
Size: 315935 Color: 2

Bin 164: 2570 of cap free
Amount of items: 2
Items: 
Size: 613272 Color: 2
Size: 384159 Color: 15

Bin 165: 2650 of cap free
Amount of items: 2
Items: 
Size: 617685 Color: 14
Size: 379666 Color: 9

Bin 166: 2751 of cap free
Amount of items: 2
Items: 
Size: 520360 Color: 14
Size: 476890 Color: 8

Bin 167: 2819 of cap free
Amount of items: 2
Items: 
Size: 568231 Color: 6
Size: 428951 Color: 16

Bin 168: 2980 of cap free
Amount of items: 2
Items: 
Size: 711413 Color: 5
Size: 285608 Color: 11

Bin 169: 2999 of cap free
Amount of items: 2
Items: 
Size: 503627 Color: 15
Size: 493375 Color: 5

Bin 170: 3006 of cap free
Amount of items: 2
Items: 
Size: 605422 Color: 2
Size: 391573 Color: 9

Bin 171: 3009 of cap free
Amount of items: 2
Items: 
Size: 574135 Color: 8
Size: 422857 Color: 17

Bin 172: 3027 of cap free
Amount of items: 2
Items: 
Size: 693112 Color: 7
Size: 303862 Color: 11

Bin 173: 3180 of cap free
Amount of items: 2
Items: 
Size: 746461 Color: 17
Size: 250360 Color: 1

Bin 174: 3228 of cap free
Amount of items: 2
Items: 
Size: 663824 Color: 0
Size: 332949 Color: 5

Bin 175: 3360 of cap free
Amount of items: 2
Items: 
Size: 563924 Color: 3
Size: 432717 Color: 6

Bin 176: 3445 of cap free
Amount of items: 2
Items: 
Size: 515599 Color: 9
Size: 480957 Color: 4

Bin 177: 3450 of cap free
Amount of items: 2
Items: 
Size: 663102 Color: 9
Size: 333449 Color: 19

Bin 178: 3659 of cap free
Amount of items: 2
Items: 
Size: 717006 Color: 8
Size: 279336 Color: 2

Bin 179: 3748 of cap free
Amount of items: 2
Items: 
Size: 671461 Color: 10
Size: 324792 Color: 3

Bin 180: 3965 of cap free
Amount of items: 2
Items: 
Size: 731285 Color: 0
Size: 264751 Color: 2

Bin 181: 4173 of cap free
Amount of items: 2
Items: 
Size: 543931 Color: 6
Size: 451897 Color: 2

Bin 182: 4379 of cap free
Amount of items: 2
Items: 
Size: 717755 Color: 13
Size: 277867 Color: 8

Bin 183: 4433 of cap free
Amount of items: 2
Items: 
Size: 537904 Color: 7
Size: 457664 Color: 8

Bin 184: 4565 of cap free
Amount of items: 2
Items: 
Size: 681149 Color: 5
Size: 314287 Color: 0

Bin 185: 4851 of cap free
Amount of items: 2
Items: 
Size: 536223 Color: 17
Size: 458927 Color: 5

Bin 186: 4920 of cap free
Amount of items: 2
Items: 
Size: 786081 Color: 2
Size: 209000 Color: 7

Bin 187: 4987 of cap free
Amount of items: 2
Items: 
Size: 717021 Color: 18
Size: 277993 Color: 14

Bin 188: 5013 of cap free
Amount of items: 2
Items: 
Size: 624834 Color: 11
Size: 370154 Color: 13

Bin 189: 5258 of cap free
Amount of items: 2
Items: 
Size: 596580 Color: 17
Size: 398163 Color: 16

Bin 190: 5275 of cap free
Amount of items: 2
Items: 
Size: 792535 Color: 11
Size: 202191 Color: 17

Bin 191: 5423 of cap free
Amount of items: 2
Items: 
Size: 604719 Color: 2
Size: 389859 Color: 18

Bin 192: 5472 of cap free
Amount of items: 2
Items: 
Size: 550790 Color: 7
Size: 443739 Color: 1

Bin 193: 5502 of cap free
Amount of items: 2
Items: 
Size: 709505 Color: 14
Size: 284994 Color: 19

Bin 194: 5647 of cap free
Amount of items: 2
Items: 
Size: 731295 Color: 11
Size: 263059 Color: 19

Bin 195: 6153 of cap free
Amount of items: 2
Items: 
Size: 792022 Color: 16
Size: 201826 Color: 11

Bin 196: 6243 of cap free
Amount of items: 2
Items: 
Size: 537523 Color: 8
Size: 456235 Color: 14

Bin 197: 6304 of cap free
Amount of items: 2
Items: 
Size: 625252 Color: 11
Size: 368445 Color: 19

Bin 198: 6368 of cap free
Amount of items: 2
Items: 
Size: 767045 Color: 14
Size: 226588 Color: 1

Bin 199: 6371 of cap free
Amount of items: 2
Items: 
Size: 569580 Color: 12
Size: 424050 Color: 1

Bin 200: 6423 of cap free
Amount of items: 2
Items: 
Size: 591751 Color: 0
Size: 401827 Color: 17

Bin 201: 6438 of cap free
Amount of items: 2
Items: 
Size: 759199 Color: 10
Size: 234364 Color: 7

Bin 202: 6554 of cap free
Amount of items: 2
Items: 
Size: 504305 Color: 6
Size: 489142 Color: 7

Bin 203: 6859 of cap free
Amount of items: 2
Items: 
Size: 690472 Color: 12
Size: 302670 Color: 3

Bin 204: 7711 of cap free
Amount of items: 2
Items: 
Size: 624447 Color: 1
Size: 367843 Color: 10

Bin 205: 8000 of cap free
Amount of items: 2
Items: 
Size: 591457 Color: 5
Size: 400544 Color: 0

Bin 206: 8260 of cap free
Amount of items: 2
Items: 
Size: 570007 Color: 9
Size: 421734 Color: 12

Bin 207: 9625 of cap free
Amount of items: 2
Items: 
Size: 790639 Color: 3
Size: 199737 Color: 15

Bin 208: 10854 of cap free
Amount of items: 2
Items: 
Size: 535053 Color: 3
Size: 454094 Color: 7

Bin 209: 11416 of cap free
Amount of items: 2
Items: 
Size: 788391 Color: 16
Size: 200194 Color: 7

Bin 210: 12413 of cap free
Amount of items: 2
Items: 
Size: 502946 Color: 12
Size: 484642 Color: 1

Bin 211: 12554 of cap free
Amount of items: 2
Items: 
Size: 502029 Color: 2
Size: 485418 Color: 19

Bin 212: 12640 of cap free
Amount of items: 2
Items: 
Size: 753055 Color: 18
Size: 234306 Color: 10

Bin 213: 13634 of cap free
Amount of items: 2
Items: 
Size: 568213 Color: 17
Size: 418154 Color: 12

Bin 214: 14194 of cap free
Amount of items: 2
Items: 
Size: 766125 Color: 1
Size: 219682 Color: 5

Bin 215: 15647 of cap free
Amount of items: 2
Items: 
Size: 787063 Color: 19
Size: 197291 Color: 11

Bin 216: 19891 of cap free
Amount of items: 2
Items: 
Size: 785241 Color: 8
Size: 194869 Color: 1

Bin 217: 20613 of cap free
Amount of items: 2
Items: 
Size: 785164 Color: 18
Size: 194224 Color: 1

Bin 218: 30389 of cap free
Amount of items: 2
Items: 
Size: 782380 Color: 9
Size: 187232 Color: 7

Bin 219: 39669 of cap free
Amount of items: 2
Items: 
Size: 705203 Color: 16
Size: 255129 Color: 19

Bin 220: 48488 of cap free
Amount of items: 2
Items: 
Size: 720848 Color: 5
Size: 230665 Color: 6

Bin 221: 55429 of cap free
Amount of items: 2
Items: 
Size: 759704 Color: 12
Size: 184868 Color: 10

Bin 222: 58413 of cap free
Amount of items: 3
Items: 
Size: 636300 Color: 2
Size: 165922 Color: 3
Size: 139366 Color: 4

Bin 223: 60824 of cap free
Amount of items: 2
Items: 
Size: 567142 Color: 19
Size: 372035 Color: 18

Bin 224: 92479 of cap free
Amount of items: 2
Items: 
Size: 774903 Color: 0
Size: 132619 Color: 2

Bin 225: 187831 of cap free
Amount of items: 2
Items: 
Size: 444313 Color: 12
Size: 367857 Color: 13

Bin 226: 193183 of cap free
Amount of items: 2
Items: 
Size: 442086 Color: 0
Size: 364732 Color: 2

Bin 227: 198703 of cap free
Amount of items: 3
Items: 
Size: 560898 Color: 0
Size: 124307 Color: 18
Size: 116093 Color: 12

Total size: 225554786
Total free space: 1445441

