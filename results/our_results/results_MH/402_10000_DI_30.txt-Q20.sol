Capicity Bin: 4912
Lower Bound: 132

Bins used: 132
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 2472 Color: 12
Size: 1656 Color: 14
Size: 192 Color: 19
Size: 176 Color: 12
Size: 176 Color: 3
Size: 112 Color: 16
Size: 96 Color: 0
Size: 32 Color: 19

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4360 Color: 7
Size: 472 Color: 3
Size: 80 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3880 Color: 13
Size: 1016 Color: 6
Size: 16 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4216 Color: 16
Size: 464 Color: 5
Size: 232 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3946 Color: 1
Size: 806 Color: 2
Size: 160 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4072 Color: 11
Size: 728 Color: 3
Size: 112 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4141 Color: 5
Size: 643 Color: 5
Size: 128 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2476 Color: 9
Size: 2036 Color: 19
Size: 400 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 3410 Color: 14
Size: 1254 Color: 3
Size: 248 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4198 Color: 1
Size: 598 Color: 12
Size: 116 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 1
Size: 524 Color: 17
Size: 96 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 3753 Color: 0
Size: 967 Color: 0
Size: 192 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4056 Color: 16
Size: 680 Color: 2
Size: 176 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 3732 Color: 1
Size: 988 Color: 14
Size: 192 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4233 Color: 13
Size: 567 Color: 11
Size: 112 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 3855 Color: 7
Size: 881 Color: 10
Size: 176 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4384 Color: 11
Size: 400 Color: 15
Size: 128 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 2
Size: 486 Color: 5
Size: 96 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 3990 Color: 10
Size: 838 Color: 3
Size: 84 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 3072 Color: 11
Size: 1680 Color: 7
Size: 160 Color: 2

Bin 21: 0 of cap free
Amount of items: 4
Items: 
Size: 3392 Color: 2
Size: 1136 Color: 1
Size: 320 Color: 2
Size: 64 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2690 Color: 3
Size: 1886 Color: 3
Size: 336 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 3724 Color: 6
Size: 996 Color: 12
Size: 192 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 3432 Color: 6
Size: 1240 Color: 0
Size: 240 Color: 5

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4327 Color: 14
Size: 489 Color: 5
Size: 96 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 3460 Color: 4
Size: 1212 Color: 2
Size: 240 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2458 Color: 6
Size: 2150 Color: 16
Size: 304 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 3784 Color: 7
Size: 584 Color: 0
Size: 544 Color: 7

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 3704 Color: 18
Size: 768 Color: 14
Size: 440 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 3816 Color: 8
Size: 872 Color: 1
Size: 224 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2936 Color: 3
Size: 1024 Color: 17
Size: 952 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2852 Color: 10
Size: 1740 Color: 3
Size: 320 Color: 19

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 3262 Color: 2
Size: 1378 Color: 19
Size: 272 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2457 Color: 19
Size: 2047 Color: 8
Size: 408 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4236 Color: 17
Size: 564 Color: 1
Size: 112 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 3900 Color: 9
Size: 996 Color: 1
Size: 16 Color: 16

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4326 Color: 17
Size: 490 Color: 7
Size: 96 Color: 19

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 3484 Color: 12
Size: 1196 Color: 6
Size: 232 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4248 Color: 1
Size: 568 Color: 19
Size: 96 Color: 13

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 3558 Color: 16
Size: 1130 Color: 3
Size: 224 Color: 13

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4418 Color: 3
Size: 414 Color: 1
Size: 80 Color: 16

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4386 Color: 10
Size: 442 Color: 18
Size: 84 Color: 5

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 4331 Color: 7
Size: 485 Color: 8
Size: 96 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2728 Color: 2
Size: 1832 Color: 2
Size: 352 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 3838 Color: 1
Size: 898 Color: 7
Size: 176 Color: 10

Bin 46: 0 of cap free
Amount of items: 4
Items: 
Size: 3548 Color: 10
Size: 1140 Color: 18
Size: 128 Color: 0
Size: 96 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 2958 Color: 8
Size: 1698 Color: 16
Size: 256 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 3116 Color: 2
Size: 1500 Color: 0
Size: 296 Color: 11

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 3528 Color: 10
Size: 1160 Color: 16
Size: 224 Color: 5

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 4396 Color: 6
Size: 436 Color: 6
Size: 80 Color: 10

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2460 Color: 4
Size: 2044 Color: 8
Size: 408 Color: 6

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2040 Color: 3
Size: 1952 Color: 6
Size: 920 Color: 14

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 3848 Color: 5
Size: 888 Color: 17
Size: 176 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4392 Color: 10
Size: 344 Color: 12
Size: 176 Color: 8

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 3269 Color: 12
Size: 1371 Color: 6
Size: 272 Color: 19

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 3516 Color: 16
Size: 1164 Color: 4
Size: 232 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2820 Color: 16
Size: 1532 Color: 9
Size: 560 Color: 19

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 3126 Color: 8
Size: 1562 Color: 12
Size: 224 Color: 11

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2462 Color: 13
Size: 2042 Color: 4
Size: 408 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 4104 Color: 1
Size: 728 Color: 7
Size: 80 Color: 14

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 3288 Color: 11
Size: 1368 Color: 8
Size: 256 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 3148 Color: 18
Size: 1748 Color: 10
Size: 16 Color: 19

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4038 Color: 1
Size: 730 Color: 14
Size: 144 Color: 9

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2995 Color: 7
Size: 1599 Color: 11
Size: 318 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 3051 Color: 2
Size: 1551 Color: 19
Size: 310 Color: 18

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 3690 Color: 17
Size: 1022 Color: 7
Size: 200 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 3586 Color: 9
Size: 1106 Color: 14
Size: 220 Color: 14

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 3449 Color: 15
Size: 1221 Color: 17
Size: 242 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4194 Color: 15
Size: 670 Color: 6
Size: 48 Color: 9

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 2938 Color: 0
Size: 1646 Color: 14
Size: 328 Color: 7

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 4125 Color: 3
Size: 657 Color: 6
Size: 130 Color: 8

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 4035 Color: 5
Size: 731 Color: 11
Size: 146 Color: 10

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 2674 Color: 18
Size: 1866 Color: 10
Size: 372 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 3963 Color: 2
Size: 791 Color: 8
Size: 158 Color: 8

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 4260 Color: 9
Size: 548 Color: 18
Size: 104 Color: 19

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 11
Size: 724 Color: 8
Size: 136 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 3925 Color: 12
Size: 823 Color: 17
Size: 164 Color: 16

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 5
Size: 692 Color: 9
Size: 136 Color: 6

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 3839 Color: 2
Size: 895 Color: 16
Size: 178 Color: 14

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 3822 Color: 6
Size: 910 Color: 19
Size: 180 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 3228 Color: 3
Size: 1404 Color: 13
Size: 280 Color: 9

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 3602 Color: 7
Size: 1238 Color: 13
Size: 72 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 3063 Color: 8
Size: 1541 Color: 8
Size: 308 Color: 5

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 3613 Color: 11
Size: 1083 Color: 9
Size: 216 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 4179 Color: 9
Size: 611 Color: 5
Size: 122 Color: 13

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 3868 Color: 4
Size: 876 Color: 15
Size: 168 Color: 10

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4100 Color: 12
Size: 684 Color: 0
Size: 128 Color: 12

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 4115 Color: 11
Size: 665 Color: 12
Size: 132 Color: 11

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 13
Size: 708 Color: 8
Size: 136 Color: 8

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 4
Size: 564 Color: 10
Size: 104 Color: 18

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 3570 Color: 4
Size: 1122 Color: 19
Size: 220 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 4246 Color: 9
Size: 558 Color: 19
Size: 108 Color: 11

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 4187 Color: 17
Size: 605 Color: 9
Size: 120 Color: 5

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 2971 Color: 15
Size: 1619 Color: 17
Size: 322 Color: 19

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4276 Color: 19
Size: 532 Color: 12
Size: 104 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 3260 Color: 1
Size: 1380 Color: 10
Size: 272 Color: 5

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 3971 Color: 5
Size: 785 Color: 7
Size: 156 Color: 11

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 2773 Color: 16
Size: 1783 Color: 10
Size: 356 Color: 5

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 4067 Color: 17
Size: 705 Color: 11
Size: 140 Color: 19

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 4051 Color: 11
Size: 719 Color: 14
Size: 142 Color: 9

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 4279 Color: 11
Size: 529 Color: 3
Size: 104 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 3112 Color: 15
Size: 1512 Color: 0
Size: 288 Color: 4

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 2459 Color: 16
Size: 2145 Color: 0
Size: 308 Color: 4

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 4126 Color: 1
Size: 658 Color: 8
Size: 128 Color: 8

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 4122 Color: 15
Size: 662 Color: 15
Size: 128 Color: 12

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 3737 Color: 15
Size: 981 Color: 1
Size: 194 Color: 6

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 2955 Color: 4
Size: 1751 Color: 7
Size: 206 Color: 11

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 3465 Color: 7
Size: 1207 Color: 9
Size: 240 Color: 13

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 4315 Color: 19
Size: 499 Color: 6
Size: 98 Color: 3

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 3378 Color: 16
Size: 1282 Color: 0
Size: 252 Color: 13

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 4059 Color: 12
Size: 711 Color: 13
Size: 142 Color: 10

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 3279 Color: 5
Size: 1361 Color: 6
Size: 272 Color: 18

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 3394 Color: 9
Size: 1266 Color: 12
Size: 252 Color: 10

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 4402 Color: 13
Size: 426 Color: 14
Size: 84 Color: 17

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4262 Color: 4
Size: 542 Color: 19
Size: 108 Color: 8

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 3599 Color: 15
Size: 1209 Color: 6
Size: 104 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 3635 Color: 18
Size: 1065 Color: 11
Size: 212 Color: 12

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 3932 Color: 6
Size: 820 Color: 7
Size: 160 Color: 12

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 3939 Color: 18
Size: 811 Color: 2
Size: 162 Color: 7

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 2675 Color: 10
Size: 1865 Color: 1
Size: 372 Color: 10

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 3178 Color: 10
Size: 1446 Color: 14
Size: 288 Color: 3

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 4225 Color: 0
Size: 573 Color: 16
Size: 114 Color: 18

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 4404 Color: 17
Size: 428 Color: 14
Size: 80 Color: 16

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 3196 Color: 8
Size: 1436 Color: 16
Size: 280 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 3974 Color: 19
Size: 782 Color: 7
Size: 156 Color: 13

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 4263 Color: 8
Size: 541 Color: 6
Size: 108 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 3955 Color: 6
Size: 799 Color: 17
Size: 158 Color: 13

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 3634 Color: 7
Size: 1214 Color: 9
Size: 64 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 3618 Color: 3
Size: 1082 Color: 14
Size: 212 Color: 8

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 3958 Color: 4
Size: 798 Color: 12
Size: 156 Color: 10

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 4209 Color: 0
Size: 657 Color: 8
Size: 46 Color: 18

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 4278 Color: 1
Size: 530 Color: 13
Size: 104 Color: 8

Total size: 648384
Total free space: 0

