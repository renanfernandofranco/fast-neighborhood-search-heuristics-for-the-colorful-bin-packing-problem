Capicity Bin: 2020
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1598 Color: 2
Size: 312 Color: 4
Size: 102 Color: 3
Size: 8 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1706 Color: 0
Size: 142 Color: 3
Size: 76 Color: 4
Size: 56 Color: 4
Size: 40 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1758 Color: 2
Size: 166 Color: 0
Size: 60 Color: 4
Size: 36 Color: 1

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1011 Color: 0
Size: 833 Color: 2
Size: 120 Color: 0
Size: 56 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 4
Size: 132 Color: 1
Size: 102 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1315 Color: 1
Size: 589 Color: 3
Size: 116 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1014 Color: 3
Size: 786 Color: 0
Size: 220 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 816 Color: 1
Size: 698 Color: 3
Size: 506 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1155 Color: 2
Size: 721 Color: 2
Size: 144 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 0
Size: 363 Color: 3
Size: 72 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 3
Size: 241 Color: 4
Size: 48 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1013 Color: 1
Size: 841 Color: 3
Size: 166 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1386 Color: 1
Size: 420 Color: 0
Size: 214 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 2
Size: 190 Color: 0
Size: 36 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 2
Size: 365 Color: 0
Size: 72 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 2
Size: 286 Color: 0
Size: 64 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1378 Color: 0
Size: 586 Color: 2
Size: 56 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 1
Size: 405 Color: 0
Size: 80 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1268 Color: 0
Size: 684 Color: 1
Size: 68 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 2
Size: 301 Color: 2
Size: 58 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1222 Color: 3
Size: 666 Color: 4
Size: 132 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1537 Color: 3
Size: 403 Color: 4
Size: 80 Color: 3

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 1399 Color: 2
Size: 515 Color: 4
Size: 66 Color: 1
Size: 40 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1772 Color: 4
Size: 212 Color: 1
Size: 36 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1217 Color: 4
Size: 713 Color: 4
Size: 90 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1362 Color: 4
Size: 614 Color: 2
Size: 44 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 1
Size: 222 Color: 1
Size: 164 Color: 2

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 1665 Color: 1
Size: 331 Color: 2
Size: 16 Color: 3
Size: 8 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 1
Size: 198 Color: 2
Size: 44 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 1
Size: 269 Color: 4
Size: 52 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 0
Size: 509 Color: 2
Size: 100 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 4
Size: 398 Color: 0
Size: 76 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1400 Color: 2
Size: 338 Color: 4
Size: 282 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 2
Size: 831 Color: 0
Size: 168 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1477 Color: 3
Size: 491 Color: 2
Size: 52 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1766 Color: 1
Size: 202 Color: 2
Size: 52 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 1
Size: 721 Color: 4
Size: 134 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1017 Color: 0
Size: 837 Color: 2
Size: 166 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 1
Size: 519 Color: 2
Size: 20 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 3
Size: 354 Color: 3
Size: 48 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1078 Color: 0
Size: 842 Color: 0
Size: 100 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1403 Color: 4
Size: 453 Color: 1
Size: 164 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 2
Size: 294 Color: 3
Size: 40 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 0
Size: 374 Color: 4
Size: 72 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1157 Color: 1
Size: 841 Color: 1
Size: 22 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 1
Size: 230 Color: 0
Size: 44 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1161 Color: 4
Size: 717 Color: 4
Size: 142 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1186 Color: 0
Size: 610 Color: 0
Size: 224 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1623 Color: 2
Size: 337 Color: 2
Size: 60 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 4
Size: 451 Color: 3
Size: 52 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 3
Size: 574 Color: 0
Size: 156 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 2
Size: 287 Color: 1
Size: 56 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 0
Size: 464 Color: 1
Size: 142 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1466 Color: 2
Size: 462 Color: 4
Size: 92 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 0
Size: 329 Color: 1
Size: 64 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 2
Size: 426 Color: 3
Size: 84 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 2
Size: 673 Color: 4
Size: 322 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 3
Size: 270 Color: 0
Size: 72 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 3
Size: 297 Color: 2
Size: 64 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1213 Color: 0
Size: 671 Color: 1
Size: 136 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 0
Size: 267 Color: 0
Size: 16 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1705 Color: 0
Size: 263 Color: 2
Size: 52 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 2
Size: 587 Color: 4
Size: 116 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 2
Size: 511 Color: 1
Size: 102 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1169 Color: 4
Size: 775 Color: 4
Size: 76 Color: 0

Total size: 131300
Total free space: 0

