Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8
Size: 350 Color: 15
Size: 250 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7
Size: 362 Color: 9
Size: 268 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 331 Color: 5
Size: 263 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 5
Size: 346 Color: 19
Size: 278 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 15
Size: 355 Color: 3
Size: 272 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 2
Size: 296 Color: 1
Size: 259 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 0
Size: 265 Color: 3
Size: 254 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 286 Color: 1
Size: 258 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 255 Color: 18
Size: 254 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 16
Size: 303 Color: 13
Size: 284 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 15
Size: 340 Color: 5
Size: 273 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 6
Size: 266 Color: 14
Size: 263 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 341 Color: 9
Size: 274 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 11
Size: 272 Color: 12
Size: 252 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 7
Size: 278 Color: 8
Size: 266 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 328 Color: 15
Size: 283 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 15
Size: 285 Color: 14
Size: 253 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 9
Size: 355 Color: 4
Size: 273 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 6
Size: 268 Color: 19
Size: 265 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 18
Size: 311 Color: 13
Size: 285 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 19
Size: 291 Color: 17
Size: 260 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 364 Color: 14
Size: 256 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 4
Size: 273 Color: 5
Size: 259 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 336 Color: 5
Size: 250 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 282 Color: 8
Size: 276 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 18
Size: 332 Color: 17
Size: 281 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 291 Color: 4
Size: 278 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 3
Size: 326 Color: 0
Size: 263 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 19
Size: 367 Color: 3
Size: 264 Color: 12

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 16
Size: 308 Color: 16
Size: 281 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 12
Size: 288 Color: 2
Size: 265 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 8
Size: 258 Color: 17
Size: 250 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 262 Color: 8
Size: 258 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 12
Size: 333 Color: 0
Size: 256 Color: 7

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 10
Size: 301 Color: 2
Size: 262 Color: 15

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 8
Size: 351 Color: 1
Size: 283 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 18
Size: 299 Color: 2
Size: 265 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 19
Size: 268 Color: 19
Size: 252 Color: 13

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 14
Size: 311 Color: 16
Size: 260 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 6
Size: 253 Color: 1
Size: 252 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 16
Size: 340 Color: 1
Size: 264 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 10
Size: 284 Color: 14
Size: 252 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 11
Size: 293 Color: 2
Size: 268 Color: 11

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8
Size: 287 Color: 17
Size: 272 Color: 11

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 10
Size: 262 Color: 8
Size: 256 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 10
Size: 357 Color: 5
Size: 278 Color: 12

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 15
Size: 365 Color: 6
Size: 252 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 3
Size: 296 Color: 19
Size: 250 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 14
Size: 255 Color: 4
Size: 252 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 12
Size: 288 Color: 15
Size: 272 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 310 Color: 19
Size: 278 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9
Size: 293 Color: 5
Size: 257 Color: 5

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 13
Size: 335 Color: 15
Size: 282 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 255 Color: 15
Size: 252 Color: 8

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 7
Size: 314 Color: 11
Size: 254 Color: 19

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 17
Size: 292 Color: 8
Size: 250 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 13
Size: 307 Color: 2
Size: 280 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 13
Size: 349 Color: 11
Size: 284 Color: 12

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 11
Size: 264 Color: 4
Size: 254 Color: 10

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 14
Size: 324 Color: 9
Size: 258 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 8
Size: 353 Color: 5
Size: 271 Color: 14

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 6
Size: 313 Color: 3
Size: 294 Color: 9

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8
Size: 313 Color: 6
Size: 280 Color: 11

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 19
Size: 275 Color: 19
Size: 256 Color: 13

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 257 Color: 9
Size: 253 Color: 18

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 14
Size: 292 Color: 14
Size: 261 Color: 11

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 12
Size: 271 Color: 17
Size: 252 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 12
Size: 316 Color: 10
Size: 253 Color: 7

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 6
Size: 315 Color: 13
Size: 258 Color: 10

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 8
Size: 355 Color: 2
Size: 273 Color: 19

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 325 Color: 3
Size: 270 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 19
Size: 273 Color: 7
Size: 272 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 6
Size: 288 Color: 10
Size: 261 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 12
Size: 256 Color: 11
Size: 255 Color: 9

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 15
Size: 279 Color: 5
Size: 256 Color: 17

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 17
Size: 336 Color: 14
Size: 297 Color: 19

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 4
Size: 313 Color: 8
Size: 271 Color: 10

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 11
Size: 274 Color: 9
Size: 269 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 15
Size: 277 Color: 17
Size: 250 Color: 19

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 15
Size: 275 Color: 14
Size: 259 Color: 7

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 9
Size: 324 Color: 13
Size: 289 Color: 5

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 12
Size: 356 Color: 14
Size: 265 Color: 19

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 4
Size: 251 Color: 12
Size: 251 Color: 4

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 10
Size: 303 Color: 17
Size: 287 Color: 9

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 4
Size: 281 Color: 15
Size: 256 Color: 14

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 16
Size: 269 Color: 16
Size: 257 Color: 7

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 14
Size: 353 Color: 9
Size: 272 Color: 15

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8
Size: 329 Color: 0
Size: 252 Color: 16

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 3
Size: 276 Color: 19
Size: 259 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 2
Size: 266 Color: 5
Size: 251 Color: 9

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 13
Size: 337 Color: 13
Size: 297 Color: 15

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 13
Size: 329 Color: 5
Size: 252 Color: 19

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 2
Size: 351 Color: 18
Size: 295 Color: 3

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 13
Size: 353 Color: 9
Size: 258 Color: 6

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 10
Size: 323 Color: 18
Size: 260 Color: 10

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 10
Size: 284 Color: 3
Size: 262 Color: 10

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 17
Size: 350 Color: 0
Size: 272 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 18
Size: 369 Color: 3
Size: 262 Color: 9

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 330 Color: 12
Size: 291 Color: 17

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 3
Size: 351 Color: 5
Size: 278 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 13
Size: 317 Color: 14
Size: 306 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 19
Size: 326 Color: 8
Size: 299 Color: 8

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 11
Size: 305 Color: 15
Size: 269 Color: 10

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 13
Size: 335 Color: 14
Size: 295 Color: 14

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 18
Size: 356 Color: 11
Size: 261 Color: 12

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 19
Size: 322 Color: 4
Size: 250 Color: 6

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 19
Size: 303 Color: 19
Size: 267 Color: 2

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 6
Size: 365 Color: 3
Size: 261 Color: 15

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 12
Size: 356 Color: 3
Size: 278 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 17
Size: 253 Color: 11
Size: 252 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7
Size: 372 Color: 7
Size: 251 Color: 9

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 17
Size: 310 Color: 8
Size: 299 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 18
Size: 263 Color: 2
Size: 255 Color: 9

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 0
Size: 301 Color: 17
Size: 260 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 15
Size: 283 Color: 14
Size: 254 Color: 10

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 15
Size: 316 Color: 10
Size: 261 Color: 15

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 13
Size: 269 Color: 18
Size: 266 Color: 6

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 17
Size: 306 Color: 18
Size: 289 Color: 11

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 17
Size: 291 Color: 9
Size: 263 Color: 19

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 5
Size: 255 Color: 16
Size: 254 Color: 11

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 2
Size: 363 Color: 3
Size: 269 Color: 17

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 361 Color: 7
Size: 276 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 294 Color: 17
Size: 260 Color: 13

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 2
Size: 357 Color: 9
Size: 285 Color: 18

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 13
Size: 302 Color: 12
Size: 295 Color: 11

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 11
Size: 284 Color: 10
Size: 257 Color: 5

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 14
Size: 354 Color: 11
Size: 269 Color: 17

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8
Size: 302 Color: 11
Size: 264 Color: 9

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 15
Size: 255 Color: 19
Size: 250 Color: 13

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 16
Size: 270 Color: 5
Size: 257 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 17
Size: 289 Color: 17
Size: 283 Color: 15

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 5
Size: 367 Color: 13
Size: 263 Color: 18

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 254 Color: 16
Size: 254 Color: 16

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 19
Size: 352 Color: 10
Size: 251 Color: 19

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 312 Color: 17
Size: 260 Color: 10

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 17
Size: 283 Color: 0
Size: 270 Color: 3

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 18
Size: 299 Color: 2
Size: 258 Color: 11

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 6
Size: 320 Color: 14
Size: 278 Color: 15

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 7
Size: 279 Color: 11
Size: 302 Color: 18

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 13
Size: 311 Color: 8
Size: 262 Color: 13

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 14
Size: 257 Color: 5
Size: 252 Color: 8

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 11
Size: 251 Color: 2
Size: 250 Color: 12

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 6
Size: 300 Color: 12
Size: 286 Color: 11

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 13
Size: 325 Color: 6
Size: 286 Color: 18

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 11
Size: 284 Color: 8
Size: 254 Color: 13

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 11
Size: 355 Color: 13
Size: 286 Color: 8

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 14
Size: 310 Color: 6
Size: 277 Color: 7

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 299 Color: 15
Size: 267 Color: 11

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 10
Size: 315 Color: 12
Size: 251 Color: 16

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 12
Size: 273 Color: 1
Size: 262 Color: 8

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 5
Size: 270 Color: 19
Size: 261 Color: 13

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 9
Size: 313 Color: 11
Size: 303 Color: 18

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 12
Size: 357 Color: 11
Size: 276 Color: 12

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 13
Size: 350 Color: 12
Size: 289 Color: 9

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 5
Size: 335 Color: 4
Size: 289 Color: 8

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 3
Size: 356 Color: 10
Size: 256 Color: 2

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 15
Size: 316 Color: 16
Size: 263 Color: 13

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 16
Size: 322 Color: 18
Size: 313 Color: 2

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 363 Color: 10
Size: 261 Color: 2

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 7
Size: 307 Color: 3
Size: 293 Color: 5

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 17
Size: 358 Color: 5
Size: 278 Color: 18

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 15
Size: 309 Color: 11
Size: 299 Color: 2

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 18
Size: 324 Color: 2
Size: 314 Color: 4

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 354 Color: 19
Size: 279 Color: 19

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 17
Size: 330 Color: 2
Size: 271 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 4
Size: 261 Color: 13
Size: 254 Color: 19

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 12
Size: 333 Color: 15
Size: 285 Color: 17

Total size: 167000
Total free space: 0

