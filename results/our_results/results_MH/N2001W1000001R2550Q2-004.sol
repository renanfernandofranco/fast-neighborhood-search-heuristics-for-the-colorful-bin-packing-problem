Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 417775 Color: 1
Size: 326711 Color: 0
Size: 255515 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 432809 Color: 0
Size: 309997 Color: 0
Size: 257195 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 405485 Color: 1
Size: 332388 Color: 1
Size: 262128 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 482192 Color: 1
Size: 260016 Color: 0
Size: 257793 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 435365 Color: 0
Size: 282361 Color: 1
Size: 282275 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 376282 Color: 1
Size: 333337 Color: 0
Size: 290382 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 359462 Color: 0
Size: 338853 Color: 1
Size: 301686 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 405247 Color: 1
Size: 327482 Color: 0
Size: 267272 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 461636 Color: 1
Size: 269606 Color: 1
Size: 268759 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 390481 Color: 0
Size: 340265 Color: 1
Size: 269255 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 474967 Color: 0
Size: 266560 Color: 1
Size: 258474 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 470379 Color: 0
Size: 275080 Color: 0
Size: 254542 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 484864 Color: 0
Size: 259520 Color: 1
Size: 255617 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 409076 Color: 0
Size: 302711 Color: 1
Size: 288214 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 425551 Color: 1
Size: 309060 Color: 1
Size: 265390 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 473196 Color: 1
Size: 263644 Color: 1
Size: 263161 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 414201 Color: 1
Size: 327203 Color: 0
Size: 258597 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 421016 Color: 1
Size: 306103 Color: 1
Size: 272882 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 395521 Color: 0
Size: 334879 Color: 0
Size: 269601 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 459584 Color: 1
Size: 273634 Color: 0
Size: 266783 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 361687 Color: 1
Size: 351648 Color: 0
Size: 286666 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 423361 Color: 0
Size: 308576 Color: 1
Size: 268064 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 453088 Color: 0
Size: 274586 Color: 0
Size: 272327 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 421372 Color: 0
Size: 325325 Color: 1
Size: 253304 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 472792 Color: 0
Size: 267613 Color: 0
Size: 259596 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 407999 Color: 1
Size: 298156 Color: 0
Size: 293846 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 461654 Color: 0
Size: 286771 Color: 1
Size: 251576 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 445984 Color: 0
Size: 282680 Color: 0
Size: 271337 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 495880 Color: 1
Size: 252570 Color: 0
Size: 251551 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 464525 Color: 1
Size: 278707 Color: 1
Size: 256769 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 493611 Color: 1
Size: 255633 Color: 0
Size: 250757 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 416229 Color: 1
Size: 322803 Color: 0
Size: 260969 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 492170 Color: 0
Size: 254590 Color: 1
Size: 253241 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 404194 Color: 0
Size: 330691 Color: 0
Size: 265116 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 474321 Color: 1
Size: 266176 Color: 1
Size: 259504 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 483185 Color: 0
Size: 264571 Color: 0
Size: 252245 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 374312 Color: 0
Size: 351877 Color: 0
Size: 273812 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 456088 Color: 1
Size: 275519 Color: 0
Size: 268394 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 378422 Color: 1
Size: 313297 Color: 0
Size: 308282 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 364693 Color: 1
Size: 346331 Color: 0
Size: 288977 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 472612 Color: 1
Size: 270142 Color: 1
Size: 257247 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 434979 Color: 1
Size: 306530 Color: 0
Size: 258492 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 336254 Color: 0
Size: 332668 Color: 0
Size: 331079 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 472270 Color: 1
Size: 273000 Color: 0
Size: 254731 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 390670 Color: 1
Size: 335648 Color: 0
Size: 273683 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 493872 Color: 0
Size: 255570 Color: 1
Size: 250559 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 417009 Color: 0
Size: 308492 Color: 0
Size: 274500 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 488222 Color: 1
Size: 261083 Color: 0
Size: 250696 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 388580 Color: 1
Size: 354945 Color: 0
Size: 256476 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 411822 Color: 0
Size: 298723 Color: 0
Size: 289456 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 415119 Color: 1
Size: 299573 Color: 1
Size: 285309 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 386270 Color: 0
Size: 323371 Color: 1
Size: 290360 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 420428 Color: 1
Size: 315314 Color: 0
Size: 264259 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 385612 Color: 0
Size: 333295 Color: 1
Size: 281094 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 407061 Color: 1
Size: 320550 Color: 1
Size: 272390 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 414620 Color: 1
Size: 335114 Color: 0
Size: 250267 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 463732 Color: 1
Size: 276840 Color: 0
Size: 259429 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 413883 Color: 1
Size: 297310 Color: 0
Size: 288808 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 495153 Color: 0
Size: 253763 Color: 0
Size: 251085 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 350914 Color: 0
Size: 350241 Color: 1
Size: 298846 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 399064 Color: 0
Size: 303210 Color: 1
Size: 297727 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 470069 Color: 0
Size: 275613 Color: 0
Size: 254319 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 396684 Color: 0
Size: 327031 Color: 1
Size: 276286 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 435909 Color: 1
Size: 306875 Color: 1
Size: 257217 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 387762 Color: 0
Size: 329211 Color: 1
Size: 283028 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 474358 Color: 1
Size: 273273 Color: 0
Size: 252370 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 451378 Color: 0
Size: 282379 Color: 0
Size: 266244 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 481846 Color: 1
Size: 259732 Color: 0
Size: 258423 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 389297 Color: 1
Size: 334879 Color: 1
Size: 275825 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 359499 Color: 1
Size: 332989 Color: 0
Size: 307513 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 425424 Color: 1
Size: 310867 Color: 1
Size: 263710 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 466634 Color: 1
Size: 282517 Color: 0
Size: 250850 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 489113 Color: 0
Size: 260093 Color: 1
Size: 250795 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 392499 Color: 1
Size: 333365 Color: 0
Size: 274137 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 361491 Color: 0
Size: 330790 Color: 1
Size: 307720 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 481392 Color: 1
Size: 266000 Color: 0
Size: 252609 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 457556 Color: 0
Size: 291563 Color: 1
Size: 250882 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 355072 Color: 0
Size: 325291 Color: 1
Size: 319638 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 407456 Color: 1
Size: 327151 Color: 1
Size: 265394 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 376969 Color: 0
Size: 316068 Color: 1
Size: 306964 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 374973 Color: 0
Size: 358280 Color: 0
Size: 266748 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 385112 Color: 0
Size: 317854 Color: 1
Size: 297035 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 404444 Color: 1
Size: 298688 Color: 1
Size: 296869 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 437802 Color: 1
Size: 302855 Color: 0
Size: 259344 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 492588 Color: 0
Size: 254329 Color: 0
Size: 253084 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 454044 Color: 1
Size: 279672 Color: 0
Size: 266285 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 367569 Color: 0
Size: 364680 Color: 1
Size: 267752 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 375673 Color: 0
Size: 331772 Color: 0
Size: 292556 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 474177 Color: 1
Size: 263724 Color: 1
Size: 262100 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 398404 Color: 0
Size: 318185 Color: 0
Size: 283412 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 432642 Color: 1
Size: 311356 Color: 0
Size: 256003 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 408817 Color: 0
Size: 340766 Color: 1
Size: 250418 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 425069 Color: 0
Size: 316193 Color: 0
Size: 258739 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 394821 Color: 0
Size: 333524 Color: 0
Size: 271656 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 373594 Color: 0
Size: 348715 Color: 1
Size: 277692 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 353188 Color: 0
Size: 335302 Color: 1
Size: 311511 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 360165 Color: 1
Size: 355874 Color: 1
Size: 283962 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 346779 Color: 0
Size: 341920 Color: 1
Size: 311302 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 375488 Color: 0
Size: 357062 Color: 1
Size: 267451 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 399133 Color: 0
Size: 345022 Color: 1
Size: 255846 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 455178 Color: 0
Size: 278520 Color: 0
Size: 266303 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 387846 Color: 1
Size: 347038 Color: 1
Size: 265117 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 390398 Color: 0
Size: 344874 Color: 1
Size: 264729 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 339281 Color: 0
Size: 338635 Color: 1
Size: 322085 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 497641 Color: 1
Size: 251470 Color: 0
Size: 250890 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 364323 Color: 1
Size: 328404 Color: 1
Size: 307274 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 364162 Color: 0
Size: 325348 Color: 1
Size: 310491 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 364399 Color: 0
Size: 359468 Color: 1
Size: 276134 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 366282 Color: 1
Size: 319477 Color: 0
Size: 314242 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 366178 Color: 0
Size: 321990 Color: 0
Size: 311833 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 367163 Color: 1
Size: 322346 Color: 1
Size: 310492 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 366910 Color: 0
Size: 321905 Color: 0
Size: 311186 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 367624 Color: 0
Size: 340118 Color: 1
Size: 292259 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 370121 Color: 1
Size: 353134 Color: 1
Size: 276746 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 370081 Color: 0
Size: 346320 Color: 0
Size: 283600 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 370740 Color: 0
Size: 317781 Color: 1
Size: 311480 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 370812 Color: 0
Size: 351215 Color: 1
Size: 277974 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 371108 Color: 0
Size: 324421 Color: 0
Size: 304472 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 374972 Color: 0
Size: 372796 Color: 1
Size: 252233 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 375155 Color: 0
Size: 328698 Color: 0
Size: 296148 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 374421 Color: 1
Size: 343255 Color: 0
Size: 282325 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 374752 Color: 1
Size: 342693 Color: 1
Size: 282556 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 375763 Color: 1
Size: 372142 Color: 0
Size: 252096 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 377014 Color: 1
Size: 351414 Color: 1
Size: 271573 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 378316 Color: 0
Size: 330840 Color: 1
Size: 290845 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 380143 Color: 1
Size: 325745 Color: 1
Size: 294113 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 379394 Color: 0
Size: 369578 Color: 0
Size: 251029 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 380201 Color: 1
Size: 345430 Color: 1
Size: 274370 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 380318 Color: 1
Size: 334069 Color: 0
Size: 285614 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 381098 Color: 0
Size: 324632 Color: 1
Size: 294271 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 385177 Color: 0
Size: 348181 Color: 1
Size: 266643 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 385844 Color: 0
Size: 354195 Color: 1
Size: 259962 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 385900 Color: 0
Size: 319659 Color: 1
Size: 294442 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 384828 Color: 1
Size: 357136 Color: 0
Size: 258037 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 386010 Color: 0
Size: 339180 Color: 0
Size: 274811 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 386150 Color: 0
Size: 318762 Color: 1
Size: 295089 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 386310 Color: 1
Size: 344112 Color: 0
Size: 269579 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 386532 Color: 0
Size: 306882 Color: 1
Size: 306587 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 386613 Color: 1
Size: 321992 Color: 0
Size: 291396 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 389072 Color: 0
Size: 327944 Color: 0
Size: 282985 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 389425 Color: 1
Size: 336606 Color: 1
Size: 273970 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 389972 Color: 0
Size: 305594 Color: 1
Size: 304435 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 390043 Color: 1
Size: 305916 Color: 1
Size: 304042 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 390401 Color: 1
Size: 333280 Color: 0
Size: 276320 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 390145 Color: 0
Size: 358497 Color: 1
Size: 251359 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 390897 Color: 1
Size: 307898 Color: 0
Size: 301206 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 390954 Color: 1
Size: 338609 Color: 1
Size: 270438 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 391194 Color: 0
Size: 327262 Color: 0
Size: 281545 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 393058 Color: 0
Size: 347748 Color: 1
Size: 259195 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 393160 Color: 0
Size: 353825 Color: 1
Size: 253016 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 393735 Color: 0
Size: 352941 Color: 1
Size: 253325 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 393399 Color: 1
Size: 324862 Color: 0
Size: 281740 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 394029 Color: 0
Size: 330392 Color: 1
Size: 275580 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 395571 Color: 1
Size: 332361 Color: 0
Size: 272069 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 396233 Color: 0
Size: 312292 Color: 0
Size: 291476 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 396276 Color: 1
Size: 347220 Color: 0
Size: 256505 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 397573 Color: 0
Size: 332209 Color: 0
Size: 270219 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 397617 Color: 0
Size: 344751 Color: 1
Size: 257633 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 397550 Color: 1
Size: 336039 Color: 0
Size: 266412 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 397771 Color: 0
Size: 348662 Color: 1
Size: 253568 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 398178 Color: 1
Size: 311906 Color: 1
Size: 289917 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 398680 Color: 0
Size: 315253 Color: 0
Size: 286068 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 398918 Color: 1
Size: 317159 Color: 1
Size: 283924 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 399352 Color: 1
Size: 312145 Color: 1
Size: 288504 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 400055 Color: 0
Size: 313095 Color: 1
Size: 286851 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 400513 Color: 0
Size: 333626 Color: 1
Size: 265862 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 400441 Color: 1
Size: 312259 Color: 0
Size: 287301 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 401445 Color: 1
Size: 347540 Color: 0
Size: 251016 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 402177 Color: 1
Size: 305095 Color: 0
Size: 292729 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 402207 Color: 1
Size: 324575 Color: 0
Size: 273219 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 402922 Color: 0
Size: 319889 Color: 1
Size: 277190 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 402210 Color: 1
Size: 335439 Color: 0
Size: 262352 Color: 1

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 403174 Color: 0
Size: 331386 Color: 1
Size: 265441 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 403092 Color: 1
Size: 302640 Color: 1
Size: 294269 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 403686 Color: 1
Size: 310181 Color: 0
Size: 286134 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 403650 Color: 0
Size: 311163 Color: 1
Size: 285188 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 403725 Color: 1
Size: 307055 Color: 0
Size: 289221 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 404300 Color: 0
Size: 318986 Color: 1
Size: 276715 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 405420 Color: 1
Size: 307400 Color: 1
Size: 287181 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 405112 Color: 0
Size: 315747 Color: 1
Size: 279142 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 406057 Color: 1
Size: 342687 Color: 1
Size: 251257 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 406424 Color: 1
Size: 339039 Color: 0
Size: 254538 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 406741 Color: 0
Size: 330448 Color: 1
Size: 262812 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 406973 Color: 1
Size: 330415 Color: 0
Size: 262613 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 408012 Color: 1
Size: 317550 Color: 1
Size: 274439 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 409508 Color: 1
Size: 330096 Color: 0
Size: 260397 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 409617 Color: 1
Size: 316487 Color: 1
Size: 273897 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 409512 Color: 0
Size: 312715 Color: 1
Size: 277774 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 409908 Color: 0
Size: 320240 Color: 1
Size: 269853 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 411138 Color: 1
Size: 306538 Color: 0
Size: 282325 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 411317 Color: 1
Size: 322540 Color: 0
Size: 266144 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 412267 Color: 0
Size: 312757 Color: 1
Size: 274977 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 411853 Color: 1
Size: 316183 Color: 0
Size: 271965 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 412685 Color: 1
Size: 328119 Color: 1
Size: 259197 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 413179 Color: 1
Size: 307820 Color: 1
Size: 279002 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 413761 Color: 1
Size: 304803 Color: 0
Size: 281437 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 413538 Color: 0
Size: 315684 Color: 1
Size: 270779 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 413609 Color: 0
Size: 333816 Color: 1
Size: 252576 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 414064 Color: 1
Size: 333850 Color: 1
Size: 252087 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 413645 Color: 0
Size: 323038 Color: 1
Size: 263318 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 414399 Color: 1
Size: 329705 Color: 0
Size: 255897 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 414602 Color: 1
Size: 323352 Color: 1
Size: 262047 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 414429 Color: 0
Size: 293729 Color: 0
Size: 291843 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 414742 Color: 1
Size: 329803 Color: 0
Size: 255456 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 415213 Color: 0
Size: 327181 Color: 0
Size: 257607 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 415207 Color: 1
Size: 299153 Color: 0
Size: 285641 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 416959 Color: 1
Size: 320584 Color: 1
Size: 262458 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 417613 Color: 0
Size: 309360 Color: 0
Size: 273028 Color: 1

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 419040 Color: 1
Size: 317254 Color: 0
Size: 263707 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 419072 Color: 0
Size: 301379 Color: 0
Size: 279550 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 419213 Color: 1
Size: 291667 Color: 0
Size: 289121 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 419154 Color: 0
Size: 298792 Color: 1
Size: 282055 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 419262 Color: 1
Size: 296353 Color: 0
Size: 284386 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 419251 Color: 0
Size: 329371 Color: 0
Size: 251379 Color: 1

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 419488 Color: 1
Size: 310445 Color: 1
Size: 270068 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 420054 Color: 0
Size: 315115 Color: 0
Size: 264832 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 420809 Color: 1
Size: 321706 Color: 0
Size: 257486 Color: 1

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 420488 Color: 0
Size: 323165 Color: 0
Size: 256348 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 420841 Color: 1
Size: 311645 Color: 1
Size: 267515 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 421635 Color: 0
Size: 321063 Color: 1
Size: 257303 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 421999 Color: 0
Size: 300819 Color: 1
Size: 277183 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 422395 Color: 0
Size: 321849 Color: 1
Size: 255757 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 422573 Color: 0
Size: 293744 Color: 0
Size: 283684 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 423001 Color: 0
Size: 313666 Color: 1
Size: 263334 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 424053 Color: 1
Size: 321233 Color: 0
Size: 254715 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 424058 Color: 1
Size: 318043 Color: 0
Size: 257900 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 423698 Color: 0
Size: 318485 Color: 0
Size: 257818 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 424129 Color: 1
Size: 320054 Color: 1
Size: 255818 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 423793 Color: 0
Size: 322472 Color: 1
Size: 253736 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 424617 Color: 1
Size: 292977 Color: 1
Size: 282407 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 425228 Color: 0
Size: 291046 Color: 0
Size: 283727 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 425715 Color: 0
Size: 308124 Color: 1
Size: 266162 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 425761 Color: 0
Size: 299406 Color: 0
Size: 274834 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 427821 Color: 1
Size: 316489 Color: 0
Size: 255691 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 428596 Color: 0
Size: 316930 Color: 1
Size: 254475 Color: 1

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 429478 Color: 0
Size: 297515 Color: 1
Size: 273008 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 430168 Color: 1
Size: 311927 Color: 0
Size: 257906 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 429967 Color: 0
Size: 303010 Color: 1
Size: 267024 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 430658 Color: 1
Size: 287141 Color: 0
Size: 282202 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 431076 Color: 1
Size: 311767 Color: 0
Size: 257158 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 431122 Color: 1
Size: 291927 Color: 0
Size: 276952 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 430992 Color: 0
Size: 293821 Color: 1
Size: 275188 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 431209 Color: 1
Size: 316367 Color: 1
Size: 252425 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 431612 Color: 1
Size: 284874 Color: 0
Size: 283515 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 431568 Color: 0
Size: 311386 Color: 0
Size: 257047 Color: 1

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 432205 Color: 0
Size: 286060 Color: 0
Size: 281736 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 433277 Color: 0
Size: 307168 Color: 0
Size: 259556 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 433646 Color: 0
Size: 298264 Color: 1
Size: 268091 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 433930 Color: 0
Size: 304650 Color: 1
Size: 261421 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 434432 Color: 1
Size: 288518 Color: 0
Size: 277051 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 435001 Color: 0
Size: 297346 Color: 1
Size: 267654 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 435315 Color: 0
Size: 314065 Color: 0
Size: 250621 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 435452 Color: 0
Size: 293244 Color: 1
Size: 271305 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 435626 Color: 1
Size: 289650 Color: 0
Size: 274725 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 435510 Color: 0
Size: 283504 Color: 1
Size: 280987 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 435810 Color: 0
Size: 308263 Color: 1
Size: 255928 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 436023 Color: 0
Size: 303659 Color: 1
Size: 260319 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 437186 Color: 1
Size: 296908 Color: 0
Size: 265907 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 437358 Color: 0
Size: 285864 Color: 0
Size: 276779 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 438684 Color: 0
Size: 283639 Color: 0
Size: 277678 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 439164 Color: 1
Size: 301467 Color: 0
Size: 259370 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 439455 Color: 1
Size: 305121 Color: 0
Size: 255425 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 439558 Color: 1
Size: 298447 Color: 0
Size: 261996 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 439934 Color: 1
Size: 299785 Color: 1
Size: 260282 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 440711 Color: 1
Size: 286940 Color: 0
Size: 272350 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 440934 Color: 0
Size: 286069 Color: 0
Size: 272998 Color: 1

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 441699 Color: 0
Size: 281442 Color: 1
Size: 276860 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 442717 Color: 1
Size: 286383 Color: 0
Size: 270901 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 442873 Color: 1
Size: 285512 Color: 0
Size: 271616 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 443103 Color: 1
Size: 285581 Color: 0
Size: 271317 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 443709 Color: 0
Size: 299858 Color: 1
Size: 256434 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 443725 Color: 1
Size: 298503 Color: 0
Size: 257773 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 443812 Color: 1
Size: 288221 Color: 1
Size: 267968 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 443855 Color: 0
Size: 289460 Color: 1
Size: 266686 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 444386 Color: 1
Size: 292217 Color: 0
Size: 263398 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 445823 Color: 1
Size: 282650 Color: 1
Size: 271528 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 446286 Color: 0
Size: 288274 Color: 1
Size: 265441 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 446449 Color: 0
Size: 284201 Color: 1
Size: 269351 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 446499 Color: 0
Size: 284990 Color: 1
Size: 268512 Color: 1

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 446623 Color: 0
Size: 284703 Color: 0
Size: 268675 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 447129 Color: 0
Size: 299204 Color: 1
Size: 253668 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 447213 Color: 1
Size: 278748 Color: 1
Size: 274040 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 447453 Color: 0
Size: 276426 Color: 1
Size: 276122 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 447669 Color: 1
Size: 301386 Color: 1
Size: 250946 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 447934 Color: 1
Size: 282128 Color: 1
Size: 269939 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 448959 Color: 1
Size: 283779 Color: 0
Size: 267263 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 449108 Color: 0
Size: 286727 Color: 0
Size: 264166 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 449759 Color: 1
Size: 293316 Color: 0
Size: 256926 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 449919 Color: 0
Size: 295143 Color: 0
Size: 254939 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 450302 Color: 1
Size: 293887 Color: 1
Size: 255812 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 450745 Color: 1
Size: 292200 Color: 0
Size: 257056 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 450855 Color: 1
Size: 276854 Color: 0
Size: 272292 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 451397 Color: 0
Size: 281983 Color: 1
Size: 266621 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 451646 Color: 0
Size: 295759 Color: 0
Size: 252596 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 451904 Color: 0
Size: 286108 Color: 0
Size: 261989 Color: 1

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 452887 Color: 1
Size: 289433 Color: 1
Size: 257681 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 454291 Color: 1
Size: 283487 Color: 0
Size: 262223 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 454505 Color: 1
Size: 279150 Color: 0
Size: 266346 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 454529 Color: 0
Size: 287545 Color: 1
Size: 257927 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 454912 Color: 1
Size: 274573 Color: 0
Size: 270516 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 456023 Color: 0
Size: 282452 Color: 1
Size: 261526 Color: 1

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 456141 Color: 0
Size: 289189 Color: 0
Size: 254671 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 456483 Color: 0
Size: 274898 Color: 1
Size: 268620 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 456899 Color: 1
Size: 292637 Color: 0
Size: 250465 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 457531 Color: 0
Size: 287173 Color: 0
Size: 255297 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 458477 Color: 1
Size: 289171 Color: 1
Size: 252353 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 458890 Color: 1
Size: 271866 Color: 1
Size: 269245 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 458684 Color: 0
Size: 275752 Color: 1
Size: 265565 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 459575 Color: 1
Size: 279877 Color: 0
Size: 260549 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 460269 Color: 0
Size: 270666 Color: 1
Size: 269066 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 460945 Color: 1
Size: 270393 Color: 0
Size: 268663 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 460960 Color: 1
Size: 271564 Color: 1
Size: 267477 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 461328 Color: 0
Size: 273523 Color: 0
Size: 265150 Color: 1

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 461815 Color: 0
Size: 272144 Color: 0
Size: 266042 Color: 1

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 462619 Color: 1
Size: 277490 Color: 0
Size: 259892 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 462833 Color: 0
Size: 280298 Color: 1
Size: 256870 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 462962 Color: 0
Size: 285917 Color: 1
Size: 251122 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 463098 Color: 1
Size: 275941 Color: 1
Size: 260962 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 463338 Color: 1
Size: 269899 Color: 0
Size: 266764 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 463811 Color: 1
Size: 270586 Color: 0
Size: 265604 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 464485 Color: 0
Size: 284643 Color: 0
Size: 250873 Color: 1

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 464950 Color: 0
Size: 279000 Color: 0
Size: 256051 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 465369 Color: 1
Size: 280347 Color: 0
Size: 254285 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 465094 Color: 0
Size: 280875 Color: 0
Size: 254032 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 466868 Color: 1
Size: 277197 Color: 1
Size: 255936 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 466921 Color: 0
Size: 276526 Color: 0
Size: 256554 Color: 1

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 468060 Color: 0
Size: 280870 Color: 1
Size: 251071 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 469717 Color: 0
Size: 268799 Color: 0
Size: 261485 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 470020 Color: 0
Size: 277532 Color: 1
Size: 252449 Color: 1

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 470326 Color: 1
Size: 264977 Color: 0
Size: 264698 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 470676 Color: 1
Size: 276085 Color: 0
Size: 253240 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 470713 Color: 1
Size: 271181 Color: 1
Size: 258107 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 471316 Color: 1
Size: 277211 Color: 1
Size: 251474 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 471622 Color: 0
Size: 267065 Color: 1
Size: 261314 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 471697 Color: 0
Size: 264517 Color: 1
Size: 263787 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 473109 Color: 1
Size: 268191 Color: 0
Size: 258701 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 473290 Color: 1
Size: 265427 Color: 0
Size: 261284 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 473487 Color: 1
Size: 263865 Color: 0
Size: 262649 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 473641 Color: 1
Size: 274335 Color: 0
Size: 252025 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 476413 Color: 0
Size: 264844 Color: 1
Size: 258744 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 476614 Color: 0
Size: 270612 Color: 0
Size: 252775 Color: 1

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 476816 Color: 0
Size: 271734 Color: 1
Size: 251451 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 476564 Color: 1
Size: 270265 Color: 0
Size: 253172 Color: 1

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 478051 Color: 0
Size: 265388 Color: 0
Size: 256562 Color: 1

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 478139 Color: 1
Size: 264000 Color: 1
Size: 257862 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 479147 Color: 1
Size: 268991 Color: 0
Size: 251863 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 479747 Color: 0
Size: 266153 Color: 0
Size: 254101 Color: 1

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 479833 Color: 1
Size: 263396 Color: 1
Size: 256772 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 479953 Color: 0
Size: 268787 Color: 1
Size: 251261 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 480260 Color: 0
Size: 265550 Color: 0
Size: 254191 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 480528 Color: 0
Size: 265887 Color: 1
Size: 253586 Color: 1

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 480974 Color: 1
Size: 260290 Color: 0
Size: 258737 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 481373 Color: 1
Size: 264056 Color: 0
Size: 254572 Color: 1

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 481857 Color: 1
Size: 267580 Color: 0
Size: 250564 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 482095 Color: 1
Size: 264750 Color: 0
Size: 253156 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 482125 Color: 0
Size: 263552 Color: 0
Size: 254324 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 482770 Color: 0
Size: 265688 Color: 1
Size: 251543 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 483465 Color: 0
Size: 265079 Color: 1
Size: 251457 Color: 1

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 483589 Color: 0
Size: 265292 Color: 1
Size: 251120 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 484595 Color: 1
Size: 262623 Color: 1
Size: 252783 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 485418 Color: 0
Size: 263982 Color: 1
Size: 250601 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 485502 Color: 0
Size: 259444 Color: 1
Size: 255055 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 486437 Color: 0
Size: 263450 Color: 1
Size: 250114 Color: 1

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 487162 Color: 0
Size: 257144 Color: 1
Size: 255695 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 488200 Color: 0
Size: 259068 Color: 1
Size: 252733 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 488943 Color: 0
Size: 259389 Color: 0
Size: 251669 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 489034 Color: 1
Size: 260470 Color: 1
Size: 250497 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 489287 Color: 1
Size: 257728 Color: 0
Size: 252986 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 489912 Color: 0
Size: 258404 Color: 1
Size: 251685 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 490358 Color: 0
Size: 258785 Color: 0
Size: 250858 Color: 1

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 491223 Color: 0
Size: 254747 Color: 1
Size: 254031 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 491617 Color: 1
Size: 255667 Color: 0
Size: 252717 Color: 1

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 492833 Color: 0
Size: 256280 Color: 1
Size: 250888 Color: 1

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 493880 Color: 1
Size: 254777 Color: 0
Size: 251344 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 494566 Color: 1
Size: 253880 Color: 0
Size: 251555 Color: 0

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 386297 Color: 0
Size: 307908 Color: 0
Size: 305795 Color: 1

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 388460 Color: 1
Size: 324591 Color: 0
Size: 286949 Color: 0

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 358282 Color: 0
Size: 325725 Color: 1
Size: 315993 Color: 0

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 410177 Color: 0
Size: 325850 Color: 0
Size: 263973 Color: 1

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 464119 Color: 1
Size: 278395 Color: 0
Size: 257486 Color: 1

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 354719 Color: 0
Size: 324690 Color: 0
Size: 320591 Color: 1

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 373819 Color: 1
Size: 352862 Color: 0
Size: 273319 Color: 1

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 460249 Color: 1
Size: 289511 Color: 1
Size: 250240 Color: 0

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 391522 Color: 1
Size: 307386 Color: 1
Size: 301092 Color: 0

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 384460 Color: 1
Size: 360344 Color: 1
Size: 255196 Color: 0

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 378059 Color: 0
Size: 339259 Color: 1
Size: 282682 Color: 0

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 451777 Color: 0
Size: 297614 Color: 1
Size: 250609 Color: 1

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 379148 Color: 0
Size: 365287 Color: 1
Size: 255565 Color: 1

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 354221 Color: 0
Size: 325356 Color: 1
Size: 320423 Color: 1

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 425313 Color: 1
Size: 295303 Color: 0
Size: 279384 Color: 1

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 435924 Color: 1
Size: 312681 Color: 1
Size: 251395 Color: 0

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 490002 Color: 1
Size: 255073 Color: 0
Size: 254925 Color: 0

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 382460 Color: 1
Size: 350678 Color: 0
Size: 266862 Color: 1

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 388449 Color: 1
Size: 339971 Color: 1
Size: 271580 Color: 0

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 361317 Color: 1
Size: 337654 Color: 0
Size: 301029 Color: 1

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 360611 Color: 1
Size: 322466 Color: 0
Size: 316923 Color: 1

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 361828 Color: 1
Size: 358837 Color: 0
Size: 279335 Color: 1

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 364449 Color: 1
Size: 338802 Color: 0
Size: 296749 Color: 1

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 365641 Color: 0
Size: 363183 Color: 0
Size: 271176 Color: 1

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 366802 Color: 1
Size: 346159 Color: 1
Size: 287039 Color: 0

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 367509 Color: 1
Size: 362892 Color: 0
Size: 269599 Color: 1

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 368173 Color: 0
Size: 340407 Color: 0
Size: 291420 Color: 1

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 371286 Color: 1
Size: 330911 Color: 0
Size: 297803 Color: 1

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 372220 Color: 0
Size: 327447 Color: 0
Size: 300333 Color: 1

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 373473 Color: 1
Size: 318720 Color: 1
Size: 307807 Color: 0

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 374212 Color: 1
Size: 346970 Color: 0
Size: 278818 Color: 1

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 375237 Color: 1
Size: 314408 Color: 1
Size: 310355 Color: 0

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 378204 Color: 0
Size: 366286 Color: 1
Size: 255510 Color: 1

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 378598 Color: 1
Size: 325074 Color: 1
Size: 296328 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 379122 Color: 0
Size: 315852 Color: 0
Size: 305026 Color: 1

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 380178 Color: 0
Size: 326925 Color: 0
Size: 292897 Color: 1

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 381461 Color: 1
Size: 313583 Color: 0
Size: 304956 Color: 1

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 382506 Color: 0
Size: 318768 Color: 1
Size: 298726 Color: 0

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 386562 Color: 0
Size: 349038 Color: 1
Size: 264400 Color: 0

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 386849 Color: 0
Size: 341031 Color: 1
Size: 272120 Color: 0

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 391173 Color: 0
Size: 358122 Color: 1
Size: 250705 Color: 0

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 391325 Color: 1
Size: 332760 Color: 0
Size: 275915 Color: 1

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 392343 Color: 1
Size: 334586 Color: 1
Size: 273071 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 392235 Color: 0
Size: 319722 Color: 0
Size: 288043 Color: 1

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 394344 Color: 1
Size: 314155 Color: 0
Size: 291501 Color: 1

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 394581 Color: 1
Size: 350579 Color: 1
Size: 254840 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 398077 Color: 1
Size: 344610 Color: 1
Size: 257313 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 399280 Color: 0
Size: 348223 Color: 1
Size: 252497 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 401434 Color: 1
Size: 299884 Color: 0
Size: 298682 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 401580 Color: 0
Size: 327726 Color: 0
Size: 270694 Color: 1

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 403651 Color: 1
Size: 310074 Color: 0
Size: 286275 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 404459 Color: 0
Size: 326693 Color: 0
Size: 268848 Color: 1

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 406334 Color: 1
Size: 316104 Color: 0
Size: 277562 Color: 1

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 408157 Color: 1
Size: 298584 Color: 0
Size: 293259 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 409484 Color: 0
Size: 302640 Color: 1
Size: 287876 Color: 0

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 413471 Color: 0
Size: 336051 Color: 0
Size: 250478 Color: 1

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 417228 Color: 1
Size: 298617 Color: 1
Size: 284155 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 420318 Color: 1
Size: 322134 Color: 0
Size: 257548 Color: 0

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 422890 Color: 1
Size: 295323 Color: 0
Size: 281787 Color: 1

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 424102 Color: 1
Size: 298077 Color: 0
Size: 277821 Color: 1

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 424704 Color: 1
Size: 295862 Color: 0
Size: 279434 Color: 0

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 432157 Color: 1
Size: 317023 Color: 0
Size: 250820 Color: 1

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 433351 Color: 1
Size: 315538 Color: 1
Size: 251111 Color: 0

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 434252 Color: 1
Size: 300970 Color: 0
Size: 264778 Color: 0

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 435497 Color: 0
Size: 306825 Color: 1
Size: 257678 Color: 0

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 439374 Color: 0
Size: 299986 Color: 0
Size: 260640 Color: 1

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 439914 Color: 1
Size: 307657 Color: 0
Size: 252429 Color: 0

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 440876 Color: 1
Size: 297553 Color: 0
Size: 261571 Color: 1

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 442157 Color: 0
Size: 282942 Color: 1
Size: 274901 Color: 0

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 444674 Color: 1
Size: 303611 Color: 0
Size: 251715 Color: 0

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 447426 Color: 0
Size: 287549 Color: 0
Size: 265025 Color: 1

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 448473 Color: 1
Size: 278936 Color: 0
Size: 272591 Color: 0

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 454283 Color: 1
Size: 282732 Color: 0
Size: 262985 Color: 0

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 458111 Color: 1
Size: 274504 Color: 0
Size: 267385 Color: 0

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 462036 Color: 0
Size: 268998 Color: 1
Size: 268966 Color: 1

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 464003 Color: 0
Size: 268625 Color: 1
Size: 267372 Color: 0

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 464445 Color: 0
Size: 270914 Color: 1
Size: 264641 Color: 1

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 466476 Color: 1
Size: 282512 Color: 1
Size: 251012 Color: 0

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 467718 Color: 1
Size: 273501 Color: 1
Size: 258781 Color: 0

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 468130 Color: 1
Size: 277404 Color: 1
Size: 254466 Color: 0

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 483734 Color: 0
Size: 265260 Color: 1
Size: 251006 Color: 1

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 484253 Color: 1
Size: 261915 Color: 0
Size: 253832 Color: 0

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 493740 Color: 1
Size: 253522 Color: 1
Size: 252738 Color: 0

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 494782 Color: 1
Size: 254748 Color: 1
Size: 250470 Color: 0

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 351258 Color: 0
Size: 350961 Color: 1
Size: 297781 Color: 1

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 351940 Color: 1
Size: 339683 Color: 1
Size: 308377 Color: 0

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 365423 Color: 0
Size: 349514 Color: 0
Size: 285063 Color: 1

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 453946 Color: 1
Size: 278988 Color: 0
Size: 267066 Color: 0

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 340963 Color: 1
Size: 329978 Color: 1
Size: 329059 Color: 0

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 335149 Color: 1
Size: 334689 Color: 0
Size: 330162 Color: 0

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 478473 Color: 0
Size: 267751 Color: 0
Size: 253776 Color: 1

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 388866 Color: 0
Size: 329300 Color: 1
Size: 281833 Color: 1

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 376139 Color: 1
Size: 315352 Color: 0
Size: 308508 Color: 1

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 460815 Color: 1
Size: 274746 Color: 0
Size: 264438 Color: 1

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 389137 Color: 1
Size: 318440 Color: 1
Size: 292422 Color: 0

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 392597 Color: 1
Size: 346215 Color: 0
Size: 261187 Color: 1

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 374274 Color: 1
Size: 334594 Color: 1
Size: 291131 Color: 0

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 393207 Color: 0
Size: 338786 Color: 0
Size: 268006 Color: 1

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 367564 Color: 1
Size: 333099 Color: 1
Size: 299336 Color: 0

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 372740 Color: 0
Size: 340740 Color: 1
Size: 286519 Color: 0

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 375232 Color: 0
Size: 341184 Color: 0
Size: 283583 Color: 1

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 376059 Color: 0
Size: 317672 Color: 1
Size: 306268 Color: 0

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 377149 Color: 1
Size: 348749 Color: 1
Size: 274101 Color: 0

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 380597 Color: 0
Size: 323564 Color: 1
Size: 295838 Color: 1

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 382798 Color: 1
Size: 344376 Color: 0
Size: 272825 Color: 1

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 385955 Color: 1
Size: 317523 Color: 0
Size: 296521 Color: 1

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 390485 Color: 1
Size: 328750 Color: 0
Size: 280764 Color: 1

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 392133 Color: 0
Size: 308673 Color: 0
Size: 299193 Color: 1

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 393184 Color: 1
Size: 324488 Color: 0
Size: 282327 Color: 1

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 393213 Color: 1
Size: 340611 Color: 1
Size: 266175 Color: 0

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 397430 Color: 1
Size: 343766 Color: 1
Size: 258803 Color: 0

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 402849 Color: 0
Size: 307266 Color: 0
Size: 289884 Color: 1

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 411413 Color: 0
Size: 329526 Color: 1
Size: 259060 Color: 0

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 412010 Color: 0
Size: 337462 Color: 1
Size: 250527 Color: 1

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 421430 Color: 0
Size: 322783 Color: 0
Size: 255786 Color: 1

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 422197 Color: 1
Size: 318899 Color: 1
Size: 258903 Color: 0

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 426664 Color: 0
Size: 307184 Color: 0
Size: 266151 Color: 1

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 432073 Color: 1
Size: 292087 Color: 0
Size: 275839 Color: 1

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 435375 Color: 0
Size: 284001 Color: 0
Size: 280623 Color: 1

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 436992 Color: 0
Size: 300157 Color: 1
Size: 262850 Color: 1

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 442652 Color: 1
Size: 287523 Color: 1
Size: 269824 Color: 0

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 447792 Color: 0
Size: 292195 Color: 1
Size: 260012 Color: 0

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 468174 Color: 0
Size: 276802 Color: 1
Size: 255023 Color: 1

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 471141 Color: 1
Size: 266798 Color: 0
Size: 262060 Color: 0

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 473224 Color: 1
Size: 274190 Color: 0
Size: 252585 Color: 1

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 476348 Color: 1
Size: 262932 Color: 0
Size: 260719 Color: 1

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 351141 Color: 0
Size: 333070 Color: 1
Size: 315788 Color: 0

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 491708 Color: 1
Size: 256001 Color: 1
Size: 252290 Color: 0

Bin 504: 3 of cap free
Amount of items: 3
Items: 
Size: 374342 Color: 0
Size: 372993 Color: 1
Size: 252663 Color: 1

Bin 505: 3 of cap free
Amount of items: 3
Items: 
Size: 381631 Color: 0
Size: 366812 Color: 1
Size: 251555 Color: 0

Bin 506: 3 of cap free
Amount of items: 3
Items: 
Size: 371980 Color: 1
Size: 344543 Color: 1
Size: 283475 Color: 0

Bin 507: 3 of cap free
Amount of items: 3
Items: 
Size: 376447 Color: 1
Size: 314094 Color: 0
Size: 309457 Color: 0

Bin 508: 3 of cap free
Amount of items: 3
Items: 
Size: 343504 Color: 1
Size: 330504 Color: 0
Size: 325990 Color: 0

Bin 509: 3 of cap free
Amount of items: 3
Items: 
Size: 382583 Color: 0
Size: 355761 Color: 1
Size: 261654 Color: 0

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 365219 Color: 0
Size: 346427 Color: 1
Size: 288352 Color: 1

Bin 511: 3 of cap free
Amount of items: 3
Items: 
Size: 367457 Color: 0
Size: 352689 Color: 0
Size: 279852 Color: 1

Bin 512: 3 of cap free
Amount of items: 3
Items: 
Size: 375197 Color: 1
Size: 366051 Color: 0
Size: 258750 Color: 1

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 379367 Color: 1
Size: 352522 Color: 0
Size: 268109 Color: 1

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 380882 Color: 1
Size: 365589 Color: 1
Size: 253527 Color: 0

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 386827 Color: 1
Size: 346904 Color: 1
Size: 266267 Color: 0

Bin 516: 3 of cap free
Amount of items: 3
Items: 
Size: 389807 Color: 0
Size: 337072 Color: 1
Size: 273119 Color: 0

Bin 517: 3 of cap free
Amount of items: 3
Items: 
Size: 395904 Color: 0
Size: 347970 Color: 0
Size: 256124 Color: 1

Bin 518: 3 of cap free
Amount of items: 3
Items: 
Size: 396189 Color: 1
Size: 331673 Color: 1
Size: 272136 Color: 0

Bin 519: 3 of cap free
Amount of items: 3
Items: 
Size: 405592 Color: 0
Size: 343105 Color: 1
Size: 251301 Color: 0

Bin 520: 3 of cap free
Amount of items: 3
Items: 
Size: 411721 Color: 0
Size: 308757 Color: 1
Size: 279520 Color: 1

Bin 521: 3 of cap free
Amount of items: 3
Items: 
Size: 412041 Color: 0
Size: 318113 Color: 0
Size: 269844 Color: 1

Bin 522: 3 of cap free
Amount of items: 3
Items: 
Size: 422521 Color: 0
Size: 299290 Color: 1
Size: 278187 Color: 1

Bin 523: 3 of cap free
Amount of items: 3
Items: 
Size: 427048 Color: 0
Size: 309622 Color: 1
Size: 263328 Color: 1

Bin 524: 3 of cap free
Amount of items: 3
Items: 
Size: 438520 Color: 1
Size: 311234 Color: 0
Size: 250244 Color: 1

Bin 525: 3 of cap free
Amount of items: 3
Items: 
Size: 450307 Color: 0
Size: 284696 Color: 1
Size: 264995 Color: 1

Bin 526: 3 of cap free
Amount of items: 3
Items: 
Size: 462719 Color: 1
Size: 283938 Color: 0
Size: 253341 Color: 1

Bin 527: 3 of cap free
Amount of items: 3
Items: 
Size: 482613 Color: 0
Size: 265047 Color: 1
Size: 252338 Color: 1

Bin 528: 3 of cap free
Amount of items: 3
Items: 
Size: 412422 Color: 0
Size: 314243 Color: 1
Size: 273333 Color: 0

Bin 529: 3 of cap free
Amount of items: 3
Items: 
Size: 362531 Color: 0
Size: 342615 Color: 1
Size: 294852 Color: 1

Bin 530: 3 of cap free
Amount of items: 3
Items: 
Size: 366756 Color: 1
Size: 357675 Color: 0
Size: 275567 Color: 1

Bin 531: 3 of cap free
Amount of items: 3
Items: 
Size: 368631 Color: 1
Size: 361059 Color: 0
Size: 270308 Color: 0

Bin 532: 4 of cap free
Amount of items: 3
Items: 
Size: 357968 Color: 1
Size: 353047 Color: 0
Size: 288982 Color: 1

Bin 533: 4 of cap free
Amount of items: 3
Items: 
Size: 407036 Color: 0
Size: 301132 Color: 0
Size: 291829 Color: 1

Bin 534: 4 of cap free
Amount of items: 3
Items: 
Size: 389894 Color: 0
Size: 335622 Color: 1
Size: 274481 Color: 1

Bin 535: 4 of cap free
Amount of items: 3
Items: 
Size: 400381 Color: 0
Size: 342458 Color: 1
Size: 257158 Color: 0

Bin 536: 4 of cap free
Amount of items: 3
Items: 
Size: 363449 Color: 0
Size: 325854 Color: 0
Size: 310694 Color: 1

Bin 537: 4 of cap free
Amount of items: 3
Items: 
Size: 365439 Color: 1
Size: 353312 Color: 0
Size: 281246 Color: 1

Bin 538: 4 of cap free
Amount of items: 3
Items: 
Size: 370616 Color: 0
Size: 344461 Color: 1
Size: 284920 Color: 0

Bin 539: 4 of cap free
Amount of items: 3
Items: 
Size: 374408 Color: 0
Size: 372557 Color: 0
Size: 253032 Color: 1

Bin 540: 4 of cap free
Amount of items: 3
Items: 
Size: 374304 Color: 1
Size: 331228 Color: 0
Size: 294465 Color: 1

Bin 541: 4 of cap free
Amount of items: 3
Items: 
Size: 376744 Color: 0
Size: 354967 Color: 0
Size: 268286 Color: 1

Bin 542: 4 of cap free
Amount of items: 3
Items: 
Size: 381801 Color: 1
Size: 312864 Color: 1
Size: 305332 Color: 0

Bin 543: 4 of cap free
Amount of items: 3
Items: 
Size: 412667 Color: 1
Size: 302482 Color: 0
Size: 284848 Color: 0

Bin 544: 4 of cap free
Amount of items: 3
Items: 
Size: 470303 Color: 0
Size: 267098 Color: 1
Size: 262596 Color: 1

Bin 545: 4 of cap free
Amount of items: 3
Items: 
Size: 360096 Color: 1
Size: 332310 Color: 0
Size: 307591 Color: 0

Bin 546: 4 of cap free
Amount of items: 3
Items: 
Size: 361842 Color: 0
Size: 335976 Color: 1
Size: 302179 Color: 1

Bin 547: 5 of cap free
Amount of items: 3
Items: 
Size: 396058 Color: 1
Size: 311664 Color: 1
Size: 292274 Color: 0

Bin 548: 5 of cap free
Amount of items: 3
Items: 
Size: 487715 Color: 0
Size: 261122 Color: 1
Size: 251159 Color: 1

Bin 549: 5 of cap free
Amount of items: 3
Items: 
Size: 374242 Color: 1
Size: 327172 Color: 0
Size: 298582 Color: 1

Bin 550: 5 of cap free
Amount of items: 3
Items: 
Size: 375475 Color: 0
Size: 337155 Color: 1
Size: 287366 Color: 0

Bin 551: 5 of cap free
Amount of items: 3
Items: 
Size: 381273 Color: 1
Size: 317953 Color: 0
Size: 300770 Color: 1

Bin 552: 5 of cap free
Amount of items: 3
Items: 
Size: 382354 Color: 1
Size: 340689 Color: 0
Size: 276953 Color: 0

Bin 553: 5 of cap free
Amount of items: 3
Items: 
Size: 383273 Color: 0
Size: 332071 Color: 0
Size: 284652 Color: 1

Bin 554: 5 of cap free
Amount of items: 3
Items: 
Size: 386827 Color: 1
Size: 344457 Color: 0
Size: 268712 Color: 1

Bin 555: 5 of cap free
Amount of items: 3
Items: 
Size: 393982 Color: 0
Size: 313356 Color: 0
Size: 292658 Color: 1

Bin 556: 5 of cap free
Amount of items: 3
Items: 
Size: 412169 Color: 0
Size: 309534 Color: 1
Size: 278293 Color: 1

Bin 557: 5 of cap free
Amount of items: 3
Items: 
Size: 456435 Color: 0
Size: 276483 Color: 1
Size: 267078 Color: 1

Bin 558: 5 of cap free
Amount of items: 3
Items: 
Size: 338243 Color: 0
Size: 337633 Color: 1
Size: 324120 Color: 1

Bin 559: 6 of cap free
Amount of items: 3
Items: 
Size: 364694 Color: 1
Size: 346910 Color: 0
Size: 288391 Color: 0

Bin 560: 6 of cap free
Amount of items: 3
Items: 
Size: 368959 Color: 1
Size: 359589 Color: 0
Size: 271447 Color: 1

Bin 561: 6 of cap free
Amount of items: 3
Items: 
Size: 375038 Color: 1
Size: 341081 Color: 0
Size: 283876 Color: 1

Bin 562: 6 of cap free
Amount of items: 3
Items: 
Size: 378294 Color: 0
Size: 358086 Color: 1
Size: 263615 Color: 0

Bin 563: 6 of cap free
Amount of items: 3
Items: 
Size: 379946 Color: 0
Size: 323128 Color: 1
Size: 296921 Color: 0

Bin 564: 6 of cap free
Amount of items: 3
Items: 
Size: 407319 Color: 1
Size: 330241 Color: 0
Size: 262435 Color: 0

Bin 565: 6 of cap free
Amount of items: 3
Items: 
Size: 347843 Color: 1
Size: 343807 Color: 1
Size: 308345 Color: 0

Bin 566: 6 of cap free
Amount of items: 3
Items: 
Size: 360098 Color: 0
Size: 326364 Color: 0
Size: 313533 Color: 1

Bin 567: 7 of cap free
Amount of items: 3
Items: 
Size: 367415 Color: 0
Size: 353483 Color: 0
Size: 279096 Color: 1

Bin 568: 7 of cap free
Amount of items: 3
Items: 
Size: 371253 Color: 1
Size: 320660 Color: 1
Size: 308081 Color: 0

Bin 569: 7 of cap free
Amount of items: 3
Items: 
Size: 373578 Color: 1
Size: 331991 Color: 0
Size: 294425 Color: 1

Bin 570: 7 of cap free
Amount of items: 3
Items: 
Size: 376871 Color: 1
Size: 337715 Color: 0
Size: 285408 Color: 1

Bin 571: 7 of cap free
Amount of items: 3
Items: 
Size: 418479 Color: 1
Size: 325759 Color: 1
Size: 255756 Color: 0

Bin 572: 7 of cap free
Amount of items: 3
Items: 
Size: 357954 Color: 1
Size: 336094 Color: 0
Size: 305946 Color: 1

Bin 573: 8 of cap free
Amount of items: 3
Items: 
Size: 363094 Color: 0
Size: 362240 Color: 0
Size: 274659 Color: 1

Bin 574: 8 of cap free
Amount of items: 3
Items: 
Size: 383300 Color: 1
Size: 331846 Color: 1
Size: 284847 Color: 0

Bin 575: 8 of cap free
Amount of items: 3
Items: 
Size: 403965 Color: 1
Size: 315341 Color: 0
Size: 280687 Color: 0

Bin 576: 8 of cap free
Amount of items: 3
Items: 
Size: 349633 Color: 1
Size: 334184 Color: 1
Size: 316176 Color: 0

Bin 577: 9 of cap free
Amount of items: 3
Items: 
Size: 414703 Color: 1
Size: 322739 Color: 1
Size: 262550 Color: 0

Bin 578: 9 of cap free
Amount of items: 3
Items: 
Size: 375782 Color: 0
Size: 363681 Color: 1
Size: 260529 Color: 1

Bin 579: 9 of cap free
Amount of items: 3
Items: 
Size: 359512 Color: 0
Size: 331407 Color: 1
Size: 309073 Color: 0

Bin 580: 9 of cap free
Amount of items: 3
Items: 
Size: 356148 Color: 0
Size: 356062 Color: 1
Size: 287782 Color: 0

Bin 581: 11 of cap free
Amount of items: 3
Items: 
Size: 356816 Color: 1
Size: 329772 Color: 0
Size: 313402 Color: 0

Bin 582: 11 of cap free
Amount of items: 3
Items: 
Size: 349159 Color: 0
Size: 329404 Color: 1
Size: 321427 Color: 0

Bin 583: 11 of cap free
Amount of items: 3
Items: 
Size: 346192 Color: 1
Size: 332015 Color: 0
Size: 321783 Color: 0

Bin 584: 11 of cap free
Amount of items: 3
Items: 
Size: 368617 Color: 1
Size: 347145 Color: 1
Size: 284228 Color: 0

Bin 585: 13 of cap free
Amount of items: 3
Items: 
Size: 360201 Color: 1
Size: 326865 Color: 0
Size: 312922 Color: 1

Bin 586: 14 of cap free
Amount of items: 3
Items: 
Size: 362888 Color: 0
Size: 320777 Color: 0
Size: 316322 Color: 1

Bin 587: 14 of cap free
Amount of items: 3
Items: 
Size: 361402 Color: 1
Size: 330472 Color: 0
Size: 308113 Color: 1

Bin 588: 14 of cap free
Amount of items: 3
Items: 
Size: 356321 Color: 1
Size: 322174 Color: 1
Size: 321492 Color: 0

Bin 589: 15 of cap free
Amount of items: 3
Items: 
Size: 355180 Color: 0
Size: 350549 Color: 1
Size: 294257 Color: 0

Bin 590: 15 of cap free
Amount of items: 3
Items: 
Size: 371665 Color: 1
Size: 368067 Color: 0
Size: 260254 Color: 1

Bin 591: 15 of cap free
Amount of items: 3
Items: 
Size: 373203 Color: 1
Size: 365970 Color: 0
Size: 260813 Color: 1

Bin 592: 15 of cap free
Amount of items: 3
Items: 
Size: 408250 Color: 1
Size: 327066 Color: 0
Size: 264670 Color: 0

Bin 593: 15 of cap free
Amount of items: 3
Items: 
Size: 347595 Color: 0
Size: 341238 Color: 1
Size: 311153 Color: 1

Bin 594: 16 of cap free
Amount of items: 3
Items: 
Size: 495382 Color: 1
Size: 253604 Color: 0
Size: 250999 Color: 1

Bin 595: 16 of cap free
Amount of items: 3
Items: 
Size: 359952 Color: 0
Size: 321769 Color: 1
Size: 318264 Color: 0

Bin 596: 16 of cap free
Amount of items: 3
Items: 
Size: 335106 Color: 0
Size: 333979 Color: 1
Size: 330900 Color: 1

Bin 597: 17 of cap free
Amount of items: 3
Items: 
Size: 356537 Color: 0
Size: 339708 Color: 1
Size: 303739 Color: 0

Bin 598: 17 of cap free
Amount of items: 3
Items: 
Size: 357766 Color: 1
Size: 328796 Color: 1
Size: 313422 Color: 0

Bin 599: 18 of cap free
Amount of items: 3
Items: 
Size: 364725 Color: 1
Size: 321383 Color: 0
Size: 313875 Color: 1

Bin 600: 19 of cap free
Amount of items: 3
Items: 
Size: 405478 Color: 1
Size: 301641 Color: 0
Size: 292863 Color: 1

Bin 601: 19 of cap free
Amount of items: 3
Items: 
Size: 365054 Color: 0
Size: 354627 Color: 0
Size: 280301 Color: 1

Bin 602: 21 of cap free
Amount of items: 3
Items: 
Size: 364354 Color: 1
Size: 343387 Color: 0
Size: 292239 Color: 0

Bin 603: 21 of cap free
Amount of items: 3
Items: 
Size: 361031 Color: 0
Size: 322761 Color: 0
Size: 316188 Color: 1

Bin 604: 23 of cap free
Amount of items: 3
Items: 
Size: 480220 Color: 1
Size: 265687 Color: 0
Size: 254071 Color: 0

Bin 605: 23 of cap free
Amount of items: 3
Items: 
Size: 356868 Color: 1
Size: 324649 Color: 0
Size: 318461 Color: 1

Bin 606: 24 of cap free
Amount of items: 3
Items: 
Size: 368657 Color: 1
Size: 341493 Color: 0
Size: 289827 Color: 0

Bin 607: 25 of cap free
Amount of items: 3
Items: 
Size: 366367 Color: 1
Size: 342579 Color: 0
Size: 291030 Color: 0

Bin 608: 25 of cap free
Amount of items: 3
Items: 
Size: 362291 Color: 1
Size: 343571 Color: 1
Size: 294114 Color: 0

Bin 609: 28 of cap free
Amount of items: 3
Items: 
Size: 364409 Color: 1
Size: 320055 Color: 0
Size: 315509 Color: 1

Bin 610: 29 of cap free
Amount of items: 3
Items: 
Size: 368136 Color: 1
Size: 357704 Color: 1
Size: 274132 Color: 0

Bin 611: 33 of cap free
Amount of items: 3
Items: 
Size: 369049 Color: 0
Size: 349483 Color: 1
Size: 281436 Color: 1

Bin 612: 34 of cap free
Amount of items: 3
Items: 
Size: 356896 Color: 1
Size: 353496 Color: 0
Size: 289575 Color: 0

Bin 613: 34 of cap free
Amount of items: 3
Items: 
Size: 351878 Color: 1
Size: 341423 Color: 1
Size: 306666 Color: 0

Bin 614: 36 of cap free
Amount of items: 3
Items: 
Size: 365132 Color: 1
Size: 364049 Color: 1
Size: 270784 Color: 0

Bin 615: 37 of cap free
Amount of items: 3
Items: 
Size: 357083 Color: 0
Size: 351227 Color: 1
Size: 291654 Color: 1

Bin 616: 41 of cap free
Amount of items: 3
Items: 
Size: 368594 Color: 0
Size: 367986 Color: 1
Size: 263380 Color: 0

Bin 617: 43 of cap free
Amount of items: 3
Items: 
Size: 482630 Color: 0
Size: 266283 Color: 0
Size: 251045 Color: 1

Bin 618: 47 of cap free
Amount of items: 3
Items: 
Size: 368778 Color: 1
Size: 346727 Color: 0
Size: 284449 Color: 1

Bin 619: 55 of cap free
Amount of items: 3
Items: 
Size: 454175 Color: 0
Size: 292502 Color: 1
Size: 253269 Color: 0

Bin 620: 56 of cap free
Amount of items: 3
Items: 
Size: 351036 Color: 1
Size: 346520 Color: 0
Size: 302389 Color: 0

Bin 621: 60 of cap free
Amount of items: 3
Items: 
Size: 363409 Color: 0
Size: 358167 Color: 1
Size: 278365 Color: 1

Bin 622: 67 of cap free
Amount of items: 3
Items: 
Size: 382680 Color: 1
Size: 358229 Color: 1
Size: 259025 Color: 0

Bin 623: 70 of cap free
Amount of items: 3
Items: 
Size: 495517 Color: 0
Size: 254198 Color: 0
Size: 250216 Color: 1

Bin 624: 75 of cap free
Amount of items: 3
Items: 
Size: 345480 Color: 0
Size: 340655 Color: 1
Size: 313791 Color: 0

Bin 625: 87 of cap free
Amount of items: 3
Items: 
Size: 346006 Color: 0
Size: 333229 Color: 0
Size: 320679 Color: 1

Bin 626: 88 of cap free
Amount of items: 3
Items: 
Size: 496974 Color: 1
Size: 252258 Color: 1
Size: 250681 Color: 0

Bin 627: 90 of cap free
Amount of items: 3
Items: 
Size: 452466 Color: 0
Size: 277381 Color: 1
Size: 270064 Color: 1

Bin 628: 92 of cap free
Amount of items: 3
Items: 
Size: 432775 Color: 0
Size: 308739 Color: 0
Size: 258395 Color: 1

Bin 629: 100 of cap free
Amount of items: 3
Items: 
Size: 461662 Color: 0
Size: 271134 Color: 1
Size: 267105 Color: 1

Bin 630: 102 of cap free
Amount of items: 3
Items: 
Size: 344486 Color: 0
Size: 336175 Color: 1
Size: 319238 Color: 1

Bin 631: 106 of cap free
Amount of items: 3
Items: 
Size: 381359 Color: 1
Size: 361815 Color: 0
Size: 256721 Color: 0

Bin 632: 112 of cap free
Amount of items: 3
Items: 
Size: 431562 Color: 1
Size: 316826 Color: 1
Size: 251501 Color: 0

Bin 633: 113 of cap free
Amount of items: 3
Items: 
Size: 494664 Color: 0
Size: 252683 Color: 1
Size: 252541 Color: 1

Bin 634: 115 of cap free
Amount of items: 3
Items: 
Size: 357307 Color: 0
Size: 341598 Color: 0
Size: 300981 Color: 1

Bin 635: 116 of cap free
Amount of items: 3
Items: 
Size: 362140 Color: 0
Size: 324367 Color: 0
Size: 313378 Color: 1

Bin 636: 125 of cap free
Amount of items: 3
Items: 
Size: 375549 Color: 0
Size: 323983 Color: 0
Size: 300344 Color: 1

Bin 637: 126 of cap free
Amount of items: 3
Items: 
Size: 342652 Color: 0
Size: 332288 Color: 0
Size: 324935 Color: 1

Bin 638: 130 of cap free
Amount of items: 3
Items: 
Size: 358934 Color: 1
Size: 329752 Color: 0
Size: 311185 Color: 1

Bin 639: 135 of cap free
Amount of items: 3
Items: 
Size: 497185 Color: 1
Size: 251707 Color: 0
Size: 250974 Color: 1

Bin 640: 141 of cap free
Amount of items: 3
Items: 
Size: 376012 Color: 0
Size: 356299 Color: 0
Size: 267549 Color: 1

Bin 641: 141 of cap free
Amount of items: 3
Items: 
Size: 366364 Color: 0
Size: 321020 Color: 1
Size: 312476 Color: 1

Bin 642: 160 of cap free
Amount of items: 3
Items: 
Size: 425916 Color: 0
Size: 318518 Color: 0
Size: 255407 Color: 1

Bin 643: 174 of cap free
Amount of items: 3
Items: 
Size: 367279 Color: 1
Size: 339491 Color: 0
Size: 293057 Color: 1

Bin 644: 175 of cap free
Amount of items: 3
Items: 
Size: 382623 Color: 0
Size: 318067 Color: 1
Size: 299136 Color: 1

Bin 645: 188 of cap free
Amount of items: 3
Items: 
Size: 354719 Color: 1
Size: 328861 Color: 0
Size: 316233 Color: 0

Bin 646: 218 of cap free
Amount of items: 3
Items: 
Size: 408233 Color: 1
Size: 302041 Color: 0
Size: 289509 Color: 0

Bin 647: 232 of cap free
Amount of items: 3
Items: 
Size: 336919 Color: 0
Size: 333590 Color: 1
Size: 329260 Color: 1

Bin 648: 247 of cap free
Amount of items: 3
Items: 
Size: 363972 Color: 0
Size: 344233 Color: 0
Size: 291549 Color: 1

Bin 649: 285 of cap free
Amount of items: 3
Items: 
Size: 369410 Color: 0
Size: 342911 Color: 1
Size: 287395 Color: 0

Bin 650: 309 of cap free
Amount of items: 3
Items: 
Size: 458862 Color: 1
Size: 284905 Color: 1
Size: 255925 Color: 0

Bin 651: 364 of cap free
Amount of items: 3
Items: 
Size: 357103 Color: 1
Size: 356224 Color: 0
Size: 286310 Color: 0

Bin 652: 375 of cap free
Amount of items: 3
Items: 
Size: 384058 Color: 1
Size: 325320 Color: 1
Size: 290248 Color: 0

Bin 653: 386 of cap free
Amount of items: 3
Items: 
Size: 404723 Color: 1
Size: 320822 Color: 0
Size: 274070 Color: 0

Bin 654: 437 of cap free
Amount of items: 3
Items: 
Size: 443033 Color: 1
Size: 280742 Color: 1
Size: 275789 Color: 0

Bin 655: 448 of cap free
Amount of items: 3
Items: 
Size: 358165 Color: 0
Size: 332934 Color: 1
Size: 308454 Color: 1

Bin 656: 911 of cap free
Amount of items: 3
Items: 
Size: 393368 Color: 0
Size: 341128 Color: 0
Size: 264594 Color: 1

Bin 657: 1657 of cap free
Amount of items: 3
Items: 
Size: 389906 Color: 0
Size: 335524 Color: 1
Size: 272914 Color: 0

Bin 658: 1859 of cap free
Amount of items: 2
Items: 
Size: 499292 Color: 0
Size: 498850 Color: 1

Bin 659: 2110 of cap free
Amount of items: 3
Items: 
Size: 402159 Color: 1
Size: 303343 Color: 0
Size: 292389 Color: 0

Bin 660: 2197 of cap free
Amount of items: 3
Items: 
Size: 494378 Color: 0
Size: 251726 Color: 1
Size: 251700 Color: 0

Bin 661: 3312 of cap free
Amount of items: 3
Items: 
Size: 474876 Color: 0
Size: 263230 Color: 0
Size: 258583 Color: 1

Bin 662: 4975 of cap free
Amount of items: 3
Items: 
Size: 352589 Color: 0
Size: 351215 Color: 0
Size: 291222 Color: 1

Bin 663: 5081 of cap free
Amount of items: 2
Items: 
Size: 498135 Color: 1
Size: 496785 Color: 0

Bin 664: 109362 of cap free
Amount of items: 3
Items: 
Size: 313898 Color: 1
Size: 290598 Color: 1
Size: 286143 Color: 0

Bin 665: 173525 of cap free
Amount of items: 3
Items: 
Size: 284179 Color: 0
Size: 274427 Color: 1
Size: 267870 Color: 0

Bin 666: 212549 of cap free
Amount of items: 3
Items: 
Size: 263436 Color: 1
Size: 262243 Color: 0
Size: 261773 Color: 0

Bin 667: 232578 of cap free
Amount of items: 3
Items: 
Size: 261718 Color: 0
Size: 255007 Color: 1
Size: 250698 Color: 1

Bin 668: 242222 of cap free
Amount of items: 2
Items: 
Size: 498040 Color: 1
Size: 259739 Color: 0

Total size: 667000667
Total free space: 1000001

