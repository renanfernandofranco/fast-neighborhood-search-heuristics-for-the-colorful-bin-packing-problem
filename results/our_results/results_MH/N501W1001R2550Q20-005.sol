Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 342 Color: 13
Size: 292 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 4
Size: 327 Color: 13
Size: 287 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 5
Size: 286 Color: 17
Size: 250 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 19
Size: 290 Color: 1
Size: 290 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 11
Size: 338 Color: 2
Size: 250 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 19
Size: 336 Color: 6
Size: 259 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 14
Size: 295 Color: 7
Size: 270 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 8
Size: 353 Color: 18
Size: 281 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 7
Size: 327 Color: 6
Size: 326 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 8
Size: 339 Color: 10
Size: 252 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 8
Size: 358 Color: 7
Size: 282 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 260 Color: 11
Size: 257 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 345 Color: 7
Size: 292 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7
Size: 366 Color: 18
Size: 254 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 10
Size: 298 Color: 5
Size: 273 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 13
Size: 325 Color: 6
Size: 276 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 5
Size: 315 Color: 18
Size: 269 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 5
Size: 300 Color: 10
Size: 262 Color: 9

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 7
Size: 344 Color: 0
Size: 254 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 298 Color: 0
Size: 265 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 310 Color: 15
Size: 294 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 14
Size: 326 Color: 15
Size: 276 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 331 Color: 10
Size: 254 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 12
Size: 259 Color: 17
Size: 257 Color: 12

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 14
Size: 271 Color: 14
Size: 255 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 315 Color: 3
Size: 254 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 328 Color: 16
Size: 256 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 310 Color: 18
Size: 259 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 2
Size: 337 Color: 2
Size: 290 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 310 Color: 4
Size: 280 Color: 15

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 17
Size: 292 Color: 13
Size: 262 Color: 10

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 2
Size: 330 Color: 13
Size: 267 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 11
Size: 353 Color: 18
Size: 291 Color: 17

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 15
Size: 259 Color: 7
Size: 255 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 8
Size: 250 Color: 15
Size: 250 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 0
Size: 330 Color: 3
Size: 335 Color: 11

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 18
Size: 266 Color: 8
Size: 253 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 5
Size: 354 Color: 7
Size: 290 Color: 19

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 14
Size: 342 Color: 12
Size: 265 Color: 13

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 3
Size: 342 Color: 13
Size: 291 Color: 9

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 16
Size: 294 Color: 19
Size: 270 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 3
Size: 329 Color: 16
Size: 326 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 0
Size: 276 Color: 4
Size: 268 Color: 19

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 11
Size: 326 Color: 9
Size: 264 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 275 Color: 13
Size: 250 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 19
Size: 279 Color: 19
Size: 266 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 13
Size: 251 Color: 0
Size: 251 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 6
Size: 289 Color: 14
Size: 280 Color: 7

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 12
Size: 302 Color: 0
Size: 254 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 14
Size: 309 Color: 6
Size: 254 Color: 19

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 8
Size: 366 Color: 0
Size: 259 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 18
Size: 315 Color: 16
Size: 260 Color: 19

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 17
Size: 276 Color: 5
Size: 272 Color: 8

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 326 Color: 4
Size: 261 Color: 5

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 8
Size: 316 Color: 16
Size: 297 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 17
Size: 322 Color: 6
Size: 295 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 6
Size: 328 Color: 7
Size: 268 Color: 10

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 15
Size: 339 Color: 17
Size: 260 Color: 17

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 4
Size: 334 Color: 19
Size: 255 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 9
Size: 335 Color: 5
Size: 322 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7
Size: 363 Color: 9
Size: 264 Color: 17

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 2
Size: 267 Color: 10
Size: 261 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 18
Size: 344 Color: 5
Size: 252 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 11
Size: 303 Color: 1
Size: 262 Color: 14

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 11
Size: 311 Color: 18
Size: 290 Color: 6

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 17
Size: 268 Color: 17
Size: 266 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 14
Size: 322 Color: 1
Size: 300 Color: 13

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 13
Size: 307 Color: 18
Size: 305 Color: 10

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 337 Color: 1
Size: 289 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 356 Color: 6
Size: 282 Color: 11

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 2
Size: 329 Color: 3
Size: 255 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 3
Size: 328 Color: 7
Size: 268 Color: 16

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 7
Size: 288 Color: 1
Size: 263 Color: 13

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 299 Color: 2
Size: 252 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 8
Size: 256 Color: 2
Size: 253 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 343 Color: 3
Size: 272 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 14
Size: 327 Color: 7
Size: 260 Color: 12

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 16
Size: 287 Color: 2
Size: 257 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 4
Size: 325 Color: 0
Size: 294 Color: 12

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 2
Size: 264 Color: 7
Size: 251 Color: 9

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 330 Color: 6
Size: 252 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8
Size: 299 Color: 15
Size: 295 Color: 15

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 10
Size: 310 Color: 9
Size: 284 Color: 19

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 6
Size: 250 Color: 15
Size: 250 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 341 Color: 8
Size: 268 Color: 10

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 8
Size: 339 Color: 19
Size: 270 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 5
Size: 349 Color: 19
Size: 254 Color: 14

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 4
Size: 332 Color: 5
Size: 263 Color: 9

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 7
Size: 292 Color: 5
Size: 253 Color: 11

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 15
Size: 334 Color: 13
Size: 330 Color: 11

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 17
Size: 372 Color: 13
Size: 251 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 19
Size: 343 Color: 14
Size: 279 Color: 14

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 15
Size: 343 Color: 1
Size: 262 Color: 9

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 15
Size: 277 Color: 14
Size: 272 Color: 9

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9
Size: 273 Color: 12
Size: 272 Color: 7

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 17
Size: 262 Color: 16
Size: 259 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 10
Size: 323 Color: 18
Size: 304 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 4
Size: 313 Color: 13
Size: 262 Color: 15

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 5
Size: 349 Color: 6
Size: 301 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 14
Size: 314 Color: 2
Size: 255 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 347 Color: 4
Size: 287 Color: 14

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 13
Size: 351 Color: 16
Size: 262 Color: 17

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6
Size: 327 Color: 4
Size: 317 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 12
Size: 256 Color: 11
Size: 252 Color: 15

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 16
Size: 338 Color: 14
Size: 304 Color: 17

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 297 Color: 18
Size: 250 Color: 7
Size: 454 Color: 16

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 19
Size: 330 Color: 2
Size: 323 Color: 15

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 8
Size: 285 Color: 18
Size: 266 Color: 12

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 0
Size: 313 Color: 11
Size: 261 Color: 6

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 293 Color: 7
Size: 261 Color: 10

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 18
Size: 324 Color: 4
Size: 261 Color: 10

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 338 Color: 8
Size: 337 Color: 13
Size: 326 Color: 12

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 6
Size: 266 Color: 9
Size: 256 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 347 Color: 6
Size: 297 Color: 15

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 13
Size: 278 Color: 12
Size: 265 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 15
Size: 274 Color: 4
Size: 267 Color: 7

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 18
Size: 297 Color: 8
Size: 265 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 15
Size: 354 Color: 6
Size: 274 Color: 4

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 14
Size: 310 Color: 11
Size: 285 Color: 9

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 278 Color: 4
Size: 273 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 11
Size: 304 Color: 1
Size: 265 Color: 8

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 320 Color: 0
Size: 305 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 267 Color: 0
Size: 250 Color: 3

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 14
Size: 347 Color: 13
Size: 293 Color: 6

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 12
Size: 334 Color: 5
Size: 323 Color: 5

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 14
Size: 330 Color: 18
Size: 280 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 303 Color: 18
Size: 298 Color: 14

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 4
Size: 295 Color: 18
Size: 284 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 7
Size: 330 Color: 12
Size: 312 Color: 16

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 19
Size: 347 Color: 12
Size: 290 Color: 4

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 279 Color: 5
Size: 271 Color: 18

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 6
Size: 324 Color: 17
Size: 298 Color: 10

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 6
Size: 292 Color: 3
Size: 250 Color: 5

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 337 Color: 13
Size: 250 Color: 7

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 16
Size: 360 Color: 16
Size: 270 Color: 17

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 16
Size: 314 Color: 5
Size: 290 Color: 14

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 256 Color: 16
Size: 251 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 14
Size: 269 Color: 1
Size: 256 Color: 9

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 16
Size: 370 Color: 5
Size: 258 Color: 11

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 7
Size: 307 Color: 2
Size: 291 Color: 11

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 5
Size: 327 Color: 13
Size: 296 Color: 10

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 338 Color: 2
Size: 337 Color: 16
Size: 326 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 15
Size: 292 Color: 15
Size: 290 Color: 13

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 19
Size: 322 Color: 11
Size: 260 Color: 18

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 12
Size: 355 Color: 2
Size: 267 Color: 9

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 12
Size: 338 Color: 19
Size: 296 Color: 15

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 18
Size: 304 Color: 13
Size: 272 Color: 4

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 11
Size: 287 Color: 16
Size: 280 Color: 8

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 318 Color: 18
Size: 316 Color: 12

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 302 Color: 5
Size: 296 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 10
Size: 252 Color: 5
Size: 251 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 260 Color: 4
Size: 250 Color: 8

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8
Size: 313 Color: 6
Size: 282 Color: 6

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 6
Size: 315 Color: 6
Size: 258 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 19
Size: 260 Color: 2
Size: 256 Color: 7

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 9
Size: 340 Color: 11
Size: 316 Color: 7

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 0
Size: 368 Color: 10
Size: 264 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 317 Color: 5
Size: 307 Color: 7

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 12
Size: 324 Color: 5
Size: 294 Color: 5

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 16
Size: 330 Color: 18
Size: 286 Color: 2

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 8
Size: 352 Color: 17
Size: 262 Color: 11

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 15
Size: 306 Color: 5
Size: 299 Color: 3

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 6
Size: 340 Color: 10
Size: 263 Color: 2

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 19
Size: 321 Color: 9
Size: 281 Color: 11

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 6
Size: 344 Color: 8
Size: 258 Color: 19

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 11
Size: 310 Color: 6
Size: 266 Color: 6

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 292 Color: 1
Size: 278 Color: 15

Total size: 167167
Total free space: 0

