Capicity Bin: 2404
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 1188 Color: 8
Size: 364 Color: 2
Size: 340 Color: 12
Size: 204 Color: 14
Size: 128 Color: 7
Size: 104 Color: 13
Size: 40 Color: 6
Size: 36 Color: 15

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1338 Color: 5
Size: 890 Color: 0
Size: 144 Color: 4
Size: 20 Color: 15
Size: 12 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2134 Color: 13
Size: 226 Color: 11
Size: 44 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1566 Color: 19
Size: 702 Color: 2
Size: 136 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 10
Size: 294 Color: 19
Size: 56 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 19
Size: 615 Color: 13
Size: 122 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 18
Size: 971 Color: 4
Size: 192 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 4
Size: 242 Color: 8
Size: 48 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 6
Size: 558 Color: 4
Size: 108 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 4
Size: 1135 Color: 12
Size: 60 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 6
Size: 262 Color: 17
Size: 48 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 17
Size: 622 Color: 8
Size: 64 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 3
Size: 323 Color: 8
Size: 64 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1987 Color: 1
Size: 349 Color: 0
Size: 68 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1044 Color: 6
Size: 904 Color: 6
Size: 456 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 18
Size: 587 Color: 5
Size: 24 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 17
Size: 322 Color: 0
Size: 60 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1845 Color: 8
Size: 467 Color: 7
Size: 92 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1557 Color: 2
Size: 707 Color: 12
Size: 140 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 3
Size: 601 Color: 16
Size: 120 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1206 Color: 10
Size: 1002 Color: 8
Size: 196 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 8
Size: 805 Color: 19
Size: 160 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 2
Size: 718 Color: 0
Size: 140 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 17
Size: 403 Color: 6
Size: 80 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 7
Size: 915 Color: 17
Size: 110 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 18
Size: 242 Color: 12
Size: 44 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 16
Size: 831 Color: 3
Size: 166 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1203 Color: 5
Size: 1001 Color: 14
Size: 200 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2059 Color: 8
Size: 333 Color: 9
Size: 12 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 8
Size: 458 Color: 12
Size: 88 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 19
Size: 842 Color: 5
Size: 168 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 19
Size: 859 Color: 18
Size: 170 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 9
Size: 537 Color: 13
Size: 106 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1205 Color: 4
Size: 1001 Color: 6
Size: 198 Color: 16

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 13
Size: 914 Color: 13
Size: 20 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1270 Color: 11
Size: 946 Color: 16
Size: 188 Color: 9

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 5
Size: 619 Color: 19
Size: 122 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 1
Size: 713 Color: 12
Size: 142 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 4
Size: 378 Color: 18
Size: 72 Color: 13

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 14
Size: 210 Color: 14
Size: 40 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 11
Size: 502 Color: 1
Size: 100 Color: 15

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 10
Size: 406 Color: 1
Size: 80 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1430 Color: 4
Size: 814 Color: 1
Size: 160 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2066 Color: 4
Size: 282 Color: 8
Size: 56 Color: 19

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 8
Size: 354 Color: 15
Size: 68 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 2
Size: 730 Color: 10
Size: 72 Color: 17

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 14
Size: 622 Color: 8
Size: 124 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 8
Size: 296 Color: 16
Size: 176 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 10
Size: 517 Color: 13
Size: 86 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 4
Size: 521 Color: 13
Size: 102 Color: 5

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 6
Size: 497 Color: 6
Size: 98 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2047 Color: 14
Size: 299 Color: 0
Size: 58 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2101 Color: 14
Size: 253 Color: 15
Size: 50 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 2063 Color: 5
Size: 311 Color: 9
Size: 30 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1975 Color: 14
Size: 359 Color: 3
Size: 70 Color: 14

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 17
Size: 481 Color: 4
Size: 94 Color: 10

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 4
Size: 493 Color: 19
Size: 98 Color: 18

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 17
Size: 379 Color: 12
Size: 62 Color: 13

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1909 Color: 4
Size: 413 Color: 9
Size: 82 Color: 12

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 0
Size: 315 Color: 3
Size: 62 Color: 11

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 1
Size: 349 Color: 7
Size: 16 Color: 8

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 2083 Color: 18
Size: 269 Color: 19
Size: 52 Color: 15

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 0
Size: 295 Color: 5
Size: 58 Color: 15

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1833 Color: 5
Size: 477 Color: 12
Size: 94 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2113 Color: 5
Size: 275 Color: 0
Size: 16 Color: 18

Total size: 156260
Total free space: 0

