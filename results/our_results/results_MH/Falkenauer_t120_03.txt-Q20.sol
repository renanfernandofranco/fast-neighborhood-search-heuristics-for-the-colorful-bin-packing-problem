Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 13
Size: 274 Color: 13
Size: 256 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 3
Size: 285 Color: 12
Size: 276 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 13
Size: 279 Color: 16
Size: 254 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 14
Size: 317 Color: 13
Size: 290 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 5
Size: 306 Color: 14
Size: 257 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 6
Size: 285 Color: 14
Size: 276 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 8
Size: 324 Color: 8
Size: 304 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 11
Size: 328 Color: 6
Size: 254 Color: 15

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 12
Size: 364 Color: 5
Size: 255 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 15
Size: 316 Color: 10
Size: 252 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7
Size: 354 Color: 18
Size: 272 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 7
Size: 301 Color: 3
Size: 287 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 5
Size: 301 Color: 16
Size: 255 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 15
Size: 251 Color: 0
Size: 250 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 4
Size: 301 Color: 10
Size: 257 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 12
Size: 336 Color: 7
Size: 268 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 2
Size: 351 Color: 1
Size: 252 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 316 Color: 3
Size: 278 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 16
Size: 271 Color: 17
Size: 253 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8
Size: 299 Color: 14
Size: 296 Color: 16

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 278 Color: 10
Size: 265 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 13
Size: 348 Color: 3
Size: 262 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 11
Size: 251 Color: 19
Size: 250 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 12
Size: 273 Color: 16
Size: 254 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 295 Color: 5
Size: 294 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 7
Size: 274 Color: 1
Size: 255 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9
Size: 287 Color: 0
Size: 250 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 17
Size: 324 Color: 9
Size: 261 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 300 Color: 4
Size: 282 Color: 5

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 14
Size: 304 Color: 5
Size: 277 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 4
Size: 266 Color: 12
Size: 254 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 16
Size: 323 Color: 6
Size: 265 Color: 11

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 5
Size: 289 Color: 18
Size: 277 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 288 Color: 1
Size: 265 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 10
Size: 299 Color: 4
Size: 298 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 0
Size: 354 Color: 2
Size: 280 Color: 8

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 346 Color: 10
Size: 261 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 5
Size: 329 Color: 10
Size: 320 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 18
Size: 354 Color: 6
Size: 277 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 16
Size: 321 Color: 4
Size: 269 Color: 9

Total size: 40000
Total free space: 0

