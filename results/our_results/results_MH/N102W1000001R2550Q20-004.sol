Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 439504 Color: 6
Size: 288118 Color: 9
Size: 272379 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 429571 Color: 16
Size: 301042 Color: 9
Size: 269388 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 376795 Color: 9
Size: 304417 Color: 16
Size: 318789 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 427524 Color: 18
Size: 320151 Color: 11
Size: 252326 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 393576 Color: 2
Size: 324756 Color: 17
Size: 281669 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 392602 Color: 9
Size: 336979 Color: 14
Size: 270420 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 346649 Color: 18
Size: 345601 Color: 8
Size: 307751 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 440097 Color: 3
Size: 284579 Color: 0
Size: 275325 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 346163 Color: 10
Size: 356827 Color: 9
Size: 297011 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 482618 Color: 12
Size: 260727 Color: 17
Size: 256656 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 441278 Color: 2
Size: 276730 Color: 5
Size: 281993 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 360356 Color: 2
Size: 378237 Color: 6
Size: 261408 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 452928 Color: 5
Size: 294230 Color: 18
Size: 252843 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 393664 Color: 18
Size: 344732 Color: 2
Size: 261605 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 350277 Color: 19
Size: 325116 Color: 11
Size: 324608 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 371386 Color: 12
Size: 347908 Color: 12
Size: 280707 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 380886 Color: 9
Size: 358120 Color: 18
Size: 260995 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 378503 Color: 4
Size: 311195 Color: 1
Size: 310303 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 386630 Color: 14
Size: 356368 Color: 16
Size: 257003 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 372737 Color: 12
Size: 305225 Color: 1
Size: 322039 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 490451 Color: 7
Size: 250257 Color: 17
Size: 259293 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 421143 Color: 9
Size: 310853 Color: 11
Size: 268005 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 421705 Color: 0
Size: 298295 Color: 3
Size: 280001 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 484404 Color: 10
Size: 258131 Color: 12
Size: 257466 Color: 9

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 361343 Color: 18
Size: 358646 Color: 9
Size: 280012 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 390197 Color: 15
Size: 334214 Color: 7
Size: 275590 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 402888 Color: 5
Size: 342015 Color: 1
Size: 255098 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 426120 Color: 9
Size: 316026 Color: 10
Size: 257855 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 435827 Color: 15
Size: 289847 Color: 19
Size: 274327 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 440398 Color: 19
Size: 305600 Color: 16
Size: 254003 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 444138 Color: 1
Size: 280006 Color: 5
Size: 275857 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 448286 Color: 11
Size: 297423 Color: 4
Size: 254292 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 464782 Color: 4
Size: 277223 Color: 16
Size: 257996 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 477528 Color: 1
Size: 263593 Color: 18
Size: 258880 Color: 6

Total size: 34000034
Total free space: 0

