Capicity Bin: 19296
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 16948 Color: 1
Size: 1964 Color: 1
Size: 384 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 15224 Color: 1
Size: 3240 Color: 1
Size: 832 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 17008 Color: 1
Size: 2196 Color: 1
Size: 92 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 17139 Color: 1
Size: 1721 Color: 1
Size: 436 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11795 Color: 1
Size: 6251 Color: 1
Size: 1250 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 14773 Color: 1
Size: 4221 Color: 1
Size: 302 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 14666 Color: 1
Size: 3862 Color: 1
Size: 768 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13936 Color: 1
Size: 4892 Color: 1
Size: 468 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11784 Color: 1
Size: 6624 Color: 1
Size: 888 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12382 Color: 1
Size: 5762 Color: 1
Size: 1152 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11827 Color: 1
Size: 6225 Color: 1
Size: 1244 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 9674 Color: 1
Size: 9226 Color: 1
Size: 396 Color: 0

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 10961 Color: 1
Size: 4624 Color: 0
Size: 2751 Color: 1
Size: 960 Color: 0

Bin 14: 0 of cap free
Amount of items: 5
Items: 
Size: 9649 Color: 1
Size: 5928 Color: 1
Size: 2343 Color: 1
Size: 1008 Color: 0
Size: 368 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 9672 Color: 1
Size: 8504 Color: 1
Size: 1120 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 10568 Color: 1
Size: 7288 Color: 1
Size: 1440 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 9650 Color: 1
Size: 8042 Color: 1
Size: 1604 Color: 0

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 14180 Color: 1
Size: 3484 Color: 1
Size: 864 Color: 0
Size: 768 Color: 0

Bin 19: 0 of cap free
Amount of items: 5
Items: 
Size: 6764 Color: 1
Size: 6264 Color: 1
Size: 3416 Color: 1
Size: 1604 Color: 0
Size: 1248 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 9651 Color: 1
Size: 8039 Color: 1
Size: 1606 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 9652 Color: 1
Size: 9324 Color: 1
Size: 320 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 1
Size: 8040 Color: 1
Size: 1600 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 9666 Color: 1
Size: 8026 Color: 1
Size: 1604 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 10384 Color: 1
Size: 8016 Color: 1
Size: 896 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 10695 Color: 1
Size: 7169 Color: 1
Size: 1432 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11834 Color: 1
Size: 6222 Color: 1
Size: 1240 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12713 Color: 1
Size: 5487 Color: 1
Size: 1096 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12739 Color: 1
Size: 5465 Color: 1
Size: 1092 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12784 Color: 1
Size: 5456 Color: 1
Size: 1056 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12840 Color: 1
Size: 5384 Color: 1
Size: 1072 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12866 Color: 1
Size: 5362 Color: 1
Size: 1068 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 5336 Color: 1
Size: 1056 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12944 Color: 1
Size: 5416 Color: 1
Size: 936 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13116 Color: 1
Size: 6124 Color: 1
Size: 56 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13477 Color: 1
Size: 4851 Color: 1
Size: 968 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13499 Color: 1
Size: 4831 Color: 1
Size: 966 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13531 Color: 1
Size: 4805 Color: 1
Size: 960 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13768 Color: 1
Size: 4648 Color: 1
Size: 880 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 1
Size: 5168 Color: 1
Size: 352 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 1
Size: 4564 Color: 1
Size: 912 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 1
Size: 4424 Color: 1
Size: 848 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14169 Color: 1
Size: 4273 Color: 1
Size: 854 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14448 Color: 1
Size: 4408 Color: 1
Size: 440 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14508 Color: 1
Size: 3996 Color: 1
Size: 792 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14680 Color: 1
Size: 3864 Color: 1
Size: 752 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14736 Color: 1
Size: 4048 Color: 1
Size: 512 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14748 Color: 1
Size: 3684 Color: 1
Size: 864 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14769 Color: 1
Size: 3773 Color: 1
Size: 754 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14925 Color: 1
Size: 3643 Color: 1
Size: 728 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 15062 Color: 1
Size: 3446 Color: 1
Size: 788 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 15076 Color: 1
Size: 3796 Color: 1
Size: 424 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 15120 Color: 1
Size: 3824 Color: 1
Size: 352 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 15138 Color: 1
Size: 3466 Color: 1
Size: 692 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 15208 Color: 1
Size: 3400 Color: 1
Size: 688 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 15303 Color: 1
Size: 3329 Color: 1
Size: 664 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 15488 Color: 1
Size: 2736 Color: 1
Size: 1072 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 15534 Color: 1
Size: 3138 Color: 1
Size: 624 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 15540 Color: 1
Size: 3132 Color: 1
Size: 624 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 15554 Color: 1
Size: 2630 Color: 1
Size: 1112 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 15578 Color: 1
Size: 3102 Color: 1
Size: 616 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 15605 Color: 1
Size: 3339 Color: 1
Size: 352 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 15632 Color: 1
Size: 2896 Color: 1
Size: 768 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 15656 Color: 1
Size: 3312 Color: 1
Size: 328 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 15661 Color: 1
Size: 3031 Color: 1
Size: 604 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 15688 Color: 1
Size: 3080 Color: 1
Size: 528 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 15752 Color: 1
Size: 2924 Color: 1
Size: 620 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 15788 Color: 1
Size: 2900 Color: 1
Size: 608 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 15816 Color: 1
Size: 3048 Color: 1
Size: 432 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 15820 Color: 1
Size: 3122 Color: 1
Size: 354 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 15840 Color: 1
Size: 3056 Color: 1
Size: 400 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 15898 Color: 1
Size: 2720 Color: 1
Size: 678 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 15965 Color: 1
Size: 2777 Color: 1
Size: 554 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 15989 Color: 1
Size: 2757 Color: 1
Size: 550 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 16048 Color: 1
Size: 2576 Color: 0
Size: 672 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 16100 Color: 1
Size: 2912 Color: 1
Size: 284 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 16142 Color: 1
Size: 2386 Color: 1
Size: 768 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 16166 Color: 1
Size: 2442 Color: 1
Size: 688 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 16168 Color: 1
Size: 2616 Color: 1
Size: 512 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 16172 Color: 1
Size: 2580 Color: 1
Size: 544 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 16184 Color: 1
Size: 2600 Color: 1
Size: 512 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 16204 Color: 1
Size: 2668 Color: 1
Size: 424 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 16208 Color: 1
Size: 2448 Color: 1
Size: 640 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 16279 Color: 1
Size: 2515 Color: 1
Size: 502 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 16311 Color: 1
Size: 2489 Color: 1
Size: 496 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 16368 Color: 1
Size: 2368 Color: 1
Size: 560 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 16420 Color: 1
Size: 2352 Color: 1
Size: 524 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 16434 Color: 1
Size: 2354 Color: 1
Size: 508 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 16456 Color: 1
Size: 2456 Color: 1
Size: 384 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 16474 Color: 1
Size: 2302 Color: 1
Size: 520 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 16485 Color: 1
Size: 2339 Color: 1
Size: 472 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 16488 Color: 1
Size: 2376 Color: 1
Size: 432 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 16496 Color: 1
Size: 2404 Color: 1
Size: 396 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 16500 Color: 1
Size: 2332 Color: 1
Size: 464 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 16507 Color: 1
Size: 2325 Color: 1
Size: 464 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 16538 Color: 1
Size: 2182 Color: 1
Size: 576 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 16616 Color: 1
Size: 2096 Color: 1
Size: 584 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 16648 Color: 1
Size: 2216 Color: 1
Size: 432 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 16668 Color: 1
Size: 2248 Color: 1
Size: 380 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 16670 Color: 1
Size: 2190 Color: 1
Size: 436 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 16675 Color: 1
Size: 2211 Color: 1
Size: 410 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 16678 Color: 1
Size: 2234 Color: 1
Size: 384 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 16703 Color: 1
Size: 1989 Color: 1
Size: 604 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 16724 Color: 1
Size: 2124 Color: 1
Size: 448 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 16742 Color: 1
Size: 2130 Color: 1
Size: 424 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 16756 Color: 1
Size: 2020 Color: 1
Size: 520 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 1
Size: 2104 Color: 1
Size: 416 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 16795 Color: 1
Size: 2085 Color: 1
Size: 416 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 16816 Color: 1
Size: 2004 Color: 1
Size: 476 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 16856 Color: 1
Size: 2148 Color: 1
Size: 292 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 16866 Color: 1
Size: 1758 Color: 1
Size: 672 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 16876 Color: 1
Size: 2084 Color: 1
Size: 336 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 16900 Color: 1
Size: 1912 Color: 1
Size: 484 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 16911 Color: 1
Size: 1955 Color: 1
Size: 430 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 16917 Color: 1
Size: 1983 Color: 1
Size: 396 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 16970 Color: 1
Size: 1994 Color: 1
Size: 332 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 16976 Color: 1
Size: 1972 Color: 1
Size: 348 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 17002 Color: 1
Size: 1782 Color: 1
Size: 512 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 17016 Color: 1
Size: 2026 Color: 1
Size: 254 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 17084 Color: 1
Size: 1844 Color: 1
Size: 368 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 17098 Color: 1
Size: 1834 Color: 1
Size: 364 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 17105 Color: 1
Size: 1827 Color: 1
Size: 364 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 17112 Color: 1
Size: 1808 Color: 1
Size: 376 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 17136 Color: 1
Size: 1936 Color: 1
Size: 224 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 17156 Color: 1
Size: 1748 Color: 1
Size: 392 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 17163 Color: 1
Size: 1779 Color: 1
Size: 354 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 17179 Color: 1
Size: 1765 Color: 1
Size: 352 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 17190 Color: 1
Size: 1638 Color: 1
Size: 468 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 17204 Color: 1
Size: 1708 Color: 1
Size: 384 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 17240 Color: 1
Size: 1600 Color: 1
Size: 456 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 17242 Color: 1
Size: 1720 Color: 1
Size: 334 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 17252 Color: 1
Size: 1640 Color: 1
Size: 404 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 17272 Color: 1
Size: 1680 Color: 1
Size: 344 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 17289 Color: 1
Size: 1673 Color: 1
Size: 334 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 17296 Color: 1
Size: 1660 Color: 1
Size: 340 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 17297 Color: 1
Size: 1667 Color: 1
Size: 332 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 17308 Color: 1
Size: 1764 Color: 1
Size: 224 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 17334 Color: 1
Size: 1248 Color: 1
Size: 714 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 17336 Color: 1
Size: 1636 Color: 1
Size: 324 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 17340 Color: 1
Size: 1612 Color: 1
Size: 344 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 17364 Color: 1
Size: 1868 Color: 1
Size: 64 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 9660 Color: 1
Size: 8035 Color: 1
Size: 1600 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 14159 Color: 1
Size: 4496 Color: 1
Size: 640 Color: 0

Bin 143: 1 of cap free
Amount of items: 6
Items: 
Size: 13180 Color: 1
Size: 2159 Color: 1
Size: 1832 Color: 1
Size: 1388 Color: 0
Size: 368 Color: 0
Size: 368 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 14197 Color: 1
Size: 4698 Color: 1
Size: 400 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 14737 Color: 1
Size: 4162 Color: 1
Size: 396 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 15124 Color: 1
Size: 3467 Color: 1
Size: 704 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 15420 Color: 1
Size: 3395 Color: 1
Size: 480 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 15625 Color: 1
Size: 2814 Color: 1
Size: 856 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 15667 Color: 1
Size: 3236 Color: 1
Size: 392 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 15982 Color: 1
Size: 3025 Color: 1
Size: 288 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 15997 Color: 1
Size: 2890 Color: 1
Size: 408 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 16016 Color: 1
Size: 2543 Color: 1
Size: 736 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 16245 Color: 1
Size: 2610 Color: 1
Size: 440 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 16653 Color: 1
Size: 1714 Color: 1
Size: 928 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 16707 Color: 1
Size: 2040 Color: 1
Size: 548 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 16906 Color: 1
Size: 1885 Color: 1
Size: 504 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 16932 Color: 1
Size: 2203 Color: 1
Size: 160 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 17188 Color: 1
Size: 1803 Color: 1
Size: 304 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 15223 Color: 1
Size: 3760 Color: 1
Size: 312 Color: 0

Bin 160: 1 of cap free
Amount of items: 3
Items: 
Size: 17007 Color: 1
Size: 1936 Color: 1
Size: 352 Color: 0

Bin 161: 1 of cap free
Amount of items: 3
Items: 
Size: 17231 Color: 1
Size: 1720 Color: 1
Size: 344 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 11350 Color: 1
Size: 6888 Color: 1
Size: 1056 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 1
Size: 6770 Color: 1
Size: 576 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 1
Size: 6390 Color: 1
Size: 96 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 12842 Color: 1
Size: 5348 Color: 1
Size: 1104 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 1
Size: 5046 Color: 1
Size: 512 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 15162 Color: 1
Size: 3812 Color: 1
Size: 320 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 15344 Color: 1
Size: 3106 Color: 1
Size: 844 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 16370 Color: 1
Size: 2604 Color: 1
Size: 320 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 17060 Color: 1
Size: 1914 Color: 1
Size: 320 Color: 0

Bin 171: 2 of cap free
Amount of items: 3
Items: 
Size: 13662 Color: 1
Size: 5296 Color: 1
Size: 336 Color: 0

Bin 172: 3 of cap free
Amount of items: 3
Items: 
Size: 9659 Color: 1
Size: 8034 Color: 1
Size: 1600 Color: 0

Bin 173: 3 of cap free
Amount of items: 3
Items: 
Size: 14225 Color: 1
Size: 4268 Color: 1
Size: 800 Color: 0

Bin 174: 3 of cap free
Amount of items: 3
Items: 
Size: 9680 Color: 1
Size: 8269 Color: 1
Size: 1344 Color: 0

Bin 175: 4 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 1
Size: 5740 Color: 1
Size: 1368 Color: 0

Bin 176: 4 of cap free
Amount of items: 3
Items: 
Size: 12588 Color: 1
Size: 6288 Color: 1
Size: 416 Color: 0

Bin 177: 4 of cap free
Amount of items: 3
Items: 
Size: 13104 Color: 1
Size: 5596 Color: 1
Size: 592 Color: 0

Bin 178: 4 of cap free
Amount of items: 3
Items: 
Size: 13428 Color: 1
Size: 4680 Color: 1
Size: 1184 Color: 0

Bin 179: 4 of cap free
Amount of items: 3
Items: 
Size: 13954 Color: 1
Size: 3962 Color: 1
Size: 1376 Color: 0

Bin 180: 5 of cap free
Amount of items: 3
Items: 
Size: 11032 Color: 1
Size: 6947 Color: 1
Size: 1312 Color: 0

Bin 181: 5 of cap free
Amount of items: 3
Items: 
Size: 14546 Color: 1
Size: 4281 Color: 1
Size: 464 Color: 0

Bin 182: 6 of cap free
Amount of items: 3
Items: 
Size: 15922 Color: 1
Size: 2904 Color: 1
Size: 464 Color: 0

Bin 183: 7 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 1
Size: 4385 Color: 1
Size: 912 Color: 0

Bin 184: 9 of cap free
Amount of items: 3
Items: 
Size: 14302 Color: 1
Size: 4227 Color: 1
Size: 758 Color: 0

Bin 185: 12 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 1
Size: 7232 Color: 1
Size: 864 Color: 0

Bin 186: 12 of cap free
Amount of items: 3
Items: 
Size: 11344 Color: 1
Size: 7524 Color: 1
Size: 416 Color: 0

Bin 187: 20 of cap free
Amount of items: 3
Items: 
Size: 15108 Color: 1
Size: 3848 Color: 1
Size: 320 Color: 0

Bin 188: 22 of cap free
Amount of items: 3
Items: 
Size: 13242 Color: 1
Size: 4808 Color: 1
Size: 1224 Color: 0

Bin 189: 46 of cap free
Amount of items: 3
Items: 
Size: 10594 Color: 1
Size: 7600 Color: 1
Size: 1056 Color: 0

Bin 190: 62 of cap free
Amount of items: 3
Items: 
Size: 10668 Color: 1
Size: 7254 Color: 1
Size: 1312 Color: 0

Bin 191: 77 of cap free
Amount of items: 3
Items: 
Size: 9504 Color: 1
Size: 9363 Color: 1
Size: 352 Color: 0

Bin 192: 78 of cap free
Amount of items: 3
Items: 
Size: 9658 Color: 1
Size: 8384 Color: 1
Size: 1176 Color: 0

Bin 193: 111 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 1
Size: 3801 Color: 1
Size: 720 Color: 0

Bin 194: 130 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 1
Size: 4454 Color: 1
Size: 1024 Color: 0

Bin 195: 149 of cap free
Amount of items: 3
Items: 
Size: 9655 Color: 1
Size: 8044 Color: 1
Size: 1448 Color: 0

Bin 196: 528 of cap free
Amount of items: 2
Items: 
Size: 17162 Color: 1
Size: 1606 Color: 0

Bin 197: 2261 of cap free
Amount of items: 1
Items: 
Size: 17035 Color: 1

Bin 198: 3030 of cap free
Amount of items: 3
Items: 
Size: 11760 Color: 1
Size: 3530 Color: 1
Size: 976 Color: 0

Bin 199: 12656 of cap free
Amount of items: 1
Items: 
Size: 6640 Color: 1

Total size: 3820608
Total free space: 19296

