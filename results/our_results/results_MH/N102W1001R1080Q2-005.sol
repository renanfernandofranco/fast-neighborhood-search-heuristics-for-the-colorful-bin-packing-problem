Capicity Bin: 1001
Lower Bound: 47

Bins used: 49
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 563 Color: 0
Size: 253 Color: 0
Size: 185 Color: 1

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 1
Size: 489 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 690 Color: 0
Size: 177 Color: 1
Size: 134 Color: 0

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 0
Size: 487 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 564 Color: 0
Size: 312 Color: 0
Size: 125 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 596 Color: 1
Size: 287 Color: 0
Size: 118 Color: 1

Bin 7: 1 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 421 Color: 0

Bin 8: 1 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 270 Color: 1
Size: 246 Color: 0

Bin 9: 1 of cap free
Amount of items: 3
Items: 
Size: 532 Color: 0
Size: 244 Color: 1
Size: 224 Color: 0

Bin 10: 1 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 0
Size: 416 Color: 1

Bin 11: 1 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 1
Size: 376 Color: 0

Bin 12: 2 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 1
Size: 235 Color: 0

Bin 13: 2 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 0
Size: 257 Color: 1

Bin 14: 2 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 0
Size: 247 Color: 1

Bin 15: 2 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 0
Size: 168 Color: 0
Size: 106 Color: 1

Bin 16: 3 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 1
Size: 208 Color: 0
Size: 176 Color: 0

Bin 17: 3 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 0
Size: 476 Color: 1

Bin 18: 3 of cap free
Amount of items: 3
Items: 
Size: 557 Color: 0
Size: 240 Color: 0
Size: 201 Color: 1

Bin 19: 4 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 0
Size: 349 Color: 1

Bin 20: 5 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 1
Size: 239 Color: 0

Bin 21: 5 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 1
Size: 266 Color: 0

Bin 22: 5 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 1
Size: 320 Color: 0

Bin 23: 6 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 0
Size: 284 Color: 1
Size: 176 Color: 0

Bin 24: 9 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 1
Size: 450 Color: 0

Bin 25: 9 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 378 Color: 1

Bin 26: 11 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 486 Color: 1

Bin 27: 12 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 0
Size: 405 Color: 1

Bin 28: 15 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 1
Size: 354 Color: 0

Bin 29: 16 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 1
Size: 258 Color: 0

Bin 30: 17 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 1
Size: 327 Color: 0

Bin 31: 23 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 1
Size: 352 Color: 0

Bin 32: 24 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 0
Size: 394 Color: 1

Bin 33: 27 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 0
Size: 343 Color: 1

Bin 34: 29 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 198 Color: 0

Bin 35: 31 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 0
Size: 174 Color: 1

Bin 36: 35 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 0
Size: 241 Color: 1

Bin 37: 36 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 1
Size: 408 Color: 0

Bin 38: 37 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 1
Size: 460 Color: 0

Bin 39: 48 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 351 Color: 0

Bin 40: 61 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 236 Color: 1

Bin 41: 83 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 316 Color: 0

Bin 42: 92 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 314 Color: 0

Bin 43: 93 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 0
Size: 339 Color: 1

Bin 44: 210 of cap free
Amount of items: 1
Items: 
Size: 791 Color: 0

Bin 45: 213 of cap free
Amount of items: 1
Items: 
Size: 788 Color: 0

Bin 46: 235 of cap free
Amount of items: 1
Items: 
Size: 766 Color: 0

Bin 47: 291 of cap free
Amount of items: 1
Items: 
Size: 710 Color: 1

Bin 48: 300 of cap free
Amount of items: 1
Items: 
Size: 701 Color: 0

Bin 49: 328 of cap free
Amount of items: 1
Items: 
Size: 673 Color: 0

Total size: 46717
Total free space: 2332

