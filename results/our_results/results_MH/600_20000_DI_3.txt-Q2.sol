Capicity Bin: 16752
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 0
Size: 6640 Color: 1
Size: 536 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 1
Size: 6036 Color: 0
Size: 836 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10330 Color: 0
Size: 5992 Color: 1
Size: 430 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10378 Color: 1
Size: 6090 Color: 0
Size: 284 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10971 Color: 1
Size: 4819 Color: 0
Size: 962 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11218 Color: 0
Size: 5156 Color: 0
Size: 378 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12572 Color: 0
Size: 3284 Color: 0
Size: 896 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12664 Color: 1
Size: 3608 Color: 0
Size: 480 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12726 Color: 1
Size: 3358 Color: 0
Size: 668 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12839 Color: 1
Size: 3261 Color: 0
Size: 652 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 1
Size: 2600 Color: 1
Size: 1200 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13388 Color: 0
Size: 2956 Color: 1
Size: 408 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 0
Size: 1826 Color: 0
Size: 1462 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13490 Color: 0
Size: 2858 Color: 1
Size: 404 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13633 Color: 0
Size: 2601 Color: 1
Size: 518 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13692 Color: 0
Size: 2500 Color: 1
Size: 560 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13762 Color: 1
Size: 2494 Color: 1
Size: 496 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 1
Size: 2289 Color: 0
Size: 468 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 14002 Color: 1
Size: 2262 Color: 0
Size: 488 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14042 Color: 1
Size: 2244 Color: 0
Size: 466 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 0
Size: 1880 Color: 0
Size: 840 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 1
Size: 1852 Color: 0
Size: 840 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 0
Size: 2288 Color: 0
Size: 392 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14198 Color: 0
Size: 1882 Color: 0
Size: 672 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 0
Size: 1592 Color: 0
Size: 960 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 1
Size: 1944 Color: 1
Size: 560 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 1
Size: 1908 Color: 0
Size: 584 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14503 Color: 1
Size: 1965 Color: 1
Size: 284 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14722 Color: 0
Size: 1498 Color: 1
Size: 532 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14898 Color: 1
Size: 1594 Color: 1
Size: 260 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14878 Color: 0
Size: 1562 Color: 1
Size: 312 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14990 Color: 0
Size: 1392 Color: 0
Size: 370 Color: 1

Bin 33: 1 of cap free
Amount of items: 8
Items: 
Size: 8381 Color: 1
Size: 1392 Color: 1
Size: 1392 Color: 1
Size: 1216 Color: 1
Size: 1216 Color: 0
Size: 1070 Color: 0
Size: 1052 Color: 0
Size: 1032 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 9353 Color: 1
Size: 6978 Color: 1
Size: 420 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 9442 Color: 1
Size: 6973 Color: 0
Size: 336 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 10433 Color: 1
Size: 6094 Color: 0
Size: 224 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 11075 Color: 1
Size: 5164 Color: 0
Size: 512 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 11703 Color: 0
Size: 4516 Color: 1
Size: 532 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 1
Size: 3485 Color: 1
Size: 598 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 12888 Color: 0
Size: 3167 Color: 0
Size: 696 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 0
Size: 2903 Color: 1
Size: 464 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 13745 Color: 1
Size: 2666 Color: 1
Size: 340 Color: 0

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 13817 Color: 1
Size: 2934 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 13951 Color: 1
Size: 2472 Color: 1
Size: 328 Color: 0

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 14007 Color: 1
Size: 2744 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 13996 Color: 0
Size: 2299 Color: 0
Size: 456 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 14035 Color: 1
Size: 1580 Color: 0
Size: 1136 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 14163 Color: 1
Size: 1804 Color: 0
Size: 784 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 14207 Color: 1
Size: 1476 Color: 0
Size: 1068 Color: 1

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 14226 Color: 1
Size: 2525 Color: 0

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 14415 Color: 1
Size: 2336 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 14591 Color: 0
Size: 1136 Color: 1
Size: 1024 Color: 0

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 14700 Color: 1
Size: 2051 Color: 0

Bin 54: 2 of cap free
Amount of items: 7
Items: 
Size: 8378 Color: 0
Size: 1642 Color: 0
Size: 1592 Color: 1
Size: 1512 Color: 1
Size: 1394 Color: 0
Size: 1200 Color: 1
Size: 1032 Color: 0

Bin 55: 2 of cap free
Amount of items: 4
Items: 
Size: 8382 Color: 0
Size: 5981 Color: 1
Size: 1819 Color: 0
Size: 568 Color: 1

Bin 56: 2 of cap free
Amount of items: 4
Items: 
Size: 8388 Color: 0
Size: 5528 Color: 0
Size: 2556 Color: 1
Size: 278 Color: 1

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 9446 Color: 1
Size: 6968 Color: 1
Size: 336 Color: 0

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 9516 Color: 1
Size: 6888 Color: 1
Size: 346 Color: 0

Bin 59: 2 of cap free
Amount of items: 5
Items: 
Size: 11599 Color: 0
Size: 2662 Color: 1
Size: 1949 Color: 1
Size: 292 Color: 0
Size: 248 Color: 1

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 1
Size: 3302 Color: 0
Size: 880 Color: 0

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 12636 Color: 0
Size: 3750 Color: 0
Size: 364 Color: 1

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 13562 Color: 0
Size: 2876 Color: 1
Size: 312 Color: 0

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 13971 Color: 1
Size: 1859 Color: 0
Size: 920 Color: 0

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 0
Size: 1636 Color: 0
Size: 1088 Color: 1

Bin 65: 3 of cap free
Amount of items: 11
Items: 
Size: 8377 Color: 1
Size: 1152 Color: 1
Size: 1048 Color: 1
Size: 976 Color: 1
Size: 896 Color: 0
Size: 864 Color: 0
Size: 784 Color: 0
Size: 772 Color: 0
Size: 736 Color: 0
Size: 688 Color: 1
Size: 456 Color: 0

Bin 66: 3 of cap free
Amount of items: 7
Items: 
Size: 8389 Color: 1
Size: 1482 Color: 1
Size: 1452 Color: 1
Size: 1442 Color: 1
Size: 1394 Color: 0
Size: 1394 Color: 0
Size: 1196 Color: 0

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 12945 Color: 0
Size: 3532 Color: 1
Size: 272 Color: 0

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 13308 Color: 0
Size: 3201 Color: 0
Size: 240 Color: 1

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 13525 Color: 1
Size: 3224 Color: 0

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 14854 Color: 1
Size: 1895 Color: 0

Bin 71: 4 of cap free
Amount of items: 9
Items: 
Size: 8380 Color: 1
Size: 1200 Color: 1
Size: 1198 Color: 1
Size: 1184 Color: 1
Size: 1040 Color: 0
Size: 1024 Color: 1
Size: 946 Color: 0
Size: 944 Color: 0
Size: 832 Color: 0

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 8800 Color: 0
Size: 6972 Color: 1
Size: 976 Color: 1

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 9480 Color: 0
Size: 6980 Color: 1
Size: 288 Color: 0

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 10460 Color: 0
Size: 5760 Color: 0
Size: 528 Color: 1

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 10506 Color: 0
Size: 5810 Color: 0
Size: 432 Color: 1

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 0
Size: 3324 Color: 1

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 1
Size: 2808 Color: 0

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 14626 Color: 1
Size: 2122 Color: 0

Bin 79: 4 of cap free
Amount of items: 2
Items: 
Size: 14664 Color: 1
Size: 2084 Color: 0

Bin 80: 4 of cap free
Amount of items: 2
Items: 
Size: 14974 Color: 1
Size: 1774 Color: 0

Bin 81: 5 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 0
Size: 6971 Color: 0
Size: 792 Color: 1

Bin 82: 5 of cap free
Amount of items: 3
Items: 
Size: 11083 Color: 0
Size: 5160 Color: 0
Size: 504 Color: 1

Bin 83: 5 of cap free
Amount of items: 2
Items: 
Size: 11480 Color: 1
Size: 5267 Color: 0

Bin 84: 5 of cap free
Amount of items: 3
Items: 
Size: 11705 Color: 0
Size: 4738 Color: 1
Size: 304 Color: 0

Bin 85: 5 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 0
Size: 4207 Color: 0
Size: 260 Color: 1

Bin 86: 5 of cap free
Amount of items: 2
Items: 
Size: 14588 Color: 0
Size: 2159 Color: 1

Bin 87: 6 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 0
Size: 2682 Color: 1
Size: 740 Color: 1

Bin 88: 6 of cap free
Amount of items: 2
Items: 
Size: 13538 Color: 0
Size: 3208 Color: 1

Bin 89: 6 of cap free
Amount of items: 2
Items: 
Size: 14498 Color: 1
Size: 2248 Color: 0

Bin 90: 7 of cap free
Amount of items: 2
Items: 
Size: 12563 Color: 1
Size: 4182 Color: 0

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 12764 Color: 1
Size: 3980 Color: 0

Bin 92: 8 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 1
Size: 1974 Color: 0
Size: 256 Color: 0

Bin 93: 8 of cap free
Amount of items: 2
Items: 
Size: 14716 Color: 0
Size: 2028 Color: 1

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 14988 Color: 0
Size: 1756 Color: 1

Bin 95: 9 of cap free
Amount of items: 3
Items: 
Size: 10524 Color: 0
Size: 5995 Color: 1
Size: 224 Color: 1

Bin 96: 9 of cap free
Amount of items: 3
Items: 
Size: 11070 Color: 0
Size: 5353 Color: 0
Size: 320 Color: 1

Bin 97: 9 of cap free
Amount of items: 3
Items: 
Size: 12571 Color: 1
Size: 3660 Color: 0
Size: 512 Color: 1

Bin 98: 10 of cap free
Amount of items: 3
Items: 
Size: 10968 Color: 1
Size: 5206 Color: 0
Size: 568 Color: 1

Bin 99: 10 of cap free
Amount of items: 2
Items: 
Size: 13326 Color: 1
Size: 3416 Color: 0

Bin 100: 10 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 1
Size: 1662 Color: 0
Size: 1280 Color: 0

Bin 101: 10 of cap free
Amount of items: 2
Items: 
Size: 14187 Color: 1
Size: 2555 Color: 0

Bin 102: 10 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 0
Size: 1978 Color: 1
Size: 112 Color: 1

Bin 103: 10 of cap free
Amount of items: 2
Items: 
Size: 14710 Color: 1
Size: 2032 Color: 0

Bin 104: 10 of cap free
Amount of items: 2
Items: 
Size: 15048 Color: 1
Size: 1694 Color: 0

Bin 105: 11 of cap free
Amount of items: 3
Items: 
Size: 9524 Color: 0
Size: 6981 Color: 1
Size: 236 Color: 0

Bin 106: 11 of cap free
Amount of items: 2
Items: 
Size: 14234 Color: 1
Size: 2507 Color: 0

Bin 107: 11 of cap free
Amount of items: 2
Items: 
Size: 14782 Color: 1
Size: 1959 Color: 0

Bin 108: 12 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 0
Size: 5196 Color: 1
Size: 240 Color: 0

Bin 109: 12 of cap free
Amount of items: 2
Items: 
Size: 12812 Color: 1
Size: 3928 Color: 0

Bin 110: 12 of cap free
Amount of items: 2
Items: 
Size: 14386 Color: 1
Size: 2354 Color: 0

Bin 111: 12 of cap free
Amount of items: 3
Items: 
Size: 14958 Color: 1
Size: 1714 Color: 0
Size: 68 Color: 1

Bin 112: 13 of cap free
Amount of items: 2
Items: 
Size: 10572 Color: 0
Size: 6167 Color: 1

Bin 113: 13 of cap free
Amount of items: 2
Items: 
Size: 13421 Color: 0
Size: 3318 Color: 1

Bin 114: 14 of cap free
Amount of items: 2
Items: 
Size: 12516 Color: 1
Size: 4222 Color: 0

Bin 115: 14 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 0
Size: 3098 Color: 1

Bin 116: 14 of cap free
Amount of items: 2
Items: 
Size: 14403 Color: 0
Size: 2335 Color: 1

Bin 117: 15 of cap free
Amount of items: 3
Items: 
Size: 11686 Color: 0
Size: 4731 Color: 1
Size: 320 Color: 0

Bin 118: 15 of cap free
Amount of items: 2
Items: 
Size: 15002 Color: 1
Size: 1735 Color: 0

Bin 119: 16 of cap free
Amount of items: 3
Items: 
Size: 12044 Color: 1
Size: 4520 Color: 0
Size: 172 Color: 1

Bin 120: 16 of cap free
Amount of items: 2
Items: 
Size: 13038 Color: 0
Size: 3698 Color: 1

Bin 121: 16 of cap free
Amount of items: 2
Items: 
Size: 13157 Color: 1
Size: 3579 Color: 0

Bin 122: 16 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 1
Size: 3176 Color: 0

Bin 123: 18 of cap free
Amount of items: 3
Items: 
Size: 12318 Color: 0
Size: 2664 Color: 1
Size: 1752 Color: 1

Bin 124: 18 of cap free
Amount of items: 2
Items: 
Size: 15032 Color: 0
Size: 1702 Color: 1

Bin 125: 19 of cap free
Amount of items: 2
Items: 
Size: 14468 Color: 1
Size: 2265 Color: 0

Bin 126: 20 of cap free
Amount of items: 2
Items: 
Size: 14285 Color: 0
Size: 2447 Color: 1

Bin 127: 21 of cap free
Amount of items: 2
Items: 
Size: 12117 Color: 0
Size: 4614 Color: 1

Bin 128: 22 of cap free
Amount of items: 2
Items: 
Size: 13234 Color: 1
Size: 3496 Color: 0

Bin 129: 22 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 1
Size: 2860 Color: 0

Bin 130: 22 of cap free
Amount of items: 2
Items: 
Size: 14331 Color: 0
Size: 2399 Color: 1

Bin 131: 24 of cap free
Amount of items: 3
Items: 
Size: 8456 Color: 1
Size: 7328 Color: 0
Size: 944 Color: 1

Bin 132: 24 of cap free
Amount of items: 3
Items: 
Size: 15012 Color: 0
Size: 1688 Color: 1
Size: 28 Color: 1

Bin 133: 25 of cap free
Amount of items: 2
Items: 
Size: 13554 Color: 1
Size: 3173 Color: 0

Bin 134: 25 of cap free
Amount of items: 2
Items: 
Size: 14379 Color: 1
Size: 2348 Color: 0

Bin 135: 26 of cap free
Amount of items: 2
Items: 
Size: 14523 Color: 1
Size: 2203 Color: 0

Bin 136: 26 of cap free
Amount of items: 2
Items: 
Size: 15026 Color: 0
Size: 1700 Color: 1

Bin 137: 28 of cap free
Amount of items: 2
Items: 
Size: 13947 Color: 0
Size: 2777 Color: 1

Bin 138: 28 of cap free
Amount of items: 2
Items: 
Size: 14322 Color: 1
Size: 2402 Color: 0

Bin 139: 28 of cap free
Amount of items: 2
Items: 
Size: 14424 Color: 1
Size: 2300 Color: 0

Bin 140: 28 of cap free
Amount of items: 2
Items: 
Size: 14450 Color: 0
Size: 2274 Color: 1

Bin 141: 29 of cap free
Amount of items: 2
Items: 
Size: 13919 Color: 1
Size: 2804 Color: 0

Bin 142: 29 of cap free
Amount of items: 2
Items: 
Size: 14744 Color: 1
Size: 1979 Color: 0

Bin 143: 30 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 0
Size: 3924 Color: 1

Bin 144: 30 of cap free
Amount of items: 2
Items: 
Size: 14860 Color: 0
Size: 1862 Color: 1

Bin 145: 32 of cap free
Amount of items: 2
Items: 
Size: 13723 Color: 0
Size: 2997 Color: 1

Bin 146: 32 of cap free
Amount of items: 2
Items: 
Size: 14210 Color: 0
Size: 2510 Color: 1

Bin 147: 32 of cap free
Amount of items: 2
Items: 
Size: 14571 Color: 1
Size: 2149 Color: 0

Bin 148: 33 of cap free
Amount of items: 2
Items: 
Size: 9575 Color: 1
Size: 7144 Color: 0

Bin 149: 34 of cap free
Amount of items: 2
Items: 
Size: 14796 Color: 1
Size: 1922 Color: 0

Bin 150: 34 of cap free
Amount of items: 3
Items: 
Size: 14952 Color: 1
Size: 1670 Color: 0
Size: 96 Color: 0

Bin 151: 37 of cap free
Amount of items: 2
Items: 
Size: 11734 Color: 0
Size: 4981 Color: 1

Bin 152: 37 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 1
Size: 3491 Color: 0

Bin 153: 39 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 0
Size: 6977 Color: 1
Size: 1232 Color: 0

Bin 154: 39 of cap free
Amount of items: 2
Items: 
Size: 13327 Color: 0
Size: 3386 Color: 1

Bin 155: 41 of cap free
Amount of items: 2
Items: 
Size: 14575 Color: 0
Size: 2136 Color: 1

Bin 156: 42 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 1
Size: 3710 Color: 0
Size: 960 Color: 0

Bin 157: 42 of cap free
Amount of items: 2
Items: 
Size: 12302 Color: 1
Size: 4408 Color: 0

Bin 158: 42 of cap free
Amount of items: 3
Items: 
Size: 13756 Color: 1
Size: 1484 Color: 0
Size: 1470 Color: 0

Bin 159: 42 of cap free
Amount of items: 2
Items: 
Size: 14684 Color: 1
Size: 2026 Color: 0

Bin 160: 43 of cap free
Amount of items: 2
Items: 
Size: 14370 Color: 1
Size: 2339 Color: 0

Bin 161: 47 of cap free
Amount of items: 2
Items: 
Size: 11980 Color: 1
Size: 4725 Color: 0

Bin 162: 47 of cap free
Amount of items: 2
Items: 
Size: 13269 Color: 0
Size: 3436 Color: 1

Bin 163: 48 of cap free
Amount of items: 2
Items: 
Size: 10632 Color: 0
Size: 6072 Color: 1

Bin 164: 54 of cap free
Amount of items: 2
Items: 
Size: 12254 Color: 0
Size: 4444 Color: 1

Bin 165: 56 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 1
Size: 3484 Color: 0

Bin 166: 56 of cap free
Amount of items: 2
Items: 
Size: 14972 Color: 1
Size: 1724 Color: 0

Bin 167: 58 of cap free
Amount of items: 2
Items: 
Size: 11340 Color: 0
Size: 5354 Color: 1

Bin 168: 60 of cap free
Amount of items: 2
Items: 
Size: 14562 Color: 0
Size: 2130 Color: 1

Bin 169: 62 of cap free
Amount of items: 2
Items: 
Size: 14671 Color: 1
Size: 2019 Color: 0

Bin 170: 63 of cap free
Amount of items: 2
Items: 
Size: 12953 Color: 0
Size: 3736 Color: 1

Bin 171: 67 of cap free
Amount of items: 2
Items: 
Size: 14293 Color: 1
Size: 2392 Color: 0

Bin 172: 68 of cap free
Amount of items: 2
Items: 
Size: 14698 Color: 0
Size: 1986 Color: 1

Bin 173: 71 of cap free
Amount of items: 7
Items: 
Size: 8385 Color: 1
Size: 1448 Color: 1
Size: 1392 Color: 0
Size: 1392 Color: 0
Size: 1392 Color: 0
Size: 1360 Color: 0
Size: 1312 Color: 1

Bin 174: 79 of cap free
Amount of items: 2
Items: 
Size: 14552 Color: 0
Size: 2121 Color: 1

Bin 175: 80 of cap free
Amount of items: 2
Items: 
Size: 11428 Color: 1
Size: 5244 Color: 0

Bin 176: 84 of cap free
Amount of items: 2
Items: 
Size: 12459 Color: 0
Size: 4209 Color: 1

Bin 177: 85 of cap free
Amount of items: 2
Items: 
Size: 12115 Color: 1
Size: 4552 Color: 0

Bin 178: 92 of cap free
Amount of items: 2
Items: 
Size: 14856 Color: 0
Size: 1804 Color: 1

Bin 179: 99 of cap free
Amount of items: 2
Items: 
Size: 12790 Color: 1
Size: 3863 Color: 0

Bin 180: 100 of cap free
Amount of items: 2
Items: 
Size: 13930 Color: 0
Size: 2722 Color: 1

Bin 181: 100 of cap free
Amount of items: 2
Items: 
Size: 14532 Color: 0
Size: 2120 Color: 1

Bin 182: 104 of cap free
Amount of items: 2
Items: 
Size: 8392 Color: 1
Size: 8256 Color: 0

Bin 183: 105 of cap free
Amount of items: 2
Items: 
Size: 14846 Color: 0
Size: 1801 Color: 1

Bin 184: 109 of cap free
Amount of items: 2
Items: 
Size: 14324 Color: 0
Size: 2319 Color: 1

Bin 185: 110 of cap free
Amount of items: 25
Items: 
Size: 844 Color: 1
Size: 772 Color: 1
Size: 748 Color: 1
Size: 736 Color: 1
Size: 736 Color: 0
Size: 714 Color: 1
Size: 704 Color: 1
Size: 704 Color: 1
Size: 698 Color: 0
Size: 696 Color: 0
Size: 680 Color: 0
Size: 664 Color: 0
Size: 660 Color: 0
Size: 660 Color: 0
Size: 656 Color: 1
Size: 640 Color: 1
Size: 634 Color: 1
Size: 632 Color: 1
Size: 624 Color: 0
Size: 616 Color: 1
Size: 580 Color: 0
Size: 568 Color: 0
Size: 568 Color: 0
Size: 564 Color: 0
Size: 544 Color: 0

Bin 186: 113 of cap free
Amount of items: 2
Items: 
Size: 12774 Color: 0
Size: 3865 Color: 1

Bin 187: 128 of cap free
Amount of items: 2
Items: 
Size: 10136 Color: 0
Size: 6488 Color: 1

Bin 188: 128 of cap free
Amount of items: 2
Items: 
Size: 11720 Color: 0
Size: 4904 Color: 1

Bin 189: 128 of cap free
Amount of items: 2
Items: 
Size: 14522 Color: 1
Size: 2102 Color: 0

Bin 190: 142 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 0
Size: 4170 Color: 1

Bin 191: 144 of cap free
Amount of items: 2
Items: 
Size: 10872 Color: 0
Size: 5736 Color: 1

Bin 192: 165 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 0
Size: 2691 Color: 1

Bin 193: 167 of cap free
Amount of items: 2
Items: 
Size: 10329 Color: 0
Size: 6256 Color: 1

Bin 194: 167 of cap free
Amount of items: 2
Items: 
Size: 14479 Color: 0
Size: 2106 Color: 1

Bin 195: 168 of cap free
Amount of items: 2
Items: 
Size: 10556 Color: 0
Size: 6028 Color: 1

Bin 196: 178 of cap free
Amount of items: 2
Items: 
Size: 11750 Color: 1
Size: 4824 Color: 0

Bin 197: 188 of cap free
Amount of items: 38
Items: 
Size: 584 Color: 1
Size: 554 Color: 1
Size: 540 Color: 0
Size: 528 Color: 1
Size: 504 Color: 0
Size: 500 Color: 1
Size: 496 Color: 1
Size: 480 Color: 0
Size: 466 Color: 0
Size: 464 Color: 0
Size: 462 Color: 0
Size: 458 Color: 1
Size: 452 Color: 1
Size: 452 Color: 1
Size: 448 Color: 0
Size: 448 Color: 0
Size: 432 Color: 1
Size: 424 Color: 0
Size: 424 Color: 0
Size: 424 Color: 0
Size: 420 Color: 0
Size: 416 Color: 1
Size: 416 Color: 1
Size: 408 Color: 0
Size: 402 Color: 1
Size: 400 Color: 0
Size: 396 Color: 1
Size: 394 Color: 1
Size: 390 Color: 1
Size: 388 Color: 0
Size: 384 Color: 1
Size: 384 Color: 0
Size: 380 Color: 1
Size: 376 Color: 0
Size: 372 Color: 1
Size: 368 Color: 0
Size: 368 Color: 0
Size: 362 Color: 1

Bin 198: 211 of cap free
Amount of items: 2
Items: 
Size: 9559 Color: 0
Size: 6982 Color: 1

Bin 199: 11150 of cap free
Amount of items: 18
Items: 
Size: 362 Color: 0
Size: 360 Color: 1
Size: 360 Color: 0
Size: 352 Color: 1
Size: 344 Color: 0
Size: 344 Color: 0
Size: 340 Color: 1
Size: 336 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 296 Color: 1
Size: 296 Color: 1
Size: 296 Color: 0
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0
Size: 236 Color: 1
Size: 176 Color: 0

Total size: 3316896
Total free space: 16752

