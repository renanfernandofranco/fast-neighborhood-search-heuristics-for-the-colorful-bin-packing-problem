Capicity Bin: 1972
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1709 Color: 3
Size: 221 Color: 0
Size: 42 Color: 2

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1364 Color: 0
Size: 304 Color: 4
Size: 220 Color: 4
Size: 44 Color: 3
Size: 40 Color: 1

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1240 Color: 2
Size: 408 Color: 3
Size: 220 Color: 0
Size: 68 Color: 0
Size: 36 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1547 Color: 0
Size: 379 Color: 2
Size: 46 Color: 1

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1525 Color: 3
Size: 327 Color: 0
Size: 72 Color: 2
Size: 48 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1451 Color: 4
Size: 435 Color: 0
Size: 86 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1435 Color: 3
Size: 485 Color: 3
Size: 52 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 0
Size: 250 Color: 1
Size: 48 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1122 Color: 1
Size: 710 Color: 1
Size: 140 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 3
Size: 249 Color: 2
Size: 70 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 1
Size: 422 Color: 2
Size: 80 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 2
Size: 350 Color: 4
Size: 68 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 3
Size: 262 Color: 0
Size: 52 Color: 4

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 796 Color: 3
Size: 664 Color: 1
Size: 456 Color: 3
Size: 56 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1431 Color: 3
Size: 451 Color: 3
Size: 90 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1305 Color: 0
Size: 557 Color: 4
Size: 110 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 2
Size: 349 Color: 0
Size: 68 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 3
Size: 267 Color: 4
Size: 4 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 0
Size: 389 Color: 3
Size: 76 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1747 Color: 2
Size: 181 Color: 4
Size: 44 Color: 0

Bin 21: 0 of cap free
Amount of items: 4
Items: 
Size: 1763 Color: 1
Size: 189 Color: 4
Size: 16 Color: 0
Size: 4 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1631 Color: 4
Size: 285 Color: 0
Size: 56 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1756 Color: 3
Size: 184 Color: 0
Size: 32 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 1
Size: 466 Color: 0
Size: 92 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 0
Size: 522 Color: 2
Size: 100 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 0
Size: 233 Color: 4
Size: 64 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1313 Color: 1
Size: 551 Color: 4
Size: 108 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1125 Color: 1
Size: 707 Color: 4
Size: 140 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 2
Size: 322 Color: 0
Size: 64 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 0
Size: 227 Color: 0
Size: 52 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 989 Color: 1
Size: 821 Color: 3
Size: 162 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1755 Color: 2
Size: 175 Color: 2
Size: 42 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 0
Size: 213 Color: 4
Size: 42 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1309 Color: 0
Size: 553 Color: 4
Size: 110 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1226 Color: 4
Size: 666 Color: 4
Size: 80 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 0
Size: 202 Color: 4
Size: 40 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 3
Size: 382 Color: 2
Size: 76 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 0
Size: 669 Color: 4
Size: 132 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1427 Color: 4
Size: 455 Color: 3
Size: 90 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 2
Size: 211 Color: 3
Size: 8 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 0
Size: 214 Color: 1
Size: 40 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 4
Size: 305 Color: 0
Size: 34 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 987 Color: 0
Size: 821 Color: 2
Size: 164 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1385 Color: 1
Size: 491 Color: 2
Size: 96 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 990 Color: 3
Size: 938 Color: 4
Size: 44 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1267 Color: 2
Size: 589 Color: 2
Size: 116 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 997 Color: 3
Size: 813 Color: 0
Size: 162 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 4
Size: 286 Color: 1
Size: 56 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1676 Color: 4
Size: 248 Color: 3
Size: 48 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 0
Size: 355 Color: 3
Size: 36 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 4
Size: 207 Color: 1
Size: 40 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1001 Color: 2
Size: 811 Color: 4
Size: 160 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 998 Color: 0
Size: 814 Color: 4
Size: 160 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1766 Color: 2
Size: 174 Color: 0
Size: 32 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 3
Size: 283 Color: 4
Size: 64 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1270 Color: 1
Size: 586 Color: 3
Size: 116 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 993 Color: 4
Size: 817 Color: 0
Size: 162 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 4
Size: 459 Color: 3
Size: 90 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 0
Size: 429 Color: 4
Size: 84 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 2
Size: 671 Color: 1
Size: 134 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 2
Size: 241 Color: 1
Size: 46 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 0
Size: 675 Color: 1
Size: 134 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1769 Color: 4
Size: 171 Color: 2
Size: 32 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 0
Size: 295 Color: 3
Size: 8 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 0
Size: 321 Color: 0
Size: 64 Color: 4

Total size: 128180
Total free space: 0

