Capicity Bin: 1000001
Lower Bound: 896

Bins used: 896
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 546044 Color: 15
Size: 313517 Color: 17
Size: 140440 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 520702 Color: 3
Size: 333585 Color: 7
Size: 145714 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 566657 Color: 9
Size: 274968 Color: 17
Size: 158376 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 532004 Color: 17
Size: 303320 Color: 15
Size: 164677 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 578976 Color: 19
Size: 225959 Color: 3
Size: 195066 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 567250 Color: 10
Size: 297785 Color: 0
Size: 134966 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 692560 Color: 5
Size: 177670 Color: 18
Size: 129771 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 678804 Color: 5
Size: 172704 Color: 15
Size: 148493 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 779420 Color: 15
Size: 119491 Color: 14
Size: 101090 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 519658 Color: 10
Size: 285492 Color: 14
Size: 194851 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 530468 Color: 6
Size: 313167 Color: 12
Size: 156366 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 566647 Color: 17
Size: 278445 Color: 19
Size: 154909 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 437066 Color: 1
Size: 410489 Color: 7
Size: 152446 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 738114 Color: 17
Size: 142468 Color: 1
Size: 119419 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 674365 Color: 1
Size: 196744 Color: 0
Size: 128892 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 681011 Color: 19
Size: 192919 Color: 11
Size: 126071 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 438759 Color: 18
Size: 289187 Color: 15
Size: 272055 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 622949 Color: 17
Size: 272422 Color: 13
Size: 104630 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 646635 Color: 0
Size: 182227 Color: 6
Size: 171139 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 439676 Color: 16
Size: 437837 Color: 0
Size: 122488 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 649321 Color: 5
Size: 196084 Color: 15
Size: 154596 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 704922 Color: 5
Size: 182462 Color: 14
Size: 112617 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 537915 Color: 9
Size: 312744 Color: 10
Size: 149342 Color: 5

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 494326 Color: 1
Size: 329823 Color: 14
Size: 175852 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 339830 Color: 0
Size: 331265 Color: 15
Size: 328906 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 443773 Color: 4
Size: 439197 Color: 6
Size: 117031 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 457052 Color: 5
Size: 442322 Color: 13
Size: 100627 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 491080 Color: 12
Size: 308056 Color: 12
Size: 200865 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 783811 Color: 7
Size: 110472 Color: 15
Size: 105718 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 731083 Color: 9
Size: 145353 Color: 10
Size: 123565 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 524006 Color: 3
Size: 360250 Color: 10
Size: 115745 Color: 8

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 519388 Color: 2
Size: 308861 Color: 1
Size: 171752 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 509510 Color: 3
Size: 390382 Color: 19
Size: 100109 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 675433 Color: 12
Size: 175999 Color: 19
Size: 148569 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 543005 Color: 11
Size: 337150 Color: 11
Size: 119846 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 519315 Color: 10
Size: 307694 Color: 2
Size: 172992 Color: 12

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 671934 Color: 13
Size: 197094 Color: 18
Size: 130973 Color: 12

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 674456 Color: 6
Size: 182762 Color: 12
Size: 142783 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 735349 Color: 3
Size: 159799 Color: 9
Size: 104853 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 781485 Color: 1
Size: 109308 Color: 15
Size: 109208 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 675903 Color: 9
Size: 190185 Color: 19
Size: 133913 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 737039 Color: 13
Size: 157372 Color: 9
Size: 105590 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 511271 Color: 2
Size: 331106 Color: 7
Size: 157624 Color: 4

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 656948 Color: 1
Size: 343053 Color: 9

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 730042 Color: 13
Size: 138621 Color: 16
Size: 131338 Color: 1

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 577667 Color: 9
Size: 422334 Color: 1

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 612819 Color: 14
Size: 387182 Color: 9

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 703697 Color: 8
Size: 296304 Color: 17

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 730074 Color: 4
Size: 269927 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 750257 Color: 2
Size: 126701 Color: 9
Size: 123043 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 792841 Color: 17
Size: 106357 Color: 11
Size: 100803 Color: 9

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 578755 Color: 16
Size: 277070 Color: 4
Size: 144175 Color: 1

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 519062 Color: 17
Size: 291400 Color: 11
Size: 189538 Color: 0

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 594930 Color: 9
Size: 254513 Color: 17
Size: 150557 Color: 13

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 678794 Color: 3
Size: 193995 Color: 16
Size: 127211 Color: 16

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 518547 Color: 5
Size: 293639 Color: 8
Size: 187814 Color: 7

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 600369 Color: 1
Size: 262129 Color: 12
Size: 137502 Color: 5

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 566868 Color: 1
Size: 311279 Color: 19
Size: 121853 Color: 1

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 516613 Color: 9
Size: 340016 Color: 17
Size: 143371 Color: 12

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 388225 Color: 3
Size: 344821 Color: 14
Size: 266954 Color: 15

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 509200 Color: 7
Size: 298629 Color: 18
Size: 192171 Color: 9

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 643563 Color: 16
Size: 235699 Color: 14
Size: 120738 Color: 2

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 439422 Color: 4
Size: 424552 Color: 11
Size: 136026 Color: 11

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 520996 Color: 1
Size: 371228 Color: 6
Size: 107775 Color: 12

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 743791 Color: 13
Size: 138971 Color: 8
Size: 117237 Color: 16

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 650466 Color: 19
Size: 235246 Color: 6
Size: 114287 Color: 11

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 615835 Color: 16
Size: 267708 Color: 15
Size: 116456 Color: 17

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 491066 Color: 0
Size: 347414 Color: 3
Size: 161519 Color: 8

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 580671 Color: 16
Size: 236946 Color: 0
Size: 182382 Color: 2

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 519326 Color: 18
Size: 345671 Color: 19
Size: 135002 Color: 12

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 656471 Color: 2
Size: 184734 Color: 14
Size: 158794 Color: 17

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 567308 Color: 13
Size: 246266 Color: 17
Size: 186425 Color: 2

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 547417 Color: 8
Size: 331328 Color: 4
Size: 121254 Color: 9

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 687347 Color: 0
Size: 160785 Color: 18
Size: 151867 Color: 1

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 649962 Color: 4
Size: 195419 Color: 13
Size: 154618 Color: 16

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 640500 Color: 17
Size: 359499 Color: 13

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 656959 Color: 10
Size: 190746 Color: 8
Size: 152293 Color: 1

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 719459 Color: 11
Size: 150827 Color: 7
Size: 129712 Color: 2

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 597285 Color: 10
Size: 253857 Color: 4
Size: 148856 Color: 14

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 552275 Color: 17
Size: 253646 Color: 14
Size: 194077 Color: 8

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 651790 Color: 5
Size: 178590 Color: 3
Size: 169618 Color: 0

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 681546 Color: 2
Size: 196487 Color: 12
Size: 121965 Color: 3

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 719419 Color: 0
Size: 141645 Color: 4
Size: 138934 Color: 5

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 517445 Color: 19
Size: 371117 Color: 14
Size: 111436 Color: 8

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 736000 Color: 14
Size: 142082 Color: 19
Size: 121916 Color: 7

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 602269 Color: 11
Size: 397729 Color: 10

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 764679 Color: 16
Size: 235319 Color: 3

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 561981 Color: 15
Size: 438017 Color: 8

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 760411 Color: 4
Size: 239587 Color: 5

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 646918 Color: 0
Size: 187624 Color: 19
Size: 165455 Color: 1

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 734505 Color: 8
Size: 153175 Color: 17
Size: 112317 Color: 0

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 425144 Color: 16
Size: 390517 Color: 0
Size: 184336 Color: 15

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 622763 Color: 5
Size: 189781 Color: 12
Size: 187453 Color: 18

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 706169 Color: 5
Size: 153716 Color: 18
Size: 140112 Color: 0

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 571731 Color: 15
Size: 428266 Color: 11

Bin 96: 5 of cap free
Amount of items: 3
Items: 
Size: 719475 Color: 5
Size: 158894 Color: 5
Size: 121627 Color: 12

Bin 97: 5 of cap free
Amount of items: 3
Items: 
Size: 747107 Color: 11
Size: 128383 Color: 0
Size: 124506 Color: 14

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 496906 Color: 13
Size: 329316 Color: 5
Size: 173774 Color: 4

Bin 99: 6 of cap free
Amount of items: 3
Items: 
Size: 495690 Color: 7
Size: 267049 Color: 1
Size: 237256 Color: 2

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 656490 Color: 4
Size: 343505 Color: 17

Bin 101: 7 of cap free
Amount of items: 3
Items: 
Size: 656823 Color: 11
Size: 188668 Color: 0
Size: 154503 Color: 4

Bin 102: 7 of cap free
Amount of items: 3
Items: 
Size: 621205 Color: 19
Size: 195810 Color: 8
Size: 182979 Color: 11

Bin 103: 7 of cap free
Amount of items: 3
Items: 
Size: 494655 Color: 5
Size: 339546 Color: 8
Size: 165793 Color: 15

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 658872 Color: 12
Size: 341122 Color: 0

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 606814 Color: 7
Size: 393180 Color: 3

Bin 106: 8 of cap free
Amount of items: 3
Items: 
Size: 580266 Color: 10
Size: 275019 Color: 16
Size: 144708 Color: 12

Bin 107: 8 of cap free
Amount of items: 3
Items: 
Size: 511067 Color: 9
Size: 332397 Color: 5
Size: 156529 Color: 18

Bin 108: 9 of cap free
Amount of items: 3
Items: 
Size: 684532 Color: 0
Size: 159363 Color: 6
Size: 156097 Color: 1

Bin 109: 9 of cap free
Amount of items: 3
Items: 
Size: 730824 Color: 8
Size: 160837 Color: 19
Size: 108331 Color: 16

Bin 110: 9 of cap free
Amount of items: 3
Items: 
Size: 777765 Color: 4
Size: 121167 Color: 1
Size: 101060 Color: 14

Bin 111: 9 of cap free
Amount of items: 2
Items: 
Size: 508898 Color: 5
Size: 491094 Color: 19

Bin 112: 9 of cap free
Amount of items: 2
Items: 
Size: 513210 Color: 8
Size: 486782 Color: 2

Bin 113: 9 of cap free
Amount of items: 2
Items: 
Size: 775368 Color: 1
Size: 224624 Color: 3

Bin 114: 10 of cap free
Amount of items: 3
Items: 
Size: 622913 Color: 9
Size: 196770 Color: 18
Size: 180308 Color: 4

Bin 115: 10 of cap free
Amount of items: 3
Items: 
Size: 510192 Color: 19
Size: 298516 Color: 18
Size: 191283 Color: 6

Bin 116: 10 of cap free
Amount of items: 3
Items: 
Size: 697905 Color: 9
Size: 173482 Color: 14
Size: 128604 Color: 5

Bin 117: 10 of cap free
Amount of items: 3
Items: 
Size: 598474 Color: 6
Size: 275087 Color: 12
Size: 126430 Color: 3

Bin 118: 10 of cap free
Amount of items: 3
Items: 
Size: 517045 Color: 9
Size: 339652 Color: 17
Size: 143294 Color: 10

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 668690 Color: 19
Size: 331301 Color: 18

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 605954 Color: 2
Size: 394037 Color: 14

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 672662 Color: 2
Size: 327329 Color: 19

Bin 122: 11 of cap free
Amount of items: 3
Items: 
Size: 472478 Color: 17
Size: 405397 Color: 10
Size: 122115 Color: 9

Bin 123: 11 of cap free
Amount of items: 3
Items: 
Size: 426947 Color: 13
Size: 313556 Color: 12
Size: 259487 Color: 4

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 653981 Color: 6
Size: 346009 Color: 8

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 764470 Color: 4
Size: 235520 Color: 17

Bin 126: 12 of cap free
Amount of items: 3
Items: 
Size: 698868 Color: 11
Size: 191871 Color: 2
Size: 109250 Color: 16

Bin 127: 12 of cap free
Amount of items: 3
Items: 
Size: 657270 Color: 12
Size: 174361 Color: 1
Size: 168358 Color: 2

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 660414 Color: 1
Size: 339575 Color: 19

Bin 129: 13 of cap free
Amount of items: 3
Items: 
Size: 742277 Color: 10
Size: 142128 Color: 8
Size: 115583 Color: 13

Bin 130: 13 of cap free
Amount of items: 3
Items: 
Size: 735095 Color: 19
Size: 149733 Color: 15
Size: 115160 Color: 6

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 787205 Color: 19
Size: 212783 Color: 8

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 663734 Color: 1
Size: 336254 Color: 4

Bin 133: 14 of cap free
Amount of items: 3
Items: 
Size: 789505 Color: 7
Size: 109068 Color: 12
Size: 101414 Color: 3

Bin 134: 15 of cap free
Amount of items: 2
Items: 
Size: 530646 Color: 4
Size: 469340 Color: 5

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 761663 Color: 2
Size: 238322 Color: 9

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 762730 Color: 6
Size: 237255 Color: 4

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 660898 Color: 12
Size: 339087 Color: 16

Bin 138: 17 of cap free
Amount of items: 3
Items: 
Size: 730418 Color: 5
Size: 145770 Color: 6
Size: 123796 Color: 4

Bin 139: 17 of cap free
Amount of items: 3
Items: 
Size: 679776 Color: 15
Size: 191655 Color: 11
Size: 128553 Color: 11

Bin 140: 17 of cap free
Amount of items: 3
Items: 
Size: 530390 Color: 19
Size: 237227 Color: 16
Size: 232367 Color: 0

Bin 141: 18 of cap free
Amount of items: 2
Items: 
Size: 685058 Color: 8
Size: 314925 Color: 10

Bin 142: 18 of cap free
Amount of items: 3
Items: 
Size: 618952 Color: 19
Size: 193513 Color: 15
Size: 187518 Color: 8

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 677274 Color: 4
Size: 322709 Color: 15

Bin 144: 19 of cap free
Amount of items: 3
Items: 
Size: 615385 Color: 2
Size: 195179 Color: 3
Size: 189418 Color: 16

Bin 145: 19 of cap free
Amount of items: 3
Items: 
Size: 437700 Color: 17
Size: 366504 Color: 18
Size: 195778 Color: 5

Bin 146: 19 of cap free
Amount of items: 2
Items: 
Size: 518439 Color: 3
Size: 481543 Color: 9

Bin 147: 19 of cap free
Amount of items: 2
Items: 
Size: 524030 Color: 13
Size: 475952 Color: 16

Bin 148: 19 of cap free
Amount of items: 2
Items: 
Size: 555390 Color: 11
Size: 444592 Color: 1

Bin 149: 19 of cap free
Amount of items: 2
Items: 
Size: 725328 Color: 8
Size: 274654 Color: 7

Bin 150: 20 of cap free
Amount of items: 2
Items: 
Size: 646557 Color: 4
Size: 353424 Color: 3

Bin 151: 21 of cap free
Amount of items: 3
Items: 
Size: 656102 Color: 7
Size: 177427 Color: 11
Size: 166451 Color: 19

Bin 152: 21 of cap free
Amount of items: 3
Items: 
Size: 704922 Color: 9
Size: 162669 Color: 15
Size: 132389 Color: 2

Bin 153: 21 of cap free
Amount of items: 3
Items: 
Size: 731087 Color: 12
Size: 148362 Color: 8
Size: 120531 Color: 2

Bin 154: 22 of cap free
Amount of items: 3
Items: 
Size: 437070 Color: 11
Size: 392851 Color: 18
Size: 170058 Color: 7

Bin 155: 22 of cap free
Amount of items: 3
Items: 
Size: 582069 Color: 19
Size: 258488 Color: 18
Size: 159422 Color: 5

Bin 156: 22 of cap free
Amount of items: 3
Items: 
Size: 655356 Color: 2
Size: 183670 Color: 17
Size: 160953 Color: 5

Bin 157: 22 of cap free
Amount of items: 2
Items: 
Size: 566193 Color: 2
Size: 433786 Color: 5

Bin 158: 23 of cap free
Amount of items: 2
Items: 
Size: 646918 Color: 17
Size: 353060 Color: 5

Bin 159: 23 of cap free
Amount of items: 3
Items: 
Size: 367046 Color: 10
Size: 330372 Color: 14
Size: 302560 Color: 17

Bin 160: 23 of cap free
Amount of items: 3
Items: 
Size: 679010 Color: 12
Size: 196496 Color: 13
Size: 124472 Color: 0

Bin 161: 23 of cap free
Amount of items: 2
Items: 
Size: 600952 Color: 8
Size: 399026 Color: 16

Bin 162: 24 of cap free
Amount of items: 2
Items: 
Size: 616172 Color: 4
Size: 383805 Color: 7

Bin 163: 24 of cap free
Amount of items: 3
Items: 
Size: 519578 Color: 9
Size: 310232 Color: 13
Size: 170167 Color: 2

Bin 164: 24 of cap free
Amount of items: 3
Items: 
Size: 615378 Color: 11
Size: 266501 Color: 2
Size: 118098 Color: 19

Bin 165: 24 of cap free
Amount of items: 3
Items: 
Size: 678469 Color: 1
Size: 185430 Color: 18
Size: 136078 Color: 0

Bin 166: 24 of cap free
Amount of items: 2
Items: 
Size: 564550 Color: 1
Size: 435427 Color: 19

Bin 167: 24 of cap free
Amount of items: 3
Items: 
Size: 720125 Color: 12
Size: 157720 Color: 10
Size: 122132 Color: 12

Bin 168: 24 of cap free
Amount of items: 3
Items: 
Size: 458022 Color: 19
Size: 390664 Color: 9
Size: 151291 Color: 2

Bin 169: 25 of cap free
Amount of items: 2
Items: 
Size: 547376 Color: 14
Size: 452600 Color: 19

Bin 170: 25 of cap free
Amount of items: 2
Items: 
Size: 691707 Color: 14
Size: 308269 Color: 3

Bin 171: 25 of cap free
Amount of items: 2
Items: 
Size: 743567 Color: 6
Size: 256409 Color: 16

Bin 172: 26 of cap free
Amount of items: 2
Items: 
Size: 530448 Color: 9
Size: 469527 Color: 4

Bin 173: 26 of cap free
Amount of items: 2
Items: 
Size: 584477 Color: 4
Size: 415498 Color: 0

Bin 174: 27 of cap free
Amount of items: 3
Items: 
Size: 656376 Color: 6
Size: 227417 Color: 3
Size: 116181 Color: 0

Bin 175: 28 of cap free
Amount of items: 3
Items: 
Size: 457203 Color: 14
Size: 272213 Color: 9
Size: 270557 Color: 0

Bin 176: 28 of cap free
Amount of items: 2
Items: 
Size: 743519 Color: 7
Size: 256454 Color: 1

Bin 177: 28 of cap free
Amount of items: 3
Items: 
Size: 783626 Color: 8
Size: 108674 Color: 9
Size: 107673 Color: 12

Bin 178: 29 of cap free
Amount of items: 2
Items: 
Size: 601175 Color: 15
Size: 398797 Color: 5

Bin 179: 30 of cap free
Amount of items: 2
Items: 
Size: 673575 Color: 1
Size: 326396 Color: 2

Bin 180: 30 of cap free
Amount of items: 2
Items: 
Size: 588372 Color: 9
Size: 411599 Color: 10

Bin 181: 31 of cap free
Amount of items: 5
Items: 
Size: 248535 Color: 19
Size: 240200 Color: 6
Size: 233069 Color: 13
Size: 157395 Color: 9
Size: 120771 Color: 5

Bin 182: 31 of cap free
Amount of items: 3
Items: 
Size: 564289 Color: 11
Size: 250268 Color: 1
Size: 185413 Color: 14

Bin 183: 31 of cap free
Amount of items: 2
Items: 
Size: 642376 Color: 18
Size: 357594 Color: 7

Bin 184: 32 of cap free
Amount of items: 3
Items: 
Size: 730777 Color: 18
Size: 150776 Color: 9
Size: 118416 Color: 8

Bin 185: 33 of cap free
Amount of items: 3
Items: 
Size: 730556 Color: 0
Size: 146005 Color: 13
Size: 123407 Color: 0

Bin 186: 33 of cap free
Amount of items: 2
Items: 
Size: 584662 Color: 12
Size: 415306 Color: 8

Bin 187: 33 of cap free
Amount of items: 2
Items: 
Size: 736223 Color: 14
Size: 263745 Color: 2

Bin 188: 33 of cap free
Amount of items: 2
Items: 
Size: 763987 Color: 13
Size: 235981 Color: 9

Bin 189: 33 of cap free
Amount of items: 2
Items: 
Size: 770737 Color: 3
Size: 229231 Color: 0

Bin 190: 35 of cap free
Amount of items: 2
Items: 
Size: 609983 Color: 3
Size: 389983 Color: 19

Bin 191: 36 of cap free
Amount of items: 3
Items: 
Size: 678841 Color: 0
Size: 194082 Color: 2
Size: 127042 Color: 4

Bin 192: 36 of cap free
Amount of items: 2
Items: 
Size: 749699 Color: 7
Size: 250266 Color: 12

Bin 193: 37 of cap free
Amount of items: 3
Items: 
Size: 619074 Color: 19
Size: 193093 Color: 9
Size: 187797 Color: 15

Bin 194: 37 of cap free
Amount of items: 2
Items: 
Size: 581569 Color: 9
Size: 418395 Color: 6

Bin 195: 37 of cap free
Amount of items: 2
Items: 
Size: 678244 Color: 17
Size: 321720 Color: 8

Bin 196: 38 of cap free
Amount of items: 3
Items: 
Size: 678789 Color: 18
Size: 181479 Color: 16
Size: 139695 Color: 10

Bin 197: 38 of cap free
Amount of items: 3
Items: 
Size: 423269 Color: 3
Size: 291146 Color: 9
Size: 285548 Color: 4

Bin 198: 38 of cap free
Amount of items: 2
Items: 
Size: 524808 Color: 5
Size: 475155 Color: 3

Bin 199: 38 of cap free
Amount of items: 2
Items: 
Size: 728945 Color: 5
Size: 271018 Color: 4

Bin 200: 39 of cap free
Amount of items: 3
Items: 
Size: 567325 Color: 11
Size: 274349 Color: 5
Size: 158288 Color: 9

Bin 201: 39 of cap free
Amount of items: 2
Items: 
Size: 746728 Color: 2
Size: 253234 Color: 11

Bin 202: 40 of cap free
Amount of items: 3
Items: 
Size: 622066 Color: 16
Size: 198641 Color: 0
Size: 179254 Color: 13

Bin 203: 40 of cap free
Amount of items: 2
Items: 
Size: 794713 Color: 5
Size: 205248 Color: 7

Bin 204: 40 of cap free
Amount of items: 2
Items: 
Size: 598054 Color: 6
Size: 401907 Color: 16

Bin 205: 40 of cap free
Amount of items: 2
Items: 
Size: 795344 Color: 8
Size: 204617 Color: 9

Bin 206: 41 of cap free
Amount of items: 2
Items: 
Size: 526994 Color: 18
Size: 472966 Color: 19

Bin 207: 41 of cap free
Amount of items: 2
Items: 
Size: 553701 Color: 8
Size: 446259 Color: 4

Bin 208: 41 of cap free
Amount of items: 2
Items: 
Size: 649571 Color: 14
Size: 350389 Color: 16

Bin 209: 41 of cap free
Amount of items: 2
Items: 
Size: 716191 Color: 0
Size: 283769 Color: 12

Bin 210: 42 of cap free
Amount of items: 2
Items: 
Size: 575044 Color: 3
Size: 424915 Color: 2

Bin 211: 42 of cap free
Amount of items: 3
Items: 
Size: 427117 Color: 3
Size: 412343 Color: 3
Size: 160499 Color: 10

Bin 212: 43 of cap free
Amount of items: 3
Items: 
Size: 561688 Color: 16
Size: 256644 Color: 0
Size: 181626 Color: 18

Bin 213: 43 of cap free
Amount of items: 2
Items: 
Size: 559228 Color: 3
Size: 440730 Color: 4

Bin 214: 43 of cap free
Amount of items: 2
Items: 
Size: 714317 Color: 16
Size: 285641 Color: 8

Bin 215: 44 of cap free
Amount of items: 2
Items: 
Size: 686982 Color: 16
Size: 312975 Color: 11

Bin 216: 44 of cap free
Amount of items: 2
Items: 
Size: 537178 Color: 18
Size: 462779 Color: 2

Bin 217: 44 of cap free
Amount of items: 2
Items: 
Size: 719824 Color: 17
Size: 280133 Color: 13

Bin 218: 45 of cap free
Amount of items: 3
Items: 
Size: 734677 Color: 10
Size: 149189 Color: 9
Size: 116090 Color: 0

Bin 219: 46 of cap free
Amount of items: 2
Items: 
Size: 712066 Color: 2
Size: 287889 Color: 6

Bin 220: 47 of cap free
Amount of items: 2
Items: 
Size: 642054 Color: 9
Size: 357900 Color: 4

Bin 221: 47 of cap free
Amount of items: 2
Items: 
Size: 626886 Color: 8
Size: 373068 Color: 7

Bin 222: 48 of cap free
Amount of items: 3
Items: 
Size: 496735 Color: 11
Size: 308896 Color: 11
Size: 194322 Color: 16

Bin 223: 48 of cap free
Amount of items: 2
Items: 
Size: 781615 Color: 8
Size: 218338 Color: 19

Bin 224: 48 of cap free
Amount of items: 2
Items: 
Size: 754324 Color: 2
Size: 245629 Color: 13

Bin 225: 48 of cap free
Amount of items: 3
Items: 
Size: 784162 Color: 19
Size: 107974 Color: 10
Size: 107817 Color: 8

Bin 226: 49 of cap free
Amount of items: 3
Items: 
Size: 516380 Color: 14
Size: 373212 Color: 3
Size: 110360 Color: 3

Bin 227: 49 of cap free
Amount of items: 2
Items: 
Size: 654647 Color: 19
Size: 345305 Color: 0

Bin 228: 49 of cap free
Amount of items: 2
Items: 
Size: 788347 Color: 12
Size: 211605 Color: 14

Bin 229: 51 of cap free
Amount of items: 3
Items: 
Size: 788504 Color: 10
Size: 105789 Color: 19
Size: 105657 Color: 18

Bin 230: 51 of cap free
Amount of items: 2
Items: 
Size: 556964 Color: 14
Size: 442986 Color: 3

Bin 231: 52 of cap free
Amount of items: 3
Items: 
Size: 746836 Color: 6
Size: 127882 Color: 16
Size: 125231 Color: 19

Bin 232: 52 of cap free
Amount of items: 3
Items: 
Size: 692406 Color: 14
Size: 195838 Color: 10
Size: 111705 Color: 5

Bin 233: 52 of cap free
Amount of items: 3
Items: 
Size: 546468 Color: 1
Size: 331763 Color: 17
Size: 121718 Color: 7

Bin 234: 52 of cap free
Amount of items: 2
Items: 
Size: 706617 Color: 19
Size: 293332 Color: 18

Bin 235: 52 of cap free
Amount of items: 2
Items: 
Size: 519948 Color: 2
Size: 480001 Color: 6

Bin 236: 53 of cap free
Amount of items: 2
Items: 
Size: 686373 Color: 10
Size: 313575 Color: 13

Bin 237: 53 of cap free
Amount of items: 2
Items: 
Size: 517936 Color: 6
Size: 482012 Color: 4

Bin 238: 53 of cap free
Amount of items: 2
Items: 
Size: 606200 Color: 2
Size: 393748 Color: 4

Bin 239: 54 of cap free
Amount of items: 2
Items: 
Size: 611569 Color: 0
Size: 388378 Color: 9

Bin 240: 54 of cap free
Amount of items: 3
Items: 
Size: 344867 Color: 12
Size: 328705 Color: 5
Size: 326375 Color: 17

Bin 241: 54 of cap free
Amount of items: 2
Items: 
Size: 565167 Color: 12
Size: 434780 Color: 19

Bin 242: 54 of cap free
Amount of items: 2
Items: 
Size: 638971 Color: 18
Size: 360976 Color: 16

Bin 243: 55 of cap free
Amount of items: 3
Items: 
Size: 719238 Color: 17
Size: 141060 Color: 1
Size: 139648 Color: 17

Bin 244: 56 of cap free
Amount of items: 3
Items: 
Size: 543608 Color: 11
Size: 284924 Color: 9
Size: 171413 Color: 9

Bin 245: 56 of cap free
Amount of items: 3
Items: 
Size: 711237 Color: 14
Size: 166688 Color: 0
Size: 122020 Color: 9

Bin 246: 56 of cap free
Amount of items: 3
Items: 
Size: 663906 Color: 16
Size: 172202 Color: 4
Size: 163837 Color: 10

Bin 247: 56 of cap free
Amount of items: 2
Items: 
Size: 646738 Color: 16
Size: 353207 Color: 13

Bin 248: 56 of cap free
Amount of items: 2
Items: 
Size: 585386 Color: 3
Size: 414559 Color: 4

Bin 249: 56 of cap free
Amount of items: 2
Items: 
Size: 590533 Color: 18
Size: 409412 Color: 8

Bin 250: 57 of cap free
Amount of items: 3
Items: 
Size: 498294 Color: 9
Size: 392488 Color: 5
Size: 109162 Color: 18

Bin 251: 57 of cap free
Amount of items: 2
Items: 
Size: 600428 Color: 11
Size: 399516 Color: 6

Bin 252: 57 of cap free
Amount of items: 2
Items: 
Size: 572659 Color: 3
Size: 427285 Color: 5

Bin 253: 58 of cap free
Amount of items: 2
Items: 
Size: 528094 Color: 3
Size: 471849 Color: 12

Bin 254: 58 of cap free
Amount of items: 2
Items: 
Size: 691313 Color: 8
Size: 308630 Color: 7

Bin 255: 59 of cap free
Amount of items: 3
Items: 
Size: 361393 Color: 11
Size: 345439 Color: 2
Size: 293110 Color: 12

Bin 256: 59 of cap free
Amount of items: 3
Items: 
Size: 530749 Color: 12
Size: 304346 Color: 9
Size: 164847 Color: 6

Bin 257: 59 of cap free
Amount of items: 2
Items: 
Size: 718716 Color: 5
Size: 281226 Color: 14

Bin 258: 60 of cap free
Amount of items: 2
Items: 
Size: 673759 Color: 4
Size: 326182 Color: 12

Bin 259: 61 of cap free
Amount of items: 3
Items: 
Size: 423547 Color: 13
Size: 389565 Color: 4
Size: 186828 Color: 12

Bin 260: 61 of cap free
Amount of items: 3
Items: 
Size: 779109 Color: 3
Size: 112423 Color: 9
Size: 108408 Color: 15

Bin 261: 61 of cap free
Amount of items: 2
Items: 
Size: 555039 Color: 5
Size: 444901 Color: 14

Bin 262: 62 of cap free
Amount of items: 2
Items: 
Size: 611238 Color: 2
Size: 388701 Color: 19

Bin 263: 63 of cap free
Amount of items: 2
Items: 
Size: 715831 Color: 2
Size: 284107 Color: 4

Bin 264: 64 of cap free
Amount of items: 3
Items: 
Size: 580725 Color: 0
Size: 235175 Color: 6
Size: 184037 Color: 4

Bin 265: 64 of cap free
Amount of items: 2
Items: 
Size: 792350 Color: 11
Size: 207587 Color: 5

Bin 266: 64 of cap free
Amount of items: 2
Items: 
Size: 798995 Color: 17
Size: 200942 Color: 11

Bin 267: 65 of cap free
Amount of items: 2
Items: 
Size: 758352 Color: 18
Size: 241584 Color: 5

Bin 268: 65 of cap free
Amount of items: 2
Items: 
Size: 563537 Color: 3
Size: 436399 Color: 15

Bin 269: 65 of cap free
Amount of items: 2
Items: 
Size: 591014 Color: 14
Size: 408922 Color: 1

Bin 270: 65 of cap free
Amount of items: 2
Items: 
Size: 748337 Color: 17
Size: 251599 Color: 8

Bin 271: 66 of cap free
Amount of items: 2
Items: 
Size: 515128 Color: 11
Size: 484807 Color: 10

Bin 272: 67 of cap free
Amount of items: 2
Items: 
Size: 547160 Color: 2
Size: 452774 Color: 7

Bin 273: 68 of cap free
Amount of items: 2
Items: 
Size: 676467 Color: 0
Size: 323466 Color: 9

Bin 274: 68 of cap free
Amount of items: 2
Items: 
Size: 548005 Color: 15
Size: 451928 Color: 7

Bin 275: 68 of cap free
Amount of items: 2
Items: 
Size: 615035 Color: 1
Size: 384898 Color: 10

Bin 276: 69 of cap free
Amount of items: 3
Items: 
Size: 770930 Color: 14
Size: 114538 Color: 15
Size: 114464 Color: 15

Bin 277: 70 of cap free
Amount of items: 2
Items: 
Size: 749952 Color: 11
Size: 249979 Color: 2

Bin 278: 70 of cap free
Amount of items: 2
Items: 
Size: 549631 Color: 12
Size: 450300 Color: 19

Bin 279: 70 of cap free
Amount of items: 2
Items: 
Size: 587119 Color: 8
Size: 412812 Color: 15

Bin 280: 70 of cap free
Amount of items: 2
Items: 
Size: 705597 Color: 8
Size: 294334 Color: 14

Bin 281: 70 of cap free
Amount of items: 2
Items: 
Size: 789949 Color: 10
Size: 209982 Color: 1

Bin 282: 71 of cap free
Amount of items: 2
Items: 
Size: 771273 Color: 15
Size: 228657 Color: 10

Bin 283: 71 of cap free
Amount of items: 2
Items: 
Size: 536129 Color: 5
Size: 463801 Color: 0

Bin 284: 71 of cap free
Amount of items: 2
Items: 
Size: 612868 Color: 14
Size: 387062 Color: 3

Bin 285: 72 of cap free
Amount of items: 3
Items: 
Size: 790486 Color: 0
Size: 107083 Color: 3
Size: 102360 Color: 13

Bin 286: 72 of cap free
Amount of items: 2
Items: 
Size: 750087 Color: 14
Size: 249842 Color: 17

Bin 287: 73 of cap free
Amount of items: 3
Items: 
Size: 579537 Color: 2
Size: 226752 Color: 2
Size: 193639 Color: 5

Bin 288: 73 of cap free
Amount of items: 2
Items: 
Size: 583106 Color: 13
Size: 416822 Color: 5

Bin 289: 74 of cap free
Amount of items: 2
Items: 
Size: 596779 Color: 1
Size: 403148 Color: 6

Bin 290: 74 of cap free
Amount of items: 2
Items: 
Size: 741144 Color: 8
Size: 258783 Color: 13

Bin 291: 75 of cap free
Amount of items: 3
Items: 
Size: 720710 Color: 12
Size: 139778 Color: 3
Size: 139438 Color: 12

Bin 292: 76 of cap free
Amount of items: 2
Items: 
Size: 720024 Color: 15
Size: 279901 Color: 0

Bin 293: 77 of cap free
Amount of items: 3
Items: 
Size: 684609 Color: 11
Size: 175700 Color: 1
Size: 139615 Color: 13

Bin 294: 77 of cap free
Amount of items: 2
Items: 
Size: 507678 Color: 16
Size: 492246 Color: 19

Bin 295: 77 of cap free
Amount of items: 2
Items: 
Size: 598744 Color: 16
Size: 401180 Color: 1

Bin 296: 77 of cap free
Amount of items: 2
Items: 
Size: 751713 Color: 11
Size: 248211 Color: 2

Bin 297: 78 of cap free
Amount of items: 2
Items: 
Size: 590675 Color: 5
Size: 409248 Color: 0

Bin 298: 79 of cap free
Amount of items: 2
Items: 
Size: 633210 Color: 1
Size: 366712 Color: 11

Bin 299: 80 of cap free
Amount of items: 2
Items: 
Size: 678896 Color: 11
Size: 321025 Color: 1

Bin 300: 80 of cap free
Amount of items: 2
Items: 
Size: 535519 Color: 14
Size: 464402 Color: 7

Bin 301: 81 of cap free
Amount of items: 2
Items: 
Size: 764238 Color: 13
Size: 235682 Color: 3

Bin 302: 81 of cap free
Amount of items: 2
Items: 
Size: 571440 Color: 14
Size: 428480 Color: 13

Bin 303: 81 of cap free
Amount of items: 2
Items: 
Size: 738227 Color: 9
Size: 261693 Color: 8

Bin 304: 82 of cap free
Amount of items: 3
Items: 
Size: 719178 Color: 14
Size: 172996 Color: 17
Size: 107745 Color: 14

Bin 305: 82 of cap free
Amount of items: 2
Items: 
Size: 581268 Color: 6
Size: 418651 Color: 11

Bin 306: 82 of cap free
Amount of items: 3
Items: 
Size: 633606 Color: 6
Size: 185720 Color: 15
Size: 180593 Color: 5

Bin 307: 82 of cap free
Amount of items: 2
Items: 
Size: 772878 Color: 15
Size: 227041 Color: 19

Bin 308: 83 of cap free
Amount of items: 2
Items: 
Size: 725199 Color: 14
Size: 274719 Color: 18

Bin 309: 84 of cap free
Amount of items: 2
Items: 
Size: 621492 Color: 3
Size: 378425 Color: 1

Bin 310: 84 of cap free
Amount of items: 2
Items: 
Size: 795018 Color: 15
Size: 204899 Color: 17

Bin 311: 85 of cap free
Amount of items: 2
Items: 
Size: 513136 Color: 4
Size: 486780 Color: 3

Bin 312: 85 of cap free
Amount of items: 2
Items: 
Size: 514856 Color: 6
Size: 485060 Color: 7

Bin 313: 86 of cap free
Amount of items: 2
Items: 
Size: 605857 Color: 0
Size: 394058 Color: 14

Bin 314: 86 of cap free
Amount of items: 2
Items: 
Size: 658224 Color: 15
Size: 341691 Color: 18

Bin 315: 86 of cap free
Amount of items: 2
Items: 
Size: 775323 Color: 11
Size: 224592 Color: 10

Bin 316: 88 of cap free
Amount of items: 2
Items: 
Size: 696586 Color: 19
Size: 303327 Color: 8

Bin 317: 88 of cap free
Amount of items: 3
Items: 
Size: 791829 Color: 18
Size: 104383 Color: 16
Size: 103701 Color: 7

Bin 318: 89 of cap free
Amount of items: 2
Items: 
Size: 542379 Color: 14
Size: 457533 Color: 5

Bin 319: 89 of cap free
Amount of items: 2
Items: 
Size: 590032 Color: 18
Size: 409880 Color: 2

Bin 320: 89 of cap free
Amount of items: 2
Items: 
Size: 742969 Color: 5
Size: 256943 Color: 1

Bin 321: 92 of cap free
Amount of items: 2
Items: 
Size: 722205 Color: 14
Size: 277704 Color: 5

Bin 322: 92 of cap free
Amount of items: 3
Items: 
Size: 495098 Color: 0
Size: 325700 Color: 3
Size: 179111 Color: 2

Bin 323: 92 of cap free
Amount of items: 2
Items: 
Size: 557973 Color: 18
Size: 441936 Color: 15

Bin 324: 93 of cap free
Amount of items: 2
Items: 
Size: 763678 Color: 3
Size: 236230 Color: 12

Bin 325: 95 of cap free
Amount of items: 2
Items: 
Size: 622365 Color: 17
Size: 377541 Color: 9

Bin 326: 95 of cap free
Amount of items: 2
Items: 
Size: 643638 Color: 11
Size: 356268 Color: 3

Bin 327: 96 of cap free
Amount of items: 2
Items: 
Size: 733481 Color: 2
Size: 266424 Color: 12

Bin 328: 96 of cap free
Amount of items: 2
Items: 
Size: 775476 Color: 16
Size: 224429 Color: 4

Bin 329: 97 of cap free
Amount of items: 2
Items: 
Size: 776702 Color: 13
Size: 223202 Color: 6

Bin 330: 98 of cap free
Amount of items: 2
Items: 
Size: 656342 Color: 9
Size: 343561 Color: 14

Bin 331: 98 of cap free
Amount of items: 3
Items: 
Size: 584155 Color: 19
Size: 289296 Color: 0
Size: 126452 Color: 14

Bin 332: 99 of cap free
Amount of items: 2
Items: 
Size: 698218 Color: 8
Size: 301684 Color: 13

Bin 333: 99 of cap free
Amount of items: 2
Items: 
Size: 616537 Color: 4
Size: 383365 Color: 13

Bin 334: 100 of cap free
Amount of items: 2
Items: 
Size: 539215 Color: 4
Size: 460686 Color: 17

Bin 335: 100 of cap free
Amount of items: 2
Items: 
Size: 708437 Color: 9
Size: 291464 Color: 13

Bin 336: 101 of cap free
Amount of items: 2
Items: 
Size: 545804 Color: 10
Size: 454096 Color: 11

Bin 337: 102 of cap free
Amount of items: 2
Items: 
Size: 668522 Color: 13
Size: 331377 Color: 19

Bin 338: 102 of cap free
Amount of items: 2
Items: 
Size: 699825 Color: 1
Size: 300074 Color: 0

Bin 339: 103 of cap free
Amount of items: 2
Items: 
Size: 764264 Color: 13
Size: 235634 Color: 18

Bin 340: 103 of cap free
Amount of items: 2
Items: 
Size: 600075 Color: 4
Size: 399823 Color: 10

Bin 341: 103 of cap free
Amount of items: 2
Items: 
Size: 714811 Color: 11
Size: 285087 Color: 13

Bin 342: 104 of cap free
Amount of items: 2
Items: 
Size: 623003 Color: 19
Size: 376894 Color: 14

Bin 343: 104 of cap free
Amount of items: 2
Items: 
Size: 722992 Color: 13
Size: 276905 Color: 18

Bin 344: 105 of cap free
Amount of items: 2
Items: 
Size: 698407 Color: 0
Size: 301489 Color: 2

Bin 345: 105 of cap free
Amount of items: 2
Items: 
Size: 516595 Color: 11
Size: 483301 Color: 4

Bin 346: 105 of cap free
Amount of items: 2
Items: 
Size: 596918 Color: 2
Size: 402978 Color: 3

Bin 347: 105 of cap free
Amount of items: 2
Items: 
Size: 624219 Color: 11
Size: 375677 Color: 13

Bin 348: 106 of cap free
Amount of items: 3
Items: 
Size: 729119 Color: 19
Size: 137205 Color: 1
Size: 133571 Color: 13

Bin 349: 106 of cap free
Amount of items: 2
Items: 
Size: 576364 Color: 14
Size: 423531 Color: 5

Bin 350: 106 of cap free
Amount of items: 2
Items: 
Size: 642360 Color: 0
Size: 357535 Color: 10

Bin 351: 108 of cap free
Amount of items: 2
Items: 
Size: 539364 Color: 9
Size: 460529 Color: 11

Bin 352: 109 of cap free
Amount of items: 3
Items: 
Size: 520712 Color: 8
Size: 246313 Color: 11
Size: 232867 Color: 13

Bin 353: 109 of cap free
Amount of items: 2
Items: 
Size: 508459 Color: 13
Size: 491433 Color: 16

Bin 354: 109 of cap free
Amount of items: 2
Items: 
Size: 611987 Color: 9
Size: 387905 Color: 17

Bin 355: 109 of cap free
Amount of items: 3
Items: 
Size: 642492 Color: 8
Size: 194867 Color: 17
Size: 162533 Color: 6

Bin 356: 110 of cap free
Amount of items: 2
Items: 
Size: 699543 Color: 14
Size: 300348 Color: 19

Bin 357: 110 of cap free
Amount of items: 2
Items: 
Size: 709581 Color: 5
Size: 290310 Color: 13

Bin 358: 112 of cap free
Amount of items: 2
Items: 
Size: 681944 Color: 9
Size: 317945 Color: 7

Bin 359: 112 of cap free
Amount of items: 2
Items: 
Size: 684330 Color: 2
Size: 315559 Color: 8

Bin 360: 112 of cap free
Amount of items: 2
Items: 
Size: 783405 Color: 13
Size: 216484 Color: 1

Bin 361: 114 of cap free
Amount of items: 2
Items: 
Size: 616829 Color: 8
Size: 383058 Color: 7

Bin 362: 115 of cap free
Amount of items: 2
Items: 
Size: 713935 Color: 13
Size: 285951 Color: 6

Bin 363: 115 of cap free
Amount of items: 2
Items: 
Size: 676263 Color: 2
Size: 323623 Color: 17

Bin 364: 115 of cap free
Amount of items: 2
Items: 
Size: 523239 Color: 14
Size: 476647 Color: 9

Bin 365: 116 of cap free
Amount of items: 2
Items: 
Size: 757561 Color: 9
Size: 242324 Color: 8

Bin 366: 116 of cap free
Amount of items: 2
Items: 
Size: 784361 Color: 14
Size: 215524 Color: 7

Bin 367: 117 of cap free
Amount of items: 2
Items: 
Size: 677608 Color: 0
Size: 322276 Color: 12

Bin 368: 117 of cap free
Amount of items: 2
Items: 
Size: 582522 Color: 15
Size: 417362 Color: 13

Bin 369: 117 of cap free
Amount of items: 2
Items: 
Size: 585902 Color: 18
Size: 413982 Color: 3

Bin 370: 118 of cap free
Amount of items: 2
Items: 
Size: 529854 Color: 5
Size: 470029 Color: 3

Bin 371: 118 of cap free
Amount of items: 2
Items: 
Size: 740025 Color: 19
Size: 259858 Color: 13

Bin 372: 119 of cap free
Amount of items: 2
Items: 
Size: 793968 Color: 19
Size: 205914 Color: 1

Bin 373: 119 of cap free
Amount of items: 2
Items: 
Size: 640987 Color: 17
Size: 358895 Color: 14

Bin 374: 120 of cap free
Amount of items: 2
Items: 
Size: 648415 Color: 17
Size: 351466 Color: 0

Bin 375: 120 of cap free
Amount of items: 2
Items: 
Size: 590169 Color: 5
Size: 409712 Color: 16

Bin 376: 122 of cap free
Amount of items: 2
Items: 
Size: 658321 Color: 1
Size: 341558 Color: 2

Bin 377: 124 of cap free
Amount of items: 2
Items: 
Size: 702096 Color: 10
Size: 297781 Color: 4

Bin 378: 124 of cap free
Amount of items: 2
Items: 
Size: 637207 Color: 12
Size: 362670 Color: 1

Bin 379: 125 of cap free
Amount of items: 2
Items: 
Size: 761537 Color: 0
Size: 238339 Color: 3

Bin 380: 128 of cap free
Amount of items: 2
Items: 
Size: 781470 Color: 2
Size: 218403 Color: 1

Bin 381: 129 of cap free
Amount of items: 2
Items: 
Size: 508105 Color: 9
Size: 491767 Color: 0

Bin 382: 130 of cap free
Amount of items: 2
Items: 
Size: 737918 Color: 10
Size: 261953 Color: 12

Bin 383: 130 of cap free
Amount of items: 2
Items: 
Size: 799355 Color: 14
Size: 200516 Color: 13

Bin 384: 131 of cap free
Amount of items: 2
Items: 
Size: 706783 Color: 12
Size: 293087 Color: 7

Bin 385: 132 of cap free
Amount of items: 2
Items: 
Size: 556401 Color: 19
Size: 443468 Color: 8

Bin 386: 133 of cap free
Amount of items: 3
Items: 
Size: 665560 Color: 10
Size: 177783 Color: 1
Size: 156525 Color: 13

Bin 387: 133 of cap free
Amount of items: 2
Items: 
Size: 762170 Color: 9
Size: 237698 Color: 3

Bin 388: 134 of cap free
Amount of items: 3
Items: 
Size: 596906 Color: 4
Size: 272064 Color: 11
Size: 130897 Color: 10

Bin 389: 136 of cap free
Amount of items: 2
Items: 
Size: 669790 Color: 19
Size: 330075 Color: 17

Bin 390: 137 of cap free
Amount of items: 2
Items: 
Size: 637696 Color: 2
Size: 362168 Color: 8

Bin 391: 138 of cap free
Amount of items: 2
Items: 
Size: 721571 Color: 3
Size: 278292 Color: 6

Bin 392: 140 of cap free
Amount of items: 2
Items: 
Size: 555611 Color: 13
Size: 444250 Color: 6

Bin 393: 140 of cap free
Amount of items: 2
Items: 
Size: 581746 Color: 4
Size: 418115 Color: 10

Bin 394: 141 of cap free
Amount of items: 2
Items: 
Size: 521812 Color: 5
Size: 478048 Color: 14

Bin 395: 141 of cap free
Amount of items: 2
Items: 
Size: 651349 Color: 3
Size: 348511 Color: 14

Bin 396: 144 of cap free
Amount of items: 2
Items: 
Size: 574918 Color: 19
Size: 424939 Color: 3

Bin 397: 144 of cap free
Amount of items: 2
Items: 
Size: 780927 Color: 3
Size: 218930 Color: 9

Bin 398: 145 of cap free
Amount of items: 2
Items: 
Size: 681037 Color: 9
Size: 318819 Color: 11

Bin 399: 145 of cap free
Amount of items: 2
Items: 
Size: 545331 Color: 9
Size: 454525 Color: 13

Bin 400: 146 of cap free
Amount of items: 2
Items: 
Size: 735198 Color: 2
Size: 264657 Color: 8

Bin 401: 148 of cap free
Amount of items: 2
Items: 
Size: 649083 Color: 18
Size: 350770 Color: 16

Bin 402: 148 of cap free
Amount of items: 2
Items: 
Size: 577156 Color: 7
Size: 422697 Color: 17

Bin 403: 151 of cap free
Amount of items: 2
Items: 
Size: 663882 Color: 13
Size: 335968 Color: 19

Bin 404: 151 of cap free
Amount of items: 2
Items: 
Size: 659851 Color: 17
Size: 339999 Color: 14

Bin 405: 151 of cap free
Amount of items: 2
Items: 
Size: 511588 Color: 2
Size: 488262 Color: 16

Bin 406: 153 of cap free
Amount of items: 2
Items: 
Size: 546402 Color: 0
Size: 453446 Color: 11

Bin 407: 153 of cap free
Amount of items: 3
Items: 
Size: 628963 Color: 17
Size: 265532 Color: 7
Size: 105353 Color: 7

Bin 408: 154 of cap free
Amount of items: 2
Items: 
Size: 587680 Color: 7
Size: 412167 Color: 10

Bin 409: 156 of cap free
Amount of items: 2
Items: 
Size: 699531 Color: 1
Size: 300314 Color: 16

Bin 410: 157 of cap free
Amount of items: 2
Items: 
Size: 672894 Color: 1
Size: 326950 Color: 13

Bin 411: 159 of cap free
Amount of items: 2
Items: 
Size: 784723 Color: 5
Size: 215119 Color: 8

Bin 412: 160 of cap free
Amount of items: 2
Items: 
Size: 610453 Color: 7
Size: 389388 Color: 17

Bin 413: 163 of cap free
Amount of items: 2
Items: 
Size: 710414 Color: 11
Size: 289424 Color: 9

Bin 414: 164 of cap free
Amount of items: 2
Items: 
Size: 576947 Color: 9
Size: 422890 Color: 14

Bin 415: 166 of cap free
Amount of items: 2
Items: 
Size: 583810 Color: 1
Size: 416025 Color: 5

Bin 416: 166 of cap free
Amount of items: 2
Items: 
Size: 786218 Color: 2
Size: 213617 Color: 17

Bin 417: 169 of cap free
Amount of items: 2
Items: 
Size: 573009 Color: 14
Size: 426823 Color: 11

Bin 418: 169 of cap free
Amount of items: 3
Items: 
Size: 518848 Color: 11
Size: 316589 Color: 17
Size: 164395 Color: 18

Bin 419: 169 of cap free
Amount of items: 2
Items: 
Size: 573609 Color: 2
Size: 426223 Color: 15

Bin 420: 170 of cap free
Amount of items: 2
Items: 
Size: 779782 Color: 6
Size: 220049 Color: 11

Bin 421: 170 of cap free
Amount of items: 2
Items: 
Size: 652676 Color: 15
Size: 347155 Color: 9

Bin 422: 170 of cap free
Amount of items: 2
Items: 
Size: 768698 Color: 12
Size: 231133 Color: 15

Bin 423: 171 of cap free
Amount of items: 2
Items: 
Size: 774137 Color: 0
Size: 225693 Color: 4

Bin 424: 172 of cap free
Amount of items: 2
Items: 
Size: 616186 Color: 13
Size: 383643 Color: 3

Bin 425: 172 of cap free
Amount of items: 2
Items: 
Size: 509763 Color: 0
Size: 490066 Color: 14

Bin 426: 172 of cap free
Amount of items: 2
Items: 
Size: 553775 Color: 12
Size: 446054 Color: 9

Bin 427: 173 of cap free
Amount of items: 3
Items: 
Size: 563964 Color: 17
Size: 273441 Color: 15
Size: 162423 Color: 1

Bin 428: 173 of cap free
Amount of items: 2
Items: 
Size: 709866 Color: 0
Size: 289962 Color: 8

Bin 429: 174 of cap free
Amount of items: 2
Items: 
Size: 693362 Color: 0
Size: 306465 Color: 5

Bin 430: 175 of cap free
Amount of items: 2
Items: 
Size: 689006 Color: 2
Size: 310820 Color: 19

Bin 431: 175 of cap free
Amount of items: 2
Items: 
Size: 653382 Color: 14
Size: 346444 Color: 19

Bin 432: 176 of cap free
Amount of items: 3
Items: 
Size: 518919 Color: 14
Size: 317191 Color: 0
Size: 163715 Color: 9

Bin 433: 177 of cap free
Amount of items: 2
Items: 
Size: 619161 Color: 5
Size: 380663 Color: 19

Bin 434: 177 of cap free
Amount of items: 2
Items: 
Size: 713998 Color: 10
Size: 285826 Color: 19

Bin 435: 182 of cap free
Amount of items: 3
Items: 
Size: 458680 Color: 10
Size: 360445 Color: 6
Size: 180694 Color: 19

Bin 436: 184 of cap free
Amount of items: 2
Items: 
Size: 710206 Color: 3
Size: 289611 Color: 10

Bin 437: 184 of cap free
Amount of items: 3
Items: 
Size: 509268 Color: 13
Size: 344959 Color: 3
Size: 145590 Color: 16

Bin 438: 184 of cap free
Amount of items: 2
Items: 
Size: 720361 Color: 10
Size: 279456 Color: 3

Bin 439: 185 of cap free
Amount of items: 2
Items: 
Size: 648174 Color: 10
Size: 351642 Color: 13

Bin 440: 187 of cap free
Amount of items: 2
Items: 
Size: 739586 Color: 16
Size: 260228 Color: 2

Bin 441: 187 of cap free
Amount of items: 2
Items: 
Size: 792296 Color: 2
Size: 207518 Color: 19

Bin 442: 188 of cap free
Amount of items: 2
Items: 
Size: 559707 Color: 8
Size: 440106 Color: 10

Bin 443: 190 of cap free
Amount of items: 3
Items: 
Size: 580162 Color: 14
Size: 265965 Color: 18
Size: 153684 Color: 16

Bin 444: 190 of cap free
Amount of items: 2
Items: 
Size: 534905 Color: 10
Size: 464906 Color: 16

Bin 445: 191 of cap free
Amount of items: 2
Items: 
Size: 549939 Color: 11
Size: 449871 Color: 9

Bin 446: 193 of cap free
Amount of items: 2
Items: 
Size: 744610 Color: 14
Size: 255198 Color: 10

Bin 447: 195 of cap free
Amount of items: 2
Items: 
Size: 531609 Color: 10
Size: 468197 Color: 4

Bin 448: 196 of cap free
Amount of items: 2
Items: 
Size: 566338 Color: 0
Size: 433467 Color: 2

Bin 449: 196 of cap free
Amount of items: 2
Items: 
Size: 738774 Color: 3
Size: 261031 Color: 7

Bin 450: 200 of cap free
Amount of items: 2
Items: 
Size: 793232 Color: 6
Size: 206569 Color: 14

Bin 451: 202 of cap free
Amount of items: 2
Items: 
Size: 574822 Color: 2
Size: 424977 Color: 19

Bin 452: 206 of cap free
Amount of items: 2
Items: 
Size: 525956 Color: 12
Size: 473839 Color: 16

Bin 453: 208 of cap free
Amount of items: 2
Items: 
Size: 615405 Color: 15
Size: 384388 Color: 17

Bin 454: 208 of cap free
Amount of items: 2
Items: 
Size: 716378 Color: 4
Size: 283415 Color: 2

Bin 455: 209 of cap free
Amount of items: 2
Items: 
Size: 704884 Color: 5
Size: 294908 Color: 17

Bin 456: 209 of cap free
Amount of items: 2
Items: 
Size: 501524 Color: 14
Size: 498268 Color: 19

Bin 457: 210 of cap free
Amount of items: 2
Items: 
Size: 733619 Color: 5
Size: 266172 Color: 19

Bin 458: 210 of cap free
Amount of items: 2
Items: 
Size: 663595 Color: 12
Size: 336196 Color: 19

Bin 459: 212 of cap free
Amount of items: 2
Items: 
Size: 579651 Color: 11
Size: 420138 Color: 9

Bin 460: 213 of cap free
Amount of items: 2
Items: 
Size: 523206 Color: 19
Size: 476582 Color: 13

Bin 461: 214 of cap free
Amount of items: 2
Items: 
Size: 651292 Color: 11
Size: 348495 Color: 5

Bin 462: 214 of cap free
Amount of items: 2
Items: 
Size: 550634 Color: 3
Size: 449153 Color: 8

Bin 463: 215 of cap free
Amount of items: 2
Items: 
Size: 656247 Color: 11
Size: 343539 Color: 6

Bin 464: 217 of cap free
Amount of items: 3
Items: 
Size: 719619 Color: 9
Size: 174339 Color: 9
Size: 105826 Color: 15

Bin 465: 217 of cap free
Amount of items: 2
Items: 
Size: 645565 Color: 15
Size: 354219 Color: 8

Bin 466: 218 of cap free
Amount of items: 2
Items: 
Size: 753939 Color: 16
Size: 245844 Color: 10

Bin 467: 218 of cap free
Amount of items: 2
Items: 
Size: 571659 Color: 13
Size: 428124 Color: 19

Bin 468: 219 of cap free
Amount of items: 2
Items: 
Size: 647025 Color: 3
Size: 352757 Color: 7

Bin 469: 219 of cap free
Amount of items: 2
Items: 
Size: 603790 Color: 13
Size: 395992 Color: 2

Bin 470: 220 of cap free
Amount of items: 3
Items: 
Size: 710984 Color: 1
Size: 180669 Color: 2
Size: 108128 Color: 1

Bin 471: 221 of cap free
Amount of items: 2
Items: 
Size: 663246 Color: 15
Size: 336534 Color: 14

Bin 472: 221 of cap free
Amount of items: 2
Items: 
Size: 522849 Color: 2
Size: 476931 Color: 3

Bin 473: 221 of cap free
Amount of items: 2
Items: 
Size: 534017 Color: 10
Size: 465763 Color: 17

Bin 474: 221 of cap free
Amount of items: 2
Items: 
Size: 741658 Color: 14
Size: 258122 Color: 19

Bin 475: 222 of cap free
Amount of items: 2
Items: 
Size: 669868 Color: 17
Size: 329911 Color: 8

Bin 476: 222 of cap free
Amount of items: 2
Items: 
Size: 718209 Color: 7
Size: 281570 Color: 12

Bin 477: 223 of cap free
Amount of items: 2
Items: 
Size: 520937 Color: 6
Size: 478841 Color: 17

Bin 478: 226 of cap free
Amount of items: 2
Items: 
Size: 708175 Color: 18
Size: 291600 Color: 15

Bin 479: 226 of cap free
Amount of items: 2
Items: 
Size: 518279 Color: 17
Size: 481496 Color: 6

Bin 480: 227 of cap free
Amount of items: 2
Items: 
Size: 582198 Color: 3
Size: 417576 Color: 7

Bin 481: 228 of cap free
Amount of items: 2
Items: 
Size: 779181 Color: 8
Size: 220592 Color: 3

Bin 482: 231 of cap free
Amount of items: 2
Items: 
Size: 748685 Color: 10
Size: 251085 Color: 11

Bin 483: 232 of cap free
Amount of items: 2
Items: 
Size: 524400 Color: 4
Size: 475369 Color: 17

Bin 484: 232 of cap free
Amount of items: 2
Items: 
Size: 515699 Color: 17
Size: 484070 Color: 1

Bin 485: 234 of cap free
Amount of items: 3
Items: 
Size: 623173 Color: 14
Size: 198956 Color: 1
Size: 177638 Color: 6

Bin 486: 234 of cap free
Amount of items: 2
Items: 
Size: 543096 Color: 1
Size: 456671 Color: 12

Bin 487: 236 of cap free
Amount of items: 2
Items: 
Size: 531271 Color: 11
Size: 468494 Color: 10

Bin 488: 238 of cap free
Amount of items: 2
Items: 
Size: 555184 Color: 16
Size: 444579 Color: 5

Bin 489: 238 of cap free
Amount of items: 2
Items: 
Size: 660742 Color: 15
Size: 339021 Color: 17

Bin 490: 239 of cap free
Amount of items: 2
Items: 
Size: 704069 Color: 19
Size: 295693 Color: 18

Bin 491: 240 of cap free
Amount of items: 2
Items: 
Size: 558697 Color: 11
Size: 441064 Color: 19

Bin 492: 241 of cap free
Amount of items: 2
Items: 
Size: 594566 Color: 11
Size: 405194 Color: 18

Bin 493: 242 of cap free
Amount of items: 2
Items: 
Size: 747223 Color: 11
Size: 252536 Color: 12

Bin 494: 244 of cap free
Amount of items: 2
Items: 
Size: 619434 Color: 18
Size: 380323 Color: 6

Bin 495: 245 of cap free
Amount of items: 3
Items: 
Size: 657321 Color: 7
Size: 192462 Color: 0
Size: 149973 Color: 8

Bin 496: 245 of cap free
Amount of items: 2
Items: 
Size: 793893 Color: 1
Size: 205863 Color: 19

Bin 497: 247 of cap free
Amount of items: 2
Items: 
Size: 758517 Color: 9
Size: 241237 Color: 18

Bin 498: 250 of cap free
Amount of items: 2
Items: 
Size: 536853 Color: 0
Size: 462898 Color: 16

Bin 499: 251 of cap free
Amount of items: 2
Items: 
Size: 687997 Color: 17
Size: 311753 Color: 6

Bin 500: 252 of cap free
Amount of items: 2
Items: 
Size: 683960 Color: 2
Size: 315789 Color: 1

Bin 501: 255 of cap free
Amount of items: 3
Items: 
Size: 781538 Color: 7
Size: 110488 Color: 2
Size: 107720 Color: 19

Bin 502: 260 of cap free
Amount of items: 2
Items: 
Size: 521774 Color: 6
Size: 477967 Color: 7

Bin 503: 260 of cap free
Amount of items: 2
Items: 
Size: 528555 Color: 9
Size: 471186 Color: 3

Bin 504: 261 of cap free
Amount of items: 2
Items: 
Size: 586657 Color: 10
Size: 413083 Color: 2

Bin 505: 266 of cap free
Amount of items: 2
Items: 
Size: 676942 Color: 4
Size: 322793 Color: 19

Bin 506: 266 of cap free
Amount of items: 2
Items: 
Size: 777099 Color: 11
Size: 222636 Color: 19

Bin 507: 266 of cap free
Amount of items: 2
Items: 
Size: 763899 Color: 2
Size: 235836 Color: 6

Bin 508: 267 of cap free
Amount of items: 2
Items: 
Size: 717345 Color: 7
Size: 282389 Color: 9

Bin 509: 268 of cap free
Amount of items: 2
Items: 
Size: 724485 Color: 10
Size: 275248 Color: 13

Bin 510: 270 of cap free
Amount of items: 2
Items: 
Size: 506481 Color: 15
Size: 493250 Color: 19

Bin 511: 271 of cap free
Amount of items: 2
Items: 
Size: 774619 Color: 0
Size: 225111 Color: 1

Bin 512: 271 of cap free
Amount of items: 3
Items: 
Size: 535019 Color: 3
Size: 312870 Color: 4
Size: 151841 Color: 8

Bin 513: 273 of cap free
Amount of items: 2
Items: 
Size: 787630 Color: 10
Size: 212098 Color: 9

Bin 514: 276 of cap free
Amount of items: 2
Items: 
Size: 547499 Color: 3
Size: 452226 Color: 13

Bin 515: 279 of cap free
Amount of items: 2
Items: 
Size: 622314 Color: 19
Size: 377408 Color: 13

Bin 516: 280 of cap free
Amount of items: 2
Items: 
Size: 665631 Color: 11
Size: 334090 Color: 10

Bin 517: 281 of cap free
Amount of items: 3
Items: 
Size: 388327 Color: 1
Size: 328285 Color: 13
Size: 283108 Color: 12

Bin 518: 282 of cap free
Amount of items: 2
Items: 
Size: 781729 Color: 3
Size: 217990 Color: 18

Bin 519: 283 of cap free
Amount of items: 2
Items: 
Size: 583809 Color: 14
Size: 415909 Color: 2

Bin 520: 284 of cap free
Amount of items: 2
Items: 
Size: 665824 Color: 10
Size: 333893 Color: 18

Bin 521: 287 of cap free
Amount of items: 2
Items: 
Size: 693921 Color: 4
Size: 305793 Color: 19

Bin 522: 293 of cap free
Amount of items: 2
Items: 
Size: 742164 Color: 15
Size: 257544 Color: 18

Bin 523: 297 of cap free
Amount of items: 2
Items: 
Size: 757458 Color: 16
Size: 242246 Color: 4

Bin 524: 301 of cap free
Amount of items: 2
Items: 
Size: 525345 Color: 0
Size: 474355 Color: 1

Bin 525: 301 of cap free
Amount of items: 2
Items: 
Size: 572266 Color: 2
Size: 427434 Color: 6

Bin 526: 302 of cap free
Amount of items: 2
Items: 
Size: 549026 Color: 12
Size: 450673 Color: 4

Bin 527: 303 of cap free
Amount of items: 2
Items: 
Size: 700531 Color: 12
Size: 299167 Color: 4

Bin 528: 304 of cap free
Amount of items: 2
Items: 
Size: 551581 Color: 4
Size: 448116 Color: 19

Bin 529: 305 of cap free
Amount of items: 2
Items: 
Size: 768620 Color: 11
Size: 231076 Color: 4

Bin 530: 307 of cap free
Amount of items: 3
Items: 
Size: 543055 Color: 16
Size: 286153 Color: 10
Size: 170486 Color: 16

Bin 531: 308 of cap free
Amount of items: 2
Items: 
Size: 617031 Color: 8
Size: 382662 Color: 18

Bin 532: 308 of cap free
Amount of items: 2
Items: 
Size: 694485 Color: 17
Size: 305208 Color: 0

Bin 533: 308 of cap free
Amount of items: 2
Items: 
Size: 717680 Color: 10
Size: 282013 Color: 13

Bin 534: 310 of cap free
Amount of items: 2
Items: 
Size: 690465 Color: 15
Size: 309226 Color: 11

Bin 535: 312 of cap free
Amount of items: 2
Items: 
Size: 541563 Color: 16
Size: 458126 Color: 0

Bin 536: 315 of cap free
Amount of items: 2
Items: 
Size: 572128 Color: 11
Size: 427558 Color: 2

Bin 537: 315 of cap free
Amount of items: 2
Items: 
Size: 562335 Color: 1
Size: 437351 Color: 10

Bin 538: 316 of cap free
Amount of items: 2
Items: 
Size: 531014 Color: 14
Size: 468671 Color: 17

Bin 539: 316 of cap free
Amount of items: 2
Items: 
Size: 767175 Color: 0
Size: 232510 Color: 9

Bin 540: 317 of cap free
Amount of items: 2
Items: 
Size: 573291 Color: 5
Size: 426393 Color: 16

Bin 541: 321 of cap free
Amount of items: 2
Items: 
Size: 631351 Color: 19
Size: 368329 Color: 6

Bin 542: 324 of cap free
Amount of items: 2
Items: 
Size: 583325 Color: 6
Size: 416352 Color: 3

Bin 543: 325 of cap free
Amount of items: 2
Items: 
Size: 553071 Color: 9
Size: 446605 Color: 12

Bin 544: 325 of cap free
Amount of items: 2
Items: 
Size: 677022 Color: 12
Size: 322654 Color: 15

Bin 545: 325 of cap free
Amount of items: 2
Items: 
Size: 606066 Color: 19
Size: 393610 Color: 2

Bin 546: 325 of cap free
Amount of items: 2
Items: 
Size: 647700 Color: 7
Size: 351976 Color: 11

Bin 547: 327 of cap free
Amount of items: 2
Items: 
Size: 643139 Color: 10
Size: 356535 Color: 0

Bin 548: 328 of cap free
Amount of items: 2
Items: 
Size: 564691 Color: 14
Size: 434982 Color: 0

Bin 549: 329 of cap free
Amount of items: 2
Items: 
Size: 640926 Color: 17
Size: 358746 Color: 13

Bin 550: 331 of cap free
Amount of items: 2
Items: 
Size: 544798 Color: 3
Size: 454872 Color: 5

Bin 551: 333 of cap free
Amount of items: 2
Items: 
Size: 533180 Color: 11
Size: 466488 Color: 15

Bin 552: 344 of cap free
Amount of items: 2
Items: 
Size: 790101 Color: 12
Size: 209556 Color: 16

Bin 553: 345 of cap free
Amount of items: 2
Items: 
Size: 594469 Color: 1
Size: 405187 Color: 5

Bin 554: 345 of cap free
Amount of items: 2
Items: 
Size: 797137 Color: 15
Size: 202519 Color: 17

Bin 555: 348 of cap free
Amount of items: 2
Items: 
Size: 729599 Color: 11
Size: 270054 Color: 1

Bin 556: 349 of cap free
Amount of items: 2
Items: 
Size: 663210 Color: 14
Size: 336442 Color: 0

Bin 557: 350 of cap free
Amount of items: 2
Items: 
Size: 676408 Color: 13
Size: 323243 Color: 19

Bin 558: 352 of cap free
Amount of items: 2
Items: 
Size: 669208 Color: 16
Size: 330441 Color: 13

Bin 559: 354 of cap free
Amount of items: 2
Items: 
Size: 672733 Color: 4
Size: 326914 Color: 7

Bin 560: 355 of cap free
Amount of items: 2
Items: 
Size: 570473 Color: 14
Size: 429173 Color: 4

Bin 561: 356 of cap free
Amount of items: 2
Items: 
Size: 758097 Color: 0
Size: 241548 Color: 8

Bin 562: 356 of cap free
Amount of items: 2
Items: 
Size: 553590 Color: 15
Size: 446055 Color: 4

Bin 563: 360 of cap free
Amount of items: 2
Items: 
Size: 624409 Color: 5
Size: 375232 Color: 15

Bin 564: 367 of cap free
Amount of items: 2
Items: 
Size: 732281 Color: 2
Size: 267353 Color: 15

Bin 565: 367 of cap free
Amount of items: 2
Items: 
Size: 512226 Color: 7
Size: 487408 Color: 0

Bin 566: 370 of cap free
Amount of items: 2
Items: 
Size: 784902 Color: 0
Size: 214729 Color: 14

Bin 567: 372 of cap free
Amount of items: 2
Items: 
Size: 749846 Color: 18
Size: 249783 Color: 13

Bin 568: 375 of cap free
Amount of items: 2
Items: 
Size: 635004 Color: 17
Size: 364622 Color: 14

Bin 569: 380 of cap free
Amount of items: 2
Items: 
Size: 739398 Color: 10
Size: 260223 Color: 11

Bin 570: 381 of cap free
Amount of items: 3
Items: 
Size: 720224 Color: 18
Size: 144663 Color: 15
Size: 134733 Color: 18

Bin 571: 381 of cap free
Amount of items: 2
Items: 
Size: 670097 Color: 11
Size: 329523 Color: 12

Bin 572: 381 of cap free
Amount of items: 2
Items: 
Size: 685657 Color: 5
Size: 313963 Color: 12

Bin 573: 385 of cap free
Amount of items: 2
Items: 
Size: 655464 Color: 10
Size: 344152 Color: 19

Bin 574: 390 of cap free
Amount of items: 2
Items: 
Size: 639889 Color: 10
Size: 359722 Color: 15

Bin 575: 390 of cap free
Amount of items: 2
Items: 
Size: 794770 Color: 10
Size: 204841 Color: 18

Bin 576: 392 of cap free
Amount of items: 2
Items: 
Size: 629769 Color: 9
Size: 369840 Color: 5

Bin 577: 395 of cap free
Amount of items: 2
Items: 
Size: 500043 Color: 16
Size: 499563 Color: 9

Bin 578: 396 of cap free
Amount of items: 2
Items: 
Size: 646306 Color: 6
Size: 353299 Color: 0

Bin 579: 397 of cap free
Amount of items: 2
Items: 
Size: 657820 Color: 13
Size: 341784 Color: 0

Bin 580: 399 of cap free
Amount of items: 2
Items: 
Size: 565639 Color: 2
Size: 433963 Color: 18

Bin 581: 401 of cap free
Amount of items: 2
Items: 
Size: 774548 Color: 2
Size: 225052 Color: 6

Bin 582: 401 of cap free
Amount of items: 2
Items: 
Size: 735935 Color: 4
Size: 263665 Color: 6

Bin 583: 403 of cap free
Amount of items: 2
Items: 
Size: 744459 Color: 1
Size: 255139 Color: 8

Bin 584: 406 of cap free
Amount of items: 2
Items: 
Size: 732701 Color: 15
Size: 266894 Color: 7

Bin 585: 410 of cap free
Amount of items: 2
Items: 
Size: 512070 Color: 2
Size: 487521 Color: 14

Bin 586: 413 of cap free
Amount of items: 2
Items: 
Size: 665121 Color: 6
Size: 334467 Color: 18

Bin 587: 413 of cap free
Amount of items: 2
Items: 
Size: 591445 Color: 17
Size: 408143 Color: 2

Bin 588: 413 of cap free
Amount of items: 2
Items: 
Size: 747624 Color: 14
Size: 251964 Color: 15

Bin 589: 415 of cap free
Amount of items: 2
Items: 
Size: 688397 Color: 5
Size: 311189 Color: 7

Bin 590: 416 of cap free
Amount of items: 2
Items: 
Size: 629122 Color: 9
Size: 370463 Color: 1

Bin 591: 419 of cap free
Amount of items: 2
Items: 
Size: 706284 Color: 16
Size: 293298 Color: 11

Bin 592: 421 of cap free
Amount of items: 2
Items: 
Size: 756319 Color: 11
Size: 243261 Color: 15

Bin 593: 421 of cap free
Amount of items: 2
Items: 
Size: 786154 Color: 11
Size: 213426 Color: 1

Bin 594: 422 of cap free
Amount of items: 3
Items: 
Size: 692522 Color: 13
Size: 164772 Color: 3
Size: 142285 Color: 2

Bin 595: 422 of cap free
Amount of items: 2
Items: 
Size: 755628 Color: 15
Size: 243951 Color: 6

Bin 596: 423 of cap free
Amount of items: 2
Items: 
Size: 524873 Color: 5
Size: 474705 Color: 1

Bin 597: 423 of cap free
Amount of items: 2
Items: 
Size: 683885 Color: 7
Size: 315693 Color: 15

Bin 598: 425 of cap free
Amount of items: 2
Items: 
Size: 623766 Color: 14
Size: 375810 Color: 4

Bin 599: 427 of cap free
Amount of items: 2
Items: 
Size: 648052 Color: 10
Size: 351522 Color: 6

Bin 600: 431 of cap free
Amount of items: 2
Items: 
Size: 775872 Color: 7
Size: 223698 Color: 17

Bin 601: 433 of cap free
Amount of items: 2
Items: 
Size: 653916 Color: 14
Size: 345652 Color: 5

Bin 602: 437 of cap free
Amount of items: 2
Items: 
Size: 771542 Color: 3
Size: 228022 Color: 9

Bin 603: 443 of cap free
Amount of items: 2
Items: 
Size: 704074 Color: 15
Size: 295484 Color: 11

Bin 604: 444 of cap free
Amount of items: 2
Items: 
Size: 510165 Color: 9
Size: 489392 Color: 16

Bin 605: 446 of cap free
Amount of items: 2
Items: 
Size: 561808 Color: 10
Size: 437747 Color: 2

Bin 606: 446 of cap free
Amount of items: 2
Items: 
Size: 503416 Color: 9
Size: 496139 Color: 2

Bin 607: 449 of cap free
Amount of items: 2
Items: 
Size: 664385 Color: 12
Size: 335167 Color: 4

Bin 608: 451 of cap free
Amount of items: 2
Items: 
Size: 599182 Color: 12
Size: 400368 Color: 4

Bin 609: 453 of cap free
Amount of items: 2
Items: 
Size: 795847 Color: 13
Size: 203701 Color: 17

Bin 610: 454 of cap free
Amount of items: 2
Items: 
Size: 750267 Color: 14
Size: 249280 Color: 5

Bin 611: 456 of cap free
Amount of items: 2
Items: 
Size: 675892 Color: 13
Size: 323653 Color: 10

Bin 612: 462 of cap free
Amount of items: 2
Items: 
Size: 510216 Color: 13
Size: 489323 Color: 18

Bin 613: 466 of cap free
Amount of items: 2
Items: 
Size: 680016 Color: 13
Size: 319519 Color: 7

Bin 614: 466 of cap free
Amount of items: 2
Items: 
Size: 733178 Color: 18
Size: 266357 Color: 10

Bin 615: 468 of cap free
Amount of items: 2
Items: 
Size: 595655 Color: 14
Size: 403878 Color: 4

Bin 616: 474 of cap free
Amount of items: 2
Items: 
Size: 555113 Color: 15
Size: 444414 Color: 14

Bin 617: 475 of cap free
Amount of items: 2
Items: 
Size: 601651 Color: 4
Size: 397875 Color: 12

Bin 618: 477 of cap free
Amount of items: 2
Items: 
Size: 652493 Color: 2
Size: 347031 Color: 9

Bin 619: 481 of cap free
Amount of items: 2
Items: 
Size: 698693 Color: 15
Size: 300827 Color: 12

Bin 620: 487 of cap free
Amount of items: 2
Items: 
Size: 773976 Color: 17
Size: 225538 Color: 5

Bin 621: 493 of cap free
Amount of items: 2
Items: 
Size: 708165 Color: 2
Size: 291343 Color: 19

Bin 622: 497 of cap free
Amount of items: 2
Items: 
Size: 542171 Color: 1
Size: 457333 Color: 8

Bin 623: 497 of cap free
Amount of items: 2
Items: 
Size: 644019 Color: 14
Size: 355485 Color: 6

Bin 624: 498 of cap free
Amount of items: 2
Items: 
Size: 668278 Color: 8
Size: 331225 Color: 11

Bin 625: 500 of cap free
Amount of items: 2
Items: 
Size: 593323 Color: 6
Size: 406178 Color: 7

Bin 626: 502 of cap free
Amount of items: 2
Items: 
Size: 576969 Color: 18
Size: 422530 Color: 16

Bin 627: 508 of cap free
Amount of items: 2
Items: 
Size: 777871 Color: 15
Size: 221622 Color: 3

Bin 628: 510 of cap free
Amount of items: 2
Items: 
Size: 625490 Color: 0
Size: 374001 Color: 14

Bin 629: 512 of cap free
Amount of items: 3
Items: 
Size: 671958 Color: 5
Size: 198321 Color: 19
Size: 129210 Color: 7

Bin 630: 513 of cap free
Amount of items: 2
Items: 
Size: 569520 Color: 1
Size: 429968 Color: 2

Bin 631: 516 of cap free
Amount of items: 2
Items: 
Size: 750829 Color: 3
Size: 248656 Color: 12

Bin 632: 517 of cap free
Amount of items: 2
Items: 
Size: 653212 Color: 11
Size: 346272 Color: 14

Bin 633: 523 of cap free
Amount of items: 2
Items: 
Size: 501362 Color: 16
Size: 498116 Color: 10

Bin 634: 525 of cap free
Amount of items: 2
Items: 
Size: 525817 Color: 2
Size: 473659 Color: 4

Bin 635: 539 of cap free
Amount of items: 2
Items: 
Size: 602553 Color: 17
Size: 396909 Color: 11

Bin 636: 539 of cap free
Amount of items: 2
Items: 
Size: 769049 Color: 7
Size: 230413 Color: 10

Bin 637: 559 of cap free
Amount of items: 2
Items: 
Size: 578155 Color: 17
Size: 421287 Color: 10

Bin 638: 561 of cap free
Amount of items: 2
Items: 
Size: 592144 Color: 11
Size: 407296 Color: 1

Bin 639: 564 of cap free
Amount of items: 2
Items: 
Size: 579566 Color: 1
Size: 419871 Color: 8

Bin 640: 569 of cap free
Amount of items: 2
Items: 
Size: 692978 Color: 4
Size: 306454 Color: 19

Bin 641: 570 of cap free
Amount of items: 2
Items: 
Size: 695659 Color: 7
Size: 303772 Color: 19

Bin 642: 572 of cap free
Amount of items: 2
Items: 
Size: 799557 Color: 9
Size: 199872 Color: 12

Bin 643: 573 of cap free
Amount of items: 2
Items: 
Size: 685606 Color: 7
Size: 313822 Color: 12

Bin 644: 576 of cap free
Amount of items: 2
Items: 
Size: 618129 Color: 16
Size: 381296 Color: 3

Bin 645: 577 of cap free
Amount of items: 2
Items: 
Size: 715184 Color: 8
Size: 284240 Color: 0

Bin 646: 579 of cap free
Amount of items: 2
Items: 
Size: 772053 Color: 6
Size: 227369 Color: 7

Bin 647: 580 of cap free
Amount of items: 2
Items: 
Size: 793684 Color: 5
Size: 205737 Color: 3

Bin 648: 581 of cap free
Amount of items: 2
Items: 
Size: 782331 Color: 5
Size: 217089 Color: 4

Bin 649: 581 of cap free
Amount of items: 2
Items: 
Size: 616206 Color: 19
Size: 383214 Color: 9

Bin 650: 583 of cap free
Amount of items: 2
Items: 
Size: 594238 Color: 18
Size: 405180 Color: 2

Bin 651: 587 of cap free
Amount of items: 2
Items: 
Size: 511897 Color: 3
Size: 487517 Color: 7

Bin 652: 589 of cap free
Amount of items: 2
Items: 
Size: 661066 Color: 1
Size: 338346 Color: 12

Bin 653: 591 of cap free
Amount of items: 2
Items: 
Size: 752810 Color: 15
Size: 246600 Color: 16

Bin 654: 594 of cap free
Amount of items: 2
Items: 
Size: 762843 Color: 0
Size: 236564 Color: 4

Bin 655: 596 of cap free
Amount of items: 2
Items: 
Size: 694939 Color: 5
Size: 304466 Color: 19

Bin 656: 596 of cap free
Amount of items: 2
Items: 
Size: 712373 Color: 8
Size: 287032 Color: 19

Bin 657: 598 of cap free
Amount of items: 2
Items: 
Size: 749755 Color: 9
Size: 249648 Color: 0

Bin 658: 598 of cap free
Amount of items: 2
Items: 
Size: 592724 Color: 2
Size: 406679 Color: 4

Bin 659: 602 of cap free
Amount of items: 2
Items: 
Size: 751844 Color: 17
Size: 247555 Color: 9

Bin 660: 603 of cap free
Amount of items: 2
Items: 
Size: 721718 Color: 1
Size: 277680 Color: 11

Bin 661: 604 of cap free
Amount of items: 2
Items: 
Size: 568494 Color: 0
Size: 430903 Color: 19

Bin 662: 605 of cap free
Amount of items: 2
Items: 
Size: 703955 Color: 18
Size: 295441 Color: 4

Bin 663: 605 of cap free
Amount of items: 2
Items: 
Size: 514747 Color: 12
Size: 484649 Color: 13

Bin 664: 610 of cap free
Amount of items: 2
Items: 
Size: 592532 Color: 12
Size: 406859 Color: 2

Bin 665: 613 of cap free
Amount of items: 2
Items: 
Size: 574800 Color: 19
Size: 424588 Color: 2

Bin 666: 613 of cap free
Amount of items: 2
Items: 
Size: 739862 Color: 16
Size: 259526 Color: 4

Bin 667: 615 of cap free
Amount of items: 2
Items: 
Size: 632886 Color: 7
Size: 366500 Color: 6

Bin 668: 617 of cap free
Amount of items: 3
Items: 
Size: 423426 Color: 12
Size: 391409 Color: 17
Size: 184549 Color: 2

Bin 669: 617 of cap free
Amount of items: 2
Items: 
Size: 634764 Color: 3
Size: 364620 Color: 13

Bin 670: 624 of cap free
Amount of items: 2
Items: 
Size: 698858 Color: 7
Size: 300519 Color: 14

Bin 671: 624 of cap free
Amount of items: 2
Items: 
Size: 691550 Color: 16
Size: 307827 Color: 19

Bin 672: 624 of cap free
Amount of items: 2
Items: 
Size: 500184 Color: 9
Size: 499193 Color: 1

Bin 673: 632 of cap free
Amount of items: 2
Items: 
Size: 559584 Color: 9
Size: 439785 Color: 3

Bin 674: 637 of cap free
Amount of items: 2
Items: 
Size: 744330 Color: 12
Size: 255034 Color: 6

Bin 675: 639 of cap free
Amount of items: 2
Items: 
Size: 526536 Color: 10
Size: 472826 Color: 8

Bin 676: 639 of cap free
Amount of items: 2
Items: 
Size: 538295 Color: 13
Size: 461067 Color: 1

Bin 677: 643 of cap free
Amount of items: 3
Items: 
Size: 491545 Color: 14
Size: 266573 Color: 17
Size: 241240 Color: 10

Bin 678: 647 of cap free
Amount of items: 2
Items: 
Size: 681505 Color: 7
Size: 317849 Color: 3

Bin 679: 651 of cap free
Amount of items: 2
Items: 
Size: 782184 Color: 0
Size: 217166 Color: 14

Bin 680: 663 of cap free
Amount of items: 2
Items: 
Size: 665618 Color: 14
Size: 333720 Color: 11

Bin 681: 667 of cap free
Amount of items: 2
Items: 
Size: 760468 Color: 19
Size: 238866 Color: 14

Bin 682: 669 of cap free
Amount of items: 2
Items: 
Size: 728183 Color: 12
Size: 271149 Color: 9

Bin 683: 685 of cap free
Amount of items: 2
Items: 
Size: 717916 Color: 13
Size: 281400 Color: 5

Bin 684: 690 of cap free
Amount of items: 2
Items: 
Size: 662002 Color: 8
Size: 337309 Color: 9

Bin 685: 694 of cap free
Amount of items: 2
Items: 
Size: 761680 Color: 2
Size: 237627 Color: 18

Bin 686: 697 of cap free
Amount of items: 2
Items: 
Size: 636823 Color: 4
Size: 362481 Color: 6

Bin 687: 710 of cap free
Amount of items: 2
Items: 
Size: 640762 Color: 10
Size: 358529 Color: 17

Bin 688: 714 of cap free
Amount of items: 2
Items: 
Size: 741221 Color: 4
Size: 258066 Color: 0

Bin 689: 715 of cap free
Amount of items: 2
Items: 
Size: 595525 Color: 16
Size: 403761 Color: 7

Bin 690: 723 of cap free
Amount of items: 2
Items: 
Size: 742582 Color: 16
Size: 256696 Color: 10

Bin 691: 726 of cap free
Amount of items: 2
Items: 
Size: 637629 Color: 5
Size: 361646 Color: 1

Bin 692: 728 of cap free
Amount of items: 2
Items: 
Size: 763550 Color: 3
Size: 235723 Color: 13

Bin 693: 738 of cap free
Amount of items: 2
Items: 
Size: 631007 Color: 2
Size: 368256 Color: 7

Bin 694: 747 of cap free
Amount of items: 2
Items: 
Size: 689361 Color: 19
Size: 309893 Color: 16

Bin 695: 751 of cap free
Amount of items: 2
Items: 
Size: 749926 Color: 0
Size: 249324 Color: 11

Bin 696: 756 of cap free
Amount of items: 2
Items: 
Size: 555588 Color: 5
Size: 443657 Color: 12

Bin 697: 767 of cap free
Amount of items: 2
Items: 
Size: 609486 Color: 6
Size: 389748 Color: 1

Bin 698: 768 of cap free
Amount of items: 2
Items: 
Size: 722605 Color: 10
Size: 276628 Color: 7

Bin 699: 776 of cap free
Amount of items: 2
Items: 
Size: 610959 Color: 8
Size: 388266 Color: 17

Bin 700: 779 of cap free
Amount of items: 2
Items: 
Size: 775836 Color: 4
Size: 223386 Color: 7

Bin 701: 779 of cap free
Amount of items: 2
Items: 
Size: 517480 Color: 14
Size: 481742 Color: 12

Bin 702: 780 of cap free
Amount of items: 2
Items: 
Size: 683021 Color: 9
Size: 316200 Color: 3

Bin 703: 780 of cap free
Amount of items: 2
Items: 
Size: 626625 Color: 3
Size: 372596 Color: 8

Bin 704: 783 of cap free
Amount of items: 2
Items: 
Size: 513703 Color: 16
Size: 485515 Color: 15

Bin 705: 784 of cap free
Amount of items: 2
Items: 
Size: 540686 Color: 10
Size: 458531 Color: 3

Bin 706: 790 of cap free
Amount of items: 2
Items: 
Size: 580535 Color: 19
Size: 418676 Color: 16

Bin 707: 805 of cap free
Amount of items: 2
Items: 
Size: 528539 Color: 1
Size: 470657 Color: 16

Bin 708: 811 of cap free
Amount of items: 2
Items: 
Size: 755246 Color: 16
Size: 243944 Color: 2

Bin 709: 813 of cap free
Amount of items: 2
Items: 
Size: 765382 Color: 8
Size: 233806 Color: 6

Bin 710: 815 of cap free
Amount of items: 2
Items: 
Size: 795610 Color: 11
Size: 203576 Color: 12

Bin 711: 818 of cap free
Amount of items: 2
Items: 
Size: 526462 Color: 4
Size: 472721 Color: 15

Bin 712: 822 of cap free
Amount of items: 2
Items: 
Size: 679774 Color: 10
Size: 319405 Color: 18

Bin 713: 827 of cap free
Amount of items: 2
Items: 
Size: 628107 Color: 12
Size: 371067 Color: 5

Bin 714: 831 of cap free
Amount of items: 2
Items: 
Size: 501104 Color: 0
Size: 498066 Color: 15

Bin 715: 831 of cap free
Amount of items: 2
Items: 
Size: 531862 Color: 16
Size: 467308 Color: 3

Bin 716: 832 of cap free
Amount of items: 2
Items: 
Size: 651174 Color: 6
Size: 347995 Color: 12

Bin 717: 833 of cap free
Amount of items: 2
Items: 
Size: 550256 Color: 0
Size: 448912 Color: 6

Bin 718: 837 of cap free
Amount of items: 2
Items: 
Size: 792334 Color: 19
Size: 206830 Color: 15

Bin 719: 839 of cap free
Amount of items: 2
Items: 
Size: 528363 Color: 9
Size: 470799 Color: 5

Bin 720: 841 of cap free
Amount of items: 2
Items: 
Size: 765360 Color: 2
Size: 233800 Color: 10

Bin 721: 842 of cap free
Amount of items: 2
Items: 
Size: 515361 Color: 12
Size: 483798 Color: 10

Bin 722: 848 of cap free
Amount of items: 2
Items: 
Size: 718143 Color: 0
Size: 281010 Color: 13

Bin 723: 858 of cap free
Amount of items: 2
Items: 
Size: 779559 Color: 17
Size: 219584 Color: 3

Bin 724: 878 of cap free
Amount of items: 2
Items: 
Size: 620311 Color: 15
Size: 378812 Color: 9

Bin 725: 881 of cap free
Amount of items: 2
Items: 
Size: 582778 Color: 19
Size: 416342 Color: 11

Bin 726: 886 of cap free
Amount of items: 2
Items: 
Size: 521541 Color: 17
Size: 477574 Color: 1

Bin 727: 886 of cap free
Amount of items: 2
Items: 
Size: 772966 Color: 5
Size: 226149 Color: 16

Bin 728: 887 of cap free
Amount of items: 2
Items: 
Size: 716364 Color: 9
Size: 282750 Color: 14

Bin 729: 888 of cap free
Amount of items: 2
Items: 
Size: 748091 Color: 18
Size: 251022 Color: 12

Bin 730: 893 of cap free
Amount of items: 2
Items: 
Size: 540059 Color: 0
Size: 459049 Color: 9

Bin 731: 897 of cap free
Amount of items: 2
Items: 
Size: 506396 Color: 3
Size: 492708 Color: 18

Bin 732: 899 of cap free
Amount of items: 2
Items: 
Size: 534463 Color: 16
Size: 464639 Color: 4

Bin 733: 899 of cap free
Amount of items: 2
Items: 
Size: 798354 Color: 1
Size: 200748 Color: 11

Bin 734: 906 of cap free
Amount of items: 2
Items: 
Size: 789117 Color: 13
Size: 209978 Color: 4

Bin 735: 908 of cap free
Amount of items: 2
Items: 
Size: 752689 Color: 11
Size: 246404 Color: 12

Bin 736: 921 of cap free
Amount of items: 2
Items: 
Size: 571912 Color: 11
Size: 427168 Color: 5

Bin 737: 925 of cap free
Amount of items: 2
Items: 
Size: 638950 Color: 11
Size: 360126 Color: 1

Bin 738: 927 of cap free
Amount of items: 2
Items: 
Size: 741142 Color: 14
Size: 257932 Color: 12

Bin 739: 928 of cap free
Amount of items: 2
Items: 
Size: 602499 Color: 6
Size: 396574 Color: 15

Bin 740: 929 of cap free
Amount of items: 2
Items: 
Size: 777809 Color: 0
Size: 221263 Color: 17

Bin 741: 930 of cap free
Amount of items: 2
Items: 
Size: 744171 Color: 14
Size: 254900 Color: 12

Bin 742: 937 of cap free
Amount of items: 2
Items: 
Size: 667784 Color: 18
Size: 331280 Color: 14

Bin 743: 938 of cap free
Amount of items: 3
Items: 
Size: 616593 Color: 9
Size: 200237 Color: 14
Size: 182233 Color: 5

Bin 744: 940 of cap free
Amount of items: 2
Items: 
Size: 767635 Color: 17
Size: 231426 Color: 1

Bin 745: 945 of cap free
Amount of items: 2
Items: 
Size: 645276 Color: 16
Size: 353780 Color: 8

Bin 746: 945 of cap free
Amount of items: 2
Items: 
Size: 795580 Color: 4
Size: 203476 Color: 15

Bin 747: 959 of cap free
Amount of items: 2
Items: 
Size: 576772 Color: 12
Size: 422270 Color: 13

Bin 748: 963 of cap free
Amount of items: 2
Items: 
Size: 561735 Color: 9
Size: 437303 Color: 11

Bin 749: 968 of cap free
Amount of items: 2
Items: 
Size: 673120 Color: 16
Size: 325913 Color: 13

Bin 750: 975 of cap free
Amount of items: 2
Items: 
Size: 535568 Color: 14
Size: 463458 Color: 3

Bin 751: 978 of cap free
Amount of items: 2
Items: 
Size: 774860 Color: 7
Size: 224163 Color: 19

Bin 752: 981 of cap free
Amount of items: 2
Items: 
Size: 539862 Color: 10
Size: 459158 Color: 2

Bin 753: 995 of cap free
Amount of items: 2
Items: 
Size: 702638 Color: 15
Size: 296368 Color: 17

Bin 754: 998 of cap free
Amount of items: 2
Items: 
Size: 756145 Color: 17
Size: 242858 Color: 15

Bin 755: 999 of cap free
Amount of items: 2
Items: 
Size: 557587 Color: 14
Size: 441415 Color: 11

Bin 756: 1003 of cap free
Amount of items: 2
Items: 
Size: 587210 Color: 0
Size: 411788 Color: 13

Bin 757: 1005 of cap free
Amount of items: 2
Items: 
Size: 651105 Color: 15
Size: 347891 Color: 19

Bin 758: 1019 of cap free
Amount of items: 2
Items: 
Size: 724376 Color: 4
Size: 274606 Color: 3

Bin 759: 1019 of cap free
Amount of items: 2
Items: 
Size: 687860 Color: 19
Size: 311122 Color: 17

Bin 760: 1024 of cap free
Amount of items: 2
Items: 
Size: 517239 Color: 5
Size: 481738 Color: 11

Bin 761: 1028 of cap free
Amount of items: 2
Items: 
Size: 625074 Color: 0
Size: 373899 Color: 11

Bin 762: 1048 of cap free
Amount of items: 2
Items: 
Size: 707818 Color: 14
Size: 291135 Color: 17

Bin 763: 1058 of cap free
Amount of items: 2
Items: 
Size: 630653 Color: 6
Size: 368290 Color: 2

Bin 764: 1059 of cap free
Amount of items: 2
Items: 
Size: 535667 Color: 15
Size: 463275 Color: 3

Bin 765: 1066 of cap free
Amount of items: 2
Items: 
Size: 570072 Color: 9
Size: 428863 Color: 0

Bin 766: 1082 of cap free
Amount of items: 3
Items: 
Size: 745233 Color: 1
Size: 152755 Color: 16
Size: 100931 Color: 10

Bin 767: 1083 of cap free
Amount of items: 2
Items: 
Size: 690340 Color: 0
Size: 308578 Color: 8

Bin 768: 1092 of cap free
Amount of items: 2
Items: 
Size: 640554 Color: 11
Size: 358355 Color: 6

Bin 769: 1096 of cap free
Amount of items: 2
Items: 
Size: 507771 Color: 1
Size: 491134 Color: 9

Bin 770: 1098 of cap free
Amount of items: 2
Items: 
Size: 599048 Color: 14
Size: 399855 Color: 9

Bin 771: 1100 of cap free
Amount of items: 2
Items: 
Size: 677005 Color: 16
Size: 321896 Color: 1

Bin 772: 1105 of cap free
Amount of items: 2
Items: 
Size: 700435 Color: 3
Size: 298461 Color: 18

Bin 773: 1105 of cap free
Amount of items: 2
Items: 
Size: 634419 Color: 16
Size: 364477 Color: 6

Bin 774: 1121 of cap free
Amount of items: 2
Items: 
Size: 753816 Color: 13
Size: 245064 Color: 18

Bin 775: 1124 of cap free
Amount of items: 2
Items: 
Size: 501035 Color: 13
Size: 497842 Color: 14

Bin 776: 1130 of cap free
Amount of items: 2
Items: 
Size: 534487 Color: 7
Size: 464384 Color: 0

Bin 777: 1152 of cap free
Amount of items: 2
Items: 
Size: 636499 Color: 10
Size: 362350 Color: 3

Bin 778: 1156 of cap free
Amount of items: 2
Items: 
Size: 583093 Color: 12
Size: 415752 Color: 19

Bin 779: 1161 of cap free
Amount of items: 2
Items: 
Size: 574618 Color: 18
Size: 424222 Color: 6

Bin 780: 1164 of cap free
Amount of items: 2
Items: 
Size: 781188 Color: 17
Size: 217649 Color: 13

Bin 781: 1172 of cap free
Amount of items: 3
Items: 
Size: 579629 Color: 17
Size: 298254 Color: 12
Size: 120946 Color: 10

Bin 782: 1180 of cap free
Amount of items: 2
Items: 
Size: 768669 Color: 19
Size: 230152 Color: 13

Bin 783: 1183 of cap free
Amount of items: 2
Items: 
Size: 557415 Color: 9
Size: 441403 Color: 17

Bin 784: 1183 of cap free
Amount of items: 2
Items: 
Size: 785457 Color: 6
Size: 213361 Color: 16

Bin 785: 1185 of cap free
Amount of items: 2
Items: 
Size: 732631 Color: 15
Size: 266185 Color: 10

Bin 786: 1185 of cap free
Amount of items: 2
Items: 
Size: 676902 Color: 6
Size: 321914 Color: 16

Bin 787: 1199 of cap free
Amount of items: 2
Items: 
Size: 768560 Color: 7
Size: 230242 Color: 11

Bin 788: 1205 of cap free
Amount of items: 2
Items: 
Size: 602195 Color: 19
Size: 396601 Color: 6

Bin 789: 1212 of cap free
Amount of items: 2
Items: 
Size: 630600 Color: 16
Size: 368189 Color: 0

Bin 790: 1218 of cap free
Amount of items: 2
Items: 
Size: 681607 Color: 14
Size: 317176 Color: 12

Bin 791: 1225 of cap free
Amount of items: 2
Items: 
Size: 781881 Color: 4
Size: 216895 Color: 1

Bin 792: 1234 of cap free
Amount of items: 2
Items: 
Size: 650761 Color: 7
Size: 348006 Color: 19

Bin 793: 1251 of cap free
Amount of items: 2
Items: 
Size: 739226 Color: 0
Size: 259524 Color: 13

Bin 794: 1260 of cap free
Amount of items: 2
Items: 
Size: 740612 Color: 3
Size: 258129 Color: 14

Bin 795: 1279 of cap free
Amount of items: 2
Items: 
Size: 667209 Color: 5
Size: 331513 Color: 3

Bin 796: 1281 of cap free
Amount of items: 2
Items: 
Size: 530545 Color: 11
Size: 468175 Color: 1

Bin 797: 1288 of cap free
Amount of items: 2
Items: 
Size: 623673 Color: 5
Size: 375040 Color: 17

Bin 798: 1290 of cap free
Amount of items: 2
Items: 
Size: 598944 Color: 15
Size: 399767 Color: 6

Bin 799: 1291 of cap free
Amount of items: 2
Items: 
Size: 521480 Color: 11
Size: 477230 Color: 10

Bin 800: 1311 of cap free
Amount of items: 2
Items: 
Size: 544724 Color: 1
Size: 453966 Color: 4

Bin 801: 1339 of cap free
Amount of items: 2
Items: 
Size: 597242 Color: 10
Size: 401420 Color: 6

Bin 802: 1344 of cap free
Amount of items: 2
Items: 
Size: 586919 Color: 4
Size: 411738 Color: 16

Bin 803: 1350 of cap free
Amount of items: 2
Items: 
Size: 540021 Color: 13
Size: 458630 Color: 17

Bin 804: 1351 of cap free
Amount of items: 3
Items: 
Size: 457947 Color: 1
Size: 379869 Color: 19
Size: 160834 Color: 16

Bin 805: 1380 of cap free
Amount of items: 2
Items: 
Size: 546112 Color: 18
Size: 452509 Color: 5

Bin 806: 1385 of cap free
Amount of items: 2
Items: 
Size: 612174 Color: 9
Size: 386442 Color: 18

Bin 807: 1390 of cap free
Amount of items: 2
Items: 
Size: 514049 Color: 9
Size: 484562 Color: 14

Bin 808: 1391 of cap free
Amount of items: 2
Items: 
Size: 538430 Color: 1
Size: 460180 Color: 19

Bin 809: 1395 of cap free
Amount of items: 2
Items: 
Size: 574180 Color: 1
Size: 424426 Color: 10

Bin 810: 1419 of cap free
Amount of items: 2
Items: 
Size: 757985 Color: 0
Size: 240597 Color: 5

Bin 811: 1425 of cap free
Amount of items: 2
Items: 
Size: 777672 Color: 7
Size: 220904 Color: 14

Bin 812: 1426 of cap free
Amount of items: 2
Items: 
Size: 743785 Color: 8
Size: 254790 Color: 1

Bin 813: 1430 of cap free
Amount of items: 2
Items: 
Size: 623475 Color: 17
Size: 375096 Color: 15

Bin 814: 1442 of cap free
Amount of items: 2
Items: 
Size: 634279 Color: 2
Size: 364280 Color: 12

Bin 815: 1444 of cap free
Amount of items: 2
Items: 
Size: 638372 Color: 12
Size: 360185 Color: 17

Bin 816: 1452 of cap free
Amount of items: 2
Items: 
Size: 764919 Color: 16
Size: 233630 Color: 9

Bin 817: 1461 of cap free
Amount of items: 2
Items: 
Size: 506130 Color: 11
Size: 492410 Color: 3

Bin 818: 1478 of cap free
Amount of items: 2
Items: 
Size: 564638 Color: 1
Size: 433885 Color: 17

Bin 819: 1485 of cap free
Amount of items: 2
Items: 
Size: 528516 Color: 13
Size: 470000 Color: 3

Bin 820: 1489 of cap free
Amount of items: 2
Items: 
Size: 702352 Color: 5
Size: 296160 Color: 8

Bin 821: 1494 of cap free
Amount of items: 2
Items: 
Size: 720882 Color: 9
Size: 277625 Color: 2

Bin 822: 1504 of cap free
Amount of items: 2
Items: 
Size: 521281 Color: 7
Size: 477216 Color: 16

Bin 823: 1549 of cap free
Amount of items: 2
Items: 
Size: 616065 Color: 0
Size: 382387 Color: 13

Bin 824: 1561 of cap free
Amount of items: 2
Items: 
Size: 726038 Color: 1
Size: 272402 Color: 4

Bin 825: 1590 of cap free
Amount of items: 2
Items: 
Size: 652207 Color: 17
Size: 346204 Color: 19

Bin 826: 1603 of cap free
Amount of items: 2
Items: 
Size: 684963 Color: 9
Size: 313435 Color: 10

Bin 827: 1610 of cap free
Amount of items: 2
Items: 
Size: 768076 Color: 1
Size: 230315 Color: 16

Bin 828: 1636 of cap free
Amount of items: 2
Items: 
Size: 709201 Color: 19
Size: 289164 Color: 9

Bin 829: 1637 of cap free
Amount of items: 2
Items: 
Size: 514737 Color: 9
Size: 483627 Color: 12

Bin 830: 1660 of cap free
Amount of items: 2
Items: 
Size: 616059 Color: 8
Size: 382282 Color: 14

Bin 831: 1678 of cap free
Amount of items: 2
Items: 
Size: 554859 Color: 13
Size: 443464 Color: 2

Bin 832: 1683 of cap free
Amount of items: 2
Items: 
Size: 568107 Color: 13
Size: 430211 Color: 1

Bin 833: 1695 of cap free
Amount of items: 2
Items: 
Size: 634127 Color: 7
Size: 364179 Color: 2

Bin 834: 1707 of cap free
Amount of items: 2
Items: 
Size: 672424 Color: 16
Size: 325870 Color: 10

Bin 835: 1723 of cap free
Amount of items: 2
Items: 
Size: 707094 Color: 8
Size: 291184 Color: 16

Bin 836: 1755 of cap free
Amount of items: 2
Items: 
Size: 652145 Color: 19
Size: 346101 Color: 6

Bin 837: 1813 of cap free
Amount of items: 2
Items: 
Size: 528213 Color: 13
Size: 469975 Color: 18

Bin 838: 1822 of cap free
Amount of items: 2
Items: 
Size: 574039 Color: 11
Size: 424140 Color: 7

Bin 839: 1829 of cap free
Amount of items: 2
Items: 
Size: 652115 Color: 5
Size: 346057 Color: 17

Bin 840: 1898 of cap free
Amount of items: 2
Items: 
Size: 552074 Color: 12
Size: 446029 Color: 19

Bin 841: 1921 of cap free
Amount of items: 2
Items: 
Size: 797575 Color: 10
Size: 200505 Color: 7

Bin 842: 1947 of cap free
Amount of items: 2
Items: 
Size: 768459 Color: 14
Size: 229595 Color: 12

Bin 843: 1969 of cap free
Amount of items: 2
Items: 
Size: 513661 Color: 19
Size: 484371 Color: 0

Bin 844: 1993 of cap free
Amount of items: 2
Items: 
Size: 557018 Color: 18
Size: 440990 Color: 8

Bin 845: 1997 of cap free
Amount of items: 2
Items: 
Size: 793445 Color: 13
Size: 204559 Color: 8

Bin 846: 2003 of cap free
Amount of items: 2
Items: 
Size: 657645 Color: 12
Size: 340353 Color: 14

Bin 847: 2034 of cap free
Amount of items: 2
Items: 
Size: 499982 Color: 14
Size: 497985 Color: 7

Bin 848: 2070 of cap free
Amount of items: 3
Items: 
Size: 520295 Color: 16
Size: 240319 Color: 10
Size: 237317 Color: 13

Bin 849: 2081 of cap free
Amount of items: 2
Items: 
Size: 760422 Color: 11
Size: 237498 Color: 9

Bin 850: 2083 of cap free
Amount of items: 2
Items: 
Size: 601350 Color: 13
Size: 396568 Color: 3

Bin 851: 2087 of cap free
Amount of items: 2
Items: 
Size: 556691 Color: 12
Size: 441223 Color: 11

Bin 852: 2091 of cap free
Amount of items: 3
Items: 
Size: 702449 Color: 18
Size: 147782 Color: 13
Size: 147679 Color: 3

Bin 853: 2115 of cap free
Amount of items: 2
Items: 
Size: 784716 Color: 17
Size: 213170 Color: 11

Bin 854: 2162 of cap free
Amount of items: 2
Items: 
Size: 635977 Color: 15
Size: 361862 Color: 14

Bin 855: 2269 of cap free
Amount of items: 2
Items: 
Size: 548845 Color: 10
Size: 448887 Color: 0

Bin 856: 2273 of cap free
Amount of items: 2
Items: 
Size: 681442 Color: 8
Size: 316286 Color: 17

Bin 857: 2335 of cap free
Amount of items: 2
Items: 
Size: 527784 Color: 10
Size: 469882 Color: 18

Bin 858: 2342 of cap free
Amount of items: 2
Items: 
Size: 601325 Color: 0
Size: 396334 Color: 13

Bin 859: 2464 of cap free
Amount of items: 2
Items: 
Size: 595581 Color: 7
Size: 401956 Color: 10

Bin 860: 2470 of cap free
Amount of items: 2
Items: 
Size: 712306 Color: 19
Size: 285225 Color: 1

Bin 861: 2485 of cap free
Amount of items: 2
Items: 
Size: 797438 Color: 10
Size: 200078 Color: 4

Bin 862: 2504 of cap free
Amount of items: 2
Items: 
Size: 605337 Color: 6
Size: 392160 Color: 15

Bin 863: 2523 of cap free
Amount of items: 2
Items: 
Size: 551542 Color: 13
Size: 445936 Color: 15

Bin 864: 2590 of cap free
Amount of items: 2
Items: 
Size: 612343 Color: 2
Size: 385068 Color: 0

Bin 865: 2652 of cap free
Amount of items: 2
Items: 
Size: 657399 Color: 14
Size: 339950 Color: 12

Bin 866: 2703 of cap free
Amount of items: 3
Items: 
Size: 657796 Color: 5
Size: 197255 Color: 16
Size: 142247 Color: 7

Bin 867: 2788 of cap free
Amount of items: 2
Items: 
Size: 780498 Color: 8
Size: 216715 Color: 1

Bin 868: 2825 of cap free
Amount of items: 2
Items: 
Size: 601205 Color: 15
Size: 395971 Color: 8

Bin 869: 2827 of cap free
Amount of items: 2
Items: 
Size: 720855 Color: 17
Size: 276319 Color: 6

Bin 870: 2889 of cap free
Amount of items: 2
Items: 
Size: 612175 Color: 17
Size: 384937 Color: 1

Bin 871: 3039 of cap free
Amount of items: 2
Items: 
Size: 797411 Color: 0
Size: 199551 Color: 15

Bin 872: 3110 of cap free
Amount of items: 2
Items: 
Size: 628971 Color: 12
Size: 367920 Color: 8

Bin 873: 3214 of cap free
Amount of items: 2
Items: 
Size: 785047 Color: 7
Size: 211740 Color: 1

Bin 874: 3467 of cap free
Amount of items: 2
Items: 
Size: 505703 Color: 8
Size: 490831 Color: 16

Bin 875: 3496 of cap free
Amount of items: 3
Items: 
Size: 698378 Color: 8
Size: 169057 Color: 4
Size: 129070 Color: 11

Bin 876: 3506 of cap free
Amount of items: 2
Items: 
Size: 533735 Color: 13
Size: 462760 Color: 19

Bin 877: 3507 of cap free
Amount of items: 2
Items: 
Size: 533659 Color: 2
Size: 462835 Color: 18

Bin 878: 3514 of cap free
Amount of items: 2
Items: 
Size: 524103 Color: 16
Size: 472384 Color: 2

Bin 879: 3559 of cap free
Amount of items: 2
Items: 
Size: 584795 Color: 17
Size: 411647 Color: 12

Bin 880: 3561 of cap free
Amount of items: 2
Items: 
Size: 590665 Color: 5
Size: 405775 Color: 8

Bin 881: 3631 of cap free
Amount of items: 2
Items: 
Size: 556857 Color: 10
Size: 439513 Color: 7

Bin 882: 3647 of cap free
Amount of items: 2
Items: 
Size: 601270 Color: 8
Size: 395084 Color: 1

Bin 883: 3910 of cap free
Amount of items: 2
Items: 
Size: 600843 Color: 3
Size: 395248 Color: 19

Bin 884: 3919 of cap free
Amount of items: 2
Items: 
Size: 791288 Color: 4
Size: 204794 Color: 9

Bin 885: 3950 of cap free
Amount of items: 2
Items: 
Size: 585286 Color: 18
Size: 410765 Color: 1

Bin 886: 4665 of cap free
Amount of items: 2
Items: 
Size: 600534 Color: 1
Size: 394802 Color: 9

Bin 887: 4983 of cap free
Amount of items: 2
Items: 
Size: 791947 Color: 15
Size: 203071 Color: 0

Bin 888: 5548 of cap free
Amount of items: 2
Items: 
Size: 567384 Color: 2
Size: 427069 Color: 11

Bin 889: 6368 of cap free
Amount of items: 2
Items: 
Size: 698220 Color: 10
Size: 295413 Color: 16

Bin 890: 6672 of cap free
Amount of items: 2
Items: 
Size: 777588 Color: 18
Size: 215741 Color: 19

Bin 891: 6703 of cap free
Amount of items: 2
Items: 
Size: 496668 Color: 10
Size: 496630 Color: 14

Bin 892: 6706 of cap free
Amount of items: 2
Items: 
Size: 790077 Color: 3
Size: 203218 Color: 18

Bin 893: 7194 of cap free
Amount of items: 2
Items: 
Size: 656187 Color: 15
Size: 336620 Color: 11

Bin 894: 15752 of cap free
Amount of items: 2
Items: 
Size: 530291 Color: 10
Size: 453958 Color: 12

Bin 895: 18436 of cap free
Amount of items: 4
Items: 
Size: 509238 Color: 16
Size: 188851 Color: 19
Size: 167013 Color: 10
Size: 116463 Color: 3

Bin 896: 24401 of cap free
Amount of items: 3
Items: 
Size: 719134 Color: 6
Size: 139370 Color: 3
Size: 117096 Color: 7

Total size: 895477482
Total free space: 523414

