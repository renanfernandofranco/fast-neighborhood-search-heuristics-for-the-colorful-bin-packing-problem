Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 278 Color: 1
Size: 255 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 323 Color: 1
Size: 270 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 2
Size: 302 Color: 1
Size: 275 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 318 Color: 4
Size: 278 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 4
Size: 360 Color: 4
Size: 256 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 2
Size: 329 Color: 3
Size: 315 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 3
Size: 272 Color: 4
Size: 264 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 356 Color: 0
Size: 266 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 4
Size: 370 Color: 4
Size: 255 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 313 Color: 3
Size: 290 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 323 Color: 3
Size: 290 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 317 Color: 2
Size: 283 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 355 Color: 1
Size: 261 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 273 Color: 2
Size: 266 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 299 Color: 0
Size: 295 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 2
Size: 265 Color: 4
Size: 263 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 305 Color: 0
Size: 269 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 3
Size: 329 Color: 0
Size: 255 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 267 Color: 1
Size: 256 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 2
Size: 321 Color: 3
Size: 265 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 375 Color: 2
Size: 250 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 3
Size: 354 Color: 3
Size: 290 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 291 Color: 2
Size: 261 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 0
Size: 305 Color: 2
Size: 278 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 283 Color: 0
Size: 263 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 372 Color: 2
Size: 250 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 0
Size: 297 Color: 3
Size: 263 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 274 Color: 4
Size: 267 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 3
Size: 273 Color: 0
Size: 254 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 258 Color: 1
Size: 251 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 3
Size: 314 Color: 4
Size: 273 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 297 Color: 3
Size: 259 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 280 Color: 2
Size: 261 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 4
Size: 330 Color: 3
Size: 309 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 324 Color: 3
Size: 268 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 3
Size: 334 Color: 2
Size: 267 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 2
Size: 337 Color: 3
Size: 322 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 356 Color: 0
Size: 253 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 4
Size: 287 Color: 0
Size: 255 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 2
Size: 350 Color: 3
Size: 291 Color: 2

Total size: 40000
Total free space: 0

