Capicity Bin: 7512
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 3759 Color: 0
Size: 2701 Color: 1
Size: 780 Color: 3
Size: 272 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4156 Color: 1
Size: 2988 Color: 0
Size: 368 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4306 Color: 0
Size: 2335 Color: 1
Size: 871 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4698 Color: 2
Size: 2666 Color: 1
Size: 148 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 0
Size: 2180 Color: 1
Size: 176 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 0
Size: 2148 Color: 2
Size: 128 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 3
Size: 2044 Color: 1
Size: 184 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5410 Color: 2
Size: 1754 Color: 3
Size: 348 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5580 Color: 2
Size: 1468 Color: 3
Size: 464 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5629 Color: 3
Size: 1347 Color: 4
Size: 536 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5615 Color: 4
Size: 1581 Color: 3
Size: 316 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5634 Color: 4
Size: 1566 Color: 3
Size: 312 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5678 Color: 3
Size: 1668 Color: 0
Size: 166 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5841 Color: 3
Size: 934 Color: 2
Size: 737 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5849 Color: 1
Size: 1387 Color: 3
Size: 276 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5894 Color: 0
Size: 1338 Color: 3
Size: 280 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5964 Color: 1
Size: 1380 Color: 3
Size: 168 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6019 Color: 0
Size: 1087 Color: 3
Size: 406 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 3
Size: 962 Color: 1
Size: 468 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6074 Color: 0
Size: 1194 Color: 0
Size: 244 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 3
Size: 1202 Color: 1
Size: 162 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6194 Color: 3
Size: 1182 Color: 1
Size: 136 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6204 Color: 0
Size: 1102 Color: 3
Size: 206 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6230 Color: 0
Size: 1136 Color: 3
Size: 146 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6238 Color: 2
Size: 954 Color: 2
Size: 320 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 1
Size: 684 Color: 3
Size: 544 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 4
Size: 932 Color: 4
Size: 240 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 2
Size: 914 Color: 3
Size: 236 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6391 Color: 4
Size: 897 Color: 3
Size: 224 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 3
Size: 946 Color: 4
Size: 170 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6469 Color: 3
Size: 699 Color: 4
Size: 344 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 0
Size: 608 Color: 3
Size: 412 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6515 Color: 2
Size: 831 Color: 3
Size: 166 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6539 Color: 0
Size: 811 Color: 3
Size: 162 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 4
Size: 782 Color: 3
Size: 156 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6571 Color: 3
Size: 785 Color: 2
Size: 156 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 2
Size: 576 Color: 4
Size: 348 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 2
Size: 651 Color: 3
Size: 232 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 2
Size: 701 Color: 3
Size: 154 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 4
Size: 778 Color: 3
Size: 66 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6682 Color: 0
Size: 722 Color: 3
Size: 108 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 3
Size: 466 Color: 2
Size: 336 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6740 Color: 3
Size: 476 Color: 2
Size: 296 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 0
Size: 737 Color: 3
Size: 16 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5484 Color: 3
Size: 1789 Color: 2
Size: 238 Color: 4

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 2
Size: 1563 Color: 0
Size: 192 Color: 4

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6011 Color: 4
Size: 1028 Color: 0
Size: 472 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6203 Color: 3
Size: 980 Color: 1
Size: 328 Color: 0

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6348 Color: 0
Size: 1163 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6357 Color: 1
Size: 962 Color: 0
Size: 192 Color: 3

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6483 Color: 3
Size: 624 Color: 1
Size: 404 Color: 4

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 3
Size: 852 Color: 2
Size: 152 Color: 1

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 6490 Color: 2
Size: 1021 Color: 1

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 6582 Color: 4
Size: 929 Color: 2

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6619 Color: 2
Size: 764 Color: 3
Size: 128 Color: 4

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 6731 Color: 3
Size: 512 Color: 2
Size: 268 Color: 1

Bin 57: 2 of cap free
Amount of items: 25
Items: 
Size: 532 Color: 2
Size: 464 Color: 1
Size: 464 Color: 1
Size: 424 Color: 0
Size: 416 Color: 2
Size: 404 Color: 0
Size: 400 Color: 1
Size: 356 Color: 0
Size: 348 Color: 2
Size: 314 Color: 1
Size: 288 Color: 3
Size: 280 Color: 4
Size: 278 Color: 4
Size: 248 Color: 1
Size: 236 Color: 4
Size: 232 Color: 3
Size: 224 Color: 3
Size: 222 Color: 4
Size: 216 Color: 3
Size: 216 Color: 2
Size: 210 Color: 0
Size: 208 Color: 2
Size: 200 Color: 4
Size: 186 Color: 0
Size: 144 Color: 2

Bin 58: 2 of cap free
Amount of items: 15
Items: 
Size: 724 Color: 2
Size: 644 Color: 2
Size: 642 Color: 3
Size: 624 Color: 3
Size: 624 Color: 3
Size: 592 Color: 1
Size: 540 Color: 3
Size: 538 Color: 3
Size: 538 Color: 0
Size: 532 Color: 3
Size: 356 Color: 1
Size: 352 Color: 4
Size: 312 Color: 4
Size: 280 Color: 4
Size: 212 Color: 4

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 4246 Color: 1
Size: 3124 Color: 4
Size: 140 Color: 2

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 3
Size: 2722 Color: 1
Size: 304 Color: 0

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 0
Size: 1530 Color: 3
Size: 168 Color: 0

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 6117 Color: 1
Size: 1393 Color: 2

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 0
Size: 884 Color: 3
Size: 256 Color: 2

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 6478 Color: 3
Size: 624 Color: 2
Size: 408 Color: 1

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 5060 Color: 3
Size: 2329 Color: 1
Size: 120 Color: 0

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 6000 Color: 3
Size: 815 Color: 0
Size: 694 Color: 4

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 6650 Color: 0
Size: 859 Color: 1

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 1
Size: 2520 Color: 3
Size: 192 Color: 4

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 5354 Color: 1
Size: 2022 Color: 4
Size: 132 Color: 3

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 5373 Color: 2
Size: 2031 Color: 2
Size: 104 Color: 1

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 5612 Color: 2
Size: 1783 Color: 1
Size: 112 Color: 0

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 5846 Color: 4
Size: 1661 Color: 2

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6287 Color: 1
Size: 1220 Color: 4

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 6452 Color: 1
Size: 1055 Color: 0

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 6084 Color: 1
Size: 1422 Color: 2

Bin 76: 7 of cap free
Amount of items: 5
Items: 
Size: 3762 Color: 1
Size: 1201 Color: 1
Size: 1143 Color: 3
Size: 935 Color: 1
Size: 464 Color: 2

Bin 77: 8 of cap free
Amount of items: 3
Items: 
Size: 4020 Color: 2
Size: 3296 Color: 2
Size: 188 Color: 4

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 6658 Color: 2
Size: 846 Color: 4

Bin 79: 9 of cap free
Amount of items: 3
Items: 
Size: 6503 Color: 1
Size: 972 Color: 2
Size: 28 Color: 3

Bin 80: 10 of cap free
Amount of items: 3
Items: 
Size: 4322 Color: 2
Size: 2916 Color: 3
Size: 264 Color: 1

Bin 81: 10 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 2
Size: 1742 Color: 1
Size: 670 Color: 0

Bin 82: 10 of cap free
Amount of items: 3
Items: 
Size: 5418 Color: 4
Size: 1692 Color: 0
Size: 392 Color: 3

Bin 83: 11 of cap free
Amount of items: 3
Items: 
Size: 6399 Color: 0
Size: 1070 Color: 2
Size: 32 Color: 0

Bin 84: 11 of cap free
Amount of items: 2
Items: 
Size: 6675 Color: 0
Size: 826 Color: 1

Bin 85: 12 of cap free
Amount of items: 3
Items: 
Size: 6378 Color: 4
Size: 1066 Color: 0
Size: 56 Color: 4

Bin 86: 13 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 3
Size: 3129 Color: 0
Size: 120 Color: 1

Bin 87: 13 of cap free
Amount of items: 3
Items: 
Size: 4719 Color: 2
Size: 2588 Color: 1
Size: 192 Color: 3

Bin 88: 13 of cap free
Amount of items: 2
Items: 
Size: 6563 Color: 4
Size: 936 Color: 0

Bin 89: 14 of cap free
Amount of items: 3
Items: 
Size: 6052 Color: 4
Size: 1350 Color: 0
Size: 96 Color: 3

Bin 90: 14 of cap free
Amount of items: 2
Items: 
Size: 6358 Color: 1
Size: 1140 Color: 0

Bin 91: 14 of cap free
Amount of items: 2
Items: 
Size: 6535 Color: 2
Size: 963 Color: 1

Bin 92: 15 of cap free
Amount of items: 2
Items: 
Size: 5637 Color: 1
Size: 1860 Color: 2

Bin 93: 16 of cap free
Amount of items: 2
Items: 
Size: 5004 Color: 0
Size: 2492 Color: 3

Bin 94: 17 of cap free
Amount of items: 3
Items: 
Size: 4281 Color: 3
Size: 2682 Color: 0
Size: 532 Color: 1

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 5402 Color: 0
Size: 2092 Color: 3

Bin 96: 19 of cap free
Amount of items: 2
Items: 
Size: 6073 Color: 4
Size: 1420 Color: 2

Bin 97: 20 of cap free
Amount of items: 2
Items: 
Size: 5426 Color: 2
Size: 2066 Color: 1

Bin 98: 20 of cap free
Amount of items: 2
Items: 
Size: 6703 Color: 1
Size: 789 Color: 4

Bin 99: 21 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 1
Size: 2693 Color: 4
Size: 138 Color: 3

Bin 100: 21 of cap free
Amount of items: 3
Items: 
Size: 6596 Color: 2
Size: 839 Color: 0
Size: 56 Color: 2

Bin 101: 21 of cap free
Amount of items: 2
Items: 
Size: 6700 Color: 2
Size: 791 Color: 1

Bin 102: 23 of cap free
Amount of items: 3
Items: 
Size: 4298 Color: 3
Size: 2687 Color: 4
Size: 504 Color: 4

Bin 103: 23 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 2
Size: 2026 Color: 4
Size: 96 Color: 0

Bin 104: 23 of cap free
Amount of items: 2
Items: 
Size: 6247 Color: 1
Size: 1242 Color: 4

Bin 105: 24 of cap free
Amount of items: 2
Items: 
Size: 6498 Color: 2
Size: 990 Color: 1

Bin 106: 25 of cap free
Amount of items: 2
Items: 
Size: 5910 Color: 4
Size: 1577 Color: 2

Bin 107: 25 of cap free
Amount of items: 2
Items: 
Size: 6742 Color: 4
Size: 745 Color: 0

Bin 108: 26 of cap free
Amount of items: 4
Items: 
Size: 3758 Color: 0
Size: 1762 Color: 1
Size: 1610 Color: 0
Size: 356 Color: 4

Bin 109: 26 of cap free
Amount of items: 3
Items: 
Size: 3852 Color: 4
Size: 2342 Color: 1
Size: 1292 Color: 3

Bin 110: 26 of cap free
Amount of items: 2
Items: 
Size: 6394 Color: 4
Size: 1092 Color: 0

Bin 111: 30 of cap free
Amount of items: 3
Items: 
Size: 5077 Color: 1
Size: 2037 Color: 2
Size: 368 Color: 3

Bin 112: 31 of cap free
Amount of items: 3
Items: 
Size: 4711 Color: 0
Size: 2346 Color: 0
Size: 424 Color: 3

Bin 113: 32 of cap free
Amount of items: 2
Items: 
Size: 5516 Color: 1
Size: 1964 Color: 4

Bin 114: 32 of cap free
Amount of items: 2
Items: 
Size: 5860 Color: 0
Size: 1620 Color: 2

Bin 115: 34 of cap free
Amount of items: 3
Items: 
Size: 5810 Color: 0
Size: 1612 Color: 2
Size: 56 Color: 3

Bin 116: 35 of cap free
Amount of items: 2
Items: 
Size: 3757 Color: 0
Size: 3720 Color: 3

Bin 117: 42 of cap free
Amount of items: 2
Items: 
Size: 6098 Color: 4
Size: 1372 Color: 1

Bin 118: 44 of cap free
Amount of items: 2
Items: 
Size: 5082 Color: 2
Size: 2386 Color: 3

Bin 119: 48 of cap free
Amount of items: 2
Items: 
Size: 4412 Color: 4
Size: 3052 Color: 0

Bin 120: 48 of cap free
Amount of items: 2
Items: 
Size: 4940 Color: 1
Size: 2524 Color: 4

Bin 121: 48 of cap free
Amount of items: 2
Items: 
Size: 6219 Color: 0
Size: 1245 Color: 1

Bin 122: 50 of cap free
Amount of items: 3
Items: 
Size: 3764 Color: 3
Size: 2674 Color: 4
Size: 1024 Color: 2

Bin 123: 54 of cap free
Amount of items: 2
Items: 
Size: 6026 Color: 0
Size: 1432 Color: 4

Bin 124: 67 of cap free
Amount of items: 2
Items: 
Size: 4314 Color: 0
Size: 3131 Color: 4

Bin 125: 73 of cap free
Amount of items: 3
Items: 
Size: 5069 Color: 1
Size: 1746 Color: 0
Size: 624 Color: 4

Bin 126: 78 of cap free
Amount of items: 2
Items: 
Size: 4650 Color: 4
Size: 2784 Color: 2

Bin 127: 80 of cap free
Amount of items: 2
Items: 
Size: 4706 Color: 3
Size: 2726 Color: 4

Bin 128: 89 of cap free
Amount of items: 2
Items: 
Size: 5621 Color: 2
Size: 1802 Color: 4

Bin 129: 93 of cap free
Amount of items: 2
Items: 
Size: 4289 Color: 1
Size: 3130 Color: 3

Bin 130: 94 of cap free
Amount of items: 2
Items: 
Size: 4696 Color: 0
Size: 2722 Color: 4

Bin 131: 98 of cap free
Amount of items: 2
Items: 
Size: 5034 Color: 3
Size: 2380 Color: 2

Bin 132: 113 of cap free
Amount of items: 2
Items: 
Size: 4273 Color: 3
Size: 3126 Color: 4

Bin 133: 5538 of cap free
Amount of items: 12
Items: 
Size: 204 Color: 2
Size: 188 Color: 1
Size: 188 Color: 1
Size: 184 Color: 3
Size: 184 Color: 1
Size: 172 Color: 4
Size: 166 Color: 0
Size: 158 Color: 4
Size: 152 Color: 4
Size: 130 Color: 2
Size: 128 Color: 0
Size: 120 Color: 0

Total size: 991584
Total free space: 7512

