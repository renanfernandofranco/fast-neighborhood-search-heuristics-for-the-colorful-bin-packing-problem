Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 2130 Color: 1
Size: 242 Color: 1
Size: 84 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 2060 Color: 1
Size: 332 Color: 1
Size: 64 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 1
Size: 152 Color: 0
Size: 140 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 1
Size: 128 Color: 0
Size: 126 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 1
Size: 434 Color: 1
Size: 132 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 1
Size: 252 Color: 1
Size: 102 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2030 Color: 1
Size: 226 Color: 1
Size: 200 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 1
Size: 406 Color: 1
Size: 112 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 1
Size: 540 Color: 0
Size: 474 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 1
Size: 444 Color: 1
Size: 170 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 1
Size: 484 Color: 1
Size: 96 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2138 Color: 1
Size: 236 Color: 1
Size: 82 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 840 Color: 1
Size: 80 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 1
Size: 308 Color: 1
Size: 168 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 1
Size: 404 Color: 1
Size: 32 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 1
Size: 491 Color: 1
Size: 272 Color: 0

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 1238 Color: 1
Size: 1020 Color: 1
Size: 122 Color: 0
Size: 76 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 1
Size: 262 Color: 1
Size: 48 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 1
Size: 342 Color: 1
Size: 52 Color: 0

Bin 20: 0 of cap free
Amount of items: 5
Items: 
Size: 1229 Color: 1
Size: 861 Color: 1
Size: 202 Color: 0
Size: 116 Color: 0
Size: 48 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 1
Size: 382 Color: 1
Size: 104 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2156 Color: 1
Size: 200 Color: 1
Size: 100 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1919 Color: 1
Size: 449 Color: 1
Size: 88 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 1
Size: 621 Color: 1
Size: 122 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1834 Color: 1
Size: 570 Color: 1
Size: 52 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 1
Size: 234 Color: 1
Size: 52 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 1
Size: 244 Color: 1
Size: 32 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2046 Color: 1
Size: 330 Color: 1
Size: 80 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 1
Size: 266 Color: 1
Size: 68 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 1
Size: 585 Color: 1
Size: 50 Color: 0

Bin 31: 0 of cap free
Amount of items: 7
Items: 
Size: 954 Color: 1
Size: 718 Color: 1
Size: 364 Color: 1
Size: 172 Color: 1
Size: 96 Color: 0
Size: 88 Color: 0
Size: 64 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 1
Size: 244 Color: 1
Size: 40 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 1
Size: 611 Color: 1
Size: 120 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 1
Size: 874 Color: 1
Size: 172 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 1
Size: 806 Color: 1
Size: 160 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 1
Size: 884 Color: 1
Size: 40 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 1
Size: 514 Color: 1
Size: 44 Color: 0

Bin 38: 0 of cap free
Amount of items: 4
Items: 
Size: 1445 Color: 1
Size: 777 Color: 1
Size: 146 Color: 0
Size: 88 Color: 0

Bin 39: 0 of cap free
Amount of items: 5
Items: 
Size: 1021 Color: 1
Size: 882 Color: 1
Size: 413 Color: 1
Size: 92 Color: 0
Size: 48 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 1
Size: 576 Color: 1
Size: 68 Color: 0

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 1869 Color: 1
Size: 522 Color: 1
Size: 64 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 1
Size: 526 Color: 1
Size: 358 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 1
Size: 684 Color: 1
Size: 116 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 1
Size: 1022 Color: 1
Size: 200 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 1
Size: 669 Color: 1
Size: 100 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 1
Size: 678 Color: 1
Size: 56 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1402 Color: 1
Size: 1045 Color: 1
Size: 8 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 1
Size: 739 Color: 1
Size: 72 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 1
Size: 604 Color: 1
Size: 204 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 1
Size: 504 Color: 1
Size: 176 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 1
Size: 274 Color: 1
Size: 182 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 1
Size: 590 Color: 1
Size: 132 Color: 0

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 1
Size: 513 Color: 1
Size: 8 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 1
Size: 843 Color: 1
Size: 12 Color: 0

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 1
Size: 984 Color: 1
Size: 72 Color: 0

Bin 56: 6 of cap free
Amount of items: 5
Items: 
Size: 1236 Color: 1
Size: 772 Color: 1
Size: 298 Color: 1
Size: 96 Color: 0
Size: 48 Color: 0

Bin 57: 8 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 1
Size: 1007 Color: 1
Size: 16 Color: 0

Bin 58: 9 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 1
Size: 637 Color: 1
Size: 60 Color: 0

Bin 59: 35 of cap free
Amount of items: 3
Items: 
Size: 1688 Color: 1
Size: 613 Color: 1
Size: 120 Color: 0

Bin 60: 61 of cap free
Amount of items: 3
Items: 
Size: 1841 Color: 1
Size: 282 Color: 1
Size: 272 Color: 0

Bin 61: 161 of cap free
Amount of items: 3
Items: 
Size: 1249 Color: 1
Size: 1018 Color: 1
Size: 28 Color: 0

Bin 62: 188 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 1
Size: 654 Color: 1
Size: 384 Color: 0

Bin 63: 246 of cap free
Amount of items: 1
Items: 
Size: 2210 Color: 1

Bin 64: 356 of cap free
Amount of items: 1
Items: 
Size: 2100 Color: 1

Bin 65: 495 of cap free
Amount of items: 1
Items: 
Size: 1961 Color: 1

Bin 66: 865 of cap free
Amount of items: 1
Items: 
Size: 1591 Color: 1

Total size: 159640
Total free space: 2456

