Capicity Bin: 15296
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 10
Items: 
Size: 7656 Color: 0
Size: 1132 Color: 0
Size: 1100 Color: 0
Size: 1040 Color: 0
Size: 992 Color: 1
Size: 964 Color: 1
Size: 928 Color: 1
Size: 876 Color: 1
Size: 352 Color: 1
Size: 256 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 7720 Color: 0
Size: 3578 Color: 1
Size: 2902 Color: 1
Size: 824 Color: 0
Size: 272 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10184 Color: 1
Size: 4264 Color: 1
Size: 848 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10324 Color: 1
Size: 4148 Color: 0
Size: 824 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10372 Color: 0
Size: 4108 Color: 1
Size: 816 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10512 Color: 0
Size: 4520 Color: 1
Size: 264 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11184 Color: 1
Size: 3168 Color: 0
Size: 944 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11336 Color: 0
Size: 3596 Color: 0
Size: 364 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11370 Color: 1
Size: 2898 Color: 1
Size: 1028 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12102 Color: 1
Size: 2600 Color: 0
Size: 594 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12400 Color: 1
Size: 2160 Color: 0
Size: 736 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12439 Color: 0
Size: 2373 Color: 1
Size: 484 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12461 Color: 0
Size: 2363 Color: 1
Size: 472 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12496 Color: 0
Size: 2368 Color: 1
Size: 432 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 1
Size: 2488 Color: 0
Size: 352 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12814 Color: 0
Size: 1922 Color: 0
Size: 560 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12828 Color: 1
Size: 1316 Color: 0
Size: 1152 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 0
Size: 1318 Color: 1
Size: 1002 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 1
Size: 1264 Color: 0
Size: 1096 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13004 Color: 0
Size: 1644 Color: 1
Size: 648 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12943 Color: 1
Size: 1809 Color: 0
Size: 544 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 0
Size: 1900 Color: 0
Size: 376 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12992 Color: 1
Size: 1936 Color: 0
Size: 368 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 0
Size: 1370 Color: 0
Size: 830 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13228 Color: 1
Size: 1740 Color: 0
Size: 328 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13208 Color: 0
Size: 1096 Color: 0
Size: 992 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 1
Size: 1404 Color: 1
Size: 590 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13418 Color: 0
Size: 1274 Color: 1
Size: 604 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13654 Color: 1
Size: 1354 Color: 0
Size: 288 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 1
Size: 1096 Color: 0
Size: 512 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13684 Color: 0
Size: 1348 Color: 0
Size: 264 Color: 1

Bin 32: 1 of cap free
Amount of items: 11
Items: 
Size: 7649 Color: 0
Size: 944 Color: 0
Size: 896 Color: 0
Size: 896 Color: 0
Size: 866 Color: 0
Size: 852 Color: 0
Size: 680 Color: 1
Size: 672 Color: 1
Size: 662 Color: 1
Size: 656 Color: 1
Size: 522 Color: 1

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 9080 Color: 0
Size: 4967 Color: 1
Size: 1248 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 9168 Color: 1
Size: 4175 Color: 1
Size: 1952 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 9277 Color: 0
Size: 5506 Color: 1
Size: 512 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 9337 Color: 0
Size: 5666 Color: 1
Size: 292 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 10351 Color: 1
Size: 4816 Color: 0
Size: 128 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 10782 Color: 0
Size: 4121 Color: 1
Size: 392 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 11319 Color: 1
Size: 2888 Color: 0
Size: 1088 Color: 1

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 11371 Color: 0
Size: 3528 Color: 1
Size: 396 Color: 0

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 11663 Color: 0
Size: 2864 Color: 1
Size: 768 Color: 0

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 12338 Color: 1
Size: 2957 Color: 0

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 12674 Color: 1
Size: 2621 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 12697 Color: 0
Size: 1448 Color: 0
Size: 1150 Color: 1

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 13375 Color: 0
Size: 1496 Color: 1
Size: 424 Color: 0

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 1
Size: 1691 Color: 0

Bin 47: 2 of cap free
Amount of items: 7
Items: 
Size: 7654 Color: 1
Size: 1456 Color: 0
Size: 1424 Color: 0
Size: 1384 Color: 1
Size: 1364 Color: 1
Size: 1308 Color: 1
Size: 704 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 8626 Color: 1
Size: 6384 Color: 1
Size: 284 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 8690 Color: 0
Size: 6344 Color: 1
Size: 260 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 9562 Color: 0
Size: 5484 Color: 1
Size: 248 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 10033 Color: 0
Size: 4973 Color: 1
Size: 288 Color: 0

Bin 52: 2 of cap free
Amount of items: 4
Items: 
Size: 10166 Color: 1
Size: 4724 Color: 0
Size: 304 Color: 0
Size: 100 Color: 1

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 10850 Color: 1
Size: 4164 Color: 0
Size: 280 Color: 0

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 11298 Color: 1
Size: 3644 Color: 1
Size: 352 Color: 0

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 12390 Color: 1
Size: 2904 Color: 0

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 12990 Color: 0
Size: 1264 Color: 1
Size: 1040 Color: 1

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 13127 Color: 1
Size: 2167 Color: 0

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 1
Size: 2070 Color: 0

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 13318 Color: 0
Size: 1976 Color: 1

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 0
Size: 1540 Color: 1

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 8385 Color: 1
Size: 6364 Color: 0
Size: 544 Color: 0

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 11281 Color: 1
Size: 2152 Color: 0
Size: 1860 Color: 0

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 11729 Color: 1
Size: 3260 Color: 0
Size: 304 Color: 1

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 11738 Color: 0
Size: 1971 Color: 0
Size: 1584 Color: 1

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 12153 Color: 0
Size: 3140 Color: 1

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 13264 Color: 1
Size: 2029 Color: 0

Bin 67: 4 of cap free
Amount of items: 10
Items: 
Size: 7650 Color: 0
Size: 1024 Color: 0
Size: 1006 Color: 0
Size: 994 Color: 0
Size: 994 Color: 0
Size: 866 Color: 1
Size: 844 Color: 1
Size: 832 Color: 1
Size: 738 Color: 1
Size: 344 Color: 1

Bin 68: 4 of cap free
Amount of items: 4
Items: 
Size: 8304 Color: 1
Size: 4016 Color: 1
Size: 2476 Color: 0
Size: 496 Color: 0

Bin 69: 4 of cap free
Amount of items: 5
Items: 
Size: 8984 Color: 1
Size: 4159 Color: 0
Size: 1721 Color: 0
Size: 272 Color: 0
Size: 156 Color: 1

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 10924 Color: 0
Size: 3520 Color: 1
Size: 848 Color: 1

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 11888 Color: 1
Size: 3404 Color: 0

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 10091 Color: 1
Size: 4248 Color: 0
Size: 952 Color: 1

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 10099 Color: 0
Size: 5192 Color: 1

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 11749 Color: 1
Size: 3262 Color: 0
Size: 280 Color: 1

Bin 75: 6 of cap free
Amount of items: 3
Items: 
Size: 8562 Color: 0
Size: 6376 Color: 1
Size: 352 Color: 0

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 1
Size: 3702 Color: 1
Size: 376 Color: 0

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 12380 Color: 0
Size: 2910 Color: 1

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 13138 Color: 0
Size: 2152 Color: 1

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 0
Size: 1650 Color: 1

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 13724 Color: 1
Size: 1566 Color: 0

Bin 81: 7 of cap free
Amount of items: 12
Items: 
Size: 7653 Color: 1
Size: 848 Color: 0
Size: 840 Color: 0
Size: 834 Color: 0
Size: 824 Color: 0
Size: 768 Color: 0
Size: 752 Color: 0
Size: 648 Color: 1
Size: 608 Color: 1
Size: 592 Color: 1
Size: 580 Color: 1
Size: 342 Color: 1

Bin 82: 8 of cap free
Amount of items: 3
Items: 
Size: 9048 Color: 1
Size: 5500 Color: 1
Size: 740 Color: 0

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 9588 Color: 1
Size: 5444 Color: 0
Size: 256 Color: 1

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 10307 Color: 0
Size: 4981 Color: 1

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 0
Size: 1964 Color: 1

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 1
Size: 1776 Color: 0

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 0
Size: 1736 Color: 1

Bin 88: 10 of cap free
Amount of items: 8
Items: 
Size: 7664 Color: 0
Size: 1272 Color: 0
Size: 1248 Color: 0
Size: 1154 Color: 0
Size: 1108 Color: 1
Size: 1088 Color: 1
Size: 1040 Color: 1
Size: 712 Color: 1

Bin 89: 10 of cap free
Amount of items: 2
Items: 
Size: 11064 Color: 1
Size: 4222 Color: 0

Bin 90: 10 of cap free
Amount of items: 2
Items: 
Size: 12166 Color: 0
Size: 3120 Color: 1

Bin 91: 10 of cap free
Amount of items: 2
Items: 
Size: 12910 Color: 1
Size: 2376 Color: 0

Bin 92: 11 of cap free
Amount of items: 2
Items: 
Size: 10013 Color: 1
Size: 5272 Color: 0

Bin 93: 12 of cap free
Amount of items: 3
Items: 
Size: 8700 Color: 1
Size: 6328 Color: 0
Size: 256 Color: 0

Bin 94: 12 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 1
Size: 2524 Color: 0

Bin 95: 12 of cap free
Amount of items: 3
Items: 
Size: 13732 Color: 0
Size: 1520 Color: 1
Size: 32 Color: 1

Bin 96: 13 of cap free
Amount of items: 3
Items: 
Size: 8429 Color: 0
Size: 6374 Color: 1
Size: 480 Color: 1

Bin 97: 13 of cap free
Amount of items: 2
Items: 
Size: 12931 Color: 1
Size: 2352 Color: 0

Bin 98: 14 of cap free
Amount of items: 3
Items: 
Size: 10230 Color: 1
Size: 4764 Color: 0
Size: 288 Color: 0

Bin 99: 14 of cap free
Amount of items: 2
Items: 
Size: 13540 Color: 1
Size: 1742 Color: 0

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 12844 Color: 1
Size: 2436 Color: 0

Bin 101: 17 of cap free
Amount of items: 2
Items: 
Size: 11299 Color: 1
Size: 3980 Color: 0

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 13452 Color: 1
Size: 1827 Color: 0

Bin 103: 18 of cap free
Amount of items: 2
Items: 
Size: 13616 Color: 0
Size: 1662 Color: 1

Bin 104: 19 of cap free
Amount of items: 3
Items: 
Size: 12720 Color: 0
Size: 2381 Color: 1
Size: 176 Color: 1

Bin 105: 19 of cap free
Amount of items: 2
Items: 
Size: 13233 Color: 0
Size: 2044 Color: 1

Bin 106: 20 of cap free
Amount of items: 2
Items: 
Size: 12520 Color: 1
Size: 2756 Color: 0

Bin 107: 20 of cap free
Amount of items: 2
Items: 
Size: 12660 Color: 0
Size: 2616 Color: 1

Bin 108: 20 of cap free
Amount of items: 3
Items: 
Size: 13674 Color: 1
Size: 1582 Color: 0
Size: 20 Color: 0

Bin 109: 21 of cap free
Amount of items: 2
Items: 
Size: 9498 Color: 0
Size: 5777 Color: 1

Bin 110: 21 of cap free
Amount of items: 2
Items: 
Size: 9552 Color: 1
Size: 5723 Color: 0

Bin 111: 22 of cap free
Amount of items: 2
Items: 
Size: 12612 Color: 0
Size: 2662 Color: 1

Bin 112: 22 of cap free
Amount of items: 2
Items: 
Size: 13210 Color: 1
Size: 2064 Color: 0

Bin 113: 24 of cap free
Amount of items: 2
Items: 
Size: 10120 Color: 0
Size: 5152 Color: 1

Bin 114: 24 of cap free
Amount of items: 2
Items: 
Size: 11832 Color: 1
Size: 3440 Color: 0

Bin 115: 24 of cap free
Amount of items: 2
Items: 
Size: 13068 Color: 0
Size: 2204 Color: 1

Bin 116: 24 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 0
Size: 1848 Color: 1

Bin 117: 24 of cap free
Amount of items: 2
Items: 
Size: 13402 Color: 1
Size: 1870 Color: 0

Bin 118: 24 of cap free
Amount of items: 2
Items: 
Size: 13576 Color: 0
Size: 1696 Color: 1

Bin 119: 25 of cap free
Amount of items: 3
Items: 
Size: 9257 Color: 1
Size: 5502 Color: 1
Size: 512 Color: 0

Bin 120: 25 of cap free
Amount of items: 2
Items: 
Size: 11924 Color: 0
Size: 3347 Color: 1

Bin 121: 26 of cap free
Amount of items: 2
Items: 
Size: 11996 Color: 0
Size: 3274 Color: 1

Bin 122: 26 of cap free
Amount of items: 2
Items: 
Size: 12332 Color: 1
Size: 2938 Color: 0

Bin 123: 26 of cap free
Amount of items: 2
Items: 
Size: 12848 Color: 0
Size: 2422 Color: 1

Bin 124: 27 of cap free
Amount of items: 2
Items: 
Size: 13668 Color: 0
Size: 1601 Color: 1

Bin 125: 28 of cap free
Amount of items: 2
Items: 
Size: 12940 Color: 0
Size: 2328 Color: 1

Bin 126: 28 of cap free
Amount of items: 3
Items: 
Size: 13718 Color: 1
Size: 1518 Color: 0
Size: 32 Color: 0

Bin 127: 29 of cap free
Amount of items: 2
Items: 
Size: 11568 Color: 1
Size: 3699 Color: 0

Bin 128: 30 of cap free
Amount of items: 2
Items: 
Size: 10988 Color: 0
Size: 4278 Color: 1

Bin 129: 30 of cap free
Amount of items: 2
Items: 
Size: 13542 Color: 0
Size: 1724 Color: 1

Bin 130: 31 of cap free
Amount of items: 2
Items: 
Size: 10238 Color: 1
Size: 5027 Color: 0

Bin 131: 31 of cap free
Amount of items: 2
Items: 
Size: 12654 Color: 1
Size: 2611 Color: 0

Bin 132: 34 of cap free
Amount of items: 2
Items: 
Size: 13111 Color: 0
Size: 2151 Color: 1

Bin 133: 35 of cap free
Amount of items: 2
Items: 
Size: 12449 Color: 0
Size: 2812 Color: 1

Bin 134: 36 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 0
Size: 6176 Color: 1
Size: 68 Color: 1

Bin 135: 36 of cap free
Amount of items: 2
Items: 
Size: 13200 Color: 1
Size: 2060 Color: 0

Bin 136: 36 of cap free
Amount of items: 2
Items: 
Size: 13269 Color: 0
Size: 1991 Color: 1

Bin 137: 36 of cap free
Amount of items: 2
Items: 
Size: 13620 Color: 1
Size: 1640 Color: 0

Bin 138: 37 of cap free
Amount of items: 2
Items: 
Size: 12715 Color: 1
Size: 2544 Color: 0

Bin 139: 38 of cap free
Amount of items: 7
Items: 
Size: 7668 Color: 0
Size: 1352 Color: 1
Size: 1286 Color: 0
Size: 1272 Color: 0
Size: 1272 Color: 0
Size: 1264 Color: 1
Size: 1144 Color: 1

Bin 140: 38 of cap free
Amount of items: 2
Items: 
Size: 13488 Color: 1
Size: 1770 Color: 0

Bin 141: 39 of cap free
Amount of items: 2
Items: 
Size: 11822 Color: 1
Size: 3435 Color: 0

Bin 142: 40 of cap free
Amount of items: 2
Items: 
Size: 11400 Color: 1
Size: 3856 Color: 0

Bin 143: 40 of cap free
Amount of items: 2
Items: 
Size: 13340 Color: 0
Size: 1916 Color: 1

Bin 144: 42 of cap free
Amount of items: 2
Items: 
Size: 11036 Color: 0
Size: 4218 Color: 1

Bin 145: 43 of cap free
Amount of items: 2
Items: 
Size: 10560 Color: 0
Size: 4693 Color: 1

Bin 146: 47 of cap free
Amount of items: 3
Items: 
Size: 8365 Color: 1
Size: 6372 Color: 1
Size: 512 Color: 0

Bin 147: 47 of cap free
Amount of items: 2
Items: 
Size: 10216 Color: 0
Size: 5033 Color: 1

Bin 148: 48 of cap free
Amount of items: 2
Items: 
Size: 8698 Color: 0
Size: 6550 Color: 1

Bin 149: 48 of cap free
Amount of items: 2
Items: 
Size: 13336 Color: 0
Size: 1912 Color: 1

Bin 150: 50 of cap free
Amount of items: 2
Items: 
Size: 10859 Color: 0
Size: 4387 Color: 1

Bin 151: 53 of cap free
Amount of items: 2
Items: 
Size: 10107 Color: 1
Size: 5136 Color: 0

Bin 152: 61 of cap free
Amount of items: 3
Items: 
Size: 12144 Color: 1
Size: 2907 Color: 0
Size: 184 Color: 0

Bin 153: 61 of cap free
Amount of items: 2
Items: 
Size: 12909 Color: 1
Size: 2326 Color: 0

Bin 154: 62 of cap free
Amount of items: 2
Items: 
Size: 13105 Color: 0
Size: 2129 Color: 1

Bin 155: 66 of cap free
Amount of items: 3
Items: 
Size: 8716 Color: 1
Size: 4328 Color: 1
Size: 2186 Color: 0

Bin 156: 70 of cap free
Amount of items: 2
Items: 
Size: 11532 Color: 1
Size: 3694 Color: 0

Bin 157: 70 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 1
Size: 1636 Color: 0

Bin 158: 72 of cap free
Amount of items: 2
Items: 
Size: 12584 Color: 0
Size: 2640 Color: 1

Bin 159: 77 of cap free
Amount of items: 2
Items: 
Size: 11386 Color: 1
Size: 3833 Color: 0

Bin 160: 83 of cap free
Amount of items: 2
Items: 
Size: 12184 Color: 1
Size: 3029 Color: 0

Bin 161: 86 of cap free
Amount of items: 2
Items: 
Size: 10879 Color: 1
Size: 4331 Color: 0

Bin 162: 96 of cap free
Amount of items: 2
Items: 
Size: 10672 Color: 1
Size: 4528 Color: 0

Bin 163: 102 of cap free
Amount of items: 2
Items: 
Size: 12276 Color: 0
Size: 2918 Color: 1

Bin 164: 106 of cap free
Amount of items: 33
Items: 
Size: 652 Color: 0
Size: 640 Color: 0
Size: 624 Color: 0
Size: 608 Color: 0
Size: 580 Color: 0
Size: 580 Color: 0
Size: 576 Color: 0
Size: 576 Color: 0
Size: 496 Color: 1
Size: 488 Color: 0
Size: 476 Color: 1
Size: 474 Color: 0
Size: 464 Color: 1
Size: 448 Color: 1
Size: 440 Color: 1
Size: 436 Color: 0
Size: 432 Color: 1
Size: 430 Color: 0
Size: 416 Color: 1
Size: 412 Color: 1
Size: 408 Color: 0
Size: 408 Color: 0
Size: 396 Color: 0
Size: 394 Color: 1
Size: 392 Color: 0
Size: 392 Color: 0
Size: 384 Color: 1
Size: 384 Color: 1
Size: 384 Color: 1
Size: 360 Color: 1
Size: 352 Color: 1
Size: 352 Color: 1
Size: 336 Color: 1

Bin 165: 106 of cap free
Amount of items: 2
Items: 
Size: 9628 Color: 1
Size: 5562 Color: 0

Bin 166: 108 of cap free
Amount of items: 2
Items: 
Size: 12578 Color: 1
Size: 2610 Color: 0

Bin 167: 122 of cap free
Amount of items: 2
Items: 
Size: 13478 Color: 0
Size: 1696 Color: 1

Bin 168: 123 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 0
Size: 1961 Color: 1

Bin 169: 127 of cap free
Amount of items: 2
Items: 
Size: 9329 Color: 1
Size: 5840 Color: 0

Bin 170: 127 of cap free
Amount of items: 2
Items: 
Size: 10152 Color: 0
Size: 5017 Color: 1

Bin 171: 132 of cap free
Amount of items: 6
Items: 
Size: 7688 Color: 1
Size: 1712 Color: 0
Size: 1468 Color: 0
Size: 1462 Color: 0
Size: 1422 Color: 1
Size: 1412 Color: 1

Bin 172: 132 of cap free
Amount of items: 2
Items: 
Size: 13174 Color: 1
Size: 1990 Color: 0

Bin 173: 133 of cap free
Amount of items: 2
Items: 
Size: 10824 Color: 0
Size: 4339 Color: 1

Bin 174: 142 of cap free
Amount of items: 2
Items: 
Size: 10858 Color: 1
Size: 4296 Color: 0

Bin 175: 146 of cap free
Amount of items: 2
Items: 
Size: 11388 Color: 0
Size: 3762 Color: 1

Bin 176: 154 of cap free
Amount of items: 13
Items: 
Size: 7652 Color: 1
Size: 736 Color: 0
Size: 736 Color: 0
Size: 728 Color: 0
Size: 668 Color: 0
Size: 666 Color: 0
Size: 652 Color: 0
Size: 580 Color: 1
Size: 576 Color: 1
Size: 576 Color: 1
Size: 532 Color: 1
Size: 520 Color: 1
Size: 520 Color: 1

Bin 177: 154 of cap free
Amount of items: 2
Items: 
Size: 10308 Color: 1
Size: 4834 Color: 0

Bin 178: 155 of cap free
Amount of items: 2
Items: 
Size: 12168 Color: 1
Size: 2973 Color: 0

Bin 179: 156 of cap free
Amount of items: 2
Items: 
Size: 11809 Color: 0
Size: 3331 Color: 1

Bin 180: 159 of cap free
Amount of items: 2
Items: 
Size: 8764 Color: 1
Size: 6373 Color: 0

Bin 181: 159 of cap free
Amount of items: 2
Items: 
Size: 12743 Color: 0
Size: 2394 Color: 1

Bin 182: 163 of cap free
Amount of items: 2
Items: 
Size: 11816 Color: 1
Size: 3317 Color: 0

Bin 183: 165 of cap free
Amount of items: 2
Items: 
Size: 12165 Color: 1
Size: 2966 Color: 0

Bin 184: 167 of cap free
Amount of items: 2
Items: 
Size: 11814 Color: 1
Size: 3315 Color: 0

Bin 185: 176 of cap free
Amount of items: 2
Items: 
Size: 9880 Color: 1
Size: 5240 Color: 0

Bin 186: 180 of cap free
Amount of items: 2
Items: 
Size: 12872 Color: 1
Size: 2244 Color: 0

Bin 187: 182 of cap free
Amount of items: 2
Items: 
Size: 8744 Color: 1
Size: 6370 Color: 0

Bin 188: 183 of cap free
Amount of items: 2
Items: 
Size: 8498 Color: 0
Size: 6615 Color: 1

Bin 189: 186 of cap free
Amount of items: 2
Items: 
Size: 11806 Color: 0
Size: 3304 Color: 1

Bin 190: 194 of cap free
Amount of items: 2
Items: 
Size: 11366 Color: 0
Size: 3736 Color: 1

Bin 191: 214 of cap free
Amount of items: 2
Items: 
Size: 9321 Color: 1
Size: 5761 Color: 0

Bin 192: 216 of cap free
Amount of items: 2
Items: 
Size: 9872 Color: 1
Size: 5208 Color: 0

Bin 193: 227 of cap free
Amount of items: 2
Items: 
Size: 10287 Color: 1
Size: 4782 Color: 0

Bin 194: 227 of cap free
Amount of items: 2
Items: 
Size: 11363 Color: 0
Size: 3706 Color: 1

Bin 195: 232 of cap free
Amount of items: 2
Items: 
Size: 9600 Color: 1
Size: 5464 Color: 0

Bin 196: 232 of cap free
Amount of items: 2
Items: 
Size: 11798 Color: 1
Size: 3266 Color: 0

Bin 197: 237 of cap free
Amount of items: 2
Items: 
Size: 11378 Color: 1
Size: 3681 Color: 0

Bin 198: 247 of cap free
Amount of items: 2
Items: 
Size: 11793 Color: 0
Size: 3256 Color: 1

Bin 199: 6612 of cap free
Amount of items: 28
Items: 
Size: 384 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 344 Color: 0
Size: 344 Color: 0
Size: 336 Color: 0
Size: 332 Color: 0
Size: 328 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 312 Color: 1
Size: 312 Color: 0
Size: 300 Color: 1
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0
Size: 288 Color: 0
Size: 272 Color: 1
Size: 272 Color: 0
Size: 272 Color: 0
Size: 268 Color: 1
Size: 256 Color: 1
Size: 256 Color: 1
Size: 256 Color: 1

Total size: 3028608
Total free space: 15296

