Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 2
Size: 290 Color: 4
Size: 267 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 275 Color: 0
Size: 260 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 268 Color: 2
Size: 250 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 4
Size: 267 Color: 1
Size: 267 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 276 Color: 4
Size: 256 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 321 Color: 0
Size: 427 Color: 2
Size: 253 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 2
Size: 327 Color: 3
Size: 266 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 277 Color: 0
Size: 265 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 2
Size: 303 Color: 2
Size: 259 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 3
Size: 265 Color: 1
Size: 255 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 4
Size: 329 Color: 1
Size: 256 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 2
Size: 279 Color: 2
Size: 263 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 1
Size: 251 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 2
Size: 304 Color: 0
Size: 275 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 317 Color: 3
Size: 313 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 0
Size: 298 Color: 3
Size: 270 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 339 Color: 2
Size: 256 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 301 Color: 3
Size: 293 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 4
Size: 339 Color: 4
Size: 304 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 4
Size: 298 Color: 3
Size: 281 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 327 Color: 4
Size: 291 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 286 Color: 3
Size: 266 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 291 Color: 0
Size: 259 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 370 Color: 2
Size: 255 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 290 Color: 3
Size: 279 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 275 Color: 2
Size: 266 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 4
Size: 333 Color: 1
Size: 329 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 309 Color: 0
Size: 306 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 298 Color: 2
Size: 284 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 0
Size: 294 Color: 1
Size: 261 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 3
Size: 337 Color: 0
Size: 300 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 277 Color: 4
Size: 268 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 305 Color: 4
Size: 284 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 2
Size: 331 Color: 3
Size: 257 Color: 0

Total size: 34034
Total free space: 0

