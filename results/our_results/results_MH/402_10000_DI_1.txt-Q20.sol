Capicity Bin: 8184
Lower Bound: 132

Bins used: 132
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 3032 Color: 18
Size: 2792 Color: 3
Size: 1016 Color: 12
Size: 504 Color: 8
Size: 456 Color: 7
Size: 384 Color: 6

Bin 2: 0 of cap free
Amount of items: 6
Items: 
Size: 5676 Color: 12
Size: 2092 Color: 14
Size: 136 Color: 18
Size: 112 Color: 1
Size: 112 Color: 0
Size: 56 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 15
Size: 1084 Color: 1
Size: 80 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 4
Size: 1212 Color: 4
Size: 240 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5079 Color: 5
Size: 2589 Color: 14
Size: 516 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6994 Color: 9
Size: 994 Color: 6
Size: 196 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6227 Color: 14
Size: 1631 Color: 1
Size: 326 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 4
Size: 826 Color: 8
Size: 164 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7332 Color: 18
Size: 716 Color: 6
Size: 136 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6581 Color: 19
Size: 1337 Color: 7
Size: 266 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 0
Size: 1588 Color: 18
Size: 312 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4986 Color: 6
Size: 2666 Color: 4
Size: 532 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6150 Color: 6
Size: 1698 Color: 18
Size: 336 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5828 Color: 15
Size: 1964 Color: 10
Size: 392 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6420 Color: 17
Size: 1476 Color: 4
Size: 288 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 5
Size: 1222 Color: 10
Size: 240 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7330 Color: 7
Size: 714 Color: 4
Size: 140 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7294 Color: 15
Size: 838 Color: 13
Size: 52 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7043 Color: 9
Size: 951 Color: 4
Size: 190 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4631 Color: 4
Size: 2961 Color: 1
Size: 592 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 10
Size: 1402 Color: 1
Size: 276 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 0
Size: 1388 Color: 3
Size: 272 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 13
Size: 2724 Color: 14
Size: 544 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 15
Size: 2460 Color: 18
Size: 488 Color: 5

Bin 25: 0 of cap free
Amount of items: 4
Items: 
Size: 5060 Color: 11
Size: 2604 Color: 17
Size: 416 Color: 0
Size: 104 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 11
Size: 1876 Color: 8
Size: 368 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 5
Size: 1147 Color: 5
Size: 228 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 4056 Color: 14
Size: 3592 Color: 11
Size: 536 Color: 10

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5080 Color: 7
Size: 2592 Color: 16
Size: 512 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 9
Size: 870 Color: 3
Size: 172 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 3
Size: 2324 Color: 17
Size: 464 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6203 Color: 2
Size: 1651 Color: 13
Size: 330 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 17
Size: 1099 Color: 7
Size: 218 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6188 Color: 1
Size: 1860 Color: 13
Size: 136 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6085 Color: 16
Size: 1751 Color: 17
Size: 348 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 9
Size: 762 Color: 17
Size: 152 Color: 5

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6099 Color: 2
Size: 1739 Color: 19
Size: 346 Color: 12

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6231 Color: 7
Size: 1629 Color: 5
Size: 324 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 6
Size: 950 Color: 6
Size: 188 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6926 Color: 12
Size: 1050 Color: 4
Size: 208 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7132 Color: 3
Size: 884 Color: 15
Size: 168 Color: 10

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4629 Color: 7
Size: 2963 Color: 9
Size: 592 Color: 8

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6818 Color: 12
Size: 1142 Color: 19
Size: 224 Color: 9

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 14
Size: 782 Color: 0
Size: 152 Color: 14

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 7047 Color: 19
Size: 949 Color: 17
Size: 188 Color: 10

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5802 Color: 15
Size: 1986 Color: 17
Size: 396 Color: 16

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 7
Size: 1150 Color: 8
Size: 212 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4095 Color: 15
Size: 3409 Color: 14
Size: 680 Color: 5

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5516 Color: 4
Size: 2228 Color: 16
Size: 440 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6058 Color: 7
Size: 1886 Color: 19
Size: 240 Color: 7

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4380 Color: 16
Size: 3172 Color: 3
Size: 632 Color: 6

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 4842 Color: 16
Size: 2786 Color: 4
Size: 556 Color: 17

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 4196 Color: 3
Size: 3324 Color: 13
Size: 664 Color: 15

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4093 Color: 15
Size: 3411 Color: 12
Size: 680 Color: 12

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 19
Size: 2274 Color: 6
Size: 452 Color: 14

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 2
Size: 830 Color: 16
Size: 80 Color: 6

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6219 Color: 8
Size: 1639 Color: 16
Size: 326 Color: 6

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6706 Color: 3
Size: 1234 Color: 17
Size: 244 Color: 7

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5449 Color: 4
Size: 2281 Color: 3
Size: 454 Color: 14

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 4630 Color: 7
Size: 2962 Color: 8
Size: 592 Color: 19

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6927 Color: 12
Size: 1049 Color: 17
Size: 208 Color: 11

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6632 Color: 9
Size: 1296 Color: 0
Size: 256 Color: 16

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6330 Color: 8
Size: 1546 Color: 13
Size: 308 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5482 Color: 14
Size: 2254 Color: 14
Size: 448 Color: 9

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 12
Size: 780 Color: 12
Size: 152 Color: 14

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6223 Color: 3
Size: 1635 Color: 14
Size: 326 Color: 14

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6076 Color: 13
Size: 2012 Color: 0
Size: 96 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 12
Size: 3194 Color: 11
Size: 340 Color: 12

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 9
Size: 902 Color: 2
Size: 180 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5766 Color: 18
Size: 2018 Color: 6
Size: 400 Color: 7

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 7015 Color: 3
Size: 975 Color: 13
Size: 194 Color: 12

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7019 Color: 17
Size: 971 Color: 9
Size: 194 Color: 9

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6427 Color: 14
Size: 1465 Color: 0
Size: 292 Color: 5

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 3
Size: 964 Color: 4
Size: 192 Color: 6

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5810 Color: 13
Size: 1982 Color: 9
Size: 392 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 14
Size: 1308 Color: 2
Size: 256 Color: 19

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5430 Color: 5
Size: 2298 Color: 4
Size: 456 Color: 9

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 3
Size: 1018 Color: 2
Size: 196 Color: 15

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 16
Size: 1084 Color: 2
Size: 208 Color: 17

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 7026 Color: 15
Size: 966 Color: 5
Size: 192 Color: 7

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6530 Color: 15
Size: 1554 Color: 9
Size: 100 Color: 16

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 4094 Color: 15
Size: 3410 Color: 6
Size: 680 Color: 6

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6875 Color: 12
Size: 1183 Color: 6
Size: 126 Color: 13

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 13
Size: 2444 Color: 8
Size: 480 Color: 12

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 13
Size: 842 Color: 12
Size: 168 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6852 Color: 19
Size: 1116 Color: 13
Size: 216 Color: 15

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 19
Size: 862 Color: 5
Size: 172 Color: 15

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6134 Color: 19
Size: 1710 Color: 18
Size: 340 Color: 5

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4100 Color: 7
Size: 3404 Color: 7
Size: 680 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5772 Color: 13
Size: 2012 Color: 17
Size: 400 Color: 18

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 4836 Color: 13
Size: 3036 Color: 6
Size: 312 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 0
Size: 844 Color: 5
Size: 168 Color: 15

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 4098 Color: 15
Size: 3406 Color: 4
Size: 680 Color: 11

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 4634 Color: 2
Size: 2962 Color: 13
Size: 588 Color: 5

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 11
Size: 1716 Color: 11
Size: 336 Color: 16

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6919 Color: 3
Size: 1055 Color: 10
Size: 210 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 7212 Color: 14
Size: 812 Color: 7
Size: 160 Color: 12

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 7
Size: 1620 Color: 19
Size: 320 Color: 15

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5884 Color: 3
Size: 1924 Color: 11
Size: 376 Color: 6

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 6494 Color: 15
Size: 1410 Color: 10
Size: 280 Color: 4

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6115 Color: 15
Size: 1837 Color: 3
Size: 232 Color: 2

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5786 Color: 3
Size: 2002 Color: 19
Size: 396 Color: 8

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 4738 Color: 7
Size: 2874 Color: 17
Size: 572 Color: 15

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 1
Size: 1204 Color: 10
Size: 232 Color: 15

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 9
Size: 1386 Color: 6
Size: 276 Color: 12

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 18
Size: 2582 Color: 7
Size: 512 Color: 16

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 5465 Color: 10
Size: 2267 Color: 19
Size: 452 Color: 16

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6577 Color: 12
Size: 1341 Color: 17
Size: 266 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6709 Color: 16
Size: 1231 Color: 19
Size: 244 Color: 5

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 5759 Color: 9
Size: 2201 Color: 10
Size: 224 Color: 6

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 5
Size: 924 Color: 15
Size: 184 Color: 18

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 2
Size: 1314 Color: 19
Size: 260 Color: 19

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 4580 Color: 12
Size: 3004 Color: 4
Size: 600 Color: 2

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 4930 Color: 0
Size: 2714 Color: 2
Size: 540 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 6991 Color: 11
Size: 995 Color: 17
Size: 198 Color: 15

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 12
Size: 1189 Color: 18
Size: 236 Color: 5

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 12
Size: 2660 Color: 16
Size: 528 Color: 10

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6346 Color: 4
Size: 1534 Color: 18
Size: 304 Color: 11

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 5773 Color: 4
Size: 2011 Color: 16
Size: 400 Color: 7

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6544 Color: 16
Size: 1120 Color: 3
Size: 520 Color: 3

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 5077 Color: 17
Size: 2591 Color: 8
Size: 516 Color: 6

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5839 Color: 10
Size: 1955 Color: 17
Size: 390 Color: 12

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 7
Size: 1353 Color: 13
Size: 270 Color: 4

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6589 Color: 10
Size: 1331 Color: 1
Size: 264 Color: 5

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6585 Color: 6
Size: 1333 Color: 4
Size: 266 Color: 16

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 15
Size: 1070 Color: 11
Size: 212 Color: 13

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 5855 Color: 3
Size: 1941 Color: 13
Size: 388 Color: 14

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 7111 Color: 11
Size: 895 Color: 12
Size: 178 Color: 11

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 7091 Color: 15
Size: 911 Color: 9
Size: 182 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 7115 Color: 18
Size: 891 Color: 9
Size: 178 Color: 18

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 10
Size: 1479 Color: 6
Size: 294 Color: 15

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6871 Color: 8
Size: 1095 Color: 18
Size: 218 Color: 18

Total size: 1080288
Total free space: 0

