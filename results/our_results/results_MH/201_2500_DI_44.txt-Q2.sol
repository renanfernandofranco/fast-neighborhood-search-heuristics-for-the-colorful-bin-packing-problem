Capicity Bin: 2036
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 684 Color: 1
Size: 424 Color: 0
Size: 316 Color: 1
Size: 228 Color: 1
Size: 228 Color: 0
Size: 64 Color: 0
Size: 48 Color: 0
Size: 40 Color: 1
Size: 4 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1447 Color: 1
Size: 491 Color: 1
Size: 98 Color: 0

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1404 Color: 0
Size: 472 Color: 1
Size: 76 Color: 0
Size: 64 Color: 1
Size: 20 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 0
Size: 220 Color: 1
Size: 40 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 0
Size: 847 Color: 0
Size: 168 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 1
Size: 382 Color: 1
Size: 76 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 1
Size: 701 Color: 0
Size: 138 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1022 Color: 0
Size: 846 Color: 0
Size: 168 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 1
Size: 581 Color: 0
Size: 116 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 0
Size: 294 Color: 0
Size: 56 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 1
Size: 201 Color: 1
Size: 38 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 0
Size: 173 Color: 0
Size: 34 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 0
Size: 194 Color: 1
Size: 36 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 1
Size: 402 Color: 0
Size: 76 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 1
Size: 213 Color: 1
Size: 42 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 0
Size: 499 Color: 1
Size: 98 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1019 Color: 1
Size: 849 Color: 1
Size: 168 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 1
Size: 229 Color: 0
Size: 44 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 0
Size: 361 Color: 0
Size: 72 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 0
Size: 395 Color: 1
Size: 78 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 0
Size: 391 Color: 1
Size: 78 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 0
Size: 721 Color: 1
Size: 144 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1498 Color: 1
Size: 450 Color: 1
Size: 88 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 0
Size: 278 Color: 1
Size: 52 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 1
Size: 214 Color: 0
Size: 40 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 0
Size: 193 Color: 0
Size: 38 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 0
Size: 315 Color: 0
Size: 62 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1226 Color: 1
Size: 678 Color: 0
Size: 132 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 1
Size: 334 Color: 0
Size: 28 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 1
Size: 323 Color: 1
Size: 64 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 1
Size: 257 Color: 1
Size: 50 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 0
Size: 218 Color: 0
Size: 40 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 0
Size: 481 Color: 1
Size: 96 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 1
Size: 590 Color: 1
Size: 116 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 0
Size: 243 Color: 0
Size: 48 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 1
Size: 295 Color: 1
Size: 58 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1327 Color: 1
Size: 591 Color: 1
Size: 118 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 1
Size: 482 Color: 1
Size: 96 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 0
Size: 311 Color: 0
Size: 46 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 1
Size: 178 Color: 1
Size: 32 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 1
Size: 289 Color: 1
Size: 48 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1443 Color: 1
Size: 495 Color: 0
Size: 98 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1752 Color: 0
Size: 244 Color: 1
Size: 40 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1429 Color: 0
Size: 507 Color: 0
Size: 100 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1177 Color: 1
Size: 717 Color: 0
Size: 142 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1134 Color: 0
Size: 754 Color: 1
Size: 148 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 0
Size: 407 Color: 0
Size: 80 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 0
Size: 522 Color: 1
Size: 100 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 1
Size: 713 Color: 0
Size: 142 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1193 Color: 0
Size: 713 Color: 1
Size: 130 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 0
Size: 191 Color: 1
Size: 36 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 0
Size: 217 Color: 0
Size: 42 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1302 Color: 1
Size: 650 Color: 0
Size: 84 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 1
Size: 311 Color: 0
Size: 62 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 0
Size: 606 Color: 0
Size: 40 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 1
Size: 493 Color: 1
Size: 4 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 0
Size: 691 Color: 1
Size: 136 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1173 Color: 0
Size: 721 Color: 0
Size: 142 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 0
Size: 366 Color: 1
Size: 72 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 0
Size: 417 Color: 0
Size: 48 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 1
Size: 399 Color: 0
Size: 78 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 1
Size: 334 Color: 1
Size: 64 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 1
Size: 242 Color: 1
Size: 48 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 1
Size: 197 Color: 0
Size: 38 Color: 0

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 1280 Color: 1
Size: 756 Color: 0

Total size: 132340
Total free space: 0

