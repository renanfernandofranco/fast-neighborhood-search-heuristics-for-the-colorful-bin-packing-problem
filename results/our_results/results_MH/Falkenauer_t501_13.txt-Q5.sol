Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 3
Size: 269 Color: 0
Size: 264 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 257 Color: 0
Size: 251 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 306 Color: 4
Size: 284 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 297 Color: 4
Size: 260 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 3
Size: 292 Color: 1
Size: 289 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 324 Color: 2
Size: 257 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 326 Color: 2
Size: 268 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 2
Size: 263 Color: 1
Size: 254 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 284 Color: 3
Size: 260 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 3
Size: 324 Color: 1
Size: 271 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 311 Color: 4
Size: 278 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 295 Color: 1
Size: 271 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 0
Size: 291 Color: 2
Size: 270 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 323 Color: 1
Size: 251 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 340 Color: 0
Size: 278 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 4
Size: 306 Color: 0
Size: 273 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 0
Size: 251 Color: 0
Size: 250 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 302 Color: 3
Size: 252 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 299 Color: 2
Size: 296 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 373 Color: 4
Size: 252 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 335 Color: 1
Size: 295 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 363 Color: 3
Size: 254 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 3
Size: 355 Color: 2
Size: 252 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 353 Color: 0
Size: 286 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 2
Size: 331 Color: 2
Size: 293 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 2
Size: 264 Color: 1
Size: 254 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 355 Color: 2
Size: 270 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 0
Size: 333 Color: 2
Size: 280 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 313 Color: 2
Size: 288 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 3
Size: 250 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 3
Size: 336 Color: 4
Size: 302 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 285 Color: 0
Size: 279 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 353 Color: 0
Size: 281 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 3
Size: 294 Color: 0
Size: 289 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 351 Color: 4
Size: 284 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 0
Size: 277 Color: 2
Size: 254 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 353 Color: 1
Size: 270 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 3
Size: 351 Color: 0
Size: 261 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 267 Color: 3
Size: 256 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 3
Size: 330 Color: 0
Size: 254 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 271 Color: 0
Size: 264 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 313 Color: 4
Size: 269 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 0
Size: 265 Color: 4
Size: 262 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 3
Size: 283 Color: 0
Size: 255 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 2
Size: 303 Color: 4
Size: 260 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 4
Size: 284 Color: 1
Size: 258 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 3
Size: 278 Color: 1
Size: 256 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 4
Size: 364 Color: 2
Size: 269 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 350 Color: 0
Size: 272 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 303 Color: 0
Size: 263 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 2
Size: 295 Color: 2
Size: 256 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 301 Color: 3
Size: 252 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 320 Color: 1
Size: 261 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 2
Size: 269 Color: 1
Size: 251 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 289 Color: 2
Size: 283 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 371 Color: 2
Size: 257 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 356 Color: 2
Size: 260 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 322 Color: 3
Size: 301 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 2
Size: 309 Color: 4
Size: 261 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 287 Color: 4
Size: 257 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 3
Size: 314 Color: 4
Size: 273 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 2
Size: 358 Color: 4
Size: 272 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 254 Color: 3
Size: 251 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 4
Size: 259 Color: 4
Size: 252 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 299 Color: 1
Size: 261 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 356 Color: 1
Size: 278 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 2
Size: 283 Color: 0
Size: 266 Color: 4

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 4
Size: 266 Color: 2
Size: 261 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 355 Color: 3
Size: 278 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 4
Size: 346 Color: 2
Size: 291 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 354 Color: 0
Size: 270 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 0
Size: 256 Color: 1
Size: 254 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 3
Size: 356 Color: 0
Size: 282 Color: 3

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 3
Size: 253 Color: 2
Size: 252 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 0
Size: 351 Color: 4
Size: 262 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 3
Size: 307 Color: 1
Size: 282 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 3
Size: 257 Color: 1
Size: 250 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 288 Color: 2
Size: 265 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 272 Color: 3
Size: 263 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 279 Color: 3
Size: 266 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 350 Color: 0
Size: 265 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 274 Color: 3
Size: 250 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 261 Color: 4
Size: 258 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 365 Color: 0
Size: 255 Color: 3

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 2
Size: 259 Color: 4
Size: 250 Color: 4

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 2
Size: 325 Color: 0
Size: 286 Color: 4

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 313 Color: 1
Size: 291 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 356 Color: 2
Size: 281 Color: 4

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 4
Size: 268 Color: 2
Size: 267 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 4
Size: 255 Color: 1
Size: 254 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 3
Size: 274 Color: 4
Size: 269 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 289 Color: 0
Size: 252 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 354 Color: 0
Size: 263 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 2
Size: 340 Color: 1
Size: 291 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 316 Color: 1
Size: 284 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 2
Size: 337 Color: 3
Size: 250 Color: 4

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 299 Color: 2
Size: 273 Color: 4

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 299 Color: 4
Size: 297 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 278 Color: 0
Size: 268 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 361 Color: 1
Size: 273 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 357 Color: 2
Size: 278 Color: 4

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 4
Size: 255 Color: 3
Size: 253 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 255 Color: 2
Size: 252 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 4
Size: 255 Color: 2
Size: 250 Color: 4

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 336 Color: 4
Size: 299 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 313 Color: 4
Size: 300 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 4
Size: 287 Color: 2
Size: 259 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 329 Color: 3
Size: 258 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 335 Color: 4
Size: 251 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 314 Color: 2
Size: 258 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 312 Color: 3
Size: 261 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 285 Color: 0
Size: 269 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 273 Color: 4
Size: 264 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 310 Color: 3
Size: 293 Color: 3

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 3
Size: 333 Color: 4
Size: 310 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 265 Color: 4
Size: 253 Color: 2

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 4
Size: 266 Color: 0
Size: 263 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 359 Color: 0
Size: 250 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 4
Size: 285 Color: 3
Size: 265 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 280 Color: 1
Size: 258 Color: 2

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 352 Color: 2
Size: 279 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 262 Color: 1
Size: 258 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 349 Color: 3
Size: 272 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 276 Color: 3
Size: 260 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 389 Color: 4
Size: 256 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 325 Color: 0
Size: 275 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 3
Size: 326 Color: 2
Size: 272 Color: 2

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 2
Size: 350 Color: 2
Size: 286 Color: 4

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 281 Color: 4
Size: 272 Color: 2

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 2
Size: 367 Color: 0
Size: 261 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 263 Color: 2
Size: 252 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 322 Color: 4
Size: 302 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 276 Color: 4
Size: 259 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 0
Size: 329 Color: 4
Size: 303 Color: 2

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 2
Size: 305 Color: 3
Size: 250 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 315 Color: 1
Size: 251 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 4
Size: 285 Color: 4
Size: 284 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 0
Size: 313 Color: 1
Size: 260 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 354 Color: 0
Size: 254 Color: 2

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 357 Color: 0
Size: 276 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 316 Color: 3
Size: 252 Color: 3

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 335 Color: 1
Size: 262 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 275 Color: 1
Size: 262 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 256 Color: 0
Size: 253 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 358 Color: 0
Size: 272 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 330 Color: 3
Size: 293 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 4
Size: 317 Color: 1
Size: 316 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 286 Color: 0
Size: 283 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 332 Color: 1
Size: 289 Color: 3

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 341 Color: 4
Size: 292 Color: 4

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 369 Color: 2
Size: 257 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 372 Color: 4
Size: 252 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 3
Size: 328 Color: 4
Size: 283 Color: 2

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 3
Size: 311 Color: 2
Size: 278 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 311 Color: 4
Size: 277 Color: 4

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 2
Size: 324 Color: 4
Size: 253 Color: 3

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 308 Color: 2
Size: 263 Color: 4

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 310 Color: 4
Size: 307 Color: 3

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 315 Color: 4
Size: 271 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 288 Color: 3
Size: 273 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 4
Size: 303 Color: 3
Size: 256 Color: 2

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 296 Color: 1
Size: 262 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 299 Color: 4
Size: 294 Color: 2

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 3
Size: 278 Color: 2
Size: 254 Color: 3

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 0
Size: 276 Color: 1
Size: 255 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 3
Size: 268 Color: 2
Size: 258 Color: 3

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 3
Size: 262 Color: 3
Size: 256 Color: 2

Total size: 167000
Total free space: 0

