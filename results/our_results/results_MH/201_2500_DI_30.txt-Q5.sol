Capicity Bin: 2032
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1408 Color: 3
Size: 420 Color: 2
Size: 84 Color: 0
Size: 52 Color: 1
Size: 40 Color: 4
Size: 20 Color: 4
Size: 8 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 2
Size: 322 Color: 2
Size: 64 Color: 4

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1272 Color: 2
Size: 468 Color: 3
Size: 220 Color: 1
Size: 72 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 1
Size: 202 Color: 2
Size: 36 Color: 2

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1521 Color: 2
Size: 427 Color: 1
Size: 76 Color: 2
Size: 8 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 1
Size: 374 Color: 4
Size: 72 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 0
Size: 266 Color: 3
Size: 52 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1142 Color: 3
Size: 742 Color: 0
Size: 148 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 2
Size: 257 Color: 2
Size: 50 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1018 Color: 1
Size: 886 Color: 2
Size: 128 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1695 Color: 2
Size: 281 Color: 1
Size: 56 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 4
Size: 174 Color: 0
Size: 32 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 1
Size: 282 Color: 2
Size: 52 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 1
Size: 210 Color: 2
Size: 40 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1277 Color: 1
Size: 631 Color: 1
Size: 124 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 0
Size: 309 Color: 3
Size: 60 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 2
Size: 202 Color: 1
Size: 40 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 0
Size: 373 Color: 3
Size: 74 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 0
Size: 277 Color: 2
Size: 54 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 3
Size: 382 Color: 1
Size: 72 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1186 Color: 1
Size: 710 Color: 3
Size: 136 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 2
Size: 212 Color: 3
Size: 40 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 2
Size: 410 Color: 3
Size: 80 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1157 Color: 4
Size: 731 Color: 2
Size: 144 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 4
Size: 231 Color: 3
Size: 44 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 2
Size: 219 Color: 3
Size: 42 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 0
Size: 425 Color: 2
Size: 84 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1273 Color: 1
Size: 633 Color: 4
Size: 126 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 3
Size: 233 Color: 1
Size: 46 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1258 Color: 3
Size: 646 Color: 0
Size: 128 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 4
Size: 377 Color: 4
Size: 74 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1159 Color: 1
Size: 729 Color: 1
Size: 144 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 2
Size: 303 Color: 3
Size: 60 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 2
Size: 342 Color: 3
Size: 64 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 1
Size: 218 Color: 3
Size: 40 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1779 Color: 4
Size: 211 Color: 2
Size: 42 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 3
Size: 182 Color: 2
Size: 32 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 0
Size: 203 Color: 2
Size: 22 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1017 Color: 1
Size: 847 Color: 0
Size: 168 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 3
Size: 367 Color: 4
Size: 72 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 2
Size: 274 Color: 1
Size: 52 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 2
Size: 205 Color: 0
Size: 40 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 1
Size: 426 Color: 2
Size: 84 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1102 Color: 1
Size: 858 Color: 2
Size: 72 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1631 Color: 2
Size: 335 Color: 1
Size: 66 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1342 Color: 0
Size: 578 Color: 1
Size: 112 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1042 Color: 4
Size: 826 Color: 0
Size: 164 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1474 Color: 2
Size: 466 Color: 3
Size: 92 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 1
Size: 570 Color: 2
Size: 112 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1019 Color: 2
Size: 845 Color: 0
Size: 168 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 2
Size: 306 Color: 0
Size: 60 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 2
Size: 186 Color: 2
Size: 36 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1455 Color: 3
Size: 481 Color: 1
Size: 96 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 1
Size: 262 Color: 2
Size: 48 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 0
Size: 251 Color: 2
Size: 48 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1705 Color: 0
Size: 289 Color: 4
Size: 38 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 0
Size: 246 Color: 2
Size: 48 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 2
Size: 381 Color: 2
Size: 74 Color: 3

Bin 59: 0 of cap free
Amount of items: 4
Items: 
Size: 824 Color: 4
Size: 692 Color: 1
Size: 312 Color: 2
Size: 204 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 2
Size: 194 Color: 4
Size: 36 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1371 Color: 2
Size: 551 Color: 2
Size: 110 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 4
Size: 549 Color: 0
Size: 108 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 3
Size: 514 Color: 1
Size: 100 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1453 Color: 2
Size: 483 Color: 2
Size: 96 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 1
Size: 191 Color: 3
Size: 38 Color: 3

Total size: 132080
Total free space: 0

