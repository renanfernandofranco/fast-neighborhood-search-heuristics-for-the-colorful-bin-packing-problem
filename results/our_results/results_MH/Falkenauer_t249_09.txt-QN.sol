Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 181
Size: 319 Color: 137
Size: 302 Color: 119

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 236
Size: 279 Color: 84
Size: 251 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 170
Size: 364 Color: 167
Size: 270 Color: 63

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 189
Size: 349 Color: 156
Size: 257 Color: 26

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 227
Size: 286 Color: 93
Size: 255 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 190
Size: 335 Color: 147
Size: 269 Color: 60

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 230
Size: 283 Color: 91
Size: 257 Color: 23

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 246
Size: 257 Color: 25
Size: 252 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 175
Size: 355 Color: 160
Size: 273 Color: 72

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 220
Size: 279 Color: 83
Size: 275 Color: 78

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 168
Size: 332 Color: 145
Size: 303 Color: 122

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 223
Size: 287 Color: 95
Size: 258 Color: 28

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 159
Size: 352 Color: 158
Size: 295 Color: 110

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 217
Size: 299 Color: 116
Size: 261 Color: 37

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 165
Size: 350 Color: 157
Size: 288 Color: 96

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 243
Size: 268 Color: 56
Size: 250 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 229
Size: 289 Color: 100
Size: 251 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 240
Size: 268 Color: 57
Size: 255 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 205
Size: 315 Color: 134
Size: 259 Color: 30

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 193
Size: 329 Color: 144
Size: 271 Color: 66

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 176
Size: 333 Color: 146
Size: 292 Color: 106

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 200
Size: 303 Color: 123
Size: 280 Color: 86

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 206
Size: 318 Color: 136
Size: 255 Color: 19

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 228
Size: 280 Color: 85
Size: 260 Color: 35

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 239
Size: 263 Color: 43
Size: 261 Color: 38

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 179
Size: 361 Color: 164
Size: 262 Color: 41

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 162
Size: 343 Color: 154
Size: 301 Color: 117

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 213
Size: 305 Color: 124
Size: 257 Color: 24

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 185
Size: 324 Color: 139
Size: 292 Color: 105

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 211
Size: 312 Color: 132
Size: 252 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 184
Size: 327 Color: 141
Size: 291 Color: 103

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 245
Size: 256 Color: 22
Size: 256 Color: 20

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 187
Size: 337 Color: 148
Size: 275 Color: 80

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 178
Size: 356 Color: 161
Size: 267 Color: 54

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 214
Size: 289 Color: 98
Size: 273 Color: 70

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 198
Size: 294 Color: 108
Size: 291 Color: 102

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 196
Size: 298 Color: 115
Size: 291 Color: 104

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 234
Size: 266 Color: 50
Size: 264 Color: 46

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 222
Size: 297 Color: 113
Size: 252 Color: 8

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 186
Size: 339 Color: 150
Size: 275 Color: 79

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 235
Size: 271 Color: 67
Size: 259 Color: 32

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 208
Size: 295 Color: 109
Size: 271 Color: 68

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 244
Size: 263 Color: 45
Size: 250 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 224
Size: 285 Color: 92
Size: 258 Color: 29

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 216
Size: 302 Color: 120
Size: 258 Color: 27

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 248
Size: 255 Color: 18
Size: 251 Color: 6

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 195
Size: 307 Color: 127
Size: 282 Color: 90

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 219
Size: 302 Color: 118
Size: 252 Color: 10

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 166
Size: 340 Color: 152
Size: 296 Color: 112

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 204
Size: 323 Color: 138
Size: 252 Color: 12

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 237
Size: 267 Color: 52
Size: 262 Color: 39

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 218
Size: 287 Color: 94
Size: 269 Color: 58

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 226
Size: 290 Color: 101
Size: 252 Color: 11

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 238
Size: 264 Color: 47
Size: 262 Color: 40

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 197
Size: 317 Color: 135
Size: 269 Color: 61

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 241
Size: 262 Color: 42
Size: 260 Color: 33

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 242
Size: 260 Color: 36
Size: 260 Color: 34

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 192
Size: 305 Color: 125
Size: 297 Color: 114

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 203
Size: 312 Color: 130
Size: 264 Color: 49

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 182
Size: 340 Color: 151
Size: 280 Color: 87

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 202
Size: 326 Color: 140
Size: 250 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 270 Color: 62
Size: 264 Color: 48
Size: 466 Color: 232

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 233
Size: 275 Color: 81
Size: 256 Color: 21

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 247
Size: 259 Color: 31
Size: 250 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 212
Size: 295 Color: 111
Size: 268 Color: 55

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 171
Size: 365 Color: 169
Size: 266 Color: 51

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 209
Size: 312 Color: 131
Size: 253 Color: 13

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 201
Size: 314 Color: 133
Size: 267 Color: 53

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 172
Size: 357 Color: 163
Size: 274 Color: 74

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 174
Size: 341 Color: 153
Size: 289 Color: 99

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 207
Size: 303 Color: 121
Size: 270 Color: 65

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 199
Size: 308 Color: 128
Size: 275 Color: 82

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 191
Size: 329 Color: 143
Size: 274 Color: 75

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 210
Size: 309 Color: 129
Size: 255 Color: 14

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 225
Size: 273 Color: 71
Size: 269 Color: 59

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 183
Size: 345 Color: 155
Size: 274 Color: 73

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 180
Size: 328 Color: 142
Size: 294 Color: 107

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 231
Size: 274 Color: 76
Size: 263 Color: 44

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 194
Size: 307 Color: 126
Size: 282 Color: 89

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 188
Size: 338 Color: 149
Size: 274 Color: 77

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 215
Size: 289 Color: 97
Size: 273 Color: 69

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 177
Size: 369 Color: 173
Size: 255 Color: 16

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 221
Size: 281 Color: 88
Size: 270 Color: 64

Total size: 83000
Total free space: 0

