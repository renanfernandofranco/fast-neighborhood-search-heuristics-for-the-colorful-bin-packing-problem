Capicity Bin: 9824
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4996 Color: 282
Size: 4084 Color: 265
Size: 296 Color: 59
Size: 224 Color: 41
Size: 224 Color: 40

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6728 Color: 311
Size: 3020 Color: 240
Size: 76 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6836 Color: 314
Size: 1652 Color: 193
Size: 1336 Color: 175

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6904 Color: 318
Size: 2584 Color: 231
Size: 336 Color: 68

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 323
Size: 2118 Color: 213
Size: 702 Color: 121

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7184 Color: 325
Size: 2440 Color: 224
Size: 200 Color: 32

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7284 Color: 327
Size: 2124 Color: 214
Size: 416 Color: 80

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 328
Size: 1496 Color: 185
Size: 1042 Color: 151

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7288 Color: 329
Size: 2356 Color: 219
Size: 180 Color: 25

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7512 Color: 334
Size: 1928 Color: 204
Size: 384 Color: 79

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7556 Color: 336
Size: 2048 Color: 208
Size: 220 Color: 38

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7610 Color: 339
Size: 1702 Color: 196
Size: 512 Color: 99

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7624 Color: 340
Size: 1908 Color: 203
Size: 292 Color: 58

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7664 Color: 341
Size: 1848 Color: 200
Size: 312 Color: 63

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7844 Color: 347
Size: 1180 Color: 166
Size: 800 Color: 128

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7856 Color: 348
Size: 1000 Color: 149
Size: 968 Color: 147

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7864 Color: 349
Size: 1116 Color: 160
Size: 844 Color: 138

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7934 Color: 351
Size: 1470 Color: 183
Size: 420 Color: 85

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 8062 Color: 355
Size: 1160 Color: 163
Size: 602 Color: 109

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8080 Color: 356
Size: 928 Color: 145
Size: 816 Color: 132

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8088 Color: 357
Size: 880 Color: 141
Size: 856 Color: 139

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8092 Color: 358
Size: 1268 Color: 169
Size: 464 Color: 87

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 8208 Color: 361
Size: 808 Color: 130
Size: 808 Color: 129

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 362
Size: 1360 Color: 177
Size: 248 Color: 46

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 8298 Color: 366
Size: 918 Color: 144
Size: 608 Color: 111

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8426 Color: 374
Size: 822 Color: 136
Size: 576 Color: 104

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8432 Color: 375
Size: 816 Color: 133
Size: 576 Color: 105

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8434 Color: 376
Size: 1046 Color: 152
Size: 344 Color: 70

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 8578 Color: 383
Size: 878 Color: 140
Size: 368 Color: 75

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8584 Color: 384
Size: 816 Color: 134
Size: 424 Color: 86

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 8596 Color: 385
Size: 1092 Color: 156
Size: 136 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 8620 Color: 387
Size: 828 Color: 137
Size: 376 Color: 77

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 8632 Color: 388
Size: 904 Color: 143
Size: 288 Color: 55

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 8720 Color: 392
Size: 688 Color: 119
Size: 416 Color: 81

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 8744 Color: 394
Size: 592 Color: 106
Size: 488 Color: 94

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 8756 Color: 395
Size: 892 Color: 142
Size: 176 Color: 24

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 8768 Color: 396
Size: 640 Color: 115
Size: 416 Color: 83

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 8784 Color: 398
Size: 712 Color: 124
Size: 328 Color: 67

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 8812 Color: 400
Size: 672 Color: 117
Size: 340 Color: 69

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 8836 Color: 401
Size: 700 Color: 120
Size: 288 Color: 56

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 8838 Color: 402
Size: 818 Color: 135
Size: 168 Color: 20

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 6878 Color: 317
Size: 2396 Color: 222
Size: 548 Color: 102

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 7550 Color: 335
Size: 2272 Color: 218

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 7924 Color: 350
Size: 1898 Color: 202

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 8026 Color: 353
Size: 1648 Color: 192
Size: 148 Color: 13

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 8134 Color: 359
Size: 1688 Color: 195

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 8376 Color: 371
Size: 1446 Color: 181

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 8386 Color: 372
Size: 1084 Color: 154
Size: 352 Color: 74

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 8412 Color: 373
Size: 1410 Color: 179

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 8500 Color: 380
Size: 1322 Color: 173

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 8532 Color: 382
Size: 1290 Color: 170

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 8610 Color: 386
Size: 1212 Color: 168

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 8660 Color: 389
Size: 1162 Color: 164

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 8714 Color: 391
Size: 1108 Color: 158

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 8726 Color: 393
Size: 1096 Color: 157

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 8774 Color: 397
Size: 1048 Color: 153

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 8808 Color: 399
Size: 1014 Color: 150

Bin 58: 3 of cap free
Amount of items: 5
Items: 
Size: 6209 Color: 302
Size: 3000 Color: 238
Size: 264 Color: 51
Size: 176 Color: 22
Size: 172 Color: 21

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 6776 Color: 312
Size: 3045 Color: 244

Bin 60: 4 of cap free
Amount of items: 5
Items: 
Size: 4948 Color: 280
Size: 4072 Color: 263
Size: 304 Color: 60
Size: 256 Color: 47
Size: 240 Color: 44

Bin 61: 4 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 315
Size: 1892 Color: 201
Size: 1088 Color: 155

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 7604 Color: 338
Size: 2216 Color: 216

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 7716 Color: 343
Size: 2104 Color: 212

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 8180 Color: 360
Size: 1640 Color: 191

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 8232 Color: 364
Size: 1588 Color: 190

Bin 66: 4 of cap free
Amount of items: 3
Items: 
Size: 8268 Color: 365
Size: 1536 Color: 188
Size: 16 Color: 0

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 8490 Color: 378
Size: 1330 Color: 174

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 8520 Color: 381
Size: 1300 Color: 171

Bin 69: 6 of cap free
Amount of items: 5
Items: 
Size: 4918 Color: 276
Size: 2084 Color: 210
Size: 2000 Color: 205
Size: 544 Color: 101
Size: 272 Color: 52

Bin 70: 6 of cap free
Amount of items: 4
Items: 
Size: 6954 Color: 320
Size: 2724 Color: 232
Size: 76 Color: 7
Size: 64 Color: 6

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 7056 Color: 324
Size: 2762 Color: 235

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 7266 Color: 326
Size: 2552 Color: 230

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 7594 Color: 337
Size: 2224 Color: 217

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 8340 Color: 368
Size: 1478 Color: 184

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 8362 Color: 369
Size: 1456 Color: 182

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 6784 Color: 313
Size: 3033 Color: 243

Bin 77: 8 of cap free
Amount of items: 3
Items: 
Size: 6296 Color: 305
Size: 3360 Color: 249
Size: 160 Color: 16

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 6864 Color: 316
Size: 2952 Color: 237

Bin 79: 8 of cap free
Amount of items: 3
Items: 
Size: 7304 Color: 330
Size: 2480 Color: 226
Size: 32 Color: 2

Bin 80: 8 of cap free
Amount of items: 2
Items: 
Size: 7324 Color: 332
Size: 2492 Color: 228

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 8372 Color: 370
Size: 1444 Color: 180

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 8680 Color: 390
Size: 1136 Color: 162

Bin 83: 10 of cap free
Amount of items: 2
Items: 
Size: 7440 Color: 333
Size: 2374 Color: 220

Bin 84: 12 of cap free
Amount of items: 5
Items: 
Size: 4944 Color: 279
Size: 4068 Color: 262
Size: 288 Color: 57
Size: 256 Color: 49
Size: 256 Color: 48

Bin 85: 12 of cap free
Amount of items: 5
Items: 
Size: 4952 Color: 281
Size: 4080 Color: 264
Size: 320 Color: 66
Size: 232 Color: 43
Size: 228 Color: 42

Bin 86: 12 of cap free
Amount of items: 5
Items: 
Size: 5644 Color: 290
Size: 3496 Color: 252
Size: 256 Color: 50
Size: 208 Color: 36
Size: 208 Color: 35

Bin 87: 12 of cap free
Amount of items: 3
Items: 
Size: 5704 Color: 292
Size: 3904 Color: 260
Size: 204 Color: 33

Bin 88: 12 of cap free
Amount of items: 3
Items: 
Size: 5956 Color: 294
Size: 3664 Color: 258
Size: 192 Color: 29

Bin 89: 12 of cap free
Amount of items: 3
Items: 
Size: 6144 Color: 297
Size: 3476 Color: 251
Size: 192 Color: 26

Bin 90: 12 of cap free
Amount of items: 2
Items: 
Size: 8440 Color: 377
Size: 1372 Color: 178

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 8496 Color: 379
Size: 1316 Color: 172

Bin 92: 14 of cap free
Amount of items: 4
Items: 
Size: 6956 Color: 321
Size: 2726 Color: 233
Size: 64 Color: 5
Size: 64 Color: 4

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 8308 Color: 367
Size: 1502 Color: 186

Bin 94: 15 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 299
Size: 3448 Color: 250
Size: 176 Color: 23

Bin 95: 15 of cap free
Amount of items: 5
Items: 
Size: 6224 Color: 303
Size: 3013 Color: 239
Size: 240 Color: 45
Size: 168 Color: 19
Size: 164 Color: 18

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 7782 Color: 345
Size: 2026 Color: 207

Bin 97: 16 of cap free
Amount of items: 2
Items: 
Size: 8230 Color: 363
Size: 1578 Color: 189

Bin 98: 18 of cap free
Amount of items: 2
Items: 
Size: 5546 Color: 284
Size: 4260 Color: 271

Bin 99: 18 of cap free
Amount of items: 2
Items: 
Size: 7318 Color: 331
Size: 2488 Color: 227

Bin 100: 18 of cap free
Amount of items: 2
Items: 
Size: 7800 Color: 346
Size: 2006 Color: 206

Bin 101: 19 of cap free
Amount of items: 3
Items: 
Size: 6171 Color: 298
Size: 2426 Color: 223
Size: 1208 Color: 167

Bin 102: 20 of cap free
Amount of items: 2
Items: 
Size: 8040 Color: 354
Size: 1764 Color: 197

Bin 103: 22 of cap free
Amount of items: 2
Items: 
Size: 7668 Color: 342
Size: 2134 Color: 215

Bin 104: 28 of cap free
Amount of items: 2
Items: 
Size: 7988 Color: 352
Size: 1808 Color: 198

Bin 105: 30 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 285
Size: 4028 Color: 261
Size: 216 Color: 37

Bin 106: 30 of cap free
Amount of items: 4
Items: 
Size: 6978 Color: 322
Size: 2736 Color: 234
Size: 48 Color: 3
Size: 32 Color: 1

Bin 107: 30 of cap free
Amount of items: 2
Items: 
Size: 7742 Color: 344
Size: 2052 Color: 209

Bin 108: 33 of cap free
Amount of items: 3
Items: 
Size: 6078 Color: 296
Size: 3521 Color: 255
Size: 192 Color: 27

Bin 109: 34 of cap free
Amount of items: 3
Items: 
Size: 6576 Color: 310
Size: 3126 Color: 246
Size: 88 Color: 9

Bin 110: 34 of cap free
Amount of items: 2
Items: 
Size: 6914 Color: 319
Size: 2876 Color: 236

Bin 111: 37 of cap free
Amount of items: 3
Items: 
Size: 6074 Color: 295
Size: 3521 Color: 254
Size: 192 Color: 28

Bin 112: 40 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 278
Size: 2458 Color: 225
Size: 2394 Color: 221

Bin 113: 42 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 309
Size: 3122 Color: 245
Size: 104 Color: 10

Bin 114: 54 of cap free
Amount of items: 2
Items: 
Size: 6204 Color: 301
Size: 3566 Color: 257

Bin 115: 56 of cap free
Amount of items: 3
Items: 
Size: 5456 Color: 283
Size: 4088 Color: 266
Size: 224 Color: 39

Bin 116: 56 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 306
Size: 3228 Color: 247
Size: 160 Color: 15

Bin 117: 60 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 291
Size: 3900 Color: 259
Size: 204 Color: 34

Bin 118: 61 of cap free
Amount of items: 4
Items: 
Size: 5872 Color: 293
Size: 3507 Color: 253
Size: 192 Color: 31
Size: 192 Color: 30

Bin 119: 61 of cap free
Amount of items: 2
Items: 
Size: 6201 Color: 300
Size: 3562 Color: 256

Bin 120: 90 of cap free
Amount of items: 2
Items: 
Size: 5640 Color: 289
Size: 4094 Color: 270

Bin 121: 114 of cap free
Amount of items: 2
Items: 
Size: 5617 Color: 288
Size: 4093 Color: 269

Bin 122: 118 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 308
Size: 3024 Color: 242
Size: 128 Color: 11

Bin 123: 120 of cap free
Amount of items: 3
Items: 
Size: 6232 Color: 304
Size: 3312 Color: 248
Size: 160 Color: 17

Bin 124: 129 of cap free
Amount of items: 3
Items: 
Size: 6514 Color: 307
Size: 3021 Color: 241
Size: 160 Color: 14

Bin 125: 132 of cap free
Amount of items: 2
Items: 
Size: 5601 Color: 287
Size: 4091 Color: 268

Bin 126: 135 of cap free
Amount of items: 2
Items: 
Size: 5599 Color: 286
Size: 4090 Color: 267

Bin 127: 137 of cap free
Amount of items: 5
Items: 
Size: 4917 Color: 275
Size: 1846 Color: 199
Size: 1672 Color: 194
Size: 972 Color: 148
Size: 280 Color: 53

Bin 128: 167 of cap free
Amount of items: 9
Items: 
Size: 4913 Color: 272
Size: 800 Color: 127
Size: 800 Color: 126
Size: 712 Color: 125
Size: 704 Color: 123
Size: 704 Color: 122
Size: 352 Color: 72
Size: 352 Color: 71
Size: 320 Color: 65

Bin 129: 264 of cap free
Amount of items: 6
Items: 
Size: 4916 Color: 274
Size: 1532 Color: 187
Size: 1352 Color: 176
Size: 1168 Color: 165
Size: 312 Color: 61
Size: 280 Color: 54

Bin 130: 270 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 277
Size: 2544 Color: 229
Size: 2090 Color: 211

Bin 131: 284 of cap free
Amount of items: 7
Items: 
Size: 4914 Color: 273
Size: 1120 Color: 161
Size: 1114 Color: 159
Size: 944 Color: 146
Size: 816 Color: 131
Size: 320 Color: 64
Size: 312 Color: 62

Bin 132: 316 of cap free
Amount of items: 18
Items: 
Size: 688 Color: 118
Size: 648 Color: 116
Size: 640 Color: 114
Size: 624 Color: 113
Size: 624 Color: 112
Size: 606 Color: 110
Size: 602 Color: 108
Size: 600 Color: 107
Size: 568 Color: 103
Size: 544 Color: 100
Size: 476 Color: 90
Size: 472 Color: 89
Size: 472 Color: 88
Size: 416 Color: 84
Size: 416 Color: 82
Size: 384 Color: 78
Size: 376 Color: 76
Size: 352 Color: 73

Bin 133: 6380 of cap free
Amount of items: 7
Items: 
Size: 512 Color: 98
Size: 496 Color: 97
Size: 496 Color: 96
Size: 496 Color: 95
Size: 484 Color: 93
Size: 480 Color: 92
Size: 480 Color: 91

Total size: 1296768
Total free space: 9824

