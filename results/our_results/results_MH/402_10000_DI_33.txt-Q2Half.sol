Capicity Bin: 7824
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 1
Size: 970 Color: 1
Size: 444 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5992 Color: 1
Size: 1182 Color: 1
Size: 650 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5814 Color: 1
Size: 1772 Color: 1
Size: 238 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6481 Color: 1
Size: 801 Color: 1
Size: 542 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 1
Size: 698 Color: 1
Size: 136 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6706 Color: 1
Size: 710 Color: 1
Size: 408 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6671 Color: 1
Size: 961 Color: 1
Size: 192 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 1
Size: 1042 Color: 1
Size: 508 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 1
Size: 1800 Color: 1
Size: 512 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6477 Color: 1
Size: 1157 Color: 1
Size: 190 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5356 Color: 1
Size: 2428 Color: 1
Size: 40 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7036 Color: 1
Size: 464 Color: 0
Size: 324 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6188 Color: 1
Size: 1164 Color: 1
Size: 472 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 1
Size: 730 Color: 1
Size: 144 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 1
Size: 660 Color: 1
Size: 176 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 1
Size: 1086 Color: 1
Size: 216 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 3913 Color: 1
Size: 3261 Color: 1
Size: 650 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 1
Size: 1604 Color: 1
Size: 280 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 1
Size: 980 Color: 1
Size: 192 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 1
Size: 728 Color: 1
Size: 128 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6054 Color: 1
Size: 1430 Color: 1
Size: 340 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6948 Color: 1
Size: 700 Color: 1
Size: 176 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6862 Color: 1
Size: 802 Color: 1
Size: 160 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6901 Color: 1
Size: 771 Color: 1
Size: 152 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5594 Color: 1
Size: 1862 Color: 1
Size: 368 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6713 Color: 1
Size: 927 Color: 1
Size: 184 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 1
Size: 2356 Color: 1
Size: 552 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 1
Size: 912 Color: 1
Size: 168 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 1
Size: 1196 Color: 1
Size: 544 Color: 0

Bin 30: 0 of cap free
Amount of items: 4
Items: 
Size: 6812 Color: 1
Size: 724 Color: 1
Size: 144 Color: 0
Size: 144 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 1
Size: 968 Color: 1
Size: 332 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6081 Color: 1
Size: 1311 Color: 1
Size: 432 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5707 Color: 1
Size: 1765 Color: 1
Size: 352 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6720 Color: 1
Size: 944 Color: 1
Size: 160 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6600 Color: 1
Size: 1096 Color: 1
Size: 128 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 1
Size: 764 Color: 1
Size: 152 Color: 0

Bin 37: 0 of cap free
Amount of items: 5
Items: 
Size: 3917 Color: 1
Size: 1887 Color: 1
Size: 1364 Color: 1
Size: 464 Color: 0
Size: 192 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6918 Color: 1
Size: 758 Color: 1
Size: 148 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6247 Color: 1
Size: 1573 Color: 1
Size: 4 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 1
Size: 1138 Color: 1
Size: 290 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 1
Size: 1403 Color: 1
Size: 36 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6736 Color: 1
Size: 920 Color: 1
Size: 168 Color: 0

Bin 43: 0 of cap free
Amount of items: 5
Items: 
Size: 5210 Color: 1
Size: 2182 Color: 1
Size: 192 Color: 0
Size: 160 Color: 0
Size: 80 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6698 Color: 1
Size: 838 Color: 1
Size: 288 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 3918 Color: 1
Size: 3258 Color: 1
Size: 648 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5912 Color: 1
Size: 1608 Color: 1
Size: 304 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6440 Color: 1
Size: 808 Color: 1
Size: 576 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 1
Size: 892 Color: 1
Size: 176 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 1
Size: 1100 Color: 1
Size: 376 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5017 Color: 1
Size: 2341 Color: 1
Size: 466 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6101 Color: 1
Size: 1437 Color: 1
Size: 286 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 1
Size: 1086 Color: 1
Size: 164 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 1
Size: 732 Color: 1
Size: 136 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5652 Color: 1
Size: 2020 Color: 1
Size: 152 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 3916 Color: 1
Size: 3260 Color: 1
Size: 648 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 1
Size: 804 Color: 1
Size: 148 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4984 Color: 1
Size: 2624 Color: 1
Size: 216 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6182 Color: 1
Size: 1370 Color: 1
Size: 272 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6517 Color: 1
Size: 1115 Color: 1
Size: 192 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5797 Color: 1
Size: 1691 Color: 1
Size: 336 Color: 0

Bin 61: 0 of cap free
Amount of items: 5
Items: 
Size: 3262 Color: 1
Size: 2396 Color: 1
Size: 1678 Color: 1
Size: 304 Color: 0
Size: 184 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 3932 Color: 1
Size: 3244 Color: 1
Size: 648 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 1
Size: 2796 Color: 1
Size: 552 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 1
Size: 1032 Color: 1
Size: 272 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 1
Size: 844 Color: 1
Size: 288 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6260 Color: 1
Size: 1308 Color: 1
Size: 256 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6786 Color: 1
Size: 942 Color: 1
Size: 96 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6141 Color: 1
Size: 1453 Color: 1
Size: 230 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 3929 Color: 1
Size: 3701 Color: 1
Size: 194 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5453 Color: 1
Size: 1977 Color: 1
Size: 394 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5518 Color: 1
Size: 1922 Color: 1
Size: 384 Color: 0

Bin 72: 0 of cap free
Amount of items: 5
Items: 
Size: 3914 Color: 1
Size: 1702 Color: 1
Size: 1452 Color: 1
Size: 500 Color: 0
Size: 256 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 1
Size: 948 Color: 1
Size: 288 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6152 Color: 1
Size: 1400 Color: 1
Size: 272 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 1
Size: 1676 Color: 1
Size: 248 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 1
Size: 1528 Color: 1
Size: 224 Color: 0

Bin 77: 0 of cap free
Amount of items: 5
Items: 
Size: 4237 Color: 1
Size: 1751 Color: 1
Size: 1308 Color: 1
Size: 320 Color: 0
Size: 208 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 1
Size: 728 Color: 1
Size: 144 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 1
Size: 1060 Color: 1
Size: 352 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 1
Size: 2186 Color: 1
Size: 436 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 1
Size: 934 Color: 1
Size: 228 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 3928 Color: 1
Size: 3256 Color: 1
Size: 640 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 1
Size: 1180 Color: 1
Size: 216 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6312 Color: 1
Size: 1216 Color: 1
Size: 296 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6508 Color: 1
Size: 1084 Color: 1
Size: 232 Color: 0

Bin 86: 0 of cap free
Amount of items: 5
Items: 
Size: 4498 Color: 1
Size: 1494 Color: 1
Size: 1242 Color: 1
Size: 432 Color: 0
Size: 158 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 3948 Color: 1
Size: 3236 Color: 1
Size: 640 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 1
Size: 898 Color: 1
Size: 176 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 5672 Color: 1
Size: 1824 Color: 1
Size: 328 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 1
Size: 1928 Color: 1
Size: 492 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5919 Color: 1
Size: 1589 Color: 1
Size: 316 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5224 Color: 1
Size: 2376 Color: 1
Size: 224 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 1
Size: 1650 Color: 1
Size: 64 Color: 0

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 4775 Color: 1
Size: 2756 Color: 1
Size: 292 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 4956 Color: 1
Size: 2715 Color: 1
Size: 152 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 6865 Color: 1
Size: 774 Color: 1
Size: 184 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 1
Size: 1462 Color: 1
Size: 352 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 3921 Color: 1
Size: 2896 Color: 0
Size: 1006 Color: 1

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 5782 Color: 1
Size: 1161 Color: 1
Size: 880 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 5392 Color: 1
Size: 2231 Color: 1
Size: 200 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 6433 Color: 1
Size: 742 Color: 1
Size: 648 Color: 0

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 1
Size: 1201 Color: 1
Size: 160 Color: 0

Bin 103: 1 of cap free
Amount of items: 3
Items: 
Size: 4442 Color: 1
Size: 3253 Color: 1
Size: 128 Color: 0

Bin 104: 2 of cap free
Amount of items: 3
Items: 
Size: 5522 Color: 1
Size: 1812 Color: 1
Size: 488 Color: 0

Bin 105: 2 of cap free
Amount of items: 3
Items: 
Size: 6677 Color: 1
Size: 957 Color: 1
Size: 188 Color: 0

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 4696 Color: 1
Size: 2904 Color: 1
Size: 222 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 1
Size: 2114 Color: 1
Size: 8 Color: 0

Bin 108: 2 of cap free
Amount of items: 4
Items: 
Size: 4928 Color: 1
Size: 2502 Color: 1
Size: 232 Color: 0
Size: 160 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 4822 Color: 1
Size: 2616 Color: 1
Size: 384 Color: 0

Bin 110: 2 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 1
Size: 2458 Color: 1
Size: 360 Color: 0

Bin 111: 2 of cap free
Amount of items: 3
Items: 
Size: 4524 Color: 1
Size: 3058 Color: 1
Size: 240 Color: 0

Bin 112: 2 of cap free
Amount of items: 3
Items: 
Size: 5561 Color: 1
Size: 2085 Color: 1
Size: 176 Color: 0

Bin 113: 2 of cap free
Amount of items: 3
Items: 
Size: 6070 Color: 1
Size: 1616 Color: 1
Size: 136 Color: 0

Bin 114: 4 of cap free
Amount of items: 3
Items: 
Size: 6868 Color: 1
Size: 712 Color: 1
Size: 240 Color: 0

Bin 115: 5 of cap free
Amount of items: 3
Items: 
Size: 4878 Color: 1
Size: 2541 Color: 1
Size: 400 Color: 0

Bin 116: 5 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 1
Size: 2168 Color: 1
Size: 284 Color: 0

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 1
Size: 1121 Color: 1
Size: 16 Color: 0

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 5988 Color: 1
Size: 1572 Color: 1
Size: 256 Color: 0

Bin 119: 10 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 1
Size: 1272 Color: 1
Size: 208 Color: 0

Bin 120: 12 of cap free
Amount of items: 3
Items: 
Size: 4450 Color: 1
Size: 2882 Color: 1
Size: 480 Color: 0

Bin 121: 14 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 1
Size: 1464 Color: 1
Size: 312 Color: 0

Bin 122: 19 of cap free
Amount of items: 3
Items: 
Size: 5149 Color: 1
Size: 2060 Color: 1
Size: 596 Color: 0

Bin 123: 34 of cap free
Amount of items: 3
Items: 
Size: 4567 Color: 1
Size: 2991 Color: 1
Size: 232 Color: 0

Bin 124: 64 of cap free
Amount of items: 3
Items: 
Size: 5846 Color: 1
Size: 1542 Color: 1
Size: 372 Color: 0

Bin 125: 83 of cap free
Amount of items: 3
Items: 
Size: 6437 Color: 1
Size: 1160 Color: 1
Size: 144 Color: 0

Bin 126: 271 of cap free
Amount of items: 3
Items: 
Size: 3899 Color: 1
Size: 2774 Color: 1
Size: 880 Color: 0

Bin 127: 432 of cap free
Amount of items: 3
Items: 
Size: 6984 Color: 1
Size: 232 Color: 0
Size: 176 Color: 1

Bin 128: 846 of cap free
Amount of items: 1
Items: 
Size: 6978 Color: 1

Bin 129: 890 of cap free
Amount of items: 1
Items: 
Size: 6934 Color: 1

Bin 130: 926 of cap free
Amount of items: 1
Items: 
Size: 6898 Color: 1

Bin 131: 1002 of cap free
Amount of items: 1
Items: 
Size: 6822 Color: 1

Bin 132: 1206 of cap free
Amount of items: 1
Items: 
Size: 6618 Color: 1

Bin 133: 1956 of cap free
Amount of items: 3
Items: 
Size: 4344 Color: 1
Size: 1294 Color: 1
Size: 230 Color: 0

Total size: 1032768
Total free space: 7824

