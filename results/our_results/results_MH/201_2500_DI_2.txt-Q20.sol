Capicity Bin: 2048
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 1314 Color: 9
Size: 388 Color: 18
Size: 126 Color: 19
Size: 76 Color: 8
Size: 76 Color: 0
Size: 68 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 8
Size: 186 Color: 8
Size: 36 Color: 3

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1822 Color: 14
Size: 110 Color: 18
Size: 108 Color: 8
Size: 8 Color: 10

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1674 Color: 6
Size: 194 Color: 5
Size: 120 Color: 0
Size: 60 Color: 12

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1385 Color: 7
Size: 635 Color: 14
Size: 20 Color: 10
Size: 8 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 7
Size: 487 Color: 10
Size: 96 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 3
Size: 303 Color: 14
Size: 60 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1319 Color: 3
Size: 609 Color: 11
Size: 120 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 4
Size: 854 Color: 11
Size: 108 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1283 Color: 13
Size: 639 Color: 6
Size: 126 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 6
Size: 385 Color: 3
Size: 76 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 19
Size: 696 Color: 16
Size: 72 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1026 Color: 18
Size: 802 Color: 16
Size: 220 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 14
Size: 273 Color: 2
Size: 54 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 7
Size: 262 Color: 12
Size: 48 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1173 Color: 9
Size: 731 Color: 13
Size: 144 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 8
Size: 472 Color: 8
Size: 160 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 10
Size: 190 Color: 14
Size: 40 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 4
Size: 626 Color: 17
Size: 124 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 6
Size: 551 Color: 2
Size: 108 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 6
Size: 198 Color: 15
Size: 36 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1463 Color: 2
Size: 489 Color: 8
Size: 96 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 18
Size: 342 Color: 5
Size: 316 Color: 9

Bin 24: 0 of cap free
Amount of items: 4
Items: 
Size: 828 Color: 17
Size: 614 Color: 19
Size: 550 Color: 19
Size: 56 Color: 17

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 4
Size: 542 Color: 15
Size: 36 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 18
Size: 382 Color: 10
Size: 76 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 16
Size: 338 Color: 2
Size: 64 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 13
Size: 429 Color: 3
Size: 84 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 0
Size: 706 Color: 4
Size: 140 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 9
Size: 631 Color: 0
Size: 126 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 7
Size: 435 Color: 18
Size: 20 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 6
Size: 446 Color: 17
Size: 88 Color: 10

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 7
Size: 424 Color: 11
Size: 36 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 6
Size: 258 Color: 11
Size: 48 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1169 Color: 16
Size: 733 Color: 4
Size: 146 Color: 12

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 13
Size: 510 Color: 18
Size: 100 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 13
Size: 378 Color: 3
Size: 72 Color: 12

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 16
Size: 390 Color: 5
Size: 76 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1398 Color: 7
Size: 482 Color: 8
Size: 168 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 6
Size: 202 Color: 3
Size: 36 Color: 15

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 13
Size: 281 Color: 19
Size: 56 Color: 18

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 18
Size: 682 Color: 8
Size: 132 Color: 15

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 15
Size: 246 Color: 17
Size: 48 Color: 8

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 19
Size: 903 Color: 14
Size: 120 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 18
Size: 737 Color: 15
Size: 146 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 18
Size: 314 Color: 8
Size: 96 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 15
Size: 398 Color: 13
Size: 76 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1027 Color: 4
Size: 851 Color: 7
Size: 170 Color: 7

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 4
Size: 318 Color: 9
Size: 48 Color: 5

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 19
Size: 553 Color: 11
Size: 208 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1031 Color: 13
Size: 849 Color: 16
Size: 168 Color: 5

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 19
Size: 242 Color: 11
Size: 48 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 15
Size: 281 Color: 1
Size: 54 Color: 14

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 1
Size: 263 Color: 10
Size: 52 Color: 19

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1621 Color: 16
Size: 357 Color: 13
Size: 70 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 16
Size: 351 Color: 9
Size: 68 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 17
Size: 313 Color: 4
Size: 62 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 4
Size: 325 Color: 1
Size: 64 Color: 17

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 13
Size: 265 Color: 7
Size: 2 Color: 14

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 0
Size: 463 Color: 17
Size: 58 Color: 13

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 19
Size: 275 Color: 14
Size: 30 Color: 15

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 17
Size: 430 Color: 10
Size: 84 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 9
Size: 213 Color: 2
Size: 42 Color: 10

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 8
Size: 431 Color: 9
Size: 86 Color: 15

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 4
Size: 241 Color: 19
Size: 46 Color: 1

Total size: 133120
Total free space: 0

