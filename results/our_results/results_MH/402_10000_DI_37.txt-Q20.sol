Capicity Bin: 8080
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 6
Size: 3364 Color: 2
Size: 232 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4557 Color: 14
Size: 2937 Color: 11
Size: 586 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4589 Color: 0
Size: 2911 Color: 5
Size: 580 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 13
Size: 2738 Color: 2
Size: 346 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5930 Color: 16
Size: 1782 Color: 13
Size: 368 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5938 Color: 3
Size: 1786 Color: 12
Size: 356 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6202 Color: 11
Size: 1566 Color: 1
Size: 312 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 1
Size: 1240 Color: 12
Size: 506 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6355 Color: 1
Size: 1533 Color: 2
Size: 192 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 10
Size: 1382 Color: 5
Size: 272 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6436 Color: 11
Size: 1508 Color: 18
Size: 136 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6602 Color: 10
Size: 1234 Color: 11
Size: 244 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 8
Size: 956 Color: 15
Size: 514 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 18
Size: 1222 Color: 3
Size: 240 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 0
Size: 1044 Color: 14
Size: 416 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6676 Color: 12
Size: 1220 Color: 8
Size: 184 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6679 Color: 11
Size: 1049 Color: 18
Size: 352 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6713 Color: 19
Size: 1055 Color: 17
Size: 312 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6758 Color: 2
Size: 1004 Color: 17
Size: 318 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6766 Color: 3
Size: 1102 Color: 0
Size: 212 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6797 Color: 9
Size: 1127 Color: 19
Size: 156 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 2
Size: 1052 Color: 6
Size: 208 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6851 Color: 19
Size: 1141 Color: 4
Size: 88 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 7
Size: 1132 Color: 1
Size: 72 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 15
Size: 822 Color: 8
Size: 376 Color: 5

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6923 Color: 2
Size: 965 Color: 1
Size: 192 Color: 18

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6938 Color: 16
Size: 954 Color: 7
Size: 188 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 11
Size: 684 Color: 4
Size: 456 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6996 Color: 0
Size: 684 Color: 16
Size: 400 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7094 Color: 15
Size: 754 Color: 16
Size: 232 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 19
Size: 582 Color: 14
Size: 396 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 2
Size: 632 Color: 11
Size: 244 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7226 Color: 6
Size: 544 Color: 0
Size: 310 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7234 Color: 5
Size: 592 Color: 10
Size: 254 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7242 Color: 19
Size: 702 Color: 17
Size: 136 Color: 11

Bin 36: 1 of cap free
Amount of items: 8
Items: 
Size: 4041 Color: 14
Size: 724 Color: 19
Size: 706 Color: 12
Size: 672 Color: 6
Size: 672 Color: 5
Size: 672 Color: 4
Size: 432 Color: 16
Size: 160 Color: 17

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 4266 Color: 12
Size: 3367 Color: 11
Size: 446 Color: 14

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5044 Color: 8
Size: 2531 Color: 10
Size: 504 Color: 5

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 1
Size: 2213 Color: 4
Size: 176 Color: 1

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 18
Size: 1755 Color: 8
Size: 544 Color: 6

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 15
Size: 1949 Color: 0
Size: 256 Color: 18

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5909 Color: 11
Size: 1580 Color: 13
Size: 590 Color: 17

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5946 Color: 2
Size: 1271 Color: 6
Size: 862 Color: 16

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6171 Color: 4
Size: 1558 Color: 7
Size: 350 Color: 17

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6195 Color: 4
Size: 1756 Color: 4
Size: 128 Color: 5

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6214 Color: 0
Size: 1729 Color: 19
Size: 136 Color: 2

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 10
Size: 1571 Color: 1
Size: 144 Color: 15

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 6
Size: 908 Color: 2
Size: 732 Color: 15

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 18
Size: 1420 Color: 5
Size: 104 Color: 17

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6564 Color: 6
Size: 1291 Color: 14
Size: 224 Color: 2

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6729 Color: 2
Size: 714 Color: 14
Size: 636 Color: 9

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6917 Color: 19
Size: 942 Color: 11
Size: 220 Color: 0

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 6946 Color: 6
Size: 1133 Color: 11

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 7013 Color: 11
Size: 1002 Color: 6
Size: 64 Color: 18

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 4802 Color: 12
Size: 3276 Color: 10

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 4989 Color: 14
Size: 2917 Color: 10
Size: 172 Color: 12

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 5330 Color: 3
Size: 2748 Color: 0

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5999 Color: 5
Size: 1735 Color: 16
Size: 344 Color: 15

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6007 Color: 1
Size: 1775 Color: 8
Size: 296 Color: 2

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 7046 Color: 18
Size: 1032 Color: 16

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 7260 Color: 12
Size: 818 Color: 5

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 7268 Color: 14
Size: 762 Color: 8
Size: 48 Color: 5

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 4581 Color: 19
Size: 3182 Color: 17
Size: 314 Color: 19

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 5393 Color: 6
Size: 2532 Color: 5
Size: 152 Color: 3

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 5417 Color: 6
Size: 2402 Color: 0
Size: 258 Color: 12

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 8
Size: 2241 Color: 4
Size: 168 Color: 0

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 3
Size: 1551 Color: 13

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 4890 Color: 9
Size: 3186 Color: 4

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 6485 Color: 0
Size: 1591 Color: 13

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6531 Color: 7
Size: 1545 Color: 5

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 6721 Color: 19
Size: 1298 Color: 13
Size: 56 Color: 17

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 7044 Color: 16
Size: 1031 Color: 9

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 6823 Color: 17
Size: 1251 Color: 16

Bin 74: 6 of cap free
Amount of items: 3
Items: 
Size: 7038 Color: 15
Size: 648 Color: 11
Size: 388 Color: 5

Bin 75: 7 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 9
Size: 2957 Color: 17
Size: 112 Color: 19

Bin 76: 7 of cap free
Amount of items: 3
Items: 
Size: 5572 Color: 4
Size: 2261 Color: 6
Size: 240 Color: 5

Bin 77: 7 of cap free
Amount of items: 2
Items: 
Size: 6262 Color: 15
Size: 1811 Color: 13

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 14
Size: 2092 Color: 13

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 6954 Color: 17
Size: 1118 Color: 10

Bin 80: 9 of cap free
Amount of items: 3
Items: 
Size: 4242 Color: 2
Size: 3365 Color: 2
Size: 464 Color: 3

Bin 81: 10 of cap free
Amount of items: 16
Items: 
Size: 672 Color: 3
Size: 636 Color: 13
Size: 632 Color: 6
Size: 544 Color: 12
Size: 528 Color: 12
Size: 512 Color: 15
Size: 512 Color: 3
Size: 510 Color: 19
Size: 504 Color: 4
Size: 496 Color: 13
Size: 496 Color: 1
Size: 442 Color: 14
Size: 440 Color: 9
Size: 416 Color: 0
Size: 376 Color: 10
Size: 354 Color: 16

Bin 82: 10 of cap free
Amount of items: 2
Items: 
Size: 5890 Color: 7
Size: 2180 Color: 4

Bin 83: 10 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 13
Size: 1794 Color: 14

Bin 84: 11 of cap free
Amount of items: 3
Items: 
Size: 5425 Color: 0
Size: 2220 Color: 15
Size: 424 Color: 3

Bin 85: 11 of cap free
Amount of items: 2
Items: 
Size: 6227 Color: 11
Size: 1842 Color: 8

Bin 86: 11 of cap free
Amount of items: 2
Items: 
Size: 7178 Color: 3
Size: 891 Color: 5

Bin 87: 13 of cap free
Amount of items: 3
Items: 
Size: 5582 Color: 11
Size: 2221 Color: 16
Size: 264 Color: 3

Bin 88: 15 of cap free
Amount of items: 2
Items: 
Size: 6893 Color: 9
Size: 1172 Color: 17

Bin 89: 16 of cap free
Amount of items: 2
Items: 
Size: 6188 Color: 15
Size: 1876 Color: 17

Bin 90: 17 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 5
Size: 1369 Color: 10
Size: 1226 Color: 12

Bin 91: 18 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 2
Size: 3362 Color: 13
Size: 450 Color: 4

Bin 92: 19 of cap free
Amount of items: 2
Items: 
Size: 4533 Color: 15
Size: 3528 Color: 17

Bin 93: 19 of cap free
Amount of items: 2
Items: 
Size: 5951 Color: 9
Size: 2110 Color: 16

Bin 94: 20 of cap free
Amount of items: 2
Items: 
Size: 6815 Color: 15
Size: 1245 Color: 19

Bin 95: 21 of cap free
Amount of items: 2
Items: 
Size: 6890 Color: 12
Size: 1169 Color: 19

Bin 96: 21 of cap free
Amount of items: 2
Items: 
Size: 6988 Color: 11
Size: 1071 Color: 12

Bin 97: 22 of cap free
Amount of items: 2
Items: 
Size: 6600 Color: 11
Size: 1458 Color: 19

Bin 98: 23 of cap free
Amount of items: 2
Items: 
Size: 5975 Color: 9
Size: 2082 Color: 14

Bin 99: 24 of cap free
Amount of items: 27
Items: 
Size: 440 Color: 4
Size: 432 Color: 12
Size: 364 Color: 9
Size: 364 Color: 5
Size: 360 Color: 6
Size: 356 Color: 17
Size: 356 Color: 12
Size: 344 Color: 14
Size: 344 Color: 1
Size: 320 Color: 2
Size: 308 Color: 9
Size: 308 Color: 8
Size: 296 Color: 19
Size: 288 Color: 3
Size: 286 Color: 0
Size: 280 Color: 16
Size: 280 Color: 8
Size: 280 Color: 3
Size: 272 Color: 5
Size: 250 Color: 18
Size: 248 Color: 7
Size: 248 Color: 5
Size: 226 Color: 11
Size: 210 Color: 6
Size: 208 Color: 16
Size: 204 Color: 0
Size: 184 Color: 7

Bin 100: 24 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 1
Size: 1924 Color: 5

Bin 101: 24 of cap free
Amount of items: 2
Items: 
Size: 7220 Color: 5
Size: 836 Color: 10

Bin 102: 25 of cap free
Amount of items: 2
Items: 
Size: 6724 Color: 17
Size: 1331 Color: 4

Bin 103: 25 of cap free
Amount of items: 2
Items: 
Size: 7084 Color: 17
Size: 971 Color: 13

Bin 104: 28 of cap free
Amount of items: 2
Items: 
Size: 4044 Color: 14
Size: 4008 Color: 3

Bin 105: 35 of cap free
Amount of items: 2
Items: 
Size: 5037 Color: 11
Size: 3008 Color: 3

Bin 106: 35 of cap free
Amount of items: 2
Items: 
Size: 6219 Color: 17
Size: 1826 Color: 11

Bin 107: 36 of cap free
Amount of items: 3
Items: 
Size: 5045 Color: 8
Size: 2557 Color: 6
Size: 442 Color: 12

Bin 108: 36 of cap free
Amount of items: 2
Items: 
Size: 6464 Color: 13
Size: 1580 Color: 19

Bin 109: 37 of cap free
Amount of items: 2
Items: 
Size: 5836 Color: 8
Size: 2207 Color: 2

Bin 110: 42 of cap free
Amount of items: 4
Items: 
Size: 4043 Color: 11
Size: 1991 Color: 8
Size: 1764 Color: 7
Size: 240 Color: 12

Bin 111: 43 of cap free
Amount of items: 2
Items: 
Size: 5743 Color: 2
Size: 2294 Color: 16

Bin 112: 44 of cap free
Amount of items: 2
Items: 
Size: 7166 Color: 17
Size: 870 Color: 18

Bin 113: 46 of cap free
Amount of items: 3
Items: 
Size: 5246 Color: 7
Size: 2572 Color: 0
Size: 216 Color: 9

Bin 114: 49 of cap free
Amount of items: 2
Items: 
Size: 5369 Color: 2
Size: 2662 Color: 16

Bin 115: 50 of cap free
Amount of items: 2
Items: 
Size: 6380 Color: 7
Size: 1650 Color: 19

Bin 116: 52 of cap free
Amount of items: 2
Items: 
Size: 5284 Color: 19
Size: 2744 Color: 17

Bin 117: 54 of cap free
Amount of items: 3
Items: 
Size: 4684 Color: 6
Size: 3194 Color: 11
Size: 148 Color: 2

Bin 118: 54 of cap free
Amount of items: 2
Items: 
Size: 6587 Color: 15
Size: 1439 Color: 7

Bin 119: 55 of cap free
Amount of items: 2
Items: 
Size: 5693 Color: 1
Size: 2332 Color: 2

Bin 120: 63 of cap free
Amount of items: 2
Items: 
Size: 5013 Color: 13
Size: 3004 Color: 7

Bin 121: 65 of cap free
Amount of items: 2
Items: 
Size: 6579 Color: 5
Size: 1436 Color: 11

Bin 122: 68 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 3
Size: 2900 Color: 7
Size: 868 Color: 18

Bin 123: 70 of cap free
Amount of items: 2
Items: 
Size: 5433 Color: 10
Size: 2577 Color: 11

Bin 124: 73 of cap free
Amount of items: 2
Items: 
Size: 6379 Color: 6
Size: 1628 Color: 15

Bin 125: 78 of cap free
Amount of items: 6
Items: 
Size: 4042 Color: 17
Size: 1112 Color: 9
Size: 1098 Color: 0
Size: 946 Color: 7
Size: 636 Color: 16
Size: 168 Color: 5

Bin 126: 78 of cap free
Amount of items: 2
Items: 
Size: 4798 Color: 0
Size: 3204 Color: 9

Bin 127: 80 of cap free
Amount of items: 2
Items: 
Size: 4258 Color: 7
Size: 3742 Color: 9

Bin 128: 87 of cap free
Amount of items: 3
Items: 
Size: 4046 Color: 2
Size: 2922 Color: 17
Size: 1025 Color: 6

Bin 129: 90 of cap free
Amount of items: 2
Items: 
Size: 4788 Color: 4
Size: 3202 Color: 17

Bin 130: 96 of cap free
Amount of items: 2
Items: 
Size: 5420 Color: 15
Size: 2564 Color: 9

Bin 131: 96 of cap free
Amount of items: 2
Items: 
Size: 5972 Color: 1
Size: 2012 Color: 16

Bin 132: 119 of cap free
Amount of items: 3
Items: 
Size: 4156 Color: 10
Size: 2537 Color: 5
Size: 1268 Color: 15

Bin 133: 5782 of cap free
Amount of items: 13
Items: 
Size: 226 Color: 18
Size: 224 Color: 8
Size: 200 Color: 0
Size: 196 Color: 6
Size: 188 Color: 10
Size: 176 Color: 4
Size: 172 Color: 14
Size: 168 Color: 15
Size: 164 Color: 9
Size: 160 Color: 7
Size: 144 Color: 2
Size: 140 Color: 19
Size: 140 Color: 16

Total size: 1066560
Total free space: 8080

