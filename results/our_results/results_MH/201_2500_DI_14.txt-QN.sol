Capicity Bin: 2000
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 808 Color: 134
Size: 460 Color: 113
Size: 330 Color: 101
Size: 220 Color: 81
Size: 182 Color: 71

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1794 Color: 201
Size: 76 Color: 42
Size: 76 Color: 41
Size: 46 Color: 19
Size: 8 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1479 Color: 163
Size: 457 Color: 112
Size: 64 Color: 35

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1655 Color: 178
Size: 245 Color: 88
Size: 52 Color: 25
Size: 48 Color: 22

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 160
Size: 493 Color: 115
Size: 98 Color: 52

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1642 Color: 175
Size: 302 Color: 97
Size: 56 Color: 29

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 157
Size: 501 Color: 118
Size: 98 Color: 50

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 182
Size: 262 Color: 90
Size: 52 Color: 27

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1134 Color: 145
Size: 738 Color: 129
Size: 128 Color: 60

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 191
Size: 218 Color: 79
Size: 40 Color: 12

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 146
Size: 669 Color: 127
Size: 132 Color: 61

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 190
Size: 225 Color: 82
Size: 34 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1002 Color: 139
Size: 834 Color: 136
Size: 164 Color: 67

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 174
Size: 311 Color: 99
Size: 60 Color: 32

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 172
Size: 362 Color: 105
Size: 32 Color: 5

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1707 Color: 184
Size: 241 Color: 87
Size: 52 Color: 24

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 188
Size: 231 Color: 84
Size: 44 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 154
Size: 562 Color: 120
Size: 112 Color: 54

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 198
Size: 186 Color: 72
Size: 36 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 170
Size: 343 Color: 102
Size: 68 Color: 38

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1526 Color: 166
Size: 416 Color: 109
Size: 58 Color: 31

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1592 Color: 171
Size: 344 Color: 103
Size: 64 Color: 33

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 1384 Color: 155
Size: 308 Color: 98
Size: 236 Color: 85
Size: 72 Color: 40

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 167
Size: 383 Color: 106
Size: 76 Color: 43

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 189
Size: 230 Color: 83
Size: 44 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1515 Color: 165
Size: 405 Color: 108
Size: 80 Color: 44

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 176
Size: 289 Color: 94
Size: 64 Color: 36

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 183
Size: 246 Color: 89
Size: 48 Color: 21

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1256 Color: 149
Size: 676 Color: 128
Size: 68 Color: 37

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 168
Size: 361 Color: 104
Size: 72 Color: 39

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 197
Size: 187 Color: 73
Size: 36 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1205 Color: 147
Size: 663 Color: 126
Size: 132 Color: 62

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 192
Size: 213 Color: 78
Size: 42 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1303 Color: 150
Size: 581 Color: 124
Size: 116 Color: 58

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 195
Size: 202 Color: 75
Size: 40 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1313 Color: 152
Size: 573 Color: 122
Size: 114 Color: 57

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1065 Color: 141
Size: 781 Color: 133
Size: 154 Color: 64

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 148
Size: 642 Color: 125
Size: 128 Color: 59

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1309 Color: 151
Size: 577 Color: 123
Size: 114 Color: 56

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 199
Size: 181 Color: 70
Size: 36 Color: 8

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 161
Size: 487 Color: 114
Size: 96 Color: 48

Bin 42: 0 of cap free
Amount of items: 4
Items: 
Size: 1713 Color: 185
Size: 263 Color: 91
Size: 16 Color: 2
Size: 8 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 194
Size: 210 Color: 76
Size: 40 Color: 14

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 169
Size: 398 Color: 107
Size: 32 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 196
Size: 197 Color: 74
Size: 38 Color: 10

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 158
Size: 497 Color: 116
Size: 98 Color: 51

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 193
Size: 211 Color: 77
Size: 40 Color: 15

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 153
Size: 571 Color: 121
Size: 112 Color: 55

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 186
Size: 237 Color: 86
Size: 46 Color: 20

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1069 Color: 142
Size: 777 Color: 132
Size: 154 Color: 65

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 156
Size: 501 Color: 119
Size: 100 Color: 53

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 159
Size: 498 Color: 117
Size: 96 Color: 49

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1724 Color: 187
Size: 220 Color: 80
Size: 56 Color: 28

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 162
Size: 442 Color: 111
Size: 88 Color: 47

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1485 Color: 164
Size: 431 Color: 110
Size: 84 Color: 45

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1001 Color: 138
Size: 833 Color: 135
Size: 166 Color: 68

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 180
Size: 265 Color: 92
Size: 52 Color: 26

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 177
Size: 291 Color: 95
Size: 58 Color: 30

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1611 Color: 173
Size: 325 Color: 100
Size: 64 Color: 34

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 200
Size: 174 Color: 69
Size: 40 Color: 11

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1005 Color: 140
Size: 907 Color: 137
Size: 88 Color: 46

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1081 Color: 144
Size: 767 Color: 130
Size: 152 Color: 63

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 181
Size: 295 Color: 96
Size: 20 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 179
Size: 282 Color: 93
Size: 52 Color: 23

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1073 Color: 143
Size: 773 Color: 131
Size: 154 Color: 66

Total size: 130000
Total free space: 0

