Capicity Bin: 9824
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 3904 Color: 1
Size: 2272 Color: 1
Size: 2048 Color: 0
Size: 1088 Color: 0
Size: 512 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 6556 Color: 1
Size: 2724 Color: 1
Size: 352 Color: 0
Size: 192 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 6776 Color: 1
Size: 2584 Color: 1
Size: 240 Color: 0
Size: 224 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8720 Color: 1
Size: 944 Color: 1
Size: 160 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 7056 Color: 1
Size: 1456 Color: 1
Size: 1120 Color: 0
Size: 192 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 7864 Color: 1
Size: 1640 Color: 1
Size: 288 Color: 0
Size: 32 Color: 0

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 8520 Color: 1
Size: 1208 Color: 1
Size: 64 Color: 0
Size: 32 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7934 Color: 1
Size: 1578 Color: 1
Size: 312 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4918 Color: 1
Size: 4090 Color: 1
Size: 816 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 1
Size: 4260 Color: 1
Size: 648 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8040 Color: 1
Size: 1496 Color: 1
Size: 288 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7550 Color: 1
Size: 1898 Color: 1
Size: 376 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8308 Color: 1
Size: 1268 Color: 1
Size: 248 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6914 Color: 1
Size: 2426 Color: 1
Size: 484 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 1
Size: 2084 Color: 1
Size: 416 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8026 Color: 1
Size: 1502 Color: 1
Size: 296 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6078 Color: 1
Size: 3122 Color: 1
Size: 624 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 1
Size: 4084 Color: 1
Size: 808 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7512 Color: 1
Size: 1928 Color: 1
Size: 384 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4917 Color: 1
Size: 4091 Color: 1
Size: 816 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4952 Color: 1
Size: 4072 Color: 1
Size: 800 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8768 Color: 1
Size: 928 Color: 1
Size: 128 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7800 Color: 1
Size: 1688 Color: 1
Size: 336 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 1
Size: 2356 Color: 1
Size: 464 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 8496 Color: 1
Size: 1136 Color: 1
Size: 192 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7288 Color: 1
Size: 2216 Color: 1
Size: 320 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8756 Color: 1
Size: 892 Color: 1
Size: 176 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 1
Size: 2488 Color: 1
Size: 496 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 8532 Color: 1
Size: 1084 Color: 1
Size: 208 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8632 Color: 1
Size: 1000 Color: 1
Size: 192 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7610 Color: 1
Size: 1846 Color: 1
Size: 368 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6232 Color: 1
Size: 3000 Color: 1
Size: 592 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 8080 Color: 1
Size: 1536 Color: 1
Size: 208 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6864 Color: 1
Size: 2480 Color: 1
Size: 480 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6204 Color: 1
Size: 3020 Color: 1
Size: 600 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6784 Color: 1
Size: 2544 Color: 1
Size: 496 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6728 Color: 1
Size: 2552 Color: 1
Size: 544 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6514 Color: 1
Size: 2762 Color: 1
Size: 548 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7318 Color: 1
Size: 2090 Color: 1
Size: 416 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 1
Size: 2396 Color: 1
Size: 472 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 1
Size: 2876 Color: 1
Size: 568 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 8500 Color: 1
Size: 1108 Color: 1
Size: 216 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7924 Color: 1
Size: 1588 Color: 1
Size: 312 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 8596 Color: 1
Size: 1092 Color: 1
Size: 136 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 8714 Color: 1
Size: 1046 Color: 1
Size: 64 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7716 Color: 1
Size: 1764 Color: 1
Size: 344 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 1
Size: 2374 Color: 1
Size: 472 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5872 Color: 1
Size: 3312 Color: 1
Size: 640 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 1
Size: 2118 Color: 1
Size: 420 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 7624 Color: 1
Size: 1848 Color: 1
Size: 352 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 1
Size: 4028 Color: 1
Size: 800 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 8268 Color: 1
Size: 1300 Color: 1
Size: 256 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 8680 Color: 1
Size: 968 Color: 1
Size: 176 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6296 Color: 1
Size: 2952 Color: 1
Size: 576 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 7988 Color: 1
Size: 1532 Color: 1
Size: 304 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 8836 Color: 1
Size: 828 Color: 1
Size: 160 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 7594 Color: 1
Size: 2026 Color: 1
Size: 204 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6224 Color: 1
Size: 3024 Color: 1
Size: 576 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 7304 Color: 1
Size: 2104 Color: 1
Size: 416 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 8660 Color: 1
Size: 972 Color: 1
Size: 192 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 8744 Color: 1
Size: 904 Color: 1
Size: 176 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 8812 Color: 1
Size: 844 Color: 1
Size: 168 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6074 Color: 1
Size: 3126 Color: 1
Size: 624 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 8092 Color: 1
Size: 1444 Color: 1
Size: 288 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 1
Size: 1352 Color: 1
Size: 256 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 1
Size: 3476 Color: 1
Size: 688 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6904 Color: 1
Size: 2440 Color: 1
Size: 480 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6576 Color: 1
Size: 2736 Color: 1
Size: 512 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5456 Color: 1
Size: 3664 Color: 1
Size: 704 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 8784 Color: 1
Size: 880 Color: 1
Size: 160 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 1
Size: 4068 Color: 1
Size: 808 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 8440 Color: 1
Size: 1160 Color: 1
Size: 224 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 8584 Color: 1
Size: 1048 Color: 1
Size: 192 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5599 Color: 1
Size: 3521 Color: 1
Size: 704 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5644 Color: 1
Size: 3900 Color: 1
Size: 280 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5956 Color: 1
Size: 3228 Color: 1
Size: 640 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 1
Size: 1372 Color: 1
Size: 272 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 8232 Color: 1
Size: 1336 Color: 1
Size: 256 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 8372 Color: 1
Size: 1212 Color: 1
Size: 240 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 7668 Color: 1
Size: 2052 Color: 1
Size: 104 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5640 Color: 1
Size: 3496 Color: 1
Size: 688 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 7856 Color: 1
Size: 1648 Color: 1
Size: 320 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7184 Color: 1
Size: 2224 Color: 1
Size: 416 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 7284 Color: 1
Size: 2124 Color: 1
Size: 416 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 8230 Color: 1
Size: 1330 Color: 1
Size: 264 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 7604 Color: 1
Size: 1908 Color: 1
Size: 312 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6878 Color: 1
Size: 2458 Color: 1
Size: 488 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 8376 Color: 1
Size: 1096 Color: 1
Size: 352 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 8412 Color: 1
Size: 1180 Color: 1
Size: 232 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 8134 Color: 1
Size: 1410 Color: 1
Size: 280 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6836 Color: 1
Size: 2492 Color: 1
Size: 496 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 7664 Color: 1
Size: 1808 Color: 1
Size: 352 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5704 Color: 1
Size: 3448 Color: 1
Size: 672 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 1
Size: 856 Color: 1
Size: 160 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 8088 Color: 1
Size: 1672 Color: 1
Size: 64 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 1
Size: 4088 Color: 1
Size: 816 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 8432 Color: 1
Size: 1168 Color: 1
Size: 224 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 4944 Color: 1
Size: 4080 Color: 1
Size: 800 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 6171 Color: 1
Size: 3045 Color: 1
Size: 608 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 7440 Color: 1
Size: 2000 Color: 1
Size: 384 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 8208 Color: 1
Size: 1360 Color: 1
Size: 256 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 7556 Color: 1
Size: 1892 Color: 1
Size: 376 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6144 Color: 1
Size: 3360 Color: 1
Size: 320 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 8340 Color: 1
Size: 1316 Color: 1
Size: 168 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 7844 Color: 1
Size: 1652 Color: 1
Size: 328 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 8620 Color: 1
Size: 1116 Color: 1
Size: 88 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 4914 Color: 1
Size: 4094 Color: 1
Size: 816 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 8386 Color: 1
Size: 1290 Color: 1
Size: 148 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 7266 Color: 1
Size: 2134 Color: 1
Size: 424 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 8298 Color: 1
Size: 1478 Color: 1
Size: 48 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 1
Size: 2726 Color: 1
Size: 544 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5601 Color: 1
Size: 3521 Color: 1
Size: 702 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 8426 Color: 1
Size: 1322 Color: 1
Size: 76 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 8362 Color: 1
Size: 1446 Color: 1
Size: 16 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 7742 Color: 1
Size: 2006 Color: 1
Size: 76 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4913 Color: 1
Size: 4093 Color: 1
Size: 818 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 8726 Color: 1
Size: 918 Color: 1
Size: 180 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 8434 Color: 1
Size: 1162 Color: 1
Size: 228 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6201 Color: 1
Size: 3021 Color: 1
Size: 602 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 8062 Color: 1
Size: 1470 Color: 1
Size: 292 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6209 Color: 1
Size: 3013 Color: 1
Size: 602 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 1
Size: 3562 Color: 1
Size: 712 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5546 Color: 1
Size: 3566 Color: 1
Size: 712 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6954 Color: 1
Size: 2394 Color: 1
Size: 476 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 5617 Color: 1
Size: 3507 Color: 1
Size: 700 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 1
Size: 3033 Color: 1
Size: 606 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 7782 Color: 1
Size: 1702 Color: 1
Size: 340 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 8490 Color: 1
Size: 1114 Color: 1
Size: 220 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 8578 Color: 1
Size: 1042 Color: 1
Size: 204 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 8610 Color: 1
Size: 1014 Color: 1
Size: 200 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 8774 Color: 1
Size: 878 Color: 1
Size: 172 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 8838 Color: 1
Size: 822 Color: 1
Size: 164 Color: 0

Total size: 1296768
Total free space: 0

