Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 407
Size: 321 Color: 266
Size: 270 Color: 123

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 499
Size: 252 Color: 13
Size: 250 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 437
Size: 292 Color: 210
Size: 275 Color: 138

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 377
Size: 315 Color: 252
Size: 303 Color: 237

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 462
Size: 286 Color: 189
Size: 257 Color: 57

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 431
Size: 318 Color: 259
Size: 253 Color: 31

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 488
Size: 258 Color: 63
Size: 257 Color: 56

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 388
Size: 357 Color: 315
Size: 253 Color: 25

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 490
Size: 262 Color: 84
Size: 250 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 385
Size: 330 Color: 280
Size: 285 Color: 182

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 458
Size: 285 Color: 181
Size: 261 Color: 76

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 348
Size: 346 Color: 298
Size: 285 Color: 186

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 392
Size: 322 Color: 267
Size: 284 Color: 176

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 457
Size: 293 Color: 212
Size: 254 Color: 37

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 363
Size: 371 Color: 349
Size: 253 Color: 29

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 448
Size: 297 Color: 220
Size: 254 Color: 36

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 330
Size: 359 Color: 321
Size: 280 Color: 159

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 454
Size: 278 Color: 153
Size: 271 Color: 126

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 438
Size: 289 Color: 201
Size: 278 Color: 152

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 406
Size: 325 Color: 272
Size: 267 Color: 106

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 353
Size: 371 Color: 350
Size: 258 Color: 66

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 401
Size: 310 Color: 243
Size: 291 Color: 208

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 497
Size: 252 Color: 20
Size: 252 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 435
Size: 291 Color: 204
Size: 277 Color: 146

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 324
Size: 358 Color: 318
Size: 282 Color: 169

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 491
Size: 258 Color: 62
Size: 254 Color: 38

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 356
Size: 369 Color: 345
Size: 258 Color: 67

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 328
Size: 338 Color: 289
Size: 302 Color: 235

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 430
Size: 320 Color: 265
Size: 252 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 461
Size: 276 Color: 142
Size: 269 Color: 114

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 364
Size: 326 Color: 276
Size: 297 Color: 222

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 450
Size: 296 Color: 215
Size: 255 Color: 46

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 417
Size: 315 Color: 254
Size: 271 Color: 128

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 398
Size: 337 Color: 287
Size: 265 Color: 95

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 402
Size: 339 Color: 290
Size: 261 Color: 79

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 361
Size: 369 Color: 346
Size: 256 Color: 50

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 358
Size: 369 Color: 347
Size: 257 Color: 58

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 371
Size: 320 Color: 264
Size: 301 Color: 232

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 332
Size: 362 Color: 331
Size: 276 Color: 143

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 351
Size: 368 Color: 343
Size: 261 Color: 75

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 447
Size: 289 Color: 199
Size: 263 Color: 89

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 323
Size: 334 Color: 284
Size: 307 Color: 239

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 453
Size: 281 Color: 166
Size: 268 Color: 113

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 478
Size: 267 Color: 104
Size: 262 Color: 85

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 333
Size: 360 Color: 327
Size: 278 Color: 154

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 440
Size: 289 Color: 196
Size: 275 Color: 137

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 373
Size: 351 Color: 304
Size: 269 Color: 116

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 376
Size: 357 Color: 312
Size: 261 Color: 78

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 472
Size: 279 Color: 158
Size: 256 Color: 49

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 465
Size: 281 Color: 167
Size: 260 Color: 72

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 341
Size: 328 Color: 279
Size: 305 Color: 238

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 428
Size: 320 Color: 262
Size: 254 Color: 39

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 306
Size: 352 Color: 305
Size: 296 Color: 217

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 326 Color: 275
Size: 254 Color: 32
Size: 420 Color: 424

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 433
Size: 303 Color: 236
Size: 267 Color: 102

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 495
Size: 256 Color: 52
Size: 252 Color: 18

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 452
Size: 289 Color: 195
Size: 260 Color: 73

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 372
Size: 347 Color: 300
Size: 273 Color: 130

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 400
Size: 316 Color: 256
Size: 285 Color: 183

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 445
Size: 307 Color: 240
Size: 250 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 391
Size: 341 Color: 293
Size: 266 Color: 99

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 484
Size: 268 Color: 109
Size: 253 Color: 30

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 338
Size: 345 Color: 296
Size: 290 Color: 202

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 469
Size: 275 Color: 139
Size: 262 Color: 82

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 470
Size: 270 Color: 125
Size: 267 Color: 107

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 397
Size: 319 Color: 260
Size: 284 Color: 179

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 399
Size: 350 Color: 302
Size: 252 Color: 14

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 476
Size: 267 Color: 101
Size: 264 Color: 90

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 466
Size: 276 Color: 144
Size: 263 Color: 87

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 342
Size: 365 Color: 337
Size: 268 Color: 112

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 387
Size: 360 Color: 329
Size: 254 Color: 35

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 365
Size: 326 Color: 273
Size: 297 Color: 223

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 479
Size: 274 Color: 135
Size: 255 Color: 41

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 403
Size: 334 Color: 285
Size: 265 Color: 93

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 394
Size: 320 Color: 263
Size: 285 Color: 180

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 443
Size: 300 Color: 229
Size: 259 Color: 70

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 473
Size: 268 Color: 111
Size: 267 Color: 108

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 434
Size: 317 Color: 257
Size: 252 Color: 16

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 393
Size: 328 Color: 278
Size: 278 Color: 149

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 432
Size: 300 Color: 230
Size: 271 Color: 129

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 477
Size: 270 Color: 124
Size: 260 Color: 74

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 384
Size: 341 Color: 292
Size: 274 Color: 132

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 334
Size: 355 Color: 310
Size: 282 Color: 171

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 390
Size: 357 Color: 313
Size: 251 Color: 8

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 492
Size: 259 Color: 68
Size: 251 Color: 7

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 408
Size: 314 Color: 249
Size: 277 Color: 145

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 500
Size: 251 Color: 11
Size: 250 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 449
Size: 285 Color: 184
Size: 266 Color: 100

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 326
Size: 360 Color: 325
Size: 280 Color: 163

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 320
Size: 356 Color: 311
Size: 285 Color: 187

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 446
Size: 289 Color: 197
Size: 266 Color: 96

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 355
Size: 338 Color: 288
Size: 289 Color: 200

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 493
Size: 255 Color: 48
Size: 255 Color: 47

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 405
Size: 302 Color: 234
Size: 291 Color: 207

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 427
Size: 313 Color: 247
Size: 264 Color: 92

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 482
Size: 268 Color: 110
Size: 257 Color: 59

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 420
Size: 316 Color: 255
Size: 267 Color: 105

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 498
Size: 251 Color: 10
Size: 251 Color: 9

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 415
Size: 312 Color: 244
Size: 274 Color: 133

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 386
Size: 345 Color: 297
Size: 269 Color: 115

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 456
Size: 291 Color: 206
Size: 256 Color: 54

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 468
Size: 280 Color: 161
Size: 258 Color: 65

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 442
Size: 288 Color: 192
Size: 271 Color: 127

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 383
Size: 315 Color: 250
Size: 300 Color: 227

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 409
Size: 309 Color: 241
Size: 282 Color: 168

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 404
Size: 300 Color: 228
Size: 296 Color: 214

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 395
Size: 325 Color: 271
Size: 279 Color: 157

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 485
Size: 265 Color: 94
Size: 255 Color: 45

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 483
Size: 269 Color: 117
Size: 253 Color: 21

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 422
Size: 297 Color: 224
Size: 284 Color: 177

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 419
Size: 313 Color: 248
Size: 270 Color: 121

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 410
Size: 324 Color: 270
Size: 267 Color: 103

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 380
Size: 367 Color: 340
Size: 250 Color: 5

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 374
Size: 357 Color: 316
Size: 262 Color: 81

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 269 Color: 118
Size: 257 Color: 60
Size: 474 Color: 481

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 396
Size: 353 Color: 309
Size: 250 Color: 3

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 389
Size: 350 Color: 301
Size: 259 Color: 69

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 441
Size: 292 Color: 211
Size: 270 Color: 120

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 455
Size: 282 Color: 172
Size: 266 Color: 98

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 368
Size: 342 Color: 294
Size: 280 Color: 164

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 475
Size: 278 Color: 148
Size: 254 Color: 33

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 444
Size: 280 Color: 160
Size: 278 Color: 150

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 489
Size: 262 Color: 83
Size: 253 Color: 22

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 413
Size: 309 Color: 242
Size: 278 Color: 147

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 494
Size: 256 Color: 53
Size: 253 Color: 27

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 436
Size: 312 Color: 246
Size: 255 Color: 44

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 439
Size: 287 Color: 190
Size: 279 Color: 156

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 463
Size: 283 Color: 173
Size: 259 Color: 71

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 451
Size: 280 Color: 162
Size: 270 Color: 122

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 421
Size: 298 Color: 225
Size: 284 Color: 178

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 487
Size: 262 Color: 86
Size: 255 Color: 43

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 459
Size: 288 Color: 194
Size: 258 Color: 64

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 464
Size: 275 Color: 141
Size: 266 Color: 97

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 460
Size: 283 Color: 174
Size: 263 Color: 88

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 367
Size: 335 Color: 286
Size: 287 Color: 191

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 378
Size: 339 Color: 291
Size: 278 Color: 151

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 360
Size: 373 Color: 354
Size: 253 Color: 24

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 317
Size: 357 Color: 314
Size: 285 Color: 185

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 426
Size: 322 Color: 268
Size: 255 Color: 40

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 412
Size: 297 Color: 221
Size: 291 Color: 209

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 486
Size: 261 Color: 77
Size: 256 Color: 51

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 357
Size: 330 Color: 282
Size: 297 Color: 219

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 418
Size: 295 Color: 213
Size: 288 Color: 193

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 471
Size: 283 Color: 175
Size: 253 Color: 26

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 480
Size: 275 Color: 136
Size: 252 Color: 15

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 322
Size: 359 Color: 319
Size: 282 Color: 170

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 370
Size: 351 Color: 303
Size: 270 Color: 119

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 411
Size: 315 Color: 253
Size: 275 Color: 140

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 382
Size: 315 Color: 251
Size: 301 Color: 231

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 425
Size: 320 Color: 261
Size: 258 Color: 61

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 336
Size: 346 Color: 299
Size: 290 Color: 203

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 352
Size: 368 Color: 344
Size: 261 Color: 80

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 414
Size: 332 Color: 283
Size: 255 Color: 42

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 416
Size: 312 Color: 245
Size: 274 Color: 131

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 366
Size: 323 Color: 269
Size: 299 Color: 226

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 339
Size: 345 Color: 295
Size: 289 Color: 198

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 375
Size: 317 Color: 258
Size: 302 Color: 233

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 362
Size: 374 Color: 359
Size: 250 Color: 4

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 496
Size: 254 Color: 34
Size: 251 Color: 12

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 423
Size: 328 Color: 277
Size: 253 Color: 28

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 379
Size: 326 Color: 274
Size: 291 Color: 205

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 369
Size: 364 Color: 335
Size: 257 Color: 55

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 429
Size: 296 Color: 218
Size: 278 Color: 155

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 381
Size: 330 Color: 281
Size: 286 Color: 188

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 467
Size: 274 Color: 134
Size: 264 Color: 91

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 474
Size: 280 Color: 165
Size: 253 Color: 23

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 308
Size: 352 Color: 307
Size: 296 Color: 216

Total size: 167000
Total free space: 0

