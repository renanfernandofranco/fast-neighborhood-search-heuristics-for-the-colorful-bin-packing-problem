Capicity Bin: 19296
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9666 Color: 1
Size: 8026 Color: 1
Size: 1604 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9680 Color: 1
Size: 8016 Color: 1
Size: 1600 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 0
Size: 7524 Color: 1
Size: 584 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11344 Color: 0
Size: 7600 Color: 1
Size: 352 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11760 Color: 1
Size: 6640 Color: 1
Size: 896 Color: 0

Bin 6: 0 of cap free
Amount of items: 5
Items: 
Size: 12713 Color: 1
Size: 2124 Color: 0
Size: 1885 Color: 0
Size: 1606 Color: 0
Size: 968 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 1
Size: 5596 Color: 1
Size: 520 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14180 Color: 1
Size: 4680 Color: 0
Size: 436 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14508 Color: 1
Size: 4268 Color: 0
Size: 520 Color: 1

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 14680 Color: 0
Size: 2332 Color: 1
Size: 1660 Color: 1
Size: 624 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15076 Color: 1
Size: 3796 Color: 0
Size: 424 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15108 Color: 1
Size: 3848 Color: 1
Size: 340 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15840 Color: 0
Size: 2736 Color: 0
Size: 720 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 16016 Color: 0
Size: 1680 Color: 1
Size: 1600 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 16168 Color: 0
Size: 2216 Color: 0
Size: 912 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 16456 Color: 1
Size: 1600 Color: 1
Size: 1240 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 16434 Color: 0
Size: 1638 Color: 0
Size: 1224 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 16474 Color: 1
Size: 2386 Color: 1
Size: 436 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 16485 Color: 0
Size: 1803 Color: 1
Size: 1008 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16496 Color: 1
Size: 2196 Color: 0
Size: 604 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 16488 Color: 0
Size: 1832 Color: 1
Size: 976 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16724 Color: 1
Size: 1606 Color: 0
Size: 966 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16948 Color: 0
Size: 1244 Color: 1
Size: 1104 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 17016 Color: 0
Size: 1344 Color: 1
Size: 936 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 17084 Color: 0
Size: 1844 Color: 0
Size: 368 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 17098 Color: 0
Size: 1782 Color: 1
Size: 416 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 17060 Color: 1
Size: 1764 Color: 1
Size: 472 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 17163 Color: 1
Size: 1765 Color: 1
Size: 368 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 17204 Color: 0
Size: 1758 Color: 1
Size: 334 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 10568 Color: 0
Size: 8035 Color: 0
Size: 692 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 10594 Color: 0
Size: 8269 Color: 0
Size: 432 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 12739 Color: 1
Size: 6264 Color: 1
Size: 292 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 14159 Color: 1
Size: 4808 Color: 0
Size: 328 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 14197 Color: 0
Size: 4698 Color: 0
Size: 400 Color: 1

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 15138 Color: 0
Size: 3773 Color: 0
Size: 384 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 15667 Color: 0
Size: 3132 Color: 0
Size: 496 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 16166 Color: 1
Size: 2777 Color: 0
Size: 352 Color: 1

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 16538 Color: 1
Size: 2757 Color: 0

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 16703 Color: 1
Size: 1834 Color: 0
Size: 758 Color: 1

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 16876 Color: 0
Size: 1779 Color: 1
Size: 640 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 17002 Color: 0
Size: 1673 Color: 1
Size: 620 Color: 1

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 17007 Color: 0
Size: 1176 Color: 0
Size: 1112 Color: 1

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 16970 Color: 1
Size: 2325 Color: 0

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 17105 Color: 0
Size: 2190 Color: 1

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 17340 Color: 1
Size: 1955 Color: 0

Bin 46: 2 of cap free
Amount of items: 28
Items: 
Size: 856 Color: 1
Size: 848 Color: 1
Size: 844 Color: 1
Size: 832 Color: 1
Size: 792 Color: 0
Size: 788 Color: 1
Size: 768 Color: 1
Size: 768 Color: 0
Size: 768 Color: 0
Size: 754 Color: 1
Size: 752 Color: 1
Size: 736 Color: 1
Size: 728 Color: 1
Size: 714 Color: 0
Size: 704 Color: 1
Size: 688 Color: 1
Size: 688 Color: 1
Size: 678 Color: 1
Size: 664 Color: 0
Size: 640 Color: 0
Size: 608 Color: 0
Size: 604 Color: 0
Size: 560 Color: 0
Size: 554 Color: 0
Size: 550 Color: 0
Size: 548 Color: 0
Size: 528 Color: 0
Size: 332 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 11350 Color: 1
Size: 6888 Color: 1
Size: 1056 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 13104 Color: 0
Size: 5336 Color: 1
Size: 854 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 13662 Color: 1
Size: 5296 Color: 1
Size: 336 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 14169 Color: 1
Size: 4805 Color: 1
Size: 320 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 15120 Color: 0
Size: 3106 Color: 0
Size: 1068 Color: 1

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 15534 Color: 0
Size: 3760 Color: 1

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 15965 Color: 1
Size: 3329 Color: 0

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 16142 Color: 1
Size: 1720 Color: 1
Size: 1432 Color: 0

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 17112 Color: 0
Size: 2182 Color: 1

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 17190 Color: 1
Size: 2104 Color: 0

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 13477 Color: 1
Size: 5384 Color: 1
Size: 432 Color: 0

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 13531 Color: 1
Size: 5762 Color: 0

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 15898 Color: 1
Size: 3395 Color: 0

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 15997 Color: 1
Size: 2900 Color: 0
Size: 396 Color: 1

Bin 61: 4 of cap free
Amount of items: 3
Items: 
Size: 12588 Color: 0
Size: 6288 Color: 1
Size: 416 Color: 0

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 15554 Color: 0
Size: 3122 Color: 0
Size: 616 Color: 1

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 15656 Color: 0
Size: 2580 Color: 0
Size: 1056 Color: 1

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 16368 Color: 0
Size: 2924 Color: 1

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 17252 Color: 1
Size: 2040 Color: 0

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 17272 Color: 0
Size: 2020 Color: 1

Bin 67: 5 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 1
Size: 4851 Color: 0
Size: 448 Color: 1

Bin 68: 5 of cap free
Amount of items: 3
Items: 
Size: 14546 Color: 0
Size: 4281 Color: 0
Size: 464 Color: 1

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 16675 Color: 0
Size: 2616 Color: 1

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 16776 Color: 0
Size: 2515 Color: 1

Bin 71: 5 of cap free
Amount of items: 2
Items: 
Size: 17297 Color: 0
Size: 1994 Color: 1

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 17308 Color: 0
Size: 1983 Color: 1

Bin 73: 6 of cap free
Amount of items: 3
Items: 
Size: 10384 Color: 0
Size: 8042 Color: 1
Size: 864 Color: 1

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 14666 Color: 0
Size: 4624 Color: 1

Bin 75: 6 of cap free
Amount of items: 3
Items: 
Size: 15540 Color: 1
Size: 3416 Color: 0
Size: 334 Color: 1

Bin 76: 7 of cap free
Amount of items: 3
Items: 
Size: 11784 Color: 1
Size: 7169 Color: 1
Size: 336 Color: 0

Bin 77: 7 of cap free
Amount of items: 2
Items: 
Size: 15488 Color: 1
Size: 3801 Color: 0

Bin 78: 8 of cap free
Amount of items: 3
Items: 
Size: 9650 Color: 0
Size: 8034 Color: 1
Size: 1604 Color: 0

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 16678 Color: 1
Size: 2610 Color: 0

Bin 80: 9 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 0
Size: 4831 Color: 1
Size: 768 Color: 0

Bin 81: 10 of cap free
Amount of items: 2
Items: 
Size: 15124 Color: 1
Size: 4162 Color: 0

Bin 82: 10 of cap free
Amount of items: 2
Items: 
Size: 15820 Color: 0
Size: 3466 Color: 1

Bin 83: 10 of cap free
Amount of items: 3
Items: 
Size: 16172 Color: 0
Size: 2890 Color: 1
Size: 224 Color: 0

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 16932 Color: 0
Size: 2354 Color: 1

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 17156 Color: 1
Size: 2130 Color: 0

Bin 86: 11 of cap free
Amount of items: 7
Items: 
Size: 9656 Color: 1
Size: 1748 Color: 0
Size: 1721 Color: 0
Size: 1720 Color: 0
Size: 1604 Color: 0
Size: 1448 Color: 1
Size: 1388 Color: 1

Bin 87: 11 of cap free
Amount of items: 2
Items: 
Size: 13820 Color: 0
Size: 5465 Color: 1

Bin 88: 11 of cap free
Amount of items: 3
Items: 
Size: 15982 Color: 0
Size: 1667 Color: 1
Size: 1636 Color: 1

Bin 89: 11 of cap free
Amount of items: 2
Items: 
Size: 16742 Color: 0
Size: 2543 Color: 1

Bin 90: 11 of cap free
Amount of items: 2
Items: 
Size: 17296 Color: 0
Size: 1989 Color: 1

Bin 91: 12 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 0
Size: 6222 Color: 1
Size: 254 Color: 0

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 13936 Color: 1
Size: 5348 Color: 0

Bin 93: 12 of cap free
Amount of items: 2
Items: 
Size: 16048 Color: 0
Size: 3236 Color: 1

Bin 94: 12 of cap free
Amount of items: 2
Items: 
Size: 16616 Color: 1
Size: 2668 Color: 0

Bin 95: 12 of cap free
Amount of items: 2
Items: 
Size: 16795 Color: 0
Size: 2489 Color: 1

Bin 96: 13 of cap free
Amount of items: 3
Items: 
Size: 11795 Color: 1
Size: 6624 Color: 0
Size: 864 Color: 0

Bin 97: 13 of cap free
Amount of items: 2
Items: 
Size: 15062 Color: 0
Size: 4221 Color: 1

Bin 98: 13 of cap free
Amount of items: 2
Items: 
Size: 15816 Color: 1
Size: 3467 Color: 0

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 16653 Color: 0
Size: 2630 Color: 1

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 16707 Color: 0
Size: 2576 Color: 1

Bin 101: 13 of cap free
Amount of items: 2
Items: 
Size: 17035 Color: 1
Size: 2248 Color: 0

Bin 102: 14 of cap free
Amount of items: 3
Items: 
Size: 9652 Color: 0
Size: 9226 Color: 1
Size: 404 Color: 0

Bin 103: 14 of cap free
Amount of items: 2
Items: 
Size: 15752 Color: 0
Size: 3530 Color: 1

Bin 104: 14 of cap free
Amount of items: 2
Items: 
Size: 16906 Color: 1
Size: 2376 Color: 0

Bin 105: 16 of cap free
Amount of items: 3
Items: 
Size: 17139 Color: 0
Size: 2085 Color: 1
Size: 56 Color: 0

Bin 106: 17 of cap free
Amount of items: 5
Items: 
Size: 12784 Color: 1
Size: 2159 Color: 0
Size: 2148 Color: 0
Size: 1612 Color: 1
Size: 576 Color: 1

Bin 107: 18 of cap free
Amount of items: 2
Items: 
Size: 16976 Color: 1
Size: 2302 Color: 0

Bin 108: 18 of cap free
Amount of items: 2
Items: 
Size: 17364 Color: 1
Size: 1914 Color: 0

Bin 109: 20 of cap free
Amount of items: 2
Items: 
Size: 16245 Color: 1
Size: 3031 Color: 0

Bin 110: 21 of cap free
Amount of items: 4
Items: 
Size: 12840 Color: 1
Size: 3080 Color: 0
Size: 2203 Color: 0
Size: 1152 Color: 1

Bin 111: 22 of cap free
Amount of items: 2
Items: 
Size: 16370 Color: 1
Size: 2904 Color: 0

Bin 112: 22 of cap free
Amount of items: 2
Items: 
Size: 16670 Color: 0
Size: 2604 Color: 1

Bin 113: 24 of cap free
Amount of items: 2
Items: 
Size: 17336 Color: 1
Size: 1936 Color: 0

Bin 114: 25 of cap free
Amount of items: 2
Items: 
Size: 14225 Color: 0
Size: 5046 Color: 1

Bin 115: 26 of cap free
Amount of items: 2
Items: 
Size: 17334 Color: 0
Size: 1936 Color: 1

Bin 116: 28 of cap free
Amount of items: 2
Items: 
Size: 16668 Color: 1
Size: 2600 Color: 0

Bin 117: 28 of cap free
Amount of items: 2
Items: 
Size: 16900 Color: 1
Size: 2368 Color: 0

Bin 118: 28 of cap free
Amount of items: 2
Items: 
Size: 17242 Color: 1
Size: 2026 Color: 0

Bin 119: 29 of cap free
Amount of items: 8
Items: 
Size: 9655 Color: 1
Size: 1708 Color: 0
Size: 1640 Color: 0
Size: 1600 Color: 0
Size: 1440 Color: 1
Size: 1368 Color: 1
Size: 1312 Color: 0
Size: 544 Color: 1

Bin 120: 29 of cap free
Amount of items: 3
Items: 
Size: 13954 Color: 1
Size: 3102 Color: 0
Size: 2211 Color: 0

Bin 121: 29 of cap free
Amount of items: 3
Items: 
Size: 14448 Color: 1
Size: 4227 Color: 0
Size: 592 Color: 1

Bin 122: 31 of cap free
Amount of items: 2
Items: 
Size: 14769 Color: 0
Size: 4496 Color: 1

Bin 123: 32 of cap free
Amount of items: 2
Items: 
Size: 16208 Color: 0
Size: 3056 Color: 1

Bin 124: 32 of cap free
Amount of items: 2
Items: 
Size: 16816 Color: 1
Size: 2448 Color: 0

Bin 125: 34 of cap free
Amount of items: 2
Items: 
Size: 15578 Color: 1
Size: 3684 Color: 0

Bin 126: 35 of cap free
Amount of items: 2
Items: 
Size: 17289 Color: 1
Size: 1972 Color: 0

Bin 127: 36 of cap free
Amount of items: 3
Items: 
Size: 13428 Color: 1
Size: 4648 Color: 0
Size: 1184 Color: 0

Bin 128: 36 of cap free
Amount of items: 2
Items: 
Size: 16856 Color: 0
Size: 2404 Color: 1

Bin 129: 36 of cap free
Amount of items: 2
Items: 
Size: 16917 Color: 0
Size: 2343 Color: 1

Bin 130: 37 of cap free
Amount of items: 4
Items: 
Size: 17231 Color: 0
Size: 1868 Color: 1
Size: 96 Color: 1
Size: 64 Color: 0

Bin 131: 38 of cap free
Amount of items: 3
Items: 
Size: 12842 Color: 1
Size: 5456 Color: 1
Size: 960 Color: 0

Bin 132: 38 of cap free
Amount of items: 2
Items: 
Size: 16507 Color: 0
Size: 2751 Color: 1

Bin 133: 38 of cap free
Amount of items: 2
Items: 
Size: 17162 Color: 0
Size: 2096 Color: 1

Bin 134: 40 of cap free
Amount of items: 2
Items: 
Size: 12866 Color: 0
Size: 6390 Color: 1

Bin 135: 44 of cap free
Amount of items: 2
Items: 
Size: 16204 Color: 0
Size: 3048 Color: 1

Bin 136: 45 of cap free
Amount of items: 3
Items: 
Size: 15688 Color: 0
Size: 3339 Color: 1
Size: 224 Color: 1

Bin 137: 45 of cap free
Amount of items: 3
Items: 
Size: 17179 Color: 1
Size: 1912 Color: 0
Size: 160 Color: 0

Bin 138: 46 of cap free
Amount of items: 2
Items: 
Size: 16911 Color: 0
Size: 2339 Color: 1

Bin 139: 48 of cap free
Amount of items: 2
Items: 
Size: 15605 Color: 0
Size: 3643 Color: 1

Bin 140: 52 of cap free
Amount of items: 2
Items: 
Size: 15420 Color: 1
Size: 3824 Color: 0

Bin 141: 52 of cap free
Amount of items: 3
Items: 
Size: 17188 Color: 1
Size: 1964 Color: 0
Size: 92 Color: 1

Bin 142: 52 of cap free
Amount of items: 2
Items: 
Size: 17240 Color: 0
Size: 2004 Color: 1

Bin 143: 54 of cap free
Amount of items: 2
Items: 
Size: 17008 Color: 1
Size: 2234 Color: 0

Bin 144: 56 of cap free
Amount of items: 2
Items: 
Size: 13116 Color: 1
Size: 6124 Color: 0

Bin 145: 57 of cap free
Amount of items: 2
Items: 
Size: 13499 Color: 1
Size: 5740 Color: 0

Bin 146: 58 of cap free
Amount of items: 2
Items: 
Size: 16100 Color: 1
Size: 3138 Color: 0

Bin 147: 60 of cap free
Amount of items: 2
Items: 
Size: 11948 Color: 1
Size: 7288 Color: 0

Bin 148: 62 of cap free
Amount of items: 2
Items: 
Size: 15922 Color: 1
Size: 3312 Color: 0

Bin 149: 62 of cap free
Amount of items: 2
Items: 
Size: 16420 Color: 1
Size: 2814 Color: 0

Bin 150: 67 of cap free
Amount of items: 2
Items: 
Size: 15989 Color: 1
Size: 3240 Color: 0

Bin 151: 68 of cap free
Amount of items: 2
Items: 
Size: 14664 Color: 1
Size: 4564 Color: 0

Bin 152: 69 of cap free
Amount of items: 2
Items: 
Size: 14773 Color: 1
Size: 4454 Color: 0

Bin 153: 73 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 0
Size: 5487 Color: 1

Bin 154: 73 of cap free
Amount of items: 2
Items: 
Size: 16311 Color: 0
Size: 2912 Color: 1

Bin 155: 76 of cap free
Amount of items: 2
Items: 
Size: 15224 Color: 0
Size: 3996 Color: 1

Bin 156: 76 of cap free
Amount of items: 2
Items: 
Size: 16500 Color: 0
Size: 2720 Color: 1

Bin 157: 76 of cap free
Amount of items: 2
Items: 
Size: 17136 Color: 0
Size: 2084 Color: 1

Bin 158: 78 of cap free
Amount of items: 2
Items: 
Size: 16866 Color: 1
Size: 2352 Color: 0

Bin 159: 81 of cap free
Amount of items: 4
Items: 
Size: 9658 Color: 1
Size: 6770 Color: 1
Size: 1827 Color: 0
Size: 960 Color: 0

Bin 160: 84 of cap free
Amount of items: 2
Items: 
Size: 16756 Color: 0
Size: 2456 Color: 1

Bin 161: 86 of cap free
Amount of items: 2
Items: 
Size: 15162 Color: 1
Size: 4048 Color: 0

Bin 162: 87 of cap free
Amount of items: 2
Items: 
Size: 16184 Color: 0
Size: 3025 Color: 1

Bin 163: 88 of cap free
Amount of items: 2
Items: 
Size: 15344 Color: 0
Size: 3864 Color: 1

Bin 164: 98 of cap free
Amount of items: 2
Items: 
Size: 14925 Color: 0
Size: 4273 Color: 1

Bin 165: 99 of cap free
Amount of items: 11
Items: 
Size: 9649 Color: 1
Size: 1092 Color: 0
Size: 1072 Color: 0
Size: 1056 Color: 0
Size: 1056 Color: 0
Size: 928 Color: 1
Size: 912 Color: 1
Size: 888 Color: 1
Size: 880 Color: 1
Size: 864 Color: 0
Size: 800 Color: 0

Bin 166: 101 of cap free
Amount of items: 2
Items: 
Size: 12944 Color: 1
Size: 6251 Color: 0

Bin 167: 102 of cap free
Amount of items: 2
Items: 
Size: 14302 Color: 0
Size: 4892 Color: 1

Bin 168: 104 of cap free
Amount of items: 2
Items: 
Size: 13776 Color: 1
Size: 5416 Color: 0

Bin 169: 104 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 1
Size: 5168 Color: 0

Bin 170: 108 of cap free
Amount of items: 2
Items: 
Size: 15788 Color: 0
Size: 3400 Color: 1

Bin 171: 111 of cap free
Amount of items: 2
Items: 
Size: 15223 Color: 0
Size: 3962 Color: 1

Bin 172: 113 of cap free
Amount of items: 3
Items: 
Size: 15661 Color: 0
Size: 1808 Color: 1
Size: 1714 Color: 1

Bin 173: 121 of cap free
Amount of items: 2
Items: 
Size: 16279 Color: 0
Size: 2896 Color: 1

Bin 174: 124 of cap free
Amount of items: 2
Items: 
Size: 10668 Color: 0
Size: 8504 Color: 1

Bin 175: 126 of cap free
Amount of items: 2
Items: 
Size: 13242 Color: 0
Size: 5928 Color: 1

Bin 176: 132 of cap free
Amount of items: 2
Items: 
Size: 9660 Color: 1
Size: 9504 Color: 0

Bin 177: 136 of cap free
Amount of items: 2
Items: 
Size: 14736 Color: 1
Size: 4424 Color: 0

Bin 178: 140 of cap free
Amount of items: 2
Items: 
Size: 14748 Color: 0
Size: 4408 Color: 1

Bin 179: 150 of cap free
Amount of items: 2
Items: 
Size: 12382 Color: 1
Size: 6764 Color: 0

Bin 180: 165 of cap free
Amount of items: 2
Items: 
Size: 12184 Color: 0
Size: 6947 Color: 1

Bin 181: 166 of cap free
Amount of items: 41
Items: 
Size: 672 Color: 1
Size: 672 Color: 1
Size: 624 Color: 1
Size: 576 Color: 1
Size: 524 Color: 0
Size: 512 Color: 1
Size: 512 Color: 1
Size: 512 Color: 1
Size: 512 Color: 1
Size: 512 Color: 0
Size: 508 Color: 0
Size: 504 Color: 0
Size: 502 Color: 0
Size: 484 Color: 0
Size: 480 Color: 1
Size: 476 Color: 0
Size: 468 Color: 0
Size: 468 Color: 0
Size: 464 Color: 1
Size: 464 Color: 1
Size: 464 Color: 1
Size: 456 Color: 1
Size: 440 Color: 1
Size: 440 Color: 0
Size: 432 Color: 1
Size: 430 Color: 1
Size: 424 Color: 0
Size: 424 Color: 0
Size: 416 Color: 1
Size: 416 Color: 1
Size: 410 Color: 1
Size: 408 Color: 1
Size: 400 Color: 0
Size: 396 Color: 1
Size: 396 Color: 0
Size: 396 Color: 0
Size: 392 Color: 0
Size: 392 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0

Bin 182: 166 of cap free
Amount of items: 2
Items: 
Size: 13768 Color: 1
Size: 5362 Color: 0

Bin 183: 167 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 1
Size: 6225 Color: 0

Bin 184: 174 of cap free
Amount of items: 2
Items: 
Size: 14737 Color: 0
Size: 4385 Color: 1

Bin 185: 180 of cap free
Amount of items: 2
Items: 
Size: 15632 Color: 1
Size: 3484 Color: 0

Bin 186: 181 of cap free
Amount of items: 2
Items: 
Size: 15303 Color: 1
Size: 3812 Color: 0

Bin 187: 206 of cap free
Amount of items: 2
Items: 
Size: 16648 Color: 0
Size: 2442 Color: 1

Bin 188: 207 of cap free
Amount of items: 3
Items: 
Size: 9674 Color: 1
Size: 8039 Color: 1
Size: 1376 Color: 0

Bin 189: 215 of cap free
Amount of items: 2
Items: 
Size: 11827 Color: 1
Size: 7254 Color: 0

Bin 190: 217 of cap free
Amount of items: 2
Items: 
Size: 10695 Color: 1
Size: 8384 Color: 0

Bin 191: 224 of cap free
Amount of items: 2
Items: 
Size: 11032 Color: 1
Size: 8040 Color: 0

Bin 192: 225 of cap free
Amount of items: 2
Items: 
Size: 15625 Color: 1
Size: 3446 Color: 0

Bin 193: 226 of cap free
Amount of items: 2
Items: 
Size: 15208 Color: 0
Size: 3862 Color: 1

Bin 194: 230 of cap free
Amount of items: 2
Items: 
Size: 11834 Color: 0
Size: 7232 Color: 1

Bin 195: 261 of cap free
Amount of items: 2
Items: 
Size: 9672 Color: 0
Size: 9363 Color: 1

Bin 196: 275 of cap free
Amount of items: 9
Items: 
Size: 9651 Color: 1
Size: 1312 Color: 1
Size: 1250 Color: 0
Size: 1248 Color: 0
Size: 1248 Color: 0
Size: 1120 Color: 1
Size: 1096 Color: 0
Size: 1072 Color: 1
Size: 1024 Color: 1

Bin 197: 291 of cap free
Amount of items: 2
Items: 
Size: 10961 Color: 0
Size: 8044 Color: 1

Bin 198: 313 of cap free
Amount of items: 2
Items: 
Size: 9659 Color: 0
Size: 9324 Color: 1

Bin 199: 10154 of cap free
Amount of items: 27
Items: 
Size: 380 Color: 1
Size: 376 Color: 0
Size: 368 Color: 1
Size: 368 Color: 1
Size: 364 Color: 1
Size: 364 Color: 1
Size: 354 Color: 1
Size: 354 Color: 0
Size: 352 Color: 1
Size: 352 Color: 1
Size: 352 Color: 0
Size: 352 Color: 0
Size: 348 Color: 0
Size: 344 Color: 1
Size: 344 Color: 1
Size: 344 Color: 0
Size: 332 Color: 0
Size: 324 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 312 Color: 0
Size: 304 Color: 0
Size: 302 Color: 0
Size: 288 Color: 0
Size: 284 Color: 0

Total size: 3820608
Total free space: 19296

