Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 359 Color: 1
Size: 263 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 287 Color: 1
Size: 254 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 290 Color: 0
Size: 256 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 259 Color: 0
Size: 250 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 0
Size: 318 Color: 1
Size: 265 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 375 Color: 0
Size: 250 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 0
Size: 329 Color: 1
Size: 264 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 329 Color: 0
Size: 315 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 305 Color: 0
Size: 255 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 273 Color: 1
Size: 269 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 355 Color: 1
Size: 261 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 255 Color: 0
Size: 273 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 297 Color: 1
Size: 255 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 280 Color: 1
Size: 261 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 275 Color: 0
Size: 258 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 278 Color: 1
Size: 278 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 266 Color: 1
Size: 261 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 314 Color: 0
Size: 302 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 305 Color: 1
Size: 291 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 0
Size: 267 Color: 1
Size: 256 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 354 Color: 1
Size: 268 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 270 Color: 0
Size: 266 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 324 Color: 1
Size: 263 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 350 Color: 1
Size: 290 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 356 Color: 0
Size: 253 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 313 Color: 0
Size: 290 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 370 Color: 1
Size: 255 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 323 Color: 0
Size: 263 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 317 Color: 1
Size: 267 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 299 Color: 0
Size: 278 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 0
Size: 309 Color: 1
Size: 291 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 356 Color: 1
Size: 283 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 323 Color: 0
Size: 251 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 330 Color: 1
Size: 283 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 0
Size: 334 Color: 1
Size: 267 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 356 Color: 0
Size: 272 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 321 Color: 0
Size: 273 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 0
Size: 337 Color: 1
Size: 322 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 0
Size: 265 Color: 1
Size: 274 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 297 Color: 0
Size: 295 Color: 1

Total size: 40000
Total free space: 0

