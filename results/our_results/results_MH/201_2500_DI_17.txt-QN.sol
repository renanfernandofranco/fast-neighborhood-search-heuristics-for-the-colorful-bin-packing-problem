Capicity Bin: 1996
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 804 Color: 134
Size: 460 Color: 112
Size: 340 Color: 96
Size: 220 Color: 78
Size: 108 Color: 53
Size: 64 Color: 28

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1380 Color: 158
Size: 506 Color: 114
Size: 72 Color: 35
Size: 38 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1346 Color: 156
Size: 550 Color: 119
Size: 100 Color: 48

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1588 Color: 176
Size: 322 Color: 93
Size: 76 Color: 38
Size: 10 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1610 Color: 179
Size: 330 Color: 95
Size: 56 Color: 24

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 197
Size: 190 Color: 73
Size: 36 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 163
Size: 451 Color: 111
Size: 88 Color: 44

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 191
Size: 246 Color: 83
Size: 36 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1002 Color: 139
Size: 830 Color: 136
Size: 164 Color: 67

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 153
Size: 591 Color: 121
Size: 118 Color: 55

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 167
Size: 401 Color: 105
Size: 78 Color: 40

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1259 Color: 151
Size: 615 Color: 123
Size: 122 Color: 57

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 182
Size: 303 Color: 90
Size: 60 Color: 25

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 200
Size: 182 Color: 70
Size: 36 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 187
Size: 262 Color: 85
Size: 52 Color: 20

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 164
Size: 449 Color: 110
Size: 88 Color: 45

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 174
Size: 354 Color: 98
Size: 68 Color: 32

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 184
Size: 294 Color: 89
Size: 48 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 183
Size: 308 Color: 91
Size: 42 Color: 14

Bin 20: 0 of cap free
Amount of items: 4
Items: 
Size: 1761 Color: 195
Size: 211 Color: 77
Size: 16 Color: 3
Size: 8 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 152
Size: 610 Color: 122
Size: 120 Color: 56

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 166
Size: 426 Color: 108
Size: 84 Color: 42

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 161
Size: 542 Color: 118
Size: 64 Color: 27

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 155
Size: 416 Color: 107
Size: 242 Color: 82

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 169
Size: 410 Color: 106
Size: 64 Color: 29

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 198
Size: 189 Color: 72
Size: 36 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1533 Color: 172
Size: 387 Color: 101
Size: 76 Color: 36

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 1617 Color: 180
Size: 351 Color: 97
Size: 20 Color: 4
Size: 8 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 189
Size: 220 Color: 79
Size: 70 Color: 33

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 199
Size: 186 Color: 71
Size: 36 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 165
Size: 446 Color: 109
Size: 88 Color: 46

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1695 Color: 188
Size: 251 Color: 84
Size: 50 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1387 Color: 160
Size: 509 Color: 115
Size: 100 Color: 49

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 175
Size: 369 Color: 100
Size: 52 Color: 21

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 194
Size: 205 Color: 76
Size: 40 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 177
Size: 286 Color: 88
Size: 108 Color: 52

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 181
Size: 318 Color: 92
Size: 60 Color: 26

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 148
Size: 663 Color: 125
Size: 132 Color: 59

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1252 Color: 149
Size: 676 Color: 127
Size: 68 Color: 31

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 196
Size: 198 Color: 75
Size: 36 Color: 7

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1198 Color: 147
Size: 666 Color: 126
Size: 132 Color: 60

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 190
Size: 239 Color: 81
Size: 46 Color: 16

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 186
Size: 263 Color: 86
Size: 52 Color: 19

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 173
Size: 366 Color: 99
Size: 72 Color: 34

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 185
Size: 281 Color: 87
Size: 56 Color: 23

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1525 Color: 170
Size: 393 Color: 103
Size: 78 Color: 39

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 141
Size: 778 Color: 133
Size: 152 Color: 65

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 201
Size: 178 Color: 69
Size: 32 Color: 5

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1607 Color: 178
Size: 325 Color: 94
Size: 64 Color: 30

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 193
Size: 197 Color: 74
Size: 56 Color: 22

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1003 Color: 140
Size: 829 Color: 135
Size: 164 Color: 66

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1145 Color: 145
Size: 767 Color: 132
Size: 84 Color: 43

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 154
Size: 589 Color: 120
Size: 116 Color: 54

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 171
Size: 390 Color: 102
Size: 76 Color: 37

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 999 Color: 138
Size: 831 Color: 137
Size: 166 Color: 68

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 162
Size: 478 Color: 113
Size: 92 Color: 47

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1255 Color: 150
Size: 619 Color: 124
Size: 122 Color: 58

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1137 Color: 143
Size: 717 Color: 130
Size: 142 Color: 62

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 142
Size: 734 Color: 131
Size: 144 Color: 64

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 157
Size: 515 Color: 117
Size: 102 Color: 50

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 168
Size: 397 Color: 104
Size: 78 Color: 41

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 192
Size: 230 Color: 80
Size: 44 Color: 15

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1141 Color: 144
Size: 713 Color: 129
Size: 142 Color: 63

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1162 Color: 146
Size: 698 Color: 128
Size: 136 Color: 61

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 159
Size: 511 Color: 116
Size: 102 Color: 51

Total size: 129740
Total free space: 0

