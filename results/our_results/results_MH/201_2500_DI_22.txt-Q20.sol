Capicity Bin: 2036
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 9
Size: 190 Color: 11
Size: 20 Color: 17

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 820 Color: 6
Size: 468 Color: 9
Size: 424 Color: 0
Size: 316 Color: 1
Size: 8 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 13
Size: 326 Color: 0
Size: 48 Color: 1

Bin 4: 0 of cap free
Amount of items: 6
Items: 
Size: 1408 Color: 4
Size: 348 Color: 12
Size: 224 Color: 4
Size: 32 Color: 18
Size: 16 Color: 18
Size: 8 Color: 7

Bin 5: 0 of cap free
Amount of items: 5
Items: 
Size: 1620 Color: 16
Size: 224 Color: 18
Size: 76 Color: 2
Size: 64 Color: 12
Size: 52 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 2
Size: 282 Color: 15
Size: 56 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 7
Size: 185 Color: 18
Size: 36 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 10
Size: 666 Color: 5
Size: 132 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 8
Size: 407 Color: 8
Size: 20 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 0
Size: 293 Color: 14
Size: 58 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 19
Size: 246 Color: 7
Size: 48 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 0
Size: 569 Color: 7
Size: 112 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 15
Size: 847 Color: 15
Size: 168 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1178 Color: 4
Size: 718 Color: 0
Size: 140 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 16
Size: 727 Color: 7
Size: 144 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 12
Size: 490 Color: 12
Size: 96 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 0
Size: 389 Color: 6
Size: 76 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1022 Color: 12
Size: 890 Color: 4
Size: 124 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 5
Size: 247 Color: 4
Size: 48 Color: 14

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1645 Color: 5
Size: 327 Color: 2
Size: 64 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 17
Size: 441 Color: 18
Size: 88 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 8
Size: 367 Color: 7
Size: 52 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 15
Size: 254 Color: 0
Size: 32 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 1
Size: 242 Color: 6
Size: 60 Color: 9

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 13
Size: 362 Color: 8
Size: 72 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 12
Size: 225 Color: 9
Size: 44 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 5
Size: 407 Color: 1
Size: 80 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 13
Size: 657 Color: 4
Size: 88 Color: 7

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1664 Color: 3
Size: 316 Color: 6
Size: 56 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 6
Size: 321 Color: 10
Size: 64 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1719 Color: 5
Size: 265 Color: 15
Size: 52 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 2
Size: 187 Color: 13
Size: 36 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1566 Color: 2
Size: 394 Color: 14
Size: 76 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1276 Color: 12
Size: 692 Color: 8
Size: 68 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 13
Size: 190 Color: 19
Size: 36 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 5
Size: 178 Color: 5
Size: 44 Color: 7

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 5
Size: 438 Color: 18
Size: 84 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 18
Size: 230 Color: 4
Size: 44 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 4
Size: 442 Color: 10
Size: 88 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1023 Color: 2
Size: 845 Color: 5
Size: 168 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 5
Size: 531 Color: 19
Size: 104 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1019 Color: 7
Size: 989 Color: 10
Size: 28 Color: 17

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 12
Size: 361 Color: 15
Size: 70 Color: 9

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 7
Size: 257 Color: 1
Size: 50 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 11
Size: 463 Color: 7
Size: 92 Color: 16

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 16
Size: 290 Color: 14
Size: 56 Color: 8

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1393 Color: 10
Size: 537 Color: 17
Size: 106 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 19
Size: 721 Color: 14
Size: 144 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1366 Color: 11
Size: 562 Color: 17
Size: 108 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 17
Size: 622 Color: 11
Size: 124 Color: 13

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 7
Size: 541 Color: 1
Size: 106 Color: 17

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 1
Size: 810 Color: 4
Size: 160 Color: 5

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 7
Size: 198 Color: 14
Size: 36 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 4
Size: 262 Color: 13
Size: 48 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 3
Size: 298 Color: 16
Size: 56 Color: 5

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 18
Size: 314 Color: 10
Size: 76 Color: 13

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1289 Color: 10
Size: 623 Color: 1
Size: 124 Color: 13

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 8
Size: 283 Color: 6
Size: 56 Color: 7

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 19
Size: 743 Color: 7
Size: 126 Color: 17

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 16
Size: 538 Color: 11
Size: 104 Color: 11

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1785 Color: 5
Size: 211 Color: 19
Size: 40 Color: 19

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 13
Size: 227 Color: 6
Size: 44 Color: 6

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 16
Size: 209 Color: 0
Size: 40 Color: 6

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 6
Size: 182 Color: 4
Size: 36 Color: 9

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1475 Color: 4
Size: 525 Color: 10
Size: 36 Color: 19

Total size: 132340
Total free space: 0

