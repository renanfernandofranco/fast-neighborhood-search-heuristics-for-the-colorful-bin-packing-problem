Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 0
Size: 363 Color: 4
Size: 271 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 9
Size: 320 Color: 16
Size: 261 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 10
Size: 355 Color: 15
Size: 273 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 8
Size: 287 Color: 1
Size: 269 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 350 Color: 13
Size: 255 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 3
Size: 283 Color: 6
Size: 272 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 2
Size: 347 Color: 12
Size: 292 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 5
Size: 269 Color: 3
Size: 259 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 11
Size: 272 Color: 7
Size: 262 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 12
Size: 315 Color: 12
Size: 275 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 15
Size: 275 Color: 11
Size: 252 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 14
Size: 307 Color: 1
Size: 263 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 15
Size: 274 Color: 9
Size: 252 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 15
Size: 366 Color: 0
Size: 268 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 5
Size: 303 Color: 2
Size: 258 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 273 Color: 16
Size: 357 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 0
Size: 350 Color: 8
Size: 299 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 2
Size: 298 Color: 11
Size: 288 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 3
Size: 254 Color: 0
Size: 251 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 19
Size: 298 Color: 0
Size: 252 Color: 14

Total size: 20000
Total free space: 0

