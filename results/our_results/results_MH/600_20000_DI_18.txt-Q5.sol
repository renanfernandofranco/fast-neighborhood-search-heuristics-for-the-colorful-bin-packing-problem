Capicity Bin: 15760
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 7881 Color: 4
Size: 1507 Color: 4
Size: 1348 Color: 0
Size: 1312 Color: 3
Size: 1312 Color: 1
Size: 1034 Color: 2
Size: 768 Color: 2
Size: 336 Color: 2
Size: 262 Color: 1

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 7886 Color: 3
Size: 2072 Color: 0
Size: 1964 Color: 1
Size: 1867 Color: 4
Size: 1375 Color: 2
Size: 332 Color: 1
Size: 264 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 2
Size: 5816 Color: 1
Size: 928 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 2
Size: 5808 Color: 3
Size: 296 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 2
Size: 5624 Color: 3
Size: 268 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10146 Color: 4
Size: 5338 Color: 2
Size: 276 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10285 Color: 4
Size: 5151 Color: 4
Size: 324 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 0
Size: 4842 Color: 3
Size: 126 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11420 Color: 4
Size: 4028 Color: 2
Size: 312 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11898 Color: 1
Size: 3222 Color: 3
Size: 640 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12140 Color: 4
Size: 3294 Color: 0
Size: 326 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12220 Color: 3
Size: 3260 Color: 1
Size: 280 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12284 Color: 2
Size: 2900 Color: 2
Size: 576 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12506 Color: 1
Size: 2266 Color: 1
Size: 988 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 2
Size: 1631 Color: 4
Size: 1331 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 4
Size: 1912 Color: 2
Size: 924 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 4
Size: 2291 Color: 1
Size: 464 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13042 Color: 1
Size: 2458 Color: 4
Size: 260 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 4
Size: 2296 Color: 0
Size: 368 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13170 Color: 0
Size: 2102 Color: 0
Size: 488 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13242 Color: 4
Size: 2122 Color: 0
Size: 396 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13228 Color: 3
Size: 2116 Color: 2
Size: 416 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 1
Size: 1412 Color: 0
Size: 984 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13381 Color: 1
Size: 1809 Color: 4
Size: 570 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 2
Size: 2176 Color: 4
Size: 184 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13412 Color: 4
Size: 1356 Color: 2
Size: 992 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13410 Color: 3
Size: 1962 Color: 3
Size: 388 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 0
Size: 1976 Color: 4
Size: 288 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 1
Size: 1756 Color: 4
Size: 488 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13539 Color: 4
Size: 1501 Color: 1
Size: 720 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13576 Color: 4
Size: 1304 Color: 1
Size: 880 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 4
Size: 1296 Color: 2
Size: 808 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13662 Color: 4
Size: 1494 Color: 0
Size: 604 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 3
Size: 1304 Color: 4
Size: 796 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13707 Color: 2
Size: 1699 Color: 4
Size: 354 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 4
Size: 1625 Color: 3
Size: 412 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 0
Size: 1304 Color: 3
Size: 720 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13805 Color: 4
Size: 1631 Color: 0
Size: 324 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13788 Color: 2
Size: 1624 Color: 4
Size: 348 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 4
Size: 1312 Color: 1
Size: 640 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13811 Color: 3
Size: 1633 Color: 4
Size: 316 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 2
Size: 1414 Color: 2
Size: 492 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13868 Color: 0
Size: 1580 Color: 4
Size: 312 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13906 Color: 4
Size: 1482 Color: 1
Size: 372 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 2
Size: 964 Color: 1
Size: 916 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 3
Size: 1576 Color: 4
Size: 288 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13938 Color: 4
Size: 1484 Color: 2
Size: 338 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 4
Size: 1448 Color: 1
Size: 368 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13970 Color: 1
Size: 1522 Color: 0
Size: 268 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13982 Color: 2
Size: 1386 Color: 4
Size: 392 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 1
Size: 1152 Color: 4
Size: 584 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14034 Color: 2
Size: 1442 Color: 2
Size: 284 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14066 Color: 2
Size: 1394 Color: 3
Size: 300 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14098 Color: 0
Size: 1322 Color: 4
Size: 340 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14111 Color: 2
Size: 1351 Color: 4
Size: 298 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14178 Color: 4
Size: 864 Color: 3
Size: 718 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14148 Color: 1
Size: 1176 Color: 4
Size: 436 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14170 Color: 1
Size: 1136 Color: 4
Size: 454 Color: 2

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 8871 Color: 3
Size: 6568 Color: 2
Size: 320 Color: 2

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 9549 Color: 1
Size: 5682 Color: 3
Size: 528 Color: 2

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 10661 Color: 3
Size: 4754 Color: 1
Size: 344 Color: 4

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 10910 Color: 0
Size: 4585 Color: 4
Size: 264 Color: 3

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11260 Color: 0
Size: 4223 Color: 0
Size: 276 Color: 3

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12134 Color: 0
Size: 3009 Color: 1
Size: 616 Color: 3

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 12151 Color: 2
Size: 3608 Color: 4

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12624 Color: 4
Size: 2647 Color: 2
Size: 488 Color: 1

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12755 Color: 1
Size: 2696 Color: 4
Size: 308 Color: 0

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12814 Color: 4
Size: 2185 Color: 0
Size: 760 Color: 3

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12881 Color: 0
Size: 2662 Color: 3
Size: 216 Color: 1

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13139 Color: 0
Size: 2460 Color: 4
Size: 160 Color: 2

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13213 Color: 0
Size: 1346 Color: 0
Size: 1200 Color: 4

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13283 Color: 2
Size: 1528 Color: 4
Size: 948 Color: 0

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 2
Size: 2377 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13627 Color: 4
Size: 1444 Color: 3
Size: 688 Color: 3

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13953 Color: 3
Size: 1516 Color: 4
Size: 290 Color: 3

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 14073 Color: 1
Size: 1326 Color: 2
Size: 360 Color: 4

Bin 77: 2 of cap free
Amount of items: 19
Items: 
Size: 1140 Color: 0
Size: 1128 Color: 4
Size: 1120 Color: 3
Size: 1120 Color: 3
Size: 1088 Color: 3
Size: 1028 Color: 0
Size: 976 Color: 4
Size: 856 Color: 4
Size: 848 Color: 4
Size: 844 Color: 4
Size: 812 Color: 0
Size: 746 Color: 1
Size: 712 Color: 1
Size: 656 Color: 2
Size: 636 Color: 1
Size: 600 Color: 1
Size: 504 Color: 2
Size: 480 Color: 2
Size: 464 Color: 2

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 0
Size: 6566 Color: 3
Size: 672 Color: 1

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 9826 Color: 2
Size: 5652 Color: 1
Size: 280 Color: 0

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 9852 Color: 3
Size: 5486 Color: 2
Size: 420 Color: 1

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 10266 Color: 2
Size: 5104 Color: 3
Size: 388 Color: 0

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 10983 Color: 4
Size: 4775 Color: 0

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 1
Size: 2336 Color: 4
Size: 2226 Color: 3

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 11426 Color: 3
Size: 3324 Color: 4
Size: 1008 Color: 1

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 11641 Color: 1
Size: 3733 Color: 2
Size: 384 Color: 3

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 3
Size: 3084 Color: 2
Size: 458 Color: 0

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 12246 Color: 1
Size: 2984 Color: 3
Size: 528 Color: 4

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 12453 Color: 1
Size: 1850 Color: 4
Size: 1455 Color: 3

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 12790 Color: 2
Size: 2968 Color: 3

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 0
Size: 2558 Color: 4
Size: 328 Color: 1

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 13013 Color: 2
Size: 1449 Color: 0
Size: 1296 Color: 4

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 13090 Color: 1
Size: 2408 Color: 3
Size: 260 Color: 4

Bin 93: 2 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 3
Size: 2470 Color: 0

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 13594 Color: 4
Size: 1690 Color: 2
Size: 474 Color: 0

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 8697 Color: 1
Size: 6532 Color: 0
Size: 528 Color: 0

Bin 96: 3 of cap free
Amount of items: 2
Items: 
Size: 10580 Color: 0
Size: 5177 Color: 4

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 11276 Color: 2
Size: 3981 Color: 2
Size: 500 Color: 3

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 11441 Color: 0
Size: 4028 Color: 2
Size: 288 Color: 3

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 11573 Color: 3
Size: 3896 Color: 4
Size: 288 Color: 2

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 12076 Color: 2
Size: 3377 Color: 4
Size: 304 Color: 3

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 4
Size: 2857 Color: 4
Size: 336 Color: 3

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 3
Size: 2401 Color: 3
Size: 400 Color: 4

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 3
Size: 6548 Color: 0
Size: 416 Color: 1

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 9400 Color: 2
Size: 5816 Color: 4
Size: 540 Color: 3

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 4
Size: 4152 Color: 2
Size: 360 Color: 0

Bin 106: 4 of cap free
Amount of items: 2
Items: 
Size: 11432 Color: 4
Size: 4324 Color: 1

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 12060 Color: 3
Size: 3022 Color: 4
Size: 674 Color: 1

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 12812 Color: 4
Size: 2648 Color: 1
Size: 296 Color: 2

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 13138 Color: 0
Size: 2456 Color: 1
Size: 162 Color: 1

Bin 110: 4 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 3
Size: 1768 Color: 0

Bin 111: 4 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 2
Size: 1688 Color: 3

Bin 112: 5 of cap free
Amount of items: 3
Items: 
Size: 11709 Color: 0
Size: 3614 Color: 1
Size: 432 Color: 3

Bin 113: 5 of cap free
Amount of items: 2
Items: 
Size: 14101 Color: 0
Size: 1654 Color: 3

Bin 114: 5 of cap free
Amount of items: 3
Items: 
Size: 14141 Color: 1
Size: 1560 Color: 3
Size: 54 Color: 4

Bin 115: 6 of cap free
Amount of items: 3
Items: 
Size: 7896 Color: 1
Size: 6562 Color: 3
Size: 1296 Color: 4

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 8942 Color: 1
Size: 6504 Color: 3
Size: 308 Color: 0

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 10202 Color: 3
Size: 4924 Color: 4
Size: 628 Color: 3

Bin 118: 6 of cap free
Amount of items: 3
Items: 
Size: 10472 Color: 3
Size: 4946 Color: 4
Size: 336 Color: 0

Bin 119: 6 of cap free
Amount of items: 2
Items: 
Size: 11470 Color: 2
Size: 4284 Color: 4

Bin 120: 6 of cap free
Amount of items: 2
Items: 
Size: 12824 Color: 0
Size: 2930 Color: 3

Bin 121: 6 of cap free
Amount of items: 2
Items: 
Size: 13903 Color: 2
Size: 1851 Color: 1

Bin 122: 6 of cap free
Amount of items: 2
Items: 
Size: 13948 Color: 0
Size: 1806 Color: 3

Bin 123: 7 of cap free
Amount of items: 2
Items: 
Size: 13521 Color: 1
Size: 2232 Color: 3

Bin 124: 7 of cap free
Amount of items: 2
Items: 
Size: 13591 Color: 1
Size: 2162 Color: 3

Bin 125: 7 of cap free
Amount of items: 2
Items: 
Size: 14015 Color: 4
Size: 1738 Color: 2

Bin 126: 8 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 4
Size: 3444 Color: 4
Size: 528 Color: 3

Bin 127: 8 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 2
Size: 3024 Color: 1
Size: 144 Color: 4

Bin 128: 9 of cap free
Amount of items: 7
Items: 
Size: 7884 Color: 3
Size: 1674 Color: 4
Size: 1546 Color: 4
Size: 1383 Color: 0
Size: 1312 Color: 0
Size: 1136 Color: 2
Size: 816 Color: 3

Bin 129: 9 of cap free
Amount of items: 3
Items: 
Size: 11890 Color: 4
Size: 3733 Color: 1
Size: 128 Color: 4

Bin 130: 9 of cap free
Amount of items: 2
Items: 
Size: 14165 Color: 0
Size: 1586 Color: 1

Bin 131: 10 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 1
Size: 2085 Color: 3
Size: 1753 Color: 4

Bin 132: 10 of cap free
Amount of items: 2
Items: 
Size: 12570 Color: 4
Size: 3180 Color: 2

Bin 133: 10 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 3
Size: 1934 Color: 2

Bin 134: 11 of cap free
Amount of items: 3
Items: 
Size: 10259 Color: 2
Size: 4634 Color: 4
Size: 856 Color: 1

Bin 135: 11 of cap free
Amount of items: 2
Items: 
Size: 12333 Color: 2
Size: 3416 Color: 1

Bin 136: 12 of cap free
Amount of items: 2
Items: 
Size: 10104 Color: 1
Size: 5644 Color: 3

Bin 137: 12 of cap free
Amount of items: 3
Items: 
Size: 10596 Color: 1
Size: 4728 Color: 2
Size: 424 Color: 3

Bin 138: 13 of cap free
Amount of items: 3
Items: 
Size: 14023 Color: 2
Size: 1644 Color: 3
Size: 80 Color: 2

Bin 139: 14 of cap free
Amount of items: 3
Items: 
Size: 9581 Color: 0
Size: 5741 Color: 2
Size: 424 Color: 3

Bin 140: 14 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 1
Size: 4308 Color: 4
Size: 342 Color: 3

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 4
Size: 3208 Color: 1

Bin 142: 16 of cap free
Amount of items: 2
Items: 
Size: 14028 Color: 1
Size: 1716 Color: 2

Bin 143: 17 of cap free
Amount of items: 2
Items: 
Size: 13403 Color: 0
Size: 2340 Color: 1

Bin 144: 17 of cap free
Amount of items: 2
Items: 
Size: 13678 Color: 0
Size: 2065 Color: 1

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 13778 Color: 0
Size: 1965 Color: 3

Bin 146: 21 of cap free
Amount of items: 2
Items: 
Size: 12548 Color: 1
Size: 3191 Color: 2

Bin 147: 21 of cap free
Amount of items: 2
Items: 
Size: 13442 Color: 1
Size: 2297 Color: 2

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 1
Size: 2598 Color: 3

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 14090 Color: 0
Size: 1648 Color: 1

Bin 150: 23 of cap free
Amount of items: 3
Items: 
Size: 10693 Color: 4
Size: 4916 Color: 2
Size: 128 Color: 3

Bin 151: 23 of cap free
Amount of items: 2
Items: 
Size: 11933 Color: 1
Size: 3804 Color: 0

Bin 152: 23 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 1
Size: 1983 Color: 0

Bin 153: 23 of cap free
Amount of items: 2
Items: 
Size: 13885 Color: 1
Size: 1852 Color: 2

Bin 154: 24 of cap free
Amount of items: 3
Items: 
Size: 7924 Color: 1
Size: 6564 Color: 3
Size: 1248 Color: 4

Bin 155: 24 of cap free
Amount of items: 2
Items: 
Size: 13180 Color: 3
Size: 2556 Color: 0

Bin 156: 25 of cap free
Amount of items: 2
Items: 
Size: 8996 Color: 0
Size: 6739 Color: 1

Bin 157: 25 of cap free
Amount of items: 2
Items: 
Size: 12585 Color: 1
Size: 3150 Color: 0

Bin 158: 25 of cap free
Amount of items: 2
Items: 
Size: 13371 Color: 1
Size: 2364 Color: 0

Bin 159: 26 of cap free
Amount of items: 2
Items: 
Size: 12156 Color: 4
Size: 3578 Color: 0

Bin 160: 26 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 0
Size: 2186 Color: 1

Bin 161: 28 of cap free
Amount of items: 2
Items: 
Size: 9208 Color: 0
Size: 6524 Color: 2

Bin 162: 30 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 0
Size: 2714 Color: 1

Bin 163: 30 of cap free
Amount of items: 2
Items: 
Size: 13503 Color: 1
Size: 2227 Color: 2

Bin 164: 32 of cap free
Amount of items: 2
Items: 
Size: 12700 Color: 0
Size: 3028 Color: 3

Bin 165: 32 of cap free
Amount of items: 2
Items: 
Size: 13540 Color: 3
Size: 2188 Color: 0

Bin 166: 34 of cap free
Amount of items: 2
Items: 
Size: 13058 Color: 1
Size: 2668 Color: 3

Bin 167: 36 of cap free
Amount of items: 2
Items: 
Size: 10628 Color: 1
Size: 5096 Color: 2

Bin 168: 37 of cap free
Amount of items: 3
Items: 
Size: 7883 Color: 4
Size: 6928 Color: 3
Size: 912 Color: 1

Bin 169: 37 of cap free
Amount of items: 2
Items: 
Size: 13218 Color: 1
Size: 2505 Color: 0

Bin 170: 37 of cap free
Amount of items: 2
Items: 
Size: 13803 Color: 1
Size: 1920 Color: 3

Bin 171: 38 of cap free
Amount of items: 34
Items: 
Size: 744 Color: 3
Size: 700 Color: 3
Size: 604 Color: 0
Size: 600 Color: 4
Size: 592 Color: 4
Size: 584 Color: 3
Size: 576 Color: 1
Size: 550 Color: 4
Size: 528 Color: 0
Size: 480 Color: 0
Size: 478 Color: 4
Size: 472 Color: 2
Size: 472 Color: 1
Size: 464 Color: 0
Size: 456 Color: 3
Size: 452 Color: 3
Size: 448 Color: 1
Size: 444 Color: 1
Size: 436 Color: 2
Size: 432 Color: 1
Size: 428 Color: 2
Size: 424 Color: 1
Size: 424 Color: 0
Size: 392 Color: 2
Size: 384 Color: 4
Size: 384 Color: 3
Size: 372 Color: 4
Size: 370 Color: 3
Size: 368 Color: 1
Size: 368 Color: 1
Size: 344 Color: 2
Size: 328 Color: 0
Size: 320 Color: 2
Size: 304 Color: 2

Bin 172: 38 of cap free
Amount of items: 2
Items: 
Size: 11982 Color: 2
Size: 3740 Color: 4

Bin 173: 39 of cap free
Amount of items: 4
Items: 
Size: 7940 Color: 3
Size: 3993 Color: 2
Size: 3020 Color: 2
Size: 768 Color: 0

Bin 174: 44 of cap free
Amount of items: 4
Items: 
Size: 7908 Color: 3
Size: 3876 Color: 4
Size: 2956 Color: 4
Size: 976 Color: 1

Bin 175: 46 of cap free
Amount of items: 2
Items: 
Size: 9818 Color: 0
Size: 5896 Color: 4

Bin 176: 46 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 4
Size: 4042 Color: 2

Bin 177: 46 of cap free
Amount of items: 2
Items: 
Size: 12184 Color: 0
Size: 3530 Color: 1

Bin 178: 48 of cap free
Amount of items: 2
Items: 
Size: 13708 Color: 3
Size: 2004 Color: 1

Bin 179: 49 of cap free
Amount of items: 2
Items: 
Size: 13961 Color: 2
Size: 1750 Color: 0

Bin 180: 51 of cap free
Amount of items: 2
Items: 
Size: 14146 Color: 2
Size: 1563 Color: 0

Bin 181: 53 of cap free
Amount of items: 2
Items: 
Size: 10870 Color: 1
Size: 4837 Color: 4

Bin 182: 54 of cap free
Amount of items: 5
Items: 
Size: 7892 Color: 3
Size: 2123 Color: 3
Size: 2084 Color: 0
Size: 1896 Color: 0
Size: 1711 Color: 2

Bin 183: 54 of cap free
Amount of items: 2
Items: 
Size: 11628 Color: 1
Size: 4078 Color: 4

Bin 184: 56 of cap free
Amount of items: 2
Items: 
Size: 9664 Color: 1
Size: 6040 Color: 4

Bin 185: 58 of cap free
Amount of items: 2
Items: 
Size: 13858 Color: 0
Size: 1844 Color: 3

Bin 186: 62 of cap free
Amount of items: 2
Items: 
Size: 13542 Color: 0
Size: 2156 Color: 3

Bin 187: 66 of cap free
Amount of items: 3
Items: 
Size: 10058 Color: 2
Size: 5464 Color: 3
Size: 172 Color: 4

Bin 188: 70 of cap free
Amount of items: 2
Items: 
Size: 7882 Color: 1
Size: 7808 Color: 3

Bin 189: 71 of cap free
Amount of items: 2
Items: 
Size: 11281 Color: 4
Size: 4408 Color: 0

Bin 190: 71 of cap free
Amount of items: 2
Items: 
Size: 14140 Color: 0
Size: 1549 Color: 2

Bin 191: 80 of cap free
Amount of items: 3
Items: 
Size: 8980 Color: 3
Size: 4582 Color: 2
Size: 2118 Color: 0

Bin 192: 81 of cap free
Amount of items: 2
Items: 
Size: 12078 Color: 1
Size: 3601 Color: 4

Bin 193: 88 of cap free
Amount of items: 3
Items: 
Size: 7960 Color: 2
Size: 6564 Color: 0
Size: 1148 Color: 4

Bin 194: 94 of cap free
Amount of items: 2
Items: 
Size: 12909 Color: 2
Size: 2757 Color: 3

Bin 195: 102 of cap free
Amount of items: 2
Items: 
Size: 13879 Color: 3
Size: 1779 Color: 2

Bin 196: 105 of cap free
Amount of items: 3
Items: 
Size: 9624 Color: 0
Size: 5887 Color: 4
Size: 144 Color: 1

Bin 197: 120 of cap free
Amount of items: 2
Items: 
Size: 9954 Color: 1
Size: 5686 Color: 4

Bin 198: 257 of cap free
Amount of items: 2
Items: 
Size: 8938 Color: 2
Size: 6565 Color: 0

Bin 199: 12646 of cap free
Amount of items: 11
Items: 
Size: 304 Color: 3
Size: 304 Color: 0
Size: 300 Color: 4
Size: 296 Color: 1
Size: 288 Color: 1
Size: 276 Color: 2
Size: 274 Color: 3
Size: 272 Color: 3
Size: 272 Color: 2
Size: 264 Color: 2
Size: 264 Color: 0

Total size: 3120480
Total free space: 15760

