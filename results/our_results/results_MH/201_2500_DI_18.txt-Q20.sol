Capicity Bin: 1888
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1090 Color: 18
Size: 298 Color: 0
Size: 292 Color: 4
Size: 208 Color: 13

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1184 Color: 14
Size: 640 Color: 0
Size: 44 Color: 15
Size: 20 Color: 15

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1678 Color: 4
Size: 72 Color: 13
Size: 52 Color: 15
Size: 50 Color: 8
Size: 36 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 18
Size: 209 Color: 11
Size: 32 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 10
Size: 522 Color: 14
Size: 104 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 19
Size: 254 Color: 4
Size: 48 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 19
Size: 530 Color: 6
Size: 32 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 2
Size: 573 Color: 15
Size: 114 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 8
Size: 208 Color: 2
Size: 26 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 15
Size: 666 Color: 13
Size: 436 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 946 Color: 16
Size: 760 Color: 9
Size: 182 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 9
Size: 491 Color: 5
Size: 96 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1367 Color: 11
Size: 435 Color: 12
Size: 86 Color: 10

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 1651 Color: 10
Size: 213 Color: 1
Size: 16 Color: 19
Size: 8 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1190 Color: 4
Size: 606 Color: 9
Size: 92 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 2
Size: 198 Color: 19
Size: 68 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 18
Size: 170 Color: 18
Size: 32 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 4
Size: 261 Color: 2
Size: 50 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1500 Color: 2
Size: 324 Color: 9
Size: 64 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 17
Size: 350 Color: 13
Size: 68 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 7
Size: 178 Color: 7
Size: 40 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 2
Size: 522 Color: 7
Size: 112 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 18
Size: 257 Color: 11
Size: 38 Color: 19

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1193 Color: 6
Size: 581 Color: 16
Size: 114 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 18
Size: 501 Color: 5
Size: 100 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 19
Size: 199 Color: 19
Size: 56 Color: 14

Bin 27: 0 of cap free
Amount of items: 4
Items: 
Size: 1209 Color: 1
Size: 567 Color: 13
Size: 104 Color: 19
Size: 8 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1297 Color: 1
Size: 493 Color: 10
Size: 98 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 14
Size: 265 Color: 12
Size: 42 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 7
Size: 327 Color: 7
Size: 44 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1451 Color: 0
Size: 365 Color: 7
Size: 72 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 15
Size: 217 Color: 11
Size: 42 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 10
Size: 313 Color: 12
Size: 62 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 13
Size: 577 Color: 2
Size: 114 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 15
Size: 253 Color: 8
Size: 50 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 10
Size: 190 Color: 1
Size: 36 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 12
Size: 462 Color: 6
Size: 92 Color: 17

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1187 Color: 5
Size: 669 Color: 8
Size: 32 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 5
Size: 269 Color: 14
Size: 48 Color: 13

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 17
Size: 242 Color: 16
Size: 44 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 15
Size: 283 Color: 3
Size: 56 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1126 Color: 5
Size: 638 Color: 10
Size: 124 Color: 9

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1642 Color: 9
Size: 206 Color: 4
Size: 40 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1077 Color: 19
Size: 677 Color: 15
Size: 134 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 14
Size: 231 Color: 3
Size: 44 Color: 13

Bin 46: 0 of cap free
Amount of items: 4
Items: 
Size: 1304 Color: 13
Size: 392 Color: 5
Size: 156 Color: 17
Size: 36 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 14
Size: 322 Color: 11
Size: 60 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1006 Color: 14
Size: 738 Color: 9
Size: 144 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1293 Color: 0
Size: 497 Color: 19
Size: 98 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 16
Size: 414 Color: 13
Size: 80 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1089 Color: 15
Size: 667 Color: 15
Size: 132 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 4
Size: 222 Color: 16
Size: 132 Color: 18

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1495 Color: 19
Size: 329 Color: 1
Size: 64 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 12
Size: 387 Color: 15
Size: 76 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 7
Size: 274 Color: 14
Size: 52 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1537 Color: 8
Size: 293 Color: 7
Size: 58 Color: 5

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 945 Color: 14
Size: 787 Color: 19
Size: 156 Color: 7

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 7
Size: 398 Color: 5
Size: 76 Color: 5

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1081 Color: 8
Size: 673 Color: 15
Size: 134 Color: 17

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1473 Color: 7
Size: 347 Color: 11
Size: 68 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 1
Size: 191 Color: 0
Size: 36 Color: 8

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 13
Size: 358 Color: 17
Size: 68 Color: 14

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 947 Color: 13
Size: 785 Color: 0
Size: 156 Color: 13

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 7
Size: 409 Color: 18
Size: 80 Color: 17

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1073 Color: 5
Size: 681 Color: 1
Size: 134 Color: 13

Total size: 122720
Total free space: 0

