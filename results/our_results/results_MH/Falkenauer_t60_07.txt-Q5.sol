Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 353 Color: 3
Size: 281 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 330 Color: 3
Size: 260 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 329 Color: 1
Size: 293 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 0
Size: 266 Color: 3
Size: 256 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 4
Size: 362 Color: 0
Size: 273 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 326 Color: 0
Size: 268 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 3
Size: 314 Color: 4
Size: 264 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 365 Color: 1
Size: 255 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 279 Color: 2
Size: 267 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 297 Color: 3
Size: 296 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 0
Size: 324 Color: 4
Size: 303 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 307 Color: 3
Size: 281 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 3
Size: 279 Color: 1
Size: 256 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 264 Color: 4
Size: 260 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 370 Color: 0
Size: 260 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 327 Color: 2
Size: 263 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 318 Color: 4
Size: 250 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 2
Size: 322 Color: 1
Size: 286 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 261 Color: 3
Size: 252 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 4
Size: 265 Color: 2
Size: 255 Color: 1

Total size: 20000
Total free space: 0

