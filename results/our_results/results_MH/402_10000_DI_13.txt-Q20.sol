Capicity Bin: 8224
Lower Bound: 132

Bins used: 132
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 2768 Color: 10
Size: 1904 Color: 1
Size: 1712 Color: 9
Size: 912 Color: 12
Size: 448 Color: 16
Size: 304 Color: 14
Size: 160 Color: 6
Size: 16 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 8
Size: 1128 Color: 10
Size: 224 Color: 19

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 5680 Color: 5
Size: 1280 Color: 10
Size: 928 Color: 11
Size: 256 Color: 18
Size: 80 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6776 Color: 19
Size: 1208 Color: 7
Size: 240 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7032 Color: 11
Size: 1000 Color: 16
Size: 192 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7092 Color: 11
Size: 948 Color: 12
Size: 184 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4625 Color: 12
Size: 3001 Color: 15
Size: 598 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4113 Color: 18
Size: 3427 Color: 12
Size: 684 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6636 Color: 8
Size: 1324 Color: 0
Size: 264 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6165 Color: 19
Size: 1753 Color: 16
Size: 306 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 4
Size: 1748 Color: 0
Size: 344 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5266 Color: 1
Size: 2466 Color: 9
Size: 492 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7126 Color: 6
Size: 918 Color: 13
Size: 180 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6932 Color: 10
Size: 1084 Color: 1
Size: 208 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 12
Size: 2114 Color: 5
Size: 420 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4654 Color: 7
Size: 2978 Color: 11
Size: 592 Color: 10

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4746 Color: 14
Size: 2902 Color: 9
Size: 576 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 10
Size: 868 Color: 19
Size: 168 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4116 Color: 7
Size: 3988 Color: 5
Size: 120 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4641 Color: 19
Size: 2987 Color: 1
Size: 596 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5967 Color: 0
Size: 1881 Color: 3
Size: 376 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 18
Size: 1044 Color: 18
Size: 208 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 19
Size: 1131 Color: 13
Size: 226 Color: 8

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4124 Color: 19
Size: 3420 Color: 18
Size: 680 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7342 Color: 1
Size: 738 Color: 10
Size: 144 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6582 Color: 10
Size: 1370 Color: 0
Size: 272 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5723 Color: 9
Size: 2085 Color: 12
Size: 416 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5706 Color: 2
Size: 2406 Color: 10
Size: 112 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 16
Size: 2732 Color: 1
Size: 544 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4120 Color: 7
Size: 3432 Color: 3
Size: 672 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 9
Size: 1500 Color: 2
Size: 296 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6706 Color: 19
Size: 1266 Color: 4
Size: 252 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 15
Size: 932 Color: 5
Size: 184 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5059 Color: 4
Size: 2639 Color: 3
Size: 526 Color: 14

Bin 35: 0 of cap free
Amount of items: 4
Items: 
Size: 5516 Color: 19
Size: 2260 Color: 8
Size: 256 Color: 16
Size: 192 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7176 Color: 15
Size: 888 Color: 11
Size: 160 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7134 Color: 1
Size: 910 Color: 9
Size: 180 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7207 Color: 5
Size: 849 Color: 4
Size: 168 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 7
Size: 1180 Color: 11
Size: 232 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4115 Color: 8
Size: 3425 Color: 9
Size: 684 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 1
Size: 964 Color: 17
Size: 184 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 2
Size: 1242 Color: 9
Size: 244 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6993 Color: 16
Size: 1027 Color: 14
Size: 204 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 3
Size: 1300 Color: 14
Size: 256 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 1
Size: 1064 Color: 17
Size: 208 Color: 17

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7115 Color: 18
Size: 925 Color: 9
Size: 184 Color: 11

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6349 Color: 16
Size: 1563 Color: 0
Size: 312 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 9
Size: 1662 Color: 2
Size: 228 Color: 13

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 17
Size: 1860 Color: 6
Size: 368 Color: 18

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 13
Size: 1528 Color: 14
Size: 304 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5736 Color: 6
Size: 2088 Color: 13
Size: 400 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7300 Color: 18
Size: 772 Color: 6
Size: 152 Color: 9

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 6
Size: 1140 Color: 8
Size: 224 Color: 14

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4572 Color: 10
Size: 3044 Color: 7
Size: 608 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5274 Color: 3
Size: 2462 Color: 13
Size: 488 Color: 17

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 5
Size: 898 Color: 16
Size: 176 Color: 14

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4738 Color: 10
Size: 2906 Color: 19
Size: 580 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 6
Size: 2044 Color: 13
Size: 400 Color: 15

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 4760 Color: 13
Size: 2888 Color: 2
Size: 576 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 7002 Color: 6
Size: 1022 Color: 19
Size: 200 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6106 Color: 14
Size: 1774 Color: 2
Size: 344 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4122 Color: 5
Size: 3422 Color: 3
Size: 680 Color: 7

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5421 Color: 11
Size: 2337 Color: 6
Size: 466 Color: 15

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 17
Size: 1800 Color: 8
Size: 352 Color: 6

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 7112 Color: 11
Size: 936 Color: 19
Size: 176 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4114 Color: 4
Size: 3458 Color: 17
Size: 652 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 7027 Color: 6
Size: 999 Color: 2
Size: 198 Color: 19

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5912 Color: 15
Size: 1928 Color: 12
Size: 384 Color: 5

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4568 Color: 15
Size: 3240 Color: 4
Size: 416 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6489 Color: 13
Size: 1447 Color: 14
Size: 288 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6818 Color: 19
Size: 1174 Color: 13
Size: 232 Color: 5

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 5210 Color: 5
Size: 2514 Color: 16
Size: 500 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 19
Size: 1570 Color: 14
Size: 312 Color: 15

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7338 Color: 18
Size: 742 Color: 1
Size: 144 Color: 19

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 15
Size: 1612 Color: 16
Size: 320 Color: 18

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4690 Color: 5
Size: 2946 Color: 8
Size: 588 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 15
Size: 814 Color: 10
Size: 160 Color: 10

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6632 Color: 14
Size: 1416 Color: 12
Size: 176 Color: 16

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6540 Color: 1
Size: 1404 Color: 15
Size: 280 Color: 0

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 5168 Color: 12
Size: 3056 Color: 14

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 7193 Color: 0
Size: 861 Color: 18
Size: 170 Color: 15

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 16
Size: 1012 Color: 11
Size: 8 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 5
Size: 2106 Color: 6
Size: 420 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 6
Size: 2476 Color: 12
Size: 488 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 1
Size: 1186 Color: 2
Size: 236 Color: 10

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 19
Size: 902 Color: 19
Size: 180 Color: 14

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7135 Color: 8
Size: 909 Color: 14
Size: 180 Color: 9

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 14
Size: 1042 Color: 14
Size: 204 Color: 2

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6490 Color: 7
Size: 1446 Color: 5
Size: 288 Color: 6

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 7168 Color: 19
Size: 880 Color: 16
Size: 176 Color: 4

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 7380 Color: 6
Size: 708 Color: 19
Size: 136 Color: 14

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5961 Color: 13
Size: 1887 Color: 12
Size: 376 Color: 6

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5258 Color: 16
Size: 2474 Color: 11
Size: 492 Color: 17

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 11
Size: 2631 Color: 10
Size: 526 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 6
Size: 2264 Color: 19
Size: 448 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6312 Color: 16
Size: 1608 Color: 8
Size: 304 Color: 15

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 5770 Color: 15
Size: 2046 Color: 14
Size: 408 Color: 13

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 7384 Color: 8
Size: 712 Color: 6
Size: 128 Color: 11

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 17
Size: 810 Color: 10
Size: 160 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 6717 Color: 17
Size: 1257 Color: 10
Size: 250 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 5715 Color: 16
Size: 2091 Color: 19
Size: 418 Color: 13

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 7304 Color: 14
Size: 776 Color: 6
Size: 144 Color: 15

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6875 Color: 5
Size: 1125 Color: 14
Size: 224 Color: 9

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6786 Color: 3
Size: 1202 Color: 15
Size: 236 Color: 7

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 7400 Color: 14
Size: 696 Color: 18
Size: 128 Color: 15

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 1
Size: 1033 Color: 9
Size: 206 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 4730 Color: 7
Size: 2914 Color: 1
Size: 580 Color: 17

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6725 Color: 5
Size: 1251 Color: 14
Size: 248 Color: 7

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 5112 Color: 11
Size: 2600 Color: 16
Size: 512 Color: 18

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 4
Size: 1175 Color: 5
Size: 232 Color: 7

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6994 Color: 7
Size: 1026 Color: 13
Size: 204 Color: 6

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 19
Size: 1034 Color: 16
Size: 204 Color: 12

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 5400 Color: 12
Size: 2568 Color: 16
Size: 256 Color: 4

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5650 Color: 10
Size: 2146 Color: 19
Size: 428 Color: 6

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 6631 Color: 6
Size: 1329 Color: 16
Size: 264 Color: 14

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6299 Color: 14
Size: 1605 Color: 14
Size: 320 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5250 Color: 13
Size: 2482 Color: 12
Size: 492 Color: 8

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6472 Color: 14
Size: 1464 Color: 12
Size: 288 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 1
Size: 1335 Color: 18
Size: 266 Color: 10

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 5429 Color: 13
Size: 2331 Color: 12
Size: 464 Color: 14

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 8
Size: 915 Color: 4
Size: 182 Color: 15

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6803 Color: 11
Size: 1185 Color: 19
Size: 236 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 7107 Color: 8
Size: 931 Color: 15
Size: 186 Color: 14

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 13
Size: 1703 Color: 12
Size: 340 Color: 10

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6511 Color: 2
Size: 1429 Color: 2
Size: 284 Color: 5

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 7143 Color: 4
Size: 901 Color: 2
Size: 180 Color: 6

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6977 Color: 13
Size: 1041 Color: 1
Size: 206 Color: 6

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6046 Color: 17
Size: 1818 Color: 9
Size: 360 Color: 6

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 6503 Color: 10
Size: 1435 Color: 19
Size: 286 Color: 19

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 4633 Color: 14
Size: 2993 Color: 4
Size: 598 Color: 3

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 5075 Color: 0
Size: 2625 Color: 2
Size: 524 Color: 12

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 6
Size: 1927 Color: 0
Size: 124 Color: 11

Total size: 1085568
Total free space: 0

