Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 461478 Color: 2
Size: 281829 Color: 0
Size: 256694 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 375752 Color: 1
Size: 333509 Color: 3
Size: 290740 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 357007 Color: 4
Size: 325284 Color: 0
Size: 317710 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 340541 Color: 3
Size: 339542 Color: 2
Size: 319918 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 357535 Color: 2
Size: 329998 Color: 4
Size: 312468 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 360450 Color: 3
Size: 347046 Color: 1
Size: 292505 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 361536 Color: 4
Size: 359630 Color: 3
Size: 278835 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 370480 Color: 0
Size: 364375 Color: 3
Size: 265146 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 370174 Color: 4
Size: 348151 Color: 2
Size: 281676 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 373452 Color: 3
Size: 368893 Color: 2
Size: 257656 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 374892 Color: 0
Size: 345157 Color: 1
Size: 279952 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 375034 Color: 1
Size: 324583 Color: 0
Size: 300384 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 376892 Color: 2
Size: 353873 Color: 1
Size: 269236 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 377853 Color: 0
Size: 357734 Color: 4
Size: 264414 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 379796 Color: 4
Size: 332914 Color: 1
Size: 287291 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 380532 Color: 1
Size: 323037 Color: 4
Size: 296432 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 380625 Color: 2
Size: 326941 Color: 0
Size: 292435 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 380991 Color: 2
Size: 316491 Color: 2
Size: 302519 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 379934 Color: 4
Size: 334752 Color: 4
Size: 285315 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 381359 Color: 3
Size: 334072 Color: 0
Size: 284570 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 381443 Color: 2
Size: 337813 Color: 2
Size: 280745 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 382667 Color: 2
Size: 340197 Color: 3
Size: 277137 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 383874 Color: 0
Size: 314024 Color: 4
Size: 302103 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 384069 Color: 1
Size: 358448 Color: 3
Size: 257484 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 385298 Color: 0
Size: 338244 Color: 4
Size: 276459 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 386355 Color: 1
Size: 339295 Color: 3
Size: 274351 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 386517 Color: 1
Size: 330525 Color: 3
Size: 282959 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 386518 Color: 2
Size: 327037 Color: 0
Size: 286446 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 386780 Color: 3
Size: 325260 Color: 3
Size: 287961 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 386985 Color: 2
Size: 318336 Color: 4
Size: 294680 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 387054 Color: 3
Size: 320083 Color: 1
Size: 292864 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 386901 Color: 1
Size: 357597 Color: 0
Size: 255503 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 388117 Color: 0
Size: 327711 Color: 3
Size: 284173 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 387928 Color: 1
Size: 342525 Color: 3
Size: 269548 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 388144 Color: 2
Size: 337595 Color: 0
Size: 274262 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 388641 Color: 4
Size: 345900 Color: 0
Size: 265460 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 388986 Color: 3
Size: 315897 Color: 2
Size: 295118 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 389302 Color: 3
Size: 325098 Color: 2
Size: 285601 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 389734 Color: 0
Size: 346544 Color: 2
Size: 263723 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 390241 Color: 1
Size: 313845 Color: 0
Size: 295915 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 389863 Color: 2
Size: 350579 Color: 3
Size: 259559 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 390273 Color: 4
Size: 327099 Color: 0
Size: 282629 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 390754 Color: 4
Size: 319828 Color: 1
Size: 289419 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 391224 Color: 3
Size: 358464 Color: 3
Size: 250313 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 391637 Color: 0
Size: 354126 Color: 3
Size: 254238 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 392284 Color: 1
Size: 318824 Color: 4
Size: 288893 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 391640 Color: 4
Size: 331466 Color: 2
Size: 276895 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 392146 Color: 4
Size: 343404 Color: 2
Size: 264451 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 392320 Color: 4
Size: 309254 Color: 1
Size: 298427 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 392608 Color: 0
Size: 324648 Color: 3
Size: 282745 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 393245 Color: 4
Size: 305421 Color: 3
Size: 301335 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 393437 Color: 0
Size: 341767 Color: 3
Size: 264797 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 393589 Color: 3
Size: 330359 Color: 3
Size: 276053 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 394547 Color: 4
Size: 307737 Color: 2
Size: 297717 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 395497 Color: 0
Size: 327179 Color: 0
Size: 277325 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 395523 Color: 2
Size: 323563 Color: 4
Size: 280915 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 396423 Color: 2
Size: 338964 Color: 2
Size: 264614 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 396615 Color: 1
Size: 327964 Color: 4
Size: 275422 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 397117 Color: 2
Size: 310035 Color: 1
Size: 292849 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 397283 Color: 3
Size: 352446 Color: 0
Size: 250272 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 397563 Color: 1
Size: 333356 Color: 4
Size: 269082 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 400049 Color: 1
Size: 347966 Color: 1
Size: 251986 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 400923 Color: 1
Size: 313319 Color: 0
Size: 285759 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 400940 Color: 0
Size: 319434 Color: 4
Size: 279627 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 401049 Color: 0
Size: 304259 Color: 3
Size: 294693 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 401104 Color: 1
Size: 315443 Color: 4
Size: 283454 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 401185 Color: 0
Size: 326986 Color: 0
Size: 271830 Color: 4

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 401391 Color: 0
Size: 341006 Color: 1
Size: 257604 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 401473 Color: 4
Size: 303511 Color: 0
Size: 295017 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 402279 Color: 1
Size: 309889 Color: 0
Size: 287833 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 402312 Color: 1
Size: 344373 Color: 3
Size: 253316 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 403008 Color: 2
Size: 324897 Color: 1
Size: 272096 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 404157 Color: 0
Size: 314429 Color: 2
Size: 281415 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 405128 Color: 1
Size: 321846 Color: 2
Size: 273027 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 405633 Color: 0
Size: 300219 Color: 1
Size: 294149 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 405836 Color: 4
Size: 337254 Color: 1
Size: 256911 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 405986 Color: 4
Size: 338653 Color: 0
Size: 255362 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 406006 Color: 2
Size: 309213 Color: 1
Size: 284782 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 406594 Color: 2
Size: 325163 Color: 1
Size: 268244 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 406626 Color: 1
Size: 305586 Color: 2
Size: 287789 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 407146 Color: 0
Size: 301448 Color: 4
Size: 291407 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 407182 Color: 0
Size: 307272 Color: 3
Size: 285547 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 407450 Color: 4
Size: 336869 Color: 4
Size: 255682 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 407916 Color: 2
Size: 313381 Color: 0
Size: 278704 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 407996 Color: 0
Size: 337741 Color: 2
Size: 254264 Color: 4

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 408080 Color: 1
Size: 310744 Color: 2
Size: 281177 Color: 2

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 408871 Color: 2
Size: 300052 Color: 4
Size: 291078 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 409976 Color: 1
Size: 321068 Color: 3
Size: 268957 Color: 3

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 410227 Color: 4
Size: 313379 Color: 2
Size: 276395 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 410689 Color: 0
Size: 330480 Color: 2
Size: 258832 Color: 2

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 411064 Color: 1
Size: 320812 Color: 2
Size: 268125 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 410904 Color: 0
Size: 295791 Color: 4
Size: 293306 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 411189 Color: 1
Size: 297325 Color: 0
Size: 291487 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 411926 Color: 1
Size: 307380 Color: 2
Size: 280695 Color: 4

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 412724 Color: 4
Size: 334057 Color: 1
Size: 253220 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 414339 Color: 4
Size: 324968 Color: 0
Size: 260694 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 414859 Color: 0
Size: 299397 Color: 3
Size: 285745 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 415105 Color: 0
Size: 332524 Color: 4
Size: 252372 Color: 2

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 416384 Color: 1
Size: 329575 Color: 4
Size: 254042 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 415923 Color: 2
Size: 306181 Color: 1
Size: 277897 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 417036 Color: 2
Size: 311497 Color: 2
Size: 271468 Color: 4

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 417958 Color: 4
Size: 301531 Color: 4
Size: 280512 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 417966 Color: 2
Size: 326813 Color: 1
Size: 255222 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 418142 Color: 0
Size: 312374 Color: 4
Size: 269485 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 418411 Color: 2
Size: 304910 Color: 4
Size: 276680 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 418695 Color: 1
Size: 309744 Color: 1
Size: 271562 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 418426 Color: 2
Size: 309497 Color: 4
Size: 272078 Color: 2

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 418561 Color: 2
Size: 306556 Color: 1
Size: 274884 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 418721 Color: 4
Size: 305615 Color: 4
Size: 275665 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 420131 Color: 1
Size: 308617 Color: 4
Size: 271253 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 419484 Color: 4
Size: 306994 Color: 2
Size: 273523 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 420485 Color: 2
Size: 300487 Color: 3
Size: 279029 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 420842 Color: 1
Size: 293789 Color: 4
Size: 285370 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 421001 Color: 1
Size: 326790 Color: 3
Size: 252210 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 421334 Color: 1
Size: 297565 Color: 3
Size: 281102 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 420916 Color: 2
Size: 300129 Color: 0
Size: 278956 Color: 3

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 421261 Color: 0
Size: 313421 Color: 1
Size: 265319 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 421583 Color: 2
Size: 298889 Color: 3
Size: 279529 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 421669 Color: 4
Size: 326300 Color: 1
Size: 252032 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 421828 Color: 2
Size: 324793 Color: 4
Size: 253380 Color: 4

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 422101 Color: 0
Size: 309432 Color: 1
Size: 268468 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 422172 Color: 0
Size: 292519 Color: 3
Size: 285310 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 422228 Color: 0
Size: 327353 Color: 3
Size: 250420 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 422285 Color: 4
Size: 302892 Color: 0
Size: 274824 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 422313 Color: 2
Size: 303125 Color: 4
Size: 274563 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 422412 Color: 2
Size: 313055 Color: 3
Size: 264534 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 423330 Color: 2
Size: 295919 Color: 1
Size: 280752 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 423530 Color: 1
Size: 305687 Color: 3
Size: 270784 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 424095 Color: 1
Size: 293970 Color: 3
Size: 281936 Color: 4

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 423716 Color: 3
Size: 305335 Color: 0
Size: 270950 Color: 4

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 425178 Color: 4
Size: 318944 Color: 3
Size: 255879 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 425666 Color: 0
Size: 289044 Color: 1
Size: 285291 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 426280 Color: 0
Size: 313302 Color: 4
Size: 260419 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 426716 Color: 4
Size: 319657 Color: 1
Size: 253628 Color: 2

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 426509 Color: 1
Size: 318572 Color: 0
Size: 254920 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 426843 Color: 2
Size: 299486 Color: 0
Size: 273672 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 426865 Color: 2
Size: 308853 Color: 3
Size: 264283 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 427008 Color: 2
Size: 301111 Color: 4
Size: 271882 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 427320 Color: 4
Size: 312163 Color: 2
Size: 260518 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 427631 Color: 3
Size: 318590 Color: 1
Size: 253780 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 427879 Color: 3
Size: 321410 Color: 4
Size: 250712 Color: 3

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 428267 Color: 2
Size: 316632 Color: 2
Size: 255102 Color: 3

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 428401 Color: 4
Size: 300810 Color: 3
Size: 270790 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 428731 Color: 1
Size: 305392 Color: 4
Size: 265878 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 428986 Color: 0
Size: 297324 Color: 4
Size: 273691 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 429076 Color: 2
Size: 301220 Color: 4
Size: 269705 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 430154 Color: 2
Size: 307102 Color: 0
Size: 262745 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 430266 Color: 2
Size: 300526 Color: 1
Size: 269209 Color: 4

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 430911 Color: 3
Size: 287419 Color: 0
Size: 281671 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 431456 Color: 3
Size: 296486 Color: 2
Size: 272059 Color: 3

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 431848 Color: 3
Size: 293975 Color: 4
Size: 274178 Color: 2

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 431997 Color: 3
Size: 289825 Color: 0
Size: 278179 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 431647 Color: 4
Size: 297181 Color: 0
Size: 271173 Color: 2

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 431729 Color: 1
Size: 313004 Color: 0
Size: 255268 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 432115 Color: 3
Size: 315900 Color: 4
Size: 251986 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 431773 Color: 0
Size: 314820 Color: 4
Size: 253408 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 431882 Color: 2
Size: 308094 Color: 4
Size: 260025 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 432045 Color: 0
Size: 306396 Color: 3
Size: 261560 Color: 2

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 432422 Color: 3
Size: 293982 Color: 2
Size: 273597 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 432461 Color: 4
Size: 292613 Color: 2
Size: 274927 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 432743 Color: 0
Size: 304013 Color: 4
Size: 263245 Color: 3

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 433441 Color: 0
Size: 313878 Color: 1
Size: 252682 Color: 3

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 433716 Color: 2
Size: 299277 Color: 1
Size: 267008 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 434076 Color: 1
Size: 291469 Color: 3
Size: 274456 Color: 2

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 435064 Color: 3
Size: 289617 Color: 2
Size: 275320 Color: 4

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 435305 Color: 3
Size: 291614 Color: 4
Size: 273082 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 435381 Color: 0
Size: 312489 Color: 1
Size: 252131 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 436102 Color: 3
Size: 309106 Color: 0
Size: 254793 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 435619 Color: 0
Size: 293631 Color: 1
Size: 270751 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 435780 Color: 1
Size: 289388 Color: 4
Size: 274833 Color: 3

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 436130 Color: 3
Size: 312961 Color: 1
Size: 250910 Color: 2

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 435961 Color: 1
Size: 285619 Color: 4
Size: 278421 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 436218 Color: 0
Size: 299942 Color: 1
Size: 263841 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 436874 Color: 1
Size: 299977 Color: 0
Size: 263150 Color: 3

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 437076 Color: 1
Size: 289199 Color: 1
Size: 273726 Color: 3

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 437795 Color: 4
Size: 293550 Color: 4
Size: 268656 Color: 2

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 438247 Color: 2
Size: 304629 Color: 3
Size: 257125 Color: 4

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 438548 Color: 0
Size: 290229 Color: 0
Size: 271224 Color: 3

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 438552 Color: 2
Size: 290106 Color: 1
Size: 271343 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 438681 Color: 4
Size: 295711 Color: 0
Size: 265609 Color: 3

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 439090 Color: 4
Size: 309832 Color: 1
Size: 251079 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 439386 Color: 4
Size: 309813 Color: 3
Size: 250802 Color: 2

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 439493 Color: 4
Size: 302436 Color: 1
Size: 258072 Color: 2

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 440294 Color: 3
Size: 300231 Color: 4
Size: 259476 Color: 2

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 439865 Color: 2
Size: 286433 Color: 4
Size: 273703 Color: 2

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 440308 Color: 0
Size: 284974 Color: 3
Size: 274719 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 441603 Color: 1
Size: 279366 Color: 3
Size: 279032 Color: 2

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 441673 Color: 2
Size: 298909 Color: 1
Size: 259419 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 441868 Color: 4
Size: 305051 Color: 3
Size: 253082 Color: 4

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 442172 Color: 2
Size: 285444 Color: 0
Size: 272385 Color: 2

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 442465 Color: 4
Size: 304421 Color: 3
Size: 253115 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 442502 Color: 0
Size: 300108 Color: 0
Size: 257391 Color: 3

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 442514 Color: 1
Size: 299087 Color: 4
Size: 258400 Color: 4

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 442802 Color: 2
Size: 287324 Color: 3
Size: 269875 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 443359 Color: 0
Size: 303182 Color: 2
Size: 253460 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 444234 Color: 4
Size: 305029 Color: 1
Size: 250738 Color: 4

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 444597 Color: 1
Size: 302270 Color: 1
Size: 253134 Color: 3

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 445317 Color: 3
Size: 289256 Color: 2
Size: 265428 Color: 2

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 444678 Color: 0
Size: 292866 Color: 2
Size: 262457 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 446009 Color: 3
Size: 277631 Color: 0
Size: 276361 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 445376 Color: 4
Size: 289077 Color: 2
Size: 265548 Color: 4

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 446946 Color: 3
Size: 294696 Color: 1
Size: 258359 Color: 4

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 445757 Color: 4
Size: 280486 Color: 0
Size: 273758 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 447032 Color: 3
Size: 277458 Color: 1
Size: 275511 Color: 4

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 447451 Color: 1
Size: 278688 Color: 3
Size: 273862 Color: 4

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 447672 Color: 2
Size: 300213 Color: 0
Size: 252116 Color: 2

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 447852 Color: 2
Size: 276642 Color: 1
Size: 275507 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 448232 Color: 1
Size: 296286 Color: 4
Size: 255483 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 448482 Color: 3
Size: 300508 Color: 2
Size: 251011 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 448774 Color: 2
Size: 287904 Color: 1
Size: 263323 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 449267 Color: 2
Size: 294220 Color: 3
Size: 256514 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 449289 Color: 2
Size: 286016 Color: 4
Size: 264696 Color: 2

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 449624 Color: 0
Size: 277078 Color: 1
Size: 273299 Color: 3

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 449808 Color: 0
Size: 295000 Color: 1
Size: 255193 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 450552 Color: 3
Size: 282956 Color: 1
Size: 266493 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 450940 Color: 0
Size: 295215 Color: 3
Size: 253846 Color: 4

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 451143 Color: 2
Size: 285816 Color: 4
Size: 263042 Color: 4

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 451382 Color: 4
Size: 281156 Color: 2
Size: 267463 Color: 3

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 451486 Color: 4
Size: 277625 Color: 1
Size: 270890 Color: 3

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 451701 Color: 1
Size: 285441 Color: 1
Size: 262859 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 452225 Color: 3
Size: 293589 Color: 1
Size: 254187 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 451843 Color: 2
Size: 296051 Color: 4
Size: 252107 Color: 4

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 452421 Color: 3
Size: 288261 Color: 2
Size: 259319 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 452819 Color: 3
Size: 277315 Color: 0
Size: 269867 Color: 2

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 452706 Color: 1
Size: 283973 Color: 2
Size: 263322 Color: 4

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 452973 Color: 4
Size: 288617 Color: 4
Size: 258411 Color: 3

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 453298 Color: 0
Size: 280582 Color: 2
Size: 266121 Color: 3

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 453844 Color: 0
Size: 274946 Color: 2
Size: 271211 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 454098 Color: 2
Size: 289171 Color: 3
Size: 256732 Color: 4

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 454139 Color: 2
Size: 282835 Color: 4
Size: 263027 Color: 4

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 454459 Color: 4
Size: 294168 Color: 3
Size: 251374 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 454660 Color: 3
Size: 285121 Color: 1
Size: 260220 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 454663 Color: 4
Size: 285858 Color: 1
Size: 259480 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 454837 Color: 3
Size: 286644 Color: 2
Size: 258520 Color: 4

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 455245 Color: 1
Size: 288974 Color: 1
Size: 255782 Color: 2

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 455484 Color: 0
Size: 289727 Color: 1
Size: 254790 Color: 3

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 455846 Color: 3
Size: 273475 Color: 0
Size: 270680 Color: 4

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 456254 Color: 4
Size: 286978 Color: 2
Size: 256769 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 456582 Color: 0
Size: 272874 Color: 4
Size: 270545 Color: 3

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 456671 Color: 4
Size: 275852 Color: 2
Size: 267478 Color: 2

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 457264 Color: 3
Size: 292662 Color: 1
Size: 250075 Color: 2

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 457850 Color: 0
Size: 282884 Color: 3
Size: 259267 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 457552 Color: 4
Size: 276847 Color: 1
Size: 265602 Color: 2

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 458406 Color: 1
Size: 278935 Color: 1
Size: 262660 Color: 3

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 458196 Color: 4
Size: 282400 Color: 2
Size: 259405 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 458542 Color: 1
Size: 277221 Color: 3
Size: 264238 Color: 2

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 458569 Color: 0
Size: 278047 Color: 0
Size: 263385 Color: 4

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 459385 Color: 3
Size: 275759 Color: 4
Size: 264857 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 460204 Color: 3
Size: 282503 Color: 1
Size: 257294 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 460266 Color: 0
Size: 271627 Color: 1
Size: 268108 Color: 4

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 460890 Color: 0
Size: 274584 Color: 3
Size: 264527 Color: 2

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 461487 Color: 1
Size: 276102 Color: 1
Size: 262412 Color: 3

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 461684 Color: 1
Size: 288202 Color: 0
Size: 250115 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 461934 Color: 2
Size: 281371 Color: 0
Size: 256696 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 462123 Color: 1
Size: 273279 Color: 0
Size: 264599 Color: 3

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 462763 Color: 0
Size: 270302 Color: 3
Size: 266936 Color: 4

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 462866 Color: 1
Size: 284265 Color: 0
Size: 252870 Color: 3

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 464015 Color: 4
Size: 270997 Color: 0
Size: 264989 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 463721 Color: 1
Size: 278138 Color: 2
Size: 258142 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 463865 Color: 2
Size: 283829 Color: 4
Size: 252307 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 464185 Color: 4
Size: 274396 Color: 2
Size: 261420 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 464036 Color: 1
Size: 282512 Color: 2
Size: 253453 Color: 3

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 464202 Color: 2
Size: 271300 Color: 4
Size: 264499 Color: 2

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 464227 Color: 0
Size: 281240 Color: 3
Size: 254534 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 464992 Color: 4
Size: 279267 Color: 1
Size: 255742 Color: 2

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 466021 Color: 4
Size: 278443 Color: 0
Size: 255537 Color: 1

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 465902 Color: 0
Size: 280200 Color: 1
Size: 253899 Color: 3

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 466901 Color: 1
Size: 266833 Color: 3
Size: 266267 Color: 2

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 467734 Color: 4
Size: 276043 Color: 3
Size: 256224 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 468224 Color: 2
Size: 274401 Color: 0
Size: 257376 Color: 3

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 468750 Color: 4
Size: 268918 Color: 0
Size: 262333 Color: 2

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 468505 Color: 1
Size: 265864 Color: 0
Size: 265632 Color: 3

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 469452 Color: 2
Size: 271365 Color: 2
Size: 259184 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 469501 Color: 3
Size: 273068 Color: 2
Size: 257432 Color: 4

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 470111 Color: 3
Size: 278509 Color: 4
Size: 251381 Color: 3

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 470244 Color: 3
Size: 278257 Color: 1
Size: 251500 Color: 3

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 470379 Color: 4
Size: 279561 Color: 1
Size: 250061 Color: 3

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 470263 Color: 1
Size: 278191 Color: 0
Size: 251547 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 470915 Color: 3
Size: 279053 Color: 1
Size: 250033 Color: 4

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 471661 Color: 2
Size: 266495 Color: 2
Size: 261845 Color: 4

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 472123 Color: 2
Size: 277748 Color: 0
Size: 250130 Color: 3

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 472640 Color: 1
Size: 267384 Color: 4
Size: 259977 Color: 1

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 472862 Color: 0
Size: 273249 Color: 2
Size: 253890 Color: 1

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 473155 Color: 1
Size: 269910 Color: 0
Size: 256936 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 473500 Color: 2
Size: 271937 Color: 4
Size: 254564 Color: 3

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 474982 Color: 2
Size: 271704 Color: 3
Size: 253315 Color: 2

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 476017 Color: 4
Size: 270920 Color: 2
Size: 253064 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 475432 Color: 0
Size: 269728 Color: 0
Size: 254841 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 476513 Color: 4
Size: 268079 Color: 3
Size: 255409 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 477708 Color: 0
Size: 265588 Color: 1
Size: 256705 Color: 3

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 478217 Color: 0
Size: 264873 Color: 4
Size: 256911 Color: 2

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 478645 Color: 2
Size: 268491 Color: 3
Size: 252865 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 478884 Color: 2
Size: 264050 Color: 0
Size: 257067 Color: 4

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 479460 Color: 4
Size: 269871 Color: 2
Size: 250670 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 478977 Color: 1
Size: 263289 Color: 3
Size: 257735 Color: 2

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 479890 Color: 3
Size: 269628 Color: 4
Size: 250483 Color: 2

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 479810 Color: 4
Size: 263657 Color: 0
Size: 256534 Color: 3

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 480085 Color: 0
Size: 262253 Color: 2
Size: 257663 Color: 2

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 480317 Color: 2
Size: 267103 Color: 4
Size: 252581 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 480407 Color: 1
Size: 262337 Color: 2
Size: 257257 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 480660 Color: 2
Size: 266439 Color: 1
Size: 252902 Color: 4

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 480654 Color: 4
Size: 260580 Color: 3
Size: 258767 Color: 2

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 480695 Color: 0
Size: 259888 Color: 2
Size: 259418 Color: 1

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 483129 Color: 4
Size: 258514 Color: 2
Size: 258358 Color: 2

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 483650 Color: 2
Size: 263130 Color: 2
Size: 253221 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 483985 Color: 0
Size: 264713 Color: 4
Size: 251303 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 484078 Color: 4
Size: 263482 Color: 1
Size: 252441 Color: 2

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 484706 Color: 0
Size: 260862 Color: 3
Size: 254433 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 485833 Color: 3
Size: 259709 Color: 4
Size: 254459 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 486233 Color: 3
Size: 258947 Color: 0
Size: 254821 Color: 3

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 486488 Color: 3
Size: 261569 Color: 4
Size: 251944 Color: 3

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 486747 Color: 1
Size: 260918 Color: 1
Size: 252336 Color: 2

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 487613 Color: 4
Size: 260664 Color: 3
Size: 251724 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 487643 Color: 2
Size: 260708 Color: 1
Size: 251650 Color: 3

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 487777 Color: 2
Size: 258036 Color: 4
Size: 254188 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 487988 Color: 0
Size: 256386 Color: 3
Size: 255627 Color: 3

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 488324 Color: 4
Size: 256341 Color: 0
Size: 255336 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 488543 Color: 3
Size: 258553 Color: 4
Size: 252905 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 488645 Color: 1
Size: 258905 Color: 1
Size: 252451 Color: 2

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 488872 Color: 3
Size: 258750 Color: 3
Size: 252379 Color: 4

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 489166 Color: 3
Size: 260008 Color: 2
Size: 250827 Color: 2

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 490100 Color: 2
Size: 255508 Color: 1
Size: 254393 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 491148 Color: 1
Size: 258538 Color: 4
Size: 250315 Color: 2

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 491239 Color: 4
Size: 255626 Color: 0
Size: 253136 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 492363 Color: 4
Size: 254802 Color: 3
Size: 252836 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 493577 Color: 2
Size: 255515 Color: 2
Size: 250909 Color: 1

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 495008 Color: 4
Size: 253650 Color: 0
Size: 251343 Color: 2

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 495269 Color: 4
Size: 253835 Color: 2
Size: 250897 Color: 2

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 495719 Color: 3
Size: 253176 Color: 0
Size: 251106 Color: 2

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 495931 Color: 1
Size: 253988 Color: 0
Size: 250082 Color: 4

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 497275 Color: 3
Size: 251823 Color: 1
Size: 250903 Color: 2

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 498706 Color: 4
Size: 251075 Color: 0
Size: 250220 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 499975 Color: 3
Size: 250013 Color: 4
Size: 250013 Color: 0

Bin 334: 1 of cap free
Amount of items: 3
Items: 
Size: 363835 Color: 3
Size: 320459 Color: 3
Size: 315706 Color: 1

Bin 335: 1 of cap free
Amount of items: 3
Items: 
Size: 364979 Color: 4
Size: 341732 Color: 2
Size: 293289 Color: 3

Bin 336: 1 of cap free
Amount of items: 3
Items: 
Size: 366646 Color: 2
Size: 330562 Color: 1
Size: 302792 Color: 2

Bin 337: 1 of cap free
Amount of items: 3
Items: 
Size: 370451 Color: 1
Size: 325199 Color: 2
Size: 304350 Color: 0

Bin 338: 1 of cap free
Amount of items: 3
Items: 
Size: 375759 Color: 0
Size: 361660 Color: 0
Size: 262581 Color: 4

Bin 339: 1 of cap free
Amount of items: 3
Items: 
Size: 376575 Color: 1
Size: 318731 Color: 0
Size: 304694 Color: 2

Bin 340: 1 of cap free
Amount of items: 3
Items: 
Size: 376940 Color: 1
Size: 352893 Color: 1
Size: 270167 Color: 3

Bin 341: 1 of cap free
Amount of items: 3
Items: 
Size: 377999 Color: 1
Size: 312833 Color: 4
Size: 309168 Color: 3

Bin 342: 1 of cap free
Amount of items: 3
Items: 
Size: 378162 Color: 1
Size: 334756 Color: 4
Size: 287082 Color: 3

Bin 343: 1 of cap free
Amount of items: 3
Items: 
Size: 378951 Color: 4
Size: 369403 Color: 0
Size: 251646 Color: 4

Bin 344: 1 of cap free
Amount of items: 3
Items: 
Size: 379475 Color: 4
Size: 350771 Color: 4
Size: 269754 Color: 0

Bin 345: 1 of cap free
Amount of items: 3
Items: 
Size: 379495 Color: 4
Size: 357743 Color: 4
Size: 262762 Color: 3

Bin 346: 1 of cap free
Amount of items: 3
Items: 
Size: 380976 Color: 2
Size: 310892 Color: 4
Size: 308132 Color: 2

Bin 347: 1 of cap free
Amount of items: 3
Items: 
Size: 383562 Color: 1
Size: 345204 Color: 1
Size: 271234 Color: 4

Bin 348: 1 of cap free
Amount of items: 3
Items: 
Size: 384363 Color: 4
Size: 332205 Color: 0
Size: 283432 Color: 2

Bin 349: 1 of cap free
Amount of items: 3
Items: 
Size: 384981 Color: 1
Size: 349318 Color: 3
Size: 265701 Color: 2

Bin 350: 1 of cap free
Amount of items: 3
Items: 
Size: 388500 Color: 4
Size: 345469 Color: 1
Size: 266031 Color: 3

Bin 351: 1 of cap free
Amount of items: 3
Items: 
Size: 390409 Color: 2
Size: 323570 Color: 0
Size: 286021 Color: 4

Bin 352: 1 of cap free
Amount of items: 3
Items: 
Size: 390531 Color: 1
Size: 332906 Color: 3
Size: 276563 Color: 3

Bin 353: 1 of cap free
Amount of items: 3
Items: 
Size: 391205 Color: 4
Size: 309006 Color: 2
Size: 299789 Color: 0

Bin 354: 1 of cap free
Amount of items: 3
Items: 
Size: 391310 Color: 3
Size: 346475 Color: 4
Size: 262215 Color: 0

Bin 355: 1 of cap free
Amount of items: 3
Items: 
Size: 391405 Color: 4
Size: 354336 Color: 3
Size: 254259 Color: 1

Bin 356: 1 of cap free
Amount of items: 3
Items: 
Size: 392404 Color: 1
Size: 325567 Color: 4
Size: 282029 Color: 4

Bin 357: 1 of cap free
Amount of items: 3
Items: 
Size: 393949 Color: 1
Size: 321297 Color: 4
Size: 284754 Color: 2

Bin 358: 1 of cap free
Amount of items: 3
Items: 
Size: 394876 Color: 1
Size: 330698 Color: 3
Size: 274426 Color: 4

Bin 359: 1 of cap free
Amount of items: 3
Items: 
Size: 398714 Color: 4
Size: 321552 Color: 3
Size: 279734 Color: 1

Bin 360: 1 of cap free
Amount of items: 3
Items: 
Size: 401091 Color: 0
Size: 310506 Color: 2
Size: 288403 Color: 1

Bin 361: 1 of cap free
Amount of items: 3
Items: 
Size: 402166 Color: 0
Size: 314387 Color: 4
Size: 283447 Color: 0

Bin 362: 1 of cap free
Amount of items: 3
Items: 
Size: 401850 Color: 4
Size: 302701 Color: 1
Size: 295449 Color: 3

Bin 363: 1 of cap free
Amount of items: 3
Items: 
Size: 403976 Color: 4
Size: 340771 Color: 2
Size: 255253 Color: 3

Bin 364: 1 of cap free
Amount of items: 3
Items: 
Size: 405302 Color: 2
Size: 300509 Color: 0
Size: 294189 Color: 3

Bin 365: 1 of cap free
Amount of items: 3
Items: 
Size: 405376 Color: 2
Size: 299458 Color: 1
Size: 295166 Color: 2

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 404606 Color: 3
Size: 302683 Color: 0
Size: 292711 Color: 1

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 407015 Color: 2
Size: 310713 Color: 0
Size: 282272 Color: 1

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 410742 Color: 2
Size: 304560 Color: 1
Size: 284698 Color: 2

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 411325 Color: 3
Size: 329654 Color: 2
Size: 259021 Color: 0

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 412383 Color: 3
Size: 324299 Color: 1
Size: 263318 Color: 3

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 414019 Color: 3
Size: 327162 Color: 2
Size: 258819 Color: 0

Bin 372: 1 of cap free
Amount of items: 3
Items: 
Size: 414857 Color: 2
Size: 305127 Color: 1
Size: 280016 Color: 3

Bin 373: 1 of cap free
Amount of items: 3
Items: 
Size: 414927 Color: 2
Size: 324820 Color: 1
Size: 260253 Color: 0

Bin 374: 1 of cap free
Amount of items: 3
Items: 
Size: 415050 Color: 3
Size: 310279 Color: 0
Size: 274671 Color: 1

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 415583 Color: 3
Size: 304882 Color: 0
Size: 279535 Color: 3

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 416701 Color: 0
Size: 328839 Color: 1
Size: 254460 Color: 3

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 418006 Color: 3
Size: 319098 Color: 2
Size: 262896 Color: 0

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 418904 Color: 1
Size: 325953 Color: 4
Size: 255143 Color: 3

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 419350 Color: 1
Size: 325336 Color: 4
Size: 255314 Color: 0

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 420088 Color: 2
Size: 327189 Color: 1
Size: 252723 Color: 3

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 422918 Color: 4
Size: 322638 Color: 0
Size: 254444 Color: 3

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 423660 Color: 2
Size: 308137 Color: 4
Size: 268203 Color: 0

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 424285 Color: 1
Size: 300096 Color: 0
Size: 275619 Color: 2

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 426210 Color: 2
Size: 310144 Color: 1
Size: 263646 Color: 3

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 426863 Color: 0
Size: 308626 Color: 0
Size: 264511 Color: 1

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 426891 Color: 4
Size: 291863 Color: 4
Size: 281246 Color: 1

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 426958 Color: 3
Size: 319902 Color: 1
Size: 253140 Color: 2

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 430110 Color: 2
Size: 286991 Color: 0
Size: 282899 Color: 1

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 430611 Color: 3
Size: 288200 Color: 3
Size: 281189 Color: 0

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 432264 Color: 3
Size: 317547 Color: 2
Size: 250189 Color: 4

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 432291 Color: 0
Size: 289197 Color: 0
Size: 278512 Color: 4

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 432497 Color: 3
Size: 312353 Color: 2
Size: 255150 Color: 1

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 432842 Color: 1
Size: 307633 Color: 4
Size: 259525 Color: 3

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 432937 Color: 0
Size: 304438 Color: 4
Size: 262625 Color: 4

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 433197 Color: 2
Size: 306072 Color: 4
Size: 260731 Color: 3

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 434394 Color: 0
Size: 311883 Color: 2
Size: 253723 Color: 1

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 435995 Color: 1
Size: 283873 Color: 3
Size: 280132 Color: 4

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 440607 Color: 3
Size: 303020 Color: 0
Size: 256373 Color: 0

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 441031 Color: 2
Size: 283741 Color: 3
Size: 275228 Color: 4

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 441878 Color: 3
Size: 306140 Color: 1
Size: 251982 Color: 0

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 442656 Color: 4
Size: 292012 Color: 3
Size: 265332 Color: 2

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 442760 Color: 4
Size: 291292 Color: 2
Size: 265948 Color: 0

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 444588 Color: 3
Size: 304383 Color: 4
Size: 251029 Color: 2

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 447596 Color: 0
Size: 278781 Color: 0
Size: 273623 Color: 3

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 447709 Color: 4
Size: 288816 Color: 4
Size: 263475 Color: 3

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 448050 Color: 1
Size: 293500 Color: 3
Size: 258450 Color: 4

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 448125 Color: 4
Size: 293530 Color: 4
Size: 258345 Color: 3

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 448383 Color: 1
Size: 295376 Color: 2
Size: 256241 Color: 3

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 457529 Color: 0
Size: 283528 Color: 4
Size: 258943 Color: 3

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 459512 Color: 2
Size: 272837 Color: 0
Size: 267651 Color: 3

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 461311 Color: 0
Size: 286809 Color: 4
Size: 251880 Color: 3

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 461841 Color: 0
Size: 287243 Color: 3
Size: 250916 Color: 4

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 462798 Color: 0
Size: 271174 Color: 2
Size: 266028 Color: 0

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 462952 Color: 4
Size: 284948 Color: 0
Size: 252100 Color: 4

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 463364 Color: 0
Size: 280066 Color: 4
Size: 256570 Color: 2

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 467739 Color: 1
Size: 280789 Color: 4
Size: 251472 Color: 3

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 468378 Color: 2
Size: 279001 Color: 2
Size: 252621 Color: 4

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 473196 Color: 3
Size: 274561 Color: 3
Size: 252243 Color: 4

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 486615 Color: 1
Size: 258329 Color: 1
Size: 255056 Color: 4

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 487164 Color: 3
Size: 262127 Color: 0
Size: 250709 Color: 4

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 492870 Color: 1
Size: 254648 Color: 0
Size: 252482 Color: 3

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 493955 Color: 1
Size: 254865 Color: 4
Size: 251180 Color: 2

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 494268 Color: 1
Size: 253192 Color: 2
Size: 252540 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 494726 Color: 0
Size: 254931 Color: 3
Size: 250343 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 376703 Color: 2
Size: 326964 Color: 2
Size: 296333 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 341924 Color: 4
Size: 340793 Color: 2
Size: 317283 Color: 2

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 417302 Color: 2
Size: 329539 Color: 3
Size: 253159 Color: 4

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 349528 Color: 3
Size: 338517 Color: 2
Size: 311955 Color: 1

Bin 429: 2 of cap free
Amount of items: 3
Items: 
Size: 366280 Color: 3
Size: 352157 Color: 4
Size: 281562 Color: 0

Bin 430: 2 of cap free
Amount of items: 3
Items: 
Size: 365505 Color: 4
Size: 334283 Color: 2
Size: 300211 Color: 4

Bin 431: 2 of cap free
Amount of items: 3
Items: 
Size: 368415 Color: 0
Size: 341342 Color: 3
Size: 290242 Color: 0

Bin 432: 2 of cap free
Amount of items: 3
Items: 
Size: 369240 Color: 2
Size: 357223 Color: 0
Size: 273536 Color: 2

Bin 433: 2 of cap free
Amount of items: 3
Items: 
Size: 372013 Color: 2
Size: 331773 Color: 2
Size: 296213 Color: 1

Bin 434: 2 of cap free
Amount of items: 3
Items: 
Size: 372670 Color: 2
Size: 343304 Color: 0
Size: 284025 Color: 3

Bin 435: 2 of cap free
Amount of items: 3
Items: 
Size: 375190 Color: 0
Size: 329281 Color: 4
Size: 295528 Color: 3

Bin 436: 2 of cap free
Amount of items: 3
Items: 
Size: 376035 Color: 0
Size: 332602 Color: 2
Size: 291362 Color: 3

Bin 437: 2 of cap free
Amount of items: 3
Items: 
Size: 376119 Color: 3
Size: 324017 Color: 0
Size: 299863 Color: 4

Bin 438: 2 of cap free
Amount of items: 3
Items: 
Size: 377388 Color: 3
Size: 370366 Color: 1
Size: 252245 Color: 4

Bin 439: 2 of cap free
Amount of items: 3
Items: 
Size: 381290 Color: 3
Size: 365469 Color: 0
Size: 253240 Color: 1

Bin 440: 2 of cap free
Amount of items: 3
Items: 
Size: 386538 Color: 1
Size: 324025 Color: 3
Size: 289436 Color: 4

Bin 441: 2 of cap free
Amount of items: 3
Items: 
Size: 386725 Color: 1
Size: 350976 Color: 2
Size: 262298 Color: 4

Bin 442: 2 of cap free
Amount of items: 3
Items: 
Size: 387559 Color: 3
Size: 307729 Color: 2
Size: 304711 Color: 1

Bin 443: 2 of cap free
Amount of items: 3
Items: 
Size: 388879 Color: 1
Size: 357154 Color: 2
Size: 253966 Color: 0

Bin 444: 2 of cap free
Amount of items: 3
Items: 
Size: 389064 Color: 4
Size: 320850 Color: 1
Size: 290085 Color: 4

Bin 445: 2 of cap free
Amount of items: 3
Items: 
Size: 389168 Color: 3
Size: 344287 Color: 4
Size: 266544 Color: 4

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 392194 Color: 0
Size: 327763 Color: 2
Size: 280042 Color: 2

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 392619 Color: 4
Size: 354076 Color: 3
Size: 253304 Color: 1

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 394429 Color: 3
Size: 340428 Color: 1
Size: 265142 Color: 4

Bin 449: 2 of cap free
Amount of items: 3
Items: 
Size: 397446 Color: 3
Size: 317850 Color: 1
Size: 284703 Color: 3

Bin 450: 2 of cap free
Amount of items: 3
Items: 
Size: 400033 Color: 2
Size: 338877 Color: 4
Size: 261089 Color: 3

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 398926 Color: 4
Size: 305047 Color: 0
Size: 296026 Color: 3

Bin 452: 2 of cap free
Amount of items: 3
Items: 
Size: 401663 Color: 2
Size: 315356 Color: 4
Size: 282980 Color: 3

Bin 453: 2 of cap free
Amount of items: 3
Items: 
Size: 404279 Color: 4
Size: 327255 Color: 1
Size: 268465 Color: 3

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 405215 Color: 1
Size: 341203 Color: 3
Size: 253581 Color: 1

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 404430 Color: 3
Size: 325076 Color: 0
Size: 270493 Color: 1

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 404932 Color: 3
Size: 312796 Color: 2
Size: 282271 Color: 4

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 406569 Color: 4
Size: 311559 Color: 3
Size: 281871 Color: 2

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 410656 Color: 3
Size: 320714 Color: 0
Size: 268629 Color: 1

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 410980 Color: 3
Size: 327261 Color: 4
Size: 261758 Color: 1

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 411909 Color: 3
Size: 323363 Color: 4
Size: 264727 Color: 0

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 412087 Color: 4
Size: 307093 Color: 3
Size: 280819 Color: 2

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 412617 Color: 0
Size: 323731 Color: 0
Size: 263651 Color: 1

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 412958 Color: 0
Size: 332721 Color: 4
Size: 254320 Color: 3

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 413439 Color: 1
Size: 327556 Color: 3
Size: 259004 Color: 4

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 414336 Color: 0
Size: 331683 Color: 1
Size: 253980 Color: 2

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 417661 Color: 3
Size: 319027 Color: 1
Size: 263311 Color: 3

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 418832 Color: 3
Size: 330857 Color: 0
Size: 250310 Color: 2

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 427279 Color: 4
Size: 319855 Color: 1
Size: 252865 Color: 3

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 430765 Color: 3
Size: 304877 Color: 0
Size: 264357 Color: 1

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 431568 Color: 1
Size: 297590 Color: 3
Size: 270841 Color: 2

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 443066 Color: 0
Size: 289258 Color: 1
Size: 267675 Color: 3

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 457922 Color: 0
Size: 288362 Color: 4
Size: 253715 Color: 4

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 457995 Color: 1
Size: 273070 Color: 1
Size: 268934 Color: 3

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 469377 Color: 1
Size: 279455 Color: 4
Size: 251167 Color: 0

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 473126 Color: 3
Size: 269927 Color: 2
Size: 256946 Color: 4

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 475994 Color: 0
Size: 268119 Color: 4
Size: 255886 Color: 3

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 490097 Color: 0
Size: 255390 Color: 0
Size: 254512 Color: 4

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 491753 Color: 1
Size: 257519 Color: 2
Size: 250727 Color: 0

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 493094 Color: 0
Size: 253457 Color: 4
Size: 253448 Color: 0

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 494951 Color: 3
Size: 254615 Color: 4
Size: 250433 Color: 3

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 496730 Color: 2
Size: 251875 Color: 4
Size: 251394 Color: 3

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 369824 Color: 1
Size: 336900 Color: 1
Size: 293275 Color: 0

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 375770 Color: 2
Size: 355408 Color: 0
Size: 268821 Color: 3

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 365840 Color: 3
Size: 321154 Color: 3
Size: 313005 Color: 1

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 352536 Color: 0
Size: 350472 Color: 2
Size: 296991 Color: 2

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 343815 Color: 0
Size: 342829 Color: 3
Size: 313355 Color: 3

Bin 487: 3 of cap free
Amount of items: 3
Items: 
Size: 366562 Color: 2
Size: 342898 Color: 3
Size: 290538 Color: 1

Bin 488: 3 of cap free
Amount of items: 3
Items: 
Size: 367576 Color: 4
Size: 328833 Color: 0
Size: 303589 Color: 1

Bin 489: 3 of cap free
Amount of items: 3
Items: 
Size: 372287 Color: 1
Size: 351406 Color: 0
Size: 276305 Color: 2

Bin 490: 3 of cap free
Amount of items: 3
Items: 
Size: 372620 Color: 1
Size: 341107 Color: 2
Size: 286271 Color: 3

Bin 491: 3 of cap free
Amount of items: 3
Items: 
Size: 375888 Color: 3
Size: 338866 Color: 1
Size: 285244 Color: 3

Bin 492: 3 of cap free
Amount of items: 3
Items: 
Size: 378328 Color: 1
Size: 342116 Color: 4
Size: 279554 Color: 3

Bin 493: 3 of cap free
Amount of items: 3
Items: 
Size: 380563 Color: 2
Size: 350196 Color: 1
Size: 269239 Color: 3

Bin 494: 3 of cap free
Amount of items: 3
Items: 
Size: 381486 Color: 1
Size: 331012 Color: 2
Size: 287500 Color: 0

Bin 495: 3 of cap free
Amount of items: 3
Items: 
Size: 383981 Color: 1
Size: 341338 Color: 2
Size: 274679 Color: 3

Bin 496: 3 of cap free
Amount of items: 3
Items: 
Size: 389581 Color: 4
Size: 315609 Color: 1
Size: 294808 Color: 2

Bin 497: 3 of cap free
Amount of items: 3
Items: 
Size: 393697 Color: 4
Size: 307715 Color: 4
Size: 298586 Color: 1

Bin 498: 3 of cap free
Amount of items: 3
Items: 
Size: 403470 Color: 4
Size: 322305 Color: 0
Size: 274223 Color: 3

Bin 499: 3 of cap free
Amount of items: 3
Items: 
Size: 407346 Color: 0
Size: 312788 Color: 0
Size: 279864 Color: 4

Bin 500: 3 of cap free
Amount of items: 3
Items: 
Size: 409146 Color: 1
Size: 329401 Color: 0
Size: 261451 Color: 3

Bin 501: 3 of cap free
Amount of items: 3
Items: 
Size: 420459 Color: 2
Size: 319787 Color: 4
Size: 259752 Color: 1

Bin 502: 3 of cap free
Amount of items: 3
Items: 
Size: 423709 Color: 2
Size: 292898 Color: 0
Size: 283391 Color: 1

Bin 503: 3 of cap free
Amount of items: 3
Items: 
Size: 427979 Color: 2
Size: 317964 Color: 3
Size: 254055 Color: 1

Bin 504: 3 of cap free
Amount of items: 3
Items: 
Size: 431549 Color: 4
Size: 285082 Color: 4
Size: 283367 Color: 2

Bin 505: 3 of cap free
Amount of items: 3
Items: 
Size: 441497 Color: 4
Size: 287855 Color: 4
Size: 270646 Color: 1

Bin 506: 3 of cap free
Amount of items: 3
Items: 
Size: 472880 Color: 2
Size: 265286 Color: 0
Size: 261832 Color: 4

Bin 507: 3 of cap free
Amount of items: 3
Items: 
Size: 493417 Color: 1
Size: 254956 Color: 3
Size: 251625 Color: 4

Bin 508: 3 of cap free
Amount of items: 3
Items: 
Size: 362036 Color: 3
Size: 320053 Color: 0
Size: 317909 Color: 2

Bin 509: 3 of cap free
Amount of items: 3
Items: 
Size: 357025 Color: 1
Size: 330736 Color: 2
Size: 312237 Color: 0

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 337396 Color: 2
Size: 332834 Color: 1
Size: 329768 Color: 3

Bin 511: 4 of cap free
Amount of items: 3
Items: 
Size: 387297 Color: 2
Size: 333506 Color: 0
Size: 279194 Color: 0

Bin 512: 4 of cap free
Amount of items: 3
Items: 
Size: 392082 Color: 1
Size: 314641 Color: 0
Size: 293274 Color: 3

Bin 513: 4 of cap free
Amount of items: 3
Items: 
Size: 362271 Color: 2
Size: 351873 Color: 0
Size: 285853 Color: 4

Bin 514: 4 of cap free
Amount of items: 3
Items: 
Size: 360297 Color: 4
Size: 355269 Color: 1
Size: 284431 Color: 0

Bin 515: 4 of cap free
Amount of items: 3
Items: 
Size: 363934 Color: 1
Size: 355308 Color: 1
Size: 280755 Color: 2

Bin 516: 4 of cap free
Amount of items: 3
Items: 
Size: 368289 Color: 1
Size: 362058 Color: 4
Size: 269650 Color: 2

Bin 517: 4 of cap free
Amount of items: 3
Items: 
Size: 370997 Color: 3
Size: 336108 Color: 4
Size: 292892 Color: 3

Bin 518: 4 of cap free
Amount of items: 3
Items: 
Size: 371105 Color: 1
Size: 333715 Color: 4
Size: 295177 Color: 0

Bin 519: 4 of cap free
Amount of items: 3
Items: 
Size: 371495 Color: 3
Size: 334914 Color: 4
Size: 293588 Color: 1

Bin 520: 4 of cap free
Amount of items: 3
Items: 
Size: 371766 Color: 3
Size: 331974 Color: 2
Size: 296257 Color: 3

Bin 521: 4 of cap free
Amount of items: 3
Items: 
Size: 372253 Color: 4
Size: 328912 Color: 3
Size: 298832 Color: 4

Bin 522: 4 of cap free
Amount of items: 3
Items: 
Size: 372472 Color: 1
Size: 321544 Color: 3
Size: 305981 Color: 4

Bin 523: 4 of cap free
Amount of items: 3
Items: 
Size: 374491 Color: 4
Size: 330925 Color: 4
Size: 294581 Color: 1

Bin 524: 4 of cap free
Amount of items: 3
Items: 
Size: 379912 Color: 0
Size: 334751 Color: 0
Size: 285334 Color: 1

Bin 525: 4 of cap free
Amount of items: 3
Items: 
Size: 388133 Color: 2
Size: 335143 Color: 3
Size: 276721 Color: 1

Bin 526: 4 of cap free
Amount of items: 3
Items: 
Size: 391280 Color: 1
Size: 325531 Color: 2
Size: 283186 Color: 0

Bin 527: 4 of cap free
Amount of items: 3
Items: 
Size: 401418 Color: 0
Size: 334444 Color: 3
Size: 264135 Color: 1

Bin 528: 4 of cap free
Amount of items: 3
Items: 
Size: 405570 Color: 2
Size: 322317 Color: 3
Size: 272110 Color: 0

Bin 529: 4 of cap free
Amount of items: 3
Items: 
Size: 409719 Color: 3
Size: 328070 Color: 0
Size: 262208 Color: 1

Bin 530: 4 of cap free
Amount of items: 3
Items: 
Size: 418270 Color: 2
Size: 320122 Color: 0
Size: 261605 Color: 3

Bin 531: 4 of cap free
Amount of items: 3
Items: 
Size: 420579 Color: 2
Size: 301975 Color: 2
Size: 277443 Color: 4

Bin 532: 4 of cap free
Amount of items: 3
Items: 
Size: 427797 Color: 3
Size: 305118 Color: 1
Size: 267082 Color: 0

Bin 533: 4 of cap free
Amount of items: 3
Items: 
Size: 489926 Color: 2
Size: 256555 Color: 4
Size: 253516 Color: 0

Bin 534: 5 of cap free
Amount of items: 3
Items: 
Size: 362303 Color: 2
Size: 331229 Color: 4
Size: 306464 Color: 0

Bin 535: 5 of cap free
Amount of items: 3
Items: 
Size: 372810 Color: 3
Size: 353680 Color: 1
Size: 273506 Color: 4

Bin 536: 5 of cap free
Amount of items: 3
Items: 
Size: 376618 Color: 1
Size: 367736 Color: 2
Size: 255642 Color: 2

Bin 537: 5 of cap free
Amount of items: 3
Items: 
Size: 381420 Color: 0
Size: 321073 Color: 3
Size: 297503 Color: 4

Bin 538: 5 of cap free
Amount of items: 3
Items: 
Size: 390678 Color: 3
Size: 326472 Color: 2
Size: 282846 Color: 2

Bin 539: 5 of cap free
Amount of items: 3
Items: 
Size: 395138 Color: 3
Size: 313950 Color: 2
Size: 290908 Color: 1

Bin 540: 5 of cap free
Amount of items: 3
Items: 
Size: 400307 Color: 2
Size: 332284 Color: 2
Size: 267405 Color: 1

Bin 541: 5 of cap free
Amount of items: 3
Items: 
Size: 404701 Color: 2
Size: 305193 Color: 3
Size: 290102 Color: 4

Bin 542: 5 of cap free
Amount of items: 3
Items: 
Size: 410821 Color: 4
Size: 299258 Color: 1
Size: 289917 Color: 3

Bin 543: 5 of cap free
Amount of items: 3
Items: 
Size: 420681 Color: 3
Size: 293067 Color: 3
Size: 286248 Color: 1

Bin 544: 5 of cap free
Amount of items: 3
Items: 
Size: 494909 Color: 4
Size: 253673 Color: 3
Size: 251414 Color: 2

Bin 545: 6 of cap free
Amount of items: 3
Items: 
Size: 378317 Color: 2
Size: 325347 Color: 1
Size: 296331 Color: 3

Bin 546: 6 of cap free
Amount of items: 3
Items: 
Size: 358624 Color: 3
Size: 356226 Color: 0
Size: 285145 Color: 4

Bin 547: 6 of cap free
Amount of items: 3
Items: 
Size: 364241 Color: 1
Size: 341572 Color: 1
Size: 294182 Color: 2

Bin 548: 6 of cap free
Amount of items: 3
Items: 
Size: 371356 Color: 2
Size: 318044 Color: 3
Size: 310595 Color: 3

Bin 549: 6 of cap free
Amount of items: 3
Items: 
Size: 377008 Color: 2
Size: 320790 Color: 4
Size: 302197 Color: 0

Bin 550: 6 of cap free
Amount of items: 3
Items: 
Size: 378677 Color: 0
Size: 347270 Color: 0
Size: 274048 Color: 2

Bin 551: 6 of cap free
Amount of items: 3
Items: 
Size: 378693 Color: 0
Size: 358792 Color: 1
Size: 262510 Color: 0

Bin 552: 6 of cap free
Amount of items: 3
Items: 
Size: 385706 Color: 2
Size: 338944 Color: 4
Size: 275345 Color: 0

Bin 553: 6 of cap free
Amount of items: 3
Items: 
Size: 398980 Color: 0
Size: 331722 Color: 4
Size: 269293 Color: 3

Bin 554: 6 of cap free
Amount of items: 3
Items: 
Size: 405446 Color: 3
Size: 306795 Color: 1
Size: 287754 Color: 3

Bin 555: 6 of cap free
Amount of items: 3
Items: 
Size: 360033 Color: 3
Size: 346217 Color: 1
Size: 293745 Color: 2

Bin 556: 6 of cap free
Amount of items: 3
Items: 
Size: 346407 Color: 1
Size: 343629 Color: 4
Size: 309959 Color: 2

Bin 557: 7 of cap free
Amount of items: 3
Items: 
Size: 358902 Color: 3
Size: 330622 Color: 1
Size: 310470 Color: 1

Bin 558: 7 of cap free
Amount of items: 3
Items: 
Size: 364122 Color: 0
Size: 340202 Color: 3
Size: 295670 Color: 1

Bin 559: 7 of cap free
Amount of items: 3
Items: 
Size: 363976 Color: 3
Size: 340473 Color: 2
Size: 295545 Color: 4

Bin 560: 7 of cap free
Amount of items: 3
Items: 
Size: 381320 Color: 2
Size: 355851 Color: 4
Size: 262823 Color: 2

Bin 561: 7 of cap free
Amount of items: 3
Items: 
Size: 397384 Color: 1
Size: 347151 Color: 4
Size: 255459 Color: 0

Bin 562: 7 of cap free
Amount of items: 3
Items: 
Size: 408262 Color: 4
Size: 330218 Color: 1
Size: 261514 Color: 4

Bin 563: 7 of cap free
Amount of items: 3
Items: 
Size: 409343 Color: 4
Size: 307142 Color: 2
Size: 283509 Color: 4

Bin 564: 7 of cap free
Amount of items: 3
Items: 
Size: 419071 Color: 2
Size: 307918 Color: 4
Size: 273005 Color: 1

Bin 565: 7 of cap free
Amount of items: 3
Items: 
Size: 371595 Color: 0
Size: 360516 Color: 4
Size: 267883 Color: 2

Bin 566: 8 of cap free
Amount of items: 3
Items: 
Size: 355269 Color: 3
Size: 324469 Color: 2
Size: 320255 Color: 3

Bin 567: 8 of cap free
Amount of items: 3
Items: 
Size: 367233 Color: 1
Size: 354837 Color: 3
Size: 277923 Color: 0

Bin 568: 8 of cap free
Amount of items: 3
Items: 
Size: 367145 Color: 0
Size: 348651 Color: 2
Size: 284197 Color: 4

Bin 569: 8 of cap free
Amount of items: 3
Items: 
Size: 367664 Color: 0
Size: 352325 Color: 4
Size: 280004 Color: 3

Bin 570: 8 of cap free
Amount of items: 3
Items: 
Size: 375036 Color: 3
Size: 344926 Color: 0
Size: 280031 Color: 0

Bin 571: 8 of cap free
Amount of items: 3
Items: 
Size: 380000 Color: 3
Size: 310988 Color: 0
Size: 309005 Color: 2

Bin 572: 8 of cap free
Amount of items: 3
Items: 
Size: 389776 Color: 2
Size: 322196 Color: 1
Size: 288021 Color: 2

Bin 573: 8 of cap free
Amount of items: 3
Items: 
Size: 402497 Color: 0
Size: 342747 Color: 0
Size: 254749 Color: 1

Bin 574: 8 of cap free
Amount of items: 3
Items: 
Size: 365600 Color: 2
Size: 363344 Color: 3
Size: 271049 Color: 2

Bin 575: 9 of cap free
Amount of items: 3
Items: 
Size: 360888 Color: 0
Size: 357276 Color: 3
Size: 281828 Color: 4

Bin 576: 9 of cap free
Amount of items: 3
Items: 
Size: 357774 Color: 4
Size: 352728 Color: 0
Size: 289490 Color: 2

Bin 577: 9 of cap free
Amount of items: 3
Items: 
Size: 371221 Color: 1
Size: 332090 Color: 1
Size: 296681 Color: 0

Bin 578: 9 of cap free
Amount of items: 3
Items: 
Size: 390448 Color: 3
Size: 307320 Color: 0
Size: 302224 Color: 1

Bin 579: 9 of cap free
Amount of items: 3
Items: 
Size: 359278 Color: 1
Size: 328610 Color: 2
Size: 312104 Color: 2

Bin 580: 10 of cap free
Amount of items: 3
Items: 
Size: 361527 Color: 0
Size: 338124 Color: 3
Size: 300340 Color: 2

Bin 581: 10 of cap free
Amount of items: 3
Items: 
Size: 360297 Color: 4
Size: 323807 Color: 1
Size: 315887 Color: 1

Bin 582: 10 of cap free
Amount of items: 3
Items: 
Size: 365598 Color: 3
Size: 349040 Color: 0
Size: 285353 Color: 3

Bin 583: 10 of cap free
Amount of items: 3
Items: 
Size: 371664 Color: 1
Size: 353506 Color: 2
Size: 274821 Color: 4

Bin 584: 10 of cap free
Amount of items: 3
Items: 
Size: 376127 Color: 1
Size: 342938 Color: 2
Size: 280926 Color: 0

Bin 585: 10 of cap free
Amount of items: 3
Items: 
Size: 344820 Color: 0
Size: 328884 Color: 4
Size: 326287 Color: 2

Bin 586: 11 of cap free
Amount of items: 3
Items: 
Size: 365108 Color: 4
Size: 363580 Color: 2
Size: 271302 Color: 2

Bin 587: 11 of cap free
Amount of items: 3
Items: 
Size: 366073 Color: 1
Size: 357329 Color: 3
Size: 276588 Color: 4

Bin 588: 11 of cap free
Amount of items: 3
Items: 
Size: 363480 Color: 1
Size: 337331 Color: 4
Size: 299179 Color: 0

Bin 589: 12 of cap free
Amount of items: 3
Items: 
Size: 365168 Color: 4
Size: 359105 Color: 3
Size: 275716 Color: 3

Bin 590: 12 of cap free
Amount of items: 3
Items: 
Size: 367459 Color: 1
Size: 361912 Color: 3
Size: 270618 Color: 4

Bin 591: 12 of cap free
Amount of items: 3
Items: 
Size: 379291 Color: 3
Size: 356396 Color: 1
Size: 264302 Color: 0

Bin 592: 12 of cap free
Amount of items: 3
Items: 
Size: 363192 Color: 1
Size: 335666 Color: 1
Size: 301131 Color: 2

Bin 593: 12 of cap free
Amount of items: 3
Items: 
Size: 346977 Color: 0
Size: 346892 Color: 4
Size: 306120 Color: 1

Bin 594: 13 of cap free
Amount of items: 3
Items: 
Size: 380176 Color: 2
Size: 357015 Color: 3
Size: 262797 Color: 0

Bin 595: 13 of cap free
Amount of items: 3
Items: 
Size: 367198 Color: 1
Size: 341451 Color: 3
Size: 291339 Color: 0

Bin 596: 13 of cap free
Amount of items: 3
Items: 
Size: 367710 Color: 0
Size: 333779 Color: 2
Size: 298499 Color: 3

Bin 597: 13 of cap free
Amount of items: 3
Items: 
Size: 369498 Color: 2
Size: 352382 Color: 4
Size: 278108 Color: 2

Bin 598: 13 of cap free
Amount of items: 3
Items: 
Size: 375345 Color: 0
Size: 334835 Color: 4
Size: 289808 Color: 1

Bin 599: 13 of cap free
Amount of items: 3
Items: 
Size: 375707 Color: 2
Size: 348414 Color: 0
Size: 275867 Color: 2

Bin 600: 13 of cap free
Amount of items: 3
Items: 
Size: 380998 Color: 1
Size: 344696 Color: 0
Size: 274294 Color: 2

Bin 601: 14 of cap free
Amount of items: 3
Items: 
Size: 364207 Color: 0
Size: 320033 Color: 0
Size: 315747 Color: 4

Bin 602: 14 of cap free
Amount of items: 3
Items: 
Size: 368471 Color: 0
Size: 352260 Color: 1
Size: 279256 Color: 4

Bin 603: 15 of cap free
Amount of items: 3
Items: 
Size: 406903 Color: 4
Size: 317176 Color: 3
Size: 275907 Color: 3

Bin 604: 16 of cap free
Amount of items: 3
Items: 
Size: 361736 Color: 4
Size: 358013 Color: 0
Size: 280236 Color: 4

Bin 605: 16 of cap free
Amount of items: 3
Items: 
Size: 364478 Color: 2
Size: 363067 Color: 0
Size: 272440 Color: 1

Bin 606: 16 of cap free
Amount of items: 3
Items: 
Size: 384056 Color: 0
Size: 354483 Color: 4
Size: 261446 Color: 3

Bin 607: 17 of cap free
Amount of items: 3
Items: 
Size: 365675 Color: 2
Size: 355407 Color: 3
Size: 278902 Color: 2

Bin 608: 18 of cap free
Amount of items: 3
Items: 
Size: 377258 Color: 4
Size: 357349 Color: 1
Size: 265376 Color: 4

Bin 609: 18 of cap free
Amount of items: 3
Items: 
Size: 360484 Color: 0
Size: 326073 Color: 2
Size: 313426 Color: 3

Bin 610: 19 of cap free
Amount of items: 3
Items: 
Size: 363734 Color: 0
Size: 345309 Color: 4
Size: 290939 Color: 2

Bin 611: 19 of cap free
Amount of items: 3
Items: 
Size: 354481 Color: 1
Size: 324353 Color: 0
Size: 321148 Color: 0

Bin 612: 20 of cap free
Amount of items: 3
Items: 
Size: 363655 Color: 4
Size: 318555 Color: 4
Size: 317771 Color: 0

Bin 613: 20 of cap free
Amount of items: 3
Items: 
Size: 371484 Color: 0
Size: 326532 Color: 1
Size: 301965 Color: 2

Bin 614: 21 of cap free
Amount of items: 3
Items: 
Size: 359216 Color: 4
Size: 320597 Color: 1
Size: 320167 Color: 1

Bin 615: 22 of cap free
Amount of items: 3
Items: 
Size: 359771 Color: 1
Size: 321502 Color: 4
Size: 318706 Color: 0

Bin 616: 23 of cap free
Amount of items: 3
Items: 
Size: 365160 Color: 0
Size: 331142 Color: 1
Size: 303676 Color: 4

Bin 617: 23 of cap free
Amount of items: 3
Items: 
Size: 369847 Color: 0
Size: 317758 Color: 4
Size: 312373 Color: 1

Bin 618: 25 of cap free
Amount of items: 3
Items: 
Size: 355882 Color: 3
Size: 342354 Color: 3
Size: 301740 Color: 4

Bin 619: 25 of cap free
Amount of items: 3
Items: 
Size: 360112 Color: 4
Size: 355303 Color: 0
Size: 284561 Color: 1

Bin 620: 25 of cap free
Amount of items: 3
Items: 
Size: 370835 Color: 1
Size: 326867 Color: 3
Size: 302274 Color: 0

Bin 621: 25 of cap free
Amount of items: 3
Items: 
Size: 363573 Color: 1
Size: 332496 Color: 2
Size: 303907 Color: 0

Bin 622: 27 of cap free
Amount of items: 3
Items: 
Size: 365894 Color: 3
Size: 337319 Color: 1
Size: 296761 Color: 0

Bin 623: 27 of cap free
Amount of items: 3
Items: 
Size: 343207 Color: 4
Size: 339335 Color: 1
Size: 317432 Color: 2

Bin 624: 29 of cap free
Amount of items: 3
Items: 
Size: 371578 Color: 4
Size: 371026 Color: 3
Size: 257368 Color: 1

Bin 625: 29 of cap free
Amount of items: 3
Items: 
Size: 355923 Color: 0
Size: 333843 Color: 2
Size: 310206 Color: 3

Bin 626: 29 of cap free
Amount of items: 3
Items: 
Size: 365715 Color: 1
Size: 321814 Color: 0
Size: 312443 Color: 1

Bin 627: 30 of cap free
Amount of items: 3
Items: 
Size: 359518 Color: 4
Size: 338979 Color: 0
Size: 301474 Color: 3

Bin 628: 32 of cap free
Amount of items: 3
Items: 
Size: 363644 Color: 0
Size: 350128 Color: 1
Size: 286197 Color: 4

Bin 629: 32 of cap free
Amount of items: 3
Items: 
Size: 340803 Color: 3
Size: 336938 Color: 3
Size: 322228 Color: 0

Bin 630: 38 of cap free
Amount of items: 3
Items: 
Size: 369928 Color: 3
Size: 333416 Color: 4
Size: 296619 Color: 0

Bin 631: 39 of cap free
Amount of items: 3
Items: 
Size: 498843 Color: 3
Size: 250788 Color: 3
Size: 250331 Color: 4

Bin 632: 41 of cap free
Amount of items: 3
Items: 
Size: 356257 Color: 0
Size: 352943 Color: 2
Size: 290760 Color: 0

Bin 633: 47 of cap free
Amount of items: 3
Items: 
Size: 359203 Color: 2
Size: 323698 Color: 3
Size: 317053 Color: 2

Bin 634: 55 of cap free
Amount of items: 3
Items: 
Size: 353885 Color: 3
Size: 334142 Color: 1
Size: 311919 Color: 2

Bin 635: 56 of cap free
Amount of items: 3
Items: 
Size: 342861 Color: 2
Size: 342017 Color: 0
Size: 315067 Color: 1

Bin 636: 57 of cap free
Amount of items: 3
Items: 
Size: 388910 Color: 4
Size: 319876 Color: 2
Size: 291158 Color: 1

Bin 637: 61 of cap free
Amount of items: 3
Items: 
Size: 356188 Color: 4
Size: 328410 Color: 4
Size: 315342 Color: 1

Bin 638: 63 of cap free
Amount of items: 3
Items: 
Size: 359582 Color: 2
Size: 337006 Color: 4
Size: 303350 Color: 4

Bin 639: 70 of cap free
Amount of items: 3
Items: 
Size: 365580 Color: 4
Size: 359312 Color: 4
Size: 275039 Color: 1

Bin 640: 78 of cap free
Amount of items: 3
Items: 
Size: 349601 Color: 4
Size: 346426 Color: 2
Size: 303896 Color: 3

Bin 641: 87 of cap free
Amount of items: 3
Items: 
Size: 337372 Color: 4
Size: 333622 Color: 1
Size: 328920 Color: 2

Bin 642: 92 of cap free
Amount of items: 3
Items: 
Size: 338631 Color: 4
Size: 334135 Color: 0
Size: 327143 Color: 4

Bin 643: 99 of cap free
Amount of items: 3
Items: 
Size: 347182 Color: 3
Size: 326850 Color: 4
Size: 325870 Color: 3

Bin 644: 114 of cap free
Amount of items: 3
Items: 
Size: 354545 Color: 3
Size: 322830 Color: 0
Size: 322512 Color: 3

Bin 645: 118 of cap free
Amount of items: 3
Items: 
Size: 344097 Color: 0
Size: 335769 Color: 0
Size: 320017 Color: 4

Bin 646: 127 of cap free
Amount of items: 3
Items: 
Size: 357171 Color: 3
Size: 356349 Color: 0
Size: 286354 Color: 4

Bin 647: 133 of cap free
Amount of items: 3
Items: 
Size: 359297 Color: 1
Size: 334692 Color: 0
Size: 305879 Color: 2

Bin 648: 140 of cap free
Amount of items: 3
Items: 
Size: 350906 Color: 0
Size: 324670 Color: 1
Size: 324285 Color: 1

Bin 649: 176 of cap free
Amount of items: 3
Items: 
Size: 343605 Color: 4
Size: 337352 Color: 3
Size: 318868 Color: 2

Bin 650: 253 of cap free
Amount of items: 3
Items: 
Size: 366124 Color: 3
Size: 357513 Color: 2
Size: 276111 Color: 1

Bin 651: 255 of cap free
Amount of items: 3
Items: 
Size: 370621 Color: 3
Size: 341690 Color: 1
Size: 287435 Color: 2

Bin 652: 258 of cap free
Amount of items: 3
Items: 
Size: 413493 Color: 2
Size: 335638 Color: 3
Size: 250612 Color: 0

Bin 653: 291 of cap free
Amount of items: 3
Items: 
Size: 355069 Color: 0
Size: 353000 Color: 1
Size: 291641 Color: 4

Bin 654: 369 of cap free
Amount of items: 3
Items: 
Size: 343417 Color: 4
Size: 330114 Color: 3
Size: 326101 Color: 1

Bin 655: 580 of cap free
Amount of items: 3
Items: 
Size: 468087 Color: 0
Size: 278752 Color: 1
Size: 252582 Color: 1

Bin 656: 1042 of cap free
Amount of items: 3
Items: 
Size: 341204 Color: 1
Size: 333930 Color: 4
Size: 323825 Color: 0

Bin 657: 2248 of cap free
Amount of items: 3
Items: 
Size: 362874 Color: 3
Size: 355052 Color: 1
Size: 279827 Color: 0

Bin 658: 4183 of cap free
Amount of items: 2
Items: 
Size: 499609 Color: 2
Size: 496209 Color: 1

Bin 659: 10738 of cap free
Amount of items: 3
Items: 
Size: 361489 Color: 1
Size: 358979 Color: 2
Size: 268795 Color: 1

Bin 660: 18389 of cap free
Amount of items: 3
Items: 
Size: 361264 Color: 1
Size: 354460 Color: 4
Size: 265888 Color: 1

Bin 661: 30126 of cap free
Amount of items: 3
Items: 
Size: 354196 Color: 0
Size: 352662 Color: 3
Size: 263017 Color: 2

Bin 662: 42059 of cap free
Amount of items: 3
Items: 
Size: 348089 Color: 1
Size: 347614 Color: 2
Size: 262239 Color: 4

Bin 663: 49228 of cap free
Amount of items: 3
Items: 
Size: 344850 Color: 3
Size: 344796 Color: 1
Size: 261127 Color: 3

Bin 664: 51594 of cap free
Amount of items: 3
Items: 
Size: 344525 Color: 4
Size: 342965 Color: 1
Size: 260917 Color: 2

Bin 665: 54506 of cap free
Amount of items: 3
Items: 
Size: 342693 Color: 4
Size: 342544 Color: 4
Size: 260258 Color: 3

Bin 666: 65861 of cap free
Amount of items: 3
Items: 
Size: 340924 Color: 4
Size: 333380 Color: 3
Size: 259836 Color: 3

Bin 667: 160893 of cap free
Amount of items: 3
Items: 
Size: 331512 Color: 4
Size: 257033 Color: 0
Size: 250563 Color: 0

Bin 668: 503865 of cap free
Amount of items: 1
Items: 
Size: 496136 Color: 0

Total size: 667000667
Total free space: 1000001

