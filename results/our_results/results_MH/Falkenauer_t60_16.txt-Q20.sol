Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 4
Size: 269 Color: 9
Size: 250 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 263 Color: 15
Size: 255 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8
Size: 314 Color: 5
Size: 274 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 7
Size: 308 Color: 9
Size: 256 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 12
Size: 329 Color: 9
Size: 263 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 11
Size: 264 Color: 3
Size: 258 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 18
Size: 255 Color: 18
Size: 253 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 19
Size: 280 Color: 1
Size: 270 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 252 Color: 14
Size: 251 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 10
Size: 334 Color: 9
Size: 264 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 7
Size: 282 Color: 10
Size: 279 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 296 Color: 14
Size: 275 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9
Size: 252 Color: 11
Size: 250 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 6
Size: 279 Color: 2
Size: 266 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 14
Size: 354 Color: 8
Size: 264 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 2
Size: 267 Color: 16
Size: 253 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 300 Color: 10
Size: 268 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 18
Size: 315 Color: 4
Size: 253 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 19
Size: 314 Color: 17
Size: 284 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 13
Size: 282 Color: 2
Size: 274 Color: 10

Total size: 20000
Total free space: 0

