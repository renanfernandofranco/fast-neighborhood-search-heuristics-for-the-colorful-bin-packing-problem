Capicity Bin: 2068
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 1300 Color: 2
Size: 768 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 0
Size: 507 Color: 4
Size: 136 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 4
Size: 522 Color: 2
Size: 100 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 1
Size: 537 Color: 2
Size: 70 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 0
Size: 417 Color: 1
Size: 82 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 0
Size: 428 Color: 1
Size: 50 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 4
Size: 386 Color: 1
Size: 76 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 3
Size: 354 Color: 3
Size: 76 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1641 Color: 0
Size: 331 Color: 1
Size: 96 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 4
Size: 351 Color: 1
Size: 42 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 0
Size: 262 Color: 1
Size: 76 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 4
Size: 211 Color: 1
Size: 124 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 4
Size: 273 Color: 1
Size: 54 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 1
Size: 274 Color: 4
Size: 52 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 0
Size: 168 Color: 4
Size: 142 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 4
Size: 261 Color: 2
Size: 58 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 1
Size: 247 Color: 4
Size: 48 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 4
Size: 152 Color: 3
Size: 106 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 2
Size: 221 Color: 4
Size: 44 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 4
Size: 187 Color: 3
Size: 52 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 0
Size: 214 Color: 3
Size: 40 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 4
Size: 193 Color: 1
Size: 38 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 4
Size: 168 Color: 1
Size: 42 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1853 Color: 0
Size: 181 Color: 4
Size: 34 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 1
Size: 521 Color: 3
Size: 232 Color: 4

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 1
Size: 357 Color: 4
Size: 56 Color: 3

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 4
Size: 383 Color: 2
Size: 38 Color: 4

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 2
Size: 217 Color: 2
Size: 201 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 2
Size: 362 Color: 4
Size: 32 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 1
Size: 281 Color: 0
Size: 36 Color: 0

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 1776 Color: 3
Size: 291 Color: 0

Bin 32: 2 of cap free
Amount of items: 4
Items: 
Size: 1053 Color: 1
Size: 694 Color: 2
Size: 267 Color: 0
Size: 52 Color: 3

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1605 Color: 2
Size: 461 Color: 3

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 4
Size: 342 Color: 3
Size: 102 Color: 1

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 1
Size: 480 Color: 1
Size: 68 Color: 2

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1719 Color: 3
Size: 346 Color: 4

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 3
Size: 232 Color: 2
Size: 24 Color: 1

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 1
Size: 841 Color: 4
Size: 178 Color: 0

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1177 Color: 4
Size: 847 Color: 2
Size: 40 Color: 0

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1486 Color: 4
Size: 578 Color: 0

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1662 Color: 2
Size: 402 Color: 4

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1734 Color: 2
Size: 329 Color: 3

Bin 43: 5 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 0
Size: 282 Color: 2
Size: 16 Color: 2

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1845 Color: 3
Size: 218 Color: 1

Bin 45: 6 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 2
Size: 853 Color: 3
Size: 172 Color: 2

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1817 Color: 2
Size: 244 Color: 3

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1289 Color: 2
Size: 770 Color: 1

Bin 48: 9 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 2
Size: 400 Color: 1
Size: 320 Color: 3

Bin 49: 10 of cap free
Amount of items: 3
Items: 
Size: 1373 Color: 2
Size: 609 Color: 3
Size: 76 Color: 0

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1428 Color: 4
Size: 630 Color: 0

Bin 51: 11 of cap free
Amount of items: 4
Items: 
Size: 1035 Color: 0
Size: 693 Color: 2
Size: 261 Color: 1
Size: 68 Color: 2

Bin 52: 12 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 1
Size: 438 Color: 4
Size: 68 Color: 2

Bin 53: 13 of cap free
Amount of items: 4
Items: 
Size: 1757 Color: 0
Size: 282 Color: 4
Size: 8 Color: 3
Size: 8 Color: 2

Bin 54: 19 of cap free
Amount of items: 4
Items: 
Size: 1038 Color: 4
Size: 861 Color: 2
Size: 100 Color: 0
Size: 50 Color: 1

Bin 55: 19 of cap free
Amount of items: 2
Items: 
Size: 1563 Color: 0
Size: 486 Color: 4

Bin 56: 20 of cap free
Amount of items: 2
Items: 
Size: 1609 Color: 1
Size: 439 Color: 4

Bin 57: 21 of cap free
Amount of items: 2
Items: 
Size: 1185 Color: 2
Size: 862 Color: 4

Bin 58: 22 of cap free
Amount of items: 2
Items: 
Size: 1612 Color: 4
Size: 434 Color: 2

Bin 59: 26 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 4
Size: 737 Color: 2
Size: 68 Color: 0

Bin 60: 26 of cap free
Amount of items: 3
Items: 
Size: 1445 Color: 3
Size: 513 Color: 0
Size: 84 Color: 2

Bin 61: 32 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 2
Size: 696 Color: 1
Size: 102 Color: 0

Bin 62: 33 of cap free
Amount of items: 2
Items: 
Size: 1146 Color: 4
Size: 889 Color: 1

Bin 63: 34 of cap free
Amount of items: 2
Items: 
Size: 1453 Color: 3
Size: 581 Color: 4

Bin 64: 35 of cap free
Amount of items: 15
Items: 
Size: 421 Color: 1
Size: 266 Color: 0
Size: 170 Color: 2
Size: 146 Color: 4
Size: 138 Color: 3
Size: 128 Color: 1
Size: 120 Color: 4
Size: 114 Color: 3
Size: 112 Color: 3
Size: 90 Color: 4
Size: 84 Color: 1
Size: 68 Color: 0
Size: 64 Color: 0
Size: 64 Color: 0
Size: 48 Color: 1

Bin 65: 39 of cap free
Amount of items: 2
Items: 
Size: 1378 Color: 1
Size: 651 Color: 2

Bin 66: 1602 of cap free
Amount of items: 9
Items: 
Size: 64 Color: 2
Size: 64 Color: 0
Size: 56 Color: 4
Size: 54 Color: 0
Size: 52 Color: 4
Size: 48 Color: 1
Size: 48 Color: 1
Size: 40 Color: 3
Size: 40 Color: 3

Total size: 134420
Total free space: 2068

