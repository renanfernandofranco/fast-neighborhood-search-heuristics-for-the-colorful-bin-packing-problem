Capicity Bin: 16480
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 0
Size: 5944 Color: 4
Size: 544 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10217 Color: 4
Size: 5221 Color: 2
Size: 1042 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 4
Size: 4626 Color: 4
Size: 924 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11634 Color: 2
Size: 4200 Color: 4
Size: 646 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11658 Color: 0
Size: 4440 Color: 3
Size: 382 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11860 Color: 2
Size: 2374 Color: 0
Size: 2246 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12372 Color: 1
Size: 3892 Color: 0
Size: 216 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12670 Color: 1
Size: 3464 Color: 0
Size: 346 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 3
Size: 2724 Color: 4
Size: 1044 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12974 Color: 4
Size: 2922 Color: 2
Size: 584 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12923 Color: 2
Size: 2877 Color: 2
Size: 680 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13025 Color: 4
Size: 2937 Color: 3
Size: 518 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 0
Size: 2888 Color: 2
Size: 560 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13073 Color: 4
Size: 2841 Color: 3
Size: 566 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13080 Color: 1
Size: 3004 Color: 0
Size: 396 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13097 Color: 2
Size: 2567 Color: 0
Size: 816 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13432 Color: 1
Size: 2678 Color: 4
Size: 370 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 0
Size: 1524 Color: 4
Size: 1384 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13586 Color: 0
Size: 1982 Color: 2
Size: 912 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13582 Color: 1
Size: 1602 Color: 3
Size: 1296 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13602 Color: 4
Size: 1702 Color: 2
Size: 1176 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13634 Color: 4
Size: 2418 Color: 1
Size: 428 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 0
Size: 2260 Color: 4
Size: 532 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13778 Color: 0
Size: 1662 Color: 3
Size: 1040 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 4
Size: 2336 Color: 0
Size: 296 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 2
Size: 1842 Color: 0
Size: 784 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 1
Size: 2124 Color: 2
Size: 416 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14012 Color: 4
Size: 1876 Color: 0
Size: 592 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 4
Size: 1752 Color: 0
Size: 560 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14209 Color: 4
Size: 1913 Color: 0
Size: 358 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14225 Color: 3
Size: 1881 Color: 2
Size: 374 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14253 Color: 3
Size: 1859 Color: 1
Size: 368 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14277 Color: 1
Size: 1837 Color: 2
Size: 366 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 1
Size: 1576 Color: 2
Size: 592 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14325 Color: 2
Size: 1741 Color: 3
Size: 414 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 0
Size: 1796 Color: 3
Size: 304 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 2
Size: 1496 Color: 2
Size: 624 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14336 Color: 4
Size: 1692 Color: 1
Size: 452 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14394 Color: 4
Size: 1450 Color: 0
Size: 636 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 2
Size: 1554 Color: 1
Size: 474 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14486 Color: 2
Size: 1742 Color: 0
Size: 252 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 1
Size: 1588 Color: 2
Size: 404 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 1
Size: 1372 Color: 0
Size: 574 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14584 Color: 1
Size: 1542 Color: 4
Size: 354 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 0
Size: 1528 Color: 2
Size: 352 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14630 Color: 1
Size: 1502 Color: 4
Size: 348 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 2
Size: 1376 Color: 0
Size: 408 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14660 Color: 0
Size: 1016 Color: 1
Size: 804 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14694 Color: 3
Size: 1216 Color: 4
Size: 570 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14824 Color: 2
Size: 1120 Color: 4
Size: 536 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14776 Color: 0
Size: 1360 Color: 2
Size: 344 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14828 Color: 3
Size: 1188 Color: 2
Size: 464 Color: 1

Bin 53: 1 of cap free
Amount of items: 5
Items: 
Size: 8260 Color: 0
Size: 4561 Color: 4
Size: 1882 Color: 4
Size: 1380 Color: 1
Size: 396 Color: 4

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 10177 Color: 4
Size: 5998 Color: 0
Size: 304 Color: 1

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 10225 Color: 2
Size: 5208 Color: 0
Size: 1046 Color: 4

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 10201 Color: 3
Size: 5938 Color: 4
Size: 340 Color: 0

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 11160 Color: 2
Size: 4601 Color: 4
Size: 718 Color: 0

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12593 Color: 0
Size: 3566 Color: 4
Size: 320 Color: 2

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12868 Color: 3
Size: 3227 Color: 0
Size: 384 Color: 3

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 13210 Color: 1
Size: 2861 Color: 4
Size: 408 Color: 1

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13278 Color: 0
Size: 2803 Color: 1
Size: 398 Color: 4

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 0
Size: 2149 Color: 2
Size: 914 Color: 3

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13401 Color: 3
Size: 2684 Color: 0
Size: 394 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13772 Color: 2
Size: 2375 Color: 0
Size: 332 Color: 4

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13903 Color: 1
Size: 1658 Color: 0
Size: 918 Color: 3

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13976 Color: 1
Size: 1933 Color: 2
Size: 570 Color: 0

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 14113 Color: 4
Size: 2022 Color: 2
Size: 344 Color: 0

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 14174 Color: 2
Size: 1777 Color: 2
Size: 528 Color: 0

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 14185 Color: 1
Size: 1970 Color: 2
Size: 324 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 14301 Color: 2
Size: 1792 Color: 3
Size: 386 Color: 2

Bin 71: 2 of cap free
Amount of items: 9
Items: 
Size: 8242 Color: 1
Size: 1360 Color: 0
Size: 1196 Color: 3
Size: 1184 Color: 3
Size: 1176 Color: 1
Size: 1168 Color: 4
Size: 912 Color: 4
Size: 888 Color: 2
Size: 352 Color: 2

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 9104 Color: 1
Size: 4566 Color: 2
Size: 2808 Color: 0

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 10961 Color: 2
Size: 5213 Color: 0
Size: 304 Color: 3

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 11586 Color: 2
Size: 4452 Color: 4
Size: 440 Color: 2

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 3
Size: 3012 Color: 1
Size: 512 Color: 0

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 1
Size: 2601 Color: 0
Size: 872 Color: 4

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 13117 Color: 1
Size: 2673 Color: 0
Size: 688 Color: 3

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 0
Size: 2670 Color: 0
Size: 540 Color: 4

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13597 Color: 4
Size: 2881 Color: 3

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 4
Size: 1432 Color: 4
Size: 944 Color: 0

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 4
Size: 2200 Color: 3
Size: 168 Color: 2

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 14270 Color: 2
Size: 1928 Color: 4
Size: 280 Color: 3

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 1
Size: 1368 Color: 4
Size: 720 Color: 4

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 8243 Color: 1
Size: 6866 Color: 0
Size: 1368 Color: 3

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 11448 Color: 2
Size: 4581 Color: 0
Size: 448 Color: 3

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 11812 Color: 4
Size: 4087 Color: 0
Size: 578 Color: 2

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 12130 Color: 4
Size: 2857 Color: 1
Size: 1490 Color: 3

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 13361 Color: 0
Size: 2808 Color: 4
Size: 308 Color: 2

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 13767 Color: 0
Size: 2710 Color: 2

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 13993 Color: 1
Size: 2484 Color: 4

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 14417 Color: 3
Size: 2060 Color: 0

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 10380 Color: 3
Size: 5648 Color: 2
Size: 448 Color: 1

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 11621 Color: 0
Size: 4543 Color: 4
Size: 312 Color: 3

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 12586 Color: 0
Size: 3586 Color: 4
Size: 304 Color: 3

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 12552 Color: 2
Size: 3924 Color: 1

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 1
Size: 3198 Color: 0
Size: 632 Color: 3

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 13053 Color: 0
Size: 3207 Color: 2
Size: 216 Color: 3

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 13049 Color: 3
Size: 3139 Color: 4
Size: 288 Color: 0

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 14222 Color: 3
Size: 2254 Color: 2

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 14708 Color: 1
Size: 1768 Color: 0

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 10978 Color: 4
Size: 5233 Color: 2
Size: 264 Color: 2

Bin 102: 5 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 0
Size: 3601 Color: 0
Size: 1762 Color: 1

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 11651 Color: 0
Size: 4488 Color: 1
Size: 336 Color: 3

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 12202 Color: 3
Size: 3601 Color: 2
Size: 672 Color: 0

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 14678 Color: 0
Size: 1797 Color: 4

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 14754 Color: 4
Size: 1721 Color: 1

Bin 107: 6 of cap free
Amount of items: 21
Items: 
Size: 1050 Color: 1
Size: 1042 Color: 3
Size: 1040 Color: 0
Size: 992 Color: 3
Size: 910 Color: 3
Size: 908 Color: 0
Size: 880 Color: 2
Size: 832 Color: 3
Size: 832 Color: 2
Size: 812 Color: 1
Size: 808 Color: 1
Size: 800 Color: 2
Size: 800 Color: 2
Size: 776 Color: 3
Size: 768 Color: 1
Size: 752 Color: 4
Size: 640 Color: 4
Size: 616 Color: 4
Size: 586 Color: 4
Size: 342 Color: 4
Size: 288 Color: 0

Bin 108: 6 of cap free
Amount of items: 3
Items: 
Size: 9358 Color: 4
Size: 6844 Color: 3
Size: 272 Color: 1

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 10262 Color: 2
Size: 5924 Color: 0
Size: 288 Color: 1

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 10760 Color: 2
Size: 4042 Color: 0
Size: 1672 Color: 1

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 14558 Color: 1
Size: 1916 Color: 4

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 14612 Color: 1
Size: 1862 Color: 3

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 14742 Color: 4
Size: 1732 Color: 3

Bin 114: 7 of cap free
Amount of items: 3
Items: 
Size: 9334 Color: 4
Size: 6867 Color: 3
Size: 272 Color: 2

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 10214 Color: 4
Size: 5987 Color: 3
Size: 272 Color: 3

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 12161 Color: 4
Size: 2261 Color: 2
Size: 2051 Color: 2

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 14580 Color: 4
Size: 1893 Color: 0

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 10332 Color: 0
Size: 5532 Color: 4
Size: 608 Color: 1

Bin 119: 8 of cap free
Amount of items: 3
Items: 
Size: 12178 Color: 0
Size: 4022 Color: 0
Size: 272 Color: 4

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 13651 Color: 4
Size: 2821 Color: 2

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 13830 Color: 3
Size: 2642 Color: 4

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 1
Size: 2284 Color: 2

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 9286 Color: 2
Size: 6865 Color: 4
Size: 320 Color: 3

Bin 124: 9 of cap free
Amount of items: 3
Items: 
Size: 11029 Color: 4
Size: 4730 Color: 2
Size: 712 Color: 1

Bin 125: 9 of cap free
Amount of items: 2
Items: 
Size: 12420 Color: 3
Size: 4051 Color: 4

Bin 126: 9 of cap free
Amount of items: 2
Items: 
Size: 13230 Color: 3
Size: 3241 Color: 1

Bin 127: 9 of cap free
Amount of items: 2
Items: 
Size: 13631 Color: 1
Size: 2840 Color: 4

Bin 128: 10 of cap free
Amount of items: 3
Items: 
Size: 11772 Color: 2
Size: 4184 Color: 0
Size: 514 Color: 4

Bin 129: 10 of cap free
Amount of items: 3
Items: 
Size: 14438 Color: 2
Size: 1984 Color: 3
Size: 48 Color: 1

Bin 130: 10 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 4
Size: 1998 Color: 0
Size: 68 Color: 1

Bin 131: 11 of cap free
Amount of items: 3
Items: 
Size: 9297 Color: 1
Size: 6872 Color: 3
Size: 300 Color: 1

Bin 132: 11 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 3
Size: 3265 Color: 4
Size: 92 Color: 4

Bin 133: 12 of cap free
Amount of items: 3
Items: 
Size: 9428 Color: 3
Size: 6160 Color: 2
Size: 880 Color: 0

Bin 134: 12 of cap free
Amount of items: 3
Items: 
Size: 9704 Color: 2
Size: 5884 Color: 3
Size: 880 Color: 3

Bin 135: 12 of cap free
Amount of items: 2
Items: 
Size: 12056 Color: 1
Size: 4412 Color: 4

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 13220 Color: 1
Size: 3246 Color: 3

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 13740 Color: 3
Size: 2726 Color: 1

Bin 138: 15 of cap free
Amount of items: 3
Items: 
Size: 12197 Color: 3
Size: 3852 Color: 0
Size: 416 Color: 3

Bin 139: 15 of cap free
Amount of items: 3
Items: 
Size: 12957 Color: 3
Size: 3236 Color: 1
Size: 272 Color: 1

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 13077 Color: 2
Size: 3388 Color: 4

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 14648 Color: 1
Size: 1817 Color: 3

Bin 142: 16 of cap free
Amount of items: 6
Items: 
Size: 8248 Color: 3
Size: 2008 Color: 2
Size: 1857 Color: 1
Size: 1813 Color: 3
Size: 1622 Color: 4
Size: 916 Color: 0

Bin 143: 18 of cap free
Amount of items: 3
Items: 
Size: 14393 Color: 1
Size: 1973 Color: 4
Size: 96 Color: 0

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 14646 Color: 0
Size: 1816 Color: 3

Bin 145: 19 of cap free
Amount of items: 2
Items: 
Size: 13385 Color: 2
Size: 3076 Color: 3

Bin 146: 19 of cap free
Amount of items: 2
Items: 
Size: 14161 Color: 4
Size: 2300 Color: 1

Bin 147: 21 of cap free
Amount of items: 2
Items: 
Size: 12563 Color: 4
Size: 3896 Color: 3

Bin 148: 21 of cap free
Amount of items: 2
Items: 
Size: 13878 Color: 3
Size: 2581 Color: 1

Bin 149: 22 of cap free
Amount of items: 3
Items: 
Size: 8250 Color: 1
Size: 5656 Color: 2
Size: 2552 Color: 0

Bin 150: 22 of cap free
Amount of items: 2
Items: 
Size: 14370 Color: 2
Size: 2088 Color: 3

Bin 151: 23 of cap free
Amount of items: 2
Items: 
Size: 13029 Color: 3
Size: 3428 Color: 1

Bin 152: 24 of cap free
Amount of items: 2
Items: 
Size: 14054 Color: 4
Size: 2402 Color: 3

Bin 153: 25 of cap free
Amount of items: 2
Items: 
Size: 12884 Color: 4
Size: 3571 Color: 1

Bin 154: 26 of cap free
Amount of items: 2
Items: 
Size: 13310 Color: 2
Size: 3144 Color: 1

Bin 155: 27 of cap free
Amount of items: 3
Items: 
Size: 9299 Color: 0
Size: 5958 Color: 3
Size: 1196 Color: 1

Bin 156: 27 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 1
Size: 2965 Color: 2
Size: 652 Color: 2

Bin 157: 28 of cap free
Amount of items: 3
Items: 
Size: 10806 Color: 2
Size: 5084 Color: 4
Size: 562 Color: 3

Bin 158: 28 of cap free
Amount of items: 3
Items: 
Size: 14086 Color: 0
Size: 2190 Color: 3
Size: 176 Color: 1

Bin 159: 29 of cap free
Amount of items: 2
Items: 
Size: 11675 Color: 3
Size: 4776 Color: 1

Bin 160: 31 of cap free
Amount of items: 3
Items: 
Size: 12617 Color: 2
Size: 3688 Color: 1
Size: 144 Color: 2

Bin 161: 31 of cap free
Amount of items: 2
Items: 
Size: 14021 Color: 1
Size: 2428 Color: 3

Bin 162: 32 of cap free
Amount of items: 30
Items: 
Size: 768 Color: 0
Size: 742 Color: 3
Size: 736 Color: 3
Size: 724 Color: 0
Size: 716 Color: 3
Size: 712 Color: 2
Size: 688 Color: 1
Size: 648 Color: 0
Size: 640 Color: 0
Size: 600 Color: 1
Size: 584 Color: 1
Size: 574 Color: 2
Size: 560 Color: 4
Size: 560 Color: 3
Size: 544 Color: 2
Size: 528 Color: 2
Size: 496 Color: 4
Size: 496 Color: 3
Size: 480 Color: 3
Size: 480 Color: 2
Size: 476 Color: 4
Size: 472 Color: 1
Size: 456 Color: 1
Size: 436 Color: 0
Size: 432 Color: 1
Size: 432 Color: 0
Size: 404 Color: 2
Size: 392 Color: 4
Size: 376 Color: 4
Size: 296 Color: 4

Bin 163: 32 of cap free
Amount of items: 3
Items: 
Size: 10985 Color: 3
Size: 5253 Color: 2
Size: 210 Color: 3

Bin 164: 33 of cap free
Amount of items: 2
Items: 
Size: 14236 Color: 2
Size: 2211 Color: 3

Bin 165: 33 of cap free
Amount of items: 3
Items: 
Size: 14349 Color: 1
Size: 1978 Color: 0
Size: 120 Color: 4

Bin 166: 37 of cap free
Amount of items: 2
Items: 
Size: 14305 Color: 1
Size: 2138 Color: 4

Bin 167: 38 of cap free
Amount of items: 2
Items: 
Size: 13264 Color: 2
Size: 3178 Color: 3

Bin 168: 38 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 2
Size: 2942 Color: 1

Bin 169: 38 of cap free
Amount of items: 2
Items: 
Size: 14369 Color: 3
Size: 2073 Color: 4

Bin 170: 39 of cap free
Amount of items: 2
Items: 
Size: 12981 Color: 2
Size: 3460 Color: 4

Bin 171: 43 of cap free
Amount of items: 3
Items: 
Size: 9380 Color: 0
Size: 5985 Color: 4
Size: 1072 Color: 0

Bin 172: 51 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 2
Size: 4005 Color: 3
Size: 92 Color: 1

Bin 173: 52 of cap free
Amount of items: 3
Items: 
Size: 8632 Color: 3
Size: 6852 Color: 1
Size: 944 Color: 0

Bin 174: 52 of cap free
Amount of items: 2
Items: 
Size: 11464 Color: 1
Size: 4964 Color: 3

Bin 175: 52 of cap free
Amount of items: 3
Items: 
Size: 12159 Color: 1
Size: 2897 Color: 2
Size: 1372 Color: 4

Bin 176: 55 of cap free
Amount of items: 2
Items: 
Size: 11009 Color: 0
Size: 5416 Color: 1

Bin 177: 58 of cap free
Amount of items: 2
Items: 
Size: 12796 Color: 1
Size: 3626 Color: 0

Bin 178: 59 of cap free
Amount of items: 2
Items: 
Size: 10237 Color: 1
Size: 6184 Color: 3

Bin 179: 60 of cap free
Amount of items: 2
Items: 
Size: 14250 Color: 3
Size: 2170 Color: 2

Bin 180: 64 of cap free
Amount of items: 2
Items: 
Size: 10524 Color: 4
Size: 5892 Color: 3

Bin 181: 64 of cap free
Amount of items: 2
Items: 
Size: 13128 Color: 3
Size: 3288 Color: 4

Bin 182: 64 of cap free
Amount of items: 2
Items: 
Size: 14088 Color: 3
Size: 2328 Color: 4

Bin 183: 70 of cap free
Amount of items: 2
Items: 
Size: 11188 Color: 2
Size: 5222 Color: 1

Bin 184: 70 of cap free
Amount of items: 2
Items: 
Size: 12328 Color: 0
Size: 4082 Color: 2

Bin 185: 71 of cap free
Amount of items: 7
Items: 
Size: 8241 Color: 2
Size: 1564 Color: 2
Size: 1484 Color: 0
Size: 1372 Color: 3
Size: 1368 Color: 0
Size: 1196 Color: 1
Size: 1184 Color: 1

Bin 186: 76 of cap free
Amount of items: 2
Items: 
Size: 12633 Color: 2
Size: 3771 Color: 1

Bin 187: 78 of cap free
Amount of items: 2
Items: 
Size: 11816 Color: 2
Size: 4586 Color: 3

Bin 188: 80 of cap free
Amount of items: 3
Items: 
Size: 9352 Color: 0
Size: 6552 Color: 3
Size: 496 Color: 1

Bin 189: 83 of cap free
Amount of items: 2
Items: 
Size: 13829 Color: 3
Size: 2568 Color: 2

Bin 190: 85 of cap free
Amount of items: 2
Items: 
Size: 13964 Color: 4
Size: 2431 Color: 1

Bin 191: 88 of cap free
Amount of items: 2
Items: 
Size: 10232 Color: 4
Size: 6160 Color: 3

Bin 192: 91 of cap free
Amount of items: 2
Items: 
Size: 11002 Color: 3
Size: 5387 Color: 0

Bin 193: 104 of cap free
Amount of items: 3
Items: 
Size: 8276 Color: 0
Size: 6868 Color: 0
Size: 1232 Color: 4

Bin 194: 108 of cap free
Amount of items: 2
Items: 
Size: 8244 Color: 4
Size: 8128 Color: 2

Bin 195: 133 of cap free
Amount of items: 2
Items: 
Size: 11001 Color: 0
Size: 5346 Color: 1

Bin 196: 137 of cap free
Amount of items: 2
Items: 
Size: 11140 Color: 2
Size: 5203 Color: 4

Bin 197: 206 of cap free
Amount of items: 2
Items: 
Size: 9412 Color: 0
Size: 6862 Color: 1

Bin 198: 232 of cap free
Amount of items: 2
Items: 
Size: 9064 Color: 4
Size: 7184 Color: 2

Bin 199: 12834 of cap free
Amount of items: 10
Items: 
Size: 408 Color: 1
Size: 384 Color: 3
Size: 378 Color: 1
Size: 376 Color: 0
Size: 368 Color: 2
Size: 368 Color: 2
Size: 362 Color: 4
Size: 362 Color: 4
Size: 352 Color: 3
Size: 288 Color: 0

Total size: 3263040
Total free space: 16480

