Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 55
Size: 273 Color: 20
Size: 250 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 43
Size: 357 Color: 37
Size: 268 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 42
Size: 354 Color: 36
Size: 275 Color: 21

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 44
Size: 345 Color: 34
Size: 277 Color: 23

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 40
Size: 367 Color: 39
Size: 265 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 53
Size: 281 Color: 26
Size: 258 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 316 Color: 32
Size: 426 Color: 48
Size: 258 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 56
Size: 265 Color: 16
Size: 257 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 49
Size: 293 Color: 29
Size: 278 Color: 25

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 46
Size: 298 Color: 31
Size: 297 Color: 30

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 57
Size: 261 Color: 11
Size: 254 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 58
Size: 255 Color: 6
Size: 252 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 52
Size: 278 Color: 24
Size: 263 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 38
Size: 270 Color: 19
Size: 369 Color: 41

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 59
Size: 255 Color: 5
Size: 250 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 51
Size: 281 Color: 27
Size: 263 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 45
Size: 347 Color: 35
Size: 262 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 47
Size: 332 Color: 33
Size: 254 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 50
Size: 293 Color: 28
Size: 256 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 54
Size: 277 Color: 22
Size: 261 Color: 12

Total size: 20000
Total free space: 0

