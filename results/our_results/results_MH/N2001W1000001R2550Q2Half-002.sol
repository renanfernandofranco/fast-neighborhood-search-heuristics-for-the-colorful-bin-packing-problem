Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 385901 Color: 1
Size: 324950 Color: 1
Size: 289150 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 461335 Color: 1
Size: 285219 Color: 1
Size: 253447 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 388549 Color: 1
Size: 332760 Color: 1
Size: 278692 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 353086 Color: 1
Size: 327655 Color: 1
Size: 319260 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 409270 Color: 1
Size: 306798 Color: 0
Size: 283933 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 365188 Color: 1
Size: 328446 Color: 1
Size: 306367 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 364413 Color: 1
Size: 323465 Color: 1
Size: 312123 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 356324 Color: 1
Size: 321918 Color: 1
Size: 321759 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 354177 Color: 1
Size: 353205 Color: 1
Size: 292619 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 354015 Color: 1
Size: 328106 Color: 1
Size: 317880 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 347653 Color: 1
Size: 337074 Color: 1
Size: 315274 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 351982 Color: 1
Size: 343245 Color: 1
Size: 304774 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 346590 Color: 1
Size: 339538 Color: 1
Size: 313873 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 361902 Color: 1
Size: 338663 Color: 1
Size: 299436 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 362851 Color: 1
Size: 336921 Color: 1
Size: 300229 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 363921 Color: 1
Size: 345516 Color: 1
Size: 290564 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 366750 Color: 1
Size: 363381 Color: 1
Size: 269870 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 367603 Color: 1
Size: 341537 Color: 1
Size: 290861 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 367835 Color: 1
Size: 347863 Color: 1
Size: 284303 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 368630 Color: 1
Size: 331124 Color: 1
Size: 300247 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 369234 Color: 1
Size: 364327 Color: 1
Size: 266440 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 369346 Color: 1
Size: 341415 Color: 1
Size: 289240 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 369740 Color: 1
Size: 327040 Color: 1
Size: 303221 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 370002 Color: 1
Size: 333521 Color: 1
Size: 296478 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 370253 Color: 1
Size: 342206 Color: 1
Size: 287542 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 372127 Color: 1
Size: 359406 Color: 1
Size: 268468 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 372431 Color: 1
Size: 344732 Color: 1
Size: 282838 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 372968 Color: 1
Size: 327040 Color: 1
Size: 299993 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 374078 Color: 1
Size: 335955 Color: 1
Size: 289968 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 374528 Color: 1
Size: 338208 Color: 1
Size: 287265 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 374963 Color: 1
Size: 334187 Color: 1
Size: 290851 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 376207 Color: 1
Size: 322039 Color: 1
Size: 301755 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 376231 Color: 1
Size: 346333 Color: 1
Size: 277437 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 376465 Color: 1
Size: 336830 Color: 1
Size: 286706 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 376580 Color: 1
Size: 340953 Color: 1
Size: 282468 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 376754 Color: 1
Size: 338768 Color: 1
Size: 284479 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 376869 Color: 1
Size: 351791 Color: 1
Size: 271341 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 377102 Color: 1
Size: 343795 Color: 1
Size: 279104 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 377466 Color: 1
Size: 345029 Color: 1
Size: 277506 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 377512 Color: 1
Size: 341616 Color: 1
Size: 280873 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 377558 Color: 1
Size: 328991 Color: 1
Size: 293452 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 377808 Color: 1
Size: 364760 Color: 1
Size: 257433 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 378441 Color: 1
Size: 354367 Color: 1
Size: 267193 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 379737 Color: 1
Size: 329968 Color: 1
Size: 290296 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 380450 Color: 1
Size: 336998 Color: 1
Size: 282553 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 380746 Color: 1
Size: 326777 Color: 1
Size: 292478 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 380794 Color: 1
Size: 314838 Color: 1
Size: 304369 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 381117 Color: 1
Size: 332335 Color: 1
Size: 286549 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 381244 Color: 1
Size: 335322 Color: 1
Size: 283435 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 382102 Color: 1
Size: 340917 Color: 1
Size: 276982 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 383218 Color: 1
Size: 340516 Color: 1
Size: 276267 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 383388 Color: 1
Size: 325261 Color: 1
Size: 291352 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 383474 Color: 1
Size: 311099 Color: 0
Size: 305428 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 383914 Color: 1
Size: 319364 Color: 1
Size: 296723 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 384344 Color: 1
Size: 323699 Color: 1
Size: 291958 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 384635 Color: 1
Size: 310363 Color: 1
Size: 305003 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 384880 Color: 1
Size: 323949 Color: 1
Size: 291172 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 385577 Color: 1
Size: 328375 Color: 1
Size: 286049 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 386171 Color: 1
Size: 320568 Color: 1
Size: 293262 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 386859 Color: 1
Size: 307656 Color: 1
Size: 305486 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 387257 Color: 1
Size: 345439 Color: 1
Size: 267305 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 387939 Color: 1
Size: 307177 Color: 1
Size: 304885 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 389080 Color: 1
Size: 318415 Color: 1
Size: 292506 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 389412 Color: 1
Size: 343181 Color: 1
Size: 267408 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 389499 Color: 1
Size: 324599 Color: 1
Size: 285903 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 389889 Color: 1
Size: 328500 Color: 1
Size: 281612 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 391295 Color: 1
Size: 330461 Color: 1
Size: 278245 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 391332 Color: 1
Size: 328936 Color: 1
Size: 279733 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 391496 Color: 1
Size: 325733 Color: 1
Size: 282772 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 391921 Color: 1
Size: 345372 Color: 1
Size: 262708 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 391963 Color: 1
Size: 340741 Color: 1
Size: 267297 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 393166 Color: 1
Size: 322458 Color: 1
Size: 284377 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 393241 Color: 1
Size: 304005 Color: 1
Size: 302755 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 393265 Color: 1
Size: 303909 Color: 1
Size: 302827 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 393468 Color: 1
Size: 315210 Color: 1
Size: 291323 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 393486 Color: 1
Size: 336926 Color: 1
Size: 269589 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 393692 Color: 1
Size: 340385 Color: 1
Size: 265924 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 393710 Color: 1
Size: 311456 Color: 0
Size: 294835 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 393752 Color: 1
Size: 326119 Color: 1
Size: 280130 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 394203 Color: 1
Size: 315725 Color: 0
Size: 290073 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 394372 Color: 1
Size: 329773 Color: 1
Size: 275856 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 394436 Color: 1
Size: 320639 Color: 1
Size: 284926 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 394635 Color: 1
Size: 308030 Color: 0
Size: 297336 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 394636 Color: 1
Size: 350766 Color: 1
Size: 254599 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 395173 Color: 1
Size: 329355 Color: 1
Size: 275473 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 395301 Color: 1
Size: 314814 Color: 1
Size: 289886 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 395735 Color: 1
Size: 319338 Color: 1
Size: 284928 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 395757 Color: 1
Size: 346298 Color: 1
Size: 257946 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 396424 Color: 1
Size: 325476 Color: 1
Size: 278101 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 396656 Color: 1
Size: 345958 Color: 1
Size: 257387 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 396890 Color: 1
Size: 338295 Color: 1
Size: 264816 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 397178 Color: 1
Size: 335078 Color: 1
Size: 267745 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 397439 Color: 1
Size: 337357 Color: 1
Size: 265205 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 397542 Color: 1
Size: 325170 Color: 1
Size: 277289 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 398037 Color: 1
Size: 337499 Color: 1
Size: 264465 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 398193 Color: 1
Size: 312352 Color: 1
Size: 289456 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 399622 Color: 1
Size: 334458 Color: 1
Size: 265921 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 400020 Color: 1
Size: 312962 Color: 1
Size: 287019 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 400255 Color: 1
Size: 347648 Color: 1
Size: 252098 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 400565 Color: 1
Size: 315400 Color: 1
Size: 284036 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 401026 Color: 1
Size: 305206 Color: 1
Size: 293769 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 401097 Color: 1
Size: 346519 Color: 1
Size: 252385 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 401292 Color: 1
Size: 337477 Color: 1
Size: 261232 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 401349 Color: 1
Size: 344311 Color: 1
Size: 254341 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 401548 Color: 1
Size: 336110 Color: 1
Size: 262343 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 401963 Color: 1
Size: 331819 Color: 1
Size: 266219 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 402065 Color: 1
Size: 331950 Color: 1
Size: 265986 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 402346 Color: 1
Size: 323879 Color: 1
Size: 273776 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 402518 Color: 1
Size: 323279 Color: 1
Size: 274204 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 402630 Color: 1
Size: 302395 Color: 1
Size: 294976 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 402838 Color: 1
Size: 337915 Color: 1
Size: 259248 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 403016 Color: 1
Size: 308355 Color: 1
Size: 288630 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 403494 Color: 1
Size: 306491 Color: 1
Size: 290016 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 403495 Color: 1
Size: 323944 Color: 1
Size: 272562 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 403563 Color: 1
Size: 336156 Color: 1
Size: 260282 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 403588 Color: 1
Size: 301103 Color: 1
Size: 295310 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 403813 Color: 1
Size: 333103 Color: 1
Size: 263085 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 404256 Color: 1
Size: 316047 Color: 1
Size: 279698 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 404416 Color: 1
Size: 314762 Color: 1
Size: 280823 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 404936 Color: 1
Size: 301297 Color: 1
Size: 293768 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 407914 Color: 1
Size: 311555 Color: 1
Size: 280532 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 407922 Color: 1
Size: 311161 Color: 1
Size: 280918 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 408016 Color: 1
Size: 321177 Color: 1
Size: 270808 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 408033 Color: 1
Size: 321525 Color: 1
Size: 270443 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 408472 Color: 1
Size: 296623 Color: 1
Size: 294906 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 408516 Color: 1
Size: 303256 Color: 1
Size: 288229 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 409214 Color: 1
Size: 307341 Color: 1
Size: 283446 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 409463 Color: 1
Size: 322677 Color: 1
Size: 267861 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 409571 Color: 1
Size: 321617 Color: 1
Size: 268813 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 409805 Color: 1
Size: 329607 Color: 1
Size: 260589 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 410203 Color: 1
Size: 308076 Color: 1
Size: 281722 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 410238 Color: 1
Size: 318708 Color: 1
Size: 271055 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 410867 Color: 1
Size: 336558 Color: 1
Size: 252576 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 411634 Color: 1
Size: 332073 Color: 1
Size: 256294 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 411949 Color: 1
Size: 300513 Color: 1
Size: 287539 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 411953 Color: 1
Size: 328753 Color: 1
Size: 259295 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 411977 Color: 1
Size: 327696 Color: 1
Size: 260328 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 412170 Color: 1
Size: 309432 Color: 1
Size: 278399 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 412209 Color: 1
Size: 318898 Color: 1
Size: 268894 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 413009 Color: 1
Size: 295617 Color: 0
Size: 291375 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 413415 Color: 1
Size: 330955 Color: 1
Size: 255631 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 413899 Color: 1
Size: 321571 Color: 1
Size: 264531 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 413929 Color: 1
Size: 321509 Color: 1
Size: 264563 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 413996 Color: 1
Size: 307853 Color: 1
Size: 278152 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 414430 Color: 1
Size: 318059 Color: 1
Size: 267512 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 414430 Color: 1
Size: 313415 Color: 1
Size: 272156 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 414785 Color: 1
Size: 313391 Color: 1
Size: 271825 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 415382 Color: 1
Size: 311381 Color: 1
Size: 273238 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 416005 Color: 1
Size: 322581 Color: 1
Size: 261415 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 416146 Color: 1
Size: 302121 Color: 0
Size: 281734 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 416201 Color: 1
Size: 324869 Color: 1
Size: 258931 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 416298 Color: 1
Size: 313063 Color: 1
Size: 270640 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 416583 Color: 1
Size: 298990 Color: 1
Size: 284428 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 417054 Color: 1
Size: 294920 Color: 1
Size: 288027 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 417578 Color: 1
Size: 320058 Color: 1
Size: 262365 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 417903 Color: 1
Size: 322085 Color: 1
Size: 260013 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 418070 Color: 1
Size: 319709 Color: 1
Size: 262222 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 418451 Color: 1
Size: 325221 Color: 1
Size: 256329 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 419224 Color: 1
Size: 303004 Color: 1
Size: 277773 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 419433 Color: 1
Size: 300789 Color: 1
Size: 279779 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 419503 Color: 1
Size: 305222 Color: 1
Size: 275276 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 419810 Color: 1
Size: 309416 Color: 1
Size: 270775 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 419906 Color: 1
Size: 307365 Color: 1
Size: 272730 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 420975 Color: 1
Size: 325391 Color: 1
Size: 253635 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 421206 Color: 1
Size: 306857 Color: 1
Size: 271938 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 421386 Color: 1
Size: 294994 Color: 1
Size: 283621 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 421470 Color: 1
Size: 307615 Color: 1
Size: 270916 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 421677 Color: 1
Size: 305205 Color: 1
Size: 273119 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 421759 Color: 1
Size: 310155 Color: 1
Size: 268087 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 422005 Color: 1
Size: 301136 Color: 1
Size: 276860 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 422044 Color: 1
Size: 312739 Color: 1
Size: 265218 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 422118 Color: 1
Size: 304501 Color: 1
Size: 273382 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 422369 Color: 1
Size: 302081 Color: 1
Size: 275551 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 422588 Color: 1
Size: 300438 Color: 1
Size: 276975 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 422785 Color: 1
Size: 314298 Color: 1
Size: 262918 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 422897 Color: 1
Size: 299819 Color: 1
Size: 277285 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 423849 Color: 1
Size: 304511 Color: 1
Size: 271641 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 423869 Color: 1
Size: 324071 Color: 1
Size: 252061 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 424042 Color: 1
Size: 308602 Color: 1
Size: 267357 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 424282 Color: 1
Size: 317670 Color: 1
Size: 258049 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 424854 Color: 1
Size: 319267 Color: 1
Size: 255880 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 425175 Color: 1
Size: 313993 Color: 1
Size: 260833 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 425656 Color: 1
Size: 297369 Color: 1
Size: 276976 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 426356 Color: 1
Size: 304443 Color: 1
Size: 269202 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 427505 Color: 1
Size: 321692 Color: 1
Size: 250804 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 427538 Color: 1
Size: 304161 Color: 1
Size: 268302 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 428278 Color: 1
Size: 310574 Color: 1
Size: 261149 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 428472 Color: 1
Size: 316834 Color: 1
Size: 254695 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 428740 Color: 1
Size: 302929 Color: 1
Size: 268332 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 428838 Color: 1
Size: 303468 Color: 1
Size: 267695 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 429526 Color: 1
Size: 294387 Color: 1
Size: 276088 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 429948 Color: 1
Size: 301871 Color: 1
Size: 268182 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 430103 Color: 1
Size: 315519 Color: 1
Size: 254379 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 430455 Color: 1
Size: 286369 Color: 1
Size: 283177 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 430854 Color: 1
Size: 308373 Color: 1
Size: 260774 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 430992 Color: 1
Size: 311994 Color: 1
Size: 257015 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 431247 Color: 1
Size: 310352 Color: 1
Size: 258402 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 431309 Color: 1
Size: 316662 Color: 1
Size: 252030 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 431506 Color: 1
Size: 284987 Color: 1
Size: 283508 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 431627 Color: 1
Size: 306887 Color: 1
Size: 261487 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 431864 Color: 1
Size: 294136 Color: 1
Size: 274001 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 432082 Color: 1
Size: 296465 Color: 1
Size: 271454 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 432639 Color: 1
Size: 307507 Color: 1
Size: 259855 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 432778 Color: 1
Size: 309127 Color: 1
Size: 258096 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 433293 Color: 1
Size: 300231 Color: 1
Size: 266477 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 434292 Color: 1
Size: 304745 Color: 1
Size: 260964 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 434504 Color: 1
Size: 299919 Color: 1
Size: 265578 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 435123 Color: 1
Size: 309913 Color: 1
Size: 254965 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 435332 Color: 1
Size: 311021 Color: 1
Size: 253648 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 435573 Color: 1
Size: 310980 Color: 1
Size: 253448 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 435589 Color: 1
Size: 306354 Color: 1
Size: 258058 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 435868 Color: 1
Size: 301101 Color: 1
Size: 263032 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 435876 Color: 1
Size: 291342 Color: 1
Size: 272783 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 435888 Color: 1
Size: 282821 Color: 1
Size: 281292 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 436324 Color: 1
Size: 304650 Color: 1
Size: 259027 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 436752 Color: 1
Size: 307520 Color: 1
Size: 255729 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 438546 Color: 1
Size: 298348 Color: 1
Size: 263107 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 439021 Color: 1
Size: 285588 Color: 1
Size: 275392 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 439443 Color: 1
Size: 300542 Color: 1
Size: 260016 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 439659 Color: 1
Size: 296576 Color: 1
Size: 263766 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 439734 Color: 1
Size: 306746 Color: 1
Size: 253521 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 439753 Color: 1
Size: 297042 Color: 1
Size: 263206 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 440449 Color: 1
Size: 298829 Color: 1
Size: 260723 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 440616 Color: 1
Size: 306133 Color: 1
Size: 253252 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 441117 Color: 1
Size: 286468 Color: 1
Size: 272416 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 441151 Color: 1
Size: 289743 Color: 0
Size: 269107 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 441332 Color: 1
Size: 304056 Color: 1
Size: 254613 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 441503 Color: 1
Size: 297222 Color: 1
Size: 261276 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 441776 Color: 1
Size: 296096 Color: 1
Size: 262129 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 441859 Color: 1
Size: 302587 Color: 1
Size: 255555 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 442495 Color: 1
Size: 293546 Color: 1
Size: 263960 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 442669 Color: 1
Size: 297475 Color: 1
Size: 259857 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 442712 Color: 1
Size: 281273 Color: 1
Size: 276016 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 443176 Color: 1
Size: 297817 Color: 1
Size: 259008 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 443473 Color: 1
Size: 289567 Color: 1
Size: 266961 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 443965 Color: 1
Size: 293809 Color: 1
Size: 262227 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 444088 Color: 1
Size: 288597 Color: 1
Size: 267316 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 444401 Color: 1
Size: 305495 Color: 1
Size: 250105 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 445388 Color: 1
Size: 283652 Color: 1
Size: 270961 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 445618 Color: 1
Size: 296114 Color: 1
Size: 258269 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 446049 Color: 1
Size: 283848 Color: 1
Size: 270104 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 446684 Color: 1
Size: 300607 Color: 1
Size: 252710 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 447021 Color: 1
Size: 286242 Color: 1
Size: 266738 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 447727 Color: 1
Size: 301524 Color: 1
Size: 250750 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 448057 Color: 1
Size: 285148 Color: 1
Size: 266796 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 448348 Color: 1
Size: 297240 Color: 1
Size: 254413 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 448478 Color: 1
Size: 289956 Color: 1
Size: 261567 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 448875 Color: 1
Size: 290786 Color: 1
Size: 260340 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 448889 Color: 1
Size: 285329 Color: 1
Size: 265783 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 449442 Color: 1
Size: 276092 Color: 1
Size: 274467 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 449675 Color: 1
Size: 282667 Color: 1
Size: 267659 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 449904 Color: 1
Size: 279836 Color: 0
Size: 270261 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 450221 Color: 1
Size: 277709 Color: 0
Size: 272071 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 451024 Color: 1
Size: 295345 Color: 1
Size: 253632 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 451137 Color: 1
Size: 291848 Color: 0
Size: 257016 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 451443 Color: 1
Size: 290013 Color: 1
Size: 258545 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 451674 Color: 1
Size: 293314 Color: 1
Size: 255013 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 451919 Color: 1
Size: 279173 Color: 1
Size: 268909 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 452016 Color: 1
Size: 293271 Color: 1
Size: 254714 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 452183 Color: 1
Size: 279949 Color: 1
Size: 267869 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 452624 Color: 1
Size: 284408 Color: 1
Size: 262969 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 452748 Color: 1
Size: 289228 Color: 1
Size: 258025 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 453153 Color: 1
Size: 279099 Color: 1
Size: 267749 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 453567 Color: 1
Size: 291560 Color: 1
Size: 254874 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 453610 Color: 1
Size: 288206 Color: 1
Size: 258185 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 453628 Color: 1
Size: 274427 Color: 1
Size: 271946 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 453679 Color: 1
Size: 287782 Color: 1
Size: 258540 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 453736 Color: 1
Size: 281262 Color: 1
Size: 265003 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 455024 Color: 1
Size: 287332 Color: 1
Size: 257645 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 455059 Color: 1
Size: 274269 Color: 1
Size: 270673 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 455411 Color: 1
Size: 290163 Color: 1
Size: 254427 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 455829 Color: 1
Size: 290784 Color: 1
Size: 253388 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 455835 Color: 1
Size: 287210 Color: 1
Size: 256956 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 455889 Color: 1
Size: 282667 Color: 1
Size: 261445 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 456262 Color: 1
Size: 288119 Color: 1
Size: 255620 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 456725 Color: 1
Size: 289290 Color: 1
Size: 253986 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 457791 Color: 1
Size: 291791 Color: 1
Size: 250419 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 458046 Color: 1
Size: 289018 Color: 1
Size: 252937 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 458069 Color: 1
Size: 280364 Color: 1
Size: 261568 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 458219 Color: 1
Size: 286812 Color: 1
Size: 254970 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 458344 Color: 1
Size: 276667 Color: 1
Size: 264990 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 458971 Color: 1
Size: 277510 Color: 1
Size: 263520 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 459156 Color: 1
Size: 277655 Color: 1
Size: 263190 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 459391 Color: 1
Size: 288504 Color: 1
Size: 252106 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 460022 Color: 1
Size: 289347 Color: 1
Size: 250632 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 460190 Color: 1
Size: 288912 Color: 1
Size: 250899 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 460790 Color: 1
Size: 276599 Color: 1
Size: 262612 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 461167 Color: 1
Size: 280297 Color: 1
Size: 258537 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 461587 Color: 1
Size: 285527 Color: 1
Size: 252887 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 462919 Color: 1
Size: 285501 Color: 1
Size: 251581 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 462998 Color: 1
Size: 273999 Color: 1
Size: 263004 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 463350 Color: 1
Size: 280705 Color: 1
Size: 255946 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 463880 Color: 1
Size: 276338 Color: 1
Size: 259783 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 463917 Color: 1
Size: 274969 Color: 1
Size: 261115 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 464200 Color: 1
Size: 276229 Color: 1
Size: 259572 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 464448 Color: 1
Size: 278115 Color: 1
Size: 257438 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 464550 Color: 1
Size: 283027 Color: 1
Size: 252424 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 464986 Color: 1
Size: 274534 Color: 1
Size: 260481 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 465745 Color: 1
Size: 272205 Color: 1
Size: 262051 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 465809 Color: 1
Size: 274700 Color: 1
Size: 259492 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 466052 Color: 1
Size: 274660 Color: 1
Size: 259289 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 466209 Color: 1
Size: 281441 Color: 1
Size: 252351 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 466922 Color: 1
Size: 279354 Color: 1
Size: 253725 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 468268 Color: 1
Size: 280675 Color: 1
Size: 251058 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 468418 Color: 1
Size: 276828 Color: 1
Size: 254755 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 468975 Color: 1
Size: 280272 Color: 1
Size: 250754 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 468986 Color: 1
Size: 269050 Color: 1
Size: 261965 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 469868 Color: 1
Size: 271941 Color: 1
Size: 258192 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 470259 Color: 1
Size: 276395 Color: 1
Size: 253347 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 470353 Color: 1
Size: 268765 Color: 1
Size: 260883 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 471255 Color: 1
Size: 271021 Color: 1
Size: 257725 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 471694 Color: 1
Size: 272977 Color: 1
Size: 255330 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 471821 Color: 1
Size: 266903 Color: 1
Size: 261277 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 471879 Color: 1
Size: 277020 Color: 1
Size: 251102 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 471915 Color: 1
Size: 266816 Color: 1
Size: 261270 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 471964 Color: 1
Size: 266282 Color: 1
Size: 261755 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 472616 Color: 1
Size: 276168 Color: 1
Size: 251217 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 472642 Color: 1
Size: 267376 Color: 1
Size: 259983 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 473806 Color: 1
Size: 274336 Color: 1
Size: 251859 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 474319 Color: 1
Size: 267536 Color: 1
Size: 258146 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 475114 Color: 1
Size: 272154 Color: 1
Size: 252733 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 475134 Color: 1
Size: 269480 Color: 1
Size: 255387 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 476172 Color: 1
Size: 272088 Color: 1
Size: 251741 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 476461 Color: 1
Size: 270838 Color: 1
Size: 252702 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 479387 Color: 1
Size: 262503 Color: 1
Size: 258111 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 479456 Color: 1
Size: 267933 Color: 1
Size: 252612 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 479835 Color: 1
Size: 263308 Color: 1
Size: 256858 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 480892 Color: 1
Size: 268545 Color: 1
Size: 250564 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 481341 Color: 1
Size: 264309 Color: 1
Size: 254351 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 482046 Color: 1
Size: 264653 Color: 1
Size: 253302 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 482825 Color: 1
Size: 267073 Color: 1
Size: 250103 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 483344 Color: 1
Size: 261926 Color: 1
Size: 254731 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 483819 Color: 1
Size: 260769 Color: 1
Size: 255413 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 483924 Color: 1
Size: 264999 Color: 1
Size: 251078 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 484344 Color: 1
Size: 258923 Color: 1
Size: 256734 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 484388 Color: 1
Size: 259269 Color: 1
Size: 256344 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 484493 Color: 1
Size: 261486 Color: 1
Size: 254022 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 485129 Color: 1
Size: 260284 Color: 1
Size: 254588 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 485260 Color: 1
Size: 264202 Color: 1
Size: 250539 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 485339 Color: 1
Size: 260769 Color: 1
Size: 253893 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 485350 Color: 1
Size: 259343 Color: 1
Size: 255308 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 485734 Color: 1
Size: 258748 Color: 1
Size: 255519 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 485801 Color: 1
Size: 258848 Color: 1
Size: 255352 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 486327 Color: 1
Size: 257790 Color: 1
Size: 255884 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 487017 Color: 1
Size: 261895 Color: 1
Size: 251089 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 487020 Color: 1
Size: 260386 Color: 1
Size: 252595 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 487268 Color: 1
Size: 259334 Color: 1
Size: 253399 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 487558 Color: 1
Size: 257700 Color: 1
Size: 254743 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 488107 Color: 1
Size: 259340 Color: 1
Size: 252554 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 488872 Color: 1
Size: 259786 Color: 1
Size: 251343 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 489183 Color: 1
Size: 258447 Color: 1
Size: 252371 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 489788 Color: 1
Size: 259044 Color: 1
Size: 251169 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 490120 Color: 1
Size: 259581 Color: 1
Size: 250300 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 490195 Color: 1
Size: 259576 Color: 1
Size: 250230 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 490241 Color: 1
Size: 257729 Color: 1
Size: 252031 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 490433 Color: 1
Size: 257963 Color: 1
Size: 251605 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 490623 Color: 1
Size: 258096 Color: 1
Size: 251282 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 492104 Color: 1
Size: 257274 Color: 1
Size: 250623 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 492772 Color: 1
Size: 254604 Color: 1
Size: 252625 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 492902 Color: 1
Size: 254683 Color: 1
Size: 252416 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 493371 Color: 1
Size: 255749 Color: 1
Size: 250881 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 493694 Color: 1
Size: 254635 Color: 1
Size: 251672 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 493985 Color: 1
Size: 253384 Color: 1
Size: 252632 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 494139 Color: 1
Size: 255573 Color: 1
Size: 250289 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 494259 Color: 1
Size: 253027 Color: 1
Size: 252715 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 494856 Color: 1
Size: 253263 Color: 1
Size: 251882 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 495418 Color: 1
Size: 254351 Color: 1
Size: 250232 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 495471 Color: 1
Size: 252320 Color: 1
Size: 252210 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 495566 Color: 1
Size: 252840 Color: 1
Size: 251595 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 495796 Color: 1
Size: 253574 Color: 1
Size: 250631 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 496933 Color: 1
Size: 251583 Color: 1
Size: 251485 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 497389 Color: 1
Size: 252568 Color: 1
Size: 250044 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 497690 Color: 1
Size: 251541 Color: 1
Size: 250770 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 498086 Color: 1
Size: 250973 Color: 1
Size: 250942 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 498244 Color: 1
Size: 251705 Color: 1
Size: 250052 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 498699 Color: 1
Size: 250771 Color: 1
Size: 250531 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 499059 Color: 1
Size: 250903 Color: 1
Size: 250039 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 499291 Color: 1
Size: 250422 Color: 1
Size: 250288 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 499327 Color: 1
Size: 250358 Color: 1
Size: 250316 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 499847 Color: 1
Size: 250134 Color: 1
Size: 250020 Color: 0

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 421431 Color: 1
Size: 302223 Color: 1
Size: 276346 Color: 0

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 357612 Color: 1
Size: 342034 Color: 1
Size: 300354 Color: 0

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 361755 Color: 1
Size: 342116 Color: 1
Size: 296129 Color: 0

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 367303 Color: 1
Size: 348689 Color: 1
Size: 284008 Color: 0

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 368074 Color: 1
Size: 348670 Color: 1
Size: 283256 Color: 0

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 373721 Color: 1
Size: 337260 Color: 1
Size: 289019 Color: 0

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 375357 Color: 1
Size: 324938 Color: 1
Size: 299705 Color: 0

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 377859 Color: 1
Size: 329490 Color: 1
Size: 292651 Color: 0

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 379103 Color: 1
Size: 349482 Color: 1
Size: 271415 Color: 0

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 379288 Color: 1
Size: 343084 Color: 1
Size: 277628 Color: 0

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 379923 Color: 1
Size: 353063 Color: 1
Size: 267014 Color: 0

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 380484 Color: 1
Size: 357054 Color: 1
Size: 262462 Color: 0

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 381489 Color: 1
Size: 351104 Color: 1
Size: 267407 Color: 0

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 383351 Color: 1
Size: 342331 Color: 1
Size: 274318 Color: 0

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 384004 Color: 1
Size: 350074 Color: 1
Size: 265922 Color: 0

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 387316 Color: 1
Size: 318860 Color: 0
Size: 293824 Color: 1

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 387908 Color: 1
Size: 327496 Color: 1
Size: 284596 Color: 0

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 388107 Color: 1
Size: 325585 Color: 1
Size: 286308 Color: 0

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 389731 Color: 1
Size: 337493 Color: 1
Size: 272776 Color: 0

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 389930 Color: 1
Size: 351463 Color: 1
Size: 258607 Color: 0

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 390353 Color: 1
Size: 317687 Color: 1
Size: 291960 Color: 0

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 390850 Color: 1
Size: 343428 Color: 1
Size: 265722 Color: 0

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 392892 Color: 1
Size: 313144 Color: 1
Size: 293964 Color: 0

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 393445 Color: 1
Size: 325393 Color: 1
Size: 281162 Color: 0

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 394330 Color: 1
Size: 312226 Color: 0
Size: 293444 Color: 1

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 394395 Color: 1
Size: 349091 Color: 1
Size: 256514 Color: 0

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 394460 Color: 1
Size: 339376 Color: 1
Size: 266164 Color: 0

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 394626 Color: 1
Size: 319374 Color: 1
Size: 286000 Color: 0

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 394848 Color: 1
Size: 311755 Color: 1
Size: 293397 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 397613 Color: 1
Size: 322470 Color: 0
Size: 279917 Color: 1

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 397923 Color: 1
Size: 341402 Color: 1
Size: 260675 Color: 0

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 398143 Color: 1
Size: 322002 Color: 1
Size: 279855 Color: 0

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 398593 Color: 1
Size: 315094 Color: 0
Size: 286313 Color: 1

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 399607 Color: 1
Size: 338948 Color: 1
Size: 261445 Color: 0

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 399721 Color: 1
Size: 336462 Color: 1
Size: 263817 Color: 0

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 400288 Color: 1
Size: 331294 Color: 1
Size: 268418 Color: 0

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 401529 Color: 1
Size: 321360 Color: 1
Size: 277111 Color: 0

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 403904 Color: 1
Size: 301866 Color: 1
Size: 294230 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 404208 Color: 1
Size: 333629 Color: 1
Size: 262163 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 404466 Color: 1
Size: 313668 Color: 1
Size: 281866 Color: 0

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 405487 Color: 1
Size: 336671 Color: 1
Size: 257842 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 406061 Color: 1
Size: 312322 Color: 1
Size: 281617 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 407198 Color: 1
Size: 301170 Color: 1
Size: 291632 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 407558 Color: 1
Size: 324782 Color: 1
Size: 267660 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 409273 Color: 1
Size: 309205 Color: 1
Size: 281522 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 409649 Color: 1
Size: 298084 Color: 1
Size: 292267 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 411175 Color: 1
Size: 318380 Color: 1
Size: 270445 Color: 0

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 412058 Color: 1
Size: 327367 Color: 1
Size: 260575 Color: 0

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 418349 Color: 1
Size: 292547 Color: 0
Size: 289104 Color: 1

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 421018 Color: 1
Size: 322448 Color: 1
Size: 256534 Color: 0

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 422307 Color: 1
Size: 326649 Color: 1
Size: 251044 Color: 0

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 422520 Color: 1
Size: 325892 Color: 1
Size: 251588 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 423775 Color: 1
Size: 297673 Color: 0
Size: 278552 Color: 1

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 427811 Color: 1
Size: 301841 Color: 1
Size: 270348 Color: 0

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 429821 Color: 1
Size: 310255 Color: 1
Size: 259924 Color: 0

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 437657 Color: 1
Size: 282510 Color: 1
Size: 279833 Color: 0

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 439356 Color: 1
Size: 296055 Color: 1
Size: 264589 Color: 0

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 444178 Color: 1
Size: 290411 Color: 1
Size: 265411 Color: 0

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 444440 Color: 1
Size: 287381 Color: 1
Size: 268179 Color: 0

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 444783 Color: 1
Size: 287608 Color: 1
Size: 267609 Color: 0

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 444983 Color: 1
Size: 284881 Color: 1
Size: 270136 Color: 0

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 449579 Color: 1
Size: 291365 Color: 0
Size: 259056 Color: 1

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 449699 Color: 1
Size: 285558 Color: 1
Size: 264743 Color: 0

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 450599 Color: 1
Size: 285876 Color: 1
Size: 263525 Color: 0

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 452524 Color: 1
Size: 291643 Color: 1
Size: 255833 Color: 0

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 452634 Color: 1
Size: 284662 Color: 1
Size: 262704 Color: 0

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 453563 Color: 1
Size: 274878 Color: 1
Size: 271559 Color: 0

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 454134 Color: 1
Size: 292553 Color: 1
Size: 253313 Color: 0

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 456230 Color: 1
Size: 284167 Color: 1
Size: 259603 Color: 0

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 457192 Color: 1
Size: 288739 Color: 1
Size: 254069 Color: 0

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 457587 Color: 1
Size: 284508 Color: 1
Size: 257905 Color: 0

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 460999 Color: 1
Size: 282237 Color: 1
Size: 256764 Color: 0

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 462785 Color: 1
Size: 279583 Color: 1
Size: 257632 Color: 0

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 467135 Color: 1
Size: 282591 Color: 1
Size: 250274 Color: 0

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 468457 Color: 1
Size: 278961 Color: 1
Size: 252582 Color: 0

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 470166 Color: 1
Size: 278693 Color: 1
Size: 251141 Color: 0

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 473023 Color: 1
Size: 266805 Color: 1
Size: 260172 Color: 0

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 478397 Color: 1
Size: 268403 Color: 1
Size: 253200 Color: 0

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 481727 Color: 1
Size: 266601 Color: 1
Size: 251672 Color: 0

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 483701 Color: 1
Size: 263895 Color: 1
Size: 252404 Color: 0

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 485751 Color: 1
Size: 262908 Color: 1
Size: 251341 Color: 0

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 485755 Color: 1
Size: 262457 Color: 1
Size: 251788 Color: 0

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 487112 Color: 1
Size: 259884 Color: 1
Size: 253004 Color: 0

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 373394 Color: 1
Size: 359257 Color: 1
Size: 267349 Color: 0

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 353171 Color: 1
Size: 325419 Color: 1
Size: 321410 Color: 0

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 351341 Color: 1
Size: 332745 Color: 1
Size: 315914 Color: 0

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 407639 Color: 1
Size: 324949 Color: 1
Size: 267412 Color: 0

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 388776 Color: 1
Size: 327509 Color: 1
Size: 283714 Color: 0

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 360016 Color: 1
Size: 340578 Color: 1
Size: 299405 Color: 0

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 365999 Color: 1
Size: 326370 Color: 1
Size: 307630 Color: 0

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 366839 Color: 1
Size: 332821 Color: 1
Size: 300339 Color: 0

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 369855 Color: 1
Size: 342810 Color: 1
Size: 287334 Color: 0

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 370609 Color: 1
Size: 350749 Color: 1
Size: 278641 Color: 0

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 382026 Color: 1
Size: 365900 Color: 1
Size: 252073 Color: 0

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 384003 Color: 1
Size: 316772 Color: 1
Size: 299224 Color: 0

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 384116 Color: 1
Size: 332388 Color: 1
Size: 283495 Color: 0

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 387555 Color: 1
Size: 351386 Color: 1
Size: 261058 Color: 0

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 388086 Color: 1
Size: 316441 Color: 1
Size: 295472 Color: 0

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 389494 Color: 1
Size: 331573 Color: 1
Size: 278932 Color: 0

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 395531 Color: 1
Size: 347817 Color: 1
Size: 256651 Color: 0

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 396398 Color: 1
Size: 302394 Color: 1
Size: 301207 Color: 0

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 396866 Color: 1
Size: 329902 Color: 1
Size: 273231 Color: 0

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 399661 Color: 1
Size: 303217 Color: 1
Size: 297121 Color: 0

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 405990 Color: 1
Size: 323867 Color: 1
Size: 270142 Color: 0

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 407631 Color: 1
Size: 334553 Color: 1
Size: 257815 Color: 0

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 408263 Color: 1
Size: 307971 Color: 0
Size: 283765 Color: 1

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 419566 Color: 1
Size: 294318 Color: 0
Size: 286115 Color: 1

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 420447 Color: 1
Size: 316946 Color: 1
Size: 262606 Color: 0

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 421191 Color: 1
Size: 305475 Color: 1
Size: 273333 Color: 0

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 428555 Color: 1
Size: 311784 Color: 1
Size: 259660 Color: 0

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 428662 Color: 1
Size: 317841 Color: 1
Size: 253496 Color: 0

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 432192 Color: 1
Size: 298497 Color: 1
Size: 269310 Color: 0

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 432357 Color: 1
Size: 309848 Color: 1
Size: 257794 Color: 0

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 438527 Color: 1
Size: 287245 Color: 1
Size: 274227 Color: 0

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 439715 Color: 1
Size: 299370 Color: 1
Size: 260914 Color: 0

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 439849 Color: 1
Size: 304430 Color: 1
Size: 255720 Color: 0

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 445560 Color: 1
Size: 295033 Color: 1
Size: 259406 Color: 0

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 447435 Color: 1
Size: 283940 Color: 1
Size: 268624 Color: 0

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 448241 Color: 1
Size: 284697 Color: 1
Size: 267061 Color: 0

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 452158 Color: 1
Size: 284395 Color: 1
Size: 263446 Color: 0

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 454695 Color: 1
Size: 280313 Color: 1
Size: 264991 Color: 0

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 457758 Color: 1
Size: 290421 Color: 1
Size: 251820 Color: 0

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 458594 Color: 1
Size: 279781 Color: 1
Size: 261624 Color: 0

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 459481 Color: 1
Size: 277707 Color: 1
Size: 262811 Color: 0

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 474863 Color: 1
Size: 270626 Color: 1
Size: 254510 Color: 0

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 484511 Color: 1
Size: 260037 Color: 1
Size: 255451 Color: 0

Bin 507: 2 of cap free
Amount of items: 3
Items: 
Size: 484555 Color: 1
Size: 264353 Color: 1
Size: 251091 Color: 0

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 359442 Color: 1
Size: 321934 Color: 0
Size: 318623 Color: 1

Bin 509: 2 of cap free
Amount of items: 3
Items: 
Size: 351054 Color: 1
Size: 349304 Color: 1
Size: 299641 Color: 0

Bin 510: 2 of cap free
Amount of items: 3
Items: 
Size: 360834 Color: 1
Size: 324541 Color: 1
Size: 314624 Color: 0

Bin 511: 2 of cap free
Amount of items: 3
Items: 
Size: 351816 Color: 1
Size: 335014 Color: 1
Size: 313169 Color: 0

Bin 512: 2 of cap free
Amount of items: 3
Items: 
Size: 348965 Color: 1
Size: 339292 Color: 1
Size: 311742 Color: 0

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 361371 Color: 1
Size: 333095 Color: 1
Size: 305532 Color: 0

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 369622 Color: 1
Size: 351440 Color: 1
Size: 278936 Color: 0

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 369719 Color: 1
Size: 325413 Color: 1
Size: 304866 Color: 0

Bin 516: 3 of cap free
Amount of items: 3
Items: 
Size: 370528 Color: 1
Size: 336034 Color: 1
Size: 293436 Color: 0

Bin 517: 3 of cap free
Amount of items: 3
Items: 
Size: 371647 Color: 1
Size: 337187 Color: 1
Size: 291164 Color: 0

Bin 518: 3 of cap free
Amount of items: 3
Items: 
Size: 372226 Color: 1
Size: 322520 Color: 1
Size: 305252 Color: 0

Bin 519: 3 of cap free
Amount of items: 3
Items: 
Size: 375003 Color: 1
Size: 342943 Color: 1
Size: 282052 Color: 0

Bin 520: 3 of cap free
Amount of items: 3
Items: 
Size: 378532 Color: 1
Size: 335779 Color: 1
Size: 285687 Color: 0

Bin 521: 3 of cap free
Amount of items: 3
Items: 
Size: 378618 Color: 1
Size: 341520 Color: 1
Size: 279860 Color: 0

Bin 522: 3 of cap free
Amount of items: 3
Items: 
Size: 378960 Color: 1
Size: 351234 Color: 1
Size: 269804 Color: 0

Bin 523: 3 of cap free
Amount of items: 3
Items: 
Size: 384080 Color: 1
Size: 318545 Color: 1
Size: 297373 Color: 0

Bin 524: 3 of cap free
Amount of items: 3
Items: 
Size: 385577 Color: 1
Size: 343506 Color: 1
Size: 270915 Color: 0

Bin 525: 3 of cap free
Amount of items: 3
Items: 
Size: 386272 Color: 1
Size: 330179 Color: 1
Size: 283547 Color: 0

Bin 526: 3 of cap free
Amount of items: 3
Items: 
Size: 387780 Color: 1
Size: 356064 Color: 1
Size: 256154 Color: 0

Bin 527: 3 of cap free
Amount of items: 3
Items: 
Size: 388231 Color: 1
Size: 355661 Color: 1
Size: 256106 Color: 0

Bin 528: 3 of cap free
Amount of items: 3
Items: 
Size: 388756 Color: 1
Size: 325134 Color: 1
Size: 286108 Color: 0

Bin 529: 3 of cap free
Amount of items: 3
Items: 
Size: 398431 Color: 1
Size: 332243 Color: 1
Size: 269324 Color: 0

Bin 530: 3 of cap free
Amount of items: 3
Items: 
Size: 399249 Color: 1
Size: 315573 Color: 1
Size: 285176 Color: 0

Bin 531: 3 of cap free
Amount of items: 3
Items: 
Size: 400435 Color: 1
Size: 326539 Color: 1
Size: 273024 Color: 0

Bin 532: 3 of cap free
Amount of items: 3
Items: 
Size: 401839 Color: 1
Size: 322659 Color: 1
Size: 275500 Color: 0

Bin 533: 3 of cap free
Amount of items: 3
Items: 
Size: 403908 Color: 1
Size: 325829 Color: 1
Size: 270261 Color: 0

Bin 534: 3 of cap free
Amount of items: 3
Items: 
Size: 405625 Color: 1
Size: 305234 Color: 0
Size: 289139 Color: 1

Bin 535: 3 of cap free
Amount of items: 3
Items: 
Size: 419555 Color: 1
Size: 327028 Color: 1
Size: 253415 Color: 0

Bin 536: 3 of cap free
Amount of items: 3
Items: 
Size: 432032 Color: 1
Size: 316843 Color: 1
Size: 251123 Color: 0

Bin 537: 3 of cap free
Amount of items: 3
Items: 
Size: 432672 Color: 1
Size: 301928 Color: 1
Size: 265398 Color: 0

Bin 538: 3 of cap free
Amount of items: 3
Items: 
Size: 433144 Color: 1
Size: 286490 Color: 0
Size: 280364 Color: 1

Bin 539: 3 of cap free
Amount of items: 3
Items: 
Size: 445912 Color: 1
Size: 300645 Color: 1
Size: 253441 Color: 0

Bin 540: 3 of cap free
Amount of items: 3
Items: 
Size: 470768 Color: 1
Size: 275589 Color: 1
Size: 253641 Color: 0

Bin 541: 3 of cap free
Amount of items: 3
Items: 
Size: 471178 Color: 1
Size: 271786 Color: 1
Size: 257034 Color: 0

Bin 542: 3 of cap free
Amount of items: 3
Items: 
Size: 473463 Color: 1
Size: 269764 Color: 1
Size: 256771 Color: 0

Bin 543: 3 of cap free
Amount of items: 3
Items: 
Size: 474929 Color: 1
Size: 264922 Color: 1
Size: 260147 Color: 0

Bin 544: 3 of cap free
Amount of items: 3
Items: 
Size: 483466 Color: 1
Size: 262882 Color: 1
Size: 253650 Color: 0

Bin 545: 3 of cap free
Amount of items: 3
Items: 
Size: 355217 Color: 1
Size: 345083 Color: 1
Size: 299698 Color: 0

Bin 546: 4 of cap free
Amount of items: 3
Items: 
Size: 461900 Color: 1
Size: 278690 Color: 0
Size: 259407 Color: 1

Bin 547: 4 of cap free
Amount of items: 3
Items: 
Size: 364855 Color: 1
Size: 343463 Color: 1
Size: 291679 Color: 0

Bin 548: 4 of cap free
Amount of items: 3
Items: 
Size: 367939 Color: 1
Size: 346751 Color: 1
Size: 285307 Color: 0

Bin 549: 4 of cap free
Amount of items: 3
Items: 
Size: 368668 Color: 1
Size: 351284 Color: 1
Size: 280045 Color: 0

Bin 550: 4 of cap free
Amount of items: 3
Items: 
Size: 374693 Color: 1
Size: 331615 Color: 1
Size: 293689 Color: 0

Bin 551: 4 of cap free
Amount of items: 3
Items: 
Size: 377293 Color: 1
Size: 330269 Color: 1
Size: 292435 Color: 0

Bin 552: 4 of cap free
Amount of items: 3
Items: 
Size: 378225 Color: 1
Size: 357441 Color: 1
Size: 264331 Color: 0

Bin 553: 4 of cap free
Amount of items: 3
Items: 
Size: 380408 Color: 1
Size: 344970 Color: 1
Size: 274619 Color: 0

Bin 554: 4 of cap free
Amount of items: 3
Items: 
Size: 385422 Color: 1
Size: 340382 Color: 1
Size: 274193 Color: 0

Bin 555: 4 of cap free
Amount of items: 3
Items: 
Size: 399652 Color: 1
Size: 310783 Color: 0
Size: 289562 Color: 1

Bin 556: 4 of cap free
Amount of items: 3
Items: 
Size: 402511 Color: 1
Size: 331877 Color: 1
Size: 265609 Color: 0

Bin 557: 4 of cap free
Amount of items: 3
Items: 
Size: 405361 Color: 1
Size: 325801 Color: 1
Size: 268835 Color: 0

Bin 558: 4 of cap free
Amount of items: 3
Items: 
Size: 405371 Color: 1
Size: 316184 Color: 1
Size: 278442 Color: 0

Bin 559: 4 of cap free
Amount of items: 3
Items: 
Size: 406751 Color: 1
Size: 324096 Color: 1
Size: 269150 Color: 0

Bin 560: 4 of cap free
Amount of items: 3
Items: 
Size: 415875 Color: 1
Size: 307279 Color: 1
Size: 276843 Color: 0

Bin 561: 4 of cap free
Amount of items: 3
Items: 
Size: 427565 Color: 1
Size: 317321 Color: 1
Size: 255111 Color: 0

Bin 562: 4 of cap free
Amount of items: 3
Items: 
Size: 440710 Color: 1
Size: 297972 Color: 1
Size: 261315 Color: 0

Bin 563: 4 of cap free
Amount of items: 3
Items: 
Size: 446421 Color: 1
Size: 292357 Color: 1
Size: 261219 Color: 0

Bin 564: 4 of cap free
Amount of items: 3
Items: 
Size: 459923 Color: 1
Size: 282681 Color: 1
Size: 257393 Color: 0

Bin 565: 4 of cap free
Amount of items: 3
Items: 
Size: 360839 Color: 1
Size: 343258 Color: 1
Size: 295900 Color: 0

Bin 566: 4 of cap free
Amount of items: 3
Items: 
Size: 393942 Color: 1
Size: 315461 Color: 0
Size: 290594 Color: 1

Bin 567: 4 of cap free
Amount of items: 3
Items: 
Size: 360752 Color: 1
Size: 333393 Color: 1
Size: 305852 Color: 0

Bin 568: 5 of cap free
Amount of items: 3
Items: 
Size: 366863 Color: 1
Size: 316659 Color: 1
Size: 316474 Color: 0

Bin 569: 5 of cap free
Amount of items: 3
Items: 
Size: 368850 Color: 1
Size: 352099 Color: 1
Size: 279047 Color: 0

Bin 570: 5 of cap free
Amount of items: 3
Items: 
Size: 370123 Color: 1
Size: 338017 Color: 1
Size: 291856 Color: 0

Bin 571: 5 of cap free
Amount of items: 3
Items: 
Size: 373145 Color: 1
Size: 365904 Color: 1
Size: 260947 Color: 0

Bin 572: 5 of cap free
Amount of items: 3
Items: 
Size: 379923 Color: 1
Size: 335864 Color: 1
Size: 284209 Color: 0

Bin 573: 5 of cap free
Amount of items: 3
Items: 
Size: 416368 Color: 1
Size: 306313 Color: 1
Size: 277315 Color: 0

Bin 574: 5 of cap free
Amount of items: 3
Items: 
Size: 437067 Color: 1
Size: 289368 Color: 0
Size: 273561 Color: 1

Bin 575: 5 of cap free
Amount of items: 3
Items: 
Size: 469932 Color: 1
Size: 277328 Color: 1
Size: 252736 Color: 0

Bin 576: 5 of cap free
Amount of items: 3
Items: 
Size: 471011 Color: 1
Size: 268190 Color: 1
Size: 260795 Color: 0

Bin 577: 5 of cap free
Amount of items: 3
Items: 
Size: 473741 Color: 1
Size: 272897 Color: 1
Size: 253358 Color: 0

Bin 578: 5 of cap free
Amount of items: 3
Items: 
Size: 361907 Color: 1
Size: 319174 Color: 1
Size: 318915 Color: 0

Bin 579: 6 of cap free
Amount of items: 3
Items: 
Size: 359268 Color: 1
Size: 330318 Color: 1
Size: 310409 Color: 0

Bin 580: 6 of cap free
Amount of items: 3
Items: 
Size: 368750 Color: 1
Size: 342026 Color: 1
Size: 289219 Color: 0

Bin 581: 6 of cap free
Amount of items: 3
Items: 
Size: 373895 Color: 1
Size: 321982 Color: 1
Size: 304118 Color: 0

Bin 582: 6 of cap free
Amount of items: 3
Items: 
Size: 483446 Color: 1
Size: 264636 Color: 1
Size: 251913 Color: 0

Bin 583: 7 of cap free
Amount of items: 3
Items: 
Size: 361592 Color: 1
Size: 355082 Color: 1
Size: 283320 Color: 0

Bin 584: 7 of cap free
Amount of items: 3
Items: 
Size: 362933 Color: 1
Size: 339558 Color: 1
Size: 297503 Color: 0

Bin 585: 7 of cap free
Amount of items: 3
Items: 
Size: 364333 Color: 1
Size: 339799 Color: 1
Size: 295862 Color: 0

Bin 586: 7 of cap free
Amount of items: 3
Items: 
Size: 375057 Color: 1
Size: 333933 Color: 1
Size: 291004 Color: 0

Bin 587: 7 of cap free
Amount of items: 3
Items: 
Size: 376552 Color: 1
Size: 330381 Color: 1
Size: 293061 Color: 0

Bin 588: 7 of cap free
Amount of items: 3
Items: 
Size: 377068 Color: 1
Size: 343482 Color: 1
Size: 279444 Color: 0

Bin 589: 7 of cap free
Amount of items: 3
Items: 
Size: 379877 Color: 1
Size: 335280 Color: 1
Size: 284837 Color: 0

Bin 590: 7 of cap free
Amount of items: 3
Items: 
Size: 475206 Color: 1
Size: 267990 Color: 0
Size: 256798 Color: 1

Bin 591: 7 of cap free
Amount of items: 3
Items: 
Size: 353522 Color: 1
Size: 335054 Color: 1
Size: 311418 Color: 0

Bin 592: 8 of cap free
Amount of items: 3
Items: 
Size: 370075 Color: 1
Size: 337445 Color: 1
Size: 292473 Color: 0

Bin 593: 8 of cap free
Amount of items: 3
Items: 
Size: 354921 Color: 1
Size: 329767 Color: 1
Size: 315305 Color: 0

Bin 594: 8 of cap free
Amount of items: 3
Items: 
Size: 359231 Color: 1
Size: 354283 Color: 1
Size: 286479 Color: 0

Bin 595: 8 of cap free
Amount of items: 3
Items: 
Size: 353421 Color: 1
Size: 349028 Color: 1
Size: 297544 Color: 0

Bin 596: 8 of cap free
Amount of items: 3
Items: 
Size: 351275 Color: 1
Size: 346182 Color: 1
Size: 302536 Color: 0

Bin 597: 9 of cap free
Amount of items: 3
Items: 
Size: 359760 Color: 1
Size: 342168 Color: 1
Size: 298064 Color: 0

Bin 598: 9 of cap free
Amount of items: 3
Items: 
Size: 361077 Color: 1
Size: 325885 Color: 1
Size: 313030 Color: 0

Bin 599: 9 of cap free
Amount of items: 3
Items: 
Size: 368265 Color: 1
Size: 337560 Color: 1
Size: 294167 Color: 0

Bin 600: 9 of cap free
Amount of items: 3
Items: 
Size: 372272 Color: 1
Size: 322198 Color: 1
Size: 305522 Color: 0

Bin 601: 9 of cap free
Amount of items: 3
Items: 
Size: 372365 Color: 1
Size: 345094 Color: 1
Size: 282533 Color: 0

Bin 602: 9 of cap free
Amount of items: 3
Items: 
Size: 375609 Color: 1
Size: 325636 Color: 1
Size: 298747 Color: 0

Bin 603: 9 of cap free
Amount of items: 3
Items: 
Size: 387927 Color: 1
Size: 349404 Color: 1
Size: 262661 Color: 0

Bin 604: 9 of cap free
Amount of items: 3
Items: 
Size: 448522 Color: 1
Size: 282445 Color: 0
Size: 269025 Color: 1

Bin 605: 9 of cap free
Amount of items: 3
Items: 
Size: 468482 Color: 1
Size: 273364 Color: 1
Size: 258146 Color: 0

Bin 606: 9 of cap free
Amount of items: 3
Items: 
Size: 474036 Color: 1
Size: 266629 Color: 1
Size: 259327 Color: 0

Bin 607: 11 of cap free
Amount of items: 3
Items: 
Size: 363915 Color: 1
Size: 334085 Color: 1
Size: 301990 Color: 0

Bin 608: 11 of cap free
Amount of items: 3
Items: 
Size: 385974 Color: 1
Size: 329839 Color: 1
Size: 284177 Color: 0

Bin 609: 12 of cap free
Amount of items: 3
Items: 
Size: 369199 Color: 1
Size: 338799 Color: 1
Size: 291991 Color: 0

Bin 610: 12 of cap free
Amount of items: 3
Items: 
Size: 359073 Color: 1
Size: 322610 Color: 1
Size: 318306 Color: 0

Bin 611: 12 of cap free
Amount of items: 3
Items: 
Size: 349011 Color: 1
Size: 337799 Color: 1
Size: 313179 Color: 0

Bin 612: 13 of cap free
Amount of items: 3
Items: 
Size: 369969 Color: 1
Size: 367963 Color: 1
Size: 262056 Color: 0

Bin 613: 14 of cap free
Amount of items: 3
Items: 
Size: 365113 Color: 1
Size: 339853 Color: 1
Size: 295021 Color: 0

Bin 614: 14 of cap free
Amount of items: 3
Items: 
Size: 364645 Color: 1
Size: 356747 Color: 1
Size: 278595 Color: 0

Bin 615: 14 of cap free
Amount of items: 3
Items: 
Size: 351675 Color: 1
Size: 333037 Color: 1
Size: 315275 Color: 0

Bin 616: 15 of cap free
Amount of items: 3
Items: 
Size: 366369 Color: 1
Size: 329413 Color: 1
Size: 304204 Color: 0

Bin 617: 15 of cap free
Amount of items: 3
Items: 
Size: 378055 Color: 1
Size: 328943 Color: 1
Size: 292988 Color: 0

Bin 618: 15 of cap free
Amount of items: 3
Items: 
Size: 352611 Color: 1
Size: 348381 Color: 1
Size: 298994 Color: 0

Bin 619: 16 of cap free
Amount of items: 3
Items: 
Size: 367067 Color: 1
Size: 349236 Color: 1
Size: 283682 Color: 0

Bin 620: 19 of cap free
Amount of items: 3
Items: 
Size: 366634 Color: 1
Size: 353352 Color: 1
Size: 279996 Color: 0

Bin 621: 19 of cap free
Amount of items: 3
Items: 
Size: 348933 Color: 1
Size: 347350 Color: 1
Size: 303699 Color: 0

Bin 622: 20 of cap free
Amount of items: 3
Items: 
Size: 367562 Color: 1
Size: 339491 Color: 1
Size: 292928 Color: 0

Bin 623: 20 of cap free
Amount of items: 3
Items: 
Size: 351922 Color: 1
Size: 340113 Color: 1
Size: 307946 Color: 0

Bin 624: 21 of cap free
Amount of items: 3
Items: 
Size: 347749 Color: 1
Size: 344734 Color: 1
Size: 307497 Color: 0

Bin 625: 22 of cap free
Amount of items: 3
Items: 
Size: 346659 Color: 1
Size: 345264 Color: 1
Size: 308056 Color: 0

Bin 626: 23 of cap free
Amount of items: 3
Items: 
Size: 371719 Color: 1
Size: 368236 Color: 1
Size: 260023 Color: 0

Bin 627: 24 of cap free
Amount of items: 3
Items: 
Size: 368159 Color: 1
Size: 338725 Color: 1
Size: 293093 Color: 0

Bin 628: 25 of cap free
Amount of items: 3
Items: 
Size: 358427 Color: 1
Size: 341418 Color: 1
Size: 300131 Color: 0

Bin 629: 26 of cap free
Amount of items: 3
Items: 
Size: 362525 Color: 1
Size: 346166 Color: 1
Size: 291284 Color: 0

Bin 630: 26 of cap free
Amount of items: 3
Items: 
Size: 354350 Color: 1
Size: 331625 Color: 1
Size: 314000 Color: 0

Bin 631: 29 of cap free
Amount of items: 3
Items: 
Size: 366027 Color: 1
Size: 343255 Color: 1
Size: 290690 Color: 0

Bin 632: 35 of cap free
Amount of items: 3
Items: 
Size: 376598 Color: 1
Size: 327324 Color: 0
Size: 296044 Color: 1

Bin 633: 41 of cap free
Amount of items: 3
Items: 
Size: 383135 Color: 1
Size: 345049 Color: 1
Size: 271776 Color: 0

Bin 634: 42 of cap free
Amount of items: 3
Items: 
Size: 352985 Color: 1
Size: 343090 Color: 1
Size: 303884 Color: 0

Bin 635: 42 of cap free
Amount of items: 3
Items: 
Size: 357727 Color: 1
Size: 345738 Color: 1
Size: 296494 Color: 0

Bin 636: 49 of cap free
Amount of items: 3
Items: 
Size: 361400 Color: 1
Size: 344411 Color: 1
Size: 294141 Color: 0

Bin 637: 51 of cap free
Amount of items: 3
Items: 
Size: 388098 Color: 1
Size: 324371 Color: 0
Size: 287481 Color: 1

Bin 638: 52 of cap free
Amount of items: 3
Items: 
Size: 350336 Color: 1
Size: 347247 Color: 1
Size: 302366 Color: 0

Bin 639: 59 of cap free
Amount of items: 3
Items: 
Size: 397939 Color: 1
Size: 345334 Color: 1
Size: 256669 Color: 0

Bin 640: 70 of cap free
Amount of items: 3
Items: 
Size: 351077 Color: 1
Size: 349561 Color: 1
Size: 299293 Color: 0

Bin 641: 72 of cap free
Amount of items: 3
Items: 
Size: 360111 Color: 1
Size: 344890 Color: 1
Size: 294928 Color: 0

Bin 642: 82 of cap free
Amount of items: 3
Items: 
Size: 364184 Color: 1
Size: 349738 Color: 1
Size: 285997 Color: 0

Bin 643: 106 of cap free
Amount of items: 3
Items: 
Size: 352145 Color: 1
Size: 338708 Color: 1
Size: 309042 Color: 0

Bin 644: 108 of cap free
Amount of items: 3
Items: 
Size: 351412 Color: 1
Size: 341466 Color: 1
Size: 307015 Color: 0

Bin 645: 116 of cap free
Amount of items: 3
Items: 
Size: 359104 Color: 1
Size: 347038 Color: 1
Size: 293743 Color: 0

Bin 646: 152 of cap free
Amount of items: 3
Items: 
Size: 354802 Color: 1
Size: 347595 Color: 1
Size: 297452 Color: 0

Bin 647: 157 of cap free
Amount of items: 3
Items: 
Size: 349709 Color: 1
Size: 343686 Color: 1
Size: 306449 Color: 0

Bin 648: 183 of cap free
Amount of items: 3
Items: 
Size: 347944 Color: 1
Size: 346541 Color: 1
Size: 305333 Color: 0

Bin 649: 187 of cap free
Amount of items: 3
Items: 
Size: 346077 Color: 1
Size: 345806 Color: 1
Size: 307931 Color: 0

Bin 650: 188 of cap free
Amount of items: 3
Items: 
Size: 420579 Color: 1
Size: 324356 Color: 1
Size: 254878 Color: 0

Bin 651: 272 of cap free
Amount of items: 3
Items: 
Size: 360249 Color: 1
Size: 348890 Color: 1
Size: 290590 Color: 0

Bin 652: 284 of cap free
Amount of items: 3
Items: 
Size: 361270 Color: 1
Size: 355669 Color: 1
Size: 282778 Color: 0

Bin 653: 387 of cap free
Amount of items: 3
Items: 
Size: 354082 Color: 1
Size: 342583 Color: 1
Size: 302949 Color: 0

Bin 654: 481 of cap free
Amount of items: 3
Items: 
Size: 354993 Color: 1
Size: 349472 Color: 1
Size: 295055 Color: 0

Bin 655: 586 of cap free
Amount of items: 3
Items: 
Size: 348132 Color: 1
Size: 344857 Color: 1
Size: 306426 Color: 0

Bin 656: 621 of cap free
Amount of items: 3
Items: 
Size: 373048 Color: 1
Size: 315879 Color: 1
Size: 310453 Color: 0

Bin 657: 801 of cap free
Amount of items: 3
Items: 
Size: 360939 Color: 1
Size: 324186 Color: 1
Size: 314075 Color: 0

Bin 658: 893 of cap free
Amount of items: 3
Items: 
Size: 366925 Color: 1
Size: 319170 Color: 1
Size: 313013 Color: 0

Bin 659: 1365 of cap free
Amount of items: 3
Items: 
Size: 374349 Color: 1
Size: 354730 Color: 1
Size: 269557 Color: 0

Bin 660: 1812 of cap free
Amount of items: 3
Items: 
Size: 366712 Color: 1
Size: 364134 Color: 1
Size: 267343 Color: 0

Bin 661: 1942 of cap free
Amount of items: 3
Items: 
Size: 348855 Color: 1
Size: 348835 Color: 1
Size: 300369 Color: 0

Bin 662: 3209 of cap free
Amount of items: 3
Items: 
Size: 446510 Color: 1
Size: 285291 Color: 0
Size: 264991 Color: 1

Bin 663: 3241 of cap free
Amount of items: 3
Items: 
Size: 374031 Color: 1
Size: 369152 Color: 1
Size: 253577 Color: 0

Bin 664: 3438 of cap free
Amount of items: 3
Items: 
Size: 378974 Color: 1
Size: 316118 Color: 0
Size: 301471 Color: 1

Bin 665: 11825 of cap free
Amount of items: 3
Items: 
Size: 357127 Color: 1
Size: 325068 Color: 0
Size: 305981 Color: 1

Bin 666: 30067 of cap free
Amount of items: 3
Items: 
Size: 354007 Color: 1
Size: 352709 Color: 1
Size: 263218 Color: 0

Bin 667: 288441 of cap free
Amount of items: 2
Items: 
Size: 390518 Color: 1
Size: 321042 Color: 0

Bin 668: 647460 of cap free
Amount of items: 1
Items: 
Size: 352541 Color: 1

Total size: 667000667
Total free space: 1000001

