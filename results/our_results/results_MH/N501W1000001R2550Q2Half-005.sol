Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 489839 Color: 1
Size: 250464 Color: 0
Size: 259698 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 457493 Color: 1
Size: 277305 Color: 1
Size: 265203 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 463051 Color: 1
Size: 276961 Color: 1
Size: 259989 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 489169 Color: 1
Size: 257047 Color: 1
Size: 253785 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 369392 Color: 1
Size: 354658 Color: 1
Size: 275951 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 339369 Color: 1
Size: 335865 Color: 1
Size: 324767 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 463310 Color: 1
Size: 268897 Color: 1
Size: 267794 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 376215 Color: 1
Size: 333657 Color: 1
Size: 290129 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 383796 Color: 1
Size: 323743 Color: 1
Size: 292462 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 374907 Color: 1
Size: 351826 Color: 1
Size: 273268 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 474978 Color: 1
Size: 270939 Color: 1
Size: 254084 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 466204 Color: 1
Size: 269343 Color: 1
Size: 264454 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 388325 Color: 1
Size: 346487 Color: 1
Size: 265189 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 467691 Color: 1
Size: 271542 Color: 1
Size: 260768 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 420153 Color: 1
Size: 327279 Color: 1
Size: 252569 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 439021 Color: 1
Size: 288109 Color: 1
Size: 272871 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 354445 Color: 1
Size: 341494 Color: 1
Size: 304062 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 471632 Color: 1
Size: 275606 Color: 1
Size: 252763 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 457219 Color: 1
Size: 289156 Color: 1
Size: 253626 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 389717 Color: 1
Size: 350850 Color: 1
Size: 259434 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 354752 Color: 1
Size: 339848 Color: 1
Size: 305401 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 374000 Color: 1
Size: 314343 Color: 1
Size: 311658 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 385431 Color: 1
Size: 350885 Color: 1
Size: 263685 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 454397 Color: 1
Size: 276401 Color: 1
Size: 269203 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 380917 Color: 1
Size: 355902 Color: 1
Size: 263182 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 488566 Color: 1
Size: 259425 Color: 1
Size: 252010 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 356626 Color: 1
Size: 335975 Color: 1
Size: 307400 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 447372 Color: 1
Size: 283803 Color: 1
Size: 268826 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 422441 Color: 1
Size: 298330 Color: 1
Size: 279230 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 399173 Color: 1
Size: 330519 Color: 1
Size: 270309 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 384529 Color: 1
Size: 364004 Color: 1
Size: 251468 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 359242 Color: 1
Size: 335034 Color: 1
Size: 305725 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 407042 Color: 1
Size: 317729 Color: 1
Size: 275230 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 413738 Color: 1
Size: 334297 Color: 1
Size: 251966 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 350176 Color: 1
Size: 341725 Color: 1
Size: 308100 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 432802 Color: 1
Size: 294651 Color: 1
Size: 272548 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 477636 Color: 1
Size: 268871 Color: 1
Size: 253494 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 417208 Color: 1
Size: 307869 Color: 1
Size: 274924 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 436712 Color: 1
Size: 282355 Color: 1
Size: 280934 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 427236 Color: 1
Size: 295467 Color: 1
Size: 277298 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 389677 Color: 1
Size: 357659 Color: 1
Size: 252665 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 370876 Color: 1
Size: 367240 Color: 1
Size: 261885 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 377316 Color: 1
Size: 321616 Color: 1
Size: 301069 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 377447 Color: 1
Size: 365690 Color: 1
Size: 256864 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 363745 Color: 1
Size: 362066 Color: 1
Size: 274190 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 366345 Color: 1
Size: 329509 Color: 1
Size: 304147 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 400716 Color: 1
Size: 320798 Color: 1
Size: 278487 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 381392 Color: 1
Size: 368321 Color: 1
Size: 250288 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 353561 Color: 1
Size: 347804 Color: 1
Size: 298636 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 372687 Color: 1
Size: 345475 Color: 1
Size: 281839 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 379062 Color: 1
Size: 324019 Color: 1
Size: 296920 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 376212 Color: 1
Size: 332552 Color: 1
Size: 291237 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 356897 Color: 1
Size: 338799 Color: 1
Size: 304305 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 356378 Color: 1
Size: 328571 Color: 1
Size: 315052 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 361540 Color: 1
Size: 322797 Color: 1
Size: 315664 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 353416 Color: 1
Size: 338306 Color: 1
Size: 308279 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 354114 Color: 1
Size: 335852 Color: 1
Size: 310035 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 345465 Color: 1
Size: 343954 Color: 1
Size: 310582 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 361751 Color: 1
Size: 335545 Color: 1
Size: 302705 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 354898 Color: 1
Size: 333509 Color: 1
Size: 311594 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 376440 Color: 1
Size: 360578 Color: 1
Size: 262983 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 380893 Color: 1
Size: 339836 Color: 1
Size: 279272 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 351827 Color: 1
Size: 325018 Color: 1
Size: 323156 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 341704 Color: 1
Size: 332529 Color: 1
Size: 325768 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 340435 Color: 1
Size: 334622 Color: 1
Size: 324944 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 363413 Color: 1
Size: 345342 Color: 1
Size: 291246 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 343287 Color: 1
Size: 339545 Color: 1
Size: 317169 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 367436 Color: 1
Size: 322531 Color: 1
Size: 310034 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 369716 Color: 1
Size: 353577 Color: 1
Size: 276708 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 372556 Color: 1
Size: 355233 Color: 1
Size: 272212 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 376502 Color: 1
Size: 313574 Color: 1
Size: 309925 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 379846 Color: 1
Size: 357056 Color: 1
Size: 263099 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 380059 Color: 1
Size: 345147 Color: 1
Size: 274795 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 380563 Color: 1
Size: 359069 Color: 1
Size: 260369 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 457603 Color: 1
Size: 275195 Color: 1
Size: 267203 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 381032 Color: 1
Size: 363723 Color: 1
Size: 255246 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 381411 Color: 1
Size: 344198 Color: 1
Size: 274392 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 385773 Color: 1
Size: 330850 Color: 1
Size: 283378 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 386110 Color: 1
Size: 325940 Color: 1
Size: 287951 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 388436 Color: 1
Size: 347502 Color: 1
Size: 264063 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 388696 Color: 1
Size: 333463 Color: 1
Size: 277842 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 389096 Color: 1
Size: 329791 Color: 1
Size: 281114 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 390003 Color: 1
Size: 339959 Color: 1
Size: 270039 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 393737 Color: 1
Size: 319320 Color: 1
Size: 286944 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 395771 Color: 1
Size: 321701 Color: 1
Size: 282529 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 397884 Color: 1
Size: 303218 Color: 1
Size: 298899 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 398458 Color: 1
Size: 346600 Color: 1
Size: 254943 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 398781 Color: 1
Size: 320915 Color: 1
Size: 280305 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 399240 Color: 1
Size: 313003 Color: 1
Size: 287758 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 399372 Color: 1
Size: 306234 Color: 1
Size: 294395 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 399964 Color: 1
Size: 316905 Color: 1
Size: 283132 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 401922 Color: 1
Size: 317841 Color: 1
Size: 280238 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 403623 Color: 1
Size: 317560 Color: 1
Size: 278818 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 403940 Color: 1
Size: 321047 Color: 1
Size: 275014 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 405783 Color: 1
Size: 305574 Color: 1
Size: 288644 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 406019 Color: 1
Size: 322033 Color: 1
Size: 271949 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 406423 Color: 1
Size: 303572 Color: 1
Size: 290006 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 406614 Color: 1
Size: 337151 Color: 1
Size: 256236 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 408470 Color: 1
Size: 335215 Color: 1
Size: 256316 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 409410 Color: 1
Size: 308968 Color: 1
Size: 281623 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 409647 Color: 1
Size: 300287 Color: 1
Size: 290067 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 409681 Color: 1
Size: 326616 Color: 1
Size: 263704 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 412939 Color: 1
Size: 318103 Color: 1
Size: 268959 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 413587 Color: 1
Size: 303170 Color: 1
Size: 283244 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 415386 Color: 1
Size: 300007 Color: 1
Size: 284608 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 416597 Color: 1
Size: 313831 Color: 1
Size: 269573 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 417270 Color: 1
Size: 319875 Color: 1
Size: 262856 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 418327 Color: 1
Size: 321852 Color: 1
Size: 259822 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 421056 Color: 1
Size: 312667 Color: 1
Size: 266278 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 421238 Color: 1
Size: 293096 Color: 1
Size: 285667 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 421960 Color: 1
Size: 297400 Color: 1
Size: 280641 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 422167 Color: 1
Size: 293662 Color: 1
Size: 284172 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 441352 Color: 1
Size: 291794 Color: 1
Size: 266855 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 424243 Color: 1
Size: 311188 Color: 1
Size: 264570 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 424727 Color: 1
Size: 287898 Color: 1
Size: 287376 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 425651 Color: 1
Size: 322222 Color: 1
Size: 252128 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 425826 Color: 1
Size: 302676 Color: 1
Size: 271499 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 426785 Color: 1
Size: 273565 Color: 0
Size: 299651 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 426849 Color: 1
Size: 299730 Color: 1
Size: 273422 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 428634 Color: 1
Size: 308714 Color: 1
Size: 262653 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 429104 Color: 1
Size: 317485 Color: 1
Size: 253412 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 429490 Color: 1
Size: 317793 Color: 1
Size: 252718 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 430054 Color: 1
Size: 295123 Color: 1
Size: 274824 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 431311 Color: 1
Size: 311606 Color: 1
Size: 257084 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 431936 Color: 1
Size: 305525 Color: 1
Size: 262540 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 433051 Color: 1
Size: 286426 Color: 1
Size: 280524 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 434510 Color: 1
Size: 303955 Color: 1
Size: 261536 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 434985 Color: 1
Size: 285479 Color: 1
Size: 279537 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 435765 Color: 1
Size: 291520 Color: 1
Size: 272716 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 437453 Color: 1
Size: 309594 Color: 1
Size: 252954 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 437672 Color: 1
Size: 298049 Color: 1
Size: 264280 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 455651 Color: 1
Size: 293720 Color: 1
Size: 250630 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 442260 Color: 1
Size: 290122 Color: 1
Size: 267619 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 442406 Color: 1
Size: 299236 Color: 1
Size: 258359 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 443733 Color: 1
Size: 284011 Color: 1
Size: 272257 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 443805 Color: 1
Size: 282083 Color: 1
Size: 274113 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 445078 Color: 1
Size: 299776 Color: 1
Size: 255147 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 446106 Color: 1
Size: 297204 Color: 1
Size: 256691 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 450171 Color: 1
Size: 295798 Color: 1
Size: 254032 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 450672 Color: 1
Size: 277724 Color: 1
Size: 271605 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 451795 Color: 1
Size: 281867 Color: 1
Size: 266339 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 452900 Color: 1
Size: 278330 Color: 1
Size: 268771 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 454771 Color: 1
Size: 272934 Color: 1
Size: 272296 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 455317 Color: 1
Size: 284640 Color: 1
Size: 260044 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 456433 Color: 1
Size: 286346 Color: 1
Size: 257222 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 458378 Color: 1
Size: 284442 Color: 1
Size: 257181 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 458595 Color: 1
Size: 283359 Color: 1
Size: 258047 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 461369 Color: 1
Size: 285801 Color: 1
Size: 252831 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 462995 Color: 1
Size: 269287 Color: 1
Size: 267719 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 468211 Color: 1
Size: 278863 Color: 1
Size: 252927 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 473287 Color: 1
Size: 266409 Color: 1
Size: 260305 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 478263 Color: 1
Size: 269930 Color: 1
Size: 251808 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 479834 Color: 1
Size: 269499 Color: 1
Size: 250668 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 479985 Color: 1
Size: 268338 Color: 1
Size: 251678 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 482683 Color: 1
Size: 260612 Color: 1
Size: 256706 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 482810 Color: 1
Size: 261901 Color: 1
Size: 255290 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 483487 Color: 1
Size: 262794 Color: 1
Size: 253720 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 484406 Color: 1
Size: 260152 Color: 1
Size: 255443 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 484556 Color: 1
Size: 258573 Color: 1
Size: 256872 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 484800 Color: 1
Size: 259061 Color: 1
Size: 256140 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 486752 Color: 1
Size: 260661 Color: 1
Size: 252588 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 487750 Color: 1
Size: 257281 Color: 1
Size: 254970 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 490311 Color: 1
Size: 256236 Color: 1
Size: 253454 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 490322 Color: 1
Size: 255130 Color: 1
Size: 254549 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 490544 Color: 1
Size: 258071 Color: 1
Size: 251386 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 491992 Color: 1
Size: 254233 Color: 1
Size: 253776 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 492020 Color: 1
Size: 257898 Color: 1
Size: 250083 Color: 0

Total size: 167000167
Total free space: 0

