Capicity Bin: 2028
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1015 Color: 4
Size: 326 Color: 0
Size: 261 Color: 0
Size: 190 Color: 17
Size: 108 Color: 7
Size: 72 Color: 9
Size: 56 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 1
Size: 731 Color: 13
Size: 58 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1258 Color: 19
Size: 714 Color: 16
Size: 56 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 1
Size: 518 Color: 13
Size: 100 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 9
Size: 475 Color: 0
Size: 32 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 13
Size: 362 Color: 9
Size: 52 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 4
Size: 234 Color: 7
Size: 140 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 10
Size: 329 Color: 7
Size: 20 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1691 Color: 8
Size: 245 Color: 13
Size: 92 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 19
Size: 261 Color: 18
Size: 50 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 14
Size: 250 Color: 13
Size: 48 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 15
Size: 236 Color: 2
Size: 44 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 18
Size: 242 Color: 11
Size: 36 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 17
Size: 257 Color: 9
Size: 20 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 8
Size: 227 Color: 12
Size: 44 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 15
Size: 228 Color: 18
Size: 36 Color: 18

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 16
Size: 316 Color: 3
Size: 84 Color: 6

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 8
Size: 318 Color: 6
Size: 44 Color: 5

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 16
Size: 245 Color: 0
Size: 88 Color: 5

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1735 Color: 2
Size: 194 Color: 0
Size: 98 Color: 5

Bin 21: 2 of cap free
Amount of items: 3
Items: 
Size: 1400 Color: 1
Size: 582 Color: 4
Size: 44 Color: 6

Bin 22: 2 of cap free
Amount of items: 3
Items: 
Size: 1454 Color: 3
Size: 462 Color: 5
Size: 110 Color: 9

Bin 23: 2 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 14
Size: 472 Color: 16
Size: 60 Color: 11

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 1
Size: 152 Color: 19
Size: 76 Color: 13

Bin 25: 2 of cap free
Amount of items: 2
Items: 
Size: 1802 Color: 12
Size: 224 Color: 11

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1806 Color: 10
Size: 220 Color: 19

Bin 27: 3 of cap free
Amount of items: 3
Items: 
Size: 1050 Color: 11
Size: 845 Color: 2
Size: 130 Color: 5

Bin 28: 3 of cap free
Amount of items: 4
Items: 
Size: 1151 Color: 6
Size: 411 Color: 10
Size: 295 Color: 9
Size: 168 Color: 17

Bin 29: 3 of cap free
Amount of items: 2
Items: 
Size: 1243 Color: 17
Size: 782 Color: 11

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 4
Size: 659 Color: 9
Size: 36 Color: 6

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1341 Color: 11
Size: 684 Color: 1

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 18
Size: 557 Color: 7
Size: 94 Color: 6

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 1
Size: 446 Color: 1
Size: 44 Color: 7

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 9
Size: 382 Color: 2

Bin 35: 5 of cap free
Amount of items: 3
Items: 
Size: 1174 Color: 3
Size: 757 Color: 2
Size: 92 Color: 3

Bin 36: 6 of cap free
Amount of items: 2
Items: 
Size: 1377 Color: 5
Size: 645 Color: 3

Bin 37: 7 of cap free
Amount of items: 3
Items: 
Size: 1018 Color: 9
Size: 843 Color: 7
Size: 160 Color: 15

Bin 38: 7 of cap free
Amount of items: 2
Items: 
Size: 1475 Color: 7
Size: 546 Color: 0

Bin 39: 7 of cap free
Amount of items: 2
Items: 
Size: 1539 Color: 5
Size: 482 Color: 2

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 1530 Color: 11
Size: 491 Color: 16

Bin 41: 7 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 6
Size: 335 Color: 8
Size: 16 Color: 14

Bin 42: 7 of cap free
Amount of items: 2
Items: 
Size: 1675 Color: 13
Size: 346 Color: 2

Bin 43: 7 of cap free
Amount of items: 3
Items: 
Size: 1755 Color: 13
Size: 262 Color: 2
Size: 4 Color: 7

Bin 44: 8 of cap free
Amount of items: 2
Items: 
Size: 1459 Color: 3
Size: 561 Color: 19

Bin 45: 8 of cap free
Amount of items: 2
Items: 
Size: 1738 Color: 1
Size: 282 Color: 11

Bin 46: 9 of cap free
Amount of items: 3
Items: 
Size: 1153 Color: 3
Size: 818 Color: 12
Size: 48 Color: 7

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1650 Color: 14
Size: 369 Color: 11

Bin 48: 10 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 3
Size: 398 Color: 15
Size: 66 Color: 5

Bin 49: 10 of cap free
Amount of items: 2
Items: 
Size: 1715 Color: 6
Size: 303 Color: 13

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1737 Color: 7
Size: 281 Color: 13

Bin 51: 11 of cap free
Amount of items: 2
Items: 
Size: 1474 Color: 5
Size: 543 Color: 0

Bin 52: 11 of cap free
Amount of items: 2
Items: 
Size: 1594 Color: 17
Size: 423 Color: 18

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 1714 Color: 6
Size: 302 Color: 13

Bin 54: 14 of cap free
Amount of items: 3
Items: 
Size: 1361 Color: 15
Size: 573 Color: 19
Size: 80 Color: 9

Bin 55: 14 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 1
Size: 461 Color: 19
Size: 114 Color: 11

Bin 56: 16 of cap free
Amount of items: 2
Items: 
Size: 1357 Color: 7
Size: 655 Color: 18

Bin 57: 18 of cap free
Amount of items: 3
Items: 
Size: 1276 Color: 14
Size: 642 Color: 5
Size: 92 Color: 11

Bin 58: 19 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 15
Size: 331 Color: 0
Size: 104 Color: 1

Bin 59: 19 of cap free
Amount of items: 2
Items: 
Size: 1589 Color: 4
Size: 420 Color: 13

Bin 60: 21 of cap free
Amount of items: 2
Items: 
Size: 1255 Color: 11
Size: 752 Color: 10

Bin 61: 23 of cap free
Amount of items: 2
Items: 
Size: 1587 Color: 3
Size: 418 Color: 17

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1094 Color: 16
Size: 906 Color: 5

Bin 63: 30 of cap free
Amount of items: 2
Items: 
Size: 1631 Color: 6
Size: 367 Color: 5

Bin 64: 38 of cap free
Amount of items: 4
Items: 
Size: 1017 Color: 14
Size: 409 Color: 3
Size: 321 Color: 2
Size: 243 Color: 3

Bin 65: 42 of cap free
Amount of items: 19
Items: 
Size: 186 Color: 9
Size: 168 Color: 8
Size: 144 Color: 17
Size: 130 Color: 5
Size: 128 Color: 18
Size: 128 Color: 8
Size: 120 Color: 19
Size: 116 Color: 19
Size: 110 Color: 9
Size: 108 Color: 12
Size: 82 Color: 13
Size: 80 Color: 13
Size: 76 Color: 11
Size: 72 Color: 18
Size: 72 Color: 12
Size: 72 Color: 7
Size: 68 Color: 2
Size: 66 Color: 16
Size: 60 Color: 1

Bin 66: 1548 of cap free
Amount of items: 9
Items: 
Size: 64 Color: 13
Size: 64 Color: 7
Size: 60 Color: 4
Size: 52 Color: 5
Size: 52 Color: 1
Size: 48 Color: 12
Size: 48 Color: 10
Size: 48 Color: 2
Size: 44 Color: 0

Total size: 131820
Total free space: 2028

