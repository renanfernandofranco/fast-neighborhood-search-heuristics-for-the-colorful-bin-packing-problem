Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9123 Color: 10
Size: 6287 Color: 11
Size: 878 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 13
Size: 6756 Color: 16
Size: 358 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9723 Color: 12
Size: 5795 Color: 0
Size: 770 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10265 Color: 12
Size: 5021 Color: 5
Size: 1002 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12018 Color: 6
Size: 3502 Color: 16
Size: 768 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12072 Color: 18
Size: 3942 Color: 11
Size: 274 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 4
Size: 3528 Color: 7
Size: 592 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12458 Color: 3
Size: 3460 Color: 4
Size: 370 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12596 Color: 18
Size: 3332 Color: 12
Size: 360 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12766 Color: 10
Size: 2000 Color: 9
Size: 1522 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 4
Size: 3072 Color: 8
Size: 372 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13008 Color: 5
Size: 1886 Color: 5
Size: 1394 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13151 Color: 0
Size: 2609 Color: 16
Size: 528 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13252 Color: 13
Size: 2876 Color: 8
Size: 160 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 7
Size: 1812 Color: 15
Size: 1174 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13304 Color: 19
Size: 2712 Color: 5
Size: 272 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13396 Color: 0
Size: 2532 Color: 17
Size: 360 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 12
Size: 2208 Color: 14
Size: 664 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13489 Color: 10
Size: 2333 Color: 9
Size: 466 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13505 Color: 4
Size: 2147 Color: 12
Size: 636 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13617 Color: 5
Size: 1891 Color: 8
Size: 780 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13624 Color: 10
Size: 2218 Color: 0
Size: 446 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13630 Color: 11
Size: 1860 Color: 3
Size: 798 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13713 Color: 17
Size: 2227 Color: 13
Size: 348 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13753 Color: 10
Size: 1911 Color: 5
Size: 624 Color: 6

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13841 Color: 7
Size: 1967 Color: 19
Size: 480 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 8
Size: 1692 Color: 18
Size: 656 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 0
Size: 1356 Color: 13
Size: 988 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13982 Color: 12
Size: 1170 Color: 15
Size: 1136 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 17
Size: 1878 Color: 3
Size: 384 Color: 9

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 9
Size: 1964 Color: 1
Size: 296 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 12
Size: 1528 Color: 9
Size: 688 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 18
Size: 1136 Color: 11
Size: 1092 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14116 Color: 18
Size: 1772 Color: 11
Size: 400 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14131 Color: 19
Size: 1461 Color: 6
Size: 696 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14139 Color: 0
Size: 1565 Color: 13
Size: 584 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 12
Size: 1328 Color: 13
Size: 700 Color: 16

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14171 Color: 15
Size: 1765 Color: 8
Size: 352 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14206 Color: 0
Size: 1532 Color: 3
Size: 550 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14266 Color: 12
Size: 1686 Color: 8
Size: 336 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14271 Color: 17
Size: 1681 Color: 0
Size: 336 Color: 18

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14340 Color: 16
Size: 1582 Color: 15
Size: 366 Color: 12

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 8
Size: 1162 Color: 4
Size: 736 Color: 16

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14396 Color: 16
Size: 1580 Color: 19
Size: 312 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14456 Color: 3
Size: 1312 Color: 17
Size: 520 Color: 10

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14457 Color: 6
Size: 1527 Color: 2
Size: 304 Color: 17

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14502 Color: 11
Size: 1410 Color: 17
Size: 376 Color: 9

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 1
Size: 1320 Color: 19
Size: 444 Color: 5

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 9
Size: 1356 Color: 16
Size: 316 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14618 Color: 10
Size: 998 Color: 14
Size: 672 Color: 13

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14642 Color: 7
Size: 1022 Color: 7
Size: 624 Color: 12

Bin 52: 1 of cap free
Amount of items: 10
Items: 
Size: 8145 Color: 6
Size: 1016 Color: 17
Size: 1016 Color: 15
Size: 1010 Color: 13
Size: 1008 Color: 17
Size: 1000 Color: 11
Size: 992 Color: 4
Size: 900 Color: 18
Size: 620 Color: 10
Size: 580 Color: 7

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 9115 Color: 10
Size: 6780 Color: 5
Size: 392 Color: 4

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 9211 Color: 15
Size: 6764 Color: 15
Size: 312 Color: 2

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 10188 Color: 0
Size: 5811 Color: 10
Size: 288 Color: 0

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 10225 Color: 18
Size: 5782 Color: 19
Size: 280 Color: 1

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 10866 Color: 17
Size: 5053 Color: 0
Size: 368 Color: 4

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 11495 Color: 5
Size: 4600 Color: 17
Size: 192 Color: 14

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 11527 Color: 6
Size: 4520 Color: 16
Size: 240 Color: 5

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11939 Color: 10
Size: 4028 Color: 11
Size: 320 Color: 12

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 12557 Color: 11
Size: 3506 Color: 12
Size: 224 Color: 8

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 12991 Color: 8
Size: 2894 Color: 18
Size: 402 Color: 15

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 4
Size: 3111 Color: 16
Size: 128 Color: 15

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 16
Size: 1611 Color: 12
Size: 1500 Color: 0

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13349 Color: 4
Size: 1706 Color: 3
Size: 1232 Color: 0

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 13376 Color: 7
Size: 2911 Color: 16

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13418 Color: 5
Size: 2025 Color: 12
Size: 844 Color: 5

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13529 Color: 18
Size: 2394 Color: 4
Size: 364 Color: 18

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13838 Color: 18
Size: 2027 Color: 19
Size: 422 Color: 11

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 14059 Color: 16
Size: 1364 Color: 5
Size: 864 Color: 16

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 0
Size: 1799 Color: 8

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 14535 Color: 8
Size: 1512 Color: 12
Size: 240 Color: 2

Bin 73: 2 of cap free
Amount of items: 9
Items: 
Size: 8146 Color: 15
Size: 1352 Color: 17
Size: 1152 Color: 11
Size: 1152 Color: 2
Size: 1136 Color: 12
Size: 1104 Color: 0
Size: 1072 Color: 14
Size: 796 Color: 17
Size: 376 Color: 7

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 9348 Color: 12
Size: 5690 Color: 7
Size: 1248 Color: 11

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 9354 Color: 8
Size: 6264 Color: 19
Size: 668 Color: 6

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 9731 Color: 3
Size: 6177 Color: 13
Size: 378 Color: 9

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 5
Size: 6080 Color: 0
Size: 358 Color: 18

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 10776 Color: 13
Size: 3448 Color: 4
Size: 2062 Color: 18

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 15
Size: 5084 Color: 19
Size: 272 Color: 10

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 10938 Color: 14
Size: 5044 Color: 11
Size: 304 Color: 15

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 10948 Color: 9
Size: 5018 Color: 17
Size: 320 Color: 2

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 14
Size: 3602 Color: 15
Size: 292 Color: 8

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 12789 Color: 5
Size: 3117 Color: 15
Size: 380 Color: 16

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 13537 Color: 16
Size: 2749 Color: 1

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 14091 Color: 11
Size: 1859 Color: 12
Size: 336 Color: 17

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 14387 Color: 19
Size: 1899 Color: 3

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 14462 Color: 19
Size: 1502 Color: 14
Size: 322 Color: 12

Bin 88: 2 of cap free
Amount of items: 2
Items: 
Size: 14598 Color: 9
Size: 1688 Color: 17

Bin 89: 3 of cap free
Amount of items: 8
Items: 
Size: 8148 Color: 1
Size: 1441 Color: 0
Size: 1374 Color: 8
Size: 1372 Color: 6
Size: 1366 Color: 2
Size: 1352 Color: 7
Size: 944 Color: 18
Size: 288 Color: 5

Bin 90: 3 of cap free
Amount of items: 7
Items: 
Size: 8152 Color: 12
Size: 1818 Color: 19
Size: 1547 Color: 15
Size: 1476 Color: 17
Size: 1400 Color: 8
Size: 1344 Color: 0
Size: 548 Color: 14

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 8156 Color: 2
Size: 6785 Color: 11
Size: 1344 Color: 1

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 9267 Color: 12
Size: 6606 Color: 18
Size: 412 Color: 5

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 10145 Color: 4
Size: 5788 Color: 15
Size: 352 Color: 5

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 12292 Color: 2
Size: 3631 Color: 0
Size: 362 Color: 9

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 13159 Color: 12
Size: 2686 Color: 11
Size: 440 Color: 13

Bin 96: 3 of cap free
Amount of items: 2
Items: 
Size: 13609 Color: 7
Size: 2676 Color: 19

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 13761 Color: 19
Size: 1356 Color: 2
Size: 1168 Color: 4

Bin 98: 3 of cap free
Amount of items: 2
Items: 
Size: 13873 Color: 14
Size: 2412 Color: 13

Bin 99: 3 of cap free
Amount of items: 2
Items: 
Size: 14038 Color: 3
Size: 2247 Color: 17

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 9432 Color: 8
Size: 6184 Color: 15
Size: 668 Color: 18

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 10353 Color: 16
Size: 5931 Color: 14

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 11096 Color: 1
Size: 5188 Color: 11

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 12548 Color: 12
Size: 3736 Color: 7

Bin 104: 4 of cap free
Amount of items: 2
Items: 
Size: 14110 Color: 13
Size: 2174 Color: 6

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 14406 Color: 5
Size: 1570 Color: 17
Size: 308 Color: 12

Bin 106: 5 of cap free
Amount of items: 3
Items: 
Size: 11144 Color: 6
Size: 4947 Color: 8
Size: 192 Color: 16

Bin 107: 5 of cap free
Amount of items: 3
Items: 
Size: 11453 Color: 10
Size: 4462 Color: 8
Size: 368 Color: 4

Bin 108: 5 of cap free
Amount of items: 3
Items: 
Size: 12090 Color: 6
Size: 3625 Color: 7
Size: 568 Color: 8

Bin 109: 5 of cap free
Amount of items: 3
Items: 
Size: 13457 Color: 7
Size: 2746 Color: 1
Size: 80 Color: 18

Bin 110: 5 of cap free
Amount of items: 2
Items: 
Size: 14492 Color: 4
Size: 1791 Color: 3

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 11416 Color: 17
Size: 4522 Color: 10
Size: 344 Color: 14

Bin 112: 6 of cap free
Amount of items: 3
Items: 
Size: 11498 Color: 11
Size: 4296 Color: 6
Size: 488 Color: 3

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 11816 Color: 12
Size: 4466 Color: 0

Bin 114: 6 of cap free
Amount of items: 2
Items: 
Size: 13682 Color: 11
Size: 2600 Color: 17

Bin 115: 6 of cap free
Amount of items: 2
Items: 
Size: 14433 Color: 17
Size: 1849 Color: 4

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 10552 Color: 16
Size: 4993 Color: 6
Size: 736 Color: 7

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 13997 Color: 19
Size: 2284 Color: 1

Bin 118: 7 of cap free
Amount of items: 2
Items: 
Size: 14168 Color: 16
Size: 2113 Color: 14

Bin 119: 7 of cap free
Amount of items: 2
Items: 
Size: 14239 Color: 19
Size: 2042 Color: 1

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 14346 Color: 2
Size: 1934 Color: 3

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 14652 Color: 1
Size: 1628 Color: 17

Bin 122: 9 of cap free
Amount of items: 3
Items: 
Size: 11562 Color: 12
Size: 4413 Color: 18
Size: 304 Color: 4

Bin 123: 9 of cap free
Amount of items: 2
Items: 
Size: 11832 Color: 19
Size: 4447 Color: 1

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 13022 Color: 7
Size: 3256 Color: 5

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 14264 Color: 19
Size: 2013 Color: 2

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 14355 Color: 15
Size: 1922 Color: 4

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 10297 Color: 8
Size: 5979 Color: 19

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 13788 Color: 19
Size: 2488 Color: 12

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 14234 Color: 1
Size: 2041 Color: 2

Bin 130: 14 of cap free
Amount of items: 3
Items: 
Size: 8362 Color: 19
Size: 5844 Color: 3
Size: 2068 Color: 9

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 14452 Color: 0
Size: 1822 Color: 5

Bin 132: 15 of cap free
Amount of items: 5
Items: 
Size: 8154 Color: 3
Size: 2448 Color: 4
Size: 2445 Color: 1
Size: 2040 Color: 8
Size: 1186 Color: 0

Bin 133: 15 of cap free
Amount of items: 3
Items: 
Size: 12549 Color: 17
Size: 3084 Color: 3
Size: 640 Color: 3

Bin 134: 15 of cap free
Amount of items: 3
Items: 
Size: 12983 Color: 12
Size: 3194 Color: 3
Size: 96 Color: 10

Bin 135: 15 of cap free
Amount of items: 2
Items: 
Size: 14559 Color: 0
Size: 1714 Color: 8

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 11852 Color: 8
Size: 4420 Color: 1

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 13355 Color: 9
Size: 2917 Color: 1

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 14650 Color: 5
Size: 1622 Color: 8

Bin 139: 17 of cap free
Amount of items: 3
Items: 
Size: 11966 Color: 18
Size: 3969 Color: 12
Size: 336 Color: 4

Bin 140: 17 of cap free
Amount of items: 2
Items: 
Size: 13786 Color: 1
Size: 2485 Color: 13

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 13929 Color: 1
Size: 2342 Color: 5

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 12275 Color: 9
Size: 3995 Color: 3

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 19
Size: 2722 Color: 0

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 12632 Color: 11
Size: 3636 Color: 8

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 14107 Color: 6
Size: 2160 Color: 3

Bin 146: 22 of cap free
Amount of items: 2
Items: 
Size: 9656 Color: 2
Size: 6610 Color: 4

Bin 147: 23 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 2
Size: 4031 Color: 3
Size: 152 Color: 6

Bin 148: 23 of cap free
Amount of items: 2
Items: 
Size: 13814 Color: 3
Size: 2451 Color: 14

Bin 149: 23 of cap free
Amount of items: 2
Items: 
Size: 13857 Color: 12
Size: 2408 Color: 18

Bin 150: 24 of cap free
Amount of items: 2
Items: 
Size: 14031 Color: 8
Size: 2233 Color: 1

Bin 151: 25 of cap free
Amount of items: 2
Items: 
Size: 12269 Color: 10
Size: 3994 Color: 12

Bin 152: 25 of cap free
Amount of items: 2
Items: 
Size: 13970 Color: 19
Size: 2293 Color: 9

Bin 153: 28 of cap free
Amount of items: 2
Items: 
Size: 13084 Color: 3
Size: 3176 Color: 9

Bin 154: 29 of cap free
Amount of items: 2
Items: 
Size: 11931 Color: 13
Size: 4328 Color: 9

Bin 155: 29 of cap free
Amount of items: 2
Items: 
Size: 13644 Color: 12
Size: 2615 Color: 11

Bin 156: 29 of cap free
Amount of items: 2
Items: 
Size: 14411 Color: 3
Size: 1848 Color: 17

Bin 157: 30 of cap free
Amount of items: 2
Items: 
Size: 14490 Color: 11
Size: 1768 Color: 16

Bin 158: 32 of cap free
Amount of items: 2
Items: 
Size: 8872 Color: 7
Size: 7384 Color: 18

Bin 159: 32 of cap free
Amount of items: 2
Items: 
Size: 12728 Color: 8
Size: 3528 Color: 9

Bin 160: 34 of cap free
Amount of items: 2
Items: 
Size: 9462 Color: 8
Size: 6792 Color: 4

Bin 161: 34 of cap free
Amount of items: 2
Items: 
Size: 13712 Color: 7
Size: 2542 Color: 17

Bin 162: 34 of cap free
Amount of items: 2
Items: 
Size: 13772 Color: 1
Size: 2482 Color: 5

Bin 163: 35 of cap free
Amount of items: 3
Items: 
Size: 10953 Color: 8
Size: 3562 Color: 4
Size: 1738 Color: 7

Bin 164: 35 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 8
Size: 1585 Color: 19
Size: 24 Color: 2

Bin 165: 36 of cap free
Amount of items: 34
Items: 
Size: 724 Color: 2
Size: 720 Color: 16
Size: 608 Color: 8
Size: 582 Color: 3
Size: 576 Color: 6
Size: 544 Color: 1
Size: 536 Color: 13
Size: 528 Color: 15
Size: 512 Color: 18
Size: 508 Color: 15
Size: 504 Color: 11
Size: 496 Color: 18
Size: 496 Color: 15
Size: 488 Color: 14
Size: 476 Color: 9
Size: 472 Color: 3
Size: 470 Color: 7
Size: 464 Color: 16
Size: 464 Color: 14
Size: 462 Color: 2
Size: 458 Color: 16
Size: 456 Color: 12
Size: 440 Color: 1
Size: 432 Color: 19
Size: 428 Color: 18
Size: 416 Color: 13
Size: 416 Color: 9
Size: 408 Color: 7
Size: 404 Color: 10
Size: 384 Color: 11
Size: 384 Color: 10
Size: 352 Color: 0
Size: 340 Color: 0
Size: 304 Color: 14

Bin 166: 36 of cap free
Amount of items: 2
Items: 
Size: 11460 Color: 19
Size: 4792 Color: 5

Bin 167: 36 of cap free
Amount of items: 2
Items: 
Size: 12180 Color: 13
Size: 4072 Color: 6

Bin 168: 38 of cap free
Amount of items: 2
Items: 
Size: 14164 Color: 18
Size: 2086 Color: 5

Bin 169: 39 of cap free
Amount of items: 2
Items: 
Size: 9113 Color: 1
Size: 7136 Color: 16

Bin 170: 40 of cap free
Amount of items: 2
Items: 
Size: 13310 Color: 4
Size: 2938 Color: 16

Bin 171: 45 of cap free
Amount of items: 2
Items: 
Size: 14011 Color: 13
Size: 2232 Color: 16

Bin 172: 46 of cap free
Amount of items: 2
Items: 
Size: 12522 Color: 12
Size: 3720 Color: 3

Bin 173: 46 of cap free
Amount of items: 2
Items: 
Size: 14102 Color: 7
Size: 2140 Color: 17

Bin 174: 47 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 5
Size: 5465 Color: 15
Size: 2528 Color: 4

Bin 175: 48 of cap free
Amount of items: 2
Items: 
Size: 10872 Color: 4
Size: 5368 Color: 19

Bin 176: 51 of cap free
Amount of items: 2
Items: 
Size: 13482 Color: 15
Size: 2755 Color: 19

Bin 177: 52 of cap free
Amount of items: 3
Items: 
Size: 10236 Color: 6
Size: 5528 Color: 16
Size: 472 Color: 10

Bin 178: 56 of cap free
Amount of items: 3
Items: 
Size: 9318 Color: 3
Size: 6642 Color: 12
Size: 272 Color: 10

Bin 179: 56 of cap free
Amount of items: 2
Items: 
Size: 9315 Color: 4
Size: 6917 Color: 12

Bin 180: 59 of cap free
Amount of items: 3
Items: 
Size: 8188 Color: 4
Size: 5720 Color: 3
Size: 2321 Color: 10

Bin 181: 68 of cap free
Amount of items: 2
Items: 
Size: 8172 Color: 18
Size: 8048 Color: 7

Bin 182: 71 of cap free
Amount of items: 2
Items: 
Size: 10178 Color: 19
Size: 6039 Color: 13

Bin 183: 72 of cap free
Amount of items: 3
Items: 
Size: 9171 Color: 13
Size: 5851 Color: 12
Size: 1194 Color: 10

Bin 184: 74 of cap free
Amount of items: 5
Items: 
Size: 8147 Color: 17
Size: 2204 Color: 3
Size: 2084 Color: 1
Size: 1960 Color: 16
Size: 1819 Color: 6

Bin 185: 79 of cap free
Amount of items: 2
Items: 
Size: 13848 Color: 15
Size: 2361 Color: 11

Bin 186: 80 of cap free
Amount of items: 2
Items: 
Size: 13066 Color: 3
Size: 3142 Color: 17

Bin 187: 82 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 6
Size: 2968 Color: 13

Bin 188: 100 of cap free
Amount of items: 2
Items: 
Size: 12488 Color: 9
Size: 3700 Color: 0

Bin 189: 125 of cap free
Amount of items: 2
Items: 
Size: 12818 Color: 2
Size: 3345 Color: 1

Bin 190: 136 of cap free
Amount of items: 2
Items: 
Size: 11031 Color: 15
Size: 5121 Color: 16

Bin 191: 138 of cap free
Amount of items: 21
Items: 
Size: 944 Color: 8
Size: 912 Color: 12
Size: 896 Color: 9
Size: 892 Color: 4
Size: 888 Color: 17
Size: 888 Color: 13
Size: 880 Color: 7
Size: 848 Color: 1
Size: 812 Color: 0
Size: 804 Color: 14
Size: 800 Color: 13
Size: 800 Color: 12
Size: 792 Color: 4
Size: 784 Color: 6
Size: 736 Color: 12
Size: 726 Color: 19
Size: 708 Color: 3
Size: 622 Color: 3
Size: 522 Color: 18
Size: 464 Color: 10
Size: 432 Color: 10

Bin 192: 139 of cap free
Amount of items: 2
Items: 
Size: 10270 Color: 19
Size: 5879 Color: 5

Bin 193: 140 of cap free
Amount of items: 2
Items: 
Size: 12797 Color: 4
Size: 3351 Color: 2

Bin 194: 170 of cap free
Amount of items: 2
Items: 
Size: 9331 Color: 3
Size: 6787 Color: 17

Bin 195: 184 of cap free
Amount of items: 2
Items: 
Size: 9276 Color: 19
Size: 6828 Color: 9

Bin 196: 190 of cap free
Amount of items: 2
Items: 
Size: 8776 Color: 4
Size: 7322 Color: 14

Bin 197: 206 of cap free
Amount of items: 2
Items: 
Size: 10988 Color: 3
Size: 5094 Color: 9

Bin 198: 267 of cap free
Amount of items: 2
Items: 
Size: 9235 Color: 7
Size: 6786 Color: 0

Bin 199: 12130 of cap free
Amount of items: 13
Items: 
Size: 416 Color: 6
Size: 406 Color: 18
Size: 384 Color: 10
Size: 360 Color: 7
Size: 316 Color: 17
Size: 312 Color: 15
Size: 296 Color: 0
Size: 288 Color: 11
Size: 280 Color: 16
Size: 280 Color: 3
Size: 276 Color: 2
Size: 272 Color: 14
Size: 272 Color: 5

Total size: 3225024
Total free space: 16288

