Capicity Bin: 16128
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 14
Items: 
Size: 8068 Color: 1
Size: 720 Color: 0
Size: 704 Color: 0
Size: 672 Color: 1
Size: 672 Color: 0
Size: 666 Color: 1
Size: 664 Color: 1
Size: 656 Color: 1
Size: 640 Color: 1
Size: 624 Color: 0
Size: 612 Color: 0
Size: 612 Color: 0
Size: 410 Color: 1
Size: 408 Color: 0

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 8072 Color: 0
Size: 1312 Color: 1
Size: 1156 Color: 1
Size: 1156 Color: 1
Size: 1152 Color: 0
Size: 1148 Color: 0
Size: 1136 Color: 0
Size: 576 Color: 1
Size: 420 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9672 Color: 1
Size: 5768 Color: 0
Size: 688 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9968 Color: 1
Size: 5872 Color: 0
Size: 288 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11065 Color: 1
Size: 4389 Color: 0
Size: 674 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11048 Color: 0
Size: 3600 Color: 1
Size: 1480 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 0
Size: 4088 Color: 1
Size: 816 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11654 Color: 0
Size: 3730 Color: 0
Size: 744 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12086 Color: 1
Size: 3734 Color: 0
Size: 308 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12240 Color: 0
Size: 3644 Color: 1
Size: 244 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12368 Color: 0
Size: 2900 Color: 0
Size: 860 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12458 Color: 1
Size: 2642 Color: 0
Size: 1028 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12600 Color: 1
Size: 3236 Color: 0
Size: 292 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 2896 Color: 0
Size: 328 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12968 Color: 1
Size: 2344 Color: 1
Size: 816 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 1
Size: 1616 Color: 0
Size: 1532 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 1
Size: 2596 Color: 0
Size: 512 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 0
Size: 1792 Color: 0
Size: 1136 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13208 Color: 1
Size: 2358 Color: 0
Size: 562 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13546 Color: 0
Size: 1738 Color: 0
Size: 844 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13584 Color: 1
Size: 1272 Color: 1
Size: 1272 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13580 Color: 0
Size: 1524 Color: 0
Size: 1024 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13840 Color: 1
Size: 1340 Color: 1
Size: 948 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 1
Size: 1328 Color: 0
Size: 952 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14104 Color: 0
Size: 1072 Color: 1
Size: 952 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14116 Color: 0
Size: 1372 Color: 1
Size: 640 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 0
Size: 1228 Color: 1
Size: 704 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14430 Color: 1
Size: 1418 Color: 1
Size: 280 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 0
Size: 1592 Color: 1
Size: 52 Color: 0

Bin 30: 1 of cap free
Amount of items: 16
Items: 
Size: 8065 Color: 1
Size: 608 Color: 1
Size: 608 Color: 1
Size: 608 Color: 0
Size: 608 Color: 0
Size: 608 Color: 0
Size: 576 Color: 1
Size: 576 Color: 0
Size: 556 Color: 0
Size: 556 Color: 0
Size: 552 Color: 0
Size: 524 Color: 1
Size: 520 Color: 1
Size: 520 Color: 1
Size: 338 Color: 0
Size: 304 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 10404 Color: 0
Size: 5387 Color: 1
Size: 336 Color: 0

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 11639 Color: 1
Size: 3744 Color: 0
Size: 744 Color: 0

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 12087 Color: 0
Size: 4040 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 1
Size: 2327 Color: 0
Size: 1264 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 13174 Color: 1
Size: 2569 Color: 0
Size: 384 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 13259 Color: 0
Size: 1528 Color: 0
Size: 1340 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 13595 Color: 1
Size: 1684 Color: 1
Size: 848 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 13597 Color: 0
Size: 1602 Color: 0
Size: 928 Color: 1

Bin 39: 1 of cap free
Amount of items: 4
Items: 
Size: 13918 Color: 1
Size: 2021 Color: 0
Size: 156 Color: 1
Size: 32 Color: 0

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 14095 Color: 0
Size: 2032 Color: 1

Bin 41: 2 of cap free
Amount of items: 7
Items: 
Size: 8084 Color: 0
Size: 1612 Color: 0
Size: 1542 Color: 0
Size: 1482 Color: 1
Size: 1430 Color: 1
Size: 1096 Color: 0
Size: 880 Color: 1

Bin 42: 2 of cap free
Amount of items: 5
Items: 
Size: 8080 Color: 1
Size: 5798 Color: 0
Size: 1688 Color: 1
Size: 288 Color: 0
Size: 272 Color: 1

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 9516 Color: 0
Size: 6200 Color: 1
Size: 410 Color: 0

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 9532 Color: 1
Size: 6194 Color: 0
Size: 400 Color: 1

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 9704 Color: 0
Size: 5742 Color: 1
Size: 680 Color: 0

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 10704 Color: 0
Size: 5422 Color: 1

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 11062 Color: 0
Size: 4680 Color: 0
Size: 384 Color: 1

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 11134 Color: 0
Size: 3560 Color: 1
Size: 1432 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 1
Size: 4092 Color: 1
Size: 384 Color: 0

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 14206 Color: 0
Size: 1920 Color: 1

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 14278 Color: 1
Size: 1848 Color: 0

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 1
Size: 3779 Color: 0
Size: 1312 Color: 0

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 12791 Color: 0
Size: 3334 Color: 1

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 13247 Color: 1
Size: 2672 Color: 0
Size: 206 Color: 0

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 14360 Color: 1
Size: 1765 Color: 0

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 1
Size: 1637 Color: 0

Bin 57: 4 of cap free
Amount of items: 8
Items: 
Size: 8082 Color: 0
Size: 1342 Color: 0
Size: 1336 Color: 1
Size: 1328 Color: 1
Size: 1328 Color: 1
Size: 1172 Color: 0
Size: 1168 Color: 0
Size: 368 Color: 1

Bin 58: 4 of cap free
Amount of items: 7
Items: 
Size: 8074 Color: 1
Size: 1484 Color: 0
Size: 1428 Color: 0
Size: 1374 Color: 1
Size: 1340 Color: 1
Size: 1336 Color: 0
Size: 1088 Color: 1

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 8500 Color: 1
Size: 5782 Color: 0
Size: 1842 Color: 0

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 9114 Color: 1
Size: 6706 Color: 0
Size: 304 Color: 1

Bin 61: 4 of cap free
Amount of items: 3
Items: 
Size: 10388 Color: 0
Size: 5368 Color: 1
Size: 368 Color: 0

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 11046 Color: 0
Size: 4182 Color: 0
Size: 896 Color: 1

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 11077 Color: 1
Size: 4211 Color: 0
Size: 836 Color: 1

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 1
Size: 1881 Color: 0
Size: 1851 Color: 0

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 14232 Color: 1
Size: 1892 Color: 0

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 8749 Color: 0
Size: 5790 Color: 1
Size: 1584 Color: 0

Bin 67: 5 of cap free
Amount of items: 3
Items: 
Size: 10879 Color: 1
Size: 4148 Color: 0
Size: 1096 Color: 1

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 11595 Color: 1
Size: 4528 Color: 0

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 13758 Color: 0
Size: 2365 Color: 1

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 14356 Color: 0
Size: 1767 Color: 1

Bin 71: 6 of cap free
Amount of items: 4
Items: 
Size: 8104 Color: 0
Size: 5736 Color: 0
Size: 1482 Color: 1
Size: 800 Color: 1

Bin 72: 6 of cap free
Amount of items: 4
Items: 
Size: 8136 Color: 1
Size: 5846 Color: 1
Size: 1788 Color: 0
Size: 352 Color: 0

Bin 73: 6 of cap free
Amount of items: 3
Items: 
Size: 10394 Color: 1
Size: 5136 Color: 1
Size: 592 Color: 0

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 13122 Color: 1
Size: 3000 Color: 0

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 13320 Color: 0
Size: 2802 Color: 1

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 13451 Color: 0
Size: 2671 Color: 1

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 13874 Color: 1
Size: 2248 Color: 0

Bin 78: 7 of cap free
Amount of items: 4
Items: 
Size: 8090 Color: 0
Size: 5076 Color: 1
Size: 2427 Color: 1
Size: 528 Color: 0

Bin 79: 7 of cap free
Amount of items: 3
Items: 
Size: 10144 Color: 1
Size: 5145 Color: 1
Size: 832 Color: 0

Bin 80: 7 of cap free
Amount of items: 3
Items: 
Size: 12037 Color: 0
Size: 3636 Color: 1
Size: 448 Color: 1

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 12449 Color: 1
Size: 2908 Color: 0
Size: 764 Color: 0

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 12742 Color: 1
Size: 3379 Color: 0

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 13047 Color: 0
Size: 3074 Color: 1

Bin 84: 8 of cap free
Amount of items: 3
Items: 
Size: 9104 Color: 1
Size: 6664 Color: 0
Size: 352 Color: 0

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 0
Size: 4016 Color: 1

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 1
Size: 3504 Color: 1
Size: 448 Color: 0

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 0
Size: 2696 Color: 1

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 13516 Color: 1
Size: 2604 Color: 0

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 0
Size: 2448 Color: 1

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 13712 Color: 1
Size: 2408 Color: 0

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 14142 Color: 0
Size: 1978 Color: 1

Bin 92: 9 of cap free
Amount of items: 3
Items: 
Size: 12447 Color: 1
Size: 3244 Color: 1
Size: 428 Color: 0

Bin 93: 9 of cap free
Amount of items: 2
Items: 
Size: 13300 Color: 1
Size: 2819 Color: 0

Bin 94: 9 of cap free
Amount of items: 2
Items: 
Size: 13888 Color: 0
Size: 2231 Color: 1

Bin 95: 9 of cap free
Amount of items: 2
Items: 
Size: 14256 Color: 0
Size: 1863 Color: 1

Bin 96: 11 of cap free
Amount of items: 3
Items: 
Size: 9095 Color: 1
Size: 6648 Color: 1
Size: 374 Color: 0

Bin 97: 11 of cap free
Amount of items: 3
Items: 
Size: 11110 Color: 0
Size: 4751 Color: 1
Size: 256 Color: 1

Bin 98: 11 of cap free
Amount of items: 2
Items: 
Size: 12962 Color: 0
Size: 3155 Color: 1

Bin 99: 11 of cap free
Amount of items: 2
Items: 
Size: 14011 Color: 1
Size: 2106 Color: 0

Bin 100: 12 of cap free
Amount of items: 2
Items: 
Size: 12747 Color: 0
Size: 3369 Color: 1

Bin 101: 12 of cap free
Amount of items: 2
Items: 
Size: 12988 Color: 1
Size: 3128 Color: 0

Bin 102: 12 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 0
Size: 2860 Color: 1

Bin 103: 12 of cap free
Amount of items: 2
Items: 
Size: 13860 Color: 1
Size: 2256 Color: 0

Bin 104: 13 of cap free
Amount of items: 2
Items: 
Size: 14420 Color: 0
Size: 1695 Color: 1

Bin 105: 14 of cap free
Amount of items: 2
Items: 
Size: 14424 Color: 1
Size: 1690 Color: 0

Bin 106: 16 of cap free
Amount of items: 36
Items: 
Size: 544 Color: 0
Size: 524 Color: 0
Size: 520 Color: 0
Size: 520 Color: 0
Size: 512 Color: 1
Size: 512 Color: 1
Size: 512 Color: 0
Size: 512 Color: 0
Size: 492 Color: 0
Size: 484 Color: 0
Size: 480 Color: 0
Size: 480 Color: 0
Size: 478 Color: 0
Size: 472 Color: 1
Size: 472 Color: 0
Size: 468 Color: 1
Size: 464 Color: 1
Size: 464 Color: 0
Size: 464 Color: 0
Size: 448 Color: 1
Size: 446 Color: 0
Size: 444 Color: 0
Size: 440 Color: 1
Size: 424 Color: 0
Size: 422 Color: 0
Size: 416 Color: 1
Size: 404 Color: 1
Size: 400 Color: 1
Size: 388 Color: 1
Size: 376 Color: 1
Size: 368 Color: 1
Size: 368 Color: 1
Size: 360 Color: 1
Size: 356 Color: 1
Size: 352 Color: 1
Size: 326 Color: 1

Bin 107: 16 of cap free
Amount of items: 2
Items: 
Size: 11288 Color: 1
Size: 4824 Color: 0

Bin 108: 16 of cap free
Amount of items: 2
Items: 
Size: 11864 Color: 0
Size: 4248 Color: 1

Bin 109: 17 of cap free
Amount of items: 3
Items: 
Size: 8468 Color: 1
Size: 7067 Color: 1
Size: 576 Color: 0

Bin 110: 17 of cap free
Amount of items: 2
Items: 
Size: 10671 Color: 0
Size: 5440 Color: 1

Bin 111: 17 of cap free
Amount of items: 2
Items: 
Size: 13463 Color: 1
Size: 2648 Color: 0

Bin 112: 17 of cap free
Amount of items: 2
Items: 
Size: 14009 Color: 1
Size: 2102 Color: 0

Bin 113: 18 of cap free
Amount of items: 3
Items: 
Size: 13291 Color: 0
Size: 2611 Color: 1
Size: 208 Color: 1

Bin 114: 18 of cap free
Amount of items: 2
Items: 
Size: 14165 Color: 0
Size: 1945 Color: 1

Bin 115: 18 of cap free
Amount of items: 2
Items: 
Size: 14350 Color: 1
Size: 1760 Color: 0

Bin 116: 19 of cap free
Amount of items: 9
Items: 
Size: 8069 Color: 1
Size: 1156 Color: 1
Size: 1084 Color: 0
Size: 1080 Color: 1
Size: 1076 Color: 0
Size: 1056 Color: 1
Size: 992 Color: 0
Size: 842 Color: 0
Size: 754 Color: 1

Bin 117: 19 of cap free
Amount of items: 2
Items: 
Size: 14051 Color: 0
Size: 2058 Color: 1

Bin 118: 20 of cap free
Amount of items: 2
Items: 
Size: 13217 Color: 0
Size: 2891 Color: 1

Bin 119: 21 of cap free
Amount of items: 2
Items: 
Size: 13667 Color: 0
Size: 2440 Color: 1

Bin 120: 22 of cap free
Amount of items: 2
Items: 
Size: 9955 Color: 0
Size: 6151 Color: 1

Bin 121: 22 of cap free
Amount of items: 2
Items: 
Size: 11944 Color: 0
Size: 4162 Color: 1

Bin 122: 22 of cap free
Amount of items: 2
Items: 
Size: 14194 Color: 1
Size: 1912 Color: 0

Bin 123: 24 of cap free
Amount of items: 2
Items: 
Size: 13608 Color: 1
Size: 2496 Color: 0

Bin 124: 24 of cap free
Amount of items: 2
Items: 
Size: 13976 Color: 0
Size: 2128 Color: 1

Bin 125: 26 of cap free
Amount of items: 2
Items: 
Size: 14160 Color: 0
Size: 1942 Color: 1

Bin 126: 28 of cap free
Amount of items: 2
Items: 
Size: 14300 Color: 1
Size: 1800 Color: 0

Bin 127: 29 of cap free
Amount of items: 3
Items: 
Size: 12997 Color: 0
Size: 2952 Color: 1
Size: 150 Color: 0

Bin 128: 29 of cap free
Amount of items: 2
Items: 
Size: 14048 Color: 0
Size: 2051 Color: 1

Bin 129: 30 of cap free
Amount of items: 2
Items: 
Size: 12770 Color: 1
Size: 3328 Color: 0

Bin 130: 30 of cap free
Amount of items: 2
Items: 
Size: 13802 Color: 0
Size: 2296 Color: 1

Bin 131: 30 of cap free
Amount of items: 4
Items: 
Size: 14482 Color: 0
Size: 1512 Color: 1
Size: 72 Color: 0
Size: 32 Color: 1

Bin 132: 31 of cap free
Amount of items: 2
Items: 
Size: 13035 Color: 0
Size: 3062 Color: 1

Bin 133: 32 of cap free
Amount of items: 2
Items: 
Size: 12944 Color: 0
Size: 3152 Color: 1

Bin 134: 34 of cap free
Amount of items: 2
Items: 
Size: 11312 Color: 0
Size: 4782 Color: 1

Bin 135: 34 of cap free
Amount of items: 2
Items: 
Size: 13703 Color: 0
Size: 2391 Color: 1

Bin 136: 37 of cap free
Amount of items: 2
Items: 
Size: 13873 Color: 1
Size: 2218 Color: 0

Bin 137: 38 of cap free
Amount of items: 2
Items: 
Size: 13470 Color: 0
Size: 2620 Color: 1

Bin 138: 40 of cap free
Amount of items: 2
Items: 
Size: 13708 Color: 1
Size: 2380 Color: 0

Bin 139: 40 of cap free
Amount of items: 2
Items: 
Size: 14296 Color: 0
Size: 1792 Color: 1

Bin 140: 43 of cap free
Amount of items: 2
Items: 
Size: 14354 Color: 0
Size: 1731 Color: 1

Bin 141: 45 of cap free
Amount of items: 2
Items: 
Size: 13302 Color: 0
Size: 2781 Color: 1

Bin 142: 46 of cap free
Amount of items: 3
Items: 
Size: 12442 Color: 1
Size: 3512 Color: 1
Size: 128 Color: 0

Bin 143: 48 of cap free
Amount of items: 2
Items: 
Size: 13540 Color: 0
Size: 2540 Color: 1

Bin 144: 50 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 0
Size: 2154 Color: 1

Bin 145: 52 of cap free
Amount of items: 2
Items: 
Size: 9580 Color: 0
Size: 6496 Color: 1

Bin 146: 54 of cap free
Amount of items: 3
Items: 
Size: 11156 Color: 0
Size: 4770 Color: 1
Size: 148 Color: 1

Bin 147: 54 of cap free
Amount of items: 2
Items: 
Size: 14292 Color: 0
Size: 1782 Color: 1

Bin 148: 55 of cap free
Amount of items: 2
Items: 
Size: 11136 Color: 1
Size: 4937 Color: 0

Bin 149: 55 of cap free
Amount of items: 2
Items: 
Size: 13004 Color: 1
Size: 3069 Color: 0

Bin 150: 56 of cap free
Amount of items: 3
Items: 
Size: 11192 Color: 1
Size: 4732 Color: 0
Size: 148 Color: 1

Bin 151: 56 of cap free
Amount of items: 2
Items: 
Size: 13610 Color: 0
Size: 2462 Color: 1

Bin 152: 60 of cap free
Amount of items: 2
Items: 
Size: 14224 Color: 0
Size: 1844 Color: 1

Bin 153: 62 of cap free
Amount of items: 2
Items: 
Size: 14046 Color: 0
Size: 2020 Color: 1

Bin 154: 63 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 1
Size: 3855 Color: 0
Size: 128 Color: 0

Bin 155: 65 of cap free
Amount of items: 2
Items: 
Size: 12652 Color: 0
Size: 3411 Color: 1

Bin 156: 66 of cap free
Amount of items: 2
Items: 
Size: 11824 Color: 1
Size: 4238 Color: 0

Bin 157: 66 of cap free
Amount of items: 2
Items: 
Size: 12688 Color: 1
Size: 3374 Color: 0

Bin 158: 66 of cap free
Amount of items: 2
Items: 
Size: 14414 Color: 1
Size: 1648 Color: 0

Bin 159: 70 of cap free
Amount of items: 2
Items: 
Size: 11756 Color: 0
Size: 4302 Color: 1

Bin 160: 74 of cap free
Amount of items: 2
Items: 
Size: 13384 Color: 0
Size: 2670 Color: 1

Bin 161: 75 of cap free
Amount of items: 2
Items: 
Size: 9665 Color: 1
Size: 6388 Color: 0

Bin 162: 77 of cap free
Amount of items: 2
Items: 
Size: 14115 Color: 1
Size: 1936 Color: 0

Bin 163: 79 of cap free
Amount of items: 2
Items: 
Size: 13990 Color: 1
Size: 2059 Color: 0

Bin 164: 80 of cap free
Amount of items: 2
Items: 
Size: 9320 Color: 0
Size: 6728 Color: 1

Bin 165: 84 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 1
Size: 2056 Color: 0

Bin 166: 85 of cap free
Amount of items: 2
Items: 
Size: 11607 Color: 1
Size: 4436 Color: 0

Bin 167: 92 of cap free
Amount of items: 2
Items: 
Size: 13912 Color: 0
Size: 2124 Color: 1

Bin 168: 94 of cap free
Amount of items: 2
Items: 
Size: 13662 Color: 1
Size: 2372 Color: 0

Bin 169: 96 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 0
Size: 3660 Color: 1
Size: 128 Color: 0

Bin 170: 97 of cap free
Amount of items: 2
Items: 
Size: 12661 Color: 1
Size: 3370 Color: 0

Bin 171: 100 of cap free
Amount of items: 2
Items: 
Size: 11220 Color: 1
Size: 4808 Color: 0

Bin 172: 105 of cap free
Amount of items: 2
Items: 
Size: 13659 Color: 1
Size: 2364 Color: 0

Bin 173: 108 of cap free
Amount of items: 2
Items: 
Size: 13909 Color: 0
Size: 2111 Color: 1

Bin 174: 112 of cap free
Amount of items: 2
Items: 
Size: 13795 Color: 0
Size: 2221 Color: 1

Bin 175: 116 of cap free
Amount of items: 2
Items: 
Size: 12644 Color: 0
Size: 3368 Color: 1

Bin 176: 118 of cap free
Amount of items: 2
Items: 
Size: 9638 Color: 0
Size: 6372 Color: 1

Bin 177: 119 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 1
Size: 2725 Color: 0

Bin 178: 125 of cap free
Amount of items: 2
Items: 
Size: 12075 Color: 0
Size: 3928 Color: 1

Bin 179: 130 of cap free
Amount of items: 11
Items: 
Size: 8066 Color: 0
Size: 944 Color: 1
Size: 840 Color: 0
Size: 832 Color: 1
Size: 824 Color: 1
Size: 816 Color: 0
Size: 800 Color: 0
Size: 748 Color: 0
Size: 728 Color: 1
Size: 728 Color: 0
Size: 672 Color: 1

Bin 180: 135 of cap free
Amount of items: 2
Items: 
Size: 11772 Color: 1
Size: 4221 Color: 0

Bin 181: 135 of cap free
Amount of items: 2
Items: 
Size: 12252 Color: 0
Size: 3741 Color: 1

Bin 182: 136 of cap free
Amount of items: 2
Items: 
Size: 9256 Color: 1
Size: 6736 Color: 0

Bin 183: 142 of cap free
Amount of items: 2
Items: 
Size: 9622 Color: 1
Size: 6364 Color: 0

Bin 184: 142 of cap free
Amount of items: 2
Items: 
Size: 11740 Color: 0
Size: 4246 Color: 1

Bin 185: 144 of cap free
Amount of items: 2
Items: 
Size: 10812 Color: 0
Size: 5172 Color: 1

Bin 186: 158 of cap free
Amount of items: 2
Items: 
Size: 9246 Color: 0
Size: 6724 Color: 1

Bin 187: 160 of cap free
Amount of items: 2
Items: 
Size: 10452 Color: 0
Size: 5516 Color: 1

Bin 188: 168 of cap free
Amount of items: 2
Items: 
Size: 9238 Color: 1
Size: 6722 Color: 0

Bin 189: 190 of cap free
Amount of items: 2
Items: 
Size: 9224 Color: 1
Size: 6714 Color: 0

Bin 190: 198 of cap free
Amount of items: 2
Items: 
Size: 10520 Color: 1
Size: 5410 Color: 0

Bin 191: 199 of cap free
Amount of items: 2
Items: 
Size: 10429 Color: 0
Size: 5500 Color: 1

Bin 192: 217 of cap free
Amount of items: 2
Items: 
Size: 9190 Color: 0
Size: 6721 Color: 1

Bin 193: 224 of cap free
Amount of items: 2
Items: 
Size: 8484 Color: 0
Size: 7420 Color: 1

Bin 194: 244 of cap free
Amount of items: 2
Items: 
Size: 9182 Color: 0
Size: 6702 Color: 1

Bin 195: 256 of cap free
Amount of items: 2
Items: 
Size: 10488 Color: 1
Size: 5384 Color: 0

Bin 196: 258 of cap free
Amount of items: 2
Items: 
Size: 9174 Color: 0
Size: 6696 Color: 1

Bin 197: 261 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 1
Size: 5861 Color: 0
Size: 1838 Color: 0

Bin 198: 262 of cap free
Amount of items: 2
Items: 
Size: 10406 Color: 0
Size: 5460 Color: 1

Bin 199: 8138 of cap free
Amount of items: 25
Items: 
Size: 416 Color: 0
Size: 392 Color: 0
Size: 352 Color: 1
Size: 352 Color: 1
Size: 352 Color: 1
Size: 352 Color: 0
Size: 346 Color: 0
Size: 344 Color: 1
Size: 336 Color: 0
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 304 Color: 0
Size: 304 Color: 0
Size: 296 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 284 Color: 0
Size: 280 Color: 1
Size: 272 Color: 1
Size: 272 Color: 1
Size: 272 Color: 0

Total size: 3193344
Total free space: 16128

