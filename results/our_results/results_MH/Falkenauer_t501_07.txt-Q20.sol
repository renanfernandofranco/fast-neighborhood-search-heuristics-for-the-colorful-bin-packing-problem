Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 352 Color: 8
Size: 256 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 311 Color: 2
Size: 306 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 7
Size: 253 Color: 17
Size: 253 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 12
Size: 341 Color: 2
Size: 251 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 10
Size: 278 Color: 14
Size: 255 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 3
Size: 269 Color: 10
Size: 266 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 17
Size: 328 Color: 14
Size: 255 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 5
Size: 283 Color: 17
Size: 262 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 18
Size: 360 Color: 14
Size: 269 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 18
Size: 340 Color: 11
Size: 293 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 17
Size: 282 Color: 19
Size: 253 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 6
Size: 357 Color: 7
Size: 250 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 298 Color: 12
Size: 251 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 5
Size: 361 Color: 0
Size: 272 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 1
Size: 344 Color: 13
Size: 311 Color: 5

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 14
Size: 253 Color: 2
Size: 250 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 6
Size: 267 Color: 18
Size: 267 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 19
Size: 357 Color: 12
Size: 272 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 16
Size: 332 Color: 7
Size: 260 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 10
Size: 324 Color: 9
Size: 277 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 12
Size: 312 Color: 13
Size: 272 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 2
Size: 312 Color: 9
Size: 284 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 17
Size: 317 Color: 0
Size: 253 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 19
Size: 347 Color: 10
Size: 284 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 10
Size: 290 Color: 4
Size: 268 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 16
Size: 328 Color: 19
Size: 300 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 3
Size: 289 Color: 10
Size: 257 Color: 19

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 18
Size: 287 Color: 10
Size: 284 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 366 Color: 8
Size: 258 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 11
Size: 256 Color: 15
Size: 251 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 11
Size: 340 Color: 3
Size: 263 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 8
Size: 255 Color: 8
Size: 250 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 18
Size: 309 Color: 8
Size: 309 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 14
Size: 288 Color: 15
Size: 262 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 359 Color: 2
Size: 250 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7
Size: 355 Color: 7
Size: 273 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 9
Size: 335 Color: 4
Size: 271 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 308 Color: 9
Size: 271 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 13
Size: 263 Color: 15
Size: 260 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 337 Color: 19
Size: 255 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 16
Size: 350 Color: 11
Size: 250 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 19
Size: 291 Color: 11
Size: 251 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 18
Size: 291 Color: 10
Size: 272 Color: 13

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 19
Size: 333 Color: 9
Size: 271 Color: 9

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 3
Size: 325 Color: 3
Size: 318 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 2
Size: 264 Color: 9
Size: 257 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7
Size: 343 Color: 9
Size: 286 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 7
Size: 264 Color: 1
Size: 250 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 4
Size: 359 Color: 4
Size: 271 Color: 12

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 305 Color: 0
Size: 303 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 7
Size: 325 Color: 16
Size: 256 Color: 16

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7
Size: 335 Color: 13
Size: 268 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 17
Size: 301 Color: 12
Size: 266 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 12
Size: 257 Color: 10
Size: 250 Color: 18

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 2
Size: 316 Color: 16
Size: 257 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 300 Color: 8
Size: 269 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7
Size: 344 Color: 11
Size: 276 Color: 13

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 11
Size: 260 Color: 19
Size: 251 Color: 16

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8
Size: 324 Color: 18
Size: 259 Color: 5

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 12
Size: 306 Color: 17
Size: 280 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 13
Size: 351 Color: 15
Size: 280 Color: 9

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 3
Size: 283 Color: 14
Size: 276 Color: 15

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 2
Size: 251 Color: 14
Size: 250 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 10
Size: 273 Color: 13
Size: 250 Color: 11

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 6
Size: 267 Color: 13
Size: 263 Color: 12

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 310 Color: 10
Size: 305 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 15
Size: 285 Color: 19
Size: 266 Color: 16

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 291 Color: 6
Size: 281 Color: 7

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 10
Size: 345 Color: 10
Size: 287 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 10
Size: 296 Color: 4
Size: 271 Color: 16

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 19
Size: 331 Color: 5
Size: 266 Color: 13

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 16
Size: 325 Color: 15
Size: 294 Color: 18

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 8
Size: 343 Color: 10
Size: 314 Color: 15

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 15
Size: 263 Color: 8
Size: 254 Color: 14

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 10
Size: 265 Color: 6
Size: 258 Color: 7

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 6
Size: 279 Color: 7
Size: 259 Color: 5

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 19
Size: 261 Color: 2
Size: 255 Color: 13

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 16
Size: 332 Color: 5
Size: 286 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 5
Size: 290 Color: 3
Size: 262 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 5
Size: 344 Color: 5
Size: 251 Color: 7

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 290 Color: 7
Size: 272 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 16
Size: 259 Color: 18
Size: 254 Color: 5

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 2
Size: 318 Color: 5
Size: 280 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 18
Size: 295 Color: 18
Size: 295 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9
Size: 282 Color: 4
Size: 253 Color: 11

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 10
Size: 307 Color: 17
Size: 256 Color: 16

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 17
Size: 266 Color: 10
Size: 259 Color: 9

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 290 Color: 14
Size: 289 Color: 10

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 13
Size: 256 Color: 9
Size: 250 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 18
Size: 364 Color: 17
Size: 272 Color: 16

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 14
Size: 319 Color: 2
Size: 289 Color: 14

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 11
Size: 361 Color: 7
Size: 263 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 5
Size: 274 Color: 10
Size: 260 Color: 14

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 335 Color: 3
Size: 287 Color: 19

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 340 Color: 14
Size: 286 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 274 Color: 2
Size: 256 Color: 7

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 16
Size: 273 Color: 8
Size: 262 Color: 6

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 3
Size: 300 Color: 13
Size: 271 Color: 4

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7
Size: 374 Color: 18
Size: 250 Color: 18

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 7
Size: 330 Color: 14
Size: 296 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 17
Size: 280 Color: 14
Size: 263 Color: 6

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 278 Color: 1
Size: 266 Color: 13

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 10
Size: 326 Color: 7
Size: 258 Color: 16

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 10
Size: 289 Color: 5
Size: 276 Color: 8

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 15
Size: 284 Color: 7
Size: 260 Color: 10

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 2
Size: 351 Color: 19
Size: 295 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 17
Size: 286 Color: 6
Size: 253 Color: 10

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 5
Size: 322 Color: 14
Size: 250 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 11
Size: 290 Color: 16
Size: 250 Color: 19

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 9
Size: 306 Color: 14
Size: 290 Color: 7

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 2
Size: 301 Color: 4
Size: 253 Color: 5

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 19
Size: 293 Color: 14
Size: 255 Color: 13

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 6
Size: 309 Color: 8
Size: 300 Color: 2

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 4
Size: 306 Color: 2
Size: 271 Color: 8

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 15
Size: 270 Color: 19
Size: 251 Color: 18

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 17
Size: 349 Color: 10
Size: 288 Color: 16

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 364 Color: 9
Size: 266 Color: 9

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 18
Size: 317 Color: 17
Size: 295 Color: 12

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 336 Color: 4
Size: 276 Color: 2

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 12
Size: 354 Color: 15
Size: 280 Color: 9

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 4
Size: 300 Color: 16
Size: 291 Color: 15

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 17
Size: 257 Color: 15
Size: 251 Color: 15

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 18
Size: 302 Color: 4
Size: 252 Color: 9

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 16
Size: 335 Color: 7
Size: 296 Color: 16

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 6
Size: 307 Color: 12
Size: 290 Color: 10

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 6
Size: 355 Color: 4
Size: 251 Color: 16

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 267 Color: 3
Size: 263 Color: 14

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 11
Size: 268 Color: 5
Size: 263 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 10
Size: 303 Color: 6
Size: 277 Color: 2

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 17
Size: 309 Color: 18
Size: 260 Color: 19

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 13
Size: 304 Color: 16
Size: 281 Color: 14

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 14
Size: 274 Color: 15
Size: 256 Color: 3

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8
Size: 318 Color: 19
Size: 251 Color: 3

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 15
Size: 273 Color: 7
Size: 250 Color: 11

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 269 Color: 4
Size: 268 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 6
Size: 274 Color: 4
Size: 274 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 16
Size: 352 Color: 9
Size: 257 Color: 19

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 10
Size: 350 Color: 12
Size: 295 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9
Size: 279 Color: 4
Size: 271 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 360 Color: 3
Size: 256 Color: 4

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 369 Color: 4
Size: 256 Color: 6

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 19
Size: 366 Color: 8
Size: 259 Color: 9

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 16
Size: 308 Color: 10
Size: 280 Color: 5

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 317 Color: 2
Size: 311 Color: 9

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 17
Size: 270 Color: 4
Size: 263 Color: 15

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 13
Size: 343 Color: 6
Size: 312 Color: 17

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 5
Size: 300 Color: 11
Size: 266 Color: 10

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 18
Size: 257 Color: 15
Size: 252 Color: 6

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 18
Size: 316 Color: 15
Size: 263 Color: 6

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 8
Size: 262 Color: 9
Size: 258 Color: 7

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 18
Size: 320 Color: 5
Size: 273 Color: 7

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 11
Size: 257 Color: 15
Size: 251 Color: 3

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 17
Size: 328 Color: 19
Size: 277 Color: 7

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 16
Size: 279 Color: 15
Size: 276 Color: 8

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 10
Size: 265 Color: 4
Size: 251 Color: 12

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 5
Size: 268 Color: 19
Size: 253 Color: 15

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 17
Size: 273 Color: 9
Size: 261 Color: 2

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 13
Size: 328 Color: 17
Size: 304 Color: 8

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 340 Color: 1
Size: 253 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 17
Size: 265 Color: 4
Size: 264 Color: 15

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 11
Size: 350 Color: 4
Size: 289 Color: 19

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 7
Size: 288 Color: 2
Size: 282 Color: 11

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 11
Size: 309 Color: 13
Size: 301 Color: 7

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 19
Size: 251 Color: 6
Size: 250 Color: 6

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 17
Size: 308 Color: 15
Size: 297 Color: 16

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 16
Size: 299 Color: 7
Size: 268 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 19
Size: 299 Color: 13
Size: 252 Color: 10

Total size: 167000
Total free space: 0

