Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 3
Size: 270 Color: 16
Size: 251 Color: 12

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 19
Size: 256 Color: 8
Size: 251 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 15
Size: 273 Color: 15
Size: 261 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 17
Size: 291 Color: 6
Size: 275 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 366 Color: 19
Size: 254 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 11
Size: 361 Color: 13
Size: 275 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 0
Size: 295 Color: 6
Size: 252 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 19
Size: 315 Color: 1
Size: 261 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 277 Color: 18
Size: 252 Color: 3
Size: 471 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 11
Size: 255 Color: 2
Size: 253 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 12
Size: 281 Color: 14
Size: 276 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7
Size: 319 Color: 4
Size: 304 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 4
Size: 290 Color: 12
Size: 271 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 11
Size: 255 Color: 4
Size: 250 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 15
Size: 331 Color: 10
Size: 296 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 16
Size: 352 Color: 11
Size: 290 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 16
Size: 370 Color: 7
Size: 253 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 337 Color: 11
Size: 278 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 18
Size: 324 Color: 11
Size: 256 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 347 Color: 2
Size: 254 Color: 17

Total size: 20000
Total free space: 0

