Capicity Bin: 1001
Lower Bound: 667

Bins used: 667
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 17
Size: 343 Color: 18
Size: 300 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 6
Size: 279 Color: 12
Size: 268 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 18
Size: 295 Color: 16
Size: 277 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 18
Size: 292 Color: 13
Size: 284 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 5
Size: 315 Color: 18
Size: 309 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9
Size: 270 Color: 13
Size: 262 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 3
Size: 288 Color: 7
Size: 251 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 7
Size: 301 Color: 3
Size: 296 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 8
Size: 367 Color: 1
Size: 260 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 9
Size: 322 Color: 9
Size: 312 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 0
Size: 306 Color: 5
Size: 280 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 16
Size: 281 Color: 13
Size: 255 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 2
Size: 327 Color: 13
Size: 322 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 11
Size: 279 Color: 14
Size: 257 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 5
Size: 319 Color: 1
Size: 260 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 15
Size: 324 Color: 18
Size: 305 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 16
Size: 340 Color: 2
Size: 289 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 10
Size: 267 Color: 2
Size: 261 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 340 Color: 0
Size: 283 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 3
Size: 318 Color: 16
Size: 285 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 273 Color: 17
Size: 273 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 11
Size: 328 Color: 14
Size: 251 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 2
Size: 357 Color: 11
Size: 279 Color: 11

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 15
Size: 313 Color: 6
Size: 251 Color: 17

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 10
Size: 264 Color: 5
Size: 252 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 3
Size: 275 Color: 17
Size: 274 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 10
Size: 268 Color: 10
Size: 253 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 13
Size: 290 Color: 18
Size: 260 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 304 Color: 18
Size: 278 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 15
Size: 340 Color: 9
Size: 305 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 5
Size: 297 Color: 16
Size: 290 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 17
Size: 288 Color: 19
Size: 275 Color: 11

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 15
Size: 374 Color: 13
Size: 253 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 16
Size: 303 Color: 15
Size: 298 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 17
Size: 274 Color: 1
Size: 268 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 14
Size: 266 Color: 7
Size: 263 Color: 14

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 14
Size: 341 Color: 16
Size: 277 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 7
Size: 305 Color: 16
Size: 268 Color: 7

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 4
Size: 350 Color: 12
Size: 291 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 4
Size: 269 Color: 10
Size: 259 Color: 8

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 332 Color: 2
Size: 292 Color: 14

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 14
Size: 331 Color: 11
Size: 321 Color: 7

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 16
Size: 329 Color: 12
Size: 289 Color: 14

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 8
Size: 331 Color: 0
Size: 327 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 11
Size: 267 Color: 5
Size: 265 Color: 6

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 14
Size: 350 Color: 10
Size: 255 Color: 10

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 0
Size: 338 Color: 6
Size: 318 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 16
Size: 275 Color: 0
Size: 271 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 13
Size: 317 Color: 2
Size: 279 Color: 11

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8
Size: 316 Color: 2
Size: 271 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 8
Size: 274 Color: 0
Size: 259 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 8
Size: 322 Color: 8
Size: 287 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 356 Color: 14
Size: 253 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 10
Size: 321 Color: 17
Size: 260 Color: 13

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 10
Size: 309 Color: 11
Size: 268 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 14
Size: 356 Color: 6
Size: 271 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 302 Color: 13
Size: 250 Color: 6

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 7
Size: 289 Color: 2
Size: 251 Color: 13

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 9
Size: 315 Color: 18
Size: 291 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 7
Size: 317 Color: 6
Size: 270 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 14
Size: 258 Color: 2
Size: 251 Color: 15

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 10
Size: 356 Color: 13
Size: 278 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 11
Size: 319 Color: 15
Size: 299 Color: 5

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 12
Size: 296 Color: 11
Size: 261 Color: 12

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 19
Size: 287 Color: 1
Size: 277 Color: 17

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8
Size: 321 Color: 16
Size: 271 Color: 18

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 14
Size: 347 Color: 16
Size: 283 Color: 6

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 15
Size: 296 Color: 12
Size: 287 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 350 Color: 1
Size: 255 Color: 19

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 10
Size: 299 Color: 16
Size: 264 Color: 18

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 5
Size: 284 Color: 14
Size: 282 Color: 16

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 333 Color: 10
Size: 303 Color: 19

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 15
Size: 274 Color: 2
Size: 274 Color: 13

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 12
Size: 293 Color: 8
Size: 267 Color: 15

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 17
Size: 279 Color: 10
Size: 273 Color: 13

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 2
Size: 325 Color: 3
Size: 300 Color: 16

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 18
Size: 291 Color: 1
Size: 262 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 18
Size: 259 Color: 0
Size: 256 Color: 10

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 14
Size: 308 Color: 0
Size: 301 Color: 18

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 5
Size: 360 Color: 14
Size: 260 Color: 16

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 343 Color: 15
Size: 279 Color: 12

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 3
Size: 272 Color: 0
Size: 255 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9
Size: 261 Color: 7
Size: 254 Color: 17

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 13
Size: 341 Color: 5
Size: 300 Color: 7

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 10
Size: 250 Color: 12
Size: 250 Color: 12

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6
Size: 327 Color: 15
Size: 318 Color: 7

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 16
Size: 286 Color: 0
Size: 284 Color: 11

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 305 Color: 16
Size: 260 Color: 11

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 289 Color: 0
Size: 256 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8
Size: 324 Color: 2
Size: 277 Color: 7

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 14
Size: 299 Color: 2
Size: 263 Color: 15

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 370 Color: 13
Size: 253 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 7
Size: 325 Color: 8
Size: 318 Color: 13

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 15
Size: 302 Color: 17
Size: 302 Color: 7

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 6
Size: 336 Color: 13
Size: 262 Color: 19

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 14
Size: 283 Color: 1
Size: 257 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 17
Size: 328 Color: 18
Size: 280 Color: 19

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 13
Size: 368 Color: 19
Size: 256 Color: 12

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 17
Size: 265 Color: 13
Size: 260 Color: 8

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7
Size: 309 Color: 12
Size: 299 Color: 18

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 3
Size: 292 Color: 10
Size: 255 Color: 8

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 348 Color: 8
Size: 265 Color: 14

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 13
Size: 288 Color: 10
Size: 262 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 11
Size: 292 Color: 9
Size: 277 Color: 6

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 18
Size: 271 Color: 3
Size: 269 Color: 16

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 0
Size: 285 Color: 3
Size: 277 Color: 13

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 2
Size: 318 Color: 14
Size: 259 Color: 13

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 11
Size: 313 Color: 10
Size: 267 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 17
Size: 259 Color: 12
Size: 257 Color: 3

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 11
Size: 283 Color: 17
Size: 262 Color: 5

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 15
Size: 375 Color: 6
Size: 250 Color: 10

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 9
Size: 500 Color: 14

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 14
Size: 306 Color: 12
Size: 259 Color: 9

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 19
Size: 290 Color: 11
Size: 254 Color: 13

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 19
Size: 288 Color: 8
Size: 266 Color: 5

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 7
Size: 302 Color: 5
Size: 263 Color: 3

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 16
Size: 291 Color: 14
Size: 277 Color: 15

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 11
Size: 270 Color: 10
Size: 251 Color: 11

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 19
Size: 348 Color: 0
Size: 269 Color: 5

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 6
Size: 334 Color: 19
Size: 277 Color: 4

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 12
Size: 344 Color: 6
Size: 264 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 5
Size: 292 Color: 3
Size: 269 Color: 19

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 11
Size: 281 Color: 1
Size: 273 Color: 11

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 15
Size: 355 Color: 6
Size: 272 Color: 14

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 16
Size: 319 Color: 11
Size: 257 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 19
Size: 289 Color: 2
Size: 263 Color: 6

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 10
Size: 290 Color: 8
Size: 251 Color: 10

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 15
Size: 268 Color: 11
Size: 251 Color: 11

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 11
Size: 265 Color: 4
Size: 250 Color: 16

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 8
Size: 304 Color: 19
Size: 303 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 9
Size: 305 Color: 7
Size: 278 Color: 5

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 16
Size: 332 Color: 15
Size: 287 Color: 14

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 16
Size: 314 Color: 11
Size: 279 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 4
Size: 272 Color: 14
Size: 258 Color: 12

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 7
Size: 276 Color: 12
Size: 255 Color: 17

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 3
Size: 346 Color: 5
Size: 298 Color: 8

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 307 Color: 15
Size: 281 Color: 4

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 17
Size: 334 Color: 14
Size: 321 Color: 9

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 3
Size: 262 Color: 10
Size: 254 Color: 3

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 12
Size: 331 Color: 15
Size: 251 Color: 6

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 12
Size: 258 Color: 3
Size: 251 Color: 5

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 8
Size: 322 Color: 5
Size: 315 Color: 15

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 11
Size: 356 Color: 19
Size: 272 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 13
Size: 350 Color: 1
Size: 265 Color: 12

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 15
Size: 297 Color: 5
Size: 258 Color: 19

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 8
Size: 256 Color: 9
Size: 250 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 3
Size: 305 Color: 8
Size: 260 Color: 12

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 4
Size: 252 Color: 12
Size: 251 Color: 11

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 6
Size: 271 Color: 8
Size: 259 Color: 5

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 10
Size: 326 Color: 19
Size: 270 Color: 8

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 10
Size: 337 Color: 14
Size: 284 Color: 7

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 9
Size: 297 Color: 0
Size: 251 Color: 6

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 14
Size: 327 Color: 4
Size: 304 Color: 18

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 8
Size: 317 Color: 2
Size: 315 Color: 19

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 8
Size: 369 Color: 3
Size: 259 Color: 18

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 8
Size: 336 Color: 12
Size: 306 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 17
Size: 297 Color: 16
Size: 260 Color: 4

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 8
Size: 315 Color: 3
Size: 305 Color: 18

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 0
Size: 294 Color: 16
Size: 268 Color: 6

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 13
Size: 325 Color: 6
Size: 280 Color: 9

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 16
Size: 271 Color: 18
Size: 257 Color: 14

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 15
Size: 339 Color: 14
Size: 258 Color: 15

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 340 Color: 2
Size: 295 Color: 14

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 10
Size: 339 Color: 13
Size: 254 Color: 13

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 17
Size: 319 Color: 3
Size: 297 Color: 17

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 15
Size: 297 Color: 2
Size: 295 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 13
Size: 334 Color: 7
Size: 321 Color: 14

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 12
Size: 323 Color: 14
Size: 287 Color: 15

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 7
Size: 286 Color: 19
Size: 259 Color: 8

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 11
Size: 320 Color: 18
Size: 297 Color: 15

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 18
Size: 274 Color: 8
Size: 274 Color: 3

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 12
Size: 349 Color: 6
Size: 292 Color: 1

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 316 Color: 3
Size: 253 Color: 9

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9
Size: 271 Color: 11
Size: 262 Color: 4

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 19
Size: 352 Color: 3
Size: 262 Color: 13

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 10
Size: 302 Color: 2
Size: 282 Color: 14

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 5
Size: 347 Color: 7
Size: 294 Color: 16

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 11
Size: 261 Color: 17
Size: 256 Color: 7

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 12
Size: 266 Color: 12
Size: 253 Color: 14

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 9
Size: 330 Color: 0
Size: 253 Color: 10

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 12
Size: 327 Color: 1
Size: 310 Color: 7

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 3
Size: 296 Color: 8
Size: 289 Color: 12

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 4
Size: 327 Color: 3
Size: 250 Color: 19

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 5
Size: 364 Color: 12
Size: 272 Color: 7

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 368 Color: 5
Size: 250 Color: 17

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 7
Size: 293 Color: 11
Size: 293 Color: 9

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 19
Size: 288 Color: 16
Size: 274 Color: 15

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 5
Size: 294 Color: 13
Size: 293 Color: 8

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 16
Size: 346 Color: 0
Size: 284 Color: 14

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 329 Color: 2
Size: 253 Color: 18

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 8
Size: 274 Color: 1
Size: 268 Color: 7

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 12
Size: 326 Color: 15
Size: 282 Color: 8

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 18
Size: 339 Color: 18
Size: 254 Color: 9

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 6
Size: 263 Color: 4
Size: 253 Color: 19

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 9
Size: 352 Color: 19
Size: 290 Color: 19

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 4
Size: 285 Color: 18
Size: 259 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 6
Size: 295 Color: 1
Size: 294 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 14
Size: 329 Color: 3
Size: 303 Color: 3

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 6
Size: 324 Color: 12
Size: 274 Color: 7

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 350 Color: 11
Size: 269 Color: 5

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 2
Size: 340 Color: 0
Size: 252 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 301 Color: 19
Size: 300 Color: 12

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 13
Size: 313 Color: 3
Size: 250 Color: 11

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 322 Color: 11
Size: 299 Color: 9

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 286 Color: 14
Size: 253 Color: 8

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 4
Size: 329 Color: 11
Size: 263 Color: 11

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 6
Size: 310 Color: 17
Size: 260 Color: 5

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7
Size: 319 Color: 6
Size: 309 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 6
Size: 299 Color: 15
Size: 257 Color: 3

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 9
Size: 305 Color: 19
Size: 278 Color: 6

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 313 Color: 2
Size: 311 Color: 15

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 19
Size: 284 Color: 4
Size: 259 Color: 4

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 18
Size: 337 Color: 6
Size: 257 Color: 11

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 11
Size: 331 Color: 17
Size: 297 Color: 12

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 17
Size: 273 Color: 15
Size: 253 Color: 8

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 10
Size: 258 Color: 7
Size: 254 Color: 7

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 5
Size: 329 Color: 14
Size: 280 Color: 9

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 2
Size: 333 Color: 19
Size: 274 Color: 9

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 8
Size: 357 Color: 16
Size: 273 Color: 9

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 290 Color: 19
Size: 288 Color: 7

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 16
Size: 297 Color: 18
Size: 283 Color: 3

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 7
Size: 263 Color: 16
Size: 258 Color: 18

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 12
Size: 298 Color: 4
Size: 284 Color: 6

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 328 Color: 7
Size: 282 Color: 3

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 16
Size: 316 Color: 16
Size: 303 Color: 10

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9
Size: 276 Color: 3
Size: 273 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 370 Color: 0
Size: 250 Color: 9

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 11
Size: 305 Color: 11
Size: 283 Color: 4

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 19
Size: 342 Color: 18
Size: 267 Color: 17

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 12
Size: 314 Color: 14
Size: 309 Color: 16

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 10
Size: 332 Color: 18
Size: 297 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 7
Size: 256 Color: 5
Size: 251 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7
Size: 343 Color: 8
Size: 261 Color: 2

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 295 Color: 7
Size: 264 Color: 15

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 7
Size: 285 Color: 5
Size: 263 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8
Size: 307 Color: 6
Size: 292 Color: 2

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 3
Size: 266 Color: 8
Size: 258 Color: 19

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 5
Size: 311 Color: 2
Size: 285 Color: 6

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 259 Color: 16
Size: 251 Color: 16

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 8
Size: 317 Color: 17
Size: 305 Color: 18

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 12
Size: 323 Color: 6
Size: 264 Color: 16

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 2
Size: 347 Color: 11
Size: 295 Color: 2

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 13
Size: 329 Color: 12
Size: 258 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 8
Size: 324 Color: 6
Size: 283 Color: 8

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 8
Size: 316 Color: 5
Size: 305 Color: 2

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 16
Size: 285 Color: 15
Size: 254 Color: 14

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 10
Size: 289 Color: 5
Size: 276 Color: 6

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 7
Size: 259 Color: 8
Size: 251 Color: 3

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 16
Size: 330 Color: 5
Size: 278 Color: 10

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 11
Size: 303 Color: 0
Size: 260 Color: 13

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 8
Size: 251 Color: 19
Size: 251 Color: 12

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 14
Size: 266 Color: 13
Size: 258 Color: 5

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 331 Color: 15
Size: 300 Color: 10

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 14
Size: 346 Color: 11
Size: 253 Color: 10

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 12
Size: 323 Color: 17
Size: 316 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 17
Size: 345 Color: 10
Size: 294 Color: 9

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 349 Color: 13
Size: 267 Color: 6

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 252 Color: 1
Size: 251 Color: 12

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 17
Size: 263 Color: 15
Size: 257 Color: 7

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 16
Size: 333 Color: 6
Size: 298 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 9
Size: 321 Color: 6
Size: 265 Color: 3

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 13
Size: 349 Color: 6
Size: 253 Color: 3

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 10
Size: 255 Color: 11
Size: 251 Color: 5

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 259 Color: 11
Size: 256 Color: 16

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 17
Size: 293 Color: 1
Size: 291 Color: 10

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 15
Size: 334 Color: 11
Size: 290 Color: 19

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 3
Size: 300 Color: 6
Size: 285 Color: 4

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 14
Size: 301 Color: 2
Size: 250 Color: 2

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 18
Size: 269 Color: 3
Size: 263 Color: 12

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 296 Color: 12
Size: 257 Color: 6

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 12
Size: 289 Color: 8
Size: 273 Color: 18

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9
Size: 278 Color: 0
Size: 277 Color: 17

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 328 Color: 0
Size: 279 Color: 12

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 5
Size: 288 Color: 17
Size: 280 Color: 5

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 312 Color: 5
Size: 258 Color: 17

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 13
Size: 361 Color: 18
Size: 264 Color: 16

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 13
Size: 350 Color: 10
Size: 255 Color: 5

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 330 Color: 7
Size: 279 Color: 3

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 10
Size: 336 Color: 17
Size: 311 Color: 4

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 4
Size: 314 Color: 6
Size: 274 Color: 3

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 320 Color: 9
Size: 255 Color: 5

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 19
Size: 286 Color: 19
Size: 262 Color: 2

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 18
Size: 332 Color: 10
Size: 297 Color: 11

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 16
Size: 315 Color: 19
Size: 257 Color: 8

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 15
Size: 271 Color: 8
Size: 268 Color: 18

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 4
Size: 303 Color: 5
Size: 259 Color: 4

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 3
Size: 284 Color: 18
Size: 259 Color: 14

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 15
Size: 326 Color: 16
Size: 308 Color: 8

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 6
Size: 323 Color: 5
Size: 259 Color: 3

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 18
Size: 357 Color: 4
Size: 277 Color: 18

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 16
Size: 306 Color: 3
Size: 252 Color: 9

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 0
Size: 322 Color: 13
Size: 280 Color: 15

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 282 Color: 3
Size: 255 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 11
Size: 289 Color: 4
Size: 251 Color: 13

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8
Size: 284 Color: 13
Size: 283 Color: 15

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 10
Size: 317 Color: 8
Size: 284 Color: 4

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 13
Size: 327 Color: 17
Size: 327 Color: 5

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 10
Size: 299 Color: 14
Size: 289 Color: 10

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 12
Size: 300 Color: 7
Size: 298 Color: 16

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 0
Size: 334 Color: 19
Size: 325 Color: 8

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 5
Size: 341 Color: 8
Size: 258 Color: 19

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 3
Size: 300 Color: 3
Size: 285 Color: 13

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 4
Size: 322 Color: 9
Size: 252 Color: 17

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 15
Size: 308 Color: 8
Size: 288 Color: 5

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 10
Size: 351 Color: 17
Size: 252 Color: 2

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 4
Size: 309 Color: 18
Size: 264 Color: 5

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 3
Size: 285 Color: 16
Size: 260 Color: 19

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 19
Size: 255 Color: 1
Size: 252 Color: 4

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 13
Size: 359 Color: 15
Size: 268 Color: 19

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 6
Size: 296 Color: 2
Size: 263 Color: 8

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 17
Size: 351 Color: 8
Size: 253 Color: 18

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 12
Size: 306 Color: 15
Size: 254 Color: 18

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 2
Size: 343 Color: 10
Size: 285 Color: 15

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 12
Size: 320 Color: 4
Size: 303 Color: 7

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 5
Size: 354 Color: 5
Size: 265 Color: 4

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 3
Size: 282 Color: 7
Size: 260 Color: 15

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 5
Size: 331 Color: 11
Size: 322 Color: 12

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 13
Size: 308 Color: 16
Size: 307 Color: 11

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 14
Size: 349 Color: 5
Size: 285 Color: 19

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 276 Color: 5
Size: 261 Color: 3

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 19
Size: 356 Color: 10
Size: 251 Color: 9

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 359 Color: 5
Size: 258 Color: 17

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 17
Size: 298 Color: 11
Size: 274 Color: 14

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 14
Size: 358 Color: 10
Size: 267 Color: 3

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 17
Size: 265 Color: 7
Size: 260 Color: 10

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 6
Size: 320 Color: 3
Size: 306 Color: 19

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 11
Size: 342 Color: 9
Size: 264 Color: 10

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 8
Size: 334 Color: 7
Size: 308 Color: 6

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 13
Size: 352 Color: 1
Size: 267 Color: 13

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 13
Size: 290 Color: 9
Size: 261 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 352 Color: 10
Size: 282 Color: 10

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7
Size: 341 Color: 11
Size: 272 Color: 15

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 16
Size: 271 Color: 12
Size: 261 Color: 2

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 12
Size: 340 Color: 2
Size: 282 Color: 7

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 11
Size: 276 Color: 16
Size: 253 Color: 13

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 14
Size: 272 Color: 12
Size: 253 Color: 2

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 339 Color: 7
Size: 269 Color: 3

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 0
Size: 328 Color: 17
Size: 264 Color: 5

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7
Size: 367 Color: 3
Size: 265 Color: 11

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 16
Size: 329 Color: 8
Size: 252 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 9
Size: 344 Color: 4
Size: 287 Color: 19

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 16
Size: 257 Color: 13
Size: 257 Color: 6

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 16
Size: 275 Color: 16
Size: 265 Color: 5

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 15
Size: 338 Color: 4
Size: 278 Color: 5

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 10
Size: 351 Color: 3
Size: 250 Color: 16

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 15
Size: 255 Color: 1
Size: 254 Color: 2

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 336 Color: 3
Size: 268 Color: 8

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 12
Size: 339 Color: 1
Size: 311 Color: 18

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 10
Size: 266 Color: 17
Size: 252 Color: 11

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 309 Color: 10
Size: 260 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 324 Color: 10
Size: 250 Color: 2

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 17
Size: 313 Color: 18
Size: 291 Color: 17

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 2
Size: 350 Color: 15
Size: 262 Color: 13

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 11
Size: 325 Color: 10
Size: 271 Color: 14

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 3
Size: 355 Color: 15
Size: 262 Color: 2

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 11
Size: 297 Color: 4
Size: 291 Color: 2

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 11
Size: 294 Color: 9
Size: 272 Color: 5

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 11
Size: 336 Color: 5
Size: 285 Color: 17

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 330 Color: 8
Size: 253 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 6
Size: 318 Color: 0
Size: 269 Color: 6

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8
Size: 299 Color: 16
Size: 262 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 18
Size: 268 Color: 12
Size: 263 Color: 6

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 19
Size: 319 Color: 14
Size: 263 Color: 17

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 15
Size: 342 Color: 4
Size: 295 Color: 3

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 299 Color: 3
Size: 253 Color: 17

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 5
Size: 362 Color: 15
Size: 263 Color: 6

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 11
Size: 320 Color: 18
Size: 292 Color: 1

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 16
Size: 319 Color: 5
Size: 271 Color: 18

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 19
Size: 316 Color: 4
Size: 305 Color: 19

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 9
Size: 316 Color: 6
Size: 289 Color: 6

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 19
Size: 320 Color: 7
Size: 284 Color: 6

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 12
Size: 334 Color: 8
Size: 299 Color: 6

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 17
Size: 276 Color: 0
Size: 254 Color: 5

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 16
Size: 279 Color: 6
Size: 276 Color: 9

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 15
Size: 280 Color: 14
Size: 250 Color: 3

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 16
Size: 304 Color: 10
Size: 290 Color: 6

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 5
Size: 267 Color: 4
Size: 258 Color: 16

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 4
Size: 310 Color: 8
Size: 274 Color: 2

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 10
Size: 309 Color: 10
Size: 281 Color: 5

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7
Size: 324 Color: 7
Size: 283 Color: 10

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 11
Size: 348 Color: 3
Size: 284 Color: 6

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 321 Color: 10
Size: 259 Color: 11

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 9
Size: 337 Color: 19
Size: 264 Color: 4

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 302 Color: 7
Size: 283 Color: 12
Size: 416 Color: 6

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 3
Size: 288 Color: 13
Size: 255 Color: 2

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 12
Size: 355 Color: 12
Size: 267 Color: 19

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 15
Size: 272 Color: 2
Size: 264 Color: 13

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 2
Size: 330 Color: 4
Size: 262 Color: 12

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 312 Color: 9
Size: 312 Color: 3

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 5
Size: 276 Color: 18
Size: 254 Color: 11

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7
Size: 354 Color: 2
Size: 251 Color: 3

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 13
Size: 294 Color: 5
Size: 285 Color: 15

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 3
Size: 286 Color: 18
Size: 284 Color: 17

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 4
Size: 351 Color: 0
Size: 291 Color: 12

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 328 Color: 5
Size: 296 Color: 11

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 15
Size: 290 Color: 3
Size: 281 Color: 7

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 18
Size: 334 Color: 12
Size: 261 Color: 17

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 11
Size: 263 Color: 3
Size: 256 Color: 6

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 15
Size: 283 Color: 6
Size: 268 Color: 1

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 3
Size: 279 Color: 1
Size: 257 Color: 3

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 13
Size: 354 Color: 7
Size: 279 Color: 7

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 17
Size: 311 Color: 2
Size: 262 Color: 0

Bin 403: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 13
Size: 250 Color: 14
Size: 250 Color: 7
Size: 250 Color: 7

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 5
Size: 334 Color: 2
Size: 296 Color: 3

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 311 Color: 13
Size: 307 Color: 8

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 11
Size: 360 Color: 16
Size: 271 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 5
Size: 334 Color: 16
Size: 321 Color: 11

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 2
Size: 324 Color: 11
Size: 260 Color: 12

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 17
Size: 284 Color: 3
Size: 283 Color: 16

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7
Size: 358 Color: 4
Size: 251 Color: 2

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 13
Size: 260 Color: 8
Size: 256 Color: 0

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 354 Color: 1
Size: 268 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 18
Size: 283 Color: 10
Size: 254 Color: 1

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 16
Size: 289 Color: 12
Size: 275 Color: 11

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 5
Size: 348 Color: 12
Size: 277 Color: 12

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 17
Size: 317 Color: 10
Size: 315 Color: 4

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 16
Size: 300 Color: 18
Size: 292 Color: 3

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 18
Size: 283 Color: 3
Size: 253 Color: 11

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 19
Size: 263 Color: 10
Size: 250 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 342 Color: 8
Size: 280 Color: 1

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 17
Size: 264 Color: 3
Size: 251 Color: 13

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 9
Size: 290 Color: 17
Size: 283 Color: 14

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 13
Size: 269 Color: 1
Size: 263 Color: 16

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 17
Size: 331 Color: 2
Size: 297 Color: 17

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 7
Size: 290 Color: 16
Size: 254 Color: 12

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7
Size: 335 Color: 7
Size: 289 Color: 18

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 6
Size: 315 Color: 8
Size: 252 Color: 9

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 338 Color: 19
Size: 337 Color: 15
Size: 326 Color: 5

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 306 Color: 12
Size: 259 Color: 8

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 10
Size: 355 Color: 0
Size: 278 Color: 5

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 17
Size: 283 Color: 8
Size: 275 Color: 5

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 19
Size: 254 Color: 12
Size: 250 Color: 5

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 16
Size: 351 Color: 1
Size: 289 Color: 3

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 297 Color: 2
Size: 253 Color: 6

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 310 Color: 0
Size: 261 Color: 4

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 15
Size: 341 Color: 19
Size: 303 Color: 13

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 7
Size: 284 Color: 12
Size: 252 Color: 15

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 13
Size: 290 Color: 1
Size: 265 Color: 2

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 314 Color: 15
Size: 307 Color: 5

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 7
Size: 323 Color: 3
Size: 255 Color: 15

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 328 Color: 16
Size: 303 Color: 2

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 357 Color: 13
Size: 269 Color: 9

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 5
Size: 261 Color: 2
Size: 254 Color: 12

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 10
Size: 358 Color: 15
Size: 266 Color: 6

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 3
Size: 296 Color: 15
Size: 284 Color: 9

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 17
Size: 303 Color: 8
Size: 252 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 11
Size: 317 Color: 12
Size: 304 Color: 1

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 5
Size: 267 Color: 3
Size: 253 Color: 2

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 18
Size: 350 Color: 8
Size: 295 Color: 14

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 289 Color: 9
Size: 259 Color: 8

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 11
Size: 287 Color: 9
Size: 253 Color: 16

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 14
Size: 275 Color: 19
Size: 256 Color: 13

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 10
Size: 320 Color: 12
Size: 312 Color: 18

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 5
Size: 317 Color: 5
Size: 311 Color: 17

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 14
Size: 373 Color: 9
Size: 253 Color: 4

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 4
Size: 288 Color: 17
Size: 271 Color: 15

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 9
Size: 331 Color: 12
Size: 328 Color: 14

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 16
Size: 308 Color: 5
Size: 266 Color: 2

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 4
Size: 326 Color: 6
Size: 251 Color: 14

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 10
Size: 292 Color: 1
Size: 270 Color: 9

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 15
Size: 267 Color: 1
Size: 251 Color: 6

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 323 Color: 9
Size: 308 Color: 10

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 4
Size: 293 Color: 9
Size: 275 Color: 6

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 11
Size: 279 Color: 6
Size: 269 Color: 5

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 6
Size: 289 Color: 13
Size: 252 Color: 15

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 10
Size: 256 Color: 7
Size: 250 Color: 3

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 5
Size: 276 Color: 13
Size: 265 Color: 8

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 10
Size: 347 Color: 0
Size: 286 Color: 16

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 12
Size: 333 Color: 10
Size: 284 Color: 8

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 9
Size: 341 Color: 2
Size: 258 Color: 9

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 6
Size: 294 Color: 7
Size: 289 Color: 16

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 12
Size: 358 Color: 19
Size: 250 Color: 16

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 254 Color: 2
Size: 253 Color: 7

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 12
Size: 260 Color: 8
Size: 259 Color: 11

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 16
Size: 258 Color: 17
Size: 256 Color: 3

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 8
Size: 289 Color: 7
Size: 250 Color: 13

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 12
Size: 333 Color: 8
Size: 280 Color: 1

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 6
Size: 329 Color: 12
Size: 260 Color: 16

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 11
Size: 256 Color: 6
Size: 250 Color: 13

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 4
Size: 345 Color: 13
Size: 300 Color: 4

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 17
Size: 259 Color: 14
Size: 250 Color: 5

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 3
Size: 305 Color: 12
Size: 252 Color: 17

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 9
Size: 284 Color: 19
Size: 266 Color: 15

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 7
Size: 320 Color: 16
Size: 278 Color: 12

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 9
Size: 336 Color: 11
Size: 295 Color: 1

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 18
Size: 319 Color: 3
Size: 316 Color: 18

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 10
Size: 354 Color: 5
Size: 286 Color: 2

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 14
Size: 302 Color: 10
Size: 267 Color: 17

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 2
Size: 252 Color: 16
Size: 251 Color: 7

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 3
Size: 300 Color: 15
Size: 264 Color: 15

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 5
Size: 268 Color: 19
Size: 250 Color: 16

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 4
Size: 251 Color: 0
Size: 250 Color: 7

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 2
Size: 252 Color: 3
Size: 250 Color: 6

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 10
Size: 346 Color: 3
Size: 282 Color: 15

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 3
Size: 279 Color: 4
Size: 265 Color: 15

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 11
Size: 305 Color: 17
Size: 278 Color: 7

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 6
Size: 317 Color: 16
Size: 314 Color: 3

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 287 Color: 9
Size: 255 Color: 1

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 17
Size: 277 Color: 17
Size: 277 Color: 5

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 11
Size: 329 Color: 5
Size: 278 Color: 5

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7
Size: 364 Color: 13
Size: 257 Color: 11

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 14
Size: 263 Color: 5
Size: 255 Color: 5

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8
Size: 335 Color: 2
Size: 260 Color: 8

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 10
Size: 294 Color: 18
Size: 275 Color: 9

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 324 Color: 14
Size: 251 Color: 9

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 368 Color: 10
Size: 255 Color: 12

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 15
Size: 273 Color: 3
Size: 253 Color: 3

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9
Size: 287 Color: 10
Size: 250 Color: 19

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 9
Size: 251 Color: 19
Size: 250 Color: 12

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 256 Color: 17
Size: 251 Color: 0

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 16
Size: 311 Color: 0
Size: 259 Color: 5

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 11
Size: 330 Color: 16
Size: 253 Color: 10

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 16
Size: 277 Color: 6
Size: 252 Color: 8

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 10
Size: 336 Color: 17
Size: 250 Color: 3

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 15
Size: 306 Color: 0
Size: 276 Color: 6

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 3
Size: 367 Color: 15
Size: 265 Color: 7

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 10
Size: 318 Color: 18
Size: 289 Color: 2

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 18
Size: 298 Color: 12
Size: 275 Color: 6

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 14
Size: 327 Color: 17
Size: 303 Color: 16

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 7
Size: 339 Color: 0
Size: 250 Color: 8

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 13
Size: 267 Color: 17
Size: 267 Color: 2

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 8
Size: 271 Color: 11
Size: 252 Color: 9

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 14
Size: 281 Color: 4
Size: 271 Color: 11

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 18
Size: 344 Color: 19
Size: 310 Color: 15

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 12
Size: 266 Color: 9
Size: 255 Color: 2

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 5
Size: 282 Color: 12
Size: 262 Color: 11

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 278 Color: 10
Size: 255 Color: 7

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 12
Size: 339 Color: 19
Size: 260 Color: 17

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8
Size: 295 Color: 2
Size: 265 Color: 1

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 11
Size: 289 Color: 18
Size: 276 Color: 1

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 8
Size: 351 Color: 15
Size: 278 Color: 14

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 11
Size: 268 Color: 8
Size: 261 Color: 7

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 18
Size: 341 Color: 3
Size: 260 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 14
Size: 315 Color: 3
Size: 253 Color: 5

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 12
Size: 354 Color: 11
Size: 278 Color: 17

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 13
Size: 287 Color: 11
Size: 273 Color: 14

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 8
Size: 257 Color: 17
Size: 250 Color: 8

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 10
Size: 266 Color: 2
Size: 255 Color: 18

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 10
Size: 303 Color: 0
Size: 265 Color: 1

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 313 Color: 6
Size: 298 Color: 3

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 11
Size: 260 Color: 11
Size: 251 Color: 15

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 14
Size: 292 Color: 15
Size: 267 Color: 16

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 3
Size: 352 Color: 11
Size: 271 Color: 19

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 5
Size: 278 Color: 15
Size: 270 Color: 11

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 15
Size: 334 Color: 10
Size: 272 Color: 3

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 5
Size: 277 Color: 19
Size: 257 Color: 8

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 12
Size: 277 Color: 18
Size: 250 Color: 18

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8
Size: 298 Color: 13
Size: 284 Color: 15

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8
Size: 315 Color: 10
Size: 286 Color: 17

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9
Size: 292 Color: 14
Size: 263 Color: 13

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 16
Size: 339 Color: 15
Size: 252 Color: 4

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 3
Size: 289 Color: 2
Size: 267 Color: 5

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 7
Size: 324 Color: 2
Size: 270 Color: 13

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 14
Size: 334 Color: 10
Size: 280 Color: 9

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 5
Size: 316 Color: 18
Size: 252 Color: 17

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 15
Size: 343 Color: 16
Size: 257 Color: 9

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 17
Size: 355 Color: 6
Size: 253 Color: 7

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 15
Size: 312 Color: 11
Size: 272 Color: 19

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 4
Size: 260 Color: 7
Size: 254 Color: 12

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 13
Size: 309 Color: 6
Size: 288 Color: 3

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9
Size: 291 Color: 2
Size: 250 Color: 10

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 14
Size: 372 Color: 4
Size: 253 Color: 3

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 17
Size: 345 Color: 5
Size: 257 Color: 12

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 12
Size: 287 Color: 16
Size: 264 Color: 15

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 10
Size: 295 Color: 12
Size: 275 Color: 13

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 263 Color: 4
Size: 258 Color: 11

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 3
Size: 318 Color: 14
Size: 260 Color: 5

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 0
Size: 293 Color: 14
Size: 268 Color: 5

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 308 Color: 16
Size: 255 Color: 17

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 8
Size: 275 Color: 6
Size: 253 Color: 3

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 17
Size: 323 Color: 17
Size: 268 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 337 Color: 7
Size: 271 Color: 2

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 15
Size: 332 Color: 1
Size: 263 Color: 12

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 269 Color: 15
Size: 250 Color: 8

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 12
Size: 265 Color: 1
Size: 259 Color: 1

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 0
Size: 271 Color: 19
Size: 269 Color: 17

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 5
Size: 344 Color: 15
Size: 304 Color: 3

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 16
Size: 262 Color: 14
Size: 253 Color: 8

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 10
Size: 310 Color: 10
Size: 251 Color: 3

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 14
Size: 331 Color: 12
Size: 287 Color: 12

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 12
Size: 331 Color: 0
Size: 263 Color: 18

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 342 Color: 6
Size: 300 Color: 9

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 5
Size: 327 Color: 6
Size: 311 Color: 11

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 338 Color: 16
Size: 334 Color: 16
Size: 329 Color: 19

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 12
Size: 264 Color: 13
Size: 250 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 5
Size: 322 Color: 10
Size: 289 Color: 0

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 13
Size: 356 Color: 13
Size: 250 Color: 3

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 14
Size: 327 Color: 12
Size: 316 Color: 4

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 10
Size: 319 Color: 14
Size: 255 Color: 6

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 17
Size: 306 Color: 17
Size: 254 Color: 14

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 17
Size: 330 Color: 4
Size: 289 Color: 13

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 304 Color: 16
Size: 255 Color: 13

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 19
Size: 317 Color: 17
Size: 279 Color: 10

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 310 Color: 6
Size: 283 Color: 6

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 4
Size: 331 Color: 12
Size: 324 Color: 19

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 7
Size: 303 Color: 11
Size: 251 Color: 2

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 6
Size: 280 Color: 19
Size: 261 Color: 18

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 9
Size: 320 Color: 3
Size: 286 Color: 15

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 3
Size: 341 Color: 16
Size: 303 Color: 13

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 14
Size: 317 Color: 8
Size: 310 Color: 14

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 12
Size: 327 Color: 0
Size: 258 Color: 12

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 15
Size: 370 Color: 6
Size: 251 Color: 13

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 7
Size: 296 Color: 4
Size: 255 Color: 18

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 17
Size: 270 Color: 1
Size: 251 Color: 5

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 12
Size: 329 Color: 6
Size: 328 Color: 5

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 0
Size: 332 Color: 14
Size: 296 Color: 4

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 16
Size: 331 Color: 18
Size: 286 Color: 16

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 311 Color: 6
Size: 308 Color: 18

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 281 Color: 9
Size: 255 Color: 15

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 4
Size: 278 Color: 19
Size: 273 Color: 8

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 15
Size: 286 Color: 7
Size: 262 Color: 17

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 12
Size: 297 Color: 5
Size: 275 Color: 1

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 19
Size: 308 Color: 18
Size: 252 Color: 7

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 5
Size: 340 Color: 16
Size: 280 Color: 9

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 5
Size: 255 Color: 9
Size: 253 Color: 12

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 12
Size: 302 Color: 14
Size: 263 Color: 19

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 324 Color: 5
Size: 322 Color: 19

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 5
Size: 301 Color: 1
Size: 292 Color: 7

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 16
Size: 268 Color: 12
Size: 258 Color: 15

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 18
Size: 300 Color: 18
Size: 287 Color: 9

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 16
Size: 328 Color: 15
Size: 316 Color: 6

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 10
Size: 257 Color: 5
Size: 251 Color: 12

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 14
Size: 319 Color: 18
Size: 270 Color: 6

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 308 Color: 16
Size: 293 Color: 9

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 19
Size: 320 Color: 1
Size: 300 Color: 11

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 0
Size: 348 Color: 1
Size: 252 Color: 8

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 5
Size: 319 Color: 4
Size: 257 Color: 15

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 16
Size: 332 Color: 1
Size: 287 Color: 8

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 15
Size: 336 Color: 15
Size: 264 Color: 19

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 6
Size: 298 Color: 13
Size: 270 Color: 17

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 6
Size: 278 Color: 8
Size: 266 Color: 10

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 18
Size: 347 Color: 14
Size: 298 Color: 15

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 13
Size: 331 Color: 14
Size: 310 Color: 16

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 12
Size: 337 Color: 0
Size: 303 Color: 12

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 8
Size: 280 Color: 1
Size: 277 Color: 17

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 6
Size: 254 Color: 11
Size: 250 Color: 19

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 13
Size: 297 Color: 7
Size: 256 Color: 13

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 17
Size: 267 Color: 5
Size: 260 Color: 15

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 342 Color: 15
Size: 264 Color: 1

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8
Size: 321 Color: 10
Size: 276 Color: 15

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 323 Color: 5
Size: 279 Color: 17

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 12
Size: 369 Color: 6
Size: 255 Color: 3

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 11
Size: 294 Color: 0
Size: 250 Color: 3

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 16
Size: 305 Color: 11
Size: 294 Color: 5

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9
Size: 280 Color: 7
Size: 255 Color: 4

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 349 Color: 2
Size: 280 Color: 2

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 8
Size: 353 Color: 1
Size: 258 Color: 10

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 7
Size: 271 Color: 17
Size: 270 Color: 1

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 16
Size: 342 Color: 2
Size: 260 Color: 16

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 16
Size: 327 Color: 7
Size: 293 Color: 19

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 16
Size: 338 Color: 13
Size: 320 Color: 11

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 15
Size: 329 Color: 0
Size: 314 Color: 14

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 10
Size: 269 Color: 12
Size: 265 Color: 15

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 12
Size: 293 Color: 17
Size: 279 Color: 10

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9
Size: 286 Color: 2
Size: 260 Color: 1

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 14
Size: 335 Color: 3
Size: 287 Color: 18

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 14
Size: 352 Color: 17
Size: 290 Color: 14

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 17
Size: 284 Color: 2
Size: 281 Color: 9

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 17
Size: 344 Color: 10
Size: 298 Color: 2

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 7
Size: 336 Color: 17
Size: 264 Color: 9

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 16
Size: 354 Color: 6
Size: 293 Color: 15

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 12
Size: 338 Color: 15
Size: 288 Color: 8

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 12
Size: 263 Color: 18
Size: 257 Color: 10

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 316 Color: 1
Size: 310 Color: 14

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 4
Size: 299 Color: 2
Size: 255 Color: 12

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 5
Size: 339 Color: 18
Size: 293 Color: 12

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 10
Size: 301 Color: 10
Size: 293 Color: 9

Total size: 667667
Total free space: 0

