Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 15
Size: 252 Color: 5
Size: 250 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 18
Size: 350 Color: 5
Size: 280 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 11
Size: 272 Color: 11
Size: 256 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 12
Size: 331 Color: 10
Size: 268 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 9
Size: 344 Color: 3
Size: 299 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 272 Color: 6
Size: 474 Color: 13
Size: 254 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 19
Size: 336 Color: 7
Size: 272 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 17
Size: 274 Color: 3
Size: 253 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 8
Size: 359 Color: 10
Size: 275 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 15
Size: 340 Color: 12
Size: 294 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 18
Size: 347 Color: 18
Size: 302 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 19
Size: 260 Color: 14
Size: 252 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 14
Size: 303 Color: 6
Size: 275 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 13
Size: 271 Color: 4
Size: 262 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 14
Size: 350 Color: 0
Size: 287 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 5
Size: 333 Color: 9
Size: 282 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 6
Size: 355 Color: 11
Size: 253 Color: 12

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 16
Size: 288 Color: 9
Size: 287 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 11
Size: 295 Color: 19
Size: 280 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 14
Size: 255 Color: 6
Size: 250 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 19
Size: 266 Color: 4
Size: 259 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6
Size: 356 Color: 2
Size: 288 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 12
Size: 265 Color: 19
Size: 250 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 15
Size: 260 Color: 15
Size: 259 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 7
Size: 265 Color: 5
Size: 250 Color: 18

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 6
Size: 253 Color: 3
Size: 250 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 13
Size: 253 Color: 3
Size: 251 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 17
Size: 281 Color: 4
Size: 274 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 6
Size: 268 Color: 9
Size: 252 Color: 12

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 15
Size: 285 Color: 2
Size: 271 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 2
Size: 329 Color: 16
Size: 258 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 8
Size: 262 Color: 4
Size: 253 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 13
Size: 276 Color: 15
Size: 274 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9
Size: 292 Color: 10
Size: 252 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 18
Size: 301 Color: 11
Size: 254 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 5
Size: 272 Color: 14
Size: 258 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 8
Size: 369 Color: 6
Size: 250 Color: 16

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 5
Size: 312 Color: 14
Size: 269 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 275 Color: 7
Size: 274 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 10
Size: 274 Color: 7
Size: 263 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8
Size: 338 Color: 6
Size: 255 Color: 10

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8
Size: 299 Color: 2
Size: 280 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 16
Size: 337 Color: 14
Size: 253 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 253 Color: 4
Size: 252 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 4
Size: 253 Color: 17
Size: 250 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 4
Size: 269 Color: 15
Size: 251 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 18
Size: 255 Color: 13
Size: 254 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 13
Size: 255 Color: 13
Size: 254 Color: 16

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 10
Size: 267 Color: 7
Size: 255 Color: 18

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 11
Size: 325 Color: 9
Size: 269 Color: 6

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 6
Size: 262 Color: 16
Size: 260 Color: 5

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 5
Size: 308 Color: 18
Size: 253 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 308 Color: 13
Size: 267 Color: 15

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 19
Size: 363 Color: 12
Size: 271 Color: 16

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6
Size: 362 Color: 19
Size: 275 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 19
Size: 354 Color: 8
Size: 254 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 18
Size: 257 Color: 18
Size: 250 Color: 13

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 18
Size: 357 Color: 14
Size: 282 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 3
Size: 301 Color: 7
Size: 269 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 294 Color: 11
Size: 287 Color: 12

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 306 Color: 17
Size: 252 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8
Size: 318 Color: 6
Size: 264 Color: 13

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 12
Size: 256 Color: 16
Size: 251 Color: 16

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 5
Size: 343 Color: 2
Size: 258 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 7
Size: 262 Color: 3
Size: 259 Color: 12

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9
Size: 287 Color: 2
Size: 253 Color: 9

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 5
Size: 260 Color: 19
Size: 256 Color: 15

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 6
Size: 291 Color: 15
Size: 254 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 13
Size: 317 Color: 4
Size: 250 Color: 11

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 5
Size: 331 Color: 11
Size: 305 Color: 14

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 15
Size: 312 Color: 17
Size: 307 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 354 Color: 3
Size: 271 Color: 15

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 15
Size: 330 Color: 0
Size: 255 Color: 17

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 14
Size: 351 Color: 5
Size: 254 Color: 11

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 13
Size: 282 Color: 11
Size: 273 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 2
Size: 259 Color: 17
Size: 253 Color: 13

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 19
Size: 263 Color: 3
Size: 254 Color: 12

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 19
Size: 265 Color: 8
Size: 262 Color: 7

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 14
Size: 282 Color: 12
Size: 259 Color: 14

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8
Size: 296 Color: 4
Size: 263 Color: 9

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 14
Size: 324 Color: 11
Size: 258 Color: 16

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 14
Size: 347 Color: 8
Size: 258 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 16
Size: 346 Color: 1
Size: 275 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 14
Size: 254 Color: 4
Size: 251 Color: 17

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 16
Size: 264 Color: 12
Size: 261 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 19
Size: 347 Color: 16
Size: 259 Color: 4

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 13
Size: 287 Color: 2
Size: 280 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 11
Size: 356 Color: 16
Size: 275 Color: 19

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 15
Size: 340 Color: 5
Size: 294 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 4
Size: 324 Color: 0
Size: 274 Color: 5

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 7
Size: 276 Color: 10
Size: 255 Color: 18

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 17
Size: 350 Color: 19
Size: 273 Color: 7

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 276 Color: 4
Size: 258 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 19
Size: 285 Color: 13
Size: 269 Color: 12

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 291 Color: 1
Size: 263 Color: 5

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 8
Size: 261 Color: 14
Size: 252 Color: 7

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 19
Size: 297 Color: 15
Size: 285 Color: 18

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 5
Size: 352 Color: 2
Size: 273 Color: 10

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 11
Size: 301 Color: 16
Size: 273 Color: 5

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 11
Size: 333 Color: 2
Size: 297 Color: 15

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 18
Size: 263 Color: 5
Size: 258 Color: 18

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 310 Color: 14
Size: 271 Color: 14

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 7
Size: 347 Color: 7
Size: 253 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 19
Size: 326 Color: 1
Size: 276 Color: 5

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 18
Size: 278 Color: 4
Size: 251 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 16
Size: 357 Color: 12
Size: 262 Color: 14

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 9
Size: 339 Color: 6
Size: 255 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 15
Size: 252 Color: 11
Size: 250 Color: 17

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 318 Color: 12
Size: 272 Color: 11

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 5
Size: 253 Color: 18
Size: 251 Color: 14

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 4
Size: 333 Color: 11
Size: 291 Color: 10

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 10
Size: 281 Color: 2
Size: 257 Color: 5

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 313 Color: 2
Size: 250 Color: 8

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 12
Size: 318 Color: 16
Size: 289 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7
Size: 344 Color: 11
Size: 270 Color: 16

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 11
Size: 340 Color: 13
Size: 294 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 17
Size: 314 Color: 14
Size: 314 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 4
Size: 274 Color: 5
Size: 259 Color: 13

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 352 Color: 17
Size: 258 Color: 9

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 13
Size: 346 Color: 1
Size: 250 Color: 16

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 14
Size: 298 Color: 4
Size: 281 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7
Size: 350 Color: 5
Size: 269 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 14
Size: 320 Color: 17
Size: 283 Color: 13

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 4
Size: 252 Color: 19
Size: 251 Color: 15

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7
Size: 367 Color: 3
Size: 266 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 14
Size: 333 Color: 5
Size: 273 Color: 10

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 359 Color: 9
Size: 264 Color: 6

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 18
Size: 329 Color: 17
Size: 251 Color: 16

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 293 Color: 18
Size: 293 Color: 12

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 6
Size: 290 Color: 15
Size: 254 Color: 15

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 15
Size: 271 Color: 18
Size: 264 Color: 7

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 10
Size: 259 Color: 9
Size: 254 Color: 8

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 10
Size: 284 Color: 10
Size: 271 Color: 3

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 3
Size: 257 Color: 14
Size: 253 Color: 11

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 13
Size: 258 Color: 1
Size: 250 Color: 10

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 7
Size: 299 Color: 3
Size: 267 Color: 7

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 10
Size: 290 Color: 12
Size: 284 Color: 18

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 5
Size: 307 Color: 17
Size: 275 Color: 14

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 350 Color: 3
Size: 258 Color: 3

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 19
Size: 323 Color: 17
Size: 254 Color: 8

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 10
Size: 349 Color: 14
Size: 258 Color: 15

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 8
Size: 292 Color: 7
Size: 291 Color: 2

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 277 Color: 14
Size: 259 Color: 2

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 14
Size: 307 Color: 17
Size: 252 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 19
Size: 252 Color: 13
Size: 250 Color: 5

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 3
Size: 298 Color: 12
Size: 285 Color: 6

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 19
Size: 342 Color: 17
Size: 282 Color: 7

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 18
Size: 342 Color: 13
Size: 296 Color: 13

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 16
Size: 340 Color: 15
Size: 295 Color: 11

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 369 Color: 1
Size: 259 Color: 10

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 4
Size: 356 Color: 10
Size: 271 Color: 19

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 10
Size: 355 Color: 18
Size: 270 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 13
Size: 334 Color: 15
Size: 283 Color: 9

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 13
Size: 343 Color: 3
Size: 252 Color: 11

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 18
Size: 321 Color: 13
Size: 267 Color: 13

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 15
Size: 320 Color: 3
Size: 298 Color: 14

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 4
Size: 315 Color: 10
Size: 251 Color: 11

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 313 Color: 3
Size: 250 Color: 11

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 12
Size: 298 Color: 5
Size: 259 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 287 Color: 18
Size: 267 Color: 9

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 6
Size: 298 Color: 18
Size: 255 Color: 12

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 4
Size: 274 Color: 19
Size: 270 Color: 12

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 12
Size: 274 Color: 8
Size: 269 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 267 Color: 15
Size: 255 Color: 11
Size: 478 Color: 17

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 12
Size: 261 Color: 11
Size: 259 Color: 9

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 12
Size: 259 Color: 7
Size: 256 Color: 9

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 6
Size: 256 Color: 6
Size: 254 Color: 11

Total size: 167000
Total free space: 0

