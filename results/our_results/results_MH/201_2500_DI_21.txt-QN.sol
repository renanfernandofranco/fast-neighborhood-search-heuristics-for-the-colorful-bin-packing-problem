Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1442 Color: 148
Size: 484 Color: 98
Size: 358 Color: 87
Size: 172 Color: 61

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 172
Size: 526 Color: 104
Size: 88 Color: 32

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1571 Color: 153
Size: 637 Color: 114
Size: 100 Color: 40
Size: 96 Color: 38
Size: 52 Color: 15

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 1598 Color: 155
Size: 718 Color: 119
Size: 92 Color: 35
Size: 40 Color: 8
Size: 8 Color: 1

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 2062 Color: 188
Size: 204 Color: 68
Size: 126 Color: 50
Size: 64 Color: 21

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 197
Size: 146 Color: 55
Size: 140 Color: 54

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 199
Size: 244 Color: 74
Size: 32 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2046 Color: 186
Size: 282 Color: 81
Size: 128 Color: 51

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 190
Size: 298 Color: 82
Size: 56 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1445 Color: 149
Size: 843 Color: 125
Size: 168 Color: 58

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 179
Size: 474 Color: 97
Size: 44 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2130 Color: 192
Size: 262 Color: 76
Size: 64 Color: 22

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 176
Size: 406 Color: 92
Size: 152 Color: 56

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 144
Size: 884 Color: 129
Size: 176 Color: 62

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 200
Size: 226 Color: 69
Size: 28 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 181
Size: 382 Color: 89
Size: 104 Color: 42

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 154
Size: 777 Color: 122
Size: 88 Color: 34

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1834 Color: 170
Size: 522 Color: 103
Size: 100 Color: 39

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1249 Color: 143
Size: 1007 Color: 132
Size: 200 Color: 64

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 138
Size: 1045 Color: 137
Size: 182 Color: 63

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 196
Size: 244 Color: 73
Size: 48 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 166
Size: 434 Color: 94
Size: 272 Color: 79

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 156
Size: 772 Color: 121
Size: 40 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1402 Color: 145
Size: 1022 Color: 136
Size: 32 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 158
Size: 669 Color: 116
Size: 132 Color: 52

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 140
Size: 1021 Color: 135
Size: 202 Color: 67

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2138 Color: 193
Size: 266 Color: 77
Size: 52 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 162
Size: 621 Color: 113
Size: 122 Color: 49

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 169
Size: 585 Color: 108
Size: 50 Color: 14

Bin 30: 0 of cap free
Amount of items: 4
Items: 
Size: 1693 Color: 161
Size: 739 Color: 120
Size: 16 Color: 3
Size: 8 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 152
Size: 840 Color: 124
Size: 80 Color: 29

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 142
Size: 1018 Color: 133
Size: 200 Color: 65

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 139
Size: 954 Color: 130
Size: 272 Color: 78

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 168
Size: 576 Color: 107
Size: 68 Color: 23

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 165
Size: 604 Color: 110
Size: 120 Color: 46

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2156 Color: 195
Size: 252 Color: 75
Size: 48 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 984 Color: 131
Size: 882 Color: 128
Size: 590 Color: 109

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 182
Size: 404 Color: 91
Size: 72 Color: 25

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 164
Size: 611 Color: 111
Size: 120 Color: 47

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1688 Color: 160
Size: 684 Color: 118
Size: 84 Color: 31

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1919 Color: 177
Size: 449 Color: 96
Size: 88 Color: 33

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 147
Size: 861 Color: 126
Size: 170 Color: 59

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 2060 Color: 187
Size: 332 Color: 85
Size: 64 Color: 20

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 201
Size: 234 Color: 70
Size: 12 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 151
Size: 540 Color: 105
Size: 384 Color: 90

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 150
Size: 806 Color: 123
Size: 160 Color: 57

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1841 Color: 171
Size: 513 Color: 101
Size: 102 Color: 41

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2030 Color: 185
Size: 330 Color: 84
Size: 96 Color: 37

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 198
Size: 236 Color: 71
Size: 48 Color: 11

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 175
Size: 514 Color: 102
Size: 52 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 191
Size: 274 Color: 80
Size: 60 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 174
Size: 504 Color: 100
Size: 76 Color: 27

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 141
Size: 1020 Color: 134
Size: 200 Color: 66

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 157
Size: 678 Color: 117
Size: 132 Color: 53

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 178
Size: 444 Color: 95
Size: 80 Color: 28

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 146
Size: 874 Color: 127
Size: 172 Color: 60

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 183
Size: 342 Color: 86
Size: 116 Color: 44

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 184
Size: 364 Color: 88
Size: 72 Color: 26

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 167
Size: 570 Color: 106
Size: 112 Color: 43

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 159
Size: 654 Color: 115
Size: 116 Color: 45

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 189
Size: 308 Color: 83
Size: 48 Color: 12

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 194
Size: 242 Color: 72
Size: 68 Color: 24

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 163
Size: 613 Color: 112
Size: 122 Color: 48

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1869 Color: 173
Size: 491 Color: 99
Size: 96 Color: 36

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 180
Size: 413 Color: 93
Size: 82 Color: 30

Total size: 159640
Total free space: 0

