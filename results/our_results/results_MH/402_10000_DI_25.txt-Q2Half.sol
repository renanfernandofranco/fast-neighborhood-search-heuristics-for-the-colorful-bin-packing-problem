Capicity Bin: 8120
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 3568 Color: 1
Size: 1544 Color: 0
Size: 1120 Color: 1
Size: 1000 Color: 1
Size: 640 Color: 1
Size: 128 Color: 0
Size: 80 Color: 0
Size: 40 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 1
Size: 1134 Color: 1
Size: 224 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 1
Size: 988 Color: 1
Size: 192 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 1
Size: 1426 Color: 1
Size: 284 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7141 Color: 1
Size: 887 Color: 1
Size: 92 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 1
Size: 2420 Color: 1
Size: 480 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5073 Color: 1
Size: 2677 Color: 1
Size: 370 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5692 Color: 1
Size: 2028 Color: 1
Size: 400 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 1
Size: 1292 Color: 1
Size: 256 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 1
Size: 1490 Color: 1
Size: 296 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5862 Color: 1
Size: 1882 Color: 1
Size: 376 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5822 Color: 1
Size: 1918 Color: 1
Size: 380 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6045 Color: 1
Size: 1731 Color: 1
Size: 344 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 1
Size: 1381 Color: 1
Size: 274 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6994 Color: 1
Size: 942 Color: 1
Size: 184 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7265 Color: 1
Size: 713 Color: 1
Size: 142 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 1
Size: 1932 Color: 1
Size: 384 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4615 Color: 1
Size: 2921 Color: 1
Size: 584 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7049 Color: 1
Size: 893 Color: 1
Size: 178 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4062 Color: 1
Size: 3382 Color: 1
Size: 676 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 1
Size: 964 Color: 1
Size: 184 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5102 Color: 1
Size: 2518 Color: 1
Size: 500 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7164 Color: 1
Size: 804 Color: 1
Size: 152 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 1
Size: 1364 Color: 1
Size: 272 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 1
Size: 1474 Color: 1
Size: 292 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 1
Size: 2894 Color: 1
Size: 576 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7291 Color: 1
Size: 691 Color: 1
Size: 138 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 1
Size: 1232 Color: 1
Size: 384 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7214 Color: 1
Size: 758 Color: 1
Size: 148 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7251 Color: 1
Size: 725 Color: 1
Size: 144 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 1
Size: 1676 Color: 1
Size: 328 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7041 Color: 1
Size: 901 Color: 1
Size: 178 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7306 Color: 1
Size: 682 Color: 1
Size: 132 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6265 Color: 1
Size: 1547 Color: 1
Size: 308 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 1
Size: 1135 Color: 1
Size: 226 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6458 Color: 1
Size: 1386 Color: 1
Size: 276 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 1
Size: 2204 Color: 1
Size: 440 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5471 Color: 1
Size: 2209 Color: 1
Size: 440 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 1
Size: 1312 Color: 1
Size: 128 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6975 Color: 1
Size: 955 Color: 1
Size: 190 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6936 Color: 1
Size: 992 Color: 1
Size: 192 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 1
Size: 1034 Color: 1
Size: 204 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6913 Color: 1
Size: 1007 Color: 1
Size: 200 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 1
Size: 1894 Color: 1
Size: 136 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6987 Color: 1
Size: 945 Color: 1
Size: 188 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 1
Size: 1476 Color: 1
Size: 288 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6114 Color: 1
Size: 1674 Color: 1
Size: 332 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4716 Color: 1
Size: 2844 Color: 1
Size: 560 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 1
Size: 1316 Color: 1
Size: 136 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 1
Size: 922 Color: 1
Size: 184 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 1
Size: 1045 Color: 1
Size: 208 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 1
Size: 764 Color: 1
Size: 152 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5142 Color: 1
Size: 2482 Color: 1
Size: 496 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 7243 Color: 1
Size: 731 Color: 1
Size: 146 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 7134 Color: 1
Size: 822 Color: 1
Size: 164 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6051 Color: 1
Size: 1725 Color: 1
Size: 344 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 1
Size: 1154 Color: 1
Size: 228 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 1
Size: 706 Color: 1
Size: 140 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 1
Size: 812 Color: 1
Size: 160 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 1
Size: 796 Color: 1
Size: 80 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5514 Color: 1
Size: 2174 Color: 1
Size: 432 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5908 Color: 1
Size: 1844 Color: 1
Size: 368 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5787 Color: 1
Size: 1945 Color: 1
Size: 388 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6881 Color: 1
Size: 1033 Color: 1
Size: 206 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 1
Size: 1434 Color: 1
Size: 284 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4613 Color: 1
Size: 2923 Color: 1
Size: 584 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 1
Size: 1022 Color: 1
Size: 200 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6450 Color: 1
Size: 1394 Color: 1
Size: 276 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6873 Color: 1
Size: 1041 Color: 1
Size: 206 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6755 Color: 1
Size: 1139 Color: 1
Size: 226 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6740 Color: 1
Size: 1156 Color: 1
Size: 224 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6874 Color: 1
Size: 1042 Color: 1
Size: 204 Color: 0

Bin 73: 0 of cap free
Amount of items: 5
Items: 
Size: 4016 Color: 1
Size: 3008 Color: 1
Size: 512 Color: 1
Size: 448 Color: 0
Size: 136 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7203 Color: 1
Size: 765 Color: 1
Size: 152 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 1
Size: 1243 Color: 1
Size: 248 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4070 Color: 1
Size: 3378 Color: 1
Size: 672 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5988 Color: 1
Size: 1780 Color: 1
Size: 352 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 1
Size: 900 Color: 1
Size: 176 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 1
Size: 802 Color: 1
Size: 128 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 1
Size: 1249 Color: 1
Size: 248 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 1
Size: 1270 Color: 1
Size: 208 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 1
Size: 1606 Color: 1
Size: 152 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7225 Color: 1
Size: 747 Color: 1
Size: 148 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5081 Color: 1
Size: 2533 Color: 1
Size: 506 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7305 Color: 1
Size: 681 Color: 1
Size: 134 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 4626 Color: 1
Size: 2914 Color: 1
Size: 580 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4061 Color: 1
Size: 3383 Color: 1
Size: 676 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 7053 Color: 1
Size: 891 Color: 1
Size: 176 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4324 Color: 1
Size: 3164 Color: 1
Size: 632 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 7074 Color: 1
Size: 874 Color: 1
Size: 172 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 7105 Color: 1
Size: 1001 Color: 1
Size: 14 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6746 Color: 1
Size: 1146 Color: 1
Size: 228 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 7169 Color: 1
Size: 793 Color: 1
Size: 158 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 1
Size: 2364 Color: 1
Size: 472 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 1
Size: 692 Color: 1
Size: 136 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 1
Size: 724 Color: 1
Size: 144 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 1
Size: 2612 Color: 1
Size: 520 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6332 Color: 1
Size: 1692 Color: 1
Size: 96 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 7064 Color: 1
Size: 880 Color: 1
Size: 176 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 1
Size: 1564 Color: 1
Size: 312 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 1
Size: 2988 Color: 1
Size: 592 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 4634 Color: 1
Size: 2906 Color: 1
Size: 580 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 1
Size: 1124 Color: 1
Size: 216 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6767 Color: 1
Size: 1129 Color: 1
Size: 224 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 6897 Color: 1
Size: 1021 Color: 1
Size: 202 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5463 Color: 1
Size: 2215 Color: 1
Size: 442 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 1
Size: 790 Color: 1
Size: 156 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6138 Color: 1
Size: 1654 Color: 1
Size: 328 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 5490 Color: 1
Size: 2194 Color: 1
Size: 436 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 7066 Color: 1
Size: 882 Color: 1
Size: 172 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 7068 Color: 1
Size: 884 Color: 1
Size: 168 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 1
Size: 722 Color: 1
Size: 144 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 7230 Color: 1
Size: 742 Color: 1
Size: 148 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5498 Color: 1
Size: 2186 Color: 1
Size: 436 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 1
Size: 782 Color: 1
Size: 156 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6763 Color: 1
Size: 1131 Color: 1
Size: 226 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 1
Size: 1218 Color: 1
Size: 240 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6442 Color: 1
Size: 1402 Color: 1
Size: 276 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 7103 Color: 1
Size: 849 Color: 1
Size: 168 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 1
Size: 1531 Color: 1
Size: 306 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 1
Size: 1092 Color: 1
Size: 216 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6271 Color: 1
Size: 1713 Color: 1
Size: 136 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6291 Color: 1
Size: 1525 Color: 1
Size: 304 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 1
Size: 1366 Color: 1
Size: 272 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 1
Size: 862 Color: 1
Size: 168 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5781 Color: 1
Size: 1951 Color: 1
Size: 388 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6481 Color: 1
Size: 1367 Color: 1
Size: 272 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6473 Color: 1
Size: 1373 Color: 1
Size: 274 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 4063 Color: 1
Size: 3381 Color: 1
Size: 676 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 1
Size: 3380 Color: 1
Size: 672 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 5093 Color: 1
Size: 2523 Color: 1
Size: 504 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7145 Color: 1
Size: 813 Color: 1
Size: 162 Color: 0

Total size: 1071840
Total free space: 0

