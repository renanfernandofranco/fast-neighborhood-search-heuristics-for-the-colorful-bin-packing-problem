Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 54
Size: 273 Color: 17
Size: 261 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 50
Size: 315 Color: 30
Size: 251 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 51
Size: 290 Color: 25
Size: 271 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 43
Size: 370 Color: 41
Size: 253 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 39
Size: 361 Color: 38
Size: 275 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 46
Size: 337 Color: 34
Size: 278 Color: 22

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 48
Size: 319 Color: 31
Size: 261 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 37
Size: 352 Color: 36
Size: 290 Color: 24

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 42
Size: 331 Color: 33
Size: 296 Color: 28

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 52
Size: 304 Color: 29
Size: 253 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 49
Size: 295 Color: 27
Size: 281 Color: 23

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 53
Size: 291 Color: 26
Size: 256 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 56
Size: 270 Color: 15
Size: 251 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 47
Size: 324 Color: 32
Size: 277 Color: 21

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 45
Size: 366 Color: 40
Size: 254 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 55
Size: 275 Color: 19
Size: 254 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 59
Size: 255 Color: 9
Size: 250 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 57
Size: 256 Color: 11
Size: 252 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 58
Size: 255 Color: 10
Size: 252 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 44
Size: 347 Color: 35
Size: 276 Color: 20

Total size: 20000
Total free space: 0

