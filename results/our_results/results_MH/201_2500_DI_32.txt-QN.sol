Capicity Bin: 2356
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1880 Color: 176
Size: 188 Color: 65
Size: 144 Color: 56
Size: 112 Color: 45
Size: 32 Color: 5

Bin 2: 0 of cap free
Amount of items: 8
Items: 
Size: 1164 Color: 138
Size: 360 Color: 95
Size: 320 Color: 91
Size: 292 Color: 87
Size: 136 Color: 53
Size: 36 Color: 9
Size: 32 Color: 7
Size: 16 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1957 Color: 181
Size: 333 Color: 92
Size: 66 Color: 28

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 171
Size: 454 Color: 101
Size: 88 Color: 36

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2075 Color: 192
Size: 235 Color: 77
Size: 46 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 158
Size: 649 Color: 115
Size: 128 Color: 50

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 183
Size: 301 Color: 89
Size: 58 Color: 24

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 169
Size: 459 Color: 103
Size: 90 Color: 38

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 199
Size: 241 Color: 79
Size: 12 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2080 Color: 194
Size: 236 Color: 78
Size: 40 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 153
Size: 742 Color: 121
Size: 144 Color: 55

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 164
Size: 547 Color: 110
Size: 108 Color: 43

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 188
Size: 262 Color: 83
Size: 52 Color: 20

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1281 Color: 146
Size: 897 Color: 128
Size: 178 Color: 62

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 170
Size: 455 Color: 102
Size: 90 Color: 39

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1179 Color: 139
Size: 981 Color: 133
Size: 196 Color: 69

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1032 Color: 136
Size: 876 Color: 127
Size: 448 Color: 100

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 166
Size: 495 Color: 107
Size: 98 Color: 41

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 147
Size: 951 Color: 131
Size: 88 Color: 37

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 173
Size: 429 Color: 99
Size: 84 Color: 35

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 186
Size: 283 Color: 85
Size: 56 Color: 23

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 144
Size: 933 Color: 130
Size: 186 Color: 64

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1413 Color: 152
Size: 787 Color: 122
Size: 156 Color: 57

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1873 Color: 174
Size: 403 Color: 98
Size: 80 Color: 34

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2047 Color: 189
Size: 259 Color: 82
Size: 50 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 156
Size: 679 Color: 118
Size: 134 Color: 51

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 140
Size: 981 Color: 134
Size: 194 Color: 68

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1878 Color: 175
Size: 474 Color: 104
Size: 4 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 193
Size: 231 Color: 76
Size: 46 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2099 Color: 197
Size: 215 Color: 73
Size: 42 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 160
Size: 666 Color: 116
Size: 32 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1518 Color: 154
Size: 702 Color: 120
Size: 136 Color: 52

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 184
Size: 297 Color: 88
Size: 58 Color: 25

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1366 Color: 149
Size: 826 Color: 125
Size: 164 Color: 60

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1397 Color: 151
Size: 801 Color: 123
Size: 158 Color: 58

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2119 Color: 201
Size: 199 Color: 70
Size: 38 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 162
Size: 613 Color: 113
Size: 62 Color: 27

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1899 Color: 178
Size: 381 Color: 96
Size: 76 Color: 32

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 163
Size: 554 Color: 111
Size: 108 Color: 44

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 161
Size: 565 Color: 112
Size: 112 Color: 46

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 155
Size: 691 Color: 119
Size: 138 Color: 54

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1827 Color: 172
Size: 499 Color: 108
Size: 30 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1182 Color: 141
Size: 982 Color: 135
Size: 192 Color: 66

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2071 Color: 191
Size: 255 Color: 81
Size: 30 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 177
Size: 390 Color: 97
Size: 76 Color: 31

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 2070 Color: 190
Size: 242 Color: 80
Size: 44 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 196
Size: 226 Color: 74
Size: 44 Color: 14

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 198
Size: 214 Color: 72
Size: 40 Color: 12

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2010 Color: 185
Size: 290 Color: 86
Size: 56 Color: 22

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 200
Size: 202 Color: 71
Size: 36 Color: 8

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 148
Size: 858 Color: 126
Size: 168 Color: 61

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2031 Color: 187
Size: 271 Color: 84
Size: 54 Color: 21

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 157
Size: 667 Color: 117
Size: 122 Color: 48

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 179
Size: 350 Color: 94
Size: 68 Color: 29

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1189 Color: 143
Size: 1053 Color: 137
Size: 114 Color: 47

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 182
Size: 318 Color: 90
Size: 60 Color: 26

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 165
Size: 514 Color: 109
Size: 100 Color: 42

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 180
Size: 341 Color: 93
Size: 68 Color: 30

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 145
Size: 930 Color: 129
Size: 184 Color: 63

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 159
Size: 638 Color: 114
Size: 124 Color: 49

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1377 Color: 150
Size: 817 Color: 124
Size: 162 Color: 59

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 168
Size: 475 Color: 105
Size: 78 Color: 33

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1185 Color: 142
Size: 977 Color: 132
Size: 194 Color: 67

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2083 Color: 195
Size: 229 Color: 75
Size: 44 Color: 16

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 167
Size: 482 Color: 106
Size: 92 Color: 40

Total size: 153140
Total free space: 0

