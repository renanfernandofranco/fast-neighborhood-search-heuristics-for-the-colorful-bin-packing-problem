Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3336
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 362393 Color: 3
Size: 350344 Color: 18
Size: 287264 Color: 12

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 387144 Color: 9
Size: 362555 Color: 12
Size: 250302 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 379238 Color: 11
Size: 355875 Color: 5
Size: 264888 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 455622 Color: 11
Size: 289121 Color: 4
Size: 255258 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 419368 Color: 10
Size: 326216 Color: 6
Size: 254417 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 456220 Color: 17
Size: 281355 Color: 19
Size: 262426 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 400437 Color: 1
Size: 322503 Color: 19
Size: 277061 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 475328 Color: 9
Size: 274094 Color: 9
Size: 250579 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 354004 Color: 19
Size: 349299 Color: 0
Size: 296698 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 451384 Color: 1
Size: 294178 Color: 14
Size: 254439 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 391044 Color: 16
Size: 346058 Color: 11
Size: 262899 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 395473 Color: 3
Size: 314863 Color: 19
Size: 289665 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 463104 Color: 8
Size: 284218 Color: 0
Size: 252679 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 430682 Color: 5
Size: 317450 Color: 0
Size: 251869 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 490179 Color: 17
Size: 257178 Color: 0
Size: 252644 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 402786 Color: 12
Size: 320045 Color: 2
Size: 277170 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 473184 Color: 16
Size: 268140 Color: 19
Size: 258677 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 466146 Color: 4
Size: 282127 Color: 2
Size: 251728 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 421692 Color: 13
Size: 319681 Color: 17
Size: 258628 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 345045 Color: 17
Size: 333001 Color: 0
Size: 321955 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 432193 Color: 2
Size: 296404 Color: 5
Size: 271404 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 376921 Color: 6
Size: 356760 Color: 5
Size: 266320 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 428640 Color: 8
Size: 292623 Color: 15
Size: 278738 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 457346 Color: 2
Size: 281500 Color: 13
Size: 261155 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 469288 Color: 1
Size: 266403 Color: 1
Size: 264310 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 427548 Color: 13
Size: 313455 Color: 7
Size: 258998 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 418509 Color: 11
Size: 296350 Color: 17
Size: 285142 Color: 19

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 362800 Color: 7
Size: 319911 Color: 7
Size: 317290 Color: 14

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 431953 Color: 18
Size: 296359 Color: 10
Size: 271689 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 443695 Color: 7
Size: 291128 Color: 0
Size: 265178 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 346552 Color: 7
Size: 341629 Color: 4
Size: 311820 Color: 16

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 450573 Color: 13
Size: 295840 Color: 6
Size: 253588 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 425259 Color: 11
Size: 315828 Color: 7
Size: 258914 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 348877 Color: 2
Size: 344918 Color: 14
Size: 306206 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 450747 Color: 9
Size: 279924 Color: 6
Size: 269330 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 396497 Color: 14
Size: 340101 Color: 3
Size: 263403 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 496988 Color: 13
Size: 252256 Color: 4
Size: 250757 Color: 11

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 405989 Color: 17
Size: 328018 Color: 11
Size: 265994 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 477001 Color: 16
Size: 270712 Color: 12
Size: 252288 Color: 12

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 388776 Color: 0
Size: 360238 Color: 9
Size: 250987 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 431393 Color: 6
Size: 298528 Color: 5
Size: 270080 Color: 10

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 398126 Color: 17
Size: 314387 Color: 13
Size: 287488 Color: 15

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 368758 Color: 2
Size: 342864 Color: 8
Size: 288379 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 475703 Color: 9
Size: 270640 Color: 3
Size: 253658 Color: 12

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 361762 Color: 16
Size: 338210 Color: 8
Size: 300029 Color: 18

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 448667 Color: 16
Size: 292564 Color: 11
Size: 258770 Color: 13

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 432324 Color: 14
Size: 299767 Color: 5
Size: 267910 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 382653 Color: 7
Size: 360769 Color: 12
Size: 256579 Color: 12

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 445856 Color: 16
Size: 282461 Color: 17
Size: 271684 Color: 13

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 388664 Color: 16
Size: 307315 Color: 11
Size: 304022 Color: 8

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 396731 Color: 17
Size: 325625 Color: 10
Size: 277645 Color: 9

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 420059 Color: 9
Size: 293862 Color: 6
Size: 286080 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 418903 Color: 12
Size: 305467 Color: 7
Size: 275631 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 393514 Color: 11
Size: 343932 Color: 8
Size: 262555 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 414829 Color: 4
Size: 297820 Color: 11
Size: 287352 Color: 19

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 485265 Color: 19
Size: 260249 Color: 9
Size: 254487 Color: 13

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 429362 Color: 12
Size: 318243 Color: 17
Size: 252396 Color: 10

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 380818 Color: 7
Size: 349121 Color: 5
Size: 270062 Color: 16

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 403187 Color: 6
Size: 331761 Color: 13
Size: 265053 Color: 12

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 393697 Color: 2
Size: 354804 Color: 0
Size: 251500 Color: 5

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 446987 Color: 4
Size: 283459 Color: 12
Size: 269555 Color: 7

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 473181 Color: 7
Size: 275548 Color: 13
Size: 251272 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 389449 Color: 12
Size: 315126 Color: 18
Size: 295426 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 427969 Color: 13
Size: 309799 Color: 3
Size: 262233 Color: 14

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 418977 Color: 12
Size: 297009 Color: 4
Size: 284015 Color: 5

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 412905 Color: 12
Size: 324675 Color: 7
Size: 262421 Color: 8

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 432972 Color: 9
Size: 287016 Color: 2
Size: 280013 Color: 15

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 488296 Color: 3
Size: 260999 Color: 1
Size: 250706 Color: 10

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 402594 Color: 9
Size: 343211 Color: 3
Size: 254196 Color: 17

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 355800 Color: 14
Size: 345634 Color: 2
Size: 298567 Color: 7

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 402173 Color: 12
Size: 330862 Color: 3
Size: 266966 Color: 11

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 387953 Color: 14
Size: 310898 Color: 14
Size: 301150 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 347005 Color: 7
Size: 337102 Color: 6
Size: 315894 Color: 9

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 364858 Color: 17
Size: 361950 Color: 12
Size: 273193 Color: 11

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 375897 Color: 16
Size: 347922 Color: 5
Size: 276182 Color: 5

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 369600 Color: 6
Size: 348292 Color: 6
Size: 282109 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 408047 Color: 7
Size: 319396 Color: 1
Size: 272558 Color: 5

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 425338 Color: 3
Size: 298814 Color: 13
Size: 275849 Color: 16

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 488678 Color: 17
Size: 259622 Color: 6
Size: 251701 Color: 9

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 468635 Color: 14
Size: 272724 Color: 8
Size: 258642 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 381784 Color: 2
Size: 342508 Color: 4
Size: 275709 Color: 18

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 486642 Color: 14
Size: 257042 Color: 1
Size: 256317 Color: 12

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 455751 Color: 4
Size: 273456 Color: 9
Size: 270794 Color: 11

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 442726 Color: 7
Size: 300774 Color: 12
Size: 256501 Color: 13

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 477288 Color: 13
Size: 264883 Color: 5
Size: 257830 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 400195 Color: 5
Size: 316533 Color: 19
Size: 283273 Color: 12

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 384071 Color: 2
Size: 308662 Color: 3
Size: 307268 Color: 16

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 429198 Color: 17
Size: 288246 Color: 11
Size: 282557 Color: 8

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 404394 Color: 14
Size: 341054 Color: 11
Size: 254553 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 370036 Color: 19
Size: 366009 Color: 13
Size: 263956 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 366576 Color: 3
Size: 316809 Color: 15
Size: 316616 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 457135 Color: 15
Size: 277943 Color: 16
Size: 264923 Color: 13

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 435130 Color: 1
Size: 296363 Color: 16
Size: 268508 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 382956 Color: 11
Size: 326940 Color: 15
Size: 290105 Color: 7

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 398354 Color: 6
Size: 304515 Color: 17
Size: 297132 Color: 16

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 382200 Color: 14
Size: 360782 Color: 10
Size: 257019 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 426033 Color: 16
Size: 292822 Color: 2
Size: 281146 Color: 13

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 495906 Color: 9
Size: 252714 Color: 13
Size: 251381 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 425811 Color: 10
Size: 320368 Color: 5
Size: 253822 Color: 5

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 490712 Color: 6
Size: 258401 Color: 8
Size: 250888 Color: 18

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 477047 Color: 3
Size: 266116 Color: 11
Size: 256838 Color: 5

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 438318 Color: 14
Size: 298726 Color: 0
Size: 262957 Color: 16

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 358306 Color: 3
Size: 330653 Color: 3
Size: 311042 Color: 6

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 446571 Color: 9
Size: 284985 Color: 6
Size: 268445 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 479210 Color: 15
Size: 265381 Color: 18
Size: 255410 Color: 9

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 399850 Color: 7
Size: 312224 Color: 15
Size: 287927 Color: 17

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 437092 Color: 4
Size: 312265 Color: 0
Size: 250644 Color: 4

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 486108 Color: 6
Size: 262761 Color: 2
Size: 251132 Color: 14

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 381792 Color: 5
Size: 337895 Color: 19
Size: 280314 Color: 5

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 461042 Color: 15
Size: 274593 Color: 12
Size: 264366 Color: 7

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 482888 Color: 17
Size: 260672 Color: 6
Size: 256441 Color: 16

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 400215 Color: 12
Size: 349201 Color: 2
Size: 250585 Color: 17

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 457360 Color: 18
Size: 282886 Color: 1
Size: 259755 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 412071 Color: 0
Size: 326192 Color: 7
Size: 261738 Color: 8

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 441191 Color: 18
Size: 295870 Color: 4
Size: 262940 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 458046 Color: 16
Size: 290172 Color: 6
Size: 251783 Color: 6

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 395949 Color: 19
Size: 335124 Color: 8
Size: 268928 Color: 17

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 420839 Color: 19
Size: 297953 Color: 1
Size: 281209 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 452106 Color: 1
Size: 277663 Color: 3
Size: 270232 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 387661 Color: 4
Size: 360851 Color: 4
Size: 251489 Color: 6

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 404445 Color: 14
Size: 322650 Color: 19
Size: 272906 Color: 10

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 417527 Color: 7
Size: 316960 Color: 2
Size: 265514 Color: 13

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 355475 Color: 7
Size: 354581 Color: 11
Size: 289945 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 393420 Color: 14
Size: 344835 Color: 12
Size: 261746 Color: 5

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 495808 Color: 4
Size: 252149 Color: 17
Size: 252044 Color: 2

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 395835 Color: 2
Size: 347710 Color: 16
Size: 256456 Color: 15

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 448515 Color: 2
Size: 287092 Color: 17
Size: 264394 Color: 5

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 379185 Color: 10
Size: 353655 Color: 7
Size: 267161 Color: 16

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 476504 Color: 10
Size: 270834 Color: 13
Size: 252663 Color: 18

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 345495 Color: 11
Size: 330081 Color: 1
Size: 324425 Color: 3

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 489955 Color: 7
Size: 256179 Color: 13
Size: 253867 Color: 18

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 370005 Color: 14
Size: 362413 Color: 4
Size: 267583 Color: 3

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 411823 Color: 6
Size: 336446 Color: 18
Size: 251732 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 377519 Color: 19
Size: 351676 Color: 16
Size: 270806 Color: 2

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 433337 Color: 7
Size: 299981 Color: 10
Size: 266683 Color: 9

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 487911 Color: 5
Size: 256396 Color: 4
Size: 255694 Color: 16

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 375392 Color: 10
Size: 359137 Color: 14
Size: 265472 Color: 5

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 468141 Color: 6
Size: 275957 Color: 9
Size: 255903 Color: 17

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 489575 Color: 6
Size: 258038 Color: 13
Size: 252388 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 411442 Color: 10
Size: 294416 Color: 14
Size: 294143 Color: 12

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 405777 Color: 9
Size: 343242 Color: 1
Size: 250982 Color: 13

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 365216 Color: 13
Size: 356495 Color: 3
Size: 278290 Color: 4

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 438534 Color: 13
Size: 301751 Color: 14
Size: 259716 Color: 9

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 466025 Color: 6
Size: 273706 Color: 6
Size: 260270 Color: 18

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 405997 Color: 5
Size: 306491 Color: 19
Size: 287513 Color: 11

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 439452 Color: 6
Size: 287139 Color: 19
Size: 273410 Color: 18

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 369457 Color: 0
Size: 338890 Color: 1
Size: 291654 Color: 9

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 377839 Color: 16
Size: 368669 Color: 11
Size: 253493 Color: 4

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 400540 Color: 7
Size: 330403 Color: 15
Size: 269058 Color: 15

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 483546 Color: 4
Size: 259914 Color: 11
Size: 256541 Color: 6

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 483106 Color: 19
Size: 259090 Color: 11
Size: 257805 Color: 19

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 379090 Color: 3
Size: 334655 Color: 6
Size: 286256 Color: 10

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 397935 Color: 17
Size: 333379 Color: 7
Size: 268687 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 426647 Color: 9
Size: 287596 Color: 17
Size: 285758 Color: 17

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 464665 Color: 19
Size: 274242 Color: 0
Size: 261094 Color: 12

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 454463 Color: 10
Size: 272859 Color: 17
Size: 272679 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 375660 Color: 4
Size: 348008 Color: 7
Size: 276333 Color: 13

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 457326 Color: 0
Size: 272575 Color: 8
Size: 270100 Color: 19

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 474356 Color: 12
Size: 268313 Color: 12
Size: 257332 Color: 15

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 376023 Color: 2
Size: 347715 Color: 19
Size: 276263 Color: 5

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 354871 Color: 11
Size: 346139 Color: 6
Size: 298991 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 478508 Color: 2
Size: 263321 Color: 8
Size: 258172 Color: 17

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 424283 Color: 18
Size: 323128 Color: 6
Size: 252590 Color: 13

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 446167 Color: 8
Size: 281944 Color: 19
Size: 271890 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 479967 Color: 4
Size: 266395 Color: 14
Size: 253639 Color: 2

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 393010 Color: 15
Size: 344164 Color: 13
Size: 262827 Color: 8

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 383373 Color: 13
Size: 310168 Color: 17
Size: 306460 Color: 10

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 405011 Color: 3
Size: 324198 Color: 4
Size: 270792 Color: 15

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 412831 Color: 5
Size: 326546 Color: 2
Size: 260624 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 422435 Color: 15
Size: 308712 Color: 12
Size: 268854 Color: 8

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 372057 Color: 6
Size: 356652 Color: 15
Size: 271292 Color: 12

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 387816 Color: 8
Size: 356159 Color: 16
Size: 256026 Color: 6

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 400488 Color: 16
Size: 338624 Color: 2
Size: 260889 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 483927 Color: 1
Size: 261659 Color: 3
Size: 254415 Color: 10

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 367958 Color: 9
Size: 361684 Color: 6
Size: 270359 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 493406 Color: 1
Size: 255529 Color: 6
Size: 251066 Color: 11

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 363293 Color: 10
Size: 327877 Color: 10
Size: 308831 Color: 6

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 485203 Color: 4
Size: 260359 Color: 0
Size: 254439 Color: 14

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 436689 Color: 13
Size: 291332 Color: 1
Size: 271980 Color: 13

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 387774 Color: 2
Size: 314618 Color: 14
Size: 297609 Color: 7

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 446826 Color: 10
Size: 303038 Color: 15
Size: 250137 Color: 2

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 441604 Color: 6
Size: 284949 Color: 17
Size: 273448 Color: 10

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 379144 Color: 10
Size: 325113 Color: 16
Size: 295744 Color: 7

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 459502 Color: 16
Size: 279288 Color: 18
Size: 261211 Color: 17

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 400387 Color: 2
Size: 332239 Color: 7
Size: 267375 Color: 6

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 424630 Color: 8
Size: 301999 Color: 13
Size: 273372 Color: 13

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 394934 Color: 10
Size: 350276 Color: 16
Size: 254791 Color: 2

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 375238 Color: 2
Size: 335468 Color: 12
Size: 289295 Color: 6

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 407464 Color: 16
Size: 301258 Color: 17
Size: 291279 Color: 3

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 488778 Color: 16
Size: 257248 Color: 1
Size: 253975 Color: 15

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 374969 Color: 14
Size: 338321 Color: 4
Size: 286711 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 454039 Color: 3
Size: 278618 Color: 17
Size: 267344 Color: 11

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 447045 Color: 11
Size: 290559 Color: 10
Size: 262397 Color: 5

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 414270 Color: 16
Size: 301937 Color: 5
Size: 283794 Color: 9

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 433107 Color: 8
Size: 304092 Color: 16
Size: 262802 Color: 5

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 403425 Color: 13
Size: 346334 Color: 16
Size: 250242 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 389114 Color: 1
Size: 313335 Color: 3
Size: 297552 Color: 11

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 406972 Color: 0
Size: 321891 Color: 19
Size: 271138 Color: 8

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 492537 Color: 11
Size: 254284 Color: 12
Size: 253180 Color: 13

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 404310 Color: 4
Size: 336986 Color: 16
Size: 258705 Color: 11

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 403454 Color: 5
Size: 326077 Color: 15
Size: 270470 Color: 16

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 420124 Color: 10
Size: 293036 Color: 0
Size: 286841 Color: 12

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 482665 Color: 10
Size: 259167 Color: 10
Size: 258169 Color: 6

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 403258 Color: 6
Size: 318675 Color: 10
Size: 278068 Color: 17

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 479560 Color: 16
Size: 266315 Color: 18
Size: 254126 Color: 8

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 424740 Color: 0
Size: 308146 Color: 16
Size: 267115 Color: 10

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 427027 Color: 1
Size: 318562 Color: 19
Size: 254412 Color: 6

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 452393 Color: 1
Size: 294812 Color: 12
Size: 252796 Color: 3

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 461021 Color: 5
Size: 278313 Color: 11
Size: 260667 Color: 8

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 454107 Color: 1
Size: 280500 Color: 14
Size: 265394 Color: 17

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 411158 Color: 13
Size: 328533 Color: 18
Size: 260310 Color: 8

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 446543 Color: 3
Size: 285331 Color: 13
Size: 268127 Color: 14

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 395729 Color: 3
Size: 320838 Color: 13
Size: 283434 Color: 4

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 357320 Color: 2
Size: 331206 Color: 12
Size: 311475 Color: 10

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 357161 Color: 2
Size: 322130 Color: 1
Size: 320710 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 385779 Color: 4
Size: 356812 Color: 18
Size: 257410 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 399114 Color: 18
Size: 340217 Color: 2
Size: 260670 Color: 6

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 475366 Color: 8
Size: 263285 Color: 3
Size: 261350 Color: 7

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 382368 Color: 19
Size: 320842 Color: 11
Size: 296791 Color: 15

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 445023 Color: 6
Size: 278087 Color: 0
Size: 276891 Color: 7

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 366964 Color: 5
Size: 329243 Color: 18
Size: 303794 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 469970 Color: 12
Size: 277717 Color: 9
Size: 252314 Color: 9

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 364936 Color: 19
Size: 335503 Color: 18
Size: 299562 Color: 8

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 455615 Color: 1
Size: 285598 Color: 12
Size: 258788 Color: 10

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 379308 Color: 4
Size: 343256 Color: 13
Size: 277437 Color: 11

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 445503 Color: 2
Size: 294126 Color: 16
Size: 260372 Color: 19

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 369697 Color: 10
Size: 368659 Color: 16
Size: 261645 Color: 12

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 430927 Color: 16
Size: 301256 Color: 6
Size: 267818 Color: 4

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 485035 Color: 13
Size: 261884 Color: 6
Size: 253082 Color: 17

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 472107 Color: 4
Size: 277241 Color: 16
Size: 250653 Color: 3

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 435597 Color: 3
Size: 303035 Color: 19
Size: 261369 Color: 6

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 406107 Color: 4
Size: 341735 Color: 13
Size: 252159 Color: 18

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 473547 Color: 11
Size: 271967 Color: 6
Size: 254487 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 392985 Color: 1
Size: 347921 Color: 19
Size: 259095 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 368940 Color: 1
Size: 320561 Color: 14
Size: 310500 Color: 3

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 382435 Color: 2
Size: 350785 Color: 3
Size: 266781 Color: 12

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 422152 Color: 8
Size: 314849 Color: 17
Size: 263000 Color: 3

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 401022 Color: 1
Size: 324328 Color: 8
Size: 274651 Color: 1

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 455923 Color: 18
Size: 289733 Color: 7
Size: 254345 Color: 14

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 475524 Color: 12
Size: 269580 Color: 2
Size: 254897 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 393361 Color: 1
Size: 315052 Color: 13
Size: 291588 Color: 17

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 388597 Color: 18
Size: 337988 Color: 19
Size: 273416 Color: 2

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 423620 Color: 8
Size: 323039 Color: 9
Size: 253342 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 411064 Color: 11
Size: 298502 Color: 17
Size: 290435 Color: 2

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 481533 Color: 3
Size: 265124 Color: 4
Size: 253344 Color: 14

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 387782 Color: 9
Size: 353776 Color: 18
Size: 258443 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 363984 Color: 8
Size: 340148 Color: 11
Size: 295869 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 487068 Color: 14
Size: 257159 Color: 16
Size: 255774 Color: 15

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 354659 Color: 19
Size: 332180 Color: 2
Size: 313162 Color: 16

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 466742 Color: 4
Size: 279836 Color: 14
Size: 253423 Color: 3

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 382224 Color: 17
Size: 358024 Color: 8
Size: 259753 Color: 9

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 484166 Color: 12
Size: 265114 Color: 17
Size: 250721 Color: 2

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 382203 Color: 6
Size: 328273 Color: 11
Size: 289525 Color: 10

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 471778 Color: 3
Size: 270919 Color: 5
Size: 257304 Color: 14

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 491034 Color: 6
Size: 257686 Color: 9
Size: 251281 Color: 13

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 390363 Color: 7
Size: 338312 Color: 10
Size: 271326 Color: 4

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 361618 Color: 9
Size: 320389 Color: 8
Size: 317994 Color: 8

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 426977 Color: 6
Size: 306304 Color: 10
Size: 266720 Color: 12

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 459191 Color: 0
Size: 286110 Color: 14
Size: 254700 Color: 2

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 456120 Color: 17
Size: 278381 Color: 9
Size: 265500 Color: 2

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 492814 Color: 7
Size: 254580 Color: 0
Size: 252607 Color: 6

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 436592 Color: 14
Size: 304761 Color: 18
Size: 258648 Color: 4

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 379458 Color: 10
Size: 314460 Color: 14
Size: 306083 Color: 19

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 430744 Color: 7
Size: 315083 Color: 3
Size: 254174 Color: 2

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 459227 Color: 13
Size: 277785 Color: 9
Size: 262989 Color: 8

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 472063 Color: 15
Size: 265128 Color: 12
Size: 262810 Color: 10

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 418559 Color: 4
Size: 299600 Color: 0
Size: 281842 Color: 3

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 419623 Color: 4
Size: 297962 Color: 1
Size: 282416 Color: 17

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 465792 Color: 17
Size: 281424 Color: 9
Size: 252785 Color: 4

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 426505 Color: 10
Size: 301037 Color: 6
Size: 272459 Color: 17

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 442570 Color: 2
Size: 286199 Color: 0
Size: 271232 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 384321 Color: 2
Size: 340415 Color: 1
Size: 275265 Color: 6

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 453964 Color: 18
Size: 283604 Color: 19
Size: 262433 Color: 7

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 355164 Color: 7
Size: 339952 Color: 19
Size: 304885 Color: 8

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 417789 Color: 8
Size: 323469 Color: 15
Size: 258743 Color: 8

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 476129 Color: 15
Size: 266727 Color: 8
Size: 257145 Color: 7

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 392802 Color: 8
Size: 337756 Color: 3
Size: 269443 Color: 13

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 379402 Color: 16
Size: 316738 Color: 0
Size: 303861 Color: 7

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 373637 Color: 13
Size: 324561 Color: 16
Size: 301803 Color: 1

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 418734 Color: 7
Size: 311591 Color: 2
Size: 269676 Color: 7

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 391683 Color: 6
Size: 310676 Color: 19
Size: 297642 Color: 17

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 392870 Color: 3
Size: 313769 Color: 7
Size: 293362 Color: 15

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 464043 Color: 18
Size: 281296 Color: 13
Size: 254662 Color: 16

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 439625 Color: 3
Size: 294647 Color: 19
Size: 265729 Color: 11

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 385775 Color: 8
Size: 349369 Color: 5
Size: 264857 Color: 4

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 435342 Color: 18
Size: 282564 Color: 0
Size: 282095 Color: 14

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 436874 Color: 2
Size: 292968 Color: 4
Size: 270159 Color: 3

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 472477 Color: 16
Size: 273048 Color: 12
Size: 254476 Color: 7

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 366553 Color: 16
Size: 363973 Color: 1
Size: 269475 Color: 6

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 461434 Color: 11
Size: 284707 Color: 13
Size: 253860 Color: 10

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 372241 Color: 6
Size: 320224 Color: 8
Size: 307536 Color: 15

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 486810 Color: 7
Size: 257403 Color: 6
Size: 255788 Color: 14

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 406785 Color: 12
Size: 296689 Color: 18
Size: 296527 Color: 19

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 491231 Color: 8
Size: 255915 Color: 7
Size: 252855 Color: 15

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 372385 Color: 9
Size: 352011 Color: 19
Size: 275605 Color: 4

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 360754 Color: 9
Size: 339940 Color: 6
Size: 299307 Color: 3

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 440765 Color: 4
Size: 281326 Color: 6
Size: 277910 Color: 7

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 399429 Color: 0
Size: 324560 Color: 2
Size: 276012 Color: 19

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 416486 Color: 15
Size: 302436 Color: 15
Size: 281079 Color: 10

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 431042 Color: 14
Size: 315302 Color: 17
Size: 253657 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 372931 Color: 15
Size: 315260 Color: 8
Size: 311810 Color: 19

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 436287 Color: 15
Size: 284179 Color: 6
Size: 279535 Color: 17

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 424085 Color: 16
Size: 317497 Color: 1
Size: 258419 Color: 17

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 453345 Color: 3
Size: 287724 Color: 11
Size: 258932 Color: 7

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 426611 Color: 13
Size: 321243 Color: 4
Size: 252147 Color: 12

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 462948 Color: 3
Size: 273421 Color: 11
Size: 263632 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 380415 Color: 18
Size: 315008 Color: 18
Size: 304578 Color: 11

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 367445 Color: 17
Size: 345824 Color: 12
Size: 286732 Color: 6

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 436054 Color: 4
Size: 306703 Color: 14
Size: 257244 Color: 19

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 388194 Color: 17
Size: 316359 Color: 16
Size: 295448 Color: 4

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 435504 Color: 12
Size: 286481 Color: 18
Size: 278016 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 368406 Color: 13
Size: 361897 Color: 2
Size: 269698 Color: 7

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 364170 Color: 17
Size: 354967 Color: 5
Size: 280864 Color: 11

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 358892 Color: 1
Size: 324067 Color: 18
Size: 317042 Color: 9

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 346481 Color: 6
Size: 342706 Color: 16
Size: 310814 Color: 18

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 435204 Color: 15
Size: 282524 Color: 19
Size: 282273 Color: 10

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 360995 Color: 4
Size: 323652 Color: 9
Size: 315354 Color: 6

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 408134 Color: 12
Size: 339789 Color: 9
Size: 252078 Color: 10

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 483749 Color: 3
Size: 259097 Color: 16
Size: 257155 Color: 8

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 428814 Color: 13
Size: 317974 Color: 11
Size: 253213 Color: 17

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 456837 Color: 16
Size: 280235 Color: 2
Size: 262929 Color: 19

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 412189 Color: 19
Size: 300233 Color: 16
Size: 287579 Color: 12

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 385826 Color: 18
Size: 319656 Color: 5
Size: 294519 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 363481 Color: 18
Size: 357820 Color: 16
Size: 278700 Color: 10

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 489584 Color: 19
Size: 259194 Color: 0
Size: 251223 Color: 13

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 358819 Color: 6
Size: 331185 Color: 11
Size: 309997 Color: 14

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 419864 Color: 2
Size: 296194 Color: 14
Size: 283943 Color: 15

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 480155 Color: 8
Size: 260261 Color: 10
Size: 259585 Color: 8

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 393404 Color: 3
Size: 330562 Color: 16
Size: 276035 Color: 5

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 497422 Color: 19
Size: 252519 Color: 13
Size: 250060 Color: 5

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 405992 Color: 19
Size: 330124 Color: 17
Size: 263885 Color: 18

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 337810 Color: 7
Size: 337302 Color: 13
Size: 324889 Color: 17

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 408108 Color: 6
Size: 336479 Color: 8
Size: 255414 Color: 5

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 401990 Color: 17
Size: 300751 Color: 16
Size: 297260 Color: 8

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 397725 Color: 13
Size: 335786 Color: 0
Size: 266490 Color: 2

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 464401 Color: 11
Size: 274702 Color: 11
Size: 260898 Color: 12

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 386460 Color: 12
Size: 321655 Color: 16
Size: 291886 Color: 11

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 340489 Color: 5
Size: 338458 Color: 15
Size: 321054 Color: 7

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 358395 Color: 12
Size: 352872 Color: 18
Size: 288734 Color: 7

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 360383 Color: 16
Size: 358675 Color: 16
Size: 280943 Color: 7

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 354053 Color: 12
Size: 328162 Color: 17
Size: 317786 Color: 17

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 358916 Color: 9
Size: 321880 Color: 8
Size: 319205 Color: 9

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 341432 Color: 3
Size: 332072 Color: 0
Size: 326497 Color: 3

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 353641 Color: 7
Size: 342441 Color: 2
Size: 303919 Color: 1

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 341832 Color: 17
Size: 331178 Color: 8
Size: 326991 Color: 5

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 364703 Color: 18
Size: 329434 Color: 18
Size: 305864 Color: 2

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 342147 Color: 14
Size: 338522 Color: 10
Size: 319332 Color: 15

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 367158 Color: 6
Size: 327911 Color: 8
Size: 304932 Color: 19

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 361680 Color: 19
Size: 361148 Color: 12
Size: 277173 Color: 19

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 374432 Color: 4
Size: 366663 Color: 14
Size: 258906 Color: 19

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 375867 Color: 5
Size: 353328 Color: 18
Size: 270806 Color: 10

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 354838 Color: 15
Size: 333349 Color: 16
Size: 311814 Color: 12

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 381169 Color: 16
Size: 347953 Color: 7
Size: 270879 Color: 17

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 350647 Color: 10
Size: 332057 Color: 4
Size: 317297 Color: 11

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 387468 Color: 6
Size: 326299 Color: 3
Size: 286234 Color: 12

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 365345 Color: 5
Size: 323980 Color: 14
Size: 310676 Color: 10

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 398357 Color: 10
Size: 349816 Color: 15
Size: 251828 Color: 16

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 369566 Color: 19
Size: 319129 Color: 1
Size: 311306 Color: 16

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 339142 Color: 9
Size: 335312 Color: 14
Size: 325547 Color: 8

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 357795 Color: 6
Size: 342198 Color: 8
Size: 300008 Color: 11

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 358927 Color: 14
Size: 323906 Color: 15
Size: 317168 Color: 11

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 339997 Color: 9
Size: 334920 Color: 7
Size: 325084 Color: 10

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 359334 Color: 11
Size: 325670 Color: 2
Size: 314997 Color: 8

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 363386 Color: 14
Size: 353525 Color: 12
Size: 283090 Color: 2

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 364184 Color: 4
Size: 338083 Color: 2
Size: 297734 Color: 8

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 353067 Color: 18
Size: 339502 Color: 6
Size: 307432 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 373696 Color: 12
Size: 357221 Color: 16
Size: 269084 Color: 3

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 353974 Color: 10
Size: 323472 Color: 6
Size: 322555 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 362659 Color: 3
Size: 331395 Color: 2
Size: 305947 Color: 17

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 351502 Color: 16
Size: 339924 Color: 6
Size: 308575 Color: 4

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 365833 Color: 3
Size: 361920 Color: 18
Size: 272248 Color: 4

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 348222 Color: 2
Size: 333647 Color: 11
Size: 318132 Color: 11

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 369756 Color: 4
Size: 339123 Color: 9
Size: 291122 Color: 10

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 370040 Color: 9
Size: 347162 Color: 13
Size: 282799 Color: 2

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 357225 Color: 18
Size: 330915 Color: 18
Size: 311861 Color: 10

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 352843 Color: 17
Size: 350953 Color: 17
Size: 296205 Color: 19

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 345898 Color: 12
Size: 332334 Color: 8
Size: 321769 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 358779 Color: 3
Size: 337705 Color: 11
Size: 303517 Color: 5

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 362223 Color: 13
Size: 357893 Color: 5
Size: 279885 Color: 1

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 344200 Color: 17
Size: 342588 Color: 0
Size: 313213 Color: 18

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 338359 Color: 6
Size: 332642 Color: 3
Size: 329000 Color: 15

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 357671 Color: 6
Size: 340800 Color: 12
Size: 301530 Color: 12

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 361000 Color: 19
Size: 359666 Color: 7
Size: 279335 Color: 9

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 348843 Color: 2
Size: 336911 Color: 2
Size: 314247 Color: 11

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 369633 Color: 19
Size: 348258 Color: 10
Size: 282110 Color: 5

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 359634 Color: 4
Size: 341640 Color: 7
Size: 298727 Color: 2

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 348983 Color: 13
Size: 326269 Color: 5
Size: 324749 Color: 13

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 348342 Color: 8
Size: 326628 Color: 4
Size: 325031 Color: 15

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 351838 Color: 4
Size: 337375 Color: 17
Size: 310788 Color: 16

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 350904 Color: 10
Size: 335764 Color: 10
Size: 313333 Color: 16

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 389974 Color: 4
Size: 356002 Color: 19
Size: 254025 Color: 4

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 344144 Color: 4
Size: 329148 Color: 12
Size: 326709 Color: 3

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 357610 Color: 14
Size: 340258 Color: 15
Size: 302133 Color: 10

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 375546 Color: 12
Size: 358194 Color: 1
Size: 266261 Color: 2

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 347474 Color: 13
Size: 327286 Color: 19
Size: 325241 Color: 6

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 360002 Color: 10
Size: 353589 Color: 5
Size: 286410 Color: 4

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 345442 Color: 19
Size: 341236 Color: 11
Size: 313323 Color: 11

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 353370 Color: 4
Size: 348127 Color: 19
Size: 298504 Color: 4

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 349250 Color: 14
Size: 336459 Color: 18
Size: 314292 Color: 2

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 355965 Color: 16
Size: 334018 Color: 6
Size: 310018 Color: 18

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 353425 Color: 15
Size: 326154 Color: 11
Size: 320422 Color: 14

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 347053 Color: 16
Size: 335110 Color: 4
Size: 317838 Color: 13

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 360168 Color: 16
Size: 340143 Color: 8
Size: 299690 Color: 7

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 347853 Color: 8
Size: 335380 Color: 14
Size: 316768 Color: 5

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 364776 Color: 2
Size: 358438 Color: 13
Size: 276787 Color: 3

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 351390 Color: 9
Size: 330630 Color: 3
Size: 317981 Color: 2

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 342128 Color: 13
Size: 338105 Color: 18
Size: 319768 Color: 18

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 341974 Color: 19
Size: 332958 Color: 8
Size: 325069 Color: 6

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 348268 Color: 14
Size: 344741 Color: 14
Size: 306992 Color: 17

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 365956 Color: 18
Size: 357000 Color: 0
Size: 277045 Color: 10

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 344598 Color: 0
Size: 328873 Color: 8
Size: 326530 Color: 18

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 357383 Color: 0
Size: 351726 Color: 7
Size: 290892 Color: 16

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 355928 Color: 5
Size: 329237 Color: 13
Size: 314836 Color: 19

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 349187 Color: 2
Size: 345085 Color: 12
Size: 305729 Color: 3

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 348229 Color: 1
Size: 339153 Color: 16
Size: 312619 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 358774 Color: 17
Size: 347839 Color: 9
Size: 293388 Color: 10

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 357654 Color: 14
Size: 326530 Color: 13
Size: 315817 Color: 16

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 356485 Color: 17
Size: 324186 Color: 7
Size: 319330 Color: 12

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 346953 Color: 4
Size: 341191 Color: 3
Size: 311857 Color: 11

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 348912 Color: 5
Size: 338626 Color: 0
Size: 312463 Color: 2

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 356363 Color: 16
Size: 336014 Color: 5
Size: 307624 Color: 7

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 346798 Color: 0
Size: 338262 Color: 5
Size: 314941 Color: 1

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 360850 Color: 12
Size: 320667 Color: 11
Size: 318484 Color: 11

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 371483 Color: 3
Size: 345234 Color: 15
Size: 283284 Color: 8

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 346966 Color: 13
Size: 330679 Color: 10
Size: 322356 Color: 4

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 442192 Color: 17
Size: 288507 Color: 2
Size: 269302 Color: 19

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 339541 Color: 2
Size: 337696 Color: 2
Size: 322764 Color: 10

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 341096 Color: 3
Size: 330156 Color: 0
Size: 328749 Color: 18

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 354178 Color: 12
Size: 326792 Color: 7
Size: 319031 Color: 18

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 393235 Color: 9
Size: 342511 Color: 1
Size: 264255 Color: 19

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 346180 Color: 6
Size: 326995 Color: 0
Size: 326826 Color: 11

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 356455 Color: 4
Size: 355575 Color: 14
Size: 287971 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 346469 Color: 17
Size: 338403 Color: 14
Size: 315129 Color: 13

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 356627 Color: 7
Size: 330778 Color: 9
Size: 312596 Color: 1

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 349366 Color: 12
Size: 342259 Color: 8
Size: 308376 Color: 17

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 339913 Color: 11
Size: 332911 Color: 18
Size: 327177 Color: 18

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 398155 Color: 2
Size: 302130 Color: 9
Size: 299716 Color: 18

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 359490 Color: 4
Size: 348450 Color: 18
Size: 292061 Color: 4

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 355835 Color: 6
Size: 351939 Color: 5
Size: 292227 Color: 17

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 354746 Color: 17
Size: 354598 Color: 4
Size: 290657 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 371321 Color: 0
Size: 357503 Color: 17
Size: 271177 Color: 7

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 360836 Color: 0
Size: 351454 Color: 17
Size: 287711 Color: 9

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 356555 Color: 2
Size: 350102 Color: 2
Size: 293344 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 355252 Color: 5
Size: 347910 Color: 3
Size: 296839 Color: 9

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 356035 Color: 17
Size: 353564 Color: 15
Size: 290402 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 353080 Color: 10
Size: 349084 Color: 8
Size: 297837 Color: 3

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 353680 Color: 4
Size: 352315 Color: 6
Size: 294006 Color: 9

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 381548 Color: 4
Size: 366852 Color: 0
Size: 251601 Color: 4

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 379753 Color: 16
Size: 353918 Color: 10
Size: 266330 Color: 12

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 338986 Color: 5
Size: 331687 Color: 12
Size: 329328 Color: 9

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 379391 Color: 0
Size: 363766 Color: 16
Size: 256844 Color: 13

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 370866 Color: 12
Size: 318548 Color: 7
Size: 310587 Color: 7

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 351908 Color: 7
Size: 346271 Color: 0
Size: 301822 Color: 19

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 366359 Color: 12
Size: 337787 Color: 14
Size: 295855 Color: 4

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 393420 Color: 16
Size: 350775 Color: 4
Size: 255806 Color: 10

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 351283 Color: 4
Size: 350118 Color: 2
Size: 298600 Color: 7

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 346890 Color: 11
Size: 338721 Color: 8
Size: 314390 Color: 14

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 359521 Color: 19
Size: 328244 Color: 17
Size: 312236 Color: 18

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 337715 Color: 19
Size: 332614 Color: 16
Size: 329672 Color: 10

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 337031 Color: 13
Size: 334371 Color: 10
Size: 328599 Color: 9

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 365182 Color: 4
Size: 341528 Color: 9
Size: 293291 Color: 18

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 345660 Color: 3
Size: 338671 Color: 5
Size: 315670 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 347785 Color: 7
Size: 342864 Color: 2
Size: 309352 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 355341 Color: 5
Size: 350767 Color: 6
Size: 293893 Color: 19

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 354298 Color: 3
Size: 345133 Color: 0
Size: 300570 Color: 4

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 342855 Color: 9
Size: 340713 Color: 0
Size: 316433 Color: 1

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 350926 Color: 10
Size: 345844 Color: 12
Size: 303231 Color: 2

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 349900 Color: 9
Size: 345025 Color: 13
Size: 305076 Color: 5

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 344726 Color: 2
Size: 341088 Color: 19
Size: 314187 Color: 2

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 363047 Color: 2
Size: 344622 Color: 5
Size: 292332 Color: 11

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 343752 Color: 18
Size: 340799 Color: 9
Size: 315450 Color: 12

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 344187 Color: 3
Size: 342225 Color: 8
Size: 313589 Color: 8

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 353422 Color: 19
Size: 345195 Color: 2
Size: 301384 Color: 18

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 363737 Color: 2
Size: 343801 Color: 17
Size: 292463 Color: 6

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 356828 Color: 13
Size: 342548 Color: 11
Size: 300625 Color: 2

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 363696 Color: 14
Size: 338613 Color: 7
Size: 297692 Color: 16

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 353990 Color: 1
Size: 342619 Color: 19
Size: 303392 Color: 17

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 347123 Color: 15
Size: 346755 Color: 11
Size: 306123 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 346911 Color: 9
Size: 332885 Color: 7
Size: 320205 Color: 10

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 358152 Color: 14
Size: 343292 Color: 0
Size: 298557 Color: 15

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 373003 Color: 18
Size: 338392 Color: 15
Size: 288606 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 353630 Color: 14
Size: 348691 Color: 3
Size: 297680 Color: 11

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 341169 Color: 11
Size: 334284 Color: 16
Size: 324548 Color: 15

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 349578 Color: 14
Size: 329748 Color: 17
Size: 320675 Color: 15

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 340301 Color: 2
Size: 339749 Color: 1
Size: 319951 Color: 7

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 340621 Color: 8
Size: 333206 Color: 16
Size: 326174 Color: 1

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 347778 Color: 15
Size: 326246 Color: 12
Size: 325977 Color: 7

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 346009 Color: 1
Size: 340841 Color: 3
Size: 313151 Color: 8

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 338876 Color: 0
Size: 338055 Color: 2
Size: 323070 Color: 1

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 353418 Color: 4
Size: 326093 Color: 16
Size: 320490 Color: 2

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 336783 Color: 14
Size: 333631 Color: 9
Size: 329587 Color: 1

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 344595 Color: 13
Size: 336927 Color: 4
Size: 318479 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 338793 Color: 9
Size: 336335 Color: 18
Size: 324873 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 338117 Color: 2
Size: 334093 Color: 3
Size: 327791 Color: 12

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 335997 Color: 9
Size: 335835 Color: 14
Size: 328169 Color: 10

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 354573 Color: 4
Size: 334945 Color: 13
Size: 310483 Color: 14

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 359870 Color: 3
Size: 333636 Color: 18
Size: 306495 Color: 13

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 350953 Color: 2
Size: 332037 Color: 0
Size: 317011 Color: 9

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 356114 Color: 2
Size: 323233 Color: 1
Size: 320654 Color: 5

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 356689 Color: 12
Size: 338529 Color: 3
Size: 304783 Color: 1

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 357108 Color: 18
Size: 325171 Color: 14
Size: 317722 Color: 13

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 357631 Color: 6
Size: 323281 Color: 10
Size: 319089 Color: 11

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 358212 Color: 8
Size: 336236 Color: 17
Size: 305553 Color: 8

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 358879 Color: 0
Size: 323072 Color: 12
Size: 318050 Color: 9

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 358571 Color: 5
Size: 324041 Color: 19
Size: 317389 Color: 13

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 359202 Color: 9
Size: 326332 Color: 10
Size: 314467 Color: 17

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 359217 Color: 8
Size: 326254 Color: 15
Size: 314530 Color: 18

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 359251 Color: 17
Size: 335220 Color: 14
Size: 305530 Color: 13

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 359345 Color: 16
Size: 330985 Color: 15
Size: 309671 Color: 7

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 359357 Color: 1
Size: 336197 Color: 4
Size: 304447 Color: 12

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 359904 Color: 19
Size: 346804 Color: 18
Size: 293293 Color: 3

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 359111 Color: 18
Size: 320557 Color: 17
Size: 320333 Color: 7

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 359951 Color: 13
Size: 329064 Color: 12
Size: 310986 Color: 5

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 360442 Color: 0
Size: 339161 Color: 14
Size: 300398 Color: 5

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 360605 Color: 14
Size: 346790 Color: 15
Size: 292606 Color: 3

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 360626 Color: 12
Size: 352965 Color: 15
Size: 286410 Color: 16

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 360628 Color: 11
Size: 322318 Color: 10
Size: 317055 Color: 6

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 360706 Color: 10
Size: 345402 Color: 4
Size: 293893 Color: 9

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 360873 Color: 14
Size: 338730 Color: 1
Size: 300398 Color: 8

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 361232 Color: 0
Size: 327777 Color: 13
Size: 310992 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 361314 Color: 0
Size: 330804 Color: 3
Size: 307883 Color: 15

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 361338 Color: 16
Size: 336497 Color: 13
Size: 302166 Color: 5

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 361557 Color: 7
Size: 356979 Color: 17
Size: 281465 Color: 11

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 361563 Color: 16
Size: 342252 Color: 1
Size: 296186 Color: 10

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 361580 Color: 8
Size: 341861 Color: 17
Size: 296560 Color: 17

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 361745 Color: 6
Size: 345287 Color: 4
Size: 292969 Color: 13

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 361772 Color: 5
Size: 333354 Color: 1
Size: 304875 Color: 5

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 361812 Color: 16
Size: 357867 Color: 11
Size: 280322 Color: 19

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 362019 Color: 8
Size: 340912 Color: 0
Size: 297070 Color: 19

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 362039 Color: 4
Size: 331793 Color: 10
Size: 306169 Color: 18

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 362105 Color: 10
Size: 323349 Color: 3
Size: 314547 Color: 17

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 362108 Color: 4
Size: 328899 Color: 9
Size: 308994 Color: 10

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 362326 Color: 13
Size: 333508 Color: 19
Size: 304167 Color: 17

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 362375 Color: 3
Size: 324296 Color: 1
Size: 313330 Color: 6

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 362609 Color: 14
Size: 321783 Color: 13
Size: 315609 Color: 13

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 362668 Color: 6
Size: 352683 Color: 4
Size: 284650 Color: 17

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 362713 Color: 14
Size: 321751 Color: 13
Size: 315537 Color: 1

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 362793 Color: 18
Size: 345015 Color: 13
Size: 292193 Color: 6

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 362898 Color: 15
Size: 359465 Color: 10
Size: 277638 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 362980 Color: 18
Size: 328564 Color: 3
Size: 308457 Color: 16

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 363051 Color: 6
Size: 327893 Color: 1
Size: 309057 Color: 10

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 363009 Color: 18
Size: 329900 Color: 0
Size: 307092 Color: 17

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 363174 Color: 1
Size: 322832 Color: 4
Size: 313995 Color: 13

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 363354 Color: 0
Size: 354380 Color: 19
Size: 282267 Color: 7

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 363385 Color: 8
Size: 339705 Color: 14
Size: 296911 Color: 7

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 363704 Color: 2
Size: 330663 Color: 7
Size: 305634 Color: 14

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 363495 Color: 16
Size: 335395 Color: 4
Size: 301111 Color: 5

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 363538 Color: 11
Size: 342042 Color: 0
Size: 294421 Color: 8

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 363551 Color: 14
Size: 337285 Color: 17
Size: 299165 Color: 16

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 363586 Color: 9
Size: 318864 Color: 15
Size: 317551 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 363750 Color: 19
Size: 323638 Color: 7
Size: 312613 Color: 19

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 363756 Color: 9
Size: 328045 Color: 17
Size: 308200 Color: 0

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 363784 Color: 15
Size: 347373 Color: 3
Size: 288844 Color: 12

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 363820 Color: 0
Size: 333146 Color: 2
Size: 303035 Color: 6

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 363878 Color: 17
Size: 342674 Color: 13
Size: 293449 Color: 17

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 363881 Color: 8
Size: 345770 Color: 13
Size: 290350 Color: 14

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 363930 Color: 0
Size: 358990 Color: 16
Size: 277081 Color: 10

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 364034 Color: 14
Size: 327390 Color: 5
Size: 308577 Color: 5

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 364048 Color: 0
Size: 321775 Color: 1
Size: 314178 Color: 10

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 364084 Color: 5
Size: 335652 Color: 6
Size: 300265 Color: 5

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 364140 Color: 3
Size: 350943 Color: 17
Size: 284918 Color: 3

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 364240 Color: 3
Size: 331997 Color: 14
Size: 303764 Color: 7

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 364276 Color: 3
Size: 347438 Color: 9
Size: 288287 Color: 3

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 364278 Color: 12
Size: 330895 Color: 17
Size: 304828 Color: 9

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 364320 Color: 8
Size: 353793 Color: 18
Size: 281888 Color: 15

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 364371 Color: 19
Size: 338430 Color: 0
Size: 297200 Color: 6

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 364372 Color: 10
Size: 351268 Color: 1
Size: 284361 Color: 14

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 364448 Color: 3
Size: 321252 Color: 19
Size: 314301 Color: 10

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 364588 Color: 9
Size: 329814 Color: 9
Size: 305599 Color: 12

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 364603 Color: 12
Size: 358682 Color: 15
Size: 276716 Color: 12

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 364735 Color: 14
Size: 342214 Color: 0
Size: 293052 Color: 5

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 364768 Color: 3
Size: 325561 Color: 16
Size: 309672 Color: 12

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 364780 Color: 11
Size: 349329 Color: 10
Size: 285892 Color: 12

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 364793 Color: 14
Size: 358895 Color: 8
Size: 276313 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 365192 Color: 14
Size: 327054 Color: 5
Size: 307755 Color: 13

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 364896 Color: 8
Size: 320031 Color: 13
Size: 315074 Color: 6

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 365116 Color: 16
Size: 327528 Color: 13
Size: 307357 Color: 6

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 365158 Color: 7
Size: 331340 Color: 19
Size: 303503 Color: 14

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 365212 Color: 1
Size: 361144 Color: 0
Size: 273645 Color: 17

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 365227 Color: 1
Size: 323855 Color: 13
Size: 310919 Color: 16

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 365287 Color: 16
Size: 354426 Color: 18
Size: 280288 Color: 15

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 365295 Color: 12
Size: 362955 Color: 3
Size: 271751 Color: 19

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 365314 Color: 19
Size: 350533 Color: 0
Size: 284154 Color: 17

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 365423 Color: 10
Size: 325695 Color: 3
Size: 308883 Color: 4

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 365474 Color: 17
Size: 341878 Color: 0
Size: 292649 Color: 18

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 365560 Color: 12
Size: 331023 Color: 2
Size: 303418 Color: 16

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 365579 Color: 18
Size: 347737 Color: 19
Size: 286685 Color: 3

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 365661 Color: 11
Size: 332036 Color: 17
Size: 302304 Color: 3

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 365681 Color: 19
Size: 337933 Color: 8
Size: 296387 Color: 19

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 365820 Color: 14
Size: 360654 Color: 2
Size: 273527 Color: 15

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 365849 Color: 16
Size: 336902 Color: 0
Size: 297250 Color: 12

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 365858 Color: 1
Size: 326201 Color: 11
Size: 307942 Color: 15

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 365892 Color: 19
Size: 365684 Color: 1
Size: 268425 Color: 8

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 366098 Color: 14
Size: 357237 Color: 13
Size: 276666 Color: 3

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 366149 Color: 10
Size: 345177 Color: 11
Size: 288675 Color: 4

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 366150 Color: 12
Size: 360920 Color: 6
Size: 272931 Color: 14

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 366181 Color: 0
Size: 340847 Color: 8
Size: 292973 Color: 18

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 366269 Color: 10
Size: 359170 Color: 18
Size: 274562 Color: 11

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 366311 Color: 5
Size: 330623 Color: 2
Size: 303067 Color: 13

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 366311 Color: 5
Size: 324417 Color: 15
Size: 309273 Color: 13

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 366315 Color: 8
Size: 345662 Color: 15
Size: 288024 Color: 17

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 366329 Color: 14
Size: 343076 Color: 12
Size: 290596 Color: 19

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 366409 Color: 1
Size: 333899 Color: 16
Size: 299693 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 366460 Color: 18
Size: 339586 Color: 10
Size: 293955 Color: 14

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 366470 Color: 8
Size: 336464 Color: 7
Size: 297067 Color: 7

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 366582 Color: 18
Size: 328932 Color: 7
Size: 304487 Color: 10

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 366705 Color: 10
Size: 326179 Color: 12
Size: 307117 Color: 7

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 366789 Color: 9
Size: 320597 Color: 14
Size: 312615 Color: 15

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 366807 Color: 2
Size: 360044 Color: 9
Size: 273150 Color: 16

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 366877 Color: 18
Size: 335518 Color: 0
Size: 297606 Color: 6

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 366892 Color: 0
Size: 331939 Color: 18
Size: 301170 Color: 17

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 366990 Color: 18
Size: 337094 Color: 13
Size: 295917 Color: 4

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 367204 Color: 10
Size: 363304 Color: 7
Size: 269493 Color: 8

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 367217 Color: 2
Size: 356830 Color: 8
Size: 275954 Color: 12

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 367229 Color: 16
Size: 322194 Color: 8
Size: 310578 Color: 4

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 367007 Color: 18
Size: 322035 Color: 2
Size: 310959 Color: 12

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 367363 Color: 17
Size: 320280 Color: 9
Size: 312358 Color: 2

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 367372 Color: 1
Size: 320704 Color: 13
Size: 311925 Color: 8

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 367454 Color: 1
Size: 323225 Color: 19
Size: 309322 Color: 3

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 367498 Color: 8
Size: 336088 Color: 9
Size: 296415 Color: 10

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 367545 Color: 19
Size: 361246 Color: 4
Size: 271210 Color: 2

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 367558 Color: 5
Size: 318767 Color: 8
Size: 313676 Color: 9

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 367638 Color: 5
Size: 338155 Color: 4
Size: 294208 Color: 9

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 367683 Color: 11
Size: 366821 Color: 17
Size: 265497 Color: 8

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 367811 Color: 8
Size: 349925 Color: 18
Size: 282265 Color: 2

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 367812 Color: 1
Size: 357008 Color: 19
Size: 275181 Color: 3

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 367826 Color: 2
Size: 335918 Color: 5
Size: 296257 Color: 11

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 367832 Color: 13
Size: 361645 Color: 7
Size: 270524 Color: 1

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 367629 Color: 19
Size: 341729 Color: 19
Size: 290643 Color: 1

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 367855 Color: 11
Size: 351059 Color: 12
Size: 281087 Color: 18

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 368089 Color: 0
Size: 352146 Color: 10
Size: 279766 Color: 16

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 367929 Color: 3
Size: 337611 Color: 16
Size: 294461 Color: 15

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 367935 Color: 12
Size: 341235 Color: 16
Size: 290831 Color: 7

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 368037 Color: 10
Size: 322386 Color: 17
Size: 309578 Color: 9

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 368152 Color: 0
Size: 347060 Color: 15
Size: 284789 Color: 4

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 368099 Color: 2
Size: 317623 Color: 16
Size: 314279 Color: 7

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 368139 Color: 16
Size: 366983 Color: 18
Size: 264879 Color: 10

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 368245 Color: 8
Size: 353663 Color: 10
Size: 278093 Color: 17

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 368352 Color: 15
Size: 359898 Color: 8
Size: 271751 Color: 7

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 368448 Color: 0
Size: 362630 Color: 16
Size: 268923 Color: 2

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 368504 Color: 6
Size: 318379 Color: 16
Size: 313118 Color: 14

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 368635 Color: 3
Size: 331816 Color: 10
Size: 299550 Color: 9

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 368723 Color: 12
Size: 347215 Color: 9
Size: 284063 Color: 7

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 368731 Color: 2
Size: 368413 Color: 19
Size: 262857 Color: 6

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 368781 Color: 1
Size: 327301 Color: 5
Size: 303919 Color: 13

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 368821 Color: 18
Size: 327028 Color: 19
Size: 304152 Color: 18

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 368876 Color: 8
Size: 331164 Color: 13
Size: 299961 Color: 15

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 368915 Color: 4
Size: 330046 Color: 12
Size: 301040 Color: 1

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 368926 Color: 17
Size: 348761 Color: 2
Size: 282314 Color: 1

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 368940 Color: 16
Size: 324205 Color: 0
Size: 306856 Color: 10

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 368948 Color: 10
Size: 332199 Color: 2
Size: 298854 Color: 17

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 368974 Color: 1
Size: 362231 Color: 13
Size: 268796 Color: 4

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 369023 Color: 19
Size: 318904 Color: 17
Size: 312074 Color: 6

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 369043 Color: 2
Size: 336450 Color: 15
Size: 294508 Color: 14

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 369227 Color: 2
Size: 328400 Color: 15
Size: 302374 Color: 15

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 369050 Color: 8
Size: 339341 Color: 8
Size: 291610 Color: 9

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 369067 Color: 18
Size: 355384 Color: 5
Size: 275550 Color: 14

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 369102 Color: 3
Size: 355026 Color: 17
Size: 275873 Color: 5

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 369114 Color: 5
Size: 323764 Color: 10
Size: 307123 Color: 4

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 369181 Color: 3
Size: 336729 Color: 6
Size: 294091 Color: 11

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 369448 Color: 2
Size: 327002 Color: 10
Size: 303551 Color: 12

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 369303 Color: 4
Size: 327567 Color: 6
Size: 303131 Color: 3

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 369421 Color: 7
Size: 331108 Color: 19
Size: 299472 Color: 4

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 369424 Color: 1
Size: 346159 Color: 12
Size: 284418 Color: 4

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 369463 Color: 18
Size: 332923 Color: 3
Size: 297615 Color: 12

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 369464 Color: 15
Size: 324878 Color: 16
Size: 305659 Color: 16

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 369497 Color: 11
Size: 323092 Color: 18
Size: 307412 Color: 19

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 369570 Color: 13
Size: 317683 Color: 1
Size: 312748 Color: 12

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 369573 Color: 12
Size: 356401 Color: 14
Size: 274027 Color: 5

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 369630 Color: 6
Size: 363448 Color: 6
Size: 266923 Color: 15

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 369646 Color: 9
Size: 331286 Color: 10
Size: 299069 Color: 17

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 369652 Color: 4
Size: 346667 Color: 9
Size: 283682 Color: 9

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 369668 Color: 4
Size: 325325 Color: 5
Size: 305008 Color: 6

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 369733 Color: 15
Size: 325206 Color: 7
Size: 305062 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 369871 Color: 10
Size: 335231 Color: 16
Size: 294899 Color: 7

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 369933 Color: 7
Size: 366652 Color: 9
Size: 263416 Color: 18

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 369960 Color: 8
Size: 348139 Color: 11
Size: 281902 Color: 16

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 370063 Color: 3
Size: 351591 Color: 11
Size: 278347 Color: 8

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 370064 Color: 16
Size: 323411 Color: 4
Size: 306526 Color: 19

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 370130 Color: 0
Size: 322343 Color: 17
Size: 307528 Color: 17

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 370170 Color: 17
Size: 315513 Color: 6
Size: 314318 Color: 15

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 370360 Color: 3
Size: 325570 Color: 0
Size: 304071 Color: 16

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 370581 Color: 5
Size: 320146 Color: 6
Size: 309274 Color: 12

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 370622 Color: 6
Size: 320236 Color: 14
Size: 309143 Color: 8

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 370655 Color: 9
Size: 326475 Color: 12
Size: 302871 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 370704 Color: 8
Size: 366582 Color: 18
Size: 262715 Color: 3

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 370730 Color: 13
Size: 321962 Color: 10
Size: 307309 Color: 17

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 370819 Color: 0
Size: 324491 Color: 16
Size: 304691 Color: 17

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 370931 Color: 0
Size: 326083 Color: 18
Size: 302987 Color: 3

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 370746 Color: 15
Size: 327093 Color: 17
Size: 302162 Color: 8

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 370765 Color: 18
Size: 351083 Color: 6
Size: 278153 Color: 2

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 370765 Color: 4
Size: 361958 Color: 18
Size: 267278 Color: 14

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 370920 Color: 6
Size: 345758 Color: 8
Size: 283323 Color: 3

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 370969 Color: 10
Size: 364437 Color: 3
Size: 264595 Color: 19

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 370989 Color: 8
Size: 353488 Color: 18
Size: 275524 Color: 6

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 371057 Color: 1
Size: 362576 Color: 5
Size: 266368 Color: 2

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 371112 Color: 11
Size: 338110 Color: 1
Size: 290779 Color: 11

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 371144 Color: 14
Size: 351648 Color: 5
Size: 277209 Color: 16

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 371153 Color: 7
Size: 352343 Color: 15
Size: 276505 Color: 13

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 371164 Color: 17
Size: 324120 Color: 18
Size: 304717 Color: 2

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 371263 Color: 5
Size: 342193 Color: 19
Size: 286545 Color: 3

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 371274 Color: 15
Size: 369732 Color: 11
Size: 258995 Color: 3

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 371281 Color: 5
Size: 344490 Color: 11
Size: 284230 Color: 9

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 371315 Color: 8
Size: 326047 Color: 12
Size: 302639 Color: 10

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 371449 Color: 0
Size: 347960 Color: 16
Size: 280592 Color: 3

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 371352 Color: 9
Size: 330871 Color: 11
Size: 297778 Color: 9

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 371358 Color: 7
Size: 327110 Color: 19
Size: 301533 Color: 17

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 371385 Color: 3
Size: 348821 Color: 7
Size: 279795 Color: 15

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 371447 Color: 7
Size: 349462 Color: 1
Size: 279092 Color: 6

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 371481 Color: 13
Size: 360320 Color: 6
Size: 268200 Color: 16

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 371519 Color: 7
Size: 315845 Color: 18
Size: 312637 Color: 7

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 371599 Color: 15
Size: 322793 Color: 10
Size: 305609 Color: 6

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 371649 Color: 8
Size: 319881 Color: 9
Size: 308471 Color: 6

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 371630 Color: 16
Size: 358196 Color: 8
Size: 270175 Color: 17

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 371664 Color: 15
Size: 339410 Color: 3
Size: 288927 Color: 14

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 371742 Color: 2
Size: 348557 Color: 0
Size: 279702 Color: 9

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 371777 Color: 2
Size: 360704 Color: 18
Size: 267520 Color: 14

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 371810 Color: 2
Size: 358157 Color: 1
Size: 270034 Color: 4

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 371830 Color: 10
Size: 349872 Color: 8
Size: 278299 Color: 8

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 371870 Color: 17
Size: 358110 Color: 4
Size: 270021 Color: 11

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 371887 Color: 7
Size: 357972 Color: 4
Size: 270142 Color: 2

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 372115 Color: 2
Size: 343171 Color: 6
Size: 284715 Color: 18

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 372021 Color: 9
Size: 348873 Color: 11
Size: 279107 Color: 15

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 372035 Color: 1
Size: 330390 Color: 10
Size: 297576 Color: 6

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 372083 Color: 9
Size: 321807 Color: 8
Size: 306111 Color: 10

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 372211 Color: 2
Size: 348912 Color: 6
Size: 278878 Color: 0

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 372280 Color: 15
Size: 334115 Color: 17
Size: 293606 Color: 9

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 372338 Color: 1
Size: 314246 Color: 19
Size: 313417 Color: 9

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 372383 Color: 19
Size: 338727 Color: 13
Size: 288891 Color: 9

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 372531 Color: 18
Size: 352643 Color: 0
Size: 274827 Color: 9

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 372637 Color: 5
Size: 351368 Color: 11
Size: 275996 Color: 12

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 372708 Color: 9
Size: 370240 Color: 14
Size: 257053 Color: 17

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 372718 Color: 2
Size: 322530 Color: 16
Size: 304753 Color: 15

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 372753 Color: 12
Size: 368900 Color: 1
Size: 258348 Color: 8

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 372840 Color: 9
Size: 316157 Color: 19
Size: 311004 Color: 1

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 372865 Color: 8
Size: 349487 Color: 12
Size: 277649 Color: 4

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 372970 Color: 19
Size: 332170 Color: 12
Size: 294861 Color: 14

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 373008 Color: 9
Size: 357009 Color: 12
Size: 269984 Color: 14

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 373047 Color: 3
Size: 328345 Color: 1
Size: 298609 Color: 3

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 373066 Color: 15
Size: 329563 Color: 16
Size: 297372 Color: 6

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 373211 Color: 15
Size: 334056 Color: 6
Size: 292734 Color: 10

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 373299 Color: 14
Size: 325308 Color: 19
Size: 301394 Color: 6

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 373399 Color: 0
Size: 362822 Color: 13
Size: 263780 Color: 10

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 373310 Color: 15
Size: 360682 Color: 6
Size: 266009 Color: 4

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 373370 Color: 7
Size: 326667 Color: 18
Size: 299964 Color: 19

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 373416 Color: 6
Size: 334621 Color: 19
Size: 291964 Color: 1

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 373483 Color: 14
Size: 365527 Color: 12
Size: 260991 Color: 2

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 373500 Color: 11
Size: 346818 Color: 2
Size: 279683 Color: 1

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 373606 Color: 0
Size: 372652 Color: 14
Size: 253743 Color: 18

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 373510 Color: 12
Size: 349815 Color: 13
Size: 276676 Color: 4

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 373511 Color: 1
Size: 327637 Color: 7
Size: 298853 Color: 19

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 373607 Color: 4
Size: 351650 Color: 11
Size: 274744 Color: 11

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 373640 Color: 16
Size: 334278 Color: 8
Size: 292083 Color: 13

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 373769 Color: 15
Size: 339085 Color: 13
Size: 287147 Color: 5

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 373840 Color: 9
Size: 346574 Color: 8
Size: 279587 Color: 10

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 373871 Color: 10
Size: 325321 Color: 2
Size: 300809 Color: 17

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 373873 Color: 2
Size: 341116 Color: 8
Size: 285012 Color: 18

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 373975 Color: 16
Size: 360314 Color: 18
Size: 265712 Color: 12

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 374007 Color: 1
Size: 333459 Color: 3
Size: 292535 Color: 10

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 374036 Color: 19
Size: 329119 Color: 1
Size: 296846 Color: 19

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 374058 Color: 10
Size: 321270 Color: 19
Size: 304673 Color: 5

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 374130 Color: 16
Size: 370007 Color: 16
Size: 255864 Color: 15

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 374056 Color: 19
Size: 325194 Color: 4
Size: 300751 Color: 5

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 374292 Color: 18
Size: 370650 Color: 11
Size: 255059 Color: 0

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 374315 Color: 3
Size: 324073 Color: 8
Size: 301613 Color: 9

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 374340 Color: 12
Size: 334286 Color: 9
Size: 291375 Color: 6

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 374382 Color: 16
Size: 334571 Color: 1
Size: 291048 Color: 2

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 374417 Color: 15
Size: 372401 Color: 2
Size: 253183 Color: 2

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 374417 Color: 15
Size: 361119 Color: 15
Size: 264465 Color: 18

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 374473 Color: 12
Size: 346539 Color: 9
Size: 278989 Color: 17

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 374478 Color: 16
Size: 316728 Color: 14
Size: 308795 Color: 6

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 374510 Color: 15
Size: 337724 Color: 1
Size: 287767 Color: 7

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 374526 Color: 4
Size: 355004 Color: 8
Size: 270471 Color: 17

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 374555 Color: 17
Size: 320905 Color: 13
Size: 304541 Color: 7

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 374821 Color: 0
Size: 318420 Color: 13
Size: 306760 Color: 19

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 374560 Color: 12
Size: 315474 Color: 17
Size: 309967 Color: 5

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 374587 Color: 17
Size: 314897 Color: 9
Size: 310517 Color: 9

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 374665 Color: 10
Size: 324723 Color: 8
Size: 300613 Color: 5

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 374703 Color: 9
Size: 334482 Color: 14
Size: 290816 Color: 7

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 374752 Color: 19
Size: 330212 Color: 0
Size: 295037 Color: 4

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 374754 Color: 19
Size: 347819 Color: 4
Size: 277428 Color: 2

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 374771 Color: 8
Size: 340297 Color: 8
Size: 284933 Color: 15

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 374799 Color: 7
Size: 374785 Color: 5
Size: 250417 Color: 14

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 374802 Color: 10
Size: 331817 Color: 12
Size: 293382 Color: 4

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 374899 Color: 8
Size: 373757 Color: 11
Size: 251345 Color: 12

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 374929 Color: 13
Size: 340825 Color: 5
Size: 284247 Color: 16

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 374943 Color: 6
Size: 344912 Color: 10
Size: 280146 Color: 4

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 375016 Color: 7
Size: 339637 Color: 11
Size: 285348 Color: 3

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 375060 Color: 13
Size: 371396 Color: 14
Size: 253545 Color: 6

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 375081 Color: 5
Size: 357493 Color: 7
Size: 267427 Color: 16

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 375146 Color: 2
Size: 338951 Color: 19
Size: 285904 Color: 18

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 375165 Color: 14
Size: 326954 Color: 2
Size: 297882 Color: 5

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 375214 Color: 12
Size: 344929 Color: 7
Size: 279858 Color: 2

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 375177 Color: 19
Size: 318145 Color: 11
Size: 306679 Color: 14

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 375217 Color: 15
Size: 332203 Color: 4
Size: 292581 Color: 0

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 375287 Color: 18
Size: 373021 Color: 9
Size: 251693 Color: 1

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 375299 Color: 10
Size: 366603 Color: 3
Size: 258099 Color: 3

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 375242 Color: 19
Size: 336117 Color: 8
Size: 288642 Color: 8

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 375405 Color: 4
Size: 354128 Color: 13
Size: 270468 Color: 14

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 375430 Color: 3
Size: 337381 Color: 17
Size: 287190 Color: 1

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 375458 Color: 1
Size: 322946 Color: 5
Size: 301597 Color: 14

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 375499 Color: 9
Size: 358004 Color: 8
Size: 266498 Color: 16

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 375558 Color: 3
Size: 338693 Color: 13
Size: 285750 Color: 18

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 375579 Color: 7
Size: 342551 Color: 11
Size: 281871 Color: 17

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 375588 Color: 11
Size: 336844 Color: 6
Size: 287569 Color: 9

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 375605 Color: 13
Size: 313273 Color: 9
Size: 311123 Color: 15

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 375633 Color: 6
Size: 351612 Color: 19
Size: 272756 Color: 6

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 375636 Color: 6
Size: 360665 Color: 0
Size: 263700 Color: 7

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 375677 Color: 12
Size: 328071 Color: 18
Size: 296253 Color: 16

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 375695 Color: 6
Size: 333112 Color: 11
Size: 291194 Color: 16

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 375789 Color: 15
Size: 331726 Color: 8
Size: 292486 Color: 3

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 375805 Color: 10
Size: 320360 Color: 3
Size: 303836 Color: 19

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 375919 Color: 8
Size: 346741 Color: 12
Size: 277341 Color: 12

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 375981 Color: 14
Size: 337235 Color: 6
Size: 286785 Color: 6

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 375992 Color: 10
Size: 357997 Color: 7
Size: 266012 Color: 11

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 376073 Color: 5
Size: 316049 Color: 9
Size: 307879 Color: 0

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 376101 Color: 5
Size: 363566 Color: 13
Size: 260334 Color: 10

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 376122 Color: 18
Size: 369132 Color: 5
Size: 254747 Color: 15

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 376160 Color: 1
Size: 356227 Color: 6
Size: 267614 Color: 8

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 376240 Color: 12
Size: 314802 Color: 9
Size: 308959 Color: 2

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 376251 Color: 12
Size: 345211 Color: 19
Size: 278539 Color: 13

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 376262 Color: 2
Size: 322519 Color: 13
Size: 301220 Color: 11

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 376271 Color: 14
Size: 316059 Color: 9
Size: 307671 Color: 5

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 376290 Color: 2
Size: 328940 Color: 1
Size: 294771 Color: 9

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 376305 Color: 9
Size: 318609 Color: 15
Size: 305087 Color: 12

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 376311 Color: 3
Size: 327812 Color: 3
Size: 295878 Color: 18

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 376344 Color: 15
Size: 360335 Color: 18
Size: 263322 Color: 7

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 376412 Color: 6
Size: 316020 Color: 6
Size: 307569 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 376418 Color: 18
Size: 344210 Color: 19
Size: 279373 Color: 9

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 376444 Color: 7
Size: 329184 Color: 18
Size: 294373 Color: 17

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 376468 Color: 15
Size: 325102 Color: 7
Size: 298431 Color: 10

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 376472 Color: 18
Size: 339295 Color: 9
Size: 284234 Color: 9

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 376472 Color: 4
Size: 322684 Color: 13
Size: 300845 Color: 10

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 376599 Color: 15
Size: 322017 Color: 18
Size: 301385 Color: 9

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 376712 Color: 0
Size: 343237 Color: 14
Size: 280052 Color: 7

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 376615 Color: 3
Size: 320695 Color: 6
Size: 302691 Color: 15

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 376645 Color: 19
Size: 368081 Color: 5
Size: 255275 Color: 12

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 376943 Color: 10
Size: 372855 Color: 18
Size: 250203 Color: 16

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 376985 Color: 11
Size: 361147 Color: 17
Size: 261869 Color: 1

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 377024 Color: 5
Size: 372927 Color: 1
Size: 250050 Color: 11

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 377065 Color: 10
Size: 358989 Color: 15
Size: 263947 Color: 13

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 377096 Color: 5
Size: 368423 Color: 12
Size: 254482 Color: 7

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 376966 Color: 18
Size: 347041 Color: 6
Size: 275994 Color: 7

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 377099 Color: 2
Size: 340746 Color: 0
Size: 282156 Color: 6

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 377120 Color: 8
Size: 335524 Color: 4
Size: 287357 Color: 13

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 377192 Color: 2
Size: 365622 Color: 4
Size: 257187 Color: 9

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 377195 Color: 8
Size: 331791 Color: 6
Size: 291015 Color: 13

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 377197 Color: 12
Size: 361622 Color: 15
Size: 261182 Color: 6

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 377252 Color: 19
Size: 365780 Color: 9
Size: 256969 Color: 12

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 377257 Color: 5
Size: 342345 Color: 2
Size: 280399 Color: 17

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 377294 Color: 12
Size: 352366 Color: 5
Size: 270341 Color: 17

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 377300 Color: 11
Size: 330882 Color: 1
Size: 291819 Color: 17

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 377312 Color: 19
Size: 365262 Color: 8
Size: 257427 Color: 6

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 377427 Color: 7
Size: 333617 Color: 9
Size: 288957 Color: 7

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 377544 Color: 2
Size: 359879 Color: 3
Size: 262578 Color: 1

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 377552 Color: 0
Size: 332846 Color: 5
Size: 289603 Color: 7

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 377584 Color: 2
Size: 348188 Color: 13
Size: 274229 Color: 18

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 377620 Color: 0
Size: 323920 Color: 16
Size: 298461 Color: 9

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 377629 Color: 6
Size: 350155 Color: 0
Size: 272217 Color: 5

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 377680 Color: 16
Size: 337937 Color: 7
Size: 284384 Color: 13

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 377691 Color: 17
Size: 349927 Color: 5
Size: 272383 Color: 7

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 377692 Color: 14
Size: 333031 Color: 11
Size: 289278 Color: 13

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 377793 Color: 7
Size: 325008 Color: 8
Size: 297200 Color: 4

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 377847 Color: 9
Size: 354407 Color: 5
Size: 267747 Color: 8

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 377878 Color: 19
Size: 312610 Color: 6
Size: 309513 Color: 11

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 377950 Color: 13
Size: 356660 Color: 11
Size: 265391 Color: 18

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 377953 Color: 8
Size: 356657 Color: 6
Size: 265391 Color: 5

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 377959 Color: 3
Size: 351849 Color: 14
Size: 270193 Color: 3

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 378015 Color: 18
Size: 355055 Color: 17
Size: 266931 Color: 0

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 378025 Color: 8
Size: 360719 Color: 3
Size: 261257 Color: 16

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 378026 Color: 13
Size: 331696 Color: 6
Size: 290279 Color: 3

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 378094 Color: 15
Size: 337178 Color: 0
Size: 284729 Color: 17

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 378120 Color: 8
Size: 321695 Color: 14
Size: 300186 Color: 10

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 378294 Color: 2
Size: 337366 Color: 11
Size: 284341 Color: 1

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 378355 Color: 17
Size: 335303 Color: 10
Size: 286343 Color: 18

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 378394 Color: 6
Size: 361780 Color: 14
Size: 259827 Color: 2

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 378456 Color: 3
Size: 337219 Color: 2
Size: 284326 Color: 16

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 378528 Color: 16
Size: 344265 Color: 4
Size: 277208 Color: 11

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 378554 Color: 9
Size: 339964 Color: 18
Size: 281483 Color: 3

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 378739 Color: 0
Size: 338130 Color: 5
Size: 283132 Color: 2

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 378635 Color: 16
Size: 343423 Color: 7
Size: 277943 Color: 10

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 378651 Color: 14
Size: 335044 Color: 7
Size: 286306 Color: 1

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 378680 Color: 12
Size: 314131 Color: 9
Size: 307190 Color: 7

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 378717 Color: 3
Size: 322660 Color: 7
Size: 298624 Color: 10

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 378771 Color: 13
Size: 365692 Color: 7
Size: 255538 Color: 2

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 378815 Color: 14
Size: 312838 Color: 17
Size: 308348 Color: 4

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 378939 Color: 15
Size: 310925 Color: 16
Size: 310137 Color: 11

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 379014 Color: 12
Size: 314667 Color: 19
Size: 306320 Color: 14

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 379075 Color: 10
Size: 326302 Color: 16
Size: 294624 Color: 15

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 379125 Color: 18
Size: 347504 Color: 5
Size: 273372 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 379158 Color: 19
Size: 323568 Color: 6
Size: 297275 Color: 11

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 379210 Color: 4
Size: 330910 Color: 2
Size: 289881 Color: 9

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 379217 Color: 12
Size: 314259 Color: 12
Size: 306525 Color: 2

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 379277 Color: 3
Size: 326451 Color: 18
Size: 294273 Color: 7

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 379278 Color: 19
Size: 343210 Color: 9
Size: 277513 Color: 9

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 379312 Color: 10
Size: 356802 Color: 8
Size: 263887 Color: 7

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 379320 Color: 19
Size: 361562 Color: 8
Size: 259119 Color: 5

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 379330 Color: 10
Size: 340850 Color: 8
Size: 279821 Color: 19

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 379390 Color: 4
Size: 335295 Color: 16
Size: 285316 Color: 1

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 379397 Color: 14
Size: 358703 Color: 1
Size: 261901 Color: 16

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 379345 Color: 2
Size: 330879 Color: 12
Size: 289777 Color: 4

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 379416 Color: 13
Size: 330860 Color: 7
Size: 289725 Color: 14

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 379477 Color: 9
Size: 356768 Color: 11
Size: 263756 Color: 18

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 379500 Color: 6
Size: 314064 Color: 8
Size: 306437 Color: 11

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 379504 Color: 16
Size: 339629 Color: 7
Size: 280868 Color: 18

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 379680 Color: 14
Size: 364479 Color: 17
Size: 255842 Color: 12

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 379682 Color: 18
Size: 365761 Color: 11
Size: 254558 Color: 7

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 379701 Color: 10
Size: 311417 Color: 8
Size: 308883 Color: 9

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 379726 Color: 13
Size: 363983 Color: 11
Size: 256292 Color: 1

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 379740 Color: 13
Size: 325512 Color: 6
Size: 294749 Color: 6

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 379755 Color: 10
Size: 368461 Color: 15
Size: 251785 Color: 3

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 379842 Color: 10
Size: 328381 Color: 6
Size: 291778 Color: 4

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 379844 Color: 10
Size: 334148 Color: 12
Size: 286009 Color: 3

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 379869 Color: 5
Size: 361602 Color: 2
Size: 258530 Color: 2

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 379873 Color: 17
Size: 356880 Color: 18
Size: 263248 Color: 7

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 379911 Color: 9
Size: 353377 Color: 4
Size: 266713 Color: 6

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 379929 Color: 4
Size: 369644 Color: 11
Size: 250428 Color: 5

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 380049 Color: 19
Size: 320481 Color: 7
Size: 299471 Color: 1

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 380054 Color: 4
Size: 345326 Color: 16
Size: 274621 Color: 7

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 380055 Color: 10
Size: 322837 Color: 12
Size: 297109 Color: 2

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 380111 Color: 10
Size: 316928 Color: 0
Size: 302962 Color: 13

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 380138 Color: 16
Size: 345314 Color: 16
Size: 274549 Color: 7

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 380162 Color: 19
Size: 342269 Color: 14
Size: 277570 Color: 4

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 380223 Color: 18
Size: 350488 Color: 13
Size: 269290 Color: 1

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 380229 Color: 8
Size: 316095 Color: 1
Size: 303677 Color: 9

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 380284 Color: 3
Size: 328081 Color: 2
Size: 291636 Color: 17

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 380363 Color: 7
Size: 319922 Color: 17
Size: 299716 Color: 8

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 380442 Color: 9
Size: 368550 Color: 1
Size: 251009 Color: 6

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 380460 Color: 9
Size: 350875 Color: 13
Size: 268666 Color: 19

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 380519 Color: 7
Size: 329697 Color: 17
Size: 289785 Color: 4

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 380592 Color: 2
Size: 320090 Color: 12
Size: 299319 Color: 18

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 380554 Color: 12
Size: 327482 Color: 9
Size: 291965 Color: 3

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 380621 Color: 14
Size: 311629 Color: 19
Size: 307751 Color: 3

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 380642 Color: 4
Size: 325366 Color: 13
Size: 293993 Color: 5

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 380651 Color: 14
Size: 315544 Color: 19
Size: 303806 Color: 12

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 380718 Color: 5
Size: 342156 Color: 1
Size: 277127 Color: 19

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 380832 Color: 6
Size: 346667 Color: 6
Size: 272502 Color: 3

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 380838 Color: 1
Size: 327471 Color: 5
Size: 291692 Color: 3

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 380871 Color: 3
Size: 332456 Color: 4
Size: 286674 Color: 13

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 380881 Color: 15
Size: 322798 Color: 4
Size: 296322 Color: 5

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 380899 Color: 0
Size: 326196 Color: 2
Size: 292906 Color: 18

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 381004 Color: 19
Size: 336175 Color: 19
Size: 282822 Color: 14

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 381030 Color: 19
Size: 311148 Color: 9
Size: 307823 Color: 9

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 381066 Color: 6
Size: 318786 Color: 1
Size: 300149 Color: 19

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 381071 Color: 2
Size: 354994 Color: 5
Size: 263936 Color: 13

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 381149 Color: 5
Size: 346461 Color: 15
Size: 272391 Color: 2

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 381180 Color: 5
Size: 316978 Color: 4
Size: 301843 Color: 8

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 381184 Color: 16
Size: 358320 Color: 5
Size: 260497 Color: 15

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 381232 Color: 14
Size: 353530 Color: 10
Size: 265239 Color: 13

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 381269 Color: 8
Size: 345406 Color: 14
Size: 273326 Color: 9

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 381275 Color: 12
Size: 339496 Color: 17
Size: 279230 Color: 11

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 381284 Color: 18
Size: 332936 Color: 3
Size: 285781 Color: 2

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 381360 Color: 9
Size: 345882 Color: 11
Size: 272759 Color: 11

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 381434 Color: 8
Size: 349149 Color: 3
Size: 269418 Color: 19

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 381465 Color: 7
Size: 323531 Color: 7
Size: 295005 Color: 8

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 381471 Color: 5
Size: 353857 Color: 6
Size: 264673 Color: 16

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 381513 Color: 4
Size: 335010 Color: 8
Size: 283478 Color: 9

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 381634 Color: 8
Size: 317815 Color: 9
Size: 300552 Color: 14

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 381639 Color: 10
Size: 325222 Color: 1
Size: 293140 Color: 17

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 381662 Color: 4
Size: 320379 Color: 18
Size: 297960 Color: 12

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 381691 Color: 12
Size: 329587 Color: 3
Size: 288723 Color: 9

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 381710 Color: 13
Size: 309727 Color: 1
Size: 308564 Color: 17

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 381754 Color: 9
Size: 328025 Color: 0
Size: 290222 Color: 11

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 381775 Color: 13
Size: 314376 Color: 10
Size: 303850 Color: 10

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 381777 Color: 15
Size: 323962 Color: 19
Size: 294262 Color: 13

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 381796 Color: 18
Size: 350105 Color: 1
Size: 268100 Color: 6

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 381891 Color: 12
Size: 350654 Color: 4
Size: 267456 Color: 10

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 382002 Color: 19
Size: 355376 Color: 15
Size: 262623 Color: 8

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 382065 Color: 17
Size: 343786 Color: 4
Size: 274150 Color: 1

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 382180 Color: 13
Size: 342363 Color: 6
Size: 275458 Color: 8

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 382183 Color: 3
Size: 318282 Color: 16
Size: 299536 Color: 11

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 382218 Color: 1
Size: 349117 Color: 17
Size: 268666 Color: 17

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 382258 Color: 3
Size: 311779 Color: 17
Size: 305964 Color: 13

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 382290 Color: 5
Size: 351443 Color: 4
Size: 266268 Color: 14

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 382298 Color: 11
Size: 348888 Color: 14
Size: 268815 Color: 17

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 382337 Color: 15
Size: 362343 Color: 8
Size: 255321 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 382337 Color: 15
Size: 357311 Color: 1
Size: 260353 Color: 5

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 382389 Color: 11
Size: 355240 Color: 3
Size: 262372 Color: 5

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 382607 Color: 2
Size: 345098 Color: 10
Size: 272296 Color: 3

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 382469 Color: 13
Size: 354390 Color: 10
Size: 263142 Color: 15

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 382500 Color: 7
Size: 361083 Color: 8
Size: 256418 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 382510 Color: 12
Size: 344773 Color: 0
Size: 272718 Color: 10

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 382550 Color: 17
Size: 315743 Color: 1
Size: 301708 Color: 8

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 382611 Color: 14
Size: 358365 Color: 1
Size: 259025 Color: 6

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 382645 Color: 15
Size: 319429 Color: 0
Size: 297927 Color: 4

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 382655 Color: 9
Size: 316520 Color: 11
Size: 300826 Color: 17

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 382656 Color: 3
Size: 361241 Color: 19
Size: 256104 Color: 8

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 382716 Color: 12
Size: 354272 Color: 9
Size: 263013 Color: 15

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 382762 Color: 18
Size: 316257 Color: 19
Size: 300982 Color: 5

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 382885 Color: 1
Size: 333263 Color: 19
Size: 283853 Color: 3

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 382917 Color: 19
Size: 331939 Color: 0
Size: 285145 Color: 15

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 382920 Color: 9
Size: 360007 Color: 6
Size: 257074 Color: 19

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 382934 Color: 6
Size: 332039 Color: 5
Size: 285028 Color: 14

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 382964 Color: 14
Size: 335418 Color: 9
Size: 281619 Color: 2

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 382985 Color: 18
Size: 352767 Color: 9
Size: 264249 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 383030 Color: 5
Size: 311286 Color: 14
Size: 305685 Color: 10

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 383070 Color: 16
Size: 361505 Color: 10
Size: 255426 Color: 3

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 383079 Color: 18
Size: 341058 Color: 17
Size: 275864 Color: 12

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 383092 Color: 14
Size: 315661 Color: 10
Size: 301248 Color: 2

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 383148 Color: 9
Size: 356706 Color: 9
Size: 260147 Color: 19

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 383156 Color: 12
Size: 326338 Color: 5
Size: 290507 Color: 10

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 383253 Color: 3
Size: 322702 Color: 19
Size: 294046 Color: 10

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 383254 Color: 16
Size: 325184 Color: 15
Size: 291563 Color: 12

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 383268 Color: 6
Size: 311287 Color: 11
Size: 305446 Color: 12

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 383328 Color: 3
Size: 329236 Color: 0
Size: 287437 Color: 2

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 383416 Color: 15
Size: 325538 Color: 0
Size: 291047 Color: 14

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 383430 Color: 11
Size: 351031 Color: 14
Size: 265540 Color: 4

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 383433 Color: 12
Size: 331340 Color: 4
Size: 285228 Color: 0

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 383436 Color: 18
Size: 345178 Color: 17
Size: 271387 Color: 3

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 383453 Color: 2
Size: 337541 Color: 4
Size: 279007 Color: 11

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 383501 Color: 6
Size: 330004 Color: 14
Size: 286496 Color: 9

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 383508 Color: 6
Size: 358986 Color: 8
Size: 257507 Color: 13

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 383519 Color: 10
Size: 340173 Color: 16
Size: 276309 Color: 19

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 383584 Color: 11
Size: 337293 Color: 1
Size: 279124 Color: 13

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 383758 Color: 19
Size: 346795 Color: 18
Size: 269448 Color: 14

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 383854 Color: 11
Size: 330598 Color: 2
Size: 285549 Color: 12

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 383937 Color: 15
Size: 312684 Color: 15
Size: 303380 Color: 0

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 383972 Color: 19
Size: 311802 Color: 13
Size: 304227 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 384009 Color: 1
Size: 334825 Color: 7
Size: 281167 Color: 15

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 384053 Color: 17
Size: 355782 Color: 14
Size: 260166 Color: 1

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 384077 Color: 4
Size: 353020 Color: 9
Size: 262904 Color: 14

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 384093 Color: 4
Size: 323070 Color: 0
Size: 292838 Color: 6

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 384121 Color: 13
Size: 309349 Color: 5
Size: 306531 Color: 18

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 384122 Color: 4
Size: 326646 Color: 9
Size: 289233 Color: 9

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 384187 Color: 12
Size: 353324 Color: 14
Size: 262490 Color: 10

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 384194 Color: 13
Size: 312950 Color: 18
Size: 302857 Color: 16

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 384222 Color: 12
Size: 326670 Color: 18
Size: 289109 Color: 18

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 384281 Color: 10
Size: 353946 Color: 12
Size: 261774 Color: 8

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 384284 Color: 10
Size: 331086 Color: 6
Size: 284631 Color: 12

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 384355 Color: 16
Size: 312632 Color: 8
Size: 303014 Color: 3

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 384365 Color: 7
Size: 340676 Color: 12
Size: 274960 Color: 1

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 384529 Color: 17
Size: 318786 Color: 17
Size: 296686 Color: 2

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 384575 Color: 19
Size: 324700 Color: 17
Size: 290726 Color: 3

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 384587 Color: 9
Size: 328306 Color: 18
Size: 287108 Color: 12

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 384596 Color: 14
Size: 358185 Color: 15
Size: 257220 Color: 13

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 384689 Color: 13
Size: 313382 Color: 4
Size: 301930 Color: 13

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 384708 Color: 14
Size: 339520 Color: 11
Size: 275773 Color: 19

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 384793 Color: 2
Size: 348552 Color: 10
Size: 266656 Color: 6

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 384804 Color: 8
Size: 316038 Color: 4
Size: 299159 Color: 12

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 384846 Color: 16
Size: 355921 Color: 15
Size: 259234 Color: 12

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 384915 Color: 17
Size: 337507 Color: 6
Size: 277579 Color: 7

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 384921 Color: 6
Size: 333573 Color: 5
Size: 281507 Color: 12

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 384984 Color: 17
Size: 358338 Color: 19
Size: 256679 Color: 1

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 385009 Color: 10
Size: 320766 Color: 2
Size: 294226 Color: 16

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 385021 Color: 14
Size: 330345 Color: 4
Size: 284635 Color: 13

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 385055 Color: 19
Size: 350097 Color: 0
Size: 264849 Color: 0

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 385083 Color: 12
Size: 354766 Color: 4
Size: 260152 Color: 1

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 385121 Color: 13
Size: 314738 Color: 19
Size: 300142 Color: 9

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 385149 Color: 13
Size: 364375 Color: 18
Size: 250477 Color: 15

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 385161 Color: 15
Size: 328573 Color: 2
Size: 286267 Color: 15

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 385166 Color: 13
Size: 346045 Color: 19
Size: 268790 Color: 16

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 385215 Color: 12
Size: 323001 Color: 2
Size: 291785 Color: 3

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 385316 Color: 18
Size: 328617 Color: 12
Size: 286068 Color: 9

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 385356 Color: 8
Size: 329310 Color: 15
Size: 285335 Color: 8

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 385378 Color: 9
Size: 319633 Color: 17
Size: 294990 Color: 4

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 385391 Color: 1
Size: 345499 Color: 2
Size: 269111 Color: 11

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 385409 Color: 3
Size: 339630 Color: 13
Size: 274962 Color: 15

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 385444 Color: 3
Size: 314575 Color: 17
Size: 299982 Color: 17

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 385474 Color: 11
Size: 342246 Color: 9
Size: 272281 Color: 4

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 385524 Color: 16
Size: 328512 Color: 10
Size: 285965 Color: 3

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 385726 Color: 17
Size: 313394 Color: 18
Size: 300881 Color: 17

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 385754 Color: 18
Size: 332117 Color: 1
Size: 282130 Color: 2

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 385809 Color: 9
Size: 308852 Color: 6
Size: 305340 Color: 18

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 385901 Color: 4
Size: 332130 Color: 5
Size: 281970 Color: 5

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 385905 Color: 12
Size: 345985 Color: 14
Size: 268111 Color: 6

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 385908 Color: 3
Size: 345680 Color: 1
Size: 268413 Color: 10

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 385928 Color: 18
Size: 335910 Color: 15
Size: 278163 Color: 8

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 386245 Color: 2
Size: 318440 Color: 18
Size: 295316 Color: 10

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 385937 Color: 11
Size: 315841 Color: 4
Size: 298223 Color: 14

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 385991 Color: 14
Size: 354490 Color: 9
Size: 259520 Color: 13

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 386038 Color: 6
Size: 359746 Color: 17
Size: 254217 Color: 4

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 386092 Color: 14
Size: 334167 Color: 19
Size: 279742 Color: 1

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 386284 Color: 2
Size: 361160 Color: 14
Size: 252557 Color: 11

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 386137 Color: 1
Size: 317068 Color: 11
Size: 296796 Color: 17

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 386233 Color: 7
Size: 357504 Color: 8
Size: 256264 Color: 13

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 386244 Color: 6
Size: 315070 Color: 14
Size: 298687 Color: 19

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 386308 Color: 0
Size: 363361 Color: 2
Size: 250332 Color: 19

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 386371 Color: 16
Size: 361769 Color: 19
Size: 251861 Color: 17

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 386399 Color: 9
Size: 361485 Color: 17
Size: 252117 Color: 9

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 386407 Color: 9
Size: 307405 Color: 14
Size: 306189 Color: 16

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 386435 Color: 6
Size: 343589 Color: 13
Size: 269977 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 386435 Color: 4
Size: 327135 Color: 5
Size: 286431 Color: 3

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 386494 Color: 2
Size: 329560 Color: 0
Size: 283947 Color: 9

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 386452 Color: 19
Size: 325619 Color: 7
Size: 287930 Color: 19

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 386462 Color: 0
Size: 342731 Color: 5
Size: 270808 Color: 8

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 386516 Color: 4
Size: 310910 Color: 11
Size: 302575 Color: 4

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 386634 Color: 6
Size: 313087 Color: 8
Size: 300280 Color: 5

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 386755 Color: 2
Size: 362094 Color: 0
Size: 251152 Color: 8

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 386699 Color: 6
Size: 347858 Color: 4
Size: 265444 Color: 5

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 386770 Color: 9
Size: 347919 Color: 5
Size: 265312 Color: 0

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 386792 Color: 14
Size: 339179 Color: 8
Size: 274030 Color: 3

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 386814 Color: 8
Size: 310618 Color: 12
Size: 302569 Color: 4

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 386820 Color: 16
Size: 359516 Color: 13
Size: 253665 Color: 19

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 386862 Color: 6
Size: 350730 Color: 7
Size: 262409 Color: 2

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 386897 Color: 11
Size: 310607 Color: 3
Size: 302497 Color: 14

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 386969 Color: 16
Size: 327480 Color: 5
Size: 285552 Color: 3

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 387012 Color: 3
Size: 342056 Color: 11
Size: 270933 Color: 15

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 387027 Color: 5
Size: 326785 Color: 17
Size: 286189 Color: 0

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 387066 Color: 7
Size: 328016 Color: 8
Size: 284919 Color: 10

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 387233 Color: 15
Size: 332146 Color: 11
Size: 280622 Color: 7

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 387239 Color: 17
Size: 345531 Color: 16
Size: 267231 Color: 12

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 387290 Color: 12
Size: 315452 Color: 9
Size: 297259 Color: 14

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 387393 Color: 17
Size: 309208 Color: 16
Size: 303400 Color: 11

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 387454 Color: 3
Size: 350216 Color: 5
Size: 262331 Color: 13

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 387488 Color: 11
Size: 319404 Color: 5
Size: 293109 Color: 4

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 387501 Color: 0
Size: 307822 Color: 17
Size: 304678 Color: 6

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 387518 Color: 18
Size: 326656 Color: 13
Size: 285827 Color: 10

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 387532 Color: 8
Size: 360456 Color: 13
Size: 252013 Color: 13

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 387595 Color: 17
Size: 320323 Color: 5
Size: 292083 Color: 13

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 387624 Color: 9
Size: 313243 Color: 15
Size: 299134 Color: 2

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 387633 Color: 11
Size: 310064 Color: 8
Size: 302304 Color: 18

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 387663 Color: 8
Size: 357472 Color: 16
Size: 254866 Color: 8

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 387665 Color: 3
Size: 355280 Color: 0
Size: 257056 Color: 0

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 387702 Color: 12
Size: 309390 Color: 9
Size: 302909 Color: 16

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 387747 Color: 18
Size: 333210 Color: 11
Size: 279044 Color: 14

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 387770 Color: 8
Size: 315156 Color: 18
Size: 297075 Color: 6

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 387773 Color: 10
Size: 358789 Color: 16
Size: 253439 Color: 0

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 387815 Color: 2
Size: 310865 Color: 6
Size: 301321 Color: 9

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 387784 Color: 14
Size: 329341 Color: 6
Size: 282876 Color: 5

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 387797 Color: 17
Size: 347946 Color: 19
Size: 264258 Color: 12

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 387857 Color: 15
Size: 353875 Color: 6
Size: 258269 Color: 17

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 387931 Color: 0
Size: 347080 Color: 2
Size: 264990 Color: 1

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 388096 Color: 19
Size: 342478 Color: 12
Size: 269427 Color: 16

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 388110 Color: 0
Size: 349336 Color: 12
Size: 262555 Color: 17

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 388311 Color: 0
Size: 311510 Color: 0
Size: 300180 Color: 8

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 388359 Color: 15
Size: 343695 Color: 9
Size: 267947 Color: 12

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 388611 Color: 2
Size: 351085 Color: 4
Size: 260305 Color: 8

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 388476 Color: 16
Size: 334351 Color: 19
Size: 277174 Color: 4

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 388477 Color: 0
Size: 305865 Color: 15
Size: 305659 Color: 2

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 388496 Color: 1
Size: 323931 Color: 8
Size: 287574 Color: 10

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 388561 Color: 8
Size: 342466 Color: 16
Size: 268974 Color: 11

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 388599 Color: 16
Size: 310205 Color: 16
Size: 301197 Color: 3

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 388630 Color: 18
Size: 309214 Color: 13
Size: 302157 Color: 8

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 388711 Color: 18
Size: 311250 Color: 9
Size: 300040 Color: 14

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 388740 Color: 17
Size: 326874 Color: 6
Size: 284387 Color: 11

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 388819 Color: 11
Size: 334169 Color: 7
Size: 277013 Color: 15

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 388863 Color: 14
Size: 343253 Color: 18
Size: 267885 Color: 0

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 388881 Color: 7
Size: 343987 Color: 9
Size: 267133 Color: 16

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 389006 Color: 13
Size: 324928 Color: 18
Size: 286067 Color: 12

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 388982 Color: 9
Size: 305713 Color: 10
Size: 305306 Color: 19

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 389094 Color: 8
Size: 321740 Color: 10
Size: 289167 Color: 2

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 389121 Color: 15
Size: 336816 Color: 14
Size: 274064 Color: 13

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 389146 Color: 1
Size: 342046 Color: 16
Size: 268809 Color: 17

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 389148 Color: 14
Size: 342520 Color: 8
Size: 268333 Color: 17

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 389234 Color: 13
Size: 328453 Color: 8
Size: 282314 Color: 9

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 389258 Color: 10
Size: 316552 Color: 2
Size: 294191 Color: 15

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 389281 Color: 15
Size: 355825 Color: 18
Size: 254895 Color: 8

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 389292 Color: 14
Size: 309041 Color: 19
Size: 301668 Color: 15

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 389299 Color: 0
Size: 333101 Color: 13
Size: 277601 Color: 19

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 389326 Color: 3
Size: 317127 Color: 1
Size: 293548 Color: 19

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 389332 Color: 19
Size: 322540 Color: 10
Size: 288129 Color: 5

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 389478 Color: 8
Size: 350633 Color: 13
Size: 259890 Color: 19

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 389480 Color: 19
Size: 336102 Color: 18
Size: 274419 Color: 12

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 389485 Color: 18
Size: 308884 Color: 5
Size: 301632 Color: 17

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 389505 Color: 7
Size: 357402 Color: 3
Size: 253094 Color: 10

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 389556 Color: 2
Size: 339195 Color: 9
Size: 271250 Color: 11

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 389556 Color: 15
Size: 308490 Color: 1
Size: 301955 Color: 13

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 389600 Color: 0
Size: 326365 Color: 7
Size: 284036 Color: 15

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 389667 Color: 18
Size: 344347 Color: 5
Size: 265987 Color: 14

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 389710 Color: 12
Size: 344021 Color: 8
Size: 266270 Color: 19

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 389725 Color: 5
Size: 333347 Color: 9
Size: 276929 Color: 2

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 389795 Color: 13
Size: 313656 Color: 19
Size: 296550 Color: 2

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 389869 Color: 14
Size: 357372 Color: 18
Size: 252760 Color: 5

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 389915 Color: 11
Size: 309852 Color: 19
Size: 300234 Color: 15

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 389972 Color: 4
Size: 355360 Color: 1
Size: 254669 Color: 14

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 389974 Color: 15
Size: 315653 Color: 9
Size: 294374 Color: 17

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 389992 Color: 12
Size: 350765 Color: 10
Size: 259244 Color: 8

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 390008 Color: 10
Size: 316983 Color: 19
Size: 293010 Color: 14

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 390035 Color: 17
Size: 306049 Color: 13
Size: 303917 Color: 10

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 390049 Color: 14
Size: 349101 Color: 7
Size: 260851 Color: 18

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 390054 Color: 17
Size: 331784 Color: 15
Size: 278163 Color: 14

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 390105 Color: 18
Size: 342837 Color: 18
Size: 267059 Color: 17

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 390055 Color: 14
Size: 305725 Color: 3
Size: 304221 Color: 8

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 390338 Color: 2
Size: 341030 Color: 11
Size: 268633 Color: 3

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 390153 Color: 4
Size: 343588 Color: 12
Size: 266260 Color: 5

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 390320 Color: 9
Size: 329714 Color: 5
Size: 279967 Color: 1

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 390338 Color: 3
Size: 305809 Color: 15
Size: 303854 Color: 8

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 390435 Color: 17
Size: 355258 Color: 4
Size: 254308 Color: 11

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 390484 Color: 4
Size: 339932 Color: 10
Size: 269585 Color: 0

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 390544 Color: 1
Size: 323210 Color: 4
Size: 286247 Color: 17

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 390552 Color: 17
Size: 322388 Color: 8
Size: 287061 Color: 10

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 390737 Color: 2
Size: 309638 Color: 17
Size: 299626 Color: 9

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 390737 Color: 2
Size: 356280 Color: 7
Size: 252984 Color: 18

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 390564 Color: 11
Size: 350103 Color: 18
Size: 259334 Color: 6

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 390698 Color: 10
Size: 340251 Color: 15
Size: 269052 Color: 19

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 390727 Color: 10
Size: 325854 Color: 5
Size: 283420 Color: 13

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 390733 Color: 12
Size: 341383 Color: 14
Size: 267885 Color: 13

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 390820 Color: 2
Size: 321695 Color: 10
Size: 287486 Color: 16

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 390782 Color: 15
Size: 311332 Color: 8
Size: 297887 Color: 17

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 390802 Color: 14
Size: 342913 Color: 8
Size: 266286 Color: 17

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 390814 Color: 8
Size: 329608 Color: 13
Size: 279579 Color: 5

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 390933 Color: 15
Size: 337793 Color: 0
Size: 271275 Color: 10

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 390946 Color: 19
Size: 317421 Color: 17
Size: 291634 Color: 18

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 391003 Color: 18
Size: 338509 Color: 2
Size: 270489 Color: 12

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 391008 Color: 11
Size: 340440 Color: 14
Size: 268553 Color: 8

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 391010 Color: 0
Size: 306686 Color: 5
Size: 302305 Color: 12

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 391050 Color: 13
Size: 339260 Color: 12
Size: 269691 Color: 6

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 391060 Color: 10
Size: 346085 Color: 12
Size: 262856 Color: 18

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 391073 Color: 7
Size: 346824 Color: 13
Size: 262104 Color: 2

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 391076 Color: 13
Size: 327342 Color: 14
Size: 281583 Color: 6

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 391077 Color: 1
Size: 326979 Color: 9
Size: 281945 Color: 16

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 391147 Color: 19
Size: 322548 Color: 3
Size: 286306 Color: 19

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 391172 Color: 16
Size: 337084 Color: 9
Size: 271745 Color: 5

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 391195 Color: 2
Size: 314878 Color: 17
Size: 293928 Color: 18

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 391174 Color: 13
Size: 345663 Color: 3
Size: 263164 Color: 7

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 391184 Color: 15
Size: 345363 Color: 4
Size: 263454 Color: 17

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 391291 Color: 16
Size: 343633 Color: 12
Size: 265077 Color: 17

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 391312 Color: 7
Size: 311245 Color: 19
Size: 297444 Color: 3

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 391371 Color: 8
Size: 345040 Color: 1
Size: 263590 Color: 9

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 391393 Color: 7
Size: 350883 Color: 15
Size: 257725 Color: 11

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 391396 Color: 3
Size: 307616 Color: 0
Size: 300989 Color: 6

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 391421 Color: 15
Size: 319223 Color: 16
Size: 289357 Color: 8

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 391500 Color: 0
Size: 325467 Color: 18
Size: 283034 Color: 6

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 391538 Color: 3
Size: 354163 Color: 2
Size: 254300 Color: 3

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 391672 Color: 17
Size: 347326 Color: 0
Size: 261003 Color: 17

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 391711 Color: 8
Size: 346498 Color: 6
Size: 261792 Color: 6

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 391733 Color: 16
Size: 358126 Color: 17
Size: 250142 Color: 6

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 391847 Color: 16
Size: 335644 Color: 9
Size: 272510 Color: 4

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 391867 Color: 2
Size: 337514 Color: 1
Size: 270620 Color: 9

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 391901 Color: 11
Size: 308084 Color: 10
Size: 300016 Color: 3

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 391920 Color: 1
Size: 344072 Color: 3
Size: 264009 Color: 15

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 391934 Color: 16
Size: 336267 Color: 6
Size: 271800 Color: 1

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 392052 Color: 3
Size: 313316 Color: 2
Size: 294633 Color: 12

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 392103 Color: 5
Size: 308835 Color: 9
Size: 299063 Color: 15

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 392155 Color: 5
Size: 334580 Color: 4
Size: 273266 Color: 2

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 392193 Color: 8
Size: 327235 Color: 15
Size: 280573 Color: 10

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 392222 Color: 0
Size: 316305 Color: 12
Size: 291474 Color: 14

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 392327 Color: 9
Size: 314068 Color: 7
Size: 293606 Color: 17

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 392328 Color: 3
Size: 341446 Color: 17
Size: 266227 Color: 7

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 392385 Color: 19
Size: 314940 Color: 1
Size: 292676 Color: 12

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 392422 Color: 13
Size: 309454 Color: 5
Size: 298125 Color: 2

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 392428 Color: 0
Size: 314461 Color: 12
Size: 293112 Color: 17

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 392441 Color: 15
Size: 328314 Color: 19
Size: 279246 Color: 12

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 392455 Color: 6
Size: 348829 Color: 12
Size: 258717 Color: 18

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 392479 Color: 11
Size: 310012 Color: 18
Size: 297510 Color: 5

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 392486 Color: 16
Size: 309987 Color: 1
Size: 297528 Color: 17

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 392511 Color: 17
Size: 343608 Color: 6
Size: 263882 Color: 2

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 392515 Color: 7
Size: 339127 Color: 15
Size: 268359 Color: 12

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 392540 Color: 8
Size: 346021 Color: 18
Size: 261440 Color: 17

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 392586 Color: 16
Size: 330737 Color: 17
Size: 276678 Color: 4

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 392599 Color: 3
Size: 310130 Color: 16
Size: 297272 Color: 14

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 392724 Color: 9
Size: 349839 Color: 14
Size: 257438 Color: 19

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 392779 Color: 7
Size: 320147 Color: 6
Size: 287075 Color: 2

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 392799 Color: 16
Size: 341896 Color: 9
Size: 265306 Color: 5

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 392833 Color: 16
Size: 323424 Color: 8
Size: 283744 Color: 19

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 392854 Color: 8
Size: 336693 Color: 7
Size: 270454 Color: 18

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 392912 Color: 2
Size: 326148 Color: 3
Size: 280941 Color: 10

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 392992 Color: 14
Size: 324950 Color: 18
Size: 282059 Color: 5

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 392997 Color: 4
Size: 333296 Color: 8
Size: 273708 Color: 17

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 393006 Color: 19
Size: 315931 Color: 4
Size: 291064 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 393149 Color: 14
Size: 348551 Color: 2
Size: 258301 Color: 19

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 393189 Color: 6
Size: 328984 Color: 19
Size: 277828 Color: 12

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 393189 Color: 0
Size: 317902 Color: 17
Size: 288910 Color: 19

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 393215 Color: 16
Size: 304174 Color: 6
Size: 302612 Color: 6

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 393218 Color: 3
Size: 339122 Color: 0
Size: 267661 Color: 1

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 393232 Color: 7
Size: 325791 Color: 18
Size: 280978 Color: 17

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 393255 Color: 12
Size: 348524 Color: 4
Size: 258222 Color: 8

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 393261 Color: 3
Size: 310054 Color: 7
Size: 296686 Color: 13

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 393292 Color: 13
Size: 307467 Color: 16
Size: 299242 Color: 15

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 393339 Color: 19
Size: 305165 Color: 5
Size: 301497 Color: 1

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 393344 Color: 17
Size: 347323 Color: 3
Size: 259334 Color: 3

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 393349 Color: 14
Size: 314745 Color: 2
Size: 291907 Color: 17

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 393395 Color: 5
Size: 340368 Color: 0
Size: 266238 Color: 7

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 393402 Color: 3
Size: 345271 Color: 6
Size: 261328 Color: 13

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 393404 Color: 9
Size: 349306 Color: 4
Size: 257291 Color: 15

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 393460 Color: 5
Size: 312393 Color: 16
Size: 294148 Color: 3

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 393461 Color: 17
Size: 334837 Color: 9
Size: 271703 Color: 15

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 393494 Color: 4
Size: 340665 Color: 5
Size: 265842 Color: 13

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 393563 Color: 7
Size: 314117 Color: 0
Size: 292321 Color: 19

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 393630 Color: 7
Size: 315300 Color: 15
Size: 291071 Color: 13

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 393658 Color: 4
Size: 313138 Color: 5
Size: 293205 Color: 15

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 393676 Color: 5
Size: 342712 Color: 1
Size: 263613 Color: 3

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 393721 Color: 14
Size: 352309 Color: 3
Size: 253971 Color: 18

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 393772 Color: 11
Size: 325654 Color: 2
Size: 280575 Color: 3

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 393892 Color: 8
Size: 332739 Color: 0
Size: 273370 Color: 11

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 394018 Color: 10
Size: 325699 Color: 5
Size: 280284 Color: 9

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 394041 Color: 8
Size: 333878 Color: 19
Size: 272082 Color: 19

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 394060 Color: 19
Size: 351498 Color: 17
Size: 254443 Color: 8

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 394072 Color: 7
Size: 318817 Color: 3
Size: 287112 Color: 4

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 394082 Color: 17
Size: 322019 Color: 19
Size: 283900 Color: 2

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 394103 Color: 1
Size: 310964 Color: 13
Size: 294934 Color: 11

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 394114 Color: 12
Size: 339262 Color: 16
Size: 266625 Color: 10

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 394188 Color: 4
Size: 337606 Color: 0
Size: 268207 Color: 12

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 394204 Color: 3
Size: 329279 Color: 11
Size: 276518 Color: 19

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 394298 Color: 17
Size: 318981 Color: 18
Size: 286722 Color: 7

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 394304 Color: 9
Size: 352009 Color: 2
Size: 253688 Color: 4

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 394320 Color: 11
Size: 344784 Color: 15
Size: 260897 Color: 4

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 394330 Color: 19
Size: 307391 Color: 4
Size: 298280 Color: 3

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 394332 Color: 19
Size: 329485 Color: 7
Size: 276184 Color: 3

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 394486 Color: 9
Size: 342112 Color: 6
Size: 263403 Color: 5

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 394561 Color: 13
Size: 325784 Color: 0
Size: 279656 Color: 15

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 394599 Color: 15
Size: 311051 Color: 1
Size: 294351 Color: 1

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 394714 Color: 2
Size: 325070 Color: 9
Size: 280217 Color: 11

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 394680 Color: 7
Size: 320092 Color: 10
Size: 285229 Color: 11

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 394698 Color: 16
Size: 343946 Color: 3
Size: 261357 Color: 14

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 394747 Color: 10
Size: 309857 Color: 19
Size: 295397 Color: 6

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 394806 Color: 16
Size: 303680 Color: 4
Size: 301515 Color: 13

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 394838 Color: 9
Size: 313916 Color: 8
Size: 291247 Color: 2

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 394859 Color: 7
Size: 349822 Color: 13
Size: 255320 Color: 3

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 394861 Color: 18
Size: 348656 Color: 11
Size: 256484 Color: 19

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 394863 Color: 9
Size: 325149 Color: 1
Size: 279989 Color: 3

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 394877 Color: 9
Size: 326135 Color: 6
Size: 278989 Color: 19

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 394905 Color: 1
Size: 331503 Color: 6
Size: 273593 Color: 5

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 394961 Color: 6
Size: 342431 Color: 15
Size: 262609 Color: 1

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 395047 Color: 12
Size: 303965 Color: 0
Size: 300989 Color: 15

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 395053 Color: 0
Size: 323721 Color: 16
Size: 281227 Color: 19

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 395094 Color: 12
Size: 343201 Color: 3
Size: 261706 Color: 10

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 395101 Color: 8
Size: 314913 Color: 13
Size: 289987 Color: 12

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 395123 Color: 6
Size: 348649 Color: 2
Size: 256229 Color: 12

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 395138 Color: 17
Size: 305091 Color: 18
Size: 299772 Color: 1

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 395242 Color: 14
Size: 336009 Color: 0
Size: 268750 Color: 14

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 395273 Color: 9
Size: 340213 Color: 16
Size: 264515 Color: 19

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 395297 Color: 15
Size: 319904 Color: 6
Size: 284800 Color: 4

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 395353 Color: 16
Size: 328917 Color: 12
Size: 275731 Color: 6

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 395408 Color: 5
Size: 305454 Color: 19
Size: 299139 Color: 7

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 395446 Color: 12
Size: 330292 Color: 10
Size: 274263 Color: 1

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 395653 Color: 2
Size: 349689 Color: 16
Size: 254659 Color: 5

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 395451 Color: 15
Size: 310781 Color: 7
Size: 293769 Color: 11

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 395480 Color: 10
Size: 342556 Color: 1
Size: 261965 Color: 1

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 395482 Color: 6
Size: 336236 Color: 8
Size: 268283 Color: 11

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 395508 Color: 1
Size: 335778 Color: 17
Size: 268715 Color: 6

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 395698 Color: 2
Size: 326962 Color: 9
Size: 277341 Color: 10

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 395560 Color: 1
Size: 316288 Color: 4
Size: 288153 Color: 16

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 395648 Color: 16
Size: 311471 Color: 4
Size: 292882 Color: 1

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 395657 Color: 17
Size: 342259 Color: 12
Size: 262085 Color: 6

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 395681 Color: 12
Size: 325440 Color: 1
Size: 278880 Color: 15

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 395716 Color: 11
Size: 325901 Color: 1
Size: 278384 Color: 12

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 395762 Color: 10
Size: 327929 Color: 12
Size: 276310 Color: 14

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 395777 Color: 14
Size: 345541 Color: 18
Size: 258683 Color: 4

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 395896 Color: 2
Size: 349209 Color: 19
Size: 254896 Color: 14

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 395896 Color: 18
Size: 320400 Color: 6
Size: 283705 Color: 16

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 395957 Color: 5
Size: 313613 Color: 7
Size: 290431 Color: 13

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 395961 Color: 5
Size: 313518 Color: 14
Size: 290522 Color: 7

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 395999 Color: 12
Size: 337023 Color: 15
Size: 266979 Color: 0

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 396007 Color: 16
Size: 309193 Color: 18
Size: 294801 Color: 12

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 396162 Color: 2
Size: 304206 Color: 18
Size: 299633 Color: 17

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 396053 Color: 17
Size: 346302 Color: 13
Size: 257646 Color: 19

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 396076 Color: 15
Size: 319784 Color: 14
Size: 284141 Color: 9

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 396211 Color: 15
Size: 310454 Color: 1
Size: 293336 Color: 7

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 396239 Color: 16
Size: 306720 Color: 14
Size: 297042 Color: 3

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 396256 Color: 12
Size: 311662 Color: 2
Size: 292083 Color: 4

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 396266 Color: 16
Size: 307459 Color: 14
Size: 296276 Color: 8

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 396289 Color: 17
Size: 343055 Color: 19
Size: 260657 Color: 5

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 396334 Color: 0
Size: 346286 Color: 16
Size: 257381 Color: 1

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 396390 Color: 7
Size: 311798 Color: 8
Size: 291813 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 396471 Color: 12
Size: 312200 Color: 11
Size: 291330 Color: 14

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 396578 Color: 15
Size: 344614 Color: 13
Size: 258809 Color: 9

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 396602 Color: 8
Size: 351597 Color: 12
Size: 251802 Color: 7

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 396755 Color: 10
Size: 308267 Color: 16
Size: 294979 Color: 10

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 396785 Color: 10
Size: 342737 Color: 18
Size: 260479 Color: 14

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 396862 Color: 17
Size: 336153 Color: 2
Size: 266986 Color: 12

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 396918 Color: 14
Size: 334694 Color: 18
Size: 268389 Color: 16

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 396947 Color: 6
Size: 350568 Color: 18
Size: 252486 Color: 4

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 396991 Color: 17
Size: 320183 Color: 9
Size: 282827 Color: 6

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 397042 Color: 13
Size: 306959 Color: 9
Size: 296000 Color: 17

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 397043 Color: 1
Size: 306342 Color: 1
Size: 296616 Color: 10

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 397072 Color: 19
Size: 347154 Color: 3
Size: 255775 Color: 2

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 397073 Color: 7
Size: 303503 Color: 19
Size: 299425 Color: 10

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 397138 Color: 4
Size: 327483 Color: 19
Size: 275380 Color: 19

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 397217 Color: 8
Size: 310112 Color: 13
Size: 292672 Color: 15

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 397367 Color: 12
Size: 302631 Color: 11
Size: 300003 Color: 1

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 397371 Color: 15
Size: 307849 Color: 16
Size: 294781 Color: 9

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 397394 Color: 4
Size: 313696 Color: 8
Size: 288911 Color: 2

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 397527 Color: 15
Size: 344568 Color: 7
Size: 257906 Color: 14

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 397539 Color: 16
Size: 350119 Color: 8
Size: 252343 Color: 7

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 397592 Color: 15
Size: 344377 Color: 13
Size: 258032 Color: 14

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 397649 Color: 5
Size: 338601 Color: 17
Size: 263751 Color: 4

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 397671 Color: 11
Size: 327980 Color: 14
Size: 274350 Color: 7

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 397764 Color: 8
Size: 343993 Color: 19
Size: 258244 Color: 12

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 397869 Color: 5
Size: 325948 Color: 8
Size: 276184 Color: 11

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 397875 Color: 3
Size: 326715 Color: 11
Size: 275411 Color: 12

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 397970 Color: 9
Size: 320581 Color: 19
Size: 281450 Color: 10

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 397986 Color: 12
Size: 333069 Color: 14
Size: 268946 Color: 7

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 398058 Color: 11
Size: 319834 Color: 8
Size: 282109 Color: 16

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 398095 Color: 15
Size: 334891 Color: 11
Size: 267015 Color: 5

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 398127 Color: 16
Size: 315850 Color: 11
Size: 286024 Color: 15

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 398251 Color: 2
Size: 308487 Color: 18
Size: 293263 Color: 14

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 398151 Color: 17
Size: 324908 Color: 11
Size: 276942 Color: 5

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 398178 Color: 19
Size: 318763 Color: 12
Size: 283060 Color: 10

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 398260 Color: 5
Size: 312402 Color: 11
Size: 289339 Color: 4

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 398275 Color: 14
Size: 349943 Color: 19
Size: 251783 Color: 16

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 398367 Color: 8
Size: 325203 Color: 7
Size: 276431 Color: 16

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 398375 Color: 18
Size: 346203 Color: 3
Size: 255423 Color: 5

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 398431 Color: 1
Size: 347247 Color: 17
Size: 254323 Color: 10

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 398462 Color: 18
Size: 343683 Color: 19
Size: 257856 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 398463 Color: 3
Size: 302075 Color: 1
Size: 299463 Color: 19

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 398494 Color: 0
Size: 304841 Color: 11
Size: 296666 Color: 2

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 398537 Color: 5
Size: 332675 Color: 16
Size: 268789 Color: 9

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 398550 Color: 0
Size: 322667 Color: 10
Size: 278784 Color: 8

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 398607 Color: 1
Size: 317360 Color: 4
Size: 284034 Color: 6

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 398621 Color: 12
Size: 315625 Color: 1
Size: 285755 Color: 7

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 398637 Color: 3
Size: 303919 Color: 10
Size: 297445 Color: 2

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 398640 Color: 1
Size: 345787 Color: 13
Size: 255574 Color: 12

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 398793 Color: 1
Size: 338456 Color: 17
Size: 262752 Color: 16

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 398842 Color: 4
Size: 316482 Color: 9
Size: 284677 Color: 3

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 398873 Color: 19
Size: 346226 Color: 10
Size: 254902 Color: 9

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 398878 Color: 9
Size: 321836 Color: 1
Size: 279287 Color: 7

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 398904 Color: 6
Size: 342281 Color: 5
Size: 258816 Color: 2

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 398979 Color: 18
Size: 311314 Color: 12
Size: 289708 Color: 19

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 399018 Color: 12
Size: 336234 Color: 19
Size: 264749 Color: 7

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 399029 Color: 13
Size: 320961 Color: 5
Size: 280011 Color: 12

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 399039 Color: 3
Size: 324339 Color: 12
Size: 276623 Color: 9

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 399113 Color: 4
Size: 343757 Color: 1
Size: 257131 Color: 14

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 399121 Color: 14
Size: 314295 Color: 1
Size: 286585 Color: 5

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 399121 Color: 13
Size: 318960 Color: 4
Size: 281920 Color: 10

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 399152 Color: 17
Size: 314485 Color: 0
Size: 286364 Color: 4

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 399160 Color: 19
Size: 331365 Color: 6
Size: 269476 Color: 11

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 399165 Color: 4
Size: 340187 Color: 6
Size: 260649 Color: 18

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 399248 Color: 4
Size: 344643 Color: 19
Size: 256110 Color: 1

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 399314 Color: 10
Size: 348106 Color: 12
Size: 252581 Color: 14

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 399323 Color: 3
Size: 316123 Color: 18
Size: 284555 Color: 13

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 399328 Color: 18
Size: 331474 Color: 15
Size: 269199 Color: 3

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 399430 Color: 1
Size: 306911 Color: 4
Size: 293660 Color: 2

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 399486 Color: 5
Size: 338416 Color: 7
Size: 262099 Color: 9

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 399536 Color: 1
Size: 319354 Color: 0
Size: 281111 Color: 3

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 399540 Color: 8
Size: 306562 Color: 11
Size: 293899 Color: 5

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 399580 Color: 17
Size: 305938 Color: 12
Size: 294483 Color: 8

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 399593 Color: 11
Size: 331159 Color: 4
Size: 269249 Color: 17

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 399693 Color: 10
Size: 300593 Color: 2
Size: 299715 Color: 12

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 399748 Color: 19
Size: 304970 Color: 4
Size: 295283 Color: 8

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 399837 Color: 7
Size: 341784 Color: 18
Size: 258380 Color: 12

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 399916 Color: 3
Size: 330154 Color: 6
Size: 269931 Color: 5

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 399988 Color: 10
Size: 303546 Color: 15
Size: 296467 Color: 12

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 400055 Color: 8
Size: 322184 Color: 8
Size: 277762 Color: 2

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 400073 Color: 16
Size: 306070 Color: 19
Size: 293858 Color: 0

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 400091 Color: 11
Size: 313969 Color: 3
Size: 285941 Color: 15

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 400164 Color: 16
Size: 308213 Color: 13
Size: 291624 Color: 14

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 400174 Color: 17
Size: 306369 Color: 16
Size: 293458 Color: 11

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 400227 Color: 1
Size: 302153 Color: 8
Size: 297621 Color: 13

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 400259 Color: 16
Size: 305109 Color: 7
Size: 294633 Color: 18

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 400280 Color: 0
Size: 321674 Color: 1
Size: 278047 Color: 17

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 400309 Color: 6
Size: 321393 Color: 15
Size: 278299 Color: 13

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 400386 Color: 7
Size: 342543 Color: 14
Size: 257072 Color: 9

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 400393 Color: 18
Size: 344572 Color: 13
Size: 255036 Color: 14

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 400394 Color: 15
Size: 315828 Color: 5
Size: 283779 Color: 8

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 400413 Color: 0
Size: 341969 Color: 1
Size: 257619 Color: 5

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 400444 Color: 14
Size: 302227 Color: 19
Size: 297330 Color: 12

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 400492 Color: 8
Size: 346425 Color: 18
Size: 253084 Color: 16

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 400619 Color: 9
Size: 317958 Color: 4
Size: 281424 Color: 4

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 400632 Color: 13
Size: 302892 Color: 16
Size: 296477 Color: 12

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 400661 Color: 11
Size: 328662 Color: 10
Size: 270678 Color: 4

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 400708 Color: 8
Size: 303076 Color: 12
Size: 296217 Color: 1

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 400934 Color: 2
Size: 328716 Color: 17
Size: 270351 Color: 13

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 400819 Color: 16
Size: 324651 Color: 8
Size: 274531 Color: 12

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 400915 Color: 19
Size: 322560 Color: 1
Size: 276526 Color: 17

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 400923 Color: 9
Size: 319921 Color: 8
Size: 279157 Color: 11

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 400954 Color: 4
Size: 348874 Color: 3
Size: 250173 Color: 5

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 400973 Color: 7
Size: 330627 Color: 2
Size: 268401 Color: 18

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 401104 Color: 16
Size: 308860 Color: 5
Size: 290037 Color: 18

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 401140 Color: 17
Size: 339453 Color: 1
Size: 259408 Color: 11

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 401166 Color: 0
Size: 330653 Color: 0
Size: 268182 Color: 13

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 401278 Color: 4
Size: 311734 Color: 3
Size: 286989 Color: 9

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 401419 Color: 1
Size: 303653 Color: 2
Size: 294929 Color: 14

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 401428 Color: 9
Size: 347364 Color: 5
Size: 251209 Color: 11

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 401490 Color: 17
Size: 311351 Color: 17
Size: 287160 Color: 3

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 401499 Color: 6
Size: 328779 Color: 3
Size: 269723 Color: 15

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 401499 Color: 13
Size: 312748 Color: 15
Size: 285754 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 401500 Color: 3
Size: 318061 Color: 17
Size: 280440 Color: 1

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 401519 Color: 19
Size: 308041 Color: 2
Size: 290441 Color: 12

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 401529 Color: 1
Size: 340642 Color: 7
Size: 257830 Color: 17

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 401642 Color: 11
Size: 305283 Color: 8
Size: 293076 Color: 7

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 401650 Color: 18
Size: 313477 Color: 7
Size: 284874 Color: 12

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 401658 Color: 16
Size: 336722 Color: 4
Size: 261621 Color: 12

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 401670 Color: 11
Size: 304825 Color: 16
Size: 293506 Color: 1

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 401687 Color: 5
Size: 325668 Color: 2
Size: 272646 Color: 11

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 401697 Color: 8
Size: 300744 Color: 15
Size: 297560 Color: 14

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 401758 Color: 9
Size: 332434 Color: 14
Size: 265809 Color: 18

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 401799 Color: 9
Size: 327602 Color: 19
Size: 270600 Color: 18

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 401847 Color: 12
Size: 324112 Color: 6
Size: 274042 Color: 3

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 401873 Color: 19
Size: 307417 Color: 17
Size: 290711 Color: 11

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 401875 Color: 2
Size: 334984 Color: 1
Size: 263142 Color: 16

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 401884 Color: 8
Size: 329891 Color: 4
Size: 268226 Color: 18

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 401953 Color: 9
Size: 299502 Color: 7
Size: 298546 Color: 1

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 401958 Color: 6
Size: 331259 Color: 4
Size: 266784 Color: 3

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 402069 Color: 14
Size: 303775 Color: 7
Size: 294157 Color: 11

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 402092 Color: 13
Size: 308392 Color: 4
Size: 289517 Color: 2

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 402107 Color: 0
Size: 315054 Color: 9
Size: 282840 Color: 6

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 402119 Color: 3
Size: 329841 Color: 9
Size: 268041 Color: 18

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 402199 Color: 6
Size: 308052 Color: 13
Size: 289750 Color: 8

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 402245 Color: 11
Size: 301179 Color: 10
Size: 296577 Color: 8

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 402289 Color: 11
Size: 339221 Color: 14
Size: 258491 Color: 2

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 402323 Color: 11
Size: 340709 Color: 8
Size: 256969 Color: 17

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 402326 Color: 12
Size: 304316 Color: 13
Size: 293359 Color: 10

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 402356 Color: 6
Size: 307681 Color: 17
Size: 289964 Color: 6

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 402411 Color: 4
Size: 318507 Color: 9
Size: 279083 Color: 14

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 402454 Color: 9
Size: 302249 Color: 18
Size: 295298 Color: 16

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 402478 Color: 5
Size: 313723 Color: 5
Size: 283800 Color: 2

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 402522 Color: 10
Size: 299663 Color: 14
Size: 297816 Color: 4

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 402555 Color: 16
Size: 309699 Color: 13
Size: 287747 Color: 11

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 402558 Color: 13
Size: 339787 Color: 6
Size: 257656 Color: 15

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 402596 Color: 15
Size: 317324 Color: 19
Size: 280081 Color: 13

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 402673 Color: 15
Size: 302301 Color: 13
Size: 295027 Color: 2

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 402688 Color: 13
Size: 345943 Color: 11
Size: 251370 Color: 3

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 402691 Color: 11
Size: 321017 Color: 16
Size: 276293 Color: 5

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 402757 Color: 6
Size: 307223 Color: 18
Size: 290021 Color: 17

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 402759 Color: 0
Size: 318357 Color: 3
Size: 278885 Color: 12

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 402783 Color: 12
Size: 308199 Color: 14
Size: 289019 Color: 16

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 402877 Color: 6
Size: 334363 Color: 9
Size: 262761 Color: 12

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 402882 Color: 18
Size: 307695 Color: 8
Size: 289424 Color: 4

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 402904 Color: 16
Size: 307294 Color: 1
Size: 289803 Color: 17

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 403014 Color: 11
Size: 329457 Color: 15
Size: 267530 Color: 14

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 403029 Color: 11
Size: 336725 Color: 13
Size: 260247 Color: 3

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 403064 Color: 6
Size: 315517 Color: 2
Size: 281420 Color: 12

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 403118 Color: 9
Size: 327036 Color: 5
Size: 269847 Color: 12

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 403174 Color: 12
Size: 302557 Color: 17
Size: 294270 Color: 0

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 403226 Color: 4
Size: 325019 Color: 10
Size: 271756 Color: 18

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 403339 Color: 15
Size: 315940 Color: 17
Size: 280722 Color: 2

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 403357 Color: 1
Size: 305274 Color: 14
Size: 291370 Color: 15

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 403369 Color: 8
Size: 328446 Color: 5
Size: 268186 Color: 16

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 403414 Color: 10
Size: 339989 Color: 12
Size: 256598 Color: 3

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 403473 Color: 15
Size: 317981 Color: 5
Size: 278547 Color: 4

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 403499 Color: 14
Size: 305728 Color: 17
Size: 290774 Color: 19

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 403548 Color: 11
Size: 331531 Color: 9
Size: 264922 Color: 7

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 403556 Color: 14
Size: 300558 Color: 15
Size: 295887 Color: 16

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 403586 Color: 13
Size: 342196 Color: 3
Size: 254219 Color: 4

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 403804 Color: 19
Size: 318580 Color: 18
Size: 277617 Color: 14

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 403893 Color: 2
Size: 323546 Color: 9
Size: 272562 Color: 5

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 403827 Color: 13
Size: 324017 Color: 6
Size: 272157 Color: 12

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 403888 Color: 0
Size: 314144 Color: 10
Size: 281969 Color: 14

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 403899 Color: 19
Size: 302135 Color: 8
Size: 293967 Color: 19

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 403949 Color: 4
Size: 341879 Color: 15
Size: 254173 Color: 13

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 403985 Color: 11
Size: 329426 Color: 9
Size: 266590 Color: 14

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 404016 Color: 13
Size: 341857 Color: 6
Size: 254128 Color: 2

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 404030 Color: 3
Size: 300625 Color: 11
Size: 295346 Color: 8

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 404102 Color: 10
Size: 329483 Color: 4
Size: 266416 Color: 7

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 404180 Color: 8
Size: 315128 Color: 0
Size: 280693 Color: 9

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 404203 Color: 11
Size: 337580 Color: 3
Size: 258218 Color: 5

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 404311 Color: 3
Size: 338060 Color: 7
Size: 257630 Color: 2

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 404321 Color: 13
Size: 319680 Color: 19
Size: 276000 Color: 1

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 404332 Color: 19
Size: 310301 Color: 14
Size: 285368 Color: 7

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 404341 Color: 9
Size: 324740 Color: 4
Size: 270920 Color: 8

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 404344 Color: 14
Size: 325568 Color: 5
Size: 270089 Color: 9

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 404444 Color: 13
Size: 328384 Color: 2
Size: 267173 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 404496 Color: 5
Size: 339495 Color: 9
Size: 256010 Color: 3

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 404503 Color: 15
Size: 305531 Color: 11
Size: 289967 Color: 8

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 404504 Color: 11
Size: 310931 Color: 8
Size: 284566 Color: 6

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 404505 Color: 14
Size: 340014 Color: 12
Size: 255482 Color: 6

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 404549 Color: 10
Size: 324077 Color: 2
Size: 271375 Color: 10

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 404643 Color: 3
Size: 312732 Color: 16
Size: 282626 Color: 17

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 404649 Color: 10
Size: 334233 Color: 12
Size: 261119 Color: 5

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 404864 Color: 15
Size: 314378 Color: 18
Size: 280759 Color: 12

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 404872 Color: 18
Size: 344329 Color: 4
Size: 250800 Color: 2

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 404891 Color: 6
Size: 324563 Color: 1
Size: 270547 Color: 15

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 404921 Color: 1
Size: 328385 Color: 4
Size: 266695 Color: 2

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 404952 Color: 19
Size: 299990 Color: 1
Size: 295059 Color: 9

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 404991 Color: 16
Size: 329464 Color: 15
Size: 265546 Color: 8

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 405031 Color: 19
Size: 340449 Color: 19
Size: 254521 Color: 8

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 405057 Color: 5
Size: 328013 Color: 10
Size: 266931 Color: 12

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 405072 Color: 18
Size: 319728 Color: 2
Size: 275201 Color: 0

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 405124 Color: 0
Size: 302305 Color: 7
Size: 292572 Color: 12

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 405136 Color: 11
Size: 324568 Color: 7
Size: 270297 Color: 12

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 405143 Color: 5
Size: 316940 Color: 7
Size: 277918 Color: 19

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 405154 Color: 13
Size: 300264 Color: 1
Size: 294583 Color: 15

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 405227 Color: 1
Size: 322655 Color: 2
Size: 272119 Color: 8

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 405231 Color: 4
Size: 297554 Color: 10
Size: 297216 Color: 9

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 405319 Color: 4
Size: 320791 Color: 14
Size: 273891 Color: 14

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 405330 Color: 15
Size: 335767 Color: 3
Size: 258904 Color: 13

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 405350 Color: 7
Size: 341122 Color: 6
Size: 253529 Color: 3

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 405382 Color: 11
Size: 330484 Color: 5
Size: 264135 Color: 11

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 405406 Color: 5
Size: 312361 Color: 2
Size: 282234 Color: 18

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 405434 Color: 0
Size: 339096 Color: 17
Size: 255471 Color: 12

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 405437 Color: 16
Size: 300057 Color: 1
Size: 294507 Color: 8

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 405488 Color: 14
Size: 333479 Color: 16
Size: 261034 Color: 15

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 405899 Color: 5
Size: 307600 Color: 15
Size: 286502 Color: 14

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 405935 Color: 2
Size: 301378 Color: 7
Size: 292688 Color: 0

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 405936 Color: 4
Size: 323443 Color: 7
Size: 270622 Color: 0

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 405997 Color: 12
Size: 331724 Color: 15
Size: 262280 Color: 14

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 406004 Color: 19
Size: 328375 Color: 2
Size: 265622 Color: 5

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 406024 Color: 7
Size: 328051 Color: 19
Size: 265926 Color: 8

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 406051 Color: 12
Size: 312091 Color: 17
Size: 281859 Color: 8

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 406067 Color: 10
Size: 297529 Color: 12
Size: 296405 Color: 11

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 406310 Color: 2
Size: 315727 Color: 0
Size: 277964 Color: 14

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 406223 Color: 4
Size: 302013 Color: 7
Size: 291765 Color: 11

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 406226 Color: 0
Size: 338310 Color: 19
Size: 255465 Color: 4

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 406233 Color: 13
Size: 331321 Color: 9
Size: 262447 Color: 12

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 406247 Color: 15
Size: 320332 Color: 0
Size: 273422 Color: 19

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 406327 Color: 14
Size: 315680 Color: 7
Size: 277994 Color: 9

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 406443 Color: 2
Size: 309732 Color: 10
Size: 283826 Color: 10

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 406355 Color: 11
Size: 299140 Color: 13
Size: 294506 Color: 17

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 406452 Color: 17
Size: 312247 Color: 10
Size: 281302 Color: 18

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 406716 Color: 1
Size: 319074 Color: 7
Size: 274211 Color: 6

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 406770 Color: 6
Size: 329877 Color: 11
Size: 263354 Color: 12

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 406880 Color: 6
Size: 323816 Color: 2
Size: 269305 Color: 17

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 406900 Color: 13
Size: 337686 Color: 15
Size: 255415 Color: 6

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 406931 Color: 18
Size: 315972 Color: 13
Size: 277098 Color: 18

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 407029 Color: 0
Size: 313942 Color: 6
Size: 279030 Color: 18

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 407061 Color: 14
Size: 313120 Color: 12
Size: 279820 Color: 10

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 407095 Color: 13
Size: 317586 Color: 2
Size: 275320 Color: 1

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 407180 Color: 9
Size: 325667 Color: 19
Size: 267154 Color: 8

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 407183 Color: 15
Size: 327586 Color: 6
Size: 265232 Color: 6

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 407233 Color: 12
Size: 321964 Color: 1
Size: 270804 Color: 17

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 407273 Color: 4
Size: 305856 Color: 11
Size: 286872 Color: 12

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 407333 Color: 9
Size: 298786 Color: 7
Size: 293882 Color: 3

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 407351 Color: 15
Size: 317012 Color: 0
Size: 275638 Color: 2

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 407410 Color: 12
Size: 323515 Color: 17
Size: 269076 Color: 8

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 407429 Color: 7
Size: 315079 Color: 7
Size: 277493 Color: 4

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 407479 Color: 9
Size: 298369 Color: 17
Size: 294153 Color: 13

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 407544 Color: 7
Size: 330048 Color: 1
Size: 262409 Color: 15

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 407583 Color: 2
Size: 311969 Color: 19
Size: 280449 Color: 4

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 407547 Color: 6
Size: 299221 Color: 8
Size: 293233 Color: 5

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 407584 Color: 5
Size: 325509 Color: 15
Size: 266908 Color: 5

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 407584 Color: 10
Size: 300780 Color: 7
Size: 291637 Color: 14

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 407640 Color: 9
Size: 334707 Color: 9
Size: 257654 Color: 0

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 407865 Color: 9
Size: 297036 Color: 16
Size: 295100 Color: 2

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 407901 Color: 18
Size: 337479 Color: 1
Size: 254621 Color: 15

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 407961 Color: 14
Size: 306206 Color: 15
Size: 285834 Color: 10

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 407972 Color: 14
Size: 333538 Color: 19
Size: 258491 Color: 17

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 408016 Color: 10
Size: 338022 Color: 14
Size: 253963 Color: 8

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 408068 Color: 0
Size: 326922 Color: 2
Size: 265011 Color: 17

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 408073 Color: 10
Size: 340456 Color: 3
Size: 251472 Color: 13

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 408204 Color: 8
Size: 332852 Color: 18
Size: 258945 Color: 12

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 408245 Color: 1
Size: 306962 Color: 10
Size: 284794 Color: 13

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 408253 Color: 0
Size: 333828 Color: 2
Size: 257920 Color: 16

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 408276 Color: 3
Size: 332181 Color: 11
Size: 259544 Color: 18

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 408283 Color: 15
Size: 309562 Color: 1
Size: 282156 Color: 4

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 408301 Color: 5
Size: 321326 Color: 3
Size: 270374 Color: 6

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 408317 Color: 16
Size: 300480 Color: 19
Size: 291204 Color: 9

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 408389 Color: 6
Size: 304621 Color: 14
Size: 286991 Color: 2

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 408429 Color: 18
Size: 331018 Color: 16
Size: 260554 Color: 13

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 408490 Color: 12
Size: 340042 Color: 3
Size: 251469 Color: 0

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 408547 Color: 15
Size: 323824 Color: 16
Size: 267630 Color: 10

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 408587 Color: 13
Size: 336598 Color: 11
Size: 254816 Color: 17

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 408608 Color: 14
Size: 313767 Color: 18
Size: 277626 Color: 18

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 408616 Color: 5
Size: 330816 Color: 7
Size: 260569 Color: 2

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 408626 Color: 14
Size: 327183 Color: 17
Size: 264192 Color: 3

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 408668 Color: 0
Size: 334715 Color: 7
Size: 256618 Color: 19

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 408694 Color: 17
Size: 309518 Color: 3
Size: 281789 Color: 6

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 408708 Color: 19
Size: 328774 Color: 1
Size: 262519 Color: 10

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 408731 Color: 10
Size: 316875 Color: 15
Size: 274395 Color: 19

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 408727 Color: 2
Size: 328823 Color: 17
Size: 262451 Color: 8

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 408737 Color: 1
Size: 327221 Color: 5
Size: 264043 Color: 3

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 408774 Color: 11
Size: 314364 Color: 10
Size: 276863 Color: 9

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 408785 Color: 5
Size: 316329 Color: 0
Size: 274887 Color: 5

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 408885 Color: 5
Size: 329559 Color: 18
Size: 261557 Color: 13

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 408897 Color: 17
Size: 298093 Color: 14
Size: 293011 Color: 18

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 408918 Color: 5
Size: 328401 Color: 2
Size: 262682 Color: 13

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 409006 Color: 8
Size: 323830 Color: 18
Size: 267165 Color: 6

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 409051 Color: 19
Size: 337730 Color: 15
Size: 253220 Color: 3

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 409068 Color: 13
Size: 330479 Color: 5
Size: 260454 Color: 4

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 409098 Color: 5
Size: 331292 Color: 3
Size: 259611 Color: 12

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 409100 Color: 16
Size: 328157 Color: 13
Size: 262744 Color: 19

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 409185 Color: 14
Size: 315344 Color: 2
Size: 275472 Color: 5

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 409206 Color: 4
Size: 314431 Color: 1
Size: 276364 Color: 11

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 409222 Color: 0
Size: 322132 Color: 4
Size: 268647 Color: 7

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 409254 Color: 18
Size: 314973 Color: 6
Size: 275774 Color: 10

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 409294 Color: 3
Size: 321136 Color: 19
Size: 269571 Color: 14

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 409362 Color: 1
Size: 321330 Color: 1
Size: 269309 Color: 2

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 409376 Color: 7
Size: 324681 Color: 14
Size: 265944 Color: 18

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 409381 Color: 17
Size: 323010 Color: 4
Size: 267610 Color: 7

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 409419 Color: 1
Size: 304735 Color: 6
Size: 285847 Color: 9

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 409437 Color: 8
Size: 308663 Color: 17
Size: 281901 Color: 11

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 409465 Color: 7
Size: 332696 Color: 10
Size: 257840 Color: 5

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 409502 Color: 16
Size: 301057 Color: 2
Size: 289442 Color: 7

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 409506 Color: 11
Size: 306495 Color: 17
Size: 284000 Color: 14

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 409528 Color: 19
Size: 312912 Color: 11
Size: 277561 Color: 17

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 409653 Color: 5
Size: 336177 Color: 6
Size: 254171 Color: 19

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 409680 Color: 16
Size: 296461 Color: 14
Size: 293860 Color: 10

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 409694 Color: 5
Size: 308077 Color: 15
Size: 282230 Color: 13

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 409704 Color: 1
Size: 308982 Color: 2
Size: 281315 Color: 17

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 409705 Color: 12
Size: 302250 Color: 12
Size: 288046 Color: 19

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 409749 Color: 10
Size: 324513 Color: 8
Size: 265739 Color: 7

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 409763 Color: 10
Size: 339725 Color: 15
Size: 250513 Color: 7

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 409798 Color: 16
Size: 320284 Color: 10
Size: 269919 Color: 11

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 409919 Color: 14
Size: 322490 Color: 9
Size: 267592 Color: 18

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 409931 Color: 15
Size: 304679 Color: 2
Size: 285391 Color: 10

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 410025 Color: 19
Size: 294997 Color: 18
Size: 294979 Color: 6

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 410026 Color: 7
Size: 311668 Color: 15
Size: 278307 Color: 17

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 410037 Color: 1
Size: 321268 Color: 1
Size: 268696 Color: 0

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 410097 Color: 15
Size: 325886 Color: 18
Size: 264018 Color: 4

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 410103 Color: 1
Size: 314634 Color: 3
Size: 275264 Color: 16

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 410147 Color: 2
Size: 327975 Color: 0
Size: 261879 Color: 17

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 410152 Color: 7
Size: 329829 Color: 17
Size: 260020 Color: 13

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 410171 Color: 9
Size: 326443 Color: 8
Size: 263387 Color: 1

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 410236 Color: 15
Size: 331216 Color: 19
Size: 258549 Color: 5

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 410355 Color: 1
Size: 296767 Color: 15
Size: 292879 Color: 8

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 410356 Color: 16
Size: 313947 Color: 18
Size: 275698 Color: 7

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 410361 Color: 6
Size: 331119 Color: 2
Size: 258521 Color: 9

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 410362 Color: 16
Size: 330175 Color: 7
Size: 259464 Color: 7

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 410394 Color: 19
Size: 321494 Color: 3
Size: 268113 Color: 4

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 410406 Color: 10
Size: 328461 Color: 7
Size: 261134 Color: 4

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 410460 Color: 10
Size: 317374 Color: 16
Size: 272167 Color: 3

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 410464 Color: 13
Size: 329632 Color: 19
Size: 259905 Color: 15

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 410532 Color: 2
Size: 324939 Color: 19
Size: 264530 Color: 11

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 410498 Color: 15
Size: 325065 Color: 11
Size: 264438 Color: 3

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 410592 Color: 17
Size: 335521 Color: 4
Size: 253888 Color: 11

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 410621 Color: 6
Size: 304192 Color: 14
Size: 285188 Color: 12

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 410678 Color: 12
Size: 335112 Color: 4
Size: 254211 Color: 4

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 410726 Color: 4
Size: 302840 Color: 12
Size: 286435 Color: 10

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 410774 Color: 9
Size: 318065 Color: 6
Size: 271162 Color: 2

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 410791 Color: 6
Size: 332485 Color: 12
Size: 256725 Color: 6

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 410834 Color: 9
Size: 317711 Color: 3
Size: 271456 Color: 14

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 410836 Color: 18
Size: 314957 Color: 15
Size: 274208 Color: 12

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 410952 Color: 12
Size: 323523 Color: 19
Size: 265526 Color: 8

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 410959 Color: 12
Size: 318191 Color: 10
Size: 270851 Color: 16

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 411070 Color: 6
Size: 334069 Color: 3
Size: 254862 Color: 1

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 411113 Color: 14
Size: 322450 Color: 13
Size: 266438 Color: 17

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 411181 Color: 15
Size: 313444 Color: 15
Size: 275376 Color: 5

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 411233 Color: 4
Size: 316498 Color: 7
Size: 272270 Color: 13

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 411291 Color: 11
Size: 336666 Color: 16
Size: 252044 Color: 13

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 411313 Color: 10
Size: 319912 Color: 4
Size: 268776 Color: 5

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 411335 Color: 1
Size: 295035 Color: 17
Size: 293631 Color: 11

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 411350 Color: 10
Size: 295909 Color: 12
Size: 292742 Color: 18

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 411365 Color: 15
Size: 328134 Color: 0
Size: 260502 Color: 19

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 411387 Color: 10
Size: 319956 Color: 8
Size: 268658 Color: 13

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 411393 Color: 0
Size: 308480 Color: 2
Size: 280128 Color: 17

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 411455 Color: 17
Size: 315522 Color: 17
Size: 273024 Color: 12

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 411484 Color: 8
Size: 332101 Color: 13
Size: 256416 Color: 9

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 411516 Color: 1
Size: 325186 Color: 6
Size: 263299 Color: 7

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 411531 Color: 6
Size: 305413 Color: 5
Size: 283057 Color: 7

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 411537 Color: 17
Size: 317614 Color: 19
Size: 270850 Color: 2

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 411589 Color: 13
Size: 327116 Color: 10
Size: 261296 Color: 10

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 411609 Color: 19
Size: 333481 Color: 12
Size: 254911 Color: 17

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 411703 Color: 4
Size: 319043 Color: 6
Size: 269255 Color: 11

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 411756 Color: 6
Size: 328535 Color: 10
Size: 259710 Color: 1

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 411778 Color: 6
Size: 323271 Color: 5
Size: 264952 Color: 18

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 411843 Color: 13
Size: 331464 Color: 11
Size: 256694 Color: 9

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 411843 Color: 6
Size: 325104 Color: 5
Size: 263054 Color: 14

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 411857 Color: 6
Size: 322279 Color: 5
Size: 265865 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 411874 Color: 13
Size: 325647 Color: 12
Size: 262480 Color: 10

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 411962 Color: 14
Size: 316725 Color: 17
Size: 271314 Color: 13

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 411992 Color: 17
Size: 297940 Color: 2
Size: 290069 Color: 17

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 412030 Color: 9
Size: 320617 Color: 6
Size: 267354 Color: 0

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 412194 Color: 1
Size: 329269 Color: 4
Size: 258538 Color: 10

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 412201 Color: 1
Size: 319367 Color: 1
Size: 268433 Color: 12

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 412235 Color: 12
Size: 309869 Color: 13
Size: 277897 Color: 15

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 412432 Color: 2
Size: 332275 Color: 1
Size: 255294 Color: 1

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 412354 Color: 15
Size: 329769 Color: 13
Size: 257878 Color: 17

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 412360 Color: 18
Size: 307593 Color: 1
Size: 280048 Color: 9

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 412426 Color: 1
Size: 295436 Color: 12
Size: 292139 Color: 16

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 412470 Color: 4
Size: 296705 Color: 12
Size: 290826 Color: 12

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 412474 Color: 9
Size: 315299 Color: 2
Size: 272228 Color: 9

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 412522 Color: 19
Size: 323353 Color: 6
Size: 264126 Color: 12

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 412685 Color: 18
Size: 313589 Color: 16
Size: 273727 Color: 4

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 412714 Color: 4
Size: 295673 Color: 10
Size: 291614 Color: 10

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 412715 Color: 10
Size: 304334 Color: 8
Size: 282952 Color: 8

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 412766 Color: 10
Size: 310431 Color: 14
Size: 276804 Color: 18

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 412854 Color: 13
Size: 305574 Color: 8
Size: 281573 Color: 16

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 412874 Color: 11
Size: 324901 Color: 3
Size: 262226 Color: 12

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 412906 Color: 6
Size: 324517 Color: 4
Size: 262578 Color: 4

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 412980 Color: 4
Size: 330216 Color: 18
Size: 256805 Color: 11

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 413020 Color: 1
Size: 321588 Color: 10
Size: 265393 Color: 2

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 413125 Color: 10
Size: 297970 Color: 10
Size: 288906 Color: 1

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 413287 Color: 3
Size: 299353 Color: 18
Size: 287361 Color: 4

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 413344 Color: 11
Size: 293604 Color: 19
Size: 293053 Color: 3

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 413394 Color: 14
Size: 325486 Color: 14
Size: 261121 Color: 19

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 413407 Color: 5
Size: 324601 Color: 5
Size: 261993 Color: 19

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 413554 Color: 15
Size: 316846 Color: 2
Size: 269601 Color: 13

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 413605 Color: 11
Size: 320870 Color: 13
Size: 265526 Color: 17

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 413637 Color: 1
Size: 333869 Color: 13
Size: 252495 Color: 7

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 413706 Color: 18
Size: 335476 Color: 7
Size: 250819 Color: 17

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 413731 Color: 5
Size: 307015 Color: 15
Size: 279255 Color: 1

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 413783 Color: 3
Size: 311683 Color: 7
Size: 274535 Color: 12

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 413840 Color: 12
Size: 334236 Color: 6
Size: 251925 Color: 2

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 413934 Color: 19
Size: 302310 Color: 1
Size: 283757 Color: 4

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 413977 Color: 3
Size: 325078 Color: 9
Size: 260946 Color: 18

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 414076 Color: 18
Size: 293408 Color: 11
Size: 292517 Color: 4

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 414114 Color: 0
Size: 298315 Color: 10
Size: 287572 Color: 9

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 414154 Color: 15
Size: 320838 Color: 12
Size: 265009 Color: 7

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 414162 Color: 1
Size: 335510 Color: 2
Size: 250329 Color: 8

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 414206 Color: 5
Size: 308595 Color: 13
Size: 277200 Color: 3

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 414237 Color: 16
Size: 314618 Color: 14
Size: 271146 Color: 1

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 414301 Color: 19
Size: 321223 Color: 13
Size: 264477 Color: 4

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 414347 Color: 13
Size: 318860 Color: 9
Size: 266794 Color: 11

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 414376 Color: 8
Size: 301738 Color: 5
Size: 283887 Color: 2

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 414441 Color: 14
Size: 298144 Color: 0
Size: 287416 Color: 0

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 414504 Color: 3
Size: 330530 Color: 15
Size: 254967 Color: 10

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 414514 Color: 11
Size: 298856 Color: 19
Size: 286631 Color: 19

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 414521 Color: 5
Size: 294742 Color: 3
Size: 290738 Color: 1

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 414533 Color: 13
Size: 306197 Color: 0
Size: 279271 Color: 8

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 414552 Color: 3
Size: 313439 Color: 13
Size: 272010 Color: 7

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 414845 Color: 2
Size: 293967 Color: 6
Size: 291189 Color: 0

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 414810 Color: 17
Size: 321483 Color: 12
Size: 263708 Color: 7

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 414920 Color: 16
Size: 294379 Color: 14
Size: 290702 Color: 18

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 414973 Color: 3
Size: 329935 Color: 14
Size: 255093 Color: 13

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 414991 Color: 7
Size: 317850 Color: 19
Size: 267160 Color: 2

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 415032 Color: 4
Size: 322652 Color: 3
Size: 262317 Color: 10

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 415056 Color: 1
Size: 294466 Color: 8
Size: 290479 Color: 1

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 415066 Color: 0
Size: 308908 Color: 1
Size: 276027 Color: 17

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 415116 Color: 12
Size: 313404 Color: 0
Size: 271481 Color: 7

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 415209 Color: 15
Size: 330340 Color: 19
Size: 254452 Color: 10

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 415231 Color: 0
Size: 330290 Color: 13
Size: 254480 Color: 2

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 415295 Color: 12
Size: 309765 Color: 7
Size: 274941 Color: 18

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 415299 Color: 1
Size: 323766 Color: 13
Size: 260936 Color: 17

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 415354 Color: 10
Size: 302170 Color: 14
Size: 282477 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 415434 Color: 16
Size: 328444 Color: 17
Size: 256123 Color: 1

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 415465 Color: 12
Size: 331897 Color: 3
Size: 252639 Color: 16

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 415479 Color: 6
Size: 332230 Color: 14
Size: 252292 Color: 18

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 415495 Color: 16
Size: 332390 Color: 13
Size: 252116 Color: 1

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 415501 Color: 4
Size: 333972 Color: 16
Size: 250528 Color: 11

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 415516 Color: 19
Size: 309317 Color: 17
Size: 275168 Color: 18

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 415827 Color: 2
Size: 300452 Color: 8
Size: 283722 Color: 11

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 415572 Color: 12
Size: 315824 Color: 5
Size: 268605 Color: 10

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 415589 Color: 12
Size: 314252 Color: 6
Size: 270160 Color: 14

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 415800 Color: 18
Size: 332107 Color: 17
Size: 252094 Color: 16

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 415801 Color: 9
Size: 293482 Color: 4
Size: 290718 Color: 7

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 415891 Color: 8
Size: 292603 Color: 18
Size: 291507 Color: 18

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 416154 Color: 2
Size: 324126 Color: 6
Size: 259721 Color: 10

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 415898 Color: 15
Size: 311087 Color: 18
Size: 273016 Color: 6

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 415972 Color: 7
Size: 306934 Color: 8
Size: 277095 Color: 12

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 415982 Color: 12
Size: 301143 Color: 8
Size: 282876 Color: 17

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 416170 Color: 4
Size: 305449 Color: 3
Size: 278382 Color: 9

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 416387 Color: 2
Size: 311710 Color: 19
Size: 271904 Color: 5

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 416210 Color: 15
Size: 319322 Color: 18
Size: 264469 Color: 3

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 416335 Color: 10
Size: 297471 Color: 0
Size: 286195 Color: 5

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 416353 Color: 5
Size: 330366 Color: 17
Size: 253282 Color: 12

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 416436 Color: 2
Size: 324300 Color: 8
Size: 259265 Color: 14

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 416391 Color: 13
Size: 314868 Color: 0
Size: 268742 Color: 5

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 416497 Color: 7
Size: 303955 Color: 3
Size: 279549 Color: 15

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 416502 Color: 16
Size: 329932 Color: 5
Size: 253567 Color: 10

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 416727 Color: 10
Size: 295283 Color: 13
Size: 287991 Color: 5

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 416900 Color: 2
Size: 326994 Color: 19
Size: 256107 Color: 19

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 416815 Color: 8
Size: 324941 Color: 18
Size: 258245 Color: 11

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 416845 Color: 18
Size: 307738 Color: 7
Size: 275418 Color: 16

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 416895 Color: 11
Size: 301882 Color: 15
Size: 281224 Color: 19

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 416961 Color: 16
Size: 300059 Color: 0
Size: 282981 Color: 5

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 416962 Color: 7
Size: 295638 Color: 18
Size: 287401 Color: 10

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 417063 Color: 19
Size: 304073 Color: 1
Size: 278865 Color: 18

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 417242 Color: 2
Size: 320368 Color: 19
Size: 262391 Color: 11

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 417148 Color: 5
Size: 317136 Color: 12
Size: 265717 Color: 1

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 417236 Color: 12
Size: 320802 Color: 3
Size: 261963 Color: 9

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 417325 Color: 13
Size: 292591 Color: 14
Size: 290085 Color: 8

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 417386 Color: 11
Size: 302217 Color: 2
Size: 280398 Color: 15

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 417401 Color: 7
Size: 295461 Color: 15
Size: 287139 Color: 18

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 417426 Color: 14
Size: 321777 Color: 3
Size: 260798 Color: 19

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 417488 Color: 13
Size: 297697 Color: 11
Size: 284816 Color: 4

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 417520 Color: 1
Size: 317735 Color: 13
Size: 264746 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 417527 Color: 13
Size: 332110 Color: 17
Size: 250364 Color: 14

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 417529 Color: 8
Size: 294242 Color: 4
Size: 288230 Color: 17

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 417541 Color: 8
Size: 296543 Color: 6
Size: 285917 Color: 10

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 417672 Color: 8
Size: 312837 Color: 7
Size: 269492 Color: 13

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 417764 Color: 16
Size: 316698 Color: 8
Size: 265539 Color: 11

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 417913 Color: 0
Size: 313286 Color: 15
Size: 268802 Color: 2

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 417917 Color: 0
Size: 298643 Color: 16
Size: 283441 Color: 13

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 417917 Color: 5
Size: 330332 Color: 7
Size: 251752 Color: 9

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 418008 Color: 6
Size: 307771 Color: 15
Size: 274222 Color: 4

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 418039 Color: 4
Size: 318406 Color: 15
Size: 263556 Color: 11

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 418090 Color: 15
Size: 317015 Color: 14
Size: 264896 Color: 16

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 418136 Color: 17
Size: 329939 Color: 18
Size: 251926 Color: 12

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 418254 Color: 15
Size: 328614 Color: 4
Size: 253133 Color: 8

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 418257 Color: 1
Size: 307372 Color: 12
Size: 274372 Color: 7

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 418367 Color: 6
Size: 301294 Color: 15
Size: 280340 Color: 9

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 418382 Color: 19
Size: 325855 Color: 11
Size: 255764 Color: 13

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 418392 Color: 15
Size: 324186 Color: 5
Size: 257423 Color: 6

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 418393 Color: 14
Size: 325582 Color: 16
Size: 256026 Color: 14

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 418500 Color: 17
Size: 317133 Color: 18
Size: 264368 Color: 16

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 418515 Color: 17
Size: 307318 Color: 12
Size: 274168 Color: 5

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 418574 Color: 12
Size: 309847 Color: 2
Size: 271580 Color: 6

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 418603 Color: 8
Size: 324489 Color: 4
Size: 256909 Color: 14

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 418653 Color: 19
Size: 309124 Color: 19
Size: 272224 Color: 7

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 418660 Color: 17
Size: 293817 Color: 14
Size: 287524 Color: 5

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 418715 Color: 15
Size: 309908 Color: 13
Size: 271378 Color: 1

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 418716 Color: 7
Size: 293546 Color: 5
Size: 287739 Color: 12

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 418738 Color: 7
Size: 314525 Color: 4
Size: 266738 Color: 4

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 418757 Color: 7
Size: 329901 Color: 10
Size: 251343 Color: 16

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 418799 Color: 6
Size: 309824 Color: 5
Size: 271378 Color: 3

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 418854 Color: 8
Size: 293734 Color: 1
Size: 287413 Color: 4

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 418860 Color: 12
Size: 328788 Color: 3
Size: 252353 Color: 7

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 418880 Color: 7
Size: 326421 Color: 10
Size: 254700 Color: 2

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 418915 Color: 18
Size: 302397 Color: 3
Size: 278689 Color: 19

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 418945 Color: 18
Size: 324770 Color: 7
Size: 256286 Color: 1

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 419007 Color: 8
Size: 323890 Color: 0
Size: 257104 Color: 11

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 419051 Color: 17
Size: 323231 Color: 2
Size: 257719 Color: 1

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 419087 Color: 8
Size: 296472 Color: 16
Size: 284442 Color: 11

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 419110 Color: 11
Size: 306475 Color: 14
Size: 274416 Color: 10

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 419294 Color: 6
Size: 326706 Color: 13
Size: 254001 Color: 9

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 419312 Color: 4
Size: 306302 Color: 17
Size: 274387 Color: 3

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 419338 Color: 19
Size: 296944 Color: 3
Size: 283719 Color: 4

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 419346 Color: 10
Size: 323001 Color: 1
Size: 257654 Color: 4

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 419397 Color: 10
Size: 303700 Color: 16
Size: 276904 Color: 8

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 419483 Color: 1
Size: 320857 Color: 14
Size: 259661 Color: 9

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 419483 Color: 3
Size: 311940 Color: 4
Size: 268578 Color: 7

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 419570 Color: 12
Size: 316702 Color: 7
Size: 263729 Color: 1

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 419582 Color: 11
Size: 291575 Color: 5
Size: 288844 Color: 1

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 419864 Color: 7
Size: 327858 Color: 11
Size: 252279 Color: 17

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 419809 Color: 8
Size: 306260 Color: 17
Size: 273932 Color: 12

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 419840 Color: 10
Size: 303495 Color: 4
Size: 276666 Color: 12

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 419882 Color: 2
Size: 314680 Color: 8
Size: 265439 Color: 9

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 419916 Color: 1
Size: 328148 Color: 3
Size: 251937 Color: 12

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 419975 Color: 13
Size: 303848 Color: 17
Size: 276178 Color: 14

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 420111 Color: 7
Size: 314206 Color: 9
Size: 265684 Color: 6

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 420129 Color: 1
Size: 329072 Color: 14
Size: 250800 Color: 6

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 420187 Color: 11
Size: 303157 Color: 16
Size: 276657 Color: 18

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 420187 Color: 10
Size: 324739 Color: 14
Size: 255075 Color: 16

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 420219 Color: 3
Size: 329053 Color: 14
Size: 250729 Color: 11

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 420322 Color: 18
Size: 318032 Color: 0
Size: 261647 Color: 4

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 420332 Color: 12
Size: 307844 Color: 7
Size: 271825 Color: 5

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 420338 Color: 17
Size: 323305 Color: 2
Size: 256358 Color: 12

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 420550 Color: 6
Size: 320424 Color: 4
Size: 259027 Color: 16

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 420570 Color: 7
Size: 315255 Color: 19
Size: 264176 Color: 8

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 420665 Color: 7
Size: 295736 Color: 12
Size: 283600 Color: 19

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 420673 Color: 13
Size: 294815 Color: 13
Size: 284513 Color: 15

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 420810 Color: 19
Size: 325135 Color: 10
Size: 254056 Color: 16

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 420816 Color: 11
Size: 296672 Color: 2
Size: 282513 Color: 17

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 420829 Color: 19
Size: 298792 Color: 8
Size: 280380 Color: 11

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 420880 Color: 18
Size: 292204 Color: 7
Size: 286917 Color: 9

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 420942 Color: 1
Size: 305026 Color: 18
Size: 274033 Color: 8

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 420949 Color: 4
Size: 317305 Color: 10
Size: 261747 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 421325 Color: 19
Size: 314646 Color: 15
Size: 264030 Color: 2

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 421355 Color: 9
Size: 290784 Color: 19
Size: 287862 Color: 19

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 421404 Color: 5
Size: 317408 Color: 3
Size: 261189 Color: 6

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 421420 Color: 19
Size: 324689 Color: 19
Size: 253892 Color: 5

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 421426 Color: 5
Size: 323520 Color: 18
Size: 255055 Color: 13

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 421483 Color: 3
Size: 319396 Color: 15
Size: 259122 Color: 17

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 421511 Color: 10
Size: 305432 Color: 2
Size: 273058 Color: 0

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 421617 Color: 10
Size: 308120 Color: 17
Size: 270264 Color: 14

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 421624 Color: 19
Size: 306582 Color: 15
Size: 271795 Color: 15

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 421664 Color: 12
Size: 316747 Color: 4
Size: 261590 Color: 10

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 421670 Color: 18
Size: 302190 Color: 8
Size: 276141 Color: 5

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 421721 Color: 3
Size: 314013 Color: 1
Size: 264267 Color: 1

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 421838 Color: 2
Size: 289604 Color: 6
Size: 288559 Color: 3

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 421754 Color: 19
Size: 319068 Color: 14
Size: 259179 Color: 8

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 421866 Color: 10
Size: 319128 Color: 0
Size: 259007 Color: 19

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 421899 Color: 0
Size: 307153 Color: 1
Size: 270949 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 421923 Color: 5
Size: 320859 Color: 11
Size: 257219 Color: 16

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 421950 Color: 17
Size: 304286 Color: 2
Size: 273765 Color: 7

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 421974 Color: 8
Size: 319121 Color: 7
Size: 258906 Color: 14

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 422138 Color: 10
Size: 310882 Color: 18
Size: 266981 Color: 19

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 422172 Color: 19
Size: 327466 Color: 6
Size: 250363 Color: 13

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 422188 Color: 6
Size: 309207 Color: 11
Size: 268606 Color: 14

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 422195 Color: 14
Size: 313316 Color: 11
Size: 264490 Color: 0

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 422292 Color: 19
Size: 295571 Color: 3
Size: 282138 Color: 3

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 422306 Color: 16
Size: 290574 Color: 15
Size: 287121 Color: 16

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 422323 Color: 3
Size: 294516 Color: 17
Size: 283162 Color: 17

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 422334 Color: 18
Size: 318489 Color: 5
Size: 259178 Color: 16

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 422437 Color: 15
Size: 305563 Color: 0
Size: 272001 Color: 0

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 422687 Color: 2
Size: 314020 Color: 18
Size: 263294 Color: 9

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 422636 Color: 4
Size: 306450 Color: 8
Size: 270915 Color: 11

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 422658 Color: 3
Size: 326956 Color: 14
Size: 250387 Color: 18

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 422677 Color: 16
Size: 312182 Color: 9
Size: 265142 Color: 16

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 422795 Color: 3
Size: 293249 Color: 11
Size: 283957 Color: 1

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 422830 Color: 2
Size: 324455 Color: 6
Size: 252716 Color: 5

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 422854 Color: 1
Size: 302642 Color: 3
Size: 274505 Color: 16

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 422892 Color: 0
Size: 313446 Color: 19
Size: 263663 Color: 19

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 422927 Color: 14
Size: 303699 Color: 1
Size: 273375 Color: 14

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 423008 Color: 14
Size: 298532 Color: 13
Size: 278461 Color: 12

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 423064 Color: 12
Size: 302814 Color: 1
Size: 274123 Color: 0

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 423088 Color: 0
Size: 296914 Color: 16
Size: 279999 Color: 3

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 423297 Color: 2
Size: 289541 Color: 5
Size: 287163 Color: 14

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 423206 Color: 4
Size: 307760 Color: 17
Size: 269035 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 423241 Color: 3
Size: 314005 Color: 1
Size: 262755 Color: 16

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 423259 Color: 11
Size: 296839 Color: 16
Size: 279903 Color: 7

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 423421 Color: 15
Size: 295880 Color: 6
Size: 280700 Color: 1

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 423468 Color: 9
Size: 317453 Color: 2
Size: 259080 Color: 14

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 423650 Color: 17
Size: 297870 Color: 15
Size: 278481 Color: 4

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 423795 Color: 19
Size: 319033 Color: 12
Size: 257173 Color: 15

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 423797 Color: 12
Size: 323111 Color: 16
Size: 253093 Color: 9

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 423811 Color: 13
Size: 301313 Color: 17
Size: 274877 Color: 15

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 423915 Color: 19
Size: 323079 Color: 16
Size: 253007 Color: 10

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 424009 Color: 2
Size: 317853 Color: 1
Size: 258139 Color: 4

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 423920 Color: 0
Size: 315232 Color: 11
Size: 260849 Color: 11

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 424035 Color: 13
Size: 311318 Color: 16
Size: 264648 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 424116 Color: 3
Size: 298418 Color: 1
Size: 277467 Color: 10

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 424214 Color: 9
Size: 323380 Color: 19
Size: 252407 Color: 2

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 424313 Color: 0
Size: 321963 Color: 3
Size: 253725 Color: 15

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 424331 Color: 10
Size: 315314 Color: 4
Size: 260356 Color: 1

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 424386 Color: 9
Size: 306166 Color: 11
Size: 269449 Color: 4

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 424387 Color: 16
Size: 290151 Color: 3
Size: 285463 Color: 6

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 424429 Color: 8
Size: 294953 Color: 3
Size: 280619 Color: 19

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 424498 Color: 2
Size: 319003 Color: 18
Size: 256500 Color: 1

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 424431 Color: 19
Size: 291777 Color: 7
Size: 283793 Color: 12

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 424457 Color: 7
Size: 315902 Color: 14
Size: 259642 Color: 19

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 424600 Color: 13
Size: 298090 Color: 5
Size: 277311 Color: 7

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 424683 Color: 7
Size: 291959 Color: 12
Size: 283359 Color: 2

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 424691 Color: 7
Size: 305253 Color: 5
Size: 270057 Color: 12

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 424763 Color: 7
Size: 321674 Color: 18
Size: 253564 Color: 15

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 424772 Color: 19
Size: 294848 Color: 12
Size: 280381 Color: 5

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 424808 Color: 17
Size: 318270 Color: 1
Size: 256923 Color: 12

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 424808 Color: 9
Size: 313764 Color: 15
Size: 261429 Color: 2

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 424856 Color: 9
Size: 297561 Color: 12
Size: 277584 Color: 4

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 424948 Color: 4
Size: 312563 Color: 10
Size: 262490 Color: 10

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 424976 Color: 16
Size: 310691 Color: 9
Size: 264334 Color: 17

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 425034 Color: 18
Size: 294392 Color: 18
Size: 280575 Color: 14

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 425093 Color: 17
Size: 318740 Color: 10
Size: 256168 Color: 0

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 425360 Color: 2
Size: 302878 Color: 17
Size: 271763 Color: 9

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 425277 Color: 8
Size: 288314 Color: 18
Size: 286410 Color: 13

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 425323 Color: 4
Size: 309388 Color: 18
Size: 265290 Color: 13

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 425372 Color: 3
Size: 321751 Color: 17
Size: 252878 Color: 14

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 425414 Color: 14
Size: 313479 Color: 2
Size: 261108 Color: 5

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 425437 Color: 13
Size: 296307 Color: 10
Size: 278257 Color: 0

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 425453 Color: 18
Size: 317054 Color: 6
Size: 257494 Color: 11

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 425469 Color: 18
Size: 305328 Color: 11
Size: 269204 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 425481 Color: 6
Size: 318649 Color: 19
Size: 255871 Color: 1

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 425497 Color: 5
Size: 310407 Color: 16
Size: 264097 Color: 8

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 425533 Color: 11
Size: 309796 Color: 8
Size: 264672 Color: 16

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 425620 Color: 6
Size: 317131 Color: 12
Size: 257250 Color: 3

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 425851 Color: 2
Size: 295527 Color: 14
Size: 278623 Color: 9

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 425711 Color: 11
Size: 323922 Color: 13
Size: 250368 Color: 18

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 425787 Color: 9
Size: 313428 Color: 18
Size: 260786 Color: 9

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 425800 Color: 4
Size: 323994 Color: 8
Size: 250207 Color: 8

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 425881 Color: 2
Size: 291749 Color: 1
Size: 282371 Color: 7

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 425827 Color: 3
Size: 291018 Color: 7
Size: 283156 Color: 12

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 425842 Color: 9
Size: 323572 Color: 18
Size: 250587 Color: 3

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 425847 Color: 7
Size: 309516 Color: 19
Size: 264638 Color: 9

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 425909 Color: 18
Size: 302371 Color: 6
Size: 271721 Color: 4

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 425910 Color: 9
Size: 306845 Color: 2
Size: 267246 Color: 7

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 425935 Color: 10
Size: 299591 Color: 13
Size: 274475 Color: 17

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 425942 Color: 18
Size: 287325 Color: 8
Size: 286734 Color: 4

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 425961 Color: 7
Size: 315461 Color: 8
Size: 258579 Color: 6

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 425967 Color: 7
Size: 298744 Color: 0
Size: 275290 Color: 1

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 425995 Color: 15
Size: 310057 Color: 19
Size: 263949 Color: 8

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 426054 Color: 11
Size: 307708 Color: 5
Size: 266239 Color: 13

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 426100 Color: 9
Size: 322519 Color: 0
Size: 251382 Color: 18

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 426129 Color: 1
Size: 310182 Color: 10
Size: 263690 Color: 7

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 426214 Color: 7
Size: 303675 Color: 13
Size: 270112 Color: 11

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 426242 Color: 1
Size: 315866 Color: 7
Size: 257893 Color: 15

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 426267 Color: 17
Size: 293748 Color: 7
Size: 279986 Color: 7

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 426343 Color: 19
Size: 291766 Color: 13
Size: 281892 Color: 5

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 426370 Color: 14
Size: 300315 Color: 8
Size: 273316 Color: 16

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 426381 Color: 0
Size: 290472 Color: 17
Size: 283148 Color: 1

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 426491 Color: 17
Size: 291863 Color: 15
Size: 281647 Color: 14

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 426556 Color: 2
Size: 308395 Color: 8
Size: 265050 Color: 19

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 426523 Color: 11
Size: 318441 Color: 16
Size: 255037 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 426533 Color: 6
Size: 314829 Color: 1
Size: 258639 Color: 11

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 426549 Color: 16
Size: 295407 Color: 14
Size: 278045 Color: 3

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 426601 Color: 12
Size: 299542 Color: 14
Size: 273858 Color: 19

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 426606 Color: 16
Size: 289967 Color: 7
Size: 283428 Color: 2

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 426644 Color: 1
Size: 287808 Color: 18
Size: 285549 Color: 13

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 426791 Color: 8
Size: 302793 Color: 11
Size: 270417 Color: 3

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 426807 Color: 11
Size: 319919 Color: 5
Size: 253275 Color: 7

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 426895 Color: 6
Size: 288634 Color: 12
Size: 284472 Color: 2

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 426928 Color: 3
Size: 288895 Color: 19
Size: 284178 Color: 5

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 426998 Color: 0
Size: 298733 Color: 5
Size: 274270 Color: 6

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 427033 Color: 18
Size: 321300 Color: 7
Size: 251668 Color: 14

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 427053 Color: 19
Size: 310561 Color: 19
Size: 262387 Color: 2

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 427142 Color: 8
Size: 303840 Color: 18
Size: 269019 Color: 10

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 427151 Color: 9
Size: 305206 Color: 11
Size: 267644 Color: 15

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 427184 Color: 11
Size: 304765 Color: 18
Size: 268052 Color: 3

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 427259 Color: 11
Size: 310793 Color: 7
Size: 261949 Color: 14

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 427328 Color: 10
Size: 316564 Color: 0
Size: 256109 Color: 19

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 427390 Color: 17
Size: 300159 Color: 15
Size: 272452 Color: 2

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 427483 Color: 11
Size: 304997 Color: 1
Size: 267521 Color: 7

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 427561 Color: 9
Size: 290240 Color: 13
Size: 282200 Color: 1

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 427609 Color: 15
Size: 305480 Color: 11
Size: 266912 Color: 17

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 427640 Color: 11
Size: 314410 Color: 6
Size: 257951 Color: 8

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 427707 Color: 2
Size: 307097 Color: 8
Size: 265197 Color: 8

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 427722 Color: 9
Size: 289613 Color: 5
Size: 282666 Color: 3

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 427749 Color: 14
Size: 290948 Color: 8
Size: 281304 Color: 18

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 427852 Color: 11
Size: 292448 Color: 17
Size: 279701 Color: 5

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 427890 Color: 10
Size: 313670 Color: 15
Size: 258441 Color: 7

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 427891 Color: 3
Size: 311503 Color: 4
Size: 260607 Color: 15

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 427901 Color: 15
Size: 314797 Color: 2
Size: 257303 Color: 8

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 427936 Color: 3
Size: 302914 Color: 9
Size: 269151 Color: 4

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 427949 Color: 11
Size: 298185 Color: 16
Size: 273867 Color: 5

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 427962 Color: 17
Size: 287248 Color: 6
Size: 284791 Color: 16

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 427991 Color: 2
Size: 306259 Color: 7
Size: 265751 Color: 8

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 428010 Color: 0
Size: 308284 Color: 19
Size: 263707 Color: 18

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 428020 Color: 8
Size: 305342 Color: 7
Size: 266639 Color: 6

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 428112 Color: 15
Size: 299490 Color: 17
Size: 272399 Color: 15

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 428201 Color: 15
Size: 289532 Color: 13
Size: 282268 Color: 5

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 428206 Color: 0
Size: 304865 Color: 0
Size: 266930 Color: 9

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 428310 Color: 8
Size: 301535 Color: 16
Size: 270156 Color: 2

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 428320 Color: 15
Size: 298907 Color: 13
Size: 272774 Color: 10

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 428322 Color: 12
Size: 319762 Color: 3
Size: 251917 Color: 18

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 428412 Color: 7
Size: 320484 Color: 5
Size: 251105 Color: 18

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 428434 Color: 14
Size: 321028 Color: 10
Size: 250539 Color: 17

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 428457 Color: 18
Size: 296485 Color: 6
Size: 275059 Color: 11

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 428649 Color: 16
Size: 288972 Color: 9
Size: 282380 Color: 4

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 428675 Color: 1
Size: 286801 Color: 19
Size: 284525 Color: 15

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 428744 Color: 13
Size: 310914 Color: 12
Size: 260343 Color: 1

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 428756 Color: 4
Size: 293731 Color: 14
Size: 277514 Color: 19

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 428821 Color: 9
Size: 312463 Color: 2
Size: 258717 Color: 15

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 428842 Color: 14
Size: 318161 Color: 16
Size: 252998 Color: 2

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 428857 Color: 18
Size: 320493 Color: 14
Size: 250651 Color: 18

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 429049 Color: 16
Size: 310707 Color: 0
Size: 260245 Color: 19

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 429066 Color: 8
Size: 313009 Color: 12
Size: 257926 Color: 15

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 429352 Color: 7
Size: 290790 Color: 2
Size: 279859 Color: 13

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 429402 Color: 11
Size: 295022 Color: 1
Size: 275577 Color: 4

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 429440 Color: 5
Size: 301911 Color: 10
Size: 268650 Color: 19

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 429531 Color: 7
Size: 294113 Color: 6
Size: 276357 Color: 13

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 429560 Color: 12
Size: 305618 Color: 18
Size: 264823 Color: 19

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 429571 Color: 3
Size: 291934 Color: 2
Size: 278496 Color: 13

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 429603 Color: 10
Size: 313536 Color: 6
Size: 256862 Color: 12

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 429664 Color: 15
Size: 305639 Color: 18
Size: 264698 Color: 3

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 429785 Color: 8
Size: 302761 Color: 11
Size: 267455 Color: 7

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 429954 Color: 16
Size: 314586 Color: 9
Size: 255461 Color: 0

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 429966 Color: 14
Size: 303851 Color: 18
Size: 266184 Color: 19

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 430065 Color: 5
Size: 315762 Color: 4
Size: 254174 Color: 14

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 430116 Color: 15
Size: 302436 Color: 3
Size: 267449 Color: 16

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 430147 Color: 7
Size: 295325 Color: 6
Size: 274529 Color: 18

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 430209 Color: 12
Size: 298585 Color: 4
Size: 271207 Color: 15

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 430248 Color: 16
Size: 288148 Color: 4
Size: 281605 Color: 14

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 430260 Color: 1
Size: 288163 Color: 5
Size: 281578 Color: 12

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 430275 Color: 3
Size: 313475 Color: 5
Size: 256251 Color: 2

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 430296 Color: 5
Size: 306245 Color: 1
Size: 263460 Color: 3

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 430374 Color: 14
Size: 307865 Color: 0
Size: 261762 Color: 19

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 430421 Color: 6
Size: 313117 Color: 14
Size: 256463 Color: 12

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 430519 Color: 19
Size: 287418 Color: 11
Size: 282064 Color: 9

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 430741 Color: 12
Size: 301381 Color: 7
Size: 267879 Color: 2

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 430748 Color: 19
Size: 318949 Color: 15
Size: 250304 Color: 0

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 430771 Color: 9
Size: 317597 Color: 16
Size: 251633 Color: 14

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 430803 Color: 4
Size: 313075 Color: 11
Size: 256123 Color: 17

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 430805 Color: 9
Size: 311852 Color: 16
Size: 257344 Color: 17

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 430899 Color: 4
Size: 306766 Color: 10
Size: 262336 Color: 2

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 431043 Color: 17
Size: 290452 Color: 15
Size: 278506 Color: 13

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 431168 Color: 5
Size: 299594 Color: 18
Size: 269239 Color: 4

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 431237 Color: 3
Size: 317833 Color: 19
Size: 250931 Color: 13

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 431341 Color: 18
Size: 310490 Color: 8
Size: 258170 Color: 9

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 431351 Color: 13
Size: 314435 Color: 4
Size: 254215 Color: 19

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 431355 Color: 13
Size: 302406 Color: 10
Size: 266240 Color: 19

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 431729 Color: 2
Size: 288484 Color: 3
Size: 279788 Color: 4

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 431705 Color: 19
Size: 296759 Color: 19
Size: 271537 Color: 12

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 431747 Color: 16
Size: 317179 Color: 4
Size: 251075 Color: 2

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 431816 Color: 12
Size: 291011 Color: 17
Size: 277174 Color: 3

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 431865 Color: 0
Size: 303138 Color: 4
Size: 264998 Color: 15

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 431873 Color: 12
Size: 303599 Color: 14
Size: 264529 Color: 3

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 431882 Color: 8
Size: 300039 Color: 11
Size: 268080 Color: 19

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 431948 Color: 4
Size: 285614 Color: 0
Size: 282439 Color: 11

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 431956 Color: 13
Size: 305146 Color: 15
Size: 262899 Color: 18

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 431962 Color: 16
Size: 288822 Color: 3
Size: 279217 Color: 11

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 432008 Color: 6
Size: 308170 Color: 5
Size: 259823 Color: 16

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 432015 Color: 18
Size: 302229 Color: 8
Size: 265757 Color: 3

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 432028 Color: 14
Size: 298826 Color: 4
Size: 269147 Color: 13

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 432036 Color: 6
Size: 317704 Color: 7
Size: 250261 Color: 14

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 432057 Color: 5
Size: 292681 Color: 4
Size: 275263 Color: 17

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 432136 Color: 12
Size: 293459 Color: 16
Size: 274406 Color: 9

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 432236 Color: 6
Size: 296640 Color: 4
Size: 271125 Color: 1

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 432269 Color: 1
Size: 291595 Color: 18
Size: 276137 Color: 10

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 432561 Color: 0
Size: 284774 Color: 17
Size: 282666 Color: 11

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 432633 Color: 4
Size: 298647 Color: 7
Size: 268721 Color: 4

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 432719 Color: 4
Size: 299432 Color: 14
Size: 267850 Color: 5

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 432733 Color: 14
Size: 293264 Color: 6
Size: 274004 Color: 10

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 432939 Color: 13
Size: 309132 Color: 19
Size: 257930 Color: 4

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 433009 Color: 5
Size: 287456 Color: 10
Size: 279536 Color: 17

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 433035 Color: 1
Size: 287869 Color: 6
Size: 279097 Color: 17

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 433055 Color: 11
Size: 285990 Color: 11
Size: 280956 Color: 12

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 433116 Color: 17
Size: 307662 Color: 10
Size: 259223 Color: 6

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 433175 Color: 2
Size: 310733 Color: 5
Size: 256093 Color: 11

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 433214 Color: 3
Size: 287400 Color: 7
Size: 279387 Color: 15

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 433296 Color: 4
Size: 284291 Color: 1
Size: 282414 Color: 4

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 433318 Color: 5
Size: 301836 Color: 5
Size: 264847 Color: 1

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 433423 Color: 4
Size: 315862 Color: 19
Size: 250716 Color: 18

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 433649 Color: 5
Size: 290867 Color: 2
Size: 275485 Color: 13

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 433671 Color: 13
Size: 298573 Color: 4
Size: 267757 Color: 16

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 433674 Color: 4
Size: 294651 Color: 13
Size: 271676 Color: 0

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 433898 Color: 9
Size: 284175 Color: 16
Size: 281928 Color: 16

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 433905 Color: 11
Size: 303287 Color: 19
Size: 262809 Color: 13

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 433943 Color: 13
Size: 313904 Color: 5
Size: 252154 Color: 8

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 434127 Color: 4
Size: 295281 Color: 5
Size: 270593 Color: 14

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 434198 Color: 2
Size: 306062 Color: 10
Size: 259741 Color: 15

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 434152 Color: 19
Size: 295389 Color: 17
Size: 270460 Color: 9

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 434174 Color: 19
Size: 305004 Color: 5
Size: 260823 Color: 13

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 434224 Color: 4
Size: 309906 Color: 6
Size: 255871 Color: 11

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 434240 Color: 14
Size: 312628 Color: 19
Size: 253133 Color: 6

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 434250 Color: 19
Size: 297660 Color: 12
Size: 268091 Color: 17

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 434332 Color: 2
Size: 314993 Color: 6
Size: 250676 Color: 4

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 434272 Color: 1
Size: 287499 Color: 9
Size: 278230 Color: 15

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 434367 Color: 5
Size: 305899 Color: 3
Size: 259735 Color: 8

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 434370 Color: 11
Size: 298569 Color: 9
Size: 267062 Color: 15

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 434378 Color: 6
Size: 298204 Color: 11
Size: 267419 Color: 0

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 434412 Color: 19
Size: 295640 Color: 6
Size: 269949 Color: 5

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 434478 Color: 2
Size: 298067 Color: 17
Size: 267456 Color: 14

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 434442 Color: 4
Size: 283262 Color: 11
Size: 282297 Color: 5

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 434449 Color: 16
Size: 286375 Color: 10
Size: 279177 Color: 11

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 434551 Color: 17
Size: 285471 Color: 19
Size: 279979 Color: 15

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 434576 Color: 10
Size: 287741 Color: 11
Size: 277684 Color: 17

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 434579 Color: 18
Size: 294737 Color: 10
Size: 270685 Color: 7

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 434634 Color: 2
Size: 294210 Color: 11
Size: 271157 Color: 1

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 434586 Color: 19
Size: 294367 Color: 16
Size: 271048 Color: 19

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 434620 Color: 6
Size: 315342 Color: 17
Size: 250039 Color: 11

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 434652 Color: 0
Size: 288991 Color: 3
Size: 276358 Color: 19

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 434667 Color: 3
Size: 293322 Color: 18
Size: 272012 Color: 19

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 434670 Color: 3
Size: 310139 Color: 16
Size: 255192 Color: 2

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 434735 Color: 13
Size: 302017 Color: 15
Size: 263249 Color: 14

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 434765 Color: 1
Size: 310175 Color: 10
Size: 255061 Color: 10

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 434833 Color: 12
Size: 285534 Color: 14
Size: 279634 Color: 8

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 434850 Color: 8
Size: 286134 Color: 1
Size: 279017 Color: 7

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 434853 Color: 0
Size: 304554 Color: 15
Size: 260594 Color: 18

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 434882 Color: 1
Size: 307714 Color: 0
Size: 257405 Color: 19

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 434911 Color: 1
Size: 293962 Color: 7
Size: 271128 Color: 4

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 434993 Color: 15
Size: 302550 Color: 5
Size: 262458 Color: 18

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 434994 Color: 13
Size: 293955 Color: 7
Size: 271052 Color: 15

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 435057 Color: 3
Size: 286149 Color: 14
Size: 278795 Color: 12

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 435212 Color: 2
Size: 295312 Color: 6
Size: 269477 Color: 9

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 435362 Color: 2
Size: 297097 Color: 10
Size: 267542 Color: 4

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 435263 Color: 18
Size: 304264 Color: 5
Size: 260474 Color: 14

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 435306 Color: 12
Size: 292166 Color: 16
Size: 272529 Color: 10

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 435427 Color: 19
Size: 301202 Color: 2
Size: 263372 Color: 16

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 435455 Color: 15
Size: 311536 Color: 13
Size: 253010 Color: 18

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 435466 Color: 11
Size: 287653 Color: 8
Size: 276882 Color: 18

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 435505 Color: 16
Size: 290194 Color: 6
Size: 274302 Color: 1

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 435578 Color: 11
Size: 304940 Color: 12
Size: 259483 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 435664 Color: 18
Size: 285394 Color: 9
Size: 278943 Color: 5

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 435718 Color: 1
Size: 298837 Color: 1
Size: 265446 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 435803 Color: 7
Size: 298182 Color: 12
Size: 266016 Color: 8

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 435906 Color: 5
Size: 290462 Color: 12
Size: 273633 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 435921 Color: 1
Size: 300677 Color: 15
Size: 263403 Color: 10

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 435934 Color: 9
Size: 302372 Color: 19
Size: 261695 Color: 15

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 435995 Color: 0
Size: 309485 Color: 11
Size: 254521 Color: 18

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 436009 Color: 7
Size: 304758 Color: 16
Size: 259234 Color: 17

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 436032 Color: 1
Size: 310027 Color: 14
Size: 253942 Color: 6

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 436037 Color: 8
Size: 299562 Color: 12
Size: 264402 Color: 13

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 436046 Color: 3
Size: 290852 Color: 7
Size: 273103 Color: 18

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 436160 Color: 18
Size: 293472 Color: 17
Size: 270369 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 436249 Color: 17
Size: 290171 Color: 16
Size: 273581 Color: 13

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 436307 Color: 0
Size: 297413 Color: 9
Size: 266281 Color: 12

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 436386 Color: 7
Size: 299559 Color: 4
Size: 264056 Color: 17

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 436545 Color: 6
Size: 307450 Color: 19
Size: 256006 Color: 18

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 436624 Color: 7
Size: 308133 Color: 16
Size: 255244 Color: 18

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 436713 Color: 2
Size: 301578 Color: 3
Size: 261710 Color: 9

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 436822 Color: 12
Size: 306950 Color: 8
Size: 256229 Color: 13

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 436836 Color: 11
Size: 306471 Color: 12
Size: 256694 Color: 19

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 436871 Color: 13
Size: 284355 Color: 6
Size: 278775 Color: 0

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 436934 Color: 2
Size: 281613 Color: 4
Size: 281454 Color: 8

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 436980 Color: 16
Size: 301046 Color: 1
Size: 261975 Color: 13

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 436982 Color: 6
Size: 299185 Color: 1
Size: 263834 Color: 8

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 437038 Color: 19
Size: 294890 Color: 6
Size: 268073 Color: 9

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 436990 Color: 18
Size: 292152 Color: 3
Size: 270859 Color: 10

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 436992 Color: 9
Size: 313005 Color: 0
Size: 250004 Color: 13

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 437069 Color: 18
Size: 283924 Color: 14
Size: 279008 Color: 7

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 437094 Color: 14
Size: 300538 Color: 13
Size: 262369 Color: 11

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 437130 Color: 19
Size: 286674 Color: 14
Size: 276197 Color: 2

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 437137 Color: 18
Size: 286474 Color: 4
Size: 276390 Color: 7

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 437186 Color: 17
Size: 298461 Color: 16
Size: 264354 Color: 16

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 437237 Color: 15
Size: 282187 Color: 6
Size: 280577 Color: 1

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 437270 Color: 16
Size: 298619 Color: 9
Size: 264112 Color: 10

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 437329 Color: 5
Size: 297821 Color: 15
Size: 264851 Color: 6

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 437331 Color: 0
Size: 302881 Color: 2
Size: 259789 Color: 12

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 437575 Color: 19
Size: 289348 Color: 17
Size: 273078 Color: 5

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 437490 Color: 12
Size: 309257 Color: 15
Size: 253254 Color: 10

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 437638 Color: 13
Size: 301525 Color: 1
Size: 260838 Color: 2

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 437721 Color: 0
Size: 309523 Color: 3
Size: 252757 Color: 1

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 437728 Color: 17
Size: 292357 Color: 4
Size: 269916 Color: 9

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 437764 Color: 14
Size: 290643 Color: 19
Size: 271594 Color: 11

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 437774 Color: 15
Size: 301138 Color: 9
Size: 261089 Color: 6

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 437778 Color: 16
Size: 304077 Color: 17
Size: 258146 Color: 7

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 437870 Color: 8
Size: 292067 Color: 18
Size: 270064 Color: 6

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 437914 Color: 17
Size: 304266 Color: 14
Size: 257821 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 438022 Color: 16
Size: 308050 Color: 6
Size: 253929 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 438026 Color: 13
Size: 283728 Color: 2
Size: 278247 Color: 19

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 438085 Color: 10
Size: 283690 Color: 10
Size: 278226 Color: 6

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 438128 Color: 5
Size: 298834 Color: 16
Size: 263039 Color: 2

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 438150 Color: 18
Size: 308978 Color: 16
Size: 252873 Color: 12

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 438221 Color: 1
Size: 298528 Color: 15
Size: 263252 Color: 12

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 438229 Color: 12
Size: 301673 Color: 15
Size: 260099 Color: 6

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 438318 Color: 4
Size: 286086 Color: 19
Size: 275597 Color: 9

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 438445 Color: 4
Size: 292869 Color: 4
Size: 268687 Color: 12

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 438686 Color: 12
Size: 297430 Color: 6
Size: 263885 Color: 14

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 438745 Color: 17
Size: 307697 Color: 18
Size: 253559 Color: 1

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 438756 Color: 15
Size: 297459 Color: 19
Size: 263786 Color: 17

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 438801 Color: 8
Size: 292173 Color: 10
Size: 269027 Color: 16

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 438808 Color: 4
Size: 293560 Color: 5
Size: 267633 Color: 5

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 438820 Color: 11
Size: 308268 Color: 9
Size: 252913 Color: 15

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 438899 Color: 16
Size: 286215 Color: 18
Size: 274887 Color: 10

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 438932 Color: 8
Size: 282503 Color: 12
Size: 278566 Color: 12

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 438991 Color: 14
Size: 296391 Color: 4
Size: 264619 Color: 19

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 439013 Color: 4
Size: 290573 Color: 1
Size: 270415 Color: 11

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 439038 Color: 10
Size: 306152 Color: 0
Size: 254811 Color: 18

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 439077 Color: 17
Size: 304404 Color: 8
Size: 256520 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 439089 Color: 7
Size: 305728 Color: 12
Size: 255184 Color: 1

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 439113 Color: 11
Size: 296669 Color: 7
Size: 264219 Color: 0

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 439116 Color: 3
Size: 310819 Color: 19
Size: 250066 Color: 15

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 439241 Color: 1
Size: 303079 Color: 8
Size: 257681 Color: 14

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 439307 Color: 3
Size: 286440 Color: 14
Size: 274254 Color: 17

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 439419 Color: 7
Size: 304549 Color: 5
Size: 256033 Color: 15

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 439425 Color: 16
Size: 281906 Color: 14
Size: 278670 Color: 11

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 439496 Color: 8
Size: 303576 Color: 8
Size: 256929 Color: 0

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 439511 Color: 12
Size: 281001 Color: 15
Size: 279489 Color: 7

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 439521 Color: 4
Size: 294676 Color: 11
Size: 265804 Color: 5

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 439530 Color: 0
Size: 295128 Color: 15
Size: 265343 Color: 13

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 439618 Color: 5
Size: 280894 Color: 3
Size: 279489 Color: 9

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 439643 Color: 7
Size: 286965 Color: 14
Size: 273393 Color: 17

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 439671 Color: 0
Size: 304173 Color: 8
Size: 256157 Color: 17

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 439726 Color: 17
Size: 296148 Color: 11
Size: 264127 Color: 3

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 439784 Color: 17
Size: 282310 Color: 1
Size: 277907 Color: 9

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 439808 Color: 15
Size: 293001 Color: 6
Size: 267192 Color: 9

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 439875 Color: 4
Size: 281125 Color: 9
Size: 279001 Color: 4

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 439959 Color: 12
Size: 308649 Color: 13
Size: 251393 Color: 3

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 440142 Color: 1
Size: 279965 Color: 13
Size: 279894 Color: 13

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 440346 Color: 19
Size: 282934 Color: 0
Size: 276721 Color: 13

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 440268 Color: 12
Size: 309632 Color: 0
Size: 250101 Color: 10

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 440285 Color: 8
Size: 304277 Color: 18
Size: 255439 Color: 1

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 440341 Color: 16
Size: 286859 Color: 5
Size: 272801 Color: 6

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 440431 Color: 19
Size: 305145 Color: 8
Size: 254425 Color: 11

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 440408 Color: 15
Size: 284781 Color: 4
Size: 274812 Color: 15

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 440637 Color: 9
Size: 297050 Color: 18
Size: 262314 Color: 1

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 440654 Color: 10
Size: 303122 Color: 17
Size: 256225 Color: 11

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 440744 Color: 7
Size: 283595 Color: 15
Size: 275662 Color: 11

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 440762 Color: 10
Size: 294217 Color: 19
Size: 265022 Color: 18

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 440844 Color: 18
Size: 296179 Color: 0
Size: 262978 Color: 14

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 440887 Color: 0
Size: 285853 Color: 11
Size: 273261 Color: 10

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 440935 Color: 17
Size: 299815 Color: 17
Size: 259251 Color: 9

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 441001 Color: 0
Size: 302237 Color: 6
Size: 256763 Color: 2

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 441029 Color: 12
Size: 295309 Color: 1
Size: 263663 Color: 19

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 441042 Color: 3
Size: 292901 Color: 0
Size: 266058 Color: 9

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 441084 Color: 16
Size: 296459 Color: 3
Size: 262458 Color: 1

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 441251 Color: 17
Size: 301745 Color: 5
Size: 257005 Color: 1

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 441445 Color: 4
Size: 306426 Color: 18
Size: 252130 Color: 10

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 441494 Color: 15
Size: 283523 Color: 19
Size: 274984 Color: 5

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 441512 Color: 17
Size: 295756 Color: 4
Size: 262733 Color: 6

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 441576 Color: 17
Size: 280343 Color: 10
Size: 278082 Color: 12

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 441766 Color: 12
Size: 290134 Color: 13
Size: 268101 Color: 1

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 441779 Color: 8
Size: 302252 Color: 1
Size: 255970 Color: 6

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 441817 Color: 4
Size: 306712 Color: 13
Size: 251472 Color: 19

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 441818 Color: 0
Size: 297218 Color: 7
Size: 260965 Color: 15

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 441937 Color: 5
Size: 297334 Color: 2
Size: 260730 Color: 9

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 441939 Color: 11
Size: 300155 Color: 12
Size: 257907 Color: 2

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 441955 Color: 13
Size: 292330 Color: 9
Size: 265716 Color: 8

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 441963 Color: 11
Size: 293492 Color: 12
Size: 264546 Color: 1

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 442013 Color: 0
Size: 284438 Color: 13
Size: 273550 Color: 19

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 442051 Color: 2
Size: 297376 Color: 0
Size: 260574 Color: 9

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 442073 Color: 7
Size: 286120 Color: 13
Size: 271808 Color: 2

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 442092 Color: 1
Size: 292438 Color: 12
Size: 265471 Color: 7

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 442119 Color: 8
Size: 306860 Color: 4
Size: 251022 Color: 5

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 442189 Color: 7
Size: 303683 Color: 5
Size: 254129 Color: 15

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 442257 Color: 8
Size: 289858 Color: 13
Size: 267886 Color: 17

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 442280 Color: 12
Size: 299231 Color: 1
Size: 258490 Color: 3

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 442285 Color: 1
Size: 299928 Color: 16
Size: 257788 Color: 8

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 442291 Color: 6
Size: 284819 Color: 11
Size: 272891 Color: 15

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 442320 Color: 8
Size: 291322 Color: 6
Size: 266359 Color: 9

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 442337 Color: 7
Size: 299358 Color: 13
Size: 258306 Color: 19

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 442417 Color: 5
Size: 302918 Color: 6
Size: 254666 Color: 6

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 442485 Color: 13
Size: 292393 Color: 14
Size: 265123 Color: 18

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 442554 Color: 8
Size: 297047 Color: 2
Size: 260400 Color: 8

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 442886 Color: 7
Size: 306140 Color: 3
Size: 250975 Color: 7

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 442887 Color: 12
Size: 303346 Color: 14
Size: 253768 Color: 4

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 442961 Color: 8
Size: 293428 Color: 1
Size: 263612 Color: 11

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 443123 Color: 19
Size: 290366 Color: 3
Size: 266512 Color: 17

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 442974 Color: 8
Size: 305830 Color: 7
Size: 251197 Color: 4

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 443047 Color: 0
Size: 281555 Color: 11
Size: 275399 Color: 5

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 443146 Color: 6
Size: 290006 Color: 19
Size: 266849 Color: 14

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 443149 Color: 17
Size: 306829 Color: 14
Size: 250023 Color: 2

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 443172 Color: 1
Size: 293782 Color: 3
Size: 263047 Color: 17

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 443180 Color: 0
Size: 280328 Color: 9
Size: 276493 Color: 7

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 443340 Color: 12
Size: 284837 Color: 3
Size: 271824 Color: 7

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 443599 Color: 10
Size: 284940 Color: 16
Size: 271462 Color: 1

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 443686 Color: 17
Size: 289904 Color: 19
Size: 266411 Color: 16

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 443691 Color: 8
Size: 286955 Color: 12
Size: 269355 Color: 7

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 443828 Color: 18
Size: 303370 Color: 2
Size: 252803 Color: 13

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 443888 Color: 9
Size: 284567 Color: 17
Size: 271546 Color: 7

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 443898 Color: 11
Size: 283223 Color: 2
Size: 272880 Color: 16

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 443915 Color: 16
Size: 280585 Color: 19
Size: 275501 Color: 17

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 444005 Color: 8
Size: 293688 Color: 17
Size: 262308 Color: 11

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 444070 Color: 17
Size: 294932 Color: 8
Size: 260999 Color: 12

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 444089 Color: 0
Size: 305608 Color: 1
Size: 250304 Color: 12

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 444142 Color: 16
Size: 304596 Color: 6
Size: 251263 Color: 5

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 444146 Color: 11
Size: 279889 Color: 2
Size: 275966 Color: 10

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 444287 Color: 19
Size: 278115 Color: 15
Size: 277599 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 444277 Color: 11
Size: 287871 Color: 0
Size: 267853 Color: 15

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 444393 Color: 9
Size: 287503 Color: 8
Size: 268105 Color: 11

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 444393 Color: 7
Size: 304500 Color: 16
Size: 251108 Color: 9

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 444479 Color: 2
Size: 287311 Color: 8
Size: 268211 Color: 4

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 444643 Color: 9
Size: 304165 Color: 18
Size: 251193 Color: 4

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 444752 Color: 9
Size: 304832 Color: 19
Size: 250417 Color: 14

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 444792 Color: 3
Size: 278495 Color: 10
Size: 276714 Color: 9

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 444819 Color: 17
Size: 278461 Color: 13
Size: 276721 Color: 13

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 444849 Color: 6
Size: 297927 Color: 2
Size: 257225 Color: 0

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 444962 Color: 8
Size: 279388 Color: 8
Size: 275651 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 444991 Color: 3
Size: 292207 Color: 13
Size: 262803 Color: 18

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 445005 Color: 15
Size: 286843 Color: 19
Size: 268153 Color: 11

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 445084 Color: 5
Size: 279614 Color: 5
Size: 275303 Color: 17

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 445135 Color: 7
Size: 299338 Color: 3
Size: 255528 Color: 10

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 445179 Color: 13
Size: 278238 Color: 3
Size: 276584 Color: 6

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 445236 Color: 2
Size: 283280 Color: 3
Size: 271485 Color: 12

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 445237 Color: 16
Size: 300593 Color: 1
Size: 254171 Color: 18

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 445287 Color: 0
Size: 298848 Color: 17
Size: 255866 Color: 5

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 445300 Color: 6
Size: 298949 Color: 6
Size: 255752 Color: 0

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 445337 Color: 1
Size: 294845 Color: 3
Size: 259819 Color: 5

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 445350 Color: 4
Size: 298495 Color: 13
Size: 256156 Color: 16

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 445414 Color: 14
Size: 295144 Color: 13
Size: 259443 Color: 3

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 445525 Color: 0
Size: 303181 Color: 13
Size: 251295 Color: 7

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 445584 Color: 0
Size: 283014 Color: 14
Size: 271403 Color: 6

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 445586 Color: 8
Size: 298032 Color: 8
Size: 256383 Color: 17

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 445634 Color: 15
Size: 281844 Color: 3
Size: 272523 Color: 4

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 445655 Color: 10
Size: 293121 Color: 18
Size: 261225 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 445679 Color: 9
Size: 297831 Color: 19
Size: 256491 Color: 4

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 445798 Color: 18
Size: 304066 Color: 7
Size: 250137 Color: 10

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 445805 Color: 2
Size: 277791 Color: 7
Size: 276405 Color: 16

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 445877 Color: 12
Size: 278098 Color: 7
Size: 276026 Color: 13

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 445890 Color: 1
Size: 284869 Color: 4
Size: 269242 Color: 5

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 445978 Color: 5
Size: 298913 Color: 6
Size: 255110 Color: 19

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 445996 Color: 1
Size: 277308 Color: 7
Size: 276697 Color: 6

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 446006 Color: 18
Size: 283455 Color: 5
Size: 270540 Color: 2

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 446036 Color: 2
Size: 294595 Color: 0
Size: 259370 Color: 13

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 446082 Color: 4
Size: 278659 Color: 2
Size: 275260 Color: 6

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 446133 Color: 7
Size: 301466 Color: 13
Size: 252402 Color: 17

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 446297 Color: 2
Size: 293557 Color: 10
Size: 260147 Color: 5

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 446306 Color: 12
Size: 281901 Color: 18
Size: 271794 Color: 10

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 446378 Color: 2
Size: 277640 Color: 7
Size: 275983 Color: 17

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 446500 Color: 11
Size: 297318 Color: 5
Size: 256183 Color: 16

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 446515 Color: 12
Size: 299335 Color: 5
Size: 254151 Color: 12

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 446543 Color: 5
Size: 286538 Color: 8
Size: 266920 Color: 19

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 446569 Color: 6
Size: 303387 Color: 3
Size: 250045 Color: 5

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 446599 Color: 9
Size: 297166 Color: 3
Size: 256236 Color: 1

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 446817 Color: 14
Size: 288680 Color: 18
Size: 264504 Color: 3

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 446835 Color: 0
Size: 289945 Color: 4
Size: 263221 Color: 10

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 446922 Color: 13
Size: 298347 Color: 17
Size: 254732 Color: 7

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 447162 Color: 19
Size: 283177 Color: 12
Size: 269662 Color: 6

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 447026 Color: 4
Size: 277646 Color: 2
Size: 275329 Color: 3

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 447087 Color: 5
Size: 301659 Color: 18
Size: 251255 Color: 18

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 447124 Color: 15
Size: 283528 Color: 12
Size: 269349 Color: 17

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 447128 Color: 11
Size: 300019 Color: 3
Size: 252854 Color: 17

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 447167 Color: 16
Size: 301875 Color: 11
Size: 250959 Color: 0

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 447516 Color: 19
Size: 281857 Color: 6
Size: 270628 Color: 10

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 447335 Color: 1
Size: 281813 Color: 4
Size: 270853 Color: 10

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 447353 Color: 15
Size: 281121 Color: 16
Size: 271527 Color: 10

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 447381 Color: 6
Size: 281815 Color: 13
Size: 270805 Color: 12

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 447389 Color: 7
Size: 285469 Color: 1
Size: 267143 Color: 14

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 447423 Color: 15
Size: 293536 Color: 10
Size: 259042 Color: 13

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 447724 Color: 19
Size: 283865 Color: 15
Size: 268412 Color: 8

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 447564 Color: 14
Size: 289835 Color: 2
Size: 262602 Color: 4

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 447565 Color: 16
Size: 277238 Color: 1
Size: 275198 Color: 1

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 447654 Color: 8
Size: 289631 Color: 0
Size: 262716 Color: 7

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 447805 Color: 19
Size: 281351 Color: 2
Size: 270845 Color: 11

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 447735 Color: 9
Size: 276330 Color: 0
Size: 275936 Color: 0

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 447760 Color: 14
Size: 295844 Color: 4
Size: 256397 Color: 8

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 447829 Color: 16
Size: 292394 Color: 19
Size: 259778 Color: 5

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 447893 Color: 6
Size: 290175 Color: 14
Size: 261933 Color: 17

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 447910 Color: 17
Size: 283237 Color: 4
Size: 268854 Color: 9

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 447926 Color: 15
Size: 285457 Color: 2
Size: 266618 Color: 14

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 447943 Color: 2
Size: 276718 Color: 12
Size: 275340 Color: 18

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 447944 Color: 8
Size: 290058 Color: 3
Size: 261999 Color: 15

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 447987 Color: 9
Size: 287152 Color: 19
Size: 264862 Color: 17

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 448006 Color: 0
Size: 301146 Color: 18
Size: 250849 Color: 9

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 448115 Color: 11
Size: 287471 Color: 2
Size: 264415 Color: 18

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 448206 Color: 8
Size: 288185 Color: 6
Size: 263610 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 448209 Color: 2
Size: 298674 Color: 9
Size: 253118 Color: 12

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 448410 Color: 16
Size: 283707 Color: 2
Size: 267884 Color: 13

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 448421 Color: 5
Size: 286763 Color: 19
Size: 264817 Color: 11

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 448446 Color: 13
Size: 291140 Color: 16
Size: 260415 Color: 10

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 448468 Color: 12
Size: 296729 Color: 13
Size: 254804 Color: 3

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 448569 Color: 6
Size: 282647 Color: 12
Size: 268785 Color: 11

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 448586 Color: 13
Size: 294619 Color: 3
Size: 256796 Color: 15

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 448618 Color: 6
Size: 288251 Color: 14
Size: 263132 Color: 19

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 448662 Color: 12
Size: 294422 Color: 10
Size: 256917 Color: 11

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 448711 Color: 5
Size: 296347 Color: 13
Size: 254943 Color: 18

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 448718 Color: 14
Size: 285486 Color: 15
Size: 265797 Color: 15

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 448777 Color: 10
Size: 285970 Color: 13
Size: 265254 Color: 14

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 448785 Color: 14
Size: 299317 Color: 19
Size: 251899 Color: 15

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 448841 Color: 12
Size: 297819 Color: 5
Size: 253341 Color: 18

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 448853 Color: 13
Size: 278319 Color: 4
Size: 272829 Color: 8

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 448922 Color: 16
Size: 298891 Color: 11
Size: 252188 Color: 5

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 448927 Color: 16
Size: 293725 Color: 13
Size: 257349 Color: 8

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 449026 Color: 7
Size: 295575 Color: 9
Size: 255400 Color: 11

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 449074 Color: 8
Size: 299011 Color: 4
Size: 251916 Color: 19

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 449084 Color: 4
Size: 296596 Color: 12
Size: 254321 Color: 18

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 449118 Color: 9
Size: 284964 Color: 6
Size: 265919 Color: 11

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 449165 Color: 16
Size: 285441 Color: 18
Size: 265395 Color: 13

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 449204 Color: 18
Size: 275562 Color: 4
Size: 275235 Color: 2

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 449230 Color: 9
Size: 286349 Color: 11
Size: 264422 Color: 2

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 449314 Color: 11
Size: 284725 Color: 0
Size: 265962 Color: 1

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 449721 Color: 19
Size: 278203 Color: 3
Size: 272077 Color: 4

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 449696 Color: 2
Size: 289367 Color: 18
Size: 260938 Color: 17

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 449757 Color: 13
Size: 294702 Color: 13
Size: 255542 Color: 10

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 449772 Color: 13
Size: 284956 Color: 8
Size: 265273 Color: 11

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 449818 Color: 9
Size: 276095 Color: 12
Size: 274088 Color: 10

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 449818 Color: 3
Size: 292574 Color: 5
Size: 257609 Color: 10

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 449917 Color: 0
Size: 282319 Color: 3
Size: 267765 Color: 5

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 449933 Color: 11
Size: 281404 Color: 5
Size: 268664 Color: 15

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 450110 Color: 19
Size: 282868 Color: 10
Size: 267023 Color: 11

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 449939 Color: 10
Size: 277472 Color: 11
Size: 272590 Color: 4

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 449940 Color: 0
Size: 286001 Color: 16
Size: 264060 Color: 2

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 450054 Color: 12
Size: 286244 Color: 3
Size: 263703 Color: 2

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 450218 Color: 19
Size: 284775 Color: 2
Size: 265008 Color: 9

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 450173 Color: 16
Size: 293007 Color: 18
Size: 256821 Color: 13

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 450219 Color: 1
Size: 278215 Color: 17
Size: 271567 Color: 15

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 450243 Color: 8
Size: 290554 Color: 7
Size: 259204 Color: 9

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 450250 Color: 14
Size: 295632 Color: 4
Size: 254119 Color: 2

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 450265 Color: 7
Size: 280333 Color: 19
Size: 269403 Color: 18

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 450397 Color: 0
Size: 285223 Color: 11
Size: 264381 Color: 14

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 450462 Color: 17
Size: 284909 Color: 6
Size: 264630 Color: 14

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 450538 Color: 4
Size: 288686 Color: 0
Size: 260777 Color: 9

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 450538 Color: 7
Size: 296269 Color: 8
Size: 253194 Color: 13

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 450644 Color: 18
Size: 288239 Color: 19
Size: 261118 Color: 7

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 450912 Color: 6
Size: 279207 Color: 0
Size: 269882 Color: 4

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 450930 Color: 9
Size: 288416 Color: 13
Size: 260655 Color: 13

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 450967 Color: 9
Size: 298659 Color: 18
Size: 250375 Color: 6

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 451006 Color: 11
Size: 296677 Color: 12
Size: 252318 Color: 15

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 451149 Color: 1
Size: 277996 Color: 19
Size: 270856 Color: 10

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 451287 Color: 15
Size: 281481 Color: 0
Size: 267233 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 451298 Color: 12
Size: 292646 Color: 8
Size: 256057 Color: 2

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 451307 Color: 9
Size: 298633 Color: 4
Size: 250061 Color: 3

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 451339 Color: 4
Size: 286348 Color: 1
Size: 262314 Color: 7

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 451357 Color: 5
Size: 282512 Color: 19
Size: 266132 Color: 10

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 451406 Color: 3
Size: 276259 Color: 13
Size: 272336 Color: 18

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 451423 Color: 3
Size: 291413 Color: 6
Size: 257165 Color: 7

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 451513 Color: 17
Size: 285788 Color: 3
Size: 262700 Color: 17

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 451514 Color: 8
Size: 289334 Color: 2
Size: 259153 Color: 10

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 451569 Color: 17
Size: 293833 Color: 1
Size: 254599 Color: 8

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 451773 Color: 6
Size: 275331 Color: 3
Size: 272897 Color: 5

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 451966 Color: 19
Size: 277805 Color: 14
Size: 270230 Color: 9

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 452050 Color: 3
Size: 295356 Color: 0
Size: 252595 Color: 14

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 452114 Color: 17
Size: 289797 Color: 8
Size: 258090 Color: 2

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 452175 Color: 15
Size: 281503 Color: 19
Size: 266323 Color: 18

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 452266 Color: 14
Size: 295621 Color: 6
Size: 252114 Color: 2

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 452383 Color: 3
Size: 293156 Color: 14
Size: 254462 Color: 4

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 452471 Color: 12
Size: 277299 Color: 11
Size: 270231 Color: 15

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 452532 Color: 14
Size: 278923 Color: 13
Size: 268546 Color: 18

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 452582 Color: 1
Size: 295553 Color: 5
Size: 251866 Color: 4

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 452753 Color: 9
Size: 277632 Color: 13
Size: 269616 Color: 7

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 452945 Color: 10
Size: 275582 Color: 2
Size: 271474 Color: 9

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 453067 Color: 19
Size: 275803 Color: 9
Size: 271131 Color: 11

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 453053 Color: 4
Size: 294780 Color: 10
Size: 252168 Color: 10

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 453091 Color: 9
Size: 274017 Color: 7
Size: 272893 Color: 6

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 453113 Color: 1
Size: 278208 Color: 3
Size: 268680 Color: 19

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 453219 Color: 5
Size: 291127 Color: 4
Size: 255655 Color: 10

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 453225 Color: 7
Size: 284965 Color: 3
Size: 261811 Color: 1

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 453269 Color: 8
Size: 287968 Color: 6
Size: 258764 Color: 3

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 453405 Color: 15
Size: 275705 Color: 13
Size: 270891 Color: 0

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 453437 Color: 6
Size: 289838 Color: 3
Size: 256726 Color: 4

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 453495 Color: 19
Size: 279469 Color: 13
Size: 267037 Color: 4

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 453458 Color: 18
Size: 281453 Color: 6
Size: 265090 Color: 10

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 453506 Color: 16
Size: 283210 Color: 8
Size: 263285 Color: 5

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 453545 Color: 17
Size: 289234 Color: 17
Size: 257222 Color: 11

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 453651 Color: 10
Size: 293802 Color: 14
Size: 252548 Color: 3

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 453686 Color: 2
Size: 283668 Color: 19
Size: 262647 Color: 18

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 453698 Color: 10
Size: 290196 Color: 13
Size: 256107 Color: 6

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 453722 Color: 3
Size: 292290 Color: 8
Size: 253989 Color: 10

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 453748 Color: 1
Size: 277598 Color: 6
Size: 268655 Color: 13

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 453794 Color: 16
Size: 293235 Color: 8
Size: 252972 Color: 3

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 453862 Color: 12
Size: 288652 Color: 7
Size: 257487 Color: 15

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 454074 Color: 8
Size: 278923 Color: 15
Size: 267004 Color: 11

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 454109 Color: 13
Size: 285954 Color: 12
Size: 259938 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 454128 Color: 5
Size: 295292 Color: 17
Size: 250581 Color: 5

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 454145 Color: 7
Size: 283175 Color: 19
Size: 262681 Color: 2

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 454262 Color: 2
Size: 278904 Color: 3
Size: 266835 Color: 17

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 454283 Color: 11
Size: 290285 Color: 5
Size: 255433 Color: 4

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 454399 Color: 6
Size: 286667 Color: 9
Size: 258935 Color: 11

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 454501 Color: 15
Size: 289851 Color: 2
Size: 255649 Color: 3

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 454570 Color: 0
Size: 273754 Color: 19
Size: 271677 Color: 9

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 454603 Color: 6
Size: 286218 Color: 16
Size: 259180 Color: 8

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 454651 Color: 9
Size: 281749 Color: 15
Size: 263601 Color: 2

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 454680 Color: 17
Size: 286366 Color: 9
Size: 258955 Color: 6

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 454723 Color: 7
Size: 293903 Color: 14
Size: 251375 Color: 16

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 454738 Color: 0
Size: 277360 Color: 18
Size: 267903 Color: 15

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 454738 Color: 6
Size: 283218 Color: 19
Size: 262045 Color: 10

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 454739 Color: 12
Size: 291536 Color: 4
Size: 253726 Color: 6

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 454747 Color: 18
Size: 272924 Color: 10
Size: 272330 Color: 4

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 454815 Color: 18
Size: 280653 Color: 7
Size: 264533 Color: 10

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 454900 Color: 1
Size: 293021 Color: 3
Size: 252080 Color: 11

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 454921 Color: 13
Size: 288132 Color: 6
Size: 256948 Color: 8

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 454928 Color: 9
Size: 293415 Color: 15
Size: 251658 Color: 19

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 454929 Color: 14
Size: 277036 Color: 17
Size: 268036 Color: 2

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 455088 Color: 11
Size: 281640 Color: 17
Size: 263273 Color: 10

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 455293 Color: 6
Size: 283212 Color: 16
Size: 261496 Color: 3

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 455301 Color: 6
Size: 284075 Color: 3
Size: 260625 Color: 16

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 455325 Color: 1
Size: 280549 Color: 2
Size: 264127 Color: 18

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 455384 Color: 12
Size: 277546 Color: 2
Size: 267071 Color: 19

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 455389 Color: 16
Size: 283440 Color: 3
Size: 261172 Color: 17

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 455411 Color: 13
Size: 289382 Color: 1
Size: 255208 Color: 18

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 455439 Color: 11
Size: 288116 Color: 17
Size: 256446 Color: 17

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 455520 Color: 1
Size: 274808 Color: 4
Size: 269673 Color: 10

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 455535 Color: 5
Size: 288740 Color: 17
Size: 255726 Color: 14

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 455554 Color: 13
Size: 272443 Color: 0
Size: 272004 Color: 19

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 455583 Color: 4
Size: 282157 Color: 17
Size: 262261 Color: 7

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 455585 Color: 9
Size: 283805 Color: 18
Size: 260611 Color: 18

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 455596 Color: 15
Size: 280995 Color: 17
Size: 263410 Color: 9

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 455651 Color: 2
Size: 285638 Color: 12
Size: 258712 Color: 19

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 455798 Color: 15
Size: 293658 Color: 12
Size: 250545 Color: 13

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 455811 Color: 7
Size: 279711 Color: 0
Size: 264479 Color: 2

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 456014 Color: 5
Size: 293405 Color: 14
Size: 250582 Color: 8

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 456037 Color: 16
Size: 275088 Color: 3
Size: 268876 Color: 19

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 456108 Color: 4
Size: 290736 Color: 10
Size: 253157 Color: 0

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 456162 Color: 18
Size: 278376 Color: 17
Size: 265463 Color: 9

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 456199 Color: 0
Size: 275888 Color: 8
Size: 267914 Color: 15

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 456204 Color: 17
Size: 273312 Color: 10
Size: 270485 Color: 15

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 456235 Color: 13
Size: 279339 Color: 17
Size: 264427 Color: 2

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 456246 Color: 0
Size: 292816 Color: 6
Size: 250939 Color: 9

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 456258 Color: 3
Size: 275437 Color: 7
Size: 268306 Color: 12

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 456415 Color: 2
Size: 291694 Color: 9
Size: 251892 Color: 17

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 456558 Color: 13
Size: 287962 Color: 16
Size: 255481 Color: 6

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 456618 Color: 5
Size: 282946 Color: 19
Size: 260437 Color: 1

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 456621 Color: 4
Size: 281741 Color: 6
Size: 261639 Color: 4

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 456677 Color: 14
Size: 275712 Color: 15
Size: 267612 Color: 2

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 456750 Color: 15
Size: 284640 Color: 1
Size: 258611 Color: 5

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 456805 Color: 5
Size: 290075 Color: 7
Size: 253121 Color: 7

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 456814 Color: 2
Size: 276588 Color: 12
Size: 266599 Color: 5

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 456921 Color: 12
Size: 291728 Color: 5
Size: 251352 Color: 18

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 456937 Color: 10
Size: 287679 Color: 16
Size: 255385 Color: 1

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 456988 Color: 2
Size: 286071 Color: 1
Size: 256942 Color: 16

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 457068 Color: 5
Size: 273258 Color: 4
Size: 269675 Color: 18

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 457077 Color: 12
Size: 274954 Color: 1
Size: 267970 Color: 6

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 457084 Color: 18
Size: 273491 Color: 19
Size: 269426 Color: 11

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 457121 Color: 12
Size: 278498 Color: 13
Size: 264382 Color: 2

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 457137 Color: 17
Size: 277911 Color: 13
Size: 264953 Color: 10

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 457225 Color: 2
Size: 278222 Color: 12
Size: 264554 Color: 14

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 457304 Color: 18
Size: 285489 Color: 9
Size: 257208 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 457331 Color: 12
Size: 278680 Color: 18
Size: 263990 Color: 2

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 457346 Color: 9
Size: 289840 Color: 15
Size: 252815 Color: 6

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 457359 Color: 9
Size: 280117 Color: 10
Size: 262525 Color: 18

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 457390 Color: 14
Size: 281675 Color: 9
Size: 260936 Color: 6

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 457470 Color: 19
Size: 283045 Color: 4
Size: 259486 Color: 17

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 457438 Color: 14
Size: 288007 Color: 16
Size: 254556 Color: 1

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 457491 Color: 0
Size: 273026 Color: 3
Size: 269484 Color: 9

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 457508 Color: 10
Size: 289599 Color: 9
Size: 252894 Color: 7

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 457530 Color: 8
Size: 290668 Color: 17
Size: 251803 Color: 8

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 457535 Color: 9
Size: 287243 Color: 19
Size: 255223 Color: 11

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 457661 Color: 2
Size: 286443 Color: 3
Size: 255897 Color: 6

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 457797 Color: 3
Size: 289908 Color: 6
Size: 252296 Color: 0

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 457828 Color: 10
Size: 291389 Color: 5
Size: 250784 Color: 17

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 457913 Color: 2
Size: 290639 Color: 7
Size: 251449 Color: 15

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 458126 Color: 11
Size: 272364 Color: 7
Size: 269511 Color: 16

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 458128 Color: 10
Size: 283982 Color: 11
Size: 257891 Color: 2

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 458302 Color: 19
Size: 290159 Color: 13
Size: 251540 Color: 4

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 458230 Color: 0
Size: 282956 Color: 12
Size: 258815 Color: 5

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 458281 Color: 9
Size: 283393 Color: 8
Size: 258327 Color: 0

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 458316 Color: 10
Size: 281670 Color: 14
Size: 260015 Color: 0

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 458415 Color: 9
Size: 275679 Color: 18
Size: 265907 Color: 19

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 458456 Color: 14
Size: 290237 Color: 10
Size: 251308 Color: 9

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 458462 Color: 6
Size: 284320 Color: 15
Size: 257219 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 458506 Color: 14
Size: 275742 Color: 10
Size: 265753 Color: 16

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 458517 Color: 18
Size: 271621 Color: 3
Size: 269863 Color: 3

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 458577 Color: 3
Size: 274600 Color: 1
Size: 266824 Color: 11

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 458637 Color: 9
Size: 280283 Color: 1
Size: 261081 Color: 19

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 458744 Color: 17
Size: 282097 Color: 4
Size: 259160 Color: 4

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 458820 Color: 8
Size: 284192 Color: 10
Size: 256989 Color: 12

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 458871 Color: 1
Size: 287471 Color: 8
Size: 253659 Color: 13

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 459074 Color: 14
Size: 284789 Color: 16
Size: 256138 Color: 3

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 459258 Color: 19
Size: 283326 Color: 15
Size: 257417 Color: 8

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 459423 Color: 6
Size: 285283 Color: 11
Size: 255295 Color: 3

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 459448 Color: 1
Size: 275281 Color: 11
Size: 265272 Color: 5

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 459556 Color: 2
Size: 271402 Color: 4
Size: 269043 Color: 2

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 459706 Color: 19
Size: 279927 Color: 12
Size: 260368 Color: 7

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 459678 Color: 17
Size: 288583 Color: 16
Size: 251740 Color: 5

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 459737 Color: 4
Size: 271405 Color: 8
Size: 268859 Color: 7

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 459769 Color: 5
Size: 282075 Color: 9
Size: 258157 Color: 9

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 459776 Color: 7
Size: 274712 Color: 1
Size: 265513 Color: 14

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 459777 Color: 2
Size: 280304 Color: 13
Size: 259920 Color: 11

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 459804 Color: 14
Size: 285260 Color: 19
Size: 254937 Color: 9

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 459861 Color: 12
Size: 284439 Color: 8
Size: 255701 Color: 2

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 459876 Color: 8
Size: 272180 Color: 15
Size: 267945 Color: 3

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 460047 Color: 9
Size: 276126 Color: 10
Size: 263828 Color: 6

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 460068 Color: 15
Size: 286321 Color: 6
Size: 253612 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 460166 Color: 17
Size: 285575 Color: 5
Size: 254260 Color: 6

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 460189 Color: 19
Size: 287220 Color: 7
Size: 252592 Color: 2

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 460215 Color: 8
Size: 287430 Color: 13
Size: 252356 Color: 9

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 460307 Color: 2
Size: 281907 Color: 5
Size: 257787 Color: 0

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 460315 Color: 11
Size: 276257 Color: 2
Size: 263429 Color: 1

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 460415 Color: 16
Size: 276280 Color: 16
Size: 263306 Color: 17

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 460447 Color: 9
Size: 276323 Color: 5
Size: 263231 Color: 0

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 460490 Color: 14
Size: 276385 Color: 6
Size: 263126 Color: 3

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 460563 Color: 19
Size: 283825 Color: 12
Size: 255613 Color: 4

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 460610 Color: 18
Size: 284291 Color: 15
Size: 255100 Color: 17

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 460753 Color: 4
Size: 279460 Color: 15
Size: 259788 Color: 14

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 460757 Color: 11
Size: 282013 Color: 13
Size: 257231 Color: 4

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 460849 Color: 14
Size: 283832 Color: 1
Size: 255320 Color: 3

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 461017 Color: 3
Size: 285723 Color: 2
Size: 253261 Color: 19

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 461033 Color: 18
Size: 280194 Color: 5
Size: 258774 Color: 4

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 461052 Color: 6
Size: 284607 Color: 15
Size: 254342 Color: 2

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 461310 Color: 2
Size: 271705 Color: 7
Size: 266986 Color: 8

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 461079 Color: 10
Size: 272048 Color: 11
Size: 266874 Color: 15

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 461320 Color: 8
Size: 274282 Color: 11
Size: 264399 Color: 18

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 461420 Color: 6
Size: 286788 Color: 13
Size: 251793 Color: 15

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 461648 Color: 2
Size: 277889 Color: 16
Size: 260464 Color: 4

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 461596 Color: 8
Size: 278948 Color: 15
Size: 259457 Color: 18

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 461610 Color: 3
Size: 287925 Color: 6
Size: 250466 Color: 6

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 461707 Color: 12
Size: 275245 Color: 0
Size: 263049 Color: 6

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 461813 Color: 2
Size: 286149 Color: 10
Size: 252039 Color: 8

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 461711 Color: 12
Size: 271575 Color: 0
Size: 266715 Color: 19

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 461738 Color: 11
Size: 273552 Color: 6
Size: 264711 Color: 1

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 461818 Color: 19
Size: 283881 Color: 9
Size: 254302 Color: 5

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 461872 Color: 15
Size: 270114 Color: 10
Size: 268015 Color: 19

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 461882 Color: 0
Size: 286281 Color: 3
Size: 251838 Color: 15

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 461933 Color: 18
Size: 287509 Color: 5
Size: 250559 Color: 1

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 462124 Color: 17
Size: 283709 Color: 3
Size: 254168 Color: 15

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 462167 Color: 13
Size: 279211 Color: 7
Size: 258623 Color: 11

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 462198 Color: 18
Size: 281751 Color: 14
Size: 256052 Color: 4

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 462319 Color: 17
Size: 279741 Color: 15
Size: 257941 Color: 19

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 462577 Color: 11
Size: 275288 Color: 15
Size: 262136 Color: 8

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 462582 Color: 17
Size: 274209 Color: 7
Size: 263210 Color: 13

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 462688 Color: 10
Size: 275179 Color: 3
Size: 262134 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 462697 Color: 1
Size: 281339 Color: 0
Size: 255965 Color: 6

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 462762 Color: 13
Size: 269349 Color: 0
Size: 267890 Color: 14

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 462784 Color: 1
Size: 272891 Color: 19
Size: 264326 Color: 2

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 462846 Color: 7
Size: 278539 Color: 16
Size: 258616 Color: 18

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 462853 Color: 15
Size: 280670 Color: 9
Size: 256478 Color: 7

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 462870 Color: 18
Size: 282995 Color: 9
Size: 254136 Color: 3

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 462880 Color: 12
Size: 269010 Color: 7
Size: 268111 Color: 11

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 462965 Color: 5
Size: 280762 Color: 10
Size: 256274 Color: 19

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 463048 Color: 18
Size: 278163 Color: 15
Size: 258790 Color: 12

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 463132 Color: 15
Size: 269274 Color: 14
Size: 267595 Color: 10

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 463158 Color: 18
Size: 275008 Color: 10
Size: 261835 Color: 2

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 463233 Color: 2
Size: 278142 Color: 7
Size: 258626 Color: 3

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 463375 Color: 3
Size: 279341 Color: 16
Size: 257285 Color: 19

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 463379 Color: 4
Size: 278256 Color: 6
Size: 258366 Color: 8

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 463471 Color: 0
Size: 276689 Color: 10
Size: 259841 Color: 13

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 463559 Color: 2
Size: 282252 Color: 14
Size: 254190 Color: 7

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 463563 Color: 5
Size: 281752 Color: 11
Size: 254686 Color: 7

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 463751 Color: 4
Size: 275543 Color: 2
Size: 260707 Color: 19

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 463780 Color: 14
Size: 272208 Color: 15
Size: 264013 Color: 2

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 463798 Color: 17
Size: 278498 Color: 16
Size: 257705 Color: 4

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 463802 Color: 6
Size: 283850 Color: 13
Size: 252349 Color: 18

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 463832 Color: 8
Size: 273698 Color: 10
Size: 262471 Color: 18

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 463950 Color: 5
Size: 282057 Color: 4
Size: 253994 Color: 15

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 463963 Color: 6
Size: 273139 Color: 2
Size: 262899 Color: 4

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 464013 Color: 13
Size: 277666 Color: 0
Size: 258322 Color: 1

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 464013 Color: 16
Size: 284683 Color: 0
Size: 251305 Color: 8

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 464041 Color: 1
Size: 282865 Color: 19
Size: 253095 Color: 14

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 464096 Color: 13
Size: 284091 Color: 18
Size: 251814 Color: 15

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 464158 Color: 8
Size: 271260 Color: 8
Size: 264583 Color: 2

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 464297 Color: 16
Size: 273298 Color: 6
Size: 262406 Color: 19

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 464372 Color: 1
Size: 281628 Color: 18
Size: 254001 Color: 1

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 464452 Color: 18
Size: 280718 Color: 5
Size: 254831 Color: 17

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 464697 Color: 3
Size: 280412 Color: 2
Size: 254892 Color: 14

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 464741 Color: 3
Size: 281053 Color: 0
Size: 254207 Color: 9

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 464742 Color: 7
Size: 269402 Color: 10
Size: 265857 Color: 3

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 464920 Color: 11
Size: 276340 Color: 10
Size: 258741 Color: 14

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 464923 Color: 16
Size: 269310 Color: 0
Size: 265768 Color: 9

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 465164 Color: 7
Size: 281348 Color: 13
Size: 253489 Color: 10

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 465227 Color: 8
Size: 267541 Color: 2
Size: 267233 Color: 11

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 465231 Color: 1
Size: 278414 Color: 13
Size: 256356 Color: 17

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 465267 Color: 16
Size: 271131 Color: 15
Size: 263603 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 465358 Color: 7
Size: 282760 Color: 0
Size: 251883 Color: 14

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 465363 Color: 6
Size: 283805 Color: 14
Size: 250833 Color: 19

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 465379 Color: 6
Size: 277500 Color: 13
Size: 257122 Color: 4

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 465381 Color: 15
Size: 280770 Color: 5
Size: 253850 Color: 14

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 465458 Color: 17
Size: 274967 Color: 16
Size: 259576 Color: 5

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 465781 Color: 2
Size: 284185 Color: 0
Size: 250035 Color: 17

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 465778 Color: 15
Size: 275530 Color: 0
Size: 258693 Color: 5

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 465803 Color: 0
Size: 280724 Color: 17
Size: 253474 Color: 13

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 465814 Color: 4
Size: 280965 Color: 2
Size: 253222 Color: 17

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 466015 Color: 17
Size: 271016 Color: 1
Size: 262970 Color: 8

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 466103 Color: 11
Size: 273482 Color: 5
Size: 260416 Color: 9

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 466108 Color: 11
Size: 272515 Color: 3
Size: 261378 Color: 17

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 466111 Color: 1
Size: 268082 Color: 6
Size: 265808 Color: 11

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 466352 Color: 5
Size: 275210 Color: 16
Size: 258439 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 466430 Color: 17
Size: 272762 Color: 15
Size: 260809 Color: 18

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 466510 Color: 7
Size: 278233 Color: 19
Size: 255258 Color: 17

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 466611 Color: 13
Size: 280004 Color: 10
Size: 253386 Color: 15

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 466783 Color: 8
Size: 269241 Color: 2
Size: 263977 Color: 18

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 466825 Color: 19
Size: 282427 Color: 16
Size: 250749 Color: 15

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 466850 Color: 8
Size: 282399 Color: 5
Size: 250752 Color: 6

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 466913 Color: 10
Size: 275487 Color: 11
Size: 257601 Color: 13

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 466918 Color: 18
Size: 270702 Color: 19
Size: 262381 Color: 7

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 466962 Color: 10
Size: 276169 Color: 19
Size: 256870 Color: 1

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 466965 Color: 16
Size: 273882 Color: 2
Size: 259154 Color: 19

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 466991 Color: 13
Size: 281537 Color: 8
Size: 251473 Color: 8

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 466992 Color: 1
Size: 271285 Color: 16
Size: 261724 Color: 6

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 467079 Color: 15
Size: 270839 Color: 16
Size: 262083 Color: 7

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 467136 Color: 6
Size: 280653 Color: 13
Size: 252212 Color: 13

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 467138 Color: 15
Size: 278484 Color: 13
Size: 254379 Color: 19

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 467146 Color: 14
Size: 266952 Color: 2
Size: 265903 Color: 10

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 467156 Color: 10
Size: 269904 Color: 10
Size: 262941 Color: 13

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 467159 Color: 5
Size: 273998 Color: 0
Size: 258844 Color: 12

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 467160 Color: 11
Size: 281748 Color: 1
Size: 251093 Color: 1

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 467176 Color: 18
Size: 281428 Color: 8
Size: 251397 Color: 12

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 467243 Color: 15
Size: 268588 Color: 13
Size: 264170 Color: 10

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 467291 Color: 16
Size: 280514 Color: 3
Size: 252196 Color: 3

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 467352 Color: 2
Size: 277128 Color: 10
Size: 255521 Color: 17

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 467421 Color: 10
Size: 269139 Color: 0
Size: 263441 Color: 19

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 467676 Color: 13
Size: 267758 Color: 18
Size: 264567 Color: 13

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 467737 Color: 15
Size: 276406 Color: 17
Size: 255858 Color: 1

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 467790 Color: 6
Size: 281788 Color: 9
Size: 250423 Color: 8

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 467983 Color: 8
Size: 281074 Color: 4
Size: 250944 Color: 13

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 468089 Color: 2
Size: 277234 Color: 10
Size: 254678 Color: 6

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 468077 Color: 18
Size: 278207 Color: 14
Size: 253717 Color: 13

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 468194 Color: 7
Size: 274755 Color: 6
Size: 257052 Color: 12

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 468213 Color: 10
Size: 268761 Color: 5
Size: 263027 Color: 5

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 468317 Color: 13
Size: 279092 Color: 6
Size: 252592 Color: 9

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 468320 Color: 1
Size: 271166 Color: 8
Size: 260515 Color: 3

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 468374 Color: 15
Size: 277430 Color: 14
Size: 254197 Color: 13

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 468539 Color: 13
Size: 267037 Color: 16
Size: 264425 Color: 4

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 468553 Color: 17
Size: 268093 Color: 13
Size: 263355 Color: 8

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 468630 Color: 1
Size: 273390 Color: 18
Size: 257981 Color: 5

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 468699 Color: 9
Size: 279648 Color: 11
Size: 251654 Color: 3

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 468808 Color: 13
Size: 270719 Color: 0
Size: 260474 Color: 5

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 468920 Color: 15
Size: 280100 Color: 4
Size: 250981 Color: 7

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 468929 Color: 6
Size: 278417 Color: 5
Size: 252655 Color: 18

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 469064 Color: 19
Size: 268102 Color: 5
Size: 262835 Color: 17

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 469270 Color: 6
Size: 276180 Color: 4
Size: 254551 Color: 2

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 469291 Color: 14
Size: 272390 Color: 16
Size: 258320 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 469341 Color: 4
Size: 273833 Color: 18
Size: 256827 Color: 6

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 469425 Color: 0
Size: 267521 Color: 11
Size: 263055 Color: 6

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 469446 Color: 19
Size: 279350 Color: 14
Size: 251205 Color: 5

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 469453 Color: 3
Size: 269009 Color: 2
Size: 261539 Color: 13

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 469593 Color: 13
Size: 269052 Color: 7
Size: 261356 Color: 15

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 469596 Color: 7
Size: 268201 Color: 1
Size: 262204 Color: 3

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 469680 Color: 0
Size: 272650 Color: 16
Size: 257671 Color: 9

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 469880 Color: 18
Size: 272327 Color: 0
Size: 257794 Color: 5

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 469937 Color: 16
Size: 272807 Color: 3
Size: 257257 Color: 12

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 469986 Color: 2
Size: 273789 Color: 12
Size: 256226 Color: 6

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 469982 Color: 8
Size: 272350 Color: 5
Size: 257669 Color: 8

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 470114 Color: 18
Size: 271277 Color: 9
Size: 258610 Color: 18

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 470132 Color: 12
Size: 273758 Color: 6
Size: 256111 Color: 16

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 470247 Color: 9
Size: 278649 Color: 0
Size: 251105 Color: 4

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 470318 Color: 18
Size: 265691 Color: 19
Size: 263992 Color: 4

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 470372 Color: 2
Size: 266163 Color: 19
Size: 263466 Color: 6

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 470322 Color: 7
Size: 273738 Color: 6
Size: 255941 Color: 13

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 470437 Color: 9
Size: 273236 Color: 7
Size: 256328 Color: 19

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 470453 Color: 19
Size: 267637 Color: 0
Size: 261911 Color: 17

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 470568 Color: 1
Size: 272581 Color: 9
Size: 256852 Color: 10

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 470658 Color: 14
Size: 265254 Color: 17
Size: 264089 Color: 2

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 470892 Color: 4
Size: 273766 Color: 5
Size: 255343 Color: 3

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 470916 Color: 13
Size: 276334 Color: 19
Size: 252751 Color: 4

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 470958 Color: 7
Size: 267187 Color: 3
Size: 261856 Color: 18

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 470965 Color: 3
Size: 272619 Color: 14
Size: 256417 Color: 1

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 471024 Color: 5
Size: 270772 Color: 14
Size: 258205 Color: 16

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 471164 Color: 16
Size: 273699 Color: 2
Size: 255138 Color: 8

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 471228 Color: 17
Size: 275900 Color: 18
Size: 252873 Color: 10

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 471284 Color: 15
Size: 264450 Color: 5
Size: 264267 Color: 8

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 471320 Color: 16
Size: 268274 Color: 11
Size: 260407 Color: 4

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 471392 Color: 4
Size: 272988 Color: 17
Size: 255621 Color: 19

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 471406 Color: 12
Size: 278420 Color: 15
Size: 250175 Color: 7

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 471461 Color: 4
Size: 267926 Color: 7
Size: 260614 Color: 2

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 471468 Color: 16
Size: 273517 Color: 1
Size: 255016 Color: 16

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 471491 Color: 1
Size: 270527 Color: 16
Size: 257983 Color: 10

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 471518 Color: 7
Size: 271333 Color: 11
Size: 257150 Color: 11

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 471607 Color: 11
Size: 266788 Color: 16
Size: 261606 Color: 19

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 471729 Color: 8
Size: 273750 Color: 7
Size: 254522 Color: 10

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 471736 Color: 12
Size: 266609 Color: 19
Size: 261656 Color: 2

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 471747 Color: 13
Size: 267293 Color: 3
Size: 260961 Color: 8

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 471751 Color: 8
Size: 271687 Color: 0
Size: 256563 Color: 3

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 471862 Color: 13
Size: 268187 Color: 11
Size: 259952 Color: 5

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 471969 Color: 1
Size: 268363 Color: 17
Size: 259669 Color: 4

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 472002 Color: 18
Size: 267158 Color: 2
Size: 260841 Color: 1

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 472015 Color: 10
Size: 276248 Color: 7
Size: 251738 Color: 8

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 472034 Color: 12
Size: 271047 Color: 8
Size: 256920 Color: 11

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 472083 Color: 13
Size: 275731 Color: 18
Size: 252187 Color: 18

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 472348 Color: 4
Size: 266863 Color: 2
Size: 260790 Color: 18

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 472358 Color: 8
Size: 264445 Color: 10
Size: 263198 Color: 4

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 472506 Color: 3
Size: 275041 Color: 8
Size: 252454 Color: 10

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 472525 Color: 6
Size: 275717 Color: 18
Size: 251759 Color: 3

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 472763 Color: 9
Size: 269264 Color: 7
Size: 257974 Color: 10

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 472952 Color: 17
Size: 276904 Color: 10
Size: 250145 Color: 2

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 473269 Color: 7
Size: 264334 Color: 11
Size: 262398 Color: 19

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 473363 Color: 9
Size: 271629 Color: 7
Size: 255009 Color: 6

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 473391 Color: 1
Size: 273715 Color: 12
Size: 252895 Color: 10

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 473458 Color: 12
Size: 274634 Color: 6
Size: 251909 Color: 2

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 473598 Color: 1
Size: 271539 Color: 11
Size: 254864 Color: 12

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 473747 Color: 18
Size: 265975 Color: 12
Size: 260279 Color: 10

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 473804 Color: 16
Size: 274401 Color: 4
Size: 251796 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 473945 Color: 11
Size: 269033 Color: 17
Size: 257023 Color: 17

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 474025 Color: 3
Size: 273946 Color: 12
Size: 252030 Color: 10

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 474113 Color: 2
Size: 270416 Color: 18
Size: 255472 Color: 15

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 474050 Color: 4
Size: 272849 Color: 7
Size: 253102 Color: 12

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 474063 Color: 18
Size: 269791 Color: 8
Size: 256147 Color: 6

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 474116 Color: 17
Size: 266202 Color: 8
Size: 259683 Color: 17

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 474119 Color: 18
Size: 263198 Color: 14
Size: 262684 Color: 3

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 474144 Color: 9
Size: 263776 Color: 8
Size: 262081 Color: 2

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 474165 Color: 14
Size: 272312 Color: 1
Size: 253524 Color: 17

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 474256 Color: 13
Size: 271896 Color: 0
Size: 253849 Color: 9

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 474300 Color: 15
Size: 273087 Color: 6
Size: 252614 Color: 11

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 474413 Color: 8
Size: 269650 Color: 19
Size: 255938 Color: 11

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 474575 Color: 15
Size: 275117 Color: 10
Size: 250309 Color: 15

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 474595 Color: 1
Size: 267408 Color: 13
Size: 257998 Color: 6

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 474724 Color: 17
Size: 268284 Color: 7
Size: 256993 Color: 5

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 474744 Color: 19
Size: 269182 Color: 6
Size: 256075 Color: 14

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 474943 Color: 11
Size: 269894 Color: 1
Size: 255164 Color: 4

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 475004 Color: 8
Size: 273949 Color: 0
Size: 251048 Color: 2

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 475017 Color: 8
Size: 272741 Color: 11
Size: 252243 Color: 15

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 475028 Color: 1
Size: 274870 Color: 9
Size: 250103 Color: 13

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 475080 Color: 19
Size: 263167 Color: 6
Size: 261754 Color: 8

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 475128 Color: 5
Size: 265665 Color: 1
Size: 259208 Color: 3

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 475346 Color: 5
Size: 263405 Color: 2
Size: 261250 Color: 1

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 475385 Color: 12
Size: 272990 Color: 1
Size: 251626 Color: 4

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 475401 Color: 10
Size: 264129 Color: 15
Size: 260471 Color: 8

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 475433 Color: 18
Size: 269668 Color: 10
Size: 254900 Color: 3

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 475515 Color: 17
Size: 271971 Color: 16
Size: 252515 Color: 12

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 475557 Color: 12
Size: 270511 Color: 15
Size: 253933 Color: 8

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 475584 Color: 17
Size: 269213 Color: 19
Size: 255204 Color: 1

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 475591 Color: 9
Size: 263990 Color: 11
Size: 260420 Color: 10

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 475620 Color: 4
Size: 269535 Color: 11
Size: 254846 Color: 8

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 475639 Color: 11
Size: 268555 Color: 0
Size: 255807 Color: 17

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 475891 Color: 2
Size: 268974 Color: 15
Size: 255136 Color: 17

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 475897 Color: 9
Size: 268228 Color: 1
Size: 255876 Color: 11

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 476024 Color: 4
Size: 265862 Color: 11
Size: 258115 Color: 0

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 476122 Color: 11
Size: 267526 Color: 4
Size: 256353 Color: 3

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 476155 Color: 17
Size: 268484 Color: 18
Size: 255362 Color: 7

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 476209 Color: 13
Size: 266104 Color: 11
Size: 257688 Color: 8

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 476308 Color: 3
Size: 267988 Color: 19
Size: 255705 Color: 13

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 476312 Color: 4
Size: 266415 Color: 15
Size: 257274 Color: 3

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 476515 Color: 9
Size: 262947 Color: 18
Size: 260539 Color: 1

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 476548 Color: 19
Size: 272347 Color: 2
Size: 251106 Color: 7

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 476579 Color: 14
Size: 265502 Color: 1
Size: 257920 Color: 5

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 476585 Color: 13
Size: 269000 Color: 16
Size: 254416 Color: 17

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 476713 Color: 7
Size: 272071 Color: 17
Size: 251217 Color: 9

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 476757 Color: 12
Size: 267669 Color: 1
Size: 255575 Color: 8

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 476919 Color: 3
Size: 271185 Color: 0
Size: 251897 Color: 6

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 476928 Color: 5
Size: 271203 Color: 3
Size: 251870 Color: 2

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 476966 Color: 10
Size: 263727 Color: 16
Size: 259308 Color: 6

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 477019 Color: 12
Size: 263151 Color: 6
Size: 259831 Color: 6

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 477064 Color: 4
Size: 267482 Color: 5
Size: 255455 Color: 7

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 477070 Color: 19
Size: 272807 Color: 2
Size: 250124 Color: 4

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 477175 Color: 19
Size: 270163 Color: 13
Size: 252663 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 477199 Color: 17
Size: 264739 Color: 5
Size: 258063 Color: 19

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 477400 Color: 6
Size: 266149 Color: 13
Size: 256452 Color: 10

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 477432 Color: 3
Size: 262935 Color: 14
Size: 259634 Color: 13

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 477480 Color: 3
Size: 264214 Color: 1
Size: 258307 Color: 2

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 477541 Color: 5
Size: 267459 Color: 3
Size: 255001 Color: 10

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 477610 Color: 18
Size: 271773 Color: 5
Size: 250618 Color: 14

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 477648 Color: 1
Size: 264664 Color: 10
Size: 257689 Color: 5

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 477858 Color: 5
Size: 268508 Color: 19
Size: 253635 Color: 10

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 477991 Color: 7
Size: 264195 Color: 11
Size: 257815 Color: 5

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 477991 Color: 10
Size: 268808 Color: 10
Size: 253202 Color: 11

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 478058 Color: 2
Size: 271013 Color: 17
Size: 250930 Color: 4

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 477994 Color: 13
Size: 271043 Color: 18
Size: 250964 Color: 6

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 478042 Color: 7
Size: 270659 Color: 3
Size: 251300 Color: 6

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 478070 Color: 18
Size: 267890 Color: 16
Size: 254041 Color: 4

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 478085 Color: 12
Size: 263412 Color: 0
Size: 258504 Color: 2

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 478100 Color: 14
Size: 267841 Color: 11
Size: 254060 Color: 9

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 478121 Color: 19
Size: 267549 Color: 18
Size: 254331 Color: 10

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 478187 Color: 8
Size: 265633 Color: 1
Size: 256181 Color: 6

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 478203 Color: 19
Size: 261064 Color: 4
Size: 260734 Color: 8

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 478355 Color: 4
Size: 269141 Color: 14
Size: 252505 Color: 17

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 478440 Color: 12
Size: 264086 Color: 4
Size: 257475 Color: 12

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 478456 Color: 12
Size: 260790 Color: 1
Size: 260755 Color: 5

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 478553 Color: 16
Size: 267036 Color: 7
Size: 254412 Color: 8

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 478584 Color: 0
Size: 263415 Color: 19
Size: 258002 Color: 7

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 478702 Color: 0
Size: 269703 Color: 2
Size: 251596 Color: 4

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 478849 Color: 13
Size: 270381 Color: 19
Size: 250771 Color: 7

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 478862 Color: 8
Size: 268422 Color: 17
Size: 252717 Color: 4

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 478880 Color: 15
Size: 263924 Color: 4
Size: 257197 Color: 12

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 478968 Color: 17
Size: 267802 Color: 8
Size: 253231 Color: 6

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 479056 Color: 9
Size: 260835 Color: 0
Size: 260110 Color: 19

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 479081 Color: 17
Size: 264444 Color: 2
Size: 256476 Color: 15

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 479124 Color: 9
Size: 264816 Color: 17
Size: 256061 Color: 3

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 479231 Color: 6
Size: 267687 Color: 3
Size: 253083 Color: 1

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 479352 Color: 13
Size: 263018 Color: 4
Size: 257631 Color: 4

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 479426 Color: 5
Size: 264019 Color: 12
Size: 256556 Color: 18

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 479467 Color: 8
Size: 268244 Color: 10
Size: 252290 Color: 2

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 479568 Color: 18
Size: 269150 Color: 19
Size: 251283 Color: 5

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 479607 Color: 5
Size: 265901 Color: 14
Size: 254493 Color: 12

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 479671 Color: 7
Size: 266117 Color: 4
Size: 254213 Color: 0

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 479864 Color: 6
Size: 263931 Color: 13
Size: 256206 Color: 14

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 479979 Color: 5
Size: 267855 Color: 0
Size: 252167 Color: 9

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 480166 Color: 15
Size: 269834 Color: 3
Size: 250001 Color: 16

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 480249 Color: 16
Size: 260323 Color: 5
Size: 259429 Color: 1

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 480293 Color: 11
Size: 269276 Color: 10
Size: 250432 Color: 7

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 480328 Color: 15
Size: 263448 Color: 2
Size: 256225 Color: 17

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 480439 Color: 8
Size: 264411 Color: 7
Size: 255151 Color: 4

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 480636 Color: 14
Size: 268525 Color: 5
Size: 250840 Color: 1

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 480672 Color: 17
Size: 261002 Color: 14
Size: 258327 Color: 15

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 480688 Color: 10
Size: 262512 Color: 11
Size: 256801 Color: 17

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 480704 Color: 14
Size: 264382 Color: 4
Size: 254915 Color: 14

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 480720 Color: 12
Size: 268214 Color: 2
Size: 251067 Color: 12

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 480770 Color: 12
Size: 262248 Color: 5
Size: 256983 Color: 7

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 481131 Color: 16
Size: 268475 Color: 11
Size: 250395 Color: 10

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 481216 Color: 16
Size: 261274 Color: 19
Size: 257511 Color: 7

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 481264 Color: 7
Size: 268493 Color: 9
Size: 250244 Color: 9

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 481282 Color: 6
Size: 264564 Color: 14
Size: 254155 Color: 17

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 481306 Color: 2
Size: 267280 Color: 17
Size: 251415 Color: 12

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 481510 Color: 4
Size: 259865 Color: 10
Size: 258626 Color: 9

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 481597 Color: 19
Size: 259943 Color: 4
Size: 258461 Color: 16

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 481762 Color: 9
Size: 264719 Color: 12
Size: 253520 Color: 0

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 481798 Color: 12
Size: 267770 Color: 12
Size: 250433 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 481832 Color: 9
Size: 260988 Color: 9
Size: 257181 Color: 18

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 481885 Color: 12
Size: 262287 Color: 15
Size: 255829 Color: 9

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 481996 Color: 2
Size: 267649 Color: 19
Size: 250356 Color: 7

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 482000 Color: 6
Size: 262717 Color: 0
Size: 255284 Color: 4

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 482154 Color: 9
Size: 267767 Color: 17
Size: 250080 Color: 7

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 482177 Color: 1
Size: 266452 Color: 14
Size: 251372 Color: 9

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 482209 Color: 19
Size: 267092 Color: 2
Size: 250700 Color: 18

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 482339 Color: 7
Size: 260936 Color: 14
Size: 256726 Color: 12

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 482371 Color: 17
Size: 260340 Color: 3
Size: 257290 Color: 15

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 482456 Color: 4
Size: 259976 Color: 5
Size: 257569 Color: 18

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 482483 Color: 0
Size: 265960 Color: 18
Size: 251558 Color: 15

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 482506 Color: 12
Size: 260125 Color: 10
Size: 257370 Color: 8

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 482546 Color: 17
Size: 260911 Color: 2
Size: 256544 Color: 8

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 482561 Color: 5
Size: 259931 Color: 9
Size: 257509 Color: 10

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 482639 Color: 5
Size: 264514 Color: 16
Size: 252848 Color: 0

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 482744 Color: 5
Size: 266749 Color: 3
Size: 250508 Color: 8

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 482754 Color: 14
Size: 259731 Color: 6
Size: 257516 Color: 8

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 482762 Color: 2
Size: 263089 Color: 18
Size: 254150 Color: 14

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 482756 Color: 17
Size: 261883 Color: 14
Size: 255362 Color: 2

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 482922 Color: 14
Size: 261590 Color: 11
Size: 255489 Color: 9

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 482929 Color: 7
Size: 265985 Color: 4
Size: 251087 Color: 7

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 483032 Color: 9
Size: 260002 Color: 19
Size: 256967 Color: 11

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 483072 Color: 11
Size: 260431 Color: 2
Size: 256498 Color: 10

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 483177 Color: 7
Size: 260278 Color: 5
Size: 256546 Color: 0

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 483382 Color: 7
Size: 258608 Color: 10
Size: 258011 Color: 5

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 483487 Color: 4
Size: 260167 Color: 8
Size: 256347 Color: 10

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 483596 Color: 11
Size: 258998 Color: 2
Size: 257407 Color: 1

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 483684 Color: 18
Size: 259888 Color: 17
Size: 256429 Color: 7

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 483696 Color: 10
Size: 259151 Color: 1
Size: 257154 Color: 9

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 483703 Color: 3
Size: 262129 Color: 5
Size: 254169 Color: 7

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 483768 Color: 12
Size: 265660 Color: 15
Size: 250573 Color: 9

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 483859 Color: 9
Size: 266038 Color: 2
Size: 250104 Color: 17

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 483890 Color: 5
Size: 259725 Color: 12
Size: 256386 Color: 0

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 483995 Color: 19
Size: 258879 Color: 4
Size: 257127 Color: 12

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 484026 Color: 8
Size: 259238 Color: 0
Size: 256737 Color: 19

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 484067 Color: 11
Size: 265346 Color: 1
Size: 250588 Color: 13

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 484227 Color: 14
Size: 261501 Color: 14
Size: 254273 Color: 16

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 484237 Color: 12
Size: 261772 Color: 10
Size: 253992 Color: 11

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 484238 Color: 11
Size: 263499 Color: 4
Size: 252264 Color: 5

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 484291 Color: 6
Size: 259032 Color: 10
Size: 256678 Color: 3

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 484362 Color: 9
Size: 265630 Color: 10
Size: 250009 Color: 19

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 484385 Color: 13
Size: 265045 Color: 0
Size: 250571 Color: 17

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 484443 Color: 9
Size: 260038 Color: 8
Size: 255520 Color: 19

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 484621 Color: 2
Size: 259871 Color: 0
Size: 255509 Color: 6

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 484637 Color: 15
Size: 263862 Color: 8
Size: 251502 Color: 11

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 484663 Color: 14
Size: 262379 Color: 9
Size: 252959 Color: 12

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 484685 Color: 4
Size: 258269 Color: 12
Size: 257047 Color: 19

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 484704 Color: 17
Size: 263310 Color: 2
Size: 251987 Color: 4

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 484796 Color: 10
Size: 262799 Color: 4
Size: 252406 Color: 19

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 484866 Color: 6
Size: 263345 Color: 1
Size: 251790 Color: 13

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 484888 Color: 1
Size: 264092 Color: 7
Size: 251021 Color: 10

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 484987 Color: 8
Size: 258393 Color: 9
Size: 256621 Color: 14

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 485058 Color: 14
Size: 259003 Color: 2
Size: 255940 Color: 9

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 485096 Color: 18
Size: 264405 Color: 6
Size: 250500 Color: 1

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 485140 Color: 15
Size: 257554 Color: 14
Size: 257307 Color: 11

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 485237 Color: 19
Size: 263595 Color: 8
Size: 251169 Color: 9

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 485266 Color: 3
Size: 264682 Color: 5
Size: 250053 Color: 2

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 485299 Color: 5
Size: 260738 Color: 13
Size: 253964 Color: 17

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 485352 Color: 1
Size: 263991 Color: 13
Size: 250658 Color: 0

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 485373 Color: 11
Size: 263225 Color: 3
Size: 251403 Color: 18

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 485490 Color: 4
Size: 262585 Color: 13
Size: 251926 Color: 2

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 485638 Color: 11
Size: 259262 Color: 17
Size: 255101 Color: 8

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 485710 Color: 4
Size: 262719 Color: 18
Size: 251572 Color: 13

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 485755 Color: 16
Size: 262581 Color: 15
Size: 251665 Color: 17

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 485855 Color: 2
Size: 262772 Color: 8
Size: 251374 Color: 14

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 485860 Color: 14
Size: 258348 Color: 6
Size: 255793 Color: 11

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 485883 Color: 13
Size: 257745 Color: 17
Size: 256373 Color: 9

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 485935 Color: 8
Size: 258472 Color: 9
Size: 255594 Color: 11

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 486126 Color: 10
Size: 262864 Color: 5
Size: 251011 Color: 14

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 486192 Color: 6
Size: 256990 Color: 19
Size: 256819 Color: 19

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 486194 Color: 18
Size: 259596 Color: 0
Size: 254211 Color: 17

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 486211 Color: 8
Size: 258554 Color: 14
Size: 255236 Color: 12

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 486773 Color: 0
Size: 262768 Color: 2
Size: 250460 Color: 19

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 486966 Color: 8
Size: 261696 Color: 5
Size: 251339 Color: 17

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 487028 Color: 4
Size: 262816 Color: 19
Size: 250157 Color: 15

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 487092 Color: 7
Size: 260451 Color: 17
Size: 252458 Color: 8

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 487187 Color: 7
Size: 258748 Color: 2
Size: 254066 Color: 5

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 487209 Color: 6
Size: 261315 Color: 0
Size: 251477 Color: 15

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 487293 Color: 14
Size: 259242 Color: 9
Size: 253466 Color: 5

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 487472 Color: 13
Size: 262149 Color: 15
Size: 250380 Color: 15

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 487515 Color: 10
Size: 258184 Color: 17
Size: 254302 Color: 7

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 487633 Color: 6
Size: 257791 Color: 0
Size: 254577 Color: 11

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 487644 Color: 18
Size: 258408 Color: 2
Size: 253949 Color: 14

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 487699 Color: 14
Size: 262109 Color: 16
Size: 250193 Color: 19

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 487897 Color: 3
Size: 258159 Color: 6
Size: 253945 Color: 14

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 487935 Color: 18
Size: 261068 Color: 4
Size: 250998 Color: 12

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 487936 Color: 3
Size: 256439 Color: 9
Size: 255626 Color: 7

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 488044 Color: 8
Size: 261673 Color: 4
Size: 250284 Color: 12

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 488200 Color: 15
Size: 258615 Color: 17
Size: 253186 Color: 14

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 488206 Color: 16
Size: 260361 Color: 4
Size: 251434 Color: 10

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 488233 Color: 12
Size: 259945 Color: 4
Size: 251823 Color: 6

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 488238 Color: 5
Size: 259790 Color: 9
Size: 251973 Color: 12

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 488257 Color: 18
Size: 260977 Color: 11
Size: 250767 Color: 2

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 488495 Color: 12
Size: 259074 Color: 17
Size: 252432 Color: 5

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 488923 Color: 1
Size: 255989 Color: 9
Size: 255089 Color: 1

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 488946 Color: 6
Size: 258323 Color: 2
Size: 252732 Color: 0

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 488951 Color: 10
Size: 260915 Color: 3
Size: 250135 Color: 3

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 489113 Color: 15
Size: 259398 Color: 18
Size: 251490 Color: 18

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 489329 Color: 10
Size: 259366 Color: 17
Size: 251306 Color: 18

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 489376 Color: 8
Size: 257380 Color: 14
Size: 253245 Color: 3

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 489406 Color: 10
Size: 260216 Color: 9
Size: 250379 Color: 16

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 489505 Color: 9
Size: 256524 Color: 18
Size: 253972 Color: 19

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 489649 Color: 18
Size: 259319 Color: 8
Size: 251033 Color: 8

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 489652 Color: 16
Size: 255574 Color: 13
Size: 254775 Color: 17

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 489676 Color: 14
Size: 259976 Color: 7
Size: 250349 Color: 16

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 489689 Color: 7
Size: 256003 Color: 1
Size: 254309 Color: 2

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 489860 Color: 17
Size: 257240 Color: 10
Size: 252901 Color: 3

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 489901 Color: 16
Size: 255276 Color: 8
Size: 254824 Color: 10

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 489980 Color: 4
Size: 259583 Color: 0
Size: 250438 Color: 19

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 490031 Color: 8
Size: 259525 Color: 0
Size: 250445 Color: 18

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 490153 Color: 12
Size: 258808 Color: 2
Size: 251040 Color: 6

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 490246 Color: 1
Size: 258272 Color: 10
Size: 251483 Color: 8

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 490366 Color: 7
Size: 258119 Color: 3
Size: 251516 Color: 4

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 490436 Color: 3
Size: 257094 Color: 9
Size: 252471 Color: 10

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 490495 Color: 8
Size: 254967 Color: 18
Size: 254539 Color: 15

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 490592 Color: 0
Size: 255921 Color: 2
Size: 253488 Color: 5

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 490603 Color: 6
Size: 255265 Color: 10
Size: 254133 Color: 1

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 490622 Color: 9
Size: 255237 Color: 12
Size: 254142 Color: 12

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 490683 Color: 13
Size: 258962 Color: 11
Size: 250356 Color: 18

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 490776 Color: 14
Size: 257275 Color: 19
Size: 251950 Color: 19

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 490810 Color: 5
Size: 257331 Color: 0
Size: 251860 Color: 2

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 490912 Color: 17
Size: 255533 Color: 4
Size: 253556 Color: 14

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 490984 Color: 4
Size: 258364 Color: 18
Size: 250653 Color: 6

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 491018 Color: 7
Size: 256663 Color: 5
Size: 252320 Color: 16

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 491144 Color: 9
Size: 258210 Color: 10
Size: 250647 Color: 9

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 491191 Color: 16
Size: 254621 Color: 2
Size: 254189 Color: 10

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 491215 Color: 11
Size: 255025 Color: 3
Size: 253761 Color: 18

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 491247 Color: 15
Size: 257342 Color: 18
Size: 251412 Color: 1

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 491316 Color: 0
Size: 255183 Color: 5
Size: 253502 Color: 0

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 491375 Color: 0
Size: 256573 Color: 0
Size: 252053 Color: 11

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 491502 Color: 4
Size: 258246 Color: 2
Size: 250253 Color: 14

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 491904 Color: 4
Size: 257106 Color: 6
Size: 250991 Color: 8

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 491951 Color: 4
Size: 255694 Color: 10
Size: 252356 Color: 18

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 492001 Color: 6
Size: 256561 Color: 11
Size: 251439 Color: 15

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 492066 Color: 5
Size: 256310 Color: 5
Size: 251625 Color: 0

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 492113 Color: 16
Size: 256603 Color: 3
Size: 251285 Color: 9

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 492197 Color: 17
Size: 255086 Color: 2
Size: 252718 Color: 10

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 492318 Color: 1
Size: 253972 Color: 2
Size: 253711 Color: 0

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 492499 Color: 6
Size: 256896 Color: 0
Size: 250606 Color: 14

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 492507 Color: 16
Size: 254401 Color: 6
Size: 253093 Color: 13

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 492618 Color: 10
Size: 256849 Color: 6
Size: 250534 Color: 7

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 492722 Color: 7
Size: 255153 Color: 3
Size: 252126 Color: 2

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 492799 Color: 15
Size: 255368 Color: 3
Size: 251834 Color: 0

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 492986 Color: 8
Size: 256203 Color: 8
Size: 250812 Color: 12

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 493168 Color: 1
Size: 255729 Color: 10
Size: 251104 Color: 4

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 493175 Color: 9
Size: 253715 Color: 15
Size: 253111 Color: 1

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 493219 Color: 7
Size: 254952 Color: 3
Size: 251830 Color: 15

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 493247 Color: 9
Size: 254957 Color: 4
Size: 251797 Color: 11

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 493273 Color: 16
Size: 254418 Color: 14
Size: 252310 Color: 3

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 493290 Color: 16
Size: 256515 Color: 17
Size: 250196 Color: 9

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 493504 Color: 2
Size: 253425 Color: 7
Size: 253072 Color: 11

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 493373 Color: 9
Size: 255282 Color: 16
Size: 251346 Color: 3

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 493590 Color: 2
Size: 255772 Color: 15
Size: 250639 Color: 4

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 493543 Color: 11
Size: 256259 Color: 6
Size: 250199 Color: 8

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 493604 Color: 12
Size: 254932 Color: 15
Size: 251465 Color: 12

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 493622 Color: 18
Size: 255547 Color: 10
Size: 250832 Color: 1

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 493641 Color: 17
Size: 254311 Color: 4
Size: 252049 Color: 4

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 493685 Color: 11
Size: 255337 Color: 0
Size: 250979 Color: 2

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 493721 Color: 16
Size: 254562 Color: 8
Size: 251718 Color: 6

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 493722 Color: 8
Size: 254653 Color: 0
Size: 251626 Color: 5

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 493727 Color: 1
Size: 256042 Color: 17
Size: 250232 Color: 10

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 493745 Color: 4
Size: 254460 Color: 13
Size: 251796 Color: 1

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 493777 Color: 13
Size: 255968 Color: 2
Size: 250256 Color: 12

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 493857 Color: 14
Size: 255433 Color: 11
Size: 250711 Color: 13

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 493869 Color: 7
Size: 256098 Color: 6
Size: 250034 Color: 16

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 493873 Color: 5
Size: 255620 Color: 10
Size: 250508 Color: 16

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 493927 Color: 19
Size: 255588 Color: 8
Size: 250486 Color: 8

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 494007 Color: 12
Size: 255632 Color: 12
Size: 250362 Color: 17

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 494052 Color: 19
Size: 254046 Color: 11
Size: 251903 Color: 2

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 494142 Color: 13
Size: 255531 Color: 11
Size: 250328 Color: 10

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 494386 Color: 6
Size: 253977 Color: 17
Size: 251638 Color: 7

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 494470 Color: 5
Size: 254746 Color: 15
Size: 250785 Color: 11

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 494519 Color: 3
Size: 255301 Color: 0
Size: 250181 Color: 8

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 494660 Color: 11
Size: 253720 Color: 5
Size: 251621 Color: 6

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 494765 Color: 4
Size: 254927 Color: 13
Size: 250309 Color: 2

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 494815 Color: 1
Size: 253060 Color: 9
Size: 252126 Color: 19

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 494833 Color: 10
Size: 252926 Color: 8
Size: 252242 Color: 1

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 494956 Color: 15
Size: 253456 Color: 5
Size: 251589 Color: 14

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 494985 Color: 3
Size: 253468 Color: 11
Size: 251548 Color: 5

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 495171 Color: 6
Size: 253803 Color: 12
Size: 251027 Color: 11

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 495215 Color: 13
Size: 254376 Color: 16
Size: 250410 Color: 2

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 495249 Color: 5
Size: 254141 Color: 10
Size: 250611 Color: 17

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 495334 Color: 1
Size: 254112 Color: 1
Size: 250555 Color: 4

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 495354 Color: 14
Size: 254536 Color: 19
Size: 250111 Color: 8

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 495409 Color: 16
Size: 253266 Color: 15
Size: 251326 Color: 6

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 495447 Color: 3
Size: 253231 Color: 5
Size: 251323 Color: 11

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 495450 Color: 16
Size: 254105 Color: 1
Size: 250446 Color: 1

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 495484 Color: 1
Size: 253883 Color: 8
Size: 250634 Color: 18

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 495532 Color: 3
Size: 254143 Color: 9
Size: 250326 Color: 15

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 495681 Color: 3
Size: 253145 Color: 7
Size: 251175 Color: 7

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 495875 Color: 2
Size: 253354 Color: 11
Size: 250772 Color: 7

Bin 3126: 0 of cap free
Amount of items: 3
Items: 
Size: 495900 Color: 6
Size: 253751 Color: 2
Size: 250350 Color: 12

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 496187 Color: 13
Size: 252544 Color: 7
Size: 251270 Color: 12

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 496219 Color: 16
Size: 253484 Color: 12
Size: 250298 Color: 13

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 496238 Color: 15
Size: 253394 Color: 12
Size: 250369 Color: 17

Bin 3130: 0 of cap free
Amount of items: 3
Items: 
Size: 496252 Color: 8
Size: 253281 Color: 0
Size: 250468 Color: 15

Bin 3131: 0 of cap free
Amount of items: 3
Items: 
Size: 496260 Color: 9
Size: 252998 Color: 17
Size: 250743 Color: 3

Bin 3132: 0 of cap free
Amount of items: 3
Items: 
Size: 496316 Color: 6
Size: 253068 Color: 11
Size: 250617 Color: 10

Bin 3133: 0 of cap free
Amount of items: 3
Items: 
Size: 496443 Color: 18
Size: 253120 Color: 15
Size: 250438 Color: 16

Bin 3134: 0 of cap free
Amount of items: 3
Items: 
Size: 496464 Color: 9
Size: 252707 Color: 12
Size: 250830 Color: 16

Bin 3135: 0 of cap free
Amount of items: 3
Items: 
Size: 496579 Color: 7
Size: 251941 Color: 7
Size: 251481 Color: 3

Bin 3136: 0 of cap free
Amount of items: 3
Items: 
Size: 496626 Color: 13
Size: 252578 Color: 10
Size: 250797 Color: 7

Bin 3137: 0 of cap free
Amount of items: 3
Items: 
Size: 496888 Color: 12
Size: 251955 Color: 15
Size: 251158 Color: 6

Bin 3138: 0 of cap free
Amount of items: 3
Items: 
Size: 497136 Color: 2
Size: 251585 Color: 7
Size: 251280 Color: 5

Bin 3139: 0 of cap free
Amount of items: 3
Items: 
Size: 497052 Color: 9
Size: 251607 Color: 17
Size: 251342 Color: 11

Bin 3140: 0 of cap free
Amount of items: 3
Items: 
Size: 497300 Color: 19
Size: 251798 Color: 6
Size: 250903 Color: 10

Bin 3141: 0 of cap free
Amount of items: 3
Items: 
Size: 497318 Color: 0
Size: 252595 Color: 10
Size: 250088 Color: 12

Bin 3142: 0 of cap free
Amount of items: 3
Items: 
Size: 497391 Color: 16
Size: 252510 Color: 2
Size: 250100 Color: 3

Bin 3143: 0 of cap free
Amount of items: 3
Items: 
Size: 497414 Color: 0
Size: 251356 Color: 16
Size: 251231 Color: 11

Bin 3144: 0 of cap free
Amount of items: 3
Items: 
Size: 497660 Color: 3
Size: 251870 Color: 11
Size: 250471 Color: 16

Bin 3145: 0 of cap free
Amount of items: 3
Items: 
Size: 497759 Color: 13
Size: 251227 Color: 8
Size: 251015 Color: 14

Bin 3146: 0 of cap free
Amount of items: 3
Items: 
Size: 497808 Color: 15
Size: 251475 Color: 14
Size: 250718 Color: 11

Bin 3147: 0 of cap free
Amount of items: 3
Items: 
Size: 497882 Color: 7
Size: 252024 Color: 3
Size: 250095 Color: 2

Bin 3148: 0 of cap free
Amount of items: 3
Items: 
Size: 498046 Color: 15
Size: 251082 Color: 0
Size: 250873 Color: 18

Bin 3149: 0 of cap free
Amount of items: 3
Items: 
Size: 498223 Color: 14
Size: 251655 Color: 8
Size: 250123 Color: 16

Bin 3150: 0 of cap free
Amount of items: 3
Items: 
Size: 498368 Color: 12
Size: 251051 Color: 6
Size: 250582 Color: 7

Bin 3151: 0 of cap free
Amount of items: 3
Items: 
Size: 498370 Color: 13
Size: 251215 Color: 19
Size: 250416 Color: 14

Bin 3152: 0 of cap free
Amount of items: 3
Items: 
Size: 498489 Color: 14
Size: 251395 Color: 9
Size: 250117 Color: 9

Bin 3153: 0 of cap free
Amount of items: 3
Items: 
Size: 498599 Color: 4
Size: 251019 Color: 2
Size: 250383 Color: 14

Bin 3154: 0 of cap free
Amount of items: 3
Items: 
Size: 498620 Color: 6
Size: 251042 Color: 3
Size: 250339 Color: 18

Bin 3155: 0 of cap free
Amount of items: 3
Items: 
Size: 498642 Color: 8
Size: 250834 Color: 12
Size: 250525 Color: 10

Bin 3156: 0 of cap free
Amount of items: 3
Items: 
Size: 498671 Color: 19
Size: 250959 Color: 12
Size: 250371 Color: 18

Bin 3157: 0 of cap free
Amount of items: 3
Items: 
Size: 498683 Color: 12
Size: 251211 Color: 6
Size: 250107 Color: 14

Bin 3158: 0 of cap free
Amount of items: 3
Items: 
Size: 498802 Color: 3
Size: 251165 Color: 14
Size: 250034 Color: 12

Bin 3159: 0 of cap free
Amount of items: 3
Items: 
Size: 498903 Color: 5
Size: 250664 Color: 11
Size: 250434 Color: 11

Bin 3160: 0 of cap free
Amount of items: 3
Items: 
Size: 498926 Color: 10
Size: 251056 Color: 0
Size: 250019 Color: 1

Bin 3161: 0 of cap free
Amount of items: 3
Items: 
Size: 498952 Color: 1
Size: 250603 Color: 0
Size: 250446 Color: 16

Bin 3162: 0 of cap free
Amount of items: 3
Items: 
Size: 499099 Color: 13
Size: 250717 Color: 3
Size: 250185 Color: 15

Bin 3163: 0 of cap free
Amount of items: 3
Items: 
Size: 499248 Color: 2
Size: 250536 Color: 16
Size: 250217 Color: 4

Bin 3164: 0 of cap free
Amount of items: 3
Items: 
Size: 499161 Color: 1
Size: 250578 Color: 13
Size: 250262 Color: 11

Bin 3165: 0 of cap free
Amount of items: 3
Items: 
Size: 499259 Color: 16
Size: 250561 Color: 2
Size: 250181 Color: 11

Bin 3166: 1 of cap free
Amount of items: 3
Items: 
Size: 354401 Color: 2
Size: 328461 Color: 8
Size: 317138 Color: 6

Bin 3167: 1 of cap free
Amount of items: 3
Items: 
Size: 374001 Color: 9
Size: 356748 Color: 14
Size: 269251 Color: 4

Bin 3168: 1 of cap free
Amount of items: 3
Items: 
Size: 363240 Color: 11
Size: 339007 Color: 11
Size: 297753 Color: 16

Bin 3169: 1 of cap free
Amount of items: 3
Items: 
Size: 361850 Color: 9
Size: 333727 Color: 16
Size: 304423 Color: 10

Bin 3170: 1 of cap free
Amount of items: 3
Items: 
Size: 343475 Color: 9
Size: 334473 Color: 10
Size: 322052 Color: 12

Bin 3171: 1 of cap free
Amount of items: 3
Items: 
Size: 387870 Color: 8
Size: 335472 Color: 9
Size: 276658 Color: 19

Bin 3172: 1 of cap free
Amount of items: 3
Items: 
Size: 359061 Color: 15
Size: 345070 Color: 12
Size: 295869 Color: 5

Bin 3173: 1 of cap free
Amount of items: 3
Items: 
Size: 360390 Color: 7
Size: 352538 Color: 13
Size: 287072 Color: 18

Bin 3174: 1 of cap free
Amount of items: 3
Items: 
Size: 360800 Color: 16
Size: 322867 Color: 13
Size: 316333 Color: 9

Bin 3175: 1 of cap free
Amount of items: 3
Items: 
Size: 362828 Color: 11
Size: 342528 Color: 10
Size: 294644 Color: 11

Bin 3176: 1 of cap free
Amount of items: 3
Items: 
Size: 363922 Color: 18
Size: 329222 Color: 5
Size: 306856 Color: 13

Bin 3177: 1 of cap free
Amount of items: 3
Items: 
Size: 344690 Color: 2
Size: 337178 Color: 15
Size: 318132 Color: 4

Bin 3178: 1 of cap free
Amount of items: 3
Items: 
Size: 364703 Color: 10
Size: 317668 Color: 0
Size: 317629 Color: 15

Bin 3179: 1 of cap free
Amount of items: 3
Items: 
Size: 365406 Color: 8
Size: 365321 Color: 0
Size: 269273 Color: 19

Bin 3180: 1 of cap free
Amount of items: 3
Items: 
Size: 367230 Color: 13
Size: 349624 Color: 6
Size: 283146 Color: 14

Bin 3181: 1 of cap free
Amount of items: 3
Items: 
Size: 367761 Color: 4
Size: 342291 Color: 0
Size: 289948 Color: 2

Bin 3182: 1 of cap free
Amount of items: 3
Items: 
Size: 371010 Color: 0
Size: 323280 Color: 16
Size: 305710 Color: 18

Bin 3183: 1 of cap free
Amount of items: 3
Items: 
Size: 371247 Color: 4
Size: 364203 Color: 5
Size: 264550 Color: 18

Bin 3184: 1 of cap free
Amount of items: 3
Items: 
Size: 371260 Color: 8
Size: 350231 Color: 16
Size: 278509 Color: 5

Bin 3185: 1 of cap free
Amount of items: 3
Items: 
Size: 375502 Color: 2
Size: 314045 Color: 5
Size: 310453 Color: 18

Bin 3186: 1 of cap free
Amount of items: 3
Items: 
Size: 372766 Color: 6
Size: 358080 Color: 18
Size: 269154 Color: 5

Bin 3187: 1 of cap free
Amount of items: 3
Items: 
Size: 375074 Color: 17
Size: 356164 Color: 7
Size: 268762 Color: 4

Bin 3188: 1 of cap free
Amount of items: 3
Items: 
Size: 353962 Color: 2
Size: 338708 Color: 11
Size: 307330 Color: 19

Bin 3189: 1 of cap free
Amount of items: 3
Items: 
Size: 349065 Color: 9
Size: 346071 Color: 5
Size: 304864 Color: 5

Bin 3190: 1 of cap free
Amount of items: 3
Items: 
Size: 376954 Color: 11
Size: 371967 Color: 0
Size: 251079 Color: 2

Bin 3191: 1 of cap free
Amount of items: 3
Items: 
Size: 358687 Color: 6
Size: 322275 Color: 7
Size: 319038 Color: 9

Bin 3192: 1 of cap free
Amount of items: 3
Items: 
Size: 377845 Color: 2
Size: 352838 Color: 0
Size: 269317 Color: 10

Bin 3193: 1 of cap free
Amount of items: 3
Items: 
Size: 379234 Color: 13
Size: 323879 Color: 0
Size: 296887 Color: 13

Bin 3194: 1 of cap free
Amount of items: 3
Items: 
Size: 379894 Color: 2
Size: 369050 Color: 11
Size: 251056 Color: 12

Bin 3195: 1 of cap free
Amount of items: 3
Items: 
Size: 380347 Color: 13
Size: 317936 Color: 18
Size: 301717 Color: 0

Bin 3196: 1 of cap free
Amount of items: 3
Items: 
Size: 380934 Color: 5
Size: 324534 Color: 0
Size: 294532 Color: 7

Bin 3197: 1 of cap free
Amount of items: 3
Items: 
Size: 390348 Color: 15
Size: 330708 Color: 9
Size: 278944 Color: 3

Bin 3198: 1 of cap free
Amount of items: 3
Items: 
Size: 391343 Color: 13
Size: 351880 Color: 8
Size: 256777 Color: 18

Bin 3199: 1 of cap free
Amount of items: 3
Items: 
Size: 426265 Color: 10
Size: 320962 Color: 2
Size: 252773 Color: 4

Bin 3200: 1 of cap free
Amount of items: 3
Items: 
Size: 353933 Color: 18
Size: 353930 Color: 4
Size: 292137 Color: 13

Bin 3201: 1 of cap free
Amount of items: 3
Items: 
Size: 362938 Color: 6
Size: 318582 Color: 18
Size: 318480 Color: 7

Bin 3202: 1 of cap free
Amount of items: 3
Items: 
Size: 360951 Color: 9
Size: 358390 Color: 6
Size: 280659 Color: 16

Bin 3203: 1 of cap free
Amount of items: 3
Items: 
Size: 343034 Color: 11
Size: 334243 Color: 14
Size: 322723 Color: 15

Bin 3204: 1 of cap free
Amount of items: 3
Items: 
Size: 361729 Color: 18
Size: 344733 Color: 8
Size: 293538 Color: 0

Bin 3205: 1 of cap free
Amount of items: 3
Items: 
Size: 357989 Color: 12
Size: 329765 Color: 14
Size: 312246 Color: 17

Bin 3206: 1 of cap free
Amount of items: 3
Items: 
Size: 356314 Color: 14
Size: 352973 Color: 19
Size: 290713 Color: 3

Bin 3207: 1 of cap free
Amount of items: 3
Items: 
Size: 333885 Color: 11
Size: 333797 Color: 8
Size: 332318 Color: 4

Bin 3208: 1 of cap free
Amount of items: 3
Items: 
Size: 358793 Color: 1
Size: 355677 Color: 18
Size: 285530 Color: 13

Bin 3209: 1 of cap free
Amount of items: 3
Items: 
Size: 358462 Color: 18
Size: 352712 Color: 6
Size: 288826 Color: 2

Bin 3210: 1 of cap free
Amount of items: 3
Items: 
Size: 353420 Color: 4
Size: 328236 Color: 17
Size: 318344 Color: 13

Bin 3211: 1 of cap free
Amount of items: 3
Items: 
Size: 389336 Color: 8
Size: 352126 Color: 11
Size: 258538 Color: 18

Bin 3212: 1 of cap free
Amount of items: 3
Items: 
Size: 372250 Color: 0
Size: 355505 Color: 8
Size: 272245 Color: 8

Bin 3213: 1 of cap free
Amount of items: 3
Items: 
Size: 349346 Color: 19
Size: 344349 Color: 9
Size: 306305 Color: 15

Bin 3214: 1 of cap free
Amount of items: 3
Items: 
Size: 349691 Color: 19
Size: 349660 Color: 19
Size: 300649 Color: 9

Bin 3215: 1 of cap free
Amount of items: 3
Items: 
Size: 348879 Color: 6
Size: 346761 Color: 0
Size: 304360 Color: 8

Bin 3216: 1 of cap free
Amount of items: 3
Items: 
Size: 348416 Color: 9
Size: 332046 Color: 14
Size: 319538 Color: 11

Bin 3217: 1 of cap free
Amount of items: 3
Items: 
Size: 345006 Color: 4
Size: 336642 Color: 10
Size: 318352 Color: 13

Bin 3218: 1 of cap free
Amount of items: 3
Items: 
Size: 347888 Color: 6
Size: 326873 Color: 12
Size: 325239 Color: 12

Bin 3219: 1 of cap free
Amount of items: 3
Items: 
Size: 343777 Color: 0
Size: 333288 Color: 17
Size: 322935 Color: 7

Bin 3220: 1 of cap free
Amount of items: 3
Items: 
Size: 335186 Color: 17
Size: 334417 Color: 19
Size: 330397 Color: 14

Bin 3221: 2 of cap free
Amount of items: 3
Items: 
Size: 341873 Color: 15
Size: 329446 Color: 8
Size: 328680 Color: 9

Bin 3222: 2 of cap free
Amount of items: 3
Items: 
Size: 358244 Color: 3
Size: 355308 Color: 2
Size: 286447 Color: 17

Bin 3223: 2 of cap free
Amount of items: 3
Items: 
Size: 362147 Color: 3
Size: 328467 Color: 18
Size: 309385 Color: 19

Bin 3224: 2 of cap free
Amount of items: 3
Items: 
Size: 368738 Color: 13
Size: 365807 Color: 9
Size: 265454 Color: 7

Bin 3225: 2 of cap free
Amount of items: 3
Items: 
Size: 361340 Color: 15
Size: 321777 Color: 2
Size: 316882 Color: 19

Bin 3226: 2 of cap free
Amount of items: 3
Items: 
Size: 362438 Color: 0
Size: 359784 Color: 11
Size: 277777 Color: 8

Bin 3227: 2 of cap free
Amount of items: 3
Items: 
Size: 370535 Color: 9
Size: 337431 Color: 2
Size: 292033 Color: 19

Bin 3228: 2 of cap free
Amount of items: 3
Items: 
Size: 374671 Color: 0
Size: 365965 Color: 18
Size: 259363 Color: 5

Bin 3229: 2 of cap free
Amount of items: 3
Items: 
Size: 391362 Color: 12
Size: 348494 Color: 19
Size: 260143 Color: 1

Bin 3230: 2 of cap free
Amount of items: 3
Items: 
Size: 347524 Color: 3
Size: 334311 Color: 16
Size: 318164 Color: 16

Bin 3231: 2 of cap free
Amount of items: 3
Items: 
Size: 348317 Color: 12
Size: 346582 Color: 16
Size: 305100 Color: 2

Bin 3232: 2 of cap free
Amount of items: 3
Items: 
Size: 381888 Color: 7
Size: 366671 Color: 0
Size: 251440 Color: 5

Bin 3233: 2 of cap free
Amount of items: 3
Items: 
Size: 349931 Color: 4
Size: 331947 Color: 3
Size: 318121 Color: 8

Bin 3234: 2 of cap free
Amount of items: 3
Items: 
Size: 339430 Color: 0
Size: 336593 Color: 1
Size: 323976 Color: 18

Bin 3235: 3 of cap free
Amount of items: 3
Items: 
Size: 336215 Color: 11
Size: 335354 Color: 3
Size: 328429 Color: 7

Bin 3236: 3 of cap free
Amount of items: 3
Items: 
Size: 351278 Color: 0
Size: 331694 Color: 5
Size: 317026 Color: 2

Bin 3237: 3 of cap free
Amount of items: 3
Items: 
Size: 364188 Color: 5
Size: 363129 Color: 9
Size: 272681 Color: 14

Bin 3238: 3 of cap free
Amount of items: 3
Items: 
Size: 344626 Color: 15
Size: 343704 Color: 9
Size: 311668 Color: 2

Bin 3239: 3 of cap free
Amount of items: 3
Items: 
Size: 374909 Color: 18
Size: 366603 Color: 11
Size: 258486 Color: 15

Bin 3240: 3 of cap free
Amount of items: 3
Items: 
Size: 360339 Color: 11
Size: 343441 Color: 7
Size: 296218 Color: 1

Bin 3241: 3 of cap free
Amount of items: 3
Items: 
Size: 366840 Color: 6
Size: 348071 Color: 6
Size: 285087 Color: 3

Bin 3242: 3 of cap free
Amount of items: 3
Items: 
Size: 357623 Color: 12
Size: 343025 Color: 3
Size: 299350 Color: 2

Bin 3243: 3 of cap free
Amount of items: 3
Items: 
Size: 358341 Color: 12
Size: 354208 Color: 18
Size: 287449 Color: 3

Bin 3244: 3 of cap free
Amount of items: 3
Items: 
Size: 372831 Color: 19
Size: 360307 Color: 0
Size: 266860 Color: 6

Bin 3245: 3 of cap free
Amount of items: 3
Items: 
Size: 378330 Color: 10
Size: 348011 Color: 14
Size: 273657 Color: 5

Bin 3246: 4 of cap free
Amount of items: 3
Items: 
Size: 348650 Color: 5
Size: 330432 Color: 0
Size: 320915 Color: 19

Bin 3247: 4 of cap free
Amount of items: 3
Items: 
Size: 365671 Color: 16
Size: 354311 Color: 0
Size: 280015 Color: 14

Bin 3248: 4 of cap free
Amount of items: 3
Items: 
Size: 367110 Color: 17
Size: 362852 Color: 5
Size: 270035 Color: 10

Bin 3249: 4 of cap free
Amount of items: 3
Items: 
Size: 379674 Color: 14
Size: 340681 Color: 8
Size: 279642 Color: 0

Bin 3250: 4 of cap free
Amount of items: 3
Items: 
Size: 342856 Color: 3
Size: 330033 Color: 12
Size: 327108 Color: 10

Bin 3251: 4 of cap free
Amount of items: 3
Items: 
Size: 371106 Color: 1
Size: 353397 Color: 16
Size: 275494 Color: 13

Bin 3252: 4 of cap free
Amount of items: 3
Items: 
Size: 357968 Color: 2
Size: 338585 Color: 12
Size: 303444 Color: 10

Bin 3253: 5 of cap free
Amount of items: 3
Items: 
Size: 339991 Color: 19
Size: 331836 Color: 9
Size: 328169 Color: 9

Bin 3254: 5 of cap free
Amount of items: 3
Items: 
Size: 383056 Color: 19
Size: 355964 Color: 3
Size: 260976 Color: 0

Bin 3255: 5 of cap free
Amount of items: 3
Items: 
Size: 352315 Color: 4
Size: 345292 Color: 7
Size: 302389 Color: 17

Bin 3256: 6 of cap free
Amount of items: 3
Items: 
Size: 387763 Color: 5
Size: 358019 Color: 18
Size: 254213 Color: 0

Bin 3257: 6 of cap free
Amount of items: 3
Items: 
Size: 355388 Color: 13
Size: 336320 Color: 9
Size: 308287 Color: 19

Bin 3258: 7 of cap free
Amount of items: 3
Items: 
Size: 407804 Color: 1
Size: 340146 Color: 2
Size: 252044 Color: 11

Bin 3259: 7 of cap free
Amount of items: 3
Items: 
Size: 377372 Color: 4
Size: 369865 Color: 0
Size: 252757 Color: 0

Bin 3260: 7 of cap free
Amount of items: 3
Items: 
Size: 356150 Color: 11
Size: 338071 Color: 18
Size: 305773 Color: 5

Bin 3261: 8 of cap free
Amount of items: 3
Items: 
Size: 352145 Color: 19
Size: 329588 Color: 9
Size: 318260 Color: 16

Bin 3262: 8 of cap free
Amount of items: 3
Items: 
Size: 347812 Color: 14
Size: 339272 Color: 6
Size: 312909 Color: 2

Bin 3263: 8 of cap free
Amount of items: 3
Items: 
Size: 343231 Color: 14
Size: 337518 Color: 12
Size: 319244 Color: 10

Bin 3264: 9 of cap free
Amount of items: 3
Items: 
Size: 348640 Color: 6
Size: 334672 Color: 15
Size: 316680 Color: 15

Bin 3265: 9 of cap free
Amount of items: 3
Items: 
Size: 347120 Color: 12
Size: 346567 Color: 17
Size: 306305 Color: 2

Bin 3266: 10 of cap free
Amount of items: 3
Items: 
Size: 499453 Color: 3
Size: 250431 Color: 4
Size: 250107 Color: 11

Bin 3267: 10 of cap free
Amount of items: 3
Items: 
Size: 358560 Color: 12
Size: 354729 Color: 19
Size: 286702 Color: 12

Bin 3268: 10 of cap free
Amount of items: 3
Items: 
Size: 353342 Color: 1
Size: 330726 Color: 7
Size: 315923 Color: 7

Bin 3269: 11 of cap free
Amount of items: 3
Items: 
Size: 359423 Color: 2
Size: 355193 Color: 17
Size: 285374 Color: 8

Bin 3270: 11 of cap free
Amount of items: 3
Items: 
Size: 351063 Color: 14
Size: 327446 Color: 10
Size: 321481 Color: 3

Bin 3271: 11 of cap free
Amount of items: 3
Items: 
Size: 346854 Color: 1
Size: 346313 Color: 16
Size: 306823 Color: 18

Bin 3272: 12 of cap free
Amount of items: 3
Items: 
Size: 353720 Color: 12
Size: 324120 Color: 10
Size: 322149 Color: 5

Bin 3273: 13 of cap free
Amount of items: 3
Items: 
Size: 360675 Color: 6
Size: 360473 Color: 1
Size: 278840 Color: 0

Bin 3274: 14 of cap free
Amount of items: 3
Items: 
Size: 339842 Color: 14
Size: 335959 Color: 4
Size: 324186 Color: 2

Bin 3275: 14 of cap free
Amount of items: 3
Items: 
Size: 353099 Color: 16
Size: 331883 Color: 1
Size: 315005 Color: 4

Bin 3276: 16 of cap free
Amount of items: 3
Items: 
Size: 345369 Color: 19
Size: 329371 Color: 4
Size: 325245 Color: 7

Bin 3277: 18 of cap free
Amount of items: 3
Items: 
Size: 343695 Color: 8
Size: 342244 Color: 10
Size: 314044 Color: 0

Bin 3278: 20 of cap free
Amount of items: 3
Items: 
Size: 339087 Color: 16
Size: 336974 Color: 2
Size: 323920 Color: 0

Bin 3279: 21 of cap free
Amount of items: 3
Items: 
Size: 345897 Color: 10
Size: 330055 Color: 10
Size: 324028 Color: 2

Bin 3280: 22 of cap free
Amount of items: 3
Items: 
Size: 358277 Color: 19
Size: 345075 Color: 2
Size: 296627 Color: 0

Bin 3281: 23 of cap free
Amount of items: 3
Items: 
Size: 378806 Color: 3
Size: 367268 Color: 1
Size: 253904 Color: 13

Bin 3282: 24 of cap free
Amount of items: 3
Items: 
Size: 372786 Color: 6
Size: 365634 Color: 4
Size: 261557 Color: 2

Bin 3283: 27 of cap free
Amount of items: 3
Items: 
Size: 340468 Color: 4
Size: 333748 Color: 14
Size: 325758 Color: 13

Bin 3284: 28 of cap free
Amount of items: 3
Items: 
Size: 389799 Color: 14
Size: 358498 Color: 1
Size: 251676 Color: 3

Bin 3285: 30 of cap free
Amount of items: 3
Items: 
Size: 349081 Color: 10
Size: 334365 Color: 19
Size: 316525 Color: 17

Bin 3286: 30 of cap free
Amount of items: 3
Items: 
Size: 356648 Color: 0
Size: 355947 Color: 12
Size: 287376 Color: 18

Bin 3287: 31 of cap free
Amount of items: 3
Items: 
Size: 357084 Color: 18
Size: 356134 Color: 3
Size: 286752 Color: 9

Bin 3288: 35 of cap free
Amount of items: 3
Items: 
Size: 335720 Color: 9
Size: 332669 Color: 2
Size: 331577 Color: 11

Bin 3289: 35 of cap free
Amount of items: 3
Items: 
Size: 398560 Color: 1
Size: 351251 Color: 14
Size: 250155 Color: 9

Bin 3290: 52 of cap free
Amount of items: 3
Items: 
Size: 362823 Color: 16
Size: 360266 Color: 4
Size: 276860 Color: 2

Bin 3291: 60 of cap free
Amount of items: 3
Items: 
Size: 377245 Color: 14
Size: 364340 Color: 5
Size: 258356 Color: 16

Bin 3292: 68 of cap free
Amount of items: 3
Items: 
Size: 343597 Color: 6
Size: 340387 Color: 15
Size: 315949 Color: 3

Bin 3293: 79 of cap free
Amount of items: 3
Items: 
Size: 362927 Color: 2
Size: 360579 Color: 8
Size: 276416 Color: 18

Bin 3294: 83 of cap free
Amount of items: 3
Items: 
Size: 360720 Color: 0
Size: 358914 Color: 8
Size: 280284 Color: 11

Bin 3295: 86 of cap free
Amount of items: 3
Items: 
Size: 350089 Color: 8
Size: 327000 Color: 1
Size: 322826 Color: 12

Bin 3296: 88 of cap free
Amount of items: 3
Items: 
Size: 345650 Color: 2
Size: 331662 Color: 4
Size: 322601 Color: 9

Bin 3297: 100 of cap free
Amount of items: 3
Items: 
Size: 356392 Color: 0
Size: 346829 Color: 15
Size: 296680 Color: 4

Bin 3298: 104 of cap free
Amount of items: 3
Items: 
Size: 371083 Color: 9
Size: 352137 Color: 19
Size: 276677 Color: 5

Bin 3299: 107 of cap free
Amount of items: 3
Items: 
Size: 406072 Color: 4
Size: 335822 Color: 18
Size: 258000 Color: 8

Bin 3300: 130 of cap free
Amount of items: 3
Items: 
Size: 350649 Color: 16
Size: 329720 Color: 11
Size: 319502 Color: 6

Bin 3301: 145 of cap free
Amount of items: 3
Items: 
Size: 367486 Color: 13
Size: 359176 Color: 19
Size: 273194 Color: 2

Bin 3302: 155 of cap free
Amount of items: 3
Items: 
Size: 488014 Color: 5
Size: 257725 Color: 6
Size: 254107 Color: 15

Bin 3303: 160 of cap free
Amount of items: 3
Items: 
Size: 362471 Color: 13
Size: 332801 Color: 15
Size: 304569 Color: 11

Bin 3304: 169 of cap free
Amount of items: 3
Items: 
Size: 336840 Color: 5
Size: 331879 Color: 8
Size: 331113 Color: 19

Bin 3305: 189 of cap free
Amount of items: 3
Items: 
Size: 341041 Color: 17
Size: 333263 Color: 2
Size: 325508 Color: 13

Bin 3306: 234 of cap free
Amount of items: 3
Items: 
Size: 353583 Color: 2
Size: 353208 Color: 8
Size: 292976 Color: 19

Bin 3307: 292 of cap free
Amount of items: 3
Items: 
Size: 474497 Color: 18
Size: 272270 Color: 15
Size: 252942 Color: 2

Bin 3308: 386 of cap free
Amount of items: 3
Items: 
Size: 405179 Color: 18
Size: 340336 Color: 1
Size: 254100 Color: 0

Bin 3309: 454 of cap free
Amount of items: 3
Items: 
Size: 367475 Color: 6
Size: 364119 Color: 2
Size: 267953 Color: 18

Bin 3310: 478 of cap free
Amount of items: 3
Items: 
Size: 359161 Color: 15
Size: 340805 Color: 17
Size: 299557 Color: 17

Bin 3311: 480 of cap free
Amount of items: 2
Items: 
Size: 499867 Color: 4
Size: 499654 Color: 0

Bin 3312: 496 of cap free
Amount of items: 3
Items: 
Size: 339393 Color: 16
Size: 338877 Color: 7
Size: 321235 Color: 2

Bin 3313: 531 of cap free
Amount of items: 3
Items: 
Size: 478005 Color: 18
Size: 265284 Color: 16
Size: 256181 Color: 2

Bin 3314: 604 of cap free
Amount of items: 3
Items: 
Size: 409249 Color: 16
Size: 330442 Color: 16
Size: 259706 Color: 12

Bin 3315: 983 of cap free
Amount of items: 2
Items: 
Size: 499522 Color: 9
Size: 499496 Color: 17

Bin 3316: 1178 of cap free
Amount of items: 2
Items: 
Size: 499452 Color: 7
Size: 499371 Color: 3

Bin 3317: 2500 of cap free
Amount of items: 3
Items: 
Size: 362442 Color: 19
Size: 358402 Color: 4
Size: 276657 Color: 18

Bin 3318: 3607 of cap free
Amount of items: 3
Items: 
Size: 366638 Color: 11
Size: 358191 Color: 18
Size: 271565 Color: 0

Bin 3319: 16245 of cap free
Amount of items: 3
Items: 
Size: 357950 Color: 18
Size: 357923 Color: 11
Size: 267883 Color: 8

Bin 3320: 16880 of cap free
Amount of items: 3
Items: 
Size: 357734 Color: 12
Size: 357582 Color: 7
Size: 267805 Color: 2

Bin 3321: 21041 of cap free
Amount of items: 3
Items: 
Size: 357170 Color: 5
Size: 356919 Color: 8
Size: 264871 Color: 18

Bin 3322: 26756 of cap free
Amount of items: 3
Items: 
Size: 355193 Color: 18
Size: 354565 Color: 1
Size: 263487 Color: 14

Bin 3323: 29173 of cap free
Amount of items: 3
Items: 
Size: 354530 Color: 14
Size: 354092 Color: 1
Size: 262206 Color: 8

Bin 3324: 30042 of cap free
Amount of items: 3
Items: 
Size: 354077 Color: 17
Size: 353861 Color: 19
Size: 262021 Color: 18

Bin 3325: 33906 of cap free
Amount of items: 3
Items: 
Size: 352134 Color: 14
Size: 351986 Color: 16
Size: 261975 Color: 16

Bin 3326: 35839 of cap free
Amount of items: 3
Items: 
Size: 351481 Color: 18
Size: 350981 Color: 10
Size: 261700 Color: 8

Bin 3327: 39754 of cap free
Amount of items: 3
Items: 
Size: 350584 Color: 6
Size: 350551 Color: 13
Size: 259112 Color: 18

Bin 3328: 40927 of cap free
Amount of items: 3
Items: 
Size: 350391 Color: 12
Size: 350389 Color: 17
Size: 258294 Color: 13

Bin 3329: 42855 of cap free
Amount of items: 3
Items: 
Size: 349971 Color: 2
Size: 349918 Color: 16
Size: 257257 Color: 10

Bin 3330: 46994 of cap free
Amount of items: 3
Items: 
Size: 349303 Color: 19
Size: 346541 Color: 3
Size: 257163 Color: 17

Bin 3331: 56186 of cap free
Amount of items: 3
Items: 
Size: 345044 Color: 3
Size: 341987 Color: 15
Size: 256784 Color: 5

Bin 3332: 70702 of cap free
Amount of items: 3
Items: 
Size: 338213 Color: 1
Size: 335238 Color: 10
Size: 255848 Color: 14

Bin 3333: 236587 of cap free
Amount of items: 3
Items: 
Size: 255833 Color: 5
Size: 253949 Color: 7
Size: 253632 Color: 5

Bin 3334: 241857 of cap free
Amount of items: 3
Items: 
Size: 253295 Color: 14
Size: 252539 Color: 11
Size: 252310 Color: 17

Bin 3335: 250048 of cap free
Amount of items: 2
Items: 
Size: 499358 Color: 0
Size: 250595 Color: 9

Bin 3336: 749502 of cap free
Amount of items: 1
Items: 
Size: 250499 Color: 2

Total size: 3334003334
Total free space: 2000002

