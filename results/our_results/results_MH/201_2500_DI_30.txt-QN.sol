Capicity Bin: 2032
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1408 Color: 155
Size: 420 Color: 111
Size: 76 Color: 45
Size: 60 Color: 32
Size: 40 Color: 11
Size: 20 Color: 2
Size: 8 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 201
Size: 174 Color: 69
Size: 32 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1142 Color: 143
Size: 742 Color: 131
Size: 148 Color: 65

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 162
Size: 425 Color: 112
Size: 84 Color: 48

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 170
Size: 342 Color: 103
Size: 64 Color: 35

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 166
Size: 377 Color: 107
Size: 74 Color: 43

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 177
Size: 282 Color: 95
Size: 52 Color: 28

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1186 Color: 146
Size: 710 Color: 128
Size: 136 Color: 62

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 198
Size: 203 Color: 76
Size: 22 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1277 Color: 150
Size: 631 Color: 124
Size: 124 Color: 58

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 184
Size: 251 Color: 88
Size: 48 Color: 23

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 193
Size: 205 Color: 78
Size: 40 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1453 Color: 157
Size: 483 Color: 118
Size: 96 Color: 52

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1455 Color: 158
Size: 481 Color: 117
Size: 96 Color: 51

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 165
Size: 382 Color: 109
Size: 72 Color: 40

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 200
Size: 182 Color: 70
Size: 32 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1695 Color: 176
Size: 281 Color: 94
Size: 56 Color: 30

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 186
Size: 233 Color: 86
Size: 46 Color: 20

Bin 19: 0 of cap free
Amount of items: 4
Items: 
Size: 1272 Color: 148
Size: 468 Color: 116
Size: 220 Color: 84
Size: 72 Color: 39

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 196
Size: 194 Color: 73
Size: 36 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 182
Size: 262 Color: 90
Size: 48 Color: 22

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 164
Size: 381 Color: 108
Size: 74 Color: 42

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 1666 Color: 174
Size: 306 Color: 98
Size: 52 Color: 26
Size: 8 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 152
Size: 570 Color: 122
Size: 112 Color: 57

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 168
Size: 374 Color: 106
Size: 72 Color: 37

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 173
Size: 309 Color: 99
Size: 60 Color: 33

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 172
Size: 322 Color: 101
Size: 64 Color: 34

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 163
Size: 410 Color: 110
Size: 80 Color: 46

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 199
Size: 186 Color: 71
Size: 36 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 189
Size: 218 Color: 82
Size: 40 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1779 Color: 190
Size: 211 Color: 80
Size: 42 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 197
Size: 191 Color: 72
Size: 38 Color: 10

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1159 Color: 145
Size: 729 Color: 129
Size: 144 Color: 63

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 169
Size: 367 Color: 104
Size: 72 Color: 38

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 156
Size: 514 Color: 119
Size: 100 Color: 53

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 187
Size: 231 Color: 85
Size: 44 Color: 19

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 192
Size: 210 Color: 79
Size: 40 Color: 14

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 160
Size: 427 Color: 114
Size: 84 Color: 49

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 194
Size: 202 Color: 74
Size: 40 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1258 Color: 147
Size: 646 Color: 126
Size: 128 Color: 60

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 185
Size: 246 Color: 87
Size: 48 Color: 21

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 154
Size: 549 Color: 120
Size: 108 Color: 54

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1018 Color: 139
Size: 886 Color: 137
Size: 128 Color: 61

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 180
Size: 274 Color: 92
Size: 52 Color: 25

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1342 Color: 151
Size: 578 Color: 123
Size: 112 Color: 56

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1102 Color: 142
Size: 858 Color: 136
Size: 72 Color: 41

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 183
Size: 257 Color: 89
Size: 50 Color: 24

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1705 Color: 179
Size: 289 Color: 96
Size: 38 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 167
Size: 373 Color: 105
Size: 74 Color: 44

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1042 Color: 141
Size: 826 Color: 133
Size: 164 Color: 66

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 161
Size: 426 Color: 113
Size: 84 Color: 47

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1273 Color: 149
Size: 633 Color: 125
Size: 126 Color: 59

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1474 Color: 159
Size: 466 Color: 115
Size: 92 Color: 50

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1631 Color: 171
Size: 335 Color: 102
Size: 66 Color: 36

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1371 Color: 153
Size: 551 Color: 121
Size: 110 Color: 55

Bin 56: 0 of cap free
Amount of items: 4
Items: 
Size: 824 Color: 132
Size: 692 Color: 127
Size: 312 Color: 100
Size: 204 Color: 77

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 178
Size: 277 Color: 93
Size: 54 Color: 29

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 181
Size: 266 Color: 91
Size: 52 Color: 27

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1019 Color: 140
Size: 845 Color: 134
Size: 168 Color: 67

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 195
Size: 202 Color: 75
Size: 36 Color: 7

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 191
Size: 212 Color: 81
Size: 40 Color: 15

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1017 Color: 138
Size: 847 Color: 135
Size: 168 Color: 68

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1157 Color: 144
Size: 731 Color: 130
Size: 144 Color: 64

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 175
Size: 303 Color: 97
Size: 60 Color: 31

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 188
Size: 219 Color: 83
Size: 42 Color: 17

Total size: 132080
Total free space: 0

