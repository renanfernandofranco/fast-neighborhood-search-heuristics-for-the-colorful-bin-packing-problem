Capicity Bin: 4912
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3392 Color: 308
Size: 1436 Color: 242
Size: 84 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3634 Color: 326
Size: 1022 Color: 213
Size: 256 Color: 105

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3724 Color: 330
Size: 996 Color: 210
Size: 192 Color: 82

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3838 Color: 337
Size: 898 Color: 203
Size: 176 Color: 73

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3848 Color: 339
Size: 1016 Color: 212
Size: 48 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3855 Color: 340
Size: 558 Color: 157
Size: 499 Color: 148

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 3868 Color: 341
Size: 888 Color: 201
Size: 156 Color: 61

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 3900 Color: 343
Size: 996 Color: 211
Size: 16 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 3932 Color: 345
Size: 728 Color: 184
Size: 252 Color: 103

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 3946 Color: 347
Size: 658 Color: 171
Size: 308 Color: 117

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 3955 Color: 348
Size: 705 Color: 178
Size: 252 Color: 104

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 3971 Color: 351
Size: 799 Color: 192
Size: 142 Color: 55

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 3974 Color: 352
Size: 798 Color: 191
Size: 140 Color: 54

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4051 Color: 356
Size: 719 Color: 181
Size: 142 Color: 56

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 357
Size: 728 Color: 183
Size: 132 Color: 50

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4056 Color: 358
Size: 568 Color: 162
Size: 288 Color: 113

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4059 Color: 359
Size: 573 Color: 163
Size: 280 Color: 111

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4067 Color: 360
Size: 605 Color: 166
Size: 240 Color: 100

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 361
Size: 708 Color: 179
Size: 136 Color: 52

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4122 Color: 367
Size: 542 Color: 154
Size: 248 Color: 102

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4194 Color: 373
Size: 598 Color: 165
Size: 120 Color: 41

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4209 Color: 375
Size: 541 Color: 153
Size: 162 Color: 67

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4233 Color: 378
Size: 567 Color: 161
Size: 112 Color: 38

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 380
Size: 564 Color: 160
Size: 104 Color: 31

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4260 Color: 383
Size: 440 Color: 140
Size: 212 Color: 86

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4262 Color: 384
Size: 408 Color: 135
Size: 242 Color: 101

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4276 Color: 386
Size: 532 Color: 152
Size: 104 Color: 30

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 4278 Color: 387
Size: 428 Color: 138
Size: 206 Color: 85

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4279 Color: 388
Size: 529 Color: 150
Size: 104 Color: 28

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 389
Size: 426 Color: 137
Size: 194 Color: 83

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4315 Color: 390
Size: 489 Color: 146
Size: 108 Color: 32

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4326 Color: 391
Size: 490 Color: 147
Size: 96 Color: 20

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 393
Size: 486 Color: 145
Size: 96 Color: 17

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4331 Color: 394
Size: 485 Color: 144
Size: 96 Color: 21

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4384 Color: 396
Size: 304 Color: 116
Size: 224 Color: 92

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4392 Color: 398
Size: 296 Color: 115
Size: 224 Color: 94

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4396 Color: 399
Size: 356 Color: 128
Size: 160 Color: 64

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4402 Color: 400
Size: 414 Color: 136
Size: 96 Color: 18

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4404 Color: 401
Size: 352 Color: 127
Size: 156 Color: 59

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4418 Color: 402
Size: 318 Color: 120
Size: 176 Color: 76

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 3449 Color: 312
Size: 1378 Color: 239
Size: 84 Color: 14

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 3460 Color: 313
Size: 1371 Color: 238
Size: 80 Color: 13

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 3613 Color: 324
Size: 1106 Color: 218
Size: 192 Color: 79

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 3690 Color: 328
Size: 1221 Color: 230

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 3704 Color: 329
Size: 1207 Color: 226

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 3963 Color: 350
Size: 820 Color: 195
Size: 128 Color: 43

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 4035 Color: 354
Size: 876 Color: 199

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 4100 Color: 364
Size: 811 Color: 194

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 4126 Color: 369
Size: 785 Color: 189

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 4187 Color: 372
Size: 724 Color: 182

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 4246 Color: 381
Size: 665 Color: 173

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 4327 Color: 392
Size: 584 Color: 164

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 2958 Color: 289
Size: 1952 Color: 264

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 3051 Color: 292
Size: 1751 Color: 258
Size: 108 Color: 33

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 3126 Color: 297
Size: 1680 Color: 254
Size: 104 Color: 27

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 3378 Color: 307
Size: 1532 Color: 246

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 3410 Color: 310
Size: 1500 Color: 244

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 3958 Color: 349
Size: 952 Color: 206

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 3990 Color: 353
Size: 920 Color: 205

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 4038 Color: 355
Size: 872 Color: 198

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 4072 Color: 362
Size: 838 Color: 197

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 4104 Color: 365
Size: 806 Color: 193

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 4179 Color: 371
Size: 731 Color: 186

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 4248 Color: 382
Size: 662 Color: 172

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 4386 Color: 397
Size: 524 Color: 149

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 3262 Color: 303
Size: 1551 Color: 248
Size: 96 Color: 23

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 3753 Color: 333
Size: 1140 Color: 222
Size: 16 Color: 2

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 4141 Color: 370
Size: 768 Color: 187

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 4198 Color: 374
Size: 711 Color: 180

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 4225 Color: 377
Size: 684 Color: 176

Bin 71: 4 of cap free
Amount of items: 7
Items: 
Size: 2460 Color: 275
Size: 544 Color: 155
Size: 530 Color: 151
Size: 472 Color: 143
Size: 464 Color: 142
Size: 322 Color: 123
Size: 116 Color: 40

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 3148 Color: 298
Size: 1656 Color: 253
Size: 104 Color: 26

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 3732 Color: 331
Size: 1160 Color: 223
Size: 16 Color: 0

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 4216 Color: 376
Size: 692 Color: 177

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 4360 Color: 395
Size: 548 Color: 156

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 3063 Color: 293
Size: 1740 Color: 256
Size: 104 Color: 29

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 4084 Color: 363
Size: 823 Color: 196

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 4125 Color: 368
Size: 782 Color: 188

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 3260 Color: 302
Size: 1646 Color: 252

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 3269 Color: 304
Size: 1541 Color: 247
Size: 96 Color: 22

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 3394 Color: 309
Size: 1512 Color: 245

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 3465 Color: 314
Size: 1361 Color: 236
Size: 80 Color: 12

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 3784 Color: 334
Size: 1122 Color: 219

Bin 84: 6 of cap free
Amount of items: 2
Items: 
Size: 3925 Color: 344
Size: 981 Color: 208

Bin 85: 6 of cap free
Amount of items: 2
Items: 
Size: 3939 Color: 346
Size: 967 Color: 207

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 4115 Color: 366
Size: 791 Color: 190

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 4236 Color: 379
Size: 670 Color: 174

Bin 88: 6 of cap free
Amount of items: 2
Items: 
Size: 4263 Color: 385
Size: 643 Color: 168

Bin 89: 7 of cap free
Amount of items: 3
Items: 
Size: 3112 Color: 295
Size: 1136 Color: 221
Size: 657 Color: 170

Bin 90: 7 of cap free
Amount of items: 2
Items: 
Size: 3822 Color: 336
Size: 1083 Color: 217

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 3072 Color: 294
Size: 1832 Color: 260

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 3839 Color: 338
Size: 1065 Color: 215

Bin 93: 8 of cap free
Amount of items: 2
Items: 
Size: 3880 Color: 342
Size: 1024 Color: 214

Bin 94: 10 of cap free
Amount of items: 24
Items: 
Size: 272 Color: 109
Size: 272 Color: 108
Size: 272 Color: 107
Size: 256 Color: 106
Size: 240 Color: 99
Size: 240 Color: 98
Size: 232 Color: 97
Size: 232 Color: 96
Size: 232 Color: 95
Size: 224 Color: 93
Size: 224 Color: 91
Size: 220 Color: 90
Size: 220 Color: 89
Size: 216 Color: 88
Size: 168 Color: 69
Size: 164 Color: 68
Size: 160 Color: 66
Size: 160 Color: 65
Size: 158 Color: 63
Size: 158 Color: 62
Size: 156 Color: 60
Size: 146 Color: 58
Size: 144 Color: 57
Size: 136 Color: 53

Bin 95: 11 of cap free
Amount of items: 5
Items: 
Size: 2476 Color: 278
Size: 1130 Color: 220
Size: 895 Color: 202
Size: 288 Color: 114
Size: 112 Color: 37

Bin 96: 11 of cap free
Amount of items: 2
Items: 
Size: 3635 Color: 327
Size: 1266 Color: 234

Bin 97: 11 of cap free
Amount of items: 2
Items: 
Size: 3737 Color: 332
Size: 1164 Color: 224

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 3618 Color: 325
Size: 1282 Color: 235

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 2852 Color: 285
Size: 2047 Color: 269

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 3116 Color: 296
Size: 1783 Color: 259

Bin 101: 14 of cap free
Amount of items: 2
Items: 
Size: 3279 Color: 305
Size: 1619 Color: 251

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 3816 Color: 335
Size: 1082 Color: 216

Bin 103: 16 of cap free
Amount of items: 2
Items: 
Size: 3516 Color: 316
Size: 1380 Color: 240

Bin 104: 19 of cap free
Amount of items: 3
Items: 
Size: 3196 Color: 300
Size: 1599 Color: 250
Size: 98 Color: 25

Bin 105: 24 of cap free
Amount of items: 2
Items: 
Size: 3484 Color: 315
Size: 1404 Color: 241

Bin 106: 24 of cap free
Amount of items: 3
Items: 
Size: 3586 Color: 321
Size: 1238 Color: 231
Size: 64 Color: 6

Bin 107: 24 of cap free
Amount of items: 3
Items: 
Size: 3602 Color: 323
Size: 1254 Color: 233
Size: 32 Color: 3

Bin 108: 26 of cap free
Amount of items: 3
Items: 
Size: 3228 Color: 301
Size: 1562 Color: 249
Size: 96 Color: 24

Bin 109: 27 of cap free
Amount of items: 3
Items: 
Size: 3599 Color: 322
Size: 1240 Color: 232
Size: 46 Color: 4

Bin 110: 28 of cap free
Amount of items: 3
Items: 
Size: 3432 Color: 311
Size: 1368 Color: 237
Size: 84 Color: 15

Bin 111: 28 of cap free
Amount of items: 4
Items: 
Size: 3528 Color: 317
Size: 1196 Color: 225
Size: 80 Color: 11
Size: 80 Color: 10

Bin 112: 32 of cap free
Amount of items: 3
Items: 
Size: 2728 Color: 282
Size: 2040 Color: 266
Size: 112 Color: 35

Bin 113: 35 of cap free
Amount of items: 5
Items: 
Size: 2472 Color: 277
Size: 881 Color: 200
Size: 730 Color: 185
Size: 680 Color: 175
Size: 114 Color: 39

Bin 114: 36 of cap free
Amount of items: 2
Items: 
Size: 3178 Color: 299
Size: 1698 Color: 255

Bin 115: 46 of cap free
Amount of items: 9
Items: 
Size: 2458 Color: 273
Size: 400 Color: 131
Size: 372 Color: 130
Size: 372 Color: 129
Size: 344 Color: 126
Size: 336 Color: 125
Size: 328 Color: 124
Size: 128 Color: 46
Size: 128 Color: 45

Bin 116: 48 of cap free
Amount of items: 2
Items: 
Size: 2820 Color: 284
Size: 2044 Color: 268

Bin 117: 55 of cap free
Amount of items: 2
Items: 
Size: 2971 Color: 290
Size: 1886 Color: 263

Bin 118: 58 of cap free
Amount of items: 5
Items: 
Size: 2462 Color: 276
Size: 657 Color: 169
Size: 611 Color: 167
Size: 564 Color: 159
Size: 560 Color: 158

Bin 119: 59 of cap free
Amount of items: 3
Items: 
Size: 2955 Color: 288
Size: 988 Color: 209
Size: 910 Color: 204

Bin 120: 61 of cap free
Amount of items: 3
Items: 
Size: 2995 Color: 291
Size: 1748 Color: 257
Size: 108 Color: 34

Bin 121: 64 of cap free
Amount of items: 3
Items: 
Size: 3570 Color: 320
Size: 1214 Color: 229
Size: 64 Color: 7

Bin 122: 70 of cap free
Amount of items: 3
Items: 
Size: 3558 Color: 319
Size: 1212 Color: 228
Size: 72 Color: 8

Bin 123: 74 of cap free
Amount of items: 3
Items: 
Size: 2690 Color: 281
Size: 2036 Color: 265
Size: 112 Color: 36

Bin 124: 75 of cap free
Amount of items: 3
Items: 
Size: 3548 Color: 318
Size: 1209 Color: 227
Size: 80 Color: 9

Bin 125: 82 of cap free
Amount of items: 3
Items: 
Size: 3288 Color: 306
Size: 1446 Color: 243
Size: 96 Color: 19

Bin 126: 87 of cap free
Amount of items: 2
Items: 
Size: 2675 Color: 280
Size: 2150 Color: 271

Bin 127: 93 of cap free
Amount of items: 2
Items: 
Size: 2674 Color: 279
Size: 2145 Color: 270

Bin 128: 97 of cap free
Amount of items: 2
Items: 
Size: 2773 Color: 283
Size: 2042 Color: 267

Bin 129: 108 of cap free
Amount of items: 2
Items: 
Size: 2938 Color: 287
Size: 1866 Color: 262

Bin 130: 109 of cap free
Amount of items: 8
Items: 
Size: 2459 Color: 274
Size: 442 Color: 141
Size: 436 Color: 139
Size: 408 Color: 134
Size: 408 Color: 133
Size: 400 Color: 132
Size: 128 Color: 44
Size: 122 Color: 42

Bin 131: 111 of cap free
Amount of items: 2
Items: 
Size: 2936 Color: 286
Size: 1865 Color: 261

Bin 132: 123 of cap free
Amount of items: 11
Items: 
Size: 2457 Color: 272
Size: 320 Color: 122
Size: 320 Color: 121
Size: 310 Color: 119
Size: 308 Color: 118
Size: 280 Color: 112
Size: 272 Color: 110
Size: 136 Color: 51
Size: 130 Color: 49
Size: 128 Color: 48
Size: 128 Color: 47

Bin 133: 2878 of cap free
Amount of items: 11
Items: 
Size: 212 Color: 87
Size: 200 Color: 84
Size: 192 Color: 81
Size: 192 Color: 80
Size: 180 Color: 78
Size: 178 Color: 77
Size: 176 Color: 75
Size: 176 Color: 74
Size: 176 Color: 72
Size: 176 Color: 71
Size: 176 Color: 70

Total size: 648384
Total free space: 4912

