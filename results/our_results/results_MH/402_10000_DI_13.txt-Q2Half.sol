Capicity Bin: 8224
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 5168 Color: 1
Size: 3056 Color: 0

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 2768 Color: 1
Size: 1712 Color: 1
Size: 1280 Color: 1
Size: 928 Color: 1
Size: 912 Color: 1
Size: 256 Color: 0
Size: 192 Color: 0
Size: 160 Color: 0
Size: 16 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 1
Size: 1140 Color: 1
Size: 224 Color: 0

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 5680 Color: 1
Size: 1904 Color: 1
Size: 304 Color: 0
Size: 256 Color: 0
Size: 80 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5706 Color: 1
Size: 2406 Color: 1
Size: 112 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4124 Color: 1
Size: 3420 Color: 1
Size: 680 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 1
Size: 898 Color: 1
Size: 176 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7300 Color: 1
Size: 772 Color: 1
Size: 152 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 1
Size: 1034 Color: 1
Size: 204 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7380 Color: 1
Size: 708 Color: 1
Size: 136 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 1
Size: 2732 Color: 1
Size: 544 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 1
Size: 1570 Color: 1
Size: 312 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7126 Color: 1
Size: 918 Color: 1
Size: 180 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6977 Color: 1
Size: 1041 Color: 1
Size: 206 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 1
Size: 1180 Color: 1
Size: 232 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7176 Color: 1
Size: 888 Color: 1
Size: 160 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6312 Color: 1
Size: 1608 Color: 1
Size: 304 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5059 Color: 1
Size: 2639 Color: 1
Size: 526 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6540 Color: 1
Size: 1404 Color: 1
Size: 280 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7143 Color: 1
Size: 901 Color: 1
Size: 180 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 1
Size: 1335 Color: 1
Size: 266 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4568 Color: 1
Size: 3240 Color: 1
Size: 416 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5112 Color: 1
Size: 2600 Color: 1
Size: 512 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4572 Color: 1
Size: 3044 Color: 1
Size: 608 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5736 Color: 1
Size: 2088 Color: 1
Size: 400 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7002 Color: 1
Size: 1022 Color: 1
Size: 200 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7400 Color: 1
Size: 696 Color: 1
Size: 128 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7384 Color: 1
Size: 712 Color: 1
Size: 128 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6489 Color: 1
Size: 1447 Color: 1
Size: 288 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4690 Color: 1
Size: 2946 Color: 1
Size: 588 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 1
Size: 1175 Color: 1
Size: 232 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 1
Size: 1242 Color: 1
Size: 244 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 1
Size: 964 Color: 1
Size: 184 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 1
Size: 1131 Color: 1
Size: 226 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4122 Color: 1
Size: 3422 Color: 1
Size: 680 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 1
Size: 868 Color: 1
Size: 168 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 1
Size: 1860 Color: 1
Size: 368 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 1
Size: 902 Color: 1
Size: 180 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7092 Color: 1
Size: 948 Color: 1
Size: 184 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5723 Color: 1
Size: 2085 Color: 1
Size: 416 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 1
Size: 1800 Color: 1
Size: 352 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6046 Color: 1
Size: 1818 Color: 1
Size: 360 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6725 Color: 1
Size: 1251 Color: 1
Size: 248 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 7168 Color: 1
Size: 880 Color: 1
Size: 176 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6165 Color: 1
Size: 1753 Color: 1
Size: 306 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 1
Size: 2106 Color: 1
Size: 420 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6994 Color: 1
Size: 1026 Color: 1
Size: 204 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 1
Size: 814 Color: 1
Size: 160 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 7135 Color: 1
Size: 909 Color: 1
Size: 180 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 1
Size: 2114 Color: 1
Size: 420 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 1
Size: 1044 Color: 1
Size: 208 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7112 Color: 1
Size: 936 Color: 1
Size: 176 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6106 Color: 1
Size: 1774 Color: 1
Size: 344 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 7107 Color: 1
Size: 931 Color: 1
Size: 186 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6472 Color: 1
Size: 1464 Color: 1
Size: 288 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6776 Color: 1
Size: 1208 Color: 1
Size: 240 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6490 Color: 1
Size: 1446 Color: 1
Size: 288 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 4113 Color: 1
Size: 3427 Color: 1
Size: 684 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 1
Size: 2264 Color: 1
Size: 448 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 7207 Color: 1
Size: 849 Color: 1
Size: 168 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 4116 Color: 1
Size: 3988 Color: 1
Size: 120 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 1
Size: 1662 Color: 1
Size: 228 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7134 Color: 1
Size: 910 Color: 1
Size: 180 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 1
Size: 932 Color: 1
Size: 184 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 1
Size: 1748 Color: 1
Size: 344 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 1
Size: 1042 Color: 1
Size: 204 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 4114 Color: 1
Size: 3458 Color: 1
Size: 652 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 1
Size: 1612 Color: 1
Size: 320 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5400 Color: 1
Size: 2568 Color: 1
Size: 256 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6631 Color: 1
Size: 1329 Color: 1
Size: 264 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5516 Color: 1
Size: 2260 Color: 1
Size: 448 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7032 Color: 1
Size: 1000 Color: 1
Size: 192 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 1
Size: 1500 Color: 1
Size: 296 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5274 Color: 1
Size: 2462 Color: 1
Size: 488 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 7115 Color: 1
Size: 925 Color: 1
Size: 184 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4115 Color: 1
Size: 3425 Color: 1
Size: 684 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6932 Color: 1
Size: 1084 Color: 1
Size: 208 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 1
Size: 1128 Color: 1
Size: 224 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6818 Color: 1
Size: 1174 Color: 1
Size: 232 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5770 Color: 1
Size: 2046 Color: 1
Size: 408 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 1
Size: 1064 Color: 1
Size: 208 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6786 Color: 1
Size: 1202 Color: 1
Size: 236 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 1
Size: 1300 Color: 1
Size: 256 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6349 Color: 1
Size: 1563 Color: 1
Size: 312 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6299 Color: 1
Size: 1605 Color: 1
Size: 320 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6717 Color: 1
Size: 1257 Color: 1
Size: 250 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 1
Size: 1012 Color: 1
Size: 8 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 7304 Color: 1
Size: 776 Color: 1
Size: 144 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4730 Color: 1
Size: 2914 Color: 1
Size: 580 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6636 Color: 1
Size: 1324 Color: 1
Size: 264 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 1
Size: 2044 Color: 1
Size: 400 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5210 Color: 1
Size: 2514 Color: 1
Size: 500 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 4120 Color: 1
Size: 3432 Color: 1
Size: 672 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 1
Size: 2631 Color: 1
Size: 526 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6582 Color: 1
Size: 1370 Color: 1
Size: 272 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 1
Size: 1528 Color: 1
Size: 304 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 5650 Color: 1
Size: 2146 Color: 1
Size: 428 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6875 Color: 1
Size: 1125 Color: 1
Size: 224 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 6993 Color: 1
Size: 1027 Color: 1
Size: 204 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 7027 Color: 1
Size: 999 Color: 1
Size: 198 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6632 Color: 1
Size: 1416 Color: 1
Size: 176 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 1
Size: 810 Color: 1
Size: 160 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 5912 Color: 1
Size: 1928 Color: 1
Size: 384 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 4760 Color: 1
Size: 2888 Color: 1
Size: 576 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 1
Size: 1033 Color: 1
Size: 206 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 7193 Color: 1
Size: 861 Color: 1
Size: 170 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 5266 Color: 1
Size: 2466 Color: 1
Size: 492 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 1
Size: 2476 Color: 1
Size: 488 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 1
Size: 1186 Color: 1
Size: 236 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 5250 Color: 1
Size: 2482 Color: 1
Size: 492 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5421 Color: 1
Size: 2337 Color: 1
Size: 466 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 1
Size: 1927 Color: 1
Size: 124 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 7338 Color: 1
Size: 742 Color: 1
Size: 144 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 7342 Color: 1
Size: 738 Color: 1
Size: 144 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4738 Color: 1
Size: 2906 Color: 1
Size: 580 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4641 Color: 1
Size: 2987 Color: 1
Size: 596 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5075 Color: 1
Size: 2625 Color: 1
Size: 524 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 5429 Color: 1
Size: 2331 Color: 1
Size: 464 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 4746 Color: 1
Size: 2902 Color: 1
Size: 576 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 4625 Color: 1
Size: 3001 Color: 1
Size: 598 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4633 Color: 1
Size: 2993 Color: 1
Size: 598 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 4654 Color: 1
Size: 2978 Color: 1
Size: 592 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5258 Color: 1
Size: 2474 Color: 1
Size: 492 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 5715 Color: 1
Size: 2091 Color: 1
Size: 418 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 5961 Color: 1
Size: 1887 Color: 1
Size: 376 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5967 Color: 1
Size: 1881 Color: 1
Size: 376 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 1
Size: 1703 Color: 1
Size: 340 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6503 Color: 1
Size: 1435 Color: 1
Size: 286 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 6511 Color: 1
Size: 1429 Color: 1
Size: 284 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6706 Color: 1
Size: 1266 Color: 1
Size: 252 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6803 Color: 1
Size: 1185 Color: 1
Size: 236 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 1
Size: 915 Color: 1
Size: 182 Color: 0

Total size: 1085568
Total free space: 0

