Capicity Bin: 1001
Lower Bound: 46

Bins used: 46
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 313 Color: 11
Size: 261 Color: 11

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 1
Size: 253 Color: 7

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 15
Size: 409 Color: 9

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 8
Size: 380 Color: 7

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 8
Size: 285 Color: 3

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 2
Size: 306 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 13
Size: 185 Color: 9
Size: 107 Color: 4

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 5
Size: 424 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 11
Size: 154 Color: 11
Size: 144 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 12
Size: 302 Color: 16
Size: 276 Color: 0

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 2
Size: 434 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 339 Color: 19
Size: 302 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 6
Size: 123 Color: 10
Size: 118 Color: 4

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 483 Color: 9
Size: 305 Color: 5
Size: 112 Color: 18
Size: 101 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 539 Color: 8
Size: 317 Color: 10
Size: 145 Color: 11

Bin 16: 1 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 10
Size: 436 Color: 7

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 17
Size: 475 Color: 16

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 342 Color: 12
Size: 266 Color: 3

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 5
Size: 338 Color: 4
Size: 257 Color: 8

Bin 20: 2 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 14
Size: 468 Color: 0

Bin 21: 2 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 18
Size: 308 Color: 4

Bin 22: 2 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 12
Size: 355 Color: 8

Bin 23: 2 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 5
Size: 430 Color: 11

Bin 24: 3 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 17
Size: 440 Color: 9

Bin 25: 3 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 1
Size: 450 Color: 18

Bin 26: 5 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 325 Color: 17

Bin 27: 5 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 2
Size: 380 Color: 18

Bin 28: 6 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 0
Size: 412 Color: 14

Bin 29: 6 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 3
Size: 277 Color: 19

Bin 30: 7 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 18
Size: 491 Color: 10

Bin 31: 8 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 0
Size: 392 Color: 9

Bin 32: 9 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 9
Size: 410 Color: 6

Bin 33: 9 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 0
Size: 490 Color: 19

Bin 34: 10 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 13
Size: 229 Color: 8

Bin 35: 11 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 19
Size: 391 Color: 8

Bin 36: 23 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 16
Size: 340 Color: 12

Bin 37: 24 of cap free
Amount of items: 2
Items: 
Size: 489 Color: 6
Size: 488 Color: 1

Bin 38: 27 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 16
Size: 407 Color: 12

Bin 39: 30 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 16
Size: 359 Color: 10

Bin 40: 33 of cap free
Amount of items: 2
Items: 
Size: 483 Color: 3
Size: 485 Color: 11

Bin 41: 36 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 12
Size: 351 Color: 14

Bin 42: 44 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 3
Size: 174 Color: 8

Bin 43: 63 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 8
Size: 176 Color: 19

Bin 44: 70 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 8
Size: 131 Color: 15

Bin 45: 92 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 16
Size: 128 Color: 1

Bin 46: 249 of cap free
Amount of items: 1
Items: 
Size: 752 Color: 4

Total size: 45261
Total free space: 785

