Capicity Bin: 6528
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3268 Color: 11
Size: 724 Color: 4
Size: 686 Color: 7
Size: 644 Color: 19
Size: 634 Color: 6
Size: 336 Color: 15
Size: 236 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3274 Color: 11
Size: 2714 Color: 14
Size: 540 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3284 Color: 19
Size: 2708 Color: 8
Size: 536 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3624 Color: 13
Size: 2568 Color: 16
Size: 336 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4026 Color: 13
Size: 2086 Color: 19
Size: 416 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4232 Color: 13
Size: 2172 Color: 12
Size: 124 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 6
Size: 1772 Color: 12
Size: 416 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4424 Color: 17
Size: 1928 Color: 0
Size: 176 Color: 19

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4478 Color: 1
Size: 1854 Color: 2
Size: 196 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4584 Color: 19
Size: 1768 Color: 3
Size: 176 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 13
Size: 1564 Color: 11
Size: 304 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4835 Color: 11
Size: 1103 Color: 1
Size: 590 Color: 18

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4831 Color: 6
Size: 1241 Color: 10
Size: 456 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4902 Color: 12
Size: 1526 Color: 3
Size: 100 Color: 16

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4942 Color: 12
Size: 1382 Color: 1
Size: 204 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4968 Color: 9
Size: 1192 Color: 8
Size: 368 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5048 Color: 6
Size: 1232 Color: 18
Size: 248 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5012 Color: 13
Size: 988 Color: 13
Size: 528 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5035 Color: 17
Size: 1021 Color: 8
Size: 472 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5050 Color: 6
Size: 1234 Color: 17
Size: 244 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5098 Color: 10
Size: 888 Color: 3
Size: 542 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5112 Color: 16
Size: 1304 Color: 6
Size: 112 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5178 Color: 4
Size: 1110 Color: 17
Size: 240 Color: 10

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 10
Size: 1121 Color: 14
Size: 224 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5204 Color: 6
Size: 1108 Color: 4
Size: 216 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5240 Color: 5
Size: 720 Color: 0
Size: 568 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5246 Color: 18
Size: 1018 Color: 9
Size: 264 Color: 8

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5252 Color: 11
Size: 1240 Color: 2
Size: 36 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5316 Color: 5
Size: 1012 Color: 7
Size: 200 Color: 11

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5342 Color: 11
Size: 930 Color: 6
Size: 256 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5368 Color: 10
Size: 1080 Color: 0
Size: 80 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5414 Color: 4
Size: 984 Color: 19
Size: 130 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5464 Color: 14
Size: 884 Color: 5
Size: 180 Color: 19

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 3
Size: 1008 Color: 9
Size: 52 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 4
Size: 584 Color: 12
Size: 420 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 0
Size: 820 Color: 12
Size: 160 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 3
Size: 734 Color: 7
Size: 240 Color: 11

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5578 Color: 14
Size: 814 Color: 15
Size: 136 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5610 Color: 6
Size: 542 Color: 12
Size: 376 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5636 Color: 13
Size: 728 Color: 15
Size: 164 Color: 19

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 19
Size: 730 Color: 15
Size: 144 Color: 9

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5656 Color: 17
Size: 760 Color: 18
Size: 112 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5710 Color: 18
Size: 738 Color: 4
Size: 80 Color: 9

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5720 Color: 11
Size: 728 Color: 6
Size: 80 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 16
Size: 540 Color: 11
Size: 224 Color: 16

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5788 Color: 6
Size: 540 Color: 12
Size: 200 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5832 Color: 18
Size: 536 Color: 15
Size: 160 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 1
Size: 542 Color: 16
Size: 152 Color: 19

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 12
Size: 480 Color: 3
Size: 212 Color: 14

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5848 Color: 15
Size: 668 Color: 0
Size: 12 Color: 6

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 3994 Color: 5
Size: 2285 Color: 17
Size: 248 Color: 2

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 4200 Color: 13
Size: 2051 Color: 17
Size: 276 Color: 12

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 4308 Color: 4
Size: 1771 Color: 15
Size: 448 Color: 15

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 7
Size: 1881 Color: 14
Size: 372 Color: 9

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 4306 Color: 19
Size: 1797 Color: 8
Size: 424 Color: 2

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 5009 Color: 1
Size: 1518 Color: 9

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 5157 Color: 13
Size: 1258 Color: 5
Size: 112 Color: 16

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 5205 Color: 16
Size: 1322 Color: 14

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 3690 Color: 6
Size: 2724 Color: 14
Size: 112 Color: 10

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 4710 Color: 17
Size: 1576 Color: 19
Size: 240 Color: 2

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 4742 Color: 3
Size: 1784 Color: 15

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 4876 Color: 2
Size: 1566 Color: 4
Size: 84 Color: 0

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 5442 Color: 12
Size: 968 Color: 6
Size: 116 Color: 5

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 5624 Color: 0
Size: 902 Color: 19

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 5732 Color: 0
Size: 794 Color: 14

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 5756 Color: 19
Size: 770 Color: 11

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 5806 Color: 19
Size: 720 Color: 12

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 3332 Color: 16
Size: 2717 Color: 0
Size: 476 Color: 7

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 4405 Color: 2
Size: 2120 Color: 11

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 5284 Color: 5
Size: 1241 Color: 7

Bin 71: 4 of cap free
Amount of items: 5
Items: 
Size: 3282 Color: 4
Size: 2706 Color: 16
Size: 272 Color: 13
Size: 152 Color: 19
Size: 112 Color: 12

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 3992 Color: 16
Size: 2308 Color: 18
Size: 224 Color: 0

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 4404 Color: 3
Size: 2120 Color: 8

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 5060 Color: 14
Size: 1464 Color: 0

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 5352 Color: 7
Size: 1140 Color: 15
Size: 32 Color: 13

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 5690 Color: 16
Size: 834 Color: 13

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 5812 Color: 9
Size: 712 Color: 16

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 5822 Color: 19
Size: 702 Color: 9

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 3272 Color: 5
Size: 2711 Color: 3
Size: 540 Color: 4

Bin 80: 5 of cap free
Amount of items: 3
Items: 
Size: 3658 Color: 2
Size: 2721 Color: 4
Size: 144 Color: 16

Bin 81: 5 of cap free
Amount of items: 3
Items: 
Size: 5039 Color: 10
Size: 1420 Color: 1
Size: 64 Color: 3

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 3300 Color: 1
Size: 2640 Color: 17
Size: 582 Color: 9

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 4373 Color: 10
Size: 2149 Color: 12

Bin 84: 6 of cap free
Amount of items: 3
Items: 
Size: 4678 Color: 5
Size: 1504 Color: 18
Size: 340 Color: 4

Bin 85: 6 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 3
Size: 1126 Color: 10
Size: 48 Color: 0

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 3273 Color: 6
Size: 2668 Color: 6
Size: 580 Color: 16

Bin 87: 7 of cap free
Amount of items: 3
Items: 
Size: 4069 Color: 14
Size: 2268 Color: 4
Size: 184 Color: 7

Bin 88: 7 of cap free
Amount of items: 2
Items: 
Size: 4867 Color: 19
Size: 1654 Color: 16

Bin 89: 8 of cap free
Amount of items: 4
Items: 
Size: 3269 Color: 12
Size: 1380 Color: 0
Size: 1267 Color: 3
Size: 604 Color: 6

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 4096 Color: 14
Size: 2424 Color: 16

Bin 91: 8 of cap free
Amount of items: 3
Items: 
Size: 5444 Color: 0
Size: 1044 Color: 15
Size: 32 Color: 5

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 5450 Color: 8
Size: 1070 Color: 15

Bin 93: 8 of cap free
Amount of items: 2
Items: 
Size: 5530 Color: 11
Size: 990 Color: 0

Bin 94: 9 of cap free
Amount of items: 3
Items: 
Size: 4648 Color: 0
Size: 1415 Color: 2
Size: 456 Color: 10

Bin 95: 10 of cap free
Amount of items: 2
Items: 
Size: 4124 Color: 14
Size: 2394 Color: 17

Bin 96: 10 of cap free
Amount of items: 2
Items: 
Size: 5770 Color: 17
Size: 748 Color: 2

Bin 97: 11 of cap free
Amount of items: 11
Items: 
Size: 3265 Color: 11
Size: 468 Color: 19
Size: 368 Color: 1
Size: 360 Color: 8
Size: 358 Color: 17
Size: 358 Color: 2
Size: 352 Color: 12
Size: 352 Color: 7
Size: 300 Color: 15
Size: 208 Color: 13
Size: 128 Color: 13

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 4512 Color: 5
Size: 2004 Color: 8

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 4164 Color: 2
Size: 2351 Color: 6

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 5130 Color: 14
Size: 1385 Color: 0

Bin 101: 14 of cap free
Amount of items: 28
Items: 
Size: 320 Color: 2
Size: 316 Color: 18
Size: 312 Color: 14
Size: 312 Color: 5
Size: 304 Color: 2
Size: 296 Color: 10
Size: 288 Color: 11
Size: 282 Color: 16
Size: 280 Color: 7
Size: 248 Color: 11
Size: 240 Color: 3
Size: 224 Color: 6
Size: 224 Color: 0
Size: 220 Color: 12
Size: 220 Color: 9
Size: 220 Color: 8
Size: 208 Color: 16
Size: 200 Color: 18
Size: 196 Color: 14
Size: 192 Color: 13
Size: 192 Color: 10
Size: 192 Color: 7
Size: 192 Color: 1
Size: 176 Color: 19
Size: 176 Color: 17
Size: 164 Color: 17
Size: 160 Color: 15
Size: 160 Color: 15

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 3812 Color: 5
Size: 2702 Color: 4

Bin 103: 14 of cap free
Amount of items: 2
Items: 
Size: 5606 Color: 11
Size: 908 Color: 13

Bin 104: 14 of cap free
Amount of items: 2
Items: 
Size: 5688 Color: 1
Size: 826 Color: 0

Bin 105: 14 of cap free
Amount of items: 2
Items: 
Size: 5722 Color: 15
Size: 792 Color: 10

Bin 106: 18 of cap free
Amount of items: 2
Items: 
Size: 4828 Color: 6
Size: 1682 Color: 1

Bin 107: 20 of cap free
Amount of items: 2
Items: 
Size: 5164 Color: 11
Size: 1344 Color: 8

Bin 108: 21 of cap free
Amount of items: 4
Items: 
Size: 3277 Color: 4
Size: 1567 Color: 8
Size: 1411 Color: 14
Size: 252 Color: 13

Bin 109: 21 of cap free
Amount of items: 3
Items: 
Size: 3290 Color: 6
Size: 2713 Color: 11
Size: 504 Color: 7

Bin 110: 21 of cap free
Amount of items: 2
Items: 
Size: 4369 Color: 16
Size: 2138 Color: 8

Bin 111: 22 of cap free
Amount of items: 2
Items: 
Size: 5310 Color: 9
Size: 1196 Color: 4

Bin 112: 23 of cap free
Amount of items: 3
Items: 
Size: 3787 Color: 3
Size: 1490 Color: 11
Size: 1228 Color: 12

Bin 113: 24 of cap free
Amount of items: 2
Items: 
Size: 5660 Color: 16
Size: 844 Color: 5

Bin 114: 24 of cap free
Amount of items: 2
Items: 
Size: 5738 Color: 4
Size: 766 Color: 8

Bin 115: 27 of cap free
Amount of items: 2
Items: 
Size: 4135 Color: 18
Size: 2366 Color: 6

Bin 116: 27 of cap free
Amount of items: 2
Items: 
Size: 5072 Color: 10
Size: 1429 Color: 13

Bin 117: 30 of cap free
Amount of items: 2
Items: 
Size: 5592 Color: 16
Size: 906 Color: 1

Bin 118: 31 of cap free
Amount of items: 2
Items: 
Size: 5303 Color: 17
Size: 1194 Color: 19

Bin 119: 36 of cap free
Amount of items: 2
Items: 
Size: 3764 Color: 2
Size: 2728 Color: 8

Bin 120: 38 of cap free
Amount of items: 7
Items: 
Size: 3266 Color: 4
Size: 644 Color: 19
Size: 620 Color: 0
Size: 536 Color: 4
Size: 528 Color: 3
Size: 496 Color: 7
Size: 400 Color: 5

Bin 121: 40 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 12
Size: 2114 Color: 2
Size: 408 Color: 18

Bin 122: 40 of cap free
Amount of items: 3
Items: 
Size: 4623 Color: 6
Size: 1589 Color: 18
Size: 276 Color: 4

Bin 123: 42 of cap free
Amount of items: 2
Items: 
Size: 4604 Color: 2
Size: 1882 Color: 1

Bin 124: 42 of cap free
Amount of items: 2
Items: 
Size: 4776 Color: 10
Size: 1710 Color: 17

Bin 125: 46 of cap free
Amount of items: 2
Items: 
Size: 4510 Color: 19
Size: 1972 Color: 10

Bin 126: 49 of cap free
Amount of items: 2
Items: 
Size: 4271 Color: 9
Size: 2208 Color: 6

Bin 127: 50 of cap free
Amount of items: 2
Items: 
Size: 4650 Color: 13
Size: 1828 Color: 6

Bin 128: 54 of cap free
Amount of items: 2
Items: 
Size: 4870 Color: 16
Size: 1604 Color: 7

Bin 129: 60 of cap free
Amount of items: 3
Items: 
Size: 3464 Color: 4
Size: 2722 Color: 12
Size: 282 Color: 19

Bin 130: 62 of cap free
Amount of items: 2
Items: 
Size: 5198 Color: 9
Size: 1268 Color: 7

Bin 131: 78 of cap free
Amount of items: 2
Items: 
Size: 4649 Color: 18
Size: 1801 Color: 5

Bin 132: 95 of cap free
Amount of items: 2
Items: 
Size: 3709 Color: 10
Size: 2724 Color: 16

Bin 133: 5172 of cap free
Amount of items: 10
Items: 
Size: 160 Color: 18
Size: 144 Color: 14
Size: 144 Color: 13
Size: 144 Color: 3
Size: 144 Color: 1
Size: 128 Color: 19
Size: 128 Color: 15
Size: 124 Color: 12
Size: 120 Color: 11
Size: 120 Color: 2

Total size: 861696
Total free space: 6528

