Capicity Bin: 1996
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 804 Color: 15
Size: 478 Color: 11
Size: 354 Color: 0
Size: 220 Color: 7
Size: 76 Color: 19
Size: 64 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 2
Size: 446 Color: 19
Size: 88 Color: 10

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1380 Color: 15
Size: 460 Color: 14
Size: 108 Color: 16
Size: 38 Color: 4
Size: 10 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 13
Size: 663 Color: 1
Size: 132 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 6
Size: 294 Color: 12
Size: 48 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 12
Size: 778 Color: 14
Size: 152 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 4
Size: 198 Color: 3
Size: 36 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1198 Color: 18
Size: 666 Color: 19
Size: 132 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1255 Color: 16
Size: 619 Color: 5
Size: 122 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 9
Size: 178 Color: 11
Size: 32 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1003 Color: 19
Size: 829 Color: 13
Size: 164 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1259 Color: 2
Size: 615 Color: 4
Size: 122 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1695 Color: 1
Size: 251 Color: 3
Size: 50 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 8
Size: 734 Color: 8
Size: 144 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 6
Size: 330 Color: 17
Size: 92 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 3
Size: 205 Color: 6
Size: 40 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 8
Size: 550 Color: 0
Size: 56 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 0
Size: 397 Color: 19
Size: 78 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 1
Size: 511 Color: 11
Size: 102 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 14
Size: 449 Color: 12
Size: 88 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 2
Size: 190 Color: 18
Size: 36 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 16
Size: 182 Color: 2
Size: 36 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 16
Size: 281 Color: 0
Size: 56 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 4
Size: 589 Color: 17
Size: 116 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 18
Size: 340 Color: 2
Size: 68 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 0
Size: 426 Color: 19
Size: 84 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 3
Size: 322 Color: 4
Size: 72 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 10
Size: 515 Color: 16
Size: 102 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 19
Size: 303 Color: 11
Size: 60 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1141 Color: 7
Size: 713 Color: 14
Size: 142 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 4
Size: 410 Color: 5
Size: 64 Color: 5

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1145 Color: 16
Size: 767 Color: 19
Size: 84 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 13
Size: 591 Color: 13
Size: 118 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1162 Color: 5
Size: 698 Color: 19
Size: 136 Color: 9

Bin 35: 0 of cap free
Amount of items: 4
Items: 
Size: 1617 Color: 0
Size: 351 Color: 11
Size: 20 Color: 3
Size: 8 Color: 15

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1533 Color: 1
Size: 387 Color: 6
Size: 76 Color: 9

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 999 Color: 10
Size: 831 Color: 0
Size: 166 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 2
Size: 451 Color: 12
Size: 88 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 14
Size: 239 Color: 10
Size: 46 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 4
Size: 197 Color: 13
Size: 56 Color: 7

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1252 Color: 3
Size: 676 Color: 17
Size: 68 Color: 7

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 16
Size: 318 Color: 9
Size: 60 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1607 Color: 14
Size: 325 Color: 14
Size: 64 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1610 Color: 13
Size: 286 Color: 9
Size: 100 Color: 6

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1387 Color: 1
Size: 509 Color: 1
Size: 100 Color: 0

Bin 46: 0 of cap free
Amount of items: 4
Items: 
Size: 1761 Color: 10
Size: 211 Color: 6
Size: 16 Color: 14
Size: 8 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 16
Size: 401 Color: 0
Size: 78 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 3
Size: 506 Color: 4
Size: 64 Color: 11

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 7
Size: 610 Color: 2
Size: 120 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 19
Size: 366 Color: 3
Size: 72 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 15
Size: 262 Color: 11
Size: 52 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 4
Size: 369 Color: 10
Size: 52 Color: 6

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 1
Size: 308 Color: 12
Size: 42 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1002 Color: 10
Size: 830 Color: 12
Size: 164 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 8
Size: 263 Color: 7
Size: 52 Color: 15

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 10
Size: 220 Color: 3
Size: 70 Color: 5

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1525 Color: 7
Size: 393 Color: 6
Size: 78 Color: 12

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 11
Size: 390 Color: 5
Size: 76 Color: 11

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1346 Color: 12
Size: 542 Color: 2
Size: 108 Color: 5

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 3
Size: 186 Color: 4
Size: 36 Color: 16

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 15
Size: 416 Color: 16
Size: 242 Color: 17

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 11
Size: 230 Color: 17
Size: 44 Color: 8

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1137 Color: 19
Size: 717 Color: 14
Size: 142 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 1
Size: 246 Color: 2
Size: 36 Color: 13

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 4
Size: 189 Color: 18
Size: 36 Color: 2

Total size: 129740
Total free space: 0

