Capicity Bin: 5392
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 2700 Color: 15
Size: 1385 Color: 9
Size: 877 Color: 5
Size: 334 Color: 1
Size: 96 Color: 2

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 2698 Color: 5
Size: 1702 Color: 0
Size: 638 Color: 12
Size: 224 Color: 12
Size: 130 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3034 Color: 4
Size: 2244 Color: 17
Size: 114 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3224 Color: 7
Size: 1816 Color: 4
Size: 352 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3376 Color: 18
Size: 1844 Color: 7
Size: 172 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3580 Color: 2
Size: 1420 Color: 10
Size: 392 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 3656 Color: 11
Size: 1448 Color: 3
Size: 288 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 3712 Color: 15
Size: 1436 Color: 0
Size: 244 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 3784 Color: 2
Size: 1448 Color: 17
Size: 160 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 3804 Color: 12
Size: 1484 Color: 16
Size: 104 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 3834 Color: 4
Size: 1458 Color: 2
Size: 100 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 3921 Color: 5
Size: 1085 Color: 9
Size: 386 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 3927 Color: 15
Size: 873 Color: 0
Size: 592 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 3988 Color: 12
Size: 1144 Color: 5
Size: 260 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4038 Color: 4
Size: 760 Color: 18
Size: 594 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4006 Color: 13
Size: 1130 Color: 5
Size: 256 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4014 Color: 3
Size: 1150 Color: 1
Size: 228 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 4
Size: 1288 Color: 13
Size: 52 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4024 Color: 5
Size: 1248 Color: 9
Size: 120 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4085 Color: 13
Size: 791 Color: 10
Size: 516 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4172 Color: 11
Size: 924 Color: 15
Size: 296 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4182 Color: 12
Size: 1146 Color: 15
Size: 64 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4221 Color: 14
Size: 723 Color: 9
Size: 448 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4229 Color: 3
Size: 649 Color: 14
Size: 514 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 13
Size: 936 Color: 19
Size: 140 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4318 Color: 18
Size: 794 Color: 16
Size: 280 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4362 Color: 7
Size: 862 Color: 9
Size: 168 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 4376 Color: 2
Size: 522 Color: 4
Size: 494 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4443 Color: 16
Size: 609 Color: 2
Size: 340 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4506 Color: 16
Size: 470 Color: 5
Size: 416 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4518 Color: 3
Size: 742 Color: 9
Size: 132 Color: 11

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 2
Size: 454 Color: 15
Size: 386 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4556 Color: 0
Size: 652 Color: 17
Size: 184 Color: 13

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4588 Color: 9
Size: 608 Color: 19
Size: 196 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4589 Color: 6
Size: 655 Color: 13
Size: 148 Color: 11

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4603 Color: 2
Size: 659 Color: 6
Size: 130 Color: 11

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4630 Color: 15
Size: 568 Color: 3
Size: 194 Color: 10

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4663 Color: 9
Size: 571 Color: 19
Size: 158 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4668 Color: 18
Size: 492 Color: 18
Size: 232 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4680 Color: 0
Size: 600 Color: 16
Size: 112 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4703 Color: 10
Size: 575 Color: 4
Size: 114 Color: 14

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4707 Color: 14
Size: 565 Color: 1
Size: 120 Color: 11

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 4712 Color: 8
Size: 396 Color: 9
Size: 284 Color: 14

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 4744 Color: 19
Size: 496 Color: 10
Size: 152 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 16
Size: 368 Color: 6
Size: 276 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 4770 Color: 5
Size: 432 Color: 19
Size: 190 Color: 9

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 4802 Color: 12
Size: 344 Color: 5
Size: 246 Color: 7

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 10
Size: 396 Color: 12
Size: 192 Color: 3

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 3010 Color: 13
Size: 2245 Color: 6
Size: 136 Color: 11

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 3346 Color: 17
Size: 1933 Color: 13
Size: 112 Color: 0

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 3379 Color: 16
Size: 1764 Color: 6
Size: 248 Color: 9

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 3431 Color: 4
Size: 1840 Color: 17
Size: 120 Color: 16

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 3685 Color: 17
Size: 1706 Color: 14

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 3719 Color: 2
Size: 1348 Color: 4
Size: 324 Color: 15

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 4091 Color: 8
Size: 1172 Color: 4
Size: 128 Color: 2

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 4420 Color: 9
Size: 971 Color: 3

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 4525 Color: 16
Size: 546 Color: 4
Size: 320 Color: 7

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 4543 Color: 2
Size: 848 Color: 13

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 4607 Color: 17
Size: 632 Color: 9
Size: 152 Color: 15

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 4615 Color: 12
Size: 776 Color: 15

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 4715 Color: 9
Size: 564 Color: 7
Size: 112 Color: 7

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 4682 Color: 8
Size: 709 Color: 15

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 4780 Color: 6
Size: 611 Color: 14

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 3002 Color: 0
Size: 2244 Color: 15
Size: 144 Color: 4

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 3316 Color: 15
Size: 1986 Color: 3
Size: 88 Color: 9

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 3383 Color: 4
Size: 1763 Color: 1
Size: 244 Color: 6

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 3826 Color: 18
Size: 1500 Color: 3
Size: 64 Color: 1

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 4020 Color: 14
Size: 1306 Color: 7
Size: 64 Color: 17

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 9
Size: 1166 Color: 5
Size: 56 Color: 2

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 4218 Color: 12
Size: 1172 Color: 2

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 0
Size: 764 Color: 1
Size: 296 Color: 4

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 4450 Color: 3
Size: 604 Color: 9
Size: 336 Color: 10

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 1
Size: 898 Color: 6
Size: 16 Color: 0

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 4578 Color: 4
Size: 812 Color: 15

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 4830 Color: 6
Size: 344 Color: 9
Size: 216 Color: 5

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 4850 Color: 12
Size: 540 Color: 13

Bin 77: 3 of cap free
Amount of items: 10
Items: 
Size: 2697 Color: 9
Size: 440 Color: 2
Size: 440 Color: 1
Size: 384 Color: 5
Size: 336 Color: 3
Size: 296 Color: 19
Size: 288 Color: 15
Size: 280 Color: 8
Size: 128 Color: 10
Size: 100 Color: 4

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 4738 Color: 17
Size: 651 Color: 5

Bin 79: 4 of cap free
Amount of items: 4
Items: 
Size: 2712 Color: 3
Size: 1486 Color: 11
Size: 982 Color: 8
Size: 208 Color: 16

Bin 80: 4 of cap free
Amount of items: 2
Items: 
Size: 4488 Color: 7
Size: 900 Color: 19

Bin 81: 4 of cap free
Amount of items: 2
Items: 
Size: 4648 Color: 5
Size: 740 Color: 13

Bin 82: 4 of cap free
Amount of items: 2
Items: 
Size: 4778 Color: 7
Size: 610 Color: 14

Bin 83: 5 of cap free
Amount of items: 3
Items: 
Size: 3326 Color: 8
Size: 1931 Color: 15
Size: 130 Color: 7

Bin 84: 5 of cap free
Amount of items: 2
Items: 
Size: 3731 Color: 1
Size: 1656 Color: 3

Bin 85: 5 of cap free
Amount of items: 2
Items: 
Size: 4335 Color: 5
Size: 1052 Color: 0

Bin 86: 5 of cap free
Amount of items: 2
Items: 
Size: 4439 Color: 6
Size: 948 Color: 1

Bin 87: 5 of cap free
Amount of items: 2
Items: 
Size: 4716 Color: 14
Size: 671 Color: 11

Bin 88: 6 of cap free
Amount of items: 3
Items: 
Size: 3065 Color: 5
Size: 1937 Color: 4
Size: 384 Color: 3

Bin 89: 6 of cap free
Amount of items: 3
Items: 
Size: 3180 Color: 9
Size: 1411 Color: 1
Size: 795 Color: 15

Bin 90: 6 of cap free
Amount of items: 3
Items: 
Size: 4202 Color: 9
Size: 1120 Color: 4
Size: 64 Color: 17

Bin 91: 6 of cap free
Amount of items: 3
Items: 
Size: 4280 Color: 9
Size: 994 Color: 0
Size: 112 Color: 4

Bin 92: 8 of cap free
Amount of items: 3
Items: 
Size: 2708 Color: 17
Size: 2228 Color: 0
Size: 448 Color: 6

Bin 93: 8 of cap free
Amount of items: 2
Items: 
Size: 3443 Color: 3
Size: 1941 Color: 5

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 3916 Color: 13
Size: 1468 Color: 8

Bin 95: 8 of cap free
Amount of items: 2
Items: 
Size: 4204 Color: 6
Size: 1180 Color: 9

Bin 96: 8 of cap free
Amount of items: 2
Items: 
Size: 4388 Color: 6
Size: 996 Color: 7

Bin 97: 8 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 0
Size: 881 Color: 16
Size: 32 Color: 14

Bin 98: 8 of cap free
Amount of items: 2
Items: 
Size: 4662 Color: 4
Size: 722 Color: 1

Bin 99: 8 of cap free
Amount of items: 2
Items: 
Size: 4832 Color: 2
Size: 552 Color: 14

Bin 100: 9 of cap free
Amount of items: 2
Items: 
Size: 3073 Color: 6
Size: 2310 Color: 17

Bin 101: 9 of cap free
Amount of items: 2
Items: 
Size: 3612 Color: 17
Size: 1771 Color: 2

Bin 102: 9 of cap free
Amount of items: 2
Items: 
Size: 4292 Color: 3
Size: 1091 Color: 8

Bin 103: 10 of cap free
Amount of items: 3
Items: 
Size: 2740 Color: 4
Size: 2242 Color: 13
Size: 400 Color: 19

Bin 104: 10 of cap free
Amount of items: 2
Items: 
Size: 3416 Color: 17
Size: 1966 Color: 8

Bin 105: 10 of cap free
Amount of items: 2
Items: 
Size: 4526 Color: 5
Size: 856 Color: 12

Bin 106: 11 of cap free
Amount of items: 3
Items: 
Size: 3077 Color: 12
Size: 2144 Color: 7
Size: 160 Color: 18

Bin 107: 11 of cap free
Amount of items: 2
Items: 
Size: 4349 Color: 0
Size: 1032 Color: 9

Bin 108: 11 of cap free
Amount of items: 2
Items: 
Size: 4612 Color: 2
Size: 769 Color: 13

Bin 109: 12 of cap free
Amount of items: 2
Items: 
Size: 3864 Color: 7
Size: 1516 Color: 6

Bin 110: 14 of cap free
Amount of items: 2
Items: 
Size: 3646 Color: 19
Size: 1732 Color: 14

Bin 111: 14 of cap free
Amount of items: 2
Items: 
Size: 3699 Color: 11
Size: 1679 Color: 9

Bin 112: 15 of cap free
Amount of items: 3
Items: 
Size: 3610 Color: 8
Size: 1423 Color: 2
Size: 344 Color: 4

Bin 113: 15 of cap free
Amount of items: 2
Items: 
Size: 4611 Color: 12
Size: 766 Color: 8

Bin 114: 15 of cap free
Amount of items: 3
Items: 
Size: 4661 Color: 15
Size: 700 Color: 0
Size: 16 Color: 1

Bin 115: 16 of cap free
Amount of items: 2
Items: 
Size: 3284 Color: 1
Size: 2092 Color: 17

Bin 116: 21 of cap free
Amount of items: 2
Items: 
Size: 3740 Color: 17
Size: 1631 Color: 11

Bin 117: 22 of cap free
Amount of items: 4
Items: 
Size: 2702 Color: 16
Size: 1395 Color: 8
Size: 977 Color: 0
Size: 296 Color: 12

Bin 118: 27 of cap free
Amount of items: 2
Items: 
Size: 4345 Color: 3
Size: 1020 Color: 19

Bin 119: 28 of cap free
Amount of items: 3
Items: 
Size: 3676 Color: 9
Size: 1302 Color: 15
Size: 386 Color: 4

Bin 120: 30 of cap free
Amount of items: 2
Items: 
Size: 3354 Color: 0
Size: 2008 Color: 17

Bin 121: 33 of cap free
Amount of items: 2
Items: 
Size: 4132 Color: 9
Size: 1227 Color: 14

Bin 122: 36 of cap free
Amount of items: 3
Items: 
Size: 2884 Color: 3
Size: 2248 Color: 4
Size: 224 Color: 15

Bin 123: 37 of cap free
Amount of items: 2
Items: 
Size: 3633 Color: 14
Size: 1722 Color: 7

Bin 124: 40 of cap free
Amount of items: 2
Items: 
Size: 3644 Color: 7
Size: 1708 Color: 12

Bin 125: 41 of cap free
Amount of items: 2
Items: 
Size: 4341 Color: 3
Size: 1010 Color: 17

Bin 126: 42 of cap free
Amount of items: 30
Items: 
Size: 282 Color: 0
Size: 278 Color: 8
Size: 240 Color: 0
Size: 220 Color: 15
Size: 216 Color: 11
Size: 208 Color: 2
Size: 200 Color: 19
Size: 200 Color: 16
Size: 192 Color: 19
Size: 192 Color: 16
Size: 192 Color: 11
Size: 192 Color: 2
Size: 176 Color: 14
Size: 176 Color: 8
Size: 176 Color: 8
Size: 176 Color: 7
Size: 176 Color: 3
Size: 174 Color: 19
Size: 174 Color: 3
Size: 160 Color: 12
Size: 160 Color: 5
Size: 158 Color: 16
Size: 144 Color: 6
Size: 144 Color: 5
Size: 144 Color: 2
Size: 128 Color: 10
Size: 128 Color: 5
Size: 120 Color: 13
Size: 112 Color: 10
Size: 112 Color: 6

Bin 127: 48 of cap free
Amount of items: 6
Items: 
Size: 2699 Color: 15
Size: 871 Color: 11
Size: 686 Color: 19
Size: 448 Color: 12
Size: 448 Color: 1
Size: 192 Color: 10

Bin 128: 50 of cap free
Amount of items: 2
Items: 
Size: 3348 Color: 15
Size: 1994 Color: 18

Bin 129: 51 of cap free
Amount of items: 3
Items: 
Size: 2724 Color: 2
Size: 1396 Color: 16
Size: 1221 Color: 4

Bin 130: 52 of cap free
Amount of items: 2
Items: 
Size: 2984 Color: 19
Size: 2356 Color: 6

Bin 131: 75 of cap free
Amount of items: 2
Items: 
Size: 3692 Color: 14
Size: 1625 Color: 3

Bin 132: 76 of cap free
Amount of items: 2
Items: 
Size: 3069 Color: 7
Size: 2247 Color: 5

Bin 133: 4316 of cap free
Amount of items: 10
Items: 
Size: 128 Color: 18
Size: 124 Color: 0
Size: 116 Color: 1
Size: 112 Color: 4
Size: 108 Color: 14
Size: 108 Color: 10
Size: 96 Color: 17
Size: 96 Color: 6
Size: 96 Color: 3
Size: 92 Color: 13

Total size: 711744
Total free space: 5392

