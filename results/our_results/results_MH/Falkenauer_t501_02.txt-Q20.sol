Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 16
Size: 287 Color: 0
Size: 251 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 19
Size: 353 Color: 11
Size: 253 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 4
Size: 292 Color: 13
Size: 266 Color: 8

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 13
Size: 256 Color: 15
Size: 251 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 2
Size: 298 Color: 16
Size: 291 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 18
Size: 364 Color: 5
Size: 268 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 8
Size: 256 Color: 9
Size: 253 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 5
Size: 309 Color: 17
Size: 257 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 2
Size: 367 Color: 11
Size: 259 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 5
Size: 331 Color: 8
Size: 280 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 10
Size: 321 Color: 8
Size: 297 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 6
Size: 344 Color: 6
Size: 263 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 19
Size: 269 Color: 3
Size: 261 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 251 Color: 18
Size: 251 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 19
Size: 358 Color: 7
Size: 253 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 291 Color: 18
Size: 253 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 10
Size: 322 Color: 12
Size: 266 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 258 Color: 17
Size: 254 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 16
Size: 255 Color: 12
Size: 252 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 306 Color: 16
Size: 282 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8
Size: 313 Color: 7
Size: 255 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 14
Size: 311 Color: 18
Size: 309 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 15
Size: 299 Color: 14
Size: 295 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 2
Size: 369 Color: 13
Size: 259 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 13
Size: 286 Color: 17
Size: 276 Color: 5

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 5
Size: 319 Color: 10
Size: 280 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 10
Size: 284 Color: 7
Size: 275 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9
Size: 258 Color: 16
Size: 256 Color: 7

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 19
Size: 312 Color: 2
Size: 259 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 280 Color: 11
Size: 269 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 14
Size: 360 Color: 12
Size: 278 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 7
Size: 266 Color: 12
Size: 259 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 6
Size: 312 Color: 12
Size: 251 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 19
Size: 373 Color: 3
Size: 254 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 323 Color: 15
Size: 319 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 17
Size: 368 Color: 10
Size: 258 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 17
Size: 300 Color: 6
Size: 258 Color: 11

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 325 Color: 0
Size: 283 Color: 18

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 18
Size: 338 Color: 13
Size: 306 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 280 Color: 6
Size: 278 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 17
Size: 314 Color: 11
Size: 256 Color: 8

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 12
Size: 332 Color: 1
Size: 275 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 10
Size: 321 Color: 9
Size: 275 Color: 11

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 16
Size: 287 Color: 8
Size: 261 Color: 6

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 18
Size: 287 Color: 16
Size: 279 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 6
Size: 294 Color: 12
Size: 287 Color: 6

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 13
Size: 269 Color: 7
Size: 263 Color: 16

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 290 Color: 5
Size: 263 Color: 15

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 14
Size: 307 Color: 9
Size: 304 Color: 14

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 9
Size: 300 Color: 17
Size: 290 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 12
Size: 357 Color: 10
Size: 250 Color: 5

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 12
Size: 330 Color: 11
Size: 253 Color: 9

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 11
Size: 317 Color: 5
Size: 253 Color: 9

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 308 Color: 14
Size: 266 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 291 Color: 12
Size: 262 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 11
Size: 338 Color: 16
Size: 307 Color: 15

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9
Size: 272 Color: 15
Size: 250 Color: 9

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 277 Color: 0
Size: 271 Color: 12

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 293 Color: 13
Size: 275 Color: 11

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 11
Size: 262 Color: 13
Size: 252 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 5
Size: 305 Color: 10
Size: 252 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 12
Size: 317 Color: 10
Size: 265 Color: 13

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 287 Color: 16
Size: 276 Color: 18

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 6
Size: 269 Color: 2
Size: 253 Color: 13

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 353 Color: 11
Size: 262 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 299 Color: 12
Size: 260 Color: 9

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 15
Size: 350 Color: 15
Size: 256 Color: 12

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 0
Size: 281 Color: 13
Size: 273 Color: 16

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 5
Size: 351 Color: 17
Size: 294 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 11
Size: 360 Color: 0
Size: 273 Color: 16

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 344 Color: 5
Size: 273 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 6
Size: 306 Color: 18
Size: 270 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 14
Size: 279 Color: 14
Size: 257 Color: 3

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 9
Size: 338 Color: 6
Size: 300 Color: 6

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 0
Size: 370 Color: 16
Size: 257 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 19
Size: 302 Color: 8
Size: 293 Color: 17

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 14
Size: 283 Color: 19
Size: 270 Color: 5

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 3
Size: 279 Color: 18
Size: 250 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 331 Color: 18
Size: 294 Color: 16

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 18
Size: 287 Color: 4
Size: 258 Color: 15

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 295 Color: 8
Size: 263 Color: 14

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 329 Color: 5
Size: 266 Color: 10

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 356 Color: 2
Size: 259 Color: 11

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 14
Size: 325 Color: 11
Size: 263 Color: 5

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 9
Size: 259 Color: 15
Size: 259 Color: 12

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 2
Size: 350 Color: 3
Size: 282 Color: 10

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 10
Size: 298 Color: 10
Size: 276 Color: 14

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 15
Size: 289 Color: 12
Size: 256 Color: 5

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 351 Color: 8
Size: 276 Color: 6

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9
Size: 263 Color: 1
Size: 257 Color: 10

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8
Size: 294 Color: 7
Size: 279 Color: 13

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 6
Size: 250 Color: 7

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 5
Size: 281 Color: 10
Size: 261 Color: 11

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 4
Size: 290 Color: 8
Size: 276 Color: 12

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 8
Size: 277 Color: 2
Size: 259 Color: 10

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 18
Size: 339 Color: 14
Size: 255 Color: 6

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 11
Size: 320 Color: 7
Size: 255 Color: 15

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 309 Color: 17
Size: 273 Color: 17

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 349 Color: 1
Size: 255 Color: 7

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 10
Size: 273 Color: 4
Size: 254 Color: 12

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 355 Color: 14
Size: 271 Color: 2

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 8
Size: 271 Color: 18
Size: 257 Color: 5

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 10
Size: 358 Color: 8
Size: 276 Color: 18

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 8
Size: 265 Color: 17
Size: 258 Color: 19

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 15
Size: 322 Color: 1
Size: 253 Color: 14

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 17
Size: 271 Color: 5
Size: 269 Color: 11

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 8
Size: 285 Color: 16
Size: 259 Color: 9

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 6
Size: 284 Color: 6
Size: 259 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 17
Size: 310 Color: 11
Size: 275 Color: 12

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 5
Size: 351 Color: 7
Size: 274 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 5
Size: 282 Color: 15
Size: 261 Color: 14

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 12
Size: 353 Color: 19
Size: 272 Color: 13

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 10
Size: 341 Color: 16
Size: 292 Color: 4

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 10
Size: 355 Color: 14
Size: 282 Color: 17

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 7
Size: 303 Color: 6
Size: 265 Color: 19

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 17
Size: 325 Color: 2
Size: 276 Color: 16

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 3
Size: 323 Color: 7
Size: 316 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 18
Size: 277 Color: 3
Size: 251 Color: 8

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 10
Size: 351 Color: 16
Size: 278 Color: 19

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 17
Size: 282 Color: 11
Size: 267 Color: 17

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 5
Size: 360 Color: 17
Size: 278 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 8
Size: 355 Color: 10
Size: 286 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 333 Color: 0
Size: 275 Color: 5

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 8
Size: 321 Color: 14
Size: 292 Color: 13

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 16
Size: 333 Color: 9
Size: 291 Color: 15

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 10
Size: 327 Color: 3
Size: 290 Color: 6

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 14
Size: 258 Color: 8
Size: 252 Color: 11

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 12
Size: 293 Color: 2
Size: 266 Color: 4

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 5
Size: 325 Color: 6
Size: 297 Color: 14

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 8
Size: 351 Color: 0
Size: 279 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 5
Size: 327 Color: 8
Size: 291 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 8
Size: 347 Color: 10
Size: 262 Color: 15

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 304 Color: 6
Size: 286 Color: 12

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 17
Size: 285 Color: 5
Size: 270 Color: 10

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 7
Size: 286 Color: 19
Size: 261 Color: 15

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 17
Size: 273 Color: 2
Size: 269 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 18
Size: 309 Color: 16
Size: 295 Color: 6

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 17
Size: 354 Color: 3
Size: 285 Color: 17

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8
Size: 297 Color: 14
Size: 297 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 14
Size: 344 Color: 15
Size: 262 Color: 14

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 5
Size: 335 Color: 14
Size: 250 Color: 5

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 14
Size: 352 Color: 3
Size: 270 Color: 8

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 10
Size: 275 Color: 19
Size: 264 Color: 13

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 14
Size: 266 Color: 18
Size: 250 Color: 7

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 288 Color: 15
Size: 263 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 6
Size: 352 Color: 14
Size: 271 Color: 5

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 17
Size: 284 Color: 1
Size: 252 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 12
Size: 288 Color: 10
Size: 259 Color: 4

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 17
Size: 268 Color: 7
Size: 255 Color: 4

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 12
Size: 339 Color: 15
Size: 285 Color: 3

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 13
Size: 295 Color: 18
Size: 262 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 7
Size: 316 Color: 15
Size: 272 Color: 17

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 14
Size: 272 Color: 15
Size: 256 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 10
Size: 353 Color: 8
Size: 293 Color: 19

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 19
Size: 322 Color: 14
Size: 270 Color: 15

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 16
Size: 308 Color: 6
Size: 301 Color: 8

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 307 Color: 16
Size: 287 Color: 6

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 7
Size: 340 Color: 1
Size: 259 Color: 7

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 4
Size: 269 Color: 9
Size: 267 Color: 15

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 11
Size: 295 Color: 9
Size: 282 Color: 2

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 17
Size: 325 Color: 2
Size: 283 Color: 9

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 18
Size: 281 Color: 6
Size: 271 Color: 14

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 353 Color: 16
Size: 269 Color: 18

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 18
Size: 317 Color: 10
Size: 252 Color: 15

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 343 Color: 9
Size: 260 Color: 16

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 6
Size: 267 Color: 10
Size: 261 Color: 19

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 14
Size: 266 Color: 5
Size: 258 Color: 10

Total size: 167000
Total free space: 0

