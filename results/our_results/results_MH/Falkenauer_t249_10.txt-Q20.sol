Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 254 Color: 14
Size: 254 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 12
Size: 301 Color: 15
Size: 290 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 5
Size: 269 Color: 5
Size: 251 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 17
Size: 254 Color: 19
Size: 252 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 16
Size: 348 Color: 3
Size: 263 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 2
Size: 269 Color: 11
Size: 250 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 307 Color: 9
Size: 300 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 11
Size: 296 Color: 7
Size: 289 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 13
Size: 318 Color: 15
Size: 305 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 12
Size: 274 Color: 2
Size: 267 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 15
Size: 308 Color: 12
Size: 256 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 5
Size: 330 Color: 14
Size: 274 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 326 Color: 8
Size: 300 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 13
Size: 260 Color: 7
Size: 251 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 12
Size: 304 Color: 15
Size: 294 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 16
Size: 302 Color: 4
Size: 262 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 17
Size: 352 Color: 12
Size: 258 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 16
Size: 266 Color: 1
Size: 257 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 14
Size: 255 Color: 2
Size: 253 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 363 Color: 16
Size: 264 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 13
Size: 366 Color: 8
Size: 251 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 3
Size: 324 Color: 16
Size: 324 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 15
Size: 272 Color: 5
Size: 257 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 13
Size: 350 Color: 16
Size: 296 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 295 Color: 15
Size: 257 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 2
Size: 303 Color: 5
Size: 292 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 313 Color: 4
Size: 288 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 294 Color: 18
Size: 288 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 11
Size: 265 Color: 19
Size: 258 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 14
Size: 360 Color: 7
Size: 269 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 17
Size: 345 Color: 8
Size: 293 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 10
Size: 261 Color: 13
Size: 254 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 16
Size: 290 Color: 7
Size: 265 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 14
Size: 267 Color: 13
Size: 260 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 12
Size: 299 Color: 5
Size: 258 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 13
Size: 265 Color: 14
Size: 260 Color: 16

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 18
Size: 326 Color: 13
Size: 279 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 6
Size: 287 Color: 16
Size: 255 Color: 19

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 15
Size: 323 Color: 7
Size: 294 Color: 13

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 14
Size: 291 Color: 18
Size: 275 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 3
Size: 310 Color: 9
Size: 292 Color: 13

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 10
Size: 362 Color: 15
Size: 261 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 13
Size: 329 Color: 17
Size: 261 Color: 5

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 5
Size: 339 Color: 0
Size: 264 Color: 11

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 15
Size: 251 Color: 11
Size: 250 Color: 6

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 18
Size: 260 Color: 18
Size: 253 Color: 6

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 349 Color: 16
Size: 266 Color: 14

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 9
Size: 275 Color: 8
Size: 250 Color: 17

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 17
Size: 265 Color: 1
Size: 250 Color: 11

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 10
Size: 287 Color: 0
Size: 263 Color: 13

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 6
Size: 352 Color: 9
Size: 250 Color: 7

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 2
Size: 279 Color: 10
Size: 260 Color: 14

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 12
Size: 312 Color: 18
Size: 266 Color: 11

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 13
Size: 284 Color: 18
Size: 255 Color: 13

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 19
Size: 319 Color: 16
Size: 281 Color: 5

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 10
Size: 315 Color: 0
Size: 261 Color: 13

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 6
Size: 257 Color: 18
Size: 250 Color: 14

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 16
Size: 347 Color: 8
Size: 296 Color: 12

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 9
Size: 313 Color: 5
Size: 279 Color: 12

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 4
Size: 278 Color: 10
Size: 265 Color: 6

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 2
Size: 303 Color: 13
Size: 281 Color: 18

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 274 Color: 16
Size: 255 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 271 Color: 14
Size: 264 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9
Size: 263 Color: 11
Size: 263 Color: 11

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 13
Size: 284 Color: 10
Size: 279 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 8
Size: 276 Color: 3
Size: 252 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 3
Size: 284 Color: 17
Size: 275 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 19
Size: 260 Color: 16
Size: 259 Color: 6

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 15
Size: 359 Color: 2
Size: 252 Color: 15

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 4
Size: 272 Color: 16
Size: 251 Color: 15

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 3
Size: 309 Color: 10
Size: 251 Color: 10

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 280 Color: 19
Size: 271 Color: 8

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 4
Size: 264 Color: 10
Size: 250 Color: 14

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 8
Size: 366 Color: 0
Size: 260 Color: 12

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 18
Size: 321 Color: 17
Size: 305 Color: 6

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9
Size: 261 Color: 0
Size: 251 Color: 18

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 11
Size: 287 Color: 9
Size: 261 Color: 9

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 3
Size: 270 Color: 13
Size: 269 Color: 7

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 14
Size: 267 Color: 8
Size: 250 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 19
Size: 358 Color: 18
Size: 277 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 17
Size: 277 Color: 15
Size: 268 Color: 9

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 11
Size: 280 Color: 7
Size: 261 Color: 15

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 11
Size: 305 Color: 19
Size: 302 Color: 9

Total size: 83000
Total free space: 0

