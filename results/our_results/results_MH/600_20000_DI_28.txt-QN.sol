Capicity Bin: 19008
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 481
Size: 4372 Color: 333
Size: 388 Color: 49

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 483
Size: 3828 Color: 319
Size: 868 Color: 142

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 14396 Color: 484
Size: 2684 Color: 275
Size: 1928 Color: 226

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 14806 Color: 494
Size: 3502 Color: 309
Size: 700 Color: 119

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 14808 Color: 495
Size: 3536 Color: 312
Size: 664 Color: 110

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 14840 Color: 496
Size: 3480 Color: 308
Size: 688 Color: 116

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 14940 Color: 497
Size: 2492 Color: 267
Size: 1576 Color: 196

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14998 Color: 500
Size: 3354 Color: 304
Size: 656 Color: 108

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 15120 Color: 504
Size: 3512 Color: 311
Size: 376 Color: 42

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 15306 Color: 510
Size: 2028 Color: 236
Size: 1674 Color: 210

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15372 Color: 511
Size: 1964 Color: 231
Size: 1672 Color: 209

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15388 Color: 512
Size: 2732 Color: 279
Size: 888 Color: 145

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15408 Color: 513
Size: 2996 Color: 289
Size: 604 Color: 102

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15544 Color: 517
Size: 3288 Color: 300
Size: 176 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 15735 Color: 522
Size: 2277 Color: 255
Size: 996 Color: 154

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15772 Color: 525
Size: 3036 Color: 294
Size: 200 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15788 Color: 526
Size: 2888 Color: 287
Size: 332 Color: 27

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15792 Color: 527
Size: 2158 Color: 247
Size: 1058 Color: 161

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15820 Color: 528
Size: 2860 Color: 285
Size: 328 Color: 25

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15884 Color: 531
Size: 2168 Color: 248
Size: 956 Color: 149

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15896 Color: 532
Size: 1576 Color: 197
Size: 1536 Color: 189

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16018 Color: 537
Size: 2078 Color: 242
Size: 912 Color: 148

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16028 Color: 538
Size: 2444 Color: 264
Size: 536 Color: 89

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16084 Color: 540
Size: 1950 Color: 229
Size: 974 Color: 150

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16202 Color: 544
Size: 2352 Color: 258
Size: 454 Color: 70

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16284 Color: 548
Size: 2276 Color: 254
Size: 448 Color: 67

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16408 Color: 553
Size: 1608 Color: 204
Size: 992 Color: 152

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16439 Color: 555
Size: 2045 Color: 239
Size: 524 Color: 85

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16454 Color: 556
Size: 1842 Color: 221
Size: 712 Color: 122

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16464 Color: 557
Size: 2168 Color: 249
Size: 376 Color: 41

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16575 Color: 564
Size: 2021 Color: 234
Size: 412 Color: 58

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16580 Color: 565
Size: 1334 Color: 182
Size: 1094 Color: 163

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16582 Color: 566
Size: 1754 Color: 215
Size: 672 Color: 113

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16668 Color: 570
Size: 1344 Color: 184
Size: 996 Color: 155

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16670 Color: 571
Size: 2008 Color: 233
Size: 330 Color: 26

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16688 Color: 573
Size: 1568 Color: 192
Size: 752 Color: 124

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16694 Color: 574
Size: 1840 Color: 220
Size: 474 Color: 75

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16695 Color: 575
Size: 1929 Color: 227
Size: 384 Color: 47

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16769 Color: 579
Size: 1867 Color: 224
Size: 372 Color: 40

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 580
Size: 1956 Color: 230
Size: 276 Color: 15

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16815 Color: 582
Size: 1829 Color: 219
Size: 364 Color: 36

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 16816 Color: 583
Size: 1848 Color: 222
Size: 344 Color: 30

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 16863 Color: 584
Size: 1789 Color: 217
Size: 356 Color: 35

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 16900 Color: 586
Size: 1352 Color: 185
Size: 756 Color: 125

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 16916 Color: 588
Size: 1708 Color: 211
Size: 384 Color: 44

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 16952 Color: 590
Size: 1384 Color: 188
Size: 672 Color: 114

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17002 Color: 593
Size: 1622 Color: 206
Size: 384 Color: 48

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17016 Color: 594
Size: 1232 Color: 177
Size: 760 Color: 126

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17018 Color: 595
Size: 1568 Color: 193
Size: 422 Color: 61

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17066 Color: 597
Size: 1536 Color: 190
Size: 406 Color: 56

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17086 Color: 599
Size: 1602 Color: 203
Size: 320 Color: 22

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17096 Color: 600
Size: 1576 Color: 195
Size: 336 Color: 28

Bin 53: 1 of cap free
Amount of items: 5
Items: 
Size: 9540 Color: 418
Size: 7917 Color: 394
Size: 536 Color: 88
Size: 512 Color: 83
Size: 502 Color: 82

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 452
Size: 5743 Color: 359
Size: 344 Color: 29

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 13939 Color: 476
Size: 5004 Color: 346
Size: 64 Color: 2

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 14269 Color: 482
Size: 4306 Color: 330
Size: 432 Color: 65

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 14467 Color: 488
Size: 3372 Color: 305
Size: 1168 Color: 175

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 15264 Color: 509
Size: 3743 Color: 315

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 15822 Color: 529
Size: 3185 Color: 297

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 16161 Color: 542
Size: 2494 Color: 268
Size: 352 Color: 32

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 16471 Color: 558
Size: 2536 Color: 271

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 16486 Color: 559
Size: 2521 Color: 270

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 16567 Color: 563
Size: 1864 Color: 223
Size: 576 Color: 96

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 16687 Color: 572
Size: 1662 Color: 208
Size: 658 Color: 109

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 16756 Color: 578
Size: 2251 Color: 251

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 13528 Color: 465
Size: 4362 Color: 331
Size: 1116 Color: 166

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 14805 Color: 493
Size: 3785 Color: 317
Size: 416 Color: 59

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 15055 Color: 502
Size: 3951 Color: 323

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 15246 Color: 508
Size: 2128 Color: 245
Size: 1632 Color: 207

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 15920 Color: 533
Size: 3086 Color: 295

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 16277 Color: 547
Size: 2729 Color: 278

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 16664 Color: 569
Size: 2342 Color: 257

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 16696 Color: 576
Size: 2310 Color: 256

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 16906 Color: 587
Size: 2100 Color: 243

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 17076 Color: 598
Size: 1930 Color: 228

Bin 76: 3 of cap free
Amount of items: 7
Items: 
Size: 9508 Color: 407
Size: 2115 Color: 244
Size: 2060 Color: 240
Size: 2022 Color: 235
Size: 1892 Color: 225
Size: 768 Color: 131
Size: 640 Color: 106

Bin 77: 3 of cap free
Amount of items: 7
Items: 
Size: 9509 Color: 408
Size: 2354 Color: 260
Size: 2354 Color: 259
Size: 2272 Color: 253
Size: 1256 Color: 178
Size: 636 Color: 105
Size: 624 Color: 104

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 14216 Color: 479
Size: 4789 Color: 340

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 14741 Color: 491
Size: 4264 Color: 328

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 15985 Color: 535
Size: 3020 Color: 292

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 16864 Color: 585
Size: 2141 Color: 246

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 16976 Color: 592
Size: 2029 Color: 237

Bin 83: 4 of cap free
Amount of items: 5
Items: 
Size: 12008 Color: 441
Size: 5776 Color: 360
Size: 412 Color: 57
Size: 404 Color: 54
Size: 404 Color: 53

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 13780 Color: 473
Size: 5080 Color: 352
Size: 144 Color: 5

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 14138 Color: 477
Size: 3778 Color: 316
Size: 1088 Color: 162

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 14224 Color: 480
Size: 4364 Color: 332
Size: 416 Color: 60

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 14452 Color: 487
Size: 4552 Color: 337

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 15128 Color: 505
Size: 3844 Color: 321
Size: 32 Color: 0

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 16740 Color: 577
Size: 2264 Color: 252

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 17036 Color: 596
Size: 1968 Color: 232

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 11976 Color: 438
Size: 7027 Color: 381

Bin 92: 6 of cap free
Amount of items: 3
Items: 
Size: 15413 Color: 514
Size: 3557 Color: 313
Size: 32 Color: 1

Bin 93: 6 of cap free
Amount of items: 2
Items: 
Size: 15754 Color: 524
Size: 3248 Color: 299

Bin 94: 6 of cap free
Amount of items: 2
Items: 
Size: 16002 Color: 536
Size: 3000 Color: 291

Bin 95: 6 of cap free
Amount of items: 2
Items: 
Size: 16208 Color: 545
Size: 2794 Color: 282

Bin 96: 6 of cap free
Amount of items: 2
Items: 
Size: 16518 Color: 561
Size: 2484 Color: 266

Bin 97: 6 of cap free
Amount of items: 2
Items: 
Size: 16798 Color: 581
Size: 2204 Color: 250

Bin 98: 7 of cap free
Amount of items: 5
Items: 
Size: 9556 Color: 419
Size: 7921 Color: 395
Size: 528 Color: 87
Size: 500 Color: 81
Size: 496 Color: 80

Bin 99: 7 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 455
Size: 3295 Color: 301
Size: 2742 Color: 280

Bin 100: 7 of cap free
Amount of items: 2
Items: 
Size: 15498 Color: 516
Size: 3503 Color: 310

Bin 101: 8 of cap free
Amount of items: 3
Items: 
Size: 13842 Color: 474
Size: 5028 Color: 349
Size: 130 Color: 4

Bin 102: 8 of cap free
Amount of items: 2
Items: 
Size: 15658 Color: 521
Size: 3342 Color: 303

Bin 103: 8 of cap free
Amount of items: 2
Items: 
Size: 15976 Color: 534
Size: 3024 Color: 293

Bin 104: 8 of cap free
Amount of items: 2
Items: 
Size: 16136 Color: 541
Size: 2864 Color: 286

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 16938 Color: 589
Size: 2062 Color: 241

Bin 106: 9 of cap free
Amount of items: 3
Items: 
Size: 10900 Color: 429
Size: 7631 Color: 385
Size: 468 Color: 74

Bin 107: 9 of cap free
Amount of items: 3
Items: 
Size: 11007 Color: 431
Size: 7536 Color: 384
Size: 456 Color: 71

Bin 108: 9 of cap free
Amount of items: 2
Items: 
Size: 16372 Color: 552
Size: 2627 Color: 273

Bin 109: 9 of cap free
Amount of items: 2
Items: 
Size: 16964 Color: 591
Size: 2035 Color: 238

Bin 110: 10 of cap free
Amount of items: 4
Items: 
Size: 13602 Color: 467
Size: 4976 Color: 343
Size: 212 Color: 14
Size: 208 Color: 13

Bin 111: 10 of cap free
Amount of items: 3
Items: 
Size: 13778 Color: 472
Size: 5060 Color: 351
Size: 160 Color: 6

Bin 112: 10 of cap free
Amount of items: 3
Items: 
Size: 13904 Color: 475
Size: 4982 Color: 344
Size: 112 Color: 3

Bin 113: 10 of cap free
Amount of items: 2
Items: 
Size: 15622 Color: 519
Size: 3376 Color: 306

Bin 114: 10 of cap free
Amount of items: 2
Items: 
Size: 16340 Color: 551
Size: 2658 Color: 274

Bin 115: 10 of cap free
Amount of items: 2
Items: 
Size: 16422 Color: 554
Size: 2576 Color: 272

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 16492 Color: 560
Size: 2506 Color: 269

Bin 117: 11 of cap free
Amount of items: 2
Items: 
Size: 16309 Color: 550
Size: 2688 Color: 276

Bin 118: 12 of cap free
Amount of items: 3
Items: 
Size: 12326 Color: 449
Size: 6318 Color: 371
Size: 352 Color: 34

Bin 119: 12 of cap free
Amount of items: 2
Items: 
Size: 14404 Color: 485
Size: 4592 Color: 339

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 16296 Color: 549
Size: 2700 Color: 277

Bin 121: 13 of cap free
Amount of items: 3
Items: 
Size: 12987 Color: 457
Size: 4426 Color: 334
Size: 1582 Color: 200

Bin 122: 13 of cap free
Amount of items: 2
Items: 
Size: 15857 Color: 530
Size: 3138 Color: 296

Bin 123: 13 of cap free
Amount of items: 2
Items: 
Size: 16239 Color: 546
Size: 2756 Color: 281

Bin 124: 14 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 436
Size: 7104 Color: 382
Size: 428 Color: 62

Bin 125: 14 of cap free
Amount of items: 2
Items: 
Size: 14722 Color: 490
Size: 4272 Color: 329

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 15420 Color: 515
Size: 3574 Color: 314

Bin 127: 14 of cap free
Amount of items: 2
Items: 
Size: 15656 Color: 520
Size: 3338 Color: 302

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 16186 Color: 543
Size: 2808 Color: 283

Bin 129: 15 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 445
Size: 6496 Color: 373
Size: 380 Color: 43

Bin 130: 15 of cap free
Amount of items: 2
Items: 
Size: 14768 Color: 492
Size: 4225 Color: 327

Bin 131: 16 of cap free
Amount of items: 3
Items: 
Size: 9680 Color: 420
Size: 8816 Color: 402
Size: 496 Color: 79

Bin 132: 16 of cap free
Amount of items: 2
Items: 
Size: 15064 Color: 503
Size: 3928 Color: 322

Bin 133: 17 of cap free
Amount of items: 2
Items: 
Size: 15187 Color: 507
Size: 3804 Color: 318

Bin 134: 17 of cap free
Amount of items: 2
Items: 
Size: 16065 Color: 539
Size: 2926 Color: 288

Bin 135: 17 of cap free
Amount of items: 2
Items: 
Size: 16583 Color: 567
Size: 2408 Color: 263

Bin 136: 19 of cap free
Amount of items: 2
Items: 
Size: 16616 Color: 568
Size: 2373 Color: 262

Bin 137: 20 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 478
Size: 4802 Color: 341

Bin 138: 20 of cap free
Amount of items: 2
Items: 
Size: 14420 Color: 486
Size: 4568 Color: 338

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 15748 Color: 523
Size: 3240 Color: 298

Bin 140: 21 of cap free
Amount of items: 2
Items: 
Size: 16534 Color: 562
Size: 2453 Color: 265

Bin 141: 22 of cap free
Amount of items: 2
Items: 
Size: 14964 Color: 499
Size: 4022 Color: 326

Bin 142: 23 of cap free
Amount of items: 2
Items: 
Size: 15589 Color: 518
Size: 3396 Color: 307

Bin 143: 24 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 471
Size: 5044 Color: 350
Size: 176 Color: 7

Bin 144: 26 of cap free
Amount of items: 2
Items: 
Size: 15006 Color: 501
Size: 3976 Color: 324

Bin 145: 28 of cap free
Amount of items: 3
Items: 
Size: 12310 Color: 448
Size: 6302 Color: 370
Size: 368 Color: 37

Bin 146: 29 of cap free
Amount of items: 2
Items: 
Size: 15135 Color: 506
Size: 3844 Color: 320

Bin 147: 32 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 424
Size: 7792 Color: 387
Size: 480 Color: 76

Bin 148: 32 of cap free
Amount of items: 2
Items: 
Size: 14960 Color: 498
Size: 4016 Color: 325

Bin 149: 33 of cap free
Amount of items: 3
Items: 
Size: 13520 Color: 464
Size: 4447 Color: 335
Size: 1008 Color: 160

Bin 150: 36 of cap free
Amount of items: 17
Items: 
Size: 1576 Color: 194
Size: 1548 Color: 191
Size: 1366 Color: 187
Size: 1360 Color: 186
Size: 1344 Color: 183
Size: 1280 Color: 181
Size: 1260 Color: 180
Size: 1260 Color: 179
Size: 1176 Color: 176
Size: 1160 Color: 174
Size: 1160 Color: 173
Size: 1008 Color: 159
Size: 710 Color: 121
Size: 704 Color: 120
Size: 700 Color: 118
Size: 688 Color: 117
Size: 672 Color: 115

Bin 151: 36 of cap free
Amount of items: 2
Items: 
Size: 14478 Color: 489
Size: 4494 Color: 336

Bin 152: 39 of cap free
Amount of items: 4
Items: 
Size: 12020 Color: 443
Size: 6165 Color: 367
Size: 400 Color: 50
Size: 384 Color: 46

Bin 153: 42 of cap free
Amount of items: 2
Items: 
Size: 10678 Color: 422
Size: 8288 Color: 400

Bin 154: 50 of cap free
Amount of items: 3
Items: 
Size: 13040 Color: 461
Size: 5598 Color: 357
Size: 320 Color: 19

Bin 155: 52 of cap free
Amount of items: 3
Items: 
Size: 13046 Color: 462
Size: 5606 Color: 358
Size: 304 Color: 18

Bin 156: 56 of cap free
Amount of items: 7
Items: 
Size: 9506 Color: 406
Size: 1824 Color: 218
Size: 1764 Color: 216
Size: 1748 Color: 214
Size: 1744 Color: 213
Size: 1726 Color: 212
Size: 640 Color: 107

Bin 157: 56 of cap free
Amount of items: 3
Items: 
Size: 12294 Color: 447
Size: 6290 Color: 369
Size: 368 Color: 38

Bin 158: 67 of cap free
Amount of items: 6
Items: 
Size: 9512 Color: 409
Size: 2997 Color: 290
Size: 2851 Color: 284
Size: 2365 Color: 261
Size: 616 Color: 103
Size: 600 Color: 101

Bin 159: 70 of cap free
Amount of items: 2
Items: 
Size: 12004 Color: 440
Size: 6934 Color: 380

Bin 160: 76 of cap free
Amount of items: 3
Items: 
Size: 13030 Color: 460
Size: 5582 Color: 356
Size: 320 Color: 20

Bin 161: 85 of cap free
Amount of items: 3
Items: 
Size: 10690 Color: 423
Size: 7743 Color: 386
Size: 490 Color: 77

Bin 162: 92 of cap free
Amount of items: 3
Items: 
Size: 12052 Color: 444
Size: 6480 Color: 372
Size: 384 Color: 45

Bin 163: 100 of cap free
Amount of items: 4
Items: 
Size: 13012 Color: 458
Size: 5256 Color: 353
Size: 320 Color: 24
Size: 320 Color: 23

Bin 164: 104 of cap free
Amount of items: 3
Items: 
Size: 13014 Color: 459
Size: 5570 Color: 355
Size: 320 Color: 21

Bin 165: 115 of cap free
Amount of items: 3
Items: 
Size: 13698 Color: 470
Size: 5019 Color: 348
Size: 176 Color: 8

Bin 166: 118 of cap free
Amount of items: 2
Items: 
Size: 9514 Color: 411
Size: 9376 Color: 404

Bin 167: 133 of cap free
Amount of items: 3
Items: 
Size: 13673 Color: 469
Size: 5010 Color: 347
Size: 192 Color: 10

Bin 168: 146 of cap free
Amount of items: 3
Items: 
Size: 12282 Color: 446
Size: 6212 Color: 368
Size: 368 Color: 39

Bin 169: 158 of cap free
Amount of items: 3
Items: 
Size: 11248 Color: 433
Size: 7154 Color: 383
Size: 448 Color: 66

Bin 170: 162 of cap free
Amount of items: 3
Items: 
Size: 13263 Color: 463
Size: 5295 Color: 354
Size: 288 Color: 17

Bin 171: 164 of cap free
Amount of items: 3
Items: 
Size: 12656 Color: 451
Size: 5840 Color: 363
Size: 348 Color: 31

Bin 172: 164 of cap free
Amount of items: 2
Items: 
Size: 12980 Color: 456
Size: 5864 Color: 366

Bin 173: 173 of cap free
Amount of items: 3
Items: 
Size: 12655 Color: 450
Size: 5828 Color: 362
Size: 352 Color: 33

Bin 174: 177 of cap free
Amount of items: 2
Items: 
Size: 9513 Color: 410
Size: 9318 Color: 403

Bin 175: 180 of cap free
Amount of items: 2
Items: 
Size: 10884 Color: 428
Size: 7944 Color: 399

Bin 176: 181 of cap free
Amount of items: 2
Items: 
Size: 11996 Color: 439
Size: 6831 Color: 379

Bin 177: 184 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 468
Size: 4998 Color: 345
Size: 208 Color: 12

Bin 178: 185 of cap free
Amount of items: 3
Items: 
Size: 11611 Color: 437
Size: 6808 Color: 378
Size: 404 Color: 55

Bin 179: 190 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 466
Size: 4970 Color: 342
Size: 288 Color: 16

Bin 180: 208 of cap free
Amount of items: 2
Items: 
Size: 10872 Color: 427
Size: 7928 Color: 398

Bin 181: 208 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 454
Size: 5848 Color: 365

Bin 182: 224 of cap free
Amount of items: 2
Items: 
Size: 12940 Color: 453
Size: 5844 Color: 364

Bin 183: 227 of cap free
Amount of items: 8
Items: 
Size: 9505 Color: 405
Size: 1612 Color: 205
Size: 1582 Color: 202
Size: 1582 Color: 201
Size: 1580 Color: 199
Size: 1580 Color: 198
Size: 672 Color: 112
Size: 668 Color: 111

Bin 184: 244 of cap free
Amount of items: 2
Items: 
Size: 10840 Color: 426
Size: 7924 Color: 397

Bin 185: 275 of cap free
Amount of items: 2
Items: 
Size: 10811 Color: 425
Size: 7922 Color: 396

Bin 186: 301 of cap free
Amount of items: 4
Items: 
Size: 11047 Color: 432
Size: 6764 Color: 375
Size: 448 Color: 69
Size: 448 Color: 68

Bin 187: 342 of cap free
Amount of items: 3
Items: 
Size: 11446 Color: 435
Size: 6792 Color: 377
Size: 428 Color: 63

Bin 188: 374 of cap free
Amount of items: 3
Items: 
Size: 11430 Color: 434
Size: 6772 Color: 376
Size: 432 Color: 64

Bin 189: 384 of cap free
Amount of items: 4
Items: 
Size: 12016 Color: 442
Size: 5804 Color: 361
Size: 404 Color: 52
Size: 400 Color: 51

Bin 190: 398 of cap free
Amount of items: 3
Items: 
Size: 9717 Color: 421
Size: 8397 Color: 401
Size: 496 Color: 78

Bin 191: 406 of cap free
Amount of items: 4
Items: 
Size: 11003 Color: 430
Size: 6671 Color: 374
Size: 464 Color: 73
Size: 464 Color: 72

Bin 192: 409 of cap free
Amount of items: 4
Items: 
Size: 9517 Color: 412
Size: 7884 Color: 388
Size: 600 Color: 100
Size: 598 Color: 99

Bin 193: 420 of cap free
Amount of items: 4
Items: 
Size: 9520 Color: 413
Size: 7892 Color: 389
Size: 592 Color: 98
Size: 584 Color: 97

Bin 194: 440 of cap free
Amount of items: 4
Items: 
Size: 9522 Color: 414
Size: 7894 Color: 390
Size: 576 Color: 95
Size: 576 Color: 94

Bin 195: 454 of cap free
Amount of items: 4
Items: 
Size: 9524 Color: 415
Size: 7906 Color: 391
Size: 568 Color: 93
Size: 556 Color: 92

Bin 196: 484 of cap free
Amount of items: 4
Items: 
Size: 9528 Color: 416
Size: 7908 Color: 392
Size: 544 Color: 91
Size: 544 Color: 90

Bin 197: 517 of cap free
Amount of items: 4
Items: 
Size: 9538 Color: 417
Size: 7913 Color: 393
Size: 528 Color: 86
Size: 512 Color: 84

Bin 198: 746 of cap free
Amount of items: 19
Items: 
Size: 1152 Color: 172
Size: 1152 Color: 171
Size: 1152 Color: 170
Size: 1148 Color: 169
Size: 1120 Color: 168
Size: 1120 Color: 167
Size: 1116 Color: 165
Size: 1112 Color: 164
Size: 1002 Color: 158
Size: 1000 Color: 157
Size: 1000 Color: 156
Size: 800 Color: 134
Size: 788 Color: 133
Size: 784 Color: 132
Size: 768 Color: 130
Size: 768 Color: 129
Size: 768 Color: 128
Size: 760 Color: 127
Size: 752 Color: 123

Bin 199: 7644 of cap free
Amount of items: 13
Items: 
Size: 992 Color: 153
Size: 992 Color: 151
Size: 896 Color: 147
Size: 896 Color: 146
Size: 884 Color: 144
Size: 872 Color: 143
Size: 864 Color: 141
Size: 860 Color: 140
Size: 844 Color: 139
Size: 832 Color: 138
Size: 832 Color: 137
Size: 800 Color: 136
Size: 800 Color: 135

Total size: 3763584
Total free space: 19008

