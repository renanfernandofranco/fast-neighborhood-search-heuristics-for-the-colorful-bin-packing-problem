Capicity Bin: 1000001
Lower Bound: 46

Bins used: 47
Amount of Colors: 2

Bin 1: 306 of cap free
Amount of items: 3
Items: 
Size: 718213 Color: 0
Size: 156112 Color: 1
Size: 125370 Color: 1

Bin 2: 890 of cap free
Amount of items: 3
Items: 
Size: 413047 Color: 0
Size: 409115 Color: 1
Size: 176949 Color: 0

Bin 3: 1010 of cap free
Amount of items: 2
Items: 
Size: 700026 Color: 1
Size: 298965 Color: 0

Bin 4: 1025 of cap free
Amount of items: 2
Items: 
Size: 620192 Color: 0
Size: 378784 Color: 1

Bin 5: 1651 of cap free
Amount of items: 3
Items: 
Size: 638126 Color: 1
Size: 238823 Color: 0
Size: 121401 Color: 1

Bin 6: 1955 of cap free
Amount of items: 2
Items: 
Size: 626059 Color: 0
Size: 371987 Color: 1

Bin 7: 2749 of cap free
Amount of items: 2
Items: 
Size: 707050 Color: 0
Size: 290202 Color: 1

Bin 8: 2912 of cap free
Amount of items: 2
Items: 
Size: 737795 Color: 1
Size: 259294 Color: 0

Bin 9: 3186 of cap free
Amount of items: 2
Items: 
Size: 782312 Color: 0
Size: 214503 Color: 1

Bin 10: 3223 of cap free
Amount of items: 3
Items: 
Size: 638042 Color: 1
Size: 200283 Color: 1
Size: 158453 Color: 0

Bin 11: 4774 of cap free
Amount of items: 2
Items: 
Size: 530621 Color: 1
Size: 464606 Color: 0

Bin 12: 5248 of cap free
Amount of items: 2
Items: 
Size: 599152 Color: 1
Size: 395601 Color: 0

Bin 13: 5262 of cap free
Amount of items: 2
Items: 
Size: 642444 Color: 1
Size: 352295 Color: 0

Bin 14: 5538 of cap free
Amount of items: 3
Items: 
Size: 729805 Color: 0
Size: 164444 Color: 1
Size: 100214 Color: 0

Bin 15: 6648 of cap free
Amount of items: 2
Items: 
Size: 657499 Color: 0
Size: 335854 Color: 1

Bin 16: 7544 of cap free
Amount of items: 3
Items: 
Size: 464021 Color: 0
Size: 413445 Color: 1
Size: 114991 Color: 1

Bin 17: 8172 of cap free
Amount of items: 2
Items: 
Size: 633089 Color: 1
Size: 358740 Color: 0

Bin 18: 8964 of cap free
Amount of items: 2
Items: 
Size: 621077 Color: 1
Size: 369960 Color: 0

Bin 19: 9235 of cap free
Amount of items: 2
Items: 
Size: 689941 Color: 1
Size: 300825 Color: 0

Bin 20: 10763 of cap free
Amount of items: 2
Items: 
Size: 766496 Color: 1
Size: 222742 Color: 0

Bin 21: 10947 of cap free
Amount of items: 2
Items: 
Size: 540035 Color: 1
Size: 449019 Color: 0

Bin 22: 14467 of cap free
Amount of items: 2
Items: 
Size: 497977 Color: 1
Size: 487557 Color: 0

Bin 23: 15105 of cap free
Amount of items: 2
Items: 
Size: 548295 Color: 0
Size: 436601 Color: 1

Bin 24: 15850 of cap free
Amount of items: 2
Items: 
Size: 642055 Color: 0
Size: 342096 Color: 1

Bin 25: 17294 of cap free
Amount of items: 2
Items: 
Size: 667701 Color: 0
Size: 315006 Color: 1

Bin 26: 23600 of cap free
Amount of items: 2
Items: 
Size: 491871 Color: 1
Size: 484530 Color: 0

Bin 27: 25329 of cap free
Amount of items: 2
Items: 
Size: 747341 Color: 0
Size: 227331 Color: 1

Bin 28: 27090 of cap free
Amount of items: 2
Items: 
Size: 649254 Color: 1
Size: 323657 Color: 0

Bin 29: 30373 of cap free
Amount of items: 3
Items: 
Size: 644526 Color: 1
Size: 174853 Color: 0
Size: 150249 Color: 1

Bin 30: 32322 of cap free
Amount of items: 2
Items: 
Size: 779974 Color: 1
Size: 187705 Color: 0

Bin 31: 35834 of cap free
Amount of items: 2
Items: 
Size: 676508 Color: 0
Size: 287659 Color: 1

Bin 32: 35906 of cap free
Amount of items: 2
Items: 
Size: 723739 Color: 1
Size: 240356 Color: 0

Bin 33: 36047 of cap free
Amount of items: 2
Items: 
Size: 695485 Color: 1
Size: 268469 Color: 0

Bin 34: 41461 of cap free
Amount of items: 2
Items: 
Size: 634268 Color: 1
Size: 324272 Color: 0

Bin 35: 43246 of cap free
Amount of items: 2
Items: 
Size: 515656 Color: 0
Size: 441099 Color: 1

Bin 36: 44891 of cap free
Amount of items: 2
Items: 
Size: 666842 Color: 0
Size: 288268 Color: 1

Bin 37: 71330 of cap free
Amount of items: 2
Items: 
Size: 793435 Color: 0
Size: 135236 Color: 1

Bin 38: 71717 of cap free
Amount of items: 2
Items: 
Size: 756309 Color: 0
Size: 171975 Color: 1

Bin 39: 75900 of cap free
Amount of items: 3
Items: 
Size: 424703 Color: 1
Size: 396665 Color: 1
Size: 102733 Color: 0

Bin 40: 79727 of cap free
Amount of items: 2
Items: 
Size: 790367 Color: 1
Size: 129907 Color: 0

Bin 41: 79821 of cap free
Amount of items: 2
Items: 
Size: 738549 Color: 0
Size: 181631 Color: 1

Bin 42: 88326 of cap free
Amount of items: 2
Items: 
Size: 731690 Color: 0
Size: 179985 Color: 1

Bin 43: 98917 of cap free
Amount of items: 2
Items: 
Size: 584380 Color: 0
Size: 316704 Color: 1

Bin 44: 105718 of cap free
Amount of items: 2
Items: 
Size: 645374 Color: 1
Size: 248909 Color: 0

Bin 45: 113600 of cap free
Amount of items: 2
Items: 
Size: 754489 Color: 0
Size: 131912 Color: 1

Bin 46: 115767 of cap free
Amount of items: 2
Items: 
Size: 643119 Color: 1
Size: 241115 Color: 0

Bin 47: 192139 of cap free
Amount of items: 2
Items: 
Size: 437838 Color: 0
Size: 370024 Color: 1

Total size: 45366268
Total free space: 1633779

