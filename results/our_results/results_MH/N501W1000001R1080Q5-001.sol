Capicity Bin: 1000001
Lower Bound: 227

Bins used: 228
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 721417 Color: 0
Size: 145072 Color: 0
Size: 133512 Color: 1

Bin 2: 1 of cap free
Amount of items: 3
Items: 
Size: 729323 Color: 1
Size: 145280 Color: 3
Size: 125397 Color: 0

Bin 3: 2 of cap free
Amount of items: 3
Items: 
Size: 723138 Color: 4
Size: 166605 Color: 1
Size: 110256 Color: 2

Bin 4: 3 of cap free
Amount of items: 3
Items: 
Size: 459105 Color: 4
Size: 413910 Color: 1
Size: 126983 Color: 3

Bin 5: 7 of cap free
Amount of items: 3
Items: 
Size: 741393 Color: 3
Size: 155682 Color: 3
Size: 102919 Color: 1

Bin 6: 7 of cap free
Amount of items: 3
Items: 
Size: 758888 Color: 2
Size: 124866 Color: 2
Size: 116240 Color: 3

Bin 7: 13 of cap free
Amount of items: 2
Items: 
Size: 645616 Color: 2
Size: 354372 Color: 4

Bin 8: 14 of cap free
Amount of items: 3
Items: 
Size: 548452 Color: 2
Size: 307063 Color: 1
Size: 144472 Color: 0

Bin 9: 20 of cap free
Amount of items: 3
Items: 
Size: 456723 Color: 3
Size: 408593 Color: 1
Size: 134665 Color: 2

Bin 10: 23 of cap free
Amount of items: 2
Items: 
Size: 699276 Color: 0
Size: 300702 Color: 4

Bin 11: 29 of cap free
Amount of items: 3
Items: 
Size: 549687 Color: 2
Size: 308970 Color: 4
Size: 141315 Color: 4

Bin 12: 53 of cap free
Amount of items: 2
Items: 
Size: 518443 Color: 1
Size: 481505 Color: 0

Bin 13: 53 of cap free
Amount of items: 3
Items: 
Size: 457161 Color: 3
Size: 382940 Color: 1
Size: 159847 Color: 2

Bin 14: 57 of cap free
Amount of items: 3
Items: 
Size: 454424 Color: 0
Size: 428566 Color: 1
Size: 116954 Color: 4

Bin 15: 65 of cap free
Amount of items: 2
Items: 
Size: 634001 Color: 3
Size: 365935 Color: 1

Bin 16: 68 of cap free
Amount of items: 3
Items: 
Size: 641411 Color: 4
Size: 182601 Color: 4
Size: 175921 Color: 3

Bin 17: 74 of cap free
Amount of items: 3
Items: 
Size: 759329 Color: 2
Size: 125228 Color: 1
Size: 115370 Color: 4

Bin 18: 76 of cap free
Amount of items: 3
Items: 
Size: 641699 Color: 0
Size: 181754 Color: 0
Size: 176472 Color: 3

Bin 19: 82 of cap free
Amount of items: 3
Items: 
Size: 754663 Color: 1
Size: 143570 Color: 4
Size: 101686 Color: 3

Bin 20: 89 of cap free
Amount of items: 3
Items: 
Size: 723645 Color: 1
Size: 139955 Color: 3
Size: 136312 Color: 2

Bin 21: 90 of cap free
Amount of items: 3
Items: 
Size: 459389 Color: 3
Size: 278795 Color: 3
Size: 261727 Color: 0

Bin 22: 90 of cap free
Amount of items: 2
Items: 
Size: 533738 Color: 1
Size: 466173 Color: 3

Bin 23: 91 of cap free
Amount of items: 2
Items: 
Size: 643670 Color: 1
Size: 356240 Color: 3

Bin 24: 97 of cap free
Amount of items: 3
Items: 
Size: 739061 Color: 2
Size: 140664 Color: 1
Size: 120179 Color: 1

Bin 25: 103 of cap free
Amount of items: 2
Items: 
Size: 738407 Color: 2
Size: 261491 Color: 3

Bin 26: 128 of cap free
Amount of items: 3
Items: 
Size: 729699 Color: 3
Size: 156025 Color: 2
Size: 114149 Color: 4

Bin 27: 131 of cap free
Amount of items: 3
Items: 
Size: 453071 Color: 4
Size: 439853 Color: 2
Size: 106946 Color: 0

Bin 28: 156 of cap free
Amount of items: 2
Items: 
Size: 711778 Color: 3
Size: 288067 Color: 1

Bin 29: 183 of cap free
Amount of items: 3
Items: 
Size: 741402 Color: 1
Size: 144277 Color: 2
Size: 114139 Color: 3

Bin 30: 201 of cap free
Amount of items: 2
Items: 
Size: 558564 Color: 3
Size: 441236 Color: 0

Bin 31: 205 of cap free
Amount of items: 2
Items: 
Size: 752736 Color: 0
Size: 247060 Color: 1

Bin 32: 215 of cap free
Amount of items: 3
Items: 
Size: 727124 Color: 1
Size: 170515 Color: 1
Size: 102147 Color: 0

Bin 33: 216 of cap free
Amount of items: 2
Items: 
Size: 574593 Color: 4
Size: 425192 Color: 1

Bin 34: 222 of cap free
Amount of items: 3
Items: 
Size: 762913 Color: 3
Size: 121933 Color: 4
Size: 114933 Color: 2

Bin 35: 223 of cap free
Amount of items: 3
Items: 
Size: 758011 Color: 3
Size: 134976 Color: 3
Size: 106791 Color: 2

Bin 36: 242 of cap free
Amount of items: 2
Items: 
Size: 612608 Color: 3
Size: 387151 Color: 2

Bin 37: 247 of cap free
Amount of items: 2
Items: 
Size: 726100 Color: 1
Size: 273654 Color: 0

Bin 38: 257 of cap free
Amount of items: 3
Items: 
Size: 730120 Color: 3
Size: 158009 Color: 3
Size: 111615 Color: 2

Bin 39: 261 of cap free
Amount of items: 3
Items: 
Size: 641881 Color: 2
Size: 183737 Color: 1
Size: 174122 Color: 4

Bin 40: 281 of cap free
Amount of items: 2
Items: 
Size: 556820 Color: 1
Size: 442900 Color: 2

Bin 41: 286 of cap free
Amount of items: 3
Items: 
Size: 652481 Color: 1
Size: 177233 Color: 0
Size: 170001 Color: 4

Bin 42: 288 of cap free
Amount of items: 3
Items: 
Size: 661188 Color: 4
Size: 171247 Color: 2
Size: 167278 Color: 0

Bin 43: 298 of cap free
Amount of items: 2
Items: 
Size: 525672 Color: 1
Size: 474031 Color: 4

Bin 44: 337 of cap free
Amount of items: 3
Items: 
Size: 759145 Color: 1
Size: 138220 Color: 3
Size: 102299 Color: 4

Bin 45: 338 of cap free
Amount of items: 3
Items: 
Size: 453961 Color: 4
Size: 363265 Color: 2
Size: 182437 Color: 0

Bin 46: 350 of cap free
Amount of items: 2
Items: 
Size: 796859 Color: 4
Size: 202792 Color: 1

Bin 47: 365 of cap free
Amount of items: 2
Items: 
Size: 609153 Color: 4
Size: 390483 Color: 0

Bin 48: 369 of cap free
Amount of items: 3
Items: 
Size: 454564 Color: 1
Size: 391212 Color: 1
Size: 153856 Color: 3

Bin 49: 371 of cap free
Amount of items: 3
Items: 
Size: 641912 Color: 1
Size: 180563 Color: 3
Size: 177155 Color: 0

Bin 50: 392 of cap free
Amount of items: 3
Items: 
Size: 455727 Color: 0
Size: 393235 Color: 3
Size: 150647 Color: 0

Bin 51: 409 of cap free
Amount of items: 2
Items: 
Size: 589839 Color: 3
Size: 409753 Color: 4

Bin 52: 436 of cap free
Amount of items: 2
Items: 
Size: 554445 Color: 4
Size: 445120 Color: 3

Bin 53: 439 of cap free
Amount of items: 2
Items: 
Size: 739414 Color: 3
Size: 260148 Color: 4

Bin 54: 440 of cap free
Amount of items: 2
Items: 
Size: 765931 Color: 2
Size: 233630 Color: 3

Bin 55: 464 of cap free
Amount of items: 2
Items: 
Size: 718527 Color: 1
Size: 281010 Color: 2

Bin 56: 477 of cap free
Amount of items: 3
Items: 
Size: 728101 Color: 1
Size: 149190 Color: 2
Size: 122233 Color: 4

Bin 57: 483 of cap free
Amount of items: 2
Items: 
Size: 613474 Color: 1
Size: 386044 Color: 4

Bin 58: 486 of cap free
Amount of items: 2
Items: 
Size: 691725 Color: 1
Size: 307790 Color: 3

Bin 59: 495 of cap free
Amount of items: 2
Items: 
Size: 597672 Color: 3
Size: 401834 Color: 1

Bin 60: 501 of cap free
Amount of items: 2
Items: 
Size: 662865 Color: 0
Size: 336635 Color: 3

Bin 61: 514 of cap free
Amount of items: 2
Items: 
Size: 529442 Color: 4
Size: 470045 Color: 2

Bin 62: 515 of cap free
Amount of items: 2
Items: 
Size: 616226 Color: 3
Size: 383260 Color: 1

Bin 63: 520 of cap free
Amount of items: 2
Items: 
Size: 772597 Color: 3
Size: 226884 Color: 2

Bin 64: 522 of cap free
Amount of items: 2
Items: 
Size: 596659 Color: 4
Size: 402820 Color: 0

Bin 65: 524 of cap free
Amount of items: 2
Items: 
Size: 699147 Color: 3
Size: 300330 Color: 4

Bin 66: 553 of cap free
Amount of items: 2
Items: 
Size: 683461 Color: 1
Size: 315987 Color: 2

Bin 67: 562 of cap free
Amount of items: 2
Items: 
Size: 534429 Color: 0
Size: 465010 Color: 2

Bin 68: 565 of cap free
Amount of items: 2
Items: 
Size: 563710 Color: 1
Size: 435726 Color: 4

Bin 69: 583 of cap free
Amount of items: 2
Items: 
Size: 589368 Color: 2
Size: 410050 Color: 1

Bin 70: 588 of cap free
Amount of items: 2
Items: 
Size: 514721 Color: 3
Size: 484692 Color: 0

Bin 71: 588 of cap free
Amount of items: 2
Items: 
Size: 735444 Color: 1
Size: 263969 Color: 4

Bin 72: 595 of cap free
Amount of items: 2
Items: 
Size: 780658 Color: 1
Size: 218748 Color: 3

Bin 73: 597 of cap free
Amount of items: 2
Items: 
Size: 638146 Color: 1
Size: 361258 Color: 3

Bin 74: 616 of cap free
Amount of items: 2
Items: 
Size: 694995 Color: 1
Size: 304390 Color: 3

Bin 75: 625 of cap free
Amount of items: 2
Items: 
Size: 641166 Color: 2
Size: 358210 Color: 0

Bin 76: 650 of cap free
Amount of items: 2
Items: 
Size: 765011 Color: 2
Size: 234340 Color: 1

Bin 77: 670 of cap free
Amount of items: 3
Items: 
Size: 699458 Color: 1
Size: 152913 Color: 0
Size: 146960 Color: 4

Bin 78: 689 of cap free
Amount of items: 2
Items: 
Size: 727943 Color: 1
Size: 271369 Color: 2

Bin 79: 717 of cap free
Amount of items: 2
Items: 
Size: 711706 Color: 2
Size: 287578 Color: 4

Bin 80: 719 of cap free
Amount of items: 2
Items: 
Size: 665993 Color: 0
Size: 333289 Color: 3

Bin 81: 744 of cap free
Amount of items: 2
Items: 
Size: 518629 Color: 0
Size: 480628 Color: 3

Bin 82: 795 of cap free
Amount of items: 2
Items: 
Size: 613289 Color: 0
Size: 385917 Color: 3

Bin 83: 799 of cap free
Amount of items: 2
Items: 
Size: 582606 Color: 0
Size: 416596 Color: 3

Bin 84: 805 of cap free
Amount of items: 2
Items: 
Size: 755056 Color: 2
Size: 244140 Color: 0

Bin 85: 809 of cap free
Amount of items: 2
Items: 
Size: 787701 Color: 0
Size: 211491 Color: 2

Bin 86: 841 of cap free
Amount of items: 2
Items: 
Size: 542059 Color: 1
Size: 457101 Color: 4

Bin 87: 862 of cap free
Amount of items: 2
Items: 
Size: 527464 Color: 1
Size: 471675 Color: 0

Bin 88: 899 of cap free
Amount of items: 2
Items: 
Size: 599582 Color: 2
Size: 399520 Color: 4

Bin 89: 912 of cap free
Amount of items: 2
Items: 
Size: 549844 Color: 2
Size: 449245 Color: 0

Bin 90: 939 of cap free
Amount of items: 3
Items: 
Size: 453315 Color: 3
Size: 394943 Color: 1
Size: 150804 Color: 3

Bin 91: 939 of cap free
Amount of items: 2
Items: 
Size: 547530 Color: 1
Size: 451532 Color: 3

Bin 92: 951 of cap free
Amount of items: 2
Items: 
Size: 712678 Color: 1
Size: 286372 Color: 4

Bin 93: 983 of cap free
Amount of items: 2
Items: 
Size: 500259 Color: 0
Size: 498759 Color: 1

Bin 94: 985 of cap free
Amount of items: 2
Items: 
Size: 774705 Color: 4
Size: 224311 Color: 1

Bin 95: 1051 of cap free
Amount of items: 2
Items: 
Size: 579318 Color: 0
Size: 419632 Color: 4

Bin 96: 1145 of cap free
Amount of items: 3
Items: 
Size: 722143 Color: 2
Size: 168337 Color: 0
Size: 108376 Color: 4

Bin 97: 1148 of cap free
Amount of items: 3
Items: 
Size: 440470 Color: 4
Size: 311459 Color: 0
Size: 246924 Color: 0

Bin 98: 1163 of cap free
Amount of items: 2
Items: 
Size: 542353 Color: 1
Size: 456485 Color: 0

Bin 99: 1193 of cap free
Amount of items: 2
Items: 
Size: 675704 Color: 2
Size: 323104 Color: 4

Bin 100: 1208 of cap free
Amount of items: 2
Items: 
Size: 571639 Color: 3
Size: 427154 Color: 1

Bin 101: 1235 of cap free
Amount of items: 2
Items: 
Size: 611715 Color: 2
Size: 387051 Color: 3

Bin 102: 1236 of cap free
Amount of items: 2
Items: 
Size: 732920 Color: 2
Size: 265845 Color: 3

Bin 103: 1278 of cap free
Amount of items: 2
Items: 
Size: 531177 Color: 1
Size: 467546 Color: 3

Bin 104: 1285 of cap free
Amount of items: 2
Items: 
Size: 642655 Color: 2
Size: 356061 Color: 0

Bin 105: 1318 of cap free
Amount of items: 2
Items: 
Size: 527825 Color: 3
Size: 470858 Color: 2

Bin 106: 1323 of cap free
Amount of items: 2
Items: 
Size: 750724 Color: 2
Size: 247954 Color: 4

Bin 107: 1342 of cap free
Amount of items: 2
Items: 
Size: 574365 Color: 0
Size: 424294 Color: 1

Bin 108: 1347 of cap free
Amount of items: 2
Items: 
Size: 689424 Color: 4
Size: 309230 Color: 0

Bin 109: 1352 of cap free
Amount of items: 2
Items: 
Size: 633347 Color: 4
Size: 365302 Color: 3

Bin 110: 1365 of cap free
Amount of items: 2
Items: 
Size: 721572 Color: 4
Size: 277064 Color: 1

Bin 111: 1380 of cap free
Amount of items: 2
Items: 
Size: 643500 Color: 4
Size: 355121 Color: 3

Bin 112: 1404 of cap free
Amount of items: 2
Items: 
Size: 553653 Color: 2
Size: 444944 Color: 0

Bin 113: 1478 of cap free
Amount of items: 2
Items: 
Size: 670079 Color: 1
Size: 328444 Color: 2

Bin 114: 1499 of cap free
Amount of items: 2
Items: 
Size: 736748 Color: 0
Size: 261754 Color: 3

Bin 115: 1548 of cap free
Amount of items: 2
Items: 
Size: 502891 Color: 0
Size: 495562 Color: 1

Bin 116: 1564 of cap free
Amount of items: 2
Items: 
Size: 740866 Color: 1
Size: 257571 Color: 3

Bin 117: 1644 of cap free
Amount of items: 2
Items: 
Size: 567707 Color: 4
Size: 430650 Color: 2

Bin 118: 1700 of cap free
Amount of items: 2
Items: 
Size: 517954 Color: 2
Size: 480347 Color: 0

Bin 119: 1722 of cap free
Amount of items: 2
Items: 
Size: 756106 Color: 0
Size: 242173 Color: 2

Bin 120: 1746 of cap free
Amount of items: 2
Items: 
Size: 602587 Color: 4
Size: 395668 Color: 3

Bin 121: 1859 of cap free
Amount of items: 2
Items: 
Size: 600680 Color: 0
Size: 397462 Color: 2

Bin 122: 1892 of cap free
Amount of items: 3
Items: 
Size: 723980 Color: 4
Size: 164157 Color: 0
Size: 109972 Color: 0

Bin 123: 1945 of cap free
Amount of items: 2
Items: 
Size: 738388 Color: 0
Size: 259668 Color: 2

Bin 124: 2003 of cap free
Amount of items: 2
Items: 
Size: 600508 Color: 0
Size: 397490 Color: 2

Bin 125: 2058 of cap free
Amount of items: 2
Items: 
Size: 591427 Color: 2
Size: 406516 Color: 0

Bin 126: 2069 of cap free
Amount of items: 2
Items: 
Size: 520459 Color: 1
Size: 477473 Color: 2

Bin 127: 2098 of cap free
Amount of items: 2
Items: 
Size: 629773 Color: 1
Size: 368130 Color: 4

Bin 128: 2120 of cap free
Amount of items: 2
Items: 
Size: 638962 Color: 2
Size: 358919 Color: 0

Bin 129: 2120 of cap free
Amount of items: 2
Items: 
Size: 655379 Color: 2
Size: 342502 Color: 3

Bin 130: 2136 of cap free
Amount of items: 2
Items: 
Size: 742651 Color: 2
Size: 255214 Color: 0

Bin 131: 2162 of cap free
Amount of items: 2
Items: 
Size: 594494 Color: 4
Size: 403345 Color: 2

Bin 132: 2392 of cap free
Amount of items: 2
Items: 
Size: 691333 Color: 3
Size: 306276 Color: 0

Bin 133: 2458 of cap free
Amount of items: 2
Items: 
Size: 646993 Color: 0
Size: 350550 Color: 2

Bin 134: 2464 of cap free
Amount of items: 2
Items: 
Size: 588117 Color: 3
Size: 409420 Color: 1

Bin 135: 2525 of cap free
Amount of items: 2
Items: 
Size: 693465 Color: 0
Size: 304011 Color: 3

Bin 136: 2655 of cap free
Amount of items: 2
Items: 
Size: 516057 Color: 4
Size: 481289 Color: 2

Bin 137: 2791 of cap free
Amount of items: 2
Items: 
Size: 567034 Color: 3
Size: 430176 Color: 2

Bin 138: 2804 of cap free
Amount of items: 2
Items: 
Size: 695039 Color: 3
Size: 302158 Color: 2

Bin 139: 2954 of cap free
Amount of items: 2
Items: 
Size: 746824 Color: 4
Size: 250223 Color: 1

Bin 140: 3082 of cap free
Amount of items: 2
Items: 
Size: 551423 Color: 0
Size: 445496 Color: 1

Bin 141: 3093 of cap free
Amount of items: 2
Items: 
Size: 504542 Color: 4
Size: 492366 Color: 0

Bin 142: 3102 of cap free
Amount of items: 2
Items: 
Size: 569423 Color: 4
Size: 427476 Color: 0

Bin 143: 3173 of cap free
Amount of items: 2
Items: 
Size: 743954 Color: 1
Size: 252874 Color: 2

Bin 144: 3227 of cap free
Amount of items: 2
Items: 
Size: 789719 Color: 3
Size: 207055 Color: 1

Bin 145: 3262 of cap free
Amount of items: 3
Items: 
Size: 567076 Color: 3
Size: 288595 Color: 0
Size: 141068 Color: 1

Bin 146: 3284 of cap free
Amount of items: 2
Items: 
Size: 532279 Color: 0
Size: 464438 Color: 2

Bin 147: 3312 of cap free
Amount of items: 2
Items: 
Size: 716987 Color: 2
Size: 279702 Color: 1

Bin 148: 3327 of cap free
Amount of items: 2
Items: 
Size: 503014 Color: 4
Size: 493660 Color: 0

Bin 149: 3494 of cap free
Amount of items: 2
Items: 
Size: 767390 Color: 2
Size: 229117 Color: 0

Bin 150: 3549 of cap free
Amount of items: 2
Items: 
Size: 714898 Color: 1
Size: 281554 Color: 2

Bin 151: 3631 of cap free
Amount of items: 2
Items: 
Size: 630303 Color: 2
Size: 366067 Color: 3

Bin 152: 3662 of cap free
Amount of items: 2
Items: 
Size: 578152 Color: 0
Size: 418187 Color: 4

Bin 153: 3709 of cap free
Amount of items: 2
Items: 
Size: 714098 Color: 3
Size: 282194 Color: 2

Bin 154: 3761 of cap free
Amount of items: 2
Items: 
Size: 541030 Color: 3
Size: 455210 Color: 0

Bin 155: 3798 of cap free
Amount of items: 2
Items: 
Size: 704371 Color: 0
Size: 291832 Color: 1

Bin 156: 3907 of cap free
Amount of items: 2
Items: 
Size: 534881 Color: 2
Size: 461213 Color: 3

Bin 157: 4024 of cap free
Amount of items: 2
Items: 
Size: 655322 Color: 3
Size: 340655 Color: 2

Bin 158: 4084 of cap free
Amount of items: 2
Items: 
Size: 694745 Color: 2
Size: 301172 Color: 4

Bin 159: 4178 of cap free
Amount of items: 2
Items: 
Size: 505916 Color: 0
Size: 489907 Color: 3

Bin 160: 4297 of cap free
Amount of items: 2
Items: 
Size: 700659 Color: 2
Size: 295045 Color: 0

Bin 161: 4382 of cap free
Amount of items: 2
Items: 
Size: 672948 Color: 0
Size: 322671 Color: 4

Bin 162: 4394 of cap free
Amount of items: 2
Items: 
Size: 505255 Color: 0
Size: 490352 Color: 4

Bin 163: 4587 of cap free
Amount of items: 2
Items: 
Size: 598804 Color: 2
Size: 396610 Color: 3

Bin 164: 4849 of cap free
Amount of items: 2
Items: 
Size: 775329 Color: 1
Size: 219823 Color: 2

Bin 165: 4967 of cap free
Amount of items: 2
Items: 
Size: 784352 Color: 0
Size: 210682 Color: 3

Bin 166: 5157 of cap free
Amount of items: 2
Items: 
Size: 614416 Color: 2
Size: 380428 Color: 4

Bin 167: 5246 of cap free
Amount of items: 2
Items: 
Size: 706136 Color: 4
Size: 288619 Color: 1

Bin 168: 5251 of cap free
Amount of items: 2
Items: 
Size: 510800 Color: 3
Size: 483950 Color: 0

Bin 169: 5298 of cap free
Amount of items: 2
Items: 
Size: 731705 Color: 1
Size: 262998 Color: 4

Bin 170: 5303 of cap free
Amount of items: 2
Items: 
Size: 563213 Color: 3
Size: 431485 Color: 2

Bin 171: 5405 of cap free
Amount of items: 2
Items: 
Size: 652688 Color: 1
Size: 341908 Color: 3

Bin 172: 5438 of cap free
Amount of items: 2
Items: 
Size: 517672 Color: 2
Size: 476891 Color: 3

Bin 173: 5508 of cap free
Amount of items: 2
Items: 
Size: 646519 Color: 2
Size: 347974 Color: 1

Bin 174: 5671 of cap free
Amount of items: 2
Items: 
Size: 745921 Color: 2
Size: 248409 Color: 3

Bin 175: 5726 of cap free
Amount of items: 2
Items: 
Size: 773357 Color: 0
Size: 220918 Color: 1

Bin 176: 5797 of cap free
Amount of items: 2
Items: 
Size: 628750 Color: 2
Size: 365454 Color: 1

Bin 177: 5833 of cap free
Amount of items: 2
Items: 
Size: 579272 Color: 1
Size: 414896 Color: 0

Bin 178: 5909 of cap free
Amount of items: 2
Items: 
Size: 525106 Color: 2
Size: 468986 Color: 4

Bin 179: 6065 of cap free
Amount of items: 2
Items: 
Size: 675189 Color: 4
Size: 318747 Color: 0

Bin 180: 6268 of cap free
Amount of items: 2
Items: 
Size: 646120 Color: 4
Size: 347613 Color: 2

Bin 181: 6408 of cap free
Amount of items: 2
Items: 
Size: 799777 Color: 0
Size: 193816 Color: 1

Bin 182: 6441 of cap free
Amount of items: 2
Items: 
Size: 668727 Color: 1
Size: 324833 Color: 2

Bin 183: 6470 of cap free
Amount of items: 2
Items: 
Size: 622204 Color: 4
Size: 371327 Color: 3

Bin 184: 6477 of cap free
Amount of items: 2
Items: 
Size: 653164 Color: 3
Size: 340360 Color: 1

Bin 185: 6552 of cap free
Amount of items: 2
Items: 
Size: 653165 Color: 3
Size: 340284 Color: 2

Bin 186: 6617 of cap free
Amount of items: 2
Items: 
Size: 792718 Color: 1
Size: 200666 Color: 3

Bin 187: 6886 of cap free
Amount of items: 2
Items: 
Size: 653125 Color: 3
Size: 339990 Color: 4

Bin 188: 6914 of cap free
Amount of items: 2
Items: 
Size: 622574 Color: 1
Size: 370513 Color: 4

Bin 189: 7031 of cap free
Amount of items: 2
Items: 
Size: 759929 Color: 3
Size: 233041 Color: 2

Bin 190: 7117 of cap free
Amount of items: 2
Items: 
Size: 704202 Color: 4
Size: 288682 Color: 1

Bin 191: 7133 of cap free
Amount of items: 2
Items: 
Size: 703017 Color: 3
Size: 289851 Color: 2

Bin 192: 7370 of cap free
Amount of items: 2
Items: 
Size: 550412 Color: 0
Size: 442219 Color: 2

Bin 193: 7540 of cap free
Amount of items: 2
Items: 
Size: 772547 Color: 2
Size: 219914 Color: 0

Bin 194: 7895 of cap free
Amount of items: 2
Items: 
Size: 664596 Color: 2
Size: 327510 Color: 1

Bin 195: 8046 of cap free
Amount of items: 3
Items: 
Size: 699174 Color: 0
Size: 155156 Color: 1
Size: 137625 Color: 3

Bin 196: 8724 of cap free
Amount of items: 2
Items: 
Size: 786684 Color: 3
Size: 204593 Color: 1

Bin 197: 9003 of cap free
Amount of items: 2
Items: 
Size: 672694 Color: 2
Size: 318304 Color: 4

Bin 198: 9025 of cap free
Amount of items: 2
Items: 
Size: 627036 Color: 1
Size: 363940 Color: 4

Bin 199: 9306 of cap free
Amount of items: 2
Items: 
Size: 673246 Color: 2
Size: 317449 Color: 1

Bin 200: 10016 of cap free
Amount of items: 2
Items: 
Size: 759847 Color: 1
Size: 230138 Color: 3

Bin 201: 10167 of cap free
Amount of items: 3
Items: 
Size: 495025 Color: 1
Size: 323211 Color: 3
Size: 171598 Color: 2

Bin 202: 11105 of cap free
Amount of items: 2
Items: 
Size: 622950 Color: 4
Size: 365946 Color: 1

Bin 203: 11127 of cap free
Amount of items: 2
Items: 
Size: 549051 Color: 2
Size: 439823 Color: 3

Bin 204: 11717 of cap free
Amount of items: 2
Items: 
Size: 510164 Color: 0
Size: 478120 Color: 4

Bin 205: 12051 of cap free
Amount of items: 2
Items: 
Size: 669534 Color: 3
Size: 318416 Color: 4

Bin 206: 12057 of cap free
Amount of items: 2
Items: 
Size: 788733 Color: 3
Size: 199211 Color: 0

Bin 207: 13362 of cap free
Amount of items: 2
Items: 
Size: 793478 Color: 3
Size: 193161 Color: 2

Bin 208: 14950 of cap free
Amount of items: 2
Items: 
Size: 502007 Color: 3
Size: 483044 Color: 0

Bin 209: 15204 of cap free
Amount of items: 2
Items: 
Size: 790427 Color: 4
Size: 194370 Color: 0

Bin 210: 15444 of cap free
Amount of items: 2
Items: 
Size: 790502 Color: 3
Size: 194055 Color: 0

Bin 211: 16094 of cap free
Amount of items: 2
Items: 
Size: 782863 Color: 2
Size: 201044 Color: 1

Bin 212: 17547 of cap free
Amount of items: 2
Items: 
Size: 620598 Color: 0
Size: 361856 Color: 4

Bin 213: 18493 of cap free
Amount of items: 2
Items: 
Size: 505159 Color: 0
Size: 476349 Color: 1

Bin 214: 20435 of cap free
Amount of items: 2
Items: 
Size: 504472 Color: 4
Size: 475094 Color: 0

Bin 215: 21630 of cap free
Amount of items: 2
Items: 
Size: 502207 Color: 4
Size: 476164 Color: 2

Bin 216: 22693 of cap free
Amount of items: 3
Items: 
Size: 563545 Color: 0
Size: 281583 Color: 2
Size: 132180 Color: 1

Bin 217: 23111 of cap free
Amount of items: 3
Items: 
Size: 676900 Color: 2
Size: 155606 Color: 1
Size: 144384 Color: 4

Bin 218: 26215 of cap free
Amount of items: 2
Items: 
Size: 501428 Color: 2
Size: 472358 Color: 1

Bin 219: 29011 of cap free
Amount of items: 2
Items: 
Size: 782133 Color: 1
Size: 188857 Color: 3

Bin 220: 32008 of cap free
Amount of items: 2
Items: 
Size: 639139 Color: 1
Size: 328854 Color: 0

Bin 221: 32432 of cap free
Amount of items: 2
Items: 
Size: 758627 Color: 1
Size: 208942 Color: 3

Bin 222: 38984 of cap free
Amount of items: 2
Items: 
Size: 771555 Color: 0
Size: 189462 Color: 3

Bin 223: 40540 of cap free
Amount of items: 2
Items: 
Size: 781933 Color: 3
Size: 177528 Color: 1

Bin 224: 57851 of cap free
Amount of items: 2
Items: 
Size: 504985 Color: 2
Size: 437165 Color: 0

Bin 225: 58531 of cap free
Amount of items: 3
Items: 
Size: 619674 Color: 1
Size: 180316 Color: 3
Size: 141480 Color: 0

Bin 226: 58669 of cap free
Amount of items: 2
Items: 
Size: 662008 Color: 4
Size: 279324 Color: 0

Bin 227: 60329 of cap free
Amount of items: 2
Items: 
Size: 577073 Color: 2
Size: 362599 Color: 1

Bin 228: 62002 of cap free
Amount of items: 2
Items: 
Size: 729405 Color: 4
Size: 208594 Color: 3

Total size: 226787735
Total free space: 1212493

