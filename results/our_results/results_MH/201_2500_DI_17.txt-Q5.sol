Capicity Bin: 1996
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 804 Color: 2
Size: 506 Color: 1
Size: 286 Color: 1
Size: 220 Color: 1
Size: 88 Color: 3
Size: 76 Color: 2
Size: 16 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 4
Size: 182 Color: 0
Size: 36 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 4
Size: 294 Color: 2
Size: 48 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 3
Size: 262 Color: 0
Size: 52 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 4
Size: 354 Color: 4
Size: 68 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1003 Color: 4
Size: 829 Color: 4
Size: 164 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 3
Size: 369 Color: 1
Size: 52 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1610 Color: 2
Size: 322 Color: 4
Size: 64 Color: 0

Bin 9: 0 of cap free
Amount of items: 4
Items: 
Size: 1380 Color: 0
Size: 460 Color: 3
Size: 84 Color: 0
Size: 72 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 4
Size: 220 Color: 2
Size: 70 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1145 Color: 0
Size: 767 Color: 0
Size: 84 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1252 Color: 0
Size: 676 Color: 3
Size: 68 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1525 Color: 3
Size: 393 Color: 0
Size: 78 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 3
Size: 205 Color: 2
Size: 40 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 4
Size: 478 Color: 4
Size: 92 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 2
Size: 451 Color: 4
Size: 88 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 2
Size: 239 Color: 3
Size: 46 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 3
Size: 230 Color: 1
Size: 44 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 2
Size: 340 Color: 3
Size: 10 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 3
Size: 778 Color: 3
Size: 152 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 0
Size: 401 Color: 3
Size: 78 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 3
Size: 511 Color: 1
Size: 102 Color: 2

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 1602 Color: 4
Size: 330 Color: 2
Size: 56 Color: 2
Size: 8 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1141 Color: 1
Size: 713 Color: 3
Size: 142 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 0
Size: 189 Color: 3
Size: 36 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 999 Color: 0
Size: 831 Color: 0
Size: 166 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 4
Size: 246 Color: 2
Size: 36 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 4
Size: 426 Color: 2
Size: 108 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 1
Size: 416 Color: 2
Size: 242 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1255 Color: 2
Size: 619 Color: 0
Size: 122 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 4
Size: 446 Color: 2
Size: 64 Color: 1

Bin 32: 0 of cap free
Amount of items: 4
Items: 
Size: 1617 Color: 3
Size: 351 Color: 4
Size: 20 Color: 3
Size: 8 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1162 Color: 4
Size: 698 Color: 3
Size: 136 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1346 Color: 1
Size: 542 Color: 1
Size: 108 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 3
Size: 211 Color: 1
Size: 42 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 1
Size: 410 Color: 4
Size: 64 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 0
Size: 281 Color: 3
Size: 56 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 0
Size: 550 Color: 2
Size: 56 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1607 Color: 3
Size: 325 Color: 4
Size: 64 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1387 Color: 0
Size: 509 Color: 4
Size: 100 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 4
Size: 734 Color: 4
Size: 144 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 4
Size: 190 Color: 3
Size: 36 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1695 Color: 3
Size: 251 Color: 4
Size: 50 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1259 Color: 2
Size: 615 Color: 1
Size: 122 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 4
Size: 663 Color: 1
Size: 132 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1533 Color: 4
Size: 387 Color: 1
Size: 76 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1198 Color: 4
Size: 666 Color: 4
Size: 132 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 1
Size: 178 Color: 3
Size: 32 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 1
Size: 610 Color: 3
Size: 120 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 1
Size: 515 Color: 1
Size: 102 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 2
Size: 197 Color: 1
Size: 38 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1002 Color: 2
Size: 830 Color: 3
Size: 164 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 4
Size: 397 Color: 1
Size: 78 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1137 Color: 0
Size: 717 Color: 3
Size: 142 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 1
Size: 591 Color: 1
Size: 118 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 2
Size: 303 Color: 0
Size: 60 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 1
Size: 449 Color: 2
Size: 88 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 4
Size: 308 Color: 0
Size: 100 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 3
Size: 589 Color: 3
Size: 116 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 2
Size: 390 Color: 0
Size: 76 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 1
Size: 263 Color: 1
Size: 52 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 2
Size: 186 Color: 4
Size: 36 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 1
Size: 318 Color: 2
Size: 60 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 4
Size: 198 Color: 0
Size: 36 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 1
Size: 366 Color: 0
Size: 72 Color: 3

Total size: 129740
Total free space: 0

