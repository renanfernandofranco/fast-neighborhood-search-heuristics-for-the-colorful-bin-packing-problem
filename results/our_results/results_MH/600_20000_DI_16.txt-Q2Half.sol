Capicity Bin: 16336
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8169 Color: 1
Size: 6807 Color: 1
Size: 1360 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 8172 Color: 1
Size: 5764 Color: 1
Size: 992 Color: 0
Size: 896 Color: 1
Size: 512 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8184 Color: 1
Size: 6808 Color: 1
Size: 1344 Color: 0

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 8171 Color: 1
Size: 3592 Color: 1
Size: 2901 Color: 1
Size: 1352 Color: 0
Size: 320 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8170 Color: 1
Size: 6806 Color: 1
Size: 1360 Color: 0

Bin 6: 0 of cap free
Amount of items: 5
Items: 
Size: 8212 Color: 1
Size: 3120 Color: 0
Size: 2713 Color: 1
Size: 1651 Color: 1
Size: 640 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 8174 Color: 1
Size: 6802 Color: 1
Size: 1360 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 1
Size: 6804 Color: 1
Size: 1352 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8228 Color: 1
Size: 6764 Color: 1
Size: 1344 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8680 Color: 1
Size: 6788 Color: 1
Size: 868 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 9634 Color: 1
Size: 5586 Color: 1
Size: 1116 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 9682 Color: 1
Size: 5546 Color: 1
Size: 1108 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 9720 Color: 1
Size: 5832 Color: 1
Size: 784 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9867 Color: 1
Size: 5391 Color: 1
Size: 1078 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 9895 Color: 1
Size: 5369 Color: 1
Size: 1072 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 10300 Color: 1
Size: 5036 Color: 1
Size: 1000 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 10328 Color: 1
Size: 5016 Color: 1
Size: 992 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 10364 Color: 1
Size: 4980 Color: 1
Size: 992 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 10549 Color: 1
Size: 4823 Color: 1
Size: 964 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 10718 Color: 1
Size: 5204 Color: 1
Size: 414 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 10728 Color: 1
Size: 5240 Color: 1
Size: 368 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 10787 Color: 1
Size: 4625 Color: 1
Size: 924 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 10808 Color: 1
Size: 4680 Color: 1
Size: 848 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11128 Color: 1
Size: 4344 Color: 1
Size: 864 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 1
Size: 4300 Color: 1
Size: 872 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11180 Color: 1
Size: 4276 Color: 1
Size: 880 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 1
Size: 3964 Color: 1
Size: 1160 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11315 Color: 1
Size: 4185 Color: 1
Size: 836 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11544 Color: 1
Size: 4008 Color: 1
Size: 784 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11589 Color: 1
Size: 3957 Color: 1
Size: 790 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11637 Color: 1
Size: 3917 Color: 1
Size: 782 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11669 Color: 1
Size: 3891 Color: 1
Size: 776 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11800 Color: 1
Size: 3724 Color: 1
Size: 812 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11884 Color: 1
Size: 3716 Color: 1
Size: 736 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11960 Color: 1
Size: 4008 Color: 1
Size: 368 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11978 Color: 1
Size: 3634 Color: 1
Size: 724 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 1
Size: 3784 Color: 1
Size: 512 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12049 Color: 1
Size: 3573 Color: 1
Size: 714 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12129 Color: 1
Size: 3507 Color: 1
Size: 700 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12374 Color: 1
Size: 2810 Color: 1
Size: 1152 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 1
Size: 3268 Color: 1
Size: 648 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12436 Color: 1
Size: 3252 Color: 1
Size: 648 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12513 Color: 1
Size: 2815 Color: 1
Size: 1008 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12558 Color: 1
Size: 3442 Color: 1
Size: 336 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12718 Color: 1
Size: 3018 Color: 1
Size: 600 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12753 Color: 1
Size: 2987 Color: 1
Size: 596 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 1
Size: 3240 Color: 1
Size: 288 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12858 Color: 1
Size: 2902 Color: 1
Size: 576 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 1
Size: 2888 Color: 1
Size: 576 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12892 Color: 1
Size: 2804 Color: 1
Size: 640 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 1
Size: 2660 Color: 1
Size: 704 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13001 Color: 1
Size: 2781 Color: 1
Size: 554 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13033 Color: 1
Size: 2753 Color: 1
Size: 550 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13038 Color: 1
Size: 2648 Color: 1
Size: 650 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13054 Color: 1
Size: 2738 Color: 1
Size: 544 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13085 Color: 1
Size: 2711 Color: 1
Size: 540 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 1
Size: 2648 Color: 1
Size: 528 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 1
Size: 2876 Color: 1
Size: 284 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13229 Color: 1
Size: 2673 Color: 1
Size: 434 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13258 Color: 1
Size: 2566 Color: 1
Size: 512 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 1
Size: 2564 Color: 1
Size: 504 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13277 Color: 1
Size: 1751 Color: 1
Size: 1308 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 1
Size: 2452 Color: 1
Size: 600 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 1
Size: 2548 Color: 1
Size: 454 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 1
Size: 2372 Color: 1
Size: 628 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13350 Color: 1
Size: 2490 Color: 1
Size: 496 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13419 Color: 1
Size: 2275 Color: 1
Size: 642 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13459 Color: 1
Size: 2329 Color: 1
Size: 548 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13479 Color: 1
Size: 2381 Color: 1
Size: 476 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13480 Color: 1
Size: 2568 Color: 1
Size: 288 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13492 Color: 1
Size: 2364 Color: 1
Size: 480 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13508 Color: 1
Size: 2248 Color: 1
Size: 580 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13566 Color: 1
Size: 2310 Color: 1
Size: 460 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13607 Color: 1
Size: 2077 Color: 1
Size: 652 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13630 Color: 1
Size: 2258 Color: 1
Size: 448 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13639 Color: 1
Size: 2261 Color: 1
Size: 436 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 1
Size: 2392 Color: 1
Size: 288 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13727 Color: 1
Size: 2175 Color: 1
Size: 434 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 1
Size: 2296 Color: 1
Size: 304 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13750 Color: 1
Size: 2158 Color: 1
Size: 428 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13780 Color: 1
Size: 2132 Color: 1
Size: 424 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13782 Color: 1
Size: 2074 Color: 1
Size: 480 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 1
Size: 2092 Color: 1
Size: 428 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 13824 Color: 1
Size: 2432 Color: 1
Size: 80 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 1
Size: 2096 Color: 1
Size: 412 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13839 Color: 1
Size: 2081 Color: 1
Size: 416 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13845 Color: 1
Size: 1915 Color: 1
Size: 576 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 1
Size: 2040 Color: 1
Size: 400 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13991 Color: 1
Size: 1955 Color: 1
Size: 390 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 14012 Color: 1
Size: 1908 Color: 1
Size: 416 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 14039 Color: 1
Size: 1905 Color: 1
Size: 392 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 1
Size: 1716 Color: 1
Size: 568 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 14058 Color: 1
Size: 1902 Color: 1
Size: 376 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 1
Size: 1898 Color: 1
Size: 376 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14100 Color: 1
Size: 1868 Color: 1
Size: 368 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 1
Size: 1794 Color: 1
Size: 432 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 1
Size: 1851 Color: 1
Size: 370 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 1
Size: 1928 Color: 1
Size: 288 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 14159 Color: 1
Size: 1815 Color: 1
Size: 362 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14186 Color: 1
Size: 1858 Color: 1
Size: 292 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 1
Size: 1784 Color: 1
Size: 352 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 14227 Color: 1
Size: 1759 Color: 1
Size: 350 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 14235 Color: 1
Size: 1749 Color: 1
Size: 352 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 14252 Color: 1
Size: 1612 Color: 1
Size: 472 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 14280 Color: 1
Size: 1494 Color: 1
Size: 562 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14284 Color: 1
Size: 1678 Color: 1
Size: 374 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14319 Color: 1
Size: 1681 Color: 1
Size: 336 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14320 Color: 1
Size: 1672 Color: 1
Size: 344 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14322 Color: 1
Size: 1682 Color: 1
Size: 332 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 1
Size: 1600 Color: 1
Size: 392 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14378 Color: 1
Size: 1610 Color: 1
Size: 348 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 1
Size: 1740 Color: 1
Size: 192 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14406 Color: 1
Size: 1580 Color: 1
Size: 350 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14410 Color: 1
Size: 1606 Color: 1
Size: 320 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14416 Color: 1
Size: 1696 Color: 1
Size: 224 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14439 Color: 1
Size: 1581 Color: 1
Size: 316 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14444 Color: 1
Size: 1528 Color: 1
Size: 364 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 1
Size: 1556 Color: 1
Size: 304 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14511 Color: 1
Size: 1521 Color: 1
Size: 304 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 1
Size: 1782 Color: 1
Size: 40 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14520 Color: 1
Size: 1460 Color: 1
Size: 356 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14546 Color: 1
Size: 1458 Color: 1
Size: 332 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14561 Color: 1
Size: 1481 Color: 1
Size: 294 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14562 Color: 1
Size: 1414 Color: 1
Size: 360 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14569 Color: 1
Size: 1473 Color: 1
Size: 294 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14580 Color: 1
Size: 1426 Color: 1
Size: 330 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14588 Color: 1
Size: 1468 Color: 1
Size: 280 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 1
Size: 1546 Color: 1
Size: 200 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 1
Size: 1448 Color: 1
Size: 288 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14626 Color: 1
Size: 1328 Color: 1
Size: 382 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14642 Color: 1
Size: 1562 Color: 1
Size: 132 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 14674 Color: 1
Size: 1386 Color: 1
Size: 276 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 14680 Color: 1
Size: 1416 Color: 1
Size: 240 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 1
Size: 1380 Color: 1
Size: 272 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 14692 Color: 1
Size: 1372 Color: 1
Size: 272 Color: 0

Bin 136: 1 of cap free
Amount of items: 3
Items: 
Size: 10056 Color: 1
Size: 5551 Color: 1
Size: 728 Color: 0

Bin 137: 1 of cap free
Amount of items: 3
Items: 
Size: 12097 Color: 1
Size: 3302 Color: 1
Size: 936 Color: 0

Bin 138: 1 of cap free
Amount of items: 3
Items: 
Size: 12422 Color: 1
Size: 3253 Color: 1
Size: 660 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 12433 Color: 1
Size: 3150 Color: 1
Size: 752 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 12468 Color: 1
Size: 3213 Color: 1
Size: 654 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 13081 Color: 1
Size: 2750 Color: 1
Size: 504 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 13104 Color: 1
Size: 2689 Color: 1
Size: 542 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 1
Size: 2551 Color: 1
Size: 528 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 1
Size: 2315 Color: 1
Size: 704 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 13396 Color: 1
Size: 2431 Color: 1
Size: 508 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 13447 Color: 1
Size: 2504 Color: 1
Size: 384 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 13559 Color: 1
Size: 2288 Color: 1
Size: 488 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 13759 Color: 1
Size: 2168 Color: 1
Size: 408 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 13882 Color: 1
Size: 1967 Color: 1
Size: 486 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 13897 Color: 1
Size: 2130 Color: 1
Size: 308 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 13977 Color: 1
Size: 1982 Color: 1
Size: 376 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 1
Size: 1879 Color: 1
Size: 432 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 14083 Color: 1
Size: 1940 Color: 1
Size: 312 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 14138 Color: 1
Size: 2149 Color: 1
Size: 48 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 14239 Color: 1
Size: 1634 Color: 1
Size: 462 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 14355 Color: 1
Size: 1720 Color: 1
Size: 260 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 14391 Color: 1
Size: 1834 Color: 1
Size: 110 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 14469 Color: 1
Size: 1482 Color: 1
Size: 384 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 14482 Color: 1
Size: 1557 Color: 1
Size: 296 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 9356 Color: 1
Size: 5868 Color: 1
Size: 1110 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 11028 Color: 1
Size: 4682 Color: 1
Size: 624 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 11084 Color: 1
Size: 3898 Color: 1
Size: 1352 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 11090 Color: 1
Size: 4316 Color: 1
Size: 928 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 1
Size: 4616 Color: 1
Size: 256 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 1
Size: 3542 Color: 1
Size: 336 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 12481 Color: 1
Size: 3533 Color: 1
Size: 320 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 12606 Color: 1
Size: 2952 Color: 1
Size: 776 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 13049 Color: 1
Size: 2821 Color: 1
Size: 464 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 13389 Color: 1
Size: 2399 Color: 1
Size: 546 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 13442 Color: 1
Size: 2414 Color: 1
Size: 478 Color: 0

Bin 171: 2 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 1
Size: 1848 Color: 1
Size: 284 Color: 0

Bin 172: 2 of cap free
Amount of items: 3
Items: 
Size: 14326 Color: 1
Size: 1264 Color: 0
Size: 744 Color: 0

Bin 173: 3 of cap free
Amount of items: 3
Items: 
Size: 9202 Color: 1
Size: 5963 Color: 1
Size: 1168 Color: 0

Bin 174: 3 of cap free
Amount of items: 3
Items: 
Size: 11662 Color: 1
Size: 4351 Color: 1
Size: 320 Color: 0

Bin 175: 3 of cap free
Amount of items: 3
Items: 
Size: 9181 Color: 1
Size: 6728 Color: 1
Size: 424 Color: 0

Bin 176: 4 of cap free
Amount of items: 3
Items: 
Size: 10274 Color: 1
Size: 5562 Color: 1
Size: 496 Color: 0

Bin 177: 4 of cap free
Amount of items: 3
Items: 
Size: 13962 Color: 1
Size: 2046 Color: 1
Size: 324 Color: 0

Bin 178: 5 of cap free
Amount of items: 3
Items: 
Size: 9471 Color: 1
Size: 5820 Color: 1
Size: 1040 Color: 0

Bin 179: 5 of cap free
Amount of items: 3
Items: 
Size: 9698 Color: 1
Size: 6265 Color: 1
Size: 368 Color: 0

Bin 180: 5 of cap free
Amount of items: 3
Items: 
Size: 11117 Color: 1
Size: 5054 Color: 1
Size: 160 Color: 0

Bin 181: 6 of cap free
Amount of items: 3
Items: 
Size: 12953 Color: 1
Size: 2741 Color: 1
Size: 636 Color: 0

Bin 182: 6 of cap free
Amount of items: 3
Items: 
Size: 9666 Color: 1
Size: 6354 Color: 1
Size: 310 Color: 0

Bin 183: 6 of cap free
Amount of items: 3
Items: 
Size: 12855 Color: 1
Size: 3187 Color: 1
Size: 288 Color: 0

Bin 184: 8 of cap free
Amount of items: 3
Items: 
Size: 8264 Color: 1
Size: 7152 Color: 1
Size: 912 Color: 0

Bin 185: 9 of cap free
Amount of items: 3
Items: 
Size: 9675 Color: 1
Size: 5946 Color: 1
Size: 706 Color: 0

Bin 186: 10 of cap free
Amount of items: 3
Items: 
Size: 9234 Color: 1
Size: 6772 Color: 1
Size: 320 Color: 0

Bin 187: 13 of cap free
Amount of items: 3
Items: 
Size: 10703 Color: 1
Size: 4428 Color: 1
Size: 1192 Color: 0

Bin 188: 14 of cap free
Amount of items: 3
Items: 
Size: 11844 Color: 1
Size: 4062 Color: 1
Size: 416 Color: 0

Bin 189: 18 of cap free
Amount of items: 3
Items: 
Size: 11868 Color: 1
Size: 3262 Color: 1
Size: 1188 Color: 0

Bin 190: 19 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 1
Size: 6549 Color: 1
Size: 752 Color: 0

Bin 191: 26 of cap free
Amount of items: 3
Items: 
Size: 8479 Color: 1
Size: 7511 Color: 1
Size: 320 Color: 0

Bin 192: 34 of cap free
Amount of items: 3
Items: 
Size: 13850 Color: 1
Size: 1344 Color: 0
Size: 1108 Color: 0

Bin 193: 40 of cap free
Amount of items: 3
Items: 
Size: 8196 Color: 1
Size: 7540 Color: 1
Size: 560 Color: 0

Bin 194: 60 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 1
Size: 6392 Color: 1
Size: 464 Color: 0

Bin 195: 76 of cap free
Amount of items: 3
Items: 
Size: 9300 Color: 1
Size: 6104 Color: 1
Size: 856 Color: 0

Bin 196: 162 of cap free
Amount of items: 3
Items: 
Size: 12090 Color: 1
Size: 3228 Color: 1
Size: 856 Color: 0

Bin 197: 192 of cap free
Amount of items: 3
Items: 
Size: 8080 Color: 1
Size: 6064 Color: 1
Size: 2000 Color: 0

Bin 198: 3593 of cap free
Amount of items: 3
Items: 
Size: 6374 Color: 1
Size: 5153 Color: 1
Size: 1216 Color: 0

Bin 199: 11962 of cap free
Amount of items: 1
Items: 
Size: 4374 Color: 1

Total size: 3234528
Total free space: 16336

