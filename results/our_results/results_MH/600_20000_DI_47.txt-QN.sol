Capicity Bin: 15200
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 10716 Color: 462
Size: 4228 Color: 356
Size: 256 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10978 Color: 469
Size: 3602 Color: 338
Size: 620 Color: 124

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10986 Color: 470
Size: 3514 Color: 333
Size: 700 Color: 139

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10994 Color: 471
Size: 2864 Color: 309
Size: 1342 Color: 209

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11010 Color: 472
Size: 4030 Color: 350
Size: 160 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11042 Color: 475
Size: 3466 Color: 328
Size: 692 Color: 135

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11047 Color: 477
Size: 3461 Color: 326
Size: 692 Color: 138

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11324 Color: 483
Size: 3592 Color: 335
Size: 284 Color: 29

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11432 Color: 485
Size: 3236 Color: 321
Size: 532 Color: 109

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11434 Color: 486
Size: 3598 Color: 337
Size: 168 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 489
Size: 3466 Color: 327
Size: 204 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11628 Color: 491
Size: 3276 Color: 322
Size: 296 Color: 36

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11793 Color: 498
Size: 2841 Color: 306
Size: 566 Color: 112

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11796 Color: 499
Size: 2844 Color: 307
Size: 560 Color: 110

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11797 Color: 500
Size: 2837 Color: 305
Size: 566 Color: 113

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11974 Color: 502
Size: 2256 Color: 275
Size: 970 Color: 176

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11992 Color: 503
Size: 2492 Color: 288
Size: 716 Color: 144

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12109 Color: 507
Size: 2577 Color: 296
Size: 514 Color: 105

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12160 Color: 509
Size: 1888 Color: 252
Size: 1152 Color: 194

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12170 Color: 510
Size: 2568 Color: 295
Size: 462 Color: 91

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 512
Size: 1610 Color: 233
Size: 1406 Color: 213

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 519
Size: 2314 Color: 280
Size: 488 Color: 96

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12426 Color: 521
Size: 1758 Color: 243
Size: 1016 Color: 183

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 522
Size: 2440 Color: 286
Size: 332 Color: 50

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12445 Color: 523
Size: 2313 Color: 279
Size: 442 Color: 85

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12482 Color: 524
Size: 2126 Color: 266
Size: 592 Color: 120

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 525
Size: 2064 Color: 264
Size: 648 Color: 130

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12528 Color: 528
Size: 1800 Color: 246
Size: 872 Color: 165

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 532
Size: 2563 Color: 294
Size: 72 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12572 Color: 533
Size: 1644 Color: 235
Size: 984 Color: 180

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 536
Size: 1912 Color: 255
Size: 644 Color: 128

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 537
Size: 1448 Color: 217
Size: 1104 Color: 190

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12772 Color: 541
Size: 1846 Color: 250
Size: 582 Color: 119

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12792 Color: 542
Size: 1532 Color: 227
Size: 876 Color: 167

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 543
Size: 1900 Color: 253
Size: 502 Color: 101

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12890 Color: 548
Size: 2290 Color: 278
Size: 20 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12911 Color: 549
Size: 1909 Color: 254
Size: 380 Color: 66

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 552
Size: 1368 Color: 210
Size: 880 Color: 168

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 555
Size: 1324 Color: 207
Size: 856 Color: 162

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 558
Size: 1388 Color: 212
Size: 728 Color: 148

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13091 Color: 559
Size: 1759 Color: 244
Size: 350 Color: 56

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13143 Color: 562
Size: 1719 Color: 242
Size: 338 Color: 52

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13183 Color: 564
Size: 1681 Color: 238
Size: 336 Color: 51

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13270 Color: 570
Size: 984 Color: 179
Size: 946 Color: 172

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 571
Size: 1624 Color: 234
Size: 260 Color: 20

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13322 Color: 573
Size: 1494 Color: 223
Size: 384 Color: 68

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 574
Size: 1516 Color: 225
Size: 348 Color: 55

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 575
Size: 1460 Color: 218
Size: 376 Color: 65

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13418 Color: 578
Size: 1522 Color: 226
Size: 260 Color: 21

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13445 Color: 580
Size: 1497 Color: 224
Size: 258 Color: 19

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13448 Color: 581
Size: 1248 Color: 197
Size: 504 Color: 102

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13449 Color: 582
Size: 1311 Color: 206
Size: 440 Color: 84

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 583
Size: 1428 Color: 214
Size: 320 Color: 45

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13462 Color: 584
Size: 1434 Color: 215
Size: 304 Color: 41

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 585
Size: 1064 Color: 186
Size: 672 Color: 132

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13469 Color: 586
Size: 1443 Color: 216
Size: 288 Color: 31

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13540 Color: 590
Size: 960 Color: 175
Size: 700 Color: 140

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13557 Color: 591
Size: 1371 Color: 211
Size: 272 Color: 24

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 592
Size: 952 Color: 173
Size: 688 Color: 134

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13594 Color: 593
Size: 1264 Color: 201
Size: 342 Color: 54

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13610 Color: 595
Size: 1302 Color: 205
Size: 288 Color: 33

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13620 Color: 596
Size: 1000 Color: 182
Size: 580 Color: 118

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13651 Color: 599
Size: 1291 Color: 204
Size: 258 Color: 18

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 600
Size: 976 Color: 178
Size: 564 Color: 111

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 9290 Color: 428
Size: 5557 Color: 386
Size: 352 Color: 57

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 10283 Color: 451
Size: 4916 Color: 374

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11471 Color: 487
Size: 2968 Color: 314
Size: 760 Color: 150

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12177 Color: 511
Size: 2882 Color: 310
Size: 140 Color: 7

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12193 Color: 513
Size: 2526 Color: 292
Size: 480 Color: 95

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 515
Size: 2917 Color: 312
Size: 28 Color: 1

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12425 Color: 520
Size: 2206 Color: 272
Size: 568 Color: 114

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12521 Color: 527
Size: 1566 Color: 229
Size: 1112 Color: 193

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 12692 Color: 539
Size: 2507 Color: 289

Bin 74: 1 of cap free
Amount of items: 2
Items: 
Size: 12986 Color: 553
Size: 2213 Color: 273

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 13319 Color: 572
Size: 1880 Color: 251

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 577
Size: 1811 Color: 247

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 8522 Color: 416
Size: 6312 Color: 394
Size: 364 Color: 61

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 8526 Color: 417
Size: 6672 Color: 401

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 459
Size: 4306 Color: 360
Size: 272 Color: 25

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11044 Color: 476
Size: 3994 Color: 347
Size: 160 Color: 9

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 516
Size: 2918 Color: 313

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 13190 Color: 566
Size: 2008 Color: 261

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 13514 Color: 589
Size: 1684 Color: 239

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 13629 Color: 597
Size: 1569 Color: 230

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 13638 Color: 598
Size: 1560 Color: 228

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 10882 Color: 466
Size: 4059 Color: 351
Size: 256 Color: 15

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 11034 Color: 474
Size: 4163 Color: 354

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 12545 Color: 530
Size: 2652 Color: 301

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 12554 Color: 531
Size: 2643 Color: 300

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 13139 Color: 561
Size: 2058 Color: 263

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 13482 Color: 587
Size: 1715 Color: 241

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 10647 Color: 460
Size: 4285 Color: 358
Size: 264 Color: 23

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 11050 Color: 478
Size: 4002 Color: 348
Size: 144 Color: 8

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 11780 Color: 497
Size: 3416 Color: 324

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 506
Size: 2680 Color: 302
Size: 408 Color: 75

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 12858 Color: 546
Size: 2338 Color: 282

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 569
Size: 1940 Color: 256

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 11721 Color: 495
Size: 3474 Color: 330

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 12372 Color: 518
Size: 2823 Color: 304

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 12585 Color: 534
Size: 2610 Color: 298

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 10904 Color: 468
Size: 4082 Color: 353
Size: 208 Color: 13

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 11026 Color: 473
Size: 4168 Color: 355

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 480
Size: 3986 Color: 346
Size: 96 Color: 5

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 12998 Color: 554
Size: 2196 Color: 271

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13374 Color: 576
Size: 1820 Color: 248

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13596 Color: 594
Size: 1598 Color: 232

Bin 107: 7 of cap free
Amount of items: 9
Items: 
Size: 7603 Color: 407
Size: 1266 Color: 202
Size: 1264 Color: 200
Size: 1264 Color: 199
Size: 1264 Color: 198
Size: 1232 Color: 196
Size: 500 Color: 100
Size: 400 Color: 74
Size: 400 Color: 73

Bin 108: 8 of cap free
Amount of items: 3
Items: 
Size: 9377 Color: 433
Size: 5487 Color: 384
Size: 328 Color: 49

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 9465 Color: 435
Size: 5727 Color: 391

Bin 110: 8 of cap free
Amount of items: 3
Items: 
Size: 10026 Color: 444
Size: 3594 Color: 336
Size: 1572 Color: 231

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 12212 Color: 514
Size: 2980 Color: 315

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 12734 Color: 540
Size: 2458 Color: 287

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 12828 Color: 544
Size: 2364 Color: 284

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 12876 Color: 547
Size: 2316 Color: 281

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 13029 Color: 556
Size: 2163 Color: 269

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 13163 Color: 563
Size: 2028 Color: 262

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 13492 Color: 588
Size: 1699 Color: 240

Bin 118: 10 of cap free
Amount of items: 25
Items: 
Size: 796 Color: 152
Size: 792 Color: 151
Size: 720 Color: 147
Size: 720 Color: 146
Size: 716 Color: 145
Size: 704 Color: 143
Size: 704 Color: 142
Size: 700 Color: 141
Size: 692 Color: 137
Size: 692 Color: 136
Size: 680 Color: 133
Size: 664 Color: 131
Size: 648 Color: 129
Size: 608 Color: 123
Size: 520 Color: 106
Size: 512 Color: 104
Size: 512 Color: 103
Size: 496 Color: 99
Size: 496 Color: 98
Size: 494 Color: 97
Size: 480 Color: 94
Size: 464 Color: 93
Size: 464 Color: 92
Size: 460 Color: 90
Size: 456 Color: 89

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 12924 Color: 551
Size: 2266 Color: 277

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 13188 Color: 565
Size: 2002 Color: 260

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 13236 Color: 568
Size: 1954 Color: 257

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 12837 Color: 545
Size: 2352 Color: 283

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 13425 Color: 579
Size: 1764 Color: 245

Bin 124: 12 of cap free
Amount of items: 3
Items: 
Size: 7612 Color: 412
Size: 7208 Color: 403
Size: 368 Color: 64

Bin 125: 12 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 467
Size: 4074 Color: 352
Size: 224 Color: 14

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 13208 Color: 567
Size: 1980 Color: 259

Bin 127: 13 of cap free
Amount of items: 2
Items: 
Size: 12493 Color: 526
Size: 2694 Color: 303

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 13094 Color: 560
Size: 2092 Color: 265

Bin 129: 15 of cap free
Amount of items: 2
Items: 
Size: 12284 Color: 517
Size: 2901 Color: 311

Bin 130: 15 of cap free
Amount of items: 2
Items: 
Size: 12605 Color: 535
Size: 2580 Color: 297

Bin 131: 15 of cap free
Amount of items: 2
Items: 
Size: 12650 Color: 538
Size: 2535 Color: 293

Bin 132: 16 of cap free
Amount of items: 3
Items: 
Size: 11098 Color: 479
Size: 3974 Color: 345
Size: 112 Color: 6

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 11702 Color: 494
Size: 3482 Color: 331

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 12920 Color: 550
Size: 2264 Color: 276

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 13048 Color: 557
Size: 2136 Color: 268

Bin 136: 17 of cap free
Amount of items: 3
Items: 
Size: 10108 Color: 448
Size: 4781 Color: 370
Size: 294 Color: 35

Bin 137: 19 of cap free
Amount of items: 3
Items: 
Size: 9529 Color: 438
Size: 5340 Color: 383
Size: 312 Color: 44

Bin 138: 20 of cap free
Amount of items: 3
Items: 
Size: 9640 Color: 439
Size: 5228 Color: 380
Size: 312 Color: 43

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 11496 Color: 488
Size: 3684 Color: 341

Bin 140: 21 of cap free
Amount of items: 2
Items: 
Size: 11325 Color: 484
Size: 3854 Color: 343

Bin 141: 21 of cap free
Amount of items: 2
Items: 
Size: 12070 Color: 505
Size: 3109 Color: 318

Bin 142: 22 of cap free
Amount of items: 2
Items: 
Size: 11538 Color: 490
Size: 3640 Color: 340

Bin 143: 22 of cap free
Amount of items: 2
Items: 
Size: 11656 Color: 492
Size: 3522 Color: 334

Bin 144: 24 of cap free
Amount of items: 2
Items: 
Size: 7640 Color: 413
Size: 7536 Color: 404

Bin 145: 24 of cap free
Amount of items: 3
Items: 
Size: 9948 Color: 443
Size: 4924 Color: 376
Size: 304 Color: 39

Bin 146: 25 of cap free
Amount of items: 3
Items: 
Size: 10667 Color: 461
Size: 4244 Color: 357
Size: 264 Color: 22

Bin 147: 26 of cap free
Amount of items: 21
Items: 
Size: 944 Color: 170
Size: 912 Color: 169
Size: 876 Color: 166
Size: 860 Color: 164
Size: 860 Color: 163
Size: 848 Color: 161
Size: 832 Color: 160
Size: 832 Color: 159
Size: 816 Color: 158
Size: 812 Color: 157
Size: 812 Color: 156
Size: 800 Color: 155
Size: 796 Color: 154
Size: 796 Color: 153
Size: 736 Color: 149
Size: 452 Color: 88
Size: 448 Color: 87
Size: 444 Color: 86
Size: 434 Color: 83
Size: 432 Color: 82
Size: 432 Color: 81

Bin 148: 26 of cap free
Amount of items: 2
Items: 
Size: 10780 Color: 463
Size: 4394 Color: 364

Bin 149: 26 of cap free
Amount of items: 2
Items: 
Size: 12120 Color: 508
Size: 3054 Color: 316

Bin 150: 27 of cap free
Amount of items: 2
Items: 
Size: 12530 Color: 529
Size: 2643 Color: 299

Bin 151: 31 of cap free
Amount of items: 2
Items: 
Size: 11701 Color: 493
Size: 3468 Color: 329

Bin 152: 31 of cap free
Amount of items: 2
Items: 
Size: 11813 Color: 501
Size: 3356 Color: 323

Bin 153: 36 of cap free
Amount of items: 3
Items: 
Size: 9938 Color: 442
Size: 4922 Color: 375
Size: 304 Color: 40

Bin 154: 36 of cap free
Amount of items: 2
Items: 
Size: 11742 Color: 496
Size: 3422 Color: 325

Bin 155: 36 of cap free
Amount of items: 2
Items: 
Size: 12020 Color: 504
Size: 3144 Color: 319

Bin 156: 37 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 432
Size: 5501 Color: 385
Size: 342 Color: 53

Bin 157: 39 of cap free
Amount of items: 3
Items: 
Size: 11180 Color: 481
Size: 3885 Color: 344
Size: 96 Color: 4

Bin 158: 40 of cap free
Amount of items: 2
Items: 
Size: 10376 Color: 454
Size: 4784 Color: 371

Bin 159: 40 of cap free
Amount of items: 3
Items: 
Size: 11276 Color: 482
Size: 3820 Color: 342
Size: 64 Color: 2

Bin 160: 41 of cap free
Amount of items: 2
Items: 
Size: 10306 Color: 452
Size: 4853 Color: 372

Bin 161: 42 of cap free
Amount of items: 2
Items: 
Size: 8824 Color: 423
Size: 6334 Color: 399

Bin 162: 44 of cap free
Amount of items: 2
Items: 
Size: 9896 Color: 440
Size: 5260 Color: 381

Bin 163: 46 of cap free
Amount of items: 3
Items: 
Size: 10216 Color: 450
Size: 4648 Color: 367
Size: 290 Color: 34

Bin 164: 46 of cap free
Amount of items: 2
Items: 
Size: 10840 Color: 464
Size: 4314 Color: 361

Bin 165: 47 of cap free
Amount of items: 3
Items: 
Size: 9513 Color: 437
Size: 5320 Color: 382
Size: 320 Color: 46

Bin 166: 47 of cap free
Amount of items: 3
Items: 
Size: 10873 Color: 465
Size: 4024 Color: 349
Size: 256 Color: 16

Bin 167: 48 of cap free
Amount of items: 3
Items: 
Size: 10314 Color: 453
Size: 4550 Color: 366
Size: 288 Color: 32

Bin 168: 50 of cap free
Amount of items: 2
Items: 
Size: 10034 Color: 445
Size: 5116 Color: 379

Bin 169: 52 of cap free
Amount of items: 4
Items: 
Size: 9472 Color: 436
Size: 5036 Color: 378
Size: 320 Color: 48
Size: 320 Color: 47

Bin 170: 54 of cap free
Amount of items: 3
Items: 
Size: 9930 Color: 441
Size: 4904 Color: 373
Size: 312 Color: 42

Bin 171: 58 of cap free
Amount of items: 9
Items: 
Size: 7602 Color: 406
Size: 1216 Color: 195
Size: 1112 Color: 192
Size: 1110 Color: 191
Size: 1098 Color: 189
Size: 1096 Color: 188
Size: 1076 Color: 187
Size: 416 Color: 77
Size: 416 Color: 76

Bin 172: 60 of cap free
Amount of items: 2
Items: 
Size: 8617 Color: 421
Size: 6523 Color: 400

Bin 173: 64 of cap free
Amount of items: 6
Items: 
Size: 7607 Color: 410
Size: 2436 Color: 285
Size: 2181 Color: 270
Size: 2132 Color: 267
Size: 392 Color: 70
Size: 388 Color: 69

Bin 174: 65 of cap free
Amount of items: 3
Items: 
Size: 8533 Color: 419
Size: 3506 Color: 332
Size: 3096 Color: 317

Bin 175: 69 of cap free
Amount of items: 2
Items: 
Size: 10205 Color: 449
Size: 4926 Color: 377

Bin 176: 70 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 458
Size: 4424 Color: 365
Size: 272 Color: 26

Bin 177: 72 of cap free
Amount of items: 2
Items: 
Size: 8796 Color: 422
Size: 6332 Color: 398

Bin 178: 73 of cap free
Amount of items: 3
Items: 
Size: 9294 Color: 430
Size: 3607 Color: 339
Size: 2226 Color: 274

Bin 179: 76 of cap free
Amount of items: 2
Items: 
Size: 9308 Color: 431
Size: 5816 Color: 392

Bin 180: 91 of cap free
Amount of items: 2
Items: 
Size: 9445 Color: 434
Size: 5664 Color: 390

Bin 181: 116 of cap free
Amount of items: 3
Items: 
Size: 10418 Color: 457
Size: 4386 Color: 363
Size: 280 Color: 27

Bin 182: 118 of cap free
Amount of items: 3
Items: 
Size: 9164 Color: 427
Size: 5566 Color: 389
Size: 352 Color: 58

Bin 183: 119 of cap free
Amount of items: 3
Items: 
Size: 10044 Color: 447
Size: 4741 Color: 369
Size: 296 Color: 37

Bin 184: 130 of cap free
Amount of items: 3
Items: 
Size: 10410 Color: 456
Size: 4380 Color: 362
Size: 280 Color: 28

Bin 185: 135 of cap free
Amount of items: 3
Items: 
Size: 10042 Color: 446
Size: 4727 Color: 368
Size: 296 Color: 38

Bin 186: 144 of cap free
Amount of items: 2
Items: 
Size: 8892 Color: 424
Size: 6164 Color: 393

Bin 187: 156 of cap free
Amount of items: 3
Items: 
Size: 9292 Color: 429
Size: 3231 Color: 320
Size: 2521 Color: 291

Bin 188: 158 of cap free
Amount of items: 5
Items: 
Size: 7608 Color: 411
Size: 2852 Color: 308
Size: 2520 Color: 290
Size: 1678 Color: 237
Size: 384 Color: 67

Bin 189: 166 of cap free
Amount of items: 7
Items: 
Size: 7604 Color: 408
Size: 1481 Color: 221
Size: 1464 Color: 220
Size: 1461 Color: 219
Size: 1340 Color: 208
Size: 1284 Color: 203
Size: 400 Color: 72

Bin 190: 168 of cap free
Amount of items: 3
Items: 
Size: 7804 Color: 414
Size: 6860 Color: 402
Size: 368 Color: 63

Bin 191: 208 of cap free
Amount of items: 3
Items: 
Size: 9068 Color: 426
Size: 5564 Color: 388
Size: 360 Color: 59

Bin 192: 210 of cap free
Amount of items: 3
Items: 
Size: 10402 Color: 455
Size: 4300 Color: 359
Size: 288 Color: 30

Bin 193: 235 of cap free
Amount of items: 6
Items: 
Size: 7606 Color: 409
Size: 1971 Color: 258
Size: 1838 Color: 249
Size: 1672 Color: 236
Size: 1486 Color: 222
Size: 392 Color: 71

Bin 194: 258 of cap free
Amount of items: 3
Items: 
Size: 9020 Color: 425
Size: 5562 Color: 387
Size: 360 Color: 60

Bin 195: 268 of cap free
Amount of items: 2
Items: 
Size: 8601 Color: 420
Size: 6331 Color: 397

Bin 196: 271 of cap free
Amount of items: 3
Items: 
Size: 8232 Color: 415
Size: 6329 Color: 395
Size: 368 Color: 62

Bin 197: 338 of cap free
Amount of items: 2
Items: 
Size: 8532 Color: 418
Size: 6330 Color: 396

Bin 198: 373 of cap free
Amount of items: 10
Items: 
Size: 7601 Color: 405
Size: 1056 Color: 185
Size: 1048 Color: 184
Size: 984 Color: 181
Size: 976 Color: 177
Size: 954 Color: 174
Size: 944 Color: 171
Size: 424 Color: 80
Size: 424 Color: 79
Size: 416 Color: 78

Bin 199: 9302 of cap free
Amount of items: 10
Items: 
Size: 640 Color: 127
Size: 632 Color: 126
Size: 624 Color: 125
Size: 608 Color: 122
Size: 608 Color: 121
Size: 578 Color: 117
Size: 576 Color: 116
Size: 576 Color: 115
Size: 528 Color: 108
Size: 528 Color: 107

Total size: 3009600
Total free space: 15200

