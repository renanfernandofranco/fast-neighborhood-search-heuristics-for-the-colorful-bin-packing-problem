Capicity Bin: 16800
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 19
Items: 
Size: 1192 Color: 3
Size: 1110 Color: 3
Size: 1056 Color: 3
Size: 1000 Color: 0
Size: 976 Color: 0
Size: 960 Color: 0
Size: 952 Color: 4
Size: 920 Color: 1
Size: 902 Color: 1
Size: 900 Color: 2
Size: 856 Color: 4
Size: 816 Color: 4
Size: 808 Color: 3
Size: 800 Color: 2
Size: 800 Color: 0
Size: 756 Color: 2
Size: 732 Color: 3
Size: 720 Color: 1
Size: 544 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 1
Size: 7576 Color: 0
Size: 720 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9428 Color: 0
Size: 7004 Color: 1
Size: 368 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9368 Color: 3
Size: 7000 Color: 0
Size: 432 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 4
Size: 4060 Color: 0
Size: 792 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12034 Color: 4
Size: 3338 Color: 2
Size: 1428 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12734 Color: 4
Size: 3658 Color: 1
Size: 408 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 2
Size: 3598 Color: 0
Size: 358 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 4
Size: 3168 Color: 2
Size: 616 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13240 Color: 0
Size: 3124 Color: 1
Size: 436 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 1
Size: 3160 Color: 3
Size: 368 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 3
Size: 2804 Color: 1
Size: 632 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13460 Color: 2
Size: 2988 Color: 0
Size: 352 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 2
Size: 2858 Color: 0
Size: 478 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13534 Color: 3
Size: 2602 Color: 1
Size: 664 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 0
Size: 1916 Color: 4
Size: 1224 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 4
Size: 2792 Color: 1
Size: 336 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 4
Size: 2492 Color: 3
Size: 480 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 2
Size: 1524 Color: 1
Size: 1396 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 0
Size: 2584 Color: 3
Size: 324 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13923 Color: 0
Size: 1669 Color: 1
Size: 1208 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13976 Color: 4
Size: 2152 Color: 4
Size: 672 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 3
Size: 2044 Color: 4
Size: 592 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 0
Size: 1842 Color: 2
Size: 758 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14205 Color: 0
Size: 2163 Color: 1
Size: 432 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14232 Color: 3
Size: 2484 Color: 4
Size: 84 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14270 Color: 2
Size: 2180 Color: 1
Size: 350 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14330 Color: 0
Size: 2154 Color: 1
Size: 316 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14342 Color: 4
Size: 2106 Color: 0
Size: 352 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 4
Size: 1928 Color: 2
Size: 492 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14472 Color: 1
Size: 1944 Color: 0
Size: 384 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 1
Size: 2050 Color: 4
Size: 262 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14483 Color: 2
Size: 1931 Color: 1
Size: 386 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14491 Color: 3
Size: 1925 Color: 3
Size: 384 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14572 Color: 0
Size: 1444 Color: 2
Size: 784 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 4
Size: 1642 Color: 2
Size: 568 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14601 Color: 3
Size: 1665 Color: 2
Size: 534 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14656 Color: 2
Size: 1396 Color: 4
Size: 748 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14674 Color: 2
Size: 1518 Color: 1
Size: 608 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14693 Color: 1
Size: 1757 Color: 0
Size: 350 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14701 Color: 0
Size: 1443 Color: 2
Size: 656 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14799 Color: 2
Size: 1533 Color: 3
Size: 468 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14836 Color: 2
Size: 1028 Color: 3
Size: 936 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14804 Color: 3
Size: 1628 Color: 2
Size: 368 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14887 Color: 2
Size: 1505 Color: 1
Size: 408 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14830 Color: 4
Size: 1116 Color: 1
Size: 854 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14886 Color: 1
Size: 1406 Color: 0
Size: 508 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14904 Color: 2
Size: 1168 Color: 3
Size: 728 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14982 Color: 2
Size: 1478 Color: 3
Size: 340 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14990 Color: 3
Size: 1510 Color: 0
Size: 300 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 15030 Color: 2
Size: 1442 Color: 3
Size: 328 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 15068 Color: 3
Size: 868 Color: 2
Size: 864 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 15070 Color: 4
Size: 1398 Color: 2
Size: 332 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 15092 Color: 2
Size: 928 Color: 4
Size: 780 Color: 3

Bin 55: 0 of cap free
Amount of items: 4
Items: 
Size: 15076 Color: 0
Size: 1702 Color: 1
Size: 14 Color: 0
Size: 8 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 15114 Color: 4
Size: 1398 Color: 1
Size: 288 Color: 3

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 1
Size: 6993 Color: 3
Size: 1396 Color: 1

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 9066 Color: 2
Size: 6997 Color: 3
Size: 736 Color: 2

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12479 Color: 2
Size: 4088 Color: 1
Size: 232 Color: 0

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12728 Color: 4
Size: 3791 Color: 2
Size: 280 Color: 0

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13118 Color: 3
Size: 2977 Color: 2
Size: 704 Color: 0

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 14007 Color: 4
Size: 2168 Color: 2
Size: 624 Color: 1

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 14086 Color: 1
Size: 2169 Color: 2
Size: 544 Color: 4

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 2
Size: 2452 Color: 1
Size: 232 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 14199 Color: 2
Size: 1592 Color: 4
Size: 1008 Color: 1

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 14475 Color: 3
Size: 1860 Color: 2
Size: 464 Color: 1

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 14668 Color: 3
Size: 2131 Color: 4

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 14961 Color: 3
Size: 1598 Color: 2
Size: 240 Color: 1

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 15009 Color: 4
Size: 1548 Color: 2
Size: 242 Color: 4

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 8412 Color: 3
Size: 6994 Color: 0
Size: 1392 Color: 2

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 9524 Color: 4
Size: 6986 Color: 2
Size: 288 Color: 1

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 2
Size: 6192 Color: 0
Size: 300 Color: 1

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 11074 Color: 3
Size: 5388 Color: 3
Size: 336 Color: 0

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 11665 Color: 4
Size: 4517 Color: 0
Size: 616 Color: 3

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 12110 Color: 1
Size: 4284 Color: 1
Size: 404 Color: 2

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 12302 Color: 1
Size: 3544 Color: 2
Size: 952 Color: 4

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 1
Size: 3910 Color: 2
Size: 512 Color: 3

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 12979 Color: 2
Size: 3595 Color: 1
Size: 224 Color: 0

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 13226 Color: 0
Size: 3172 Color: 1
Size: 400 Color: 2

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 13393 Color: 0
Size: 3085 Color: 0
Size: 320 Color: 3

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 13682 Color: 1
Size: 1652 Color: 0
Size: 1464 Color: 4

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14438 Color: 3
Size: 2360 Color: 2

Bin 83: 3 of cap free
Amount of items: 7
Items: 
Size: 8402 Color: 0
Size: 1644 Color: 0
Size: 1595 Color: 3
Size: 1396 Color: 3
Size: 1392 Color: 1
Size: 1288 Color: 4
Size: 1080 Color: 4

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 10952 Color: 0
Size: 5141 Color: 1
Size: 704 Color: 0

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 12414 Color: 0
Size: 4083 Color: 0
Size: 300 Color: 1

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 13860 Color: 1
Size: 2841 Color: 4
Size: 96 Color: 3

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 8413 Color: 2
Size: 6991 Color: 3
Size: 1392 Color: 4

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 4
Size: 6256 Color: 3
Size: 328 Color: 2

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 0
Size: 4040 Color: 1
Size: 192 Color: 4

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 3
Size: 3300 Color: 0
Size: 306 Color: 4

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 13724 Color: 0
Size: 2414 Color: 4
Size: 658 Color: 1

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 3
Size: 2564 Color: 1
Size: 488 Color: 0

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 14076 Color: 2
Size: 2624 Color: 3
Size: 96 Color: 4

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14275 Color: 0
Size: 2521 Color: 4

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 14356 Color: 2
Size: 2440 Color: 4

Bin 96: 5 of cap free
Amount of items: 7
Items: 
Size: 8405 Color: 2
Size: 1780 Color: 2
Size: 1668 Color: 3
Size: 1512 Color: 4
Size: 1398 Color: 1
Size: 1072 Color: 1
Size: 960 Color: 4

Bin 97: 5 of cap free
Amount of items: 3
Items: 
Size: 14412 Color: 2
Size: 2239 Color: 3
Size: 144 Color: 2

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 8696 Color: 0
Size: 7842 Color: 4
Size: 256 Color: 1

Bin 99: 6 of cap free
Amount of items: 3
Items: 
Size: 12251 Color: 3
Size: 4019 Color: 3
Size: 524 Color: 0

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 13842 Color: 1
Size: 2952 Color: 3

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 14178 Color: 3
Size: 2616 Color: 2

Bin 102: 7 of cap free
Amount of items: 3
Items: 
Size: 10790 Color: 1
Size: 5551 Color: 1
Size: 452 Color: 2

Bin 103: 7 of cap free
Amount of items: 3
Items: 
Size: 11381 Color: 4
Size: 4724 Color: 4
Size: 688 Color: 0

Bin 104: 7 of cap free
Amount of items: 3
Items: 
Size: 11901 Color: 1
Size: 4076 Color: 2
Size: 816 Color: 3

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 12741 Color: 0
Size: 4052 Color: 3

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 14365 Color: 1
Size: 2428 Color: 2

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 15000 Color: 3
Size: 1793 Color: 0

Bin 108: 8 of cap free
Amount of items: 3
Items: 
Size: 11574 Color: 4
Size: 4668 Color: 0
Size: 550 Color: 3

Bin 109: 8 of cap free
Amount of items: 3
Items: 
Size: 11764 Color: 1
Size: 4708 Color: 4
Size: 320 Color: 3

Bin 110: 8 of cap free
Amount of items: 3
Items: 
Size: 11964 Color: 4
Size: 4408 Color: 1
Size: 420 Color: 2

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 13002 Color: 2
Size: 3790 Color: 0

Bin 112: 9 of cap free
Amount of items: 3
Items: 
Size: 10139 Color: 2
Size: 6220 Color: 4
Size: 432 Color: 0

Bin 113: 9 of cap free
Amount of items: 3
Items: 
Size: 10856 Color: 1
Size: 3830 Color: 3
Size: 2105 Color: 2

Bin 114: 9 of cap free
Amount of items: 3
Items: 
Size: 12247 Color: 3
Size: 4252 Color: 1
Size: 292 Color: 2

Bin 115: 9 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 0
Size: 4281 Color: 3
Size: 256 Color: 0

Bin 116: 9 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 4
Size: 3299 Color: 0
Size: 432 Color: 4

Bin 117: 10 of cap free
Amount of items: 3
Items: 
Size: 11204 Color: 3
Size: 5250 Color: 0
Size: 336 Color: 1

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 14728 Color: 1
Size: 2062 Color: 3

Bin 119: 11 of cap free
Amount of items: 3
Items: 
Size: 13145 Color: 4
Size: 3092 Color: 4
Size: 552 Color: 1

Bin 120: 12 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 1
Size: 4282 Color: 2
Size: 126 Color: 2

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 14602 Color: 3
Size: 2186 Color: 1

Bin 122: 13 of cap free
Amount of items: 2
Items: 
Size: 14995 Color: 1
Size: 1792 Color: 3

Bin 123: 14 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 2
Size: 5970 Color: 0
Size: 808 Color: 1

Bin 124: 14 of cap free
Amount of items: 3
Items: 
Size: 10082 Color: 2
Size: 6200 Color: 0
Size: 504 Color: 3

Bin 125: 14 of cap free
Amount of items: 2
Items: 
Size: 13408 Color: 3
Size: 3378 Color: 2

Bin 126: 16 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 1
Size: 5912 Color: 1
Size: 532 Color: 4

Bin 127: 16 of cap free
Amount of items: 2
Items: 
Size: 11896 Color: 3
Size: 4888 Color: 1

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 13775 Color: 4
Size: 3009 Color: 2

Bin 129: 16 of cap free
Amount of items: 2
Items: 
Size: 14845 Color: 0
Size: 1939 Color: 1

Bin 130: 16 of cap free
Amount of items: 2
Items: 
Size: 15048 Color: 1
Size: 1736 Color: 0

Bin 131: 17 of cap free
Amount of items: 4
Items: 
Size: 10728 Color: 0
Size: 2700 Color: 2
Size: 2603 Color: 3
Size: 752 Color: 3

Bin 132: 18 of cap free
Amount of items: 3
Items: 
Size: 11382 Color: 0
Size: 4968 Color: 4
Size: 432 Color: 1

Bin 133: 18 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 0
Size: 2982 Color: 2

Bin 134: 18 of cap free
Amount of items: 2
Items: 
Size: 14578 Color: 4
Size: 2204 Color: 2

Bin 135: 19 of cap free
Amount of items: 3
Items: 
Size: 11041 Color: 3
Size: 5412 Color: 0
Size: 328 Color: 2

Bin 136: 19 of cap free
Amount of items: 2
Items: 
Size: 14427 Color: 0
Size: 2354 Color: 4

Bin 137: 19 of cap free
Amount of items: 2
Items: 
Size: 14696 Color: 0
Size: 2085 Color: 1

Bin 138: 19 of cap free
Amount of items: 2
Items: 
Size: 14948 Color: 3
Size: 1833 Color: 0

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 14484 Color: 3
Size: 2296 Color: 0

Bin 140: 21 of cap free
Amount of items: 2
Items: 
Size: 10631 Color: 4
Size: 6148 Color: 2

Bin 141: 22 of cap free
Amount of items: 2
Items: 
Size: 14056 Color: 3
Size: 2722 Color: 2

Bin 142: 22 of cap free
Amount of items: 2
Items: 
Size: 14758 Color: 0
Size: 2020 Color: 4

Bin 143: 22 of cap free
Amount of items: 2
Items: 
Size: 14792 Color: 4
Size: 1986 Color: 0

Bin 144: 23 of cap free
Amount of items: 3
Items: 
Size: 9638 Color: 3
Size: 6043 Color: 0
Size: 1096 Color: 1

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 13092 Color: 4
Size: 3684 Color: 1

Bin 146: 26 of cap free
Amount of items: 3
Items: 
Size: 9453 Color: 4
Size: 7001 Color: 0
Size: 320 Color: 1

Bin 147: 26 of cap free
Amount of items: 3
Items: 
Size: 11042 Color: 1
Size: 5414 Color: 2
Size: 318 Color: 0

Bin 148: 26 of cap free
Amount of items: 2
Items: 
Size: 13374 Color: 3
Size: 3400 Color: 1

Bin 149: 26 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 3
Size: 2868 Color: 4

Bin 150: 27 of cap free
Amount of items: 3
Items: 
Size: 12843 Color: 1
Size: 3166 Color: 3
Size: 764 Color: 3

Bin 151: 27 of cap free
Amount of items: 2
Items: 
Size: 13293 Color: 4
Size: 3480 Color: 2

Bin 152: 28 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 1
Size: 3974 Color: 3

Bin 153: 30 of cap free
Amount of items: 2
Items: 
Size: 14508 Color: 4
Size: 2262 Color: 2

Bin 154: 32 of cap free
Amount of items: 2
Items: 
Size: 9720 Color: 3
Size: 7048 Color: 0

Bin 155: 32 of cap free
Amount of items: 2
Items: 
Size: 11704 Color: 3
Size: 5064 Color: 4

Bin 156: 32 of cap free
Amount of items: 2
Items: 
Size: 12552 Color: 2
Size: 4216 Color: 3

Bin 157: 36 of cap free
Amount of items: 2
Items: 
Size: 11960 Color: 3
Size: 4804 Color: 0

Bin 158: 36 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 3
Size: 2576 Color: 4

Bin 159: 39 of cap free
Amount of items: 2
Items: 
Size: 13229 Color: 2
Size: 3532 Color: 4

Bin 160: 41 of cap free
Amount of items: 5
Items: 
Size: 8409 Color: 2
Size: 2466 Color: 0
Size: 2399 Color: 4
Size: 1854 Color: 0
Size: 1631 Color: 4

Bin 161: 41 of cap free
Amount of items: 2
Items: 
Size: 14649 Color: 4
Size: 2110 Color: 3

Bin 162: 42 of cap free
Amount of items: 2
Items: 
Size: 14820 Color: 0
Size: 1938 Color: 3

Bin 163: 43 of cap free
Amount of items: 2
Items: 
Size: 15069 Color: 4
Size: 1688 Color: 0

Bin 164: 45 of cap free
Amount of items: 2
Items: 
Size: 13365 Color: 0
Size: 3390 Color: 3

Bin 165: 45 of cap free
Amount of items: 2
Items: 
Size: 13650 Color: 1
Size: 3105 Color: 2

Bin 166: 46 of cap free
Amount of items: 2
Items: 
Size: 10308 Color: 2
Size: 6446 Color: 4

Bin 167: 46 of cap free
Amount of items: 2
Items: 
Size: 14980 Color: 3
Size: 1774 Color: 0

Bin 168: 48 of cap free
Amount of items: 2
Items: 
Size: 9312 Color: 1
Size: 7440 Color: 3

Bin 169: 50 of cap free
Amount of items: 2
Items: 
Size: 14478 Color: 2
Size: 2272 Color: 4

Bin 170: 51 of cap free
Amount of items: 2
Items: 
Size: 13564 Color: 2
Size: 3185 Color: 1

Bin 171: 52 of cap free
Amount of items: 2
Items: 
Size: 12390 Color: 3
Size: 4358 Color: 0

Bin 172: 54 of cap free
Amount of items: 2
Items: 
Size: 12996 Color: 3
Size: 3750 Color: 1

Bin 173: 55 of cap free
Amount of items: 2
Items: 
Size: 13677 Color: 0
Size: 3068 Color: 3

Bin 174: 55 of cap free
Amount of items: 2
Items: 
Size: 13978 Color: 1
Size: 2767 Color: 4

Bin 175: 57 of cap free
Amount of items: 3
Items: 
Size: 8426 Color: 4
Size: 7085 Color: 1
Size: 1232 Color: 3

Bin 176: 58 of cap free
Amount of items: 2
Items: 
Size: 11140 Color: 2
Size: 5602 Color: 4

Bin 177: 60 of cap free
Amount of items: 3
Items: 
Size: 11528 Color: 3
Size: 2968 Color: 2
Size: 2244 Color: 2

Bin 178: 60 of cap free
Amount of items: 2
Items: 
Size: 14972 Color: 3
Size: 1768 Color: 1

Bin 179: 62 of cap free
Amount of items: 2
Items: 
Size: 14806 Color: 0
Size: 1932 Color: 4

Bin 180: 64 of cap free
Amount of items: 2
Items: 
Size: 12632 Color: 3
Size: 4104 Color: 0

Bin 181: 65 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 3
Size: 2329 Color: 2

Bin 182: 66 of cap free
Amount of items: 2
Items: 
Size: 14108 Color: 3
Size: 2626 Color: 4

Bin 183: 67 of cap free
Amount of items: 2
Items: 
Size: 11932 Color: 3
Size: 4801 Color: 4

Bin 184: 68 of cap free
Amount of items: 2
Items: 
Size: 13054 Color: 2
Size: 3678 Color: 4

Bin 185: 75 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 1
Size: 3601 Color: 3

Bin 186: 76 of cap free
Amount of items: 2
Items: 
Size: 12206 Color: 1
Size: 4518 Color: 0

Bin 187: 84 of cap free
Amount of items: 2
Items: 
Size: 11044 Color: 0
Size: 5672 Color: 1

Bin 188: 94 of cap free
Amount of items: 2
Items: 
Size: 8418 Color: 1
Size: 8288 Color: 4

Bin 189: 105 of cap free
Amount of items: 2
Items: 
Size: 13481 Color: 4
Size: 3214 Color: 2

Bin 190: 108 of cap free
Amount of items: 2
Items: 
Size: 11916 Color: 2
Size: 4776 Color: 3

Bin 191: 128 of cap free
Amount of items: 2
Items: 
Size: 11662 Color: 3
Size: 5010 Color: 1

Bin 192: 136 of cap free
Amount of items: 2
Items: 
Size: 11172 Color: 1
Size: 5492 Color: 2

Bin 193: 162 of cap free
Amount of items: 2
Items: 
Size: 11864 Color: 2
Size: 4774 Color: 4

Bin 194: 165 of cap free
Amount of items: 8
Items: 
Size: 8401 Color: 3
Size: 1524 Color: 0
Size: 1264 Color: 0
Size: 1232 Color: 0
Size: 1120 Color: 2
Size: 1080 Color: 2
Size: 1056 Color: 1
Size: 958 Color: 4

Bin 195: 178 of cap free
Amount of items: 33
Items: 
Size: 720 Color: 4
Size: 676 Color: 2
Size: 636 Color: 2
Size: 632 Color: 1
Size: 600 Color: 1
Size: 594 Color: 3
Size: 592 Color: 0
Size: 576 Color: 3
Size: 568 Color: 4
Size: 566 Color: 4
Size: 536 Color: 1
Size: 532 Color: 3
Size: 520 Color: 3
Size: 516 Color: 4
Size: 512 Color: 1
Size: 498 Color: 4
Size: 488 Color: 2
Size: 480 Color: 4
Size: 480 Color: 4
Size: 480 Color: 2
Size: 464 Color: 1
Size: 464 Color: 0
Size: 448 Color: 3
Size: 448 Color: 2
Size: 446 Color: 3
Size: 420 Color: 0
Size: 416 Color: 3
Size: 416 Color: 1
Size: 400 Color: 2
Size: 386 Color: 4
Size: 384 Color: 1
Size: 376 Color: 2
Size: 352 Color: 2

Bin 196: 203 of cap free
Amount of items: 5
Items: 
Size: 8408 Color: 2
Size: 2244 Color: 1
Size: 2080 Color: 0
Size: 2031 Color: 0
Size: 1834 Color: 3

Bin 197: 249 of cap free
Amount of items: 2
Items: 
Size: 9549 Color: 3
Size: 7002 Color: 2

Bin 198: 264 of cap free
Amount of items: 3
Items: 
Size: 8404 Color: 0
Size: 6996 Color: 3
Size: 1136 Color: 2

Bin 199: 12278 of cap free
Amount of items: 14
Items: 
Size: 384 Color: 3
Size: 384 Color: 0
Size: 366 Color: 4
Size: 364 Color: 1
Size: 352 Color: 2
Size: 320 Color: 3
Size: 304 Color: 4
Size: 304 Color: 1
Size: 304 Color: 0
Size: 296 Color: 1
Size: 288 Color: 2
Size: 288 Color: 2
Size: 288 Color: 0
Size: 280 Color: 4

Total size: 3326400
Total free space: 16800

