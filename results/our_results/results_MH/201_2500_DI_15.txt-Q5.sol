Capicity Bin: 1860
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 748 Color: 1
Size: 622 Color: 4
Size: 432 Color: 0
Size: 58 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1073 Color: 4
Size: 657 Color: 0
Size: 130 Color: 1

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1284 Color: 3
Size: 278 Color: 1
Size: 230 Color: 1
Size: 40 Color: 0
Size: 28 Color: 4

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1453 Color: 3
Size: 323 Color: 1
Size: 44 Color: 4
Size: 40 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1570 Color: 0
Size: 220 Color: 2
Size: 48 Color: 3
Size: 22 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 1
Size: 275 Color: 4
Size: 54 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 0
Size: 388 Color: 3
Size: 354 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1186 Color: 0
Size: 562 Color: 3
Size: 112 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 935 Color: 2
Size: 771 Color: 0
Size: 154 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 0
Size: 566 Color: 0
Size: 48 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1604 Color: 3
Size: 216 Color: 2
Size: 40 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1105 Color: 3
Size: 631 Color: 2
Size: 124 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 1
Size: 245 Color: 4
Size: 48 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1311 Color: 1
Size: 473 Color: 4
Size: 76 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 2
Size: 426 Color: 0
Size: 84 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 0
Size: 120 Color: 4
Size: 66 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1473 Color: 2
Size: 337 Color: 2
Size: 50 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 1
Size: 502 Color: 4
Size: 96 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 931 Color: 0
Size: 775 Color: 2
Size: 154 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 934 Color: 1
Size: 774 Color: 4
Size: 152 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1211 Color: 1
Size: 541 Color: 3
Size: 108 Color: 3

Bin 22: 0 of cap free
Amount of items: 4
Items: 
Size: 1457 Color: 0
Size: 271 Color: 4
Size: 76 Color: 0
Size: 56 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1635 Color: 0
Size: 189 Color: 0
Size: 36 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1435 Color: 3
Size: 369 Color: 0
Size: 56 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 0
Size: 291 Color: 4
Size: 26 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1600 Color: 2
Size: 208 Color: 4
Size: 52 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 2
Size: 690 Color: 2
Size: 136 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1637 Color: 2
Size: 195 Color: 2
Size: 28 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 939 Color: 0
Size: 769 Color: 4
Size: 152 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 2
Size: 281 Color: 3
Size: 40 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 1
Size: 271 Color: 0
Size: 16 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1371 Color: 1
Size: 409 Color: 2
Size: 80 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1077 Color: 3
Size: 653 Color: 1
Size: 130 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1213 Color: 3
Size: 541 Color: 0
Size: 106 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 2
Size: 333 Color: 2
Size: 66 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1322 Color: 4
Size: 474 Color: 3
Size: 64 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1101 Color: 1
Size: 633 Color: 1
Size: 126 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 3
Size: 242 Color: 1
Size: 68 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 1
Size: 208 Color: 2
Size: 66 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 4
Size: 158 Color: 4
Size: 48 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 0
Size: 262 Color: 2
Size: 68 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1321 Color: 0
Size: 451 Color: 4
Size: 88 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 0
Size: 386 Color: 0
Size: 36 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 3
Size: 294 Color: 4
Size: 56 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1221 Color: 0
Size: 533 Color: 3
Size: 106 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 3
Size: 213 Color: 1
Size: 42 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 0
Size: 218 Color: 2
Size: 40 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1168 Color: 3
Size: 628 Color: 0
Size: 64 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 3
Size: 213 Color: 2
Size: 38 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 3
Size: 207 Color: 1
Size: 40 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 4
Size: 322 Color: 1
Size: 60 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 2
Size: 271 Color: 4
Size: 54 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 1
Size: 523 Color: 0
Size: 104 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1217 Color: 0
Size: 537 Color: 1
Size: 106 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 3
Size: 265 Color: 4
Size: 32 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1393 Color: 2
Size: 391 Color: 4
Size: 76 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 2
Size: 237 Color: 0
Size: 46 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1093 Color: 0
Size: 641 Color: 4
Size: 126 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1097 Color: 1
Size: 637 Color: 0
Size: 126 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 178 Color: 3
Size: 24 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 4
Size: 190 Color: 2
Size: 36 Color: 0

Bin 62: 0 of cap free
Amount of items: 4
Items: 
Size: 1511 Color: 1
Size: 341 Color: 3
Size: 4 Color: 2
Size: 4 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1398 Color: 2
Size: 288 Color: 2
Size: 174 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 157 Color: 2
Size: 30 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 0
Size: 171 Color: 2
Size: 34 Color: 3

Total size: 120900
Total free space: 0

