Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 352 Color: 6
Size: 256 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 7
Size: 251 Color: 13
Size: 250 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 14
Size: 292 Color: 10
Size: 261 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 9
Size: 313 Color: 11
Size: 304 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 10
Size: 266 Color: 17
Size: 258 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 3
Size: 276 Color: 9
Size: 256 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8
Size: 289 Color: 19
Size: 275 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 4
Size: 335 Color: 3
Size: 281 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 4
Size: 324 Color: 2
Size: 287 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 11
Size: 363 Color: 17
Size: 260 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 13
Size: 314 Color: 12
Size: 303 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 16
Size: 304 Color: 15
Size: 266 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 12
Size: 270 Color: 2
Size: 262 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 368 Color: 15
Size: 261 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 16
Size: 341 Color: 1
Size: 268 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 304 Color: 19
Size: 277 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 5
Size: 267 Color: 18
Size: 261 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 10
Size: 258 Color: 4
Size: 253 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 359 Color: 12
Size: 266 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 4
Size: 292 Color: 18
Size: 283 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 18
Size: 262 Color: 4
Size: 250 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 6
Size: 289 Color: 16
Size: 255 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 12
Size: 300 Color: 8
Size: 260 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 14
Size: 350 Color: 17
Size: 254 Color: 9

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 6
Size: 252 Color: 14
Size: 251 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 14
Size: 252 Color: 3
Size: 401 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 18
Size: 335 Color: 14
Size: 261 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 18
Size: 268 Color: 6
Size: 264 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 5
Size: 291 Color: 10
Size: 259 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 324 Color: 7
Size: 279 Color: 13
Size: 397 Color: 15

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 11
Size: 264 Color: 13
Size: 250 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 11
Size: 361 Color: 17
Size: 259 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 2
Size: 355 Color: 19
Size: 250 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 296 Color: 9
Size: 255 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 12
Size: 319 Color: 2
Size: 276 Color: 12

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 15
Size: 301 Color: 9
Size: 280 Color: 7

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 327 Color: 1
Size: 262 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 13
Size: 365 Color: 7
Size: 270 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 6
Size: 258 Color: 10
Size: 254 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 313 Color: 18
Size: 291 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 322 Color: 16
Size: 298 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 12
Size: 271 Color: 9
Size: 268 Color: 8

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 4
Size: 296 Color: 7
Size: 287 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 3
Size: 282 Color: 9
Size: 253 Color: 11

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 284 Color: 16
Size: 254 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 6
Size: 253 Color: 13
Size: 252 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 9
Size: 303 Color: 19
Size: 271 Color: 17

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 2
Size: 295 Color: 4
Size: 254 Color: 13

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 254 Color: 13
Size: 252 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 296 Color: 9
Size: 284 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9
Size: 277 Color: 7
Size: 253 Color: 15

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 10
Size: 309 Color: 1
Size: 264 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 5
Size: 271 Color: 7
Size: 258 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 18
Size: 336 Color: 0
Size: 283 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 16
Size: 284 Color: 0
Size: 256 Color: 8

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 14
Size: 304 Color: 14
Size: 266 Color: 18

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 278 Color: 8
Size: 254 Color: 12

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 252 Color: 11
Size: 250 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 14
Size: 285 Color: 15
Size: 255 Color: 11

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 15
Size: 355 Color: 15
Size: 266 Color: 8

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 14
Size: 331 Color: 11
Size: 299 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 10
Size: 337 Color: 13
Size: 319 Color: 13

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 14
Size: 350 Color: 4
Size: 258 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 15
Size: 274 Color: 18
Size: 258 Color: 16

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 330 Color: 6
Size: 255 Color: 7

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 5
Size: 305 Color: 5
Size: 283 Color: 7

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 8
Size: 252 Color: 19
Size: 250 Color: 15

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 318 Color: 13
Size: 276 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 13
Size: 358 Color: 5
Size: 266 Color: 12

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 13
Size: 292 Color: 6
Size: 278 Color: 5

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 4
Size: 284 Color: 13
Size: 254 Color: 14

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 14
Size: 332 Color: 14
Size: 256 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 4
Size: 303 Color: 8
Size: 254 Color: 18

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 7
Size: 278 Color: 11
Size: 273 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 2
Size: 340 Color: 13
Size: 253 Color: 16

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 347 Color: 8
Size: 279 Color: 9

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 7
Size: 305 Color: 14
Size: 277 Color: 9

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 19
Size: 354 Color: 7
Size: 269 Color: 14

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 2
Size: 297 Color: 12
Size: 259 Color: 11

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 19
Size: 307 Color: 2
Size: 260 Color: 16

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 9
Size: 321 Color: 12
Size: 262 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 19
Size: 271 Color: 10
Size: 259 Color: 12

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 7
Size: 261 Color: 12
Size: 259 Color: 17

Total size: 83000
Total free space: 0

