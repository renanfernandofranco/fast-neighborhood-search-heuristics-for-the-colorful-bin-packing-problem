Capicity Bin: 7696
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 12
Items: 
Size: 3850 Color: 1
Size: 520 Color: 0
Size: 400 Color: 1
Size: 400 Color: 0
Size: 372 Color: 0
Size: 368 Color: 1
Size: 368 Color: 1
Size: 368 Color: 1
Size: 360 Color: 0
Size: 320 Color: 0
Size: 202 Color: 0
Size: 168 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3852 Color: 0
Size: 3204 Color: 1
Size: 640 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3876 Color: 1
Size: 3188 Color: 1
Size: 632 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 1
Size: 3164 Color: 1
Size: 624 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 0
Size: 2636 Color: 0
Size: 520 Color: 1

Bin 6: 0 of cap free
Amount of items: 5
Items: 
Size: 5162 Color: 0
Size: 1464 Color: 0
Size: 684 Color: 1
Size: 258 Color: 1
Size: 128 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5283 Color: 0
Size: 2011 Color: 1
Size: 402 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5462 Color: 1
Size: 2102 Color: 0
Size: 132 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 0
Size: 1654 Color: 1
Size: 328 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 0
Size: 1482 Color: 0
Size: 488 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5736 Color: 0
Size: 1788 Color: 0
Size: 172 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6169 Color: 0
Size: 1273 Color: 0
Size: 254 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 1
Size: 1281 Color: 1
Size: 254 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6177 Color: 0
Size: 1379 Color: 1
Size: 140 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 1
Size: 1288 Color: 1
Size: 138 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 0
Size: 954 Color: 0
Size: 260 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6552 Color: 1
Size: 848 Color: 0
Size: 296 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6536 Color: 0
Size: 832 Color: 0
Size: 328 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6578 Color: 1
Size: 934 Color: 0
Size: 184 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6580 Color: 1
Size: 644 Color: 0
Size: 472 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6818 Color: 1
Size: 700 Color: 0
Size: 178 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6890 Color: 1
Size: 652 Color: 0
Size: 154 Color: 1

Bin 23: 1 of cap free
Amount of items: 10
Items: 
Size: 3851 Color: 1
Size: 552 Color: 0
Size: 552 Color: 0
Size: 544 Color: 0
Size: 520 Color: 0
Size: 420 Color: 1
Size: 416 Color: 1
Size: 416 Color: 1
Size: 216 Color: 0
Size: 208 Color: 1

Bin 24: 1 of cap free
Amount of items: 5
Items: 
Size: 4556 Color: 1
Size: 1462 Color: 0
Size: 983 Color: 0
Size: 554 Color: 0
Size: 140 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 4807 Color: 0
Size: 2408 Color: 0
Size: 480 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 1
Size: 2411 Color: 0
Size: 144 Color: 1

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 5259 Color: 0
Size: 2436 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 5480 Color: 0
Size: 2031 Color: 1
Size: 184 Color: 0

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 5847 Color: 1
Size: 1848 Color: 0

Bin 30: 1 of cap free
Amount of items: 4
Items: 
Size: 6110 Color: 0
Size: 782 Color: 1
Size: 691 Color: 1
Size: 112 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6117 Color: 0
Size: 1176 Color: 1
Size: 402 Color: 1

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 6212 Color: 1
Size: 1483 Color: 0

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 6395 Color: 1
Size: 1300 Color: 0

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 6517 Color: 1
Size: 1178 Color: 0

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 6674 Color: 0
Size: 1021 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6767 Color: 0
Size: 640 Color: 1
Size: 288 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 0
Size: 624 Color: 0
Size: 288 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6825 Color: 1
Size: 854 Color: 0
Size: 16 Color: 1

Bin 39: 2 of cap free
Amount of items: 8
Items: 
Size: 3854 Color: 1
Size: 674 Color: 0
Size: 640 Color: 0
Size: 640 Color: 0
Size: 632 Color: 0
Size: 478 Color: 1
Size: 424 Color: 1
Size: 352 Color: 1

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 4802 Color: 1
Size: 2748 Color: 1
Size: 144 Color: 0

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 4806 Color: 0
Size: 2624 Color: 0
Size: 264 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 1
Size: 1600 Color: 1
Size: 354 Color: 0

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 6130 Color: 0
Size: 1564 Color: 1

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 6152 Color: 0
Size: 1306 Color: 0
Size: 236 Color: 1

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 1
Size: 968 Color: 0

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 0
Size: 2773 Color: 1
Size: 172 Color: 1

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 4827 Color: 1
Size: 2460 Color: 0
Size: 406 Color: 1

Bin 48: 3 of cap free
Amount of items: 2
Items: 
Size: 5917 Color: 0
Size: 1776 Color: 1

Bin 49: 3 of cap free
Amount of items: 2
Items: 
Size: 5922 Color: 0
Size: 1771 Color: 1

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 6140 Color: 0
Size: 921 Color: 0
Size: 632 Color: 1

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 6645 Color: 1
Size: 1048 Color: 0

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 1
Size: 785 Color: 0
Size: 16 Color: 0

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 6918 Color: 0
Size: 775 Color: 1

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 4780 Color: 0
Size: 2604 Color: 1
Size: 308 Color: 0

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 5207 Color: 0
Size: 2005 Color: 1
Size: 480 Color: 0

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 5328 Color: 0
Size: 2044 Color: 1
Size: 320 Color: 1

Bin 57: 4 of cap free
Amount of items: 3
Items: 
Size: 6114 Color: 0
Size: 1164 Color: 1
Size: 414 Color: 0

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 6120 Color: 0
Size: 1572 Color: 1

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 6138 Color: 1
Size: 1322 Color: 1
Size: 232 Color: 0

Bin 60: 5 of cap free
Amount of items: 3
Items: 
Size: 5571 Color: 0
Size: 1640 Color: 1
Size: 480 Color: 0

Bin 61: 5 of cap free
Amount of items: 2
Items: 
Size: 6398 Color: 1
Size: 1293 Color: 0

Bin 62: 5 of cap free
Amount of items: 2
Items: 
Size: 6501 Color: 1
Size: 1190 Color: 0

Bin 63: 5 of cap free
Amount of items: 2
Items: 
Size: 6564 Color: 0
Size: 1127 Color: 1

Bin 64: 5 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 0
Size: 1074 Color: 1

Bin 65: 5 of cap free
Amount of items: 4
Items: 
Size: 6705 Color: 0
Size: 866 Color: 1
Size: 88 Color: 1
Size: 32 Color: 0

Bin 66: 6 of cap free
Amount of items: 2
Items: 
Size: 5828 Color: 0
Size: 1862 Color: 1

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 0
Size: 932 Color: 1

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 6846 Color: 0
Size: 844 Color: 1

Bin 69: 6 of cap free
Amount of items: 4
Items: 
Size: 6916 Color: 1
Size: 734 Color: 0
Size: 24 Color: 0
Size: 16 Color: 1

Bin 70: 7 of cap free
Amount of items: 3
Items: 
Size: 5176 Color: 1
Size: 2369 Color: 0
Size: 144 Color: 1

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 5938 Color: 0
Size: 1751 Color: 1

Bin 72: 7 of cap free
Amount of items: 3
Items: 
Size: 6145 Color: 0
Size: 784 Color: 1
Size: 760 Color: 1

Bin 73: 7 of cap free
Amount of items: 2
Items: 
Size: 6692 Color: 0
Size: 997 Color: 1

Bin 74: 7 of cap free
Amount of items: 2
Items: 
Size: 6867 Color: 1
Size: 822 Color: 0

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 5556 Color: 1
Size: 2132 Color: 0

Bin 76: 8 of cap free
Amount of items: 2
Items: 
Size: 6300 Color: 1
Size: 1388 Color: 0

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 6386 Color: 0
Size: 1302 Color: 1

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 6444 Color: 1
Size: 1244 Color: 0

Bin 79: 9 of cap free
Amount of items: 2
Items: 
Size: 6221 Color: 1
Size: 1466 Color: 0

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 0
Size: 827 Color: 1

Bin 81: 10 of cap free
Amount of items: 2
Items: 
Size: 6642 Color: 1
Size: 1044 Color: 0

Bin 82: 11 of cap free
Amount of items: 2
Items: 
Size: 5275 Color: 0
Size: 2410 Color: 1

Bin 83: 11 of cap free
Amount of items: 2
Items: 
Size: 6924 Color: 1
Size: 761 Color: 0

Bin 84: 12 of cap free
Amount of items: 3
Items: 
Size: 6784 Color: 1
Size: 852 Color: 0
Size: 48 Color: 1

Bin 85: 12 of cap free
Amount of items: 2
Items: 
Size: 6876 Color: 1
Size: 808 Color: 0

Bin 86: 14 of cap free
Amount of items: 2
Items: 
Size: 5291 Color: 1
Size: 2391 Color: 0

Bin 87: 14 of cap free
Amount of items: 2
Items: 
Size: 6345 Color: 1
Size: 1337 Color: 0

Bin 88: 15 of cap free
Amount of items: 2
Items: 
Size: 6296 Color: 0
Size: 1385 Color: 1

Bin 89: 15 of cap free
Amount of items: 2
Items: 
Size: 6596 Color: 1
Size: 1085 Color: 0

Bin 90: 18 of cap free
Amount of items: 3
Items: 
Size: 5178 Color: 0
Size: 1850 Color: 0
Size: 650 Color: 1

Bin 91: 18 of cap free
Amount of items: 2
Items: 
Size: 5623 Color: 0
Size: 2055 Color: 1

Bin 92: 18 of cap free
Amount of items: 2
Items: 
Size: 6036 Color: 1
Size: 1642 Color: 0

Bin 93: 18 of cap free
Amount of items: 2
Items: 
Size: 6494 Color: 1
Size: 1184 Color: 0

Bin 94: 18 of cap free
Amount of items: 2
Items: 
Size: 6676 Color: 1
Size: 1002 Color: 0

Bin 95: 19 of cap free
Amount of items: 3
Items: 
Size: 4369 Color: 1
Size: 3172 Color: 0
Size: 136 Color: 0

Bin 96: 21 of cap free
Amount of items: 2
Items: 
Size: 5946 Color: 0
Size: 1729 Color: 1

Bin 97: 22 of cap free
Amount of items: 3
Items: 
Size: 4572 Color: 1
Size: 2620 Color: 0
Size: 482 Color: 1

Bin 98: 22 of cap free
Amount of items: 2
Items: 
Size: 6792 Color: 1
Size: 882 Color: 0

Bin 99: 23 of cap free
Amount of items: 2
Items: 
Size: 4808 Color: 0
Size: 2865 Color: 1

Bin 100: 24 of cap free
Amount of items: 3
Items: 
Size: 3892 Color: 0
Size: 3104 Color: 1
Size: 676 Color: 0

Bin 101: 24 of cap free
Amount of items: 2
Items: 
Size: 5812 Color: 1
Size: 1860 Color: 0

Bin 102: 24 of cap free
Amount of items: 2
Items: 
Size: 6658 Color: 0
Size: 1014 Color: 1

Bin 103: 25 of cap free
Amount of items: 2
Items: 
Size: 6035 Color: 1
Size: 1636 Color: 0

Bin 104: 26 of cap free
Amount of items: 2
Items: 
Size: 5595 Color: 0
Size: 2075 Color: 1

Bin 105: 28 of cap free
Amount of items: 2
Items: 
Size: 6744 Color: 0
Size: 924 Color: 1

Bin 106: 29 of cap free
Amount of items: 4
Items: 
Size: 5944 Color: 1
Size: 1541 Color: 0
Size: 96 Color: 0
Size: 86 Color: 1

Bin 107: 30 of cap free
Amount of items: 2
Items: 
Size: 5252 Color: 1
Size: 2414 Color: 0

Bin 108: 33 of cap free
Amount of items: 3
Items: 
Size: 6286 Color: 1
Size: 1317 Color: 0
Size: 60 Color: 0

Bin 109: 36 of cap free
Amount of items: 3
Items: 
Size: 5231 Color: 0
Size: 2019 Color: 1
Size: 410 Color: 0

Bin 110: 39 of cap free
Amount of items: 2
Items: 
Size: 6593 Color: 1
Size: 1064 Color: 0

Bin 111: 40 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 0
Size: 1094 Color: 1

Bin 112: 41 of cap free
Amount of items: 2
Items: 
Size: 6424 Color: 1
Size: 1231 Color: 0

Bin 113: 43 of cap free
Amount of items: 2
Items: 
Size: 6093 Color: 1
Size: 1560 Color: 0

Bin 114: 48 of cap free
Amount of items: 30
Items: 
Size: 350 Color: 1
Size: 344 Color: 1
Size: 312 Color: 1
Size: 304 Color: 1
Size: 292 Color: 1
Size: 292 Color: 1
Size: 288 Color: 1
Size: 288 Color: 1
Size: 276 Color: 0
Size: 272 Color: 0
Size: 266 Color: 0
Size: 262 Color: 1
Size: 260 Color: 1
Size: 256 Color: 0
Size: 256 Color: 0
Size: 256 Color: 0
Size: 244 Color: 0
Size: 240 Color: 0
Size: 240 Color: 0
Size: 232 Color: 0
Size: 224 Color: 1
Size: 224 Color: 1
Size: 224 Color: 0
Size: 216 Color: 1
Size: 216 Color: 0
Size: 208 Color: 1
Size: 208 Color: 0
Size: 200 Color: 1
Size: 200 Color: 0
Size: 198 Color: 0

Bin 115: 52 of cap free
Amount of items: 2
Items: 
Size: 4436 Color: 1
Size: 3208 Color: 0

Bin 116: 58 of cap free
Amount of items: 2
Items: 
Size: 6316 Color: 1
Size: 1322 Color: 0

Bin 117: 65 of cap free
Amount of items: 2
Items: 
Size: 4855 Color: 1
Size: 2776 Color: 0

Bin 118: 66 of cap free
Amount of items: 2
Items: 
Size: 6548 Color: 0
Size: 1082 Color: 1

Bin 119: 67 of cap free
Amount of items: 2
Items: 
Size: 6473 Color: 1
Size: 1156 Color: 0

Bin 120: 76 of cap free
Amount of items: 3
Items: 
Size: 3864 Color: 1
Size: 3204 Color: 1
Size: 552 Color: 0

Bin 121: 85 of cap free
Amount of items: 2
Items: 
Size: 6710 Color: 0
Size: 901 Color: 1

Bin 122: 88 of cap free
Amount of items: 2
Items: 
Size: 4588 Color: 1
Size: 3020 Color: 0

Bin 123: 95 of cap free
Amount of items: 6
Items: 
Size: 3860 Color: 1
Size: 963 Color: 0
Size: 948 Color: 0
Size: 710 Color: 0
Size: 640 Color: 1
Size: 480 Color: 1

Bin 124: 104 of cap free
Amount of items: 2
Items: 
Size: 5478 Color: 1
Size: 2114 Color: 0

Bin 125: 106 of cap free
Amount of items: 2
Items: 
Size: 4816 Color: 1
Size: 2774 Color: 0

Bin 126: 112 of cap free
Amount of items: 3
Items: 
Size: 3849 Color: 0
Size: 2771 Color: 1
Size: 964 Color: 0

Bin 127: 113 of cap free
Amount of items: 2
Items: 
Size: 4376 Color: 1
Size: 3207 Color: 0

Bin 128: 116 of cap free
Amount of items: 2
Items: 
Size: 4374 Color: 1
Size: 3206 Color: 0

Bin 129: 118 of cap free
Amount of items: 2
Items: 
Size: 4373 Color: 1
Size: 3205 Color: 0

Bin 130: 123 of cap free
Amount of items: 2
Items: 
Size: 4803 Color: 1
Size: 2770 Color: 0

Bin 131: 124 of cap free
Amount of items: 2
Items: 
Size: 4370 Color: 1
Size: 3202 Color: 0

Bin 132: 124 of cap free
Amount of items: 2
Items: 
Size: 5468 Color: 1
Size: 2104 Color: 0

Bin 133: 4986 of cap free
Amount of items: 17
Items: 
Size: 196 Color: 0
Size: 184 Color: 1
Size: 184 Color: 0
Size: 182 Color: 1
Size: 176 Color: 1
Size: 176 Color: 0
Size: 168 Color: 1
Size: 164 Color: 1
Size: 164 Color: 1
Size: 160 Color: 0
Size: 156 Color: 0
Size: 152 Color: 1
Size: 136 Color: 0
Size: 128 Color: 1
Size: 128 Color: 0
Size: 128 Color: 0
Size: 128 Color: 0

Total size: 1015872
Total free space: 7696

