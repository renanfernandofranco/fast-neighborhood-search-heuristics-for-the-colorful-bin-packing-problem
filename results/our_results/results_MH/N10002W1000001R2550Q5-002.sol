Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 447415 Color: 0
Size: 279892 Color: 1
Size: 272694 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 460404 Color: 2
Size: 281871 Color: 0
Size: 257726 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 458145 Color: 4
Size: 279851 Color: 3
Size: 262005 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 411114 Color: 2
Size: 310084 Color: 1
Size: 278803 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 410919 Color: 3
Size: 333198 Color: 2
Size: 255884 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 369150 Color: 3
Size: 354486 Color: 3
Size: 276365 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 443315 Color: 1
Size: 297835 Color: 3
Size: 258851 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 376659 Color: 2
Size: 349657 Color: 2
Size: 273685 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 395419 Color: 2
Size: 328733 Color: 0
Size: 275849 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 369659 Color: 2
Size: 367305 Color: 3
Size: 263037 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 419062 Color: 2
Size: 311811 Color: 0
Size: 269128 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 380185 Color: 3
Size: 354056 Color: 1
Size: 265760 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 361218 Color: 4
Size: 327515 Color: 1
Size: 311268 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 392278 Color: 3
Size: 319475 Color: 1
Size: 288248 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 408530 Color: 3
Size: 333466 Color: 4
Size: 258005 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 477685 Color: 3
Size: 267364 Color: 3
Size: 254952 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 416072 Color: 1
Size: 306780 Color: 0
Size: 277149 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 415719 Color: 4
Size: 333243 Color: 2
Size: 251039 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 394813 Color: 2
Size: 344643 Color: 4
Size: 260545 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 367524 Color: 3
Size: 322850 Color: 0
Size: 309627 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 400304 Color: 3
Size: 328836 Color: 1
Size: 270861 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 361117 Color: 4
Size: 338253 Color: 2
Size: 300631 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 428700 Color: 2
Size: 320431 Color: 3
Size: 250870 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 339816 Color: 2
Size: 337638 Color: 2
Size: 322547 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 392623 Color: 3
Size: 333284 Color: 4
Size: 274094 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 468511 Color: 4
Size: 277651 Color: 2
Size: 253839 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 382591 Color: 0
Size: 343694 Color: 1
Size: 273716 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 380327 Color: 2
Size: 345614 Color: 2
Size: 274060 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 382874 Color: 2
Size: 362173 Color: 4
Size: 254954 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 403767 Color: 1
Size: 305876 Color: 4
Size: 290358 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 473473 Color: 4
Size: 268326 Color: 1
Size: 258202 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 381956 Color: 0
Size: 325078 Color: 4
Size: 292967 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 359664 Color: 2
Size: 343921 Color: 4
Size: 296416 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 469713 Color: 0
Size: 271292 Color: 4
Size: 258996 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 371977 Color: 4
Size: 335018 Color: 4
Size: 293006 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 370328 Color: 0
Size: 351363 Color: 3
Size: 278310 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 461254 Color: 4
Size: 275635 Color: 1
Size: 263112 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 377169 Color: 2
Size: 356809 Color: 0
Size: 266023 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 408854 Color: 4
Size: 337897 Color: 0
Size: 253250 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 418682 Color: 1
Size: 326585 Color: 4
Size: 254734 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 390902 Color: 2
Size: 307136 Color: 4
Size: 301963 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 436026 Color: 1
Size: 301414 Color: 4
Size: 262561 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 488309 Color: 3
Size: 260244 Color: 2
Size: 251448 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 444816 Color: 2
Size: 282219 Color: 4
Size: 272966 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 379940 Color: 1
Size: 317270 Color: 0
Size: 302791 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 421795 Color: 2
Size: 308682 Color: 1
Size: 269524 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 372831 Color: 0
Size: 346428 Color: 2
Size: 280742 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 377492 Color: 2
Size: 319588 Color: 3
Size: 302921 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 357748 Color: 1
Size: 342819 Color: 1
Size: 299434 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 496653 Color: 4
Size: 252998 Color: 3
Size: 250350 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 405431 Color: 4
Size: 304614 Color: 0
Size: 289956 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 421536 Color: 4
Size: 324937 Color: 3
Size: 253528 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 429453 Color: 4
Size: 305877 Color: 0
Size: 264671 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 367944 Color: 1
Size: 357564 Color: 2
Size: 274493 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 396610 Color: 2
Size: 349958 Color: 1
Size: 253433 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 389245 Color: 4
Size: 338762 Color: 1
Size: 271994 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 394262 Color: 1
Size: 306096 Color: 0
Size: 299643 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 481789 Color: 4
Size: 259119 Color: 2
Size: 259093 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 464785 Color: 1
Size: 275983 Color: 2
Size: 259233 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 440878 Color: 0
Size: 302506 Color: 4
Size: 256617 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 371313 Color: 2
Size: 344729 Color: 1
Size: 283959 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 430420 Color: 0
Size: 304806 Color: 2
Size: 264775 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 473237 Color: 4
Size: 265911 Color: 2
Size: 260853 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 430971 Color: 0
Size: 287987 Color: 2
Size: 281043 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 394749 Color: 4
Size: 334951 Color: 2
Size: 270301 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 455389 Color: 3
Size: 279981 Color: 2
Size: 264631 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 377142 Color: 0
Size: 331569 Color: 1
Size: 291290 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 431765 Color: 3
Size: 300419 Color: 4
Size: 267817 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 487605 Color: 1
Size: 256394 Color: 4
Size: 256002 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 449582 Color: 0
Size: 276890 Color: 1
Size: 273529 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 450827 Color: 3
Size: 279104 Color: 1
Size: 270070 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 456622 Color: 4
Size: 281572 Color: 3
Size: 261807 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 470175 Color: 0
Size: 271566 Color: 3
Size: 258260 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 468486 Color: 4
Size: 276281 Color: 1
Size: 255234 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 412230 Color: 1
Size: 328769 Color: 3
Size: 259002 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 496483 Color: 0
Size: 252869 Color: 3
Size: 250649 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 399143 Color: 2
Size: 324526 Color: 4
Size: 276332 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 436918 Color: 0
Size: 288690 Color: 1
Size: 274393 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 441243 Color: 0
Size: 280768 Color: 1
Size: 277990 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 490338 Color: 2
Size: 255867 Color: 1
Size: 253796 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 447624 Color: 3
Size: 294372 Color: 0
Size: 258005 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 422811 Color: 1
Size: 311618 Color: 3
Size: 265572 Color: 3

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 421781 Color: 1
Size: 299646 Color: 3
Size: 278574 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 471623 Color: 2
Size: 270207 Color: 3
Size: 258171 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 384976 Color: 2
Size: 348991 Color: 3
Size: 266034 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 371691 Color: 1
Size: 327100 Color: 2
Size: 301210 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 466511 Color: 0
Size: 268121 Color: 4
Size: 265369 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 470357 Color: 0
Size: 267643 Color: 2
Size: 262001 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 471896 Color: 2
Size: 273274 Color: 1
Size: 254831 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 401526 Color: 3
Size: 334209 Color: 1
Size: 264266 Color: 2

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 368339 Color: 0
Size: 328999 Color: 3
Size: 302663 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 429768 Color: 4
Size: 287980 Color: 0
Size: 282253 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 424709 Color: 3
Size: 319674 Color: 4
Size: 255618 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 412376 Color: 3
Size: 324181 Color: 1
Size: 263444 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 375052 Color: 1
Size: 342068 Color: 4
Size: 282881 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 424347 Color: 2
Size: 298843 Color: 0
Size: 276811 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 390116 Color: 0
Size: 353729 Color: 4
Size: 256156 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 373777 Color: 1
Size: 335939 Color: 1
Size: 290285 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 362900 Color: 1
Size: 360220 Color: 3
Size: 276881 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 380191 Color: 1
Size: 324787 Color: 3
Size: 295023 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 460287 Color: 2
Size: 279380 Color: 3
Size: 260334 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 372996 Color: 1
Size: 321352 Color: 3
Size: 305653 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 388325 Color: 3
Size: 330321 Color: 2
Size: 281355 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 394402 Color: 4
Size: 308216 Color: 4
Size: 297383 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 349949 Color: 1
Size: 340424 Color: 3
Size: 309628 Color: 4

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 446187 Color: 3
Size: 285520 Color: 2
Size: 268294 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 466970 Color: 0
Size: 268756 Color: 4
Size: 264275 Color: 2

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 386620 Color: 3
Size: 333564 Color: 2
Size: 279817 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 381957 Color: 3
Size: 332490 Color: 0
Size: 285554 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 413456 Color: 0
Size: 327897 Color: 1
Size: 258648 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 420357 Color: 3
Size: 291069 Color: 2
Size: 288575 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 435107 Color: 0
Size: 289718 Color: 2
Size: 275176 Color: 2

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 379838 Color: 2
Size: 312451 Color: 4
Size: 307712 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 386904 Color: 1
Size: 353242 Color: 3
Size: 259855 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 361019 Color: 3
Size: 336672 Color: 1
Size: 302310 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 431011 Color: 1
Size: 303630 Color: 3
Size: 265360 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 343499 Color: 0
Size: 341581 Color: 3
Size: 314921 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 402915 Color: 1
Size: 342730 Color: 4
Size: 254356 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 407961 Color: 2
Size: 303909 Color: 4
Size: 288131 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 358699 Color: 2
Size: 333693 Color: 3
Size: 307609 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 378060 Color: 4
Size: 364492 Color: 1
Size: 257449 Color: 3

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 384271 Color: 4
Size: 348546 Color: 2
Size: 267184 Color: 3

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 426126 Color: 2
Size: 293935 Color: 0
Size: 279940 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 458203 Color: 4
Size: 282799 Color: 0
Size: 258999 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 435749 Color: 1
Size: 308752 Color: 1
Size: 255500 Color: 2

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 442739 Color: 0
Size: 289703 Color: 2
Size: 267559 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 448969 Color: 1
Size: 281251 Color: 1
Size: 269781 Color: 3

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 369857 Color: 2
Size: 318987 Color: 3
Size: 311157 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 352409 Color: 3
Size: 333172 Color: 3
Size: 314420 Color: 4

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 404409 Color: 0
Size: 318427 Color: 3
Size: 277165 Color: 4

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 378393 Color: 3
Size: 311002 Color: 1
Size: 310606 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 433013 Color: 1
Size: 285488 Color: 3
Size: 281500 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 495957 Color: 1
Size: 253212 Color: 4
Size: 250832 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 366305 Color: 3
Size: 328818 Color: 0
Size: 304878 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 382310 Color: 2
Size: 313396 Color: 0
Size: 304295 Color: 4

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 430256 Color: 2
Size: 286262 Color: 4
Size: 283483 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 465816 Color: 4
Size: 269026 Color: 0
Size: 265159 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 480160 Color: 4
Size: 261552 Color: 4
Size: 258289 Color: 3

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 492224 Color: 0
Size: 254273 Color: 3
Size: 253504 Color: 3

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 491146 Color: 1
Size: 255107 Color: 2
Size: 253748 Color: 3

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 424890 Color: 4
Size: 290506 Color: 0
Size: 284605 Color: 3

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 493425 Color: 0
Size: 256527 Color: 4
Size: 250049 Color: 3

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 414941 Color: 3
Size: 296498 Color: 1
Size: 288562 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 371528 Color: 3
Size: 316203 Color: 1
Size: 312270 Color: 2

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 453562 Color: 2
Size: 290896 Color: 3
Size: 255543 Color: 4

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 430909 Color: 2
Size: 317339 Color: 0
Size: 251753 Color: 2

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 432353 Color: 4
Size: 315896 Color: 0
Size: 251752 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 377011 Color: 1
Size: 351000 Color: 0
Size: 271990 Color: 2

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 404054 Color: 2
Size: 342038 Color: 0
Size: 253909 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 445339 Color: 1
Size: 302178 Color: 3
Size: 252484 Color: 2

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 450045 Color: 1
Size: 292683 Color: 0
Size: 257273 Color: 2

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 369095 Color: 4
Size: 353854 Color: 1
Size: 277052 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 405494 Color: 3
Size: 320607 Color: 1
Size: 273900 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 484218 Color: 4
Size: 265625 Color: 0
Size: 250158 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 437839 Color: 1
Size: 309047 Color: 2
Size: 253115 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 454872 Color: 4
Size: 287582 Color: 1
Size: 257547 Color: 3

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 386585 Color: 3
Size: 317972 Color: 4
Size: 295444 Color: 4

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 458147 Color: 3
Size: 289577 Color: 0
Size: 252277 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 470033 Color: 4
Size: 275739 Color: 2
Size: 254229 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 452641 Color: 0
Size: 281412 Color: 1
Size: 265948 Color: 4

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 413950 Color: 4
Size: 318643 Color: 0
Size: 267408 Color: 3

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 373297 Color: 4
Size: 316371 Color: 1
Size: 310333 Color: 2

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 437938 Color: 4
Size: 286870 Color: 2
Size: 275193 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 420180 Color: 0
Size: 324096 Color: 3
Size: 255725 Color: 4

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 411316 Color: 2
Size: 295281 Color: 0
Size: 293404 Color: 3

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 409573 Color: 1
Size: 312114 Color: 0
Size: 278314 Color: 3

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 440717 Color: 4
Size: 301544 Color: 3
Size: 257740 Color: 2

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 410614 Color: 1
Size: 316114 Color: 0
Size: 273273 Color: 2

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 403544 Color: 2
Size: 320218 Color: 4
Size: 276239 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 434556 Color: 3
Size: 292422 Color: 1
Size: 273023 Color: 4

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 377288 Color: 0
Size: 324405 Color: 2
Size: 298308 Color: 2

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 379175 Color: 0
Size: 324258 Color: 4
Size: 296568 Color: 2

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 498193 Color: 1
Size: 251151 Color: 0
Size: 250657 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 353348 Color: 2
Size: 347800 Color: 3
Size: 298853 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 475688 Color: 2
Size: 264595 Color: 0
Size: 259718 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 476758 Color: 0
Size: 272016 Color: 1
Size: 251227 Color: 3

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 387419 Color: 3
Size: 312760 Color: 0
Size: 299822 Color: 2

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 432130 Color: 0
Size: 286800 Color: 3
Size: 281071 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 377334 Color: 0
Size: 340906 Color: 4
Size: 281761 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 403742 Color: 2
Size: 343415 Color: 1
Size: 252844 Color: 2

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 453576 Color: 1
Size: 282373 Color: 4
Size: 264052 Color: 2

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 480119 Color: 2
Size: 268295 Color: 1
Size: 251587 Color: 4

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 443997 Color: 4
Size: 291349 Color: 4
Size: 264655 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 411349 Color: 3
Size: 327993 Color: 1
Size: 260659 Color: 2

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 422224 Color: 2
Size: 294996 Color: 1
Size: 282781 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 352782 Color: 3
Size: 351554 Color: 2
Size: 295665 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 403224 Color: 2
Size: 330204 Color: 4
Size: 266573 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 340525 Color: 1
Size: 331830 Color: 3
Size: 327646 Color: 3

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 411006 Color: 3
Size: 323460 Color: 4
Size: 265535 Color: 4

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 399687 Color: 2
Size: 312654 Color: 1
Size: 287660 Color: 3

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 492200 Color: 4
Size: 257277 Color: 3
Size: 250524 Color: 2

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 392672 Color: 1
Size: 338157 Color: 3
Size: 269172 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 424587 Color: 3
Size: 325237 Color: 0
Size: 250177 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 473324 Color: 1
Size: 271931 Color: 1
Size: 254746 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 392548 Color: 0
Size: 311281 Color: 3
Size: 296172 Color: 2

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 467033 Color: 0
Size: 276755 Color: 4
Size: 256213 Color: 2

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 392566 Color: 1
Size: 305840 Color: 4
Size: 301595 Color: 3

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 394069 Color: 1
Size: 355593 Color: 3
Size: 250339 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 491111 Color: 2
Size: 255671 Color: 2
Size: 253219 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 471251 Color: 2
Size: 277177 Color: 3
Size: 251573 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 477089 Color: 1
Size: 264055 Color: 0
Size: 258857 Color: 2

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 461905 Color: 1
Size: 282583 Color: 4
Size: 255513 Color: 2

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 429444 Color: 4
Size: 316929 Color: 0
Size: 253628 Color: 2

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 373471 Color: 4
Size: 334890 Color: 4
Size: 291640 Color: 3

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 492572 Color: 3
Size: 255742 Color: 0
Size: 251687 Color: 4

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 431286 Color: 4
Size: 315977 Color: 1
Size: 252738 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 493544 Color: 0
Size: 255488 Color: 3
Size: 250969 Color: 4

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 481072 Color: 0
Size: 268289 Color: 1
Size: 250640 Color: 4

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 418399 Color: 1
Size: 303989 Color: 4
Size: 277613 Color: 4

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 391103 Color: 0
Size: 309156 Color: 1
Size: 299742 Color: 2

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 399079 Color: 2
Size: 339583 Color: 3
Size: 261339 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 370271 Color: 1
Size: 317588 Color: 3
Size: 312142 Color: 1

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 389900 Color: 1
Size: 357372 Color: 3
Size: 252729 Color: 2

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 420515 Color: 0
Size: 322110 Color: 0
Size: 257376 Color: 4

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 355809 Color: 1
Size: 333646 Color: 2
Size: 310546 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 365806 Color: 2
Size: 338844 Color: 3
Size: 295351 Color: 3

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 462594 Color: 2
Size: 279563 Color: 0
Size: 257844 Color: 3

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 426482 Color: 2
Size: 296210 Color: 0
Size: 277309 Color: 3

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 440806 Color: 2
Size: 304681 Color: 1
Size: 254514 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 384194 Color: 3
Size: 339906 Color: 3
Size: 275901 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 453356 Color: 0
Size: 287813 Color: 2
Size: 258832 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 379371 Color: 4
Size: 313059 Color: 3
Size: 307571 Color: 2

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 374623 Color: 1
Size: 366747 Color: 0
Size: 258631 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 478355 Color: 2
Size: 266692 Color: 1
Size: 254954 Color: 2

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 386606 Color: 3
Size: 363365 Color: 4
Size: 250030 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 372650 Color: 0
Size: 352411 Color: 1
Size: 274940 Color: 2

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 420160 Color: 2
Size: 312691 Color: 4
Size: 267150 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 435711 Color: 0
Size: 311463 Color: 1
Size: 252827 Color: 3

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 430179 Color: 2
Size: 297988 Color: 3
Size: 271834 Color: 4

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 415174 Color: 4
Size: 329837 Color: 2
Size: 254990 Color: 2

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 343100 Color: 1
Size: 334123 Color: 1
Size: 322778 Color: 2

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 448427 Color: 1
Size: 285353 Color: 3
Size: 266221 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 391412 Color: 3
Size: 343774 Color: 0
Size: 264815 Color: 4

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 436724 Color: 2
Size: 310220 Color: 0
Size: 253057 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 376049 Color: 3
Size: 360503 Color: 0
Size: 263449 Color: 3

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 470746 Color: 4
Size: 274936 Color: 2
Size: 254319 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 409722 Color: 4
Size: 315017 Color: 0
Size: 275262 Color: 3

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 404232 Color: 3
Size: 342523 Color: 4
Size: 253246 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 436235 Color: 0
Size: 294596 Color: 1
Size: 269170 Color: 2

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 382002 Color: 1
Size: 328192 Color: 3
Size: 289807 Color: 2

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 427490 Color: 0
Size: 321218 Color: 2
Size: 251293 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 423461 Color: 0
Size: 295380 Color: 4
Size: 281160 Color: 1

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 440089 Color: 3
Size: 289944 Color: 0
Size: 269968 Color: 3

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 450779 Color: 3
Size: 294729 Color: 1
Size: 254493 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 462711 Color: 3
Size: 275900 Color: 2
Size: 261390 Color: 4

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 345155 Color: 0
Size: 337427 Color: 2
Size: 317419 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 404723 Color: 0
Size: 327281 Color: 3
Size: 267997 Color: 4

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 382830 Color: 1
Size: 366094 Color: 0
Size: 251077 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 344869 Color: 1
Size: 337956 Color: 3
Size: 317176 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 386284 Color: 3
Size: 359248 Color: 4
Size: 254469 Color: 4

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 486144 Color: 1
Size: 258443 Color: 2
Size: 255414 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 377356 Color: 2
Size: 336478 Color: 0
Size: 286167 Color: 4

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 381270 Color: 4
Size: 327722 Color: 0
Size: 291009 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 476071 Color: 0
Size: 272842 Color: 1
Size: 251088 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 465320 Color: 0
Size: 273125 Color: 1
Size: 261556 Color: 2

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 454646 Color: 2
Size: 286563 Color: 2
Size: 258792 Color: 3

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 442480 Color: 4
Size: 295534 Color: 0
Size: 261987 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 399558 Color: 3
Size: 339210 Color: 0
Size: 261233 Color: 4

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 397587 Color: 0
Size: 314994 Color: 3
Size: 287420 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 474078 Color: 1
Size: 268153 Color: 3
Size: 257770 Color: 2

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 362124 Color: 3
Size: 320679 Color: 4
Size: 317198 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 461309 Color: 1
Size: 281752 Color: 3
Size: 256940 Color: 3

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 478107 Color: 1
Size: 261860 Color: 4
Size: 260034 Color: 2

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 499154 Color: 4
Size: 250527 Color: 3
Size: 250320 Color: 3

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 395861 Color: 1
Size: 329351 Color: 4
Size: 274789 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 454431 Color: 1
Size: 281057 Color: 4
Size: 264513 Color: 2

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 479810 Color: 1
Size: 260979 Color: 4
Size: 259212 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 351120 Color: 3
Size: 345423 Color: 1
Size: 303458 Color: 4

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 387903 Color: 3
Size: 308374 Color: 1
Size: 303724 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 357910 Color: 1
Size: 324082 Color: 4
Size: 318009 Color: 3

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 422937 Color: 0
Size: 291681 Color: 1
Size: 285383 Color: 3

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 375200 Color: 3
Size: 343174 Color: 3
Size: 281627 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 349096 Color: 1
Size: 337536 Color: 4
Size: 313369 Color: 2

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 382619 Color: 0
Size: 355512 Color: 1
Size: 261870 Color: 2

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 389489 Color: 0
Size: 347344 Color: 1
Size: 263168 Color: 2

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 348086 Color: 2
Size: 347209 Color: 4
Size: 304706 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 358874 Color: 1
Size: 330412 Color: 4
Size: 310715 Color: 3

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 472575 Color: 1
Size: 275711 Color: 0
Size: 251715 Color: 4

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 455247 Color: 3
Size: 277568 Color: 0
Size: 267186 Color: 1

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 407123 Color: 2
Size: 316508 Color: 2
Size: 276370 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 424157 Color: 4
Size: 312623 Color: 2
Size: 263221 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 373917 Color: 0
Size: 329482 Color: 4
Size: 296602 Color: 1

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 484109 Color: 3
Size: 260631 Color: 4
Size: 255261 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 401882 Color: 1
Size: 324313 Color: 2
Size: 273806 Color: 3

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 415956 Color: 4
Size: 330046 Color: 0
Size: 253999 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 380347 Color: 2
Size: 339848 Color: 4
Size: 279806 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 382462 Color: 4
Size: 312921 Color: 3
Size: 304618 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 407408 Color: 0
Size: 299304 Color: 1
Size: 293289 Color: 3

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 367009 Color: 4
Size: 358823 Color: 1
Size: 274169 Color: 2

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 388715 Color: 1
Size: 340454 Color: 0
Size: 270832 Color: 3

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 367853 Color: 1
Size: 335568 Color: 2
Size: 296580 Color: 2

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 483979 Color: 4
Size: 260887 Color: 3
Size: 255135 Color: 2

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 342563 Color: 2
Size: 341880 Color: 4
Size: 315558 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 435118 Color: 2
Size: 286022 Color: 4
Size: 278861 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 389848 Color: 4
Size: 306199 Color: 1
Size: 303954 Color: 3

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 430761 Color: 1
Size: 286737 Color: 3
Size: 282503 Color: 3

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 423405 Color: 2
Size: 326520 Color: 2
Size: 250076 Color: 3

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 466850 Color: 3
Size: 274759 Color: 1
Size: 258392 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 433493 Color: 4
Size: 294329 Color: 3
Size: 272179 Color: 3

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 374285 Color: 4
Size: 317166 Color: 1
Size: 308550 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 433151 Color: 3
Size: 302034 Color: 1
Size: 264816 Color: 2

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 373086 Color: 0
Size: 344878 Color: 1
Size: 282037 Color: 2

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 482987 Color: 4
Size: 259772 Color: 0
Size: 257242 Color: 2

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 416472 Color: 2
Size: 293210 Color: 1
Size: 290319 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 442299 Color: 2
Size: 279085 Color: 4
Size: 278617 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 494598 Color: 1
Size: 254275 Color: 1
Size: 251128 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 367929 Color: 1
Size: 323216 Color: 3
Size: 308856 Color: 4

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 498140 Color: 1
Size: 251027 Color: 0
Size: 250834 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 378316 Color: 0
Size: 354240 Color: 2
Size: 267445 Color: 3

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 375356 Color: 1
Size: 325460 Color: 0
Size: 299185 Color: 3

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 476575 Color: 3
Size: 270999 Color: 0
Size: 252427 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 350085 Color: 0
Size: 326946 Color: 3
Size: 322970 Color: 3

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 467738 Color: 4
Size: 276378 Color: 4
Size: 255885 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 449828 Color: 0
Size: 283849 Color: 4
Size: 266324 Color: 3

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 415877 Color: 4
Size: 293172 Color: 0
Size: 290952 Color: 4

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 413251 Color: 0
Size: 336215 Color: 1
Size: 250535 Color: 4

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 435092 Color: 1
Size: 285034 Color: 3
Size: 279875 Color: 2

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 431466 Color: 4
Size: 316132 Color: 4
Size: 252403 Color: 3

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 467710 Color: 1
Size: 273413 Color: 0
Size: 258878 Color: 4

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 423671 Color: 0
Size: 305844 Color: 1
Size: 270486 Color: 3

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 432947 Color: 2
Size: 302779 Color: 1
Size: 264275 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 352790 Color: 1
Size: 333419 Color: 0
Size: 313792 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 360808 Color: 0
Size: 344247 Color: 1
Size: 294946 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 425805 Color: 3
Size: 317178 Color: 4
Size: 257018 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 377639 Color: 4
Size: 334323 Color: 2
Size: 288039 Color: 3

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 410277 Color: 1
Size: 314499 Color: 3
Size: 275225 Color: 2

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 433554 Color: 1
Size: 300830 Color: 4
Size: 265617 Color: 2

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 483833 Color: 4
Size: 258466 Color: 3
Size: 257702 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 390893 Color: 2
Size: 305320 Color: 1
Size: 303788 Color: 3

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 477120 Color: 3
Size: 267470 Color: 4
Size: 255411 Color: 2

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 478851 Color: 4
Size: 268576 Color: 4
Size: 252574 Color: 2

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 381180 Color: 1
Size: 348067 Color: 4
Size: 270754 Color: 3

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 381298 Color: 1
Size: 326891 Color: 2
Size: 291812 Color: 3

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 430296 Color: 4
Size: 293751 Color: 1
Size: 275954 Color: 4

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 377273 Color: 3
Size: 341318 Color: 2
Size: 281410 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 398987 Color: 0
Size: 326030 Color: 1
Size: 274984 Color: 2

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 403644 Color: 1
Size: 335024 Color: 4
Size: 261333 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 445336 Color: 1
Size: 288524 Color: 0
Size: 266141 Color: 4

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 416216 Color: 0
Size: 332226 Color: 2
Size: 251559 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 389508 Color: 4
Size: 350958 Color: 2
Size: 259535 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 470170 Color: 0
Size: 268624 Color: 4
Size: 261207 Color: 3

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 370828 Color: 0
Size: 319961 Color: 3
Size: 309212 Color: 3

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 422028 Color: 1
Size: 312937 Color: 0
Size: 265036 Color: 2

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 354856 Color: 1
Size: 331943 Color: 4
Size: 313202 Color: 2

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 463529 Color: 1
Size: 269949 Color: 0
Size: 266523 Color: 2

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 395807 Color: 4
Size: 315658 Color: 0
Size: 288536 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 394583 Color: 1
Size: 353145 Color: 1
Size: 252273 Color: 2

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 393316 Color: 0
Size: 352187 Color: 4
Size: 254498 Color: 2

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 455325 Color: 2
Size: 272383 Color: 1
Size: 272293 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 497824 Color: 4
Size: 251622 Color: 2
Size: 250555 Color: 3

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 363756 Color: 4
Size: 320164 Color: 4
Size: 316081 Color: 2

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 443651 Color: 2
Size: 295667 Color: 3
Size: 260683 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 367239 Color: 4
Size: 321382 Color: 2
Size: 311380 Color: 4

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 421760 Color: 4
Size: 325630 Color: 0
Size: 252611 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 393489 Color: 2
Size: 353430 Color: 0
Size: 253082 Color: 4

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 395345 Color: 0
Size: 327577 Color: 2
Size: 277079 Color: 3

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 383507 Color: 3
Size: 321751 Color: 1
Size: 294743 Color: 4

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 369630 Color: 0
Size: 332702 Color: 4
Size: 297669 Color: 1

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 371737 Color: 0
Size: 340492 Color: 4
Size: 287772 Color: 3

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 418936 Color: 4
Size: 295967 Color: 3
Size: 285098 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 453032 Color: 0
Size: 279180 Color: 1
Size: 267789 Color: 3

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 384126 Color: 4
Size: 330366 Color: 1
Size: 285509 Color: 4

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 366541 Color: 2
Size: 361366 Color: 0
Size: 272094 Color: 2

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 370005 Color: 1
Size: 366414 Color: 3
Size: 263582 Color: 4

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 413820 Color: 0
Size: 310072 Color: 4
Size: 276109 Color: 4

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 368952 Color: 1
Size: 319747 Color: 4
Size: 311302 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 464239 Color: 1
Size: 275345 Color: 3
Size: 260417 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 438823 Color: 1
Size: 281205 Color: 3
Size: 279973 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 413644 Color: 1
Size: 329476 Color: 0
Size: 256881 Color: 2

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 389580 Color: 1
Size: 334677 Color: 2
Size: 275744 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 387239 Color: 0
Size: 340008 Color: 3
Size: 272754 Color: 3

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 429541 Color: 3
Size: 315964 Color: 4
Size: 254496 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 447723 Color: 4
Size: 290286 Color: 2
Size: 261992 Color: 1

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 482366 Color: 0
Size: 267077 Color: 1
Size: 250558 Color: 2

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 413564 Color: 1
Size: 297895 Color: 0
Size: 288542 Color: 4

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 409864 Color: 2
Size: 319044 Color: 0
Size: 271093 Color: 2

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 411222 Color: 0
Size: 315304 Color: 2
Size: 273475 Color: 2

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 392569 Color: 0
Size: 316467 Color: 1
Size: 290965 Color: 4

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 412297 Color: 4
Size: 335882 Color: 0
Size: 251822 Color: 1

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 474573 Color: 4
Size: 269851 Color: 1
Size: 255577 Color: 3

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 362790 Color: 4
Size: 340550 Color: 2
Size: 296661 Color: 3

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 394520 Color: 1
Size: 303750 Color: 1
Size: 301731 Color: 2

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 406181 Color: 2
Size: 312673 Color: 4
Size: 281147 Color: 3

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 368405 Color: 3
Size: 333292 Color: 1
Size: 298304 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 414103 Color: 2
Size: 327048 Color: 1
Size: 258850 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 368475 Color: 4
Size: 321201 Color: 2
Size: 310325 Color: 4

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 392866 Color: 2
Size: 344705 Color: 4
Size: 262430 Color: 2

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 488409 Color: 3
Size: 259165 Color: 1
Size: 252427 Color: 3

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 417922 Color: 3
Size: 320780 Color: 4
Size: 261299 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 407809 Color: 3
Size: 327148 Color: 1
Size: 265044 Color: 2

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 409730 Color: 0
Size: 324594 Color: 1
Size: 265677 Color: 4

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 491372 Color: 3
Size: 257659 Color: 1
Size: 250970 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 370466 Color: 0
Size: 340329 Color: 4
Size: 289206 Color: 3

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 362315 Color: 0
Size: 334524 Color: 3
Size: 303162 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 436296 Color: 0
Size: 308692 Color: 1
Size: 255013 Color: 4

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 395009 Color: 3
Size: 317471 Color: 2
Size: 287521 Color: 1

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 404256 Color: 3
Size: 333169 Color: 0
Size: 262576 Color: 4

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 419159 Color: 1
Size: 324341 Color: 3
Size: 256501 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 456930 Color: 4
Size: 277507 Color: 1
Size: 265564 Color: 2

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 407639 Color: 3
Size: 328294 Color: 2
Size: 264068 Color: 2

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 441249 Color: 4
Size: 291986 Color: 4
Size: 266766 Color: 2

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 470973 Color: 3
Size: 275890 Color: 2
Size: 253138 Color: 1

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 361121 Color: 4
Size: 332620 Color: 2
Size: 306260 Color: 3

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 441066 Color: 2
Size: 305190 Color: 0
Size: 253745 Color: 4

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 459992 Color: 0
Size: 280519 Color: 0
Size: 259490 Color: 2

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 426817 Color: 0
Size: 305998 Color: 4
Size: 267186 Color: 1

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 402336 Color: 1
Size: 299913 Color: 0
Size: 297752 Color: 2

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 390345 Color: 0
Size: 320167 Color: 2
Size: 289489 Color: 3

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 452870 Color: 4
Size: 276904 Color: 4
Size: 270227 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 436007 Color: 2
Size: 313918 Color: 2
Size: 250076 Color: 4

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 484336 Color: 1
Size: 260281 Color: 0
Size: 255384 Color: 2

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 466811 Color: 2
Size: 280929 Color: 0
Size: 252261 Color: 2

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 486637 Color: 4
Size: 259882 Color: 1
Size: 253482 Color: 2

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 416549 Color: 1
Size: 299525 Color: 3
Size: 283927 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 429904 Color: 0
Size: 294342 Color: 2
Size: 275755 Color: 1

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 405704 Color: 4
Size: 298182 Color: 0
Size: 296115 Color: 3

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 409413 Color: 2
Size: 333754 Color: 0
Size: 256834 Color: 1

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 362920 Color: 0
Size: 330148 Color: 1
Size: 306933 Color: 4

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 465318 Color: 0
Size: 277929 Color: 3
Size: 256754 Color: 4

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 431972 Color: 4
Size: 290235 Color: 2
Size: 277794 Color: 1

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 479055 Color: 1
Size: 262213 Color: 4
Size: 258733 Color: 3

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 450270 Color: 4
Size: 280894 Color: 4
Size: 268837 Color: 1

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 481359 Color: 1
Size: 268460 Color: 3
Size: 250182 Color: 1

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 466662 Color: 0
Size: 281663 Color: 1
Size: 251676 Color: 4

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 401245 Color: 0
Size: 347004 Color: 3
Size: 251752 Color: 4

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 418230 Color: 0
Size: 324899 Color: 3
Size: 256872 Color: 2

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 478070 Color: 2
Size: 262220 Color: 3
Size: 259711 Color: 1

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 363436 Color: 4
Size: 333017 Color: 1
Size: 303548 Color: 2

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 417381 Color: 1
Size: 309166 Color: 2
Size: 273454 Color: 2

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 358500 Color: 3
Size: 325550 Color: 4
Size: 315951 Color: 4

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 495562 Color: 1
Size: 253743 Color: 3
Size: 250696 Color: 2

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 373025 Color: 3
Size: 317884 Color: 0
Size: 309092 Color: 1

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 379556 Color: 2
Size: 358150 Color: 0
Size: 262295 Color: 1

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 401657 Color: 4
Size: 344574 Color: 2
Size: 253770 Color: 4

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 384229 Color: 1
Size: 340388 Color: 2
Size: 275384 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 496654 Color: 0
Size: 252706 Color: 1
Size: 250641 Color: 2

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 396735 Color: 3
Size: 315243 Color: 2
Size: 288023 Color: 3

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 374617 Color: 3
Size: 331770 Color: 2
Size: 293614 Color: 2

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 370401 Color: 0
Size: 345619 Color: 1
Size: 283981 Color: 3

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 411074 Color: 3
Size: 323553 Color: 2
Size: 265374 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 372774 Color: 3
Size: 347100 Color: 1
Size: 280127 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 398509 Color: 3
Size: 324607 Color: 0
Size: 276885 Color: 3

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 480917 Color: 2
Size: 264260 Color: 4
Size: 254824 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 470954 Color: 1
Size: 273854 Color: 0
Size: 255193 Color: 4

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 409573 Color: 1
Size: 313976 Color: 4
Size: 276452 Color: 3

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 371521 Color: 4
Size: 318235 Color: 3
Size: 310245 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 397391 Color: 4
Size: 325911 Color: 3
Size: 276699 Color: 0

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 381459 Color: 1
Size: 323364 Color: 2
Size: 295178 Color: 3

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 410433 Color: 4
Size: 313225 Color: 2
Size: 276343 Color: 1

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 392822 Color: 1
Size: 354651 Color: 4
Size: 252528 Color: 1

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 435177 Color: 0
Size: 306295 Color: 4
Size: 258529 Color: 4

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 450537 Color: 4
Size: 276303 Color: 1
Size: 273161 Color: 2

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 352358 Color: 2
Size: 331758 Color: 3
Size: 315885 Color: 2

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 427223 Color: 4
Size: 318126 Color: 2
Size: 254652 Color: 0

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 339340 Color: 0
Size: 334048 Color: 0
Size: 326613 Color: 4

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 378065 Color: 0
Size: 358158 Color: 3
Size: 263778 Color: 2

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 480499 Color: 0
Size: 263148 Color: 4
Size: 256354 Color: 4

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 352056 Color: 0
Size: 351532 Color: 1
Size: 296413 Color: 4

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 478555 Color: 1
Size: 271158 Color: 0
Size: 250288 Color: 2

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 402355 Color: 3
Size: 342193 Color: 1
Size: 255453 Color: 4

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 364356 Color: 0
Size: 341450 Color: 3
Size: 294195 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 403518 Color: 2
Size: 310700 Color: 1
Size: 285783 Color: 4

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 415460 Color: 4
Size: 296814 Color: 0
Size: 287727 Color: 2

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 419070 Color: 4
Size: 322655 Color: 3
Size: 258276 Color: 2

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 444513 Color: 0
Size: 294260 Color: 4
Size: 261228 Color: 1

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 388472 Color: 3
Size: 339318 Color: 0
Size: 272211 Color: 1

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 393150 Color: 0
Size: 334685 Color: 1
Size: 272166 Color: 1

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 473566 Color: 2
Size: 270237 Color: 1
Size: 256198 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 384858 Color: 4
Size: 357245 Color: 1
Size: 257898 Color: 1

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 387095 Color: 3
Size: 349083 Color: 0
Size: 263823 Color: 4

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 420455 Color: 0
Size: 323920 Color: 2
Size: 255626 Color: 2

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 450555 Color: 1
Size: 293299 Color: 2
Size: 256147 Color: 2

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 378626 Color: 0
Size: 323172 Color: 3
Size: 298203 Color: 3

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 384158 Color: 4
Size: 331801 Color: 2
Size: 284042 Color: 4

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 353968 Color: 0
Size: 342187 Color: 1
Size: 303846 Color: 3

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 389548 Color: 4
Size: 323995 Color: 0
Size: 286458 Color: 4

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 378414 Color: 0
Size: 357413 Color: 1
Size: 264174 Color: 1

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 459902 Color: 4
Size: 273263 Color: 1
Size: 266836 Color: 4

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 423107 Color: 3
Size: 298929 Color: 1
Size: 277965 Color: 2

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 369339 Color: 3
Size: 332421 Color: 2
Size: 298241 Color: 1

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 430666 Color: 3
Size: 307304 Color: 0
Size: 262031 Color: 1

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 439087 Color: 4
Size: 308502 Color: 1
Size: 252412 Color: 3

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 418455 Color: 0
Size: 324324 Color: 1
Size: 257222 Color: 4

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 369721 Color: 1
Size: 336679 Color: 4
Size: 293601 Color: 2

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 387019 Color: 4
Size: 313263 Color: 1
Size: 299719 Color: 1

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 433518 Color: 0
Size: 290525 Color: 2
Size: 275958 Color: 1

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 408043 Color: 4
Size: 314058 Color: 3
Size: 277900 Color: 1

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 372925 Color: 1
Size: 326980 Color: 0
Size: 300096 Color: 3

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 366860 Color: 1
Size: 336920 Color: 2
Size: 296221 Color: 4

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 400627 Color: 1
Size: 324318 Color: 1
Size: 275056 Color: 2

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 464466 Color: 0
Size: 284112 Color: 2
Size: 251423 Color: 3

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 386656 Color: 2
Size: 331260 Color: 4
Size: 282085 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 384404 Color: 0
Size: 335915 Color: 1
Size: 279682 Color: 2

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 463946 Color: 3
Size: 270871 Color: 0
Size: 265184 Color: 2

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 378169 Color: 0
Size: 369260 Color: 3
Size: 252572 Color: 4

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 459558 Color: 2
Size: 289039 Color: 0
Size: 251404 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 439679 Color: 2
Size: 289441 Color: 4
Size: 270881 Color: 3

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 468736 Color: 3
Size: 280163 Color: 2
Size: 251102 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 397487 Color: 2
Size: 333413 Color: 0
Size: 269101 Color: 1

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 469721 Color: 3
Size: 275854 Color: 2
Size: 254426 Color: 2

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 384893 Color: 3
Size: 326524 Color: 2
Size: 288584 Color: 1

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 389920 Color: 3
Size: 337808 Color: 1
Size: 272273 Color: 2

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 388886 Color: 2
Size: 334064 Color: 3
Size: 277051 Color: 0

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 454537 Color: 1
Size: 280697 Color: 3
Size: 264767 Color: 0

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 487825 Color: 0
Size: 258842 Color: 4
Size: 253334 Color: 3

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 414430 Color: 0
Size: 319293 Color: 0
Size: 266278 Color: 1

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 454987 Color: 4
Size: 284430 Color: 0
Size: 260584 Color: 1

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 464875 Color: 3
Size: 277729 Color: 2
Size: 257397 Color: 4

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 424196 Color: 3
Size: 307349 Color: 0
Size: 268456 Color: 4

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 463945 Color: 1
Size: 276034 Color: 4
Size: 260022 Color: 0

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 457061 Color: 1
Size: 277803 Color: 3
Size: 265137 Color: 3

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 472139 Color: 0
Size: 269577 Color: 2
Size: 258285 Color: 2

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 461472 Color: 2
Size: 281496 Color: 3
Size: 257033 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 463179 Color: 0
Size: 282330 Color: 2
Size: 254492 Color: 3

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 468855 Color: 1
Size: 275537 Color: 2
Size: 255609 Color: 2

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 442260 Color: 1
Size: 289621 Color: 0
Size: 268120 Color: 3

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 357572 Color: 0
Size: 353752 Color: 0
Size: 288677 Color: 4

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 367933 Color: 2
Size: 349942 Color: 4
Size: 282126 Color: 4

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 369992 Color: 0
Size: 319932 Color: 2
Size: 310077 Color: 3

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 424064 Color: 0
Size: 322360 Color: 4
Size: 253577 Color: 4

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 453086 Color: 3
Size: 280776 Color: 2
Size: 266139 Color: 2

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 373082 Color: 3
Size: 320898 Color: 2
Size: 306021 Color: 2

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 415772 Color: 0
Size: 310211 Color: 2
Size: 274018 Color: 1

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 476609 Color: 1
Size: 272481 Color: 2
Size: 250911 Color: 3

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 401398 Color: 0
Size: 318924 Color: 1
Size: 279679 Color: 3

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 372810 Color: 4
Size: 330709 Color: 2
Size: 296482 Color: 4

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 418749 Color: 4
Size: 329948 Color: 2
Size: 251304 Color: 2

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 458047 Color: 0
Size: 290422 Color: 1
Size: 251532 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 418748 Color: 1
Size: 291789 Color: 4
Size: 289464 Color: 4

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 434905 Color: 3
Size: 290936 Color: 1
Size: 274160 Color: 0

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 384169 Color: 3
Size: 354582 Color: 0
Size: 261250 Color: 2

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 395707 Color: 4
Size: 347242 Color: 2
Size: 257052 Color: 1

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 368854 Color: 3
Size: 332274 Color: 1
Size: 298873 Color: 4

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 473176 Color: 4
Size: 272000 Color: 3
Size: 254825 Color: 1

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 494600 Color: 3
Size: 255344 Color: 2
Size: 250057 Color: 1

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 444791 Color: 1
Size: 299241 Color: 3
Size: 255969 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 354274 Color: 3
Size: 331064 Color: 0
Size: 314663 Color: 4

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 394827 Color: 4
Size: 326362 Color: 2
Size: 278812 Color: 0

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 368870 Color: 1
Size: 355356 Color: 4
Size: 275775 Color: 1

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 484550 Color: 0
Size: 264244 Color: 3
Size: 251207 Color: 4

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 401703 Color: 3
Size: 304743 Color: 4
Size: 293555 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 406987 Color: 4
Size: 331653 Color: 2
Size: 261361 Color: 1

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 481223 Color: 0
Size: 260716 Color: 4
Size: 258062 Color: 3

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 396213 Color: 1
Size: 326974 Color: 3
Size: 276814 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 450984 Color: 2
Size: 292710 Color: 4
Size: 256307 Color: 1

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 489198 Color: 1
Size: 257072 Color: 4
Size: 253731 Color: 3

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 378629 Color: 2
Size: 365540 Color: 0
Size: 255832 Color: 2

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 423689 Color: 1
Size: 321503 Color: 3
Size: 254809 Color: 3

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 440082 Color: 1
Size: 292267 Color: 4
Size: 267652 Color: 2

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 395198 Color: 3
Size: 318264 Color: 4
Size: 286539 Color: 1

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 386949 Color: 3
Size: 330972 Color: 3
Size: 282080 Color: 2

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 480692 Color: 3
Size: 265271 Color: 3
Size: 254038 Color: 4

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 364448 Color: 1
Size: 354772 Color: 4
Size: 280781 Color: 0

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 428462 Color: 1
Size: 304345 Color: 3
Size: 267194 Color: 3

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 419255 Color: 2
Size: 314367 Color: 3
Size: 266379 Color: 1

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 458137 Color: 0
Size: 273590 Color: 3
Size: 268274 Color: 1

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 462551 Color: 1
Size: 284114 Color: 2
Size: 253336 Color: 3

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 424844 Color: 0
Size: 315611 Color: 4
Size: 259546 Color: 4

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 436465 Color: 1
Size: 296129 Color: 0
Size: 267407 Color: 2

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 359641 Color: 0
Size: 343882 Color: 0
Size: 296478 Color: 1

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 450734 Color: 0
Size: 281988 Color: 2
Size: 267279 Color: 4

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 363650 Color: 2
Size: 319033 Color: 3
Size: 317318 Color: 1

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 408303 Color: 1
Size: 313809 Color: 3
Size: 277889 Color: 2

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 423276 Color: 4
Size: 302797 Color: 1
Size: 273928 Color: 3

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 375306 Color: 4
Size: 335904 Color: 2
Size: 288791 Color: 2

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 366934 Color: 0
Size: 336311 Color: 2
Size: 296756 Color: 3

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 437328 Color: 1
Size: 295936 Color: 3
Size: 266737 Color: 2

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 438588 Color: 1
Size: 301208 Color: 4
Size: 260205 Color: 2

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 397990 Color: 1
Size: 331289 Color: 2
Size: 270722 Color: 1

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 368948 Color: 1
Size: 341730 Color: 4
Size: 289323 Color: 2

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 391439 Color: 0
Size: 321798 Color: 1
Size: 286764 Color: 2

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 432626 Color: 3
Size: 303257 Color: 4
Size: 264118 Color: 1

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 474216 Color: 1
Size: 264713 Color: 3
Size: 261072 Color: 3

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 493529 Color: 4
Size: 254615 Color: 0
Size: 251857 Color: 2

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 458010 Color: 0
Size: 291946 Color: 4
Size: 250045 Color: 1

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 372851 Color: 2
Size: 336470 Color: 0
Size: 290680 Color: 1

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 459111 Color: 0
Size: 281472 Color: 2
Size: 259418 Color: 2

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 441586 Color: 4
Size: 296433 Color: 2
Size: 261982 Color: 1

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 431758 Color: 2
Size: 293272 Color: 3
Size: 274971 Color: 2

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 415941 Color: 0
Size: 313701 Color: 2
Size: 270359 Color: 4

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 461285 Color: 1
Size: 285936 Color: 1
Size: 252780 Color: 4

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 373643 Color: 4
Size: 350708 Color: 0
Size: 275650 Color: 3

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 354985 Color: 1
Size: 342692 Color: 2
Size: 302324 Color: 3

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 397737 Color: 3
Size: 306353 Color: 3
Size: 295911 Color: 0

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 366425 Color: 2
Size: 327548 Color: 1
Size: 306028 Color: 3

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 377232 Color: 2
Size: 363358 Color: 0
Size: 259411 Color: 3

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 356012 Color: 0
Size: 333893 Color: 2
Size: 310096 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 445638 Color: 2
Size: 291847 Color: 3
Size: 262516 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 364700 Color: 3
Size: 346856 Color: 2
Size: 288445 Color: 4

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 411125 Color: 3
Size: 322136 Color: 1
Size: 266740 Color: 3

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 417213 Color: 0
Size: 302521 Color: 4
Size: 280267 Color: 1

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 455979 Color: 4
Size: 279008 Color: 4
Size: 265014 Color: 1

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 367618 Color: 2
Size: 328684 Color: 4
Size: 303699 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 416819 Color: 0
Size: 297947 Color: 3
Size: 285235 Color: 4

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 460190 Color: 0
Size: 276171 Color: 1
Size: 263640 Color: 2

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 463270 Color: 2
Size: 273359 Color: 0
Size: 263372 Color: 1

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 352845 Color: 2
Size: 335481 Color: 2
Size: 311675 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 370547 Color: 4
Size: 357899 Color: 0
Size: 271555 Color: 4

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 478482 Color: 4
Size: 263644 Color: 4
Size: 257875 Color: 3

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 387724 Color: 3
Size: 346483 Color: 3
Size: 265794 Color: 4

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 392363 Color: 3
Size: 311776 Color: 4
Size: 295862 Color: 0

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 471199 Color: 4
Size: 276456 Color: 4
Size: 252346 Color: 1

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 369846 Color: 1
Size: 342374 Color: 0
Size: 287781 Color: 3

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 459646 Color: 0
Size: 284333 Color: 0
Size: 256022 Color: 4

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 483490 Color: 3
Size: 258303 Color: 0
Size: 258208 Color: 1

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 400407 Color: 2
Size: 319731 Color: 1
Size: 279863 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 388894 Color: 1
Size: 353576 Color: 4
Size: 257531 Color: 2

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 396324 Color: 2
Size: 353333 Color: 4
Size: 250344 Color: 1

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 461308 Color: 4
Size: 284897 Color: 2
Size: 253796 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 397832 Color: 3
Size: 349185 Color: 1
Size: 252984 Color: 2

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 480307 Color: 3
Size: 259886 Color: 3
Size: 259808 Color: 0

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 352299 Color: 1
Size: 347709 Color: 0
Size: 299993 Color: 2

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 488373 Color: 3
Size: 259127 Color: 0
Size: 252501 Color: 2

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 426266 Color: 2
Size: 293785 Color: 0
Size: 279950 Color: 1

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 457637 Color: 1
Size: 287575 Color: 2
Size: 254789 Color: 4

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 453237 Color: 1
Size: 291544 Color: 0
Size: 255220 Color: 4

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 426235 Color: 4
Size: 305580 Color: 0
Size: 268186 Color: 2

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 495061 Color: 4
Size: 253871 Color: 0
Size: 251069 Color: 2

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 417731 Color: 0
Size: 297980 Color: 1
Size: 284290 Color: 1

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 462770 Color: 1
Size: 276928 Color: 2
Size: 260303 Color: 4

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 437691 Color: 0
Size: 293763 Color: 3
Size: 268547 Color: 4

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 394599 Color: 0
Size: 316016 Color: 4
Size: 289386 Color: 2

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 348541 Color: 2
Size: 329887 Color: 4
Size: 321573 Color: 3

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 493547 Color: 1
Size: 254965 Color: 2
Size: 251489 Color: 4

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 376710 Color: 2
Size: 353797 Color: 1
Size: 269494 Color: 3

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 402981 Color: 3
Size: 315600 Color: 2
Size: 281420 Color: 2

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 425467 Color: 0
Size: 289658 Color: 2
Size: 284876 Color: 4

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 406017 Color: 4
Size: 322755 Color: 2
Size: 271229 Color: 3

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 395967 Color: 3
Size: 309049 Color: 2
Size: 294985 Color: 1

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 424732 Color: 0
Size: 313446 Color: 2
Size: 261823 Color: 2

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 367883 Color: 4
Size: 331776 Color: 0
Size: 300342 Color: 2

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 377896 Color: 0
Size: 329139 Color: 2
Size: 292966 Color: 1

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 461842 Color: 0
Size: 279328 Color: 2
Size: 258831 Color: 1

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 398891 Color: 1
Size: 325835 Color: 4
Size: 275275 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 453222 Color: 0
Size: 278756 Color: 3
Size: 268023 Color: 1

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 386325 Color: 4
Size: 362113 Color: 1
Size: 251563 Color: 4

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 449173 Color: 1
Size: 285407 Color: 2
Size: 265421 Color: 4

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 367508 Color: 0
Size: 354222 Color: 2
Size: 278271 Color: 1

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 434323 Color: 3
Size: 303692 Color: 0
Size: 261986 Color: 4

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 349377 Color: 4
Size: 337644 Color: 4
Size: 312980 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 405835 Color: 2
Size: 310990 Color: 2
Size: 283176 Color: 1

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 365366 Color: 1
Size: 363256 Color: 0
Size: 271379 Color: 1

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 400948 Color: 4
Size: 304715 Color: 3
Size: 294338 Color: 0

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 464140 Color: 2
Size: 277447 Color: 3
Size: 258414 Color: 2

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 415461 Color: 1
Size: 315808 Color: 3
Size: 268732 Color: 0

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 382511 Color: 2
Size: 338174 Color: 1
Size: 279316 Color: 4

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 479877 Color: 0
Size: 265499 Color: 1
Size: 254625 Color: 1

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 363018 Color: 3
Size: 342007 Color: 0
Size: 294976 Color: 2

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 416046 Color: 0
Size: 299962 Color: 1
Size: 283993 Color: 3

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 439338 Color: 4
Size: 294529 Color: 0
Size: 266134 Color: 2

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 473449 Color: 0
Size: 268834 Color: 1
Size: 257718 Color: 1

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 443060 Color: 1
Size: 282774 Color: 4
Size: 274167 Color: 0

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 434341 Color: 3
Size: 288587 Color: 2
Size: 277073 Color: 4

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 356287 Color: 4
Size: 339663 Color: 0
Size: 304051 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 461306 Color: 0
Size: 285821 Color: 3
Size: 252874 Color: 3

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 432222 Color: 2
Size: 295595 Color: 4
Size: 272184 Color: 4

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 468690 Color: 0
Size: 277030 Color: 0
Size: 254281 Color: 1

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 434603 Color: 4
Size: 309413 Color: 3
Size: 255985 Color: 3

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 439517 Color: 0
Size: 306602 Color: 1
Size: 253882 Color: 3

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 432359 Color: 1
Size: 312985 Color: 2
Size: 254657 Color: 4

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 446957 Color: 4
Size: 291937 Color: 0
Size: 261107 Color: 2

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 471795 Color: 2
Size: 268283 Color: 2
Size: 259923 Color: 4

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 410925 Color: 2
Size: 325639 Color: 3
Size: 263437 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 339771 Color: 2
Size: 332812 Color: 2
Size: 327418 Color: 4

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 421788 Color: 0
Size: 320528 Color: 2
Size: 257685 Color: 4

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 366675 Color: 0
Size: 330489 Color: 1
Size: 302837 Color: 2

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 400354 Color: 3
Size: 339658 Color: 1
Size: 259989 Color: 2

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 413464 Color: 0
Size: 318202 Color: 1
Size: 268335 Color: 0

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 462061 Color: 3
Size: 273545 Color: 4
Size: 264395 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 482770 Color: 1
Size: 263359 Color: 2
Size: 253872 Color: 0

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 426454 Color: 1
Size: 305538 Color: 3
Size: 268009 Color: 3

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 475288 Color: 2
Size: 267416 Color: 2
Size: 257297 Color: 3

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 366600 Color: 1
Size: 325514 Color: 2
Size: 307887 Color: 4

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 349795 Color: 3
Size: 328808 Color: 3
Size: 321398 Color: 1

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 406358 Color: 4
Size: 321565 Color: 2
Size: 272078 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 371798 Color: 1
Size: 346805 Color: 4
Size: 281398 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 455364 Color: 4
Size: 275978 Color: 3
Size: 268659 Color: 2

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 430405 Color: 3
Size: 295192 Color: 1
Size: 274404 Color: 0

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 462981 Color: 4
Size: 284077 Color: 2
Size: 252943 Color: 1

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 409236 Color: 3
Size: 327750 Color: 2
Size: 263015 Color: 3

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 338488 Color: 0
Size: 330788 Color: 2
Size: 330725 Color: 0

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 367288 Color: 0
Size: 364270 Color: 4
Size: 268443 Color: 3

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 478220 Color: 0
Size: 263061 Color: 2
Size: 258720 Color: 1

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 363214 Color: 1
Size: 320669 Color: 3
Size: 316118 Color: 4

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 435057 Color: 1
Size: 311498 Color: 3
Size: 253446 Color: 1

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 403727 Color: 1
Size: 308313 Color: 2
Size: 287961 Color: 2

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 370227 Color: 0
Size: 351621 Color: 3
Size: 278153 Color: 1

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 457545 Color: 2
Size: 281914 Color: 1
Size: 260542 Color: 4

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 461775 Color: 3
Size: 278953 Color: 2
Size: 259273 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 431287 Color: 1
Size: 297741 Color: 2
Size: 270973 Color: 4

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 460880 Color: 2
Size: 278686 Color: 4
Size: 260435 Color: 0

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 393756 Color: 3
Size: 344716 Color: 0
Size: 261529 Color: 4

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 391973 Color: 2
Size: 335372 Color: 4
Size: 272656 Color: 4

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 451133 Color: 3
Size: 275564 Color: 4
Size: 273304 Color: 2

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 468069 Color: 3
Size: 268542 Color: 4
Size: 263390 Color: 2

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 451689 Color: 4
Size: 279447 Color: 0
Size: 268865 Color: 3

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 391594 Color: 1
Size: 347711 Color: 1
Size: 260696 Color: 0

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 383185 Color: 0
Size: 336257 Color: 1
Size: 280559 Color: 1

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 400130 Color: 3
Size: 334482 Color: 0
Size: 265389 Color: 1

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 489786 Color: 3
Size: 259129 Color: 1
Size: 251086 Color: 4

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 497711 Color: 2
Size: 251933 Color: 0
Size: 250357 Color: 1

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 371878 Color: 1
Size: 361180 Color: 2
Size: 266943 Color: 3

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 414859 Color: 1
Size: 305983 Color: 3
Size: 279159 Color: 1

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 451184 Color: 3
Size: 296531 Color: 4
Size: 252286 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 432030 Color: 0
Size: 288240 Color: 2
Size: 279731 Color: 4

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 377807 Color: 4
Size: 318396 Color: 1
Size: 303798 Color: 0

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 488720 Color: 4
Size: 260940 Color: 0
Size: 250341 Color: 3

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 374064 Color: 1
Size: 316550 Color: 0
Size: 309387 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 374546 Color: 0
Size: 350750 Color: 2
Size: 274705 Color: 2

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 398613 Color: 0
Size: 333535 Color: 4
Size: 267853 Color: 4

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 446758 Color: 0
Size: 300036 Color: 1
Size: 253207 Color: 4

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 384105 Color: 2
Size: 322067 Color: 2
Size: 293829 Color: 1

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 486000 Color: 0
Size: 257033 Color: 2
Size: 256968 Color: 4

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 367092 Color: 1
Size: 336406 Color: 0
Size: 296503 Color: 1

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 462554 Color: 4
Size: 279107 Color: 0
Size: 258340 Color: 1

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 410003 Color: 4
Size: 339541 Color: 1
Size: 250457 Color: 1

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 452710 Color: 4
Size: 290949 Color: 4
Size: 256342 Color: 0

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 377862 Color: 1
Size: 327372 Color: 1
Size: 294767 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 405290 Color: 2
Size: 329472 Color: 0
Size: 265239 Color: 1

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 425085 Color: 3
Size: 320436 Color: 3
Size: 254480 Color: 0

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 453925 Color: 1
Size: 293475 Color: 4
Size: 252601 Color: 4

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 355878 Color: 4
Size: 328365 Color: 3
Size: 315758 Color: 3

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 397659 Color: 4
Size: 346458 Color: 2
Size: 255884 Color: 1

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 438587 Color: 3
Size: 285035 Color: 4
Size: 276379 Color: 2

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 436072 Color: 4
Size: 305141 Color: 3
Size: 258788 Color: 2

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 423476 Color: 3
Size: 323675 Color: 0
Size: 252850 Color: 2

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 411462 Color: 1
Size: 329228 Color: 4
Size: 259311 Color: 2

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 385371 Color: 0
Size: 350105 Color: 2
Size: 264525 Color: 0

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 406103 Color: 0
Size: 319724 Color: 4
Size: 274174 Color: 3

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 406719 Color: 4
Size: 300221 Color: 3
Size: 293061 Color: 4

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 415925 Color: 1
Size: 296313 Color: 3
Size: 287763 Color: 2

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 416837 Color: 4
Size: 293674 Color: 3
Size: 289490 Color: 0

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 357677 Color: 1
Size: 332405 Color: 2
Size: 309919 Color: 4

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 422534 Color: 2
Size: 290042 Color: 2
Size: 287425 Color: 3

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 457604 Color: 0
Size: 273656 Color: 2
Size: 268741 Color: 2

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 453445 Color: 2
Size: 284188 Color: 0
Size: 262368 Color: 2

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 342851 Color: 1
Size: 340410 Color: 2
Size: 316740 Color: 1

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 471373 Color: 0
Size: 276015 Color: 3
Size: 252613 Color: 4

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 434760 Color: 3
Size: 286161 Color: 0
Size: 279080 Color: 3

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 454391 Color: 1
Size: 287045 Color: 3
Size: 258565 Color: 3

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 407656 Color: 1
Size: 297499 Color: 4
Size: 294846 Color: 2

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 398046 Color: 3
Size: 321523 Color: 4
Size: 280432 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 440980 Color: 3
Size: 285363 Color: 1
Size: 273658 Color: 1

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 363032 Color: 2
Size: 359607 Color: 4
Size: 277362 Color: 2

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 460415 Color: 3
Size: 280842 Color: 4
Size: 258744 Color: 2

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 410007 Color: 3
Size: 317622 Color: 4
Size: 272372 Color: 2

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 397435 Color: 1
Size: 336615 Color: 0
Size: 265951 Color: 4

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 366219 Color: 1
Size: 357310 Color: 0
Size: 276472 Color: 2

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 487338 Color: 2
Size: 259317 Color: 4
Size: 253346 Color: 0

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 443684 Color: 2
Size: 279725 Color: 4
Size: 276592 Color: 1

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 382411 Color: 2
Size: 319221 Color: 3
Size: 298369 Color: 1

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 455797 Color: 0
Size: 288795 Color: 2
Size: 255409 Color: 2

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 362257 Color: 1
Size: 327397 Color: 2
Size: 310347 Color: 4

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 381158 Color: 2
Size: 333889 Color: 3
Size: 284954 Color: 0

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 418120 Color: 4
Size: 327529 Color: 1
Size: 254352 Color: 0

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 385889 Color: 2
Size: 311209 Color: 2
Size: 302903 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 372811 Color: 3
Size: 320866 Color: 0
Size: 306324 Color: 2

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 433771 Color: 3
Size: 300457 Color: 1
Size: 265773 Color: 2

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 486635 Color: 0
Size: 260794 Color: 1
Size: 252572 Color: 1

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 412389 Color: 3
Size: 325767 Color: 0
Size: 261845 Color: 2

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 375744 Color: 1
Size: 328535 Color: 0
Size: 295722 Color: 3

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 399426 Color: 0
Size: 323796 Color: 0
Size: 276779 Color: 4

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 469700 Color: 4
Size: 272986 Color: 4
Size: 257315 Color: 0

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 419071 Color: 2
Size: 295690 Color: 1
Size: 285240 Color: 0

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 402461 Color: 1
Size: 317400 Color: 4
Size: 280140 Color: 3

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 395194 Color: 4
Size: 302446 Color: 0
Size: 302361 Color: 3

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 464588 Color: 3
Size: 275772 Color: 2
Size: 259641 Color: 4

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 343201 Color: 4
Size: 335587 Color: 0
Size: 321213 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 419118 Color: 3
Size: 327732 Color: 0
Size: 253151 Color: 2

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 479518 Color: 2
Size: 266880 Color: 1
Size: 253603 Color: 0

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 460341 Color: 2
Size: 273335 Color: 1
Size: 266325 Color: 0

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 453590 Color: 3
Size: 279248 Color: 2
Size: 267163 Color: 0

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 470153 Color: 4
Size: 269004 Color: 3
Size: 260844 Color: 0

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 428014 Color: 4
Size: 318413 Color: 1
Size: 253574 Color: 1

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 378599 Color: 3
Size: 341981 Color: 2
Size: 279421 Color: 4

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 390010 Color: 0
Size: 319080 Color: 1
Size: 290911 Color: 4

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 392617 Color: 4
Size: 327719 Color: 3
Size: 279665 Color: 0

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 421009 Color: 1
Size: 309601 Color: 2
Size: 269391 Color: 2

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 381388 Color: 4
Size: 328082 Color: 3
Size: 290531 Color: 0

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 351601 Color: 3
Size: 335448 Color: 0
Size: 312952 Color: 1

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 413372 Color: 1
Size: 304430 Color: 3
Size: 282199 Color: 4

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 454833 Color: 0
Size: 281201 Color: 4
Size: 263967 Color: 2

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 421223 Color: 3
Size: 297496 Color: 4
Size: 281282 Color: 0

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 462981 Color: 2
Size: 280173 Color: 0
Size: 256847 Color: 2

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 408638 Color: 4
Size: 297707 Color: 0
Size: 293656 Color: 1

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 402069 Color: 4
Size: 320778 Color: 0
Size: 277154 Color: 4

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 375567 Color: 4
Size: 340566 Color: 2
Size: 283868 Color: 1

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 451060 Color: 0
Size: 276147 Color: 0
Size: 272794 Color: 1

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 399805 Color: 0
Size: 300835 Color: 4
Size: 299361 Color: 3

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 389442 Color: 2
Size: 324049 Color: 1
Size: 286510 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 406101 Color: 1
Size: 328864 Color: 0
Size: 265036 Color: 2

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 394692 Color: 4
Size: 306908 Color: 0
Size: 298401 Color: 3

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 464738 Color: 3
Size: 282165 Color: 4
Size: 253098 Color: 2

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 463858 Color: 3
Size: 269018 Color: 4
Size: 267125 Color: 4

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 376483 Color: 3
Size: 342399 Color: 0
Size: 281119 Color: 4

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 418261 Color: 0
Size: 318417 Color: 4
Size: 263323 Color: 4

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 441350 Color: 0
Size: 308605 Color: 4
Size: 250046 Color: 4

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 443461 Color: 1
Size: 294394 Color: 2
Size: 262146 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 394115 Color: 0
Size: 326454 Color: 3
Size: 279432 Color: 3

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 371085 Color: 2
Size: 321347 Color: 1
Size: 307569 Color: 3

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 457810 Color: 4
Size: 279000 Color: 0
Size: 263191 Color: 1

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 499816 Color: 3
Size: 250106 Color: 4
Size: 250079 Color: 0

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 441504 Color: 2
Size: 281118 Color: 3
Size: 277379 Color: 4

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 377823 Color: 2
Size: 353425 Color: 2
Size: 268753 Color: 3

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 446416 Color: 2
Size: 290352 Color: 0
Size: 263233 Color: 1

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 377723 Color: 1
Size: 322538 Color: 3
Size: 299740 Color: 2

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 437686 Color: 4
Size: 293466 Color: 0
Size: 268849 Color: 0

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 425926 Color: 1
Size: 306996 Color: 4
Size: 267079 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 396283 Color: 1
Size: 346911 Color: 1
Size: 256807 Color: 3

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 449463 Color: 4
Size: 294744 Color: 1
Size: 255794 Color: 2

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 458634 Color: 1
Size: 271192 Color: 0
Size: 270175 Color: 3

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 373481 Color: 3
Size: 317488 Color: 0
Size: 309032 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 378403 Color: 2
Size: 369832 Color: 4
Size: 251766 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 374572 Color: 0
Size: 370570 Color: 3
Size: 254859 Color: 4

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 448098 Color: 3
Size: 295701 Color: 1
Size: 256202 Color: 1

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 455115 Color: 1
Size: 293763 Color: 2
Size: 251123 Color: 2

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 381782 Color: 0
Size: 337180 Color: 0
Size: 281039 Color: 1

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 495488 Color: 3
Size: 253805 Color: 0
Size: 250708 Color: 4

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 459796 Color: 1
Size: 271529 Color: 2
Size: 268676 Color: 2

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 426713 Color: 2
Size: 292714 Color: 3
Size: 280574 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 479049 Color: 4
Size: 265305 Color: 0
Size: 255647 Color: 2

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 459111 Color: 1
Size: 285423 Color: 2
Size: 255467 Color: 1

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 411756 Color: 3
Size: 329160 Color: 2
Size: 259085 Color: 4

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 467809 Color: 3
Size: 273353 Color: 0
Size: 258839 Color: 4

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 379064 Color: 0
Size: 348319 Color: 1
Size: 272618 Color: 1

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 379519 Color: 3
Size: 332258 Color: 2
Size: 288224 Color: 2

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 377462 Color: 0
Size: 314582 Color: 3
Size: 307957 Color: 4

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 470943 Color: 3
Size: 267388 Color: 3
Size: 261670 Color: 2

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 433406 Color: 0
Size: 300370 Color: 4
Size: 266225 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 376555 Color: 2
Size: 335442 Color: 1
Size: 288004 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 466868 Color: 4
Size: 277450 Color: 2
Size: 255683 Color: 2

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 460096 Color: 3
Size: 274779 Color: 4
Size: 265126 Color: 2

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 388570 Color: 2
Size: 352595 Color: 0
Size: 258836 Color: 4

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 365557 Color: 3
Size: 317974 Color: 0
Size: 316470 Color: 3

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 367233 Color: 4
Size: 346726 Color: 0
Size: 286042 Color: 1

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 420042 Color: 3
Size: 323117 Color: 0
Size: 256842 Color: 4

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 479433 Color: 2
Size: 268241 Color: 0
Size: 252327 Color: 4

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 384847 Color: 0
Size: 360594 Color: 2
Size: 254560 Color: 1

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 453129 Color: 4
Size: 286146 Color: 3
Size: 260726 Color: 2

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 408351 Color: 4
Size: 337428 Color: 0
Size: 254222 Color: 1

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 391853 Color: 0
Size: 330442 Color: 1
Size: 277706 Color: 4

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 443823 Color: 3
Size: 287151 Color: 3
Size: 269027 Color: 1

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 390559 Color: 4
Size: 343041 Color: 3
Size: 266401 Color: 3

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 369102 Color: 1
Size: 339903 Color: 4
Size: 290996 Color: 0

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 379235 Color: 2
Size: 313828 Color: 0
Size: 306938 Color: 1

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 426919 Color: 4
Size: 308522 Color: 1
Size: 264560 Color: 3

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 343889 Color: 0
Size: 342077 Color: 3
Size: 314035 Color: 2

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 427919 Color: 0
Size: 304267 Color: 4
Size: 267815 Color: 4

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 393726 Color: 1
Size: 321046 Color: 4
Size: 285229 Color: 0

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 459822 Color: 2
Size: 283772 Color: 1
Size: 256407 Color: 3

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 379467 Color: 2
Size: 353376 Color: 0
Size: 267158 Color: 3

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 414692 Color: 2
Size: 300755 Color: 0
Size: 284554 Color: 1

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 461917 Color: 2
Size: 269106 Color: 3
Size: 268978 Color: 0

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 478550 Color: 1
Size: 262936 Color: 0
Size: 258515 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 393841 Color: 1
Size: 327482 Color: 2
Size: 278678 Color: 2

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 399712 Color: 3
Size: 325227 Color: 2
Size: 275062 Color: 1

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 394023 Color: 0
Size: 335827 Color: 1
Size: 270151 Color: 3

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 385986 Color: 1
Size: 317561 Color: 1
Size: 296454 Color: 2

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 393266 Color: 1
Size: 351748 Color: 0
Size: 254987 Color: 2

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 409291 Color: 4
Size: 324729 Color: 2
Size: 265981 Color: 0

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 448567 Color: 1
Size: 276336 Color: 4
Size: 275098 Color: 3

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 381383 Color: 2
Size: 361922 Color: 0
Size: 256696 Color: 3

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 454064 Color: 4
Size: 284466 Color: 0
Size: 261471 Color: 1

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 481504 Color: 1
Size: 263614 Color: 2
Size: 254883 Color: 2

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 497338 Color: 1
Size: 251762 Color: 1
Size: 250901 Color: 0

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 430685 Color: 3
Size: 312414 Color: 0
Size: 256902 Color: 2

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 466007 Color: 2
Size: 276622 Color: 0
Size: 257372 Color: 4

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 362522 Color: 0
Size: 325677 Color: 4
Size: 311802 Color: 3

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 400684 Color: 1
Size: 324126 Color: 4
Size: 275191 Color: 1

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 366852 Color: 0
Size: 321437 Color: 0
Size: 311712 Color: 3

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 407623 Color: 1
Size: 297091 Color: 3
Size: 295287 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 387811 Color: 0
Size: 309776 Color: 2
Size: 302414 Color: 2

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 376787 Color: 1
Size: 329730 Color: 4
Size: 293484 Color: 0

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 469277 Color: 0
Size: 278540 Color: 2
Size: 252184 Color: 3

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 374934 Color: 4
Size: 332583 Color: 3
Size: 292484 Color: 0

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 392418 Color: 4
Size: 352219 Color: 2
Size: 255364 Color: 1

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 469120 Color: 3
Size: 270760 Color: 1
Size: 260121 Color: 1

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 403967 Color: 1
Size: 319421 Color: 3
Size: 276613 Color: 2

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 400275 Color: 1
Size: 322360 Color: 1
Size: 277366 Color: 2

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 422801 Color: 4
Size: 308860 Color: 0
Size: 268340 Color: 2

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 381363 Color: 2
Size: 338850 Color: 0
Size: 279788 Color: 2

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 415574 Color: 1
Size: 333490 Color: 0
Size: 250937 Color: 1

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 371089 Color: 2
Size: 364014 Color: 2
Size: 264898 Color: 4

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 436662 Color: 3
Size: 294783 Color: 2
Size: 268556 Color: 3

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 393812 Color: 1
Size: 352456 Color: 1
Size: 253733 Color: 0

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 435566 Color: 1
Size: 302620 Color: 0
Size: 261815 Color: 4

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 455933 Color: 2
Size: 282944 Color: 0
Size: 261124 Color: 0

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 384268 Color: 3
Size: 324535 Color: 2
Size: 291198 Color: 1

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 389045 Color: 2
Size: 342028 Color: 2
Size: 268928 Color: 4

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 355801 Color: 2
Size: 345396 Color: 3
Size: 298804 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 366600 Color: 1
Size: 326406 Color: 0
Size: 306995 Color: 4

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 452037 Color: 3
Size: 274473 Color: 1
Size: 273491 Color: 2

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 388154 Color: 0
Size: 350758 Color: 3
Size: 261089 Color: 4

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 379458 Color: 0
Size: 314329 Color: 2
Size: 306214 Color: 4

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 343012 Color: 4
Size: 337495 Color: 2
Size: 319494 Color: 1

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 461931 Color: 2
Size: 269755 Color: 4
Size: 268315 Color: 1

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 440959 Color: 2
Size: 289481 Color: 1
Size: 269561 Color: 3

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 476424 Color: 1
Size: 266816 Color: 3
Size: 256761 Color: 1

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 480856 Color: 0
Size: 261739 Color: 1
Size: 257406 Color: 1

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 372644 Color: 2
Size: 362691 Color: 1
Size: 264666 Color: 1

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 375635 Color: 1
Size: 355891 Color: 2
Size: 268475 Color: 3

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 377305 Color: 1
Size: 320354 Color: 4
Size: 302342 Color: 3

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 364460 Color: 0
Size: 322920 Color: 1
Size: 312621 Color: 3

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 425813 Color: 0
Size: 320913 Color: 4
Size: 253275 Color: 2

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 475114 Color: 3
Size: 266171 Color: 0
Size: 258716 Color: 3

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 363491 Color: 4
Size: 329832 Color: 3
Size: 306678 Color: 0

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 352421 Color: 4
Size: 346317 Color: 1
Size: 301263 Color: 0

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 418061 Color: 4
Size: 329303 Color: 3
Size: 252637 Color: 2

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 389678 Color: 0
Size: 323012 Color: 4
Size: 287311 Color: 2

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 414333 Color: 2
Size: 297816 Color: 4
Size: 287852 Color: 0

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 413673 Color: 3
Size: 312667 Color: 0
Size: 273661 Color: 3

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 352706 Color: 3
Size: 328399 Color: 3
Size: 318896 Color: 4

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 460710 Color: 1
Size: 273778 Color: 3
Size: 265513 Color: 3

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 370555 Color: 2
Size: 327282 Color: 2
Size: 302164 Color: 3

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 405952 Color: 2
Size: 336561 Color: 0
Size: 257488 Color: 1

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 384647 Color: 0
Size: 358274 Color: 2
Size: 257080 Color: 3

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 367673 Color: 2
Size: 357347 Color: 2
Size: 274981 Color: 1

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 459153 Color: 2
Size: 288294 Color: 0
Size: 252554 Color: 4

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 408697 Color: 0
Size: 325463 Color: 3
Size: 265841 Color: 2

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 440544 Color: 0
Size: 306048 Color: 4
Size: 253409 Color: 2

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 364458 Color: 0
Size: 320122 Color: 0
Size: 315421 Color: 2

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 461698 Color: 4
Size: 278911 Color: 1
Size: 259392 Color: 4

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 368989 Color: 1
Size: 353431 Color: 2
Size: 277581 Color: 2

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 372705 Color: 4
Size: 360940 Color: 0
Size: 266356 Color: 3

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 449408 Color: 1
Size: 285679 Color: 3
Size: 264914 Color: 4

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 472564 Color: 4
Size: 265663 Color: 0
Size: 261774 Color: 3

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 399238 Color: 3
Size: 339922 Color: 2
Size: 260841 Color: 4

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 405833 Color: 4
Size: 343147 Color: 1
Size: 251021 Color: 4

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 420311 Color: 3
Size: 321102 Color: 1
Size: 258588 Color: 2

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 408714 Color: 1
Size: 313308 Color: 1
Size: 277979 Color: 3

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 396507 Color: 0
Size: 322344 Color: 2
Size: 281150 Color: 2

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 398057 Color: 4
Size: 328392 Color: 4
Size: 273552 Color: 0

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 378134 Color: 1
Size: 354292 Color: 3
Size: 267575 Color: 1

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 440783 Color: 4
Size: 282019 Color: 0
Size: 277199 Color: 3

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 391256 Color: 3
Size: 338728 Color: 4
Size: 270017 Color: 0

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 417523 Color: 4
Size: 300996 Color: 3
Size: 281482 Color: 0

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 419063 Color: 4
Size: 291353 Color: 3
Size: 289585 Color: 0

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 382470 Color: 0
Size: 343590 Color: 2
Size: 273941 Color: 1

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 392515 Color: 0
Size: 325931 Color: 1
Size: 281555 Color: 2

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 438059 Color: 3
Size: 309556 Color: 0
Size: 252386 Color: 1

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 395256 Color: 4
Size: 306416 Color: 2
Size: 298329 Color: 2

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 372275 Color: 1
Size: 322257 Color: 3
Size: 305469 Color: 3

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 383693 Color: 3
Size: 327285 Color: 1
Size: 289023 Color: 4

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 381498 Color: 4
Size: 352343 Color: 1
Size: 266160 Color: 2

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 384475 Color: 1
Size: 357200 Color: 0
Size: 258326 Color: 4

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 386414 Color: 2
Size: 311384 Color: 0
Size: 302203 Color: 4

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 405859 Color: 2
Size: 321943 Color: 1
Size: 272199 Color: 4

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 427799 Color: 3
Size: 301401 Color: 2
Size: 270801 Color: 3

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 359770 Color: 3
Size: 332843 Color: 4
Size: 307388 Color: 4

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 371473 Color: 3
Size: 352501 Color: 4
Size: 276027 Color: 2

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 364114 Color: 3
Size: 334866 Color: 3
Size: 301021 Color: 4

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 469560 Color: 3
Size: 276091 Color: 4
Size: 254350 Color: 1

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 413894 Color: 2
Size: 309057 Color: 4
Size: 277050 Color: 4

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 416676 Color: 1
Size: 294858 Color: 2
Size: 288467 Color: 4

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 383518 Color: 4
Size: 351214 Color: 1
Size: 265269 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 353174 Color: 4
Size: 342208 Color: 1
Size: 304619 Color: 1

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 418877 Color: 2
Size: 315502 Color: 3
Size: 265622 Color: 1

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 432415 Color: 3
Size: 311836 Color: 1
Size: 255750 Color: 0

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 396747 Color: 3
Size: 350117 Color: 3
Size: 253137 Color: 4

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 378236 Color: 3
Size: 338861 Color: 0
Size: 282904 Color: 1

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 440204 Color: 3
Size: 288750 Color: 0
Size: 271047 Color: 4

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 472350 Color: 0
Size: 274350 Color: 1
Size: 253301 Color: 1

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 472379 Color: 1
Size: 276078 Color: 1
Size: 251544 Color: 0

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 446854 Color: 2
Size: 290458 Color: 0
Size: 262689 Color: 1

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 382700 Color: 4
Size: 362570 Color: 0
Size: 254731 Color: 4

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 443203 Color: 1
Size: 302696 Color: 2
Size: 254102 Color: 3

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 367415 Color: 1
Size: 323266 Color: 3
Size: 309320 Color: 4

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 458382 Color: 1
Size: 280313 Color: 0
Size: 261306 Color: 2

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 385816 Color: 3
Size: 359035 Color: 0
Size: 255150 Color: 1

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 486438 Color: 3
Size: 262084 Color: 1
Size: 251479 Color: 0

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 407633 Color: 0
Size: 315081 Color: 1
Size: 277287 Color: 4

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 367574 Color: 2
Size: 329001 Color: 4
Size: 303426 Color: 1

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 416813 Color: 4
Size: 323974 Color: 0
Size: 259214 Color: 3

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 423129 Color: 3
Size: 324622 Color: 2
Size: 252250 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 415564 Color: 4
Size: 331748 Color: 1
Size: 252689 Color: 4

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 423356 Color: 1
Size: 323972 Color: 4
Size: 252673 Color: 3

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 377901 Color: 0
Size: 358663 Color: 4
Size: 263437 Color: 4

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 346628 Color: 0
Size: 345839 Color: 2
Size: 307534 Color: 2

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 430789 Color: 4
Size: 294626 Color: 4
Size: 274586 Color: 1

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 480927 Color: 0
Size: 267713 Color: 3
Size: 251361 Color: 2

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 377784 Color: 3
Size: 352331 Color: 0
Size: 269886 Color: 4

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 424566 Color: 3
Size: 307939 Color: 1
Size: 267496 Color: 3

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 370839 Color: 3
Size: 366369 Color: 4
Size: 262793 Color: 3

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 404088 Color: 1
Size: 332816 Color: 3
Size: 263097 Color: 0

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 476972 Color: 0
Size: 263721 Color: 3
Size: 259308 Color: 2

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 386681 Color: 3
Size: 360985 Color: 1
Size: 252335 Color: 2

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 403583 Color: 1
Size: 334478 Color: 3
Size: 261940 Color: 0

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 418319 Color: 0
Size: 303081 Color: 3
Size: 278601 Color: 1

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 415970 Color: 2
Size: 310311 Color: 1
Size: 273720 Color: 3

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 365556 Color: 4
Size: 324197 Color: 3
Size: 310248 Color: 4

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 372810 Color: 0
Size: 337405 Color: 1
Size: 289786 Color: 4

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 426537 Color: 2
Size: 301311 Color: 2
Size: 272153 Color: 4

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 409014 Color: 2
Size: 334472 Color: 1
Size: 256515 Color: 2

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 404881 Color: 3
Size: 335850 Color: 2
Size: 259270 Color: 0

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 421499 Color: 0
Size: 314410 Color: 3
Size: 264092 Color: 1

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 434556 Color: 1
Size: 306068 Color: 0
Size: 259377 Color: 2

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 387970 Color: 0
Size: 355422 Color: 4
Size: 256609 Color: 1

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 379617 Color: 2
Size: 326441 Color: 0
Size: 293943 Color: 1

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 397040 Color: 1
Size: 342133 Color: 3
Size: 260828 Color: 4

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 397314 Color: 1
Size: 346086 Color: 0
Size: 256601 Color: 1

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 362639 Color: 2
Size: 337818 Color: 0
Size: 299544 Color: 4

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 456011 Color: 3
Size: 285536 Color: 0
Size: 258454 Color: 0

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 475365 Color: 1
Size: 266146 Color: 3
Size: 258490 Color: 3

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 440968 Color: 3
Size: 300556 Color: 0
Size: 258477 Color: 1

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 409199 Color: 1
Size: 295882 Color: 4
Size: 294920 Color: 3

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 387527 Color: 4
Size: 318719 Color: 3
Size: 293755 Color: 0

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 409802 Color: 3
Size: 316164 Color: 2
Size: 274035 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 379891 Color: 4
Size: 360096 Color: 1
Size: 260014 Color: 4

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 439662 Color: 1
Size: 295500 Color: 2
Size: 264839 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 401990 Color: 1
Size: 316876 Color: 3
Size: 281135 Color: 3

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 457384 Color: 1
Size: 279067 Color: 0
Size: 263550 Color: 3

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 403781 Color: 4
Size: 342583 Color: 1
Size: 253637 Color: 0

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 488307 Color: 2
Size: 259780 Color: 0
Size: 251914 Color: 3

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 493087 Color: 2
Size: 255283 Color: 4
Size: 251631 Color: 0

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 431789 Color: 1
Size: 299804 Color: 3
Size: 268408 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 398512 Color: 0
Size: 329343 Color: 3
Size: 272146 Color: 4

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 390741 Color: 2
Size: 351723 Color: 1
Size: 257537 Color: 0

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 389883 Color: 3
Size: 356592 Color: 2
Size: 253526 Color: 1

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 373621 Color: 0
Size: 323695 Color: 2
Size: 302685 Color: 2

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 417178 Color: 4
Size: 322526 Color: 1
Size: 260297 Color: 3

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 451052 Color: 2
Size: 286196 Color: 0
Size: 262753 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 372842 Color: 3
Size: 328347 Color: 1
Size: 298812 Color: 3

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 367111 Color: 1
Size: 327005 Color: 2
Size: 305885 Color: 4

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 422277 Color: 1
Size: 313278 Color: 2
Size: 264446 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 442911 Color: 2
Size: 279805 Color: 0
Size: 277285 Color: 3

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 396388 Color: 1
Size: 326295 Color: 4
Size: 277318 Color: 0

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 454441 Color: 2
Size: 279636 Color: 2
Size: 265924 Color: 4

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 371155 Color: 2
Size: 339888 Color: 1
Size: 288958 Color: 3

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 375889 Color: 0
Size: 323276 Color: 4
Size: 300836 Color: 2

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 441545 Color: 2
Size: 288818 Color: 1
Size: 269638 Color: 4

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 415204 Color: 2
Size: 330504 Color: 1
Size: 254293 Color: 4

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 495484 Color: 4
Size: 253233 Color: 3
Size: 251284 Color: 2

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 461810 Color: 2
Size: 269264 Color: 1
Size: 268927 Color: 4

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 389824 Color: 3
Size: 342445 Color: 1
Size: 267732 Color: 0

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 348969 Color: 4
Size: 329637 Color: 1
Size: 321395 Color: 3

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 404289 Color: 3
Size: 309554 Color: 2
Size: 286158 Color: 0

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 398258 Color: 3
Size: 344564 Color: 0
Size: 257179 Color: 1

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 385977 Color: 3
Size: 320030 Color: 1
Size: 293994 Color: 4

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 367023 Color: 4
Size: 328068 Color: 4
Size: 304910 Color: 2

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 395628 Color: 2
Size: 311820 Color: 4
Size: 292553 Color: 3

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 441413 Color: 3
Size: 284861 Color: 4
Size: 273727 Color: 2

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 373713 Color: 1
Size: 321846 Color: 4
Size: 304442 Color: 4

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 441796 Color: 4
Size: 306536 Color: 3
Size: 251669 Color: 1

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 342638 Color: 3
Size: 337990 Color: 3
Size: 319373 Color: 1

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 479934 Color: 2
Size: 264146 Color: 2
Size: 255921 Color: 4

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 384333 Color: 3
Size: 334181 Color: 0
Size: 281487 Color: 3

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 388074 Color: 4
Size: 316512 Color: 3
Size: 295415 Color: 0

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 393555 Color: 3
Size: 311521 Color: 3
Size: 294925 Color: 4

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 393449 Color: 4
Size: 316136 Color: 2
Size: 290416 Color: 3

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 400477 Color: 1
Size: 328843 Color: 3
Size: 270681 Color: 4

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 373126 Color: 1
Size: 335198 Color: 4
Size: 291677 Color: 1

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 401856 Color: 1
Size: 346066 Color: 4
Size: 252079 Color: 4

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 359499 Color: 4
Size: 341706 Color: 0
Size: 298796 Color: 2

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 394453 Color: 3
Size: 335680 Color: 4
Size: 269868 Color: 4

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 483645 Color: 2
Size: 265392 Color: 0
Size: 250964 Color: 2

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 413623 Color: 3
Size: 300329 Color: 3
Size: 286049 Color: 1

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 490934 Color: 1
Size: 257177 Color: 1
Size: 251890 Color: 4

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 489145 Color: 3
Size: 255589 Color: 2
Size: 255267 Color: 4

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 364377 Color: 0
Size: 356908 Color: 3
Size: 278716 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 353951 Color: 0
Size: 331554 Color: 3
Size: 314496 Color: 4

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 389948 Color: 1
Size: 332843 Color: 3
Size: 277210 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 409793 Color: 4
Size: 308609 Color: 0
Size: 281599 Color: 4

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 344138 Color: 3
Size: 330147 Color: 0
Size: 325716 Color: 1

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 396799 Color: 0
Size: 309111 Color: 4
Size: 294091 Color: 2

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 489317 Color: 0
Size: 259767 Color: 1
Size: 250917 Color: 2

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 422919 Color: 1
Size: 298738 Color: 4
Size: 278344 Color: 1

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 430432 Color: 3
Size: 295138 Color: 1
Size: 274431 Color: 4

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 469658 Color: 4
Size: 274854 Color: 2
Size: 255489 Color: 3

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 405619 Color: 1
Size: 330279 Color: 2
Size: 264103 Color: 4

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 405510 Color: 0
Size: 297974 Color: 3
Size: 296517 Color: 4

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 461966 Color: 4
Size: 278047 Color: 1
Size: 259988 Color: 2

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 444195 Color: 3
Size: 282315 Color: 4
Size: 273491 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 353221 Color: 3
Size: 347956 Color: 1
Size: 298824 Color: 0

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 411522 Color: 1
Size: 301454 Color: 0
Size: 287025 Color: 2

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 427306 Color: 2
Size: 287546 Color: 0
Size: 285149 Color: 2

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 463246 Color: 3
Size: 275435 Color: 1
Size: 261320 Color: 4

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 451829 Color: 3
Size: 277561 Color: 0
Size: 270611 Color: 1

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 459715 Color: 0
Size: 286696 Color: 0
Size: 253590 Color: 3

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 487497 Color: 2
Size: 259476 Color: 0
Size: 253028 Color: 4

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 383591 Color: 4
Size: 352761 Color: 2
Size: 263649 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 476271 Color: 3
Size: 262527 Color: 4
Size: 261203 Color: 4

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 364147 Color: 0
Size: 318393 Color: 2
Size: 317461 Color: 1

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 469834 Color: 0
Size: 271124 Color: 1
Size: 259043 Color: 2

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 397028 Color: 1
Size: 348191 Color: 2
Size: 254782 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 414430 Color: 2
Size: 294046 Color: 1
Size: 291525 Color: 1

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 372564 Color: 1
Size: 319765 Color: 4
Size: 307672 Color: 0

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 387156 Color: 4
Size: 324316 Color: 0
Size: 288529 Color: 3

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 403521 Color: 0
Size: 304667 Color: 2
Size: 291813 Color: 1

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 488599 Color: 0
Size: 257691 Color: 2
Size: 253711 Color: 4

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 493730 Color: 3
Size: 254166 Color: 4
Size: 252105 Color: 1

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 465002 Color: 2
Size: 284040 Color: 3
Size: 250959 Color: 4

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 436396 Color: 0
Size: 308877 Color: 1
Size: 254728 Color: 1

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 412473 Color: 3
Size: 333247 Color: 4
Size: 254281 Color: 2

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 447131 Color: 4
Size: 280467 Color: 0
Size: 272403 Color: 2

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 455109 Color: 3
Size: 293770 Color: 1
Size: 251122 Color: 4

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 376551 Color: 4
Size: 355704 Color: 0
Size: 267746 Color: 4

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 379133 Color: 1
Size: 368395 Color: 4
Size: 252473 Color: 2

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 379835 Color: 1
Size: 327841 Color: 1
Size: 292325 Color: 0

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 410854 Color: 4
Size: 329455 Color: 0
Size: 259692 Color: 4

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 471465 Color: 3
Size: 275831 Color: 0
Size: 252705 Color: 4

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 427888 Color: 1
Size: 312236 Color: 0
Size: 259877 Color: 2

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 429873 Color: 3
Size: 295805 Color: 4
Size: 274323 Color: 4

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 375967 Color: 3
Size: 363584 Color: 3
Size: 260450 Color: 4

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 372833 Color: 1
Size: 338515 Color: 0
Size: 288653 Color: 3

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 382081 Color: 3
Size: 348142 Color: 0
Size: 269778 Color: 4

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 420341 Color: 4
Size: 291662 Color: 1
Size: 287998 Color: 2

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 475349 Color: 0
Size: 267639 Color: 4
Size: 257013 Color: 1

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 402293 Color: 0
Size: 335292 Color: 3
Size: 262416 Color: 2

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 356851 Color: 2
Size: 328194 Color: 3
Size: 314956 Color: 1

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 377212 Color: 0
Size: 354983 Color: 1
Size: 267806 Color: 4

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 497920 Color: 3
Size: 251808 Color: 4
Size: 250273 Color: 0

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 369591 Color: 0
Size: 334339 Color: 3
Size: 296071 Color: 2

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 479490 Color: 1
Size: 260335 Color: 4
Size: 260176 Color: 0

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 472898 Color: 0
Size: 274174 Color: 1
Size: 252929 Color: 3

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 408928 Color: 3
Size: 325630 Color: 4
Size: 265443 Color: 4

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 451756 Color: 4
Size: 279728 Color: 3
Size: 268517 Color: 0

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 439447 Color: 4
Size: 302435 Color: 0
Size: 258119 Color: 3

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 465279 Color: 1
Size: 275074 Color: 2
Size: 259648 Color: 4

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 416629 Color: 2
Size: 320440 Color: 4
Size: 262932 Color: 3

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 476430 Color: 0
Size: 268636 Color: 1
Size: 254935 Color: 4

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 356584 Color: 1
Size: 335812 Color: 4
Size: 307605 Color: 3

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 496503 Color: 1
Size: 252204 Color: 4
Size: 251294 Color: 3

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 484092 Color: 3
Size: 258335 Color: 2
Size: 257574 Color: 1

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 405676 Color: 2
Size: 340759 Color: 4
Size: 253566 Color: 2

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 379296 Color: 4
Size: 369023 Color: 1
Size: 251682 Color: 4

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 400386 Color: 1
Size: 320443 Color: 4
Size: 279172 Color: 0

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 424560 Color: 2
Size: 309838 Color: 3
Size: 265603 Color: 0

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 494017 Color: 0
Size: 254023 Color: 3
Size: 251961 Color: 4

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 419372 Color: 0
Size: 321750 Color: 3
Size: 258879 Color: 3

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 481710 Color: 4
Size: 264903 Color: 2
Size: 253388 Color: 0

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 473344 Color: 1
Size: 271901 Color: 3
Size: 254756 Color: 4

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 398786 Color: 3
Size: 334713 Color: 2
Size: 266502 Color: 2

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 463313 Color: 3
Size: 278549 Color: 4
Size: 258139 Color: 2

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 434621 Color: 3
Size: 293573 Color: 1
Size: 271807 Color: 1

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 426824 Color: 4
Size: 321549 Color: 2
Size: 251628 Color: 2

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 446461 Color: 0
Size: 278577 Color: 3
Size: 274963 Color: 2

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 447056 Color: 4
Size: 293093 Color: 0
Size: 259852 Color: 2

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 378992 Color: 3
Size: 336936 Color: 2
Size: 284073 Color: 0

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 400683 Color: 2
Size: 331458 Color: 0
Size: 267860 Color: 3

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 376559 Color: 3
Size: 332347 Color: 3
Size: 291095 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 376239 Color: 1
Size: 366995 Color: 0
Size: 256767 Color: 4

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 422339 Color: 2
Size: 296200 Color: 1
Size: 281462 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 468051 Color: 4
Size: 280830 Color: 3
Size: 251120 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 404521 Color: 4
Size: 341998 Color: 0
Size: 253482 Color: 3

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 387893 Color: 4
Size: 347586 Color: 3
Size: 264522 Color: 2

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 443215 Color: 4
Size: 295112 Color: 4
Size: 261674 Color: 0

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 398070 Color: 1
Size: 313929 Color: 0
Size: 288002 Color: 4

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 474952 Color: 0
Size: 271307 Color: 3
Size: 253742 Color: 4

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 440472 Color: 0
Size: 287208 Color: 2
Size: 272321 Color: 2

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 467093 Color: 2
Size: 275107 Color: 2
Size: 257801 Color: 1

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 354046 Color: 1
Size: 329235 Color: 1
Size: 316720 Color: 3

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 412131 Color: 3
Size: 326817 Color: 2
Size: 261053 Color: 0

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 377742 Color: 2
Size: 370058 Color: 1
Size: 252201 Color: 0

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 389134 Color: 4
Size: 345729 Color: 1
Size: 265138 Color: 4

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 434159 Color: 0
Size: 303487 Color: 3
Size: 262355 Color: 2

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 401255 Color: 3
Size: 325443 Color: 4
Size: 273303 Color: 2

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 475101 Color: 2
Size: 268221 Color: 0
Size: 256679 Color: 0

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 409876 Color: 1
Size: 339865 Color: 4
Size: 250260 Color: 0

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 430922 Color: 2
Size: 299715 Color: 1
Size: 269364 Color: 4

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 401893 Color: 4
Size: 329994 Color: 2
Size: 268114 Color: 2

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 443463 Color: 3
Size: 302270 Color: 0
Size: 254268 Color: 4

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 460111 Color: 1
Size: 274765 Color: 0
Size: 265125 Color: 4

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 374764 Color: 3
Size: 338334 Color: 1
Size: 286903 Color: 4

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 484634 Color: 1
Size: 261857 Color: 2
Size: 253510 Color: 0

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 489988 Color: 0
Size: 257624 Color: 2
Size: 252389 Color: 2

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 477374 Color: 4
Size: 266138 Color: 3
Size: 256489 Color: 0

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 434304 Color: 2
Size: 283782 Color: 0
Size: 281915 Color: 3

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 404258 Color: 3
Size: 319358 Color: 1
Size: 276385 Color: 3

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 388603 Color: 0
Size: 316099 Color: 3
Size: 295299 Color: 3

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 424898 Color: 2
Size: 301602 Color: 3
Size: 273501 Color: 3

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 346758 Color: 3
Size: 327398 Color: 4
Size: 325845 Color: 2

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 441960 Color: 2
Size: 289245 Color: 0
Size: 268796 Color: 2

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 340832 Color: 4
Size: 338390 Color: 3
Size: 320779 Color: 3

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 395250 Color: 3
Size: 326242 Color: 0
Size: 278509 Color: 4

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 496447 Color: 3
Size: 252089 Color: 1
Size: 251465 Color: 2

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 428142 Color: 4
Size: 291121 Color: 1
Size: 280738 Color: 0

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 420978 Color: 4
Size: 316774 Color: 1
Size: 262249 Color: 3

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 430486 Color: 4
Size: 290786 Color: 1
Size: 278729 Color: 0

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 468798 Color: 3
Size: 266728 Color: 4
Size: 264475 Color: 2

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 416227 Color: 3
Size: 323495 Color: 3
Size: 260279 Color: 0

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 455557 Color: 2
Size: 293360 Color: 3
Size: 251084 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 431351 Color: 0
Size: 303100 Color: 1
Size: 265550 Color: 4

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 439548 Color: 3
Size: 303101 Color: 0
Size: 257352 Color: 1

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 497395 Color: 1
Size: 252234 Color: 4
Size: 250372 Color: 4

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 420683 Color: 4
Size: 319138 Color: 2
Size: 260180 Color: 1

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 359190 Color: 4
Size: 322436 Color: 1
Size: 318375 Color: 3

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 405295 Color: 4
Size: 304233 Color: 1
Size: 290473 Color: 1

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 435747 Color: 4
Size: 308131 Color: 4
Size: 256123 Color: 1

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 414466 Color: 0
Size: 323431 Color: 3
Size: 262104 Color: 1

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 410795 Color: 1
Size: 315299 Color: 4
Size: 273907 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 411050 Color: 3
Size: 333432 Color: 4
Size: 255519 Color: 2

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 361510 Color: 3
Size: 356325 Color: 3
Size: 282166 Color: 4

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 362858 Color: 4
Size: 332276 Color: 3
Size: 304867 Color: 2

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 398201 Color: 0
Size: 302480 Color: 3
Size: 299320 Color: 2

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 365769 Color: 3
Size: 339976 Color: 3
Size: 294256 Color: 2

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 417562 Color: 3
Size: 297791 Color: 1
Size: 284648 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 396903 Color: 2
Size: 319829 Color: 3
Size: 283269 Color: 2

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 408335 Color: 1
Size: 331656 Color: 2
Size: 260010 Color: 4

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 410425 Color: 1
Size: 295358 Color: 2
Size: 294218 Color: 2

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 349775 Color: 3
Size: 347627 Color: 2
Size: 302599 Color: 0

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 375039 Color: 4
Size: 357491 Color: 1
Size: 267471 Color: 1

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 420876 Color: 3
Size: 316818 Color: 2
Size: 262307 Color: 3

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 387730 Color: 3
Size: 318224 Color: 0
Size: 294047 Color: 2

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 452562 Color: 0
Size: 282976 Color: 3
Size: 264463 Color: 2

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 407149 Color: 3
Size: 338481 Color: 0
Size: 254371 Color: 1

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 389444 Color: 3
Size: 347269 Color: 4
Size: 263288 Color: 4

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 463391 Color: 3
Size: 283028 Color: 2
Size: 253582 Color: 2

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 451062 Color: 4
Size: 290768 Color: 2
Size: 258171 Color: 3

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 431439 Color: 4
Size: 303178 Color: 3
Size: 265384 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 495324 Color: 3
Size: 254116 Color: 2
Size: 250561 Color: 1

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 468981 Color: 4
Size: 276099 Color: 3
Size: 254921 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 482748 Color: 1
Size: 266321 Color: 0
Size: 250932 Color: 3

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 343588 Color: 0
Size: 335845 Color: 0
Size: 320568 Color: 1

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 425635 Color: 0
Size: 306202 Color: 3
Size: 268164 Color: 4

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 464980 Color: 0
Size: 283963 Color: 3
Size: 251058 Color: 4

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 415962 Color: 2
Size: 313992 Color: 4
Size: 270047 Color: 2

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 441023 Color: 1
Size: 303626 Color: 0
Size: 255352 Color: 4

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 382620 Color: 3
Size: 340008 Color: 1
Size: 277373 Color: 2

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 472785 Color: 2
Size: 265173 Color: 0
Size: 262043 Color: 4

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 471339 Color: 1
Size: 266945 Color: 2
Size: 261717 Color: 2

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 382736 Color: 4
Size: 309012 Color: 3
Size: 308253 Color: 3

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 396053 Color: 1
Size: 336213 Color: 0
Size: 267735 Color: 3

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 389231 Color: 4
Size: 331155 Color: 3
Size: 279615 Color: 1

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 383607 Color: 3
Size: 348171 Color: 4
Size: 268223 Color: 0

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 463546 Color: 3
Size: 276739 Color: 2
Size: 259716 Color: 1

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 423262 Color: 1
Size: 308044 Color: 3
Size: 268695 Color: 2

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 382133 Color: 2
Size: 330516 Color: 3
Size: 287352 Color: 4

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 392967 Color: 0
Size: 344162 Color: 1
Size: 262872 Color: 3

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 478026 Color: 4
Size: 267672 Color: 3
Size: 254303 Color: 0

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 407209 Color: 3
Size: 297178 Color: 2
Size: 295614 Color: 1

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 477397 Color: 2
Size: 268873 Color: 3
Size: 253731 Color: 1

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 392549 Color: 4
Size: 321421 Color: 3
Size: 286031 Color: 3

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 445979 Color: 1
Size: 283398 Color: 0
Size: 270624 Color: 2

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 367203 Color: 4
Size: 325265 Color: 0
Size: 307533 Color: 1

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 446121 Color: 2
Size: 284962 Color: 1
Size: 268918 Color: 2

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 382041 Color: 4
Size: 356759 Color: 3
Size: 261201 Color: 0

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 406345 Color: 1
Size: 338443 Color: 4
Size: 255213 Color: 4

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 491387 Color: 1
Size: 256254 Color: 2
Size: 252360 Color: 2

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 369267 Color: 4
Size: 350344 Color: 0
Size: 280390 Color: 4

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 474843 Color: 2
Size: 264952 Color: 4
Size: 260206 Color: 3

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 417567 Color: 1
Size: 326701 Color: 3
Size: 255733 Color: 2

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 366023 Color: 3
Size: 345465 Color: 0
Size: 288513 Color: 1

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 397995 Color: 0
Size: 302201 Color: 4
Size: 299805 Color: 3

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 381048 Color: 0
Size: 368423 Color: 4
Size: 250530 Color: 2

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 358393 Color: 4
Size: 329922 Color: 0
Size: 311686 Color: 2

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 359715 Color: 4
Size: 332894 Color: 0
Size: 307392 Color: 2

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 403814 Color: 0
Size: 314494 Color: 4
Size: 281693 Color: 2

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 394915 Color: 4
Size: 320642 Color: 3
Size: 284444 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 488443 Color: 2
Size: 255833 Color: 4
Size: 255725 Color: 4

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 400345 Color: 0
Size: 336078 Color: 2
Size: 263578 Color: 3

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 475664 Color: 2
Size: 273346 Color: 3
Size: 250991 Color: 3

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 457828 Color: 0
Size: 288549 Color: 2
Size: 253624 Color: 4

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 347689 Color: 0
Size: 341554 Color: 0
Size: 310758 Color: 2

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 385869 Color: 4
Size: 309007 Color: 1
Size: 305125 Color: 2

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 451538 Color: 2
Size: 293702 Color: 3
Size: 254761 Color: 0

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 393228 Color: 0
Size: 330739 Color: 1
Size: 276034 Color: 3

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 464057 Color: 0
Size: 283711 Color: 3
Size: 252233 Color: 2

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 383113 Color: 4
Size: 349720 Color: 1
Size: 267168 Color: 0

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 418464 Color: 2
Size: 329695 Color: 2
Size: 251842 Color: 0

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 370683 Color: 0
Size: 367369 Color: 1
Size: 261949 Color: 2

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 382851 Color: 3
Size: 349339 Color: 2
Size: 267811 Color: 3

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 396500 Color: 0
Size: 312680 Color: 1
Size: 290821 Color: 2

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 367811 Color: 3
Size: 361927 Color: 2
Size: 270263 Color: 3

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 374588 Color: 0
Size: 321645 Color: 1
Size: 303768 Color: 1

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 474855 Color: 3
Size: 263079 Color: 1
Size: 262067 Color: 4

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 464925 Color: 1
Size: 281997 Color: 0
Size: 253079 Color: 4

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 452219 Color: 0
Size: 290717 Color: 4
Size: 257065 Color: 1

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 384453 Color: 3
Size: 363630 Color: 4
Size: 251918 Color: 1

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 423940 Color: 3
Size: 305809 Color: 1
Size: 270252 Color: 3

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 388816 Color: 3
Size: 306973 Color: 2
Size: 304212 Color: 0

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 410654 Color: 4
Size: 312338 Color: 0
Size: 277009 Color: 3

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 397312 Color: 0
Size: 341569 Color: 1
Size: 261120 Color: 1

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 459409 Color: 2
Size: 274172 Color: 0
Size: 266420 Color: 3

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 371336 Color: 3
Size: 352172 Color: 2
Size: 276493 Color: 1

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 371529 Color: 0
Size: 349549 Color: 4
Size: 278923 Color: 4

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 364497 Color: 3
Size: 336575 Color: 4
Size: 298929 Color: 2

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 371626 Color: 3
Size: 329024 Color: 1
Size: 299351 Color: 2

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 394157 Color: 2
Size: 328119 Color: 4
Size: 277725 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 397954 Color: 1
Size: 312201 Color: 3
Size: 289846 Color: 0

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 430790 Color: 0
Size: 305099 Color: 1
Size: 264112 Color: 1

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 385139 Color: 1
Size: 358670 Color: 3
Size: 256192 Color: 3

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 386240 Color: 1
Size: 357824 Color: 0
Size: 255937 Color: 4

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 414555 Color: 4
Size: 330324 Color: 3
Size: 255122 Color: 0

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 498518 Color: 4
Size: 251170 Color: 0
Size: 250313 Color: 4

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 441232 Color: 1
Size: 292580 Color: 4
Size: 266189 Color: 3

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 350181 Color: 4
Size: 344248 Color: 0
Size: 305572 Color: 1

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 399664 Color: 2
Size: 326847 Color: 0
Size: 273490 Color: 4

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 417193 Color: 2
Size: 306153 Color: 0
Size: 276655 Color: 1

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 386421 Color: 4
Size: 317863 Color: 1
Size: 295717 Color: 3

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 373959 Color: 1
Size: 339126 Color: 4
Size: 286916 Color: 3

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 407186 Color: 1
Size: 330095 Color: 4
Size: 262720 Color: 0

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 465329 Color: 0
Size: 273677 Color: 2
Size: 260995 Color: 2

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 371598 Color: 1
Size: 357432 Color: 0
Size: 270971 Color: 3

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 470248 Color: 4
Size: 269907 Color: 2
Size: 259846 Color: 2

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 487438 Color: 4
Size: 261751 Color: 1
Size: 250812 Color: 1

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 390052 Color: 0
Size: 319607 Color: 3
Size: 290342 Color: 4

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 368884 Color: 1
Size: 358809 Color: 3
Size: 272308 Color: 4

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 372988 Color: 1
Size: 321843 Color: 1
Size: 305170 Color: 0

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 490342 Color: 0
Size: 258131 Color: 2
Size: 251528 Color: 3

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 408666 Color: 0
Size: 331252 Color: 2
Size: 260083 Color: 1

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 487582 Color: 1
Size: 257093 Color: 1
Size: 255326 Color: 2

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 491851 Color: 2
Size: 254746 Color: 1
Size: 253404 Color: 2

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 396867 Color: 3
Size: 302930 Color: 0
Size: 300204 Color: 1

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 390788 Color: 0
Size: 354597 Color: 1
Size: 254616 Color: 3

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 465269 Color: 2
Size: 281593 Color: 3
Size: 253139 Color: 1

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 433740 Color: 4
Size: 293241 Color: 0
Size: 273020 Color: 2

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 386982 Color: 3
Size: 311531 Color: 3
Size: 301488 Color: 1

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 393463 Color: 3
Size: 345202 Color: 0
Size: 261336 Color: 4

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 482567 Color: 1
Size: 263191 Color: 4
Size: 254243 Color: 4

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 396020 Color: 3
Size: 315371 Color: 1
Size: 288610 Color: 0

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 389160 Color: 0
Size: 349593 Color: 1
Size: 261248 Color: 1

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 489589 Color: 4
Size: 256887 Color: 1
Size: 253525 Color: 0

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 437032 Color: 1
Size: 309219 Color: 1
Size: 253750 Color: 0

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 471936 Color: 0
Size: 275881 Color: 1
Size: 252184 Color: 2

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 447321 Color: 3
Size: 299072 Color: 2
Size: 253608 Color: 3

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 455243 Color: 4
Size: 276847 Color: 2
Size: 267911 Color: 2

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 367482 Color: 2
Size: 344315 Color: 0
Size: 288204 Color: 4

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 457049 Color: 4
Size: 288296 Color: 2
Size: 254656 Color: 1

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 387768 Color: 4
Size: 346680 Color: 1
Size: 265553 Color: 2

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 353172 Color: 0
Size: 333464 Color: 3
Size: 313365 Color: 0

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 372374 Color: 0
Size: 348932 Color: 1
Size: 278695 Color: 1

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 453404 Color: 1
Size: 276902 Color: 2
Size: 269695 Color: 3

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 403445 Color: 2
Size: 322117 Color: 0
Size: 274439 Color: 4

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 487908 Color: 3
Size: 257788 Color: 0
Size: 254305 Color: 1

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 377579 Color: 4
Size: 354994 Color: 2
Size: 267428 Color: 2

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 405697 Color: 1
Size: 338564 Color: 2
Size: 255740 Color: 1

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 403052 Color: 2
Size: 336897 Color: 1
Size: 260052 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 470413 Color: 2
Size: 279061 Color: 4
Size: 250527 Color: 4

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 366491 Color: 4
Size: 340171 Color: 0
Size: 293339 Color: 4

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 361802 Color: 4
Size: 342356 Color: 1
Size: 295843 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 401970 Color: 1
Size: 300869 Color: 3
Size: 297162 Color: 2

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 445184 Color: 2
Size: 285148 Color: 4
Size: 269669 Color: 3

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 488336 Color: 4
Size: 258624 Color: 2
Size: 253041 Color: 0

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 445701 Color: 3
Size: 287079 Color: 1
Size: 267221 Color: 4

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 433710 Color: 2
Size: 289422 Color: 1
Size: 276869 Color: 3

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 386190 Color: 2
Size: 313086 Color: 3
Size: 300725 Color: 2

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 401717 Color: 0
Size: 311984 Color: 1
Size: 286300 Color: 4

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 347676 Color: 2
Size: 337409 Color: 0
Size: 314916 Color: 4

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 421561 Color: 3
Size: 302473 Color: 1
Size: 275967 Color: 2

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 381720 Color: 1
Size: 326466 Color: 3
Size: 291815 Color: 3

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 405155 Color: 3
Size: 314404 Color: 4
Size: 280442 Color: 3

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 451342 Color: 4
Size: 274989 Color: 1
Size: 273670 Color: 2

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 435854 Color: 0
Size: 293624 Color: 3
Size: 270523 Color: 1

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 480276 Color: 0
Size: 261498 Color: 3
Size: 258227 Color: 1

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 447122 Color: 3
Size: 302349 Color: 3
Size: 250530 Color: 1

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 422705 Color: 4
Size: 294801 Color: 2
Size: 282495 Color: 3

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 413655 Color: 4
Size: 324827 Color: 4
Size: 261519 Color: 1

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 380842 Color: 0
Size: 316598 Color: 4
Size: 302561 Color: 2

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 378175 Color: 4
Size: 311674 Color: 3
Size: 310152 Color: 1

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 408689 Color: 2
Size: 338135 Color: 2
Size: 253177 Color: 0

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 426914 Color: 0
Size: 301763 Color: 1
Size: 271324 Color: 3

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 364641 Color: 4
Size: 324728 Color: 2
Size: 310632 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 449753 Color: 4
Size: 297520 Color: 2
Size: 252728 Color: 3

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 373093 Color: 0
Size: 325169 Color: 4
Size: 301739 Color: 1

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 357005 Color: 2
Size: 344848 Color: 0
Size: 298148 Color: 2

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 445975 Color: 0
Size: 279030 Color: 1
Size: 274996 Color: 3

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 422953 Color: 4
Size: 317657 Color: 2
Size: 259391 Color: 2

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 478528 Color: 4
Size: 270624 Color: 3
Size: 250849 Color: 2

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 412481 Color: 3
Size: 294885 Color: 4
Size: 292635 Color: 0

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 482939 Color: 0
Size: 265177 Color: 1
Size: 251885 Color: 3

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 437739 Color: 1
Size: 293173 Color: 2
Size: 269089 Color: 0

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 384598 Color: 0
Size: 351084 Color: 2
Size: 264319 Color: 1

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 357006 Color: 2
Size: 348345 Color: 3
Size: 294650 Color: 2

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 368437 Color: 3
Size: 336932 Color: 1
Size: 294632 Color: 3

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 446285 Color: 4
Size: 289650 Color: 2
Size: 264066 Color: 3

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 358155 Color: 2
Size: 329919 Color: 1
Size: 311927 Color: 1

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 466258 Color: 2
Size: 269136 Color: 1
Size: 264607 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 445353 Color: 1
Size: 287666 Color: 0
Size: 266982 Color: 4

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 366109 Color: 1
Size: 359518 Color: 4
Size: 274374 Color: 1

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 429845 Color: 1
Size: 309243 Color: 3
Size: 260913 Color: 0

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 435455 Color: 0
Size: 290444 Color: 3
Size: 274102 Color: 4

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 376401 Color: 0
Size: 348732 Color: 4
Size: 274868 Color: 4

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 367245 Color: 4
Size: 318513 Color: 0
Size: 314243 Color: 1

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 436129 Color: 1
Size: 290258 Color: 1
Size: 273614 Color: 4

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 388666 Color: 1
Size: 332509 Color: 0
Size: 278826 Color: 3

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 366714 Color: 0
Size: 350472 Color: 2
Size: 282815 Color: 1

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 435307 Color: 3
Size: 312499 Color: 1
Size: 252195 Color: 1

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 400544 Color: 2
Size: 320574 Color: 0
Size: 278883 Color: 1

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 430263 Color: 0
Size: 299383 Color: 3
Size: 270355 Color: 3

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 374866 Color: 0
Size: 343451 Color: 4
Size: 281684 Color: 3

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 357853 Color: 2
Size: 338209 Color: 4
Size: 303939 Color: 3

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 420272 Color: 4
Size: 327255 Color: 1
Size: 252474 Color: 3

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 376783 Color: 3
Size: 349298 Color: 0
Size: 273920 Color: 4

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 384361 Color: 1
Size: 321110 Color: 1
Size: 294530 Color: 3

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 429988 Color: 2
Size: 294612 Color: 4
Size: 275401 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 448389 Color: 2
Size: 276938 Color: 0
Size: 274674 Color: 1

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 416422 Color: 3
Size: 315104 Color: 0
Size: 268475 Color: 2

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 416809 Color: 3
Size: 316683 Color: 3
Size: 266509 Color: 2

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 405842 Color: 0
Size: 299101 Color: 2
Size: 295058 Color: 2

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 356236 Color: 1
Size: 350455 Color: 0
Size: 293310 Color: 0

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 440525 Color: 2
Size: 307332 Color: 0
Size: 252144 Color: 3

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 441072 Color: 4
Size: 301804 Color: 2
Size: 257125 Color: 3

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 489884 Color: 0
Size: 259571 Color: 3
Size: 250546 Color: 1

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 420456 Color: 4
Size: 311468 Color: 1
Size: 268077 Color: 0

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 354326 Color: 1
Size: 342055 Color: 2
Size: 303620 Color: 1

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 381989 Color: 1
Size: 329428 Color: 2
Size: 288584 Color: 0

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 401754 Color: 0
Size: 330100 Color: 2
Size: 268147 Color: 1

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 426679 Color: 2
Size: 313156 Color: 3
Size: 260166 Color: 2

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 365475 Color: 1
Size: 323635 Color: 0
Size: 310891 Color: 0

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 406218 Color: 1
Size: 325784 Color: 2
Size: 267999 Color: 3

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 356562 Color: 1
Size: 325706 Color: 1
Size: 317733 Color: 0

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 448047 Color: 4
Size: 295211 Color: 0
Size: 256743 Color: 1

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 381073 Color: 4
Size: 349725 Color: 4
Size: 269203 Color: 1

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 458504 Color: 2
Size: 291381 Color: 1
Size: 250116 Color: 0

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 453435 Color: 4
Size: 273722 Color: 0
Size: 272844 Color: 1

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 420000 Color: 0
Size: 318964 Color: 4
Size: 261037 Color: 3

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 357552 Color: 4
Size: 328372 Color: 1
Size: 314077 Color: 1

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 428959 Color: 2
Size: 304345 Color: 2
Size: 266697 Color: 3

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 381769 Color: 2
Size: 358755 Color: 4
Size: 259477 Color: 1

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 469367 Color: 4
Size: 273867 Color: 2
Size: 256767 Color: 2

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 368947 Color: 3
Size: 343016 Color: 0
Size: 288038 Color: 1

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 357000 Color: 4
Size: 335046 Color: 4
Size: 307955 Color: 2

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 367808 Color: 0
Size: 351649 Color: 1
Size: 280544 Color: 1

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 430228 Color: 4
Size: 301871 Color: 3
Size: 267902 Color: 4

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 471161 Color: 1
Size: 272559 Color: 2
Size: 256281 Color: 2

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 406899 Color: 1
Size: 301459 Color: 4
Size: 291643 Color: 0

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 368591 Color: 0
Size: 326198 Color: 1
Size: 305212 Color: 2

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 412579 Color: 0
Size: 303194 Color: 1
Size: 284228 Color: 1

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 401967 Color: 2
Size: 318165 Color: 0
Size: 279869 Color: 3

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 454117 Color: 0
Size: 294011 Color: 3
Size: 251873 Color: 0

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 421284 Color: 1
Size: 314198 Color: 4
Size: 264519 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 459650 Color: 0
Size: 275280 Color: 0
Size: 265071 Color: 1

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 389980 Color: 3
Size: 347737 Color: 2
Size: 262284 Color: 1

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 347162 Color: 4
Size: 328129 Color: 0
Size: 324710 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 393890 Color: 4
Size: 341456 Color: 1
Size: 264655 Color: 4

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 371628 Color: 2
Size: 314694 Color: 0
Size: 313679 Color: 1

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 381690 Color: 0
Size: 367805 Color: 2
Size: 250506 Color: 2

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 426412 Color: 1
Size: 305368 Color: 3
Size: 268221 Color: 3

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 391102 Color: 0
Size: 343981 Color: 4
Size: 264918 Color: 2

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 359347 Color: 1
Size: 331962 Color: 4
Size: 308692 Color: 3

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 349368 Color: 0
Size: 340581 Color: 2
Size: 310052 Color: 4

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 409925 Color: 4
Size: 329590 Color: 2
Size: 260486 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 460179 Color: 3
Size: 283644 Color: 1
Size: 256178 Color: 0

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 493801 Color: 3
Size: 255545 Color: 0
Size: 250655 Color: 4

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 426043 Color: 0
Size: 311561 Color: 4
Size: 262397 Color: 2

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 379858 Color: 0
Size: 352720 Color: 4
Size: 267423 Color: 4

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 414910 Color: 4
Size: 311588 Color: 3
Size: 273503 Color: 0

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 361694 Color: 2
Size: 337478 Color: 0
Size: 300829 Color: 2

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 385225 Color: 1
Size: 330675 Color: 2
Size: 284101 Color: 0

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 406295 Color: 2
Size: 343119 Color: 2
Size: 250587 Color: 3

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 401037 Color: 1
Size: 318728 Color: 4
Size: 280236 Color: 0

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 350843 Color: 0
Size: 349020 Color: 4
Size: 300138 Color: 4

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 419339 Color: 2
Size: 291424 Color: 3
Size: 289238 Color: 4

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 405830 Color: 4
Size: 319819 Color: 3
Size: 274352 Color: 1

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 387534 Color: 3
Size: 338119 Color: 0
Size: 274348 Color: 4

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 422143 Color: 3
Size: 316219 Color: 3
Size: 261639 Color: 2

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 398590 Color: 3
Size: 342340 Color: 3
Size: 259071 Color: 0

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 363190 Color: 0
Size: 335521 Color: 0
Size: 301290 Color: 2

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 394662 Color: 2
Size: 308161 Color: 3
Size: 297178 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 489797 Color: 3
Size: 258214 Color: 4
Size: 251990 Color: 2

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 443772 Color: 4
Size: 297970 Color: 4
Size: 258259 Color: 0

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 369093 Color: 2
Size: 319339 Color: 0
Size: 311569 Color: 4

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 495537 Color: 0
Size: 253615 Color: 1
Size: 250849 Color: 3

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 391901 Color: 1
Size: 346407 Color: 3
Size: 261693 Color: 0

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 467896 Color: 1
Size: 275029 Color: 2
Size: 257076 Color: 2

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 434051 Color: 4
Size: 306501 Color: 0
Size: 259449 Color: 3

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 366879 Color: 1
Size: 344477 Color: 0
Size: 288645 Color: 2

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 356549 Color: 4
Size: 336658 Color: 3
Size: 306794 Color: 2

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 416771 Color: 3
Size: 322637 Color: 4
Size: 260593 Color: 0

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 354769 Color: 3
Size: 344218 Color: 3
Size: 301014 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 470442 Color: 0
Size: 264914 Color: 1
Size: 264645 Color: 3

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 433800 Color: 4
Size: 289636 Color: 0
Size: 276565 Color: 3

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 426781 Color: 1
Size: 322956 Color: 4
Size: 250264 Color: 1

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 354617 Color: 3
Size: 323563 Color: 2
Size: 321821 Color: 2

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 358678 Color: 1
Size: 351432 Color: 3
Size: 289891 Color: 3

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 395981 Color: 1
Size: 354010 Color: 3
Size: 250010 Color: 1

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 362838 Color: 3
Size: 324168 Color: 0
Size: 312995 Color: 3

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 364983 Color: 0
Size: 334275 Color: 0
Size: 300743 Color: 3

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 481164 Color: 3
Size: 264571 Color: 2
Size: 254266 Color: 0

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 367842 Color: 4
Size: 361764 Color: 4
Size: 270395 Color: 3

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 361990 Color: 3
Size: 323371 Color: 3
Size: 314640 Color: 4

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 351928 Color: 2
Size: 351785 Color: 1
Size: 296288 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 362632 Color: 0
Size: 354113 Color: 2
Size: 283256 Color: 3

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 344469 Color: 3
Size: 331253 Color: 3
Size: 324279 Color: 1

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 363596 Color: 0
Size: 361041 Color: 2
Size: 275364 Color: 1

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 370406 Color: 4
Size: 363944 Color: 0
Size: 265651 Color: 0

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 361598 Color: 0
Size: 343769 Color: 0
Size: 294634 Color: 1

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 365106 Color: 0
Size: 347209 Color: 3
Size: 287686 Color: 0

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 431596 Color: 3
Size: 294186 Color: 0
Size: 274219 Color: 1

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 350634 Color: 3
Size: 346051 Color: 1
Size: 303316 Color: 3

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 370795 Color: 1
Size: 363287 Color: 4
Size: 265919 Color: 0

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 354528 Color: 1
Size: 350711 Color: 3
Size: 294762 Color: 3

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 464264 Color: 0
Size: 271680 Color: 4
Size: 264057 Color: 1

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 373173 Color: 1
Size: 354588 Color: 3
Size: 272240 Color: 4

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 354454 Color: 3
Size: 349864 Color: 3
Size: 295683 Color: 1

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 370675 Color: 3
Size: 356958 Color: 1
Size: 272368 Color: 3

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 365512 Color: 1
Size: 351046 Color: 3
Size: 283443 Color: 0

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 353818 Color: 0
Size: 353594 Color: 3
Size: 292589 Color: 3

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 348748 Color: 4
Size: 331209 Color: 0
Size: 320044 Color: 0

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 370708 Color: 3
Size: 358152 Color: 0
Size: 271141 Color: 4

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 353724 Color: 4
Size: 324908 Color: 0
Size: 321369 Color: 0

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 354397 Color: 2
Size: 326951 Color: 0
Size: 318653 Color: 3

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 356410 Color: 1
Size: 348557 Color: 2
Size: 295034 Color: 2

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 498625 Color: 0
Size: 250808 Color: 2
Size: 250568 Color: 1

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 364968 Color: 1
Size: 362010 Color: 2
Size: 273023 Color: 1

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 369060 Color: 2
Size: 362068 Color: 3
Size: 268873 Color: 4

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 357133 Color: 0
Size: 339584 Color: 0
Size: 303284 Color: 1

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 375237 Color: 0
Size: 341094 Color: 1
Size: 283670 Color: 4

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 349816 Color: 2
Size: 340784 Color: 4
Size: 309401 Color: 1

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 362600 Color: 0
Size: 357343 Color: 0
Size: 280058 Color: 1

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 355257 Color: 4
Size: 331673 Color: 0
Size: 313071 Color: 4

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 345599 Color: 3
Size: 329558 Color: 1
Size: 324844 Color: 2

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 348618 Color: 3
Size: 331053 Color: 2
Size: 320330 Color: 1

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 360020 Color: 2
Size: 344163 Color: 4
Size: 295818 Color: 2

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 360232 Color: 1
Size: 340587 Color: 3
Size: 299182 Color: 1

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 336999 Color: 4
Size: 331631 Color: 2
Size: 331371 Color: 3

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 375599 Color: 4
Size: 362543 Color: 3
Size: 261859 Color: 2

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 350830 Color: 4
Size: 347134 Color: 4
Size: 302037 Color: 2

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 362054 Color: 1
Size: 349982 Color: 2
Size: 287965 Color: 4

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 355416 Color: 1
Size: 331749 Color: 2
Size: 312836 Color: 1

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 351523 Color: 0
Size: 328513 Color: 2
Size: 319965 Color: 1

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 349260 Color: 4
Size: 325775 Color: 1
Size: 324966 Color: 1

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 381796 Color: 4
Size: 355704 Color: 0
Size: 262501 Color: 2

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 349603 Color: 1
Size: 330672 Color: 4
Size: 319726 Color: 2

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 358606 Color: 3
Size: 343841 Color: 4
Size: 297554 Color: 3

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 350678 Color: 3
Size: 347046 Color: 2
Size: 302277 Color: 2

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 337478 Color: 3
Size: 333021 Color: 2
Size: 329502 Color: 4

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 341550 Color: 4
Size: 335218 Color: 4
Size: 323233 Color: 0

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 350608 Color: 3
Size: 339051 Color: 4
Size: 310342 Color: 1

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 355574 Color: 1
Size: 326952 Color: 0
Size: 317475 Color: 3

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 355501 Color: 4
Size: 334806 Color: 3
Size: 309694 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 338709 Color: 0
Size: 337624 Color: 1
Size: 323668 Color: 4

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 358476 Color: 0
Size: 357875 Color: 3
Size: 283650 Color: 3

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 358663 Color: 1
Size: 325153 Color: 0
Size: 316185 Color: 0

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 348693 Color: 3
Size: 334486 Color: 3
Size: 316822 Color: 2

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 360551 Color: 0
Size: 335938 Color: 1
Size: 303512 Color: 3

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 353467 Color: 2
Size: 330156 Color: 1
Size: 316378 Color: 1

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 341443 Color: 0
Size: 331064 Color: 4
Size: 327494 Color: 4

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 357498 Color: 4
Size: 340748 Color: 1
Size: 301755 Color: 3

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 444536 Color: 0
Size: 279018 Color: 4
Size: 276447 Color: 1

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 365289 Color: 0
Size: 328287 Color: 2
Size: 306425 Color: 2

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 336696 Color: 0
Size: 334348 Color: 2
Size: 328957 Color: 2

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 357526 Color: 0
Size: 349464 Color: 0
Size: 293011 Color: 4

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 365941 Color: 0
Size: 320886 Color: 1
Size: 313174 Color: 1

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 358180 Color: 0
Size: 335200 Color: 2
Size: 306621 Color: 4

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 355244 Color: 4
Size: 340603 Color: 2
Size: 304154 Color: 2

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 352781 Color: 1
Size: 343542 Color: 4
Size: 303678 Color: 0

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 351430 Color: 0
Size: 332036 Color: 0
Size: 316535 Color: 1

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 355954 Color: 0
Size: 349264 Color: 2
Size: 294783 Color: 3

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 353733 Color: 3
Size: 342876 Color: 0
Size: 303392 Color: 0

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 354931 Color: 1
Size: 340493 Color: 2
Size: 304577 Color: 1

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 338861 Color: 0
Size: 334639 Color: 1
Size: 326501 Color: 2

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 351511 Color: 4
Size: 327876 Color: 3
Size: 320614 Color: 3

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 367419 Color: 3
Size: 356717 Color: 4
Size: 275865 Color: 3

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 365031 Color: 3
Size: 357215 Color: 3
Size: 277755 Color: 4

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 347407 Color: 4
Size: 333302 Color: 4
Size: 319292 Color: 2

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 355677 Color: 0
Size: 324710 Color: 2
Size: 319614 Color: 3

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 345056 Color: 1
Size: 342936 Color: 4
Size: 312009 Color: 1

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 354885 Color: 2
Size: 338401 Color: 3
Size: 306715 Color: 4

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 360015 Color: 2
Size: 330765 Color: 1
Size: 309221 Color: 3

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 363033 Color: 2
Size: 336729 Color: 1
Size: 300239 Color: 2

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 367094 Color: 1
Size: 358029 Color: 2
Size: 274878 Color: 1

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 362049 Color: 4
Size: 359771 Color: 4
Size: 278181 Color: 0

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 348104 Color: 1
Size: 328630 Color: 4
Size: 323267 Color: 4

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 364654 Color: 1
Size: 358417 Color: 4
Size: 276930 Color: 3

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 433338 Color: 3
Size: 293723 Color: 1
Size: 272940 Color: 0

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 364808 Color: 2
Size: 331168 Color: 1
Size: 304025 Color: 3

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 350635 Color: 2
Size: 345248 Color: 0
Size: 304118 Color: 4

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 354416 Color: 4
Size: 341228 Color: 3
Size: 304357 Color: 4

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 357517 Color: 0
Size: 324403 Color: 4
Size: 318081 Color: 3

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 363757 Color: 1
Size: 361384 Color: 0
Size: 274860 Color: 1

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 372458 Color: 0
Size: 353655 Color: 1
Size: 273888 Color: 3

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 461483 Color: 4
Size: 269815 Color: 0
Size: 268703 Color: 3

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 419862 Color: 2
Size: 311056 Color: 3
Size: 269083 Color: 3

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 410070 Color: 0
Size: 336218 Color: 2
Size: 253713 Color: 1

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 385014 Color: 4
Size: 312877 Color: 3
Size: 302110 Color: 0

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 481895 Color: 0
Size: 267615 Color: 3
Size: 250491 Color: 3

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 351412 Color: 1
Size: 347891 Color: 3
Size: 300698 Color: 2

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 408443 Color: 0
Size: 337245 Color: 2
Size: 254313 Color: 2

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 352872 Color: 0
Size: 344880 Color: 4
Size: 302249 Color: 4

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 411768 Color: 3
Size: 333453 Color: 0
Size: 254780 Color: 3

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 359138 Color: 1
Size: 357159 Color: 4
Size: 283704 Color: 4

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 371776 Color: 3
Size: 314792 Color: 4
Size: 313433 Color: 3

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 357521 Color: 2
Size: 334567 Color: 3
Size: 307913 Color: 1

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 384418 Color: 0
Size: 359540 Color: 0
Size: 256043 Color: 3

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 408925 Color: 4
Size: 327303 Color: 3
Size: 263773 Color: 4

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 350853 Color: 1
Size: 338171 Color: 0
Size: 310977 Color: 3

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 364360 Color: 0
Size: 331224 Color: 2
Size: 304417 Color: 4

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 347754 Color: 0
Size: 343264 Color: 4
Size: 308983 Color: 2

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 389481 Color: 1
Size: 350685 Color: 0
Size: 259835 Color: 1

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 345706 Color: 2
Size: 341856 Color: 3
Size: 312439 Color: 0

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 357285 Color: 0
Size: 347686 Color: 1
Size: 295030 Color: 2

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 345011 Color: 2
Size: 342341 Color: 0
Size: 312649 Color: 4

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 349271 Color: 3
Size: 343169 Color: 4
Size: 307561 Color: 1

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 346073 Color: 0
Size: 335603 Color: 0
Size: 318325 Color: 1

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 356877 Color: 1
Size: 345666 Color: 4
Size: 297458 Color: 4

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 362358 Color: 2
Size: 341768 Color: 1
Size: 295875 Color: 1

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 392120 Color: 0
Size: 353748 Color: 3
Size: 254133 Color: 3

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 360464 Color: 0
Size: 344618 Color: 4
Size: 294919 Color: 2

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 351215 Color: 4
Size: 338006 Color: 4
Size: 310780 Color: 1

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 420586 Color: 0
Size: 328787 Color: 4
Size: 250628 Color: 1

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 350834 Color: 1
Size: 343848 Color: 1
Size: 305319 Color: 2

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 356952 Color: 2
Size: 344859 Color: 3
Size: 298190 Color: 4

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 358016 Color: 3
Size: 342843 Color: 0
Size: 299142 Color: 2

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 347269 Color: 0
Size: 341332 Color: 0
Size: 311400 Color: 3

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 350923 Color: 3
Size: 341940 Color: 2
Size: 307138 Color: 3

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 345497 Color: 4
Size: 341600 Color: 2
Size: 312904 Color: 1

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 366838 Color: 2
Size: 340998 Color: 4
Size: 292165 Color: 4

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 359576 Color: 2
Size: 339235 Color: 4
Size: 301190 Color: 1

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 352870 Color: 4
Size: 339843 Color: 4
Size: 307288 Color: 3

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 350034 Color: 4
Size: 339164 Color: 2
Size: 310803 Color: 1

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 350589 Color: 3
Size: 338815 Color: 1
Size: 310597 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 360813 Color: 0
Size: 338220 Color: 1
Size: 300968 Color: 4

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 448790 Color: 0
Size: 291026 Color: 3
Size: 260185 Color: 1

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 344436 Color: 3
Size: 336438 Color: 4
Size: 319127 Color: 1

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 352603 Color: 0
Size: 326887 Color: 1
Size: 320511 Color: 3

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 344434 Color: 2
Size: 336823 Color: 0
Size: 318744 Color: 2

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 352495 Color: 4
Size: 335308 Color: 3
Size: 312198 Color: 2

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 347390 Color: 2
Size: 333290 Color: 0
Size: 319321 Color: 2

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 356806 Color: 4
Size: 335392 Color: 2
Size: 307803 Color: 3

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 357521 Color: 2
Size: 346778 Color: 1
Size: 295702 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 358943 Color: 2
Size: 326951 Color: 0
Size: 314107 Color: 4

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 359543 Color: 4
Size: 342968 Color: 0
Size: 297490 Color: 1

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 359668 Color: 3
Size: 345301 Color: 0
Size: 295032 Color: 4

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 360246 Color: 0
Size: 338015 Color: 2
Size: 301740 Color: 3

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 360033 Color: 3
Size: 359388 Color: 3
Size: 280580 Color: 1

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 360563 Color: 1
Size: 336944 Color: 2
Size: 302494 Color: 4

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 360594 Color: 1
Size: 332535 Color: 0
Size: 306872 Color: 3

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 360540 Color: 3
Size: 354273 Color: 4
Size: 285188 Color: 3

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 360523 Color: 3
Size: 352794 Color: 2
Size: 286684 Color: 1

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 360564 Color: 3
Size: 342774 Color: 4
Size: 296663 Color: 1

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 361064 Color: 0
Size: 332178 Color: 0
Size: 306759 Color: 1

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 361284 Color: 4
Size: 338064 Color: 0
Size: 300653 Color: 1

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 361715 Color: 2
Size: 360102 Color: 3
Size: 278184 Color: 1

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 362158 Color: 1
Size: 349147 Color: 2
Size: 288696 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 362324 Color: 1
Size: 351687 Color: 3
Size: 285990 Color: 2

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 362648 Color: 0
Size: 353070 Color: 3
Size: 284283 Color: 4

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 363403 Color: 3
Size: 342736 Color: 1
Size: 293862 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 363164 Color: 4
Size: 330982 Color: 0
Size: 305855 Color: 0

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 363868 Color: 0
Size: 333776 Color: 1
Size: 302357 Color: 3

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 363923 Color: 0
Size: 356003 Color: 4
Size: 280075 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 363585 Color: 3
Size: 337422 Color: 2
Size: 298994 Color: 0

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 364309 Color: 1
Size: 341238 Color: 4
Size: 294454 Color: 1

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 364655 Color: 0
Size: 351325 Color: 1
Size: 284021 Color: 4

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 364295 Color: 3
Size: 337572 Color: 3
Size: 298134 Color: 2

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 364657 Color: 4
Size: 336848 Color: 4
Size: 298496 Color: 2

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 365205 Color: 4
Size: 354245 Color: 1
Size: 280551 Color: 2

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 364926 Color: 3
Size: 330821 Color: 1
Size: 304254 Color: 4

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 365206 Color: 0
Size: 327253 Color: 2
Size: 307542 Color: 0

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 365273 Color: 1
Size: 340718 Color: 4
Size: 294010 Color: 2

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 364855 Color: 2
Size: 346512 Color: 1
Size: 288634 Color: 1

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 365451 Color: 3
Size: 360382 Color: 4
Size: 274168 Color: 2

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 365549 Color: 3
Size: 325738 Color: 2
Size: 308714 Color: 0

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 365768 Color: 0
Size: 364238 Color: 2
Size: 269995 Color: 3

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 365749 Color: 4
Size: 364298 Color: 1
Size: 269954 Color: 0

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 365821 Color: 4
Size: 348503 Color: 4
Size: 285677 Color: 0

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 365968 Color: 1
Size: 328474 Color: 1
Size: 305559 Color: 2

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 365955 Color: 0
Size: 339181 Color: 1
Size: 294865 Color: 2

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 366037 Color: 3
Size: 342346 Color: 4
Size: 291618 Color: 0

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 366382 Color: 0
Size: 356323 Color: 3
Size: 277296 Color: 4

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 366364 Color: 4
Size: 348785 Color: 2
Size: 284852 Color: 3

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 366463 Color: 4
Size: 323405 Color: 2
Size: 310133 Color: 3

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 366104 Color: 3
Size: 365665 Color: 3
Size: 268232 Color: 2

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 366514 Color: 0
Size: 317829 Color: 2
Size: 315658 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 366673 Color: 0
Size: 337399 Color: 1
Size: 295929 Color: 3

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 366631 Color: 1
Size: 343287 Color: 1
Size: 290083 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 366203 Color: 2
Size: 323473 Color: 3
Size: 310325 Color: 2

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 366761 Color: 4
Size: 336464 Color: 0
Size: 296776 Color: 4

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 366835 Color: 3
Size: 326979 Color: 2
Size: 306187 Color: 3

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 366970 Color: 3
Size: 349624 Color: 3
Size: 283407 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 366588 Color: 2
Size: 331387 Color: 3
Size: 302026 Color: 3

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 367076 Color: 3
Size: 320963 Color: 4
Size: 311962 Color: 4

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 367265 Color: 0
Size: 350370 Color: 1
Size: 282366 Color: 4

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 367264 Color: 3
Size: 318626 Color: 1
Size: 314111 Color: 4

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 367375 Color: 3
Size: 355005 Color: 0
Size: 277621 Color: 4

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 367507 Color: 2
Size: 349623 Color: 2
Size: 282871 Color: 4

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 367763 Color: 3
Size: 361688 Color: 0
Size: 270550 Color: 4

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 367798 Color: 4
Size: 335970 Color: 0
Size: 296233 Color: 1

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 367878 Color: 1
Size: 322690 Color: 1
Size: 309433 Color: 4

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 368024 Color: 0
Size: 350439 Color: 1
Size: 281538 Color: 4

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 367959 Color: 2
Size: 366427 Color: 3
Size: 265615 Color: 3

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 367971 Color: 2
Size: 318000 Color: 0
Size: 314030 Color: 3

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 368051 Color: 2
Size: 323158 Color: 4
Size: 308792 Color: 4

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 368196 Color: 2
Size: 355876 Color: 3
Size: 275929 Color: 0

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 368274 Color: 3
Size: 357714 Color: 2
Size: 274013 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 368447 Color: 0
Size: 359246 Color: 2
Size: 272308 Color: 1

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 368715 Color: 0
Size: 342720 Color: 2
Size: 288566 Color: 3

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 369246 Color: 4
Size: 318083 Color: 3
Size: 312672 Color: 3

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 369284 Color: 2
Size: 339536 Color: 3
Size: 291181 Color: 2

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 369316 Color: 4
Size: 347645 Color: 3
Size: 283040 Color: 0

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 369196 Color: 0
Size: 339848 Color: 4
Size: 290957 Color: 4

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 369434 Color: 3
Size: 360310 Color: 2
Size: 270257 Color: 4

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 369359 Color: 1
Size: 333907 Color: 2
Size: 296735 Color: 1

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 369477 Color: 4
Size: 316530 Color: 3
Size: 313994 Color: 2

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 369808 Color: 2
Size: 318661 Color: 0
Size: 311532 Color: 4

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 369918 Color: 2
Size: 362056 Color: 0
Size: 268027 Color: 3

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 369934 Color: 2
Size: 349303 Color: 3
Size: 280764 Color: 4

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 369941 Color: 2
Size: 365015 Color: 0
Size: 265045 Color: 4

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 370183 Color: 0
Size: 323815 Color: 2
Size: 306003 Color: 1

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 364945 Color: 0
Size: 354607 Color: 0
Size: 280449 Color: 2

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 343852 Color: 0
Size: 334023 Color: 2
Size: 322126 Color: 2

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 370573 Color: 0
Size: 346064 Color: 3
Size: 283364 Color: 2

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 370573 Color: 0
Size: 336642 Color: 2
Size: 292786 Color: 1

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 370520 Color: 2
Size: 350521 Color: 1
Size: 278960 Color: 1

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 370632 Color: 0
Size: 363030 Color: 1
Size: 266339 Color: 2

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 370684 Color: 1
Size: 336747 Color: 3
Size: 292570 Color: 2

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 370684 Color: 1
Size: 352724 Color: 0
Size: 276593 Color: 3

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 364557 Color: 1
Size: 333877 Color: 0
Size: 301567 Color: 0

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 370789 Color: 3
Size: 315019 Color: 1
Size: 314193 Color: 4

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 370899 Color: 4
Size: 351820 Color: 3
Size: 277282 Color: 2

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 370949 Color: 4
Size: 340605 Color: 4
Size: 288447 Color: 0

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 370949 Color: 4
Size: 317167 Color: 0
Size: 311885 Color: 4

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 370968 Color: 1
Size: 348692 Color: 3
Size: 280341 Color: 0

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 371076 Color: 4
Size: 342883 Color: 3
Size: 286042 Color: 0

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 371009 Color: 0
Size: 315790 Color: 2
Size: 313202 Color: 1

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 371130 Color: 1
Size: 337169 Color: 3
Size: 291702 Color: 2

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 371132 Color: 3
Size: 331703 Color: 4
Size: 297166 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 371365 Color: 0
Size: 323611 Color: 4
Size: 305025 Color: 2

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 371220 Color: 3
Size: 357598 Color: 1
Size: 271183 Color: 1

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 371461 Color: 2
Size: 354670 Color: 0
Size: 273870 Color: 4

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 371603 Color: 2
Size: 315876 Color: 4
Size: 312522 Color: 0

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 371619 Color: 1
Size: 363709 Color: 4
Size: 264673 Color: 3

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 371693 Color: 0
Size: 314977 Color: 1
Size: 313331 Color: 4

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 371642 Color: 4
Size: 328701 Color: 2
Size: 299658 Color: 3

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 371778 Color: 0
Size: 331861 Color: 1
Size: 296362 Color: 4

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 371849 Color: 2
Size: 334610 Color: 1
Size: 293542 Color: 1

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 371854 Color: 2
Size: 347296 Color: 0
Size: 280851 Color: 2

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 371893 Color: 2
Size: 367205 Color: 4
Size: 260903 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 372045 Color: 4
Size: 367944 Color: 0
Size: 260012 Color: 1

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 372071 Color: 2
Size: 340363 Color: 1
Size: 287567 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 372455 Color: 0
Size: 355261 Color: 3
Size: 272285 Color: 3

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 372294 Color: 4
Size: 333832 Color: 1
Size: 293875 Color: 1

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 372339 Color: 4
Size: 321244 Color: 4
Size: 306418 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 372445 Color: 2
Size: 329717 Color: 2
Size: 297839 Color: 3

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 372481 Color: 4
Size: 338643 Color: 2
Size: 288877 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 372486 Color: 3
Size: 347066 Color: 2
Size: 280449 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 372506 Color: 2
Size: 361074 Color: 4
Size: 266421 Color: 0

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 372623 Color: 0
Size: 331849 Color: 1
Size: 295529 Color: 2

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 372534 Color: 4
Size: 344910 Color: 1
Size: 282557 Color: 4

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 372564 Color: 4
Size: 334864 Color: 2
Size: 292573 Color: 2

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 372621 Color: 1
Size: 363421 Color: 2
Size: 263959 Color: 0

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 372631 Color: 2
Size: 320712 Color: 4
Size: 306658 Color: 4

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 372787 Color: 0
Size: 368045 Color: 3
Size: 259169 Color: 2

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 372856 Color: 4
Size: 330553 Color: 4
Size: 296592 Color: 3

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 372909 Color: 1
Size: 324516 Color: 2
Size: 302576 Color: 1

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 373014 Color: 2
Size: 339942 Color: 0
Size: 287045 Color: 4

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 373027 Color: 3
Size: 347643 Color: 2
Size: 279331 Color: 1

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 373187 Color: 1
Size: 324632 Color: 0
Size: 302182 Color: 1

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 373196 Color: 1
Size: 348703 Color: 2
Size: 278102 Color: 2

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 373224 Color: 3
Size: 336128 Color: 1
Size: 290649 Color: 0

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 373224 Color: 4
Size: 315063 Color: 0
Size: 311714 Color: 4

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 373430 Color: 1
Size: 361859 Color: 2
Size: 264712 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 373570 Color: 4
Size: 340915 Color: 3
Size: 285516 Color: 1

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 373681 Color: 0
Size: 347612 Color: 1
Size: 278708 Color: 3

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 373726 Color: 1
Size: 332952 Color: 0
Size: 293323 Color: 2

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 373761 Color: 0
Size: 370021 Color: 2
Size: 256219 Color: 1

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 373826 Color: 1
Size: 369617 Color: 1
Size: 256558 Color: 3

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 373859 Color: 4
Size: 356059 Color: 0
Size: 270083 Color: 3

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 373864 Color: 2
Size: 324653 Color: 3
Size: 301484 Color: 0

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 374032 Color: 4
Size: 326430 Color: 2
Size: 299539 Color: 0

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 374036 Color: 4
Size: 370829 Color: 1
Size: 255136 Color: 2

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 374107 Color: 3
Size: 363781 Color: 2
Size: 262113 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 374140 Color: 2
Size: 327171 Color: 4
Size: 298690 Color: 2

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 374212 Color: 4
Size: 317266 Color: 3
Size: 308523 Color: 0

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 374080 Color: 0
Size: 339324 Color: 1
Size: 286597 Color: 2

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 374261 Color: 3
Size: 314901 Color: 1
Size: 310839 Color: 1

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 374459 Color: 0
Size: 361575 Color: 2
Size: 263967 Color: 1

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 374433 Color: 1
Size: 342955 Color: 4
Size: 282613 Color: 3

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 374523 Color: 4
Size: 330303 Color: 0
Size: 295175 Color: 2

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 374720 Color: 4
Size: 332604 Color: 3
Size: 292677 Color: 2

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 374856 Color: 3
Size: 343434 Color: 4
Size: 281711 Color: 0

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 374938 Color: 4
Size: 342749 Color: 2
Size: 282314 Color: 3

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 375015 Color: 2
Size: 340206 Color: 0
Size: 284780 Color: 3

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 374876 Color: 0
Size: 369922 Color: 2
Size: 255203 Color: 2

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 375114 Color: 0
Size: 360905 Color: 3
Size: 263982 Color: 0

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 375068 Color: 3
Size: 329728 Color: 3
Size: 295205 Color: 0

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 375202 Color: 0
Size: 361265 Color: 4
Size: 263534 Color: 1

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 375167 Color: 4
Size: 374807 Color: 4
Size: 250027 Color: 1

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 375223 Color: 2
Size: 342803 Color: 1
Size: 281975 Color: 1

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 375351 Color: 2
Size: 366274 Color: 1
Size: 258376 Color: 4

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 375613 Color: 4
Size: 374069 Color: 4
Size: 250319 Color: 0

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 375636 Color: 4
Size: 343593 Color: 1
Size: 280772 Color: 0

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 375639 Color: 1
Size: 352499 Color: 0
Size: 271863 Color: 3

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 375650 Color: 3
Size: 331190 Color: 4
Size: 293161 Color: 4

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 375818 Color: 0
Size: 316926 Color: 1
Size: 307257 Color: 3

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 375704 Color: 2
Size: 325614 Color: 4
Size: 298683 Color: 3

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 375835 Color: 0
Size: 334768 Color: 1
Size: 289398 Color: 1

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 375801 Color: 3
Size: 352655 Color: 1
Size: 271545 Color: 2

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 375821 Color: 1
Size: 352225 Color: 3
Size: 271955 Color: 0

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 375872 Color: 3
Size: 333645 Color: 2
Size: 290484 Color: 4

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 375925 Color: 4
Size: 330068 Color: 2
Size: 294008 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 375955 Color: 2
Size: 335319 Color: 1
Size: 288727 Color: 3

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 375999 Color: 3
Size: 373875 Color: 2
Size: 250127 Color: 4

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 376106 Color: 0
Size: 315055 Color: 3
Size: 308840 Color: 2

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 376236 Color: 0
Size: 360059 Color: 1
Size: 263706 Color: 1

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 376301 Color: 3
Size: 347020 Color: 2
Size: 276680 Color: 2

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 376431 Color: 4
Size: 354715 Color: 3
Size: 268855 Color: 3

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 376403 Color: 0
Size: 373303 Color: 3
Size: 250295 Color: 4

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 376514 Color: 4
Size: 320496 Color: 2
Size: 302991 Color: 3

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 376532 Color: 1
Size: 365932 Color: 1
Size: 257537 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 376630 Color: 3
Size: 358461 Color: 4
Size: 264910 Color: 2

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 376672 Color: 2
Size: 342325 Color: 0
Size: 281004 Color: 2

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 376689 Color: 1
Size: 349401 Color: 2
Size: 273911 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 376693 Color: 2
Size: 356956 Color: 1
Size: 266352 Color: 3

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 376722 Color: 4
Size: 345804 Color: 2
Size: 277475 Color: 3

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 376732 Color: 1
Size: 361661 Color: 4
Size: 261608 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 376737 Color: 2
Size: 316676 Color: 1
Size: 306588 Color: 0

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 376762 Color: 4
Size: 349917 Color: 1
Size: 273322 Color: 2

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 376821 Color: 3
Size: 367542 Color: 2
Size: 255638 Color: 1

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 376833 Color: 4
Size: 360756 Color: 2
Size: 262412 Color: 0

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 376854 Color: 4
Size: 355790 Color: 1
Size: 267357 Color: 1

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 377037 Color: 4
Size: 327861 Color: 1
Size: 295103 Color: 1

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 377055 Color: 3
Size: 326396 Color: 4
Size: 296550 Color: 3

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 377116 Color: 4
Size: 366122 Color: 3
Size: 256763 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 377159 Color: 3
Size: 364097 Color: 3
Size: 258745 Color: 2

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 377218 Color: 1
Size: 317561 Color: 0
Size: 305222 Color: 2

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 377303 Color: 2
Size: 360681 Color: 2
Size: 262017 Color: 0

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 377462 Color: 0
Size: 337795 Color: 2
Size: 284744 Color: 3

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 377352 Color: 2
Size: 338254 Color: 1
Size: 284395 Color: 2

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 377506 Color: 0
Size: 332977 Color: 2
Size: 289518 Color: 3

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 377559 Color: 4
Size: 354304 Color: 0
Size: 268138 Color: 3

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 377573 Color: 2
Size: 335639 Color: 4
Size: 286789 Color: 4

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 377603 Color: 4
Size: 315970 Color: 1
Size: 306428 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 377657 Color: 3
Size: 369751 Color: 0
Size: 252593 Color: 3

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 377881 Color: 2
Size: 313569 Color: 3
Size: 308551 Color: 2

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 377860 Color: 3
Size: 334409 Color: 4
Size: 287732 Color: 2

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 377881 Color: 0
Size: 311433 Color: 1
Size: 310687 Color: 2

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 378083 Color: 0
Size: 318325 Color: 2
Size: 303593 Color: 4

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 377990 Color: 3
Size: 367907 Color: 0
Size: 254104 Color: 2

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 378083 Color: 0
Size: 320505 Color: 4
Size: 301413 Color: 1

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 377992 Color: 4
Size: 360066 Color: 2
Size: 261943 Color: 1

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 377997 Color: 2
Size: 335214 Color: 0
Size: 286790 Color: 1

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 378090 Color: 3
Size: 324899 Color: 3
Size: 297012 Color: 0

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 378091 Color: 2
Size: 350543 Color: 0
Size: 271367 Color: 4

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 378164 Color: 4
Size: 345539 Color: 0
Size: 276298 Color: 1

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 378164 Color: 1
Size: 320270 Color: 3
Size: 301567 Color: 3

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 378300 Color: 1
Size: 327846 Color: 1
Size: 293855 Color: 4

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 378455 Color: 0
Size: 334538 Color: 1
Size: 287008 Color: 4

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 378374 Color: 4
Size: 317807 Color: 3
Size: 303820 Color: 3

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 378388 Color: 1
Size: 369142 Color: 0
Size: 252471 Color: 3

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 378498 Color: 0
Size: 356730 Color: 1
Size: 264773 Color: 3

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 378435 Color: 2
Size: 317390 Color: 4
Size: 304176 Color: 4

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 378474 Color: 3
Size: 314208 Color: 0
Size: 307319 Color: 3

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 378521 Color: 0
Size: 366864 Color: 4
Size: 254616 Color: 1

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 378503 Color: 2
Size: 325317 Color: 1
Size: 296181 Color: 4

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 378510 Color: 1
Size: 318617 Color: 2
Size: 302874 Color: 0

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 378603 Color: 0
Size: 353148 Color: 3
Size: 268250 Color: 4

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 378522 Color: 1
Size: 353689 Color: 2
Size: 267790 Color: 3

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 378659 Color: 3
Size: 338175 Color: 3
Size: 283167 Color: 2

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 378785 Color: 0
Size: 337548 Color: 4
Size: 283668 Color: 1

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 378861 Color: 2
Size: 345176 Color: 2
Size: 275964 Color: 1

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 378913 Color: 1
Size: 345166 Color: 0
Size: 275922 Color: 3

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 378980 Color: 2
Size: 339732 Color: 1
Size: 281289 Color: 4

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 379071 Color: 4
Size: 326476 Color: 3
Size: 294454 Color: 1

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 379215 Color: 3
Size: 319093 Color: 3
Size: 301693 Color: 4

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 379257 Color: 3
Size: 328531 Color: 3
Size: 292213 Color: 1

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 379403 Color: 0
Size: 355547 Color: 2
Size: 265051 Color: 3

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 379365 Color: 3
Size: 318651 Color: 4
Size: 301985 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 379704 Color: 0
Size: 330881 Color: 2
Size: 289416 Color: 2

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 379575 Color: 1
Size: 345204 Color: 1
Size: 275222 Color: 3

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 379742 Color: 4
Size: 363758 Color: 1
Size: 256501 Color: 0

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 379778 Color: 2
Size: 366313 Color: 0
Size: 253910 Color: 1

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 379920 Color: 2
Size: 366860 Color: 4
Size: 253221 Color: 3

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 379966 Color: 3
Size: 313390 Color: 1
Size: 306645 Color: 1

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 380116 Color: 4
Size: 353230 Color: 0
Size: 266655 Color: 3

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 380218 Color: 2
Size: 353711 Color: 1
Size: 266072 Color: 4

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 380319 Color: 1
Size: 332819 Color: 3
Size: 286863 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 380387 Color: 4
Size: 342452 Color: 2
Size: 277162 Color: 1

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 380451 Color: 1
Size: 367848 Color: 0
Size: 251702 Color: 2

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 380659 Color: 0
Size: 332633 Color: 2
Size: 286709 Color: 2

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 380524 Color: 2
Size: 318221 Color: 4
Size: 301256 Color: 4

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 380561 Color: 4
Size: 337604 Color: 0
Size: 281836 Color: 4

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 380811 Color: 0
Size: 342280 Color: 4
Size: 276910 Color: 1

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 380569 Color: 3
Size: 318420 Color: 1
Size: 301012 Color: 2

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 380609 Color: 2
Size: 317250 Color: 2
Size: 302142 Color: 1

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 380649 Color: 1
Size: 330499 Color: 3
Size: 288853 Color: 0

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 380820 Color: 3
Size: 341202 Color: 4
Size: 277979 Color: 3

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 380852 Color: 4
Size: 353323 Color: 1
Size: 265826 Color: 1

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 381001 Color: 1
Size: 343492 Color: 0
Size: 275508 Color: 4

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 381086 Color: 0
Size: 357011 Color: 3
Size: 261904 Color: 4

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 381091 Color: 3
Size: 329778 Color: 2
Size: 289132 Color: 4

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 381159 Color: 4
Size: 362637 Color: 0
Size: 256205 Color: 3

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 381187 Color: 4
Size: 364721 Color: 1
Size: 254093 Color: 0

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 381246 Color: 1
Size: 356188 Color: 1
Size: 262567 Color: 2

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 381257 Color: 1
Size: 348529 Color: 2
Size: 270215 Color: 4

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 381595 Color: 0
Size: 358426 Color: 1
Size: 259980 Color: 4

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 381464 Color: 2
Size: 362340 Color: 2
Size: 256197 Color: 1

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 381498 Color: 2
Size: 341900 Color: 2
Size: 276603 Color: 1

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 381610 Color: 3
Size: 351903 Color: 1
Size: 266488 Color: 0

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 381639 Color: 3
Size: 311430 Color: 1
Size: 306932 Color: 3

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 381778 Color: 2
Size: 339174 Color: 0
Size: 279049 Color: 3

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 476599 Color: 0
Size: 265801 Color: 4
Size: 257601 Color: 2

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 381678 Color: 1
Size: 351604 Color: 0
Size: 266719 Color: 2

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 381854 Color: 3
Size: 325497 Color: 3
Size: 292650 Color: 0

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 381930 Color: 3
Size: 344296 Color: 4
Size: 273775 Color: 4

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 382001 Color: 3
Size: 356558 Color: 2
Size: 261442 Color: 4

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 382025 Color: 3
Size: 357534 Color: 0
Size: 260442 Color: 1

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 382206 Color: 0
Size: 326597 Color: 1
Size: 291198 Color: 2

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 382249 Color: 0
Size: 308963 Color: 1
Size: 308789 Color: 3

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 382224 Color: 3
Size: 317088 Color: 0
Size: 300689 Color: 2

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 382285 Color: 2
Size: 336080 Color: 4
Size: 281636 Color: 3

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 382358 Color: 0
Size: 334231 Color: 1
Size: 283412 Color: 2

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 382323 Color: 2
Size: 356500 Color: 3
Size: 261178 Color: 1

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 382514 Color: 3
Size: 338675 Color: 1
Size: 278812 Color: 1

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 382515 Color: 4
Size: 325104 Color: 3
Size: 292382 Color: 3

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 382532 Color: 2
Size: 313918 Color: 1
Size: 303551 Color: 0

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 382606 Color: 4
Size: 327422 Color: 1
Size: 289973 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 382751 Color: 1
Size: 313014 Color: 0
Size: 304236 Color: 2

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 382802 Color: 4
Size: 324027 Color: 2
Size: 293172 Color: 4

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 382815 Color: 4
Size: 335701 Color: 0
Size: 281485 Color: 1

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 382821 Color: 1
Size: 356126 Color: 0
Size: 261054 Color: 1

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 382826 Color: 2
Size: 309145 Color: 3
Size: 308030 Color: 4

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 382877 Color: 2
Size: 344862 Color: 0
Size: 272262 Color: 3

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 383019 Color: 2
Size: 312948 Color: 4
Size: 304034 Color: 3

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 383013 Color: 1
Size: 343303 Color: 4
Size: 273685 Color: 2

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 383122 Color: 4
Size: 322818 Color: 2
Size: 294061 Color: 0

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 383125 Color: 2
Size: 328448 Color: 4
Size: 288428 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 383134 Color: 4
Size: 339344 Color: 3
Size: 277523 Color: 2

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 383155 Color: 2
Size: 313192 Color: 0
Size: 303654 Color: 2

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 383189 Color: 1
Size: 342222 Color: 3
Size: 274590 Color: 4

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 383213 Color: 4
Size: 332297 Color: 0
Size: 284491 Color: 2

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 383302 Color: 2
Size: 364400 Color: 4
Size: 252299 Color: 4

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 383367 Color: 2
Size: 328488 Color: 0
Size: 288146 Color: 4

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 383381 Color: 0
Size: 337444 Color: 2
Size: 279176 Color: 3

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 383435 Color: 3
Size: 357443 Color: 2
Size: 259123 Color: 2

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 383468 Color: 3
Size: 314260 Color: 3
Size: 302273 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 383604 Color: 2
Size: 319926 Color: 3
Size: 296471 Color: 1

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 383773 Color: 2
Size: 364206 Color: 0
Size: 252022 Color: 1

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 383825 Color: 1
Size: 313958 Color: 0
Size: 302218 Color: 4

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 383980 Color: 1
Size: 344376 Color: 2
Size: 271645 Color: 4

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 384065 Color: 4
Size: 335112 Color: 0
Size: 280824 Color: 1

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 384043 Color: 0
Size: 320660 Color: 1
Size: 295298 Color: 2

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 384204 Color: 0
Size: 346273 Color: 4
Size: 269524 Color: 3

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 384137 Color: 4
Size: 312983 Color: 0
Size: 302881 Color: 2

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 384222 Color: 0
Size: 347540 Color: 3
Size: 268239 Color: 1

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 369676 Color: 3
Size: 317769 Color: 0
Size: 312556 Color: 2

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 384264 Color: 2
Size: 316460 Color: 0
Size: 299277 Color: 4

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 380067 Color: 3
Size: 362192 Color: 1
Size: 257742 Color: 4

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 384577 Color: 0
Size: 313602 Color: 4
Size: 301822 Color: 4

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 384372 Color: 2
Size: 347837 Color: 1
Size: 267792 Color: 3

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 384512 Color: 4
Size: 338725 Color: 0
Size: 276764 Color: 4

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 384797 Color: 3
Size: 331751 Color: 2
Size: 283453 Color: 1

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 384822 Color: 4
Size: 354771 Color: 0
Size: 260408 Color: 1

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 384806 Color: 1
Size: 352554 Color: 3
Size: 262641 Color: 2

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 384919 Color: 3
Size: 338310 Color: 2
Size: 276772 Color: 0

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 385007 Color: 3
Size: 337372 Color: 0
Size: 277622 Color: 4

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 385050 Color: 2
Size: 352594 Color: 4
Size: 262357 Color: 4

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 385101 Color: 1
Size: 335006 Color: 0
Size: 279894 Color: 3

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 385114 Color: 4
Size: 321224 Color: 3
Size: 293663 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 385198 Color: 4
Size: 339917 Color: 2
Size: 274886 Color: 2

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 385417 Color: 3
Size: 364501 Color: 4
Size: 250083 Color: 4

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 385426 Color: 3
Size: 348555 Color: 0
Size: 266020 Color: 1

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 385489 Color: 2
Size: 314908 Color: 3
Size: 299604 Color: 4

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 385533 Color: 2
Size: 327119 Color: 2
Size: 287349 Color: 0

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 385731 Color: 0
Size: 322593 Color: 1
Size: 291677 Color: 0

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 385554 Color: 3
Size: 322264 Color: 1
Size: 292183 Color: 4

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 385704 Color: 3
Size: 340823 Color: 0
Size: 273474 Color: 1

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 385707 Color: 4
Size: 318503 Color: 1
Size: 295791 Color: 3

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 385752 Color: 1
Size: 328462 Color: 0
Size: 285787 Color: 2

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 385982 Color: 0
Size: 341816 Color: 3
Size: 272203 Color: 2

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 386099 Color: 4
Size: 317310 Color: 3
Size: 296592 Color: 1

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 386148 Color: 2
Size: 324816 Color: 4
Size: 289037 Color: 0

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 386123 Color: 0
Size: 362588 Color: 4
Size: 251290 Color: 1

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 386285 Color: 3
Size: 348393 Color: 2
Size: 265323 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 386272 Color: 0
Size: 346502 Color: 2
Size: 267227 Color: 2

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 386477 Color: 4
Size: 342197 Color: 0
Size: 271327 Color: 2

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 386542 Color: 1
Size: 308910 Color: 2
Size: 304549 Color: 0

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 386644 Color: 0
Size: 350728 Color: 3
Size: 262629 Color: 1

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 386893 Color: 2
Size: 357123 Color: 0
Size: 255985 Color: 3

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 386960 Color: 1
Size: 342659 Color: 0
Size: 270382 Color: 4

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 386987 Color: 4
Size: 347650 Color: 0
Size: 265364 Color: 2

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 387109 Color: 0
Size: 349644 Color: 1
Size: 263248 Color: 4

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 387065 Color: 2
Size: 308173 Color: 0
Size: 304763 Color: 3

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 387091 Color: 1
Size: 344468 Color: 3
Size: 268442 Color: 4

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 387134 Color: 4
Size: 333080 Color: 0
Size: 279787 Color: 1

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 387153 Color: 4
Size: 313487 Color: 1
Size: 299361 Color: 3

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 387239 Color: 2
Size: 318918 Color: 2
Size: 293844 Color: 4

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 387243 Color: 1
Size: 317659 Color: 2
Size: 295099 Color: 0

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 387408 Color: 4
Size: 309162 Color: 0
Size: 303431 Color: 2

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 387428 Color: 4
Size: 330328 Color: 3
Size: 282245 Color: 3

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 387527 Color: 3
Size: 346518 Color: 1
Size: 265956 Color: 2

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 387553 Color: 1
Size: 331489 Color: 3
Size: 280959 Color: 1

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 387627 Color: 1
Size: 306683 Color: 0
Size: 305691 Color: 3

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 387821 Color: 2
Size: 330318 Color: 3
Size: 281862 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 388076 Color: 2
Size: 323997 Color: 4
Size: 287928 Color: 3

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 388189 Color: 4
Size: 313581 Color: 3
Size: 298231 Color: 2

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 388226 Color: 1
Size: 346214 Color: 3
Size: 265561 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 388216 Color: 0
Size: 336456 Color: 1
Size: 275329 Color: 4

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 388382 Color: 2
Size: 359511 Color: 0
Size: 252108 Color: 2

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 388385 Color: 2
Size: 331600 Color: 3
Size: 280016 Color: 2

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 388389 Color: 2
Size: 306026 Color: 0
Size: 305586 Color: 2

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 388513 Color: 3
Size: 360523 Color: 4
Size: 250965 Color: 3

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 388606 Color: 3
Size: 312561 Color: 2
Size: 298834 Color: 2

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 388700 Color: 4
Size: 336906 Color: 1
Size: 274395 Color: 4

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 388704 Color: 3
Size: 309500 Color: 4
Size: 301797 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 388798 Color: 3
Size: 333590 Color: 2
Size: 277613 Color: 4

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 388843 Color: 4
Size: 355676 Color: 2
Size: 255482 Color: 3

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 389065 Color: 0
Size: 307208 Color: 1
Size: 303728 Color: 1

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 389138 Color: 0
Size: 343997 Color: 3
Size: 266866 Color: 4

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 388946 Color: 2
Size: 347209 Color: 1
Size: 263846 Color: 1

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 389144 Color: 0
Size: 307009 Color: 2
Size: 303848 Color: 4

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 389174 Color: 0
Size: 325777 Color: 1
Size: 285050 Color: 1

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 389363 Color: 0
Size: 334103 Color: 1
Size: 276535 Color: 1

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 389248 Color: 3
Size: 320517 Color: 0
Size: 290236 Color: 1

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 389472 Color: 0
Size: 310676 Color: 3
Size: 299853 Color: 4

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 389339 Color: 1
Size: 309605 Color: 2
Size: 301057 Color: 4

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 389556 Color: 4
Size: 324460 Color: 1
Size: 285985 Color: 4

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 389581 Color: 2
Size: 358932 Color: 0
Size: 251488 Color: 1

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 389763 Color: 4
Size: 331722 Color: 2
Size: 278516 Color: 4

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 389766 Color: 4
Size: 338938 Color: 2
Size: 271297 Color: 0

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 389807 Color: 4
Size: 321855 Color: 4
Size: 288339 Color: 2

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 389927 Color: 2
Size: 329879 Color: 1
Size: 280195 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 389934 Color: 4
Size: 330800 Color: 1
Size: 279267 Color: 1

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 389985 Color: 3
Size: 351711 Color: 4
Size: 258305 Color: 3

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 390061 Color: 2
Size: 343264 Color: 1
Size: 266676 Color: 0

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 390274 Color: 2
Size: 308446 Color: 4
Size: 301281 Color: 4

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 390387 Color: 1
Size: 348510 Color: 3
Size: 261104 Color: 0

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 390447 Color: 1
Size: 318142 Color: 3
Size: 291412 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 390514 Color: 2
Size: 307191 Color: 3
Size: 302296 Color: 4

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 390547 Color: 1
Size: 314520 Color: 2
Size: 294934 Color: 0

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 390585 Color: 0
Size: 348280 Color: 4
Size: 261136 Color: 1

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 390596 Color: 0
Size: 338014 Color: 4
Size: 271391 Color: 4

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 390657 Color: 1
Size: 306934 Color: 1
Size: 302410 Color: 2

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 390873 Color: 3
Size: 350505 Color: 4
Size: 258623 Color: 4

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 390889 Color: 1
Size: 341825 Color: 0
Size: 267287 Color: 3

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 391070 Color: 0
Size: 333468 Color: 3
Size: 275463 Color: 2

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 390907 Color: 2
Size: 344197 Color: 1
Size: 264897 Color: 4

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 390923 Color: 3
Size: 335127 Color: 0
Size: 273951 Color: 1

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 391157 Color: 0
Size: 320282 Color: 4
Size: 288562 Color: 3

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 391010 Color: 3
Size: 335118 Color: 1
Size: 273873 Color: 2

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 405125 Color: 4
Size: 322926 Color: 0
Size: 271950 Color: 1

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 391435 Color: 2
Size: 353667 Color: 0
Size: 254899 Color: 2

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 391633 Color: 0
Size: 316416 Color: 1
Size: 291952 Color: 1

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 391442 Color: 2
Size: 320299 Color: 2
Size: 288260 Color: 1

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 391691 Color: 2
Size: 337140 Color: 4
Size: 271170 Color: 2

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 391710 Color: 2
Size: 327716 Color: 1
Size: 280575 Color: 0

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 391882 Color: 2
Size: 354445 Color: 3
Size: 253674 Color: 0

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 391897 Color: 3
Size: 315780 Color: 2
Size: 292324 Color: 1

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 391923 Color: 2
Size: 350286 Color: 3
Size: 257792 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 392057 Color: 3
Size: 318289 Color: 0
Size: 289655 Color: 3

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 392234 Color: 3
Size: 333629 Color: 1
Size: 274138 Color: 4

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 392464 Color: 0
Size: 349360 Color: 2
Size: 258177 Color: 2

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 392333 Color: 4
Size: 331930 Color: 2
Size: 275738 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 392496 Color: 0
Size: 329225 Color: 4
Size: 278280 Color: 2

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 392346 Color: 3
Size: 354233 Color: 2
Size: 253422 Color: 2

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 392361 Color: 4
Size: 311707 Color: 3
Size: 295933 Color: 1

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 392504 Color: 3
Size: 311569 Color: 3
Size: 295928 Color: 0

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 392554 Color: 2
Size: 349027 Color: 1
Size: 258420 Color: 0

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 392587 Color: 0
Size: 349460 Color: 3
Size: 257954 Color: 2

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 393154 Color: 0
Size: 333520 Color: 3
Size: 273327 Color: 2

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 392920 Color: 2
Size: 353638 Color: 3
Size: 253443 Color: 4

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 392998 Color: 4
Size: 309639 Color: 0
Size: 297364 Color: 3

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 393181 Color: 1
Size: 333097 Color: 4
Size: 273723 Color: 2

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 393210 Color: 1
Size: 305842 Color: 2
Size: 300949 Color: 3

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 393391 Color: 0
Size: 317197 Color: 3
Size: 289413 Color: 3

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 393497 Color: 3
Size: 310944 Color: 3
Size: 295560 Color: 4

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 393586 Color: 0
Size: 331255 Color: 1
Size: 275160 Color: 4

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 393770 Color: 4
Size: 315935 Color: 1
Size: 290296 Color: 3

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 393889 Color: 4
Size: 334689 Color: 4
Size: 271423 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 394094 Color: 4
Size: 345825 Color: 2
Size: 260082 Color: 0

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 394172 Color: 2
Size: 333322 Color: 4
Size: 272507 Color: 1

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 394335 Color: 4
Size: 310918 Color: 1
Size: 294748 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 394416 Color: 4
Size: 350673 Color: 4
Size: 254912 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 394478 Color: 4
Size: 336089 Color: 3
Size: 269434 Color: 0

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 394553 Color: 3
Size: 318022 Color: 0
Size: 287426 Color: 2

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 394609 Color: 1
Size: 353179 Color: 3
Size: 252213 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 394951 Color: 0
Size: 313932 Color: 3
Size: 291118 Color: 2

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 394916 Color: 2
Size: 348975 Color: 4
Size: 256110 Color: 1

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 394931 Color: 3
Size: 323359 Color: 3
Size: 281711 Color: 0

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 395126 Color: 4
Size: 313636 Color: 0
Size: 291239 Color: 0

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 395163 Color: 3
Size: 333157 Color: 3
Size: 271681 Color: 1

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 395220 Color: 2
Size: 347351 Color: 3
Size: 257430 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 395268 Color: 1
Size: 332368 Color: 1
Size: 272365 Color: 4

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 395284 Color: 2
Size: 347485 Color: 3
Size: 257232 Color: 0

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 395453 Color: 3
Size: 325078 Color: 1
Size: 279470 Color: 2

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 395504 Color: 3
Size: 308674 Color: 0
Size: 295823 Color: 4

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 395518 Color: 3
Size: 354468 Color: 0
Size: 250015 Color: 1

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 395530 Color: 3
Size: 334339 Color: 2
Size: 270132 Color: 1

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 395610 Color: 2
Size: 350658 Color: 4
Size: 253733 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 395688 Color: 1
Size: 317221 Color: 4
Size: 287092 Color: 0

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 395730 Color: 1
Size: 336145 Color: 0
Size: 268126 Color: 3

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 395804 Color: 4
Size: 313552 Color: 3
Size: 290645 Color: 3

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 396009 Color: 4
Size: 305137 Color: 2
Size: 298855 Color: 2

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 396057 Color: 4
Size: 305676 Color: 1
Size: 298268 Color: 1

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 396130 Color: 4
Size: 336437 Color: 0
Size: 267434 Color: 1

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 396245 Color: 4
Size: 331521 Color: 2
Size: 272235 Color: 3

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 396279 Color: 1
Size: 314856 Color: 0
Size: 288866 Color: 2

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 396289 Color: 2
Size: 326817 Color: 3
Size: 276895 Color: 0

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 396313 Color: 3
Size: 302715 Color: 1
Size: 300973 Color: 2

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 396507 Color: 0
Size: 321272 Color: 1
Size: 282222 Color: 1

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 396491 Color: 1
Size: 304973 Color: 3
Size: 298537 Color: 1

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 396635 Color: 1
Size: 344292 Color: 2
Size: 259074 Color: 2

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 396673 Color: 4
Size: 333863 Color: 0
Size: 269465 Color: 4

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 396691 Color: 4
Size: 352630 Color: 0
Size: 250680 Color: 2

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 396740 Color: 3
Size: 311396 Color: 0
Size: 291865 Color: 2

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 396677 Color: 0
Size: 305704 Color: 2
Size: 297620 Color: 4

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 396853 Color: 4
Size: 330665 Color: 2
Size: 272483 Color: 3

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 396891 Color: 3
Size: 308206 Color: 0
Size: 294904 Color: 1

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 397064 Color: 0
Size: 309835 Color: 2
Size: 293102 Color: 3

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 396954 Color: 3
Size: 331211 Color: 2
Size: 271836 Color: 2

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 397070 Color: 0
Size: 349244 Color: 2
Size: 253687 Color: 4

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 397155 Color: 0
Size: 331157 Color: 3
Size: 271689 Color: 3

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 397059 Color: 2
Size: 323212 Color: 3
Size: 279730 Color: 1

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 397383 Color: 4
Size: 311316 Color: 3
Size: 291302 Color: 2

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 397446 Color: 4
Size: 342036 Color: 2
Size: 260519 Color: 2

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 397522 Color: 1
Size: 316871 Color: 3
Size: 285608 Color: 3

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 397544 Color: 4
Size: 332083 Color: 3
Size: 270374 Color: 0

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 397551 Color: 3
Size: 307569 Color: 1
Size: 294881 Color: 4

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 397673 Color: 0
Size: 311999 Color: 3
Size: 290329 Color: 4

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 397579 Color: 1
Size: 320119 Color: 2
Size: 282303 Color: 3

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 397594 Color: 1
Size: 312365 Color: 0
Size: 290042 Color: 1

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 397746 Color: 0
Size: 325257 Color: 3
Size: 276998 Color: 4

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 397796 Color: 1
Size: 316561 Color: 1
Size: 285644 Color: 3

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 397826 Color: 2
Size: 323508 Color: 3
Size: 278667 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 397950 Color: 0
Size: 336259 Color: 4
Size: 265792 Color: 4

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 398052 Color: 1
Size: 323891 Color: 3
Size: 278058 Color: 3

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 398103 Color: 3
Size: 311570 Color: 1
Size: 290328 Color: 4

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 398148 Color: 1
Size: 336654 Color: 1
Size: 265199 Color: 2

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 398588 Color: 1
Size: 340174 Color: 4
Size: 261239 Color: 0

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 398585 Color: 4
Size: 329780 Color: 1
Size: 271636 Color: 3

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 398593 Color: 1
Size: 329207 Color: 3
Size: 272201 Color: 3

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 398686 Color: 1
Size: 329448 Color: 3
Size: 271867 Color: 3

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 398851 Color: 3
Size: 330955 Color: 0
Size: 270195 Color: 1

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 398879 Color: 2
Size: 313593 Color: 3
Size: 287529 Color: 3

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 398910 Color: 4
Size: 340609 Color: 4
Size: 260482 Color: 1

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 399124 Color: 2
Size: 333596 Color: 0
Size: 267281 Color: 2

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 399212 Color: 1
Size: 331744 Color: 0
Size: 269045 Color: 4

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 399426 Color: 2
Size: 309015 Color: 3
Size: 291560 Color: 4

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 399448 Color: 2
Size: 320999 Color: 3
Size: 279554 Color: 1

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 399518 Color: 1
Size: 328444 Color: 0
Size: 272039 Color: 3

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 399644 Color: 1
Size: 349211 Color: 0
Size: 251146 Color: 3

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 399646 Color: 4
Size: 313490 Color: 1
Size: 286865 Color: 1

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 399958 Color: 0
Size: 303017 Color: 2
Size: 297026 Color: 1

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 399909 Color: 3
Size: 329690 Color: 0
Size: 270402 Color: 2

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 400191 Color: 0
Size: 313431 Color: 3
Size: 286379 Color: 2

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 482092 Color: 1
Size: 260667 Color: 1
Size: 257242 Color: 4

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 400199 Color: 1
Size: 308633 Color: 4
Size: 291169 Color: 0

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 400206 Color: 1
Size: 320010 Color: 2
Size: 279785 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 400332 Color: 2
Size: 335112 Color: 1
Size: 264557 Color: 4

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 400344 Color: 1
Size: 333287 Color: 0
Size: 266370 Color: 3

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 400424 Color: 3
Size: 338379 Color: 3
Size: 261198 Color: 2

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 400476 Color: 3
Size: 344228 Color: 0
Size: 255297 Color: 2

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 400545 Color: 2
Size: 303860 Color: 4
Size: 295596 Color: 0

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 400609 Color: 4
Size: 316919 Color: 2
Size: 282473 Color: 4

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 400730 Color: 0
Size: 310836 Color: 3
Size: 288435 Color: 4

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 400954 Color: 1
Size: 328846 Color: 4
Size: 270201 Color: 2

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 400971 Color: 2
Size: 305162 Color: 0
Size: 293868 Color: 4

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 401005 Color: 3
Size: 339715 Color: 2
Size: 259281 Color: 2

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 401033 Color: 4
Size: 323538 Color: 0
Size: 275430 Color: 3

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 401040 Color: 1
Size: 309355 Color: 2
Size: 289606 Color: 3

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 401056 Color: 1
Size: 310405 Color: 2
Size: 288540 Color: 1

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 401322 Color: 0
Size: 326047 Color: 4
Size: 272632 Color: 3

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 401483 Color: 2
Size: 307544 Color: 1
Size: 290974 Color: 0

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 401777 Color: 1
Size: 300919 Color: 4
Size: 297305 Color: 4

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 401856 Color: 1
Size: 313332 Color: 2
Size: 284813 Color: 3

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 401868 Color: 0
Size: 302757 Color: 0
Size: 295376 Color: 1

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 401988 Color: 1
Size: 315550 Color: 0
Size: 282463 Color: 4

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 402029 Color: 3
Size: 312724 Color: 1
Size: 285248 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 402111 Color: 2
Size: 311093 Color: 3
Size: 286797 Color: 1

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 402164 Color: 3
Size: 341845 Color: 2
Size: 255992 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 402219 Color: 4
Size: 339197 Color: 4
Size: 258585 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 402312 Color: 3
Size: 300850 Color: 1
Size: 296839 Color: 4

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 402499 Color: 4
Size: 313722 Color: 0
Size: 283780 Color: 1

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 402533 Color: 2
Size: 331131 Color: 4
Size: 266337 Color: 0

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 402543 Color: 3
Size: 306777 Color: 3
Size: 290681 Color: 0

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 402847 Color: 0
Size: 300026 Color: 2
Size: 297128 Color: 4

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 402588 Color: 1
Size: 306081 Color: 4
Size: 291332 Color: 4

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 402751 Color: 2
Size: 328240 Color: 0
Size: 269010 Color: 4

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 403089 Color: 3
Size: 318777 Color: 3
Size: 278135 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 403093 Color: 3
Size: 313373 Color: 1
Size: 283535 Color: 0

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 403226 Color: 4
Size: 330440 Color: 2
Size: 266335 Color: 1

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 403499 Color: 2
Size: 339647 Color: 1
Size: 256855 Color: 4

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 403600 Color: 2
Size: 341307 Color: 0
Size: 255094 Color: 3

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 403607 Color: 2
Size: 338131 Color: 0
Size: 258263 Color: 4

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 403637 Color: 4
Size: 307979 Color: 1
Size: 288385 Color: 3

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 403676 Color: 2
Size: 326417 Color: 3
Size: 269908 Color: 0

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 403598 Color: 1
Size: 330278 Color: 4
Size: 266125 Color: 3

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 403684 Color: 2
Size: 330179 Color: 4
Size: 266138 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 403779 Color: 4
Size: 336366 Color: 3
Size: 259856 Color: 1

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 403803 Color: 4
Size: 308266 Color: 0
Size: 287932 Color: 2

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 403842 Color: 3
Size: 345199 Color: 4
Size: 250960 Color: 0

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 403853 Color: 3
Size: 334723 Color: 2
Size: 261425 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 403860 Color: 1
Size: 327033 Color: 4
Size: 269108 Color: 2

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 404077 Color: 0
Size: 322681 Color: 2
Size: 273243 Color: 1

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 404007 Color: 3
Size: 329084 Color: 2
Size: 266910 Color: 2

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 404129 Color: 2
Size: 338698 Color: 0
Size: 257174 Color: 1

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 404248 Color: 2
Size: 302443 Color: 1
Size: 293310 Color: 3

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 404171 Color: 1
Size: 342563 Color: 1
Size: 253267 Color: 4

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 404392 Color: 1
Size: 335588 Color: 1
Size: 260021 Color: 3

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 404317 Color: 2
Size: 320966 Color: 3
Size: 274718 Color: 3

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 404403 Color: 4
Size: 327761 Color: 1
Size: 267837 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 404444 Color: 1
Size: 341229 Color: 2
Size: 254328 Color: 3

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 404517 Color: 2
Size: 323617 Color: 1
Size: 271867 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 404559 Color: 2
Size: 305221 Color: 0
Size: 290221 Color: 1

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 404551 Color: 0
Size: 317576 Color: 1
Size: 277874 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 404567 Color: 2
Size: 344622 Color: 2
Size: 250812 Color: 4

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 404582 Color: 4
Size: 316547 Color: 3
Size: 278872 Color: 4

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 404603 Color: 2
Size: 326678 Color: 1
Size: 268720 Color: 0

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 404755 Color: 0
Size: 321403 Color: 3
Size: 273843 Color: 3

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 404802 Color: 3
Size: 342726 Color: 2
Size: 252473 Color: 0

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 404883 Color: 2
Size: 314650 Color: 0
Size: 280468 Color: 3

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 404994 Color: 1
Size: 298619 Color: 1
Size: 296388 Color: 2

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 405237 Color: 2
Size: 340109 Color: 2
Size: 254655 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 405386 Color: 0
Size: 307029 Color: 2
Size: 287586 Color: 1

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 405425 Color: 1
Size: 329008 Color: 4
Size: 265568 Color: 4

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 405443 Color: 2
Size: 337152 Color: 4
Size: 257406 Color: 2

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 405512 Color: 0
Size: 302298 Color: 4
Size: 292191 Color: 3

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 405445 Color: 1
Size: 300959 Color: 2
Size: 293597 Color: 1

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 405556 Color: 3
Size: 325693 Color: 0
Size: 268752 Color: 4

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 405700 Color: 0
Size: 313723 Color: 4
Size: 280578 Color: 2

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 405890 Color: 2
Size: 328363 Color: 0
Size: 265748 Color: 4

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 405996 Color: 0
Size: 299406 Color: 1
Size: 294599 Color: 0

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 406124 Color: 3
Size: 299785 Color: 0
Size: 294092 Color: 4

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 406301 Color: 1
Size: 340222 Color: 3
Size: 253478 Color: 2

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 406280 Color: 3
Size: 305074 Color: 2
Size: 288647 Color: 1

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 406424 Color: 1
Size: 318793 Color: 4
Size: 274784 Color: 2

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 406376 Color: 4
Size: 319231 Color: 0
Size: 274394 Color: 3

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 406518 Color: 2
Size: 311585 Color: 3
Size: 281898 Color: 1

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 406551 Color: 0
Size: 324279 Color: 2
Size: 269171 Color: 3

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 406611 Color: 3
Size: 332039 Color: 4
Size: 261351 Color: 1

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 406878 Color: 3
Size: 342604 Color: 0
Size: 250519 Color: 0

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 406894 Color: 2
Size: 303222 Color: 0
Size: 289885 Color: 1

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 407110 Color: 3
Size: 311986 Color: 2
Size: 280905 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 407120 Color: 0
Size: 324557 Color: 1
Size: 268324 Color: 3

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 407191 Color: 3
Size: 315429 Color: 0
Size: 277381 Color: 2

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 407397 Color: 0
Size: 298295 Color: 1
Size: 294309 Color: 0

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 407790 Color: 0
Size: 312959 Color: 4
Size: 279252 Color: 4

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 407914 Color: 0
Size: 329728 Color: 0
Size: 262359 Color: 1

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 408046 Color: 4
Size: 338130 Color: 0
Size: 253825 Color: 2

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 408059 Color: 3
Size: 308677 Color: 1
Size: 283265 Color: 0

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 408136 Color: 4
Size: 334684 Color: 0
Size: 257181 Color: 3

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 408138 Color: 4
Size: 311691 Color: 0
Size: 280172 Color: 3

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 408305 Color: 2
Size: 323416 Color: 3
Size: 268280 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 408395 Color: 3
Size: 315238 Color: 1
Size: 276368 Color: 4

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 408565 Color: 2
Size: 313636 Color: 2
Size: 277800 Color: 1

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 408637 Color: 2
Size: 300789 Color: 4
Size: 290575 Color: 4

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 408662 Color: 2
Size: 329474 Color: 2
Size: 261865 Color: 1

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 408769 Color: 1
Size: 333087 Color: 1
Size: 258145 Color: 4

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 408940 Color: 3
Size: 339913 Color: 3
Size: 251148 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 408952 Color: 3
Size: 312684 Color: 1
Size: 278365 Color: 2

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 408979 Color: 2
Size: 321401 Color: 3
Size: 269621 Color: 0

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 409000 Color: 4
Size: 316692 Color: 0
Size: 274309 Color: 4

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 409098 Color: 3
Size: 329655 Color: 0
Size: 261248 Color: 4

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 409114 Color: 1
Size: 312796 Color: 4
Size: 278091 Color: 2

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 409177 Color: 4
Size: 302588 Color: 0
Size: 288236 Color: 3

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 409183 Color: 2
Size: 338006 Color: 4
Size: 252812 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 409472 Color: 3
Size: 326501 Color: 0
Size: 264028 Color: 4

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 409531 Color: 3
Size: 326086 Color: 1
Size: 264384 Color: 0

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 409544 Color: 3
Size: 312901 Color: 1
Size: 277556 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 409633 Color: 1
Size: 331661 Color: 3
Size: 258707 Color: 4

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 409635 Color: 4
Size: 318476 Color: 3
Size: 271890 Color: 0

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 409663 Color: 4
Size: 322434 Color: 4
Size: 267904 Color: 3

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 409805 Color: 1
Size: 326146 Color: 3
Size: 264050 Color: 3

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 409820 Color: 1
Size: 307891 Color: 2
Size: 282290 Color: 2

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 409888 Color: 2
Size: 297851 Color: 4
Size: 292262 Color: 1

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 409911 Color: 2
Size: 298814 Color: 4
Size: 291276 Color: 0

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 409992 Color: 2
Size: 296832 Color: 3
Size: 293177 Color: 1

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 410072 Color: 2
Size: 296960 Color: 3
Size: 292969 Color: 2

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 410174 Color: 0
Size: 297115 Color: 2
Size: 292712 Color: 1

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 410168 Color: 3
Size: 319748 Color: 4
Size: 270085 Color: 3

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 410172 Color: 3
Size: 305139 Color: 0
Size: 284690 Color: 1

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 410235 Color: 4
Size: 314192 Color: 4
Size: 275574 Color: 0

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 410342 Color: 0
Size: 327747 Color: 3
Size: 261912 Color: 4

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 410283 Color: 1
Size: 328408 Color: 1
Size: 261310 Color: 3

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 410283 Color: 2
Size: 328810 Color: 3
Size: 260908 Color: 0

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 410398 Color: 4
Size: 331710 Color: 4
Size: 257893 Color: 0

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 410467 Color: 1
Size: 332876 Color: 4
Size: 256658 Color: 0

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 410469 Color: 4
Size: 334815 Color: 1
Size: 254717 Color: 3

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 410476 Color: 2
Size: 317236 Color: 2
Size: 272289 Color: 0

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 410488 Color: 1
Size: 317356 Color: 4
Size: 272157 Color: 4

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 410615 Color: 3
Size: 329285 Color: 0
Size: 260101 Color: 4

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 410652 Color: 3
Size: 329637 Color: 1
Size: 259712 Color: 4

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 410780 Color: 3
Size: 335157 Color: 1
Size: 254064 Color: 3

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 410865 Color: 2
Size: 315856 Color: 1
Size: 273280 Color: 3

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 410887 Color: 3
Size: 320665 Color: 0
Size: 268449 Color: 4

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 410932 Color: 3
Size: 326715 Color: 0
Size: 262354 Color: 4

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 411044 Color: 1
Size: 323457 Color: 3
Size: 265500 Color: 0

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 411060 Color: 0
Size: 296735 Color: 2
Size: 292206 Color: 3

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 411096 Color: 4
Size: 331343 Color: 3
Size: 257562 Color: 4

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 411318 Color: 0
Size: 312685 Color: 4
Size: 275998 Color: 2

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 411190 Color: 1
Size: 295834 Color: 2
Size: 292977 Color: 3

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 411469 Color: 0
Size: 327715 Color: 2
Size: 260817 Color: 4

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 411416 Color: 4
Size: 337215 Color: 2
Size: 251370 Color: 0

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 411418 Color: 1
Size: 295565 Color: 4
Size: 293018 Color: 4

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 411442 Color: 2
Size: 307657 Color: 1
Size: 280902 Color: 0

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 411497 Color: 0
Size: 337562 Color: 4
Size: 250942 Color: 3

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 411692 Color: 0
Size: 308337 Color: 4
Size: 279972 Color: 4

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 411657 Color: 2
Size: 326663 Color: 3
Size: 261681 Color: 1

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 411733 Color: 0
Size: 332610 Color: 2
Size: 255658 Color: 2

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 411884 Color: 1
Size: 323716 Color: 2
Size: 264401 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 411916 Color: 1
Size: 334353 Color: 2
Size: 253732 Color: 4

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 411964 Color: 0
Size: 322532 Color: 4
Size: 265505 Color: 2

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 411994 Color: 3
Size: 326689 Color: 1
Size: 261318 Color: 1

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 412033 Color: 3
Size: 327485 Color: 4
Size: 260483 Color: 0

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 412230 Color: 3
Size: 321734 Color: 4
Size: 266037 Color: 4

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 412255 Color: 2
Size: 319036 Color: 4
Size: 268710 Color: 3

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 412356 Color: 0
Size: 304570 Color: 4
Size: 283075 Color: 1

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 412515 Color: 0
Size: 305807 Color: 3
Size: 281679 Color: 4

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 412523 Color: 0
Size: 324925 Color: 3
Size: 262553 Color: 1

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 412474 Color: 3
Size: 308434 Color: 2
Size: 279093 Color: 3

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 412531 Color: 3
Size: 299724 Color: 1
Size: 287746 Color: 4

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 412582 Color: 3
Size: 319344 Color: 0
Size: 268075 Color: 1

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 412678 Color: 4
Size: 315887 Color: 1
Size: 271436 Color: 1

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 413061 Color: 1
Size: 310492 Color: 0
Size: 276448 Color: 3

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 413064 Color: 3
Size: 301737 Color: 0
Size: 285200 Color: 1

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 413187 Color: 2
Size: 304295 Color: 1
Size: 282519 Color: 1

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 413189 Color: 1
Size: 298319 Color: 4
Size: 288493 Color: 2

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 413199 Color: 1
Size: 324328 Color: 4
Size: 262474 Color: 0

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 413410 Color: 0
Size: 315052 Color: 4
Size: 271539 Color: 4

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 413259 Color: 4
Size: 317139 Color: 1
Size: 269603 Color: 3

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 413687 Color: 0
Size: 321169 Color: 1
Size: 265145 Color: 1

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 413696 Color: 4
Size: 332094 Color: 2
Size: 254211 Color: 4

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 413739 Color: 2
Size: 333058 Color: 3
Size: 253204 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 413796 Color: 3
Size: 316392 Color: 4
Size: 269813 Color: 2

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 413813 Color: 2
Size: 328857 Color: 2
Size: 257331 Color: 0

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 413906 Color: 4
Size: 330056 Color: 2
Size: 256039 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 413908 Color: 2
Size: 295242 Color: 3
Size: 290851 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 413927 Color: 4
Size: 296255 Color: 2
Size: 289819 Color: 4

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 413943 Color: 0
Size: 333542 Color: 2
Size: 252516 Color: 1

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 413962 Color: 1
Size: 312995 Color: 0
Size: 273044 Color: 2

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 413982 Color: 3
Size: 312782 Color: 0
Size: 273237 Color: 4

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 413992 Color: 2
Size: 302908 Color: 3
Size: 283101 Color: 4

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 414250 Color: 4
Size: 315513 Color: 3
Size: 270238 Color: 0

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 414268 Color: 1
Size: 304373 Color: 2
Size: 281360 Color: 4

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 414275 Color: 1
Size: 300710 Color: 2
Size: 285016 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 414293 Color: 2
Size: 327983 Color: 3
Size: 257725 Color: 1

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 414360 Color: 1
Size: 296444 Color: 4
Size: 289197 Color: 3

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 414372 Color: 3
Size: 301137 Color: 3
Size: 284492 Color: 2

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 414496 Color: 4
Size: 305007 Color: 0
Size: 280498 Color: 4

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 414504 Color: 4
Size: 294603 Color: 4
Size: 290894 Color: 2

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 414847 Color: 1
Size: 326474 Color: 1
Size: 258680 Color: 3

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 414858 Color: 3
Size: 293053 Color: 0
Size: 292090 Color: 1

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 414911 Color: 2
Size: 331883 Color: 0
Size: 253207 Color: 4

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 353176 Color: 0
Size: 348455 Color: 1
Size: 298370 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 415606 Color: 2
Size: 331030 Color: 2
Size: 253365 Color: 4

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 415715 Color: 0
Size: 317731 Color: 1
Size: 266555 Color: 4

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 415749 Color: 3
Size: 300421 Color: 4
Size: 283831 Color: 1

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 415795 Color: 1
Size: 319459 Color: 2
Size: 264747 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 415874 Color: 4
Size: 330742 Color: 1
Size: 253385 Color: 3

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 416010 Color: 3
Size: 313029 Color: 0
Size: 270962 Color: 4

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 416082 Color: 3
Size: 300260 Color: 1
Size: 283659 Color: 3

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 416207 Color: 1
Size: 322883 Color: 2
Size: 260911 Color: 2

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 416222 Color: 3
Size: 311708 Color: 4
Size: 272071 Color: 0

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 416223 Color: 4
Size: 304919 Color: 0
Size: 278859 Color: 3

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 456881 Color: 2
Size: 276429 Color: 3
Size: 266691 Color: 0

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 416258 Color: 2
Size: 323962 Color: 2
Size: 259781 Color: 0

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 416286 Color: 3
Size: 302247 Color: 4
Size: 281468 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 416299 Color: 1
Size: 322312 Color: 3
Size: 261390 Color: 4

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 416384 Color: 3
Size: 321976 Color: 4
Size: 261641 Color: 2

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 416502 Color: 3
Size: 293851 Color: 2
Size: 289648 Color: 1

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 416612 Color: 2
Size: 316208 Color: 1
Size: 267181 Color: 1

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 416695 Color: 0
Size: 318248 Color: 1
Size: 265058 Color: 4

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 416653 Color: 4
Size: 295393 Color: 3
Size: 287955 Color: 1

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 416782 Color: 0
Size: 293114 Color: 2
Size: 290105 Color: 2

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 416821 Color: 0
Size: 312559 Color: 3
Size: 270621 Color: 3

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 416817 Color: 3
Size: 292832 Color: 3
Size: 290352 Color: 1

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 416955 Color: 1
Size: 327615 Color: 2
Size: 255431 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 416887 Color: 1
Size: 307358 Color: 4
Size: 275756 Color: 3

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 416938 Color: 3
Size: 313377 Color: 1
Size: 269686 Color: 0

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 417138 Color: 2
Size: 292923 Color: 3
Size: 289940 Color: 2

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 417265 Color: 3
Size: 318754 Color: 1
Size: 263982 Color: 3

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 417401 Color: 2
Size: 320954 Color: 4
Size: 261646 Color: 0

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 417493 Color: 4
Size: 329639 Color: 2
Size: 252869 Color: 1

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 417592 Color: 1
Size: 318822 Color: 4
Size: 263587 Color: 0

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 417694 Color: 1
Size: 325541 Color: 0
Size: 256766 Color: 4

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 417706 Color: 2
Size: 315961 Color: 3
Size: 266334 Color: 3

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 417748 Color: 3
Size: 304557 Color: 1
Size: 277696 Color: 1

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 417800 Color: 3
Size: 300254 Color: 1
Size: 281947 Color: 0

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 417806 Color: 1
Size: 306328 Color: 0
Size: 275867 Color: 3

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 417831 Color: 3
Size: 328567 Color: 4
Size: 253603 Color: 2

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 417867 Color: 3
Size: 305873 Color: 0
Size: 276261 Color: 1

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 417918 Color: 4
Size: 291774 Color: 1
Size: 290309 Color: 3

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 417959 Color: 2
Size: 298864 Color: 3
Size: 283178 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 417964 Color: 1
Size: 291785 Color: 2
Size: 290252 Color: 2

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 418005 Color: 4
Size: 316431 Color: 0
Size: 265565 Color: 3

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 418088 Color: 2
Size: 312663 Color: 0
Size: 269250 Color: 1

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 418089 Color: 3
Size: 299429 Color: 2
Size: 282483 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 418116 Color: 3
Size: 330203 Color: 2
Size: 251682 Color: 3

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 418191 Color: 0
Size: 316736 Color: 4
Size: 265074 Color: 4

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 418135 Color: 4
Size: 308370 Color: 3
Size: 273496 Color: 4

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 418155 Color: 1
Size: 293548 Color: 1
Size: 288298 Color: 3

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 418317 Color: 0
Size: 315627 Color: 1
Size: 266057 Color: 3

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 418200 Color: 3
Size: 307404 Color: 4
Size: 274397 Color: 2

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 418210 Color: 2
Size: 321767 Color: 1
Size: 260024 Color: 3

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 418258 Color: 1
Size: 315972 Color: 1
Size: 265771 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 418345 Color: 2
Size: 309132 Color: 4
Size: 272524 Color: 1

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 418686 Color: 4
Size: 307907 Color: 2
Size: 273408 Color: 2

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 418691 Color: 1
Size: 314076 Color: 2
Size: 267234 Color: 2

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 418703 Color: 4
Size: 291650 Color: 2
Size: 289648 Color: 0

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 418767 Color: 2
Size: 308281 Color: 3
Size: 272953 Color: 1

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 418797 Color: 4
Size: 297802 Color: 2
Size: 283402 Color: 0

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 418827 Color: 3
Size: 306999 Color: 1
Size: 274175 Color: 4

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 418922 Color: 4
Size: 329955 Color: 0
Size: 251124 Color: 0

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 419030 Color: 0
Size: 290997 Color: 1
Size: 289974 Color: 0

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 419178 Color: 4
Size: 294686 Color: 2
Size: 286137 Color: 4

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 419202 Color: 1
Size: 323405 Color: 0
Size: 257394 Color: 2

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 419144 Color: 0
Size: 319574 Color: 3
Size: 261283 Color: 2

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 419255 Color: 1
Size: 319331 Color: 1
Size: 261415 Color: 3

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 419293 Color: 2
Size: 303873 Color: 2
Size: 276835 Color: 3

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 419415 Color: 0
Size: 300904 Color: 2
Size: 279682 Color: 4

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 419408 Color: 4
Size: 313753 Color: 0
Size: 266840 Color: 4

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 419457 Color: 3
Size: 294582 Color: 1
Size: 285962 Color: 2

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 419480 Color: 1
Size: 309478 Color: 0
Size: 271043 Color: 1

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 419546 Color: 1
Size: 318741 Color: 0
Size: 261714 Color: 4

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 419615 Color: 3
Size: 290364 Color: 4
Size: 290022 Color: 3

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 419650 Color: 1
Size: 293083 Color: 3
Size: 287268 Color: 0

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 419719 Color: 2
Size: 325309 Color: 0
Size: 254973 Color: 3

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 419724 Color: 2
Size: 319750 Color: 0
Size: 260527 Color: 1

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 419726 Color: 4
Size: 322330 Color: 1
Size: 257945 Color: 1

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 419800 Color: 0
Size: 311216 Color: 3
Size: 268985 Color: 2

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 419969 Color: 3
Size: 296203 Color: 1
Size: 283829 Color: 1

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 422973 Color: 1
Size: 302022 Color: 4
Size: 275006 Color: 0

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 420142 Color: 3
Size: 290959 Color: 1
Size: 288900 Color: 4

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 420158 Color: 4
Size: 321766 Color: 0
Size: 258077 Color: 4

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 420214 Color: 3
Size: 292869 Color: 1
Size: 286918 Color: 4

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 420216 Color: 1
Size: 318540 Color: 0
Size: 261245 Color: 3

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 420260 Color: 1
Size: 307808 Color: 1
Size: 271933 Color: 2

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 420265 Color: 2
Size: 316870 Color: 3
Size: 262866 Color: 0

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 420305 Color: 4
Size: 313358 Color: 2
Size: 266338 Color: 4

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 420390 Color: 4
Size: 299431 Color: 1
Size: 280180 Color: 3

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 420464 Color: 2
Size: 308009 Color: 4
Size: 271528 Color: 1

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 420551 Color: 1
Size: 307417 Color: 3
Size: 272033 Color: 3

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 420753 Color: 4
Size: 307550 Color: 3
Size: 271698 Color: 1

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 420754 Color: 2
Size: 328231 Color: 3
Size: 251016 Color: 0

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 420984 Color: 1
Size: 305085 Color: 2
Size: 273932 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 420978 Color: 3
Size: 306980 Color: 4
Size: 272043 Color: 4

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 420981 Color: 3
Size: 308123 Color: 1
Size: 270897 Color: 0

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 421119 Color: 1
Size: 292810 Color: 2
Size: 286072 Color: 0

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 421134 Color: 0
Size: 301411 Color: 2
Size: 277456 Color: 4

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 421497 Color: 4
Size: 307467 Color: 1
Size: 271037 Color: 4

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 421593 Color: 2
Size: 322156 Color: 4
Size: 256252 Color: 0

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 421755 Color: 1
Size: 321620 Color: 3
Size: 256626 Color: 2

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 421669 Color: 4
Size: 324429 Color: 0
Size: 253903 Color: 0

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 421900 Color: 1
Size: 324232 Color: 3
Size: 253869 Color: 0

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 421796 Color: 2
Size: 320904 Color: 0
Size: 257301 Color: 2

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 422064 Color: 4
Size: 323581 Color: 4
Size: 254356 Color: 1

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 422352 Color: 2
Size: 321857 Color: 3
Size: 255792 Color: 2

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 422571 Color: 0
Size: 314473 Color: 0
Size: 262957 Color: 1

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 422555 Color: 2
Size: 291394 Color: 0
Size: 286052 Color: 3

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 422732 Color: 0
Size: 301689 Color: 1
Size: 275580 Color: 3

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 422915 Color: 0
Size: 325551 Color: 1
Size: 251535 Color: 2

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 422971 Color: 4
Size: 304734 Color: 0
Size: 272296 Color: 2

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 423020 Color: 2
Size: 310524 Color: 0
Size: 266457 Color: 4

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 423091 Color: 0
Size: 312087 Color: 2
Size: 264823 Color: 4

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 423139 Color: 1
Size: 308038 Color: 2
Size: 268824 Color: 4

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 423194 Color: 3
Size: 297629 Color: 0
Size: 279178 Color: 3

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 423709 Color: 0
Size: 317898 Color: 2
Size: 258394 Color: 2

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 423727 Color: 3
Size: 296656 Color: 1
Size: 279618 Color: 4

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 423737 Color: 2
Size: 319196 Color: 2
Size: 257068 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 423776 Color: 0
Size: 299451 Color: 1
Size: 276774 Color: 2

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 423801 Color: 2
Size: 324194 Color: 3
Size: 252006 Color: 1

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 423904 Color: 4
Size: 308994 Color: 3
Size: 267103 Color: 4

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 423917 Color: 1
Size: 319255 Color: 0
Size: 256829 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 423970 Color: 4
Size: 293084 Color: 0
Size: 282947 Color: 3

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 423974 Color: 3
Size: 294503 Color: 1
Size: 281524 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 424222 Color: 2
Size: 296160 Color: 3
Size: 279619 Color: 4

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 424280 Color: 4
Size: 296401 Color: 0
Size: 279320 Color: 1

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 424308 Color: 3
Size: 288604 Color: 2
Size: 287089 Color: 4

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 424334 Color: 1
Size: 321355 Color: 2
Size: 254312 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 424515 Color: 1
Size: 315067 Color: 4
Size: 260419 Color: 4

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 424683 Color: 1
Size: 308979 Color: 2
Size: 266339 Color: 1

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 424850 Color: 2
Size: 290533 Color: 1
Size: 284618 Color: 3

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 424909 Color: 4
Size: 309875 Color: 4
Size: 265217 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 424983 Color: 0
Size: 292710 Color: 2
Size: 282308 Color: 1

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 424987 Color: 2
Size: 317352 Color: 2
Size: 257662 Color: 4

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 425070 Color: 2
Size: 313925 Color: 2
Size: 261006 Color: 0

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 425223 Color: 1
Size: 318830 Color: 0
Size: 255948 Color: 3

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 425248 Color: 0
Size: 298820 Color: 2
Size: 275933 Color: 3

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 425278 Color: 4
Size: 301315 Color: 4
Size: 273408 Color: 0

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 425280 Color: 3
Size: 290000 Color: 0
Size: 284721 Color: 3

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 425282 Color: 3
Size: 324204 Color: 4
Size: 250515 Color: 4

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 425356 Color: 4
Size: 289010 Color: 0
Size: 285635 Color: 3

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 425428 Color: 2
Size: 300473 Color: 2
Size: 274100 Color: 4

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 425508 Color: 3
Size: 322238 Color: 2
Size: 252255 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 425588 Color: 1
Size: 290865 Color: 4
Size: 283548 Color: 2

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 425763 Color: 0
Size: 313160 Color: 4
Size: 261078 Color: 2

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 425772 Color: 3
Size: 290327 Color: 2
Size: 283902 Color: 2

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 425879 Color: 1
Size: 303844 Color: 2
Size: 270278 Color: 4

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 425991 Color: 1
Size: 299349 Color: 4
Size: 274661 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 426046 Color: 2
Size: 319359 Color: 3
Size: 254596 Color: 1

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 426143 Color: 3
Size: 308711 Color: 2
Size: 265147 Color: 1

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 426293 Color: 4
Size: 297375 Color: 3
Size: 276333 Color: 2

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 426315 Color: 2
Size: 323675 Color: 0
Size: 250011 Color: 3

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 426380 Color: 0
Size: 308751 Color: 4
Size: 264870 Color: 2

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 426372 Color: 1
Size: 295312 Color: 2
Size: 278317 Color: 2

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 426407 Color: 3
Size: 295414 Color: 4
Size: 278180 Color: 1

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 426437 Color: 4
Size: 308587 Color: 0
Size: 264977 Color: 1

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 426494 Color: 3
Size: 301708 Color: 4
Size: 271799 Color: 4

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 426498 Color: 1
Size: 298845 Color: 0
Size: 274658 Color: 2

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 426510 Color: 4
Size: 319290 Color: 3
Size: 254201 Color: 0

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 426618 Color: 1
Size: 318177 Color: 3
Size: 255206 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 426688 Color: 2
Size: 294380 Color: 1
Size: 278933 Color: 0

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 426765 Color: 3
Size: 306271 Color: 4
Size: 266965 Color: 2

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 426767 Color: 2
Size: 316844 Color: 0
Size: 256390 Color: 4

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 426848 Color: 4
Size: 303223 Color: 1
Size: 269930 Color: 0

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 427025 Color: 3
Size: 294147 Color: 4
Size: 278829 Color: 0

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 427042 Color: 3
Size: 319429 Color: 3
Size: 253530 Color: 0

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 427154 Color: 4
Size: 287205 Color: 1
Size: 285642 Color: 1

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 427236 Color: 0
Size: 320511 Color: 1
Size: 252254 Color: 3

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 427361 Color: 0
Size: 312790 Color: 4
Size: 259850 Color: 1

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 427255 Color: 3
Size: 292048 Color: 2
Size: 280698 Color: 1

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 427524 Color: 4
Size: 302396 Color: 1
Size: 270081 Color: 0

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 427605 Color: 3
Size: 305306 Color: 0
Size: 267090 Color: 4

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 427740 Color: 0
Size: 298575 Color: 4
Size: 273686 Color: 3

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 473685 Color: 4
Size: 266856 Color: 1
Size: 259460 Color: 0

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 367468 Color: 3
Size: 328455 Color: 3
Size: 304078 Color: 4

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 427981 Color: 3
Size: 307956 Color: 0
Size: 264064 Color: 4

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 428095 Color: 2
Size: 294043 Color: 3
Size: 277863 Color: 0

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 428106 Color: 3
Size: 293610 Color: 3
Size: 278285 Color: 2

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 428179 Color: 4
Size: 286576 Color: 0
Size: 285246 Color: 3

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 428186 Color: 1
Size: 289377 Color: 3
Size: 282438 Color: 1

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 428325 Color: 4
Size: 317833 Color: 0
Size: 253843 Color: 1

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 428547 Color: 1
Size: 312602 Color: 4
Size: 258852 Color: 4

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 428551 Color: 4
Size: 315126 Color: 3
Size: 256324 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 428618 Color: 2
Size: 286322 Color: 3
Size: 285061 Color: 4

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 428757 Color: 0
Size: 309037 Color: 2
Size: 262207 Color: 2

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 428844 Color: 0
Size: 320738 Color: 2
Size: 250419 Color: 3

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 428751 Color: 1
Size: 318592 Color: 4
Size: 252658 Color: 2

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 428913 Color: 2
Size: 312580 Color: 4
Size: 258508 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 428922 Color: 0
Size: 312896 Color: 2
Size: 258183 Color: 2

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 428934 Color: 3
Size: 306919 Color: 2
Size: 264148 Color: 2

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 429140 Color: 0
Size: 305370 Color: 4
Size: 265491 Color: 3

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 428942 Color: 4
Size: 312118 Color: 1
Size: 258941 Color: 1

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 429222 Color: 0
Size: 293260 Color: 4
Size: 277519 Color: 2

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 429393 Color: 0
Size: 319844 Color: 4
Size: 250764 Color: 3

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 429401 Color: 3
Size: 299967 Color: 0
Size: 270633 Color: 4

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 429447 Color: 1
Size: 307158 Color: 3
Size: 263396 Color: 2

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 429470 Color: 1
Size: 286502 Color: 4
Size: 284029 Color: 1

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 429491 Color: 1
Size: 305505 Color: 4
Size: 265005 Color: 0

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 429650 Color: 4
Size: 294641 Color: 1
Size: 275710 Color: 2

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 429721 Color: 1
Size: 305673 Color: 3
Size: 264607 Color: 4

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 429722 Color: 1
Size: 307741 Color: 1
Size: 262538 Color: 4

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 429777 Color: 2
Size: 288977 Color: 1
Size: 281247 Color: 3

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 429783 Color: 1
Size: 292694 Color: 2
Size: 277524 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 429794 Color: 2
Size: 287633 Color: 1
Size: 282574 Color: 4

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 429901 Color: 2
Size: 319737 Color: 1
Size: 250363 Color: 0

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 430255 Color: 2
Size: 288571 Color: 2
Size: 281175 Color: 0

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 430288 Color: 0
Size: 292857 Color: 1
Size: 276856 Color: 4

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 430293 Color: 4
Size: 297912 Color: 2
Size: 271796 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 430628 Color: 1
Size: 290102 Color: 1
Size: 279271 Color: 3

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 430636 Color: 4
Size: 287558 Color: 2
Size: 281807 Color: 0

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 430683 Color: 4
Size: 304833 Color: 4
Size: 264485 Color: 1

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 430703 Color: 0
Size: 289980 Color: 1
Size: 279318 Color: 4

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 431228 Color: 3
Size: 290027 Color: 2
Size: 278746 Color: 2

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 431274 Color: 3
Size: 304467 Color: 2
Size: 264260 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 431513 Color: 0
Size: 312721 Color: 3
Size: 255767 Color: 3

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 431646 Color: 3
Size: 312715 Color: 1
Size: 255640 Color: 0

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 431770 Color: 0
Size: 313007 Color: 4
Size: 255224 Color: 3

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 431941 Color: 0
Size: 296366 Color: 4
Size: 271694 Color: 3

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 431838 Color: 4
Size: 295292 Color: 1
Size: 272871 Color: 3

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 432095 Color: 4
Size: 285960 Color: 3
Size: 281946 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 432173 Color: 1
Size: 287961 Color: 4
Size: 279867 Color: 4

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 432248 Color: 0
Size: 293789 Color: 2
Size: 273964 Color: 1

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 432397 Color: 0
Size: 289522 Color: 1
Size: 278082 Color: 4

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 432447 Color: 2
Size: 301640 Color: 4
Size: 265914 Color: 1

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 432450 Color: 3
Size: 307305 Color: 2
Size: 260246 Color: 2

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 432731 Color: 0
Size: 287440 Color: 3
Size: 279830 Color: 2

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 432763 Color: 0
Size: 284091 Color: 1
Size: 283147 Color: 2

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 432789 Color: 0
Size: 306315 Color: 2
Size: 260897 Color: 4

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 432922 Color: 0
Size: 295298 Color: 4
Size: 271781 Color: 4

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 432949 Color: 3
Size: 294989 Color: 4
Size: 272063 Color: 1

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 433019 Color: 3
Size: 308796 Color: 1
Size: 258186 Color: 0

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 433038 Color: 2
Size: 289670 Color: 1
Size: 277293 Color: 4

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 433211 Color: 2
Size: 305947 Color: 4
Size: 260843 Color: 0

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 433344 Color: 4
Size: 284535 Color: 2
Size: 282122 Color: 4

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 433475 Color: 3
Size: 312273 Color: 0
Size: 254253 Color: 3

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 433585 Color: 3
Size: 307473 Color: 0
Size: 258943 Color: 1

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 433602 Color: 0
Size: 308543 Color: 1
Size: 257856 Color: 2

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 463288 Color: 1
Size: 283331 Color: 3
Size: 253382 Color: 4

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 433841 Color: 2
Size: 305766 Color: 0
Size: 260394 Color: 1

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 433974 Color: 3
Size: 315068 Color: 4
Size: 250959 Color: 4

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 434112 Color: 1
Size: 309799 Color: 3
Size: 256090 Color: 3

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 434121 Color: 4
Size: 308932 Color: 0
Size: 256948 Color: 3

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 434148 Color: 1
Size: 286039 Color: 2
Size: 279814 Color: 2

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 434183 Color: 1
Size: 301187 Color: 3
Size: 264631 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 434294 Color: 0
Size: 294038 Color: 4
Size: 271669 Color: 2

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 434240 Color: 1
Size: 294666 Color: 3
Size: 271095 Color: 1

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 434308 Color: 4
Size: 297301 Color: 3
Size: 268392 Color: 3

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 434310 Color: 0
Size: 306734 Color: 4
Size: 258957 Color: 3

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 434371 Color: 4
Size: 312972 Color: 0
Size: 252658 Color: 3

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 434564 Color: 2
Size: 301629 Color: 3
Size: 263808 Color: 0

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 434786 Color: 0
Size: 288150 Color: 1
Size: 277065 Color: 2

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 434794 Color: 3
Size: 305256 Color: 0
Size: 259951 Color: 1

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 434804 Color: 3
Size: 296510 Color: 4
Size: 268687 Color: 3

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 434874 Color: 1
Size: 296339 Color: 2
Size: 268788 Color: 0

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 434961 Color: 3
Size: 314611 Color: 2
Size: 250429 Color: 3

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 434991 Color: 2
Size: 289195 Color: 2
Size: 275815 Color: 0

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 435064 Color: 1
Size: 302978 Color: 4
Size: 261959 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 435174 Color: 2
Size: 305014 Color: 1
Size: 259813 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 435206 Color: 1
Size: 302558 Color: 0
Size: 262237 Color: 4

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 435220 Color: 3
Size: 299778 Color: 1
Size: 265003 Color: 3

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 435609 Color: 2
Size: 297207 Color: 1
Size: 267185 Color: 3

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 435660 Color: 1
Size: 291218 Color: 4
Size: 273123 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 435774 Color: 0
Size: 300801 Color: 3
Size: 263426 Color: 4

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 435879 Color: 4
Size: 306811 Color: 0
Size: 257311 Color: 1

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 435973 Color: 3
Size: 284053 Color: 4
Size: 279975 Color: 0

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 435977 Color: 4
Size: 287006 Color: 3
Size: 277018 Color: 1

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 435982 Color: 4
Size: 302617 Color: 0
Size: 261402 Color: 2

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 436322 Color: 0
Size: 303937 Color: 4
Size: 259742 Color: 2

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 436168 Color: 4
Size: 301130 Color: 1
Size: 262703 Color: 3

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 436392 Color: 4
Size: 307543 Color: 1
Size: 256066 Color: 0

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 436751 Color: 4
Size: 287388 Color: 2
Size: 275862 Color: 1

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 436881 Color: 1
Size: 285415 Color: 0
Size: 277705 Color: 4

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 436903 Color: 3
Size: 289224 Color: 4
Size: 273874 Color: 1

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 436939 Color: 2
Size: 304610 Color: 3
Size: 258452 Color: 0

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 437005 Color: 4
Size: 309875 Color: 4
Size: 253121 Color: 1

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 420193 Color: 1
Size: 306417 Color: 0
Size: 273391 Color: 1

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 437188 Color: 0
Size: 308007 Color: 1
Size: 254806 Color: 4

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 437239 Color: 4
Size: 298425 Color: 0
Size: 264337 Color: 3

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 437240 Color: 3
Size: 284889 Color: 2
Size: 277872 Color: 3

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 437311 Color: 3
Size: 309893 Color: 0
Size: 252797 Color: 3

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 437384 Color: 0
Size: 281576 Color: 1
Size: 281041 Color: 2

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 437470 Color: 4
Size: 281670 Color: 0
Size: 280861 Color: 2

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 437486 Color: 1
Size: 292160 Color: 2
Size: 270355 Color: 1

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 437677 Color: 0
Size: 294432 Color: 4
Size: 267892 Color: 3

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 437578 Color: 3
Size: 281383 Color: 3
Size: 281040 Color: 2

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 437734 Color: 3
Size: 297336 Color: 2
Size: 264931 Color: 3

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 437808 Color: 1
Size: 282832 Color: 2
Size: 279361 Color: 4

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 437811 Color: 4
Size: 296940 Color: 1
Size: 265250 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 437805 Color: 0
Size: 282185 Color: 3
Size: 280011 Color: 1

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 437842 Color: 3
Size: 284070 Color: 0
Size: 278089 Color: 4

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 438264 Color: 0
Size: 301462 Color: 3
Size: 260275 Color: 4

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 438151 Color: 4
Size: 291710 Color: 3
Size: 270140 Color: 2

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 438354 Color: 1
Size: 306314 Color: 3
Size: 255333 Color: 0

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 438365 Color: 1
Size: 291817 Color: 2
Size: 269819 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 438508 Color: 2
Size: 293863 Color: 4
Size: 267630 Color: 1

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 438853 Color: 4
Size: 301879 Color: 3
Size: 259269 Color: 1

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 438949 Color: 0
Size: 299091 Color: 3
Size: 261961 Color: 4

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 439037 Color: 3
Size: 289257 Color: 3
Size: 271707 Color: 1

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 439063 Color: 4
Size: 298029 Color: 4
Size: 262909 Color: 3

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 439210 Color: 4
Size: 296604 Color: 0
Size: 264187 Color: 1

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 439355 Color: 0
Size: 299503 Color: 1
Size: 261143 Color: 3

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 439515 Color: 0
Size: 293611 Color: 2
Size: 266875 Color: 1

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 439670 Color: 4
Size: 280317 Color: 0
Size: 280014 Color: 2

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 439696 Color: 3
Size: 284495 Color: 1
Size: 275810 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 439760 Color: 4
Size: 306329 Color: 4
Size: 253912 Color: 2

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 439859 Color: 1
Size: 307748 Color: 0
Size: 252394 Color: 3

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 439948 Color: 2
Size: 300399 Color: 4
Size: 259654 Color: 0

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 440008 Color: 2
Size: 294537 Color: 1
Size: 265456 Color: 4

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 440068 Color: 0
Size: 301961 Color: 3
Size: 257972 Color: 3

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 440217 Color: 1
Size: 299576 Color: 2
Size: 260208 Color: 3

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 440482 Color: 2
Size: 292923 Color: 4
Size: 266596 Color: 3

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 440670 Color: 1
Size: 285769 Color: 2
Size: 273562 Color: 3

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 440672 Color: 3
Size: 290613 Color: 0
Size: 268716 Color: 4

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 440687 Color: 2
Size: 298369 Color: 3
Size: 260945 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 440793 Color: 1
Size: 291488 Color: 4
Size: 267720 Color: 3

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 441239 Color: 4
Size: 307407 Color: 0
Size: 251355 Color: 1

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 441239 Color: 1
Size: 295249 Color: 2
Size: 263513 Color: 3

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 441311 Color: 4
Size: 280142 Color: 0
Size: 278548 Color: 1

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 441389 Color: 0
Size: 289424 Color: 1
Size: 269188 Color: 4

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 441442 Color: 1
Size: 294750 Color: 3
Size: 263809 Color: 0

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 441485 Color: 4
Size: 281269 Color: 2
Size: 277247 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 441557 Color: 0
Size: 305276 Color: 1
Size: 253168 Color: 3

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 441642 Color: 0
Size: 286307 Color: 2
Size: 272052 Color: 1

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 441647 Color: 0
Size: 282909 Color: 4
Size: 275445 Color: 2

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 441658 Color: 0
Size: 299597 Color: 1
Size: 258746 Color: 3

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 441816 Color: 1
Size: 296395 Color: 0
Size: 261790 Color: 3

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 441911 Color: 4
Size: 280256 Color: 3
Size: 277834 Color: 1

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 442110 Color: 4
Size: 293207 Color: 2
Size: 264684 Color: 4

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 442116 Color: 3
Size: 305460 Color: 3
Size: 252425 Color: 0

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 442129 Color: 4
Size: 288348 Color: 0
Size: 269524 Color: 3

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 442220 Color: 1
Size: 307424 Color: 2
Size: 250357 Color: 4

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 442334 Color: 1
Size: 282738 Color: 4
Size: 274929 Color: 1

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 442523 Color: 3
Size: 284141 Color: 2
Size: 273337 Color: 4

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 442720 Color: 4
Size: 284799 Color: 3
Size: 272482 Color: 0

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 442869 Color: 2
Size: 281025 Color: 1
Size: 276107 Color: 4

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 442955 Color: 1
Size: 304532 Color: 2
Size: 252514 Color: 4

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 443006 Color: 2
Size: 284082 Color: 0
Size: 272913 Color: 1

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 443125 Color: 2
Size: 288968 Color: 4
Size: 267908 Color: 1

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 443128 Color: 2
Size: 300823 Color: 0
Size: 256050 Color: 4

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 443239 Color: 0
Size: 302248 Color: 2
Size: 254514 Color: 0

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 443617 Color: 2
Size: 283050 Color: 4
Size: 273334 Color: 2

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 443744 Color: 2
Size: 289365 Color: 1
Size: 266892 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 443776 Color: 4
Size: 303805 Color: 2
Size: 252420 Color: 1

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 443806 Color: 2
Size: 278515 Color: 3
Size: 277680 Color: 0

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 443892 Color: 0
Size: 294333 Color: 1
Size: 261776 Color: 1

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 444004 Color: 4
Size: 303480 Color: 2
Size: 252517 Color: 1

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 444223 Color: 2
Size: 285618 Color: 3
Size: 270160 Color: 3

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 444544 Color: 0
Size: 281317 Color: 2
Size: 274140 Color: 2

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 444842 Color: 1
Size: 279287 Color: 3
Size: 275872 Color: 0

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 444847 Color: 3
Size: 300287 Color: 2
Size: 254867 Color: 1

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 444896 Color: 2
Size: 299388 Color: 1
Size: 255717 Color: 3

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 445004 Color: 4
Size: 301474 Color: 1
Size: 253523 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 445063 Color: 0
Size: 286826 Color: 1
Size: 268112 Color: 3

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 445060 Color: 2
Size: 302341 Color: 0
Size: 252600 Color: 2

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 445279 Color: 0
Size: 298035 Color: 2
Size: 256687 Color: 3

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 445423 Color: 2
Size: 294858 Color: 3
Size: 259720 Color: 1

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 445445 Color: 4
Size: 283937 Color: 0
Size: 270619 Color: 2

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 445582 Color: 4
Size: 286273 Color: 4
Size: 268146 Color: 0

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 445622 Color: 1
Size: 279559 Color: 4
Size: 274820 Color: 4

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 445670 Color: 1
Size: 300065 Color: 2
Size: 254266 Color: 2

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 445871 Color: 1
Size: 298901 Color: 3
Size: 255229 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 445936 Color: 3
Size: 303980 Color: 2
Size: 250085 Color: 4

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 446128 Color: 4
Size: 284392 Color: 4
Size: 269481 Color: 0

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 446164 Color: 4
Size: 285308 Color: 2
Size: 268529 Color: 2

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 446165 Color: 1
Size: 296365 Color: 0
Size: 257471 Color: 4

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 446328 Color: 1
Size: 285955 Color: 2
Size: 267718 Color: 4

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 446498 Color: 1
Size: 277627 Color: 3
Size: 275876 Color: 1

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 446663 Color: 4
Size: 288315 Color: 3
Size: 265023 Color: 3

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 446908 Color: 4
Size: 278868 Color: 0
Size: 274225 Color: 1

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 447015 Color: 3
Size: 276933 Color: 4
Size: 276053 Color: 1

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 446884 Color: 1
Size: 294695 Color: 0
Size: 258422 Color: 2

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 447141 Color: 2
Size: 292568 Color: 1
Size: 260292 Color: 2

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 447198 Color: 2
Size: 301930 Color: 0
Size: 250873 Color: 3

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 447200 Color: 2
Size: 288240 Color: 4
Size: 264561 Color: 0

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 447292 Color: 4
Size: 281446 Color: 3
Size: 271263 Color: 2

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 447355 Color: 0
Size: 287432 Color: 1
Size: 265214 Color: 3

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 447362 Color: 1
Size: 288804 Color: 0
Size: 263835 Color: 3

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 447379 Color: 4
Size: 296264 Color: 1
Size: 256358 Color: 3

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 447568 Color: 0
Size: 294596 Color: 3
Size: 257837 Color: 2

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 447596 Color: 0
Size: 291886 Color: 4
Size: 260519 Color: 3

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 447501 Color: 1
Size: 295904 Color: 3
Size: 256596 Color: 4

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 447757 Color: 4
Size: 281305 Color: 1
Size: 270939 Color: 0

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 447768 Color: 2
Size: 297626 Color: 3
Size: 254607 Color: 0

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 447792 Color: 4
Size: 299893 Color: 4
Size: 252316 Color: 3

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 447971 Color: 0
Size: 277262 Color: 4
Size: 274768 Color: 2

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 448086 Color: 4
Size: 293084 Color: 3
Size: 258831 Color: 0

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 448089 Color: 3
Size: 299887 Color: 4
Size: 252025 Color: 4

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 448300 Color: 0
Size: 286974 Color: 3
Size: 264727 Color: 2

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 448469 Color: 2
Size: 283443 Color: 0
Size: 268089 Color: 3

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 448483 Color: 1
Size: 279039 Color: 4
Size: 272479 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 448732 Color: 1
Size: 283368 Color: 4
Size: 267901 Color: 4

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 448925 Color: 4
Size: 278333 Color: 0
Size: 272743 Color: 2

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 448971 Color: 0
Size: 295715 Color: 2
Size: 255315 Color: 1

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 449236 Color: 2
Size: 275422 Color: 0
Size: 275343 Color: 4

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 449312 Color: 4
Size: 282988 Color: 3
Size: 267701 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 449468 Color: 0
Size: 281956 Color: 4
Size: 268577 Color: 3

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 449719 Color: 3
Size: 292383 Color: 4
Size: 257899 Color: 1

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 449869 Color: 2
Size: 284886 Color: 0
Size: 265246 Color: 4

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 449953 Color: 3
Size: 286487 Color: 1
Size: 263561 Color: 2

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 450090 Color: 3
Size: 296779 Color: 1
Size: 253132 Color: 4

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 450171 Color: 0
Size: 294406 Color: 4
Size: 255424 Color: 1

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 450219 Color: 2
Size: 284604 Color: 0
Size: 265178 Color: 1

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 450377 Color: 1
Size: 276975 Color: 3
Size: 272649 Color: 0

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 450463 Color: 4
Size: 289619 Color: 1
Size: 259919 Color: 4

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 450502 Color: 2
Size: 299065 Color: 4
Size: 250434 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 450552 Color: 0
Size: 296474 Color: 1
Size: 252975 Color: 3

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 450905 Color: 0
Size: 290770 Color: 4
Size: 258326 Color: 1

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 450927 Color: 1
Size: 294486 Color: 2
Size: 254588 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 451042 Color: 0
Size: 277077 Color: 3
Size: 271882 Color: 4

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 451067 Color: 4
Size: 295586 Color: 1
Size: 253348 Color: 3

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 451344 Color: 1
Size: 290286 Color: 4
Size: 258371 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 451332 Color: 0
Size: 276806 Color: 1
Size: 271863 Color: 2

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 451450 Color: 1
Size: 296235 Color: 3
Size: 252316 Color: 4

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 451609 Color: 2
Size: 296109 Color: 3
Size: 252283 Color: 1

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 451740 Color: 2
Size: 279548 Color: 0
Size: 268713 Color: 1

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 451750 Color: 4
Size: 296566 Color: 1
Size: 251685 Color: 4

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 451875 Color: 1
Size: 278339 Color: 0
Size: 269787 Color: 4

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 452058 Color: 4
Size: 293969 Color: 4
Size: 253974 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 452100 Color: 2
Size: 288771 Color: 3
Size: 259130 Color: 3

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 452233 Color: 3
Size: 291763 Color: 2
Size: 256005 Color: 2

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 452234 Color: 3
Size: 281792 Color: 4
Size: 265975 Color: 1

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 452246 Color: 1
Size: 293188 Color: 3
Size: 254567 Color: 2

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 452663 Color: 0
Size: 285270 Color: 3
Size: 262068 Color: 3

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 452766 Color: 4
Size: 288893 Color: 1
Size: 258342 Color: 1

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 452834 Color: 2
Size: 294531 Color: 1
Size: 252636 Color: 0

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 452867 Color: 4
Size: 295365 Color: 2
Size: 251769 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 452877 Color: 3
Size: 290306 Color: 0
Size: 256818 Color: 3

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 452905 Color: 4
Size: 292049 Color: 1
Size: 255047 Color: 3

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 453158 Color: 0
Size: 280658 Color: 1
Size: 266185 Color: 2

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 453275 Color: 2
Size: 277627 Color: 1
Size: 269099 Color: 3

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 453306 Color: 3
Size: 287249 Color: 3
Size: 259446 Color: 2

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 453442 Color: 4
Size: 285339 Color: 0
Size: 261220 Color: 1

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 378583 Color: 4
Size: 355106 Color: 3
Size: 266312 Color: 1

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 356943 Color: 1
Size: 336160 Color: 1
Size: 306898 Color: 3

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 453615 Color: 1
Size: 295661 Color: 4
Size: 250725 Color: 2

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 453692 Color: 1
Size: 282226 Color: 2
Size: 264083 Color: 3

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 453923 Color: 4
Size: 274978 Color: 3
Size: 271100 Color: 3

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 454181 Color: 4
Size: 292590 Color: 3
Size: 253230 Color: 1

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 454333 Color: 4
Size: 276937 Color: 3
Size: 268731 Color: 2

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 454335 Color: 4
Size: 285029 Color: 3
Size: 260637 Color: 4

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 454421 Color: 4
Size: 289752 Color: 1
Size: 255828 Color: 3

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 454612 Color: 3
Size: 283316 Color: 1
Size: 262073 Color: 2

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 454644 Color: 2
Size: 285393 Color: 1
Size: 259964 Color: 2

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 454695 Color: 1
Size: 278327 Color: 2
Size: 266979 Color: 4

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 454721 Color: 4
Size: 272908 Color: 1
Size: 272372 Color: 0

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 454910 Color: 2
Size: 286463 Color: 4
Size: 258628 Color: 2

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 454916 Color: 4
Size: 290235 Color: 4
Size: 254850 Color: 1

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 455067 Color: 0
Size: 275795 Color: 4
Size: 269139 Color: 2

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 455082 Color: 2
Size: 278563 Color: 3
Size: 266356 Color: 2

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 455197 Color: 3
Size: 294363 Color: 4
Size: 250441 Color: 1

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 455244 Color: 3
Size: 290202 Color: 4
Size: 254555 Color: 1

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 455256 Color: 4
Size: 280399 Color: 0
Size: 264346 Color: 3

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 455427 Color: 2
Size: 282948 Color: 1
Size: 261626 Color: 4

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 455754 Color: 4
Size: 278380 Color: 1
Size: 265867 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 455878 Color: 2
Size: 286679 Color: 3
Size: 257444 Color: 1

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 456162 Color: 1
Size: 292945 Color: 3
Size: 250894 Color: 2

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 456198 Color: 0
Size: 278463 Color: 0
Size: 265340 Color: 4

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 456595 Color: 1
Size: 288240 Color: 1
Size: 255166 Color: 3

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 456552 Color: 4
Size: 291415 Color: 3
Size: 252034 Color: 1

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 456698 Color: 1
Size: 283981 Color: 2
Size: 259322 Color: 3

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 456720 Color: 3
Size: 275145 Color: 2
Size: 268136 Color: 1

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 456887 Color: 0
Size: 271582 Color: 1
Size: 271532 Color: 4

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 456963 Color: 2
Size: 282175 Color: 4
Size: 260863 Color: 2

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 457028 Color: 0
Size: 278182 Color: 4
Size: 264791 Color: 4

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 457083 Color: 0
Size: 279553 Color: 0
Size: 263365 Color: 1

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 457249 Color: 3
Size: 277330 Color: 0
Size: 265422 Color: 0

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 457250 Color: 4
Size: 279882 Color: 1
Size: 262869 Color: 3

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 457292 Color: 0
Size: 290719 Color: 2
Size: 251990 Color: 3

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 457435 Color: 3
Size: 282140 Color: 1
Size: 260426 Color: 2

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 457469 Color: 0
Size: 279236 Color: 2
Size: 263296 Color: 3

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 457511 Color: 1
Size: 285209 Color: 0
Size: 257281 Color: 4

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 457720 Color: 4
Size: 291576 Color: 3
Size: 250705 Color: 4

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 457794 Color: 3
Size: 286979 Color: 1
Size: 255228 Color: 4

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 372698 Color: 2
Size: 350553 Color: 1
Size: 276750 Color: 3

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 458211 Color: 2
Size: 275979 Color: 1
Size: 265811 Color: 3

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 458372 Color: 0
Size: 284916 Color: 4
Size: 256713 Color: 4

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 458611 Color: 1
Size: 282100 Color: 3
Size: 259290 Color: 0

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 458595 Color: 4
Size: 276398 Color: 0
Size: 265008 Color: 2

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 458606 Color: 4
Size: 273121 Color: 4
Size: 268274 Color: 3

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 458803 Color: 3
Size: 283707 Color: 4
Size: 257491 Color: 3

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 459271 Color: 2
Size: 285835 Color: 1
Size: 254895 Color: 0

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 459335 Color: 3
Size: 285396 Color: 1
Size: 255270 Color: 0

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 459396 Color: 2
Size: 279289 Color: 3
Size: 261316 Color: 3

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 459398 Color: 0
Size: 282704 Color: 4
Size: 257899 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 459482 Color: 2
Size: 277064 Color: 3
Size: 263455 Color: 1

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 459512 Color: 2
Size: 272105 Color: 1
Size: 268384 Color: 4

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 459548 Color: 0
Size: 276271 Color: 4
Size: 264182 Color: 3

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 459590 Color: 0
Size: 272946 Color: 3
Size: 267465 Color: 1

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 459717 Color: 1
Size: 284895 Color: 3
Size: 255389 Color: 0

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 459751 Color: 0
Size: 285533 Color: 3
Size: 254717 Color: 2

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 459818 Color: 3
Size: 278068 Color: 4
Size: 262115 Color: 0

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 460030 Color: 0
Size: 288259 Color: 4
Size: 251712 Color: 2

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 460055 Color: 0
Size: 284840 Color: 4
Size: 255106 Color: 1

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 460120 Color: 4
Size: 276248 Color: 2
Size: 263633 Color: 0

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 460132 Color: 1
Size: 278438 Color: 2
Size: 261431 Color: 2

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 444407 Color: 2
Size: 302051 Color: 3
Size: 253543 Color: 4

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 460383 Color: 0
Size: 282388 Color: 1
Size: 257230 Color: 4

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 460383 Color: 3
Size: 274599 Color: 2
Size: 265019 Color: 1

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 460515 Color: 0
Size: 274072 Color: 1
Size: 265414 Color: 2

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 460599 Color: 3
Size: 281647 Color: 2
Size: 257755 Color: 2

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 460695 Color: 2
Size: 274241 Color: 0
Size: 265065 Color: 1

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 460800 Color: 0
Size: 276286 Color: 1
Size: 262915 Color: 2

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 460870 Color: 2
Size: 287639 Color: 4
Size: 251492 Color: 0

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 460875 Color: 4
Size: 279414 Color: 1
Size: 259712 Color: 1

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 460970 Color: 3
Size: 288407 Color: 3
Size: 250624 Color: 1

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 461012 Color: 2
Size: 273392 Color: 0
Size: 265597 Color: 2

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 461073 Color: 3
Size: 288066 Color: 0
Size: 250862 Color: 2

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 461074 Color: 3
Size: 271777 Color: 3
Size: 267150 Color: 1

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 461295 Color: 0
Size: 287404 Color: 1
Size: 251302 Color: 2

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 461287 Color: 3
Size: 274076 Color: 2
Size: 264638 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 461654 Color: 0
Size: 283036 Color: 4
Size: 255311 Color: 2

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 461729 Color: 3
Size: 273899 Color: 1
Size: 264373 Color: 0

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 461925 Color: 2
Size: 281272 Color: 3
Size: 256804 Color: 3

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 461948 Color: 2
Size: 274318 Color: 3
Size: 263735 Color: 0

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 462039 Color: 0
Size: 279293 Color: 2
Size: 258669 Color: 1

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 462075 Color: 0
Size: 283915 Color: 0
Size: 254011 Color: 1

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 462029 Color: 1
Size: 275024 Color: 2
Size: 262948 Color: 4

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 462254 Color: 4
Size: 277818 Color: 0
Size: 259929 Color: 3

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 462371 Color: 0
Size: 284139 Color: 1
Size: 253491 Color: 4

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 462287 Color: 3
Size: 274010 Color: 4
Size: 263704 Color: 4

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 462676 Color: 4
Size: 275349 Color: 1
Size: 261976 Color: 2

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 462811 Color: 1
Size: 284112 Color: 2
Size: 253078 Color: 0

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 462777 Color: 4
Size: 275033 Color: 3
Size: 262191 Color: 0

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 463001 Color: 0
Size: 273949 Color: 1
Size: 263051 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 463063 Color: 4
Size: 286336 Color: 2
Size: 250602 Color: 1

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 463187 Color: 4
Size: 281459 Color: 0
Size: 255355 Color: 4

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 493585 Color: 4
Size: 254865 Color: 0
Size: 251551 Color: 3

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 463247 Color: 3
Size: 274360 Color: 3
Size: 262394 Color: 0

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 463298 Color: 1
Size: 275463 Color: 4
Size: 261240 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 463361 Color: 4
Size: 273779 Color: 2
Size: 262861 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 463371 Color: 2
Size: 284366 Color: 1
Size: 252264 Color: 0

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 463674 Color: 4
Size: 270695 Color: 2
Size: 265632 Color: 0

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 463811 Color: 2
Size: 283331 Color: 3
Size: 252859 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 463974 Color: 4
Size: 281992 Color: 3
Size: 254035 Color: 3

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 464208 Color: 1
Size: 282476 Color: 0
Size: 253317 Color: 3

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 464234 Color: 4
Size: 285538 Color: 3
Size: 250229 Color: 3

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 464290 Color: 3
Size: 282124 Color: 2
Size: 253587 Color: 2

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 464489 Color: 4
Size: 283424 Color: 1
Size: 252088 Color: 0

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 464502 Color: 1
Size: 270106 Color: 4
Size: 265393 Color: 1

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 464526 Color: 4
Size: 276720 Color: 0
Size: 258755 Color: 2

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 464700 Color: 0
Size: 274962 Color: 4
Size: 260339 Color: 4

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 464721 Color: 1
Size: 271049 Color: 3
Size: 264231 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 464821 Color: 2
Size: 271717 Color: 2
Size: 263463 Color: 0

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 464980 Color: 4
Size: 276292 Color: 0
Size: 258729 Color: 1

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 465157 Color: 3
Size: 278409 Color: 0
Size: 256435 Color: 2

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 465189 Color: 4
Size: 272259 Color: 3
Size: 262553 Color: 2

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 465542 Color: 2
Size: 282763 Color: 2
Size: 251696 Color: 0

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 465607 Color: 4
Size: 277882 Color: 2
Size: 256512 Color: 0

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 465727 Color: 2
Size: 268032 Color: 1
Size: 266242 Color: 4

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 466188 Color: 2
Size: 281348 Color: 1
Size: 252465 Color: 1

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 466203 Color: 2
Size: 281358 Color: 0
Size: 252440 Color: 1

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 466260 Color: 2
Size: 283009 Color: 0
Size: 250732 Color: 2

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 466290 Color: 2
Size: 277164 Color: 2
Size: 256547 Color: 4

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 466429 Color: 0
Size: 269696 Color: 1
Size: 263876 Color: 4

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 466668 Color: 3
Size: 280654 Color: 3
Size: 252679 Color: 2

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 466821 Color: 0
Size: 277390 Color: 2
Size: 255790 Color: 1

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 466839 Color: 1
Size: 277356 Color: 3
Size: 255806 Color: 3

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 466984 Color: 3
Size: 271355 Color: 0
Size: 261662 Color: 4

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 467065 Color: 2
Size: 267335 Color: 2
Size: 265601 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 467114 Color: 3
Size: 273592 Color: 0
Size: 259295 Color: 1

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 467119 Color: 2
Size: 282167 Color: 2
Size: 250715 Color: 0

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 467210 Color: 2
Size: 270774 Color: 1
Size: 262017 Color: 4

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 467223 Color: 1
Size: 280143 Color: 0
Size: 252635 Color: 3

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 467407 Color: 3
Size: 278222 Color: 3
Size: 254372 Color: 1

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 467794 Color: 4
Size: 268633 Color: 3
Size: 263574 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 468038 Color: 1
Size: 271267 Color: 4
Size: 260696 Color: 0

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 468131 Color: 0
Size: 269721 Color: 1
Size: 262149 Color: 3

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 468247 Color: 3
Size: 268807 Color: 0
Size: 262947 Color: 1

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 468390 Color: 3
Size: 274702 Color: 1
Size: 256909 Color: 4

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 468460 Color: 4
Size: 272803 Color: 0
Size: 258738 Color: 2

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 468479 Color: 2
Size: 273625 Color: 4
Size: 257897 Color: 3

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 468553 Color: 0
Size: 276908 Color: 3
Size: 254540 Color: 1

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 468733 Color: 4
Size: 275932 Color: 1
Size: 255336 Color: 4

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 468825 Color: 1
Size: 266371 Color: 4
Size: 264805 Color: 2

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 468849 Color: 1
Size: 274825 Color: 0
Size: 256327 Color: 3

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 468931 Color: 1
Size: 267818 Color: 3
Size: 263252 Color: 0

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 469185 Color: 1
Size: 275300 Color: 0
Size: 255516 Color: 4

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 469226 Color: 2
Size: 266728 Color: 0
Size: 264047 Color: 1

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 469403 Color: 0
Size: 276137 Color: 3
Size: 254461 Color: 3

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 469409 Color: 3
Size: 278345 Color: 0
Size: 252247 Color: 3

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 469504 Color: 0
Size: 276929 Color: 2
Size: 253568 Color: 4

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 469444 Color: 1
Size: 266345 Color: 4
Size: 264212 Color: 3

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 469524 Color: 3
Size: 272714 Color: 1
Size: 257763 Color: 0

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 470231 Color: 3
Size: 267455 Color: 0
Size: 262315 Color: 2

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 470243 Color: 2
Size: 265266 Color: 0
Size: 264492 Color: 4

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 470279 Color: 1
Size: 276468 Color: 4
Size: 253254 Color: 3

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 470398 Color: 0
Size: 274640 Color: 2
Size: 254963 Color: 2

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 470544 Color: 1
Size: 274907 Color: 3
Size: 254550 Color: 0

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 470556 Color: 4
Size: 266513 Color: 1
Size: 262932 Color: 4

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 470612 Color: 0
Size: 274999 Color: 1
Size: 254390 Color: 2

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 470558 Color: 2
Size: 277028 Color: 1
Size: 252415 Color: 4

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 470560 Color: 2
Size: 275628 Color: 4
Size: 253813 Color: 0

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 470640 Color: 3
Size: 269266 Color: 1
Size: 260095 Color: 0

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 470657 Color: 2
Size: 279014 Color: 4
Size: 250330 Color: 1

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 470732 Color: 3
Size: 277901 Color: 0
Size: 251368 Color: 1

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 470993 Color: 2
Size: 273367 Color: 0
Size: 255641 Color: 1

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 471125 Color: 1
Size: 273440 Color: 1
Size: 255436 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 471163 Color: 4
Size: 267730 Color: 2
Size: 261108 Color: 4

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 471218 Color: 1
Size: 269839 Color: 0
Size: 258944 Color: 3

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 473848 Color: 4
Size: 269933 Color: 3
Size: 256220 Color: 0

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 471322 Color: 4
Size: 273849 Color: 3
Size: 254830 Color: 2

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 471431 Color: 1
Size: 264774 Color: 3
Size: 263796 Color: 0

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 471456 Color: 1
Size: 264540 Color: 4
Size: 264005 Color: 4

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 471485 Color: 0
Size: 274377 Color: 1
Size: 254139 Color: 2

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 471511 Color: 4
Size: 274440 Color: 1
Size: 254050 Color: 4

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 471587 Color: 0
Size: 270206 Color: 3
Size: 258208 Color: 3

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 471705 Color: 2
Size: 275750 Color: 2
Size: 252546 Color: 1

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 472145 Color: 0
Size: 273654 Color: 2
Size: 254202 Color: 1

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 472548 Color: 4
Size: 264941 Color: 2
Size: 262512 Color: 3

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 472756 Color: 1
Size: 264892 Color: 2
Size: 262353 Color: 4

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 472827 Color: 0
Size: 275556 Color: 4
Size: 251618 Color: 1

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 472802 Color: 4
Size: 266812 Color: 3
Size: 260387 Color: 3

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 472843 Color: 2
Size: 275855 Color: 1
Size: 251303 Color: 1

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 472907 Color: 1
Size: 274441 Color: 3
Size: 252653 Color: 0

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 473175 Color: 1
Size: 272677 Color: 4
Size: 254149 Color: 0

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 437413 Color: 2
Size: 302159 Color: 4
Size: 260429 Color: 0

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 473697 Color: 4
Size: 273424 Color: 1
Size: 252880 Color: 3

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 473823 Color: 4
Size: 267339 Color: 2
Size: 258839 Color: 0

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 474155 Color: 3
Size: 273286 Color: 0
Size: 252560 Color: 2

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 474317 Color: 4
Size: 274024 Color: 0
Size: 251660 Color: 3

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 474446 Color: 1
Size: 265327 Color: 4
Size: 260228 Color: 0

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 474471 Color: 2
Size: 271994 Color: 3
Size: 253536 Color: 3

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 474727 Color: 3
Size: 274141 Color: 0
Size: 251133 Color: 2

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 474837 Color: 2
Size: 274453 Color: 0
Size: 250711 Color: 2

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 475186 Color: 4
Size: 274495 Color: 4
Size: 250320 Color: 0

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 475240 Color: 3
Size: 270188 Color: 1
Size: 254573 Color: 0

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 475274 Color: 0
Size: 266809 Color: 3
Size: 257918 Color: 1

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 475489 Color: 4
Size: 265180 Color: 0
Size: 259332 Color: 2

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 475662 Color: 0
Size: 268638 Color: 3
Size: 255701 Color: 3

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 475718 Color: 4
Size: 269664 Color: 1
Size: 254619 Color: 1

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 475758 Color: 1
Size: 266415 Color: 0
Size: 257828 Color: 1

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 475915 Color: 3
Size: 270535 Color: 0
Size: 253551 Color: 2

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 475939 Color: 2
Size: 272756 Color: 1
Size: 251306 Color: 1

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 476155 Color: 2
Size: 272239 Color: 3
Size: 251607 Color: 2

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 476214 Color: 4
Size: 271748 Color: 3
Size: 252039 Color: 0

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 476226 Color: 2
Size: 269689 Color: 1
Size: 254086 Color: 0

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 476339 Color: 0
Size: 269680 Color: 3
Size: 253982 Color: 1

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 476521 Color: 4
Size: 262505 Color: 0
Size: 260975 Color: 4

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 476523 Color: 3
Size: 269423 Color: 1
Size: 254055 Color: 3

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 476549 Color: 3
Size: 264764 Color: 1
Size: 258688 Color: 0

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 476571 Color: 0
Size: 265335 Color: 2
Size: 258095 Color: 1

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 476571 Color: 4
Size: 262322 Color: 3
Size: 261108 Color: 0

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 406600 Color: 2
Size: 321885 Color: 1
Size: 271516 Color: 0

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 476882 Color: 0
Size: 269646 Color: 4
Size: 253473 Color: 3

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 476953 Color: 0
Size: 265208 Color: 3
Size: 257840 Color: 1

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 476817 Color: 1
Size: 270542 Color: 4
Size: 252642 Color: 2

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 477312 Color: 4
Size: 263364 Color: 0
Size: 259325 Color: 4

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 477328 Color: 3
Size: 266646 Color: 2
Size: 256027 Color: 0

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 477344 Color: 1
Size: 262534 Color: 4
Size: 260123 Color: 2

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 477599 Color: 4
Size: 268005 Color: 0
Size: 254397 Color: 3

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 477664 Color: 2
Size: 268710 Color: 3
Size: 253627 Color: 0

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 477746 Color: 0
Size: 267754 Color: 3
Size: 254501 Color: 3

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 477895 Color: 4
Size: 268003 Color: 1
Size: 254103 Color: 0

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 478409 Color: 1
Size: 263424 Color: 2
Size: 258168 Color: 0

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 478485 Color: 4
Size: 265539 Color: 0
Size: 255977 Color: 1

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 478557 Color: 1
Size: 261957 Color: 3
Size: 259487 Color: 3

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 478740 Color: 3
Size: 265282 Color: 1
Size: 255979 Color: 3

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 478811 Color: 0
Size: 268661 Color: 3
Size: 252529 Color: 1

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 478848 Color: 3
Size: 263306 Color: 0
Size: 257847 Color: 2

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 478885 Color: 2
Size: 261176 Color: 0
Size: 259940 Color: 4

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 478963 Color: 1
Size: 267684 Color: 3
Size: 253354 Color: 4

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 479023 Color: 4
Size: 270764 Color: 0
Size: 250214 Color: 1

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 479057 Color: 2
Size: 270636 Color: 0
Size: 250308 Color: 4

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 479449 Color: 4
Size: 267860 Color: 1
Size: 252692 Color: 2

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 479475 Color: 1
Size: 263919 Color: 1
Size: 256607 Color: 0

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 479506 Color: 4
Size: 262173 Color: 3
Size: 258322 Color: 1

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 479534 Color: 1
Size: 260471 Color: 4
Size: 259996 Color: 1

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 479521 Color: 0
Size: 261878 Color: 2
Size: 258602 Color: 2

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 479707 Color: 3
Size: 267013 Color: 1
Size: 253281 Color: 1

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 479722 Color: 4
Size: 261447 Color: 3
Size: 258832 Color: 0

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 479794 Color: 4
Size: 268851 Color: 2
Size: 251356 Color: 1

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 480005 Color: 2
Size: 261398 Color: 1
Size: 258598 Color: 0

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 480015 Color: 4
Size: 263423 Color: 0
Size: 256563 Color: 4

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 480336 Color: 0
Size: 263922 Color: 4
Size: 255743 Color: 2

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 480311 Color: 4
Size: 261769 Color: 3
Size: 257921 Color: 1

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 480415 Color: 4
Size: 262987 Color: 3
Size: 256599 Color: 2

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 480584 Color: 2
Size: 262754 Color: 0
Size: 256663 Color: 1

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 480703 Color: 0
Size: 263342 Color: 4
Size: 255956 Color: 4

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 480598 Color: 4
Size: 259729 Color: 4
Size: 259674 Color: 1

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 480876 Color: 4
Size: 262138 Color: 1
Size: 256987 Color: 2

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 481009 Color: 4
Size: 262753 Color: 1
Size: 256239 Color: 2

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 481044 Color: 4
Size: 260985 Color: 2
Size: 257972 Color: 2

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 481117 Color: 3
Size: 261743 Color: 0
Size: 257141 Color: 2

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 481279 Color: 4
Size: 260528 Color: 2
Size: 258194 Color: 0

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 481460 Color: 1
Size: 266584 Color: 3
Size: 251957 Color: 0

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 481473 Color: 4
Size: 261379 Color: 3
Size: 257149 Color: 0

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 481759 Color: 3
Size: 263575 Color: 2
Size: 254667 Color: 4

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 481882 Color: 0
Size: 264469 Color: 4
Size: 253650 Color: 2

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 481812 Color: 3
Size: 263369 Color: 4
Size: 254820 Color: 2

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 481894 Color: 0
Size: 266222 Color: 4
Size: 251885 Color: 3

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 482173 Color: 2
Size: 265417 Color: 4
Size: 252411 Color: 0

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 482270 Color: 3
Size: 261378 Color: 1
Size: 256353 Color: 2

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 482403 Color: 4
Size: 259337 Color: 3
Size: 258261 Color: 0

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 482476 Color: 1
Size: 263253 Color: 3
Size: 254272 Color: 1

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 482657 Color: 0
Size: 265177 Color: 3
Size: 252167 Color: 2

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 482901 Color: 4
Size: 266911 Color: 3
Size: 250189 Color: 4

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 482979 Color: 1
Size: 263351 Color: 4
Size: 253671 Color: 2

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 483189 Color: 4
Size: 266207 Color: 0
Size: 250605 Color: 3

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 483323 Color: 4
Size: 264300 Color: 1
Size: 252378 Color: 2

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 483396 Color: 4
Size: 265032 Color: 0
Size: 251573 Color: 3

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 483506 Color: 3
Size: 258946 Color: 3
Size: 257549 Color: 1

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 483610 Color: 4
Size: 258870 Color: 1
Size: 257521 Color: 0

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 483633 Color: 4
Size: 265381 Color: 3
Size: 250987 Color: 3

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 484004 Color: 1
Size: 261484 Color: 0
Size: 254513 Color: 4

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 484108 Color: 4
Size: 265745 Color: 2
Size: 250148 Color: 0

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 484121 Color: 2
Size: 264117 Color: 1
Size: 251763 Color: 1

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 484257 Color: 2
Size: 260755 Color: 3
Size: 254989 Color: 2

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 484406 Color: 1
Size: 262388 Color: 1
Size: 253207 Color: 4

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 484835 Color: 0
Size: 264813 Color: 2
Size: 250353 Color: 2

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 484756 Color: 3
Size: 258956 Color: 4
Size: 256289 Color: 1

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 485360 Color: 4
Size: 259364 Color: 3
Size: 255277 Color: 1

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 485610 Color: 0
Size: 259917 Color: 3
Size: 254474 Color: 1

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 485650 Color: 4
Size: 264120 Color: 0
Size: 250231 Color: 4

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 485645 Color: 0
Size: 260875 Color: 2
Size: 253481 Color: 2

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 485817 Color: 1
Size: 258054 Color: 1
Size: 256130 Color: 3

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 485824 Color: 1
Size: 261980 Color: 2
Size: 252197 Color: 4

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 486327 Color: 4
Size: 259967 Color: 1
Size: 253707 Color: 4

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 486518 Color: 1
Size: 260602 Color: 2
Size: 252881 Color: 0

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 486550 Color: 3
Size: 262392 Color: 3
Size: 251059 Color: 2

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 486556 Color: 4
Size: 258391 Color: 0
Size: 255054 Color: 1

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 486752 Color: 3
Size: 256938 Color: 0
Size: 256311 Color: 2

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 487151 Color: 4
Size: 258746 Color: 1
Size: 254104 Color: 1

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 487189 Color: 2
Size: 262523 Color: 0
Size: 250289 Color: 3

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 487241 Color: 4
Size: 261916 Color: 2
Size: 250844 Color: 1

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 487339 Color: 0
Size: 260624 Color: 1
Size: 252038 Color: 1

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 487587 Color: 0
Size: 259982 Color: 1
Size: 252432 Color: 2

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 487536 Color: 4
Size: 261116 Color: 3
Size: 251349 Color: 4

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 487686 Color: 4
Size: 259704 Color: 1
Size: 252611 Color: 1

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 488411 Color: 3
Size: 258508 Color: 0
Size: 253082 Color: 4

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 488562 Color: 2
Size: 260167 Color: 4
Size: 251272 Color: 0

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 488625 Color: 4
Size: 257158 Color: 4
Size: 254218 Color: 3

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 488743 Color: 2
Size: 260674 Color: 1
Size: 250584 Color: 2

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 489063 Color: 4
Size: 257757 Color: 0
Size: 253181 Color: 3

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 489114 Color: 2
Size: 260094 Color: 3
Size: 250793 Color: 0

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 489153 Color: 1
Size: 259834 Color: 0
Size: 251014 Color: 4

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 489193 Color: 3
Size: 257536 Color: 3
Size: 253272 Color: 4

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 489277 Color: 0
Size: 258219 Color: 2
Size: 252505 Color: 4

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 489398 Color: 3
Size: 255705 Color: 0
Size: 254898 Color: 4

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 489633 Color: 2
Size: 257727 Color: 4
Size: 252641 Color: 4

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 489793 Color: 0
Size: 255858 Color: 3
Size: 254350 Color: 4

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 489703 Color: 3
Size: 257751 Color: 1
Size: 252547 Color: 4

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 489976 Color: 0
Size: 256537 Color: 3
Size: 253488 Color: 4

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 489993 Color: 1
Size: 258286 Color: 2
Size: 251722 Color: 0

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 490004 Color: 4
Size: 258222 Color: 2
Size: 251775 Color: 0

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 490206 Color: 3
Size: 255161 Color: 3
Size: 254634 Color: 2

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 490335 Color: 2
Size: 255280 Color: 1
Size: 254386 Color: 3

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 490405 Color: 0
Size: 257979 Color: 1
Size: 251617 Color: 2

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 490406 Color: 3
Size: 257004 Color: 2
Size: 252591 Color: 1

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 490521 Color: 4
Size: 256927 Color: 3
Size: 252553 Color: 0

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 490492 Color: 0
Size: 256724 Color: 4
Size: 252785 Color: 3

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 490542 Color: 3
Size: 258795 Color: 2
Size: 250664 Color: 4

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 490691 Color: 3
Size: 257318 Color: 2
Size: 251992 Color: 4

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 490863 Color: 0
Size: 254685 Color: 4
Size: 254453 Color: 1

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 490977 Color: 2
Size: 255653 Color: 2
Size: 253371 Color: 0

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 491111 Color: 1
Size: 258872 Color: 2
Size: 250018 Color: 0

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 491556 Color: 0
Size: 257370 Color: 3
Size: 251075 Color: 3

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 491905 Color: 0
Size: 257987 Color: 4
Size: 250109 Color: 2

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 492019 Color: 0
Size: 257483 Color: 1
Size: 250499 Color: 1

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 492303 Color: 0
Size: 255342 Color: 4
Size: 252356 Color: 4

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 492223 Color: 3
Size: 254875 Color: 1
Size: 252903 Color: 2

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 492579 Color: 0
Size: 257240 Color: 4
Size: 250182 Color: 2

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 492818 Color: 4
Size: 256349 Color: 1
Size: 250834 Color: 3

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 493077 Color: 0
Size: 256548 Color: 3
Size: 250376 Color: 3

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 493254 Color: 2
Size: 256439 Color: 1
Size: 250308 Color: 3

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 493266 Color: 4
Size: 256344 Color: 2
Size: 250391 Color: 3

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 493684 Color: 1
Size: 255996 Color: 3
Size: 250321 Color: 2

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 493840 Color: 1
Size: 253453 Color: 4
Size: 252708 Color: 3

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 493850 Color: 3
Size: 254022 Color: 1
Size: 252129 Color: 0

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 493898 Color: 4
Size: 253852 Color: 4
Size: 252251 Color: 2

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 493919 Color: 1
Size: 254140 Color: 4
Size: 251942 Color: 2

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 494061 Color: 0
Size: 255448 Color: 3
Size: 250492 Color: 4

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 494023 Color: 2
Size: 254362 Color: 1
Size: 251616 Color: 0

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 494255 Color: 2
Size: 254693 Color: 3
Size: 251053 Color: 0

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 494310 Color: 1
Size: 255480 Color: 3
Size: 250211 Color: 2

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 494587 Color: 4
Size: 253275 Color: 3
Size: 252139 Color: 1

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 494595 Color: 3
Size: 255235 Color: 0
Size: 250171 Color: 4

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 467020 Color: 4
Size: 272958 Color: 2
Size: 260023 Color: 1

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 494750 Color: 1
Size: 253221 Color: 3
Size: 252030 Color: 2

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 494930 Color: 0
Size: 253923 Color: 2
Size: 251148 Color: 1

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 495034 Color: 1
Size: 252810 Color: 0
Size: 252157 Color: 3

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 495083 Color: 4
Size: 252705 Color: 4
Size: 252213 Color: 2

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 495167 Color: 3
Size: 253202 Color: 4
Size: 251632 Color: 0

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 495276 Color: 2
Size: 254478 Color: 0
Size: 250247 Color: 4

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 495535 Color: 0
Size: 253133 Color: 1
Size: 251333 Color: 4

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 495673 Color: 1
Size: 252435 Color: 0
Size: 251893 Color: 3

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 495737 Color: 3
Size: 252842 Color: 4
Size: 251422 Color: 4

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 496091 Color: 0
Size: 252570 Color: 3
Size: 251340 Color: 3

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 496265 Color: 0
Size: 252213 Color: 2
Size: 251523 Color: 4

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 496440 Color: 4
Size: 252305 Color: 3
Size: 251256 Color: 0

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 496466 Color: 1
Size: 253535 Color: 0
Size: 250000 Color: 2

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 496702 Color: 1
Size: 251689 Color: 3
Size: 251610 Color: 4

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 496730 Color: 3
Size: 251795 Color: 0
Size: 251476 Color: 2

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 496979 Color: 2
Size: 252728 Color: 1
Size: 250294 Color: 3

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 497049 Color: 2
Size: 251578 Color: 4
Size: 251374 Color: 0

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 497285 Color: 2
Size: 252439 Color: 1
Size: 250277 Color: 0

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 497287 Color: 1
Size: 251744 Color: 2
Size: 250970 Color: 2

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 497612 Color: 3
Size: 251416 Color: 0
Size: 250973 Color: 2

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 497812 Color: 3
Size: 251147 Color: 1
Size: 251042 Color: 2

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 497814 Color: 4
Size: 251197 Color: 0
Size: 250990 Color: 2

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 498390 Color: 4
Size: 250869 Color: 0
Size: 250742 Color: 1

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 498764 Color: 2
Size: 250646 Color: 1
Size: 250591 Color: 3

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 498922 Color: 4
Size: 250826 Color: 0
Size: 250253 Color: 2

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 499435 Color: 2
Size: 250349 Color: 0
Size: 250217 Color: 3

Bin 3125: 1 of cap free
Amount of items: 3
Items: 
Size: 366845 Color: 1
Size: 348793 Color: 3
Size: 284362 Color: 4

Bin 3126: 1 of cap free
Amount of items: 3
Items: 
Size: 352478 Color: 0
Size: 351288 Color: 2
Size: 296234 Color: 4

Bin 3127: 1 of cap free
Amount of items: 3
Items: 
Size: 365676 Color: 1
Size: 356617 Color: 2
Size: 277707 Color: 0

Bin 3128: 1 of cap free
Amount of items: 3
Items: 
Size: 426383 Color: 4
Size: 293601 Color: 4
Size: 280016 Color: 2

Bin 3129: 1 of cap free
Amount of items: 3
Items: 
Size: 377890 Color: 1
Size: 361032 Color: 0
Size: 261078 Color: 3

Bin 3130: 1 of cap free
Amount of items: 3
Items: 
Size: 368457 Color: 3
Size: 346197 Color: 0
Size: 285346 Color: 4

Bin 3131: 1 of cap free
Amount of items: 3
Items: 
Size: 344100 Color: 2
Size: 329048 Color: 1
Size: 326852 Color: 0

Bin 3132: 1 of cap free
Amount of items: 3
Items: 
Size: 361997 Color: 3
Size: 346336 Color: 3
Size: 291667 Color: 4

Bin 3133: 1 of cap free
Amount of items: 3
Items: 
Size: 345875 Color: 1
Size: 330893 Color: 0
Size: 323232 Color: 1

Bin 3134: 1 of cap free
Amount of items: 3
Items: 
Size: 419116 Color: 4
Size: 329828 Color: 3
Size: 251056 Color: 1

Bin 3135: 1 of cap free
Amount of items: 3
Items: 
Size: 486295 Color: 3
Size: 262432 Color: 1
Size: 251273 Color: 0

Bin 3136: 1 of cap free
Amount of items: 3
Items: 
Size: 421796 Color: 4
Size: 326382 Color: 0
Size: 251822 Color: 2

Bin 3137: 1 of cap free
Amount of items: 3
Items: 
Size: 403723 Color: 4
Size: 320914 Color: 2
Size: 275363 Color: 0

Bin 3138: 1 of cap free
Amount of items: 3
Items: 
Size: 395690 Color: 1
Size: 323370 Color: 3
Size: 280940 Color: 0

Bin 3139: 1 of cap free
Amount of items: 3
Items: 
Size: 367116 Color: 2
Size: 364882 Color: 2
Size: 268002 Color: 4

Bin 3140: 1 of cap free
Amount of items: 3
Items: 
Size: 344139 Color: 4
Size: 340222 Color: 2
Size: 315639 Color: 0

Bin 3141: 1 of cap free
Amount of items: 3
Items: 
Size: 460787 Color: 2
Size: 270838 Color: 3
Size: 268375 Color: 4

Bin 3142: 1 of cap free
Amount of items: 3
Items: 
Size: 499714 Color: 3
Size: 250199 Color: 4
Size: 250087 Color: 2

Bin 3143: 1 of cap free
Amount of items: 3
Items: 
Size: 454697 Color: 0
Size: 281113 Color: 2
Size: 264190 Color: 4

Bin 3144: 1 of cap free
Amount of items: 3
Items: 
Size: 351052 Color: 1
Size: 344691 Color: 0
Size: 304257 Color: 1

Bin 3145: 1 of cap free
Amount of items: 3
Items: 
Size: 478125 Color: 3
Size: 266187 Color: 1
Size: 255688 Color: 2

Bin 3146: 1 of cap free
Amount of items: 3
Items: 
Size: 360031 Color: 2
Size: 322080 Color: 0
Size: 317889 Color: 2

Bin 3147: 1 of cap free
Amount of items: 3
Items: 
Size: 345404 Color: 0
Size: 343843 Color: 3
Size: 310753 Color: 1

Bin 3148: 1 of cap free
Amount of items: 3
Items: 
Size: 376646 Color: 3
Size: 365077 Color: 3
Size: 258277 Color: 0

Bin 3149: 1 of cap free
Amount of items: 3
Items: 
Size: 358497 Color: 3
Size: 320756 Color: 0
Size: 320747 Color: 0

Bin 3150: 1 of cap free
Amount of items: 3
Items: 
Size: 349641 Color: 1
Size: 347466 Color: 0
Size: 302893 Color: 4

Bin 3151: 1 of cap free
Amount of items: 3
Items: 
Size: 474557 Color: 2
Size: 263227 Color: 4
Size: 262216 Color: 0

Bin 3152: 1 of cap free
Amount of items: 3
Items: 
Size: 355855 Color: 4
Size: 353372 Color: 1
Size: 290773 Color: 3

Bin 3153: 1 of cap free
Amount of items: 3
Items: 
Size: 359702 Color: 0
Size: 352448 Color: 4
Size: 287850 Color: 0

Bin 3154: 1 of cap free
Amount of items: 3
Items: 
Size: 368604 Color: 4
Size: 354739 Color: 2
Size: 276657 Color: 2

Bin 3155: 1 of cap free
Amount of items: 3
Items: 
Size: 400610 Color: 2
Size: 318326 Color: 1
Size: 281064 Color: 2

Bin 3156: 1 of cap free
Amount of items: 3
Items: 
Size: 494550 Color: 2
Size: 254866 Color: 3
Size: 250584 Color: 0

Bin 3157: 1 of cap free
Amount of items: 3
Items: 
Size: 347847 Color: 4
Size: 346585 Color: 0
Size: 305568 Color: 1

Bin 3158: 1 of cap free
Amount of items: 3
Items: 
Size: 371381 Color: 0
Size: 363883 Color: 2
Size: 264736 Color: 4

Bin 3159: 1 of cap free
Amount of items: 3
Items: 
Size: 335577 Color: 4
Size: 334040 Color: 2
Size: 330383 Color: 3

Bin 3160: 1 of cap free
Amount of items: 3
Items: 
Size: 353934 Color: 3
Size: 347812 Color: 0
Size: 298254 Color: 3

Bin 3161: 1 of cap free
Amount of items: 3
Items: 
Size: 357370 Color: 0
Size: 330334 Color: 0
Size: 312296 Color: 2

Bin 3162: 1 of cap free
Amount of items: 3
Items: 
Size: 354987 Color: 3
Size: 334228 Color: 4
Size: 310785 Color: 3

Bin 3163: 1 of cap free
Amount of items: 3
Items: 
Size: 363633 Color: 3
Size: 359525 Color: 4
Size: 276842 Color: 3

Bin 3164: 1 of cap free
Amount of items: 3
Items: 
Size: 350755 Color: 0
Size: 334536 Color: 1
Size: 314709 Color: 0

Bin 3165: 1 of cap free
Amount of items: 3
Items: 
Size: 366447 Color: 4
Size: 348164 Color: 2
Size: 285389 Color: 3

Bin 3166: 1 of cap free
Amount of items: 3
Items: 
Size: 359821 Color: 0
Size: 349746 Color: 4
Size: 290433 Color: 0

Bin 3167: 1 of cap free
Amount of items: 3
Items: 
Size: 347817 Color: 1
Size: 327592 Color: 1
Size: 324591 Color: 3

Bin 3168: 1 of cap free
Amount of items: 3
Items: 
Size: 354307 Color: 3
Size: 344928 Color: 4
Size: 300765 Color: 2

Bin 3169: 1 of cap free
Amount of items: 3
Items: 
Size: 358413 Color: 1
Size: 348160 Color: 3
Size: 293427 Color: 3

Bin 3170: 1 of cap free
Amount of items: 3
Items: 
Size: 348152 Color: 2
Size: 336239 Color: 3
Size: 315609 Color: 3

Bin 3171: 1 of cap free
Amount of items: 3
Items: 
Size: 346314 Color: 1
Size: 329522 Color: 4
Size: 324164 Color: 4

Bin 3172: 1 of cap free
Amount of items: 3
Items: 
Size: 342194 Color: 2
Size: 334683 Color: 3
Size: 323123 Color: 0

Bin 3173: 1 of cap free
Amount of items: 3
Items: 
Size: 356757 Color: 3
Size: 330425 Color: 1
Size: 312818 Color: 4

Bin 3174: 1 of cap free
Amount of items: 3
Items: 
Size: 356881 Color: 1
Size: 335942 Color: 3
Size: 307177 Color: 1

Bin 3175: 1 of cap free
Amount of items: 3
Items: 
Size: 368939 Color: 1
Size: 340747 Color: 0
Size: 290314 Color: 0

Bin 3176: 1 of cap free
Amount of items: 3
Items: 
Size: 291314 Color: 4
Size: 347457 Color: 2
Size: 361229 Color: 0

Bin 3177: 1 of cap free
Amount of items: 3
Items: 
Size: 368708 Color: 4
Size: 354956 Color: 3
Size: 276336 Color: 2

Bin 3178: 1 of cap free
Amount of items: 3
Items: 
Size: 407480 Color: 4
Size: 311287 Color: 1
Size: 281233 Color: 4

Bin 3179: 2 of cap free
Amount of items: 3
Items: 
Size: 466371 Color: 2
Size: 277642 Color: 4
Size: 255986 Color: 3

Bin 3180: 2 of cap free
Amount of items: 3
Items: 
Size: 344795 Color: 2
Size: 330653 Color: 4
Size: 324551 Color: 1

Bin 3181: 2 of cap free
Amount of items: 3
Items: 
Size: 499025 Color: 3
Size: 250957 Color: 0
Size: 250017 Color: 2

Bin 3182: 2 of cap free
Amount of items: 3
Items: 
Size: 360272 Color: 2
Size: 350851 Color: 4
Size: 288876 Color: 1

Bin 3183: 2 of cap free
Amount of items: 3
Items: 
Size: 390201 Color: 2
Size: 357891 Color: 2
Size: 251907 Color: 0

Bin 3184: 2 of cap free
Amount of items: 3
Items: 
Size: 366398 Color: 3
Size: 353295 Color: 2
Size: 280306 Color: 2

Bin 3185: 2 of cap free
Amount of items: 3
Items: 
Size: 357187 Color: 2
Size: 336683 Color: 4
Size: 306129 Color: 3

Bin 3186: 2 of cap free
Amount of items: 3
Items: 
Size: 358456 Color: 2
Size: 341884 Color: 1
Size: 299659 Color: 1

Bin 3187: 2 of cap free
Amount of items: 3
Items: 
Size: 428634 Color: 1
Size: 319710 Color: 3
Size: 251655 Color: 2

Bin 3188: 2 of cap free
Amount of items: 3
Items: 
Size: 427214 Color: 3
Size: 317582 Color: 4
Size: 255203 Color: 1

Bin 3189: 2 of cap free
Amount of items: 3
Items: 
Size: 406066 Color: 4
Size: 322415 Color: 0
Size: 271518 Color: 1

Bin 3190: 2 of cap free
Amount of items: 3
Items: 
Size: 353291 Color: 0
Size: 342992 Color: 1
Size: 303716 Color: 3

Bin 3191: 2 of cap free
Amount of items: 3
Items: 
Size: 359171 Color: 1
Size: 359017 Color: 1
Size: 281811 Color: 4

Bin 3192: 2 of cap free
Amount of items: 3
Items: 
Size: 381091 Color: 4
Size: 339148 Color: 4
Size: 279760 Color: 3

Bin 3193: 2 of cap free
Amount of items: 3
Items: 
Size: 345667 Color: 1
Size: 338631 Color: 4
Size: 315701 Color: 2

Bin 3194: 3 of cap free
Amount of items: 3
Items: 
Size: 454862 Color: 2
Size: 276186 Color: 4
Size: 268950 Color: 2

Bin 3195: 3 of cap free
Amount of items: 3
Items: 
Size: 364361 Color: 1
Size: 323461 Color: 4
Size: 312176 Color: 2

Bin 3196: 3 of cap free
Amount of items: 3
Items: 
Size: 404390 Color: 1
Size: 317079 Color: 4
Size: 278529 Color: 2

Bin 3197: 3 of cap free
Amount of items: 3
Items: 
Size: 391717 Color: 3
Size: 347956 Color: 2
Size: 260325 Color: 4

Bin 3198: 3 of cap free
Amount of items: 3
Items: 
Size: 381197 Color: 3
Size: 348933 Color: 2
Size: 269868 Color: 3

Bin 3199: 3 of cap free
Amount of items: 3
Items: 
Size: 477810 Color: 2
Size: 265469 Color: 0
Size: 256719 Color: 4

Bin 3200: 3 of cap free
Amount of items: 3
Items: 
Size: 358272 Color: 3
Size: 337912 Color: 0
Size: 303814 Color: 4

Bin 3201: 3 of cap free
Amount of items: 3
Items: 
Size: 343885 Color: 2
Size: 337652 Color: 3
Size: 318461 Color: 1

Bin 3202: 3 of cap free
Amount of items: 3
Items: 
Size: 426890 Color: 1
Size: 315939 Color: 2
Size: 257169 Color: 1

Bin 3203: 3 of cap free
Amount of items: 3
Items: 
Size: 452932 Color: 4
Size: 282492 Color: 3
Size: 264574 Color: 4

Bin 3204: 3 of cap free
Amount of items: 3
Items: 
Size: 348167 Color: 2
Size: 346607 Color: 4
Size: 305224 Color: 4

Bin 3205: 4 of cap free
Amount of items: 3
Items: 
Size: 338338 Color: 2
Size: 334220 Color: 2
Size: 327439 Color: 1

Bin 3206: 4 of cap free
Amount of items: 3
Items: 
Size: 478357 Color: 4
Size: 270848 Color: 2
Size: 250792 Color: 2

Bin 3207: 4 of cap free
Amount of items: 3
Items: 
Size: 376177 Color: 3
Size: 361684 Color: 0
Size: 262136 Color: 4

Bin 3208: 4 of cap free
Amount of items: 3
Items: 
Size: 363925 Color: 1
Size: 321056 Color: 2
Size: 315016 Color: 1

Bin 3209: 4 of cap free
Amount of items: 3
Items: 
Size: 441981 Color: 0
Size: 279900 Color: 1
Size: 278116 Color: 2

Bin 3210: 4 of cap free
Amount of items: 3
Items: 
Size: 470429 Color: 3
Size: 279233 Color: 4
Size: 250335 Color: 3

Bin 3211: 5 of cap free
Amount of items: 3
Items: 
Size: 403456 Color: 1
Size: 344523 Color: 4
Size: 252017 Color: 3

Bin 3212: 5 of cap free
Amount of items: 3
Items: 
Size: 398974 Color: 2
Size: 328398 Color: 4
Size: 272624 Color: 4

Bin 3213: 5 of cap free
Amount of items: 3
Items: 
Size: 433141 Color: 3
Size: 314601 Color: 4
Size: 252254 Color: 4

Bin 3214: 5 of cap free
Amount of items: 3
Items: 
Size: 487999 Color: 1
Size: 261234 Color: 0
Size: 250763 Color: 4

Bin 3215: 5 of cap free
Amount of items: 3
Items: 
Size: 356096 Color: 4
Size: 328786 Color: 2
Size: 315114 Color: 1

Bin 3216: 6 of cap free
Amount of items: 3
Items: 
Size: 365416 Color: 3
Size: 346242 Color: 2
Size: 288337 Color: 3

Bin 3217: 6 of cap free
Amount of items: 3
Items: 
Size: 486513 Color: 0
Size: 259753 Color: 3
Size: 253729 Color: 2

Bin 3218: 6 of cap free
Amount of items: 3
Items: 
Size: 362528 Color: 1
Size: 355446 Color: 0
Size: 282021 Color: 0

Bin 3219: 6 of cap free
Amount of items: 3
Items: 
Size: 481116 Color: 1
Size: 265053 Color: 3
Size: 253826 Color: 3

Bin 3220: 6 of cap free
Amount of items: 3
Items: 
Size: 352073 Color: 1
Size: 338308 Color: 4
Size: 309614 Color: 0

Bin 3221: 6 of cap free
Amount of items: 3
Items: 
Size: 366969 Color: 3
Size: 360505 Color: 4
Size: 272521 Color: 3

Bin 3222: 6 of cap free
Amount of items: 3
Items: 
Size: 419671 Color: 3
Size: 323458 Color: 2
Size: 256866 Color: 2

Bin 3223: 6 of cap free
Amount of items: 3
Items: 
Size: 479163 Color: 0
Size: 262940 Color: 4
Size: 257892 Color: 2

Bin 3224: 6 of cap free
Amount of items: 3
Items: 
Size: 344482 Color: 0
Size: 333614 Color: 4
Size: 321899 Color: 3

Bin 3225: 7 of cap free
Amount of items: 3
Items: 
Size: 403046 Color: 0
Size: 336773 Color: 0
Size: 260175 Color: 1

Bin 3226: 7 of cap free
Amount of items: 3
Items: 
Size: 471253 Color: 1
Size: 271661 Color: 2
Size: 257080 Color: 0

Bin 3227: 7 of cap free
Amount of items: 3
Items: 
Size: 455907 Color: 0
Size: 284434 Color: 0
Size: 259653 Color: 4

Bin 3228: 7 of cap free
Amount of items: 3
Items: 
Size: 472266 Color: 0
Size: 270802 Color: 4
Size: 256926 Color: 3

Bin 3229: 8 of cap free
Amount of items: 3
Items: 
Size: 349297 Color: 4
Size: 339622 Color: 2
Size: 311074 Color: 3

Bin 3230: 8 of cap free
Amount of items: 3
Items: 
Size: 464958 Color: 4
Size: 268223 Color: 3
Size: 266812 Color: 3

Bin 3231: 8 of cap free
Amount of items: 3
Items: 
Size: 452747 Color: 2
Size: 290280 Color: 4
Size: 256966 Color: 2

Bin 3232: 8 of cap free
Amount of items: 3
Items: 
Size: 351390 Color: 2
Size: 331296 Color: 0
Size: 317307 Color: 4

Bin 3233: 9 of cap free
Amount of items: 3
Items: 
Size: 349451 Color: 4
Size: 330314 Color: 2
Size: 320227 Color: 1

Bin 3234: 9 of cap free
Amount of items: 3
Items: 
Size: 380312 Color: 0
Size: 321340 Color: 2
Size: 298340 Color: 4

Bin 3235: 9 of cap free
Amount of items: 3
Items: 
Size: 372876 Color: 4
Size: 340004 Color: 0
Size: 287112 Color: 2

Bin 3236: 10 of cap free
Amount of items: 3
Items: 
Size: 451782 Color: 3
Size: 279425 Color: 2
Size: 268784 Color: 1

Bin 3237: 10 of cap free
Amount of items: 3
Items: 
Size: 426876 Color: 1
Size: 317950 Color: 4
Size: 255165 Color: 3

Bin 3238: 10 of cap free
Amount of items: 3
Items: 
Size: 449157 Color: 2
Size: 291617 Color: 1
Size: 259217 Color: 1

Bin 3239: 10 of cap free
Amount of items: 3
Items: 
Size: 357726 Color: 2
Size: 353245 Color: 0
Size: 289020 Color: 3

Bin 3240: 10 of cap free
Amount of items: 3
Items: 
Size: 352634 Color: 3
Size: 350442 Color: 2
Size: 296915 Color: 4

Bin 3241: 10 of cap free
Amount of items: 3
Items: 
Size: 420078 Color: 4
Size: 328859 Color: 4
Size: 251054 Color: 0

Bin 3242: 11 of cap free
Amount of items: 3
Items: 
Size: 431365 Color: 4
Size: 295565 Color: 2
Size: 273060 Color: 3

Bin 3243: 11 of cap free
Amount of items: 3
Items: 
Size: 394687 Color: 2
Size: 344607 Color: 0
Size: 260696 Color: 0

Bin 3244: 11 of cap free
Amount of items: 3
Items: 
Size: 359663 Color: 0
Size: 347279 Color: 3
Size: 293048 Color: 0

Bin 3245: 11 of cap free
Amount of items: 3
Items: 
Size: 360057 Color: 2
Size: 357773 Color: 1
Size: 282160 Color: 0

Bin 3246: 12 of cap free
Amount of items: 3
Items: 
Size: 427699 Color: 2
Size: 302994 Color: 1
Size: 269296 Color: 1

Bin 3247: 12 of cap free
Amount of items: 3
Items: 
Size: 479386 Color: 3
Size: 268942 Color: 0
Size: 251661 Color: 1

Bin 3248: 12 of cap free
Amount of items: 3
Items: 
Size: 391352 Color: 3
Size: 343201 Color: 4
Size: 265436 Color: 1

Bin 3249: 12 of cap free
Amount of items: 3
Items: 
Size: 400794 Color: 3
Size: 348466 Color: 3
Size: 250729 Color: 2

Bin 3250: 13 of cap free
Amount of items: 3
Items: 
Size: 487943 Color: 3
Size: 256902 Color: 4
Size: 255143 Color: 4

Bin 3251: 15 of cap free
Amount of items: 3
Items: 
Size: 401776 Color: 0
Size: 337433 Color: 2
Size: 260777 Color: 2

Bin 3252: 15 of cap free
Amount of items: 3
Items: 
Size: 360745 Color: 2
Size: 352726 Color: 4
Size: 286515 Color: 1

Bin 3253: 15 of cap free
Amount of items: 3
Items: 
Size: 486487 Color: 1
Size: 258968 Color: 4
Size: 254531 Color: 2

Bin 3254: 16 of cap free
Amount of items: 3
Items: 
Size: 358909 Color: 3
Size: 350152 Color: 3
Size: 290924 Color: 0

Bin 3255: 16 of cap free
Amount of items: 3
Items: 
Size: 368240 Color: 4
Size: 348524 Color: 2
Size: 283221 Color: 4

Bin 3256: 16 of cap free
Amount of items: 3
Items: 
Size: 398449 Color: 1
Size: 301617 Color: 2
Size: 299919 Color: 2

Bin 3257: 17 of cap free
Amount of items: 3
Items: 
Size: 370984 Color: 2
Size: 341804 Color: 4
Size: 287196 Color: 1

Bin 3258: 17 of cap free
Amount of items: 3
Items: 
Size: 362739 Color: 3
Size: 333804 Color: 1
Size: 303441 Color: 0

Bin 3259: 18 of cap free
Amount of items: 3
Items: 
Size: 451962 Color: 4
Size: 274900 Color: 4
Size: 273121 Color: 1

Bin 3260: 18 of cap free
Amount of items: 3
Items: 
Size: 427525 Color: 3
Size: 293799 Color: 0
Size: 278659 Color: 0

Bin 3261: 19 of cap free
Amount of items: 3
Items: 
Size: 395043 Color: 2
Size: 302864 Color: 3
Size: 302075 Color: 2

Bin 3262: 19 of cap free
Amount of items: 3
Items: 
Size: 352528 Color: 1
Size: 348187 Color: 3
Size: 299267 Color: 3

Bin 3263: 19 of cap free
Amount of items: 3
Items: 
Size: 400149 Color: 4
Size: 339645 Color: 2
Size: 260188 Color: 0

Bin 3264: 19 of cap free
Amount of items: 3
Items: 
Size: 419835 Color: 4
Size: 325720 Color: 4
Size: 254427 Color: 2

Bin 3265: 21 of cap free
Amount of items: 3
Items: 
Size: 487624 Color: 0
Size: 261223 Color: 1
Size: 251133 Color: 4

Bin 3266: 21 of cap free
Amount of items: 3
Items: 
Size: 478690 Color: 0
Size: 266263 Color: 0
Size: 255027 Color: 4

Bin 3267: 23 of cap free
Amount of items: 3
Items: 
Size: 381386 Color: 3
Size: 328370 Color: 2
Size: 290222 Color: 2

Bin 3268: 23 of cap free
Amount of items: 3
Items: 
Size: 352384 Color: 4
Size: 334875 Color: 3
Size: 312719 Color: 0

Bin 3269: 23 of cap free
Amount of items: 3
Items: 
Size: 362737 Color: 0
Size: 335293 Color: 0
Size: 301948 Color: 1

Bin 3270: 23 of cap free
Amount of items: 3
Items: 
Size: 361197 Color: 4
Size: 359607 Color: 4
Size: 279174 Color: 1

Bin 3271: 23 of cap free
Amount of items: 3
Items: 
Size: 344665 Color: 0
Size: 338448 Color: 2
Size: 316865 Color: 0

Bin 3272: 25 of cap free
Amount of items: 3
Items: 
Size: 391406 Color: 4
Size: 307235 Color: 3
Size: 301335 Color: 3

Bin 3273: 27 of cap free
Amount of items: 3
Items: 
Size: 409763 Color: 1
Size: 312849 Color: 0
Size: 277362 Color: 3

Bin 3274: 29 of cap free
Amount of items: 3
Items: 
Size: 427515 Color: 2
Size: 308252 Color: 1
Size: 264205 Color: 3

Bin 3275: 31 of cap free
Amount of items: 3
Items: 
Size: 370706 Color: 1
Size: 342555 Color: 2
Size: 286709 Color: 3

Bin 3276: 33 of cap free
Amount of items: 3
Items: 
Size: 341993 Color: 0
Size: 335920 Color: 3
Size: 322055 Color: 1

Bin 3277: 34 of cap free
Amount of items: 3
Items: 
Size: 408076 Color: 2
Size: 338046 Color: 0
Size: 253845 Color: 4

Bin 3278: 34 of cap free
Amount of items: 3
Items: 
Size: 407881 Color: 2
Size: 335483 Color: 4
Size: 256603 Color: 2

Bin 3279: 34 of cap free
Amount of items: 3
Items: 
Size: 345979 Color: 2
Size: 337299 Color: 3
Size: 316689 Color: 2

Bin 3280: 36 of cap free
Amount of items: 3
Items: 
Size: 441177 Color: 0
Size: 284517 Color: 0
Size: 274271 Color: 2

Bin 3281: 38 of cap free
Amount of items: 3
Items: 
Size: 399910 Color: 1
Size: 333282 Color: 0
Size: 266771 Color: 0

Bin 3282: 39 of cap free
Amount of items: 3
Items: 
Size: 479537 Color: 3
Size: 264643 Color: 1
Size: 255782 Color: 2

Bin 3283: 42 of cap free
Amount of items: 3
Items: 
Size: 409820 Color: 3
Size: 302631 Color: 2
Size: 287508 Color: 2

Bin 3284: 43 of cap free
Amount of items: 3
Items: 
Size: 339187 Color: 1
Size: 338107 Color: 3
Size: 322664 Color: 4

Bin 3285: 44 of cap free
Amount of items: 3
Items: 
Size: 425955 Color: 1
Size: 307667 Color: 3
Size: 266335 Color: 0

Bin 3286: 46 of cap free
Amount of items: 3
Items: 
Size: 385295 Color: 0
Size: 311636 Color: 2
Size: 303024 Color: 3

Bin 3287: 50 of cap free
Amount of items: 3
Items: 
Size: 364782 Color: 2
Size: 321946 Color: 1
Size: 313223 Color: 3

Bin 3288: 51 of cap free
Amount of items: 3
Items: 
Size: 498201 Color: 3
Size: 251050 Color: 1
Size: 250699 Color: 1

Bin 3289: 53 of cap free
Amount of items: 3
Items: 
Size: 348356 Color: 0
Size: 331289 Color: 3
Size: 320303 Color: 2

Bin 3290: 54 of cap free
Amount of items: 3
Items: 
Size: 373777 Color: 1
Size: 370742 Color: 4
Size: 255428 Color: 0

Bin 3291: 56 of cap free
Amount of items: 3
Items: 
Size: 381375 Color: 0
Size: 351938 Color: 1
Size: 266632 Color: 0

Bin 3292: 56 of cap free
Amount of items: 3
Items: 
Size: 373860 Color: 2
Size: 317137 Color: 3
Size: 308948 Color: 0

Bin 3293: 64 of cap free
Amount of items: 3
Items: 
Size: 480803 Color: 3
Size: 267194 Color: 0
Size: 251940 Color: 2

Bin 3294: 65 of cap free
Amount of items: 3
Items: 
Size: 352301 Color: 2
Size: 328354 Color: 3
Size: 319281 Color: 1

Bin 3295: 67 of cap free
Amount of items: 3
Items: 
Size: 469685 Color: 0
Size: 272880 Color: 0
Size: 257369 Color: 2

Bin 3296: 76 of cap free
Amount of items: 3
Items: 
Size: 360071 Color: 1
Size: 356388 Color: 2
Size: 283466 Color: 1

Bin 3297: 78 of cap free
Amount of items: 3
Items: 
Size: 473276 Color: 3
Size: 272411 Color: 2
Size: 254236 Color: 4

Bin 3298: 78 of cap free
Amount of items: 3
Items: 
Size: 346638 Color: 3
Size: 333938 Color: 1
Size: 319347 Color: 0

Bin 3299: 78 of cap free
Amount of items: 3
Items: 
Size: 387531 Color: 3
Size: 352025 Color: 0
Size: 260367 Color: 1

Bin 3300: 79 of cap free
Amount of items: 3
Items: 
Size: 364941 Color: 1
Size: 336197 Color: 4
Size: 298784 Color: 3

Bin 3301: 80 of cap free
Amount of items: 3
Items: 
Size: 350583 Color: 2
Size: 348464 Color: 0
Size: 300874 Color: 2

Bin 3302: 85 of cap free
Amount of items: 3
Items: 
Size: 394814 Color: 3
Size: 334490 Color: 1
Size: 270612 Color: 4

Bin 3303: 87 of cap free
Amount of items: 3
Items: 
Size: 380823 Color: 0
Size: 357370 Color: 4
Size: 261721 Color: 4

Bin 3304: 98 of cap free
Amount of items: 3
Items: 
Size: 498547 Color: 2
Size: 250877 Color: 1
Size: 250479 Color: 0

Bin 3305: 99 of cap free
Amount of items: 3
Items: 
Size: 356243 Color: 3
Size: 330277 Color: 4
Size: 313382 Color: 2

Bin 3306: 108 of cap free
Amount of items: 3
Items: 
Size: 448243 Color: 3
Size: 295726 Color: 0
Size: 255924 Color: 3

Bin 3307: 117 of cap free
Amount of items: 3
Items: 
Size: 354540 Color: 3
Size: 341253 Color: 4
Size: 304091 Color: 4

Bin 3308: 118 of cap free
Amount of items: 3
Items: 
Size: 379269 Color: 0
Size: 329282 Color: 3
Size: 291332 Color: 2

Bin 3309: 124 of cap free
Amount of items: 3
Items: 
Size: 358776 Color: 2
Size: 357678 Color: 2
Size: 283423 Color: 1

Bin 3310: 129 of cap free
Amount of items: 3
Items: 
Size: 362280 Color: 1
Size: 350924 Color: 0
Size: 286668 Color: 4

Bin 3311: 132 of cap free
Amount of items: 3
Items: 
Size: 387732 Color: 4
Size: 359824 Color: 0
Size: 252313 Color: 3

Bin 3312: 140 of cap free
Amount of items: 3
Items: 
Size: 409399 Color: 0
Size: 338945 Color: 3
Size: 251517 Color: 1

Bin 3313: 144 of cap free
Amount of items: 3
Items: 
Size: 399082 Color: 2
Size: 340984 Color: 1
Size: 259791 Color: 1

Bin 3314: 166 of cap free
Amount of items: 3
Items: 
Size: 402544 Color: 2
Size: 341236 Color: 0
Size: 256055 Color: 2

Bin 3315: 166 of cap free
Amount of items: 3
Items: 
Size: 416300 Color: 3
Size: 296228 Color: 2
Size: 287307 Color: 3

Bin 3316: 204 of cap free
Amount of items: 3
Items: 
Size: 407420 Color: 2
Size: 306609 Color: 2
Size: 285768 Color: 3

Bin 3317: 224 of cap free
Amount of items: 3
Items: 
Size: 473244 Color: 2
Size: 269417 Color: 0
Size: 257116 Color: 3

Bin 3318: 227 of cap free
Amount of items: 3
Items: 
Size: 458023 Color: 2
Size: 289745 Color: 0
Size: 252006 Color: 3

Bin 3319: 234 of cap free
Amount of items: 3
Items: 
Size: 362223 Color: 4
Size: 343763 Color: 3
Size: 293781 Color: 3

Bin 3320: 305 of cap free
Amount of items: 3
Items: 
Size: 381902 Color: 2
Size: 335832 Color: 0
Size: 281962 Color: 1

Bin 3321: 336 of cap free
Amount of items: 3
Items: 
Size: 437160 Color: 4
Size: 308339 Color: 3
Size: 254166 Color: 0

Bin 3322: 422 of cap free
Amount of items: 3
Items: 
Size: 371330 Color: 0
Size: 339153 Color: 2
Size: 289096 Color: 1

Bin 3323: 738 of cap free
Amount of items: 3
Items: 
Size: 399586 Color: 1
Size: 301473 Color: 0
Size: 298204 Color: 0

Bin 3324: 833 of cap free
Amount of items: 3
Items: 
Size: 410061 Color: 0
Size: 333236 Color: 4
Size: 255871 Color: 2

Bin 3325: 866 of cap free
Amount of items: 3
Items: 
Size: 429724 Color: 1
Size: 294318 Color: 3
Size: 275093 Color: 2

Bin 3326: 932 of cap free
Amount of items: 3
Items: 
Size: 478085 Color: 2
Size: 267355 Color: 3
Size: 253629 Color: 0

Bin 3327: 1038 of cap free
Amount of items: 3
Items: 
Size: 447797 Color: 2
Size: 291693 Color: 3
Size: 259473 Color: 2

Bin 3328: 1127 of cap free
Amount of items: 3
Items: 
Size: 426841 Color: 0
Size: 294184 Color: 1
Size: 277849 Color: 0

Bin 3329: 2130 of cap free
Amount of items: 3
Items: 
Size: 387932 Color: 1
Size: 335322 Color: 4
Size: 274617 Color: 4

Bin 3330: 2702 of cap free
Amount of items: 3
Items: 
Size: 354762 Color: 1
Size: 322176 Color: 1
Size: 320361 Color: 2

Bin 3331: 9829 of cap free
Amount of items: 3
Items: 
Size: 387400 Color: 2
Size: 305717 Color: 0
Size: 297055 Color: 0

Bin 3332: 17277 of cap free
Amount of items: 3
Items: 
Size: 458647 Color: 0
Size: 270959 Color: 2
Size: 253118 Color: 1

Bin 3333: 247772 of cap free
Amount of items: 2
Items: 
Size: 499509 Color: 4
Size: 252720 Color: 1

Bin 3334: 347465 of cap free
Amount of items: 2
Items: 
Size: 354527 Color: 2
Size: 298009 Color: 0

Bin 3335: 361239 of cap free
Amount of items: 2
Items: 
Size: 289417 Color: 2
Size: 349345 Color: 3

Total size: 3334003334
Total free space: 1000001

