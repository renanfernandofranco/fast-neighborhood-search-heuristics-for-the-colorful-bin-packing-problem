Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 5385 Color: 1
Size: 1651 Color: 1
Size: 448 Color: 0
Size: 388 Color: 0
Size: 264 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 7294 Color: 1
Size: 702 Color: 1
Size: 140 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6199 Color: 1
Size: 1609 Color: 1
Size: 328 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7221 Color: 1
Size: 779 Color: 1
Size: 136 Color: 0

Bin 5: 0 of cap free
Amount of items: 5
Items: 
Size: 4024 Color: 1
Size: 1932 Color: 1
Size: 1474 Color: 1
Size: 384 Color: 0
Size: 322 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7206 Color: 1
Size: 698 Color: 1
Size: 232 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 1
Size: 1404 Color: 1
Size: 182 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7236 Color: 1
Size: 728 Color: 1
Size: 172 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 1
Size: 1244 Color: 1
Size: 176 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7196 Color: 1
Size: 908 Color: 1
Size: 32 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4069 Color: 1
Size: 3391 Color: 1
Size: 676 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7101 Color: 1
Size: 885 Color: 1
Size: 150 Color: 0

Bin 13: 0 of cap free
Amount of items: 7
Items: 
Size: 4292 Color: 1
Size: 1950 Color: 1
Size: 1272 Color: 1
Size: 248 Color: 0
Size: 176 Color: 0
Size: 158 Color: 0
Size: 40 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6371 Color: 1
Size: 1241 Color: 1
Size: 524 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6517 Color: 1
Size: 1351 Color: 1
Size: 268 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 1
Size: 1010 Color: 1
Size: 304 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 1
Size: 844 Color: 1
Size: 272 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7035 Color: 1
Size: 919 Color: 1
Size: 182 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7264 Color: 1
Size: 656 Color: 1
Size: 216 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 1
Size: 1446 Color: 1
Size: 288 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 1
Size: 3388 Color: 1
Size: 672 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6094 Color: 1
Size: 1530 Color: 1
Size: 512 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4462 Color: 1
Size: 3062 Color: 1
Size: 612 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5949 Color: 1
Size: 1823 Color: 1
Size: 364 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6134 Color: 1
Size: 1722 Color: 1
Size: 280 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 1
Size: 1465 Color: 1
Size: 292 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 1
Size: 1020 Color: 1
Size: 200 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 1
Size: 1300 Color: 1
Size: 80 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4071 Color: 1
Size: 3389 Color: 1
Size: 676 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 3576 Color: 1
Size: 3040 Color: 1
Size: 1520 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7117 Color: 1
Size: 791 Color: 1
Size: 228 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4070 Color: 1
Size: 3390 Color: 1
Size: 676 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7013 Color: 1
Size: 1075 Color: 1
Size: 48 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6797 Color: 1
Size: 1117 Color: 1
Size: 222 Color: 0

Bin 35: 0 of cap free
Amount of items: 5
Items: 
Size: 5687 Color: 1
Size: 1052 Color: 1
Size: 945 Color: 1
Size: 248 Color: 0
Size: 204 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4254 Color: 1
Size: 3238 Color: 1
Size: 644 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7232 Color: 1
Size: 456 Color: 0
Size: 448 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5782 Color: 1
Size: 2022 Color: 1
Size: 332 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 1
Size: 1068 Color: 1
Size: 384 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 1
Size: 2684 Color: 1
Size: 232 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 1
Size: 1006 Color: 1
Size: 270 Color: 0

Bin 42: 0 of cap free
Amount of items: 5
Items: 
Size: 5393 Color: 1
Size: 1182 Color: 1
Size: 863 Color: 1
Size: 404 Color: 0
Size: 294 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6930 Color: 1
Size: 934 Color: 1
Size: 272 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6142 Color: 1
Size: 1702 Color: 1
Size: 292 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6763 Color: 1
Size: 1197 Color: 1
Size: 176 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5693 Color: 1
Size: 2287 Color: 1
Size: 156 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 1
Size: 1660 Color: 1
Size: 328 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 1
Size: 2900 Color: 1
Size: 576 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 1
Size: 1444 Color: 1
Size: 280 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 4589 Color: 1
Size: 2957 Color: 1
Size: 590 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 7132 Color: 1
Size: 788 Color: 1
Size: 216 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 1
Size: 1356 Color: 1
Size: 280 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 4446 Color: 1
Size: 3078 Color: 1
Size: 612 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6926 Color: 1
Size: 1002 Color: 1
Size: 208 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 1
Size: 772 Color: 1
Size: 88 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6935 Color: 1
Size: 1001 Color: 1
Size: 200 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 1
Size: 938 Color: 1
Size: 184 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7195 Color: 1
Size: 759 Color: 1
Size: 182 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 1
Size: 2412 Color: 1
Size: 480 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5031 Color: 1
Size: 2589 Color: 1
Size: 516 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 1
Size: 1670 Color: 1
Size: 356 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 1
Size: 2060 Color: 1
Size: 408 Color: 0

Bin 63: 0 of cap free
Amount of items: 5
Items: 
Size: 4486 Color: 1
Size: 2286 Color: 1
Size: 760 Color: 1
Size: 452 Color: 0
Size: 152 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 4086 Color: 1
Size: 3042 Color: 1
Size: 1008 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5388 Color: 1
Size: 2596 Color: 1
Size: 152 Color: 0

Bin 66: 0 of cap free
Amount of items: 5
Items: 
Size: 4982 Color: 1
Size: 1284 Color: 1
Size: 1110 Color: 1
Size: 608 Color: 0
Size: 152 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 4294 Color: 1
Size: 3578 Color: 1
Size: 264 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 4962 Color: 1
Size: 2646 Color: 1
Size: 528 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 1
Size: 1964 Color: 1
Size: 392 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4597 Color: 1
Size: 2951 Color: 1
Size: 588 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 4270 Color: 1
Size: 3222 Color: 1
Size: 644 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6689 Color: 1
Size: 1207 Color: 1
Size: 240 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 1
Size: 1692 Color: 1
Size: 336 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 1
Size: 778 Color: 1
Size: 256 Color: 0

Bin 75: 0 of cap free
Amount of items: 5
Items: 
Size: 3770 Color: 1
Size: 2037 Color: 1
Size: 1781 Color: 1
Size: 360 Color: 0
Size: 188 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 1
Size: 774 Color: 1
Size: 640 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 1
Size: 3204 Color: 1
Size: 456 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 7092 Color: 1
Size: 876 Color: 1
Size: 168 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 4078 Color: 1
Size: 3382 Color: 1
Size: 676 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6909 Color: 1
Size: 1027 Color: 1
Size: 200 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 1
Size: 2260 Color: 1
Size: 448 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 1
Size: 1846 Color: 1
Size: 200 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7210 Color: 1
Size: 520 Color: 1
Size: 406 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5406 Color: 1
Size: 2278 Color: 1
Size: 452 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 1
Size: 2568 Color: 1
Size: 496 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 7106 Color: 1
Size: 862 Color: 1
Size: 168 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 1
Size: 1212 Color: 1
Size: 408 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 1
Size: 1468 Color: 1
Size: 288 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6644 Color: 1
Size: 1364 Color: 1
Size: 128 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5023 Color: 1
Size: 2595 Color: 1
Size: 518 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6781 Color: 1
Size: 1023 Color: 1
Size: 332 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 1
Size: 1628 Color: 1
Size: 240 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6207 Color: 1
Size: 1120 Color: 0
Size: 809 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 4998 Color: 1
Size: 2618 Color: 1
Size: 520 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 1
Size: 3052 Color: 1
Size: 168 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 1
Size: 1357 Color: 1
Size: 150 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 1
Size: 1219 Color: 1
Size: 456 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 7043 Color: 1
Size: 932 Color: 1
Size: 160 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 6157 Color: 1
Size: 1930 Color: 1
Size: 48 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 1
Size: 1471 Color: 1
Size: 144 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 1
Size: 1331 Color: 1
Size: 200 Color: 0

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 5822 Color: 1
Size: 2041 Color: 1
Size: 272 Color: 0

Bin 103: 2 of cap free
Amount of items: 3
Items: 
Size: 5394 Color: 1
Size: 2636 Color: 1
Size: 104 Color: 0

Bin 104: 2 of cap free
Amount of items: 3
Items: 
Size: 7003 Color: 1
Size: 911 Color: 1
Size: 220 Color: 0

Bin 105: 2 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 1
Size: 1098 Color: 1
Size: 160 Color: 0

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 6927 Color: 1
Size: 1071 Color: 1
Size: 136 Color: 0

Bin 107: 3 of cap free
Amount of items: 3
Items: 
Size: 7209 Color: 1
Size: 788 Color: 1
Size: 136 Color: 0

Bin 108: 3 of cap free
Amount of items: 3
Items: 
Size: 7075 Color: 1
Size: 874 Color: 1
Size: 184 Color: 0

Bin 109: 3 of cap free
Amount of items: 3
Items: 
Size: 7302 Color: 1
Size: 751 Color: 1
Size: 80 Color: 0

Bin 110: 3 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 1
Size: 851 Color: 1
Size: 264 Color: 0

Bin 111: 3 of cap free
Amount of items: 3
Items: 
Size: 6647 Color: 1
Size: 1322 Color: 1
Size: 164 Color: 0

Bin 112: 4 of cap free
Amount of items: 3
Items: 
Size: 6702 Color: 1
Size: 1218 Color: 1
Size: 212 Color: 0

Bin 113: 5 of cap free
Amount of items: 3
Items: 
Size: 6509 Color: 1
Size: 1302 Color: 1
Size: 320 Color: 0

Bin 114: 6 of cap free
Amount of items: 3
Items: 
Size: 5701 Color: 1
Size: 2293 Color: 1
Size: 136 Color: 0

Bin 115: 6 of cap free
Amount of items: 3
Items: 
Size: 7052 Color: 1
Size: 830 Color: 1
Size: 248 Color: 0

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 5999 Color: 1
Size: 1986 Color: 1
Size: 144 Color: 0

Bin 117: 9 of cap free
Amount of items: 3
Items: 
Size: 6806 Color: 1
Size: 785 Color: 1
Size: 536 Color: 0

Bin 118: 11 of cap free
Amount of items: 3
Items: 
Size: 6302 Color: 1
Size: 1615 Color: 1
Size: 208 Color: 0

Bin 119: 13 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 1
Size: 1145 Color: 1
Size: 608 Color: 0

Bin 120: 14 of cap free
Amount of items: 3
Items: 
Size: 5422 Color: 1
Size: 2242 Color: 1
Size: 458 Color: 0

Bin 121: 14 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 1
Size: 2262 Color: 1
Size: 40 Color: 0

Bin 122: 15 of cap free
Amount of items: 3
Items: 
Size: 6853 Color: 1
Size: 1188 Color: 1
Size: 80 Color: 0

Bin 123: 57 of cap free
Amount of items: 3
Items: 
Size: 7235 Color: 1
Size: 724 Color: 1
Size: 120 Color: 0

Bin 124: 58 of cap free
Amount of items: 3
Items: 
Size: 5446 Color: 1
Size: 2292 Color: 1
Size: 340 Color: 0

Bin 125: 67 of cap free
Amount of items: 3
Items: 
Size: 5798 Color: 1
Size: 2031 Color: 1
Size: 240 Color: 0

Bin 126: 294 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 1
Size: 2630 Color: 1
Size: 184 Color: 0

Bin 127: 836 of cap free
Amount of items: 1
Items: 
Size: 7300 Color: 1

Bin 128: 909 of cap free
Amount of items: 1
Items: 
Size: 7227 Color: 1

Bin 129: 962 of cap free
Amount of items: 1
Items: 
Size: 7174 Color: 1

Bin 130: 969 of cap free
Amount of items: 1
Items: 
Size: 7167 Color: 1

Bin 131: 994 of cap free
Amount of items: 1
Items: 
Size: 7142 Color: 1

Bin 132: 1299 of cap free
Amount of items: 1
Items: 
Size: 6837 Color: 1

Bin 133: 1558 of cap free
Amount of items: 1
Items: 
Size: 6578 Color: 1

Total size: 1073952
Total free space: 8136

