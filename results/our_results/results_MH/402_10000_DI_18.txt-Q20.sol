Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4069 Color: 1
Size: 788 Color: 19
Size: 788 Color: 9
Size: 772 Color: 6
Size: 760 Color: 18
Size: 759 Color: 10
Size: 200 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 4071 Color: 13
Size: 1930 Color: 13
Size: 1351 Color: 8
Size: 576 Color: 16
Size: 208 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 5
Size: 3388 Color: 15
Size: 272 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4486 Color: 2
Size: 3062 Color: 6
Size: 588 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4597 Color: 0
Size: 3389 Color: 1
Size: 150 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 19
Size: 3204 Color: 18
Size: 272 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5394 Color: 19
Size: 2262 Color: 8
Size: 480 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 17
Size: 2286 Color: 18
Size: 182 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5701 Color: 12
Size: 2031 Color: 0
Size: 404 Color: 16

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5782 Color: 11
Size: 2060 Color: 13
Size: 294 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5949 Color: 17
Size: 2037 Color: 17
Size: 150 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 3
Size: 1846 Color: 15
Size: 182 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6207 Color: 5
Size: 1609 Color: 2
Size: 320 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 12
Size: 1006 Color: 4
Size: 862 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 15
Size: 1465 Color: 7
Size: 292 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 7
Size: 1446 Color: 0
Size: 288 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 2
Size: 1468 Color: 7
Size: 152 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 4
Size: 1300 Color: 8
Size: 232 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6702 Color: 10
Size: 844 Color: 8
Size: 590 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 18
Size: 1212 Color: 14
Size: 208 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6797 Color: 12
Size: 1071 Color: 8
Size: 268 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 3
Size: 778 Color: 18
Size: 536 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6909 Color: 13
Size: 1027 Color: 3
Size: 200 Color: 15

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6930 Color: 19
Size: 934 Color: 13
Size: 272 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6935 Color: 15
Size: 1001 Color: 0
Size: 200 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 7
Size: 932 Color: 14
Size: 184 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7043 Color: 3
Size: 911 Color: 8
Size: 182 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 19
Size: 874 Color: 12
Size: 160 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7106 Color: 9
Size: 698 Color: 8
Size: 332 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7117 Color: 4
Size: 863 Color: 18
Size: 156 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7196 Color: 3
Size: 676 Color: 6
Size: 264 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 8
Size: 724 Color: 16
Size: 270 Color: 16

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7167 Color: 15
Size: 785 Color: 9
Size: 184 Color: 17

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7206 Color: 3
Size: 702 Color: 6
Size: 228 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7210 Color: 10
Size: 774 Color: 17
Size: 152 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7221 Color: 17
Size: 779 Color: 7
Size: 136 Color: 19

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7264 Color: 3
Size: 672 Color: 0
Size: 200 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7236 Color: 17
Size: 608 Color: 14
Size: 292 Color: 13

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 6
Size: 644 Color: 11
Size: 216 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7302 Color: 3
Size: 676 Color: 8
Size: 158 Color: 11

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5385 Color: 17
Size: 2630 Color: 2
Size: 120 Color: 4

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5687 Color: 2
Size: 2260 Color: 3
Size: 188 Color: 17

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6142 Color: 3
Size: 1781 Color: 11
Size: 212 Color: 8

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 13
Size: 1651 Color: 14
Size: 104 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6578 Color: 6
Size: 1357 Color: 12
Size: 200 Color: 9

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 12
Size: 1218 Color: 2
Size: 288 Color: 7

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6689 Color: 16
Size: 1302 Color: 1
Size: 144 Color: 12

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6781 Color: 16
Size: 1182 Color: 7
Size: 172 Color: 13

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 18
Size: 1219 Color: 15

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 7003 Color: 15
Size: 676 Color: 3
Size: 456 Color: 16

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 7018 Color: 19
Size: 1117 Color: 7

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 7035 Color: 11
Size: 1068 Color: 10
Size: 32 Color: 0

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 7227 Color: 1
Size: 908 Color: 11

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 3
Size: 2022 Color: 8
Size: 332 Color: 13

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 2
Size: 1660 Color: 14
Size: 364 Color: 11

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 14
Size: 1986 Color: 6

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6412 Color: 0
Size: 1722 Color: 11

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6509 Color: 16
Size: 1241 Color: 17
Size: 384 Color: 2

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 4
Size: 1098 Color: 12
Size: 160 Color: 10

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 7014 Color: 19
Size: 1120 Color: 11

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 7132 Color: 2
Size: 1002 Color: 11

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 19
Size: 2589 Color: 17
Size: 516 Color: 15

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 5999 Color: 0
Size: 1950 Color: 14
Size: 184 Color: 17

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 6134 Color: 3
Size: 1615 Color: 2
Size: 384 Color: 13

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 6094 Color: 19
Size: 1823 Color: 15
Size: 216 Color: 14

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6926 Color: 15
Size: 1207 Color: 11

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 7195 Color: 16
Size: 938 Color: 9

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 7294 Color: 9
Size: 791 Color: 6
Size: 48 Color: 7

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 4270 Color: 9
Size: 3222 Color: 6
Size: 640 Color: 10

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 15
Size: 3576 Color: 4
Size: 264 Color: 19

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 4462 Color: 2
Size: 3390 Color: 14
Size: 280 Color: 18

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 4589 Color: 19
Size: 3391 Color: 7
Size: 152 Color: 10

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 5388 Color: 1
Size: 2568 Color: 4
Size: 176 Color: 15

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 5406 Color: 9
Size: 2646 Color: 18
Size: 80 Color: 16

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 11
Size: 1272 Color: 8

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 6090 Color: 2
Size: 2041 Color: 10

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 6199 Color: 18
Size: 1932 Color: 2

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 8
Size: 2242 Color: 5
Size: 644 Color: 4

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 12
Size: 1670 Color: 8

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 7300 Color: 18
Size: 830 Color: 19

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 10
Size: 1628 Color: 4

Bin 82: 8 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 11
Size: 1520 Color: 4
Size: 88 Color: 0

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 13
Size: 1530 Color: 11
Size: 48 Color: 5

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 6684 Color: 0
Size: 1444 Color: 9

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 6806 Color: 5
Size: 1322 Color: 12

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 7209 Color: 8
Size: 919 Color: 14

Bin 87: 9 of cap free
Amount of items: 2
Items: 
Size: 6763 Color: 11
Size: 1364 Color: 16

Bin 88: 9 of cap free
Amount of items: 2
Items: 
Size: 7052 Color: 15
Size: 1075 Color: 2

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 7075 Color: 18
Size: 1052 Color: 0

Bin 90: 10 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 4
Size: 2618 Color: 7
Size: 80 Color: 8

Bin 91: 10 of cap free
Amount of items: 2
Items: 
Size: 6722 Color: 12
Size: 1404 Color: 7

Bin 92: 12 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 15
Size: 3040 Color: 0
Size: 1008 Color: 12

Bin 93: 12 of cap free
Amount of items: 2
Items: 
Size: 6927 Color: 11
Size: 1197 Color: 10

Bin 94: 12 of cap free
Amount of items: 2
Items: 
Size: 7101 Color: 11
Size: 1023 Color: 17

Bin 95: 13 of cap free
Amount of items: 2
Items: 
Size: 7013 Color: 2
Size: 1110 Color: 5

Bin 96: 15 of cap free
Amount of items: 2
Items: 
Size: 6157 Color: 10
Size: 1964 Color: 16

Bin 97: 15 of cap free
Amount of items: 3
Items: 
Size: 6302 Color: 7
Size: 1010 Color: 18
Size: 809 Color: 3

Bin 98: 15 of cap free
Amount of items: 2
Items: 
Size: 6647 Color: 15
Size: 1474 Color: 0

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 6837 Color: 5
Size: 1284 Color: 9

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 5220 Color: 16
Size: 2900 Color: 13

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 7235 Color: 12
Size: 885 Color: 18

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 7174 Color: 14
Size: 945 Color: 13

Bin 103: 18 of cap free
Amount of items: 3
Items: 
Size: 5031 Color: 12
Size: 2951 Color: 3
Size: 136 Color: 7

Bin 104: 20 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 13
Size: 2596 Color: 17
Size: 448 Color: 6

Bin 105: 21 of cap free
Amount of items: 2
Items: 
Size: 5822 Color: 5
Size: 2293 Color: 3

Bin 106: 21 of cap free
Amount of items: 2
Items: 
Size: 6644 Color: 16
Size: 1471 Color: 17

Bin 107: 23 of cap free
Amount of items: 3
Items: 
Size: 6371 Color: 18
Size: 1702 Color: 12
Size: 40 Color: 5

Bin 108: 23 of cap free
Amount of items: 3
Items: 
Size: 6517 Color: 10
Size: 1188 Color: 6
Size: 408 Color: 2

Bin 109: 24 of cap free
Amount of items: 3
Items: 
Size: 5446 Color: 3
Size: 2278 Color: 9
Size: 388 Color: 4

Bin 110: 24 of cap free
Amount of items: 2
Items: 
Size: 5820 Color: 10
Size: 2292 Color: 3

Bin 111: 24 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 12
Size: 1356 Color: 11

Bin 112: 24 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 11
Size: 1020 Color: 8

Bin 113: 27 of cap free
Amount of items: 3
Items: 
Size: 5393 Color: 19
Size: 2636 Color: 10
Size: 80 Color: 10

Bin 114: 28 of cap free
Amount of items: 2
Items: 
Size: 7232 Color: 1
Size: 876 Color: 16

Bin 115: 30 of cap free
Amount of items: 2
Items: 
Size: 5422 Color: 9
Size: 2684 Color: 0

Bin 116: 31 of cap free
Amount of items: 2
Items: 
Size: 5693 Color: 3
Size: 2412 Color: 14

Bin 117: 34 of cap free
Amount of items: 2
Items: 
Size: 4078 Color: 11
Size: 4024 Color: 4

Bin 118: 34 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 16
Size: 1692 Color: 17
Size: 40 Color: 18

Bin 119: 36 of cap free
Amount of items: 4
Items: 
Size: 4086 Color: 16
Size: 3238 Color: 13
Size: 528 Color: 12
Size: 248 Color: 8

Bin 120: 39 of cap free
Amount of items: 2
Items: 
Size: 6853 Color: 17
Size: 1244 Color: 11

Bin 121: 43 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 15
Size: 2957 Color: 12
Size: 220 Color: 19

Bin 122: 51 of cap free
Amount of items: 2
Items: 
Size: 5798 Color: 1
Size: 2287 Color: 15

Bin 123: 52 of cap free
Amount of items: 3
Items: 
Size: 4254 Color: 15
Size: 3382 Color: 2
Size: 448 Color: 7

Bin 124: 59 of cap free
Amount of items: 3
Items: 
Size: 4962 Color: 15
Size: 2595 Color: 17
Size: 520 Color: 7

Bin 125: 60 of cap free
Amount of items: 2
Items: 
Size: 4998 Color: 14
Size: 3078 Color: 4

Bin 126: 66 of cap free
Amount of items: 26
Items: 
Size: 458 Color: 11
Size: 456 Color: 14
Size: 452 Color: 14
Size: 452 Color: 11
Size: 408 Color: 1
Size: 406 Color: 19
Size: 392 Color: 3
Size: 356 Color: 9
Size: 340 Color: 18
Size: 336 Color: 16
Size: 328 Color: 15
Size: 328 Color: 5
Size: 322 Color: 12
Size: 304 Color: 7
Size: 280 Color: 12
Size: 280 Color: 5
Size: 264 Color: 6
Size: 256 Color: 15
Size: 248 Color: 7
Size: 248 Color: 6
Size: 240 Color: 2
Size: 232 Color: 18
Size: 204 Color: 4
Size: 176 Color: 8
Size: 168 Color: 8
Size: 136 Color: 16

Bin 127: 71 of cap free
Amount of items: 2
Items: 
Size: 5023 Color: 6
Size: 3042 Color: 12

Bin 128: 72 of cap free
Amount of items: 2
Items: 
Size: 4294 Color: 16
Size: 3770 Color: 2

Bin 129: 75 of cap free
Amount of items: 15
Items: 
Size: 751 Color: 9
Size: 728 Color: 5
Size: 676 Color: 6
Size: 612 Color: 10
Size: 612 Color: 10
Size: 608 Color: 18
Size: 524 Color: 17
Size: 520 Color: 10
Size: 518 Color: 13
Size: 512 Color: 17
Size: 496 Color: 13
Size: 456 Color: 12
Size: 448 Color: 7
Size: 360 Color: 7
Size: 240 Color: 8

Bin 130: 83 of cap free
Amount of items: 5
Items: 
Size: 4070 Color: 13
Size: 1331 Color: 9
Size: 1145 Color: 9
Size: 851 Color: 19
Size: 656 Color: 7

Bin 131: 102 of cap free
Amount of items: 2
Items: 
Size: 4982 Color: 5
Size: 3052 Color: 2

Bin 132: 112 of cap free
Amount of items: 2
Items: 
Size: 4446 Color: 4
Size: 3578 Color: 10

Bin 133: 6450 of cap free
Amount of items: 10
Items: 
Size: 240 Color: 3
Size: 222 Color: 12
Size: 176 Color: 18
Size: 168 Color: 4
Size: 168 Color: 1
Size: 164 Color: 8
Size: 144 Color: 11
Size: 140 Color: 14
Size: 136 Color: 0
Size: 128 Color: 16

Total size: 1073952
Total free space: 8136

