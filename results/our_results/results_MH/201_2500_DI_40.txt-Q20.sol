Capicity Bin: 1916
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 772 Color: 19
Size: 582 Color: 19
Size: 396 Color: 15
Size: 110 Color: 14
Size: 56 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 962 Color: 0
Size: 798 Color: 3
Size: 156 Color: 5

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1233 Color: 5
Size: 559 Color: 0
Size: 52 Color: 19
Size: 38 Color: 13
Size: 34 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1526 Color: 17
Size: 326 Color: 8
Size: 64 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1331 Color: 11
Size: 489 Color: 9
Size: 96 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 11
Size: 442 Color: 18
Size: 84 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1672 Color: 14
Size: 204 Color: 19
Size: 40 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 0
Size: 262 Color: 15
Size: 52 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1511 Color: 7
Size: 339 Color: 9
Size: 66 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 4
Size: 209 Color: 15
Size: 40 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1133 Color: 6
Size: 571 Color: 14
Size: 212 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 0
Size: 253 Color: 18
Size: 50 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 971 Color: 10
Size: 789 Color: 6
Size: 156 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 16
Size: 221 Color: 18
Size: 44 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1109 Color: 14
Size: 673 Color: 13
Size: 134 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 7
Size: 194 Color: 18
Size: 36 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 7
Size: 309 Color: 6
Size: 58 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1565 Color: 2
Size: 293 Color: 3
Size: 58 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 5
Size: 190 Color: 16
Size: 20 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 19
Size: 171 Color: 7
Size: 56 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 6
Size: 433 Color: 10
Size: 44 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1247 Color: 7
Size: 653 Color: 18
Size: 16 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1637 Color: 4
Size: 233 Color: 19
Size: 46 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 12
Size: 246 Color: 10
Size: 48 Color: 12

Bin 25: 0 of cap free
Amount of items: 4
Items: 
Size: 1320 Color: 19
Size: 444 Color: 0
Size: 112 Color: 2
Size: 40 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 5
Size: 474 Color: 11
Size: 92 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1648 Color: 10
Size: 228 Color: 1
Size: 40 Color: 8

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 16
Size: 300 Color: 17
Size: 130 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 8
Size: 642 Color: 8
Size: 128 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1129 Color: 6
Size: 657 Color: 1
Size: 130 Color: 15

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 1278 Color: 19
Size: 362 Color: 3
Size: 216 Color: 16
Size: 60 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 11
Size: 202 Color: 14
Size: 40 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1251 Color: 3
Size: 555 Color: 17
Size: 110 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 14
Size: 355 Color: 19
Size: 70 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1267 Color: 19
Size: 615 Color: 5
Size: 34 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 3
Size: 582 Color: 4
Size: 116 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 963 Color: 18
Size: 795 Color: 10
Size: 158 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 4
Size: 298 Color: 2
Size: 36 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 10
Size: 237 Color: 16
Size: 46 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 6
Size: 282 Color: 3
Size: 72 Color: 10

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1295 Color: 17
Size: 519 Color: 4
Size: 102 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 5
Size: 427 Color: 12
Size: 84 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 15
Size: 271 Color: 6
Size: 52 Color: 7

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1282 Color: 12
Size: 530 Color: 14
Size: 104 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1204 Color: 10
Size: 644 Color: 13
Size: 68 Color: 17

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 959 Color: 4
Size: 799 Color: 0
Size: 158 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1538 Color: 6
Size: 318 Color: 11
Size: 60 Color: 15

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 9
Size: 321 Color: 13
Size: 64 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 3
Size: 394 Color: 6
Size: 76 Color: 5

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1709 Color: 12
Size: 173 Color: 9
Size: 34 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1137 Color: 6
Size: 651 Color: 19
Size: 128 Color: 8

Bin 52: 0 of cap free
Amount of items: 4
Items: 
Size: 1711 Color: 5
Size: 197 Color: 2
Size: 4 Color: 13
Size: 4 Color: 5

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1062 Color: 11
Size: 714 Color: 16
Size: 140 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1467 Color: 14
Size: 375 Color: 0
Size: 74 Color: 14

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 3
Size: 251 Color: 8
Size: 48 Color: 18

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 17
Size: 234 Color: 12
Size: 44 Color: 15

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1365 Color: 0
Size: 461 Color: 19
Size: 90 Color: 15

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1095 Color: 9
Size: 685 Color: 11
Size: 136 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 16
Size: 221 Color: 9
Size: 42 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 2
Size: 191 Color: 15
Size: 44 Color: 5

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 967 Color: 12
Size: 791 Color: 12
Size: 158 Color: 10

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 8
Size: 383 Color: 4
Size: 76 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1515 Color: 5
Size: 335 Color: 2
Size: 66 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 15
Size: 171 Color: 6
Size: 32 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 7
Size: 219 Color: 14
Size: 20 Color: 5

Total size: 124540
Total free space: 0

