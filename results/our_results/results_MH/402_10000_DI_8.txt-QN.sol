Capicity Bin: 7760
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 4680 Color: 290
Size: 1816 Color: 225
Size: 1200 Color: 192
Size: 64 Color: 6

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 4870 Color: 296
Size: 2410 Color: 245
Size: 304 Color: 86
Size: 176 Color: 38

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 5592 Color: 317
Size: 1616 Color: 216
Size: 392 Color: 102
Size: 160 Color: 29

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6881 Color: 391
Size: 733 Color: 146
Size: 146 Color: 24

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4855 Color: 295
Size: 2421 Color: 246
Size: 484 Color: 115

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3884 Color: 274
Size: 3236 Color: 271
Size: 640 Color: 128

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4403 Color: 282
Size: 2799 Color: 259
Size: 558 Color: 126

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 298
Size: 2324 Color: 244
Size: 464 Color: 112

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6819 Color: 385
Size: 785 Color: 153
Size: 156 Color: 28

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 5360 Color: 308
Size: 1792 Color: 222
Size: 320 Color: 91
Size: 288 Color: 84

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 376
Size: 881 Color: 163
Size: 176 Color: 39

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 327
Size: 1596 Color: 212
Size: 312 Color: 89

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 354
Size: 1134 Color: 186
Size: 224 Color: 59

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 396
Size: 678 Color: 141
Size: 132 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6730 Color: 378
Size: 862 Color: 159
Size: 168 Color: 36

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5758 Color: 322
Size: 1726 Color: 220
Size: 276 Color: 81

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6982 Color: 401
Size: 650 Color: 136
Size: 128 Color: 12

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5605 Color: 318
Size: 1797 Color: 223
Size: 358 Color: 96

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 356
Size: 1061 Color: 182
Size: 212 Color: 57

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 3882 Color: 273
Size: 3234 Color: 270
Size: 644 Color: 133

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6215 Color: 343
Size: 1289 Color: 198
Size: 256 Color: 76

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6688 Color: 374
Size: 1008 Color: 178
Size: 64 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 309
Size: 2324 Color: 243
Size: 32 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 289
Size: 2732 Color: 251
Size: 544 Color: 120

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6265 Color: 346
Size: 1247 Color: 195
Size: 248 Color: 70

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 369
Size: 924 Color: 168
Size: 176 Color: 40

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 389
Size: 756 Color: 148
Size: 144 Color: 23

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 350
Size: 1162 Color: 190
Size: 228 Color: 63

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5929 Color: 332
Size: 1579 Color: 210
Size: 252 Color: 72

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 381
Size: 834 Color: 157
Size: 164 Color: 35

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 3136 Color: 263
Size: 2776 Color: 254
Size: 1608 Color: 214
Size: 240 Color: 67

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 395
Size: 690 Color: 142
Size: 136 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6490 Color: 357
Size: 1062 Color: 183
Size: 208 Color: 55

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4328 Color: 280
Size: 2872 Color: 262
Size: 560 Color: 127

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6386 Color: 352
Size: 1146 Color: 188
Size: 228 Color: 65

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 372
Size: 904 Color: 166
Size: 176 Color: 41

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6778 Color: 383
Size: 822 Color: 156
Size: 160 Color: 32

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 3885 Color: 275
Size: 3231 Color: 268
Size: 644 Color: 134

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4418 Color: 285
Size: 2786 Color: 256
Size: 556 Color: 122

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 379
Size: 664 Color: 139
Size: 352 Color: 95

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6202 Color: 342
Size: 1302 Color: 199
Size: 256 Color: 75

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4880 Color: 297
Size: 2608 Color: 250
Size: 272 Color: 80

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6057 Color: 336
Size: 1421 Color: 204
Size: 282 Color: 83

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 348
Size: 1202 Color: 193
Size: 236 Color: 66

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 338
Size: 1372 Color: 202
Size: 272 Color: 78

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6404 Color: 355
Size: 1132 Color: 185
Size: 224 Color: 60

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6694 Color: 375
Size: 890 Color: 164
Size: 176 Color: 42

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6097 Color: 337
Size: 1387 Color: 203
Size: 276 Color: 82

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 4776 Color: 291
Size: 2488 Color: 249
Size: 496 Color: 118

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6672 Color: 371
Size: 928 Color: 169
Size: 160 Color: 31

Bin 51: 0 of cap free
Amount of items: 4
Items: 
Size: 6984 Color: 402
Size: 456 Color: 110
Size: 160 Color: 33
Size: 160 Color: 30

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6844 Color: 387
Size: 764 Color: 150
Size: 152 Color: 26

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5962 Color: 333
Size: 1502 Color: 206
Size: 296 Color: 85

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4838 Color: 292
Size: 2438 Color: 248
Size: 484 Color: 116

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5774 Color: 323
Size: 1658 Color: 218
Size: 328 Color: 93

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 360
Size: 1048 Color: 180
Size: 192 Color: 50

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 390
Size: 744 Color: 147
Size: 144 Color: 22

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6784 Color: 384
Size: 864 Color: 160
Size: 112 Color: 10

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5304 Color: 307
Size: 2056 Color: 235
Size: 400 Color: 103

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6509 Color: 359
Size: 1043 Color: 179
Size: 208 Color: 54

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6389 Color: 353
Size: 1143 Color: 187
Size: 228 Color: 62

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5500 Color: 313
Size: 1884 Color: 228
Size: 376 Color: 99

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6040 Color: 335
Size: 864 Color: 161
Size: 856 Color: 158

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6383 Color: 351
Size: 1149 Color: 189
Size: 228 Color: 64

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6344 Color: 349
Size: 1192 Color: 191
Size: 224 Color: 61

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5490 Color: 312
Size: 1894 Color: 229
Size: 376 Color: 100

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 311
Size: 1980 Color: 232
Size: 312 Color: 88

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 304
Size: 2134 Color: 238
Size: 424 Color: 106

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 393
Size: 718 Color: 144
Size: 140 Color: 19

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6885 Color: 392
Size: 731 Color: 145
Size: 144 Color: 21

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 320
Size: 2014 Color: 233
Size: 16 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6600 Color: 365
Size: 968 Color: 174
Size: 192 Color: 48

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6597 Color: 364
Size: 971 Color: 175
Size: 192 Color: 49

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 329
Size: 1556 Color: 208
Size: 304 Color: 87

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6687 Color: 373
Size: 895 Color: 165
Size: 178 Color: 43

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6955 Color: 397
Size: 671 Color: 140
Size: 134 Color: 17

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6593 Color: 363
Size: 973 Color: 176
Size: 194 Color: 51

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6630 Color: 366
Size: 942 Color: 172
Size: 188 Color: 47

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 347
Size: 1242 Color: 194
Size: 248 Color: 69

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5516 Color: 315
Size: 1876 Color: 226
Size: 368 Color: 97

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 3881 Color: 272
Size: 3233 Color: 269
Size: 646 Color: 135

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 3896 Color: 278
Size: 3224 Color: 265
Size: 640 Color: 131

Bin 83: 0 of cap free
Amount of items: 4
Items: 
Size: 6156 Color: 340
Size: 1572 Color: 209
Size: 16 Color: 1
Size: 16 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5998 Color: 334
Size: 1538 Color: 207
Size: 224 Color: 58

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6118 Color: 339
Size: 1370 Color: 201
Size: 272 Color: 79

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5097 Color: 303
Size: 2221 Color: 239
Size: 442 Color: 107

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 5297 Color: 306
Size: 2053 Color: 234
Size: 410 Color: 104

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5576 Color: 316
Size: 2120 Color: 236
Size: 64 Color: 8

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 400
Size: 664 Color: 138
Size: 128 Color: 13

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5064 Color: 302
Size: 2248 Color: 240
Size: 448 Color: 108

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 300
Size: 2308 Color: 242
Size: 456 Color: 111

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5832 Color: 326
Size: 1448 Color: 205
Size: 480 Color: 113

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 3898 Color: 279
Size: 3222 Color: 264
Size: 640 Color: 130

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6918 Color: 394
Size: 702 Color: 143
Size: 140 Color: 20

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6644 Color: 367
Size: 932 Color: 171
Size: 184 Color: 46

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 294
Size: 2422 Color: 247
Size: 484 Color: 114

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 3892 Color: 277
Size: 3228 Color: 267
Size: 640 Color: 129

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6220 Color: 344
Size: 1284 Color: 197
Size: 256 Color: 74

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5828 Color: 325
Size: 1612 Color: 215
Size: 320 Color: 90

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 3890 Color: 276
Size: 3226 Color: 266
Size: 644 Color: 132

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 358
Size: 1060 Color: 181
Size: 208 Color: 56

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5044 Color: 301
Size: 2268 Color: 241
Size: 448 Color: 109

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 5502 Color: 314
Size: 1882 Color: 227
Size: 376 Color: 98

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 398
Size: 662 Color: 137
Size: 132 Color: 16

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5214 Color: 305
Size: 2122 Color: 237
Size: 424 Color: 105

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5817 Color: 324
Size: 1621 Color: 217
Size: 322 Color: 92

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 4980 Color: 299
Size: 1964 Color: 231
Size: 816 Color: 154

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 288
Size: 2740 Color: 252
Size: 544 Color: 119

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 4845 Color: 293
Size: 2797 Color: 258
Size: 118 Color: 11

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6714 Color: 377
Size: 874 Color: 162
Size: 172 Color: 37

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6200 Color: 341
Size: 1304 Color: 200
Size: 256 Color: 73

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6663 Color: 370
Size: 915 Color: 167
Size: 182 Color: 44

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6851 Color: 388
Size: 759 Color: 149
Size: 150 Color: 25

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5897 Color: 328
Size: 1731 Color: 221
Size: 132 Color: 15

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5921 Color: 331
Size: 1593 Color: 211
Size: 246 Color: 68

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 5905 Color: 330
Size: 1597 Color: 213
Size: 258 Color: 77

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5465 Color: 310
Size: 1913 Color: 230
Size: 382 Color: 101

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 4411 Color: 284
Size: 2791 Color: 257
Size: 558 Color: 125

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6249 Color: 345
Size: 1261 Color: 196
Size: 250 Color: 71

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 4434 Color: 287
Size: 2774 Color: 253
Size: 552 Color: 121

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4407 Color: 283
Size: 2859 Color: 261
Size: 494 Color: 117

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6646 Color: 368
Size: 930 Color: 170
Size: 184 Color: 45

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 321
Size: 1814 Color: 224
Size: 204 Color: 53

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 361
Size: 1090 Color: 184
Size: 96 Color: 9

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 4402 Color: 281
Size: 2802 Color: 260
Size: 556 Color: 124

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6837 Color: 386
Size: 771 Color: 152
Size: 152 Color: 27

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 362
Size: 989 Color: 177
Size: 196 Color: 52

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 5721 Color: 319
Size: 1701 Color: 219
Size: 338 Color: 94

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 4423 Color: 286
Size: 2781 Color: 255
Size: 556 Color: 123

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 380
Size: 951 Color: 173
Size: 50 Color: 5

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6775 Color: 382
Size: 821 Color: 155
Size: 164 Color: 34

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6967 Color: 399
Size: 769 Color: 151
Size: 24 Color: 3

Total size: 1024320
Total free space: 0

