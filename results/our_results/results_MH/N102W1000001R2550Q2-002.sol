Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 361181 Color: 1
Size: 355356 Color: 0
Size: 283464 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 404236 Color: 0
Size: 323498 Color: 0
Size: 272267 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 360531 Color: 1
Size: 308524 Color: 0
Size: 330946 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 367850 Color: 1
Size: 334975 Color: 1
Size: 297176 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 456424 Color: 0
Size: 292203 Color: 1
Size: 251374 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 423015 Color: 1
Size: 301155 Color: 0
Size: 275831 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 459496 Color: 0
Size: 283271 Color: 1
Size: 257234 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 404260 Color: 0
Size: 342294 Color: 1
Size: 253447 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 409656 Color: 1
Size: 326108 Color: 0
Size: 264237 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 463043 Color: 1
Size: 274306 Color: 0
Size: 262652 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 420366 Color: 0
Size: 311852 Color: 1
Size: 267783 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 386174 Color: 1
Size: 348934 Color: 1
Size: 264893 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 456561 Color: 0
Size: 273073 Color: 1
Size: 270367 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 377791 Color: 0
Size: 350988 Color: 1
Size: 271222 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 469015 Color: 1
Size: 267991 Color: 0
Size: 262995 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 477894 Color: 0
Size: 261563 Color: 1
Size: 260544 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 425103 Color: 1
Size: 324430 Color: 0
Size: 250468 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 404432 Color: 0
Size: 332821 Color: 1
Size: 262748 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 424170 Color: 1
Size: 299410 Color: 0
Size: 276421 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 424129 Color: 0
Size: 323977 Color: 1
Size: 251895 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 386803 Color: 1
Size: 333959 Color: 0
Size: 279239 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 400802 Color: 0
Size: 320299 Color: 1
Size: 278900 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 436431 Color: 1
Size: 288129 Color: 0
Size: 275441 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 372791 Color: 0
Size: 347861 Color: 1
Size: 279349 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 406621 Color: 1
Size: 305431 Color: 0
Size: 287949 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 361882 Color: 0
Size: 359225 Color: 1
Size: 278894 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 449871 Color: 0
Size: 276805 Color: 1
Size: 273325 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 367354 Color: 1
Size: 335926 Color: 1
Size: 296721 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 366188 Color: 0
Size: 333953 Color: 0
Size: 299860 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 408920 Color: 0
Size: 333494 Color: 1
Size: 257587 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 499931 Color: 1
Size: 250045 Color: 0
Size: 250025 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 430993 Color: 0
Size: 295302 Color: 1
Size: 273706 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 424095 Color: 0
Size: 299124 Color: 0
Size: 276782 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 442337 Color: 1
Size: 296700 Color: 1
Size: 260964 Color: 0

Total size: 34000034
Total free space: 0

