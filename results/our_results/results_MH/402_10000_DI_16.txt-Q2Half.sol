Capicity Bin: 8352
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4186 Color: 1
Size: 3474 Color: 1
Size: 692 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 1
Size: 3476 Color: 1
Size: 688 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4194 Color: 1
Size: 3466 Color: 1
Size: 692 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4202 Color: 1
Size: 3462 Color: 1
Size: 688 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4882 Color: 1
Size: 2894 Color: 1
Size: 576 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 1
Size: 2908 Color: 1
Size: 288 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 1
Size: 2660 Color: 1
Size: 216 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5505 Color: 1
Size: 2373 Color: 1
Size: 474 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5709 Color: 1
Size: 2203 Color: 1
Size: 440 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 1
Size: 2164 Color: 1
Size: 432 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5762 Color: 1
Size: 2162 Color: 1
Size: 428 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6060 Color: 1
Size: 1916 Color: 1
Size: 376 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 1
Size: 1894 Color: 1
Size: 376 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6094 Color: 1
Size: 1882 Color: 1
Size: 376 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6252 Color: 1
Size: 1756 Color: 1
Size: 344 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 1
Size: 1650 Color: 1
Size: 360 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6358 Color: 1
Size: 1662 Color: 1
Size: 332 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6374 Color: 1
Size: 1482 Color: 1
Size: 496 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 1
Size: 1453 Color: 1
Size: 412 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 1
Size: 1364 Color: 1
Size: 472 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 1
Size: 1258 Color: 1
Size: 520 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 1
Size: 1481 Color: 1
Size: 296 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 1
Size: 1442 Color: 1
Size: 284 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 1
Size: 1426 Color: 1
Size: 284 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 1
Size: 1136 Color: 1
Size: 536 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 1
Size: 1303 Color: 1
Size: 344 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 1
Size: 1372 Color: 1
Size: 272 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 1
Size: 1460 Color: 1
Size: 168 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6743 Color: 1
Size: 1341 Color: 1
Size: 268 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6789 Color: 1
Size: 1225 Color: 1
Size: 338 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 1
Size: 1294 Color: 1
Size: 256 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 1
Size: 1422 Color: 1
Size: 106 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6846 Color: 1
Size: 1174 Color: 1
Size: 332 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6852 Color: 1
Size: 1252 Color: 1
Size: 248 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 1
Size: 1205 Color: 1
Size: 264 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 1
Size: 1036 Color: 0
Size: 400 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 1
Size: 1204 Color: 1
Size: 202 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6964 Color: 1
Size: 1078 Color: 1
Size: 310 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6969 Color: 1
Size: 1021 Color: 1
Size: 362 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6982 Color: 1
Size: 1158 Color: 1
Size: 212 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6998 Color: 1
Size: 1062 Color: 1
Size: 292 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 1
Size: 1164 Color: 1
Size: 168 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7033 Color: 1
Size: 1111 Color: 1
Size: 208 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 7052 Color: 1
Size: 1084 Color: 1
Size: 216 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 1
Size: 1116 Color: 1
Size: 174 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7075 Color: 1
Size: 1053 Color: 1
Size: 224 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 1
Size: 1094 Color: 1
Size: 180 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 7110 Color: 1
Size: 1012 Color: 1
Size: 230 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 7118 Color: 1
Size: 1030 Color: 1
Size: 204 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 7129 Color: 1
Size: 1057 Color: 1
Size: 166 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 1
Size: 964 Color: 1
Size: 248 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 1
Size: 942 Color: 1
Size: 260 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 1
Size: 1056 Color: 1
Size: 140 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 1
Size: 828 Color: 1
Size: 320 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 7215 Color: 1
Size: 949 Color: 1
Size: 188 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 7222 Color: 1
Size: 742 Color: 1
Size: 388 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 7238 Color: 1
Size: 902 Color: 1
Size: 212 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7247 Color: 1
Size: 921 Color: 1
Size: 184 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 1
Size: 890 Color: 1
Size: 192 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 1
Size: 916 Color: 1
Size: 160 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 7305 Color: 1
Size: 873 Color: 1
Size: 174 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 7308 Color: 1
Size: 860 Color: 1
Size: 184 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 1
Size: 876 Color: 1
Size: 152 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 7334 Color: 1
Size: 850 Color: 1
Size: 168 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 7338 Color: 1
Size: 718 Color: 1
Size: 296 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 7353 Color: 1
Size: 799 Color: 1
Size: 200 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 7363 Color: 1
Size: 825 Color: 1
Size: 164 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 7364 Color: 1
Size: 778 Color: 1
Size: 210 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 7378 Color: 1
Size: 730 Color: 1
Size: 244 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 7396 Color: 1
Size: 954 Color: 1
Size: 2 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 7412 Color: 1
Size: 788 Color: 1
Size: 152 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7422 Color: 1
Size: 778 Color: 1
Size: 152 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 7427 Color: 1
Size: 771 Color: 1
Size: 154 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7442 Color: 1
Size: 846 Color: 1
Size: 64 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 7452 Color: 1
Size: 724 Color: 1
Size: 176 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 7457 Color: 1
Size: 747 Color: 1
Size: 148 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7466 Color: 1
Size: 814 Color: 1
Size: 72 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 7478 Color: 1
Size: 716 Color: 1
Size: 158 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 7484 Color: 1
Size: 520 Color: 1
Size: 348 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 7494 Color: 1
Size: 746 Color: 1
Size: 112 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 7500 Color: 1
Size: 664 Color: 1
Size: 188 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 7503 Color: 1
Size: 709 Color: 1
Size: 140 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7510 Color: 1
Size: 756 Color: 1
Size: 86 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 7511 Color: 1
Size: 701 Color: 1
Size: 140 Color: 0

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 4284 Color: 1
Size: 3477 Color: 1
Size: 590 Color: 0

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 1
Size: 3004 Color: 1
Size: 224 Color: 0

Bin 87: 1 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 1
Size: 2691 Color: 1
Size: 200 Color: 0

Bin 88: 1 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 1
Size: 2083 Color: 1
Size: 538 Color: 0

Bin 89: 1 of cap free
Amount of items: 3
Items: 
Size: 5786 Color: 1
Size: 2061 Color: 1
Size: 504 Color: 0

Bin 90: 1 of cap free
Amount of items: 3
Items: 
Size: 6021 Color: 1
Size: 2186 Color: 1
Size: 144 Color: 0

Bin 91: 1 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 1
Size: 1924 Color: 1
Size: 160 Color: 0

Bin 92: 1 of cap free
Amount of items: 3
Items: 
Size: 6315 Color: 1
Size: 1828 Color: 1
Size: 208 Color: 0

Bin 93: 1 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 1
Size: 1699 Color: 1
Size: 240 Color: 0

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 1
Size: 1555 Color: 1
Size: 336 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 1
Size: 1153 Color: 1
Size: 232 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 7085 Color: 1
Size: 1162 Color: 1
Size: 104 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 7213 Color: 1
Size: 1002 Color: 1
Size: 136 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 1
Size: 833 Color: 1
Size: 232 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 7395 Color: 1
Size: 762 Color: 1
Size: 194 Color: 0

Bin 100: 2 of cap free
Amount of items: 3
Items: 
Size: 4178 Color: 1
Size: 3532 Color: 1
Size: 640 Color: 0

Bin 101: 2 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 1
Size: 2906 Color: 1
Size: 576 Color: 0

Bin 102: 2 of cap free
Amount of items: 3
Items: 
Size: 5212 Color: 1
Size: 2994 Color: 1
Size: 144 Color: 0

Bin 103: 2 of cap free
Amount of items: 3
Items: 
Size: 5330 Color: 1
Size: 2852 Color: 1
Size: 168 Color: 0

Bin 104: 2 of cap free
Amount of items: 3
Items: 
Size: 5916 Color: 1
Size: 2218 Color: 1
Size: 216 Color: 0

Bin 105: 2 of cap free
Amount of items: 3
Items: 
Size: 6175 Color: 1
Size: 1943 Color: 1
Size: 232 Color: 0

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 6330 Color: 1
Size: 1620 Color: 1
Size: 400 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 1
Size: 1678 Color: 1
Size: 332 Color: 0

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 1
Size: 1612 Color: 1
Size: 144 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 1
Size: 1466 Color: 1
Size: 280 Color: 0

Bin 110: 2 of cap free
Amount of items: 3
Items: 
Size: 6650 Color: 1
Size: 1580 Color: 1
Size: 120 Color: 0

Bin 111: 2 of cap free
Amount of items: 3
Items: 
Size: 7458 Color: 1
Size: 804 Color: 0
Size: 88 Color: 0

Bin 112: 3 of cap free
Amount of items: 3
Items: 
Size: 4144 Color: 1
Size: 4061 Color: 1
Size: 144 Color: 0

Bin 113: 3 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 1
Size: 3481 Color: 1
Size: 528 Color: 0

Bin 114: 3 of cap free
Amount of items: 3
Items: 
Size: 5879 Color: 1
Size: 2142 Color: 1
Size: 328 Color: 0

Bin 115: 4 of cap free
Amount of items: 3
Items: 
Size: 5694 Color: 1
Size: 2494 Color: 1
Size: 160 Color: 0

Bin 116: 4 of cap free
Amount of items: 3
Items: 
Size: 6164 Color: 1
Size: 2036 Color: 1
Size: 148 Color: 0

Bin 117: 5 of cap free
Amount of items: 3
Items: 
Size: 4637 Color: 1
Size: 3482 Color: 1
Size: 228 Color: 0

Bin 118: 5 of cap free
Amount of items: 3
Items: 
Size: 4803 Color: 1
Size: 3104 Color: 1
Size: 440 Color: 0

Bin 119: 6 of cap free
Amount of items: 3
Items: 
Size: 5362 Color: 1
Size: 2404 Color: 1
Size: 580 Color: 0

Bin 120: 7 of cap free
Amount of items: 3
Items: 
Size: 5391 Color: 1
Size: 2530 Color: 1
Size: 424 Color: 0

Bin 121: 11 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 1
Size: 3431 Color: 1
Size: 148 Color: 0

Bin 122: 13 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 1
Size: 3279 Color: 1
Size: 312 Color: 0

Bin 123: 16 of cap free
Amount of items: 5
Items: 
Size: 4180 Color: 1
Size: 2620 Color: 1
Size: 694 Color: 0
Size: 694 Color: 0
Size: 148 Color: 0

Bin 124: 19 of cap free
Amount of items: 3
Items: 
Size: 5318 Color: 1
Size: 2959 Color: 1
Size: 56 Color: 0

Bin 125: 29 of cap free
Amount of items: 3
Items: 
Size: 4747 Color: 1
Size: 3348 Color: 1
Size: 228 Color: 0

Bin 126: 31 of cap free
Amount of items: 3
Items: 
Size: 4185 Color: 1
Size: 3540 Color: 1
Size: 596 Color: 0

Bin 127: 38 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 1
Size: 3358 Color: 1
Size: 160 Color: 0

Bin 128: 38 of cap free
Amount of items: 3
Items: 
Size: 4866 Color: 1
Size: 2848 Color: 1
Size: 600 Color: 0

Bin 129: 66 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 1
Size: 3348 Color: 1
Size: 160 Color: 0

Bin 130: 75 of cap free
Amount of items: 3
Items: 
Size: 4181 Color: 1
Size: 3656 Color: 1
Size: 440 Color: 0

Bin 131: 793 of cap free
Amount of items: 3
Items: 
Size: 4177 Color: 1
Size: 2690 Color: 1
Size: 692 Color: 0

Bin 132: 1366 of cap free
Amount of items: 5
Items: 
Size: 2613 Color: 1
Size: 1815 Color: 1
Size: 1686 Color: 1
Size: 436 Color: 0
Size: 436 Color: 0

Bin 133: 5778 of cap free
Amount of items: 3
Items: 
Size: 1288 Color: 1
Size: 1142 Color: 1
Size: 144 Color: 0

Total size: 1102464
Total free space: 8352

