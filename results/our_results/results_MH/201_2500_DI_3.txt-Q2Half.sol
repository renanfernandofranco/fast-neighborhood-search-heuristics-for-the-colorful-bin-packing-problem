Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 976 Color: 1
Size: 840 Color: 1
Size: 384 Color: 1
Size: 128 Color: 0
Size: 80 Color: 0
Size: 48 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 1
Size: 394 Color: 1
Size: 122 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1713 Color: 1
Size: 607 Color: 1
Size: 88 Color: 0
Size: 48 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 1
Size: 596 Color: 1
Size: 112 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 2028 Color: 1
Size: 364 Color: 1
Size: 48 Color: 0
Size: 16 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 1986 Color: 1
Size: 272 Color: 0
Size: 142 Color: 1
Size: 56 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1993 Color: 1
Size: 387 Color: 1
Size: 76 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 1
Size: 467 Color: 1
Size: 92 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 1
Size: 478 Color: 1
Size: 88 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 1
Size: 877 Color: 1
Size: 174 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2074 Color: 1
Size: 322 Color: 1
Size: 60 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 1
Size: 530 Color: 1
Size: 104 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 1
Size: 425 Color: 1
Size: 84 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 1
Size: 650 Color: 1
Size: 128 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 1
Size: 242 Color: 1
Size: 48 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 1
Size: 719 Color: 1
Size: 8 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 1
Size: 833 Color: 1
Size: 52 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 1
Size: 724 Color: 1
Size: 144 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 1
Size: 214 Color: 1
Size: 40 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1660 Color: 1
Size: 512 Color: 1
Size: 284 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 1
Size: 1020 Color: 1
Size: 200 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 1
Size: 842 Color: 1
Size: 280 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 1
Size: 715 Color: 1
Size: 142 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 1
Size: 668 Color: 1
Size: 92 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 844 Color: 1
Size: 76 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 1
Size: 884 Color: 1
Size: 104 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 1
Size: 863 Color: 1
Size: 172 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 742 Color: 1
Size: 144 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 1
Size: 232 Color: 1
Size: 32 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 1
Size: 349 Color: 1
Size: 68 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 1
Size: 497 Color: 1
Size: 72 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 1
Size: 474 Color: 0
Size: 8 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 1
Size: 446 Color: 1
Size: 88 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 1
Size: 236 Color: 1
Size: 112 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 1
Size: 678 Color: 1
Size: 132 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 1
Size: 574 Color: 1
Size: 112 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 1
Size: 938 Color: 1
Size: 4 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1500 Color: 1
Size: 916 Color: 1
Size: 40 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 1
Size: 314 Color: 1
Size: 36 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 1
Size: 568 Color: 1
Size: 524 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 1
Size: 751 Color: 1
Size: 150 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 1
Size: 262 Color: 1
Size: 48 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 1
Size: 1022 Color: 1
Size: 204 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 1
Size: 873 Color: 1
Size: 174 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 1142 Color: 1
Size: 76 Color: 0

Bin 46: 0 of cap free
Amount of items: 4
Items: 
Size: 1595 Color: 1
Size: 621 Color: 1
Size: 176 Color: 0
Size: 64 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 1
Size: 356 Color: 1
Size: 64 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 1
Size: 650 Color: 1
Size: 52 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 1
Size: 156 Color: 1
Size: 120 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 1
Size: 435 Color: 1
Size: 86 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 1
Size: 164 Color: 1
Size: 96 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 1
Size: 292 Color: 1
Size: 40 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1611 Color: 1
Size: 705 Color: 1
Size: 140 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1924 Color: 1
Size: 444 Color: 1
Size: 88 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 1
Size: 531 Color: 1
Size: 104 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 1
Size: 354 Color: 1
Size: 68 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1931 Color: 1
Size: 439 Color: 1
Size: 86 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 1
Size: 786 Color: 1
Size: 220 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1804 Color: 1
Size: 548 Color: 1
Size: 104 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 1
Size: 541 Color: 1
Size: 108 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 1
Size: 226 Color: 1
Size: 44 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1836 Color: 1
Size: 436 Color: 1
Size: 184 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 1
Size: 1023 Color: 1
Size: 204 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 1
Size: 1021 Color: 1
Size: 204 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2003 Color: 1
Size: 389 Color: 1
Size: 64 Color: 0

Total size: 159640
Total free space: 0

