Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1396 Color: 1
Size: 988 Color: 1
Size: 44 Color: 0
Size: 28 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1558 Color: 1
Size: 738 Color: 1
Size: 88 Color: 0
Size: 56 Color: 0
Size: 16 Color: 0

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1660 Color: 1
Size: 532 Color: 1
Size: 104 Color: 0
Size: 96 Color: 0
Size: 64 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1891 Color: 1
Size: 485 Color: 1
Size: 80 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 1
Size: 438 Color: 1
Size: 48 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 1
Size: 254 Color: 1
Size: 88 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2132 Color: 1
Size: 276 Color: 1
Size: 48 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 1
Size: 330 Color: 1
Size: 64 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 1
Size: 1020 Color: 1
Size: 200 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 1
Size: 863 Color: 1
Size: 172 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 1
Size: 1021 Color: 1
Size: 204 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 1
Size: 609 Color: 1
Size: 120 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 1
Size: 1022 Color: 1
Size: 204 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 1
Size: 646 Color: 1
Size: 128 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 1
Size: 873 Color: 1
Size: 174 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 1
Size: 730 Color: 1
Size: 16 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 1
Size: 248 Color: 1
Size: 16 Color: 0

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 1974 Color: 1
Size: 384 Color: 1
Size: 94 Color: 0
Size: 4 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 1
Size: 602 Color: 1
Size: 120 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 1
Size: 458 Color: 1
Size: 88 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 1
Size: 284 Color: 1
Size: 56 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2060 Color: 1
Size: 332 Color: 1
Size: 64 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1484 Color: 1
Size: 812 Color: 1
Size: 160 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 1
Size: 874 Color: 1
Size: 172 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 1
Size: 286 Color: 1
Size: 112 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 1
Size: 750 Color: 1
Size: 280 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 1
Size: 262 Color: 1
Size: 48 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 1
Size: 741 Color: 1
Size: 146 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 1
Size: 668 Color: 1
Size: 8 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 1
Size: 228 Color: 1
Size: 40 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 1
Size: 508 Color: 1
Size: 128 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 1
Size: 1023 Color: 1
Size: 204 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1964 Color: 1
Size: 412 Color: 1
Size: 80 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 1
Size: 564 Color: 1
Size: 40 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1907 Color: 1
Size: 541 Color: 1
Size: 8 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2074 Color: 1
Size: 322 Color: 1
Size: 60 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 1
Size: 731 Color: 1
Size: 144 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 1
Size: 507 Color: 1
Size: 100 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1906 Color: 1
Size: 406 Color: 1
Size: 144 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 1
Size: 512 Color: 1
Size: 370 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 1
Size: 262 Color: 1
Size: 36 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1018 Color: 1
Size: 976 Color: 1
Size: 462 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 2174 Color: 1
Size: 238 Color: 1
Size: 44 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 1
Size: 483 Color: 1
Size: 26 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 1
Size: 272 Color: 0
Size: 148 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 840 Color: 1
Size: 80 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 1
Size: 749 Color: 1
Size: 148 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1276 Color: 1
Size: 980 Color: 1
Size: 200 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1719 Color: 1
Size: 615 Color: 1
Size: 122 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 1
Size: 568 Color: 1
Size: 192 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 1
Size: 370 Color: 1
Size: 72 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 1
Size: 436 Color: 1
Size: 80 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 862 Color: 1
Size: 356 Color: 0

Bin 54: 0 of cap free
Amount of items: 4
Items: 
Size: 1833 Color: 1
Size: 471 Color: 1
Size: 88 Color: 0
Size: 64 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 1
Size: 220 Color: 1
Size: 32 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 1
Size: 526 Color: 1
Size: 104 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 1
Size: 692 Color: 1
Size: 32 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 1
Size: 631 Color: 1
Size: 124 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 1
Size: 210 Color: 1
Size: 48 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1572 Color: 1
Size: 740 Color: 1
Size: 144 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1823 Color: 1
Size: 585 Color: 1
Size: 48 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 1
Size: 638 Color: 1
Size: 124 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2206 Color: 1
Size: 168 Color: 1
Size: 82 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1830 Color: 1
Size: 522 Color: 1
Size: 104 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1723 Color: 1
Size: 611 Color: 1
Size: 122 Color: 0

Total size: 159640
Total free space: 0

