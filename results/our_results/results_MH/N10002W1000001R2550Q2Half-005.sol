Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 483991 Color: 1
Size: 264958 Color: 1
Size: 251052 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 391254 Color: 1
Size: 319184 Color: 1
Size: 289563 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 411246 Color: 1
Size: 298588 Color: 0
Size: 290167 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 466684 Color: 1
Size: 275491 Color: 1
Size: 257826 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 405116 Color: 1
Size: 306988 Color: 1
Size: 287897 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 380740 Color: 1
Size: 315598 Color: 1
Size: 303663 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 368955 Color: 1
Size: 358625 Color: 1
Size: 272421 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 355726 Color: 1
Size: 335151 Color: 1
Size: 309124 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 466424 Color: 1
Size: 280061 Color: 1
Size: 253516 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 397213 Color: 1
Size: 329983 Color: 1
Size: 272805 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 345741 Color: 1
Size: 344264 Color: 1
Size: 309996 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 442348 Color: 1
Size: 295068 Color: 1
Size: 262585 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 490933 Color: 1
Size: 257154 Color: 1
Size: 251914 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 403601 Color: 1
Size: 328949 Color: 1
Size: 267451 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 408209 Color: 1
Size: 336228 Color: 1
Size: 255564 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 386765 Color: 1
Size: 333332 Color: 1
Size: 279904 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 392610 Color: 1
Size: 318999 Color: 1
Size: 288392 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 390932 Color: 1
Size: 334823 Color: 1
Size: 274246 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 436651 Color: 1
Size: 286389 Color: 0
Size: 276961 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 476238 Color: 1
Size: 268892 Color: 1
Size: 254871 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 468698 Color: 1
Size: 280717 Color: 1
Size: 250586 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 387351 Color: 1
Size: 346935 Color: 1
Size: 265715 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 397238 Color: 1
Size: 333513 Color: 1
Size: 269250 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 390974 Color: 1
Size: 323823 Color: 1
Size: 285204 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 392441 Color: 1
Size: 319739 Color: 1
Size: 287821 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 400120 Color: 1
Size: 324003 Color: 1
Size: 275878 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 487712 Color: 1
Size: 261885 Color: 1
Size: 250404 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 448635 Color: 1
Size: 283409 Color: 1
Size: 267957 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 438838 Color: 1
Size: 305822 Color: 1
Size: 255341 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 423154 Color: 1
Size: 308552 Color: 1
Size: 268295 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 409716 Color: 1
Size: 299820 Color: 1
Size: 290465 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 470898 Color: 1
Size: 273781 Color: 1
Size: 255322 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 445770 Color: 1
Size: 278081 Color: 1
Size: 276150 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 347231 Color: 1
Size: 338170 Color: 1
Size: 314600 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 374242 Color: 1
Size: 356533 Color: 1
Size: 269226 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 412316 Color: 1
Size: 317680 Color: 1
Size: 270005 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 469457 Color: 1
Size: 272722 Color: 1
Size: 257822 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 467873 Color: 1
Size: 276206 Color: 1
Size: 255922 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 457887 Color: 1
Size: 290851 Color: 1
Size: 251263 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 434674 Color: 1
Size: 303438 Color: 1
Size: 261889 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 443042 Color: 1
Size: 288186 Color: 1
Size: 268773 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 433063 Color: 1
Size: 291819 Color: 1
Size: 275119 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 393678 Color: 1
Size: 314331 Color: 1
Size: 291992 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 444196 Color: 1
Size: 302886 Color: 1
Size: 252919 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 491266 Color: 1
Size: 257789 Color: 1
Size: 250946 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 475810 Color: 1
Size: 263560 Color: 0
Size: 260631 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 479008 Color: 1
Size: 265632 Color: 0
Size: 255361 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 414268 Color: 1
Size: 315617 Color: 0
Size: 270116 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 435600 Color: 1
Size: 305580 Color: 1
Size: 258821 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 378578 Color: 1
Size: 324407 Color: 1
Size: 297016 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 444416 Color: 1
Size: 301375 Color: 1
Size: 254210 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 417414 Color: 1
Size: 321678 Color: 1
Size: 260909 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 483833 Color: 1
Size: 263039 Color: 1
Size: 253129 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 382804 Color: 1
Size: 343006 Color: 1
Size: 274191 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 461737 Color: 1
Size: 285308 Color: 1
Size: 252956 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 438276 Color: 1
Size: 298613 Color: 1
Size: 263112 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 380152 Color: 1
Size: 354633 Color: 1
Size: 265216 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 399827 Color: 1
Size: 311595 Color: 1
Size: 288579 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 471262 Color: 1
Size: 278635 Color: 1
Size: 250104 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 375409 Color: 1
Size: 332487 Color: 1
Size: 292105 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 366837 Color: 1
Size: 331609 Color: 1
Size: 301555 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 451630 Color: 1
Size: 288779 Color: 1
Size: 259592 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 419668 Color: 1
Size: 319251 Color: 1
Size: 261082 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 465561 Color: 1
Size: 282195 Color: 1
Size: 252245 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 455140 Color: 1
Size: 280128 Color: 1
Size: 264733 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 496124 Color: 1
Size: 253468 Color: 1
Size: 250409 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 428298 Color: 1
Size: 309085 Color: 1
Size: 262618 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 418585 Color: 1
Size: 301898 Color: 1
Size: 279518 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 384247 Color: 1
Size: 317334 Color: 1
Size: 298420 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 396427 Color: 1
Size: 322538 Color: 1
Size: 281036 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 419120 Color: 1
Size: 291903 Color: 1
Size: 288978 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 445269 Color: 1
Size: 297137 Color: 1
Size: 257595 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 428654 Color: 1
Size: 313808 Color: 1
Size: 257539 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 417536 Color: 1
Size: 308629 Color: 1
Size: 273836 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 463749 Color: 1
Size: 282992 Color: 1
Size: 253260 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 411874 Color: 1
Size: 305250 Color: 1
Size: 282877 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 428353 Color: 1
Size: 307297 Color: 1
Size: 264351 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 349367 Color: 1
Size: 337649 Color: 1
Size: 312985 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 410639 Color: 1
Size: 302432 Color: 0
Size: 286930 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 458219 Color: 1
Size: 275788 Color: 0
Size: 265994 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 418545 Color: 1
Size: 318793 Color: 1
Size: 262663 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 419314 Color: 1
Size: 321044 Color: 1
Size: 259643 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 436716 Color: 1
Size: 306618 Color: 1
Size: 256667 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 404519 Color: 1
Size: 338474 Color: 1
Size: 257008 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 430768 Color: 1
Size: 294507 Color: 1
Size: 274726 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 412304 Color: 1
Size: 321675 Color: 1
Size: 266022 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 385240 Color: 1
Size: 333962 Color: 1
Size: 280799 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 439493 Color: 1
Size: 301619 Color: 1
Size: 258889 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 385977 Color: 1
Size: 341328 Color: 1
Size: 272696 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 432901 Color: 1
Size: 300084 Color: 1
Size: 267016 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 478744 Color: 1
Size: 266151 Color: 1
Size: 255106 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 432878 Color: 1
Size: 292878 Color: 1
Size: 274245 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 418133 Color: 1
Size: 310533 Color: 1
Size: 271335 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 435882 Color: 1
Size: 307055 Color: 1
Size: 257064 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 446465 Color: 1
Size: 277484 Color: 1
Size: 276052 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 442075 Color: 1
Size: 293585 Color: 1
Size: 264341 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 407853 Color: 1
Size: 328032 Color: 1
Size: 264116 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 450438 Color: 1
Size: 280895 Color: 1
Size: 268668 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 437349 Color: 1
Size: 290943 Color: 1
Size: 271709 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 399931 Color: 1
Size: 308125 Color: 0
Size: 291945 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 375206 Color: 1
Size: 346489 Color: 1
Size: 278306 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 438850 Color: 1
Size: 305900 Color: 1
Size: 255251 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 385449 Color: 1
Size: 311937 Color: 0
Size: 302615 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 469741 Color: 1
Size: 267281 Color: 0
Size: 262979 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 475931 Color: 1
Size: 265055 Color: 0
Size: 259015 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 490855 Color: 1
Size: 256496 Color: 0
Size: 252650 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 392554 Color: 1
Size: 323679 Color: 1
Size: 283768 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 440882 Color: 1
Size: 291297 Color: 1
Size: 267822 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 470619 Color: 1
Size: 269723 Color: 1
Size: 259659 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 441390 Color: 1
Size: 308222 Color: 1
Size: 250389 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 447345 Color: 1
Size: 292410 Color: 1
Size: 260246 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 427353 Color: 1
Size: 321243 Color: 1
Size: 251405 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 467002 Color: 1
Size: 277674 Color: 1
Size: 255325 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 428465 Color: 1
Size: 298029 Color: 1
Size: 273507 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 486036 Color: 1
Size: 258576 Color: 1
Size: 255389 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 404927 Color: 1
Size: 326369 Color: 1
Size: 268705 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 394071 Color: 1
Size: 345408 Color: 1
Size: 260522 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 378092 Color: 1
Size: 349364 Color: 1
Size: 272545 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 451827 Color: 1
Size: 286420 Color: 1
Size: 261754 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 439433 Color: 1
Size: 300856 Color: 1
Size: 259712 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 476737 Color: 1
Size: 271343 Color: 1
Size: 251921 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 473057 Color: 1
Size: 275235 Color: 1
Size: 251709 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 475062 Color: 1
Size: 270528 Color: 1
Size: 254411 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 482286 Color: 1
Size: 263425 Color: 1
Size: 254290 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 400799 Color: 1
Size: 315490 Color: 1
Size: 283712 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 365234 Color: 1
Size: 347823 Color: 1
Size: 286944 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 431686 Color: 1
Size: 300422 Color: 1
Size: 267893 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 391488 Color: 1
Size: 331310 Color: 1
Size: 277203 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 354457 Color: 1
Size: 326008 Color: 1
Size: 319536 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 383382 Color: 1
Size: 316626 Color: 0
Size: 299993 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 411008 Color: 1
Size: 329196 Color: 1
Size: 259797 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 445208 Color: 1
Size: 295607 Color: 1
Size: 259186 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 419601 Color: 1
Size: 319502 Color: 1
Size: 260898 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 406064 Color: 1
Size: 331564 Color: 1
Size: 262373 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 466756 Color: 1
Size: 269129 Color: 1
Size: 264116 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 383791 Color: 1
Size: 326187 Color: 1
Size: 290023 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 376905 Color: 1
Size: 325154 Color: 0
Size: 297942 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 441700 Color: 1
Size: 282432 Color: 1
Size: 275869 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 389784 Color: 1
Size: 327388 Color: 1
Size: 282829 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 413126 Color: 1
Size: 306227 Color: 1
Size: 280648 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 399130 Color: 1
Size: 327928 Color: 1
Size: 272943 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 403933 Color: 1
Size: 344502 Color: 1
Size: 251566 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 404903 Color: 1
Size: 336130 Color: 1
Size: 258968 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 475825 Color: 1
Size: 263535 Color: 0
Size: 260641 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 374384 Color: 1
Size: 332076 Color: 1
Size: 293541 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 367212 Color: 1
Size: 337892 Color: 1
Size: 294897 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 490270 Color: 1
Size: 259698 Color: 1
Size: 250033 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 419206 Color: 1
Size: 310881 Color: 1
Size: 269914 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 471213 Color: 1
Size: 265749 Color: 1
Size: 263039 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 493855 Color: 1
Size: 254896 Color: 1
Size: 251250 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 443403 Color: 1
Size: 305768 Color: 1
Size: 250830 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 379365 Color: 1
Size: 315298 Color: 1
Size: 305338 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 398045 Color: 1
Size: 334248 Color: 1
Size: 267708 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 488299 Color: 1
Size: 258596 Color: 1
Size: 253106 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 379211 Color: 1
Size: 340576 Color: 1
Size: 280214 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 405907 Color: 1
Size: 323761 Color: 1
Size: 270333 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 475716 Color: 1
Size: 268615 Color: 1
Size: 255670 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 481306 Color: 1
Size: 267023 Color: 1
Size: 251672 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 360594 Color: 1
Size: 344540 Color: 1
Size: 294867 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 402614 Color: 1
Size: 319700 Color: 1
Size: 277687 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 379179 Color: 1
Size: 316504 Color: 1
Size: 304318 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 499141 Color: 1
Size: 250538 Color: 0
Size: 250322 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 415583 Color: 1
Size: 304740 Color: 0
Size: 279678 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 375378 Color: 1
Size: 348650 Color: 1
Size: 275973 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 467550 Color: 1
Size: 278480 Color: 1
Size: 253971 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 403054 Color: 1
Size: 319040 Color: 1
Size: 277907 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 362022 Color: 1
Size: 356054 Color: 1
Size: 281925 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 379638 Color: 1
Size: 325706 Color: 1
Size: 294657 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 408679 Color: 1
Size: 335165 Color: 1
Size: 256157 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 420776 Color: 1
Size: 325782 Color: 1
Size: 253443 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 421619 Color: 1
Size: 313042 Color: 1
Size: 265340 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 476038 Color: 1
Size: 266540 Color: 1
Size: 257423 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 378672 Color: 1
Size: 337118 Color: 1
Size: 284211 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 488050 Color: 1
Size: 260518 Color: 1
Size: 251433 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 467972 Color: 1
Size: 268345 Color: 1
Size: 263684 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 424987 Color: 1
Size: 311944 Color: 1
Size: 263070 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 412123 Color: 1
Size: 322188 Color: 1
Size: 265690 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 485356 Color: 1
Size: 260614 Color: 1
Size: 254031 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 394486 Color: 1
Size: 340477 Color: 1
Size: 265038 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 386635 Color: 1
Size: 319591 Color: 1
Size: 293775 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 450448 Color: 1
Size: 298486 Color: 1
Size: 251067 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 398850 Color: 1
Size: 319447 Color: 1
Size: 281704 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 435230 Color: 1
Size: 291731 Color: 1
Size: 273040 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 397115 Color: 1
Size: 330841 Color: 1
Size: 272045 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 415498 Color: 1
Size: 300114 Color: 1
Size: 284389 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 413240 Color: 1
Size: 327981 Color: 1
Size: 258780 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 347180 Color: 1
Size: 346572 Color: 1
Size: 306249 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 469184 Color: 1
Size: 269350 Color: 1
Size: 261467 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 408265 Color: 1
Size: 296844 Color: 0
Size: 294892 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 383670 Color: 1
Size: 348912 Color: 1
Size: 267419 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 484723 Color: 1
Size: 265231 Color: 1
Size: 250047 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 492816 Color: 1
Size: 255508 Color: 1
Size: 251677 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 374943 Color: 1
Size: 358542 Color: 1
Size: 266516 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 397392 Color: 1
Size: 347160 Color: 1
Size: 255449 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 374977 Color: 1
Size: 358620 Color: 1
Size: 266404 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 439573 Color: 1
Size: 299356 Color: 1
Size: 261072 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 459363 Color: 1
Size: 290409 Color: 1
Size: 250229 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 386679 Color: 1
Size: 337073 Color: 1
Size: 276249 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 406914 Color: 1
Size: 299557 Color: 0
Size: 293530 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 395295 Color: 1
Size: 330651 Color: 1
Size: 274055 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 360128 Color: 1
Size: 327641 Color: 1
Size: 312232 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 379848 Color: 1
Size: 359485 Color: 1
Size: 260668 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 454448 Color: 1
Size: 279657 Color: 1
Size: 265896 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 405626 Color: 1
Size: 304847 Color: 0
Size: 289528 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 449700 Color: 1
Size: 292597 Color: 1
Size: 257704 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 361164 Color: 1
Size: 322968 Color: 1
Size: 315869 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 475507 Color: 1
Size: 272216 Color: 1
Size: 252278 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 399618 Color: 1
Size: 316281 Color: 1
Size: 284102 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 423788 Color: 1
Size: 301036 Color: 1
Size: 275177 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 412519 Color: 1
Size: 303257 Color: 1
Size: 284225 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 375762 Color: 1
Size: 321923 Color: 1
Size: 302316 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 378181 Color: 1
Size: 352137 Color: 1
Size: 269683 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 405810 Color: 1
Size: 302107 Color: 1
Size: 292084 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 491874 Color: 1
Size: 257996 Color: 1
Size: 250131 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 447143 Color: 1
Size: 298399 Color: 1
Size: 254459 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 397507 Color: 1
Size: 317025 Color: 1
Size: 285469 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 468942 Color: 1
Size: 271253 Color: 1
Size: 259806 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 483213 Color: 1
Size: 261852 Color: 1
Size: 254936 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 436355 Color: 1
Size: 300658 Color: 1
Size: 262988 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 426013 Color: 1
Size: 313555 Color: 1
Size: 260433 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 357290 Color: 1
Size: 331714 Color: 1
Size: 310997 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 365250 Color: 1
Size: 341097 Color: 1
Size: 293654 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 441505 Color: 1
Size: 299839 Color: 1
Size: 258657 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 424243 Color: 1
Size: 305781 Color: 1
Size: 269977 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 453779 Color: 1
Size: 291331 Color: 1
Size: 254891 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 478647 Color: 1
Size: 268076 Color: 1
Size: 253278 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 387186 Color: 1
Size: 327304 Color: 1
Size: 285511 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 407824 Color: 1
Size: 336308 Color: 1
Size: 255869 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 391260 Color: 1
Size: 318297 Color: 1
Size: 290444 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 395184 Color: 1
Size: 327033 Color: 1
Size: 277784 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 447401 Color: 1
Size: 286287 Color: 1
Size: 266313 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 499678 Color: 1
Size: 250222 Color: 1
Size: 250101 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 371491 Color: 1
Size: 348376 Color: 1
Size: 280134 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 406753 Color: 1
Size: 296852 Color: 0
Size: 296396 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 497235 Color: 1
Size: 252127 Color: 1
Size: 250639 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 435447 Color: 1
Size: 306824 Color: 1
Size: 257730 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 462294 Color: 1
Size: 283036 Color: 1
Size: 254671 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 357437 Color: 1
Size: 326459 Color: 1
Size: 316105 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 409592 Color: 1
Size: 330721 Color: 1
Size: 259688 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 387849 Color: 1
Size: 309567 Color: 1
Size: 302585 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 376700 Color: 1
Size: 355590 Color: 1
Size: 267711 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 416936 Color: 1
Size: 313396 Color: 1
Size: 269669 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 432153 Color: 1
Size: 298774 Color: 1
Size: 269074 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 461664 Color: 1
Size: 277546 Color: 1
Size: 260791 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 438781 Color: 1
Size: 300536 Color: 1
Size: 260684 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 443432 Color: 1
Size: 283367 Color: 1
Size: 273202 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 457835 Color: 1
Size: 271490 Color: 1
Size: 270676 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 418384 Color: 1
Size: 293526 Color: 0
Size: 288091 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 376541 Color: 1
Size: 343443 Color: 1
Size: 280017 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 353740 Color: 1
Size: 332132 Color: 1
Size: 314129 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 454131 Color: 1
Size: 284078 Color: 1
Size: 261792 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 461586 Color: 1
Size: 269331 Color: 0
Size: 269084 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 358839 Color: 1
Size: 320664 Color: 0
Size: 320498 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 459384 Color: 1
Size: 281749 Color: 1
Size: 258868 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 472067 Color: 1
Size: 277081 Color: 1
Size: 250853 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 473307 Color: 1
Size: 275867 Color: 1
Size: 250827 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 411790 Color: 1
Size: 329327 Color: 1
Size: 258884 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 438750 Color: 1
Size: 294514 Color: 1
Size: 266737 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 408496 Color: 1
Size: 336485 Color: 1
Size: 255020 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 407651 Color: 1
Size: 339960 Color: 1
Size: 252390 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 478288 Color: 1
Size: 266502 Color: 1
Size: 255211 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 409128 Color: 1
Size: 307708 Color: 1
Size: 283165 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 411339 Color: 1
Size: 321975 Color: 1
Size: 266687 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 417251 Color: 1
Size: 313031 Color: 1
Size: 269719 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 437239 Color: 1
Size: 311019 Color: 1
Size: 251743 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 447972 Color: 1
Size: 298936 Color: 1
Size: 253093 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 388722 Color: 1
Size: 335360 Color: 1
Size: 275919 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 497591 Color: 1
Size: 251889 Color: 1
Size: 250521 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 419471 Color: 1
Size: 299107 Color: 1
Size: 281423 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 450339 Color: 1
Size: 278034 Color: 0
Size: 271628 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 474102 Color: 1
Size: 274477 Color: 1
Size: 251422 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 430750 Color: 1
Size: 319240 Color: 1
Size: 250011 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 377080 Color: 1
Size: 328755 Color: 0
Size: 294166 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 468892 Color: 1
Size: 267577 Color: 1
Size: 263532 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 424850 Color: 1
Size: 308820 Color: 1
Size: 266331 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 369352 Color: 1
Size: 350722 Color: 1
Size: 279927 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 440961 Color: 1
Size: 280406 Color: 1
Size: 278634 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 423737 Color: 1
Size: 297209 Color: 1
Size: 279055 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 461108 Color: 1
Size: 288609 Color: 1
Size: 250284 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 485296 Color: 1
Size: 262807 Color: 1
Size: 251898 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 486798 Color: 1
Size: 261639 Color: 1
Size: 251564 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 413873 Color: 1
Size: 326236 Color: 1
Size: 259892 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 455349 Color: 1
Size: 293767 Color: 1
Size: 250885 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 431511 Color: 1
Size: 312875 Color: 1
Size: 255615 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 488874 Color: 1
Size: 258743 Color: 1
Size: 252384 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 474867 Color: 1
Size: 270317 Color: 1
Size: 254817 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 434989 Color: 1
Size: 299329 Color: 1
Size: 265683 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 352342 Color: 1
Size: 331687 Color: 1
Size: 315972 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 465289 Color: 1
Size: 282797 Color: 1
Size: 251915 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 403093 Color: 1
Size: 325566 Color: 1
Size: 271342 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 372810 Color: 1
Size: 328159 Color: 1
Size: 299032 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 483461 Color: 1
Size: 263865 Color: 1
Size: 252675 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 471144 Color: 1
Size: 278046 Color: 1
Size: 250811 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 352813 Color: 1
Size: 328166 Color: 1
Size: 319022 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 443767 Color: 1
Size: 303307 Color: 1
Size: 252927 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 370103 Color: 1
Size: 346900 Color: 1
Size: 282998 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 373720 Color: 1
Size: 337246 Color: 1
Size: 289035 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 380739 Color: 1
Size: 318434 Color: 1
Size: 300828 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 409098 Color: 1
Size: 301189 Color: 1
Size: 289714 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 405283 Color: 1
Size: 321393 Color: 1
Size: 273325 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 481688 Color: 1
Size: 266135 Color: 1
Size: 252178 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 377411 Color: 1
Size: 342883 Color: 1
Size: 279707 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 404706 Color: 1
Size: 340560 Color: 1
Size: 254735 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 421289 Color: 1
Size: 297111 Color: 0
Size: 281601 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 449149 Color: 1
Size: 295137 Color: 1
Size: 255715 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 363844 Color: 1
Size: 344766 Color: 1
Size: 291391 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 422670 Color: 1
Size: 292264 Color: 1
Size: 285067 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 448046 Color: 1
Size: 298508 Color: 1
Size: 253447 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 447714 Color: 1
Size: 287114 Color: 0
Size: 265173 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 458298 Color: 1
Size: 284262 Color: 1
Size: 257441 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 489396 Color: 1
Size: 259874 Color: 1
Size: 250731 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 477469 Color: 1
Size: 271786 Color: 1
Size: 250746 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 484592 Color: 1
Size: 259942 Color: 1
Size: 255467 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 349283 Color: 1
Size: 334399 Color: 1
Size: 316319 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 351407 Color: 1
Size: 340123 Color: 1
Size: 308471 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 353650 Color: 1
Size: 336053 Color: 1
Size: 310298 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 490637 Color: 1
Size: 256410 Color: 1
Size: 252954 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 399302 Color: 1
Size: 330813 Color: 1
Size: 269886 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 401329 Color: 1
Size: 323979 Color: 1
Size: 274693 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 467256 Color: 1
Size: 282215 Color: 1
Size: 250530 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 351903 Color: 1
Size: 344213 Color: 1
Size: 303885 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 423827 Color: 1
Size: 311266 Color: 1
Size: 264908 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 426132 Color: 1
Size: 305908 Color: 1
Size: 267961 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 426515 Color: 1
Size: 316101 Color: 1
Size: 257385 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 453114 Color: 1
Size: 287767 Color: 1
Size: 259120 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 421587 Color: 1
Size: 318740 Color: 1
Size: 259674 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 479778 Color: 1
Size: 270144 Color: 1
Size: 250079 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 426006 Color: 1
Size: 288935 Color: 0
Size: 285060 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 401853 Color: 1
Size: 309455 Color: 1
Size: 288693 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 424109 Color: 1
Size: 308532 Color: 1
Size: 267360 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 436386 Color: 1
Size: 295635 Color: 1
Size: 267980 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 442668 Color: 1
Size: 294412 Color: 1
Size: 262921 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 463316 Color: 1
Size: 275443 Color: 1
Size: 261242 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 429031 Color: 1
Size: 307512 Color: 1
Size: 263458 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 477341 Color: 1
Size: 262433 Color: 1
Size: 260227 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 481088 Color: 1
Size: 266081 Color: 1
Size: 252832 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 366522 Color: 1
Size: 318347 Color: 1
Size: 315132 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 443911 Color: 1
Size: 300468 Color: 1
Size: 255622 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 455078 Color: 1
Size: 289263 Color: 1
Size: 255660 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 356670 Color: 1
Size: 337149 Color: 1
Size: 306182 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 403381 Color: 1
Size: 337487 Color: 1
Size: 259133 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 446823 Color: 1
Size: 289514 Color: 1
Size: 263664 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 420736 Color: 1
Size: 322076 Color: 1
Size: 257189 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 358624 Color: 1
Size: 357354 Color: 1
Size: 284023 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 429373 Color: 1
Size: 287370 Color: 0
Size: 283258 Color: 1

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 472223 Color: 1
Size: 270427 Color: 1
Size: 257351 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 375770 Color: 1
Size: 346574 Color: 1
Size: 277657 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 370061 Color: 1
Size: 323410 Color: 1
Size: 306530 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 469447 Color: 1
Size: 266194 Color: 1
Size: 264360 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 369814 Color: 1
Size: 344312 Color: 1
Size: 285875 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 371516 Color: 1
Size: 317340 Color: 0
Size: 311145 Color: 1

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 489124 Color: 1
Size: 257582 Color: 0
Size: 253295 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 383122 Color: 1
Size: 348960 Color: 1
Size: 267919 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 451846 Color: 1
Size: 294957 Color: 1
Size: 253198 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 430354 Color: 1
Size: 305328 Color: 1
Size: 264319 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 391061 Color: 1
Size: 321788 Color: 1
Size: 287152 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 467483 Color: 1
Size: 271120 Color: 1
Size: 261398 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 408264 Color: 1
Size: 328648 Color: 1
Size: 263089 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 351884 Color: 1
Size: 327646 Color: 1
Size: 320471 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 417496 Color: 1
Size: 311103 Color: 1
Size: 271402 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 402116 Color: 1
Size: 310691 Color: 1
Size: 287194 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 437074 Color: 1
Size: 291636 Color: 0
Size: 271291 Color: 1

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 363233 Color: 1
Size: 343972 Color: 1
Size: 292796 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 391593 Color: 1
Size: 342591 Color: 1
Size: 265817 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 385817 Color: 1
Size: 330414 Color: 1
Size: 283770 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 376458 Color: 1
Size: 346857 Color: 1
Size: 276686 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 499107 Color: 1
Size: 250605 Color: 1
Size: 250289 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 424662 Color: 1
Size: 317762 Color: 1
Size: 257577 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 380886 Color: 1
Size: 311577 Color: 0
Size: 307538 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 353057 Color: 1
Size: 343347 Color: 1
Size: 303597 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 380732 Color: 1
Size: 336504 Color: 1
Size: 282765 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 461719 Color: 1
Size: 275513 Color: 1
Size: 262769 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 456798 Color: 1
Size: 282374 Color: 1
Size: 260829 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 401258 Color: 1
Size: 328396 Color: 1
Size: 270347 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 368783 Color: 1
Size: 327126 Color: 1
Size: 304092 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 430414 Color: 1
Size: 302991 Color: 1
Size: 266596 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 497356 Color: 1
Size: 252251 Color: 1
Size: 250394 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 381331 Color: 1
Size: 312727 Color: 1
Size: 305943 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 457643 Color: 1
Size: 290494 Color: 1
Size: 251864 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 397258 Color: 1
Size: 313938 Color: 1
Size: 288805 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 449110 Color: 1
Size: 291116 Color: 1
Size: 259775 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 359733 Color: 1
Size: 358393 Color: 1
Size: 281875 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 415526 Color: 1
Size: 316385 Color: 1
Size: 268090 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 476427 Color: 1
Size: 266809 Color: 1
Size: 256765 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 430931 Color: 1
Size: 297256 Color: 1
Size: 271814 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 394214 Color: 1
Size: 337152 Color: 1
Size: 268635 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 415660 Color: 1
Size: 298328 Color: 1
Size: 286013 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 493989 Color: 1
Size: 254989 Color: 1
Size: 251023 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 413786 Color: 1
Size: 309510 Color: 1
Size: 276705 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 446510 Color: 1
Size: 297811 Color: 1
Size: 255680 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 351135 Color: 1
Size: 340532 Color: 1
Size: 308334 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 450722 Color: 1
Size: 284947 Color: 1
Size: 264332 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 471620 Color: 1
Size: 277865 Color: 1
Size: 250516 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 392363 Color: 1
Size: 316025 Color: 1
Size: 291613 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 437715 Color: 1
Size: 302190 Color: 1
Size: 260096 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 487183 Color: 1
Size: 261468 Color: 1
Size: 251350 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 489340 Color: 1
Size: 258581 Color: 1
Size: 252080 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 425478 Color: 1
Size: 305256 Color: 0
Size: 269267 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 426546 Color: 1
Size: 304542 Color: 1
Size: 268913 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 394427 Color: 1
Size: 332175 Color: 1
Size: 273399 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 411831 Color: 1
Size: 309054 Color: 1
Size: 279116 Color: 0

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 377495 Color: 1
Size: 342436 Color: 1
Size: 280070 Color: 0

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 457385 Color: 1
Size: 291239 Color: 1
Size: 251377 Color: 0

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 409038 Color: 1
Size: 315609 Color: 1
Size: 275354 Color: 0

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 456680 Color: 1
Size: 284038 Color: 1
Size: 259283 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 383706 Color: 1
Size: 345191 Color: 1
Size: 271104 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 474065 Color: 1
Size: 269910 Color: 1
Size: 256026 Color: 0

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 377086 Color: 1
Size: 343378 Color: 1
Size: 279537 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 415476 Color: 1
Size: 322133 Color: 1
Size: 262392 Color: 0

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 382987 Color: 1
Size: 332846 Color: 1
Size: 284168 Color: 0

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 380956 Color: 1
Size: 341705 Color: 1
Size: 277340 Color: 0

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 465923 Color: 1
Size: 272256 Color: 1
Size: 261822 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 405865 Color: 1
Size: 320603 Color: 1
Size: 273533 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 349412 Color: 1
Size: 348021 Color: 1
Size: 302568 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 374430 Color: 1
Size: 361540 Color: 1
Size: 264031 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 453850 Color: 1
Size: 290746 Color: 1
Size: 255405 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 419150 Color: 1
Size: 296887 Color: 1
Size: 283964 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 415736 Color: 1
Size: 299601 Color: 0
Size: 284664 Color: 1

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 372967 Color: 1
Size: 327116 Color: 1
Size: 299918 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 387211 Color: 1
Size: 335470 Color: 1
Size: 277320 Color: 0

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 370696 Color: 1
Size: 350489 Color: 1
Size: 278816 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 400036 Color: 1
Size: 323040 Color: 1
Size: 276925 Color: 0

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 467438 Color: 1
Size: 276042 Color: 1
Size: 256521 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 392898 Color: 1
Size: 344282 Color: 1
Size: 262821 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 366521 Color: 1
Size: 342100 Color: 1
Size: 291380 Color: 0

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 477374 Color: 1
Size: 265039 Color: 0
Size: 257588 Color: 1

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 478168 Color: 1
Size: 264267 Color: 1
Size: 257566 Color: 0

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 453932 Color: 1
Size: 273326 Color: 1
Size: 272743 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 441747 Color: 1
Size: 286363 Color: 1
Size: 271891 Color: 0

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 393638 Color: 1
Size: 332768 Color: 1
Size: 273595 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 432403 Color: 1
Size: 292707 Color: 1
Size: 274891 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 380860 Color: 1
Size: 335363 Color: 1
Size: 283778 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 377980 Color: 1
Size: 340743 Color: 1
Size: 281278 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 396250 Color: 1
Size: 316734 Color: 1
Size: 287017 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 403598 Color: 1
Size: 333118 Color: 1
Size: 263285 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 422925 Color: 1
Size: 305391 Color: 1
Size: 271685 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 392744 Color: 1
Size: 318134 Color: 1
Size: 289123 Color: 0

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 487511 Color: 1
Size: 257950 Color: 1
Size: 254540 Color: 0

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 499039 Color: 1
Size: 250940 Color: 1
Size: 250022 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 403414 Color: 1
Size: 308756 Color: 1
Size: 287831 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 406101 Color: 1
Size: 329957 Color: 1
Size: 263943 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 412847 Color: 1
Size: 329345 Color: 1
Size: 257809 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 431521 Color: 1
Size: 315151 Color: 1
Size: 253329 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 428140 Color: 1
Size: 306487 Color: 1
Size: 265374 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 378534 Color: 1
Size: 332918 Color: 1
Size: 288549 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 422275 Color: 1
Size: 295458 Color: 0
Size: 282268 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 366865 Color: 1
Size: 357366 Color: 1
Size: 275770 Color: 0

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 402158 Color: 1
Size: 324600 Color: 1
Size: 273243 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 467893 Color: 1
Size: 277711 Color: 1
Size: 254397 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 493425 Color: 1
Size: 254521 Color: 0
Size: 252055 Color: 1

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 442701 Color: 1
Size: 285292 Color: 1
Size: 272008 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 388930 Color: 1
Size: 317778 Color: 1
Size: 293293 Color: 0

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 378705 Color: 1
Size: 321286 Color: 0
Size: 300010 Color: 1

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 387154 Color: 1
Size: 309428 Color: 1
Size: 303419 Color: 0

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 443552 Color: 1
Size: 292528 Color: 1
Size: 263921 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 450751 Color: 1
Size: 280675 Color: 1
Size: 268575 Color: 0

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 371373 Color: 1
Size: 342674 Color: 1
Size: 285954 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 377882 Color: 1
Size: 345932 Color: 1
Size: 276187 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 470006 Color: 1
Size: 268735 Color: 1
Size: 261260 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 444823 Color: 1
Size: 297279 Color: 1
Size: 257899 Color: 0

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 450355 Color: 1
Size: 298460 Color: 1
Size: 251186 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 369294 Color: 1
Size: 363743 Color: 1
Size: 266964 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 362495 Color: 1
Size: 335596 Color: 1
Size: 301910 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 491562 Color: 1
Size: 256829 Color: 1
Size: 251610 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 423999 Color: 1
Size: 308082 Color: 1
Size: 267920 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 474518 Color: 1
Size: 268320 Color: 1
Size: 257163 Color: 0

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 374688 Color: 1
Size: 365883 Color: 1
Size: 259430 Color: 0

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 469761 Color: 1
Size: 265737 Color: 1
Size: 264503 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 350381 Color: 1
Size: 345108 Color: 1
Size: 304512 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 381045 Color: 1
Size: 342933 Color: 1
Size: 276023 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 413822 Color: 1
Size: 326781 Color: 1
Size: 259398 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 377627 Color: 1
Size: 321391 Color: 1
Size: 300983 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 443138 Color: 1
Size: 296674 Color: 1
Size: 260189 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 437730 Color: 1
Size: 302113 Color: 1
Size: 260158 Color: 0

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 413145 Color: 1
Size: 317906 Color: 1
Size: 268950 Color: 0

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 434769 Color: 1
Size: 290340 Color: 1
Size: 274892 Color: 0

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 464743 Color: 1
Size: 282600 Color: 1
Size: 252658 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 355146 Color: 1
Size: 350235 Color: 1
Size: 294620 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 489213 Color: 1
Size: 258583 Color: 1
Size: 252205 Color: 0

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 405757 Color: 1
Size: 326089 Color: 1
Size: 268155 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 425403 Color: 1
Size: 322140 Color: 1
Size: 252458 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 379933 Color: 1
Size: 317308 Color: 1
Size: 302760 Color: 0

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 489597 Color: 1
Size: 255397 Color: 1
Size: 255007 Color: 0

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 423675 Color: 1
Size: 295121 Color: 0
Size: 281205 Color: 1

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 442012 Color: 1
Size: 303244 Color: 1
Size: 254745 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 455819 Color: 1
Size: 287005 Color: 1
Size: 257177 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 383248 Color: 1
Size: 352210 Color: 1
Size: 264543 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 392292 Color: 1
Size: 324381 Color: 0
Size: 283328 Color: 1

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 421604 Color: 1
Size: 313662 Color: 1
Size: 264735 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 400373 Color: 1
Size: 331083 Color: 1
Size: 268545 Color: 0

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 416656 Color: 1
Size: 314887 Color: 1
Size: 268458 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 432582 Color: 1
Size: 302682 Color: 1
Size: 264737 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 499506 Color: 1
Size: 250374 Color: 1
Size: 250121 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 425090 Color: 1
Size: 322018 Color: 1
Size: 252893 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 381767 Color: 1
Size: 318125 Color: 1
Size: 300109 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 423429 Color: 1
Size: 313217 Color: 1
Size: 263355 Color: 0

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 381735 Color: 1
Size: 331777 Color: 1
Size: 286489 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 450226 Color: 1
Size: 296736 Color: 1
Size: 253039 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 367290 Color: 1
Size: 335548 Color: 1
Size: 297163 Color: 0

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 460679 Color: 1
Size: 281883 Color: 1
Size: 257439 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 401085 Color: 1
Size: 314076 Color: 1
Size: 284840 Color: 0

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 349634 Color: 1
Size: 345589 Color: 1
Size: 304778 Color: 0

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 461257 Color: 1
Size: 278993 Color: 1
Size: 259751 Color: 0

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 432544 Color: 1
Size: 296715 Color: 1
Size: 270742 Color: 0

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 367564 Color: 1
Size: 321405 Color: 1
Size: 311032 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 391530 Color: 1
Size: 341269 Color: 1
Size: 267202 Color: 0

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 408885 Color: 1
Size: 321701 Color: 1
Size: 269415 Color: 0

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 359890 Color: 1
Size: 322322 Color: 1
Size: 317789 Color: 0

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 420288 Color: 1
Size: 327115 Color: 1
Size: 252598 Color: 0

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 372490 Color: 1
Size: 338949 Color: 1
Size: 288562 Color: 0

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 463310 Color: 1
Size: 271598 Color: 1
Size: 265093 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 376706 Color: 1
Size: 343936 Color: 1
Size: 279359 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 407741 Color: 1
Size: 317535 Color: 1
Size: 274725 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 397784 Color: 1
Size: 337276 Color: 1
Size: 264941 Color: 0

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 488328 Color: 1
Size: 260773 Color: 1
Size: 250900 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 446155 Color: 1
Size: 286717 Color: 1
Size: 267129 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 479926 Color: 1
Size: 264145 Color: 1
Size: 255930 Color: 0

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 480271 Color: 1
Size: 264786 Color: 1
Size: 254944 Color: 0

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 445662 Color: 1
Size: 301874 Color: 1
Size: 252465 Color: 0

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 492655 Color: 1
Size: 256665 Color: 1
Size: 250681 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 438100 Color: 1
Size: 297718 Color: 1
Size: 264183 Color: 0

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 485437 Color: 1
Size: 258220 Color: 1
Size: 256344 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 430775 Color: 1
Size: 314674 Color: 1
Size: 254552 Color: 0

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 419438 Color: 1
Size: 292971 Color: 1
Size: 287592 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 471994 Color: 1
Size: 268368 Color: 1
Size: 259639 Color: 0

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 460631 Color: 1
Size: 282809 Color: 1
Size: 256561 Color: 0

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 353811 Color: 1
Size: 331984 Color: 1
Size: 314206 Color: 0

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 495844 Color: 1
Size: 253984 Color: 1
Size: 250173 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 385949 Color: 1
Size: 360931 Color: 1
Size: 253121 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 365541 Color: 1
Size: 360621 Color: 1
Size: 273839 Color: 0

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 450732 Color: 1
Size: 282166 Color: 1
Size: 267103 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 490071 Color: 1
Size: 258226 Color: 1
Size: 251704 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 427482 Color: 1
Size: 303728 Color: 1
Size: 268791 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 404821 Color: 1
Size: 305467 Color: 0
Size: 289713 Color: 1

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 377027 Color: 1
Size: 357488 Color: 1
Size: 265486 Color: 0

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 490923 Color: 1
Size: 257189 Color: 1
Size: 251889 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 479792 Color: 1
Size: 265432 Color: 1
Size: 254777 Color: 0

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 464975 Color: 1
Size: 282961 Color: 1
Size: 252065 Color: 0

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 354627 Color: 1
Size: 329120 Color: 1
Size: 316254 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 364792 Color: 1
Size: 360810 Color: 1
Size: 274399 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 485592 Color: 1
Size: 262179 Color: 1
Size: 252230 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 385399 Color: 1
Size: 337598 Color: 1
Size: 277004 Color: 0

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 364609 Color: 1
Size: 340086 Color: 1
Size: 295306 Color: 0

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 449214 Color: 1
Size: 292014 Color: 1
Size: 258773 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 476468 Color: 1
Size: 273164 Color: 1
Size: 250369 Color: 0

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 451133 Color: 1
Size: 286290 Color: 1
Size: 262578 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 357781 Color: 1
Size: 323328 Color: 0
Size: 318892 Color: 1

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 373957 Color: 1
Size: 328019 Color: 1
Size: 298025 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 473790 Color: 1
Size: 275483 Color: 1
Size: 250728 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 451398 Color: 1
Size: 291470 Color: 1
Size: 257133 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 369784 Color: 1
Size: 324036 Color: 1
Size: 306181 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 414983 Color: 1
Size: 319686 Color: 1
Size: 265332 Color: 0

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 392457 Color: 1
Size: 337849 Color: 1
Size: 269695 Color: 0

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 483792 Color: 1
Size: 263709 Color: 1
Size: 252500 Color: 0

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 408419 Color: 1
Size: 334115 Color: 1
Size: 257467 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 350719 Color: 1
Size: 345123 Color: 1
Size: 304159 Color: 0

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 446355 Color: 1
Size: 301160 Color: 1
Size: 252486 Color: 0

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 481493 Color: 1
Size: 268203 Color: 1
Size: 250305 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 437422 Color: 1
Size: 295269 Color: 1
Size: 267310 Color: 0

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 486478 Color: 1
Size: 262790 Color: 1
Size: 250733 Color: 0

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 455599 Color: 1
Size: 285697 Color: 1
Size: 258705 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 451417 Color: 1
Size: 291661 Color: 1
Size: 256923 Color: 0

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 413800 Color: 1
Size: 305015 Color: 1
Size: 281186 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 365274 Color: 1
Size: 320823 Color: 0
Size: 313904 Color: 1

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 465816 Color: 1
Size: 274217 Color: 1
Size: 259968 Color: 0

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 365568 Color: 1
Size: 342620 Color: 1
Size: 291813 Color: 0

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 461809 Color: 1
Size: 275495 Color: 1
Size: 262697 Color: 0

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 372312 Color: 1
Size: 340438 Color: 1
Size: 287251 Color: 0

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 407967 Color: 1
Size: 296644 Color: 0
Size: 295390 Color: 1

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 479317 Color: 1
Size: 264491 Color: 1
Size: 256193 Color: 0

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 491623 Color: 1
Size: 257463 Color: 0
Size: 250915 Color: 1

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 369109 Color: 1
Size: 350797 Color: 1
Size: 280095 Color: 0

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 486874 Color: 1
Size: 261337 Color: 1
Size: 251790 Color: 0

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 408095 Color: 1
Size: 339035 Color: 1
Size: 252871 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 393736 Color: 1
Size: 310465 Color: 0
Size: 295800 Color: 1

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 436099 Color: 1
Size: 298297 Color: 1
Size: 265605 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 432698 Color: 1
Size: 310371 Color: 1
Size: 256932 Color: 0

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 349977 Color: 1
Size: 339390 Color: 1
Size: 310634 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 419670 Color: 1
Size: 311752 Color: 0
Size: 268579 Color: 1

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 453590 Color: 1
Size: 293454 Color: 1
Size: 252957 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 405007 Color: 1
Size: 308751 Color: 1
Size: 286243 Color: 0

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 462887 Color: 1
Size: 275312 Color: 1
Size: 261802 Color: 0

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 385395 Color: 1
Size: 358202 Color: 1
Size: 256404 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 491527 Color: 1
Size: 255760 Color: 1
Size: 252714 Color: 0

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 398220 Color: 1
Size: 304565 Color: 1
Size: 297216 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 471691 Color: 1
Size: 275228 Color: 1
Size: 253082 Color: 0

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 346721 Color: 1
Size: 340870 Color: 1
Size: 312410 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 481551 Color: 1
Size: 267827 Color: 1
Size: 250623 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 472928 Color: 1
Size: 273788 Color: 1
Size: 253285 Color: 0

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 429159 Color: 1
Size: 310990 Color: 1
Size: 259852 Color: 0

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 356183 Color: 1
Size: 355975 Color: 1
Size: 287843 Color: 0

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 410775 Color: 1
Size: 316947 Color: 0
Size: 272279 Color: 1

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 380374 Color: 1
Size: 332848 Color: 1
Size: 286779 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 447252 Color: 1
Size: 296862 Color: 1
Size: 255887 Color: 0

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 467300 Color: 1
Size: 267957 Color: 1
Size: 264744 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 412676 Color: 1
Size: 315324 Color: 0
Size: 272001 Color: 1

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 479724 Color: 1
Size: 262070 Color: 1
Size: 258207 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 365856 Color: 1
Size: 352259 Color: 1
Size: 281886 Color: 0

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 448627 Color: 1
Size: 286400 Color: 1
Size: 264974 Color: 0

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 357601 Color: 1
Size: 321620 Color: 1
Size: 320780 Color: 0

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 464546 Color: 1
Size: 278256 Color: 1
Size: 257199 Color: 0

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 399999 Color: 1
Size: 329945 Color: 1
Size: 270057 Color: 0

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 484306 Color: 1
Size: 261931 Color: 1
Size: 253764 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 364813 Color: 1
Size: 357670 Color: 1
Size: 277518 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 433356 Color: 1
Size: 300093 Color: 1
Size: 266552 Color: 0

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 487726 Color: 1
Size: 261690 Color: 1
Size: 250585 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 436857 Color: 1
Size: 285623 Color: 1
Size: 277521 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 467826 Color: 1
Size: 278070 Color: 1
Size: 254105 Color: 0

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 357170 Color: 1
Size: 341484 Color: 1
Size: 301347 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 394774 Color: 1
Size: 335132 Color: 1
Size: 270095 Color: 0

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 364526 Color: 1
Size: 361569 Color: 1
Size: 273906 Color: 0

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 375736 Color: 1
Size: 324396 Color: 1
Size: 299869 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 361300 Color: 1
Size: 323898 Color: 1
Size: 314803 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 361547 Color: 1
Size: 326824 Color: 1
Size: 311630 Color: 0

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 397180 Color: 1
Size: 344933 Color: 1
Size: 257888 Color: 0

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 364646 Color: 1
Size: 318366 Color: 0
Size: 316989 Color: 1

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 360781 Color: 1
Size: 357410 Color: 1
Size: 281810 Color: 0

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 361782 Color: 1
Size: 326466 Color: 1
Size: 311753 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 354262 Color: 1
Size: 342049 Color: 1
Size: 303690 Color: 0

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 366594 Color: 1
Size: 337297 Color: 1
Size: 296110 Color: 0

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 362118 Color: 1
Size: 325021 Color: 1
Size: 312862 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 363400 Color: 1
Size: 342176 Color: 1
Size: 294425 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 363236 Color: 1
Size: 326106 Color: 1
Size: 310659 Color: 0

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 364465 Color: 1
Size: 320226 Color: 0
Size: 315310 Color: 1

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 350536 Color: 1
Size: 347251 Color: 1
Size: 302214 Color: 0

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 498036 Color: 1
Size: 251699 Color: 1
Size: 250266 Color: 0

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 388671 Color: 1
Size: 349775 Color: 1
Size: 261555 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 392638 Color: 1
Size: 308480 Color: 1
Size: 298883 Color: 0

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 361803 Color: 1
Size: 344343 Color: 1
Size: 293855 Color: 0

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 395808 Color: 1
Size: 326224 Color: 1
Size: 277969 Color: 0

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 356177 Color: 1
Size: 323516 Color: 0
Size: 320308 Color: 1

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 351721 Color: 1
Size: 335794 Color: 1
Size: 312486 Color: 0

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 364287 Color: 1
Size: 320889 Color: 1
Size: 314825 Color: 0

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 355742 Color: 1
Size: 323650 Color: 1
Size: 320609 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 360784 Color: 1
Size: 320384 Color: 0
Size: 318833 Color: 1

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 483995 Color: 1
Size: 262772 Color: 1
Size: 253234 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 361169 Color: 1
Size: 324176 Color: 0
Size: 314656 Color: 1

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 347087 Color: 1
Size: 337022 Color: 1
Size: 315892 Color: 0

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 355423 Color: 1
Size: 341624 Color: 1
Size: 302954 Color: 0

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 356078 Color: 1
Size: 333383 Color: 1
Size: 310540 Color: 0

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 344821 Color: 1
Size: 344033 Color: 1
Size: 311147 Color: 0

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 373018 Color: 1
Size: 346534 Color: 1
Size: 280449 Color: 0

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 354911 Color: 1
Size: 345740 Color: 1
Size: 299350 Color: 0

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 347216 Color: 1
Size: 329155 Color: 1
Size: 323630 Color: 0

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 359538 Color: 1
Size: 337081 Color: 1
Size: 303382 Color: 0

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 352968 Color: 1
Size: 342084 Color: 1
Size: 304949 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 360204 Color: 1
Size: 323652 Color: 1
Size: 316145 Color: 0

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 381131 Color: 1
Size: 346548 Color: 1
Size: 272322 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 376825 Color: 1
Size: 318650 Color: 1
Size: 304526 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 360319 Color: 1
Size: 326630 Color: 0
Size: 313052 Color: 1

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 360581 Color: 1
Size: 358236 Color: 1
Size: 281184 Color: 0

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 358232 Color: 1
Size: 346612 Color: 1
Size: 295157 Color: 0

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 418385 Color: 1
Size: 294753 Color: 0
Size: 286863 Color: 1

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 360667 Color: 1
Size: 343478 Color: 1
Size: 295856 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 381536 Color: 1
Size: 355184 Color: 1
Size: 263281 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 377537 Color: 1
Size: 332263 Color: 1
Size: 290201 Color: 0

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 358655 Color: 1
Size: 354997 Color: 1
Size: 286349 Color: 0

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 364127 Color: 1
Size: 359741 Color: 1
Size: 276133 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 346197 Color: 1
Size: 339489 Color: 1
Size: 314315 Color: 0

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 358831 Color: 1
Size: 358670 Color: 1
Size: 282500 Color: 0

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 358876 Color: 1
Size: 327496 Color: 1
Size: 313629 Color: 0

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 365256 Color: 1
Size: 361328 Color: 1
Size: 273417 Color: 0

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 362184 Color: 1
Size: 361886 Color: 1
Size: 275931 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 360284 Color: 1
Size: 320691 Color: 0
Size: 319026 Color: 1

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 358307 Color: 1
Size: 349189 Color: 1
Size: 292505 Color: 0

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 360512 Color: 1
Size: 349173 Color: 1
Size: 290316 Color: 0

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 447441 Color: 1
Size: 301714 Color: 1
Size: 250846 Color: 0

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 336941 Color: 1
Size: 335232 Color: 1
Size: 327828 Color: 0

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 426851 Color: 1
Size: 320803 Color: 1
Size: 252347 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 363152 Color: 1
Size: 358623 Color: 1
Size: 278226 Color: 0

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 359582 Color: 1
Size: 326802 Color: 1
Size: 313617 Color: 0

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 406482 Color: 1
Size: 318819 Color: 1
Size: 274700 Color: 0

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 372356 Color: 1
Size: 358701 Color: 1
Size: 268944 Color: 0

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 361143 Color: 1
Size: 359362 Color: 1
Size: 279496 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 358415 Color: 1
Size: 326762 Color: 1
Size: 314824 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 352759 Color: 1
Size: 335738 Color: 1
Size: 311504 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 349667 Color: 1
Size: 339635 Color: 1
Size: 310699 Color: 0

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 421229 Color: 1
Size: 299650 Color: 1
Size: 279122 Color: 0

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 377172 Color: 1
Size: 354164 Color: 1
Size: 268665 Color: 0

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 391944 Color: 1
Size: 334674 Color: 1
Size: 273383 Color: 0

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 360686 Color: 1
Size: 328164 Color: 0
Size: 311151 Color: 1

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 371878 Color: 1
Size: 359402 Color: 1
Size: 268721 Color: 0

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 353853 Color: 1
Size: 325035 Color: 1
Size: 321113 Color: 0

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 360242 Color: 1
Size: 325390 Color: 1
Size: 314369 Color: 0

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 355765 Color: 1
Size: 328337 Color: 1
Size: 315899 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 353549 Color: 1
Size: 338632 Color: 1
Size: 307820 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 357705 Color: 1
Size: 326877 Color: 0
Size: 315419 Color: 1

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 429670 Color: 1
Size: 305042 Color: 1
Size: 265289 Color: 0

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 459469 Color: 1
Size: 287793 Color: 1
Size: 252739 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 362214 Color: 1
Size: 325925 Color: 0
Size: 311862 Color: 1

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 352236 Color: 1
Size: 345645 Color: 1
Size: 302120 Color: 0

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 366789 Color: 1
Size: 361299 Color: 1
Size: 271913 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 351643 Color: 1
Size: 348415 Color: 1
Size: 299943 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 351404 Color: 1
Size: 327196 Color: 1
Size: 321401 Color: 0

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 423569 Color: 1
Size: 323685 Color: 1
Size: 252747 Color: 0

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 353377 Color: 1
Size: 339554 Color: 1
Size: 307070 Color: 0

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 350551 Color: 1
Size: 343586 Color: 1
Size: 305864 Color: 0

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 388462 Color: 1
Size: 321620 Color: 1
Size: 289919 Color: 0

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 349775 Color: 1
Size: 348286 Color: 1
Size: 301940 Color: 0

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 496060 Color: 1
Size: 253015 Color: 0
Size: 250926 Color: 1

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 347978 Color: 1
Size: 346891 Color: 1
Size: 305132 Color: 0

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 386035 Color: 1
Size: 350402 Color: 1
Size: 263564 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 351869 Color: 1
Size: 330361 Color: 1
Size: 317771 Color: 0

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 388941 Color: 1
Size: 328382 Color: 1
Size: 282678 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 352562 Color: 1
Size: 331325 Color: 1
Size: 316114 Color: 0

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 353857 Color: 1
Size: 335020 Color: 1
Size: 311124 Color: 0

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 353353 Color: 1
Size: 323837 Color: 0
Size: 322811 Color: 1

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 410701 Color: 1
Size: 298637 Color: 0
Size: 290663 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 354990 Color: 1
Size: 338040 Color: 1
Size: 306971 Color: 0

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 354750 Color: 1
Size: 350136 Color: 1
Size: 295115 Color: 0

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 354494 Color: 1
Size: 326361 Color: 1
Size: 319146 Color: 0

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 354257 Color: 1
Size: 332955 Color: 1
Size: 312789 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 357483 Color: 1
Size: 340918 Color: 1
Size: 301600 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 357677 Color: 1
Size: 325168 Color: 1
Size: 317156 Color: 0

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 353620 Color: 1
Size: 341913 Color: 1
Size: 304468 Color: 0

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 352937 Color: 1
Size: 332878 Color: 1
Size: 314186 Color: 0

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 352234 Color: 1
Size: 347520 Color: 1
Size: 300247 Color: 0

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 352290 Color: 1
Size: 335685 Color: 1
Size: 312026 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 352241 Color: 1
Size: 331782 Color: 1
Size: 315978 Color: 0

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 352234 Color: 1
Size: 339367 Color: 1
Size: 308400 Color: 0

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 362454 Color: 1
Size: 324450 Color: 1
Size: 313097 Color: 0

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 351882 Color: 1
Size: 344124 Color: 1
Size: 303995 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 386327 Color: 1
Size: 348612 Color: 1
Size: 265062 Color: 0

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 351655 Color: 1
Size: 349847 Color: 1
Size: 298499 Color: 0

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 350409 Color: 1
Size: 343879 Color: 1
Size: 305713 Color: 0

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 350270 Color: 1
Size: 338126 Color: 1
Size: 311605 Color: 0

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 350016 Color: 1
Size: 349971 Color: 1
Size: 300014 Color: 0

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 349742 Color: 1
Size: 336792 Color: 1
Size: 313467 Color: 0

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 350255 Color: 1
Size: 348657 Color: 1
Size: 301089 Color: 0

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 349965 Color: 1
Size: 347745 Color: 1
Size: 302291 Color: 0

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 347428 Color: 1
Size: 347249 Color: 1
Size: 305324 Color: 0

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 347004 Color: 1
Size: 346567 Color: 1
Size: 306430 Color: 0

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 360717 Color: 1
Size: 345546 Color: 1
Size: 293738 Color: 0

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 360798 Color: 1
Size: 337472 Color: 1
Size: 301731 Color: 0

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 360972 Color: 1
Size: 324832 Color: 0
Size: 314197 Color: 1

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 361092 Color: 1
Size: 343877 Color: 1
Size: 295032 Color: 0

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 361496 Color: 1
Size: 341167 Color: 1
Size: 297338 Color: 0

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 361569 Color: 1
Size: 342364 Color: 1
Size: 296068 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 361592 Color: 1
Size: 350916 Color: 1
Size: 287493 Color: 0

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 361806 Color: 1
Size: 337859 Color: 1
Size: 300336 Color: 0

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 362075 Color: 1
Size: 347216 Color: 1
Size: 290710 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 362190 Color: 1
Size: 337892 Color: 1
Size: 299919 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 362334 Color: 1
Size: 323727 Color: 1
Size: 313940 Color: 0

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 362458 Color: 1
Size: 350493 Color: 1
Size: 287050 Color: 0

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 362505 Color: 1
Size: 344150 Color: 1
Size: 293346 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 362583 Color: 1
Size: 329284 Color: 0
Size: 308134 Color: 1

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 362668 Color: 1
Size: 335131 Color: 1
Size: 302202 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 362741 Color: 1
Size: 323907 Color: 1
Size: 313353 Color: 0

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 362760 Color: 1
Size: 323701 Color: 1
Size: 313540 Color: 0

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 362884 Color: 1
Size: 335956 Color: 1
Size: 301161 Color: 0

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 362895 Color: 1
Size: 349172 Color: 1
Size: 287934 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 362988 Color: 1
Size: 331956 Color: 1
Size: 305057 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 363018 Color: 1
Size: 343637 Color: 1
Size: 293346 Color: 0

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 363462 Color: 1
Size: 339400 Color: 1
Size: 297139 Color: 0

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 363676 Color: 1
Size: 342346 Color: 1
Size: 293979 Color: 0

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 363680 Color: 1
Size: 324861 Color: 1
Size: 311460 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 363737 Color: 1
Size: 331169 Color: 1
Size: 305095 Color: 0

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 363825 Color: 1
Size: 341679 Color: 1
Size: 294497 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 363830 Color: 1
Size: 336791 Color: 1
Size: 299380 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 364023 Color: 1
Size: 337097 Color: 1
Size: 298881 Color: 0

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 364130 Color: 1
Size: 341185 Color: 1
Size: 294686 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 364134 Color: 1
Size: 360478 Color: 1
Size: 275389 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 364140 Color: 1
Size: 346490 Color: 1
Size: 289371 Color: 0

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 364141 Color: 1
Size: 341638 Color: 1
Size: 294222 Color: 0

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 364288 Color: 1
Size: 349965 Color: 1
Size: 285748 Color: 0

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 364330 Color: 1
Size: 344160 Color: 1
Size: 291511 Color: 0

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 364362 Color: 1
Size: 332977 Color: 1
Size: 302662 Color: 0

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 364381 Color: 1
Size: 328098 Color: 1
Size: 307522 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 364391 Color: 1
Size: 339504 Color: 1
Size: 296106 Color: 0

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 364414 Color: 1
Size: 346189 Color: 1
Size: 289398 Color: 0

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 364476 Color: 1
Size: 343076 Color: 1
Size: 292449 Color: 0

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 364488 Color: 1
Size: 338291 Color: 1
Size: 297222 Color: 0

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 364559 Color: 1
Size: 358117 Color: 1
Size: 277325 Color: 0

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 364614 Color: 1
Size: 349748 Color: 1
Size: 285639 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 364648 Color: 1
Size: 339205 Color: 1
Size: 296148 Color: 0

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 364664 Color: 1
Size: 337746 Color: 1
Size: 297591 Color: 0

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 364671 Color: 1
Size: 325754 Color: 1
Size: 309576 Color: 0

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 364756 Color: 1
Size: 342705 Color: 1
Size: 292540 Color: 0

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 364807 Color: 1
Size: 336553 Color: 1
Size: 298641 Color: 0

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 364842 Color: 1
Size: 342932 Color: 1
Size: 292227 Color: 0

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 364858 Color: 1
Size: 338588 Color: 1
Size: 296555 Color: 0

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 364937 Color: 1
Size: 330520 Color: 1
Size: 304544 Color: 0

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 365088 Color: 1
Size: 352717 Color: 1
Size: 282196 Color: 0

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 365093 Color: 1
Size: 344546 Color: 1
Size: 290362 Color: 0

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 365103 Color: 1
Size: 341439 Color: 1
Size: 293459 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 365266 Color: 1
Size: 339208 Color: 1
Size: 295527 Color: 0

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 365283 Color: 1
Size: 335072 Color: 1
Size: 299646 Color: 0

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 365347 Color: 1
Size: 317351 Color: 1
Size: 317303 Color: 0

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 365451 Color: 1
Size: 364684 Color: 1
Size: 269866 Color: 0

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 365459 Color: 1
Size: 349860 Color: 1
Size: 284682 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 365500 Color: 1
Size: 343544 Color: 1
Size: 290957 Color: 0

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 365509 Color: 1
Size: 340785 Color: 1
Size: 293707 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 365539 Color: 1
Size: 339503 Color: 1
Size: 294959 Color: 0

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 365549 Color: 1
Size: 351271 Color: 1
Size: 283181 Color: 0

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 365648 Color: 1
Size: 322699 Color: 0
Size: 311654 Color: 1

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 365714 Color: 1
Size: 340760 Color: 1
Size: 293527 Color: 0

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 365718 Color: 1
Size: 321315 Color: 0
Size: 312968 Color: 1

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 365954 Color: 1
Size: 328442 Color: 1
Size: 305605 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 365954 Color: 1
Size: 339961 Color: 1
Size: 294086 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 366052 Color: 1
Size: 356975 Color: 1
Size: 276974 Color: 0

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 366059 Color: 1
Size: 338962 Color: 1
Size: 294980 Color: 0

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 366117 Color: 1
Size: 333059 Color: 1
Size: 300825 Color: 0

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 366134 Color: 1
Size: 329167 Color: 1
Size: 304700 Color: 0

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 366176 Color: 1
Size: 359731 Color: 1
Size: 274094 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 366191 Color: 1
Size: 348799 Color: 1
Size: 285011 Color: 0

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 366211 Color: 1
Size: 330787 Color: 0
Size: 303003 Color: 1

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 366285 Color: 1
Size: 326835 Color: 1
Size: 306881 Color: 0

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 366336 Color: 1
Size: 346685 Color: 1
Size: 286980 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 366428 Color: 1
Size: 340740 Color: 1
Size: 292833 Color: 0

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 366437 Color: 1
Size: 350096 Color: 1
Size: 283468 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 366451 Color: 1
Size: 351230 Color: 1
Size: 282320 Color: 0

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 366598 Color: 1
Size: 344755 Color: 1
Size: 288648 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 366602 Color: 1
Size: 350188 Color: 1
Size: 283211 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 366646 Color: 1
Size: 319334 Color: 0
Size: 314021 Color: 1

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 366709 Color: 1
Size: 319439 Color: 0
Size: 313853 Color: 1

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 366765 Color: 1
Size: 337726 Color: 1
Size: 295510 Color: 0

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 366843 Color: 1
Size: 344091 Color: 1
Size: 289067 Color: 0

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 366861 Color: 1
Size: 366721 Color: 1
Size: 266419 Color: 0

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 366867 Color: 1
Size: 336002 Color: 1
Size: 297132 Color: 0

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 366979 Color: 1
Size: 341214 Color: 1
Size: 291808 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 366993 Color: 1
Size: 339433 Color: 1
Size: 293575 Color: 0

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 367013 Color: 1
Size: 327810 Color: 1
Size: 305178 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 367079 Color: 1
Size: 338121 Color: 1
Size: 294801 Color: 0

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 367098 Color: 1
Size: 337344 Color: 1
Size: 295559 Color: 0

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 367197 Color: 1
Size: 340359 Color: 1
Size: 292445 Color: 0

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 367211 Color: 1
Size: 329030 Color: 1
Size: 303760 Color: 0

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 367214 Color: 1
Size: 331826 Color: 1
Size: 300961 Color: 0

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 367356 Color: 1
Size: 355601 Color: 1
Size: 277044 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 367382 Color: 1
Size: 342729 Color: 1
Size: 289890 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 367384 Color: 1
Size: 322423 Color: 1
Size: 310194 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 367384 Color: 1
Size: 341761 Color: 1
Size: 290856 Color: 0

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 367400 Color: 1
Size: 317345 Color: 0
Size: 315256 Color: 1

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 367459 Color: 1
Size: 346350 Color: 1
Size: 286192 Color: 0

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 367615 Color: 1
Size: 327253 Color: 1
Size: 305133 Color: 0

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 367791 Color: 1
Size: 326612 Color: 1
Size: 305598 Color: 0

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 367841 Color: 1
Size: 356402 Color: 1
Size: 275758 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 367845 Color: 1
Size: 337379 Color: 1
Size: 294777 Color: 0

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 367856 Color: 1
Size: 339751 Color: 1
Size: 292394 Color: 0

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 367927 Color: 1
Size: 341919 Color: 1
Size: 290155 Color: 0

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 367958 Color: 1
Size: 322737 Color: 1
Size: 309306 Color: 0

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 367994 Color: 1
Size: 331631 Color: 1
Size: 300376 Color: 0

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 368008 Color: 1
Size: 344028 Color: 1
Size: 287965 Color: 0

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 368015 Color: 1
Size: 367550 Color: 1
Size: 264436 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 368035 Color: 1
Size: 341990 Color: 1
Size: 289976 Color: 0

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 368092 Color: 1
Size: 348428 Color: 1
Size: 283481 Color: 0

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 368115 Color: 1
Size: 345514 Color: 1
Size: 286372 Color: 0

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 368202 Color: 1
Size: 357133 Color: 1
Size: 274666 Color: 0

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 368216 Color: 1
Size: 344127 Color: 1
Size: 287658 Color: 0

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 368224 Color: 1
Size: 335699 Color: 1
Size: 296078 Color: 0

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 368261 Color: 1
Size: 339332 Color: 1
Size: 292408 Color: 0

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 368267 Color: 1
Size: 340658 Color: 1
Size: 291076 Color: 0

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 368433 Color: 1
Size: 335756 Color: 1
Size: 295812 Color: 0

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 368498 Color: 1
Size: 353948 Color: 1
Size: 277555 Color: 0

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 368559 Color: 1
Size: 335049 Color: 1
Size: 296393 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 368582 Color: 1
Size: 328301 Color: 1
Size: 303118 Color: 0

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 368609 Color: 1
Size: 338777 Color: 1
Size: 292615 Color: 0

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 368632 Color: 1
Size: 324607 Color: 0
Size: 306762 Color: 1

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 368716 Color: 1
Size: 337163 Color: 1
Size: 294122 Color: 0

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 368922 Color: 1
Size: 316536 Color: 0
Size: 314543 Color: 1

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 368994 Color: 1
Size: 368085 Color: 1
Size: 262922 Color: 0

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 368996 Color: 1
Size: 344580 Color: 1
Size: 286425 Color: 0

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 369014 Color: 1
Size: 338210 Color: 1
Size: 292777 Color: 0

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 369027 Color: 1
Size: 327912 Color: 1
Size: 303062 Color: 0

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 369144 Color: 1
Size: 341882 Color: 1
Size: 288975 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 369154 Color: 1
Size: 340768 Color: 1
Size: 290079 Color: 0

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 369190 Color: 1
Size: 338103 Color: 1
Size: 292708 Color: 0

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 369191 Color: 1
Size: 332765 Color: 1
Size: 298045 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 369236 Color: 1
Size: 350524 Color: 1
Size: 280241 Color: 0

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 369273 Color: 1
Size: 338126 Color: 1
Size: 292602 Color: 0

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 369275 Color: 1
Size: 355458 Color: 1
Size: 275268 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 369277 Color: 1
Size: 340527 Color: 1
Size: 290197 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 369425 Color: 1
Size: 344685 Color: 1
Size: 285891 Color: 0

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 369502 Color: 1
Size: 346188 Color: 1
Size: 284311 Color: 0

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 369589 Color: 1
Size: 319005 Color: 1
Size: 311407 Color: 0

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 369726 Color: 1
Size: 338934 Color: 1
Size: 291341 Color: 0

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 369761 Color: 1
Size: 319963 Color: 1
Size: 310277 Color: 0

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 369767 Color: 1
Size: 338886 Color: 1
Size: 291348 Color: 0

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 369825 Color: 1
Size: 363785 Color: 1
Size: 266391 Color: 0

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 369832 Color: 1
Size: 340204 Color: 1
Size: 289965 Color: 0

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 369875 Color: 1
Size: 355083 Color: 1
Size: 275043 Color: 0

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 369956 Color: 1
Size: 355383 Color: 1
Size: 274662 Color: 0

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 369973 Color: 1
Size: 347785 Color: 1
Size: 282243 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 370019 Color: 1
Size: 339134 Color: 1
Size: 290848 Color: 0

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 370029 Color: 1
Size: 332461 Color: 1
Size: 297511 Color: 0

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 370030 Color: 1
Size: 349661 Color: 1
Size: 280310 Color: 0

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 370054 Color: 1
Size: 340828 Color: 1
Size: 289119 Color: 0

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 370126 Color: 1
Size: 336051 Color: 1
Size: 293824 Color: 0

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 370195 Color: 1
Size: 338050 Color: 1
Size: 291756 Color: 0

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 370274 Color: 1
Size: 353378 Color: 1
Size: 276349 Color: 0

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 370339 Color: 1
Size: 325437 Color: 1
Size: 304225 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 370390 Color: 1
Size: 315318 Color: 1
Size: 314293 Color: 0

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 370426 Color: 1
Size: 345603 Color: 1
Size: 283972 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 370492 Color: 1
Size: 329073 Color: 0
Size: 300436 Color: 1

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 370658 Color: 1
Size: 326072 Color: 1
Size: 303271 Color: 0

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 370845 Color: 1
Size: 353077 Color: 1
Size: 276079 Color: 0

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 370859 Color: 1
Size: 345328 Color: 1
Size: 283814 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 370859 Color: 1
Size: 332395 Color: 1
Size: 296747 Color: 0

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 370873 Color: 1
Size: 352236 Color: 1
Size: 276892 Color: 0

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 370938 Color: 1
Size: 339090 Color: 1
Size: 289973 Color: 0

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 370958 Color: 1
Size: 326889 Color: 1
Size: 302154 Color: 0

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 370966 Color: 1
Size: 331735 Color: 1
Size: 297300 Color: 0

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 370969 Color: 1
Size: 339236 Color: 1
Size: 289796 Color: 0

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 370963 Color: 1
Size: 315910 Color: 0
Size: 313128 Color: 1

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 370985 Color: 1
Size: 342566 Color: 1
Size: 286450 Color: 0

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 371030 Color: 1
Size: 358557 Color: 1
Size: 270414 Color: 0

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 371082 Color: 1
Size: 317454 Color: 1
Size: 311465 Color: 0

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 371088 Color: 1
Size: 339799 Color: 1
Size: 289114 Color: 0

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 371102 Color: 1
Size: 349738 Color: 1
Size: 279161 Color: 0

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 371106 Color: 1
Size: 336534 Color: 1
Size: 292361 Color: 0

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 371117 Color: 1
Size: 338964 Color: 1
Size: 289920 Color: 0

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 371124 Color: 1
Size: 367923 Color: 1
Size: 260954 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 371150 Color: 1
Size: 351403 Color: 1
Size: 277448 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 371221 Color: 1
Size: 346428 Color: 1
Size: 282352 Color: 0

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 371285 Color: 1
Size: 315114 Color: 1
Size: 313602 Color: 0

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 371289 Color: 1
Size: 330686 Color: 1
Size: 298026 Color: 0

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 371318 Color: 1
Size: 340030 Color: 1
Size: 288653 Color: 0

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 371529 Color: 1
Size: 329889 Color: 1
Size: 298583 Color: 0

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 371653 Color: 1
Size: 316422 Color: 0
Size: 311926 Color: 1

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 371767 Color: 1
Size: 335487 Color: 1
Size: 292747 Color: 0

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 371802 Color: 1
Size: 351330 Color: 1
Size: 276869 Color: 0

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 371873 Color: 1
Size: 329995 Color: 1
Size: 298133 Color: 0

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 371964 Color: 1
Size: 316226 Color: 1
Size: 311811 Color: 0

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 372059 Color: 1
Size: 341209 Color: 1
Size: 286733 Color: 0

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 372130 Color: 1
Size: 331504 Color: 1
Size: 296367 Color: 0

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 372180 Color: 1
Size: 336540 Color: 1
Size: 291281 Color: 0

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 372185 Color: 1
Size: 329058 Color: 1
Size: 298758 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 372214 Color: 1
Size: 344281 Color: 1
Size: 283506 Color: 0

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 372262 Color: 1
Size: 342241 Color: 1
Size: 285498 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 372347 Color: 1
Size: 346304 Color: 1
Size: 281350 Color: 0

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 372361 Color: 1
Size: 320010 Color: 1
Size: 307630 Color: 0

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 372398 Color: 1
Size: 332909 Color: 1
Size: 294694 Color: 0

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 372410 Color: 1
Size: 369297 Color: 1
Size: 258294 Color: 0

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 372472 Color: 1
Size: 353465 Color: 1
Size: 274064 Color: 0

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 372501 Color: 1
Size: 344215 Color: 1
Size: 283285 Color: 0

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 372581 Color: 1
Size: 320704 Color: 1
Size: 306716 Color: 0

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 372588 Color: 1
Size: 347876 Color: 1
Size: 279537 Color: 0

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 372706 Color: 1
Size: 338390 Color: 1
Size: 288905 Color: 0

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 372726 Color: 1
Size: 334293 Color: 1
Size: 292982 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 372731 Color: 1
Size: 334522 Color: 1
Size: 292748 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 372750 Color: 1
Size: 321990 Color: 1
Size: 305261 Color: 0

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 372754 Color: 1
Size: 320895 Color: 0
Size: 306352 Color: 1

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 372793 Color: 1
Size: 372363 Color: 1
Size: 254845 Color: 0

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 372795 Color: 1
Size: 348203 Color: 1
Size: 279003 Color: 0

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 372820 Color: 1
Size: 330364 Color: 1
Size: 296817 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 372833 Color: 1
Size: 319970 Color: 1
Size: 307198 Color: 0

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 372872 Color: 1
Size: 330233 Color: 1
Size: 296896 Color: 0

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 372885 Color: 1
Size: 347600 Color: 1
Size: 279516 Color: 0

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 372899 Color: 1
Size: 331944 Color: 1
Size: 295158 Color: 0

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 372914 Color: 1
Size: 322295 Color: 1
Size: 304792 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 372945 Color: 1
Size: 330859 Color: 1
Size: 296197 Color: 0

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 372946 Color: 1
Size: 343385 Color: 1
Size: 283670 Color: 0

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 372948 Color: 1
Size: 338570 Color: 1
Size: 288483 Color: 0

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 372998 Color: 1
Size: 340927 Color: 1
Size: 286076 Color: 0

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 373030 Color: 1
Size: 336671 Color: 1
Size: 290300 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 373076 Color: 1
Size: 324406 Color: 1
Size: 302519 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 373090 Color: 1
Size: 351307 Color: 1
Size: 275604 Color: 0

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 373119 Color: 1
Size: 335935 Color: 1
Size: 290947 Color: 0

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 373174 Color: 1
Size: 354894 Color: 1
Size: 271933 Color: 0

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 373191 Color: 1
Size: 343143 Color: 1
Size: 283667 Color: 0

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 373212 Color: 1
Size: 325128 Color: 1
Size: 301661 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 373222 Color: 1
Size: 330491 Color: 1
Size: 296288 Color: 0

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 373274 Color: 1
Size: 320834 Color: 1
Size: 305893 Color: 0

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 373326 Color: 1
Size: 322278 Color: 1
Size: 304397 Color: 0

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 373419 Color: 1
Size: 339319 Color: 1
Size: 287263 Color: 0

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 373483 Color: 1
Size: 368224 Color: 1
Size: 258294 Color: 0

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 373495 Color: 1
Size: 344055 Color: 1
Size: 282451 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 373582 Color: 1
Size: 350357 Color: 1
Size: 276062 Color: 0

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 373616 Color: 1
Size: 348735 Color: 1
Size: 277650 Color: 0

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 373625 Color: 1
Size: 330300 Color: 1
Size: 296076 Color: 0

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 373635 Color: 1
Size: 320704 Color: 1
Size: 305662 Color: 0

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 373637 Color: 1
Size: 340600 Color: 1
Size: 285764 Color: 0

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 373647 Color: 1
Size: 343887 Color: 1
Size: 282467 Color: 0

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 373654 Color: 1
Size: 335609 Color: 1
Size: 290738 Color: 0

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 373656 Color: 1
Size: 326485 Color: 1
Size: 299860 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 373670 Color: 1
Size: 355752 Color: 1
Size: 270579 Color: 0

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 373688 Color: 1
Size: 362286 Color: 1
Size: 264027 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 373691 Color: 1
Size: 364610 Color: 1
Size: 261700 Color: 0

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 373727 Color: 1
Size: 351759 Color: 1
Size: 274515 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 373817 Color: 1
Size: 332379 Color: 1
Size: 293805 Color: 0

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 373842 Color: 1
Size: 324372 Color: 1
Size: 301787 Color: 0

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 373874 Color: 1
Size: 341615 Color: 1
Size: 284512 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 373923 Color: 1
Size: 360095 Color: 1
Size: 265983 Color: 0

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 373943 Color: 1
Size: 358418 Color: 1
Size: 267640 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 373955 Color: 1
Size: 328237 Color: 1
Size: 297809 Color: 0

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 373963 Color: 1
Size: 348819 Color: 1
Size: 277219 Color: 0

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 374056 Color: 1
Size: 351283 Color: 1
Size: 274662 Color: 0

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 374099 Color: 1
Size: 337754 Color: 1
Size: 288148 Color: 0

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 374122 Color: 1
Size: 340040 Color: 1
Size: 285839 Color: 0

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 374122 Color: 1
Size: 332335 Color: 1
Size: 293544 Color: 0

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 374160 Color: 1
Size: 325261 Color: 1
Size: 300580 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 374227 Color: 1
Size: 342355 Color: 1
Size: 283419 Color: 0

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 374245 Color: 1
Size: 318917 Color: 1
Size: 306839 Color: 0

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 374284 Color: 1
Size: 337072 Color: 1
Size: 288645 Color: 0

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 374286 Color: 1
Size: 350661 Color: 1
Size: 275054 Color: 0

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 374326 Color: 1
Size: 349108 Color: 1
Size: 276567 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 374343 Color: 1
Size: 334649 Color: 1
Size: 291009 Color: 0

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 374583 Color: 1
Size: 352986 Color: 1
Size: 272432 Color: 0

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 374591 Color: 1
Size: 333581 Color: 1
Size: 291829 Color: 0

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 374628 Color: 1
Size: 332138 Color: 1
Size: 293235 Color: 0

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 374756 Color: 1
Size: 358009 Color: 1
Size: 267236 Color: 0

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 374793 Color: 1
Size: 340919 Color: 1
Size: 284289 Color: 0

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 374894 Color: 1
Size: 336981 Color: 1
Size: 288126 Color: 0

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 374946 Color: 1
Size: 340902 Color: 1
Size: 284153 Color: 0

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 374986 Color: 1
Size: 344883 Color: 1
Size: 280132 Color: 0

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 375039 Color: 1
Size: 321787 Color: 1
Size: 303175 Color: 0

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 375056 Color: 1
Size: 332952 Color: 1
Size: 291993 Color: 0

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 375059 Color: 1
Size: 347558 Color: 1
Size: 277384 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 375180 Color: 1
Size: 360651 Color: 1
Size: 264170 Color: 0

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 375208 Color: 1
Size: 331263 Color: 1
Size: 293530 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 375260 Color: 1
Size: 372187 Color: 1
Size: 252554 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 375277 Color: 1
Size: 341274 Color: 1
Size: 283450 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 375318 Color: 1
Size: 332550 Color: 1
Size: 292133 Color: 0

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 375354 Color: 1
Size: 336521 Color: 1
Size: 288126 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 375369 Color: 1
Size: 349886 Color: 1
Size: 274746 Color: 0

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 375410 Color: 1
Size: 370770 Color: 1
Size: 253821 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 375532 Color: 1
Size: 339847 Color: 1
Size: 284622 Color: 0

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 375555 Color: 1
Size: 323688 Color: 0
Size: 300758 Color: 1

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 375652 Color: 1
Size: 333292 Color: 1
Size: 291057 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 375788 Color: 1
Size: 363003 Color: 1
Size: 261210 Color: 0

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 375799 Color: 1
Size: 346560 Color: 1
Size: 277642 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 375832 Color: 1
Size: 354370 Color: 1
Size: 269799 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 376013 Color: 1
Size: 313006 Color: 1
Size: 310982 Color: 0

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 376018 Color: 1
Size: 372349 Color: 1
Size: 251634 Color: 0

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 376057 Color: 1
Size: 350643 Color: 1
Size: 273301 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 376089 Color: 1
Size: 334167 Color: 1
Size: 289745 Color: 0

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 376108 Color: 1
Size: 324673 Color: 1
Size: 299220 Color: 0

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 376124 Color: 1
Size: 336721 Color: 1
Size: 287156 Color: 0

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 376126 Color: 1
Size: 331830 Color: 1
Size: 292045 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 376155 Color: 1
Size: 361087 Color: 1
Size: 262759 Color: 0

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 376185 Color: 1
Size: 363294 Color: 1
Size: 260522 Color: 0

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 376421 Color: 1
Size: 341313 Color: 1
Size: 282267 Color: 0

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 376461 Color: 1
Size: 319216 Color: 1
Size: 304324 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 376502 Color: 1
Size: 341977 Color: 1
Size: 281522 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 376531 Color: 1
Size: 337856 Color: 1
Size: 285614 Color: 0

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 376536 Color: 1
Size: 342060 Color: 1
Size: 281405 Color: 0

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 376698 Color: 1
Size: 318491 Color: 1
Size: 304812 Color: 0

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 376727 Color: 1
Size: 336715 Color: 1
Size: 286559 Color: 0

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 376731 Color: 1
Size: 330316 Color: 1
Size: 292954 Color: 0

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 376737 Color: 1
Size: 352477 Color: 1
Size: 270787 Color: 0

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 376751 Color: 1
Size: 347900 Color: 1
Size: 275350 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 376759 Color: 1
Size: 324767 Color: 0
Size: 298475 Color: 1

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 376783 Color: 1
Size: 312372 Color: 1
Size: 310846 Color: 0

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 376800 Color: 1
Size: 340136 Color: 1
Size: 283065 Color: 0

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 376807 Color: 1
Size: 351678 Color: 1
Size: 271516 Color: 0

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 376914 Color: 1
Size: 313180 Color: 0
Size: 309907 Color: 1

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 376980 Color: 1
Size: 355188 Color: 1
Size: 267833 Color: 0

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 377004 Color: 1
Size: 329508 Color: 1
Size: 293489 Color: 0

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 377067 Color: 1
Size: 368691 Color: 1
Size: 254243 Color: 0

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 377164 Color: 1
Size: 369918 Color: 1
Size: 252919 Color: 0

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 377182 Color: 1
Size: 355855 Color: 1
Size: 266964 Color: 0

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 377208 Color: 1
Size: 341701 Color: 1
Size: 281092 Color: 0

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 377222 Color: 1
Size: 345530 Color: 1
Size: 277249 Color: 0

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 377238 Color: 1
Size: 351225 Color: 1
Size: 271538 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 377240 Color: 1
Size: 324435 Color: 1
Size: 298326 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 377273 Color: 1
Size: 344696 Color: 1
Size: 278032 Color: 0

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 377297 Color: 1
Size: 339696 Color: 1
Size: 283008 Color: 0

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 377322 Color: 1
Size: 321094 Color: 0
Size: 301585 Color: 1

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 377323 Color: 1
Size: 324140 Color: 1
Size: 298538 Color: 0

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 377327 Color: 1
Size: 327001 Color: 1
Size: 295673 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 377398 Color: 1
Size: 333719 Color: 1
Size: 288884 Color: 0

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 377400 Color: 1
Size: 349611 Color: 1
Size: 272990 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 377423 Color: 1
Size: 335398 Color: 1
Size: 287180 Color: 0

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 377447 Color: 1
Size: 329474 Color: 1
Size: 293080 Color: 0

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 377451 Color: 1
Size: 347206 Color: 1
Size: 275344 Color: 0

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 377471 Color: 1
Size: 319614 Color: 1
Size: 302916 Color: 0

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 377539 Color: 1
Size: 346643 Color: 1
Size: 275819 Color: 0

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 377549 Color: 1
Size: 351343 Color: 1
Size: 271109 Color: 0

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 377699 Color: 1
Size: 362298 Color: 1
Size: 260004 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 377738 Color: 1
Size: 333017 Color: 1
Size: 289246 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 377739 Color: 1
Size: 357802 Color: 1
Size: 264460 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 377743 Color: 1
Size: 315029 Color: 0
Size: 307229 Color: 1

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 377812 Color: 1
Size: 321657 Color: 1
Size: 300532 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 377835 Color: 1
Size: 320544 Color: 0
Size: 301622 Color: 1

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 377848 Color: 1
Size: 315421 Color: 1
Size: 306732 Color: 0

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 377943 Color: 1
Size: 351022 Color: 1
Size: 271036 Color: 0

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 377961 Color: 1
Size: 331893 Color: 1
Size: 290147 Color: 0

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 377968 Color: 1
Size: 323300 Color: 1
Size: 298733 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 377983 Color: 1
Size: 337417 Color: 1
Size: 284601 Color: 0

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 377988 Color: 1
Size: 366525 Color: 1
Size: 255488 Color: 0

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 377993 Color: 1
Size: 326440 Color: 1
Size: 295568 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 378132 Color: 1
Size: 311714 Color: 1
Size: 310155 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 378298 Color: 1
Size: 338395 Color: 1
Size: 283308 Color: 0

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 378325 Color: 1
Size: 348737 Color: 1
Size: 272939 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 378365 Color: 1
Size: 337604 Color: 1
Size: 284032 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 378385 Color: 1
Size: 338658 Color: 1
Size: 282958 Color: 0

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 378443 Color: 1
Size: 352533 Color: 1
Size: 269025 Color: 0

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 378452 Color: 1
Size: 331542 Color: 1
Size: 290007 Color: 0

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 378513 Color: 1
Size: 327168 Color: 1
Size: 294320 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 378517 Color: 1
Size: 326307 Color: 1
Size: 295177 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 378520 Color: 1
Size: 358108 Color: 1
Size: 263373 Color: 0

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 378542 Color: 1
Size: 359085 Color: 1
Size: 262374 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 378578 Color: 1
Size: 334999 Color: 1
Size: 286424 Color: 0

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 378660 Color: 1
Size: 342840 Color: 1
Size: 278501 Color: 0

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 378741 Color: 1
Size: 324297 Color: 1
Size: 296963 Color: 0

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 378774 Color: 1
Size: 356790 Color: 1
Size: 264437 Color: 0

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 378781 Color: 1
Size: 317172 Color: 1
Size: 304048 Color: 0

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 378790 Color: 1
Size: 312720 Color: 1
Size: 308491 Color: 0

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 378793 Color: 1
Size: 340046 Color: 1
Size: 281162 Color: 0

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 378809 Color: 1
Size: 317743 Color: 1
Size: 303449 Color: 0

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 378874 Color: 1
Size: 312000 Color: 1
Size: 309127 Color: 0

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 378888 Color: 1
Size: 325621 Color: 1
Size: 295492 Color: 0

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 378896 Color: 1
Size: 314173 Color: 1
Size: 306932 Color: 0

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 378995 Color: 1
Size: 337992 Color: 1
Size: 283014 Color: 0

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 379023 Color: 1
Size: 312456 Color: 0
Size: 308522 Color: 1

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 379060 Color: 1
Size: 335619 Color: 1
Size: 285322 Color: 0

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 379081 Color: 1
Size: 336354 Color: 1
Size: 284566 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 379085 Color: 1
Size: 346586 Color: 1
Size: 274330 Color: 0

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 379087 Color: 1
Size: 328683 Color: 1
Size: 292231 Color: 0

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 379092 Color: 1
Size: 335820 Color: 1
Size: 285089 Color: 0

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 379117 Color: 1
Size: 332516 Color: 1
Size: 288368 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 379146 Color: 1
Size: 319264 Color: 1
Size: 301591 Color: 0

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 379344 Color: 1
Size: 357394 Color: 1
Size: 263263 Color: 0

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 379407 Color: 1
Size: 334393 Color: 1
Size: 286201 Color: 0

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 379414 Color: 1
Size: 319011 Color: 1
Size: 301576 Color: 0

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 379433 Color: 1
Size: 338813 Color: 1
Size: 281755 Color: 0

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 379488 Color: 1
Size: 318721 Color: 1
Size: 301792 Color: 0

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 379560 Color: 1
Size: 322950 Color: 1
Size: 297491 Color: 0

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 379569 Color: 1
Size: 347012 Color: 1
Size: 273420 Color: 0

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 379609 Color: 1
Size: 320896 Color: 1
Size: 299496 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 379692 Color: 1
Size: 337181 Color: 1
Size: 283128 Color: 0

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 379720 Color: 1
Size: 333631 Color: 1
Size: 286650 Color: 0

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 379758 Color: 1
Size: 337897 Color: 1
Size: 282346 Color: 0

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 379942 Color: 1
Size: 332672 Color: 1
Size: 287387 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 380015 Color: 1
Size: 362544 Color: 1
Size: 257442 Color: 0

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 380078 Color: 1
Size: 353409 Color: 1
Size: 266514 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 380107 Color: 1
Size: 350698 Color: 1
Size: 269196 Color: 0

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 380116 Color: 1
Size: 320559 Color: 1
Size: 299326 Color: 0

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 380224 Color: 1
Size: 329821 Color: 1
Size: 289956 Color: 0

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 380279 Color: 1
Size: 358857 Color: 1
Size: 260865 Color: 0

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 380312 Color: 1
Size: 316132 Color: 0
Size: 303557 Color: 1

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 380327 Color: 1
Size: 339432 Color: 1
Size: 280242 Color: 0

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 380388 Color: 1
Size: 326937 Color: 1
Size: 292676 Color: 0

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 380422 Color: 1
Size: 356857 Color: 1
Size: 262722 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 380451 Color: 1
Size: 310104 Color: 0
Size: 309446 Color: 1

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 380561 Color: 1
Size: 345868 Color: 1
Size: 273572 Color: 0

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 380590 Color: 1
Size: 348190 Color: 1
Size: 271221 Color: 0

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 380628 Color: 1
Size: 346654 Color: 1
Size: 272719 Color: 0

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 380638 Color: 1
Size: 328793 Color: 0
Size: 290570 Color: 1

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 380654 Color: 1
Size: 344766 Color: 1
Size: 274581 Color: 0

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 380704 Color: 1
Size: 342883 Color: 1
Size: 276414 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 380724 Color: 1
Size: 332535 Color: 1
Size: 286742 Color: 0

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 380828 Color: 1
Size: 336187 Color: 1
Size: 282986 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 380889 Color: 1
Size: 357701 Color: 1
Size: 261411 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 380970 Color: 1
Size: 322469 Color: 1
Size: 296562 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 380971 Color: 1
Size: 332906 Color: 1
Size: 286124 Color: 0

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 380984 Color: 1
Size: 340475 Color: 1
Size: 278542 Color: 0

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 381035 Color: 1
Size: 328027 Color: 1
Size: 290939 Color: 0

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 381084 Color: 1
Size: 343499 Color: 1
Size: 275418 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 381091 Color: 1
Size: 338848 Color: 1
Size: 280062 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 381209 Color: 1
Size: 338508 Color: 1
Size: 280284 Color: 0

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 381236 Color: 1
Size: 332060 Color: 1
Size: 286705 Color: 0

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 381238 Color: 1
Size: 349157 Color: 1
Size: 269606 Color: 0

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 381305 Color: 1
Size: 337374 Color: 1
Size: 281322 Color: 0

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 381336 Color: 1
Size: 347807 Color: 1
Size: 270858 Color: 0

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 381356 Color: 1
Size: 325746 Color: 1
Size: 292899 Color: 0

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 381365 Color: 1
Size: 344375 Color: 1
Size: 274261 Color: 0

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 381420 Color: 1
Size: 334103 Color: 1
Size: 284478 Color: 0

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 381469 Color: 1
Size: 359274 Color: 1
Size: 259258 Color: 0

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 381490 Color: 1
Size: 343146 Color: 1
Size: 275365 Color: 0

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 381519 Color: 1
Size: 317927 Color: 1
Size: 300555 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 381567 Color: 1
Size: 324656 Color: 1
Size: 293778 Color: 0

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 381592 Color: 1
Size: 331835 Color: 1
Size: 286574 Color: 0

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 381598 Color: 1
Size: 328622 Color: 1
Size: 289781 Color: 0

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 381738 Color: 1
Size: 354378 Color: 1
Size: 263885 Color: 0

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 381830 Color: 1
Size: 337887 Color: 1
Size: 280284 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 382047 Color: 1
Size: 327996 Color: 1
Size: 289958 Color: 0

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 382053 Color: 1
Size: 318491 Color: 1
Size: 299457 Color: 0

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 382064 Color: 1
Size: 349861 Color: 1
Size: 268076 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 382070 Color: 1
Size: 315234 Color: 1
Size: 302697 Color: 0

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 382081 Color: 1
Size: 349298 Color: 1
Size: 268622 Color: 0

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 382086 Color: 1
Size: 366836 Color: 1
Size: 251079 Color: 0

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 382092 Color: 1
Size: 359167 Color: 1
Size: 258742 Color: 0

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 382123 Color: 1
Size: 345293 Color: 1
Size: 272585 Color: 0

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 382204 Color: 1
Size: 359562 Color: 1
Size: 258235 Color: 0

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 382251 Color: 1
Size: 343191 Color: 1
Size: 274559 Color: 0

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 382276 Color: 1
Size: 354049 Color: 1
Size: 263676 Color: 0

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 382277 Color: 1
Size: 312319 Color: 1
Size: 305405 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 382278 Color: 1
Size: 343578 Color: 1
Size: 274145 Color: 0

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 382286 Color: 1
Size: 316708 Color: 1
Size: 301007 Color: 0

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 382327 Color: 1
Size: 341389 Color: 1
Size: 276285 Color: 0

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 382468 Color: 1
Size: 335805 Color: 1
Size: 281728 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 382481 Color: 1
Size: 342253 Color: 1
Size: 275267 Color: 0

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 382532 Color: 1
Size: 367132 Color: 1
Size: 250337 Color: 0

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 382586 Color: 1
Size: 333531 Color: 1
Size: 283884 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 382623 Color: 1
Size: 333064 Color: 1
Size: 284314 Color: 0

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 382659 Color: 1
Size: 327743 Color: 1
Size: 289599 Color: 0

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 382678 Color: 1
Size: 331384 Color: 1
Size: 285939 Color: 0

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 382699 Color: 1
Size: 325586 Color: 1
Size: 291716 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 382702 Color: 1
Size: 320935 Color: 1
Size: 296364 Color: 0

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 382721 Color: 1
Size: 350321 Color: 1
Size: 266959 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 382787 Color: 1
Size: 345383 Color: 1
Size: 271831 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 382859 Color: 1
Size: 328651 Color: 1
Size: 288491 Color: 0

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 383045 Color: 1
Size: 335907 Color: 1
Size: 281049 Color: 0

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 383086 Color: 1
Size: 325892 Color: 1
Size: 291023 Color: 0

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 383129 Color: 1
Size: 346470 Color: 1
Size: 270402 Color: 0

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 383215 Color: 1
Size: 308939 Color: 0
Size: 307847 Color: 1

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 383220 Color: 1
Size: 343602 Color: 1
Size: 273179 Color: 0

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 383223 Color: 1
Size: 337486 Color: 1
Size: 279292 Color: 0

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 383253 Color: 1
Size: 325983 Color: 1
Size: 290765 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 383403 Color: 1
Size: 330482 Color: 1
Size: 286116 Color: 0

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 383427 Color: 1
Size: 325336 Color: 1
Size: 291238 Color: 0

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 383435 Color: 1
Size: 320053 Color: 1
Size: 296513 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 383478 Color: 1
Size: 328938 Color: 1
Size: 287585 Color: 0

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 383495 Color: 1
Size: 319523 Color: 1
Size: 296983 Color: 0

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 383568 Color: 1
Size: 343643 Color: 1
Size: 272790 Color: 0

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 383592 Color: 1
Size: 348028 Color: 1
Size: 268381 Color: 0

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 383617 Color: 1
Size: 314021 Color: 0
Size: 302363 Color: 1

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 383642 Color: 1
Size: 348389 Color: 1
Size: 267970 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 383650 Color: 1
Size: 329715 Color: 1
Size: 286636 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 383740 Color: 1
Size: 334009 Color: 1
Size: 282252 Color: 0

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 383829 Color: 1
Size: 334163 Color: 1
Size: 282009 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 383839 Color: 1
Size: 326504 Color: 1
Size: 289658 Color: 0

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 383919 Color: 1
Size: 312293 Color: 1
Size: 303789 Color: 0

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 383996 Color: 1
Size: 323192 Color: 1
Size: 292813 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 384168 Color: 1
Size: 330944 Color: 1
Size: 284889 Color: 0

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 384193 Color: 1
Size: 329012 Color: 1
Size: 286796 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 384228 Color: 1
Size: 343489 Color: 1
Size: 272284 Color: 0

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 384270 Color: 1
Size: 339203 Color: 1
Size: 276528 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 384382 Color: 1
Size: 349735 Color: 1
Size: 265884 Color: 0

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 384395 Color: 1
Size: 355342 Color: 1
Size: 260264 Color: 0

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 384399 Color: 1
Size: 340874 Color: 1
Size: 274728 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 384402 Color: 1
Size: 348068 Color: 1
Size: 267531 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 384415 Color: 1
Size: 339511 Color: 1
Size: 276075 Color: 0

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 384473 Color: 1
Size: 331687 Color: 1
Size: 283841 Color: 0

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 384528 Color: 1
Size: 356338 Color: 1
Size: 259135 Color: 0

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 384633 Color: 1
Size: 322874 Color: 1
Size: 292494 Color: 0

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 384635 Color: 1
Size: 346944 Color: 1
Size: 268422 Color: 0

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 384677 Color: 1
Size: 311707 Color: 0
Size: 303617 Color: 1

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 384688 Color: 1
Size: 336532 Color: 1
Size: 278781 Color: 0

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 384774 Color: 1
Size: 329257 Color: 1
Size: 285970 Color: 0

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 384949 Color: 1
Size: 351739 Color: 1
Size: 263313 Color: 0

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 384952 Color: 1
Size: 343104 Color: 1
Size: 271945 Color: 0

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 385068 Color: 1
Size: 318176 Color: 1
Size: 296757 Color: 0

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 385298 Color: 1
Size: 322827 Color: 1
Size: 291876 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 385306 Color: 1
Size: 331282 Color: 1
Size: 283413 Color: 0

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 385315 Color: 1
Size: 339104 Color: 1
Size: 275582 Color: 0

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 385435 Color: 1
Size: 335262 Color: 1
Size: 279304 Color: 0

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 385441 Color: 1
Size: 316186 Color: 1
Size: 298374 Color: 0

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 385509 Color: 1
Size: 325070 Color: 1
Size: 289422 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 385566 Color: 1
Size: 348551 Color: 1
Size: 265884 Color: 0

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 385667 Color: 1
Size: 313924 Color: 0
Size: 300410 Color: 1

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 385709 Color: 1
Size: 329327 Color: 0
Size: 284965 Color: 1

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 385828 Color: 1
Size: 347758 Color: 1
Size: 266415 Color: 0

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 385845 Color: 1
Size: 333465 Color: 1
Size: 280691 Color: 0

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 385867 Color: 1
Size: 325360 Color: 1
Size: 288774 Color: 0

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 385896 Color: 1
Size: 349188 Color: 1
Size: 264917 Color: 0

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 385945 Color: 1
Size: 338011 Color: 1
Size: 276045 Color: 0

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 385949 Color: 1
Size: 339316 Color: 1
Size: 274736 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 385973 Color: 1
Size: 340482 Color: 1
Size: 273546 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 385982 Color: 1
Size: 325464 Color: 1
Size: 288555 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 386049 Color: 1
Size: 335485 Color: 1
Size: 278467 Color: 0

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 386058 Color: 1
Size: 346298 Color: 1
Size: 267645 Color: 0

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 386075 Color: 1
Size: 308139 Color: 1
Size: 305787 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 386131 Color: 1
Size: 313295 Color: 0
Size: 300575 Color: 1

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 386157 Color: 1
Size: 332161 Color: 1
Size: 281683 Color: 0

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 386170 Color: 1
Size: 310817 Color: 0
Size: 303014 Color: 1

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 386175 Color: 1
Size: 311651 Color: 0
Size: 302175 Color: 1

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 386263 Color: 1
Size: 323343 Color: 1
Size: 290395 Color: 0

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 386268 Color: 1
Size: 334256 Color: 1
Size: 279477 Color: 0

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 386312 Color: 1
Size: 319395 Color: 1
Size: 294294 Color: 0

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 386338 Color: 1
Size: 322770 Color: 1
Size: 290893 Color: 0

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 386360 Color: 1
Size: 311319 Color: 1
Size: 302322 Color: 0

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 386474 Color: 1
Size: 320555 Color: 1
Size: 292972 Color: 0

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 386513 Color: 1
Size: 339456 Color: 1
Size: 274032 Color: 0

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 386521 Color: 1
Size: 326986 Color: 1
Size: 286494 Color: 0

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 386549 Color: 1
Size: 316112 Color: 1
Size: 297340 Color: 0

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 386563 Color: 1
Size: 340193 Color: 1
Size: 273245 Color: 0

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 386609 Color: 1
Size: 339825 Color: 1
Size: 273567 Color: 0

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 386621 Color: 1
Size: 342966 Color: 1
Size: 270414 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 386688 Color: 1
Size: 337074 Color: 1
Size: 276239 Color: 0

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 386762 Color: 1
Size: 347347 Color: 1
Size: 265892 Color: 0

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 386881 Color: 1
Size: 307733 Color: 0
Size: 305387 Color: 1

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 386881 Color: 1
Size: 322452 Color: 1
Size: 290668 Color: 0

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 387000 Color: 1
Size: 332851 Color: 1
Size: 280150 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 387006 Color: 1
Size: 319929 Color: 1
Size: 293066 Color: 0

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 387008 Color: 1
Size: 335973 Color: 1
Size: 277020 Color: 0

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 387052 Color: 1
Size: 341581 Color: 1
Size: 271368 Color: 0

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 387055 Color: 1
Size: 337292 Color: 1
Size: 275654 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 387123 Color: 1
Size: 339918 Color: 1
Size: 272960 Color: 0

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 387200 Color: 1
Size: 348103 Color: 1
Size: 264698 Color: 0

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 387281 Color: 1
Size: 358486 Color: 1
Size: 254234 Color: 0

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 387282 Color: 1
Size: 310976 Color: 0
Size: 301743 Color: 1

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 387303 Color: 1
Size: 311996 Color: 0
Size: 300702 Color: 1

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 387314 Color: 1
Size: 332558 Color: 1
Size: 280129 Color: 0

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 387373 Color: 1
Size: 338058 Color: 1
Size: 274570 Color: 0

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 387398 Color: 1
Size: 343897 Color: 1
Size: 268706 Color: 0

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 387443 Color: 1
Size: 345391 Color: 1
Size: 267167 Color: 0

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 387472 Color: 1
Size: 320890 Color: 1
Size: 291639 Color: 0

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 387616 Color: 1
Size: 322041 Color: 1
Size: 290344 Color: 0

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 387640 Color: 1
Size: 346902 Color: 1
Size: 265459 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 387692 Color: 1
Size: 345515 Color: 1
Size: 266794 Color: 0

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 387744 Color: 1
Size: 314657 Color: 0
Size: 297600 Color: 1

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 387772 Color: 1
Size: 355398 Color: 1
Size: 256831 Color: 0

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 387828 Color: 1
Size: 339273 Color: 1
Size: 272900 Color: 0

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 387893 Color: 1
Size: 325909 Color: 1
Size: 286199 Color: 0

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 387971 Color: 1
Size: 319635 Color: 1
Size: 292395 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 388069 Color: 1
Size: 355736 Color: 1
Size: 256196 Color: 0

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 388128 Color: 1
Size: 337178 Color: 1
Size: 274695 Color: 0

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 388131 Color: 1
Size: 342069 Color: 1
Size: 269801 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 388153 Color: 1
Size: 316725 Color: 1
Size: 295123 Color: 0

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 388205 Color: 1
Size: 317134 Color: 1
Size: 294662 Color: 0

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 388221 Color: 1
Size: 331632 Color: 1
Size: 280148 Color: 0

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 388236 Color: 1
Size: 339790 Color: 1
Size: 271975 Color: 0

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 388294 Color: 1
Size: 352384 Color: 1
Size: 259323 Color: 0

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 388343 Color: 1
Size: 336096 Color: 1
Size: 275562 Color: 0

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 388397 Color: 1
Size: 330649 Color: 1
Size: 280955 Color: 0

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 388431 Color: 1
Size: 336378 Color: 1
Size: 275192 Color: 0

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 388434 Color: 1
Size: 334943 Color: 1
Size: 276624 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 388441 Color: 1
Size: 340206 Color: 1
Size: 271354 Color: 0

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 388532 Color: 1
Size: 334119 Color: 1
Size: 277350 Color: 0

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 388583 Color: 1
Size: 313427 Color: 1
Size: 297991 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 388612 Color: 1
Size: 317687 Color: 1
Size: 293702 Color: 0

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 388677 Color: 1
Size: 323172 Color: 1
Size: 288152 Color: 0

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 388691 Color: 1
Size: 329074 Color: 1
Size: 282236 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 388792 Color: 1
Size: 341562 Color: 1
Size: 269647 Color: 0

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 388906 Color: 1
Size: 336406 Color: 1
Size: 274689 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 388928 Color: 1
Size: 307293 Color: 0
Size: 303780 Color: 1

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 388981 Color: 1
Size: 317039 Color: 0
Size: 293981 Color: 1

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 388996 Color: 1
Size: 334188 Color: 1
Size: 276817 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 389038 Color: 1
Size: 326285 Color: 1
Size: 284678 Color: 0

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 389135 Color: 1
Size: 322830 Color: 0
Size: 288036 Color: 1

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 389153 Color: 1
Size: 325937 Color: 1
Size: 284911 Color: 0

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 389227 Color: 1
Size: 330381 Color: 1
Size: 280393 Color: 0

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 389255 Color: 1
Size: 308034 Color: 0
Size: 302712 Color: 1

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 389314 Color: 1
Size: 337552 Color: 1
Size: 273135 Color: 0

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 389329 Color: 1
Size: 336248 Color: 1
Size: 274424 Color: 0

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 389365 Color: 1
Size: 308768 Color: 1
Size: 301868 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 389447 Color: 1
Size: 338946 Color: 1
Size: 271608 Color: 0

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 389580 Color: 1
Size: 320361 Color: 1
Size: 290060 Color: 0

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 389628 Color: 1
Size: 354811 Color: 1
Size: 255562 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 389688 Color: 1
Size: 327728 Color: 1
Size: 282585 Color: 0

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 389752 Color: 1
Size: 327133 Color: 1
Size: 283116 Color: 0

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 389803 Color: 1
Size: 327868 Color: 1
Size: 282330 Color: 0

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 389803 Color: 1
Size: 306368 Color: 0
Size: 303830 Color: 1

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 389844 Color: 1
Size: 329434 Color: 1
Size: 280723 Color: 0

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 389898 Color: 1
Size: 342441 Color: 1
Size: 267662 Color: 0

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 389960 Color: 1
Size: 330228 Color: 1
Size: 279813 Color: 0

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 390065 Color: 1
Size: 330757 Color: 1
Size: 279179 Color: 0

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 390072 Color: 1
Size: 306833 Color: 0
Size: 303096 Color: 1

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 390149 Color: 1
Size: 334793 Color: 1
Size: 275059 Color: 0

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 390205 Color: 1
Size: 342101 Color: 1
Size: 267695 Color: 0

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 359659 Color: 1
Size: 356575 Color: 1
Size: 283767 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 390255 Color: 1
Size: 320163 Color: 1
Size: 289583 Color: 0

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 390258 Color: 1
Size: 342268 Color: 1
Size: 267475 Color: 0

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 390290 Color: 1
Size: 331735 Color: 1
Size: 277976 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 390345 Color: 1
Size: 313175 Color: 0
Size: 296481 Color: 1

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 390377 Color: 1
Size: 328905 Color: 1
Size: 280719 Color: 0

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 420683 Color: 1
Size: 318973 Color: 1
Size: 260345 Color: 0

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 390456 Color: 1
Size: 345885 Color: 1
Size: 263660 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 390461 Color: 1
Size: 334063 Color: 1
Size: 275477 Color: 0

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 390587 Color: 1
Size: 305379 Color: 1
Size: 304035 Color: 0

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 390685 Color: 1
Size: 318253 Color: 1
Size: 291063 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 390778 Color: 1
Size: 326484 Color: 1
Size: 282739 Color: 0

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 390781 Color: 1
Size: 329920 Color: 1
Size: 279300 Color: 0

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 390794 Color: 1
Size: 328896 Color: 1
Size: 280311 Color: 0

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 390830 Color: 1
Size: 307135 Color: 0
Size: 302036 Color: 1

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 390834 Color: 1
Size: 309224 Color: 0
Size: 299943 Color: 1

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 390853 Color: 1
Size: 304645 Color: 0
Size: 304503 Color: 1

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 390978 Color: 1
Size: 310242 Color: 1
Size: 298781 Color: 0

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 391003 Color: 1
Size: 305974 Color: 0
Size: 303024 Color: 1

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 391066 Color: 1
Size: 314110 Color: 1
Size: 294825 Color: 0

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 391068 Color: 1
Size: 333357 Color: 1
Size: 275576 Color: 0

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 391100 Color: 1
Size: 338498 Color: 1
Size: 270403 Color: 0

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 391139 Color: 1
Size: 342307 Color: 1
Size: 266555 Color: 0

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 391203 Color: 1
Size: 332894 Color: 1
Size: 275904 Color: 0

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 391214 Color: 1
Size: 312073 Color: 1
Size: 296714 Color: 0

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 391225 Color: 1
Size: 311726 Color: 1
Size: 297050 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 391259 Color: 1
Size: 339321 Color: 1
Size: 269421 Color: 0

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 391296 Color: 1
Size: 326000 Color: 1
Size: 282705 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 391363 Color: 1
Size: 332626 Color: 1
Size: 276012 Color: 0

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 391407 Color: 1
Size: 333012 Color: 1
Size: 275582 Color: 0

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 391429 Color: 1
Size: 305403 Color: 1
Size: 303169 Color: 0

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 391437 Color: 1
Size: 310778 Color: 1
Size: 297786 Color: 0

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 391479 Color: 1
Size: 345460 Color: 1
Size: 263062 Color: 0

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 391499 Color: 1
Size: 341453 Color: 1
Size: 267049 Color: 0

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 391505 Color: 1
Size: 334045 Color: 1
Size: 274451 Color: 0

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 391522 Color: 1
Size: 304593 Color: 1
Size: 303886 Color: 0

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 391556 Color: 1
Size: 334864 Color: 1
Size: 273581 Color: 0

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 391570 Color: 1
Size: 336835 Color: 1
Size: 271596 Color: 0

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 391650 Color: 1
Size: 312606 Color: 1
Size: 295745 Color: 0

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 391657 Color: 1
Size: 314704 Color: 1
Size: 293640 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 391722 Color: 1
Size: 331016 Color: 1
Size: 277263 Color: 0

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 391748 Color: 1
Size: 324930 Color: 1
Size: 283323 Color: 0

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 391755 Color: 1
Size: 313286 Color: 1
Size: 294960 Color: 0

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 391760 Color: 1
Size: 338526 Color: 1
Size: 269715 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 391826 Color: 1
Size: 336420 Color: 1
Size: 271755 Color: 0

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 391840 Color: 1
Size: 336251 Color: 1
Size: 271910 Color: 0

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 391855 Color: 1
Size: 342083 Color: 1
Size: 266063 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 391900 Color: 1
Size: 334044 Color: 1
Size: 274057 Color: 0

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 391946 Color: 1
Size: 329914 Color: 1
Size: 278141 Color: 0

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 391992 Color: 1
Size: 347515 Color: 1
Size: 260494 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 392012 Color: 1
Size: 335482 Color: 1
Size: 272507 Color: 0

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 392124 Color: 1
Size: 334564 Color: 1
Size: 273313 Color: 0

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 392134 Color: 1
Size: 326309 Color: 1
Size: 281558 Color: 0

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 392155 Color: 1
Size: 343870 Color: 1
Size: 263976 Color: 0

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 392157 Color: 1
Size: 324287 Color: 1
Size: 283557 Color: 0

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 392171 Color: 1
Size: 323579 Color: 1
Size: 284251 Color: 0

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 392173 Color: 1
Size: 327057 Color: 1
Size: 280771 Color: 0

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 392226 Color: 1
Size: 320037 Color: 1
Size: 287738 Color: 0

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 392298 Color: 1
Size: 318391 Color: 1
Size: 289312 Color: 0

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 392323 Color: 1
Size: 331078 Color: 1
Size: 276600 Color: 0

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 392345 Color: 1
Size: 339021 Color: 1
Size: 268635 Color: 0

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 392437 Color: 1
Size: 320150 Color: 1
Size: 287414 Color: 0

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 392446 Color: 1
Size: 311254 Color: 1
Size: 296301 Color: 0

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 392485 Color: 1
Size: 309582 Color: 1
Size: 297934 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 392539 Color: 1
Size: 305176 Color: 1
Size: 302286 Color: 0

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 392540 Color: 1
Size: 337493 Color: 1
Size: 269968 Color: 0

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 392682 Color: 1
Size: 339996 Color: 1
Size: 267323 Color: 0

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 392684 Color: 1
Size: 335144 Color: 1
Size: 272173 Color: 0

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 392761 Color: 1
Size: 345169 Color: 1
Size: 262071 Color: 0

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 392772 Color: 1
Size: 338555 Color: 1
Size: 268674 Color: 0

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 392875 Color: 1
Size: 327008 Color: 1
Size: 280118 Color: 0

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 392897 Color: 1
Size: 329516 Color: 1
Size: 277588 Color: 0

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 392908 Color: 1
Size: 335325 Color: 1
Size: 271768 Color: 0

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 392931 Color: 1
Size: 337518 Color: 1
Size: 269552 Color: 0

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 393024 Color: 1
Size: 306300 Color: 0
Size: 300677 Color: 1

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 393080 Color: 1
Size: 338477 Color: 1
Size: 268444 Color: 0

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 393089 Color: 1
Size: 332646 Color: 1
Size: 274266 Color: 0

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 393133 Color: 1
Size: 330995 Color: 1
Size: 275873 Color: 0

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 393234 Color: 1
Size: 316614 Color: 1
Size: 290153 Color: 0

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 393289 Color: 1
Size: 338433 Color: 1
Size: 268279 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 393321 Color: 1
Size: 311780 Color: 1
Size: 294900 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 393361 Color: 1
Size: 316391 Color: 1
Size: 290249 Color: 0

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 393375 Color: 1
Size: 324705 Color: 1
Size: 281921 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 393425 Color: 1
Size: 324404 Color: 1
Size: 282172 Color: 0

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 393608 Color: 1
Size: 306153 Color: 1
Size: 300240 Color: 0

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 393616 Color: 1
Size: 346594 Color: 1
Size: 259791 Color: 0

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 393657 Color: 1
Size: 336962 Color: 1
Size: 269382 Color: 0

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 393694 Color: 1
Size: 326076 Color: 1
Size: 280231 Color: 0

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 393747 Color: 1
Size: 320130 Color: 1
Size: 286124 Color: 0

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 393761 Color: 1
Size: 337818 Color: 1
Size: 268422 Color: 0

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 393779 Color: 1
Size: 314761 Color: 1
Size: 291461 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 393786 Color: 1
Size: 342651 Color: 1
Size: 263564 Color: 0

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 393828 Color: 1
Size: 315293 Color: 1
Size: 290880 Color: 0

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 393844 Color: 1
Size: 317804 Color: 1
Size: 288353 Color: 0

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 393957 Color: 1
Size: 310544 Color: 1
Size: 295500 Color: 0

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 393980 Color: 1
Size: 308375 Color: 1
Size: 297646 Color: 0

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 394027 Color: 1
Size: 318937 Color: 1
Size: 287037 Color: 0

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 394130 Color: 1
Size: 335044 Color: 1
Size: 270827 Color: 0

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 394170 Color: 1
Size: 341632 Color: 1
Size: 264199 Color: 0

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 394173 Color: 1
Size: 313812 Color: 0
Size: 292016 Color: 1

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 394214 Color: 1
Size: 313131 Color: 1
Size: 292656 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 394270 Color: 1
Size: 306342 Color: 0
Size: 299389 Color: 1

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 394275 Color: 1
Size: 349079 Color: 1
Size: 256647 Color: 0

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 394405 Color: 1
Size: 347173 Color: 1
Size: 258423 Color: 0

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 394410 Color: 1
Size: 320206 Color: 1
Size: 285385 Color: 0

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 394750 Color: 1
Size: 307419 Color: 1
Size: 297832 Color: 0

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 394771 Color: 1
Size: 332632 Color: 1
Size: 272598 Color: 0

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 394785 Color: 1
Size: 338206 Color: 1
Size: 267010 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 394812 Color: 1
Size: 346458 Color: 1
Size: 258731 Color: 0

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 394836 Color: 1
Size: 345878 Color: 1
Size: 259287 Color: 0

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 394916 Color: 1
Size: 322569 Color: 1
Size: 282516 Color: 0

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 394937 Color: 1
Size: 334091 Color: 1
Size: 270973 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 394973 Color: 1
Size: 311874 Color: 1
Size: 293154 Color: 0

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 394987 Color: 1
Size: 307518 Color: 1
Size: 297496 Color: 0

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 395018 Color: 1
Size: 322933 Color: 1
Size: 282050 Color: 0

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 395049 Color: 1
Size: 320375 Color: 1
Size: 284577 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 395081 Color: 1
Size: 320264 Color: 1
Size: 284656 Color: 0

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 395118 Color: 1
Size: 327428 Color: 1
Size: 277455 Color: 0

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 395126 Color: 1
Size: 312571 Color: 1
Size: 292304 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 395147 Color: 1
Size: 346794 Color: 1
Size: 258060 Color: 0

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 395157 Color: 1
Size: 313333 Color: 1
Size: 291511 Color: 0

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 395217 Color: 1
Size: 320474 Color: 1
Size: 284310 Color: 0

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 395260 Color: 1
Size: 318581 Color: 0
Size: 286160 Color: 1

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 395287 Color: 1
Size: 333258 Color: 1
Size: 271456 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 395348 Color: 1
Size: 341570 Color: 1
Size: 263083 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 395078 Color: 1
Size: 346612 Color: 1
Size: 258311 Color: 0

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 395431 Color: 1
Size: 313364 Color: 1
Size: 291206 Color: 0

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 395435 Color: 1
Size: 333729 Color: 1
Size: 270837 Color: 0

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 395460 Color: 1
Size: 325814 Color: 1
Size: 278727 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 395479 Color: 1
Size: 346066 Color: 1
Size: 258456 Color: 0

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 395511 Color: 1
Size: 321749 Color: 1
Size: 282741 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 395576 Color: 1
Size: 338391 Color: 1
Size: 266034 Color: 0

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 395571 Color: 1
Size: 311013 Color: 1
Size: 293417 Color: 0

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 395590 Color: 1
Size: 325387 Color: 1
Size: 279024 Color: 0

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 395713 Color: 1
Size: 348200 Color: 1
Size: 256088 Color: 0

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 395752 Color: 1
Size: 335663 Color: 1
Size: 268586 Color: 0

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 395775 Color: 1
Size: 334862 Color: 1
Size: 269364 Color: 0

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 395802 Color: 1
Size: 320422 Color: 1
Size: 283777 Color: 0

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 395807 Color: 1
Size: 345959 Color: 1
Size: 258235 Color: 0

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 395830 Color: 1
Size: 334039 Color: 1
Size: 270132 Color: 0

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 396155 Color: 1
Size: 350362 Color: 1
Size: 253484 Color: 0

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 396296 Color: 1
Size: 326107 Color: 1
Size: 277598 Color: 0

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 396352 Color: 1
Size: 330837 Color: 1
Size: 272812 Color: 0

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 396405 Color: 1
Size: 307736 Color: 1
Size: 295860 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 396443 Color: 1
Size: 340178 Color: 1
Size: 263380 Color: 0

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 396506 Color: 1
Size: 332805 Color: 1
Size: 270690 Color: 0

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 396555 Color: 1
Size: 327996 Color: 1
Size: 275450 Color: 0

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 396577 Color: 1
Size: 314153 Color: 1
Size: 289271 Color: 0

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 396583 Color: 1
Size: 306943 Color: 1
Size: 296475 Color: 0

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 396590 Color: 1
Size: 316864 Color: 1
Size: 286547 Color: 0

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 396623 Color: 1
Size: 341494 Color: 1
Size: 261884 Color: 0

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 396824 Color: 1
Size: 304945 Color: 1
Size: 298232 Color: 0

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 396853 Color: 1
Size: 301597 Color: 0
Size: 301551 Color: 1

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 396877 Color: 1
Size: 324293 Color: 1
Size: 278831 Color: 0

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 397111 Color: 1
Size: 329359 Color: 1
Size: 273531 Color: 0

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 397140 Color: 1
Size: 324023 Color: 1
Size: 278838 Color: 0

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 397206 Color: 1
Size: 332670 Color: 1
Size: 270125 Color: 0

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 397234 Color: 1
Size: 325931 Color: 1
Size: 276836 Color: 0

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 397255 Color: 1
Size: 324994 Color: 1
Size: 277752 Color: 0

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 397269 Color: 1
Size: 336770 Color: 1
Size: 265962 Color: 0

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 397333 Color: 1
Size: 352169 Color: 1
Size: 250499 Color: 0

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 397426 Color: 1
Size: 335459 Color: 1
Size: 267116 Color: 0

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 397474 Color: 1
Size: 335779 Color: 1
Size: 266748 Color: 0

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 397546 Color: 1
Size: 307344 Color: 0
Size: 295111 Color: 1

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 397592 Color: 1
Size: 303508 Color: 0
Size: 298901 Color: 1

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 397635 Color: 1
Size: 330199 Color: 1
Size: 272167 Color: 0

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 397654 Color: 1
Size: 340462 Color: 1
Size: 261885 Color: 0

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 397663 Color: 1
Size: 331741 Color: 1
Size: 270597 Color: 0

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 397706 Color: 1
Size: 309965 Color: 1
Size: 292330 Color: 0

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 397708 Color: 1
Size: 327992 Color: 1
Size: 274301 Color: 0

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 397746 Color: 1
Size: 340672 Color: 1
Size: 261583 Color: 0

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 397825 Color: 1
Size: 316749 Color: 1
Size: 285427 Color: 0

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 397882 Color: 1
Size: 326409 Color: 1
Size: 275710 Color: 0

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 398120 Color: 1
Size: 325074 Color: 1
Size: 276807 Color: 0

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 398132 Color: 1
Size: 318380 Color: 0
Size: 283489 Color: 1

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 398165 Color: 1
Size: 321234 Color: 1
Size: 280602 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 398216 Color: 1
Size: 320068 Color: 1
Size: 281717 Color: 0

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 398217 Color: 1
Size: 341762 Color: 1
Size: 260022 Color: 0

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 398264 Color: 1
Size: 332712 Color: 1
Size: 269025 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 398293 Color: 1
Size: 318041 Color: 1
Size: 283667 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 398453 Color: 1
Size: 313061 Color: 1
Size: 288487 Color: 0

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 398456 Color: 1
Size: 321867 Color: 1
Size: 279678 Color: 0

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 398738 Color: 1
Size: 310650 Color: 1
Size: 290613 Color: 0

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 398772 Color: 1
Size: 336306 Color: 1
Size: 264923 Color: 0

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 398777 Color: 1
Size: 341563 Color: 1
Size: 259661 Color: 0

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 398827 Color: 1
Size: 314355 Color: 1
Size: 286819 Color: 0

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 398861 Color: 1
Size: 314208 Color: 0
Size: 286932 Color: 1

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 398865 Color: 1
Size: 320567 Color: 1
Size: 280569 Color: 0

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 398950 Color: 1
Size: 333025 Color: 1
Size: 268026 Color: 0

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 398994 Color: 1
Size: 329949 Color: 1
Size: 271058 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 399016 Color: 1
Size: 332105 Color: 1
Size: 268880 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 399091 Color: 1
Size: 330885 Color: 1
Size: 270025 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 399199 Color: 1
Size: 305571 Color: 1
Size: 295231 Color: 0

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 399240 Color: 1
Size: 345719 Color: 1
Size: 255042 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 399293 Color: 1
Size: 327601 Color: 1
Size: 273107 Color: 0

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 399315 Color: 1
Size: 324781 Color: 1
Size: 275905 Color: 0

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 399361 Color: 1
Size: 332676 Color: 1
Size: 267964 Color: 0

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 399380 Color: 1
Size: 336407 Color: 1
Size: 264214 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 399431 Color: 1
Size: 329133 Color: 1
Size: 271437 Color: 0

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 399462 Color: 1
Size: 349429 Color: 1
Size: 251110 Color: 0

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 399491 Color: 1
Size: 331039 Color: 1
Size: 269471 Color: 0

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 399533 Color: 1
Size: 311127 Color: 1
Size: 289341 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 399586 Color: 1
Size: 316537 Color: 1
Size: 283878 Color: 0

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 399652 Color: 1
Size: 343056 Color: 1
Size: 257293 Color: 0

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 399669 Color: 1
Size: 325422 Color: 1
Size: 274910 Color: 0

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 399697 Color: 1
Size: 327050 Color: 1
Size: 273254 Color: 0

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 399700 Color: 1
Size: 325414 Color: 1
Size: 274887 Color: 0

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 399757 Color: 1
Size: 337058 Color: 1
Size: 263186 Color: 0

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 399795 Color: 1
Size: 320345 Color: 1
Size: 279861 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 399907 Color: 1
Size: 329274 Color: 1
Size: 270820 Color: 0

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 399913 Color: 1
Size: 314987 Color: 1
Size: 285101 Color: 0

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 399927 Color: 1
Size: 325622 Color: 1
Size: 274452 Color: 0

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 348027 Color: 1
Size: 346240 Color: 1
Size: 305734 Color: 0

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 399939 Color: 1
Size: 315158 Color: 1
Size: 284904 Color: 0

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 399949 Color: 1
Size: 321961 Color: 1
Size: 278091 Color: 0

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 399976 Color: 1
Size: 317030 Color: 1
Size: 282995 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 400103 Color: 1
Size: 308455 Color: 1
Size: 291443 Color: 0

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 400166 Color: 1
Size: 324230 Color: 1
Size: 275605 Color: 0

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 400270 Color: 1
Size: 323205 Color: 1
Size: 276526 Color: 0

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 400275 Color: 1
Size: 321181 Color: 1
Size: 278545 Color: 0

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 400302 Color: 1
Size: 337350 Color: 1
Size: 262349 Color: 0

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 400330 Color: 1
Size: 345783 Color: 1
Size: 253888 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 400356 Color: 1
Size: 337016 Color: 1
Size: 262629 Color: 0

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 400365 Color: 1
Size: 347665 Color: 1
Size: 251971 Color: 0

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 400375 Color: 1
Size: 318047 Color: 1
Size: 281579 Color: 0

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 400402 Color: 1
Size: 333093 Color: 1
Size: 266506 Color: 0

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 400469 Color: 1
Size: 304888 Color: 0
Size: 294644 Color: 1

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 400479 Color: 1
Size: 307753 Color: 1
Size: 291769 Color: 0

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 400512 Color: 1
Size: 312311 Color: 1
Size: 287178 Color: 0

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 400516 Color: 1
Size: 321512 Color: 1
Size: 277973 Color: 0

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 400634 Color: 1
Size: 311866 Color: 1
Size: 287501 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 400649 Color: 1
Size: 332348 Color: 1
Size: 267004 Color: 0

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 400733 Color: 1
Size: 329611 Color: 1
Size: 269657 Color: 0

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 400756 Color: 1
Size: 336097 Color: 1
Size: 263148 Color: 0

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 400770 Color: 1
Size: 332818 Color: 1
Size: 266413 Color: 0

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 400836 Color: 1
Size: 311801 Color: 0
Size: 287364 Color: 1

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 400917 Color: 1
Size: 328695 Color: 1
Size: 270389 Color: 0

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 401088 Color: 1
Size: 326981 Color: 1
Size: 271932 Color: 0

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 401167 Color: 1
Size: 309734 Color: 1
Size: 289100 Color: 0

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 401189 Color: 1
Size: 331981 Color: 1
Size: 266831 Color: 0

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 401195 Color: 1
Size: 316850 Color: 1
Size: 281956 Color: 0

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 401296 Color: 1
Size: 313665 Color: 0
Size: 285040 Color: 1

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 401311 Color: 1
Size: 323312 Color: 1
Size: 275378 Color: 0

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 401320 Color: 1
Size: 332444 Color: 1
Size: 266237 Color: 0

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 401342 Color: 1
Size: 336005 Color: 1
Size: 262654 Color: 0

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 401367 Color: 1
Size: 330234 Color: 1
Size: 268400 Color: 0

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 401383 Color: 1
Size: 341236 Color: 1
Size: 257382 Color: 0

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 401404 Color: 1
Size: 324672 Color: 1
Size: 273925 Color: 0

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 401407 Color: 1
Size: 330554 Color: 1
Size: 268040 Color: 0

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 401490 Color: 1
Size: 324492 Color: 1
Size: 274019 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 401584 Color: 1
Size: 340680 Color: 1
Size: 257737 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 401588 Color: 1
Size: 323332 Color: 1
Size: 275081 Color: 0

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 401680 Color: 1
Size: 302695 Color: 1
Size: 295626 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 401688 Color: 1
Size: 315965 Color: 1
Size: 282348 Color: 0

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 401748 Color: 1
Size: 328530 Color: 1
Size: 269723 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 401758 Color: 1
Size: 327546 Color: 1
Size: 270697 Color: 0

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 401913 Color: 1
Size: 335616 Color: 1
Size: 262472 Color: 0

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 401955 Color: 1
Size: 305573 Color: 1
Size: 292473 Color: 0

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 401987 Color: 1
Size: 320601 Color: 1
Size: 277413 Color: 0

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 401989 Color: 1
Size: 325472 Color: 1
Size: 272540 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 402025 Color: 1
Size: 317810 Color: 1
Size: 280166 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 402103 Color: 1
Size: 324309 Color: 1
Size: 273589 Color: 0

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 402123 Color: 1
Size: 337184 Color: 1
Size: 260694 Color: 0

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 402137 Color: 1
Size: 324981 Color: 1
Size: 272883 Color: 0

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 402174 Color: 1
Size: 301247 Color: 1
Size: 296580 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 402271 Color: 1
Size: 321521 Color: 1
Size: 276209 Color: 0

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 402332 Color: 1
Size: 329737 Color: 1
Size: 267932 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 402575 Color: 1
Size: 319822 Color: 1
Size: 277604 Color: 0

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 402653 Color: 1
Size: 326064 Color: 1
Size: 271284 Color: 0

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 402666 Color: 1
Size: 318210 Color: 1
Size: 279125 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 402800 Color: 1
Size: 338795 Color: 1
Size: 258406 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 402839 Color: 1
Size: 305981 Color: 0
Size: 291181 Color: 1

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 402896 Color: 1
Size: 314964 Color: 1
Size: 282141 Color: 0

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 402898 Color: 1
Size: 344480 Color: 1
Size: 252623 Color: 0

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 402902 Color: 1
Size: 334412 Color: 1
Size: 262687 Color: 0

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 402982 Color: 1
Size: 325109 Color: 1
Size: 271910 Color: 0

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 403036 Color: 1
Size: 329131 Color: 1
Size: 267834 Color: 0

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 403050 Color: 1
Size: 319645 Color: 1
Size: 277306 Color: 0

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 403058 Color: 1
Size: 340653 Color: 1
Size: 256290 Color: 0

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 403134 Color: 1
Size: 302531 Color: 1
Size: 294336 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 403145 Color: 1
Size: 326684 Color: 1
Size: 270172 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 403219 Color: 1
Size: 316070 Color: 1
Size: 280712 Color: 0

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 403235 Color: 1
Size: 319313 Color: 1
Size: 277453 Color: 0

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 403243 Color: 1
Size: 299738 Color: 1
Size: 297020 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 403259 Color: 1
Size: 334454 Color: 1
Size: 262288 Color: 0

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 403272 Color: 1
Size: 317040 Color: 1
Size: 279689 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 403330 Color: 1
Size: 315037 Color: 1
Size: 281634 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 403359 Color: 1
Size: 322542 Color: 1
Size: 274100 Color: 0

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 403413 Color: 1
Size: 299035 Color: 1
Size: 297553 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 403656 Color: 1
Size: 313043 Color: 1
Size: 283302 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 403716 Color: 1
Size: 318747 Color: 1
Size: 277538 Color: 0

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 403729 Color: 1
Size: 335689 Color: 1
Size: 260583 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 403757 Color: 1
Size: 325743 Color: 1
Size: 270501 Color: 0

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 403773 Color: 1
Size: 312757 Color: 1
Size: 283471 Color: 0

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 403821 Color: 1
Size: 332999 Color: 1
Size: 263181 Color: 0

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 403829 Color: 1
Size: 323378 Color: 1
Size: 272794 Color: 0

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 403871 Color: 1
Size: 340820 Color: 1
Size: 255310 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 403903 Color: 1
Size: 334287 Color: 1
Size: 261811 Color: 0

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 403968 Color: 1
Size: 318898 Color: 1
Size: 277135 Color: 0

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 404030 Color: 1
Size: 335585 Color: 1
Size: 260386 Color: 0

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 404034 Color: 1
Size: 326582 Color: 1
Size: 269385 Color: 0

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 404077 Color: 1
Size: 304527 Color: 1
Size: 291397 Color: 0

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 404099 Color: 1
Size: 331544 Color: 1
Size: 264358 Color: 0

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 404123 Color: 1
Size: 329236 Color: 1
Size: 266642 Color: 0

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 404141 Color: 1
Size: 311568 Color: 1
Size: 284292 Color: 0

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 404268 Color: 1
Size: 322455 Color: 1
Size: 273278 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 404292 Color: 1
Size: 332519 Color: 1
Size: 263190 Color: 0

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 404358 Color: 1
Size: 338738 Color: 1
Size: 256905 Color: 0

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 404441 Color: 1
Size: 312806 Color: 1
Size: 282754 Color: 0

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 404455 Color: 1
Size: 328785 Color: 1
Size: 266761 Color: 0

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 404519 Color: 1
Size: 316795 Color: 1
Size: 278687 Color: 0

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 404580 Color: 1
Size: 316326 Color: 1
Size: 279095 Color: 0

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 404977 Color: 1
Size: 340630 Color: 1
Size: 254394 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 405075 Color: 1
Size: 311405 Color: 1
Size: 283521 Color: 0

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 405086 Color: 1
Size: 323156 Color: 1
Size: 271759 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 405157 Color: 1
Size: 335667 Color: 1
Size: 259177 Color: 0

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 405279 Color: 1
Size: 328875 Color: 1
Size: 265847 Color: 0

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 405442 Color: 1
Size: 322509 Color: 1
Size: 272050 Color: 0

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 405442 Color: 1
Size: 311122 Color: 1
Size: 283437 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 405553 Color: 1
Size: 320845 Color: 1
Size: 273603 Color: 0

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 405554 Color: 1
Size: 325630 Color: 1
Size: 268817 Color: 0

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 405566 Color: 1
Size: 298529 Color: 0
Size: 295906 Color: 1

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 405644 Color: 1
Size: 303280 Color: 0
Size: 291077 Color: 1

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 405656 Color: 1
Size: 313912 Color: 1
Size: 280433 Color: 0

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 405743 Color: 1
Size: 302744 Color: 1
Size: 291514 Color: 0

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 405850 Color: 1
Size: 299188 Color: 1
Size: 294963 Color: 0

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 405922 Color: 1
Size: 326125 Color: 1
Size: 267954 Color: 0

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 406065 Color: 1
Size: 339436 Color: 1
Size: 254500 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 406146 Color: 1
Size: 299350 Color: 1
Size: 294505 Color: 0

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 406173 Color: 1
Size: 329145 Color: 1
Size: 264683 Color: 0

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 406208 Color: 1
Size: 307128 Color: 1
Size: 286665 Color: 0

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 406293 Color: 1
Size: 340360 Color: 1
Size: 253348 Color: 0

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 406343 Color: 1
Size: 316792 Color: 1
Size: 276866 Color: 0

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 406452 Color: 1
Size: 311418 Color: 1
Size: 282131 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 406452 Color: 1
Size: 310948 Color: 1
Size: 282601 Color: 0

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 406503 Color: 1
Size: 316704 Color: 1
Size: 276794 Color: 0

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 406535 Color: 1
Size: 315530 Color: 1
Size: 277936 Color: 0

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 406696 Color: 1
Size: 313856 Color: 1
Size: 279449 Color: 0

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 406797 Color: 1
Size: 323128 Color: 1
Size: 270076 Color: 0

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 406894 Color: 1
Size: 322112 Color: 1
Size: 270995 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 406897 Color: 1
Size: 306514 Color: 1
Size: 286590 Color: 0

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 406898 Color: 1
Size: 314013 Color: 1
Size: 279090 Color: 0

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 406973 Color: 1
Size: 325541 Color: 1
Size: 267487 Color: 0

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 407044 Color: 1
Size: 316225 Color: 1
Size: 276732 Color: 0

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 407097 Color: 1
Size: 328519 Color: 1
Size: 264385 Color: 0

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 407109 Color: 1
Size: 300103 Color: 1
Size: 292789 Color: 0

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 407111 Color: 1
Size: 302232 Color: 1
Size: 290658 Color: 0

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 407170 Color: 1
Size: 317544 Color: 1
Size: 275287 Color: 0

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 407236 Color: 1
Size: 312049 Color: 1
Size: 280716 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 407306 Color: 1
Size: 321189 Color: 1
Size: 271506 Color: 0

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 407373 Color: 1
Size: 304583 Color: 1
Size: 288045 Color: 0

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 407481 Color: 1
Size: 329952 Color: 1
Size: 262568 Color: 0

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 407565 Color: 1
Size: 315280 Color: 1
Size: 277156 Color: 0

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 407575 Color: 1
Size: 307610 Color: 1
Size: 284816 Color: 0

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 407712 Color: 1
Size: 316304 Color: 1
Size: 275985 Color: 0

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 407714 Color: 1
Size: 333128 Color: 1
Size: 259159 Color: 0

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 407732 Color: 1
Size: 323966 Color: 1
Size: 268303 Color: 0

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 407738 Color: 1
Size: 335883 Color: 1
Size: 256380 Color: 0

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 407765 Color: 1
Size: 310166 Color: 1
Size: 282070 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 407935 Color: 1
Size: 297673 Color: 0
Size: 294393 Color: 1

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 408005 Color: 1
Size: 311018 Color: 1
Size: 280978 Color: 0

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 408053 Color: 1
Size: 323848 Color: 1
Size: 268100 Color: 0

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 408088 Color: 1
Size: 314421 Color: 0
Size: 277492 Color: 1

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 408119 Color: 1
Size: 338313 Color: 1
Size: 253569 Color: 0

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 408289 Color: 1
Size: 329068 Color: 1
Size: 262644 Color: 0

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 408310 Color: 1
Size: 328288 Color: 1
Size: 263403 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 408390 Color: 1
Size: 316497 Color: 1
Size: 275114 Color: 0

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 408438 Color: 1
Size: 329680 Color: 1
Size: 261883 Color: 0

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 408503 Color: 1
Size: 321946 Color: 1
Size: 269552 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 408584 Color: 1
Size: 324438 Color: 1
Size: 266979 Color: 0

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 408607 Color: 1
Size: 327901 Color: 1
Size: 263493 Color: 0

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 408660 Color: 1
Size: 335339 Color: 1
Size: 256002 Color: 0

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 408757 Color: 1
Size: 315114 Color: 1
Size: 276130 Color: 0

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 408788 Color: 1
Size: 309585 Color: 1
Size: 281628 Color: 0

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 408799 Color: 1
Size: 307423 Color: 0
Size: 283779 Color: 1

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 408803 Color: 1
Size: 314627 Color: 1
Size: 276571 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 408821 Color: 1
Size: 307602 Color: 1
Size: 283578 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 408905 Color: 1
Size: 312837 Color: 1
Size: 278259 Color: 0

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 408934 Color: 1
Size: 338693 Color: 1
Size: 252374 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 409136 Color: 1
Size: 336995 Color: 1
Size: 253870 Color: 0

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 409148 Color: 1
Size: 296013 Color: 0
Size: 294840 Color: 1

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 409302 Color: 1
Size: 331677 Color: 1
Size: 259022 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 409371 Color: 1
Size: 333373 Color: 1
Size: 257257 Color: 0

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 409402 Color: 1
Size: 318492 Color: 1
Size: 272107 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 409423 Color: 1
Size: 311904 Color: 1
Size: 278674 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 409434 Color: 1
Size: 327607 Color: 1
Size: 262960 Color: 0

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 409447 Color: 1
Size: 334694 Color: 1
Size: 255860 Color: 0

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 409469 Color: 1
Size: 315475 Color: 1
Size: 275057 Color: 0

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 409520 Color: 1
Size: 302572 Color: 1
Size: 287909 Color: 0

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 409526 Color: 1
Size: 326015 Color: 1
Size: 264460 Color: 0

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 409528 Color: 1
Size: 305992 Color: 1
Size: 284481 Color: 0

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 409630 Color: 1
Size: 315673 Color: 1
Size: 274698 Color: 0

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 409711 Color: 1
Size: 311068 Color: 1
Size: 279222 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 409719 Color: 1
Size: 308098 Color: 1
Size: 282184 Color: 0

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 409865 Color: 1
Size: 299311 Color: 1
Size: 290825 Color: 0

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 409903 Color: 1
Size: 310839 Color: 1
Size: 279259 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 409928 Color: 1
Size: 338433 Color: 1
Size: 251640 Color: 0

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 409982 Color: 1
Size: 317753 Color: 1
Size: 272266 Color: 0

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 410004 Color: 1
Size: 322141 Color: 1
Size: 267856 Color: 0

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 410017 Color: 1
Size: 335502 Color: 1
Size: 254482 Color: 0

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 410056 Color: 1
Size: 321364 Color: 1
Size: 268581 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 410072 Color: 1
Size: 329367 Color: 1
Size: 260562 Color: 0

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 410142 Color: 1
Size: 306979 Color: 0
Size: 282880 Color: 1

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 410198 Color: 1
Size: 327913 Color: 1
Size: 261890 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 410206 Color: 1
Size: 319530 Color: 1
Size: 270265 Color: 0

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 410526 Color: 1
Size: 326812 Color: 1
Size: 262663 Color: 0

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 410569 Color: 1
Size: 331877 Color: 1
Size: 257555 Color: 0

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 410648 Color: 1
Size: 334214 Color: 1
Size: 255139 Color: 0

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 410576 Color: 1
Size: 303416 Color: 1
Size: 286009 Color: 0

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 410718 Color: 1
Size: 336036 Color: 1
Size: 253247 Color: 0

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 410758 Color: 1
Size: 320461 Color: 1
Size: 268782 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 410905 Color: 1
Size: 337889 Color: 1
Size: 251207 Color: 0

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 411013 Color: 1
Size: 322080 Color: 1
Size: 266908 Color: 0

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 411025 Color: 1
Size: 303328 Color: 1
Size: 285648 Color: 0

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 411134 Color: 1
Size: 315640 Color: 1
Size: 273227 Color: 0

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 411143 Color: 1
Size: 335923 Color: 1
Size: 252935 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 411150 Color: 1
Size: 332561 Color: 1
Size: 256290 Color: 0

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 411240 Color: 1
Size: 329641 Color: 1
Size: 259120 Color: 0

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 411349 Color: 1
Size: 308512 Color: 0
Size: 280140 Color: 1

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 411454 Color: 1
Size: 314432 Color: 1
Size: 274115 Color: 0

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 411465 Color: 1
Size: 329137 Color: 1
Size: 259399 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 411513 Color: 1
Size: 307876 Color: 1
Size: 280612 Color: 0

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 411642 Color: 1
Size: 325840 Color: 1
Size: 262519 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 411666 Color: 1
Size: 326108 Color: 1
Size: 262227 Color: 0

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 411680 Color: 1
Size: 315715 Color: 1
Size: 272606 Color: 0

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 411713 Color: 1
Size: 299223 Color: 0
Size: 289065 Color: 1

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 411844 Color: 1
Size: 321522 Color: 1
Size: 266635 Color: 0

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 411848 Color: 1
Size: 313582 Color: 1
Size: 274571 Color: 0

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 411863 Color: 1
Size: 308221 Color: 1
Size: 279917 Color: 0

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 411942 Color: 1
Size: 315719 Color: 1
Size: 272340 Color: 0

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 412109 Color: 1
Size: 315419 Color: 1
Size: 272473 Color: 0

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 412113 Color: 1
Size: 295310 Color: 1
Size: 292578 Color: 0

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 412197 Color: 1
Size: 335578 Color: 1
Size: 252226 Color: 0

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 412248 Color: 1
Size: 320420 Color: 1
Size: 267333 Color: 0

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 412255 Color: 1
Size: 326121 Color: 1
Size: 261625 Color: 0

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 412267 Color: 1
Size: 320743 Color: 0
Size: 266991 Color: 1

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 412287 Color: 1
Size: 304980 Color: 1
Size: 282734 Color: 0

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 412345 Color: 1
Size: 298786 Color: 0
Size: 288870 Color: 1

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 412577 Color: 1
Size: 304390 Color: 1
Size: 283034 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 412594 Color: 1
Size: 331397 Color: 1
Size: 256010 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 412634 Color: 1
Size: 327356 Color: 1
Size: 260011 Color: 0

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 412658 Color: 1
Size: 325674 Color: 1
Size: 261669 Color: 0

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 412698 Color: 1
Size: 306122 Color: 1
Size: 281181 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 412714 Color: 1
Size: 330462 Color: 1
Size: 256825 Color: 0

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 412719 Color: 1
Size: 332817 Color: 1
Size: 254465 Color: 0

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 412785 Color: 1
Size: 327377 Color: 1
Size: 259839 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 412798 Color: 1
Size: 320233 Color: 1
Size: 266970 Color: 0

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 412799 Color: 1
Size: 305409 Color: 1
Size: 281793 Color: 0

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 412856 Color: 1
Size: 311374 Color: 1
Size: 275771 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 413033 Color: 1
Size: 335687 Color: 1
Size: 251281 Color: 0

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 413088 Color: 1
Size: 333953 Color: 1
Size: 252960 Color: 0

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 413226 Color: 1
Size: 312381 Color: 1
Size: 274394 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 413296 Color: 1
Size: 328116 Color: 1
Size: 258589 Color: 0

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 413406 Color: 1
Size: 310705 Color: 1
Size: 275890 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 413469 Color: 1
Size: 318269 Color: 1
Size: 268263 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 413490 Color: 1
Size: 319361 Color: 1
Size: 267150 Color: 0

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 413503 Color: 1
Size: 319476 Color: 1
Size: 267022 Color: 0

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 413530 Color: 1
Size: 332937 Color: 1
Size: 253534 Color: 0

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 413571 Color: 1
Size: 308648 Color: 1
Size: 277782 Color: 0

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 413576 Color: 1
Size: 333620 Color: 1
Size: 252805 Color: 0

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 413596 Color: 1
Size: 305337 Color: 1
Size: 281068 Color: 0

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 413638 Color: 1
Size: 295601 Color: 0
Size: 290762 Color: 1

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 413748 Color: 1
Size: 320734 Color: 1
Size: 265519 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 413872 Color: 1
Size: 327848 Color: 1
Size: 258281 Color: 0

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 413878 Color: 1
Size: 323806 Color: 1
Size: 262317 Color: 0

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 413889 Color: 1
Size: 304874 Color: 1
Size: 281238 Color: 0

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 413913 Color: 1
Size: 312133 Color: 0
Size: 273955 Color: 1

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 413987 Color: 1
Size: 333485 Color: 1
Size: 252529 Color: 0

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 414091 Color: 1
Size: 318833 Color: 1
Size: 267077 Color: 0

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 414117 Color: 1
Size: 307359 Color: 1
Size: 278525 Color: 0

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 414152 Color: 1
Size: 310907 Color: 1
Size: 274942 Color: 0

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 414166 Color: 1
Size: 331059 Color: 1
Size: 254776 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 414184 Color: 1
Size: 320864 Color: 1
Size: 264953 Color: 0

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 414249 Color: 1
Size: 322416 Color: 1
Size: 263336 Color: 0

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 414251 Color: 1
Size: 330187 Color: 1
Size: 255563 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 414267 Color: 1
Size: 297639 Color: 1
Size: 288095 Color: 0

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 414334 Color: 1
Size: 325225 Color: 1
Size: 260442 Color: 0

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 414395 Color: 1
Size: 292820 Color: 0
Size: 292786 Color: 1

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 414415 Color: 1
Size: 325885 Color: 1
Size: 259701 Color: 0

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 414428 Color: 1
Size: 308548 Color: 1
Size: 277025 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 414489 Color: 1
Size: 315752 Color: 1
Size: 269760 Color: 0

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 414497 Color: 1
Size: 332351 Color: 1
Size: 253153 Color: 0

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 414576 Color: 1
Size: 315426 Color: 1
Size: 269999 Color: 0

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 414650 Color: 1
Size: 320858 Color: 1
Size: 264493 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 414710 Color: 1
Size: 305723 Color: 1
Size: 279568 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 414825 Color: 1
Size: 316870 Color: 1
Size: 268306 Color: 0

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 414833 Color: 1
Size: 310722 Color: 1
Size: 274446 Color: 0

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 414992 Color: 1
Size: 318405 Color: 1
Size: 266604 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 415020 Color: 1
Size: 327585 Color: 1
Size: 257396 Color: 0

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 415192 Color: 1
Size: 309811 Color: 1
Size: 274998 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 415206 Color: 1
Size: 328596 Color: 1
Size: 256199 Color: 0

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 415210 Color: 1
Size: 314598 Color: 1
Size: 270193 Color: 0

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 415267 Color: 1
Size: 308279 Color: 1
Size: 276455 Color: 0

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 415350 Color: 1
Size: 298836 Color: 1
Size: 285815 Color: 0

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 415400 Color: 1
Size: 327566 Color: 1
Size: 257035 Color: 0

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 415467 Color: 1
Size: 314691 Color: 1
Size: 269843 Color: 0

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 415560 Color: 1
Size: 300368 Color: 1
Size: 284073 Color: 0

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 415608 Color: 1
Size: 299497 Color: 0
Size: 284896 Color: 1

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 415615 Color: 1
Size: 329416 Color: 1
Size: 254970 Color: 0

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 415666 Color: 1
Size: 304515 Color: 1
Size: 279820 Color: 0

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 415696 Color: 1
Size: 315270 Color: 1
Size: 269035 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 415728 Color: 1
Size: 326447 Color: 1
Size: 257826 Color: 0

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 415734 Color: 1
Size: 321625 Color: 1
Size: 262642 Color: 0

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 415904 Color: 1
Size: 318576 Color: 1
Size: 265521 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 415928 Color: 1
Size: 317120 Color: 1
Size: 266953 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 415929 Color: 1
Size: 298370 Color: 1
Size: 285702 Color: 0

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 415933 Color: 1
Size: 312147 Color: 1
Size: 271921 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 416035 Color: 1
Size: 308851 Color: 1
Size: 275115 Color: 0

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 416111 Color: 1
Size: 318612 Color: 1
Size: 265278 Color: 0

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 416222 Color: 1
Size: 320375 Color: 1
Size: 263404 Color: 0

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 416282 Color: 1
Size: 321540 Color: 1
Size: 262179 Color: 0

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 416286 Color: 1
Size: 322929 Color: 1
Size: 260786 Color: 0

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 416392 Color: 1
Size: 304933 Color: 1
Size: 278676 Color: 0

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 416398 Color: 1
Size: 308802 Color: 0
Size: 274801 Color: 1

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 416526 Color: 1
Size: 293937 Color: 1
Size: 289538 Color: 0

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 416561 Color: 1
Size: 319853 Color: 1
Size: 263587 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 416572 Color: 1
Size: 312115 Color: 1
Size: 271314 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 416637 Color: 1
Size: 331183 Color: 1
Size: 252181 Color: 0

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 416671 Color: 1
Size: 301487 Color: 0
Size: 281843 Color: 1

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 416738 Color: 1
Size: 299754 Color: 1
Size: 283509 Color: 0

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 416810 Color: 1
Size: 304949 Color: 1
Size: 278242 Color: 0

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 416862 Color: 1
Size: 312816 Color: 1
Size: 270323 Color: 0

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 416864 Color: 1
Size: 309155 Color: 1
Size: 273982 Color: 0

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 416958 Color: 1
Size: 317924 Color: 1
Size: 265119 Color: 0

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 417116 Color: 1
Size: 293488 Color: 1
Size: 289397 Color: 0

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 417128 Color: 1
Size: 322431 Color: 1
Size: 260442 Color: 0

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 417130 Color: 1
Size: 299512 Color: 1
Size: 283359 Color: 0

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 417136 Color: 1
Size: 316747 Color: 1
Size: 266118 Color: 0

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 417264 Color: 1
Size: 317754 Color: 1
Size: 264983 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 417287 Color: 1
Size: 300565 Color: 1
Size: 282149 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 417325 Color: 1
Size: 311661 Color: 1
Size: 271015 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 417423 Color: 1
Size: 332160 Color: 1
Size: 250418 Color: 0

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 417463 Color: 1
Size: 321273 Color: 1
Size: 261265 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 417501 Color: 1
Size: 308006 Color: 1
Size: 274494 Color: 0

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 417710 Color: 1
Size: 324103 Color: 1
Size: 258188 Color: 0

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 417781 Color: 1
Size: 303411 Color: 1
Size: 278809 Color: 0

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 417815 Color: 1
Size: 313596 Color: 1
Size: 268590 Color: 0

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 417950 Color: 1
Size: 317167 Color: 0
Size: 264884 Color: 1

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 418097 Color: 1
Size: 320602 Color: 1
Size: 261302 Color: 0

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 418147 Color: 1
Size: 324351 Color: 1
Size: 257503 Color: 0

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 418147 Color: 1
Size: 322976 Color: 1
Size: 258878 Color: 0

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 418222 Color: 1
Size: 298353 Color: 1
Size: 283426 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 418254 Color: 1
Size: 297952 Color: 0
Size: 283795 Color: 1

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 418312 Color: 1
Size: 316753 Color: 1
Size: 264936 Color: 0

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 418329 Color: 1
Size: 328092 Color: 1
Size: 253580 Color: 0

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 418383 Color: 1
Size: 314429 Color: 1
Size: 267189 Color: 0

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 418387 Color: 1
Size: 317016 Color: 1
Size: 264598 Color: 0

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 418404 Color: 1
Size: 306441 Color: 1
Size: 275156 Color: 0

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 418546 Color: 1
Size: 323573 Color: 1
Size: 257882 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 418603 Color: 1
Size: 319858 Color: 1
Size: 261540 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 418874 Color: 1
Size: 304460 Color: 1
Size: 276667 Color: 0

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 418892 Color: 1
Size: 318194 Color: 1
Size: 262915 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 418999 Color: 1
Size: 311212 Color: 1
Size: 269790 Color: 0

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 419250 Color: 1
Size: 312134 Color: 1
Size: 268617 Color: 0

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 419296 Color: 1
Size: 294103 Color: 0
Size: 286602 Color: 1

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 419299 Color: 1
Size: 290747 Color: 1
Size: 289955 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 419337 Color: 1
Size: 305064 Color: 1
Size: 275600 Color: 0

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 419485 Color: 1
Size: 321940 Color: 1
Size: 258576 Color: 0

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 419550 Color: 1
Size: 317609 Color: 1
Size: 262842 Color: 0

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 419658 Color: 1
Size: 290597 Color: 0
Size: 289746 Color: 1

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 419672 Color: 1
Size: 309389 Color: 1
Size: 270940 Color: 0

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 419707 Color: 1
Size: 323624 Color: 1
Size: 256670 Color: 0

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 419719 Color: 1
Size: 316675 Color: 1
Size: 263607 Color: 0

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 419766 Color: 1
Size: 317833 Color: 1
Size: 262402 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 419843 Color: 1
Size: 305457 Color: 1
Size: 274701 Color: 0

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 419920 Color: 1
Size: 319232 Color: 1
Size: 260849 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 419926 Color: 1
Size: 311721 Color: 1
Size: 268354 Color: 0

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 419939 Color: 1
Size: 299596 Color: 1
Size: 280466 Color: 0

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 420027 Color: 1
Size: 295580 Color: 0
Size: 284394 Color: 1

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 420042 Color: 1
Size: 317589 Color: 1
Size: 262370 Color: 0

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 420046 Color: 1
Size: 305240 Color: 1
Size: 274715 Color: 0

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 420056 Color: 1
Size: 311882 Color: 1
Size: 268063 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 360407 Color: 1
Size: 358564 Color: 1
Size: 281030 Color: 0

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 420357 Color: 1
Size: 312760 Color: 1
Size: 266884 Color: 0

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 420389 Color: 1
Size: 320899 Color: 1
Size: 258713 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 420445 Color: 1
Size: 307831 Color: 1
Size: 271725 Color: 0

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 420477 Color: 1
Size: 289834 Color: 0
Size: 289690 Color: 1

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 420558 Color: 1
Size: 305686 Color: 1
Size: 273757 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 420738 Color: 1
Size: 305161 Color: 1
Size: 274102 Color: 0

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 420838 Color: 1
Size: 324614 Color: 1
Size: 254549 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 420896 Color: 1
Size: 322132 Color: 1
Size: 256973 Color: 0

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 420909 Color: 1
Size: 316649 Color: 1
Size: 262443 Color: 0

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 420937 Color: 1
Size: 315753 Color: 1
Size: 263311 Color: 0

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 420954 Color: 1
Size: 327967 Color: 1
Size: 251080 Color: 0

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 420997 Color: 1
Size: 308437 Color: 1
Size: 270567 Color: 0

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 421043 Color: 1
Size: 316118 Color: 1
Size: 262840 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 421062 Color: 1
Size: 307756 Color: 1
Size: 271183 Color: 0

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 421066 Color: 1
Size: 308741 Color: 1
Size: 270194 Color: 0

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 421152 Color: 1
Size: 302730 Color: 1
Size: 276119 Color: 0

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 421154 Color: 1
Size: 289729 Color: 1
Size: 289118 Color: 0

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 421232 Color: 1
Size: 315410 Color: 1
Size: 263359 Color: 0

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 421238 Color: 1
Size: 325357 Color: 0
Size: 253406 Color: 1

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 421273 Color: 1
Size: 322562 Color: 1
Size: 256166 Color: 0

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 421413 Color: 1
Size: 315172 Color: 1
Size: 263416 Color: 0

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 421474 Color: 1
Size: 322542 Color: 1
Size: 255985 Color: 0

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 421523 Color: 1
Size: 315145 Color: 1
Size: 263333 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 421601 Color: 1
Size: 322931 Color: 1
Size: 255469 Color: 0

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 421633 Color: 1
Size: 290890 Color: 1
Size: 287478 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 421861 Color: 1
Size: 303190 Color: 1
Size: 274950 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 421972 Color: 1
Size: 320857 Color: 1
Size: 257172 Color: 0

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 422037 Color: 1
Size: 313346 Color: 1
Size: 264618 Color: 0

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 422080 Color: 1
Size: 297382 Color: 1
Size: 280539 Color: 0

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 422168 Color: 1
Size: 317400 Color: 1
Size: 260433 Color: 0

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 422215 Color: 1
Size: 323054 Color: 1
Size: 254732 Color: 0

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 422304 Color: 1
Size: 307534 Color: 1
Size: 270163 Color: 0

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 422419 Color: 1
Size: 311649 Color: 1
Size: 265933 Color: 0

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 422461 Color: 1
Size: 316410 Color: 0
Size: 261130 Color: 1

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 422501 Color: 1
Size: 303087 Color: 1
Size: 274413 Color: 0

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 422508 Color: 1
Size: 296020 Color: 0
Size: 281473 Color: 1

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 422537 Color: 1
Size: 317407 Color: 1
Size: 260057 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 422560 Color: 1
Size: 309121 Color: 1
Size: 268320 Color: 0

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 422570 Color: 1
Size: 301771 Color: 1
Size: 275660 Color: 0

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 422619 Color: 1
Size: 315268 Color: 1
Size: 262114 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 422674 Color: 1
Size: 310166 Color: 1
Size: 267161 Color: 0

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 422679 Color: 1
Size: 322062 Color: 1
Size: 255260 Color: 0

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 422696 Color: 1
Size: 303281 Color: 1
Size: 274024 Color: 0

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 422758 Color: 1
Size: 305662 Color: 1
Size: 271581 Color: 0

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 422762 Color: 1
Size: 295335 Color: 1
Size: 281904 Color: 0

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 422837 Color: 1
Size: 314882 Color: 1
Size: 262282 Color: 0

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 422852 Color: 1
Size: 314010 Color: 1
Size: 263139 Color: 0

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 422877 Color: 1
Size: 300181 Color: 1
Size: 276943 Color: 0

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 423012 Color: 1
Size: 317837 Color: 1
Size: 259152 Color: 0

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 423064 Color: 1
Size: 303328 Color: 1
Size: 273609 Color: 0

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 423118 Color: 1
Size: 310792 Color: 1
Size: 266091 Color: 0

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 423207 Color: 1
Size: 320939 Color: 1
Size: 255855 Color: 0

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 423236 Color: 1
Size: 320643 Color: 1
Size: 256122 Color: 0

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 423386 Color: 1
Size: 320752 Color: 1
Size: 255863 Color: 0

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 423421 Color: 1
Size: 321198 Color: 1
Size: 255382 Color: 0

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 423474 Color: 1
Size: 300988 Color: 0
Size: 275539 Color: 1

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 423495 Color: 1
Size: 289664 Color: 1
Size: 286842 Color: 0

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 423552 Color: 1
Size: 298155 Color: 1
Size: 278294 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 423637 Color: 1
Size: 312716 Color: 1
Size: 263648 Color: 0

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 423687 Color: 1
Size: 324909 Color: 1
Size: 251405 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 423874 Color: 1
Size: 308004 Color: 1
Size: 268123 Color: 0

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 423922 Color: 1
Size: 295884 Color: 1
Size: 280195 Color: 0

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 424053 Color: 1
Size: 321741 Color: 1
Size: 254207 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 424058 Color: 1
Size: 302373 Color: 1
Size: 273570 Color: 0

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 424152 Color: 1
Size: 305312 Color: 1
Size: 270537 Color: 0

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 424202 Color: 1
Size: 303650 Color: 1
Size: 272149 Color: 0

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 424210 Color: 1
Size: 301245 Color: 1
Size: 274546 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 424235 Color: 1
Size: 319893 Color: 1
Size: 255873 Color: 0

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 424311 Color: 1
Size: 305280 Color: 0
Size: 270410 Color: 1

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 424543 Color: 1
Size: 314205 Color: 1
Size: 261253 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 424580 Color: 1
Size: 305397 Color: 1
Size: 270024 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 358600 Color: 1
Size: 333624 Color: 1
Size: 307777 Color: 0

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 424635 Color: 1
Size: 308586 Color: 1
Size: 266780 Color: 0

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 424642 Color: 1
Size: 293780 Color: 0
Size: 281579 Color: 1

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 424702 Color: 1
Size: 308666 Color: 1
Size: 266633 Color: 0

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 424729 Color: 1
Size: 313099 Color: 1
Size: 262173 Color: 0

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 424738 Color: 1
Size: 314618 Color: 1
Size: 260645 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 424863 Color: 1
Size: 319923 Color: 1
Size: 255215 Color: 0

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 424954 Color: 1
Size: 323732 Color: 1
Size: 251315 Color: 0

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 425025 Color: 1
Size: 300393 Color: 0
Size: 274583 Color: 1

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 425044 Color: 1
Size: 315834 Color: 1
Size: 259123 Color: 0

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 425164 Color: 1
Size: 289816 Color: 0
Size: 285021 Color: 1

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 425189 Color: 1
Size: 321257 Color: 1
Size: 253555 Color: 0

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 425247 Color: 1
Size: 321914 Color: 1
Size: 252840 Color: 0

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 425360 Color: 1
Size: 314393 Color: 1
Size: 260248 Color: 0

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 425431 Color: 1
Size: 300697 Color: 1
Size: 273873 Color: 0

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 425509 Color: 1
Size: 321368 Color: 1
Size: 253124 Color: 0

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 425514 Color: 1
Size: 308735 Color: 1
Size: 265752 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 425575 Color: 1
Size: 307036 Color: 1
Size: 267390 Color: 0

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 425594 Color: 1
Size: 321705 Color: 1
Size: 252702 Color: 0

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 425600 Color: 1
Size: 316888 Color: 1
Size: 257513 Color: 0

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 425629 Color: 1
Size: 316429 Color: 1
Size: 257943 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 425775 Color: 1
Size: 309543 Color: 1
Size: 264683 Color: 0

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 425831 Color: 1
Size: 307774 Color: 1
Size: 266396 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 425891 Color: 1
Size: 306905 Color: 1
Size: 267205 Color: 0

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 425945 Color: 1
Size: 321639 Color: 1
Size: 252417 Color: 0

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 425971 Color: 1
Size: 318632 Color: 1
Size: 255398 Color: 0

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 426064 Color: 1
Size: 297063 Color: 1
Size: 276874 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 426111 Color: 1
Size: 297457 Color: 0
Size: 276433 Color: 1

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 426120 Color: 1
Size: 315568 Color: 1
Size: 258313 Color: 0

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 426158 Color: 1
Size: 319972 Color: 1
Size: 253871 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 426217 Color: 1
Size: 320176 Color: 1
Size: 253608 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 426271 Color: 1
Size: 306329 Color: 1
Size: 267401 Color: 0

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 426347 Color: 1
Size: 306069 Color: 1
Size: 267585 Color: 0

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 426358 Color: 1
Size: 317608 Color: 1
Size: 256035 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 426429 Color: 1
Size: 308887 Color: 1
Size: 264685 Color: 0

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 426435 Color: 1
Size: 310534 Color: 1
Size: 263032 Color: 0

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 426670 Color: 1
Size: 320118 Color: 1
Size: 253213 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 426745 Color: 1
Size: 312993 Color: 1
Size: 260263 Color: 0

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 426815 Color: 1
Size: 309656 Color: 1
Size: 263530 Color: 0

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 426935 Color: 1
Size: 306494 Color: 1
Size: 266572 Color: 0

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 426965 Color: 1
Size: 318424 Color: 1
Size: 254612 Color: 0

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 427069 Color: 1
Size: 314767 Color: 1
Size: 258165 Color: 0

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 427163 Color: 1
Size: 306089 Color: 1
Size: 266749 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 427173 Color: 1
Size: 299542 Color: 1
Size: 273286 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 427175 Color: 1
Size: 303172 Color: 1
Size: 269654 Color: 0

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 427313 Color: 1
Size: 287577 Color: 0
Size: 285111 Color: 1

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 427450 Color: 1
Size: 312710 Color: 1
Size: 259841 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 427475 Color: 1
Size: 296319 Color: 1
Size: 276207 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 427624 Color: 1
Size: 298622 Color: 1
Size: 273755 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 427657 Color: 1
Size: 301235 Color: 1
Size: 271109 Color: 0

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 427684 Color: 1
Size: 303340 Color: 1
Size: 268977 Color: 0

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 427704 Color: 1
Size: 295700 Color: 1
Size: 276597 Color: 0

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 427736 Color: 1
Size: 312105 Color: 1
Size: 260160 Color: 0

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 427884 Color: 1
Size: 317506 Color: 1
Size: 254611 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 427942 Color: 1
Size: 292695 Color: 1
Size: 279364 Color: 0

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 428047 Color: 1
Size: 307012 Color: 1
Size: 264942 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 428075 Color: 1
Size: 307110 Color: 1
Size: 264816 Color: 0

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 428224 Color: 1
Size: 320205 Color: 1
Size: 251572 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 428289 Color: 1
Size: 295157 Color: 1
Size: 276555 Color: 0

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 428298 Color: 1
Size: 307776 Color: 1
Size: 263927 Color: 0

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 428410 Color: 1
Size: 306682 Color: 1
Size: 264909 Color: 0

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 428430 Color: 1
Size: 316005 Color: 1
Size: 255566 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 428435 Color: 1
Size: 311604 Color: 1
Size: 259962 Color: 0

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 428460 Color: 1
Size: 291783 Color: 1
Size: 279758 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 428599 Color: 1
Size: 310189 Color: 1
Size: 261213 Color: 0

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 428649 Color: 1
Size: 290042 Color: 0
Size: 281310 Color: 1

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 428681 Color: 1
Size: 288590 Color: 0
Size: 282730 Color: 1

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 428752 Color: 1
Size: 318218 Color: 1
Size: 253031 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 428821 Color: 1
Size: 297026 Color: 0
Size: 274154 Color: 1

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 428828 Color: 1
Size: 313101 Color: 1
Size: 258072 Color: 0

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 428844 Color: 1
Size: 308561 Color: 1
Size: 262596 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 428867 Color: 1
Size: 300046 Color: 1
Size: 271088 Color: 0

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 429230 Color: 1
Size: 299259 Color: 1
Size: 271512 Color: 0

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 429321 Color: 1
Size: 310556 Color: 1
Size: 260124 Color: 0

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 429450 Color: 1
Size: 285642 Color: 0
Size: 284909 Color: 1

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 429490 Color: 1
Size: 285634 Color: 1
Size: 284877 Color: 0

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 429549 Color: 1
Size: 313027 Color: 1
Size: 257425 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 429685 Color: 1
Size: 302252 Color: 1
Size: 268064 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 429691 Color: 1
Size: 290435 Color: 0
Size: 279875 Color: 1

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 429816 Color: 1
Size: 306612 Color: 1
Size: 263573 Color: 0

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 429866 Color: 1
Size: 303455 Color: 1
Size: 266680 Color: 0

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 429944 Color: 1
Size: 308934 Color: 1
Size: 261123 Color: 0

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 429960 Color: 1
Size: 314638 Color: 1
Size: 255403 Color: 0

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 429999 Color: 1
Size: 313821 Color: 1
Size: 256181 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 430031 Color: 1
Size: 290686 Color: 1
Size: 279284 Color: 0

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 430083 Color: 1
Size: 315092 Color: 1
Size: 254826 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 430219 Color: 1
Size: 314326 Color: 1
Size: 255456 Color: 0

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 430246 Color: 1
Size: 297193 Color: 1
Size: 272562 Color: 0

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 430253 Color: 1
Size: 315318 Color: 1
Size: 254430 Color: 0

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 430282 Color: 1
Size: 310723 Color: 1
Size: 258996 Color: 0

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 430298 Color: 1
Size: 304656 Color: 1
Size: 265047 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 430424 Color: 1
Size: 302114 Color: 1
Size: 267463 Color: 0

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 430470 Color: 1
Size: 315069 Color: 1
Size: 254462 Color: 0

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 430497 Color: 1
Size: 299335 Color: 1
Size: 270169 Color: 0

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 430509 Color: 1
Size: 312050 Color: 1
Size: 257442 Color: 0

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 430552 Color: 1
Size: 288587 Color: 1
Size: 280862 Color: 0

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 430588 Color: 1
Size: 304974 Color: 1
Size: 264439 Color: 0

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 430594 Color: 1
Size: 289471 Color: 0
Size: 279936 Color: 1

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 430633 Color: 1
Size: 299236 Color: 1
Size: 270132 Color: 0

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 430737 Color: 1
Size: 315177 Color: 1
Size: 254087 Color: 0

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 430751 Color: 1
Size: 317450 Color: 1
Size: 251800 Color: 0

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 430822 Color: 1
Size: 298170 Color: 1
Size: 271009 Color: 0

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 431023 Color: 1
Size: 304434 Color: 1
Size: 264544 Color: 0

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 431068 Color: 1
Size: 312997 Color: 1
Size: 255936 Color: 0

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 431154 Color: 1
Size: 318348 Color: 1
Size: 250499 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 431198 Color: 1
Size: 288522 Color: 1
Size: 280281 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 431260 Color: 1
Size: 297714 Color: 1
Size: 271027 Color: 0

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 431261 Color: 1
Size: 307343 Color: 1
Size: 261397 Color: 0

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 431298 Color: 1
Size: 300902 Color: 1
Size: 267801 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 431417 Color: 1
Size: 304030 Color: 1
Size: 264554 Color: 0

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 431505 Color: 1
Size: 288645 Color: 1
Size: 279851 Color: 0

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 431511 Color: 1
Size: 290150 Color: 1
Size: 278340 Color: 0

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 431589 Color: 1
Size: 297869 Color: 1
Size: 270543 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 431609 Color: 1
Size: 299831 Color: 1
Size: 268561 Color: 0

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 431705 Color: 1
Size: 310094 Color: 1
Size: 258202 Color: 0

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 431799 Color: 1
Size: 311164 Color: 1
Size: 257038 Color: 0

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 432015 Color: 1
Size: 309593 Color: 1
Size: 258393 Color: 0

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 432055 Color: 1
Size: 309079 Color: 1
Size: 258867 Color: 0

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 432067 Color: 1
Size: 316931 Color: 1
Size: 251003 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 432081 Color: 1
Size: 301882 Color: 1
Size: 266038 Color: 0

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 432097 Color: 1
Size: 306761 Color: 1
Size: 261143 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 432138 Color: 1
Size: 307493 Color: 1
Size: 260370 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 432188 Color: 1
Size: 306940 Color: 1
Size: 260873 Color: 0

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 432197 Color: 1
Size: 291214 Color: 1
Size: 276590 Color: 0

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 432291 Color: 1
Size: 308156 Color: 1
Size: 259554 Color: 0

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 432304 Color: 1
Size: 296921 Color: 1
Size: 270776 Color: 0

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 432566 Color: 1
Size: 293012 Color: 1
Size: 274423 Color: 0

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 432568 Color: 1
Size: 300977 Color: 1
Size: 266456 Color: 0

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 432601 Color: 1
Size: 305803 Color: 1
Size: 261597 Color: 0

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 432684 Color: 1
Size: 291694 Color: 1
Size: 275623 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 432833 Color: 1
Size: 308594 Color: 1
Size: 258574 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 432933 Color: 1
Size: 302288 Color: 1
Size: 264780 Color: 0

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 432970 Color: 1
Size: 308060 Color: 1
Size: 258971 Color: 0

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 433036 Color: 1
Size: 302815 Color: 1
Size: 264150 Color: 0

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 433083 Color: 1
Size: 305468 Color: 1
Size: 261450 Color: 0

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 433129 Color: 1
Size: 291415 Color: 1
Size: 275457 Color: 0

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 433146 Color: 1
Size: 298552 Color: 1
Size: 268303 Color: 0

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 433216 Color: 1
Size: 303705 Color: 1
Size: 263080 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 433326 Color: 1
Size: 314697 Color: 1
Size: 251978 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 433352 Color: 1
Size: 310902 Color: 1
Size: 255747 Color: 0

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 433451 Color: 1
Size: 310747 Color: 1
Size: 255803 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 433475 Color: 1
Size: 310415 Color: 1
Size: 256111 Color: 0

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 433494 Color: 1
Size: 290044 Color: 1
Size: 276463 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 433513 Color: 1
Size: 311195 Color: 1
Size: 255293 Color: 0

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 433521 Color: 1
Size: 316358 Color: 1
Size: 250122 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 433683 Color: 1
Size: 287712 Color: 1
Size: 278606 Color: 0

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 433746 Color: 1
Size: 293929 Color: 0
Size: 272326 Color: 1

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 433781 Color: 1
Size: 300774 Color: 1
Size: 265446 Color: 0

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 433826 Color: 1
Size: 294678 Color: 1
Size: 271497 Color: 0

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 433974 Color: 1
Size: 290708 Color: 1
Size: 275319 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 433987 Color: 1
Size: 306320 Color: 1
Size: 259694 Color: 0

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 434031 Color: 1
Size: 305155 Color: 1
Size: 260815 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 434234 Color: 1
Size: 302480 Color: 1
Size: 263287 Color: 0

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 434242 Color: 1
Size: 306984 Color: 1
Size: 258775 Color: 0

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 434252 Color: 1
Size: 302491 Color: 1
Size: 263258 Color: 0

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 434284 Color: 1
Size: 312576 Color: 1
Size: 253141 Color: 0

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 434284 Color: 1
Size: 311044 Color: 1
Size: 254673 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 434341 Color: 1
Size: 298079 Color: 1
Size: 267581 Color: 0

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 434362 Color: 1
Size: 290028 Color: 0
Size: 275611 Color: 1

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 434412 Color: 1
Size: 301438 Color: 1
Size: 264151 Color: 0

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 434561 Color: 1
Size: 310746 Color: 1
Size: 254694 Color: 0

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 434577 Color: 1
Size: 297101 Color: 1
Size: 268323 Color: 0

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 434614 Color: 1
Size: 285010 Color: 1
Size: 280377 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 434687 Color: 1
Size: 301072 Color: 1
Size: 264242 Color: 0

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 434835 Color: 1
Size: 294705 Color: 1
Size: 270461 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 434919 Color: 1
Size: 302517 Color: 1
Size: 262565 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 435160 Color: 1
Size: 292270 Color: 1
Size: 272571 Color: 0

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 435165 Color: 1
Size: 300354 Color: 1
Size: 264482 Color: 0

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 435177 Color: 1
Size: 298366 Color: 1
Size: 266458 Color: 0

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 435411 Color: 1
Size: 307381 Color: 1
Size: 257209 Color: 0

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 435454 Color: 1
Size: 306564 Color: 1
Size: 257983 Color: 0

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 435512 Color: 1
Size: 308001 Color: 1
Size: 256488 Color: 0

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 435552 Color: 1
Size: 289241 Color: 1
Size: 275208 Color: 0

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 435579 Color: 1
Size: 302039 Color: 1
Size: 262383 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 435600 Color: 1
Size: 297135 Color: 1
Size: 267266 Color: 0

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 435681 Color: 1
Size: 311197 Color: 1
Size: 253123 Color: 0

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 435741 Color: 1
Size: 298314 Color: 1
Size: 265946 Color: 0

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 435873 Color: 1
Size: 307733 Color: 1
Size: 256395 Color: 0

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 435873 Color: 1
Size: 293223 Color: 1
Size: 270905 Color: 0

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 435985 Color: 1
Size: 282209 Color: 0
Size: 281807 Color: 1

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 436063 Color: 1
Size: 283024 Color: 0
Size: 280914 Color: 1

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 436170 Color: 1
Size: 295634 Color: 1
Size: 268197 Color: 0

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 436194 Color: 1
Size: 294645 Color: 1
Size: 269162 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 436208 Color: 1
Size: 302889 Color: 1
Size: 260904 Color: 0

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 436213 Color: 1
Size: 298887 Color: 1
Size: 264901 Color: 0

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 436217 Color: 1
Size: 300482 Color: 1
Size: 263302 Color: 0

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 436287 Color: 1
Size: 288931 Color: 1
Size: 274783 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 436289 Color: 1
Size: 303979 Color: 1
Size: 259733 Color: 0

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 436349 Color: 1
Size: 283448 Color: 0
Size: 280204 Color: 1

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 436381 Color: 1
Size: 309894 Color: 1
Size: 253726 Color: 0

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 436461 Color: 1
Size: 308949 Color: 1
Size: 254591 Color: 0

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 436574 Color: 1
Size: 309462 Color: 1
Size: 253965 Color: 0

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 436697 Color: 1
Size: 297242 Color: 1
Size: 266062 Color: 0

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 436717 Color: 1
Size: 297962 Color: 1
Size: 265322 Color: 0

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 436729 Color: 1
Size: 288569 Color: 1
Size: 274703 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 436800 Color: 1
Size: 291198 Color: 1
Size: 272003 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 436816 Color: 1
Size: 294555 Color: 1
Size: 268630 Color: 0

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 436846 Color: 1
Size: 300220 Color: 1
Size: 262935 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 436971 Color: 1
Size: 297869 Color: 1
Size: 265161 Color: 0

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 437107 Color: 1
Size: 288412 Color: 1
Size: 274482 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 437137 Color: 1
Size: 302891 Color: 1
Size: 259973 Color: 0

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 437203 Color: 1
Size: 302868 Color: 1
Size: 259930 Color: 0

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 437299 Color: 1
Size: 299063 Color: 1
Size: 263639 Color: 0

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 437362 Color: 1
Size: 307781 Color: 1
Size: 254858 Color: 0

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 437469 Color: 1
Size: 303797 Color: 1
Size: 258735 Color: 0

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 437577 Color: 1
Size: 311246 Color: 1
Size: 251178 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 437594 Color: 1
Size: 281949 Color: 0
Size: 280458 Color: 1

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 437644 Color: 1
Size: 310127 Color: 1
Size: 252230 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 437753 Color: 1
Size: 282595 Color: 1
Size: 279653 Color: 0

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 437811 Color: 1
Size: 307943 Color: 1
Size: 254247 Color: 0

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 437861 Color: 1
Size: 295178 Color: 1
Size: 266962 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 437968 Color: 1
Size: 308735 Color: 1
Size: 253298 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 437978 Color: 1
Size: 308870 Color: 1
Size: 253153 Color: 0

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 438200 Color: 1
Size: 302625 Color: 1
Size: 259176 Color: 0

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 438201 Color: 1
Size: 303871 Color: 1
Size: 257929 Color: 0

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 438216 Color: 1
Size: 281980 Color: 0
Size: 279805 Color: 1

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 438216 Color: 1
Size: 307811 Color: 1
Size: 253974 Color: 0

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 438228 Color: 1
Size: 290831 Color: 1
Size: 270942 Color: 0

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 438278 Color: 1
Size: 282342 Color: 1
Size: 279381 Color: 0

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 438287 Color: 1
Size: 303098 Color: 1
Size: 258616 Color: 0

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 438397 Color: 1
Size: 294303 Color: 1
Size: 267301 Color: 0

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 438598 Color: 1
Size: 310736 Color: 1
Size: 250667 Color: 0

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 438604 Color: 1
Size: 305289 Color: 1
Size: 256108 Color: 0

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 438646 Color: 1
Size: 293349 Color: 1
Size: 268006 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 438780 Color: 1
Size: 282415 Color: 1
Size: 278806 Color: 0

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 438786 Color: 1
Size: 301671 Color: 1
Size: 259544 Color: 0

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 438808 Color: 1
Size: 296741 Color: 1
Size: 264452 Color: 0

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 438813 Color: 1
Size: 297010 Color: 1
Size: 264178 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 438826 Color: 1
Size: 286748 Color: 1
Size: 274427 Color: 0

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 438861 Color: 1
Size: 290318 Color: 1
Size: 270822 Color: 0

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 438977 Color: 1
Size: 288061 Color: 1
Size: 272963 Color: 0

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 439055 Color: 1
Size: 288860 Color: 1
Size: 272086 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 439093 Color: 1
Size: 298449 Color: 1
Size: 262459 Color: 0

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 439121 Color: 1
Size: 286986 Color: 0
Size: 273894 Color: 1

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 439139 Color: 1
Size: 296295 Color: 1
Size: 264567 Color: 0

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 439164 Color: 1
Size: 303771 Color: 1
Size: 257066 Color: 0

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 439176 Color: 1
Size: 306327 Color: 1
Size: 254498 Color: 0

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 439309 Color: 1
Size: 301447 Color: 1
Size: 259245 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 439309 Color: 1
Size: 295751 Color: 1
Size: 264941 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 439341 Color: 1
Size: 304861 Color: 1
Size: 255799 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 439452 Color: 1
Size: 290266 Color: 1
Size: 270283 Color: 0

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 439498 Color: 1
Size: 290330 Color: 1
Size: 270173 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 439528 Color: 1
Size: 297911 Color: 1
Size: 262562 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 439538 Color: 1
Size: 291531 Color: 1
Size: 268932 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 439554 Color: 1
Size: 296907 Color: 1
Size: 263540 Color: 0

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 439554 Color: 1
Size: 282569 Color: 0
Size: 277878 Color: 1

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 439581 Color: 1
Size: 301047 Color: 1
Size: 259373 Color: 0

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 439612 Color: 1
Size: 283253 Color: 0
Size: 277136 Color: 1

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 439772 Color: 1
Size: 306067 Color: 1
Size: 254162 Color: 0

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 439787 Color: 1
Size: 297773 Color: 1
Size: 262441 Color: 0

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 439862 Color: 1
Size: 308197 Color: 1
Size: 251942 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 439896 Color: 1
Size: 297890 Color: 1
Size: 262215 Color: 0

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 439897 Color: 1
Size: 286131 Color: 1
Size: 273973 Color: 0

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 439955 Color: 1
Size: 307912 Color: 1
Size: 252134 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 440029 Color: 1
Size: 299862 Color: 1
Size: 260110 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 440103 Color: 1
Size: 302395 Color: 1
Size: 257503 Color: 0

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 440116 Color: 1
Size: 301723 Color: 1
Size: 258162 Color: 0

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 440119 Color: 1
Size: 305574 Color: 1
Size: 254308 Color: 0

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 440130 Color: 1
Size: 298163 Color: 1
Size: 261708 Color: 0

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 440352 Color: 1
Size: 281936 Color: 1
Size: 277713 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 440468 Color: 1
Size: 282287 Color: 1
Size: 277246 Color: 0

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 440561 Color: 1
Size: 282939 Color: 1
Size: 276501 Color: 0

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 440599 Color: 1
Size: 307231 Color: 1
Size: 252171 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 440604 Color: 1
Size: 289725 Color: 0
Size: 269672 Color: 1

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 440844 Color: 1
Size: 308388 Color: 1
Size: 250769 Color: 0

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 440883 Color: 1
Size: 301836 Color: 1
Size: 257282 Color: 0

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 440885 Color: 1
Size: 293587 Color: 1
Size: 265529 Color: 0

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 440918 Color: 1
Size: 306543 Color: 1
Size: 252540 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 440928 Color: 1
Size: 304708 Color: 1
Size: 254365 Color: 0

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 440987 Color: 1
Size: 284095 Color: 1
Size: 274919 Color: 0

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 441049 Color: 1
Size: 288762 Color: 1
Size: 270190 Color: 0

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 441088 Color: 1
Size: 299190 Color: 1
Size: 259723 Color: 0

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 441131 Color: 1
Size: 285448 Color: 1
Size: 273422 Color: 0

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 441188 Color: 1
Size: 299418 Color: 1
Size: 259395 Color: 0

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 441310 Color: 1
Size: 300690 Color: 1
Size: 258001 Color: 0

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 441348 Color: 1
Size: 298624 Color: 1
Size: 260029 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 441378 Color: 1
Size: 301641 Color: 1
Size: 256982 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 441397 Color: 1
Size: 306666 Color: 1
Size: 251938 Color: 0

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 429293 Color: 1
Size: 317818 Color: 1
Size: 252890 Color: 0

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 441565 Color: 1
Size: 295098 Color: 1
Size: 263338 Color: 0

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 441660 Color: 1
Size: 303324 Color: 1
Size: 255017 Color: 0

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 441724 Color: 1
Size: 300383 Color: 1
Size: 257894 Color: 0

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 441744 Color: 1
Size: 294656 Color: 1
Size: 263601 Color: 0

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 441751 Color: 1
Size: 285707 Color: 1
Size: 272543 Color: 0

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 441825 Color: 1
Size: 286922 Color: 1
Size: 271254 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 441841 Color: 1
Size: 284518 Color: 1
Size: 273642 Color: 0

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 442037 Color: 1
Size: 283857 Color: 1
Size: 274107 Color: 0

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 442042 Color: 1
Size: 281193 Color: 1
Size: 276766 Color: 0

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 442170 Color: 1
Size: 292193 Color: 1
Size: 265638 Color: 0

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 442279 Color: 1
Size: 288655 Color: 0
Size: 269067 Color: 1

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 442296 Color: 1
Size: 295451 Color: 1
Size: 262254 Color: 0

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 442306 Color: 1
Size: 302059 Color: 1
Size: 255636 Color: 0

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 442325 Color: 1
Size: 289830 Color: 0
Size: 267846 Color: 1

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 442337 Color: 1
Size: 302172 Color: 1
Size: 255492 Color: 0

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 442494 Color: 1
Size: 298636 Color: 1
Size: 258871 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 442567 Color: 1
Size: 289750 Color: 1
Size: 267684 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 442590 Color: 1
Size: 297676 Color: 1
Size: 259735 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 442624 Color: 1
Size: 284294 Color: 1
Size: 273083 Color: 0

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 442645 Color: 1
Size: 285180 Color: 1
Size: 272176 Color: 0

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 442659 Color: 1
Size: 287628 Color: 1
Size: 269714 Color: 0

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 442659 Color: 1
Size: 295418 Color: 1
Size: 261924 Color: 0

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 442772 Color: 1
Size: 306644 Color: 1
Size: 250585 Color: 0

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 442774 Color: 1
Size: 281788 Color: 1
Size: 275439 Color: 0

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 442828 Color: 1
Size: 292917 Color: 1
Size: 264256 Color: 0

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 442869 Color: 1
Size: 297808 Color: 1
Size: 259324 Color: 0

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 442957 Color: 1
Size: 286755 Color: 1
Size: 270289 Color: 0

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 443081 Color: 1
Size: 305074 Color: 1
Size: 251846 Color: 0

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 443090 Color: 1
Size: 299040 Color: 1
Size: 257871 Color: 0

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 443107 Color: 1
Size: 288023 Color: 0
Size: 268871 Color: 1

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 443126 Color: 1
Size: 283972 Color: 1
Size: 272903 Color: 0

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 443150 Color: 1
Size: 294844 Color: 1
Size: 262007 Color: 0

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 443174 Color: 1
Size: 303358 Color: 1
Size: 253469 Color: 0

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 443283 Color: 1
Size: 288499 Color: 1
Size: 268219 Color: 0

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 443294 Color: 1
Size: 299174 Color: 1
Size: 257533 Color: 0

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 443371 Color: 1
Size: 293996 Color: 1
Size: 262634 Color: 0

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 443446 Color: 1
Size: 294210 Color: 1
Size: 262345 Color: 0

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 443935 Color: 1
Size: 305812 Color: 1
Size: 250254 Color: 0

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 444096 Color: 1
Size: 301792 Color: 1
Size: 254113 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 444197 Color: 1
Size: 283980 Color: 0
Size: 271824 Color: 1

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 444335 Color: 1
Size: 278235 Color: 0
Size: 277431 Color: 1

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 444410 Color: 1
Size: 290393 Color: 1
Size: 265198 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 444458 Color: 1
Size: 296798 Color: 1
Size: 258745 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 444627 Color: 1
Size: 284284 Color: 1
Size: 271090 Color: 0

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 444870 Color: 1
Size: 294379 Color: 1
Size: 260752 Color: 0

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 444896 Color: 1
Size: 292144 Color: 1
Size: 262961 Color: 0

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 444982 Color: 1
Size: 281678 Color: 1
Size: 273341 Color: 0

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 445181 Color: 1
Size: 291530 Color: 1
Size: 263290 Color: 0

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 445208 Color: 1
Size: 300051 Color: 1
Size: 254742 Color: 0

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 445321 Color: 1
Size: 278878 Color: 0
Size: 275802 Color: 1

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 445341 Color: 1
Size: 294952 Color: 1
Size: 259708 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 445370 Color: 1
Size: 280227 Color: 1
Size: 274404 Color: 0

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 445447 Color: 1
Size: 291387 Color: 1
Size: 263167 Color: 0

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 445551 Color: 1
Size: 293665 Color: 1
Size: 260785 Color: 0

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 445653 Color: 1
Size: 278907 Color: 1
Size: 275441 Color: 0

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 445671 Color: 1
Size: 299248 Color: 1
Size: 255082 Color: 0

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 445715 Color: 1
Size: 290586 Color: 1
Size: 263700 Color: 0

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 445777 Color: 1
Size: 290563 Color: 1
Size: 263661 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 445825 Color: 1
Size: 302405 Color: 1
Size: 251771 Color: 0

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 445895 Color: 1
Size: 287446 Color: 1
Size: 266660 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 445946 Color: 1
Size: 280054 Color: 0
Size: 274001 Color: 1

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 445956 Color: 1
Size: 286040 Color: 1
Size: 268005 Color: 0

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 446000 Color: 1
Size: 280545 Color: 1
Size: 273456 Color: 0

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 446040 Color: 1
Size: 298234 Color: 1
Size: 255727 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 446355 Color: 1
Size: 289932 Color: 1
Size: 263714 Color: 0

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 446357 Color: 1
Size: 294995 Color: 1
Size: 258649 Color: 0

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 446363 Color: 1
Size: 298422 Color: 1
Size: 255216 Color: 0

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 482139 Color: 1
Size: 263494 Color: 1
Size: 254368 Color: 0

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 446497 Color: 1
Size: 290579 Color: 1
Size: 262925 Color: 0

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 446502 Color: 1
Size: 290310 Color: 1
Size: 263189 Color: 0

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 446518 Color: 1
Size: 285278 Color: 1
Size: 268205 Color: 0

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 446627 Color: 1
Size: 298520 Color: 1
Size: 254854 Color: 0

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 446629 Color: 1
Size: 297545 Color: 1
Size: 255827 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 446747 Color: 1
Size: 287778 Color: 1
Size: 265476 Color: 0

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 446828 Color: 1
Size: 297598 Color: 1
Size: 255575 Color: 0

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 446901 Color: 1
Size: 289683 Color: 1
Size: 263417 Color: 0

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 446925 Color: 1
Size: 296656 Color: 1
Size: 256420 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 446984 Color: 1
Size: 297700 Color: 1
Size: 255317 Color: 0

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 447007 Color: 1
Size: 282957 Color: 0
Size: 270037 Color: 1

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 447025 Color: 1
Size: 298029 Color: 1
Size: 254947 Color: 0

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 447038 Color: 1
Size: 292406 Color: 1
Size: 260557 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 447046 Color: 1
Size: 299761 Color: 1
Size: 253194 Color: 0

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 447065 Color: 1
Size: 291721 Color: 1
Size: 261215 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 447500 Color: 1
Size: 294800 Color: 1
Size: 257701 Color: 0

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 447558 Color: 1
Size: 294926 Color: 1
Size: 257517 Color: 0

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 447591 Color: 1
Size: 287115 Color: 1
Size: 265295 Color: 0

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 447738 Color: 1
Size: 290334 Color: 1
Size: 261929 Color: 0

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 447871 Color: 1
Size: 292802 Color: 1
Size: 259328 Color: 0

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 447881 Color: 1
Size: 279949 Color: 1
Size: 272171 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 447973 Color: 1
Size: 282672 Color: 1
Size: 269356 Color: 0

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 448025 Color: 1
Size: 297742 Color: 1
Size: 254234 Color: 0

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 448184 Color: 1
Size: 296366 Color: 1
Size: 255451 Color: 0

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 448239 Color: 1
Size: 287879 Color: 1
Size: 263883 Color: 0

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 448298 Color: 1
Size: 282876 Color: 0
Size: 268827 Color: 1

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 448302 Color: 1
Size: 299548 Color: 1
Size: 252151 Color: 0

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 448401 Color: 1
Size: 291039 Color: 1
Size: 260561 Color: 0

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 448443 Color: 1
Size: 300986 Color: 1
Size: 250572 Color: 0

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 448494 Color: 1
Size: 298623 Color: 1
Size: 252884 Color: 0

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 448513 Color: 1
Size: 298229 Color: 1
Size: 253259 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 448566 Color: 1
Size: 298609 Color: 1
Size: 252826 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 448623 Color: 1
Size: 299149 Color: 1
Size: 252229 Color: 0

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 448628 Color: 1
Size: 294131 Color: 1
Size: 257242 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 448646 Color: 1
Size: 286410 Color: 1
Size: 264945 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 448717 Color: 1
Size: 293503 Color: 1
Size: 257781 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 448740 Color: 1
Size: 287740 Color: 1
Size: 263521 Color: 0

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 449010 Color: 1
Size: 284851 Color: 1
Size: 266140 Color: 0

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 449035 Color: 1
Size: 293708 Color: 1
Size: 257258 Color: 0

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 449219 Color: 1
Size: 290606 Color: 1
Size: 260176 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 449254 Color: 1
Size: 283479 Color: 1
Size: 267268 Color: 0

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 449409 Color: 1
Size: 297513 Color: 1
Size: 253079 Color: 0

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 449505 Color: 1
Size: 296193 Color: 1
Size: 254303 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 449517 Color: 1
Size: 293444 Color: 1
Size: 257040 Color: 0

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 449518 Color: 1
Size: 296598 Color: 1
Size: 253885 Color: 0

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 449639 Color: 1
Size: 276548 Color: 1
Size: 273814 Color: 0

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 449751 Color: 1
Size: 280272 Color: 1
Size: 269978 Color: 0

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 450041 Color: 1
Size: 297677 Color: 1
Size: 252283 Color: 0

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 450223 Color: 1
Size: 299367 Color: 1
Size: 250411 Color: 0

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 450257 Color: 1
Size: 292110 Color: 1
Size: 257634 Color: 0

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 450325 Color: 1
Size: 288044 Color: 1
Size: 261632 Color: 0

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 450427 Color: 1
Size: 298751 Color: 1
Size: 250823 Color: 0

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 449901 Color: 1
Size: 287038 Color: 1
Size: 263062 Color: 0

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 450508 Color: 1
Size: 291590 Color: 1
Size: 257903 Color: 0

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 392385 Color: 1
Size: 354413 Color: 1
Size: 253203 Color: 0

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 450724 Color: 1
Size: 292690 Color: 1
Size: 256587 Color: 0

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 450812 Color: 1
Size: 286748 Color: 1
Size: 262441 Color: 0

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 450835 Color: 1
Size: 278566 Color: 1
Size: 270600 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 450838 Color: 1
Size: 279374 Color: 1
Size: 269789 Color: 0

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 450895 Color: 1
Size: 281100 Color: 1
Size: 268006 Color: 0

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 450905 Color: 1
Size: 294549 Color: 1
Size: 254547 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 450926 Color: 1
Size: 290507 Color: 1
Size: 258568 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 450967 Color: 1
Size: 287504 Color: 1
Size: 261530 Color: 0

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 451003 Color: 1
Size: 277515 Color: 1
Size: 271483 Color: 0

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 451017 Color: 1
Size: 283140 Color: 1
Size: 265844 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 451157 Color: 1
Size: 284575 Color: 1
Size: 264269 Color: 0

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 451188 Color: 1
Size: 274602 Color: 1
Size: 274211 Color: 0

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 451201 Color: 1
Size: 287358 Color: 1
Size: 261442 Color: 0

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 451227 Color: 1
Size: 297345 Color: 1
Size: 251429 Color: 0

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 451240 Color: 1
Size: 287702 Color: 1
Size: 261059 Color: 0

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 451408 Color: 1
Size: 279596 Color: 0
Size: 268997 Color: 1

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 451455 Color: 1
Size: 292721 Color: 1
Size: 255825 Color: 0

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 451459 Color: 1
Size: 287178 Color: 1
Size: 261364 Color: 0

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 451558 Color: 1
Size: 295140 Color: 1
Size: 253303 Color: 0

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 451569 Color: 1
Size: 286466 Color: 1
Size: 261966 Color: 0

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 451651 Color: 1
Size: 295925 Color: 1
Size: 252425 Color: 0

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 451739 Color: 1
Size: 278742 Color: 1
Size: 269520 Color: 0

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 451776 Color: 1
Size: 279899 Color: 1
Size: 268326 Color: 0

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 451814 Color: 1
Size: 283090 Color: 1
Size: 265097 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 451986 Color: 1
Size: 286928 Color: 1
Size: 261087 Color: 0

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 452004 Color: 1
Size: 284619 Color: 1
Size: 263378 Color: 0

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 452093 Color: 1
Size: 292751 Color: 1
Size: 255157 Color: 0

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 452112 Color: 1
Size: 293803 Color: 1
Size: 254086 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 452211 Color: 1
Size: 284400 Color: 0
Size: 263390 Color: 1

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 452252 Color: 1
Size: 294434 Color: 1
Size: 253315 Color: 0

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 452280 Color: 1
Size: 274622 Color: 1
Size: 273099 Color: 0

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 452368 Color: 1
Size: 293815 Color: 1
Size: 253818 Color: 0

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 452392 Color: 1
Size: 288285 Color: 1
Size: 259324 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 452432 Color: 1
Size: 277044 Color: 1
Size: 270525 Color: 0

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 452543 Color: 1
Size: 282676 Color: 1
Size: 264782 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 452597 Color: 1
Size: 282861 Color: 1
Size: 264543 Color: 0

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 452679 Color: 1
Size: 284885 Color: 1
Size: 262437 Color: 0

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 452682 Color: 1
Size: 287924 Color: 1
Size: 259395 Color: 0

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 452811 Color: 1
Size: 284286 Color: 1
Size: 262904 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 453040 Color: 1
Size: 292522 Color: 1
Size: 254439 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 453048 Color: 1
Size: 277041 Color: 0
Size: 269912 Color: 1

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 453294 Color: 1
Size: 293075 Color: 1
Size: 253632 Color: 0

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 453324 Color: 1
Size: 293013 Color: 1
Size: 253664 Color: 0

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 453530 Color: 1
Size: 288508 Color: 1
Size: 257963 Color: 0

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 453614 Color: 1
Size: 286421 Color: 1
Size: 259966 Color: 0

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 453702 Color: 1
Size: 275785 Color: 1
Size: 270514 Color: 0

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 453725 Color: 1
Size: 282574 Color: 1
Size: 263702 Color: 0

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 453737 Color: 1
Size: 293865 Color: 1
Size: 252399 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 453741 Color: 1
Size: 294180 Color: 1
Size: 252080 Color: 0

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 453802 Color: 1
Size: 289782 Color: 1
Size: 256417 Color: 0

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 453804 Color: 1
Size: 292745 Color: 1
Size: 253452 Color: 0

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 453817 Color: 1
Size: 285972 Color: 1
Size: 260212 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 453865 Color: 1
Size: 273289 Color: 1
Size: 272847 Color: 0

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 453883 Color: 1
Size: 294747 Color: 1
Size: 251371 Color: 0

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 454003 Color: 1
Size: 291440 Color: 1
Size: 254558 Color: 0

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 454051 Color: 1
Size: 292208 Color: 1
Size: 253742 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 454065 Color: 1
Size: 281601 Color: 1
Size: 264335 Color: 0

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 454125 Color: 1
Size: 288675 Color: 1
Size: 257201 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 454269 Color: 1
Size: 291385 Color: 1
Size: 254347 Color: 0

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 454311 Color: 1
Size: 280453 Color: 1
Size: 265237 Color: 0

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 454345 Color: 1
Size: 278785 Color: 1
Size: 266871 Color: 0

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 454367 Color: 1
Size: 294461 Color: 1
Size: 251173 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 454412 Color: 1
Size: 284675 Color: 1
Size: 260914 Color: 0

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 454486 Color: 1
Size: 288380 Color: 1
Size: 257135 Color: 0

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 454705 Color: 1
Size: 286485 Color: 1
Size: 258811 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 454727 Color: 1
Size: 281595 Color: 1
Size: 263679 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 454876 Color: 1
Size: 280055 Color: 1
Size: 265070 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 455056 Color: 1
Size: 276056 Color: 1
Size: 268889 Color: 0

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 455161 Color: 1
Size: 285452 Color: 1
Size: 259388 Color: 0

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 455222 Color: 1
Size: 293480 Color: 1
Size: 251299 Color: 0

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 455292 Color: 1
Size: 289946 Color: 1
Size: 254763 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 455316 Color: 1
Size: 294219 Color: 1
Size: 250466 Color: 0

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 455412 Color: 1
Size: 284252 Color: 1
Size: 260337 Color: 0

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 455564 Color: 1
Size: 292365 Color: 1
Size: 252072 Color: 0

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 455679 Color: 1
Size: 291394 Color: 1
Size: 252928 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 455896 Color: 1
Size: 285339 Color: 1
Size: 258766 Color: 0

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 456132 Color: 1
Size: 277764 Color: 1
Size: 266105 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 456287 Color: 1
Size: 282146 Color: 1
Size: 261568 Color: 0

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 456309 Color: 1
Size: 280036 Color: 0
Size: 263656 Color: 1

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 456326 Color: 1
Size: 282674 Color: 1
Size: 261001 Color: 0

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 456403 Color: 1
Size: 279062 Color: 1
Size: 264536 Color: 0

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 456415 Color: 1
Size: 277960 Color: 1
Size: 265626 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 456430 Color: 1
Size: 293438 Color: 1
Size: 250133 Color: 0

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 456448 Color: 1
Size: 276631 Color: 1
Size: 266922 Color: 0

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 456472 Color: 1
Size: 288354 Color: 1
Size: 255175 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 456517 Color: 1
Size: 280957 Color: 1
Size: 262527 Color: 0

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 456583 Color: 1
Size: 292599 Color: 1
Size: 250819 Color: 0

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 456590 Color: 1
Size: 287492 Color: 1
Size: 255919 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 456625 Color: 1
Size: 289520 Color: 1
Size: 253856 Color: 0

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 456663 Color: 1
Size: 292071 Color: 1
Size: 251267 Color: 0

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 456721 Color: 1
Size: 290811 Color: 1
Size: 252469 Color: 0

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 456728 Color: 1
Size: 287851 Color: 1
Size: 255422 Color: 0

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 456734 Color: 1
Size: 282531 Color: 1
Size: 260736 Color: 0

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 456813 Color: 1
Size: 283257 Color: 1
Size: 259931 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 456936 Color: 1
Size: 281574 Color: 1
Size: 261491 Color: 0

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 456946 Color: 1
Size: 278521 Color: 1
Size: 264534 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 456966 Color: 1
Size: 283665 Color: 1
Size: 259370 Color: 0

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 457004 Color: 1
Size: 277472 Color: 1
Size: 265525 Color: 0

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 457053 Color: 1
Size: 288528 Color: 1
Size: 254420 Color: 0

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 457344 Color: 1
Size: 289350 Color: 1
Size: 253307 Color: 0

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 457350 Color: 1
Size: 280381 Color: 1
Size: 262270 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 457471 Color: 1
Size: 282031 Color: 1
Size: 260499 Color: 0

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 457480 Color: 1
Size: 276648 Color: 1
Size: 265873 Color: 0

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 457483 Color: 1
Size: 282212 Color: 1
Size: 260306 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 457533 Color: 1
Size: 288336 Color: 1
Size: 254132 Color: 0

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 457646 Color: 1
Size: 281948 Color: 1
Size: 260407 Color: 0

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 457725 Color: 1
Size: 273971 Color: 1
Size: 268305 Color: 0

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 457789 Color: 1
Size: 290447 Color: 1
Size: 251765 Color: 0

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 457830 Color: 1
Size: 289971 Color: 1
Size: 252200 Color: 0

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 457976 Color: 1
Size: 275789 Color: 0
Size: 266236 Color: 1

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 458000 Color: 1
Size: 274328 Color: 0
Size: 267673 Color: 1

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 458107 Color: 1
Size: 283678 Color: 1
Size: 258216 Color: 0

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 458119 Color: 1
Size: 271449 Color: 0
Size: 270433 Color: 1

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 458263 Color: 1
Size: 287759 Color: 1
Size: 253979 Color: 0

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 458366 Color: 1
Size: 289323 Color: 1
Size: 252312 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 458444 Color: 1
Size: 291476 Color: 1
Size: 250081 Color: 0

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 458454 Color: 1
Size: 277441 Color: 1
Size: 264106 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 458515 Color: 1
Size: 282760 Color: 1
Size: 258726 Color: 0

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 458651 Color: 1
Size: 281922 Color: 1
Size: 259428 Color: 0

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 458714 Color: 1
Size: 283224 Color: 1
Size: 258063 Color: 0

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 458754 Color: 1
Size: 282981 Color: 1
Size: 258266 Color: 0

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 458774 Color: 1
Size: 280119 Color: 1
Size: 261108 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 458886 Color: 1
Size: 287194 Color: 1
Size: 253921 Color: 0

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 458967 Color: 1
Size: 288924 Color: 1
Size: 252110 Color: 0

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 459011 Color: 1
Size: 278257 Color: 1
Size: 262733 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 459064 Color: 1
Size: 272341 Color: 0
Size: 268596 Color: 1

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 459120 Color: 1
Size: 288481 Color: 1
Size: 252400 Color: 0

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 459186 Color: 1
Size: 282654 Color: 1
Size: 258161 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 459193 Color: 1
Size: 274553 Color: 1
Size: 266255 Color: 0

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 459220 Color: 1
Size: 286688 Color: 1
Size: 254093 Color: 0

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 459254 Color: 1
Size: 277997 Color: 1
Size: 262750 Color: 0

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 459334 Color: 1
Size: 286168 Color: 1
Size: 254499 Color: 0

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 459353 Color: 1
Size: 284877 Color: 1
Size: 255771 Color: 0

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 459518 Color: 1
Size: 283863 Color: 1
Size: 256620 Color: 0

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 459551 Color: 1
Size: 285841 Color: 1
Size: 254609 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 459633 Color: 1
Size: 286834 Color: 1
Size: 253534 Color: 0

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 459653 Color: 1
Size: 278178 Color: 1
Size: 262170 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 459672 Color: 1
Size: 284019 Color: 1
Size: 256310 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 459680 Color: 1
Size: 273694 Color: 1
Size: 266627 Color: 0

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 459725 Color: 1
Size: 274956 Color: 0
Size: 265320 Color: 1

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 459805 Color: 1
Size: 279041 Color: 0
Size: 261155 Color: 1

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 459897 Color: 1
Size: 289860 Color: 1
Size: 250244 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 459974 Color: 1
Size: 276411 Color: 1
Size: 263616 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 460072 Color: 1
Size: 280119 Color: 1
Size: 259810 Color: 0

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 460181 Color: 1
Size: 286406 Color: 1
Size: 253414 Color: 0

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 460188 Color: 1
Size: 282474 Color: 1
Size: 257339 Color: 0

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 460262 Color: 1
Size: 274288 Color: 1
Size: 265451 Color: 0

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 460333 Color: 1
Size: 285630 Color: 1
Size: 254038 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 460341 Color: 1
Size: 272913 Color: 1
Size: 266747 Color: 0

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 460424 Color: 1
Size: 285648 Color: 1
Size: 253929 Color: 0

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 460479 Color: 1
Size: 273217 Color: 1
Size: 266305 Color: 0

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 460605 Color: 1
Size: 275986 Color: 1
Size: 263410 Color: 0

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 460621 Color: 1
Size: 277233 Color: 1
Size: 262147 Color: 0

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 460639 Color: 1
Size: 270334 Color: 0
Size: 269028 Color: 1

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 460734 Color: 1
Size: 272208 Color: 1
Size: 267059 Color: 0

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 460830 Color: 1
Size: 282077 Color: 1
Size: 257094 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 460936 Color: 1
Size: 281450 Color: 1
Size: 257615 Color: 0

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 461091 Color: 1
Size: 285790 Color: 1
Size: 253120 Color: 0

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 461094 Color: 1
Size: 270035 Color: 1
Size: 268872 Color: 0

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 461211 Color: 1
Size: 285359 Color: 1
Size: 253431 Color: 0

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 461214 Color: 1
Size: 284718 Color: 1
Size: 254069 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 461219 Color: 1
Size: 288553 Color: 1
Size: 250229 Color: 0

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 461265 Color: 1
Size: 287481 Color: 1
Size: 251255 Color: 0

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 461310 Color: 1
Size: 284244 Color: 1
Size: 254447 Color: 0

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 461344 Color: 1
Size: 280233 Color: 1
Size: 258424 Color: 0

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 461438 Color: 1
Size: 271779 Color: 1
Size: 266784 Color: 0

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 461565 Color: 1
Size: 269686 Color: 1
Size: 268750 Color: 0

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 461577 Color: 1
Size: 287725 Color: 1
Size: 250699 Color: 0

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 461658 Color: 1
Size: 279381 Color: 1
Size: 258962 Color: 0

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 461988 Color: 1
Size: 281022 Color: 1
Size: 256991 Color: 0

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 462007 Color: 1
Size: 284860 Color: 1
Size: 253134 Color: 0

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 462168 Color: 1
Size: 282945 Color: 1
Size: 254888 Color: 0

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 462200 Color: 1
Size: 282627 Color: 1
Size: 255174 Color: 0

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 462275 Color: 1
Size: 284790 Color: 1
Size: 252936 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 462290 Color: 1
Size: 270162 Color: 1
Size: 267549 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 462340 Color: 1
Size: 279135 Color: 1
Size: 258526 Color: 0

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 462406 Color: 1
Size: 279293 Color: 1
Size: 258302 Color: 0

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 462502 Color: 1
Size: 284850 Color: 1
Size: 252649 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 462705 Color: 1
Size: 284355 Color: 1
Size: 252941 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 462710 Color: 1
Size: 286131 Color: 1
Size: 251160 Color: 0

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 462999 Color: 1
Size: 283182 Color: 1
Size: 253820 Color: 0

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 463183 Color: 1
Size: 283843 Color: 1
Size: 252975 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 463287 Color: 1
Size: 286141 Color: 1
Size: 250573 Color: 0

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 463302 Color: 1
Size: 275346 Color: 1
Size: 261353 Color: 0

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 463314 Color: 1
Size: 284919 Color: 1
Size: 251768 Color: 0

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 463369 Color: 1
Size: 278450 Color: 1
Size: 258182 Color: 0

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 463410 Color: 1
Size: 273771 Color: 1
Size: 262820 Color: 0

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 463477 Color: 1
Size: 285821 Color: 1
Size: 250703 Color: 0

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 463488 Color: 1
Size: 278472 Color: 1
Size: 258041 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 463490 Color: 1
Size: 274184 Color: 0
Size: 262327 Color: 1

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 463546 Color: 1
Size: 280338 Color: 1
Size: 256117 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 463579 Color: 1
Size: 275103 Color: 1
Size: 261319 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 463597 Color: 1
Size: 273887 Color: 0
Size: 262517 Color: 1

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 463602 Color: 1
Size: 284005 Color: 1
Size: 252394 Color: 0

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 463676 Color: 1
Size: 272839 Color: 1
Size: 263486 Color: 0

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 406208 Color: 1
Size: 324521 Color: 1
Size: 269272 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 463867 Color: 1
Size: 280631 Color: 1
Size: 255503 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 463914 Color: 1
Size: 268573 Color: 0
Size: 267514 Color: 1

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 464021 Color: 1
Size: 278205 Color: 1
Size: 257775 Color: 0

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 464030 Color: 1
Size: 269511 Color: 1
Size: 266460 Color: 0

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 464171 Color: 1
Size: 274094 Color: 1
Size: 261736 Color: 0

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 464199 Color: 1
Size: 273957 Color: 1
Size: 261845 Color: 0

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 464298 Color: 1
Size: 274897 Color: 1
Size: 260806 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 464298 Color: 1
Size: 280448 Color: 1
Size: 255255 Color: 0

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 464313 Color: 1
Size: 270550 Color: 1
Size: 265138 Color: 0

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 464370 Color: 1
Size: 280532 Color: 1
Size: 255099 Color: 0

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 464427 Color: 1
Size: 281983 Color: 1
Size: 253591 Color: 0

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 464518 Color: 1
Size: 277840 Color: 1
Size: 257643 Color: 0

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 464532 Color: 1
Size: 282831 Color: 1
Size: 252638 Color: 0

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 464663 Color: 1
Size: 271150 Color: 1
Size: 264188 Color: 0

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 464759 Color: 1
Size: 271906 Color: 1
Size: 263336 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 464765 Color: 1
Size: 281924 Color: 1
Size: 253312 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 464837 Color: 1
Size: 282955 Color: 1
Size: 252209 Color: 0

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 464904 Color: 1
Size: 277902 Color: 1
Size: 257195 Color: 0

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 465003 Color: 1
Size: 281805 Color: 1
Size: 253193 Color: 0

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 465029 Color: 1
Size: 268883 Color: 0
Size: 266089 Color: 1

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 465049 Color: 1
Size: 278739 Color: 1
Size: 256213 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 465156 Color: 1
Size: 280884 Color: 1
Size: 253961 Color: 0

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 465213 Color: 1
Size: 276456 Color: 1
Size: 258332 Color: 0

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 465552 Color: 1
Size: 277774 Color: 1
Size: 256675 Color: 0

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 465568 Color: 1
Size: 280665 Color: 1
Size: 253768 Color: 0

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 465616 Color: 1
Size: 277162 Color: 1
Size: 257223 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 465776 Color: 1
Size: 272474 Color: 1
Size: 261751 Color: 0

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 465819 Color: 1
Size: 283549 Color: 1
Size: 250633 Color: 0

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 465834 Color: 1
Size: 271848 Color: 1
Size: 262319 Color: 0

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 465865 Color: 1
Size: 284046 Color: 1
Size: 250090 Color: 0

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 465870 Color: 1
Size: 284127 Color: 1
Size: 250004 Color: 0

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 466307 Color: 1
Size: 267149 Color: 1
Size: 266545 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 412497 Color: 1
Size: 327503 Color: 1
Size: 260001 Color: 0

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 466420 Color: 1
Size: 267812 Color: 1
Size: 265769 Color: 0

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 466424 Color: 1
Size: 273946 Color: 1
Size: 259631 Color: 0

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 466435 Color: 1
Size: 283123 Color: 1
Size: 250443 Color: 0

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 466442 Color: 1
Size: 267036 Color: 1
Size: 266523 Color: 0

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 466477 Color: 1
Size: 277932 Color: 1
Size: 255592 Color: 0

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 466638 Color: 1
Size: 279602 Color: 1
Size: 253761 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 466730 Color: 1
Size: 282998 Color: 1
Size: 250273 Color: 0

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 466764 Color: 1
Size: 273733 Color: 1
Size: 259504 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 466893 Color: 1
Size: 267100 Color: 1
Size: 266008 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 466974 Color: 1
Size: 280221 Color: 1
Size: 252806 Color: 0

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 467102 Color: 1
Size: 277032 Color: 1
Size: 255867 Color: 0

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 467119 Color: 1
Size: 280630 Color: 1
Size: 252252 Color: 0

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 467240 Color: 1
Size: 275142 Color: 0
Size: 257619 Color: 1

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 467293 Color: 1
Size: 281059 Color: 1
Size: 251649 Color: 0

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 467313 Color: 1
Size: 279868 Color: 1
Size: 252820 Color: 0

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 467479 Color: 1
Size: 278890 Color: 1
Size: 253632 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 467745 Color: 1
Size: 281574 Color: 1
Size: 250682 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 467912 Color: 1
Size: 279069 Color: 1
Size: 253020 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 467958 Color: 1
Size: 280427 Color: 1
Size: 251616 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 467991 Color: 1
Size: 280158 Color: 1
Size: 251852 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 468001 Color: 1
Size: 278274 Color: 1
Size: 253726 Color: 0

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 468014 Color: 1
Size: 275200 Color: 1
Size: 256787 Color: 0

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 468186 Color: 1
Size: 280031 Color: 1
Size: 251784 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 468242 Color: 1
Size: 280052 Color: 1
Size: 251707 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 468336 Color: 1
Size: 273128 Color: 1
Size: 258537 Color: 0

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 468393 Color: 1
Size: 272590 Color: 1
Size: 259018 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 468515 Color: 1
Size: 274488 Color: 1
Size: 256998 Color: 0

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 468518 Color: 1
Size: 278541 Color: 1
Size: 252942 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 468537 Color: 1
Size: 272302 Color: 1
Size: 259162 Color: 0

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 468538 Color: 1
Size: 277488 Color: 1
Size: 253975 Color: 0

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 468544 Color: 1
Size: 274569 Color: 1
Size: 256888 Color: 0

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 468599 Color: 1
Size: 275360 Color: 1
Size: 256042 Color: 0

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 468700 Color: 1
Size: 278941 Color: 1
Size: 252360 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 468850 Color: 1
Size: 278646 Color: 1
Size: 252505 Color: 0

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 468896 Color: 1
Size: 273317 Color: 1
Size: 257788 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 468929 Color: 1
Size: 270854 Color: 1
Size: 260218 Color: 0

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 469074 Color: 1
Size: 277426 Color: 1
Size: 253501 Color: 0

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 469098 Color: 1
Size: 276973 Color: 1
Size: 253930 Color: 0

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 469113 Color: 1
Size: 279978 Color: 1
Size: 250910 Color: 0

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 469268 Color: 1
Size: 275664 Color: 1
Size: 255069 Color: 0

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 469338 Color: 1
Size: 266488 Color: 0
Size: 264175 Color: 1

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 469386 Color: 1
Size: 273094 Color: 1
Size: 257521 Color: 0

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 469488 Color: 1
Size: 274939 Color: 1
Size: 255574 Color: 0

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 469643 Color: 1
Size: 265635 Color: 1
Size: 264723 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 469708 Color: 1
Size: 269269 Color: 1
Size: 261024 Color: 0

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 469803 Color: 1
Size: 275055 Color: 1
Size: 255143 Color: 0

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 469809 Color: 1
Size: 268765 Color: 1
Size: 261427 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 469825 Color: 1
Size: 266201 Color: 0
Size: 263975 Color: 1

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 469878 Color: 1
Size: 269432 Color: 1
Size: 260691 Color: 0

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 469941 Color: 1
Size: 268514 Color: 1
Size: 261546 Color: 0

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 470019 Color: 1
Size: 278671 Color: 1
Size: 251311 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 470062 Color: 1
Size: 277741 Color: 1
Size: 252198 Color: 0

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 470087 Color: 1
Size: 277688 Color: 1
Size: 252226 Color: 0

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 470109 Color: 1
Size: 273572 Color: 1
Size: 256320 Color: 0

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 470122 Color: 1
Size: 273134 Color: 1
Size: 256745 Color: 0

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 470201 Color: 1
Size: 268472 Color: 1
Size: 261328 Color: 0

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 470293 Color: 1
Size: 276070 Color: 1
Size: 253638 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 470327 Color: 1
Size: 269904 Color: 1
Size: 259770 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 470337 Color: 1
Size: 269026 Color: 1
Size: 260638 Color: 0

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 470395 Color: 1
Size: 271369 Color: 1
Size: 258237 Color: 0

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 470453 Color: 1
Size: 277936 Color: 1
Size: 251612 Color: 0

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 470649 Color: 1
Size: 271310 Color: 1
Size: 258042 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 470736 Color: 1
Size: 267113 Color: 0
Size: 262152 Color: 1

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 470846 Color: 1
Size: 265834 Color: 0
Size: 263321 Color: 1

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 470872 Color: 1
Size: 270236 Color: 1
Size: 258893 Color: 0

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 470946 Color: 1
Size: 277458 Color: 1
Size: 251597 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 471221 Color: 1
Size: 273257 Color: 1
Size: 255523 Color: 0

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 471625 Color: 1
Size: 271826 Color: 1
Size: 256550 Color: 0

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 471638 Color: 1
Size: 269113 Color: 1
Size: 259250 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 471748 Color: 1
Size: 277290 Color: 1
Size: 250963 Color: 0

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 471842 Color: 1
Size: 276240 Color: 1
Size: 251919 Color: 0

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 471888 Color: 1
Size: 277771 Color: 1
Size: 250342 Color: 0

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 471922 Color: 1
Size: 274548 Color: 1
Size: 253531 Color: 0

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 471924 Color: 1
Size: 272188 Color: 1
Size: 255889 Color: 0

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 471967 Color: 1
Size: 277752 Color: 1
Size: 250282 Color: 0

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 472070 Color: 1
Size: 272079 Color: 1
Size: 255852 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 472096 Color: 1
Size: 275583 Color: 1
Size: 252322 Color: 0

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 472229 Color: 1
Size: 273732 Color: 1
Size: 254040 Color: 0

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 472397 Color: 1
Size: 265281 Color: 0
Size: 262323 Color: 1

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 472526 Color: 1
Size: 269100 Color: 1
Size: 258375 Color: 0

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 472567 Color: 1
Size: 268507 Color: 0
Size: 258927 Color: 1

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 472589 Color: 1
Size: 274847 Color: 1
Size: 252565 Color: 0

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 472675 Color: 1
Size: 276925 Color: 1
Size: 250401 Color: 0

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 472836 Color: 1
Size: 274985 Color: 1
Size: 252180 Color: 0

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 472895 Color: 1
Size: 266975 Color: 1
Size: 260131 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 472978 Color: 1
Size: 267118 Color: 1
Size: 259905 Color: 0

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 473017 Color: 1
Size: 274739 Color: 1
Size: 252245 Color: 0

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 473185 Color: 1
Size: 275920 Color: 1
Size: 250896 Color: 0

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 473219 Color: 1
Size: 267202 Color: 1
Size: 259580 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 473349 Color: 1
Size: 273046 Color: 1
Size: 253606 Color: 0

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 473776 Color: 1
Size: 268626 Color: 1
Size: 257599 Color: 0

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 474039 Color: 1
Size: 270727 Color: 1
Size: 255235 Color: 0

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 474072 Color: 1
Size: 273449 Color: 1
Size: 252480 Color: 0

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 474086 Color: 1
Size: 272565 Color: 1
Size: 253350 Color: 0

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 474316 Color: 1
Size: 264623 Color: 1
Size: 261062 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 474358 Color: 1
Size: 264507 Color: 1
Size: 261136 Color: 0

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 474518 Color: 1
Size: 274816 Color: 1
Size: 250667 Color: 0

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 474576 Color: 1
Size: 265050 Color: 1
Size: 260375 Color: 0

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 474611 Color: 1
Size: 272532 Color: 1
Size: 252858 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 474626 Color: 1
Size: 266865 Color: 1
Size: 258510 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 474646 Color: 1
Size: 270414 Color: 1
Size: 254941 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 474699 Color: 1
Size: 274131 Color: 1
Size: 251171 Color: 0

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 474888 Color: 1
Size: 273522 Color: 1
Size: 251591 Color: 0

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 475181 Color: 1
Size: 267620 Color: 1
Size: 257200 Color: 0

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 475260 Color: 1
Size: 273497 Color: 1
Size: 251244 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 475280 Color: 1
Size: 272068 Color: 1
Size: 252653 Color: 0

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 475380 Color: 1
Size: 271023 Color: 1
Size: 253598 Color: 0

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 475393 Color: 1
Size: 272069 Color: 1
Size: 252539 Color: 0

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 475758 Color: 1
Size: 266220 Color: 0
Size: 258023 Color: 1

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 475792 Color: 1
Size: 269867 Color: 1
Size: 254342 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 475855 Color: 1
Size: 268348 Color: 1
Size: 255798 Color: 0

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 475979 Color: 1
Size: 262807 Color: 0
Size: 261215 Color: 1

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 475988 Color: 1
Size: 269567 Color: 1
Size: 254446 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 476287 Color: 1
Size: 269905 Color: 1
Size: 253809 Color: 0

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 476438 Color: 1
Size: 272374 Color: 1
Size: 251189 Color: 0

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 476793 Color: 1
Size: 267604 Color: 1
Size: 255604 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 476841 Color: 1
Size: 266883 Color: 1
Size: 256277 Color: 0

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 476910 Color: 1
Size: 271007 Color: 1
Size: 252084 Color: 0

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 476971 Color: 1
Size: 271573 Color: 1
Size: 251457 Color: 0

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 477005 Color: 1
Size: 272779 Color: 1
Size: 250217 Color: 0

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 477011 Color: 1
Size: 266030 Color: 1
Size: 256960 Color: 0

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 477079 Color: 1
Size: 268161 Color: 1
Size: 254761 Color: 0

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 477101 Color: 1
Size: 262455 Color: 1
Size: 260445 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 477175 Color: 1
Size: 263694 Color: 1
Size: 259132 Color: 0

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 477237 Color: 1
Size: 270242 Color: 1
Size: 252522 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 477383 Color: 1
Size: 267937 Color: 0
Size: 254681 Color: 1

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 477450 Color: 1
Size: 271690 Color: 1
Size: 250861 Color: 0

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 477459 Color: 1
Size: 269077 Color: 1
Size: 253465 Color: 0

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 477487 Color: 1
Size: 264014 Color: 1
Size: 258500 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 477551 Color: 1
Size: 267916 Color: 1
Size: 254534 Color: 0

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 477573 Color: 1
Size: 267659 Color: 1
Size: 254769 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 477604 Color: 1
Size: 266608 Color: 1
Size: 255789 Color: 0

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 477664 Color: 1
Size: 268423 Color: 1
Size: 253914 Color: 0

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 477770 Color: 1
Size: 268639 Color: 1
Size: 253592 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 477864 Color: 1
Size: 266399 Color: 1
Size: 255738 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 477930 Color: 1
Size: 261724 Color: 1
Size: 260347 Color: 0

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 477990 Color: 1
Size: 266113 Color: 1
Size: 255898 Color: 0

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 478102 Color: 1
Size: 271637 Color: 1
Size: 250262 Color: 0

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 478133 Color: 1
Size: 271503 Color: 1
Size: 250365 Color: 0

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 478217 Color: 1
Size: 261281 Color: 1
Size: 260503 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 478256 Color: 1
Size: 262939 Color: 1
Size: 258806 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 478280 Color: 1
Size: 267600 Color: 1
Size: 254121 Color: 0

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 478316 Color: 1
Size: 264674 Color: 1
Size: 257011 Color: 0

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 435988 Color: 1
Size: 284119 Color: 1
Size: 279894 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 478582 Color: 1
Size: 263002 Color: 1
Size: 258417 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 478871 Color: 1
Size: 270225 Color: 1
Size: 250905 Color: 0

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 479120 Color: 1
Size: 263062 Color: 1
Size: 257819 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 457779 Color: 1
Size: 277153 Color: 1
Size: 265069 Color: 0

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 479383 Color: 1
Size: 265973 Color: 1
Size: 254645 Color: 0

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 479402 Color: 1
Size: 266774 Color: 1
Size: 253825 Color: 0

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 479410 Color: 1
Size: 261100 Color: 0
Size: 259491 Color: 1

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 479445 Color: 1
Size: 266827 Color: 1
Size: 253729 Color: 0

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 479954 Color: 1
Size: 266538 Color: 1
Size: 253509 Color: 0

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 479976 Color: 1
Size: 262157 Color: 0
Size: 257868 Color: 1

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 480130 Color: 1
Size: 260820 Color: 1
Size: 259051 Color: 0

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 480165 Color: 1
Size: 264300 Color: 1
Size: 255536 Color: 0

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 480228 Color: 1
Size: 266621 Color: 1
Size: 253152 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 480335 Color: 1
Size: 261702 Color: 1
Size: 257964 Color: 0

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 480424 Color: 1
Size: 264831 Color: 1
Size: 254746 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 480535 Color: 1
Size: 261029 Color: 1
Size: 258437 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 480633 Color: 1
Size: 268083 Color: 1
Size: 251285 Color: 0

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 480690 Color: 1
Size: 262536 Color: 1
Size: 256775 Color: 0

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 480730 Color: 1
Size: 264429 Color: 1
Size: 254842 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 481000 Color: 1
Size: 261345 Color: 0
Size: 257656 Color: 1

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 481185 Color: 1
Size: 267981 Color: 1
Size: 250835 Color: 0

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 481306 Color: 1
Size: 265147 Color: 1
Size: 253548 Color: 0

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 481493 Color: 1
Size: 266412 Color: 1
Size: 252096 Color: 0

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 481525 Color: 1
Size: 266106 Color: 1
Size: 252370 Color: 0

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 481531 Color: 1
Size: 259977 Color: 1
Size: 258493 Color: 0

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 481546 Color: 1
Size: 264405 Color: 1
Size: 254050 Color: 0

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 481789 Color: 1
Size: 266697 Color: 1
Size: 251515 Color: 0

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 482266 Color: 1
Size: 260569 Color: 1
Size: 257166 Color: 0

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 482288 Color: 1
Size: 267560 Color: 1
Size: 250153 Color: 0

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 482382 Color: 1
Size: 262527 Color: 1
Size: 255092 Color: 0

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 484742 Color: 1
Size: 265171 Color: 1
Size: 250088 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 482499 Color: 1
Size: 263177 Color: 1
Size: 254325 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 482520 Color: 1
Size: 266120 Color: 1
Size: 251361 Color: 0

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 482598 Color: 1
Size: 264916 Color: 1
Size: 252487 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 482701 Color: 1
Size: 264908 Color: 1
Size: 252392 Color: 0

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 482826 Color: 1
Size: 264226 Color: 1
Size: 252949 Color: 0

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 482850 Color: 1
Size: 264493 Color: 1
Size: 252658 Color: 0

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 483051 Color: 1
Size: 265625 Color: 1
Size: 251325 Color: 0

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 483342 Color: 1
Size: 260943 Color: 1
Size: 255716 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 483415 Color: 1
Size: 264734 Color: 1
Size: 251852 Color: 0

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 483442 Color: 1
Size: 265456 Color: 1
Size: 251103 Color: 0

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 483641 Color: 1
Size: 263919 Color: 1
Size: 252441 Color: 0

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 483697 Color: 1
Size: 263541 Color: 1
Size: 252763 Color: 0

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 483810 Color: 1
Size: 264433 Color: 1
Size: 251758 Color: 0

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 484026 Color: 1
Size: 264699 Color: 1
Size: 251276 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 484093 Color: 1
Size: 263668 Color: 1
Size: 252240 Color: 0

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 484136 Color: 1
Size: 258269 Color: 1
Size: 257596 Color: 0

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 484281 Color: 1
Size: 260801 Color: 1
Size: 254919 Color: 0

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 484309 Color: 1
Size: 260424 Color: 1
Size: 255268 Color: 0

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 484514 Color: 1
Size: 258538 Color: 1
Size: 256949 Color: 0

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 484559 Color: 1
Size: 264140 Color: 1
Size: 251302 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 484577 Color: 1
Size: 262778 Color: 1
Size: 252646 Color: 0

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 484657 Color: 1
Size: 264113 Color: 1
Size: 251231 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 484673 Color: 1
Size: 262714 Color: 1
Size: 252614 Color: 0

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 484688 Color: 1
Size: 262007 Color: 1
Size: 253306 Color: 0

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 484694 Color: 1
Size: 262232 Color: 1
Size: 253075 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 484890 Color: 1
Size: 262454 Color: 1
Size: 252657 Color: 0

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 484959 Color: 1
Size: 262893 Color: 1
Size: 252149 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 485102 Color: 1
Size: 259069 Color: 1
Size: 255830 Color: 0

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 485293 Color: 1
Size: 261667 Color: 1
Size: 253041 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 485310 Color: 1
Size: 263022 Color: 1
Size: 251669 Color: 0

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 485349 Color: 1
Size: 260828 Color: 1
Size: 253824 Color: 0

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 485481 Color: 1
Size: 257786 Color: 1
Size: 256734 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 485655 Color: 1
Size: 263634 Color: 1
Size: 250712 Color: 0

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 485745 Color: 1
Size: 260360 Color: 1
Size: 253896 Color: 0

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 485851 Color: 1
Size: 261600 Color: 1
Size: 252550 Color: 0

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 485885 Color: 1
Size: 259590 Color: 1
Size: 254526 Color: 0

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 485916 Color: 1
Size: 263663 Color: 1
Size: 250422 Color: 0

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 485953 Color: 1
Size: 263025 Color: 1
Size: 251023 Color: 0

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 486182 Color: 1
Size: 260659 Color: 1
Size: 253160 Color: 0

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 486322 Color: 1
Size: 257835 Color: 1
Size: 255844 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 486351 Color: 1
Size: 263483 Color: 1
Size: 250167 Color: 0

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 486493 Color: 1
Size: 260505 Color: 1
Size: 253003 Color: 0

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 486512 Color: 1
Size: 256899 Color: 1
Size: 256590 Color: 0

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 486701 Color: 1
Size: 259092 Color: 1
Size: 254208 Color: 0

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 486744 Color: 1
Size: 261892 Color: 1
Size: 251365 Color: 0

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 486763 Color: 1
Size: 261797 Color: 1
Size: 251441 Color: 0

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 486963 Color: 1
Size: 262480 Color: 1
Size: 250558 Color: 0

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 486975 Color: 1
Size: 257156 Color: 0
Size: 255870 Color: 1

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 487233 Color: 1
Size: 257426 Color: 1
Size: 255342 Color: 0

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 487318 Color: 1
Size: 261520 Color: 1
Size: 251163 Color: 0

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 487514 Color: 1
Size: 256952 Color: 1
Size: 255535 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 490536 Color: 1
Size: 256111 Color: 1
Size: 253354 Color: 0

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 487609 Color: 1
Size: 261804 Color: 1
Size: 250588 Color: 0

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 487656 Color: 1
Size: 261300 Color: 1
Size: 251045 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 487666 Color: 1
Size: 260574 Color: 1
Size: 251761 Color: 0

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 487802 Color: 1
Size: 257586 Color: 1
Size: 254613 Color: 0

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 487848 Color: 1
Size: 260433 Color: 1
Size: 251720 Color: 0

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 488018 Color: 1
Size: 256201 Color: 1
Size: 255782 Color: 0

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 488083 Color: 1
Size: 261016 Color: 1
Size: 250902 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 488173 Color: 1
Size: 256080 Color: 1
Size: 255748 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 488199 Color: 1
Size: 259767 Color: 1
Size: 252035 Color: 0

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 488205 Color: 1
Size: 256225 Color: 1
Size: 255571 Color: 0

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 488261 Color: 1
Size: 258895 Color: 1
Size: 252845 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 488291 Color: 1
Size: 257782 Color: 1
Size: 253928 Color: 0

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 488490 Color: 1
Size: 259910 Color: 1
Size: 251601 Color: 0

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 488641 Color: 1
Size: 256540 Color: 0
Size: 254820 Color: 1

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 488649 Color: 1
Size: 260662 Color: 1
Size: 250690 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 488795 Color: 1
Size: 256781 Color: 0
Size: 254425 Color: 1

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 489185 Color: 1
Size: 260758 Color: 1
Size: 250058 Color: 0

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 489191 Color: 1
Size: 259614 Color: 1
Size: 251196 Color: 0

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 489200 Color: 1
Size: 257573 Color: 1
Size: 253228 Color: 0

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 489261 Color: 1
Size: 259449 Color: 1
Size: 251291 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 489347 Color: 1
Size: 260060 Color: 1
Size: 250594 Color: 0

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 489525 Color: 1
Size: 257575 Color: 1
Size: 252901 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 489565 Color: 1
Size: 257118 Color: 1
Size: 253318 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 489569 Color: 1
Size: 255494 Color: 0
Size: 254938 Color: 1

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 489583 Color: 1
Size: 257769 Color: 1
Size: 252649 Color: 0

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 489590 Color: 1
Size: 258561 Color: 1
Size: 251850 Color: 0

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 489759 Color: 1
Size: 259085 Color: 1
Size: 251157 Color: 0

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 489797 Color: 1
Size: 257482 Color: 1
Size: 252722 Color: 0

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 489820 Color: 1
Size: 256204 Color: 1
Size: 253977 Color: 0

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 489906 Color: 1
Size: 259431 Color: 1
Size: 250664 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 490049 Color: 1
Size: 258439 Color: 1
Size: 251513 Color: 0

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 490078 Color: 1
Size: 255423 Color: 1
Size: 254500 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 490094 Color: 1
Size: 257315 Color: 1
Size: 252592 Color: 0

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 490206 Color: 1
Size: 259346 Color: 1
Size: 250449 Color: 0

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 490367 Color: 1
Size: 257184 Color: 1
Size: 252450 Color: 0

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 490381 Color: 1
Size: 258635 Color: 1
Size: 250985 Color: 0

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 490497 Color: 1
Size: 258906 Color: 1
Size: 250598 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 490554 Color: 1
Size: 258079 Color: 1
Size: 251368 Color: 0

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 490714 Color: 1
Size: 256192 Color: 1
Size: 253095 Color: 0

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 490776 Color: 1
Size: 257903 Color: 1
Size: 251322 Color: 0

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 490883 Color: 1
Size: 254988 Color: 1
Size: 254130 Color: 0

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 490891 Color: 1
Size: 258301 Color: 1
Size: 250809 Color: 0

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 490937 Color: 1
Size: 258981 Color: 1
Size: 250083 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 491084 Color: 1
Size: 258426 Color: 1
Size: 250491 Color: 0

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 491085 Color: 1
Size: 258223 Color: 1
Size: 250693 Color: 0

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 491145 Color: 1
Size: 255565 Color: 1
Size: 253291 Color: 0

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 491176 Color: 1
Size: 257168 Color: 1
Size: 251657 Color: 0

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 491218 Color: 1
Size: 256347 Color: 1
Size: 252436 Color: 0

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 491611 Color: 1
Size: 258132 Color: 1
Size: 250258 Color: 0

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 491649 Color: 1
Size: 255577 Color: 1
Size: 252775 Color: 0

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 491770 Color: 1
Size: 257304 Color: 1
Size: 250927 Color: 0

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 491852 Color: 1
Size: 254283 Color: 0
Size: 253866 Color: 1

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 491888 Color: 1
Size: 256165 Color: 1
Size: 251948 Color: 0

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 492019 Color: 1
Size: 256174 Color: 1
Size: 251808 Color: 0

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 492098 Color: 1
Size: 256342 Color: 1
Size: 251561 Color: 0

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 492445 Color: 1
Size: 256341 Color: 1
Size: 251215 Color: 0

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 492503 Color: 1
Size: 254615 Color: 1
Size: 252883 Color: 0

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 492597 Color: 1
Size: 256452 Color: 1
Size: 250952 Color: 0

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 492642 Color: 1
Size: 257343 Color: 1
Size: 250016 Color: 0

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 492657 Color: 1
Size: 253826 Color: 1
Size: 253518 Color: 0

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 492664 Color: 1
Size: 256532 Color: 1
Size: 250805 Color: 0

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 492710 Color: 1
Size: 254015 Color: 1
Size: 253276 Color: 0

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 492930 Color: 1
Size: 255425 Color: 1
Size: 251646 Color: 0

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 492943 Color: 1
Size: 255182 Color: 1
Size: 251876 Color: 0

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 493070 Color: 1
Size: 256075 Color: 1
Size: 250856 Color: 0

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 493111 Color: 1
Size: 253543 Color: 0
Size: 253347 Color: 1

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 493264 Color: 1
Size: 256205 Color: 1
Size: 250532 Color: 0

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 493323 Color: 1
Size: 256078 Color: 1
Size: 250600 Color: 0

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 493359 Color: 1
Size: 256608 Color: 1
Size: 250034 Color: 0

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 493409 Color: 1
Size: 255181 Color: 1
Size: 251411 Color: 0

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 493447 Color: 1
Size: 256432 Color: 1
Size: 250122 Color: 0

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 493613 Color: 1
Size: 253743 Color: 1
Size: 252645 Color: 0

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 493651 Color: 1
Size: 255754 Color: 1
Size: 250596 Color: 0

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 493697 Color: 1
Size: 253770 Color: 1
Size: 252534 Color: 0

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 493876 Color: 1
Size: 254974 Color: 1
Size: 251151 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 493879 Color: 1
Size: 255641 Color: 1
Size: 250481 Color: 0

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 493905 Color: 1
Size: 253050 Color: 1
Size: 253046 Color: 0

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 494301 Color: 1
Size: 253727 Color: 1
Size: 251973 Color: 0

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 494396 Color: 1
Size: 254855 Color: 1
Size: 250750 Color: 0

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 494506 Color: 1
Size: 255204 Color: 1
Size: 250291 Color: 0

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 494522 Color: 1
Size: 252744 Color: 1
Size: 252735 Color: 0

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 494555 Color: 1
Size: 254910 Color: 1
Size: 250536 Color: 0

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 494618 Color: 1
Size: 254588 Color: 1
Size: 250795 Color: 0

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 494696 Color: 1
Size: 254463 Color: 1
Size: 250842 Color: 0

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 494917 Color: 1
Size: 254527 Color: 1
Size: 250557 Color: 0

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 495005 Color: 1
Size: 253777 Color: 1
Size: 251219 Color: 0

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 495070 Color: 1
Size: 254685 Color: 1
Size: 250246 Color: 0

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 495141 Color: 1
Size: 254614 Color: 1
Size: 250246 Color: 0

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 495290 Color: 1
Size: 254340 Color: 1
Size: 250371 Color: 0

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 495340 Color: 1
Size: 253053 Color: 1
Size: 251608 Color: 0

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 495355 Color: 1
Size: 253632 Color: 1
Size: 251014 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 495441 Color: 1
Size: 253551 Color: 1
Size: 251009 Color: 0

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 495597 Color: 1
Size: 253893 Color: 1
Size: 250511 Color: 0

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 495631 Color: 1
Size: 252935 Color: 1
Size: 251435 Color: 0

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 495648 Color: 1
Size: 253546 Color: 1
Size: 250807 Color: 0

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 495723 Color: 1
Size: 252954 Color: 1
Size: 251324 Color: 0

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 495834 Color: 1
Size: 253707 Color: 1
Size: 250460 Color: 0

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 495937 Color: 1
Size: 252520 Color: 1
Size: 251544 Color: 0

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 495946 Color: 1
Size: 253140 Color: 1
Size: 250915 Color: 0

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 496146 Color: 1
Size: 253762 Color: 1
Size: 250093 Color: 0

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 496264 Color: 1
Size: 253716 Color: 1
Size: 250021 Color: 0

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 496363 Color: 1
Size: 253148 Color: 1
Size: 250490 Color: 0

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 496443 Color: 1
Size: 253106 Color: 1
Size: 250452 Color: 0

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 496503 Color: 1
Size: 252483 Color: 0
Size: 251015 Color: 1

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 496621 Color: 1
Size: 253329 Color: 1
Size: 250051 Color: 0

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 496692 Color: 1
Size: 253133 Color: 1
Size: 250176 Color: 0

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 496807 Color: 1
Size: 252315 Color: 1
Size: 250879 Color: 0

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 496876 Color: 1
Size: 251620 Color: 1
Size: 251505 Color: 0

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 472087 Color: 1
Size: 264193 Color: 0
Size: 263721 Color: 1

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 497147 Color: 1
Size: 252054 Color: 1
Size: 250800 Color: 0

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 497174 Color: 1
Size: 251996 Color: 0
Size: 250831 Color: 1

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 497182 Color: 1
Size: 252635 Color: 1
Size: 250184 Color: 0

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 497355 Color: 1
Size: 252426 Color: 1
Size: 250220 Color: 0

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 497355 Color: 1
Size: 252171 Color: 1
Size: 250475 Color: 0

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 497367 Color: 1
Size: 252312 Color: 1
Size: 250322 Color: 0

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 497585 Color: 1
Size: 252335 Color: 1
Size: 250081 Color: 0

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 497878 Color: 1
Size: 251774 Color: 1
Size: 250349 Color: 0

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 497934 Color: 1
Size: 251937 Color: 1
Size: 250130 Color: 0

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 498051 Color: 1
Size: 251788 Color: 1
Size: 250162 Color: 0

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 498158 Color: 1
Size: 251494 Color: 1
Size: 250349 Color: 0

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 498311 Color: 1
Size: 251054 Color: 1
Size: 250636 Color: 0

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 498536 Color: 1
Size: 251281 Color: 1
Size: 250184 Color: 0

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 498541 Color: 1
Size: 251441 Color: 1
Size: 250019 Color: 0

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 498754 Color: 1
Size: 250939 Color: 1
Size: 250308 Color: 0

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 498817 Color: 1
Size: 251071 Color: 1
Size: 250113 Color: 0

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 498952 Color: 1
Size: 250860 Color: 1
Size: 250189 Color: 0

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 499030 Color: 1
Size: 250727 Color: 1
Size: 250244 Color: 0

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 499125 Color: 1
Size: 250533 Color: 1
Size: 250343 Color: 0

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 499289 Color: 1
Size: 250379 Color: 1
Size: 250333 Color: 0

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 499417 Color: 1
Size: 250454 Color: 1
Size: 250130 Color: 0

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 499482 Color: 1
Size: 250462 Color: 1
Size: 250057 Color: 0

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 499514 Color: 1
Size: 250459 Color: 1
Size: 250028 Color: 0

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 499655 Color: 1
Size: 250186 Color: 1
Size: 250160 Color: 0

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 499698 Color: 1
Size: 250251 Color: 1
Size: 250052 Color: 0

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 499700 Color: 1
Size: 250165 Color: 1
Size: 250136 Color: 0

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 499814 Color: 1
Size: 250113 Color: 1
Size: 250074 Color: 0

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 499867 Color: 1
Size: 250080 Color: 1
Size: 250054 Color: 0

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 499909 Color: 1
Size: 250079 Color: 1
Size: 250013 Color: 0

Bin 2982: 1 of cap free
Amount of items: 3
Items: 
Size: 349527 Color: 1
Size: 349136 Color: 1
Size: 301337 Color: 0

Bin 2983: 1 of cap free
Amount of items: 3
Items: 
Size: 363750 Color: 1
Size: 333434 Color: 1
Size: 302816 Color: 0

Bin 2984: 1 of cap free
Amount of items: 3
Items: 
Size: 428593 Color: 1
Size: 304528 Color: 1
Size: 266879 Color: 0

Bin 2985: 1 of cap free
Amount of items: 3
Items: 
Size: 407016 Color: 1
Size: 297858 Color: 1
Size: 295126 Color: 0

Bin 2986: 1 of cap free
Amount of items: 3
Items: 
Size: 389174 Color: 1
Size: 352852 Color: 1
Size: 257974 Color: 0

Bin 2987: 1 of cap free
Amount of items: 3
Items: 
Size: 363592 Color: 1
Size: 339638 Color: 1
Size: 296770 Color: 0

Bin 2988: 1 of cap free
Amount of items: 3
Items: 
Size: 388944 Color: 1
Size: 354504 Color: 1
Size: 256552 Color: 0

Bin 2989: 1 of cap free
Amount of items: 3
Items: 
Size: 366092 Color: 1
Size: 333985 Color: 1
Size: 299923 Color: 0

Bin 2990: 1 of cap free
Amount of items: 3
Items: 
Size: 497238 Color: 1
Size: 252360 Color: 1
Size: 250402 Color: 0

Bin 2991: 1 of cap free
Amount of items: 3
Items: 
Size: 375044 Color: 1
Size: 347229 Color: 1
Size: 277727 Color: 0

Bin 2992: 1 of cap free
Amount of items: 3
Items: 
Size: 363847 Color: 1
Size: 326028 Color: 1
Size: 310125 Color: 0

Bin 2993: 1 of cap free
Amount of items: 3
Items: 
Size: 349545 Color: 1
Size: 339717 Color: 1
Size: 310738 Color: 0

Bin 2994: 1 of cap free
Amount of items: 3
Items: 
Size: 362845 Color: 1
Size: 350075 Color: 1
Size: 287080 Color: 0

Bin 2995: 1 of cap free
Amount of items: 3
Items: 
Size: 351251 Color: 1
Size: 341301 Color: 1
Size: 307448 Color: 0

Bin 2996: 1 of cap free
Amount of items: 3
Items: 
Size: 470026 Color: 1
Size: 271231 Color: 0
Size: 258743 Color: 1

Bin 2997: 1 of cap free
Amount of items: 3
Items: 
Size: 357201 Color: 1
Size: 335333 Color: 1
Size: 307466 Color: 0

Bin 2998: 1 of cap free
Amount of items: 3
Items: 
Size: 360698 Color: 1
Size: 358095 Color: 1
Size: 281207 Color: 0

Bin 2999: 1 of cap free
Amount of items: 3
Items: 
Size: 364312 Color: 1
Size: 337025 Color: 1
Size: 298663 Color: 0

Bin 3000: 1 of cap free
Amount of items: 3
Items: 
Size: 357969 Color: 1
Size: 335555 Color: 1
Size: 306476 Color: 0

Bin 3001: 1 of cap free
Amount of items: 3
Items: 
Size: 403113 Color: 1
Size: 299314 Color: 0
Size: 297573 Color: 1

Bin 3002: 1 of cap free
Amount of items: 3
Items: 
Size: 362752 Color: 1
Size: 330018 Color: 1
Size: 307230 Color: 0

Bin 3003: 1 of cap free
Amount of items: 3
Items: 
Size: 397931 Color: 1
Size: 309279 Color: 0
Size: 292790 Color: 1

Bin 3004: 1 of cap free
Amount of items: 3
Items: 
Size: 411552 Color: 1
Size: 302952 Color: 0
Size: 285496 Color: 1

Bin 3005: 1 of cap free
Amount of items: 3
Items: 
Size: 344339 Color: 1
Size: 336367 Color: 1
Size: 319294 Color: 0

Bin 3006: 1 of cap free
Amount of items: 3
Items: 
Size: 348075 Color: 1
Size: 346352 Color: 1
Size: 305573 Color: 0

Bin 3007: 1 of cap free
Amount of items: 3
Items: 
Size: 361084 Color: 1
Size: 341880 Color: 1
Size: 297036 Color: 0

Bin 3008: 1 of cap free
Amount of items: 3
Items: 
Size: 361107 Color: 1
Size: 337458 Color: 1
Size: 301435 Color: 0

Bin 3009: 1 of cap free
Amount of items: 3
Items: 
Size: 402608 Color: 1
Size: 322504 Color: 0
Size: 274888 Color: 1

Bin 3010: 1 of cap free
Amount of items: 3
Items: 
Size: 352555 Color: 1
Size: 329958 Color: 1
Size: 317487 Color: 0

Bin 3011: 1 of cap free
Amount of items: 3
Items: 
Size: 364617 Color: 1
Size: 335103 Color: 1
Size: 300280 Color: 0

Bin 3012: 1 of cap free
Amount of items: 3
Items: 
Size: 365288 Color: 1
Size: 342810 Color: 1
Size: 291902 Color: 0

Bin 3013: 1 of cap free
Amount of items: 3
Items: 
Size: 365710 Color: 1
Size: 338964 Color: 1
Size: 295326 Color: 0

Bin 3014: 1 of cap free
Amount of items: 3
Items: 
Size: 365748 Color: 1
Size: 329784 Color: 1
Size: 304468 Color: 0

Bin 3015: 1 of cap free
Amount of items: 3
Items: 
Size: 365916 Color: 1
Size: 326943 Color: 1
Size: 307141 Color: 0

Bin 3016: 1 of cap free
Amount of items: 3
Items: 
Size: 358359 Color: 1
Size: 322978 Color: 0
Size: 318663 Color: 1

Bin 3017: 1 of cap free
Amount of items: 3
Items: 
Size: 369458 Color: 1
Size: 353958 Color: 1
Size: 276584 Color: 0

Bin 3018: 1 of cap free
Amount of items: 3
Items: 
Size: 352656 Color: 1
Size: 342593 Color: 1
Size: 304751 Color: 0

Bin 3019: 1 of cap free
Amount of items: 3
Items: 
Size: 396716 Color: 1
Size: 350959 Color: 1
Size: 252325 Color: 0

Bin 3020: 1 of cap free
Amount of items: 3
Items: 
Size: 358780 Color: 1
Size: 325451 Color: 1
Size: 315769 Color: 0

Bin 3021: 1 of cap free
Amount of items: 3
Items: 
Size: 361579 Color: 1
Size: 360068 Color: 1
Size: 278353 Color: 0

Bin 3022: 1 of cap free
Amount of items: 3
Items: 
Size: 376126 Color: 1
Size: 368090 Color: 1
Size: 255784 Color: 0

Bin 3023: 1 of cap free
Amount of items: 3
Items: 
Size: 357774 Color: 1
Size: 345738 Color: 1
Size: 296488 Color: 0

Bin 3024: 1 of cap free
Amount of items: 3
Items: 
Size: 390230 Color: 1
Size: 358471 Color: 1
Size: 251299 Color: 0

Bin 3025: 1 of cap free
Amount of items: 3
Items: 
Size: 431208 Color: 1
Size: 318403 Color: 1
Size: 250389 Color: 0

Bin 3026: 1 of cap free
Amount of items: 3
Items: 
Size: 347683 Color: 1
Size: 347437 Color: 1
Size: 304880 Color: 0

Bin 3027: 1 of cap free
Amount of items: 3
Items: 
Size: 349136 Color: 1
Size: 333934 Color: 1
Size: 316930 Color: 0

Bin 3028: 1 of cap free
Amount of items: 3
Items: 
Size: 424628 Color: 1
Size: 316733 Color: 1
Size: 258639 Color: 0

Bin 3029: 1 of cap free
Amount of items: 3
Items: 
Size: 350865 Color: 1
Size: 346508 Color: 1
Size: 302627 Color: 0

Bin 3030: 1 of cap free
Amount of items: 3
Items: 
Size: 351682 Color: 1
Size: 351388 Color: 1
Size: 296930 Color: 0

Bin 3031: 1 of cap free
Amount of items: 3
Items: 
Size: 350638 Color: 1
Size: 347272 Color: 1
Size: 302090 Color: 0

Bin 3032: 1 of cap free
Amount of items: 3
Items: 
Size: 374879 Color: 1
Size: 333742 Color: 1
Size: 291379 Color: 0

Bin 3033: 1 of cap free
Amount of items: 3
Items: 
Size: 357947 Color: 1
Size: 325027 Color: 1
Size: 317026 Color: 0

Bin 3034: 1 of cap free
Amount of items: 3
Items: 
Size: 361457 Color: 1
Size: 349029 Color: 1
Size: 289514 Color: 0

Bin 3035: 1 of cap free
Amount of items: 3
Items: 
Size: 434139 Color: 1
Size: 292135 Color: 1
Size: 273726 Color: 0

Bin 3036: 1 of cap free
Amount of items: 3
Items: 
Size: 440315 Color: 1
Size: 303330 Color: 1
Size: 256355 Color: 0

Bin 3037: 2 of cap free
Amount of items: 3
Items: 
Size: 382151 Color: 1
Size: 349274 Color: 1
Size: 268574 Color: 0

Bin 3038: 2 of cap free
Amount of items: 3
Items: 
Size: 368998 Color: 1
Size: 357265 Color: 1
Size: 273736 Color: 0

Bin 3039: 2 of cap free
Amount of items: 3
Items: 
Size: 354572 Color: 1
Size: 327547 Color: 1
Size: 317880 Color: 0

Bin 3040: 2 of cap free
Amount of items: 3
Items: 
Size: 427637 Color: 1
Size: 298465 Color: 1
Size: 273897 Color: 0

Bin 3041: 2 of cap free
Amount of items: 3
Items: 
Size: 384743 Color: 1
Size: 339372 Color: 1
Size: 275884 Color: 0

Bin 3042: 2 of cap free
Amount of items: 3
Items: 
Size: 356783 Color: 1
Size: 332895 Color: 1
Size: 310321 Color: 0

Bin 3043: 2 of cap free
Amount of items: 3
Items: 
Size: 385781 Color: 1
Size: 338044 Color: 1
Size: 276174 Color: 0

Bin 3044: 2 of cap free
Amount of items: 3
Items: 
Size: 449190 Color: 1
Size: 299477 Color: 1
Size: 251332 Color: 0

Bin 3045: 2 of cap free
Amount of items: 3
Items: 
Size: 359034 Color: 1
Size: 356538 Color: 1
Size: 284427 Color: 0

Bin 3046: 2 of cap free
Amount of items: 3
Items: 
Size: 390994 Color: 1
Size: 329511 Color: 1
Size: 279494 Color: 0

Bin 3047: 2 of cap free
Amount of items: 3
Items: 
Size: 354799 Color: 1
Size: 349672 Color: 1
Size: 295528 Color: 0

Bin 3048: 2 of cap free
Amount of items: 3
Items: 
Size: 412395 Color: 1
Size: 297646 Color: 1
Size: 289958 Color: 0

Bin 3049: 2 of cap free
Amount of items: 3
Items: 
Size: 359501 Color: 1
Size: 321689 Color: 0
Size: 318809 Color: 1

Bin 3050: 2 of cap free
Amount of items: 3
Items: 
Size: 427011 Color: 1
Size: 322154 Color: 1
Size: 250834 Color: 0

Bin 3051: 2 of cap free
Amount of items: 3
Items: 
Size: 348836 Color: 1
Size: 333190 Color: 1
Size: 317973 Color: 0

Bin 3052: 2 of cap free
Amount of items: 3
Items: 
Size: 357061 Color: 1
Size: 355759 Color: 1
Size: 287179 Color: 0

Bin 3053: 2 of cap free
Amount of items: 3
Items: 
Size: 343382 Color: 1
Size: 333595 Color: 1
Size: 323022 Color: 0

Bin 3054: 2 of cap free
Amount of items: 3
Items: 
Size: 353557 Color: 1
Size: 346273 Color: 1
Size: 300169 Color: 0

Bin 3055: 2 of cap free
Amount of items: 3
Items: 
Size: 358597 Color: 1
Size: 350108 Color: 1
Size: 291294 Color: 0

Bin 3056: 2 of cap free
Amount of items: 3
Items: 
Size: 358414 Color: 1
Size: 353987 Color: 1
Size: 287598 Color: 0

Bin 3057: 2 of cap free
Amount of items: 3
Items: 
Size: 404345 Color: 1
Size: 305432 Color: 0
Size: 290222 Color: 1

Bin 3058: 2 of cap free
Amount of items: 3
Items: 
Size: 362470 Color: 1
Size: 344785 Color: 1
Size: 292744 Color: 0

Bin 3059: 2 of cap free
Amount of items: 3
Items: 
Size: 363299 Color: 1
Size: 355221 Color: 1
Size: 281479 Color: 0

Bin 3060: 2 of cap free
Amount of items: 3
Items: 
Size: 363755 Color: 1
Size: 323497 Color: 1
Size: 312747 Color: 0

Bin 3061: 2 of cap free
Amount of items: 3
Items: 
Size: 356826 Color: 1
Size: 335768 Color: 1
Size: 307405 Color: 0

Bin 3062: 2 of cap free
Amount of items: 3
Items: 
Size: 352994 Color: 1
Size: 350734 Color: 1
Size: 296271 Color: 0

Bin 3063: 2 of cap free
Amount of items: 3
Items: 
Size: 387350 Color: 1
Size: 325402 Color: 0
Size: 287247 Color: 1

Bin 3064: 2 of cap free
Amount of items: 3
Items: 
Size: 358825 Color: 1
Size: 343332 Color: 1
Size: 297842 Color: 0

Bin 3065: 2 of cap free
Amount of items: 3
Items: 
Size: 354872 Color: 1
Size: 339938 Color: 1
Size: 305189 Color: 0

Bin 3066: 2 of cap free
Amount of items: 3
Items: 
Size: 350194 Color: 1
Size: 337916 Color: 1
Size: 311889 Color: 0

Bin 3067: 2 of cap free
Amount of items: 3
Items: 
Size: 394413 Color: 1
Size: 322711 Color: 0
Size: 282875 Color: 1

Bin 3068: 2 of cap free
Amount of items: 3
Items: 
Size: 351456 Color: 1
Size: 341921 Color: 1
Size: 306622 Color: 0

Bin 3069: 2 of cap free
Amount of items: 3
Items: 
Size: 356747 Color: 1
Size: 356068 Color: 1
Size: 287184 Color: 0

Bin 3070: 2 of cap free
Amount of items: 3
Items: 
Size: 354535 Color: 1
Size: 346337 Color: 1
Size: 299127 Color: 0

Bin 3071: 2 of cap free
Amount of items: 3
Items: 
Size: 368028 Color: 1
Size: 324038 Color: 1
Size: 307933 Color: 0

Bin 3072: 2 of cap free
Amount of items: 3
Items: 
Size: 379941 Color: 1
Size: 321618 Color: 1
Size: 298440 Color: 0

Bin 3073: 2 of cap free
Amount of items: 3
Items: 
Size: 426857 Color: 1
Size: 309565 Color: 1
Size: 263577 Color: 0

Bin 3074: 2 of cap free
Amount of items: 3
Items: 
Size: 498477 Color: 1
Size: 251258 Color: 1
Size: 250264 Color: 0

Bin 3075: 3 of cap free
Amount of items: 3
Items: 
Size: 401925 Color: 1
Size: 324242 Color: 1
Size: 273831 Color: 0

Bin 3076: 3 of cap free
Amount of items: 3
Items: 
Size: 448870 Color: 1
Size: 292445 Color: 1
Size: 258683 Color: 0

Bin 3077: 3 of cap free
Amount of items: 3
Items: 
Size: 361952 Color: 1
Size: 340492 Color: 1
Size: 297554 Color: 0

Bin 3078: 3 of cap free
Amount of items: 3
Items: 
Size: 380102 Color: 1
Size: 319962 Color: 1
Size: 299934 Color: 0

Bin 3079: 3 of cap free
Amount of items: 3
Items: 
Size: 396435 Color: 1
Size: 321459 Color: 1
Size: 282104 Color: 0

Bin 3080: 3 of cap free
Amount of items: 3
Items: 
Size: 418842 Color: 1
Size: 321025 Color: 1
Size: 260131 Color: 0

Bin 3081: 3 of cap free
Amount of items: 3
Items: 
Size: 355011 Color: 1
Size: 354059 Color: 1
Size: 290928 Color: 0

Bin 3082: 3 of cap free
Amount of items: 3
Items: 
Size: 358039 Color: 1
Size: 326164 Color: 1
Size: 315795 Color: 0

Bin 3083: 3 of cap free
Amount of items: 3
Items: 
Size: 360075 Color: 1
Size: 350014 Color: 1
Size: 289909 Color: 0

Bin 3084: 3 of cap free
Amount of items: 3
Items: 
Size: 365758 Color: 1
Size: 325243 Color: 1
Size: 308997 Color: 0

Bin 3085: 3 of cap free
Amount of items: 3
Items: 
Size: 444242 Color: 1
Size: 300505 Color: 1
Size: 255251 Color: 0

Bin 3086: 3 of cap free
Amount of items: 3
Items: 
Size: 374385 Color: 1
Size: 357522 Color: 1
Size: 268091 Color: 0

Bin 3087: 3 of cap free
Amount of items: 3
Items: 
Size: 366159 Color: 1
Size: 337143 Color: 1
Size: 296696 Color: 0

Bin 3088: 3 of cap free
Amount of items: 3
Items: 
Size: 363426 Color: 1
Size: 339368 Color: 1
Size: 297204 Color: 0

Bin 3089: 3 of cap free
Amount of items: 3
Items: 
Size: 347575 Color: 1
Size: 346359 Color: 1
Size: 306064 Color: 0

Bin 3090: 3 of cap free
Amount of items: 3
Items: 
Size: 356359 Color: 1
Size: 325667 Color: 1
Size: 317972 Color: 0

Bin 3091: 3 of cap free
Amount of items: 3
Items: 
Size: 360284 Color: 1
Size: 336578 Color: 1
Size: 303136 Color: 0

Bin 3092: 3 of cap free
Amount of items: 3
Items: 
Size: 355555 Color: 1
Size: 354116 Color: 1
Size: 290327 Color: 0

Bin 3093: 3 of cap free
Amount of items: 3
Items: 
Size: 353492 Color: 1
Size: 344280 Color: 1
Size: 302226 Color: 0

Bin 3094: 3 of cap free
Amount of items: 3
Items: 
Size: 349161 Color: 1
Size: 347390 Color: 1
Size: 303447 Color: 0

Bin 3095: 4 of cap free
Amount of items: 3
Items: 
Size: 443219 Color: 1
Size: 281528 Color: 1
Size: 275250 Color: 0

Bin 3096: 4 of cap free
Amount of items: 3
Items: 
Size: 431742 Color: 1
Size: 311586 Color: 1
Size: 256669 Color: 0

Bin 3097: 4 of cap free
Amount of items: 3
Items: 
Size: 363128 Color: 1
Size: 359017 Color: 1
Size: 277852 Color: 0

Bin 3098: 4 of cap free
Amount of items: 3
Items: 
Size: 356860 Color: 1
Size: 350794 Color: 1
Size: 292343 Color: 0

Bin 3099: 4 of cap free
Amount of items: 3
Items: 
Size: 358897 Color: 1
Size: 357925 Color: 1
Size: 283175 Color: 0

Bin 3100: 4 of cap free
Amount of items: 3
Items: 
Size: 405132 Color: 1
Size: 305840 Color: 0
Size: 289025 Color: 1

Bin 3101: 4 of cap free
Amount of items: 3
Items: 
Size: 356477 Color: 1
Size: 346139 Color: 1
Size: 297381 Color: 0

Bin 3102: 4 of cap free
Amount of items: 3
Items: 
Size: 428438 Color: 1
Size: 319454 Color: 1
Size: 252105 Color: 0

Bin 3103: 4 of cap free
Amount of items: 3
Items: 
Size: 351926 Color: 1
Size: 339219 Color: 1
Size: 308852 Color: 0

Bin 3104: 4 of cap free
Amount of items: 3
Items: 
Size: 382574 Color: 1
Size: 359741 Color: 1
Size: 257682 Color: 0

Bin 3105: 4 of cap free
Amount of items: 3
Items: 
Size: 395731 Color: 1
Size: 353731 Color: 1
Size: 250535 Color: 0

Bin 3106: 4 of cap free
Amount of items: 3
Items: 
Size: 414725 Color: 1
Size: 303636 Color: 1
Size: 281636 Color: 0

Bin 3107: 4 of cap free
Amount of items: 3
Items: 
Size: 357300 Color: 1
Size: 329445 Color: 1
Size: 313252 Color: 0

Bin 3108: 4 of cap free
Amount of items: 3
Items: 
Size: 381124 Color: 1
Size: 332846 Color: 1
Size: 286027 Color: 0

Bin 3109: 4 of cap free
Amount of items: 3
Items: 
Size: 360776 Color: 1
Size: 324738 Color: 1
Size: 314483 Color: 0

Bin 3110: 5 of cap free
Amount of items: 3
Items: 
Size: 447316 Color: 1
Size: 293930 Color: 1
Size: 258750 Color: 0

Bin 3111: 5 of cap free
Amount of items: 3
Items: 
Size: 396994 Color: 1
Size: 344262 Color: 1
Size: 258740 Color: 0

Bin 3112: 5 of cap free
Amount of items: 3
Items: 
Size: 398162 Color: 1
Size: 333333 Color: 1
Size: 268501 Color: 0

Bin 3113: 5 of cap free
Amount of items: 3
Items: 
Size: 381531 Color: 1
Size: 341708 Color: 1
Size: 276757 Color: 0

Bin 3114: 5 of cap free
Amount of items: 3
Items: 
Size: 444639 Color: 1
Size: 278562 Color: 0
Size: 276795 Color: 1

Bin 3115: 5 of cap free
Amount of items: 3
Items: 
Size: 386232 Color: 1
Size: 355193 Color: 1
Size: 258571 Color: 0

Bin 3116: 5 of cap free
Amount of items: 3
Items: 
Size: 363943 Color: 1
Size: 360826 Color: 1
Size: 275227 Color: 0

Bin 3117: 5 of cap free
Amount of items: 3
Items: 
Size: 357972 Color: 1
Size: 326894 Color: 1
Size: 315130 Color: 0

Bin 3118: 5 of cap free
Amount of items: 3
Items: 
Size: 393882 Color: 1
Size: 312382 Color: 0
Size: 293732 Color: 1

Bin 3119: 5 of cap free
Amount of items: 3
Items: 
Size: 343757 Color: 1
Size: 337870 Color: 1
Size: 318369 Color: 0

Bin 3120: 5 of cap free
Amount of items: 3
Items: 
Size: 405462 Color: 1
Size: 310924 Color: 1
Size: 283610 Color: 0

Bin 3121: 5 of cap free
Amount of items: 3
Items: 
Size: 355778 Color: 1
Size: 341886 Color: 1
Size: 302332 Color: 0

Bin 3122: 5 of cap free
Amount of items: 3
Items: 
Size: 402194 Color: 1
Size: 322638 Color: 0
Size: 275164 Color: 1

Bin 3123: 5 of cap free
Amount of items: 3
Items: 
Size: 371049 Color: 1
Size: 362421 Color: 1
Size: 266526 Color: 0

Bin 3124: 5 of cap free
Amount of items: 3
Items: 
Size: 358477 Color: 1
Size: 346600 Color: 1
Size: 294919 Color: 0

Bin 3125: 5 of cap free
Amount of items: 3
Items: 
Size: 349844 Color: 1
Size: 342856 Color: 1
Size: 307296 Color: 0

Bin 3126: 5 of cap free
Amount of items: 3
Items: 
Size: 402875 Color: 1
Size: 315872 Color: 0
Size: 281249 Color: 1

Bin 3127: 6 of cap free
Amount of items: 3
Items: 
Size: 411584 Color: 1
Size: 332644 Color: 1
Size: 255767 Color: 0

Bin 3128: 6 of cap free
Amount of items: 3
Items: 
Size: 370981 Color: 1
Size: 323780 Color: 1
Size: 305234 Color: 0

Bin 3129: 6 of cap free
Amount of items: 3
Items: 
Size: 398666 Color: 1
Size: 343540 Color: 1
Size: 257789 Color: 0

Bin 3130: 6 of cap free
Amount of items: 3
Items: 
Size: 395179 Color: 1
Size: 304739 Color: 0
Size: 300077 Color: 1

Bin 3131: 6 of cap free
Amount of items: 3
Items: 
Size: 389372 Color: 1
Size: 354835 Color: 1
Size: 255788 Color: 0

Bin 3132: 6 of cap free
Amount of items: 3
Items: 
Size: 403112 Color: 1
Size: 340074 Color: 1
Size: 256809 Color: 0

Bin 3133: 6 of cap free
Amount of items: 3
Items: 
Size: 394906 Color: 1
Size: 336374 Color: 1
Size: 268715 Color: 0

Bin 3134: 6 of cap free
Amount of items: 3
Items: 
Size: 482495 Color: 1
Size: 266050 Color: 0
Size: 251450 Color: 1

Bin 3135: 6 of cap free
Amount of items: 3
Items: 
Size: 364234 Color: 1
Size: 360342 Color: 1
Size: 275419 Color: 0

Bin 3136: 6 of cap free
Amount of items: 3
Items: 
Size: 353220 Color: 1
Size: 333131 Color: 1
Size: 313644 Color: 0

Bin 3137: 6 of cap free
Amount of items: 3
Items: 
Size: 466180 Color: 1
Size: 277962 Color: 1
Size: 255853 Color: 0

Bin 3138: 6 of cap free
Amount of items: 3
Items: 
Size: 419471 Color: 1
Size: 317050 Color: 0
Size: 263474 Color: 1

Bin 3139: 6 of cap free
Amount of items: 3
Items: 
Size: 352291 Color: 1
Size: 340122 Color: 1
Size: 307582 Color: 0

Bin 3140: 6 of cap free
Amount of items: 3
Items: 
Size: 359972 Color: 1
Size: 359038 Color: 1
Size: 280985 Color: 0

Bin 3141: 6 of cap free
Amount of items: 3
Items: 
Size: 352370 Color: 1
Size: 347212 Color: 1
Size: 300413 Color: 0

Bin 3142: 6 of cap free
Amount of items: 3
Items: 
Size: 363812 Color: 1
Size: 357542 Color: 1
Size: 278641 Color: 0

Bin 3143: 7 of cap free
Amount of items: 3
Items: 
Size: 352150 Color: 1
Size: 334145 Color: 1
Size: 313699 Color: 0

Bin 3144: 7 of cap free
Amount of items: 3
Items: 
Size: 357114 Color: 1
Size: 348346 Color: 1
Size: 294534 Color: 0

Bin 3145: 7 of cap free
Amount of items: 3
Items: 
Size: 392719 Color: 1
Size: 313573 Color: 1
Size: 293702 Color: 0

Bin 3146: 7 of cap free
Amount of items: 3
Items: 
Size: 423663 Color: 1
Size: 288387 Color: 0
Size: 287944 Color: 1

Bin 3147: 7 of cap free
Amount of items: 3
Items: 
Size: 360488 Color: 1
Size: 324698 Color: 0
Size: 314808 Color: 1

Bin 3148: 7 of cap free
Amount of items: 3
Items: 
Size: 351492 Color: 1
Size: 345328 Color: 1
Size: 303174 Color: 0

Bin 3149: 7 of cap free
Amount of items: 3
Items: 
Size: 400294 Color: 1
Size: 328668 Color: 1
Size: 271032 Color: 0

Bin 3150: 7 of cap free
Amount of items: 3
Items: 
Size: 353563 Color: 1
Size: 347648 Color: 1
Size: 298783 Color: 0

Bin 3151: 7 of cap free
Amount of items: 3
Items: 
Size: 353954 Color: 1
Size: 353369 Color: 1
Size: 292671 Color: 0

Bin 3152: 7 of cap free
Amount of items: 3
Items: 
Size: 363735 Color: 1
Size: 358760 Color: 1
Size: 277499 Color: 0

Bin 3153: 7 of cap free
Amount of items: 3
Items: 
Size: 359574 Color: 1
Size: 353797 Color: 1
Size: 286623 Color: 0

Bin 3154: 8 of cap free
Amount of items: 3
Items: 
Size: 410388 Color: 1
Size: 322467 Color: 1
Size: 267138 Color: 0

Bin 3155: 8 of cap free
Amount of items: 3
Items: 
Size: 439518 Color: 1
Size: 303465 Color: 1
Size: 257010 Color: 0

Bin 3156: 8 of cap free
Amount of items: 3
Items: 
Size: 408512 Color: 1
Size: 339642 Color: 1
Size: 251839 Color: 0

Bin 3157: 8 of cap free
Amount of items: 3
Items: 
Size: 403744 Color: 1
Size: 342107 Color: 1
Size: 254142 Color: 0

Bin 3158: 8 of cap free
Amount of items: 3
Items: 
Size: 403625 Color: 1
Size: 299372 Color: 1
Size: 296996 Color: 0

Bin 3159: 8 of cap free
Amount of items: 3
Items: 
Size: 363245 Color: 1
Size: 327246 Color: 1
Size: 309502 Color: 0

Bin 3160: 8 of cap free
Amount of items: 3
Items: 
Size: 364759 Color: 1
Size: 347867 Color: 1
Size: 287367 Color: 0

Bin 3161: 8 of cap free
Amount of items: 3
Items: 
Size: 374445 Color: 1
Size: 345338 Color: 1
Size: 280210 Color: 0

Bin 3162: 9 of cap free
Amount of items: 3
Items: 
Size: 367584 Color: 1
Size: 331151 Color: 0
Size: 301257 Color: 1

Bin 3163: 9 of cap free
Amount of items: 3
Items: 
Size: 446231 Color: 1
Size: 298541 Color: 1
Size: 255220 Color: 0

Bin 3164: 9 of cap free
Amount of items: 3
Items: 
Size: 382300 Color: 1
Size: 310607 Color: 1
Size: 307085 Color: 0

Bin 3165: 9 of cap free
Amount of items: 3
Items: 
Size: 434821 Color: 1
Size: 306477 Color: 1
Size: 258694 Color: 0

Bin 3166: 9 of cap free
Amount of items: 3
Items: 
Size: 360747 Color: 1
Size: 326918 Color: 1
Size: 312327 Color: 0

Bin 3167: 9 of cap free
Amount of items: 3
Items: 
Size: 398436 Color: 1
Size: 338786 Color: 1
Size: 262770 Color: 0

Bin 3168: 9 of cap free
Amount of items: 3
Items: 
Size: 360088 Color: 1
Size: 335938 Color: 1
Size: 303966 Color: 0

Bin 3169: 9 of cap free
Amount of items: 3
Items: 
Size: 351835 Color: 1
Size: 342600 Color: 1
Size: 305557 Color: 0

Bin 3170: 9 of cap free
Amount of items: 3
Items: 
Size: 358514 Color: 1
Size: 358201 Color: 1
Size: 283277 Color: 0

Bin 3171: 9 of cap free
Amount of items: 3
Items: 
Size: 390419 Color: 1
Size: 348363 Color: 1
Size: 261210 Color: 0

Bin 3172: 9 of cap free
Amount of items: 3
Items: 
Size: 345527 Color: 1
Size: 339367 Color: 1
Size: 315098 Color: 0

Bin 3173: 9 of cap free
Amount of items: 3
Items: 
Size: 352734 Color: 1
Size: 352134 Color: 1
Size: 295124 Color: 0

Bin 3174: 10 of cap free
Amount of items: 3
Items: 
Size: 449453 Color: 1
Size: 276050 Color: 1
Size: 274488 Color: 0

Bin 3175: 10 of cap free
Amount of items: 3
Items: 
Size: 422355 Color: 1
Size: 303390 Color: 1
Size: 274246 Color: 0

Bin 3176: 10 of cap free
Amount of items: 3
Items: 
Size: 388196 Color: 1
Size: 354902 Color: 1
Size: 256893 Color: 0

Bin 3177: 10 of cap free
Amount of items: 3
Items: 
Size: 420349 Color: 1
Size: 297211 Color: 0
Size: 282431 Color: 1

Bin 3178: 10 of cap free
Amount of items: 3
Items: 
Size: 446479 Color: 1
Size: 294557 Color: 1
Size: 258955 Color: 0

Bin 3179: 10 of cap free
Amount of items: 3
Items: 
Size: 414245 Color: 1
Size: 327463 Color: 1
Size: 258283 Color: 0

Bin 3180: 10 of cap free
Amount of items: 3
Items: 
Size: 378707 Color: 1
Size: 355158 Color: 1
Size: 266126 Color: 0

Bin 3181: 11 of cap free
Amount of items: 3
Items: 
Size: 426796 Color: 1
Size: 289843 Color: 0
Size: 283351 Color: 1

Bin 3182: 11 of cap free
Amount of items: 3
Items: 
Size: 350138 Color: 1
Size: 331728 Color: 1
Size: 318124 Color: 0

Bin 3183: 11 of cap free
Amount of items: 3
Items: 
Size: 355439 Color: 1
Size: 344609 Color: 1
Size: 299942 Color: 0

Bin 3184: 11 of cap free
Amount of items: 3
Items: 
Size: 423478 Color: 1
Size: 313280 Color: 0
Size: 263232 Color: 1

Bin 3185: 11 of cap free
Amount of items: 3
Items: 
Size: 440401 Color: 1
Size: 295259 Color: 1
Size: 264330 Color: 0

Bin 3186: 11 of cap free
Amount of items: 3
Items: 
Size: 380342 Color: 1
Size: 328013 Color: 1
Size: 291635 Color: 0

Bin 3187: 12 of cap free
Amount of items: 3
Items: 
Size: 372198 Color: 1
Size: 352757 Color: 1
Size: 275034 Color: 0

Bin 3188: 12 of cap free
Amount of items: 3
Items: 
Size: 395080 Color: 1
Size: 317208 Color: 0
Size: 287701 Color: 1

Bin 3189: 12 of cap free
Amount of items: 3
Items: 
Size: 405341 Color: 1
Size: 308341 Color: 0
Size: 286307 Color: 1

Bin 3190: 12 of cap free
Amount of items: 3
Items: 
Size: 363542 Color: 1
Size: 352448 Color: 1
Size: 283999 Color: 0

Bin 3191: 12 of cap free
Amount of items: 3
Items: 
Size: 392563 Color: 1
Size: 310267 Color: 1
Size: 297159 Color: 0

Bin 3192: 12 of cap free
Amount of items: 3
Items: 
Size: 382249 Color: 1
Size: 350605 Color: 1
Size: 267135 Color: 0

Bin 3193: 13 of cap free
Amount of items: 3
Items: 
Size: 358068 Color: 1
Size: 351659 Color: 1
Size: 290261 Color: 0

Bin 3194: 13 of cap free
Amount of items: 3
Items: 
Size: 395379 Color: 1
Size: 329429 Color: 1
Size: 275180 Color: 0

Bin 3195: 13 of cap free
Amount of items: 3
Items: 
Size: 375441 Color: 1
Size: 366087 Color: 1
Size: 258460 Color: 0

Bin 3196: 13 of cap free
Amount of items: 3
Items: 
Size: 362415 Color: 1
Size: 333244 Color: 0
Size: 304329 Color: 1

Bin 3197: 13 of cap free
Amount of items: 3
Items: 
Size: 363150 Color: 1
Size: 355231 Color: 1
Size: 281607 Color: 0

Bin 3198: 14 of cap free
Amount of items: 3
Items: 
Size: 436031 Color: 1
Size: 301692 Color: 1
Size: 262264 Color: 0

Bin 3199: 14 of cap free
Amount of items: 3
Items: 
Size: 427860 Color: 1
Size: 307896 Color: 1
Size: 264231 Color: 0

Bin 3200: 14 of cap free
Amount of items: 3
Items: 
Size: 352390 Color: 1
Size: 347694 Color: 1
Size: 299903 Color: 0

Bin 3201: 15 of cap free
Amount of items: 3
Items: 
Size: 423398 Color: 1
Size: 314455 Color: 1
Size: 262133 Color: 0

Bin 3202: 15 of cap free
Amount of items: 3
Items: 
Size: 346260 Color: 1
Size: 341589 Color: 1
Size: 312137 Color: 0

Bin 3203: 15 of cap free
Amount of items: 3
Items: 
Size: 406078 Color: 1
Size: 306171 Color: 0
Size: 287737 Color: 1

Bin 3204: 15 of cap free
Amount of items: 3
Items: 
Size: 440879 Color: 1
Size: 284207 Color: 1
Size: 274900 Color: 0

Bin 3205: 15 of cap free
Amount of items: 3
Items: 
Size: 358830 Color: 1
Size: 337033 Color: 1
Size: 304123 Color: 0

Bin 3206: 15 of cap free
Amount of items: 3
Items: 
Size: 351886 Color: 1
Size: 341258 Color: 1
Size: 306842 Color: 0

Bin 3207: 16 of cap free
Amount of items: 3
Items: 
Size: 420714 Color: 1
Size: 313474 Color: 1
Size: 265797 Color: 0

Bin 3208: 16 of cap free
Amount of items: 3
Items: 
Size: 375974 Color: 1
Size: 314879 Color: 1
Size: 309132 Color: 0

Bin 3209: 16 of cap free
Amount of items: 3
Items: 
Size: 363795 Color: 1
Size: 351764 Color: 1
Size: 284426 Color: 0

Bin 3210: 16 of cap free
Amount of items: 3
Items: 
Size: 444793 Color: 1
Size: 295304 Color: 1
Size: 259888 Color: 0

Bin 3211: 16 of cap free
Amount of items: 3
Items: 
Size: 414143 Color: 1
Size: 312228 Color: 1
Size: 273614 Color: 0

Bin 3212: 16 of cap free
Amount of items: 3
Items: 
Size: 357236 Color: 1
Size: 353467 Color: 1
Size: 289282 Color: 0

Bin 3213: 16 of cap free
Amount of items: 3
Items: 
Size: 406680 Color: 1
Size: 301422 Color: 1
Size: 291883 Color: 0

Bin 3214: 16 of cap free
Amount of items: 3
Items: 
Size: 349050 Color: 1
Size: 325912 Color: 0
Size: 325023 Color: 1

Bin 3215: 17 of cap free
Amount of items: 3
Items: 
Size: 362080 Color: 1
Size: 337272 Color: 1
Size: 300632 Color: 0

Bin 3216: 17 of cap free
Amount of items: 3
Items: 
Size: 434232 Color: 1
Size: 287764 Color: 1
Size: 277988 Color: 0

Bin 3217: 17 of cap free
Amount of items: 3
Items: 
Size: 401463 Color: 1
Size: 319148 Color: 1
Size: 279373 Color: 0

Bin 3218: 17 of cap free
Amount of items: 3
Items: 
Size: 350341 Color: 1
Size: 348678 Color: 1
Size: 300965 Color: 0

Bin 3219: 19 of cap free
Amount of items: 3
Items: 
Size: 375060 Color: 1
Size: 339027 Color: 1
Size: 285895 Color: 0

Bin 3220: 19 of cap free
Amount of items: 3
Items: 
Size: 394405 Color: 1
Size: 335588 Color: 1
Size: 269989 Color: 0

Bin 3221: 20 of cap free
Amount of items: 3
Items: 
Size: 435601 Color: 1
Size: 295561 Color: 1
Size: 268819 Color: 0

Bin 3222: 20 of cap free
Amount of items: 3
Items: 
Size: 373228 Color: 1
Size: 358976 Color: 1
Size: 267777 Color: 0

Bin 3223: 20 of cap free
Amount of items: 3
Items: 
Size: 358023 Color: 1
Size: 340234 Color: 1
Size: 301724 Color: 0

Bin 3224: 20 of cap free
Amount of items: 3
Items: 
Size: 364654 Color: 1
Size: 361259 Color: 1
Size: 274068 Color: 0

Bin 3225: 20 of cap free
Amount of items: 3
Items: 
Size: 386513 Color: 1
Size: 345110 Color: 1
Size: 268358 Color: 0

Bin 3226: 21 of cap free
Amount of items: 3
Items: 
Size: 379564 Color: 1
Size: 347466 Color: 1
Size: 272950 Color: 0

Bin 3227: 22 of cap free
Amount of items: 3
Items: 
Size: 463439 Color: 1
Size: 275372 Color: 1
Size: 261168 Color: 0

Bin 3228: 22 of cap free
Amount of items: 3
Items: 
Size: 357564 Color: 1
Size: 339689 Color: 1
Size: 302726 Color: 0

Bin 3229: 22 of cap free
Amount of items: 3
Items: 
Size: 408385 Color: 1
Size: 301077 Color: 1
Size: 290517 Color: 0

Bin 3230: 23 of cap free
Amount of items: 3
Items: 
Size: 441937 Color: 1
Size: 289089 Color: 1
Size: 268952 Color: 0

Bin 3231: 23 of cap free
Amount of items: 3
Items: 
Size: 435979 Color: 1
Size: 305944 Color: 1
Size: 258055 Color: 0

Bin 3232: 23 of cap free
Amount of items: 3
Items: 
Size: 342856 Color: 1
Size: 340780 Color: 1
Size: 316342 Color: 0

Bin 3233: 24 of cap free
Amount of items: 3
Items: 
Size: 420154 Color: 1
Size: 318682 Color: 1
Size: 261141 Color: 0

Bin 3234: 25 of cap free
Amount of items: 3
Items: 
Size: 341439 Color: 1
Size: 340994 Color: 1
Size: 317543 Color: 0

Bin 3235: 25 of cap free
Amount of items: 3
Items: 
Size: 434707 Color: 1
Size: 309047 Color: 1
Size: 256222 Color: 0

Bin 3236: 26 of cap free
Amount of items: 3
Items: 
Size: 360955 Color: 1
Size: 358791 Color: 1
Size: 280229 Color: 0

Bin 3237: 26 of cap free
Amount of items: 3
Items: 
Size: 356206 Color: 1
Size: 338419 Color: 1
Size: 305350 Color: 0

Bin 3238: 27 of cap free
Amount of items: 3
Items: 
Size: 378386 Color: 1
Size: 347298 Color: 1
Size: 274290 Color: 0

Bin 3239: 27 of cap free
Amount of items: 3
Items: 
Size: 369607 Color: 1
Size: 368889 Color: 1
Size: 261478 Color: 0

Bin 3240: 27 of cap free
Amount of items: 3
Items: 
Size: 399772 Color: 1
Size: 330549 Color: 1
Size: 269653 Color: 0

Bin 3241: 27 of cap free
Amount of items: 3
Items: 
Size: 487572 Color: 1
Size: 261419 Color: 1
Size: 250983 Color: 0

Bin 3242: 28 of cap free
Amount of items: 3
Items: 
Size: 450055 Color: 1
Size: 287688 Color: 1
Size: 262230 Color: 0

Bin 3243: 28 of cap free
Amount of items: 3
Items: 
Size: 453282 Color: 1
Size: 278121 Color: 1
Size: 268570 Color: 0

Bin 3244: 28 of cap free
Amount of items: 3
Items: 
Size: 378494 Color: 1
Size: 359854 Color: 1
Size: 261625 Color: 0

Bin 3245: 28 of cap free
Amount of items: 3
Items: 
Size: 346726 Color: 1
Size: 343054 Color: 1
Size: 310193 Color: 0

Bin 3246: 28 of cap free
Amount of items: 3
Items: 
Size: 359150 Color: 1
Size: 326062 Color: 0
Size: 314761 Color: 1

Bin 3247: 29 of cap free
Amount of items: 3
Items: 
Size: 379467 Color: 1
Size: 349204 Color: 1
Size: 271301 Color: 0

Bin 3248: 29 of cap free
Amount of items: 3
Items: 
Size: 362260 Color: 1
Size: 359512 Color: 1
Size: 278200 Color: 0

Bin 3249: 30 of cap free
Amount of items: 3
Items: 
Size: 397833 Color: 1
Size: 348764 Color: 1
Size: 253374 Color: 0

Bin 3250: 30 of cap free
Amount of items: 3
Items: 
Size: 351885 Color: 1
Size: 346062 Color: 1
Size: 302024 Color: 0

Bin 3251: 31 of cap free
Amount of items: 3
Items: 
Size: 349385 Color: 1
Size: 337086 Color: 1
Size: 313499 Color: 0

Bin 3252: 31 of cap free
Amount of items: 3
Items: 
Size: 430105 Color: 1
Size: 313292 Color: 1
Size: 256573 Color: 0

Bin 3253: 31 of cap free
Amount of items: 3
Items: 
Size: 346676 Color: 1
Size: 345214 Color: 1
Size: 308080 Color: 0

Bin 3254: 32 of cap free
Amount of items: 3
Items: 
Size: 368571 Color: 1
Size: 359398 Color: 1
Size: 272000 Color: 0

Bin 3255: 33 of cap free
Amount of items: 3
Items: 
Size: 368697 Color: 1
Size: 341496 Color: 1
Size: 289775 Color: 0

Bin 3256: 34 of cap free
Amount of items: 3
Items: 
Size: 395560 Color: 1
Size: 309283 Color: 1
Size: 295124 Color: 0

Bin 3257: 35 of cap free
Amount of items: 3
Items: 
Size: 384860 Color: 1
Size: 352515 Color: 1
Size: 262591 Color: 0

Bin 3258: 35 of cap free
Amount of items: 3
Items: 
Size: 361665 Color: 1
Size: 331595 Color: 1
Size: 306706 Color: 0

Bin 3259: 36 of cap free
Amount of items: 3
Items: 
Size: 461463 Color: 1
Size: 285337 Color: 1
Size: 253165 Color: 0

Bin 3260: 37 of cap free
Amount of items: 3
Items: 
Size: 405180 Color: 1
Size: 308910 Color: 0
Size: 285874 Color: 1

Bin 3261: 38 of cap free
Amount of items: 3
Items: 
Size: 405380 Color: 1
Size: 321412 Color: 1
Size: 273171 Color: 0

Bin 3262: 39 of cap free
Amount of items: 3
Items: 
Size: 422641 Color: 1
Size: 303889 Color: 1
Size: 273432 Color: 0

Bin 3263: 42 of cap free
Amount of items: 3
Items: 
Size: 393622 Color: 1
Size: 309537 Color: 0
Size: 296800 Color: 1

Bin 3264: 42 of cap free
Amount of items: 3
Items: 
Size: 355230 Color: 1
Size: 349193 Color: 1
Size: 295536 Color: 0

Bin 3265: 43 of cap free
Amount of items: 3
Items: 
Size: 355420 Color: 1
Size: 344701 Color: 1
Size: 299837 Color: 0

Bin 3266: 45 of cap free
Amount of items: 3
Items: 
Size: 421767 Color: 1
Size: 299741 Color: 1
Size: 278448 Color: 0

Bin 3267: 45 of cap free
Amount of items: 3
Items: 
Size: 476538 Color: 1
Size: 265999 Color: 1
Size: 257419 Color: 0

Bin 3268: 45 of cap free
Amount of items: 3
Items: 
Size: 375896 Color: 1
Size: 312867 Color: 1
Size: 311193 Color: 0

Bin 3269: 47 of cap free
Amount of items: 3
Items: 
Size: 390348 Color: 1
Size: 355790 Color: 1
Size: 253816 Color: 0

Bin 3270: 49 of cap free
Amount of items: 3
Items: 
Size: 482718 Color: 1
Size: 262399 Color: 1
Size: 254835 Color: 0

Bin 3271: 49 of cap free
Amount of items: 3
Items: 
Size: 343693 Color: 1
Size: 338957 Color: 1
Size: 317302 Color: 0

Bin 3272: 50 of cap free
Amount of items: 3
Items: 
Size: 391393 Color: 1
Size: 342783 Color: 1
Size: 265775 Color: 0

Bin 3273: 50 of cap free
Amount of items: 3
Items: 
Size: 478565 Color: 1
Size: 270572 Color: 1
Size: 250814 Color: 0

Bin 3274: 51 of cap free
Amount of items: 3
Items: 
Size: 442457 Color: 1
Size: 295151 Color: 1
Size: 262342 Color: 0

Bin 3275: 53 of cap free
Amount of items: 3
Items: 
Size: 464748 Color: 1
Size: 272675 Color: 1
Size: 262525 Color: 0

Bin 3276: 53 of cap free
Amount of items: 3
Items: 
Size: 356944 Color: 1
Size: 356030 Color: 1
Size: 286974 Color: 0

Bin 3277: 54 of cap free
Amount of items: 3
Items: 
Size: 393181 Color: 1
Size: 306781 Color: 1
Size: 299985 Color: 0

Bin 3278: 55 of cap free
Amount of items: 3
Items: 
Size: 356430 Color: 1
Size: 333044 Color: 1
Size: 310472 Color: 0

Bin 3279: 56 of cap free
Amount of items: 3
Items: 
Size: 438389 Color: 1
Size: 308182 Color: 1
Size: 253374 Color: 0

Bin 3280: 56 of cap free
Amount of items: 3
Items: 
Size: 426454 Color: 1
Size: 317550 Color: 1
Size: 255941 Color: 0

Bin 3281: 57 of cap free
Amount of items: 3
Items: 
Size: 410170 Color: 1
Size: 322535 Color: 1
Size: 267239 Color: 0

Bin 3282: 57 of cap free
Amount of items: 3
Items: 
Size: 356859 Color: 1
Size: 332794 Color: 1
Size: 310291 Color: 0

Bin 3283: 58 of cap free
Amount of items: 3
Items: 
Size: 413592 Color: 1
Size: 327420 Color: 1
Size: 258931 Color: 0

Bin 3284: 61 of cap free
Amount of items: 3
Items: 
Size: 442726 Color: 1
Size: 291000 Color: 1
Size: 266214 Color: 0

Bin 3285: 61 of cap free
Amount of items: 3
Items: 
Size: 427605 Color: 1
Size: 308624 Color: 1
Size: 263711 Color: 0

Bin 3286: 61 of cap free
Amount of items: 3
Items: 
Size: 382020 Color: 1
Size: 346972 Color: 1
Size: 270948 Color: 0

Bin 3287: 72 of cap free
Amount of items: 3
Items: 
Size: 385978 Color: 1
Size: 310787 Color: 1
Size: 303164 Color: 0

Bin 3288: 74 of cap free
Amount of items: 3
Items: 
Size: 496922 Color: 1
Size: 252194 Color: 1
Size: 250811 Color: 0

Bin 3289: 74 of cap free
Amount of items: 3
Items: 
Size: 359230 Color: 1
Size: 322015 Color: 1
Size: 318682 Color: 0

Bin 3290: 77 of cap free
Amount of items: 3
Items: 
Size: 424612 Color: 1
Size: 295341 Color: 1
Size: 279971 Color: 0

Bin 3291: 77 of cap free
Amount of items: 3
Items: 
Size: 458287 Color: 1
Size: 278715 Color: 1
Size: 262922 Color: 0

Bin 3292: 81 of cap free
Amount of items: 3
Items: 
Size: 359392 Color: 1
Size: 355473 Color: 1
Size: 285055 Color: 0

Bin 3293: 81 of cap free
Amount of items: 3
Items: 
Size: 444918 Color: 1
Size: 285479 Color: 1
Size: 269523 Color: 0

Bin 3294: 81 of cap free
Amount of items: 3
Items: 
Size: 371284 Color: 1
Size: 340734 Color: 1
Size: 287902 Color: 0

Bin 3295: 82 of cap free
Amount of items: 3
Items: 
Size: 466316 Color: 1
Size: 277958 Color: 1
Size: 255645 Color: 0

Bin 3296: 87 of cap free
Amount of items: 3
Items: 
Size: 351444 Color: 1
Size: 341385 Color: 1
Size: 307085 Color: 0

Bin 3297: 88 of cap free
Amount of items: 3
Items: 
Size: 421398 Color: 1
Size: 292418 Color: 1
Size: 286097 Color: 0

Bin 3298: 95 of cap free
Amount of items: 3
Items: 
Size: 365040 Color: 1
Size: 339342 Color: 1
Size: 295524 Color: 0

Bin 3299: 103 of cap free
Amount of items: 3
Items: 
Size: 367354 Color: 1
Size: 359006 Color: 1
Size: 273538 Color: 0

Bin 3300: 104 of cap free
Amount of items: 3
Items: 
Size: 363887 Color: 1
Size: 338305 Color: 1
Size: 297705 Color: 0

Bin 3301: 105 of cap free
Amount of items: 3
Items: 
Size: 418297 Color: 1
Size: 311614 Color: 1
Size: 269985 Color: 0

Bin 3302: 109 of cap free
Amount of items: 3
Items: 
Size: 395368 Color: 1
Size: 344921 Color: 1
Size: 259603 Color: 0

Bin 3303: 114 of cap free
Amount of items: 3
Items: 
Size: 406313 Color: 1
Size: 340227 Color: 1
Size: 253347 Color: 0

Bin 3304: 116 of cap free
Amount of items: 3
Items: 
Size: 394638 Color: 1
Size: 344370 Color: 1
Size: 260877 Color: 0

Bin 3305: 116 of cap free
Amount of items: 3
Items: 
Size: 463832 Color: 1
Size: 282241 Color: 1
Size: 253812 Color: 0

Bin 3306: 126 of cap free
Amount of items: 3
Items: 
Size: 412646 Color: 1
Size: 294285 Color: 0
Size: 292944 Color: 1

Bin 3307: 136 of cap free
Amount of items: 3
Items: 
Size: 362670 Color: 1
Size: 348567 Color: 1
Size: 288628 Color: 0

Bin 3308: 137 of cap free
Amount of items: 3
Items: 
Size: 406527 Color: 1
Size: 322461 Color: 1
Size: 270876 Color: 0

Bin 3309: 167 of cap free
Amount of items: 3
Items: 
Size: 383317 Color: 1
Size: 339553 Color: 1
Size: 276964 Color: 0

Bin 3310: 169 of cap free
Amount of items: 3
Items: 
Size: 416634 Color: 1
Size: 316036 Color: 1
Size: 267162 Color: 0

Bin 3311: 170 of cap free
Amount of items: 3
Items: 
Size: 363588 Color: 1
Size: 344018 Color: 1
Size: 292225 Color: 0

Bin 3312: 175 of cap free
Amount of items: 3
Items: 
Size: 404667 Color: 1
Size: 300074 Color: 1
Size: 295085 Color: 0

Bin 3313: 177 of cap free
Amount of items: 3
Items: 
Size: 407488 Color: 1
Size: 314087 Color: 0
Size: 278249 Color: 1

Bin 3314: 183 of cap free
Amount of items: 3
Items: 
Size: 373735 Color: 1
Size: 340199 Color: 1
Size: 285884 Color: 0

Bin 3315: 196 of cap free
Amount of items: 3
Items: 
Size: 366894 Color: 1
Size: 329500 Color: 1
Size: 303411 Color: 0

Bin 3316: 209 of cap free
Amount of items: 3
Items: 
Size: 469059 Color: 1
Size: 275650 Color: 1
Size: 255083 Color: 0

Bin 3317: 236 of cap free
Amount of items: 3
Items: 
Size: 410427 Color: 1
Size: 330147 Color: 1
Size: 259191 Color: 0

Bin 3318: 238 of cap free
Amount of items: 3
Items: 
Size: 347041 Color: 1
Size: 339226 Color: 1
Size: 313496 Color: 0

Bin 3319: 267 of cap free
Amount of items: 3
Items: 
Size: 459603 Color: 1
Size: 285476 Color: 1
Size: 254655 Color: 0

Bin 3320: 280 of cap free
Amount of items: 3
Items: 
Size: 384782 Color: 1
Size: 320662 Color: 1
Size: 294277 Color: 0

Bin 3321: 282 of cap free
Amount of items: 3
Items: 
Size: 395713 Color: 1
Size: 352563 Color: 1
Size: 251443 Color: 0

Bin 3322: 308 of cap free
Amount of items: 3
Items: 
Size: 406143 Color: 1
Size: 311015 Color: 1
Size: 282535 Color: 0

Bin 3323: 342 of cap free
Amount of items: 3
Items: 
Size: 358774 Color: 1
Size: 351857 Color: 1
Size: 289028 Color: 0

Bin 3324: 471 of cap free
Amount of items: 3
Items: 
Size: 398184 Color: 1
Size: 317657 Color: 1
Size: 283689 Color: 0

Bin 3325: 573 of cap free
Amount of items: 3
Items: 
Size: 440132 Color: 1
Size: 303547 Color: 1
Size: 255749 Color: 0

Bin 3326: 651 of cap free
Amount of items: 3
Items: 
Size: 448830 Color: 1
Size: 300000 Color: 1
Size: 250520 Color: 0

Bin 3327: 740 of cap free
Amount of items: 3
Items: 
Size: 434554 Color: 1
Size: 291348 Color: 1
Size: 273359 Color: 0

Bin 3328: 2214 of cap free
Amount of items: 3
Items: 
Size: 421562 Color: 1
Size: 294060 Color: 0
Size: 282165 Color: 1

Bin 3329: 2928 of cap free
Amount of items: 3
Items: 
Size: 472106 Color: 1
Size: 266300 Color: 1
Size: 258667 Color: 0

Bin 3330: 4412 of cap free
Amount of items: 3
Items: 
Size: 430933 Color: 1
Size: 298011 Color: 1
Size: 266645 Color: 0

Bin 3331: 8652 of cap free
Amount of items: 3
Items: 
Size: 355993 Color: 1
Size: 331903 Color: 0
Size: 303453 Color: 1

Bin 3332: 54597 of cap free
Amount of items: 3
Items: 
Size: 335295 Color: 1
Size: 321170 Color: 1
Size: 288939 Color: 0

Bin 3333: 162108 of cap free
Amount of items: 3
Items: 
Size: 303253 Color: 1
Size: 277371 Color: 1
Size: 257269 Color: 0

Bin 3334: 249486 of cap free
Amount of items: 2
Items: 
Size: 498686 Color: 1
Size: 251829 Color: 0

Bin 3335: 503915 of cap free
Amount of items: 1
Items: 
Size: 496086 Color: 1

Total size: 3334003334
Total free space: 1000001

