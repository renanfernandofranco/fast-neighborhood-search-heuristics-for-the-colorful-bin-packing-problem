Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1238 Color: 142
Size: 1020 Color: 134
Size: 94 Color: 40
Size: 56 Color: 20
Size: 48 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 155
Size: 512 Color: 103
Size: 300 Color: 81

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 162
Size: 620 Color: 113
Size: 120 Color: 49

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1900 Color: 173
Size: 484 Color: 101
Size: 72 Color: 28

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1916 Color: 175
Size: 468 Color: 100
Size: 72 Color: 26

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 176
Size: 467 Color: 99
Size: 64 Color: 23

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 181
Size: 422 Color: 93
Size: 60 Color: 21

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 182
Size: 443 Color: 95
Size: 8 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 184
Size: 380 Color: 90
Size: 56 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2021 Color: 185
Size: 363 Color: 86
Size: 72 Color: 27

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 186
Size: 244 Color: 73
Size: 176 Color: 63

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 187
Size: 280 Color: 78
Size: 126 Color: 52

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 192
Size: 330 Color: 83
Size: 8 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 193
Size: 204 Color: 68
Size: 112 Color: 46

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 194
Size: 244 Color: 74
Size: 66 Color: 25

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 195
Size: 226 Color: 71
Size: 64 Color: 24

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 197
Size: 168 Color: 59
Size: 102 Color: 44

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 198
Size: 168 Color: 60
Size: 100 Color: 41

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 200
Size: 200 Color: 65
Size: 54 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 201
Size: 204 Color: 67
Size: 48 Color: 17

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 157
Size: 622 Color: 115
Size: 144 Color: 55

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1841 Color: 168
Size: 454 Color: 98
Size: 160 Color: 58

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1914 Color: 174
Size: 541 Color: 107

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 188
Size: 232 Color: 72
Size: 172 Color: 61

Bin 25: 2 of cap free
Amount of items: 2
Items: 
Size: 1713 Color: 160
Size: 741 Color: 121

Bin 26: 2 of cap free
Amount of items: 5
Items: 
Size: 1714 Color: 161
Size: 684 Color: 118
Size: 24 Color: 6
Size: 16 Color: 4
Size: 16 Color: 2

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 170
Size: 586 Color: 110
Size: 16 Color: 3

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1951 Color: 180
Size: 503 Color: 102

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 196
Size: 282 Color: 79

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2192 Color: 199
Size: 262 Color: 76

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 149
Size: 877 Color: 130
Size: 40 Color: 10

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 156
Size: 580 Color: 109
Size: 200 Color: 64

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1764 Color: 164
Size: 689 Color: 119

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 167
Size: 324 Color: 82
Size: 292 Color: 80

Bin 35: 4 of cap free
Amount of items: 5
Items: 
Size: 1426 Color: 146
Size: 840 Color: 125
Size: 102 Color: 43
Size: 44 Color: 14
Size: 40 Color: 13

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 159
Size: 756 Color: 123

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 1884 Color: 172
Size: 568 Color: 108

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1935 Color: 179
Size: 517 Color: 106

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 2068 Color: 189
Size: 384 Color: 91

Bin 40: 6 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 151
Size: 862 Color: 128
Size: 32 Color: 8

Bin 41: 6 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 154
Size: 828 Color: 124
Size: 20 Color: 5

Bin 42: 6 of cap free
Amount of items: 2
Items: 
Size: 2006 Color: 183
Size: 444 Color: 96

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 2086 Color: 190
Size: 364 Color: 87

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 2108 Color: 191
Size: 342 Color: 85

Bin 45: 7 of cap free
Amount of items: 3
Items: 
Size: 1551 Color: 150
Size: 858 Color: 127
Size: 40 Color: 9

Bin 46: 7 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 152
Size: 850 Color: 126
Size: 32 Color: 7

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1586 Color: 153
Size: 863 Color: 129

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1842 Color: 169
Size: 607 Color: 112

Bin 49: 8 of cap free
Amount of items: 2
Items: 
Size: 1693 Color: 158
Size: 755 Color: 122

Bin 50: 8 of cap free
Amount of items: 2
Items: 
Size: 1807 Color: 166
Size: 641 Color: 117

Bin 51: 10 of cap free
Amount of items: 5
Items: 
Size: 1230 Color: 139
Size: 439 Color: 94
Size: 421 Color: 92
Size: 272 Color: 77
Size: 84 Color: 31

Bin 52: 10 of cap free
Amount of items: 3
Items: 
Size: 1430 Color: 147
Size: 976 Color: 132
Size: 40 Color: 12

Bin 53: 10 of cap free
Amount of items: 2
Items: 
Size: 1932 Color: 178
Size: 514 Color: 105

Bin 54: 12 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 145
Size: 1023 Color: 137

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1931 Color: 177
Size: 513 Color: 104

Bin 56: 13 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 163
Size: 714 Color: 120

Bin 57: 13 of cap free
Amount of items: 2
Items: 
Size: 1806 Color: 165
Size: 637 Color: 116

Bin 58: 15 of cap free
Amount of items: 2
Items: 
Size: 1853 Color: 171
Size: 588 Color: 111

Bin 59: 23 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 143
Size: 1021 Color: 135
Size: 48 Color: 15

Bin 60: 29 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 144
Size: 1022 Color: 136

Bin 61: 32 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 148
Size: 916 Color: 131
Size: 40 Color: 11

Bin 62: 47 of cap free
Amount of items: 5
Items: 
Size: 1229 Color: 138
Size: 378 Color: 89
Size: 377 Color: 88
Size: 339 Color: 84
Size: 86 Color: 32

Bin 63: 64 of cap free
Amount of items: 4
Items: 
Size: 1236 Color: 141
Size: 1018 Color: 133
Size: 74 Color: 29
Size: 64 Color: 22

Bin 64: 72 of cap free
Amount of items: 4
Items: 
Size: 1231 Color: 140
Size: 621 Color: 114
Size: 452 Color: 97
Size: 80 Color: 30

Bin 65: 86 of cap free
Amount of items: 17
Items: 
Size: 258 Color: 75
Size: 214 Color: 70
Size: 212 Color: 69
Size: 204 Color: 66
Size: 174 Color: 62
Size: 150 Color: 57
Size: 148 Color: 56
Size: 140 Color: 54
Size: 128 Color: 53
Size: 126 Color: 51
Size: 88 Color: 39
Size: 88 Color: 38
Size: 88 Color: 37
Size: 88 Color: 36
Size: 88 Color: 35
Size: 88 Color: 34
Size: 88 Color: 33

Bin 66: 1886 of cap free
Amount of items: 5
Items: 
Size: 122 Color: 50
Size: 120 Color: 48
Size: 120 Color: 47
Size: 108 Color: 45
Size: 100 Color: 42

Total size: 159640
Total free space: 2456

