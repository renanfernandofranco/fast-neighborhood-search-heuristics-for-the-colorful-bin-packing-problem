Capicity Bin: 19712
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 15
Size: 7080 Color: 2
Size: 1408 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 19
Size: 7076 Color: 18
Size: 1424 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 13164 Color: 1
Size: 6152 Color: 14
Size: 396 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 7
Size: 5459 Color: 5
Size: 1090 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 14
Size: 6224 Color: 19
Size: 168 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13860 Color: 8
Size: 5444 Color: 0
Size: 408 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 14680 Color: 17
Size: 3736 Color: 15
Size: 1296 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 15176 Color: 18
Size: 3438 Color: 15
Size: 1098 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 15489 Color: 17
Size: 3223 Color: 16
Size: 1000 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 15550 Color: 6
Size: 3470 Color: 0
Size: 692 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15590 Color: 16
Size: 2290 Color: 12
Size: 1832 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15851 Color: 14
Size: 3219 Color: 11
Size: 642 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15875 Color: 10
Size: 3199 Color: 13
Size: 638 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15915 Color: 15
Size: 3165 Color: 11
Size: 632 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 15944 Color: 15
Size: 3096 Color: 15
Size: 672 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 16030 Color: 1
Size: 3238 Color: 8
Size: 444 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 16046 Color: 15
Size: 1844 Color: 4
Size: 1822 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 16144 Color: 16
Size: 2992 Color: 4
Size: 576 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 16292 Color: 5
Size: 2004 Color: 6
Size: 1416 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16328 Color: 2
Size: 2776 Color: 13
Size: 608 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 16417 Color: 12
Size: 2747 Color: 19
Size: 548 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16466 Color: 3
Size: 3110 Color: 16
Size: 136 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16468 Color: 17
Size: 3020 Color: 10
Size: 224 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16591 Color: 4
Size: 2033 Color: 3
Size: 1088 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16548 Color: 13
Size: 1748 Color: 3
Size: 1416 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16712 Color: 4
Size: 2568 Color: 11
Size: 432 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16600 Color: 1
Size: 2448 Color: 17
Size: 664 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16648 Color: 11
Size: 1632 Color: 9
Size: 1432 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 4
Size: 2504 Color: 15
Size: 432 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16764 Color: 9
Size: 1808 Color: 1
Size: 1140 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16784 Color: 12
Size: 2288 Color: 10
Size: 640 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16826 Color: 4
Size: 1642 Color: 11
Size: 1244 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16948 Color: 7
Size: 2406 Color: 15
Size: 358 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 17067 Color: 6
Size: 2033 Color: 17
Size: 612 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 17109 Color: 18
Size: 1891 Color: 8
Size: 712 Color: 14

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 17176 Color: 11
Size: 1960 Color: 6
Size: 576 Color: 16

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 17220 Color: 15
Size: 1632 Color: 13
Size: 860 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 17246 Color: 0
Size: 2008 Color: 1
Size: 458 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 17313 Color: 3
Size: 1715 Color: 9
Size: 684 Color: 13

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 17329 Color: 15
Size: 1775 Color: 4
Size: 608 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 17368 Color: 19
Size: 1872 Color: 9
Size: 472 Color: 9

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17444 Color: 8
Size: 1850 Color: 10
Size: 418 Color: 15

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17488 Color: 14
Size: 1568 Color: 1
Size: 656 Color: 13

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17494 Color: 15
Size: 1892 Color: 4
Size: 326 Color: 16

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17552 Color: 14
Size: 1672 Color: 7
Size: 488 Color: 17

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17611 Color: 8
Size: 1645 Color: 7
Size: 456 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17616 Color: 11
Size: 1472 Color: 1
Size: 624 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17620 Color: 2
Size: 1088 Color: 15
Size: 1004 Color: 7

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17660 Color: 9
Size: 1508 Color: 7
Size: 544 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17682 Color: 7
Size: 1694 Color: 16
Size: 336 Color: 7

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 11119 Color: 0
Size: 8200 Color: 9
Size: 392 Color: 8

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 12205 Color: 1
Size: 7100 Color: 13
Size: 406 Color: 2

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 13927 Color: 1
Size: 5384 Color: 19
Size: 400 Color: 2

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 14482 Color: 1
Size: 4821 Color: 2
Size: 408 Color: 18

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 14477 Color: 12
Size: 4450 Color: 11
Size: 784 Color: 3

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 14511 Color: 19
Size: 4880 Color: 15
Size: 320 Color: 17

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 14988 Color: 8
Size: 4363 Color: 9
Size: 360 Color: 3

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 15037 Color: 18
Size: 3418 Color: 16
Size: 1256 Color: 4

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 15879 Color: 2
Size: 3422 Color: 19
Size: 410 Color: 18

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 16342 Color: 1
Size: 2921 Color: 4
Size: 448 Color: 18

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 17559 Color: 5
Size: 2000 Color: 15
Size: 152 Color: 13

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 10654 Color: 19
Size: 8208 Color: 5
Size: 848 Color: 14

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 12862 Color: 15
Size: 6720 Color: 13
Size: 128 Color: 3

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 13887 Color: 18
Size: 5503 Color: 12
Size: 320 Color: 15

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 14374 Color: 4
Size: 5336 Color: 3

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 14384 Color: 7
Size: 4442 Color: 1
Size: 884 Color: 9

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 14386 Color: 7
Size: 4884 Color: 13
Size: 440 Color: 3

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 14890 Color: 2
Size: 4200 Color: 4
Size: 620 Color: 5

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 14972 Color: 0
Size: 4362 Color: 18
Size: 376 Color: 18

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 15378 Color: 11
Size: 4332 Color: 1

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 15688 Color: 0
Size: 4022 Color: 9

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 15752 Color: 3
Size: 3478 Color: 19
Size: 480 Color: 5

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 15776 Color: 8
Size: 3070 Color: 15
Size: 864 Color: 1

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 16570 Color: 0
Size: 2600 Color: 14
Size: 540 Color: 15

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 16585 Color: 10
Size: 2781 Color: 6
Size: 344 Color: 11

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 17277 Color: 10
Size: 2433 Color: 16

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 17505 Color: 12
Size: 2205 Color: 16

Bin 78: 3 of cap free
Amount of items: 5
Items: 
Size: 9861 Color: 13
Size: 3144 Color: 3
Size: 3108 Color: 8
Size: 1880 Color: 19
Size: 1716 Color: 4

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 11121 Color: 0
Size: 8204 Color: 13
Size: 384 Color: 6

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 13072 Color: 7
Size: 6285 Color: 3
Size: 352 Color: 13

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 15988 Color: 1
Size: 2291 Color: 17
Size: 1430 Color: 16

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 16377 Color: 7
Size: 3332 Color: 8

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 16765 Color: 7
Size: 2944 Color: 11

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 17249 Color: 0
Size: 2084 Color: 15
Size: 376 Color: 0

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 17656 Color: 13
Size: 2053 Color: 3

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 17720 Color: 6
Size: 1935 Color: 19
Size: 54 Color: 2

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 17739 Color: 13
Size: 1970 Color: 1

Bin 88: 4 of cap free
Amount of items: 35
Items: 
Size: 770 Color: 5
Size: 768 Color: 6
Size: 768 Color: 4
Size: 736 Color: 8
Size: 736 Color: 6
Size: 692 Color: 3
Size: 680 Color: 15
Size: 656 Color: 9
Size: 656 Color: 2
Size: 644 Color: 1
Size: 638 Color: 10
Size: 634 Color: 7
Size: 590 Color: 12
Size: 584 Color: 16
Size: 560 Color: 12
Size: 560 Color: 1
Size: 554 Color: 17
Size: 520 Color: 11
Size: 520 Color: 5
Size: 520 Color: 5
Size: 512 Color: 17
Size: 512 Color: 15
Size: 496 Color: 19
Size: 496 Color: 15
Size: 496 Color: 6
Size: 488 Color: 1
Size: 486 Color: 13
Size: 484 Color: 18
Size: 480 Color: 19
Size: 480 Color: 15
Size: 448 Color: 2
Size: 432 Color: 7
Size: 384 Color: 2
Size: 368 Color: 3
Size: 360 Color: 10

Bin 89: 4 of cap free
Amount of items: 7
Items: 
Size: 9860 Color: 12
Size: 1776 Color: 7
Size: 1768 Color: 16
Size: 1768 Color: 14
Size: 1720 Color: 13
Size: 1632 Color: 1
Size: 1184 Color: 9

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 17702 Color: 1
Size: 1762 Color: 15
Size: 244 Color: 0

Bin 91: 5 of cap free
Amount of items: 3
Items: 
Size: 10662 Color: 19
Size: 8213 Color: 3
Size: 832 Color: 4

Bin 92: 5 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 4
Size: 7127 Color: 13
Size: 256 Color: 10

Bin 93: 5 of cap free
Amount of items: 3
Items: 
Size: 14892 Color: 4
Size: 4335 Color: 16
Size: 480 Color: 19

Bin 94: 5 of cap free
Amount of items: 3
Items: 
Size: 17042 Color: 10
Size: 2441 Color: 9
Size: 224 Color: 15

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 17106 Color: 13
Size: 2601 Color: 9

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 17222 Color: 10
Size: 2485 Color: 11

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 17265 Color: 12
Size: 2442 Color: 6

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 17304 Color: 0
Size: 2403 Color: 7

Bin 99: 6 of cap free
Amount of items: 4
Items: 
Size: 9864 Color: 12
Size: 7161 Color: 17
Size: 2065 Color: 0
Size: 616 Color: 19

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 14546 Color: 6
Size: 4808 Color: 18
Size: 352 Color: 1

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 15240 Color: 7
Size: 3058 Color: 17
Size: 1408 Color: 4

Bin 102: 7 of cap free
Amount of items: 3
Items: 
Size: 12171 Color: 8
Size: 7096 Color: 15
Size: 438 Color: 3

Bin 103: 7 of cap free
Amount of items: 3
Items: 
Size: 14551 Color: 18
Size: 5022 Color: 16
Size: 132 Color: 8

Bin 104: 7 of cap free
Amount of items: 3
Items: 
Size: 16963 Color: 8
Size: 2058 Color: 4
Size: 684 Color: 13

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 17273 Color: 8
Size: 2432 Color: 10

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 16008 Color: 14
Size: 3696 Color: 15

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 17478 Color: 0
Size: 2226 Color: 13

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 17530 Color: 9
Size: 2174 Color: 12

Bin 109: 9 of cap free
Amount of items: 9
Items: 
Size: 9857 Color: 9
Size: 1632 Color: 1
Size: 1250 Color: 16
Size: 1248 Color: 4
Size: 1224 Color: 14
Size: 1216 Color: 7
Size: 1216 Color: 0
Size: 1100 Color: 12
Size: 960 Color: 15

Bin 110: 9 of cap free
Amount of items: 3
Items: 
Size: 12990 Color: 18
Size: 6257 Color: 16
Size: 456 Color: 14

Bin 111: 9 of cap free
Amount of items: 3
Items: 
Size: 13690 Color: 15
Size: 5445 Color: 7
Size: 568 Color: 4

Bin 112: 9 of cap free
Amount of items: 2
Items: 
Size: 17081 Color: 0
Size: 2622 Color: 10

Bin 113: 10 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 18
Size: 7984 Color: 6
Size: 490 Color: 4

Bin 114: 10 of cap free
Amount of items: 2
Items: 
Size: 15845 Color: 4
Size: 3857 Color: 11

Bin 115: 10 of cap free
Amount of items: 2
Items: 
Size: 16392 Color: 2
Size: 3310 Color: 9

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 17608 Color: 11
Size: 2094 Color: 0

Bin 117: 11 of cap free
Amount of items: 3
Items: 
Size: 13682 Color: 8
Size: 5499 Color: 5
Size: 520 Color: 19

Bin 118: 11 of cap free
Amount of items: 2
Items: 
Size: 17508 Color: 13
Size: 2193 Color: 10

Bin 119: 12 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 11
Size: 6168 Color: 16
Size: 352 Color: 4

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 17240 Color: 7
Size: 2460 Color: 8

Bin 121: 13 of cap free
Amount of items: 2
Items: 
Size: 11161 Color: 6
Size: 8538 Color: 19

Bin 122: 13 of cap free
Amount of items: 2
Items: 
Size: 17092 Color: 7
Size: 2607 Color: 1

Bin 123: 13 of cap free
Amount of items: 2
Items: 
Size: 17528 Color: 13
Size: 2171 Color: 7

Bin 124: 14 of cap free
Amount of items: 2
Items: 
Size: 12328 Color: 1
Size: 7370 Color: 14

Bin 125: 15 of cap free
Amount of items: 2
Items: 
Size: 15396 Color: 19
Size: 4301 Color: 18

Bin 126: 16 of cap free
Amount of items: 2
Items: 
Size: 11056 Color: 2
Size: 8640 Color: 9

Bin 127: 16 of cap free
Amount of items: 3
Items: 
Size: 12854 Color: 4
Size: 6140 Color: 15
Size: 702 Color: 16

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 16976 Color: 16
Size: 2720 Color: 11

Bin 129: 16 of cap free
Amount of items: 2
Items: 
Size: 17655 Color: 9
Size: 2041 Color: 7

Bin 130: 17 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 7
Size: 5552 Color: 1
Size: 1795 Color: 12

Bin 131: 18 of cap free
Amount of items: 3
Items: 
Size: 15606 Color: 9
Size: 3368 Color: 7
Size: 720 Color: 19

Bin 132: 18 of cap free
Amount of items: 2
Items: 
Size: 16207 Color: 14
Size: 3487 Color: 7

Bin 133: 19 of cap free
Amount of items: 2
Items: 
Size: 15792 Color: 3
Size: 3901 Color: 2

Bin 134: 20 of cap free
Amount of items: 2
Items: 
Size: 15280 Color: 7
Size: 4412 Color: 19

Bin 135: 20 of cap free
Amount of items: 3
Items: 
Size: 16786 Color: 2
Size: 2802 Color: 8
Size: 104 Color: 7

Bin 136: 21 of cap free
Amount of items: 2
Items: 
Size: 16496 Color: 13
Size: 3195 Color: 18

Bin 137: 21 of cap free
Amount of items: 2
Items: 
Size: 17598 Color: 1
Size: 2093 Color: 4

Bin 138: 22 of cap free
Amount of items: 2
Items: 
Size: 14088 Color: 2
Size: 5602 Color: 12

Bin 139: 23 of cap free
Amount of items: 3
Items: 
Size: 13179 Color: 7
Size: 6158 Color: 3
Size: 352 Color: 13

Bin 140: 24 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 2
Size: 8216 Color: 10
Size: 768 Color: 4

Bin 141: 24 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 2
Size: 7084 Color: 18
Size: 1408 Color: 14

Bin 142: 24 of cap free
Amount of items: 2
Items: 
Size: 16167 Color: 14
Size: 3521 Color: 6

Bin 143: 26 of cap free
Amount of items: 2
Items: 
Size: 16731 Color: 8
Size: 2955 Color: 11

Bin 144: 29 of cap free
Amount of items: 2
Items: 
Size: 15899 Color: 3
Size: 3784 Color: 14

Bin 145: 30 of cap free
Amount of items: 2
Items: 
Size: 11922 Color: 1
Size: 7760 Color: 15

Bin 146: 30 of cap free
Amount of items: 3
Items: 
Size: 15529 Color: 1
Size: 3897 Color: 19
Size: 256 Color: 12

Bin 147: 31 of cap free
Amount of items: 2
Items: 
Size: 16829 Color: 15
Size: 2852 Color: 18

Bin 148: 32 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 11
Size: 8212 Color: 8
Size: 1308 Color: 4

Bin 149: 32 of cap free
Amount of items: 2
Items: 
Size: 13264 Color: 15
Size: 6416 Color: 18

Bin 150: 32 of cap free
Amount of items: 2
Items: 
Size: 14288 Color: 7
Size: 5392 Color: 9

Bin 151: 33 of cap free
Amount of items: 2
Items: 
Size: 17350 Color: 11
Size: 2329 Color: 13

Bin 152: 35 of cap free
Amount of items: 3
Items: 
Size: 17445 Color: 9
Size: 2168 Color: 17
Size: 64 Color: 10

Bin 153: 37 of cap free
Amount of items: 4
Items: 
Size: 9880 Color: 4
Size: 8211 Color: 3
Size: 1216 Color: 1
Size: 368 Color: 12

Bin 154: 37 of cap free
Amount of items: 3
Items: 
Size: 14420 Color: 1
Size: 4855 Color: 15
Size: 400 Color: 4

Bin 155: 38 of cap free
Amount of items: 2
Items: 
Size: 15086 Color: 14
Size: 4588 Color: 10

Bin 156: 38 of cap free
Amount of items: 2
Items: 
Size: 16968 Color: 10
Size: 2706 Color: 2

Bin 157: 40 of cap free
Amount of items: 21
Items: 
Size: 1120 Color: 9
Size: 1120 Color: 0
Size: 1104 Color: 11
Size: 1088 Color: 8
Size: 1072 Color: 3
Size: 1056 Color: 16
Size: 1056 Color: 4
Size: 970 Color: 12
Size: 968 Color: 18
Size: 928 Color: 8
Size: 928 Color: 0
Size: 888 Color: 5
Size: 880 Color: 12
Size: 872 Color: 6
Size: 868 Color: 16
Size: 866 Color: 11
Size: 864 Color: 8
Size: 800 Color: 3
Size: 780 Color: 15
Size: 752 Color: 17
Size: 692 Color: 1

Bin 158: 40 of cap free
Amount of items: 3
Items: 
Size: 11208 Color: 4
Size: 7216 Color: 6
Size: 1248 Color: 12

Bin 159: 40 of cap free
Amount of items: 3
Items: 
Size: 15542 Color: 4
Size: 3858 Color: 1
Size: 272 Color: 12

Bin 160: 40 of cap free
Amount of items: 3
Items: 
Size: 15614 Color: 15
Size: 3280 Color: 13
Size: 778 Color: 4

Bin 161: 40 of cap free
Amount of items: 2
Items: 
Size: 16192 Color: 9
Size: 3480 Color: 5

Bin 162: 41 of cap free
Amount of items: 3
Items: 
Size: 15031 Color: 1
Size: 4464 Color: 15
Size: 176 Color: 16

Bin 163: 41 of cap free
Amount of items: 2
Items: 
Size: 17027 Color: 2
Size: 2644 Color: 15

Bin 164: 42 of cap free
Amount of items: 2
Items: 
Size: 16966 Color: 15
Size: 2704 Color: 6

Bin 165: 48 of cap free
Amount of items: 2
Items: 
Size: 17592 Color: 14
Size: 2072 Color: 12

Bin 166: 51 of cap free
Amount of items: 2
Items: 
Size: 13115 Color: 4
Size: 6546 Color: 19

Bin 167: 53 of cap free
Amount of items: 2
Items: 
Size: 15816 Color: 5
Size: 3843 Color: 9

Bin 168: 55 of cap free
Amount of items: 2
Items: 
Size: 17201 Color: 2
Size: 2456 Color: 0

Bin 169: 56 of cap free
Amount of items: 2
Items: 
Size: 14960 Color: 12
Size: 4696 Color: 17

Bin 170: 56 of cap free
Amount of items: 2
Items: 
Size: 15716 Color: 1
Size: 3940 Color: 18

Bin 171: 58 of cap free
Amount of items: 2
Items: 
Size: 16350 Color: 2
Size: 3304 Color: 13

Bin 172: 60 of cap free
Amount of items: 2
Items: 
Size: 17464 Color: 3
Size: 2188 Color: 1

Bin 173: 64 of cap free
Amount of items: 2
Items: 
Size: 15568 Color: 2
Size: 4080 Color: 10

Bin 174: 70 of cap free
Amount of items: 2
Items: 
Size: 14616 Color: 13
Size: 5026 Color: 19

Bin 175: 76 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 11
Size: 6380 Color: 14

Bin 176: 76 of cap free
Amount of items: 2
Items: 
Size: 17328 Color: 14
Size: 2308 Color: 19

Bin 177: 80 of cap free
Amount of items: 2
Items: 
Size: 9872 Color: 13
Size: 9760 Color: 0

Bin 178: 80 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 10
Size: 4248 Color: 11
Size: 1344 Color: 13

Bin 179: 83 of cap free
Amount of items: 2
Items: 
Size: 15101 Color: 10
Size: 4528 Color: 8

Bin 180: 88 of cap free
Amount of items: 2
Items: 
Size: 15640 Color: 13
Size: 3984 Color: 18

Bin 181: 92 of cap free
Amount of items: 2
Items: 
Size: 17321 Color: 14
Size: 2299 Color: 13

Bin 182: 95 of cap free
Amount of items: 2
Items: 
Size: 16793 Color: 17
Size: 2824 Color: 11

Bin 183: 96 of cap free
Amount of items: 2
Items: 
Size: 10870 Color: 9
Size: 8746 Color: 3

Bin 184: 100 of cap free
Amount of items: 3
Items: 
Size: 10702 Color: 19
Size: 8214 Color: 18
Size: 696 Color: 12

Bin 185: 100 of cap free
Amount of items: 2
Items: 
Size: 17316 Color: 6
Size: 2296 Color: 10

Bin 186: 108 of cap free
Amount of items: 2
Items: 
Size: 15990 Color: 5
Size: 3614 Color: 7

Bin 187: 109 of cap free
Amount of items: 2
Items: 
Size: 13109 Color: 16
Size: 6494 Color: 13

Bin 188: 114 of cap free
Amount of items: 2
Items: 
Size: 12048 Color: 17
Size: 7550 Color: 11

Bin 189: 117 of cap free
Amount of items: 2
Items: 
Size: 15085 Color: 12
Size: 4510 Color: 6

Bin 190: 122 of cap free
Amount of items: 2
Items: 
Size: 13872 Color: 11
Size: 5718 Color: 12

Bin 191: 126 of cap free
Amount of items: 2
Items: 
Size: 15982 Color: 15
Size: 3604 Color: 12

Bin 192: 144 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 2
Size: 7296 Color: 16

Bin 193: 181 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 6
Size: 6223 Color: 1
Size: 964 Color: 19

Bin 194: 183 of cap free
Amount of items: 2
Items: 
Size: 12245 Color: 19
Size: 7284 Color: 13

Bin 195: 218 of cap free
Amount of items: 3
Items: 
Size: 9876 Color: 8
Size: 7161 Color: 5
Size: 2457 Color: 15

Bin 196: 308 of cap free
Amount of items: 7
Items: 
Size: 9858 Color: 5
Size: 1678 Color: 19
Size: 1640 Color: 17
Size: 1640 Color: 13
Size: 1640 Color: 6
Size: 1508 Color: 13
Size: 1440 Color: 4

Bin 197: 312 of cap free
Amount of items: 2
Items: 
Size: 11858 Color: 9
Size: 7542 Color: 5

Bin 198: 317 of cap free
Amount of items: 5
Items: 
Size: 9862 Color: 14
Size: 3400 Color: 14
Size: 3179 Color: 5
Size: 2058 Color: 9
Size: 896 Color: 1

Bin 199: 14312 of cap free
Amount of items: 15
Items: 
Size: 416 Color: 3
Size: 412 Color: 13
Size: 406 Color: 19
Size: 392 Color: 5
Size: 368 Color: 2
Size: 358 Color: 17
Size: 352 Color: 12
Size: 352 Color: 11
Size: 342 Color: 19
Size: 336 Color: 14
Size: 336 Color: 4
Size: 336 Color: 1
Size: 334 Color: 17
Size: 332 Color: 14
Size: 328 Color: 13

Total size: 3902976
Total free space: 19712

