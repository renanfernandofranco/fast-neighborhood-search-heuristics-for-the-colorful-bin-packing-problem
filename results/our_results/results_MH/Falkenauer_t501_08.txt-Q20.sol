Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 9
Size: 266 Color: 9
Size: 252 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7
Size: 354 Color: 12
Size: 271 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 16
Size: 254 Color: 17
Size: 250 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 14
Size: 329 Color: 7
Size: 287 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 301 Color: 2
Size: 261 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 10
Size: 293 Color: 17
Size: 288 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 7
Size: 279 Color: 9
Size: 276 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 4
Size: 320 Color: 9
Size: 259 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 5
Size: 346 Color: 0
Size: 278 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 8
Size: 253 Color: 15
Size: 252 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 4
Size: 312 Color: 17
Size: 253 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 17
Size: 291 Color: 1
Size: 262 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 351 Color: 13
Size: 292 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 285 Color: 14
Size: 267 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 6
Size: 266 Color: 8
Size: 255 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 19
Size: 285 Color: 14
Size: 275 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 9
Size: 340 Color: 6
Size: 276 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 0
Size: 360 Color: 5
Size: 278 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 310 Color: 5
Size: 274 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 14
Size: 294 Color: 13
Size: 267 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 4
Size: 294 Color: 14
Size: 253 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 328 Color: 1
Size: 292 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 10
Size: 251 Color: 7
Size: 250 Color: 8

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 15
Size: 282 Color: 17
Size: 259 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 15
Size: 306 Color: 16
Size: 284 Color: 5

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 14
Size: 358 Color: 19
Size: 257 Color: 18

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8
Size: 321 Color: 2
Size: 253 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 17
Size: 286 Color: 18
Size: 261 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 300 Color: 2
Size: 254 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 17
Size: 294 Color: 15
Size: 270 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 16
Size: 261 Color: 8
Size: 258 Color: 11

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 15
Size: 255 Color: 2
Size: 250 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 11
Size: 330 Color: 19
Size: 260 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 4
Size: 311 Color: 9
Size: 283 Color: 16

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 6
Size: 307 Color: 19
Size: 295 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 5
Size: 318 Color: 16
Size: 290 Color: 13

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 5
Size: 251 Color: 8
Size: 251 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 330 Color: 10
Size: 280 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 7
Size: 259 Color: 4
Size: 255 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 5
Size: 329 Color: 16
Size: 303 Color: 10

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 19
Size: 255 Color: 1
Size: 252 Color: 10

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 362 Color: 9
Size: 252 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 322 Color: 5
Size: 277 Color: 9

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 2
Size: 302 Color: 8
Size: 267 Color: 14

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 18
Size: 350 Color: 0
Size: 286 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 15
Size: 274 Color: 18
Size: 271 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 10
Size: 334 Color: 13
Size: 309 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 7
Size: 314 Color: 12
Size: 279 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 17
Size: 350 Color: 19
Size: 257 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 4
Size: 320 Color: 5
Size: 260 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 12
Size: 357 Color: 19
Size: 259 Color: 16

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 16
Size: 269 Color: 3
Size: 251 Color: 11

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 3
Size: 284 Color: 0
Size: 258 Color: 9

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 16
Size: 308 Color: 4
Size: 303 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 16
Size: 320 Color: 16
Size: 268 Color: 6

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 10
Size: 270 Color: 2
Size: 262 Color: 10

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 2
Size: 307 Color: 16
Size: 252 Color: 15

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 2
Size: 292 Color: 9
Size: 263 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 7
Size: 256 Color: 3
Size: 252 Color: 9

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 315 Color: 7
Size: 256 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 14
Size: 326 Color: 17
Size: 296 Color: 10

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 13
Size: 327 Color: 12
Size: 262 Color: 7

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 11
Size: 304 Color: 17
Size: 293 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 17
Size: 253 Color: 10
Size: 250 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 351 Color: 10
Size: 250 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 14
Size: 292 Color: 8
Size: 282 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 359 Color: 2
Size: 265 Color: 15

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 256 Color: 7
Size: 250 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 0
Size: 331 Color: 1
Size: 269 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 14
Size: 315 Color: 17
Size: 290 Color: 8

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 8
Size: 279 Color: 6
Size: 251 Color: 15

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 19
Size: 254 Color: 4
Size: 250 Color: 6

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 299 Color: 0
Size: 267 Color: 13

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 13
Size: 288 Color: 16
Size: 270 Color: 17

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 15
Size: 312 Color: 9
Size: 260 Color: 19

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 14
Size: 318 Color: 5
Size: 252 Color: 6

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 11
Size: 280 Color: 18
Size: 254 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 9
Size: 317 Color: 1
Size: 260 Color: 11

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 6
Size: 281 Color: 11
Size: 269 Color: 8

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 19
Size: 364 Color: 12
Size: 266 Color: 15

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 16
Size: 284 Color: 13
Size: 266 Color: 6

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 5
Size: 288 Color: 17
Size: 261 Color: 16

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 4
Size: 355 Color: 3
Size: 262 Color: 9

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 334 Color: 17
Size: 302 Color: 11

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 13
Size: 325 Color: 5
Size: 303 Color: 18

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 12
Size: 270 Color: 4
Size: 254 Color: 14

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 12
Size: 328 Color: 5
Size: 296 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 19
Size: 290 Color: 7
Size: 259 Color: 17

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 2
Size: 316 Color: 7
Size: 259 Color: 6

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 263 Color: 2
Size: 253 Color: 9

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 7
Size: 311 Color: 19
Size: 267 Color: 7

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 13
Size: 313 Color: 17
Size: 273 Color: 8

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 11
Size: 281 Color: 10
Size: 263 Color: 10

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 327 Color: 7
Size: 257 Color: 6

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 18
Size: 321 Color: 19
Size: 318 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 12
Size: 277 Color: 10
Size: 258 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 18
Size: 291 Color: 8
Size: 263 Color: 5

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 13
Size: 314 Color: 12
Size: 257 Color: 16

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 11
Size: 305 Color: 17
Size: 250 Color: 12

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 10
Size: 351 Color: 6
Size: 295 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 14
Size: 315 Color: 10
Size: 256 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 7
Size: 306 Color: 17
Size: 279 Color: 7

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8
Size: 317 Color: 10
Size: 251 Color: 15

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 364 Color: 3
Size: 259 Color: 17

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 17
Size: 301 Color: 7
Size: 280 Color: 18

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 19
Size: 351 Color: 2
Size: 273 Color: 11

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 306 Color: 4
Size: 282 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 6
Size: 329 Color: 2
Size: 276 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 3
Size: 279 Color: 3
Size: 267 Color: 7

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 14
Size: 293 Color: 6
Size: 261 Color: 19

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 17
Size: 259 Color: 4
Size: 250 Color: 6

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 19
Size: 304 Color: 5
Size: 301 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 10
Size: 315 Color: 15
Size: 263 Color: 11

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 11
Size: 331 Color: 16
Size: 283 Color: 19

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 14
Size: 301 Color: 19
Size: 273 Color: 15

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 8
Size: 274 Color: 18
Size: 257 Color: 5

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9
Size: 290 Color: 18
Size: 258 Color: 12

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 14
Size: 293 Color: 13
Size: 276 Color: 18

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 5
Size: 332 Color: 3
Size: 276 Color: 7

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 4
Size: 349 Color: 16
Size: 267 Color: 14

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 11
Size: 273 Color: 12
Size: 250 Color: 19

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 259 Color: 3
Size: 250 Color: 14

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 303 Color: 15
Size: 266 Color: 15

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 17
Size: 322 Color: 7
Size: 266 Color: 14

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 5
Size: 340 Color: 17
Size: 283 Color: 9

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 3
Size: 292 Color: 15
Size: 291 Color: 19

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 9
Size: 328 Color: 15
Size: 320 Color: 12

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 15
Size: 281 Color: 10
Size: 263 Color: 17

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 10
Size: 358 Color: 1
Size: 284 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 19
Size: 351 Color: 17
Size: 276 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 9
Size: 291 Color: 4
Size: 284 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 269 Color: 16
Size: 254 Color: 4

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 7
Size: 301 Color: 0
Size: 278 Color: 13

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 258 Color: 18
Size: 251 Color: 11

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 8
Size: 293 Color: 8
Size: 254 Color: 3

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 14
Size: 323 Color: 5
Size: 251 Color: 9

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 6
Size: 362 Color: 5
Size: 255 Color: 9

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 19
Size: 315 Color: 16
Size: 261 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 10
Size: 315 Color: 14
Size: 274 Color: 16

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 17
Size: 354 Color: 15
Size: 281 Color: 3

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 14
Size: 332 Color: 13
Size: 286 Color: 17

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 17
Size: 363 Color: 9
Size: 267 Color: 18

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 5
Size: 325 Color: 11
Size: 253 Color: 14

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 5
Size: 330 Color: 7
Size: 274 Color: 12

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 9
Size: 275 Color: 14
Size: 268 Color: 5

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 12
Size: 309 Color: 15
Size: 297 Color: 9

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 285 Color: 12
Size: 270 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 16
Size: 264 Color: 8
Size: 263 Color: 9

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 16
Size: 278 Color: 7
Size: 272 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 7
Size: 313 Color: 10
Size: 282 Color: 8

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 292 Color: 9
Size: 271 Color: 15

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 277 Color: 14
Size: 265 Color: 14

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 13
Size: 262 Color: 3
Size: 250 Color: 11

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 6
Size: 354 Color: 1
Size: 260 Color: 15

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 10
Size: 356 Color: 19
Size: 252 Color: 19

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 16
Size: 311 Color: 0
Size: 272 Color: 16

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 304 Color: 13
Size: 270 Color: 19

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 8
Size: 363 Color: 12
Size: 266 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6
Size: 362 Color: 15
Size: 275 Color: 6

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 11
Size: 273 Color: 4
Size: 254 Color: 11

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 16
Size: 323 Color: 13
Size: 292 Color: 3

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 13
Size: 353 Color: 17
Size: 290 Color: 4

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 2
Size: 318 Color: 4
Size: 253 Color: 18

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 17
Size: 273 Color: 3
Size: 251 Color: 3

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 14
Size: 297 Color: 2
Size: 262 Color: 4

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 18
Size: 318 Color: 0
Size: 297 Color: 8

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 11
Size: 257 Color: 14
Size: 252 Color: 16

Total size: 167000
Total free space: 0

