Capicity Bin: 7512
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 21
Items: 
Size: 468 Color: 1
Size: 464 Color: 1
Size: 464 Color: 0
Size: 424 Color: 0
Size: 424 Color: 0
Size: 416 Color: 0
Size: 408 Color: 1
Size: 404 Color: 0
Size: 400 Color: 1
Size: 392 Color: 0
Size: 356 Color: 0
Size: 356 Color: 0
Size: 352 Color: 0
Size: 344 Color: 1
Size: 328 Color: 1
Size: 316 Color: 1
Size: 314 Color: 1
Size: 304 Color: 1
Size: 280 Color: 1
Size: 154 Color: 1
Size: 144 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4246 Color: 1
Size: 3126 Color: 1
Size: 140 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4298 Color: 1
Size: 3052 Color: 1
Size: 162 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4412 Color: 1
Size: 2916 Color: 0
Size: 184 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 1
Size: 2674 Color: 1
Size: 188 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5082 Color: 1
Size: 2026 Color: 1
Size: 404 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 0
Size: 2148 Color: 1
Size: 128 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5410 Color: 1
Size: 1566 Color: 1
Size: 536 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5810 Color: 1
Size: 1530 Color: 0
Size: 172 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6000 Color: 0
Size: 811 Color: 1
Size: 701 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6011 Color: 0
Size: 859 Color: 0
Size: 642 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6098 Color: 1
Size: 1182 Color: 0
Size: 232 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6194 Color: 1
Size: 780 Color: 1
Size: 538 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 1
Size: 932 Color: 1
Size: 296 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 1
Size: 684 Color: 0
Size: 466 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6391 Color: 0
Size: 785 Color: 0
Size: 336 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6394 Color: 0
Size: 980 Color: 1
Size: 138 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 0
Size: 954 Color: 1
Size: 66 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6498 Color: 0
Size: 538 Color: 0
Size: 476 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6478 Color: 1
Size: 778 Color: 0
Size: 256 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 1
Size: 512 Color: 0
Size: 412 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 1
Size: 651 Color: 0
Size: 232 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 1
Size: 532 Color: 1
Size: 312 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6682 Color: 1
Size: 608 Color: 0
Size: 222 Color: 1

Bin 25: 1 of cap free
Amount of items: 7
Items: 
Size: 3759 Color: 1
Size: 929 Color: 0
Size: 839 Color: 0
Size: 722 Color: 1
Size: 694 Color: 1
Size: 356 Color: 1
Size: 212 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 1
Size: 3131 Color: 1
Size: 130 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 4289 Color: 1
Size: 2682 Color: 0
Size: 540 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 4696 Color: 1
Size: 2687 Color: 1
Size: 128 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 5580 Color: 1
Size: 1563 Color: 1
Size: 368 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 5621 Color: 0
Size: 1762 Color: 1
Size: 128 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6019 Color: 0
Size: 1372 Color: 1
Size: 120 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 6219 Color: 1
Size: 1136 Color: 1
Size: 156 Color: 0

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 6348 Color: 0
Size: 1163 Color: 1

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 6490 Color: 0
Size: 1021 Color: 1

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 6539 Color: 1
Size: 972 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6731 Color: 1
Size: 644 Color: 1
Size: 136 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6742 Color: 1
Size: 737 Color: 0
Size: 32 Color: 0

Bin 38: 2 of cap free
Amount of items: 4
Items: 
Size: 3852 Color: 1
Size: 2520 Color: 1
Size: 934 Color: 0
Size: 204 Color: 0

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 4281 Color: 1
Size: 2492 Color: 0
Size: 737 Color: 1

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 5077 Color: 1
Size: 2329 Color: 0
Size: 104 Color: 1

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 5060 Color: 0
Size: 2342 Color: 0
Size: 108 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 1
Size: 2031 Color: 1
Size: 112 Color: 0

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 5418 Color: 0
Size: 2092 Color: 1

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 5849 Color: 1
Size: 1661 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 5910 Color: 1
Size: 1194 Color: 1
Size: 406 Color: 0

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 5964 Color: 1
Size: 1338 Color: 0
Size: 208 Color: 1

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 6574 Color: 1
Size: 936 Color: 0

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 6596 Color: 0
Size: 914 Color: 1

Bin 49: 3 of cap free
Amount of items: 9
Items: 
Size: 3757 Color: 1
Size: 592 Color: 0
Size: 544 Color: 0
Size: 532 Color: 0
Size: 504 Color: 1
Size: 472 Color: 1
Size: 464 Color: 0
Size: 368 Color: 1
Size: 276 Color: 0

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 0
Size: 2693 Color: 1
Size: 156 Color: 0

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 6563 Color: 0
Size: 946 Color: 1

Bin 52: 3 of cap free
Amount of items: 2
Items: 
Size: 6657 Color: 0
Size: 852 Color: 1

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 4020 Color: 0
Size: 3296 Color: 1
Size: 192 Color: 1

Bin 54: 5 of cap free
Amount of items: 2
Items: 
Size: 6452 Color: 0
Size: 1055 Color: 1

Bin 55: 5 of cap free
Amount of items: 2
Items: 
Size: 6483 Color: 1
Size: 1024 Color: 0

Bin 56: 6 of cap free
Amount of items: 2
Items: 
Size: 5484 Color: 1
Size: 2022 Color: 0

Bin 57: 6 of cap free
Amount of items: 3
Items: 
Size: 5612 Color: 0
Size: 1746 Color: 1
Size: 148 Color: 0

Bin 58: 6 of cap free
Amount of items: 2
Items: 
Size: 5894 Color: 0
Size: 1612 Color: 1

Bin 59: 6 of cap free
Amount of items: 2
Items: 
Size: 6084 Color: 1
Size: 1422 Color: 0

Bin 60: 6 of cap free
Amount of items: 2
Items: 
Size: 6571 Color: 1
Size: 935 Color: 0

Bin 61: 6 of cap free
Amount of items: 2
Items: 
Size: 6675 Color: 0
Size: 831 Color: 1

Bin 62: 7 of cap free
Amount of items: 2
Items: 
Size: 6073 Color: 0
Size: 1432 Color: 1

Bin 63: 7 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 1
Size: 789 Color: 0
Size: 16 Color: 0

Bin 64: 8 of cap free
Amount of items: 3
Items: 
Size: 4306 Color: 0
Size: 2666 Color: 0
Size: 532 Color: 1

Bin 65: 8 of cap free
Amount of items: 2
Items: 
Size: 5812 Color: 1
Size: 1692 Color: 0

Bin 66: 8 of cap free
Amount of items: 2
Items: 
Size: 6117 Color: 0
Size: 1387 Color: 1

Bin 67: 8 of cap free
Amount of items: 3
Items: 
Size: 6650 Color: 0
Size: 826 Color: 1
Size: 28 Color: 0

Bin 68: 8 of cap free
Amount of items: 2
Items: 
Size: 6658 Color: 1
Size: 846 Color: 0

Bin 69: 8 of cap free
Amount of items: 2
Items: 
Size: 6740 Color: 0
Size: 764 Color: 1

Bin 70: 8 of cap free
Amount of items: 2
Items: 
Size: 6759 Color: 0
Size: 745 Color: 1

Bin 71: 9 of cap free
Amount of items: 2
Items: 
Size: 4719 Color: 1
Size: 2784 Color: 0

Bin 72: 9 of cap free
Amount of items: 2
Items: 
Size: 6619 Color: 1
Size: 884 Color: 0

Bin 73: 10 of cap free
Amount of items: 2
Items: 
Size: 6082 Color: 0
Size: 1420 Color: 1

Bin 74: 11 of cap free
Amount of items: 3
Items: 
Size: 4156 Color: 0
Size: 3129 Color: 1
Size: 216 Color: 0

Bin 75: 11 of cap free
Amount of items: 3
Items: 
Size: 5615 Color: 0
Size: 1754 Color: 1
Size: 132 Color: 1

Bin 76: 11 of cap free
Amount of items: 3
Items: 
Size: 6203 Color: 0
Size: 1242 Color: 1
Size: 56 Color: 0

Bin 77: 11 of cap free
Amount of items: 2
Items: 
Size: 6358 Color: 0
Size: 1143 Color: 1

Bin 78: 11 of cap free
Amount of items: 2
Items: 
Size: 6710 Color: 0
Size: 791 Color: 1

Bin 79: 12 of cap free
Amount of items: 7
Items: 
Size: 3758 Color: 1
Size: 670 Color: 0
Size: 624 Color: 1
Size: 624 Color: 1
Size: 624 Color: 0
Size: 624 Color: 0
Size: 576 Color: 1

Bin 80: 12 of cap free
Amount of items: 3
Items: 
Size: 5034 Color: 0
Size: 2346 Color: 0
Size: 120 Color: 1

Bin 81: 14 of cap free
Amount of items: 2
Items: 
Size: 5756 Color: 1
Size: 1742 Color: 0

Bin 82: 15 of cap free
Amount of items: 5
Items: 
Size: 3762 Color: 0
Size: 1350 Color: 0
Size: 871 Color: 0
Size: 815 Color: 1
Size: 699 Color: 1

Bin 83: 15 of cap free
Amount of items: 2
Items: 
Size: 4796 Color: 0
Size: 2701 Color: 1

Bin 84: 15 of cap free
Amount of items: 2
Items: 
Size: 5637 Color: 0
Size: 1860 Color: 1

Bin 85: 15 of cap free
Amount of items: 2
Items: 
Size: 6469 Color: 0
Size: 1028 Color: 1

Bin 86: 15 of cap free
Amount of items: 2
Items: 
Size: 6507 Color: 0
Size: 990 Color: 1

Bin 87: 15 of cap free
Amount of items: 2
Items: 
Size: 6535 Color: 0
Size: 962 Color: 1

Bin 88: 16 of cap free
Amount of items: 2
Items: 
Size: 6204 Color: 1
Size: 1292 Color: 0

Bin 89: 17 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 0
Size: 1347 Color: 1

Bin 90: 18 of cap free
Amount of items: 2
Items: 
Size: 6026 Color: 1
Size: 1468 Color: 0

Bin 91: 20 of cap free
Amount of items: 2
Items: 
Size: 6247 Color: 1
Size: 1245 Color: 0

Bin 92: 21 of cap free
Amount of items: 2
Items: 
Size: 6399 Color: 0
Size: 1092 Color: 1

Bin 93: 23 of cap free
Amount of items: 2
Items: 
Size: 6287 Color: 0
Size: 1202 Color: 1

Bin 94: 25 of cap free
Amount of items: 3
Items: 
Size: 6230 Color: 1
Size: 1201 Color: 0
Size: 56 Color: 0

Bin 95: 26 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 1
Size: 2386 Color: 0
Size: 96 Color: 1

Bin 96: 26 of cap free
Amount of items: 3
Items: 
Size: 5354 Color: 1
Size: 1668 Color: 0
Size: 464 Color: 0

Bin 97: 27 of cap free
Amount of items: 3
Items: 
Size: 4273 Color: 1
Size: 2588 Color: 0
Size: 624 Color: 0

Bin 98: 27 of cap free
Amount of items: 2
Items: 
Size: 6703 Color: 0
Size: 782 Color: 1

Bin 99: 28 of cap free
Amount of items: 2
Items: 
Size: 3764 Color: 0
Size: 3720 Color: 1

Bin 100: 30 of cap free
Amount of items: 31
Items: 
Size: 348 Color: 0
Size: 348 Color: 0
Size: 348 Color: 0
Size: 320 Color: 0
Size: 312 Color: 0
Size: 288 Color: 0
Size: 280 Color: 1
Size: 280 Color: 0
Size: 278 Color: 1
Size: 272 Color: 1
Size: 268 Color: 1
Size: 264 Color: 0
Size: 248 Color: 1
Size: 244 Color: 0
Size: 240 Color: 0
Size: 238 Color: 1
Size: 236 Color: 1
Size: 236 Color: 0
Size: 224 Color: 1
Size: 224 Color: 1
Size: 216 Color: 0
Size: 210 Color: 0
Size: 206 Color: 1
Size: 200 Color: 0
Size: 192 Color: 1
Size: 192 Color: 1
Size: 186 Color: 1
Size: 184 Color: 1
Size: 184 Color: 1
Size: 120 Color: 0
Size: 96 Color: 1

Bin 101: 32 of cap free
Amount of items: 2
Items: 
Size: 5516 Color: 0
Size: 1964 Color: 1

Bin 102: 32 of cap free
Amount of items: 2
Items: 
Size: 6340 Color: 0
Size: 1140 Color: 1

Bin 103: 32 of cap free
Amount of items: 2
Items: 
Size: 6378 Color: 1
Size: 1102 Color: 0

Bin 104: 33 of cap free
Amount of items: 3
Items: 
Size: 6357 Color: 1
Size: 1066 Color: 0
Size: 56 Color: 0

Bin 105: 33 of cap free
Amount of items: 2
Items: 
Size: 6582 Color: 0
Size: 897 Color: 1

Bin 106: 34 of cap free
Amount of items: 2
Items: 
Size: 6515 Color: 1
Size: 963 Color: 0

Bin 107: 40 of cap free
Amount of items: 2
Items: 
Size: 4484 Color: 1
Size: 2988 Color: 0

Bin 108: 42 of cap free
Amount of items: 2
Items: 
Size: 5090 Color: 0
Size: 2380 Color: 1

Bin 109: 42 of cap free
Amount of items: 2
Items: 
Size: 5860 Color: 0
Size: 1610 Color: 1

Bin 110: 44 of cap free
Amount of items: 2
Items: 
Size: 5402 Color: 1
Size: 2066 Color: 0

Bin 111: 45 of cap free
Amount of items: 2
Items: 
Size: 5678 Color: 0
Size: 1789 Color: 1

Bin 112: 45 of cap free
Amount of items: 2
Items: 
Size: 6074 Color: 1
Size: 1393 Color: 0

Bin 113: 46 of cap free
Amount of items: 2
Items: 
Size: 5846 Color: 1
Size: 1620 Color: 0

Bin 114: 46 of cap free
Amount of items: 2
Items: 
Size: 6396 Color: 0
Size: 1070 Color: 1

Bin 115: 47 of cap free
Amount of items: 2
Items: 
Size: 6503 Color: 1
Size: 962 Color: 0

Bin 116: 48 of cap free
Amount of items: 2
Items: 
Size: 4940 Color: 0
Size: 2524 Color: 1

Bin 117: 48 of cap free
Amount of items: 2
Items: 
Size: 5284 Color: 1
Size: 2180 Color: 0

Bin 118: 49 of cap free
Amount of items: 2
Items: 
Size: 5426 Color: 0
Size: 2037 Color: 1

Bin 119: 54 of cap free
Amount of items: 2
Items: 
Size: 6238 Color: 1
Size: 1220 Color: 0

Bin 120: 55 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 1
Size: 1577 Color: 0
Size: 724 Color: 0

Bin 121: 55 of cap free
Amount of items: 2
Items: 
Size: 6370 Color: 1
Size: 1087 Color: 0

Bin 122: 60 of cap free
Amount of items: 2
Items: 
Size: 4322 Color: 1
Size: 3130 Color: 0

Bin 123: 74 of cap free
Amount of items: 2
Items: 
Size: 4314 Color: 1
Size: 3124 Color: 0

Bin 124: 75 of cap free
Amount of items: 2
Items: 
Size: 4711 Color: 0
Size: 2726 Color: 1

Bin 125: 76 of cap free
Amount of items: 2
Items: 
Size: 5634 Color: 1
Size: 1802 Color: 0

Bin 126: 80 of cap free
Amount of items: 2
Items: 
Size: 6052 Color: 1
Size: 1380 Color: 0

Bin 127: 84 of cap free
Amount of items: 2
Items: 
Size: 4706 Color: 1
Size: 2722 Color: 0

Bin 128: 90 of cap free
Amount of items: 2
Items: 
Size: 5841 Color: 0
Size: 1581 Color: 1

Bin 129: 92 of cap free
Amount of items: 2
Items: 
Size: 4698 Color: 0
Size: 2722 Color: 1

Bin 130: 95 of cap free
Amount of items: 2
Items: 
Size: 5373 Color: 1
Size: 2044 Color: 0

Bin 131: 100 of cap free
Amount of items: 2
Items: 
Size: 5629 Color: 0
Size: 1783 Color: 1

Bin 132: 108 of cap free
Amount of items: 2
Items: 
Size: 5069 Color: 0
Size: 2335 Color: 1

Bin 133: 4994 of cap free
Amount of items: 15
Items: 
Size: 192 Color: 0
Size: 188 Color: 0
Size: 188 Color: 0
Size: 176 Color: 1
Size: 170 Color: 0
Size: 168 Color: 1
Size: 168 Color: 1
Size: 166 Color: 1
Size: 166 Color: 0
Size: 166 Color: 0
Size: 162 Color: 0
Size: 158 Color: 1
Size: 152 Color: 1
Size: 152 Color: 1
Size: 146 Color: 1

Total size: 991584
Total free space: 7512

