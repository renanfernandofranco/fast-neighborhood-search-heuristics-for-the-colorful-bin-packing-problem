Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 13
Size: 330 Color: 14
Size: 287 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 7
Size: 278 Color: 18
Size: 260 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 19
Size: 293 Color: 18
Size: 264 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 10
Size: 291 Color: 1
Size: 280 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 6
Size: 251 Color: 6
Size: 250 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 18
Size: 329 Color: 5
Size: 250 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 13
Size: 298 Color: 11
Size: 253 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 4
Size: 263 Color: 6
Size: 256 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 6
Size: 309 Color: 10
Size: 286 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 17
Size: 253 Color: 15
Size: 250 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 321 Color: 9
Size: 294 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 10
Size: 311 Color: 14
Size: 268 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 8
Size: 314 Color: 14
Size: 312 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 15
Size: 270 Color: 0
Size: 251 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 16
Size: 277 Color: 2
Size: 253 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8
Size: 316 Color: 6
Size: 251 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8
Size: 286 Color: 14
Size: 278 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 0
Size: 260 Color: 13
Size: 250 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9
Size: 264 Color: 8
Size: 263 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 13
Size: 277 Color: 10
Size: 270 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 5
Size: 256 Color: 8
Size: 255 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 5
Size: 308 Color: 3
Size: 264 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 18
Size: 299 Color: 6
Size: 250 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 18
Size: 338 Color: 5
Size: 261 Color: 17

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 16
Size: 336 Color: 3
Size: 322 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 17
Size: 275 Color: 15
Size: 255 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 372 Color: 13
Size: 254 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 19
Size: 334 Color: 19
Size: 302 Color: 10

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 15
Size: 336 Color: 4
Size: 301 Color: 5

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 18
Size: 267 Color: 4
Size: 250 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 14
Size: 269 Color: 7
Size: 266 Color: 10

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 5
Size: 273 Color: 9
Size: 260 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 17
Size: 276 Color: 19
Size: 256 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 17
Size: 321 Color: 7
Size: 287 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 4
Size: 301 Color: 2
Size: 252 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 18
Size: 323 Color: 6
Size: 252 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7
Size: 352 Color: 5
Size: 254 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 18
Size: 319 Color: 12
Size: 257 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 7
Size: 332 Color: 6
Size: 253 Color: 15

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 11
Size: 320 Color: 5
Size: 259 Color: 15

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 258 Color: 19
Size: 491 Color: 18
Size: 251 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 3
Size: 357 Color: 17
Size: 282 Color: 7

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 12
Size: 309 Color: 9
Size: 275 Color: 9

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7
Size: 347 Color: 2
Size: 272 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 13
Size: 356 Color: 6
Size: 253 Color: 12

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 5
Size: 299 Color: 2
Size: 280 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 2
Size: 361 Color: 18
Size: 271 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 371 Color: 5
Size: 250 Color: 14

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 357 Color: 2
Size: 264 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 16
Size: 253 Color: 10
Size: 252 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 10
Size: 319 Color: 0
Size: 294 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 3
Size: 279 Color: 15
Size: 268 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 19
Size: 267 Color: 12
Size: 258 Color: 13

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 19
Size: 262 Color: 19
Size: 254 Color: 8

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 14
Size: 279 Color: 7
Size: 268 Color: 14

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 19
Size: 330 Color: 19
Size: 298 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 13
Size: 329 Color: 19
Size: 291 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 3
Size: 288 Color: 6
Size: 256 Color: 10

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 10
Size: 281 Color: 9
Size: 266 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 10
Size: 264 Color: 17
Size: 256 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 4
Size: 255 Color: 3
Size: 250 Color: 8

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 17
Size: 365 Color: 19
Size: 270 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 17
Size: 301 Color: 17
Size: 294 Color: 19

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9
Size: 276 Color: 11
Size: 268 Color: 6

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 285 Color: 17
Size: 259 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 17
Size: 254 Color: 18
Size: 250 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 12
Size: 296 Color: 0
Size: 281 Color: 18

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 17
Size: 361 Color: 15
Size: 277 Color: 17

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 2
Size: 364 Color: 4
Size: 263 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 19
Size: 325 Color: 4
Size: 310 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 16
Size: 334 Color: 13
Size: 306 Color: 8

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 254 Color: 9
Size: 253 Color: 5

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 14
Size: 272 Color: 7
Size: 270 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 7
Size: 333 Color: 19
Size: 264 Color: 13

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 10
Size: 286 Color: 16
Size: 273 Color: 19

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 5
Size: 335 Color: 7
Size: 286 Color: 6

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 331 Color: 12
Size: 290 Color: 18

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 12
Size: 274 Color: 18
Size: 272 Color: 9

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 10
Size: 362 Color: 0
Size: 275 Color: 6

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 10
Size: 337 Color: 9
Size: 298 Color: 8

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 3
Size: 266 Color: 5
Size: 259 Color: 8

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 8
Size: 365 Color: 16
Size: 253 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 9
Size: 305 Color: 13
Size: 293 Color: 4

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 4
Size: 326 Color: 19
Size: 292 Color: 18

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 19
Size: 337 Color: 0
Size: 259 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 10
Size: 300 Color: 1
Size: 263 Color: 6

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 13
Size: 281 Color: 0
Size: 273 Color: 4

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7
Size: 312 Color: 10
Size: 293 Color: 16

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 15
Size: 263 Color: 10
Size: 262 Color: 5

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 348 Color: 5
Size: 254 Color: 10

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 19
Size: 294 Color: 6
Size: 285 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 7
Size: 358 Color: 1
Size: 275 Color: 10

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 326 Color: 4
Size: 271 Color: 6

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 3
Size: 359 Color: 5
Size: 280 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 11
Size: 286 Color: 16
Size: 252 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 321 Color: 5
Size: 250 Color: 11

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 18
Size: 354 Color: 17
Size: 253 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 18
Size: 283 Color: 17
Size: 254 Color: 17

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 14
Size: 270 Color: 5
Size: 251 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 324 Color: 4
Size: 309 Color: 12

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 319 Color: 1
Size: 312 Color: 13

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 4
Size: 268 Color: 14
Size: 254 Color: 13

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 284 Color: 12
Size: 254 Color: 17

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 12
Size: 261 Color: 16
Size: 261 Color: 11

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 351 Color: 8
Size: 267 Color: 9

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 4
Size: 268 Color: 11
Size: 255 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 313 Color: 17
Size: 291 Color: 5

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 277 Color: 19
Size: 263 Color: 17

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 304 Color: 10
Size: 276 Color: 9

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 16
Size: 345 Color: 19
Size: 272 Color: 14

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 7
Size: 340 Color: 17
Size: 296 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 10
Size: 352 Color: 10
Size: 283 Color: 12

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 15
Size: 286 Color: 1
Size: 253 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 5
Size: 322 Color: 19
Size: 257 Color: 11

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 13
Size: 289 Color: 19
Size: 269 Color: 13

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 18
Size: 296 Color: 8
Size: 291 Color: 7

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 264 Color: 13
Size: 253 Color: 6

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 296 Color: 6
Size: 264 Color: 4

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 4
Size: 322 Color: 19
Size: 280 Color: 15

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 261 Color: 14
Size: 252 Color: 5

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 323 Color: 6
Size: 257 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 13
Size: 340 Color: 8
Size: 301 Color: 9

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 307 Color: 14
Size: 258 Color: 11

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 6
Size: 252 Color: 11
Size: 252 Color: 7

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 9
Size: 309 Color: 5
Size: 290 Color: 11

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 12
Size: 337 Color: 13
Size: 276 Color: 15

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 18
Size: 305 Color: 1
Size: 283 Color: 6

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 327 Color: 12
Size: 258 Color: 16

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 13
Size: 296 Color: 12
Size: 257 Color: 14

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 10
Size: 321 Color: 8
Size: 253 Color: 17

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 7
Size: 305 Color: 19
Size: 253 Color: 15

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 16
Size: 280 Color: 10
Size: 266 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 16
Size: 253 Color: 6
Size: 250 Color: 16

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 18
Size: 309 Color: 9
Size: 278 Color: 9

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 15
Size: 289 Color: 19
Size: 252 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 5
Size: 330 Color: 6
Size: 297 Color: 13

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9
Size: 262 Color: 11
Size: 253 Color: 8

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9
Size: 270 Color: 3
Size: 250 Color: 9

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 368 Color: 16
Size: 253 Color: 4

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 8
Size: 358 Color: 2
Size: 270 Color: 6

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 19
Size: 296 Color: 17
Size: 254 Color: 12

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 4
Size: 343 Color: 4
Size: 257 Color: 17

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 12
Size: 356 Color: 14
Size: 256 Color: 9

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 323 Color: 14
Size: 263 Color: 17

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 18
Size: 352 Color: 8
Size: 265 Color: 7

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 347 Color: 9
Size: 270 Color: 11

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 12
Size: 251 Color: 14
Size: 251 Color: 13

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 19
Size: 361 Color: 11
Size: 264 Color: 2

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 0
Size: 278 Color: 5
Size: 261 Color: 18

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 5
Size: 253 Color: 3
Size: 250 Color: 9

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 2
Size: 361 Color: 14
Size: 275 Color: 8

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 18
Size: 265 Color: 17
Size: 264 Color: 5

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 13
Size: 339 Color: 12
Size: 254 Color: 10

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 7
Size: 352 Color: 6
Size: 267 Color: 15

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 10
Size: 302 Color: 6
Size: 301 Color: 13

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 5
Size: 359 Color: 4
Size: 274 Color: 8

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 12
Size: 361 Color: 10
Size: 254 Color: 18

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 5
Size: 256 Color: 8
Size: 253 Color: 6

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 15
Size: 290 Color: 15
Size: 279 Color: 7

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 14
Size: 259 Color: 0
Size: 255 Color: 2

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 2
Size: 262 Color: 10
Size: 252 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 316 Color: 12
Size: 309 Color: 9

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 350 Color: 12
Size: 255 Color: 7

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 2
Size: 275 Color: 19
Size: 275 Color: 5

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 293 Color: 1
Size: 265 Color: 7

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 19
Size: 355 Color: 13
Size: 290 Color: 13

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 15
Size: 263 Color: 11
Size: 256 Color: 7

Total size: 167000
Total free space: 0

