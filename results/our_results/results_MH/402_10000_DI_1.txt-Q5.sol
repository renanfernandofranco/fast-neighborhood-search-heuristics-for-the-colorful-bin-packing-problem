Capicity Bin: 8184
Lower Bound: 132

Bins used: 132
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3032 Color: 3
Size: 2792 Color: 1
Size: 1120 Color: 0
Size: 504 Color: 2
Size: 384 Color: 0
Size: 216 Color: 4
Size: 136 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 6544 Color: 0
Size: 1016 Color: 2
Size: 456 Color: 1
Size: 112 Color: 2
Size: 56 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6994 Color: 4
Size: 994 Color: 3
Size: 196 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 1
Size: 964 Color: 0
Size: 192 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 1
Size: 830 Color: 4
Size: 80 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6706 Color: 3
Size: 1234 Color: 0
Size: 244 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5449 Color: 1
Size: 2281 Color: 4
Size: 454 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7212 Color: 0
Size: 812 Color: 3
Size: 160 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6818 Color: 4
Size: 1142 Color: 4
Size: 224 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5766 Color: 1
Size: 2018 Color: 3
Size: 400 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7026 Color: 3
Size: 966 Color: 0
Size: 192 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6134 Color: 4
Size: 1710 Color: 1
Size: 340 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 2
Size: 2324 Color: 2
Size: 464 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 3
Size: 2660 Color: 4
Size: 528 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 2
Size: 1620 Color: 0
Size: 320 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4100 Color: 4
Size: 3404 Color: 2
Size: 680 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 1
Size: 1070 Color: 1
Size: 212 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5516 Color: 2
Size: 2228 Color: 0
Size: 440 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6427 Color: 1
Size: 1465 Color: 2
Size: 292 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6589 Color: 0
Size: 1331 Color: 1
Size: 264 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 2
Size: 1018 Color: 0
Size: 196 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5079 Color: 2
Size: 2589 Color: 0
Size: 516 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7294 Color: 1
Size: 838 Color: 0
Size: 52 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 2
Size: 1150 Color: 4
Size: 212 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5482 Color: 4
Size: 2254 Color: 2
Size: 448 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6420 Color: 2
Size: 1476 Color: 4
Size: 288 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4056 Color: 4
Size: 3592 Color: 2
Size: 536 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 2
Size: 782 Color: 3
Size: 152 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 3
Size: 862 Color: 3
Size: 172 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4098 Color: 1
Size: 3406 Color: 4
Size: 680 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6871 Color: 0
Size: 1095 Color: 0
Size: 218 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4093 Color: 2
Size: 3411 Color: 0
Size: 680 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5430 Color: 0
Size: 2298 Color: 0
Size: 456 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4986 Color: 3
Size: 2666 Color: 1
Size: 532 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4842 Color: 3
Size: 2786 Color: 4
Size: 556 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6076 Color: 0
Size: 2012 Color: 0
Size: 96 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 3
Size: 1388 Color: 1
Size: 272 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4738 Color: 2
Size: 2874 Color: 1
Size: 572 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6494 Color: 1
Size: 1410 Color: 4
Size: 280 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6632 Color: 2
Size: 1296 Color: 0
Size: 256 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4631 Color: 4
Size: 2961 Color: 0
Size: 592 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6530 Color: 0
Size: 1554 Color: 4
Size: 100 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 2
Size: 1588 Color: 3
Size: 312 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5773 Color: 1
Size: 2011 Color: 1
Size: 400 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 0
Size: 2092 Color: 1
Size: 416 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6085 Color: 0
Size: 1751 Color: 4
Size: 348 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5828 Color: 3
Size: 1964 Color: 4
Size: 392 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 7047 Color: 3
Size: 949 Color: 1
Size: 188 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6150 Color: 2
Size: 1698 Color: 2
Size: 336 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5810 Color: 4
Size: 1982 Color: 3
Size: 392 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6115 Color: 2
Size: 1837 Color: 1
Size: 232 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 1
Size: 842 Color: 1
Size: 168 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 7091 Color: 2
Size: 911 Color: 4
Size: 182 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6577 Color: 4
Size: 1341 Color: 1
Size: 266 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 4
Size: 1147 Color: 0
Size: 228 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 4
Size: 2724 Color: 3
Size: 544 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4634 Color: 2
Size: 2962 Color: 4
Size: 588 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 1
Size: 1099 Color: 3
Size: 218 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5884 Color: 3
Size: 1924 Color: 0
Size: 376 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6346 Color: 3
Size: 1534 Color: 1
Size: 304 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6203 Color: 1
Size: 1651 Color: 2
Size: 330 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4095 Color: 1
Size: 3409 Color: 1
Size: 680 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 3
Size: 2460 Color: 1
Size: 488 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 0
Size: 1402 Color: 3
Size: 276 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 1
Size: 1876 Color: 3
Size: 368 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4196 Color: 2
Size: 3324 Color: 2
Size: 664 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 1
Size: 844 Color: 4
Size: 168 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 4380 Color: 0
Size: 3172 Color: 1
Size: 632 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4629 Color: 4
Size: 2963 Color: 3
Size: 592 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 4
Size: 826 Color: 4
Size: 164 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 7132 Color: 0
Size: 884 Color: 1
Size: 168 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 2
Size: 780 Color: 1
Size: 152 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6581 Color: 0
Size: 1337 Color: 1
Size: 266 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7332 Color: 0
Size: 716 Color: 1
Size: 136 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 3
Size: 1308 Color: 1
Size: 256 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5772 Color: 0
Size: 2012 Color: 0
Size: 400 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5077 Color: 1
Size: 2591 Color: 1
Size: 516 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5080 Color: 3
Size: 2592 Color: 0
Size: 512 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6919 Color: 1
Size: 1055 Color: 3
Size: 210 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 3
Size: 1716 Color: 3
Size: 336 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 4836 Color: 3
Size: 3036 Color: 3
Size: 312 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 1
Size: 1084 Color: 3
Size: 208 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 3
Size: 2274 Color: 3
Size: 452 Color: 2

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6058 Color: 4
Size: 1886 Color: 3
Size: 240 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 1
Size: 3194 Color: 2
Size: 340 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6188 Color: 1
Size: 1860 Color: 4
Size: 136 Color: 4

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4094 Color: 3
Size: 3410 Color: 3
Size: 680 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5465 Color: 4
Size: 2267 Color: 2
Size: 452 Color: 2

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6875 Color: 0
Size: 1183 Color: 1
Size: 126 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6927 Color: 3
Size: 1049 Color: 4
Size: 208 Color: 2

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 2
Size: 2444 Color: 2
Size: 480 Color: 4

Bin 92: 0 of cap free
Amount of items: 4
Items: 
Size: 6852 Color: 1
Size: 1116 Color: 4
Size: 112 Color: 1
Size: 104 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5802 Color: 0
Size: 1986 Color: 4
Size: 396 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5060 Color: 1
Size: 2604 Color: 0
Size: 520 Color: 4

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 4
Size: 1204 Color: 1
Size: 232 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 0
Size: 950 Color: 2
Size: 188 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 0
Size: 1212 Color: 0
Size: 240 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 4
Size: 762 Color: 2
Size: 152 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 4
Size: 902 Color: 1
Size: 180 Color: 4

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 4580 Color: 2
Size: 3004 Color: 2
Size: 600 Color: 4

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 0
Size: 1222 Color: 4
Size: 240 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 6709 Color: 1
Size: 1231 Color: 0
Size: 244 Color: 4

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 4
Size: 1084 Color: 0
Size: 80 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 4
Size: 924 Color: 3
Size: 184 Color: 4

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 7111 Color: 1
Size: 895 Color: 4
Size: 178 Color: 3

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 7015 Color: 2
Size: 975 Color: 1
Size: 194 Color: 2

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 2
Size: 1314 Color: 1
Size: 260 Color: 2

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 7043 Color: 3
Size: 951 Color: 1
Size: 190 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 4
Size: 2582 Color: 0
Size: 512 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 5786 Color: 1
Size: 2002 Color: 1
Size: 396 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6991 Color: 1
Size: 995 Color: 2
Size: 198 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6099 Color: 3
Size: 1739 Color: 1
Size: 346 Color: 4

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 4930 Color: 2
Size: 2714 Color: 1
Size: 540 Color: 2

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 4
Size: 1386 Color: 0
Size: 276 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 7330 Color: 1
Size: 714 Color: 0
Size: 140 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6231 Color: 3
Size: 1629 Color: 4
Size: 324 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 4630 Color: 4
Size: 2962 Color: 1
Size: 592 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6330 Color: 3
Size: 1546 Color: 1
Size: 308 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 3
Size: 870 Color: 1
Size: 172 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6926 Color: 0
Size: 1050 Color: 4
Size: 208 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 7019 Color: 4
Size: 971 Color: 2
Size: 194 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5759 Color: 0
Size: 2201 Color: 3
Size: 224 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6227 Color: 4
Size: 1631 Color: 3
Size: 326 Color: 3

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 4
Size: 1479 Color: 4
Size: 294 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 2
Size: 1353 Color: 3
Size: 270 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5839 Color: 0
Size: 1955 Color: 1
Size: 390 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 1
Size: 1189 Color: 0
Size: 236 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6219 Color: 0
Size: 1639 Color: 0
Size: 326 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5855 Color: 0
Size: 1941 Color: 3
Size: 388 Color: 4

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6223 Color: 2
Size: 1635 Color: 0
Size: 326 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 7115 Color: 0
Size: 891 Color: 1
Size: 178 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6585 Color: 3
Size: 1333 Color: 0
Size: 266 Color: 2

Total size: 1080288
Total free space: 0

