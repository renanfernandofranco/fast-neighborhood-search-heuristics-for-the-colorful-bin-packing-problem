Capicity Bin: 2464
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 2024 Color: 2
Size: 394 Color: 4
Size: 46 Color: 3

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 2138 Color: 3
Size: 214 Color: 0
Size: 52 Color: 4
Size: 44 Color: 3
Size: 16 Color: 4

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 2210 Color: 0
Size: 148 Color: 3
Size: 42 Color: 1
Size: 32 Color: 2
Size: 32 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 4
Size: 250 Color: 0
Size: 48 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1235 Color: 1
Size: 1025 Color: 1
Size: 204 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 1
Size: 478 Color: 1
Size: 168 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 4
Size: 274 Color: 4
Size: 92 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1866 Color: 0
Size: 542 Color: 1
Size: 56 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 1
Size: 762 Color: 0
Size: 148 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2183 Color: 4
Size: 221 Color: 4
Size: 60 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 0
Size: 810 Color: 0
Size: 160 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 1
Size: 374 Color: 1
Size: 96 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 4
Size: 362 Color: 2
Size: 68 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 2
Size: 261 Color: 3
Size: 50 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2045 Color: 1
Size: 351 Color: 1
Size: 68 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 3
Size: 442 Color: 0
Size: 88 Color: 3

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 1220 Color: 3
Size: 840 Color: 0
Size: 340 Color: 1
Size: 64 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2135 Color: 2
Size: 275 Color: 3
Size: 54 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1969 Color: 2
Size: 413 Color: 4
Size: 82 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2055 Color: 3
Size: 387 Color: 0
Size: 22 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 3
Size: 578 Color: 0
Size: 112 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 4
Size: 226 Color: 1
Size: 44 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 2
Size: 425 Color: 4
Size: 84 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 3
Size: 591 Color: 0
Size: 116 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 1
Size: 871 Color: 2
Size: 50 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 1
Size: 383 Color: 0
Size: 76 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 4
Size: 346 Color: 0
Size: 56 Color: 2

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 1080 Color: 0
Size: 912 Color: 3
Size: 368 Color: 4
Size: 104 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 2
Size: 942 Color: 2
Size: 184 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2087 Color: 3
Size: 315 Color: 2
Size: 62 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2143 Color: 0
Size: 269 Color: 0
Size: 52 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1250 Color: 0
Size: 1014 Color: 0
Size: 200 Color: 1

Bin 33: 0 of cap free
Amount of items: 4
Items: 
Size: 1972 Color: 0
Size: 304 Color: 2
Size: 116 Color: 4
Size: 72 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 3
Size: 662 Color: 3
Size: 128 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 3
Size: 523 Color: 1
Size: 104 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2150 Color: 3
Size: 262 Color: 4
Size: 52 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 2
Size: 621 Color: 3
Size: 122 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1607 Color: 0
Size: 715 Color: 3
Size: 142 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 3
Size: 1102 Color: 1
Size: 128 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2169 Color: 4
Size: 273 Color: 1
Size: 22 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 4
Size: 286 Color: 1
Size: 72 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 3
Size: 302 Color: 4
Size: 40 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 1
Size: 306 Color: 4
Size: 140 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2121 Color: 4
Size: 287 Color: 1
Size: 56 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 0
Size: 622 Color: 3
Size: 124 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 2193 Color: 1
Size: 267 Color: 0
Size: 4 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1905 Color: 0
Size: 467 Color: 3
Size: 92 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 3
Size: 722 Color: 3
Size: 140 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 1
Size: 502 Color: 2
Size: 68 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2201 Color: 3
Size: 235 Color: 2
Size: 28 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 3
Size: 421 Color: 2
Size: 82 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2067 Color: 2
Size: 331 Color: 0
Size: 66 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 0
Size: 771 Color: 3
Size: 152 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1354 Color: 3
Size: 1062 Color: 2
Size: 48 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 1
Size: 418 Color: 1
Size: 80 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 2215 Color: 1
Size: 209 Color: 0
Size: 40 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 1
Size: 338 Color: 0
Size: 76 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 1
Size: 870 Color: 4
Size: 172 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 4
Size: 1027 Color: 4
Size: 204 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 3
Size: 481 Color: 4
Size: 96 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 4
Size: 887 Color: 2
Size: 176 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 0
Size: 541 Color: 0
Size: 108 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 0
Size: 301 Color: 4
Size: 60 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1403 Color: 1
Size: 885 Color: 1
Size: 176 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 3
Size: 669 Color: 1
Size: 132 Color: 4

Total size: 160160
Total free space: 0

