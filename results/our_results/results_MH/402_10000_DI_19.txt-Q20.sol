Capicity Bin: 7896
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 17
Items: 
Size: 624 Color: 19
Size: 578 Color: 8
Size: 570 Color: 12
Size: 568 Color: 4
Size: 564 Color: 9
Size: 504 Color: 17
Size: 504 Color: 5
Size: 494 Color: 9
Size: 492 Color: 17
Size: 484 Color: 11
Size: 480 Color: 0
Size: 440 Color: 3
Size: 434 Color: 12
Size: 416 Color: 9
Size: 380 Color: 16
Size: 190 Color: 2
Size: 174 Color: 11

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 3954 Color: 10
Size: 2116 Color: 16
Size: 1224 Color: 14
Size: 346 Color: 12
Size: 256 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4446 Color: 10
Size: 3286 Color: 14
Size: 164 Color: 8

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4636 Color: 13
Size: 3044 Color: 15
Size: 216 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5180 Color: 12
Size: 2124 Color: 7
Size: 592 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5330 Color: 4
Size: 2406 Color: 2
Size: 160 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5942 Color: 10
Size: 1434 Color: 15
Size: 520 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6044 Color: 9
Size: 1548 Color: 18
Size: 304 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6065 Color: 16
Size: 1165 Color: 14
Size: 666 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6202 Color: 19
Size: 1488 Color: 4
Size: 206 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6302 Color: 18
Size: 1356 Color: 10
Size: 238 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6312 Color: 2
Size: 862 Color: 15
Size: 722 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 16
Size: 1468 Color: 0
Size: 72 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6386 Color: 0
Size: 1072 Color: 16
Size: 438 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 10
Size: 1251 Color: 11
Size: 180 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6474 Color: 1
Size: 1252 Color: 19
Size: 170 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6483 Color: 14
Size: 973 Color: 16
Size: 440 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 5
Size: 1082 Color: 11
Size: 264 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6538 Color: 19
Size: 1180 Color: 19
Size: 178 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6608 Color: 13
Size: 656 Color: 6
Size: 632 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6609 Color: 17
Size: 1179 Color: 3
Size: 108 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6649 Color: 5
Size: 1011 Color: 3
Size: 236 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 4
Size: 807 Color: 10
Size: 432 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 3
Size: 1076 Color: 9
Size: 154 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6714 Color: 14
Size: 868 Color: 17
Size: 314 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 12
Size: 692 Color: 11
Size: 480 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6778 Color: 14
Size: 976 Color: 1
Size: 142 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6839 Color: 17
Size: 891 Color: 16
Size: 166 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6868 Color: 0
Size: 656 Color: 9
Size: 372 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6939 Color: 14
Size: 799 Color: 3
Size: 158 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 0
Size: 852 Color: 7
Size: 104 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 11
Size: 574 Color: 4
Size: 352 Color: 10

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7058 Color: 19
Size: 496 Color: 18
Size: 342 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 10
Size: 512 Color: 17
Size: 306 Color: 3

Bin 35: 1 of cap free
Amount of items: 8
Items: 
Size: 3949 Color: 7
Size: 860 Color: 11
Size: 674 Color: 9
Size: 656 Color: 7
Size: 656 Color: 3
Size: 572 Color: 0
Size: 336 Color: 2
Size: 192 Color: 5

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5251 Color: 15
Size: 2444 Color: 9
Size: 200 Color: 3

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5549 Color: 16
Size: 1516 Color: 15
Size: 830 Color: 2

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 2
Size: 1707 Color: 10
Size: 424 Color: 3

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 16
Size: 1923 Color: 13
Size: 168 Color: 15

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5966 Color: 16
Size: 1733 Color: 11
Size: 196 Color: 5

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6033 Color: 15
Size: 1862 Color: 16

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6049 Color: 4
Size: 1686 Color: 4
Size: 160 Color: 5

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 18
Size: 1122 Color: 11
Size: 560 Color: 3

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 1
Size: 1375 Color: 5
Size: 204 Color: 0

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6322 Color: 1
Size: 1573 Color: 15

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6397 Color: 10
Size: 1498 Color: 14

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 18
Size: 1283 Color: 16

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6729 Color: 16
Size: 934 Color: 10
Size: 232 Color: 15

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6761 Color: 15
Size: 1134 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 2
Size: 750 Color: 14
Size: 336 Color: 4

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 1
Size: 961 Color: 10
Size: 112 Color: 19

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6849 Color: 4
Size: 898 Color: 7
Size: 148 Color: 18

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6889 Color: 10
Size: 742 Color: 8
Size: 264 Color: 6

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 16
Size: 841 Color: 7
Size: 152 Color: 10

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 5
Size: 827 Color: 8

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 5468 Color: 18
Size: 2426 Color: 5

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5817 Color: 10
Size: 1753 Color: 0
Size: 324 Color: 11

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 3
Size: 1484 Color: 5
Size: 536 Color: 14

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 3
Size: 1721 Color: 0
Size: 164 Color: 14

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 6178 Color: 2
Size: 1716 Color: 11

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 6551 Color: 19
Size: 1191 Color: 5
Size: 152 Color: 13

Bin 62: 3 of cap free
Amount of items: 6
Items: 
Size: 3950 Color: 3
Size: 1080 Color: 8
Size: 1073 Color: 17
Size: 980 Color: 14
Size: 682 Color: 6
Size: 128 Color: 1

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 5
Size: 3289 Color: 3
Size: 288 Color: 16

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 4425 Color: 3
Size: 3284 Color: 11
Size: 184 Color: 0

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 4449 Color: 4
Size: 3188 Color: 19
Size: 256 Color: 7

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 4927 Color: 18
Size: 2822 Color: 8
Size: 144 Color: 18

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 5010 Color: 5
Size: 2475 Color: 19
Size: 408 Color: 15

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 5265 Color: 10
Size: 2628 Color: 0

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 5428 Color: 2
Size: 2465 Color: 17

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 5573 Color: 11
Size: 2060 Color: 3
Size: 260 Color: 7

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 5833 Color: 8
Size: 1866 Color: 0
Size: 194 Color: 18

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 6259 Color: 5
Size: 1634 Color: 19

Bin 73: 3 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 7
Size: 1033 Color: 15

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 5
Size: 3472 Color: 18
Size: 176 Color: 4

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 5666 Color: 19
Size: 1922 Color: 10
Size: 304 Color: 5

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 6351 Color: 7
Size: 1541 Color: 10

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 16
Size: 1026 Color: 15

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 2
Size: 802 Color: 8

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 4465 Color: 17
Size: 2818 Color: 14
Size: 608 Color: 8

Bin 80: 5 of cap free
Amount of items: 4
Items: 
Size: 4918 Color: 4
Size: 2181 Color: 16
Size: 656 Color: 12
Size: 136 Color: 19

Bin 81: 5 of cap free
Amount of items: 2
Items: 
Size: 5793 Color: 2
Size: 2098 Color: 14

Bin 82: 5 of cap free
Amount of items: 2
Items: 
Size: 6602 Color: 10
Size: 1289 Color: 19

Bin 83: 5 of cap free
Amount of items: 2
Items: 
Size: 6905 Color: 19
Size: 986 Color: 16

Bin 84: 5 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 15
Size: 861 Color: 17

Bin 85: 7 of cap free
Amount of items: 3
Items: 
Size: 5844 Color: 12
Size: 1957 Color: 9
Size: 88 Color: 10

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 6575 Color: 10
Size: 1314 Color: 0

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 5356 Color: 19
Size: 2142 Color: 7
Size: 390 Color: 3

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 18
Size: 1124 Color: 8
Size: 368 Color: 5

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 5364 Color: 16
Size: 2523 Color: 10

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 6484 Color: 0
Size: 1403 Color: 16

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 6865 Color: 1
Size: 1022 Color: 2

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 6980 Color: 1
Size: 907 Color: 11

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 6556 Color: 14
Size: 1330 Color: 9

Bin 94: 10 of cap free
Amount of items: 3
Items: 
Size: 6674 Color: 8
Size: 764 Color: 17
Size: 448 Color: 5

Bin 95: 11 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 19
Size: 2847 Color: 14
Size: 234 Color: 3

Bin 96: 13 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 17
Size: 1527 Color: 8
Size: 272 Color: 3

Bin 97: 13 of cap free
Amount of items: 2
Items: 
Size: 6621 Color: 16
Size: 1262 Color: 15

Bin 98: 13 of cap free
Amount of items: 2
Items: 
Size: 6934 Color: 1
Size: 949 Color: 12

Bin 99: 14 of cap free
Amount of items: 2
Items: 
Size: 6102 Color: 10
Size: 1780 Color: 5

Bin 100: 15 of cap free
Amount of items: 4
Items: 
Size: 3951 Color: 3
Size: 1937 Color: 8
Size: 1101 Color: 10
Size: 892 Color: 13

Bin 101: 15 of cap free
Amount of items: 2
Items: 
Size: 6467 Color: 4
Size: 1414 Color: 14

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 4986 Color: 1
Size: 2893 Color: 17

Bin 103: 17 of cap free
Amount of items: 3
Items: 
Size: 6827 Color: 9
Size: 1012 Color: 2
Size: 40 Color: 11

Bin 104: 17 of cap free
Amount of items: 2
Items: 
Size: 7006 Color: 11
Size: 873 Color: 4

Bin 105: 18 of cap free
Amount of items: 2
Items: 
Size: 6745 Color: 11
Size: 1133 Color: 19

Bin 106: 19 of cap free
Amount of items: 4
Items: 
Size: 4076 Color: 5
Size: 2167 Color: 14
Size: 1324 Color: 7
Size: 310 Color: 3

Bin 107: 19 of cap free
Amount of items: 2
Items: 
Size: 5297 Color: 7
Size: 2580 Color: 5

Bin 108: 19 of cap free
Amount of items: 2
Items: 
Size: 5849 Color: 13
Size: 2028 Color: 11

Bin 109: 19 of cap free
Amount of items: 2
Items: 
Size: 6247 Color: 14
Size: 1630 Color: 17

Bin 110: 20 of cap free
Amount of items: 2
Items: 
Size: 6685 Color: 10
Size: 1191 Color: 16

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 6929 Color: 11
Size: 947 Color: 12

Bin 112: 24 of cap free
Amount of items: 2
Items: 
Size: 6124 Color: 0
Size: 1748 Color: 1

Bin 113: 24 of cap free
Amount of items: 2
Items: 
Size: 7098 Color: 4
Size: 774 Color: 9

Bin 114: 25 of cap free
Amount of items: 3
Items: 
Size: 4939 Color: 4
Size: 2724 Color: 14
Size: 208 Color: 3

Bin 115: 26 of cap free
Amount of items: 2
Items: 
Size: 6684 Color: 8
Size: 1186 Color: 12

Bin 116: 27 of cap free
Amount of items: 2
Items: 
Size: 6828 Color: 7
Size: 1041 Color: 1

Bin 117: 32 of cap free
Amount of items: 2
Items: 
Size: 5382 Color: 18
Size: 2482 Color: 15

Bin 118: 32 of cap free
Amount of items: 2
Items: 
Size: 6499 Color: 6
Size: 1365 Color: 17

Bin 119: 33 of cap free
Amount of items: 2
Items: 
Size: 5658 Color: 4
Size: 2205 Color: 7

Bin 120: 34 of cap free
Amount of items: 3
Items: 
Size: 5589 Color: 8
Size: 2193 Color: 12
Size: 80 Color: 5

Bin 121: 34 of cap free
Amount of items: 2
Items: 
Size: 5594 Color: 4
Size: 2268 Color: 17

Bin 122: 36 of cap free
Amount of items: 2
Items: 
Size: 3956 Color: 12
Size: 3904 Color: 15

Bin 123: 36 of cap free
Amount of items: 2
Items: 
Size: 4872 Color: 11
Size: 2988 Color: 6

Bin 124: 39 of cap free
Amount of items: 2
Items: 
Size: 5281 Color: 13
Size: 2576 Color: 17

Bin 125: 46 of cap free
Amount of items: 2
Items: 
Size: 4972 Color: 16
Size: 2878 Color: 13

Bin 126: 67 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 0
Size: 1553 Color: 6

Bin 127: 87 of cap free
Amount of items: 2
Items: 
Size: 4518 Color: 10
Size: 3291 Color: 5

Bin 128: 91 of cap free
Amount of items: 2
Items: 
Size: 4869 Color: 7
Size: 2936 Color: 11

Bin 129: 94 of cap free
Amount of items: 3
Items: 
Size: 4481 Color: 12
Size: 2873 Color: 8
Size: 448 Color: 3

Bin 130: 96 of cap free
Amount of items: 2
Items: 
Size: 4510 Color: 7
Size: 3290 Color: 12

Bin 131: 108 of cap free
Amount of items: 27
Items: 
Size: 416 Color: 15
Size: 416 Color: 15
Size: 400 Color: 13
Size: 386 Color: 19
Size: 384 Color: 1
Size: 376 Color: 12
Size: 350 Color: 0
Size: 344 Color: 6
Size: 340 Color: 5
Size: 296 Color: 19
Size: 296 Color: 15
Size: 296 Color: 0
Size: 284 Color: 11
Size: 280 Color: 11
Size: 280 Color: 6
Size: 274 Color: 1
Size: 248 Color: 14
Size: 248 Color: 13
Size: 248 Color: 10
Size: 232 Color: 8
Size: 224 Color: 16
Size: 224 Color: 16
Size: 220 Color: 14
Size: 214 Color: 19
Size: 212 Color: 7
Size: 168 Color: 2
Size: 132 Color: 2

Bin 132: 111 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 8
Size: 2861 Color: 10
Size: 176 Color: 12

Bin 133: 6310 of cap free
Amount of items: 9
Items: 
Size: 208 Color: 0
Size: 206 Color: 1
Size: 200 Color: 14
Size: 200 Color: 5
Size: 188 Color: 16
Size: 168 Color: 12
Size: 148 Color: 7
Size: 136 Color: 13
Size: 132 Color: 2

Total size: 1042272
Total free space: 7896

