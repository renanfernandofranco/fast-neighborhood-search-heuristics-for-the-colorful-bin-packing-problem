Capicity Bin: 2428
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 1060 Color: 4
Size: 916 Color: 8
Size: 208 Color: 18
Size: 160 Color: 19
Size: 44 Color: 6
Size: 40 Color: 2

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1626 Color: 15
Size: 662 Color: 2
Size: 108 Color: 8
Size: 32 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 11
Size: 386 Color: 2
Size: 76 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 16
Size: 638 Color: 16
Size: 132 Color: 12

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1956 Color: 1
Size: 344 Color: 2
Size: 124 Color: 0
Size: 4 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 10
Size: 522 Color: 17
Size: 104 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 7
Size: 521 Color: 15
Size: 104 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 10
Size: 342 Color: 9
Size: 64 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 10
Size: 382 Color: 6
Size: 72 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 12
Size: 702 Color: 14
Size: 140 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 11
Size: 950 Color: 6
Size: 188 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 18
Size: 902 Color: 16
Size: 176 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1779 Color: 5
Size: 609 Color: 12
Size: 40 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1834 Color: 19
Size: 498 Color: 3
Size: 96 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 18
Size: 769 Color: 2
Size: 152 Color: 5

Bin 16: 0 of cap free
Amount of items: 4
Items: 
Size: 1196 Color: 2
Size: 836 Color: 9
Size: 292 Color: 13
Size: 104 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 16
Size: 274 Color: 11
Size: 52 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2134 Color: 12
Size: 246 Color: 11
Size: 48 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 19
Size: 610 Color: 0
Size: 120 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 7
Size: 629 Color: 3
Size: 124 Color: 7

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 3
Size: 530 Color: 8
Size: 104 Color: 9

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 0
Size: 378 Color: 19
Size: 72 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 3
Size: 670 Color: 3
Size: 92 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 0
Size: 434 Color: 2
Size: 84 Color: 5

Bin 25: 0 of cap free
Amount of items: 4
Items: 
Size: 1454 Color: 19
Size: 814 Color: 5
Size: 152 Color: 15
Size: 8 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 4
Size: 1010 Color: 17
Size: 200 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 3
Size: 937 Color: 9
Size: 32 Color: 18

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 4
Size: 526 Color: 0
Size: 104 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1838 Color: 18
Size: 494 Color: 17
Size: 96 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2026 Color: 19
Size: 338 Color: 19
Size: 64 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 5
Size: 382 Color: 17
Size: 76 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1889 Color: 9
Size: 451 Color: 13
Size: 88 Color: 9

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 17
Size: 742 Color: 3
Size: 144 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 17
Size: 791 Color: 11
Size: 156 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 7
Size: 310 Color: 11
Size: 24 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1639 Color: 1
Size: 659 Color: 10
Size: 130 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 0
Size: 523 Color: 12
Size: 104 Color: 10

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1263 Color: 11
Size: 971 Color: 18
Size: 194 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 3
Size: 951 Color: 1
Size: 190 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 19
Size: 375 Color: 1
Size: 74 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1495 Color: 8
Size: 779 Color: 3
Size: 154 Color: 16

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1671 Color: 16
Size: 631 Color: 17
Size: 126 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 8
Size: 371 Color: 16
Size: 74 Color: 13

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1987 Color: 10
Size: 369 Color: 11
Size: 72 Color: 12

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 16
Size: 325 Color: 17
Size: 64 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 17
Size: 635 Color: 8
Size: 126 Color: 17

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1219 Color: 16
Size: 1149 Color: 0
Size: 60 Color: 11

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 2
Size: 306 Color: 19
Size: 60 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1885 Color: 13
Size: 453 Color: 14
Size: 90 Color: 12

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 10
Size: 395 Color: 12
Size: 78 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1959 Color: 4
Size: 391 Color: 1
Size: 78 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 13
Size: 655 Color: 8
Size: 130 Color: 12

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 10
Size: 949 Color: 19
Size: 188 Color: 10

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 2101 Color: 6
Size: 273 Color: 5
Size: 54 Color: 10

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1215 Color: 8
Size: 1011 Color: 15
Size: 202 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 1
Size: 771 Color: 1
Size: 154 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1499 Color: 15
Size: 775 Color: 1
Size: 154 Color: 10

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1259 Color: 0
Size: 975 Color: 7
Size: 194 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2105 Color: 10
Size: 271 Color: 10
Size: 52 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2162 Color: 10
Size: 222 Color: 5
Size: 44 Color: 5

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 12
Size: 639 Color: 14
Size: 126 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1283 Color: 5
Size: 955 Color: 4
Size: 190 Color: 15

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 9
Size: 307 Color: 8
Size: 60 Color: 15

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2083 Color: 9
Size: 309 Color: 1
Size: 36 Color: 12

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2135 Color: 10
Size: 245 Color: 11
Size: 48 Color: 12

Total size: 157820
Total free space: 0

