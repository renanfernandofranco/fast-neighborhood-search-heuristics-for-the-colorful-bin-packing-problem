Capicity Bin: 16000
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 8008 Color: 19
Size: 1736 Color: 14
Size: 1718 Color: 1
Size: 1650 Color: 15
Size: 1556 Color: 11
Size: 672 Color: 19
Size: 660 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8936 Color: 15
Size: 6662 Color: 5
Size: 402 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11586 Color: 17
Size: 4044 Color: 17
Size: 370 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11624 Color: 4
Size: 4088 Color: 1
Size: 288 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11828 Color: 10
Size: 3524 Color: 0
Size: 648 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 0
Size: 3432 Color: 12
Size: 288 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12347 Color: 6
Size: 3045 Color: 17
Size: 608 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 18
Size: 3112 Color: 0
Size: 540 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 6
Size: 3228 Color: 10
Size: 332 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 15
Size: 2744 Color: 4
Size: 624 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12834 Color: 3
Size: 2728 Color: 13
Size: 438 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12847 Color: 2
Size: 2417 Color: 14
Size: 736 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 1
Size: 2444 Color: 2
Size: 580 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12986 Color: 13
Size: 2722 Color: 4
Size: 292 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 12
Size: 1570 Color: 10
Size: 1370 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13062 Color: 13
Size: 2162 Color: 19
Size: 776 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13158 Color: 4
Size: 2318 Color: 13
Size: 524 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 1
Size: 2450 Color: 15
Size: 312 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13300 Color: 12
Size: 2180 Color: 12
Size: 520 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13354 Color: 5
Size: 2170 Color: 1
Size: 476 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13519 Color: 2
Size: 1573 Color: 2
Size: 908 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 1
Size: 1784 Color: 4
Size: 652 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 3
Size: 1853 Color: 1
Size: 500 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 18
Size: 1548 Color: 11
Size: 784 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13670 Color: 7
Size: 2016 Color: 4
Size: 314 Color: 18

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 13
Size: 1816 Color: 11
Size: 352 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 10
Size: 1144 Color: 13
Size: 928 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13942 Color: 11
Size: 1684 Color: 8
Size: 374 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13980 Color: 15
Size: 1692 Color: 8
Size: 328 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13997 Color: 2
Size: 1563 Color: 14
Size: 440 Color: 18

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 11
Size: 1332 Color: 1
Size: 618 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14066 Color: 19
Size: 1582 Color: 13
Size: 352 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 3
Size: 1404 Color: 6
Size: 528 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14106 Color: 17
Size: 1858 Color: 11
Size: 36 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 10
Size: 1394 Color: 0
Size: 488 Color: 12

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 13
Size: 1264 Color: 3
Size: 616 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 16
Size: 1512 Color: 11
Size: 288 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14214 Color: 19
Size: 1152 Color: 16
Size: 634 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 4
Size: 1464 Color: 16
Size: 288 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14330 Color: 17
Size: 1302 Color: 15
Size: 368 Color: 8

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14338 Color: 1
Size: 898 Color: 13
Size: 764 Color: 14

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14346 Color: 17
Size: 1382 Color: 7
Size: 272 Color: 4

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 10558 Color: 1
Size: 4981 Color: 1
Size: 460 Color: 2

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 12078 Color: 7
Size: 3469 Color: 18
Size: 452 Color: 9

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 12101 Color: 18
Size: 3666 Color: 11
Size: 232 Color: 13

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 12444 Color: 17
Size: 3555 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 12611 Color: 10
Size: 2906 Color: 17
Size: 482 Color: 7

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 12743 Color: 17
Size: 3056 Color: 2
Size: 200 Color: 13

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 6
Size: 2715 Color: 19
Size: 540 Color: 10

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 4
Size: 2514 Color: 0
Size: 480 Color: 17

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 13253 Color: 11
Size: 1466 Color: 11
Size: 1280 Color: 17

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 13425 Color: 10
Size: 2206 Color: 4
Size: 368 Color: 16

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 13758 Color: 12
Size: 1857 Color: 12
Size: 384 Color: 17

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 13773 Color: 12
Size: 1930 Color: 19
Size: 296 Color: 4

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 13945 Color: 14
Size: 2054 Color: 5

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 14083 Color: 4
Size: 1368 Color: 12
Size: 548 Color: 8

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 14140 Color: 1
Size: 1859 Color: 19

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 8814 Color: 19
Size: 6660 Color: 19
Size: 524 Color: 11

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 8830 Color: 18
Size: 6664 Color: 3
Size: 504 Color: 14

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 10504 Color: 1
Size: 5494 Color: 12

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 1
Size: 4550 Color: 14
Size: 352 Color: 1

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 11413 Color: 9
Size: 2913 Color: 4
Size: 1672 Color: 4

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 11580 Color: 18
Size: 3928 Color: 19
Size: 490 Color: 18

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 15
Size: 3946 Color: 17
Size: 272 Color: 10

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 12260 Color: 7
Size: 3434 Color: 10
Size: 304 Color: 2

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 12514 Color: 7
Size: 3484 Color: 10

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 12698 Color: 4
Size: 3300 Color: 17

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 13274 Color: 8
Size: 2452 Color: 3
Size: 272 Color: 4

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 15
Size: 1604 Color: 5
Size: 1058 Color: 17

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 13365 Color: 2
Size: 2633 Color: 3

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 13538 Color: 3
Size: 2136 Color: 17
Size: 324 Color: 2

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 14360 Color: 6
Size: 1638 Color: 7

Bin 73: 3 of cap free
Amount of items: 9
Items: 
Size: 8003 Color: 3
Size: 1088 Color: 18
Size: 1080 Color: 9
Size: 1000 Color: 9
Size: 1000 Color: 0
Size: 994 Color: 11
Size: 992 Color: 8
Size: 936 Color: 16
Size: 904 Color: 4

Bin 74: 3 of cap free
Amount of items: 6
Items: 
Size: 8010 Color: 17
Size: 1942 Color: 16
Size: 1844 Color: 18
Size: 1807 Color: 18
Size: 1766 Color: 5
Size: 628 Color: 11

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 9432 Color: 12
Size: 6133 Color: 16
Size: 432 Color: 7

Bin 76: 3 of cap free
Amount of items: 3
Items: 
Size: 9502 Color: 14
Size: 6099 Color: 7
Size: 396 Color: 7

Bin 77: 3 of cap free
Amount of items: 2
Items: 
Size: 11896 Color: 9
Size: 4101 Color: 2

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 13833 Color: 16
Size: 1772 Color: 4
Size: 392 Color: 1

Bin 79: 4 of cap free
Amount of items: 3
Items: 
Size: 8185 Color: 13
Size: 7459 Color: 7
Size: 352 Color: 2

Bin 80: 4 of cap free
Amount of items: 3
Items: 
Size: 11266 Color: 8
Size: 4538 Color: 10
Size: 192 Color: 17

Bin 81: 4 of cap free
Amount of items: 3
Items: 
Size: 11512 Color: 17
Size: 4164 Color: 11
Size: 320 Color: 13

Bin 82: 4 of cap free
Amount of items: 3
Items: 
Size: 11534 Color: 8
Size: 3130 Color: 3
Size: 1332 Color: 12

Bin 83: 4 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 1
Size: 3208 Color: 4

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 13370 Color: 10
Size: 2626 Color: 15

Bin 85: 5 of cap free
Amount of items: 2
Items: 
Size: 14125 Color: 5
Size: 1870 Color: 2

Bin 86: 5 of cap free
Amount of items: 2
Items: 
Size: 14324 Color: 3
Size: 1671 Color: 2

Bin 87: 6 of cap free
Amount of items: 7
Items: 
Size: 8012 Color: 10
Size: 2147 Color: 13
Size: 1986 Color: 0
Size: 1713 Color: 9
Size: 1328 Color: 3
Size: 536 Color: 9
Size: 272 Color: 19

Bin 88: 6 of cap free
Amount of items: 3
Items: 
Size: 9131 Color: 4
Size: 6503 Color: 15
Size: 360 Color: 1

Bin 89: 6 of cap free
Amount of items: 2
Items: 
Size: 12230 Color: 2
Size: 3764 Color: 7

Bin 90: 6 of cap free
Amount of items: 2
Items: 
Size: 12338 Color: 1
Size: 3656 Color: 6

Bin 91: 6 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 13
Size: 2530 Color: 1
Size: 96 Color: 19

Bin 92: 6 of cap free
Amount of items: 2
Items: 
Size: 13624 Color: 10
Size: 2370 Color: 12

Bin 93: 6 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 7
Size: 2274 Color: 10

Bin 94: 6 of cap free
Amount of items: 2
Items: 
Size: 14148 Color: 0
Size: 1846 Color: 16

Bin 95: 7 of cap free
Amount of items: 2
Items: 
Size: 10025 Color: 3
Size: 5968 Color: 14

Bin 96: 7 of cap free
Amount of items: 3
Items: 
Size: 11297 Color: 1
Size: 4584 Color: 15
Size: 112 Color: 16

Bin 97: 7 of cap free
Amount of items: 3
Items: 
Size: 13398 Color: 15
Size: 2291 Color: 3
Size: 304 Color: 17

Bin 98: 7 of cap free
Amount of items: 2
Items: 
Size: 13793 Color: 6
Size: 2200 Color: 15

Bin 99: 8 of cap free
Amount of items: 9
Items: 
Size: 8002 Color: 0
Size: 1356 Color: 17
Size: 1328 Color: 17
Size: 1328 Color: 6
Size: 1328 Color: 0
Size: 1096 Color: 2
Size: 800 Color: 3
Size: 412 Color: 8
Size: 342 Color: 2

Bin 100: 8 of cap free
Amount of items: 2
Items: 
Size: 9016 Color: 15
Size: 6976 Color: 4

Bin 101: 8 of cap free
Amount of items: 3
Items: 
Size: 12102 Color: 11
Size: 3614 Color: 8
Size: 276 Color: 3

Bin 102: 8 of cap free
Amount of items: 2
Items: 
Size: 13308 Color: 16
Size: 2684 Color: 14

Bin 103: 8 of cap free
Amount of items: 2
Items: 
Size: 13448 Color: 9
Size: 2544 Color: 0

Bin 104: 8 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 11
Size: 1612 Color: 7

Bin 105: 9 of cap free
Amount of items: 10
Items: 
Size: 8001 Color: 17
Size: 988 Color: 8
Size: 984 Color: 6
Size: 976 Color: 10
Size: 942 Color: 8
Size: 936 Color: 6
Size: 936 Color: 6
Size: 848 Color: 16
Size: 788 Color: 13
Size: 592 Color: 10

Bin 106: 9 of cap free
Amount of items: 3
Items: 
Size: 9645 Color: 8
Size: 6070 Color: 6
Size: 276 Color: 6

Bin 107: 9 of cap free
Amount of items: 2
Items: 
Size: 12168 Color: 0
Size: 3823 Color: 4

Bin 108: 9 of cap free
Amount of items: 2
Items: 
Size: 12740 Color: 17
Size: 3251 Color: 19

Bin 109: 10 of cap free
Amount of items: 3
Items: 
Size: 9141 Color: 17
Size: 6513 Color: 8
Size: 336 Color: 9

Bin 110: 10 of cap free
Amount of items: 3
Items: 
Size: 12850 Color: 9
Size: 2964 Color: 17
Size: 176 Color: 7

Bin 111: 10 of cap free
Amount of items: 2
Items: 
Size: 13954 Color: 10
Size: 2036 Color: 13

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 14113 Color: 16
Size: 1877 Color: 5

Bin 113: 11 of cap free
Amount of items: 3
Items: 
Size: 10341 Color: 13
Size: 5336 Color: 10
Size: 312 Color: 6

Bin 114: 11 of cap free
Amount of items: 2
Items: 
Size: 10897 Color: 0
Size: 5092 Color: 14

Bin 115: 11 of cap free
Amount of items: 2
Items: 
Size: 12890 Color: 1
Size: 3099 Color: 12

Bin 116: 11 of cap free
Amount of items: 2
Items: 
Size: 13053 Color: 14
Size: 2936 Color: 1

Bin 117: 11 of cap free
Amount of items: 2
Items: 
Size: 13492 Color: 10
Size: 2497 Color: 15

Bin 118: 12 of cap free
Amount of items: 33
Items: 
Size: 688 Color: 2
Size: 648 Color: 8
Size: 624 Color: 2
Size: 608 Color: 6
Size: 592 Color: 1
Size: 584 Color: 0
Size: 542 Color: 15
Size: 536 Color: 13
Size: 528 Color: 14
Size: 528 Color: 10
Size: 528 Color: 9
Size: 512 Color: 19
Size: 512 Color: 11
Size: 498 Color: 3
Size: 496 Color: 12
Size: 488 Color: 13
Size: 480 Color: 16
Size: 480 Color: 13
Size: 472 Color: 16
Size: 456 Color: 15
Size: 448 Color: 2
Size: 432 Color: 5
Size: 432 Color: 4
Size: 428 Color: 12
Size: 416 Color: 7
Size: 416 Color: 7
Size: 412 Color: 16
Size: 408 Color: 1
Size: 400 Color: 12
Size: 388 Color: 4
Size: 384 Color: 5
Size: 320 Color: 3
Size: 304 Color: 10

Bin 119: 12 of cap free
Amount of items: 2
Items: 
Size: 11735 Color: 5
Size: 4253 Color: 4

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 12738 Color: 3
Size: 3250 Color: 18

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 13
Size: 3084 Color: 11

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 3
Size: 2302 Color: 7

Bin 123: 12 of cap free
Amount of items: 3
Items: 
Size: 13876 Color: 2
Size: 1988 Color: 16
Size: 124 Color: 12

Bin 124: 12 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 8
Size: 1992 Color: 0
Size: 52 Color: 19

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 14076 Color: 1
Size: 1912 Color: 14

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 14268 Color: 2
Size: 1720 Color: 9

Bin 127: 13 of cap free
Amount of items: 2
Items: 
Size: 13587 Color: 14
Size: 2400 Color: 2

Bin 128: 14 of cap free
Amount of items: 3
Items: 
Size: 10542 Color: 11
Size: 5012 Color: 15
Size: 432 Color: 13

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 11036 Color: 7
Size: 4950 Color: 9

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 8
Size: 2970 Color: 5

Bin 131: 15 of cap free
Amount of items: 3
Items: 
Size: 8408 Color: 7
Size: 6665 Color: 7
Size: 912 Color: 16

Bin 132: 15 of cap free
Amount of items: 2
Items: 
Size: 13788 Color: 16
Size: 2197 Color: 6

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 1
Size: 1961 Color: 2

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 12022 Color: 13
Size: 3962 Color: 10

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 12300 Color: 11
Size: 3684 Color: 15

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 14358 Color: 16
Size: 1626 Color: 17

Bin 137: 17 of cap free
Amount of items: 2
Items: 
Size: 13909 Color: 12
Size: 2074 Color: 7

Bin 138: 17 of cap free
Amount of items: 2
Items: 
Size: 13972 Color: 17
Size: 2011 Color: 8

Bin 139: 17 of cap free
Amount of items: 2
Items: 
Size: 14242 Color: 18
Size: 1741 Color: 16

Bin 140: 18 of cap free
Amount of items: 2
Items: 
Size: 11330 Color: 14
Size: 4652 Color: 15

Bin 141: 18 of cap free
Amount of items: 3
Items: 
Size: 11652 Color: 10
Size: 3894 Color: 9
Size: 436 Color: 3

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 16
Size: 3270 Color: 17

Bin 143: 19 of cap free
Amount of items: 2
Items: 
Size: 13749 Color: 0
Size: 2232 Color: 17

Bin 144: 21 of cap free
Amount of items: 2
Items: 
Size: 12800 Color: 9
Size: 3179 Color: 3

Bin 145: 22 of cap free
Amount of items: 2
Items: 
Size: 10930 Color: 18
Size: 5048 Color: 17

Bin 146: 24 of cap free
Amount of items: 2
Items: 
Size: 8040 Color: 11
Size: 7936 Color: 14

Bin 147: 24 of cap free
Amount of items: 3
Items: 
Size: 10946 Color: 18
Size: 4708 Color: 12
Size: 322 Color: 3

Bin 148: 24 of cap free
Amount of items: 3
Items: 
Size: 12685 Color: 1
Size: 2155 Color: 10
Size: 1136 Color: 8

Bin 149: 24 of cap free
Amount of items: 3
Items: 
Size: 13101 Color: 2
Size: 2763 Color: 17
Size: 112 Color: 8

Bin 150: 25 of cap free
Amount of items: 3
Items: 
Size: 10605 Color: 1
Size: 4938 Color: 17
Size: 432 Color: 3

Bin 151: 25 of cap free
Amount of items: 3
Items: 
Size: 11250 Color: 14
Size: 4497 Color: 2
Size: 228 Color: 7

Bin 152: 25 of cap free
Amount of items: 2
Items: 
Size: 13117 Color: 14
Size: 2858 Color: 8

Bin 153: 26 of cap free
Amount of items: 2
Items: 
Size: 13618 Color: 13
Size: 2356 Color: 5

Bin 154: 26 of cap free
Amount of items: 2
Items: 
Size: 13882 Color: 12
Size: 2092 Color: 14

Bin 155: 27 of cap free
Amount of items: 3
Items: 
Size: 11839 Color: 4
Size: 3446 Color: 12
Size: 688 Color: 8

Bin 156: 28 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 0
Size: 2584 Color: 9

Bin 157: 29 of cap free
Amount of items: 2
Items: 
Size: 13514 Color: 10
Size: 2457 Color: 6

Bin 158: 29 of cap free
Amount of items: 2
Items: 
Size: 13777 Color: 15
Size: 2194 Color: 9

Bin 159: 30 of cap free
Amount of items: 3
Items: 
Size: 10062 Color: 2
Size: 5700 Color: 9
Size: 208 Color: 4

Bin 160: 30 of cap free
Amount of items: 3
Items: 
Size: 13076 Color: 4
Size: 2754 Color: 19
Size: 140 Color: 7

Bin 161: 30 of cap free
Amount of items: 2
Items: 
Size: 14022 Color: 1
Size: 1948 Color: 19

Bin 162: 31 of cap free
Amount of items: 2
Items: 
Size: 13900 Color: 14
Size: 2069 Color: 6

Bin 163: 32 of cap free
Amount of items: 7
Items: 
Size: 8004 Color: 15
Size: 1490 Color: 6
Size: 1444 Color: 11
Size: 1386 Color: 17
Size: 1300 Color: 16
Size: 1192 Color: 6
Size: 1152 Color: 9

Bin 164: 32 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 14
Size: 5404 Color: 11
Size: 224 Color: 16

Bin 165: 32 of cap free
Amount of items: 2
Items: 
Size: 12246 Color: 10
Size: 3722 Color: 2

Bin 166: 33 of cap free
Amount of items: 4
Items: 
Size: 9960 Color: 14
Size: 2488 Color: 15
Size: 2403 Color: 14
Size: 1116 Color: 19

Bin 167: 34 of cap free
Amount of items: 2
Items: 
Size: 13714 Color: 9
Size: 2252 Color: 11

Bin 168: 34 of cap free
Amount of items: 2
Items: 
Size: 13774 Color: 13
Size: 2192 Color: 6

Bin 169: 35 of cap free
Amount of items: 3
Items: 
Size: 10356 Color: 15
Size: 5297 Color: 10
Size: 312 Color: 8

Bin 170: 35 of cap free
Amount of items: 2
Items: 
Size: 12283 Color: 7
Size: 3682 Color: 2

Bin 171: 36 of cap free
Amount of items: 4
Items: 
Size: 9988 Color: 4
Size: 2724 Color: 0
Size: 2708 Color: 19
Size: 544 Color: 2

Bin 172: 36 of cap free
Amount of items: 2
Items: 
Size: 10825 Color: 15
Size: 5139 Color: 8

Bin 173: 42 of cap free
Amount of items: 2
Items: 
Size: 11866 Color: 17
Size: 4092 Color: 12

Bin 174: 47 of cap free
Amount of items: 3
Items: 
Size: 11288 Color: 2
Size: 3921 Color: 7
Size: 744 Color: 3

Bin 175: 50 of cap free
Amount of items: 2
Items: 
Size: 9972 Color: 8
Size: 5978 Color: 0

Bin 176: 54 of cap free
Amount of items: 2
Items: 
Size: 9410 Color: 7
Size: 6536 Color: 8

Bin 177: 54 of cap free
Amount of items: 2
Items: 
Size: 11602 Color: 14
Size: 4344 Color: 16

Bin 178: 56 of cap free
Amount of items: 2
Items: 
Size: 13302 Color: 9
Size: 2642 Color: 5

Bin 179: 61 of cap free
Amount of items: 2
Items: 
Size: 12187 Color: 0
Size: 3752 Color: 10

Bin 180: 66 of cap free
Amount of items: 2
Items: 
Size: 12966 Color: 3
Size: 2968 Color: 16

Bin 181: 102 of cap free
Amount of items: 2
Items: 
Size: 11668 Color: 14
Size: 4230 Color: 12

Bin 182: 102 of cap free
Amount of items: 2
Items: 
Size: 12756 Color: 5
Size: 3142 Color: 2

Bin 183: 108 of cap free
Amount of items: 2
Items: 
Size: 11666 Color: 13
Size: 4226 Color: 18

Bin 184: 108 of cap free
Amount of items: 2
Items: 
Size: 12574 Color: 17
Size: 3318 Color: 10

Bin 185: 127 of cap free
Amount of items: 2
Items: 
Size: 9545 Color: 3
Size: 6328 Color: 8

Bin 186: 128 of cap free
Amount of items: 2
Items: 
Size: 9164 Color: 10
Size: 6708 Color: 14

Bin 187: 128 of cap free
Amount of items: 2
Items: 
Size: 10392 Color: 17
Size: 5480 Color: 4

Bin 188: 147 of cap free
Amount of items: 2
Items: 
Size: 8197 Color: 6
Size: 7656 Color: 10

Bin 189: 150 of cap free
Amount of items: 2
Items: 
Size: 10078 Color: 4
Size: 5772 Color: 9

Bin 190: 162 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 3
Size: 5418 Color: 16

Bin 191: 182 of cap free
Amount of items: 2
Items: 
Size: 9986 Color: 2
Size: 5832 Color: 3

Bin 192: 184 of cap free
Amount of items: 2
Items: 
Size: 11092 Color: 8
Size: 4724 Color: 9

Bin 193: 194 of cap free
Amount of items: 2
Items: 
Size: 10792 Color: 13
Size: 5014 Color: 6

Bin 194: 202 of cap free
Amount of items: 2
Items: 
Size: 11081 Color: 10
Size: 4717 Color: 4

Bin 195: 207 of cap free
Amount of items: 2
Items: 
Size: 10068 Color: 5
Size: 5725 Color: 2

Bin 196: 228 of cap free
Amount of items: 21
Items: 
Size: 864 Color: 15
Size: 850 Color: 9
Size: 844 Color: 16
Size: 824 Color: 19
Size: 818 Color: 19
Size: 816 Color: 14
Size: 816 Color: 9
Size: 788 Color: 16
Size: 782 Color: 7
Size: 760 Color: 13
Size: 752 Color: 9
Size: 736 Color: 2
Size: 732 Color: 8
Size: 732 Color: 1
Size: 720 Color: 15
Size: 720 Color: 8
Size: 710 Color: 9
Size: 696 Color: 1
Size: 692 Color: 19
Size: 568 Color: 3
Size: 552 Color: 3

Bin 197: 248 of cap free
Amount of items: 2
Items: 
Size: 9084 Color: 4
Size: 6668 Color: 8

Bin 198: 258 of cap free
Amount of items: 2
Items: 
Size: 9076 Color: 15
Size: 6666 Color: 4

Bin 199: 11034 of cap free
Amount of items: 15
Items: 
Size: 384 Color: 10
Size: 380 Color: 1
Size: 372 Color: 4
Size: 370 Color: 17
Size: 368 Color: 11
Size: 340 Color: 11
Size: 336 Color: 19
Size: 336 Color: 17
Size: 336 Color: 13
Size: 336 Color: 0
Size: 304 Color: 7
Size: 296 Color: 5
Size: 272 Color: 18
Size: 272 Color: 6
Size: 264 Color: 3

Total size: 3168000
Total free space: 16000

