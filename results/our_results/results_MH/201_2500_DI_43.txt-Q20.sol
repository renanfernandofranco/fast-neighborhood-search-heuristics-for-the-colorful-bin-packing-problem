Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 1696 Color: 6
Size: 568 Color: 11
Size: 88 Color: 10
Size: 48 Color: 11
Size: 48 Color: 4
Size: 8 Color: 2

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1940 Color: 9
Size: 232 Color: 12
Size: 156 Color: 15
Size: 112 Color: 6
Size: 16 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2012 Color: 2
Size: 372 Color: 11
Size: 72 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 976 Color: 18
Size: 916 Color: 17
Size: 564 Color: 7

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1780 Color: 8
Size: 512 Color: 4
Size: 132 Color: 4
Size: 32 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 2
Size: 620 Color: 15
Size: 120 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 17
Size: 324 Color: 12
Size: 16 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 8
Size: 542 Color: 18
Size: 104 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 13
Size: 176 Color: 8
Size: 88 Color: 16

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 14
Size: 756 Color: 12
Size: 144 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 17
Size: 538 Color: 15
Size: 8 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 10
Size: 790 Color: 11
Size: 8 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 5
Size: 684 Color: 4
Size: 128 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 6
Size: 1114 Color: 11
Size: 112 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 11
Size: 260 Color: 14
Size: 48 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 7
Size: 554 Color: 14
Size: 108 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 18
Size: 244 Color: 7
Size: 24 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 9
Size: 1020 Color: 13
Size: 200 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 0
Size: 470 Color: 13
Size: 92 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 9
Size: 300 Color: 18
Size: 56 Color: 7

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 10
Size: 364 Color: 1
Size: 64 Color: 8

Bin 22: 0 of cap free
Amount of items: 4
Items: 
Size: 1364 Color: 3
Size: 436 Color: 12
Size: 384 Color: 0
Size: 272 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 6
Size: 1022 Color: 9
Size: 200 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 12
Size: 212 Color: 15
Size: 40 Color: 7

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 13
Size: 244 Color: 19
Size: 40 Color: 5

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 8
Size: 460 Color: 2
Size: 88 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 7
Size: 541 Color: 6
Size: 108 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1926 Color: 4
Size: 442 Color: 7
Size: 88 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 6
Size: 262 Color: 0
Size: 48 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1919 Color: 8
Size: 449 Color: 13
Size: 88 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 6
Size: 1023 Color: 3
Size: 204 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 17
Size: 620 Color: 8
Size: 72 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 6
Size: 840 Color: 17
Size: 80 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 12
Size: 370 Color: 12
Size: 8 Color: 7

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 18
Size: 753 Color: 12
Size: 100 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 17
Size: 508 Color: 8
Size: 96 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1707 Color: 4
Size: 653 Color: 7
Size: 96 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 19
Size: 654 Color: 9
Size: 128 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 12
Size: 467 Color: 15
Size: 92 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1526 Color: 0
Size: 778 Color: 4
Size: 152 Color: 12

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 1
Size: 828 Color: 10
Size: 160 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 15
Size: 666 Color: 14
Size: 280 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1437 Color: 16
Size: 851 Color: 16
Size: 168 Color: 5

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 12
Size: 1021 Color: 5
Size: 204 Color: 11

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 10
Size: 610 Color: 9
Size: 64 Color: 8

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 11
Size: 938 Color: 10
Size: 184 Color: 14

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 10
Size: 719 Color: 14
Size: 142 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 8
Size: 222 Color: 18
Size: 44 Color: 7

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2082 Color: 5
Size: 362 Color: 16
Size: 12 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1911 Color: 1
Size: 455 Color: 15
Size: 90 Color: 6

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2081 Color: 14
Size: 313 Color: 16
Size: 62 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 9
Size: 862 Color: 9
Size: 168 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 3
Size: 386 Color: 11
Size: 76 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 2002 Color: 6
Size: 382 Color: 18
Size: 72 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 2145 Color: 13
Size: 261 Color: 7
Size: 50 Color: 14

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 2089 Color: 11
Size: 307 Color: 8
Size: 60 Color: 14

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2139 Color: 13
Size: 265 Color: 15
Size: 52 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 15
Size: 555 Color: 3
Size: 102 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2015 Color: 2
Size: 369 Color: 5
Size: 72 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 16
Size: 306 Color: 14
Size: 60 Color: 7

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1429 Color: 12
Size: 857 Color: 4
Size: 170 Color: 10

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 2
Size: 631 Color: 9
Size: 126 Color: 15

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2011 Color: 9
Size: 371 Color: 16
Size: 74 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2003 Color: 3
Size: 379 Color: 5
Size: 74 Color: 15

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 1
Size: 383 Color: 15
Size: 76 Color: 5

Total size: 159640
Total free space: 0

