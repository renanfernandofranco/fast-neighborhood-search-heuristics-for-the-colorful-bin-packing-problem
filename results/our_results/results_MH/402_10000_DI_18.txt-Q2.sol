Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 31
Items: 
Size: 408 Color: 1
Size: 404 Color: 1
Size: 360 Color: 1
Size: 356 Color: 1
Size: 328 Color: 1
Size: 328 Color: 1
Size: 304 Color: 1
Size: 292 Color: 1
Size: 272 Color: 1
Size: 272 Color: 1
Size: 272 Color: 0
Size: 270 Color: 0
Size: 256 Color: 0
Size: 248 Color: 1
Size: 248 Color: 0
Size: 248 Color: 0
Size: 240 Color: 1
Size: 240 Color: 1
Size: 240 Color: 0
Size: 232 Color: 1
Size: 232 Color: 0
Size: 228 Color: 0
Size: 222 Color: 0
Size: 220 Color: 0
Size: 216 Color: 0
Size: 216 Color: 0
Size: 212 Color: 0
Size: 200 Color: 1
Size: 200 Color: 0
Size: 200 Color: 0
Size: 172 Color: 0

Bin 2: 0 of cap free
Amount of items: 19
Items: 
Size: 640 Color: 1
Size: 608 Color: 1
Size: 608 Color: 1
Size: 590 Color: 1
Size: 576 Color: 1
Size: 536 Color: 1
Size: 528 Color: 1
Size: 520 Color: 1
Size: 448 Color: 0
Size: 408 Color: 1
Size: 384 Color: 0
Size: 332 Color: 0
Size: 322 Color: 1
Size: 292 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 280 Color: 0
Size: 280 Color: 0
Size: 208 Color: 0

Bin 3: 0 of cap free
Amount of items: 9
Items: 
Size: 4069 Color: 1
Size: 759 Color: 1
Size: 676 Color: 1
Size: 676 Color: 1
Size: 456 Color: 0
Size: 452 Color: 0
Size: 448 Color: 0
Size: 320 Color: 1
Size: 280 Color: 0

Bin 4: 0 of cap free
Amount of items: 7
Items: 
Size: 4071 Color: 1
Size: 1145 Color: 1
Size: 1006 Color: 1
Size: 656 Color: 0
Size: 588 Color: 0
Size: 518 Color: 1
Size: 152 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4462 Color: 1
Size: 3222 Color: 0
Size: 452 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 0
Size: 3062 Color: 0
Size: 158 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5388 Color: 1
Size: 2412 Color: 0
Size: 336 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5385 Color: 0
Size: 2595 Color: 1
Size: 156 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 1
Size: 1300 Color: 0
Size: 152 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 1
Size: 702 Color: 1
Size: 612 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 1
Size: 1188 Color: 1
Size: 88 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 1
Size: 772 Color: 1
Size: 448 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6930 Color: 0
Size: 938 Color: 0
Size: 268 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 0
Size: 724 Color: 1
Size: 392 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7167 Color: 1
Size: 785 Color: 1
Size: 184 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 1
Size: 698 Color: 1
Size: 264 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7209 Color: 0
Size: 791 Color: 1
Size: 136 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7264 Color: 0
Size: 672 Color: 1
Size: 200 Color: 0

Bin 19: 1 of cap free
Amount of items: 9
Items: 
Size: 4070 Color: 1
Size: 779 Color: 1
Size: 778 Color: 1
Size: 774 Color: 1
Size: 512 Color: 0
Size: 456 Color: 0
Size: 456 Color: 0
Size: 182 Color: 1
Size: 128 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 5687 Color: 0
Size: 2060 Color: 0
Size: 388 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 5693 Color: 1
Size: 2242 Color: 1
Size: 200 Color: 0

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 6094 Color: 0
Size: 2041 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 1
Size: 1465 Color: 1
Size: 150 Color: 0

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 6517 Color: 0
Size: 1468 Color: 1
Size: 150 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 6935 Color: 1
Size: 676 Color: 1
Size: 524 Color: 0

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 4962 Color: 0
Size: 3052 Color: 0
Size: 120 Color: 1

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 0
Size: 2596 Color: 1
Size: 294 Color: 1

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 0
Size: 1986 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 1
Size: 1628 Color: 1
Size: 136 Color: 0

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 1
Size: 1302 Color: 0
Size: 332 Color: 0

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 1
Size: 1068 Color: 1
Size: 516 Color: 0

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 7014 Color: 1
Size: 1120 Color: 0

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 7132 Color: 0
Size: 1002 Color: 1

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 4589 Color: 1
Size: 3204 Color: 0
Size: 340 Color: 1

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 4998 Color: 1
Size: 2951 Color: 1
Size: 184 Color: 0

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 5393 Color: 1
Size: 2260 Color: 0
Size: 480 Color: 1

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 6689 Color: 0
Size: 1444 Color: 1

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 6926 Color: 1
Size: 1207 Color: 0

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 7035 Color: 1
Size: 1098 Color: 0

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 0
Size: 3576 Color: 0
Size: 264 Color: 1

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 4982 Color: 0
Size: 2630 Color: 1
Size: 520 Color: 1

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 6199 Color: 0
Size: 1182 Color: 1
Size: 751 Color: 0

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 6207 Color: 1
Size: 1781 Color: 1
Size: 144 Color: 0

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 1
Size: 932 Color: 0
Size: 788 Color: 1

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 6781 Color: 0
Size: 1351 Color: 1

Bin 46: 4 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 1
Size: 760 Color: 0
Size: 496 Color: 0

Bin 47: 4 of cap free
Amount of items: 2
Items: 
Size: 7221 Color: 1
Size: 911 Color: 0

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 7302 Color: 0
Size: 830 Color: 1

Bin 49: 5 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 0
Size: 3391 Color: 0
Size: 264 Color: 1

Bin 50: 5 of cap free
Amount of items: 3
Items: 
Size: 4597 Color: 1
Size: 3382 Color: 1
Size: 152 Color: 0

Bin 51: 5 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 0
Size: 1244 Color: 1
Size: 1219 Color: 1

Bin 52: 5 of cap free
Amount of items: 3
Items: 
Size: 5949 Color: 0
Size: 2022 Color: 1
Size: 160 Color: 1

Bin 53: 5 of cap free
Amount of items: 2
Items: 
Size: 6516 Color: 1
Size: 1615 Color: 0

Bin 54: 6 of cap free
Amount of items: 3
Items: 
Size: 4446 Color: 0
Size: 3040 Color: 0
Size: 644 Color: 1

Bin 55: 6 of cap free
Amount of items: 3
Items: 
Size: 5394 Color: 1
Size: 2568 Color: 0
Size: 168 Color: 1

Bin 56: 6 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 0
Size: 1670 Color: 1

Bin 57: 6 of cap free
Amount of items: 2
Items: 
Size: 7013 Color: 0
Size: 1117 Color: 1

Bin 58: 6 of cap free
Amount of items: 2
Items: 
Size: 7196 Color: 0
Size: 934 Color: 1

Bin 59: 6 of cap free
Amount of items: 3
Items: 
Size: 7294 Color: 1
Size: 788 Color: 0
Size: 48 Color: 1

Bin 60: 7 of cap free
Amount of items: 3
Items: 
Size: 7227 Color: 0
Size: 862 Color: 1
Size: 40 Color: 1

Bin 61: 8 of cap free
Amount of items: 3
Items: 
Size: 6702 Color: 1
Size: 1322 Color: 0
Size: 104 Color: 0

Bin 62: 8 of cap free
Amount of items: 2
Items: 
Size: 6797 Color: 0
Size: 1331 Color: 1

Bin 63: 8 of cap free
Amount of items: 2
Items: 
Size: 7101 Color: 1
Size: 1027 Color: 0

Bin 64: 9 of cap free
Amount of items: 6
Items: 
Size: 4076 Color: 1
Size: 1241 Color: 1
Size: 1218 Color: 1
Size: 728 Color: 0
Size: 676 Color: 0
Size: 188 Color: 0

Bin 65: 9 of cap free
Amount of items: 3
Items: 
Size: 5701 Color: 0
Size: 2286 Color: 1
Size: 140 Color: 0

Bin 66: 9 of cap free
Amount of items: 2
Items: 
Size: 6090 Color: 1
Size: 2037 Color: 0

Bin 67: 9 of cap free
Amount of items: 2
Items: 
Size: 7075 Color: 1
Size: 1052 Color: 0

Bin 68: 9 of cap free
Amount of items: 2
Items: 
Size: 7117 Color: 1
Size: 1010 Color: 0

Bin 69: 9 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 0
Size: 945 Color: 1
Size: 40 Color: 0

Bin 70: 9 of cap free
Amount of items: 2
Items: 
Size: 7276 Color: 1
Size: 851 Color: 0

Bin 71: 10 of cap free
Amount of items: 2
Items: 
Size: 7106 Color: 0
Size: 1020 Color: 1

Bin 72: 11 of cap free
Amount of items: 2
Items: 
Size: 6302 Color: 1
Size: 1823 Color: 0

Bin 73: 11 of cap free
Amount of items: 2
Items: 
Size: 7206 Color: 0
Size: 919 Color: 1

Bin 74: 12 of cap free
Amount of items: 3
Items: 
Size: 5023 Color: 0
Size: 2957 Color: 0
Size: 144 Color: 1

Bin 75: 12 of cap free
Amount of items: 2
Items: 
Size: 6402 Color: 1
Size: 1722 Color: 0

Bin 76: 12 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 0
Size: 1520 Color: 1

Bin 77: 12 of cap free
Amount of items: 2
Items: 
Size: 6927 Color: 0
Size: 1197 Color: 1

Bin 78: 13 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 1
Size: 1651 Color: 0
Size: 364 Color: 1

Bin 79: 13 of cap free
Amount of items: 2
Items: 
Size: 7052 Color: 0
Size: 1071 Color: 1

Bin 80: 14 of cap free
Amount of items: 3
Items: 
Size: 6853 Color: 1
Size: 863 Color: 0
Size: 406 Color: 0

Bin 81: 15 of cap free
Amount of items: 2
Items: 
Size: 6157 Color: 0
Size: 1964 Color: 1

Bin 82: 15 of cap free
Amount of items: 2
Items: 
Size: 6837 Color: 0
Size: 1284 Color: 1

Bin 83: 15 of cap free
Amount of items: 2
Items: 
Size: 6909 Color: 1
Size: 1212 Color: 0

Bin 84: 16 of cap free
Amount of items: 3
Items: 
Size: 4270 Color: 1
Size: 3238 Color: 0
Size: 612 Color: 1

Bin 85: 16 of cap free
Amount of items: 2
Items: 
Size: 5220 Color: 1
Size: 2900 Color: 0

Bin 86: 16 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 0
Size: 1930 Color: 1
Size: 80 Color: 0

Bin 87: 16 of cap free
Amount of items: 2
Items: 
Size: 6716 Color: 0
Size: 1404 Color: 1

Bin 88: 16 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 1
Size: 1364 Color: 0

Bin 89: 16 of cap free
Amount of items: 2
Items: 
Size: 6763 Color: 0
Size: 1357 Color: 1

Bin 90: 16 of cap free
Amount of items: 2
Items: 
Size: 7235 Color: 1
Size: 885 Color: 0

Bin 91: 17 of cap free
Amount of items: 3
Items: 
Size: 4086 Color: 1
Size: 3389 Color: 0
Size: 644 Color: 1

Bin 92: 17 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 1
Size: 1660 Color: 0
Size: 80 Color: 0

Bin 93: 17 of cap free
Amount of items: 4
Items: 
Size: 7195 Color: 0
Size: 844 Color: 1
Size: 48 Color: 0
Size: 32 Color: 1

Bin 94: 18 of cap free
Amount of items: 2
Items: 
Size: 6509 Color: 1
Size: 1609 Color: 0

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 6647 Color: 0
Size: 1471 Color: 1

Bin 96: 18 of cap free
Amount of items: 2
Items: 
Size: 6644 Color: 1
Size: 1474 Color: 0

Bin 97: 18 of cap free
Amount of items: 2
Items: 
Size: 7043 Color: 1
Size: 1075 Color: 0

Bin 98: 18 of cap free
Amount of items: 2
Items: 
Size: 7210 Color: 1
Size: 908 Color: 0

Bin 99: 21 of cap free
Amount of items: 2
Items: 
Size: 5822 Color: 1
Size: 2293 Color: 0

Bin 100: 21 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 1
Size: 1023 Color: 0

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 1
Size: 1846 Color: 0

Bin 102: 23 of cap free
Amount of items: 2
Items: 
Size: 7003 Color: 0
Size: 1110 Color: 1

Bin 103: 26 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 0
Size: 1008 Color: 1

Bin 104: 26 of cap free
Amount of items: 2
Items: 
Size: 7236 Color: 0
Size: 874 Color: 1

Bin 105: 27 of cap free
Amount of items: 2
Items: 
Size: 5031 Color: 1
Size: 3078 Color: 0

Bin 106: 27 of cap free
Amount of items: 2
Items: 
Size: 7300 Color: 1
Size: 809 Color: 0

Bin 107: 28 of cap free
Amount of items: 2
Items: 
Size: 6578 Color: 1
Size: 1530 Color: 0

Bin 108: 28 of cap free
Amount of items: 2
Items: 
Size: 7232 Color: 1
Size: 876 Color: 0

Bin 109: 34 of cap free
Amount of items: 2
Items: 
Size: 4078 Color: 1
Size: 4024 Color: 0

Bin 110: 34 of cap free
Amount of items: 3
Items: 
Size: 4254 Color: 0
Size: 3390 Color: 0
Size: 458 Color: 1

Bin 111: 37 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 1
Size: 1001 Color: 0
Size: 80 Color: 1

Bin 112: 38 of cap free
Amount of items: 2
Items: 
Size: 5820 Color: 1
Size: 2278 Color: 0

Bin 113: 44 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 0
Size: 2636 Color: 1
Size: 384 Color: 0

Bin 114: 44 of cap free
Amount of items: 2
Items: 
Size: 5446 Color: 1
Size: 2646 Color: 0

Bin 115: 44 of cap free
Amount of items: 2
Items: 
Size: 6142 Color: 1
Size: 1950 Color: 0

Bin 116: 46 of cap free
Amount of items: 2
Items: 
Size: 5406 Color: 0
Size: 2684 Color: 1

Bin 117: 46 of cap free
Amount of items: 2
Items: 
Size: 5798 Color: 0
Size: 2292 Color: 1

Bin 118: 54 of cap free
Amount of items: 2
Items: 
Size: 6380 Color: 1
Size: 1702 Color: 0

Bin 119: 58 of cap free
Amount of items: 2
Items: 
Size: 6722 Color: 1
Size: 1356 Color: 0

Bin 120: 58 of cap free
Amount of items: 2
Items: 
Size: 6806 Color: 0
Size: 1272 Color: 1

Bin 121: 61 of cap free
Amount of items: 2
Items: 
Size: 6629 Color: 1
Size: 1446 Color: 0

Bin 122: 66 of cap free
Amount of items: 2
Items: 
Size: 5028 Color: 0
Size: 3042 Color: 1

Bin 123: 69 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 0
Size: 2287 Color: 1

Bin 124: 70 of cap free
Amount of items: 2
Items: 
Size: 6134 Color: 0
Size: 1932 Color: 1

Bin 125: 72 of cap free
Amount of items: 2
Items: 
Size: 4294 Color: 0
Size: 3770 Color: 1

Bin 126: 72 of cap free
Amount of items: 2
Items: 
Size: 4486 Color: 0
Size: 3578 Color: 1

Bin 127: 73 of cap free
Amount of items: 2
Items: 
Size: 6371 Color: 0
Size: 1692 Color: 1

Bin 128: 88 of cap free
Amount of items: 2
Items: 
Size: 4660 Color: 0
Size: 3388 Color: 1

Bin 129: 90 of cap free
Amount of items: 2
Items: 
Size: 5428 Color: 1
Size: 2618 Color: 0

Bin 130: 92 of cap free
Amount of items: 2
Items: 
Size: 5782 Color: 1
Size: 2262 Color: 0

Bin 131: 106 of cap free
Amount of items: 2
Items: 
Size: 5999 Color: 0
Size: 2031 Color: 1

Bin 132: 125 of cap free
Amount of items: 2
Items: 
Size: 5422 Color: 1
Size: 2589 Color: 0

Bin 133: 5716 of cap free
Amount of items: 14
Items: 
Size: 208 Color: 1
Size: 204 Color: 1
Size: 184 Color: 1
Size: 182 Color: 1
Size: 182 Color: 0
Size: 176 Color: 1
Size: 176 Color: 0
Size: 176 Color: 0
Size: 168 Color: 1
Size: 168 Color: 0
Size: 164 Color: 0
Size: 160 Color: 0
Size: 136 Color: 1
Size: 136 Color: 0

Total size: 1073952
Total free space: 8136

