Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1536 Color: 150
Size: 620 Color: 114
Size: 204 Color: 68
Size: 88 Color: 30
Size: 8 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1696 Color: 158
Size: 568 Color: 111
Size: 88 Color: 35
Size: 56 Color: 19
Size: 48 Color: 12

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1693 Color: 157
Size: 539 Color: 108
Size: 120 Color: 47
Size: 88 Color: 33
Size: 16 Color: 3

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1910 Color: 174
Size: 272 Color: 77
Size: 226 Color: 69
Size: 48 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 139
Size: 840 Color: 126
Size: 386 Color: 89

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1827 Color: 168
Size: 525 Color: 106
Size: 104 Color: 41

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 148
Size: 828 Color: 125
Size: 160 Color: 60

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 189
Size: 330 Color: 83
Size: 64 Color: 21

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 165
Size: 542 Color: 109
Size: 104 Color: 42

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 142
Size: 1018 Color: 133
Size: 200 Color: 64

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 153
Size: 730 Color: 121
Size: 144 Color: 56

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 166
Size: 637 Color: 117
Size: 8 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 163
Size: 580 Color: 112
Size: 112 Color: 46

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2111 Color: 192
Size: 289 Color: 80
Size: 56 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 171
Size: 470 Color: 102
Size: 92 Color: 39

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 185
Size: 404 Color: 93
Size: 24 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 162
Size: 618 Color: 113
Size: 120 Color: 48

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 141
Size: 1020 Color: 134
Size: 200 Color: 65

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1924 Color: 176
Size: 444 Color: 96
Size: 88 Color: 34

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 156
Size: 654 Color: 118
Size: 128 Color: 53

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 144
Size: 887 Color: 129
Size: 148 Color: 58

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 201
Size: 128 Color: 52
Size: 126 Color: 51

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 190
Size: 280 Color: 78
Size: 76 Color: 25

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 196
Size: 253 Color: 74
Size: 50 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 159
Size: 633 Color: 116
Size: 126 Color: 50

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 195
Size: 260 Color: 75
Size: 48 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2002 Color: 183
Size: 348 Color: 86
Size: 106 Color: 44

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 172
Size: 460 Color: 101
Size: 88 Color: 31

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 199
Size: 228 Color: 71
Size: 40 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 200
Size: 232 Color: 72
Size: 32 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 145
Size: 861 Color: 127
Size: 172 Color: 62

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 154
Size: 729 Color: 120
Size: 144 Color: 55

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 194
Size: 262 Color: 76
Size: 48 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 179
Size: 402 Color: 92
Size: 76 Color: 27

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 181
Size: 398 Color: 91
Size: 64 Color: 22

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 188
Size: 331 Color: 84
Size: 64 Color: 20

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 164
Size: 554 Color: 110
Size: 108 Color: 45

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 149
Size: 774 Color: 124
Size: 152 Color: 59

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 193
Size: 282 Color: 79
Size: 56 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 197
Size: 242 Color: 73
Size: 48 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 178
Size: 436 Color: 94
Size: 80 Color: 28

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 151
Size: 756 Color: 123
Size: 144 Color: 54

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1991 Color: 180
Size: 389 Color: 90
Size: 76 Color: 26

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 167
Size: 535 Color: 107
Size: 106 Color: 43

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 184
Size: 449 Color: 97
Size: 2 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 138
Size: 1023 Color: 137
Size: 204 Color: 67

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 143
Size: 916 Color: 130
Size: 176 Color: 63

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1913 Color: 175
Size: 453 Color: 98
Size: 90 Color: 38

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 187
Size: 342 Color: 85
Size: 64 Color: 23

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 182
Size: 381 Color: 87
Size: 74 Color: 24

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1022 Color: 136
Size: 976 Color: 132
Size: 458 Color: 100

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 198
Size: 226 Color: 70
Size: 44 Color: 9

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 177
Size: 443 Color: 95
Size: 88 Color: 32

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 161
Size: 684 Color: 119
Size: 56 Color: 16

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 160
Size: 631 Color: 115
Size: 124 Color: 49

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 191
Size: 330 Color: 82
Size: 20 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1557 Color: 152
Size: 751 Color: 122
Size: 148 Color: 57

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 146
Size: 866 Color: 128
Size: 164 Color: 61

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2044 Color: 186
Size: 384 Color: 88
Size: 28 Color: 6

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 155
Size: 512 Color: 104
Size: 300 Color: 81

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 140
Size: 1021 Color: 135
Size: 204 Color: 66

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 170
Size: 476 Color: 103
Size: 88 Color: 36

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 169
Size: 524 Color: 105
Size: 80 Color: 29

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 147
Size: 918 Color: 131
Size: 96 Color: 40

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1909 Color: 173
Size: 457 Color: 99
Size: 90 Color: 37

Total size: 159640
Total free space: 0

