Capicity Bin: 2472
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1000 Color: 0
Size: 840 Color: 4
Size: 504 Color: 3
Size: 64 Color: 2
Size: 64 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 2
Size: 810 Color: 0
Size: 160 Color: 2

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1704 Color: 4
Size: 384 Color: 2
Size: 272 Color: 1
Size: 112 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 2030 Color: 0
Size: 370 Color: 4
Size: 64 Color: 2
Size: 8 Color: 3

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 2218 Color: 0
Size: 214 Color: 0
Size: 32 Color: 1
Size: 8 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 1
Size: 364 Color: 4
Size: 40 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 2
Size: 436 Color: 0
Size: 80 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 3
Size: 284 Color: 1
Size: 48 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 1
Size: 948 Color: 4
Size: 120 Color: 0

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 1799 Color: 3
Size: 561 Color: 1
Size: 96 Color: 3
Size: 16 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2105 Color: 1
Size: 307 Color: 1
Size: 60 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 3
Size: 662 Color: 0
Size: 132 Color: 0

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 1552 Color: 4
Size: 576 Color: 2
Size: 272 Color: 0
Size: 72 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 4
Size: 439 Color: 2
Size: 86 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2013 Color: 3
Size: 383 Color: 2
Size: 76 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 2
Size: 645 Color: 1
Size: 128 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 3
Size: 686 Color: 0
Size: 136 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 1
Size: 226 Color: 3
Size: 44 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 0
Size: 546 Color: 0
Size: 108 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2142 Color: 1
Size: 278 Color: 1
Size: 52 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 3
Size: 476 Color: 3
Size: 88 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1708 Color: 3
Size: 644 Color: 0
Size: 120 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 3
Size: 1022 Color: 4
Size: 204 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 0
Size: 658 Color: 3
Size: 128 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 2
Size: 747 Color: 0
Size: 148 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1691 Color: 1
Size: 651 Color: 1
Size: 130 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2097 Color: 3
Size: 349 Color: 3
Size: 26 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 4
Size: 881 Color: 2
Size: 176 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 0
Size: 297 Color: 4
Size: 22 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2035 Color: 3
Size: 365 Color: 3
Size: 72 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1788 Color: 0
Size: 572 Color: 1
Size: 112 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 2
Size: 244 Color: 2
Size: 24 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 2
Size: 1031 Color: 1
Size: 204 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1604 Color: 0
Size: 724 Color: 3
Size: 144 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1302 Color: 1
Size: 978 Color: 0
Size: 192 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 3
Size: 425 Color: 4
Size: 84 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 3
Size: 329 Color: 1
Size: 64 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 1
Size: 318 Color: 0
Size: 36 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 3
Size: 302 Color: 1
Size: 60 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1484 Color: 1
Size: 828 Color: 4
Size: 160 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 4
Size: 258 Color: 3
Size: 48 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1482 Color: 1
Size: 830 Color: 4
Size: 160 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 0
Size: 542 Color: 0
Size: 104 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 3
Size: 1054 Color: 1
Size: 180 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1413 Color: 0
Size: 883 Color: 2
Size: 176 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 4
Size: 431 Color: 3
Size: 86 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 4
Size: 450 Color: 1
Size: 88 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 1
Size: 371 Color: 0
Size: 74 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 1
Size: 446 Color: 3
Size: 88 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 0
Size: 670 Color: 1
Size: 132 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 0
Size: 753 Color: 2
Size: 150 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1278 Color: 4
Size: 998 Color: 0
Size: 196 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 0
Size: 1028 Color: 4
Size: 200 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1885 Color: 2
Size: 491 Color: 1
Size: 96 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 0
Size: 567 Color: 3
Size: 112 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 2137 Color: 1
Size: 281 Color: 3
Size: 54 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2044 Color: 1
Size: 340 Color: 0
Size: 88 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 2145 Color: 4
Size: 273 Color: 1
Size: 54 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 0
Size: 1029 Color: 4
Size: 204 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 2
Size: 366 Color: 1
Size: 72 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1860 Color: 2
Size: 596 Color: 3
Size: 16 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1877 Color: 0
Size: 497 Color: 0
Size: 98 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 0
Size: 761 Color: 0
Size: 150 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2019 Color: 0
Size: 379 Color: 2
Size: 74 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 0
Size: 276 Color: 4
Size: 48 Color: 2

Total size: 160680
Total free space: 0

