Capicity Bin: 1000001
Lower Bound: 901

Bins used: 901
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 495914 Color: 13
Size: 344878 Color: 19
Size: 159209 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 588606 Color: 0
Size: 251163 Color: 3
Size: 160232 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 662247 Color: 10
Size: 215089 Color: 1
Size: 122665 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 700543 Color: 12
Size: 192916 Color: 7
Size: 106542 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 459390 Color: 17
Size: 331516 Color: 11
Size: 209095 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 747303 Color: 5
Size: 143692 Color: 4
Size: 109006 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 719809 Color: 6
Size: 158256 Color: 12
Size: 121936 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 457480 Color: 2
Size: 390190 Color: 7
Size: 152331 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 712903 Color: 2
Size: 153247 Color: 5
Size: 133851 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 738738 Color: 17
Size: 146461 Color: 17
Size: 114802 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 600010 Color: 0
Size: 237791 Color: 10
Size: 162200 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 442297 Color: 14
Size: 279135 Color: 17
Size: 278569 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 745927 Color: 16
Size: 136263 Color: 0
Size: 117811 Color: 17

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 480218 Color: 2
Size: 345585 Color: 11
Size: 174198 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 471272 Color: 10
Size: 396738 Color: 11
Size: 131991 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 601335 Color: 19
Size: 289132 Color: 3
Size: 109534 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 684753 Color: 4
Size: 187538 Color: 10
Size: 127710 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 474563 Color: 19
Size: 366976 Color: 3
Size: 158462 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 728601 Color: 16
Size: 158691 Color: 11
Size: 112709 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 531769 Color: 4
Size: 346387 Color: 6
Size: 121845 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 435055 Color: 7
Size: 372584 Color: 5
Size: 192362 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 769702 Color: 6
Size: 122223 Color: 2
Size: 108076 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 774698 Color: 12
Size: 123303 Color: 3
Size: 102000 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 492172 Color: 17
Size: 394866 Color: 2
Size: 112963 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 580052 Color: 4
Size: 290627 Color: 3
Size: 129322 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 575865 Color: 12
Size: 265783 Color: 6
Size: 158353 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 601508 Color: 8
Size: 215209 Color: 5
Size: 183284 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 632631 Color: 0
Size: 191100 Color: 12
Size: 176270 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 621679 Color: 5
Size: 189309 Color: 19
Size: 189013 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 639220 Color: 1
Size: 185326 Color: 2
Size: 175455 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 457736 Color: 5
Size: 358753 Color: 10
Size: 183512 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 722163 Color: 4
Size: 148309 Color: 7
Size: 129529 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 509275 Color: 17
Size: 339650 Color: 8
Size: 151076 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 671051 Color: 7
Size: 197594 Color: 6
Size: 131356 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 742416 Color: 13
Size: 134245 Color: 18
Size: 123340 Color: 17

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 721685 Color: 8
Size: 139942 Color: 0
Size: 138374 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 719786 Color: 17
Size: 158546 Color: 3
Size: 121669 Color: 12

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 745750 Color: 17
Size: 148147 Color: 13
Size: 106104 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 704321 Color: 14
Size: 157572 Color: 3
Size: 138108 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 598197 Color: 6
Size: 277703 Color: 1
Size: 124101 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 751157 Color: 7
Size: 132576 Color: 16
Size: 116268 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 723869 Color: 18
Size: 150193 Color: 9
Size: 125939 Color: 11

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 700714 Color: 6
Size: 195901 Color: 7
Size: 103386 Color: 11

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 729300 Color: 1
Size: 136434 Color: 2
Size: 134267 Color: 18

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 581904 Color: 0
Size: 272064 Color: 4
Size: 146032 Color: 18

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 723382 Color: 2
Size: 144519 Color: 16
Size: 132099 Color: 10

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 663030 Color: 13
Size: 192765 Color: 6
Size: 144205 Color: 13

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 457743 Color: 11
Size: 349515 Color: 19
Size: 192742 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 697295 Color: 10
Size: 153475 Color: 2
Size: 149230 Color: 6

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 453997 Color: 3
Size: 290052 Color: 19
Size: 255951 Color: 11

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 661307 Color: 15
Size: 192058 Color: 2
Size: 146635 Color: 5

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 741515 Color: 14
Size: 129930 Color: 8
Size: 128555 Color: 0

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 457893 Color: 11
Size: 394013 Color: 3
Size: 148094 Color: 15

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 716816 Color: 6
Size: 144345 Color: 5
Size: 138839 Color: 4

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 612036 Color: 5
Size: 197443 Color: 11
Size: 190521 Color: 13

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 344266 Color: 15
Size: 342817 Color: 8
Size: 312917 Color: 3

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 640007 Color: 6
Size: 181940 Color: 9
Size: 178053 Color: 12

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 660846 Color: 16
Size: 187412 Color: 4
Size: 151742 Color: 5

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 454237 Color: 2
Size: 361255 Color: 5
Size: 184508 Color: 8

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 402830 Color: 3
Size: 312771 Color: 12
Size: 284399 Color: 15

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 677969 Color: 10
Size: 164832 Color: 11
Size: 157199 Color: 16

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 690268 Color: 3
Size: 173645 Color: 0
Size: 136087 Color: 4

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 548010 Color: 13
Size: 451990 Color: 11

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 597246 Color: 6
Size: 402754 Color: 2

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 636780 Color: 12
Size: 182785 Color: 0
Size: 180435 Color: 7

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 720408 Color: 3
Size: 140097 Color: 0
Size: 139494 Color: 14

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 709611 Color: 5
Size: 146141 Color: 17
Size: 144247 Color: 7

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 391680 Color: 15
Size: 316455 Color: 18
Size: 291864 Color: 17

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 472202 Color: 10
Size: 337472 Color: 16
Size: 190325 Color: 16

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 728694 Color: 12
Size: 137587 Color: 2
Size: 133718 Color: 14

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 661533 Color: 14
Size: 180255 Color: 2
Size: 158211 Color: 7

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 645756 Color: 11
Size: 178930 Color: 17
Size: 175313 Color: 10

Bin 73: 2 of cap free
Amount of items: 4
Items: 
Size: 425833 Color: 6
Size: 283408 Color: 7
Size: 155272 Color: 11
Size: 135486 Color: 0

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 548309 Color: 17
Size: 260609 Color: 2
Size: 191081 Color: 16

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 367618 Color: 16
Size: 316386 Color: 8
Size: 315995 Color: 2

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 636956 Color: 1
Size: 196568 Color: 6
Size: 166475 Color: 0

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 674119 Color: 15
Size: 183640 Color: 18
Size: 142240 Color: 1

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 738682 Color: 18
Size: 152993 Color: 7
Size: 108324 Color: 15

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 404235 Color: 10
Size: 316720 Color: 9
Size: 279044 Color: 10

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 649811 Color: 6
Size: 211152 Color: 18
Size: 139035 Color: 3

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 778735 Color: 9
Size: 112668 Color: 18
Size: 108595 Color: 18

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 757739 Color: 15
Size: 122673 Color: 0
Size: 119586 Color: 6

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 494348 Color: 6
Size: 329035 Color: 8
Size: 176615 Color: 2

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 546991 Color: 1
Size: 313530 Color: 5
Size: 139477 Color: 8

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 557728 Color: 12
Size: 306461 Color: 2
Size: 135809 Color: 18

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 765609 Color: 7
Size: 234389 Color: 9

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 744873 Color: 14
Size: 255125 Color: 7

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 791755 Color: 13
Size: 208243 Color: 17

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 634889 Color: 15
Size: 194015 Color: 8
Size: 171093 Color: 9

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 736376 Color: 5
Size: 136999 Color: 15
Size: 126622 Color: 3

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 547102 Color: 17
Size: 452895 Color: 6

Bin 92: 5 of cap free
Amount of items: 3
Items: 
Size: 774121 Color: 19
Size: 115562 Color: 13
Size: 110313 Color: 1

Bin 93: 5 of cap free
Amount of items: 3
Items: 
Size: 701700 Color: 4
Size: 153468 Color: 10
Size: 144828 Color: 3

Bin 94: 5 of cap free
Amount of items: 3
Items: 
Size: 616390 Color: 18
Size: 196624 Color: 2
Size: 186982 Color: 4

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 571544 Color: 16
Size: 428452 Color: 11

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 666114 Color: 11
Size: 333882 Color: 13

Bin 97: 6 of cap free
Amount of items: 3
Items: 
Size: 559163 Color: 14
Size: 316640 Color: 5
Size: 124192 Color: 11

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 580216 Color: 1
Size: 310276 Color: 1
Size: 109503 Color: 19

Bin 99: 6 of cap free
Amount of items: 3
Items: 
Size: 716785 Color: 5
Size: 152456 Color: 13
Size: 130754 Color: 13

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 459086 Color: 10
Size: 354489 Color: 1
Size: 186420 Color: 4

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 690344 Color: 15
Size: 190526 Color: 5
Size: 119125 Color: 4

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 754143 Color: 9
Size: 245852 Color: 15

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 749903 Color: 13
Size: 250092 Color: 15

Bin 104: 7 of cap free
Amount of items: 3
Items: 
Size: 580062 Color: 18
Size: 265154 Color: 4
Size: 154778 Color: 18

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 549283 Color: 12
Size: 450711 Color: 15

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 627508 Color: 3
Size: 372486 Color: 15

Bin 107: 8 of cap free
Amount of items: 3
Items: 
Size: 676265 Color: 19
Size: 175721 Color: 7
Size: 148007 Color: 4

Bin 108: 8 of cap free
Amount of items: 3
Items: 
Size: 658572 Color: 3
Size: 194088 Color: 9
Size: 147333 Color: 14

Bin 109: 8 of cap free
Amount of items: 3
Items: 
Size: 659434 Color: 8
Size: 194645 Color: 1
Size: 145914 Color: 6

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 602488 Color: 7
Size: 397505 Color: 8

Bin 111: 9 of cap free
Amount of items: 3
Items: 
Size: 749132 Color: 2
Size: 132431 Color: 7
Size: 118429 Color: 1

Bin 112: 9 of cap free
Amount of items: 3
Items: 
Size: 369928 Color: 11
Size: 343374 Color: 11
Size: 286690 Color: 1

Bin 113: 9 of cap free
Amount of items: 3
Items: 
Size: 475831 Color: 8
Size: 362980 Color: 7
Size: 161181 Color: 4

Bin 114: 9 of cap free
Amount of items: 2
Items: 
Size: 648797 Color: 11
Size: 351195 Color: 8

Bin 115: 10 of cap free
Amount of items: 3
Items: 
Size: 718800 Color: 0
Size: 146645 Color: 14
Size: 134546 Color: 19

Bin 116: 10 of cap free
Amount of items: 3
Items: 
Size: 765060 Color: 8
Size: 128990 Color: 13
Size: 105941 Color: 15

Bin 117: 10 of cap free
Amount of items: 3
Items: 
Size: 473268 Color: 1
Size: 316720 Color: 5
Size: 210003 Color: 15

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 514984 Color: 14
Size: 485007 Color: 6

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 650595 Color: 16
Size: 349396 Color: 3

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 781220 Color: 11
Size: 109537 Color: 13
Size: 109234 Color: 18

Bin 121: 11 of cap free
Amount of items: 3
Items: 
Size: 491039 Color: 11
Size: 315750 Color: 3
Size: 193201 Color: 10

Bin 122: 11 of cap free
Amount of items: 3
Items: 
Size: 764008 Color: 12
Size: 127649 Color: 17
Size: 108333 Color: 12

Bin 123: 11 of cap free
Amount of items: 3
Items: 
Size: 735052 Color: 15
Size: 137351 Color: 5
Size: 127587 Color: 19

Bin 124: 11 of cap free
Amount of items: 3
Items: 
Size: 480049 Color: 14
Size: 263459 Color: 0
Size: 256482 Color: 18

Bin 125: 11 of cap free
Amount of items: 3
Items: 
Size: 458886 Color: 3
Size: 418050 Color: 3
Size: 123054 Color: 15

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 624538 Color: 13
Size: 375452 Color: 6

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 645806 Color: 2
Size: 354184 Color: 5

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 699978 Color: 19
Size: 300012 Color: 15

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 525989 Color: 14
Size: 473999 Color: 10

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 720526 Color: 12
Size: 279461 Color: 7

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 567910 Color: 6
Size: 432077 Color: 1

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 716890 Color: 17
Size: 283097 Color: 15

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 768925 Color: 8
Size: 231062 Color: 14

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 770480 Color: 14
Size: 229507 Color: 16

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 788540 Color: 0
Size: 211447 Color: 4

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 633313 Color: 2
Size: 366673 Color: 12

Bin 137: 15 of cap free
Amount of items: 2
Items: 
Size: 665686 Color: 9
Size: 334300 Color: 19

Bin 138: 15 of cap free
Amount of items: 2
Items: 
Size: 683063 Color: 3
Size: 316923 Color: 17

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 747380 Color: 11
Size: 252605 Color: 9

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 767532 Color: 7
Size: 232453 Color: 13

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 525605 Color: 1
Size: 474379 Color: 7

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 558370 Color: 4
Size: 441614 Color: 11

Bin 143: 17 of cap free
Amount of items: 2
Items: 
Size: 626011 Color: 16
Size: 373973 Color: 0

Bin 144: 17 of cap free
Amount of items: 2
Items: 
Size: 636924 Color: 18
Size: 363060 Color: 0

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 745714 Color: 10
Size: 254270 Color: 5

Bin 146: 17 of cap free
Amount of items: 2
Items: 
Size: 795697 Color: 3
Size: 204287 Color: 7

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 625540 Color: 16
Size: 374443 Color: 11

Bin 148: 18 of cap free
Amount of items: 2
Items: 
Size: 630277 Color: 7
Size: 369706 Color: 17

Bin 149: 19 of cap free
Amount of items: 3
Items: 
Size: 664484 Color: 8
Size: 197752 Color: 6
Size: 137746 Color: 6

Bin 150: 19 of cap free
Amount of items: 3
Items: 
Size: 548074 Color: 7
Size: 263707 Color: 17
Size: 188201 Color: 12

Bin 151: 19 of cap free
Amount of items: 3
Items: 
Size: 456561 Color: 15
Size: 428533 Color: 14
Size: 114888 Color: 10

Bin 152: 19 of cap free
Amount of items: 3
Items: 
Size: 633820 Color: 13
Size: 183354 Color: 0
Size: 182808 Color: 13

Bin 153: 20 of cap free
Amount of items: 3
Items: 
Size: 602186 Color: 7
Size: 279428 Color: 8
Size: 118367 Color: 19

Bin 154: 20 of cap free
Amount of items: 3
Items: 
Size: 641344 Color: 2
Size: 180327 Color: 7
Size: 178310 Color: 11

Bin 155: 20 of cap free
Amount of items: 2
Items: 
Size: 625686 Color: 16
Size: 374295 Color: 8

Bin 156: 21 of cap free
Amount of items: 2
Items: 
Size: 608151 Color: 8
Size: 391829 Color: 12

Bin 157: 21 of cap free
Amount of items: 3
Items: 
Size: 772254 Color: 16
Size: 126105 Color: 4
Size: 101621 Color: 15

Bin 158: 21 of cap free
Amount of items: 3
Items: 
Size: 677370 Color: 4
Size: 162758 Color: 6
Size: 159852 Color: 0

Bin 159: 21 of cap free
Amount of items: 2
Items: 
Size: 538785 Color: 12
Size: 461195 Color: 8

Bin 160: 21 of cap free
Amount of items: 2
Items: 
Size: 756948 Color: 1
Size: 243032 Color: 7

Bin 161: 22 of cap free
Amount of items: 2
Items: 
Size: 586130 Color: 17
Size: 413849 Color: 16

Bin 162: 23 of cap free
Amount of items: 2
Items: 
Size: 578576 Color: 9
Size: 421402 Color: 18

Bin 163: 23 of cap free
Amount of items: 2
Items: 
Size: 637418 Color: 16
Size: 362560 Color: 14

Bin 164: 23 of cap free
Amount of items: 2
Items: 
Size: 771853 Color: 4
Size: 228125 Color: 1

Bin 165: 24 of cap free
Amount of items: 3
Items: 
Size: 634961 Color: 9
Size: 215118 Color: 3
Size: 149898 Color: 11

Bin 166: 24 of cap free
Amount of items: 2
Items: 
Size: 592840 Color: 1
Size: 407137 Color: 5

Bin 167: 24 of cap free
Amount of items: 2
Items: 
Size: 668059 Color: 14
Size: 331918 Color: 6

Bin 168: 24 of cap free
Amount of items: 2
Items: 
Size: 635857 Color: 10
Size: 364120 Color: 0

Bin 169: 25 of cap free
Amount of items: 2
Items: 
Size: 789564 Color: 5
Size: 210412 Color: 13

Bin 170: 25 of cap free
Amount of items: 3
Items: 
Size: 348281 Color: 3
Size: 345107 Color: 0
Size: 306588 Color: 12

Bin 171: 26 of cap free
Amount of items: 2
Items: 
Size: 759949 Color: 4
Size: 240026 Color: 15

Bin 172: 26 of cap free
Amount of items: 2
Items: 
Size: 517355 Color: 8
Size: 482620 Color: 19

Bin 173: 26 of cap free
Amount of items: 2
Items: 
Size: 514376 Color: 18
Size: 485599 Color: 19

Bin 174: 27 of cap free
Amount of items: 2
Items: 
Size: 631684 Color: 14
Size: 368290 Color: 16

Bin 175: 27 of cap free
Amount of items: 2
Items: 
Size: 642564 Color: 10
Size: 357410 Color: 5

Bin 176: 28 of cap free
Amount of items: 3
Items: 
Size: 752283 Color: 10
Size: 135652 Color: 18
Size: 112038 Color: 10

Bin 177: 28 of cap free
Amount of items: 2
Items: 
Size: 515102 Color: 13
Size: 484871 Color: 0

Bin 178: 28 of cap free
Amount of items: 2
Items: 
Size: 666009 Color: 6
Size: 333964 Color: 10

Bin 179: 30 of cap free
Amount of items: 2
Items: 
Size: 672524 Color: 14
Size: 327447 Color: 16

Bin 180: 30 of cap free
Amount of items: 2
Items: 
Size: 547632 Color: 15
Size: 452339 Color: 1

Bin 181: 30 of cap free
Amount of items: 3
Items: 
Size: 348019 Color: 16
Size: 347754 Color: 9
Size: 304198 Color: 3

Bin 182: 30 of cap free
Amount of items: 2
Items: 
Size: 579346 Color: 4
Size: 420625 Color: 7

Bin 183: 31 of cap free
Amount of items: 2
Items: 
Size: 581040 Color: 15
Size: 418930 Color: 0

Bin 184: 31 of cap free
Amount of items: 2
Items: 
Size: 618629 Color: 3
Size: 381341 Color: 13

Bin 185: 32 of cap free
Amount of items: 3
Items: 
Size: 674285 Color: 14
Size: 186457 Color: 2
Size: 139227 Color: 16

Bin 186: 32 of cap free
Amount of items: 2
Items: 
Size: 708544 Color: 0
Size: 291425 Color: 16

Bin 187: 32 of cap free
Amount of items: 2
Items: 
Size: 749998 Color: 3
Size: 249971 Color: 10

Bin 188: 32 of cap free
Amount of items: 3
Items: 
Size: 674650 Color: 9
Size: 172728 Color: 12
Size: 152591 Color: 15

Bin 189: 32 of cap free
Amount of items: 2
Items: 
Size: 634466 Color: 6
Size: 365503 Color: 14

Bin 190: 33 of cap free
Amount of items: 3
Items: 
Size: 746024 Color: 17
Size: 136604 Color: 9
Size: 117340 Color: 4

Bin 191: 33 of cap free
Amount of items: 3
Items: 
Size: 617296 Color: 12
Size: 193074 Color: 15
Size: 189598 Color: 16

Bin 192: 33 of cap free
Amount of items: 3
Items: 
Size: 660707 Color: 14
Size: 174490 Color: 17
Size: 164771 Color: 5

Bin 193: 33 of cap free
Amount of items: 2
Items: 
Size: 780443 Color: 15
Size: 219525 Color: 6

Bin 194: 33 of cap free
Amount of items: 2
Items: 
Size: 582048 Color: 18
Size: 417920 Color: 4

Bin 195: 34 of cap free
Amount of items: 3
Items: 
Size: 478796 Color: 7
Size: 261472 Color: 11
Size: 259699 Color: 17

Bin 196: 34 of cap free
Amount of items: 2
Items: 
Size: 654811 Color: 8
Size: 345156 Color: 11

Bin 197: 35 of cap free
Amount of items: 2
Items: 
Size: 609212 Color: 9
Size: 390754 Color: 13

Bin 198: 36 of cap free
Amount of items: 2
Items: 
Size: 632374 Color: 17
Size: 367591 Color: 3

Bin 199: 36 of cap free
Amount of items: 2
Items: 
Size: 655061 Color: 0
Size: 344904 Color: 9

Bin 200: 36 of cap free
Amount of items: 2
Items: 
Size: 760916 Color: 14
Size: 239049 Color: 0

Bin 201: 37 of cap free
Amount of items: 3
Items: 
Size: 639283 Color: 8
Size: 185563 Color: 19
Size: 175118 Color: 1

Bin 202: 37 of cap free
Amount of items: 2
Items: 
Size: 750657 Color: 12
Size: 249307 Color: 18

Bin 203: 38 of cap free
Amount of items: 3
Items: 
Size: 346926 Color: 19
Size: 332840 Color: 11
Size: 320197 Color: 13

Bin 204: 38 of cap free
Amount of items: 3
Items: 
Size: 765239 Color: 0
Size: 128523 Color: 5
Size: 106201 Color: 12

Bin 205: 38 of cap free
Amount of items: 2
Items: 
Size: 556803 Color: 0
Size: 443160 Color: 1

Bin 206: 39 of cap free
Amount of items: 3
Items: 
Size: 757038 Color: 2
Size: 121537 Color: 0
Size: 121387 Color: 10

Bin 207: 41 of cap free
Amount of items: 3
Items: 
Size: 684326 Color: 15
Size: 185351 Color: 1
Size: 130283 Color: 5

Bin 208: 41 of cap free
Amount of items: 3
Items: 
Size: 773354 Color: 0
Size: 123956 Color: 3
Size: 102650 Color: 1

Bin 209: 41 of cap free
Amount of items: 2
Items: 
Size: 769430 Color: 11
Size: 230530 Color: 5

Bin 210: 42 of cap free
Amount of items: 3
Items: 
Size: 456709 Color: 13
Size: 373930 Color: 0
Size: 169320 Color: 19

Bin 211: 42 of cap free
Amount of items: 2
Items: 
Size: 717794 Color: 15
Size: 282165 Color: 10

Bin 212: 42 of cap free
Amount of items: 2
Items: 
Size: 606269 Color: 1
Size: 393690 Color: 8

Bin 213: 42 of cap free
Amount of items: 2
Items: 
Size: 652046 Color: 15
Size: 347913 Color: 8

Bin 214: 42 of cap free
Amount of items: 2
Items: 
Size: 698232 Color: 16
Size: 301727 Color: 7

Bin 215: 42 of cap free
Amount of items: 2
Items: 
Size: 792256 Color: 10
Size: 207703 Color: 18

Bin 216: 42 of cap free
Amount of items: 2
Items: 
Size: 792533 Color: 12
Size: 207426 Color: 3

Bin 217: 43 of cap free
Amount of items: 3
Items: 
Size: 454854 Color: 6
Size: 398966 Color: 3
Size: 146138 Color: 2

Bin 218: 43 of cap free
Amount of items: 2
Items: 
Size: 625016 Color: 12
Size: 374942 Color: 2

Bin 219: 44 of cap free
Amount of items: 2
Items: 
Size: 753143 Color: 8
Size: 246814 Color: 16

Bin 220: 44 of cap free
Amount of items: 3
Items: 
Size: 366772 Color: 19
Size: 345429 Color: 15
Size: 287756 Color: 5

Bin 221: 45 of cap free
Amount of items: 2
Items: 
Size: 545435 Color: 9
Size: 454521 Color: 16

Bin 222: 46 of cap free
Amount of items: 3
Items: 
Size: 739529 Color: 8
Size: 130736 Color: 10
Size: 129690 Color: 0

Bin 223: 46 of cap free
Amount of items: 2
Items: 
Size: 595700 Color: 2
Size: 404255 Color: 14

Bin 224: 46 of cap free
Amount of items: 2
Items: 
Size: 522007 Color: 2
Size: 477948 Color: 15

Bin 225: 47 of cap free
Amount of items: 2
Items: 
Size: 763729 Color: 6
Size: 236225 Color: 4

Bin 226: 47 of cap free
Amount of items: 2
Items: 
Size: 600228 Color: 11
Size: 399726 Color: 3

Bin 227: 47 of cap free
Amount of items: 2
Items: 
Size: 599052 Color: 10
Size: 400902 Color: 17

Bin 228: 47 of cap free
Amount of items: 2
Items: 
Size: 651270 Color: 3
Size: 348684 Color: 19

Bin 229: 47 of cap free
Amount of items: 2
Items: 
Size: 689876 Color: 3
Size: 310078 Color: 19

Bin 230: 48 of cap free
Amount of items: 2
Items: 
Size: 742007 Color: 4
Size: 257946 Color: 13

Bin 231: 49 of cap free
Amount of items: 2
Items: 
Size: 662488 Color: 3
Size: 337464 Color: 5

Bin 232: 49 of cap free
Amount of items: 2
Items: 
Size: 590146 Color: 6
Size: 409806 Color: 8

Bin 233: 50 of cap free
Amount of items: 2
Items: 
Size: 641282 Color: 1
Size: 358669 Color: 10

Bin 234: 51 of cap free
Amount of items: 2
Items: 
Size: 575021 Color: 7
Size: 424929 Color: 3

Bin 235: 52 of cap free
Amount of items: 2
Items: 
Size: 744923 Color: 3
Size: 255026 Color: 12

Bin 236: 52 of cap free
Amount of items: 3
Items: 
Size: 433298 Color: 14
Size: 372889 Color: 14
Size: 193762 Color: 2

Bin 237: 53 of cap free
Amount of items: 3
Items: 
Size: 335222 Color: 1
Size: 332446 Color: 8
Size: 332280 Color: 8

Bin 238: 53 of cap free
Amount of items: 2
Items: 
Size: 737543 Color: 18
Size: 262405 Color: 11

Bin 239: 55 of cap free
Amount of items: 3
Items: 
Size: 592565 Color: 10
Size: 210681 Color: 3
Size: 196700 Color: 3

Bin 240: 55 of cap free
Amount of items: 2
Items: 
Size: 620270 Color: 6
Size: 379676 Color: 0

Bin 241: 57 of cap free
Amount of items: 3
Items: 
Size: 756724 Color: 0
Size: 142600 Color: 2
Size: 100620 Color: 18

Bin 242: 57 of cap free
Amount of items: 2
Items: 
Size: 783766 Color: 18
Size: 216178 Color: 2

Bin 243: 58 of cap free
Amount of items: 3
Items: 
Size: 493650 Color: 11
Size: 260304 Color: 0
Size: 245989 Color: 19

Bin 244: 58 of cap free
Amount of items: 2
Items: 
Size: 522537 Color: 3
Size: 477406 Color: 2

Bin 245: 60 of cap free
Amount of items: 2
Items: 
Size: 701925 Color: 0
Size: 298016 Color: 14

Bin 246: 60 of cap free
Amount of items: 2
Items: 
Size: 526196 Color: 17
Size: 473745 Color: 4

Bin 247: 61 of cap free
Amount of items: 2
Items: 
Size: 565957 Color: 3
Size: 433983 Color: 6

Bin 248: 63 of cap free
Amount of items: 3
Items: 
Size: 681830 Color: 2
Size: 163528 Color: 9
Size: 154580 Color: 6

Bin 249: 63 of cap free
Amount of items: 3
Items: 
Size: 345770 Color: 10
Size: 338178 Color: 10
Size: 315990 Color: 8

Bin 250: 63 of cap free
Amount of items: 3
Items: 
Size: 442915 Color: 16
Size: 374047 Color: 6
Size: 182976 Color: 13

Bin 251: 63 of cap free
Amount of items: 2
Items: 
Size: 790768 Color: 6
Size: 209170 Color: 16

Bin 252: 64 of cap free
Amount of items: 2
Items: 
Size: 685286 Color: 19
Size: 314651 Color: 18

Bin 253: 66 of cap free
Amount of items: 3
Items: 
Size: 640395 Color: 10
Size: 180198 Color: 6
Size: 179342 Color: 9

Bin 254: 66 of cap free
Amount of items: 3
Items: 
Size: 476978 Color: 18
Size: 261511 Color: 17
Size: 261446 Color: 2

Bin 255: 66 of cap free
Amount of items: 2
Items: 
Size: 659990 Color: 17
Size: 339945 Color: 15

Bin 256: 66 of cap free
Amount of items: 2
Items: 
Size: 664587 Color: 14
Size: 335348 Color: 17

Bin 257: 66 of cap free
Amount of items: 2
Items: 
Size: 764917 Color: 8
Size: 235018 Color: 4

Bin 258: 67 of cap free
Amount of items: 2
Items: 
Size: 728692 Color: 10
Size: 271242 Color: 15

Bin 259: 68 of cap free
Amount of items: 2
Items: 
Size: 555469 Color: 19
Size: 444464 Color: 18

Bin 260: 69 of cap free
Amount of items: 3
Items: 
Size: 725128 Color: 3
Size: 141741 Color: 9
Size: 133063 Color: 7

Bin 261: 69 of cap free
Amount of items: 3
Items: 
Size: 714336 Color: 3
Size: 144870 Color: 9
Size: 140726 Color: 2

Bin 262: 69 of cap free
Amount of items: 2
Items: 
Size: 679750 Color: 4
Size: 320182 Color: 9

Bin 263: 69 of cap free
Amount of items: 2
Items: 
Size: 690745 Color: 19
Size: 309187 Color: 17

Bin 264: 70 of cap free
Amount of items: 2
Items: 
Size: 529477 Color: 5
Size: 470454 Color: 0

Bin 265: 70 of cap free
Amount of items: 2
Items: 
Size: 545862 Color: 8
Size: 454069 Color: 16

Bin 266: 70 of cap free
Amount of items: 2
Items: 
Size: 669661 Color: 10
Size: 330270 Color: 18

Bin 267: 71 of cap free
Amount of items: 3
Items: 
Size: 577572 Color: 15
Size: 271529 Color: 8
Size: 150829 Color: 16

Bin 268: 72 of cap free
Amount of items: 2
Items: 
Size: 699202 Color: 12
Size: 300727 Color: 4

Bin 269: 74 of cap free
Amount of items: 3
Items: 
Size: 690833 Color: 19
Size: 166394 Color: 10
Size: 142700 Color: 6

Bin 270: 74 of cap free
Amount of items: 2
Items: 
Size: 791225 Color: 14
Size: 208702 Color: 17

Bin 271: 75 of cap free
Amount of items: 3
Items: 
Size: 675244 Color: 2
Size: 167925 Color: 0
Size: 156757 Color: 16

Bin 272: 75 of cap free
Amount of items: 2
Items: 
Size: 546297 Color: 1
Size: 453629 Color: 8

Bin 273: 75 of cap free
Amount of items: 2
Items: 
Size: 725927 Color: 17
Size: 273999 Color: 10

Bin 274: 76 of cap free
Amount of items: 3
Items: 
Size: 442787 Color: 4
Size: 294246 Color: 5
Size: 262892 Color: 6

Bin 275: 76 of cap free
Amount of items: 2
Items: 
Size: 600887 Color: 8
Size: 399038 Color: 5

Bin 276: 77 of cap free
Amount of items: 2
Items: 
Size: 565158 Color: 16
Size: 434766 Color: 8

Bin 277: 77 of cap free
Amount of items: 2
Items: 
Size: 666272 Color: 11
Size: 333652 Color: 3

Bin 278: 78 of cap free
Amount of items: 2
Items: 
Size: 603464 Color: 9
Size: 396459 Color: 3

Bin 279: 78 of cap free
Amount of items: 2
Items: 
Size: 624317 Color: 12
Size: 375606 Color: 10

Bin 280: 78 of cap free
Amount of items: 2
Items: 
Size: 750914 Color: 15
Size: 249009 Color: 13

Bin 281: 79 of cap free
Amount of items: 2
Items: 
Size: 663744 Color: 9
Size: 336178 Color: 15

Bin 282: 81 of cap free
Amount of items: 3
Items: 
Size: 608828 Color: 4
Size: 197606 Color: 17
Size: 193486 Color: 0

Bin 283: 81 of cap free
Amount of items: 2
Items: 
Size: 505426 Color: 13
Size: 494494 Color: 18

Bin 284: 81 of cap free
Amount of items: 2
Items: 
Size: 623443 Color: 14
Size: 376477 Color: 4

Bin 285: 82 of cap free
Amount of items: 2
Items: 
Size: 636625 Color: 17
Size: 363294 Color: 9

Bin 286: 82 of cap free
Amount of items: 2
Items: 
Size: 548204 Color: 10
Size: 451715 Color: 5

Bin 287: 82 of cap free
Amount of items: 2
Items: 
Size: 794432 Color: 19
Size: 205487 Color: 9

Bin 288: 84 of cap free
Amount of items: 2
Items: 
Size: 798506 Color: 13
Size: 201411 Color: 18

Bin 289: 85 of cap free
Amount of items: 2
Items: 
Size: 702735 Color: 8
Size: 297181 Color: 11

Bin 290: 88 of cap free
Amount of items: 2
Items: 
Size: 668361 Color: 13
Size: 331552 Color: 18

Bin 291: 89 of cap free
Amount of items: 2
Items: 
Size: 789629 Color: 6
Size: 210283 Color: 2

Bin 292: 89 of cap free
Amount of items: 2
Items: 
Size: 775345 Color: 17
Size: 224567 Color: 1

Bin 293: 89 of cap free
Amount of items: 2
Items: 
Size: 618302 Color: 11
Size: 381610 Color: 16

Bin 294: 91 of cap free
Amount of items: 3
Items: 
Size: 636120 Color: 3
Size: 183094 Color: 5
Size: 180696 Color: 16

Bin 295: 91 of cap free
Amount of items: 2
Items: 
Size: 506668 Color: 17
Size: 493242 Color: 9

Bin 296: 92 of cap free
Amount of items: 2
Items: 
Size: 649887 Color: 16
Size: 350022 Color: 5

Bin 297: 95 of cap free
Amount of items: 2
Items: 
Size: 507653 Color: 13
Size: 492253 Color: 7

Bin 298: 96 of cap free
Amount of items: 2
Items: 
Size: 601946 Color: 17
Size: 397959 Color: 8

Bin 299: 97 of cap free
Amount of items: 2
Items: 
Size: 792983 Color: 13
Size: 206921 Color: 12

Bin 300: 98 of cap free
Amount of items: 2
Items: 
Size: 707906 Color: 13
Size: 291997 Color: 4

Bin 301: 99 of cap free
Amount of items: 2
Items: 
Size: 761637 Color: 4
Size: 238265 Color: 13

Bin 302: 99 of cap free
Amount of items: 3
Items: 
Size: 758228 Color: 4
Size: 121149 Color: 11
Size: 120525 Color: 18

Bin 303: 100 of cap free
Amount of items: 2
Items: 
Size: 779032 Color: 8
Size: 220869 Color: 14

Bin 304: 101 of cap free
Amount of items: 2
Items: 
Size: 501656 Color: 0
Size: 498244 Color: 4

Bin 305: 102 of cap free
Amount of items: 3
Items: 
Size: 713902 Color: 5
Size: 176038 Color: 6
Size: 109959 Color: 11

Bin 306: 103 of cap free
Amount of items: 2
Items: 
Size: 717025 Color: 13
Size: 282873 Color: 12

Bin 307: 103 of cap free
Amount of items: 2
Items: 
Size: 546173 Color: 7
Size: 453725 Color: 4

Bin 308: 103 of cap free
Amount of items: 2
Items: 
Size: 577050 Color: 11
Size: 422848 Color: 8

Bin 309: 103 of cap free
Amount of items: 2
Items: 
Size: 779559 Color: 2
Size: 220339 Color: 12

Bin 310: 105 of cap free
Amount of items: 2
Items: 
Size: 594188 Color: 9
Size: 405708 Color: 17

Bin 311: 106 of cap free
Amount of items: 2
Items: 
Size: 669644 Color: 14
Size: 330251 Color: 6

Bin 312: 107 of cap free
Amount of items: 2
Items: 
Size: 725524 Color: 17
Size: 274370 Color: 13

Bin 313: 107 of cap free
Amount of items: 2
Items: 
Size: 621545 Color: 13
Size: 378349 Color: 6

Bin 314: 108 of cap free
Amount of items: 2
Items: 
Size: 613691 Color: 6
Size: 386202 Color: 12

Bin 315: 109 of cap free
Amount of items: 2
Items: 
Size: 742824 Color: 9
Size: 257068 Color: 12

Bin 316: 109 of cap free
Amount of items: 2
Items: 
Size: 549507 Color: 16
Size: 450385 Color: 19

Bin 317: 109 of cap free
Amount of items: 2
Items: 
Size: 609614 Color: 2
Size: 390278 Color: 7

Bin 318: 109 of cap free
Amount of items: 2
Items: 
Size: 792495 Color: 2
Size: 207397 Color: 8

Bin 319: 112 of cap free
Amount of items: 2
Items: 
Size: 709084 Color: 4
Size: 290805 Color: 15

Bin 320: 113 of cap free
Amount of items: 2
Items: 
Size: 629317 Color: 1
Size: 370571 Color: 13

Bin 321: 113 of cap free
Amount of items: 2
Items: 
Size: 764475 Color: 1
Size: 235413 Color: 15

Bin 322: 117 of cap free
Amount of items: 2
Items: 
Size: 633548 Color: 9
Size: 366336 Color: 4

Bin 323: 119 of cap free
Amount of items: 3
Items: 
Size: 354675 Color: 19
Size: 343621 Color: 14
Size: 301586 Color: 6

Bin 324: 119 of cap free
Amount of items: 2
Items: 
Size: 572715 Color: 13
Size: 427167 Color: 15

Bin 325: 120 of cap free
Amount of items: 2
Items: 
Size: 706404 Color: 12
Size: 293477 Color: 18

Bin 326: 121 of cap free
Amount of items: 2
Items: 
Size: 544564 Color: 13
Size: 455316 Color: 14

Bin 327: 123 of cap free
Amount of items: 2
Items: 
Size: 524030 Color: 6
Size: 475848 Color: 0

Bin 328: 123 of cap free
Amount of items: 2
Items: 
Size: 578101 Color: 14
Size: 421777 Color: 5

Bin 329: 123 of cap free
Amount of items: 2
Items: 
Size: 674793 Color: 2
Size: 325085 Color: 14

Bin 330: 124 of cap free
Amount of items: 2
Items: 
Size: 542704 Color: 0
Size: 457173 Color: 18

Bin 331: 125 of cap free
Amount of items: 3
Items: 
Size: 460342 Color: 2
Size: 391627 Color: 14
Size: 147907 Color: 19

Bin 332: 125 of cap free
Amount of items: 2
Items: 
Size: 551974 Color: 4
Size: 447902 Color: 17

Bin 333: 125 of cap free
Amount of items: 2
Items: 
Size: 699945 Color: 9
Size: 299931 Color: 17

Bin 334: 127 of cap free
Amount of items: 2
Items: 
Size: 581993 Color: 12
Size: 417881 Color: 5

Bin 335: 127 of cap free
Amount of items: 2
Items: 
Size: 712672 Color: 1
Size: 287202 Color: 16

Bin 336: 127 of cap free
Amount of items: 2
Items: 
Size: 770540 Color: 10
Size: 229334 Color: 14

Bin 337: 128 of cap free
Amount of items: 2
Items: 
Size: 538269 Color: 3
Size: 461604 Color: 13

Bin 338: 130 of cap free
Amount of items: 2
Items: 
Size: 760218 Color: 2
Size: 239653 Color: 11

Bin 339: 131 of cap free
Amount of items: 3
Items: 
Size: 576056 Color: 7
Size: 249578 Color: 1
Size: 174236 Color: 12

Bin 340: 131 of cap free
Amount of items: 2
Items: 
Size: 658410 Color: 16
Size: 341460 Color: 1

Bin 341: 131 of cap free
Amount of items: 2
Items: 
Size: 656922 Color: 13
Size: 342948 Color: 9

Bin 342: 131 of cap free
Amount of items: 2
Items: 
Size: 762933 Color: 3
Size: 236937 Color: 10

Bin 343: 133 of cap free
Amount of items: 3
Items: 
Size: 478999 Color: 6
Size: 262770 Color: 12
Size: 258099 Color: 2

Bin 344: 133 of cap free
Amount of items: 2
Items: 
Size: 559605 Color: 11
Size: 440263 Color: 14

Bin 345: 134 of cap free
Amount of items: 2
Items: 
Size: 614293 Color: 5
Size: 385574 Color: 13

Bin 346: 136 of cap free
Amount of items: 2
Items: 
Size: 719261 Color: 7
Size: 280604 Color: 15

Bin 347: 139 of cap free
Amount of items: 2
Items: 
Size: 652293 Color: 13
Size: 347569 Color: 9

Bin 348: 139 of cap free
Amount of items: 2
Items: 
Size: 651769 Color: 7
Size: 348093 Color: 17

Bin 349: 140 of cap free
Amount of items: 2
Items: 
Size: 790135 Color: 14
Size: 209726 Color: 0

Bin 350: 142 of cap free
Amount of items: 2
Items: 
Size: 535274 Color: 18
Size: 464585 Color: 9

Bin 351: 143 of cap free
Amount of items: 2
Items: 
Size: 737947 Color: 3
Size: 261911 Color: 18

Bin 352: 143 of cap free
Amount of items: 3
Items: 
Size: 700306 Color: 9
Size: 173748 Color: 19
Size: 125804 Color: 7

Bin 353: 144 of cap free
Amount of items: 2
Items: 
Size: 645733 Color: 5
Size: 354124 Color: 0

Bin 354: 145 of cap free
Amount of items: 3
Items: 
Size: 691983 Color: 11
Size: 170309 Color: 13
Size: 137564 Color: 6

Bin 355: 147 of cap free
Amount of items: 2
Items: 
Size: 514175 Color: 9
Size: 485679 Color: 8

Bin 356: 148 of cap free
Amount of items: 2
Items: 
Size: 610006 Color: 0
Size: 389847 Color: 2

Bin 357: 149 of cap free
Amount of items: 2
Items: 
Size: 657117 Color: 1
Size: 342735 Color: 0

Bin 358: 149 of cap free
Amount of items: 2
Items: 
Size: 573649 Color: 8
Size: 426203 Color: 0

Bin 359: 150 of cap free
Amount of items: 2
Items: 
Size: 624764 Color: 4
Size: 375087 Color: 13

Bin 360: 150 of cap free
Amount of items: 2
Items: 
Size: 507253 Color: 10
Size: 492598 Color: 5

Bin 361: 150 of cap free
Amount of items: 2
Items: 
Size: 670762 Color: 5
Size: 329089 Color: 17

Bin 362: 150 of cap free
Amount of items: 2
Items: 
Size: 763278 Color: 1
Size: 236573 Color: 12

Bin 363: 152 of cap free
Amount of items: 2
Items: 
Size: 555861 Color: 16
Size: 443988 Color: 18

Bin 364: 155 of cap free
Amount of items: 2
Items: 
Size: 685023 Color: 1
Size: 314823 Color: 14

Bin 365: 156 of cap free
Amount of items: 2
Items: 
Size: 673787 Color: 16
Size: 326058 Color: 10

Bin 366: 157 of cap free
Amount of items: 2
Items: 
Size: 619214 Color: 14
Size: 380630 Color: 3

Bin 367: 158 of cap free
Amount of items: 2
Items: 
Size: 747466 Color: 16
Size: 252377 Color: 7

Bin 368: 158 of cap free
Amount of items: 2
Items: 
Size: 660821 Color: 5
Size: 339022 Color: 8

Bin 369: 159 of cap free
Amount of items: 3
Items: 
Size: 639618 Color: 1
Size: 181726 Color: 17
Size: 178498 Color: 15

Bin 370: 161 of cap free
Amount of items: 2
Items: 
Size: 708801 Color: 6
Size: 291039 Color: 2

Bin 371: 163 of cap free
Amount of items: 2
Items: 
Size: 566541 Color: 4
Size: 433297 Color: 3

Bin 372: 165 of cap free
Amount of items: 2
Items: 
Size: 541159 Color: 1
Size: 458677 Color: 7

Bin 373: 167 of cap free
Amount of items: 2
Items: 
Size: 694203 Color: 12
Size: 305631 Color: 16

Bin 374: 168 of cap free
Amount of items: 3
Items: 
Size: 456223 Color: 9
Size: 278867 Color: 8
Size: 264743 Color: 4

Bin 375: 168 of cap free
Amount of items: 2
Items: 
Size: 572517 Color: 1
Size: 427316 Color: 2

Bin 376: 171 of cap free
Amount of items: 2
Items: 
Size: 692307 Color: 1
Size: 307523 Color: 9

Bin 377: 172 of cap free
Amount of items: 2
Items: 
Size: 622139 Color: 3
Size: 377690 Color: 17

Bin 378: 173 of cap free
Amount of items: 2
Items: 
Size: 509728 Color: 8
Size: 490100 Color: 19

Bin 379: 176 of cap free
Amount of items: 2
Items: 
Size: 575343 Color: 12
Size: 424482 Color: 18

Bin 380: 177 of cap free
Amount of items: 2
Items: 
Size: 695840 Color: 2
Size: 303984 Color: 19

Bin 381: 178 of cap free
Amount of items: 2
Items: 
Size: 684017 Color: 15
Size: 315806 Color: 7

Bin 382: 179 of cap free
Amount of items: 2
Items: 
Size: 635445 Color: 15
Size: 364377 Color: 9

Bin 383: 180 of cap free
Amount of items: 2
Items: 
Size: 540341 Color: 7
Size: 459480 Color: 19

Bin 384: 180 of cap free
Amount of items: 3
Items: 
Size: 384359 Color: 18
Size: 312468 Color: 10
Size: 302994 Color: 10

Bin 385: 180 of cap free
Amount of items: 2
Items: 
Size: 660842 Color: 8
Size: 338979 Color: 9

Bin 386: 181 of cap free
Amount of items: 2
Items: 
Size: 735867 Color: 9
Size: 263953 Color: 18

Bin 387: 181 of cap free
Amount of items: 3
Items: 
Size: 675404 Color: 6
Size: 167914 Color: 2
Size: 156502 Color: 1

Bin 388: 181 of cap free
Amount of items: 2
Items: 
Size: 630536 Color: 16
Size: 369284 Color: 3

Bin 389: 181 of cap free
Amount of items: 2
Items: 
Size: 564679 Color: 3
Size: 435141 Color: 13

Bin 390: 182 of cap free
Amount of items: 3
Items: 
Size: 694807 Color: 0
Size: 176306 Color: 5
Size: 128706 Color: 11

Bin 391: 182 of cap free
Amount of items: 3
Items: 
Size: 671120 Color: 7
Size: 168356 Color: 15
Size: 160343 Color: 0

Bin 392: 184 of cap free
Amount of items: 2
Items: 
Size: 680833 Color: 15
Size: 318984 Color: 14

Bin 393: 184 of cap free
Amount of items: 2
Items: 
Size: 579210 Color: 5
Size: 420607 Color: 9

Bin 394: 184 of cap free
Amount of items: 2
Items: 
Size: 782753 Color: 10
Size: 217064 Color: 7

Bin 395: 188 of cap free
Amount of items: 2
Items: 
Size: 500603 Color: 12
Size: 499210 Color: 17

Bin 396: 189 of cap free
Amount of items: 2
Items: 
Size: 600094 Color: 12
Size: 399718 Color: 17

Bin 397: 193 of cap free
Amount of items: 2
Items: 
Size: 733668 Color: 7
Size: 266140 Color: 3

Bin 398: 193 of cap free
Amount of items: 2
Items: 
Size: 682076 Color: 19
Size: 317732 Color: 12

Bin 399: 193 of cap free
Amount of items: 2
Items: 
Size: 567500 Color: 6
Size: 432308 Color: 16

Bin 400: 193 of cap free
Amount of items: 2
Items: 
Size: 572120 Color: 12
Size: 427688 Color: 6

Bin 401: 194 of cap free
Amount of items: 2
Items: 
Size: 607978 Color: 19
Size: 391829 Color: 3

Bin 402: 196 of cap free
Amount of items: 2
Items: 
Size: 678672 Color: 1
Size: 321133 Color: 5

Bin 403: 197 of cap free
Amount of items: 2
Items: 
Size: 778082 Color: 13
Size: 221722 Color: 17

Bin 404: 197 of cap free
Amount of items: 2
Items: 
Size: 522003 Color: 18
Size: 477801 Color: 6

Bin 405: 197 of cap free
Amount of items: 2
Items: 
Size: 543312 Color: 6
Size: 456492 Color: 8

Bin 406: 197 of cap free
Amount of items: 2
Items: 
Size: 616816 Color: 10
Size: 382988 Color: 18

Bin 407: 197 of cap free
Amount of items: 2
Items: 
Size: 766414 Color: 13
Size: 233390 Color: 2

Bin 408: 198 of cap free
Amount of items: 2
Items: 
Size: 518542 Color: 19
Size: 481261 Color: 1

Bin 409: 200 of cap free
Amount of items: 2
Items: 
Size: 725704 Color: 4
Size: 274097 Color: 19

Bin 410: 200 of cap free
Amount of items: 2
Items: 
Size: 529338 Color: 3
Size: 470463 Color: 5

Bin 411: 202 of cap free
Amount of items: 2
Items: 
Size: 731951 Color: 16
Size: 267848 Color: 6

Bin 412: 203 of cap free
Amount of items: 3
Items: 
Size: 476161 Color: 16
Size: 294516 Color: 6
Size: 229121 Color: 14

Bin 413: 203 of cap free
Amount of items: 2
Items: 
Size: 535228 Color: 1
Size: 464570 Color: 18

Bin 414: 205 of cap free
Amount of items: 2
Items: 
Size: 728365 Color: 9
Size: 271431 Color: 18

Bin 415: 206 of cap free
Amount of items: 2
Items: 
Size: 536106 Color: 16
Size: 463689 Color: 5

Bin 416: 208 of cap free
Amount of items: 2
Items: 
Size: 597689 Color: 14
Size: 402104 Color: 1

Bin 417: 210 of cap free
Amount of items: 2
Items: 
Size: 727793 Color: 5
Size: 271998 Color: 13

Bin 418: 210 of cap free
Amount of items: 3
Items: 
Size: 637602 Color: 4
Size: 183890 Color: 10
Size: 178299 Color: 2

Bin 419: 210 of cap free
Amount of items: 2
Items: 
Size: 787185 Color: 2
Size: 212606 Color: 6

Bin 420: 212 of cap free
Amount of items: 2
Items: 
Size: 540641 Color: 13
Size: 459148 Color: 16

Bin 421: 212 of cap free
Amount of items: 2
Items: 
Size: 665004 Color: 17
Size: 334785 Color: 15

Bin 422: 213 of cap free
Amount of items: 2
Items: 
Size: 714998 Color: 14
Size: 284790 Color: 15

Bin 423: 213 of cap free
Amount of items: 2
Items: 
Size: 679163 Color: 7
Size: 320625 Color: 19

Bin 424: 213 of cap free
Amount of items: 2
Items: 
Size: 710442 Color: 13
Size: 289346 Color: 18

Bin 425: 215 of cap free
Amount of items: 2
Items: 
Size: 514791 Color: 15
Size: 484995 Color: 11

Bin 426: 216 of cap free
Amount of items: 2
Items: 
Size: 509940 Color: 6
Size: 489845 Color: 1

Bin 427: 217 of cap free
Amount of items: 3
Items: 
Size: 367022 Color: 15
Size: 316804 Color: 18
Size: 315958 Color: 8

Bin 428: 218 of cap free
Amount of items: 3
Items: 
Size: 675025 Color: 3
Size: 173230 Color: 11
Size: 151528 Color: 3

Bin 429: 218 of cap free
Amount of items: 2
Items: 
Size: 774596 Color: 5
Size: 225187 Color: 19

Bin 430: 218 of cap free
Amount of items: 2
Items: 
Size: 775849 Color: 14
Size: 223934 Color: 3

Bin 431: 218 of cap free
Amount of items: 2
Items: 
Size: 781766 Color: 3
Size: 218017 Color: 9

Bin 432: 219 of cap free
Amount of items: 2
Items: 
Size: 754464 Color: 4
Size: 245318 Color: 11

Bin 433: 220 of cap free
Amount of items: 2
Items: 
Size: 669799 Color: 15
Size: 329982 Color: 7

Bin 434: 221 of cap free
Amount of items: 2
Items: 
Size: 647766 Color: 11
Size: 352014 Color: 1

Bin 435: 224 of cap free
Amount of items: 2
Items: 
Size: 511873 Color: 8
Size: 487904 Color: 15

Bin 436: 224 of cap free
Amount of items: 2
Items: 
Size: 617355 Color: 14
Size: 382422 Color: 6

Bin 437: 225 of cap free
Amount of items: 2
Items: 
Size: 648785 Color: 11
Size: 350991 Color: 15

Bin 438: 229 of cap free
Amount of items: 2
Items: 
Size: 680013 Color: 2
Size: 319759 Color: 8

Bin 439: 229 of cap free
Amount of items: 2
Items: 
Size: 796803 Color: 13
Size: 202969 Color: 12

Bin 440: 230 of cap free
Amount of items: 2
Items: 
Size: 782253 Color: 13
Size: 217518 Color: 6

Bin 441: 232 of cap free
Amount of items: 2
Items: 
Size: 744828 Color: 4
Size: 254941 Color: 13

Bin 442: 233 of cap free
Amount of items: 2
Items: 
Size: 713482 Color: 4
Size: 286286 Color: 16

Bin 443: 235 of cap free
Amount of items: 2
Items: 
Size: 762247 Color: 7
Size: 237519 Color: 11

Bin 444: 236 of cap free
Amount of items: 2
Items: 
Size: 507597 Color: 8
Size: 492168 Color: 12

Bin 445: 237 of cap free
Amount of items: 2
Items: 
Size: 503900 Color: 11
Size: 495864 Color: 0

Bin 446: 239 of cap free
Amount of items: 2
Items: 
Size: 558636 Color: 11
Size: 441126 Color: 6

Bin 447: 240 of cap free
Amount of items: 3
Items: 
Size: 636339 Color: 16
Size: 184601 Color: 15
Size: 178821 Color: 15

Bin 448: 243 of cap free
Amount of items: 2
Items: 
Size: 537464 Color: 3
Size: 462294 Color: 7

Bin 449: 245 of cap free
Amount of items: 2
Items: 
Size: 783588 Color: 7
Size: 216168 Color: 9

Bin 450: 247 of cap free
Amount of items: 2
Items: 
Size: 792810 Color: 2
Size: 206944 Color: 13

Bin 451: 252 of cap free
Amount of items: 2
Items: 
Size: 602050 Color: 17
Size: 397699 Color: 13

Bin 452: 252 of cap free
Amount of items: 2
Items: 
Size: 578269 Color: 2
Size: 421480 Color: 13

Bin 453: 252 of cap free
Amount of items: 2
Items: 
Size: 585268 Color: 2
Size: 414481 Color: 18

Bin 454: 261 of cap free
Amount of items: 2
Items: 
Size: 550323 Color: 19
Size: 449417 Color: 6

Bin 455: 261 of cap free
Amount of items: 2
Items: 
Size: 585260 Color: 10
Size: 414480 Color: 18

Bin 456: 263 of cap free
Amount of items: 2
Items: 
Size: 753977 Color: 19
Size: 245761 Color: 17

Bin 457: 267 of cap free
Amount of items: 2
Items: 
Size: 519142 Color: 2
Size: 480592 Color: 5

Bin 458: 267 of cap free
Amount of items: 2
Items: 
Size: 565527 Color: 11
Size: 434207 Color: 18

Bin 459: 268 of cap free
Amount of items: 2
Items: 
Size: 738890 Color: 19
Size: 260843 Color: 6

Bin 460: 269 of cap free
Amount of items: 2
Items: 
Size: 603582 Color: 0
Size: 396150 Color: 10

Bin 461: 271 of cap free
Amount of items: 2
Items: 
Size: 596848 Color: 0
Size: 402882 Color: 8

Bin 462: 273 of cap free
Amount of items: 2
Items: 
Size: 520558 Color: 2
Size: 479170 Color: 18

Bin 463: 274 of cap free
Amount of items: 2
Items: 
Size: 761231 Color: 5
Size: 238496 Color: 15

Bin 464: 274 of cap free
Amount of items: 2
Items: 
Size: 543697 Color: 9
Size: 456030 Color: 19

Bin 465: 276 of cap free
Amount of items: 2
Items: 
Size: 760090 Color: 15
Size: 239635 Color: 0

Bin 466: 276 of cap free
Amount of items: 2
Items: 
Size: 539541 Color: 2
Size: 460184 Color: 6

Bin 467: 281 of cap free
Amount of items: 2
Items: 
Size: 568483 Color: 17
Size: 431237 Color: 12

Bin 468: 281 of cap free
Amount of items: 2
Items: 
Size: 741170 Color: 12
Size: 258550 Color: 13

Bin 469: 282 of cap free
Amount of items: 2
Items: 
Size: 640159 Color: 1
Size: 359560 Color: 10

Bin 470: 283 of cap free
Amount of items: 2
Items: 
Size: 718388 Color: 15
Size: 281330 Color: 14

Bin 471: 283 of cap free
Amount of items: 2
Items: 
Size: 775124 Color: 6
Size: 224594 Color: 8

Bin 472: 284 of cap free
Amount of items: 2
Items: 
Size: 533302 Color: 6
Size: 466415 Color: 12

Bin 473: 284 of cap free
Amount of items: 2
Items: 
Size: 607254 Color: 18
Size: 392463 Color: 17

Bin 474: 284 of cap free
Amount of items: 2
Items: 
Size: 698592 Color: 18
Size: 301125 Color: 10

Bin 475: 285 of cap free
Amount of items: 2
Items: 
Size: 691461 Color: 10
Size: 308255 Color: 9

Bin 476: 285 of cap free
Amount of items: 2
Items: 
Size: 577564 Color: 9
Size: 422152 Color: 14

Bin 477: 286 of cap free
Amount of items: 2
Items: 
Size: 618843 Color: 7
Size: 380872 Color: 6

Bin 478: 288 of cap free
Amount of items: 2
Items: 
Size: 794838 Color: 19
Size: 204875 Color: 2

Bin 479: 289 of cap free
Amount of items: 2
Items: 
Size: 602458 Color: 16
Size: 397254 Color: 4

Bin 480: 290 of cap free
Amount of items: 2
Items: 
Size: 502913 Color: 19
Size: 496798 Color: 2

Bin 481: 291 of cap free
Amount of items: 2
Items: 
Size: 614612 Color: 1
Size: 385098 Color: 2

Bin 482: 297 of cap free
Amount of items: 2
Items: 
Size: 701516 Color: 10
Size: 298188 Color: 12

Bin 483: 297 of cap free
Amount of items: 2
Items: 
Size: 575705 Color: 4
Size: 423999 Color: 12

Bin 484: 299 of cap free
Amount of items: 2
Items: 
Size: 672624 Color: 2
Size: 327078 Color: 17

Bin 485: 300 of cap free
Amount of items: 2
Items: 
Size: 745146 Color: 14
Size: 254555 Color: 8

Bin 486: 305 of cap free
Amount of items: 2
Items: 
Size: 703774 Color: 14
Size: 295922 Color: 7

Bin 487: 307 of cap free
Amount of items: 3
Items: 
Size: 772487 Color: 8
Size: 114039 Color: 10
Size: 113168 Color: 19

Bin 488: 308 of cap free
Amount of items: 2
Items: 
Size: 513433 Color: 4
Size: 486260 Color: 8

Bin 489: 309 of cap free
Amount of items: 2
Items: 
Size: 577005 Color: 1
Size: 422687 Color: 9

Bin 490: 309 of cap free
Amount of items: 2
Items: 
Size: 758530 Color: 3
Size: 241162 Color: 13

Bin 491: 310 of cap free
Amount of items: 2
Items: 
Size: 691896 Color: 2
Size: 307795 Color: 6

Bin 492: 311 of cap free
Amount of items: 2
Items: 
Size: 501857 Color: 14
Size: 497833 Color: 15

Bin 493: 316 of cap free
Amount of items: 2
Items: 
Size: 777417 Color: 5
Size: 222268 Color: 9

Bin 494: 319 of cap free
Amount of items: 2
Items: 
Size: 505541 Color: 9
Size: 494141 Color: 18

Bin 495: 320 of cap free
Amount of items: 2
Items: 
Size: 718321 Color: 1
Size: 281360 Color: 11

Bin 496: 321 of cap free
Amount of items: 2
Items: 
Size: 761271 Color: 15
Size: 238409 Color: 11

Bin 497: 322 of cap free
Amount of items: 2
Items: 
Size: 766359 Color: 13
Size: 233320 Color: 19

Bin 498: 323 of cap free
Amount of items: 2
Items: 
Size: 683854 Color: 19
Size: 315824 Color: 6

Bin 499: 324 of cap free
Amount of items: 2
Items: 
Size: 526996 Color: 13
Size: 472681 Color: 2

Bin 500: 324 of cap free
Amount of items: 2
Items: 
Size: 583323 Color: 11
Size: 416354 Color: 10

Bin 501: 326 of cap free
Amount of items: 3
Items: 
Size: 472228 Color: 1
Size: 263900 Color: 5
Size: 263547 Color: 11

Bin 502: 326 of cap free
Amount of items: 2
Items: 
Size: 610968 Color: 12
Size: 388707 Color: 4

Bin 503: 330 of cap free
Amount of items: 2
Items: 
Size: 757579 Color: 16
Size: 242092 Color: 3

Bin 504: 331 of cap free
Amount of items: 2
Items: 
Size: 553942 Color: 17
Size: 445728 Color: 15

Bin 505: 331 of cap free
Amount of items: 2
Items: 
Size: 665359 Color: 5
Size: 334311 Color: 9

Bin 506: 333 of cap free
Amount of items: 2
Items: 
Size: 617332 Color: 19
Size: 382336 Color: 18

Bin 507: 336 of cap free
Amount of items: 2
Items: 
Size: 705966 Color: 5
Size: 293699 Color: 18

Bin 508: 337 of cap free
Amount of items: 2
Items: 
Size: 585820 Color: 8
Size: 413844 Color: 17

Bin 509: 338 of cap free
Amount of items: 2
Items: 
Size: 554736 Color: 16
Size: 444927 Color: 7

Bin 510: 339 of cap free
Amount of items: 2
Items: 
Size: 659325 Color: 3
Size: 340337 Color: 19

Bin 511: 339 of cap free
Amount of items: 2
Items: 
Size: 768269 Color: 12
Size: 231393 Color: 10

Bin 512: 340 of cap free
Amount of items: 2
Items: 
Size: 619847 Color: 16
Size: 379814 Color: 12

Bin 513: 345 of cap free
Amount of items: 2
Items: 
Size: 588759 Color: 2
Size: 410897 Color: 11

Bin 514: 345 of cap free
Amount of items: 2
Items: 
Size: 759679 Color: 8
Size: 239977 Color: 7

Bin 515: 346 of cap free
Amount of items: 2
Items: 
Size: 758699 Color: 13
Size: 240956 Color: 0

Bin 516: 348 of cap free
Amount of items: 2
Items: 
Size: 534692 Color: 16
Size: 464961 Color: 17

Bin 517: 350 of cap free
Amount of items: 2
Items: 
Size: 508106 Color: 17
Size: 491545 Color: 15

Bin 518: 351 of cap free
Amount of items: 2
Items: 
Size: 751330 Color: 8
Size: 248320 Color: 19

Bin 519: 353 of cap free
Amount of items: 2
Items: 
Size: 764507 Color: 8
Size: 235141 Color: 10

Bin 520: 356 of cap free
Amount of items: 2
Items: 
Size: 576328 Color: 1
Size: 423317 Color: 4

Bin 521: 356 of cap free
Amount of items: 2
Items: 
Size: 712519 Color: 6
Size: 287126 Color: 15

Bin 522: 359 of cap free
Amount of items: 3
Items: 
Size: 701248 Color: 12
Size: 149309 Color: 7
Size: 149085 Color: 11

Bin 523: 359 of cap free
Amount of items: 3
Items: 
Size: 627757 Color: 17
Size: 189676 Color: 16
Size: 182209 Color: 18

Bin 524: 359 of cap free
Amount of items: 2
Items: 
Size: 727360 Color: 11
Size: 272282 Color: 7

Bin 525: 360 of cap free
Amount of items: 2
Items: 
Size: 707200 Color: 1
Size: 292441 Color: 11

Bin 526: 361 of cap free
Amount of items: 2
Items: 
Size: 681882 Color: 12
Size: 317758 Color: 13

Bin 527: 362 of cap free
Amount of items: 2
Items: 
Size: 755240 Color: 4
Size: 244399 Color: 14

Bin 528: 363 of cap free
Amount of items: 2
Items: 
Size: 608966 Color: 2
Size: 390672 Color: 7

Bin 529: 363 of cap free
Amount of items: 2
Items: 
Size: 637482 Color: 14
Size: 362156 Color: 18

Bin 530: 363 of cap free
Amount of items: 2
Items: 
Size: 638251 Color: 10
Size: 361387 Color: 14

Bin 531: 364 of cap free
Amount of items: 2
Items: 
Size: 631261 Color: 14
Size: 368376 Color: 7

Bin 532: 367 of cap free
Amount of items: 2
Items: 
Size: 545387 Color: 12
Size: 454247 Color: 5

Bin 533: 370 of cap free
Amount of items: 2
Items: 
Size: 649844 Color: 0
Size: 349787 Color: 10

Bin 534: 376 of cap free
Amount of items: 2
Items: 
Size: 752101 Color: 19
Size: 247524 Color: 17

Bin 535: 377 of cap free
Amount of items: 2
Items: 
Size: 565479 Color: 10
Size: 434145 Color: 11

Bin 536: 384 of cap free
Amount of items: 2
Items: 
Size: 762689 Color: 2
Size: 236928 Color: 5

Bin 537: 385 of cap free
Amount of items: 2
Items: 
Size: 647684 Color: 13
Size: 351932 Color: 10

Bin 538: 386 of cap free
Amount of items: 2
Items: 
Size: 678570 Color: 2
Size: 321045 Color: 14

Bin 539: 388 of cap free
Amount of items: 2
Items: 
Size: 684736 Color: 13
Size: 314877 Color: 2

Bin 540: 392 of cap free
Amount of items: 2
Items: 
Size: 592552 Color: 14
Size: 407057 Color: 13

Bin 541: 392 of cap free
Amount of items: 2
Items: 
Size: 584098 Color: 2
Size: 415511 Color: 12

Bin 542: 393 of cap free
Amount of items: 2
Items: 
Size: 744749 Color: 4
Size: 254859 Color: 15

Bin 543: 394 of cap free
Amount of items: 2
Items: 
Size: 509267 Color: 13
Size: 490340 Color: 11

Bin 544: 399 of cap free
Amount of items: 2
Items: 
Size: 629607 Color: 8
Size: 369995 Color: 16

Bin 545: 399 of cap free
Amount of items: 2
Items: 
Size: 557884 Color: 2
Size: 441718 Color: 5

Bin 546: 400 of cap free
Amount of items: 2
Items: 
Size: 610314 Color: 1
Size: 389287 Color: 11

Bin 547: 406 of cap free
Amount of items: 2
Items: 
Size: 562408 Color: 6
Size: 437187 Color: 16

Bin 548: 410 of cap free
Amount of items: 2
Items: 
Size: 559372 Color: 19
Size: 440219 Color: 7

Bin 549: 414 of cap free
Amount of items: 2
Items: 
Size: 664902 Color: 10
Size: 334685 Color: 8

Bin 550: 416 of cap free
Amount of items: 2
Items: 
Size: 793562 Color: 3
Size: 206023 Color: 4

Bin 551: 418 of cap free
Amount of items: 2
Items: 
Size: 737757 Color: 7
Size: 261826 Color: 6

Bin 552: 419 of cap free
Amount of items: 2
Items: 
Size: 537726 Color: 16
Size: 461856 Color: 3

Bin 553: 425 of cap free
Amount of items: 2
Items: 
Size: 585106 Color: 14
Size: 414470 Color: 2

Bin 554: 427 of cap free
Amount of items: 2
Items: 
Size: 597928 Color: 5
Size: 401646 Color: 9

Bin 555: 434 of cap free
Amount of items: 2
Items: 
Size: 727181 Color: 18
Size: 272386 Color: 11

Bin 556: 435 of cap free
Amount of items: 2
Items: 
Size: 721661 Color: 16
Size: 277905 Color: 18

Bin 557: 436 of cap free
Amount of items: 3
Items: 
Size: 791755 Color: 17
Size: 104280 Color: 3
Size: 103530 Color: 1

Bin 558: 439 of cap free
Amount of items: 2
Items: 
Size: 687682 Color: 17
Size: 311880 Color: 12

Bin 559: 442 of cap free
Amount of items: 2
Items: 
Size: 557236 Color: 19
Size: 442323 Color: 4

Bin 560: 443 of cap free
Amount of items: 2
Items: 
Size: 729514 Color: 12
Size: 270044 Color: 5

Bin 561: 444 of cap free
Amount of items: 2
Items: 
Size: 709847 Color: 8
Size: 289710 Color: 16

Bin 562: 444 of cap free
Amount of items: 2
Items: 
Size: 614079 Color: 5
Size: 385478 Color: 7

Bin 563: 447 of cap free
Amount of items: 2
Items: 
Size: 764025 Color: 17
Size: 235529 Color: 9

Bin 564: 447 of cap free
Amount of items: 2
Items: 
Size: 672985 Color: 17
Size: 326569 Color: 8

Bin 565: 449 of cap free
Amount of items: 3
Items: 
Size: 384002 Color: 18
Size: 310211 Color: 6
Size: 305339 Color: 8

Bin 566: 449 of cap free
Amount of items: 2
Items: 
Size: 595950 Color: 16
Size: 403602 Color: 0

Bin 567: 449 of cap free
Amount of items: 2
Items: 
Size: 666887 Color: 17
Size: 332665 Color: 18

Bin 568: 449 of cap free
Amount of items: 2
Items: 
Size: 538706 Color: 11
Size: 460846 Color: 3

Bin 569: 449 of cap free
Amount of items: 2
Items: 
Size: 681028 Color: 14
Size: 318524 Color: 5

Bin 570: 451 of cap free
Amount of items: 2
Items: 
Size: 568911 Color: 12
Size: 430639 Color: 7

Bin 571: 453 of cap free
Amount of items: 2
Items: 
Size: 769228 Color: 12
Size: 230320 Color: 2

Bin 572: 460 of cap free
Amount of items: 2
Items: 
Size: 574639 Color: 2
Size: 424902 Color: 17

Bin 573: 461 of cap free
Amount of items: 2
Items: 
Size: 528687 Color: 19
Size: 470853 Color: 10

Bin 574: 463 of cap free
Amount of items: 2
Items: 
Size: 664863 Color: 11
Size: 334675 Color: 8

Bin 575: 465 of cap free
Amount of items: 2
Items: 
Size: 766263 Color: 4
Size: 233273 Color: 6

Bin 576: 467 of cap free
Amount of items: 2
Items: 
Size: 518466 Color: 3
Size: 481068 Color: 2

Bin 577: 472 of cap free
Amount of items: 2
Items: 
Size: 737311 Color: 5
Size: 262218 Color: 15

Bin 578: 474 of cap free
Amount of items: 2
Items: 
Size: 719027 Color: 14
Size: 280500 Color: 19

Bin 579: 476 of cap free
Amount of items: 2
Items: 
Size: 715667 Color: 13
Size: 283858 Color: 7

Bin 580: 480 of cap free
Amount of items: 2
Items: 
Size: 659755 Color: 3
Size: 339766 Color: 2

Bin 581: 483 of cap free
Amount of items: 2
Items: 
Size: 516468 Color: 3
Size: 483050 Color: 8

Bin 582: 484 of cap free
Amount of items: 2
Items: 
Size: 710911 Color: 8
Size: 288606 Color: 19

Bin 583: 486 of cap free
Amount of items: 2
Items: 
Size: 762614 Color: 1
Size: 236901 Color: 9

Bin 584: 489 of cap free
Amount of items: 2
Items: 
Size: 669877 Color: 6
Size: 329635 Color: 1

Bin 585: 490 of cap free
Amount of items: 3
Items: 
Size: 473716 Color: 0
Size: 412284 Color: 13
Size: 113511 Color: 4

Bin 586: 490 of cap free
Amount of items: 2
Items: 
Size: 596808 Color: 5
Size: 402703 Color: 9

Bin 587: 492 of cap free
Amount of items: 2
Items: 
Size: 672984 Color: 0
Size: 326525 Color: 1

Bin 588: 494 of cap free
Amount of items: 2
Items: 
Size: 533586 Color: 0
Size: 465921 Color: 6

Bin 589: 496 of cap free
Amount of items: 2
Items: 
Size: 551231 Color: 4
Size: 448274 Color: 15

Bin 590: 497 of cap free
Amount of items: 2
Items: 
Size: 722640 Color: 9
Size: 276864 Color: 13

Bin 591: 499 of cap free
Amount of items: 2
Items: 
Size: 780547 Color: 8
Size: 218955 Color: 4

Bin 592: 501 of cap free
Amount of items: 2
Items: 
Size: 553875 Color: 2
Size: 445625 Color: 12

Bin 593: 503 of cap free
Amount of items: 2
Items: 
Size: 746682 Color: 2
Size: 252816 Color: 6

Bin 594: 509 of cap free
Amount of items: 2
Items: 
Size: 583766 Color: 3
Size: 415726 Color: 1

Bin 595: 513 of cap free
Amount of items: 2
Items: 
Size: 615280 Color: 15
Size: 384208 Color: 3

Bin 596: 515 of cap free
Amount of items: 2
Items: 
Size: 699625 Color: 10
Size: 299861 Color: 12

Bin 597: 519 of cap free
Amount of items: 3
Items: 
Size: 608830 Color: 4
Size: 236291 Color: 11
Size: 154361 Color: 5

Bin 598: 521 of cap free
Amount of items: 2
Items: 
Size: 588689 Color: 2
Size: 410791 Color: 5

Bin 599: 522 of cap free
Amount of items: 2
Items: 
Size: 791931 Color: 17
Size: 207548 Color: 4

Bin 600: 530 of cap free
Amount of items: 2
Items: 
Size: 587178 Color: 13
Size: 412293 Color: 10

Bin 601: 536 of cap free
Amount of items: 2
Items: 
Size: 519062 Color: 1
Size: 480403 Color: 10

Bin 602: 537 of cap free
Amount of items: 2
Items: 
Size: 561413 Color: 14
Size: 438051 Color: 7

Bin 603: 539 of cap free
Amount of items: 2
Items: 
Size: 572688 Color: 13
Size: 426774 Color: 4

Bin 604: 543 of cap free
Amount of items: 2
Items: 
Size: 563218 Color: 19
Size: 436240 Color: 8

Bin 605: 545 of cap free
Amount of items: 2
Items: 
Size: 584035 Color: 0
Size: 415421 Color: 5

Bin 606: 546 of cap free
Amount of items: 2
Items: 
Size: 567349 Color: 9
Size: 432106 Color: 15

Bin 607: 549 of cap free
Amount of items: 2
Items: 
Size: 519177 Color: 7
Size: 480275 Color: 8

Bin 608: 552 of cap free
Amount of items: 2
Items: 
Size: 523082 Color: 3
Size: 476367 Color: 1

Bin 609: 558 of cap free
Amount of items: 2
Items: 
Size: 651119 Color: 6
Size: 348324 Color: 11

Bin 610: 561 of cap free
Amount of items: 2
Items: 
Size: 619836 Color: 13
Size: 379604 Color: 12

Bin 611: 562 of cap free
Amount of items: 2
Items: 
Size: 640097 Color: 0
Size: 359342 Color: 1

Bin 612: 567 of cap free
Amount of items: 2
Items: 
Size: 515138 Color: 5
Size: 484296 Color: 6

Bin 613: 580 of cap free
Amount of items: 2
Items: 
Size: 646788 Color: 19
Size: 352633 Color: 14

Bin 614: 587 of cap free
Amount of items: 2
Items: 
Size: 603856 Color: 4
Size: 395558 Color: 10

Bin 615: 589 of cap free
Amount of items: 2
Items: 
Size: 733923 Color: 7
Size: 265489 Color: 0

Bin 616: 593 of cap free
Amount of items: 2
Items: 
Size: 769910 Color: 15
Size: 229498 Color: 17

Bin 617: 596 of cap free
Amount of items: 2
Items: 
Size: 639996 Color: 1
Size: 359409 Color: 11

Bin 618: 600 of cap free
Amount of items: 2
Items: 
Size: 641542 Color: 15
Size: 357859 Color: 1

Bin 619: 600 of cap free
Amount of items: 2
Items: 
Size: 757561 Color: 5
Size: 241840 Color: 4

Bin 620: 606 of cap free
Amount of items: 2
Items: 
Size: 717352 Color: 0
Size: 282043 Color: 13

Bin 621: 606 of cap free
Amount of items: 2
Items: 
Size: 551743 Color: 18
Size: 447652 Color: 3

Bin 622: 616 of cap free
Amount of items: 2
Items: 
Size: 554699 Color: 1
Size: 444686 Color: 17

Bin 623: 619 of cap free
Amount of items: 2
Items: 
Size: 544175 Color: 9
Size: 455207 Color: 3

Bin 624: 626 of cap free
Amount of items: 2
Items: 
Size: 678368 Color: 17
Size: 321007 Color: 11

Bin 625: 630 of cap free
Amount of items: 3
Items: 
Size: 639394 Color: 14
Size: 181308 Color: 0
Size: 178669 Color: 11

Bin 626: 632 of cap free
Amount of items: 2
Items: 
Size: 633985 Color: 6
Size: 365384 Color: 7

Bin 627: 635 of cap free
Amount of items: 2
Items: 
Size: 798344 Color: 14
Size: 201022 Color: 1

Bin 628: 646 of cap free
Amount of items: 2
Items: 
Size: 747766 Color: 10
Size: 251589 Color: 0

Bin 629: 646 of cap free
Amount of items: 2
Items: 
Size: 666976 Color: 18
Size: 332379 Color: 4

Bin 630: 655 of cap free
Amount of items: 2
Items: 
Size: 553788 Color: 10
Size: 445558 Color: 12

Bin 631: 656 of cap free
Amount of items: 2
Items: 
Size: 756869 Color: 4
Size: 242476 Color: 12

Bin 632: 661 of cap free
Amount of items: 2
Items: 
Size: 504773 Color: 17
Size: 494567 Color: 19

Bin 633: 669 of cap free
Amount of items: 2
Items: 
Size: 583153 Color: 0
Size: 416179 Color: 16

Bin 634: 673 of cap free
Amount of items: 2
Items: 
Size: 542862 Color: 8
Size: 456466 Color: 16

Bin 635: 675 of cap free
Amount of items: 2
Items: 
Size: 611854 Color: 5
Size: 387472 Color: 8

Bin 636: 675 of cap free
Amount of items: 2
Items: 
Size: 748025 Color: 0
Size: 251301 Color: 14

Bin 637: 676 of cap free
Amount of items: 2
Items: 
Size: 621712 Color: 7
Size: 377613 Color: 6

Bin 638: 680 of cap free
Amount of items: 2
Items: 
Size: 685735 Color: 8
Size: 313586 Color: 4

Bin 639: 685 of cap free
Amount of items: 2
Items: 
Size: 510189 Color: 16
Size: 489127 Color: 8

Bin 640: 688 of cap free
Amount of items: 2
Items: 
Size: 620169 Color: 2
Size: 379144 Color: 14

Bin 641: 689 of cap free
Amount of items: 2
Items: 
Size: 642232 Color: 15
Size: 357080 Color: 7

Bin 642: 696 of cap free
Amount of items: 2
Items: 
Size: 705153 Color: 0
Size: 294152 Color: 2

Bin 643: 699 of cap free
Amount of items: 2
Items: 
Size: 508135 Color: 15
Size: 491167 Color: 7

Bin 644: 700 of cap free
Amount of items: 2
Items: 
Size: 626519 Color: 4
Size: 372782 Color: 9

Bin 645: 704 of cap free
Amount of items: 2
Items: 
Size: 638103 Color: 5
Size: 361194 Color: 15

Bin 646: 720 of cap free
Amount of items: 2
Items: 
Size: 546039 Color: 1
Size: 453242 Color: 5

Bin 647: 723 of cap free
Amount of items: 2
Items: 
Size: 679159 Color: 12
Size: 320119 Color: 1

Bin 648: 727 of cap free
Amount of items: 2
Items: 
Size: 574566 Color: 5
Size: 424708 Color: 13

Bin 649: 734 of cap free
Amount of items: 2
Items: 
Size: 790478 Color: 4
Size: 208789 Color: 3

Bin 650: 735 of cap free
Amount of items: 2
Items: 
Size: 597644 Color: 12
Size: 401622 Color: 9

Bin 651: 737 of cap free
Amount of items: 2
Items: 
Size: 799471 Color: 8
Size: 199793 Color: 10

Bin 652: 740 of cap free
Amount of items: 2
Items: 
Size: 555454 Color: 19
Size: 443807 Color: 3

Bin 653: 742 of cap free
Amount of items: 2
Items: 
Size: 783162 Color: 3
Size: 216097 Color: 6

Bin 654: 744 of cap free
Amount of items: 2
Items: 
Size: 749879 Color: 11
Size: 249378 Color: 4

Bin 655: 745 of cap free
Amount of items: 2
Items: 
Size: 531919 Color: 10
Size: 467337 Color: 16

Bin 656: 746 of cap free
Amount of items: 2
Items: 
Size: 573484 Color: 7
Size: 425771 Color: 3

Bin 657: 747 of cap free
Amount of items: 2
Items: 
Size: 507154 Color: 12
Size: 492100 Color: 0

Bin 658: 748 of cap free
Amount of items: 2
Items: 
Size: 681657 Color: 14
Size: 317596 Color: 19

Bin 659: 750 of cap free
Amount of items: 2
Items: 
Size: 593031 Color: 1
Size: 406220 Color: 7

Bin 660: 754 of cap free
Amount of items: 2
Items: 
Size: 768269 Color: 13
Size: 230978 Color: 14

Bin 661: 762 of cap free
Amount of items: 2
Items: 
Size: 564298 Color: 5
Size: 434941 Color: 13

Bin 662: 765 of cap free
Amount of items: 2
Items: 
Size: 777148 Color: 3
Size: 222088 Color: 17

Bin 663: 767 of cap free
Amount of items: 2
Items: 
Size: 558313 Color: 3
Size: 440921 Color: 12

Bin 664: 769 of cap free
Amount of items: 2
Items: 
Size: 520372 Color: 15
Size: 478860 Color: 9

Bin 665: 770 of cap free
Amount of items: 2
Items: 
Size: 684632 Color: 16
Size: 314599 Color: 19

Bin 666: 774 of cap free
Amount of items: 2
Items: 
Size: 676248 Color: 9
Size: 322979 Color: 14

Bin 667: 789 of cap free
Amount of items: 2
Items: 
Size: 605593 Color: 7
Size: 393619 Color: 6

Bin 668: 791 of cap free
Amount of items: 2
Items: 
Size: 599761 Color: 14
Size: 399449 Color: 15

Bin 669: 793 of cap free
Amount of items: 2
Items: 
Size: 577892 Color: 2
Size: 421316 Color: 19

Bin 670: 795 of cap free
Amount of items: 3
Items: 
Size: 797113 Color: 12
Size: 101149 Color: 3
Size: 100944 Color: 19

Bin 671: 795 of cap free
Amount of items: 2
Items: 
Size: 581793 Color: 11
Size: 417413 Color: 16

Bin 672: 796 of cap free
Amount of items: 2
Items: 
Size: 527814 Color: 6
Size: 471391 Color: 5

Bin 673: 806 of cap free
Amount of items: 2
Items: 
Size: 595940 Color: 18
Size: 403255 Color: 1

Bin 674: 814 of cap free
Amount of items: 2
Items: 
Size: 637166 Color: 2
Size: 362021 Color: 17

Bin 675: 815 of cap free
Amount of items: 2
Items: 
Size: 656849 Color: 19
Size: 342337 Color: 11

Bin 676: 826 of cap free
Amount of items: 2
Items: 
Size: 748940 Color: 2
Size: 250235 Color: 13

Bin 677: 836 of cap free
Amount of items: 2
Items: 
Size: 755697 Color: 7
Size: 243468 Color: 1

Bin 678: 837 of cap free
Amount of items: 2
Items: 
Size: 620409 Color: 19
Size: 378755 Color: 8

Bin 679: 847 of cap free
Amount of items: 2
Items: 
Size: 688748 Color: 12
Size: 310406 Color: 13

Bin 680: 847 of cap free
Amount of items: 2
Items: 
Size: 649591 Color: 9
Size: 349563 Color: 12

Bin 681: 854 of cap free
Amount of items: 2
Items: 
Size: 789112 Color: 4
Size: 210035 Color: 15

Bin 682: 857 of cap free
Amount of items: 2
Items: 
Size: 733890 Color: 7
Size: 265254 Color: 5

Bin 683: 869 of cap free
Amount of items: 2
Items: 
Size: 655786 Color: 1
Size: 343346 Color: 2

Bin 684: 872 of cap free
Amount of items: 2
Items: 
Size: 611850 Color: 7
Size: 387279 Color: 6

Bin 685: 877 of cap free
Amount of items: 2
Items: 
Size: 599834 Color: 17
Size: 399290 Color: 10

Bin 686: 885 of cap free
Amount of items: 2
Items: 
Size: 630777 Color: 7
Size: 368339 Color: 15

Bin 687: 895 of cap free
Amount of items: 2
Items: 
Size: 560549 Color: 15
Size: 438557 Color: 12

Bin 688: 913 of cap free
Amount of items: 2
Items: 
Size: 629080 Color: 12
Size: 370008 Color: 6

Bin 689: 916 of cap free
Amount of items: 2
Items: 
Size: 568449 Color: 14
Size: 430636 Color: 18

Bin 690: 920 of cap free
Amount of items: 2
Items: 
Size: 652027 Color: 7
Size: 347054 Color: 6

Bin 691: 928 of cap free
Amount of items: 2
Items: 
Size: 597427 Color: 9
Size: 401646 Color: 7

Bin 692: 931 of cap free
Amount of items: 2
Items: 
Size: 630780 Color: 15
Size: 368290 Color: 14

Bin 693: 933 of cap free
Amount of items: 2
Items: 
Size: 656707 Color: 3
Size: 342361 Color: 19

Bin 694: 933 of cap free
Amount of items: 2
Items: 
Size: 671393 Color: 14
Size: 327675 Color: 7

Bin 695: 939 of cap free
Amount of items: 2
Items: 
Size: 790474 Color: 17
Size: 208588 Color: 6

Bin 696: 947 of cap free
Amount of items: 2
Items: 
Size: 533292 Color: 6
Size: 465762 Color: 3

Bin 697: 948 of cap free
Amount of items: 2
Items: 
Size: 755703 Color: 10
Size: 243350 Color: 6

Bin 698: 959 of cap free
Amount of items: 2
Items: 
Size: 681512 Color: 19
Size: 317530 Color: 10

Bin 699: 959 of cap free
Amount of items: 2
Items: 
Size: 654767 Color: 15
Size: 344275 Color: 2

Bin 700: 963 of cap free
Amount of items: 2
Items: 
Size: 708282 Color: 13
Size: 290756 Color: 0

Bin 701: 966 of cap free
Amount of items: 2
Items: 
Size: 786974 Color: 17
Size: 212061 Color: 15

Bin 702: 972 of cap free
Amount of items: 2
Items: 
Size: 703052 Color: 10
Size: 295977 Color: 7

Bin 703: 975 of cap free
Amount of items: 2
Items: 
Size: 617124 Color: 15
Size: 381902 Color: 12

Bin 704: 983 of cap free
Amount of items: 2
Items: 
Size: 643747 Color: 10
Size: 355271 Color: 12

Bin 705: 983 of cap free
Amount of items: 2
Items: 
Size: 601896 Color: 17
Size: 397122 Color: 14

Bin 706: 991 of cap free
Amount of items: 2
Items: 
Size: 759095 Color: 10
Size: 239915 Color: 9

Bin 707: 998 of cap free
Amount of items: 2
Items: 
Size: 589476 Color: 9
Size: 409527 Color: 16

Bin 708: 1005 of cap free
Amount of items: 2
Items: 
Size: 524521 Color: 9
Size: 474475 Color: 17

Bin 709: 1008 of cap free
Amount of items: 2
Items: 
Size: 590908 Color: 7
Size: 408085 Color: 17

Bin 710: 1022 of cap free
Amount of items: 2
Items: 
Size: 567182 Color: 10
Size: 431797 Color: 0

Bin 711: 1024 of cap free
Amount of items: 2
Items: 
Size: 567049 Color: 9
Size: 431928 Color: 10

Bin 712: 1033 of cap free
Amount of items: 2
Items: 
Size: 664863 Color: 14
Size: 334105 Color: 12

Bin 713: 1045 of cap free
Amount of items: 2
Items: 
Size: 672440 Color: 2
Size: 326516 Color: 11

Bin 714: 1050 of cap free
Amount of items: 2
Items: 
Size: 500320 Color: 9
Size: 498631 Color: 16

Bin 715: 1055 of cap free
Amount of items: 2
Items: 
Size: 581559 Color: 1
Size: 417387 Color: 7

Bin 716: 1060 of cap free
Amount of items: 2
Items: 
Size: 730869 Color: 1
Size: 268072 Color: 19

Bin 717: 1063 of cap free
Amount of items: 2
Items: 
Size: 606558 Color: 14
Size: 392380 Color: 10

Bin 718: 1064 of cap free
Amount of items: 2
Items: 
Size: 714199 Color: 8
Size: 284738 Color: 0

Bin 719: 1065 of cap free
Amount of items: 2
Items: 
Size: 724274 Color: 3
Size: 274662 Color: 17

Bin 720: 1066 of cap free
Amount of items: 2
Items: 
Size: 662899 Color: 7
Size: 336036 Color: 3

Bin 721: 1066 of cap free
Amount of items: 2
Items: 
Size: 529238 Color: 19
Size: 469697 Color: 1

Bin 722: 1079 of cap free
Amount of items: 2
Items: 
Size: 612766 Color: 15
Size: 386156 Color: 8

Bin 723: 1094 of cap free
Amount of items: 2
Items: 
Size: 618472 Color: 18
Size: 380435 Color: 19

Bin 724: 1100 of cap free
Amount of items: 3
Items: 
Size: 640669 Color: 17
Size: 193402 Color: 14
Size: 164830 Color: 5

Bin 725: 1112 of cap free
Amount of items: 2
Items: 
Size: 659212 Color: 16
Size: 339677 Color: 6

Bin 726: 1124 of cap free
Amount of items: 2
Items: 
Size: 643170 Color: 2
Size: 355707 Color: 12

Bin 727: 1137 of cap free
Amount of items: 2
Items: 
Size: 627643 Color: 9
Size: 371221 Color: 15

Bin 728: 1140 of cap free
Amount of items: 2
Items: 
Size: 753261 Color: 18
Size: 245600 Color: 1

Bin 729: 1157 of cap free
Amount of items: 2
Items: 
Size: 775114 Color: 14
Size: 223730 Color: 6

Bin 730: 1168 of cap free
Amount of items: 2
Items: 
Size: 749837 Color: 10
Size: 248996 Color: 15

Bin 731: 1174 of cap free
Amount of items: 2
Items: 
Size: 692705 Color: 3
Size: 306122 Color: 0

Bin 732: 1175 of cap free
Amount of items: 2
Items: 
Size: 730487 Color: 2
Size: 268339 Color: 1

Bin 733: 1183 of cap free
Amount of items: 2
Items: 
Size: 638086 Color: 0
Size: 360732 Color: 1

Bin 734: 1184 of cap free
Amount of items: 2
Items: 
Size: 594728 Color: 5
Size: 404089 Color: 2

Bin 735: 1189 of cap free
Amount of items: 2
Items: 
Size: 535527 Color: 1
Size: 463285 Color: 15

Bin 736: 1193 of cap free
Amount of items: 3
Items: 
Size: 491915 Color: 6
Size: 349493 Color: 7
Size: 157400 Color: 16

Bin 737: 1219 of cap free
Amount of items: 2
Items: 
Size: 714080 Color: 5
Size: 284702 Color: 11

Bin 738: 1228 of cap free
Amount of items: 2
Items: 
Size: 535467 Color: 15
Size: 463306 Color: 16

Bin 739: 1229 of cap free
Amount of items: 2
Items: 
Size: 738653 Color: 1
Size: 260119 Color: 2

Bin 740: 1232 of cap free
Amount of items: 2
Items: 
Size: 520126 Color: 1
Size: 478643 Color: 2

Bin 741: 1235 of cap free
Amount of items: 2
Items: 
Size: 504652 Color: 8
Size: 494114 Color: 14

Bin 742: 1252 of cap free
Amount of items: 2
Items: 
Size: 782926 Color: 15
Size: 215823 Color: 17

Bin 743: 1257 of cap free
Amount of items: 2
Items: 
Size: 659173 Color: 3
Size: 339571 Color: 12

Bin 744: 1266 of cap free
Amount of items: 2
Items: 
Size: 778821 Color: 10
Size: 219914 Color: 15

Bin 745: 1279 of cap free
Amount of items: 2
Items: 
Size: 706763 Color: 6
Size: 291959 Color: 7

Bin 746: 1282 of cap free
Amount of items: 2
Items: 
Size: 590575 Color: 3
Size: 408144 Color: 12

Bin 747: 1295 of cap free
Amount of items: 2
Items: 
Size: 619594 Color: 14
Size: 379112 Color: 15

Bin 748: 1301 of cap free
Amount of items: 2
Items: 
Size: 630771 Color: 0
Size: 367929 Color: 11

Bin 749: 1330 of cap free
Amount of items: 2
Items: 
Size: 733387 Color: 12
Size: 265284 Color: 3

Bin 750: 1337 of cap free
Amount of items: 2
Items: 
Size: 518313 Color: 0
Size: 480351 Color: 13

Bin 751: 1337 of cap free
Amount of items: 2
Items: 
Size: 571907 Color: 13
Size: 426757 Color: 11

Bin 752: 1339 of cap free
Amount of items: 2
Items: 
Size: 786494 Color: 2
Size: 212168 Color: 9

Bin 753: 1342 of cap free
Amount of items: 2
Items: 
Size: 522501 Color: 8
Size: 476158 Color: 17

Bin 754: 1355 of cap free
Amount of items: 2
Items: 
Size: 786065 Color: 12
Size: 212581 Color: 7

Bin 755: 1383 of cap free
Amount of items: 2
Items: 
Size: 528656 Color: 7
Size: 469962 Color: 19

Bin 756: 1404 of cap free
Amount of items: 2
Items: 
Size: 675898 Color: 7
Size: 322699 Color: 15

Bin 757: 1418 of cap free
Amount of items: 2
Items: 
Size: 769798 Color: 0
Size: 228785 Color: 15

Bin 758: 1419 of cap free
Amount of items: 2
Items: 
Size: 552779 Color: 15
Size: 445803 Color: 13

Bin 759: 1422 of cap free
Amount of items: 2
Items: 
Size: 726303 Color: 15
Size: 272276 Color: 2

Bin 760: 1424 of cap free
Amount of items: 2
Items: 
Size: 641438 Color: 18
Size: 357139 Color: 6

Bin 761: 1430 of cap free
Amount of items: 2
Items: 
Size: 714133 Color: 17
Size: 284438 Color: 14

Bin 762: 1458 of cap free
Amount of items: 2
Items: 
Size: 717297 Color: 15
Size: 281246 Color: 8

Bin 763: 1468 of cap free
Amount of items: 2
Items: 
Size: 646769 Color: 10
Size: 351764 Color: 15

Bin 764: 1493 of cap free
Amount of items: 2
Items: 
Size: 623043 Color: 3
Size: 375465 Color: 16

Bin 765: 1504 of cap free
Amount of items: 2
Items: 
Size: 583085 Color: 4
Size: 415412 Color: 12

Bin 766: 1511 of cap free
Amount of items: 2
Items: 
Size: 675932 Color: 17
Size: 322558 Color: 1

Bin 767: 1537 of cap free
Amount of items: 2
Items: 
Size: 499904 Color: 7
Size: 498560 Color: 18

Bin 768: 1550 of cap free
Amount of items: 2
Items: 
Size: 744182 Color: 5
Size: 254269 Color: 0

Bin 769: 1552 of cap free
Amount of items: 2
Items: 
Size: 733232 Color: 5
Size: 265217 Color: 16

Bin 770: 1565 of cap free
Amount of items: 2
Items: 
Size: 769107 Color: 6
Size: 229329 Color: 8

Bin 771: 1579 of cap free
Amount of items: 2
Items: 
Size: 512879 Color: 3
Size: 485543 Color: 10

Bin 772: 1599 of cap free
Amount of items: 2
Items: 
Size: 571782 Color: 19
Size: 426620 Color: 16

Bin 773: 1643 of cap free
Amount of items: 3
Items: 
Size: 636103 Color: 16
Size: 195545 Color: 8
Size: 166710 Color: 3

Bin 774: 1643 of cap free
Amount of items: 2
Items: 
Size: 791547 Color: 11
Size: 206811 Color: 17

Bin 775: 1650 of cap free
Amount of items: 2
Items: 
Size: 592407 Color: 5
Size: 405944 Color: 15

Bin 776: 1662 of cap free
Amount of items: 2
Items: 
Size: 762118 Color: 8
Size: 236221 Color: 15

Bin 777: 1668 of cap free
Amount of items: 2
Items: 
Size: 783138 Color: 16
Size: 215195 Color: 8

Bin 778: 1678 of cap free
Amount of items: 2
Items: 
Size: 751327 Color: 17
Size: 246996 Color: 4

Bin 779: 1682 of cap free
Amount of items: 2
Items: 
Size: 548316 Color: 7
Size: 450003 Color: 14

Bin 780: 1694 of cap free
Amount of items: 2
Items: 
Size: 569682 Color: 18
Size: 428625 Color: 13

Bin 781: 1724 of cap free
Amount of items: 2
Items: 
Size: 565108 Color: 13
Size: 433169 Color: 14

Bin 782: 1741 of cap free
Amount of items: 2
Items: 
Size: 520213 Color: 2
Size: 478047 Color: 4

Bin 783: 1742 of cap free
Amount of items: 2
Items: 
Size: 538231 Color: 14
Size: 460028 Color: 4

Bin 784: 1752 of cap free
Amount of items: 2
Items: 
Size: 506352 Color: 1
Size: 491897 Color: 9

Bin 785: 1780 of cap free
Amount of items: 2
Items: 
Size: 712990 Color: 13
Size: 285231 Color: 7

Bin 786: 1788 of cap free
Amount of items: 2
Items: 
Size: 504257 Color: 17
Size: 493956 Color: 10

Bin 787: 1789 of cap free
Amount of items: 2
Items: 
Size: 675907 Color: 5
Size: 322305 Color: 3

Bin 788: 1807 of cap free
Amount of items: 2
Items: 
Size: 603418 Color: 16
Size: 394776 Color: 12

Bin 789: 1878 of cap free
Amount of items: 2
Items: 
Size: 740669 Color: 14
Size: 257454 Color: 13

Bin 790: 1891 of cap free
Amount of items: 3
Items: 
Size: 621988 Color: 13
Size: 197778 Color: 9
Size: 178344 Color: 5

Bin 791: 1895 of cap free
Amount of items: 2
Items: 
Size: 664367 Color: 10
Size: 333739 Color: 2

Bin 792: 1918 of cap free
Amount of items: 2
Items: 
Size: 778798 Color: 0
Size: 219285 Color: 16

Bin 793: 1938 of cap free
Amount of items: 2
Items: 
Size: 798041 Color: 6
Size: 200022 Color: 8

Bin 794: 1950 of cap free
Amount of items: 2
Items: 
Size: 702393 Color: 2
Size: 295658 Color: 3

Bin 795: 1981 of cap free
Amount of items: 2
Items: 
Size: 695501 Color: 8
Size: 302519 Color: 19

Bin 796: 1982 of cap free
Amount of items: 2
Items: 
Size: 630271 Color: 12
Size: 367748 Color: 14

Bin 797: 2039 of cap free
Amount of items: 2
Items: 
Size: 637072 Color: 7
Size: 360890 Color: 0

Bin 798: 2049 of cap free
Amount of items: 2
Items: 
Size: 591936 Color: 2
Size: 406016 Color: 4

Bin 799: 2077 of cap free
Amount of items: 2
Items: 
Size: 751074 Color: 16
Size: 246850 Color: 12

Bin 800: 2090 of cap free
Amount of items: 2
Items: 
Size: 560798 Color: 12
Size: 437113 Color: 2

Bin 801: 2115 of cap free
Amount of items: 2
Items: 
Size: 785865 Color: 17
Size: 212021 Color: 15

Bin 802: 2136 of cap free
Amount of items: 3
Items: 
Size: 428582 Color: 5
Size: 412929 Color: 17
Size: 156354 Color: 6

Bin 803: 2136 of cap free
Amount of items: 2
Items: 
Size: 592236 Color: 2
Size: 405629 Color: 3

Bin 804: 2146 of cap free
Amount of items: 2
Items: 
Size: 730098 Color: 3
Size: 267757 Color: 13

Bin 805: 2148 of cap free
Amount of items: 2
Items: 
Size: 576780 Color: 18
Size: 421073 Color: 10

Bin 806: 2154 of cap free
Amount of items: 2
Items: 
Size: 773582 Color: 19
Size: 224265 Color: 15

Bin 807: 2179 of cap free
Amount of items: 2
Items: 
Size: 725880 Color: 11
Size: 271942 Color: 8

Bin 808: 2191 of cap free
Amount of items: 2
Items: 
Size: 726263 Color: 1
Size: 271547 Color: 10

Bin 809: 2199 of cap free
Amount of items: 2
Items: 
Size: 785665 Color: 6
Size: 212137 Color: 3

Bin 810: 2218 of cap free
Amount of items: 2
Items: 
Size: 630270 Color: 10
Size: 367513 Color: 15

Bin 811: 2247 of cap free
Amount of items: 2
Items: 
Size: 680394 Color: 13
Size: 317360 Color: 17

Bin 812: 2249 of cap free
Amount of items: 2
Items: 
Size: 675622 Color: 15
Size: 322130 Color: 19

Bin 813: 2279 of cap free
Amount of items: 2
Items: 
Size: 760830 Color: 5
Size: 236892 Color: 11

Bin 814: 2280 of cap free
Amount of items: 2
Items: 
Size: 791878 Color: 10
Size: 205843 Color: 6

Bin 815: 2286 of cap free
Amount of items: 2
Items: 
Size: 769094 Color: 0
Size: 228621 Color: 19

Bin 816: 2286 of cap free
Amount of items: 2
Items: 
Size: 577454 Color: 19
Size: 420261 Color: 14

Bin 817: 2302 of cap free
Amount of items: 2
Items: 
Size: 701849 Color: 3
Size: 295850 Color: 12

Bin 818: 2330 of cap free
Amount of items: 2
Items: 
Size: 705940 Color: 1
Size: 291731 Color: 16

Bin 819: 2334 of cap free
Amount of items: 2
Items: 
Size: 612761 Color: 12
Size: 384906 Color: 8

Bin 820: 2342 of cap free
Amount of items: 2
Items: 
Size: 512384 Color: 10
Size: 485275 Color: 0

Bin 821: 2354 of cap free
Amount of items: 2
Items: 
Size: 765785 Color: 5
Size: 231862 Color: 18

Bin 822: 2395 of cap free
Amount of items: 2
Items: 
Size: 774196 Color: 17
Size: 223410 Color: 6

Bin 823: 2436 of cap free
Amount of items: 2
Items: 
Size: 675458 Color: 0
Size: 322107 Color: 19

Bin 824: 2462 of cap free
Amount of items: 2
Items: 
Size: 709683 Color: 4
Size: 287856 Color: 0

Bin 825: 2470 of cap free
Amount of items: 2
Items: 
Size: 753217 Color: 0
Size: 244314 Color: 3

Bin 826: 2479 of cap free
Amount of items: 2
Items: 
Size: 534277 Color: 6
Size: 463245 Color: 15

Bin 827: 2482 of cap free
Amount of items: 2
Items: 
Size: 785425 Color: 11
Size: 212094 Color: 0

Bin 828: 2491 of cap free
Amount of items: 2
Items: 
Size: 617100 Color: 18
Size: 380410 Color: 7

Bin 829: 2512 of cap free
Amount of items: 2
Items: 
Size: 679992 Color: 0
Size: 317497 Color: 2

Bin 830: 2513 of cap free
Amount of items: 2
Items: 
Size: 709685 Color: 7
Size: 287803 Color: 9

Bin 831: 2536 of cap free
Amount of items: 2
Items: 
Size: 524225 Color: 1
Size: 473240 Color: 0

Bin 832: 2554 of cap free
Amount of items: 2
Items: 
Size: 649193 Color: 7
Size: 348254 Color: 1

Bin 833: 2621 of cap free
Amount of items: 2
Items: 
Size: 785914 Color: 15
Size: 211466 Color: 9

Bin 834: 2628 of cap free
Amount of items: 2
Items: 
Size: 774111 Color: 2
Size: 223262 Color: 19

Bin 835: 2664 of cap free
Amount of items: 2
Items: 
Size: 517031 Color: 13
Size: 480306 Color: 14

Bin 836: 2672 of cap free
Amount of items: 2
Items: 
Size: 746134 Color: 2
Size: 251195 Color: 18

Bin 837: 2729 of cap free
Amount of items: 2
Items: 
Size: 564122 Color: 0
Size: 433150 Color: 7

Bin 838: 2742 of cap free
Amount of items: 2
Items: 
Size: 797582 Color: 6
Size: 199677 Color: 10

Bin 839: 2804 of cap free
Amount of items: 2
Items: 
Size: 576953 Color: 8
Size: 420244 Color: 14

Bin 840: 2817 of cap free
Amount of items: 2
Items: 
Size: 778733 Color: 2
Size: 218451 Color: 5

Bin 841: 2846 of cap free
Amount of items: 2
Items: 
Size: 583491 Color: 10
Size: 413664 Color: 15

Bin 842: 2856 of cap free
Amount of items: 2
Items: 
Size: 695036 Color: 6
Size: 302109 Color: 2

Bin 843: 2878 of cap free
Amount of items: 2
Items: 
Size: 630394 Color: 7
Size: 366729 Color: 6

Bin 844: 2891 of cap free
Amount of items: 2
Items: 
Size: 722483 Color: 9
Size: 274627 Color: 1

Bin 845: 2931 of cap free
Amount of items: 2
Items: 
Size: 560481 Color: 19
Size: 436589 Color: 18

Bin 846: 3025 of cap free
Amount of items: 2
Items: 
Size: 684219 Color: 11
Size: 312757 Color: 1

Bin 847: 3036 of cap free
Amount of items: 2
Items: 
Size: 603376 Color: 17
Size: 393589 Color: 9

Bin 848: 3240 of cap free
Amount of items: 2
Items: 
Size: 530856 Color: 15
Size: 465905 Color: 8

Bin 849: 3274 of cap free
Amount of items: 2
Items: 
Size: 592498 Color: 9
Size: 404229 Color: 4

Bin 850: 3318 of cap free
Amount of items: 2
Items: 
Size: 536916 Color: 11
Size: 459767 Color: 3

Bin 851: 3344 of cap free
Amount of items: 2
Items: 
Size: 516281 Color: 14
Size: 480376 Color: 13

Bin 852: 3376 of cap free
Amount of items: 2
Items: 
Size: 690063 Color: 2
Size: 306562 Color: 4

Bin 853: 3384 of cap free
Amount of items: 2
Items: 
Size: 768175 Color: 10
Size: 228442 Color: 17

Bin 854: 3414 of cap free
Amount of items: 2
Items: 
Size: 511588 Color: 11
Size: 484999 Color: 15

Bin 855: 3451 of cap free
Amount of items: 2
Items: 
Size: 581138 Color: 3
Size: 415412 Color: 7

Bin 856: 3484 of cap free
Amount of items: 2
Items: 
Size: 740579 Color: 17
Size: 255938 Color: 8

Bin 857: 3553 of cap free
Amount of items: 2
Items: 
Size: 552735 Color: 14
Size: 443713 Color: 2

Bin 858: 3601 of cap free
Amount of items: 2
Items: 
Size: 588316 Color: 18
Size: 408084 Color: 5

Bin 859: 3606 of cap free
Amount of items: 2
Items: 
Size: 785246 Color: 8
Size: 211149 Color: 19

Bin 860: 3635 of cap free
Amount of items: 2
Items: 
Size: 785011 Color: 3
Size: 211355 Color: 11

Bin 861: 3696 of cap free
Amount of items: 2
Items: 
Size: 797082 Color: 2
Size: 199223 Color: 12

Bin 862: 3716 of cap free
Amount of items: 2
Items: 
Size: 532890 Color: 12
Size: 463395 Color: 5

Bin 863: 3790 of cap free
Amount of items: 2
Items: 
Size: 581251 Color: 2
Size: 414960 Color: 18

Bin 864: 3827 of cap free
Amount of items: 2
Items: 
Size: 796723 Color: 16
Size: 199451 Color: 9

Bin 865: 3976 of cap free
Amount of items: 2
Items: 
Size: 552386 Color: 1
Size: 443639 Color: 15

Bin 866: 3980 of cap free
Amount of items: 2
Items: 
Size: 571575 Color: 4
Size: 424446 Color: 18

Bin 867: 3981 of cap free
Amount of items: 2
Items: 
Size: 649260 Color: 13
Size: 346760 Color: 10

Bin 868: 4765 of cap free
Amount of items: 2
Items: 
Size: 559331 Color: 2
Size: 435905 Color: 1

Bin 869: 4814 of cap free
Amount of items: 2
Items: 
Size: 649166 Color: 7
Size: 346021 Color: 8

Bin 870: 4892 of cap free
Amount of items: 2
Items: 
Size: 510994 Color: 6
Size: 484115 Color: 7

Bin 871: 5019 of cap free
Amount of items: 2
Items: 
Size: 497606 Color: 12
Size: 497376 Color: 0

Bin 872: 5308 of cap free
Amount of items: 2
Items: 
Size: 766816 Color: 19
Size: 227877 Color: 17

Bin 873: 5403 of cap free
Amount of items: 2
Items: 
Size: 610809 Color: 5
Size: 383789 Color: 15

Bin 874: 5432 of cap free
Amount of items: 2
Items: 
Size: 714813 Color: 3
Size: 279756 Color: 2

Bin 875: 5716 of cap free
Amount of items: 2
Items: 
Size: 688508 Color: 16
Size: 305777 Color: 8

Bin 876: 5849 of cap free
Amount of items: 3
Items: 
Size: 741729 Color: 0
Size: 134369 Color: 18
Size: 118054 Color: 16

Bin 877: 5910 of cap free
Amount of items: 2
Items: 
Size: 513993 Color: 15
Size: 480098 Color: 14

Bin 878: 6168 of cap free
Amount of items: 2
Items: 
Size: 621316 Color: 19
Size: 372517 Color: 13

Bin 879: 6295 of cap free
Amount of items: 2
Items: 
Size: 580330 Color: 6
Size: 413376 Color: 19

Bin 880: 6314 of cap free
Amount of items: 3
Items: 
Size: 458382 Color: 16
Size: 290977 Color: 3
Size: 244328 Color: 18

Bin 881: 6503 of cap free
Amount of items: 2
Items: 
Size: 765187 Color: 2
Size: 228311 Color: 0

Bin 882: 6612 of cap free
Amount of items: 3
Items: 
Size: 607710 Color: 14
Size: 211233 Color: 11
Size: 174446 Color: 0

Bin 883: 6672 of cap free
Amount of items: 2
Items: 
Size: 497145 Color: 11
Size: 496184 Color: 4

Bin 884: 7173 of cap free
Amount of items: 2
Items: 
Size: 636950 Color: 6
Size: 355878 Color: 16

Bin 885: 7736 of cap free
Amount of items: 2
Items: 
Size: 764954 Color: 4
Size: 227311 Color: 12

Bin 886: 8461 of cap free
Amount of items: 2
Items: 
Size: 794992 Color: 13
Size: 196548 Color: 15

Bin 887: 8630 of cap free
Amount of items: 2
Items: 
Size: 598548 Color: 16
Size: 392823 Color: 17

Bin 888: 8665 of cap free
Amount of items: 3
Items: 
Size: 732563 Color: 18
Size: 132451 Color: 1
Size: 126322 Color: 9

Bin 889: 8943 of cap free
Amount of items: 2
Items: 
Size: 510928 Color: 18
Size: 480130 Color: 8

Bin 890: 8997 of cap free
Amount of items: 3
Items: 
Size: 609750 Color: 16
Size: 192763 Color: 17
Size: 188491 Color: 7

Bin 891: 10031 of cap free
Amount of items: 2
Items: 
Size: 722494 Color: 10
Size: 267476 Color: 19

Bin 892: 10091 of cap free
Amount of items: 2
Items: 
Size: 531680 Color: 19
Size: 458230 Color: 15

Bin 893: 10155 of cap free
Amount of items: 2
Items: 
Size: 795199 Color: 1
Size: 194647 Color: 15

Bin 894: 10550 of cap free
Amount of items: 2
Items: 
Size: 495855 Color: 11
Size: 493596 Color: 1

Bin 895: 14890 of cap free
Amount of items: 2
Items: 
Size: 509104 Color: 0
Size: 476007 Color: 9

Bin 896: 16013 of cap free
Amount of items: 2
Items: 
Size: 509675 Color: 0
Size: 474313 Color: 12

Bin 897: 22031 of cap free
Amount of items: 3
Items: 
Size: 507055 Color: 7
Size: 313189 Color: 12
Size: 157726 Color: 9

Bin 898: 24931 of cap free
Amount of items: 3
Items: 
Size: 550318 Color: 5
Size: 264813 Color: 12
Size: 159939 Color: 13

Bin 899: 28398 of cap free
Amount of items: 3
Items: 
Size: 577183 Color: 17
Size: 211167 Color: 3
Size: 183253 Color: 7

Bin 900: 32740 of cap free
Amount of items: 3
Items: 
Size: 391263 Color: 7
Size: 301699 Color: 1
Size: 274299 Color: 10

Bin 901: 72940 of cap free
Amount of items: 2
Items: 
Size: 717158 Color: 3
Size: 209903 Color: 16

Total size: 900107146
Total free space: 893755

