Capicity Bin: 2464
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 840 Color: 18
Size: 504 Color: 15
Size: 384 Color: 5
Size: 272 Color: 10
Size: 272 Color: 0
Size: 120 Color: 0
Size: 72 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 17
Size: 684 Color: 18
Size: 136 Color: 6

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1870 Color: 11
Size: 498 Color: 18
Size: 88 Color: 14
Size: 8 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 3
Size: 406 Color: 9
Size: 80 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 17
Size: 382 Color: 18
Size: 76 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 16
Size: 338 Color: 10
Size: 64 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 7
Size: 258 Color: 6
Size: 48 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2161 Color: 14
Size: 253 Color: 19
Size: 50 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 4
Size: 244 Color: 11
Size: 48 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 9
Size: 698 Color: 4
Size: 136 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 12
Size: 782 Color: 16
Size: 152 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 6
Size: 234 Color: 3
Size: 44 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 12
Size: 881 Color: 4
Size: 174 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 5
Size: 626 Color: 5
Size: 124 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 19
Size: 604 Color: 5
Size: 120 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 1
Size: 482 Color: 16
Size: 40 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 17
Size: 882 Color: 10
Size: 172 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 3
Size: 236 Color: 4
Size: 40 Color: 6

Bin 19: 0 of cap free
Amount of items: 4
Items: 
Size: 2127 Color: 9
Size: 281 Color: 16
Size: 48 Color: 10
Size: 8 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 3
Size: 308 Color: 7
Size: 48 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 9
Size: 214 Color: 12
Size: 40 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 9
Size: 1022 Color: 11
Size: 200 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 16
Size: 892 Color: 4
Size: 176 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 16
Size: 444 Color: 16
Size: 80 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 14
Size: 518 Color: 13
Size: 100 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 9
Size: 378 Color: 15
Size: 72 Color: 18

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 0
Size: 1026 Color: 1
Size: 204 Color: 13

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 1722 Color: 12
Size: 622 Color: 2
Size: 64 Color: 11
Size: 56 Color: 7

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 17
Size: 364 Color: 14
Size: 72 Color: 8

Bin 30: 0 of cap free
Amount of items: 4
Items: 
Size: 1696 Color: 18
Size: 576 Color: 4
Size: 96 Color: 15
Size: 96 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 11
Size: 490 Color: 10
Size: 40 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 18
Size: 780 Color: 5
Size: 152 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 8
Size: 1079 Color: 4
Size: 2 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 3
Size: 978 Color: 16
Size: 80 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 3
Size: 332 Color: 4
Size: 64 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 6
Size: 550 Color: 17
Size: 108 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1915 Color: 1
Size: 459 Color: 12
Size: 90 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2059 Color: 9
Size: 339 Color: 0
Size: 66 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 15
Size: 758 Color: 0
Size: 148 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 3
Size: 390 Color: 7
Size: 76 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 3
Size: 902 Color: 10
Size: 112 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 17
Size: 1023 Color: 12
Size: 204 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 8
Size: 244 Color: 11
Size: 40 Color: 9

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 11
Size: 338 Color: 9
Size: 28 Color: 16

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 9
Size: 540 Color: 12
Size: 104 Color: 16

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 2130 Color: 4
Size: 282 Color: 5
Size: 52 Color: 10

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 19
Size: 454 Color: 7
Size: 88 Color: 5

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 13
Size: 763 Color: 6
Size: 152 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1990 Color: 2
Size: 398 Color: 5
Size: 76 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 3
Size: 1028 Color: 16
Size: 200 Color: 8

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 14
Size: 252 Color: 10
Size: 48 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 9
Size: 583 Color: 17
Size: 116 Color: 17

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 3
Size: 1049 Color: 8
Size: 182 Color: 13

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 12
Size: 874 Color: 10
Size: 172 Color: 9

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 7
Size: 920 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 2099 Color: 1
Size: 305 Color: 3
Size: 60 Color: 5

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 11
Size: 370 Color: 17
Size: 40 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 0
Size: 782 Color: 9
Size: 84 Color: 10

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 9
Size: 314 Color: 11
Size: 60 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1847 Color: 19
Size: 515 Color: 1
Size: 102 Color: 12

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 4
Size: 404 Color: 18
Size: 72 Color: 6

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 0
Size: 484 Color: 8
Size: 96 Color: 15

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2019 Color: 0
Size: 371 Color: 9
Size: 74 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 6
Size: 665 Color: 7
Size: 132 Color: 8

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1971 Color: 2
Size: 411 Color: 17
Size: 82 Color: 1

Total size: 160160
Total free space: 0

