Capicity Bin: 2064
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 2
Size: 186 Color: 2
Size: 52 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 832 Color: 1
Size: 618 Color: 4
Size: 476 Color: 0
Size: 74 Color: 3
Size: 64 Color: 3

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1326 Color: 4
Size: 378 Color: 4
Size: 324 Color: 2
Size: 36 Color: 3

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1537 Color: 3
Size: 375 Color: 4
Size: 84 Color: 0
Size: 68 Color: 2

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1546 Color: 2
Size: 498 Color: 4
Size: 16 Color: 0
Size: 4 Color: 1

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 1846 Color: 3
Size: 86 Color: 4
Size: 76 Color: 3
Size: 56 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 4
Size: 310 Color: 4
Size: 36 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 3
Size: 314 Color: 0
Size: 60 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 3
Size: 802 Color: 2
Size: 228 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 2
Size: 303 Color: 3
Size: 44 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 4
Size: 237 Color: 2
Size: 46 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 0
Size: 457 Color: 1
Size: 90 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 1
Size: 737 Color: 4
Size: 146 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 3
Size: 274 Color: 2
Size: 156 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1041 Color: 2
Size: 853 Color: 3
Size: 170 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1341 Color: 0
Size: 603 Color: 2
Size: 120 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 3
Size: 223 Color: 1
Size: 44 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 4
Size: 244 Color: 0
Size: 44 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 3
Size: 851 Color: 0
Size: 168 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 4
Size: 341 Color: 2
Size: 68 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 1
Size: 431 Color: 0
Size: 84 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 4
Size: 182 Color: 4
Size: 40 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 4
Size: 625 Color: 4
Size: 32 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1839 Color: 0
Size: 219 Color: 1
Size: 6 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1217 Color: 0
Size: 717 Color: 4
Size: 130 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 2
Size: 362 Color: 2
Size: 232 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 3
Size: 394 Color: 4
Size: 76 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 0
Size: 857 Color: 0
Size: 170 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 3
Size: 257 Color: 3
Size: 34 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 3
Size: 271 Color: 0
Size: 52 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 0
Size: 750 Color: 3
Size: 148 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 1
Size: 414 Color: 4
Size: 36 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 2
Size: 241 Color: 3
Size: 46 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 0
Size: 462 Color: 3
Size: 92 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1703 Color: 4
Size: 301 Color: 4
Size: 60 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 0
Size: 206 Color: 3
Size: 120 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1427 Color: 3
Size: 531 Color: 4
Size: 106 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 1
Size: 202 Color: 0
Size: 44 Color: 0

Bin 39: 0 of cap free
Amount of items: 4
Items: 
Size: 1424 Color: 3
Size: 428 Color: 0
Size: 168 Color: 0
Size: 44 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 0
Size: 261 Color: 0
Size: 52 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 4
Size: 331 Color: 1
Size: 64 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1611 Color: 0
Size: 379 Color: 3
Size: 74 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 3
Size: 226 Color: 0
Size: 44 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 4
Size: 290 Color: 0
Size: 80 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 1
Size: 211 Color: 3
Size: 42 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1615 Color: 4
Size: 441 Color: 0
Size: 8 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1315 Color: 0
Size: 625 Color: 1
Size: 124 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 1
Size: 678 Color: 4
Size: 132 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 2
Size: 606 Color: 3
Size: 108 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 4
Size: 534 Color: 2
Size: 104 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 4
Size: 389 Color: 2
Size: 48 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 1
Size: 244 Color: 0
Size: 40 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 2
Size: 202 Color: 2
Size: 40 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 434 Color: 0
Size: 60 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 3
Size: 691 Color: 2
Size: 136 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1296 Color: 2
Size: 696 Color: 1
Size: 72 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1193 Color: 1
Size: 793 Color: 2
Size: 78 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1106 Color: 2
Size: 862 Color: 1
Size: 96 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 0
Size: 293 Color: 0
Size: 58 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 1
Size: 258 Color: 1
Size: 48 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 4
Size: 461 Color: 0
Size: 90 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1419 Color: 2
Size: 539 Color: 0
Size: 106 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 4
Size: 505 Color: 1
Size: 100 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1033 Color: 2
Size: 861 Color: 2
Size: 170 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1831 Color: 1
Size: 195 Color: 2
Size: 38 Color: 1

Total size: 134160
Total free space: 0

