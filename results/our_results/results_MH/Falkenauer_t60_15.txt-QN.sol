Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 48
Size: 291 Color: 26
Size: 267 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 49
Size: 300 Color: 33
Size: 252 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 45
Size: 296 Color: 30
Size: 291 Color: 27

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 43
Size: 329 Color: 37
Size: 279 Color: 22

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 41
Size: 336 Color: 38
Size: 281 Color: 23

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 46
Size: 323 Color: 35
Size: 251 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 47
Size: 296 Color: 29
Size: 273 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 299 Color: 32
Size: 292 Color: 28
Size: 409 Color: 44

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 40
Size: 328 Color: 36
Size: 312 Color: 34

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 42
Size: 347 Color: 39
Size: 264 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 50
Size: 299 Color: 31
Size: 250 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 51
Size: 288 Color: 24
Size: 261 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 52
Size: 288 Color: 25
Size: 258 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 53
Size: 271 Color: 18
Size: 266 Color: 16

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 54
Size: 274 Color: 20
Size: 255 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 55
Size: 274 Color: 21
Size: 254 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 56
Size: 263 Color: 14
Size: 256 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 57
Size: 262 Color: 13
Size: 253 Color: 5

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 58
Size: 261 Color: 11
Size: 252 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 59
Size: 257 Color: 9
Size: 252 Color: 3

Total size: 20000
Total free space: 0

