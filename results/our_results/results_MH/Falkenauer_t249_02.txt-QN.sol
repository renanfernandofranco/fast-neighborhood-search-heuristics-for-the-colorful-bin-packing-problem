Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 201
Size: 343 Color: 146
Size: 256 Color: 27

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 237
Size: 281 Color: 85
Size: 250 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 229
Size: 281 Color: 83
Size: 262 Color: 43

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 247
Size: 254 Color: 16
Size: 252 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 195
Size: 345 Color: 147
Size: 263 Color: 46

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 165
Size: 365 Color: 164
Size: 269 Color: 58

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 241
Size: 264 Color: 51
Size: 252 Color: 8

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 203
Size: 324 Color: 133
Size: 271 Color: 65

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 231
Size: 286 Color: 92
Size: 255 Color: 20

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 246
Size: 254 Color: 15
Size: 252 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 215
Size: 300 Color: 110
Size: 269 Color: 59

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 177
Size: 363 Color: 163
Size: 263 Color: 45

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 214
Size: 301 Color: 112
Size: 269 Color: 57

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 171
Size: 369 Color: 170
Size: 259 Color: 34

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 208
Size: 322 Color: 129
Size: 264 Color: 48

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 185
Size: 362 Color: 161
Size: 255 Color: 22

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 213
Size: 319 Color: 127
Size: 257 Color: 28

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 243
Size: 257 Color: 31
Size: 256 Color: 25

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 176
Size: 339 Color: 143
Size: 287 Color: 97

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 162
Size: 360 Color: 160
Size: 278 Color: 76

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 155
Size: 349 Color: 151
Size: 298 Color: 108

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 191
Size: 357 Color: 158
Size: 253 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 186
Size: 335 Color: 141
Size: 281 Color: 82

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 240
Size: 261 Color: 42
Size: 258 Color: 33

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 205
Size: 333 Color: 140
Size: 260 Color: 38

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 204
Size: 323 Color: 130
Size: 270 Color: 60

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 184
Size: 311 Color: 121
Size: 307 Color: 116

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 226
Size: 290 Color: 101
Size: 260 Color: 40

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 236
Size: 276 Color: 74
Size: 257 Color: 29

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 239
Size: 273 Color: 70
Size: 250 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 197
Size: 333 Color: 139
Size: 271 Color: 66

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 206
Size: 340 Color: 145
Size: 250 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 169
Size: 368 Color: 168
Size: 264 Color: 49

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 209
Size: 332 Color: 138
Size: 254 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 188
Size: 357 Color: 159
Size: 256 Color: 24

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 200
Size: 326 Color: 134
Size: 273 Color: 71

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 182
Size: 351 Color: 154
Size: 272 Color: 68

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 228
Size: 292 Color: 104
Size: 252 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 210
Size: 317 Color: 125
Size: 264 Color: 50

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 196
Size: 335 Color: 142
Size: 271 Color: 67

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 181
Size: 373 Color: 172
Size: 250 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 189
Size: 311 Color: 120
Size: 299 Color: 109

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 234
Size: 274 Color: 73
Size: 263 Color: 47

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 244
Size: 259 Color: 37
Size: 253 Color: 12

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 221
Size: 302 Color: 113
Size: 255 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 217
Size: 311 Color: 119
Size: 256 Color: 26

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 225
Size: 281 Color: 81
Size: 270 Color: 61

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 230
Size: 283 Color: 88
Size: 259 Color: 36

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 212
Size: 329 Color: 137
Size: 250 Color: 5

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 224
Size: 280 Color: 80
Size: 272 Color: 69

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 223
Size: 290 Color: 103
Size: 265 Color: 53

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 192
Size: 324 Color: 132
Size: 285 Color: 89

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 218
Size: 286 Color: 94
Size: 280 Color: 79

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 180
Size: 315 Color: 124
Size: 310 Color: 118

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 174
Size: 324 Color: 131
Size: 303 Color: 114

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 211
Size: 294 Color: 105
Size: 285 Color: 90

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 248
Size: 254 Color: 17
Size: 250 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 235
Size: 271 Color: 64
Size: 263 Color: 44

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 198
Size: 318 Color: 126
Size: 285 Color: 91

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 202
Size: 346 Color: 150
Size: 251 Color: 6

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 242
Size: 260 Color: 41
Size: 256 Color: 23

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 179
Size: 373 Color: 173
Size: 253 Color: 11

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 190
Size: 314 Color: 123
Size: 296 Color: 106

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 157
Size: 356 Color: 156
Size: 288 Color: 99

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 245
Size: 257 Color: 30
Size: 253 Color: 13

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 232
Size: 281 Color: 86
Size: 260 Color: 39

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 199
Size: 329 Color: 135
Size: 274 Color: 72

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 194
Size: 329 Color: 136
Size: 279 Color: 77

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 219
Size: 304 Color: 115
Size: 255 Color: 21

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 187
Size: 346 Color: 149
Size: 267 Color: 55

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 193
Size: 319 Color: 128
Size: 290 Color: 100

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 183
Size: 311 Color: 122
Size: 308 Color: 117

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 207
Size: 297 Color: 107
Size: 290 Color: 102

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 178
Size: 339 Color: 144
Size: 287 Color: 96

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 167
Size: 367 Color: 166
Size: 266 Color: 54

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 153
Size: 350 Color: 152
Size: 300 Color: 111

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 216
Size: 287 Color: 95
Size: 280 Color: 78

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 220
Size: 288 Color: 98
Size: 270 Color: 62

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 222
Size: 286 Color: 93
Size: 271 Color: 63

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 227
Size: 278 Color: 75
Size: 269 Color: 56

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 175
Size: 345 Color: 148
Size: 282 Color: 87

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 233
Size: 281 Color: 84
Size: 258 Color: 32

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 238
Size: 265 Color: 52
Size: 259 Color: 35

Total size: 83000
Total free space: 0

