Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 250 Color: 0
Size: 251 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 277 Color: 1
Size: 265 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 347 Color: 1
Size: 289 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 260 Color: 1
Size: 251 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 354 Color: 1
Size: 265 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 321 Color: 1
Size: 255 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 336 Color: 1
Size: 295 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 256 Color: 1
Size: 251 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 1
Size: 272 Color: 0
Size: 393 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 323 Color: 1
Size: 290 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 356 Color: 1
Size: 282 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 359 Color: 1
Size: 270 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 286 Color: 1
Size: 270 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 292 Color: 1
Size: 270 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 302 Color: 1
Size: 256 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 298 Color: 1
Size: 259 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 321 Color: 0
Size: 311 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 317 Color: 1
Size: 305 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 340 Color: 1
Size: 275 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 309 Color: 1
Size: 264 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 327 Color: 1
Size: 278 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 348 Color: 1
Size: 295 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 272 Color: 1
Size: 251 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 324 Color: 1
Size: 279 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 299 Color: 1
Size: 281 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 286 Color: 1
Size: 263 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 289 Color: 0
Size: 280 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 324 Color: 1
Size: 258 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 258 Color: 1
Size: 255 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 309 Color: 1
Size: 250 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 339 Color: 1
Size: 265 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 259 Color: 0
Size: 257 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 276 Color: 1
Size: 270 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 257 Color: 0
Size: 254 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 258 Color: 1
Size: 253 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 282 Color: 1
Size: 255 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 269 Color: 1
Size: 257 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 294 Color: 0
Size: 282 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 276 Color: 1
Size: 252 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 334 Color: 1
Size: 281 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 363 Color: 1
Size: 270 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 266 Color: 1
Size: 252 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 298 Color: 1
Size: 254 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 362 Color: 1
Size: 275 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 329 Color: 1
Size: 278 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 272 Color: 1
Size: 270 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 295 Color: 1
Size: 263 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 304 Color: 1
Size: 282 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 292 Color: 0
Size: 264 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 303 Color: 1
Size: 278 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 270 Color: 0
Size: 269 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 277 Color: 1
Size: 250 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 312 Color: 1
Size: 283 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 318 Color: 0
Size: 282 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 264 Color: 1
Size: 254 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 305 Color: 1
Size: 260 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 254 Color: 1
Size: 251 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 347 Color: 1
Size: 273 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 366 Color: 1
Size: 261 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 256 Color: 0
Size: 253 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 325 Color: 1
Size: 283 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 341 Color: 1
Size: 285 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 308 Color: 1
Size: 285 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 257 Color: 1
Size: 255 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 296 Color: 1
Size: 253 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 303 Color: 1
Size: 272 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 288 Color: 1
Size: 253 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 361 Color: 1
Size: 272 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 303 Color: 1
Size: 285 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 313 Color: 1
Size: 289 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 284 Color: 1
Size: 250 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 339 Color: 1
Size: 270 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 337 Color: 1
Size: 267 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 355 Color: 1
Size: 272 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 313 Color: 1
Size: 273 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 263 Color: 1
Size: 251 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 264 Color: 0
Size: 260 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 257 Color: 1
Size: 251 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 311 Color: 1
Size: 259 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 333 Color: 1
Size: 276 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 256 Color: 1
Size: 250 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 330 Color: 1
Size: 265 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 304 Color: 1
Size: 276 Color: 0

Total size: 83000
Total free space: 0

