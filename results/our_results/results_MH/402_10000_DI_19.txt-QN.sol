Capicity Bin: 7896
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4518 Color: 287
Size: 3188 Color: 265
Size: 190 Color: 36

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 323
Size: 1124 Color: 183
Size: 898 Color: 160

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6044 Color: 328
Size: 1748 Color: 224
Size: 104 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 331
Size: 1548 Color: 214
Size: 264 Color: 66

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 336
Size: 1191 Color: 190
Size: 492 Color: 113

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 339
Size: 1468 Color: 207
Size: 152 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6302 Color: 340
Size: 852 Color: 152
Size: 742 Color: 142

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6312 Color: 341
Size: 1012 Color: 171
Size: 572 Color: 125

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6386 Color: 346
Size: 1186 Color: 189
Size: 324 Color: 81

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 349
Size: 1251 Color: 193
Size: 180 Color: 33

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6467 Color: 350
Size: 1191 Color: 191
Size: 238 Color: 58

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6483 Color: 352
Size: 1179 Color: 187
Size: 234 Color: 56

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 356
Size: 1122 Color: 182
Size: 224 Color: 52

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6608 Color: 361
Size: 1080 Color: 179
Size: 208 Color: 47

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6609 Color: 362
Size: 1133 Color: 184
Size: 154 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6649 Color: 365
Size: 1011 Color: 170
Size: 236 Color: 57

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6674 Color: 368
Size: 774 Color: 145
Size: 448 Color: 109

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 369
Size: 860 Color: 153
Size: 352 Color: 89

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6685 Color: 370
Size: 907 Color: 161
Size: 304 Color: 77

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 372
Size: 692 Color: 140
Size: 480 Color: 111

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6745 Color: 374
Size: 947 Color: 163
Size: 204 Color: 43

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6778 Color: 376
Size: 976 Color: 167
Size: 142 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6827 Color: 379
Size: 891 Color: 158
Size: 178 Color: 32

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6828 Color: 380
Size: 980 Color: 168
Size: 88 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6839 Color: 381
Size: 873 Color: 157
Size: 184 Color: 34

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6865 Color: 384
Size: 861 Color: 154
Size: 170 Color: 28

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6889 Color: 387
Size: 841 Color: 151
Size: 166 Color: 24

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 388
Size: 608 Color: 129
Size: 386 Color: 95

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6929 Color: 390
Size: 807 Color: 148
Size: 160 Color: 20

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6939 Color: 392
Size: 799 Color: 146
Size: 158 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 394
Size: 494 Color: 114
Size: 432 Color: 103

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 395
Size: 722 Color: 141
Size: 194 Color: 38

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7006 Color: 396
Size: 666 Color: 137
Size: 224 Color: 53

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7058 Color: 398
Size: 564 Color: 122
Size: 274 Color: 68

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 400
Size: 570 Color: 124
Size: 248 Color: 61

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 401
Size: 496 Color: 115
Size: 310 Color: 79

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 402
Size: 624 Color: 130
Size: 174 Color: 29

Bin 38: 1 of cap free
Amount of items: 5
Items: 
Size: 4076 Color: 278
Size: 1484 Color: 208
Size: 1365 Color: 202
Size: 764 Color: 144
Size: 206 Color: 44

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 318
Size: 1707 Color: 220
Size: 384 Color: 94

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 6033 Color: 327
Size: 1862 Color: 227

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6065 Color: 330
Size: 1516 Color: 211
Size: 314 Color: 80

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6322 Color: 343
Size: 1573 Color: 216

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6397 Color: 348
Size: 1498 Color: 210

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 363
Size: 1283 Color: 196

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 366
Size: 830 Color: 150
Size: 408 Color: 98

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6761 Color: 375
Size: 1134 Color: 185

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6822 Color: 378
Size: 1073 Color: 177

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6934 Color: 391
Size: 961 Color: 165

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 399
Size: 827 Color: 149

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 6049 Color: 329
Size: 1733 Color: 223
Size: 112 Color: 6

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6178 Color: 334
Size: 1716 Color: 221

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6538 Color: 355
Size: 1356 Color: 201

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6714 Color: 371
Size: 1180 Color: 188

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6729 Color: 373
Size: 1165 Color: 186

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6868 Color: 386
Size: 1026 Color: 173

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 5428 Color: 308
Size: 2465 Color: 247

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 6259 Color: 338
Size: 1634 Color: 218

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 383
Size: 1033 Color: 174

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 5330 Color: 304
Size: 2426 Color: 245
Size: 136 Color: 11

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 6351 Color: 344
Size: 1541 Color: 213

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 397
Size: 862 Color: 155

Bin 62: 5 of cap free
Amount of items: 7
Items: 
Size: 3951 Color: 275
Size: 934 Color: 162
Size: 892 Color: 159
Size: 868 Color: 156
Size: 802 Color: 147
Size: 232 Color: 55
Size: 212 Color: 48

Bin 63: 5 of cap free
Amount of items: 3
Items: 
Size: 5356 Color: 305
Size: 1434 Color: 206
Size: 1101 Color: 181

Bin 64: 5 of cap free
Amount of items: 2
Items: 
Size: 5793 Color: 317
Size: 2098 Color: 235

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 6602 Color: 360
Size: 1289 Color: 197

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 6809 Color: 377
Size: 1082 Color: 180

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 6905 Color: 389
Size: 986 Color: 169

Bin 68: 6 of cap free
Amount of items: 4
Items: 
Size: 4986 Color: 297
Size: 2580 Color: 252
Size: 164 Color: 22
Size: 160 Color: 21

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 6666 Color: 367
Size: 1224 Color: 192

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 6849 Color: 382
Size: 1041 Color: 175

Bin 71: 7 of cap free
Amount of items: 9
Items: 
Size: 3949 Color: 273
Size: 656 Color: 134
Size: 656 Color: 133
Size: 656 Color: 132
Size: 632 Color: 131
Size: 592 Color: 128
Size: 296 Color: 74
Size: 232 Color: 54
Size: 220 Color: 51

Bin 72: 7 of cap free
Amount of items: 3
Items: 
Size: 5594 Color: 313
Size: 2167 Color: 239
Size: 128 Color: 7

Bin 73: 7 of cap free
Amount of items: 2
Items: 
Size: 5966 Color: 325
Size: 1923 Color: 230

Bin 74: 7 of cap free
Amount of items: 2
Items: 
Size: 6575 Color: 359
Size: 1314 Color: 198

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 6940 Color: 393
Size: 949 Color: 164

Bin 76: 8 of cap free
Amount of items: 3
Items: 
Size: 5265 Color: 301
Size: 2475 Color: 248
Size: 148 Color: 15

Bin 77: 8 of cap free
Amount of items: 3
Items: 
Size: 5666 Color: 315
Size: 2142 Color: 238
Size: 80 Color: 2

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 5764 Color: 316
Size: 2124 Color: 237

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 6202 Color: 335
Size: 1686 Color: 219

Bin 80: 8 of cap free
Amount of items: 2
Items: 
Size: 6474 Color: 351
Size: 1414 Color: 205

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 385
Size: 1022 Color: 172

Bin 82: 9 of cap free
Amount of items: 3
Items: 
Size: 4872 Color: 292
Size: 2847 Color: 257
Size: 168 Color: 26

Bin 83: 9 of cap free
Amount of items: 2
Items: 
Size: 5364 Color: 306
Size: 2523 Color: 250

Bin 84: 9 of cap free
Amount of items: 2
Items: 
Size: 6484 Color: 353
Size: 1403 Color: 204

Bin 85: 10 of cap free
Amount of items: 4
Items: 
Size: 4510 Color: 286
Size: 2988 Color: 263
Size: 196 Color: 39
Size: 192 Color: 37

Bin 86: 10 of cap free
Amount of items: 3
Items: 
Size: 5010 Color: 298
Size: 2724 Color: 254
Size: 152 Color: 16

Bin 87: 10 of cap free
Amount of items: 3
Items: 
Size: 5549 Color: 310
Size: 2205 Color: 242
Size: 132 Color: 9

Bin 88: 10 of cap free
Amount of items: 3
Items: 
Size: 5573 Color: 311
Size: 2181 Color: 240
Size: 132 Color: 8

Bin 89: 10 of cap free
Amount of items: 2
Items: 
Size: 6556 Color: 358
Size: 1330 Color: 200

Bin 90: 11 of cap free
Amount of items: 3
Items: 
Size: 5297 Color: 303
Size: 2444 Color: 246
Size: 144 Color: 13

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 6396 Color: 347
Size: 1488 Color: 209

Bin 92: 13 of cap free
Amount of items: 2
Items: 
Size: 6356 Color: 345
Size: 1527 Color: 212

Bin 93: 13 of cap free
Amount of items: 2
Items: 
Size: 6621 Color: 364
Size: 1262 Color: 195

Bin 94: 14 of cap free
Amount of items: 3
Items: 
Size: 5658 Color: 314
Size: 2116 Color: 236
Size: 108 Color: 5

Bin 95: 14 of cap free
Amount of items: 2
Items: 
Size: 6102 Color: 332
Size: 1780 Color: 226

Bin 96: 16 of cap free
Amount of items: 4
Items: 
Size: 4972 Color: 296
Size: 2576 Color: 251
Size: 168 Color: 25
Size: 164 Color: 23

Bin 97: 17 of cap free
Amount of items: 2
Items: 
Size: 5251 Color: 300
Size: 2628 Color: 253

Bin 98: 17 of cap free
Amount of items: 2
Items: 
Size: 5942 Color: 324
Size: 1937 Color: 231

Bin 99: 19 of cap free
Amount of items: 2
Items: 
Size: 5817 Color: 319
Size: 2060 Color: 234

Bin 100: 19 of cap free
Amount of items: 2
Items: 
Size: 5849 Color: 322
Size: 2028 Color: 233

Bin 101: 19 of cap free
Amount of items: 2
Items: 
Size: 6124 Color: 333
Size: 1753 Color: 225

Bin 102: 19 of cap free
Amount of items: 2
Items: 
Size: 6247 Color: 337
Size: 1630 Color: 217

Bin 103: 21 of cap free
Amount of items: 2
Items: 
Size: 4939 Color: 295
Size: 2936 Color: 262

Bin 104: 21 of cap free
Amount of items: 2
Items: 
Size: 6009 Color: 326
Size: 1866 Color: 228

Bin 105: 21 of cap free
Amount of items: 2
Items: 
Size: 6551 Color: 357
Size: 1324 Color: 199

Bin 106: 22 of cap free
Amount of items: 3
Items: 
Size: 5180 Color: 299
Size: 1721 Color: 222
Size: 973 Color: 166

Bin 107: 22 of cap free
Amount of items: 2
Items: 
Size: 6499 Color: 354
Size: 1375 Color: 203

Bin 108: 27 of cap free
Amount of items: 2
Items: 
Size: 6316 Color: 342
Size: 1553 Color: 215

Bin 109: 28 of cap free
Amount of items: 3
Items: 
Size: 4636 Color: 288
Size: 3044 Color: 264
Size: 188 Color: 35

Bin 110: 32 of cap free
Amount of items: 2
Items: 
Size: 5382 Color: 307
Size: 2482 Color: 249

Bin 111: 36 of cap free
Amount of items: 2
Items: 
Size: 3956 Color: 277
Size: 3904 Color: 272

Bin 112: 37 of cap free
Amount of items: 3
Items: 
Size: 4869 Color: 291
Size: 2822 Color: 256
Size: 168 Color: 27

Bin 113: 39 of cap free
Amount of items: 2
Items: 
Size: 5589 Color: 312
Size: 2268 Color: 243

Bin 114: 43 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 290
Size: 2873 Color: 259
Size: 176 Color: 30

Bin 115: 55 of cap free
Amount of items: 3
Items: 
Size: 5844 Color: 321
Size: 1957 Color: 232
Size: 40 Color: 0

Bin 116: 61 of cap free
Amount of items: 3
Items: 
Size: 5281 Color: 302
Size: 2406 Color: 244
Size: 148 Color: 14

Bin 117: 69 of cap free
Amount of items: 3
Items: 
Size: 5833 Color: 320
Size: 1922 Color: 229
Size: 72 Color: 1

Bin 118: 76 of cap free
Amount of items: 2
Items: 
Size: 4927 Color: 294
Size: 2893 Color: 261

Bin 119: 96 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 280
Size: 3284 Color: 266
Size: 200 Color: 42

Bin 120: 98 of cap free
Amount of items: 8
Items: 
Size: 3950 Color: 274
Size: 750 Color: 143
Size: 682 Color: 139
Size: 674 Color: 138
Size: 656 Color: 136
Size: 656 Color: 135
Size: 216 Color: 50
Size: 214 Color: 49

Bin 121: 99 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 309
Size: 2193 Color: 241
Size: 136 Color: 10

Bin 122: 100 of cap free
Amount of items: 2
Items: 
Size: 4918 Color: 293
Size: 2878 Color: 260

Bin 123: 111 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 289
Size: 2861 Color: 258
Size: 176 Color: 31

Bin 124: 128 of cap free
Amount of items: 6
Items: 
Size: 3954 Color: 276
Size: 1252 Color: 194
Size: 1076 Color: 178
Size: 1072 Color: 176
Size: 208 Color: 46
Size: 206 Color: 45

Bin 125: 140 of cap free
Amount of items: 2
Items: 
Size: 4465 Color: 284
Size: 3291 Color: 270

Bin 126: 157 of cap free
Amount of items: 2
Items: 
Size: 4449 Color: 283
Size: 3290 Color: 269

Bin 127: 161 of cap free
Amount of items: 2
Items: 
Size: 4446 Color: 282
Size: 3289 Color: 268

Bin 128: 170 of cap free
Amount of items: 22
Items: 
Size: 440 Color: 106
Size: 438 Color: 105
Size: 434 Color: 104
Size: 424 Color: 102
Size: 416 Color: 101
Size: 416 Color: 100
Size: 416 Color: 99
Size: 400 Color: 97
Size: 390 Color: 96
Size: 380 Color: 93
Size: 376 Color: 92
Size: 372 Color: 91
Size: 304 Color: 76
Size: 296 Color: 75
Size: 296 Color: 73
Size: 288 Color: 72
Size: 284 Color: 71
Size: 280 Color: 70
Size: 280 Color: 69
Size: 272 Color: 67
Size: 264 Color: 65
Size: 260 Color: 64

Bin 129: 180 of cap free
Amount of items: 17
Items: 
Size: 578 Color: 127
Size: 574 Color: 126
Size: 568 Color: 123
Size: 560 Color: 121
Size: 536 Color: 120
Size: 520 Color: 119
Size: 512 Color: 118
Size: 504 Color: 117
Size: 504 Color: 116
Size: 484 Color: 112
Size: 480 Color: 110
Size: 448 Color: 108
Size: 440 Color: 107
Size: 256 Color: 63
Size: 256 Color: 62
Size: 248 Color: 60
Size: 248 Color: 59

Bin 130: 180 of cap free
Amount of items: 2
Items: 
Size: 4244 Color: 279
Size: 3472 Color: 271

Bin 131: 185 of cap free
Amount of items: 2
Items: 
Size: 4425 Color: 281
Size: 3286 Color: 267

Bin 132: 197 of cap free
Amount of items: 4
Items: 
Size: 4481 Color: 285
Size: 2818 Color: 255
Size: 200 Color: 41
Size: 200 Color: 40

Bin 133: 4828 of cap free
Amount of items: 9
Items: 
Size: 368 Color: 90
Size: 350 Color: 88
Size: 346 Color: 87
Size: 344 Color: 86
Size: 342 Color: 85
Size: 340 Color: 84
Size: 336 Color: 83
Size: 336 Color: 82
Size: 306 Color: 78

Total size: 1042272
Total free space: 7896

