Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 2
Size: 308 Color: 3
Size: 268 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 256 Color: 3
Size: 253 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 3
Size: 311 Color: 4
Size: 260 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 331 Color: 3
Size: 289 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 336 Color: 4
Size: 268 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 330 Color: 2
Size: 262 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 2
Size: 316 Color: 3
Size: 251 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 4
Size: 277 Color: 0
Size: 275 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 328 Color: 2
Size: 280 Color: 4
Size: 392 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 276 Color: 4
Size: 259 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 4
Size: 340 Color: 3
Size: 292 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 4
Size: 365 Color: 1
Size: 260 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 259 Color: 2
Size: 250 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 265 Color: 2
Size: 252 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 316 Color: 4
Size: 282 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 313 Color: 0
Size: 266 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 2
Size: 363 Color: 4
Size: 265 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 363 Color: 4
Size: 271 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 324 Color: 1
Size: 295 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 2
Size: 303 Color: 1
Size: 283 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 2
Size: 325 Color: 1
Size: 303 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 3
Size: 357 Color: 2
Size: 254 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 289 Color: 1
Size: 279 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 298 Color: 2
Size: 303 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 335 Color: 2
Size: 259 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 2
Size: 257 Color: 1
Size: 250 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 347 Color: 4
Size: 255 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 4
Size: 351 Color: 0
Size: 296 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 328 Color: 2
Size: 251 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 2
Size: 329 Color: 0
Size: 308 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 265 Color: 1
Size: 250 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 0
Size: 353 Color: 2
Size: 254 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 322 Color: 2
Size: 295 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 296 Color: 0
Size: 260 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 2
Size: 253 Color: 0
Size: 251 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 338 Color: 3
Size: 259 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 2
Size: 317 Color: 4
Size: 294 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 311 Color: 4
Size: 262 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 2
Size: 365 Color: 2
Size: 268 Color: 1

Total size: 40000
Total free space: 0

