Capicity Bin: 1000001
Lower Bound: 4479

Bins used: 4480
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 678294 Color: 3
Size: 162427 Color: 12
Size: 159280 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 706910 Color: 7
Size: 154456 Color: 9
Size: 138635 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 553385 Color: 10
Size: 251007 Color: 3
Size: 195609 Color: 1

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 772548 Color: 19
Size: 227453 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 763854 Color: 0
Size: 119216 Color: 0
Size: 116931 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 749269 Color: 2
Size: 125824 Color: 3
Size: 124908 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 784545 Color: 7
Size: 109720 Color: 12
Size: 105736 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 699152 Color: 7
Size: 162350 Color: 3
Size: 138499 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 691570 Color: 10
Size: 156677 Color: 13
Size: 151754 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 776525 Color: 10
Size: 113603 Color: 10
Size: 109873 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 461632 Color: 19
Size: 303691 Color: 16
Size: 234678 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 762622 Color: 3
Size: 126174 Color: 15
Size: 111205 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 759619 Color: 1
Size: 132489 Color: 18
Size: 107893 Color: 15

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 560226 Color: 9
Size: 219801 Color: 11
Size: 112573 Color: 10
Size: 107401 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 770851 Color: 14
Size: 116319 Color: 2
Size: 112831 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 680781 Color: 5
Size: 159723 Color: 14
Size: 159497 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 644535 Color: 12
Size: 208436 Color: 6
Size: 147030 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 736947 Color: 14
Size: 133594 Color: 3
Size: 129460 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 607372 Color: 14
Size: 246731 Color: 2
Size: 145898 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 797414 Color: 18
Size: 102008 Color: 11
Size: 100579 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 713883 Color: 11
Size: 143326 Color: 6
Size: 142792 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 708017 Color: 10
Size: 169827 Color: 14
Size: 122157 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 566938 Color: 2
Size: 264497 Color: 7
Size: 168566 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 706384 Color: 4
Size: 146917 Color: 5
Size: 146700 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 552899 Color: 4
Size: 278224 Color: 8
Size: 168878 Color: 16

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 667226 Color: 11
Size: 332775 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 645765 Color: 13
Size: 208546 Color: 6
Size: 145690 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 771190 Color: 9
Size: 127038 Color: 1
Size: 101773 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 678990 Color: 8
Size: 161985 Color: 0
Size: 159026 Color: 5

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 756960 Color: 5
Size: 142963 Color: 9
Size: 100078 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 696213 Color: 6
Size: 174236 Color: 2
Size: 129552 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 455810 Color: 11
Size: 276525 Color: 19
Size: 267666 Color: 3

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 722597 Color: 15
Size: 277404 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 608601 Color: 9
Size: 197009 Color: 2
Size: 194391 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 559788 Color: 18
Size: 220178 Color: 3
Size: 220035 Color: 12

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 668997 Color: 0
Size: 331004 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 510497 Color: 12
Size: 256105 Color: 11
Size: 233399 Color: 14

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 718849 Color: 6
Size: 148032 Color: 19
Size: 133120 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 545510 Color: 13
Size: 278230 Color: 6
Size: 176261 Color: 7

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 634571 Color: 18
Size: 186526 Color: 16
Size: 178904 Color: 5

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 658202 Color: 9
Size: 175567 Color: 8
Size: 166232 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 780043 Color: 4
Size: 114171 Color: 3
Size: 105787 Color: 9

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 728707 Color: 13
Size: 146542 Color: 7
Size: 124752 Color: 4

Bin 44: 0 of cap free
Amount of items: 4
Items: 
Size: 350533 Color: 4
Size: 342897 Color: 7
Size: 156652 Color: 12
Size: 149919 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 716857 Color: 15
Size: 163902 Color: 16
Size: 119242 Color: 15

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 681947 Color: 1
Size: 159506 Color: 13
Size: 158548 Color: 18

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 733738 Color: 16
Size: 146023 Color: 19
Size: 120240 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 748519 Color: 12
Size: 126524 Color: 12
Size: 124958 Color: 14

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 554569 Color: 11
Size: 242503 Color: 15
Size: 202929 Color: 16

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 792905 Color: 6
Size: 103958 Color: 15
Size: 103138 Color: 10

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 704319 Color: 16
Size: 148443 Color: 18
Size: 147239 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 702440 Color: 13
Size: 157554 Color: 10
Size: 140007 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 554397 Color: 0
Size: 246186 Color: 19
Size: 199418 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 691585 Color: 15
Size: 165292 Color: 19
Size: 143124 Color: 10

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 729405 Color: 3
Size: 139531 Color: 14
Size: 131065 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 724911 Color: 2
Size: 162953 Color: 17
Size: 112137 Color: 14

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 772721 Color: 2
Size: 121862 Color: 16
Size: 105418 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 740055 Color: 6
Size: 146313 Color: 18
Size: 113633 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 776531 Color: 16
Size: 121495 Color: 12
Size: 101975 Color: 11

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 467982 Color: 17
Size: 362585 Color: 14
Size: 169434 Color: 18

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 661718 Color: 9
Size: 215495 Color: 7
Size: 122788 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 699799 Color: 2
Size: 151300 Color: 3
Size: 148902 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 749475 Color: 14
Size: 133603 Color: 11
Size: 116923 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 758875 Color: 10
Size: 131455 Color: 12
Size: 109671 Color: 14

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 689905 Color: 14
Size: 197004 Color: 3
Size: 113092 Color: 18

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 715278 Color: 0
Size: 152607 Color: 15
Size: 132116 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 735769 Color: 8
Size: 133425 Color: 11
Size: 130807 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 615228 Color: 9
Size: 193963 Color: 11
Size: 190810 Color: 10

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 793970 Color: 13
Size: 104588 Color: 11
Size: 101443 Color: 18

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 775859 Color: 19
Size: 120332 Color: 17
Size: 103810 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 767553 Color: 2
Size: 117570 Color: 7
Size: 114878 Color: 13

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 795668 Color: 10
Size: 102446 Color: 6
Size: 101887 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 680182 Color: 18
Size: 160314 Color: 10
Size: 159505 Color: 13

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 703205 Color: 3
Size: 172063 Color: 8
Size: 124733 Color: 6

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 612745 Color: 19
Size: 211067 Color: 18
Size: 176189 Color: 6

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 791289 Color: 18
Size: 107555 Color: 6
Size: 101157 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 555086 Color: 14
Size: 289952 Color: 2
Size: 154963 Color: 17

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 744873 Color: 19
Size: 139378 Color: 7
Size: 115750 Color: 9

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 681146 Color: 7
Size: 159448 Color: 15
Size: 159407 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 759054 Color: 1
Size: 131757 Color: 0
Size: 109190 Color: 14

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 616002 Color: 4
Size: 216328 Color: 13
Size: 167671 Color: 16

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 677894 Color: 19
Size: 163773 Color: 8
Size: 158334 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 608819 Color: 9
Size: 287991 Color: 11
Size: 103191 Color: 16

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 787744 Color: 12
Size: 106267 Color: 19
Size: 105990 Color: 10

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 607418 Color: 19
Size: 225969 Color: 12
Size: 166614 Color: 4

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 714417 Color: 4
Size: 184232 Color: 12
Size: 101352 Color: 7

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 775316 Color: 5
Size: 116059 Color: 6
Size: 108626 Color: 17

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 667901 Color: 10
Size: 205750 Color: 18
Size: 126350 Color: 12

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 744472 Color: 16
Size: 142351 Color: 2
Size: 113178 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 774868 Color: 14
Size: 113990 Color: 14
Size: 111143 Color: 9

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 794379 Color: 12
Size: 103553 Color: 14
Size: 102069 Color: 6

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 745424 Color: 1
Size: 135130 Color: 4
Size: 119447 Color: 11

Bin 93: 0 of cap free
Amount of items: 4
Items: 
Size: 503636 Color: 16
Size: 253028 Color: 4
Size: 125495 Color: 17
Size: 117842 Color: 15

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 782125 Color: 10
Size: 112034 Color: 13
Size: 105842 Color: 13

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 738582 Color: 6
Size: 139762 Color: 17
Size: 121657 Color: 8

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 649744 Color: 11
Size: 199613 Color: 14
Size: 150644 Color: 14

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 625661 Color: 1
Size: 374340 Color: 8

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 724482 Color: 11
Size: 158680 Color: 15
Size: 116839 Color: 10

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 768759 Color: 15
Size: 116549 Color: 7
Size: 114693 Color: 12

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 723112 Color: 18
Size: 276889 Color: 10

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 615524 Color: 9
Size: 258927 Color: 8
Size: 125550 Color: 18

Bin 102: 0 of cap free
Amount of items: 4
Items: 
Size: 555129 Color: 18
Size: 160822 Color: 10
Size: 142113 Color: 5
Size: 141937 Color: 17

Bin 103: 0 of cap free
Amount of items: 4
Items: 
Size: 350655 Color: 11
Size: 298895 Color: 11
Size: 232059 Color: 18
Size: 118392 Color: 6

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 683315 Color: 11
Size: 167023 Color: 18
Size: 149663 Color: 11

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 755597 Color: 16
Size: 124336 Color: 11
Size: 120068 Color: 6

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 787615 Color: 18
Size: 106888 Color: 13
Size: 105498 Color: 7

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 613927 Color: 19
Size: 194718 Color: 1
Size: 191356 Color: 16

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 496516 Color: 14
Size: 264615 Color: 10
Size: 238870 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 785730 Color: 10
Size: 111174 Color: 5
Size: 103097 Color: 16

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 788509 Color: 10
Size: 105959 Color: 1
Size: 105533 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 771824 Color: 3
Size: 117642 Color: 1
Size: 110535 Color: 16

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 758633 Color: 14
Size: 136124 Color: 2
Size: 105244 Color: 7

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 458157 Color: 16
Size: 274903 Color: 10
Size: 266941 Color: 19

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 768097 Color: 1
Size: 118145 Color: 17
Size: 113759 Color: 17

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 755499 Color: 18
Size: 143020 Color: 3
Size: 101482 Color: 7

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 744687 Color: 4
Size: 142761 Color: 2
Size: 112553 Color: 11

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 771152 Color: 17
Size: 116660 Color: 4
Size: 112189 Color: 11

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 780768 Color: 6
Size: 110762 Color: 18
Size: 108471 Color: 8

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 786039 Color: 13
Size: 113784 Color: 0
Size: 100178 Color: 8

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 663995 Color: 5
Size: 179537 Color: 16
Size: 156469 Color: 19

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 726751 Color: 1
Size: 148102 Color: 11
Size: 125148 Color: 1

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 681524 Color: 16
Size: 318477 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 761948 Color: 4
Size: 125904 Color: 17
Size: 112149 Color: 10

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 539233 Color: 5
Size: 239439 Color: 2
Size: 221329 Color: 7

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 564901 Color: 12
Size: 218005 Color: 1
Size: 217095 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 725802 Color: 6
Size: 137358 Color: 16
Size: 136841 Color: 19

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 789174 Color: 2
Size: 105722 Color: 7
Size: 105105 Color: 8

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 680591 Color: 18
Size: 163023 Color: 16
Size: 156387 Color: 4

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 772757 Color: 6
Size: 114312 Color: 11
Size: 112932 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 718297 Color: 12
Size: 156380 Color: 9
Size: 125324 Color: 16

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 721159 Color: 4
Size: 141277 Color: 13
Size: 137565 Color: 5

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 797497 Color: 1
Size: 102285 Color: 7
Size: 100219 Color: 9

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 783282 Color: 17
Size: 115224 Color: 1
Size: 101495 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 730368 Color: 9
Size: 168574 Color: 18
Size: 101059 Color: 4

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 645606 Color: 13
Size: 200478 Color: 14
Size: 153917 Color: 15

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 673161 Color: 16
Size: 200518 Color: 13
Size: 126322 Color: 13

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 719909 Color: 0
Size: 142796 Color: 14
Size: 137296 Color: 18

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 732220 Color: 12
Size: 148945 Color: 4
Size: 118836 Color: 5

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 707013 Color: 5
Size: 173010 Color: 5
Size: 119978 Color: 6

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 752795 Color: 6
Size: 133723 Color: 6
Size: 113483 Color: 19

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 570517 Color: 12
Size: 227870 Color: 9
Size: 201614 Color: 12

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 569673 Color: 13
Size: 234017 Color: 8
Size: 196311 Color: 5

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 566936 Color: 3
Size: 283719 Color: 1
Size: 149346 Color: 18

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 657723 Color: 10
Size: 216157 Color: 9
Size: 126121 Color: 12

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 556818 Color: 11
Size: 302544 Color: 6
Size: 140639 Color: 10

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 570391 Color: 1
Size: 226941 Color: 9
Size: 202669 Color: 12

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 394513 Color: 4
Size: 324282 Color: 13
Size: 281206 Color: 6

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 675300 Color: 10
Size: 199688 Color: 2
Size: 125013 Color: 15

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 395858 Color: 19
Size: 394508 Color: 12
Size: 209635 Color: 19

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 759005 Color: 0
Size: 124022 Color: 18
Size: 116974 Color: 18

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 496451 Color: 14
Size: 309882 Color: 5
Size: 193668 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 545970 Color: 17
Size: 271894 Color: 11
Size: 182137 Color: 9

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 704180 Color: 16
Size: 148409 Color: 15
Size: 147412 Color: 11

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 615993 Color: 15
Size: 235205 Color: 15
Size: 148803 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 578829 Color: 1
Size: 293144 Color: 16
Size: 128028 Color: 19

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 722320 Color: 14
Size: 160267 Color: 9
Size: 117414 Color: 17

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 652264 Color: 11
Size: 192944 Color: 7
Size: 154793 Color: 15

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 552871 Color: 1
Size: 233491 Color: 8
Size: 213639 Color: 11

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 755256 Color: 17
Size: 128510 Color: 11
Size: 116235 Color: 5

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 776157 Color: 13
Size: 116504 Color: 0
Size: 107340 Color: 17

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 774415 Color: 16
Size: 114249 Color: 13
Size: 111337 Color: 10

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 732900 Color: 0
Size: 150847 Color: 16
Size: 116254 Color: 8

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 765607 Color: 10
Size: 122166 Color: 8
Size: 112228 Color: 7

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 778567 Color: 15
Size: 111491 Color: 18
Size: 109943 Color: 14

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 755676 Color: 18
Size: 126563 Color: 18
Size: 117762 Color: 8

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 570284 Color: 16
Size: 303542 Color: 2
Size: 126175 Color: 17

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 633085 Color: 11
Size: 216001 Color: 10
Size: 150915 Color: 11

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 702536 Color: 7
Size: 163283 Color: 0
Size: 134182 Color: 15

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 730798 Color: 2
Size: 148005 Color: 17
Size: 121198 Color: 16

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 721120 Color: 2
Size: 144297 Color: 19
Size: 134584 Color: 7

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 724684 Color: 6
Size: 145397 Color: 3
Size: 129920 Color: 17

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 746219 Color: 10
Size: 149810 Color: 1
Size: 103972 Color: 13

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 746220 Color: 17
Size: 138346 Color: 15
Size: 115435 Color: 17

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 392905 Color: 5
Size: 308000 Color: 4
Size: 299096 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 724119 Color: 17
Size: 142161 Color: 15
Size: 133721 Color: 2

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 726659 Color: 2
Size: 150832 Color: 13
Size: 122510 Color: 0

Bin 177: 0 of cap free
Amount of items: 4
Items: 
Size: 362645 Color: 8
Size: 337403 Color: 2
Size: 157709 Color: 11
Size: 142244 Color: 6

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 755030 Color: 18
Size: 126923 Color: 19
Size: 118048 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 570546 Color: 19
Size: 301113 Color: 3
Size: 128342 Color: 14

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 653421 Color: 14
Size: 173567 Color: 11
Size: 173013 Color: 13

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 690229 Color: 2
Size: 155709 Color: 18
Size: 154063 Color: 13

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 737619 Color: 5
Size: 133275 Color: 9
Size: 129107 Color: 4

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 618583 Color: 11
Size: 191043 Color: 7
Size: 190375 Color: 2

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 731845 Color: 13
Size: 134878 Color: 2
Size: 133278 Color: 4

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 758874 Color: 4
Size: 130756 Color: 14
Size: 110371 Color: 11

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 758634 Color: 18
Size: 128220 Color: 16
Size: 113147 Color: 18

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 764240 Color: 12
Size: 118840 Color: 6
Size: 116921 Color: 3

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 772121 Color: 8
Size: 120204 Color: 4
Size: 107676 Color: 10

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 779346 Color: 9
Size: 114473 Color: 19
Size: 106182 Color: 8

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 762091 Color: 19
Size: 134445 Color: 4
Size: 103465 Color: 2

Bin 191: 0 of cap free
Amount of items: 4
Items: 
Size: 505609 Color: 7
Size: 250628 Color: 6
Size: 128466 Color: 14
Size: 115298 Color: 13

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 781274 Color: 18
Size: 113913 Color: 1
Size: 104814 Color: 15

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 628918 Color: 1
Size: 188705 Color: 11
Size: 182378 Color: 18

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 726969 Color: 19
Size: 141487 Color: 19
Size: 131545 Color: 4

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 782790 Color: 9
Size: 108685 Color: 12
Size: 108526 Color: 17

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 671047 Color: 8
Size: 164482 Color: 17
Size: 164472 Color: 13

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 768700 Color: 3
Size: 116257 Color: 12
Size: 115044 Color: 19

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 792588 Color: 8
Size: 106326 Color: 11
Size: 101087 Color: 18

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 785204 Color: 19
Size: 109806 Color: 10
Size: 104991 Color: 19

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 730903 Color: 13
Size: 136388 Color: 13
Size: 132710 Color: 5

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 777479 Color: 17
Size: 119979 Color: 10
Size: 102543 Color: 2

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 781237 Color: 0
Size: 116006 Color: 17
Size: 102758 Color: 8

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 791187 Color: 5
Size: 104722 Color: 16
Size: 104092 Color: 8

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 776839 Color: 17
Size: 223162 Color: 12

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 363361 Color: 16
Size: 356220 Color: 4
Size: 280420 Color: 13

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 741436 Color: 19
Size: 133076 Color: 3
Size: 125489 Color: 12

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 765617 Color: 7
Size: 124731 Color: 3
Size: 109653 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 731841 Color: 0
Size: 152457 Color: 9
Size: 115703 Color: 16

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 758693 Color: 6
Size: 128224 Color: 14
Size: 113084 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 785983 Color: 14
Size: 110767 Color: 6
Size: 103251 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 782169 Color: 13
Size: 109494 Color: 14
Size: 108338 Color: 14

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 738018 Color: 2
Size: 134587 Color: 19
Size: 127396 Color: 10

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 772348 Color: 9
Size: 114091 Color: 16
Size: 113562 Color: 3

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 768778 Color: 11
Size: 116655 Color: 1
Size: 114568 Color: 18

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 783404 Color: 3
Size: 112297 Color: 16
Size: 104300 Color: 10

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 758947 Color: 0
Size: 140458 Color: 14
Size: 100596 Color: 4

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 695007 Color: 9
Size: 156621 Color: 3
Size: 148373 Color: 13

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 712929 Color: 12
Size: 149042 Color: 13
Size: 138030 Color: 11

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 715012 Color: 12
Size: 284989 Color: 14

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 781688 Color: 5
Size: 110311 Color: 11
Size: 108002 Color: 15

Bin 221: 0 of cap free
Amount of items: 4
Items: 
Size: 665317 Color: 19
Size: 128984 Color: 8
Size: 102880 Color: 12
Size: 102820 Color: 7

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 765034 Color: 14
Size: 119188 Color: 0
Size: 115779 Color: 7

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 787007 Color: 17
Size: 111367 Color: 2
Size: 101627 Color: 11

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 695453 Color: 11
Size: 152784 Color: 12
Size: 151764 Color: 11

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 680295 Color: 14
Size: 175947 Color: 19
Size: 143759 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 779912 Color: 18
Size: 115904 Color: 18
Size: 104185 Color: 7

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 785718 Color: 18
Size: 107853 Color: 3
Size: 106430 Color: 13

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 792565 Color: 16
Size: 106687 Color: 5
Size: 100749 Color: 4

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 653995 Color: 3
Size: 173206 Color: 19
Size: 172800 Color: 3

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 765411 Color: 16
Size: 119340 Color: 10
Size: 115250 Color: 10

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 779147 Color: 10
Size: 119511 Color: 17
Size: 101343 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 762385 Color: 17
Size: 128649 Color: 8
Size: 108967 Color: 8

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 739423 Color: 3
Size: 137321 Color: 12
Size: 123257 Color: 11

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 766892 Color: 15
Size: 119092 Color: 12
Size: 114017 Color: 7

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 619062 Color: 5
Size: 249370 Color: 10
Size: 131569 Color: 7

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 668435 Color: 13
Size: 167220 Color: 17
Size: 164346 Color: 11

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 760525 Color: 17
Size: 120190 Color: 12
Size: 119286 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 778074 Color: 6
Size: 112186 Color: 15
Size: 109741 Color: 2

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 668517 Color: 9
Size: 167021 Color: 12
Size: 164463 Color: 19

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 770023 Color: 4
Size: 115026 Color: 18
Size: 114952 Color: 17

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 739712 Color: 15
Size: 135830 Color: 6
Size: 124459 Color: 12

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 605255 Color: 3
Size: 197441 Color: 1
Size: 197305 Color: 9

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 528702 Color: 13
Size: 250748 Color: 1
Size: 220551 Color: 14

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 781278 Color: 10
Size: 115781 Color: 16
Size: 102942 Color: 13

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 756878 Color: 14
Size: 133352 Color: 2
Size: 109771 Color: 1

Bin 246: 0 of cap free
Amount of items: 2
Items: 
Size: 620329 Color: 1
Size: 379672 Color: 19

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 767354 Color: 11
Size: 125788 Color: 1
Size: 106859 Color: 3

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 371859 Color: 9
Size: 333772 Color: 18
Size: 294370 Color: 4

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 788723 Color: 10
Size: 110686 Color: 8
Size: 100592 Color: 13

Bin 250: 0 of cap free
Amount of items: 4
Items: 
Size: 552535 Color: 15
Size: 209496 Color: 15
Size: 137773 Color: 16
Size: 100197 Color: 19

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 753733 Color: 16
Size: 132377 Color: 3
Size: 113891 Color: 15

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 772823 Color: 6
Size: 119535 Color: 5
Size: 107643 Color: 9

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 685021 Color: 3
Size: 158173 Color: 6
Size: 156807 Color: 14

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 773737 Color: 5
Size: 120752 Color: 17
Size: 105512 Color: 4

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 727607 Color: 2
Size: 137665 Color: 2
Size: 134729 Color: 9

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 683185 Color: 7
Size: 162654 Color: 16
Size: 154162 Color: 4

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 769904 Color: 2
Size: 118457 Color: 14
Size: 111640 Color: 14

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 778868 Color: 16
Size: 112275 Color: 17
Size: 108858 Color: 9

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 656274 Color: 3
Size: 176564 Color: 15
Size: 167163 Color: 9

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 719494 Color: 8
Size: 140504 Color: 4
Size: 140003 Color: 10

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 549510 Color: 0
Size: 262425 Color: 2
Size: 188066 Color: 17

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 511202 Color: 19
Size: 244942 Color: 13
Size: 243857 Color: 7

Bin 263: 0 of cap free
Amount of items: 2
Items: 
Size: 692494 Color: 15
Size: 307507 Color: 12

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 627576 Color: 7
Size: 189576 Color: 0
Size: 182849 Color: 5

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 748495 Color: 1
Size: 150005 Color: 8
Size: 101501 Color: 17

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 775904 Color: 2
Size: 113151 Color: 4
Size: 110946 Color: 10

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 758926 Color: 1
Size: 136126 Color: 9
Size: 104949 Color: 9

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 785456 Color: 16
Size: 110703 Color: 10
Size: 103842 Color: 8

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 727967 Color: 8
Size: 155428 Color: 15
Size: 116606 Color: 16

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 789756 Color: 12
Size: 106922 Color: 16
Size: 103323 Color: 4

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 749863 Color: 14
Size: 137202 Color: 11
Size: 112936 Color: 9

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 630215 Color: 12
Size: 232906 Color: 7
Size: 136880 Color: 9

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 500634 Color: 12
Size: 499367 Color: 11

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 751079 Color: 6
Size: 129994 Color: 17
Size: 118928 Color: 3

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 766947 Color: 17
Size: 130872 Color: 1
Size: 102182 Color: 13

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 549391 Color: 15
Size: 239517 Color: 1
Size: 211093 Color: 12

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 354856 Color: 13
Size: 344392 Color: 1
Size: 300753 Color: 5

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 792365 Color: 3
Size: 107293 Color: 0
Size: 100343 Color: 18

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 772406 Color: 5
Size: 227595 Color: 19

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 720792 Color: 14
Size: 152907 Color: 4
Size: 126302 Color: 15

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 688456 Color: 8
Size: 311545 Color: 3

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 567578 Color: 12
Size: 216369 Color: 18
Size: 216054 Color: 5

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 641149 Color: 9
Size: 179909 Color: 17
Size: 178943 Color: 18

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 789539 Color: 0
Size: 109169 Color: 3
Size: 101293 Color: 17

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 728885 Color: 12
Size: 140488 Color: 14
Size: 130628 Color: 6

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 646850 Color: 12
Size: 353151 Color: 4

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 766166 Color: 17
Size: 131853 Color: 13
Size: 101982 Color: 2

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 530725 Color: 11
Size: 243136 Color: 15
Size: 226140 Color: 17

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 780853 Color: 8
Size: 110573 Color: 6
Size: 108575 Color: 11

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 784615 Color: 11
Size: 113654 Color: 12
Size: 101732 Color: 6

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 792363 Color: 12
Size: 104229 Color: 4
Size: 103409 Color: 2

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 781447 Color: 8
Size: 114053 Color: 13
Size: 104501 Color: 18

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 549715 Color: 11
Size: 280526 Color: 5
Size: 169760 Color: 6

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 757169 Color: 13
Size: 121830 Color: 14
Size: 121002 Color: 10

Bin 295: 0 of cap free
Amount of items: 4
Items: 
Size: 587718 Color: 3
Size: 204427 Color: 11
Size: 107669 Color: 17
Size: 100187 Color: 11

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 465068 Color: 8
Size: 291662 Color: 11
Size: 243271 Color: 16

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 454673 Color: 5
Size: 334411 Color: 18
Size: 210917 Color: 14

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 707665 Color: 17
Size: 148180 Color: 5
Size: 144156 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 537690 Color: 8
Size: 234587 Color: 6
Size: 227724 Color: 14

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 471789 Color: 16
Size: 272528 Color: 3
Size: 255684 Color: 17

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 454793 Color: 2
Size: 308169 Color: 0
Size: 237039 Color: 17

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 757292 Color: 17
Size: 123335 Color: 11
Size: 119374 Color: 11

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 758895 Color: 6
Size: 129326 Color: 6
Size: 111780 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 346344 Color: 3
Size: 345215 Color: 9
Size: 308442 Color: 4

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 354956 Color: 11
Size: 336891 Color: 0
Size: 308154 Color: 15

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 365399 Color: 8
Size: 326141 Color: 18
Size: 308461 Color: 15

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 461958 Color: 5
Size: 307411 Color: 0
Size: 230632 Color: 5

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 396983 Color: 13
Size: 306904 Color: 12
Size: 296114 Color: 14

Bin 309: 0 of cap free
Amount of items: 4
Items: 
Size: 304488 Color: 9
Size: 299534 Color: 7
Size: 295154 Color: 6
Size: 100825 Color: 19

Bin 310: 0 of cap free
Amount of items: 4
Items: 
Size: 293579 Color: 13
Size: 293164 Color: 8
Size: 291438 Color: 6
Size: 121820 Color: 19

Bin 311: 0 of cap free
Amount of items: 4
Items: 
Size: 288390 Color: 4
Size: 286393 Color: 17
Size: 285635 Color: 18
Size: 139583 Color: 16

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 433778 Color: 3
Size: 286441 Color: 19
Size: 279782 Color: 19

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 369029 Color: 8
Size: 345078 Color: 17
Size: 285894 Color: 1

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 369038 Color: 2
Size: 350406 Color: 3
Size: 280557 Color: 2

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 368463 Color: 9
Size: 337651 Color: 14
Size: 293887 Color: 18

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 371202 Color: 2
Size: 329229 Color: 7
Size: 299570 Color: 8

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 370217 Color: 14
Size: 330093 Color: 16
Size: 299691 Color: 9

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 371615 Color: 9
Size: 327938 Color: 19
Size: 300448 Color: 11

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 458448 Color: 6
Size: 271640 Color: 0
Size: 269913 Color: 15

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 464503 Color: 18
Size: 269989 Color: 5
Size: 265509 Color: 6

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 502892 Color: 0
Size: 252492 Color: 14
Size: 244617 Color: 14

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 503762 Color: 6
Size: 250894 Color: 16
Size: 245345 Color: 3

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 504429 Color: 9
Size: 495572 Color: 3

Bin 324: 0 of cap free
Amount of items: 2
Items: 
Size: 508287 Color: 7
Size: 491714 Color: 6

Bin 325: 0 of cap free
Amount of items: 2
Items: 
Size: 512425 Color: 18
Size: 487576 Color: 15

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 524116 Color: 6
Size: 475885 Color: 1

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 521319 Color: 9
Size: 478682 Color: 17

Bin 328: 0 of cap free
Amount of items: 2
Items: 
Size: 533984 Color: 14
Size: 466017 Color: 12

Bin 329: 0 of cap free
Amount of items: 2
Items: 
Size: 534585 Color: 8
Size: 465416 Color: 9

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 545529 Color: 11
Size: 454472 Color: 3

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 549060 Color: 17
Size: 450941 Color: 19

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 553547 Color: 6
Size: 446454 Color: 12

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 558533 Color: 4
Size: 441468 Color: 15

Bin 334: 0 of cap free
Amount of items: 2
Items: 
Size: 564664 Color: 0
Size: 435337 Color: 8

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 569516 Color: 11
Size: 430485 Color: 3

Bin 336: 0 of cap free
Amount of items: 2
Items: 
Size: 572425 Color: 7
Size: 427576 Color: 18

Bin 337: 0 of cap free
Amount of items: 2
Items: 
Size: 578703 Color: 3
Size: 421298 Color: 0

Bin 338: 0 of cap free
Amount of items: 2
Items: 
Size: 598561 Color: 12
Size: 401440 Color: 14

Bin 339: 0 of cap free
Amount of items: 2
Items: 
Size: 599004 Color: 16
Size: 400997 Color: 12

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 605022 Color: 2
Size: 198279 Color: 15
Size: 196700 Color: 14

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 608366 Color: 10
Size: 391635 Color: 17

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 608580 Color: 0
Size: 195996 Color: 13
Size: 195425 Color: 12

Bin 343: 0 of cap free
Amount of items: 2
Items: 
Size: 610045 Color: 8
Size: 389956 Color: 17

Bin 344: 0 of cap free
Amount of items: 2
Items: 
Size: 612274 Color: 6
Size: 387727 Color: 4

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 613577 Color: 15
Size: 386424 Color: 3

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 613807 Color: 19
Size: 194664 Color: 6
Size: 191530 Color: 10

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 618749 Color: 3
Size: 381252 Color: 4

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 625126 Color: 14
Size: 187738 Color: 14
Size: 187137 Color: 2

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 628314 Color: 14
Size: 186278 Color: 15
Size: 185409 Color: 5

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 628743 Color: 2
Size: 186363 Color: 12
Size: 184895 Color: 16

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 628826 Color: 4
Size: 185950 Color: 14
Size: 185225 Color: 16

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 628930 Color: 18
Size: 187611 Color: 3
Size: 183460 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 629373 Color: 5
Size: 186248 Color: 19
Size: 184380 Color: 14

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 632061 Color: 7
Size: 185221 Color: 3
Size: 182719 Color: 8

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 633552 Color: 6
Size: 184600 Color: 12
Size: 181849 Color: 6

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 633629 Color: 11
Size: 184504 Color: 18
Size: 181868 Color: 19

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 634977 Color: 13
Size: 183525 Color: 0
Size: 181499 Color: 13

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 635014 Color: 3
Size: 183497 Color: 4
Size: 181490 Color: 2

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 635415 Color: 9
Size: 364586 Color: 3

Bin 360: 0 of cap free
Amount of items: 2
Items: 
Size: 644370 Color: 19
Size: 355631 Color: 17

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 645823 Color: 9
Size: 178827 Color: 3
Size: 175351 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 647072 Color: 16
Size: 176474 Color: 15
Size: 176455 Color: 13

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 649219 Color: 1
Size: 350782 Color: 3

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 654468 Color: 2
Size: 173138 Color: 11
Size: 172395 Color: 12

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 658702 Color: 7
Size: 170848 Color: 7
Size: 170451 Color: 6

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 660917 Color: 8
Size: 169750 Color: 4
Size: 169334 Color: 13

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 661976 Color: 16
Size: 338025 Color: 5

Bin 368: 0 of cap free
Amount of items: 2
Items: 
Size: 662073 Color: 16
Size: 337928 Color: 8

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 669569 Color: 1
Size: 167103 Color: 19
Size: 163329 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 670689 Color: 19
Size: 165199 Color: 17
Size: 164113 Color: 15

Bin 371: 0 of cap free
Amount of items: 2
Items: 
Size: 675367 Color: 13
Size: 324634 Color: 12

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 677316 Color: 2
Size: 322685 Color: 13

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 682205 Color: 15
Size: 317796 Color: 7

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 691655 Color: 14
Size: 308346 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 691662 Color: 18
Size: 154975 Color: 16
Size: 153364 Color: 7

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 707153 Color: 5
Size: 292848 Color: 14

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 710733 Color: 18
Size: 289268 Color: 13

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 711483 Color: 9
Size: 146811 Color: 13
Size: 141707 Color: 11

Bin 379: 0 of cap free
Amount of items: 2
Items: 
Size: 716883 Color: 18
Size: 283118 Color: 7

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 717961 Color: 18
Size: 282040 Color: 5

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 728361 Color: 4
Size: 271640 Color: 19

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 728771 Color: 15
Size: 137500 Color: 12
Size: 133730 Color: 14

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 731365 Color: 2
Size: 136833 Color: 3
Size: 131803 Color: 8

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 732112 Color: 17
Size: 137167 Color: 1
Size: 130722 Color: 8

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 732912 Color: 11
Size: 133866 Color: 17
Size: 133223 Color: 19

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 733206 Color: 7
Size: 135629 Color: 18
Size: 131166 Color: 2

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 733847 Color: 10
Size: 134497 Color: 14
Size: 131657 Color: 18

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 735499 Color: 12
Size: 133894 Color: 14
Size: 130608 Color: 18

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 736094 Color: 7
Size: 132121 Color: 14
Size: 131786 Color: 18

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 736753 Color: 8
Size: 132890 Color: 5
Size: 130358 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 737318 Color: 5
Size: 131676 Color: 3
Size: 131007 Color: 0

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 738573 Color: 7
Size: 261428 Color: 13

Bin 393: 0 of cap free
Amount of items: 2
Items: 
Size: 739234 Color: 19
Size: 260767 Color: 14

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 751137 Color: 18
Size: 124498 Color: 16
Size: 124366 Color: 7

Bin 395: 0 of cap free
Amount of items: 2
Items: 
Size: 752742 Color: 7
Size: 247259 Color: 4

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 755293 Color: 0
Size: 123053 Color: 13
Size: 121655 Color: 15

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 757244 Color: 13
Size: 121526 Color: 2
Size: 121231 Color: 6

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 765782 Color: 8
Size: 117725 Color: 17
Size: 116494 Color: 3

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 769125 Color: 10
Size: 116705 Color: 19
Size: 114171 Color: 2

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 770217 Color: 16
Size: 115288 Color: 9
Size: 114496 Color: 14

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 770905 Color: 4
Size: 229096 Color: 6

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 783396 Color: 15
Size: 108652 Color: 2
Size: 107953 Color: 3

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 789242 Color: 5
Size: 210759 Color: 15

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 789804 Color: 4
Size: 105387 Color: 9
Size: 104810 Color: 0

Bin 405: 0 of cap free
Amount of items: 2
Items: 
Size: 798839 Color: 11
Size: 201162 Color: 2

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 734839 Color: 15
Size: 141237 Color: 5
Size: 123924 Color: 16

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 737986 Color: 12
Size: 152548 Color: 15
Size: 109466 Color: 7

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 714433 Color: 13
Size: 157570 Color: 1
Size: 127997 Color: 14

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 468926 Color: 12
Size: 266544 Color: 5
Size: 264530 Color: 8

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 700576 Color: 10
Size: 159371 Color: 14
Size: 140053 Color: 13

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 555177 Color: 8
Size: 314956 Color: 18
Size: 129867 Color: 8

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 741884 Color: 3
Size: 129456 Color: 17
Size: 128660 Color: 3

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 712522 Color: 3
Size: 143791 Color: 12
Size: 143687 Color: 3

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 727037 Color: 5
Size: 140403 Color: 19
Size: 132560 Color: 17

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 555853 Color: 3
Size: 226996 Color: 3
Size: 217151 Color: 18

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 740292 Color: 0
Size: 133101 Color: 15
Size: 126607 Color: 14

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 660457 Color: 6
Size: 198881 Color: 4
Size: 140662 Color: 14

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 457746 Color: 4
Size: 273396 Color: 16
Size: 268858 Color: 10

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 758317 Color: 9
Size: 120912 Color: 19
Size: 120771 Color: 12

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 726260 Color: 0
Size: 139721 Color: 5
Size: 134019 Color: 6

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 788366 Color: 10
Size: 106741 Color: 15
Size: 104893 Color: 8

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 770454 Color: 3
Size: 115095 Color: 18
Size: 114451 Color: 16

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 634595 Color: 15
Size: 193877 Color: 8
Size: 171528 Color: 12

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 393328 Color: 19
Size: 322240 Color: 8
Size: 284432 Color: 2

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 676006 Color: 8
Size: 169640 Color: 8
Size: 154354 Color: 12

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 787136 Color: 15
Size: 106488 Color: 0
Size: 106376 Color: 1

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 683256 Color: 15
Size: 164356 Color: 2
Size: 152388 Color: 6

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 625200 Color: 17
Size: 187525 Color: 17
Size: 187275 Color: 8

Bin 429: 1 of cap free
Amount of items: 2
Items: 
Size: 706450 Color: 16
Size: 293550 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 745777 Color: 4
Size: 128653 Color: 11
Size: 125570 Color: 4

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 770380 Color: 14
Size: 115021 Color: 0
Size: 114599 Color: 14

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 457732 Color: 8
Size: 277031 Color: 2
Size: 265237 Color: 17

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 497585 Color: 0
Size: 268335 Color: 14
Size: 234080 Color: 14

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 769950 Color: 16
Size: 120699 Color: 1
Size: 109351 Color: 2

Bin 435: 1 of cap free
Amount of items: 2
Items: 
Size: 660454 Color: 0
Size: 339546 Color: 9

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 372338 Color: 13
Size: 339559 Color: 17
Size: 288103 Color: 6

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 411558 Color: 18
Size: 297556 Color: 15
Size: 290886 Color: 14

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 730037 Color: 4
Size: 138894 Color: 3
Size: 131069 Color: 8

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 624588 Color: 1
Size: 190270 Color: 9
Size: 185142 Color: 15

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 663823 Color: 3
Size: 175896 Color: 12
Size: 160281 Color: 4

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 698336 Color: 5
Size: 151023 Color: 1
Size: 150641 Color: 13

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 701471 Color: 13
Size: 160840 Color: 19
Size: 137689 Color: 10

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 380852 Color: 4
Size: 318530 Color: 17
Size: 300618 Color: 3

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 518860 Color: 0
Size: 245912 Color: 1
Size: 235228 Color: 6

Bin 445: 1 of cap free
Amount of items: 2
Items: 
Size: 741187 Color: 6
Size: 258813 Color: 11

Bin 446: 1 of cap free
Amount of items: 2
Items: 
Size: 737276 Color: 11
Size: 262724 Color: 10

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 611429 Color: 5
Size: 195418 Color: 13
Size: 193153 Color: 0

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 683227 Color: 2
Size: 164590 Color: 11
Size: 152183 Color: 12

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 705641 Color: 0
Size: 147553 Color: 4
Size: 146806 Color: 14

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 648563 Color: 8
Size: 178666 Color: 1
Size: 172771 Color: 7

Bin 451: 1 of cap free
Amount of items: 2
Items: 
Size: 642515 Color: 6
Size: 357485 Color: 15

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 625522 Color: 1
Size: 188349 Color: 1
Size: 186129 Color: 19

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 558470 Color: 7
Size: 221141 Color: 3
Size: 220389 Color: 5

Bin 454: 1 of cap free
Amount of items: 2
Items: 
Size: 663183 Color: 16
Size: 336817 Color: 11

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 469023 Color: 6
Size: 273748 Color: 15
Size: 257229 Color: 0

Bin 456: 1 of cap free
Amount of items: 2
Items: 
Size: 783595 Color: 11
Size: 216405 Color: 8

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 645133 Color: 17
Size: 179424 Color: 5
Size: 175443 Color: 7

Bin 458: 1 of cap free
Amount of items: 2
Items: 
Size: 638482 Color: 12
Size: 361518 Color: 16

Bin 459: 1 of cap free
Amount of items: 2
Items: 
Size: 730040 Color: 3
Size: 269960 Color: 5

Bin 460: 1 of cap free
Amount of items: 2
Items: 
Size: 679241 Color: 1
Size: 320759 Color: 7

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 489575 Color: 11
Size: 263986 Color: 5
Size: 246439 Color: 12

Bin 462: 1 of cap free
Amount of items: 2
Items: 
Size: 793777 Color: 13
Size: 206223 Color: 16

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 459899 Color: 19
Size: 284413 Color: 11
Size: 255688 Color: 19

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 345028 Color: 3
Size: 334000 Color: 13
Size: 320972 Color: 1

Bin 465: 1 of cap free
Amount of items: 2
Items: 
Size: 569882 Color: 12
Size: 430118 Color: 18

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 434960 Color: 12
Size: 284815 Color: 16
Size: 280225 Color: 0

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 365242 Color: 0
Size: 333836 Color: 6
Size: 300922 Color: 10

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 372274 Color: 2
Size: 344383 Color: 12
Size: 283343 Color: 6

Bin 469: 1 of cap free
Amount of items: 2
Items: 
Size: 502545 Color: 18
Size: 497455 Color: 10

Bin 470: 1 of cap free
Amount of items: 2
Items: 
Size: 502896 Color: 17
Size: 497104 Color: 11

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 504568 Color: 19
Size: 250786 Color: 19
Size: 244646 Color: 5

Bin 472: 1 of cap free
Amount of items: 2
Items: 
Size: 513041 Color: 5
Size: 486959 Color: 9

Bin 473: 1 of cap free
Amount of items: 2
Items: 
Size: 529260 Color: 3
Size: 470740 Color: 4

Bin 474: 1 of cap free
Amount of items: 2
Items: 
Size: 546076 Color: 8
Size: 453924 Color: 7

Bin 475: 1 of cap free
Amount of items: 2
Items: 
Size: 548787 Color: 18
Size: 451213 Color: 17

Bin 476: 1 of cap free
Amount of items: 2
Items: 
Size: 550186 Color: 11
Size: 449814 Color: 1

Bin 477: 1 of cap free
Amount of items: 2
Items: 
Size: 555204 Color: 7
Size: 444796 Color: 0

Bin 478: 1 of cap free
Amount of items: 2
Items: 
Size: 555664 Color: 14
Size: 444336 Color: 8

Bin 479: 1 of cap free
Amount of items: 2
Items: 
Size: 557856 Color: 17
Size: 442144 Color: 15

Bin 480: 1 of cap free
Amount of items: 2
Items: 
Size: 564705 Color: 16
Size: 435295 Color: 2

Bin 481: 1 of cap free
Amount of items: 2
Items: 
Size: 572160 Color: 15
Size: 427840 Color: 7

Bin 482: 1 of cap free
Amount of items: 2
Items: 
Size: 572247 Color: 15
Size: 427753 Color: 2

Bin 483: 1 of cap free
Amount of items: 2
Items: 
Size: 577625 Color: 10
Size: 422375 Color: 18

Bin 484: 1 of cap free
Amount of items: 2
Items: 
Size: 579957 Color: 19
Size: 420043 Color: 7

Bin 485: 1 of cap free
Amount of items: 2
Items: 
Size: 601965 Color: 8
Size: 398035 Color: 16

Bin 486: 1 of cap free
Amount of items: 2
Items: 
Size: 601988 Color: 9
Size: 398012 Color: 3

Bin 487: 1 of cap free
Amount of items: 2
Items: 
Size: 605636 Color: 6
Size: 394364 Color: 17

Bin 488: 1 of cap free
Amount of items: 3
Items: 
Size: 608634 Color: 17
Size: 196739 Color: 1
Size: 194627 Color: 6

Bin 489: 1 of cap free
Amount of items: 2
Items: 
Size: 615893 Color: 6
Size: 384107 Color: 17

Bin 490: 1 of cap free
Amount of items: 2
Items: 
Size: 615947 Color: 9
Size: 384053 Color: 3

Bin 491: 1 of cap free
Amount of items: 2
Items: 
Size: 617565 Color: 14
Size: 382435 Color: 12

Bin 492: 1 of cap free
Amount of items: 2
Items: 
Size: 621239 Color: 0
Size: 378761 Color: 10

Bin 493: 1 of cap free
Amount of items: 2
Items: 
Size: 625042 Color: 7
Size: 374958 Color: 2

Bin 494: 1 of cap free
Amount of items: 3
Items: 
Size: 626427 Color: 13
Size: 187146 Color: 2
Size: 186427 Color: 5

Bin 495: 1 of cap free
Amount of items: 2
Items: 
Size: 626947 Color: 2
Size: 373053 Color: 15

Bin 496: 1 of cap free
Amount of items: 2
Items: 
Size: 628860 Color: 4
Size: 371140 Color: 6

Bin 497: 1 of cap free
Amount of items: 3
Items: 
Size: 629288 Color: 2
Size: 186543 Color: 16
Size: 184169 Color: 3

Bin 498: 1 of cap free
Amount of items: 2
Items: 
Size: 630947 Color: 19
Size: 369053 Color: 9

Bin 499: 1 of cap free
Amount of items: 2
Items: 
Size: 643988 Color: 6
Size: 356012 Color: 11

Bin 500: 1 of cap free
Amount of items: 3
Items: 
Size: 654389 Color: 12
Size: 173237 Color: 7
Size: 172374 Color: 4

Bin 501: 1 of cap free
Amount of items: 3
Items: 
Size: 663966 Color: 18
Size: 168280 Color: 6
Size: 167754 Color: 2

Bin 502: 1 of cap free
Amount of items: 2
Items: 
Size: 666797 Color: 17
Size: 333203 Color: 4

Bin 503: 1 of cap free
Amount of items: 3
Items: 
Size: 669046 Color: 8
Size: 167964 Color: 18
Size: 162990 Color: 3

Bin 504: 1 of cap free
Amount of items: 3
Items: 
Size: 670507 Color: 9
Size: 164850 Color: 13
Size: 164643 Color: 16

Bin 505: 1 of cap free
Amount of items: 2
Items: 
Size: 675111 Color: 7
Size: 324889 Color: 0

Bin 506: 1 of cap free
Amount of items: 3
Items: 
Size: 677089 Color: 18
Size: 161706 Color: 7
Size: 161205 Color: 7

Bin 507: 1 of cap free
Amount of items: 2
Items: 
Size: 689450 Color: 19
Size: 310550 Color: 1

Bin 508: 1 of cap free
Amount of items: 3
Items: 
Size: 690926 Color: 7
Size: 155926 Color: 19
Size: 153148 Color: 6

Bin 509: 1 of cap free
Amount of items: 3
Items: 
Size: 693210 Color: 11
Size: 153425 Color: 19
Size: 153365 Color: 13

Bin 510: 1 of cap free
Amount of items: 2
Items: 
Size: 694552 Color: 19
Size: 305448 Color: 15

Bin 511: 1 of cap free
Amount of items: 3
Items: 
Size: 696756 Color: 17
Size: 151818 Color: 17
Size: 151426 Color: 0

Bin 512: 1 of cap free
Amount of items: 2
Items: 
Size: 697217 Color: 15
Size: 302783 Color: 12

Bin 513: 1 of cap free
Amount of items: 3
Items: 
Size: 704042 Color: 14
Size: 148439 Color: 7
Size: 147519 Color: 10

Bin 514: 1 of cap free
Amount of items: 2
Items: 
Size: 714403 Color: 16
Size: 285597 Color: 2

Bin 515: 1 of cap free
Amount of items: 2
Items: 
Size: 715096 Color: 12
Size: 284904 Color: 0

Bin 516: 1 of cap free
Amount of items: 2
Items: 
Size: 717333 Color: 11
Size: 282667 Color: 5

Bin 517: 1 of cap free
Amount of items: 2
Items: 
Size: 720627 Color: 2
Size: 279373 Color: 10

Bin 518: 1 of cap free
Amount of items: 3
Items: 
Size: 720752 Color: 13
Size: 139892 Color: 8
Size: 139356 Color: 10

Bin 519: 1 of cap free
Amount of items: 2
Items: 
Size: 722050 Color: 15
Size: 277950 Color: 9

Bin 520: 1 of cap free
Amount of items: 2
Items: 
Size: 722336 Color: 17
Size: 277664 Color: 1

Bin 521: 1 of cap free
Amount of items: 3
Items: 
Size: 732551 Color: 14
Size: 135110 Color: 1
Size: 132339 Color: 4

Bin 522: 1 of cap free
Amount of items: 3
Items: 
Size: 732777 Color: 18
Size: 134619 Color: 6
Size: 132604 Color: 13

Bin 523: 1 of cap free
Amount of items: 3
Items: 
Size: 733336 Color: 6
Size: 134115 Color: 1
Size: 132549 Color: 16

Bin 524: 1 of cap free
Amount of items: 3
Items: 
Size: 733360 Color: 6
Size: 133834 Color: 5
Size: 132806 Color: 1

Bin 525: 1 of cap free
Amount of items: 2
Items: 
Size: 739090 Color: 14
Size: 260910 Color: 7

Bin 526: 1 of cap free
Amount of items: 2
Items: 
Size: 741363 Color: 1
Size: 258637 Color: 9

Bin 527: 1 of cap free
Amount of items: 2
Items: 
Size: 772137 Color: 4
Size: 227863 Color: 6

Bin 528: 1 of cap free
Amount of items: 3
Items: 
Size: 773817 Color: 5
Size: 113170 Color: 13
Size: 113013 Color: 11

Bin 529: 1 of cap free
Amount of items: 2
Items: 
Size: 799767 Color: 6
Size: 200233 Color: 13

Bin 530: 2 of cap free
Amount of items: 2
Items: 
Size: 719101 Color: 12
Size: 280898 Color: 0

Bin 531: 2 of cap free
Amount of items: 3
Items: 
Size: 731921 Color: 0
Size: 136671 Color: 9
Size: 131407 Color: 12

Bin 532: 2 of cap free
Amount of items: 3
Items: 
Size: 733278 Color: 6
Size: 134767 Color: 12
Size: 131954 Color: 3

Bin 533: 2 of cap free
Amount of items: 3
Items: 
Size: 773744 Color: 2
Size: 116923 Color: 13
Size: 109332 Color: 19

Bin 534: 2 of cap free
Amount of items: 3
Items: 
Size: 674563 Color: 0
Size: 171993 Color: 13
Size: 153443 Color: 8

Bin 535: 2 of cap free
Amount of items: 3
Items: 
Size: 740207 Color: 8
Size: 131212 Color: 12
Size: 128580 Color: 12

Bin 536: 2 of cap free
Amount of items: 3
Items: 
Size: 753752 Color: 17
Size: 123826 Color: 10
Size: 122421 Color: 7

Bin 537: 2 of cap free
Amount of items: 3
Items: 
Size: 361308 Color: 17
Size: 358329 Color: 13
Size: 280362 Color: 7

Bin 538: 2 of cap free
Amount of items: 3
Items: 
Size: 693131 Color: 8
Size: 166521 Color: 4
Size: 140347 Color: 15

Bin 539: 2 of cap free
Amount of items: 3
Items: 
Size: 775108 Color: 11
Size: 115657 Color: 15
Size: 109234 Color: 6

Bin 540: 2 of cap free
Amount of items: 3
Items: 
Size: 650495 Color: 12
Size: 187774 Color: 5
Size: 161730 Color: 5

Bin 541: 2 of cap free
Amount of items: 2
Items: 
Size: 747165 Color: 5
Size: 252834 Color: 3

Bin 542: 2 of cap free
Amount of items: 3
Items: 
Size: 587662 Color: 9
Size: 209634 Color: 1
Size: 202703 Color: 8

Bin 543: 2 of cap free
Amount of items: 3
Items: 
Size: 725341 Color: 1
Size: 137773 Color: 11
Size: 136885 Color: 19

Bin 544: 2 of cap free
Amount of items: 3
Items: 
Size: 536884 Color: 16
Size: 232356 Color: 13
Size: 230759 Color: 9

Bin 545: 2 of cap free
Amount of items: 3
Items: 
Size: 722285 Color: 6
Size: 142454 Color: 4
Size: 135260 Color: 1

Bin 546: 2 of cap free
Amount of items: 3
Items: 
Size: 504958 Color: 3
Size: 264439 Color: 9
Size: 230602 Color: 14

Bin 547: 2 of cap free
Amount of items: 3
Items: 
Size: 350692 Color: 3
Size: 339068 Color: 2
Size: 310239 Color: 5

Bin 548: 2 of cap free
Amount of items: 3
Items: 
Size: 727013 Color: 8
Size: 137738 Color: 12
Size: 135248 Color: 4

Bin 549: 2 of cap free
Amount of items: 3
Items: 
Size: 669529 Color: 3
Size: 197174 Color: 2
Size: 133296 Color: 15

Bin 550: 2 of cap free
Amount of items: 3
Items: 
Size: 617969 Color: 11
Size: 191321 Color: 18
Size: 190709 Color: 13

Bin 551: 2 of cap free
Amount of items: 3
Items: 
Size: 614974 Color: 7
Size: 192935 Color: 0
Size: 192090 Color: 4

Bin 552: 2 of cap free
Amount of items: 3
Items: 
Size: 733725 Color: 2
Size: 138239 Color: 10
Size: 128035 Color: 4

Bin 553: 2 of cap free
Amount of items: 3
Items: 
Size: 731734 Color: 15
Size: 137533 Color: 3
Size: 130732 Color: 17

Bin 554: 2 of cap free
Amount of items: 3
Items: 
Size: 604745 Color: 2
Size: 198418 Color: 0
Size: 196836 Color: 4

Bin 555: 2 of cap free
Amount of items: 2
Items: 
Size: 702151 Color: 15
Size: 297848 Color: 1

Bin 556: 2 of cap free
Amount of items: 2
Items: 
Size: 719884 Color: 14
Size: 280115 Color: 6

Bin 557: 2 of cap free
Amount of items: 3
Items: 
Size: 650478 Color: 4
Size: 174989 Color: 9
Size: 174532 Color: 8

Bin 558: 2 of cap free
Amount of items: 3
Items: 
Size: 354847 Color: 0
Size: 345316 Color: 14
Size: 299836 Color: 5

Bin 559: 2 of cap free
Amount of items: 3
Items: 
Size: 393302 Color: 18
Size: 328465 Color: 9
Size: 278232 Color: 13

Bin 560: 2 of cap free
Amount of items: 3
Items: 
Size: 698213 Color: 1
Size: 153151 Color: 4
Size: 148635 Color: 2

Bin 561: 2 of cap free
Amount of items: 3
Items: 
Size: 638551 Color: 0
Size: 180878 Color: 6
Size: 180570 Color: 19

Bin 562: 2 of cap free
Amount of items: 3
Items: 
Size: 616428 Color: 13
Size: 215441 Color: 18
Size: 168130 Color: 7

Bin 563: 2 of cap free
Amount of items: 3
Items: 
Size: 393243 Color: 17
Size: 313235 Color: 7
Size: 293521 Color: 11

Bin 564: 2 of cap free
Amount of items: 3
Items: 
Size: 754740 Color: 13
Size: 123460 Color: 1
Size: 121799 Color: 3

Bin 565: 2 of cap free
Amount of items: 3
Items: 
Size: 704322 Color: 11
Size: 148070 Color: 6
Size: 147607 Color: 16

Bin 566: 2 of cap free
Amount of items: 3
Items: 
Size: 627217 Color: 13
Size: 188032 Color: 5
Size: 184750 Color: 0

Bin 567: 2 of cap free
Amount of items: 3
Items: 
Size: 504937 Color: 6
Size: 264416 Color: 5
Size: 230646 Color: 19

Bin 568: 2 of cap free
Amount of items: 3
Items: 
Size: 663439 Color: 8
Size: 170993 Color: 18
Size: 165567 Color: 4

Bin 569: 2 of cap free
Amount of items: 3
Items: 
Size: 622233 Color: 7
Size: 188930 Color: 2
Size: 188836 Color: 14

Bin 570: 2 of cap free
Amount of items: 2
Items: 
Size: 594635 Color: 7
Size: 405364 Color: 8

Bin 571: 2 of cap free
Amount of items: 2
Items: 
Size: 587328 Color: 1
Size: 412671 Color: 5

Bin 572: 2 of cap free
Amount of items: 2
Items: 
Size: 668654 Color: 0
Size: 331345 Color: 15

Bin 573: 2 of cap free
Amount of items: 2
Items: 
Size: 642509 Color: 1
Size: 357490 Color: 6

Bin 574: 2 of cap free
Amount of items: 2
Items: 
Size: 663999 Color: 12
Size: 336000 Color: 7

Bin 575: 2 of cap free
Amount of items: 2
Items: 
Size: 688158 Color: 17
Size: 311841 Color: 14

Bin 576: 2 of cap free
Amount of items: 2
Items: 
Size: 744073 Color: 11
Size: 255926 Color: 14

Bin 577: 2 of cap free
Amount of items: 3
Items: 
Size: 443034 Color: 11
Size: 280469 Color: 5
Size: 276496 Color: 5

Bin 578: 2 of cap free
Amount of items: 3
Items: 
Size: 346577 Color: 19
Size: 345271 Color: 10
Size: 308151 Color: 15

Bin 579: 2 of cap free
Amount of items: 3
Items: 
Size: 370275 Color: 10
Size: 342960 Color: 14
Size: 286764 Color: 8

Bin 580: 2 of cap free
Amount of items: 3
Items: 
Size: 460965 Color: 4
Size: 272671 Color: 0
Size: 266363 Color: 12

Bin 581: 2 of cap free
Amount of items: 2
Items: 
Size: 503488 Color: 5
Size: 496511 Color: 4

Bin 582: 2 of cap free
Amount of items: 2
Items: 
Size: 512481 Color: 6
Size: 487518 Color: 9

Bin 583: 2 of cap free
Amount of items: 2
Items: 
Size: 513579 Color: 5
Size: 486420 Color: 3

Bin 584: 2 of cap free
Amount of items: 2
Items: 
Size: 515324 Color: 18
Size: 484675 Color: 19

Bin 585: 2 of cap free
Amount of items: 2
Items: 
Size: 531562 Color: 14
Size: 468437 Color: 4

Bin 586: 2 of cap free
Amount of items: 2
Items: 
Size: 534276 Color: 12
Size: 465723 Color: 9

Bin 587: 2 of cap free
Amount of items: 2
Items: 
Size: 546071 Color: 4
Size: 453928 Color: 12

Bin 588: 2 of cap free
Amount of items: 2
Items: 
Size: 557878 Color: 4
Size: 442121 Color: 18

Bin 589: 2 of cap free
Amount of items: 2
Items: 
Size: 563825 Color: 17
Size: 436174 Color: 7

Bin 590: 2 of cap free
Amount of items: 2
Items: 
Size: 564783 Color: 3
Size: 435216 Color: 18

Bin 591: 2 of cap free
Amount of items: 2
Items: 
Size: 572174 Color: 17
Size: 427825 Color: 7

Bin 592: 2 of cap free
Amount of items: 2
Items: 
Size: 582866 Color: 4
Size: 417133 Color: 5

Bin 593: 2 of cap free
Amount of items: 2
Items: 
Size: 597887 Color: 11
Size: 402112 Color: 6

Bin 594: 2 of cap free
Amount of items: 3
Items: 
Size: 613422 Color: 6
Size: 193931 Color: 3
Size: 192646 Color: 8

Bin 595: 2 of cap free
Amount of items: 2
Items: 
Size: 615133 Color: 19
Size: 384866 Color: 5

Bin 596: 2 of cap free
Amount of items: 3
Items: 
Size: 626380 Color: 2
Size: 187112 Color: 17
Size: 186507 Color: 4

Bin 597: 2 of cap free
Amount of items: 3
Items: 
Size: 629107 Color: 1
Size: 185758 Color: 19
Size: 185134 Color: 10

Bin 598: 2 of cap free
Amount of items: 3
Items: 
Size: 631073 Color: 2
Size: 187185 Color: 11
Size: 181741 Color: 2

Bin 599: 2 of cap free
Amount of items: 3
Items: 
Size: 631715 Color: 2
Size: 184423 Color: 17
Size: 183861 Color: 3

Bin 600: 2 of cap free
Amount of items: 2
Items: 
Size: 642150 Color: 6
Size: 357849 Color: 7

Bin 601: 2 of cap free
Amount of items: 2
Items: 
Size: 642603 Color: 18
Size: 357396 Color: 3

Bin 602: 2 of cap free
Amount of items: 2
Items: 
Size: 653253 Color: 17
Size: 346746 Color: 18

Bin 603: 2 of cap free
Amount of items: 3
Items: 
Size: 653910 Color: 10
Size: 173442 Color: 18
Size: 172647 Color: 0

Bin 604: 2 of cap free
Amount of items: 3
Items: 
Size: 657971 Color: 4
Size: 171202 Color: 7
Size: 170826 Color: 1

Bin 605: 2 of cap free
Amount of items: 3
Items: 
Size: 658880 Color: 5
Size: 170603 Color: 19
Size: 170516 Color: 0

Bin 606: 2 of cap free
Amount of items: 2
Items: 
Size: 670720 Color: 16
Size: 329279 Color: 4

Bin 607: 2 of cap free
Amount of items: 3
Items: 
Size: 671067 Color: 8
Size: 165779 Color: 12
Size: 163153 Color: 7

Bin 608: 2 of cap free
Amount of items: 2
Items: 
Size: 679010 Color: 14
Size: 320989 Color: 1

Bin 609: 2 of cap free
Amount of items: 3
Items: 
Size: 686742 Color: 19
Size: 157265 Color: 18
Size: 155992 Color: 8

Bin 610: 2 of cap free
Amount of items: 2
Items: 
Size: 688942 Color: 13
Size: 311057 Color: 17

Bin 611: 2 of cap free
Amount of items: 3
Items: 
Size: 690909 Color: 0
Size: 154673 Color: 13
Size: 154417 Color: 15

Bin 612: 2 of cap free
Amount of items: 2
Items: 
Size: 695453 Color: 0
Size: 304546 Color: 13

Bin 613: 2 of cap free
Amount of items: 2
Items: 
Size: 697665 Color: 12
Size: 302334 Color: 0

Bin 614: 2 of cap free
Amount of items: 2
Items: 
Size: 700521 Color: 5
Size: 299478 Color: 14

Bin 615: 2 of cap free
Amount of items: 2
Items: 
Size: 706551 Color: 4
Size: 293448 Color: 2

Bin 616: 2 of cap free
Amount of items: 3
Items: 
Size: 712430 Color: 14
Size: 146987 Color: 14
Size: 140582 Color: 11

Bin 617: 2 of cap free
Amount of items: 2
Items: 
Size: 713321 Color: 13
Size: 286678 Color: 7

Bin 618: 2 of cap free
Amount of items: 2
Items: 
Size: 714616 Color: 16
Size: 285383 Color: 5

Bin 619: 2 of cap free
Amount of items: 2
Items: 
Size: 716031 Color: 16
Size: 283968 Color: 8

Bin 620: 2 of cap free
Amount of items: 3
Items: 
Size: 716994 Color: 12
Size: 141587 Color: 16
Size: 141418 Color: 6

Bin 621: 2 of cap free
Amount of items: 2
Items: 
Size: 717398 Color: 3
Size: 282601 Color: 0

Bin 622: 2 of cap free
Amount of items: 2
Items: 
Size: 717590 Color: 19
Size: 282409 Color: 9

Bin 623: 2 of cap free
Amount of items: 3
Items: 
Size: 732351 Color: 11
Size: 134988 Color: 10
Size: 132660 Color: 1

Bin 624: 2 of cap free
Amount of items: 2
Items: 
Size: 748532 Color: 19
Size: 251467 Color: 13

Bin 625: 2 of cap free
Amount of items: 2
Items: 
Size: 749101 Color: 6
Size: 250898 Color: 19

Bin 626: 2 of cap free
Amount of items: 2
Items: 
Size: 755859 Color: 1
Size: 244140 Color: 15

Bin 627: 2 of cap free
Amount of items: 2
Items: 
Size: 766877 Color: 6
Size: 233122 Color: 18

Bin 628: 2 of cap free
Amount of items: 2
Items: 
Size: 768961 Color: 13
Size: 231038 Color: 16

Bin 629: 2 of cap free
Amount of items: 2
Items: 
Size: 779690 Color: 17
Size: 220309 Color: 6

Bin 630: 2 of cap free
Amount of items: 2
Items: 
Size: 786142 Color: 13
Size: 213857 Color: 9

Bin 631: 2 of cap free
Amount of items: 2
Items: 
Size: 793492 Color: 3
Size: 206507 Color: 16

Bin 632: 2 of cap free
Amount of items: 2
Items: 
Size: 799088 Color: 3
Size: 200911 Color: 0

Bin 633: 3 of cap free
Amount of items: 3
Items: 
Size: 725061 Color: 11
Size: 149640 Color: 13
Size: 125297 Color: 4

Bin 634: 3 of cap free
Amount of items: 3
Items: 
Size: 716686 Color: 0
Size: 172063 Color: 5
Size: 111249 Color: 3

Bin 635: 3 of cap free
Amount of items: 3
Items: 
Size: 697461 Color: 2
Size: 151354 Color: 5
Size: 151183 Color: 10

Bin 636: 3 of cap free
Amount of items: 3
Items: 
Size: 773841 Color: 5
Size: 114632 Color: 16
Size: 111525 Color: 7

Bin 637: 3 of cap free
Amount of items: 3
Items: 
Size: 703897 Color: 18
Size: 149680 Color: 10
Size: 146421 Color: 4

Bin 638: 3 of cap free
Amount of items: 3
Items: 
Size: 660371 Color: 3
Size: 169858 Color: 5
Size: 169769 Color: 5

Bin 639: 3 of cap free
Amount of items: 3
Items: 
Size: 674436 Color: 5
Size: 174925 Color: 10
Size: 150637 Color: 4

Bin 640: 3 of cap free
Amount of items: 3
Items: 
Size: 468580 Color: 4
Size: 279277 Color: 0
Size: 252141 Color: 11

Bin 641: 3 of cap free
Amount of items: 3
Items: 
Size: 554988 Color: 3
Size: 249687 Color: 4
Size: 195323 Color: 12

Bin 642: 3 of cap free
Amount of items: 3
Items: 
Size: 589740 Color: 7
Size: 220180 Color: 13
Size: 190078 Color: 11

Bin 643: 3 of cap free
Amount of items: 3
Items: 
Size: 622390 Color: 16
Size: 189060 Color: 1
Size: 188548 Color: 4

Bin 644: 3 of cap free
Amount of items: 3
Items: 
Size: 710483 Color: 19
Size: 144841 Color: 15
Size: 144674 Color: 5

Bin 645: 3 of cap free
Amount of items: 3
Items: 
Size: 605015 Color: 13
Size: 198068 Color: 11
Size: 196915 Color: 7

Bin 646: 3 of cap free
Amount of items: 3
Items: 
Size: 613061 Color: 15
Size: 194455 Color: 15
Size: 192482 Color: 17

Bin 647: 3 of cap free
Amount of items: 3
Items: 
Size: 733908 Color: 17
Size: 134231 Color: 3
Size: 131859 Color: 14

Bin 648: 3 of cap free
Amount of items: 3
Items: 
Size: 747768 Color: 12
Size: 132111 Color: 19
Size: 120119 Color: 9

Bin 649: 3 of cap free
Amount of items: 3
Items: 
Size: 732160 Color: 9
Size: 136650 Color: 7
Size: 131188 Color: 6

Bin 650: 3 of cap free
Amount of items: 3
Items: 
Size: 688498 Color: 18
Size: 158424 Color: 4
Size: 153076 Color: 4

Bin 651: 3 of cap free
Amount of items: 3
Items: 
Size: 712962 Color: 17
Size: 143576 Color: 3
Size: 143460 Color: 17

Bin 652: 3 of cap free
Amount of items: 2
Items: 
Size: 795376 Color: 8
Size: 204622 Color: 1

Bin 653: 3 of cap free
Amount of items: 3
Items: 
Size: 691038 Color: 18
Size: 155738 Color: 4
Size: 153222 Color: 17

Bin 654: 3 of cap free
Amount of items: 3
Items: 
Size: 645903 Color: 18
Size: 243800 Color: 12
Size: 110295 Color: 16

Bin 655: 3 of cap free
Amount of items: 3
Items: 
Size: 729745 Color: 17
Size: 139778 Color: 5
Size: 130475 Color: 7

Bin 656: 3 of cap free
Amount of items: 3
Items: 
Size: 537737 Color: 4
Size: 231161 Color: 10
Size: 231100 Color: 3

Bin 657: 3 of cap free
Amount of items: 3
Items: 
Size: 669357 Color: 11
Size: 167617 Color: 9
Size: 163024 Color: 18

Bin 658: 3 of cap free
Amount of items: 3
Items: 
Size: 699875 Color: 6
Size: 150202 Color: 15
Size: 149921 Color: 4

Bin 659: 3 of cap free
Amount of items: 3
Items: 
Size: 630953 Color: 1
Size: 184912 Color: 5
Size: 184133 Color: 7

Bin 660: 3 of cap free
Amount of items: 2
Items: 
Size: 772501 Color: 2
Size: 227497 Color: 10

Bin 661: 3 of cap free
Amount of items: 2
Items: 
Size: 739781 Color: 16
Size: 260217 Color: 12

Bin 662: 3 of cap free
Amount of items: 3
Items: 
Size: 641435 Color: 12
Size: 179327 Color: 14
Size: 179236 Color: 19

Bin 663: 3 of cap free
Amount of items: 3
Items: 
Size: 775900 Color: 7
Size: 113551 Color: 7
Size: 110547 Color: 12

Bin 664: 3 of cap free
Amount of items: 3
Items: 
Size: 352949 Color: 12
Size: 338267 Color: 9
Size: 308782 Color: 7

Bin 665: 3 of cap free
Amount of items: 3
Items: 
Size: 671976 Color: 1
Size: 169117 Color: 2
Size: 158905 Color: 12

Bin 666: 3 of cap free
Amount of items: 3
Items: 
Size: 554665 Color: 7
Size: 294000 Color: 1
Size: 151333 Color: 14

Bin 667: 3 of cap free
Amount of items: 3
Items: 
Size: 649948 Color: 0
Size: 176484 Color: 12
Size: 173566 Color: 9

Bin 668: 3 of cap free
Amount of items: 3
Items: 
Size: 625704 Color: 0
Size: 189075 Color: 5
Size: 185219 Color: 9

Bin 669: 3 of cap free
Amount of items: 3
Items: 
Size: 363702 Color: 0
Size: 338797 Color: 0
Size: 297499 Color: 4

Bin 670: 3 of cap free
Amount of items: 3
Items: 
Size: 381195 Color: 16
Size: 330057 Color: 3
Size: 288746 Color: 4

Bin 671: 3 of cap free
Amount of items: 3
Items: 
Size: 363717 Color: 3
Size: 337346 Color: 12
Size: 298935 Color: 0

Bin 672: 3 of cap free
Amount of items: 3
Items: 
Size: 678948 Color: 12
Size: 160736 Color: 10
Size: 160314 Color: 15

Bin 673: 3 of cap free
Amount of items: 3
Items: 
Size: 381151 Color: 15
Size: 313593 Color: 1
Size: 305254 Color: 14

Bin 674: 3 of cap free
Amount of items: 3
Items: 
Size: 652926 Color: 19
Size: 173623 Color: 0
Size: 173449 Color: 12

Bin 675: 3 of cap free
Amount of items: 3
Items: 
Size: 458108 Color: 8
Size: 286151 Color: 11
Size: 255739 Color: 15

Bin 676: 3 of cap free
Amount of items: 3
Items: 
Size: 621332 Color: 18
Size: 190374 Color: 10
Size: 188292 Color: 1

Bin 677: 3 of cap free
Amount of items: 3
Items: 
Size: 378013 Color: 11
Size: 337259 Color: 19
Size: 284726 Color: 6

Bin 678: 3 of cap free
Amount of items: 2
Items: 
Size: 745703 Color: 15
Size: 254295 Color: 12

Bin 679: 3 of cap free
Amount of items: 3
Items: 
Size: 506624 Color: 16
Size: 264144 Color: 3
Size: 229230 Color: 12

Bin 680: 3 of cap free
Amount of items: 2
Items: 
Size: 587374 Color: 18
Size: 412624 Color: 4

Bin 681: 3 of cap free
Amount of items: 2
Items: 
Size: 733220 Color: 17
Size: 266778 Color: 14

Bin 682: 3 of cap free
Amount of items: 3
Items: 
Size: 621831 Color: 10
Size: 189891 Color: 15
Size: 188276 Color: 14

Bin 683: 3 of cap free
Amount of items: 2
Items: 
Size: 746960 Color: 5
Size: 253038 Color: 12

Bin 684: 3 of cap free
Amount of items: 3
Items: 
Size: 716774 Color: 3
Size: 142520 Color: 8
Size: 140704 Color: 3

Bin 685: 3 of cap free
Amount of items: 2
Items: 
Size: 732723 Color: 16
Size: 267275 Color: 11

Bin 686: 3 of cap free
Amount of items: 2
Items: 
Size: 651317 Color: 9
Size: 348681 Color: 8

Bin 687: 3 of cap free
Amount of items: 3
Items: 
Size: 388491 Color: 5
Size: 342347 Color: 6
Size: 269160 Color: 3

Bin 688: 3 of cap free
Amount of items: 4
Items: 
Size: 291204 Color: 4
Size: 287969 Color: 1
Size: 282124 Color: 11
Size: 138701 Color: 12

Bin 689: 3 of cap free
Amount of items: 3
Items: 
Size: 430730 Color: 12
Size: 288831 Color: 18
Size: 280437 Color: 15

Bin 690: 3 of cap free
Amount of items: 3
Items: 
Size: 365190 Color: 10
Size: 349220 Color: 1
Size: 285588 Color: 17

Bin 691: 3 of cap free
Amount of items: 3
Items: 
Size: 372922 Color: 4
Size: 337792 Color: 15
Size: 289284 Color: 17

Bin 692: 3 of cap free
Amount of items: 3
Items: 
Size: 458977 Color: 2
Size: 272202 Color: 1
Size: 268819 Color: 12

Bin 693: 3 of cap free
Amount of items: 2
Items: 
Size: 507021 Color: 12
Size: 492977 Color: 16

Bin 694: 3 of cap free
Amount of items: 2
Items: 
Size: 516773 Color: 18
Size: 483225 Color: 7

Bin 695: 3 of cap free
Amount of items: 2
Items: 
Size: 535117 Color: 5
Size: 464881 Color: 14

Bin 696: 3 of cap free
Amount of items: 2
Items: 
Size: 537071 Color: 2
Size: 462927 Color: 13

Bin 697: 3 of cap free
Amount of items: 2
Items: 
Size: 557328 Color: 2
Size: 442670 Color: 12

Bin 698: 3 of cap free
Amount of items: 2
Items: 
Size: 582392 Color: 16
Size: 417606 Color: 5

Bin 699: 3 of cap free
Amount of items: 2
Items: 
Size: 583462 Color: 3
Size: 416536 Color: 7

Bin 700: 3 of cap free
Amount of items: 2
Items: 
Size: 584943 Color: 5
Size: 415055 Color: 19

Bin 701: 3 of cap free
Amount of items: 2
Items: 
Size: 594068 Color: 9
Size: 405930 Color: 11

Bin 702: 3 of cap free
Amount of items: 2
Items: 
Size: 601149 Color: 3
Size: 398849 Color: 14

Bin 703: 3 of cap free
Amount of items: 3
Items: 
Size: 607049 Color: 9
Size: 197855 Color: 18
Size: 195094 Color: 12

Bin 704: 3 of cap free
Amount of items: 3
Items: 
Size: 614917 Color: 11
Size: 192648 Color: 6
Size: 192433 Color: 17

Bin 705: 3 of cap free
Amount of items: 2
Items: 
Size: 634110 Color: 10
Size: 365888 Color: 5

Bin 706: 3 of cap free
Amount of items: 2
Items: 
Size: 640976 Color: 12
Size: 359022 Color: 11

Bin 707: 3 of cap free
Amount of items: 2
Items: 
Size: 642048 Color: 6
Size: 357950 Color: 13

Bin 708: 3 of cap free
Amount of items: 3
Items: 
Size: 646147 Color: 15
Size: 176997 Color: 19
Size: 176854 Color: 13

Bin 709: 3 of cap free
Amount of items: 3
Items: 
Size: 650095 Color: 0
Size: 175023 Color: 3
Size: 174880 Color: 6

Bin 710: 3 of cap free
Amount of items: 3
Items: 
Size: 658887 Color: 5
Size: 170833 Color: 5
Size: 170278 Color: 19

Bin 711: 3 of cap free
Amount of items: 2
Items: 
Size: 668889 Color: 19
Size: 331109 Color: 4

Bin 712: 3 of cap free
Amount of items: 3
Items: 
Size: 671355 Color: 15
Size: 164759 Color: 4
Size: 163884 Color: 10

Bin 713: 3 of cap free
Amount of items: 2
Items: 
Size: 679122 Color: 5
Size: 320876 Color: 10

Bin 714: 3 of cap free
Amount of items: 2
Items: 
Size: 682925 Color: 13
Size: 317073 Color: 3

Bin 715: 3 of cap free
Amount of items: 3
Items: 
Size: 687463 Color: 12
Size: 156326 Color: 8
Size: 156209 Color: 13

Bin 716: 3 of cap free
Amount of items: 2
Items: 
Size: 693940 Color: 3
Size: 306058 Color: 18

Bin 717: 3 of cap free
Amount of items: 2
Items: 
Size: 697336 Color: 14
Size: 302662 Color: 0

Bin 718: 3 of cap free
Amount of items: 2
Items: 
Size: 703396 Color: 7
Size: 296602 Color: 4

Bin 719: 3 of cap free
Amount of items: 3
Items: 
Size: 709104 Color: 14
Size: 145496 Color: 17
Size: 145398 Color: 1

Bin 720: 3 of cap free
Amount of items: 2
Items: 
Size: 723610 Color: 5
Size: 276388 Color: 8

Bin 721: 3 of cap free
Amount of items: 2
Items: 
Size: 747779 Color: 10
Size: 252219 Color: 6

Bin 722: 3 of cap free
Amount of items: 2
Items: 
Size: 768070 Color: 7
Size: 231928 Color: 14

Bin 723: 3 of cap free
Amount of items: 2
Items: 
Size: 768832 Color: 12
Size: 231166 Color: 6

Bin 724: 3 of cap free
Amount of items: 2
Items: 
Size: 772022 Color: 4
Size: 227976 Color: 7

Bin 725: 3 of cap free
Amount of items: 2
Items: 
Size: 782113 Color: 5
Size: 217885 Color: 0

Bin 726: 3 of cap free
Amount of items: 2
Items: 
Size: 792757 Color: 9
Size: 207241 Color: 19

Bin 727: 3 of cap free
Amount of items: 2
Items: 
Size: 795127 Color: 4
Size: 204871 Color: 16

Bin 728: 3 of cap free
Amount of items: 2
Items: 
Size: 798126 Color: 5
Size: 201872 Color: 3

Bin 729: 4 of cap free
Amount of items: 3
Items: 
Size: 649743 Color: 13
Size: 175274 Color: 2
Size: 174980 Color: 17

Bin 730: 4 of cap free
Amount of items: 3
Items: 
Size: 386949 Color: 10
Size: 335307 Color: 13
Size: 277741 Color: 3

Bin 731: 4 of cap free
Amount of items: 3
Items: 
Size: 374167 Color: 12
Size: 317368 Color: 15
Size: 308462 Color: 19

Bin 732: 4 of cap free
Amount of items: 2
Items: 
Size: 509847 Color: 9
Size: 490150 Color: 8

Bin 733: 4 of cap free
Amount of items: 3
Items: 
Size: 590038 Color: 1
Size: 207879 Color: 3
Size: 202080 Color: 11

Bin 734: 4 of cap free
Amount of items: 3
Items: 
Size: 644511 Color: 8
Size: 177796 Color: 8
Size: 177690 Color: 13

Bin 735: 4 of cap free
Amount of items: 3
Items: 
Size: 614063 Color: 13
Size: 194029 Color: 4
Size: 191905 Color: 5

Bin 736: 4 of cap free
Amount of items: 3
Items: 
Size: 746818 Color: 8
Size: 126887 Color: 16
Size: 126292 Color: 7

Bin 737: 4 of cap free
Amount of items: 3
Items: 
Size: 374813 Color: 12
Size: 319917 Color: 10
Size: 305267 Color: 12

Bin 738: 4 of cap free
Amount of items: 3
Items: 
Size: 620120 Color: 17
Size: 190611 Color: 9
Size: 189266 Color: 14

Bin 739: 4 of cap free
Amount of items: 3
Items: 
Size: 746906 Color: 16
Size: 126588 Color: 2
Size: 126503 Color: 1

Bin 740: 4 of cap free
Amount of items: 3
Items: 
Size: 685365 Color: 6
Size: 159618 Color: 11
Size: 155014 Color: 1

Bin 741: 4 of cap free
Amount of items: 3
Items: 
Size: 754555 Color: 18
Size: 123293 Color: 18
Size: 122149 Color: 4

Bin 742: 4 of cap free
Amount of items: 3
Items: 
Size: 637518 Color: 15
Size: 183800 Color: 16
Size: 178679 Color: 10

Bin 743: 4 of cap free
Amount of items: 3
Items: 
Size: 693855 Color: 2
Size: 153207 Color: 6
Size: 152935 Color: 0

Bin 744: 4 of cap free
Amount of items: 3
Items: 
Size: 716214 Color: 0
Size: 150795 Color: 10
Size: 132988 Color: 17

Bin 745: 4 of cap free
Amount of items: 3
Items: 
Size: 708376 Color: 19
Size: 146198 Color: 17
Size: 145423 Color: 4

Bin 746: 4 of cap free
Amount of items: 3
Items: 
Size: 586882 Color: 15
Size: 207719 Color: 7
Size: 205396 Color: 18

Bin 747: 4 of cap free
Amount of items: 3
Items: 
Size: 691100 Color: 7
Size: 154689 Color: 10
Size: 154208 Color: 18

Bin 748: 4 of cap free
Amount of items: 2
Items: 
Size: 585953 Color: 3
Size: 414044 Color: 8

Bin 749: 4 of cap free
Amount of items: 3
Items: 
Size: 719620 Color: 15
Size: 140430 Color: 0
Size: 139947 Color: 9

Bin 750: 4 of cap free
Amount of items: 3
Items: 
Size: 574948 Color: 14
Size: 279969 Color: 6
Size: 145080 Color: 6

Bin 751: 4 of cap free
Amount of items: 2
Items: 
Size: 700451 Color: 11
Size: 299546 Color: 10

Bin 752: 4 of cap free
Amount of items: 3
Items: 
Size: 685414 Color: 16
Size: 160726 Color: 3
Size: 153857 Color: 16

Bin 753: 4 of cap free
Amount of items: 3
Items: 
Size: 602268 Color: 1
Size: 199147 Color: 0
Size: 198582 Color: 19

Bin 754: 4 of cap free
Amount of items: 3
Items: 
Size: 506499 Color: 12
Size: 357104 Color: 3
Size: 136394 Color: 11

Bin 755: 4 of cap free
Amount of items: 3
Items: 
Size: 556687 Color: 13
Size: 226261 Color: 2
Size: 217049 Color: 14

Bin 756: 4 of cap free
Amount of items: 3
Items: 
Size: 482816 Color: 17
Size: 266949 Color: 9
Size: 250232 Color: 14

Bin 757: 4 of cap free
Amount of items: 3
Items: 
Size: 489579 Color: 12
Size: 263651 Color: 17
Size: 246767 Color: 0

Bin 758: 4 of cap free
Amount of items: 3
Items: 
Size: 559818 Color: 12
Size: 222083 Color: 8
Size: 218096 Color: 11

Bin 759: 4 of cap free
Amount of items: 2
Items: 
Size: 796604 Color: 6
Size: 203393 Color: 11

Bin 760: 4 of cap free
Amount of items: 2
Items: 
Size: 663239 Color: 3
Size: 336758 Color: 11

Bin 761: 4 of cap free
Amount of items: 2
Items: 
Size: 771138 Color: 0
Size: 228859 Color: 11

Bin 762: 4 of cap free
Amount of items: 2
Items: 
Size: 797850 Color: 9
Size: 202147 Color: 11

Bin 763: 4 of cap free
Amount of items: 2
Items: 
Size: 672962 Color: 5
Size: 327035 Color: 7

Bin 764: 4 of cap free
Amount of items: 3
Items: 
Size: 469239 Color: 13
Size: 279087 Color: 12
Size: 251671 Color: 18

Bin 765: 4 of cap free
Amount of items: 3
Items: 
Size: 363387 Color: 19
Size: 350403 Color: 11
Size: 286207 Color: 7

Bin 766: 4 of cap free
Amount of items: 3
Items: 
Size: 367767 Color: 8
Size: 345809 Color: 9
Size: 286421 Color: 3

Bin 767: 4 of cap free
Amount of items: 3
Items: 
Size: 369902 Color: 11
Size: 345268 Color: 14
Size: 284827 Color: 18

Bin 768: 4 of cap free
Amount of items: 3
Items: 
Size: 386621 Color: 8
Size: 318239 Color: 1
Size: 295137 Color: 11

Bin 769: 4 of cap free
Amount of items: 3
Items: 
Size: 458006 Color: 8
Size: 273801 Color: 6
Size: 268190 Color: 16

Bin 770: 4 of cap free
Amount of items: 3
Items: 
Size: 458084 Color: 8
Size: 273258 Color: 10
Size: 268655 Color: 9

Bin 771: 4 of cap free
Amount of items: 2
Items: 
Size: 519767 Color: 19
Size: 480230 Color: 4

Bin 772: 4 of cap free
Amount of items: 2
Items: 
Size: 521352 Color: 18
Size: 478645 Color: 6

Bin 773: 4 of cap free
Amount of items: 2
Items: 
Size: 546823 Color: 2
Size: 453174 Color: 0

Bin 774: 4 of cap free
Amount of items: 2
Items: 
Size: 550305 Color: 1
Size: 449692 Color: 15

Bin 775: 4 of cap free
Amount of items: 2
Items: 
Size: 554079 Color: 18
Size: 445918 Color: 1

Bin 776: 4 of cap free
Amount of items: 2
Items: 
Size: 569165 Color: 17
Size: 430832 Color: 5

Bin 777: 4 of cap free
Amount of items: 2
Items: 
Size: 576841 Color: 0
Size: 423156 Color: 13

Bin 778: 4 of cap free
Amount of items: 2
Items: 
Size: 583882 Color: 6
Size: 416115 Color: 18

Bin 779: 4 of cap free
Amount of items: 3
Items: 
Size: 584020 Color: 3
Size: 215068 Color: 3
Size: 200909 Color: 16

Bin 780: 4 of cap free
Amount of items: 2
Items: 
Size: 585748 Color: 0
Size: 414249 Color: 8

Bin 781: 4 of cap free
Amount of items: 2
Items: 
Size: 594913 Color: 7
Size: 405084 Color: 11

Bin 782: 4 of cap free
Amount of items: 2
Items: 
Size: 608893 Color: 5
Size: 391104 Color: 7

Bin 783: 4 of cap free
Amount of items: 3
Items: 
Size: 609040 Color: 9
Size: 195914 Color: 12
Size: 195043 Color: 18

Bin 784: 4 of cap free
Amount of items: 2
Items: 
Size: 614676 Color: 2
Size: 385321 Color: 5

Bin 785: 4 of cap free
Amount of items: 2
Items: 
Size: 616082 Color: 9
Size: 383915 Color: 17

Bin 786: 4 of cap free
Amount of items: 3
Items: 
Size: 627448 Color: 11
Size: 187100 Color: 8
Size: 185449 Color: 2

Bin 787: 4 of cap free
Amount of items: 3
Items: 
Size: 629359 Color: 7
Size: 186926 Color: 0
Size: 183712 Color: 4

Bin 788: 4 of cap free
Amount of items: 3
Items: 
Size: 632946 Color: 0
Size: 185194 Color: 19
Size: 181857 Color: 6

Bin 789: 4 of cap free
Amount of items: 2
Items: 
Size: 640652 Color: 14
Size: 359345 Color: 17

Bin 790: 4 of cap free
Amount of items: 2
Items: 
Size: 642880 Color: 3
Size: 357117 Color: 9

Bin 791: 4 of cap free
Amount of items: 2
Items: 
Size: 643395 Color: 0
Size: 356602 Color: 11

Bin 792: 4 of cap free
Amount of items: 2
Items: 
Size: 644909 Color: 9
Size: 355088 Color: 4

Bin 793: 4 of cap free
Amount of items: 2
Items: 
Size: 654495 Color: 16
Size: 345502 Color: 19

Bin 794: 4 of cap free
Amount of items: 2
Items: 
Size: 657050 Color: 6
Size: 342947 Color: 14

Bin 795: 4 of cap free
Amount of items: 3
Items: 
Size: 658144 Color: 0
Size: 171829 Color: 3
Size: 170024 Color: 13

Bin 796: 4 of cap free
Amount of items: 2
Items: 
Size: 665201 Color: 3
Size: 334796 Color: 4

Bin 797: 4 of cap free
Amount of items: 2
Items: 
Size: 670807 Color: 15
Size: 329190 Color: 4

Bin 798: 4 of cap free
Amount of items: 2
Items: 
Size: 676853 Color: 15
Size: 323144 Color: 8

Bin 799: 4 of cap free
Amount of items: 2
Items: 
Size: 692101 Color: 10
Size: 307896 Color: 1

Bin 800: 4 of cap free
Amount of items: 2
Items: 
Size: 693977 Color: 16
Size: 306020 Color: 19

Bin 801: 4 of cap free
Amount of items: 3
Items: 
Size: 698348 Color: 2
Size: 150898 Color: 10
Size: 150751 Color: 14

Bin 802: 4 of cap free
Amount of items: 2
Items: 
Size: 698405 Color: 10
Size: 301592 Color: 13

Bin 803: 4 of cap free
Amount of items: 2
Items: 
Size: 698764 Color: 16
Size: 301233 Color: 19

Bin 804: 4 of cap free
Amount of items: 2
Items: 
Size: 700710 Color: 6
Size: 299287 Color: 9

Bin 805: 4 of cap free
Amount of items: 2
Items: 
Size: 703676 Color: 16
Size: 296321 Color: 0

Bin 806: 4 of cap free
Amount of items: 3
Items: 
Size: 708234 Color: 8
Size: 146118 Color: 7
Size: 145645 Color: 2

Bin 807: 4 of cap free
Amount of items: 2
Items: 
Size: 713250 Color: 2
Size: 286747 Color: 12

Bin 808: 4 of cap free
Amount of items: 2
Items: 
Size: 738595 Color: 3
Size: 261402 Color: 7

Bin 809: 4 of cap free
Amount of items: 2
Items: 
Size: 739502 Color: 16
Size: 260495 Color: 17

Bin 810: 4 of cap free
Amount of items: 2
Items: 
Size: 744709 Color: 13
Size: 255288 Color: 15

Bin 811: 4 of cap free
Amount of items: 2
Items: 
Size: 745417 Color: 0
Size: 254580 Color: 2

Bin 812: 4 of cap free
Amount of items: 2
Items: 
Size: 753911 Color: 2
Size: 246086 Color: 16

Bin 813: 4 of cap free
Amount of items: 2
Items: 
Size: 755548 Color: 6
Size: 244449 Color: 13

Bin 814: 4 of cap free
Amount of items: 2
Items: 
Size: 761824 Color: 4
Size: 238173 Color: 13

Bin 815: 4 of cap free
Amount of items: 2
Items: 
Size: 763002 Color: 9
Size: 236995 Color: 1

Bin 816: 4 of cap free
Amount of items: 2
Items: 
Size: 766930 Color: 0
Size: 233067 Color: 19

Bin 817: 4 of cap free
Amount of items: 2
Items: 
Size: 778851 Color: 10
Size: 221146 Color: 17

Bin 818: 4 of cap free
Amount of items: 2
Items: 
Size: 784109 Color: 6
Size: 215888 Color: 4

Bin 819: 4 of cap free
Amount of items: 2
Items: 
Size: 789093 Color: 7
Size: 210904 Color: 17

Bin 820: 4 of cap free
Amount of items: 2
Items: 
Size: 794676 Color: 4
Size: 205321 Color: 19

Bin 821: 4 of cap free
Amount of items: 2
Items: 
Size: 795339 Color: 7
Size: 204658 Color: 9

Bin 822: 4 of cap free
Amount of items: 2
Items: 
Size: 796485 Color: 19
Size: 203512 Color: 8

Bin 823: 5 of cap free
Amount of items: 3
Items: 
Size: 758690 Color: 15
Size: 139610 Color: 19
Size: 101696 Color: 3

Bin 824: 5 of cap free
Amount of items: 2
Items: 
Size: 683598 Color: 11
Size: 316398 Color: 12

Bin 825: 5 of cap free
Amount of items: 3
Items: 
Size: 658828 Color: 11
Size: 171981 Color: 14
Size: 169187 Color: 6

Bin 826: 5 of cap free
Amount of items: 3
Items: 
Size: 668171 Color: 1
Size: 170430 Color: 1
Size: 161395 Color: 18

Bin 827: 5 of cap free
Amount of items: 3
Items: 
Size: 701979 Color: 9
Size: 154722 Color: 11
Size: 143295 Color: 19

Bin 828: 5 of cap free
Amount of items: 3
Items: 
Size: 715328 Color: 5
Size: 142338 Color: 6
Size: 142330 Color: 16

Bin 829: 5 of cap free
Amount of items: 3
Items: 
Size: 405253 Color: 4
Size: 307314 Color: 12
Size: 287429 Color: 11

Bin 830: 5 of cap free
Amount of items: 3
Items: 
Size: 608195 Color: 1
Size: 197001 Color: 11
Size: 194800 Color: 2

Bin 831: 5 of cap free
Amount of items: 2
Items: 
Size: 729262 Color: 18
Size: 270734 Color: 1

Bin 832: 5 of cap free
Amount of items: 3
Items: 
Size: 372894 Color: 17
Size: 338337 Color: 10
Size: 288765 Color: 17

Bin 833: 5 of cap free
Amount of items: 3
Items: 
Size: 719214 Color: 7
Size: 140474 Color: 2
Size: 140308 Color: 3

Bin 834: 5 of cap free
Amount of items: 3
Items: 
Size: 717558 Color: 14
Size: 141848 Color: 10
Size: 140590 Color: 8

Bin 835: 5 of cap free
Amount of items: 2
Items: 
Size: 562543 Color: 16
Size: 437453 Color: 19

Bin 836: 5 of cap free
Amount of items: 3
Items: 
Size: 668945 Color: 10
Size: 166123 Color: 8
Size: 164928 Color: 7

Bin 837: 5 of cap free
Amount of items: 3
Items: 
Size: 458159 Color: 18
Size: 273761 Color: 9
Size: 268076 Color: 15

Bin 838: 5 of cap free
Amount of items: 2
Items: 
Size: 755107 Color: 14
Size: 244889 Color: 15

Bin 839: 5 of cap free
Amount of items: 3
Items: 
Size: 606316 Color: 16
Size: 196884 Color: 10
Size: 196796 Color: 8

Bin 840: 5 of cap free
Amount of items: 3
Items: 
Size: 585733 Color: 13
Size: 214908 Color: 18
Size: 199355 Color: 11

Bin 841: 5 of cap free
Amount of items: 3
Items: 
Size: 681916 Color: 19
Size: 159151 Color: 11
Size: 158929 Color: 3

Bin 842: 5 of cap free
Amount of items: 2
Items: 
Size: 767396 Color: 17
Size: 232600 Color: 0

Bin 843: 5 of cap free
Amount of items: 2
Items: 
Size: 723134 Color: 10
Size: 276862 Color: 0

Bin 844: 5 of cap free
Amount of items: 3
Items: 
Size: 661098 Color: 17
Size: 169754 Color: 1
Size: 169144 Color: 7

Bin 845: 5 of cap free
Amount of items: 3
Items: 
Size: 732622 Color: 14
Size: 136472 Color: 18
Size: 130902 Color: 0

Bin 846: 5 of cap free
Amount of items: 3
Items: 
Size: 380891 Color: 14
Size: 326093 Color: 5
Size: 293012 Color: 17

Bin 847: 5 of cap free
Amount of items: 3
Items: 
Size: 728480 Color: 13
Size: 138901 Color: 15
Size: 132615 Color: 18

Bin 848: 5 of cap free
Amount of items: 3
Items: 
Size: 551915 Color: 10
Size: 269787 Color: 8
Size: 178294 Color: 9

Bin 849: 5 of cap free
Amount of items: 2
Items: 
Size: 700398 Color: 2
Size: 299598 Color: 11

Bin 850: 5 of cap free
Amount of items: 2
Items: 
Size: 757228 Color: 3
Size: 242768 Color: 18

Bin 851: 5 of cap free
Amount of items: 2
Items: 
Size: 606794 Color: 1
Size: 393202 Color: 10

Bin 852: 5 of cap free
Amount of items: 3
Items: 
Size: 363000 Color: 7
Size: 346120 Color: 3
Size: 290876 Color: 15

Bin 853: 5 of cap free
Amount of items: 2
Items: 
Size: 787105 Color: 6
Size: 212891 Color: 18

Bin 854: 5 of cap free
Amount of items: 3
Items: 
Size: 621387 Color: 6
Size: 197060 Color: 1
Size: 181549 Color: 3

Bin 855: 5 of cap free
Amount of items: 3
Items: 
Size: 471120 Color: 11
Size: 265546 Color: 5
Size: 263330 Color: 0

Bin 856: 5 of cap free
Amount of items: 2
Items: 
Size: 686732 Color: 12
Size: 313264 Color: 14

Bin 857: 5 of cap free
Amount of items: 3
Items: 
Size: 369793 Color: 12
Size: 322765 Color: 3
Size: 307438 Color: 9

Bin 858: 5 of cap free
Amount of items: 3
Items: 
Size: 668998 Color: 18
Size: 167198 Color: 11
Size: 163800 Color: 5

Bin 859: 5 of cap free
Amount of items: 3
Items: 
Size: 688173 Color: 18
Size: 155998 Color: 8
Size: 155825 Color: 10

Bin 860: 5 of cap free
Amount of items: 3
Items: 
Size: 648889 Color: 6
Size: 175761 Color: 12
Size: 175346 Color: 1

Bin 861: 5 of cap free
Amount of items: 3
Items: 
Size: 705706 Color: 4
Size: 147348 Color: 19
Size: 146942 Color: 15

Bin 862: 5 of cap free
Amount of items: 3
Items: 
Size: 638053 Color: 1
Size: 190503 Color: 13
Size: 171440 Color: 5

Bin 863: 5 of cap free
Amount of items: 3
Items: 
Size: 477143 Color: 13
Size: 272853 Color: 19
Size: 250000 Color: 1

Bin 864: 5 of cap free
Amount of items: 2
Items: 
Size: 506415 Color: 3
Size: 493581 Color: 10

Bin 865: 5 of cap free
Amount of items: 3
Items: 
Size: 610326 Color: 6
Size: 264765 Color: 15
Size: 124905 Color: 6

Bin 866: 5 of cap free
Amount of items: 2
Items: 
Size: 605208 Color: 19
Size: 394788 Color: 15

Bin 867: 5 of cap free
Amount of items: 3
Items: 
Size: 369929 Color: 18
Size: 344210 Color: 11
Size: 285857 Color: 15

Bin 868: 5 of cap free
Amount of items: 3
Items: 
Size: 370019 Color: 17
Size: 346186 Color: 19
Size: 283791 Color: 18

Bin 869: 5 of cap free
Amount of items: 3
Items: 
Size: 368949 Color: 15
Size: 344932 Color: 17
Size: 286115 Color: 16

Bin 870: 5 of cap free
Amount of items: 3
Items: 
Size: 381142 Color: 0
Size: 318254 Color: 9
Size: 300600 Color: 4

Bin 871: 5 of cap free
Amount of items: 3
Items: 
Size: 455538 Color: 6
Size: 272846 Color: 7
Size: 271612 Color: 1

Bin 872: 5 of cap free
Amount of items: 2
Items: 
Size: 537743 Color: 0
Size: 462253 Color: 11

Bin 873: 5 of cap free
Amount of items: 2
Items: 
Size: 544232 Color: 18
Size: 455764 Color: 1

Bin 874: 5 of cap free
Amount of items: 2
Items: 
Size: 551006 Color: 1
Size: 448990 Color: 5

Bin 875: 5 of cap free
Amount of items: 2
Items: 
Size: 551345 Color: 15
Size: 448651 Color: 17

Bin 876: 5 of cap free
Amount of items: 2
Items: 
Size: 552306 Color: 15
Size: 447690 Color: 8

Bin 877: 5 of cap free
Amount of items: 2
Items: 
Size: 554397 Color: 2
Size: 445599 Color: 18

Bin 878: 5 of cap free
Amount of items: 2
Items: 
Size: 562003 Color: 12
Size: 437993 Color: 13

Bin 879: 5 of cap free
Amount of items: 2
Items: 
Size: 573094 Color: 5
Size: 426902 Color: 0

Bin 880: 5 of cap free
Amount of items: 2
Items: 
Size: 592287 Color: 19
Size: 407709 Color: 15

Bin 881: 5 of cap free
Amount of items: 2
Items: 
Size: 597061 Color: 7
Size: 402935 Color: 14

Bin 882: 5 of cap free
Amount of items: 3
Items: 
Size: 606117 Color: 2
Size: 198660 Color: 7
Size: 195219 Color: 6

Bin 883: 5 of cap free
Amount of items: 2
Items: 
Size: 607954 Color: 14
Size: 392042 Color: 7

Bin 884: 5 of cap free
Amount of items: 3
Items: 
Size: 609011 Color: 1
Size: 196603 Color: 18
Size: 194382 Color: 19

Bin 885: 5 of cap free
Amount of items: 2
Items: 
Size: 618774 Color: 15
Size: 381222 Color: 16

Bin 886: 5 of cap free
Amount of items: 2
Items: 
Size: 623460 Color: 5
Size: 376536 Color: 15

Bin 887: 5 of cap free
Amount of items: 2
Items: 
Size: 623457 Color: 1
Size: 376539 Color: 12

Bin 888: 5 of cap free
Amount of items: 2
Items: 
Size: 629796 Color: 16
Size: 370200 Color: 0

Bin 889: 5 of cap free
Amount of items: 3
Items: 
Size: 633591 Color: 18
Size: 183447 Color: 0
Size: 182958 Color: 14

Bin 890: 5 of cap free
Amount of items: 2
Items: 
Size: 642865 Color: 10
Size: 357131 Color: 6

Bin 891: 5 of cap free
Amount of items: 3
Items: 
Size: 647986 Color: 19
Size: 176140 Color: 6
Size: 175870 Color: 17

Bin 892: 5 of cap free
Amount of items: 2
Items: 
Size: 659383 Color: 14
Size: 340613 Color: 10

Bin 893: 5 of cap free
Amount of items: 3
Items: 
Size: 661199 Color: 11
Size: 170270 Color: 6
Size: 168527 Color: 2

Bin 894: 5 of cap free
Amount of items: 2
Items: 
Size: 684171 Color: 17
Size: 315825 Color: 3

Bin 895: 5 of cap free
Amount of items: 2
Items: 
Size: 685248 Color: 13
Size: 314748 Color: 4

Bin 896: 5 of cap free
Amount of items: 2
Items: 
Size: 740040 Color: 17
Size: 259956 Color: 12

Bin 897: 5 of cap free
Amount of items: 2
Items: 
Size: 775950 Color: 2
Size: 224046 Color: 5

Bin 898: 5 of cap free
Amount of items: 2
Items: 
Size: 794909 Color: 15
Size: 205087 Color: 17

Bin 899: 6 of cap free
Amount of items: 3
Items: 
Size: 503135 Color: 8
Size: 250144 Color: 10
Size: 246716 Color: 1

Bin 900: 6 of cap free
Amount of items: 3
Items: 
Size: 671993 Color: 6
Size: 169231 Color: 1
Size: 158771 Color: 0

Bin 901: 6 of cap free
Amount of items: 3
Items: 
Size: 488176 Color: 4
Size: 255910 Color: 11
Size: 255909 Color: 8

Bin 902: 6 of cap free
Amount of items: 2
Items: 
Size: 582162 Color: 7
Size: 417833 Color: 1

Bin 903: 6 of cap free
Amount of items: 3
Items: 
Size: 651251 Color: 6
Size: 174413 Color: 13
Size: 174331 Color: 11

Bin 904: 6 of cap free
Amount of items: 3
Items: 
Size: 763378 Color: 13
Size: 126688 Color: 8
Size: 109929 Color: 16

Bin 905: 6 of cap free
Amount of items: 3
Items: 
Size: 784961 Color: 15
Size: 110843 Color: 12
Size: 104191 Color: 2

Bin 906: 6 of cap free
Amount of items: 3
Items: 
Size: 644463 Color: 17
Size: 193155 Color: 0
Size: 162377 Color: 2

Bin 907: 6 of cap free
Amount of items: 3
Items: 
Size: 635169 Color: 17
Size: 183194 Color: 5
Size: 181632 Color: 8

Bin 908: 6 of cap free
Amount of items: 3
Items: 
Size: 613430 Color: 12
Size: 264994 Color: 2
Size: 121571 Color: 1

Bin 909: 6 of cap free
Amount of items: 3
Items: 
Size: 569630 Color: 9
Size: 215190 Color: 13
Size: 215175 Color: 5

Bin 910: 6 of cap free
Amount of items: 3
Items: 
Size: 675984 Color: 9
Size: 164090 Color: 9
Size: 159921 Color: 18

Bin 911: 6 of cap free
Amount of items: 3
Items: 
Size: 490633 Color: 17
Size: 264738 Color: 17
Size: 244624 Color: 12

Bin 912: 6 of cap free
Amount of items: 2
Items: 
Size: 687511 Color: 3
Size: 312484 Color: 17

Bin 913: 6 of cap free
Amount of items: 3
Items: 
Size: 397124 Color: 9
Size: 304156 Color: 1
Size: 298715 Color: 8

Bin 914: 6 of cap free
Amount of items: 3
Items: 
Size: 367775 Color: 9
Size: 321901 Color: 9
Size: 310319 Color: 12

Bin 915: 6 of cap free
Amount of items: 2
Items: 
Size: 700520 Color: 18
Size: 299475 Color: 19

Bin 916: 6 of cap free
Amount of items: 2
Items: 
Size: 754925 Color: 0
Size: 245070 Color: 17

Bin 917: 6 of cap free
Amount of items: 2
Items: 
Size: 742144 Color: 4
Size: 257851 Color: 17

Bin 918: 6 of cap free
Amount of items: 2
Items: 
Size: 763638 Color: 18
Size: 236357 Color: 1

Bin 919: 6 of cap free
Amount of items: 3
Items: 
Size: 652521 Color: 3
Size: 174186 Color: 14
Size: 173288 Color: 4

Bin 920: 6 of cap free
Amount of items: 3
Items: 
Size: 656295 Color: 6
Size: 172243 Color: 12
Size: 171457 Color: 8

Bin 921: 6 of cap free
Amount of items: 2
Items: 
Size: 684570 Color: 2
Size: 315425 Color: 19

Bin 922: 6 of cap free
Amount of items: 3
Items: 
Size: 628323 Color: 15
Size: 189444 Color: 19
Size: 182228 Color: 9

Bin 923: 6 of cap free
Amount of items: 3
Items: 
Size: 369059 Color: 2
Size: 333660 Color: 10
Size: 297276 Color: 1

Bin 924: 6 of cap free
Amount of items: 3
Items: 
Size: 455441 Color: 4
Size: 276494 Color: 7
Size: 268060 Color: 11

Bin 925: 6 of cap free
Amount of items: 2
Items: 
Size: 500227 Color: 10
Size: 499768 Color: 4

Bin 926: 6 of cap free
Amount of items: 3
Items: 
Size: 349451 Color: 12
Size: 333454 Color: 8
Size: 317090 Color: 14

Bin 927: 6 of cap free
Amount of items: 2
Items: 
Size: 570172 Color: 19
Size: 429823 Color: 12

Bin 928: 6 of cap free
Amount of items: 3
Items: 
Size: 398298 Color: 14
Size: 306851 Color: 17
Size: 294846 Color: 13

Bin 929: 6 of cap free
Amount of items: 3
Items: 
Size: 361233 Color: 9
Size: 350602 Color: 15
Size: 288160 Color: 11

Bin 930: 6 of cap free
Amount of items: 3
Items: 
Size: 482854 Color: 13
Size: 265609 Color: 13
Size: 251532 Color: 16

Bin 931: 6 of cap free
Amount of items: 2
Items: 
Size: 501519 Color: 5
Size: 498476 Color: 14

Bin 932: 6 of cap free
Amount of items: 2
Items: 
Size: 514426 Color: 2
Size: 485569 Color: 7

Bin 933: 6 of cap free
Amount of items: 2
Items: 
Size: 521411 Color: 5
Size: 478584 Color: 3

Bin 934: 6 of cap free
Amount of items: 2
Items: 
Size: 525558 Color: 19
Size: 474437 Color: 17

Bin 935: 6 of cap free
Amount of items: 2
Items: 
Size: 533983 Color: 13
Size: 466012 Color: 12

Bin 936: 6 of cap free
Amount of items: 2
Items: 
Size: 534804 Color: 11
Size: 465191 Color: 0

Bin 937: 6 of cap free
Amount of items: 2
Items: 
Size: 540413 Color: 16
Size: 459582 Color: 2

Bin 938: 6 of cap free
Amount of items: 2
Items: 
Size: 542626 Color: 5
Size: 457369 Color: 12

Bin 939: 6 of cap free
Amount of items: 2
Items: 
Size: 553534 Color: 11
Size: 446461 Color: 16

Bin 940: 6 of cap free
Amount of items: 2
Items: 
Size: 558524 Color: 16
Size: 441471 Color: 13

Bin 941: 6 of cap free
Amount of items: 2
Items: 
Size: 559209 Color: 13
Size: 440786 Color: 4

Bin 942: 6 of cap free
Amount of items: 2
Items: 
Size: 567224 Color: 16
Size: 432771 Color: 0

Bin 943: 6 of cap free
Amount of items: 2
Items: 
Size: 568547 Color: 19
Size: 431448 Color: 16

Bin 944: 6 of cap free
Amount of items: 2
Items: 
Size: 570984 Color: 7
Size: 429011 Color: 1

Bin 945: 6 of cap free
Amount of items: 2
Items: 
Size: 606263 Color: 4
Size: 393732 Color: 13

Bin 946: 6 of cap free
Amount of items: 2
Items: 
Size: 615443 Color: 17
Size: 384552 Color: 14

Bin 947: 6 of cap free
Amount of items: 2
Items: 
Size: 615416 Color: 1
Size: 384579 Color: 2

Bin 948: 6 of cap free
Amount of items: 3
Items: 
Size: 625306 Color: 4
Size: 188558 Color: 15
Size: 186131 Color: 5

Bin 949: 6 of cap free
Amount of items: 3
Items: 
Size: 627037 Color: 10
Size: 187940 Color: 5
Size: 185018 Color: 10

Bin 950: 6 of cap free
Amount of items: 2
Items: 
Size: 653567 Color: 14
Size: 346428 Color: 0

Bin 951: 6 of cap free
Amount of items: 3
Items: 
Size: 653876 Color: 18
Size: 173892 Color: 8
Size: 172227 Color: 10

Bin 952: 6 of cap free
Amount of items: 2
Items: 
Size: 656623 Color: 2
Size: 343372 Color: 5

Bin 953: 6 of cap free
Amount of items: 3
Items: 
Size: 671292 Color: 11
Size: 164513 Color: 3
Size: 164190 Color: 12

Bin 954: 6 of cap free
Amount of items: 2
Items: 
Size: 674009 Color: 18
Size: 325986 Color: 19

Bin 955: 6 of cap free
Amount of items: 2
Items: 
Size: 679625 Color: 16
Size: 320370 Color: 14

Bin 956: 6 of cap free
Amount of items: 2
Items: 
Size: 687852 Color: 6
Size: 312143 Color: 13

Bin 957: 6 of cap free
Amount of items: 2
Items: 
Size: 700906 Color: 6
Size: 299089 Color: 13

Bin 958: 6 of cap free
Amount of items: 2
Items: 
Size: 701131 Color: 10
Size: 298864 Color: 8

Bin 959: 6 of cap free
Amount of items: 2
Items: 
Size: 703722 Color: 8
Size: 296273 Color: 5

Bin 960: 6 of cap free
Amount of items: 2
Items: 
Size: 713510 Color: 19
Size: 286485 Color: 10

Bin 961: 6 of cap free
Amount of items: 3
Items: 
Size: 718314 Color: 1
Size: 141066 Color: 14
Size: 140615 Color: 16

Bin 962: 6 of cap free
Amount of items: 2
Items: 
Size: 719022 Color: 2
Size: 280973 Color: 7

Bin 963: 6 of cap free
Amount of items: 2
Items: 
Size: 726852 Color: 9
Size: 273143 Color: 13

Bin 964: 6 of cap free
Amount of items: 2
Items: 
Size: 737139 Color: 16
Size: 262856 Color: 11

Bin 965: 6 of cap free
Amount of items: 2
Items: 
Size: 751441 Color: 8
Size: 248554 Color: 17

Bin 966: 6 of cap free
Amount of items: 2
Items: 
Size: 757890 Color: 13
Size: 242105 Color: 10

Bin 967: 6 of cap free
Amount of items: 2
Items: 
Size: 760497 Color: 6
Size: 239498 Color: 19

Bin 968: 6 of cap free
Amount of items: 2
Items: 
Size: 765130 Color: 19
Size: 234865 Color: 16

Bin 969: 6 of cap free
Amount of items: 2
Items: 
Size: 781061 Color: 17
Size: 218934 Color: 19

Bin 970: 7 of cap free
Amount of items: 3
Items: 
Size: 393156 Color: 3
Size: 309283 Color: 16
Size: 297555 Color: 13

Bin 971: 7 of cap free
Amount of items: 3
Items: 
Size: 707989 Color: 14
Size: 146123 Color: 19
Size: 145882 Color: 8

Bin 972: 7 of cap free
Amount of items: 3
Items: 
Size: 620399 Color: 11
Size: 189848 Color: 9
Size: 189747 Color: 17

Bin 973: 7 of cap free
Amount of items: 3
Items: 
Size: 549459 Color: 14
Size: 259852 Color: 1
Size: 190683 Color: 15

Bin 974: 7 of cap free
Amount of items: 3
Items: 
Size: 695515 Color: 1
Size: 152616 Color: 9
Size: 151863 Color: 14

Bin 975: 7 of cap free
Amount of items: 3
Items: 
Size: 662473 Color: 19
Size: 170934 Color: 16
Size: 166587 Color: 5

Bin 976: 7 of cap free
Amount of items: 3
Items: 
Size: 605730 Color: 10
Size: 197801 Color: 11
Size: 196463 Color: 16

Bin 977: 7 of cap free
Amount of items: 3
Items: 
Size: 658793 Color: 11
Size: 170621 Color: 15
Size: 170580 Color: 16

Bin 978: 7 of cap free
Amount of items: 3
Items: 
Size: 587422 Color: 1
Size: 209462 Color: 19
Size: 203110 Color: 11

Bin 979: 7 of cap free
Amount of items: 3
Items: 
Size: 606862 Color: 3
Size: 197906 Color: 18
Size: 195226 Color: 19

Bin 980: 7 of cap free
Amount of items: 3
Items: 
Size: 759442 Color: 3
Size: 127111 Color: 14
Size: 113441 Color: 6

Bin 981: 7 of cap free
Amount of items: 3
Items: 
Size: 609022 Color: 11
Size: 195840 Color: 9
Size: 195132 Color: 16

Bin 982: 7 of cap free
Amount of items: 3
Items: 
Size: 686151 Color: 5
Size: 157617 Color: 17
Size: 156226 Color: 1

Bin 983: 7 of cap free
Amount of items: 3
Items: 
Size: 463861 Color: 18
Size: 281257 Color: 18
Size: 254876 Color: 1

Bin 984: 7 of cap free
Amount of items: 3
Items: 
Size: 737975 Color: 4
Size: 150077 Color: 2
Size: 111942 Color: 2

Bin 985: 7 of cap free
Amount of items: 3
Items: 
Size: 644667 Color: 8
Size: 178175 Color: 9
Size: 177152 Color: 8

Bin 986: 7 of cap free
Amount of items: 3
Items: 
Size: 644266 Color: 12
Size: 178346 Color: 13
Size: 177382 Color: 14

Bin 987: 7 of cap free
Amount of items: 2
Items: 
Size: 696157 Color: 17
Size: 303837 Color: 13

Bin 988: 7 of cap free
Amount of items: 3
Items: 
Size: 505671 Color: 19
Size: 252764 Color: 14
Size: 241559 Color: 5

Bin 989: 7 of cap free
Amount of items: 2
Items: 
Size: 706425 Color: 9
Size: 293569 Color: 8

Bin 990: 7 of cap free
Amount of items: 3
Items: 
Size: 631649 Color: 4
Size: 200346 Color: 13
Size: 167999 Color: 12

Bin 991: 7 of cap free
Amount of items: 3
Items: 
Size: 739801 Color: 9
Size: 130325 Color: 2
Size: 129868 Color: 9

Bin 992: 7 of cap free
Amount of items: 3
Items: 
Size: 616340 Color: 7
Size: 192198 Color: 10
Size: 191456 Color: 17

Bin 993: 7 of cap free
Amount of items: 3
Items: 
Size: 686516 Color: 17
Size: 161409 Color: 17
Size: 152069 Color: 4

Bin 994: 7 of cap free
Amount of items: 3
Items: 
Size: 707438 Color: 19
Size: 146387 Color: 13
Size: 146169 Color: 18

Bin 995: 7 of cap free
Amount of items: 2
Items: 
Size: 614312 Color: 13
Size: 385682 Color: 15

Bin 996: 7 of cap free
Amount of items: 3
Items: 
Size: 364020 Color: 17
Size: 356026 Color: 15
Size: 279948 Color: 12

Bin 997: 7 of cap free
Amount of items: 2
Items: 
Size: 703914 Color: 0
Size: 296080 Color: 8

Bin 998: 7 of cap free
Amount of items: 3
Items: 
Size: 627276 Color: 19
Size: 188276 Color: 7
Size: 184442 Color: 10

Bin 999: 7 of cap free
Amount of items: 2
Items: 
Size: 615376 Color: 9
Size: 384618 Color: 3

Bin 1000: 7 of cap free
Amount of items: 2
Items: 
Size: 664293 Color: 14
Size: 335701 Color: 11

Bin 1001: 7 of cap free
Amount of items: 3
Items: 
Size: 534090 Color: 15
Size: 234981 Color: 5
Size: 230923 Color: 5

Bin 1002: 7 of cap free
Amount of items: 2
Items: 
Size: 636734 Color: 8
Size: 363260 Color: 11

Bin 1003: 7 of cap free
Amount of items: 2
Items: 
Size: 539922 Color: 19
Size: 460072 Color: 5

Bin 1004: 7 of cap free
Amount of items: 2
Items: 
Size: 516810 Color: 9
Size: 483184 Color: 16

Bin 1005: 7 of cap free
Amount of items: 2
Items: 
Size: 691270 Color: 2
Size: 308724 Color: 12

Bin 1006: 7 of cap free
Amount of items: 3
Items: 
Size: 467985 Color: 6
Size: 268930 Color: 13
Size: 263079 Color: 6

Bin 1007: 7 of cap free
Amount of items: 3
Items: 
Size: 454841 Color: 10
Size: 275446 Color: 0
Size: 269707 Color: 12

Bin 1008: 7 of cap free
Amount of items: 3
Items: 
Size: 459017 Color: 13
Size: 270650 Color: 6
Size: 270327 Color: 15

Bin 1009: 7 of cap free
Amount of items: 3
Items: 
Size: 463232 Color: 12
Size: 268641 Color: 9
Size: 268121 Color: 17

Bin 1010: 7 of cap free
Amount of items: 3
Items: 
Size: 468629 Color: 10
Size: 266766 Color: 7
Size: 264599 Color: 7

Bin 1011: 7 of cap free
Amount of items: 2
Items: 
Size: 508438 Color: 15
Size: 491556 Color: 11

Bin 1012: 7 of cap free
Amount of items: 2
Items: 
Size: 512674 Color: 8
Size: 487320 Color: 0

Bin 1013: 7 of cap free
Amount of items: 2
Items: 
Size: 516144 Color: 11
Size: 483850 Color: 19

Bin 1014: 7 of cap free
Amount of items: 2
Items: 
Size: 519440 Color: 12
Size: 480554 Color: 15

Bin 1015: 7 of cap free
Amount of items: 2
Items: 
Size: 525944 Color: 16
Size: 474050 Color: 3

Bin 1016: 7 of cap free
Amount of items: 2
Items: 
Size: 521491 Color: 9
Size: 478503 Color: 1

Bin 1017: 7 of cap free
Amount of items: 2
Items: 
Size: 527964 Color: 5
Size: 472030 Color: 11

Bin 1018: 7 of cap free
Amount of items: 2
Items: 
Size: 530875 Color: 7
Size: 469119 Color: 0

Bin 1019: 7 of cap free
Amount of items: 2
Items: 
Size: 535787 Color: 4
Size: 464207 Color: 0

Bin 1020: 7 of cap free
Amount of items: 2
Items: 
Size: 544661 Color: 13
Size: 455333 Color: 2

Bin 1021: 7 of cap free
Amount of items: 2
Items: 
Size: 552612 Color: 2
Size: 447382 Color: 4

Bin 1022: 7 of cap free
Amount of items: 2
Items: 
Size: 553077 Color: 5
Size: 446917 Color: 13

Bin 1023: 7 of cap free
Amount of items: 2
Items: 
Size: 562697 Color: 9
Size: 437297 Color: 4

Bin 1024: 7 of cap free
Amount of items: 2
Items: 
Size: 563504 Color: 3
Size: 436490 Color: 7

Bin 1025: 7 of cap free
Amount of items: 2
Items: 
Size: 567151 Color: 19
Size: 432843 Color: 9

Bin 1026: 7 of cap free
Amount of items: 2
Items: 
Size: 569955 Color: 13
Size: 430039 Color: 6

Bin 1027: 7 of cap free
Amount of items: 2
Items: 
Size: 570164 Color: 11
Size: 429830 Color: 19

Bin 1028: 7 of cap free
Amount of items: 2
Items: 
Size: 602485 Color: 5
Size: 397509 Color: 19

Bin 1029: 7 of cap free
Amount of items: 2
Items: 
Size: 609400 Color: 2
Size: 390594 Color: 7

Bin 1030: 7 of cap free
Amount of items: 2
Items: 
Size: 643894 Color: 5
Size: 356100 Color: 18

Bin 1031: 7 of cap free
Amount of items: 3
Items: 
Size: 648912 Color: 5
Size: 175700 Color: 0
Size: 175382 Color: 13

Bin 1032: 7 of cap free
Amount of items: 2
Items: 
Size: 651090 Color: 19
Size: 348904 Color: 5

Bin 1033: 7 of cap free
Amount of items: 2
Items: 
Size: 668033 Color: 0
Size: 331961 Color: 5

Bin 1034: 7 of cap free
Amount of items: 2
Items: 
Size: 684645 Color: 19
Size: 315349 Color: 7

Bin 1035: 7 of cap free
Amount of items: 2
Items: 
Size: 685796 Color: 5
Size: 314198 Color: 1

Bin 1036: 7 of cap free
Amount of items: 2
Items: 
Size: 690749 Color: 18
Size: 309245 Color: 4

Bin 1037: 7 of cap free
Amount of items: 2
Items: 
Size: 699161 Color: 4
Size: 300833 Color: 13

Bin 1038: 7 of cap free
Amount of items: 2
Items: 
Size: 708117 Color: 10
Size: 291877 Color: 3

Bin 1039: 7 of cap free
Amount of items: 2
Items: 
Size: 709916 Color: 2
Size: 290078 Color: 1

Bin 1040: 7 of cap free
Amount of items: 2
Items: 
Size: 711642 Color: 7
Size: 288352 Color: 9

Bin 1041: 7 of cap free
Amount of items: 2
Items: 
Size: 723654 Color: 3
Size: 276340 Color: 13

Bin 1042: 7 of cap free
Amount of items: 2
Items: 
Size: 732454 Color: 2
Size: 267540 Color: 0

Bin 1043: 7 of cap free
Amount of items: 2
Items: 
Size: 736209 Color: 3
Size: 263785 Color: 14

Bin 1044: 7 of cap free
Amount of items: 3
Items: 
Size: 740716 Color: 3
Size: 129757 Color: 15
Size: 129521 Color: 9

Bin 1045: 7 of cap free
Amount of items: 2
Items: 
Size: 744991 Color: 0
Size: 255003 Color: 9

Bin 1046: 7 of cap free
Amount of items: 2
Items: 
Size: 761469 Color: 9
Size: 238525 Color: 3

Bin 1047: 7 of cap free
Amount of items: 2
Items: 
Size: 768235 Color: 17
Size: 231759 Color: 8

Bin 1048: 8 of cap free
Amount of items: 3
Items: 
Size: 614806 Color: 14
Size: 192850 Color: 11
Size: 192337 Color: 11

Bin 1049: 8 of cap free
Amount of items: 3
Items: 
Size: 732358 Color: 17
Size: 136861 Color: 15
Size: 130774 Color: 15

Bin 1050: 8 of cap free
Amount of items: 3
Items: 
Size: 541910 Color: 9
Size: 231328 Color: 3
Size: 226755 Color: 3

Bin 1051: 8 of cap free
Amount of items: 3
Items: 
Size: 368415 Color: 6
Size: 340134 Color: 18
Size: 291444 Color: 0

Bin 1052: 8 of cap free
Amount of items: 3
Items: 
Size: 612205 Color: 10
Size: 194066 Color: 5
Size: 193722 Color: 11

Bin 1053: 8 of cap free
Amount of items: 3
Items: 
Size: 695138 Color: 4
Size: 154895 Color: 9
Size: 149960 Color: 2

Bin 1054: 8 of cap free
Amount of items: 2
Items: 
Size: 531238 Color: 13
Size: 468755 Color: 8

Bin 1055: 8 of cap free
Amount of items: 3
Items: 
Size: 670989 Color: 12
Size: 166494 Color: 14
Size: 162510 Color: 5

Bin 1056: 8 of cap free
Amount of items: 3
Items: 
Size: 704849 Color: 10
Size: 147595 Color: 4
Size: 147549 Color: 0

Bin 1057: 8 of cap free
Amount of items: 2
Items: 
Size: 681805 Color: 5
Size: 318188 Color: 15

Bin 1058: 8 of cap free
Amount of items: 2
Items: 
Size: 764630 Color: 5
Size: 235363 Color: 18

Bin 1059: 8 of cap free
Amount of items: 3
Items: 
Size: 385145 Color: 2
Size: 307673 Color: 16
Size: 307175 Color: 15

Bin 1060: 8 of cap free
Amount of items: 3
Items: 
Size: 345548 Color: 18
Size: 337561 Color: 13
Size: 316884 Color: 12

Bin 1061: 8 of cap free
Amount of items: 2
Items: 
Size: 568550 Color: 16
Size: 431443 Color: 19

Bin 1062: 8 of cap free
Amount of items: 3
Items: 
Size: 570458 Color: 0
Size: 258791 Color: 15
Size: 170744 Color: 14

Bin 1063: 8 of cap free
Amount of items: 3
Items: 
Size: 625439 Color: 0
Size: 189346 Color: 1
Size: 185208 Color: 15

Bin 1064: 8 of cap free
Amount of items: 2
Items: 
Size: 643342 Color: 3
Size: 356651 Color: 7

Bin 1065: 8 of cap free
Amount of items: 3
Items: 
Size: 490584 Color: 17
Size: 264455 Color: 8
Size: 244954 Color: 18

Bin 1066: 8 of cap free
Amount of items: 3
Items: 
Size: 652569 Color: 19
Size: 173788 Color: 16
Size: 173636 Color: 10

Bin 1067: 8 of cap free
Amount of items: 2
Items: 
Size: 694969 Color: 19
Size: 305024 Color: 11

Bin 1068: 8 of cap free
Amount of items: 2
Items: 
Size: 653357 Color: 0
Size: 346636 Color: 15

Bin 1069: 8 of cap free
Amount of items: 2
Items: 
Size: 511537 Color: 18
Size: 488456 Color: 2

Bin 1070: 8 of cap free
Amount of items: 2
Items: 
Size: 511828 Color: 14
Size: 488165 Color: 10

Bin 1071: 8 of cap free
Amount of items: 2
Items: 
Size: 515887 Color: 6
Size: 484106 Color: 18

Bin 1072: 8 of cap free
Amount of items: 2
Items: 
Size: 527145 Color: 1
Size: 472848 Color: 11

Bin 1073: 8 of cap free
Amount of items: 2
Items: 
Size: 534144 Color: 19
Size: 465849 Color: 2

Bin 1074: 8 of cap free
Amount of items: 2
Items: 
Size: 534502 Color: 1
Size: 465491 Color: 12

Bin 1075: 8 of cap free
Amount of items: 2
Items: 
Size: 536423 Color: 13
Size: 463570 Color: 1

Bin 1076: 8 of cap free
Amount of items: 2
Items: 
Size: 544333 Color: 11
Size: 455660 Color: 10

Bin 1077: 8 of cap free
Amount of items: 2
Items: 
Size: 558186 Color: 13
Size: 441807 Color: 8

Bin 1078: 8 of cap free
Amount of items: 2
Items: 
Size: 578526 Color: 15
Size: 421467 Color: 18

Bin 1079: 8 of cap free
Amount of items: 2
Items: 
Size: 580686 Color: 0
Size: 419307 Color: 10

Bin 1080: 8 of cap free
Amount of items: 2
Items: 
Size: 597008 Color: 7
Size: 402985 Color: 1

Bin 1081: 8 of cap free
Amount of items: 2
Items: 
Size: 599298 Color: 6
Size: 400695 Color: 2

Bin 1082: 8 of cap free
Amount of items: 2
Items: 
Size: 601852 Color: 12
Size: 398141 Color: 16

Bin 1083: 8 of cap free
Amount of items: 2
Items: 
Size: 605945 Color: 11
Size: 394048 Color: 9

Bin 1084: 8 of cap free
Amount of items: 2
Items: 
Size: 609300 Color: 9
Size: 390693 Color: 15

Bin 1085: 8 of cap free
Amount of items: 3
Items: 
Size: 616715 Color: 4
Size: 191649 Color: 7
Size: 191629 Color: 17

Bin 1086: 8 of cap free
Amount of items: 3
Items: 
Size: 627539 Color: 4
Size: 187101 Color: 7
Size: 185353 Color: 6

Bin 1087: 8 of cap free
Amount of items: 2
Items: 
Size: 631881 Color: 4
Size: 368112 Color: 15

Bin 1088: 8 of cap free
Amount of items: 2
Items: 
Size: 641759 Color: 9
Size: 358234 Color: 6

Bin 1089: 8 of cap free
Amount of items: 3
Items: 
Size: 645420 Color: 19
Size: 177325 Color: 13
Size: 177248 Color: 16

Bin 1090: 8 of cap free
Amount of items: 3
Items: 
Size: 668293 Color: 1
Size: 168312 Color: 14
Size: 163388 Color: 5

Bin 1091: 8 of cap free
Amount of items: 2
Items: 
Size: 673117 Color: 4
Size: 326876 Color: 12

Bin 1092: 8 of cap free
Amount of items: 2
Items: 
Size: 673750 Color: 2
Size: 326243 Color: 19

Bin 1093: 8 of cap free
Amount of items: 2
Items: 
Size: 675047 Color: 0
Size: 324946 Color: 17

Bin 1094: 8 of cap free
Amount of items: 2
Items: 
Size: 684995 Color: 7
Size: 314998 Color: 5

Bin 1095: 8 of cap free
Amount of items: 2
Items: 
Size: 685850 Color: 5
Size: 314143 Color: 9

Bin 1096: 8 of cap free
Amount of items: 2
Items: 
Size: 740559 Color: 19
Size: 259434 Color: 12

Bin 1097: 8 of cap free
Amount of items: 2
Items: 
Size: 748103 Color: 3
Size: 251890 Color: 6

Bin 1098: 8 of cap free
Amount of items: 2
Items: 
Size: 751860 Color: 9
Size: 248133 Color: 14

Bin 1099: 8 of cap free
Amount of items: 2
Items: 
Size: 767236 Color: 11
Size: 232757 Color: 3

Bin 1100: 8 of cap free
Amount of items: 2
Items: 
Size: 775161 Color: 13
Size: 224832 Color: 15

Bin 1101: 8 of cap free
Amount of items: 2
Items: 
Size: 778512 Color: 5
Size: 221481 Color: 1

Bin 1102: 8 of cap free
Amount of items: 2
Items: 
Size: 797339 Color: 5
Size: 202654 Color: 16

Bin 1103: 9 of cap free
Amount of items: 3
Items: 
Size: 770981 Color: 1
Size: 115866 Color: 16
Size: 113145 Color: 10

Bin 1104: 9 of cap free
Amount of items: 3
Items: 
Size: 689393 Color: 3
Size: 155444 Color: 8
Size: 155155 Color: 9

Bin 1105: 9 of cap free
Amount of items: 3
Items: 
Size: 455558 Color: 5
Size: 277041 Color: 0
Size: 267393 Color: 10

Bin 1106: 9 of cap free
Amount of items: 3
Items: 
Size: 613950 Color: 13
Size: 194369 Color: 14
Size: 191673 Color: 5

Bin 1107: 9 of cap free
Amount of items: 3
Items: 
Size: 368891 Color: 14
Size: 322163 Color: 13
Size: 308938 Color: 4

Bin 1108: 9 of cap free
Amount of items: 2
Items: 
Size: 656918 Color: 14
Size: 343074 Color: 2

Bin 1109: 9 of cap free
Amount of items: 2
Items: 
Size: 703601 Color: 2
Size: 296391 Color: 8

Bin 1110: 9 of cap free
Amount of items: 3
Items: 
Size: 517717 Color: 19
Size: 337933 Color: 19
Size: 144342 Color: 3

Bin 1111: 9 of cap free
Amount of items: 3
Items: 
Size: 367633 Color: 16
Size: 339014 Color: 12
Size: 293345 Color: 14

Bin 1112: 9 of cap free
Amount of items: 2
Items: 
Size: 506858 Color: 8
Size: 493134 Color: 9

Bin 1113: 9 of cap free
Amount of items: 3
Items: 
Size: 392407 Color: 8
Size: 307661 Color: 12
Size: 299924 Color: 19

Bin 1114: 9 of cap free
Amount of items: 2
Items: 
Size: 776110 Color: 18
Size: 223882 Color: 12

Bin 1115: 9 of cap free
Amount of items: 3
Items: 
Size: 710826 Color: 11
Size: 144604 Color: 4
Size: 144562 Color: 4

Bin 1116: 9 of cap free
Amount of items: 3
Items: 
Size: 372051 Color: 7
Size: 329243 Color: 4
Size: 298698 Color: 16

Bin 1117: 9 of cap free
Amount of items: 3
Items: 
Size: 653646 Color: 19
Size: 174226 Color: 14
Size: 172120 Color: 8

Bin 1118: 9 of cap free
Amount of items: 2
Items: 
Size: 516849 Color: 15
Size: 483143 Color: 2

Bin 1119: 9 of cap free
Amount of items: 2
Items: 
Size: 544780 Color: 10
Size: 455212 Color: 18

Bin 1120: 9 of cap free
Amount of items: 3
Items: 
Size: 368586 Color: 10
Size: 337989 Color: 5
Size: 293417 Color: 14

Bin 1121: 9 of cap free
Amount of items: 2
Items: 
Size: 685738 Color: 15
Size: 314254 Color: 5

Bin 1122: 9 of cap free
Amount of items: 2
Items: 
Size: 661246 Color: 13
Size: 338746 Color: 4

Bin 1123: 9 of cap free
Amount of items: 2
Items: 
Size: 569689 Color: 12
Size: 430303 Color: 7

Bin 1124: 9 of cap free
Amount of items: 3
Items: 
Size: 369628 Color: 7
Size: 329314 Color: 1
Size: 301050 Color: 17

Bin 1125: 9 of cap free
Amount of items: 3
Items: 
Size: 372672 Color: 14
Size: 346568 Color: 17
Size: 280752 Color: 11

Bin 1126: 9 of cap free
Amount of items: 3
Items: 
Size: 386290 Color: 9
Size: 329349 Color: 10
Size: 284353 Color: 11

Bin 1127: 9 of cap free
Amount of items: 3
Items: 
Size: 504019 Color: 0
Size: 263492 Color: 3
Size: 232481 Color: 10

Bin 1128: 9 of cap free
Amount of items: 2
Items: 
Size: 517387 Color: 5
Size: 482605 Color: 11

Bin 1129: 9 of cap free
Amount of items: 2
Items: 
Size: 531702 Color: 9
Size: 468290 Color: 19

Bin 1130: 9 of cap free
Amount of items: 2
Items: 
Size: 553587 Color: 9
Size: 446405 Color: 14

Bin 1131: 9 of cap free
Amount of items: 2
Items: 
Size: 558531 Color: 9
Size: 441461 Color: 18

Bin 1132: 9 of cap free
Amount of items: 2
Items: 
Size: 560873 Color: 4
Size: 439119 Color: 13

Bin 1133: 9 of cap free
Amount of items: 2
Items: 
Size: 565026 Color: 2
Size: 434966 Color: 10

Bin 1134: 9 of cap free
Amount of items: 2
Items: 
Size: 574847 Color: 1
Size: 425145 Color: 18

Bin 1135: 9 of cap free
Amount of items: 2
Items: 
Size: 578246 Color: 6
Size: 421746 Color: 10

Bin 1136: 9 of cap free
Amount of items: 2
Items: 
Size: 592977 Color: 2
Size: 407015 Color: 17

Bin 1137: 9 of cap free
Amount of items: 2
Items: 
Size: 598117 Color: 11
Size: 401875 Color: 10

Bin 1138: 9 of cap free
Amount of items: 2
Items: 
Size: 598310 Color: 17
Size: 401682 Color: 18

Bin 1139: 9 of cap free
Amount of items: 2
Items: 
Size: 620963 Color: 7
Size: 379029 Color: 8

Bin 1140: 9 of cap free
Amount of items: 3
Items: 
Size: 625785 Color: 1
Size: 191474 Color: 9
Size: 182733 Color: 12

Bin 1141: 9 of cap free
Amount of items: 2
Items: 
Size: 631282 Color: 12
Size: 368710 Color: 17

Bin 1142: 9 of cap free
Amount of items: 3
Items: 
Size: 640478 Color: 17
Size: 179888 Color: 0
Size: 179626 Color: 1

Bin 1143: 9 of cap free
Amount of items: 2
Items: 
Size: 641894 Color: 5
Size: 358098 Color: 14

Bin 1144: 9 of cap free
Amount of items: 2
Items: 
Size: 645662 Color: 12
Size: 354330 Color: 2

Bin 1145: 9 of cap free
Amount of items: 2
Items: 
Size: 653619 Color: 14
Size: 346373 Color: 8

Bin 1146: 9 of cap free
Amount of items: 2
Items: 
Size: 666484 Color: 16
Size: 333508 Color: 14

Bin 1147: 9 of cap free
Amount of items: 2
Items: 
Size: 673572 Color: 16
Size: 326420 Color: 5

Bin 1148: 9 of cap free
Amount of items: 2
Items: 
Size: 677286 Color: 3
Size: 322706 Color: 16

Bin 1149: 9 of cap free
Amount of items: 2
Items: 
Size: 679649 Color: 3
Size: 320343 Color: 16

Bin 1150: 9 of cap free
Amount of items: 2
Items: 
Size: 682350 Color: 7
Size: 317642 Color: 18

Bin 1151: 9 of cap free
Amount of items: 3
Items: 
Size: 690312 Color: 10
Size: 156077 Color: 1
Size: 153603 Color: 18

Bin 1152: 9 of cap free
Amount of items: 3
Items: 
Size: 692520 Color: 16
Size: 153924 Color: 4
Size: 153548 Color: 8

Bin 1153: 9 of cap free
Amount of items: 2
Items: 
Size: 694856 Color: 5
Size: 305136 Color: 1

Bin 1154: 9 of cap free
Amount of items: 2
Items: 
Size: 706311 Color: 4
Size: 293681 Color: 2

Bin 1155: 9 of cap free
Amount of items: 2
Items: 
Size: 707046 Color: 18
Size: 292946 Color: 4

Bin 1156: 9 of cap free
Amount of items: 2
Items: 
Size: 714925 Color: 2
Size: 285067 Color: 5

Bin 1157: 9 of cap free
Amount of items: 2
Items: 
Size: 718223 Color: 7
Size: 281769 Color: 14

Bin 1158: 9 of cap free
Amount of items: 2
Items: 
Size: 719749 Color: 11
Size: 280243 Color: 6

Bin 1159: 9 of cap free
Amount of items: 2
Items: 
Size: 729081 Color: 13
Size: 270911 Color: 19

Bin 1160: 9 of cap free
Amount of items: 2
Items: 
Size: 736466 Color: 18
Size: 263526 Color: 7

Bin 1161: 9 of cap free
Amount of items: 2
Items: 
Size: 745610 Color: 5
Size: 254382 Color: 19

Bin 1162: 9 of cap free
Amount of items: 2
Items: 
Size: 746351 Color: 8
Size: 253641 Color: 0

Bin 1163: 9 of cap free
Amount of items: 2
Items: 
Size: 773521 Color: 15
Size: 226471 Color: 10

Bin 1164: 9 of cap free
Amount of items: 2
Items: 
Size: 788279 Color: 7
Size: 211713 Color: 18

Bin 1165: 9 of cap free
Amount of items: 2
Items: 
Size: 792723 Color: 13
Size: 207269 Color: 2

Bin 1166: 9 of cap free
Amount of items: 2
Items: 
Size: 796827 Color: 2
Size: 203165 Color: 12

Bin 1167: 10 of cap free
Amount of items: 3
Items: 
Size: 759486 Color: 11
Size: 139847 Color: 1
Size: 100658 Color: 9

Bin 1168: 10 of cap free
Amount of items: 3
Items: 
Size: 722732 Color: 6
Size: 138739 Color: 18
Size: 138520 Color: 0

Bin 1169: 10 of cap free
Amount of items: 3
Items: 
Size: 602555 Color: 2
Size: 210643 Color: 3
Size: 186793 Color: 3

Bin 1170: 10 of cap free
Amount of items: 2
Items: 
Size: 750464 Color: 17
Size: 249527 Color: 1

Bin 1171: 10 of cap free
Amount of items: 2
Items: 
Size: 618446 Color: 3
Size: 381545 Color: 1

Bin 1172: 10 of cap free
Amount of items: 3
Items: 
Size: 670012 Color: 8
Size: 165420 Color: 3
Size: 164559 Color: 12

Bin 1173: 10 of cap free
Amount of items: 2
Items: 
Size: 779760 Color: 5
Size: 220231 Color: 2

Bin 1174: 10 of cap free
Amount of items: 3
Items: 
Size: 636867 Color: 15
Size: 182309 Color: 11
Size: 180815 Color: 4

Bin 1175: 10 of cap free
Amount of items: 3
Items: 
Size: 505061 Color: 10
Size: 264292 Color: 4
Size: 230638 Color: 8

Bin 1176: 10 of cap free
Amount of items: 3
Items: 
Size: 555977 Color: 1
Size: 265963 Color: 11
Size: 178051 Color: 13

Bin 1177: 10 of cap free
Amount of items: 2
Items: 
Size: 706345 Color: 17
Size: 293646 Color: 2

Bin 1178: 10 of cap free
Amount of items: 2
Items: 
Size: 565639 Color: 15
Size: 434352 Color: 6

Bin 1179: 10 of cap free
Amount of items: 3
Items: 
Size: 697197 Color: 13
Size: 165148 Color: 9
Size: 137646 Color: 6

Bin 1180: 10 of cap free
Amount of items: 3
Items: 
Size: 695056 Color: 18
Size: 174412 Color: 14
Size: 130523 Color: 1

Bin 1181: 10 of cap free
Amount of items: 3
Items: 
Size: 388657 Color: 4
Size: 333549 Color: 7
Size: 277785 Color: 19

Bin 1182: 10 of cap free
Amount of items: 3
Items: 
Size: 348583 Color: 8
Size: 338055 Color: 11
Size: 313353 Color: 7

Bin 1183: 10 of cap free
Amount of items: 2
Items: 
Size: 566499 Color: 11
Size: 433492 Color: 12

Bin 1184: 10 of cap free
Amount of items: 3
Items: 
Size: 363407 Color: 8
Size: 342747 Color: 4
Size: 293837 Color: 3

Bin 1185: 10 of cap free
Amount of items: 2
Items: 
Size: 693663 Color: 19
Size: 306328 Color: 17

Bin 1186: 10 of cap free
Amount of items: 2
Items: 
Size: 625569 Color: 11
Size: 374422 Color: 12

Bin 1187: 10 of cap free
Amount of items: 3
Items: 
Size: 362986 Color: 10
Size: 335829 Color: 5
Size: 301176 Color: 11

Bin 1188: 10 of cap free
Amount of items: 3
Items: 
Size: 363568 Color: 15
Size: 337503 Color: 13
Size: 298920 Color: 5

Bin 1189: 10 of cap free
Amount of items: 3
Items: 
Size: 367379 Color: 0
Size: 325568 Color: 5
Size: 307044 Color: 19

Bin 1190: 10 of cap free
Amount of items: 3
Items: 
Size: 372767 Color: 18
Size: 340832 Color: 8
Size: 286392 Color: 3

Bin 1191: 10 of cap free
Amount of items: 2
Items: 
Size: 515604 Color: 18
Size: 484387 Color: 10

Bin 1192: 10 of cap free
Amount of items: 2
Items: 
Size: 532179 Color: 18
Size: 467812 Color: 17

Bin 1193: 10 of cap free
Amount of items: 2
Items: 
Size: 534767 Color: 8
Size: 465224 Color: 5

Bin 1194: 10 of cap free
Amount of items: 2
Items: 
Size: 535500 Color: 3
Size: 464491 Color: 16

Bin 1195: 10 of cap free
Amount of items: 2
Items: 
Size: 539106 Color: 19
Size: 460885 Color: 12

Bin 1196: 10 of cap free
Amount of items: 2
Items: 
Size: 540388 Color: 13
Size: 459603 Color: 16

Bin 1197: 10 of cap free
Amount of items: 2
Items: 
Size: 542445 Color: 0
Size: 457546 Color: 16

Bin 1198: 10 of cap free
Amount of items: 2
Items: 
Size: 544050 Color: 19
Size: 455941 Color: 7

Bin 1199: 10 of cap free
Amount of items: 2
Items: 
Size: 547664 Color: 2
Size: 452327 Color: 11

Bin 1200: 10 of cap free
Amount of items: 2
Items: 
Size: 568929 Color: 6
Size: 431062 Color: 1

Bin 1201: 10 of cap free
Amount of items: 2
Items: 
Size: 576829 Color: 19
Size: 423162 Color: 1

Bin 1202: 10 of cap free
Amount of items: 2
Items: 
Size: 595309 Color: 7
Size: 404682 Color: 1

Bin 1203: 10 of cap free
Amount of items: 3
Items: 
Size: 601925 Color: 14
Size: 199033 Color: 17
Size: 199033 Color: 12

Bin 1204: 10 of cap free
Amount of items: 2
Items: 
Size: 603325 Color: 7
Size: 396666 Color: 11

Bin 1205: 10 of cap free
Amount of items: 2
Items: 
Size: 611738 Color: 16
Size: 388253 Color: 10

Bin 1206: 10 of cap free
Amount of items: 3
Items: 
Size: 619453 Color: 13
Size: 190381 Color: 6
Size: 190157 Color: 12

Bin 1207: 10 of cap free
Amount of items: 2
Items: 
Size: 620713 Color: 18
Size: 379278 Color: 2

Bin 1208: 10 of cap free
Amount of items: 2
Items: 
Size: 641263 Color: 6
Size: 358728 Color: 8

Bin 1209: 10 of cap free
Amount of items: 2
Items: 
Size: 644112 Color: 19
Size: 355879 Color: 3

Bin 1210: 10 of cap free
Amount of items: 3
Items: 
Size: 662217 Color: 2
Size: 169015 Color: 16
Size: 168759 Color: 6

Bin 1211: 10 of cap free
Amount of items: 2
Items: 
Size: 665991 Color: 12
Size: 334000 Color: 7

Bin 1212: 10 of cap free
Amount of items: 2
Items: 
Size: 671650 Color: 6
Size: 328341 Color: 4

Bin 1213: 10 of cap free
Amount of items: 2
Items: 
Size: 673128 Color: 17
Size: 326863 Color: 13

Bin 1214: 10 of cap free
Amount of items: 2
Items: 
Size: 673670 Color: 13
Size: 326321 Color: 9

Bin 1215: 10 of cap free
Amount of items: 2
Items: 
Size: 681039 Color: 1
Size: 318952 Color: 2

Bin 1216: 10 of cap free
Amount of items: 3
Items: 
Size: 686500 Color: 6
Size: 156747 Color: 19
Size: 156744 Color: 5

Bin 1217: 10 of cap free
Amount of items: 2
Items: 
Size: 702572 Color: 10
Size: 297419 Color: 18

Bin 1218: 10 of cap free
Amount of items: 2
Items: 
Size: 712816 Color: 2
Size: 287175 Color: 15

Bin 1219: 10 of cap free
Amount of items: 2
Items: 
Size: 736472 Color: 7
Size: 263519 Color: 6

Bin 1220: 10 of cap free
Amount of items: 2
Items: 
Size: 741946 Color: 6
Size: 258045 Color: 14

Bin 1221: 10 of cap free
Amount of items: 2
Items: 
Size: 764004 Color: 11
Size: 235987 Color: 3

Bin 1222: 10 of cap free
Amount of items: 2
Items: 
Size: 776385 Color: 0
Size: 223606 Color: 12

Bin 1223: 10 of cap free
Amount of items: 2
Items: 
Size: 783731 Color: 0
Size: 216260 Color: 14

Bin 1224: 10 of cap free
Amount of items: 2
Items: 
Size: 799970 Color: 16
Size: 200021 Color: 9

Bin 1225: 11 of cap free
Amount of items: 2
Items: 
Size: 712046 Color: 1
Size: 287944 Color: 14

Bin 1226: 11 of cap free
Amount of items: 3
Items: 
Size: 560686 Color: 10
Size: 219849 Color: 3
Size: 219455 Color: 9

Bin 1227: 11 of cap free
Amount of items: 3
Items: 
Size: 712016 Color: 15
Size: 147618 Color: 6
Size: 140356 Color: 11

Bin 1228: 11 of cap free
Amount of items: 3
Items: 
Size: 700784 Color: 14
Size: 149971 Color: 6
Size: 149235 Color: 14

Bin 1229: 11 of cap free
Amount of items: 3
Items: 
Size: 653956 Color: 9
Size: 173145 Color: 1
Size: 172889 Color: 5

Bin 1230: 11 of cap free
Amount of items: 3
Items: 
Size: 701635 Color: 4
Size: 149468 Color: 5
Size: 148887 Color: 11

Bin 1231: 11 of cap free
Amount of items: 3
Items: 
Size: 509713 Color: 6
Size: 263310 Color: 9
Size: 226967 Color: 18

Bin 1232: 11 of cap free
Amount of items: 2
Items: 
Size: 720915 Color: 15
Size: 279075 Color: 11

Bin 1233: 11 of cap free
Amount of items: 2
Items: 
Size: 758983 Color: 6
Size: 241007 Color: 2

Bin 1234: 11 of cap free
Amount of items: 3
Items: 
Size: 367403 Color: 17
Size: 346107 Color: 9
Size: 286480 Color: 5

Bin 1235: 11 of cap free
Amount of items: 3
Items: 
Size: 619313 Color: 11
Size: 191209 Color: 3
Size: 189468 Color: 1

Bin 1236: 11 of cap free
Amount of items: 2
Items: 
Size: 622845 Color: 2
Size: 377145 Color: 6

Bin 1237: 11 of cap free
Amount of items: 3
Items: 
Size: 624404 Color: 9
Size: 188330 Color: 12
Size: 187256 Color: 12

Bin 1238: 11 of cap free
Amount of items: 2
Items: 
Size: 680821 Color: 19
Size: 319169 Color: 9

Bin 1239: 11 of cap free
Amount of items: 3
Items: 
Size: 458854 Color: 3
Size: 272178 Color: 17
Size: 268958 Color: 2

Bin 1240: 11 of cap free
Amount of items: 3
Items: 
Size: 468018 Color: 19
Size: 277801 Color: 7
Size: 254171 Color: 0

Bin 1241: 11 of cap free
Amount of items: 3
Items: 
Size: 350665 Color: 3
Size: 339510 Color: 4
Size: 309815 Color: 11

Bin 1242: 11 of cap free
Amount of items: 3
Items: 
Size: 459992 Color: 9
Size: 270027 Color: 13
Size: 269971 Color: 18

Bin 1243: 11 of cap free
Amount of items: 2
Items: 
Size: 636302 Color: 10
Size: 363688 Color: 18

Bin 1244: 11 of cap free
Amount of items: 3
Items: 
Size: 346227 Color: 9
Size: 344599 Color: 11
Size: 309164 Color: 17

Bin 1245: 11 of cap free
Amount of items: 3
Items: 
Size: 370035 Color: 4
Size: 345193 Color: 11
Size: 284762 Color: 18

Bin 1246: 11 of cap free
Amount of items: 3
Items: 
Size: 393500 Color: 15
Size: 312673 Color: 17
Size: 293817 Color: 3

Bin 1247: 11 of cap free
Amount of items: 2
Items: 
Size: 511348 Color: 11
Size: 488642 Color: 9

Bin 1248: 11 of cap free
Amount of items: 2
Items: 
Size: 511505 Color: 7
Size: 488485 Color: 12

Bin 1249: 11 of cap free
Amount of items: 2
Items: 
Size: 514186 Color: 14
Size: 485804 Color: 10

Bin 1250: 11 of cap free
Amount of items: 2
Items: 
Size: 515975 Color: 19
Size: 484015 Color: 7

Bin 1251: 11 of cap free
Amount of items: 2
Items: 
Size: 519776 Color: 19
Size: 480214 Color: 11

Bin 1252: 11 of cap free
Amount of items: 2
Items: 
Size: 521665 Color: 15
Size: 478325 Color: 11

Bin 1253: 11 of cap free
Amount of items: 2
Items: 
Size: 524035 Color: 15
Size: 475955 Color: 12

Bin 1254: 11 of cap free
Amount of items: 2
Items: 
Size: 530454 Color: 16
Size: 469536 Color: 0

Bin 1255: 11 of cap free
Amount of items: 2
Items: 
Size: 543306 Color: 5
Size: 456684 Color: 16

Bin 1256: 11 of cap free
Amount of items: 2
Items: 
Size: 553677 Color: 13
Size: 446313 Color: 17

Bin 1257: 11 of cap free
Amount of items: 2
Items: 
Size: 557377 Color: 13
Size: 442613 Color: 1

Bin 1258: 11 of cap free
Amount of items: 3
Items: 
Size: 560715 Color: 14
Size: 219675 Color: 18
Size: 219600 Color: 15

Bin 1259: 11 of cap free
Amount of items: 2
Items: 
Size: 566277 Color: 14
Size: 433713 Color: 13

Bin 1260: 11 of cap free
Amount of items: 2
Items: 
Size: 568303 Color: 13
Size: 431687 Color: 14

Bin 1261: 11 of cap free
Amount of items: 2
Items: 
Size: 575650 Color: 18
Size: 424340 Color: 14

Bin 1262: 11 of cap free
Amount of items: 2
Items: 
Size: 578373 Color: 13
Size: 421617 Color: 2

Bin 1263: 11 of cap free
Amount of items: 2
Items: 
Size: 599279 Color: 12
Size: 400711 Color: 0

Bin 1264: 11 of cap free
Amount of items: 2
Items: 
Size: 601416 Color: 6
Size: 398574 Color: 13

Bin 1265: 11 of cap free
Amount of items: 2
Items: 
Size: 603687 Color: 11
Size: 396303 Color: 19

Bin 1266: 11 of cap free
Amount of items: 2
Items: 
Size: 607837 Color: 0
Size: 392153 Color: 2

Bin 1267: 11 of cap free
Amount of items: 2
Items: 
Size: 611560 Color: 12
Size: 388430 Color: 5

Bin 1268: 11 of cap free
Amount of items: 2
Items: 
Size: 613529 Color: 10
Size: 386461 Color: 13

Bin 1269: 11 of cap free
Amount of items: 2
Items: 
Size: 620232 Color: 3
Size: 379758 Color: 16

Bin 1270: 11 of cap free
Amount of items: 2
Items: 
Size: 632667 Color: 19
Size: 367323 Color: 11

Bin 1271: 11 of cap free
Amount of items: 3
Items: 
Size: 636205 Color: 17
Size: 182009 Color: 9
Size: 181776 Color: 18

Bin 1272: 11 of cap free
Amount of items: 2
Items: 
Size: 648287 Color: 12
Size: 351703 Color: 19

Bin 1273: 11 of cap free
Amount of items: 2
Items: 
Size: 649564 Color: 6
Size: 350426 Color: 11

Bin 1274: 11 of cap free
Amount of items: 2
Items: 
Size: 657113 Color: 10
Size: 342877 Color: 17

Bin 1275: 11 of cap free
Amount of items: 3
Items: 
Size: 661171 Color: 14
Size: 169731 Color: 8
Size: 169088 Color: 14

Bin 1276: 11 of cap free
Amount of items: 2
Items: 
Size: 670352 Color: 16
Size: 329638 Color: 12

Bin 1277: 11 of cap free
Amount of items: 2
Items: 
Size: 703463 Color: 9
Size: 296527 Color: 18

Bin 1278: 11 of cap free
Amount of items: 2
Items: 
Size: 712979 Color: 14
Size: 287011 Color: 16

Bin 1279: 11 of cap free
Amount of items: 2
Items: 
Size: 713265 Color: 10
Size: 286725 Color: 18

Bin 1280: 11 of cap free
Amount of items: 2
Items: 
Size: 743682 Color: 5
Size: 256308 Color: 14

Bin 1281: 11 of cap free
Amount of items: 2
Items: 
Size: 752350 Color: 5
Size: 247640 Color: 1

Bin 1282: 11 of cap free
Amount of items: 2
Items: 
Size: 756535 Color: 1
Size: 243455 Color: 7

Bin 1283: 11 of cap free
Amount of items: 2
Items: 
Size: 761856 Color: 14
Size: 238134 Color: 9

Bin 1284: 11 of cap free
Amount of items: 2
Items: 
Size: 770096 Color: 10
Size: 229894 Color: 17

Bin 1285: 11 of cap free
Amount of items: 2
Items: 
Size: 774782 Color: 17
Size: 225208 Color: 13

Bin 1286: 11 of cap free
Amount of items: 2
Items: 
Size: 786600 Color: 4
Size: 213390 Color: 0

Bin 1287: 11 of cap free
Amount of items: 2
Items: 
Size: 787515 Color: 8
Size: 212475 Color: 15

Bin 1288: 11 of cap free
Amount of items: 2
Items: 
Size: 794255 Color: 13
Size: 205735 Color: 3

Bin 1289: 12 of cap free
Amount of items: 2
Items: 
Size: 614420 Color: 1
Size: 385569 Color: 17

Bin 1290: 12 of cap free
Amount of items: 2
Items: 
Size: 728116 Color: 8
Size: 271873 Color: 2

Bin 1291: 12 of cap free
Amount of items: 3
Items: 
Size: 719485 Color: 1
Size: 141188 Color: 19
Size: 139316 Color: 11

Bin 1292: 12 of cap free
Amount of items: 2
Items: 
Size: 503648 Color: 11
Size: 496341 Color: 4

Bin 1293: 12 of cap free
Amount of items: 3
Items: 
Size: 609309 Color: 0
Size: 195622 Color: 18
Size: 195058 Color: 11

Bin 1294: 12 of cap free
Amount of items: 3
Items: 
Size: 606133 Color: 7
Size: 197754 Color: 1
Size: 196102 Color: 14

Bin 1295: 12 of cap free
Amount of items: 3
Items: 
Size: 507095 Color: 8
Size: 262730 Color: 17
Size: 230164 Color: 10

Bin 1296: 12 of cap free
Amount of items: 3
Items: 
Size: 712239 Color: 4
Size: 144115 Color: 9
Size: 143635 Color: 8

Bin 1297: 12 of cap free
Amount of items: 3
Items: 
Size: 615281 Color: 4
Size: 192678 Color: 5
Size: 192030 Color: 16

Bin 1298: 12 of cap free
Amount of items: 3
Items: 
Size: 680098 Color: 16
Size: 160061 Color: 3
Size: 159830 Color: 11

Bin 1299: 12 of cap free
Amount of items: 2
Items: 
Size: 602185 Color: 5
Size: 397804 Color: 2

Bin 1300: 12 of cap free
Amount of items: 3
Items: 
Size: 363368 Color: 6
Size: 336910 Color: 3
Size: 299711 Color: 7

Bin 1301: 12 of cap free
Amount of items: 2
Items: 
Size: 778271 Color: 11
Size: 221718 Color: 2

Bin 1302: 12 of cap free
Amount of items: 2
Items: 
Size: 730908 Color: 10
Size: 269081 Color: 14

Bin 1303: 12 of cap free
Amount of items: 3
Items: 
Size: 350544 Color: 4
Size: 345955 Color: 8
Size: 303490 Color: 1

Bin 1304: 12 of cap free
Amount of items: 3
Items: 
Size: 370466 Color: 10
Size: 330969 Color: 0
Size: 298554 Color: 10

Bin 1305: 12 of cap free
Amount of items: 3
Items: 
Size: 373240 Color: 7
Size: 346576 Color: 8
Size: 280173 Color: 18

Bin 1306: 12 of cap free
Amount of items: 2
Items: 
Size: 501098 Color: 2
Size: 498891 Color: 10

Bin 1307: 12 of cap free
Amount of items: 3
Items: 
Size: 503213 Color: 16
Size: 264585 Color: 1
Size: 232191 Color: 16

Bin 1308: 12 of cap free
Amount of items: 3
Items: 
Size: 504628 Color: 6
Size: 262056 Color: 5
Size: 233305 Color: 4

Bin 1309: 12 of cap free
Amount of items: 2
Items: 
Size: 508121 Color: 14
Size: 491868 Color: 9

Bin 1310: 12 of cap free
Amount of items: 2
Items: 
Size: 513283 Color: 19
Size: 486706 Color: 8

Bin 1311: 12 of cap free
Amount of items: 2
Items: 
Size: 518581 Color: 7
Size: 481408 Color: 2

Bin 1312: 12 of cap free
Amount of items: 2
Items: 
Size: 537078 Color: 4
Size: 462911 Color: 18

Bin 1313: 12 of cap free
Amount of items: 2
Items: 
Size: 549101 Color: 13
Size: 450888 Color: 1

Bin 1314: 12 of cap free
Amount of items: 2
Items: 
Size: 552260 Color: 16
Size: 447729 Color: 9

Bin 1315: 12 of cap free
Amount of items: 2
Items: 
Size: 563644 Color: 5
Size: 436345 Color: 16

Bin 1316: 12 of cap free
Amount of items: 2
Items: 
Size: 565152 Color: 13
Size: 434837 Color: 10

Bin 1317: 12 of cap free
Amount of items: 2
Items: 
Size: 566093 Color: 13
Size: 433896 Color: 5

Bin 1318: 12 of cap free
Amount of items: 2
Items: 
Size: 571693 Color: 9
Size: 428296 Color: 13

Bin 1319: 12 of cap free
Amount of items: 2
Items: 
Size: 596456 Color: 8
Size: 403533 Color: 17

Bin 1320: 12 of cap free
Amount of items: 2
Items: 
Size: 597139 Color: 8
Size: 402850 Color: 14

Bin 1321: 12 of cap free
Amount of items: 2
Items: 
Size: 611684 Color: 4
Size: 388305 Color: 5

Bin 1322: 12 of cap free
Amount of items: 2
Items: 
Size: 643935 Color: 14
Size: 356054 Color: 11

Bin 1323: 12 of cap free
Amount of items: 2
Items: 
Size: 652217 Color: 2
Size: 347772 Color: 13

Bin 1324: 12 of cap free
Amount of items: 2
Items: 
Size: 660001 Color: 0
Size: 339988 Color: 12

Bin 1325: 12 of cap free
Amount of items: 2
Items: 
Size: 682107 Color: 3
Size: 317882 Color: 15

Bin 1326: 12 of cap free
Amount of items: 2
Items: 
Size: 724841 Color: 14
Size: 275148 Color: 17

Bin 1327: 12 of cap free
Amount of items: 2
Items: 
Size: 762047 Color: 5
Size: 237942 Color: 6

Bin 1328: 12 of cap free
Amount of items: 2
Items: 
Size: 764728 Color: 10
Size: 235261 Color: 18

Bin 1329: 12 of cap free
Amount of items: 2
Items: 
Size: 783519 Color: 1
Size: 216470 Color: 16

Bin 1330: 13 of cap free
Amount of items: 2
Items: 
Size: 582081 Color: 5
Size: 417907 Color: 10

Bin 1331: 13 of cap free
Amount of items: 2
Items: 
Size: 703083 Color: 19
Size: 296905 Color: 1

Bin 1332: 13 of cap free
Amount of items: 3
Items: 
Size: 680388 Color: 4
Size: 159883 Color: 13
Size: 159717 Color: 15

Bin 1333: 13 of cap free
Amount of items: 2
Items: 
Size: 678458 Color: 13
Size: 321530 Color: 17

Bin 1334: 13 of cap free
Amount of items: 3
Items: 
Size: 455469 Color: 6
Size: 272914 Color: 17
Size: 271605 Color: 2

Bin 1335: 13 of cap free
Amount of items: 3
Items: 
Size: 568043 Color: 10
Size: 216507 Color: 3
Size: 215438 Color: 15

Bin 1336: 13 of cap free
Amount of items: 2
Items: 
Size: 694297 Color: 14
Size: 305691 Color: 15

Bin 1337: 13 of cap free
Amount of items: 2
Items: 
Size: 632216 Color: 11
Size: 367772 Color: 10

Bin 1338: 13 of cap free
Amount of items: 3
Items: 
Size: 403826 Color: 4
Size: 304699 Color: 4
Size: 291463 Color: 9

Bin 1339: 13 of cap free
Amount of items: 2
Items: 
Size: 689171 Color: 18
Size: 310817 Color: 6

Bin 1340: 13 of cap free
Amount of items: 3
Items: 
Size: 642265 Color: 3
Size: 179187 Color: 4
Size: 178536 Color: 13

Bin 1341: 13 of cap free
Amount of items: 2
Items: 
Size: 500132 Color: 2
Size: 499856 Color: 7

Bin 1342: 13 of cap free
Amount of items: 3
Items: 
Size: 368734 Color: 7
Size: 344640 Color: 14
Size: 286614 Color: 15

Bin 1343: 13 of cap free
Amount of items: 3
Items: 
Size: 504127 Color: 4
Size: 252492 Color: 14
Size: 243369 Color: 19

Bin 1344: 13 of cap free
Amount of items: 2
Items: 
Size: 516095 Color: 17
Size: 483893 Color: 3

Bin 1345: 13 of cap free
Amount of items: 2
Items: 
Size: 522246 Color: 11
Size: 477742 Color: 3

Bin 1346: 13 of cap free
Amount of items: 2
Items: 
Size: 531113 Color: 14
Size: 468875 Color: 9

Bin 1347: 13 of cap free
Amount of items: 2
Items: 
Size: 536002 Color: 6
Size: 463986 Color: 18

Bin 1348: 13 of cap free
Amount of items: 2
Items: 
Size: 541593 Color: 5
Size: 458395 Color: 0

Bin 1349: 13 of cap free
Amount of items: 2
Items: 
Size: 547552 Color: 3
Size: 452436 Color: 12

Bin 1350: 13 of cap free
Amount of items: 2
Items: 
Size: 551122 Color: 15
Size: 448866 Color: 18

Bin 1351: 13 of cap free
Amount of items: 2
Items: 
Size: 551218 Color: 3
Size: 448770 Color: 13

Bin 1352: 13 of cap free
Amount of items: 2
Items: 
Size: 553306 Color: 11
Size: 446682 Color: 18

Bin 1353: 13 of cap free
Amount of items: 2
Items: 
Size: 562718 Color: 10
Size: 437270 Color: 11

Bin 1354: 13 of cap free
Amount of items: 2
Items: 
Size: 565708 Color: 8
Size: 434280 Color: 0

Bin 1355: 13 of cap free
Amount of items: 2
Items: 
Size: 567736 Color: 10
Size: 432252 Color: 9

Bin 1356: 13 of cap free
Amount of items: 2
Items: 
Size: 572628 Color: 9
Size: 427360 Color: 3

Bin 1357: 13 of cap free
Amount of items: 2
Items: 
Size: 576293 Color: 1
Size: 423695 Color: 17

Bin 1358: 13 of cap free
Amount of items: 2
Items: 
Size: 576807 Color: 16
Size: 423181 Color: 5

Bin 1359: 13 of cap free
Amount of items: 2
Items: 
Size: 585883 Color: 6
Size: 414105 Color: 11

Bin 1360: 13 of cap free
Amount of items: 2
Items: 
Size: 591288 Color: 18
Size: 408700 Color: 14

Bin 1361: 13 of cap free
Amount of items: 2
Items: 
Size: 622069 Color: 15
Size: 377919 Color: 7

Bin 1362: 13 of cap free
Amount of items: 2
Items: 
Size: 639520 Color: 11
Size: 360468 Color: 13

Bin 1363: 13 of cap free
Amount of items: 2
Items: 
Size: 643990 Color: 2
Size: 355998 Color: 4

Bin 1364: 13 of cap free
Amount of items: 3
Items: 
Size: 647228 Color: 5
Size: 176456 Color: 8
Size: 176304 Color: 17

Bin 1365: 13 of cap free
Amount of items: 2
Items: 
Size: 668870 Color: 7
Size: 331118 Color: 12

Bin 1366: 13 of cap free
Amount of items: 2
Items: 
Size: 671566 Color: 9
Size: 328422 Color: 18

Bin 1367: 13 of cap free
Amount of items: 2
Items: 
Size: 694193 Color: 7
Size: 305795 Color: 9

Bin 1368: 13 of cap free
Amount of items: 2
Items: 
Size: 696168 Color: 8
Size: 303820 Color: 6

Bin 1369: 13 of cap free
Amount of items: 2
Items: 
Size: 697590 Color: 4
Size: 302398 Color: 9

Bin 1370: 13 of cap free
Amount of items: 2
Items: 
Size: 716196 Color: 13
Size: 283792 Color: 14

Bin 1371: 13 of cap free
Amount of items: 2
Items: 
Size: 718068 Color: 10
Size: 281920 Color: 17

Bin 1372: 13 of cap free
Amount of items: 2
Items: 
Size: 741667 Color: 18
Size: 258321 Color: 17

Bin 1373: 13 of cap free
Amount of items: 2
Items: 
Size: 746645 Color: 2
Size: 253343 Color: 12

Bin 1374: 13 of cap free
Amount of items: 2
Items: 
Size: 768732 Color: 9
Size: 231256 Color: 0

Bin 1375: 13 of cap free
Amount of items: 2
Items: 
Size: 799792 Color: 14
Size: 200196 Color: 5

Bin 1376: 14 of cap free
Amount of items: 3
Items: 
Size: 368983 Color: 5
Size: 337482 Color: 14
Size: 293522 Color: 0

Bin 1377: 14 of cap free
Amount of items: 2
Items: 
Size: 749928 Color: 0
Size: 250059 Color: 14

Bin 1378: 14 of cap free
Amount of items: 3
Items: 
Size: 660839 Color: 1
Size: 173606 Color: 12
Size: 165542 Color: 9

Bin 1379: 14 of cap free
Amount of items: 2
Items: 
Size: 739620 Color: 13
Size: 260367 Color: 10

Bin 1380: 14 of cap free
Amount of items: 3
Items: 
Size: 376298 Color: 9
Size: 325238 Color: 7
Size: 298451 Color: 11

Bin 1381: 14 of cap free
Amount of items: 2
Items: 
Size: 616536 Color: 7
Size: 383451 Color: 11

Bin 1382: 14 of cap free
Amount of items: 3
Items: 
Size: 501191 Color: 6
Size: 265632 Color: 18
Size: 233164 Color: 16

Bin 1383: 14 of cap free
Amount of items: 2
Items: 
Size: 500255 Color: 8
Size: 499732 Color: 4

Bin 1384: 14 of cap free
Amount of items: 2
Items: 
Size: 629087 Color: 18
Size: 370900 Color: 1

Bin 1385: 14 of cap free
Amount of items: 3
Items: 
Size: 355815 Color: 6
Size: 344361 Color: 13
Size: 299811 Color: 11

Bin 1386: 14 of cap free
Amount of items: 3
Items: 
Size: 438265 Color: 9
Size: 281185 Color: 18
Size: 280537 Color: 1

Bin 1387: 14 of cap free
Amount of items: 2
Items: 
Size: 514529 Color: 0
Size: 485458 Color: 19

Bin 1388: 14 of cap free
Amount of items: 2
Items: 
Size: 522300 Color: 14
Size: 477687 Color: 4

Bin 1389: 14 of cap free
Amount of items: 2
Items: 
Size: 524339 Color: 14
Size: 475648 Color: 11

Bin 1390: 14 of cap free
Amount of items: 2
Items: 
Size: 527065 Color: 9
Size: 472922 Color: 5

Bin 1391: 14 of cap free
Amount of items: 2
Items: 
Size: 538952 Color: 19
Size: 461035 Color: 8

Bin 1392: 14 of cap free
Amount of items: 2
Items: 
Size: 547242 Color: 4
Size: 452745 Color: 17

Bin 1393: 14 of cap free
Amount of items: 2
Items: 
Size: 554332 Color: 10
Size: 445655 Color: 2

Bin 1394: 14 of cap free
Amount of items: 2
Items: 
Size: 557033 Color: 19
Size: 442954 Color: 4

Bin 1395: 14 of cap free
Amount of items: 2
Items: 
Size: 562435 Color: 8
Size: 437552 Color: 13

Bin 1396: 14 of cap free
Amount of items: 2
Items: 
Size: 578118 Color: 3
Size: 421869 Color: 18

Bin 1397: 14 of cap free
Amount of items: 2
Items: 
Size: 592678 Color: 7
Size: 407309 Color: 18

Bin 1398: 14 of cap free
Amount of items: 2
Items: 
Size: 593808 Color: 17
Size: 406179 Color: 4

Bin 1399: 14 of cap free
Amount of items: 2
Items: 
Size: 596536 Color: 11
Size: 403451 Color: 1

Bin 1400: 14 of cap free
Amount of items: 2
Items: 
Size: 621642 Color: 13
Size: 378345 Color: 9

Bin 1401: 14 of cap free
Amount of items: 2
Items: 
Size: 636438 Color: 14
Size: 363549 Color: 9

Bin 1402: 14 of cap free
Amount of items: 2
Items: 
Size: 649547 Color: 12
Size: 350440 Color: 11

Bin 1403: 14 of cap free
Amount of items: 2
Items: 
Size: 652866 Color: 12
Size: 347121 Color: 4

Bin 1404: 14 of cap free
Amount of items: 2
Items: 
Size: 657090 Color: 9
Size: 342897 Color: 10

Bin 1405: 14 of cap free
Amount of items: 2
Items: 
Size: 695398 Color: 12
Size: 304589 Color: 7

Bin 1406: 14 of cap free
Amount of items: 2
Items: 
Size: 696969 Color: 9
Size: 303018 Color: 1

Bin 1407: 14 of cap free
Amount of items: 2
Items: 
Size: 718920 Color: 2
Size: 281067 Color: 10

Bin 1408: 14 of cap free
Amount of items: 2
Items: 
Size: 760003 Color: 12
Size: 239984 Color: 6

Bin 1409: 14 of cap free
Amount of items: 2
Items: 
Size: 775281 Color: 15
Size: 224706 Color: 2

Bin 1410: 15 of cap free
Amount of items: 2
Items: 
Size: 726635 Color: 5
Size: 273351 Color: 10

Bin 1411: 15 of cap free
Amount of items: 3
Items: 
Size: 702220 Color: 11
Size: 148884 Color: 13
Size: 148882 Color: 13

Bin 1412: 15 of cap free
Amount of items: 3
Items: 
Size: 350721 Color: 17
Size: 346342 Color: 1
Size: 302923 Color: 17

Bin 1413: 15 of cap free
Amount of items: 2
Items: 
Size: 688613 Color: 16
Size: 311373 Color: 14

Bin 1414: 15 of cap free
Amount of items: 2
Items: 
Size: 706611 Color: 4
Size: 293375 Color: 8

Bin 1415: 15 of cap free
Amount of items: 2
Items: 
Size: 779739 Color: 9
Size: 220247 Color: 13

Bin 1416: 15 of cap free
Amount of items: 3
Items: 
Size: 683275 Color: 3
Size: 159250 Color: 16
Size: 157461 Color: 17

Bin 1417: 15 of cap free
Amount of items: 2
Items: 
Size: 724186 Color: 13
Size: 275800 Color: 11

Bin 1418: 15 of cap free
Amount of items: 2
Items: 
Size: 759780 Color: 19
Size: 240206 Color: 6

Bin 1419: 15 of cap free
Amount of items: 2
Items: 
Size: 686853 Color: 11
Size: 313133 Color: 1

Bin 1420: 15 of cap free
Amount of items: 2
Items: 
Size: 774993 Color: 3
Size: 224993 Color: 12

Bin 1421: 15 of cap free
Amount of items: 3
Items: 
Size: 363745 Color: 7
Size: 356141 Color: 13
Size: 280100 Color: 9

Bin 1422: 15 of cap free
Amount of items: 2
Items: 
Size: 556026 Color: 12
Size: 443960 Color: 6

Bin 1423: 15 of cap free
Amount of items: 3
Items: 
Size: 409956 Color: 3
Size: 300651 Color: 18
Size: 289379 Color: 7

Bin 1424: 15 of cap free
Amount of items: 2
Items: 
Size: 712996 Color: 3
Size: 286990 Color: 1

Bin 1425: 15 of cap free
Amount of items: 2
Items: 
Size: 643780 Color: 19
Size: 356206 Color: 12

Bin 1426: 15 of cap free
Amount of items: 2
Items: 
Size: 754721 Color: 14
Size: 245265 Color: 7

Bin 1427: 15 of cap free
Amount of items: 2
Items: 
Size: 560346 Color: 17
Size: 439640 Color: 1

Bin 1428: 15 of cap free
Amount of items: 2
Items: 
Size: 519166 Color: 9
Size: 480820 Color: 13

Bin 1429: 15 of cap free
Amount of items: 2
Items: 
Size: 530183 Color: 5
Size: 469803 Color: 3

Bin 1430: 15 of cap free
Amount of items: 2
Items: 
Size: 544582 Color: 3
Size: 455404 Color: 14

Bin 1431: 15 of cap free
Amount of items: 2
Items: 
Size: 548945 Color: 0
Size: 451041 Color: 10

Bin 1432: 15 of cap free
Amount of items: 2
Items: 
Size: 571255 Color: 5
Size: 428731 Color: 2

Bin 1433: 15 of cap free
Amount of items: 2
Items: 
Size: 593268 Color: 2
Size: 406718 Color: 4

Bin 1434: 15 of cap free
Amount of items: 2
Items: 
Size: 592836 Color: 1
Size: 407150 Color: 11

Bin 1435: 15 of cap free
Amount of items: 2
Items: 
Size: 607417 Color: 18
Size: 392569 Color: 19

Bin 1436: 15 of cap free
Amount of items: 3
Items: 
Size: 617883 Color: 3
Size: 191069 Color: 16
Size: 191034 Color: 11

Bin 1437: 15 of cap free
Amount of items: 2
Items: 
Size: 627905 Color: 15
Size: 372081 Color: 10

Bin 1438: 15 of cap free
Amount of items: 2
Items: 
Size: 650985 Color: 3
Size: 349001 Color: 5

Bin 1439: 15 of cap free
Amount of items: 3
Items: 
Size: 664525 Color: 16
Size: 167733 Color: 15
Size: 167728 Color: 7

Bin 1440: 15 of cap free
Amount of items: 2
Items: 
Size: 665908 Color: 14
Size: 334078 Color: 2

Bin 1441: 15 of cap free
Amount of items: 2
Items: 
Size: 666099 Color: 13
Size: 333887 Color: 0

Bin 1442: 15 of cap free
Amount of items: 2
Items: 
Size: 738181 Color: 0
Size: 261805 Color: 18

Bin 1443: 15 of cap free
Amount of items: 2
Items: 
Size: 756021 Color: 16
Size: 243965 Color: 11

Bin 1444: 15 of cap free
Amount of items: 2
Items: 
Size: 756644 Color: 1
Size: 243342 Color: 5

Bin 1445: 16 of cap free
Amount of items: 2
Items: 
Size: 710475 Color: 5
Size: 289510 Color: 12

Bin 1446: 16 of cap free
Amount of items: 3
Items: 
Size: 646124 Color: 18
Size: 198767 Color: 6
Size: 155094 Color: 18

Bin 1447: 16 of cap free
Amount of items: 2
Items: 
Size: 679990 Color: 10
Size: 319995 Color: 11

Bin 1448: 16 of cap free
Amount of items: 3
Items: 
Size: 705793 Color: 12
Size: 147384 Color: 6
Size: 146808 Color: 4

Bin 1449: 16 of cap free
Amount of items: 3
Items: 
Size: 698027 Color: 12
Size: 151187 Color: 12
Size: 150771 Color: 11

Bin 1450: 16 of cap free
Amount of items: 2
Items: 
Size: 691072 Color: 7
Size: 308913 Color: 9

Bin 1451: 16 of cap free
Amount of items: 2
Items: 
Size: 704671 Color: 5
Size: 295314 Color: 9

Bin 1452: 16 of cap free
Amount of items: 2
Items: 
Size: 518528 Color: 6
Size: 481457 Color: 16

Bin 1453: 16 of cap free
Amount of items: 3
Items: 
Size: 361297 Color: 8
Size: 336830 Color: 6
Size: 301858 Color: 12

Bin 1454: 16 of cap free
Amount of items: 3
Items: 
Size: 369885 Color: 11
Size: 344562 Color: 14
Size: 285538 Color: 4

Bin 1455: 16 of cap free
Amount of items: 2
Items: 
Size: 516821 Color: 1
Size: 483164 Color: 0

Bin 1456: 16 of cap free
Amount of items: 2
Items: 
Size: 533624 Color: 4
Size: 466361 Color: 18

Bin 1457: 16 of cap free
Amount of items: 2
Items: 
Size: 534526 Color: 17
Size: 465459 Color: 9

Bin 1458: 16 of cap free
Amount of items: 2
Items: 
Size: 539418 Color: 17
Size: 460567 Color: 3

Bin 1459: 16 of cap free
Amount of items: 2
Items: 
Size: 543450 Color: 2
Size: 456535 Color: 7

Bin 1460: 16 of cap free
Amount of items: 2
Items: 
Size: 551926 Color: 16
Size: 448059 Color: 17

Bin 1461: 16 of cap free
Amount of items: 2
Items: 
Size: 555388 Color: 11
Size: 444597 Color: 17

Bin 1462: 16 of cap free
Amount of items: 2
Items: 
Size: 559647 Color: 7
Size: 440338 Color: 8

Bin 1463: 16 of cap free
Amount of items: 2
Items: 
Size: 567285 Color: 1
Size: 432700 Color: 16

Bin 1464: 16 of cap free
Amount of items: 2
Items: 
Size: 580553 Color: 17
Size: 419432 Color: 15

Bin 1465: 16 of cap free
Amount of items: 2
Items: 
Size: 623135 Color: 15
Size: 376850 Color: 2

Bin 1466: 16 of cap free
Amount of items: 2
Items: 
Size: 632335 Color: 3
Size: 367650 Color: 16

Bin 1467: 16 of cap free
Amount of items: 2
Items: 
Size: 654813 Color: 4
Size: 345172 Color: 9

Bin 1468: 16 of cap free
Amount of items: 2
Items: 
Size: 657496 Color: 7
Size: 342489 Color: 10

Bin 1469: 16 of cap free
Amount of items: 2
Items: 
Size: 662010 Color: 5
Size: 337975 Color: 3

Bin 1470: 16 of cap free
Amount of items: 2
Items: 
Size: 670266 Color: 10
Size: 329719 Color: 17

Bin 1471: 16 of cap free
Amount of items: 2
Items: 
Size: 673779 Color: 13
Size: 326206 Color: 9

Bin 1472: 16 of cap free
Amount of items: 2
Items: 
Size: 689124 Color: 5
Size: 310861 Color: 9

Bin 1473: 16 of cap free
Amount of items: 2
Items: 
Size: 691396 Color: 4
Size: 308589 Color: 8

Bin 1474: 16 of cap free
Amount of items: 2
Items: 
Size: 711685 Color: 18
Size: 288300 Color: 1

Bin 1475: 16 of cap free
Amount of items: 2
Items: 
Size: 737212 Color: 7
Size: 262773 Color: 13

Bin 1476: 16 of cap free
Amount of items: 2
Items: 
Size: 751607 Color: 1
Size: 248378 Color: 8

Bin 1477: 16 of cap free
Amount of items: 2
Items: 
Size: 753037 Color: 15
Size: 246948 Color: 14

Bin 1478: 16 of cap free
Amount of items: 2
Items: 
Size: 758132 Color: 1
Size: 241853 Color: 0

Bin 1479: 16 of cap free
Amount of items: 2
Items: 
Size: 768640 Color: 14
Size: 231345 Color: 19

Bin 1480: 17 of cap free
Amount of items: 3
Items: 
Size: 504362 Color: 4
Size: 330047 Color: 16
Size: 165575 Color: 19

Bin 1481: 17 of cap free
Amount of items: 3
Items: 
Size: 712135 Color: 8
Size: 152186 Color: 9
Size: 135663 Color: 9

Bin 1482: 17 of cap free
Amount of items: 2
Items: 
Size: 773189 Color: 16
Size: 226795 Color: 4

Bin 1483: 17 of cap free
Amount of items: 3
Items: 
Size: 535578 Color: 11
Size: 241410 Color: 0
Size: 222996 Color: 9

Bin 1484: 17 of cap free
Amount of items: 3
Items: 
Size: 668140 Color: 10
Size: 167071 Color: 11
Size: 164773 Color: 19

Bin 1485: 17 of cap free
Amount of items: 3
Items: 
Size: 508076 Color: 3
Size: 257329 Color: 1
Size: 234579 Color: 9

Bin 1486: 17 of cap free
Amount of items: 2
Items: 
Size: 760068 Color: 18
Size: 239916 Color: 19

Bin 1487: 17 of cap free
Amount of items: 2
Items: 
Size: 608423 Color: 2
Size: 391561 Color: 1

Bin 1488: 17 of cap free
Amount of items: 2
Items: 
Size: 693509 Color: 6
Size: 306475 Color: 13

Bin 1489: 17 of cap free
Amount of items: 2
Items: 
Size: 718535 Color: 7
Size: 281449 Color: 1

Bin 1490: 17 of cap free
Amount of items: 2
Items: 
Size: 694750 Color: 1
Size: 305234 Color: 14

Bin 1491: 17 of cap free
Amount of items: 2
Items: 
Size: 638588 Color: 7
Size: 361396 Color: 16

Bin 1492: 17 of cap free
Amount of items: 3
Items: 
Size: 465708 Color: 11
Size: 278600 Color: 8
Size: 255676 Color: 13

Bin 1493: 17 of cap free
Amount of items: 3
Items: 
Size: 355713 Color: 16
Size: 344626 Color: 13
Size: 299645 Color: 12

Bin 1494: 17 of cap free
Amount of items: 2
Items: 
Size: 515374 Color: 9
Size: 484610 Color: 8

Bin 1495: 17 of cap free
Amount of items: 2
Items: 
Size: 545864 Color: 11
Size: 454120 Color: 2

Bin 1496: 17 of cap free
Amount of items: 2
Items: 
Size: 550455 Color: 5
Size: 449529 Color: 7

Bin 1497: 17 of cap free
Amount of items: 2
Items: 
Size: 555618 Color: 11
Size: 444366 Color: 15

Bin 1498: 17 of cap free
Amount of items: 2
Items: 
Size: 556546 Color: 2
Size: 443438 Color: 3

Bin 1499: 17 of cap free
Amount of items: 2
Items: 
Size: 561270 Color: 19
Size: 438714 Color: 9

Bin 1500: 17 of cap free
Amount of items: 2
Items: 
Size: 562298 Color: 19
Size: 437686 Color: 8

Bin 1501: 17 of cap free
Amount of items: 2
Items: 
Size: 569352 Color: 7
Size: 430632 Color: 9

Bin 1502: 17 of cap free
Amount of items: 2
Items: 
Size: 575786 Color: 18
Size: 424198 Color: 17

Bin 1503: 17 of cap free
Amount of items: 2
Items: 
Size: 580343 Color: 13
Size: 419641 Color: 14

Bin 1504: 17 of cap free
Amount of items: 2
Items: 
Size: 595021 Color: 19
Size: 404963 Color: 16

Bin 1505: 17 of cap free
Amount of items: 2
Items: 
Size: 599416 Color: 4
Size: 400568 Color: 10

Bin 1506: 17 of cap free
Amount of items: 2
Items: 
Size: 627971 Color: 15
Size: 372013 Color: 3

Bin 1507: 17 of cap free
Amount of items: 2
Items: 
Size: 635297 Color: 10
Size: 364687 Color: 11

Bin 1508: 17 of cap free
Amount of items: 2
Items: 
Size: 645787 Color: 10
Size: 354197 Color: 4

Bin 1509: 17 of cap free
Amount of items: 2
Items: 
Size: 651847 Color: 3
Size: 348137 Color: 7

Bin 1510: 17 of cap free
Amount of items: 2
Items: 
Size: 657749 Color: 4
Size: 342235 Color: 16

Bin 1511: 17 of cap free
Amount of items: 2
Items: 
Size: 663506 Color: 2
Size: 336478 Color: 14

Bin 1512: 17 of cap free
Amount of items: 2
Items: 
Size: 709936 Color: 17
Size: 290048 Color: 3

Bin 1513: 17 of cap free
Amount of items: 2
Items: 
Size: 741375 Color: 7
Size: 258609 Color: 2

Bin 1514: 17 of cap free
Amount of items: 2
Items: 
Size: 747016 Color: 8
Size: 252968 Color: 2

Bin 1515: 17 of cap free
Amount of items: 2
Items: 
Size: 779229 Color: 4
Size: 220755 Color: 15

Bin 1516: 18 of cap free
Amount of items: 3
Items: 
Size: 782239 Color: 11
Size: 116276 Color: 18
Size: 101468 Color: 5

Bin 1517: 18 of cap free
Amount of items: 2
Items: 
Size: 686234 Color: 11
Size: 313749 Color: 3

Bin 1518: 18 of cap free
Amount of items: 3
Items: 
Size: 764265 Color: 11
Size: 118204 Color: 15
Size: 117514 Color: 11

Bin 1519: 18 of cap free
Amount of items: 3
Items: 
Size: 659598 Color: 9
Size: 170932 Color: 4
Size: 169453 Color: 2

Bin 1520: 18 of cap free
Amount of items: 2
Items: 
Size: 676961 Color: 15
Size: 323022 Color: 11

Bin 1521: 18 of cap free
Amount of items: 2
Items: 
Size: 553093 Color: 18
Size: 446890 Color: 0

Bin 1522: 18 of cap free
Amount of items: 3
Items: 
Size: 549622 Color: 0
Size: 270043 Color: 3
Size: 180318 Color: 4

Bin 1523: 18 of cap free
Amount of items: 2
Items: 
Size: 670001 Color: 3
Size: 329982 Color: 15

Bin 1524: 18 of cap free
Amount of items: 3
Items: 
Size: 716920 Color: 4
Size: 141700 Color: 1
Size: 141363 Color: 17

Bin 1525: 18 of cap free
Amount of items: 3
Items: 
Size: 663299 Color: 4
Size: 168816 Color: 15
Size: 167868 Color: 14

Bin 1526: 18 of cap free
Amount of items: 2
Items: 
Size: 589631 Color: 9
Size: 410352 Color: 4

Bin 1527: 18 of cap free
Amount of items: 3
Items: 
Size: 555099 Color: 7
Size: 228131 Color: 0
Size: 216753 Color: 10

Bin 1528: 18 of cap free
Amount of items: 3
Items: 
Size: 785297 Color: 13
Size: 113437 Color: 12
Size: 101249 Color: 10

Bin 1529: 18 of cap free
Amount of items: 2
Items: 
Size: 588919 Color: 1
Size: 411064 Color: 19

Bin 1530: 18 of cap free
Amount of items: 3
Items: 
Size: 369589 Color: 9
Size: 323150 Color: 6
Size: 307244 Color: 15

Bin 1531: 18 of cap free
Amount of items: 3
Items: 
Size: 370403 Color: 8
Size: 345405 Color: 11
Size: 284175 Color: 15

Bin 1532: 18 of cap free
Amount of items: 3
Items: 
Size: 455472 Color: 4
Size: 278144 Color: 6
Size: 266367 Color: 19

Bin 1533: 18 of cap free
Amount of items: 2
Items: 
Size: 529769 Color: 3
Size: 470214 Color: 6

Bin 1534: 18 of cap free
Amount of items: 2
Items: 
Size: 534610 Color: 11
Size: 465373 Color: 3

Bin 1535: 18 of cap free
Amount of items: 2
Items: 
Size: 538740 Color: 19
Size: 461243 Color: 18

Bin 1536: 18 of cap free
Amount of items: 2
Items: 
Size: 545991 Color: 6
Size: 453992 Color: 19

Bin 1537: 18 of cap free
Amount of items: 2
Items: 
Size: 554490 Color: 13
Size: 445493 Color: 2

Bin 1538: 18 of cap free
Amount of items: 2
Items: 
Size: 564003 Color: 1
Size: 435980 Color: 6

Bin 1539: 18 of cap free
Amount of items: 2
Items: 
Size: 568984 Color: 16
Size: 430999 Color: 17

Bin 1540: 18 of cap free
Amount of items: 2
Items: 
Size: 570802 Color: 18
Size: 429181 Color: 13

Bin 1541: 18 of cap free
Amount of items: 2
Items: 
Size: 577462 Color: 7
Size: 422521 Color: 16

Bin 1542: 18 of cap free
Amount of items: 2
Items: 
Size: 585697 Color: 2
Size: 414286 Color: 17

Bin 1543: 18 of cap free
Amount of items: 2
Items: 
Size: 588685 Color: 4
Size: 411298 Color: 10

Bin 1544: 18 of cap free
Amount of items: 2
Items: 
Size: 592301 Color: 15
Size: 407682 Color: 1

Bin 1545: 18 of cap free
Amount of items: 2
Items: 
Size: 593347 Color: 4
Size: 406636 Color: 15

Bin 1546: 18 of cap free
Amount of items: 2
Items: 
Size: 593803 Color: 7
Size: 406180 Color: 17

Bin 1547: 18 of cap free
Amount of items: 2
Items: 
Size: 594292 Color: 7
Size: 405691 Color: 9

Bin 1548: 18 of cap free
Amount of items: 2
Items: 
Size: 639576 Color: 18
Size: 360407 Color: 19

Bin 1549: 18 of cap free
Amount of items: 3
Items: 
Size: 645092 Color: 7
Size: 177580 Color: 15
Size: 177311 Color: 12

Bin 1550: 18 of cap free
Amount of items: 2
Items: 
Size: 646500 Color: 12
Size: 353483 Color: 15

Bin 1551: 18 of cap free
Amount of items: 2
Items: 
Size: 657794 Color: 18
Size: 342189 Color: 9

Bin 1552: 18 of cap free
Amount of items: 2
Items: 
Size: 687040 Color: 9
Size: 312943 Color: 19

Bin 1553: 18 of cap free
Amount of items: 2
Items: 
Size: 689433 Color: 15
Size: 310550 Color: 13

Bin 1554: 18 of cap free
Amount of items: 2
Items: 
Size: 703254 Color: 0
Size: 296729 Color: 15

Bin 1555: 18 of cap free
Amount of items: 2
Items: 
Size: 708658 Color: 7
Size: 291325 Color: 6

Bin 1556: 18 of cap free
Amount of items: 2
Items: 
Size: 716469 Color: 11
Size: 283514 Color: 1

Bin 1557: 18 of cap free
Amount of items: 2
Items: 
Size: 717598 Color: 16
Size: 282385 Color: 9

Bin 1558: 18 of cap free
Amount of items: 2
Items: 
Size: 723080 Color: 3
Size: 276903 Color: 6

Bin 1559: 18 of cap free
Amount of items: 2
Items: 
Size: 726477 Color: 19
Size: 273506 Color: 5

Bin 1560: 18 of cap free
Amount of items: 2
Items: 
Size: 729016 Color: 6
Size: 270967 Color: 19

Bin 1561: 18 of cap free
Amount of items: 3
Items: 
Size: 731296 Color: 0
Size: 134691 Color: 14
Size: 133996 Color: 5

Bin 1562: 18 of cap free
Amount of items: 2
Items: 
Size: 740063 Color: 9
Size: 259920 Color: 10

Bin 1563: 18 of cap free
Amount of items: 2
Items: 
Size: 743263 Color: 13
Size: 256720 Color: 1

Bin 1564: 18 of cap free
Amount of items: 2
Items: 
Size: 776653 Color: 18
Size: 223330 Color: 3

Bin 1565: 18 of cap free
Amount of items: 2
Items: 
Size: 782199 Color: 18
Size: 217784 Color: 12

Bin 1566: 18 of cap free
Amount of items: 2
Items: 
Size: 788814 Color: 16
Size: 211169 Color: 10

Bin 1567: 18 of cap free
Amount of items: 2
Items: 
Size: 788972 Color: 12
Size: 211011 Color: 5

Bin 1568: 18 of cap free
Amount of items: 3
Items: 
Size: 792739 Color: 11
Size: 104116 Color: 3
Size: 103128 Color: 19

Bin 1569: 19 of cap free
Amount of items: 2
Items: 
Size: 698664 Color: 4
Size: 301318 Color: 2

Bin 1570: 19 of cap free
Amount of items: 2
Items: 
Size: 678976 Color: 11
Size: 321006 Color: 1

Bin 1571: 19 of cap free
Amount of items: 2
Items: 
Size: 664438 Color: 16
Size: 335544 Color: 0

Bin 1572: 19 of cap free
Amount of items: 3
Items: 
Size: 647793 Color: 16
Size: 176568 Color: 9
Size: 175621 Color: 16

Bin 1573: 19 of cap free
Amount of items: 3
Items: 
Size: 681514 Color: 10
Size: 159597 Color: 6
Size: 158871 Color: 0

Bin 1574: 19 of cap free
Amount of items: 3
Items: 
Size: 566961 Color: 7
Size: 288595 Color: 5
Size: 144426 Color: 5

Bin 1575: 19 of cap free
Amount of items: 2
Items: 
Size: 687137 Color: 15
Size: 312845 Color: 2

Bin 1576: 19 of cap free
Amount of items: 2
Items: 
Size: 690611 Color: 15
Size: 309371 Color: 11

Bin 1577: 19 of cap free
Amount of items: 2
Items: 
Size: 679294 Color: 7
Size: 320688 Color: 11

Bin 1578: 19 of cap free
Amount of items: 2
Items: 
Size: 672064 Color: 14
Size: 327918 Color: 11

Bin 1579: 19 of cap free
Amount of items: 2
Items: 
Size: 653294 Color: 18
Size: 346688 Color: 13

Bin 1580: 19 of cap free
Amount of items: 3
Items: 
Size: 728222 Color: 10
Size: 140599 Color: 11
Size: 131161 Color: 7

Bin 1581: 19 of cap free
Amount of items: 2
Items: 
Size: 705746 Color: 11
Size: 294236 Color: 1

Bin 1582: 19 of cap free
Amount of items: 2
Items: 
Size: 668583 Color: 11
Size: 331399 Color: 3

Bin 1583: 19 of cap free
Amount of items: 2
Items: 
Size: 561301 Color: 19
Size: 438681 Color: 15

Bin 1584: 19 of cap free
Amount of items: 3
Items: 
Size: 458976 Color: 10
Size: 272564 Color: 10
Size: 268442 Color: 2

Bin 1585: 19 of cap free
Amount of items: 2
Items: 
Size: 501746 Color: 7
Size: 498236 Color: 10

Bin 1586: 19 of cap free
Amount of items: 2
Items: 
Size: 521976 Color: 5
Size: 478006 Color: 2

Bin 1587: 19 of cap free
Amount of items: 2
Items: 
Size: 557804 Color: 13
Size: 442178 Color: 19

Bin 1588: 19 of cap free
Amount of items: 2
Items: 
Size: 558015 Color: 18
Size: 441967 Color: 0

Bin 1589: 19 of cap free
Amount of items: 2
Items: 
Size: 561264 Color: 16
Size: 438718 Color: 19

Bin 1590: 19 of cap free
Amount of items: 2
Items: 
Size: 567688 Color: 6
Size: 432294 Color: 17

Bin 1591: 19 of cap free
Amount of items: 2
Items: 
Size: 567796 Color: 4
Size: 432186 Color: 8

Bin 1592: 19 of cap free
Amount of items: 2
Items: 
Size: 573423 Color: 10
Size: 426559 Color: 18

Bin 1593: 19 of cap free
Amount of items: 2
Items: 
Size: 598023 Color: 18
Size: 401959 Color: 17

Bin 1594: 19 of cap free
Amount of items: 3
Items: 
Size: 625135 Color: 8
Size: 187749 Color: 13
Size: 187098 Color: 0

Bin 1595: 19 of cap free
Amount of items: 2
Items: 
Size: 641528 Color: 4
Size: 358454 Color: 6

Bin 1596: 19 of cap free
Amount of items: 2
Items: 
Size: 699227 Color: 5
Size: 300755 Color: 6

Bin 1597: 19 of cap free
Amount of items: 2
Items: 
Size: 743087 Color: 1
Size: 256895 Color: 8

Bin 1598: 19 of cap free
Amount of items: 2
Items: 
Size: 762572 Color: 9
Size: 237410 Color: 17

Bin 1599: 19 of cap free
Amount of items: 2
Items: 
Size: 765098 Color: 17
Size: 234884 Color: 13

Bin 1600: 19 of cap free
Amount of items: 2
Items: 
Size: 786287 Color: 4
Size: 213695 Color: 15

Bin 1601: 19 of cap free
Amount of items: 2
Items: 
Size: 798198 Color: 6
Size: 201784 Color: 10

Bin 1602: 20 of cap free
Amount of items: 2
Items: 
Size: 695203 Color: 11
Size: 304778 Color: 2

Bin 1603: 20 of cap free
Amount of items: 3
Items: 
Size: 372852 Color: 19
Size: 338910 Color: 0
Size: 288219 Color: 8

Bin 1604: 20 of cap free
Amount of items: 2
Items: 
Size: 605001 Color: 1
Size: 394980 Color: 17

Bin 1605: 20 of cap free
Amount of items: 3
Items: 
Size: 760962 Color: 8
Size: 122447 Color: 19
Size: 116572 Color: 17

Bin 1606: 20 of cap free
Amount of items: 3
Items: 
Size: 613054 Color: 1
Size: 193757 Color: 18
Size: 193170 Color: 0

Bin 1607: 20 of cap free
Amount of items: 3
Items: 
Size: 690333 Color: 8
Size: 154986 Color: 9
Size: 154662 Color: 5

Bin 1608: 20 of cap free
Amount of items: 3
Items: 
Size: 487623 Color: 0
Size: 264622 Color: 15
Size: 247736 Color: 5

Bin 1609: 20 of cap free
Amount of items: 2
Items: 
Size: 580938 Color: 4
Size: 419043 Color: 10

Bin 1610: 20 of cap free
Amount of items: 2
Items: 
Size: 697897 Color: 18
Size: 302084 Color: 17

Bin 1611: 20 of cap free
Amount of items: 2
Items: 
Size: 755242 Color: 16
Size: 244739 Color: 0

Bin 1612: 20 of cap free
Amount of items: 2
Items: 
Size: 730157 Color: 3
Size: 269824 Color: 11

Bin 1613: 20 of cap free
Amount of items: 2
Items: 
Size: 726987 Color: 1
Size: 272994 Color: 12

Bin 1614: 20 of cap free
Amount of items: 2
Items: 
Size: 556834 Color: 14
Size: 443147 Color: 16

Bin 1615: 20 of cap free
Amount of items: 3
Items: 
Size: 459110 Color: 2
Size: 272197 Color: 8
Size: 268674 Color: 1

Bin 1616: 20 of cap free
Amount of items: 3
Items: 
Size: 351253 Color: 4
Size: 330586 Color: 7
Size: 318142 Color: 1

Bin 1617: 20 of cap free
Amount of items: 2
Items: 
Size: 612606 Color: 16
Size: 387375 Color: 18

Bin 1618: 20 of cap free
Amount of items: 3
Items: 
Size: 389933 Color: 6
Size: 306641 Color: 8
Size: 303407 Color: 10

Bin 1619: 20 of cap free
Amount of items: 2
Items: 
Size: 517418 Color: 19
Size: 482563 Color: 2

Bin 1620: 20 of cap free
Amount of items: 2
Items: 
Size: 561467 Color: 0
Size: 438514 Color: 13

Bin 1621: 20 of cap free
Amount of items: 2
Items: 
Size: 562034 Color: 4
Size: 437947 Color: 7

Bin 1622: 20 of cap free
Amount of items: 2
Items: 
Size: 563859 Color: 3
Size: 436122 Color: 18

Bin 1623: 20 of cap free
Amount of items: 2
Items: 
Size: 565233 Color: 19
Size: 434748 Color: 1

Bin 1624: 20 of cap free
Amount of items: 2
Items: 
Size: 567642 Color: 2
Size: 432339 Color: 0

Bin 1625: 20 of cap free
Amount of items: 2
Items: 
Size: 594762 Color: 2
Size: 405219 Color: 13

Bin 1626: 20 of cap free
Amount of items: 2
Items: 
Size: 602385 Color: 5
Size: 397596 Color: 16

Bin 1627: 20 of cap free
Amount of items: 2
Items: 
Size: 607396 Color: 19
Size: 392585 Color: 0

Bin 1628: 20 of cap free
Amount of items: 2
Items: 
Size: 615761 Color: 19
Size: 384220 Color: 10

Bin 1629: 20 of cap free
Amount of items: 2
Items: 
Size: 641968 Color: 10
Size: 358013 Color: 16

Bin 1630: 20 of cap free
Amount of items: 2
Items: 
Size: 657380 Color: 0
Size: 342601 Color: 6

Bin 1631: 20 of cap free
Amount of items: 2
Items: 
Size: 660670 Color: 7
Size: 339311 Color: 17

Bin 1632: 20 of cap free
Amount of items: 2
Items: 
Size: 700762 Color: 6
Size: 299219 Color: 2

Bin 1633: 20 of cap free
Amount of items: 2
Items: 
Size: 711813 Color: 18
Size: 288168 Color: 13

Bin 1634: 20 of cap free
Amount of items: 2
Items: 
Size: 732748 Color: 17
Size: 267233 Color: 12

Bin 1635: 20 of cap free
Amount of items: 2
Items: 
Size: 754141 Color: 16
Size: 245840 Color: 13

Bin 1636: 20 of cap free
Amount of items: 2
Items: 
Size: 791188 Color: 6
Size: 208793 Color: 14

Bin 1637: 20 of cap free
Amount of items: 2
Items: 
Size: 794451 Color: 13
Size: 205530 Color: 14

Bin 1638: 21 of cap free
Amount of items: 3
Items: 
Size: 740172 Color: 16
Size: 130475 Color: 9
Size: 129333 Color: 18

Bin 1639: 21 of cap free
Amount of items: 2
Items: 
Size: 767469 Color: 4
Size: 232511 Color: 8

Bin 1640: 21 of cap free
Amount of items: 3
Items: 
Size: 592016 Color: 1
Size: 208427 Color: 1
Size: 199537 Color: 4

Bin 1641: 21 of cap free
Amount of items: 3
Items: 
Size: 384967 Color: 0
Size: 308608 Color: 3
Size: 306405 Color: 7

Bin 1642: 21 of cap free
Amount of items: 3
Items: 
Size: 510238 Color: 12
Size: 255775 Color: 3
Size: 233967 Color: 12

Bin 1643: 21 of cap free
Amount of items: 2
Items: 
Size: 567455 Color: 19
Size: 432525 Color: 3

Bin 1644: 21 of cap free
Amount of items: 2
Items: 
Size: 709656 Color: 11
Size: 290324 Color: 7

Bin 1645: 21 of cap free
Amount of items: 3
Items: 
Size: 664650 Color: 7
Size: 167674 Color: 16
Size: 167656 Color: 14

Bin 1646: 21 of cap free
Amount of items: 3
Items: 
Size: 687927 Color: 0
Size: 156120 Color: 3
Size: 155933 Color: 2

Bin 1647: 21 of cap free
Amount of items: 3
Items: 
Size: 621183 Color: 18
Size: 189608 Color: 9
Size: 189189 Color: 12

Bin 1648: 21 of cap free
Amount of items: 2
Items: 
Size: 614681 Color: 7
Size: 385299 Color: 5

Bin 1649: 21 of cap free
Amount of items: 3
Items: 
Size: 668699 Color: 16
Size: 166309 Color: 7
Size: 164972 Color: 19

Bin 1650: 21 of cap free
Amount of items: 2
Items: 
Size: 566655 Color: 3
Size: 433325 Color: 6

Bin 1651: 21 of cap free
Amount of items: 2
Items: 
Size: 569875 Color: 8
Size: 430105 Color: 0

Bin 1652: 21 of cap free
Amount of items: 2
Items: 
Size: 775026 Color: 16
Size: 224954 Color: 7

Bin 1653: 21 of cap free
Amount of items: 3
Items: 
Size: 352886 Color: 10
Size: 336195 Color: 2
Size: 310899 Color: 5

Bin 1654: 21 of cap free
Amount of items: 2
Items: 
Size: 501388 Color: 10
Size: 498592 Color: 13

Bin 1655: 21 of cap free
Amount of items: 2
Items: 
Size: 504802 Color: 15
Size: 495178 Color: 12

Bin 1656: 21 of cap free
Amount of items: 2
Items: 
Size: 507660 Color: 1
Size: 492320 Color: 17

Bin 1657: 21 of cap free
Amount of items: 2
Items: 
Size: 535299 Color: 13
Size: 464681 Color: 11

Bin 1658: 21 of cap free
Amount of items: 2
Items: 
Size: 545288 Color: 11
Size: 454692 Color: 18

Bin 1659: 21 of cap free
Amount of items: 2
Items: 
Size: 546951 Color: 5
Size: 453029 Color: 10

Bin 1660: 21 of cap free
Amount of items: 2
Items: 
Size: 567397 Color: 0
Size: 432583 Color: 15

Bin 1661: 21 of cap free
Amount of items: 2
Items: 
Size: 580465 Color: 4
Size: 419515 Color: 2

Bin 1662: 21 of cap free
Amount of items: 2
Items: 
Size: 591585 Color: 13
Size: 408395 Color: 14

Bin 1663: 21 of cap free
Amount of items: 2
Items: 
Size: 594237 Color: 4
Size: 405743 Color: 10

Bin 1664: 21 of cap free
Amount of items: 2
Items: 
Size: 598167 Color: 12
Size: 401813 Color: 7

Bin 1665: 21 of cap free
Amount of items: 2
Items: 
Size: 598245 Color: 3
Size: 401735 Color: 16

Bin 1666: 21 of cap free
Amount of items: 2
Items: 
Size: 604897 Color: 19
Size: 395083 Color: 16

Bin 1667: 21 of cap free
Amount of items: 2
Items: 
Size: 626882 Color: 9
Size: 373098 Color: 18

Bin 1668: 21 of cap free
Amount of items: 2
Items: 
Size: 632054 Color: 0
Size: 367926 Color: 17

Bin 1669: 21 of cap free
Amount of items: 2
Items: 
Size: 633358 Color: 14
Size: 366622 Color: 6

Bin 1670: 21 of cap free
Amount of items: 2
Items: 
Size: 633393 Color: 8
Size: 366587 Color: 18

Bin 1671: 21 of cap free
Amount of items: 2
Items: 
Size: 644839 Color: 14
Size: 355141 Color: 9

Bin 1672: 21 of cap free
Amount of items: 2
Items: 
Size: 654549 Color: 5
Size: 345431 Color: 12

Bin 1673: 21 of cap free
Amount of items: 2
Items: 
Size: 679870 Color: 6
Size: 320110 Color: 0

Bin 1674: 21 of cap free
Amount of items: 2
Items: 
Size: 684307 Color: 19
Size: 315673 Color: 15

Bin 1675: 21 of cap free
Amount of items: 2
Items: 
Size: 698294 Color: 4
Size: 301686 Color: 17

Bin 1676: 21 of cap free
Amount of items: 2
Items: 
Size: 698495 Color: 6
Size: 301485 Color: 4

Bin 1677: 21 of cap free
Amount of items: 2
Items: 
Size: 712211 Color: 1
Size: 287769 Color: 18

Bin 1678: 21 of cap free
Amount of items: 2
Items: 
Size: 722919 Color: 16
Size: 277061 Color: 8

Bin 1679: 21 of cap free
Amount of items: 2
Items: 
Size: 745899 Color: 9
Size: 254081 Color: 17

Bin 1680: 21 of cap free
Amount of items: 2
Items: 
Size: 757022 Color: 8
Size: 242958 Color: 11

Bin 1681: 21 of cap free
Amount of items: 2
Items: 
Size: 768850 Color: 14
Size: 231130 Color: 17

Bin 1682: 22 of cap free
Amount of items: 3
Items: 
Size: 483914 Color: 1
Size: 268075 Color: 9
Size: 247990 Color: 10

Bin 1683: 22 of cap free
Amount of items: 2
Items: 
Size: 677177 Color: 16
Size: 322802 Color: 15

Bin 1684: 22 of cap free
Amount of items: 3
Items: 
Size: 476787 Color: 10
Size: 270625 Color: 1
Size: 252567 Color: 1

Bin 1685: 22 of cap free
Amount of items: 2
Items: 
Size: 699574 Color: 13
Size: 300405 Color: 18

Bin 1686: 22 of cap free
Amount of items: 3
Items: 
Size: 632164 Color: 8
Size: 185042 Color: 5
Size: 182773 Color: 3

Bin 1687: 22 of cap free
Amount of items: 2
Items: 
Size: 711610 Color: 15
Size: 288369 Color: 16

Bin 1688: 22 of cap free
Amount of items: 2
Items: 
Size: 647317 Color: 19
Size: 352662 Color: 1

Bin 1689: 22 of cap free
Amount of items: 2
Items: 
Size: 543080 Color: 16
Size: 456899 Color: 4

Bin 1690: 22 of cap free
Amount of items: 2
Items: 
Size: 648792 Color: 9
Size: 351187 Color: 12

Bin 1691: 22 of cap free
Amount of items: 2
Items: 
Size: 602360 Color: 12
Size: 397619 Color: 7

Bin 1692: 22 of cap free
Amount of items: 3
Items: 
Size: 371501 Color: 2
Size: 337084 Color: 2
Size: 291394 Color: 3

Bin 1693: 22 of cap free
Amount of items: 2
Items: 
Size: 547574 Color: 4
Size: 452405 Color: 15

Bin 1694: 22 of cap free
Amount of items: 3
Items: 
Size: 547716 Color: 4
Size: 226455 Color: 17
Size: 225808 Color: 0

Bin 1695: 22 of cap free
Amount of items: 2
Items: 
Size: 552588 Color: 12
Size: 447391 Color: 18

Bin 1696: 22 of cap free
Amount of items: 2
Items: 
Size: 570426 Color: 13
Size: 429553 Color: 10

Bin 1697: 22 of cap free
Amount of items: 2
Items: 
Size: 577868 Color: 4
Size: 422111 Color: 8

Bin 1698: 22 of cap free
Amount of items: 2
Items: 
Size: 589080 Color: 5
Size: 410899 Color: 0

Bin 1699: 22 of cap free
Amount of items: 2
Items: 
Size: 591406 Color: 2
Size: 408573 Color: 0

Bin 1700: 22 of cap free
Amount of items: 2
Items: 
Size: 608217 Color: 6
Size: 391762 Color: 14

Bin 1701: 22 of cap free
Amount of items: 2
Items: 
Size: 616385 Color: 10
Size: 383594 Color: 19

Bin 1702: 22 of cap free
Amount of items: 2
Items: 
Size: 617390 Color: 5
Size: 382589 Color: 11

Bin 1703: 22 of cap free
Amount of items: 2
Items: 
Size: 629176 Color: 14
Size: 370803 Color: 11

Bin 1704: 22 of cap free
Amount of items: 2
Items: 
Size: 631272 Color: 7
Size: 368707 Color: 18

Bin 1705: 22 of cap free
Amount of items: 2
Items: 
Size: 641281 Color: 8
Size: 358698 Color: 2

Bin 1706: 22 of cap free
Amount of items: 2
Items: 
Size: 684906 Color: 3
Size: 315073 Color: 18

Bin 1707: 22 of cap free
Amount of items: 3
Items: 
Size: 707259 Color: 15
Size: 146609 Color: 12
Size: 146111 Color: 15

Bin 1708: 22 of cap free
Amount of items: 2
Items: 
Size: 799030 Color: 8
Size: 200949 Color: 19

Bin 1709: 23 of cap free
Amount of items: 2
Items: 
Size: 629049 Color: 9
Size: 370929 Color: 14

Bin 1710: 23 of cap free
Amount of items: 4
Items: 
Size: 574714 Color: 1
Size: 199418 Color: 0
Size: 114678 Color: 14
Size: 111168 Color: 1

Bin 1711: 23 of cap free
Amount of items: 2
Items: 
Size: 690781 Color: 7
Size: 309197 Color: 11

Bin 1712: 23 of cap free
Amount of items: 2
Items: 
Size: 720744 Color: 13
Size: 279234 Color: 11

Bin 1713: 23 of cap free
Amount of items: 2
Items: 
Size: 712133 Color: 6
Size: 287845 Color: 18

Bin 1714: 23 of cap free
Amount of items: 3
Items: 
Size: 624582 Color: 0
Size: 187928 Color: 19
Size: 187468 Color: 13

Bin 1715: 23 of cap free
Amount of items: 3
Items: 
Size: 502628 Color: 15
Size: 263436 Color: 18
Size: 233914 Color: 3

Bin 1716: 23 of cap free
Amount of items: 2
Items: 
Size: 707147 Color: 7
Size: 292831 Color: 11

Bin 1717: 23 of cap free
Amount of items: 2
Items: 
Size: 774173 Color: 19
Size: 225805 Color: 6

Bin 1718: 23 of cap free
Amount of items: 2
Items: 
Size: 647001 Color: 8
Size: 352977 Color: 13

Bin 1719: 23 of cap free
Amount of items: 2
Items: 
Size: 518641 Color: 2
Size: 481337 Color: 17

Bin 1720: 23 of cap free
Amount of items: 2
Items: 
Size: 525009 Color: 12
Size: 474969 Color: 5

Bin 1721: 23 of cap free
Amount of items: 2
Items: 
Size: 528347 Color: 8
Size: 471631 Color: 11

Bin 1722: 23 of cap free
Amount of items: 2
Items: 
Size: 532554 Color: 1
Size: 467424 Color: 2

Bin 1723: 23 of cap free
Amount of items: 2
Items: 
Size: 573736 Color: 1
Size: 426242 Color: 2

Bin 1724: 23 of cap free
Amount of items: 2
Items: 
Size: 579285 Color: 5
Size: 420693 Color: 3

Bin 1725: 23 of cap free
Amount of items: 2
Items: 
Size: 580164 Color: 3
Size: 419814 Color: 9

Bin 1726: 23 of cap free
Amount of items: 2
Items: 
Size: 620950 Color: 17
Size: 379028 Color: 0

Bin 1727: 23 of cap free
Amount of items: 2
Items: 
Size: 641620 Color: 4
Size: 358358 Color: 18

Bin 1728: 23 of cap free
Amount of items: 2
Items: 
Size: 648455 Color: 6
Size: 351523 Color: 2

Bin 1729: 23 of cap free
Amount of items: 2
Items: 
Size: 689465 Color: 17
Size: 310513 Color: 2

Bin 1730: 23 of cap free
Amount of items: 2
Items: 
Size: 710852 Color: 0
Size: 289126 Color: 6

Bin 1731: 23 of cap free
Amount of items: 2
Items: 
Size: 750254 Color: 16
Size: 249724 Color: 15

Bin 1732: 23 of cap free
Amount of items: 2
Items: 
Size: 753635 Color: 3
Size: 246343 Color: 4

Bin 1733: 23 of cap free
Amount of items: 2
Items: 
Size: 756367 Color: 19
Size: 243611 Color: 18

Bin 1734: 23 of cap free
Amount of items: 2
Items: 
Size: 763436 Color: 15
Size: 236542 Color: 8

Bin 1735: 23 of cap free
Amount of items: 2
Items: 
Size: 767449 Color: 15
Size: 232529 Color: 11

Bin 1736: 23 of cap free
Amount of items: 2
Items: 
Size: 768924 Color: 14
Size: 231054 Color: 8

Bin 1737: 24 of cap free
Amount of items: 2
Items: 
Size: 740223 Color: 14
Size: 259754 Color: 0

Bin 1738: 24 of cap free
Amount of items: 3
Items: 
Size: 759780 Color: 3
Size: 120593 Color: 5
Size: 119604 Color: 4

Bin 1739: 24 of cap free
Amount of items: 2
Items: 
Size: 615360 Color: 11
Size: 384617 Color: 1

Bin 1740: 24 of cap free
Amount of items: 3
Items: 
Size: 581519 Color: 15
Size: 279796 Color: 18
Size: 138662 Color: 0

Bin 1741: 24 of cap free
Amount of items: 2
Items: 
Size: 693574 Color: 8
Size: 306403 Color: 14

Bin 1742: 24 of cap free
Amount of items: 3
Items: 
Size: 554678 Color: 0
Size: 303692 Color: 16
Size: 141607 Color: 10

Bin 1743: 24 of cap free
Amount of items: 3
Items: 
Size: 552420 Color: 5
Size: 250473 Color: 8
Size: 197084 Color: 2

Bin 1744: 24 of cap free
Amount of items: 2
Items: 
Size: 500616 Color: 12
Size: 499361 Color: 6

Bin 1745: 24 of cap free
Amount of items: 3
Items: 
Size: 367504 Color: 16
Size: 345759 Color: 1
Size: 286714 Color: 2

Bin 1746: 24 of cap free
Amount of items: 2
Items: 
Size: 500979 Color: 19
Size: 498998 Color: 2

Bin 1747: 24 of cap free
Amount of items: 2
Items: 
Size: 532412 Color: 6
Size: 467565 Color: 1

Bin 1748: 24 of cap free
Amount of items: 2
Items: 
Size: 533932 Color: 16
Size: 466045 Color: 4

Bin 1749: 24 of cap free
Amount of items: 2
Items: 
Size: 538156 Color: 2
Size: 461821 Color: 3

Bin 1750: 24 of cap free
Amount of items: 2
Items: 
Size: 565060 Color: 3
Size: 434917 Color: 14

Bin 1751: 24 of cap free
Amount of items: 2
Items: 
Size: 569433 Color: 19
Size: 430544 Color: 6

Bin 1752: 24 of cap free
Amount of items: 2
Items: 
Size: 572043 Color: 7
Size: 427934 Color: 2

Bin 1753: 24 of cap free
Amount of items: 2
Items: 
Size: 584446 Color: 5
Size: 415531 Color: 6

Bin 1754: 24 of cap free
Amount of items: 2
Items: 
Size: 592429 Color: 13
Size: 407548 Color: 6

Bin 1755: 24 of cap free
Amount of items: 2
Items: 
Size: 628451 Color: 13
Size: 371526 Color: 12

Bin 1756: 24 of cap free
Amount of items: 2
Items: 
Size: 639004 Color: 6
Size: 360973 Color: 7

Bin 1757: 24 of cap free
Amount of items: 2
Items: 
Size: 648358 Color: 7
Size: 351619 Color: 2

Bin 1758: 24 of cap free
Amount of items: 2
Items: 
Size: 666166 Color: 5
Size: 333811 Color: 13

Bin 1759: 24 of cap free
Amount of items: 2
Items: 
Size: 684458 Color: 16
Size: 315519 Color: 12

Bin 1760: 24 of cap free
Amount of items: 2
Items: 
Size: 718919 Color: 9
Size: 281058 Color: 19

Bin 1761: 24 of cap free
Amount of items: 2
Items: 
Size: 721255 Color: 14
Size: 278722 Color: 2

Bin 1762: 24 of cap free
Amount of items: 2
Items: 
Size: 742695 Color: 2
Size: 257282 Color: 7

Bin 1763: 24 of cap free
Amount of items: 2
Items: 
Size: 761233 Color: 16
Size: 238744 Color: 0

Bin 1764: 24 of cap free
Amount of items: 2
Items: 
Size: 761795 Color: 8
Size: 238182 Color: 5

Bin 1765: 25 of cap free
Amount of items: 2
Items: 
Size: 646435 Color: 3
Size: 353541 Color: 8

Bin 1766: 25 of cap free
Amount of items: 2
Items: 
Size: 669614 Color: 17
Size: 330362 Color: 1

Bin 1767: 25 of cap free
Amount of items: 2
Items: 
Size: 784355 Color: 14
Size: 215621 Color: 10

Bin 1768: 25 of cap free
Amount of items: 3
Items: 
Size: 639791 Color: 18
Size: 180501 Color: 5
Size: 179684 Color: 14

Bin 1769: 25 of cap free
Amount of items: 2
Items: 
Size: 779678 Color: 0
Size: 220298 Color: 6

Bin 1770: 25 of cap free
Amount of items: 2
Items: 
Size: 795892 Color: 13
Size: 204084 Color: 15

Bin 1771: 25 of cap free
Amount of items: 2
Items: 
Size: 749891 Color: 18
Size: 250085 Color: 16

Bin 1772: 25 of cap free
Amount of items: 2
Items: 
Size: 543783 Color: 4
Size: 456193 Color: 6

Bin 1773: 25 of cap free
Amount of items: 2
Items: 
Size: 501493 Color: 6
Size: 498483 Color: 3

Bin 1774: 25 of cap free
Amount of items: 2
Items: 
Size: 531340 Color: 17
Size: 468636 Color: 1

Bin 1775: 25 of cap free
Amount of items: 2
Items: 
Size: 535576 Color: 19
Size: 464400 Color: 4

Bin 1776: 25 of cap free
Amount of items: 2
Items: 
Size: 547148 Color: 9
Size: 452828 Color: 6

Bin 1777: 25 of cap free
Amount of items: 2
Items: 
Size: 548679 Color: 18
Size: 451297 Color: 5

Bin 1778: 25 of cap free
Amount of items: 2
Items: 
Size: 557183 Color: 14
Size: 442793 Color: 19

Bin 1779: 25 of cap free
Amount of items: 2
Items: 
Size: 600632 Color: 3
Size: 399344 Color: 9

Bin 1780: 25 of cap free
Amount of items: 2
Items: 
Size: 624098 Color: 8
Size: 375878 Color: 15

Bin 1781: 25 of cap free
Amount of items: 2
Items: 
Size: 628047 Color: 15
Size: 371929 Color: 18

Bin 1782: 25 of cap free
Amount of items: 2
Items: 
Size: 634050 Color: 10
Size: 365926 Color: 3

Bin 1783: 25 of cap free
Amount of items: 2
Items: 
Size: 638898 Color: 8
Size: 361078 Color: 5

Bin 1784: 25 of cap free
Amount of items: 2
Items: 
Size: 642034 Color: 13
Size: 357942 Color: 14

Bin 1785: 25 of cap free
Amount of items: 2
Items: 
Size: 671121 Color: 8
Size: 328855 Color: 2

Bin 1786: 25 of cap free
Amount of items: 2
Items: 
Size: 738619 Color: 14
Size: 261357 Color: 10

Bin 1787: 25 of cap free
Amount of items: 2
Items: 
Size: 768540 Color: 8
Size: 231436 Color: 16

Bin 1788: 26 of cap free
Amount of items: 3
Items: 
Size: 732372 Color: 1
Size: 153094 Color: 8
Size: 114509 Color: 17

Bin 1789: 26 of cap free
Amount of items: 3
Items: 
Size: 655899 Color: 14
Size: 182716 Color: 19
Size: 161360 Color: 3

Bin 1790: 26 of cap free
Amount of items: 2
Items: 
Size: 689259 Color: 17
Size: 310716 Color: 14

Bin 1791: 26 of cap free
Amount of items: 2
Items: 
Size: 753505 Color: 7
Size: 246470 Color: 18

Bin 1792: 26 of cap free
Amount of items: 3
Items: 
Size: 725374 Color: 3
Size: 148081 Color: 18
Size: 126520 Color: 17

Bin 1793: 26 of cap free
Amount of items: 2
Items: 
Size: 772505 Color: 15
Size: 227470 Color: 7

Bin 1794: 26 of cap free
Amount of items: 3
Items: 
Size: 612460 Color: 14
Size: 193806 Color: 11
Size: 193709 Color: 2

Bin 1795: 26 of cap free
Amount of items: 3
Items: 
Size: 654887 Color: 13
Size: 173064 Color: 16
Size: 172024 Color: 9

Bin 1796: 26 of cap free
Amount of items: 3
Items: 
Size: 609886 Color: 16
Size: 195610 Color: 0
Size: 194479 Color: 2

Bin 1797: 26 of cap free
Amount of items: 2
Items: 
Size: 723940 Color: 16
Size: 276035 Color: 8

Bin 1798: 26 of cap free
Amount of items: 2
Items: 
Size: 679810 Color: 9
Size: 320165 Color: 10

Bin 1799: 26 of cap free
Amount of items: 2
Items: 
Size: 650093 Color: 1
Size: 349882 Color: 5

Bin 1800: 26 of cap free
Amount of items: 3
Items: 
Size: 367110 Color: 6
Size: 337352 Color: 9
Size: 295513 Color: 16

Bin 1801: 26 of cap free
Amount of items: 2
Items: 
Size: 548906 Color: 14
Size: 451069 Color: 13

Bin 1802: 26 of cap free
Amount of items: 2
Items: 
Size: 550927 Color: 4
Size: 449048 Color: 10

Bin 1803: 26 of cap free
Amount of items: 2
Items: 
Size: 587875 Color: 4
Size: 412100 Color: 1

Bin 1804: 26 of cap free
Amount of items: 2
Items: 
Size: 591211 Color: 14
Size: 408764 Color: 6

Bin 1805: 26 of cap free
Amount of items: 2
Items: 
Size: 617354 Color: 15
Size: 382621 Color: 3

Bin 1806: 26 of cap free
Amount of items: 2
Items: 
Size: 620405 Color: 17
Size: 379570 Color: 16

Bin 1807: 26 of cap free
Amount of items: 2
Items: 
Size: 634883 Color: 14
Size: 365092 Color: 3

Bin 1808: 26 of cap free
Amount of items: 2
Items: 
Size: 663190 Color: 16
Size: 336785 Color: 9

Bin 1809: 26 of cap free
Amount of items: 2
Items: 
Size: 680674 Color: 18
Size: 319301 Color: 0

Bin 1810: 26 of cap free
Amount of items: 2
Items: 
Size: 788678 Color: 2
Size: 211297 Color: 13

Bin 1811: 27 of cap free
Amount of items: 2
Items: 
Size: 671870 Color: 3
Size: 328104 Color: 4

Bin 1812: 27 of cap free
Amount of items: 2
Items: 
Size: 625096 Color: 16
Size: 374878 Color: 11

Bin 1813: 27 of cap free
Amount of items: 2
Items: 
Size: 729179 Color: 17
Size: 270795 Color: 16

Bin 1814: 27 of cap free
Amount of items: 3
Items: 
Size: 743012 Color: 7
Size: 129675 Color: 14
Size: 127287 Color: 9

Bin 1815: 27 of cap free
Amount of items: 3
Items: 
Size: 503343 Color: 6
Size: 263432 Color: 17
Size: 233199 Color: 13

Bin 1816: 27 of cap free
Amount of items: 3
Items: 
Size: 650288 Color: 1
Size: 175431 Color: 18
Size: 174255 Color: 0

Bin 1817: 27 of cap free
Amount of items: 3
Items: 
Size: 406098 Color: 19
Size: 306251 Color: 18
Size: 287625 Color: 13

Bin 1818: 27 of cap free
Amount of items: 3
Items: 
Size: 380776 Color: 13
Size: 326008 Color: 1
Size: 293190 Color: 8

Bin 1819: 27 of cap free
Amount of items: 2
Items: 
Size: 518400 Color: 12
Size: 481574 Color: 15

Bin 1820: 27 of cap free
Amount of items: 2
Items: 
Size: 543602 Color: 6
Size: 456372 Color: 17

Bin 1821: 27 of cap free
Amount of items: 2
Items: 
Size: 549146 Color: 0
Size: 450828 Color: 1

Bin 1822: 27 of cap free
Amount of items: 2
Items: 
Size: 571275 Color: 17
Size: 428699 Color: 5

Bin 1823: 27 of cap free
Amount of items: 2
Items: 
Size: 571837 Color: 2
Size: 428137 Color: 4

Bin 1824: 27 of cap free
Amount of items: 2
Items: 
Size: 590751 Color: 10
Size: 409223 Color: 0

Bin 1825: 27 of cap free
Amount of items: 2
Items: 
Size: 593949 Color: 8
Size: 406025 Color: 9

Bin 1826: 27 of cap free
Amount of items: 2
Items: 
Size: 594205 Color: 3
Size: 405769 Color: 12

Bin 1827: 27 of cap free
Amount of items: 2
Items: 
Size: 601634 Color: 13
Size: 398340 Color: 10

Bin 1828: 27 of cap free
Amount of items: 2
Items: 
Size: 643483 Color: 14
Size: 356491 Color: 7

Bin 1829: 27 of cap free
Amount of items: 2
Items: 
Size: 652294 Color: 8
Size: 347680 Color: 19

Bin 1830: 27 of cap free
Amount of items: 2
Items: 
Size: 656724 Color: 17
Size: 343250 Color: 5

Bin 1831: 27 of cap free
Amount of items: 2
Items: 
Size: 716780 Color: 17
Size: 283194 Color: 13

Bin 1832: 27 of cap free
Amount of items: 2
Items: 
Size: 728051 Color: 3
Size: 271923 Color: 18

Bin 1833: 27 of cap free
Amount of items: 2
Items: 
Size: 737541 Color: 2
Size: 262433 Color: 13

Bin 1834: 27 of cap free
Amount of items: 2
Items: 
Size: 741533 Color: 15
Size: 258441 Color: 10

Bin 1835: 27 of cap free
Amount of items: 2
Items: 
Size: 742963 Color: 16
Size: 257011 Color: 10

Bin 1836: 27 of cap free
Amount of items: 2
Items: 
Size: 767609 Color: 16
Size: 232365 Color: 8

Bin 1837: 27 of cap free
Amount of items: 2
Items: 
Size: 779999 Color: 10
Size: 219975 Color: 6

Bin 1838: 27 of cap free
Amount of items: 2
Items: 
Size: 787702 Color: 4
Size: 212272 Color: 17

Bin 1839: 28 of cap free
Amount of items: 3
Items: 
Size: 621093 Color: 13
Size: 218080 Color: 16
Size: 160800 Color: 16

Bin 1840: 28 of cap free
Amount of items: 2
Items: 
Size: 729470 Color: 5
Size: 270503 Color: 12

Bin 1841: 28 of cap free
Amount of items: 2
Items: 
Size: 763023 Color: 7
Size: 236950 Color: 4

Bin 1842: 28 of cap free
Amount of items: 2
Items: 
Size: 739762 Color: 14
Size: 260211 Color: 12

Bin 1843: 28 of cap free
Amount of items: 2
Items: 
Size: 719062 Color: 11
Size: 280911 Color: 14

Bin 1844: 28 of cap free
Amount of items: 2
Items: 
Size: 501764 Color: 10
Size: 498209 Color: 8

Bin 1845: 28 of cap free
Amount of items: 2
Items: 
Size: 654455 Color: 11
Size: 345518 Color: 3

Bin 1846: 28 of cap free
Amount of items: 2
Items: 
Size: 618582 Color: 3
Size: 381391 Color: 8

Bin 1847: 28 of cap free
Amount of items: 3
Items: 
Size: 365214 Color: 15
Size: 346289 Color: 15
Size: 288470 Color: 17

Bin 1848: 28 of cap free
Amount of items: 2
Items: 
Size: 540147 Color: 6
Size: 459826 Color: 17

Bin 1849: 28 of cap free
Amount of items: 2
Items: 
Size: 557567 Color: 9
Size: 442406 Color: 12

Bin 1850: 28 of cap free
Amount of items: 2
Items: 
Size: 568780 Color: 9
Size: 431193 Color: 10

Bin 1851: 28 of cap free
Amount of items: 2
Items: 
Size: 570635 Color: 12
Size: 429338 Color: 15

Bin 1852: 28 of cap free
Amount of items: 2
Items: 
Size: 573518 Color: 19
Size: 426455 Color: 5

Bin 1853: 28 of cap free
Amount of items: 2
Items: 
Size: 577618 Color: 15
Size: 422355 Color: 12

Bin 1854: 28 of cap free
Amount of items: 2
Items: 
Size: 593281 Color: 4
Size: 406692 Color: 10

Bin 1855: 28 of cap free
Amount of items: 2
Items: 
Size: 593608 Color: 13
Size: 406365 Color: 10

Bin 1856: 28 of cap free
Amount of items: 2
Items: 
Size: 623127 Color: 0
Size: 376846 Color: 14

Bin 1857: 28 of cap free
Amount of items: 2
Items: 
Size: 635674 Color: 13
Size: 364299 Color: 4

Bin 1858: 28 of cap free
Amount of items: 2
Items: 
Size: 643310 Color: 8
Size: 356663 Color: 7

Bin 1859: 28 of cap free
Amount of items: 2
Items: 
Size: 648396 Color: 13
Size: 351577 Color: 15

Bin 1860: 28 of cap free
Amount of items: 2
Items: 
Size: 660809 Color: 4
Size: 339164 Color: 19

Bin 1861: 28 of cap free
Amount of items: 2
Items: 
Size: 677788 Color: 4
Size: 322185 Color: 8

Bin 1862: 28 of cap free
Amount of items: 2
Items: 
Size: 685559 Color: 9
Size: 314414 Color: 6

Bin 1863: 28 of cap free
Amount of items: 2
Items: 
Size: 686152 Color: 18
Size: 313821 Color: 5

Bin 1864: 28 of cap free
Amount of items: 2
Items: 
Size: 702280 Color: 13
Size: 297693 Color: 10

Bin 1865: 28 of cap free
Amount of items: 2
Items: 
Size: 722332 Color: 14
Size: 277641 Color: 8

Bin 1866: 28 of cap free
Amount of items: 2
Items: 
Size: 723737 Color: 17
Size: 276236 Color: 6

Bin 1867: 28 of cap free
Amount of items: 2
Items: 
Size: 756288 Color: 9
Size: 243685 Color: 5

Bin 1868: 28 of cap free
Amount of items: 2
Items: 
Size: 759266 Color: 7
Size: 240707 Color: 2

Bin 1869: 29 of cap free
Amount of items: 3
Items: 
Size: 668652 Color: 17
Size: 168774 Color: 18
Size: 162546 Color: 5

Bin 1870: 29 of cap free
Amount of items: 2
Items: 
Size: 698566 Color: 3
Size: 301406 Color: 2

Bin 1871: 29 of cap free
Amount of items: 2
Items: 
Size: 732476 Color: 12
Size: 267496 Color: 4

Bin 1872: 29 of cap free
Amount of items: 2
Items: 
Size: 793552 Color: 14
Size: 206420 Color: 11

Bin 1873: 29 of cap free
Amount of items: 3
Items: 
Size: 346558 Color: 19
Size: 335057 Color: 14
Size: 318357 Color: 4

Bin 1874: 29 of cap free
Amount of items: 2
Items: 
Size: 513273 Color: 3
Size: 486699 Color: 15

Bin 1875: 29 of cap free
Amount of items: 2
Items: 
Size: 526232 Color: 14
Size: 473740 Color: 7

Bin 1876: 29 of cap free
Amount of items: 2
Items: 
Size: 571984 Color: 8
Size: 427988 Color: 7

Bin 1877: 29 of cap free
Amount of items: 2
Items: 
Size: 585605 Color: 11
Size: 414367 Color: 5

Bin 1878: 29 of cap free
Amount of items: 2
Items: 
Size: 599921 Color: 0
Size: 400051 Color: 12

Bin 1879: 29 of cap free
Amount of items: 2
Items: 
Size: 600669 Color: 5
Size: 399303 Color: 7

Bin 1880: 29 of cap free
Amount of items: 2
Items: 
Size: 606243 Color: 17
Size: 393729 Color: 4

Bin 1881: 29 of cap free
Amount of items: 2
Items: 
Size: 609235 Color: 10
Size: 390737 Color: 4

Bin 1882: 29 of cap free
Amount of items: 2
Items: 
Size: 612305 Color: 0
Size: 387667 Color: 9

Bin 1883: 29 of cap free
Amount of items: 2
Items: 
Size: 615872 Color: 15
Size: 384100 Color: 5

Bin 1884: 29 of cap free
Amount of items: 2
Items: 
Size: 703627 Color: 7
Size: 296345 Color: 14

Bin 1885: 29 of cap free
Amount of items: 2
Items: 
Size: 709065 Color: 6
Size: 290907 Color: 5

Bin 1886: 29 of cap free
Amount of items: 2
Items: 
Size: 711027 Color: 19
Size: 288945 Color: 1

Bin 1887: 30 of cap free
Amount of items: 3
Items: 
Size: 742170 Color: 2
Size: 129201 Color: 12
Size: 128600 Color: 0

Bin 1888: 30 of cap free
Amount of items: 2
Items: 
Size: 732856 Color: 17
Size: 267115 Color: 13

Bin 1889: 30 of cap free
Amount of items: 2
Items: 
Size: 581957 Color: 1
Size: 418014 Color: 16

Bin 1890: 30 of cap free
Amount of items: 2
Items: 
Size: 791072 Color: 19
Size: 208899 Color: 13

Bin 1891: 30 of cap free
Amount of items: 3
Items: 
Size: 744413 Color: 14
Size: 128164 Color: 3
Size: 127394 Color: 9

Bin 1892: 30 of cap free
Amount of items: 3
Items: 
Size: 714698 Color: 17
Size: 142645 Color: 6
Size: 142628 Color: 5

Bin 1893: 30 of cap free
Amount of items: 2
Items: 
Size: 701262 Color: 15
Size: 298709 Color: 12

Bin 1894: 30 of cap free
Amount of items: 2
Items: 
Size: 643951 Color: 1
Size: 356020 Color: 4

Bin 1895: 30 of cap free
Amount of items: 2
Items: 
Size: 742126 Color: 13
Size: 257845 Color: 17

Bin 1896: 30 of cap free
Amount of items: 2
Items: 
Size: 782156 Color: 13
Size: 217815 Color: 12

Bin 1897: 30 of cap free
Amount of items: 2
Items: 
Size: 515307 Color: 3
Size: 484664 Color: 11

Bin 1898: 30 of cap free
Amount of items: 2
Items: 
Size: 517008 Color: 1
Size: 482963 Color: 6

Bin 1899: 30 of cap free
Amount of items: 2
Items: 
Size: 534520 Color: 12
Size: 465451 Color: 8

Bin 1900: 30 of cap free
Amount of items: 2
Items: 
Size: 539278 Color: 15
Size: 460693 Color: 17

Bin 1901: 30 of cap free
Amount of items: 2
Items: 
Size: 546541 Color: 0
Size: 453430 Color: 2

Bin 1902: 30 of cap free
Amount of items: 2
Items: 
Size: 552199 Color: 11
Size: 447772 Color: 12

Bin 1903: 30 of cap free
Amount of items: 2
Items: 
Size: 565318 Color: 19
Size: 434653 Color: 4

Bin 1904: 30 of cap free
Amount of items: 2
Items: 
Size: 609706 Color: 8
Size: 390265 Color: 1

Bin 1905: 30 of cap free
Amount of items: 2
Items: 
Size: 612335 Color: 9
Size: 387636 Color: 0

Bin 1906: 30 of cap free
Amount of items: 2
Items: 
Size: 623553 Color: 13
Size: 376418 Color: 4

Bin 1907: 30 of cap free
Amount of items: 2
Items: 
Size: 624138 Color: 10
Size: 375833 Color: 13

Bin 1908: 30 of cap free
Amount of items: 2
Items: 
Size: 630448 Color: 14
Size: 369523 Color: 4

Bin 1909: 30 of cap free
Amount of items: 2
Items: 
Size: 648529 Color: 6
Size: 351442 Color: 15

Bin 1910: 30 of cap free
Amount of items: 2
Items: 
Size: 712532 Color: 10
Size: 287439 Color: 12

Bin 1911: 30 of cap free
Amount of items: 2
Items: 
Size: 731497 Color: 5
Size: 268474 Color: 14

Bin 1912: 30 of cap free
Amount of items: 2
Items: 
Size: 746016 Color: 4
Size: 253955 Color: 18

Bin 1913: 30 of cap free
Amount of items: 2
Items: 
Size: 774282 Color: 19
Size: 225689 Color: 11

Bin 1914: 30 of cap free
Amount of items: 2
Items: 
Size: 786914 Color: 0
Size: 213057 Color: 13

Bin 1915: 31 of cap free
Amount of items: 2
Items: 
Size: 690056 Color: 5
Size: 309914 Color: 13

Bin 1916: 31 of cap free
Amount of items: 2
Items: 
Size: 799097 Color: 11
Size: 200873 Color: 1

Bin 1917: 31 of cap free
Amount of items: 2
Items: 
Size: 619231 Color: 4
Size: 380739 Color: 15

Bin 1918: 31 of cap free
Amount of items: 2
Items: 
Size: 680042 Color: 7
Size: 319928 Color: 11

Bin 1919: 31 of cap free
Amount of items: 2
Items: 
Size: 562706 Color: 15
Size: 437264 Color: 8

Bin 1920: 31 of cap free
Amount of items: 2
Items: 
Size: 697397 Color: 13
Size: 302573 Color: 11

Bin 1921: 31 of cap free
Amount of items: 2
Items: 
Size: 512063 Color: 0
Size: 487907 Color: 9

Bin 1922: 31 of cap free
Amount of items: 2
Items: 
Size: 520809 Color: 8
Size: 479161 Color: 1

Bin 1923: 31 of cap free
Amount of items: 2
Items: 
Size: 521626 Color: 10
Size: 478344 Color: 8

Bin 1924: 31 of cap free
Amount of items: 2
Items: 
Size: 530201 Color: 8
Size: 469769 Color: 17

Bin 1925: 31 of cap free
Amount of items: 2
Items: 
Size: 553343 Color: 8
Size: 446627 Color: 15

Bin 1926: 31 of cap free
Amount of items: 2
Items: 
Size: 557925 Color: 4
Size: 442045 Color: 12

Bin 1927: 31 of cap free
Amount of items: 2
Items: 
Size: 561875 Color: 9
Size: 438095 Color: 4

Bin 1928: 31 of cap free
Amount of items: 2
Items: 
Size: 572823 Color: 13
Size: 427147 Color: 10

Bin 1929: 31 of cap free
Amount of items: 2
Items: 
Size: 594845 Color: 18
Size: 405125 Color: 1

Bin 1930: 31 of cap free
Amount of items: 2
Items: 
Size: 598782 Color: 6
Size: 401188 Color: 4

Bin 1931: 31 of cap free
Amount of items: 2
Items: 
Size: 607737 Color: 8
Size: 392233 Color: 19

Bin 1932: 31 of cap free
Amount of items: 2
Items: 
Size: 611185 Color: 11
Size: 388785 Color: 5

Bin 1933: 31 of cap free
Amount of items: 3
Items: 
Size: 617549 Color: 0
Size: 191276 Color: 10
Size: 191145 Color: 13

Bin 1934: 31 of cap free
Amount of items: 2
Items: 
Size: 628839 Color: 3
Size: 371131 Color: 7

Bin 1935: 31 of cap free
Amount of items: 2
Items: 
Size: 677983 Color: 9
Size: 321987 Color: 8

Bin 1936: 31 of cap free
Amount of items: 2
Items: 
Size: 687082 Color: 3
Size: 312888 Color: 0

Bin 1937: 31 of cap free
Amount of items: 2
Items: 
Size: 690197 Color: 2
Size: 309773 Color: 4

Bin 1938: 31 of cap free
Amount of items: 2
Items: 
Size: 701659 Color: 19
Size: 298311 Color: 7

Bin 1939: 32 of cap free
Amount of items: 2
Items: 
Size: 772594 Color: 10
Size: 227375 Color: 11

Bin 1940: 32 of cap free
Amount of items: 3
Items: 
Size: 633763 Color: 6
Size: 183745 Color: 4
Size: 182461 Color: 1

Bin 1941: 32 of cap free
Amount of items: 3
Items: 
Size: 571411 Color: 17
Size: 278466 Color: 9
Size: 150092 Color: 19

Bin 1942: 32 of cap free
Amount of items: 2
Items: 
Size: 717233 Color: 11
Size: 282736 Color: 5

Bin 1943: 32 of cap free
Amount of items: 2
Items: 
Size: 629041 Color: 2
Size: 370928 Color: 14

Bin 1944: 32 of cap free
Amount of items: 2
Items: 
Size: 783634 Color: 0
Size: 216335 Color: 9

Bin 1945: 32 of cap free
Amount of items: 2
Items: 
Size: 652889 Color: 10
Size: 347080 Color: 13

Bin 1946: 32 of cap free
Amount of items: 2
Items: 
Size: 689073 Color: 14
Size: 310896 Color: 15

Bin 1947: 32 of cap free
Amount of items: 3
Items: 
Size: 643591 Color: 1
Size: 178485 Color: 1
Size: 177893 Color: 3

Bin 1948: 32 of cap free
Amount of items: 2
Items: 
Size: 506531 Color: 8
Size: 493438 Color: 1

Bin 1949: 32 of cap free
Amount of items: 2
Items: 
Size: 524102 Color: 12
Size: 475867 Color: 6

Bin 1950: 32 of cap free
Amount of items: 2
Items: 
Size: 582702 Color: 3
Size: 417267 Color: 14

Bin 1951: 32 of cap free
Amount of items: 2
Items: 
Size: 641615 Color: 12
Size: 358354 Color: 6

Bin 1952: 32 of cap free
Amount of items: 2
Items: 
Size: 650734 Color: 14
Size: 349235 Color: 2

Bin 1953: 32 of cap free
Amount of items: 2
Items: 
Size: 691594 Color: 10
Size: 308375 Color: 12

Bin 1954: 32 of cap free
Amount of items: 2
Items: 
Size: 724643 Color: 12
Size: 275326 Color: 7

Bin 1955: 32 of cap free
Amount of items: 3
Items: 
Size: 794320 Color: 17
Size: 102945 Color: 5
Size: 102704 Color: 9

Bin 1956: 33 of cap free
Amount of items: 3
Items: 
Size: 726208 Color: 3
Size: 142354 Color: 18
Size: 131406 Color: 8

Bin 1957: 33 of cap free
Amount of items: 3
Items: 
Size: 667576 Color: 6
Size: 167770 Color: 5
Size: 164622 Color: 2

Bin 1958: 33 of cap free
Amount of items: 2
Items: 
Size: 579048 Color: 8
Size: 420920 Color: 14

Bin 1959: 33 of cap free
Amount of items: 3
Items: 
Size: 702701 Color: 4
Size: 149033 Color: 7
Size: 148234 Color: 7

Bin 1960: 33 of cap free
Amount of items: 2
Items: 
Size: 500516 Color: 3
Size: 499452 Color: 9

Bin 1961: 33 of cap free
Amount of items: 2
Items: 
Size: 696899 Color: 7
Size: 303069 Color: 12

Bin 1962: 33 of cap free
Amount of items: 3
Items: 
Size: 363299 Color: 3
Size: 336350 Color: 18
Size: 300319 Color: 7

Bin 1963: 33 of cap free
Amount of items: 2
Items: 
Size: 772146 Color: 17
Size: 227822 Color: 11

Bin 1964: 33 of cap free
Amount of items: 3
Items: 
Size: 371407 Color: 16
Size: 337616 Color: 19
Size: 290945 Color: 4

Bin 1965: 33 of cap free
Amount of items: 2
Items: 
Size: 518301 Color: 3
Size: 481667 Color: 13

Bin 1966: 33 of cap free
Amount of items: 2
Items: 
Size: 549885 Color: 8
Size: 450083 Color: 11

Bin 1967: 33 of cap free
Amount of items: 2
Items: 
Size: 561792 Color: 15
Size: 438176 Color: 19

Bin 1968: 33 of cap free
Amount of items: 2
Items: 
Size: 573657 Color: 6
Size: 426311 Color: 12

Bin 1969: 33 of cap free
Amount of items: 2
Items: 
Size: 575954 Color: 13
Size: 424014 Color: 4

Bin 1970: 33 of cap free
Amount of items: 2
Items: 
Size: 610912 Color: 7
Size: 389056 Color: 12

Bin 1971: 33 of cap free
Amount of items: 2
Items: 
Size: 636958 Color: 18
Size: 363010 Color: 13

Bin 1972: 33 of cap free
Amount of items: 2
Items: 
Size: 650591 Color: 11
Size: 349377 Color: 8

Bin 1973: 33 of cap free
Amount of items: 2
Items: 
Size: 674865 Color: 15
Size: 325103 Color: 12

Bin 1974: 33 of cap free
Amount of items: 2
Items: 
Size: 730543 Color: 14
Size: 269425 Color: 8

Bin 1975: 33 of cap free
Amount of items: 2
Items: 
Size: 763213 Color: 15
Size: 236755 Color: 2

Bin 1976: 34 of cap free
Amount of items: 2
Items: 
Size: 716295 Color: 16
Size: 283672 Color: 14

Bin 1977: 34 of cap free
Amount of items: 2
Items: 
Size: 701174 Color: 8
Size: 298793 Color: 11

Bin 1978: 34 of cap free
Amount of items: 2
Items: 
Size: 752019 Color: 0
Size: 247948 Color: 7

Bin 1979: 34 of cap free
Amount of items: 2
Items: 
Size: 520419 Color: 6
Size: 479548 Color: 5

Bin 1980: 34 of cap free
Amount of items: 2
Items: 
Size: 699924 Color: 4
Size: 300043 Color: 8

Bin 1981: 34 of cap free
Amount of items: 2
Items: 
Size: 670201 Color: 2
Size: 329766 Color: 18

Bin 1982: 34 of cap free
Amount of items: 2
Items: 
Size: 751924 Color: 16
Size: 248043 Color: 17

Bin 1983: 34 of cap free
Amount of items: 3
Items: 
Size: 646206 Color: 12
Size: 178864 Color: 19
Size: 174897 Color: 7

Bin 1984: 34 of cap free
Amount of items: 2
Items: 
Size: 745167 Color: 6
Size: 254800 Color: 15

Bin 1985: 34 of cap free
Amount of items: 2
Items: 
Size: 691715 Color: 7
Size: 308252 Color: 14

Bin 1986: 34 of cap free
Amount of items: 2
Items: 
Size: 522817 Color: 3
Size: 477150 Color: 5

Bin 1987: 34 of cap free
Amount of items: 3
Items: 
Size: 510972 Color: 8
Size: 255675 Color: 13
Size: 233320 Color: 18

Bin 1988: 34 of cap free
Amount of items: 2
Items: 
Size: 564858 Color: 4
Size: 435109 Color: 12

Bin 1989: 34 of cap free
Amount of items: 2
Items: 
Size: 790051 Color: 11
Size: 209916 Color: 2

Bin 1990: 34 of cap free
Amount of items: 3
Items: 
Size: 381216 Color: 2
Size: 312560 Color: 4
Size: 306191 Color: 8

Bin 1991: 34 of cap free
Amount of items: 2
Items: 
Size: 551651 Color: 0
Size: 448316 Color: 18

Bin 1992: 34 of cap free
Amount of items: 2
Items: 
Size: 569429 Color: 0
Size: 430538 Color: 5

Bin 1993: 34 of cap free
Amount of items: 2
Items: 
Size: 602887 Color: 6
Size: 397080 Color: 10

Bin 1994: 34 of cap free
Amount of items: 2
Items: 
Size: 630267 Color: 17
Size: 369700 Color: 11

Bin 1995: 34 of cap free
Amount of items: 2
Items: 
Size: 639898 Color: 2
Size: 360069 Color: 14

Bin 1996: 34 of cap free
Amount of items: 3
Items: 
Size: 658100 Color: 0
Size: 170976 Color: 0
Size: 170891 Color: 14

Bin 1997: 34 of cap free
Amount of items: 2
Items: 
Size: 678904 Color: 9
Size: 321063 Color: 3

Bin 1998: 34 of cap free
Amount of items: 2
Items: 
Size: 687192 Color: 17
Size: 312775 Color: 8

Bin 1999: 34 of cap free
Amount of items: 2
Items: 
Size: 692222 Color: 1
Size: 307745 Color: 10

Bin 2000: 34 of cap free
Amount of items: 2
Items: 
Size: 706192 Color: 16
Size: 293775 Color: 8

Bin 2001: 34 of cap free
Amount of items: 2
Items: 
Size: 717459 Color: 16
Size: 282508 Color: 3

Bin 2002: 34 of cap free
Amount of items: 2
Items: 
Size: 736597 Color: 13
Size: 263370 Color: 10

Bin 2003: 34 of cap free
Amount of items: 2
Items: 
Size: 758727 Color: 8
Size: 241240 Color: 10

Bin 2004: 35 of cap free
Amount of items: 2
Items: 
Size: 604350 Color: 14
Size: 395616 Color: 2

Bin 2005: 35 of cap free
Amount of items: 2
Items: 
Size: 504510 Color: 9
Size: 495456 Color: 2

Bin 2006: 35 of cap free
Amount of items: 2
Items: 
Size: 505927 Color: 0
Size: 494039 Color: 7

Bin 2007: 35 of cap free
Amount of items: 2
Items: 
Size: 559447 Color: 14
Size: 440519 Color: 10

Bin 2008: 35 of cap free
Amount of items: 2
Items: 
Size: 560361 Color: 1
Size: 439605 Color: 10

Bin 2009: 35 of cap free
Amount of items: 2
Items: 
Size: 561627 Color: 17
Size: 438339 Color: 2

Bin 2010: 35 of cap free
Amount of items: 2
Items: 
Size: 562495 Color: 2
Size: 437471 Color: 8

Bin 2011: 35 of cap free
Amount of items: 2
Items: 
Size: 563309 Color: 19
Size: 436657 Color: 2

Bin 2012: 35 of cap free
Amount of items: 2
Items: 
Size: 676234 Color: 12
Size: 323732 Color: 17

Bin 2013: 35 of cap free
Amount of items: 2
Items: 
Size: 685987 Color: 17
Size: 313979 Color: 7

Bin 2014: 35 of cap free
Amount of items: 2
Items: 
Size: 698815 Color: 11
Size: 301151 Color: 2

Bin 2015: 35 of cap free
Amount of items: 2
Items: 
Size: 703730 Color: 4
Size: 296236 Color: 18

Bin 2016: 35 of cap free
Amount of items: 2
Items: 
Size: 711573 Color: 15
Size: 288393 Color: 1

Bin 2017: 35 of cap free
Amount of items: 2
Items: 
Size: 746003 Color: 1
Size: 253963 Color: 4

Bin 2018: 35 of cap free
Amount of items: 2
Items: 
Size: 752395 Color: 8
Size: 247571 Color: 2

Bin 2019: 36 of cap free
Amount of items: 2
Items: 
Size: 678233 Color: 4
Size: 321732 Color: 1

Bin 2020: 36 of cap free
Amount of items: 2
Items: 
Size: 737682 Color: 14
Size: 262283 Color: 5

Bin 2021: 36 of cap free
Amount of items: 2
Items: 
Size: 760442 Color: 7
Size: 239523 Color: 0

Bin 2022: 36 of cap free
Amount of items: 2
Items: 
Size: 757994 Color: 5
Size: 241971 Color: 0

Bin 2023: 36 of cap free
Amount of items: 2
Items: 
Size: 756178 Color: 6
Size: 243787 Color: 19

Bin 2024: 36 of cap free
Amount of items: 2
Items: 
Size: 607563 Color: 18
Size: 392402 Color: 3

Bin 2025: 36 of cap free
Amount of items: 2
Items: 
Size: 667574 Color: 1
Size: 332391 Color: 13

Bin 2026: 36 of cap free
Amount of items: 2
Items: 
Size: 621164 Color: 15
Size: 378801 Color: 18

Bin 2027: 36 of cap free
Amount of items: 3
Items: 
Size: 686550 Color: 9
Size: 157298 Color: 14
Size: 156117 Color: 7

Bin 2028: 36 of cap free
Amount of items: 3
Items: 
Size: 370199 Color: 8
Size: 340944 Color: 13
Size: 288822 Color: 0

Bin 2029: 36 of cap free
Amount of items: 2
Items: 
Size: 507487 Color: 14
Size: 492478 Color: 17

Bin 2030: 36 of cap free
Amount of items: 2
Items: 
Size: 525260 Color: 17
Size: 474705 Color: 7

Bin 2031: 36 of cap free
Amount of items: 2
Items: 
Size: 528139 Color: 17
Size: 471826 Color: 8

Bin 2032: 36 of cap free
Amount of items: 2
Items: 
Size: 534636 Color: 12
Size: 465329 Color: 19

Bin 2033: 36 of cap free
Amount of items: 2
Items: 
Size: 534878 Color: 11
Size: 465087 Color: 13

Bin 2034: 36 of cap free
Amount of items: 2
Items: 
Size: 543268 Color: 6
Size: 456697 Color: 3

Bin 2035: 36 of cap free
Amount of items: 2
Items: 
Size: 569164 Color: 2
Size: 430801 Color: 13

Bin 2036: 36 of cap free
Amount of items: 2
Items: 
Size: 573298 Color: 6
Size: 426667 Color: 2

Bin 2037: 36 of cap free
Amount of items: 2
Items: 
Size: 586670 Color: 3
Size: 413295 Color: 5

Bin 2038: 36 of cap free
Amount of items: 3
Items: 
Size: 604533 Color: 19
Size: 198643 Color: 0
Size: 196789 Color: 0

Bin 2039: 36 of cap free
Amount of items: 3
Items: 
Size: 626493 Color: 15
Size: 186894 Color: 2
Size: 186578 Color: 11

Bin 2040: 36 of cap free
Amount of items: 2
Items: 
Size: 643654 Color: 13
Size: 356311 Color: 17

Bin 2041: 36 of cap free
Amount of items: 2
Items: 
Size: 646924 Color: 3
Size: 353041 Color: 16

Bin 2042: 36 of cap free
Amount of items: 2
Items: 
Size: 657778 Color: 0
Size: 342187 Color: 6

Bin 2043: 36 of cap free
Amount of items: 3
Items: 
Size: 783315 Color: 18
Size: 108408 Color: 1
Size: 108242 Color: 6

Bin 2044: 37 of cap free
Amount of items: 2
Items: 
Size: 653632 Color: 11
Size: 346332 Color: 9

Bin 2045: 37 of cap free
Amount of items: 2
Items: 
Size: 770211 Color: 0
Size: 229753 Color: 6

Bin 2046: 37 of cap free
Amount of items: 2
Items: 
Size: 782472 Color: 10
Size: 217492 Color: 14

Bin 2047: 37 of cap free
Amount of items: 2
Items: 
Size: 608800 Color: 18
Size: 391164 Color: 19

Bin 2048: 37 of cap free
Amount of items: 3
Items: 
Size: 637509 Color: 4
Size: 219775 Color: 17
Size: 142680 Color: 19

Bin 2049: 37 of cap free
Amount of items: 2
Items: 
Size: 764438 Color: 17
Size: 235526 Color: 6

Bin 2050: 37 of cap free
Amount of items: 2
Items: 
Size: 781624 Color: 0
Size: 218340 Color: 2

Bin 2051: 37 of cap free
Amount of items: 3
Items: 
Size: 726616 Color: 12
Size: 138097 Color: 6
Size: 135251 Color: 12

Bin 2052: 37 of cap free
Amount of items: 2
Items: 
Size: 798421 Color: 10
Size: 201543 Color: 11

Bin 2053: 37 of cap free
Amount of items: 2
Items: 
Size: 777908 Color: 2
Size: 222056 Color: 17

Bin 2054: 37 of cap free
Amount of items: 2
Items: 
Size: 719838 Color: 12
Size: 280126 Color: 5

Bin 2055: 37 of cap free
Amount of items: 2
Items: 
Size: 530045 Color: 0
Size: 469919 Color: 7

Bin 2056: 37 of cap free
Amount of items: 2
Items: 
Size: 541052 Color: 15
Size: 458912 Color: 5

Bin 2057: 37 of cap free
Amount of items: 2
Items: 
Size: 553225 Color: 15
Size: 446739 Color: 13

Bin 2058: 37 of cap free
Amount of items: 2
Items: 
Size: 563152 Color: 18
Size: 436812 Color: 3

Bin 2059: 37 of cap free
Amount of items: 2
Items: 
Size: 572661 Color: 18
Size: 427303 Color: 0

Bin 2060: 37 of cap free
Amount of items: 2
Items: 
Size: 588745 Color: 4
Size: 411219 Color: 15

Bin 2061: 37 of cap free
Amount of items: 2
Items: 
Size: 616025 Color: 15
Size: 383939 Color: 10

Bin 2062: 37 of cap free
Amount of items: 2
Items: 
Size: 631193 Color: 17
Size: 368771 Color: 12

Bin 2063: 37 of cap free
Amount of items: 2
Items: 
Size: 650050 Color: 12
Size: 349914 Color: 10

Bin 2064: 37 of cap free
Amount of items: 2
Items: 
Size: 660150 Color: 12
Size: 339814 Color: 17

Bin 2065: 37 of cap free
Amount of items: 2
Items: 
Size: 666585 Color: 6
Size: 333379 Color: 0

Bin 2066: 37 of cap free
Amount of items: 2
Items: 
Size: 685507 Color: 12
Size: 314457 Color: 19

Bin 2067: 37 of cap free
Amount of items: 2
Items: 
Size: 722905 Color: 5
Size: 277059 Color: 10

Bin 2068: 37 of cap free
Amount of items: 2
Items: 
Size: 726825 Color: 9
Size: 273139 Color: 16

Bin 2069: 37 of cap free
Amount of items: 2
Items: 
Size: 754857 Color: 14
Size: 245107 Color: 15

Bin 2070: 37 of cap free
Amount of items: 2
Items: 
Size: 762867 Color: 17
Size: 237097 Color: 3

Bin 2071: 37 of cap free
Amount of items: 2
Items: 
Size: 778573 Color: 12
Size: 221391 Color: 2

Bin 2072: 37 of cap free
Amount of items: 2
Items: 
Size: 780883 Color: 13
Size: 219081 Color: 4

Bin 2073: 38 of cap free
Amount of items: 2
Items: 
Size: 509532 Color: 11
Size: 490431 Color: 1

Bin 2074: 38 of cap free
Amount of items: 3
Items: 
Size: 676424 Color: 1
Size: 162051 Color: 2
Size: 161488 Color: 2

Bin 2075: 38 of cap free
Amount of items: 2
Items: 
Size: 607119 Color: 2
Size: 392844 Color: 9

Bin 2076: 38 of cap free
Amount of items: 2
Items: 
Size: 616152 Color: 9
Size: 383811 Color: 13

Bin 2077: 38 of cap free
Amount of items: 2
Items: 
Size: 648721 Color: 12
Size: 351242 Color: 11

Bin 2078: 38 of cap free
Amount of items: 2
Items: 
Size: 509464 Color: 5
Size: 490499 Color: 9

Bin 2079: 38 of cap free
Amount of items: 2
Items: 
Size: 510762 Color: 9
Size: 489201 Color: 0

Bin 2080: 38 of cap free
Amount of items: 2
Items: 
Size: 518767 Color: 12
Size: 481196 Color: 13

Bin 2081: 38 of cap free
Amount of items: 2
Items: 
Size: 523273 Color: 1
Size: 476690 Color: 11

Bin 2082: 38 of cap free
Amount of items: 2
Items: 
Size: 533544 Color: 18
Size: 466419 Color: 8

Bin 2083: 38 of cap free
Amount of items: 2
Items: 
Size: 534067 Color: 18
Size: 465896 Color: 14

Bin 2084: 38 of cap free
Amount of items: 2
Items: 
Size: 571051 Color: 11
Size: 428912 Color: 3

Bin 2085: 38 of cap free
Amount of items: 2
Items: 
Size: 595930 Color: 10
Size: 404033 Color: 8

Bin 2086: 38 of cap free
Amount of items: 2
Items: 
Size: 609521 Color: 11
Size: 390442 Color: 19

Bin 2087: 38 of cap free
Amount of items: 2
Items: 
Size: 616285 Color: 0
Size: 383678 Color: 3

Bin 2088: 38 of cap free
Amount of items: 2
Items: 
Size: 646657 Color: 12
Size: 353306 Color: 15

Bin 2089: 38 of cap free
Amount of items: 2
Items: 
Size: 652789 Color: 6
Size: 347174 Color: 3

Bin 2090: 38 of cap free
Amount of items: 2
Items: 
Size: 661300 Color: 18
Size: 338663 Color: 3

Bin 2091: 38 of cap free
Amount of items: 2
Items: 
Size: 682450 Color: 2
Size: 317513 Color: 15

Bin 2092: 38 of cap free
Amount of items: 2
Items: 
Size: 683004 Color: 18
Size: 316959 Color: 13

Bin 2093: 38 of cap free
Amount of items: 2
Items: 
Size: 780484 Color: 7
Size: 219479 Color: 8

Bin 2094: 39 of cap free
Amount of items: 3
Items: 
Size: 766516 Color: 9
Size: 131983 Color: 19
Size: 101463 Color: 3

Bin 2095: 39 of cap free
Amount of items: 3
Items: 
Size: 605722 Color: 0
Size: 198763 Color: 1
Size: 195477 Color: 14

Bin 2096: 39 of cap free
Amount of items: 2
Items: 
Size: 753165 Color: 10
Size: 246797 Color: 14

Bin 2097: 39 of cap free
Amount of items: 2
Items: 
Size: 722967 Color: 12
Size: 276995 Color: 3

Bin 2098: 39 of cap free
Amount of items: 2
Items: 
Size: 797250 Color: 2
Size: 202712 Color: 19

Bin 2099: 39 of cap free
Amount of items: 2
Items: 
Size: 661632 Color: 6
Size: 338330 Color: 3

Bin 2100: 39 of cap free
Amount of items: 2
Items: 
Size: 641441 Color: 8
Size: 358521 Color: 5

Bin 2101: 39 of cap free
Amount of items: 2
Items: 
Size: 657110 Color: 11
Size: 342852 Color: 3

Bin 2102: 39 of cap free
Amount of items: 2
Items: 
Size: 502395 Color: 18
Size: 497567 Color: 17

Bin 2103: 39 of cap free
Amount of items: 2
Items: 
Size: 519334 Color: 16
Size: 480628 Color: 5

Bin 2104: 39 of cap free
Amount of items: 2
Items: 
Size: 547479 Color: 10
Size: 452483 Color: 6

Bin 2105: 39 of cap free
Amount of items: 2
Items: 
Size: 582427 Color: 7
Size: 417535 Color: 13

Bin 2106: 39 of cap free
Amount of items: 2
Items: 
Size: 611581 Color: 18
Size: 388381 Color: 4

Bin 2107: 39 of cap free
Amount of items: 2
Items: 
Size: 633263 Color: 17
Size: 366699 Color: 6

Bin 2108: 39 of cap free
Amount of items: 2
Items: 
Size: 633829 Color: 12
Size: 366133 Color: 1

Bin 2109: 39 of cap free
Amount of items: 2
Items: 
Size: 661749 Color: 6
Size: 338213 Color: 7

Bin 2110: 39 of cap free
Amount of items: 2
Items: 
Size: 667973 Color: 1
Size: 331989 Color: 19

Bin 2111: 39 of cap free
Amount of items: 2
Items: 
Size: 670405 Color: 10
Size: 329557 Color: 9

Bin 2112: 39 of cap free
Amount of items: 2
Items: 
Size: 698806 Color: 12
Size: 301156 Color: 5

Bin 2113: 39 of cap free
Amount of items: 2
Items: 
Size: 722669 Color: 7
Size: 277293 Color: 9

Bin 2114: 39 of cap free
Amount of items: 2
Items: 
Size: 753988 Color: 8
Size: 245974 Color: 10

Bin 2115: 40 of cap free
Amount of items: 3
Items: 
Size: 677908 Color: 3
Size: 163030 Color: 9
Size: 159023 Color: 3

Bin 2116: 40 of cap free
Amount of items: 2
Items: 
Size: 692160 Color: 14
Size: 307801 Color: 11

Bin 2117: 40 of cap free
Amount of items: 2
Items: 
Size: 653545 Color: 17
Size: 346416 Color: 0

Bin 2118: 40 of cap free
Amount of items: 2
Items: 
Size: 767229 Color: 12
Size: 232732 Color: 3

Bin 2119: 40 of cap free
Amount of items: 2
Items: 
Size: 754915 Color: 4
Size: 245046 Color: 3

Bin 2120: 40 of cap free
Amount of items: 2
Items: 
Size: 795733 Color: 17
Size: 204228 Color: 18

Bin 2121: 40 of cap free
Amount of items: 2
Items: 
Size: 689795 Color: 16
Size: 310166 Color: 11

Bin 2122: 40 of cap free
Amount of items: 2
Items: 
Size: 553939 Color: 2
Size: 446022 Color: 0

Bin 2123: 40 of cap free
Amount of items: 2
Items: 
Size: 639345 Color: 2
Size: 360616 Color: 16

Bin 2124: 40 of cap free
Amount of items: 2
Items: 
Size: 683061 Color: 2
Size: 316900 Color: 7

Bin 2125: 40 of cap free
Amount of items: 2
Items: 
Size: 727925 Color: 14
Size: 272036 Color: 9

Bin 2126: 40 of cap free
Amount of items: 2
Items: 
Size: 769578 Color: 7
Size: 230383 Color: 9

Bin 2127: 40 of cap free
Amount of items: 2
Items: 
Size: 770993 Color: 8
Size: 228968 Color: 2

Bin 2128: 41 of cap free
Amount of items: 3
Items: 
Size: 460977 Color: 10
Size: 271318 Color: 12
Size: 267665 Color: 12

Bin 2129: 41 of cap free
Amount of items: 2
Items: 
Size: 622336 Color: 9
Size: 377624 Color: 8

Bin 2130: 41 of cap free
Amount of items: 2
Items: 
Size: 693392 Color: 12
Size: 306568 Color: 18

Bin 2131: 41 of cap free
Amount of items: 2
Items: 
Size: 762165 Color: 14
Size: 237795 Color: 6

Bin 2132: 41 of cap free
Amount of items: 2
Items: 
Size: 789684 Color: 19
Size: 210276 Color: 14

Bin 2133: 41 of cap free
Amount of items: 2
Items: 
Size: 514897 Color: 18
Size: 485063 Color: 14

Bin 2134: 41 of cap free
Amount of items: 2
Items: 
Size: 515664 Color: 13
Size: 484296 Color: 10

Bin 2135: 41 of cap free
Amount of items: 2
Items: 
Size: 536030 Color: 14
Size: 463930 Color: 15

Bin 2136: 41 of cap free
Amount of items: 2
Items: 
Size: 543207 Color: 2
Size: 456753 Color: 10

Bin 2137: 41 of cap free
Amount of items: 2
Items: 
Size: 556290 Color: 18
Size: 443670 Color: 15

Bin 2138: 41 of cap free
Amount of items: 2
Items: 
Size: 715987 Color: 9
Size: 283973 Color: 4

Bin 2139: 41 of cap free
Amount of items: 2
Items: 
Size: 720117 Color: 18
Size: 279843 Color: 9

Bin 2140: 41 of cap free
Amount of items: 2
Items: 
Size: 756682 Color: 0
Size: 243278 Color: 17

Bin 2141: 41 of cap free
Amount of items: 2
Items: 
Size: 784072 Color: 19
Size: 215888 Color: 6

Bin 2142: 42 of cap free
Amount of items: 2
Items: 
Size: 647787 Color: 3
Size: 352172 Color: 12

Bin 2143: 42 of cap free
Amount of items: 2
Items: 
Size: 779619 Color: 17
Size: 220340 Color: 19

Bin 2144: 42 of cap free
Amount of items: 2
Items: 
Size: 695911 Color: 10
Size: 304048 Color: 6

Bin 2145: 42 of cap free
Amount of items: 2
Items: 
Size: 501041 Color: 11
Size: 498918 Color: 18

Bin 2146: 42 of cap free
Amount of items: 2
Items: 
Size: 503826 Color: 9
Size: 496133 Color: 16

Bin 2147: 42 of cap free
Amount of items: 2
Items: 
Size: 520661 Color: 1
Size: 479298 Color: 4

Bin 2148: 42 of cap free
Amount of items: 2
Items: 
Size: 533040 Color: 10
Size: 466919 Color: 8

Bin 2149: 42 of cap free
Amount of items: 2
Items: 
Size: 539118 Color: 14
Size: 460841 Color: 3

Bin 2150: 42 of cap free
Amount of items: 2
Items: 
Size: 561552 Color: 3
Size: 438407 Color: 0

Bin 2151: 42 of cap free
Amount of items: 2
Items: 
Size: 570802 Color: 5
Size: 429157 Color: 0

Bin 2152: 42 of cap free
Amount of items: 2
Items: 
Size: 578044 Color: 12
Size: 421915 Color: 8

Bin 2153: 42 of cap free
Amount of items: 2
Items: 
Size: 599159 Color: 2
Size: 400800 Color: 12

Bin 2154: 42 of cap free
Amount of items: 2
Items: 
Size: 601472 Color: 12
Size: 398487 Color: 3

Bin 2155: 42 of cap free
Amount of items: 2
Items: 
Size: 612385 Color: 19
Size: 387574 Color: 10

Bin 2156: 42 of cap free
Amount of items: 2
Items: 
Size: 686499 Color: 2
Size: 313460 Color: 13

Bin 2157: 42 of cap free
Amount of items: 2
Items: 
Size: 715420 Color: 0
Size: 284539 Color: 8

Bin 2158: 42 of cap free
Amount of items: 2
Items: 
Size: 739230 Color: 9
Size: 260729 Color: 4

Bin 2159: 42 of cap free
Amount of items: 2
Items: 
Size: 750942 Color: 14
Size: 249017 Color: 17

Bin 2160: 42 of cap free
Amount of items: 2
Items: 
Size: 759525 Color: 7
Size: 240434 Color: 1

Bin 2161: 42 of cap free
Amount of items: 2
Items: 
Size: 768534 Color: 17
Size: 231425 Color: 14

Bin 2162: 43 of cap free
Amount of items: 2
Items: 
Size: 506874 Color: 8
Size: 493084 Color: 12

Bin 2163: 43 of cap free
Amount of items: 2
Items: 
Size: 797011 Color: 17
Size: 202947 Color: 3

Bin 2164: 43 of cap free
Amount of items: 2
Items: 
Size: 707838 Color: 2
Size: 292120 Color: 12

Bin 2165: 43 of cap free
Amount of items: 2
Items: 
Size: 690372 Color: 14
Size: 309586 Color: 1

Bin 2166: 43 of cap free
Amount of items: 2
Items: 
Size: 733406 Color: 16
Size: 266552 Color: 18

Bin 2167: 43 of cap free
Amount of items: 3
Items: 
Size: 664506 Color: 3
Size: 168138 Color: 11
Size: 167314 Color: 17

Bin 2168: 43 of cap free
Amount of items: 2
Items: 
Size: 690266 Color: 11
Size: 309692 Color: 10

Bin 2169: 43 of cap free
Amount of items: 2
Items: 
Size: 517080 Color: 4
Size: 482878 Color: 18

Bin 2170: 43 of cap free
Amount of items: 2
Items: 
Size: 544953 Color: 6
Size: 455005 Color: 1

Bin 2171: 43 of cap free
Amount of items: 2
Items: 
Size: 560401 Color: 8
Size: 439557 Color: 10

Bin 2172: 43 of cap free
Amount of items: 2
Items: 
Size: 577787 Color: 7
Size: 422171 Color: 16

Bin 2173: 43 of cap free
Amount of items: 2
Items: 
Size: 594288 Color: 4
Size: 405670 Color: 0

Bin 2174: 43 of cap free
Amount of items: 2
Items: 
Size: 612108 Color: 6
Size: 387850 Color: 16

Bin 2175: 43 of cap free
Amount of items: 2
Items: 
Size: 636106 Color: 12
Size: 363852 Color: 9

Bin 2176: 43 of cap free
Amount of items: 2
Items: 
Size: 640590 Color: 12
Size: 359368 Color: 13

Bin 2177: 43 of cap free
Amount of items: 2
Items: 
Size: 645655 Color: 0
Size: 354303 Color: 18

Bin 2178: 43 of cap free
Amount of items: 2
Items: 
Size: 667720 Color: 13
Size: 332238 Color: 9

Bin 2179: 43 of cap free
Amount of items: 2
Items: 
Size: 671244 Color: 3
Size: 328714 Color: 18

Bin 2180: 43 of cap free
Amount of items: 3
Items: 
Size: 696841 Color: 5
Size: 151621 Color: 5
Size: 151496 Color: 12

Bin 2181: 43 of cap free
Amount of items: 2
Items: 
Size: 746738 Color: 5
Size: 253220 Color: 1

Bin 2182: 43 of cap free
Amount of items: 2
Items: 
Size: 756358 Color: 6
Size: 243600 Color: 12

Bin 2183: 43 of cap free
Amount of items: 2
Items: 
Size: 791506 Color: 4
Size: 208452 Color: 3

Bin 2184: 44 of cap free
Amount of items: 3
Items: 
Size: 553064 Color: 1
Size: 299298 Color: 9
Size: 147595 Color: 7

Bin 2185: 44 of cap free
Amount of items: 2
Items: 
Size: 701465 Color: 14
Size: 298492 Color: 0

Bin 2186: 44 of cap free
Amount of items: 3
Items: 
Size: 614776 Color: 7
Size: 193480 Color: 12
Size: 191701 Color: 7

Bin 2187: 44 of cap free
Amount of items: 2
Items: 
Size: 763250 Color: 1
Size: 236707 Color: 0

Bin 2188: 44 of cap free
Amount of items: 2
Items: 
Size: 769637 Color: 6
Size: 230320 Color: 10

Bin 2189: 44 of cap free
Amount of items: 2
Items: 
Size: 504551 Color: 5
Size: 495406 Color: 1

Bin 2190: 44 of cap free
Amount of items: 2
Items: 
Size: 619283 Color: 17
Size: 380674 Color: 12

Bin 2191: 44 of cap free
Amount of items: 2
Items: 
Size: 694935 Color: 4
Size: 305022 Color: 11

Bin 2192: 44 of cap free
Amount of items: 2
Items: 
Size: 521423 Color: 7
Size: 478534 Color: 9

Bin 2193: 44 of cap free
Amount of items: 2
Items: 
Size: 547918 Color: 6
Size: 452039 Color: 14

Bin 2194: 44 of cap free
Amount of items: 3
Items: 
Size: 561736 Color: 19
Size: 219354 Color: 17
Size: 218867 Color: 6

Bin 2195: 44 of cap free
Amount of items: 2
Items: 
Size: 586744 Color: 10
Size: 413213 Color: 14

Bin 2196: 44 of cap free
Amount of items: 2
Items: 
Size: 590245 Color: 5
Size: 409712 Color: 7

Bin 2197: 44 of cap free
Amount of items: 2
Items: 
Size: 592233 Color: 11
Size: 407724 Color: 7

Bin 2198: 44 of cap free
Amount of items: 2
Items: 
Size: 597898 Color: 10
Size: 402059 Color: 11

Bin 2199: 44 of cap free
Amount of items: 2
Items: 
Size: 653084 Color: 5
Size: 346873 Color: 12

Bin 2200: 44 of cap free
Amount of items: 2
Items: 
Size: 680657 Color: 10
Size: 319300 Color: 15

Bin 2201: 44 of cap free
Amount of items: 2
Items: 
Size: 686887 Color: 17
Size: 313070 Color: 11

Bin 2202: 44 of cap free
Amount of items: 2
Items: 
Size: 690933 Color: 18
Size: 309024 Color: 13

Bin 2203: 45 of cap free
Amount of items: 3
Items: 
Size: 750119 Color: 0
Size: 134421 Color: 14
Size: 115416 Color: 9

Bin 2204: 45 of cap free
Amount of items: 2
Items: 
Size: 675835 Color: 4
Size: 324121 Color: 6

Bin 2205: 45 of cap free
Amount of items: 3
Items: 
Size: 467965 Color: 12
Size: 267319 Color: 6
Size: 264672 Color: 19

Bin 2206: 45 of cap free
Amount of items: 3
Items: 
Size: 744476 Color: 8
Size: 131161 Color: 15
Size: 124319 Color: 7

Bin 2207: 45 of cap free
Amount of items: 2
Items: 
Size: 779089 Color: 17
Size: 220867 Color: 14

Bin 2208: 45 of cap free
Amount of items: 3
Items: 
Size: 639658 Color: 14
Size: 199773 Color: 4
Size: 160525 Color: 2

Bin 2209: 45 of cap free
Amount of items: 2
Items: 
Size: 724161 Color: 5
Size: 275795 Color: 17

Bin 2210: 45 of cap free
Amount of items: 2
Items: 
Size: 613236 Color: 13
Size: 386720 Color: 5

Bin 2211: 45 of cap free
Amount of items: 2
Items: 
Size: 755584 Color: 19
Size: 244372 Color: 5

Bin 2212: 45 of cap free
Amount of items: 3
Items: 
Size: 396391 Color: 11
Size: 367559 Color: 0
Size: 236006 Color: 15

Bin 2213: 45 of cap free
Amount of items: 2
Items: 
Size: 606067 Color: 3
Size: 393889 Color: 9

Bin 2214: 45 of cap free
Amount of items: 2
Items: 
Size: 739411 Color: 3
Size: 260545 Color: 16

Bin 2215: 45 of cap free
Amount of items: 2
Items: 
Size: 514086 Color: 10
Size: 485870 Color: 12

Bin 2216: 45 of cap free
Amount of items: 2
Items: 
Size: 535382 Color: 4
Size: 464574 Color: 8

Bin 2217: 45 of cap free
Amount of items: 2
Items: 
Size: 535863 Color: 8
Size: 464093 Color: 2

Bin 2218: 45 of cap free
Amount of items: 2
Items: 
Size: 537154 Color: 16
Size: 462802 Color: 17

Bin 2219: 45 of cap free
Amount of items: 2
Items: 
Size: 552280 Color: 8
Size: 447676 Color: 0

Bin 2220: 45 of cap free
Amount of items: 2
Items: 
Size: 554070 Color: 8
Size: 445886 Color: 1

Bin 2221: 45 of cap free
Amount of items: 2
Items: 
Size: 587113 Color: 0
Size: 412843 Color: 6

Bin 2222: 45 of cap free
Amount of items: 2
Items: 
Size: 602361 Color: 7
Size: 397595 Color: 0

Bin 2223: 45 of cap free
Amount of items: 2
Items: 
Size: 684387 Color: 16
Size: 315569 Color: 2

Bin 2224: 45 of cap free
Amount of items: 2
Items: 
Size: 725734 Color: 18
Size: 274222 Color: 7

Bin 2225: 46 of cap free
Amount of items: 3
Items: 
Size: 675856 Color: 17
Size: 162226 Color: 18
Size: 161873 Color: 4

Bin 2226: 46 of cap free
Amount of items: 3
Items: 
Size: 490955 Color: 19
Size: 255616 Color: 0
Size: 253384 Color: 6

Bin 2227: 46 of cap free
Amount of items: 3
Items: 
Size: 659584 Color: 6
Size: 170531 Color: 10
Size: 169840 Color: 13

Bin 2228: 46 of cap free
Amount of items: 2
Items: 
Size: 712123 Color: 14
Size: 287832 Color: 5

Bin 2229: 46 of cap free
Amount of items: 2
Items: 
Size: 719046 Color: 19
Size: 280909 Color: 1

Bin 2230: 46 of cap free
Amount of items: 2
Items: 
Size: 505694 Color: 15
Size: 494261 Color: 13

Bin 2231: 46 of cap free
Amount of items: 3
Items: 
Size: 370454 Color: 9
Size: 329422 Color: 10
Size: 300079 Color: 5

Bin 2232: 46 of cap free
Amount of items: 2
Items: 
Size: 500516 Color: 19
Size: 499439 Color: 4

Bin 2233: 46 of cap free
Amount of items: 2
Items: 
Size: 511941 Color: 1
Size: 488014 Color: 2

Bin 2234: 46 of cap free
Amount of items: 2
Items: 
Size: 531509 Color: 3
Size: 468446 Color: 8

Bin 2235: 46 of cap free
Amount of items: 2
Items: 
Size: 539268 Color: 10
Size: 460687 Color: 3

Bin 2236: 46 of cap free
Amount of items: 2
Items: 
Size: 547462 Color: 9
Size: 452493 Color: 2

Bin 2237: 46 of cap free
Amount of items: 2
Items: 
Size: 567189 Color: 0
Size: 432766 Color: 1

Bin 2238: 46 of cap free
Amount of items: 3
Items: 
Size: 603030 Color: 3
Size: 198580 Color: 18
Size: 198345 Color: 3

Bin 2239: 46 of cap free
Amount of items: 2
Items: 
Size: 619709 Color: 3
Size: 380246 Color: 10

Bin 2240: 46 of cap free
Amount of items: 2
Items: 
Size: 627969 Color: 5
Size: 371986 Color: 15

Bin 2241: 46 of cap free
Amount of items: 2
Items: 
Size: 737042 Color: 0
Size: 262913 Color: 18

Bin 2242: 46 of cap free
Amount of items: 2
Items: 
Size: 757404 Color: 7
Size: 242551 Color: 3

Bin 2243: 46 of cap free
Amount of items: 2
Items: 
Size: 780576 Color: 19
Size: 219379 Color: 13

Bin 2244: 46 of cap free
Amount of items: 2
Items: 
Size: 786062 Color: 16
Size: 213893 Color: 10

Bin 2245: 47 of cap free
Amount of items: 2
Items: 
Size: 751854 Color: 11
Size: 248100 Color: 3

Bin 2246: 47 of cap free
Amount of items: 2
Items: 
Size: 797129 Color: 14
Size: 202825 Color: 2

Bin 2247: 47 of cap free
Amount of items: 3
Items: 
Size: 650810 Color: 4
Size: 180811 Color: 4
Size: 168333 Color: 9

Bin 2248: 47 of cap free
Amount of items: 2
Items: 
Size: 733402 Color: 18
Size: 266552 Color: 9

Bin 2249: 47 of cap free
Amount of items: 2
Items: 
Size: 606994 Color: 13
Size: 392960 Color: 1

Bin 2250: 47 of cap free
Amount of items: 2
Items: 
Size: 732693 Color: 16
Size: 267261 Color: 10

Bin 2251: 47 of cap free
Amount of items: 2
Items: 
Size: 535103 Color: 18
Size: 464851 Color: 10

Bin 2252: 47 of cap free
Amount of items: 2
Items: 
Size: 538579 Color: 13
Size: 461375 Color: 17

Bin 2253: 47 of cap free
Amount of items: 2
Items: 
Size: 548784 Color: 16
Size: 451170 Color: 0

Bin 2254: 47 of cap free
Amount of items: 2
Items: 
Size: 566399 Color: 1
Size: 433555 Color: 6

Bin 2255: 47 of cap free
Amount of items: 2
Items: 
Size: 575271 Color: 5
Size: 424683 Color: 1

Bin 2256: 47 of cap free
Amount of items: 2
Items: 
Size: 621622 Color: 4
Size: 378332 Color: 19

Bin 2257: 47 of cap free
Amount of items: 2
Items: 
Size: 650370 Color: 3
Size: 349584 Color: 14

Bin 2258: 47 of cap free
Amount of items: 2
Items: 
Size: 707087 Color: 17
Size: 292867 Color: 19

Bin 2259: 47 of cap free
Amount of items: 2
Items: 
Size: 720317 Color: 6
Size: 279637 Color: 15

Bin 2260: 48 of cap free
Amount of items: 2
Items: 
Size: 585156 Color: 15
Size: 414797 Color: 12

Bin 2261: 48 of cap free
Amount of items: 2
Items: 
Size: 671024 Color: 15
Size: 328929 Color: 10

Bin 2262: 48 of cap free
Amount of items: 2
Items: 
Size: 771752 Color: 16
Size: 228201 Color: 17

Bin 2263: 48 of cap free
Amount of items: 2
Items: 
Size: 787101 Color: 0
Size: 212852 Color: 9

Bin 2264: 48 of cap free
Amount of items: 2
Items: 
Size: 502617 Color: 16
Size: 497336 Color: 15

Bin 2265: 48 of cap free
Amount of items: 2
Items: 
Size: 517516 Color: 3
Size: 482437 Color: 16

Bin 2266: 48 of cap free
Amount of items: 2
Items: 
Size: 525874 Color: 2
Size: 474079 Color: 9

Bin 2267: 48 of cap free
Amount of items: 2
Items: 
Size: 527388 Color: 12
Size: 472565 Color: 17

Bin 2268: 48 of cap free
Amount of items: 2
Items: 
Size: 527640 Color: 16
Size: 472313 Color: 15

Bin 2269: 48 of cap free
Amount of items: 2
Items: 
Size: 530453 Color: 5
Size: 469500 Color: 9

Bin 2270: 48 of cap free
Amount of items: 2
Items: 
Size: 579875 Color: 16
Size: 420078 Color: 18

Bin 2271: 48 of cap free
Amount of items: 2
Items: 
Size: 602311 Color: 15
Size: 397642 Color: 4

Bin 2272: 48 of cap free
Amount of items: 2
Items: 
Size: 617496 Color: 7
Size: 382457 Color: 6

Bin 2273: 48 of cap free
Amount of items: 2
Items: 
Size: 627384 Color: 14
Size: 372569 Color: 10

Bin 2274: 48 of cap free
Amount of items: 2
Items: 
Size: 653180 Color: 0
Size: 346773 Color: 10

Bin 2275: 48 of cap free
Amount of items: 2
Items: 
Size: 664347 Color: 0
Size: 335606 Color: 12

Bin 2276: 48 of cap free
Amount of items: 2
Items: 
Size: 756896 Color: 0
Size: 243057 Color: 1

Bin 2277: 49 of cap free
Amount of items: 3
Items: 
Size: 570564 Color: 1
Size: 215167 Color: 10
Size: 214221 Color: 3

Bin 2278: 49 of cap free
Amount of items: 2
Items: 
Size: 556240 Color: 17
Size: 443712 Color: 19

Bin 2279: 49 of cap free
Amount of items: 2
Items: 
Size: 768098 Color: 15
Size: 231854 Color: 5

Bin 2280: 49 of cap free
Amount of items: 2
Items: 
Size: 690042 Color: 16
Size: 309910 Color: 2

Bin 2281: 49 of cap free
Amount of items: 2
Items: 
Size: 751756 Color: 5
Size: 248196 Color: 2

Bin 2282: 49 of cap free
Amount of items: 2
Items: 
Size: 656451 Color: 17
Size: 343501 Color: 0

Bin 2283: 49 of cap free
Amount of items: 2
Items: 
Size: 711125 Color: 15
Size: 288827 Color: 10

Bin 2284: 49 of cap free
Amount of items: 2
Items: 
Size: 556678 Color: 17
Size: 443274 Color: 10

Bin 2285: 49 of cap free
Amount of items: 2
Items: 
Size: 701032 Color: 6
Size: 298920 Color: 10

Bin 2286: 49 of cap free
Amount of items: 2
Items: 
Size: 736133 Color: 5
Size: 263819 Color: 11

Bin 2287: 49 of cap free
Amount of items: 2
Items: 
Size: 521470 Color: 17
Size: 478482 Color: 8

Bin 2288: 49 of cap free
Amount of items: 2
Items: 
Size: 530805 Color: 16
Size: 469147 Color: 11

Bin 2289: 49 of cap free
Amount of items: 2
Items: 
Size: 569943 Color: 16
Size: 430009 Color: 15

Bin 2290: 49 of cap free
Amount of items: 2
Items: 
Size: 573404 Color: 5
Size: 426548 Color: 19

Bin 2291: 49 of cap free
Amount of items: 2
Items: 
Size: 586043 Color: 0
Size: 413909 Color: 11

Bin 2292: 49 of cap free
Amount of items: 2
Items: 
Size: 595974 Color: 0
Size: 403978 Color: 7

Bin 2293: 49 of cap free
Amount of items: 2
Items: 
Size: 640825 Color: 4
Size: 359127 Color: 5

Bin 2294: 49 of cap free
Amount of items: 2
Items: 
Size: 646657 Color: 10
Size: 353295 Color: 2

Bin 2295: 49 of cap free
Amount of items: 2
Items: 
Size: 657359 Color: 5
Size: 342593 Color: 4

Bin 2296: 49 of cap free
Amount of items: 2
Items: 
Size: 738705 Color: 10
Size: 261247 Color: 17

Bin 2297: 49 of cap free
Amount of items: 2
Items: 
Size: 758068 Color: 7
Size: 241884 Color: 19

Bin 2298: 49 of cap free
Amount of items: 2
Items: 
Size: 762857 Color: 6
Size: 237095 Color: 3

Bin 2299: 50 of cap free
Amount of items: 2
Items: 
Size: 738034 Color: 10
Size: 261917 Color: 8

Bin 2300: 50 of cap free
Amount of items: 2
Items: 
Size: 677504 Color: 10
Size: 322447 Color: 17

Bin 2301: 50 of cap free
Amount of items: 3
Items: 
Size: 681265 Color: 14
Size: 160061 Color: 7
Size: 158625 Color: 4

Bin 2302: 50 of cap free
Amount of items: 2
Items: 
Size: 697042 Color: 12
Size: 302909 Color: 16

Bin 2303: 50 of cap free
Amount of items: 2
Items: 
Size: 719223 Color: 11
Size: 280728 Color: 6

Bin 2304: 50 of cap free
Amount of items: 2
Items: 
Size: 612505 Color: 15
Size: 387446 Color: 2

Bin 2305: 50 of cap free
Amount of items: 2
Items: 
Size: 656281 Color: 2
Size: 343670 Color: 9

Bin 2306: 50 of cap free
Amount of items: 2
Items: 
Size: 523771 Color: 4
Size: 476180 Color: 19

Bin 2307: 50 of cap free
Amount of items: 2
Items: 
Size: 529116 Color: 12
Size: 470835 Color: 3

Bin 2308: 50 of cap free
Amount of items: 2
Items: 
Size: 572945 Color: 8
Size: 427006 Color: 3

Bin 2309: 50 of cap free
Amount of items: 2
Items: 
Size: 595089 Color: 5
Size: 404862 Color: 9

Bin 2310: 50 of cap free
Amount of items: 2
Items: 
Size: 614499 Color: 0
Size: 385452 Color: 4

Bin 2311: 50 of cap free
Amount of items: 2
Items: 
Size: 624110 Color: 8
Size: 375841 Color: 1

Bin 2312: 50 of cap free
Amount of items: 2
Items: 
Size: 629806 Color: 2
Size: 370145 Color: 5

Bin 2313: 50 of cap free
Amount of items: 2
Items: 
Size: 637200 Color: 16
Size: 362751 Color: 12

Bin 2314: 50 of cap free
Amount of items: 2
Items: 
Size: 679397 Color: 1
Size: 320554 Color: 7

Bin 2315: 50 of cap free
Amount of items: 2
Items: 
Size: 683686 Color: 0
Size: 316265 Color: 17

Bin 2316: 50 of cap free
Amount of items: 2
Items: 
Size: 725189 Color: 19
Size: 274762 Color: 13

Bin 2317: 50 of cap free
Amount of items: 2
Items: 
Size: 767720 Color: 13
Size: 232231 Color: 5

Bin 2318: 51 of cap free
Amount of items: 3
Items: 
Size: 745153 Color: 6
Size: 137364 Color: 12
Size: 117433 Color: 6

Bin 2319: 51 of cap free
Amount of items: 3
Items: 
Size: 768999 Color: 8
Size: 116212 Color: 6
Size: 114739 Color: 5

Bin 2320: 51 of cap free
Amount of items: 2
Items: 
Size: 655094 Color: 11
Size: 344856 Color: 13

Bin 2321: 51 of cap free
Amount of items: 3
Items: 
Size: 363766 Color: 8
Size: 329635 Color: 3
Size: 306549 Color: 5

Bin 2322: 51 of cap free
Amount of items: 2
Items: 
Size: 702832 Color: 15
Size: 297118 Color: 1

Bin 2323: 51 of cap free
Amount of items: 2
Items: 
Size: 702778 Color: 17
Size: 297172 Color: 19

Bin 2324: 51 of cap free
Amount of items: 3
Items: 
Size: 640270 Color: 6
Size: 179914 Color: 13
Size: 179766 Color: 18

Bin 2325: 51 of cap free
Amount of items: 3
Items: 
Size: 362552 Color: 13
Size: 345973 Color: 8
Size: 291425 Color: 17

Bin 2326: 51 of cap free
Amount of items: 2
Items: 
Size: 533684 Color: 11
Size: 466266 Color: 16

Bin 2327: 51 of cap free
Amount of items: 2
Items: 
Size: 544073 Color: 2
Size: 455877 Color: 4

Bin 2328: 51 of cap free
Amount of items: 2
Items: 
Size: 546888 Color: 7
Size: 453062 Color: 5

Bin 2329: 51 of cap free
Amount of items: 2
Items: 
Size: 563441 Color: 16
Size: 436509 Color: 10

Bin 2330: 51 of cap free
Amount of items: 2
Items: 
Size: 603437 Color: 4
Size: 396513 Color: 7

Bin 2331: 51 of cap free
Amount of items: 2
Items: 
Size: 630864 Color: 0
Size: 369086 Color: 3

Bin 2332: 51 of cap free
Amount of items: 2
Items: 
Size: 639033 Color: 10
Size: 360917 Color: 13

Bin 2333: 51 of cap free
Amount of items: 2
Items: 
Size: 650585 Color: 14
Size: 349365 Color: 8

Bin 2334: 51 of cap free
Amount of items: 2
Items: 
Size: 657772 Color: 8
Size: 342178 Color: 13

Bin 2335: 51 of cap free
Amount of items: 2
Items: 
Size: 751416 Color: 10
Size: 248534 Color: 0

Bin 2336: 51 of cap free
Amount of items: 2
Items: 
Size: 781279 Color: 6
Size: 218671 Color: 16

Bin 2337: 52 of cap free
Amount of items: 3
Items: 
Size: 587253 Color: 7
Size: 256894 Color: 11
Size: 155802 Color: 14

Bin 2338: 52 of cap free
Amount of items: 2
Items: 
Size: 773160 Color: 16
Size: 226789 Color: 13

Bin 2339: 52 of cap free
Amount of items: 2
Items: 
Size: 650475 Color: 1
Size: 349474 Color: 16

Bin 2340: 52 of cap free
Amount of items: 2
Items: 
Size: 674464 Color: 17
Size: 325485 Color: 4

Bin 2341: 52 of cap free
Amount of items: 3
Items: 
Size: 570547 Color: 4
Size: 235521 Color: 17
Size: 193881 Color: 18

Bin 2342: 52 of cap free
Amount of items: 2
Items: 
Size: 757199 Color: 1
Size: 242750 Color: 0

Bin 2343: 52 of cap free
Amount of items: 2
Items: 
Size: 722367 Color: 6
Size: 277582 Color: 2

Bin 2344: 52 of cap free
Amount of items: 2
Items: 
Size: 515409 Color: 12
Size: 484540 Color: 6

Bin 2345: 52 of cap free
Amount of items: 2
Items: 
Size: 525729 Color: 16
Size: 474220 Color: 14

Bin 2346: 52 of cap free
Amount of items: 2
Items: 
Size: 528740 Color: 0
Size: 471209 Color: 9

Bin 2347: 52 of cap free
Amount of items: 2
Items: 
Size: 530379 Color: 5
Size: 469570 Color: 4

Bin 2348: 52 of cap free
Amount of items: 2
Items: 
Size: 545623 Color: 19
Size: 454326 Color: 2

Bin 2349: 52 of cap free
Amount of items: 2
Items: 
Size: 552189 Color: 12
Size: 447760 Color: 14

Bin 2350: 52 of cap free
Amount of items: 2
Items: 
Size: 571780 Color: 13
Size: 428169 Color: 11

Bin 2351: 52 of cap free
Amount of items: 2
Items: 
Size: 645534 Color: 3
Size: 354415 Color: 14

Bin 2352: 52 of cap free
Amount of items: 2
Items: 
Size: 687868 Color: 14
Size: 312081 Color: 2

Bin 2353: 52 of cap free
Amount of items: 2
Items: 
Size: 721788 Color: 19
Size: 278161 Color: 17

Bin 2354: 52 of cap free
Amount of items: 2
Items: 
Size: 740446 Color: 7
Size: 259503 Color: 5

Bin 2355: 52 of cap free
Amount of items: 2
Items: 
Size: 755901 Color: 13
Size: 244048 Color: 3

Bin 2356: 53 of cap free
Amount of items: 3
Items: 
Size: 590910 Color: 1
Size: 209631 Color: 4
Size: 199407 Color: 8

Bin 2357: 53 of cap free
Amount of items: 2
Items: 
Size: 508849 Color: 12
Size: 491099 Color: 5

Bin 2358: 53 of cap free
Amount of items: 2
Items: 
Size: 733690 Color: 2
Size: 266258 Color: 5

Bin 2359: 53 of cap free
Amount of items: 2
Items: 
Size: 784425 Color: 9
Size: 215523 Color: 16

Bin 2360: 53 of cap free
Amount of items: 2
Items: 
Size: 758762 Color: 11
Size: 241186 Color: 15

Bin 2361: 53 of cap free
Amount of items: 2
Items: 
Size: 513467 Color: 18
Size: 486481 Color: 16

Bin 2362: 53 of cap free
Amount of items: 2
Items: 
Size: 524471 Color: 0
Size: 475477 Color: 11

Bin 2363: 53 of cap free
Amount of items: 2
Items: 
Size: 542022 Color: 6
Size: 457926 Color: 12

Bin 2364: 53 of cap free
Amount of items: 2
Items: 
Size: 591169 Color: 1
Size: 408779 Color: 18

Bin 2365: 53 of cap free
Amount of items: 2
Items: 
Size: 596961 Color: 7
Size: 402987 Color: 10

Bin 2366: 53 of cap free
Amount of items: 2
Items: 
Size: 602752 Color: 11
Size: 397196 Color: 12

Bin 2367: 53 of cap free
Amount of items: 2
Items: 
Size: 623300 Color: 8
Size: 376648 Color: 12

Bin 2368: 53 of cap free
Amount of items: 2
Items: 
Size: 634488 Color: 12
Size: 365460 Color: 6

Bin 2369: 53 of cap free
Amount of items: 2
Items: 
Size: 638574 Color: 1
Size: 361374 Color: 11

Bin 2370: 53 of cap free
Amount of items: 2
Items: 
Size: 697536 Color: 14
Size: 302412 Color: 6

Bin 2371: 53 of cap free
Amount of items: 2
Items: 
Size: 704586 Color: 17
Size: 295362 Color: 3

Bin 2372: 53 of cap free
Amount of items: 2
Items: 
Size: 723168 Color: 1
Size: 276780 Color: 4

Bin 2373: 53 of cap free
Amount of items: 2
Items: 
Size: 792389 Color: 0
Size: 207559 Color: 5

Bin 2374: 54 of cap free
Amount of items: 2
Items: 
Size: 756981 Color: 19
Size: 242966 Color: 2

Bin 2375: 54 of cap free
Amount of items: 3
Items: 
Size: 354756 Color: 5
Size: 337537 Color: 14
Size: 307654 Color: 17

Bin 2376: 54 of cap free
Amount of items: 2
Items: 
Size: 528480 Color: 1
Size: 471467 Color: 19

Bin 2377: 54 of cap free
Amount of items: 2
Items: 
Size: 538810 Color: 10
Size: 461137 Color: 7

Bin 2378: 54 of cap free
Amount of items: 2
Items: 
Size: 557214 Color: 13
Size: 442733 Color: 18

Bin 2379: 54 of cap free
Amount of items: 2
Items: 
Size: 574047 Color: 11
Size: 425900 Color: 3

Bin 2380: 54 of cap free
Amount of items: 2
Items: 
Size: 582365 Color: 9
Size: 417582 Color: 17

Bin 2381: 54 of cap free
Amount of items: 2
Items: 
Size: 585980 Color: 15
Size: 413967 Color: 11

Bin 2382: 54 of cap free
Amount of items: 2
Items: 
Size: 610256 Color: 9
Size: 389691 Color: 8

Bin 2383: 54 of cap free
Amount of items: 2
Items: 
Size: 616963 Color: 15
Size: 382984 Color: 4

Bin 2384: 54 of cap free
Amount of items: 2
Items: 
Size: 647414 Color: 13
Size: 352533 Color: 12

Bin 2385: 54 of cap free
Amount of items: 2
Items: 
Size: 649813 Color: 7
Size: 350134 Color: 3

Bin 2386: 54 of cap free
Amount of items: 2
Items: 
Size: 677642 Color: 17
Size: 322305 Color: 3

Bin 2387: 54 of cap free
Amount of items: 2
Items: 
Size: 714848 Color: 4
Size: 285099 Color: 15

Bin 2388: 54 of cap free
Amount of items: 2
Items: 
Size: 721611 Color: 8
Size: 278336 Color: 2

Bin 2389: 54 of cap free
Amount of items: 2
Items: 
Size: 740637 Color: 9
Size: 259310 Color: 4

Bin 2390: 54 of cap free
Amount of items: 2
Items: 
Size: 740960 Color: 15
Size: 258987 Color: 12

Bin 2391: 55 of cap free
Amount of items: 2
Items: 
Size: 768159 Color: 13
Size: 231787 Color: 2

Bin 2392: 55 of cap free
Amount of items: 3
Items: 
Size: 747906 Color: 1
Size: 126271 Color: 11
Size: 125769 Color: 7

Bin 2393: 55 of cap free
Amount of items: 2
Items: 
Size: 704352 Color: 11
Size: 295594 Color: 0

Bin 2394: 55 of cap free
Amount of items: 2
Items: 
Size: 796996 Color: 1
Size: 202950 Color: 3

Bin 2395: 55 of cap free
Amount of items: 2
Items: 
Size: 650288 Color: 12
Size: 349658 Color: 16

Bin 2396: 55 of cap free
Amount of items: 3
Items: 
Size: 570325 Color: 9
Size: 260416 Color: 19
Size: 169205 Color: 11

Bin 2397: 55 of cap free
Amount of items: 2
Items: 
Size: 506640 Color: 0
Size: 493306 Color: 2

Bin 2398: 55 of cap free
Amount of items: 2
Items: 
Size: 552066 Color: 13
Size: 447880 Color: 6

Bin 2399: 55 of cap free
Amount of items: 2
Items: 
Size: 601819 Color: 14
Size: 398127 Color: 17

Bin 2400: 55 of cap free
Amount of items: 2
Items: 
Size: 608900 Color: 3
Size: 391046 Color: 10

Bin 2401: 55 of cap free
Amount of items: 2
Items: 
Size: 644011 Color: 13
Size: 355935 Color: 16

Bin 2402: 55 of cap free
Amount of items: 2
Items: 
Size: 660229 Color: 2
Size: 339717 Color: 18

Bin 2403: 55 of cap free
Amount of items: 2
Items: 
Size: 786586 Color: 13
Size: 213360 Color: 14

Bin 2404: 56 of cap free
Amount of items: 2
Items: 
Size: 644462 Color: 8
Size: 355483 Color: 13

Bin 2405: 56 of cap free
Amount of items: 3
Items: 
Size: 644997 Color: 10
Size: 177568 Color: 10
Size: 177380 Color: 9

Bin 2406: 56 of cap free
Amount of items: 2
Items: 
Size: 505857 Color: 3
Size: 494088 Color: 12

Bin 2407: 56 of cap free
Amount of items: 2
Items: 
Size: 568581 Color: 5
Size: 431364 Color: 15

Bin 2408: 56 of cap free
Amount of items: 2
Items: 
Size: 576275 Color: 3
Size: 423670 Color: 2

Bin 2409: 56 of cap free
Amount of items: 2
Items: 
Size: 596661 Color: 16
Size: 403284 Color: 8

Bin 2410: 56 of cap free
Amount of items: 2
Items: 
Size: 600868 Color: 5
Size: 399077 Color: 15

Bin 2411: 56 of cap free
Amount of items: 2
Items: 
Size: 787305 Color: 1
Size: 212640 Color: 4

Bin 2412: 57 of cap free
Amount of items: 2
Items: 
Size: 584737 Color: 16
Size: 415207 Color: 6

Bin 2413: 57 of cap free
Amount of items: 3
Items: 
Size: 696156 Color: 17
Size: 173709 Color: 13
Size: 130079 Color: 3

Bin 2414: 57 of cap free
Amount of items: 2
Items: 
Size: 649147 Color: 5
Size: 350797 Color: 0

Bin 2415: 57 of cap free
Amount of items: 2
Items: 
Size: 697242 Color: 13
Size: 302702 Color: 1

Bin 2416: 57 of cap free
Amount of items: 2
Items: 
Size: 764588 Color: 11
Size: 235356 Color: 8

Bin 2417: 57 of cap free
Amount of items: 2
Items: 
Size: 514500 Color: 4
Size: 485444 Color: 5

Bin 2418: 57 of cap free
Amount of items: 2
Items: 
Size: 521247 Color: 9
Size: 478697 Color: 19

Bin 2419: 57 of cap free
Amount of items: 2
Items: 
Size: 525636 Color: 5
Size: 474308 Color: 1

Bin 2420: 57 of cap free
Amount of items: 2
Items: 
Size: 548396 Color: 11
Size: 451548 Color: 16

Bin 2421: 57 of cap free
Amount of items: 2
Items: 
Size: 576369 Color: 4
Size: 423575 Color: 8

Bin 2422: 57 of cap free
Amount of items: 2
Items: 
Size: 597164 Color: 18
Size: 402780 Color: 9

Bin 2423: 57 of cap free
Amount of items: 2
Items: 
Size: 651626 Color: 13
Size: 348318 Color: 4

Bin 2424: 57 of cap free
Amount of items: 2
Items: 
Size: 730647 Color: 15
Size: 269297 Color: 9

Bin 2425: 57 of cap free
Amount of items: 2
Items: 
Size: 764706 Color: 0
Size: 235238 Color: 7

Bin 2426: 57 of cap free
Amount of items: 2
Items: 
Size: 794684 Color: 7
Size: 205260 Color: 15

Bin 2427: 58 of cap free
Amount of items: 2
Items: 
Size: 585593 Color: 17
Size: 414350 Color: 2

Bin 2428: 58 of cap free
Amount of items: 3
Items: 
Size: 689228 Color: 8
Size: 159163 Color: 0
Size: 151552 Color: 14

Bin 2429: 58 of cap free
Amount of items: 2
Items: 
Size: 743791 Color: 0
Size: 256152 Color: 3

Bin 2430: 58 of cap free
Amount of items: 2
Items: 
Size: 750463 Color: 15
Size: 249480 Color: 5

Bin 2431: 58 of cap free
Amount of items: 2
Items: 
Size: 629877 Color: 9
Size: 370066 Color: 8

Bin 2432: 58 of cap free
Amount of items: 2
Items: 
Size: 539665 Color: 5
Size: 460278 Color: 8

Bin 2433: 58 of cap free
Amount of items: 2
Items: 
Size: 573215 Color: 14
Size: 426728 Color: 16

Bin 2434: 58 of cap free
Amount of items: 2
Items: 
Size: 575381 Color: 1
Size: 424562 Color: 18

Bin 2435: 58 of cap free
Amount of items: 2
Items: 
Size: 586493 Color: 15
Size: 413450 Color: 13

Bin 2436: 58 of cap free
Amount of items: 2
Items: 
Size: 684621 Color: 19
Size: 315322 Color: 12

Bin 2437: 58 of cap free
Amount of items: 2
Items: 
Size: 705540 Color: 10
Size: 294403 Color: 15

Bin 2438: 58 of cap free
Amount of items: 2
Items: 
Size: 720796 Color: 14
Size: 279147 Color: 10

Bin 2439: 58 of cap free
Amount of items: 2
Items: 
Size: 790598 Color: 1
Size: 209345 Color: 2

Bin 2440: 59 of cap free
Amount of items: 2
Items: 
Size: 792158 Color: 2
Size: 207784 Color: 8

Bin 2441: 59 of cap free
Amount of items: 2
Items: 
Size: 784423 Color: 13
Size: 215519 Color: 2

Bin 2442: 59 of cap free
Amount of items: 2
Items: 
Size: 728478 Color: 14
Size: 271464 Color: 3

Bin 2443: 59 of cap free
Amount of items: 2
Items: 
Size: 797431 Color: 19
Size: 202511 Color: 2

Bin 2444: 59 of cap free
Amount of items: 2
Items: 
Size: 566639 Color: 14
Size: 433303 Color: 3

Bin 2445: 59 of cap free
Amount of items: 2
Items: 
Size: 622812 Color: 18
Size: 377130 Color: 1

Bin 2446: 59 of cap free
Amount of items: 2
Items: 
Size: 522908 Color: 19
Size: 477034 Color: 8

Bin 2447: 59 of cap free
Amount of items: 2
Items: 
Size: 527511 Color: 16
Size: 472431 Color: 7

Bin 2448: 59 of cap free
Amount of items: 2
Items: 
Size: 539834 Color: 3
Size: 460108 Color: 2

Bin 2449: 59 of cap free
Amount of items: 2
Items: 
Size: 543708 Color: 1
Size: 456234 Color: 9

Bin 2450: 59 of cap free
Amount of items: 2
Items: 
Size: 674283 Color: 19
Size: 325659 Color: 12

Bin 2451: 59 of cap free
Amount of items: 2
Items: 
Size: 678521 Color: 5
Size: 321421 Color: 8

Bin 2452: 59 of cap free
Amount of items: 2
Items: 
Size: 715722 Color: 11
Size: 284220 Color: 12

Bin 2453: 59 of cap free
Amount of items: 2
Items: 
Size: 735789 Color: 19
Size: 264153 Color: 15

Bin 2454: 59 of cap free
Amount of items: 2
Items: 
Size: 775170 Color: 6
Size: 224772 Color: 7

Bin 2455: 60 of cap free
Amount of items: 3
Items: 
Size: 701552 Color: 7
Size: 164243 Color: 18
Size: 134146 Color: 12

Bin 2456: 60 of cap free
Amount of items: 2
Items: 
Size: 780945 Color: 3
Size: 218996 Color: 4

Bin 2457: 60 of cap free
Amount of items: 2
Items: 
Size: 796708 Color: 9
Size: 203233 Color: 1

Bin 2458: 60 of cap free
Amount of items: 2
Items: 
Size: 522529 Color: 6
Size: 477412 Color: 19

Bin 2459: 60 of cap free
Amount of items: 2
Items: 
Size: 525279 Color: 7
Size: 474662 Color: 0

Bin 2460: 60 of cap free
Amount of items: 2
Items: 
Size: 561144 Color: 10
Size: 438797 Color: 4

Bin 2461: 60 of cap free
Amount of items: 2
Items: 
Size: 564710 Color: 16
Size: 435231 Color: 3

Bin 2462: 60 of cap free
Amount of items: 2
Items: 
Size: 617863 Color: 4
Size: 382078 Color: 10

Bin 2463: 60 of cap free
Amount of items: 2
Items: 
Size: 621914 Color: 4
Size: 378027 Color: 5

Bin 2464: 60 of cap free
Amount of items: 2
Items: 
Size: 634550 Color: 13
Size: 365391 Color: 6

Bin 2465: 60 of cap free
Amount of items: 2
Items: 
Size: 672705 Color: 10
Size: 327236 Color: 19

Bin 2466: 60 of cap free
Amount of items: 2
Items: 
Size: 675216 Color: 0
Size: 324725 Color: 3

Bin 2467: 60 of cap free
Amount of items: 2
Items: 
Size: 679093 Color: 5
Size: 320848 Color: 16

Bin 2468: 60 of cap free
Amount of items: 2
Items: 
Size: 755146 Color: 13
Size: 244795 Color: 19

Bin 2469: 61 of cap free
Amount of items: 2
Items: 
Size: 512203 Color: 17
Size: 487737 Color: 4

Bin 2470: 61 of cap free
Amount of items: 2
Items: 
Size: 765673 Color: 0
Size: 234267 Color: 8

Bin 2471: 61 of cap free
Amount of items: 2
Items: 
Size: 793318 Color: 14
Size: 206622 Color: 17

Bin 2472: 61 of cap free
Amount of items: 2
Items: 
Size: 525004 Color: 17
Size: 474936 Color: 15

Bin 2473: 61 of cap free
Amount of items: 2
Items: 
Size: 537586 Color: 6
Size: 462354 Color: 13

Bin 2474: 61 of cap free
Amount of items: 2
Items: 
Size: 614108 Color: 5
Size: 385832 Color: 10

Bin 2475: 61 of cap free
Amount of items: 2
Items: 
Size: 631610 Color: 8
Size: 368330 Color: 6

Bin 2476: 61 of cap free
Amount of items: 2
Items: 
Size: 713728 Color: 11
Size: 286212 Color: 15

Bin 2477: 61 of cap free
Amount of items: 2
Items: 
Size: 770160 Color: 7
Size: 229780 Color: 0

Bin 2478: 62 of cap free
Amount of items: 2
Items: 
Size: 637265 Color: 15
Size: 362674 Color: 13

Bin 2479: 62 of cap free
Amount of items: 3
Items: 
Size: 468804 Color: 14
Size: 273916 Color: 7
Size: 257219 Color: 19

Bin 2480: 62 of cap free
Amount of items: 2
Items: 
Size: 718184 Color: 17
Size: 281755 Color: 11

Bin 2481: 62 of cap free
Amount of items: 2
Items: 
Size: 524197 Color: 8
Size: 475742 Color: 10

Bin 2482: 62 of cap free
Amount of items: 2
Items: 
Size: 572463 Color: 15
Size: 427476 Color: 5

Bin 2483: 62 of cap free
Amount of items: 2
Items: 
Size: 577721 Color: 7
Size: 422218 Color: 8

Bin 2484: 62 of cap free
Amount of items: 2
Items: 
Size: 577724 Color: 8
Size: 422215 Color: 3

Bin 2485: 62 of cap free
Amount of items: 2
Items: 
Size: 588296 Color: 3
Size: 411643 Color: 8

Bin 2486: 62 of cap free
Amount of items: 2
Items: 
Size: 597029 Color: 14
Size: 402910 Color: 4

Bin 2487: 62 of cap free
Amount of items: 2
Items: 
Size: 601208 Color: 18
Size: 398731 Color: 19

Bin 2488: 62 of cap free
Amount of items: 2
Items: 
Size: 603028 Color: 0
Size: 396911 Color: 16

Bin 2489: 62 of cap free
Amount of items: 2
Items: 
Size: 624137 Color: 14
Size: 375802 Color: 9

Bin 2490: 62 of cap free
Amount of items: 2
Items: 
Size: 626506 Color: 12
Size: 373433 Color: 18

Bin 2491: 62 of cap free
Amount of items: 2
Items: 
Size: 756440 Color: 12
Size: 243499 Color: 0

Bin 2492: 62 of cap free
Amount of items: 3
Items: 
Size: 756960 Color: 14
Size: 121674 Color: 1
Size: 121305 Color: 9

Bin 2493: 62 of cap free
Amount of items: 2
Items: 
Size: 774703 Color: 2
Size: 225236 Color: 9

Bin 2494: 62 of cap free
Amount of items: 2
Items: 
Size: 774782 Color: 7
Size: 225157 Color: 13

Bin 2495: 63 of cap free
Amount of items: 2
Items: 
Size: 615128 Color: 16
Size: 384810 Color: 0

Bin 2496: 63 of cap free
Amount of items: 2
Items: 
Size: 629491 Color: 18
Size: 370447 Color: 3

Bin 2497: 63 of cap free
Amount of items: 2
Items: 
Size: 685822 Color: 9
Size: 314116 Color: 16

Bin 2498: 63 of cap free
Amount of items: 2
Items: 
Size: 724969 Color: 8
Size: 274969 Color: 19

Bin 2499: 63 of cap free
Amount of items: 2
Items: 
Size: 651540 Color: 4
Size: 348398 Color: 14

Bin 2500: 63 of cap free
Amount of items: 2
Items: 
Size: 620273 Color: 16
Size: 379665 Color: 0

Bin 2501: 63 of cap free
Amount of items: 2
Items: 
Size: 688221 Color: 4
Size: 311717 Color: 16

Bin 2502: 63 of cap free
Amount of items: 2
Items: 
Size: 542506 Color: 15
Size: 457432 Color: 8

Bin 2503: 63 of cap free
Amount of items: 2
Items: 
Size: 544242 Color: 13
Size: 455696 Color: 12

Bin 2504: 63 of cap free
Amount of items: 2
Items: 
Size: 580536 Color: 17
Size: 419402 Color: 7

Bin 2505: 63 of cap free
Amount of items: 2
Items: 
Size: 597629 Color: 10
Size: 402309 Color: 19

Bin 2506: 63 of cap free
Amount of items: 2
Items: 
Size: 630681 Color: 7
Size: 369257 Color: 19

Bin 2507: 63 of cap free
Amount of items: 2
Items: 
Size: 647557 Color: 4
Size: 352381 Color: 11

Bin 2508: 63 of cap free
Amount of items: 2
Items: 
Size: 679633 Color: 13
Size: 320305 Color: 3

Bin 2509: 63 of cap free
Amount of items: 2
Items: 
Size: 709233 Color: 14
Size: 290705 Color: 10

Bin 2510: 63 of cap free
Amount of items: 2
Items: 
Size: 768266 Color: 12
Size: 231672 Color: 7

Bin 2511: 63 of cap free
Amount of items: 2
Items: 
Size: 772013 Color: 13
Size: 227925 Color: 4

Bin 2512: 63 of cap free
Amount of items: 2
Items: 
Size: 794839 Color: 0
Size: 205099 Color: 5

Bin 2513: 64 of cap free
Amount of items: 2
Items: 
Size: 680095 Color: 13
Size: 319842 Color: 4

Bin 2514: 64 of cap free
Amount of items: 3
Items: 
Size: 634725 Color: 17
Size: 183315 Color: 12
Size: 181897 Color: 5

Bin 2515: 64 of cap free
Amount of items: 2
Items: 
Size: 699754 Color: 5
Size: 300183 Color: 1

Bin 2516: 64 of cap free
Amount of items: 2
Items: 
Size: 616675 Color: 15
Size: 383262 Color: 3

Bin 2517: 64 of cap free
Amount of items: 2
Items: 
Size: 614668 Color: 6
Size: 385269 Color: 2

Bin 2518: 64 of cap free
Amount of items: 2
Items: 
Size: 570413 Color: 12
Size: 429524 Color: 14

Bin 2519: 64 of cap free
Amount of items: 3
Items: 
Size: 620928 Color: 12
Size: 190199 Color: 16
Size: 188810 Color: 14

Bin 2520: 64 of cap free
Amount of items: 2
Items: 
Size: 787236 Color: 13
Size: 212701 Color: 9

Bin 2521: 64 of cap free
Amount of items: 2
Items: 
Size: 519622 Color: 19
Size: 480315 Color: 18

Bin 2522: 64 of cap free
Amount of items: 2
Items: 
Size: 534629 Color: 4
Size: 465308 Color: 18

Bin 2523: 64 of cap free
Amount of items: 2
Items: 
Size: 565986 Color: 13
Size: 433951 Color: 2

Bin 2524: 64 of cap free
Amount of items: 2
Items: 
Size: 604447 Color: 16
Size: 395490 Color: 7

Bin 2525: 64 of cap free
Amount of items: 2
Items: 
Size: 688390 Color: 9
Size: 311547 Color: 1

Bin 2526: 65 of cap free
Amount of items: 2
Items: 
Size: 784995 Color: 10
Size: 214941 Color: 18

Bin 2527: 65 of cap free
Amount of items: 2
Items: 
Size: 639774 Color: 11
Size: 360162 Color: 9

Bin 2528: 65 of cap free
Amount of items: 2
Items: 
Size: 750460 Color: 13
Size: 249476 Color: 2

Bin 2529: 65 of cap free
Amount of items: 2
Items: 
Size: 761058 Color: 11
Size: 238878 Color: 9

Bin 2530: 65 of cap free
Amount of items: 2
Items: 
Size: 533821 Color: 17
Size: 466115 Color: 18

Bin 2531: 65 of cap free
Amount of items: 2
Items: 
Size: 558226 Color: 15
Size: 441710 Color: 16

Bin 2532: 65 of cap free
Amount of items: 2
Items: 
Size: 579763 Color: 10
Size: 420173 Color: 16

Bin 2533: 65 of cap free
Amount of items: 2
Items: 
Size: 590838 Color: 19
Size: 409098 Color: 5

Bin 2534: 65 of cap free
Amount of items: 2
Items: 
Size: 600743 Color: 9
Size: 399193 Color: 17

Bin 2535: 65 of cap free
Amount of items: 2
Items: 
Size: 608138 Color: 5
Size: 391798 Color: 14

Bin 2536: 65 of cap free
Amount of items: 2
Items: 
Size: 767608 Color: 13
Size: 232328 Color: 8

Bin 2537: 65 of cap free
Amount of items: 2
Items: 
Size: 768699 Color: 8
Size: 231237 Color: 13

Bin 2538: 65 of cap free
Amount of items: 2
Items: 
Size: 774630 Color: 19
Size: 225306 Color: 3

Bin 2539: 65 of cap free
Amount of items: 2
Items: 
Size: 784675 Color: 7
Size: 215261 Color: 16

Bin 2540: 66 of cap free
Amount of items: 2
Items: 
Size: 753878 Color: 11
Size: 246057 Color: 3

Bin 2541: 66 of cap free
Amount of items: 2
Items: 
Size: 689619 Color: 8
Size: 310316 Color: 4

Bin 2542: 66 of cap free
Amount of items: 3
Items: 
Size: 527167 Color: 15
Size: 244150 Color: 5
Size: 228618 Color: 18

Bin 2543: 66 of cap free
Amount of items: 2
Items: 
Size: 537889 Color: 17
Size: 462046 Color: 8

Bin 2544: 66 of cap free
Amount of items: 2
Items: 
Size: 538408 Color: 2
Size: 461527 Color: 6

Bin 2545: 66 of cap free
Amount of items: 2
Items: 
Size: 570871 Color: 12
Size: 429064 Color: 11

Bin 2546: 66 of cap free
Amount of items: 2
Items: 
Size: 598654 Color: 8
Size: 401281 Color: 16

Bin 2547: 66 of cap free
Amount of items: 2
Items: 
Size: 611553 Color: 4
Size: 388382 Color: 18

Bin 2548: 66 of cap free
Amount of items: 2
Items: 
Size: 626611 Color: 13
Size: 373324 Color: 14

Bin 2549: 66 of cap free
Amount of items: 2
Items: 
Size: 651055 Color: 1
Size: 348880 Color: 15

Bin 2550: 66 of cap free
Amount of items: 2
Items: 
Size: 758349 Color: 14
Size: 241586 Color: 18

Bin 2551: 66 of cap free
Amount of items: 2
Items: 
Size: 796510 Color: 3
Size: 203425 Color: 14

Bin 2552: 67 of cap free
Amount of items: 3
Items: 
Size: 759655 Color: 18
Size: 126609 Color: 1
Size: 113670 Color: 3

Bin 2553: 67 of cap free
Amount of items: 2
Items: 
Size: 568111 Color: 0
Size: 431823 Color: 4

Bin 2554: 67 of cap free
Amount of items: 2
Items: 
Size: 554587 Color: 15
Size: 445347 Color: 9

Bin 2555: 67 of cap free
Amount of items: 3
Items: 
Size: 678595 Color: 17
Size: 183185 Color: 8
Size: 138154 Color: 15

Bin 2556: 67 of cap free
Amount of items: 2
Items: 
Size: 688586 Color: 9
Size: 311348 Color: 16

Bin 2557: 67 of cap free
Amount of items: 2
Items: 
Size: 728476 Color: 11
Size: 271458 Color: 18

Bin 2558: 67 of cap free
Amount of items: 2
Items: 
Size: 503811 Color: 6
Size: 496123 Color: 8

Bin 2559: 67 of cap free
Amount of items: 2
Items: 
Size: 552912 Color: 6
Size: 447022 Color: 7

Bin 2560: 67 of cap free
Amount of items: 2
Items: 
Size: 578455 Color: 15
Size: 421479 Color: 11

Bin 2561: 67 of cap free
Amount of items: 2
Items: 
Size: 630438 Color: 12
Size: 369496 Color: 7

Bin 2562: 67 of cap free
Amount of items: 2
Items: 
Size: 657350 Color: 4
Size: 342584 Color: 13

Bin 2563: 67 of cap free
Amount of items: 2
Items: 
Size: 669226 Color: 14
Size: 330708 Color: 7

Bin 2564: 67 of cap free
Amount of items: 2
Items: 
Size: 687644 Color: 3
Size: 312290 Color: 18

Bin 2565: 67 of cap free
Amount of items: 2
Items: 
Size: 746371 Color: 16
Size: 253563 Color: 17

Bin 2566: 68 of cap free
Amount of items: 2
Items: 
Size: 779926 Color: 6
Size: 220007 Color: 17

Bin 2567: 68 of cap free
Amount of items: 2
Items: 
Size: 604180 Color: 16
Size: 395753 Color: 4

Bin 2568: 68 of cap free
Amount of items: 2
Items: 
Size: 732444 Color: 1
Size: 267489 Color: 19

Bin 2569: 68 of cap free
Amount of items: 2
Items: 
Size: 743331 Color: 16
Size: 256602 Color: 1

Bin 2570: 68 of cap free
Amount of items: 2
Items: 
Size: 747546 Color: 1
Size: 252387 Color: 4

Bin 2571: 68 of cap free
Amount of items: 2
Items: 
Size: 541703 Color: 2
Size: 458230 Color: 15

Bin 2572: 68 of cap free
Amount of items: 2
Items: 
Size: 561346 Color: 9
Size: 438587 Color: 4

Bin 2573: 68 of cap free
Amount of items: 2
Items: 
Size: 658513 Color: 11
Size: 341420 Color: 7

Bin 2574: 68 of cap free
Amount of items: 2
Items: 
Size: 795219 Color: 7
Size: 204714 Color: 12

Bin 2575: 69 of cap free
Amount of items: 2
Items: 
Size: 703421 Color: 1
Size: 296511 Color: 5

Bin 2576: 69 of cap free
Amount of items: 2
Items: 
Size: 601310 Color: 17
Size: 398622 Color: 8

Bin 2577: 69 of cap free
Amount of items: 2
Items: 
Size: 532852 Color: 13
Size: 467080 Color: 10

Bin 2578: 69 of cap free
Amount of items: 2
Items: 
Size: 550915 Color: 18
Size: 449017 Color: 3

Bin 2579: 69 of cap free
Amount of items: 2
Items: 
Size: 569548 Color: 11
Size: 430384 Color: 17

Bin 2580: 69 of cap free
Amount of items: 2
Items: 
Size: 575053 Color: 15
Size: 424879 Color: 12

Bin 2581: 69 of cap free
Amount of items: 2
Items: 
Size: 579647 Color: 4
Size: 420285 Color: 2

Bin 2582: 69 of cap free
Amount of items: 2
Items: 
Size: 583838 Color: 7
Size: 416094 Color: 3

Bin 2583: 69 of cap free
Amount of items: 2
Items: 
Size: 597527 Color: 18
Size: 402405 Color: 3

Bin 2584: 69 of cap free
Amount of items: 2
Items: 
Size: 602446 Color: 14
Size: 397486 Color: 17

Bin 2585: 69 of cap free
Amount of items: 2
Items: 
Size: 618292 Color: 11
Size: 381640 Color: 18

Bin 2586: 69 of cap free
Amount of items: 2
Items: 
Size: 677757 Color: 8
Size: 322175 Color: 7

Bin 2587: 69 of cap free
Amount of items: 2
Items: 
Size: 682069 Color: 17
Size: 317863 Color: 8

Bin 2588: 70 of cap free
Amount of items: 2
Items: 
Size: 545694 Color: 11
Size: 454237 Color: 1

Bin 2589: 70 of cap free
Amount of items: 2
Items: 
Size: 763613 Color: 13
Size: 236318 Color: 12

Bin 2590: 70 of cap free
Amount of items: 2
Items: 
Size: 568362 Color: 8
Size: 431569 Color: 5

Bin 2591: 70 of cap free
Amount of items: 2
Items: 
Size: 521609 Color: 11
Size: 478322 Color: 17

Bin 2592: 70 of cap free
Amount of items: 2
Items: 
Size: 550137 Color: 12
Size: 449794 Color: 2

Bin 2593: 70 of cap free
Amount of items: 2
Items: 
Size: 550677 Color: 1
Size: 449254 Color: 2

Bin 2594: 70 of cap free
Amount of items: 2
Items: 
Size: 565226 Color: 5
Size: 434705 Color: 18

Bin 2595: 70 of cap free
Amount of items: 2
Items: 
Size: 578613 Color: 3
Size: 421318 Color: 1

Bin 2596: 70 of cap free
Amount of items: 2
Items: 
Size: 593508 Color: 2
Size: 406423 Color: 10

Bin 2597: 70 of cap free
Amount of items: 2
Items: 
Size: 627829 Color: 3
Size: 372102 Color: 7

Bin 2598: 70 of cap free
Amount of items: 2
Items: 
Size: 793402 Color: 12
Size: 206529 Color: 2

Bin 2599: 71 of cap free
Amount of items: 2
Items: 
Size: 619045 Color: 0
Size: 380885 Color: 15

Bin 2600: 71 of cap free
Amount of items: 3
Items: 
Size: 646384 Color: 19
Size: 176810 Color: 1
Size: 176736 Color: 16

Bin 2601: 71 of cap free
Amount of items: 2
Items: 
Size: 790677 Color: 1
Size: 209253 Color: 13

Bin 2602: 71 of cap free
Amount of items: 2
Items: 
Size: 762332 Color: 12
Size: 237598 Color: 1

Bin 2603: 71 of cap free
Amount of items: 2
Items: 
Size: 525526 Color: 19
Size: 474404 Color: 1

Bin 2604: 71 of cap free
Amount of items: 2
Items: 
Size: 537200 Color: 5
Size: 462730 Color: 11

Bin 2605: 71 of cap free
Amount of items: 2
Items: 
Size: 557558 Color: 8
Size: 442372 Color: 0

Bin 2606: 71 of cap free
Amount of items: 2
Items: 
Size: 587967 Color: 9
Size: 411963 Color: 6

Bin 2607: 71 of cap free
Amount of items: 2
Items: 
Size: 599881 Color: 0
Size: 400049 Color: 9

Bin 2608: 71 of cap free
Amount of items: 2
Items: 
Size: 606211 Color: 7
Size: 393719 Color: 2

Bin 2609: 71 of cap free
Amount of items: 2
Items: 
Size: 659416 Color: 19
Size: 340514 Color: 8

Bin 2610: 71 of cap free
Amount of items: 2
Items: 
Size: 666006 Color: 17
Size: 333924 Color: 12

Bin 2611: 72 of cap free
Amount of items: 3
Items: 
Size: 748436 Color: 19
Size: 138738 Color: 13
Size: 112755 Color: 6

Bin 2612: 72 of cap free
Amount of items: 3
Items: 
Size: 670691 Color: 17
Size: 168866 Color: 3
Size: 160372 Color: 16

Bin 2613: 72 of cap free
Amount of items: 2
Items: 
Size: 626340 Color: 11
Size: 373589 Color: 1

Bin 2614: 72 of cap free
Amount of items: 2
Items: 
Size: 754228 Color: 17
Size: 245701 Color: 6

Bin 2615: 72 of cap free
Amount of items: 2
Items: 
Size: 531705 Color: 0
Size: 468224 Color: 16

Bin 2616: 72 of cap free
Amount of items: 2
Items: 
Size: 550021 Color: 9
Size: 449908 Color: 3

Bin 2617: 72 of cap free
Amount of items: 2
Items: 
Size: 569142 Color: 18
Size: 430787 Color: 10

Bin 2618: 72 of cap free
Amount of items: 2
Items: 
Size: 577624 Color: 11
Size: 422305 Color: 8

Bin 2619: 72 of cap free
Amount of items: 2
Items: 
Size: 582763 Color: 14
Size: 417166 Color: 5

Bin 2620: 72 of cap free
Amount of items: 2
Items: 
Size: 588080 Color: 10
Size: 411849 Color: 19

Bin 2621: 72 of cap free
Amount of items: 2
Items: 
Size: 591331 Color: 11
Size: 408598 Color: 10

Bin 2622: 72 of cap free
Amount of items: 2
Items: 
Size: 710267 Color: 12
Size: 289662 Color: 8

Bin 2623: 72 of cap free
Amount of items: 2
Items: 
Size: 729040 Color: 6
Size: 270889 Color: 4

Bin 2624: 73 of cap free
Amount of items: 3
Items: 
Size: 726917 Color: 3
Size: 140401 Color: 12
Size: 132610 Color: 0

Bin 2625: 73 of cap free
Amount of items: 2
Items: 
Size: 632750 Color: 19
Size: 367178 Color: 6

Bin 2626: 73 of cap free
Amount of items: 2
Items: 
Size: 719513 Color: 11
Size: 280415 Color: 15

Bin 2627: 73 of cap free
Amount of items: 2
Items: 
Size: 707822 Color: 15
Size: 292106 Color: 6

Bin 2628: 73 of cap free
Amount of items: 2
Items: 
Size: 516790 Color: 8
Size: 483138 Color: 9

Bin 2629: 73 of cap free
Amount of items: 2
Items: 
Size: 513766 Color: 0
Size: 486162 Color: 15

Bin 2630: 73 of cap free
Amount of items: 2
Items: 
Size: 551339 Color: 0
Size: 448589 Color: 10

Bin 2631: 73 of cap free
Amount of items: 2
Items: 
Size: 578148 Color: 12
Size: 421780 Color: 6

Bin 2632: 73 of cap free
Amount of items: 2
Items: 
Size: 592884 Color: 7
Size: 407044 Color: 17

Bin 2633: 73 of cap free
Amount of items: 2
Items: 
Size: 598847 Color: 14
Size: 401081 Color: 8

Bin 2634: 73 of cap free
Amount of items: 2
Items: 
Size: 610420 Color: 13
Size: 389508 Color: 9

Bin 2635: 73 of cap free
Amount of items: 2
Items: 
Size: 613987 Color: 8
Size: 385941 Color: 10

Bin 2636: 73 of cap free
Amount of items: 2
Items: 
Size: 647151 Color: 8
Size: 352777 Color: 16

Bin 2637: 73 of cap free
Amount of items: 2
Items: 
Size: 780747 Color: 18
Size: 219181 Color: 19

Bin 2638: 73 of cap free
Amount of items: 2
Items: 
Size: 785113 Color: 7
Size: 214815 Color: 13

Bin 2639: 74 of cap free
Amount of items: 2
Items: 
Size: 668725 Color: 17
Size: 331202 Color: 10

Bin 2640: 74 of cap free
Amount of items: 2
Items: 
Size: 713465 Color: 2
Size: 286462 Color: 14

Bin 2641: 74 of cap free
Amount of items: 2
Items: 
Size: 679089 Color: 7
Size: 320838 Color: 11

Bin 2642: 74 of cap free
Amount of items: 2
Items: 
Size: 714989 Color: 15
Size: 284938 Color: 0

Bin 2643: 74 of cap free
Amount of items: 2
Items: 
Size: 777018 Color: 9
Size: 222909 Color: 14

Bin 2644: 74 of cap free
Amount of items: 2
Items: 
Size: 752697 Color: 12
Size: 247230 Color: 2

Bin 2645: 74 of cap free
Amount of items: 2
Items: 
Size: 551199 Color: 6
Size: 448728 Color: 4

Bin 2646: 74 of cap free
Amount of items: 2
Items: 
Size: 597404 Color: 6
Size: 402523 Color: 0

Bin 2647: 74 of cap free
Amount of items: 2
Items: 
Size: 638213 Color: 4
Size: 361714 Color: 1

Bin 2648: 74 of cap free
Amount of items: 2
Items: 
Size: 766173 Color: 4
Size: 233754 Color: 12

Bin 2649: 75 of cap free
Amount of items: 2
Items: 
Size: 684511 Color: 12
Size: 315415 Color: 4

Bin 2650: 75 of cap free
Amount of items: 2
Items: 
Size: 660802 Color: 8
Size: 339124 Color: 19

Bin 2651: 75 of cap free
Amount of items: 2
Items: 
Size: 579034 Color: 10
Size: 420892 Color: 0

Bin 2652: 75 of cap free
Amount of items: 2
Items: 
Size: 787074 Color: 14
Size: 212852 Color: 5

Bin 2653: 75 of cap free
Amount of items: 2
Items: 
Size: 689491 Color: 4
Size: 310435 Color: 9

Bin 2654: 75 of cap free
Amount of items: 3
Items: 
Size: 371919 Color: 7
Size: 337345 Color: 0
Size: 290662 Color: 16

Bin 2655: 75 of cap free
Amount of items: 2
Items: 
Size: 576923 Color: 13
Size: 423003 Color: 6

Bin 2656: 75 of cap free
Amount of items: 2
Items: 
Size: 599441 Color: 17
Size: 400485 Color: 5

Bin 2657: 75 of cap free
Amount of items: 2
Items: 
Size: 737133 Color: 10
Size: 262793 Color: 9

Bin 2658: 76 of cap free
Amount of items: 2
Items: 
Size: 771079 Color: 6
Size: 228846 Color: 5

Bin 2659: 76 of cap free
Amount of items: 2
Items: 
Size: 759258 Color: 1
Size: 240667 Color: 12

Bin 2660: 76 of cap free
Amount of items: 2
Items: 
Size: 798558 Color: 2
Size: 201367 Color: 9

Bin 2661: 76 of cap free
Amount of items: 2
Items: 
Size: 766868 Color: 8
Size: 233057 Color: 12

Bin 2662: 76 of cap free
Amount of items: 3
Items: 
Size: 365660 Color: 11
Size: 338777 Color: 12
Size: 295488 Color: 2

Bin 2663: 76 of cap free
Amount of items: 2
Items: 
Size: 555483 Color: 11
Size: 444442 Color: 4

Bin 2664: 76 of cap free
Amount of items: 2
Items: 
Size: 570839 Color: 6
Size: 429086 Color: 12

Bin 2665: 76 of cap free
Amount of items: 2
Items: 
Size: 593781 Color: 8
Size: 406144 Color: 3

Bin 2666: 76 of cap free
Amount of items: 2
Items: 
Size: 637688 Color: 5
Size: 362237 Color: 10

Bin 2667: 76 of cap free
Amount of items: 2
Items: 
Size: 655751 Color: 7
Size: 344174 Color: 9

Bin 2668: 76 of cap free
Amount of items: 2
Items: 
Size: 721022 Color: 15
Size: 278903 Color: 8

Bin 2669: 76 of cap free
Amount of items: 2
Items: 
Size: 773865 Color: 4
Size: 226060 Color: 7

Bin 2670: 77 of cap free
Amount of items: 3
Items: 
Size: 733648 Color: 11
Size: 139967 Color: 18
Size: 126309 Color: 17

Bin 2671: 77 of cap free
Amount of items: 2
Items: 
Size: 684168 Color: 3
Size: 315756 Color: 15

Bin 2672: 77 of cap free
Amount of items: 2
Items: 
Size: 694526 Color: 1
Size: 305398 Color: 8

Bin 2673: 77 of cap free
Amount of items: 2
Items: 
Size: 731716 Color: 4
Size: 268208 Color: 13

Bin 2674: 77 of cap free
Amount of items: 2
Items: 
Size: 761588 Color: 1
Size: 238336 Color: 7

Bin 2675: 77 of cap free
Amount of items: 2
Items: 
Size: 590365 Color: 16
Size: 409559 Color: 19

Bin 2676: 77 of cap free
Amount of items: 2
Items: 
Size: 512792 Color: 7
Size: 487132 Color: 5

Bin 2677: 77 of cap free
Amount of items: 2
Items: 
Size: 556524 Color: 10
Size: 443400 Color: 12

Bin 2678: 78 of cap free
Amount of items: 2
Items: 
Size: 783628 Color: 0
Size: 216295 Color: 13

Bin 2679: 78 of cap free
Amount of items: 2
Items: 
Size: 604881 Color: 7
Size: 395042 Color: 13

Bin 2680: 78 of cap free
Amount of items: 2
Items: 
Size: 767212 Color: 9
Size: 232711 Color: 2

Bin 2681: 78 of cap free
Amount of items: 2
Items: 
Size: 761052 Color: 1
Size: 238871 Color: 13

Bin 2682: 78 of cap free
Amount of items: 2
Items: 
Size: 528912 Color: 6
Size: 471011 Color: 19

Bin 2683: 78 of cap free
Amount of items: 2
Items: 
Size: 701320 Color: 1
Size: 298603 Color: 15

Bin 2684: 78 of cap free
Amount of items: 2
Items: 
Size: 723564 Color: 13
Size: 276359 Color: 15

Bin 2685: 78 of cap free
Amount of items: 2
Items: 
Size: 782679 Color: 3
Size: 217244 Color: 8

Bin 2686: 79 of cap free
Amount of items: 2
Items: 
Size: 751739 Color: 13
Size: 248183 Color: 2

Bin 2687: 79 of cap free
Amount of items: 2
Items: 
Size: 545261 Color: 5
Size: 454661 Color: 13

Bin 2688: 79 of cap free
Amount of items: 2
Items: 
Size: 577350 Color: 19
Size: 422572 Color: 13

Bin 2689: 79 of cap free
Amount of items: 2
Items: 
Size: 604530 Color: 3
Size: 395392 Color: 8

Bin 2690: 79 of cap free
Amount of items: 2
Items: 
Size: 621216 Color: 12
Size: 378706 Color: 3

Bin 2691: 79 of cap free
Amount of items: 2
Items: 
Size: 621340 Color: 11
Size: 378582 Color: 6

Bin 2692: 79 of cap free
Amount of items: 2
Items: 
Size: 624636 Color: 13
Size: 375286 Color: 17

Bin 2693: 79 of cap free
Amount of items: 2
Items: 
Size: 634200 Color: 3
Size: 365722 Color: 8

Bin 2694: 79 of cap free
Amount of items: 2
Items: 
Size: 778373 Color: 2
Size: 221549 Color: 4

Bin 2695: 80 of cap free
Amount of items: 2
Items: 
Size: 665327 Color: 12
Size: 334594 Color: 11

Bin 2696: 80 of cap free
Amount of items: 2
Items: 
Size: 516598 Color: 12
Size: 483323 Color: 11

Bin 2697: 80 of cap free
Amount of items: 2
Items: 
Size: 530599 Color: 10
Size: 469322 Color: 12

Bin 2698: 80 of cap free
Amount of items: 2
Items: 
Size: 549054 Color: 8
Size: 450867 Color: 15

Bin 2699: 80 of cap free
Amount of items: 2
Items: 
Size: 599137 Color: 19
Size: 400784 Color: 18

Bin 2700: 80 of cap free
Amount of items: 2
Items: 
Size: 608152 Color: 14
Size: 391769 Color: 6

Bin 2701: 80 of cap free
Amount of items: 2
Items: 
Size: 716369 Color: 12
Size: 283552 Color: 16

Bin 2702: 80 of cap free
Amount of items: 2
Items: 
Size: 729037 Color: 9
Size: 270884 Color: 17

Bin 2703: 80 of cap free
Amount of items: 2
Items: 
Size: 758453 Color: 12
Size: 241468 Color: 10

Bin 2704: 80 of cap free
Amount of items: 2
Items: 
Size: 762537 Color: 8
Size: 237384 Color: 7

Bin 2705: 81 of cap free
Amount of items: 3
Items: 
Size: 559693 Color: 10
Size: 284129 Color: 19
Size: 156098 Color: 18

Bin 2706: 81 of cap free
Amount of items: 2
Items: 
Size: 666209 Color: 16
Size: 333711 Color: 14

Bin 2707: 81 of cap free
Amount of items: 2
Items: 
Size: 622985 Color: 10
Size: 376935 Color: 5

Bin 2708: 81 of cap free
Amount of items: 2
Items: 
Size: 560454 Color: 10
Size: 439466 Color: 3

Bin 2709: 81 of cap free
Amount of items: 2
Items: 
Size: 555748 Color: 15
Size: 444172 Color: 11

Bin 2710: 81 of cap free
Amount of items: 2
Items: 
Size: 776827 Color: 5
Size: 223093 Color: 17

Bin 2711: 81 of cap free
Amount of items: 3
Items: 
Size: 346055 Color: 13
Size: 344389 Color: 12
Size: 309476 Color: 0

Bin 2712: 81 of cap free
Amount of items: 2
Items: 
Size: 504220 Color: 17
Size: 495700 Color: 2

Bin 2713: 81 of cap free
Amount of items: 2
Items: 
Size: 533449 Color: 14
Size: 466471 Color: 12

Bin 2714: 81 of cap free
Amount of items: 2
Items: 
Size: 606036 Color: 17
Size: 393884 Color: 18

Bin 2715: 81 of cap free
Amount of items: 2
Items: 
Size: 618892 Color: 16
Size: 381028 Color: 8

Bin 2716: 81 of cap free
Amount of items: 2
Items: 
Size: 662927 Color: 6
Size: 336993 Color: 1

Bin 2717: 81 of cap free
Amount of items: 2
Items: 
Size: 675106 Color: 19
Size: 324814 Color: 12

Bin 2718: 81 of cap free
Amount of items: 2
Items: 
Size: 708830 Color: 2
Size: 291090 Color: 4

Bin 2719: 81 of cap free
Amount of items: 2
Items: 
Size: 726786 Color: 14
Size: 273134 Color: 18

Bin 2720: 81 of cap free
Amount of items: 2
Items: 
Size: 774548 Color: 15
Size: 225372 Color: 9

Bin 2721: 82 of cap free
Amount of items: 2
Items: 
Size: 670000 Color: 15
Size: 329919 Color: 1

Bin 2722: 82 of cap free
Amount of items: 2
Items: 
Size: 717000 Color: 11
Size: 282919 Color: 18

Bin 2723: 82 of cap free
Amount of items: 2
Items: 
Size: 570136 Color: 6
Size: 429783 Color: 2

Bin 2724: 82 of cap free
Amount of items: 2
Items: 
Size: 596404 Color: 17
Size: 403515 Color: 15

Bin 2725: 82 of cap free
Amount of items: 2
Items: 
Size: 628415 Color: 13
Size: 371504 Color: 9

Bin 2726: 82 of cap free
Amount of items: 2
Items: 
Size: 649621 Color: 8
Size: 350298 Color: 11

Bin 2727: 82 of cap free
Amount of items: 2
Items: 
Size: 675554 Color: 8
Size: 324365 Color: 2

Bin 2728: 83 of cap free
Amount of items: 3
Items: 
Size: 751666 Color: 8
Size: 128050 Color: 3
Size: 120202 Color: 17

Bin 2729: 83 of cap free
Amount of items: 2
Items: 
Size: 743790 Color: 4
Size: 256128 Color: 7

Bin 2730: 83 of cap free
Amount of items: 3
Items: 
Size: 683686 Color: 13
Size: 158617 Color: 16
Size: 157615 Color: 12

Bin 2731: 83 of cap free
Amount of items: 2
Items: 
Size: 793943 Color: 2
Size: 205975 Color: 19

Bin 2732: 83 of cap free
Amount of items: 3
Items: 
Size: 751112 Color: 0
Size: 124508 Color: 8
Size: 124298 Color: 13

Bin 2733: 83 of cap free
Amount of items: 2
Items: 
Size: 589477 Color: 14
Size: 410441 Color: 11

Bin 2734: 83 of cap free
Amount of items: 2
Items: 
Size: 783852 Color: 7
Size: 216066 Color: 12

Bin 2735: 84 of cap free
Amount of items: 2
Items: 
Size: 580657 Color: 12
Size: 419260 Color: 11

Bin 2736: 84 of cap free
Amount of items: 2
Items: 
Size: 776440 Color: 6
Size: 223477 Color: 1

Bin 2737: 84 of cap free
Amount of items: 2
Items: 
Size: 760058 Color: 12
Size: 239859 Color: 11

Bin 2738: 84 of cap free
Amount of items: 2
Items: 
Size: 531934 Color: 9
Size: 467983 Color: 0

Bin 2739: 84 of cap free
Amount of items: 2
Items: 
Size: 571584 Color: 14
Size: 428333 Color: 15

Bin 2740: 84 of cap free
Amount of items: 2
Items: 
Size: 591730 Color: 3
Size: 408187 Color: 12

Bin 2741: 84 of cap free
Amount of items: 2
Items: 
Size: 708707 Color: 16
Size: 291210 Color: 14

Bin 2742: 84 of cap free
Amount of items: 2
Items: 
Size: 717702 Color: 10
Size: 282215 Color: 13

Bin 2743: 85 of cap free
Amount of items: 3
Items: 
Size: 786806 Color: 4
Size: 112466 Color: 11
Size: 100644 Color: 13

Bin 2744: 85 of cap free
Amount of items: 2
Items: 
Size: 764923 Color: 14
Size: 234993 Color: 16

Bin 2745: 85 of cap free
Amount of items: 2
Items: 
Size: 505602 Color: 15
Size: 494314 Color: 5

Bin 2746: 85 of cap free
Amount of items: 2
Items: 
Size: 772923 Color: 11
Size: 226993 Color: 4

Bin 2747: 85 of cap free
Amount of items: 2
Items: 
Size: 585083 Color: 1
Size: 414833 Color: 19

Bin 2748: 85 of cap free
Amount of items: 3
Items: 
Size: 675343 Color: 16
Size: 185184 Color: 14
Size: 139389 Color: 10

Bin 2749: 85 of cap free
Amount of items: 2
Items: 
Size: 760291 Color: 10
Size: 239625 Color: 14

Bin 2750: 85 of cap free
Amount of items: 3
Items: 
Size: 624770 Color: 5
Size: 188802 Color: 9
Size: 186344 Color: 10

Bin 2751: 85 of cap free
Amount of items: 2
Items: 
Size: 556878 Color: 2
Size: 443038 Color: 7

Bin 2752: 85 of cap free
Amount of items: 2
Items: 
Size: 513093 Color: 0
Size: 486823 Color: 16

Bin 2753: 85 of cap free
Amount of items: 2
Items: 
Size: 517507 Color: 6
Size: 482409 Color: 18

Bin 2754: 85 of cap free
Amount of items: 2
Items: 
Size: 524378 Color: 6
Size: 475538 Color: 11

Bin 2755: 85 of cap free
Amount of items: 2
Items: 
Size: 535856 Color: 1
Size: 464060 Color: 16

Bin 2756: 85 of cap free
Amount of items: 2
Items: 
Size: 564030 Color: 12
Size: 435886 Color: 5

Bin 2757: 85 of cap free
Amount of items: 2
Items: 
Size: 580725 Color: 11
Size: 419191 Color: 18

Bin 2758: 85 of cap free
Amount of items: 2
Items: 
Size: 605563 Color: 3
Size: 394353 Color: 0

Bin 2759: 85 of cap free
Amount of items: 2
Items: 
Size: 698441 Color: 9
Size: 301475 Color: 10

Bin 2760: 86 of cap free
Amount of items: 3
Items: 
Size: 789587 Color: 16
Size: 105296 Color: 18
Size: 105032 Color: 13

Bin 2761: 86 of cap free
Amount of items: 2
Items: 
Size: 743304 Color: 10
Size: 256611 Color: 17

Bin 2762: 86 of cap free
Amount of items: 2
Items: 
Size: 676418 Color: 15
Size: 323497 Color: 12

Bin 2763: 86 of cap free
Amount of items: 2
Items: 
Size: 519202 Color: 9
Size: 480713 Color: 11

Bin 2764: 86 of cap free
Amount of items: 2
Items: 
Size: 523902 Color: 11
Size: 476013 Color: 14

Bin 2765: 86 of cap free
Amount of items: 2
Items: 
Size: 524478 Color: 1
Size: 475437 Color: 9

Bin 2766: 86 of cap free
Amount of items: 2
Items: 
Size: 527138 Color: 7
Size: 472777 Color: 3

Bin 2767: 86 of cap free
Amount of items: 2
Items: 
Size: 633355 Color: 18
Size: 366560 Color: 15

Bin 2768: 86 of cap free
Amount of items: 2
Items: 
Size: 641770 Color: 7
Size: 358145 Color: 13

Bin 2769: 86 of cap free
Amount of items: 2
Items: 
Size: 789585 Color: 12
Size: 210330 Color: 3

Bin 2770: 87 of cap free
Amount of items: 2
Items: 
Size: 644664 Color: 8
Size: 355250 Color: 15

Bin 2771: 87 of cap free
Amount of items: 2
Items: 
Size: 757171 Color: 15
Size: 242743 Color: 13

Bin 2772: 87 of cap free
Amount of items: 2
Items: 
Size: 744240 Color: 15
Size: 255674 Color: 2

Bin 2773: 87 of cap free
Amount of items: 2
Items: 
Size: 647277 Color: 18
Size: 352637 Color: 16

Bin 2774: 87 of cap free
Amount of items: 2
Items: 
Size: 546395 Color: 2
Size: 453519 Color: 13

Bin 2775: 87 of cap free
Amount of items: 2
Items: 
Size: 627391 Color: 10
Size: 372523 Color: 16

Bin 2776: 87 of cap free
Amount of items: 2
Items: 
Size: 777755 Color: 7
Size: 222159 Color: 4

Bin 2777: 88 of cap free
Amount of items: 2
Items: 
Size: 538097 Color: 2
Size: 461816 Color: 13

Bin 2778: 88 of cap free
Amount of items: 2
Items: 
Size: 685205 Color: 6
Size: 314708 Color: 18

Bin 2779: 89 of cap free
Amount of items: 2
Items: 
Size: 649131 Color: 1
Size: 350781 Color: 11

Bin 2780: 89 of cap free
Amount of items: 2
Items: 
Size: 749376 Color: 19
Size: 250536 Color: 1

Bin 2781: 89 of cap free
Amount of items: 2
Items: 
Size: 587652 Color: 4
Size: 412260 Color: 3

Bin 2782: 89 of cap free
Amount of items: 2
Items: 
Size: 631345 Color: 2
Size: 368567 Color: 18

Bin 2783: 89 of cap free
Amount of items: 2
Items: 
Size: 791161 Color: 11
Size: 208751 Color: 18

Bin 2784: 89 of cap free
Amount of items: 2
Items: 
Size: 550014 Color: 14
Size: 449898 Color: 9

Bin 2785: 89 of cap free
Amount of items: 2
Items: 
Size: 517378 Color: 8
Size: 482534 Color: 1

Bin 2786: 89 of cap free
Amount of items: 2
Items: 
Size: 599584 Color: 10
Size: 400328 Color: 18

Bin 2787: 89 of cap free
Amount of items: 2
Items: 
Size: 637961 Color: 15
Size: 361951 Color: 6

Bin 2788: 90 of cap free
Amount of items: 2
Items: 
Size: 531903 Color: 10
Size: 468008 Color: 0

Bin 2789: 90 of cap free
Amount of items: 3
Items: 
Size: 689847 Color: 2
Size: 159286 Color: 11
Size: 150778 Color: 4

Bin 2790: 90 of cap free
Amount of items: 2
Items: 
Size: 760793 Color: 7
Size: 239118 Color: 17

Bin 2791: 90 of cap free
Amount of items: 2
Items: 
Size: 770489 Color: 4
Size: 229422 Color: 9

Bin 2792: 90 of cap free
Amount of items: 2
Items: 
Size: 521067 Color: 3
Size: 478844 Color: 5

Bin 2793: 90 of cap free
Amount of items: 2
Items: 
Size: 543355 Color: 6
Size: 456556 Color: 18

Bin 2794: 90 of cap free
Amount of items: 2
Items: 
Size: 551076 Color: 9
Size: 448835 Color: 19

Bin 2795: 90 of cap free
Amount of items: 2
Items: 
Size: 559404 Color: 18
Size: 440507 Color: 5

Bin 2796: 90 of cap free
Amount of items: 2
Items: 
Size: 589290 Color: 5
Size: 410621 Color: 12

Bin 2797: 90 of cap free
Amount of items: 2
Items: 
Size: 624476 Color: 17
Size: 375435 Color: 6

Bin 2798: 90 of cap free
Amount of items: 2
Items: 
Size: 640647 Color: 10
Size: 359264 Color: 5

Bin 2799: 90 of cap free
Amount of items: 2
Items: 
Size: 676320 Color: 8
Size: 323591 Color: 6

Bin 2800: 90 of cap free
Amount of items: 2
Items: 
Size: 712482 Color: 18
Size: 287429 Color: 1

Bin 2801: 90 of cap free
Amount of items: 2
Items: 
Size: 734049 Color: 3
Size: 265862 Color: 9

Bin 2802: 91 of cap free
Amount of items: 2
Items: 
Size: 685106 Color: 15
Size: 314804 Color: 0

Bin 2803: 91 of cap free
Amount of items: 2
Items: 
Size: 725478 Color: 17
Size: 274432 Color: 15

Bin 2804: 91 of cap free
Amount of items: 2
Items: 
Size: 575537 Color: 10
Size: 424373 Color: 19

Bin 2805: 91 of cap free
Amount of items: 2
Items: 
Size: 529675 Color: 0
Size: 470235 Color: 9

Bin 2806: 91 of cap free
Amount of items: 2
Items: 
Size: 623532 Color: 18
Size: 376378 Color: 9

Bin 2807: 91 of cap free
Amount of items: 2
Items: 
Size: 653058 Color: 16
Size: 346852 Color: 14

Bin 2808: 91 of cap free
Amount of items: 2
Items: 
Size: 664319 Color: 8
Size: 335591 Color: 9

Bin 2809: 91 of cap free
Amount of items: 2
Items: 
Size: 776588 Color: 5
Size: 223322 Color: 19

Bin 2810: 92 of cap free
Amount of items: 2
Items: 
Size: 716750 Color: 17
Size: 283159 Color: 11

Bin 2811: 92 of cap free
Amount of items: 2
Items: 
Size: 565466 Color: 8
Size: 434443 Color: 1

Bin 2812: 92 of cap free
Amount of items: 2
Items: 
Size: 763612 Color: 6
Size: 236297 Color: 19

Bin 2813: 92 of cap free
Amount of items: 2
Items: 
Size: 519998 Color: 15
Size: 479911 Color: 5

Bin 2814: 92 of cap free
Amount of items: 2
Items: 
Size: 574671 Color: 2
Size: 425238 Color: 7

Bin 2815: 92 of cap free
Amount of items: 2
Items: 
Size: 665987 Color: 17
Size: 333922 Color: 18

Bin 2816: 92 of cap free
Amount of items: 2
Items: 
Size: 621703 Color: 11
Size: 378206 Color: 13

Bin 2817: 92 of cap free
Amount of items: 2
Items: 
Size: 552148 Color: 9
Size: 447761 Color: 17

Bin 2818: 92 of cap free
Amount of items: 2
Items: 
Size: 652766 Color: 0
Size: 347143 Color: 19

Bin 2819: 92 of cap free
Amount of items: 2
Items: 
Size: 705288 Color: 3
Size: 294621 Color: 15

Bin 2820: 92 of cap free
Amount of items: 2
Items: 
Size: 764081 Color: 11
Size: 235828 Color: 10

Bin 2821: 92 of cap free
Amount of items: 2
Items: 
Size: 787434 Color: 5
Size: 212475 Color: 10

Bin 2822: 93 of cap free
Amount of items: 2
Items: 
Size: 500492 Color: 6
Size: 499416 Color: 4

Bin 2823: 93 of cap free
Amount of items: 2
Items: 
Size: 602530 Color: 3
Size: 397378 Color: 5

Bin 2824: 93 of cap free
Amount of items: 2
Items: 
Size: 692042 Color: 0
Size: 307866 Color: 5

Bin 2825: 93 of cap free
Amount of items: 2
Items: 
Size: 759886 Color: 10
Size: 240022 Color: 8

Bin 2826: 93 of cap free
Amount of items: 2
Items: 
Size: 707519 Color: 14
Size: 292389 Color: 9

Bin 2827: 93 of cap free
Amount of items: 2
Items: 
Size: 510230 Color: 18
Size: 489678 Color: 12

Bin 2828: 93 of cap free
Amount of items: 2
Items: 
Size: 534784 Color: 1
Size: 465124 Color: 12

Bin 2829: 93 of cap free
Amount of items: 2
Items: 
Size: 617741 Color: 7
Size: 382167 Color: 17

Bin 2830: 93 of cap free
Amount of items: 2
Items: 
Size: 628641 Color: 11
Size: 371267 Color: 12

Bin 2831: 93 of cap free
Amount of items: 2
Items: 
Size: 770953 Color: 7
Size: 228955 Color: 1

Bin 2832: 93 of cap free
Amount of items: 2
Items: 
Size: 779217 Color: 14
Size: 220691 Color: 5

Bin 2833: 94 of cap free
Amount of items: 2
Items: 
Size: 589861 Color: 1
Size: 410046 Color: 6

Bin 2834: 94 of cap free
Amount of items: 2
Items: 
Size: 646052 Color: 11
Size: 353855 Color: 15

Bin 2835: 94 of cap free
Amount of items: 2
Items: 
Size: 501018 Color: 1
Size: 498889 Color: 10

Bin 2836: 94 of cap free
Amount of items: 2
Items: 
Size: 510036 Color: 6
Size: 489871 Color: 3

Bin 2837: 94 of cap free
Amount of items: 2
Items: 
Size: 523729 Color: 3
Size: 476178 Color: 1

Bin 2838: 94 of cap free
Amount of items: 2
Items: 
Size: 533794 Color: 7
Size: 466113 Color: 11

Bin 2839: 94 of cap free
Amount of items: 2
Items: 
Size: 540617 Color: 1
Size: 459290 Color: 4

Bin 2840: 94 of cap free
Amount of items: 2
Items: 
Size: 604060 Color: 19
Size: 395847 Color: 14

Bin 2841: 94 of cap free
Amount of items: 2
Items: 
Size: 634041 Color: 10
Size: 365866 Color: 14

Bin 2842: 94 of cap free
Amount of items: 2
Items: 
Size: 647147 Color: 3
Size: 352760 Color: 12

Bin 2843: 94 of cap free
Amount of items: 2
Items: 
Size: 660016 Color: 4
Size: 339891 Color: 2

Bin 2844: 94 of cap free
Amount of items: 2
Items: 
Size: 715969 Color: 4
Size: 283938 Color: 7

Bin 2845: 94 of cap free
Amount of items: 2
Items: 
Size: 778361 Color: 18
Size: 221546 Color: 6

Bin 2846: 95 of cap free
Amount of items: 2
Items: 
Size: 686674 Color: 7
Size: 313232 Color: 10

Bin 2847: 95 of cap free
Amount of items: 2
Items: 
Size: 797912 Color: 3
Size: 201994 Color: 15

Bin 2848: 95 of cap free
Amount of items: 2
Items: 
Size: 534345 Color: 9
Size: 465561 Color: 8

Bin 2849: 95 of cap free
Amount of items: 2
Items: 
Size: 579194 Color: 13
Size: 420712 Color: 5

Bin 2850: 95 of cap free
Amount of items: 2
Items: 
Size: 609053 Color: 3
Size: 390853 Color: 10

Bin 2851: 95 of cap free
Amount of items: 2
Items: 
Size: 684705 Color: 9
Size: 315201 Color: 5

Bin 2852: 95 of cap free
Amount of items: 2
Items: 
Size: 688781 Color: 17
Size: 311125 Color: 12

Bin 2853: 95 of cap free
Amount of items: 2
Items: 
Size: 740604 Color: 14
Size: 259302 Color: 10

Bin 2854: 95 of cap free
Amount of items: 2
Items: 
Size: 745968 Color: 4
Size: 253938 Color: 9

Bin 2855: 96 of cap free
Amount of items: 2
Items: 
Size: 560967 Color: 12
Size: 438938 Color: 0

Bin 2856: 96 of cap free
Amount of items: 2
Items: 
Size: 745743 Color: 8
Size: 254162 Color: 11

Bin 2857: 96 of cap free
Amount of items: 2
Items: 
Size: 670983 Color: 1
Size: 328922 Color: 8

Bin 2858: 96 of cap free
Amount of items: 2
Items: 
Size: 527925 Color: 1
Size: 471980 Color: 18

Bin 2859: 96 of cap free
Amount of items: 2
Items: 
Size: 532508 Color: 16
Size: 467397 Color: 0

Bin 2860: 96 of cap free
Amount of items: 2
Items: 
Size: 535894 Color: 11
Size: 464011 Color: 9

Bin 2861: 96 of cap free
Amount of items: 2
Items: 
Size: 548196 Color: 18
Size: 451709 Color: 4

Bin 2862: 96 of cap free
Amount of items: 2
Items: 
Size: 690605 Color: 4
Size: 309300 Color: 15

Bin 2863: 96 of cap free
Amount of items: 2
Items: 
Size: 721927 Color: 0
Size: 277978 Color: 15

Bin 2864: 96 of cap free
Amount of items: 2
Items: 
Size: 794144 Color: 19
Size: 205761 Color: 1

Bin 2865: 97 of cap free
Amount of items: 2
Items: 
Size: 705725 Color: 1
Size: 294179 Color: 15

Bin 2866: 97 of cap free
Amount of items: 2
Items: 
Size: 706504 Color: 13
Size: 293400 Color: 1

Bin 2867: 97 of cap free
Amount of items: 2
Items: 
Size: 687863 Color: 17
Size: 312041 Color: 15

Bin 2868: 97 of cap free
Amount of items: 2
Items: 
Size: 663561 Color: 4
Size: 336343 Color: 0

Bin 2869: 97 of cap free
Amount of items: 2
Items: 
Size: 616480 Color: 14
Size: 383424 Color: 0

Bin 2870: 97 of cap free
Amount of items: 2
Items: 
Size: 523479 Color: 10
Size: 476425 Color: 12

Bin 2871: 97 of cap free
Amount of items: 2
Items: 
Size: 600094 Color: 13
Size: 399810 Color: 19

Bin 2872: 98 of cap free
Amount of items: 2
Items: 
Size: 796800 Color: 18
Size: 203103 Color: 10

Bin 2873: 98 of cap free
Amount of items: 2
Items: 
Size: 681138 Color: 8
Size: 318765 Color: 2

Bin 2874: 98 of cap free
Amount of items: 2
Items: 
Size: 709934 Color: 16
Size: 289969 Color: 18

Bin 2875: 98 of cap free
Amount of items: 2
Items: 
Size: 651886 Color: 8
Size: 348017 Color: 15

Bin 2876: 98 of cap free
Amount of items: 2
Items: 
Size: 753356 Color: 16
Size: 246547 Color: 4

Bin 2877: 99 of cap free
Amount of items: 2
Items: 
Size: 778234 Color: 1
Size: 221668 Color: 13

Bin 2878: 99 of cap free
Amount of items: 2
Items: 
Size: 655864 Color: 12
Size: 344038 Color: 10

Bin 2879: 99 of cap free
Amount of items: 2
Items: 
Size: 543477 Color: 16
Size: 456425 Color: 11

Bin 2880: 99 of cap free
Amount of items: 2
Items: 
Size: 774956 Color: 11
Size: 224946 Color: 9

Bin 2881: 99 of cap free
Amount of items: 2
Items: 
Size: 507092 Color: 12
Size: 492810 Color: 10

Bin 2882: 99 of cap free
Amount of items: 2
Items: 
Size: 541681 Color: 13
Size: 458221 Color: 17

Bin 2883: 99 of cap free
Amount of items: 2
Items: 
Size: 696734 Color: 6
Size: 303168 Color: 12

Bin 2884: 99 of cap free
Amount of items: 2
Items: 
Size: 782199 Color: 9
Size: 217703 Color: 17

Bin 2885: 100 of cap free
Amount of items: 2
Items: 
Size: 729910 Color: 5
Size: 269991 Color: 18

Bin 2886: 100 of cap free
Amount of items: 2
Items: 
Size: 542189 Color: 19
Size: 457712 Color: 10

Bin 2887: 100 of cap free
Amount of items: 2
Items: 
Size: 601284 Color: 8
Size: 398617 Color: 18

Bin 2888: 100 of cap free
Amount of items: 3
Items: 
Size: 621211 Color: 6
Size: 189896 Color: 13
Size: 188794 Color: 14

Bin 2889: 100 of cap free
Amount of items: 2
Items: 
Size: 539089 Color: 3
Size: 460812 Color: 0

Bin 2890: 100 of cap free
Amount of items: 2
Items: 
Size: 606195 Color: 14
Size: 393706 Color: 2

Bin 2891: 100 of cap free
Amount of items: 2
Items: 
Size: 623295 Color: 17
Size: 376606 Color: 5

Bin 2892: 101 of cap free
Amount of items: 2
Items: 
Size: 765127 Color: 7
Size: 234773 Color: 6

Bin 2893: 101 of cap free
Amount of items: 2
Items: 
Size: 600318 Color: 1
Size: 399582 Color: 19

Bin 2894: 101 of cap free
Amount of items: 2
Items: 
Size: 739600 Color: 12
Size: 260300 Color: 3

Bin 2895: 101 of cap free
Amount of items: 2
Items: 
Size: 797204 Color: 12
Size: 202696 Color: 3

Bin 2896: 101 of cap free
Amount of items: 2
Items: 
Size: 780088 Color: 9
Size: 219812 Color: 2

Bin 2897: 101 of cap free
Amount of items: 2
Items: 
Size: 774268 Color: 19
Size: 225632 Color: 2

Bin 2898: 101 of cap free
Amount of items: 2
Items: 
Size: 757645 Color: 12
Size: 242255 Color: 4

Bin 2899: 101 of cap free
Amount of items: 2
Items: 
Size: 554627 Color: 9
Size: 445273 Color: 18

Bin 2900: 101 of cap free
Amount of items: 2
Items: 
Size: 569134 Color: 10
Size: 430766 Color: 11

Bin 2901: 101 of cap free
Amount of items: 2
Items: 
Size: 703699 Color: 12
Size: 296201 Color: 2

Bin 2902: 102 of cap free
Amount of items: 3
Items: 
Size: 759117 Color: 13
Size: 124054 Color: 19
Size: 116728 Color: 8

Bin 2903: 102 of cap free
Amount of items: 2
Items: 
Size: 676902 Color: 7
Size: 322997 Color: 2

Bin 2904: 102 of cap free
Amount of items: 2
Items: 
Size: 710807 Color: 7
Size: 289092 Color: 17

Bin 2905: 102 of cap free
Amount of items: 3
Items: 
Size: 653602 Color: 14
Size: 173196 Color: 15
Size: 173101 Color: 17

Bin 2906: 102 of cap free
Amount of items: 3
Items: 
Size: 766213 Color: 12
Size: 119908 Color: 0
Size: 113778 Color: 4

Bin 2907: 102 of cap free
Amount of items: 2
Items: 
Size: 712470 Color: 4
Size: 287429 Color: 16

Bin 2908: 102 of cap free
Amount of items: 2
Items: 
Size: 567143 Color: 19
Size: 432756 Color: 6

Bin 2909: 102 of cap free
Amount of items: 2
Items: 
Size: 588276 Color: 14
Size: 411623 Color: 0

Bin 2910: 103 of cap free
Amount of items: 2
Items: 
Size: 504980 Color: 6
Size: 494918 Color: 5

Bin 2911: 103 of cap free
Amount of items: 2
Items: 
Size: 612152 Color: 3
Size: 387746 Color: 2

Bin 2912: 103 of cap free
Amount of items: 2
Items: 
Size: 640128 Color: 0
Size: 359770 Color: 7

Bin 2913: 103 of cap free
Amount of items: 2
Items: 
Size: 631073 Color: 13
Size: 368825 Color: 0

Bin 2914: 103 of cap free
Amount of items: 2
Items: 
Size: 513229 Color: 15
Size: 486669 Color: 0

Bin 2915: 103 of cap free
Amount of items: 2
Items: 
Size: 551172 Color: 5
Size: 448726 Color: 4

Bin 2916: 103 of cap free
Amount of items: 2
Items: 
Size: 593045 Color: 3
Size: 406853 Color: 1

Bin 2917: 103 of cap free
Amount of items: 2
Items: 
Size: 682426 Color: 5
Size: 317472 Color: 9

Bin 2918: 103 of cap free
Amount of items: 2
Items: 
Size: 714667 Color: 12
Size: 285231 Color: 2

Bin 2919: 103 of cap free
Amount of items: 2
Items: 
Size: 739000 Color: 3
Size: 260898 Color: 8

Bin 2920: 103 of cap free
Amount of items: 2
Items: 
Size: 761211 Color: 17
Size: 238687 Color: 3

Bin 2921: 103 of cap free
Amount of items: 2
Items: 
Size: 779331 Color: 3
Size: 220567 Color: 9

Bin 2922: 103 of cap free
Amount of items: 2
Items: 
Size: 794831 Color: 8
Size: 205067 Color: 14

Bin 2923: 104 of cap free
Amount of items: 2
Items: 
Size: 624879 Color: 5
Size: 375018 Color: 7

Bin 2924: 105 of cap free
Amount of items: 3
Items: 
Size: 511161 Color: 11
Size: 244531 Color: 3
Size: 244204 Color: 7

Bin 2925: 105 of cap free
Amount of items: 2
Items: 
Size: 595728 Color: 12
Size: 404168 Color: 14

Bin 2926: 105 of cap free
Amount of items: 2
Items: 
Size: 667771 Color: 3
Size: 332125 Color: 10

Bin 2927: 105 of cap free
Amount of items: 2
Items: 
Size: 736219 Color: 9
Size: 263677 Color: 16

Bin 2928: 106 of cap free
Amount of items: 3
Items: 
Size: 731054 Color: 0
Size: 137821 Color: 1
Size: 131020 Color: 10

Bin 2929: 106 of cap free
Amount of items: 2
Items: 
Size: 508516 Color: 11
Size: 491379 Color: 3

Bin 2930: 106 of cap free
Amount of items: 2
Items: 
Size: 538683 Color: 18
Size: 461212 Color: 1

Bin 2931: 106 of cap free
Amount of items: 2
Items: 
Size: 546381 Color: 13
Size: 453514 Color: 17

Bin 2932: 106 of cap free
Amount of items: 2
Items: 
Size: 584217 Color: 4
Size: 415678 Color: 5

Bin 2933: 106 of cap free
Amount of items: 2
Items: 
Size: 587825 Color: 3
Size: 412070 Color: 13

Bin 2934: 106 of cap free
Amount of items: 2
Items: 
Size: 600729 Color: 14
Size: 399166 Color: 9

Bin 2935: 106 of cap free
Amount of items: 2
Items: 
Size: 641309 Color: 8
Size: 358586 Color: 0

Bin 2936: 106 of cap free
Amount of items: 2
Items: 
Size: 674161 Color: 10
Size: 325734 Color: 0

Bin 2937: 107 of cap free
Amount of items: 2
Items: 
Size: 764916 Color: 0
Size: 234978 Color: 2

Bin 2938: 107 of cap free
Amount of items: 3
Items: 
Size: 745127 Color: 1
Size: 139942 Color: 12
Size: 114825 Color: 1

Bin 2939: 107 of cap free
Amount of items: 2
Items: 
Size: 690310 Color: 10
Size: 309584 Color: 13

Bin 2940: 107 of cap free
Amount of items: 2
Items: 
Size: 660661 Color: 2
Size: 339233 Color: 7

Bin 2941: 107 of cap free
Amount of items: 2
Items: 
Size: 726416 Color: 14
Size: 273478 Color: 0

Bin 2942: 107 of cap free
Amount of items: 2
Items: 
Size: 746682 Color: 18
Size: 253212 Color: 12

Bin 2943: 107 of cap free
Amount of items: 2
Items: 
Size: 729855 Color: 0
Size: 270039 Color: 11

Bin 2944: 107 of cap free
Amount of items: 2
Items: 
Size: 688556 Color: 19
Size: 311338 Color: 5

Bin 2945: 107 of cap free
Amount of items: 2
Items: 
Size: 775530 Color: 9
Size: 224364 Color: 7

Bin 2946: 107 of cap free
Amount of items: 2
Items: 
Size: 517918 Color: 10
Size: 481976 Color: 2

Bin 2947: 107 of cap free
Amount of items: 2
Items: 
Size: 580284 Color: 12
Size: 419610 Color: 10

Bin 2948: 107 of cap free
Amount of items: 2
Items: 
Size: 582764 Color: 7
Size: 417130 Color: 1

Bin 2949: 107 of cap free
Amount of items: 2
Items: 
Size: 585789 Color: 9
Size: 414105 Color: 5

Bin 2950: 107 of cap free
Amount of items: 3
Items: 
Size: 588953 Color: 18
Size: 205480 Color: 5
Size: 205461 Color: 6

Bin 2951: 107 of cap free
Amount of items: 2
Items: 
Size: 629228 Color: 2
Size: 370666 Color: 7

Bin 2952: 107 of cap free
Amount of items: 2
Items: 
Size: 665693 Color: 0
Size: 334201 Color: 9

Bin 2953: 107 of cap free
Amount of items: 2
Items: 
Size: 679483 Color: 10
Size: 320411 Color: 14

Bin 2954: 108 of cap free
Amount of items: 3
Items: 
Size: 506280 Color: 11
Size: 264562 Color: 9
Size: 229051 Color: 6

Bin 2955: 108 of cap free
Amount of items: 2
Items: 
Size: 784287 Color: 9
Size: 215606 Color: 7

Bin 2956: 108 of cap free
Amount of items: 2
Items: 
Size: 735458 Color: 12
Size: 264435 Color: 9

Bin 2957: 108 of cap free
Amount of items: 2
Items: 
Size: 719974 Color: 6
Size: 279919 Color: 16

Bin 2958: 108 of cap free
Amount of items: 2
Items: 
Size: 736094 Color: 14
Size: 263799 Color: 17

Bin 2959: 108 of cap free
Amount of items: 2
Items: 
Size: 606939 Color: 8
Size: 392954 Color: 5

Bin 2960: 108 of cap free
Amount of items: 2
Items: 
Size: 502933 Color: 13
Size: 496960 Color: 14

Bin 2961: 108 of cap free
Amount of items: 2
Items: 
Size: 619621 Color: 5
Size: 380272 Color: 1

Bin 2962: 108 of cap free
Amount of items: 2
Items: 
Size: 639299 Color: 6
Size: 360594 Color: 1

Bin 2963: 108 of cap free
Amount of items: 2
Items: 
Size: 710990 Color: 4
Size: 288903 Color: 14

Bin 2964: 108 of cap free
Amount of items: 2
Items: 
Size: 720779 Color: 15
Size: 279114 Color: 11

Bin 2965: 109 of cap free
Amount of items: 3
Items: 
Size: 642871 Color: 3
Size: 178742 Color: 16
Size: 178279 Color: 14

Bin 2966: 109 of cap free
Amount of items: 2
Items: 
Size: 749574 Color: 19
Size: 250318 Color: 9

Bin 2967: 109 of cap free
Amount of items: 2
Items: 
Size: 529655 Color: 10
Size: 470237 Color: 0

Bin 2968: 109 of cap free
Amount of items: 2
Items: 
Size: 598824 Color: 15
Size: 401068 Color: 13

Bin 2969: 109 of cap free
Amount of items: 2
Items: 
Size: 608306 Color: 16
Size: 391586 Color: 4

Bin 2970: 109 of cap free
Amount of items: 2
Items: 
Size: 625973 Color: 7
Size: 373919 Color: 5

Bin 2971: 110 of cap free
Amount of items: 2
Items: 
Size: 663882 Color: 4
Size: 336009 Color: 5

Bin 2972: 110 of cap free
Amount of items: 2
Items: 
Size: 744805 Color: 12
Size: 255086 Color: 5

Bin 2973: 110 of cap free
Amount of items: 2
Items: 
Size: 560448 Color: 8
Size: 439443 Color: 13

Bin 2974: 110 of cap free
Amount of items: 2
Items: 
Size: 565197 Color: 14
Size: 434694 Color: 1

Bin 2975: 110 of cap free
Amount of items: 2
Items: 
Size: 597712 Color: 3
Size: 402179 Color: 11

Bin 2976: 110 of cap free
Amount of items: 2
Items: 
Size: 791734 Color: 16
Size: 208157 Color: 8

Bin 2977: 111 of cap free
Amount of items: 2
Items: 
Size: 695350 Color: 4
Size: 304540 Color: 7

Bin 2978: 111 of cap free
Amount of items: 2
Items: 
Size: 731044 Color: 3
Size: 268846 Color: 19

Bin 2979: 111 of cap free
Amount of items: 2
Items: 
Size: 796636 Color: 5
Size: 203254 Color: 19

Bin 2980: 111 of cap free
Amount of items: 2
Items: 
Size: 504205 Color: 9
Size: 495685 Color: 3

Bin 2981: 111 of cap free
Amount of items: 2
Items: 
Size: 528456 Color: 5
Size: 471434 Color: 19

Bin 2982: 112 of cap free
Amount of items: 2
Items: 
Size: 769456 Color: 5
Size: 230433 Color: 19

Bin 2983: 112 of cap free
Amount of items: 2
Items: 
Size: 737097 Color: 16
Size: 262792 Color: 11

Bin 2984: 112 of cap free
Amount of items: 2
Items: 
Size: 779566 Color: 13
Size: 220323 Color: 0

Bin 2985: 112 of cap free
Amount of items: 2
Items: 
Size: 705076 Color: 8
Size: 294813 Color: 17

Bin 2986: 112 of cap free
Amount of items: 2
Items: 
Size: 519827 Color: 1
Size: 480062 Color: 12

Bin 2987: 112 of cap free
Amount of items: 2
Items: 
Size: 583566 Color: 5
Size: 416323 Color: 1

Bin 2988: 112 of cap free
Amount of items: 2
Items: 
Size: 513970 Color: 11
Size: 485919 Color: 14

Bin 2989: 112 of cap free
Amount of items: 2
Items: 
Size: 533917 Color: 14
Size: 465972 Color: 19

Bin 2990: 112 of cap free
Amount of items: 2
Items: 
Size: 673995 Color: 17
Size: 325894 Color: 16

Bin 2991: 112 of cap free
Amount of items: 2
Items: 
Size: 750769 Color: 17
Size: 249120 Color: 18

Bin 2992: 113 of cap free
Amount of items: 2
Items: 
Size: 783953 Color: 4
Size: 215935 Color: 2

Bin 2993: 113 of cap free
Amount of items: 2
Items: 
Size: 537403 Color: 9
Size: 462485 Color: 19

Bin 2994: 113 of cap free
Amount of items: 2
Items: 
Size: 503097 Color: 1
Size: 496791 Color: 11

Bin 2995: 113 of cap free
Amount of items: 2
Items: 
Size: 520762 Color: 19
Size: 479126 Color: 6

Bin 2996: 113 of cap free
Amount of items: 2
Items: 
Size: 702215 Color: 14
Size: 297673 Color: 6

Bin 2997: 114 of cap free
Amount of items: 2
Items: 
Size: 685785 Color: 17
Size: 314102 Color: 2

Bin 2998: 114 of cap free
Amount of items: 2
Items: 
Size: 650455 Color: 0
Size: 349432 Color: 19

Bin 2999: 114 of cap free
Amount of items: 2
Items: 
Size: 732418 Color: 19
Size: 267469 Color: 13

Bin 3000: 114 of cap free
Amount of items: 2
Items: 
Size: 786005 Color: 19
Size: 213882 Color: 18

Bin 3001: 114 of cap free
Amount of items: 2
Items: 
Size: 665969 Color: 9
Size: 333918 Color: 14

Bin 3002: 114 of cap free
Amount of items: 2
Items: 
Size: 516362 Color: 5
Size: 483525 Color: 4

Bin 3003: 114 of cap free
Amount of items: 2
Items: 
Size: 526691 Color: 10
Size: 473196 Color: 8

Bin 3004: 114 of cap free
Amount of items: 2
Items: 
Size: 537176 Color: 17
Size: 462711 Color: 1

Bin 3005: 114 of cap free
Amount of items: 2
Items: 
Size: 576036 Color: 8
Size: 423851 Color: 5

Bin 3006: 114 of cap free
Amount of items: 2
Items: 
Size: 601432 Color: 0
Size: 398455 Color: 11

Bin 3007: 115 of cap free
Amount of items: 2
Items: 
Size: 616847 Color: 17
Size: 383039 Color: 14

Bin 3008: 115 of cap free
Amount of items: 2
Items: 
Size: 576603 Color: 2
Size: 423283 Color: 14

Bin 3009: 115 of cap free
Amount of items: 2
Items: 
Size: 595730 Color: 15
Size: 404156 Color: 1

Bin 3010: 115 of cap free
Amount of items: 2
Items: 
Size: 669727 Color: 13
Size: 330159 Color: 1

Bin 3011: 116 of cap free
Amount of items: 2
Items: 
Size: 783510 Color: 10
Size: 216375 Color: 13

Bin 3012: 116 of cap free
Amount of items: 2
Items: 
Size: 712273 Color: 5
Size: 287612 Color: 19

Bin 3013: 116 of cap free
Amount of items: 2
Items: 
Size: 531485 Color: 4
Size: 468400 Color: 15

Bin 3014: 116 of cap free
Amount of items: 2
Items: 
Size: 550908 Color: 8
Size: 448977 Color: 15

Bin 3015: 116 of cap free
Amount of items: 2
Items: 
Size: 609044 Color: 1
Size: 390841 Color: 3

Bin 3016: 116 of cap free
Amount of items: 2
Items: 
Size: 646600 Color: 19
Size: 353285 Color: 18

Bin 3017: 116 of cap free
Amount of items: 2
Items: 
Size: 694180 Color: 0
Size: 305705 Color: 3

Bin 3018: 117 of cap free
Amount of items: 2
Items: 
Size: 573172 Color: 2
Size: 426712 Color: 14

Bin 3019: 117 of cap free
Amount of items: 3
Items: 
Size: 619400 Color: 9
Size: 200014 Color: 6
Size: 180470 Color: 0

Bin 3020: 117 of cap free
Amount of items: 2
Items: 
Size: 514183 Color: 18
Size: 485701 Color: 2

Bin 3021: 117 of cap free
Amount of items: 2
Items: 
Size: 600895 Color: 15
Size: 398989 Color: 14

Bin 3022: 117 of cap free
Amount of items: 2
Items: 
Size: 715410 Color: 5
Size: 284474 Color: 10

Bin 3023: 117 of cap free
Amount of items: 2
Items: 
Size: 782672 Color: 5
Size: 217212 Color: 2

Bin 3024: 118 of cap free
Amount of items: 3
Items: 
Size: 662494 Color: 12
Size: 169197 Color: 11
Size: 168192 Color: 15

Bin 3025: 118 of cap free
Amount of items: 2
Items: 
Size: 701730 Color: 10
Size: 298153 Color: 8

Bin 3026: 118 of cap free
Amount of items: 2
Items: 
Size: 615274 Color: 16
Size: 384609 Color: 8

Bin 3027: 118 of cap free
Amount of items: 3
Items: 
Size: 364246 Color: 8
Size: 340867 Color: 19
Size: 294770 Color: 3

Bin 3028: 118 of cap free
Amount of items: 2
Items: 
Size: 540351 Color: 15
Size: 459532 Color: 5

Bin 3029: 118 of cap free
Amount of items: 2
Items: 
Size: 546654 Color: 3
Size: 453229 Color: 17

Bin 3030: 118 of cap free
Amount of items: 2
Items: 
Size: 620514 Color: 18
Size: 379369 Color: 3

Bin 3031: 118 of cap free
Amount of items: 2
Items: 
Size: 768255 Color: 18
Size: 231628 Color: 7

Bin 3032: 119 of cap free
Amount of items: 2
Items: 
Size: 623680 Color: 15
Size: 376202 Color: 12

Bin 3033: 119 of cap free
Amount of items: 2
Items: 
Size: 735904 Color: 14
Size: 263978 Color: 2

Bin 3034: 119 of cap free
Amount of items: 2
Items: 
Size: 601943 Color: 8
Size: 397939 Color: 18

Bin 3035: 119 of cap free
Amount of items: 2
Items: 
Size: 609649 Color: 19
Size: 390233 Color: 5

Bin 3036: 120 of cap free
Amount of items: 2
Items: 
Size: 769103 Color: 11
Size: 230778 Color: 17

Bin 3037: 120 of cap free
Amount of items: 2
Items: 
Size: 682263 Color: 7
Size: 317618 Color: 1

Bin 3038: 120 of cap free
Amount of items: 2
Items: 
Size: 722324 Color: 14
Size: 277557 Color: 7

Bin 3039: 120 of cap free
Amount of items: 2
Items: 
Size: 552371 Color: 4
Size: 447510 Color: 6

Bin 3040: 120 of cap free
Amount of items: 2
Items: 
Size: 795666 Color: 10
Size: 204215 Color: 6

Bin 3041: 120 of cap free
Amount of items: 2
Items: 
Size: 685659 Color: 10
Size: 314222 Color: 7

Bin 3042: 120 of cap free
Amount of items: 2
Items: 
Size: 519468 Color: 14
Size: 480413 Color: 10

Bin 3043: 120 of cap free
Amount of items: 2
Items: 
Size: 536524 Color: 7
Size: 463357 Color: 10

Bin 3044: 120 of cap free
Amount of items: 2
Items: 
Size: 594265 Color: 12
Size: 405616 Color: 18

Bin 3045: 120 of cap free
Amount of items: 2
Items: 
Size: 632263 Color: 16
Size: 367618 Color: 2

Bin 3046: 120 of cap free
Amount of items: 2
Items: 
Size: 720557 Color: 17
Size: 279324 Color: 10

Bin 3047: 121 of cap free
Amount of items: 2
Items: 
Size: 755987 Color: 4
Size: 243893 Color: 12

Bin 3048: 121 of cap free
Amount of items: 2
Items: 
Size: 627205 Color: 7
Size: 372675 Color: 16

Bin 3049: 121 of cap free
Amount of items: 2
Items: 
Size: 557720 Color: 15
Size: 442160 Color: 14

Bin 3050: 121 of cap free
Amount of items: 2
Items: 
Size: 581501 Color: 2
Size: 418379 Color: 10

Bin 3051: 121 of cap free
Amount of items: 2
Items: 
Size: 593920 Color: 19
Size: 405960 Color: 16

Bin 3052: 121 of cap free
Amount of items: 2
Items: 
Size: 599562 Color: 0
Size: 400318 Color: 7

Bin 3053: 121 of cap free
Amount of items: 2
Items: 
Size: 665168 Color: 1
Size: 334712 Color: 6

Bin 3054: 122 of cap free
Amount of items: 2
Items: 
Size: 554412 Color: 6
Size: 445467 Color: 7

Bin 3055: 122 of cap free
Amount of items: 2
Items: 
Size: 633322 Color: 7
Size: 366557 Color: 16

Bin 3056: 122 of cap free
Amount of items: 2
Items: 
Size: 654542 Color: 13
Size: 345337 Color: 6

Bin 3057: 122 of cap free
Amount of items: 2
Items: 
Size: 715959 Color: 1
Size: 283920 Color: 19

Bin 3058: 123 of cap free
Amount of items: 3
Items: 
Size: 716713 Color: 4
Size: 172534 Color: 7
Size: 110631 Color: 18

Bin 3059: 123 of cap free
Amount of items: 2
Items: 
Size: 677904 Color: 15
Size: 321974 Color: 19

Bin 3060: 123 of cap free
Amount of items: 2
Items: 
Size: 606566 Color: 17
Size: 393312 Color: 19

Bin 3061: 123 of cap free
Amount of items: 2
Items: 
Size: 711657 Color: 8
Size: 288221 Color: 1

Bin 3062: 123 of cap free
Amount of items: 2
Items: 
Size: 792505 Color: 16
Size: 207373 Color: 1

Bin 3063: 124 of cap free
Amount of items: 3
Items: 
Size: 677039 Color: 9
Size: 169917 Color: 8
Size: 152921 Color: 11

Bin 3064: 124 of cap free
Amount of items: 2
Items: 
Size: 714234 Color: 13
Size: 285643 Color: 1

Bin 3065: 124 of cap free
Amount of items: 2
Items: 
Size: 762325 Color: 18
Size: 237552 Color: 0

Bin 3066: 124 of cap free
Amount of items: 2
Items: 
Size: 532349 Color: 1
Size: 467528 Color: 16

Bin 3067: 124 of cap free
Amount of items: 2
Items: 
Size: 535707 Color: 10
Size: 464170 Color: 6

Bin 3068: 124 of cap free
Amount of items: 2
Items: 
Size: 550505 Color: 2
Size: 449372 Color: 17

Bin 3069: 124 of cap free
Amount of items: 2
Items: 
Size: 555450 Color: 16
Size: 444427 Color: 18

Bin 3070: 124 of cap free
Amount of items: 2
Items: 
Size: 698742 Color: 0
Size: 301135 Color: 15

Bin 3071: 124 of cap free
Amount of items: 2
Items: 
Size: 743131 Color: 3
Size: 256746 Color: 9

Bin 3072: 125 of cap free
Amount of items: 2
Items: 
Size: 538216 Color: 3
Size: 461660 Color: 4

Bin 3073: 125 of cap free
Amount of items: 2
Items: 
Size: 799176 Color: 14
Size: 200700 Color: 9

Bin 3074: 125 of cap free
Amount of items: 2
Items: 
Size: 667550 Color: 10
Size: 332326 Color: 14

Bin 3075: 125 of cap free
Amount of items: 2
Items: 
Size: 694491 Color: 13
Size: 305385 Color: 11

Bin 3076: 125 of cap free
Amount of items: 2
Items: 
Size: 775900 Color: 15
Size: 223976 Color: 4

Bin 3077: 125 of cap free
Amount of items: 2
Items: 
Size: 731444 Color: 15
Size: 268432 Color: 4

Bin 3078: 125 of cap free
Amount of items: 2
Items: 
Size: 564262 Color: 7
Size: 435614 Color: 19

Bin 3079: 125 of cap free
Amount of items: 2
Items: 
Size: 795164 Color: 17
Size: 204712 Color: 0

Bin 3080: 126 of cap free
Amount of items: 2
Items: 
Size: 748836 Color: 12
Size: 251039 Color: 7

Bin 3081: 126 of cap free
Amount of items: 2
Items: 
Size: 703548 Color: 10
Size: 296327 Color: 18

Bin 3082: 126 of cap free
Amount of items: 2
Items: 
Size: 665958 Color: 7
Size: 333917 Color: 18

Bin 3083: 126 of cap free
Amount of items: 2
Items: 
Size: 636524 Color: 6
Size: 363351 Color: 3

Bin 3084: 126 of cap free
Amount of items: 2
Items: 
Size: 516865 Color: 16
Size: 483010 Color: 0

Bin 3085: 126 of cap free
Amount of items: 2
Items: 
Size: 661463 Color: 8
Size: 338412 Color: 3

Bin 3086: 127 of cap free
Amount of items: 2
Items: 
Size: 541792 Color: 18
Size: 458082 Color: 9

Bin 3087: 127 of cap free
Amount of items: 2
Items: 
Size: 700987 Color: 10
Size: 298887 Color: 9

Bin 3088: 127 of cap free
Amount of items: 2
Items: 
Size: 576341 Color: 13
Size: 423533 Color: 17

Bin 3089: 127 of cap free
Amount of items: 2
Items: 
Size: 685917 Color: 9
Size: 313957 Color: 11

Bin 3090: 127 of cap free
Amount of items: 2
Items: 
Size: 545561 Color: 7
Size: 454313 Color: 1

Bin 3091: 127 of cap free
Amount of items: 2
Items: 
Size: 640908 Color: 10
Size: 358966 Color: 5

Bin 3092: 127 of cap free
Amount of items: 2
Items: 
Size: 737784 Color: 3
Size: 262090 Color: 5

Bin 3093: 128 of cap free
Amount of items: 2
Items: 
Size: 755544 Color: 0
Size: 244329 Color: 13

Bin 3094: 128 of cap free
Amount of items: 2
Items: 
Size: 575784 Color: 14
Size: 424089 Color: 4

Bin 3095: 128 of cap free
Amount of items: 2
Items: 
Size: 660504 Color: 9
Size: 339369 Color: 3

Bin 3096: 128 of cap free
Amount of items: 2
Items: 
Size: 662087 Color: 15
Size: 337786 Color: 4

Bin 3097: 128 of cap free
Amount of items: 2
Items: 
Size: 708147 Color: 2
Size: 291726 Color: 17

Bin 3098: 128 of cap free
Amount of items: 2
Items: 
Size: 750886 Color: 2
Size: 248987 Color: 9

Bin 3099: 129 of cap free
Amount of items: 2
Items: 
Size: 670135 Color: 13
Size: 329737 Color: 16

Bin 3100: 129 of cap free
Amount of items: 2
Items: 
Size: 678282 Color: 9
Size: 321590 Color: 14

Bin 3101: 129 of cap free
Amount of items: 2
Items: 
Size: 527491 Color: 0
Size: 472381 Color: 18

Bin 3102: 129 of cap free
Amount of items: 2
Items: 
Size: 547239 Color: 1
Size: 452633 Color: 7

Bin 3103: 129 of cap free
Amount of items: 2
Items: 
Size: 552510 Color: 8
Size: 447362 Color: 15

Bin 3104: 129 of cap free
Amount of items: 2
Items: 
Size: 623520 Color: 6
Size: 376352 Color: 18

Bin 3105: 129 of cap free
Amount of items: 2
Items: 
Size: 684691 Color: 19
Size: 315181 Color: 13

Bin 3106: 130 of cap free
Amount of items: 2
Items: 
Size: 732403 Color: 13
Size: 267468 Color: 8

Bin 3107: 130 of cap free
Amount of items: 2
Items: 
Size: 742336 Color: 11
Size: 257535 Color: 18

Bin 3108: 130 of cap free
Amount of items: 2
Items: 
Size: 513552 Color: 12
Size: 486319 Color: 13

Bin 3109: 131 of cap free
Amount of items: 3
Items: 
Size: 720537 Color: 11
Size: 153823 Color: 2
Size: 125510 Color: 2

Bin 3110: 131 of cap free
Amount of items: 2
Items: 
Size: 515376 Color: 12
Size: 484494 Color: 17

Bin 3111: 131 of cap free
Amount of items: 2
Items: 
Size: 547728 Color: 11
Size: 452142 Color: 13

Bin 3112: 131 of cap free
Amount of items: 2
Items: 
Size: 718280 Color: 3
Size: 281590 Color: 18

Bin 3113: 132 of cap free
Amount of items: 2
Items: 
Size: 651455 Color: 15
Size: 348414 Color: 11

Bin 3114: 132 of cap free
Amount of items: 2
Items: 
Size: 643299 Color: 2
Size: 356570 Color: 11

Bin 3115: 132 of cap free
Amount of items: 2
Items: 
Size: 502931 Color: 1
Size: 496938 Color: 16

Bin 3116: 132 of cap free
Amount of items: 2
Items: 
Size: 530309 Color: 18
Size: 469560 Color: 11

Bin 3117: 133 of cap free
Amount of items: 4
Items: 
Size: 462011 Color: 19
Size: 255890 Color: 13
Size: 172927 Color: 7
Size: 109040 Color: 13

Bin 3118: 133 of cap free
Amount of items: 2
Items: 
Size: 509107 Color: 4
Size: 490761 Color: 17

Bin 3119: 133 of cap free
Amount of items: 2
Items: 
Size: 656856 Color: 7
Size: 343012 Color: 0

Bin 3120: 133 of cap free
Amount of items: 2
Items: 
Size: 798399 Color: 4
Size: 201469 Color: 13

Bin 3121: 133 of cap free
Amount of items: 2
Items: 
Size: 574834 Color: 18
Size: 425034 Color: 14

Bin 3122: 133 of cap free
Amount of items: 2
Items: 
Size: 577709 Color: 17
Size: 422159 Color: 7

Bin 3123: 133 of cap free
Amount of items: 2
Items: 
Size: 627365 Color: 6
Size: 372503 Color: 14

Bin 3124: 133 of cap free
Amount of items: 2
Items: 
Size: 671633 Color: 16
Size: 328235 Color: 2

Bin 3125: 133 of cap free
Amount of items: 2
Items: 
Size: 673436 Color: 11
Size: 326432 Color: 6

Bin 3126: 133 of cap free
Amount of items: 2
Items: 
Size: 717513 Color: 7
Size: 282355 Color: 16

Bin 3127: 133 of cap free
Amount of items: 2
Items: 
Size: 723769 Color: 6
Size: 276099 Color: 1

Bin 3128: 134 of cap free
Amount of items: 2
Items: 
Size: 618052 Color: 15
Size: 381815 Color: 5

Bin 3129: 134 of cap free
Amount of items: 2
Items: 
Size: 772232 Color: 2
Size: 227635 Color: 12

Bin 3130: 134 of cap free
Amount of items: 2
Items: 
Size: 727909 Color: 13
Size: 271958 Color: 5

Bin 3131: 134 of cap free
Amount of items: 2
Items: 
Size: 520283 Color: 11
Size: 479584 Color: 0

Bin 3132: 134 of cap free
Amount of items: 2
Items: 
Size: 549048 Color: 3
Size: 450819 Color: 18

Bin 3133: 134 of cap free
Amount of items: 2
Items: 
Size: 572599 Color: 6
Size: 427268 Color: 17

Bin 3134: 134 of cap free
Amount of items: 2
Items: 
Size: 589030 Color: 15
Size: 410837 Color: 7

Bin 3135: 135 of cap free
Amount of items: 2
Items: 
Size: 791720 Color: 2
Size: 208146 Color: 7

Bin 3136: 135 of cap free
Amount of items: 2
Items: 
Size: 675314 Color: 8
Size: 324552 Color: 15

Bin 3137: 135 of cap free
Amount of items: 2
Items: 
Size: 508998 Color: 15
Size: 490868 Color: 19

Bin 3138: 135 of cap free
Amount of items: 2
Items: 
Size: 753665 Color: 0
Size: 246201 Color: 5

Bin 3139: 135 of cap free
Amount of items: 2
Items: 
Size: 527342 Color: 6
Size: 472524 Color: 11

Bin 3140: 135 of cap free
Amount of items: 2
Items: 
Size: 551629 Color: 4
Size: 448237 Color: 14

Bin 3141: 135 of cap free
Amount of items: 2
Items: 
Size: 583821 Color: 11
Size: 416045 Color: 17

Bin 3142: 135 of cap free
Amount of items: 2
Items: 
Size: 611641 Color: 0
Size: 388225 Color: 7

Bin 3143: 135 of cap free
Amount of items: 2
Items: 
Size: 622176 Color: 2
Size: 377690 Color: 5

Bin 3144: 135 of cap free
Amount of items: 2
Items: 
Size: 630656 Color: 14
Size: 369210 Color: 12

Bin 3145: 135 of cap free
Amount of items: 2
Items: 
Size: 644315 Color: 17
Size: 355551 Color: 9

Bin 3146: 135 of cap free
Amount of items: 2
Items: 
Size: 656648 Color: 6
Size: 343218 Color: 15

Bin 3147: 135 of cap free
Amount of items: 2
Items: 
Size: 669717 Color: 18
Size: 330149 Color: 17

Bin 3148: 136 of cap free
Amount of items: 2
Items: 
Size: 587248 Color: 15
Size: 412617 Color: 5

Bin 3149: 136 of cap free
Amount of items: 2
Items: 
Size: 586427 Color: 6
Size: 413438 Color: 17

Bin 3150: 136 of cap free
Amount of items: 2
Items: 
Size: 693909 Color: 5
Size: 305956 Color: 2

Bin 3151: 136 of cap free
Amount of items: 2
Items: 
Size: 725649 Color: 12
Size: 274216 Color: 10

Bin 3152: 137 of cap free
Amount of items: 2
Items: 
Size: 676896 Color: 14
Size: 322968 Color: 4

Bin 3153: 137 of cap free
Amount of items: 2
Items: 
Size: 511092 Color: 0
Size: 488772 Color: 16

Bin 3154: 137 of cap free
Amount of items: 2
Items: 
Size: 518616 Color: 19
Size: 481248 Color: 11

Bin 3155: 137 of cap free
Amount of items: 2
Items: 
Size: 573618 Color: 12
Size: 426246 Color: 7

Bin 3156: 137 of cap free
Amount of items: 2
Items: 
Size: 775135 Color: 1
Size: 224729 Color: 18

Bin 3157: 137 of cap free
Amount of items: 2
Items: 
Size: 788572 Color: 19
Size: 211292 Color: 8

Bin 3158: 138 of cap free
Amount of items: 3
Items: 
Size: 776470 Color: 13
Size: 118629 Color: 6
Size: 104764 Color: 3

Bin 3159: 138 of cap free
Amount of items: 2
Items: 
Size: 681111 Color: 17
Size: 318752 Color: 14

Bin 3160: 138 of cap free
Amount of items: 2
Items: 
Size: 724069 Color: 6
Size: 275794 Color: 15

Bin 3161: 138 of cap free
Amount of items: 2
Items: 
Size: 528125 Color: 11
Size: 471738 Color: 3

Bin 3162: 138 of cap free
Amount of items: 2
Items: 
Size: 581157 Color: 1
Size: 418706 Color: 14

Bin 3163: 138 of cap free
Amount of items: 2
Items: 
Size: 599827 Color: 3
Size: 400036 Color: 5

Bin 3164: 139 of cap free
Amount of items: 2
Items: 
Size: 667154 Color: 1
Size: 332708 Color: 11

Bin 3165: 139 of cap free
Amount of items: 2
Items: 
Size: 641089 Color: 7
Size: 358773 Color: 13

Bin 3166: 139 of cap free
Amount of items: 2
Items: 
Size: 521397 Color: 5
Size: 478465 Color: 18

Bin 3167: 139 of cap free
Amount of items: 2
Items: 
Size: 523877 Color: 2
Size: 475985 Color: 15

Bin 3168: 139 of cap free
Amount of items: 2
Items: 
Size: 738791 Color: 3
Size: 261071 Color: 5

Bin 3169: 140 of cap free
Amount of items: 2
Items: 
Size: 776156 Color: 16
Size: 223705 Color: 14

Bin 3170: 140 of cap free
Amount of items: 2
Items: 
Size: 726022 Color: 5
Size: 273839 Color: 3

Bin 3171: 140 of cap free
Amount of items: 2
Items: 
Size: 784274 Color: 18
Size: 215587 Color: 19

Bin 3172: 140 of cap free
Amount of items: 2
Items: 
Size: 726388 Color: 19
Size: 273473 Color: 16

Bin 3173: 140 of cap free
Amount of items: 2
Items: 
Size: 546871 Color: 12
Size: 452990 Color: 13

Bin 3174: 140 of cap free
Amount of items: 2
Items: 
Size: 598599 Color: 7
Size: 401262 Color: 17

Bin 3175: 140 of cap free
Amount of items: 2
Items: 
Size: 752500 Color: 16
Size: 247361 Color: 0

Bin 3176: 141 of cap free
Amount of items: 3
Items: 
Size: 727654 Color: 16
Size: 146355 Color: 0
Size: 125851 Color: 11

Bin 3177: 141 of cap free
Amount of items: 2
Items: 
Size: 604830 Color: 11
Size: 395030 Color: 14

Bin 3178: 141 of cap free
Amount of items: 2
Items: 
Size: 555945 Color: 18
Size: 443915 Color: 6

Bin 3179: 141 of cap free
Amount of items: 2
Items: 
Size: 776779 Color: 4
Size: 223081 Color: 8

Bin 3180: 141 of cap free
Amount of items: 2
Items: 
Size: 535948 Color: 8
Size: 463912 Color: 18

Bin 3181: 141 of cap free
Amount of items: 2
Items: 
Size: 583084 Color: 13
Size: 416776 Color: 3

Bin 3182: 141 of cap free
Amount of items: 2
Items: 
Size: 650971 Color: 9
Size: 348889 Color: 3

Bin 3183: 141 of cap free
Amount of items: 2
Items: 
Size: 793595 Color: 4
Size: 206265 Color: 3

Bin 3184: 142 of cap free
Amount of items: 2
Items: 
Size: 730368 Color: 1
Size: 269491 Color: 15

Bin 3185: 142 of cap free
Amount of items: 2
Items: 
Size: 754507 Color: 6
Size: 245352 Color: 7

Bin 3186: 142 of cap free
Amount of items: 2
Items: 
Size: 646243 Color: 10
Size: 353616 Color: 11

Bin 3187: 142 of cap free
Amount of items: 2
Items: 
Size: 591310 Color: 11
Size: 408549 Color: 0

Bin 3188: 142 of cap free
Amount of items: 2
Items: 
Size: 667006 Color: 19
Size: 332853 Color: 1

Bin 3189: 142 of cap free
Amount of items: 2
Items: 
Size: 694483 Color: 7
Size: 305376 Color: 3

Bin 3190: 143 of cap free
Amount of items: 2
Items: 
Size: 505649 Color: 18
Size: 494209 Color: 9

Bin 3191: 143 of cap free
Amount of items: 2
Items: 
Size: 506623 Color: 12
Size: 493235 Color: 6

Bin 3192: 143 of cap free
Amount of items: 2
Items: 
Size: 718118 Color: 15
Size: 281740 Color: 4

Bin 3193: 143 of cap free
Amount of items: 2
Items: 
Size: 777528 Color: 14
Size: 222330 Color: 15

Bin 3194: 143 of cap free
Amount of items: 2
Items: 
Size: 625361 Color: 19
Size: 374497 Color: 12

Bin 3195: 143 of cap free
Amount of items: 2
Items: 
Size: 565940 Color: 13
Size: 433918 Color: 10

Bin 3196: 143 of cap free
Amount of items: 2
Items: 
Size: 753321 Color: 2
Size: 246537 Color: 16

Bin 3197: 143 of cap free
Amount of items: 2
Items: 
Size: 792708 Color: 12
Size: 207150 Color: 13

Bin 3198: 144 of cap free
Amount of items: 2
Items: 
Size: 732865 Color: 5
Size: 266992 Color: 15

Bin 3199: 144 of cap free
Amount of items: 2
Items: 
Size: 741306 Color: 4
Size: 258551 Color: 9

Bin 3200: 144 of cap free
Amount of items: 2
Items: 
Size: 680365 Color: 19
Size: 319492 Color: 6

Bin 3201: 144 of cap free
Amount of items: 2
Items: 
Size: 689962 Color: 18
Size: 309895 Color: 9

Bin 3202: 144 of cap free
Amount of items: 2
Items: 
Size: 532497 Color: 13
Size: 467360 Color: 5

Bin 3203: 145 of cap free
Amount of items: 3
Items: 
Size: 686716 Color: 3
Size: 182933 Color: 11
Size: 130207 Color: 18

Bin 3204: 145 of cap free
Amount of items: 3
Items: 
Size: 684515 Color: 16
Size: 157841 Color: 4
Size: 157500 Color: 8

Bin 3205: 145 of cap free
Amount of items: 2
Items: 
Size: 604615 Color: 1
Size: 395241 Color: 13

Bin 3206: 145 of cap free
Amount of items: 2
Items: 
Size: 668916 Color: 19
Size: 330940 Color: 17

Bin 3207: 145 of cap free
Amount of items: 2
Items: 
Size: 785879 Color: 9
Size: 213977 Color: 7

Bin 3208: 145 of cap free
Amount of items: 3
Items: 
Size: 507448 Color: 18
Size: 250696 Color: 4
Size: 241712 Color: 16

Bin 3209: 145 of cap free
Amount of items: 2
Items: 
Size: 793121 Color: 10
Size: 206735 Color: 2

Bin 3210: 145 of cap free
Amount of items: 2
Items: 
Size: 657564 Color: 7
Size: 342292 Color: 12

Bin 3211: 145 of cap free
Amount of items: 2
Items: 
Size: 510672 Color: 10
Size: 489184 Color: 6

Bin 3212: 145 of cap free
Amount of items: 2
Items: 
Size: 534336 Color: 0
Size: 465520 Color: 3

Bin 3213: 145 of cap free
Amount of items: 2
Items: 
Size: 539370 Color: 5
Size: 460486 Color: 19

Bin 3214: 145 of cap free
Amount of items: 2
Items: 
Size: 540593 Color: 0
Size: 459263 Color: 1

Bin 3215: 146 of cap free
Amount of items: 2
Items: 
Size: 709562 Color: 18
Size: 290293 Color: 4

Bin 3216: 146 of cap free
Amount of items: 2
Items: 
Size: 554661 Color: 19
Size: 445194 Color: 12

Bin 3217: 146 of cap free
Amount of items: 2
Items: 
Size: 656643 Color: 13
Size: 343212 Color: 11

Bin 3218: 146 of cap free
Amount of items: 2
Items: 
Size: 571537 Color: 13
Size: 428318 Color: 2

Bin 3219: 146 of cap free
Amount of items: 2
Items: 
Size: 631940 Color: 8
Size: 367915 Color: 6

Bin 3220: 147 of cap free
Amount of items: 2
Items: 
Size: 698222 Color: 8
Size: 301632 Color: 18

Bin 3221: 147 of cap free
Amount of items: 2
Items: 
Size: 615598 Color: 18
Size: 384256 Color: 6

Bin 3222: 147 of cap free
Amount of items: 2
Items: 
Size: 513377 Color: 3
Size: 486477 Color: 19

Bin 3223: 147 of cap free
Amount of items: 2
Items: 
Size: 571742 Color: 9
Size: 428112 Color: 8

Bin 3224: 147 of cap free
Amount of items: 2
Items: 
Size: 733070 Color: 0
Size: 266784 Color: 5

Bin 3225: 148 of cap free
Amount of items: 2
Items: 
Size: 755081 Color: 13
Size: 244772 Color: 10

Bin 3226: 149 of cap free
Amount of items: 2
Items: 
Size: 731588 Color: 6
Size: 268264 Color: 13

Bin 3227: 149 of cap free
Amount of items: 2
Items: 
Size: 572761 Color: 1
Size: 427091 Color: 10

Bin 3228: 150 of cap free
Amount of items: 2
Items: 
Size: 590080 Color: 8
Size: 409771 Color: 18

Bin 3229: 150 of cap free
Amount of items: 2
Items: 
Size: 668481 Color: 19
Size: 331370 Color: 13

Bin 3230: 150 of cap free
Amount of items: 2
Items: 
Size: 777438 Color: 15
Size: 222413 Color: 0

Bin 3231: 150 of cap free
Amount of items: 2
Items: 
Size: 794656 Color: 14
Size: 205195 Color: 4

Bin 3232: 151 of cap free
Amount of items: 2
Items: 
Size: 618544 Color: 9
Size: 381306 Color: 14

Bin 3233: 151 of cap free
Amount of items: 2
Items: 
Size: 786881 Color: 4
Size: 212969 Color: 7

Bin 3234: 151 of cap free
Amount of items: 2
Items: 
Size: 514456 Color: 16
Size: 485394 Color: 14

Bin 3235: 151 of cap free
Amount of items: 2
Items: 
Size: 577282 Color: 1
Size: 422568 Color: 16

Bin 3236: 151 of cap free
Amount of items: 2
Items: 
Size: 633480 Color: 12
Size: 366370 Color: 8

Bin 3237: 151 of cap free
Amount of items: 2
Items: 
Size: 643123 Color: 15
Size: 356727 Color: 8

Bin 3238: 151 of cap free
Amount of items: 2
Items: 
Size: 701707 Color: 6
Size: 298143 Color: 4

Bin 3239: 152 of cap free
Amount of items: 3
Items: 
Size: 774980 Color: 15
Size: 119291 Color: 18
Size: 105578 Color: 14

Bin 3240: 152 of cap free
Amount of items: 2
Items: 
Size: 544715 Color: 4
Size: 455134 Color: 7

Bin 3241: 152 of cap free
Amount of items: 2
Items: 
Size: 665654 Color: 7
Size: 334195 Color: 4

Bin 3242: 152 of cap free
Amount of items: 2
Items: 
Size: 558048 Color: 11
Size: 441801 Color: 12

Bin 3243: 152 of cap free
Amount of items: 2
Items: 
Size: 673594 Color: 10
Size: 326255 Color: 15

Bin 3244: 153 of cap free
Amount of items: 2
Items: 
Size: 548181 Color: 13
Size: 451667 Color: 12

Bin 3245: 153 of cap free
Amount of items: 2
Items: 
Size: 623510 Color: 13
Size: 376338 Color: 18

Bin 3246: 154 of cap free
Amount of items: 2
Items: 
Size: 667531 Color: 1
Size: 332316 Color: 3

Bin 3247: 154 of cap free
Amount of items: 2
Items: 
Size: 790597 Color: 15
Size: 209250 Color: 11

Bin 3248: 154 of cap free
Amount of items: 2
Items: 
Size: 754176 Color: 19
Size: 245671 Color: 0

Bin 3249: 154 of cap free
Amount of items: 2
Items: 
Size: 569841 Color: 11
Size: 430006 Color: 10

Bin 3250: 154 of cap free
Amount of items: 2
Items: 
Size: 638344 Color: 0
Size: 361503 Color: 14

Bin 3251: 154 of cap free
Amount of items: 2
Items: 
Size: 711335 Color: 5
Size: 288512 Color: 16

Bin 3252: 154 of cap free
Amount of items: 2
Items: 
Size: 626907 Color: 1
Size: 372940 Color: 15

Bin 3253: 154 of cap free
Amount of items: 2
Items: 
Size: 795961 Color: 18
Size: 203886 Color: 17

Bin 3254: 155 of cap free
Amount of items: 2
Items: 
Size: 501796 Color: 9
Size: 498050 Color: 12

Bin 3255: 155 of cap free
Amount of items: 2
Items: 
Size: 700819 Color: 16
Size: 299027 Color: 3

Bin 3256: 155 of cap free
Amount of items: 2
Items: 
Size: 638052 Color: 2
Size: 361794 Color: 0

Bin 3257: 155 of cap free
Amount of items: 2
Items: 
Size: 576591 Color: 18
Size: 423255 Color: 3

Bin 3258: 155 of cap free
Amount of items: 2
Items: 
Size: 682735 Color: 17
Size: 317111 Color: 9

Bin 3259: 156 of cap free
Amount of items: 3
Items: 
Size: 476929 Color: 18
Size: 321865 Color: 15
Size: 201051 Color: 11

Bin 3260: 156 of cap free
Amount of items: 2
Items: 
Size: 741092 Color: 14
Size: 258753 Color: 17

Bin 3261: 156 of cap free
Amount of items: 2
Items: 
Size: 785998 Color: 4
Size: 213847 Color: 1

Bin 3262: 156 of cap free
Amount of items: 2
Items: 
Size: 565936 Color: 3
Size: 433909 Color: 12

Bin 3263: 156 of cap free
Amount of items: 2
Items: 
Size: 674944 Color: 6
Size: 324901 Color: 14

Bin 3264: 157 of cap free
Amount of items: 2
Items: 
Size: 559097 Color: 0
Size: 440747 Color: 2

Bin 3265: 157 of cap free
Amount of items: 2
Items: 
Size: 798692 Color: 0
Size: 201152 Color: 8

Bin 3266: 157 of cap free
Amount of items: 2
Items: 
Size: 620476 Color: 5
Size: 379368 Color: 13

Bin 3267: 158 of cap free
Amount of items: 2
Items: 
Size: 501816 Color: 15
Size: 498027 Color: 0

Bin 3268: 158 of cap free
Amount of items: 3
Items: 
Size: 501304 Color: 8
Size: 250850 Color: 19
Size: 247689 Color: 7

Bin 3269: 158 of cap free
Amount of items: 2
Items: 
Size: 690309 Color: 10
Size: 309534 Color: 7

Bin 3270: 158 of cap free
Amount of items: 2
Items: 
Size: 781201 Color: 13
Size: 218642 Color: 11

Bin 3271: 158 of cap free
Amount of items: 2
Items: 
Size: 657106 Color: 15
Size: 342737 Color: 1

Bin 3272: 158 of cap free
Amount of items: 2
Items: 
Size: 761761 Color: 5
Size: 238082 Color: 10

Bin 3273: 158 of cap free
Amount of items: 2
Items: 
Size: 503597 Color: 8
Size: 496246 Color: 10

Bin 3274: 158 of cap free
Amount of items: 2
Items: 
Size: 575556 Color: 19
Size: 424287 Color: 10

Bin 3275: 158 of cap free
Amount of items: 2
Items: 
Size: 579186 Color: 9
Size: 420657 Color: 14

Bin 3276: 158 of cap free
Amount of items: 2
Items: 
Size: 638930 Color: 8
Size: 360913 Color: 10

Bin 3277: 159 of cap free
Amount of items: 2
Items: 
Size: 749566 Color: 1
Size: 250276 Color: 18

Bin 3278: 159 of cap free
Amount of items: 2
Items: 
Size: 604018 Color: 1
Size: 395824 Color: 14

Bin 3279: 159 of cap free
Amount of items: 2
Items: 
Size: 602723 Color: 14
Size: 397119 Color: 16

Bin 3280: 159 of cap free
Amount of items: 2
Items: 
Size: 533160 Color: 18
Size: 466682 Color: 8

Bin 3281: 159 of cap free
Amount of items: 2
Items: 
Size: 655864 Color: 19
Size: 343978 Color: 0

Bin 3282: 159 of cap free
Amount of items: 2
Items: 
Size: 797653 Color: 16
Size: 202189 Color: 19

Bin 3283: 160 of cap free
Amount of items: 2
Items: 
Size: 682784 Color: 9
Size: 317057 Color: 17

Bin 3284: 160 of cap free
Amount of items: 2
Items: 
Size: 718091 Color: 0
Size: 281750 Color: 18

Bin 3285: 160 of cap free
Amount of items: 2
Items: 
Size: 724224 Color: 10
Size: 275617 Color: 1

Bin 3286: 160 of cap free
Amount of items: 2
Items: 
Size: 647835 Color: 15
Size: 352006 Color: 16

Bin 3287: 160 of cap free
Amount of items: 2
Items: 
Size: 748100 Color: 15
Size: 251741 Color: 16

Bin 3288: 161 of cap free
Amount of items: 2
Items: 
Size: 661920 Color: 7
Size: 337920 Color: 11

Bin 3289: 161 of cap free
Amount of items: 2
Items: 
Size: 580439 Color: 11
Size: 419401 Color: 4

Bin 3290: 162 of cap free
Amount of items: 2
Items: 
Size: 620106 Color: 14
Size: 379733 Color: 6

Bin 3291: 162 of cap free
Amount of items: 2
Items: 
Size: 565412 Color: 2
Size: 434427 Color: 18

Bin 3292: 162 of cap free
Amount of items: 2
Items: 
Size: 608292 Color: 11
Size: 391547 Color: 6

Bin 3293: 162 of cap free
Amount of items: 2
Items: 
Size: 514161 Color: 5
Size: 485678 Color: 1

Bin 3294: 162 of cap free
Amount of items: 2
Items: 
Size: 540313 Color: 5
Size: 459526 Color: 10

Bin 3295: 163 of cap free
Amount of items: 2
Items: 
Size: 553626 Color: 7
Size: 446212 Color: 18

Bin 3296: 163 of cap free
Amount of items: 2
Items: 
Size: 559227 Color: 2
Size: 440611 Color: 16

Bin 3297: 165 of cap free
Amount of items: 2
Items: 
Size: 707029 Color: 16
Size: 292807 Color: 15

Bin 3298: 165 of cap free
Amount of items: 2
Items: 
Size: 521868 Color: 5
Size: 477968 Color: 1

Bin 3299: 165 of cap free
Amount of items: 2
Items: 
Size: 587914 Color: 13
Size: 411922 Color: 0

Bin 3300: 165 of cap free
Amount of items: 2
Items: 
Size: 609841 Color: 9
Size: 389995 Color: 6

Bin 3301: 166 of cap free
Amount of items: 2
Items: 
Size: 737583 Color: 2
Size: 262252 Color: 8

Bin 3302: 166 of cap free
Amount of items: 2
Items: 
Size: 584869 Color: 13
Size: 414966 Color: 1

Bin 3303: 166 of cap free
Amount of items: 2
Items: 
Size: 564625 Color: 13
Size: 435210 Color: 6

Bin 3304: 166 of cap free
Amount of items: 2
Items: 
Size: 592298 Color: 13
Size: 407537 Color: 5

Bin 3305: 167 of cap free
Amount of items: 3
Items: 
Size: 741078 Color: 11
Size: 129418 Color: 16
Size: 129338 Color: 10

Bin 3306: 167 of cap free
Amount of items: 2
Items: 
Size: 776925 Color: 4
Size: 222909 Color: 9

Bin 3307: 167 of cap free
Amount of items: 2
Items: 
Size: 698384 Color: 1
Size: 301450 Color: 11

Bin 3308: 167 of cap free
Amount of items: 2
Items: 
Size: 516522 Color: 15
Size: 483312 Color: 4

Bin 3309: 167 of cap free
Amount of items: 2
Items: 
Size: 690567 Color: 14
Size: 309267 Color: 2

Bin 3310: 168 of cap free
Amount of items: 2
Items: 
Size: 739957 Color: 8
Size: 259876 Color: 12

Bin 3311: 168 of cap free
Amount of items: 2
Items: 
Size: 770121 Color: 13
Size: 229712 Color: 8

Bin 3312: 168 of cap free
Amount of items: 2
Items: 
Size: 584657 Color: 2
Size: 415176 Color: 5

Bin 3313: 168 of cap free
Amount of items: 2
Items: 
Size: 550876 Color: 9
Size: 448957 Color: 2

Bin 3314: 168 of cap free
Amount of items: 2
Items: 
Size: 675463 Color: 0
Size: 324370 Color: 8

Bin 3315: 168 of cap free
Amount of items: 2
Items: 
Size: 703514 Color: 12
Size: 296319 Color: 13

Bin 3316: 169 of cap free
Amount of items: 2
Items: 
Size: 782450 Color: 9
Size: 217382 Color: 6

Bin 3317: 169 of cap free
Amount of items: 3
Items: 
Size: 644123 Color: 11
Size: 178360 Color: 4
Size: 177349 Color: 9

Bin 3318: 169 of cap free
Amount of items: 2
Items: 
Size: 633744 Color: 10
Size: 366088 Color: 5

Bin 3319: 169 of cap free
Amount of items: 2
Items: 
Size: 589284 Color: 19
Size: 410548 Color: 15

Bin 3320: 170 of cap free
Amount of items: 2
Items: 
Size: 712905 Color: 7
Size: 286926 Color: 6

Bin 3321: 170 of cap free
Amount of items: 2
Items: 
Size: 512018 Color: 6
Size: 487813 Color: 5

Bin 3322: 170 of cap free
Amount of items: 2
Items: 
Size: 547235 Color: 5
Size: 452596 Color: 19

Bin 3323: 170 of cap free
Amount of items: 2
Items: 
Size: 549042 Color: 5
Size: 450789 Color: 7

Bin 3324: 170 of cap free
Amount of items: 2
Items: 
Size: 623062 Color: 5
Size: 376769 Color: 15

Bin 3325: 171 of cap free
Amount of items: 3
Items: 
Size: 721129 Color: 11
Size: 148966 Color: 1
Size: 129735 Color: 16

Bin 3326: 171 of cap free
Amount of items: 2
Items: 
Size: 633743 Color: 0
Size: 366087 Color: 6

Bin 3327: 171 of cap free
Amount of items: 2
Items: 
Size: 738269 Color: 3
Size: 261561 Color: 11

Bin 3328: 172 of cap free
Amount of items: 2
Items: 
Size: 735603 Color: 10
Size: 264226 Color: 16

Bin 3329: 172 of cap free
Amount of items: 2
Items: 
Size: 541627 Color: 6
Size: 458202 Color: 1

Bin 3330: 172 of cap free
Amount of items: 2
Items: 
Size: 704572 Color: 5
Size: 295257 Color: 4

Bin 3331: 172 of cap free
Amount of items: 2
Items: 
Size: 585768 Color: 6
Size: 414061 Color: 0

Bin 3332: 172 of cap free
Amount of items: 2
Items: 
Size: 795160 Color: 9
Size: 204669 Color: 7

Bin 3333: 173 of cap free
Amount of items: 3
Items: 
Size: 738135 Color: 19
Size: 137028 Color: 2
Size: 124665 Color: 15

Bin 3334: 173 of cap free
Amount of items: 2
Items: 
Size: 686145 Color: 12
Size: 313683 Color: 14

Bin 3335: 174 of cap free
Amount of items: 2
Items: 
Size: 688798 Color: 12
Size: 311029 Color: 6

Bin 3336: 174 of cap free
Amount of items: 2
Items: 
Size: 790217 Color: 13
Size: 209610 Color: 19

Bin 3337: 175 of cap free
Amount of items: 2
Items: 
Size: 719772 Color: 11
Size: 280054 Color: 6

Bin 3338: 175 of cap free
Amount of items: 2
Items: 
Size: 763771 Color: 14
Size: 236055 Color: 2

Bin 3339: 175 of cap free
Amount of items: 2
Items: 
Size: 540039 Color: 10
Size: 459787 Color: 7

Bin 3340: 175 of cap free
Amount of items: 2
Items: 
Size: 590342 Color: 5
Size: 409484 Color: 2

Bin 3341: 175 of cap free
Amount of items: 2
Items: 
Size: 722306 Color: 12
Size: 277520 Color: 14

Bin 3342: 176 of cap free
Amount of items: 2
Items: 
Size: 511457 Color: 18
Size: 488368 Color: 10

Bin 3343: 176 of cap free
Amount of items: 2
Items: 
Size: 523247 Color: 4
Size: 476578 Color: 14

Bin 3344: 176 of cap free
Amount of items: 2
Items: 
Size: 534320 Color: 6
Size: 465505 Color: 10

Bin 3345: 176 of cap free
Amount of items: 3
Items: 
Size: 558770 Color: 1
Size: 220541 Color: 16
Size: 220514 Color: 7

Bin 3346: 177 of cap free
Amount of items: 2
Items: 
Size: 505996 Color: 6
Size: 493828 Color: 17

Bin 3347: 177 of cap free
Amount of items: 2
Items: 
Size: 508108 Color: 2
Size: 491716 Color: 9

Bin 3348: 177 of cap free
Amount of items: 2
Items: 
Size: 636435 Color: 2
Size: 363389 Color: 17

Bin 3349: 178 of cap free
Amount of items: 2
Items: 
Size: 506591 Color: 17
Size: 493232 Color: 11

Bin 3350: 178 of cap free
Amount of items: 2
Items: 
Size: 614760 Color: 9
Size: 385063 Color: 12

Bin 3351: 178 of cap free
Amount of items: 2
Items: 
Size: 637131 Color: 10
Size: 362692 Color: 15

Bin 3352: 179 of cap free
Amount of items: 3
Items: 
Size: 747147 Color: 5
Size: 133719 Color: 3
Size: 118956 Color: 14

Bin 3353: 179 of cap free
Amount of items: 2
Items: 
Size: 683893 Color: 18
Size: 315929 Color: 2

Bin 3354: 179 of cap free
Amount of items: 2
Items: 
Size: 756436 Color: 0
Size: 243386 Color: 7

Bin 3355: 179 of cap free
Amount of items: 2
Items: 
Size: 794973 Color: 15
Size: 204849 Color: 6

Bin 3356: 180 of cap free
Amount of items: 2
Items: 
Size: 755499 Color: 15
Size: 244322 Color: 11

Bin 3357: 180 of cap free
Amount of items: 2
Items: 
Size: 591694 Color: 6
Size: 408127 Color: 19

Bin 3358: 180 of cap free
Amount of items: 2
Items: 
Size: 632883 Color: 6
Size: 366938 Color: 11

Bin 3359: 181 of cap free
Amount of items: 2
Items: 
Size: 565690 Color: 6
Size: 434130 Color: 12

Bin 3360: 181 of cap free
Amount of items: 2
Items: 
Size: 750585 Color: 18
Size: 249235 Color: 1

Bin 3361: 181 of cap free
Amount of items: 2
Items: 
Size: 503927 Color: 9
Size: 495893 Color: 13

Bin 3362: 181 of cap free
Amount of items: 2
Items: 
Size: 674159 Color: 12
Size: 325661 Color: 19

Bin 3363: 181 of cap free
Amount of items: 2
Items: 
Size: 762534 Color: 12
Size: 237286 Color: 3

Bin 3364: 182 of cap free
Amount of items: 2
Items: 
Size: 636784 Color: 7
Size: 363035 Color: 18

Bin 3365: 182 of cap free
Amount of items: 2
Items: 
Size: 736743 Color: 6
Size: 263076 Color: 4

Bin 3366: 182 of cap free
Amount of items: 2
Items: 
Size: 605236 Color: 0
Size: 394583 Color: 8

Bin 3367: 182 of cap free
Amount of items: 2
Items: 
Size: 542710 Color: 14
Size: 457109 Color: 6

Bin 3368: 182 of cap free
Amount of items: 2
Items: 
Size: 610902 Color: 13
Size: 388917 Color: 9

Bin 3369: 182 of cap free
Amount of items: 2
Items: 
Size: 702237 Color: 6
Size: 297582 Color: 3

Bin 3370: 183 of cap free
Amount of items: 2
Items: 
Size: 521376 Color: 11
Size: 478442 Color: 8

Bin 3371: 183 of cap free
Amount of items: 2
Items: 
Size: 558762 Color: 10
Size: 441056 Color: 5

Bin 3372: 183 of cap free
Amount of items: 2
Items: 
Size: 564159 Color: 14
Size: 435659 Color: 7

Bin 3373: 184 of cap free
Amount of items: 2
Items: 
Size: 684270 Color: 3
Size: 315547 Color: 15

Bin 3374: 185 of cap free
Amount of items: 2
Items: 
Size: 790836 Color: 10
Size: 208980 Color: 8

Bin 3375: 185 of cap free
Amount of items: 2
Items: 
Size: 558542 Color: 17
Size: 441274 Color: 2

Bin 3376: 185 of cap free
Amount of items: 2
Items: 
Size: 601401 Color: 18
Size: 398415 Color: 17

Bin 3377: 186 of cap free
Amount of items: 2
Items: 
Size: 584647 Color: 7
Size: 415168 Color: 17

Bin 3378: 187 of cap free
Amount of items: 2
Items: 
Size: 647518 Color: 12
Size: 352296 Color: 4

Bin 3379: 187 of cap free
Amount of items: 2
Items: 
Size: 639325 Color: 6
Size: 360489 Color: 16

Bin 3380: 188 of cap free
Amount of items: 2
Items: 
Size: 507583 Color: 12
Size: 492230 Color: 9

Bin 3381: 188 of cap free
Amount of items: 2
Items: 
Size: 532769 Color: 17
Size: 467044 Color: 11

Bin 3382: 188 of cap free
Amount of items: 2
Items: 
Size: 559825 Color: 10
Size: 439988 Color: 4

Bin 3383: 188 of cap free
Amount of items: 2
Items: 
Size: 626216 Color: 5
Size: 373597 Color: 11

Bin 3384: 189 of cap free
Amount of items: 2
Items: 
Size: 755088 Color: 10
Size: 244724 Color: 6

Bin 3385: 189 of cap free
Amount of items: 2
Items: 
Size: 611859 Color: 18
Size: 387953 Color: 12

Bin 3386: 189 of cap free
Amount of items: 2
Items: 
Size: 552660 Color: 2
Size: 447152 Color: 17

Bin 3387: 189 of cap free
Amount of items: 2
Items: 
Size: 553860 Color: 7
Size: 445952 Color: 5

Bin 3388: 189 of cap free
Amount of items: 2
Items: 
Size: 571709 Color: 10
Size: 428103 Color: 11

Bin 3389: 189 of cap free
Amount of items: 2
Items: 
Size: 576023 Color: 0
Size: 423789 Color: 13

Bin 3390: 189 of cap free
Amount of items: 2
Items: 
Size: 659347 Color: 4
Size: 340465 Color: 2

Bin 3391: 190 of cap free
Amount of items: 2
Items: 
Size: 563625 Color: 2
Size: 436186 Color: 7

Bin 3392: 190 of cap free
Amount of items: 2
Items: 
Size: 596407 Color: 15
Size: 403404 Color: 0

Bin 3393: 191 of cap free
Amount of items: 2
Items: 
Size: 580066 Color: 3
Size: 419744 Color: 15

Bin 3394: 191 of cap free
Amount of items: 2
Items: 
Size: 723732 Color: 16
Size: 276078 Color: 17

Bin 3395: 192 of cap free
Amount of items: 2
Items: 
Size: 630637 Color: 19
Size: 369172 Color: 9

Bin 3396: 193 of cap free
Amount of items: 2
Items: 
Size: 773976 Color: 8
Size: 225832 Color: 14

Bin 3397: 193 of cap free
Amount of items: 2
Items: 
Size: 511999 Color: 19
Size: 487809 Color: 13

Bin 3398: 193 of cap free
Amount of items: 2
Items: 
Size: 708091 Color: 6
Size: 291717 Color: 16

Bin 3399: 194 of cap free
Amount of items: 2
Items: 
Size: 672682 Color: 15
Size: 327125 Color: 17

Bin 3400: 194 of cap free
Amount of items: 2
Items: 
Size: 678496 Color: 6
Size: 321311 Color: 0

Bin 3401: 194 of cap free
Amount of items: 2
Items: 
Size: 695546 Color: 17
Size: 304261 Color: 2

Bin 3402: 195 of cap free
Amount of items: 2
Items: 
Size: 537148 Color: 10
Size: 462658 Color: 9

Bin 3403: 195 of cap free
Amount of items: 2
Items: 
Size: 613428 Color: 12
Size: 386378 Color: 6

Bin 3404: 195 of cap free
Amount of items: 2
Items: 
Size: 602986 Color: 18
Size: 396820 Color: 2

Bin 3405: 196 of cap free
Amount of items: 2
Items: 
Size: 574210 Color: 18
Size: 425595 Color: 12

Bin 3406: 196 of cap free
Amount of items: 2
Items: 
Size: 537691 Color: 15
Size: 462114 Color: 0

Bin 3407: 196 of cap free
Amount of items: 2
Items: 
Size: 567663 Color: 12
Size: 432142 Color: 1

Bin 3408: 197 of cap free
Amount of items: 2
Items: 
Size: 536453 Color: 8
Size: 463351 Color: 3

Bin 3409: 197 of cap free
Amount of items: 2
Items: 
Size: 738237 Color: 4
Size: 261567 Color: 13

Bin 3410: 198 of cap free
Amount of items: 2
Items: 
Size: 734042 Color: 13
Size: 265761 Color: 10

Bin 3411: 199 of cap free
Amount of items: 2
Items: 
Size: 609820 Color: 9
Size: 389982 Color: 8

Bin 3412: 199 of cap free
Amount of items: 2
Items: 
Size: 721891 Color: 2
Size: 277911 Color: 4

Bin 3413: 201 of cap free
Amount of items: 2
Items: 
Size: 689373 Color: 3
Size: 310427 Color: 18

Bin 3414: 201 of cap free
Amount of items: 2
Items: 
Size: 581934 Color: 13
Size: 417866 Color: 14

Bin 3415: 201 of cap free
Amount of items: 2
Items: 
Size: 683422 Color: 15
Size: 316378 Color: 12

Bin 3416: 201 of cap free
Amount of items: 2
Items: 
Size: 507074 Color: 7
Size: 492726 Color: 15

Bin 3417: 201 of cap free
Amount of items: 2
Items: 
Size: 715545 Color: 18
Size: 284255 Color: 7

Bin 3418: 202 of cap free
Amount of items: 2
Items: 
Size: 725610 Color: 7
Size: 274189 Color: 1

Bin 3419: 202 of cap free
Amount of items: 2
Items: 
Size: 765566 Color: 18
Size: 234233 Color: 4

Bin 3420: 202 of cap free
Amount of items: 2
Items: 
Size: 503573 Color: 19
Size: 496226 Color: 5

Bin 3421: 202 of cap free
Amount of items: 2
Items: 
Size: 515755 Color: 2
Size: 484044 Color: 5

Bin 3422: 202 of cap free
Amount of items: 2
Items: 
Size: 621828 Color: 13
Size: 377971 Color: 10

Bin 3423: 203 of cap free
Amount of items: 3
Items: 
Size: 624995 Color: 13
Size: 187849 Color: 8
Size: 186954 Color: 11

Bin 3424: 203 of cap free
Amount of items: 3
Items: 
Size: 706692 Color: 11
Size: 146940 Color: 19
Size: 146166 Color: 13

Bin 3425: 203 of cap free
Amount of items: 2
Items: 
Size: 593846 Color: 3
Size: 405952 Color: 14

Bin 3426: 203 of cap free
Amount of items: 2
Items: 
Size: 610605 Color: 17
Size: 389193 Color: 3

Bin 3427: 204 of cap free
Amount of items: 2
Items: 
Size: 524329 Color: 0
Size: 475468 Color: 7

Bin 3428: 204 of cap free
Amount of items: 2
Items: 
Size: 601919 Color: 19
Size: 397878 Color: 9

Bin 3429: 204 of cap free
Amount of items: 2
Items: 
Size: 612286 Color: 14
Size: 387511 Color: 19

Bin 3430: 205 of cap free
Amount of items: 2
Items: 
Size: 612809 Color: 1
Size: 386987 Color: 0

Bin 3431: 205 of cap free
Amount of items: 2
Items: 
Size: 553807 Color: 9
Size: 445989 Color: 10

Bin 3432: 205 of cap free
Amount of items: 2
Items: 
Size: 631899 Color: 16
Size: 367897 Color: 3

Bin 3433: 206 of cap free
Amount of items: 2
Items: 
Size: 623453 Color: 17
Size: 376342 Color: 13

Bin 3434: 206 of cap free
Amount of items: 2
Items: 
Size: 659922 Color: 14
Size: 339873 Color: 5

Bin 3435: 207 of cap free
Amount of items: 2
Items: 
Size: 747141 Color: 17
Size: 252653 Color: 2

Bin 3436: 207 of cap free
Amount of items: 2
Items: 
Size: 768476 Color: 13
Size: 231318 Color: 0

Bin 3437: 207 of cap free
Amount of items: 2
Items: 
Size: 633243 Color: 10
Size: 366551 Color: 9

Bin 3438: 208 of cap free
Amount of items: 2
Items: 
Size: 768518 Color: 2
Size: 231275 Color: 8

Bin 3439: 208 of cap free
Amount of items: 2
Items: 
Size: 587958 Color: 0
Size: 411835 Color: 8

Bin 3440: 208 of cap free
Amount of items: 2
Items: 
Size: 621556 Color: 14
Size: 378237 Color: 11

Bin 3441: 208 of cap free
Amount of items: 2
Items: 
Size: 659088 Color: 2
Size: 340705 Color: 11

Bin 3442: 209 of cap free
Amount of items: 2
Items: 
Size: 519110 Color: 1
Size: 480682 Color: 16

Bin 3443: 210 of cap free
Amount of items: 2
Items: 
Size: 729623 Color: 15
Size: 270168 Color: 18

Bin 3444: 210 of cap free
Amount of items: 2
Items: 
Size: 540743 Color: 12
Size: 459048 Color: 8

Bin 3445: 211 of cap free
Amount of items: 2
Items: 
Size: 676562 Color: 14
Size: 323228 Color: 1

Bin 3446: 211 of cap free
Amount of items: 2
Items: 
Size: 642370 Color: 3
Size: 357420 Color: 1

Bin 3447: 211 of cap free
Amount of items: 2
Items: 
Size: 762526 Color: 3
Size: 237264 Color: 2

Bin 3448: 211 of cap free
Amount of items: 2
Items: 
Size: 738232 Color: 17
Size: 261558 Color: 8

Bin 3449: 212 of cap free
Amount of items: 2
Items: 
Size: 775830 Color: 7
Size: 223959 Color: 6

Bin 3450: 212 of cap free
Amount of items: 2
Items: 
Size: 508748 Color: 13
Size: 491041 Color: 4

Bin 3451: 212 of cap free
Amount of items: 2
Items: 
Size: 705286 Color: 17
Size: 294503 Color: 18

Bin 3452: 212 of cap free
Amount of items: 2
Items: 
Size: 738971 Color: 8
Size: 260818 Color: 16

Bin 3453: 212 of cap free
Amount of items: 2
Items: 
Size: 787731 Color: 9
Size: 212058 Color: 1

Bin 3454: 213 of cap free
Amount of items: 2
Items: 
Size: 576268 Color: 9
Size: 423520 Color: 2

Bin 3455: 213 of cap free
Amount of items: 2
Items: 
Size: 599113 Color: 19
Size: 400675 Color: 18

Bin 3456: 214 of cap free
Amount of items: 2
Items: 
Size: 587225 Color: 1
Size: 412562 Color: 10

Bin 3457: 215 of cap free
Amount of items: 2
Items: 
Size: 659697 Color: 19
Size: 340089 Color: 13

Bin 3458: 215 of cap free
Amount of items: 2
Items: 
Size: 760730 Color: 5
Size: 239056 Color: 11

Bin 3459: 215 of cap free
Amount of items: 2
Items: 
Size: 723067 Color: 6
Size: 276719 Color: 1

Bin 3460: 215 of cap free
Amount of items: 2
Items: 
Size: 517901 Color: 12
Size: 481885 Color: 14

Bin 3461: 215 of cap free
Amount of items: 2
Items: 
Size: 526618 Color: 3
Size: 473168 Color: 10

Bin 3462: 216 of cap free
Amount of items: 2
Items: 
Size: 735867 Color: 16
Size: 263918 Color: 4

Bin 3463: 216 of cap free
Amount of items: 2
Items: 
Size: 650432 Color: 14
Size: 349353 Color: 6

Bin 3464: 216 of cap free
Amount of items: 2
Items: 
Size: 596805 Color: 11
Size: 402980 Color: 3

Bin 3465: 216 of cap free
Amount of items: 2
Items: 
Size: 544710 Color: 12
Size: 455075 Color: 11

Bin 3466: 216 of cap free
Amount of items: 2
Items: 
Size: 622724 Color: 9
Size: 377061 Color: 2

Bin 3467: 217 of cap free
Amount of items: 2
Items: 
Size: 773828 Color: 18
Size: 225956 Color: 7

Bin 3468: 217 of cap free
Amount of items: 2
Items: 
Size: 606143 Color: 19
Size: 393641 Color: 15

Bin 3469: 217 of cap free
Amount of items: 2
Items: 
Size: 663849 Color: 2
Size: 335935 Color: 6

Bin 3470: 217 of cap free
Amount of items: 2
Items: 
Size: 777894 Color: 10
Size: 221890 Color: 19

Bin 3471: 218 of cap free
Amount of items: 2
Items: 
Size: 577494 Color: 19
Size: 422289 Color: 17

Bin 3472: 218 of cap free
Amount of items: 2
Items: 
Size: 715925 Color: 15
Size: 283858 Color: 12

Bin 3473: 219 of cap free
Amount of items: 2
Items: 
Size: 567141 Color: 13
Size: 432641 Color: 1

Bin 3474: 220 of cap free
Amount of items: 2
Items: 
Size: 715934 Color: 8
Size: 283847 Color: 3

Bin 3475: 220 of cap free
Amount of items: 2
Items: 
Size: 571014 Color: 16
Size: 428767 Color: 0

Bin 3476: 221 of cap free
Amount of items: 2
Items: 
Size: 716877 Color: 17
Size: 282903 Color: 2

Bin 3477: 221 of cap free
Amount of items: 2
Items: 
Size: 749521 Color: 8
Size: 250259 Color: 17

Bin 3478: 221 of cap free
Amount of items: 2
Items: 
Size: 772210 Color: 13
Size: 227570 Color: 0

Bin 3479: 221 of cap free
Amount of items: 2
Items: 
Size: 708816 Color: 3
Size: 290964 Color: 12

Bin 3480: 221 of cap free
Amount of items: 2
Items: 
Size: 723703 Color: 17
Size: 276077 Color: 4

Bin 3481: 222 of cap free
Amount of items: 2
Items: 
Size: 508744 Color: 16
Size: 491035 Color: 14

Bin 3482: 222 of cap free
Amount of items: 2
Items: 
Size: 747071 Color: 7
Size: 252708 Color: 15

Bin 3483: 223 of cap free
Amount of items: 3
Items: 
Size: 614661 Color: 3
Size: 225836 Color: 8
Size: 159281 Color: 9

Bin 3484: 223 of cap free
Amount of items: 2
Items: 
Size: 510168 Color: 19
Size: 489610 Color: 4

Bin 3485: 223 of cap free
Amount of items: 2
Items: 
Size: 691031 Color: 11
Size: 308747 Color: 12

Bin 3486: 223 of cap free
Amount of items: 2
Items: 
Size: 532770 Color: 11
Size: 467008 Color: 3

Bin 3487: 223 of cap free
Amount of items: 2
Items: 
Size: 575024 Color: 2
Size: 424754 Color: 11

Bin 3488: 224 of cap free
Amount of items: 2
Items: 
Size: 615233 Color: 9
Size: 384544 Color: 7

Bin 3489: 224 of cap free
Amount of items: 2
Items: 
Size: 643538 Color: 13
Size: 356239 Color: 11

Bin 3490: 225 of cap free
Amount of items: 2
Items: 
Size: 605669 Color: 2
Size: 394107 Color: 3

Bin 3491: 225 of cap free
Amount of items: 2
Items: 
Size: 753606 Color: 1
Size: 246170 Color: 4

Bin 3492: 225 of cap free
Amount of items: 2
Items: 
Size: 520263 Color: 4
Size: 479513 Color: 13

Bin 3493: 225 of cap free
Amount of items: 2
Items: 
Size: 573112 Color: 5
Size: 426664 Color: 4

Bin 3494: 225 of cap free
Amount of items: 2
Items: 
Size: 564908 Color: 10
Size: 434868 Color: 8

Bin 3495: 226 of cap free
Amount of items: 2
Items: 
Size: 556166 Color: 7
Size: 443609 Color: 0

Bin 3496: 226 of cap free
Amount of items: 2
Items: 
Size: 529330 Color: 9
Size: 470445 Color: 15

Bin 3497: 226 of cap free
Amount of items: 2
Items: 
Size: 563903 Color: 4
Size: 435872 Color: 3

Bin 3498: 227 of cap free
Amount of items: 2
Items: 
Size: 730932 Color: 14
Size: 268842 Color: 2

Bin 3499: 227 of cap free
Amount of items: 2
Items: 
Size: 672660 Color: 2
Size: 327114 Color: 4

Bin 3500: 227 of cap free
Amount of items: 2
Items: 
Size: 673146 Color: 9
Size: 326628 Color: 16

Bin 3501: 227 of cap free
Amount of items: 2
Items: 
Size: 564792 Color: 9
Size: 434982 Color: 10

Bin 3502: 228 of cap free
Amount of items: 3
Items: 
Size: 471098 Color: 13
Size: 265197 Color: 9
Size: 263478 Color: 13

Bin 3503: 228 of cap free
Amount of items: 2
Items: 
Size: 525728 Color: 1
Size: 474045 Color: 6

Bin 3504: 228 of cap free
Amount of items: 2
Items: 
Size: 526617 Color: 7
Size: 473156 Color: 2

Bin 3505: 229 of cap free
Amount of items: 2
Items: 
Size: 596793 Color: 9
Size: 402979 Color: 5

Bin 3506: 229 of cap free
Amount of items: 2
Items: 
Size: 535208 Color: 13
Size: 464564 Color: 1

Bin 3507: 230 of cap free
Amount of items: 2
Items: 
Size: 732362 Color: 8
Size: 267409 Color: 13

Bin 3508: 231 of cap free
Amount of items: 2
Items: 
Size: 556756 Color: 4
Size: 443014 Color: 13

Bin 3509: 232 of cap free
Amount of items: 2
Items: 
Size: 555583 Color: 12
Size: 444186 Color: 8

Bin 3510: 232 of cap free
Amount of items: 2
Items: 
Size: 619389 Color: 17
Size: 380380 Color: 15

Bin 3511: 233 of cap free
Amount of items: 2
Items: 
Size: 683586 Color: 12
Size: 316182 Color: 18

Bin 3512: 234 of cap free
Amount of items: 2
Items: 
Size: 709817 Color: 4
Size: 289950 Color: 1

Bin 3513: 234 of cap free
Amount of items: 2
Items: 
Size: 751689 Color: 1
Size: 248078 Color: 15

Bin 3514: 234 of cap free
Amount of items: 2
Items: 
Size: 793043 Color: 14
Size: 206724 Color: 4

Bin 3515: 234 of cap free
Amount of items: 2
Items: 
Size: 787183 Color: 11
Size: 212584 Color: 3

Bin 3516: 234 of cap free
Amount of items: 2
Items: 
Size: 671583 Color: 8
Size: 328184 Color: 5

Bin 3517: 235 of cap free
Amount of items: 2
Items: 
Size: 512752 Color: 13
Size: 487014 Color: 2

Bin 3518: 235 of cap free
Amount of items: 2
Items: 
Size: 611553 Color: 13
Size: 388213 Color: 11

Bin 3519: 236 of cap free
Amount of items: 2
Items: 
Size: 660900 Color: 9
Size: 338865 Color: 4

Bin 3520: 236 of cap free
Amount of items: 2
Items: 
Size: 512827 Color: 17
Size: 486938 Color: 9

Bin 3521: 237 of cap free
Amount of items: 3
Items: 
Size: 740292 Color: 14
Size: 139511 Color: 11
Size: 119961 Color: 1

Bin 3522: 237 of cap free
Amount of items: 2
Items: 
Size: 626196 Color: 0
Size: 373568 Color: 13

Bin 3523: 237 of cap free
Amount of items: 2
Items: 
Size: 574582 Color: 16
Size: 425182 Color: 0

Bin 3524: 237 of cap free
Amount of items: 2
Items: 
Size: 727873 Color: 18
Size: 271891 Color: 8

Bin 3525: 237 of cap free
Amount of items: 2
Items: 
Size: 537517 Color: 11
Size: 462247 Color: 8

Bin 3526: 237 of cap free
Amount of items: 2
Items: 
Size: 598506 Color: 15
Size: 401258 Color: 5

Bin 3527: 238 of cap free
Amount of items: 2
Items: 
Size: 759109 Color: 3
Size: 240654 Color: 10

Bin 3528: 238 of cap free
Amount of items: 2
Items: 
Size: 562902 Color: 16
Size: 436861 Color: 0

Bin 3529: 238 of cap free
Amount of items: 2
Items: 
Size: 734021 Color: 12
Size: 265742 Color: 4

Bin 3530: 238 of cap free
Amount of items: 2
Items: 
Size: 677164 Color: 13
Size: 322599 Color: 4

Bin 3531: 238 of cap free
Amount of items: 2
Items: 
Size: 677480 Color: 7
Size: 322283 Color: 8

Bin 3532: 238 of cap free
Amount of items: 2
Items: 
Size: 538568 Color: 12
Size: 461195 Color: 15

Bin 3533: 239 of cap free
Amount of items: 2
Items: 
Size: 552259 Color: 4
Size: 447503 Color: 19

Bin 3534: 239 of cap free
Amount of items: 2
Items: 
Size: 762964 Color: 16
Size: 236798 Color: 18

Bin 3535: 240 of cap free
Amount of items: 2
Items: 
Size: 739598 Color: 2
Size: 260163 Color: 15

Bin 3536: 240 of cap free
Amount of items: 2
Items: 
Size: 674116 Color: 9
Size: 325645 Color: 0

Bin 3537: 241 of cap free
Amount of items: 2
Items: 
Size: 708084 Color: 17
Size: 291676 Color: 6

Bin 3538: 242 of cap free
Amount of items: 2
Items: 
Size: 765080 Color: 19
Size: 234679 Color: 10

Bin 3539: 242 of cap free
Amount of items: 2
Items: 
Size: 791364 Color: 4
Size: 208395 Color: 17

Bin 3540: 243 of cap free
Amount of items: 2
Items: 
Size: 599795 Color: 3
Size: 399963 Color: 12

Bin 3541: 244 of cap free
Amount of items: 2
Items: 
Size: 687745 Color: 8
Size: 312012 Color: 15

Bin 3542: 244 of cap free
Amount of items: 2
Items: 
Size: 504437 Color: 11
Size: 495320 Color: 6

Bin 3543: 244 of cap free
Amount of items: 2
Items: 
Size: 533362 Color: 12
Size: 466395 Color: 4

Bin 3544: 244 of cap free
Amount of items: 2
Items: 
Size: 583729 Color: 18
Size: 416028 Color: 12

Bin 3545: 245 of cap free
Amount of items: 2
Items: 
Size: 734583 Color: 3
Size: 265173 Color: 15

Bin 3546: 245 of cap free
Amount of items: 2
Items: 
Size: 643040 Color: 8
Size: 356716 Color: 14

Bin 3547: 246 of cap free
Amount of items: 2
Items: 
Size: 694066 Color: 15
Size: 305689 Color: 8

Bin 3548: 246 of cap free
Amount of items: 2
Items: 
Size: 588959 Color: 11
Size: 410796 Color: 4

Bin 3549: 246 of cap free
Amount of items: 2
Items: 
Size: 704183 Color: 11
Size: 295572 Color: 18

Bin 3550: 246 of cap free
Amount of items: 2
Items: 
Size: 752917 Color: 1
Size: 246838 Color: 17

Bin 3551: 247 of cap free
Amount of items: 2
Items: 
Size: 562824 Color: 11
Size: 436930 Color: 9

Bin 3552: 247 of cap free
Amount of items: 2
Items: 
Size: 739510 Color: 15
Size: 260244 Color: 9

Bin 3553: 247 of cap free
Amount of items: 2
Items: 
Size: 644644 Color: 6
Size: 355110 Color: 13

Bin 3554: 247 of cap free
Amount of items: 2
Items: 
Size: 679074 Color: 18
Size: 320680 Color: 2

Bin 3555: 247 of cap free
Amount of items: 2
Items: 
Size: 572759 Color: 1
Size: 426995 Color: 12

Bin 3556: 247 of cap free
Amount of items: 2
Items: 
Size: 578278 Color: 3
Size: 421476 Color: 1

Bin 3557: 248 of cap free
Amount of items: 2
Items: 
Size: 797087 Color: 5
Size: 202666 Color: 15

Bin 3558: 248 of cap free
Amount of items: 3
Items: 
Size: 679559 Color: 14
Size: 163334 Color: 15
Size: 156860 Color: 1

Bin 3559: 249 of cap free
Amount of items: 2
Items: 
Size: 561605 Color: 15
Size: 438147 Color: 2

Bin 3560: 249 of cap free
Amount of items: 2
Items: 
Size: 732688 Color: 5
Size: 267064 Color: 12

Bin 3561: 251 of cap free
Amount of items: 3
Items: 
Size: 634769 Color: 17
Size: 182670 Color: 1
Size: 182311 Color: 15

Bin 3562: 251 of cap free
Amount of items: 2
Items: 
Size: 713620 Color: 14
Size: 286130 Color: 3

Bin 3563: 251 of cap free
Amount of items: 2
Items: 
Size: 557146 Color: 5
Size: 442604 Color: 15

Bin 3564: 252 of cap free
Amount of items: 2
Items: 
Size: 637660 Color: 3
Size: 362089 Color: 15

Bin 3565: 253 of cap free
Amount of items: 2
Items: 
Size: 541241 Color: 12
Size: 458507 Color: 10

Bin 3566: 253 of cap free
Amount of items: 2
Items: 
Size: 581435 Color: 13
Size: 418313 Color: 19

Bin 3567: 254 of cap free
Amount of items: 2
Items: 
Size: 726312 Color: 13
Size: 273435 Color: 5

Bin 3568: 254 of cap free
Amount of items: 2
Items: 
Size: 793534 Color: 5
Size: 206213 Color: 0

Bin 3569: 254 of cap free
Amount of items: 2
Items: 
Size: 538584 Color: 11
Size: 461163 Color: 9

Bin 3570: 255 of cap free
Amount of items: 2
Items: 
Size: 784265 Color: 6
Size: 215481 Color: 2

Bin 3571: 255 of cap free
Amount of items: 2
Items: 
Size: 563264 Color: 19
Size: 436482 Color: 3

Bin 3572: 255 of cap free
Amount of items: 2
Items: 
Size: 567114 Color: 0
Size: 432632 Color: 17

Bin 3573: 256 of cap free
Amount of items: 2
Items: 
Size: 715585 Color: 4
Size: 284160 Color: 8

Bin 3574: 256 of cap free
Amount of items: 2
Items: 
Size: 737472 Color: 10
Size: 262273 Color: 2

Bin 3575: 256 of cap free
Amount of items: 2
Items: 
Size: 619371 Color: 5
Size: 380374 Color: 9

Bin 3576: 257 of cap free
Amount of items: 2
Items: 
Size: 573784 Color: 3
Size: 425960 Color: 0

Bin 3577: 257 of cap free
Amount of items: 2
Items: 
Size: 690558 Color: 6
Size: 309186 Color: 4

Bin 3578: 258 of cap free
Amount of items: 2
Items: 
Size: 693287 Color: 17
Size: 306456 Color: 15

Bin 3579: 259 of cap free
Amount of items: 2
Items: 
Size: 745637 Color: 9
Size: 254105 Color: 4

Bin 3580: 259 of cap free
Amount of items: 2
Items: 
Size: 618465 Color: 15
Size: 381277 Color: 10

Bin 3581: 259 of cap free
Amount of items: 2
Items: 
Size: 557989 Color: 6
Size: 441753 Color: 15

Bin 3582: 259 of cap free
Amount of items: 2
Items: 
Size: 673543 Color: 4
Size: 326199 Color: 16

Bin 3583: 260 of cap free
Amount of items: 2
Items: 
Size: 542121 Color: 11
Size: 457620 Color: 19

Bin 3584: 260 of cap free
Amount of items: 2
Items: 
Size: 580003 Color: 9
Size: 419738 Color: 10

Bin 3585: 260 of cap free
Amount of items: 2
Items: 
Size: 598504 Color: 16
Size: 401237 Color: 13

Bin 3586: 261 of cap free
Amount of items: 2
Items: 
Size: 710397 Color: 14
Size: 289343 Color: 15

Bin 3587: 263 of cap free
Amount of items: 2
Items: 
Size: 522506 Color: 4
Size: 477232 Color: 3

Bin 3588: 263 of cap free
Amount of items: 2
Items: 
Size: 705273 Color: 15
Size: 294465 Color: 4

Bin 3589: 265 of cap free
Amount of items: 2
Items: 
Size: 565385 Color: 4
Size: 434351 Color: 12

Bin 3590: 266 of cap free
Amount of items: 2
Items: 
Size: 533897 Color: 18
Size: 465838 Color: 3

Bin 3591: 267 of cap free
Amount of items: 2
Items: 
Size: 544164 Color: 15
Size: 455570 Color: 7

Bin 3592: 268 of cap free
Amount of items: 2
Items: 
Size: 523577 Color: 12
Size: 476156 Color: 7

Bin 3593: 268 of cap free
Amount of items: 2
Items: 
Size: 582540 Color: 15
Size: 417193 Color: 14

Bin 3594: 268 of cap free
Amount of items: 2
Items: 
Size: 635242 Color: 14
Size: 364491 Color: 2

Bin 3595: 269 of cap free
Amount of items: 2
Items: 
Size: 741301 Color: 9
Size: 258431 Color: 3

Bin 3596: 269 of cap free
Amount of items: 2
Items: 
Size: 759764 Color: 15
Size: 239968 Color: 8

Bin 3597: 270 of cap free
Amount of items: 2
Items: 
Size: 798171 Color: 18
Size: 201560 Color: 1

Bin 3598: 270 of cap free
Amount of items: 2
Items: 
Size: 716915 Color: 2
Size: 282816 Color: 10

Bin 3599: 270 of cap free
Amount of items: 2
Items: 
Size: 560827 Color: 1
Size: 438904 Color: 8

Bin 3600: 270 of cap free
Amount of items: 2
Items: 
Size: 775801 Color: 16
Size: 223930 Color: 4

Bin 3601: 271 of cap free
Amount of items: 2
Items: 
Size: 503065 Color: 16
Size: 496665 Color: 0

Bin 3602: 272 of cap free
Amount of items: 2
Items: 
Size: 564138 Color: 0
Size: 435591 Color: 3

Bin 3603: 273 of cap free
Amount of items: 2
Items: 
Size: 772649 Color: 3
Size: 227079 Color: 5

Bin 3604: 273 of cap free
Amount of items: 2
Items: 
Size: 513263 Color: 0
Size: 486465 Color: 2

Bin 3605: 274 of cap free
Amount of items: 2
Items: 
Size: 796634 Color: 12
Size: 203093 Color: 0

Bin 3606: 274 of cap free
Amount of items: 2
Items: 
Size: 751074 Color: 11
Size: 248653 Color: 9

Bin 3607: 274 of cap free
Amount of items: 2
Items: 
Size: 789543 Color: 12
Size: 210184 Color: 5

Bin 3608: 277 of cap free
Amount of items: 2
Items: 
Size: 521064 Color: 14
Size: 478660 Color: 4

Bin 3609: 278 of cap free
Amount of items: 2
Items: 
Size: 509711 Color: 5
Size: 490012 Color: 15

Bin 3610: 279 of cap free
Amount of items: 2
Items: 
Size: 542707 Color: 16
Size: 457015 Color: 6

Bin 3611: 279 of cap free
Amount of items: 2
Items: 
Size: 714658 Color: 0
Size: 285064 Color: 7

Bin 3612: 280 of cap free
Amount of items: 2
Items: 
Size: 723692 Color: 18
Size: 276029 Color: 12

Bin 3613: 280 of cap free
Amount of items: 2
Items: 
Size: 525383 Color: 14
Size: 474338 Color: 5

Bin 3614: 280 of cap free
Amount of items: 2
Items: 
Size: 547176 Color: 10
Size: 452545 Color: 0

Bin 3615: 281 of cap free
Amount of items: 2
Items: 
Size: 562583 Color: 15
Size: 437137 Color: 3

Bin 3616: 281 of cap free
Amount of items: 3
Items: 
Size: 606876 Color: 2
Size: 197951 Color: 3
Size: 194893 Color: 13

Bin 3617: 281 of cap free
Amount of items: 2
Items: 
Size: 671559 Color: 10
Size: 328161 Color: 17

Bin 3618: 282 of cap free
Amount of items: 2
Items: 
Size: 675454 Color: 3
Size: 324265 Color: 2

Bin 3619: 282 of cap free
Amount of items: 2
Items: 
Size: 616378 Color: 19
Size: 383341 Color: 14

Bin 3620: 283 of cap free
Amount of items: 2
Items: 
Size: 591824 Color: 12
Size: 407894 Color: 1

Bin 3621: 283 of cap free
Amount of items: 2
Items: 
Size: 658681 Color: 8
Size: 341037 Color: 2

Bin 3622: 284 of cap free
Amount of items: 2
Items: 
Size: 528055 Color: 11
Size: 471662 Color: 5

Bin 3623: 284 of cap free
Amount of items: 2
Items: 
Size: 569791 Color: 2
Size: 429926 Color: 12

Bin 3624: 285 of cap free
Amount of items: 2
Items: 
Size: 770316 Color: 18
Size: 229400 Color: 14

Bin 3625: 285 of cap free
Amount of items: 2
Items: 
Size: 546042 Color: 1
Size: 453674 Color: 16

Bin 3626: 287 of cap free
Amount of items: 2
Items: 
Size: 703214 Color: 15
Size: 296500 Color: 13

Bin 3627: 287 of cap free
Amount of items: 2
Items: 
Size: 725593 Color: 19
Size: 274121 Color: 11

Bin 3628: 288 of cap free
Amount of items: 2
Items: 
Size: 710395 Color: 14
Size: 289318 Color: 17

Bin 3629: 289 of cap free
Amount of items: 2
Items: 
Size: 721154 Color: 10
Size: 278558 Color: 14

Bin 3630: 290 of cap free
Amount of items: 2
Items: 
Size: 631606 Color: 0
Size: 368105 Color: 15

Bin 3631: 291 of cap free
Amount of items: 2
Items: 
Size: 561210 Color: 16
Size: 438500 Color: 13

Bin 3632: 292 of cap free
Amount of items: 2
Items: 
Size: 535856 Color: 13
Size: 463853 Color: 6

Bin 3633: 292 of cap free
Amount of items: 2
Items: 
Size: 607825 Color: 11
Size: 391884 Color: 13

Bin 3634: 293 of cap free
Amount of items: 2
Items: 
Size: 515231 Color: 18
Size: 484477 Color: 16

Bin 3635: 293 of cap free
Amount of items: 2
Items: 
Size: 697524 Color: 7
Size: 302184 Color: 5

Bin 3636: 294 of cap free
Amount of items: 2
Items: 
Size: 770752 Color: 11
Size: 228955 Color: 6

Bin 3637: 294 of cap free
Amount of items: 2
Items: 
Size: 797573 Color: 8
Size: 202134 Color: 3

Bin 3638: 295 of cap free
Amount of items: 2
Items: 
Size: 610206 Color: 5
Size: 389500 Color: 18

Bin 3639: 295 of cap free
Amount of items: 2
Items: 
Size: 658054 Color: 9
Size: 341652 Color: 12

Bin 3640: 296 of cap free
Amount of items: 2
Items: 
Size: 789240 Color: 4
Size: 210465 Color: 11

Bin 3641: 296 of cap free
Amount of items: 2
Items: 
Size: 682683 Color: 13
Size: 317022 Color: 0

Bin 3642: 296 of cap free
Amount of items: 2
Items: 
Size: 580631 Color: 9
Size: 419074 Color: 14

Bin 3643: 297 of cap free
Amount of items: 2
Items: 
Size: 620725 Color: 15
Size: 378979 Color: 6

Bin 3644: 297 of cap free
Amount of items: 2
Items: 
Size: 579965 Color: 16
Size: 419739 Color: 1

Bin 3645: 298 of cap free
Amount of items: 2
Items: 
Size: 604690 Color: 14
Size: 395013 Color: 4

Bin 3646: 298 of cap free
Amount of items: 2
Items: 
Size: 765543 Color: 3
Size: 234160 Color: 1

Bin 3647: 298 of cap free
Amount of items: 2
Items: 
Size: 646540 Color: 2
Size: 353163 Color: 0

Bin 3648: 299 of cap free
Amount of items: 3
Items: 
Size: 689291 Color: 15
Size: 163658 Color: 12
Size: 146753 Color: 19

Bin 3649: 299 of cap free
Amount of items: 2
Items: 
Size: 570714 Color: 14
Size: 428988 Color: 6

Bin 3650: 299 of cap free
Amount of items: 2
Items: 
Size: 777543 Color: 15
Size: 222159 Color: 11

Bin 3651: 300 of cap free
Amount of items: 2
Items: 
Size: 542700 Color: 6
Size: 457001 Color: 19

Bin 3652: 301 of cap free
Amount of items: 2
Items: 
Size: 769388 Color: 8
Size: 230312 Color: 12

Bin 3653: 302 of cap free
Amount of items: 2
Items: 
Size: 626137 Color: 0
Size: 373562 Color: 7

Bin 3654: 304 of cap free
Amount of items: 2
Items: 
Size: 652111 Color: 11
Size: 347586 Color: 15

Bin 3655: 304 of cap free
Amount of items: 2
Items: 
Size: 557663 Color: 8
Size: 442034 Color: 12

Bin 3656: 306 of cap free
Amount of items: 2
Items: 
Size: 602646 Color: 8
Size: 397049 Color: 9

Bin 3657: 308 of cap free
Amount of items: 2
Items: 
Size: 590656 Color: 15
Size: 409037 Color: 13

Bin 3658: 309 of cap free
Amount of items: 2
Items: 
Size: 699555 Color: 9
Size: 300137 Color: 5

Bin 3659: 310 of cap free
Amount of items: 2
Items: 
Size: 647090 Color: 6
Size: 352601 Color: 0

Bin 3660: 310 of cap free
Amount of items: 2
Items: 
Size: 583829 Color: 1
Size: 415862 Color: 6

Bin 3661: 311 of cap free
Amount of items: 2
Items: 
Size: 719950 Color: 9
Size: 279740 Color: 17

Bin 3662: 311 of cap free
Amount of items: 2
Items: 
Size: 544625 Color: 12
Size: 455065 Color: 19

Bin 3663: 312 of cap free
Amount of items: 2
Items: 
Size: 715545 Color: 9
Size: 284144 Color: 4

Bin 3664: 313 of cap free
Amount of items: 3
Items: 
Size: 772133 Color: 17
Size: 117164 Color: 8
Size: 110391 Color: 14

Bin 3665: 313 of cap free
Amount of items: 2
Items: 
Size: 663847 Color: 12
Size: 335841 Color: 2

Bin 3666: 314 of cap free
Amount of items: 2
Items: 
Size: 524910 Color: 6
Size: 474777 Color: 18

Bin 3667: 314 of cap free
Amount of items: 2
Items: 
Size: 555284 Color: 6
Size: 444403 Color: 12

Bin 3668: 316 of cap free
Amount of items: 2
Items: 
Size: 752478 Color: 2
Size: 247207 Color: 10

Bin 3669: 316 of cap free
Amount of items: 2
Items: 
Size: 794635 Color: 0
Size: 205050 Color: 13

Bin 3670: 317 of cap free
Amount of items: 2
Items: 
Size: 722555 Color: 11
Size: 277129 Color: 2

Bin 3671: 318 of cap free
Amount of items: 2
Items: 
Size: 744066 Color: 1
Size: 255617 Color: 17

Bin 3672: 319 of cap free
Amount of items: 2
Items: 
Size: 694078 Color: 17
Size: 305604 Color: 10

Bin 3673: 319 of cap free
Amount of items: 2
Items: 
Size: 531442 Color: 19
Size: 468240 Color: 0

Bin 3674: 321 of cap free
Amount of items: 2
Items: 
Size: 641756 Color: 18
Size: 357924 Color: 13

Bin 3675: 322 of cap free
Amount of items: 2
Items: 
Size: 794277 Color: 3
Size: 205402 Color: 11

Bin 3676: 322 of cap free
Amount of items: 2
Items: 
Size: 676186 Color: 5
Size: 323493 Color: 19

Bin 3677: 322 of cap free
Amount of items: 2
Items: 
Size: 740600 Color: 3
Size: 259079 Color: 1

Bin 3678: 323 of cap free
Amount of items: 2
Items: 
Size: 608557 Color: 11
Size: 391121 Color: 12

Bin 3679: 323 of cap free
Amount of items: 2
Items: 
Size: 606046 Color: 15
Size: 393632 Color: 0

Bin 3680: 323 of cap free
Amount of items: 2
Items: 
Size: 505850 Color: 3
Size: 493828 Color: 13

Bin 3681: 324 of cap free
Amount of items: 2
Items: 
Size: 723037 Color: 10
Size: 276640 Color: 8

Bin 3682: 324 of cap free
Amount of items: 2
Items: 
Size: 511428 Color: 1
Size: 488249 Color: 14

Bin 3683: 325 of cap free
Amount of items: 2
Items: 
Size: 576757 Color: 15
Size: 422919 Color: 19

Bin 3684: 325 of cap free
Amount of items: 2
Items: 
Size: 600538 Color: 13
Size: 399138 Color: 16

Bin 3685: 327 of cap free
Amount of items: 2
Items: 
Size: 531101 Color: 2
Size: 468573 Color: 19

Bin 3686: 327 of cap free
Amount of items: 2
Items: 
Size: 560604 Color: 9
Size: 439070 Color: 1

Bin 3687: 328 of cap free
Amount of items: 2
Items: 
Size: 671532 Color: 2
Size: 328141 Color: 11

Bin 3688: 328 of cap free
Amount of items: 2
Items: 
Size: 783453 Color: 10
Size: 216220 Color: 5

Bin 3689: 329 of cap free
Amount of items: 2
Items: 
Size: 689784 Color: 2
Size: 309888 Color: 4

Bin 3690: 329 of cap free
Amount of items: 2
Items: 
Size: 568242 Color: 13
Size: 431430 Color: 11

Bin 3691: 329 of cap free
Amount of items: 2
Items: 
Size: 753658 Color: 4
Size: 246014 Color: 10

Bin 3692: 329 of cap free
Amount of items: 2
Items: 
Size: 623676 Color: 11
Size: 375996 Color: 4

Bin 3693: 331 of cap free
Amount of items: 3
Items: 
Size: 760960 Color: 8
Size: 121486 Color: 18
Size: 117224 Color: 4

Bin 3694: 331 of cap free
Amount of items: 2
Items: 
Size: 725127 Color: 17
Size: 274543 Color: 0

Bin 3695: 332 of cap free
Amount of items: 2
Items: 
Size: 709784 Color: 9
Size: 289885 Color: 7

Bin 3696: 332 of cap free
Amount of items: 2
Items: 
Size: 595515 Color: 7
Size: 404154 Color: 16

Bin 3697: 333 of cap free
Amount of items: 2
Items: 
Size: 598768 Color: 9
Size: 400900 Color: 15

Bin 3698: 334 of cap free
Amount of items: 2
Items: 
Size: 781578 Color: 14
Size: 218089 Color: 16

Bin 3699: 335 of cap free
Amount of items: 2
Items: 
Size: 798300 Color: 0
Size: 201366 Color: 2

Bin 3700: 335 of cap free
Amount of items: 2
Items: 
Size: 780746 Color: 3
Size: 218920 Color: 8

Bin 3701: 335 of cap free
Amount of items: 2
Items: 
Size: 759083 Color: 16
Size: 240583 Color: 8

Bin 3702: 337 of cap free
Amount of items: 2
Items: 
Size: 505133 Color: 9
Size: 494531 Color: 8

Bin 3703: 337 of cap free
Amount of items: 2
Items: 
Size: 529029 Color: 17
Size: 470635 Color: 13

Bin 3704: 338 of cap free
Amount of items: 2
Items: 
Size: 708013 Color: 11
Size: 291650 Color: 16

Bin 3705: 339 of cap free
Amount of items: 2
Items: 
Size: 569328 Color: 12
Size: 430334 Color: 0

Bin 3706: 339 of cap free
Amount of items: 2
Items: 
Size: 591193 Color: 5
Size: 408469 Color: 4

Bin 3707: 340 of cap free
Amount of items: 2
Items: 
Size: 538557 Color: 19
Size: 461104 Color: 2

Bin 3708: 340 of cap free
Amount of items: 2
Items: 
Size: 721132 Color: 0
Size: 278529 Color: 2

Bin 3709: 341 of cap free
Amount of items: 2
Items: 
Size: 671510 Color: 6
Size: 328150 Color: 19

Bin 3710: 342 of cap free
Amount of items: 2
Items: 
Size: 730929 Color: 16
Size: 268730 Color: 14

Bin 3711: 342 of cap free
Amount of items: 2
Items: 
Size: 761143 Color: 9
Size: 238516 Color: 1

Bin 3712: 342 of cap free
Amount of items: 2
Items: 
Size: 618390 Color: 19
Size: 381269 Color: 1

Bin 3713: 345 of cap free
Amount of items: 3
Items: 
Size: 689112 Color: 0
Size: 176491 Color: 14
Size: 134053 Color: 17

Bin 3714: 345 of cap free
Amount of items: 2
Items: 
Size: 507029 Color: 1
Size: 492627 Color: 11

Bin 3715: 346 of cap free
Amount of items: 2
Items: 
Size: 584613 Color: 16
Size: 415042 Color: 4

Bin 3716: 346 of cap free
Amount of items: 2
Items: 
Size: 585107 Color: 10
Size: 414548 Color: 12

Bin 3717: 348 of cap free
Amount of items: 2
Items: 
Size: 623667 Color: 4
Size: 375986 Color: 5

Bin 3718: 349 of cap free
Amount of items: 2
Items: 
Size: 548550 Color: 17
Size: 451102 Color: 19

Bin 3719: 351 of cap free
Amount of items: 2
Items: 
Size: 692022 Color: 5
Size: 307628 Color: 18

Bin 3720: 351 of cap free
Amount of items: 2
Items: 
Size: 564141 Color: 13
Size: 435509 Color: 12

Bin 3721: 352 of cap free
Amount of items: 2
Items: 
Size: 694059 Color: 5
Size: 305590 Color: 10

Bin 3722: 352 of cap free
Amount of items: 2
Items: 
Size: 699088 Color: 0
Size: 300561 Color: 17

Bin 3723: 352 of cap free
Amount of items: 2
Items: 
Size: 618388 Color: 6
Size: 381261 Color: 19

Bin 3724: 352 of cap free
Amount of items: 2
Items: 
Size: 796593 Color: 5
Size: 203056 Color: 17

Bin 3725: 353 of cap free
Amount of items: 2
Items: 
Size: 680971 Color: 10
Size: 318677 Color: 5

Bin 3726: 354 of cap free
Amount of items: 2
Items: 
Size: 723684 Color: 17
Size: 275963 Color: 7

Bin 3727: 355 of cap free
Amount of items: 2
Items: 
Size: 636356 Color: 16
Size: 363290 Color: 15

Bin 3728: 355 of cap free
Amount of items: 2
Items: 
Size: 517818 Color: 7
Size: 481828 Color: 19

Bin 3729: 357 of cap free
Amount of items: 2
Items: 
Size: 531433 Color: 8
Size: 468211 Color: 6

Bin 3730: 358 of cap free
Amount of items: 2
Items: 
Size: 580606 Color: 15
Size: 419037 Color: 6

Bin 3731: 358 of cap free
Amount of items: 2
Items: 
Size: 507430 Color: 13
Size: 492213 Color: 4

Bin 3732: 358 of cap free
Amount of items: 2
Items: 
Size: 503993 Color: 0
Size: 495650 Color: 7

Bin 3733: 358 of cap free
Amount of items: 2
Items: 
Size: 643030 Color: 4
Size: 356613 Color: 2

Bin 3734: 359 of cap free
Amount of items: 2
Items: 
Size: 792502 Color: 3
Size: 207140 Color: 19

Bin 3735: 360 of cap free
Amount of items: 2
Items: 
Size: 642331 Color: 6
Size: 357310 Color: 15

Bin 3736: 360 of cap free
Amount of items: 2
Items: 
Size: 501284 Color: 2
Size: 498357 Color: 18

Bin 3737: 360 of cap free
Amount of items: 2
Items: 
Size: 611723 Color: 3
Size: 387918 Color: 1

Bin 3738: 361 of cap free
Amount of items: 2
Items: 
Size: 566112 Color: 17
Size: 433528 Color: 13

Bin 3739: 362 of cap free
Amount of items: 2
Items: 
Size: 529047 Color: 15
Size: 470592 Color: 9

Bin 3740: 362 of cap free
Amount of items: 2
Items: 
Size: 610819 Color: 19
Size: 388820 Color: 5

Bin 3741: 364 of cap free
Amount of items: 2
Items: 
Size: 519087 Color: 14
Size: 480550 Color: 6

Bin 3742: 365 of cap free
Amount of items: 2
Items: 
Size: 536340 Color: 19
Size: 463296 Color: 3

Bin 3743: 366 of cap free
Amount of items: 2
Items: 
Size: 790765 Color: 17
Size: 208870 Color: 1

Bin 3744: 366 of cap free
Amount of items: 3
Items: 
Size: 571975 Color: 11
Size: 299936 Color: 15
Size: 127724 Color: 1

Bin 3745: 366 of cap free
Amount of items: 2
Items: 
Size: 530084 Color: 6
Size: 469551 Color: 15

Bin 3746: 367 of cap free
Amount of items: 2
Items: 
Size: 510647 Color: 6
Size: 488987 Color: 1

Bin 3747: 368 of cap free
Amount of items: 2
Items: 
Size: 724043 Color: 7
Size: 275590 Color: 0

Bin 3748: 368 of cap free
Amount of items: 2
Items: 
Size: 505216 Color: 17
Size: 494417 Color: 5

Bin 3749: 368 of cap free
Amount of items: 2
Items: 
Size: 548983 Color: 12
Size: 450650 Color: 1

Bin 3750: 369 of cap free
Amount of items: 3
Items: 
Size: 598007 Color: 9
Size: 231592 Color: 1
Size: 170033 Color: 17

Bin 3751: 370 of cap free
Amount of items: 2
Items: 
Size: 591576 Color: 17
Size: 408055 Color: 14

Bin 3752: 372 of cap free
Amount of items: 2
Items: 
Size: 781572 Color: 12
Size: 218057 Color: 11

Bin 3753: 374 of cap free
Amount of items: 2
Items: 
Size: 790038 Color: 8
Size: 209589 Color: 16

Bin 3754: 375 of cap free
Amount of items: 2
Items: 
Size: 737376 Color: 13
Size: 262250 Color: 0

Bin 3755: 376 of cap free
Amount of items: 2
Items: 
Size: 524166 Color: 13
Size: 475459 Color: 3

Bin 3756: 377 of cap free
Amount of items: 2
Items: 
Size: 648007 Color: 2
Size: 351617 Color: 17

Bin 3757: 380 of cap free
Amount of items: 2
Items: 
Size: 547054 Color: 14
Size: 452567 Color: 10

Bin 3758: 381 of cap free
Amount of items: 2
Items: 
Size: 617633 Color: 7
Size: 381987 Color: 12

Bin 3759: 381 of cap free
Amount of items: 2
Items: 
Size: 622597 Color: 4
Size: 377023 Color: 1

Bin 3760: 383 of cap free
Amount of items: 2
Items: 
Size: 673453 Color: 12
Size: 326165 Color: 14

Bin 3761: 384 of cap free
Amount of items: 2
Items: 
Size: 516335 Color: 19
Size: 483282 Color: 14

Bin 3762: 385 of cap free
Amount of items: 2
Items: 
Size: 768065 Color: 16
Size: 231551 Color: 1

Bin 3763: 385 of cap free
Amount of items: 2
Items: 
Size: 541168 Color: 4
Size: 458448 Color: 14

Bin 3764: 385 of cap free
Amount of items: 2
Items: 
Size: 676137 Color: 1
Size: 323479 Color: 10

Bin 3765: 385 of cap free
Amount of items: 2
Items: 
Size: 670953 Color: 10
Size: 328663 Color: 11

Bin 3766: 385 of cap free
Amount of items: 2
Items: 
Size: 587815 Color: 18
Size: 411801 Color: 19

Bin 3767: 387 of cap free
Amount of items: 2
Items: 
Size: 717503 Color: 10
Size: 282111 Color: 18

Bin 3768: 388 of cap free
Amount of items: 2
Items: 
Size: 605988 Color: 11
Size: 393625 Color: 16

Bin 3769: 389 of cap free
Amount of items: 2
Items: 
Size: 663349 Color: 2
Size: 336263 Color: 1

Bin 3770: 390 of cap free
Amount of items: 2
Items: 
Size: 532721 Color: 14
Size: 466890 Color: 8

Bin 3771: 391 of cap free
Amount of items: 2
Items: 
Size: 748535 Color: 0
Size: 251075 Color: 12

Bin 3772: 391 of cap free
Amount of items: 2
Items: 
Size: 503940 Color: 6
Size: 495670 Color: 9

Bin 3773: 392 of cap free
Amount of items: 2
Items: 
Size: 641716 Color: 12
Size: 357893 Color: 17

Bin 3774: 393 of cap free
Amount of items: 2
Items: 
Size: 658609 Color: 17
Size: 340999 Color: 4

Bin 3775: 393 of cap free
Amount of items: 2
Items: 
Size: 708628 Color: 8
Size: 290980 Color: 11

Bin 3776: 394 of cap free
Amount of items: 2
Items: 
Size: 747960 Color: 18
Size: 251647 Color: 16

Bin 3777: 394 of cap free
Amount of items: 2
Items: 
Size: 571533 Color: 1
Size: 428074 Color: 9

Bin 3778: 395 of cap free
Amount of items: 2
Items: 
Size: 678490 Color: 12
Size: 321116 Color: 2

Bin 3779: 395 of cap free
Amount of items: 2
Items: 
Size: 583590 Color: 15
Size: 416016 Color: 18

Bin 3780: 396 of cap free
Amount of items: 2
Items: 
Size: 524841 Color: 8
Size: 474764 Color: 0

Bin 3781: 396 of cap free
Amount of items: 2
Items: 
Size: 569709 Color: 2
Size: 429896 Color: 16

Bin 3782: 397 of cap free
Amount of items: 2
Items: 
Size: 773820 Color: 17
Size: 225784 Color: 14

Bin 3783: 398 of cap free
Amount of items: 2
Items: 
Size: 782419 Color: 18
Size: 217184 Color: 1

Bin 3784: 398 of cap free
Amount of items: 2
Items: 
Size: 670123 Color: 0
Size: 329480 Color: 6

Bin 3785: 398 of cap free
Amount of items: 2
Items: 
Size: 591572 Color: 14
Size: 408031 Color: 11

Bin 3786: 398 of cap free
Amount of items: 2
Items: 
Size: 658048 Color: 8
Size: 341555 Color: 5

Bin 3787: 399 of cap free
Amount of items: 2
Items: 
Size: 538542 Color: 13
Size: 461060 Color: 2

Bin 3788: 399 of cap free
Amount of items: 2
Items: 
Size: 591164 Color: 17
Size: 408438 Color: 16

Bin 3789: 402 of cap free
Amount of items: 2
Items: 
Size: 759880 Color: 8
Size: 239719 Color: 12

Bin 3790: 403 of cap free
Amount of items: 2
Items: 
Size: 529038 Color: 13
Size: 470560 Color: 14

Bin 3791: 407 of cap free
Amount of items: 2
Items: 
Size: 577454 Color: 3
Size: 422140 Color: 7

Bin 3792: 410 of cap free
Amount of items: 2
Items: 
Size: 499812 Color: 15
Size: 499779 Color: 0

Bin 3793: 411 of cap free
Amount of items: 2
Items: 
Size: 572325 Color: 11
Size: 427265 Color: 9

Bin 3794: 411 of cap free
Amount of items: 2
Items: 
Size: 582527 Color: 9
Size: 417063 Color: 12

Bin 3795: 413 of cap free
Amount of items: 2
Items: 
Size: 799759 Color: 6
Size: 199829 Color: 16

Bin 3796: 415 of cap free
Amount of items: 2
Items: 
Size: 712255 Color: 14
Size: 287331 Color: 6

Bin 3797: 415 of cap free
Amount of items: 2
Items: 
Size: 667686 Color: 0
Size: 331900 Color: 15

Bin 3798: 415 of cap free
Amount of items: 2
Items: 
Size: 703994 Color: 3
Size: 295592 Color: 0

Bin 3799: 417 of cap free
Amount of items: 2
Items: 
Size: 519048 Color: 18
Size: 480536 Color: 17

Bin 3800: 417 of cap free
Amount of items: 2
Items: 
Size: 521852 Color: 7
Size: 477732 Color: 10

Bin 3801: 418 of cap free
Amount of items: 2
Items: 
Size: 720480 Color: 8
Size: 279103 Color: 0

Bin 3802: 418 of cap free
Amount of items: 2
Items: 
Size: 519048 Color: 14
Size: 480535 Color: 18

Bin 3803: 420 of cap free
Amount of items: 2
Items: 
Size: 738778 Color: 18
Size: 260803 Color: 2

Bin 3804: 422 of cap free
Amount of items: 2
Items: 
Size: 562463 Color: 1
Size: 437116 Color: 2

Bin 3805: 422 of cap free
Amount of items: 2
Items: 
Size: 605502 Color: 15
Size: 394077 Color: 7

Bin 3806: 422 of cap free
Amount of items: 2
Items: 
Size: 594573 Color: 19
Size: 405006 Color: 3

Bin 3807: 423 of cap free
Amount of items: 2
Items: 
Size: 569063 Color: 6
Size: 430515 Color: 13

Bin 3808: 425 of cap free
Amount of items: 2
Items: 
Size: 573772 Color: 15
Size: 425804 Color: 16

Bin 3809: 425 of cap free
Amount of items: 2
Items: 
Size: 619914 Color: 19
Size: 379662 Color: 6

Bin 3810: 426 of cap free
Amount of items: 2
Items: 
Size: 575840 Color: 4
Size: 423735 Color: 1

Bin 3811: 427 of cap free
Amount of items: 2
Items: 
Size: 566111 Color: 8
Size: 433463 Color: 1

Bin 3812: 428 of cap free
Amount of items: 2
Items: 
Size: 766002 Color: 17
Size: 233571 Color: 15

Bin 3813: 428 of cap free
Amount of items: 3
Items: 
Size: 791557 Color: 4
Size: 105382 Color: 11
Size: 102634 Color: 17

Bin 3814: 428 of cap free
Amount of items: 2
Items: 
Size: 674727 Color: 8
Size: 324846 Color: 19

Bin 3815: 428 of cap free
Amount of items: 2
Items: 
Size: 785782 Color: 17
Size: 213791 Color: 19

Bin 3816: 431 of cap free
Amount of items: 3
Items: 
Size: 788864 Color: 2
Size: 106797 Color: 7
Size: 103909 Color: 16

Bin 3817: 432 of cap free
Amount of items: 2
Items: 
Size: 545530 Color: 13
Size: 454039 Color: 4

Bin 3818: 433 of cap free
Amount of items: 2
Items: 
Size: 637510 Color: 13
Size: 362058 Color: 4

Bin 3819: 436 of cap free
Amount of items: 2
Items: 
Size: 676118 Color: 15
Size: 323447 Color: 4

Bin 3820: 436 of cap free
Amount of items: 2
Items: 
Size: 513916 Color: 15
Size: 485649 Color: 1

Bin 3821: 436 of cap free
Amount of items: 2
Items: 
Size: 628909 Color: 10
Size: 370656 Color: 11

Bin 3822: 437 of cap free
Amount of items: 2
Items: 
Size: 714944 Color: 12
Size: 284620 Color: 9

Bin 3823: 437 of cap free
Amount of items: 2
Items: 
Size: 599772 Color: 11
Size: 399792 Color: 14

Bin 3824: 437 of cap free
Amount of items: 2
Items: 
Size: 617613 Color: 0
Size: 381951 Color: 2

Bin 3825: 439 of cap free
Amount of items: 3
Items: 
Size: 506522 Color: 17
Size: 293920 Color: 17
Size: 199120 Color: 0

Bin 3826: 439 of cap free
Amount of items: 2
Items: 
Size: 646532 Color: 7
Size: 353030 Color: 6

Bin 3827: 440 of cap free
Amount of items: 2
Items: 
Size: 652073 Color: 2
Size: 347488 Color: 9

Bin 3828: 441 of cap free
Amount of items: 2
Items: 
Size: 772478 Color: 18
Size: 227082 Color: 19

Bin 3829: 443 of cap free
Amount of items: 3
Items: 
Size: 669551 Color: 4
Size: 221062 Color: 19
Size: 108945 Color: 0

Bin 3830: 443 of cap free
Amount of items: 2
Items: 
Size: 641242 Color: 15
Size: 358316 Color: 19

Bin 3831: 445 of cap free
Amount of items: 3
Items: 
Size: 704538 Color: 5
Size: 147679 Color: 13
Size: 147339 Color: 13

Bin 3832: 446 of cap free
Amount of items: 2
Items: 
Size: 639983 Color: 4
Size: 359572 Color: 9

Bin 3833: 447 of cap free
Amount of items: 2
Items: 
Size: 773767 Color: 9
Size: 225787 Color: 5

Bin 3834: 447 of cap free
Amount of items: 2
Items: 
Size: 666976 Color: 2
Size: 332578 Color: 6

Bin 3835: 448 of cap free
Amount of items: 2
Items: 
Size: 689155 Color: 15
Size: 310398 Color: 8

Bin 3836: 448 of cap free
Amount of items: 2
Items: 
Size: 619357 Color: 18
Size: 380196 Color: 17

Bin 3837: 449 of cap free
Amount of items: 2
Items: 
Size: 535003 Color: 11
Size: 464549 Color: 4

Bin 3838: 449 of cap free
Amount of items: 2
Items: 
Size: 569062 Color: 17
Size: 430490 Color: 0

Bin 3839: 451 of cap free
Amount of items: 2
Items: 
Size: 752439 Color: 6
Size: 247111 Color: 7

Bin 3840: 453 of cap free
Amount of items: 2
Items: 
Size: 712776 Color: 19
Size: 286772 Color: 2

Bin 3841: 453 of cap free
Amount of items: 2
Items: 
Size: 644609 Color: 1
Size: 354939 Color: 10

Bin 3842: 454 of cap free
Amount of items: 2
Items: 
Size: 742035 Color: 1
Size: 257512 Color: 6

Bin 3843: 454 of cap free
Amount of items: 2
Items: 
Size: 666889 Color: 13
Size: 332658 Color: 2

Bin 3844: 455 of cap free
Amount of items: 2
Items: 
Size: 754163 Color: 11
Size: 245383 Color: 14

Bin 3845: 455 of cap free
Amount of items: 2
Items: 
Size: 566982 Color: 15
Size: 432564 Color: 6

Bin 3846: 460 of cap free
Amount of items: 2
Items: 
Size: 689142 Color: 8
Size: 310399 Color: 0

Bin 3847: 460 of cap free
Amount of items: 2
Items: 
Size: 785720 Color: 18
Size: 213821 Color: 1

Bin 3848: 460 of cap free
Amount of items: 2
Items: 
Size: 517761 Color: 16
Size: 481780 Color: 6

Bin 3849: 462 of cap free
Amount of items: 2
Items: 
Size: 693954 Color: 2
Size: 305585 Color: 11

Bin 3850: 462 of cap free
Amount of items: 2
Items: 
Size: 547894 Color: 5
Size: 451645 Color: 14

Bin 3851: 463 of cap free
Amount of items: 2
Items: 
Size: 766152 Color: 9
Size: 233386 Color: 11

Bin 3852: 465 of cap free
Amount of items: 2
Items: 
Size: 568069 Color: 7
Size: 431467 Color: 13

Bin 3853: 466 of cap free
Amount of items: 2
Items: 
Size: 520124 Color: 19
Size: 479411 Color: 9

Bin 3854: 466 of cap free
Amount of items: 2
Items: 
Size: 764394 Color: 12
Size: 235141 Color: 6

Bin 3855: 466 of cap free
Amount of items: 2
Items: 
Size: 527203 Color: 15
Size: 472332 Color: 9

Bin 3856: 468 of cap free
Amount of items: 2
Items: 
Size: 505123 Color: 19
Size: 494410 Color: 17

Bin 3857: 468 of cap free
Amount of items: 2
Items: 
Size: 526450 Color: 12
Size: 473083 Color: 15

Bin 3858: 469 of cap free
Amount of items: 2
Items: 
Size: 769318 Color: 10
Size: 230214 Color: 8

Bin 3859: 472 of cap free
Amount of items: 2
Items: 
Size: 672485 Color: 15
Size: 327044 Color: 2

Bin 3860: 476 of cap free
Amount of items: 3
Items: 
Size: 768042 Color: 14
Size: 126847 Color: 11
Size: 104636 Color: 8

Bin 3861: 482 of cap free
Amount of items: 2
Items: 
Size: 633680 Color: 1
Size: 365839 Color: 5

Bin 3862: 482 of cap free
Amount of items: 2
Items: 
Size: 604529 Color: 17
Size: 394990 Color: 1

Bin 3863: 482 of cap free
Amount of items: 2
Items: 
Size: 513891 Color: 15
Size: 485628 Color: 19

Bin 3864: 483 of cap free
Amount of items: 2
Items: 
Size: 562560 Color: 9
Size: 436958 Color: 16

Bin 3865: 488 of cap free
Amount of items: 2
Items: 
Size: 553628 Color: 18
Size: 445885 Color: 19

Bin 3866: 491 of cap free
Amount of items: 2
Items: 
Size: 699547 Color: 15
Size: 299963 Color: 5

Bin 3867: 493 of cap free
Amount of items: 2
Items: 
Size: 795657 Color: 11
Size: 203851 Color: 3

Bin 3868: 494 of cap free
Amount of items: 2
Items: 
Size: 679351 Color: 10
Size: 320156 Color: 18

Bin 3869: 494 of cap free
Amount of items: 2
Items: 
Size: 696615 Color: 18
Size: 302892 Color: 11

Bin 3870: 494 of cap free
Amount of items: 2
Items: 
Size: 542611 Color: 0
Size: 456896 Color: 9

Bin 3871: 496 of cap free
Amount of items: 4
Items: 
Size: 412499 Color: 17
Size: 229538 Color: 5
Size: 185621 Color: 5
Size: 171847 Color: 18

Bin 3872: 496 of cap free
Amount of items: 2
Items: 
Size: 726745 Color: 5
Size: 272760 Color: 3

Bin 3873: 496 of cap free
Amount of items: 2
Items: 
Size: 665122 Color: 18
Size: 334383 Color: 16

Bin 3874: 496 of cap free
Amount of items: 2
Items: 
Size: 545365 Color: 1
Size: 454140 Color: 13

Bin 3875: 498 of cap free
Amount of items: 2
Items: 
Size: 774373 Color: 0
Size: 225130 Color: 10

Bin 3876: 499 of cap free
Amount of items: 2
Items: 
Size: 594561 Color: 1
Size: 404941 Color: 9

Bin 3877: 502 of cap free
Amount of items: 2
Items: 
Size: 508474 Color: 15
Size: 491025 Color: 7

Bin 3878: 504 of cap free
Amount of items: 2
Items: 
Size: 745609 Color: 9
Size: 253888 Color: 10

Bin 3879: 504 of cap free
Amount of items: 2
Items: 
Size: 695281 Color: 15
Size: 304216 Color: 3

Bin 3880: 504 of cap free
Amount of items: 2
Items: 
Size: 769980 Color: 19
Size: 229517 Color: 12

Bin 3881: 506 of cap free
Amount of items: 2
Items: 
Size: 778120 Color: 17
Size: 221375 Color: 9

Bin 3882: 506 of cap free
Amount of items: 2
Items: 
Size: 553035 Color: 7
Size: 446460 Color: 10

Bin 3883: 506 of cap free
Amount of items: 2
Items: 
Size: 790030 Color: 10
Size: 209465 Color: 19

Bin 3884: 507 of cap free
Amount of items: 2
Items: 
Size: 628883 Color: 2
Size: 370611 Color: 7

Bin 3885: 508 of cap free
Amount of items: 2
Items: 
Size: 542599 Color: 14
Size: 456894 Color: 1

Bin 3886: 509 of cap free
Amount of items: 2
Items: 
Size: 718848 Color: 1
Size: 280644 Color: 6

Bin 3887: 510 of cap free
Amount of items: 2
Items: 
Size: 771941 Color: 16
Size: 227550 Color: 7

Bin 3888: 512 of cap free
Amount of items: 2
Items: 
Size: 766815 Color: 16
Size: 232674 Color: 7

Bin 3889: 513 of cap free
Amount of items: 2
Items: 
Size: 731583 Color: 1
Size: 267905 Color: 4

Bin 3890: 514 of cap free
Amount of items: 2
Items: 
Size: 681899 Color: 11
Size: 317588 Color: 18

Bin 3891: 515 of cap free
Amount of items: 2
Items: 
Size: 650269 Color: 18
Size: 349217 Color: 19

Bin 3892: 515 of cap free
Amount of items: 2
Items: 
Size: 656026 Color: 15
Size: 343460 Color: 19

Bin 3893: 518 of cap free
Amount of items: 2
Items: 
Size: 716655 Color: 1
Size: 282828 Color: 19

Bin 3894: 519 of cap free
Amount of items: 2
Items: 
Size: 779516 Color: 1
Size: 219966 Color: 10

Bin 3895: 519 of cap free
Amount of items: 2
Items: 
Size: 543340 Color: 16
Size: 456142 Color: 2

Bin 3896: 520 of cap free
Amount of items: 2
Items: 
Size: 720420 Color: 17
Size: 279061 Color: 2

Bin 3897: 521 of cap free
Amount of items: 2
Items: 
Size: 754906 Color: 6
Size: 244574 Color: 16

Bin 3898: 523 of cap free
Amount of items: 2
Items: 
Size: 691307 Color: 1
Size: 308171 Color: 15

Bin 3899: 524 of cap free
Amount of items: 2
Items: 
Size: 508718 Color: 7
Size: 490759 Color: 11

Bin 3900: 524 of cap free
Amount of items: 2
Items: 
Size: 787660 Color: 18
Size: 211817 Color: 17

Bin 3901: 525 of cap free
Amount of items: 2
Items: 
Size: 558754 Color: 0
Size: 440722 Color: 2

Bin 3902: 527 of cap free
Amount of items: 2
Items: 
Size: 602436 Color: 16
Size: 397038 Color: 13

Bin 3903: 530 of cap free
Amount of items: 2
Items: 
Size: 516213 Color: 19
Size: 483258 Color: 17

Bin 3904: 531 of cap free
Amount of items: 2
Items: 
Size: 733842 Color: 12
Size: 265628 Color: 17

Bin 3905: 531 of cap free
Amount of items: 2
Items: 
Size: 698145 Color: 12
Size: 301325 Color: 4

Bin 3906: 531 of cap free
Amount of items: 2
Items: 
Size: 506944 Color: 2
Size: 492526 Color: 17

Bin 3907: 531 of cap free
Amount of items: 2
Items: 
Size: 789381 Color: 11
Size: 210089 Color: 14

Bin 3908: 532 of cap free
Amount of items: 2
Items: 
Size: 584736 Color: 1
Size: 414733 Color: 10

Bin 3909: 532 of cap free
Amount of items: 3
Items: 
Size: 631898 Color: 3
Size: 197791 Color: 3
Size: 169780 Color: 15

Bin 3910: 537 of cap free
Amount of items: 2
Items: 
Size: 739324 Color: 18
Size: 260140 Color: 4

Bin 3911: 539 of cap free
Amount of items: 2
Items: 
Size: 697463 Color: 3
Size: 301999 Color: 18

Bin 3912: 540 of cap free
Amount of items: 2
Items: 
Size: 502869 Color: 16
Size: 496592 Color: 5

Bin 3913: 544 of cap free
Amount of items: 2
Items: 
Size: 612795 Color: 8
Size: 386662 Color: 16

Bin 3914: 546 of cap free
Amount of items: 2
Items: 
Size: 766135 Color: 1
Size: 233320 Color: 0

Bin 3915: 546 of cap free
Amount of items: 2
Items: 
Size: 788499 Color: 19
Size: 210956 Color: 8

Bin 3916: 548 of cap free
Amount of items: 2
Items: 
Size: 520095 Color: 6
Size: 479358 Color: 8

Bin 3917: 550 of cap free
Amount of items: 2
Items: 
Size: 609623 Color: 7
Size: 389828 Color: 9

Bin 3918: 551 of cap free
Amount of items: 2
Items: 
Size: 678445 Color: 14
Size: 321005 Color: 16

Bin 3919: 551 of cap free
Amount of items: 2
Items: 
Size: 792486 Color: 3
Size: 206964 Color: 17

Bin 3920: 556 of cap free
Amount of items: 2
Items: 
Size: 755688 Color: 9
Size: 243757 Color: 13

Bin 3921: 556 of cap free
Amount of items: 2
Items: 
Size: 616282 Color: 17
Size: 383163 Color: 9

Bin 3922: 558 of cap free
Amount of items: 2
Items: 
Size: 529943 Color: 19
Size: 469500 Color: 12

Bin 3923: 558 of cap free
Amount of items: 2
Items: 
Size: 550315 Color: 15
Size: 449128 Color: 4

Bin 3924: 561 of cap free
Amount of items: 2
Items: 
Size: 501134 Color: 11
Size: 498306 Color: 17

Bin 3925: 562 of cap free
Amount of items: 2
Items: 
Size: 799720 Color: 17
Size: 199719 Color: 5

Bin 3926: 564 of cap free
Amount of items: 2
Items: 
Size: 500351 Color: 18
Size: 499086 Color: 1

Bin 3927: 564 of cap free
Amount of items: 2
Items: 
Size: 536170 Color: 5
Size: 463267 Color: 7

Bin 3928: 568 of cap free
Amount of items: 2
Items: 
Size: 744659 Color: 13
Size: 254774 Color: 15

Bin 3929: 570 of cap free
Amount of items: 2
Items: 
Size: 684267 Color: 9
Size: 315164 Color: 10

Bin 3930: 570 of cap free
Amount of items: 2
Items: 
Size: 679337 Color: 15
Size: 320094 Color: 6

Bin 3931: 571 of cap free
Amount of items: 2
Items: 
Size: 678430 Color: 1
Size: 321000 Color: 8

Bin 3932: 572 of cap free
Amount of items: 2
Items: 
Size: 688484 Color: 18
Size: 310945 Color: 1

Bin 3933: 572 of cap free
Amount of items: 2
Items: 
Size: 788450 Color: 18
Size: 210979 Color: 10

Bin 3934: 575 of cap free
Amount of items: 2
Items: 
Size: 787063 Color: 18
Size: 212363 Color: 19

Bin 3935: 576 of cap free
Amount of items: 2
Items: 
Size: 788446 Color: 11
Size: 210979 Color: 14

Bin 3936: 577 of cap free
Amount of items: 2
Items: 
Size: 634950 Color: 15
Size: 364474 Color: 3

Bin 3937: 578 of cap free
Amount of items: 2
Items: 
Size: 542581 Color: 2
Size: 456842 Color: 0

Bin 3938: 580 of cap free
Amount of items: 2
Items: 
Size: 757297 Color: 15
Size: 242124 Color: 17

Bin 3939: 581 of cap free
Amount of items: 2
Items: 
Size: 788538 Color: 13
Size: 210882 Color: 3

Bin 3940: 581 of cap free
Amount of items: 2
Items: 
Size: 522324 Color: 5
Size: 477096 Color: 9

Bin 3941: 586 of cap free
Amount of items: 2
Items: 
Size: 745360 Color: 5
Size: 254055 Color: 17

Bin 3942: 587 of cap free
Amount of items: 2
Items: 
Size: 674602 Color: 10
Size: 324812 Color: 19

Bin 3943: 588 of cap free
Amount of items: 2
Items: 
Size: 791458 Color: 17
Size: 207955 Color: 2

Bin 3944: 589 of cap free
Amount of items: 2
Items: 
Size: 609591 Color: 5
Size: 389821 Color: 8

Bin 3945: 590 of cap free
Amount of items: 2
Items: 
Size: 706310 Color: 13
Size: 293101 Color: 14

Bin 3946: 591 of cap free
Amount of items: 2
Items: 
Size: 594514 Color: 6
Size: 404896 Color: 5

Bin 3947: 591 of cap free
Amount of items: 2
Items: 
Size: 634940 Color: 4
Size: 364470 Color: 11

Bin 3948: 592 of cap free
Amount of items: 2
Items: 
Size: 553028 Color: 18
Size: 446381 Color: 3

Bin 3949: 592 of cap free
Amount of items: 2
Items: 
Size: 526330 Color: 1
Size: 473079 Color: 15

Bin 3950: 595 of cap free
Amount of items: 2
Items: 
Size: 793917 Color: 3
Size: 205489 Color: 19

Bin 3951: 597 of cap free
Amount of items: 2
Items: 
Size: 569021 Color: 15
Size: 430383 Color: 1

Bin 3952: 598 of cap free
Amount of items: 2
Items: 
Size: 562295 Color: 2
Size: 437108 Color: 7

Bin 3953: 598 of cap free
Amount of items: 2
Items: 
Size: 751381 Color: 7
Size: 248022 Color: 11

Bin 3954: 599 of cap free
Amount of items: 2
Items: 
Size: 743452 Color: 9
Size: 255950 Color: 8

Bin 3955: 603 of cap free
Amount of items: 2
Items: 
Size: 743419 Color: 7
Size: 255979 Color: 17

Bin 3956: 603 of cap free
Amount of items: 2
Items: 
Size: 622501 Color: 2
Size: 376897 Color: 5

Bin 3957: 604 of cap free
Amount of items: 2
Items: 
Size: 679341 Color: 1
Size: 320056 Color: 11

Bin 3958: 606 of cap free
Amount of items: 2
Items: 
Size: 657340 Color: 19
Size: 342055 Color: 2

Bin 3959: 608 of cap free
Amount of items: 2
Items: 
Size: 799700 Color: 14
Size: 199693 Color: 11

Bin 3960: 609 of cap free
Amount of items: 2
Items: 
Size: 770316 Color: 6
Size: 229076 Color: 1

Bin 3961: 610 of cap free
Amount of items: 2
Items: 
Size: 720501 Color: 17
Size: 278890 Color: 11

Bin 3962: 612 of cap free
Amount of items: 2
Items: 
Size: 623451 Color: 17
Size: 375938 Color: 10

Bin 3963: 612 of cap free
Amount of items: 2
Items: 
Size: 548363 Color: 7
Size: 451026 Color: 10

Bin 3964: 613 of cap free
Amount of items: 2
Items: 
Size: 738693 Color: 13
Size: 260695 Color: 7

Bin 3965: 614 of cap free
Amount of items: 2
Items: 
Size: 561931 Color: 13
Size: 437456 Color: 9

Bin 3966: 614 of cap free
Amount of items: 2
Items: 
Size: 529021 Color: 8
Size: 470366 Color: 11

Bin 3967: 617 of cap free
Amount of items: 2
Items: 
Size: 726940 Color: 3
Size: 272444 Color: 19

Bin 3968: 620 of cap free
Amount of items: 2
Items: 
Size: 733659 Color: 16
Size: 265722 Color: 5

Bin 3969: 620 of cap free
Amount of items: 2
Items: 
Size: 529922 Color: 13
Size: 469459 Color: 17

Bin 3970: 621 of cap free
Amount of items: 2
Items: 
Size: 582360 Color: 3
Size: 417020 Color: 2

Bin 3971: 624 of cap free
Amount of items: 2
Items: 
Size: 733684 Color: 19
Size: 265693 Color: 2

Bin 3972: 624 of cap free
Amount of items: 2
Items: 
Size: 757597 Color: 17
Size: 241780 Color: 0

Bin 3973: 625 of cap free
Amount of items: 2
Items: 
Size: 775718 Color: 19
Size: 223658 Color: 10

Bin 3974: 625 of cap free
Amount of items: 2
Items: 
Size: 727559 Color: 11
Size: 271817 Color: 15

Bin 3975: 626 of cap free
Amount of items: 2
Items: 
Size: 617591 Color: 8
Size: 381784 Color: 7

Bin 3976: 627 of cap free
Amount of items: 2
Items: 
Size: 557714 Color: 12
Size: 441660 Color: 17

Bin 3977: 631 of cap free
Amount of items: 2
Items: 
Size: 728250 Color: 5
Size: 271120 Color: 13

Bin 3978: 632 of cap free
Amount of items: 2
Items: 
Size: 527765 Color: 18
Size: 471604 Color: 15

Bin 3979: 637 of cap free
Amount of items: 4
Items: 
Size: 394907 Color: 6
Size: 242018 Color: 17
Size: 181621 Color: 7
Size: 180818 Color: 2

Bin 3980: 640 of cap free
Amount of items: 2
Items: 
Size: 608247 Color: 18
Size: 391114 Color: 4

Bin 3981: 643 of cap free
Amount of items: 2
Items: 
Size: 617581 Color: 2
Size: 381777 Color: 11

Bin 3982: 644 of cap free
Amount of items: 2
Items: 
Size: 506881 Color: 6
Size: 492476 Color: 13

Bin 3983: 646 of cap free
Amount of items: 2
Items: 
Size: 742030 Color: 11
Size: 257325 Color: 0

Bin 3984: 646 of cap free
Amount of items: 2
Items: 
Size: 596630 Color: 5
Size: 402725 Color: 8

Bin 3985: 647 of cap free
Amount of items: 2
Items: 
Size: 709487 Color: 2
Size: 289867 Color: 11

Bin 3986: 649 of cap free
Amount of items: 2
Items: 
Size: 610583 Color: 16
Size: 388769 Color: 12

Bin 3987: 650 of cap free
Amount of items: 2
Items: 
Size: 521059 Color: 4
Size: 478292 Color: 10

Bin 3988: 650 of cap free
Amount of items: 2
Items: 
Size: 539608 Color: 15
Size: 459743 Color: 10

Bin 3989: 651 of cap free
Amount of items: 2
Items: 
Size: 740968 Color: 12
Size: 258382 Color: 17

Bin 3990: 655 of cap free
Amount of items: 2
Items: 
Size: 749292 Color: 9
Size: 250054 Color: 17

Bin 3991: 656 of cap free
Amount of items: 2
Items: 
Size: 656142 Color: 8
Size: 343203 Color: 7

Bin 3992: 657 of cap free
Amount of items: 2
Items: 
Size: 621156 Color: 8
Size: 378188 Color: 13

Bin 3993: 660 of cap free
Amount of items: 2
Items: 
Size: 527747 Color: 16
Size: 471594 Color: 1

Bin 3994: 661 of cap free
Amount of items: 2
Items: 
Size: 505101 Color: 17
Size: 494239 Color: 15

Bin 3995: 662 of cap free
Amount of items: 2
Items: 
Size: 754085 Color: 10
Size: 245254 Color: 9

Bin 3996: 667 of cap free
Amount of items: 2
Items: 
Size: 728851 Color: 13
Size: 270483 Color: 16

Bin 3997: 667 of cap free
Amount of items: 2
Items: 
Size: 514442 Color: 15
Size: 484892 Color: 17

Bin 3998: 671 of cap free
Amount of items: 2
Items: 
Size: 660495 Color: 9
Size: 338835 Color: 14

Bin 3999: 674 of cap free
Amount of items: 3
Items: 
Size: 521687 Color: 15
Size: 246741 Color: 17
Size: 230899 Color: 12

Bin 4000: 674 of cap free
Amount of items: 2
Items: 
Size: 558490 Color: 11
Size: 440837 Color: 0

Bin 4001: 682 of cap free
Amount of items: 2
Items: 
Size: 725257 Color: 4
Size: 274062 Color: 18

Bin 4002: 683 of cap free
Amount of items: 2
Items: 
Size: 537137 Color: 14
Size: 462181 Color: 11

Bin 4003: 684 of cap free
Amount of items: 2
Items: 
Size: 596602 Color: 0
Size: 402715 Color: 13

Bin 4004: 684 of cap free
Amount of items: 2
Items: 
Size: 526273 Color: 13
Size: 473044 Color: 12

Bin 4005: 685 of cap free
Amount of items: 2
Items: 
Size: 720263 Color: 19
Size: 279053 Color: 5

Bin 4006: 686 of cap free
Amount of items: 2
Items: 
Size: 617549 Color: 11
Size: 381766 Color: 17

Bin 4007: 689 of cap free
Amount of items: 2
Items: 
Size: 686667 Color: 14
Size: 312645 Color: 17

Bin 4008: 691 of cap free
Amount of items: 2
Items: 
Size: 542476 Color: 9
Size: 456834 Color: 19

Bin 4009: 691 of cap free
Amount of items: 2
Items: 
Size: 557654 Color: 12
Size: 441656 Color: 1

Bin 4010: 698 of cap free
Amount of items: 2
Items: 
Size: 523876 Color: 19
Size: 475427 Color: 4

Bin 4011: 698 of cap free
Amount of items: 2
Items: 
Size: 543189 Color: 12
Size: 456114 Color: 10

Bin 4012: 701 of cap free
Amount of items: 2
Items: 
Size: 652038 Color: 11
Size: 347262 Color: 16

Bin 4013: 703 of cap free
Amount of items: 2
Items: 
Size: 789214 Color: 1
Size: 210084 Color: 14

Bin 4014: 705 of cap free
Amount of items: 2
Items: 
Size: 740944 Color: 4
Size: 258352 Color: 14

Bin 4015: 706 of cap free
Amount of items: 2
Items: 
Size: 633632 Color: 13
Size: 365663 Color: 19

Bin 4016: 708 of cap free
Amount of items: 2
Items: 
Size: 680884 Color: 19
Size: 318409 Color: 11

Bin 4017: 709 of cap free
Amount of items: 2
Items: 
Size: 543735 Color: 8
Size: 455557 Color: 13

Bin 4018: 709 of cap free
Amount of items: 2
Items: 
Size: 744630 Color: 4
Size: 254662 Color: 17

Bin 4019: 711 of cap free
Amount of items: 2
Items: 
Size: 650162 Color: 13
Size: 349128 Color: 15

Bin 4020: 714 of cap free
Amount of items: 2
Items: 
Size: 613716 Color: 16
Size: 385571 Color: 1

Bin 4021: 715 of cap free
Amount of items: 2
Items: 
Size: 740927 Color: 0
Size: 258359 Color: 4

Bin 4022: 716 of cap free
Amount of items: 2
Items: 
Size: 770079 Color: 6
Size: 229206 Color: 10

Bin 4023: 718 of cap free
Amount of items: 2
Items: 
Size: 681904 Color: 18
Size: 317379 Color: 0

Bin 4024: 718 of cap free
Amount of items: 2
Items: 
Size: 599552 Color: 19
Size: 399731 Color: 6

Bin 4025: 718 of cap free
Amount of items: 2
Items: 
Size: 787660 Color: 10
Size: 211623 Color: 9

Bin 4026: 721 of cap free
Amount of items: 2
Items: 
Size: 764049 Color: 13
Size: 235231 Color: 19

Bin 4027: 722 of cap free
Amount of items: 2
Items: 
Size: 557634 Color: 3
Size: 441645 Color: 15

Bin 4028: 724 of cap free
Amount of items: 2
Items: 
Size: 613765 Color: 9
Size: 385512 Color: 19

Bin 4029: 730 of cap free
Amount of items: 2
Items: 
Size: 759671 Color: 12
Size: 239600 Color: 0

Bin 4030: 730 of cap free
Amount of items: 2
Items: 
Size: 517749 Color: 13
Size: 481522 Color: 15

Bin 4031: 730 of cap free
Amount of items: 2
Items: 
Size: 577279 Color: 13
Size: 421992 Color: 0

Bin 4032: 731 of cap free
Amount of items: 2
Items: 
Size: 672242 Color: 10
Size: 327028 Color: 3

Bin 4033: 735 of cap free
Amount of items: 2
Items: 
Size: 749299 Color: 8
Size: 249967 Color: 12

Bin 4034: 736 of cap free
Amount of items: 2
Items: 
Size: 519008 Color: 0
Size: 480257 Color: 1

Bin 4035: 739 of cap free
Amount of items: 3
Items: 
Size: 687637 Color: 0
Size: 158283 Color: 15
Size: 153342 Color: 4

Bin 4036: 741 of cap free
Amount of items: 2
Items: 
Size: 567949 Color: 1
Size: 431311 Color: 15

Bin 4037: 743 of cap free
Amount of items: 2
Items: 
Size: 542444 Color: 16
Size: 456814 Color: 13

Bin 4038: 743 of cap free
Amount of items: 2
Items: 
Size: 733076 Color: 5
Size: 266182 Color: 4

Bin 4039: 747 of cap free
Amount of items: 2
Items: 
Size: 557620 Color: 0
Size: 441634 Color: 3

Bin 4040: 749 of cap free
Amount of items: 2
Items: 
Size: 610523 Color: 7
Size: 388729 Color: 0

Bin 4041: 750 of cap free
Amount of items: 2
Items: 
Size: 583443 Color: 13
Size: 415808 Color: 0

Bin 4042: 751 of cap free
Amount of items: 2
Items: 
Size: 684134 Color: 4
Size: 315116 Color: 18

Bin 4043: 752 of cap free
Amount of items: 2
Items: 
Size: 711211 Color: 0
Size: 288038 Color: 9

Bin 4044: 753 of cap free
Amount of items: 2
Items: 
Size: 772097 Color: 2
Size: 227151 Color: 9

Bin 4045: 754 of cap free
Amount of items: 2
Items: 
Size: 795605 Color: 15
Size: 203642 Color: 12

Bin 4046: 755 of cap free
Amount of items: 2
Items: 
Size: 799639 Color: 17
Size: 199607 Color: 1

Bin 4047: 756 of cap free
Amount of items: 2
Items: 
Size: 604390 Color: 0
Size: 394855 Color: 4

Bin 4048: 759 of cap free
Amount of items: 2
Items: 
Size: 619254 Color: 16
Size: 379988 Color: 1

Bin 4049: 760 of cap free
Amount of items: 2
Items: 
Size: 546041 Color: 15
Size: 453200 Color: 1

Bin 4050: 762 of cap free
Amount of items: 2
Items: 
Size: 526198 Color: 2
Size: 473041 Color: 1

Bin 4051: 763 of cap free
Amount of items: 2
Items: 
Size: 539523 Color: 0
Size: 459715 Color: 10

Bin 4052: 766 of cap free
Amount of items: 3
Items: 
Size: 696053 Color: 15
Size: 171029 Color: 14
Size: 132153 Color: 11

Bin 4053: 768 of cap free
Amount of items: 2
Items: 
Size: 558415 Color: 12
Size: 440818 Color: 0

Bin 4054: 770 of cap free
Amount of items: 2
Items: 
Size: 526202 Color: 1
Size: 473029 Color: 8

Bin 4055: 773 of cap free
Amount of items: 2
Items: 
Size: 500883 Color: 10
Size: 498345 Color: 0

Bin 4056: 773 of cap free
Amount of items: 2
Items: 
Size: 667447 Color: 18
Size: 331781 Color: 5

Bin 4057: 774 of cap free
Amount of items: 2
Items: 
Size: 561140 Color: 15
Size: 438087 Color: 1

Bin 4058: 777 of cap free
Amount of items: 2
Items: 
Size: 611833 Color: 8
Size: 387391 Color: 16

Bin 4059: 780 of cap free
Amount of items: 2
Items: 
Size: 695247 Color: 8
Size: 303974 Color: 17

Bin 4060: 782 of cap free
Amount of items: 2
Items: 
Size: 798077 Color: 5
Size: 201142 Color: 2

Bin 4061: 782 of cap free
Amount of items: 2
Items: 
Size: 625020 Color: 3
Size: 374199 Color: 4

Bin 4062: 783 of cap free
Amount of items: 2
Items: 
Size: 799678 Color: 14
Size: 199540 Color: 10

Bin 4063: 786 of cap free
Amount of items: 3
Items: 
Size: 682323 Color: 1
Size: 159975 Color: 9
Size: 156917 Color: 7

Bin 4064: 789 of cap free
Amount of items: 2
Items: 
Size: 762008 Color: 9
Size: 237204 Color: 1

Bin 4065: 790 of cap free
Amount of items: 2
Items: 
Size: 639658 Color: 16
Size: 359553 Color: 1

Bin 4066: 790 of cap free
Amount of items: 2
Items: 
Size: 529892 Color: 16
Size: 469319 Color: 10

Bin 4067: 793 of cap free
Amount of items: 2
Items: 
Size: 692401 Color: 7
Size: 306807 Color: 0

Bin 4068: 793 of cap free
Amount of items: 2
Items: 
Size: 623420 Color: 1
Size: 375788 Color: 16

Bin 4069: 793 of cap free
Amount of items: 2
Items: 
Size: 575741 Color: 14
Size: 423467 Color: 7

Bin 4070: 794 of cap free
Amount of items: 2
Items: 
Size: 754847 Color: 11
Size: 244360 Color: 14

Bin 4071: 799 of cap free
Amount of items: 2
Items: 
Size: 698072 Color: 3
Size: 301130 Color: 4

Bin 4072: 800 of cap free
Amount of items: 2
Items: 
Size: 707726 Color: 0
Size: 291475 Color: 8

Bin 4073: 803 of cap free
Amount of items: 3
Items: 
Size: 579155 Color: 11
Size: 227300 Color: 16
Size: 192743 Color: 19

Bin 4074: 805 of cap free
Amount of items: 2
Items: 
Size: 573395 Color: 11
Size: 425801 Color: 3

Bin 4075: 805 of cap free
Amount of items: 2
Items: 
Size: 759477 Color: 18
Size: 239719 Color: 10

Bin 4076: 805 of cap free
Amount of items: 2
Items: 
Size: 565896 Color: 5
Size: 433300 Color: 1

Bin 4077: 806 of cap free
Amount of items: 3
Items: 
Size: 530928 Color: 10
Size: 234355 Color: 7
Size: 233912 Color: 5

Bin 4078: 808 of cap free
Amount of items: 2
Items: 
Size: 705042 Color: 3
Size: 294151 Color: 1

Bin 4079: 808 of cap free
Amount of items: 2
Items: 
Size: 750563 Color: 5
Size: 248630 Color: 11

Bin 4080: 812 of cap free
Amount of items: 2
Items: 
Size: 741986 Color: 4
Size: 257203 Color: 19

Bin 4081: 815 of cap free
Amount of items: 2
Items: 
Size: 602419 Color: 5
Size: 396767 Color: 9

Bin 4082: 816 of cap free
Amount of items: 2
Items: 
Size: 765031 Color: 13
Size: 234154 Color: 12

Bin 4083: 820 of cap free
Amount of items: 2
Items: 
Size: 575722 Color: 1
Size: 423459 Color: 8

Bin 4084: 821 of cap free
Amount of items: 2
Items: 
Size: 511073 Color: 0
Size: 488107 Color: 4

Bin 4085: 822 of cap free
Amount of items: 2
Items: 
Size: 624751 Color: 17
Size: 374428 Color: 11

Bin 4086: 822 of cap free
Amount of items: 2
Items: 
Size: 639639 Color: 19
Size: 359540 Color: 12

Bin 4087: 823 of cap free
Amount of items: 2
Items: 
Size: 545985 Color: 15
Size: 453193 Color: 6

Bin 4088: 826 of cap free
Amount of items: 2
Items: 
Size: 720179 Color: 14
Size: 278996 Color: 18

Bin 4089: 827 of cap free
Amount of items: 2
Items: 
Size: 684097 Color: 12
Size: 315077 Color: 3

Bin 4090: 828 of cap free
Amount of items: 2
Items: 
Size: 783833 Color: 16
Size: 215340 Color: 3

Bin 4091: 835 of cap free
Amount of items: 2
Items: 
Size: 628777 Color: 14
Size: 370389 Color: 1

Bin 4092: 836 of cap free
Amount of items: 2
Items: 
Size: 664989 Color: 4
Size: 334176 Color: 8

Bin 4093: 838 of cap free
Amount of items: 2
Items: 
Size: 757147 Color: 10
Size: 242016 Color: 17

Bin 4094: 844 of cap free
Amount of items: 2
Items: 
Size: 702979 Color: 1
Size: 296178 Color: 3

Bin 4095: 844 of cap free
Amount of items: 2
Items: 
Size: 760685 Color: 10
Size: 238472 Color: 8

Bin 4096: 847 of cap free
Amount of items: 2
Items: 
Size: 563591 Color: 5
Size: 435563 Color: 18

Bin 4097: 853 of cap free
Amount of items: 2
Items: 
Size: 618987 Color: 6
Size: 380161 Color: 13

Bin 4098: 853 of cap free
Amount of items: 2
Items: 
Size: 567923 Color: 1
Size: 431225 Color: 18

Bin 4099: 855 of cap free
Amount of items: 2
Items: 
Size: 685358 Color: 8
Size: 313788 Color: 12

Bin 4100: 858 of cap free
Amount of items: 2
Items: 
Size: 594398 Color: 0
Size: 404745 Color: 7

Bin 4101: 865 of cap free
Amount of items: 2
Items: 
Size: 634851 Color: 5
Size: 364285 Color: 2

Bin 4102: 867 of cap free
Amount of items: 2
Items: 
Size: 667441 Color: 3
Size: 331693 Color: 10

Bin 4103: 870 of cap free
Amount of items: 2
Items: 
Size: 785476 Color: 12
Size: 213655 Color: 13

Bin 4104: 872 of cap free
Amount of items: 2
Items: 
Size: 672143 Color: 8
Size: 326986 Color: 4

Bin 4105: 873 of cap free
Amount of items: 2
Items: 
Size: 741982 Color: 0
Size: 257146 Color: 13

Bin 4106: 875 of cap free
Amount of items: 3
Items: 
Size: 571379 Color: 8
Size: 245395 Color: 11
Size: 182352 Color: 3

Bin 4107: 878 of cap free
Amount of items: 2
Items: 
Size: 563564 Color: 7
Size: 435559 Color: 2

Bin 4108: 882 of cap free
Amount of items: 2
Items: 
Size: 798181 Color: 2
Size: 200938 Color: 15

Bin 4109: 891 of cap free
Amount of items: 2
Items: 
Size: 686498 Color: 7
Size: 312612 Color: 1

Bin 4110: 893 of cap free
Amount of items: 2
Items: 
Size: 562649 Color: 2
Size: 436459 Color: 11

Bin 4111: 894 of cap free
Amount of items: 3
Items: 
Size: 709413 Color: 17
Size: 151738 Color: 18
Size: 137956 Color: 10

Bin 4112: 899 of cap free
Amount of items: 2
Items: 
Size: 672188 Color: 16
Size: 326914 Color: 4

Bin 4113: 900 of cap free
Amount of items: 2
Items: 
Size: 565842 Color: 8
Size: 433259 Color: 2

Bin 4114: 904 of cap free
Amount of items: 2
Items: 
Size: 653796 Color: 17
Size: 345301 Color: 4

Bin 4115: 910 of cap free
Amount of items: 2
Items: 
Size: 529789 Color: 10
Size: 469302 Color: 1

Bin 4116: 913 of cap free
Amount of items: 2
Items: 
Size: 661964 Color: 4
Size: 337124 Color: 14

Bin 4117: 915 of cap free
Amount of items: 2
Items: 
Size: 537070 Color: 6
Size: 462016 Color: 2

Bin 4118: 927 of cap free
Amount of items: 2
Items: 
Size: 508641 Color: 9
Size: 490433 Color: 6

Bin 4119: 927 of cap free
Amount of items: 2
Items: 
Size: 647732 Color: 13
Size: 351342 Color: 15

Bin 4120: 931 of cap free
Amount of items: 2
Items: 
Size: 563551 Color: 14
Size: 435519 Color: 8

Bin 4121: 934 of cap free
Amount of items: 2
Items: 
Size: 707620 Color: 16
Size: 291447 Color: 15

Bin 4122: 939 of cap free
Amount of items: 2
Items: 
Size: 608245 Color: 0
Size: 390817 Color: 18

Bin 4123: 940 of cap free
Amount of items: 2
Items: 
Size: 587537 Color: 12
Size: 411524 Color: 11

Bin 4124: 940 of cap free
Amount of items: 2
Items: 
Size: 591121 Color: 15
Size: 407940 Color: 17

Bin 4125: 942 of cap free
Amount of items: 2
Items: 
Size: 565834 Color: 4
Size: 433225 Color: 3

Bin 4126: 945 of cap free
Amount of items: 2
Items: 
Size: 791358 Color: 18
Size: 207698 Color: 8

Bin 4127: 949 of cap free
Amount of items: 2
Items: 
Size: 631809 Color: 10
Size: 367243 Color: 1

Bin 4128: 951 of cap free
Amount of items: 2
Items: 
Size: 526145 Color: 5
Size: 472905 Color: 8

Bin 4129: 954 of cap free
Amount of items: 2
Items: 
Size: 602308 Color: 10
Size: 396739 Color: 12

Bin 4130: 957 of cap free
Amount of items: 2
Items: 
Size: 664955 Color: 1
Size: 334089 Color: 14

Bin 4131: 962 of cap free
Amount of items: 2
Items: 
Size: 553169 Color: 9
Size: 445870 Color: 6

Bin 4132: 962 of cap free
Amount of items: 2
Items: 
Size: 591112 Color: 17
Size: 407927 Color: 2

Bin 4133: 963 of cap free
Amount of items: 2
Items: 
Size: 759424 Color: 18
Size: 239614 Color: 0

Bin 4134: 964 of cap free
Amount of items: 2
Items: 
Size: 779075 Color: 17
Size: 219962 Color: 1

Bin 4135: 965 of cap free
Amount of items: 2
Items: 
Size: 594387 Color: 10
Size: 404649 Color: 18

Bin 4136: 971 of cap free
Amount of items: 2
Items: 
Size: 516204 Color: 9
Size: 482826 Color: 15

Bin 4137: 972 of cap free
Amount of items: 2
Items: 
Size: 598373 Color: 9
Size: 400656 Color: 2

Bin 4138: 975 of cap free
Amount of items: 2
Items: 
Size: 601174 Color: 17
Size: 397852 Color: 18

Bin 4139: 984 of cap free
Amount of items: 2
Items: 
Size: 717441 Color: 14
Size: 281576 Color: 0

Bin 4140: 988 of cap free
Amount of items: 2
Items: 
Size: 650173 Color: 9
Size: 348840 Color: 18

Bin 4141: 991 of cap free
Amount of items: 2
Items: 
Size: 567903 Color: 3
Size: 431107 Color: 10

Bin 4142: 992 of cap free
Amount of items: 2
Items: 
Size: 583359 Color: 11
Size: 415650 Color: 8

Bin 4143: 994 of cap free
Amount of items: 2
Items: 
Size: 591109 Color: 1
Size: 407898 Color: 11

Bin 4144: 999 of cap free
Amount of items: 2
Items: 
Size: 562199 Color: 4
Size: 436803 Color: 11

Bin 4145: 999 of cap free
Amount of items: 2
Items: 
Size: 518981 Color: 13
Size: 480021 Color: 9

Bin 4146: 1000 of cap free
Amount of items: 2
Items: 
Size: 614567 Color: 4
Size: 384434 Color: 9

Bin 4147: 1009 of cap free
Amount of items: 2
Items: 
Size: 561116 Color: 2
Size: 437876 Color: 14

Bin 4148: 1013 of cap free
Amount of items: 2
Items: 
Size: 550298 Color: 17
Size: 448690 Color: 2

Bin 4149: 1019 of cap free
Amount of items: 2
Items: 
Size: 657967 Color: 15
Size: 341015 Color: 12

Bin 4150: 1023 of cap free
Amount of items: 2
Items: 
Size: 583332 Color: 0
Size: 415646 Color: 6

Bin 4151: 1028 of cap free
Amount of items: 2
Items: 
Size: 609513 Color: 11
Size: 389460 Color: 7

Bin 4152: 1032 of cap free
Amount of items: 2
Items: 
Size: 567864 Color: 16
Size: 431105 Color: 19

Bin 4153: 1033 of cap free
Amount of items: 2
Items: 
Size: 792462 Color: 11
Size: 206506 Color: 3

Bin 4154: 1033 of cap free
Amount of items: 2
Items: 
Size: 500711 Color: 5
Size: 498257 Color: 6

Bin 4155: 1037 of cap free
Amount of items: 2
Items: 
Size: 591087 Color: 11
Size: 407877 Color: 12

Bin 4156: 1041 of cap free
Amount of items: 2
Items: 
Size: 562177 Color: 8
Size: 436783 Color: 2

Bin 4157: 1044 of cap free
Amount of items: 2
Items: 
Size: 763261 Color: 11
Size: 235696 Color: 6

Bin 4158: 1051 of cap free
Amount of items: 2
Items: 
Size: 564599 Color: 11
Size: 434351 Color: 13

Bin 4159: 1064 of cap free
Amount of items: 2
Items: 
Size: 785343 Color: 14
Size: 213594 Color: 18

Bin 4160: 1065 of cap free
Amount of items: 2
Items: 
Size: 579592 Color: 11
Size: 419344 Color: 1

Bin 4161: 1067 of cap free
Amount of items: 2
Items: 
Size: 647852 Color: 1
Size: 351082 Color: 11

Bin 4162: 1070 of cap free
Amount of items: 2
Items: 
Size: 799608 Color: 19
Size: 199323 Color: 0

Bin 4163: 1071 of cap free
Amount of items: 2
Items: 
Size: 725117 Color: 16
Size: 273813 Color: 3

Bin 4164: 1072 of cap free
Amount of items: 2
Items: 
Size: 516130 Color: 16
Size: 482799 Color: 2

Bin 4165: 1076 of cap free
Amount of items: 2
Items: 
Size: 582299 Color: 17
Size: 416626 Color: 1

Bin 4166: 1077 of cap free
Amount of items: 2
Items: 
Size: 785450 Color: 15
Size: 213474 Color: 5

Bin 4167: 1080 of cap free
Amount of items: 2
Items: 
Size: 558456 Color: 3
Size: 440465 Color: 4

Bin 4168: 1082 of cap free
Amount of items: 2
Items: 
Size: 602159 Color: 13
Size: 396760 Color: 10

Bin 4169: 1083 of cap free
Amount of items: 2
Items: 
Size: 573175 Color: 12
Size: 425743 Color: 5

Bin 4170: 1085 of cap free
Amount of items: 2
Items: 
Size: 650159 Color: 14
Size: 348757 Color: 5

Bin 4171: 1085 of cap free
Amount of items: 2
Items: 
Size: 795377 Color: 11
Size: 203539 Color: 2

Bin 4172: 1090 of cap free
Amount of items: 2
Items: 
Size: 762098 Color: 5
Size: 236813 Color: 16

Bin 4173: 1106 of cap free
Amount of items: 2
Items: 
Size: 591091 Color: 15
Size: 407804 Color: 3

Bin 4174: 1107 of cap free
Amount of items: 2
Items: 
Size: 564592 Color: 8
Size: 434302 Color: 13

Bin 4175: 1114 of cap free
Amount of items: 2
Items: 
Size: 693816 Color: 14
Size: 305071 Color: 15

Bin 4176: 1115 of cap free
Amount of items: 3
Items: 
Size: 694641 Color: 3
Size: 154240 Color: 15
Size: 150005 Color: 2

Bin 4177: 1122 of cap free
Amount of items: 2
Items: 
Size: 591085 Color: 2
Size: 407794 Color: 8

Bin 4178: 1132 of cap free
Amount of items: 2
Items: 
Size: 735682 Color: 12
Size: 263187 Color: 3

Bin 4179: 1139 of cap free
Amount of items: 2
Items: 
Size: 695118 Color: 1
Size: 303744 Color: 8

Bin 4180: 1139 of cap free
Amount of items: 2
Items: 
Size: 683877 Color: 3
Size: 314985 Color: 0

Bin 4181: 1149 of cap free
Amount of items: 2
Items: 
Size: 725058 Color: 3
Size: 273794 Color: 9

Bin 4182: 1150 of cap free
Amount of items: 2
Items: 
Size: 761701 Color: 5
Size: 237150 Color: 11

Bin 4183: 1151 of cap free
Amount of items: 2
Items: 
Size: 639600 Color: 15
Size: 359250 Color: 19

Bin 4184: 1151 of cap free
Amount of items: 2
Items: 
Size: 526139 Color: 0
Size: 472711 Color: 4

Bin 4185: 1154 of cap free
Amount of items: 2
Items: 
Size: 583251 Color: 7
Size: 415596 Color: 6

Bin 4186: 1166 of cap free
Amount of items: 2
Items: 
Size: 709542 Color: 1
Size: 289293 Color: 10

Bin 4187: 1167 of cap free
Amount of items: 2
Items: 
Size: 663277 Color: 5
Size: 335557 Color: 16

Bin 4188: 1172 of cap free
Amount of items: 2
Items: 
Size: 558409 Color: 18
Size: 440420 Color: 15

Bin 4189: 1178 of cap free
Amount of items: 2
Items: 
Size: 789972 Color: 11
Size: 208851 Color: 2

Bin 4190: 1186 of cap free
Amount of items: 2
Items: 
Size: 799546 Color: 2
Size: 199269 Color: 0

Bin 4191: 1193 of cap free
Amount of items: 2
Items: 
Size: 647731 Color: 5
Size: 351077 Color: 10

Bin 4192: 1209 of cap free
Amount of items: 2
Items: 
Size: 594195 Color: 0
Size: 404597 Color: 15

Bin 4193: 1213 of cap free
Amount of items: 2
Items: 
Size: 777284 Color: 10
Size: 221504 Color: 16

Bin 4194: 1215 of cap free
Amount of items: 2
Items: 
Size: 670124 Color: 6
Size: 328662 Color: 11

Bin 4195: 1215 of cap free
Amount of items: 2
Items: 
Size: 598149 Color: 10
Size: 400637 Color: 12

Bin 4196: 1216 of cap free
Amount of items: 2
Items: 
Size: 647725 Color: 17
Size: 351060 Color: 15

Bin 4197: 1217 of cap free
Amount of items: 2
Items: 
Size: 526120 Color: 8
Size: 472664 Color: 16

Bin 4198: 1225 of cap free
Amount of items: 2
Items: 
Size: 564585 Color: 0
Size: 434191 Color: 8

Bin 4199: 1232 of cap free
Amount of items: 2
Items: 
Size: 579844 Color: 15
Size: 418925 Color: 19

Bin 4200: 1235 of cap free
Amount of items: 2
Items: 
Size: 671860 Color: 11
Size: 326906 Color: 5

Bin 4201: 1238 of cap free
Amount of items: 2
Items: 
Size: 558368 Color: 12
Size: 440395 Color: 18

Bin 4202: 1239 of cap free
Amount of items: 2
Items: 
Size: 741828 Color: 0
Size: 256934 Color: 18

Bin 4203: 1243 of cap free
Amount of items: 3
Items: 
Size: 706396 Color: 10
Size: 153314 Color: 9
Size: 139048 Color: 2

Bin 4204: 1248 of cap free
Amount of items: 2
Items: 
Size: 591034 Color: 14
Size: 407719 Color: 19

Bin 4205: 1248 of cap free
Amount of items: 2
Items: 
Size: 594161 Color: 4
Size: 404592 Color: 19

Bin 4206: 1257 of cap free
Amount of items: 2
Items: 
Size: 513953 Color: 17
Size: 484791 Color: 9

Bin 4207: 1267 of cap free
Amount of items: 2
Items: 
Size: 616020 Color: 9
Size: 382714 Color: 1

Bin 4208: 1271 of cap free
Amount of items: 2
Items: 
Size: 688482 Color: 4
Size: 310248 Color: 13

Bin 4209: 1278 of cap free
Amount of items: 2
Items: 
Size: 624453 Color: 16
Size: 374270 Color: 10

Bin 4210: 1280 of cap free
Amount of items: 2
Items: 
Size: 674517 Color: 19
Size: 324204 Color: 14

Bin 4211: 1287 of cap free
Amount of items: 2
Items: 
Size: 695088 Color: 10
Size: 303626 Color: 0

Bin 4212: 1289 of cap free
Amount of items: 2
Items: 
Size: 621137 Color: 19
Size: 377575 Color: 17

Bin 4213: 1292 of cap free
Amount of items: 2
Items: 
Size: 675811 Color: 2
Size: 322898 Color: 9

Bin 4214: 1298 of cap free
Amount of items: 2
Items: 
Size: 741769 Color: 8
Size: 256934 Color: 19

Bin 4215: 1303 of cap free
Amount of items: 2
Items: 
Size: 715885 Color: 15
Size: 282813 Color: 1

Bin 4216: 1305 of cap free
Amount of items: 2
Items: 
Size: 641693 Color: 1
Size: 357003 Color: 2

Bin 4217: 1328 of cap free
Amount of items: 2
Items: 
Size: 771960 Color: 8
Size: 226713 Color: 11

Bin 4218: 1331 of cap free
Amount of items: 2
Items: 
Size: 579763 Color: 7
Size: 418907 Color: 8

Bin 4219: 1340 of cap free
Amount of items: 2
Items: 
Size: 537048 Color: 15
Size: 461613 Color: 11

Bin 4220: 1342 of cap free
Amount of items: 2
Items: 
Size: 770025 Color: 1
Size: 228634 Color: 13

Bin 4221: 1348 of cap free
Amount of items: 2
Items: 
Size: 583081 Color: 5
Size: 415572 Color: 3

Bin 4222: 1352 of cap free
Amount of items: 2
Items: 
Size: 594074 Color: 12
Size: 404575 Color: 14

Bin 4223: 1354 of cap free
Amount of items: 2
Items: 
Size: 507944 Color: 14
Size: 490703 Color: 15

Bin 4224: 1354 of cap free
Amount of items: 2
Items: 
Size: 539237 Color: 14
Size: 459410 Color: 3

Bin 4225: 1355 of cap free
Amount of items: 2
Items: 
Size: 572938 Color: 10
Size: 425708 Color: 18

Bin 4226: 1356 of cap free
Amount of items: 2
Items: 
Size: 785340 Color: 13
Size: 213305 Color: 10

Bin 4227: 1367 of cap free
Amount of items: 2
Items: 
Size: 799552 Color: 1
Size: 199082 Color: 3

Bin 4228: 1367 of cap free
Amount of items: 2
Items: 
Size: 579741 Color: 2
Size: 418893 Color: 8

Bin 4229: 1374 of cap free
Amount of items: 2
Items: 
Size: 567543 Color: 6
Size: 431084 Color: 14

Bin 4230: 1378 of cap free
Amount of items: 2
Items: 
Size: 661852 Color: 19
Size: 336771 Color: 8

Bin 4231: 1389 of cap free
Amount of items: 2
Items: 
Size: 624345 Color: 16
Size: 374267 Color: 8

Bin 4232: 1390 of cap free
Amount of items: 2
Items: 
Size: 532262 Color: 6
Size: 466349 Color: 2

Bin 4233: 1394 of cap free
Amount of items: 2
Items: 
Size: 567531 Color: 2
Size: 431076 Color: 4

Bin 4234: 1400 of cap free
Amount of items: 2
Items: 
Size: 532262 Color: 17
Size: 466339 Color: 3

Bin 4235: 1414 of cap free
Amount of items: 2
Items: 
Size: 552896 Color: 8
Size: 445691 Color: 13

Bin 4236: 1415 of cap free
Amount of items: 2
Items: 
Size: 771739 Color: 7
Size: 226847 Color: 4

Bin 4237: 1415 of cap free
Amount of items: 2
Items: 
Size: 766095 Color: 11
Size: 232491 Color: 19

Bin 4238: 1425 of cap free
Amount of items: 2
Items: 
Size: 513201 Color: 15
Size: 485375 Color: 2

Bin 4239: 1427 of cap free
Amount of items: 2
Items: 
Size: 680091 Color: 6
Size: 318483 Color: 15

Bin 4240: 1430 of cap free
Amount of items: 2
Items: 
Size: 526049 Color: 5
Size: 472522 Color: 6

Bin 4241: 1433 of cap free
Amount of items: 2
Items: 
Size: 550013 Color: 4
Size: 448555 Color: 9

Bin 4242: 1434 of cap free
Amount of items: 2
Items: 
Size: 601907 Color: 18
Size: 396660 Color: 12

Bin 4243: 1435 of cap free
Amount of items: 2
Items: 
Size: 567529 Color: 8
Size: 431037 Color: 12

Bin 4244: 1438 of cap free
Amount of items: 2
Items: 
Size: 547786 Color: 10
Size: 450777 Color: 12

Bin 4245: 1440 of cap free
Amount of items: 2
Items: 
Size: 639320 Color: 12
Size: 359241 Color: 18

Bin 4246: 1446 of cap free
Amount of items: 2
Items: 
Size: 738020 Color: 7
Size: 260535 Color: 18

Bin 4247: 1453 of cap free
Amount of items: 2
Items: 
Size: 513184 Color: 1
Size: 485364 Color: 18

Bin 4248: 1459 of cap free
Amount of items: 2
Items: 
Size: 513182 Color: 16
Size: 485360 Color: 10

Bin 4249: 1460 of cap free
Amount of items: 2
Items: 
Size: 777381 Color: 9
Size: 221160 Color: 0

Bin 4250: 1463 of cap free
Amount of items: 2
Items: 
Size: 596394 Color: 10
Size: 402144 Color: 7

Bin 4251: 1463 of cap free
Amount of items: 2
Items: 
Size: 587047 Color: 3
Size: 411491 Color: 8

Bin 4252: 1476 of cap free
Amount of items: 2
Items: 
Size: 799516 Color: 4
Size: 199009 Color: 17

Bin 4253: 1479 of cap free
Amount of items: 2
Items: 
Size: 707745 Color: 16
Size: 290777 Color: 3

Bin 4254: 1482 of cap free
Amount of items: 2
Items: 
Size: 750111 Color: 15
Size: 248408 Color: 8

Bin 4255: 1499 of cap free
Amount of items: 2
Items: 
Size: 590996 Color: 19
Size: 407506 Color: 0

Bin 4256: 1500 of cap free
Amount of items: 2
Items: 
Size: 695017 Color: 11
Size: 303484 Color: 1

Bin 4257: 1511 of cap free
Amount of items: 2
Items: 
Size: 607691 Color: 13
Size: 390799 Color: 16

Bin 4258: 1524 of cap free
Amount of items: 2
Items: 
Size: 783311 Color: 8
Size: 215166 Color: 4

Bin 4259: 1531 of cap free
Amount of items: 2
Items: 
Size: 797338 Color: 10
Size: 201132 Color: 4

Bin 4260: 1539 of cap free
Amount of items: 2
Items: 
Size: 583003 Color: 16
Size: 415459 Color: 18

Bin 4261: 1545 of cap free
Amount of items: 2
Items: 
Size: 607682 Color: 8
Size: 390774 Color: 14

Bin 4262: 1552 of cap free
Amount of items: 2
Items: 
Size: 513097 Color: 16
Size: 485352 Color: 5

Bin 4263: 1556 of cap free
Amount of items: 2
Items: 
Size: 549971 Color: 5
Size: 448474 Color: 19

Bin 4264: 1559 of cap free
Amount of items: 2
Items: 
Size: 695037 Color: 5
Size: 303405 Color: 8

Bin 4265: 1564 of cap free
Amount of items: 2
Items: 
Size: 532217 Color: 0
Size: 466220 Color: 19

Bin 4266: 1569 of cap free
Amount of items: 2
Items: 
Size: 799514 Color: 6
Size: 198918 Color: 4

Bin 4267: 1580 of cap free
Amount of items: 2
Items: 
Size: 500471 Color: 13
Size: 497950 Color: 5

Bin 4268: 1582 of cap free
Amount of items: 2
Items: 
Size: 587031 Color: 2
Size: 411388 Color: 8

Bin 4269: 1589 of cap free
Amount of items: 2
Items: 
Size: 639309 Color: 3
Size: 359103 Color: 5

Bin 4270: 1594 of cap free
Amount of items: 2
Items: 
Size: 624228 Color: 14
Size: 374179 Color: 0

Bin 4271: 1605 of cap free
Amount of items: 2
Items: 
Size: 500371 Color: 14
Size: 498025 Color: 9

Bin 4272: 1612 of cap free
Amount of items: 2
Items: 
Size: 547745 Color: 8
Size: 450644 Color: 2

Bin 4273: 1615 of cap free
Amount of items: 2
Items: 
Size: 575528 Color: 0
Size: 422858 Color: 3

Bin 4274: 1624 of cap free
Amount of items: 2
Items: 
Size: 777324 Color: 12
Size: 221053 Color: 17

Bin 4275: 1628 of cap free
Amount of items: 2
Items: 
Size: 539364 Color: 3
Size: 459009 Color: 13

Bin 4276: 1631 of cap free
Amount of items: 2
Items: 
Size: 500972 Color: 3
Size: 497398 Color: 6

Bin 4277: 1636 of cap free
Amount of items: 2
Items: 
Size: 746265 Color: 6
Size: 252100 Color: 5

Bin 4278: 1636 of cap free
Amount of items: 2
Items: 
Size: 542344 Color: 18
Size: 456021 Color: 6

Bin 4279: 1646 of cap free
Amount of items: 2
Items: 
Size: 722601 Color: 1
Size: 275754 Color: 11

Bin 4280: 1664 of cap free
Amount of items: 2
Items: 
Size: 771677 Color: 16
Size: 226660 Color: 10

Bin 4281: 1672 of cap free
Amount of items: 2
Items: 
Size: 586947 Color: 7
Size: 411382 Color: 11

Bin 4282: 1688 of cap free
Amount of items: 2
Items: 
Size: 777214 Color: 0
Size: 221099 Color: 4

Bin 4283: 1692 of cap free
Amount of items: 2
Items: 
Size: 624278 Color: 19
Size: 374031 Color: 16

Bin 4284: 1699 of cap free
Amount of items: 2
Items: 
Size: 549848 Color: 1
Size: 448454 Color: 16

Bin 4285: 1701 of cap free
Amount of items: 2
Items: 
Size: 649916 Color: 9
Size: 348384 Color: 0

Bin 4286: 1703 of cap free
Amount of items: 2
Items: 
Size: 664823 Color: 3
Size: 333475 Color: 9

Bin 4287: 1711 of cap free
Amount of items: 2
Items: 
Size: 599407 Color: 13
Size: 398883 Color: 5

Bin 4288: 1711 of cap free
Amount of items: 2
Items: 
Size: 549848 Color: 16
Size: 448442 Color: 6

Bin 4289: 1714 of cap free
Amount of items: 2
Items: 
Size: 788233 Color: 9
Size: 210054 Color: 6

Bin 4290: 1719 of cap free
Amount of items: 2
Items: 
Size: 771619 Color: 8
Size: 226663 Color: 13

Bin 4291: 1735 of cap free
Amount of items: 2
Items: 
Size: 525999 Color: 2
Size: 472267 Color: 3

Bin 4292: 1747 of cap free
Amount of items: 2
Items: 
Size: 575485 Color: 5
Size: 422769 Color: 13

Bin 4293: 1754 of cap free
Amount of items: 2
Items: 
Size: 513076 Color: 10
Size: 485171 Color: 3

Bin 4294: 1766 of cap free
Amount of items: 2
Items: 
Size: 562759 Color: 7
Size: 435476 Color: 12

Bin 4295: 1775 of cap free
Amount of items: 2
Items: 
Size: 763297 Color: 16
Size: 234929 Color: 12

Bin 4296: 1776 of cap free
Amount of items: 2
Items: 
Size: 513074 Color: 8
Size: 485151 Color: 15

Bin 4297: 1805 of cap free
Amount of items: 2
Items: 
Size: 513074 Color: 0
Size: 485122 Color: 11

Bin 4298: 1824 of cap free
Amount of items: 2
Items: 
Size: 579601 Color: 6
Size: 418576 Color: 13

Bin 4299: 1827 of cap free
Amount of items: 2
Items: 
Size: 799703 Color: 11
Size: 198471 Color: 17

Bin 4300: 1830 of cap free
Amount of items: 2
Items: 
Size: 500444 Color: 11
Size: 497727 Color: 0

Bin 4301: 1841 of cap free
Amount of items: 2
Items: 
Size: 799478 Color: 15
Size: 198682 Color: 8

Bin 4302: 1860 of cap free
Amount of items: 2
Items: 
Size: 579052 Color: 9
Size: 419089 Color: 1

Bin 4303: 1893 of cap free
Amount of items: 2
Items: 
Size: 624218 Color: 8
Size: 373890 Color: 12

Bin 4304: 1900 of cap free
Amount of items: 2
Items: 
Size: 590645 Color: 3
Size: 407456 Color: 18

Bin 4305: 1901 of cap free
Amount of items: 3
Items: 
Size: 454918 Color: 10
Size: 272382 Color: 12
Size: 270800 Color: 12

Bin 4306: 1909 of cap free
Amount of items: 2
Items: 
Size: 639130 Color: 8
Size: 358962 Color: 5

Bin 4307: 1919 of cap free
Amount of items: 2
Items: 
Size: 683418 Color: 18
Size: 314664 Color: 10

Bin 4308: 1937 of cap free
Amount of items: 2
Items: 
Size: 639123 Color: 3
Size: 358941 Color: 13

Bin 4309: 1945 of cap free
Amount of items: 2
Items: 
Size: 560159 Color: 5
Size: 437897 Color: 1

Bin 4310: 1957 of cap free
Amount of items: 2
Items: 
Size: 666729 Color: 9
Size: 331315 Color: 2

Bin 4311: 1962 of cap free
Amount of items: 2
Items: 
Size: 609329 Color: 13
Size: 388710 Color: 0

Bin 4312: 1976 of cap free
Amount of items: 2
Items: 
Size: 771648 Color: 3
Size: 226377 Color: 8

Bin 4313: 1985 of cap free
Amount of items: 2
Items: 
Size: 560173 Color: 19
Size: 437843 Color: 0

Bin 4314: 1988 of cap free
Amount of items: 2
Items: 
Size: 695040 Color: 5
Size: 302973 Color: 6

Bin 4315: 1988 of cap free
Amount of items: 2
Items: 
Size: 513025 Color: 15
Size: 484988 Color: 18

Bin 4316: 1989 of cap free
Amount of items: 2
Items: 
Size: 590588 Color: 14
Size: 407424 Color: 15

Bin 4317: 1996 of cap free
Amount of items: 2
Items: 
Size: 775117 Color: 2
Size: 222888 Color: 16

Bin 4318: 2005 of cap free
Amount of items: 2
Items: 
Size: 542413 Color: 6
Size: 455583 Color: 0

Bin 4319: 2011 of cap free
Amount of items: 2
Items: 
Size: 685138 Color: 15
Size: 312852 Color: 11

Bin 4320: 2012 of cap free
Amount of items: 2
Items: 
Size: 507456 Color: 9
Size: 490533 Color: 12

Bin 4321: 2014 of cap free
Amount of items: 2
Items: 
Size: 560162 Color: 6
Size: 437825 Color: 15

Bin 4322: 2015 of cap free
Amount of items: 2
Items: 
Size: 771630 Color: 1
Size: 226356 Color: 6

Bin 4323: 2016 of cap free
Amount of items: 2
Items: 
Size: 698209 Color: 4
Size: 299776 Color: 16

Bin 4324: 2027 of cap free
Amount of items: 2
Items: 
Size: 549798 Color: 14
Size: 448176 Color: 3

Bin 4325: 2052 of cap free
Amount of items: 2
Items: 
Size: 797412 Color: 16
Size: 200537 Color: 5

Bin 4326: 2062 of cap free
Amount of items: 2
Items: 
Size: 659342 Color: 18
Size: 338597 Color: 6

Bin 4327: 2069 of cap free
Amount of items: 2
Items: 
Size: 504758 Color: 5
Size: 493174 Color: 15

Bin 4328: 2074 of cap free
Amount of items: 2
Items: 
Size: 590553 Color: 14
Size: 407374 Color: 13

Bin 4329: 2095 of cap free
Amount of items: 2
Items: 
Size: 504750 Color: 5
Size: 493156 Color: 16

Bin 4330: 2098 of cap free
Amount of items: 2
Items: 
Size: 586570 Color: 5
Size: 411333 Color: 14

Bin 4331: 2100 of cap free
Amount of items: 2
Items: 
Size: 628240 Color: 10
Size: 369661 Color: 7

Bin 4332: 2127 of cap free
Amount of items: 2
Items: 
Size: 601141 Color: 8
Size: 396733 Color: 0

Bin 4333: 2129 of cap free
Amount of items: 2
Items: 
Size: 666559 Color: 0
Size: 331313 Color: 11

Bin 4334: 2134 of cap free
Amount of items: 2
Items: 
Size: 590538 Color: 10
Size: 407329 Color: 0

Bin 4335: 2151 of cap free
Amount of items: 2
Items: 
Size: 578788 Color: 16
Size: 419062 Color: 1

Bin 4336: 2175 of cap free
Amount of items: 3
Items: 
Size: 763341 Color: 0
Size: 123262 Color: 10
Size: 111223 Color: 16

Bin 4337: 2178 of cap free
Amount of items: 2
Items: 
Size: 579537 Color: 16
Size: 418286 Color: 4

Bin 4338: 2187 of cap free
Amount of items: 2
Items: 
Size: 677851 Color: 19
Size: 319963 Color: 8

Bin 4339: 2210 of cap free
Amount of items: 2
Items: 
Size: 557415 Color: 4
Size: 440376 Color: 16

Bin 4340: 2213 of cap free
Amount of items: 2
Items: 
Size: 785219 Color: 13
Size: 212569 Color: 4

Bin 4341: 2216 of cap free
Amount of items: 2
Items: 
Size: 518498 Color: 16
Size: 479287 Color: 4

Bin 4342: 2226 of cap free
Amount of items: 2
Items: 
Size: 542327 Color: 3
Size: 455448 Color: 1

Bin 4343: 2239 of cap free
Amount of items: 2
Items: 
Size: 774934 Color: 6
Size: 222828 Color: 13

Bin 4344: 2248 of cap free
Amount of items: 2
Items: 
Size: 586636 Color: 13
Size: 411117 Color: 3

Bin 4345: 2254 of cap free
Amount of items: 2
Items: 
Size: 616002 Color: 0
Size: 381745 Color: 3

Bin 4346: 2278 of cap free
Amount of items: 2
Items: 
Size: 579487 Color: 16
Size: 418236 Color: 6

Bin 4347: 2279 of cap free
Amount of items: 2
Items: 
Size: 709735 Color: 18
Size: 287987 Color: 2

Bin 4348: 2298 of cap free
Amount of items: 3
Items: 
Size: 638809 Color: 17
Size: 230257 Color: 10
Size: 128637 Color: 1

Bin 4349: 2332 of cap free
Amount of items: 2
Items: 
Size: 766637 Color: 0
Size: 231032 Color: 15

Bin 4350: 2354 of cap free
Amount of items: 2
Items: 
Size: 579480 Color: 17
Size: 418167 Color: 16

Bin 4351: 2361 of cap free
Amount of items: 2
Items: 
Size: 593672 Color: 4
Size: 403968 Color: 8

Bin 4352: 2378 of cap free
Amount of items: 2
Items: 
Size: 590343 Color: 2
Size: 407280 Color: 11

Bin 4353: 2384 of cap free
Amount of items: 2
Items: 
Size: 549738 Color: 2
Size: 447879 Color: 10

Bin 4354: 2400 of cap free
Amount of items: 2
Items: 
Size: 500294 Color: 13
Size: 497307 Color: 1

Bin 4355: 2407 of cap free
Amount of items: 2
Items: 
Size: 586306 Color: 1
Size: 411288 Color: 13

Bin 4356: 2419 of cap free
Amount of items: 2
Items: 
Size: 518486 Color: 12
Size: 479096 Color: 1

Bin 4357: 2443 of cap free
Amount of items: 2
Items: 
Size: 590290 Color: 15
Size: 407268 Color: 0

Bin 4358: 2446 of cap free
Amount of items: 2
Items: 
Size: 590290 Color: 1
Size: 407265 Color: 8

Bin 4359: 2454 of cap free
Amount of items: 2
Items: 
Size: 579408 Color: 12
Size: 418139 Color: 14

Bin 4360: 2497 of cap free
Amount of items: 2
Items: 
Size: 739766 Color: 4
Size: 257738 Color: 7

Bin 4361: 2523 of cap free
Amount of items: 2
Items: 
Size: 518442 Color: 15
Size: 479036 Color: 10

Bin 4362: 2539 of cap free
Amount of items: 2
Items: 
Size: 646423 Color: 8
Size: 351039 Color: 15

Bin 4363: 2547 of cap free
Amount of items: 2
Items: 
Size: 590193 Color: 18
Size: 407261 Color: 7

Bin 4364: 2557 of cap free
Amount of items: 3
Items: 
Size: 728324 Color: 17
Size: 137356 Color: 19
Size: 131764 Color: 19

Bin 4365: 2575 of cap free
Amount of items: 2
Items: 
Size: 758969 Color: 14
Size: 238457 Color: 1

Bin 4366: 2605 of cap free
Amount of items: 2
Items: 
Size: 575483 Color: 8
Size: 421913 Color: 1

Bin 4367: 2650 of cap free
Amount of items: 2
Items: 
Size: 586255 Color: 16
Size: 411096 Color: 9

Bin 4368: 2662 of cap free
Amount of items: 2
Items: 
Size: 759031 Color: 4
Size: 238308 Color: 12

Bin 4369: 2672 of cap free
Amount of items: 2
Items: 
Size: 740437 Color: 19
Size: 256892 Color: 3

Bin 4370: 2675 of cap free
Amount of items: 2
Items: 
Size: 575462 Color: 2
Size: 421864 Color: 9

Bin 4371: 2701 of cap free
Amount of items: 2
Items: 
Size: 525714 Color: 1
Size: 471586 Color: 5

Bin 4372: 2703 of cap free
Amount of items: 2
Items: 
Size: 581903 Color: 8
Size: 415395 Color: 11

Bin 4373: 2718 of cap free
Amount of items: 3
Items: 
Size: 567936 Color: 8
Size: 267078 Color: 5
Size: 162269 Color: 4

Bin 4374: 2739 of cap free
Amount of items: 2
Items: 
Size: 646389 Color: 4
Size: 350873 Color: 5

Bin 4375: 2760 of cap free
Amount of items: 3
Items: 
Size: 537032 Color: 19
Size: 240159 Color: 5
Size: 220050 Color: 3

Bin 4376: 2824 of cap free
Amount of items: 2
Items: 
Size: 638864 Color: 4
Size: 358313 Color: 9

Bin 4377: 2861 of cap free
Amount of items: 2
Items: 
Size: 512672 Color: 15
Size: 484468 Color: 0

Bin 4378: 2930 of cap free
Amount of items: 2
Items: 
Size: 586302 Color: 17
Size: 410769 Color: 15

Bin 4379: 2973 of cap free
Amount of items: 2
Items: 
Size: 512658 Color: 19
Size: 484370 Color: 13

Bin 4380: 3011 of cap free
Amount of items: 2
Items: 
Size: 771873 Color: 13
Size: 225117 Color: 12

Bin 4381: 3066 of cap free
Amount of items: 2
Items: 
Size: 512596 Color: 5
Size: 484339 Color: 15

Bin 4382: 3071 of cap free
Amount of items: 2
Items: 
Size: 589723 Color: 14
Size: 407207 Color: 3

Bin 4383: 3089 of cap free
Amount of items: 2
Items: 
Size: 504693 Color: 3
Size: 492219 Color: 13

Bin 4384: 3106 of cap free
Amount of items: 2
Items: 
Size: 542117 Color: 11
Size: 454778 Color: 6

Bin 4385: 3116 of cap free
Amount of items: 2
Items: 
Size: 683364 Color: 2
Size: 313521 Color: 0

Bin 4386: 3120 of cap free
Amount of items: 2
Items: 
Size: 560133 Color: 6
Size: 436748 Color: 8

Bin 4387: 3122 of cap free
Amount of items: 2
Items: 
Size: 744402 Color: 14
Size: 252477 Color: 17

Bin 4388: 3200 of cap free
Amount of items: 2
Items: 
Size: 512514 Color: 16
Size: 484287 Color: 10

Bin 4389: 3208 of cap free
Amount of items: 2
Items: 
Size: 556504 Color: 10
Size: 440289 Color: 14

Bin 4390: 3237 of cap free
Amount of items: 2
Items: 
Size: 586020 Color: 11
Size: 410744 Color: 5

Bin 4391: 3277 of cap free
Amount of items: 2
Items: 
Size: 649765 Color: 8
Size: 346959 Color: 12

Bin 4392: 3299 of cap free
Amount of items: 2
Items: 
Size: 586158 Color: 2
Size: 410544 Color: 8

Bin 4393: 3318 of cap free
Amount of items: 2
Items: 
Size: 517676 Color: 19
Size: 479007 Color: 13

Bin 4394: 3328 of cap free
Amount of items: 2
Items: 
Size: 512418 Color: 13
Size: 484255 Color: 5

Bin 4395: 3337 of cap free
Amount of items: 2
Items: 
Size: 566944 Color: 14
Size: 429720 Color: 13

Bin 4396: 3342 of cap free
Amount of items: 2
Items: 
Size: 779048 Color: 19
Size: 217611 Color: 9

Bin 4397: 3412 of cap free
Amount of items: 2
Items: 
Size: 785476 Color: 4
Size: 211113 Color: 11

Bin 4398: 3417 of cap free
Amount of items: 2
Items: 
Size: 649898 Color: 15
Size: 346686 Color: 6

Bin 4399: 3433 of cap free
Amount of items: 3
Items: 
Size: 655042 Color: 16
Size: 172361 Color: 1
Size: 169165 Color: 8

Bin 4400: 3462 of cap free
Amount of items: 2
Items: 
Size: 649921 Color: 1
Size: 346618 Color: 17

Bin 4401: 3473 of cap free
Amount of items: 2
Items: 
Size: 798131 Color: 2
Size: 198397 Color: 5

Bin 4402: 3483 of cap free
Amount of items: 2
Items: 
Size: 645634 Color: 8
Size: 350884 Color: 3

Bin 4403: 3489 of cap free
Amount of items: 2
Items: 
Size: 758965 Color: 17
Size: 237547 Color: 11

Bin 4404: 3553 of cap free
Amount of items: 2
Items: 
Size: 681586 Color: 8
Size: 314862 Color: 4

Bin 4405: 3631 of cap free
Amount of items: 2
Items: 
Size: 775275 Color: 9
Size: 221095 Color: 1

Bin 4406: 3649 of cap free
Amount of items: 2
Items: 
Size: 575039 Color: 11
Size: 421313 Color: 7

Bin 4407: 3667 of cap free
Amount of items: 2
Items: 
Size: 645840 Color: 5
Size: 350494 Color: 14

Bin 4408: 3674 of cap free
Amount of items: 2
Items: 
Size: 585588 Color: 18
Size: 410739 Color: 2

Bin 4409: 3746 of cap free
Amount of items: 2
Items: 
Size: 574989 Color: 6
Size: 421266 Color: 5

Bin 4410: 3783 of cap free
Amount of items: 2
Items: 
Size: 607607 Color: 10
Size: 388611 Color: 12

Bin 4411: 3820 of cap free
Amount of items: 2
Items: 
Size: 524795 Color: 11
Size: 471386 Color: 4

Bin 4412: 3831 of cap free
Amount of items: 2
Items: 
Size: 774915 Color: 10
Size: 221255 Color: 4

Bin 4413: 3922 of cap free
Amount of items: 2
Items: 
Size: 524737 Color: 17
Size: 471342 Color: 15

Bin 4414: 3963 of cap free
Amount of items: 2
Items: 
Size: 524698 Color: 6
Size: 471340 Color: 0

Bin 4415: 3964 of cap free
Amount of items: 2
Items: 
Size: 517747 Color: 13
Size: 478290 Color: 0

Bin 4416: 3975 of cap free
Amount of items: 2
Items: 
Size: 579017 Color: 15
Size: 417009 Color: 7

Bin 4417: 4029 of cap free
Amount of items: 2
Items: 
Size: 585643 Color: 2
Size: 410329 Color: 13

Bin 4418: 4040 of cap free
Amount of items: 2
Items: 
Size: 599387 Color: 5
Size: 396574 Color: 3

Bin 4419: 4090 of cap free
Amount of items: 2
Items: 
Size: 622493 Color: 11
Size: 373418 Color: 2

Bin 4420: 4115 of cap free
Amount of items: 2
Items: 
Size: 517639 Color: 10
Size: 478247 Color: 8

Bin 4421: 4127 of cap free
Amount of items: 2
Items: 
Size: 585588 Color: 10
Size: 410286 Color: 6

Bin 4422: 4217 of cap free
Amount of items: 2
Items: 
Size: 574569 Color: 7
Size: 421215 Color: 19

Bin 4423: 4288 of cap free
Amount of items: 2
Items: 
Size: 797874 Color: 9
Size: 197839 Color: 16

Bin 4424: 4333 of cap free
Amount of items: 2
Items: 
Size: 740056 Color: 12
Size: 255612 Color: 9

Bin 4425: 4392 of cap free
Amount of items: 2
Items: 
Size: 560117 Color: 9
Size: 435492 Color: 10

Bin 4426: 4407 of cap free
Amount of items: 2
Items: 
Size: 517356 Color: 15
Size: 478238 Color: 14

Bin 4427: 4435 of cap free
Amount of items: 2
Items: 
Size: 517341 Color: 11
Size: 478225 Color: 7

Bin 4428: 4462 of cap free
Amount of items: 2
Items: 
Size: 776096 Color: 3
Size: 219443 Color: 4

Bin 4429: 4588 of cap free
Amount of items: 2
Items: 
Size: 797351 Color: 9
Size: 198062 Color: 18

Bin 4430: 4691 of cap free
Amount of items: 2
Items: 
Size: 512478 Color: 10
Size: 482832 Color: 17

Bin 4431: 5506 of cap free
Amount of items: 2
Items: 
Size: 497255 Color: 12
Size: 497240 Color: 1

Bin 4432: 5523 of cap free
Amount of items: 2
Items: 
Size: 776412 Color: 6
Size: 218066 Color: 1

Bin 4433: 5624 of cap free
Amount of items: 2
Items: 
Size: 497201 Color: 12
Size: 497176 Color: 14

Bin 4434: 5666 of cap free
Amount of items: 2
Items: 
Size: 497188 Color: 12
Size: 497147 Color: 0

Bin 4435: 5686 of cap free
Amount of items: 2
Items: 
Size: 637173 Color: 15
Size: 357142 Color: 1

Bin 4436: 5718 of cap free
Amount of items: 2
Items: 
Size: 597983 Color: 9
Size: 396300 Color: 5

Bin 4437: 5781 of cap free
Amount of items: 2
Items: 
Size: 597983 Color: 11
Size: 396237 Color: 6

Bin 4438: 5784 of cap free
Amount of items: 2
Items: 
Size: 796595 Color: 17
Size: 197622 Color: 10

Bin 4439: 5845 of cap free
Amount of items: 2
Items: 
Size: 597963 Color: 10
Size: 396193 Color: 19

Bin 4440: 5856 of cap free
Amount of items: 2
Items: 
Size: 503788 Color: 11
Size: 490357 Color: 17

Bin 4441: 5977 of cap free
Amount of items: 2
Items: 
Size: 517242 Color: 5
Size: 476782 Color: 12

Bin 4442: 6302 of cap free
Amount of items: 2
Items: 
Size: 597962 Color: 3
Size: 395737 Color: 17

Bin 4443: 6434 of cap free
Amount of items: 2
Items: 
Size: 642211 Color: 12
Size: 351356 Color: 9

Bin 4444: 6537 of cap free
Amount of items: 2
Items: 
Size: 556242 Color: 0
Size: 437222 Color: 9

Bin 4445: 7156 of cap free
Amount of items: 2
Items: 
Size: 620635 Color: 5
Size: 372210 Color: 11

Bin 4446: 7309 of cap free
Amount of items: 2
Items: 
Size: 585483 Color: 17
Size: 407209 Color: 14

Bin 4447: 7337 of cap free
Amount of items: 2
Items: 
Size: 596320 Color: 19
Size: 396344 Color: 0

Bin 4448: 7457 of cap free
Amount of items: 2
Items: 
Size: 620679 Color: 18
Size: 371865 Color: 1

Bin 4449: 7915 of cap free
Amount of items: 2
Items: 
Size: 774944 Color: 14
Size: 217142 Color: 11

Bin 4450: 7990 of cap free
Amount of items: 2
Items: 
Size: 520212 Color: 17
Size: 471799 Color: 5

Bin 4451: 8093 of cap free
Amount of items: 2
Items: 
Size: 556460 Color: 7
Size: 435448 Color: 19

Bin 4452: 8313 of cap free
Amount of items: 2
Items: 
Size: 681070 Color: 15
Size: 310618 Color: 12

Bin 4453: 8397 of cap free
Amount of items: 2
Items: 
Size: 795610 Color: 12
Size: 195994 Color: 11

Bin 4454: 8401 of cap free
Amount of items: 2
Items: 
Size: 584633 Color: 12
Size: 406967 Color: 18

Bin 4455: 8404 of cap free
Amount of items: 2
Items: 
Size: 795480 Color: 0
Size: 196117 Color: 9

Bin 4456: 8616 of cap free
Amount of items: 2
Items: 
Size: 687454 Color: 3
Size: 303931 Color: 15

Bin 4457: 9070 of cap free
Amount of items: 2
Items: 
Size: 558689 Color: 15
Size: 432242 Color: 12

Bin 4458: 9409 of cap free
Amount of items: 2
Items: 
Size: 795350 Color: 3
Size: 195242 Color: 2

Bin 4459: 9942 of cap free
Amount of items: 2
Items: 
Size: 771126 Color: 12
Size: 218933 Color: 1

Bin 4460: 10044 of cap free
Amount of items: 2
Items: 
Size: 556744 Color: 6
Size: 433213 Color: 7

Bin 4461: 10174 of cap free
Amount of items: 2
Items: 
Size: 792369 Color: 13
Size: 197458 Color: 5

Bin 4462: 10175 of cap free
Amount of items: 2
Items: 
Size: 620376 Color: 6
Size: 369450 Color: 3

Bin 4463: 10198 of cap free
Amount of items: 2
Items: 
Size: 556614 Color: 4
Size: 433189 Color: 6

Bin 4464: 10724 of cap free
Amount of items: 2
Items: 
Size: 556095 Color: 4
Size: 433182 Color: 7

Bin 4465: 11667 of cap free
Amount of items: 2
Items: 
Size: 793916 Color: 6
Size: 194418 Color: 4

Bin 4466: 14325 of cap free
Amount of items: 2
Items: 
Size: 579011 Color: 14
Size: 406665 Color: 18

Bin 4467: 16117 of cap free
Amount of items: 2
Items: 
Size: 793813 Color: 9
Size: 190071 Color: 18

Bin 4468: 16154 of cap free
Amount of items: 3
Items: 
Size: 661239 Color: 1
Size: 172743 Color: 8
Size: 149865 Color: 13

Bin 4469: 17152 of cap free
Amount of items: 2
Items: 
Size: 578982 Color: 4
Size: 403867 Color: 9

Bin 4470: 17236 of cap free
Amount of items: 2
Items: 
Size: 578961 Color: 17
Size: 403804 Color: 0

Bin 4471: 17344 of cap free
Amount of items: 2
Items: 
Size: 578879 Color: 3
Size: 403778 Color: 0

Bin 4472: 18829 of cap free
Amount of items: 2
Items: 
Size: 574491 Color: 0
Size: 406681 Color: 13

Bin 4473: 19871 of cap free
Amount of items: 2
Items: 
Size: 789320 Color: 16
Size: 190810 Color: 11

Bin 4474: 21822 of cap free
Amount of items: 2
Items: 
Size: 574488 Color: 8
Size: 403691 Color: 7

Bin 4475: 21948 of cap free
Amount of items: 2
Items: 
Size: 733397 Color: 6
Size: 244656 Color: 18

Bin 4476: 23933 of cap free
Amount of items: 2
Items: 
Size: 572181 Color: 10
Size: 403887 Color: 7

Bin 4477: 24914 of cap free
Amount of items: 2
Items: 
Size: 571366 Color: 15
Size: 403721 Color: 12

Bin 4478: 28741 of cap free
Amount of items: 2
Items: 
Size: 603513 Color: 11
Size: 367747 Color: 1

Bin 4479: 44452 of cap free
Amount of items: 2
Items: 
Size: 555888 Color: 9
Size: 399661 Color: 12

Bin 4480: 51253 of cap free
Amount of items: 2
Items: 
Size: 660913 Color: 6
Size: 287835 Color: 5

Total size: 4478319266
Total free space: 1685214

