Capicity Bin: 1000001
Lower Bound: 4466

Bins used: 4468
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 684136 Color: 0
Size: 168039 Color: 2
Size: 147826 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 681705 Color: 0
Size: 172330 Color: 3
Size: 145966 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 623507 Color: 1
Size: 190165 Color: 4
Size: 186329 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 583791 Color: 4
Size: 208561 Color: 0
Size: 104872 Color: 2
Size: 102777 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 679765 Color: 1
Size: 170337 Color: 0
Size: 149899 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 691825 Color: 3
Size: 195138 Color: 3
Size: 113038 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 681289 Color: 3
Size: 186399 Color: 1
Size: 132313 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 694669 Color: 2
Size: 173401 Color: 4
Size: 131931 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 793490 Color: 3
Size: 104722 Color: 1
Size: 101789 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 672428 Color: 0
Size: 177605 Color: 3
Size: 149968 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 730542 Color: 0
Size: 152338 Color: 2
Size: 117121 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 745126 Color: 3
Size: 132417 Color: 4
Size: 122458 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 678275 Color: 1
Size: 166359 Color: 3
Size: 155367 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 564294 Color: 0
Size: 247520 Color: 3
Size: 188187 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 620936 Color: 1
Size: 190468 Color: 1
Size: 188597 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 484700 Color: 2
Size: 402215 Color: 4
Size: 113086 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 681390 Color: 1
Size: 182124 Color: 3
Size: 136487 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 643540 Color: 4
Size: 204462 Color: 0
Size: 151999 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 696905 Color: 2
Size: 179478 Color: 0
Size: 123618 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 660135 Color: 2
Size: 211860 Color: 4
Size: 128006 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 351328 Color: 2
Size: 339844 Color: 3
Size: 308829 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 771453 Color: 0
Size: 124694 Color: 1
Size: 103854 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 719022 Color: 1
Size: 155542 Color: 3
Size: 125437 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 681219 Color: 1
Size: 198168 Color: 2
Size: 120614 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 671935 Color: 4
Size: 169110 Color: 1
Size: 158956 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 615763 Color: 1
Size: 259967 Color: 4
Size: 124271 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 727152 Color: 2
Size: 138031 Color: 3
Size: 134818 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 578600 Color: 0
Size: 304038 Color: 3
Size: 117363 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 630895 Color: 2
Size: 208786 Color: 0
Size: 160320 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 549218 Color: 1
Size: 277744 Color: 0
Size: 173039 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 626775 Color: 3
Size: 186862 Color: 0
Size: 186364 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 598323 Color: 3
Size: 295865 Color: 3
Size: 105813 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 652993 Color: 3
Size: 243321 Color: 0
Size: 103687 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 737846 Color: 2
Size: 143100 Color: 3
Size: 119055 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 604285 Color: 3
Size: 198182 Color: 0
Size: 197534 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 454158 Color: 4
Size: 421344 Color: 1
Size: 124499 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 646050 Color: 1
Size: 188794 Color: 2
Size: 165157 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 684531 Color: 2
Size: 185335 Color: 3
Size: 130135 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 707240 Color: 2
Size: 168401 Color: 1
Size: 124360 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 697396 Color: 2
Size: 170935 Color: 3
Size: 131670 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 548050 Color: 2
Size: 275211 Color: 4
Size: 176740 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 717988 Color: 0
Size: 163810 Color: 2
Size: 118203 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 615853 Color: 2
Size: 243627 Color: 2
Size: 140521 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 657964 Color: 2
Size: 187028 Color: 1
Size: 155009 Color: 4

Bin 45: 0 of cap free
Amount of items: 4
Items: 
Size: 472785 Color: 0
Size: 264781 Color: 3
Size: 134399 Color: 4
Size: 128036 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 621261 Color: 1
Size: 190303 Color: 2
Size: 188437 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 603485 Color: 3
Size: 198358 Color: 4
Size: 198158 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 651625 Color: 3
Size: 192726 Color: 4
Size: 155650 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 700098 Color: 2
Size: 169486 Color: 3
Size: 130417 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 714829 Color: 2
Size: 153662 Color: 0
Size: 131510 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 456012 Color: 3
Size: 311294 Color: 0
Size: 232695 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 653711 Color: 4
Size: 186888 Color: 4
Size: 159402 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 749876 Color: 0
Size: 145008 Color: 3
Size: 105117 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 743883 Color: 3
Size: 143207 Color: 2
Size: 112911 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 678703 Color: 1
Size: 162110 Color: 1
Size: 159188 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 671816 Color: 3
Size: 173138 Color: 4
Size: 155047 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 681185 Color: 2
Size: 169439 Color: 2
Size: 149377 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 591024 Color: 4
Size: 229778 Color: 0
Size: 179199 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 666936 Color: 4
Size: 201063 Color: 4
Size: 132002 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 415557 Color: 0
Size: 312251 Color: 2
Size: 272193 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 718974 Color: 1
Size: 149083 Color: 4
Size: 131944 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 613355 Color: 0
Size: 198029 Color: 3
Size: 188617 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 682774 Color: 4
Size: 175002 Color: 3
Size: 142225 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 455993 Color: 4
Size: 293425 Color: 3
Size: 250583 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 603375 Color: 2
Size: 221037 Color: 3
Size: 175589 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 649061 Color: 2
Size: 176997 Color: 4
Size: 173943 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 638280 Color: 4
Size: 184496 Color: 3
Size: 177225 Color: 4

Bin 68: 0 of cap free
Amount of items: 4
Items: 
Size: 586945 Color: 4
Size: 206186 Color: 2
Size: 106647 Color: 1
Size: 100223 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 698765 Color: 1
Size: 193121 Color: 4
Size: 108115 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 641801 Color: 4
Size: 186479 Color: 2
Size: 171721 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 698584 Color: 1
Size: 151993 Color: 3
Size: 149424 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 516143 Color: 0
Size: 242069 Color: 1
Size: 241789 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 598345 Color: 2
Size: 234877 Color: 0
Size: 166779 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 667569 Color: 1
Size: 200511 Color: 0
Size: 131921 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 654768 Color: 0
Size: 195745 Color: 2
Size: 149488 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 590274 Color: 2
Size: 234728 Color: 2
Size: 174999 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 731175 Color: 4
Size: 145595 Color: 3
Size: 123231 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 743401 Color: 0
Size: 132949 Color: 2
Size: 123651 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 653890 Color: 0
Size: 199141 Color: 3
Size: 146970 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 684593 Color: 1
Size: 166753 Color: 4
Size: 148655 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 693351 Color: 3
Size: 194087 Color: 2
Size: 112563 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 718718 Color: 1
Size: 161729 Color: 0
Size: 119554 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 749571 Color: 1
Size: 137298 Color: 4
Size: 113132 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 755554 Color: 4
Size: 123728 Color: 4
Size: 120719 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 467081 Color: 0
Size: 419554 Color: 1
Size: 113366 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 472631 Color: 1
Size: 294410 Color: 3
Size: 232960 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 467000 Color: 4
Size: 306836 Color: 2
Size: 226165 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 473286 Color: 4
Size: 286661 Color: 3
Size: 240054 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 524292 Color: 2
Size: 238055 Color: 0
Size: 237654 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 679014 Color: 4
Size: 194512 Color: 0
Size: 126475 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 466894 Color: 4
Size: 320908 Color: 2
Size: 212199 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 549480 Color: 3
Size: 252780 Color: 0
Size: 197741 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 473525 Color: 4
Size: 282555 Color: 4
Size: 243921 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 780826 Color: 3
Size: 113971 Color: 1
Size: 105204 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 570305 Color: 1
Size: 317287 Color: 3
Size: 112409 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 603699 Color: 0
Size: 255203 Color: 1
Size: 141099 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 464128 Color: 3
Size: 435760 Color: 3
Size: 100113 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 697747 Color: 4
Size: 189202 Color: 2
Size: 113052 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 720598 Color: 3
Size: 144425 Color: 2
Size: 134978 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 653837 Color: 3
Size: 187490 Color: 4
Size: 158674 Color: 0

Bin 101: 0 of cap free
Amount of items: 4
Items: 
Size: 388908 Color: 1
Size: 297891 Color: 4
Size: 162554 Color: 3
Size: 150648 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 602451 Color: 1
Size: 234556 Color: 1
Size: 162994 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 715093 Color: 1
Size: 162419 Color: 0
Size: 122489 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 666332 Color: 2
Size: 166848 Color: 4
Size: 166821 Color: 2

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 591062 Color: 1
Size: 240575 Color: 2
Size: 168364 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 690338 Color: 3
Size: 195264 Color: 2
Size: 114399 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 475829 Color: 0
Size: 277212 Color: 2
Size: 246960 Color: 0

Bin 108: 0 of cap free
Amount of items: 4
Items: 
Size: 464906 Color: 3
Size: 301249 Color: 4
Size: 118993 Color: 4
Size: 114853 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 771359 Color: 0
Size: 121308 Color: 3
Size: 107334 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 683285 Color: 1
Size: 166351 Color: 1
Size: 150365 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 762296 Color: 1
Size: 129148 Color: 2
Size: 108557 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 741568 Color: 3
Size: 135541 Color: 0
Size: 122892 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 598294 Color: 4
Size: 232420 Color: 3
Size: 169287 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 590085 Color: 2
Size: 238838 Color: 1
Size: 171078 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 645851 Color: 4
Size: 185102 Color: 4
Size: 169048 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 666996 Color: 4
Size: 174087 Color: 2
Size: 158918 Color: 2

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 682390 Color: 3
Size: 158906 Color: 3
Size: 158705 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 696844 Color: 1
Size: 158662 Color: 1
Size: 144495 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 639045 Color: 3
Size: 188893 Color: 0
Size: 172063 Color: 3

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 474717 Color: 1
Size: 415228 Color: 4
Size: 110056 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 721131 Color: 3
Size: 178694 Color: 4
Size: 100176 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 655440 Color: 0
Size: 175848 Color: 1
Size: 168713 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 590134 Color: 1
Size: 238376 Color: 3
Size: 171491 Color: 3

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 729396 Color: 4
Size: 169408 Color: 1
Size: 101197 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 745920 Color: 3
Size: 138466 Color: 4
Size: 115615 Color: 3

Bin 126: 0 of cap free
Amount of items: 4
Items: 
Size: 390478 Color: 1
Size: 293400 Color: 4
Size: 173879 Color: 0
Size: 142244 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 736635 Color: 4
Size: 147351 Color: 2
Size: 116015 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 452131 Color: 0
Size: 277565 Color: 2
Size: 270305 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 700576 Color: 1
Size: 152827 Color: 4
Size: 146598 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 464103 Color: 2
Size: 297525 Color: 1
Size: 238373 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 726362 Color: 2
Size: 155232 Color: 0
Size: 118407 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 589866 Color: 3
Size: 261431 Color: 4
Size: 148704 Color: 3

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 697046 Color: 3
Size: 152953 Color: 0
Size: 150002 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 681100 Color: 0
Size: 162878 Color: 0
Size: 156023 Color: 3

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 756846 Color: 3
Size: 131404 Color: 4
Size: 111751 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 700896 Color: 1
Size: 166838 Color: 1
Size: 132267 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 696650 Color: 3
Size: 154614 Color: 0
Size: 148737 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 782716 Color: 0
Size: 111589 Color: 3
Size: 105696 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 713904 Color: 0
Size: 143865 Color: 4
Size: 142232 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 734546 Color: 4
Size: 142620 Color: 1
Size: 122835 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 639245 Color: 3
Size: 181281 Color: 1
Size: 179475 Color: 2

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 677379 Color: 1
Size: 168205 Color: 2
Size: 154417 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 683477 Color: 0
Size: 182692 Color: 2
Size: 133832 Color: 4

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 763324 Color: 1
Size: 118995 Color: 4
Size: 117682 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 782784 Color: 1
Size: 109019 Color: 2
Size: 108198 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 717571 Color: 3
Size: 175257 Color: 2
Size: 107173 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 760520 Color: 1
Size: 139097 Color: 0
Size: 100384 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 645962 Color: 2
Size: 241686 Color: 2
Size: 112353 Color: 3

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 722001 Color: 2
Size: 162813 Color: 2
Size: 115187 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 700584 Color: 2
Size: 172808 Color: 3
Size: 126609 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 722464 Color: 4
Size: 164003 Color: 1
Size: 113534 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 733765 Color: 2
Size: 141153 Color: 2
Size: 125083 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 717532 Color: 3
Size: 173083 Color: 4
Size: 109386 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 713353 Color: 3
Size: 162320 Color: 3
Size: 124328 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 758322 Color: 4
Size: 137743 Color: 3
Size: 103936 Color: 3

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 641150 Color: 2
Size: 181045 Color: 3
Size: 177806 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 709797 Color: 1
Size: 184700 Color: 1
Size: 105504 Color: 2

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 766007 Color: 0
Size: 120913 Color: 0
Size: 113081 Color: 4

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 703809 Color: 3
Size: 156342 Color: 4
Size: 139850 Color: 2

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 700713 Color: 1
Size: 151033 Color: 0
Size: 148255 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 722053 Color: 1
Size: 142312 Color: 2
Size: 135636 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 713450 Color: 4
Size: 150100 Color: 1
Size: 136451 Color: 2

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 765612 Color: 2
Size: 122542 Color: 3
Size: 111847 Color: 4

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 667605 Color: 4
Size: 230735 Color: 4
Size: 101661 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 795756 Color: 1
Size: 103724 Color: 2
Size: 100521 Color: 3

Bin 166: 0 of cap free
Amount of items: 4
Items: 
Size: 485539 Color: 3
Size: 250230 Color: 1
Size: 141563 Color: 3
Size: 122669 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 675076 Color: 1
Size: 178383 Color: 2
Size: 146542 Color: 2

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 726815 Color: 2
Size: 137352 Color: 0
Size: 135834 Color: 4

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 738604 Color: 3
Size: 135819 Color: 3
Size: 125578 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 690905 Color: 1
Size: 172171 Color: 1
Size: 136925 Color: 2

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 764857 Color: 4
Size: 120074 Color: 1
Size: 115070 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 646835 Color: 1
Size: 198504 Color: 0
Size: 154662 Color: 2

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 721235 Color: 0
Size: 139929 Color: 0
Size: 138837 Color: 4

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 720893 Color: 2
Size: 142280 Color: 3
Size: 136828 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 746597 Color: 1
Size: 136460 Color: 3
Size: 116944 Color: 2

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 715449 Color: 3
Size: 166559 Color: 0
Size: 117993 Color: 4

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 783938 Color: 1
Size: 109471 Color: 4
Size: 106592 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 785266 Color: 3
Size: 111156 Color: 4
Size: 103579 Color: 3

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 718263 Color: 1
Size: 144279 Color: 3
Size: 137459 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 770992 Color: 1
Size: 128820 Color: 0
Size: 100189 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 778740 Color: 0
Size: 111861 Color: 4
Size: 109400 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 708942 Color: 1
Size: 188106 Color: 0
Size: 102953 Color: 0

Bin 183: 0 of cap free
Amount of items: 4
Items: 
Size: 482418 Color: 2
Size: 252864 Color: 1
Size: 133655 Color: 4
Size: 131064 Color: 2

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 464066 Color: 2
Size: 428570 Color: 4
Size: 107365 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 655481 Color: 0
Size: 172373 Color: 0
Size: 172147 Color: 1

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 666036 Color: 0
Size: 333965 Color: 2

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 700384 Color: 3
Size: 150660 Color: 1
Size: 148957 Color: 4

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 748950 Color: 1
Size: 131881 Color: 3
Size: 119170 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 787777 Color: 1
Size: 107659 Color: 3
Size: 104565 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 679811 Color: 1
Size: 192475 Color: 4
Size: 127715 Color: 3

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 772413 Color: 4
Size: 124604 Color: 1
Size: 102984 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 765575 Color: 1
Size: 122357 Color: 0
Size: 112069 Color: 4

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 655839 Color: 4
Size: 238606 Color: 3
Size: 105556 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 710336 Color: 3
Size: 144933 Color: 2
Size: 144732 Color: 2

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 722947 Color: 0
Size: 144583 Color: 0
Size: 132471 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 715225 Color: 0
Size: 147677 Color: 0
Size: 137099 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 755449 Color: 3
Size: 122299 Color: 4
Size: 122253 Color: 3

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 641973 Color: 3
Size: 207583 Color: 2
Size: 150445 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 769460 Color: 3
Size: 119896 Color: 2
Size: 110645 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 770804 Color: 0
Size: 118930 Color: 1
Size: 110267 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 680086 Color: 3
Size: 204158 Color: 2
Size: 115757 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 612149 Color: 0
Size: 286603 Color: 0
Size: 101249 Color: 3

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 718170 Color: 4
Size: 152661 Color: 4
Size: 129170 Color: 3

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 786072 Color: 0
Size: 107545 Color: 0
Size: 106384 Color: 3

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 792824 Color: 1
Size: 105799 Color: 0
Size: 101378 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 637203 Color: 3
Size: 189544 Color: 4
Size: 173254 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 660099 Color: 3
Size: 172499 Color: 4
Size: 167403 Color: 4

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 704966 Color: 3
Size: 169011 Color: 0
Size: 126024 Color: 2

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 727219 Color: 4
Size: 139544 Color: 3
Size: 133238 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 718085 Color: 3
Size: 154664 Color: 2
Size: 127252 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 714635 Color: 3
Size: 153154 Color: 2
Size: 132212 Color: 3

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 735567 Color: 3
Size: 157553 Color: 3
Size: 106881 Color: 1

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 763095 Color: 4
Size: 118946 Color: 3
Size: 117960 Color: 2

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 772329 Color: 1
Size: 119332 Color: 0
Size: 108340 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 748518 Color: 3
Size: 146301 Color: 0
Size: 105182 Color: 2

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 713977 Color: 3
Size: 156484 Color: 1
Size: 129540 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 731553 Color: 3
Size: 135207 Color: 3
Size: 133241 Color: 2

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 753905 Color: 1
Size: 126805 Color: 0
Size: 119291 Color: 1

Bin 219: 0 of cap free
Amount of items: 4
Items: 
Size: 477947 Color: 0
Size: 265056 Color: 4
Size: 136867 Color: 3
Size: 120131 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 721111 Color: 2
Size: 156191 Color: 0
Size: 122699 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 724032 Color: 0
Size: 138743 Color: 0
Size: 137226 Color: 3

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 745549 Color: 4
Size: 134568 Color: 2
Size: 119884 Color: 4

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 724176 Color: 0
Size: 156353 Color: 1
Size: 119472 Color: 4

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 659890 Color: 2
Size: 170683 Color: 1
Size: 169428 Color: 3

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 717481 Color: 3
Size: 143939 Color: 4
Size: 138581 Color: 4

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 740700 Color: 1
Size: 135226 Color: 4
Size: 124075 Color: 3

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 756926 Color: 1
Size: 126952 Color: 4
Size: 116123 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 747251 Color: 2
Size: 141347 Color: 1
Size: 111403 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 742682 Color: 3
Size: 154170 Color: 3
Size: 103149 Color: 4

Bin 230: 0 of cap free
Amount of items: 4
Items: 
Size: 424625 Color: 4
Size: 287857 Color: 4
Size: 176789 Color: 2
Size: 110730 Color: 0

Bin 231: 0 of cap free
Amount of items: 4
Items: 
Size: 506988 Color: 1
Size: 246364 Color: 4
Size: 128250 Color: 0
Size: 118399 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 713541 Color: 3
Size: 145734 Color: 0
Size: 140726 Color: 2

Bin 233: 0 of cap free
Amount of items: 4
Items: 
Size: 622799 Color: 0
Size: 129496 Color: 4
Size: 123881 Color: 0
Size: 123825 Color: 4

Bin 234: 0 of cap free
Amount of items: 4
Items: 
Size: 516328 Color: 3
Size: 241867 Color: 3
Size: 123272 Color: 4
Size: 118534 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 630722 Color: 2
Size: 184732 Color: 1
Size: 184547 Color: 2

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 700480 Color: 2
Size: 160794 Color: 4
Size: 138727 Color: 3

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 740165 Color: 3
Size: 157304 Color: 2
Size: 102532 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 701522 Color: 0
Size: 164107 Color: 3
Size: 134372 Color: 4

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 725813 Color: 2
Size: 151454 Color: 4
Size: 122734 Color: 4

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 431839 Color: 0
Size: 284852 Color: 3
Size: 283310 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 620358 Color: 4
Size: 195539 Color: 2
Size: 184104 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 762707 Color: 1
Size: 121432 Color: 1
Size: 115862 Color: 4

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 661365 Color: 2
Size: 187756 Color: 0
Size: 150880 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 702025 Color: 4
Size: 159862 Color: 1
Size: 138114 Color: 4

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 741051 Color: 4
Size: 141890 Color: 3
Size: 117060 Color: 1

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 694552 Color: 3
Size: 198947 Color: 2
Size: 106502 Color: 2

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 649488 Color: 1
Size: 219597 Color: 3
Size: 130916 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 727835 Color: 1
Size: 141033 Color: 3
Size: 131133 Color: 2

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 546051 Color: 0
Size: 227437 Color: 4
Size: 226513 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 668667 Color: 3
Size: 196467 Color: 0
Size: 134867 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 692365 Color: 2
Size: 164573 Color: 1
Size: 143063 Color: 4

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 734077 Color: 2
Size: 161569 Color: 4
Size: 104355 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 625377 Color: 4
Size: 193554 Color: 2
Size: 181070 Color: 4

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 703568 Color: 4
Size: 150101 Color: 0
Size: 146332 Color: 3

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 752472 Color: 3
Size: 131860 Color: 0
Size: 115669 Color: 3

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 729913 Color: 3
Size: 144899 Color: 1
Size: 125189 Color: 2

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 679370 Color: 4
Size: 208254 Color: 2
Size: 112377 Color: 1

Bin 258: 0 of cap free
Amount of items: 4
Items: 
Size: 502906 Color: 4
Size: 249355 Color: 1
Size: 124757 Color: 0
Size: 122983 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 749049 Color: 2
Size: 142081 Color: 3
Size: 108871 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 760576 Color: 1
Size: 130383 Color: 0
Size: 109042 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 651599 Color: 0
Size: 232082 Color: 0
Size: 116320 Color: 4

Bin 262: 0 of cap free
Amount of items: 4
Items: 
Size: 528442 Color: 0
Size: 235546 Color: 1
Size: 119604 Color: 2
Size: 116409 Color: 4

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 712143 Color: 2
Size: 143961 Color: 3
Size: 143897 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 743627 Color: 4
Size: 153202 Color: 0
Size: 103172 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 777576 Color: 2
Size: 120096 Color: 3
Size: 102329 Color: 1

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 550501 Color: 0
Size: 224995 Color: 4
Size: 224505 Color: 4

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 673604 Color: 4
Size: 196601 Color: 3
Size: 129796 Color: 3

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 694773 Color: 0
Size: 155638 Color: 1
Size: 149590 Color: 2

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 757555 Color: 4
Size: 141744 Color: 3
Size: 100702 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 692055 Color: 3
Size: 160628 Color: 1
Size: 147318 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 538094 Color: 1
Size: 248285 Color: 4
Size: 213622 Color: 2

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 772971 Color: 0
Size: 124916 Color: 1
Size: 102114 Color: 4

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 785392 Color: 4
Size: 107414 Color: 3
Size: 107195 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 745765 Color: 2
Size: 152193 Color: 0
Size: 102043 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 720750 Color: 0
Size: 144843 Color: 1
Size: 134408 Color: 2

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 759499 Color: 4
Size: 123223 Color: 4
Size: 117279 Color: 2

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 602097 Color: 1
Size: 215322 Color: 0
Size: 182582 Color: 4

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 750149 Color: 0
Size: 137118 Color: 0
Size: 112734 Color: 4

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 720570 Color: 1
Size: 141405 Color: 3
Size: 138026 Color: 3

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 761585 Color: 1
Size: 126931 Color: 3
Size: 111485 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 736813 Color: 1
Size: 162965 Color: 2
Size: 100223 Color: 2

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 673551 Color: 2
Size: 180611 Color: 2
Size: 145839 Color: 4

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 771686 Color: 1
Size: 121021 Color: 2
Size: 107294 Color: 3

Bin 284: 0 of cap free
Amount of items: 4
Items: 
Size: 523667 Color: 3
Size: 238502 Color: 0
Size: 126685 Color: 2
Size: 111147 Color: 2

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 627017 Color: 2
Size: 186556 Color: 1
Size: 186428 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 710698 Color: 4
Size: 150144 Color: 2
Size: 139159 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 729458 Color: 1
Size: 142890 Color: 1
Size: 127653 Color: 3

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 510749 Color: 0
Size: 244733 Color: 1
Size: 244519 Color: 1

Bin 289: 0 of cap free
Amount of items: 4
Items: 
Size: 534410 Color: 2
Size: 233484 Color: 0
Size: 118257 Color: 0
Size: 113850 Color: 3

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 695116 Color: 2
Size: 156254 Color: 0
Size: 148631 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 715835 Color: 0
Size: 182548 Color: 4
Size: 101618 Color: 3

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 532420 Color: 1
Size: 357532 Color: 0
Size: 110049 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 722937 Color: 4
Size: 139552 Color: 4
Size: 137512 Color: 2

Bin 294: 0 of cap free
Amount of items: 4
Items: 
Size: 530668 Color: 2
Size: 234576 Color: 4
Size: 121386 Color: 3
Size: 113371 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 738033 Color: 3
Size: 132892 Color: 4
Size: 129076 Color: 4

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 759201 Color: 3
Size: 121098 Color: 2
Size: 119702 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 733472 Color: 4
Size: 138825 Color: 3
Size: 127704 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 763021 Color: 3
Size: 131202 Color: 4
Size: 105778 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 764255 Color: 3
Size: 131801 Color: 4
Size: 103945 Color: 3

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 737437 Color: 3
Size: 155700 Color: 0
Size: 106864 Color: 4

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 769421 Color: 3
Size: 122561 Color: 1
Size: 108019 Color: 4

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 780321 Color: 4
Size: 119464 Color: 4
Size: 100216 Color: 3

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 716598 Color: 1
Size: 177801 Color: 3
Size: 105602 Color: 3

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 773799 Color: 3
Size: 125631 Color: 1
Size: 100571 Color: 2

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 649336 Color: 4
Size: 187533 Color: 0
Size: 163132 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 727417 Color: 1
Size: 146746 Color: 3
Size: 125838 Color: 3

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 704035 Color: 1
Size: 166941 Color: 1
Size: 129025 Color: 4

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 720216 Color: 0
Size: 157099 Color: 3
Size: 122686 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 649585 Color: 4
Size: 181093 Color: 2
Size: 169323 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 708852 Color: 0
Size: 150996 Color: 3
Size: 140153 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 697552 Color: 1
Size: 156466 Color: 2
Size: 145983 Color: 2

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 697756 Color: 1
Size: 151759 Color: 0
Size: 150486 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 736393 Color: 1
Size: 132927 Color: 4
Size: 130681 Color: 1

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 769844 Color: 3
Size: 121110 Color: 3
Size: 109047 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 730933 Color: 4
Size: 153878 Color: 4
Size: 115190 Color: 2

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 737895 Color: 1
Size: 143060 Color: 1
Size: 119046 Color: 4

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 693235 Color: 2
Size: 166551 Color: 3
Size: 140215 Color: 3

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 720969 Color: 1
Size: 139562 Color: 1
Size: 139470 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 745694 Color: 3
Size: 136719 Color: 1
Size: 117588 Color: 4

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 559162 Color: 2
Size: 238310 Color: 4
Size: 202529 Color: 2

Bin 321: 0 of cap free
Amount of items: 4
Items: 
Size: 541649 Color: 4
Size: 228917 Color: 4
Size: 120959 Color: 1
Size: 108476 Color: 2

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 733355 Color: 4
Size: 139022 Color: 0
Size: 127624 Color: 3

Bin 323: 0 of cap free
Amount of items: 4
Items: 
Size: 680169 Color: 1
Size: 109560 Color: 4
Size: 108593 Color: 0
Size: 101679 Color: 2

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 714533 Color: 0
Size: 151441 Color: 3
Size: 134027 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 721795 Color: 2
Size: 156978 Color: 0
Size: 121228 Color: 4

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 795384 Color: 3
Size: 104613 Color: 1
Size: 100004 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 696840 Color: 1
Size: 201118 Color: 0
Size: 102043 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 685368 Color: 0
Size: 158985 Color: 1
Size: 155648 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 718160 Color: 3
Size: 143655 Color: 2
Size: 138186 Color: 4

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 643097 Color: 3
Size: 245528 Color: 0
Size: 111376 Color: 3

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 444354 Color: 1
Size: 439038 Color: 4
Size: 116609 Color: 2

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 664765 Color: 3
Size: 335236 Color: 4

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 609077 Color: 4
Size: 196841 Color: 1
Size: 194083 Color: 1

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 737115 Color: 1
Size: 137976 Color: 1
Size: 124910 Color: 3

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 748999 Color: 1
Size: 132370 Color: 0
Size: 118632 Color: 2

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 771365 Color: 4
Size: 120996 Color: 2
Size: 107640 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 667320 Color: 2
Size: 201279 Color: 3
Size: 131402 Color: 4

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 631668 Color: 2
Size: 211431 Color: 0
Size: 156902 Color: 4

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 688807 Color: 0
Size: 156102 Color: 1
Size: 155092 Color: 3

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 747661 Color: 2
Size: 132911 Color: 2
Size: 119429 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 777908 Color: 1
Size: 111285 Color: 3
Size: 110808 Color: 2

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 769284 Color: 4
Size: 118677 Color: 3
Size: 112040 Color: 2

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 715753 Color: 0
Size: 145467 Color: 1
Size: 138781 Color: 2

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 678940 Color: 4
Size: 194010 Color: 3
Size: 127051 Color: 2

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 705816 Color: 1
Size: 160583 Color: 3
Size: 133602 Color: 3

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 747104 Color: 1
Size: 252897 Color: 2

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 690753 Color: 1
Size: 163335 Color: 0
Size: 145913 Color: 0

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 735563 Color: 2
Size: 264438 Color: 4

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 751630 Color: 2
Size: 126941 Color: 4
Size: 121430 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 589872 Color: 1
Size: 214965 Color: 3
Size: 195164 Color: 2

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 442814 Color: 0
Size: 439539 Color: 0
Size: 117648 Color: 1

Bin 352: 0 of cap free
Amount of items: 2
Items: 
Size: 760661 Color: 3
Size: 239340 Color: 4

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 777511 Color: 2
Size: 111704 Color: 0
Size: 110786 Color: 3

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 713589 Color: 3
Size: 150415 Color: 0
Size: 135997 Color: 2

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 716654 Color: 4
Size: 176682 Color: 2
Size: 106665 Color: 3

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 755180 Color: 2
Size: 130518 Color: 2
Size: 114303 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 711360 Color: 4
Size: 180446 Color: 1
Size: 108195 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 794495 Color: 4
Size: 104197 Color: 4
Size: 101309 Color: 3

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 676442 Color: 4
Size: 179317 Color: 0
Size: 144242 Color: 3

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 725805 Color: 4
Size: 147853 Color: 3
Size: 126343 Color: 4

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 557739 Color: 1
Size: 442262 Color: 4

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 784893 Color: 2
Size: 113347 Color: 3
Size: 101761 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 715400 Color: 1
Size: 181733 Color: 2
Size: 102868 Color: 2

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 759784 Color: 3
Size: 121099 Color: 1
Size: 119118 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 680272 Color: 3
Size: 204330 Color: 3
Size: 115399 Color: 1

Bin 366: 0 of cap free
Amount of items: 4
Items: 
Size: 550730 Color: 1
Size: 223778 Color: 0
Size: 115797 Color: 0
Size: 109696 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 717519 Color: 2
Size: 177336 Color: 3
Size: 105146 Color: 4

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 679493 Color: 2
Size: 213713 Color: 3
Size: 106795 Color: 3

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 546686 Color: 1
Size: 226658 Color: 0
Size: 226657 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 717648 Color: 1
Size: 150459 Color: 4
Size: 131894 Color: 3

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 742562 Color: 1
Size: 139051 Color: 4
Size: 118388 Color: 2

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 643374 Color: 0
Size: 249627 Color: 4
Size: 107000 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 719811 Color: 4
Size: 145454 Color: 0
Size: 134736 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 691816 Color: 2
Size: 206006 Color: 0
Size: 102179 Color: 3

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 783274 Color: 3
Size: 109908 Color: 4
Size: 106819 Color: 1

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 707837 Color: 0
Size: 153851 Color: 3
Size: 138313 Color: 2

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 706179 Color: 1
Size: 154161 Color: 2
Size: 139661 Color: 4

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 795611 Color: 2
Size: 103875 Color: 1
Size: 100515 Color: 2

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 695421 Color: 3
Size: 153977 Color: 2
Size: 150603 Color: 2

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 630892 Color: 0
Size: 232382 Color: 2
Size: 136727 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 766913 Color: 2
Size: 120976 Color: 1
Size: 112112 Color: 2

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 711298 Color: 3
Size: 144868 Color: 0
Size: 143835 Color: 3

Bin 383: 0 of cap free
Amount of items: 4
Items: 
Size: 558784 Color: 2
Size: 219562 Color: 2
Size: 116771 Color: 0
Size: 104884 Color: 3

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 655072 Color: 0
Size: 184923 Color: 2
Size: 160006 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 730184 Color: 1
Size: 147675 Color: 4
Size: 122142 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 759802 Color: 1
Size: 136564 Color: 0
Size: 103635 Color: 1

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 659564 Color: 0
Size: 177090 Color: 4
Size: 163347 Color: 3

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 688522 Color: 1
Size: 157962 Color: 0
Size: 153517 Color: 1

Bin 389: 0 of cap free
Amount of items: 4
Items: 
Size: 511166 Color: 2
Size: 244495 Color: 3
Size: 124830 Color: 1
Size: 119510 Color: 2

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 687643 Color: 0
Size: 158240 Color: 1
Size: 154118 Color: 4

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 744334 Color: 0
Size: 144203 Color: 1
Size: 111464 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 747978 Color: 0
Size: 136242 Color: 1
Size: 115781 Color: 2

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 798530 Color: 0
Size: 100738 Color: 0
Size: 100733 Color: 4

Bin 394: 0 of cap free
Amount of items: 2
Items: 
Size: 551540 Color: 1
Size: 448461 Color: 2

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 719996 Color: 0
Size: 148828 Color: 2
Size: 131177 Color: 1

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 628882 Color: 4
Size: 185671 Color: 1
Size: 185448 Color: 4

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 750125 Color: 3
Size: 142623 Color: 0
Size: 107253 Color: 2

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 721034 Color: 4
Size: 141014 Color: 3
Size: 137953 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 446606 Color: 1
Size: 417868 Color: 2
Size: 135527 Color: 2

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 688282 Color: 2
Size: 159990 Color: 4
Size: 151729 Color: 2

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 708724 Color: 0
Size: 173576 Color: 1
Size: 117701 Color: 0

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 549935 Color: 0
Size: 225077 Color: 1
Size: 224989 Color: 2

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 615485 Color: 0
Size: 200362 Color: 2
Size: 184154 Color: 1

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 451926 Color: 2
Size: 437933 Color: 0
Size: 110142 Color: 1

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 729093 Color: 4
Size: 146146 Color: 1
Size: 124762 Color: 3

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 535074 Color: 1
Size: 232542 Color: 0
Size: 232385 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 438917 Color: 1
Size: 436717 Color: 2
Size: 124367 Color: 0

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 683511 Color: 3
Size: 160804 Color: 1
Size: 155686 Color: 2

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 729371 Color: 2
Size: 140889 Color: 2
Size: 129741 Color: 3

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 628017 Color: 4
Size: 371984 Color: 2

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 574842 Color: 1
Size: 286167 Color: 2
Size: 138992 Color: 2

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 589420 Color: 0
Size: 278838 Color: 3
Size: 131743 Color: 3

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 743446 Color: 0
Size: 136414 Color: 3
Size: 120141 Color: 2

Bin 414: 0 of cap free
Amount of items: 4
Items: 
Size: 476772 Color: 0
Size: 270208 Color: 1
Size: 130466 Color: 2
Size: 122555 Color: 2

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 786438 Color: 4
Size: 107170 Color: 3
Size: 106393 Color: 2

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 424603 Color: 2
Size: 387060 Color: 3
Size: 188338 Color: 1

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 732896 Color: 1
Size: 141899 Color: 2
Size: 125206 Color: 2

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 679494 Color: 1
Size: 205961 Color: 4
Size: 114546 Color: 3

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 614897 Color: 2
Size: 192909 Color: 3
Size: 192195 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 679104 Color: 3
Size: 166170 Color: 1
Size: 154727 Color: 1

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 708351 Color: 0
Size: 156373 Color: 0
Size: 135277 Color: 3

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 764521 Color: 3
Size: 128605 Color: 3
Size: 106875 Color: 1

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 411467 Color: 4
Size: 297054 Color: 4
Size: 291480 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 756804 Color: 3
Size: 141801 Color: 4
Size: 101396 Color: 4

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 680192 Color: 4
Size: 207031 Color: 2
Size: 112778 Color: 3

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 672807 Color: 4
Size: 327194 Color: 2

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 732963 Color: 0
Size: 156963 Color: 0
Size: 110075 Color: 3

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 704032 Color: 3
Size: 153343 Color: 4
Size: 142626 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 718148 Color: 3
Size: 146404 Color: 4
Size: 135449 Color: 2

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 761376 Color: 4
Size: 238625 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 734089 Color: 0
Size: 165801 Color: 2
Size: 100111 Color: 3

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 697011 Color: 3
Size: 189661 Color: 1
Size: 113329 Color: 2

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 714778 Color: 3
Size: 157743 Color: 1
Size: 127480 Color: 1

Bin 434: 0 of cap free
Amount of items: 4
Items: 
Size: 521790 Color: 1
Size: 239370 Color: 0
Size: 121126 Color: 1
Size: 117715 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 667733 Color: 1
Size: 203308 Color: 3
Size: 128960 Color: 4

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 667153 Color: 2
Size: 224253 Color: 2
Size: 108595 Color: 4

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 483263 Color: 4
Size: 410257 Color: 2
Size: 106481 Color: 3

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 651208 Color: 3
Size: 232912 Color: 2
Size: 115881 Color: 3

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 731759 Color: 1
Size: 156282 Color: 4
Size: 111960 Color: 4

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 681048 Color: 0
Size: 167776 Color: 4
Size: 151177 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 646851 Color: 0
Size: 177651 Color: 0
Size: 175499 Color: 2

Bin 442: 0 of cap free
Amount of items: 2
Items: 
Size: 667693 Color: 2
Size: 332308 Color: 3

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 681512 Color: 1
Size: 177460 Color: 4
Size: 141029 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 696124 Color: 3
Size: 161051 Color: 4
Size: 142826 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 716694 Color: 1
Size: 149557 Color: 3
Size: 133750 Color: 1

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 739980 Color: 4
Size: 136201 Color: 3
Size: 123820 Color: 4

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 588779 Color: 2
Size: 240477 Color: 0
Size: 170745 Color: 2

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 391576 Color: 3
Size: 321413 Color: 1
Size: 287012 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 722290 Color: 0
Size: 144069 Color: 3
Size: 133642 Color: 2

Bin 450: 0 of cap free
Amount of items: 2
Items: 
Size: 604343 Color: 3
Size: 395658 Color: 2

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 705261 Color: 0
Size: 150119 Color: 3
Size: 144621 Color: 2

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 722874 Color: 4
Size: 149855 Color: 2
Size: 127272 Color: 4

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 521773 Color: 4
Size: 354908 Color: 0
Size: 123320 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 680287 Color: 0
Size: 164798 Color: 4
Size: 154916 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 782661 Color: 3
Size: 108945 Color: 4
Size: 108395 Color: 4

Bin 456: 0 of cap free
Amount of items: 4
Items: 
Size: 427370 Color: 2
Size: 283814 Color: 1
Size: 152078 Color: 0
Size: 136739 Color: 0

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 713989 Color: 1
Size: 152475 Color: 0
Size: 133537 Color: 1

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 511381 Color: 2
Size: 488620 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 679721 Color: 3
Size: 201329 Color: 0
Size: 118951 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 434869 Color: 1
Size: 282576 Color: 0
Size: 282556 Color: 3

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 722033 Color: 3
Size: 140848 Color: 2
Size: 137120 Color: 2

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 718940 Color: 3
Size: 147222 Color: 1
Size: 133839 Color: 1

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 469599 Color: 2
Size: 410263 Color: 3
Size: 120139 Color: 3

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 668335 Color: 2
Size: 170214 Color: 3
Size: 161452 Color: 2

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 769782 Color: 4
Size: 119807 Color: 3
Size: 110412 Color: 2

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 456021 Color: 0
Size: 442145 Color: 3
Size: 101835 Color: 1

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 666753 Color: 1
Size: 208267 Color: 3
Size: 124981 Color: 4

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 667904 Color: 0
Size: 194629 Color: 0
Size: 137468 Color: 3

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 645395 Color: 3
Size: 354606 Color: 4

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 680543 Color: 3
Size: 178749 Color: 0
Size: 140709 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 738598 Color: 3
Size: 153730 Color: 3
Size: 107673 Color: 2

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 447929 Color: 4
Size: 436645 Color: 2
Size: 115427 Color: 1

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 714402 Color: 4
Size: 163715 Color: 1
Size: 121884 Color: 4

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 743158 Color: 1
Size: 130379 Color: 2
Size: 126464 Color: 3

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 724871 Color: 1
Size: 161130 Color: 2
Size: 114000 Color: 2

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 680238 Color: 0
Size: 199419 Color: 1
Size: 120344 Color: 3

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 657787 Color: 1
Size: 176403 Color: 2
Size: 165811 Color: 4

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 440370 Color: 3
Size: 419747 Color: 2
Size: 139884 Color: 2

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 667614 Color: 3
Size: 221320 Color: 0
Size: 111067 Color: 4

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 704670 Color: 3
Size: 153794 Color: 1
Size: 141537 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 431994 Color: 1
Size: 415578 Color: 3
Size: 152429 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 737204 Color: 0
Size: 134519 Color: 1
Size: 128278 Color: 4

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 679719 Color: 1
Size: 180216 Color: 3
Size: 140066 Color: 2

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 684428 Color: 1
Size: 157930 Color: 3
Size: 157643 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 673180 Color: 2
Size: 167876 Color: 3
Size: 158945 Color: 2

Bin 486: 0 of cap free
Amount of items: 2
Items: 
Size: 597820 Color: 0
Size: 402181 Color: 3

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 680702 Color: 3
Size: 172556 Color: 1
Size: 146743 Color: 1

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 386535 Color: 2
Size: 310848 Color: 3
Size: 302618 Color: 2

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 387342 Color: 0
Size: 306871 Color: 0
Size: 305788 Color: 4

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 717889 Color: 1
Size: 282112 Color: 2

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 454509 Color: 4
Size: 444878 Color: 0
Size: 100614 Color: 3

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 452365 Color: 4
Size: 441560 Color: 0
Size: 106076 Color: 3

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 445477 Color: 1
Size: 442832 Color: 4
Size: 111692 Color: 3

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 450723 Color: 2
Size: 443457 Color: 0
Size: 105821 Color: 1

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 456027 Color: 1
Size: 441812 Color: 3
Size: 102162 Color: 3

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 441652 Color: 3
Size: 441581 Color: 2
Size: 116768 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 446959 Color: 2
Size: 439763 Color: 3
Size: 113279 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 438747 Color: 1
Size: 438520 Color: 3
Size: 122734 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 446288 Color: 4
Size: 438256 Color: 1
Size: 115457 Color: 4

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 442032 Color: 2
Size: 437621 Color: 1
Size: 120348 Color: 2

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 450075 Color: 1
Size: 437945 Color: 3
Size: 111981 Color: 4

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 438481 Color: 3
Size: 437054 Color: 3
Size: 124466 Color: 4

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 453123 Color: 0
Size: 419511 Color: 2
Size: 127367 Color: 2

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 453139 Color: 4
Size: 415002 Color: 2
Size: 131860 Color: 1

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 476466 Color: 4
Size: 378812 Color: 0
Size: 144723 Color: 4

Bin 506: 0 of cap free
Amount of items: 4
Items: 
Size: 386672 Color: 2
Size: 378550 Color: 0
Size: 131542 Color: 2
Size: 103237 Color: 3

Bin 507: 0 of cap free
Amount of items: 4
Items: 
Size: 376544 Color: 0
Size: 373752 Color: 1
Size: 140630 Color: 4
Size: 109075 Color: 1

Bin 508: 0 of cap free
Amount of items: 4
Items: 
Size: 376259 Color: 0
Size: 373811 Color: 4
Size: 130381 Color: 3
Size: 119550 Color: 1

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 378056 Color: 1
Size: 373120 Color: 3
Size: 248825 Color: 3

Bin 510: 0 of cap free
Amount of items: 4
Items: 
Size: 369626 Color: 0
Size: 363981 Color: 4
Size: 136701 Color: 4
Size: 129693 Color: 3

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 361132 Color: 2
Size: 360632 Color: 3
Size: 278237 Color: 1

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 357835 Color: 2
Size: 357210 Color: 0
Size: 284956 Color: 0

Bin 513: 0 of cap free
Amount of items: 4
Items: 
Size: 353251 Color: 4
Size: 351928 Color: 0
Size: 158133 Color: 3
Size: 136689 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 351943 Color: 2
Size: 346706 Color: 1
Size: 301352 Color: 3

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 348428 Color: 3
Size: 346773 Color: 3
Size: 304800 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 346885 Color: 0
Size: 344574 Color: 1
Size: 308542 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 345388 Color: 4
Size: 341589 Color: 3
Size: 313024 Color: 4

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 344942 Color: 4
Size: 340201 Color: 2
Size: 314858 Color: 4

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 341630 Color: 4
Size: 334206 Color: 1
Size: 324165 Color: 3

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 338706 Color: 2
Size: 334295 Color: 4
Size: 327000 Color: 4

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 336501 Color: 0
Size: 334259 Color: 3
Size: 329241 Color: 3

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 513764 Color: 0
Size: 243547 Color: 2
Size: 242690 Color: 1

Bin 523: 0 of cap free
Amount of items: 2
Items: 
Size: 516550 Color: 3
Size: 483451 Color: 1

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 526454 Color: 0
Size: 473547 Color: 3

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 536676 Color: 1
Size: 231853 Color: 2
Size: 231472 Color: 3

Bin 526: 0 of cap free
Amount of items: 2
Items: 
Size: 551441 Color: 3
Size: 448560 Color: 2

Bin 527: 0 of cap free
Amount of items: 2
Items: 
Size: 554246 Color: 4
Size: 445755 Color: 1

Bin 528: 0 of cap free
Amount of items: 2
Items: 
Size: 573626 Color: 2
Size: 426375 Color: 4

Bin 529: 0 of cap free
Amount of items: 2
Items: 
Size: 575706 Color: 3
Size: 424295 Color: 4

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 585153 Color: 4
Size: 414848 Color: 3

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 585808 Color: 4
Size: 414193 Color: 3

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 589391 Color: 3
Size: 410610 Color: 1

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 594840 Color: 3
Size: 405161 Color: 4

Bin 534: 0 of cap free
Amount of items: 2
Items: 
Size: 595383 Color: 4
Size: 404618 Color: 3

Bin 535: 0 of cap free
Amount of items: 2
Items: 
Size: 600971 Color: 3
Size: 399030 Color: 0

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 601464 Color: 1
Size: 398537 Color: 3

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 602555 Color: 0
Size: 198913 Color: 4
Size: 198533 Color: 2

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 613278 Color: 0
Size: 386723 Color: 1

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 615906 Color: 2
Size: 192546 Color: 1
Size: 191549 Color: 4

Bin 540: 0 of cap free
Amount of items: 2
Items: 
Size: 637496 Color: 1
Size: 362505 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 644057 Color: 2
Size: 179113 Color: 4
Size: 176831 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 644867 Color: 2
Size: 179642 Color: 3
Size: 175492 Color: 0

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 645927 Color: 2
Size: 177608 Color: 1
Size: 176466 Color: 4

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 646558 Color: 1
Size: 179653 Color: 2
Size: 173790 Color: 1

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 647499 Color: 0
Size: 177231 Color: 2
Size: 175271 Color: 0

Bin 546: 0 of cap free
Amount of items: 2
Items: 
Size: 655931 Color: 3
Size: 344070 Color: 2

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 658751 Color: 3
Size: 171036 Color: 2
Size: 170214 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 661582 Color: 3
Size: 169397 Color: 2
Size: 169022 Color: 4

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 668043 Color: 0
Size: 166037 Color: 3
Size: 165921 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 671109 Color: 3
Size: 164499 Color: 2
Size: 164393 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 676999 Color: 1
Size: 161866 Color: 3
Size: 161136 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 685939 Color: 1
Size: 157818 Color: 2
Size: 156244 Color: 0

Bin 553: 0 of cap free
Amount of items: 2
Items: 
Size: 691565 Color: 1
Size: 308436 Color: 4

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 704794 Color: 2
Size: 147727 Color: 4
Size: 147480 Color: 4

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 707165 Color: 2
Size: 147090 Color: 4
Size: 145746 Color: 3

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 707209 Color: 2
Size: 146849 Color: 0
Size: 145943 Color: 0

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 712846 Color: 0
Size: 287155 Color: 3

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 715089 Color: 3
Size: 142865 Color: 2
Size: 142047 Color: 0

Bin 559: 0 of cap free
Amount of items: 2
Items: 
Size: 719818 Color: 1
Size: 280183 Color: 0

Bin 560: 0 of cap free
Amount of items: 2
Items: 
Size: 745567 Color: 2
Size: 254434 Color: 3

Bin 561: 0 of cap free
Amount of items: 2
Items: 
Size: 746707 Color: 2
Size: 253294 Color: 4

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 765711 Color: 1
Size: 117411 Color: 4
Size: 116879 Color: 0

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 771742 Color: 1
Size: 115993 Color: 3
Size: 112266 Color: 3

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 772744 Color: 0
Size: 227257 Color: 4

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 772864 Color: 1
Size: 114576 Color: 0
Size: 112561 Color: 2

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 773666 Color: 1
Size: 113568 Color: 4
Size: 112767 Color: 2

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 773780 Color: 1
Size: 114524 Color: 3
Size: 111697 Color: 3

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 775738 Color: 1
Size: 112640 Color: 0
Size: 111623 Color: 0

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 779589 Color: 2
Size: 220412 Color: 0

Bin 570: 0 of cap free
Amount of items: 2
Items: 
Size: 786372 Color: 2
Size: 213629 Color: 3

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 788261 Color: 1
Size: 106006 Color: 2
Size: 105734 Color: 4

Bin 572: 0 of cap free
Amount of items: 2
Items: 
Size: 791727 Color: 3
Size: 208274 Color: 4

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 793472 Color: 1
Size: 103316 Color: 3
Size: 103213 Color: 0

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 797346 Color: 1
Size: 102446 Color: 0
Size: 100209 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 797945 Color: 0
Size: 101780 Color: 1
Size: 100276 Color: 0

Bin 576: 1 of cap free
Amount of items: 3
Items: 
Size: 604628 Color: 1
Size: 208317 Color: 4
Size: 187055 Color: 2

Bin 577: 1 of cap free
Amount of items: 3
Items: 
Size: 736747 Color: 1
Size: 139915 Color: 1
Size: 123338 Color: 2

Bin 578: 1 of cap free
Amount of items: 3
Items: 
Size: 629090 Color: 1
Size: 222393 Color: 3
Size: 148517 Color: 3

Bin 579: 1 of cap free
Amount of items: 3
Items: 
Size: 676228 Color: 0
Size: 194559 Color: 4
Size: 129213 Color: 3

Bin 580: 1 of cap free
Amount of items: 3
Items: 
Size: 687901 Color: 1
Size: 162337 Color: 1
Size: 149762 Color: 2

Bin 581: 1 of cap free
Amount of items: 3
Items: 
Size: 651725 Color: 3
Size: 200456 Color: 3
Size: 147819 Color: 0

Bin 582: 1 of cap free
Amount of items: 2
Items: 
Size: 567952 Color: 4
Size: 432048 Color: 2

Bin 583: 1 of cap free
Amount of items: 3
Items: 
Size: 641897 Color: 2
Size: 180549 Color: 0
Size: 177554 Color: 3

Bin 584: 1 of cap free
Amount of items: 3
Items: 
Size: 682038 Color: 4
Size: 189063 Color: 1
Size: 128899 Color: 1

Bin 585: 1 of cap free
Amount of items: 3
Items: 
Size: 356130 Color: 1
Size: 351948 Color: 2
Size: 291922 Color: 1

Bin 586: 1 of cap free
Amount of items: 3
Items: 
Size: 466977 Color: 1
Size: 289263 Color: 2
Size: 243760 Color: 2

Bin 587: 1 of cap free
Amount of items: 3
Items: 
Size: 644292 Color: 4
Size: 240650 Color: 2
Size: 115058 Color: 3

Bin 588: 1 of cap free
Amount of items: 3
Items: 
Size: 668772 Color: 3
Size: 195651 Color: 1
Size: 135577 Color: 2

Bin 589: 1 of cap free
Amount of items: 3
Items: 
Size: 732211 Color: 3
Size: 134022 Color: 0
Size: 133767 Color: 4

Bin 590: 1 of cap free
Amount of items: 3
Items: 
Size: 480047 Color: 2
Size: 267073 Color: 3
Size: 252880 Color: 4

Bin 591: 1 of cap free
Amount of items: 3
Items: 
Size: 467692 Color: 2
Size: 267396 Color: 3
Size: 264912 Color: 4

Bin 592: 1 of cap free
Amount of items: 3
Items: 
Size: 733085 Color: 3
Size: 133974 Color: 1
Size: 132941 Color: 1

Bin 593: 1 of cap free
Amount of items: 3
Items: 
Size: 622815 Color: 2
Size: 188954 Color: 3
Size: 188231 Color: 0

Bin 594: 1 of cap free
Amount of items: 3
Items: 
Size: 691791 Color: 0
Size: 164080 Color: 1
Size: 144129 Color: 0

Bin 595: 1 of cap free
Amount of items: 3
Items: 
Size: 411288 Color: 1
Size: 297408 Color: 1
Size: 291304 Color: 3

Bin 596: 1 of cap free
Amount of items: 3
Items: 
Size: 676549 Color: 1
Size: 163714 Color: 0
Size: 159737 Color: 2

Bin 597: 1 of cap free
Amount of items: 3
Items: 
Size: 644543 Color: 3
Size: 179778 Color: 3
Size: 175679 Color: 1

Bin 598: 1 of cap free
Amount of items: 2
Items: 
Size: 603196 Color: 3
Size: 396804 Color: 2

Bin 599: 1 of cap free
Amount of items: 3
Items: 
Size: 356525 Color: 1
Size: 355268 Color: 3
Size: 288207 Color: 2

Bin 600: 1 of cap free
Amount of items: 3
Items: 
Size: 416370 Color: 1
Size: 296855 Color: 4
Size: 286775 Color: 4

Bin 601: 1 of cap free
Amount of items: 3
Items: 
Size: 463050 Color: 4
Size: 273643 Color: 4
Size: 263307 Color: 2

Bin 602: 1 of cap free
Amount of items: 3
Items: 
Size: 784685 Color: 4
Size: 111826 Color: 0
Size: 103489 Color: 2

Bin 603: 1 of cap free
Amount of items: 3
Items: 
Size: 594306 Color: 3
Size: 228791 Color: 4
Size: 176903 Color: 3

Bin 604: 1 of cap free
Amount of items: 3
Items: 
Size: 524728 Color: 1
Size: 341638 Color: 1
Size: 133634 Color: 4

Bin 605: 1 of cap free
Amount of items: 3
Items: 
Size: 696338 Color: 1
Size: 153174 Color: 4
Size: 150488 Color: 4

Bin 606: 1 of cap free
Amount of items: 2
Items: 
Size: 794475 Color: 4
Size: 205525 Color: 2

Bin 607: 1 of cap free
Amount of items: 3
Items: 
Size: 680318 Color: 0
Size: 159938 Color: 3
Size: 159744 Color: 2

Bin 608: 1 of cap free
Amount of items: 3
Items: 
Size: 610271 Color: 0
Size: 195430 Color: 2
Size: 194299 Color: 3

Bin 609: 1 of cap free
Amount of items: 3
Items: 
Size: 619306 Color: 0
Size: 190689 Color: 4
Size: 190005 Color: 4

Bin 610: 1 of cap free
Amount of items: 2
Items: 
Size: 774153 Color: 3
Size: 225847 Color: 0

Bin 611: 1 of cap free
Amount of items: 3
Items: 
Size: 713061 Color: 2
Size: 177271 Color: 2
Size: 109668 Color: 0

Bin 612: 1 of cap free
Amount of items: 3
Items: 
Size: 629296 Color: 2
Size: 185488 Color: 4
Size: 185216 Color: 2

Bin 613: 1 of cap free
Amount of items: 3
Items: 
Size: 413332 Color: 0
Size: 296500 Color: 0
Size: 290168 Color: 2

Bin 614: 1 of cap free
Amount of items: 2
Items: 
Size: 559470 Color: 3
Size: 440530 Color: 1

Bin 615: 1 of cap free
Amount of items: 3
Items: 
Size: 654863 Color: 4
Size: 172798 Color: 4
Size: 172339 Color: 0

Bin 616: 1 of cap free
Amount of items: 3
Items: 
Size: 659600 Color: 4
Size: 170638 Color: 0
Size: 169762 Color: 1

Bin 617: 1 of cap free
Amount of items: 2
Items: 
Size: 713876 Color: 2
Size: 286124 Color: 1

Bin 618: 1 of cap free
Amount of items: 2
Items: 
Size: 613256 Color: 4
Size: 386744 Color: 2

Bin 619: 1 of cap free
Amount of items: 3
Items: 
Size: 642767 Color: 3
Size: 180206 Color: 3
Size: 177027 Color: 1

Bin 620: 1 of cap free
Amount of items: 2
Items: 
Size: 677602 Color: 2
Size: 322398 Color: 0

Bin 621: 1 of cap free
Amount of items: 3
Items: 
Size: 587056 Color: 0
Size: 206641 Color: 2
Size: 206303 Color: 0

Bin 622: 1 of cap free
Amount of items: 3
Items: 
Size: 774015 Color: 1
Size: 114530 Color: 1
Size: 111455 Color: 2

Bin 623: 1 of cap free
Amount of items: 3
Items: 
Size: 518595 Color: 1
Size: 240905 Color: 4
Size: 240500 Color: 1

Bin 624: 1 of cap free
Amount of items: 2
Items: 
Size: 507803 Color: 2
Size: 492197 Color: 0

Bin 625: 1 of cap free
Amount of items: 2
Items: 
Size: 504806 Color: 0
Size: 495194 Color: 4

Bin 626: 1 of cap free
Amount of items: 3
Items: 
Size: 650347 Color: 4
Size: 176755 Color: 4
Size: 172898 Color: 0

Bin 627: 1 of cap free
Amount of items: 3
Items: 
Size: 701158 Color: 0
Size: 157791 Color: 4
Size: 141051 Color: 1

Bin 628: 1 of cap free
Amount of items: 2
Items: 
Size: 793145 Color: 1
Size: 206855 Color: 2

Bin 629: 1 of cap free
Amount of items: 3
Items: 
Size: 589019 Color: 0
Size: 205502 Color: 3
Size: 205479 Color: 4

Bin 630: 1 of cap free
Amount of items: 3
Items: 
Size: 641272 Color: 0
Size: 182215 Color: 1
Size: 176513 Color: 3

Bin 631: 1 of cap free
Amount of items: 3
Items: 
Size: 552269 Color: 2
Size: 224039 Color: 2
Size: 223692 Color: 4

Bin 632: 1 of cap free
Amount of items: 2
Items: 
Size: 512287 Color: 2
Size: 487713 Color: 3

Bin 633: 1 of cap free
Amount of items: 2
Items: 
Size: 674022 Color: 3
Size: 325978 Color: 4

Bin 634: 1 of cap free
Amount of items: 3
Items: 
Size: 460979 Color: 0
Size: 437952 Color: 0
Size: 101069 Color: 3

Bin 635: 1 of cap free
Amount of items: 3
Items: 
Size: 669605 Color: 0
Size: 167319 Color: 2
Size: 163076 Color: 0

Bin 636: 1 of cap free
Amount of items: 3
Items: 
Size: 674652 Color: 0
Size: 162838 Color: 2
Size: 162510 Color: 0

Bin 637: 1 of cap free
Amount of items: 2
Items: 
Size: 530435 Color: 0
Size: 469565 Color: 1

Bin 638: 1 of cap free
Amount of items: 2
Items: 
Size: 547732 Color: 4
Size: 452268 Color: 2

Bin 639: 1 of cap free
Amount of items: 2
Items: 
Size: 508721 Color: 1
Size: 491279 Color: 0

Bin 640: 1 of cap free
Amount of items: 3
Items: 
Size: 679525 Color: 0
Size: 173507 Color: 0
Size: 146968 Color: 4

Bin 641: 1 of cap free
Amount of items: 3
Items: 
Size: 412044 Color: 3
Size: 294533 Color: 4
Size: 293423 Color: 3

Bin 642: 1 of cap free
Amount of items: 3
Items: 
Size: 666834 Color: 2
Size: 202700 Color: 2
Size: 130466 Color: 1

Bin 643: 1 of cap free
Amount of items: 3
Items: 
Size: 651660 Color: 0
Size: 181566 Color: 1
Size: 166774 Color: 1

Bin 644: 1 of cap free
Amount of items: 3
Items: 
Size: 680546 Color: 0
Size: 159907 Color: 0
Size: 159547 Color: 1

Bin 645: 1 of cap free
Amount of items: 2
Items: 
Size: 620426 Color: 2
Size: 379574 Color: 0

Bin 646: 1 of cap free
Amount of items: 3
Items: 
Size: 456702 Color: 1
Size: 274331 Color: 3
Size: 268967 Color: 4

Bin 647: 1 of cap free
Amount of items: 2
Items: 
Size: 610810 Color: 3
Size: 389190 Color: 1

Bin 648: 1 of cap free
Amount of items: 3
Items: 
Size: 388944 Color: 1
Size: 313199 Color: 0
Size: 297857 Color: 3

Bin 649: 1 of cap free
Amount of items: 2
Items: 
Size: 663371 Color: 4
Size: 336629 Color: 2

Bin 650: 1 of cap free
Amount of items: 3
Items: 
Size: 686729 Color: 3
Size: 170317 Color: 1
Size: 142954 Color: 2

Bin 651: 1 of cap free
Amount of items: 3
Items: 
Size: 387274 Color: 0
Size: 318904 Color: 2
Size: 293822 Color: 1

Bin 652: 1 of cap free
Amount of items: 3
Items: 
Size: 476049 Color: 4
Size: 371639 Color: 2
Size: 152312 Color: 3

Bin 653: 1 of cap free
Amount of items: 3
Items: 
Size: 345786 Color: 3
Size: 338162 Color: 0
Size: 316052 Color: 0

Bin 654: 1 of cap free
Amount of items: 2
Items: 
Size: 503973 Color: 3
Size: 496027 Color: 1

Bin 655: 1 of cap free
Amount of items: 3
Items: 
Size: 506433 Color: 1
Size: 246940 Color: 2
Size: 246627 Color: 0

Bin 656: 1 of cap free
Amount of items: 2
Items: 
Size: 509268 Color: 4
Size: 490732 Color: 1

Bin 657: 1 of cap free
Amount of items: 2
Items: 
Size: 521548 Color: 0
Size: 478452 Color: 3

Bin 658: 1 of cap free
Amount of items: 2
Items: 
Size: 546486 Color: 2
Size: 453514 Color: 4

Bin 659: 1 of cap free
Amount of items: 2
Items: 
Size: 550896 Color: 2
Size: 449104 Color: 0

Bin 660: 1 of cap free
Amount of items: 2
Items: 
Size: 556037 Color: 2
Size: 443963 Color: 3

Bin 661: 1 of cap free
Amount of items: 2
Items: 
Size: 573583 Color: 4
Size: 426417 Color: 2

Bin 662: 1 of cap free
Amount of items: 2
Items: 
Size: 576926 Color: 3
Size: 423074 Color: 4

Bin 663: 1 of cap free
Amount of items: 2
Items: 
Size: 594035 Color: 1
Size: 405965 Color: 4

Bin 664: 1 of cap free
Amount of items: 2
Items: 
Size: 596186 Color: 0
Size: 403814 Color: 4

Bin 665: 1 of cap free
Amount of items: 3
Items: 
Size: 612112 Color: 2
Size: 194229 Color: 1
Size: 193659 Color: 1

Bin 666: 1 of cap free
Amount of items: 2
Items: 
Size: 625256 Color: 0
Size: 374744 Color: 4

Bin 667: 1 of cap free
Amount of items: 2
Items: 
Size: 632126 Color: 3
Size: 367874 Color: 1

Bin 668: 1 of cap free
Amount of items: 3
Items: 
Size: 648631 Color: 2
Size: 176882 Color: 0
Size: 174487 Color: 0

Bin 669: 1 of cap free
Amount of items: 3
Items: 
Size: 655394 Color: 2
Size: 172865 Color: 0
Size: 171741 Color: 1

Bin 670: 1 of cap free
Amount of items: 2
Items: 
Size: 677703 Color: 0
Size: 322297 Color: 3

Bin 671: 1 of cap free
Amount of items: 2
Items: 
Size: 680892 Color: 2
Size: 319108 Color: 0

Bin 672: 1 of cap free
Amount of items: 2
Items: 
Size: 683952 Color: 4
Size: 316048 Color: 2

Bin 673: 1 of cap free
Amount of items: 2
Items: 
Size: 708421 Color: 1
Size: 291579 Color: 0

Bin 674: 1 of cap free
Amount of items: 2
Items: 
Size: 722821 Color: 0
Size: 277179 Color: 1

Bin 675: 1 of cap free
Amount of items: 2
Items: 
Size: 739620 Color: 1
Size: 260380 Color: 0

Bin 676: 1 of cap free
Amount of items: 2
Items: 
Size: 745166 Color: 1
Size: 254834 Color: 3

Bin 677: 1 of cap free
Amount of items: 2
Items: 
Size: 749891 Color: 0
Size: 250109 Color: 4

Bin 678: 1 of cap free
Amount of items: 2
Items: 
Size: 755200 Color: 2
Size: 244800 Color: 0

Bin 679: 1 of cap free
Amount of items: 2
Items: 
Size: 756656 Color: 4
Size: 243344 Color: 3

Bin 680: 1 of cap free
Amount of items: 3
Items: 
Size: 762902 Color: 1
Size: 119148 Color: 2
Size: 117950 Color: 0

Bin 681: 1 of cap free
Amount of items: 2
Items: 
Size: 764406 Color: 0
Size: 235594 Color: 2

Bin 682: 1 of cap free
Amount of items: 2
Items: 
Size: 771558 Color: 4
Size: 228442 Color: 2

Bin 683: 1 of cap free
Amount of items: 2
Items: 
Size: 786648 Color: 1
Size: 213352 Color: 4

Bin 684: 2 of cap free
Amount of items: 2
Items: 
Size: 562793 Color: 3
Size: 437206 Color: 2

Bin 685: 2 of cap free
Amount of items: 3
Items: 
Size: 467530 Color: 1
Size: 294649 Color: 1
Size: 237820 Color: 3

Bin 686: 2 of cap free
Amount of items: 3
Items: 
Size: 712721 Color: 0
Size: 158023 Color: 1
Size: 129255 Color: 2

Bin 687: 2 of cap free
Amount of items: 3
Items: 
Size: 351712 Color: 1
Size: 348827 Color: 4
Size: 299460 Color: 3

Bin 688: 2 of cap free
Amount of items: 3
Items: 
Size: 700959 Color: 3
Size: 151396 Color: 3
Size: 147644 Color: 2

Bin 689: 2 of cap free
Amount of items: 3
Items: 
Size: 422674 Color: 0
Size: 293560 Color: 4
Size: 283765 Color: 4

Bin 690: 2 of cap free
Amount of items: 3
Items: 
Size: 643311 Color: 4
Size: 179252 Color: 1
Size: 177436 Color: 3

Bin 691: 2 of cap free
Amount of items: 3
Items: 
Size: 633257 Color: 0
Size: 190318 Color: 0
Size: 176424 Color: 1

Bin 692: 2 of cap free
Amount of items: 3
Items: 
Size: 516175 Color: 2
Size: 242050 Color: 3
Size: 241774 Color: 3

Bin 693: 2 of cap free
Amount of items: 3
Items: 
Size: 476325 Color: 3
Size: 288426 Color: 3
Size: 235248 Color: 4

Bin 694: 2 of cap free
Amount of items: 3
Items: 
Size: 626330 Color: 2
Size: 199913 Color: 4
Size: 173756 Color: 4

Bin 695: 2 of cap free
Amount of items: 3
Items: 
Size: 616090 Color: 2
Size: 191981 Color: 0
Size: 191928 Color: 1

Bin 696: 2 of cap free
Amount of items: 3
Items: 
Size: 697363 Color: 2
Size: 157214 Color: 2
Size: 145422 Color: 1

Bin 697: 2 of cap free
Amount of items: 2
Items: 
Size: 564456 Color: 0
Size: 435543 Color: 2

Bin 698: 2 of cap free
Amount of items: 3
Items: 
Size: 631051 Color: 4
Size: 185255 Color: 2
Size: 183693 Color: 1

Bin 699: 2 of cap free
Amount of items: 3
Items: 
Size: 697236 Color: 0
Size: 183526 Color: 3
Size: 119237 Color: 4

Bin 700: 2 of cap free
Amount of items: 3
Items: 
Size: 476268 Color: 1
Size: 267389 Color: 3
Size: 256342 Color: 3

Bin 701: 2 of cap free
Amount of items: 3
Items: 
Size: 680037 Color: 2
Size: 169018 Color: 3
Size: 150944 Color: 0

Bin 702: 2 of cap free
Amount of items: 3
Items: 
Size: 591681 Color: 3
Size: 211372 Color: 3
Size: 196946 Color: 1

Bin 703: 2 of cap free
Amount of items: 3
Items: 
Size: 548326 Color: 4
Size: 239998 Color: 1
Size: 211675 Color: 2

Bin 704: 2 of cap free
Amount of items: 3
Items: 
Size: 693339 Color: 4
Size: 154894 Color: 3
Size: 151766 Color: 4

Bin 705: 2 of cap free
Amount of items: 3
Items: 
Size: 666137 Color: 2
Size: 167923 Color: 1
Size: 165939 Color: 2

Bin 706: 2 of cap free
Amount of items: 3
Items: 
Size: 391032 Color: 4
Size: 311171 Color: 3
Size: 297796 Color: 3

Bin 707: 2 of cap free
Amount of items: 2
Items: 
Size: 730101 Color: 2
Size: 269898 Color: 0

Bin 708: 2 of cap free
Amount of items: 2
Items: 
Size: 628887 Color: 2
Size: 371112 Color: 1

Bin 709: 2 of cap free
Amount of items: 3
Items: 
Size: 654157 Color: 2
Size: 185741 Color: 3
Size: 160101 Color: 0

Bin 710: 2 of cap free
Amount of items: 2
Items: 
Size: 678505 Color: 2
Size: 321494 Color: 4

Bin 711: 2 of cap free
Amount of items: 3
Items: 
Size: 624662 Color: 4
Size: 189524 Color: 2
Size: 185813 Color: 1

Bin 712: 2 of cap free
Amount of items: 2
Items: 
Size: 687732 Color: 3
Size: 312267 Color: 1

Bin 713: 2 of cap free
Amount of items: 3
Items: 
Size: 627861 Color: 4
Size: 186629 Color: 0
Size: 185509 Color: 0

Bin 714: 2 of cap free
Amount of items: 2
Items: 
Size: 654809 Color: 2
Size: 345190 Color: 3

Bin 715: 2 of cap free
Amount of items: 3
Items: 
Size: 605054 Color: 1
Size: 197811 Color: 4
Size: 197134 Color: 3

Bin 716: 2 of cap free
Amount of items: 3
Items: 
Size: 412854 Color: 3
Size: 296083 Color: 2
Size: 291062 Color: 0

Bin 717: 2 of cap free
Amount of items: 2
Items: 
Size: 743426 Color: 1
Size: 256573 Color: 0

Bin 718: 2 of cap free
Amount of items: 3
Items: 
Size: 541394 Color: 4
Size: 229749 Color: 3
Size: 228856 Color: 0

Bin 719: 2 of cap free
Amount of items: 2
Items: 
Size: 681342 Color: 3
Size: 318657 Color: 1

Bin 720: 2 of cap free
Amount of items: 3
Items: 
Size: 411842 Color: 4
Size: 300446 Color: 1
Size: 287711 Color: 1

Bin 721: 2 of cap free
Amount of items: 2
Items: 
Size: 508771 Color: 3
Size: 491228 Color: 0

Bin 722: 2 of cap free
Amount of items: 2
Items: 
Size: 550648 Color: 3
Size: 449351 Color: 0

Bin 723: 2 of cap free
Amount of items: 3
Items: 
Size: 617488 Color: 3
Size: 192625 Color: 2
Size: 189886 Color: 2

Bin 724: 2 of cap free
Amount of items: 3
Items: 
Size: 480468 Color: 0
Size: 267485 Color: 1
Size: 252046 Color: 2

Bin 725: 2 of cap free
Amount of items: 3
Items: 
Size: 333430 Color: 1
Size: 333426 Color: 2
Size: 333143 Color: 1

Bin 726: 2 of cap free
Amount of items: 2
Items: 
Size: 511472 Color: 1
Size: 488527 Color: 4

Bin 727: 2 of cap free
Amount of items: 2
Items: 
Size: 601171 Color: 3
Size: 398828 Color: 0

Bin 728: 2 of cap free
Amount of items: 2
Items: 
Size: 738976 Color: 1
Size: 261023 Color: 0

Bin 729: 2 of cap free
Amount of items: 2
Items: 
Size: 506872 Color: 2
Size: 493127 Color: 1

Bin 730: 2 of cap free
Amount of items: 3
Items: 
Size: 390092 Color: 2
Size: 308713 Color: 4
Size: 301194 Color: 1

Bin 731: 2 of cap free
Amount of items: 2
Items: 
Size: 728059 Color: 4
Size: 271940 Color: 0

Bin 732: 2 of cap free
Amount of items: 3
Items: 
Size: 386097 Color: 2
Size: 373755 Color: 0
Size: 240147 Color: 0

Bin 733: 2 of cap free
Amount of items: 3
Items: 
Size: 356353 Color: 1
Size: 352903 Color: 3
Size: 290743 Color: 4

Bin 734: 2 of cap free
Amount of items: 3
Items: 
Size: 414109 Color: 0
Size: 296942 Color: 1
Size: 288948 Color: 2

Bin 735: 2 of cap free
Amount of items: 2
Items: 
Size: 501654 Color: 3
Size: 498345 Color: 2

Bin 736: 2 of cap free
Amount of items: 2
Items: 
Size: 562679 Color: 2
Size: 437320 Color: 3

Bin 737: 2 of cap free
Amount of items: 2
Items: 
Size: 566035 Color: 3
Size: 433964 Color: 0

Bin 738: 2 of cap free
Amount of items: 2
Items: 
Size: 575981 Color: 1
Size: 424018 Color: 4

Bin 739: 2 of cap free
Amount of items: 2
Items: 
Size: 578557 Color: 3
Size: 421442 Color: 0

Bin 740: 2 of cap free
Amount of items: 2
Items: 
Size: 591711 Color: 4
Size: 408288 Color: 3

Bin 741: 2 of cap free
Amount of items: 2
Items: 
Size: 609708 Color: 3
Size: 390291 Color: 1

Bin 742: 2 of cap free
Amount of items: 3
Items: 
Size: 612138 Color: 2
Size: 194965 Color: 0
Size: 192896 Color: 1

Bin 743: 2 of cap free
Amount of items: 2
Items: 
Size: 616723 Color: 4
Size: 383276 Color: 1

Bin 744: 2 of cap free
Amount of items: 3
Items: 
Size: 640787 Color: 2
Size: 181054 Color: 1
Size: 178158 Color: 3

Bin 745: 2 of cap free
Amount of items: 2
Items: 
Size: 643245 Color: 3
Size: 356754 Color: 4

Bin 746: 2 of cap free
Amount of items: 2
Items: 
Size: 654446 Color: 0
Size: 345553 Color: 2

Bin 747: 2 of cap free
Amount of items: 2
Items: 
Size: 672409 Color: 4
Size: 327590 Color: 2

Bin 748: 2 of cap free
Amount of items: 2
Items: 
Size: 675911 Color: 3
Size: 324088 Color: 1

Bin 749: 2 of cap free
Amount of items: 3
Items: 
Size: 676306 Color: 1
Size: 161925 Color: 0
Size: 161768 Color: 2

Bin 750: 2 of cap free
Amount of items: 3
Items: 
Size: 679140 Color: 1
Size: 160533 Color: 2
Size: 160326 Color: 1

Bin 751: 2 of cap free
Amount of items: 2
Items: 
Size: 688133 Color: 2
Size: 311866 Color: 3

Bin 752: 2 of cap free
Amount of items: 3
Items: 
Size: 697575 Color: 0
Size: 152811 Color: 2
Size: 149613 Color: 0

Bin 753: 2 of cap free
Amount of items: 2
Items: 
Size: 708077 Color: 3
Size: 291922 Color: 1

Bin 754: 2 of cap free
Amount of items: 2
Items: 
Size: 726212 Color: 1
Size: 273787 Color: 3

Bin 755: 2 of cap free
Amount of items: 2
Items: 
Size: 733042 Color: 1
Size: 266957 Color: 4

Bin 756: 2 of cap free
Amount of items: 2
Items: 
Size: 747988 Color: 2
Size: 252011 Color: 4

Bin 757: 2 of cap free
Amount of items: 2
Items: 
Size: 761658 Color: 2
Size: 238341 Color: 4

Bin 758: 3 of cap free
Amount of items: 3
Items: 
Size: 624623 Color: 4
Size: 189488 Color: 2
Size: 185887 Color: 2

Bin 759: 3 of cap free
Amount of items: 3
Items: 
Size: 744631 Color: 4
Size: 137855 Color: 3
Size: 117512 Color: 3

Bin 760: 3 of cap free
Amount of items: 3
Items: 
Size: 627348 Color: 2
Size: 186644 Color: 3
Size: 186006 Color: 0

Bin 761: 3 of cap free
Amount of items: 3
Items: 
Size: 548107 Color: 4
Size: 265503 Color: 4
Size: 186388 Color: 1

Bin 762: 3 of cap free
Amount of items: 3
Items: 
Size: 652102 Color: 4
Size: 179737 Color: 4
Size: 168159 Color: 0

Bin 763: 3 of cap free
Amount of items: 3
Items: 
Size: 604402 Color: 2
Size: 198243 Color: 2
Size: 197353 Color: 1

Bin 764: 3 of cap free
Amount of items: 2
Items: 
Size: 651311 Color: 2
Size: 348687 Color: 3

Bin 765: 3 of cap free
Amount of items: 3
Items: 
Size: 477768 Color: 0
Size: 263826 Color: 1
Size: 258404 Color: 3

Bin 766: 3 of cap free
Amount of items: 3
Items: 
Size: 613879 Color: 2
Size: 193293 Color: 4
Size: 192826 Color: 3

Bin 767: 3 of cap free
Amount of items: 3
Items: 
Size: 626233 Color: 4
Size: 187012 Color: 0
Size: 186753 Color: 2

Bin 768: 3 of cap free
Amount of items: 3
Items: 
Size: 633607 Color: 1
Size: 183213 Color: 0
Size: 183178 Color: 4

Bin 769: 3 of cap free
Amount of items: 2
Items: 
Size: 604779 Color: 3
Size: 395219 Color: 1

Bin 770: 3 of cap free
Amount of items: 3
Items: 
Size: 668225 Color: 3
Size: 178888 Color: 1
Size: 152885 Color: 3

Bin 771: 3 of cap free
Amount of items: 3
Items: 
Size: 609718 Color: 2
Size: 195149 Color: 4
Size: 195131 Color: 2

Bin 772: 3 of cap free
Amount of items: 3
Items: 
Size: 392478 Color: 2
Size: 309161 Color: 2
Size: 298359 Color: 3

Bin 773: 3 of cap free
Amount of items: 3
Items: 
Size: 374567 Color: 3
Size: 370941 Color: 4
Size: 254490 Color: 4

Bin 774: 3 of cap free
Amount of items: 3
Items: 
Size: 632829 Color: 1
Size: 186830 Color: 0
Size: 180339 Color: 1

Bin 775: 3 of cap free
Amount of items: 3
Items: 
Size: 510168 Color: 2
Size: 244961 Color: 2
Size: 244869 Color: 4

Bin 776: 3 of cap free
Amount of items: 3
Items: 
Size: 638308 Color: 4
Size: 183087 Color: 0
Size: 178603 Color: 3

Bin 777: 3 of cap free
Amount of items: 3
Items: 
Size: 476081 Color: 2
Size: 373651 Color: 4
Size: 150266 Color: 0

Bin 778: 3 of cap free
Amount of items: 3
Items: 
Size: 467421 Color: 1
Size: 283451 Color: 2
Size: 249126 Color: 3

Bin 779: 3 of cap free
Amount of items: 3
Items: 
Size: 477089 Color: 2
Size: 265162 Color: 3
Size: 257747 Color: 2

Bin 780: 3 of cap free
Amount of items: 3
Items: 
Size: 648460 Color: 2
Size: 177631 Color: 2
Size: 173907 Color: 3

Bin 781: 3 of cap free
Amount of items: 3
Items: 
Size: 542340 Color: 2
Size: 228893 Color: 1
Size: 228765 Color: 1

Bin 782: 3 of cap free
Amount of items: 3
Items: 
Size: 615872 Color: 1
Size: 192106 Color: 4
Size: 192020 Color: 3

Bin 783: 3 of cap free
Amount of items: 2
Items: 
Size: 705932 Color: 0
Size: 294066 Color: 4

Bin 784: 3 of cap free
Amount of items: 3
Items: 
Size: 397875 Color: 2
Size: 304252 Color: 2
Size: 297871 Color: 0

Bin 785: 3 of cap free
Amount of items: 3
Items: 
Size: 618730 Color: 0
Size: 191831 Color: 1
Size: 189437 Color: 3

Bin 786: 3 of cap free
Amount of items: 3
Items: 
Size: 521042 Color: 0
Size: 240017 Color: 0
Size: 238939 Color: 3

Bin 787: 3 of cap free
Amount of items: 2
Items: 
Size: 759189 Color: 4
Size: 240809 Color: 1

Bin 788: 3 of cap free
Amount of items: 3
Items: 
Size: 629678 Color: 3
Size: 185543 Color: 3
Size: 184777 Color: 4

Bin 789: 3 of cap free
Amount of items: 2
Items: 
Size: 688465 Color: 4
Size: 311533 Color: 3

Bin 790: 3 of cap free
Amount of items: 2
Items: 
Size: 561838 Color: 4
Size: 438160 Color: 1

Bin 791: 3 of cap free
Amount of items: 3
Items: 
Size: 559080 Color: 3
Size: 221370 Color: 4
Size: 219548 Color: 4

Bin 792: 3 of cap free
Amount of items: 3
Items: 
Size: 603368 Color: 0
Size: 198549 Color: 4
Size: 198081 Color: 3

Bin 793: 3 of cap free
Amount of items: 2
Items: 
Size: 521952 Color: 0
Size: 478046 Color: 2

Bin 794: 3 of cap free
Amount of items: 2
Items: 
Size: 641936 Color: 2
Size: 358062 Color: 0

Bin 795: 3 of cap free
Amount of items: 3
Items: 
Size: 350632 Color: 4
Size: 324818 Color: 4
Size: 324548 Color: 0

Bin 796: 3 of cap free
Amount of items: 3
Items: 
Size: 410520 Color: 3
Size: 300589 Color: 0
Size: 288889 Color: 4

Bin 797: 3 of cap free
Amount of items: 3
Items: 
Size: 384793 Color: 1
Size: 321367 Color: 0
Size: 293838 Color: 1

Bin 798: 3 of cap free
Amount of items: 3
Items: 
Size: 546720 Color: 1
Size: 226740 Color: 3
Size: 226538 Color: 4

Bin 799: 3 of cap free
Amount of items: 2
Items: 
Size: 706776 Color: 0
Size: 293222 Color: 1

Bin 800: 3 of cap free
Amount of items: 2
Items: 
Size: 766103 Color: 1
Size: 233895 Color: 4

Bin 801: 3 of cap free
Amount of items: 2
Items: 
Size: 502439 Color: 3
Size: 497559 Color: 1

Bin 802: 3 of cap free
Amount of items: 2
Items: 
Size: 543514 Color: 0
Size: 456484 Color: 4

Bin 803: 3 of cap free
Amount of items: 3
Items: 
Size: 444835 Color: 0
Size: 442071 Color: 0
Size: 113092 Color: 2

Bin 804: 3 of cap free
Amount of items: 3
Items: 
Size: 443066 Color: 3
Size: 439009 Color: 3
Size: 117923 Color: 4

Bin 805: 3 of cap free
Amount of items: 3
Items: 
Size: 386023 Color: 2
Size: 378108 Color: 3
Size: 235867 Color: 0

Bin 806: 3 of cap free
Amount of items: 3
Items: 
Size: 358821 Color: 4
Size: 358396 Color: 0
Size: 282781 Color: 0

Bin 807: 3 of cap free
Amount of items: 3
Items: 
Size: 354923 Color: 2
Size: 340810 Color: 3
Size: 304265 Color: 4

Bin 808: 3 of cap free
Amount of items: 2
Items: 
Size: 511536 Color: 2
Size: 488462 Color: 3

Bin 809: 3 of cap free
Amount of items: 2
Items: 
Size: 513870 Color: 4
Size: 486128 Color: 1

Bin 810: 3 of cap free
Amount of items: 2
Items: 
Size: 518739 Color: 2
Size: 481259 Color: 1

Bin 811: 3 of cap free
Amount of items: 2
Items: 
Size: 528202 Color: 0
Size: 471796 Color: 3

Bin 812: 3 of cap free
Amount of items: 2
Items: 
Size: 528707 Color: 1
Size: 471291 Color: 3

Bin 813: 3 of cap free
Amount of items: 2
Items: 
Size: 538924 Color: 2
Size: 461074 Color: 4

Bin 814: 3 of cap free
Amount of items: 2
Items: 
Size: 551412 Color: 1
Size: 448586 Color: 4

Bin 815: 3 of cap free
Amount of items: 2
Items: 
Size: 558973 Color: 2
Size: 441025 Color: 4

Bin 816: 3 of cap free
Amount of items: 2
Items: 
Size: 559353 Color: 2
Size: 440645 Color: 4

Bin 817: 3 of cap free
Amount of items: 2
Items: 
Size: 586404 Color: 3
Size: 413594 Color: 0

Bin 818: 3 of cap free
Amount of items: 3
Items: 
Size: 602989 Color: 0
Size: 198554 Color: 3
Size: 198455 Color: 4

Bin 819: 3 of cap free
Amount of items: 3
Items: 
Size: 604325 Color: 2
Size: 198029 Color: 3
Size: 197644 Color: 4

Bin 820: 3 of cap free
Amount of items: 2
Items: 
Size: 605537 Color: 0
Size: 394461 Color: 4

Bin 821: 3 of cap free
Amount of items: 2
Items: 
Size: 606391 Color: 0
Size: 393607 Color: 3

Bin 822: 3 of cap free
Amount of items: 2
Items: 
Size: 612536 Color: 4
Size: 387462 Color: 3

Bin 823: 3 of cap free
Amount of items: 3
Items: 
Size: 617480 Color: 2
Size: 192573 Color: 1
Size: 189945 Color: 3

Bin 824: 3 of cap free
Amount of items: 2
Items: 
Size: 621209 Color: 4
Size: 378789 Color: 3

Bin 825: 3 of cap free
Amount of items: 2
Items: 
Size: 640679 Color: 4
Size: 359319 Color: 3

Bin 826: 3 of cap free
Amount of items: 2
Items: 
Size: 641764 Color: 1
Size: 358234 Color: 3

Bin 827: 3 of cap free
Amount of items: 2
Items: 
Size: 642881 Color: 0
Size: 357117 Color: 2

Bin 828: 3 of cap free
Amount of items: 2
Items: 
Size: 643140 Color: 3
Size: 356858 Color: 1

Bin 829: 3 of cap free
Amount of items: 2
Items: 
Size: 653935 Color: 1
Size: 346063 Color: 3

Bin 830: 3 of cap free
Amount of items: 2
Items: 
Size: 655037 Color: 0
Size: 344961 Color: 1

Bin 831: 3 of cap free
Amount of items: 2
Items: 
Size: 662324 Color: 0
Size: 337674 Color: 4

Bin 832: 3 of cap free
Amount of items: 3
Items: 
Size: 667857 Color: 0
Size: 166137 Color: 4
Size: 166004 Color: 1

Bin 833: 3 of cap free
Amount of items: 3
Items: 
Size: 672632 Color: 1
Size: 163832 Color: 3
Size: 163534 Color: 0

Bin 834: 3 of cap free
Amount of items: 2
Items: 
Size: 693521 Color: 0
Size: 306477 Color: 1

Bin 835: 3 of cap free
Amount of items: 2
Items: 
Size: 701429 Color: 0
Size: 298569 Color: 1

Bin 836: 3 of cap free
Amount of items: 2
Items: 
Size: 716605 Color: 0
Size: 283393 Color: 4

Bin 837: 3 of cap free
Amount of items: 2
Items: 
Size: 717414 Color: 3
Size: 282584 Color: 0

Bin 838: 3 of cap free
Amount of items: 2
Items: 
Size: 735557 Color: 3
Size: 264441 Color: 1

Bin 839: 3 of cap free
Amount of items: 2
Items: 
Size: 742824 Color: 2
Size: 257174 Color: 3

Bin 840: 3 of cap free
Amount of items: 2
Items: 
Size: 750669 Color: 2
Size: 249329 Color: 1

Bin 841: 3 of cap free
Amount of items: 2
Items: 
Size: 753154 Color: 2
Size: 246844 Color: 4

Bin 842: 3 of cap free
Amount of items: 2
Items: 
Size: 791305 Color: 0
Size: 208693 Color: 3

Bin 843: 4 of cap free
Amount of items: 3
Items: 
Size: 390877 Color: 2
Size: 315754 Color: 1
Size: 293366 Color: 4

Bin 844: 4 of cap free
Amount of items: 3
Items: 
Size: 387284 Color: 2
Size: 322267 Color: 3
Size: 290446 Color: 0

Bin 845: 4 of cap free
Amount of items: 3
Items: 
Size: 617391 Color: 4
Size: 192825 Color: 3
Size: 189781 Color: 3

Bin 846: 4 of cap free
Amount of items: 3
Items: 
Size: 587096 Color: 2
Size: 206791 Color: 0
Size: 206110 Color: 3

Bin 847: 4 of cap free
Amount of items: 2
Items: 
Size: 719071 Color: 0
Size: 280926 Color: 2

Bin 848: 4 of cap free
Amount of items: 3
Items: 
Size: 692957 Color: 2
Size: 155392 Color: 1
Size: 151648 Color: 3

Bin 849: 4 of cap free
Amount of items: 3
Items: 
Size: 413247 Color: 2
Size: 298364 Color: 1
Size: 288386 Color: 2

Bin 850: 4 of cap free
Amount of items: 3
Items: 
Size: 463894 Color: 3
Size: 392300 Color: 3
Size: 143803 Color: 1

Bin 851: 4 of cap free
Amount of items: 3
Items: 
Size: 603036 Color: 1
Size: 198528 Color: 3
Size: 198433 Color: 4

Bin 852: 4 of cap free
Amount of items: 3
Items: 
Size: 681142 Color: 1
Size: 161188 Color: 0
Size: 157667 Color: 1

Bin 853: 4 of cap free
Amount of items: 3
Items: 
Size: 473094 Color: 3
Size: 405489 Color: 3
Size: 121414 Color: 1

Bin 854: 4 of cap free
Amount of items: 3
Items: 
Size: 475819 Color: 1
Size: 383411 Color: 2
Size: 140767 Color: 0

Bin 855: 4 of cap free
Amount of items: 3
Items: 
Size: 470596 Color: 2
Size: 264886 Color: 3
Size: 264515 Color: 2

Bin 856: 4 of cap free
Amount of items: 3
Items: 
Size: 476471 Color: 1
Size: 288432 Color: 3
Size: 235094 Color: 4

Bin 857: 4 of cap free
Amount of items: 3
Items: 
Size: 356547 Color: 4
Size: 322003 Color: 1
Size: 321447 Color: 4

Bin 858: 4 of cap free
Amount of items: 3
Items: 
Size: 467429 Color: 0
Size: 300593 Color: 3
Size: 231975 Color: 4

Bin 859: 4 of cap free
Amount of items: 3
Items: 
Size: 602645 Color: 4
Size: 202252 Color: 2
Size: 195100 Color: 2

Bin 860: 4 of cap free
Amount of items: 3
Items: 
Size: 637733 Color: 2
Size: 181844 Color: 4
Size: 180420 Color: 0

Bin 861: 4 of cap free
Amount of items: 2
Items: 
Size: 741863 Color: 3
Size: 258134 Color: 4

Bin 862: 4 of cap free
Amount of items: 2
Items: 
Size: 690403 Color: 0
Size: 309594 Color: 1

Bin 863: 4 of cap free
Amount of items: 2
Items: 
Size: 613122 Color: 1
Size: 386875 Color: 3

Bin 864: 4 of cap free
Amount of items: 3
Items: 
Size: 710820 Color: 2
Size: 165429 Color: 2
Size: 123748 Color: 4

Bin 865: 4 of cap free
Amount of items: 2
Items: 
Size: 637535 Color: 2
Size: 362462 Color: 1

Bin 866: 4 of cap free
Amount of items: 2
Items: 
Size: 714498 Color: 0
Size: 285499 Color: 2

Bin 867: 4 of cap free
Amount of items: 3
Items: 
Size: 665509 Color: 3
Size: 167611 Color: 0
Size: 166877 Color: 4

Bin 868: 4 of cap free
Amount of items: 3
Items: 
Size: 423221 Color: 0
Size: 289843 Color: 3
Size: 286933 Color: 0

Bin 869: 4 of cap free
Amount of items: 3
Items: 
Size: 636848 Color: 1
Size: 182091 Color: 0
Size: 181058 Color: 0

Bin 870: 4 of cap free
Amount of items: 3
Items: 
Size: 553687 Color: 1
Size: 223187 Color: 4
Size: 223123 Color: 3

Bin 871: 4 of cap free
Amount of items: 3
Items: 
Size: 629532 Color: 3
Size: 185375 Color: 3
Size: 185090 Color: 0

Bin 872: 4 of cap free
Amount of items: 3
Items: 
Size: 640240 Color: 0
Size: 182890 Color: 2
Size: 176867 Color: 3

Bin 873: 4 of cap free
Amount of items: 2
Items: 
Size: 532011 Color: 2
Size: 467986 Color: 0

Bin 874: 4 of cap free
Amount of items: 2
Items: 
Size: 714581 Color: 4
Size: 285416 Color: 2

Bin 875: 4 of cap free
Amount of items: 2
Items: 
Size: 650805 Color: 4
Size: 349192 Color: 3

Bin 876: 4 of cap free
Amount of items: 2
Items: 
Size: 702260 Color: 3
Size: 297737 Color: 2

Bin 877: 4 of cap free
Amount of items: 2
Items: 
Size: 737402 Color: 0
Size: 262595 Color: 1

Bin 878: 4 of cap free
Amount of items: 2
Items: 
Size: 723812 Color: 2
Size: 276185 Color: 1

Bin 879: 4 of cap free
Amount of items: 2
Items: 
Size: 728037 Color: 2
Size: 271960 Color: 4

Bin 880: 4 of cap free
Amount of items: 3
Items: 
Size: 360764 Color: 1
Size: 358592 Color: 3
Size: 280641 Color: 3

Bin 881: 4 of cap free
Amount of items: 3
Items: 
Size: 357226 Color: 1
Size: 356723 Color: 1
Size: 286048 Color: 4

Bin 882: 4 of cap free
Amount of items: 3
Items: 
Size: 351220 Color: 1
Size: 348732 Color: 3
Size: 300045 Color: 4

Bin 883: 4 of cap free
Amount of items: 3
Items: 
Size: 341169 Color: 3
Size: 336527 Color: 4
Size: 322301 Color: 0

Bin 884: 4 of cap free
Amount of items: 3
Items: 
Size: 340946 Color: 3
Size: 334196 Color: 1
Size: 324855 Color: 3

Bin 885: 4 of cap free
Amount of items: 2
Items: 
Size: 513995 Color: 2
Size: 486002 Color: 4

Bin 886: 4 of cap free
Amount of items: 2
Items: 
Size: 565630 Color: 0
Size: 434367 Color: 3

Bin 887: 4 of cap free
Amount of items: 2
Items: 
Size: 579000 Color: 1
Size: 420997 Color: 2

Bin 888: 4 of cap free
Amount of items: 2
Items: 
Size: 582341 Color: 0
Size: 417656 Color: 4

Bin 889: 4 of cap free
Amount of items: 2
Items: 
Size: 599601 Color: 0
Size: 400396 Color: 1

Bin 890: 4 of cap free
Amount of items: 2
Items: 
Size: 603855 Color: 3
Size: 396142 Color: 1

Bin 891: 4 of cap free
Amount of items: 3
Items: 
Size: 604730 Color: 3
Size: 198027 Color: 2
Size: 197240 Color: 0

Bin 892: 4 of cap free
Amount of items: 2
Items: 
Size: 613729 Color: 1
Size: 386268 Color: 0

Bin 893: 4 of cap free
Amount of items: 3
Items: 
Size: 616804 Color: 2
Size: 193194 Color: 4
Size: 189999 Color: 3

Bin 894: 4 of cap free
Amount of items: 3
Items: 
Size: 645181 Color: 2
Size: 179077 Color: 0
Size: 175739 Color: 1

Bin 895: 4 of cap free
Amount of items: 3
Items: 
Size: 653493 Color: 2
Size: 174200 Color: 4
Size: 172304 Color: 0

Bin 896: 4 of cap free
Amount of items: 2
Items: 
Size: 655094 Color: 1
Size: 344903 Color: 0

Bin 897: 4 of cap free
Amount of items: 2
Items: 
Size: 664435 Color: 1
Size: 335562 Color: 0

Bin 898: 4 of cap free
Amount of items: 3
Items: 
Size: 669023 Color: 1
Size: 165585 Color: 4
Size: 165389 Color: 4

Bin 899: 4 of cap free
Amount of items: 2
Items: 
Size: 683197 Color: 0
Size: 316800 Color: 2

Bin 900: 4 of cap free
Amount of items: 2
Items: 
Size: 684398 Color: 3
Size: 315599 Color: 0

Bin 901: 4 of cap free
Amount of items: 2
Items: 
Size: 717091 Color: 3
Size: 282906 Color: 0

Bin 902: 4 of cap free
Amount of items: 2
Items: 
Size: 730670 Color: 4
Size: 269327 Color: 0

Bin 903: 4 of cap free
Amount of items: 2
Items: 
Size: 775989 Color: 2
Size: 224008 Color: 4

Bin 904: 4 of cap free
Amount of items: 2
Items: 
Size: 793391 Color: 4
Size: 206606 Color: 3

Bin 905: 4 of cap free
Amount of items: 2
Items: 
Size: 793809 Color: 3
Size: 206188 Color: 0

Bin 906: 5 of cap free
Amount of items: 3
Items: 
Size: 688972 Color: 4
Size: 156673 Color: 1
Size: 154351 Color: 3

Bin 907: 5 of cap free
Amount of items: 3
Items: 
Size: 676201 Color: 0
Size: 167243 Color: 1
Size: 156552 Color: 4

Bin 908: 5 of cap free
Amount of items: 3
Items: 
Size: 470665 Color: 2
Size: 265682 Color: 1
Size: 263649 Color: 4

Bin 909: 5 of cap free
Amount of items: 3
Items: 
Size: 514288 Color: 0
Size: 243474 Color: 2
Size: 242234 Color: 3

Bin 910: 5 of cap free
Amount of items: 3
Items: 
Size: 694970 Color: 2
Size: 153239 Color: 2
Size: 151787 Color: 4

Bin 911: 5 of cap free
Amount of items: 3
Items: 
Size: 480804 Color: 4
Size: 259854 Color: 3
Size: 259338 Color: 4

Bin 912: 5 of cap free
Amount of items: 3
Items: 
Size: 686393 Color: 3
Size: 157372 Color: 1
Size: 156231 Color: 3

Bin 913: 5 of cap free
Amount of items: 3
Items: 
Size: 392943 Color: 2
Size: 308233 Color: 4
Size: 298820 Color: 3

Bin 914: 5 of cap free
Amount of items: 3
Items: 
Size: 473386 Color: 2
Size: 386624 Color: 3
Size: 139986 Color: 1

Bin 915: 5 of cap free
Amount of items: 3
Items: 
Size: 652476 Color: 4
Size: 178611 Color: 0
Size: 168909 Color: 2

Bin 916: 5 of cap free
Amount of items: 2
Items: 
Size: 621101 Color: 1
Size: 378895 Color: 2

Bin 917: 5 of cap free
Amount of items: 3
Items: 
Size: 410747 Color: 1
Size: 300794 Color: 3
Size: 288455 Color: 3

Bin 918: 5 of cap free
Amount of items: 3
Items: 
Size: 627553 Color: 3
Size: 186303 Color: 2
Size: 186140 Color: 2

Bin 919: 5 of cap free
Amount of items: 3
Items: 
Size: 613444 Color: 4
Size: 193501 Color: 3
Size: 193051 Color: 1

Bin 920: 5 of cap free
Amount of items: 3
Items: 
Size: 612065 Color: 2
Size: 194458 Color: 0
Size: 193473 Color: 2

Bin 921: 5 of cap free
Amount of items: 3
Items: 
Size: 586619 Color: 4
Size: 232174 Color: 3
Size: 181203 Color: 0

Bin 922: 5 of cap free
Amount of items: 2
Items: 
Size: 754521 Color: 0
Size: 245475 Color: 1

Bin 923: 5 of cap free
Amount of items: 2
Items: 
Size: 769184 Color: 1
Size: 230812 Color: 4

Bin 924: 5 of cap free
Amount of items: 3
Items: 
Size: 586137 Color: 4
Size: 207431 Color: 2
Size: 206428 Color: 4

Bin 925: 5 of cap free
Amount of items: 3
Items: 
Size: 421458 Color: 3
Size: 387374 Color: 4
Size: 191164 Color: 2

Bin 926: 5 of cap free
Amount of items: 2
Items: 
Size: 734244 Color: 1
Size: 265752 Color: 3

Bin 927: 5 of cap free
Amount of items: 2
Items: 
Size: 637371 Color: 1
Size: 362625 Color: 2

Bin 928: 5 of cap free
Amount of items: 2
Items: 
Size: 587385 Color: 3
Size: 412611 Color: 2

Bin 929: 5 of cap free
Amount of items: 2
Items: 
Size: 576256 Color: 3
Size: 423740 Color: 2

Bin 930: 5 of cap free
Amount of items: 2
Items: 
Size: 675363 Color: 0
Size: 324633 Color: 2

Bin 931: 5 of cap free
Amount of items: 2
Items: 
Size: 653114 Color: 4
Size: 346882 Color: 3

Bin 932: 5 of cap free
Amount of items: 2
Items: 
Size: 510912 Color: 1
Size: 489084 Color: 2

Bin 933: 5 of cap free
Amount of items: 2
Items: 
Size: 540724 Color: 3
Size: 459272 Color: 0

Bin 934: 5 of cap free
Amount of items: 3
Items: 
Size: 352914 Color: 1
Size: 351414 Color: 3
Size: 295668 Color: 3

Bin 935: 5 of cap free
Amount of items: 3
Items: 
Size: 352790 Color: 1
Size: 349154 Color: 2
Size: 298052 Color: 3

Bin 936: 5 of cap free
Amount of items: 2
Items: 
Size: 533905 Color: 4
Size: 466091 Color: 3

Bin 937: 5 of cap free
Amount of items: 2
Items: 
Size: 565244 Color: 4
Size: 434752 Color: 0

Bin 938: 5 of cap free
Amount of items: 2
Items: 
Size: 588820 Color: 1
Size: 411176 Color: 0

Bin 939: 5 of cap free
Amount of items: 2
Items: 
Size: 595253 Color: 1
Size: 404743 Color: 4

Bin 940: 5 of cap free
Amount of items: 2
Items: 
Size: 606422 Color: 2
Size: 393574 Color: 1

Bin 941: 5 of cap free
Amount of items: 3
Items: 
Size: 611426 Color: 2
Size: 194582 Color: 0
Size: 193988 Color: 1

Bin 942: 5 of cap free
Amount of items: 2
Items: 
Size: 612951 Color: 3
Size: 387045 Color: 0

Bin 943: 5 of cap free
Amount of items: 2
Items: 
Size: 624597 Color: 3
Size: 375399 Color: 0

Bin 944: 5 of cap free
Amount of items: 2
Items: 
Size: 642626 Color: 0
Size: 357370 Color: 2

Bin 945: 5 of cap free
Amount of items: 2
Items: 
Size: 645246 Color: 3
Size: 354750 Color: 4

Bin 946: 5 of cap free
Amount of items: 2
Items: 
Size: 645904 Color: 1
Size: 354092 Color: 4

Bin 947: 5 of cap free
Amount of items: 2
Items: 
Size: 657660 Color: 4
Size: 342336 Color: 3

Bin 948: 5 of cap free
Amount of items: 2
Items: 
Size: 668566 Color: 2
Size: 331430 Color: 0

Bin 949: 5 of cap free
Amount of items: 2
Items: 
Size: 675349 Color: 0
Size: 324647 Color: 4

Bin 950: 5 of cap free
Amount of items: 2
Items: 
Size: 683222 Color: 4
Size: 316774 Color: 3

Bin 951: 5 of cap free
Amount of items: 2
Items: 
Size: 692798 Color: 3
Size: 307198 Color: 0

Bin 952: 5 of cap free
Amount of items: 2
Items: 
Size: 692879 Color: 4
Size: 307117 Color: 1

Bin 953: 5 of cap free
Amount of items: 2
Items: 
Size: 717545 Color: 4
Size: 282451 Color: 1

Bin 954: 5 of cap free
Amount of items: 2
Items: 
Size: 723201 Color: 0
Size: 276795 Color: 3

Bin 955: 5 of cap free
Amount of items: 2
Items: 
Size: 729652 Color: 3
Size: 270344 Color: 1

Bin 956: 5 of cap free
Amount of items: 2
Items: 
Size: 744333 Color: 4
Size: 255663 Color: 0

Bin 957: 5 of cap free
Amount of items: 2
Items: 
Size: 745274 Color: 1
Size: 254722 Color: 4

Bin 958: 5 of cap free
Amount of items: 2
Items: 
Size: 760244 Color: 0
Size: 239752 Color: 4

Bin 959: 6 of cap free
Amount of items: 3
Items: 
Size: 668885 Color: 1
Size: 165643 Color: 1
Size: 165467 Color: 0

Bin 960: 6 of cap free
Amount of items: 3
Items: 
Size: 504198 Color: 0
Size: 248610 Color: 4
Size: 247187 Color: 3

Bin 961: 6 of cap free
Amount of items: 3
Items: 
Size: 681969 Color: 3
Size: 162127 Color: 2
Size: 155899 Color: 0

Bin 962: 6 of cap free
Amount of items: 2
Items: 
Size: 776613 Color: 4
Size: 223382 Color: 2

Bin 963: 6 of cap free
Amount of items: 2
Items: 
Size: 520565 Color: 3
Size: 479430 Color: 1

Bin 964: 6 of cap free
Amount of items: 3
Items: 
Size: 631912 Color: 2
Size: 184076 Color: 0
Size: 184007 Color: 1

Bin 965: 6 of cap free
Amount of items: 3
Items: 
Size: 782567 Color: 4
Size: 109365 Color: 2
Size: 108063 Color: 0

Bin 966: 6 of cap free
Amount of items: 3
Items: 
Size: 456508 Color: 3
Size: 358144 Color: 4
Size: 185343 Color: 3

Bin 967: 6 of cap free
Amount of items: 3
Items: 
Size: 686515 Color: 0
Size: 160357 Color: 3
Size: 153123 Color: 2

Bin 968: 6 of cap free
Amount of items: 3
Items: 
Size: 480044 Color: 0
Size: 263542 Color: 3
Size: 256409 Color: 4

Bin 969: 6 of cap free
Amount of items: 3
Items: 
Size: 376504 Color: 2
Size: 370924 Color: 3
Size: 252567 Color: 2

Bin 970: 6 of cap free
Amount of items: 3
Items: 
Size: 650222 Color: 2
Size: 176866 Color: 1
Size: 172907 Color: 4

Bin 971: 6 of cap free
Amount of items: 3
Items: 
Size: 713658 Color: 0
Size: 170922 Color: 2
Size: 115415 Color: 1

Bin 972: 6 of cap free
Amount of items: 3
Items: 
Size: 640537 Color: 2
Size: 179811 Color: 0
Size: 179647 Color: 4

Bin 973: 6 of cap free
Amount of items: 3
Items: 
Size: 655086 Color: 0
Size: 173495 Color: 2
Size: 171414 Color: 3

Bin 974: 6 of cap free
Amount of items: 2
Items: 
Size: 750252 Color: 1
Size: 249743 Color: 3

Bin 975: 6 of cap free
Amount of items: 3
Items: 
Size: 504255 Color: 1
Size: 351812 Color: 4
Size: 143928 Color: 0

Bin 976: 6 of cap free
Amount of items: 3
Items: 
Size: 480175 Color: 1
Size: 389655 Color: 2
Size: 130165 Color: 0

Bin 977: 6 of cap free
Amount of items: 3
Items: 
Size: 529706 Color: 4
Size: 235299 Color: 4
Size: 234990 Color: 3

Bin 978: 6 of cap free
Amount of items: 3
Items: 
Size: 358102 Color: 2
Size: 321303 Color: 0
Size: 320590 Color: 1

Bin 979: 6 of cap free
Amount of items: 2
Items: 
Size: 745640 Color: 0
Size: 254355 Color: 2

Bin 980: 6 of cap free
Amount of items: 2
Items: 
Size: 561490 Color: 4
Size: 438505 Color: 2

Bin 981: 6 of cap free
Amount of items: 2
Items: 
Size: 798082 Color: 4
Size: 201913 Color: 2

Bin 982: 6 of cap free
Amount of items: 2
Items: 
Size: 583075 Color: 4
Size: 416920 Color: 3

Bin 983: 6 of cap free
Amount of items: 2
Items: 
Size: 685293 Color: 3
Size: 314702 Color: 1

Bin 984: 6 of cap free
Amount of items: 2
Items: 
Size: 507754 Color: 1
Size: 492241 Color: 4

Bin 985: 6 of cap free
Amount of items: 3
Items: 
Size: 631429 Color: 1
Size: 184603 Color: 0
Size: 183963 Color: 4

Bin 986: 6 of cap free
Amount of items: 2
Items: 
Size: 647050 Color: 3
Size: 352945 Color: 4

Bin 987: 6 of cap free
Amount of items: 3
Items: 
Size: 625185 Color: 1
Size: 187586 Color: 3
Size: 187224 Color: 3

Bin 988: 6 of cap free
Amount of items: 2
Items: 
Size: 604011 Color: 3
Size: 395984 Color: 0

Bin 989: 6 of cap free
Amount of items: 2
Items: 
Size: 698259 Color: 2
Size: 301736 Color: 0

Bin 990: 6 of cap free
Amount of items: 2
Items: 
Size: 758722 Color: 1
Size: 241273 Color: 2

Bin 991: 6 of cap free
Amount of items: 2
Items: 
Size: 518582 Color: 4
Size: 481413 Color: 0

Bin 992: 6 of cap free
Amount of items: 2
Items: 
Size: 734332 Color: 2
Size: 265663 Color: 0

Bin 993: 6 of cap free
Amount of items: 2
Items: 
Size: 508109 Color: 3
Size: 491886 Color: 0

Bin 994: 6 of cap free
Amount of items: 2
Items: 
Size: 655131 Color: 1
Size: 344864 Color: 4

Bin 995: 6 of cap free
Amount of items: 2
Items: 
Size: 604040 Color: 2
Size: 395955 Color: 3

Bin 996: 6 of cap free
Amount of items: 2
Items: 
Size: 560424 Color: 3
Size: 439571 Color: 4

Bin 997: 6 of cap free
Amount of items: 2
Items: 
Size: 529677 Color: 2
Size: 470318 Color: 3

Bin 998: 6 of cap free
Amount of items: 2
Items: 
Size: 734789 Color: 4
Size: 265206 Color: 2

Bin 999: 6 of cap free
Amount of items: 2
Items: 
Size: 747538 Color: 1
Size: 252457 Color: 0

Bin 1000: 6 of cap free
Amount of items: 3
Items: 
Size: 439135 Color: 3
Size: 439049 Color: 3
Size: 121811 Color: 4

Bin 1001: 6 of cap free
Amount of items: 3
Items: 
Size: 361069 Color: 1
Size: 358623 Color: 1
Size: 280303 Color: 4

Bin 1002: 6 of cap free
Amount of items: 3
Items: 
Size: 351544 Color: 2
Size: 344346 Color: 2
Size: 304105 Color: 1

Bin 1003: 6 of cap free
Amount of items: 3
Items: 
Size: 345307 Color: 3
Size: 337065 Color: 3
Size: 317623 Color: 0

Bin 1004: 6 of cap free
Amount of items: 2
Items: 
Size: 514726 Color: 1
Size: 485269 Color: 2

Bin 1005: 6 of cap free
Amount of items: 3
Items: 
Size: 524895 Color: 4
Size: 237803 Color: 0
Size: 237297 Color: 3

Bin 1006: 6 of cap free
Amount of items: 2
Items: 
Size: 528388 Color: 4
Size: 471607 Color: 3

Bin 1007: 6 of cap free
Amount of items: 2
Items: 
Size: 545304 Color: 2
Size: 454691 Color: 0

Bin 1008: 6 of cap free
Amount of items: 2
Items: 
Size: 562123 Color: 3
Size: 437872 Color: 2

Bin 1009: 6 of cap free
Amount of items: 2
Items: 
Size: 579776 Color: 2
Size: 420219 Color: 3

Bin 1010: 6 of cap free
Amount of items: 2
Items: 
Size: 597769 Color: 3
Size: 402226 Color: 1

Bin 1011: 6 of cap free
Amount of items: 2
Items: 
Size: 599293 Color: 0
Size: 400702 Color: 3

Bin 1012: 6 of cap free
Amount of items: 3
Items: 
Size: 617985 Color: 2
Size: 191862 Color: 1
Size: 190148 Color: 0

Bin 1013: 6 of cap free
Amount of items: 2
Items: 
Size: 623517 Color: 4
Size: 376478 Color: 1

Bin 1014: 6 of cap free
Amount of items: 2
Items: 
Size: 625568 Color: 3
Size: 374427 Color: 1

Bin 1015: 6 of cap free
Amount of items: 3
Items: 
Size: 628683 Color: 4
Size: 185847 Color: 0
Size: 185465 Color: 0

Bin 1016: 6 of cap free
Amount of items: 2
Items: 
Size: 637298 Color: 3
Size: 362697 Color: 0

Bin 1017: 6 of cap free
Amount of items: 2
Items: 
Size: 655776 Color: 0
Size: 344219 Color: 1

Bin 1018: 6 of cap free
Amount of items: 2
Items: 
Size: 663072 Color: 2
Size: 336923 Color: 1

Bin 1019: 6 of cap free
Amount of items: 2
Items: 
Size: 678135 Color: 0
Size: 321860 Color: 4

Bin 1020: 6 of cap free
Amount of items: 2
Items: 
Size: 681443 Color: 2
Size: 318552 Color: 3

Bin 1021: 6 of cap free
Amount of items: 2
Items: 
Size: 682619 Color: 2
Size: 317376 Color: 4

Bin 1022: 6 of cap free
Amount of items: 2
Items: 
Size: 718264 Color: 4
Size: 281731 Color: 1

Bin 1023: 6 of cap free
Amount of items: 2
Items: 
Size: 719272 Color: 3
Size: 280723 Color: 1

Bin 1024: 6 of cap free
Amount of items: 2
Items: 
Size: 722537 Color: 1
Size: 277458 Color: 3

Bin 1025: 6 of cap free
Amount of items: 2
Items: 
Size: 725181 Color: 0
Size: 274814 Color: 1

Bin 1026: 6 of cap free
Amount of items: 2
Items: 
Size: 727270 Color: 1
Size: 272725 Color: 0

Bin 1027: 6 of cap free
Amount of items: 2
Items: 
Size: 733085 Color: 1
Size: 266910 Color: 2

Bin 1028: 6 of cap free
Amount of items: 2
Items: 
Size: 771998 Color: 0
Size: 227997 Color: 4

Bin 1029: 6 of cap free
Amount of items: 2
Items: 
Size: 776648 Color: 3
Size: 223347 Color: 2

Bin 1030: 6 of cap free
Amount of items: 2
Items: 
Size: 777876 Color: 0
Size: 222119 Color: 4

Bin 1031: 7 of cap free
Amount of items: 3
Items: 
Size: 684807 Color: 4
Size: 161715 Color: 4
Size: 153472 Color: 3

Bin 1032: 7 of cap free
Amount of items: 3
Items: 
Size: 468809 Color: 1
Size: 265694 Color: 2
Size: 265491 Color: 0

Bin 1033: 7 of cap free
Amount of items: 3
Items: 
Size: 602890 Color: 2
Size: 198790 Color: 1
Size: 198314 Color: 2

Bin 1034: 7 of cap free
Amount of items: 3
Items: 
Size: 681125 Color: 1
Size: 160626 Color: 3
Size: 158243 Color: 2

Bin 1035: 7 of cap free
Amount of items: 3
Items: 
Size: 421510 Color: 4
Size: 291304 Color: 2
Size: 287180 Color: 3

Bin 1036: 7 of cap free
Amount of items: 2
Items: 
Size: 736822 Color: 1
Size: 263172 Color: 0

Bin 1037: 7 of cap free
Amount of items: 3
Items: 
Size: 651310 Color: 2
Size: 175815 Color: 2
Size: 172869 Color: 4

Bin 1038: 7 of cap free
Amount of items: 2
Items: 
Size: 766761 Color: 2
Size: 233233 Color: 4

Bin 1039: 7 of cap free
Amount of items: 3
Items: 
Size: 473418 Color: 0
Size: 279932 Color: 4
Size: 246644 Color: 0

Bin 1040: 7 of cap free
Amount of items: 3
Items: 
Size: 384322 Color: 3
Size: 321736 Color: 3
Size: 293936 Color: 4

Bin 1041: 7 of cap free
Amount of items: 2
Items: 
Size: 683369 Color: 1
Size: 316625 Color: 3

Bin 1042: 7 of cap free
Amount of items: 3
Items: 
Size: 652153 Color: 0
Size: 176026 Color: 3
Size: 171815 Color: 0

Bin 1043: 7 of cap free
Amount of items: 2
Items: 
Size: 618611 Color: 2
Size: 381383 Color: 1

Bin 1044: 7 of cap free
Amount of items: 2
Items: 
Size: 741076 Color: 2
Size: 258918 Color: 3

Bin 1045: 7 of cap free
Amount of items: 2
Items: 
Size: 725294 Color: 3
Size: 274700 Color: 1

Bin 1046: 7 of cap free
Amount of items: 2
Items: 
Size: 651753 Color: 1
Size: 348241 Color: 3

Bin 1047: 7 of cap free
Amount of items: 2
Items: 
Size: 591147 Color: 1
Size: 408847 Color: 3

Bin 1048: 7 of cap free
Amount of items: 3
Items: 
Size: 470602 Color: 2
Size: 264785 Color: 3
Size: 264607 Color: 0

Bin 1049: 7 of cap free
Amount of items: 2
Items: 
Size: 589631 Color: 1
Size: 410363 Color: 2

Bin 1050: 7 of cap free
Amount of items: 2
Items: 
Size: 574397 Color: 0
Size: 425597 Color: 4

Bin 1051: 7 of cap free
Amount of items: 2
Items: 
Size: 544693 Color: 3
Size: 455301 Color: 0

Bin 1052: 7 of cap free
Amount of items: 2
Items: 
Size: 706048 Color: 4
Size: 293946 Color: 2

Bin 1053: 7 of cap free
Amount of items: 2
Items: 
Size: 602501 Color: 4
Size: 397493 Color: 2

Bin 1054: 7 of cap free
Amount of items: 3
Items: 
Size: 379438 Color: 3
Size: 371404 Color: 1
Size: 249152 Color: 3

Bin 1055: 7 of cap free
Amount of items: 3
Items: 
Size: 345828 Color: 0
Size: 339623 Color: 2
Size: 314543 Color: 2

Bin 1056: 7 of cap free
Amount of items: 2
Items: 
Size: 502444 Color: 1
Size: 497550 Color: 4

Bin 1057: 7 of cap free
Amount of items: 2
Items: 
Size: 506721 Color: 0
Size: 493273 Color: 3

Bin 1058: 7 of cap free
Amount of items: 2
Items: 
Size: 515769 Color: 4
Size: 484225 Color: 3

Bin 1059: 7 of cap free
Amount of items: 2
Items: 
Size: 519325 Color: 0
Size: 480669 Color: 2

Bin 1060: 7 of cap free
Amount of items: 2
Items: 
Size: 525184 Color: 1
Size: 474810 Color: 4

Bin 1061: 7 of cap free
Amount of items: 2
Items: 
Size: 535554 Color: 3
Size: 464440 Color: 2

Bin 1062: 7 of cap free
Amount of items: 2
Items: 
Size: 538395 Color: 2
Size: 461599 Color: 3

Bin 1063: 7 of cap free
Amount of items: 2
Items: 
Size: 546415 Color: 0
Size: 453579 Color: 2

Bin 1064: 7 of cap free
Amount of items: 2
Items: 
Size: 551249 Color: 0
Size: 448745 Color: 4

Bin 1065: 7 of cap free
Amount of items: 2
Items: 
Size: 592026 Color: 0
Size: 407968 Color: 4

Bin 1066: 7 of cap free
Amount of items: 2
Items: 
Size: 592642 Color: 0
Size: 407352 Color: 4

Bin 1067: 7 of cap free
Amount of items: 2
Items: 
Size: 601198 Color: 4
Size: 398796 Color: 3

Bin 1068: 7 of cap free
Amount of items: 2
Items: 
Size: 639428 Color: 0
Size: 360566 Color: 4

Bin 1069: 7 of cap free
Amount of items: 2
Items: 
Size: 640892 Color: 1
Size: 359102 Color: 3

Bin 1070: 7 of cap free
Amount of items: 2
Items: 
Size: 649179 Color: 1
Size: 350815 Color: 2

Bin 1071: 7 of cap free
Amount of items: 2
Items: 
Size: 650755 Color: 3
Size: 349239 Color: 0

Bin 1072: 7 of cap free
Amount of items: 3
Items: 
Size: 655488 Color: 2
Size: 173145 Color: 1
Size: 171361 Color: 1

Bin 1073: 7 of cap free
Amount of items: 3
Items: 
Size: 666201 Color: 2
Size: 167099 Color: 4
Size: 166694 Color: 1

Bin 1074: 7 of cap free
Amount of items: 2
Items: 
Size: 667358 Color: 4
Size: 332636 Color: 0

Bin 1075: 7 of cap free
Amount of items: 2
Items: 
Size: 677978 Color: 3
Size: 322016 Color: 1

Bin 1076: 7 of cap free
Amount of items: 2
Items: 
Size: 685818 Color: 2
Size: 314176 Color: 0

Bin 1077: 7 of cap free
Amount of items: 2
Items: 
Size: 730369 Color: 4
Size: 269625 Color: 3

Bin 1078: 7 of cap free
Amount of items: 2
Items: 
Size: 741567 Color: 2
Size: 258427 Color: 3

Bin 1079: 7 of cap free
Amount of items: 2
Items: 
Size: 743223 Color: 3
Size: 256771 Color: 1

Bin 1080: 7 of cap free
Amount of items: 2
Items: 
Size: 750967 Color: 2
Size: 249027 Color: 0

Bin 1081: 7 of cap free
Amount of items: 2
Items: 
Size: 773656 Color: 0
Size: 226338 Color: 2

Bin 1082: 8 of cap free
Amount of items: 2
Items: 
Size: 730310 Color: 1
Size: 269683 Color: 3

Bin 1083: 8 of cap free
Amount of items: 3
Items: 
Size: 378648 Color: 0
Size: 378455 Color: 4
Size: 242890 Color: 0

Bin 1084: 8 of cap free
Amount of items: 3
Items: 
Size: 722206 Color: 2
Size: 139166 Color: 0
Size: 138621 Color: 2

Bin 1085: 8 of cap free
Amount of items: 3
Items: 
Size: 626367 Color: 4
Size: 188419 Color: 4
Size: 185207 Color: 2

Bin 1086: 8 of cap free
Amount of items: 2
Items: 
Size: 715499 Color: 2
Size: 284494 Color: 0

Bin 1087: 8 of cap free
Amount of items: 2
Items: 
Size: 751704 Color: 0
Size: 248289 Color: 4

Bin 1088: 8 of cap free
Amount of items: 2
Items: 
Size: 734024 Color: 3
Size: 265969 Color: 0

Bin 1089: 8 of cap free
Amount of items: 3
Items: 
Size: 473184 Color: 4
Size: 268105 Color: 0
Size: 258704 Color: 3

Bin 1090: 8 of cap free
Amount of items: 2
Items: 
Size: 791717 Color: 2
Size: 208276 Color: 4

Bin 1091: 8 of cap free
Amount of items: 3
Items: 
Size: 547482 Color: 1
Size: 334205 Color: 2
Size: 118306 Color: 2

Bin 1092: 8 of cap free
Amount of items: 3
Items: 
Size: 483070 Color: 1
Size: 335704 Color: 4
Size: 181219 Color: 2

Bin 1093: 8 of cap free
Amount of items: 3
Items: 
Size: 650103 Color: 0
Size: 177280 Color: 1
Size: 172610 Color: 1

Bin 1094: 8 of cap free
Amount of items: 2
Items: 
Size: 613943 Color: 0
Size: 386050 Color: 2

Bin 1095: 8 of cap free
Amount of items: 3
Items: 
Size: 573614 Color: 1
Size: 229228 Color: 4
Size: 197151 Color: 0

Bin 1096: 8 of cap free
Amount of items: 2
Items: 
Size: 629967 Color: 2
Size: 370026 Color: 3

Bin 1097: 8 of cap free
Amount of items: 3
Items: 
Size: 603959 Color: 4
Size: 198168 Color: 1
Size: 197866 Color: 3

Bin 1098: 8 of cap free
Amount of items: 2
Items: 
Size: 722544 Color: 3
Size: 277449 Color: 0

Bin 1099: 8 of cap free
Amount of items: 2
Items: 
Size: 587208 Color: 0
Size: 412785 Color: 1

Bin 1100: 8 of cap free
Amount of items: 2
Items: 
Size: 606200 Color: 4
Size: 393793 Color: 2

Bin 1101: 8 of cap free
Amount of items: 2
Items: 
Size: 764027 Color: 3
Size: 235966 Color: 2

Bin 1102: 8 of cap free
Amount of items: 2
Items: 
Size: 603948 Color: 2
Size: 396045 Color: 4

Bin 1103: 8 of cap free
Amount of items: 3
Items: 
Size: 371467 Color: 1
Size: 371254 Color: 0
Size: 257272 Color: 0

Bin 1104: 8 of cap free
Amount of items: 2
Items: 
Size: 622008 Color: 4
Size: 377985 Color: 2

Bin 1105: 8 of cap free
Amount of items: 2
Items: 
Size: 541337 Color: 3
Size: 458656 Color: 0

Bin 1106: 8 of cap free
Amount of items: 3
Items: 
Size: 357639 Color: 2
Size: 356839 Color: 1
Size: 285515 Color: 1

Bin 1107: 8 of cap free
Amount of items: 3
Items: 
Size: 351376 Color: 2
Size: 338037 Color: 4
Size: 310580 Color: 4

Bin 1108: 8 of cap free
Amount of items: 2
Items: 
Size: 506783 Color: 0
Size: 493210 Color: 1

Bin 1109: 8 of cap free
Amount of items: 2
Items: 
Size: 507035 Color: 4
Size: 492958 Color: 3

Bin 1110: 8 of cap free
Amount of items: 2
Items: 
Size: 546065 Color: 0
Size: 453928 Color: 4

Bin 1111: 8 of cap free
Amount of items: 2
Items: 
Size: 557697 Color: 3
Size: 442296 Color: 2

Bin 1112: 8 of cap free
Amount of items: 2
Items: 
Size: 565896 Color: 2
Size: 434097 Color: 0

Bin 1113: 8 of cap free
Amount of items: 2
Items: 
Size: 580150 Color: 1
Size: 419843 Color: 2

Bin 1114: 8 of cap free
Amount of items: 2
Items: 
Size: 590775 Color: 3
Size: 409218 Color: 4

Bin 1115: 8 of cap free
Amount of items: 2
Items: 
Size: 591710 Color: 0
Size: 408283 Color: 1

Bin 1116: 8 of cap free
Amount of items: 2
Items: 
Size: 598964 Color: 3
Size: 401029 Color: 1

Bin 1117: 8 of cap free
Amount of items: 2
Items: 
Size: 647990 Color: 4
Size: 352003 Color: 3

Bin 1118: 8 of cap free
Amount of items: 2
Items: 
Size: 700447 Color: 0
Size: 299546 Color: 3

Bin 1119: 8 of cap free
Amount of items: 2
Items: 
Size: 768122 Color: 4
Size: 231871 Color: 3

Bin 1120: 8 of cap free
Amount of items: 2
Items: 
Size: 768762 Color: 4
Size: 231231 Color: 0

Bin 1121: 8 of cap free
Amount of items: 2
Items: 
Size: 775060 Color: 3
Size: 224933 Color: 4

Bin 1122: 8 of cap free
Amount of items: 2
Items: 
Size: 788067 Color: 3
Size: 211926 Color: 4

Bin 1123: 9 of cap free
Amount of items: 2
Items: 
Size: 693334 Color: 4
Size: 306658 Color: 3

Bin 1124: 9 of cap free
Amount of items: 2
Items: 
Size: 704614 Color: 1
Size: 295378 Color: 0

Bin 1125: 9 of cap free
Amount of items: 2
Items: 
Size: 796970 Color: 3
Size: 203022 Color: 2

Bin 1126: 9 of cap free
Amount of items: 3
Items: 
Size: 660794 Color: 3
Size: 169735 Color: 4
Size: 169463 Color: 2

Bin 1127: 9 of cap free
Amount of items: 3
Items: 
Size: 458709 Color: 4
Size: 272471 Color: 2
Size: 268812 Color: 1

Bin 1128: 9 of cap free
Amount of items: 3
Items: 
Size: 634998 Color: 4
Size: 186267 Color: 2
Size: 178727 Color: 3

Bin 1129: 9 of cap free
Amount of items: 2
Items: 
Size: 610440 Color: 0
Size: 389552 Color: 4

Bin 1130: 9 of cap free
Amount of items: 3
Items: 
Size: 603972 Color: 0
Size: 234495 Color: 0
Size: 161525 Color: 1

Bin 1131: 9 of cap free
Amount of items: 3
Items: 
Size: 479004 Color: 3
Size: 386010 Color: 3
Size: 134978 Color: 1

Bin 1132: 9 of cap free
Amount of items: 2
Items: 
Size: 664812 Color: 0
Size: 335180 Color: 4

Bin 1133: 9 of cap free
Amount of items: 2
Items: 
Size: 607701 Color: 3
Size: 392291 Color: 2

Bin 1134: 9 of cap free
Amount of items: 2
Items: 
Size: 739639 Color: 1
Size: 260353 Color: 0

Bin 1135: 9 of cap free
Amount of items: 2
Items: 
Size: 729611 Color: 2
Size: 270381 Color: 0

Bin 1136: 9 of cap free
Amount of items: 2
Items: 
Size: 709498 Color: 1
Size: 290494 Color: 0

Bin 1137: 9 of cap free
Amount of items: 2
Items: 
Size: 705772 Color: 4
Size: 294220 Color: 2

Bin 1138: 9 of cap free
Amount of items: 2
Items: 
Size: 595661 Color: 0
Size: 404331 Color: 1

Bin 1139: 9 of cap free
Amount of items: 2
Items: 
Size: 673044 Color: 3
Size: 326948 Color: 1

Bin 1140: 9 of cap free
Amount of items: 2
Items: 
Size: 595118 Color: 2
Size: 404874 Color: 0

Bin 1141: 9 of cap free
Amount of items: 2
Items: 
Size: 684700 Color: 1
Size: 315292 Color: 3

Bin 1142: 9 of cap free
Amount of items: 2
Items: 
Size: 761089 Color: 1
Size: 238903 Color: 4

Bin 1143: 9 of cap free
Amount of items: 2
Items: 
Size: 663855 Color: 1
Size: 336137 Color: 2

Bin 1144: 9 of cap free
Amount of items: 3
Items: 
Size: 562935 Color: 0
Size: 218888 Color: 1
Size: 218169 Color: 1

Bin 1145: 9 of cap free
Amount of items: 3
Items: 
Size: 384905 Color: 0
Size: 321318 Color: 1
Size: 293769 Color: 1

Bin 1146: 9 of cap free
Amount of items: 2
Items: 
Size: 638377 Color: 2
Size: 361615 Color: 0

Bin 1147: 9 of cap free
Amount of items: 2
Items: 
Size: 521849 Color: 1
Size: 478143 Color: 4

Bin 1148: 9 of cap free
Amount of items: 2
Items: 
Size: 557211 Color: 1
Size: 442781 Color: 4

Bin 1149: 9 of cap free
Amount of items: 2
Items: 
Size: 543393 Color: 4
Size: 456599 Color: 3

Bin 1150: 9 of cap free
Amount of items: 2
Items: 
Size: 686673 Color: 0
Size: 313319 Color: 3

Bin 1151: 9 of cap free
Amount of items: 2
Items: 
Size: 668007 Color: 2
Size: 331985 Color: 1

Bin 1152: 9 of cap free
Amount of items: 3
Items: 
Size: 357627 Color: 3
Size: 356936 Color: 2
Size: 285429 Color: 0

Bin 1153: 9 of cap free
Amount of items: 3
Items: 
Size: 333636 Color: 0
Size: 333505 Color: 0
Size: 332851 Color: 4

Bin 1154: 9 of cap free
Amount of items: 2
Items: 
Size: 536192 Color: 4
Size: 463800 Color: 3

Bin 1155: 9 of cap free
Amount of items: 2
Items: 
Size: 549392 Color: 1
Size: 450600 Color: 0

Bin 1156: 9 of cap free
Amount of items: 2
Items: 
Size: 556872 Color: 4
Size: 443120 Color: 3

Bin 1157: 9 of cap free
Amount of items: 3
Items: 
Size: 561778 Color: 0
Size: 219159 Color: 1
Size: 219055 Color: 3

Bin 1158: 9 of cap free
Amount of items: 2
Items: 
Size: 599178 Color: 1
Size: 400814 Color: 0

Bin 1159: 9 of cap free
Amount of items: 2
Items: 
Size: 622125 Color: 0
Size: 377867 Color: 3

Bin 1160: 9 of cap free
Amount of items: 2
Items: 
Size: 625675 Color: 4
Size: 374317 Color: 0

Bin 1161: 9 of cap free
Amount of items: 2
Items: 
Size: 626515 Color: 1
Size: 373477 Color: 2

Bin 1162: 9 of cap free
Amount of items: 2
Items: 
Size: 677252 Color: 3
Size: 322740 Color: 2

Bin 1163: 9 of cap free
Amount of items: 2
Items: 
Size: 683600 Color: 4
Size: 316392 Color: 1

Bin 1164: 9 of cap free
Amount of items: 2
Items: 
Size: 687077 Color: 2
Size: 312915 Color: 3

Bin 1165: 9 of cap free
Amount of items: 2
Items: 
Size: 705087 Color: 0
Size: 294905 Color: 3

Bin 1166: 9 of cap free
Amount of items: 2
Items: 
Size: 712090 Color: 1
Size: 287902 Color: 4

Bin 1167: 9 of cap free
Amount of items: 2
Items: 
Size: 719108 Color: 1
Size: 280884 Color: 0

Bin 1168: 9 of cap free
Amount of items: 2
Items: 
Size: 748230 Color: 4
Size: 251762 Color: 3

Bin 1169: 9 of cap free
Amount of items: 2
Items: 
Size: 748826 Color: 1
Size: 251166 Color: 2

Bin 1170: 9 of cap free
Amount of items: 2
Items: 
Size: 753231 Color: 3
Size: 246761 Color: 0

Bin 1171: 9 of cap free
Amount of items: 3
Items: 
Size: 781421 Color: 1
Size: 110359 Color: 2
Size: 108212 Color: 4

Bin 1172: 9 of cap free
Amount of items: 2
Items: 
Size: 788702 Color: 4
Size: 211290 Color: 1

Bin 1173: 9 of cap free
Amount of items: 2
Items: 
Size: 794970 Color: 4
Size: 205022 Color: 3

Bin 1174: 10 of cap free
Amount of items: 3
Items: 
Size: 385394 Color: 3
Size: 317619 Color: 2
Size: 296978 Color: 0

Bin 1175: 10 of cap free
Amount of items: 2
Items: 
Size: 605865 Color: 4
Size: 394126 Color: 2

Bin 1176: 10 of cap free
Amount of items: 3
Items: 
Size: 671245 Color: 0
Size: 164434 Color: 1
Size: 164312 Color: 3

Bin 1177: 10 of cap free
Amount of items: 3
Items: 
Size: 430578 Color: 4
Size: 285938 Color: 3
Size: 283475 Color: 2

Bin 1178: 10 of cap free
Amount of items: 2
Items: 
Size: 687939 Color: 4
Size: 312052 Color: 2

Bin 1179: 10 of cap free
Amount of items: 3
Items: 
Size: 697713 Color: 0
Size: 190351 Color: 0
Size: 111927 Color: 4

Bin 1180: 10 of cap free
Amount of items: 3
Items: 
Size: 482126 Color: 3
Size: 264376 Color: 3
Size: 253489 Color: 1

Bin 1181: 10 of cap free
Amount of items: 3
Items: 
Size: 615496 Color: 0
Size: 192863 Color: 4
Size: 191632 Color: 1

Bin 1182: 10 of cap free
Amount of items: 3
Items: 
Size: 736924 Color: 3
Size: 142949 Color: 4
Size: 120118 Color: 4

Bin 1183: 10 of cap free
Amount of items: 3
Items: 
Size: 473161 Color: 2
Size: 289108 Color: 2
Size: 237722 Color: 0

Bin 1184: 10 of cap free
Amount of items: 3
Items: 
Size: 472661 Color: 3
Size: 291170 Color: 0
Size: 236160 Color: 0

Bin 1185: 10 of cap free
Amount of items: 2
Items: 
Size: 702206 Color: 3
Size: 297785 Color: 2

Bin 1186: 10 of cap free
Amount of items: 2
Items: 
Size: 736952 Color: 1
Size: 263039 Color: 4

Bin 1187: 10 of cap free
Amount of items: 2
Items: 
Size: 755377 Color: 1
Size: 244614 Color: 0

Bin 1188: 10 of cap free
Amount of items: 2
Items: 
Size: 778650 Color: 2
Size: 221341 Color: 0

Bin 1189: 10 of cap free
Amount of items: 3
Items: 
Size: 506150 Color: 2
Size: 250151 Color: 2
Size: 243690 Color: 3

Bin 1190: 10 of cap free
Amount of items: 2
Items: 
Size: 733786 Color: 2
Size: 266205 Color: 1

Bin 1191: 10 of cap free
Amount of items: 2
Items: 
Size: 752368 Color: 3
Size: 247623 Color: 2

Bin 1192: 10 of cap free
Amount of items: 2
Items: 
Size: 751753 Color: 1
Size: 248238 Color: 2

Bin 1193: 10 of cap free
Amount of items: 2
Items: 
Size: 629700 Color: 2
Size: 370291 Color: 0

Bin 1194: 10 of cap free
Amount of items: 2
Items: 
Size: 655503 Color: 2
Size: 344488 Color: 3

Bin 1195: 10 of cap free
Amount of items: 2
Items: 
Size: 526008 Color: 2
Size: 473983 Color: 4

Bin 1196: 10 of cap free
Amount of items: 2
Items: 
Size: 617534 Color: 0
Size: 382457 Color: 1

Bin 1197: 10 of cap free
Amount of items: 2
Items: 
Size: 519872 Color: 0
Size: 480119 Color: 2

Bin 1198: 10 of cap free
Amount of items: 2
Items: 
Size: 686577 Color: 0
Size: 313414 Color: 1

Bin 1199: 10 of cap free
Amount of items: 2
Items: 
Size: 605656 Color: 2
Size: 394335 Color: 4

Bin 1200: 10 of cap free
Amount of items: 3
Items: 
Size: 533904 Color: 1
Size: 234316 Color: 0
Size: 231771 Color: 1

Bin 1201: 10 of cap free
Amount of items: 2
Items: 
Size: 670468 Color: 4
Size: 329523 Color: 1

Bin 1202: 10 of cap free
Amount of items: 2
Items: 
Size: 739618 Color: 2
Size: 260373 Color: 3

Bin 1203: 10 of cap free
Amount of items: 3
Items: 
Size: 633950 Color: 3
Size: 183401 Color: 1
Size: 182640 Color: 1

Bin 1204: 10 of cap free
Amount of items: 2
Items: 
Size: 757004 Color: 4
Size: 242987 Color: 1

Bin 1205: 10 of cap free
Amount of items: 3
Items: 
Size: 353586 Color: 2
Size: 324299 Color: 1
Size: 322106 Color: 4

Bin 1206: 10 of cap free
Amount of items: 2
Items: 
Size: 676283 Color: 0
Size: 323708 Color: 1

Bin 1207: 10 of cap free
Amount of items: 2
Items: 
Size: 792680 Color: 0
Size: 207311 Color: 1

Bin 1208: 10 of cap free
Amount of items: 2
Items: 
Size: 535958 Color: 1
Size: 464033 Color: 4

Bin 1209: 10 of cap free
Amount of items: 3
Items: 
Size: 336369 Color: 4
Size: 334068 Color: 1
Size: 329554 Color: 1

Bin 1210: 10 of cap free
Amount of items: 2
Items: 
Size: 501715 Color: 3
Size: 498276 Color: 4

Bin 1211: 10 of cap free
Amount of items: 2
Items: 
Size: 562648 Color: 2
Size: 437343 Color: 3

Bin 1212: 10 of cap free
Amount of items: 2
Items: 
Size: 562775 Color: 3
Size: 437216 Color: 4

Bin 1213: 10 of cap free
Amount of items: 2
Items: 
Size: 580211 Color: 3
Size: 419780 Color: 4

Bin 1214: 10 of cap free
Amount of items: 2
Items: 
Size: 593008 Color: 4
Size: 406983 Color: 0

Bin 1215: 10 of cap free
Amount of items: 2
Items: 
Size: 598499 Color: 3
Size: 401492 Color: 0

Bin 1216: 10 of cap free
Amount of items: 2
Items: 
Size: 602227 Color: 3
Size: 397764 Color: 1

Bin 1217: 10 of cap free
Amount of items: 2
Items: 
Size: 637303 Color: 2
Size: 362688 Color: 0

Bin 1218: 10 of cap free
Amount of items: 2
Items: 
Size: 637804 Color: 1
Size: 362187 Color: 3

Bin 1219: 10 of cap free
Amount of items: 2
Items: 
Size: 660058 Color: 1
Size: 339933 Color: 0

Bin 1220: 10 of cap free
Amount of items: 2
Items: 
Size: 665385 Color: 2
Size: 334606 Color: 1

Bin 1221: 10 of cap free
Amount of items: 2
Items: 
Size: 670870 Color: 4
Size: 329121 Color: 3

Bin 1222: 10 of cap free
Amount of items: 2
Items: 
Size: 675191 Color: 2
Size: 324800 Color: 0

Bin 1223: 10 of cap free
Amount of items: 2
Items: 
Size: 719611 Color: 2
Size: 280380 Color: 1

Bin 1224: 10 of cap free
Amount of items: 2
Items: 
Size: 755333 Color: 1
Size: 244658 Color: 2

Bin 1225: 10 of cap free
Amount of items: 2
Items: 
Size: 772787 Color: 2
Size: 227204 Color: 0

Bin 1226: 10 of cap free
Amount of items: 2
Items: 
Size: 792747 Color: 3
Size: 207244 Color: 2

Bin 1227: 11 of cap free
Amount of items: 3
Items: 
Size: 636097 Color: 1
Size: 193968 Color: 0
Size: 169925 Color: 0

Bin 1228: 11 of cap free
Amount of items: 2
Items: 
Size: 724276 Color: 0
Size: 275714 Color: 1

Bin 1229: 11 of cap free
Amount of items: 3
Items: 
Size: 616768 Color: 0
Size: 193050 Color: 2
Size: 190172 Color: 3

Bin 1230: 11 of cap free
Amount of items: 2
Items: 
Size: 727012 Color: 4
Size: 272978 Color: 1

Bin 1231: 11 of cap free
Amount of items: 2
Items: 
Size: 736451 Color: 2
Size: 263539 Color: 3

Bin 1232: 11 of cap free
Amount of items: 3
Items: 
Size: 473560 Color: 4
Size: 287574 Color: 3
Size: 238856 Color: 3

Bin 1233: 11 of cap free
Amount of items: 3
Items: 
Size: 616350 Color: 4
Size: 194647 Color: 1
Size: 188993 Color: 4

Bin 1234: 11 of cap free
Amount of items: 2
Items: 
Size: 738029 Color: 0
Size: 261961 Color: 3

Bin 1235: 11 of cap free
Amount of items: 2
Items: 
Size: 765943 Color: 1
Size: 234047 Color: 4

Bin 1236: 11 of cap free
Amount of items: 2
Items: 
Size: 767660 Color: 2
Size: 232330 Color: 1

Bin 1237: 11 of cap free
Amount of items: 2
Items: 
Size: 725719 Color: 2
Size: 274271 Color: 3

Bin 1238: 11 of cap free
Amount of items: 2
Items: 
Size: 519033 Color: 4
Size: 480957 Color: 2

Bin 1239: 11 of cap free
Amount of items: 2
Items: 
Size: 643272 Color: 2
Size: 356718 Color: 4

Bin 1240: 11 of cap free
Amount of items: 2
Items: 
Size: 544883 Color: 0
Size: 455107 Color: 2

Bin 1241: 11 of cap free
Amount of items: 2
Items: 
Size: 748178 Color: 4
Size: 251812 Color: 1

Bin 1242: 11 of cap free
Amount of items: 2
Items: 
Size: 601866 Color: 4
Size: 398124 Color: 0

Bin 1243: 11 of cap free
Amount of items: 2
Items: 
Size: 704512 Color: 4
Size: 295478 Color: 1

Bin 1244: 11 of cap free
Amount of items: 3
Items: 
Size: 594709 Color: 3
Size: 203099 Color: 0
Size: 202182 Color: 3

Bin 1245: 11 of cap free
Amount of items: 2
Items: 
Size: 686967 Color: 4
Size: 313023 Color: 2

Bin 1246: 11 of cap free
Amount of items: 2
Items: 
Size: 541234 Color: 3
Size: 458756 Color: 4

Bin 1247: 11 of cap free
Amount of items: 3
Items: 
Size: 358528 Color: 4
Size: 356599 Color: 0
Size: 284863 Color: 1

Bin 1248: 11 of cap free
Amount of items: 2
Items: 
Size: 500957 Color: 3
Size: 499033 Color: 0

Bin 1249: 11 of cap free
Amount of items: 2
Items: 
Size: 504315 Color: 4
Size: 495675 Color: 2

Bin 1250: 11 of cap free
Amount of items: 2
Items: 
Size: 508625 Color: 3
Size: 491365 Color: 1

Bin 1251: 11 of cap free
Amount of items: 2
Items: 
Size: 512846 Color: 1
Size: 487144 Color: 2

Bin 1252: 11 of cap free
Amount of items: 2
Items: 
Size: 565392 Color: 2
Size: 434598 Color: 3

Bin 1253: 11 of cap free
Amount of items: 2
Items: 
Size: 567809 Color: 3
Size: 432181 Color: 0

Bin 1254: 11 of cap free
Amount of items: 2
Items: 
Size: 568644 Color: 4
Size: 431346 Color: 3

Bin 1255: 11 of cap free
Amount of items: 2
Items: 
Size: 568668 Color: 3
Size: 431322 Color: 2

Bin 1256: 11 of cap free
Amount of items: 2
Items: 
Size: 588298 Color: 4
Size: 411692 Color: 3

Bin 1257: 11 of cap free
Amount of items: 2
Items: 
Size: 608841 Color: 3
Size: 391149 Color: 4

Bin 1258: 11 of cap free
Amount of items: 3
Items: 
Size: 615576 Color: 2
Size: 192639 Color: 3
Size: 191775 Color: 0

Bin 1259: 11 of cap free
Amount of items: 3
Items: 
Size: 620276 Color: 2
Size: 190047 Color: 3
Size: 189667 Color: 1

Bin 1260: 11 of cap free
Amount of items: 2
Items: 
Size: 637104 Color: 2
Size: 362886 Color: 1

Bin 1261: 11 of cap free
Amount of items: 2
Items: 
Size: 652514 Color: 2
Size: 347476 Color: 1

Bin 1262: 11 of cap free
Amount of items: 2
Items: 
Size: 655771 Color: 0
Size: 344219 Color: 2

Bin 1263: 11 of cap free
Amount of items: 2
Items: 
Size: 656560 Color: 1
Size: 343430 Color: 0

Bin 1264: 11 of cap free
Amount of items: 2
Items: 
Size: 678242 Color: 3
Size: 321748 Color: 2

Bin 1265: 11 of cap free
Amount of items: 2
Items: 
Size: 682120 Color: 2
Size: 317870 Color: 0

Bin 1266: 11 of cap free
Amount of items: 2
Items: 
Size: 684993 Color: 0
Size: 314997 Color: 3

Bin 1267: 11 of cap free
Amount of items: 2
Items: 
Size: 691585 Color: 1
Size: 308405 Color: 4

Bin 1268: 11 of cap free
Amount of items: 2
Items: 
Size: 699638 Color: 1
Size: 300352 Color: 4

Bin 1269: 11 of cap free
Amount of items: 2
Items: 
Size: 726147 Color: 3
Size: 273843 Color: 1

Bin 1270: 11 of cap free
Amount of items: 2
Items: 
Size: 740217 Color: 0
Size: 259773 Color: 4

Bin 1271: 11 of cap free
Amount of items: 2
Items: 
Size: 744135 Color: 0
Size: 255855 Color: 2

Bin 1272: 11 of cap free
Amount of items: 2
Items: 
Size: 762639 Color: 4
Size: 237351 Color: 3

Bin 1273: 11 of cap free
Amount of items: 2
Items: 
Size: 799206 Color: 0
Size: 200784 Color: 1

Bin 1274: 11 of cap free
Amount of items: 2
Items: 
Size: 799743 Color: 4
Size: 200247 Color: 3

Bin 1275: 12 of cap free
Amount of items: 3
Items: 
Size: 492374 Color: 4
Size: 254281 Color: 0
Size: 253334 Color: 0

Bin 1276: 12 of cap free
Amount of items: 2
Items: 
Size: 751785 Color: 0
Size: 248204 Color: 2

Bin 1277: 12 of cap free
Amount of items: 3
Items: 
Size: 583101 Color: 0
Size: 234088 Color: 3
Size: 182800 Color: 2

Bin 1278: 12 of cap free
Amount of items: 2
Items: 
Size: 765312 Color: 0
Size: 234677 Color: 2

Bin 1279: 12 of cap free
Amount of items: 3
Items: 
Size: 646993 Color: 3
Size: 179192 Color: 0
Size: 173804 Color: 1

Bin 1280: 12 of cap free
Amount of items: 2
Items: 
Size: 536014 Color: 0
Size: 463975 Color: 1

Bin 1281: 12 of cap free
Amount of items: 3
Items: 
Size: 536168 Color: 1
Size: 232292 Color: 3
Size: 231529 Color: 4

Bin 1282: 12 of cap free
Amount of items: 2
Items: 
Size: 712347 Color: 2
Size: 287642 Color: 3

Bin 1283: 12 of cap free
Amount of items: 3
Items: 
Size: 513370 Color: 4
Size: 243980 Color: 0
Size: 242639 Color: 1

Bin 1284: 12 of cap free
Amount of items: 2
Items: 
Size: 620182 Color: 1
Size: 379807 Color: 2

Bin 1285: 12 of cap free
Amount of items: 2
Items: 
Size: 660456 Color: 0
Size: 339533 Color: 3

Bin 1286: 12 of cap free
Amount of items: 3
Items: 
Size: 393097 Color: 4
Size: 308981 Color: 3
Size: 297911 Color: 1

Bin 1287: 12 of cap free
Amount of items: 2
Items: 
Size: 670462 Color: 0
Size: 329527 Color: 4

Bin 1288: 12 of cap free
Amount of items: 3
Items: 
Size: 389169 Color: 2
Size: 381015 Color: 0
Size: 229805 Color: 1

Bin 1289: 12 of cap free
Amount of items: 3
Items: 
Size: 356107 Color: 3
Size: 355826 Color: 4
Size: 288056 Color: 0

Bin 1290: 12 of cap free
Amount of items: 3
Items: 
Size: 350672 Color: 1
Size: 348542 Color: 1
Size: 300775 Color: 2

Bin 1291: 12 of cap free
Amount of items: 3
Items: 
Size: 482784 Color: 0
Size: 263928 Color: 4
Size: 253277 Color: 1

Bin 1292: 12 of cap free
Amount of items: 2
Items: 
Size: 501380 Color: 2
Size: 498609 Color: 4

Bin 1293: 12 of cap free
Amount of items: 2
Items: 
Size: 512816 Color: 3
Size: 487173 Color: 2

Bin 1294: 12 of cap free
Amount of items: 2
Items: 
Size: 526186 Color: 1
Size: 473803 Color: 3

Bin 1295: 12 of cap free
Amount of items: 2
Items: 
Size: 574360 Color: 1
Size: 425629 Color: 4

Bin 1296: 12 of cap free
Amount of items: 2
Items: 
Size: 591095 Color: 1
Size: 408894 Color: 4

Bin 1297: 12 of cap free
Amount of items: 2
Items: 
Size: 597418 Color: 2
Size: 402571 Color: 3

Bin 1298: 12 of cap free
Amount of items: 2
Items: 
Size: 705737 Color: 2
Size: 294252 Color: 0

Bin 1299: 12 of cap free
Amount of items: 2
Items: 
Size: 731225 Color: 2
Size: 268764 Color: 0

Bin 1300: 12 of cap free
Amount of items: 2
Items: 
Size: 732283 Color: 3
Size: 267706 Color: 0

Bin 1301: 12 of cap free
Amount of items: 2
Items: 
Size: 755897 Color: 0
Size: 244092 Color: 4

Bin 1302: 12 of cap free
Amount of items: 2
Items: 
Size: 773680 Color: 0
Size: 226309 Color: 2

Bin 1303: 13 of cap free
Amount of items: 3
Items: 
Size: 612064 Color: 1
Size: 195270 Color: 3
Size: 192654 Color: 3

Bin 1304: 13 of cap free
Amount of items: 3
Items: 
Size: 638398 Color: 3
Size: 180850 Color: 0
Size: 180740 Color: 1

Bin 1305: 13 of cap free
Amount of items: 3
Items: 
Size: 493376 Color: 0
Size: 253489 Color: 1
Size: 253123 Color: 4

Bin 1306: 13 of cap free
Amount of items: 2
Items: 
Size: 773480 Color: 4
Size: 226508 Color: 0

Bin 1307: 13 of cap free
Amount of items: 3
Items: 
Size: 669874 Color: 1
Size: 182097 Color: 0
Size: 148017 Color: 4

Bin 1308: 13 of cap free
Amount of items: 3
Items: 
Size: 728893 Color: 3
Size: 139091 Color: 3
Size: 132004 Color: 0

Bin 1309: 13 of cap free
Amount of items: 3
Items: 
Size: 611953 Color: 3
Size: 195502 Color: 2
Size: 192533 Color: 4

Bin 1310: 13 of cap free
Amount of items: 3
Items: 
Size: 722687 Color: 2
Size: 163059 Color: 2
Size: 114242 Color: 3

Bin 1311: 13 of cap free
Amount of items: 3
Items: 
Size: 709771 Color: 2
Size: 145143 Color: 1
Size: 145074 Color: 0

Bin 1312: 13 of cap free
Amount of items: 2
Items: 
Size: 748858 Color: 2
Size: 251130 Color: 4

Bin 1313: 13 of cap free
Amount of items: 3
Items: 
Size: 688395 Color: 4
Size: 180806 Color: 4
Size: 130787 Color: 0

Bin 1314: 13 of cap free
Amount of items: 2
Items: 
Size: 620620 Color: 2
Size: 379368 Color: 4

Bin 1315: 13 of cap free
Amount of items: 3
Items: 
Size: 422741 Color: 3
Size: 290640 Color: 1
Size: 286607 Color: 0

Bin 1316: 13 of cap free
Amount of items: 2
Items: 
Size: 513388 Color: 2
Size: 486600 Color: 3

Bin 1317: 13 of cap free
Amount of items: 2
Items: 
Size: 545683 Color: 1
Size: 454305 Color: 2

Bin 1318: 13 of cap free
Amount of items: 2
Items: 
Size: 516394 Color: 2
Size: 483594 Color: 1

Bin 1319: 13 of cap free
Amount of items: 3
Items: 
Size: 560694 Color: 0
Size: 219752 Color: 0
Size: 219542 Color: 1

Bin 1320: 13 of cap free
Amount of items: 2
Items: 
Size: 536590 Color: 2
Size: 463398 Color: 1

Bin 1321: 13 of cap free
Amount of items: 2
Items: 
Size: 509088 Color: 0
Size: 490900 Color: 4

Bin 1322: 13 of cap free
Amount of items: 2
Items: 
Size: 776664 Color: 1
Size: 223324 Color: 0

Bin 1323: 13 of cap free
Amount of items: 2
Items: 
Size: 679911 Color: 0
Size: 320077 Color: 3

Bin 1324: 13 of cap free
Amount of items: 2
Items: 
Size: 534720 Color: 3
Size: 465268 Color: 2

Bin 1325: 13 of cap free
Amount of items: 2
Items: 
Size: 548363 Color: 2
Size: 451625 Color: 0

Bin 1326: 13 of cap free
Amount of items: 2
Items: 
Size: 573600 Color: 3
Size: 426388 Color: 2

Bin 1327: 13 of cap free
Amount of items: 2
Items: 
Size: 592057 Color: 0
Size: 407931 Color: 1

Bin 1328: 13 of cap free
Amount of items: 2
Items: 
Size: 594596 Color: 0
Size: 405392 Color: 3

Bin 1329: 13 of cap free
Amount of items: 2
Items: 
Size: 649460 Color: 3
Size: 350528 Color: 4

Bin 1330: 13 of cap free
Amount of items: 2
Items: 
Size: 652807 Color: 4
Size: 347181 Color: 0

Bin 1331: 13 of cap free
Amount of items: 2
Items: 
Size: 655901 Color: 1
Size: 344087 Color: 0

Bin 1332: 13 of cap free
Amount of items: 2
Items: 
Size: 668767 Color: 1
Size: 331221 Color: 3

Bin 1333: 13 of cap free
Amount of items: 2
Items: 
Size: 683801 Color: 3
Size: 316187 Color: 1

Bin 1334: 13 of cap free
Amount of items: 2
Items: 
Size: 689100 Color: 3
Size: 310888 Color: 0

Bin 1335: 13 of cap free
Amount of items: 2
Items: 
Size: 714250 Color: 3
Size: 285738 Color: 0

Bin 1336: 13 of cap free
Amount of items: 2
Items: 
Size: 748019 Color: 0
Size: 251969 Color: 3

Bin 1337: 13 of cap free
Amount of items: 2
Items: 
Size: 761847 Color: 0
Size: 238141 Color: 3

Bin 1338: 13 of cap free
Amount of items: 2
Items: 
Size: 790465 Color: 3
Size: 209523 Color: 1

Bin 1339: 14 of cap free
Amount of items: 3
Items: 
Size: 664783 Color: 1
Size: 168424 Color: 0
Size: 166780 Color: 2

Bin 1340: 14 of cap free
Amount of items: 3
Items: 
Size: 567534 Color: 3
Size: 239306 Color: 1
Size: 193147 Color: 3

Bin 1341: 14 of cap free
Amount of items: 3
Items: 
Size: 795833 Color: 1
Size: 102375 Color: 4
Size: 101779 Color: 3

Bin 1342: 14 of cap free
Amount of items: 2
Items: 
Size: 612289 Color: 2
Size: 387698 Color: 3

Bin 1343: 14 of cap free
Amount of items: 3
Items: 
Size: 613720 Color: 3
Size: 193757 Color: 4
Size: 192510 Color: 4

Bin 1344: 14 of cap free
Amount of items: 2
Items: 
Size: 719335 Color: 2
Size: 280652 Color: 0

Bin 1345: 14 of cap free
Amount of items: 3
Items: 
Size: 639351 Color: 2
Size: 181548 Color: 4
Size: 179088 Color: 4

Bin 1346: 14 of cap free
Amount of items: 2
Items: 
Size: 585534 Color: 1
Size: 414453 Color: 4

Bin 1347: 14 of cap free
Amount of items: 2
Items: 
Size: 557068 Color: 4
Size: 442919 Color: 2

Bin 1348: 14 of cap free
Amount of items: 3
Items: 
Size: 602669 Color: 3
Size: 201534 Color: 1
Size: 195784 Color: 1

Bin 1349: 14 of cap free
Amount of items: 2
Items: 
Size: 712128 Color: 0
Size: 287859 Color: 2

Bin 1350: 14 of cap free
Amount of items: 3
Items: 
Size: 557224 Color: 3
Size: 229723 Color: 4
Size: 213040 Color: 1

Bin 1351: 14 of cap free
Amount of items: 2
Items: 
Size: 742191 Color: 4
Size: 257796 Color: 1

Bin 1352: 14 of cap free
Amount of items: 2
Items: 
Size: 784010 Color: 3
Size: 215977 Color: 0

Bin 1353: 14 of cap free
Amount of items: 2
Items: 
Size: 776190 Color: 0
Size: 223797 Color: 1

Bin 1354: 14 of cap free
Amount of items: 2
Items: 
Size: 536458 Color: 3
Size: 463529 Color: 0

Bin 1355: 14 of cap free
Amount of items: 3
Items: 
Size: 509185 Color: 3
Size: 245824 Color: 0
Size: 244978 Color: 3

Bin 1356: 14 of cap free
Amount of items: 2
Items: 
Size: 718391 Color: 4
Size: 281596 Color: 2

Bin 1357: 14 of cap free
Amount of items: 3
Items: 
Size: 441727 Color: 2
Size: 416016 Color: 2
Size: 142244 Color: 0

Bin 1358: 14 of cap free
Amount of items: 3
Items: 
Size: 360770 Color: 1
Size: 360028 Color: 2
Size: 279189 Color: 0

Bin 1359: 14 of cap free
Amount of items: 2
Items: 
Size: 521060 Color: 3
Size: 478927 Color: 2

Bin 1360: 14 of cap free
Amount of items: 2
Items: 
Size: 529612 Color: 1
Size: 470375 Color: 3

Bin 1361: 14 of cap free
Amount of items: 2
Items: 
Size: 551000 Color: 1
Size: 448987 Color: 4

Bin 1362: 14 of cap free
Amount of items: 2
Items: 
Size: 553803 Color: 3
Size: 446184 Color: 2

Bin 1363: 14 of cap free
Amount of items: 2
Items: 
Size: 567054 Color: 3
Size: 432933 Color: 4

Bin 1364: 14 of cap free
Amount of items: 2
Items: 
Size: 572966 Color: 0
Size: 427021 Color: 2

Bin 1365: 14 of cap free
Amount of items: 2
Items: 
Size: 586397 Color: 1
Size: 413590 Color: 2

Bin 1366: 14 of cap free
Amount of items: 2
Items: 
Size: 586570 Color: 2
Size: 413417 Color: 4

Bin 1367: 14 of cap free
Amount of items: 2
Items: 
Size: 587697 Color: 3
Size: 412290 Color: 1

Bin 1368: 14 of cap free
Amount of items: 2
Items: 
Size: 600084 Color: 2
Size: 399903 Color: 1

Bin 1369: 14 of cap free
Amount of items: 2
Items: 
Size: 605333 Color: 3
Size: 394654 Color: 4

Bin 1370: 14 of cap free
Amount of items: 3
Items: 
Size: 614939 Color: 2
Size: 193302 Color: 3
Size: 191746 Color: 0

Bin 1371: 14 of cap free
Amount of items: 2
Items: 
Size: 630253 Color: 3
Size: 369734 Color: 1

Bin 1372: 14 of cap free
Amount of items: 3
Items: 
Size: 640719 Color: 2
Size: 181100 Color: 4
Size: 178168 Color: 0

Bin 1373: 14 of cap free
Amount of items: 2
Items: 
Size: 645564 Color: 1
Size: 354423 Color: 2

Bin 1374: 14 of cap free
Amount of items: 2
Items: 
Size: 690799 Color: 2
Size: 309188 Color: 0

Bin 1375: 14 of cap free
Amount of items: 2
Items: 
Size: 729159 Color: 3
Size: 270828 Color: 0

Bin 1376: 14 of cap free
Amount of items: 2
Items: 
Size: 735694 Color: 3
Size: 264293 Color: 4

Bin 1377: 14 of cap free
Amount of items: 2
Items: 
Size: 766416 Color: 4
Size: 233571 Color: 3

Bin 1378: 14 of cap free
Amount of items: 2
Items: 
Size: 780257 Color: 3
Size: 219730 Color: 4

Bin 1379: 14 of cap free
Amount of items: 2
Items: 
Size: 795673 Color: 0
Size: 204314 Color: 4

Bin 1380: 15 of cap free
Amount of items: 3
Items: 
Size: 616701 Color: 4
Size: 194073 Color: 3
Size: 189212 Color: 2

Bin 1381: 15 of cap free
Amount of items: 2
Items: 
Size: 670177 Color: 3
Size: 329809 Color: 0

Bin 1382: 15 of cap free
Amount of items: 3
Items: 
Size: 697096 Color: 4
Size: 195253 Color: 4
Size: 107637 Color: 1

Bin 1383: 15 of cap free
Amount of items: 3
Items: 
Size: 688501 Color: 4
Size: 159308 Color: 2
Size: 152177 Color: 1

Bin 1384: 15 of cap free
Amount of items: 3
Items: 
Size: 398347 Color: 1
Size: 308817 Color: 0
Size: 292822 Color: 3

Bin 1385: 15 of cap free
Amount of items: 2
Items: 
Size: 568834 Color: 2
Size: 431152 Color: 0

Bin 1386: 15 of cap free
Amount of items: 2
Items: 
Size: 531065 Color: 2
Size: 468921 Color: 0

Bin 1387: 15 of cap free
Amount of items: 3
Items: 
Size: 643520 Color: 1
Size: 180972 Color: 2
Size: 175494 Color: 1

Bin 1388: 15 of cap free
Amount of items: 3
Items: 
Size: 646620 Color: 1
Size: 179208 Color: 2
Size: 174158 Color: 0

Bin 1389: 15 of cap free
Amount of items: 3
Items: 
Size: 623823 Color: 0
Size: 190015 Color: 3
Size: 186148 Color: 1

Bin 1390: 15 of cap free
Amount of items: 2
Items: 
Size: 638693 Color: 2
Size: 361293 Color: 1

Bin 1391: 15 of cap free
Amount of items: 2
Items: 
Size: 681208 Color: 2
Size: 318778 Color: 1

Bin 1392: 15 of cap free
Amount of items: 2
Items: 
Size: 506043 Color: 2
Size: 493943 Color: 1

Bin 1393: 15 of cap free
Amount of items: 2
Items: 
Size: 513131 Color: 3
Size: 486855 Color: 4

Bin 1394: 15 of cap free
Amount of items: 2
Items: 
Size: 561047 Color: 4
Size: 438939 Color: 2

Bin 1395: 15 of cap free
Amount of items: 2
Items: 
Size: 592251 Color: 1
Size: 407735 Color: 4

Bin 1396: 15 of cap free
Amount of items: 2
Items: 
Size: 611999 Color: 0
Size: 387987 Color: 3

Bin 1397: 15 of cap free
Amount of items: 2
Items: 
Size: 614055 Color: 3
Size: 385931 Color: 1

Bin 1398: 15 of cap free
Amount of items: 3
Items: 
Size: 619061 Color: 2
Size: 191471 Color: 3
Size: 189454 Color: 0

Bin 1399: 15 of cap free
Amount of items: 2
Items: 
Size: 619834 Color: 3
Size: 380152 Color: 0

Bin 1400: 15 of cap free
Amount of items: 2
Items: 
Size: 632423 Color: 1
Size: 367563 Color: 4

Bin 1401: 15 of cap free
Amount of items: 2
Items: 
Size: 663095 Color: 1
Size: 336891 Color: 0

Bin 1402: 15 of cap free
Amount of items: 2
Items: 
Size: 677795 Color: 0
Size: 322191 Color: 2

Bin 1403: 15 of cap free
Amount of items: 2
Items: 
Size: 717578 Color: 3
Size: 282408 Color: 1

Bin 1404: 15 of cap free
Amount of items: 2
Items: 
Size: 732870 Color: 3
Size: 267116 Color: 4

Bin 1405: 15 of cap free
Amount of items: 2
Items: 
Size: 774034 Color: 3
Size: 225952 Color: 1

Bin 1406: 15 of cap free
Amount of items: 2
Items: 
Size: 788922 Color: 0
Size: 211064 Color: 2

Bin 1407: 16 of cap free
Amount of items: 3
Items: 
Size: 379454 Color: 0
Size: 364941 Color: 2
Size: 255590 Color: 3

Bin 1408: 16 of cap free
Amount of items: 3
Items: 
Size: 464574 Color: 3
Size: 298853 Color: 4
Size: 236558 Color: 3

Bin 1409: 16 of cap free
Amount of items: 2
Items: 
Size: 601489 Color: 0
Size: 398496 Color: 4

Bin 1410: 16 of cap free
Amount of items: 3
Items: 
Size: 377597 Color: 3
Size: 370930 Color: 0
Size: 251458 Color: 3

Bin 1411: 16 of cap free
Amount of items: 2
Items: 
Size: 515898 Color: 4
Size: 484087 Color: 0

Bin 1412: 16 of cap free
Amount of items: 3
Items: 
Size: 358011 Color: 1
Size: 357772 Color: 0
Size: 284202 Color: 2

Bin 1413: 16 of cap free
Amount of items: 3
Items: 
Size: 599474 Color: 3
Size: 212025 Color: 3
Size: 188486 Color: 4

Bin 1414: 16 of cap free
Amount of items: 3
Items: 
Size: 473580 Color: 1
Size: 282822 Color: 1
Size: 243583 Color: 3

Bin 1415: 16 of cap free
Amount of items: 3
Items: 
Size: 679786 Color: 4
Size: 166410 Color: 4
Size: 153789 Color: 3

Bin 1416: 16 of cap free
Amount of items: 2
Items: 
Size: 625807 Color: 2
Size: 374178 Color: 4

Bin 1417: 16 of cap free
Amount of items: 3
Items: 
Size: 629335 Color: 4
Size: 222140 Color: 3
Size: 148510 Color: 1

Bin 1418: 16 of cap free
Amount of items: 3
Items: 
Size: 672090 Color: 0
Size: 187308 Color: 0
Size: 140587 Color: 2

Bin 1419: 16 of cap free
Amount of items: 2
Items: 
Size: 566614 Color: 3
Size: 433371 Color: 4

Bin 1420: 16 of cap free
Amount of items: 3
Items: 
Size: 563313 Color: 2
Size: 221107 Color: 0
Size: 215565 Color: 1

Bin 1421: 16 of cap free
Amount of items: 2
Items: 
Size: 663492 Color: 2
Size: 336493 Color: 3

Bin 1422: 16 of cap free
Amount of items: 2
Items: 
Size: 764619 Color: 4
Size: 235366 Color: 2

Bin 1423: 16 of cap free
Amount of items: 2
Items: 
Size: 778379 Color: 1
Size: 221606 Color: 3

Bin 1424: 16 of cap free
Amount of items: 2
Items: 
Size: 647104 Color: 3
Size: 352881 Color: 4

Bin 1425: 16 of cap free
Amount of items: 2
Items: 
Size: 644303 Color: 0
Size: 355682 Color: 2

Bin 1426: 16 of cap free
Amount of items: 2
Items: 
Size: 551185 Color: 1
Size: 448800 Color: 2

Bin 1427: 16 of cap free
Amount of items: 3
Items: 
Size: 348409 Color: 4
Size: 340583 Color: 0
Size: 310993 Color: 4

Bin 1428: 16 of cap free
Amount of items: 2
Items: 
Size: 500609 Color: 2
Size: 499376 Color: 0

Bin 1429: 16 of cap free
Amount of items: 2
Items: 
Size: 522520 Color: 4
Size: 477465 Color: 2

Bin 1430: 16 of cap free
Amount of items: 2
Items: 
Size: 531906 Color: 2
Size: 468079 Color: 1

Bin 1431: 16 of cap free
Amount of items: 2
Items: 
Size: 533856 Color: 0
Size: 466129 Color: 3

Bin 1432: 16 of cap free
Amount of items: 2
Items: 
Size: 552775 Color: 4
Size: 447210 Color: 2

Bin 1433: 16 of cap free
Amount of items: 2
Items: 
Size: 561182 Color: 3
Size: 438803 Color: 0

Bin 1434: 16 of cap free
Amount of items: 2
Items: 
Size: 603334 Color: 2
Size: 396651 Color: 3

Bin 1435: 16 of cap free
Amount of items: 2
Items: 
Size: 614846 Color: 0
Size: 385139 Color: 4

Bin 1436: 16 of cap free
Amount of items: 2
Items: 
Size: 631022 Color: 1
Size: 368963 Color: 4

Bin 1437: 16 of cap free
Amount of items: 2
Items: 
Size: 637620 Color: 3
Size: 362365 Color: 0

Bin 1438: 16 of cap free
Amount of items: 2
Items: 
Size: 654340 Color: 2
Size: 345645 Color: 1

Bin 1439: 16 of cap free
Amount of items: 2
Items: 
Size: 681520 Color: 2
Size: 318465 Color: 3

Bin 1440: 16 of cap free
Amount of items: 2
Items: 
Size: 720434 Color: 1
Size: 279551 Color: 4

Bin 1441: 16 of cap free
Amount of items: 2
Items: 
Size: 772170 Color: 0
Size: 227815 Color: 2

Bin 1442: 16 of cap free
Amount of items: 2
Items: 
Size: 785902 Color: 4
Size: 214083 Color: 1

Bin 1443: 16 of cap free
Amount of items: 2
Items: 
Size: 798970 Color: 1
Size: 201015 Color: 2

Bin 1444: 17 of cap free
Amount of items: 3
Items: 
Size: 505405 Color: 4
Size: 247337 Color: 2
Size: 247242 Color: 1

Bin 1445: 17 of cap free
Amount of items: 2
Items: 
Size: 751852 Color: 4
Size: 248132 Color: 2

Bin 1446: 17 of cap free
Amount of items: 3
Items: 
Size: 697495 Color: 4
Size: 166777 Color: 0
Size: 135712 Color: 2

Bin 1447: 17 of cap free
Amount of items: 2
Items: 
Size: 718523 Color: 2
Size: 281461 Color: 1

Bin 1448: 17 of cap free
Amount of items: 2
Items: 
Size: 539401 Color: 0
Size: 460583 Color: 3

Bin 1449: 17 of cap free
Amount of items: 3
Items: 
Size: 461964 Color: 1
Size: 291004 Color: 3
Size: 247016 Color: 4

Bin 1450: 17 of cap free
Amount of items: 2
Items: 
Size: 685254 Color: 2
Size: 314730 Color: 4

Bin 1451: 17 of cap free
Amount of items: 2
Items: 
Size: 671739 Color: 2
Size: 328245 Color: 4

Bin 1452: 17 of cap free
Amount of items: 3
Items: 
Size: 371698 Color: 4
Size: 370972 Color: 1
Size: 257314 Color: 3

Bin 1453: 17 of cap free
Amount of items: 2
Items: 
Size: 517663 Color: 1
Size: 482321 Color: 3

Bin 1454: 17 of cap free
Amount of items: 2
Items: 
Size: 581321 Color: 1
Size: 418663 Color: 0

Bin 1455: 17 of cap free
Amount of items: 2
Items: 
Size: 585269 Color: 0
Size: 414715 Color: 1

Bin 1456: 17 of cap free
Amount of items: 2
Items: 
Size: 632611 Color: 3
Size: 367373 Color: 0

Bin 1457: 17 of cap free
Amount of items: 3
Items: 
Size: 633250 Color: 2
Size: 183373 Color: 1
Size: 183361 Color: 1

Bin 1458: 17 of cap free
Amount of items: 2
Items: 
Size: 654658 Color: 4
Size: 345326 Color: 0

Bin 1459: 17 of cap free
Amount of items: 2
Items: 
Size: 655996 Color: 0
Size: 343988 Color: 1

Bin 1460: 17 of cap free
Amount of items: 2
Items: 
Size: 668281 Color: 0
Size: 331703 Color: 4

Bin 1461: 17 of cap free
Amount of items: 2
Items: 
Size: 670094 Color: 2
Size: 329890 Color: 4

Bin 1462: 17 of cap free
Amount of items: 2
Items: 
Size: 684090 Color: 2
Size: 315894 Color: 0

Bin 1463: 17 of cap free
Amount of items: 2
Items: 
Size: 733154 Color: 1
Size: 266830 Color: 0

Bin 1464: 17 of cap free
Amount of items: 2
Items: 
Size: 750056 Color: 4
Size: 249928 Color: 2

Bin 1465: 18 of cap free
Amount of items: 2
Items: 
Size: 765142 Color: 3
Size: 234841 Color: 2

Bin 1466: 18 of cap free
Amount of items: 2
Items: 
Size: 727588 Color: 3
Size: 272395 Color: 2

Bin 1467: 18 of cap free
Amount of items: 3
Items: 
Size: 686050 Color: 3
Size: 162861 Color: 4
Size: 151072 Color: 2

Bin 1468: 18 of cap free
Amount of items: 3
Items: 
Size: 413218 Color: 3
Size: 298508 Color: 4
Size: 288257 Color: 2

Bin 1469: 18 of cap free
Amount of items: 2
Items: 
Size: 631303 Color: 4
Size: 368680 Color: 0

Bin 1470: 18 of cap free
Amount of items: 3
Items: 
Size: 463344 Color: 3
Size: 304497 Color: 2
Size: 232142 Color: 1

Bin 1471: 18 of cap free
Amount of items: 2
Items: 
Size: 670348 Color: 3
Size: 329635 Color: 4

Bin 1472: 18 of cap free
Amount of items: 2
Items: 
Size: 686367 Color: 2
Size: 313616 Color: 3

Bin 1473: 18 of cap free
Amount of items: 2
Items: 
Size: 676400 Color: 2
Size: 323583 Color: 1

Bin 1474: 18 of cap free
Amount of items: 2
Items: 
Size: 684442 Color: 4
Size: 315541 Color: 1

Bin 1475: 18 of cap free
Amount of items: 2
Items: 
Size: 795538 Color: 3
Size: 204445 Color: 1

Bin 1476: 18 of cap free
Amount of items: 3
Items: 
Size: 668192 Color: 3
Size: 228277 Color: 3
Size: 103514 Color: 2

Bin 1477: 18 of cap free
Amount of items: 3
Items: 
Size: 475652 Color: 4
Size: 281174 Color: 0
Size: 243157 Color: 0

Bin 1478: 18 of cap free
Amount of items: 3
Items: 
Size: 607722 Color: 2
Size: 234771 Color: 3
Size: 157490 Color: 4

Bin 1479: 18 of cap free
Amount of items: 2
Items: 
Size: 796524 Color: 4
Size: 203459 Color: 2

Bin 1480: 18 of cap free
Amount of items: 2
Items: 
Size: 764863 Color: 2
Size: 235120 Color: 1

Bin 1481: 18 of cap free
Amount of items: 2
Items: 
Size: 775760 Color: 1
Size: 224223 Color: 3

Bin 1482: 18 of cap free
Amount of items: 2
Items: 
Size: 595619 Color: 4
Size: 404364 Color: 2

Bin 1483: 18 of cap free
Amount of items: 3
Items: 
Size: 556964 Color: 1
Size: 221683 Color: 4
Size: 221336 Color: 2

Bin 1484: 18 of cap free
Amount of items: 3
Items: 
Size: 555141 Color: 1
Size: 223043 Color: 0
Size: 221799 Color: 0

Bin 1485: 18 of cap free
Amount of items: 3
Items: 
Size: 606365 Color: 2
Size: 230386 Color: 3
Size: 163232 Color: 3

Bin 1486: 18 of cap free
Amount of items: 2
Items: 
Size: 640315 Color: 4
Size: 359668 Color: 2

Bin 1487: 18 of cap free
Amount of items: 3
Items: 
Size: 606671 Color: 4
Size: 196774 Color: 3
Size: 196538 Color: 4

Bin 1488: 18 of cap free
Amount of items: 2
Items: 
Size: 726076 Color: 1
Size: 273907 Color: 4

Bin 1489: 18 of cap free
Amount of items: 2
Items: 
Size: 545966 Color: 0
Size: 454017 Color: 3

Bin 1490: 18 of cap free
Amount of items: 3
Items: 
Size: 508824 Color: 0
Size: 245842 Color: 3
Size: 245317 Color: 0

Bin 1491: 18 of cap free
Amount of items: 2
Items: 
Size: 681472 Color: 1
Size: 318511 Color: 2

Bin 1492: 18 of cap free
Amount of items: 2
Items: 
Size: 518804 Color: 4
Size: 481179 Color: 1

Bin 1493: 18 of cap free
Amount of items: 2
Items: 
Size: 530030 Color: 1
Size: 469953 Color: 2

Bin 1494: 18 of cap free
Amount of items: 2
Items: 
Size: 551038 Color: 3
Size: 448945 Color: 4

Bin 1495: 18 of cap free
Amount of items: 2
Items: 
Size: 593982 Color: 2
Size: 406001 Color: 0

Bin 1496: 18 of cap free
Amount of items: 2
Items: 
Size: 602639 Color: 3
Size: 397344 Color: 4

Bin 1497: 18 of cap free
Amount of items: 2
Items: 
Size: 650319 Color: 1
Size: 349664 Color: 4

Bin 1498: 18 of cap free
Amount of items: 2
Items: 
Size: 660201 Color: 4
Size: 339782 Color: 3

Bin 1499: 18 of cap free
Amount of items: 2
Items: 
Size: 670974 Color: 1
Size: 329009 Color: 2

Bin 1500: 18 of cap free
Amount of items: 2
Items: 
Size: 750072 Color: 1
Size: 249911 Color: 0

Bin 1501: 19 of cap free
Amount of items: 3
Items: 
Size: 417984 Color: 3
Size: 297286 Color: 1
Size: 284712 Color: 3

Bin 1502: 19 of cap free
Amount of items: 2
Items: 
Size: 511855 Color: 0
Size: 488127 Color: 4

Bin 1503: 19 of cap free
Amount of items: 3
Items: 
Size: 547959 Color: 1
Size: 226250 Color: 0
Size: 225773 Color: 4

Bin 1504: 19 of cap free
Amount of items: 2
Items: 
Size: 755115 Color: 4
Size: 244867 Color: 1

Bin 1505: 19 of cap free
Amount of items: 3
Items: 
Size: 657234 Color: 0
Size: 180824 Color: 1
Size: 161924 Color: 2

Bin 1506: 19 of cap free
Amount of items: 2
Items: 
Size: 759338 Color: 2
Size: 240644 Color: 3

Bin 1507: 19 of cap free
Amount of items: 2
Items: 
Size: 646063 Color: 0
Size: 353919 Color: 4

Bin 1508: 19 of cap free
Amount of items: 2
Items: 
Size: 614478 Color: 4
Size: 385504 Color: 1

Bin 1509: 19 of cap free
Amount of items: 3
Items: 
Size: 466949 Color: 3
Size: 294378 Color: 4
Size: 238655 Color: 1

Bin 1510: 19 of cap free
Amount of items: 2
Items: 
Size: 566480 Color: 4
Size: 433502 Color: 0

Bin 1511: 19 of cap free
Amount of items: 2
Items: 
Size: 779608 Color: 1
Size: 220374 Color: 2

Bin 1512: 19 of cap free
Amount of items: 3
Items: 
Size: 504175 Color: 1
Size: 249587 Color: 4
Size: 246220 Color: 3

Bin 1513: 19 of cap free
Amount of items: 2
Items: 
Size: 535255 Color: 1
Size: 464727 Color: 0

Bin 1514: 19 of cap free
Amount of items: 2
Items: 
Size: 642048 Color: 4
Size: 357934 Color: 2

Bin 1515: 19 of cap free
Amount of items: 2
Items: 
Size: 688990 Color: 3
Size: 310992 Color: 4

Bin 1516: 19 of cap free
Amount of items: 2
Items: 
Size: 545426 Color: 3
Size: 454556 Color: 4

Bin 1517: 19 of cap free
Amount of items: 2
Items: 
Size: 559644 Color: 1
Size: 440338 Color: 2

Bin 1518: 19 of cap free
Amount of items: 2
Items: 
Size: 709917 Color: 1
Size: 290065 Color: 2

Bin 1519: 19 of cap free
Amount of items: 2
Items: 
Size: 533834 Color: 3
Size: 466148 Color: 2

Bin 1520: 19 of cap free
Amount of items: 2
Items: 
Size: 592056 Color: 0
Size: 407926 Color: 2

Bin 1521: 19 of cap free
Amount of items: 2
Items: 
Size: 752899 Color: 1
Size: 247083 Color: 2

Bin 1522: 19 of cap free
Amount of items: 2
Items: 
Size: 628235 Color: 4
Size: 371747 Color: 0

Bin 1523: 19 of cap free
Amount of items: 2
Items: 
Size: 520059 Color: 2
Size: 479923 Color: 3

Bin 1524: 19 of cap free
Amount of items: 2
Items: 
Size: 550489 Color: 4
Size: 449493 Color: 1

Bin 1525: 19 of cap free
Amount of items: 3
Items: 
Size: 413507 Color: 3
Size: 297987 Color: 0
Size: 288488 Color: 3

Bin 1526: 19 of cap free
Amount of items: 2
Items: 
Size: 507551 Color: 3
Size: 492431 Color: 0

Bin 1527: 19 of cap free
Amount of items: 3
Items: 
Size: 514595 Color: 0
Size: 243286 Color: 1
Size: 242101 Color: 1

Bin 1528: 19 of cap free
Amount of items: 2
Items: 
Size: 554977 Color: 2
Size: 445005 Color: 3

Bin 1529: 19 of cap free
Amount of items: 2
Items: 
Size: 561565 Color: 2
Size: 438417 Color: 4

Bin 1530: 19 of cap free
Amount of items: 2
Items: 
Size: 570214 Color: 2
Size: 429768 Color: 4

Bin 1531: 19 of cap free
Amount of items: 2
Items: 
Size: 577741 Color: 1
Size: 422241 Color: 0

Bin 1532: 19 of cap free
Amount of items: 2
Items: 
Size: 626891 Color: 0
Size: 373091 Color: 4

Bin 1533: 19 of cap free
Amount of items: 2
Items: 
Size: 660174 Color: 0
Size: 339808 Color: 1

Bin 1534: 19 of cap free
Amount of items: 2
Items: 
Size: 663131 Color: 3
Size: 336851 Color: 1

Bin 1535: 19 of cap free
Amount of items: 2
Items: 
Size: 665328 Color: 1
Size: 334654 Color: 4

Bin 1536: 19 of cap free
Amount of items: 2
Items: 
Size: 686210 Color: 2
Size: 313772 Color: 4

Bin 1537: 19 of cap free
Amount of items: 2
Items: 
Size: 689169 Color: 0
Size: 310813 Color: 2

Bin 1538: 19 of cap free
Amount of items: 2
Items: 
Size: 709015 Color: 3
Size: 290967 Color: 4

Bin 1539: 19 of cap free
Amount of items: 2
Items: 
Size: 747579 Color: 4
Size: 252403 Color: 3

Bin 1540: 19 of cap free
Amount of items: 2
Items: 
Size: 757483 Color: 1
Size: 242499 Color: 0

Bin 1541: 19 of cap free
Amount of items: 2
Items: 
Size: 766201 Color: 2
Size: 233781 Color: 4

Bin 1542: 19 of cap free
Amount of items: 2
Items: 
Size: 788669 Color: 1
Size: 211313 Color: 3

Bin 1543: 19 of cap free
Amount of items: 2
Items: 
Size: 789262 Color: 4
Size: 210720 Color: 0

Bin 1544: 19 of cap free
Amount of items: 2
Items: 
Size: 798046 Color: 2
Size: 201936 Color: 4

Bin 1545: 20 of cap free
Amount of items: 3
Items: 
Size: 339923 Color: 3
Size: 337003 Color: 1
Size: 323055 Color: 3

Bin 1546: 20 of cap free
Amount of items: 3
Items: 
Size: 472952 Color: 3
Size: 378862 Color: 0
Size: 148167 Color: 2

Bin 1547: 20 of cap free
Amount of items: 3
Items: 
Size: 624274 Color: 1
Size: 201398 Color: 4
Size: 174309 Color: 0

Bin 1548: 20 of cap free
Amount of items: 2
Items: 
Size: 600927 Color: 3
Size: 399054 Color: 4

Bin 1549: 20 of cap free
Amount of items: 2
Items: 
Size: 714842 Color: 1
Size: 285139 Color: 2

Bin 1550: 20 of cap free
Amount of items: 2
Items: 
Size: 653225 Color: 4
Size: 346756 Color: 3

Bin 1551: 20 of cap free
Amount of items: 2
Items: 
Size: 765998 Color: 0
Size: 233983 Color: 4

Bin 1552: 20 of cap free
Amount of items: 2
Items: 
Size: 508282 Color: 0
Size: 491699 Color: 1

Bin 1553: 20 of cap free
Amount of items: 2
Items: 
Size: 682002 Color: 3
Size: 317979 Color: 4

Bin 1554: 20 of cap free
Amount of items: 2
Items: 
Size: 575519 Color: 1
Size: 424462 Color: 0

Bin 1555: 20 of cap free
Amount of items: 2
Items: 
Size: 643221 Color: 4
Size: 356760 Color: 0

Bin 1556: 20 of cap free
Amount of items: 2
Items: 
Size: 740329 Color: 4
Size: 259652 Color: 1

Bin 1557: 20 of cap free
Amount of items: 2
Items: 
Size: 637597 Color: 1
Size: 362384 Color: 0

Bin 1558: 20 of cap free
Amount of items: 2
Items: 
Size: 682475 Color: 4
Size: 317506 Color: 0

Bin 1559: 20 of cap free
Amount of items: 2
Items: 
Size: 512020 Color: 4
Size: 487961 Color: 3

Bin 1560: 20 of cap free
Amount of items: 2
Items: 
Size: 523834 Color: 3
Size: 476147 Color: 1

Bin 1561: 20 of cap free
Amount of items: 2
Items: 
Size: 537221 Color: 0
Size: 462760 Color: 4

Bin 1562: 20 of cap free
Amount of items: 2
Items: 
Size: 548792 Color: 2
Size: 451189 Color: 0

Bin 1563: 20 of cap free
Amount of items: 2
Items: 
Size: 596706 Color: 3
Size: 403275 Color: 1

Bin 1564: 20 of cap free
Amount of items: 2
Items: 
Size: 598119 Color: 4
Size: 401862 Color: 1

Bin 1565: 20 of cap free
Amount of items: 2
Items: 
Size: 622966 Color: 4
Size: 377015 Color: 0

Bin 1566: 20 of cap free
Amount of items: 2
Items: 
Size: 634771 Color: 3
Size: 365210 Color: 0

Bin 1567: 20 of cap free
Amount of items: 2
Items: 
Size: 669539 Color: 1
Size: 330442 Color: 0

Bin 1568: 20 of cap free
Amount of items: 2
Items: 
Size: 758826 Color: 0
Size: 241155 Color: 3

Bin 1569: 20 of cap free
Amount of items: 2
Items: 
Size: 798704 Color: 3
Size: 201277 Color: 1

Bin 1570: 21 of cap free
Amount of items: 3
Items: 
Size: 351387 Color: 2
Size: 338177 Color: 0
Size: 310416 Color: 1

Bin 1571: 21 of cap free
Amount of items: 3
Items: 
Size: 386067 Color: 4
Size: 377754 Color: 4
Size: 236159 Color: 1

Bin 1572: 21 of cap free
Amount of items: 2
Items: 
Size: 583736 Color: 0
Size: 416244 Color: 4

Bin 1573: 21 of cap free
Amount of items: 3
Items: 
Size: 710159 Color: 4
Size: 175774 Color: 4
Size: 114047 Color: 3

Bin 1574: 21 of cap free
Amount of items: 2
Items: 
Size: 756901 Color: 3
Size: 243079 Color: 2

Bin 1575: 21 of cap free
Amount of items: 2
Items: 
Size: 522867 Color: 0
Size: 477113 Color: 2

Bin 1576: 21 of cap free
Amount of items: 2
Items: 
Size: 759097 Color: 1
Size: 240883 Color: 0

Bin 1577: 21 of cap free
Amount of items: 2
Items: 
Size: 562977 Color: 3
Size: 437003 Color: 4

Bin 1578: 21 of cap free
Amount of items: 2
Items: 
Size: 680369 Color: 2
Size: 319611 Color: 0

Bin 1579: 21 of cap free
Amount of items: 2
Items: 
Size: 502147 Color: 4
Size: 497833 Color: 0

Bin 1580: 21 of cap free
Amount of items: 2
Items: 
Size: 574594 Color: 3
Size: 425386 Color: 2

Bin 1581: 21 of cap free
Amount of items: 2
Items: 
Size: 758770 Color: 1
Size: 241210 Color: 4

Bin 1582: 21 of cap free
Amount of items: 2
Items: 
Size: 515665 Color: 1
Size: 484315 Color: 2

Bin 1583: 21 of cap free
Amount of items: 2
Items: 
Size: 541516 Color: 3
Size: 458464 Color: 2

Bin 1584: 21 of cap free
Amount of items: 2
Items: 
Size: 553164 Color: 3
Size: 446816 Color: 4

Bin 1585: 21 of cap free
Amount of items: 2
Items: 
Size: 560193 Color: 1
Size: 439787 Color: 3

Bin 1586: 21 of cap free
Amount of items: 2
Items: 
Size: 567756 Color: 3
Size: 432224 Color: 0

Bin 1587: 21 of cap free
Amount of items: 2
Items: 
Size: 581835 Color: 3
Size: 418145 Color: 2

Bin 1588: 21 of cap free
Amount of items: 2
Items: 
Size: 695303 Color: 1
Size: 304677 Color: 0

Bin 1589: 21 of cap free
Amount of items: 2
Items: 
Size: 695537 Color: 0
Size: 304443 Color: 1

Bin 1590: 21 of cap free
Amount of items: 2
Items: 
Size: 728969 Color: 1
Size: 271011 Color: 4

Bin 1591: 21 of cap free
Amount of items: 2
Items: 
Size: 739426 Color: 0
Size: 260554 Color: 3

Bin 1592: 21 of cap free
Amount of items: 2
Items: 
Size: 748961 Color: 4
Size: 251019 Color: 0

Bin 1593: 21 of cap free
Amount of items: 2
Items: 
Size: 789937 Color: 3
Size: 210043 Color: 0

Bin 1594: 21 of cap free
Amount of items: 2
Items: 
Size: 795566 Color: 2
Size: 204414 Color: 1

Bin 1595: 21 of cap free
Amount of items: 2
Items: 
Size: 798027 Color: 3
Size: 201953 Color: 1

Bin 1596: 22 of cap free
Amount of items: 2
Items: 
Size: 502323 Color: 0
Size: 497656 Color: 3

Bin 1597: 22 of cap free
Amount of items: 3
Items: 
Size: 601971 Color: 4
Size: 199755 Color: 3
Size: 198253 Color: 4

Bin 1598: 22 of cap free
Amount of items: 3
Items: 
Size: 355956 Color: 1
Size: 355121 Color: 0
Size: 288902 Color: 4

Bin 1599: 22 of cap free
Amount of items: 2
Items: 
Size: 726685 Color: 3
Size: 273294 Color: 2

Bin 1600: 22 of cap free
Amount of items: 3
Items: 
Size: 621200 Color: 0
Size: 190411 Color: 2
Size: 188368 Color: 4

Bin 1601: 22 of cap free
Amount of items: 2
Items: 
Size: 651389 Color: 0
Size: 348590 Color: 1

Bin 1602: 22 of cap free
Amount of items: 2
Items: 
Size: 532373 Color: 1
Size: 467606 Color: 3

Bin 1603: 22 of cap free
Amount of items: 2
Items: 
Size: 754565 Color: 1
Size: 245414 Color: 3

Bin 1604: 22 of cap free
Amount of items: 2
Items: 
Size: 618609 Color: 4
Size: 381370 Color: 3

Bin 1605: 22 of cap free
Amount of items: 2
Items: 
Size: 729367 Color: 0
Size: 270612 Color: 3

Bin 1606: 22 of cap free
Amount of items: 2
Items: 
Size: 623676 Color: 1
Size: 376303 Color: 0

Bin 1607: 22 of cap free
Amount of items: 2
Items: 
Size: 658044 Color: 1
Size: 341935 Color: 3

Bin 1608: 22 of cap free
Amount of items: 2
Items: 
Size: 675660 Color: 2
Size: 324319 Color: 1

Bin 1609: 22 of cap free
Amount of items: 2
Items: 
Size: 716835 Color: 4
Size: 283144 Color: 3

Bin 1610: 22 of cap free
Amount of items: 2
Items: 
Size: 524882 Color: 2
Size: 475097 Color: 4

Bin 1611: 22 of cap free
Amount of items: 2
Items: 
Size: 537616 Color: 0
Size: 462363 Color: 3

Bin 1612: 22 of cap free
Amount of items: 2
Items: 
Size: 553614 Color: 1
Size: 446365 Color: 2

Bin 1613: 22 of cap free
Amount of items: 2
Items: 
Size: 570883 Color: 1
Size: 429096 Color: 2

Bin 1614: 22 of cap free
Amount of items: 2
Items: 
Size: 596220 Color: 2
Size: 403759 Color: 1

Bin 1615: 22 of cap free
Amount of items: 2
Items: 
Size: 615356 Color: 0
Size: 384623 Color: 4

Bin 1616: 22 of cap free
Amount of items: 2
Items: 
Size: 619993 Color: 1
Size: 379986 Color: 4

Bin 1617: 22 of cap free
Amount of items: 3
Items: 
Size: 624033 Color: 1
Size: 188238 Color: 2
Size: 187708 Color: 4

Bin 1618: 22 of cap free
Amount of items: 2
Items: 
Size: 634011 Color: 1
Size: 365968 Color: 0

Bin 1619: 22 of cap free
Amount of items: 2
Items: 
Size: 634329 Color: 4
Size: 365650 Color: 0

Bin 1620: 22 of cap free
Amount of items: 2
Items: 
Size: 645259 Color: 0
Size: 354720 Color: 3

Bin 1621: 22 of cap free
Amount of items: 2
Items: 
Size: 682012 Color: 4
Size: 317967 Color: 0

Bin 1622: 22 of cap free
Amount of items: 2
Items: 
Size: 701350 Color: 0
Size: 298629 Color: 3

Bin 1623: 22 of cap free
Amount of items: 2
Items: 
Size: 728092 Color: 2
Size: 271887 Color: 0

Bin 1624: 22 of cap free
Amount of items: 2
Items: 
Size: 761010 Color: 2
Size: 238969 Color: 4

Bin 1625: 22 of cap free
Amount of items: 2
Items: 
Size: 782003 Color: 0
Size: 217976 Color: 1

Bin 1626: 23 of cap free
Amount of items: 2
Items: 
Size: 719006 Color: 2
Size: 280972 Color: 4

Bin 1627: 23 of cap free
Amount of items: 2
Items: 
Size: 699856 Color: 4
Size: 300122 Color: 1

Bin 1628: 23 of cap free
Amount of items: 2
Items: 
Size: 797911 Color: 2
Size: 202067 Color: 4

Bin 1629: 23 of cap free
Amount of items: 2
Items: 
Size: 731350 Color: 1
Size: 268628 Color: 2

Bin 1630: 23 of cap free
Amount of items: 2
Items: 
Size: 753343 Color: 3
Size: 246635 Color: 4

Bin 1631: 23 of cap free
Amount of items: 2
Items: 
Size: 545507 Color: 1
Size: 454471 Color: 3

Bin 1632: 23 of cap free
Amount of items: 3
Items: 
Size: 655783 Color: 1
Size: 174610 Color: 3
Size: 169585 Color: 3

Bin 1633: 23 of cap free
Amount of items: 2
Items: 
Size: 769331 Color: 4
Size: 230647 Color: 3

Bin 1634: 23 of cap free
Amount of items: 2
Items: 
Size: 643973 Color: 2
Size: 356005 Color: 3

Bin 1635: 23 of cap free
Amount of items: 2
Items: 
Size: 615963 Color: 2
Size: 384015 Color: 4

Bin 1636: 23 of cap free
Amount of items: 2
Items: 
Size: 516730 Color: 4
Size: 483248 Color: 0

Bin 1637: 23 of cap free
Amount of items: 2
Items: 
Size: 791183 Color: 3
Size: 208795 Color: 2

Bin 1638: 23 of cap free
Amount of items: 2
Items: 
Size: 749652 Color: 4
Size: 250326 Color: 1

Bin 1639: 23 of cap free
Amount of items: 2
Items: 
Size: 788720 Color: 4
Size: 211258 Color: 1

Bin 1640: 23 of cap free
Amount of items: 2
Items: 
Size: 664943 Color: 1
Size: 335035 Color: 3

Bin 1641: 23 of cap free
Amount of items: 2
Items: 
Size: 755643 Color: 2
Size: 244335 Color: 3

Bin 1642: 23 of cap free
Amount of items: 2
Items: 
Size: 684964 Color: 0
Size: 315014 Color: 1

Bin 1643: 23 of cap free
Amount of items: 2
Items: 
Size: 605763 Color: 0
Size: 394215 Color: 2

Bin 1644: 23 of cap free
Amount of items: 2
Items: 
Size: 521139 Color: 3
Size: 478839 Color: 1

Bin 1645: 23 of cap free
Amount of items: 2
Items: 
Size: 529166 Color: 4
Size: 470812 Color: 1

Bin 1646: 23 of cap free
Amount of items: 2
Items: 
Size: 533932 Color: 4
Size: 466046 Color: 3

Bin 1647: 23 of cap free
Amount of items: 2
Items: 
Size: 561178 Color: 1
Size: 438800 Color: 4

Bin 1648: 23 of cap free
Amount of items: 2
Items: 
Size: 593991 Color: 4
Size: 405987 Color: 1

Bin 1649: 23 of cap free
Amount of items: 2
Items: 
Size: 617850 Color: 0
Size: 382128 Color: 1

Bin 1650: 23 of cap free
Amount of items: 2
Items: 
Size: 634344 Color: 0
Size: 365634 Color: 3

Bin 1651: 23 of cap free
Amount of items: 3
Items: 
Size: 637913 Color: 2
Size: 181911 Color: 0
Size: 180154 Color: 1

Bin 1652: 23 of cap free
Amount of items: 2
Items: 
Size: 667837 Color: 4
Size: 332141 Color: 3

Bin 1653: 23 of cap free
Amount of items: 2
Items: 
Size: 691429 Color: 3
Size: 308549 Color: 4

Bin 1654: 23 of cap free
Amount of items: 2
Items: 
Size: 699940 Color: 4
Size: 300038 Color: 0

Bin 1655: 23 of cap free
Amount of items: 2
Items: 
Size: 710742 Color: 0
Size: 289236 Color: 4

Bin 1656: 23 of cap free
Amount of items: 2
Items: 
Size: 737700 Color: 0
Size: 262278 Color: 3

Bin 1657: 23 of cap free
Amount of items: 2
Items: 
Size: 740583 Color: 4
Size: 259395 Color: 3

Bin 1658: 23 of cap free
Amount of items: 2
Items: 
Size: 747152 Color: 3
Size: 252826 Color: 0

Bin 1659: 23 of cap free
Amount of items: 2
Items: 
Size: 757834 Color: 4
Size: 242144 Color: 3

Bin 1660: 23 of cap free
Amount of items: 2
Items: 
Size: 768925 Color: 2
Size: 231053 Color: 0

Bin 1661: 23 of cap free
Amount of items: 2
Items: 
Size: 792582 Color: 2
Size: 207396 Color: 0

Bin 1662: 24 of cap free
Amount of items: 2
Items: 
Size: 631049 Color: 4
Size: 368928 Color: 2

Bin 1663: 24 of cap free
Amount of items: 2
Items: 
Size: 714165 Color: 1
Size: 285812 Color: 4

Bin 1664: 24 of cap free
Amount of items: 3
Items: 
Size: 643542 Color: 1
Size: 178888 Color: 2
Size: 177547 Color: 0

Bin 1665: 24 of cap free
Amount of items: 2
Items: 
Size: 723337 Color: 2
Size: 276640 Color: 4

Bin 1666: 24 of cap free
Amount of items: 2
Items: 
Size: 556822 Color: 0
Size: 443155 Color: 3

Bin 1667: 24 of cap free
Amount of items: 2
Items: 
Size: 604631 Color: 4
Size: 395346 Color: 3

Bin 1668: 24 of cap free
Amount of items: 3
Items: 
Size: 631224 Color: 0
Size: 184973 Color: 1
Size: 183780 Color: 3

Bin 1669: 24 of cap free
Amount of items: 2
Items: 
Size: 667284 Color: 2
Size: 332693 Color: 3

Bin 1670: 24 of cap free
Amount of items: 2
Items: 
Size: 729405 Color: 1
Size: 270572 Color: 0

Bin 1671: 24 of cap free
Amount of items: 2
Items: 
Size: 723148 Color: 0
Size: 276829 Color: 1

Bin 1672: 24 of cap free
Amount of items: 2
Items: 
Size: 619777 Color: 1
Size: 380200 Color: 3

Bin 1673: 24 of cap free
Amount of items: 2
Items: 
Size: 546034 Color: 0
Size: 453943 Color: 2

Bin 1674: 24 of cap free
Amount of items: 2
Items: 
Size: 510998 Color: 3
Size: 488979 Color: 4

Bin 1675: 24 of cap free
Amount of items: 2
Items: 
Size: 524218 Color: 3
Size: 475759 Color: 1

Bin 1676: 24 of cap free
Amount of items: 2
Items: 
Size: 533612 Color: 0
Size: 466365 Color: 4

Bin 1677: 24 of cap free
Amount of items: 2
Items: 
Size: 553338 Color: 4
Size: 446639 Color: 3

Bin 1678: 24 of cap free
Amount of items: 2
Items: 
Size: 565334 Color: 1
Size: 434643 Color: 2

Bin 1679: 24 of cap free
Amount of items: 2
Items: 
Size: 572203 Color: 4
Size: 427774 Color: 0

Bin 1680: 24 of cap free
Amount of items: 2
Items: 
Size: 583010 Color: 2
Size: 416967 Color: 3

Bin 1681: 24 of cap free
Amount of items: 2
Items: 
Size: 620644 Color: 3
Size: 379333 Color: 2

Bin 1682: 24 of cap free
Amount of items: 2
Items: 
Size: 674940 Color: 2
Size: 325037 Color: 4

Bin 1683: 24 of cap free
Amount of items: 2
Items: 
Size: 719040 Color: 2
Size: 280937 Color: 4

Bin 1684: 24 of cap free
Amount of items: 2
Items: 
Size: 733490 Color: 1
Size: 266487 Color: 3

Bin 1685: 24 of cap free
Amount of items: 2
Items: 
Size: 748971 Color: 0
Size: 251006 Color: 4

Bin 1686: 25 of cap free
Amount of items: 3
Items: 
Size: 660962 Color: 3
Size: 184423 Color: 2
Size: 154591 Color: 0

Bin 1687: 25 of cap free
Amount of items: 2
Items: 
Size: 605393 Color: 4
Size: 394583 Color: 2

Bin 1688: 25 of cap free
Amount of items: 2
Items: 
Size: 745970 Color: 2
Size: 254006 Color: 0

Bin 1689: 25 of cap free
Amount of items: 3
Items: 
Size: 446391 Color: 1
Size: 336991 Color: 2
Size: 216594 Color: 0

Bin 1690: 25 of cap free
Amount of items: 3
Items: 
Size: 629373 Color: 3
Size: 185839 Color: 0
Size: 184764 Color: 0

Bin 1691: 25 of cap free
Amount of items: 3
Items: 
Size: 610569 Color: 1
Size: 195715 Color: 2
Size: 193692 Color: 2

Bin 1692: 25 of cap free
Amount of items: 2
Items: 
Size: 643490 Color: 3
Size: 356486 Color: 4

Bin 1693: 25 of cap free
Amount of items: 2
Items: 
Size: 621572 Color: 4
Size: 378404 Color: 1

Bin 1694: 25 of cap free
Amount of items: 2
Items: 
Size: 544980 Color: 4
Size: 454996 Color: 1

Bin 1695: 25 of cap free
Amount of items: 2
Items: 
Size: 578095 Color: 2
Size: 421881 Color: 4

Bin 1696: 25 of cap free
Amount of items: 2
Items: 
Size: 786823 Color: 2
Size: 213153 Color: 3

Bin 1697: 25 of cap free
Amount of items: 2
Items: 
Size: 592487 Color: 0
Size: 407489 Color: 3

Bin 1698: 25 of cap free
Amount of items: 2
Items: 
Size: 683537 Color: 0
Size: 316439 Color: 2

Bin 1699: 25 of cap free
Amount of items: 2
Items: 
Size: 546875 Color: 1
Size: 453101 Color: 0

Bin 1700: 25 of cap free
Amount of items: 2
Items: 
Size: 641495 Color: 1
Size: 358481 Color: 0

Bin 1701: 25 of cap free
Amount of items: 2
Items: 
Size: 513433 Color: 0
Size: 486543 Color: 4

Bin 1702: 25 of cap free
Amount of items: 2
Items: 
Size: 520198 Color: 4
Size: 479778 Color: 0

Bin 1703: 25 of cap free
Amount of items: 2
Items: 
Size: 528373 Color: 1
Size: 471603 Color: 0

Bin 1704: 25 of cap free
Amount of items: 2
Items: 
Size: 565773 Color: 3
Size: 434203 Color: 4

Bin 1705: 25 of cap free
Amount of items: 2
Items: 
Size: 568031 Color: 0
Size: 431945 Color: 1

Bin 1706: 25 of cap free
Amount of items: 2
Items: 
Size: 573411 Color: 3
Size: 426565 Color: 4

Bin 1707: 25 of cap free
Amount of items: 2
Items: 
Size: 577697 Color: 3
Size: 422279 Color: 2

Bin 1708: 25 of cap free
Amount of items: 2
Items: 
Size: 581199 Color: 3
Size: 418777 Color: 0

Bin 1709: 25 of cap free
Amount of items: 2
Items: 
Size: 633088 Color: 0
Size: 366888 Color: 1

Bin 1710: 25 of cap free
Amount of items: 2
Items: 
Size: 646835 Color: 4
Size: 353141 Color: 0

Bin 1711: 25 of cap free
Amount of items: 2
Items: 
Size: 716093 Color: 3
Size: 283883 Color: 1

Bin 1712: 25 of cap free
Amount of items: 2
Items: 
Size: 757218 Color: 2
Size: 242758 Color: 4

Bin 1713: 25 of cap free
Amount of items: 2
Items: 
Size: 782609 Color: 3
Size: 217367 Color: 2

Bin 1714: 25 of cap free
Amount of items: 2
Items: 
Size: 785569 Color: 0
Size: 214407 Color: 3

Bin 1715: 26 of cap free
Amount of items: 3
Items: 
Size: 688177 Color: 4
Size: 156411 Color: 0
Size: 155387 Color: 4

Bin 1716: 26 of cap free
Amount of items: 3
Items: 
Size: 704151 Color: 1
Size: 149171 Color: 2
Size: 146653 Color: 4

Bin 1717: 26 of cap free
Amount of items: 2
Items: 
Size: 568600 Color: 3
Size: 431375 Color: 0

Bin 1718: 26 of cap free
Amount of items: 2
Items: 
Size: 580445 Color: 0
Size: 419530 Color: 2

Bin 1719: 26 of cap free
Amount of items: 3
Items: 
Size: 549392 Color: 2
Size: 231410 Color: 4
Size: 219173 Color: 3

Bin 1720: 26 of cap free
Amount of items: 2
Items: 
Size: 674452 Color: 1
Size: 325523 Color: 2

Bin 1721: 26 of cap free
Amount of items: 2
Items: 
Size: 641457 Color: 0
Size: 358518 Color: 3

Bin 1722: 26 of cap free
Amount of items: 2
Items: 
Size: 777768 Color: 1
Size: 222207 Color: 4

Bin 1723: 26 of cap free
Amount of items: 2
Items: 
Size: 696445 Color: 0
Size: 303530 Color: 1

Bin 1724: 26 of cap free
Amount of items: 3
Items: 
Size: 385929 Color: 1
Size: 385300 Color: 3
Size: 228746 Color: 2

Bin 1725: 26 of cap free
Amount of items: 2
Items: 
Size: 703319 Color: 1
Size: 296656 Color: 4

Bin 1726: 26 of cap free
Amount of items: 2
Items: 
Size: 559460 Color: 1
Size: 440515 Color: 2

Bin 1727: 26 of cap free
Amount of items: 2
Items: 
Size: 778183 Color: 2
Size: 221792 Color: 0

Bin 1728: 26 of cap free
Amount of items: 2
Items: 
Size: 503904 Color: 1
Size: 496071 Color: 4

Bin 1729: 26 of cap free
Amount of items: 2
Items: 
Size: 514872 Color: 2
Size: 485103 Color: 4

Bin 1730: 26 of cap free
Amount of items: 2
Items: 
Size: 518057 Color: 3
Size: 481918 Color: 1

Bin 1731: 26 of cap free
Amount of items: 2
Items: 
Size: 525584 Color: 0
Size: 474391 Color: 4

Bin 1732: 26 of cap free
Amount of items: 2
Items: 
Size: 554816 Color: 4
Size: 445159 Color: 2

Bin 1733: 26 of cap free
Amount of items: 2
Items: 
Size: 558479 Color: 0
Size: 441496 Color: 3

Bin 1734: 26 of cap free
Amount of items: 2
Items: 
Size: 585612 Color: 0
Size: 414363 Color: 1

Bin 1735: 26 of cap free
Amount of items: 2
Items: 
Size: 591432 Color: 2
Size: 408543 Color: 1

Bin 1736: 26 of cap free
Amount of items: 2
Items: 
Size: 607334 Color: 3
Size: 392641 Color: 1

Bin 1737: 26 of cap free
Amount of items: 2
Items: 
Size: 725925 Color: 3
Size: 274050 Color: 0

Bin 1738: 26 of cap free
Amount of items: 2
Items: 
Size: 750689 Color: 4
Size: 249286 Color: 2

Bin 1739: 27 of cap free
Amount of items: 3
Items: 
Size: 676610 Color: 1
Size: 161694 Color: 4
Size: 161670 Color: 2

Bin 1740: 27 of cap free
Amount of items: 2
Items: 
Size: 621922 Color: 1
Size: 378052 Color: 2

Bin 1741: 27 of cap free
Amount of items: 2
Items: 
Size: 557745 Color: 4
Size: 442229 Color: 0

Bin 1742: 27 of cap free
Amount of items: 2
Items: 
Size: 718966 Color: 0
Size: 281008 Color: 4

Bin 1743: 27 of cap free
Amount of items: 2
Items: 
Size: 566226 Color: 2
Size: 433748 Color: 4

Bin 1744: 27 of cap free
Amount of items: 2
Items: 
Size: 652475 Color: 0
Size: 347499 Color: 2

Bin 1745: 27 of cap free
Amount of items: 2
Items: 
Size: 575896 Color: 3
Size: 424078 Color: 2

Bin 1746: 27 of cap free
Amount of items: 2
Items: 
Size: 728838 Color: 0
Size: 271136 Color: 2

Bin 1747: 27 of cap free
Amount of items: 2
Items: 
Size: 549988 Color: 2
Size: 449986 Color: 1

Bin 1748: 27 of cap free
Amount of items: 2
Items: 
Size: 663083 Color: 1
Size: 336891 Color: 2

Bin 1749: 27 of cap free
Amount of items: 2
Items: 
Size: 680324 Color: 3
Size: 319650 Color: 4

Bin 1750: 27 of cap free
Amount of items: 2
Items: 
Size: 502002 Color: 4
Size: 497972 Color: 2

Bin 1751: 27 of cap free
Amount of items: 2
Items: 
Size: 510197 Color: 3
Size: 489777 Color: 1

Bin 1752: 27 of cap free
Amount of items: 2
Items: 
Size: 517491 Color: 4
Size: 482483 Color: 2

Bin 1753: 27 of cap free
Amount of items: 2
Items: 
Size: 540928 Color: 3
Size: 459046 Color: 0

Bin 1754: 27 of cap free
Amount of items: 2
Items: 
Size: 553461 Color: 2
Size: 446513 Color: 4

Bin 1755: 27 of cap free
Amount of items: 2
Items: 
Size: 554889 Color: 1
Size: 445085 Color: 4

Bin 1756: 27 of cap free
Amount of items: 2
Items: 
Size: 555168 Color: 2
Size: 444806 Color: 3

Bin 1757: 27 of cap free
Amount of items: 2
Items: 
Size: 696021 Color: 0
Size: 303953 Color: 2

Bin 1758: 27 of cap free
Amount of items: 2
Items: 
Size: 704568 Color: 0
Size: 295406 Color: 3

Bin 1759: 27 of cap free
Amount of items: 2
Items: 
Size: 729211 Color: 0
Size: 270763 Color: 3

Bin 1760: 27 of cap free
Amount of items: 2
Items: 
Size: 731750 Color: 4
Size: 268224 Color: 1

Bin 1761: 27 of cap free
Amount of items: 2
Items: 
Size: 742278 Color: 0
Size: 257696 Color: 4

Bin 1762: 27 of cap free
Amount of items: 2
Items: 
Size: 750302 Color: 1
Size: 249672 Color: 0

Bin 1763: 27 of cap free
Amount of items: 2
Items: 
Size: 780077 Color: 4
Size: 219897 Color: 0

Bin 1764: 28 of cap free
Amount of items: 3
Items: 
Size: 516003 Color: 2
Size: 242252 Color: 4
Size: 241718 Color: 0

Bin 1765: 28 of cap free
Amount of items: 2
Items: 
Size: 705995 Color: 0
Size: 293978 Color: 3

Bin 1766: 28 of cap free
Amount of items: 3
Items: 
Size: 389038 Color: 2
Size: 380831 Color: 1
Size: 230104 Color: 0

Bin 1767: 28 of cap free
Amount of items: 2
Items: 
Size: 519082 Color: 2
Size: 480891 Color: 0

Bin 1768: 28 of cap free
Amount of items: 2
Items: 
Size: 541193 Color: 1
Size: 458780 Color: 4

Bin 1769: 28 of cap free
Amount of items: 2
Items: 
Size: 710132 Color: 2
Size: 289841 Color: 4

Bin 1770: 28 of cap free
Amount of items: 2
Items: 
Size: 705694 Color: 0
Size: 294279 Color: 3

Bin 1771: 28 of cap free
Amount of items: 2
Items: 
Size: 554785 Color: 4
Size: 445188 Color: 2

Bin 1772: 28 of cap free
Amount of items: 2
Items: 
Size: 668120 Color: 1
Size: 331853 Color: 2

Bin 1773: 28 of cap free
Amount of items: 2
Items: 
Size: 500039 Color: 1
Size: 499934 Color: 4

Bin 1774: 28 of cap free
Amount of items: 2
Items: 
Size: 512446 Color: 3
Size: 487527 Color: 0

Bin 1775: 28 of cap free
Amount of items: 2
Items: 
Size: 514453 Color: 3
Size: 485520 Color: 4

Bin 1776: 28 of cap free
Amount of items: 3
Items: 
Size: 565826 Color: 3
Size: 218479 Color: 1
Size: 215668 Color: 4

Bin 1777: 28 of cap free
Amount of items: 2
Items: 
Size: 590982 Color: 2
Size: 408991 Color: 0

Bin 1778: 28 of cap free
Amount of items: 2
Items: 
Size: 617692 Color: 3
Size: 382281 Color: 4

Bin 1779: 28 of cap free
Amount of items: 2
Items: 
Size: 653628 Color: 0
Size: 346345 Color: 1

Bin 1780: 28 of cap free
Amount of items: 2
Items: 
Size: 668686 Color: 0
Size: 331287 Color: 4

Bin 1781: 28 of cap free
Amount of items: 2
Items: 
Size: 691265 Color: 4
Size: 308708 Color: 2

Bin 1782: 28 of cap free
Amount of items: 2
Items: 
Size: 707565 Color: 3
Size: 292408 Color: 0

Bin 1783: 28 of cap free
Amount of items: 2
Items: 
Size: 713769 Color: 4
Size: 286204 Color: 0

Bin 1784: 28 of cap free
Amount of items: 2
Items: 
Size: 740357 Color: 1
Size: 259616 Color: 0

Bin 1785: 28 of cap free
Amount of items: 2
Items: 
Size: 741458 Color: 2
Size: 258515 Color: 0

Bin 1786: 28 of cap free
Amount of items: 2
Items: 
Size: 780224 Color: 0
Size: 219749 Color: 1

Bin 1787: 29 of cap free
Amount of items: 3
Items: 
Size: 390996 Color: 4
Size: 321387 Color: 1
Size: 287589 Color: 1

Bin 1788: 29 of cap free
Amount of items: 2
Items: 
Size: 736092 Color: 1
Size: 263880 Color: 4

Bin 1789: 29 of cap free
Amount of items: 2
Items: 
Size: 710790 Color: 0
Size: 289182 Color: 4

Bin 1790: 29 of cap free
Amount of items: 2
Items: 
Size: 585730 Color: 4
Size: 414242 Color: 0

Bin 1791: 29 of cap free
Amount of items: 2
Items: 
Size: 772848 Color: 4
Size: 227124 Color: 1

Bin 1792: 29 of cap free
Amount of items: 3
Items: 
Size: 630803 Color: 3
Size: 196502 Color: 1
Size: 172667 Color: 4

Bin 1793: 29 of cap free
Amount of items: 2
Items: 
Size: 708605 Color: 1
Size: 291367 Color: 2

Bin 1794: 29 of cap free
Amount of items: 2
Items: 
Size: 569256 Color: 4
Size: 430716 Color: 3

Bin 1795: 29 of cap free
Amount of items: 2
Items: 
Size: 630157 Color: 4
Size: 369815 Color: 0

Bin 1796: 29 of cap free
Amount of items: 2
Items: 
Size: 635267 Color: 1
Size: 364705 Color: 3

Bin 1797: 29 of cap free
Amount of items: 2
Items: 
Size: 591333 Color: 1
Size: 408639 Color: 0

Bin 1798: 29 of cap free
Amount of items: 2
Items: 
Size: 509050 Color: 4
Size: 490922 Color: 1

Bin 1799: 29 of cap free
Amount of items: 2
Items: 
Size: 539637 Color: 4
Size: 460335 Color: 2

Bin 1800: 29 of cap free
Amount of items: 2
Items: 
Size: 548813 Color: 1
Size: 451159 Color: 0

Bin 1801: 29 of cap free
Amount of items: 2
Items: 
Size: 549011 Color: 3
Size: 450961 Color: 0

Bin 1802: 29 of cap free
Amount of items: 2
Items: 
Size: 569214 Color: 2
Size: 430758 Color: 0

Bin 1803: 29 of cap free
Amount of items: 2
Items: 
Size: 597413 Color: 4
Size: 402559 Color: 3

Bin 1804: 29 of cap free
Amount of items: 2
Items: 
Size: 614799 Color: 0
Size: 385173 Color: 3

Bin 1805: 29 of cap free
Amount of items: 2
Items: 
Size: 639709 Color: 4
Size: 360263 Color: 1

Bin 1806: 29 of cap free
Amount of items: 2
Items: 
Size: 665014 Color: 3
Size: 334958 Color: 4

Bin 1807: 29 of cap free
Amount of items: 2
Items: 
Size: 760193 Color: 4
Size: 239779 Color: 0

Bin 1808: 29 of cap free
Amount of items: 2
Items: 
Size: 764139 Color: 3
Size: 235833 Color: 4

Bin 1809: 29 of cap free
Amount of items: 3
Items: 
Size: 779999 Color: 4
Size: 110227 Color: 3
Size: 109746 Color: 4

Bin 1810: 29 of cap free
Amount of items: 2
Items: 
Size: 784356 Color: 0
Size: 215616 Color: 2

Bin 1811: 30 of cap free
Amount of items: 2
Items: 
Size: 734645 Color: 2
Size: 265326 Color: 0

Bin 1812: 30 of cap free
Amount of items: 2
Items: 
Size: 609579 Color: 2
Size: 390392 Color: 4

Bin 1813: 30 of cap free
Amount of items: 2
Items: 
Size: 758657 Color: 1
Size: 241314 Color: 0

Bin 1814: 30 of cap free
Amount of items: 2
Items: 
Size: 667071 Color: 1
Size: 332900 Color: 3

Bin 1815: 30 of cap free
Amount of items: 2
Items: 
Size: 703087 Color: 3
Size: 296884 Color: 2

Bin 1816: 30 of cap free
Amount of items: 2
Items: 
Size: 509036 Color: 3
Size: 490935 Color: 4

Bin 1817: 30 of cap free
Amount of items: 2
Items: 
Size: 563185 Color: 4
Size: 436786 Color: 0

Bin 1818: 30 of cap free
Amount of items: 2
Items: 
Size: 581960 Color: 4
Size: 418011 Color: 0

Bin 1819: 30 of cap free
Amount of items: 2
Items: 
Size: 596909 Color: 3
Size: 403062 Color: 1

Bin 1820: 30 of cap free
Amount of items: 2
Items: 
Size: 607102 Color: 2
Size: 392869 Color: 4

Bin 1821: 30 of cap free
Amount of items: 2
Items: 
Size: 609739 Color: 0
Size: 390232 Color: 1

Bin 1822: 30 of cap free
Amount of items: 2
Items: 
Size: 615070 Color: 0
Size: 384901 Color: 4

Bin 1823: 30 of cap free
Amount of items: 2
Items: 
Size: 638974 Color: 3
Size: 360997 Color: 4

Bin 1824: 30 of cap free
Amount of items: 2
Items: 
Size: 678883 Color: 4
Size: 321088 Color: 2

Bin 1825: 30 of cap free
Amount of items: 2
Items: 
Size: 680943 Color: 4
Size: 319028 Color: 0

Bin 1826: 30 of cap free
Amount of items: 2
Items: 
Size: 747818 Color: 3
Size: 252153 Color: 0

Bin 1827: 30 of cap free
Amount of items: 2
Items: 
Size: 762102 Color: 4
Size: 237869 Color: 3

Bin 1828: 30 of cap free
Amount of items: 3
Items: 
Size: 785049 Color: 1
Size: 107726 Color: 0
Size: 107196 Color: 4

Bin 1829: 31 of cap free
Amount of items: 2
Items: 
Size: 601027 Color: 0
Size: 398943 Color: 2

Bin 1830: 31 of cap free
Amount of items: 2
Items: 
Size: 614517 Color: 0
Size: 385453 Color: 2

Bin 1831: 31 of cap free
Amount of items: 2
Items: 
Size: 652263 Color: 3
Size: 347707 Color: 4

Bin 1832: 31 of cap free
Amount of items: 2
Items: 
Size: 748067 Color: 3
Size: 251903 Color: 2

Bin 1833: 31 of cap free
Amount of items: 2
Items: 
Size: 793870 Color: 3
Size: 206100 Color: 1

Bin 1834: 31 of cap free
Amount of items: 2
Items: 
Size: 524576 Color: 4
Size: 475394 Color: 2

Bin 1835: 31 of cap free
Amount of items: 3
Items: 
Size: 411935 Color: 4
Size: 295339 Color: 1
Size: 292696 Color: 2

Bin 1836: 31 of cap free
Amount of items: 2
Items: 
Size: 725797 Color: 4
Size: 274173 Color: 1

Bin 1837: 31 of cap free
Amount of items: 2
Items: 
Size: 520406 Color: 2
Size: 479564 Color: 0

Bin 1838: 31 of cap free
Amount of items: 2
Items: 
Size: 692901 Color: 4
Size: 307069 Color: 2

Bin 1839: 31 of cap free
Amount of items: 2
Items: 
Size: 666097 Color: 1
Size: 333873 Color: 2

Bin 1840: 31 of cap free
Amount of items: 2
Items: 
Size: 503821 Color: 4
Size: 496149 Color: 0

Bin 1841: 31 of cap free
Amount of items: 2
Items: 
Size: 516565 Color: 0
Size: 483405 Color: 4

Bin 1842: 31 of cap free
Amount of items: 2
Items: 
Size: 596144 Color: 3
Size: 403826 Color: 4

Bin 1843: 31 of cap free
Amount of items: 2
Items: 
Size: 652642 Color: 0
Size: 347328 Color: 1

Bin 1844: 31 of cap free
Amount of items: 2
Items: 
Size: 679169 Color: 3
Size: 320801 Color: 2

Bin 1845: 31 of cap free
Amount of items: 2
Items: 
Size: 710533 Color: 2
Size: 289437 Color: 3

Bin 1846: 31 of cap free
Amount of items: 2
Items: 
Size: 724222 Color: 4
Size: 275748 Color: 3

Bin 1847: 31 of cap free
Amount of items: 2
Items: 
Size: 742511 Color: 1
Size: 257459 Color: 2

Bin 1848: 31 of cap free
Amount of items: 2
Items: 
Size: 760705 Color: 0
Size: 239265 Color: 2

Bin 1849: 32 of cap free
Amount of items: 3
Items: 
Size: 654575 Color: 0
Size: 183765 Color: 2
Size: 161629 Color: 0

Bin 1850: 32 of cap free
Amount of items: 3
Items: 
Size: 479489 Color: 3
Size: 263370 Color: 4
Size: 257110 Color: 1

Bin 1851: 32 of cap free
Amount of items: 3
Items: 
Size: 540117 Color: 4
Size: 346963 Color: 4
Size: 112889 Color: 2

Bin 1852: 32 of cap free
Amount of items: 2
Items: 
Size: 712473 Color: 3
Size: 287496 Color: 2

Bin 1853: 32 of cap free
Amount of items: 2
Items: 
Size: 700388 Color: 3
Size: 299581 Color: 0

Bin 1854: 32 of cap free
Amount of items: 2
Items: 
Size: 686889 Color: 1
Size: 313080 Color: 0

Bin 1855: 32 of cap free
Amount of items: 2
Items: 
Size: 560409 Color: 3
Size: 439560 Color: 4

Bin 1856: 32 of cap free
Amount of items: 2
Items: 
Size: 705909 Color: 3
Size: 294060 Color: 0

Bin 1857: 32 of cap free
Amount of items: 2
Items: 
Size: 685710 Color: 1
Size: 314259 Color: 2

Bin 1858: 32 of cap free
Amount of items: 2
Items: 
Size: 657600 Color: 1
Size: 342369 Color: 3

Bin 1859: 32 of cap free
Amount of items: 2
Items: 
Size: 528267 Color: 2
Size: 471702 Color: 1

Bin 1860: 32 of cap free
Amount of items: 2
Items: 
Size: 553069 Color: 3
Size: 446900 Color: 0

Bin 1861: 32 of cap free
Amount of items: 2
Items: 
Size: 594671 Color: 1
Size: 405298 Color: 4

Bin 1862: 32 of cap free
Amount of items: 2
Items: 
Size: 596774 Color: 2
Size: 403195 Color: 3

Bin 1863: 32 of cap free
Amount of items: 2
Items: 
Size: 600161 Color: 4
Size: 399808 Color: 3

Bin 1864: 32 of cap free
Amount of items: 2
Items: 
Size: 614267 Color: 0
Size: 385702 Color: 3

Bin 1865: 32 of cap free
Amount of items: 2
Items: 
Size: 641641 Color: 4
Size: 358328 Color: 0

Bin 1866: 32 of cap free
Amount of items: 2
Items: 
Size: 659902 Color: 4
Size: 340067 Color: 0

Bin 1867: 32 of cap free
Amount of items: 2
Items: 
Size: 672512 Color: 3
Size: 327457 Color: 0

Bin 1868: 32 of cap free
Amount of items: 2
Items: 
Size: 679028 Color: 2
Size: 320941 Color: 3

Bin 1869: 32 of cap free
Amount of items: 2
Items: 
Size: 746376 Color: 0
Size: 253593 Color: 4

Bin 1870: 32 of cap free
Amount of items: 2
Items: 
Size: 750062 Color: 1
Size: 249907 Color: 0

Bin 1871: 32 of cap free
Amount of items: 2
Items: 
Size: 755276 Color: 2
Size: 244693 Color: 4

Bin 1872: 33 of cap free
Amount of items: 2
Items: 
Size: 712753 Color: 2
Size: 287215 Color: 4

Bin 1873: 33 of cap free
Amount of items: 2
Items: 
Size: 542662 Color: 0
Size: 457306 Color: 2

Bin 1874: 33 of cap free
Amount of items: 3
Items: 
Size: 608754 Color: 2
Size: 196720 Color: 1
Size: 194494 Color: 4

Bin 1875: 33 of cap free
Amount of items: 2
Items: 
Size: 542299 Color: 0
Size: 457669 Color: 3

Bin 1876: 33 of cap free
Amount of items: 2
Items: 
Size: 530804 Color: 0
Size: 469164 Color: 4

Bin 1877: 33 of cap free
Amount of items: 2
Items: 
Size: 741280 Color: 4
Size: 258688 Color: 0

Bin 1878: 33 of cap free
Amount of items: 3
Items: 
Size: 601108 Color: 1
Size: 244511 Color: 0
Size: 154349 Color: 1

Bin 1879: 33 of cap free
Amount of items: 2
Items: 
Size: 520520 Color: 1
Size: 479448 Color: 2

Bin 1880: 33 of cap free
Amount of items: 2
Items: 
Size: 551906 Color: 4
Size: 448062 Color: 1

Bin 1881: 33 of cap free
Amount of items: 2
Items: 
Size: 526101 Color: 0
Size: 473867 Color: 1

Bin 1882: 33 of cap free
Amount of items: 2
Items: 
Size: 774144 Color: 4
Size: 225824 Color: 3

Bin 1883: 33 of cap free
Amount of items: 2
Items: 
Size: 746406 Color: 4
Size: 253562 Color: 0

Bin 1884: 33 of cap free
Amount of items: 2
Items: 
Size: 547275 Color: 0
Size: 452693 Color: 4

Bin 1885: 33 of cap free
Amount of items: 2
Items: 
Size: 549671 Color: 0
Size: 450297 Color: 3

Bin 1886: 33 of cap free
Amount of items: 2
Items: 
Size: 589315 Color: 3
Size: 410653 Color: 1

Bin 1887: 33 of cap free
Amount of items: 2
Items: 
Size: 590813 Color: 0
Size: 409155 Color: 4

Bin 1888: 33 of cap free
Amount of items: 2
Items: 
Size: 617703 Color: 1
Size: 382265 Color: 4

Bin 1889: 33 of cap free
Amount of items: 2
Items: 
Size: 627825 Color: 0
Size: 372143 Color: 4

Bin 1890: 33 of cap free
Amount of items: 2
Items: 
Size: 652577 Color: 2
Size: 347391 Color: 1

Bin 1891: 33 of cap free
Amount of items: 2
Items: 
Size: 654464 Color: 1
Size: 345504 Color: 4

Bin 1892: 33 of cap free
Amount of items: 2
Items: 
Size: 670867 Color: 0
Size: 329101 Color: 2

Bin 1893: 33 of cap free
Amount of items: 2
Items: 
Size: 698814 Color: 0
Size: 301154 Color: 4

Bin 1894: 33 of cap free
Amount of items: 2
Items: 
Size: 725670 Color: 3
Size: 274298 Color: 0

Bin 1895: 33 of cap free
Amount of items: 2
Items: 
Size: 753155 Color: 1
Size: 246813 Color: 0

Bin 1896: 33 of cap free
Amount of items: 2
Items: 
Size: 765439 Color: 2
Size: 234529 Color: 4

Bin 1897: 34 of cap free
Amount of items: 2
Items: 
Size: 611064 Color: 4
Size: 388903 Color: 2

Bin 1898: 34 of cap free
Amount of items: 3
Items: 
Size: 583502 Color: 1
Size: 240918 Color: 4
Size: 175547 Color: 3

Bin 1899: 34 of cap free
Amount of items: 2
Items: 
Size: 662773 Color: 4
Size: 337194 Color: 1

Bin 1900: 34 of cap free
Amount of items: 2
Items: 
Size: 619133 Color: 1
Size: 380834 Color: 4

Bin 1901: 34 of cap free
Amount of items: 2
Items: 
Size: 795747 Color: 4
Size: 204220 Color: 1

Bin 1902: 34 of cap free
Amount of items: 3
Items: 
Size: 780526 Color: 3
Size: 110254 Color: 3
Size: 109187 Color: 4

Bin 1903: 34 of cap free
Amount of items: 2
Items: 
Size: 521579 Color: 4
Size: 478388 Color: 2

Bin 1904: 34 of cap free
Amount of items: 2
Items: 
Size: 511056 Color: 0
Size: 488911 Color: 3

Bin 1905: 34 of cap free
Amount of items: 3
Items: 
Size: 352587 Color: 1
Size: 348936 Color: 3
Size: 298444 Color: 4

Bin 1906: 34 of cap free
Amount of items: 3
Items: 
Size: 650245 Color: 1
Size: 180732 Color: 4
Size: 168990 Color: 1

Bin 1907: 34 of cap free
Amount of items: 2
Items: 
Size: 551724 Color: 3
Size: 448243 Color: 1

Bin 1908: 34 of cap free
Amount of items: 2
Items: 
Size: 686690 Color: 1
Size: 313277 Color: 2

Bin 1909: 34 of cap free
Amount of items: 2
Items: 
Size: 545586 Color: 3
Size: 454381 Color: 1

Bin 1910: 34 of cap free
Amount of items: 2
Items: 
Size: 519997 Color: 0
Size: 479970 Color: 1

Bin 1911: 34 of cap free
Amount of items: 2
Items: 
Size: 518425 Color: 2
Size: 481542 Color: 4

Bin 1912: 34 of cap free
Amount of items: 2
Items: 
Size: 796827 Color: 2
Size: 203140 Color: 4

Bin 1913: 34 of cap free
Amount of items: 2
Items: 
Size: 615244 Color: 4
Size: 384723 Color: 3

Bin 1914: 34 of cap free
Amount of items: 2
Items: 
Size: 510554 Color: 2
Size: 489413 Color: 3

Bin 1915: 34 of cap free
Amount of items: 2
Items: 
Size: 546905 Color: 3
Size: 453062 Color: 0

Bin 1916: 34 of cap free
Amount of items: 2
Items: 
Size: 567172 Color: 1
Size: 432795 Color: 4

Bin 1917: 34 of cap free
Amount of items: 2
Items: 
Size: 587428 Color: 4
Size: 412539 Color: 1

Bin 1918: 34 of cap free
Amount of items: 2
Items: 
Size: 591971 Color: 2
Size: 407996 Color: 3

Bin 1919: 34 of cap free
Amount of items: 2
Items: 
Size: 596511 Color: 1
Size: 403456 Color: 0

Bin 1920: 34 of cap free
Amount of items: 2
Items: 
Size: 640177 Color: 1
Size: 359790 Color: 3

Bin 1921: 34 of cap free
Amount of items: 2
Items: 
Size: 658548 Color: 3
Size: 341419 Color: 4

Bin 1922: 34 of cap free
Amount of items: 2
Items: 
Size: 706985 Color: 0
Size: 292982 Color: 1

Bin 1923: 34 of cap free
Amount of items: 2
Items: 
Size: 771786 Color: 3
Size: 228181 Color: 4

Bin 1924: 35 of cap free
Amount of items: 3
Items: 
Size: 542820 Color: 2
Size: 228647 Color: 3
Size: 228499 Color: 0

Bin 1925: 35 of cap free
Amount of items: 2
Items: 
Size: 741547 Color: 2
Size: 258419 Color: 0

Bin 1926: 35 of cap free
Amount of items: 2
Items: 
Size: 721448 Color: 0
Size: 278518 Color: 4

Bin 1927: 35 of cap free
Amount of items: 2
Items: 
Size: 524290 Color: 4
Size: 475676 Color: 2

Bin 1928: 35 of cap free
Amount of items: 2
Items: 
Size: 508502 Color: 0
Size: 491464 Color: 4

Bin 1929: 35 of cap free
Amount of items: 2
Items: 
Size: 530932 Color: 4
Size: 469034 Color: 3

Bin 1930: 35 of cap free
Amount of items: 2
Items: 
Size: 662378 Color: 1
Size: 337588 Color: 4

Bin 1931: 35 of cap free
Amount of items: 2
Items: 
Size: 654151 Color: 1
Size: 345815 Color: 3

Bin 1932: 35 of cap free
Amount of items: 2
Items: 
Size: 533297 Color: 2
Size: 466669 Color: 1

Bin 1933: 35 of cap free
Amount of items: 2
Items: 
Size: 530123 Color: 4
Size: 469843 Color: 2

Bin 1934: 35 of cap free
Amount of items: 2
Items: 
Size: 517764 Color: 1
Size: 482202 Color: 4

Bin 1935: 35 of cap free
Amount of items: 2
Items: 
Size: 695524 Color: 3
Size: 304442 Color: 1

Bin 1936: 35 of cap free
Amount of items: 2
Items: 
Size: 714388 Color: 1
Size: 285578 Color: 3

Bin 1937: 35 of cap free
Amount of items: 2
Items: 
Size: 616930 Color: 2
Size: 383036 Color: 1

Bin 1938: 35 of cap free
Amount of items: 2
Items: 
Size: 748736 Color: 1
Size: 251230 Color: 4

Bin 1939: 35 of cap free
Amount of items: 2
Items: 
Size: 790894 Color: 3
Size: 209072 Color: 1

Bin 1940: 35 of cap free
Amount of items: 2
Items: 
Size: 506977 Color: 3
Size: 492989 Color: 0

Bin 1941: 35 of cap free
Amount of items: 2
Items: 
Size: 522995 Color: 2
Size: 476971 Color: 4

Bin 1942: 35 of cap free
Amount of items: 2
Items: 
Size: 525182 Color: 3
Size: 474784 Color: 2

Bin 1943: 35 of cap free
Amount of items: 2
Items: 
Size: 552758 Color: 3
Size: 447208 Color: 0

Bin 1944: 35 of cap free
Amount of items: 2
Items: 
Size: 569509 Color: 0
Size: 430457 Color: 2

Bin 1945: 35 of cap free
Amount of items: 2
Items: 
Size: 574857 Color: 0
Size: 425109 Color: 4

Bin 1946: 35 of cap free
Amount of items: 2
Items: 
Size: 608471 Color: 0
Size: 391495 Color: 4

Bin 1947: 35 of cap free
Amount of items: 2
Items: 
Size: 653390 Color: 4
Size: 346576 Color: 1

Bin 1948: 35 of cap free
Amount of items: 2
Items: 
Size: 654991 Color: 0
Size: 344975 Color: 4

Bin 1949: 35 of cap free
Amount of items: 2
Items: 
Size: 667675 Color: 3
Size: 332291 Color: 0

Bin 1950: 35 of cap free
Amount of items: 2
Items: 
Size: 799988 Color: 0
Size: 199978 Color: 3

Bin 1951: 36 of cap free
Amount of items: 3
Items: 
Size: 351663 Color: 2
Size: 340575 Color: 0
Size: 307727 Color: 2

Bin 1952: 36 of cap free
Amount of items: 2
Items: 
Size: 590276 Color: 2
Size: 409689 Color: 1

Bin 1953: 36 of cap free
Amount of items: 2
Items: 
Size: 657149 Color: 4
Size: 342816 Color: 2

Bin 1954: 36 of cap free
Amount of items: 3
Items: 
Size: 475895 Color: 2
Size: 279812 Color: 1
Size: 244258 Color: 4

Bin 1955: 36 of cap free
Amount of items: 2
Items: 
Size: 666992 Color: 1
Size: 332973 Color: 0

Bin 1956: 36 of cap free
Amount of items: 2
Items: 
Size: 550474 Color: 1
Size: 449491 Color: 3

Bin 1957: 36 of cap free
Amount of items: 2
Items: 
Size: 572012 Color: 4
Size: 427953 Color: 1

Bin 1958: 36 of cap free
Amount of items: 2
Items: 
Size: 629940 Color: 4
Size: 370025 Color: 3

Bin 1959: 36 of cap free
Amount of items: 2
Items: 
Size: 786764 Color: 1
Size: 213201 Color: 3

Bin 1960: 36 of cap free
Amount of items: 2
Items: 
Size: 503475 Color: 4
Size: 496490 Color: 3

Bin 1961: 36 of cap free
Amount of items: 2
Items: 
Size: 507184 Color: 0
Size: 492781 Color: 2

Bin 1962: 36 of cap free
Amount of items: 2
Items: 
Size: 516653 Color: 0
Size: 483312 Color: 4

Bin 1963: 36 of cap free
Amount of items: 2
Items: 
Size: 529060 Color: 1
Size: 470905 Color: 2

Bin 1964: 36 of cap free
Amount of items: 2
Items: 
Size: 571513 Color: 0
Size: 428452 Color: 1

Bin 1965: 36 of cap free
Amount of items: 2
Items: 
Size: 629864 Color: 1
Size: 370101 Color: 2

Bin 1966: 36 of cap free
Amount of items: 2
Items: 
Size: 677038 Color: 2
Size: 322927 Color: 4

Bin 1967: 36 of cap free
Amount of items: 2
Items: 
Size: 678582 Color: 1
Size: 321383 Color: 2

Bin 1968: 36 of cap free
Amount of items: 2
Items: 
Size: 711661 Color: 2
Size: 288304 Color: 4

Bin 1969: 36 of cap free
Amount of items: 2
Items: 
Size: 727026 Color: 0
Size: 272939 Color: 3

Bin 1970: 36 of cap free
Amount of items: 2
Items: 
Size: 750689 Color: 2
Size: 249276 Color: 0

Bin 1971: 36 of cap free
Amount of items: 2
Items: 
Size: 761753 Color: 3
Size: 238212 Color: 2

Bin 1972: 36 of cap free
Amount of items: 2
Items: 
Size: 785253 Color: 3
Size: 214712 Color: 0

Bin 1973: 36 of cap free
Amount of items: 2
Items: 
Size: 796153 Color: 4
Size: 203812 Color: 0

Bin 1974: 37 of cap free
Amount of items: 3
Items: 
Size: 355423 Color: 2
Size: 351102 Color: 4
Size: 293439 Color: 3

Bin 1975: 37 of cap free
Amount of items: 2
Items: 
Size: 799014 Color: 4
Size: 200950 Color: 0

Bin 1976: 37 of cap free
Amount of items: 3
Items: 
Size: 742622 Color: 0
Size: 145633 Color: 4
Size: 111709 Color: 3

Bin 1977: 37 of cap free
Amount of items: 2
Items: 
Size: 641793 Color: 3
Size: 358171 Color: 0

Bin 1978: 37 of cap free
Amount of items: 2
Items: 
Size: 657444 Color: 0
Size: 342520 Color: 1

Bin 1979: 37 of cap free
Amount of items: 2
Items: 
Size: 591663 Color: 3
Size: 408301 Color: 2

Bin 1980: 37 of cap free
Amount of items: 2
Items: 
Size: 674108 Color: 4
Size: 325856 Color: 1

Bin 1981: 37 of cap free
Amount of items: 2
Items: 
Size: 728434 Color: 0
Size: 271530 Color: 4

Bin 1982: 37 of cap free
Amount of items: 2
Items: 
Size: 574422 Color: 3
Size: 425542 Color: 1

Bin 1983: 37 of cap free
Amount of items: 2
Items: 
Size: 639815 Color: 4
Size: 360149 Color: 3

Bin 1984: 37 of cap free
Amount of items: 2
Items: 
Size: 530641 Color: 1
Size: 469323 Color: 2

Bin 1985: 37 of cap free
Amount of items: 2
Items: 
Size: 548883 Color: 2
Size: 451081 Color: 0

Bin 1986: 37 of cap free
Amount of items: 2
Items: 
Size: 553989 Color: 4
Size: 445975 Color: 3

Bin 1987: 37 of cap free
Amount of items: 2
Items: 
Size: 559394 Color: 0
Size: 440570 Color: 3

Bin 1988: 37 of cap free
Amount of items: 2
Items: 
Size: 590431 Color: 3
Size: 409533 Color: 2

Bin 1989: 37 of cap free
Amount of items: 2
Items: 
Size: 592524 Color: 3
Size: 407440 Color: 1

Bin 1990: 37 of cap free
Amount of items: 2
Items: 
Size: 598456 Color: 3
Size: 401508 Color: 0

Bin 1991: 37 of cap free
Amount of items: 2
Items: 
Size: 615420 Color: 0
Size: 384544 Color: 1

Bin 1992: 37 of cap free
Amount of items: 2
Items: 
Size: 702828 Color: 1
Size: 297136 Color: 3

Bin 1993: 37 of cap free
Amount of items: 2
Items: 
Size: 732806 Color: 4
Size: 267158 Color: 1

Bin 1994: 37 of cap free
Amount of items: 2
Items: 
Size: 753976 Color: 0
Size: 245988 Color: 2

Bin 1995: 38 of cap free
Amount of items: 2
Items: 
Size: 695040 Color: 2
Size: 304923 Color: 4

Bin 1996: 38 of cap free
Amount of items: 2
Items: 
Size: 763695 Color: 1
Size: 236268 Color: 2

Bin 1997: 38 of cap free
Amount of items: 3
Items: 
Size: 411326 Color: 0
Size: 294526 Color: 1
Size: 294111 Color: 0

Bin 1998: 38 of cap free
Amount of items: 2
Items: 
Size: 622253 Color: 0
Size: 377710 Color: 1

Bin 1999: 38 of cap free
Amount of items: 2
Items: 
Size: 530879 Color: 4
Size: 469084 Color: 0

Bin 2000: 38 of cap free
Amount of items: 2
Items: 
Size: 649948 Color: 0
Size: 350015 Color: 4

Bin 2001: 38 of cap free
Amount of items: 2
Items: 
Size: 789493 Color: 4
Size: 210470 Color: 1

Bin 2002: 38 of cap free
Amount of items: 2
Items: 
Size: 521237 Color: 0
Size: 478726 Color: 2

Bin 2003: 38 of cap free
Amount of items: 2
Items: 
Size: 624504 Color: 2
Size: 375459 Color: 0

Bin 2004: 38 of cap free
Amount of items: 2
Items: 
Size: 638103 Color: 0
Size: 361860 Color: 4

Bin 2005: 38 of cap free
Amount of items: 2
Items: 
Size: 671063 Color: 4
Size: 328900 Color: 2

Bin 2006: 38 of cap free
Amount of items: 2
Items: 
Size: 751637 Color: 3
Size: 248326 Color: 0

Bin 2007: 39 of cap free
Amount of items: 2
Items: 
Size: 604271 Color: 0
Size: 395691 Color: 4

Bin 2008: 39 of cap free
Amount of items: 3
Items: 
Size: 670847 Color: 4
Size: 164671 Color: 1
Size: 164444 Color: 3

Bin 2009: 39 of cap free
Amount of items: 2
Items: 
Size: 709225 Color: 1
Size: 290737 Color: 4

Bin 2010: 39 of cap free
Amount of items: 2
Items: 
Size: 525316 Color: 1
Size: 474646 Color: 3

Bin 2011: 39 of cap free
Amount of items: 2
Items: 
Size: 529636 Color: 1
Size: 470326 Color: 3

Bin 2012: 39 of cap free
Amount of items: 2
Items: 
Size: 570325 Color: 4
Size: 429637 Color: 1

Bin 2013: 39 of cap free
Amount of items: 2
Items: 
Size: 590690 Color: 1
Size: 409272 Color: 2

Bin 2014: 39 of cap free
Amount of items: 2
Items: 
Size: 592625 Color: 1
Size: 407337 Color: 2

Bin 2015: 39 of cap free
Amount of items: 2
Items: 
Size: 600477 Color: 0
Size: 399485 Color: 1

Bin 2016: 39 of cap free
Amount of items: 2
Items: 
Size: 627742 Color: 4
Size: 372220 Color: 1

Bin 2017: 39 of cap free
Amount of items: 2
Items: 
Size: 632409 Color: 3
Size: 367553 Color: 1

Bin 2018: 39 of cap free
Amount of items: 2
Items: 
Size: 649258 Color: 3
Size: 350704 Color: 4

Bin 2019: 39 of cap free
Amount of items: 2
Items: 
Size: 687736 Color: 2
Size: 312226 Color: 0

Bin 2020: 39 of cap free
Amount of items: 2
Items: 
Size: 717803 Color: 1
Size: 282159 Color: 4

Bin 2021: 40 of cap free
Amount of items: 2
Items: 
Size: 787388 Color: 2
Size: 212573 Color: 1

Bin 2022: 40 of cap free
Amount of items: 3
Items: 
Size: 789977 Color: 1
Size: 105360 Color: 4
Size: 104624 Color: 3

Bin 2023: 40 of cap free
Amount of items: 2
Items: 
Size: 604228 Color: 2
Size: 395733 Color: 0

Bin 2024: 40 of cap free
Amount of items: 2
Items: 
Size: 717147 Color: 4
Size: 282814 Color: 1

Bin 2025: 40 of cap free
Amount of items: 3
Items: 
Size: 667376 Color: 1
Size: 171681 Color: 2
Size: 160904 Color: 1

Bin 2026: 40 of cap free
Amount of items: 2
Items: 
Size: 691621 Color: 2
Size: 308340 Color: 4

Bin 2027: 40 of cap free
Amount of items: 2
Items: 
Size: 580183 Color: 4
Size: 419778 Color: 2

Bin 2028: 40 of cap free
Amount of items: 2
Items: 
Size: 570695 Color: 2
Size: 429266 Color: 0

Bin 2029: 40 of cap free
Amount of items: 2
Items: 
Size: 605624 Color: 4
Size: 394337 Color: 2

Bin 2030: 40 of cap free
Amount of items: 2
Items: 
Size: 559878 Color: 3
Size: 440083 Color: 4

Bin 2031: 40 of cap free
Amount of items: 2
Items: 
Size: 561134 Color: 4
Size: 438827 Color: 0

Bin 2032: 40 of cap free
Amount of items: 2
Items: 
Size: 585293 Color: 1
Size: 414668 Color: 3

Bin 2033: 40 of cap free
Amount of items: 2
Items: 
Size: 589675 Color: 4
Size: 410286 Color: 0

Bin 2034: 40 of cap free
Amount of items: 2
Items: 
Size: 619685 Color: 3
Size: 380276 Color: 1

Bin 2035: 40 of cap free
Amount of items: 2
Items: 
Size: 646674 Color: 0
Size: 353287 Color: 3

Bin 2036: 40 of cap free
Amount of items: 2
Items: 
Size: 651853 Color: 1
Size: 348108 Color: 3

Bin 2037: 40 of cap free
Amount of items: 2
Items: 
Size: 685538 Color: 0
Size: 314423 Color: 3

Bin 2038: 40 of cap free
Amount of items: 2
Items: 
Size: 708354 Color: 4
Size: 291607 Color: 3

Bin 2039: 40 of cap free
Amount of items: 2
Items: 
Size: 743166 Color: 1
Size: 256795 Color: 3

Bin 2040: 40 of cap free
Amount of items: 2
Items: 
Size: 760051 Color: 3
Size: 239910 Color: 4

Bin 2041: 40 of cap free
Amount of items: 2
Items: 
Size: 799922 Color: 0
Size: 200039 Color: 1

Bin 2042: 41 of cap free
Amount of items: 2
Items: 
Size: 660536 Color: 1
Size: 339424 Color: 2

Bin 2043: 41 of cap free
Amount of items: 2
Items: 
Size: 604941 Color: 0
Size: 395019 Color: 2

Bin 2044: 41 of cap free
Amount of items: 2
Items: 
Size: 670027 Color: 4
Size: 329933 Color: 0

Bin 2045: 41 of cap free
Amount of items: 3
Items: 
Size: 654905 Color: 2
Size: 190384 Color: 2
Size: 154671 Color: 3

Bin 2046: 41 of cap free
Amount of items: 2
Items: 
Size: 526848 Color: 2
Size: 473112 Color: 0

Bin 2047: 41 of cap free
Amount of items: 2
Items: 
Size: 727688 Color: 2
Size: 272272 Color: 4

Bin 2048: 41 of cap free
Amount of items: 2
Items: 
Size: 688775 Color: 3
Size: 311185 Color: 4

Bin 2049: 41 of cap free
Amount of items: 2
Items: 
Size: 540222 Color: 4
Size: 459738 Color: 0

Bin 2050: 41 of cap free
Amount of items: 2
Items: 
Size: 622473 Color: 0
Size: 377487 Color: 2

Bin 2051: 41 of cap free
Amount of items: 2
Items: 
Size: 587920 Color: 3
Size: 412040 Color: 2

Bin 2052: 41 of cap free
Amount of items: 2
Items: 
Size: 664424 Color: 0
Size: 335536 Color: 2

Bin 2053: 41 of cap free
Amount of items: 2
Items: 
Size: 552956 Color: 3
Size: 447004 Color: 4

Bin 2054: 41 of cap free
Amount of items: 2
Items: 
Size: 565170 Color: 3
Size: 434790 Color: 4

Bin 2055: 41 of cap free
Amount of items: 2
Items: 
Size: 575703 Color: 2
Size: 424257 Color: 1

Bin 2056: 41 of cap free
Amount of items: 2
Items: 
Size: 583345 Color: 4
Size: 416615 Color: 0

Bin 2057: 41 of cap free
Amount of items: 2
Items: 
Size: 629201 Color: 3
Size: 370759 Color: 4

Bin 2058: 41 of cap free
Amount of items: 2
Items: 
Size: 636989 Color: 4
Size: 362971 Color: 1

Bin 2059: 41 of cap free
Amount of items: 2
Items: 
Size: 651327 Color: 1
Size: 348633 Color: 0

Bin 2060: 41 of cap free
Amount of items: 2
Items: 
Size: 673875 Color: 2
Size: 326085 Color: 1

Bin 2061: 41 of cap free
Amount of items: 2
Items: 
Size: 690489 Color: 3
Size: 309471 Color: 1

Bin 2062: 41 of cap free
Amount of items: 2
Items: 
Size: 692803 Color: 0
Size: 307157 Color: 4

Bin 2063: 41 of cap free
Amount of items: 2
Items: 
Size: 754771 Color: 2
Size: 245189 Color: 1

Bin 2064: 41 of cap free
Amount of items: 2
Items: 
Size: 763183 Color: 2
Size: 236777 Color: 4

Bin 2065: 42 of cap free
Amount of items: 3
Items: 
Size: 604122 Color: 3
Size: 198921 Color: 3
Size: 196916 Color: 0

Bin 2066: 42 of cap free
Amount of items: 2
Items: 
Size: 603943 Color: 4
Size: 396016 Color: 1

Bin 2067: 42 of cap free
Amount of items: 2
Items: 
Size: 577308 Color: 0
Size: 422651 Color: 3

Bin 2068: 42 of cap free
Amount of items: 2
Items: 
Size: 541033 Color: 1
Size: 458926 Color: 2

Bin 2069: 42 of cap free
Amount of items: 2
Items: 
Size: 775619 Color: 2
Size: 224340 Color: 1

Bin 2070: 42 of cap free
Amount of items: 2
Items: 
Size: 576675 Color: 0
Size: 423284 Color: 4

Bin 2071: 42 of cap free
Amount of items: 2
Items: 
Size: 675710 Color: 0
Size: 324249 Color: 1

Bin 2072: 42 of cap free
Amount of items: 2
Items: 
Size: 546543 Color: 3
Size: 453416 Color: 1

Bin 2073: 42 of cap free
Amount of items: 2
Items: 
Size: 539025 Color: 0
Size: 460934 Color: 3

Bin 2074: 42 of cap free
Amount of items: 2
Items: 
Size: 557458 Color: 0
Size: 442501 Color: 3

Bin 2075: 42 of cap free
Amount of items: 3
Items: 
Size: 569623 Color: 0
Size: 215384 Color: 1
Size: 214952 Color: 4

Bin 2076: 42 of cap free
Amount of items: 2
Items: 
Size: 594255 Color: 0
Size: 405704 Color: 4

Bin 2077: 42 of cap free
Amount of items: 2
Items: 
Size: 620773 Color: 3
Size: 379186 Color: 0

Bin 2078: 42 of cap free
Amount of items: 2
Items: 
Size: 693176 Color: 2
Size: 306783 Color: 0

Bin 2079: 42 of cap free
Amount of items: 2
Items: 
Size: 714254 Color: 4
Size: 285705 Color: 2

Bin 2080: 42 of cap free
Amount of items: 2
Items: 
Size: 762877 Color: 2
Size: 237082 Color: 1

Bin 2081: 42 of cap free
Amount of items: 2
Items: 
Size: 781948 Color: 2
Size: 218011 Color: 0

Bin 2082: 43 of cap free
Amount of items: 2
Items: 
Size: 746202 Color: 4
Size: 253756 Color: 2

Bin 2083: 43 of cap free
Amount of items: 3
Items: 
Size: 356989 Color: 1
Size: 355756 Color: 0
Size: 287213 Color: 2

Bin 2084: 43 of cap free
Amount of items: 2
Items: 
Size: 737100 Color: 3
Size: 262858 Color: 1

Bin 2085: 43 of cap free
Amount of items: 2
Items: 
Size: 671146 Color: 1
Size: 328812 Color: 3

Bin 2086: 43 of cap free
Amount of items: 2
Items: 
Size: 583603 Color: 0
Size: 416355 Color: 4

Bin 2087: 43 of cap free
Amount of items: 2
Items: 
Size: 535056 Color: 1
Size: 464902 Color: 4

Bin 2088: 43 of cap free
Amount of items: 2
Items: 
Size: 541720 Color: 4
Size: 458238 Color: 1

Bin 2089: 43 of cap free
Amount of items: 2
Items: 
Size: 561896 Color: 4
Size: 438062 Color: 3

Bin 2090: 43 of cap free
Amount of items: 2
Items: 
Size: 673055 Color: 4
Size: 326903 Color: 2

Bin 2091: 43 of cap free
Amount of items: 2
Items: 
Size: 504401 Color: 0
Size: 495557 Color: 4

Bin 2092: 43 of cap free
Amount of items: 2
Items: 
Size: 579959 Color: 3
Size: 419999 Color: 0

Bin 2093: 43 of cap free
Amount of items: 2
Items: 
Size: 640914 Color: 1
Size: 359044 Color: 0

Bin 2094: 43 of cap free
Amount of items: 2
Items: 
Size: 655602 Color: 1
Size: 344356 Color: 4

Bin 2095: 43 of cap free
Amount of items: 2
Items: 
Size: 729821 Color: 4
Size: 270137 Color: 0

Bin 2096: 43 of cap free
Amount of items: 2
Items: 
Size: 740665 Color: 2
Size: 259293 Color: 4

Bin 2097: 44 of cap free
Amount of items: 2
Items: 
Size: 688207 Color: 1
Size: 311750 Color: 2

Bin 2098: 44 of cap free
Amount of items: 2
Items: 
Size: 686759 Color: 4
Size: 313198 Color: 2

Bin 2099: 44 of cap free
Amount of items: 2
Items: 
Size: 653522 Color: 3
Size: 346435 Color: 2

Bin 2100: 44 of cap free
Amount of items: 2
Items: 
Size: 792466 Color: 0
Size: 207491 Color: 1

Bin 2101: 44 of cap free
Amount of items: 2
Items: 
Size: 531719 Color: 3
Size: 468238 Color: 4

Bin 2102: 44 of cap free
Amount of items: 2
Items: 
Size: 501605 Color: 0
Size: 498352 Color: 1

Bin 2103: 44 of cap free
Amount of items: 2
Items: 
Size: 546392 Color: 1
Size: 453565 Color: 3

Bin 2104: 44 of cap free
Amount of items: 2
Items: 
Size: 573761 Color: 1
Size: 426196 Color: 4

Bin 2105: 44 of cap free
Amount of items: 2
Items: 
Size: 587826 Color: 1
Size: 412131 Color: 3

Bin 2106: 44 of cap free
Amount of items: 2
Items: 
Size: 668032 Color: 3
Size: 331925 Color: 1

Bin 2107: 44 of cap free
Amount of items: 2
Items: 
Size: 686270 Color: 4
Size: 313687 Color: 0

Bin 2108: 45 of cap free
Amount of items: 2
Items: 
Size: 740150 Color: 0
Size: 259806 Color: 1

Bin 2109: 45 of cap free
Amount of items: 2
Items: 
Size: 562502 Color: 4
Size: 437454 Color: 3

Bin 2110: 45 of cap free
Amount of items: 2
Items: 
Size: 767201 Color: 4
Size: 232755 Color: 3

Bin 2111: 45 of cap free
Amount of items: 2
Items: 
Size: 705370 Color: 0
Size: 294586 Color: 3

Bin 2112: 45 of cap free
Amount of items: 2
Items: 
Size: 551385 Color: 4
Size: 448571 Color: 1

Bin 2113: 45 of cap free
Amount of items: 2
Items: 
Size: 634617 Color: 4
Size: 365339 Color: 3

Bin 2114: 45 of cap free
Amount of items: 2
Items: 
Size: 508175 Color: 0
Size: 491781 Color: 2

Bin 2115: 45 of cap free
Amount of items: 2
Items: 
Size: 535696 Color: 3
Size: 464260 Color: 2

Bin 2116: 45 of cap free
Amount of items: 2
Items: 
Size: 544410 Color: 2
Size: 455546 Color: 4

Bin 2117: 45 of cap free
Amount of items: 2
Items: 
Size: 546200 Color: 0
Size: 453756 Color: 3

Bin 2118: 45 of cap free
Amount of items: 2
Items: 
Size: 548800 Color: 0
Size: 451156 Color: 2

Bin 2119: 45 of cap free
Amount of items: 2
Items: 
Size: 564247 Color: 1
Size: 435709 Color: 4

Bin 2120: 45 of cap free
Amount of items: 2
Items: 
Size: 565221 Color: 4
Size: 434735 Color: 0

Bin 2121: 45 of cap free
Amount of items: 2
Items: 
Size: 566810 Color: 4
Size: 433146 Color: 0

Bin 2122: 45 of cap free
Amount of items: 2
Items: 
Size: 593231 Color: 2
Size: 406725 Color: 4

Bin 2123: 45 of cap free
Amount of items: 2
Items: 
Size: 608878 Color: 0
Size: 391078 Color: 2

Bin 2124: 45 of cap free
Amount of items: 2
Items: 
Size: 649097 Color: 2
Size: 350859 Color: 1

Bin 2125: 45 of cap free
Amount of items: 2
Items: 
Size: 664552 Color: 2
Size: 335404 Color: 1

Bin 2126: 45 of cap free
Amount of items: 2
Items: 
Size: 679112 Color: 3
Size: 320844 Color: 1

Bin 2127: 45 of cap free
Amount of items: 2
Items: 
Size: 709277 Color: 2
Size: 290679 Color: 0

Bin 2128: 46 of cap free
Amount of items: 2
Items: 
Size: 670517 Color: 0
Size: 329438 Color: 4

Bin 2129: 46 of cap free
Amount of items: 2
Items: 
Size: 675276 Color: 4
Size: 324679 Color: 3

Bin 2130: 46 of cap free
Amount of items: 2
Items: 
Size: 738044 Color: 3
Size: 261911 Color: 4

Bin 2131: 46 of cap free
Amount of items: 2
Items: 
Size: 508281 Color: 0
Size: 491674 Color: 3

Bin 2132: 46 of cap free
Amount of items: 2
Items: 
Size: 691465 Color: 0
Size: 308490 Color: 2

Bin 2133: 46 of cap free
Amount of items: 2
Items: 
Size: 594430 Color: 1
Size: 405525 Color: 3

Bin 2134: 46 of cap free
Amount of items: 2
Items: 
Size: 741830 Color: 2
Size: 258125 Color: 3

Bin 2135: 46 of cap free
Amount of items: 2
Items: 
Size: 625119 Color: 2
Size: 374836 Color: 1

Bin 2136: 46 of cap free
Amount of items: 2
Items: 
Size: 543553 Color: 3
Size: 456402 Color: 4

Bin 2137: 46 of cap free
Amount of items: 2
Items: 
Size: 588837 Color: 4
Size: 411118 Color: 2

Bin 2138: 46 of cap free
Amount of items: 2
Items: 
Size: 591863 Color: 4
Size: 408092 Color: 2

Bin 2139: 46 of cap free
Amount of items: 2
Items: 
Size: 630016 Color: 4
Size: 369939 Color: 3

Bin 2140: 46 of cap free
Amount of items: 2
Items: 
Size: 662679 Color: 0
Size: 337276 Color: 4

Bin 2141: 46 of cap free
Amount of items: 2
Items: 
Size: 722817 Color: 0
Size: 277138 Color: 2

Bin 2142: 46 of cap free
Amount of items: 2
Items: 
Size: 730802 Color: 0
Size: 269153 Color: 4

Bin 2143: 46 of cap free
Amount of items: 2
Items: 
Size: 763533 Color: 3
Size: 236422 Color: 0

Bin 2144: 46 of cap free
Amount of items: 2
Items: 
Size: 767957 Color: 0
Size: 231998 Color: 4

Bin 2145: 47 of cap free
Amount of items: 3
Items: 
Size: 696618 Color: 1
Size: 201988 Color: 1
Size: 101348 Color: 3

Bin 2146: 47 of cap free
Amount of items: 2
Items: 
Size: 526045 Color: 3
Size: 473909 Color: 0

Bin 2147: 47 of cap free
Amount of items: 2
Items: 
Size: 567434 Color: 1
Size: 432520 Color: 2

Bin 2148: 47 of cap free
Amount of items: 2
Items: 
Size: 548246 Color: 4
Size: 451708 Color: 1

Bin 2149: 47 of cap free
Amount of items: 2
Items: 
Size: 606667 Color: 2
Size: 393287 Color: 4

Bin 2150: 47 of cap free
Amount of items: 2
Items: 
Size: 521578 Color: 2
Size: 478376 Color: 0

Bin 2151: 47 of cap free
Amount of items: 2
Items: 
Size: 539174 Color: 1
Size: 460780 Color: 0

Bin 2152: 47 of cap free
Amount of items: 2
Items: 
Size: 574590 Color: 2
Size: 425364 Color: 3

Bin 2153: 47 of cap free
Amount of items: 2
Items: 
Size: 659557 Color: 0
Size: 340397 Color: 4

Bin 2154: 47 of cap free
Amount of items: 2
Items: 
Size: 675924 Color: 3
Size: 324030 Color: 4

Bin 2155: 47 of cap free
Amount of items: 2
Items: 
Size: 747748 Color: 2
Size: 252206 Color: 3

Bin 2156: 47 of cap free
Amount of items: 2
Items: 
Size: 793442 Color: 3
Size: 206512 Color: 0

Bin 2157: 48 of cap free
Amount of items: 3
Items: 
Size: 361197 Color: 2
Size: 358353 Color: 4
Size: 280403 Color: 2

Bin 2158: 48 of cap free
Amount of items: 2
Items: 
Size: 660115 Color: 4
Size: 339838 Color: 2

Bin 2159: 48 of cap free
Amount of items: 2
Items: 
Size: 663853 Color: 1
Size: 336100 Color: 0

Bin 2160: 48 of cap free
Amount of items: 2
Items: 
Size: 747945 Color: 4
Size: 252008 Color: 3

Bin 2161: 48 of cap free
Amount of items: 2
Items: 
Size: 543789 Color: 4
Size: 456164 Color: 1

Bin 2162: 48 of cap free
Amount of items: 2
Items: 
Size: 514657 Color: 1
Size: 485296 Color: 0

Bin 2163: 48 of cap free
Amount of items: 2
Items: 
Size: 575137 Color: 1
Size: 424816 Color: 0

Bin 2164: 48 of cap free
Amount of items: 2
Items: 
Size: 547942 Color: 3
Size: 452011 Color: 1

Bin 2165: 48 of cap free
Amount of items: 2
Items: 
Size: 791463 Color: 2
Size: 208490 Color: 3

Bin 2166: 48 of cap free
Amount of items: 2
Items: 
Size: 618661 Color: 4
Size: 381292 Color: 1

Bin 2167: 48 of cap free
Amount of items: 2
Items: 
Size: 509839 Color: 2
Size: 490114 Color: 1

Bin 2168: 48 of cap free
Amount of items: 2
Items: 
Size: 502078 Color: 4
Size: 497875 Color: 2

Bin 2169: 48 of cap free
Amount of items: 2
Items: 
Size: 519388 Color: 3
Size: 480565 Color: 2

Bin 2170: 48 of cap free
Amount of items: 2
Items: 
Size: 616216 Color: 3
Size: 383737 Color: 0

Bin 2171: 48 of cap free
Amount of items: 2
Items: 
Size: 683586 Color: 4
Size: 316367 Color: 0

Bin 2172: 48 of cap free
Amount of items: 2
Items: 
Size: 784353 Color: 3
Size: 215600 Color: 1

Bin 2173: 49 of cap free
Amount of items: 2
Items: 
Size: 653922 Color: 1
Size: 346030 Color: 4

Bin 2174: 49 of cap free
Amount of items: 2
Items: 
Size: 615816 Color: 0
Size: 384136 Color: 2

Bin 2175: 49 of cap free
Amount of items: 2
Items: 
Size: 716813 Color: 1
Size: 283139 Color: 3

Bin 2176: 49 of cap free
Amount of items: 2
Items: 
Size: 558780 Color: 2
Size: 441172 Color: 3

Bin 2177: 49 of cap free
Amount of items: 2
Items: 
Size: 769737 Color: 1
Size: 230215 Color: 4

Bin 2178: 49 of cap free
Amount of items: 2
Items: 
Size: 563036 Color: 1
Size: 436916 Color: 0

Bin 2179: 49 of cap free
Amount of items: 2
Items: 
Size: 522777 Color: 0
Size: 477175 Color: 2

Bin 2180: 49 of cap free
Amount of items: 2
Items: 
Size: 613560 Color: 1
Size: 386392 Color: 3

Bin 2181: 49 of cap free
Amount of items: 2
Items: 
Size: 665461 Color: 1
Size: 334491 Color: 3

Bin 2182: 49 of cap free
Amount of items: 2
Items: 
Size: 669127 Color: 0
Size: 330825 Color: 1

Bin 2183: 49 of cap free
Amount of items: 2
Items: 
Size: 712423 Color: 3
Size: 287529 Color: 4

Bin 2184: 49 of cap free
Amount of items: 2
Items: 
Size: 762796 Color: 2
Size: 237156 Color: 4

Bin 2185: 50 of cap free
Amount of items: 3
Items: 
Size: 635160 Color: 2
Size: 182416 Color: 0
Size: 182375 Color: 0

Bin 2186: 50 of cap free
Amount of items: 3
Items: 
Size: 456399 Color: 0
Size: 297206 Color: 3
Size: 246346 Color: 1

Bin 2187: 50 of cap free
Amount of items: 2
Items: 
Size: 506576 Color: 1
Size: 493375 Color: 0

Bin 2188: 50 of cap free
Amount of items: 2
Items: 
Size: 668827 Color: 1
Size: 331124 Color: 0

Bin 2189: 50 of cap free
Amount of items: 2
Items: 
Size: 579588 Color: 2
Size: 420363 Color: 1

Bin 2190: 50 of cap free
Amount of items: 2
Items: 
Size: 624719 Color: 2
Size: 375232 Color: 1

Bin 2191: 50 of cap free
Amount of items: 2
Items: 
Size: 586226 Color: 0
Size: 413725 Color: 4

Bin 2192: 50 of cap free
Amount of items: 2
Items: 
Size: 636733 Color: 4
Size: 363218 Color: 3

Bin 2193: 50 of cap free
Amount of items: 2
Items: 
Size: 728298 Color: 4
Size: 271653 Color: 1

Bin 2194: 50 of cap free
Amount of items: 2
Items: 
Size: 790951 Color: 1
Size: 209000 Color: 3

Bin 2195: 50 of cap free
Amount of items: 2
Items: 
Size: 522421 Color: 2
Size: 477530 Color: 1

Bin 2196: 50 of cap free
Amount of items: 2
Items: 
Size: 550050 Color: 0
Size: 449901 Color: 4

Bin 2197: 50 of cap free
Amount of items: 2
Items: 
Size: 576958 Color: 0
Size: 422993 Color: 1

Bin 2198: 50 of cap free
Amount of items: 2
Items: 
Size: 636228 Color: 4
Size: 363723 Color: 3

Bin 2199: 50 of cap free
Amount of items: 2
Items: 
Size: 636637 Color: 3
Size: 363314 Color: 0

Bin 2200: 50 of cap free
Amount of items: 2
Items: 
Size: 666988 Color: 0
Size: 332963 Color: 2

Bin 2201: 50 of cap free
Amount of items: 2
Items: 
Size: 733154 Color: 2
Size: 266797 Color: 1

Bin 2202: 50 of cap free
Amount of items: 2
Items: 
Size: 796000 Color: 4
Size: 203951 Color: 0

Bin 2203: 51 of cap free
Amount of items: 2
Items: 
Size: 754972 Color: 3
Size: 244978 Color: 1

Bin 2204: 51 of cap free
Amount of items: 2
Items: 
Size: 646206 Color: 4
Size: 353744 Color: 3

Bin 2205: 51 of cap free
Amount of items: 2
Items: 
Size: 770800 Color: 1
Size: 229150 Color: 0

Bin 2206: 51 of cap free
Amount of items: 2
Items: 
Size: 716874 Color: 4
Size: 283076 Color: 3

Bin 2207: 51 of cap free
Amount of items: 2
Items: 
Size: 667279 Color: 4
Size: 332671 Color: 2

Bin 2208: 51 of cap free
Amount of items: 2
Items: 
Size: 517916 Color: 3
Size: 482034 Color: 2

Bin 2209: 51 of cap free
Amount of items: 2
Items: 
Size: 733382 Color: 2
Size: 266568 Color: 3

Bin 2210: 51 of cap free
Amount of items: 2
Items: 
Size: 720744 Color: 0
Size: 279206 Color: 4

Bin 2211: 51 of cap free
Amount of items: 2
Items: 
Size: 635973 Color: 2
Size: 363977 Color: 4

Bin 2212: 51 of cap free
Amount of items: 2
Items: 
Size: 523364 Color: 4
Size: 476586 Color: 2

Bin 2213: 51 of cap free
Amount of items: 2
Items: 
Size: 524622 Color: 4
Size: 475328 Color: 2

Bin 2214: 51 of cap free
Amount of items: 2
Items: 
Size: 554702 Color: 3
Size: 445248 Color: 0

Bin 2215: 51 of cap free
Amount of items: 2
Items: 
Size: 596849 Color: 1
Size: 403101 Color: 0

Bin 2216: 51 of cap free
Amount of items: 2
Items: 
Size: 642907 Color: 4
Size: 357043 Color: 3

Bin 2217: 51 of cap free
Amount of items: 2
Items: 
Size: 781061 Color: 4
Size: 218889 Color: 0

Bin 2218: 52 of cap free
Amount of items: 3
Items: 
Size: 660812 Color: 4
Size: 169699 Color: 2
Size: 169438 Color: 1

Bin 2219: 52 of cap free
Amount of items: 2
Items: 
Size: 682793 Color: 1
Size: 317156 Color: 0

Bin 2220: 52 of cap free
Amount of items: 2
Items: 
Size: 684518 Color: 1
Size: 315431 Color: 2

Bin 2221: 52 of cap free
Amount of items: 2
Items: 
Size: 744635 Color: 1
Size: 255314 Color: 4

Bin 2222: 52 of cap free
Amount of items: 2
Items: 
Size: 512585 Color: 1
Size: 487364 Color: 2

Bin 2223: 52 of cap free
Amount of items: 2
Items: 
Size: 678420 Color: 4
Size: 321529 Color: 0

Bin 2224: 52 of cap free
Amount of items: 2
Items: 
Size: 655071 Color: 0
Size: 344878 Color: 3

Bin 2225: 52 of cap free
Amount of items: 2
Items: 
Size: 743950 Color: 0
Size: 255999 Color: 4

Bin 2226: 52 of cap free
Amount of items: 2
Items: 
Size: 625566 Color: 0
Size: 374383 Color: 1

Bin 2227: 52 of cap free
Amount of items: 2
Items: 
Size: 565533 Color: 1
Size: 434416 Color: 4

Bin 2228: 52 of cap free
Amount of items: 2
Items: 
Size: 585347 Color: 3
Size: 414602 Color: 0

Bin 2229: 52 of cap free
Amount of items: 2
Items: 
Size: 733771 Color: 4
Size: 266178 Color: 1

Bin 2230: 52 of cap free
Amount of items: 3
Items: 
Size: 351369 Color: 2
Size: 340782 Color: 4
Size: 307798 Color: 2

Bin 2231: 52 of cap free
Amount of items: 2
Items: 
Size: 501264 Color: 3
Size: 498685 Color: 1

Bin 2232: 52 of cap free
Amount of items: 2
Items: 
Size: 501046 Color: 1
Size: 498903 Color: 3

Bin 2233: 52 of cap free
Amount of items: 2
Items: 
Size: 548082 Color: 0
Size: 451867 Color: 1

Bin 2234: 52 of cap free
Amount of items: 2
Items: 
Size: 553900 Color: 1
Size: 446049 Color: 2

Bin 2235: 52 of cap free
Amount of items: 2
Items: 
Size: 709273 Color: 2
Size: 290676 Color: 4

Bin 2236: 52 of cap free
Amount of items: 2
Items: 
Size: 779521 Color: 4
Size: 220428 Color: 1

Bin 2237: 53 of cap free
Amount of items: 3
Items: 
Size: 657858 Color: 1
Size: 171975 Color: 1
Size: 170115 Color: 3

Bin 2238: 53 of cap free
Amount of items: 2
Items: 
Size: 750361 Color: 3
Size: 249587 Color: 4

Bin 2239: 53 of cap free
Amount of items: 2
Items: 
Size: 670247 Color: 2
Size: 329701 Color: 1

Bin 2240: 53 of cap free
Amount of items: 2
Items: 
Size: 763073 Color: 1
Size: 236875 Color: 3

Bin 2241: 53 of cap free
Amount of items: 2
Items: 
Size: 659449 Color: 1
Size: 340499 Color: 4

Bin 2242: 53 of cap free
Amount of items: 3
Items: 
Size: 386008 Color: 3
Size: 373635 Color: 1
Size: 240305 Color: 4

Bin 2243: 53 of cap free
Amount of items: 2
Items: 
Size: 661046 Color: 2
Size: 338902 Color: 0

Bin 2244: 53 of cap free
Amount of items: 2
Items: 
Size: 668093 Color: 3
Size: 331855 Color: 1

Bin 2245: 53 of cap free
Amount of items: 2
Items: 
Size: 533555 Color: 1
Size: 466393 Color: 3

Bin 2246: 53 of cap free
Amount of items: 2
Items: 
Size: 584672 Color: 1
Size: 415276 Color: 0

Bin 2247: 53 of cap free
Amount of items: 2
Items: 
Size: 501758 Color: 0
Size: 498190 Color: 1

Bin 2248: 53 of cap free
Amount of items: 2
Items: 
Size: 504057 Color: 3
Size: 495891 Color: 2

Bin 2249: 53 of cap free
Amount of items: 2
Items: 
Size: 523658 Color: 3
Size: 476290 Color: 1

Bin 2250: 53 of cap free
Amount of items: 2
Items: 
Size: 552396 Color: 0
Size: 447552 Color: 4

Bin 2251: 53 of cap free
Amount of items: 2
Items: 
Size: 569855 Color: 1
Size: 430093 Color: 3

Bin 2252: 53 of cap free
Amount of items: 2
Items: 
Size: 585745 Color: 0
Size: 414203 Color: 3

Bin 2253: 53 of cap free
Amount of items: 2
Items: 
Size: 587421 Color: 3
Size: 412527 Color: 0

Bin 2254: 53 of cap free
Amount of items: 2
Items: 
Size: 596274 Color: 2
Size: 403674 Color: 4

Bin 2255: 53 of cap free
Amount of items: 2
Items: 
Size: 748937 Color: 0
Size: 251011 Color: 1

Bin 2256: 54 of cap free
Amount of items: 2
Items: 
Size: 763684 Color: 0
Size: 236263 Color: 2

Bin 2257: 54 of cap free
Amount of items: 2
Items: 
Size: 504954 Color: 2
Size: 494993 Color: 3

Bin 2258: 54 of cap free
Amount of items: 2
Items: 
Size: 666258 Color: 3
Size: 333689 Color: 4

Bin 2259: 54 of cap free
Amount of items: 2
Items: 
Size: 660301 Color: 0
Size: 339646 Color: 3

Bin 2260: 54 of cap free
Amount of items: 2
Items: 
Size: 511638 Color: 2
Size: 488309 Color: 0

Bin 2261: 54 of cap free
Amount of items: 2
Items: 
Size: 662071 Color: 0
Size: 337876 Color: 1

Bin 2262: 54 of cap free
Amount of items: 2
Items: 
Size: 536816 Color: 0
Size: 463131 Color: 1

Bin 2263: 54 of cap free
Amount of items: 2
Items: 
Size: 726475 Color: 2
Size: 273472 Color: 3

Bin 2264: 54 of cap free
Amount of items: 2
Items: 
Size: 539428 Color: 2
Size: 460519 Color: 3

Bin 2265: 54 of cap free
Amount of items: 2
Items: 
Size: 575564 Color: 4
Size: 424383 Color: 0

Bin 2266: 54 of cap free
Amount of items: 2
Items: 
Size: 581718 Color: 4
Size: 418229 Color: 1

Bin 2267: 54 of cap free
Amount of items: 2
Items: 
Size: 595763 Color: 0
Size: 404184 Color: 4

Bin 2268: 54 of cap free
Amount of items: 2
Items: 
Size: 614358 Color: 0
Size: 385589 Color: 1

Bin 2269: 54 of cap free
Amount of items: 2
Items: 
Size: 639051 Color: 1
Size: 360896 Color: 4

Bin 2270: 54 of cap free
Amount of items: 2
Items: 
Size: 679375 Color: 1
Size: 320572 Color: 4

Bin 2271: 55 of cap free
Amount of items: 2
Items: 
Size: 788230 Color: 4
Size: 211716 Color: 3

Bin 2272: 55 of cap free
Amount of items: 2
Items: 
Size: 512804 Color: 0
Size: 487142 Color: 2

Bin 2273: 55 of cap free
Amount of items: 2
Items: 
Size: 736391 Color: 1
Size: 263555 Color: 0

Bin 2274: 55 of cap free
Amount of items: 2
Items: 
Size: 719470 Color: 0
Size: 280476 Color: 2

Bin 2275: 55 of cap free
Amount of items: 2
Items: 
Size: 538679 Color: 1
Size: 461267 Color: 3

Bin 2276: 55 of cap free
Amount of items: 2
Items: 
Size: 606592 Color: 4
Size: 393354 Color: 2

Bin 2277: 55 of cap free
Amount of items: 2
Items: 
Size: 775759 Color: 2
Size: 224187 Color: 1

Bin 2278: 55 of cap free
Amount of items: 2
Items: 
Size: 506423 Color: 2
Size: 493523 Color: 0

Bin 2279: 55 of cap free
Amount of items: 2
Items: 
Size: 528961 Color: 3
Size: 470985 Color: 4

Bin 2280: 55 of cap free
Amount of items: 2
Items: 
Size: 603399 Color: 2
Size: 396547 Color: 3

Bin 2281: 55 of cap free
Amount of items: 2
Items: 
Size: 634698 Color: 3
Size: 365248 Color: 4

Bin 2282: 55 of cap free
Amount of items: 2
Items: 
Size: 684723 Color: 3
Size: 315223 Color: 1

Bin 2283: 55 of cap free
Amount of items: 2
Items: 
Size: 696910 Color: 1
Size: 303036 Color: 2

Bin 2284: 55 of cap free
Amount of items: 2
Items: 
Size: 701018 Color: 3
Size: 298928 Color: 0

Bin 2285: 55 of cap free
Amount of items: 2
Items: 
Size: 728072 Color: 0
Size: 271874 Color: 1

Bin 2286: 55 of cap free
Amount of items: 2
Items: 
Size: 788505 Color: 4
Size: 211441 Color: 2

Bin 2287: 56 of cap free
Amount of items: 2
Items: 
Size: 557821 Color: 1
Size: 442124 Color: 3

Bin 2288: 56 of cap free
Amount of items: 2
Items: 
Size: 525022 Color: 0
Size: 474923 Color: 3

Bin 2289: 56 of cap free
Amount of items: 2
Items: 
Size: 703839 Color: 1
Size: 296106 Color: 2

Bin 2290: 56 of cap free
Amount of items: 2
Items: 
Size: 666675 Color: 4
Size: 333270 Color: 0

Bin 2291: 56 of cap free
Amount of items: 2
Items: 
Size: 534143 Color: 3
Size: 465802 Color: 2

Bin 2292: 56 of cap free
Amount of items: 2
Items: 
Size: 516115 Color: 2
Size: 483830 Color: 1

Bin 2293: 56 of cap free
Amount of items: 2
Items: 
Size: 531639 Color: 2
Size: 468306 Color: 1

Bin 2294: 56 of cap free
Amount of items: 2
Items: 
Size: 578020 Color: 1
Size: 421925 Color: 0

Bin 2295: 56 of cap free
Amount of items: 2
Items: 
Size: 579346 Color: 3
Size: 420599 Color: 2

Bin 2296: 56 of cap free
Amount of items: 2
Items: 
Size: 580097 Color: 1
Size: 419848 Color: 4

Bin 2297: 56 of cap free
Amount of items: 2
Items: 
Size: 591738 Color: 4
Size: 408207 Color: 0

Bin 2298: 56 of cap free
Amount of items: 2
Items: 
Size: 608028 Color: 4
Size: 391917 Color: 1

Bin 2299: 56 of cap free
Amount of items: 2
Items: 
Size: 669196 Color: 3
Size: 330749 Color: 4

Bin 2300: 56 of cap free
Amount of items: 2
Items: 
Size: 738061 Color: 4
Size: 261884 Color: 0

Bin 2301: 56 of cap free
Amount of items: 2
Items: 
Size: 796536 Color: 2
Size: 203409 Color: 3

Bin 2302: 57 of cap free
Amount of items: 2
Items: 
Size: 730271 Color: 1
Size: 269673 Color: 0

Bin 2303: 57 of cap free
Amount of items: 2
Items: 
Size: 695161 Color: 2
Size: 304783 Color: 0

Bin 2304: 57 of cap free
Amount of items: 2
Items: 
Size: 651787 Color: 3
Size: 348157 Color: 2

Bin 2305: 57 of cap free
Amount of items: 2
Items: 
Size: 731608 Color: 1
Size: 268336 Color: 3

Bin 2306: 57 of cap free
Amount of items: 2
Items: 
Size: 502935 Color: 2
Size: 497009 Color: 4

Bin 2307: 57 of cap free
Amount of items: 2
Items: 
Size: 507153 Color: 3
Size: 492791 Color: 1

Bin 2308: 57 of cap free
Amount of items: 2
Items: 
Size: 668217 Color: 1
Size: 331727 Color: 3

Bin 2309: 57 of cap free
Amount of items: 2
Items: 
Size: 673322 Color: 0
Size: 326622 Color: 1

Bin 2310: 57 of cap free
Amount of items: 2
Items: 
Size: 763272 Color: 2
Size: 236672 Color: 0

Bin 2311: 58 of cap free
Amount of items: 3
Items: 
Size: 568998 Color: 0
Size: 215475 Color: 2
Size: 215470 Color: 1

Bin 2312: 58 of cap free
Amount of items: 2
Items: 
Size: 514014 Color: 4
Size: 485929 Color: 2

Bin 2313: 58 of cap free
Amount of items: 2
Items: 
Size: 642605 Color: 2
Size: 357338 Color: 4

Bin 2314: 58 of cap free
Amount of items: 2
Items: 
Size: 514141 Color: 3
Size: 485802 Color: 0

Bin 2315: 58 of cap free
Amount of items: 2
Items: 
Size: 768312 Color: 4
Size: 231631 Color: 2

Bin 2316: 58 of cap free
Amount of items: 2
Items: 
Size: 570478 Color: 2
Size: 429465 Color: 1

Bin 2317: 58 of cap free
Amount of items: 2
Items: 
Size: 616580 Color: 3
Size: 383363 Color: 0

Bin 2318: 58 of cap free
Amount of items: 2
Items: 
Size: 775679 Color: 3
Size: 224264 Color: 2

Bin 2319: 59 of cap free
Amount of items: 2
Items: 
Size: 604874 Color: 4
Size: 395068 Color: 0

Bin 2320: 59 of cap free
Amount of items: 3
Items: 
Size: 684892 Color: 1
Size: 176470 Color: 4
Size: 138580 Color: 2

Bin 2321: 59 of cap free
Amount of items: 2
Items: 
Size: 684424 Color: 0
Size: 315518 Color: 3

Bin 2322: 59 of cap free
Amount of items: 2
Items: 
Size: 725964 Color: 2
Size: 273978 Color: 1

Bin 2323: 59 of cap free
Amount of items: 3
Items: 
Size: 653225 Color: 0
Size: 175336 Color: 2
Size: 171381 Color: 3

Bin 2324: 59 of cap free
Amount of items: 2
Items: 
Size: 760917 Color: 3
Size: 239025 Color: 1

Bin 2325: 59 of cap free
Amount of items: 2
Items: 
Size: 750278 Color: 0
Size: 249664 Color: 4

Bin 2326: 59 of cap free
Amount of items: 2
Items: 
Size: 596037 Color: 2
Size: 403905 Color: 1

Bin 2327: 59 of cap free
Amount of items: 2
Items: 
Size: 575824 Color: 2
Size: 424118 Color: 1

Bin 2328: 59 of cap free
Amount of items: 2
Items: 
Size: 600213 Color: 1
Size: 399729 Color: 4

Bin 2329: 59 of cap free
Amount of items: 2
Items: 
Size: 517171 Color: 4
Size: 482771 Color: 2

Bin 2330: 59 of cap free
Amount of items: 2
Items: 
Size: 517212 Color: 0
Size: 482730 Color: 1

Bin 2331: 59 of cap free
Amount of items: 2
Items: 
Size: 550643 Color: 1
Size: 449299 Color: 4

Bin 2332: 59 of cap free
Amount of items: 2
Items: 
Size: 586232 Color: 4
Size: 413710 Color: 1

Bin 2333: 59 of cap free
Amount of items: 2
Items: 
Size: 632642 Color: 2
Size: 367300 Color: 3

Bin 2334: 59 of cap free
Amount of items: 2
Items: 
Size: 679419 Color: 3
Size: 320523 Color: 4

Bin 2335: 59 of cap free
Amount of items: 2
Items: 
Size: 782236 Color: 4
Size: 217706 Color: 3

Bin 2336: 60 of cap free
Amount of items: 2
Items: 
Size: 642132 Color: 1
Size: 357809 Color: 3

Bin 2337: 60 of cap free
Amount of items: 2
Items: 
Size: 701919 Color: 4
Size: 298022 Color: 1

Bin 2338: 60 of cap free
Amount of items: 2
Items: 
Size: 500106 Color: 0
Size: 499835 Color: 4

Bin 2339: 60 of cap free
Amount of items: 2
Items: 
Size: 738365 Color: 1
Size: 261576 Color: 0

Bin 2340: 60 of cap free
Amount of items: 2
Items: 
Size: 637751 Color: 2
Size: 362190 Color: 1

Bin 2341: 60 of cap free
Amount of items: 2
Items: 
Size: 731982 Color: 2
Size: 267959 Color: 1

Bin 2342: 60 of cap free
Amount of items: 2
Items: 
Size: 514431 Color: 4
Size: 485510 Color: 3

Bin 2343: 60 of cap free
Amount of items: 2
Items: 
Size: 500235 Color: 0
Size: 499706 Color: 2

Bin 2344: 60 of cap free
Amount of items: 2
Items: 
Size: 550586 Color: 3
Size: 449355 Color: 0

Bin 2345: 60 of cap free
Amount of items: 2
Items: 
Size: 763347 Color: 2
Size: 236594 Color: 0

Bin 2346: 60 of cap free
Amount of items: 2
Items: 
Size: 784775 Color: 4
Size: 215166 Color: 3

Bin 2347: 61 of cap free
Amount of items: 2
Items: 
Size: 660604 Color: 0
Size: 339336 Color: 4

Bin 2348: 61 of cap free
Amount of items: 2
Items: 
Size: 734701 Color: 1
Size: 265239 Color: 2

Bin 2349: 61 of cap free
Amount of items: 2
Items: 
Size: 707783 Color: 1
Size: 292157 Color: 0

Bin 2350: 61 of cap free
Amount of items: 2
Items: 
Size: 608368 Color: 1
Size: 391572 Color: 2

Bin 2351: 61 of cap free
Amount of items: 2
Items: 
Size: 747811 Color: 2
Size: 252129 Color: 0

Bin 2352: 61 of cap free
Amount of items: 2
Items: 
Size: 501773 Color: 4
Size: 498167 Color: 1

Bin 2353: 61 of cap free
Amount of items: 2
Items: 
Size: 507653 Color: 1
Size: 492287 Color: 2

Bin 2354: 61 of cap free
Amount of items: 2
Items: 
Size: 649996 Color: 0
Size: 349944 Color: 3

Bin 2355: 61 of cap free
Amount of items: 2
Items: 
Size: 665984 Color: 4
Size: 333956 Color: 0

Bin 2356: 62 of cap free
Amount of items: 2
Items: 
Size: 706518 Color: 4
Size: 293421 Color: 0

Bin 2357: 62 of cap free
Amount of items: 2
Items: 
Size: 506029 Color: 4
Size: 493910 Color: 2

Bin 2358: 62 of cap free
Amount of items: 2
Items: 
Size: 514341 Color: 4
Size: 485598 Color: 3

Bin 2359: 62 of cap free
Amount of items: 2
Items: 
Size: 549366 Color: 2
Size: 450573 Color: 3

Bin 2360: 62 of cap free
Amount of items: 2
Items: 
Size: 753732 Color: 2
Size: 246207 Color: 3

Bin 2361: 62 of cap free
Amount of items: 2
Items: 
Size: 595610 Color: 3
Size: 404329 Color: 4

Bin 2362: 62 of cap free
Amount of items: 2
Items: 
Size: 791223 Color: 1
Size: 208716 Color: 2

Bin 2363: 62 of cap free
Amount of items: 2
Items: 
Size: 527953 Color: 4
Size: 471986 Color: 0

Bin 2364: 62 of cap free
Amount of items: 2
Items: 
Size: 500514 Color: 2
Size: 499425 Color: 1

Bin 2365: 62 of cap free
Amount of items: 2
Items: 
Size: 532226 Color: 3
Size: 467713 Color: 0

Bin 2366: 62 of cap free
Amount of items: 2
Items: 
Size: 570215 Color: 1
Size: 429724 Color: 2

Bin 2367: 62 of cap free
Amount of items: 2
Items: 
Size: 615079 Color: 1
Size: 384860 Color: 3

Bin 2368: 62 of cap free
Amount of items: 2
Items: 
Size: 681621 Color: 2
Size: 318318 Color: 3

Bin 2369: 62 of cap free
Amount of items: 2
Items: 
Size: 789696 Color: 2
Size: 210243 Color: 3

Bin 2370: 63 of cap free
Amount of items: 3
Items: 
Size: 703556 Color: 0
Size: 148624 Color: 4
Size: 147758 Color: 1

Bin 2371: 63 of cap free
Amount of items: 2
Items: 
Size: 798260 Color: 0
Size: 201678 Color: 4

Bin 2372: 63 of cap free
Amount of items: 2
Items: 
Size: 586915 Color: 1
Size: 413023 Color: 3

Bin 2373: 63 of cap free
Amount of items: 2
Items: 
Size: 736079 Color: 1
Size: 263859 Color: 2

Bin 2374: 63 of cap free
Amount of items: 2
Items: 
Size: 633808 Color: 2
Size: 366130 Color: 0

Bin 2375: 63 of cap free
Amount of items: 3
Items: 
Size: 619896 Color: 2
Size: 191036 Color: 1
Size: 189006 Color: 3

Bin 2376: 63 of cap free
Amount of items: 2
Items: 
Size: 657582 Color: 1
Size: 342356 Color: 3

Bin 2377: 63 of cap free
Amount of items: 2
Items: 
Size: 726469 Color: 1
Size: 273469 Color: 2

Bin 2378: 63 of cap free
Amount of items: 2
Items: 
Size: 556330 Color: 3
Size: 443608 Color: 4

Bin 2379: 63 of cap free
Amount of items: 2
Items: 
Size: 706247 Color: 0
Size: 293691 Color: 3

Bin 2380: 63 of cap free
Amount of items: 2
Items: 
Size: 782980 Color: 1
Size: 216958 Color: 4

Bin 2381: 63 of cap free
Amount of items: 2
Items: 
Size: 515459 Color: 1
Size: 484479 Color: 2

Bin 2382: 63 of cap free
Amount of items: 2
Items: 
Size: 518241 Color: 1
Size: 481697 Color: 3

Bin 2383: 63 of cap free
Amount of items: 2
Items: 
Size: 537526 Color: 2
Size: 462412 Color: 4

Bin 2384: 63 of cap free
Amount of items: 2
Items: 
Size: 628579 Color: 1
Size: 371359 Color: 0

Bin 2385: 63 of cap free
Amount of items: 2
Items: 
Size: 658611 Color: 4
Size: 341327 Color: 1

Bin 2386: 63 of cap free
Amount of items: 2
Items: 
Size: 692445 Color: 3
Size: 307493 Color: 1

Bin 2387: 64 of cap free
Amount of items: 2
Items: 
Size: 784346 Color: 1
Size: 215591 Color: 3

Bin 2388: 64 of cap free
Amount of items: 2
Items: 
Size: 662375 Color: 1
Size: 337562 Color: 2

Bin 2389: 64 of cap free
Amount of items: 2
Items: 
Size: 619908 Color: 4
Size: 380029 Color: 2

Bin 2390: 64 of cap free
Amount of items: 2
Items: 
Size: 669602 Color: 1
Size: 330335 Color: 2

Bin 2391: 64 of cap free
Amount of items: 2
Items: 
Size: 694362 Color: 4
Size: 305575 Color: 2

Bin 2392: 64 of cap free
Amount of items: 2
Items: 
Size: 616325 Color: 3
Size: 383612 Color: 4

Bin 2393: 64 of cap free
Amount of items: 2
Items: 
Size: 636969 Color: 2
Size: 362968 Color: 1

Bin 2394: 64 of cap free
Amount of items: 2
Items: 
Size: 637465 Color: 4
Size: 362472 Color: 3

Bin 2395: 64 of cap free
Amount of items: 2
Items: 
Size: 681761 Color: 4
Size: 318176 Color: 1

Bin 2396: 64 of cap free
Amount of items: 2
Items: 
Size: 719108 Color: 4
Size: 280829 Color: 3

Bin 2397: 65 of cap free
Amount of items: 2
Items: 
Size: 703676 Color: 1
Size: 296260 Color: 2

Bin 2398: 65 of cap free
Amount of items: 2
Items: 
Size: 625312 Color: 1
Size: 374624 Color: 4

Bin 2399: 65 of cap free
Amount of items: 2
Items: 
Size: 575988 Color: 3
Size: 423948 Color: 0

Bin 2400: 65 of cap free
Amount of items: 2
Items: 
Size: 617347 Color: 1
Size: 382589 Color: 3

Bin 2401: 65 of cap free
Amount of items: 2
Items: 
Size: 636867 Color: 3
Size: 363069 Color: 0

Bin 2402: 65 of cap free
Amount of items: 2
Items: 
Size: 522775 Color: 0
Size: 477161 Color: 1

Bin 2403: 65 of cap free
Amount of items: 2
Items: 
Size: 638971 Color: 1
Size: 360965 Color: 0

Bin 2404: 65 of cap free
Amount of items: 3
Items: 
Size: 659838 Color: 1
Size: 171111 Color: 2
Size: 168987 Color: 3

Bin 2405: 65 of cap free
Amount of items: 2
Items: 
Size: 757409 Color: 1
Size: 242527 Color: 4

Bin 2406: 66 of cap free
Amount of items: 3
Items: 
Size: 676042 Color: 2
Size: 168465 Color: 1
Size: 155428 Color: 4

Bin 2407: 66 of cap free
Amount of items: 2
Items: 
Size: 545638 Color: 3
Size: 454297 Color: 2

Bin 2408: 66 of cap free
Amount of items: 2
Items: 
Size: 635526 Color: 4
Size: 364409 Color: 0

Bin 2409: 66 of cap free
Amount of items: 2
Items: 
Size: 585095 Color: 3
Size: 414840 Color: 4

Bin 2410: 66 of cap free
Amount of items: 2
Items: 
Size: 677566 Color: 2
Size: 322369 Color: 4

Bin 2411: 66 of cap free
Amount of items: 2
Items: 
Size: 525647 Color: 1
Size: 474288 Color: 0

Bin 2412: 66 of cap free
Amount of items: 2
Items: 
Size: 532344 Color: 1
Size: 467591 Color: 4

Bin 2413: 66 of cap free
Amount of items: 2
Items: 
Size: 524805 Color: 2
Size: 475130 Color: 1

Bin 2414: 66 of cap free
Amount of items: 2
Items: 
Size: 568273 Color: 4
Size: 431662 Color: 2

Bin 2415: 66 of cap free
Amount of items: 2
Items: 
Size: 577338 Color: 3
Size: 422597 Color: 1

Bin 2416: 66 of cap free
Amount of items: 2
Items: 
Size: 591568 Color: 3
Size: 408367 Color: 1

Bin 2417: 66 of cap free
Amount of items: 2
Items: 
Size: 601725 Color: 3
Size: 398210 Color: 1

Bin 2418: 66 of cap free
Amount of items: 2
Items: 
Size: 660607 Color: 4
Size: 339328 Color: 3

Bin 2419: 66 of cap free
Amount of items: 2
Items: 
Size: 764305 Color: 3
Size: 235630 Color: 0

Bin 2420: 67 of cap free
Amount of items: 2
Items: 
Size: 639354 Color: 3
Size: 360580 Color: 4

Bin 2421: 67 of cap free
Amount of items: 3
Items: 
Size: 603480 Color: 4
Size: 229241 Color: 4
Size: 167213 Color: 2

Bin 2422: 67 of cap free
Amount of items: 2
Items: 
Size: 606932 Color: 0
Size: 393002 Color: 4

Bin 2423: 67 of cap free
Amount of items: 2
Items: 
Size: 555629 Color: 0
Size: 444305 Color: 2

Bin 2424: 67 of cap free
Amount of items: 2
Items: 
Size: 729887 Color: 2
Size: 270047 Color: 4

Bin 2425: 67 of cap free
Amount of items: 2
Items: 
Size: 557105 Color: 1
Size: 442829 Color: 4

Bin 2426: 67 of cap free
Amount of items: 2
Items: 
Size: 528108 Color: 3
Size: 471826 Color: 4

Bin 2427: 67 of cap free
Amount of items: 2
Items: 
Size: 711383 Color: 0
Size: 288551 Color: 2

Bin 2428: 67 of cap free
Amount of items: 2
Items: 
Size: 538669 Color: 3
Size: 461265 Color: 2

Bin 2429: 67 of cap free
Amount of items: 2
Items: 
Size: 544545 Color: 0
Size: 455389 Color: 3

Bin 2430: 67 of cap free
Amount of items: 2
Items: 
Size: 558097 Color: 3
Size: 441837 Color: 4

Bin 2431: 67 of cap free
Amount of items: 2
Items: 
Size: 576308 Color: 3
Size: 423626 Color: 1

Bin 2432: 67 of cap free
Amount of items: 2
Items: 
Size: 738883 Color: 4
Size: 261051 Color: 3

Bin 2433: 67 of cap free
Amount of items: 2
Items: 
Size: 795589 Color: 3
Size: 204345 Color: 2

Bin 2434: 68 of cap free
Amount of items: 2
Items: 
Size: 718879 Color: 4
Size: 281054 Color: 3

Bin 2435: 68 of cap free
Amount of items: 2
Items: 
Size: 701993 Color: 2
Size: 297940 Color: 3

Bin 2436: 68 of cap free
Amount of items: 2
Items: 
Size: 626137 Color: 2
Size: 373796 Color: 4

Bin 2437: 68 of cap free
Amount of items: 2
Items: 
Size: 507791 Color: 1
Size: 492142 Color: 4

Bin 2438: 68 of cap free
Amount of items: 2
Items: 
Size: 508508 Color: 4
Size: 491425 Color: 3

Bin 2439: 68 of cap free
Amount of items: 2
Items: 
Size: 639919 Color: 1
Size: 360014 Color: 0

Bin 2440: 68 of cap free
Amount of items: 2
Items: 
Size: 790375 Color: 0
Size: 209558 Color: 4

Bin 2441: 68 of cap free
Amount of items: 2
Items: 
Size: 792089 Color: 3
Size: 207844 Color: 2

Bin 2442: 69 of cap free
Amount of items: 2
Items: 
Size: 725419 Color: 2
Size: 274513 Color: 3

Bin 2443: 69 of cap free
Amount of items: 3
Items: 
Size: 487416 Color: 0
Size: 260416 Color: 0
Size: 252100 Color: 1

Bin 2444: 69 of cap free
Amount of items: 2
Items: 
Size: 743471 Color: 4
Size: 256461 Color: 1

Bin 2445: 69 of cap free
Amount of items: 2
Items: 
Size: 750784 Color: 1
Size: 249148 Color: 4

Bin 2446: 69 of cap free
Amount of items: 2
Items: 
Size: 546076 Color: 4
Size: 453856 Color: 2

Bin 2447: 69 of cap free
Amount of items: 2
Items: 
Size: 593020 Color: 2
Size: 406912 Color: 4

Bin 2448: 69 of cap free
Amount of items: 2
Items: 
Size: 551764 Color: 0
Size: 448168 Color: 3

Bin 2449: 69 of cap free
Amount of items: 2
Items: 
Size: 505251 Color: 4
Size: 494681 Color: 0

Bin 2450: 69 of cap free
Amount of items: 2
Items: 
Size: 540700 Color: 0
Size: 459232 Color: 1

Bin 2451: 69 of cap free
Amount of items: 2
Items: 
Size: 578713 Color: 3
Size: 421219 Color: 0

Bin 2452: 69 of cap free
Amount of items: 2
Items: 
Size: 581515 Color: 2
Size: 418417 Color: 0

Bin 2453: 69 of cap free
Amount of items: 2
Items: 
Size: 594165 Color: 2
Size: 405767 Color: 1

Bin 2454: 69 of cap free
Amount of items: 2
Items: 
Size: 629844 Color: 3
Size: 370088 Color: 0

Bin 2455: 69 of cap free
Amount of items: 2
Items: 
Size: 670636 Color: 1
Size: 329296 Color: 4

Bin 2456: 69 of cap free
Amount of items: 2
Items: 
Size: 680901 Color: 0
Size: 319031 Color: 4

Bin 2457: 69 of cap free
Amount of items: 2
Items: 
Size: 797317 Color: 3
Size: 202615 Color: 4

Bin 2458: 70 of cap free
Amount of items: 3
Items: 
Size: 410481 Color: 0
Size: 297453 Color: 4
Size: 291997 Color: 4

Bin 2459: 70 of cap free
Amount of items: 2
Items: 
Size: 527178 Color: 4
Size: 472753 Color: 1

Bin 2460: 70 of cap free
Amount of items: 2
Items: 
Size: 651902 Color: 3
Size: 348029 Color: 2

Bin 2461: 70 of cap free
Amount of items: 2
Items: 
Size: 620031 Color: 2
Size: 379900 Color: 4

Bin 2462: 70 of cap free
Amount of items: 2
Items: 
Size: 557811 Color: 4
Size: 442120 Color: 3

Bin 2463: 70 of cap free
Amount of items: 2
Items: 
Size: 560682 Color: 1
Size: 439249 Color: 0

Bin 2464: 70 of cap free
Amount of items: 2
Items: 
Size: 689963 Color: 3
Size: 309968 Color: 4

Bin 2465: 70 of cap free
Amount of items: 2
Items: 
Size: 673731 Color: 0
Size: 326200 Color: 3

Bin 2466: 70 of cap free
Amount of items: 2
Items: 
Size: 542176 Color: 1
Size: 457755 Color: 4

Bin 2467: 70 of cap free
Amount of items: 2
Items: 
Size: 664341 Color: 0
Size: 335590 Color: 2

Bin 2468: 70 of cap free
Amount of items: 2
Items: 
Size: 724523 Color: 0
Size: 275408 Color: 3

Bin 2469: 70 of cap free
Amount of items: 2
Items: 
Size: 674701 Color: 1
Size: 325230 Color: 4

Bin 2470: 70 of cap free
Amount of items: 2
Items: 
Size: 501964 Color: 2
Size: 497967 Color: 0

Bin 2471: 70 of cap free
Amount of items: 2
Items: 
Size: 516440 Color: 4
Size: 483491 Color: 2

Bin 2472: 70 of cap free
Amount of items: 2
Items: 
Size: 622463 Color: 0
Size: 377468 Color: 1

Bin 2473: 70 of cap free
Amount of items: 2
Items: 
Size: 634688 Color: 2
Size: 365243 Color: 1

Bin 2474: 70 of cap free
Amount of items: 2
Items: 
Size: 694149 Color: 4
Size: 305782 Color: 1

Bin 2475: 70 of cap free
Amount of items: 2
Items: 
Size: 718006 Color: 0
Size: 281925 Color: 4

Bin 2476: 70 of cap free
Amount of items: 2
Items: 
Size: 730755 Color: 1
Size: 269176 Color: 2

Bin 2477: 71 of cap free
Amount of items: 2
Items: 
Size: 702219 Color: 2
Size: 297711 Color: 1

Bin 2478: 71 of cap free
Amount of items: 2
Items: 
Size: 771457 Color: 2
Size: 228473 Color: 3

Bin 2479: 71 of cap free
Amount of items: 2
Items: 
Size: 633980 Color: 1
Size: 365950 Color: 4

Bin 2480: 71 of cap free
Amount of items: 2
Items: 
Size: 524031 Color: 2
Size: 475899 Color: 0

Bin 2481: 71 of cap free
Amount of items: 2
Items: 
Size: 558681 Color: 1
Size: 441249 Color: 3

Bin 2482: 71 of cap free
Amount of items: 2
Items: 
Size: 608168 Color: 2
Size: 391762 Color: 0

Bin 2483: 71 of cap free
Amount of items: 2
Items: 
Size: 697590 Color: 0
Size: 302340 Color: 4

Bin 2484: 71 of cap free
Amount of items: 2
Items: 
Size: 779990 Color: 2
Size: 219940 Color: 0

Bin 2485: 72 of cap free
Amount of items: 2
Items: 
Size: 506307 Color: 4
Size: 493622 Color: 2

Bin 2486: 72 of cap free
Amount of items: 3
Items: 
Size: 619264 Color: 2
Size: 190343 Color: 3
Size: 190322 Color: 0

Bin 2487: 72 of cap free
Amount of items: 2
Items: 
Size: 522671 Color: 2
Size: 477258 Color: 1

Bin 2488: 72 of cap free
Amount of items: 2
Items: 
Size: 544217 Color: 4
Size: 455712 Color: 0

Bin 2489: 72 of cap free
Amount of items: 2
Items: 
Size: 509661 Color: 4
Size: 490268 Color: 0

Bin 2490: 72 of cap free
Amount of items: 2
Items: 
Size: 538255 Color: 3
Size: 461674 Color: 1

Bin 2491: 72 of cap free
Amount of items: 2
Items: 
Size: 647426 Color: 0
Size: 352503 Color: 4

Bin 2492: 72 of cap free
Amount of items: 2
Items: 
Size: 659242 Color: 4
Size: 340687 Color: 2

Bin 2493: 72 of cap free
Amount of items: 2
Items: 
Size: 738257 Color: 3
Size: 261672 Color: 4

Bin 2494: 73 of cap free
Amount of items: 2
Items: 
Size: 745687 Color: 1
Size: 254241 Color: 3

Bin 2495: 73 of cap free
Amount of items: 2
Items: 
Size: 735889 Color: 2
Size: 264039 Color: 4

Bin 2496: 73 of cap free
Amount of items: 2
Items: 
Size: 761845 Color: 4
Size: 238083 Color: 2

Bin 2497: 73 of cap free
Amount of items: 2
Items: 
Size: 675334 Color: 3
Size: 324594 Color: 4

Bin 2498: 73 of cap free
Amount of items: 2
Items: 
Size: 555694 Color: 2
Size: 444234 Color: 0

Bin 2499: 73 of cap free
Amount of items: 2
Items: 
Size: 630491 Color: 0
Size: 369437 Color: 1

Bin 2500: 73 of cap free
Amount of items: 2
Items: 
Size: 657781 Color: 3
Size: 342147 Color: 2

Bin 2501: 73 of cap free
Amount of items: 2
Items: 
Size: 669119 Color: 2
Size: 330809 Color: 4

Bin 2502: 73 of cap free
Amount of items: 2
Items: 
Size: 688375 Color: 2
Size: 311553 Color: 4

Bin 2503: 73 of cap free
Amount of items: 2
Items: 
Size: 739074 Color: 2
Size: 260854 Color: 0

Bin 2504: 73 of cap free
Amount of items: 2
Items: 
Size: 758503 Color: 4
Size: 241425 Color: 3

Bin 2505: 73 of cap free
Amount of items: 2
Items: 
Size: 789869 Color: 3
Size: 210059 Color: 1

Bin 2506: 74 of cap free
Amount of items: 2
Items: 
Size: 620743 Color: 0
Size: 379184 Color: 2

Bin 2507: 74 of cap free
Amount of items: 2
Items: 
Size: 532472 Color: 1
Size: 467455 Color: 2

Bin 2508: 74 of cap free
Amount of items: 2
Items: 
Size: 534117 Color: 0
Size: 465810 Color: 3

Bin 2509: 75 of cap free
Amount of items: 2
Items: 
Size: 747978 Color: 2
Size: 251948 Color: 4

Bin 2510: 75 of cap free
Amount of items: 2
Items: 
Size: 701157 Color: 3
Size: 298769 Color: 1

Bin 2511: 75 of cap free
Amount of items: 2
Items: 
Size: 606322 Color: 4
Size: 393604 Color: 0

Bin 2512: 75 of cap free
Amount of items: 2
Items: 
Size: 678493 Color: 2
Size: 321433 Color: 0

Bin 2513: 75 of cap free
Amount of items: 2
Items: 
Size: 608249 Color: 0
Size: 391677 Color: 2

Bin 2514: 75 of cap free
Amount of items: 2
Items: 
Size: 692298 Color: 4
Size: 307628 Color: 2

Bin 2515: 75 of cap free
Amount of items: 2
Items: 
Size: 649403 Color: 0
Size: 350523 Color: 1

Bin 2516: 75 of cap free
Amount of items: 2
Items: 
Size: 668490 Color: 0
Size: 331436 Color: 3

Bin 2517: 75 of cap free
Amount of items: 2
Items: 
Size: 669985 Color: 0
Size: 329941 Color: 4

Bin 2518: 75 of cap free
Amount of items: 2
Items: 
Size: 709008 Color: 1
Size: 290918 Color: 4

Bin 2519: 76 of cap free
Amount of items: 2
Items: 
Size: 729354 Color: 1
Size: 270571 Color: 4

Bin 2520: 76 of cap free
Amount of items: 2
Items: 
Size: 712266 Color: 0
Size: 287659 Color: 2

Bin 2521: 76 of cap free
Amount of items: 2
Items: 
Size: 664340 Color: 4
Size: 335585 Color: 2

Bin 2522: 76 of cap free
Amount of items: 2
Items: 
Size: 614082 Color: 3
Size: 385843 Color: 2

Bin 2523: 76 of cap free
Amount of items: 2
Items: 
Size: 707388 Color: 0
Size: 292537 Color: 2

Bin 2524: 77 of cap free
Amount of items: 2
Items: 
Size: 722168 Color: 1
Size: 277756 Color: 2

Bin 2525: 77 of cap free
Amount of items: 2
Items: 
Size: 604254 Color: 0
Size: 395670 Color: 3

Bin 2526: 77 of cap free
Amount of items: 2
Items: 
Size: 689346 Color: 2
Size: 310578 Color: 3

Bin 2527: 77 of cap free
Amount of items: 2
Items: 
Size: 659148 Color: 4
Size: 340776 Color: 0

Bin 2528: 77 of cap free
Amount of items: 2
Items: 
Size: 508025 Color: 0
Size: 491899 Color: 2

Bin 2529: 77 of cap free
Amount of items: 2
Items: 
Size: 579555 Color: 3
Size: 420369 Color: 2

Bin 2530: 77 of cap free
Amount of items: 2
Items: 
Size: 600747 Color: 2
Size: 399177 Color: 3

Bin 2531: 77 of cap free
Amount of items: 2
Items: 
Size: 618368 Color: 4
Size: 381556 Color: 3

Bin 2532: 77 of cap free
Amount of items: 2
Items: 
Size: 683713 Color: 4
Size: 316211 Color: 2

Bin 2533: 78 of cap free
Amount of items: 2
Items: 
Size: 639115 Color: 1
Size: 360808 Color: 0

Bin 2534: 78 of cap free
Amount of items: 2
Items: 
Size: 715248 Color: 2
Size: 284675 Color: 1

Bin 2535: 78 of cap free
Amount of items: 2
Items: 
Size: 653600 Color: 4
Size: 346323 Color: 1

Bin 2536: 78 of cap free
Amount of items: 2
Items: 
Size: 708481 Color: 4
Size: 291442 Color: 3

Bin 2537: 78 of cap free
Amount of items: 2
Items: 
Size: 726210 Color: 4
Size: 273713 Color: 2

Bin 2538: 78 of cap free
Amount of items: 2
Items: 
Size: 554927 Color: 2
Size: 444996 Color: 4

Bin 2539: 78 of cap free
Amount of items: 2
Items: 
Size: 559287 Color: 4
Size: 440636 Color: 1

Bin 2540: 78 of cap free
Amount of items: 2
Items: 
Size: 606112 Color: 3
Size: 393811 Color: 0

Bin 2541: 78 of cap free
Amount of items: 2
Items: 
Size: 655258 Color: 0
Size: 344665 Color: 1

Bin 2542: 78 of cap free
Amount of items: 2
Items: 
Size: 763940 Color: 2
Size: 235983 Color: 4

Bin 2543: 79 of cap free
Amount of items: 2
Items: 
Size: 782743 Color: 3
Size: 217179 Color: 2

Bin 2544: 79 of cap free
Amount of items: 2
Items: 
Size: 737552 Color: 4
Size: 262370 Color: 2

Bin 2545: 79 of cap free
Amount of items: 2
Items: 
Size: 779778 Color: 0
Size: 220144 Color: 1

Bin 2546: 79 of cap free
Amount of items: 2
Items: 
Size: 548622 Color: 2
Size: 451300 Color: 3

Bin 2547: 79 of cap free
Amount of items: 2
Items: 
Size: 545068 Color: 2
Size: 454854 Color: 1

Bin 2548: 79 of cap free
Amount of items: 2
Items: 
Size: 570478 Color: 0
Size: 429444 Color: 4

Bin 2549: 79 of cap free
Amount of items: 2
Items: 
Size: 571601 Color: 4
Size: 428321 Color: 1

Bin 2550: 79 of cap free
Amount of items: 2
Items: 
Size: 593363 Color: 0
Size: 406559 Color: 3

Bin 2551: 79 of cap free
Amount of items: 2
Items: 
Size: 663662 Color: 3
Size: 336260 Color: 0

Bin 2552: 79 of cap free
Amount of items: 2
Items: 
Size: 760764 Color: 3
Size: 239158 Color: 2

Bin 2553: 79 of cap free
Amount of items: 2
Items: 
Size: 779345 Color: 3
Size: 220577 Color: 0

Bin 2554: 80 of cap free
Amount of items: 2
Items: 
Size: 752452 Color: 3
Size: 247469 Color: 2

Bin 2555: 80 of cap free
Amount of items: 2
Items: 
Size: 658811 Color: 4
Size: 341110 Color: 1

Bin 2556: 80 of cap free
Amount of items: 2
Items: 
Size: 704274 Color: 3
Size: 295647 Color: 4

Bin 2557: 80 of cap free
Amount of items: 2
Items: 
Size: 518851 Color: 3
Size: 481070 Color: 4

Bin 2558: 80 of cap free
Amount of items: 2
Items: 
Size: 543656 Color: 4
Size: 456265 Color: 2

Bin 2559: 80 of cap free
Amount of items: 2
Items: 
Size: 519371 Color: 3
Size: 480550 Color: 4

Bin 2560: 80 of cap free
Amount of items: 2
Items: 
Size: 798080 Color: 3
Size: 201841 Color: 0

Bin 2561: 80 of cap free
Amount of items: 2
Items: 
Size: 564940 Color: 0
Size: 434981 Color: 2

Bin 2562: 80 of cap free
Amount of items: 2
Items: 
Size: 581178 Color: 1
Size: 418743 Color: 2

Bin 2563: 80 of cap free
Amount of items: 2
Items: 
Size: 627429 Color: 0
Size: 372492 Color: 1

Bin 2564: 80 of cap free
Amount of items: 2
Items: 
Size: 659845 Color: 3
Size: 340076 Color: 4

Bin 2565: 80 of cap free
Amount of items: 2
Items: 
Size: 794780 Color: 2
Size: 205141 Color: 4

Bin 2566: 81 of cap free
Amount of items: 2
Items: 
Size: 536629 Color: 2
Size: 463291 Color: 1

Bin 2567: 81 of cap free
Amount of items: 3
Items: 
Size: 452721 Color: 0
Size: 437158 Color: 0
Size: 110041 Color: 2

Bin 2568: 81 of cap free
Amount of items: 3
Items: 
Size: 671190 Color: 2
Size: 164445 Color: 1
Size: 164285 Color: 2

Bin 2569: 81 of cap free
Amount of items: 2
Items: 
Size: 795381 Color: 2
Size: 204539 Color: 1

Bin 2570: 81 of cap free
Amount of items: 2
Items: 
Size: 501872 Color: 4
Size: 498048 Color: 3

Bin 2571: 81 of cap free
Amount of items: 2
Items: 
Size: 597372 Color: 2
Size: 402548 Color: 3

Bin 2572: 81 of cap free
Amount of items: 2
Items: 
Size: 644017 Color: 1
Size: 355903 Color: 2

Bin 2573: 81 of cap free
Amount of items: 2
Items: 
Size: 789808 Color: 2
Size: 210112 Color: 3

Bin 2574: 82 of cap free
Amount of items: 2
Items: 
Size: 736143 Color: 1
Size: 263776 Color: 2

Bin 2575: 82 of cap free
Amount of items: 3
Items: 
Size: 609656 Color: 4
Size: 196852 Color: 2
Size: 193411 Color: 2

Bin 2576: 82 of cap free
Amount of items: 2
Items: 
Size: 746237 Color: 1
Size: 253682 Color: 3

Bin 2577: 82 of cap free
Amount of items: 3
Items: 
Size: 352017 Color: 3
Size: 324011 Color: 2
Size: 323891 Color: 2

Bin 2578: 82 of cap free
Amount of items: 2
Items: 
Size: 578080 Color: 0
Size: 421839 Color: 2

Bin 2579: 82 of cap free
Amount of items: 2
Items: 
Size: 626423 Color: 0
Size: 373496 Color: 2

Bin 2580: 82 of cap free
Amount of items: 2
Items: 
Size: 765522 Color: 3
Size: 234397 Color: 1

Bin 2581: 82 of cap free
Amount of items: 2
Items: 
Size: 627635 Color: 0
Size: 372284 Color: 4

Bin 2582: 82 of cap free
Amount of items: 2
Items: 
Size: 608686 Color: 0
Size: 391233 Color: 2

Bin 2583: 82 of cap free
Amount of items: 2
Items: 
Size: 714672 Color: 3
Size: 285247 Color: 2

Bin 2584: 82 of cap free
Amount of items: 2
Items: 
Size: 611118 Color: 3
Size: 388801 Color: 1

Bin 2585: 83 of cap free
Amount of items: 2
Items: 
Size: 501395 Color: 0
Size: 498523 Color: 3

Bin 2586: 83 of cap free
Amount of items: 2
Items: 
Size: 655991 Color: 4
Size: 343927 Color: 1

Bin 2587: 83 of cap free
Amount of items: 2
Items: 
Size: 516336 Color: 4
Size: 483582 Color: 3

Bin 2588: 83 of cap free
Amount of items: 2
Items: 
Size: 503717 Color: 0
Size: 496201 Color: 1

Bin 2589: 83 of cap free
Amount of items: 2
Items: 
Size: 558766 Color: 0
Size: 441152 Color: 3

Bin 2590: 83 of cap free
Amount of items: 2
Items: 
Size: 570796 Color: 4
Size: 429122 Color: 2

Bin 2591: 83 of cap free
Amount of items: 2
Items: 
Size: 605723 Color: 3
Size: 394195 Color: 1

Bin 2592: 83 of cap free
Amount of items: 2
Items: 
Size: 751941 Color: 2
Size: 247977 Color: 3

Bin 2593: 84 of cap free
Amount of items: 2
Items: 
Size: 588331 Color: 2
Size: 411586 Color: 3

Bin 2594: 84 of cap free
Amount of items: 2
Items: 
Size: 715085 Color: 0
Size: 284832 Color: 2

Bin 2595: 84 of cap free
Amount of items: 2
Items: 
Size: 729873 Color: 4
Size: 270044 Color: 1

Bin 2596: 84 of cap free
Amount of items: 2
Items: 
Size: 528102 Color: 0
Size: 471815 Color: 1

Bin 2597: 84 of cap free
Amount of items: 2
Items: 
Size: 501022 Color: 1
Size: 498895 Color: 2

Bin 2598: 84 of cap free
Amount of items: 2
Items: 
Size: 531498 Color: 2
Size: 468419 Color: 4

Bin 2599: 84 of cap free
Amount of items: 2
Items: 
Size: 653096 Color: 3
Size: 346821 Color: 0

Bin 2600: 84 of cap free
Amount of items: 2
Items: 
Size: 663519 Color: 4
Size: 336398 Color: 1

Bin 2601: 84 of cap free
Amount of items: 2
Items: 
Size: 687403 Color: 4
Size: 312514 Color: 0

Bin 2602: 84 of cap free
Amount of items: 2
Items: 
Size: 747570 Color: 2
Size: 252347 Color: 3

Bin 2603: 85 of cap free
Amount of items: 2
Items: 
Size: 719637 Color: 4
Size: 280279 Color: 2

Bin 2604: 85 of cap free
Amount of items: 3
Items: 
Size: 410432 Color: 4
Size: 296189 Color: 4
Size: 293295 Color: 1

Bin 2605: 85 of cap free
Amount of items: 2
Items: 
Size: 646118 Color: 1
Size: 353798 Color: 3

Bin 2606: 85 of cap free
Amount of items: 2
Items: 
Size: 674180 Color: 2
Size: 325736 Color: 1

Bin 2607: 85 of cap free
Amount of items: 2
Items: 
Size: 750355 Color: 4
Size: 249561 Color: 1

Bin 2608: 85 of cap free
Amount of items: 2
Items: 
Size: 505393 Color: 4
Size: 494523 Color: 2

Bin 2609: 85 of cap free
Amount of items: 2
Items: 
Size: 653911 Color: 3
Size: 346005 Color: 1

Bin 2610: 85 of cap free
Amount of items: 2
Items: 
Size: 587570 Color: 3
Size: 412346 Color: 4

Bin 2611: 85 of cap free
Amount of items: 2
Items: 
Size: 552249 Color: 0
Size: 447667 Color: 2

Bin 2612: 85 of cap free
Amount of items: 2
Items: 
Size: 585223 Color: 2
Size: 414693 Color: 1

Bin 2613: 85 of cap free
Amount of items: 2
Items: 
Size: 682432 Color: 1
Size: 317484 Color: 4

Bin 2614: 85 of cap free
Amount of items: 2
Items: 
Size: 789289 Color: 2
Size: 210627 Color: 1

Bin 2615: 86 of cap free
Amount of items: 2
Items: 
Size: 698444 Color: 2
Size: 301471 Color: 0

Bin 2616: 86 of cap free
Amount of items: 2
Items: 
Size: 671441 Color: 1
Size: 328474 Color: 2

Bin 2617: 86 of cap free
Amount of items: 2
Items: 
Size: 529270 Color: 3
Size: 470645 Color: 2

Bin 2618: 86 of cap free
Amount of items: 2
Items: 
Size: 774959 Color: 3
Size: 224956 Color: 1

Bin 2619: 86 of cap free
Amount of items: 2
Items: 
Size: 637039 Color: 4
Size: 362876 Color: 2

Bin 2620: 86 of cap free
Amount of items: 2
Items: 
Size: 710190 Color: 2
Size: 289725 Color: 1

Bin 2621: 86 of cap free
Amount of items: 2
Items: 
Size: 561210 Color: 1
Size: 438705 Color: 4

Bin 2622: 86 of cap free
Amount of items: 2
Items: 
Size: 571419 Color: 3
Size: 428496 Color: 1

Bin 2623: 86 of cap free
Amount of items: 2
Items: 
Size: 517270 Color: 1
Size: 482645 Color: 4

Bin 2624: 86 of cap free
Amount of items: 2
Items: 
Size: 618344 Color: 0
Size: 381571 Color: 2

Bin 2625: 86 of cap free
Amount of items: 2
Items: 
Size: 635551 Color: 0
Size: 364364 Color: 4

Bin 2626: 86 of cap free
Amount of items: 2
Items: 
Size: 729731 Color: 1
Size: 270184 Color: 0

Bin 2627: 86 of cap free
Amount of items: 2
Items: 
Size: 742275 Color: 1
Size: 257640 Color: 2

Bin 2628: 87 of cap free
Amount of items: 2
Items: 
Size: 783192 Color: 4
Size: 216722 Color: 0

Bin 2629: 87 of cap free
Amount of items: 2
Items: 
Size: 568494 Color: 4
Size: 431420 Color: 0

Bin 2630: 87 of cap free
Amount of items: 2
Items: 
Size: 624106 Color: 3
Size: 375808 Color: 0

Bin 2631: 88 of cap free
Amount of items: 2
Items: 
Size: 583159 Color: 2
Size: 416754 Color: 0

Bin 2632: 88 of cap free
Amount of items: 2
Items: 
Size: 527427 Color: 3
Size: 472486 Color: 1

Bin 2633: 88 of cap free
Amount of items: 2
Items: 
Size: 590651 Color: 2
Size: 409262 Color: 1

Bin 2634: 89 of cap free
Amount of items: 2
Items: 
Size: 767410 Color: 3
Size: 232502 Color: 2

Bin 2635: 89 of cap free
Amount of items: 2
Items: 
Size: 576756 Color: 4
Size: 423156 Color: 1

Bin 2636: 89 of cap free
Amount of items: 2
Items: 
Size: 634107 Color: 0
Size: 365805 Color: 2

Bin 2637: 89 of cap free
Amount of items: 2
Items: 
Size: 551887 Color: 4
Size: 448025 Color: 0

Bin 2638: 89 of cap free
Amount of items: 2
Items: 
Size: 624244 Color: 4
Size: 375668 Color: 0

Bin 2639: 89 of cap free
Amount of items: 2
Items: 
Size: 627370 Color: 0
Size: 372542 Color: 2

Bin 2640: 89 of cap free
Amount of items: 2
Items: 
Size: 654505 Color: 4
Size: 345407 Color: 0

Bin 2641: 89 of cap free
Amount of items: 2
Items: 
Size: 673554 Color: 4
Size: 326358 Color: 0

Bin 2642: 89 of cap free
Amount of items: 2
Items: 
Size: 718789 Color: 0
Size: 281123 Color: 1

Bin 2643: 89 of cap free
Amount of items: 2
Items: 
Size: 724166 Color: 1
Size: 275746 Color: 3

Bin 2644: 90 of cap free
Amount of items: 2
Items: 
Size: 584547 Color: 3
Size: 415364 Color: 2

Bin 2645: 90 of cap free
Amount of items: 2
Items: 
Size: 518042 Color: 3
Size: 481869 Color: 0

Bin 2646: 90 of cap free
Amount of items: 2
Items: 
Size: 546752 Color: 3
Size: 453159 Color: 0

Bin 2647: 90 of cap free
Amount of items: 2
Items: 
Size: 558606 Color: 0
Size: 441305 Color: 1

Bin 2648: 90 of cap free
Amount of items: 2
Items: 
Size: 579059 Color: 4
Size: 420852 Color: 0

Bin 2649: 90 of cap free
Amount of items: 2
Items: 
Size: 630665 Color: 4
Size: 369246 Color: 3

Bin 2650: 90 of cap free
Amount of items: 2
Items: 
Size: 661599 Color: 0
Size: 338312 Color: 1

Bin 2651: 90 of cap free
Amount of items: 2
Items: 
Size: 679222 Color: 3
Size: 320689 Color: 2

Bin 2652: 90 of cap free
Amount of items: 2
Items: 
Size: 791062 Color: 3
Size: 208849 Color: 2

Bin 2653: 91 of cap free
Amount of items: 3
Items: 
Size: 725251 Color: 4
Size: 142526 Color: 0
Size: 132133 Color: 1

Bin 2654: 91 of cap free
Amount of items: 2
Items: 
Size: 761170 Color: 2
Size: 238740 Color: 1

Bin 2655: 91 of cap free
Amount of items: 2
Items: 
Size: 727286 Color: 4
Size: 272624 Color: 1

Bin 2656: 91 of cap free
Amount of items: 2
Items: 
Size: 539843 Color: 4
Size: 460067 Color: 3

Bin 2657: 91 of cap free
Amount of items: 2
Items: 
Size: 517571 Color: 0
Size: 482339 Color: 1

Bin 2658: 91 of cap free
Amount of items: 2
Items: 
Size: 571091 Color: 2
Size: 428819 Color: 0

Bin 2659: 91 of cap free
Amount of items: 2
Items: 
Size: 712718 Color: 0
Size: 287192 Color: 3

Bin 2660: 92 of cap free
Amount of items: 2
Items: 
Size: 621025 Color: 0
Size: 378884 Color: 3

Bin 2661: 92 of cap free
Amount of items: 2
Items: 
Size: 646070 Color: 3
Size: 353839 Color: 2

Bin 2662: 92 of cap free
Amount of items: 2
Items: 
Size: 583427 Color: 4
Size: 416482 Color: 1

Bin 2663: 92 of cap free
Amount of items: 2
Items: 
Size: 770887 Color: 0
Size: 229022 Color: 1

Bin 2664: 92 of cap free
Amount of items: 2
Items: 
Size: 546574 Color: 1
Size: 453335 Color: 2

Bin 2665: 92 of cap free
Amount of items: 2
Items: 
Size: 522093 Color: 1
Size: 477816 Color: 3

Bin 2666: 92 of cap free
Amount of items: 2
Items: 
Size: 542939 Color: 0
Size: 456970 Color: 3

Bin 2667: 92 of cap free
Amount of items: 2
Items: 
Size: 728589 Color: 4
Size: 271320 Color: 1

Bin 2668: 92 of cap free
Amount of items: 2
Items: 
Size: 744943 Color: 2
Size: 254966 Color: 4

Bin 2669: 93 of cap free
Amount of items: 2
Items: 
Size: 767167 Color: 1
Size: 232741 Color: 2

Bin 2670: 93 of cap free
Amount of items: 2
Items: 
Size: 653478 Color: 0
Size: 346430 Color: 2

Bin 2671: 93 of cap free
Amount of items: 2
Items: 
Size: 780601 Color: 3
Size: 219307 Color: 1

Bin 2672: 93 of cap free
Amount of items: 2
Items: 
Size: 719987 Color: 2
Size: 279921 Color: 1

Bin 2673: 93 of cap free
Amount of items: 2
Items: 
Size: 530610 Color: 2
Size: 469298 Color: 4

Bin 2674: 93 of cap free
Amount of items: 2
Items: 
Size: 735309 Color: 4
Size: 264599 Color: 2

Bin 2675: 93 of cap free
Amount of items: 2
Items: 
Size: 564037 Color: 1
Size: 435871 Color: 2

Bin 2676: 93 of cap free
Amount of items: 2
Items: 
Size: 635776 Color: 1
Size: 364132 Color: 0

Bin 2677: 93 of cap free
Amount of items: 2
Items: 
Size: 657830 Color: 3
Size: 342078 Color: 1

Bin 2678: 93 of cap free
Amount of items: 2
Items: 
Size: 696021 Color: 1
Size: 303887 Color: 4

Bin 2679: 93 of cap free
Amount of items: 2
Items: 
Size: 709096 Color: 4
Size: 290812 Color: 3

Bin 2680: 93 of cap free
Amount of items: 2
Items: 
Size: 725464 Color: 1
Size: 274444 Color: 0

Bin 2681: 93 of cap free
Amount of items: 2
Items: 
Size: 760311 Color: 0
Size: 239597 Color: 4

Bin 2682: 94 of cap free
Amount of items: 2
Items: 
Size: 734237 Color: 3
Size: 265670 Color: 0

Bin 2683: 94 of cap free
Amount of items: 2
Items: 
Size: 731210 Color: 4
Size: 268697 Color: 2

Bin 2684: 94 of cap free
Amount of items: 2
Items: 
Size: 700417 Color: 1
Size: 299490 Color: 2

Bin 2685: 94 of cap free
Amount of items: 2
Items: 
Size: 629087 Color: 4
Size: 370820 Color: 2

Bin 2686: 94 of cap free
Amount of items: 2
Items: 
Size: 629152 Color: 2
Size: 370755 Color: 1

Bin 2687: 94 of cap free
Amount of items: 2
Items: 
Size: 741602 Color: 0
Size: 258305 Color: 1

Bin 2688: 94 of cap free
Amount of items: 2
Items: 
Size: 515768 Color: 3
Size: 484139 Color: 1

Bin 2689: 94 of cap free
Amount of items: 2
Items: 
Size: 564682 Color: 4
Size: 435225 Color: 3

Bin 2690: 94 of cap free
Amount of items: 2
Items: 
Size: 567530 Color: 2
Size: 432377 Color: 3

Bin 2691: 94 of cap free
Amount of items: 2
Items: 
Size: 580327 Color: 1
Size: 419580 Color: 0

Bin 2692: 94 of cap free
Amount of items: 2
Items: 
Size: 674515 Color: 4
Size: 325392 Color: 2

Bin 2693: 95 of cap free
Amount of items: 2
Items: 
Size: 723016 Color: 1
Size: 276890 Color: 2

Bin 2694: 95 of cap free
Amount of items: 2
Items: 
Size: 648056 Color: 1
Size: 351850 Color: 3

Bin 2695: 95 of cap free
Amount of items: 2
Items: 
Size: 535211 Color: 2
Size: 464695 Color: 4

Bin 2696: 95 of cap free
Amount of items: 2
Items: 
Size: 637445 Color: 2
Size: 362461 Color: 3

Bin 2697: 95 of cap free
Amount of items: 2
Items: 
Size: 530512 Color: 2
Size: 469394 Color: 3

Bin 2698: 95 of cap free
Amount of items: 2
Items: 
Size: 786467 Color: 3
Size: 213439 Color: 0

Bin 2699: 96 of cap free
Amount of items: 2
Items: 
Size: 712476 Color: 2
Size: 287429 Color: 0

Bin 2700: 96 of cap free
Amount of items: 2
Items: 
Size: 551487 Color: 4
Size: 448418 Color: 1

Bin 2701: 96 of cap free
Amount of items: 2
Items: 
Size: 503996 Color: 0
Size: 495909 Color: 3

Bin 2702: 96 of cap free
Amount of items: 2
Items: 
Size: 689602 Color: 2
Size: 310303 Color: 1

Bin 2703: 96 of cap free
Amount of items: 2
Items: 
Size: 643120 Color: 0
Size: 356785 Color: 2

Bin 2704: 96 of cap free
Amount of items: 2
Items: 
Size: 587156 Color: 1
Size: 412749 Color: 0

Bin 2705: 96 of cap free
Amount of items: 2
Items: 
Size: 738603 Color: 0
Size: 261302 Color: 1

Bin 2706: 96 of cap free
Amount of items: 2
Items: 
Size: 739522 Color: 0
Size: 260383 Color: 2

Bin 2707: 96 of cap free
Amount of items: 2
Items: 
Size: 761474 Color: 0
Size: 238431 Color: 3

Bin 2708: 96 of cap free
Amount of items: 2
Items: 
Size: 798414 Color: 4
Size: 201491 Color: 0

Bin 2709: 97 of cap free
Amount of items: 2
Items: 
Size: 744770 Color: 4
Size: 255134 Color: 0

Bin 2710: 97 of cap free
Amount of items: 3
Items: 
Size: 620745 Color: 1
Size: 190828 Color: 2
Size: 188331 Color: 1

Bin 2711: 97 of cap free
Amount of items: 2
Items: 
Size: 533373 Color: 1
Size: 466531 Color: 0

Bin 2712: 97 of cap free
Amount of items: 2
Items: 
Size: 509558 Color: 0
Size: 490346 Color: 4

Bin 2713: 97 of cap free
Amount of items: 2
Items: 
Size: 760175 Color: 4
Size: 239729 Color: 0

Bin 2714: 97 of cap free
Amount of items: 2
Items: 
Size: 762848 Color: 4
Size: 237056 Color: 2

Bin 2715: 98 of cap free
Amount of items: 2
Items: 
Size: 703223 Color: 3
Size: 296680 Color: 1

Bin 2716: 98 of cap free
Amount of items: 2
Items: 
Size: 788112 Color: 2
Size: 211791 Color: 0

Bin 2717: 98 of cap free
Amount of items: 2
Items: 
Size: 614430 Color: 2
Size: 385473 Color: 0

Bin 2718: 98 of cap free
Amount of items: 2
Items: 
Size: 594240 Color: 3
Size: 405663 Color: 0

Bin 2719: 98 of cap free
Amount of items: 2
Items: 
Size: 636532 Color: 2
Size: 363371 Color: 1

Bin 2720: 98 of cap free
Amount of items: 2
Items: 
Size: 749185 Color: 0
Size: 250718 Color: 1

Bin 2721: 98 of cap free
Amount of items: 2
Items: 
Size: 665168 Color: 4
Size: 334735 Color: 1

Bin 2722: 98 of cap free
Amount of items: 2
Items: 
Size: 693353 Color: 2
Size: 306550 Color: 4

Bin 2723: 99 of cap free
Amount of items: 2
Items: 
Size: 605843 Color: 2
Size: 394059 Color: 4

Bin 2724: 99 of cap free
Amount of items: 2
Items: 
Size: 773715 Color: 2
Size: 226187 Color: 3

Bin 2725: 99 of cap free
Amount of items: 2
Items: 
Size: 622991 Color: 0
Size: 376911 Color: 1

Bin 2726: 99 of cap free
Amount of items: 2
Items: 
Size: 752868 Color: 3
Size: 247034 Color: 1

Bin 2727: 99 of cap free
Amount of items: 2
Items: 
Size: 768922 Color: 2
Size: 230980 Color: 0

Bin 2728: 99 of cap free
Amount of items: 2
Items: 
Size: 669235 Color: 4
Size: 330667 Color: 1

Bin 2729: 99 of cap free
Amount of items: 2
Items: 
Size: 550861 Color: 2
Size: 449041 Color: 4

Bin 2730: 99 of cap free
Amount of items: 2
Items: 
Size: 673243 Color: 0
Size: 326659 Color: 2

Bin 2731: 99 of cap free
Amount of items: 2
Items: 
Size: 720367 Color: 1
Size: 279535 Color: 3

Bin 2732: 99 of cap free
Amount of items: 2
Items: 
Size: 770359 Color: 0
Size: 229543 Color: 2

Bin 2733: 100 of cap free
Amount of items: 2
Items: 
Size: 772575 Color: 3
Size: 227326 Color: 0

Bin 2734: 100 of cap free
Amount of items: 2
Items: 
Size: 736521 Color: 0
Size: 263380 Color: 3

Bin 2735: 100 of cap free
Amount of items: 2
Items: 
Size: 741540 Color: 4
Size: 258361 Color: 0

Bin 2736: 100 of cap free
Amount of items: 2
Items: 
Size: 559534 Color: 2
Size: 440367 Color: 1

Bin 2737: 100 of cap free
Amount of items: 2
Items: 
Size: 505178 Color: 2
Size: 494723 Color: 3

Bin 2738: 100 of cap free
Amount of items: 2
Items: 
Size: 651995 Color: 1
Size: 347906 Color: 4

Bin 2739: 101 of cap free
Amount of items: 2
Items: 
Size: 671348 Color: 2
Size: 328552 Color: 4

Bin 2740: 101 of cap free
Amount of items: 2
Items: 
Size: 542118 Color: 4
Size: 457782 Color: 1

Bin 2741: 101 of cap free
Amount of items: 2
Items: 
Size: 693513 Color: 4
Size: 306387 Color: 3

Bin 2742: 101 of cap free
Amount of items: 2
Items: 
Size: 534466 Color: 1
Size: 465434 Color: 4

Bin 2743: 101 of cap free
Amount of items: 2
Items: 
Size: 735820 Color: 1
Size: 264080 Color: 2

Bin 2744: 101 of cap free
Amount of items: 2
Items: 
Size: 542788 Color: 2
Size: 457112 Color: 3

Bin 2745: 101 of cap free
Amount of items: 2
Items: 
Size: 577750 Color: 3
Size: 422150 Color: 2

Bin 2746: 101 of cap free
Amount of items: 2
Items: 
Size: 715865 Color: 0
Size: 284035 Color: 1

Bin 2747: 102 of cap free
Amount of items: 2
Items: 
Size: 590310 Color: 4
Size: 409589 Color: 1

Bin 2748: 102 of cap free
Amount of items: 2
Items: 
Size: 707232 Color: 1
Size: 292667 Color: 4

Bin 2749: 102 of cap free
Amount of items: 2
Items: 
Size: 549348 Color: 4
Size: 450551 Color: 1

Bin 2750: 102 of cap free
Amount of items: 2
Items: 
Size: 741799 Color: 1
Size: 258100 Color: 4

Bin 2751: 102 of cap free
Amount of items: 2
Items: 
Size: 680756 Color: 2
Size: 319143 Color: 0

Bin 2752: 102 of cap free
Amount of items: 2
Items: 
Size: 516797 Color: 1
Size: 483102 Color: 2

Bin 2753: 102 of cap free
Amount of items: 2
Items: 
Size: 745279 Color: 4
Size: 254620 Color: 1

Bin 2754: 102 of cap free
Amount of items: 2
Items: 
Size: 782304 Color: 1
Size: 217595 Color: 0

Bin 2755: 102 of cap free
Amount of items: 2
Items: 
Size: 564983 Color: 2
Size: 434916 Color: 0

Bin 2756: 102 of cap free
Amount of items: 2
Items: 
Size: 578552 Color: 3
Size: 421347 Color: 0

Bin 2757: 102 of cap free
Amount of items: 2
Items: 
Size: 781430 Color: 3
Size: 218469 Color: 0

Bin 2758: 102 of cap free
Amount of items: 2
Items: 
Size: 795035 Color: 3
Size: 204864 Color: 1

Bin 2759: 103 of cap free
Amount of items: 2
Items: 
Size: 744454 Color: 3
Size: 255444 Color: 2

Bin 2760: 103 of cap free
Amount of items: 2
Items: 
Size: 661271 Color: 0
Size: 338627 Color: 2

Bin 2761: 103 of cap free
Amount of items: 2
Items: 
Size: 555275 Color: 2
Size: 444623 Color: 1

Bin 2762: 103 of cap free
Amount of items: 2
Items: 
Size: 660312 Color: 3
Size: 339586 Color: 1

Bin 2763: 104 of cap free
Amount of items: 2
Items: 
Size: 778871 Color: 0
Size: 221026 Color: 4

Bin 2764: 104 of cap free
Amount of items: 2
Items: 
Size: 772364 Color: 0
Size: 227533 Color: 3

Bin 2765: 104 of cap free
Amount of items: 2
Items: 
Size: 762562 Color: 3
Size: 237335 Color: 1

Bin 2766: 104 of cap free
Amount of items: 2
Items: 
Size: 568874 Color: 3
Size: 431023 Color: 0

Bin 2767: 104 of cap free
Amount of items: 2
Items: 
Size: 597301 Color: 1
Size: 402596 Color: 0

Bin 2768: 104 of cap free
Amount of items: 2
Items: 
Size: 646539 Color: 3
Size: 353358 Color: 4

Bin 2769: 104 of cap free
Amount of items: 2
Items: 
Size: 723220 Color: 3
Size: 276677 Color: 4

Bin 2770: 104 of cap free
Amount of items: 2
Items: 
Size: 795735 Color: 4
Size: 204162 Color: 3

Bin 2771: 105 of cap free
Amount of items: 3
Items: 
Size: 658727 Color: 2
Size: 170677 Color: 1
Size: 170492 Color: 4

Bin 2772: 105 of cap free
Amount of items: 2
Items: 
Size: 530736 Color: 0
Size: 469160 Color: 2

Bin 2773: 105 of cap free
Amount of items: 2
Items: 
Size: 771027 Color: 3
Size: 228869 Color: 1

Bin 2774: 105 of cap free
Amount of items: 2
Items: 
Size: 612107 Color: 2
Size: 387789 Color: 3

Bin 2775: 105 of cap free
Amount of items: 2
Items: 
Size: 705731 Color: 0
Size: 294165 Color: 2

Bin 2776: 105 of cap free
Amount of items: 2
Items: 
Size: 728422 Color: 1
Size: 271474 Color: 4

Bin 2777: 105 of cap free
Amount of items: 2
Items: 
Size: 506103 Color: 0
Size: 493793 Color: 4

Bin 2778: 105 of cap free
Amount of items: 2
Items: 
Size: 569444 Color: 4
Size: 430452 Color: 0

Bin 2779: 105 of cap free
Amount of items: 2
Items: 
Size: 623314 Color: 0
Size: 376582 Color: 4

Bin 2780: 105 of cap free
Amount of items: 2
Items: 
Size: 631549 Color: 2
Size: 368347 Color: 3

Bin 2781: 106 of cap free
Amount of items: 2
Items: 
Size: 706984 Color: 3
Size: 292911 Color: 2

Bin 2782: 106 of cap free
Amount of items: 2
Items: 
Size: 765198 Color: 0
Size: 234697 Color: 1

Bin 2783: 106 of cap free
Amount of items: 2
Items: 
Size: 521226 Color: 1
Size: 478669 Color: 2

Bin 2784: 106 of cap free
Amount of items: 2
Items: 
Size: 548666 Color: 3
Size: 451229 Color: 4

Bin 2785: 106 of cap free
Amount of items: 2
Items: 
Size: 576094 Color: 3
Size: 423801 Color: 4

Bin 2786: 106 of cap free
Amount of items: 2
Items: 
Size: 671706 Color: 1
Size: 328189 Color: 2

Bin 2787: 106 of cap free
Amount of items: 2
Items: 
Size: 720672 Color: 1
Size: 279223 Color: 0

Bin 2788: 107 of cap free
Amount of items: 3
Items: 
Size: 468678 Color: 0
Size: 267844 Color: 2
Size: 263372 Color: 2

Bin 2789: 107 of cap free
Amount of items: 2
Items: 
Size: 640794 Color: 1
Size: 359100 Color: 3

Bin 2790: 107 of cap free
Amount of items: 2
Items: 
Size: 700128 Color: 1
Size: 299766 Color: 2

Bin 2791: 107 of cap free
Amount of items: 2
Items: 
Size: 568982 Color: 0
Size: 430912 Color: 2

Bin 2792: 107 of cap free
Amount of items: 2
Items: 
Size: 796815 Color: 4
Size: 203079 Color: 3

Bin 2793: 107 of cap free
Amount of items: 2
Items: 
Size: 715860 Color: 4
Size: 284034 Color: 3

Bin 2794: 107 of cap free
Amount of items: 2
Items: 
Size: 579653 Color: 4
Size: 420241 Color: 2

Bin 2795: 107 of cap free
Amount of items: 2
Items: 
Size: 683810 Color: 3
Size: 316084 Color: 4

Bin 2796: 108 of cap free
Amount of items: 2
Items: 
Size: 604096 Color: 4
Size: 395797 Color: 1

Bin 2797: 108 of cap free
Amount of items: 2
Items: 
Size: 632220 Color: 1
Size: 367673 Color: 3

Bin 2798: 108 of cap free
Amount of items: 2
Items: 
Size: 530881 Color: 0
Size: 469012 Color: 3

Bin 2799: 108 of cap free
Amount of items: 2
Items: 
Size: 554038 Color: 4
Size: 445855 Color: 0

Bin 2800: 108 of cap free
Amount of items: 2
Items: 
Size: 568259 Color: 4
Size: 431634 Color: 2

Bin 2801: 108 of cap free
Amount of items: 2
Items: 
Size: 574216 Color: 0
Size: 425677 Color: 4

Bin 2802: 109 of cap free
Amount of items: 3
Items: 
Size: 549554 Color: 3
Size: 225183 Color: 2
Size: 225155 Color: 1

Bin 2803: 109 of cap free
Amount of items: 2
Items: 
Size: 734391 Color: 4
Size: 265501 Color: 3

Bin 2804: 109 of cap free
Amount of items: 2
Items: 
Size: 651909 Color: 0
Size: 347983 Color: 1

Bin 2805: 109 of cap free
Amount of items: 2
Items: 
Size: 518659 Color: 1
Size: 481233 Color: 3

Bin 2806: 109 of cap free
Amount of items: 2
Items: 
Size: 587769 Color: 2
Size: 412123 Color: 1

Bin 2807: 110 of cap free
Amount of items: 2
Items: 
Size: 727025 Color: 3
Size: 272866 Color: 1

Bin 2808: 110 of cap free
Amount of items: 2
Items: 
Size: 516170 Color: 1
Size: 483721 Color: 2

Bin 2809: 110 of cap free
Amount of items: 2
Items: 
Size: 536221 Color: 4
Size: 463670 Color: 3

Bin 2810: 110 of cap free
Amount of items: 2
Items: 
Size: 562510 Color: 3
Size: 437381 Color: 0

Bin 2811: 111 of cap free
Amount of items: 2
Items: 
Size: 590167 Color: 3
Size: 409723 Color: 2

Bin 2812: 111 of cap free
Amount of items: 2
Items: 
Size: 718121 Color: 2
Size: 281769 Color: 4

Bin 2813: 111 of cap free
Amount of items: 2
Items: 
Size: 705079 Color: 0
Size: 294811 Color: 3

Bin 2814: 111 of cap free
Amount of items: 2
Items: 
Size: 576604 Color: 1
Size: 423286 Color: 4

Bin 2815: 111 of cap free
Amount of items: 2
Items: 
Size: 593300 Color: 0
Size: 406590 Color: 2

Bin 2816: 111 of cap free
Amount of items: 2
Items: 
Size: 649793 Color: 4
Size: 350097 Color: 0

Bin 2817: 112 of cap free
Amount of items: 2
Items: 
Size: 759436 Color: 2
Size: 240453 Color: 4

Bin 2818: 112 of cap free
Amount of items: 2
Items: 
Size: 593229 Color: 2
Size: 406660 Color: 0

Bin 2819: 112 of cap free
Amount of items: 2
Items: 
Size: 733097 Color: 0
Size: 266792 Color: 2

Bin 2820: 112 of cap free
Amount of items: 2
Items: 
Size: 718557 Color: 3
Size: 281332 Color: 4

Bin 2821: 112 of cap free
Amount of items: 2
Items: 
Size: 780028 Color: 1
Size: 219861 Color: 3

Bin 2822: 113 of cap free
Amount of items: 2
Items: 
Size: 548053 Color: 2
Size: 451835 Color: 3

Bin 2823: 113 of cap free
Amount of items: 2
Items: 
Size: 632086 Color: 4
Size: 367802 Color: 3

Bin 2824: 113 of cap free
Amount of items: 2
Items: 
Size: 787232 Color: 2
Size: 212656 Color: 3

Bin 2825: 113 of cap free
Amount of items: 2
Items: 
Size: 685728 Color: 2
Size: 314160 Color: 1

Bin 2826: 113 of cap free
Amount of items: 2
Items: 
Size: 584250 Color: 2
Size: 415638 Color: 4

Bin 2827: 113 of cap free
Amount of items: 2
Items: 
Size: 544833 Color: 3
Size: 455055 Color: 2

Bin 2828: 113 of cap free
Amount of items: 2
Items: 
Size: 510637 Color: 4
Size: 489251 Color: 2

Bin 2829: 113 of cap free
Amount of items: 2
Items: 
Size: 601096 Color: 2
Size: 398792 Color: 0

Bin 2830: 113 of cap free
Amount of items: 2
Items: 
Size: 693890 Color: 1
Size: 305998 Color: 2

Bin 2831: 114 of cap free
Amount of items: 2
Items: 
Size: 648245 Color: 1
Size: 351642 Color: 0

Bin 2832: 114 of cap free
Amount of items: 2
Items: 
Size: 608253 Color: 2
Size: 391634 Color: 4

Bin 2833: 114 of cap free
Amount of items: 2
Items: 
Size: 538954 Color: 4
Size: 460933 Color: 1

Bin 2834: 114 of cap free
Amount of items: 2
Items: 
Size: 513711 Color: 3
Size: 486176 Color: 0

Bin 2835: 114 of cap free
Amount of items: 2
Items: 
Size: 571035 Color: 3
Size: 428852 Color: 2

Bin 2836: 114 of cap free
Amount of items: 2
Items: 
Size: 571555 Color: 2
Size: 428332 Color: 3

Bin 2837: 114 of cap free
Amount of items: 2
Items: 
Size: 658112 Color: 3
Size: 341775 Color: 1

Bin 2838: 114 of cap free
Amount of items: 2
Items: 
Size: 701764 Color: 1
Size: 298123 Color: 4

Bin 2839: 114 of cap free
Amount of items: 2
Items: 
Size: 721432 Color: 1
Size: 278455 Color: 2

Bin 2840: 114 of cap free
Amount of items: 2
Items: 
Size: 773036 Color: 4
Size: 226851 Color: 1

Bin 2841: 115 of cap free
Amount of items: 2
Items: 
Size: 597368 Color: 3
Size: 402518 Color: 1

Bin 2842: 115 of cap free
Amount of items: 2
Items: 
Size: 790146 Color: 2
Size: 209740 Color: 0

Bin 2843: 115 of cap free
Amount of items: 2
Items: 
Size: 785972 Color: 4
Size: 213914 Color: 3

Bin 2844: 115 of cap free
Amount of items: 2
Items: 
Size: 758642 Color: 3
Size: 241244 Color: 4

Bin 2845: 115 of cap free
Amount of items: 2
Items: 
Size: 634779 Color: 0
Size: 365107 Color: 2

Bin 2846: 115 of cap free
Amount of items: 2
Items: 
Size: 757542 Color: 1
Size: 242344 Color: 0

Bin 2847: 115 of cap free
Amount of items: 2
Items: 
Size: 537815 Color: 3
Size: 462071 Color: 2

Bin 2848: 115 of cap free
Amount of items: 2
Items: 
Size: 556449 Color: 0
Size: 443437 Color: 4

Bin 2849: 115 of cap free
Amount of items: 2
Items: 
Size: 576554 Color: 1
Size: 423332 Color: 2

Bin 2850: 115 of cap free
Amount of items: 2
Items: 
Size: 780026 Color: 1
Size: 219860 Color: 3

Bin 2851: 115 of cap free
Amount of items: 2
Items: 
Size: 795141 Color: 0
Size: 204745 Color: 2

Bin 2852: 116 of cap free
Amount of items: 3
Items: 
Size: 648694 Color: 2
Size: 176902 Color: 2
Size: 174289 Color: 3

Bin 2853: 116 of cap free
Amount of items: 2
Items: 
Size: 550981 Color: 2
Size: 448904 Color: 0

Bin 2854: 116 of cap free
Amount of items: 2
Items: 
Size: 509359 Color: 2
Size: 490526 Color: 3

Bin 2855: 116 of cap free
Amount of items: 2
Items: 
Size: 565037 Color: 1
Size: 434848 Color: 0

Bin 2856: 117 of cap free
Amount of items: 3
Items: 
Size: 718431 Color: 4
Size: 147758 Color: 1
Size: 133695 Color: 0

Bin 2857: 117 of cap free
Amount of items: 2
Items: 
Size: 548945 Color: 4
Size: 450939 Color: 3

Bin 2858: 118 of cap free
Amount of items: 2
Items: 
Size: 633795 Color: 4
Size: 366088 Color: 2

Bin 2859: 118 of cap free
Amount of items: 2
Items: 
Size: 613227 Color: 0
Size: 386656 Color: 2

Bin 2860: 118 of cap free
Amount of items: 2
Items: 
Size: 609821 Color: 4
Size: 390062 Color: 2

Bin 2861: 118 of cap free
Amount of items: 2
Items: 
Size: 631853 Color: 1
Size: 368030 Color: 3

Bin 2862: 118 of cap free
Amount of items: 2
Items: 
Size: 756815 Color: 4
Size: 243068 Color: 1

Bin 2863: 118 of cap free
Amount of items: 2
Items: 
Size: 764776 Color: 1
Size: 235107 Color: 2

Bin 2864: 118 of cap free
Amount of items: 2
Items: 
Size: 721683 Color: 1
Size: 278200 Color: 2

Bin 2865: 118 of cap free
Amount of items: 2
Items: 
Size: 625529 Color: 3
Size: 374354 Color: 0

Bin 2866: 119 of cap free
Amount of items: 2
Items: 
Size: 627790 Color: 1
Size: 372092 Color: 2

Bin 2867: 119 of cap free
Amount of items: 2
Items: 
Size: 560641 Color: 1
Size: 439241 Color: 3

Bin 2868: 119 of cap free
Amount of items: 2
Items: 
Size: 591736 Color: 3
Size: 408146 Color: 1

Bin 2869: 119 of cap free
Amount of items: 2
Items: 
Size: 609045 Color: 2
Size: 390837 Color: 4

Bin 2870: 119 of cap free
Amount of items: 2
Items: 
Size: 507344 Color: 4
Size: 492538 Color: 1

Bin 2871: 119 of cap free
Amount of items: 2
Items: 
Size: 625919 Color: 3
Size: 373963 Color: 1

Bin 2872: 119 of cap free
Amount of items: 2
Items: 
Size: 683458 Color: 4
Size: 316424 Color: 2

Bin 2873: 119 of cap free
Amount of items: 2
Items: 
Size: 739925 Color: 2
Size: 259957 Color: 3

Bin 2874: 120 of cap free
Amount of items: 2
Items: 
Size: 615508 Color: 3
Size: 384373 Color: 4

Bin 2875: 120 of cap free
Amount of items: 2
Items: 
Size: 709537 Color: 2
Size: 290344 Color: 1

Bin 2876: 120 of cap free
Amount of items: 2
Items: 
Size: 631979 Color: 0
Size: 367902 Color: 2

Bin 2877: 120 of cap free
Amount of items: 2
Items: 
Size: 519221 Color: 3
Size: 480660 Color: 1

Bin 2878: 120 of cap free
Amount of items: 2
Items: 
Size: 741027 Color: 1
Size: 258854 Color: 3

Bin 2879: 120 of cap free
Amount of items: 2
Items: 
Size: 584106 Color: 0
Size: 415775 Color: 2

Bin 2880: 120 of cap free
Amount of items: 2
Items: 
Size: 509266 Color: 4
Size: 490615 Color: 2

Bin 2881: 121 of cap free
Amount of items: 2
Items: 
Size: 693019 Color: 4
Size: 306861 Color: 2

Bin 2882: 121 of cap free
Amount of items: 2
Items: 
Size: 564623 Color: 3
Size: 435257 Color: 4

Bin 2883: 121 of cap free
Amount of items: 2
Items: 
Size: 724653 Color: 3
Size: 275227 Color: 1

Bin 2884: 122 of cap free
Amount of items: 2
Items: 
Size: 604814 Color: 3
Size: 395065 Color: 0

Bin 2885: 122 of cap free
Amount of items: 2
Items: 
Size: 572540 Color: 1
Size: 427339 Color: 2

Bin 2886: 122 of cap free
Amount of items: 2
Items: 
Size: 570788 Color: 2
Size: 429091 Color: 1

Bin 2887: 122 of cap free
Amount of items: 2
Items: 
Size: 575129 Color: 1
Size: 424750 Color: 4

Bin 2888: 122 of cap free
Amount of items: 2
Items: 
Size: 758458 Color: 1
Size: 241421 Color: 2

Bin 2889: 122 of cap free
Amount of items: 2
Items: 
Size: 773044 Color: 0
Size: 226835 Color: 3

Bin 2890: 123 of cap free
Amount of items: 2
Items: 
Size: 791654 Color: 0
Size: 208224 Color: 3

Bin 2891: 123 of cap free
Amount of items: 2
Items: 
Size: 616338 Color: 4
Size: 383540 Color: 0

Bin 2892: 123 of cap free
Amount of items: 2
Items: 
Size: 560368 Color: 4
Size: 439510 Color: 2

Bin 2893: 123 of cap free
Amount of items: 2
Items: 
Size: 565160 Color: 1
Size: 434718 Color: 3

Bin 2894: 123 of cap free
Amount of items: 2
Items: 
Size: 546856 Color: 2
Size: 453022 Color: 1

Bin 2895: 123 of cap free
Amount of items: 2
Items: 
Size: 728740 Color: 4
Size: 271138 Color: 0

Bin 2896: 123 of cap free
Amount of items: 2
Items: 
Size: 789530 Color: 1
Size: 210348 Color: 0

Bin 2897: 124 of cap free
Amount of items: 2
Items: 
Size: 530228 Color: 4
Size: 469649 Color: 3

Bin 2898: 124 of cap free
Amount of items: 2
Items: 
Size: 598059 Color: 3
Size: 401818 Color: 4

Bin 2899: 124 of cap free
Amount of items: 2
Items: 
Size: 569180 Color: 1
Size: 430697 Color: 3

Bin 2900: 125 of cap free
Amount of items: 2
Items: 
Size: 787225 Color: 0
Size: 212651 Color: 4

Bin 2901: 125 of cap free
Amount of items: 2
Items: 
Size: 632790 Color: 4
Size: 367086 Color: 3

Bin 2902: 125 of cap free
Amount of items: 2
Items: 
Size: 631852 Color: 1
Size: 368024 Color: 3

Bin 2903: 125 of cap free
Amount of items: 2
Items: 
Size: 538422 Color: 1
Size: 461454 Color: 4

Bin 2904: 125 of cap free
Amount of items: 2
Items: 
Size: 777431 Color: 1
Size: 222445 Color: 4

Bin 2905: 125 of cap free
Amount of items: 2
Items: 
Size: 553126 Color: 1
Size: 446750 Color: 2

Bin 2906: 125 of cap free
Amount of items: 2
Items: 
Size: 581607 Color: 4
Size: 418269 Color: 2

Bin 2907: 125 of cap free
Amount of items: 2
Items: 
Size: 787639 Color: 0
Size: 212237 Color: 2

Bin 2908: 126 of cap free
Amount of items: 2
Items: 
Size: 611531 Color: 3
Size: 388344 Color: 2

Bin 2909: 126 of cap free
Amount of items: 2
Items: 
Size: 748345 Color: 1
Size: 251530 Color: 2

Bin 2910: 126 of cap free
Amount of items: 2
Items: 
Size: 682273 Color: 2
Size: 317602 Color: 3

Bin 2911: 126 of cap free
Amount of items: 2
Items: 
Size: 636489 Color: 1
Size: 363386 Color: 2

Bin 2912: 126 of cap free
Amount of items: 2
Items: 
Size: 573922 Color: 3
Size: 425953 Color: 2

Bin 2913: 126 of cap free
Amount of items: 2
Items: 
Size: 592950 Color: 4
Size: 406925 Color: 2

Bin 2914: 126 of cap free
Amount of items: 2
Items: 
Size: 690805 Color: 1
Size: 309070 Color: 3

Bin 2915: 127 of cap free
Amount of items: 2
Items: 
Size: 585529 Color: 0
Size: 414345 Color: 4

Bin 2916: 127 of cap free
Amount of items: 2
Items: 
Size: 657423 Color: 4
Size: 342451 Color: 1

Bin 2917: 127 of cap free
Amount of items: 2
Items: 
Size: 759764 Color: 2
Size: 240110 Color: 1

Bin 2918: 127 of cap free
Amount of items: 2
Items: 
Size: 593526 Color: 3
Size: 406348 Color: 2

Bin 2919: 128 of cap free
Amount of items: 3
Items: 
Size: 361685 Color: 0
Size: 361491 Color: 1
Size: 276697 Color: 3

Bin 2920: 128 of cap free
Amount of items: 2
Items: 
Size: 740831 Color: 3
Size: 259042 Color: 1

Bin 2921: 128 of cap free
Amount of items: 2
Items: 
Size: 613010 Color: 0
Size: 386863 Color: 3

Bin 2922: 128 of cap free
Amount of items: 2
Items: 
Size: 541305 Color: 0
Size: 458568 Color: 4

Bin 2923: 128 of cap free
Amount of items: 2
Items: 
Size: 578553 Color: 2
Size: 421320 Color: 0

Bin 2924: 128 of cap free
Amount of items: 2
Items: 
Size: 594790 Color: 0
Size: 405083 Color: 4

Bin 2925: 128 of cap free
Amount of items: 2
Items: 
Size: 692763 Color: 4
Size: 307110 Color: 2

Bin 2926: 128 of cap free
Amount of items: 2
Items: 
Size: 733322 Color: 0
Size: 266551 Color: 3

Bin 2927: 128 of cap free
Amount of items: 2
Items: 
Size: 747761 Color: 1
Size: 252112 Color: 0

Bin 2928: 129 of cap free
Amount of items: 2
Items: 
Size: 627469 Color: 1
Size: 372403 Color: 2

Bin 2929: 129 of cap free
Amount of items: 2
Items: 
Size: 607746 Color: 3
Size: 392126 Color: 2

Bin 2930: 129 of cap free
Amount of items: 2
Items: 
Size: 506754 Color: 3
Size: 493118 Color: 4

Bin 2931: 129 of cap free
Amount of items: 2
Items: 
Size: 553856 Color: 1
Size: 446016 Color: 4

Bin 2932: 129 of cap free
Amount of items: 2
Items: 
Size: 588982 Color: 3
Size: 410890 Color: 0

Bin 2933: 129 of cap free
Amount of items: 2
Items: 
Size: 594237 Color: 1
Size: 405635 Color: 2

Bin 2934: 129 of cap free
Amount of items: 2
Items: 
Size: 662169 Color: 4
Size: 337703 Color: 2

Bin 2935: 129 of cap free
Amount of items: 2
Items: 
Size: 784098 Color: 0
Size: 215774 Color: 4

Bin 2936: 130 of cap free
Amount of items: 2
Items: 
Size: 561531 Color: 0
Size: 438340 Color: 3

Bin 2937: 130 of cap free
Amount of items: 2
Items: 
Size: 579303 Color: 2
Size: 420568 Color: 3

Bin 2938: 130 of cap free
Amount of items: 2
Items: 
Size: 528672 Color: 1
Size: 471199 Color: 0

Bin 2939: 130 of cap free
Amount of items: 2
Items: 
Size: 708398 Color: 3
Size: 291473 Color: 4

Bin 2940: 130 of cap free
Amount of items: 2
Items: 
Size: 529038 Color: 4
Size: 470833 Color: 0

Bin 2941: 130 of cap free
Amount of items: 2
Items: 
Size: 728275 Color: 1
Size: 271596 Color: 0

Bin 2942: 131 of cap free
Amount of items: 2
Items: 
Size: 752294 Color: 4
Size: 247576 Color: 2

Bin 2943: 131 of cap free
Amount of items: 2
Items: 
Size: 789035 Color: 4
Size: 210835 Color: 0

Bin 2944: 131 of cap free
Amount of items: 2
Items: 
Size: 712955 Color: 4
Size: 286915 Color: 2

Bin 2945: 131 of cap free
Amount of items: 2
Items: 
Size: 514080 Color: 3
Size: 485790 Color: 2

Bin 2946: 131 of cap free
Amount of items: 2
Items: 
Size: 525177 Color: 1
Size: 474693 Color: 3

Bin 2947: 131 of cap free
Amount of items: 2
Items: 
Size: 542382 Color: 3
Size: 457488 Color: 4

Bin 2948: 132 of cap free
Amount of items: 2
Items: 
Size: 663126 Color: 2
Size: 336743 Color: 4

Bin 2949: 132 of cap free
Amount of items: 2
Items: 
Size: 743466 Color: 0
Size: 256403 Color: 3

Bin 2950: 132 of cap free
Amount of items: 2
Items: 
Size: 772127 Color: 3
Size: 227742 Color: 0

Bin 2951: 132 of cap free
Amount of items: 2
Items: 
Size: 552880 Color: 0
Size: 446989 Color: 1

Bin 2952: 132 of cap free
Amount of items: 2
Items: 
Size: 567302 Color: 0
Size: 432567 Color: 1

Bin 2953: 132 of cap free
Amount of items: 2
Items: 
Size: 754407 Color: 3
Size: 245462 Color: 4

Bin 2954: 132 of cap free
Amount of items: 2
Items: 
Size: 553972 Color: 4
Size: 445897 Color: 1

Bin 2955: 132 of cap free
Amount of items: 2
Items: 
Size: 539805 Color: 0
Size: 460064 Color: 2

Bin 2956: 132 of cap free
Amount of items: 2
Items: 
Size: 546270 Color: 4
Size: 453599 Color: 0

Bin 2957: 132 of cap free
Amount of items: 2
Items: 
Size: 624090 Color: 1
Size: 375779 Color: 0

Bin 2958: 133 of cap free
Amount of items: 3
Items: 
Size: 660024 Color: 4
Size: 186608 Color: 0
Size: 153236 Color: 1

Bin 2959: 133 of cap free
Amount of items: 2
Items: 
Size: 682631 Color: 0
Size: 317237 Color: 4

Bin 2960: 133 of cap free
Amount of items: 2
Items: 
Size: 646808 Color: 2
Size: 353060 Color: 1

Bin 2961: 133 of cap free
Amount of items: 2
Items: 
Size: 576419 Color: 2
Size: 423449 Color: 3

Bin 2962: 133 of cap free
Amount of items: 2
Items: 
Size: 550035 Color: 3
Size: 449833 Color: 0

Bin 2963: 133 of cap free
Amount of items: 2
Items: 
Size: 563023 Color: 4
Size: 436845 Color: 1

Bin 2964: 133 of cap free
Amount of items: 2
Items: 
Size: 581449 Color: 4
Size: 418419 Color: 3

Bin 2965: 133 of cap free
Amount of items: 2
Items: 
Size: 595340 Color: 4
Size: 404528 Color: 1

Bin 2966: 133 of cap free
Amount of items: 2
Items: 
Size: 597881 Color: 2
Size: 401987 Color: 4

Bin 2967: 133 of cap free
Amount of items: 2
Items: 
Size: 616387 Color: 0
Size: 383481 Color: 2

Bin 2968: 134 of cap free
Amount of items: 2
Items: 
Size: 606222 Color: 3
Size: 393645 Color: 4

Bin 2969: 134 of cap free
Amount of items: 2
Items: 
Size: 760758 Color: 0
Size: 239109 Color: 3

Bin 2970: 135 of cap free
Amount of items: 2
Items: 
Size: 642611 Color: 4
Size: 357255 Color: 3

Bin 2971: 135 of cap free
Amount of items: 2
Items: 
Size: 669981 Color: 4
Size: 329885 Color: 1

Bin 2972: 135 of cap free
Amount of items: 2
Items: 
Size: 505995 Color: 4
Size: 493871 Color: 0

Bin 2973: 135 of cap free
Amount of items: 2
Items: 
Size: 501120 Color: 3
Size: 498746 Color: 0

Bin 2974: 135 of cap free
Amount of items: 2
Items: 
Size: 582424 Color: 0
Size: 417442 Color: 1

Bin 2975: 135 of cap free
Amount of items: 2
Items: 
Size: 515689 Color: 2
Size: 484177 Color: 0

Bin 2976: 136 of cap free
Amount of items: 2
Items: 
Size: 505748 Color: 1
Size: 494117 Color: 0

Bin 2977: 136 of cap free
Amount of items: 2
Items: 
Size: 536568 Color: 4
Size: 463297 Color: 3

Bin 2978: 136 of cap free
Amount of items: 2
Items: 
Size: 707040 Color: 0
Size: 292825 Color: 1

Bin 2979: 136 of cap free
Amount of items: 2
Items: 
Size: 791608 Color: 4
Size: 208257 Color: 2

Bin 2980: 136 of cap free
Amount of items: 2
Items: 
Size: 632042 Color: 4
Size: 367823 Color: 2

Bin 2981: 136 of cap free
Amount of items: 2
Items: 
Size: 576746 Color: 0
Size: 423119 Color: 2

Bin 2982: 136 of cap free
Amount of items: 2
Items: 
Size: 796214 Color: 4
Size: 203651 Color: 3

Bin 2983: 136 of cap free
Amount of items: 2
Items: 
Size: 621549 Color: 4
Size: 378316 Color: 1

Bin 2984: 136 of cap free
Amount of items: 2
Items: 
Size: 525034 Color: 3
Size: 474831 Color: 2

Bin 2985: 136 of cap free
Amount of items: 2
Items: 
Size: 580717 Color: 3
Size: 419148 Color: 4

Bin 2986: 136 of cap free
Amount of items: 2
Items: 
Size: 616507 Color: 0
Size: 383358 Color: 3

Bin 2987: 137 of cap free
Amount of items: 2
Items: 
Size: 764618 Color: 4
Size: 235246 Color: 3

Bin 2988: 137 of cap free
Amount of items: 2
Items: 
Size: 763171 Color: 4
Size: 236693 Color: 2

Bin 2989: 137 of cap free
Amount of items: 2
Items: 
Size: 681538 Color: 0
Size: 318326 Color: 2

Bin 2990: 137 of cap free
Amount of items: 2
Items: 
Size: 691056 Color: 2
Size: 308808 Color: 0

Bin 2991: 137 of cap free
Amount of items: 2
Items: 
Size: 539446 Color: 2
Size: 460418 Color: 1

Bin 2992: 137 of cap free
Amount of items: 2
Items: 
Size: 586451 Color: 3
Size: 413413 Color: 1

Bin 2993: 137 of cap free
Amount of items: 2
Items: 
Size: 631456 Color: 3
Size: 368408 Color: 0

Bin 2994: 137 of cap free
Amount of items: 2
Items: 
Size: 631511 Color: 0
Size: 368353 Color: 1

Bin 2995: 138 of cap free
Amount of items: 2
Items: 
Size: 772748 Color: 4
Size: 227115 Color: 3

Bin 2996: 138 of cap free
Amount of items: 2
Items: 
Size: 518652 Color: 1
Size: 481211 Color: 4

Bin 2997: 138 of cap free
Amount of items: 2
Items: 
Size: 774496 Color: 3
Size: 225367 Color: 2

Bin 2998: 138 of cap free
Amount of items: 2
Items: 
Size: 546834 Color: 0
Size: 453029 Color: 2

Bin 2999: 138 of cap free
Amount of items: 2
Items: 
Size: 533107 Color: 4
Size: 466756 Color: 3

Bin 3000: 138 of cap free
Amount of items: 2
Items: 
Size: 562533 Color: 0
Size: 437330 Color: 2

Bin 3001: 138 of cap free
Amount of items: 2
Items: 
Size: 664087 Color: 2
Size: 335776 Color: 0

Bin 3002: 138 of cap free
Amount of items: 2
Items: 
Size: 785715 Color: 4
Size: 214148 Color: 2

Bin 3003: 139 of cap free
Amount of items: 2
Items: 
Size: 611699 Color: 4
Size: 388163 Color: 0

Bin 3004: 139 of cap free
Amount of items: 2
Items: 
Size: 636132 Color: 1
Size: 363730 Color: 4

Bin 3005: 139 of cap free
Amount of items: 2
Items: 
Size: 663655 Color: 4
Size: 336207 Color: 0

Bin 3006: 139 of cap free
Amount of items: 2
Items: 
Size: 534061 Color: 2
Size: 465801 Color: 1

Bin 3007: 139 of cap free
Amount of items: 2
Items: 
Size: 598384 Color: 3
Size: 401478 Color: 1

Bin 3008: 139 of cap free
Amount of items: 2
Items: 
Size: 673494 Color: 0
Size: 326368 Color: 4

Bin 3009: 139 of cap free
Amount of items: 2
Items: 
Size: 742704 Color: 1
Size: 257158 Color: 3

Bin 3010: 140 of cap free
Amount of items: 2
Items: 
Size: 794303 Color: 2
Size: 205558 Color: 3

Bin 3011: 140 of cap free
Amount of items: 2
Items: 
Size: 644511 Color: 1
Size: 355350 Color: 2

Bin 3012: 140 of cap free
Amount of items: 2
Items: 
Size: 715197 Color: 3
Size: 284664 Color: 1

Bin 3013: 140 of cap free
Amount of items: 2
Items: 
Size: 582756 Color: 4
Size: 417105 Color: 1

Bin 3014: 140 of cap free
Amount of items: 2
Items: 
Size: 602045 Color: 1
Size: 397816 Color: 4

Bin 3015: 140 of cap free
Amount of items: 2
Items: 
Size: 757050 Color: 0
Size: 242811 Color: 4

Bin 3016: 141 of cap free
Amount of items: 2
Items: 
Size: 580496 Color: 2
Size: 419364 Color: 1

Bin 3017: 141 of cap free
Amount of items: 2
Items: 
Size: 664864 Color: 3
Size: 334996 Color: 1

Bin 3018: 141 of cap free
Amount of items: 2
Items: 
Size: 789311 Color: 0
Size: 210549 Color: 3

Bin 3019: 142 of cap free
Amount of items: 2
Items: 
Size: 647578 Color: 4
Size: 352281 Color: 3

Bin 3020: 142 of cap free
Amount of items: 2
Items: 
Size: 613044 Color: 1
Size: 386815 Color: 0

Bin 3021: 142 of cap free
Amount of items: 2
Items: 
Size: 501330 Color: 2
Size: 498529 Color: 0

Bin 3022: 142 of cap free
Amount of items: 2
Items: 
Size: 505375 Color: 3
Size: 494484 Color: 1

Bin 3023: 142 of cap free
Amount of items: 2
Items: 
Size: 553056 Color: 4
Size: 446803 Color: 0

Bin 3024: 142 of cap free
Amount of items: 2
Items: 
Size: 600421 Color: 4
Size: 399438 Color: 1

Bin 3025: 143 of cap free
Amount of items: 2
Items: 
Size: 717066 Color: 3
Size: 282792 Color: 0

Bin 3026: 143 of cap free
Amount of items: 2
Items: 
Size: 715707 Color: 1
Size: 284151 Color: 0

Bin 3027: 143 of cap free
Amount of items: 2
Items: 
Size: 755175 Color: 2
Size: 244683 Color: 0

Bin 3028: 143 of cap free
Amount of items: 2
Items: 
Size: 713852 Color: 4
Size: 286006 Color: 1

Bin 3029: 143 of cap free
Amount of items: 2
Items: 
Size: 689332 Color: 4
Size: 310526 Color: 3

Bin 3030: 143 of cap free
Amount of items: 2
Items: 
Size: 650513 Color: 0
Size: 349345 Color: 3

Bin 3031: 143 of cap free
Amount of items: 2
Items: 
Size: 634828 Color: 2
Size: 365030 Color: 4

Bin 3032: 143 of cap free
Amount of items: 2
Items: 
Size: 579283 Color: 0
Size: 420575 Color: 2

Bin 3033: 143 of cap free
Amount of items: 2
Items: 
Size: 577587 Color: 3
Size: 422271 Color: 1

Bin 3034: 144 of cap free
Amount of items: 2
Items: 
Size: 779261 Color: 0
Size: 220596 Color: 3

Bin 3035: 144 of cap free
Amount of items: 2
Items: 
Size: 619962 Color: 2
Size: 379895 Color: 1

Bin 3036: 144 of cap free
Amount of items: 2
Items: 
Size: 619411 Color: 4
Size: 380446 Color: 1

Bin 3037: 144 of cap free
Amount of items: 2
Items: 
Size: 527382 Color: 2
Size: 472475 Color: 3

Bin 3038: 144 of cap free
Amount of items: 2
Items: 
Size: 566011 Color: 3
Size: 433846 Color: 0

Bin 3039: 144 of cap free
Amount of items: 2
Items: 
Size: 646588 Color: 4
Size: 353269 Color: 0

Bin 3040: 145 of cap free
Amount of items: 2
Items: 
Size: 688900 Color: 1
Size: 310956 Color: 0

Bin 3041: 145 of cap free
Amount of items: 2
Items: 
Size: 659530 Color: 1
Size: 340326 Color: 3

Bin 3042: 145 of cap free
Amount of items: 2
Items: 
Size: 542891 Color: 4
Size: 456965 Color: 3

Bin 3043: 145 of cap free
Amount of items: 2
Items: 
Size: 563982 Color: 3
Size: 435874 Color: 2

Bin 3044: 145 of cap free
Amount of items: 2
Items: 
Size: 605419 Color: 4
Size: 394437 Color: 2

Bin 3045: 146 of cap free
Amount of items: 2
Items: 
Size: 763467 Color: 1
Size: 236388 Color: 2

Bin 3046: 146 of cap free
Amount of items: 2
Items: 
Size: 629579 Color: 2
Size: 370276 Color: 1

Bin 3047: 146 of cap free
Amount of items: 2
Items: 
Size: 610836 Color: 3
Size: 389019 Color: 1

Bin 3048: 146 of cap free
Amount of items: 2
Items: 
Size: 688481 Color: 0
Size: 311374 Color: 3

Bin 3049: 147 of cap free
Amount of items: 3
Items: 
Size: 730091 Color: 1
Size: 135108 Color: 0
Size: 134655 Color: 0

Bin 3050: 147 of cap free
Amount of items: 2
Items: 
Size: 727588 Color: 2
Size: 272266 Color: 0

Bin 3051: 147 of cap free
Amount of items: 2
Items: 
Size: 775100 Color: 0
Size: 224754 Color: 2

Bin 3052: 147 of cap free
Amount of items: 2
Items: 
Size: 672035 Color: 2
Size: 327819 Color: 3

Bin 3053: 147 of cap free
Amount of items: 2
Items: 
Size: 762062 Color: 4
Size: 237792 Color: 3

Bin 3054: 147 of cap free
Amount of items: 2
Items: 
Size: 760928 Color: 1
Size: 238926 Color: 3

Bin 3055: 147 of cap free
Amount of items: 2
Items: 
Size: 651986 Color: 1
Size: 347868 Color: 3

Bin 3056: 147 of cap free
Amount of items: 2
Items: 
Size: 539138 Color: 4
Size: 460716 Color: 0

Bin 3057: 147 of cap free
Amount of items: 2
Items: 
Size: 548436 Color: 0
Size: 451418 Color: 4

Bin 3058: 147 of cap free
Amount of items: 2
Items: 
Size: 645597 Color: 1
Size: 354257 Color: 4

Bin 3059: 148 of cap free
Amount of items: 2
Items: 
Size: 786146 Color: 3
Size: 213707 Color: 4

Bin 3060: 148 of cap free
Amount of items: 2
Items: 
Size: 677183 Color: 2
Size: 322670 Color: 3

Bin 3061: 149 of cap free
Amount of items: 2
Items: 
Size: 778541 Color: 3
Size: 221311 Color: 2

Bin 3062: 149 of cap free
Amount of items: 2
Items: 
Size: 608433 Color: 0
Size: 391419 Color: 4

Bin 3063: 149 of cap free
Amount of items: 2
Items: 
Size: 590742 Color: 0
Size: 409110 Color: 4

Bin 3064: 149 of cap free
Amount of items: 2
Items: 
Size: 728438 Color: 4
Size: 271414 Color: 0

Bin 3065: 150 of cap free
Amount of items: 2
Items: 
Size: 697980 Color: 0
Size: 301871 Color: 2

Bin 3066: 150 of cap free
Amount of items: 2
Items: 
Size: 737337 Color: 4
Size: 262514 Color: 2

Bin 3067: 150 of cap free
Amount of items: 2
Items: 
Size: 781564 Color: 1
Size: 218287 Color: 4

Bin 3068: 150 of cap free
Amount of items: 2
Items: 
Size: 755629 Color: 4
Size: 244222 Color: 2

Bin 3069: 150 of cap free
Amount of items: 2
Items: 
Size: 569909 Color: 3
Size: 429942 Color: 0

Bin 3070: 150 of cap free
Amount of items: 2
Items: 
Size: 625614 Color: 1
Size: 374237 Color: 2

Bin 3071: 150 of cap free
Amount of items: 2
Items: 
Size: 662483 Color: 2
Size: 337368 Color: 1

Bin 3072: 151 of cap free
Amount of items: 3
Items: 
Size: 754259 Color: 2
Size: 129285 Color: 0
Size: 116306 Color: 1

Bin 3073: 151 of cap free
Amount of items: 2
Items: 
Size: 511776 Color: 2
Size: 488074 Color: 0

Bin 3074: 151 of cap free
Amount of items: 2
Items: 
Size: 607969 Color: 2
Size: 391881 Color: 4

Bin 3075: 151 of cap free
Amount of items: 2
Items: 
Size: 781231 Color: 2
Size: 218619 Color: 1

Bin 3076: 151 of cap free
Amount of items: 2
Items: 
Size: 623926 Color: 1
Size: 375924 Color: 2

Bin 3077: 151 of cap free
Amount of items: 2
Items: 
Size: 522939 Color: 0
Size: 476911 Color: 1

Bin 3078: 151 of cap free
Amount of items: 2
Items: 
Size: 593204 Color: 4
Size: 406646 Color: 3

Bin 3079: 151 of cap free
Amount of items: 2
Items: 
Size: 606925 Color: 3
Size: 392925 Color: 0

Bin 3080: 151 of cap free
Amount of items: 2
Items: 
Size: 680730 Color: 0
Size: 319120 Color: 4

Bin 3081: 151 of cap free
Amount of items: 2
Items: 
Size: 747891 Color: 1
Size: 251959 Color: 2

Bin 3082: 151 of cap free
Amount of items: 2
Items: 
Size: 754386 Color: 4
Size: 245464 Color: 3

Bin 3083: 151 of cap free
Amount of items: 2
Items: 
Size: 798497 Color: 0
Size: 201353 Color: 4

Bin 3084: 152 of cap free
Amount of items: 2
Items: 
Size: 512280 Color: 2
Size: 487569 Color: 0

Bin 3085: 152 of cap free
Amount of items: 2
Items: 
Size: 679501 Color: 4
Size: 320348 Color: 0

Bin 3086: 152 of cap free
Amount of items: 2
Items: 
Size: 553295 Color: 1
Size: 446554 Color: 0

Bin 3087: 152 of cap free
Amount of items: 2
Items: 
Size: 563994 Color: 2
Size: 435855 Color: 3

Bin 3088: 153 of cap free
Amount of items: 2
Items: 
Size: 647767 Color: 2
Size: 352081 Color: 4

Bin 3089: 153 of cap free
Amount of items: 2
Items: 
Size: 699261 Color: 2
Size: 300587 Color: 3

Bin 3090: 153 of cap free
Amount of items: 2
Items: 
Size: 520124 Color: 1
Size: 479724 Color: 4

Bin 3091: 153 of cap free
Amount of items: 2
Items: 
Size: 553894 Color: 4
Size: 445954 Color: 1

Bin 3092: 154 of cap free
Amount of items: 2
Items: 
Size: 711951 Color: 2
Size: 287896 Color: 1

Bin 3093: 154 of cap free
Amount of items: 2
Items: 
Size: 625537 Color: 0
Size: 374310 Color: 4

Bin 3094: 154 of cap free
Amount of items: 2
Items: 
Size: 532259 Color: 0
Size: 467588 Color: 1

Bin 3095: 154 of cap free
Amount of items: 2
Items: 
Size: 680724 Color: 3
Size: 319123 Color: 0

Bin 3096: 154 of cap free
Amount of items: 2
Items: 
Size: 621707 Color: 3
Size: 378140 Color: 0

Bin 3097: 155 of cap free
Amount of items: 2
Items: 
Size: 694905 Color: 4
Size: 304941 Color: 3

Bin 3098: 155 of cap free
Amount of items: 2
Items: 
Size: 604528 Color: 2
Size: 395318 Color: 4

Bin 3099: 155 of cap free
Amount of items: 2
Items: 
Size: 719573 Color: 2
Size: 280273 Color: 0

Bin 3100: 155 of cap free
Amount of items: 2
Items: 
Size: 710858 Color: 0
Size: 288988 Color: 2

Bin 3101: 155 of cap free
Amount of items: 2
Items: 
Size: 638525 Color: 0
Size: 361321 Color: 2

Bin 3102: 156 of cap free
Amount of items: 2
Items: 
Size: 550314 Color: 1
Size: 449531 Color: 0

Bin 3103: 156 of cap free
Amount of items: 3
Items: 
Size: 478305 Color: 2
Size: 263923 Color: 4
Size: 257617 Color: 3

Bin 3104: 156 of cap free
Amount of items: 2
Items: 
Size: 641728 Color: 3
Size: 358117 Color: 2

Bin 3105: 156 of cap free
Amount of items: 2
Items: 
Size: 576940 Color: 2
Size: 422905 Color: 3

Bin 3106: 156 of cap free
Amount of items: 2
Items: 
Size: 549863 Color: 3
Size: 449982 Color: 1

Bin 3107: 156 of cap free
Amount of items: 2
Items: 
Size: 649026 Color: 4
Size: 350819 Color: 3

Bin 3108: 157 of cap free
Amount of items: 3
Items: 
Size: 386493 Color: 1
Size: 378612 Color: 0
Size: 234739 Color: 0

Bin 3109: 157 of cap free
Amount of items: 2
Items: 
Size: 645128 Color: 1
Size: 354716 Color: 4

Bin 3110: 157 of cap free
Amount of items: 2
Items: 
Size: 677738 Color: 2
Size: 322106 Color: 1

Bin 3111: 157 of cap free
Amount of items: 2
Items: 
Size: 682812 Color: 0
Size: 317032 Color: 3

Bin 3112: 158 of cap free
Amount of items: 2
Items: 
Size: 740029 Color: 3
Size: 259814 Color: 0

Bin 3113: 158 of cap free
Amount of items: 2
Items: 
Size: 513801 Color: 1
Size: 486042 Color: 4

Bin 3114: 158 of cap free
Amount of items: 2
Items: 
Size: 556829 Color: 3
Size: 443014 Color: 1

Bin 3115: 158 of cap free
Amount of items: 2
Items: 
Size: 580479 Color: 4
Size: 419364 Color: 0

Bin 3116: 158 of cap free
Amount of items: 2
Items: 
Size: 595546 Color: 4
Size: 404297 Color: 1

Bin 3117: 159 of cap free
Amount of items: 3
Items: 
Size: 739633 Color: 2
Size: 143042 Color: 4
Size: 117167 Color: 2

Bin 3118: 159 of cap free
Amount of items: 2
Items: 
Size: 774260 Color: 4
Size: 225582 Color: 3

Bin 3119: 159 of cap free
Amount of items: 2
Items: 
Size: 655977 Color: 0
Size: 343865 Color: 1

Bin 3120: 159 of cap free
Amount of items: 2
Items: 
Size: 673234 Color: 1
Size: 326608 Color: 4

Bin 3121: 159 of cap free
Amount of items: 2
Items: 
Size: 635696 Color: 0
Size: 364146 Color: 2

Bin 3122: 160 of cap free
Amount of items: 2
Items: 
Size: 682654 Color: 3
Size: 317187 Color: 0

Bin 3123: 160 of cap free
Amount of items: 2
Items: 
Size: 654231 Color: 1
Size: 345610 Color: 2

Bin 3124: 160 of cap free
Amount of items: 2
Items: 
Size: 747729 Color: 0
Size: 252112 Color: 1

Bin 3125: 160 of cap free
Amount of items: 2
Items: 
Size: 644089 Color: 0
Size: 355752 Color: 1

Bin 3126: 160 of cap free
Amount of items: 2
Items: 
Size: 736855 Color: 3
Size: 262986 Color: 4

Bin 3127: 160 of cap free
Amount of items: 2
Items: 
Size: 762356 Color: 4
Size: 237485 Color: 3

Bin 3128: 161 of cap free
Amount of items: 2
Items: 
Size: 512629 Color: 3
Size: 487211 Color: 0

Bin 3129: 161 of cap free
Amount of items: 2
Items: 
Size: 687505 Color: 4
Size: 312335 Color: 2

Bin 3130: 161 of cap free
Amount of items: 2
Items: 
Size: 553293 Color: 1
Size: 446547 Color: 4

Bin 3131: 161 of cap free
Amount of items: 2
Items: 
Size: 511404 Color: 4
Size: 488436 Color: 1

Bin 3132: 161 of cap free
Amount of items: 2
Items: 
Size: 531895 Color: 0
Size: 467945 Color: 2

Bin 3133: 162 of cap free
Amount of items: 3
Items: 
Size: 545644 Color: 1
Size: 248699 Color: 4
Size: 205496 Color: 4

Bin 3134: 162 of cap free
Amount of items: 2
Items: 
Size: 719034 Color: 1
Size: 280805 Color: 0

Bin 3135: 162 of cap free
Amount of items: 2
Items: 
Size: 775669 Color: 3
Size: 224170 Color: 4

Bin 3136: 162 of cap free
Amount of items: 2
Items: 
Size: 646787 Color: 4
Size: 353052 Color: 2

Bin 3137: 162 of cap free
Amount of items: 2
Items: 
Size: 500074 Color: 4
Size: 499765 Color: 1

Bin 3138: 162 of cap free
Amount of items: 2
Items: 
Size: 553059 Color: 1
Size: 446780 Color: 0

Bin 3139: 162 of cap free
Amount of items: 2
Items: 
Size: 565647 Color: 0
Size: 434192 Color: 1

Bin 3140: 163 of cap free
Amount of items: 3
Items: 
Size: 633097 Color: 1
Size: 198624 Color: 4
Size: 168117 Color: 4

Bin 3141: 163 of cap free
Amount of items: 2
Items: 
Size: 590423 Color: 2
Size: 409415 Color: 4

Bin 3142: 163 of cap free
Amount of items: 2
Items: 
Size: 775249 Color: 3
Size: 224589 Color: 2

Bin 3143: 163 of cap free
Amount of items: 2
Items: 
Size: 593971 Color: 1
Size: 405867 Color: 0

Bin 3144: 164 of cap free
Amount of items: 2
Items: 
Size: 768618 Color: 2
Size: 231219 Color: 4

Bin 3145: 164 of cap free
Amount of items: 2
Items: 
Size: 534737 Color: 3
Size: 465100 Color: 1

Bin 3146: 164 of cap free
Amount of items: 2
Items: 
Size: 503783 Color: 1
Size: 496054 Color: 3

Bin 3147: 165 of cap free
Amount of items: 2
Items: 
Size: 703773 Color: 4
Size: 296063 Color: 3

Bin 3148: 165 of cap free
Amount of items: 2
Items: 
Size: 733454 Color: 0
Size: 266382 Color: 3

Bin 3149: 165 of cap free
Amount of items: 2
Items: 
Size: 609535 Color: 3
Size: 390301 Color: 4

Bin 3150: 165 of cap free
Amount of items: 2
Items: 
Size: 575517 Color: 0
Size: 424319 Color: 3

Bin 3151: 165 of cap free
Amount of items: 2
Items: 
Size: 636641 Color: 0
Size: 363195 Color: 3

Bin 3152: 165 of cap free
Amount of items: 2
Items: 
Size: 618128 Color: 4
Size: 381708 Color: 3

Bin 3153: 166 of cap free
Amount of items: 2
Items: 
Size: 751985 Color: 3
Size: 247850 Color: 4

Bin 3154: 166 of cap free
Amount of items: 2
Items: 
Size: 595005 Color: 3
Size: 404830 Color: 2

Bin 3155: 166 of cap free
Amount of items: 2
Items: 
Size: 729103 Color: 2
Size: 270732 Color: 4

Bin 3156: 167 of cap free
Amount of items: 2
Items: 
Size: 611494 Color: 4
Size: 388340 Color: 2

Bin 3157: 167 of cap free
Amount of items: 2
Items: 
Size: 606237 Color: 4
Size: 393597 Color: 3

Bin 3158: 167 of cap free
Amount of items: 2
Items: 
Size: 709706 Color: 0
Size: 290128 Color: 2

Bin 3159: 167 of cap free
Amount of items: 2
Items: 
Size: 789915 Color: 2
Size: 209919 Color: 4

Bin 3160: 168 of cap free
Amount of items: 3
Items: 
Size: 515875 Color: 4
Size: 302700 Color: 4
Size: 181258 Color: 1

Bin 3161: 168 of cap free
Amount of items: 3
Items: 
Size: 780953 Color: 3
Size: 114229 Color: 2
Size: 104651 Color: 0

Bin 3162: 168 of cap free
Amount of items: 2
Items: 
Size: 505741 Color: 1
Size: 494092 Color: 0

Bin 3163: 168 of cap free
Amount of items: 2
Items: 
Size: 639460 Color: 0
Size: 360373 Color: 2

Bin 3164: 168 of cap free
Amount of items: 2
Items: 
Size: 624086 Color: 1
Size: 375747 Color: 4

Bin 3165: 169 of cap free
Amount of items: 2
Items: 
Size: 753245 Color: 1
Size: 246587 Color: 3

Bin 3166: 169 of cap free
Amount of items: 2
Items: 
Size: 517918 Color: 2
Size: 481914 Color: 3

Bin 3167: 169 of cap free
Amount of items: 2
Items: 
Size: 771944 Color: 3
Size: 227888 Color: 1

Bin 3168: 170 of cap free
Amount of items: 2
Items: 
Size: 771705 Color: 2
Size: 228126 Color: 3

Bin 3169: 170 of cap free
Amount of items: 2
Items: 
Size: 640733 Color: 1
Size: 359098 Color: 2

Bin 3170: 170 of cap free
Amount of items: 2
Items: 
Size: 750864 Color: 3
Size: 248967 Color: 0

Bin 3171: 170 of cap free
Amount of items: 2
Items: 
Size: 526978 Color: 1
Size: 472853 Color: 4

Bin 3172: 170 of cap free
Amount of items: 2
Items: 
Size: 634314 Color: 4
Size: 365517 Color: 0

Bin 3173: 171 of cap free
Amount of items: 2
Items: 
Size: 629261 Color: 1
Size: 370569 Color: 0

Bin 3174: 171 of cap free
Amount of items: 2
Items: 
Size: 738566 Color: 4
Size: 261264 Color: 2

Bin 3175: 171 of cap free
Amount of items: 2
Items: 
Size: 630403 Color: 2
Size: 369427 Color: 0

Bin 3176: 171 of cap free
Amount of items: 2
Items: 
Size: 533192 Color: 3
Size: 466638 Color: 4

Bin 3177: 171 of cap free
Amount of items: 2
Items: 
Size: 528855 Color: 1
Size: 470975 Color: 4

Bin 3178: 171 of cap free
Amount of items: 2
Items: 
Size: 533541 Color: 1
Size: 466289 Color: 2

Bin 3179: 171 of cap free
Amount of items: 2
Items: 
Size: 580946 Color: 0
Size: 418884 Color: 3

Bin 3180: 171 of cap free
Amount of items: 2
Items: 
Size: 661000 Color: 0
Size: 338830 Color: 1

Bin 3181: 171 of cap free
Amount of items: 2
Items: 
Size: 670835 Color: 4
Size: 328995 Color: 2

Bin 3182: 172 of cap free
Amount of items: 2
Items: 
Size: 690247 Color: 0
Size: 309582 Color: 2

Bin 3183: 172 of cap free
Amount of items: 2
Items: 
Size: 709687 Color: 4
Size: 290142 Color: 0

Bin 3184: 172 of cap free
Amount of items: 2
Items: 
Size: 717059 Color: 3
Size: 282770 Color: 2

Bin 3185: 172 of cap free
Amount of items: 2
Items: 
Size: 686659 Color: 2
Size: 313170 Color: 3

Bin 3186: 172 of cap free
Amount of items: 2
Items: 
Size: 704228 Color: 1
Size: 295601 Color: 4

Bin 3187: 172 of cap free
Amount of items: 2
Items: 
Size: 651379 Color: 0
Size: 348450 Color: 3

Bin 3188: 172 of cap free
Amount of items: 2
Items: 
Size: 615807 Color: 4
Size: 384022 Color: 2

Bin 3189: 172 of cap free
Amount of items: 2
Items: 
Size: 561028 Color: 4
Size: 438801 Color: 1

Bin 3190: 172 of cap free
Amount of items: 2
Items: 
Size: 583299 Color: 2
Size: 416530 Color: 4

Bin 3191: 173 of cap free
Amount of items: 2
Items: 
Size: 723505 Color: 0
Size: 276323 Color: 2

Bin 3192: 173 of cap free
Amount of items: 2
Items: 
Size: 587500 Color: 4
Size: 412328 Color: 2

Bin 3193: 173 of cap free
Amount of items: 2
Items: 
Size: 573542 Color: 4
Size: 426286 Color: 2

Bin 3194: 173 of cap free
Amount of items: 2
Items: 
Size: 677765 Color: 4
Size: 322063 Color: 0

Bin 3195: 174 of cap free
Amount of items: 2
Items: 
Size: 702779 Color: 3
Size: 297048 Color: 0

Bin 3196: 174 of cap free
Amount of items: 2
Items: 
Size: 777822 Color: 3
Size: 222005 Color: 4

Bin 3197: 175 of cap free
Amount of items: 2
Items: 
Size: 754970 Color: 0
Size: 244856 Color: 4

Bin 3198: 175 of cap free
Amount of items: 2
Items: 
Size: 669589 Color: 3
Size: 330237 Color: 2

Bin 3199: 175 of cap free
Amount of items: 2
Items: 
Size: 548718 Color: 4
Size: 451108 Color: 2

Bin 3200: 175 of cap free
Amount of items: 2
Items: 
Size: 652415 Color: 4
Size: 347411 Color: 2

Bin 3201: 176 of cap free
Amount of items: 2
Items: 
Size: 566290 Color: 3
Size: 433535 Color: 4

Bin 3202: 176 of cap free
Amount of items: 2
Items: 
Size: 504290 Color: 2
Size: 495535 Color: 4

Bin 3203: 176 of cap free
Amount of items: 2
Items: 
Size: 665190 Color: 2
Size: 334635 Color: 4

Bin 3204: 177 of cap free
Amount of items: 2
Items: 
Size: 735117 Color: 4
Size: 264707 Color: 1

Bin 3205: 177 of cap free
Amount of items: 2
Items: 
Size: 654985 Color: 2
Size: 344839 Color: 0

Bin 3206: 177 of cap free
Amount of items: 2
Items: 
Size: 596544 Color: 0
Size: 403280 Color: 2

Bin 3207: 178 of cap free
Amount of items: 2
Items: 
Size: 650061 Color: 1
Size: 349762 Color: 0

Bin 3208: 178 of cap free
Amount of items: 2
Items: 
Size: 582162 Color: 4
Size: 417661 Color: 3

Bin 3209: 178 of cap free
Amount of items: 2
Items: 
Size: 783235 Color: 0
Size: 216588 Color: 3

Bin 3210: 179 of cap free
Amount of items: 2
Items: 
Size: 635169 Color: 3
Size: 364653 Color: 0

Bin 3211: 179 of cap free
Amount of items: 2
Items: 
Size: 573284 Color: 0
Size: 426538 Color: 4

Bin 3212: 179 of cap free
Amount of items: 2
Items: 
Size: 576424 Color: 3
Size: 423398 Color: 1

Bin 3213: 179 of cap free
Amount of items: 2
Items: 
Size: 684615 Color: 1
Size: 315207 Color: 2

Bin 3214: 180 of cap free
Amount of items: 3
Items: 
Size: 479000 Color: 2
Size: 265485 Color: 1
Size: 255336 Color: 2

Bin 3215: 180 of cap free
Amount of items: 2
Items: 
Size: 522379 Color: 0
Size: 477442 Color: 4

Bin 3216: 180 of cap free
Amount of items: 2
Items: 
Size: 730719 Color: 2
Size: 269102 Color: 0

Bin 3217: 180 of cap free
Amount of items: 2
Items: 
Size: 574178 Color: 4
Size: 425643 Color: 1

Bin 3218: 180 of cap free
Amount of items: 2
Items: 
Size: 691917 Color: 0
Size: 307904 Color: 3

Bin 3219: 181 of cap free
Amount of items: 2
Items: 
Size: 703615 Color: 0
Size: 296205 Color: 4

Bin 3220: 181 of cap free
Amount of items: 2
Items: 
Size: 782616 Color: 2
Size: 217204 Color: 1

Bin 3221: 181 of cap free
Amount of items: 2
Items: 
Size: 609576 Color: 4
Size: 390244 Color: 0

Bin 3222: 181 of cap free
Amount of items: 2
Items: 
Size: 512903 Color: 2
Size: 486917 Color: 1

Bin 3223: 181 of cap free
Amount of items: 2
Items: 
Size: 628312 Color: 1
Size: 371508 Color: 0

Bin 3224: 181 of cap free
Amount of items: 2
Items: 
Size: 693824 Color: 3
Size: 305996 Color: 4

Bin 3225: 182 of cap free
Amount of items: 2
Items: 
Size: 658430 Color: 3
Size: 341389 Color: 4

Bin 3226: 182 of cap free
Amount of items: 2
Items: 
Size: 649751 Color: 1
Size: 350068 Color: 0

Bin 3227: 182 of cap free
Amount of items: 2
Items: 
Size: 571265 Color: 0
Size: 428554 Color: 2

Bin 3228: 182 of cap free
Amount of items: 2
Items: 
Size: 539343 Color: 1
Size: 460476 Color: 0

Bin 3229: 182 of cap free
Amount of items: 2
Items: 
Size: 687384 Color: 2
Size: 312435 Color: 4

Bin 3230: 182 of cap free
Amount of items: 2
Items: 
Size: 591465 Color: 0
Size: 408354 Color: 3

Bin 3231: 183 of cap free
Amount of items: 2
Items: 
Size: 633678 Color: 0
Size: 366140 Color: 4

Bin 3232: 183 of cap free
Amount of items: 2
Items: 
Size: 505124 Color: 0
Size: 494694 Color: 1

Bin 3233: 183 of cap free
Amount of items: 2
Items: 
Size: 794685 Color: 2
Size: 205133 Color: 1

Bin 3234: 183 of cap free
Amount of items: 2
Items: 
Size: 797295 Color: 2
Size: 202523 Color: 0

Bin 3235: 184 of cap free
Amount of items: 2
Items: 
Size: 548051 Color: 2
Size: 451766 Color: 0

Bin 3236: 184 of cap free
Amount of items: 2
Items: 
Size: 551365 Color: 4
Size: 448452 Color: 2

Bin 3237: 184 of cap free
Amount of items: 2
Items: 
Size: 506545 Color: 3
Size: 493272 Color: 1

Bin 3238: 184 of cap free
Amount of items: 2
Items: 
Size: 512265 Color: 1
Size: 487552 Color: 3

Bin 3239: 184 of cap free
Amount of items: 2
Items: 
Size: 586822 Color: 2
Size: 412995 Color: 3

Bin 3240: 185 of cap free
Amount of items: 2
Items: 
Size: 568910 Color: 0
Size: 430906 Color: 1

Bin 3241: 185 of cap free
Amount of items: 2
Items: 
Size: 593223 Color: 4
Size: 406593 Color: 0

Bin 3242: 185 of cap free
Amount of items: 2
Items: 
Size: 587825 Color: 1
Size: 411991 Color: 3

Bin 3243: 185 of cap free
Amount of items: 2
Items: 
Size: 674473 Color: 2
Size: 325343 Color: 0

Bin 3244: 185 of cap free
Amount of items: 2
Items: 
Size: 653834 Color: 0
Size: 345982 Color: 3

Bin 3245: 186 of cap free
Amount of items: 2
Items: 
Size: 792172 Color: 1
Size: 207643 Color: 2

Bin 3246: 186 of cap free
Amount of items: 2
Items: 
Size: 595067 Color: 2
Size: 404748 Color: 1

Bin 3247: 186 of cap free
Amount of items: 2
Items: 
Size: 597833 Color: 0
Size: 401982 Color: 1

Bin 3248: 186 of cap free
Amount of items: 2
Items: 
Size: 598609 Color: 4
Size: 401206 Color: 2

Bin 3249: 186 of cap free
Amount of items: 2
Items: 
Size: 789910 Color: 4
Size: 209905 Color: 2

Bin 3250: 187 of cap free
Amount of items: 2
Items: 
Size: 714266 Color: 3
Size: 285548 Color: 0

Bin 3251: 187 of cap free
Amount of items: 2
Items: 
Size: 701309 Color: 3
Size: 298505 Color: 1

Bin 3252: 188 of cap free
Amount of items: 2
Items: 
Size: 520497 Color: 3
Size: 479316 Color: 1

Bin 3253: 188 of cap free
Amount of items: 2
Items: 
Size: 574837 Color: 4
Size: 424976 Color: 3

Bin 3254: 188 of cap free
Amount of items: 2
Items: 
Size: 617281 Color: 0
Size: 382532 Color: 4

Bin 3255: 189 of cap free
Amount of items: 2
Items: 
Size: 769468 Color: 4
Size: 230344 Color: 3

Bin 3256: 189 of cap free
Amount of items: 2
Items: 
Size: 585029 Color: 3
Size: 414783 Color: 2

Bin 3257: 189 of cap free
Amount of items: 2
Items: 
Size: 609876 Color: 2
Size: 389936 Color: 3

Bin 3258: 189 of cap free
Amount of items: 2
Items: 
Size: 673204 Color: 4
Size: 326608 Color: 3

Bin 3259: 190 of cap free
Amount of items: 2
Items: 
Size: 714961 Color: 1
Size: 284850 Color: 2

Bin 3260: 190 of cap free
Amount of items: 2
Items: 
Size: 663951 Color: 3
Size: 335860 Color: 2

Bin 3261: 190 of cap free
Amount of items: 2
Items: 
Size: 618802 Color: 1
Size: 381009 Color: 4

Bin 3262: 192 of cap free
Amount of items: 2
Items: 
Size: 632289 Color: 4
Size: 367520 Color: 1

Bin 3263: 192 of cap free
Amount of items: 2
Items: 
Size: 712919 Color: 2
Size: 286890 Color: 0

Bin 3264: 192 of cap free
Amount of items: 2
Items: 
Size: 774865 Color: 3
Size: 224944 Color: 0

Bin 3265: 192 of cap free
Amount of items: 2
Items: 
Size: 774252 Color: 4
Size: 225557 Color: 1

Bin 3266: 192 of cap free
Amount of items: 2
Items: 
Size: 796588 Color: 4
Size: 203221 Color: 1

Bin 3267: 192 of cap free
Amount of items: 2
Items: 
Size: 559871 Color: 2
Size: 439938 Color: 3

Bin 3268: 192 of cap free
Amount of items: 2
Items: 
Size: 612686 Color: 0
Size: 387123 Color: 4

Bin 3269: 193 of cap free
Amount of items: 2
Items: 
Size: 716734 Color: 0
Size: 283074 Color: 1

Bin 3270: 193 of cap free
Amount of items: 2
Items: 
Size: 614932 Color: 4
Size: 384876 Color: 0

Bin 3271: 193 of cap free
Amount of items: 2
Items: 
Size: 760895 Color: 4
Size: 238913 Color: 1

Bin 3272: 194 of cap free
Amount of items: 2
Items: 
Size: 655462 Color: 4
Size: 344345 Color: 2

Bin 3273: 194 of cap free
Amount of items: 2
Items: 
Size: 738169 Color: 1
Size: 261638 Color: 2

Bin 3274: 194 of cap free
Amount of items: 2
Items: 
Size: 508717 Color: 0
Size: 491090 Color: 2

Bin 3275: 194 of cap free
Amount of items: 2
Items: 
Size: 721679 Color: 1
Size: 278128 Color: 3

Bin 3276: 195 of cap free
Amount of items: 2
Items: 
Size: 768405 Color: 1
Size: 231401 Color: 2

Bin 3277: 196 of cap free
Amount of items: 2
Items: 
Size: 725878 Color: 3
Size: 273927 Color: 4

Bin 3278: 196 of cap free
Amount of items: 2
Items: 
Size: 563101 Color: 1
Size: 436704 Color: 0

Bin 3279: 196 of cap free
Amount of items: 2
Items: 
Size: 525981 Color: 0
Size: 473824 Color: 3

Bin 3280: 196 of cap free
Amount of items: 2
Items: 
Size: 571493 Color: 1
Size: 428312 Color: 4

Bin 3281: 196 of cap free
Amount of items: 2
Items: 
Size: 583226 Color: 0
Size: 416579 Color: 1

Bin 3282: 197 of cap free
Amount of items: 2
Items: 
Size: 602003 Color: 0
Size: 397801 Color: 2

Bin 3283: 197 of cap free
Amount of items: 2
Items: 
Size: 756563 Color: 1
Size: 243241 Color: 2

Bin 3284: 197 of cap free
Amount of items: 2
Items: 
Size: 700465 Color: 3
Size: 299339 Color: 1

Bin 3285: 197 of cap free
Amount of items: 2
Items: 
Size: 749654 Color: 1
Size: 250150 Color: 2

Bin 3286: 197 of cap free
Amount of items: 2
Items: 
Size: 598703 Color: 0
Size: 401101 Color: 1

Bin 3287: 197 of cap free
Amount of items: 2
Items: 
Size: 620708 Color: 3
Size: 379096 Color: 1

Bin 3288: 198 of cap free
Amount of items: 2
Items: 
Size: 768011 Color: 4
Size: 231792 Color: 3

Bin 3289: 198 of cap free
Amount of items: 2
Items: 
Size: 744316 Color: 0
Size: 255487 Color: 1

Bin 3290: 198 of cap free
Amount of items: 2
Items: 
Size: 698914 Color: 2
Size: 300889 Color: 3

Bin 3291: 198 of cap free
Amount of items: 2
Items: 
Size: 635065 Color: 1
Size: 364738 Color: 2

Bin 3292: 198 of cap free
Amount of items: 2
Items: 
Size: 673880 Color: 4
Size: 325923 Color: 0

Bin 3293: 198 of cap free
Amount of items: 2
Items: 
Size: 737957 Color: 2
Size: 261846 Color: 0

Bin 3294: 199 of cap free
Amount of items: 2
Items: 
Size: 619902 Color: 1
Size: 379900 Color: 2

Bin 3295: 199 of cap free
Amount of items: 2
Items: 
Size: 508495 Color: 4
Size: 491307 Color: 1

Bin 3296: 199 of cap free
Amount of items: 2
Items: 
Size: 543042 Color: 1
Size: 456760 Color: 0

Bin 3297: 199 of cap free
Amount of items: 2
Items: 
Size: 605417 Color: 4
Size: 394385 Color: 1

Bin 3298: 200 of cap free
Amount of items: 2
Items: 
Size: 676765 Color: 3
Size: 323036 Color: 2

Bin 3299: 200 of cap free
Amount of items: 2
Items: 
Size: 672939 Color: 2
Size: 326862 Color: 0

Bin 3300: 200 of cap free
Amount of items: 2
Items: 
Size: 686988 Color: 0
Size: 312813 Color: 2

Bin 3301: 201 of cap free
Amount of items: 2
Items: 
Size: 749175 Color: 2
Size: 250625 Color: 0

Bin 3302: 201 of cap free
Amount of items: 2
Items: 
Size: 502117 Color: 2
Size: 497683 Color: 4

Bin 3303: 201 of cap free
Amount of items: 2
Items: 
Size: 579689 Color: 2
Size: 420111 Color: 4

Bin 3304: 201 of cap free
Amount of items: 2
Items: 
Size: 656334 Color: 4
Size: 343466 Color: 1

Bin 3305: 201 of cap free
Amount of items: 2
Items: 
Size: 656621 Color: 0
Size: 343179 Color: 2

Bin 3306: 202 of cap free
Amount of items: 2
Items: 
Size: 507144 Color: 3
Size: 492655 Color: 4

Bin 3307: 202 of cap free
Amount of items: 2
Items: 
Size: 534679 Color: 1
Size: 465120 Color: 3

Bin 3308: 202 of cap free
Amount of items: 2
Items: 
Size: 542519 Color: 2
Size: 457280 Color: 4

Bin 3309: 202 of cap free
Amount of items: 2
Items: 
Size: 597822 Color: 4
Size: 401977 Color: 3

Bin 3310: 203 of cap free
Amount of items: 2
Items: 
Size: 766796 Color: 0
Size: 233002 Color: 3

Bin 3311: 203 of cap free
Amount of items: 2
Items: 
Size: 703557 Color: 1
Size: 296241 Color: 0

Bin 3312: 203 of cap free
Amount of items: 2
Items: 
Size: 605619 Color: 3
Size: 394179 Color: 0

Bin 3313: 203 of cap free
Amount of items: 2
Items: 
Size: 547550 Color: 4
Size: 452248 Color: 0

Bin 3314: 204 of cap free
Amount of items: 2
Items: 
Size: 516106 Color: 3
Size: 483691 Color: 0

Bin 3315: 204 of cap free
Amount of items: 2
Items: 
Size: 765978 Color: 2
Size: 233819 Color: 3

Bin 3316: 204 of cap free
Amount of items: 2
Items: 
Size: 754614 Color: 1
Size: 245183 Color: 3

Bin 3317: 204 of cap free
Amount of items: 2
Items: 
Size: 586016 Color: 1
Size: 413781 Color: 2

Bin 3318: 204 of cap free
Amount of items: 2
Items: 
Size: 758604 Color: 4
Size: 241193 Color: 3

Bin 3319: 204 of cap free
Amount of items: 2
Items: 
Size: 757077 Color: 4
Size: 242720 Color: 3

Bin 3320: 205 of cap free
Amount of items: 2
Items: 
Size: 739948 Color: 3
Size: 259848 Color: 4

Bin 3321: 205 of cap free
Amount of items: 2
Items: 
Size: 618823 Color: 4
Size: 380973 Color: 0

Bin 3322: 205 of cap free
Amount of items: 2
Items: 
Size: 744006 Color: 3
Size: 255790 Color: 4

Bin 3323: 205 of cap free
Amount of items: 2
Items: 
Size: 550343 Color: 0
Size: 449453 Color: 1

Bin 3324: 205 of cap free
Amount of items: 2
Items: 
Size: 519565 Color: 0
Size: 480231 Color: 2

Bin 3325: 207 of cap free
Amount of items: 2
Items: 
Size: 659120 Color: 2
Size: 340674 Color: 4

Bin 3326: 207 of cap free
Amount of items: 2
Items: 
Size: 745297 Color: 1
Size: 254497 Color: 2

Bin 3327: 208 of cap free
Amount of items: 2
Items: 
Size: 664324 Color: 4
Size: 335469 Color: 2

Bin 3328: 209 of cap free
Amount of items: 2
Items: 
Size: 794262 Color: 3
Size: 205530 Color: 0

Bin 3329: 209 of cap free
Amount of items: 2
Items: 
Size: 777241 Color: 3
Size: 222551 Color: 1

Bin 3330: 210 of cap free
Amount of items: 2
Items: 
Size: 632145 Color: 2
Size: 367646 Color: 4

Bin 3331: 210 of cap free
Amount of items: 2
Items: 
Size: 779665 Color: 4
Size: 220126 Color: 1

Bin 3332: 211 of cap free
Amount of items: 2
Items: 
Size: 754819 Color: 3
Size: 244971 Color: 0

Bin 3333: 211 of cap free
Amount of items: 2
Items: 
Size: 693425 Color: 4
Size: 306365 Color: 0

Bin 3334: 211 of cap free
Amount of items: 2
Items: 
Size: 611907 Color: 3
Size: 387883 Color: 2

Bin 3335: 211 of cap free
Amount of items: 2
Items: 
Size: 520286 Color: 0
Size: 479504 Color: 3

Bin 3336: 211 of cap free
Amount of items: 2
Items: 
Size: 529841 Color: 1
Size: 469949 Color: 3

Bin 3337: 211 of cap free
Amount of items: 2
Items: 
Size: 687935 Color: 4
Size: 311855 Color: 1

Bin 3338: 211 of cap free
Amount of items: 2
Items: 
Size: 739367 Color: 3
Size: 260423 Color: 0

Bin 3339: 212 of cap free
Amount of items: 3
Items: 
Size: 699787 Color: 4
Size: 151128 Color: 3
Size: 148874 Color: 4

Bin 3340: 212 of cap free
Amount of items: 3
Items: 
Size: 719483 Color: 4
Size: 148090 Color: 2
Size: 132216 Color: 0

Bin 3341: 212 of cap free
Amount of items: 2
Items: 
Size: 778135 Color: 4
Size: 221654 Color: 1

Bin 3342: 212 of cap free
Amount of items: 2
Items: 
Size: 576682 Color: 4
Size: 423107 Color: 3

Bin 3343: 213 of cap free
Amount of items: 2
Items: 
Size: 688388 Color: 4
Size: 311400 Color: 1

Bin 3344: 213 of cap free
Amount of items: 2
Items: 
Size: 650462 Color: 3
Size: 349326 Color: 1

Bin 3345: 214 of cap free
Amount of items: 2
Items: 
Size: 511720 Color: 4
Size: 488067 Color: 3

Bin 3346: 215 of cap free
Amount of items: 2
Items: 
Size: 679496 Color: 3
Size: 320290 Color: 1

Bin 3347: 215 of cap free
Amount of items: 2
Items: 
Size: 502540 Color: 3
Size: 497246 Color: 4

Bin 3348: 216 of cap free
Amount of items: 2
Items: 
Size: 511740 Color: 0
Size: 488045 Color: 3

Bin 3349: 216 of cap free
Amount of items: 2
Items: 
Size: 750123 Color: 1
Size: 249662 Color: 4

Bin 3350: 216 of cap free
Amount of items: 2
Items: 
Size: 600132 Color: 4
Size: 399653 Color: 0

Bin 3351: 217 of cap free
Amount of items: 2
Items: 
Size: 725862 Color: 0
Size: 273922 Color: 1

Bin 3352: 217 of cap free
Amount of items: 2
Items: 
Size: 797535 Color: 4
Size: 202249 Color: 3

Bin 3353: 217 of cap free
Amount of items: 2
Items: 
Size: 785343 Color: 1
Size: 214441 Color: 3

Bin 3354: 217 of cap free
Amount of items: 2
Items: 
Size: 569441 Color: 2
Size: 430343 Color: 1

Bin 3355: 217 of cap free
Amount of items: 2
Items: 
Size: 570734 Color: 0
Size: 429050 Color: 3

Bin 3356: 217 of cap free
Amount of items: 2
Items: 
Size: 665425 Color: 2
Size: 334359 Color: 3

Bin 3357: 217 of cap free
Amount of items: 2
Items: 
Size: 720610 Color: 2
Size: 279174 Color: 1

Bin 3358: 218 of cap free
Amount of items: 2
Items: 
Size: 744200 Color: 4
Size: 255583 Color: 2

Bin 3359: 218 of cap free
Amount of items: 2
Items: 
Size: 591034 Color: 1
Size: 408749 Color: 2

Bin 3360: 219 of cap free
Amount of items: 2
Items: 
Size: 791180 Color: 1
Size: 208602 Color: 2

Bin 3361: 219 of cap free
Amount of items: 2
Items: 
Size: 571882 Color: 1
Size: 427900 Color: 3

Bin 3362: 219 of cap free
Amount of items: 2
Items: 
Size: 553058 Color: 1
Size: 446724 Color: 2

Bin 3363: 219 of cap free
Amount of items: 2
Items: 
Size: 565475 Color: 4
Size: 434307 Color: 3

Bin 3364: 220 of cap free
Amount of items: 2
Items: 
Size: 737652 Color: 3
Size: 262129 Color: 2

Bin 3365: 220 of cap free
Amount of items: 2
Items: 
Size: 651907 Color: 1
Size: 347874 Color: 3

Bin 3366: 220 of cap free
Amount of items: 2
Items: 
Size: 563569 Color: 2
Size: 436212 Color: 4

Bin 3367: 220 of cap free
Amount of items: 2
Items: 
Size: 758594 Color: 3
Size: 241187 Color: 1

Bin 3368: 221 of cap free
Amount of items: 2
Items: 
Size: 757531 Color: 1
Size: 242249 Color: 0

Bin 3369: 221 of cap free
Amount of items: 2
Items: 
Size: 503031 Color: 2
Size: 496749 Color: 4

Bin 3370: 222 of cap free
Amount of items: 2
Items: 
Size: 772140 Color: 0
Size: 227639 Color: 3

Bin 3371: 222 of cap free
Amount of items: 2
Items: 
Size: 653096 Color: 0
Size: 346683 Color: 4

Bin 3372: 222 of cap free
Amount of items: 2
Items: 
Size: 569124 Color: 0
Size: 430655 Color: 2

Bin 3373: 222 of cap free
Amount of items: 2
Items: 
Size: 575069 Color: 1
Size: 424710 Color: 4

Bin 3374: 223 of cap free
Amount of items: 2
Items: 
Size: 759734 Color: 0
Size: 240044 Color: 2

Bin 3375: 224 of cap free
Amount of items: 3
Items: 
Size: 505740 Color: 1
Size: 247461 Color: 2
Size: 246576 Color: 3

Bin 3376: 224 of cap free
Amount of items: 2
Items: 
Size: 683672 Color: 4
Size: 316105 Color: 3

Bin 3377: 225 of cap free
Amount of items: 2
Items: 
Size: 676920 Color: 0
Size: 322856 Color: 1

Bin 3378: 225 of cap free
Amount of items: 2
Items: 
Size: 661509 Color: 2
Size: 338267 Color: 0

Bin 3379: 225 of cap free
Amount of items: 2
Items: 
Size: 704638 Color: 1
Size: 295138 Color: 0

Bin 3380: 226 of cap free
Amount of items: 2
Items: 
Size: 586444 Color: 1
Size: 413331 Color: 3

Bin 3381: 226 of cap free
Amount of items: 2
Items: 
Size: 555159 Color: 0
Size: 444616 Color: 1

Bin 3382: 226 of cap free
Amount of items: 2
Items: 
Size: 566661 Color: 2
Size: 433114 Color: 3

Bin 3383: 226 of cap free
Amount of items: 2
Items: 
Size: 577587 Color: 0
Size: 422188 Color: 3

Bin 3384: 227 of cap free
Amount of items: 2
Items: 
Size: 698893 Color: 0
Size: 300881 Color: 1

Bin 3385: 227 of cap free
Amount of items: 2
Items: 
Size: 548394 Color: 1
Size: 451380 Color: 3

Bin 3386: 227 of cap free
Amount of items: 2
Items: 
Size: 555984 Color: 0
Size: 443790 Color: 1

Bin 3387: 228 of cap free
Amount of items: 2
Items: 
Size: 642572 Color: 2
Size: 357201 Color: 3

Bin 3388: 228 of cap free
Amount of items: 2
Items: 
Size: 570734 Color: 0
Size: 429039 Color: 2

Bin 3389: 229 of cap free
Amount of items: 2
Items: 
Size: 623568 Color: 4
Size: 376204 Color: 1

Bin 3390: 229 of cap free
Amount of items: 2
Items: 
Size: 784948 Color: 4
Size: 214824 Color: 3

Bin 3391: 230 of cap free
Amount of items: 2
Items: 
Size: 710022 Color: 4
Size: 289749 Color: 2

Bin 3392: 230 of cap free
Amount of items: 2
Items: 
Size: 688415 Color: 4
Size: 311356 Color: 2

Bin 3393: 231 of cap free
Amount of items: 2
Items: 
Size: 630387 Color: 3
Size: 369383 Color: 2

Bin 3394: 232 of cap free
Amount of items: 2
Items: 
Size: 622717 Color: 0
Size: 377052 Color: 3

Bin 3395: 233 of cap free
Amount of items: 2
Items: 
Size: 788359 Color: 4
Size: 211409 Color: 0

Bin 3396: 233 of cap free
Amount of items: 2
Items: 
Size: 531809 Color: 2
Size: 467959 Color: 0

Bin 3397: 234 of cap free
Amount of items: 2
Items: 
Size: 577949 Color: 1
Size: 421818 Color: 3

Bin 3398: 234 of cap free
Amount of items: 2
Items: 
Size: 793927 Color: 4
Size: 205840 Color: 1

Bin 3399: 234 of cap free
Amount of items: 2
Items: 
Size: 567388 Color: 1
Size: 432379 Color: 2

Bin 3400: 235 of cap free
Amount of items: 2
Items: 
Size: 545358 Color: 1
Size: 454408 Color: 3

Bin 3401: 235 of cap free
Amount of items: 2
Items: 
Size: 721614 Color: 3
Size: 278152 Color: 2

Bin 3402: 236 of cap free
Amount of items: 2
Items: 
Size: 538382 Color: 0
Size: 461383 Color: 1

Bin 3403: 236 of cap free
Amount of items: 2
Items: 
Size: 781154 Color: 3
Size: 218611 Color: 1

Bin 3404: 236 of cap free
Amount of items: 2
Items: 
Size: 531084 Color: 1
Size: 468681 Color: 3

Bin 3405: 237 of cap free
Amount of items: 2
Items: 
Size: 674444 Color: 1
Size: 325320 Color: 0

Bin 3406: 237 of cap free
Amount of items: 2
Items: 
Size: 557097 Color: 4
Size: 442667 Color: 2

Bin 3407: 237 of cap free
Amount of items: 2
Items: 
Size: 537129 Color: 3
Size: 462635 Color: 4

Bin 3408: 238 of cap free
Amount of items: 2
Items: 
Size: 686193 Color: 2
Size: 313570 Color: 0

Bin 3409: 238 of cap free
Amount of items: 2
Items: 
Size: 723478 Color: 4
Size: 276285 Color: 3

Bin 3410: 239 of cap free
Amount of items: 3
Items: 
Size: 720953 Color: 2
Size: 160326 Color: 0
Size: 118483 Color: 2

Bin 3411: 239 of cap free
Amount of items: 2
Items: 
Size: 787821 Color: 3
Size: 211941 Color: 2

Bin 3412: 239 of cap free
Amount of items: 2
Items: 
Size: 673861 Color: 3
Size: 325901 Color: 2

Bin 3413: 239 of cap free
Amount of items: 2
Items: 
Size: 505693 Color: 0
Size: 494069 Color: 1

Bin 3414: 240 of cap free
Amount of items: 2
Items: 
Size: 650042 Color: 3
Size: 349719 Color: 0

Bin 3415: 240 of cap free
Amount of items: 2
Items: 
Size: 536193 Color: 3
Size: 463568 Color: 4

Bin 3416: 240 of cap free
Amount of items: 2
Items: 
Size: 583990 Color: 3
Size: 415771 Color: 0

Bin 3417: 241 of cap free
Amount of items: 2
Items: 
Size: 691858 Color: 2
Size: 307902 Color: 3

Bin 3418: 241 of cap free
Amount of items: 2
Items: 
Size: 590684 Color: 4
Size: 409076 Color: 0

Bin 3419: 241 of cap free
Amount of items: 2
Items: 
Size: 652207 Color: 4
Size: 347553 Color: 1

Bin 3420: 241 of cap free
Amount of items: 2
Items: 
Size: 772961 Color: 2
Size: 226799 Color: 0

Bin 3421: 242 of cap free
Amount of items: 2
Items: 
Size: 504739 Color: 3
Size: 495020 Color: 2

Bin 3422: 243 of cap free
Amount of items: 2
Items: 
Size: 700306 Color: 1
Size: 299452 Color: 2

Bin 3423: 243 of cap free
Amount of items: 2
Items: 
Size: 635402 Color: 1
Size: 364356 Color: 3

Bin 3424: 243 of cap free
Amount of items: 2
Items: 
Size: 645122 Color: 0
Size: 354636 Color: 4

Bin 3425: 243 of cap free
Amount of items: 2
Items: 
Size: 569110 Color: 2
Size: 430648 Color: 0

Bin 3426: 244 of cap free
Amount of items: 2
Items: 
Size: 776450 Color: 2
Size: 223307 Color: 0

Bin 3427: 245 of cap free
Amount of items: 2
Items: 
Size: 783354 Color: 3
Size: 216402 Color: 1

Bin 3428: 246 of cap free
Amount of items: 2
Items: 
Size: 645939 Color: 1
Size: 353816 Color: 2

Bin 3429: 246 of cap free
Amount of items: 2
Items: 
Size: 771619 Color: 4
Size: 228136 Color: 2

Bin 3430: 246 of cap free
Amount of items: 2
Items: 
Size: 561599 Color: 3
Size: 438156 Color: 1

Bin 3431: 248 of cap free
Amount of items: 2
Items: 
Size: 684162 Color: 4
Size: 315591 Color: 1

Bin 3432: 248 of cap free
Amount of items: 2
Items: 
Size: 734595 Color: 2
Size: 265158 Color: 1

Bin 3433: 248 of cap free
Amount of items: 2
Items: 
Size: 799526 Color: 0
Size: 200227 Color: 3

Bin 3434: 248 of cap free
Amount of items: 2
Items: 
Size: 503708 Color: 1
Size: 496045 Color: 3

Bin 3435: 249 of cap free
Amount of items: 2
Items: 
Size: 657257 Color: 0
Size: 342495 Color: 4

Bin 3436: 249 of cap free
Amount of items: 2
Items: 
Size: 724150 Color: 1
Size: 275602 Color: 2

Bin 3437: 250 of cap free
Amount of items: 2
Items: 
Size: 510149 Color: 0
Size: 489602 Color: 2

Bin 3438: 252 of cap free
Amount of items: 2
Items: 
Size: 672775 Color: 0
Size: 326974 Color: 2

Bin 3439: 252 of cap free
Amount of items: 2
Items: 
Size: 512875 Color: 3
Size: 486874 Color: 4

Bin 3440: 252 of cap free
Amount of items: 2
Items: 
Size: 618077 Color: 4
Size: 381672 Color: 1

Bin 3441: 253 of cap free
Amount of items: 2
Items: 
Size: 761318 Color: 0
Size: 238430 Color: 4

Bin 3442: 253 of cap free
Amount of items: 2
Items: 
Size: 555970 Color: 0
Size: 443778 Color: 4

Bin 3443: 253 of cap free
Amount of items: 2
Items: 
Size: 566664 Color: 0
Size: 433084 Color: 1

Bin 3444: 253 of cap free
Amount of items: 2
Items: 
Size: 700956 Color: 4
Size: 298792 Color: 3

Bin 3445: 255 of cap free
Amount of items: 2
Items: 
Size: 626308 Color: 1
Size: 373438 Color: 3

Bin 3446: 256 of cap free
Amount of items: 2
Items: 
Size: 511136 Color: 0
Size: 488609 Color: 3

Bin 3447: 257 of cap free
Amount of items: 2
Items: 
Size: 635645 Color: 3
Size: 364099 Color: 4

Bin 3448: 258 of cap free
Amount of items: 3
Items: 
Size: 676715 Color: 0
Size: 169424 Color: 0
Size: 153604 Color: 1

Bin 3449: 258 of cap free
Amount of items: 2
Items: 
Size: 791033 Color: 0
Size: 208710 Color: 2

Bin 3450: 258 of cap free
Amount of items: 2
Items: 
Size: 623549 Color: 3
Size: 376194 Color: 2

Bin 3451: 258 of cap free
Amount of items: 2
Items: 
Size: 740316 Color: 1
Size: 259427 Color: 0

Bin 3452: 259 of cap free
Amount of items: 2
Items: 
Size: 706363 Color: 1
Size: 293379 Color: 2

Bin 3453: 259 of cap free
Amount of items: 2
Items: 
Size: 547095 Color: 4
Size: 452647 Color: 3

Bin 3454: 259 of cap free
Amount of items: 2
Items: 
Size: 528047 Color: 0
Size: 471695 Color: 1

Bin 3455: 259 of cap free
Amount of items: 2
Items: 
Size: 598057 Color: 3
Size: 401685 Color: 4

Bin 3456: 259 of cap free
Amount of items: 2
Items: 
Size: 600305 Color: 0
Size: 399437 Color: 3

Bin 3457: 260 of cap free
Amount of items: 2
Items: 
Size: 535646 Color: 3
Size: 464095 Color: 4

Bin 3458: 260 of cap free
Amount of items: 2
Items: 
Size: 667257 Color: 3
Size: 332484 Color: 2

Bin 3459: 261 of cap free
Amount of items: 2
Items: 
Size: 770056 Color: 2
Size: 229684 Color: 3

Bin 3460: 261 of cap free
Amount of items: 2
Items: 
Size: 611018 Color: 1
Size: 388722 Color: 2

Bin 3461: 261 of cap free
Amount of items: 2
Items: 
Size: 660514 Color: 1
Size: 339226 Color: 3

Bin 3462: 261 of cap free
Amount of items: 2
Items: 
Size: 693758 Color: 0
Size: 305982 Color: 1

Bin 3463: 262 of cap free
Amount of items: 2
Items: 
Size: 501000 Color: 0
Size: 498739 Color: 2

Bin 3464: 262 of cap free
Amount of items: 2
Items: 
Size: 695394 Color: 1
Size: 304345 Color: 2

Bin 3465: 262 of cap free
Amount of items: 2
Items: 
Size: 667161 Color: 0
Size: 332578 Color: 3

Bin 3466: 262 of cap free
Amount of items: 2
Items: 
Size: 560335 Color: 3
Size: 439404 Color: 2

Bin 3467: 263 of cap free
Amount of items: 2
Items: 
Size: 704963 Color: 3
Size: 294775 Color: 2

Bin 3468: 263 of cap free
Amount of items: 2
Items: 
Size: 748654 Color: 3
Size: 251084 Color: 0

Bin 3469: 263 of cap free
Amount of items: 2
Items: 
Size: 564593 Color: 1
Size: 435145 Color: 4

Bin 3470: 264 of cap free
Amount of items: 2
Items: 
Size: 542777 Color: 4
Size: 456960 Color: 0

Bin 3471: 265 of cap free
Amount of items: 2
Items: 
Size: 721396 Color: 0
Size: 278340 Color: 3

Bin 3472: 266 of cap free
Amount of items: 2
Items: 
Size: 777904 Color: 4
Size: 221831 Color: 2

Bin 3473: 266 of cap free
Amount of items: 2
Items: 
Size: 662787 Color: 1
Size: 336948 Color: 4

Bin 3474: 266 of cap free
Amount of items: 2
Items: 
Size: 579550 Color: 0
Size: 420185 Color: 2

Bin 3475: 267 of cap free
Amount of items: 2
Items: 
Size: 519608 Color: 1
Size: 480126 Color: 0

Bin 3476: 268 of cap free
Amount of items: 2
Items: 
Size: 592927 Color: 3
Size: 406806 Color: 2

Bin 3477: 268 of cap free
Amount of items: 2
Items: 
Size: 552237 Color: 2
Size: 447496 Color: 3

Bin 3478: 268 of cap free
Amount of items: 2
Items: 
Size: 536485 Color: 2
Size: 463248 Color: 3

Bin 3479: 269 of cap free
Amount of items: 2
Items: 
Size: 742068 Color: 2
Size: 257664 Color: 3

Bin 3480: 269 of cap free
Amount of items: 2
Items: 
Size: 541003 Color: 4
Size: 458729 Color: 1

Bin 3481: 270 of cap free
Amount of items: 2
Items: 
Size: 788912 Color: 2
Size: 210819 Color: 4

Bin 3482: 270 of cap free
Amount of items: 2
Items: 
Size: 731466 Color: 3
Size: 268265 Color: 4

Bin 3483: 271 of cap free
Amount of items: 2
Items: 
Size: 500375 Color: 0
Size: 499355 Color: 2

Bin 3484: 271 of cap free
Amount of items: 2
Items: 
Size: 697667 Color: 4
Size: 302063 Color: 2

Bin 3485: 271 of cap free
Amount of items: 2
Items: 
Size: 601221 Color: 4
Size: 398509 Color: 0

Bin 3486: 272 of cap free
Amount of items: 2
Items: 
Size: 556301 Color: 2
Size: 443428 Color: 4

Bin 3487: 273 of cap free
Amount of items: 3
Items: 
Size: 431995 Color: 1
Size: 314641 Color: 0
Size: 253092 Color: 3

Bin 3488: 273 of cap free
Amount of items: 2
Items: 
Size: 717003 Color: 4
Size: 282725 Color: 1

Bin 3489: 273 of cap free
Amount of items: 2
Items: 
Size: 644386 Color: 4
Size: 355342 Color: 3

Bin 3490: 273 of cap free
Amount of items: 2
Items: 
Size: 513730 Color: 2
Size: 485998 Color: 4

Bin 3491: 274 of cap free
Amount of items: 2
Items: 
Size: 699746 Color: 0
Size: 299981 Color: 2

Bin 3492: 274 of cap free
Amount of items: 2
Items: 
Size: 714152 Color: 2
Size: 285575 Color: 3

Bin 3493: 274 of cap free
Amount of items: 2
Items: 
Size: 532556 Color: 2
Size: 467171 Color: 4

Bin 3494: 274 of cap free
Amount of items: 2
Items: 
Size: 663332 Color: 1
Size: 336395 Color: 2

Bin 3495: 275 of cap free
Amount of items: 3
Items: 
Size: 371822 Color: 4
Size: 370939 Color: 1
Size: 256965 Color: 1

Bin 3496: 275 of cap free
Amount of items: 3
Items: 
Size: 789801 Color: 1
Size: 105136 Color: 3
Size: 104789 Color: 2

Bin 3497: 276 of cap free
Amount of items: 2
Items: 
Size: 564598 Color: 4
Size: 435127 Color: 2

Bin 3498: 276 of cap free
Amount of items: 2
Items: 
Size: 568390 Color: 2
Size: 431335 Color: 1

Bin 3499: 277 of cap free
Amount of items: 2
Items: 
Size: 567044 Color: 4
Size: 432680 Color: 0

Bin 3500: 277 of cap free
Amount of items: 2
Items: 
Size: 775588 Color: 2
Size: 224136 Color: 1

Bin 3501: 277 of cap free
Amount of items: 2
Items: 
Size: 668480 Color: 0
Size: 331244 Color: 4

Bin 3502: 278 of cap free
Amount of items: 2
Items: 
Size: 574755 Color: 2
Size: 424968 Color: 3

Bin 3503: 278 of cap free
Amount of items: 2
Items: 
Size: 592148 Color: 0
Size: 407575 Color: 4

Bin 3504: 279 of cap free
Amount of items: 2
Items: 
Size: 557634 Color: 2
Size: 442088 Color: 3

Bin 3505: 279 of cap free
Amount of items: 2
Items: 
Size: 781783 Color: 1
Size: 217939 Color: 2

Bin 3506: 280 of cap free
Amount of items: 2
Items: 
Size: 555604 Color: 3
Size: 444117 Color: 2

Bin 3507: 281 of cap free
Amount of items: 3
Items: 
Size: 692133 Color: 4
Size: 156527 Color: 3
Size: 151060 Color: 3

Bin 3508: 281 of cap free
Amount of items: 2
Items: 
Size: 658423 Color: 1
Size: 341297 Color: 4

Bin 3509: 281 of cap free
Amount of items: 2
Items: 
Size: 523167 Color: 0
Size: 476553 Color: 2

Bin 3510: 281 of cap free
Amount of items: 2
Items: 
Size: 755579 Color: 1
Size: 244141 Color: 0

Bin 3511: 281 of cap free
Amount of items: 2
Items: 
Size: 563017 Color: 3
Size: 436703 Color: 4

Bin 3512: 281 of cap free
Amount of items: 2
Items: 
Size: 666622 Color: 0
Size: 333098 Color: 1

Bin 3513: 281 of cap free
Amount of items: 2
Items: 
Size: 619298 Color: 2
Size: 380422 Color: 3

Bin 3514: 282 of cap free
Amount of items: 2
Items: 
Size: 661596 Color: 0
Size: 338123 Color: 2

Bin 3515: 282 of cap free
Amount of items: 2
Items: 
Size: 582085 Color: 0
Size: 417634 Color: 4

Bin 3516: 283 of cap free
Amount of items: 2
Items: 
Size: 589384 Color: 3
Size: 410334 Color: 2

Bin 3517: 283 of cap free
Amount of items: 2
Items: 
Size: 797187 Color: 0
Size: 202531 Color: 2

Bin 3518: 284 of cap free
Amount of items: 3
Items: 
Size: 534402 Color: 4
Size: 238817 Color: 2
Size: 226498 Color: 2

Bin 3519: 284 of cap free
Amount of items: 2
Items: 
Size: 757880 Color: 3
Size: 241837 Color: 1

Bin 3520: 284 of cap free
Amount of items: 2
Items: 
Size: 787974 Color: 3
Size: 211743 Color: 1

Bin 3521: 284 of cap free
Amount of items: 2
Items: 
Size: 519207 Color: 0
Size: 480510 Color: 1

Bin 3522: 284 of cap free
Amount of items: 2
Items: 
Size: 554179 Color: 0
Size: 445538 Color: 2

Bin 3523: 285 of cap free
Amount of items: 2
Items: 
Size: 693398 Color: 4
Size: 306318 Color: 0

Bin 3524: 285 of cap free
Amount of items: 2
Items: 
Size: 704602 Color: 2
Size: 295114 Color: 0

Bin 3525: 285 of cap free
Amount of items: 2
Items: 
Size: 795963 Color: 1
Size: 203753 Color: 4

Bin 3526: 285 of cap free
Amount of items: 2
Items: 
Size: 562473 Color: 3
Size: 437243 Color: 1

Bin 3527: 286 of cap free
Amount of items: 2
Items: 
Size: 612270 Color: 1
Size: 387445 Color: 3

Bin 3528: 286 of cap free
Amount of items: 2
Items: 
Size: 732952 Color: 0
Size: 266763 Color: 1

Bin 3529: 286 of cap free
Amount of items: 2
Items: 
Size: 500454 Color: 4
Size: 499261 Color: 3

Bin 3530: 286 of cap free
Amount of items: 2
Items: 
Size: 505048 Color: 3
Size: 494667 Color: 4

Bin 3531: 287 of cap free
Amount of items: 2
Items: 
Size: 683670 Color: 3
Size: 316044 Color: 2

Bin 3532: 287 of cap free
Amount of items: 2
Items: 
Size: 673169 Color: 0
Size: 326545 Color: 3

Bin 3533: 290 of cap free
Amount of items: 2
Items: 
Size: 540900 Color: 0
Size: 458811 Color: 1

Bin 3534: 290 of cap free
Amount of items: 2
Items: 
Size: 653436 Color: 1
Size: 346275 Color: 4

Bin 3535: 291 of cap free
Amount of items: 2
Items: 
Size: 731439 Color: 1
Size: 268271 Color: 3

Bin 3536: 291 of cap free
Amount of items: 2
Items: 
Size: 555949 Color: 0
Size: 443761 Color: 1

Bin 3537: 291 of cap free
Amount of items: 2
Items: 
Size: 610250 Color: 0
Size: 389460 Color: 4

Bin 3538: 292 of cap free
Amount of items: 2
Items: 
Size: 659071 Color: 4
Size: 340638 Color: 0

Bin 3539: 294 of cap free
Amount of items: 2
Items: 
Size: 613220 Color: 0
Size: 386487 Color: 1

Bin 3540: 294 of cap free
Amount of items: 2
Items: 
Size: 613780 Color: 1
Size: 385927 Color: 2

Bin 3541: 294 of cap free
Amount of items: 2
Items: 
Size: 780215 Color: 1
Size: 219492 Color: 3

Bin 3542: 294 of cap free
Amount of items: 2
Items: 
Size: 557088 Color: 1
Size: 442619 Color: 4

Bin 3543: 295 of cap free
Amount of items: 2
Items: 
Size: 564575 Color: 1
Size: 435131 Color: 4

Bin 3544: 296 of cap free
Amount of items: 2
Items: 
Size: 637262 Color: 4
Size: 362443 Color: 3

Bin 3545: 296 of cap free
Amount of items: 2
Items: 
Size: 681977 Color: 3
Size: 317728 Color: 2

Bin 3546: 296 of cap free
Amount of items: 2
Items: 
Size: 575480 Color: 3
Size: 424225 Color: 4

Bin 3547: 297 of cap free
Amount of items: 2
Items: 
Size: 750860 Color: 3
Size: 248844 Color: 1

Bin 3548: 297 of cap free
Amount of items: 2
Items: 
Size: 570671 Color: 0
Size: 429033 Color: 1

Bin 3549: 298 of cap free
Amount of items: 2
Items: 
Size: 521189 Color: 3
Size: 478514 Color: 2

Bin 3550: 298 of cap free
Amount of items: 2
Items: 
Size: 501211 Color: 3
Size: 498492 Color: 4

Bin 3551: 298 of cap free
Amount of items: 2
Items: 
Size: 587483 Color: 3
Size: 412220 Color: 2

Bin 3552: 300 of cap free
Amount of items: 2
Items: 
Size: 677428 Color: 3
Size: 322273 Color: 1

Bin 3553: 300 of cap free
Amount of items: 2
Items: 
Size: 679731 Color: 1
Size: 319970 Color: 0

Bin 3554: 300 of cap free
Amount of items: 2
Items: 
Size: 573594 Color: 1
Size: 426107 Color: 2

Bin 3555: 301 of cap free
Amount of items: 2
Items: 
Size: 684843 Color: 4
Size: 314857 Color: 2

Bin 3556: 302 of cap free
Amount of items: 2
Items: 
Size: 777613 Color: 3
Size: 222086 Color: 1

Bin 3557: 303 of cap free
Amount of items: 2
Items: 
Size: 728262 Color: 1
Size: 271436 Color: 4

Bin 3558: 303 of cap free
Amount of items: 2
Items: 
Size: 512881 Color: 3
Size: 486817 Color: 0

Bin 3559: 304 of cap free
Amount of items: 2
Items: 
Size: 521181 Color: 2
Size: 478516 Color: 3

Bin 3560: 304 of cap free
Amount of items: 2
Items: 
Size: 639704 Color: 1
Size: 359993 Color: 0

Bin 3561: 305 of cap free
Amount of items: 2
Items: 
Size: 510473 Color: 4
Size: 489223 Color: 3

Bin 3562: 305 of cap free
Amount of items: 2
Items: 
Size: 599901 Color: 3
Size: 399795 Color: 2

Bin 3563: 306 of cap free
Amount of items: 2
Items: 
Size: 677174 Color: 0
Size: 322521 Color: 3

Bin 3564: 306 of cap free
Amount of items: 2
Items: 
Size: 508149 Color: 2
Size: 491546 Color: 1

Bin 3565: 308 of cap free
Amount of items: 2
Items: 
Size: 646470 Color: 4
Size: 353223 Color: 3

Bin 3566: 308 of cap free
Amount of items: 2
Items: 
Size: 687373 Color: 0
Size: 312320 Color: 1

Bin 3567: 308 of cap free
Amount of items: 2
Items: 
Size: 592514 Color: 2
Size: 407179 Color: 1

Bin 3568: 309 of cap free
Amount of items: 2
Items: 
Size: 660898 Color: 1
Size: 338794 Color: 4

Bin 3569: 309 of cap free
Amount of items: 2
Items: 
Size: 537349 Color: 4
Size: 462343 Color: 1

Bin 3570: 310 of cap free
Amount of items: 2
Items: 
Size: 504276 Color: 3
Size: 495415 Color: 2

Bin 3571: 310 of cap free
Amount of items: 2
Items: 
Size: 779593 Color: 2
Size: 220098 Color: 0

Bin 3572: 311 of cap free
Amount of items: 2
Items: 
Size: 603363 Color: 3
Size: 396327 Color: 4

Bin 3573: 312 of cap free
Amount of items: 2
Items: 
Size: 562399 Color: 4
Size: 437290 Color: 3

Bin 3574: 312 of cap free
Amount of items: 2
Items: 
Size: 512237 Color: 4
Size: 487452 Color: 0

Bin 3575: 312 of cap free
Amount of items: 2
Items: 
Size: 664698 Color: 0
Size: 334991 Color: 1

Bin 3576: 314 of cap free
Amount of items: 2
Items: 
Size: 793837 Color: 4
Size: 205850 Color: 3

Bin 3577: 314 of cap free
Amount of items: 2
Items: 
Size: 702726 Color: 0
Size: 296961 Color: 3

Bin 3578: 314 of cap free
Amount of items: 2
Items: 
Size: 696225 Color: 3
Size: 303462 Color: 2

Bin 3579: 314 of cap free
Amount of items: 2
Items: 
Size: 529539 Color: 2
Size: 470148 Color: 1

Bin 3580: 314 of cap free
Amount of items: 2
Items: 
Size: 585979 Color: 1
Size: 413708 Color: 0

Bin 3581: 314 of cap free
Amount of items: 2
Items: 
Size: 634683 Color: 3
Size: 365004 Color: 2

Bin 3582: 314 of cap free
Amount of items: 2
Items: 
Size: 669086 Color: 3
Size: 330601 Color: 1

Bin 3583: 315 of cap free
Amount of items: 2
Items: 
Size: 686166 Color: 0
Size: 313520 Color: 3

Bin 3584: 315 of cap free
Amount of items: 2
Items: 
Size: 713363 Color: 0
Size: 286323 Color: 3

Bin 3585: 315 of cap free
Amount of items: 2
Items: 
Size: 554686 Color: 4
Size: 445000 Color: 2

Bin 3586: 316 of cap free
Amount of items: 2
Items: 
Size: 654126 Color: 3
Size: 345559 Color: 0

Bin 3587: 316 of cap free
Amount of items: 2
Items: 
Size: 720246 Color: 0
Size: 279439 Color: 2

Bin 3588: 317 of cap free
Amount of items: 4
Items: 
Size: 355215 Color: 0
Size: 293373 Color: 1
Size: 175570 Color: 1
Size: 175526 Color: 3

Bin 3589: 319 of cap free
Amount of items: 2
Items: 
Size: 533998 Color: 2
Size: 465684 Color: 0

Bin 3590: 320 of cap free
Amount of items: 2
Items: 
Size: 748823 Color: 1
Size: 250858 Color: 3

Bin 3591: 320 of cap free
Amount of items: 2
Items: 
Size: 608782 Color: 3
Size: 390899 Color: 2

Bin 3592: 320 of cap free
Amount of items: 2
Items: 
Size: 783927 Color: 3
Size: 215754 Color: 4

Bin 3593: 320 of cap free
Amount of items: 2
Items: 
Size: 549945 Color: 1
Size: 449736 Color: 0

Bin 3594: 320 of cap free
Amount of items: 2
Items: 
Size: 514344 Color: 3
Size: 485337 Color: 1

Bin 3595: 320 of cap free
Amount of items: 2
Items: 
Size: 554155 Color: 0
Size: 445526 Color: 1

Bin 3596: 321 of cap free
Amount of items: 2
Items: 
Size: 779590 Color: 1
Size: 220090 Color: 0

Bin 3597: 322 of cap free
Amount of items: 2
Items: 
Size: 535665 Color: 4
Size: 464014 Color: 0

Bin 3598: 322 of cap free
Amount of items: 2
Items: 
Size: 720950 Color: 2
Size: 278729 Color: 4

Bin 3599: 323 of cap free
Amount of items: 2
Items: 
Size: 787494 Color: 2
Size: 212184 Color: 4

Bin 3600: 323 of cap free
Amount of items: 2
Items: 
Size: 566645 Color: 0
Size: 433033 Color: 2

Bin 3601: 323 of cap free
Amount of items: 2
Items: 
Size: 555068 Color: 3
Size: 444610 Color: 1

Bin 3602: 323 of cap free
Amount of items: 2
Items: 
Size: 695330 Color: 1
Size: 304348 Color: 3

Bin 3603: 325 of cap free
Amount of items: 2
Items: 
Size: 731457 Color: 3
Size: 268219 Color: 4

Bin 3604: 326 of cap free
Amount of items: 2
Items: 
Size: 647099 Color: 4
Size: 352576 Color: 0

Bin 3605: 326 of cap free
Amount of items: 2
Items: 
Size: 575941 Color: 3
Size: 423734 Color: 4

Bin 3606: 327 of cap free
Amount of items: 2
Items: 
Size: 784564 Color: 2
Size: 215110 Color: 0

Bin 3607: 328 of cap free
Amount of items: 2
Items: 
Size: 707577 Color: 0
Size: 292096 Color: 4

Bin 3608: 328 of cap free
Amount of items: 2
Items: 
Size: 633574 Color: 4
Size: 366099 Color: 3

Bin 3609: 329 of cap free
Amount of items: 2
Items: 
Size: 543043 Color: 1
Size: 456629 Color: 3

Bin 3610: 330 of cap free
Amount of items: 2
Items: 
Size: 546653 Color: 2
Size: 453018 Color: 0

Bin 3611: 330 of cap free
Amount of items: 2
Items: 
Size: 687365 Color: 4
Size: 312306 Color: 1

Bin 3612: 331 of cap free
Amount of items: 2
Items: 
Size: 787598 Color: 1
Size: 212072 Color: 3

Bin 3613: 331 of cap free
Amount of items: 2
Items: 
Size: 681520 Color: 3
Size: 318150 Color: 4

Bin 3614: 332 of cap free
Amount of items: 2
Items: 
Size: 715730 Color: 4
Size: 283939 Color: 2

Bin 3615: 333 of cap free
Amount of items: 2
Items: 
Size: 503075 Color: 4
Size: 496593 Color: 0

Bin 3616: 334 of cap free
Amount of items: 2
Items: 
Size: 708309 Color: 0
Size: 291358 Color: 2

Bin 3617: 334 of cap free
Amount of items: 2
Items: 
Size: 509227 Color: 3
Size: 490440 Color: 1

Bin 3618: 335 of cap free
Amount of items: 2
Items: 
Size: 595484 Color: 4
Size: 404182 Color: 3

Bin 3619: 335 of cap free
Amount of items: 2
Items: 
Size: 789753 Color: 1
Size: 209913 Color: 4

Bin 3620: 337 of cap free
Amount of items: 2
Items: 
Size: 570654 Color: 2
Size: 429010 Color: 4

Bin 3621: 337 of cap free
Amount of items: 2
Items: 
Size: 525441 Color: 4
Size: 474223 Color: 2

Bin 3622: 337 of cap free
Amount of items: 2
Items: 
Size: 500010 Color: 1
Size: 499654 Color: 4

Bin 3623: 338 of cap free
Amount of items: 2
Items: 
Size: 614898 Color: 4
Size: 384765 Color: 1

Bin 3624: 338 of cap free
Amount of items: 2
Items: 
Size: 663925 Color: 2
Size: 335738 Color: 4

Bin 3625: 338 of cap free
Amount of items: 2
Items: 
Size: 558961 Color: 2
Size: 440702 Color: 4

Bin 3626: 339 of cap free
Amount of items: 2
Items: 
Size: 769588 Color: 3
Size: 230074 Color: 2

Bin 3627: 341 of cap free
Amount of items: 2
Items: 
Size: 705729 Color: 4
Size: 293931 Color: 2

Bin 3628: 342 of cap free
Amount of items: 2
Items: 
Size: 722630 Color: 1
Size: 277029 Color: 3

Bin 3629: 343 of cap free
Amount of items: 2
Items: 
Size: 599959 Color: 0
Size: 399699 Color: 4

Bin 3630: 343 of cap free
Amount of items: 2
Items: 
Size: 627389 Color: 1
Size: 372269 Color: 0

Bin 3631: 344 of cap free
Amount of items: 2
Items: 
Size: 755938 Color: 0
Size: 243719 Color: 3

Bin 3632: 344 of cap free
Amount of items: 2
Items: 
Size: 669070 Color: 1
Size: 330587 Color: 3

Bin 3633: 344 of cap free
Amount of items: 2
Items: 
Size: 559747 Color: 3
Size: 439910 Color: 4

Bin 3634: 344 of cap free
Amount of items: 2
Items: 
Size: 596599 Color: 3
Size: 403058 Color: 0

Bin 3635: 345 of cap free
Amount of items: 2
Items: 
Size: 792821 Color: 3
Size: 206835 Color: 0

Bin 3636: 346 of cap free
Amount of items: 2
Items: 
Size: 686156 Color: 0
Size: 313499 Color: 2

Bin 3637: 346 of cap free
Amount of items: 2
Items: 
Size: 506168 Color: 4
Size: 493487 Color: 1

Bin 3638: 347 of cap free
Amount of items: 2
Items: 
Size: 683010 Color: 4
Size: 316644 Color: 1

Bin 3639: 348 of cap free
Amount of items: 2
Items: 
Size: 691359 Color: 0
Size: 308294 Color: 4

Bin 3640: 348 of cap free
Amount of items: 2
Items: 
Size: 693711 Color: 4
Size: 305942 Color: 0

Bin 3641: 349 of cap free
Amount of items: 2
Items: 
Size: 775083 Color: 4
Size: 224569 Color: 2

Bin 3642: 349 of cap free
Amount of items: 2
Items: 
Size: 500401 Color: 2
Size: 499251 Color: 0

Bin 3643: 350 of cap free
Amount of items: 2
Items: 
Size: 582597 Color: 4
Size: 417054 Color: 2

Bin 3644: 350 of cap free
Amount of items: 2
Items: 
Size: 544876 Color: 2
Size: 454775 Color: 1

Bin 3645: 350 of cap free
Amount of items: 2
Items: 
Size: 690241 Color: 3
Size: 309410 Color: 2

Bin 3646: 351 of cap free
Amount of items: 2
Items: 
Size: 757083 Color: 4
Size: 242567 Color: 1

Bin 3647: 351 of cap free
Amount of items: 2
Items: 
Size: 516078 Color: 2
Size: 483572 Color: 3

Bin 3648: 352 of cap free
Amount of items: 3
Items: 
Size: 758594 Color: 0
Size: 138269 Color: 3
Size: 102786 Color: 2

Bin 3649: 352 of cap free
Amount of items: 2
Items: 
Size: 556290 Color: 1
Size: 443359 Color: 4

Bin 3650: 353 of cap free
Amount of items: 2
Items: 
Size: 508117 Color: 4
Size: 491531 Color: 3

Bin 3651: 353 of cap free
Amount of items: 2
Items: 
Size: 749597 Color: 2
Size: 250051 Color: 0

Bin 3652: 354 of cap free
Amount of items: 2
Items: 
Size: 791029 Color: 2
Size: 208618 Color: 4

Bin 3653: 357 of cap free
Amount of items: 2
Items: 
Size: 530182 Color: 4
Size: 469462 Color: 2

Bin 3654: 359 of cap free
Amount of items: 2
Items: 
Size: 766794 Color: 2
Size: 232848 Color: 3

Bin 3655: 359 of cap free
Amount of items: 2
Items: 
Size: 595181 Color: 1
Size: 404461 Color: 4

Bin 3656: 360 of cap free
Amount of items: 2
Items: 
Size: 643284 Color: 0
Size: 356357 Color: 2

Bin 3657: 360 of cap free
Amount of items: 2
Items: 
Size: 729676 Color: 3
Size: 269965 Color: 2

Bin 3658: 360 of cap free
Amount of items: 2
Items: 
Size: 741585 Color: 1
Size: 258056 Color: 4

Bin 3659: 360 of cap free
Amount of items: 2
Items: 
Size: 617991 Color: 3
Size: 381650 Color: 1

Bin 3660: 362 of cap free
Amount of items: 3
Items: 
Size: 482103 Color: 2
Size: 265168 Color: 4
Size: 252368 Color: 1

Bin 3661: 362 of cap free
Amount of items: 2
Items: 
Size: 606913 Color: 2
Size: 392726 Color: 1

Bin 3662: 362 of cap free
Amount of items: 2
Items: 
Size: 674437 Color: 1
Size: 325202 Color: 2

Bin 3663: 363 of cap free
Amount of items: 2
Items: 
Size: 769970 Color: 1
Size: 229668 Color: 2

Bin 3664: 363 of cap free
Amount of items: 2
Items: 
Size: 795952 Color: 1
Size: 203686 Color: 4

Bin 3665: 363 of cap free
Amount of items: 2
Items: 
Size: 595897 Color: 4
Size: 403741 Color: 2

Bin 3666: 364 of cap free
Amount of items: 2
Items: 
Size: 692662 Color: 3
Size: 306975 Color: 4

Bin 3667: 364 of cap free
Amount of items: 2
Items: 
Size: 545520 Color: 3
Size: 454117 Color: 0

Bin 3668: 364 of cap free
Amount of items: 2
Items: 
Size: 728229 Color: 0
Size: 271408 Color: 4

Bin 3669: 365 of cap free
Amount of items: 2
Items: 
Size: 653454 Color: 4
Size: 346182 Color: 1

Bin 3670: 367 of cap free
Amount of items: 2
Items: 
Size: 585965 Color: 2
Size: 413669 Color: 0

Bin 3671: 368 of cap free
Amount of items: 2
Items: 
Size: 567267 Color: 0
Size: 432366 Color: 3

Bin 3672: 369 of cap free
Amount of items: 2
Items: 
Size: 532180 Color: 4
Size: 467452 Color: 2

Bin 3673: 369 of cap free
Amount of items: 2
Items: 
Size: 551996 Color: 0
Size: 447636 Color: 2

Bin 3674: 371 of cap free
Amount of items: 2
Items: 
Size: 593133 Color: 2
Size: 406497 Color: 0

Bin 3675: 371 of cap free
Amount of items: 2
Items: 
Size: 538751 Color: 3
Size: 460879 Color: 2

Bin 3676: 371 of cap free
Amount of items: 2
Items: 
Size: 537297 Color: 4
Size: 462333 Color: 2

Bin 3677: 372 of cap free
Amount of items: 3
Items: 
Size: 702350 Color: 4
Size: 152414 Color: 3
Size: 144865 Color: 1

Bin 3678: 372 of cap free
Amount of items: 2
Items: 
Size: 782119 Color: 4
Size: 217510 Color: 2

Bin 3679: 373 of cap free
Amount of items: 2
Items: 
Size: 676172 Color: 4
Size: 323456 Color: 0

Bin 3680: 376 of cap free
Amount of items: 2
Items: 
Size: 612631 Color: 3
Size: 386994 Color: 2

Bin 3681: 378 of cap free
Amount of items: 2
Items: 
Size: 623932 Color: 2
Size: 375691 Color: 4

Bin 3682: 378 of cap free
Amount of items: 2
Items: 
Size: 671264 Color: 3
Size: 328359 Color: 2

Bin 3683: 378 of cap free
Amount of items: 2
Items: 
Size: 759628 Color: 3
Size: 239995 Color: 0

Bin 3684: 379 of cap free
Amount of items: 2
Items: 
Size: 560622 Color: 3
Size: 439000 Color: 0

Bin 3685: 380 of cap free
Amount of items: 2
Items: 
Size: 777155 Color: 2
Size: 222466 Color: 1

Bin 3686: 380 of cap free
Amount of items: 2
Items: 
Size: 665968 Color: 2
Size: 333653 Color: 4

Bin 3687: 381 of cap free
Amount of items: 2
Items: 
Size: 609855 Color: 2
Size: 389765 Color: 4

Bin 3688: 381 of cap free
Amount of items: 2
Items: 
Size: 604765 Color: 1
Size: 394855 Color: 0

Bin 3689: 381 of cap free
Amount of items: 2
Items: 
Size: 659446 Color: 3
Size: 340174 Color: 0

Bin 3690: 381 of cap free
Amount of items: 2
Items: 
Size: 682618 Color: 2
Size: 317002 Color: 3

Bin 3691: 381 of cap free
Amount of items: 2
Items: 
Size: 640653 Color: 3
Size: 358967 Color: 1

Bin 3692: 382 of cap free
Amount of items: 2
Items: 
Size: 579052 Color: 4
Size: 420567 Color: 2

Bin 3693: 385 of cap free
Amount of items: 2
Items: 
Size: 671851 Color: 2
Size: 327765 Color: 1

Bin 3694: 386 of cap free
Amount of items: 2
Items: 
Size: 566640 Color: 3
Size: 432975 Color: 1

Bin 3695: 386 of cap free
Amount of items: 2
Items: 
Size: 610263 Color: 4
Size: 389352 Color: 3

Bin 3696: 387 of cap free
Amount of items: 2
Items: 
Size: 568247 Color: 4
Size: 431367 Color: 1

Bin 3697: 388 of cap free
Amount of items: 2
Items: 
Size: 511605 Color: 0
Size: 488008 Color: 3

Bin 3698: 388 of cap free
Amount of items: 2
Items: 
Size: 783856 Color: 0
Size: 215757 Color: 3

Bin 3699: 390 of cap free
Amount of items: 2
Items: 
Size: 583882 Color: 0
Size: 415729 Color: 2

Bin 3700: 390 of cap free
Amount of items: 2
Items: 
Size: 766736 Color: 4
Size: 232875 Color: 2

Bin 3701: 393 of cap free
Amount of items: 2
Items: 
Size: 599732 Color: 1
Size: 399876 Color: 0

Bin 3702: 396 of cap free
Amount of items: 2
Items: 
Size: 791964 Color: 3
Size: 207641 Color: 2

Bin 3703: 396 of cap free
Amount of items: 2
Items: 
Size: 720912 Color: 4
Size: 278693 Color: 1

Bin 3704: 397 of cap free
Amount of items: 3
Items: 
Size: 704217 Color: 2
Size: 149785 Color: 3
Size: 145602 Color: 0

Bin 3705: 397 of cap free
Amount of items: 2
Items: 
Size: 704011 Color: 0
Size: 295593 Color: 2

Bin 3706: 397 of cap free
Amount of items: 2
Items: 
Size: 700263 Color: 4
Size: 299341 Color: 2

Bin 3707: 397 of cap free
Amount of items: 2
Items: 
Size: 557987 Color: 4
Size: 441617 Color: 3

Bin 3708: 398 of cap free
Amount of items: 2
Items: 
Size: 645999 Color: 1
Size: 353604 Color: 3

Bin 3709: 398 of cap free
Amount of items: 3
Items: 
Size: 460032 Color: 3
Size: 269973 Color: 3
Size: 269598 Color: 1

Bin 3710: 399 of cap free
Amount of items: 2
Items: 
Size: 512174 Color: 1
Size: 487428 Color: 2

Bin 3711: 399 of cap free
Amount of items: 2
Items: 
Size: 617803 Color: 4
Size: 381799 Color: 2

Bin 3712: 399 of cap free
Amount of items: 2
Items: 
Size: 631417 Color: 1
Size: 368185 Color: 0

Bin 3713: 400 of cap free
Amount of items: 2
Items: 
Size: 797103 Color: 2
Size: 202498 Color: 3

Bin 3714: 402 of cap free
Amount of items: 2
Items: 
Size: 767552 Color: 1
Size: 232047 Color: 2

Bin 3715: 403 of cap free
Amount of items: 2
Items: 
Size: 669970 Color: 3
Size: 329628 Color: 0

Bin 3716: 406 of cap free
Amount of items: 2
Items: 
Size: 796017 Color: 2
Size: 203578 Color: 3

Bin 3717: 406 of cap free
Amount of items: 2
Items: 
Size: 768204 Color: 3
Size: 231391 Color: 1

Bin 3718: 406 of cap free
Amount of items: 2
Items: 
Size: 521093 Color: 1
Size: 478502 Color: 0

Bin 3719: 407 of cap free
Amount of items: 2
Items: 
Size: 518392 Color: 3
Size: 481202 Color: 4

Bin 3720: 407 of cap free
Amount of items: 2
Items: 
Size: 568708 Color: 1
Size: 430886 Color: 2

Bin 3721: 407 of cap free
Amount of items: 2
Items: 
Size: 656411 Color: 1
Size: 343183 Color: 4

Bin 3722: 408 of cap free
Amount of items: 2
Items: 
Size: 724949 Color: 0
Size: 274644 Color: 3

Bin 3723: 408 of cap free
Amount of items: 2
Items: 
Size: 619883 Color: 0
Size: 379710 Color: 1

Bin 3724: 408 of cap free
Amount of items: 2
Items: 
Size: 652228 Color: 1
Size: 347365 Color: 3

Bin 3725: 408 of cap free
Amount of items: 2
Items: 
Size: 544413 Color: 4
Size: 455180 Color: 0

Bin 3726: 409 of cap free
Amount of items: 2
Items: 
Size: 567371 Color: 1
Size: 432221 Color: 3

Bin 3727: 410 of cap free
Amount of items: 2
Items: 
Size: 535538 Color: 1
Size: 464053 Color: 4

Bin 3728: 410 of cap free
Amount of items: 2
Items: 
Size: 579027 Color: 2
Size: 420564 Color: 1

Bin 3729: 410 of cap free
Amount of items: 2
Items: 
Size: 765926 Color: 2
Size: 233665 Color: 0

Bin 3730: 411 of cap free
Amount of items: 2
Items: 
Size: 698802 Color: 3
Size: 300788 Color: 0

Bin 3731: 412 of cap free
Amount of items: 2
Items: 
Size: 775559 Color: 4
Size: 224030 Color: 2

Bin 3732: 415 of cap free
Amount of items: 2
Items: 
Size: 710741 Color: 4
Size: 288845 Color: 0

Bin 3733: 416 of cap free
Amount of items: 2
Items: 
Size: 619195 Color: 1
Size: 380390 Color: 0

Bin 3734: 418 of cap free
Amount of items: 2
Items: 
Size: 736888 Color: 4
Size: 262695 Color: 1

Bin 3735: 419 of cap free
Amount of items: 2
Items: 
Size: 752198 Color: 2
Size: 247384 Color: 0

Bin 3736: 419 of cap free
Amount of items: 2
Items: 
Size: 500907 Color: 3
Size: 498675 Color: 0

Bin 3737: 419 of cap free
Amount of items: 2
Items: 
Size: 501720 Color: 4
Size: 497862 Color: 2

Bin 3738: 420 of cap free
Amount of items: 2
Items: 
Size: 617165 Color: 3
Size: 382416 Color: 4

Bin 3739: 421 of cap free
Amount of items: 2
Items: 
Size: 590597 Color: 0
Size: 408983 Color: 3

Bin 3740: 422 of cap free
Amount of items: 2
Items: 
Size: 534574 Color: 4
Size: 465005 Color: 3

Bin 3741: 423 of cap free
Amount of items: 2
Items: 
Size: 689308 Color: 3
Size: 310270 Color: 1

Bin 3742: 424 of cap free
Amount of items: 2
Items: 
Size: 797087 Color: 4
Size: 202490 Color: 1

Bin 3743: 424 of cap free
Amount of items: 2
Items: 
Size: 556312 Color: 4
Size: 443265 Color: 0

Bin 3744: 424 of cap free
Amount of items: 2
Items: 
Size: 657957 Color: 2
Size: 341620 Color: 3

Bin 3745: 426 of cap free
Amount of items: 2
Items: 
Size: 640578 Color: 3
Size: 358997 Color: 2

Bin 3746: 426 of cap free
Amount of items: 2
Items: 
Size: 708226 Color: 4
Size: 291349 Color: 0

Bin 3747: 427 of cap free
Amount of items: 2
Items: 
Size: 566603 Color: 0
Size: 432971 Color: 2

Bin 3748: 428 of cap free
Amount of items: 2
Items: 
Size: 578026 Color: 2
Size: 421547 Color: 0

Bin 3749: 429 of cap free
Amount of items: 2
Items: 
Size: 574895 Color: 3
Size: 424677 Color: 4

Bin 3750: 429 of cap free
Amount of items: 2
Items: 
Size: 562435 Color: 1
Size: 437137 Color: 3

Bin 3751: 431 of cap free
Amount of items: 2
Items: 
Size: 773276 Color: 0
Size: 226294 Color: 4

Bin 3752: 431 of cap free
Amount of items: 2
Items: 
Size: 552178 Color: 2
Size: 447392 Color: 4

Bin 3753: 432 of cap free
Amount of items: 2
Items: 
Size: 657200 Color: 4
Size: 342369 Color: 1

Bin 3754: 432 of cap free
Amount of items: 2
Items: 
Size: 624535 Color: 0
Size: 375034 Color: 1

Bin 3755: 433 of cap free
Amount of items: 2
Items: 
Size: 591440 Color: 3
Size: 408128 Color: 4

Bin 3756: 434 of cap free
Amount of items: 2
Items: 
Size: 561686 Color: 1
Size: 437881 Color: 2

Bin 3757: 434 of cap free
Amount of items: 2
Items: 
Size: 779492 Color: 4
Size: 220075 Color: 0

Bin 3758: 435 of cap free
Amount of items: 2
Items: 
Size: 511607 Color: 3
Size: 487959 Color: 4

Bin 3759: 435 of cap free
Amount of items: 2
Items: 
Size: 705638 Color: 1
Size: 293928 Color: 0

Bin 3760: 436 of cap free
Amount of items: 2
Items: 
Size: 666147 Color: 2
Size: 333418 Color: 1

Bin 3761: 436 of cap free
Amount of items: 2
Items: 
Size: 775571 Color: 2
Size: 223994 Color: 0

Bin 3762: 437 of cap free
Amount of items: 2
Items: 
Size: 720148 Color: 2
Size: 279416 Color: 4

Bin 3763: 442 of cap free
Amount of items: 2
Items: 
Size: 772969 Color: 1
Size: 226590 Color: 0

Bin 3764: 442 of cap free
Amount of items: 2
Items: 
Size: 784589 Color: 0
Size: 214970 Color: 4

Bin 3765: 443 of cap free
Amount of items: 2
Items: 
Size: 768401 Color: 1
Size: 231157 Color: 3

Bin 3766: 443 of cap free
Amount of items: 2
Items: 
Size: 725751 Color: 0
Size: 273807 Color: 1

Bin 3767: 444 of cap free
Amount of items: 2
Items: 
Size: 700291 Color: 2
Size: 299266 Color: 4

Bin 3768: 444 of cap free
Amount of items: 2
Items: 
Size: 692705 Color: 4
Size: 306852 Color: 3

Bin 3769: 446 of cap free
Amount of items: 2
Items: 
Size: 516603 Color: 1
Size: 482952 Color: 0

Bin 3770: 447 of cap free
Amount of items: 2
Items: 
Size: 740995 Color: 1
Size: 258559 Color: 3

Bin 3771: 447 of cap free
Amount of items: 2
Items: 
Size: 767366 Color: 3
Size: 232188 Color: 4

Bin 3772: 447 of cap free
Amount of items: 2
Items: 
Size: 794907 Color: 4
Size: 204647 Color: 2

Bin 3773: 447 of cap free
Amount of items: 2
Items: 
Size: 508104 Color: 0
Size: 491450 Color: 4

Bin 3774: 449 of cap free
Amount of items: 2
Items: 
Size: 746009 Color: 1
Size: 253543 Color: 0

Bin 3775: 451 of cap free
Amount of items: 2
Items: 
Size: 594141 Color: 0
Size: 405409 Color: 3

Bin 3776: 452 of cap free
Amount of items: 2
Items: 
Size: 658990 Color: 4
Size: 340559 Color: 0

Bin 3777: 453 of cap free
Amount of items: 2
Items: 
Size: 662309 Color: 3
Size: 337239 Color: 0

Bin 3778: 454 of cap free
Amount of items: 2
Items: 
Size: 621501 Color: 4
Size: 378046 Color: 2

Bin 3779: 455 of cap free
Amount of items: 2
Items: 
Size: 634578 Color: 2
Size: 364968 Color: 4

Bin 3780: 457 of cap free
Amount of items: 2
Items: 
Size: 600351 Color: 3
Size: 399193 Color: 2

Bin 3781: 457 of cap free
Amount of items: 2
Items: 
Size: 783290 Color: 3
Size: 216254 Color: 2

Bin 3782: 458 of cap free
Amount of items: 2
Items: 
Size: 611793 Color: 2
Size: 387750 Color: 4

Bin 3783: 458 of cap free
Amount of items: 2
Items: 
Size: 720138 Color: 3
Size: 279405 Color: 1

Bin 3784: 459 of cap free
Amount of items: 2
Items: 
Size: 581222 Color: 2
Size: 418320 Color: 0

Bin 3785: 460 of cap free
Amount of items: 2
Items: 
Size: 784698 Color: 0
Size: 214843 Color: 1

Bin 3786: 460 of cap free
Amount of items: 2
Items: 
Size: 789289 Color: 0
Size: 210252 Color: 2

Bin 3787: 461 of cap free
Amount of items: 2
Items: 
Size: 644422 Color: 3
Size: 355118 Color: 2

Bin 3788: 461 of cap free
Amount of items: 2
Items: 
Size: 583084 Color: 3
Size: 416456 Color: 2

Bin 3789: 463 of cap free
Amount of items: 2
Items: 
Size: 557088 Color: 4
Size: 442450 Color: 1

Bin 3790: 463 of cap free
Amount of items: 2
Items: 
Size: 578974 Color: 0
Size: 420564 Color: 3

Bin 3791: 465 of cap free
Amount of items: 2
Items: 
Size: 523076 Color: 1
Size: 476460 Color: 3

Bin 3792: 465 of cap free
Amount of items: 2
Items: 
Size: 751827 Color: 1
Size: 247709 Color: 2

Bin 3793: 466 of cap free
Amount of items: 2
Items: 
Size: 715615 Color: 0
Size: 283920 Color: 1

Bin 3794: 467 of cap free
Amount of items: 2
Items: 
Size: 561487 Color: 4
Size: 438047 Color: 3

Bin 3795: 468 of cap free
Amount of items: 2
Items: 
Size: 744701 Color: 3
Size: 254832 Color: 0

Bin 3796: 468 of cap free
Amount of items: 2
Items: 
Size: 563430 Color: 1
Size: 436103 Color: 4

Bin 3797: 468 of cap free
Amount of items: 2
Items: 
Size: 509018 Color: 2
Size: 490515 Color: 3

Bin 3798: 468 of cap free
Amount of items: 2
Items: 
Size: 604857 Color: 0
Size: 394676 Color: 4

Bin 3799: 469 of cap free
Amount of items: 2
Items: 
Size: 585864 Color: 0
Size: 413668 Color: 2

Bin 3800: 472 of cap free
Amount of items: 3
Items: 
Size: 681218 Color: 1
Size: 159324 Color: 3
Size: 158987 Color: 3

Bin 3801: 472 of cap free
Amount of items: 2
Items: 
Size: 529485 Color: 0
Size: 470044 Color: 1

Bin 3802: 473 of cap free
Amount of items: 2
Items: 
Size: 786169 Color: 4
Size: 213359 Color: 3

Bin 3803: 473 of cap free
Amount of items: 2
Items: 
Size: 563410 Color: 0
Size: 436118 Color: 1

Bin 3804: 474 of cap free
Amount of items: 2
Items: 
Size: 799748 Color: 0
Size: 199779 Color: 1

Bin 3805: 475 of cap free
Amount of items: 2
Items: 
Size: 745996 Color: 1
Size: 253530 Color: 2

Bin 3806: 475 of cap free
Amount of items: 2
Items: 
Size: 637961 Color: 1
Size: 361565 Color: 4

Bin 3807: 476 of cap free
Amount of items: 2
Items: 
Size: 661450 Color: 2
Size: 338075 Color: 0

Bin 3808: 478 of cap free
Amount of items: 2
Items: 
Size: 637971 Color: 4
Size: 361552 Color: 2

Bin 3809: 479 of cap free
Amount of items: 2
Items: 
Size: 619869 Color: 1
Size: 379653 Color: 3

Bin 3810: 480 of cap free
Amount of items: 2
Items: 
Size: 668966 Color: 0
Size: 330555 Color: 3

Bin 3811: 480 of cap free
Amount of items: 2
Items: 
Size: 617883 Color: 4
Size: 381638 Color: 3

Bin 3812: 480 of cap free
Amount of items: 2
Items: 
Size: 506401 Color: 0
Size: 493120 Color: 3

Bin 3813: 482 of cap free
Amount of items: 2
Items: 
Size: 528619 Color: 2
Size: 470900 Color: 1

Bin 3814: 484 of cap free
Amount of items: 2
Items: 
Size: 698757 Color: 2
Size: 300760 Color: 1

Bin 3815: 484 of cap free
Amount of items: 2
Items: 
Size: 625795 Color: 3
Size: 373722 Color: 4

Bin 3816: 485 of cap free
Amount of items: 2
Items: 
Size: 654083 Color: 3
Size: 345433 Color: 4

Bin 3817: 489 of cap free
Amount of items: 2
Items: 
Size: 792726 Color: 0
Size: 206786 Color: 2

Bin 3818: 491 of cap free
Amount of items: 2
Items: 
Size: 571775 Color: 4
Size: 427735 Color: 1

Bin 3819: 491 of cap free
Amount of items: 2
Items: 
Size: 511565 Color: 1
Size: 487945 Color: 4

Bin 3820: 491 of cap free
Amount of items: 2
Items: 
Size: 537294 Color: 0
Size: 462216 Color: 1

Bin 3821: 492 of cap free
Amount of items: 2
Items: 
Size: 598323 Color: 0
Size: 401186 Color: 2

Bin 3822: 493 of cap free
Amount of items: 2
Items: 
Size: 722581 Color: 0
Size: 276927 Color: 1

Bin 3823: 494 of cap free
Amount of items: 2
Items: 
Size: 714889 Color: 2
Size: 284618 Color: 4

Bin 3824: 495 of cap free
Amount of items: 2
Items: 
Size: 545394 Color: 4
Size: 454112 Color: 1

Bin 3825: 496 of cap free
Amount of items: 2
Items: 
Size: 744688 Color: 3
Size: 254817 Color: 0

Bin 3826: 496 of cap free
Amount of items: 2
Items: 
Size: 708167 Color: 1
Size: 291338 Color: 2

Bin 3827: 497 of cap free
Amount of items: 2
Items: 
Size: 588222 Color: 2
Size: 411282 Color: 4

Bin 3828: 499 of cap free
Amount of items: 2
Items: 
Size: 783209 Color: 0
Size: 216293 Color: 3

Bin 3829: 499 of cap free
Amount of items: 2
Items: 
Size: 640554 Color: 2
Size: 358948 Color: 0

Bin 3830: 499 of cap free
Amount of items: 2
Items: 
Size: 509960 Color: 3
Size: 489542 Color: 2

Bin 3831: 502 of cap free
Amount of items: 2
Items: 
Size: 528032 Color: 0
Size: 471467 Color: 4

Bin 3832: 503 of cap free
Amount of items: 2
Items: 
Size: 657032 Color: 3
Size: 342466 Color: 4

Bin 3833: 504 of cap free
Amount of items: 2
Items: 
Size: 718845 Color: 1
Size: 280652 Color: 2

Bin 3834: 504 of cap free
Amount of items: 2
Items: 
Size: 566587 Color: 4
Size: 432910 Color: 1

Bin 3835: 505 of cap free
Amount of items: 2
Items: 
Size: 536217 Color: 0
Size: 463279 Color: 1

Bin 3836: 506 of cap free
Amount of items: 2
Items: 
Size: 671032 Color: 4
Size: 328463 Color: 1

Bin 3837: 510 of cap free
Amount of items: 2
Items: 
Size: 672242 Color: 0
Size: 327249 Color: 1

Bin 3838: 510 of cap free
Amount of items: 2
Items: 
Size: 627365 Color: 4
Size: 372126 Color: 1

Bin 3839: 513 of cap free
Amount of items: 2
Items: 
Size: 629727 Color: 0
Size: 369761 Color: 1

Bin 3840: 516 of cap free
Amount of items: 2
Items: 
Size: 504171 Color: 2
Size: 495314 Color: 1

Bin 3841: 517 of cap free
Amount of items: 2
Items: 
Size: 580304 Color: 4
Size: 419180 Color: 3

Bin 3842: 521 of cap free
Amount of items: 2
Items: 
Size: 632402 Color: 1
Size: 367078 Color: 4

Bin 3843: 522 of cap free
Amount of items: 2
Items: 
Size: 588646 Color: 0
Size: 410833 Color: 3

Bin 3844: 523 of cap free
Amount of items: 2
Items: 
Size: 570470 Color: 4
Size: 429008 Color: 0

Bin 3845: 523 of cap free
Amount of items: 2
Items: 
Size: 542807 Color: 1
Size: 456671 Color: 2

Bin 3846: 524 of cap free
Amount of items: 2
Items: 
Size: 707200 Color: 1
Size: 292277 Color: 3

Bin 3847: 525 of cap free
Amount of items: 2
Items: 
Size: 605989 Color: 4
Size: 393487 Color: 1

Bin 3848: 528 of cap free
Amount of items: 2
Items: 
Size: 579535 Color: 4
Size: 419938 Color: 1

Bin 3849: 529 of cap free
Amount of items: 2
Items: 
Size: 710606 Color: 0
Size: 288866 Color: 2

Bin 3850: 530 of cap free
Amount of items: 2
Items: 
Size: 793267 Color: 4
Size: 206204 Color: 1

Bin 3851: 530 of cap free
Amount of items: 2
Items: 
Size: 640434 Color: 1
Size: 359037 Color: 2

Bin 3852: 532 of cap free
Amount of items: 2
Items: 
Size: 519494 Color: 2
Size: 479975 Color: 4

Bin 3853: 532 of cap free
Amount of items: 2
Items: 
Size: 785318 Color: 0
Size: 214151 Color: 4

Bin 3854: 534 of cap free
Amount of items: 2
Items: 
Size: 507107 Color: 3
Size: 492360 Color: 0

Bin 3855: 534 of cap free
Amount of items: 2
Items: 
Size: 570651 Color: 0
Size: 428816 Color: 2

Bin 3856: 539 of cap free
Amount of items: 2
Items: 
Size: 577916 Color: 4
Size: 421546 Color: 0

Bin 3857: 540 of cap free
Amount of items: 3
Items: 
Size: 698158 Color: 1
Size: 169924 Color: 3
Size: 131379 Color: 3

Bin 3858: 540 of cap free
Amount of items: 2
Items: 
Size: 761811 Color: 4
Size: 237650 Color: 3

Bin 3859: 541 of cap free
Amount of items: 2
Items: 
Size: 709944 Color: 4
Size: 289516 Color: 1

Bin 3860: 542 of cap free
Amount of items: 3
Items: 
Size: 716704 Color: 0
Size: 155533 Color: 2
Size: 127222 Color: 4

Bin 3861: 542 of cap free
Amount of items: 2
Items: 
Size: 577485 Color: 4
Size: 421974 Color: 2

Bin 3862: 543 of cap free
Amount of items: 2
Items: 
Size: 689194 Color: 0
Size: 310264 Color: 1

Bin 3863: 543 of cap free
Amount of items: 2
Items: 
Size: 601010 Color: 0
Size: 398448 Color: 2

Bin 3864: 545 of cap free
Amount of items: 2
Items: 
Size: 652197 Color: 3
Size: 347259 Color: 1

Bin 3865: 546 of cap free
Amount of items: 2
Items: 
Size: 682585 Color: 0
Size: 316870 Color: 4

Bin 3866: 549 of cap free
Amount of items: 2
Items: 
Size: 599090 Color: 2
Size: 400362 Color: 0

Bin 3867: 549 of cap free
Amount of items: 2
Items: 
Size: 637934 Color: 3
Size: 361518 Color: 1

Bin 3868: 551 of cap free
Amount of items: 2
Items: 
Size: 695123 Color: 4
Size: 304327 Color: 0

Bin 3869: 551 of cap free
Amount of items: 2
Items: 
Size: 797032 Color: 3
Size: 202418 Color: 4

Bin 3870: 552 of cap free
Amount of items: 2
Items: 
Size: 625488 Color: 3
Size: 373961 Color: 4

Bin 3871: 554 of cap free
Amount of items: 3
Items: 
Size: 590943 Color: 3
Size: 225463 Color: 1
Size: 183041 Color: 2

Bin 3872: 555 of cap free
Amount of items: 2
Items: 
Size: 529500 Color: 1
Size: 469946 Color: 3

Bin 3873: 557 of cap free
Amount of items: 2
Items: 
Size: 583039 Color: 1
Size: 416405 Color: 2

Bin 3874: 557 of cap free
Amount of items: 2
Items: 
Size: 675236 Color: 0
Size: 324208 Color: 1

Bin 3875: 559 of cap free
Amount of items: 2
Items: 
Size: 616274 Color: 3
Size: 383168 Color: 1

Bin 3876: 559 of cap free
Amount of items: 2
Items: 
Size: 619833 Color: 1
Size: 379609 Color: 3

Bin 3877: 560 of cap free
Amount of items: 2
Items: 
Size: 611841 Color: 3
Size: 387600 Color: 1

Bin 3878: 561 of cap free
Amount of items: 2
Items: 
Size: 571226 Color: 0
Size: 428214 Color: 4

Bin 3879: 561 of cap free
Amount of items: 2
Items: 
Size: 731412 Color: 0
Size: 268028 Color: 4

Bin 3880: 566 of cap free
Amount of items: 3
Items: 
Size: 545563 Color: 1
Size: 247143 Color: 2
Size: 206729 Color: 4

Bin 3881: 568 of cap free
Amount of items: 2
Items: 
Size: 501677 Color: 0
Size: 497756 Color: 2

Bin 3882: 569 of cap free
Amount of items: 2
Items: 
Size: 615185 Color: 1
Size: 384247 Color: 0

Bin 3883: 570 of cap free
Amount of items: 2
Items: 
Size: 687355 Color: 0
Size: 312076 Color: 2

Bin 3884: 571 of cap free
Amount of items: 2
Items: 
Size: 691202 Color: 4
Size: 308228 Color: 2

Bin 3885: 577 of cap free
Amount of items: 2
Items: 
Size: 520122 Color: 3
Size: 479302 Color: 0

Bin 3886: 578 of cap free
Amount of items: 2
Items: 
Size: 619805 Color: 3
Size: 379618 Color: 1

Bin 3887: 579 of cap free
Amount of items: 2
Items: 
Size: 673690 Color: 0
Size: 325732 Color: 3

Bin 3888: 580 of cap free
Amount of items: 2
Items: 
Size: 518505 Color: 4
Size: 480916 Color: 2

Bin 3889: 582 of cap free
Amount of items: 2
Items: 
Size: 661392 Color: 1
Size: 338027 Color: 4

Bin 3890: 582 of cap free
Amount of items: 2
Items: 
Size: 662151 Color: 0
Size: 337268 Color: 2

Bin 3891: 583 of cap free
Amount of items: 2
Items: 
Size: 665086 Color: 3
Size: 334332 Color: 0

Bin 3892: 584 of cap free
Amount of items: 2
Items: 
Size: 714016 Color: 3
Size: 285401 Color: 0

Bin 3893: 585 of cap free
Amount of items: 2
Items: 
Size: 794900 Color: 2
Size: 204516 Color: 4

Bin 3894: 587 of cap free
Amount of items: 2
Items: 
Size: 695099 Color: 2
Size: 304315 Color: 0

Bin 3895: 591 of cap free
Amount of items: 2
Items: 
Size: 611663 Color: 0
Size: 387747 Color: 4

Bin 3896: 592 of cap free
Amount of items: 2
Items: 
Size: 715482 Color: 1
Size: 283927 Color: 0

Bin 3897: 595 of cap free
Amount of items: 2
Items: 
Size: 777649 Color: 0
Size: 221757 Color: 3

Bin 3898: 596 of cap free
Amount of items: 2
Items: 
Size: 578895 Color: 2
Size: 420510 Color: 0

Bin 3899: 597 of cap free
Amount of items: 2
Items: 
Size: 513583 Color: 2
Size: 485821 Color: 3

Bin 3900: 598 of cap free
Amount of items: 2
Items: 
Size: 562277 Color: 4
Size: 437126 Color: 0

Bin 3901: 598 of cap free
Amount of items: 2
Items: 
Size: 644340 Color: 2
Size: 355063 Color: 0

Bin 3902: 598 of cap free
Amount of items: 2
Items: 
Size: 748545 Color: 3
Size: 250858 Color: 1

Bin 3903: 600 of cap free
Amount of items: 2
Items: 
Size: 727142 Color: 0
Size: 272259 Color: 3

Bin 3904: 601 of cap free
Amount of items: 2
Items: 
Size: 657131 Color: 3
Size: 342269 Color: 0

Bin 3905: 606 of cap free
Amount of items: 2
Items: 
Size: 611293 Color: 0
Size: 388102 Color: 3

Bin 3906: 607 of cap free
Amount of items: 2
Items: 
Size: 521039 Color: 2
Size: 478355 Color: 0

Bin 3907: 609 of cap free
Amount of items: 2
Items: 
Size: 758392 Color: 2
Size: 241000 Color: 1

Bin 3908: 610 of cap free
Amount of items: 2
Items: 
Size: 700062 Color: 4
Size: 299329 Color: 2

Bin 3909: 611 of cap free
Amount of items: 2
Items: 
Size: 606847 Color: 1
Size: 392543 Color: 0

Bin 3910: 611 of cap free
Amount of items: 2
Items: 
Size: 596479 Color: 1
Size: 402911 Color: 2

Bin 3911: 612 of cap free
Amount of items: 2
Items: 
Size: 728886 Color: 0
Size: 270503 Color: 3

Bin 3912: 612 of cap free
Amount of items: 2
Items: 
Size: 551202 Color: 1
Size: 448187 Color: 0

Bin 3913: 613 of cap free
Amount of items: 2
Items: 
Size: 794896 Color: 2
Size: 204492 Color: 4

Bin 3914: 618 of cap free
Amount of items: 2
Items: 
Size: 536165 Color: 1
Size: 463218 Color: 0

Bin 3915: 620 of cap free
Amount of items: 2
Items: 
Size: 722555 Color: 2
Size: 276826 Color: 0

Bin 3916: 620 of cap free
Amount of items: 2
Items: 
Size: 778861 Color: 3
Size: 220520 Color: 0

Bin 3917: 622 of cap free
Amount of items: 2
Items: 
Size: 627319 Color: 3
Size: 372060 Color: 1

Bin 3918: 624 of cap free
Amount of items: 3
Items: 
Size: 737585 Color: 1
Size: 141178 Color: 2
Size: 120614 Color: 3

Bin 3919: 627 of cap free
Amount of items: 2
Items: 
Size: 581168 Color: 2
Size: 418206 Color: 0

Bin 3920: 629 of cap free
Amount of items: 3
Items: 
Size: 392527 Color: 1
Size: 365723 Color: 3
Size: 241122 Color: 0

Bin 3921: 632 of cap free
Amount of items: 2
Items: 
Size: 594644 Color: 2
Size: 404725 Color: 1

Bin 3922: 633 of cap free
Amount of items: 2
Items: 
Size: 786104 Color: 1
Size: 213264 Color: 0

Bin 3923: 633 of cap free
Amount of items: 2
Items: 
Size: 785313 Color: 3
Size: 214055 Color: 0

Bin 3924: 635 of cap free
Amount of items: 2
Items: 
Size: 540183 Color: 3
Size: 459183 Color: 4

Bin 3925: 636 of cap free
Amount of items: 2
Items: 
Size: 735140 Color: 1
Size: 264225 Color: 4

Bin 3926: 636 of cap free
Amount of items: 2
Items: 
Size: 766724 Color: 3
Size: 232641 Color: 1

Bin 3927: 636 of cap free
Amount of items: 2
Items: 
Size: 607916 Color: 1
Size: 391449 Color: 0

Bin 3928: 638 of cap free
Amount of items: 2
Items: 
Size: 739811 Color: 3
Size: 259552 Color: 1

Bin 3929: 642 of cap free
Amount of items: 2
Items: 
Size: 647556 Color: 1
Size: 351803 Color: 0

Bin 3930: 646 of cap free
Amount of items: 2
Items: 
Size: 747733 Color: 1
Size: 251622 Color: 3

Bin 3931: 646 of cap free
Amount of items: 2
Items: 
Size: 592919 Color: 3
Size: 406436 Color: 0

Bin 3932: 648 of cap free
Amount of items: 2
Items: 
Size: 730468 Color: 4
Size: 268885 Color: 0

Bin 3933: 650 of cap free
Amount of items: 2
Items: 
Size: 508009 Color: 0
Size: 491342 Color: 4

Bin 3934: 653 of cap free
Amount of items: 3
Items: 
Size: 689541 Color: 4
Size: 159592 Color: 4
Size: 150215 Color: 2

Bin 3935: 654 of cap free
Amount of items: 2
Items: 
Size: 704416 Color: 2
Size: 294931 Color: 3

Bin 3936: 656 of cap free
Amount of items: 2
Items: 
Size: 593097 Color: 2
Size: 406248 Color: 0

Bin 3937: 656 of cap free
Amount of items: 2
Items: 
Size: 580272 Color: 2
Size: 419073 Color: 4

Bin 3938: 658 of cap free
Amount of items: 2
Items: 
Size: 744575 Color: 4
Size: 254768 Color: 3

Bin 3939: 658 of cap free
Amount of items: 2
Items: 
Size: 559019 Color: 4
Size: 440324 Color: 1

Bin 3940: 660 of cap free
Amount of items: 2
Items: 
Size: 535409 Color: 0
Size: 463932 Color: 1

Bin 3941: 661 of cap free
Amount of items: 2
Items: 
Size: 624391 Color: 3
Size: 374949 Color: 4

Bin 3942: 662 of cap free
Amount of items: 2
Items: 
Size: 503975 Color: 2
Size: 495364 Color: 0

Bin 3943: 664 of cap free
Amount of items: 2
Items: 
Size: 640407 Color: 1
Size: 358930 Color: 2

Bin 3944: 666 of cap free
Amount of items: 2
Items: 
Size: 702357 Color: 1
Size: 296978 Color: 0

Bin 3945: 667 of cap free
Amount of items: 2
Items: 
Size: 522081 Color: 0
Size: 477253 Color: 3

Bin 3946: 667 of cap free
Amount of items: 2
Items: 
Size: 637823 Color: 3
Size: 361511 Color: 4

Bin 3947: 669 of cap free
Amount of items: 2
Items: 
Size: 622631 Color: 0
Size: 376701 Color: 2

Bin 3948: 669 of cap free
Amount of items: 2
Items: 
Size: 574751 Color: 2
Size: 424581 Color: 3

Bin 3949: 670 of cap free
Amount of items: 2
Items: 
Size: 636469 Color: 2
Size: 362862 Color: 3

Bin 3950: 672 of cap free
Amount of items: 2
Items: 
Size: 600891 Color: 1
Size: 398438 Color: 4

Bin 3951: 674 of cap free
Amount of items: 2
Items: 
Size: 585015 Color: 3
Size: 414312 Color: 1

Bin 3952: 675 of cap free
Amount of items: 2
Items: 
Size: 671822 Color: 1
Size: 327504 Color: 0

Bin 3953: 678 of cap free
Amount of items: 2
Items: 
Size: 501704 Color: 2
Size: 497619 Color: 3

Bin 3954: 678 of cap free
Amount of items: 2
Items: 
Size: 669699 Color: 2
Size: 329624 Color: 3

Bin 3955: 681 of cap free
Amount of items: 2
Items: 
Size: 531017 Color: 4
Size: 468303 Color: 1

Bin 3956: 682 of cap free
Amount of items: 2
Items: 
Size: 750445 Color: 0
Size: 248874 Color: 2

Bin 3957: 682 of cap free
Amount of items: 3
Items: 
Size: 392285 Color: 2
Size: 309130 Color: 2
Size: 297904 Color: 3

Bin 3958: 685 of cap free
Amount of items: 2
Items: 
Size: 532224 Color: 2
Size: 467092 Color: 4

Bin 3959: 685 of cap free
Amount of items: 2
Items: 
Size: 510149 Color: 3
Size: 489167 Color: 1

Bin 3960: 687 of cap free
Amount of items: 2
Items: 
Size: 587397 Color: 0
Size: 411917 Color: 2

Bin 3961: 690 of cap free
Amount of items: 2
Items: 
Size: 510190 Color: 1
Size: 489121 Color: 0

Bin 3962: 692 of cap free
Amount of items: 2
Items: 
Size: 541173 Color: 2
Size: 458136 Color: 3

Bin 3963: 695 of cap free
Amount of items: 2
Items: 
Size: 509003 Color: 2
Size: 490303 Color: 4

Bin 3964: 696 of cap free
Amount of items: 2
Items: 
Size: 540201 Color: 4
Size: 459104 Color: 1

Bin 3965: 698 of cap free
Amount of items: 2
Items: 
Size: 689164 Color: 1
Size: 310139 Color: 3

Bin 3966: 699 of cap free
Amount of items: 2
Items: 
Size: 530172 Color: 3
Size: 469130 Color: 1

Bin 3967: 700 of cap free
Amount of items: 2
Items: 
Size: 793903 Color: 3
Size: 205398 Color: 4

Bin 3968: 703 of cap free
Amount of items: 2
Items: 
Size: 553852 Color: 4
Size: 445446 Color: 0

Bin 3969: 705 of cap free
Amount of items: 2
Items: 
Size: 513576 Color: 4
Size: 485720 Color: 3

Bin 3970: 705 of cap free
Amount of items: 2
Items: 
Size: 607883 Color: 3
Size: 391413 Color: 4

Bin 3971: 718 of cap free
Amount of items: 2
Items: 
Size: 500823 Color: 2
Size: 498460 Color: 3

Bin 3972: 723 of cap free
Amount of items: 2
Items: 
Size: 510112 Color: 3
Size: 489166 Color: 2

Bin 3973: 725 of cap free
Amount of items: 2
Items: 
Size: 644334 Color: 2
Size: 354942 Color: 0

Bin 3974: 725 of cap free
Amount of items: 2
Items: 
Size: 596446 Color: 4
Size: 402830 Color: 0

Bin 3975: 727 of cap free
Amount of items: 2
Items: 
Size: 604470 Color: 2
Size: 394804 Color: 0

Bin 3976: 730 of cap free
Amount of items: 2
Items: 
Size: 576397 Color: 4
Size: 422874 Color: 0

Bin 3977: 732 of cap free
Amount of items: 2
Items: 
Size: 725685 Color: 0
Size: 273584 Color: 2

Bin 3978: 732 of cap free
Amount of items: 2
Items: 
Size: 502858 Color: 1
Size: 496411 Color: 2

Bin 3979: 732 of cap free
Amount of items: 2
Items: 
Size: 747414 Color: 2
Size: 251855 Color: 1

Bin 3980: 732 of cap free
Amount of items: 2
Items: 
Size: 548341 Color: 2
Size: 450928 Color: 3

Bin 3981: 732 of cap free
Amount of items: 2
Items: 
Size: 783087 Color: 2
Size: 216182 Color: 1

Bin 3982: 733 of cap free
Amount of items: 2
Items: 
Size: 606829 Color: 1
Size: 392439 Color: 4

Bin 3983: 734 of cap free
Amount of items: 2
Items: 
Size: 676816 Color: 1
Size: 322451 Color: 0

Bin 3984: 736 of cap free
Amount of items: 2
Items: 
Size: 638966 Color: 3
Size: 360299 Color: 2

Bin 3985: 737 of cap free
Amount of items: 2
Items: 
Size: 711839 Color: 2
Size: 287425 Color: 3

Bin 3986: 739 of cap free
Amount of items: 2
Items: 
Size: 502811 Color: 3
Size: 496451 Color: 2

Bin 3987: 739 of cap free
Amount of items: 2
Items: 
Size: 627318 Color: 1
Size: 371944 Color: 3

Bin 3988: 741 of cap free
Amount of items: 2
Items: 
Size: 633345 Color: 0
Size: 365915 Color: 4

Bin 3989: 743 of cap free
Amount of items: 2
Items: 
Size: 773968 Color: 2
Size: 225290 Color: 1

Bin 3990: 746 of cap free
Amount of items: 2
Items: 
Size: 571446 Color: 1
Size: 427809 Color: 4

Bin 3991: 746 of cap free
Amount of items: 2
Items: 
Size: 687985 Color: 1
Size: 311270 Color: 0

Bin 3992: 752 of cap free
Amount of items: 2
Items: 
Size: 518400 Color: 4
Size: 480849 Color: 3

Bin 3993: 753 of cap free
Amount of items: 2
Items: 
Size: 745930 Color: 1
Size: 253318 Color: 2

Bin 3994: 754 of cap free
Amount of items: 2
Items: 
Size: 527797 Color: 1
Size: 471450 Color: 2

Bin 3995: 755 of cap free
Amount of items: 2
Items: 
Size: 720120 Color: 3
Size: 279126 Color: 1

Bin 3996: 757 of cap free
Amount of items: 2
Items: 
Size: 629473 Color: 1
Size: 369771 Color: 2

Bin 3997: 761 of cap free
Amount of items: 2
Items: 
Size: 500771 Color: 1
Size: 498469 Color: 4

Bin 3998: 764 of cap free
Amount of items: 3
Items: 
Size: 734867 Color: 1
Size: 135416 Color: 0
Size: 128954 Color: 1

Bin 3999: 766 of cap free
Amount of items: 2
Items: 
Size: 796988 Color: 2
Size: 202247 Color: 1

Bin 4000: 778 of cap free
Amount of items: 2
Items: 
Size: 545247 Color: 2
Size: 453976 Color: 0

Bin 4001: 778 of cap free
Amount of items: 2
Items: 
Size: 564535 Color: 4
Size: 434688 Color: 2

Bin 4002: 781 of cap free
Amount of items: 2
Items: 
Size: 739330 Color: 0
Size: 259890 Color: 3

Bin 4003: 781 of cap free
Amount of items: 2
Items: 
Size: 537093 Color: 3
Size: 462127 Color: 1

Bin 4004: 787 of cap free
Amount of items: 2
Items: 
Size: 527710 Color: 1
Size: 471504 Color: 4

Bin 4005: 787 of cap free
Amount of items: 2
Items: 
Size: 754541 Color: 2
Size: 244673 Color: 0

Bin 4006: 789 of cap free
Amount of items: 2
Items: 
Size: 715397 Color: 1
Size: 283815 Color: 3

Bin 4007: 794 of cap free
Amount of items: 2
Items: 
Size: 668797 Color: 2
Size: 330410 Color: 3

Bin 4008: 797 of cap free
Amount of items: 2
Items: 
Size: 520916 Color: 0
Size: 478288 Color: 2

Bin 4009: 800 of cap free
Amount of items: 2
Items: 
Size: 567034 Color: 4
Size: 432167 Color: 2

Bin 4010: 817 of cap free
Amount of items: 2
Items: 
Size: 760628 Color: 1
Size: 238556 Color: 0

Bin 4011: 817 of cap free
Amount of items: 2
Items: 
Size: 663800 Color: 0
Size: 335384 Color: 4

Bin 4012: 828 of cap free
Amount of items: 2
Items: 
Size: 542296 Color: 4
Size: 456877 Color: 1

Bin 4013: 831 of cap free
Amount of items: 2
Items: 
Size: 516562 Color: 2
Size: 482608 Color: 4

Bin 4014: 834 of cap free
Amount of items: 3
Items: 
Size: 787277 Color: 4
Size: 111595 Color: 4
Size: 100295 Color: 1

Bin 4015: 834 of cap free
Amount of items: 2
Items: 
Size: 536131 Color: 2
Size: 463036 Color: 3

Bin 4016: 835 of cap free
Amount of items: 2
Items: 
Size: 748513 Color: 1
Size: 250653 Color: 3

Bin 4017: 840 of cap free
Amount of items: 2
Items: 
Size: 782997 Color: 4
Size: 216164 Color: 0

Bin 4018: 843 of cap free
Amount of items: 2
Items: 
Size: 649396 Color: 0
Size: 349762 Color: 4

Bin 4019: 846 of cap free
Amount of items: 2
Items: 
Size: 609349 Color: 4
Size: 389806 Color: 2

Bin 4020: 850 of cap free
Amount of items: 2
Items: 
Size: 728886 Color: 3
Size: 270265 Color: 1

Bin 4021: 851 of cap free
Amount of items: 2
Items: 
Size: 570335 Color: 4
Size: 428815 Color: 0

Bin 4022: 853 of cap free
Amount of items: 2
Items: 
Size: 798911 Color: 2
Size: 200237 Color: 3

Bin 4023: 855 of cap free
Amount of items: 2
Items: 
Size: 527764 Color: 0
Size: 471382 Color: 2

Bin 4024: 855 of cap free
Amount of items: 2
Items: 
Size: 596335 Color: 1
Size: 402811 Color: 3

Bin 4025: 856 of cap free
Amount of items: 2
Items: 
Size: 611558 Color: 3
Size: 387587 Color: 1

Bin 4026: 857 of cap free
Amount of items: 2
Items: 
Size: 698398 Color: 3
Size: 300746 Color: 2

Bin 4027: 857 of cap free
Amount of items: 2
Items: 
Size: 718475 Color: 2
Size: 280669 Color: 1

Bin 4028: 860 of cap free
Amount of items: 2
Items: 
Size: 614894 Color: 3
Size: 384247 Color: 2

Bin 4029: 862 of cap free
Amount of items: 2
Items: 
Size: 732793 Color: 2
Size: 266346 Color: 1

Bin 4030: 865 of cap free
Amount of items: 2
Items: 
Size: 748515 Color: 4
Size: 250621 Color: 3

Bin 4031: 868 of cap free
Amount of items: 2
Items: 
Size: 726927 Color: 0
Size: 272206 Color: 1

Bin 4032: 869 of cap free
Amount of items: 2
Items: 
Size: 521881 Color: 1
Size: 477251 Color: 4

Bin 4033: 873 of cap free
Amount of items: 2
Items: 
Size: 513543 Color: 4
Size: 485585 Color: 3

Bin 4034: 874 of cap free
Amount of items: 2
Items: 
Size: 652112 Color: 3
Size: 347015 Color: 0

Bin 4035: 875 of cap free
Amount of items: 2
Items: 
Size: 564533 Color: 0
Size: 434593 Color: 1

Bin 4036: 886 of cap free
Amount of items: 3
Items: 
Size: 562960 Color: 2
Size: 254018 Color: 1
Size: 182137 Color: 4

Bin 4037: 890 of cap free
Amount of items: 2
Items: 
Size: 533089 Color: 4
Size: 466022 Color: 2

Bin 4038: 895 of cap free
Amount of items: 2
Items: 
Size: 548338 Color: 2
Size: 450768 Color: 1

Bin 4039: 899 of cap free
Amount of items: 2
Items: 
Size: 505022 Color: 2
Size: 494080 Color: 0

Bin 4040: 904 of cap free
Amount of items: 2
Items: 
Size: 722429 Color: 2
Size: 276668 Color: 3

Bin 4041: 905 of cap free
Amount of items: 2
Items: 
Size: 637599 Color: 0
Size: 361497 Color: 2

Bin 4042: 909 of cap free
Amount of items: 2
Items: 
Size: 791594 Color: 2
Size: 207498 Color: 0

Bin 4043: 909 of cap free
Amount of items: 2
Items: 
Size: 505986 Color: 1
Size: 493106 Color: 2

Bin 4044: 909 of cap free
Amount of items: 2
Items: 
Size: 787477 Color: 3
Size: 211615 Color: 4

Bin 4045: 909 of cap free
Amount of items: 2
Items: 
Size: 541998 Color: 0
Size: 457094 Color: 4

Bin 4046: 913 of cap free
Amount of items: 2
Items: 
Size: 762769 Color: 0
Size: 236319 Color: 1

Bin 4047: 913 of cap free
Amount of items: 2
Items: 
Size: 639240 Color: 2
Size: 359848 Color: 3

Bin 4048: 917 of cap free
Amount of items: 3
Items: 
Size: 500767 Color: 2
Size: 249333 Color: 0
Size: 248984 Color: 1

Bin 4049: 923 of cap free
Amount of items: 2
Items: 
Size: 756340 Color: 0
Size: 242738 Color: 4

Bin 4050: 924 of cap free
Amount of items: 2
Items: 
Size: 751844 Color: 2
Size: 247233 Color: 1

Bin 4051: 926 of cap free
Amount of items: 2
Items: 
Size: 789187 Color: 4
Size: 209888 Color: 2

Bin 4052: 931 of cap free
Amount of items: 2
Items: 
Size: 737893 Color: 0
Size: 261177 Color: 1

Bin 4053: 931 of cap free
Amount of items: 2
Items: 
Size: 538660 Color: 4
Size: 460410 Color: 1

Bin 4054: 934 of cap free
Amount of items: 2
Items: 
Size: 525390 Color: 4
Size: 473677 Color: 0

Bin 4055: 936 of cap free
Amount of items: 2
Items: 
Size: 552613 Color: 2
Size: 446452 Color: 1

Bin 4056: 939 of cap free
Amount of items: 2
Items: 
Size: 616909 Color: 1
Size: 382153 Color: 3

Bin 4057: 942 of cap free
Amount of items: 2
Items: 
Size: 627104 Color: 2
Size: 371955 Color: 1

Bin 4058: 942 of cap free
Amount of items: 2
Items: 
Size: 799861 Color: 3
Size: 199198 Color: 1

Bin 4059: 945 of cap free
Amount of items: 2
Items: 
Size: 787435 Color: 2
Size: 211621 Color: 4

Bin 4060: 953 of cap free
Amount of items: 2
Items: 
Size: 758289 Color: 4
Size: 240759 Color: 2

Bin 4061: 954 of cap free
Amount of items: 2
Items: 
Size: 739327 Color: 0
Size: 259720 Color: 3

Bin 4062: 954 of cap free
Amount of items: 2
Items: 
Size: 694946 Color: 2
Size: 304101 Color: 1

Bin 4063: 955 of cap free
Amount of items: 2
Items: 
Size: 799835 Color: 0
Size: 199211 Color: 3

Bin 4064: 968 of cap free
Amount of items: 2
Items: 
Size: 685912 Color: 2
Size: 313121 Color: 3

Bin 4065: 969 of cap free
Amount of items: 2
Items: 
Size: 640234 Color: 2
Size: 358798 Color: 4

Bin 4066: 976 of cap free
Amount of items: 2
Items: 
Size: 793676 Color: 1
Size: 205349 Color: 3

Bin 4067: 977 of cap free
Amount of items: 2
Items: 
Size: 592909 Color: 0
Size: 406115 Color: 1

Bin 4068: 988 of cap free
Amount of items: 2
Items: 
Size: 772719 Color: 1
Size: 226294 Color: 2

Bin 4069: 989 of cap free
Amount of items: 2
Items: 
Size: 599706 Color: 2
Size: 399306 Color: 4

Bin 4070: 995 of cap free
Amount of items: 2
Items: 
Size: 534458 Color: 2
Size: 464548 Color: 3

Bin 4071: 995 of cap free
Amount of items: 2
Items: 
Size: 702152 Color: 0
Size: 296854 Color: 1

Bin 4072: 998 of cap free
Amount of items: 2
Items: 
Size: 643102 Color: 3
Size: 355901 Color: 4

Bin 4073: 1009 of cap free
Amount of items: 2
Items: 
Size: 716316 Color: 2
Size: 282676 Color: 3

Bin 4074: 1011 of cap free
Amount of items: 2
Items: 
Size: 603109 Color: 2
Size: 395881 Color: 4

Bin 4075: 1013 of cap free
Amount of items: 2
Items: 
Size: 507864 Color: 1
Size: 491124 Color: 0

Bin 4076: 1015 of cap free
Amount of items: 2
Items: 
Size: 538658 Color: 2
Size: 460328 Color: 0

Bin 4077: 1017 of cap free
Amount of items: 2
Items: 
Size: 633499 Color: 1
Size: 365485 Color: 2

Bin 4078: 1018 of cap free
Amount of items: 2
Items: 
Size: 564508 Color: 2
Size: 434475 Color: 4

Bin 4079: 1018 of cap free
Amount of items: 2
Items: 
Size: 754257 Color: 3
Size: 244726 Color: 2

Bin 4080: 1020 of cap free
Amount of items: 2
Items: 
Size: 599284 Color: 0
Size: 399697 Color: 4

Bin 4081: 1022 of cap free
Amount of items: 2
Items: 
Size: 583693 Color: 3
Size: 415286 Color: 1

Bin 4082: 1027 of cap free
Amount of items: 2
Items: 
Size: 649386 Color: 3
Size: 349588 Color: 0

Bin 4083: 1029 of cap free
Amount of items: 2
Items: 
Size: 785680 Color: 0
Size: 213292 Color: 1

Bin 4084: 1030 of cap free
Amount of items: 2
Items: 
Size: 645480 Color: 1
Size: 353491 Color: 4

Bin 4085: 1031 of cap free
Amount of items: 2
Items: 
Size: 624082 Color: 0
Size: 374888 Color: 4

Bin 4086: 1031 of cap free
Amount of items: 2
Items: 
Size: 576394 Color: 1
Size: 422576 Color: 4

Bin 4087: 1034 of cap free
Amount of items: 2
Items: 
Size: 726919 Color: 0
Size: 272048 Color: 2

Bin 4088: 1035 of cap free
Amount of items: 2
Items: 
Size: 694767 Color: 3
Size: 304199 Color: 0

Bin 4089: 1035 of cap free
Amount of items: 2
Items: 
Size: 760053 Color: 4
Size: 238913 Color: 1

Bin 4090: 1035 of cap free
Amount of items: 2
Items: 
Size: 793834 Color: 3
Size: 205132 Color: 2

Bin 4091: 1037 of cap free
Amount of items: 2
Items: 
Size: 627173 Color: 1
Size: 371791 Color: 2

Bin 4092: 1039 of cap free
Amount of items: 2
Items: 
Size: 627132 Color: 0
Size: 371830 Color: 2

Bin 4093: 1040 of cap free
Amount of items: 2
Items: 
Size: 624360 Color: 4
Size: 374601 Color: 3

Bin 4094: 1043 of cap free
Amount of items: 2
Items: 
Size: 750107 Color: 4
Size: 248851 Color: 3

Bin 4095: 1045 of cap free
Amount of items: 2
Items: 
Size: 687277 Color: 3
Size: 311679 Color: 0

Bin 4096: 1046 of cap free
Amount of items: 2
Items: 
Size: 790735 Color: 1
Size: 208220 Color: 3

Bin 4097: 1049 of cap free
Amount of items: 2
Items: 
Size: 587090 Color: 4
Size: 411862 Color: 0

Bin 4098: 1058 of cap free
Amount of items: 2
Items: 
Size: 762677 Color: 1
Size: 236266 Color: 0

Bin 4099: 1071 of cap free
Amount of items: 2
Items: 
Size: 595296 Color: 3
Size: 403634 Color: 2

Bin 4100: 1075 of cap free
Amount of items: 2
Items: 
Size: 558606 Color: 2
Size: 440320 Color: 3

Bin 4101: 1078 of cap free
Amount of items: 2
Items: 
Size: 570285 Color: 4
Size: 428638 Color: 0

Bin 4102: 1081 of cap free
Amount of items: 2
Items: 
Size: 764411 Color: 1
Size: 234509 Color: 0

Bin 4103: 1082 of cap free
Amount of items: 2
Items: 
Size: 771447 Color: 0
Size: 227472 Color: 2

Bin 4104: 1084 of cap free
Amount of items: 2
Items: 
Size: 542116 Color: 0
Size: 456801 Color: 1

Bin 4105: 1084 of cap free
Amount of items: 2
Items: 
Size: 511123 Color: 0
Size: 487794 Color: 1

Bin 4106: 1086 of cap free
Amount of items: 2
Items: 
Size: 676384 Color: 4
Size: 322531 Color: 0

Bin 4107: 1086 of cap free
Amount of items: 2
Items: 
Size: 622030 Color: 1
Size: 376885 Color: 0

Bin 4108: 1087 of cap free
Amount of items: 2
Items: 
Size: 750117 Color: 3
Size: 248797 Color: 0

Bin 4109: 1101 of cap free
Amount of items: 2
Items: 
Size: 611500 Color: 3
Size: 387400 Color: 1

Bin 4110: 1103 of cap free
Amount of items: 2
Items: 
Size: 685899 Color: 1
Size: 312999 Color: 0

Bin 4111: 1104 of cap free
Amount of items: 2
Items: 
Size: 760515 Color: 3
Size: 238382 Color: 4

Bin 4112: 1104 of cap free
Amount of items: 2
Items: 
Size: 658727 Color: 2
Size: 340170 Color: 0

Bin 4113: 1122 of cap free
Amount of items: 2
Items: 
Size: 566361 Color: 0
Size: 432518 Color: 1

Bin 4114: 1122 of cap free
Amount of items: 2
Items: 
Size: 732710 Color: 2
Size: 266169 Color: 3

Bin 4115: 1123 of cap free
Amount of items: 3
Items: 
Size: 725224 Color: 2
Size: 146824 Color: 0
Size: 126830 Color: 4

Bin 4116: 1124 of cap free
Amount of items: 2
Items: 
Size: 726731 Color: 0
Size: 272146 Color: 1

Bin 4117: 1125 of cap free
Amount of items: 2
Items: 
Size: 564320 Color: 0
Size: 434556 Color: 3

Bin 4118: 1133 of cap free
Amount of items: 2
Items: 
Size: 558557 Color: 4
Size: 440311 Color: 1

Bin 4119: 1135 of cap free
Amount of items: 2
Items: 
Size: 718302 Color: 2
Size: 280564 Color: 1

Bin 4120: 1135 of cap free
Amount of items: 2
Items: 
Size: 641715 Color: 4
Size: 357151 Color: 0

Bin 4121: 1147 of cap free
Amount of items: 2
Items: 
Size: 633283 Color: 3
Size: 365571 Color: 4

Bin 4122: 1149 of cap free
Amount of items: 2
Items: 
Size: 730150 Color: 3
Size: 268702 Color: 4

Bin 4123: 1149 of cap free
Amount of items: 2
Items: 
Size: 769428 Color: 0
Size: 229424 Color: 3

Bin 4124: 1152 of cap free
Amount of items: 2
Items: 
Size: 665001 Color: 1
Size: 333848 Color: 2

Bin 4125: 1155 of cap free
Amount of items: 2
Items: 
Size: 540155 Color: 3
Size: 458691 Color: 0

Bin 4126: 1162 of cap free
Amount of items: 2
Items: 
Size: 656588 Color: 2
Size: 342251 Color: 3

Bin 4127: 1164 of cap free
Amount of items: 2
Items: 
Size: 513632 Color: 3
Size: 485205 Color: 0

Bin 4128: 1165 of cap free
Amount of items: 2
Items: 
Size: 782719 Color: 4
Size: 216117 Color: 0

Bin 4129: 1173 of cap free
Amount of items: 2
Items: 
Size: 706697 Color: 1
Size: 292131 Color: 3

Bin 4130: 1178 of cap free
Amount of items: 2
Items: 
Size: 507826 Color: 4
Size: 490997 Color: 2

Bin 4131: 1188 of cap free
Amount of items: 2
Items: 
Size: 730121 Color: 2
Size: 268692 Color: 4

Bin 4132: 1188 of cap free
Amount of items: 2
Items: 
Size: 769323 Color: 0
Size: 229490 Color: 1

Bin 4133: 1189 of cap free
Amount of items: 2
Items: 
Size: 759277 Color: 1
Size: 239535 Color: 4

Bin 4134: 1198 of cap free
Amount of items: 2
Items: 
Size: 706938 Color: 3
Size: 291865 Color: 4

Bin 4135: 1202 of cap free
Amount of items: 2
Items: 
Size: 790730 Color: 0
Size: 208069 Color: 4

Bin 4136: 1208 of cap free
Amount of items: 2
Items: 
Size: 706794 Color: 3
Size: 291999 Color: 0

Bin 4137: 1211 of cap free
Amount of items: 2
Items: 
Size: 730098 Color: 4
Size: 268692 Color: 1

Bin 4138: 1213 of cap free
Amount of items: 2
Items: 
Size: 609456 Color: 4
Size: 389332 Color: 3

Bin 4139: 1227 of cap free
Amount of items: 2
Items: 
Size: 728746 Color: 0
Size: 270028 Color: 3

Bin 4140: 1227 of cap free
Amount of items: 2
Items: 
Size: 620683 Color: 1
Size: 378091 Color: 4

Bin 4141: 1230 of cap free
Amount of items: 2
Items: 
Size: 799481 Color: 4
Size: 199290 Color: 0

Bin 4142: 1232 of cap free
Amount of items: 2
Items: 
Size: 751528 Color: 1
Size: 247241 Color: 0

Bin 4143: 1239 of cap free
Amount of items: 2
Items: 
Size: 683367 Color: 4
Size: 315395 Color: 2

Bin 4144: 1240 of cap free
Amount of items: 2
Items: 
Size: 691935 Color: 3
Size: 306826 Color: 2

Bin 4145: 1250 of cap free
Amount of items: 2
Items: 
Size: 743940 Color: 3
Size: 254811 Color: 4

Bin 4146: 1262 of cap free
Amount of items: 2
Items: 
Size: 722168 Color: 1
Size: 276571 Color: 2

Bin 4147: 1275 of cap free
Amount of items: 2
Items: 
Size: 786999 Color: 3
Size: 211727 Color: 4

Bin 4148: 1280 of cap free
Amount of items: 2
Items: 
Size: 706845 Color: 0
Size: 291876 Color: 2

Bin 4149: 1282 of cap free
Amount of items: 2
Items: 
Size: 538295 Color: 0
Size: 460424 Color: 2

Bin 4150: 1283 of cap free
Amount of items: 2
Items: 
Size: 645797 Color: 4
Size: 352921 Color: 0

Bin 4151: 1289 of cap free
Amount of items: 2
Items: 
Size: 619248 Color: 0
Size: 379464 Color: 4

Bin 4152: 1290 of cap free
Amount of items: 2
Items: 
Size: 564470 Color: 3
Size: 434241 Color: 4

Bin 4153: 1292 of cap free
Amount of items: 2
Items: 
Size: 751541 Color: 2
Size: 247168 Color: 4

Bin 4154: 1292 of cap free
Amount of items: 2
Items: 
Size: 706805 Color: 4
Size: 291904 Color: 0

Bin 4155: 1292 of cap free
Amount of items: 2
Items: 
Size: 542016 Color: 0
Size: 456693 Color: 1

Bin 4156: 1293 of cap free
Amount of items: 2
Items: 
Size: 546033 Color: 2
Size: 452675 Color: 4

Bin 4157: 1303 of cap free
Amount of items: 2
Items: 
Size: 629293 Color: 4
Size: 369405 Color: 3

Bin 4158: 1309 of cap free
Amount of items: 2
Items: 
Size: 649759 Color: 1
Size: 348933 Color: 2

Bin 4159: 1318 of cap free
Amount of items: 2
Items: 
Size: 789222 Color: 1
Size: 209461 Color: 0

Bin 4160: 1326 of cap free
Amount of items: 2
Items: 
Size: 542097 Color: 3
Size: 456578 Color: 0

Bin 4161: 1328 of cap free
Amount of items: 2
Items: 
Size: 558459 Color: 0
Size: 440214 Color: 3

Bin 4162: 1331 of cap free
Amount of items: 2
Items: 
Size: 566548 Color: 4
Size: 432122 Color: 1

Bin 4163: 1333 of cap free
Amount of items: 2
Items: 
Size: 786849 Color: 2
Size: 211819 Color: 3

Bin 4164: 1338 of cap free
Amount of items: 2
Items: 
Size: 534664 Color: 3
Size: 463999 Color: 4

Bin 4165: 1340 of cap free
Amount of items: 2
Items: 
Size: 778871 Color: 0
Size: 219790 Color: 3

Bin 4166: 1341 of cap free
Amount of items: 2
Items: 
Size: 505554 Color: 0
Size: 493106 Color: 2

Bin 4167: 1342 of cap free
Amount of items: 2
Items: 
Size: 732358 Color: 1
Size: 266301 Color: 2

Bin 4168: 1353 of cap free
Amount of items: 2
Items: 
Size: 518139 Color: 4
Size: 480509 Color: 1

Bin 4169: 1355 of cap free
Amount of items: 3
Items: 
Size: 402478 Color: 1
Size: 400591 Color: 0
Size: 195577 Color: 1

Bin 4170: 1355 of cap free
Amount of items: 2
Items: 
Size: 534659 Color: 3
Size: 463987 Color: 0

Bin 4171: 1356 of cap free
Amount of items: 2
Items: 
Size: 640174 Color: 3
Size: 358471 Color: 1

Bin 4172: 1356 of cap free
Amount of items: 2
Items: 
Size: 782912 Color: 0
Size: 215733 Color: 1

Bin 4173: 1357 of cap free
Amount of items: 2
Items: 
Size: 561510 Color: 3
Size: 437134 Color: 4

Bin 4174: 1357 of cap free
Amount of items: 2
Items: 
Size: 637147 Color: 0
Size: 361497 Color: 4

Bin 4175: 1359 of cap free
Amount of items: 2
Items: 
Size: 796428 Color: 4
Size: 202214 Color: 0

Bin 4176: 1361 of cap free
Amount of items: 2
Items: 
Size: 551174 Color: 4
Size: 447466 Color: 2

Bin 4177: 1367 of cap free
Amount of items: 2
Items: 
Size: 531461 Color: 1
Size: 467173 Color: 2

Bin 4178: 1371 of cap free
Amount of items: 2
Items: 
Size: 664677 Color: 1
Size: 333953 Color: 3

Bin 4179: 1372 of cap free
Amount of items: 2
Items: 
Size: 594579 Color: 3
Size: 404050 Color: 4

Bin 4180: 1378 of cap free
Amount of items: 2
Items: 
Size: 740632 Color: 4
Size: 257991 Color: 0

Bin 4181: 1381 of cap free
Amount of items: 2
Items: 
Size: 719471 Color: 2
Size: 279149 Color: 4

Bin 4182: 1389 of cap free
Amount of items: 2
Items: 
Size: 725060 Color: 2
Size: 273552 Color: 3

Bin 4183: 1391 of cap free
Amount of items: 2
Items: 
Size: 540106 Color: 0
Size: 458504 Color: 4

Bin 4184: 1401 of cap free
Amount of items: 2
Items: 
Size: 619184 Color: 3
Size: 379416 Color: 1

Bin 4185: 1402 of cap free
Amount of items: 2
Items: 
Size: 566534 Color: 1
Size: 432065 Color: 2

Bin 4186: 1403 of cap free
Amount of items: 2
Items: 
Size: 709445 Color: 3
Size: 289153 Color: 4

Bin 4187: 1405 of cap free
Amount of items: 2
Items: 
Size: 633153 Color: 2
Size: 365443 Color: 3

Bin 4188: 1421 of cap free
Amount of items: 2
Items: 
Size: 754103 Color: 1
Size: 244477 Color: 2

Bin 4189: 1424 of cap free
Amount of items: 2
Items: 
Size: 619738 Color: 4
Size: 378839 Color: 1

Bin 4190: 1428 of cap free
Amount of items: 2
Items: 
Size: 609293 Color: 4
Size: 389280 Color: 2

Bin 4191: 1431 of cap free
Amount of items: 2
Items: 
Size: 510939 Color: 3
Size: 487631 Color: 2

Bin 4192: 1431 of cap free
Amount of items: 2
Items: 
Size: 737411 Color: 2
Size: 261159 Color: 4

Bin 4193: 1432 of cap free
Amount of items: 2
Items: 
Size: 566479 Color: 3
Size: 432090 Color: 1

Bin 4194: 1433 of cap free
Amount of items: 2
Items: 
Size: 766485 Color: 0
Size: 232083 Color: 1

Bin 4195: 1441 of cap free
Amount of items: 2
Items: 
Size: 515977 Color: 0
Size: 482583 Color: 2

Bin 4196: 1444 of cap free
Amount of items: 2
Items: 
Size: 507517 Color: 0
Size: 491040 Color: 4

Bin 4197: 1445 of cap free
Amount of items: 2
Items: 
Size: 734573 Color: 4
Size: 263983 Color: 2

Bin 4198: 1449 of cap free
Amount of items: 2
Items: 
Size: 787210 Color: 0
Size: 211342 Color: 3

Bin 4199: 1460 of cap free
Amount of items: 2
Items: 
Size: 732248 Color: 1
Size: 266293 Color: 2

Bin 4200: 1462 of cap free
Amount of items: 2
Items: 
Size: 799266 Color: 3
Size: 199273 Color: 2

Bin 4201: 1462 of cap free
Amount of items: 2
Items: 
Size: 787206 Color: 0
Size: 211333 Color: 1

Bin 4202: 1467 of cap free
Amount of items: 2
Items: 
Size: 644291 Color: 4
Size: 354243 Color: 1

Bin 4203: 1469 of cap free
Amount of items: 2
Items: 
Size: 694704 Color: 0
Size: 303828 Color: 3

Bin 4204: 1470 of cap free
Amount of items: 2
Items: 
Size: 558655 Color: 3
Size: 439876 Color: 2

Bin 4205: 1471 of cap free
Amount of items: 2
Items: 
Size: 524709 Color: 0
Size: 473821 Color: 4

Bin 4206: 1480 of cap free
Amount of items: 2
Items: 
Size: 584878 Color: 4
Size: 413643 Color: 3

Bin 4207: 1487 of cap free
Amount of items: 2
Items: 
Size: 725109 Color: 3
Size: 273405 Color: 2

Bin 4208: 1489 of cap free
Amount of items: 2
Items: 
Size: 671657 Color: 0
Size: 326855 Color: 2

Bin 4209: 1497 of cap free
Amount of items: 2
Items: 
Size: 631438 Color: 0
Size: 367066 Color: 1

Bin 4210: 1503 of cap free
Amount of items: 2
Items: 
Size: 513518 Color: 0
Size: 484980 Color: 1

Bin 4211: 1511 of cap free
Amount of items: 2
Items: 
Size: 785300 Color: 1
Size: 213190 Color: 0

Bin 4212: 1512 of cap free
Amount of items: 2
Items: 
Size: 616300 Color: 1
Size: 382189 Color: 0

Bin 4213: 1512 of cap free
Amount of items: 2
Items: 
Size: 552053 Color: 2
Size: 446436 Color: 3

Bin 4214: 1516 of cap free
Amount of items: 2
Items: 
Size: 649503 Color: 2
Size: 348982 Color: 3

Bin 4215: 1527 of cap free
Amount of items: 2
Items: 
Size: 766718 Color: 2
Size: 231756 Color: 3

Bin 4216: 1549 of cap free
Amount of items: 2
Items: 
Size: 575889 Color: 4
Size: 422563 Color: 1

Bin 4217: 1553 of cap free
Amount of items: 2
Items: 
Size: 751564 Color: 0
Size: 246884 Color: 1

Bin 4218: 1558 of cap free
Amount of items: 2
Items: 
Size: 673645 Color: 4
Size: 324798 Color: 2

Bin 4219: 1569 of cap free
Amount of items: 2
Items: 
Size: 515903 Color: 0
Size: 482529 Color: 4

Bin 4220: 1574 of cap free
Amount of items: 2
Items: 
Size: 700070 Color: 4
Size: 298357 Color: 0

Bin 4221: 1584 of cap free
Amount of items: 2
Items: 
Size: 637158 Color: 4
Size: 361259 Color: 0

Bin 4222: 1587 of cap free
Amount of items: 2
Items: 
Size: 623822 Color: 0
Size: 374592 Color: 4

Bin 4223: 1591 of cap free
Amount of items: 2
Items: 
Size: 717853 Color: 1
Size: 280557 Color: 4

Bin 4224: 1592 of cap free
Amount of items: 2
Items: 
Size: 588235 Color: 3
Size: 410174 Color: 4

Bin 4225: 1597 of cap free
Amount of items: 2
Items: 
Size: 737576 Color: 3
Size: 260828 Color: 1

Bin 4226: 1602 of cap free
Amount of items: 2
Items: 
Size: 576292 Color: 0
Size: 422107 Color: 1

Bin 4227: 1606 of cap free
Amount of items: 2
Items: 
Size: 626568 Color: 4
Size: 371827 Color: 2

Bin 4228: 1607 of cap free
Amount of items: 2
Items: 
Size: 798195 Color: 3
Size: 200199 Color: 0

Bin 4229: 1609 of cap free
Amount of items: 2
Items: 
Size: 631326 Color: 2
Size: 367066 Color: 1

Bin 4230: 1612 of cap free
Amount of items: 2
Items: 
Size: 660505 Color: 3
Size: 337884 Color: 0

Bin 4231: 1615 of cap free
Amount of items: 2
Items: 
Size: 745523 Color: 1
Size: 252863 Color: 3

Bin 4232: 1617 of cap free
Amount of items: 2
Items: 
Size: 676341 Color: 3
Size: 322043 Color: 2

Bin 4233: 1622 of cap free
Amount of items: 2
Items: 
Size: 510988 Color: 3
Size: 487391 Color: 0

Bin 4234: 1637 of cap free
Amount of items: 2
Items: 
Size: 629135 Color: 2
Size: 369229 Color: 1

Bin 4235: 1642 of cap free
Amount of items: 2
Items: 
Size: 751490 Color: 3
Size: 246869 Color: 4

Bin 4236: 1646 of cap free
Amount of items: 2
Items: 
Size: 552022 Color: 2
Size: 446333 Color: 3

Bin 4237: 1661 of cap free
Amount of items: 2
Items: 
Size: 580251 Color: 4
Size: 418089 Color: 2

Bin 4238: 1668 of cap free
Amount of items: 2
Items: 
Size: 524657 Color: 2
Size: 473676 Color: 3

Bin 4239: 1673 of cap free
Amount of items: 2
Items: 
Size: 724923 Color: 3
Size: 273405 Color: 2

Bin 4240: 1677 of cap free
Amount of items: 2
Items: 
Size: 764668 Color: 0
Size: 233656 Color: 2

Bin 4241: 1680 of cap free
Amount of items: 2
Items: 
Size: 673184 Color: 3
Size: 325137 Color: 4

Bin 4242: 1688 of cap free
Amount of items: 3
Items: 
Size: 442029 Color: 1
Size: 318536 Color: 0
Size: 237748 Color: 1

Bin 4243: 1689 of cap free
Amount of items: 2
Items: 
Size: 713485 Color: 1
Size: 284827 Color: 2

Bin 4244: 1692 of cap free
Amount of items: 2
Items: 
Size: 511027 Color: 0
Size: 487282 Color: 1

Bin 4245: 1694 of cap free
Amount of items: 2
Items: 
Size: 799085 Color: 2
Size: 199222 Color: 1

Bin 4246: 1705 of cap free
Amount of items: 2
Items: 
Size: 616131 Color: 1
Size: 382165 Color: 0

Bin 4247: 1706 of cap free
Amount of items: 2
Items: 
Size: 566347 Color: 1
Size: 431948 Color: 0

Bin 4248: 1709 of cap free
Amount of items: 2
Items: 
Size: 751502 Color: 4
Size: 246790 Color: 2

Bin 4249: 1710 of cap free
Amount of items: 3
Items: 
Size: 626654 Color: 3
Size: 243238 Color: 3
Size: 128399 Color: 2

Bin 4250: 1710 of cap free
Amount of items: 2
Items: 
Size: 676467 Color: 0
Size: 321824 Color: 3

Bin 4251: 1716 of cap free
Amount of items: 2
Items: 
Size: 782605 Color: 0
Size: 215680 Color: 1

Bin 4252: 1719 of cap free
Amount of items: 2
Items: 
Size: 787174 Color: 1
Size: 211108 Color: 4

Bin 4253: 1724 of cap free
Amount of items: 2
Items: 
Size: 681494 Color: 3
Size: 316783 Color: 4

Bin 4254: 1734 of cap free
Amount of items: 2
Items: 
Size: 751466 Color: 1
Size: 246801 Color: 4

Bin 4255: 1736 of cap free
Amount of items: 2
Items: 
Size: 598672 Color: 1
Size: 399593 Color: 0

Bin 4256: 1736 of cap free
Amount of items: 2
Items: 
Size: 538372 Color: 2
Size: 459893 Color: 0

Bin 4257: 1740 of cap free
Amount of items: 2
Items: 
Size: 683585 Color: 2
Size: 314676 Color: 3

Bin 4258: 1743 of cap free
Amount of items: 2
Items: 
Size: 709493 Color: 1
Size: 288765 Color: 0

Bin 4259: 1748 of cap free
Amount of items: 2
Items: 
Size: 608999 Color: 4
Size: 389254 Color: 1

Bin 4260: 1751 of cap free
Amount of items: 2
Items: 
Size: 766633 Color: 4
Size: 231617 Color: 3

Bin 4261: 1794 of cap free
Amount of items: 2
Items: 
Size: 782437 Color: 2
Size: 215770 Color: 3

Bin 4262: 1799 of cap free
Amount of items: 2
Items: 
Size: 609199 Color: 0
Size: 389003 Color: 1

Bin 4263: 1804 of cap free
Amount of items: 2
Items: 
Size: 776661 Color: 0
Size: 221536 Color: 1

Bin 4264: 1810 of cap free
Amount of items: 2
Items: 
Size: 631209 Color: 0
Size: 366982 Color: 1

Bin 4265: 1810 of cap free
Amount of items: 2
Items: 
Size: 518136 Color: 2
Size: 480055 Color: 4

Bin 4266: 1817 of cap free
Amount of items: 2
Items: 
Size: 790139 Color: 0
Size: 208045 Color: 2

Bin 4267: 1821 of cap free
Amount of items: 2
Items: 
Size: 515546 Color: 2
Size: 482634 Color: 3

Bin 4268: 1832 of cap free
Amount of items: 2
Items: 
Size: 511028 Color: 0
Size: 487141 Color: 2

Bin 4269: 1843 of cap free
Amount of items: 2
Items: 
Size: 751496 Color: 4
Size: 246662 Color: 1

Bin 4270: 1847 of cap free
Amount of items: 2
Items: 
Size: 566212 Color: 0
Size: 431942 Color: 2

Bin 4271: 1847 of cap free
Amount of items: 2
Items: 
Size: 580231 Color: 0
Size: 417923 Color: 1

Bin 4272: 1849 of cap free
Amount of items: 2
Items: 
Size: 609148 Color: 4
Size: 389004 Color: 0

Bin 4273: 1856 of cap free
Amount of items: 2
Items: 
Size: 636951 Color: 3
Size: 361194 Color: 2

Bin 4274: 1858 of cap free
Amount of items: 2
Items: 
Size: 520913 Color: 4
Size: 477230 Color: 2

Bin 4275: 1868 of cap free
Amount of items: 2
Items: 
Size: 524482 Color: 3
Size: 473651 Color: 2

Bin 4276: 1881 of cap free
Amount of items: 3
Items: 
Size: 437114 Color: 2
Size: 354970 Color: 2
Size: 206036 Color: 3

Bin 4277: 1884 of cap free
Amount of items: 2
Items: 
Size: 513157 Color: 0
Size: 484960 Color: 2

Bin 4278: 1912 of cap free
Amount of items: 2
Items: 
Size: 658043 Color: 3
Size: 340046 Color: 2

Bin 4279: 1932 of cap free
Amount of items: 2
Items: 
Size: 694647 Color: 3
Size: 303422 Color: 2

Bin 4280: 1934 of cap free
Amount of items: 2
Items: 
Size: 524451 Color: 2
Size: 473616 Color: 3

Bin 4281: 1943 of cap free
Amount of items: 3
Items: 
Size: 732227 Color: 2
Size: 134396 Color: 4
Size: 131435 Color: 3

Bin 4282: 1952 of cap free
Amount of items: 2
Items: 
Size: 699701 Color: 1
Size: 298348 Color: 2

Bin 4283: 1953 of cap free
Amount of items: 2
Items: 
Size: 563428 Color: 1
Size: 434620 Color: 0

Bin 4284: 1970 of cap free
Amount of items: 3
Items: 
Size: 473202 Color: 3
Size: 359980 Color: 4
Size: 164849 Color: 1

Bin 4285: 1972 of cap free
Amount of items: 2
Items: 
Size: 504944 Color: 2
Size: 493085 Color: 0

Bin 4286: 1987 of cap free
Amount of items: 2
Items: 
Size: 576328 Color: 0
Size: 421686 Color: 2

Bin 4287: 2006 of cap free
Amount of items: 2
Items: 
Size: 555593 Color: 3
Size: 442402 Color: 1

Bin 4288: 2028 of cap free
Amount of items: 2
Items: 
Size: 603185 Color: 3
Size: 394788 Color: 2

Bin 4289: 2034 of cap free
Amount of items: 2
Items: 
Size: 743970 Color: 4
Size: 253997 Color: 1

Bin 4290: 2036 of cap free
Amount of items: 2
Items: 
Size: 668414 Color: 0
Size: 329551 Color: 1

Bin 4291: 2045 of cap free
Amount of items: 2
Items: 
Size: 520826 Color: 3
Size: 477130 Color: 0

Bin 4292: 2102 of cap free
Amount of items: 2
Items: 
Size: 527910 Color: 2
Size: 469989 Color: 1

Bin 4293: 2105 of cap free
Amount of items: 2
Items: 
Size: 776663 Color: 1
Size: 221233 Color: 2

Bin 4294: 2114 of cap free
Amount of items: 3
Items: 
Size: 648324 Color: 2
Size: 200696 Color: 1
Size: 148867 Color: 4

Bin 4295: 2126 of cap free
Amount of items: 2
Items: 
Size: 697493 Color: 0
Size: 300382 Color: 1

Bin 4296: 2130 of cap free
Amount of items: 2
Items: 
Size: 734354 Color: 1
Size: 263517 Color: 2

Bin 4297: 2140 of cap free
Amount of items: 2
Items: 
Size: 603103 Color: 3
Size: 394758 Color: 2

Bin 4298: 2148 of cap free
Amount of items: 2
Items: 
Size: 608952 Color: 4
Size: 388901 Color: 1

Bin 4299: 2162 of cap free
Amount of items: 2
Items: 
Size: 704002 Color: 2
Size: 293837 Color: 1

Bin 4300: 2178 of cap free
Amount of items: 2
Items: 
Size: 754065 Color: 0
Size: 243758 Color: 2

Bin 4301: 2181 of cap free
Amount of items: 2
Items: 
Size: 602997 Color: 2
Size: 394823 Color: 4

Bin 4302: 2183 of cap free
Amount of items: 2
Items: 
Size: 776590 Color: 4
Size: 221228 Color: 2

Bin 4303: 2189 of cap free
Amount of items: 2
Items: 
Size: 743004 Color: 1
Size: 254808 Color: 3

Bin 4304: 2191 of cap free
Amount of items: 2
Items: 
Size: 512853 Color: 2
Size: 484957 Color: 0

Bin 4305: 2217 of cap free
Amount of items: 2
Items: 
Size: 671261 Color: 4
Size: 326523 Color: 3

Bin 4306: 2218 of cap free
Amount of items: 2
Items: 
Size: 769076 Color: 2
Size: 228707 Color: 0

Bin 4307: 2236 of cap free
Amount of items: 2
Items: 
Size: 524413 Color: 1
Size: 473352 Color: 0

Bin 4308: 2245 of cap free
Amount of items: 2
Items: 
Size: 771595 Color: 4
Size: 226161 Color: 3

Bin 4309: 2253 of cap free
Amount of items: 2
Items: 
Size: 500626 Color: 0
Size: 497122 Color: 1

Bin 4310: 2254 of cap free
Amount of items: 2
Items: 
Size: 565854 Color: 3
Size: 431893 Color: 4

Bin 4311: 2272 of cap free
Amount of items: 3
Items: 
Size: 719736 Color: 3
Size: 176414 Color: 2
Size: 101579 Color: 3

Bin 4312: 2278 of cap free
Amount of items: 2
Items: 
Size: 761744 Color: 3
Size: 235979 Color: 1

Bin 4313: 2288 of cap free
Amount of items: 2
Items: 
Size: 766601 Color: 0
Size: 231112 Color: 3

Bin 4314: 2291 of cap free
Amount of items: 2
Items: 
Size: 798913 Color: 3
Size: 198797 Color: 2

Bin 4315: 2295 of cap free
Amount of items: 2
Items: 
Size: 792577 Color: 1
Size: 205129 Color: 4

Bin 4316: 2314 of cap free
Amount of items: 2
Items: 
Size: 797950 Color: 3
Size: 199737 Color: 2

Bin 4317: 2316 of cap free
Amount of items: 2
Items: 
Size: 697311 Color: 2
Size: 300374 Color: 0

Bin 4318: 2318 of cap free
Amount of items: 3
Items: 
Size: 477533 Color: 2
Size: 267292 Color: 3
Size: 252858 Color: 1

Bin 4319: 2330 of cap free
Amount of items: 2
Items: 
Size: 584482 Color: 1
Size: 413189 Color: 0

Bin 4320: 2336 of cap free
Amount of items: 2
Items: 
Size: 605380 Color: 0
Size: 392285 Color: 2

Bin 4321: 2348 of cap free
Amount of items: 2
Items: 
Size: 580031 Color: 1
Size: 417622 Color: 0

Bin 4322: 2362 of cap free
Amount of items: 2
Items: 
Size: 524370 Color: 1
Size: 473269 Color: 0

Bin 4323: 2402 of cap free
Amount of items: 2
Items: 
Size: 598546 Color: 2
Size: 399053 Color: 1

Bin 4324: 2404 of cap free
Amount of items: 2
Items: 
Size: 636434 Color: 2
Size: 361163 Color: 0

Bin 4325: 2418 of cap free
Amount of items: 2
Items: 
Size: 668398 Color: 1
Size: 329185 Color: 3

Bin 4326: 2423 of cap free
Amount of items: 2
Items: 
Size: 736834 Color: 0
Size: 260744 Color: 2

Bin 4327: 2423 of cap free
Amount of items: 2
Items: 
Size: 515437 Color: 4
Size: 482141 Color: 0

Bin 4328: 2430 of cap free
Amount of items: 2
Items: 
Size: 790126 Color: 0
Size: 207445 Color: 1

Bin 4329: 2444 of cap free
Amount of items: 2
Items: 
Size: 593906 Color: 4
Size: 403651 Color: 1

Bin 4330: 2461 of cap free
Amount of items: 3
Items: 
Size: 472744 Color: 2
Size: 327799 Color: 1
Size: 196997 Color: 2

Bin 4331: 2474 of cap free
Amount of items: 2
Items: 
Size: 736827 Color: 4
Size: 260700 Color: 2

Bin 4332: 2493 of cap free
Amount of items: 2
Items: 
Size: 636353 Color: 2
Size: 361155 Color: 0

Bin 4333: 2502 of cap free
Amount of items: 2
Items: 
Size: 708956 Color: 4
Size: 288543 Color: 1

Bin 4334: 2505 of cap free
Amount of items: 2
Items: 
Size: 766589 Color: 0
Size: 230907 Color: 2

Bin 4335: 2523 of cap free
Amount of items: 2
Items: 
Size: 636413 Color: 0
Size: 361065 Color: 3

Bin 4336: 2532 of cap free
Amount of items: 2
Items: 
Size: 798236 Color: 4
Size: 199233 Color: 1

Bin 4337: 2551 of cap free
Amount of items: 2
Items: 
Size: 766502 Color: 3
Size: 230948 Color: 1

Bin 4338: 2637 of cap free
Amount of items: 2
Items: 
Size: 728691 Color: 4
Size: 268673 Color: 1

Bin 4339: 2639 of cap free
Amount of items: 2
Items: 
Size: 771609 Color: 4
Size: 225753 Color: 2

Bin 4340: 2650 of cap free
Amount of items: 2
Items: 
Size: 551098 Color: 2
Size: 446253 Color: 0

Bin 4341: 2658 of cap free
Amount of items: 2
Items: 
Size: 570018 Color: 0
Size: 427325 Color: 3

Bin 4342: 2669 of cap free
Amount of items: 2
Items: 
Size: 563410 Color: 2
Size: 433922 Color: 3

Bin 4343: 2726 of cap free
Amount of items: 2
Items: 
Size: 693238 Color: 2
Size: 304037 Color: 3

Bin 4344: 2775 of cap free
Amount of items: 2
Items: 
Size: 574771 Color: 3
Size: 422455 Color: 4

Bin 4345: 2785 of cap free
Amount of items: 2
Items: 
Size: 660484 Color: 1
Size: 336732 Color: 0

Bin 4346: 2796 of cap free
Amount of items: 2
Items: 
Size: 798199 Color: 3
Size: 199006 Color: 1

Bin 4347: 2799 of cap free
Amount of items: 2
Items: 
Size: 570064 Color: 0
Size: 427138 Color: 4

Bin 4348: 2839 of cap free
Amount of items: 2
Items: 
Size: 499620 Color: 0
Size: 497542 Color: 2

Bin 4349: 2879 of cap free
Amount of items: 2
Items: 
Size: 743801 Color: 0
Size: 253321 Color: 3

Bin 4350: 2882 of cap free
Amount of items: 2
Items: 
Size: 512321 Color: 0
Size: 484798 Color: 4

Bin 4351: 2896 of cap free
Amount of items: 2
Items: 
Size: 599071 Color: 1
Size: 398034 Color: 0

Bin 4352: 2910 of cap free
Amount of items: 2
Items: 
Size: 544523 Color: 0
Size: 452568 Color: 2

Bin 4353: 2914 of cap free
Amount of items: 2
Items: 
Size: 580022 Color: 1
Size: 417065 Color: 4

Bin 4354: 2920 of cap free
Amount of items: 2
Items: 
Size: 599419 Color: 0
Size: 397662 Color: 2

Bin 4355: 2927 of cap free
Amount of items: 2
Items: 
Size: 762408 Color: 3
Size: 234666 Color: 4

Bin 4356: 2954 of cap free
Amount of items: 2
Items: 
Size: 798819 Color: 2
Size: 198228 Color: 3

Bin 4357: 2956 of cap free
Amount of items: 2
Items: 
Size: 737501 Color: 4
Size: 259544 Color: 1

Bin 4358: 2983 of cap free
Amount of items: 2
Items: 
Size: 566451 Color: 1
Size: 430567 Color: 3

Bin 4359: 2988 of cap free
Amount of items: 2
Items: 
Size: 686136 Color: 0
Size: 310877 Color: 1

Bin 4360: 3001 of cap free
Amount of items: 2
Items: 
Size: 575478 Color: 4
Size: 421522 Color: 2

Bin 4361: 3122 of cap free
Amount of items: 2
Items: 
Size: 599225 Color: 3
Size: 397654 Color: 2

Bin 4362: 3150 of cap free
Amount of items: 2
Items: 
Size: 799017 Color: 3
Size: 197834 Color: 0

Bin 4363: 3194 of cap free
Amount of items: 2
Items: 
Size: 538358 Color: 1
Size: 458449 Color: 0

Bin 4364: 3263 of cap free
Amount of items: 2
Items: 
Size: 593976 Color: 1
Size: 402762 Color: 2

Bin 4365: 3290 of cap free
Amount of items: 2
Items: 
Size: 671058 Color: 0
Size: 325653 Color: 2

Bin 4366: 3321 of cap free
Amount of items: 2
Items: 
Size: 538658 Color: 2
Size: 458022 Color: 1

Bin 4367: 3323 of cap free
Amount of items: 2
Items: 
Size: 618633 Color: 4
Size: 378045 Color: 0

Bin 4368: 3349 of cap free
Amount of items: 2
Items: 
Size: 520102 Color: 4
Size: 476550 Color: 1

Bin 4369: 3389 of cap free
Amount of items: 2
Items: 
Size: 798858 Color: 3
Size: 197754 Color: 2

Bin 4370: 3424 of cap free
Amount of items: 2
Items: 
Size: 584792 Color: 0
Size: 411785 Color: 4

Bin 4371: 3538 of cap free
Amount of items: 2
Items: 
Size: 551086 Color: 0
Size: 445377 Color: 2

Bin 4372: 3602 of cap free
Amount of items: 2
Items: 
Size: 670827 Color: 4
Size: 325572 Color: 3

Bin 4373: 3608 of cap free
Amount of items: 3
Items: 
Size: 609258 Color: 3
Size: 226487 Color: 2
Size: 160648 Color: 3

Bin 4374: 3628 of cap free
Amount of items: 2
Items: 
Size: 538248 Color: 2
Size: 458125 Color: 3

Bin 4375: 3651 of cap free
Amount of items: 2
Items: 
Size: 551030 Color: 0
Size: 445320 Color: 3

Bin 4376: 3670 of cap free
Amount of items: 2
Items: 
Size: 743344 Color: 3
Size: 252987 Color: 1

Bin 4377: 3693 of cap free
Amount of items: 2
Items: 
Size: 544211 Color: 2
Size: 452097 Color: 1

Bin 4378: 3729 of cap free
Amount of items: 2
Items: 
Size: 544201 Color: 0
Size: 452071 Color: 3

Bin 4379: 3779 of cap free
Amount of items: 2
Items: 
Size: 519965 Color: 1
Size: 476257 Color: 0

Bin 4380: 3780 of cap free
Amount of items: 2
Items: 
Size: 498788 Color: 0
Size: 497433 Color: 3

Bin 4381: 3798 of cap free
Amount of items: 2
Items: 
Size: 574542 Color: 1
Size: 421661 Color: 0

Bin 4382: 3863 of cap free
Amount of items: 2
Items: 
Size: 798419 Color: 0
Size: 197719 Color: 1

Bin 4383: 3974 of cap free
Amount of items: 2
Items: 
Size: 723888 Color: 2
Size: 272139 Color: 4

Bin 4384: 3998 of cap free
Amount of items: 2
Items: 
Size: 568748 Color: 2
Size: 427255 Color: 0

Bin 4385: 4024 of cap free
Amount of items: 2
Items: 
Size: 527356 Color: 1
Size: 468621 Color: 4

Bin 4386: 4062 of cap free
Amount of items: 2
Items: 
Size: 573456 Color: 4
Size: 422483 Color: 1

Bin 4387: 4069 of cap free
Amount of items: 2
Items: 
Size: 656213 Color: 3
Size: 339719 Color: 4

Bin 4388: 4081 of cap free
Amount of items: 2
Items: 
Size: 533897 Color: 2
Size: 462023 Color: 4

Bin 4389: 4146 of cap free
Amount of items: 2
Items: 
Size: 510925 Color: 1
Size: 484930 Color: 2

Bin 4390: 4156 of cap free
Amount of items: 2
Items: 
Size: 798345 Color: 4
Size: 197500 Color: 1

Bin 4391: 4184 of cap free
Amount of items: 2
Items: 
Size: 670829 Color: 3
Size: 324988 Color: 2

Bin 4392: 4200 of cap free
Amount of items: 2
Items: 
Size: 685848 Color: 0
Size: 309953 Color: 2

Bin 4393: 4216 of cap free
Amount of items: 2
Items: 
Size: 742694 Color: 2
Size: 253091 Color: 1

Bin 4394: 4292 of cap free
Amount of items: 2
Items: 
Size: 722148 Color: 2
Size: 273561 Color: 3

Bin 4395: 4299 of cap free
Amount of items: 2
Items: 
Size: 574500 Color: 2
Size: 421202 Color: 3

Bin 4396: 4328 of cap free
Amount of items: 2
Items: 
Size: 550729 Color: 4
Size: 444944 Color: 0

Bin 4397: 4368 of cap free
Amount of items: 2
Items: 
Size: 784627 Color: 1
Size: 211006 Color: 2

Bin 4398: 4382 of cap free
Amount of items: 2
Items: 
Size: 574089 Color: 3
Size: 421530 Color: 4

Bin 4399: 4394 of cap free
Amount of items: 2
Items: 
Size: 722168 Color: 3
Size: 273439 Color: 1

Bin 4400: 4396 of cap free
Amount of items: 2
Items: 
Size: 742547 Color: 4
Size: 253058 Color: 2

Bin 4401: 4450 of cap free
Amount of items: 2
Items: 
Size: 722154 Color: 1
Size: 273397 Color: 0

Bin 4402: 4539 of cap free
Amount of items: 2
Items: 
Size: 573002 Color: 2
Size: 422460 Color: 1

Bin 4403: 4658 of cap free
Amount of items: 2
Items: 
Size: 685840 Color: 1
Size: 309503 Color: 3

Bin 4404: 4680 of cap free
Amount of items: 2
Items: 
Size: 766495 Color: 0
Size: 228826 Color: 1

Bin 4405: 4757 of cap free
Amount of items: 2
Items: 
Size: 697384 Color: 4
Size: 297860 Color: 1

Bin 4406: 4774 of cap free
Amount of items: 2
Items: 
Size: 631262 Color: 1
Size: 363965 Color: 0

Bin 4407: 4797 of cap free
Amount of items: 2
Items: 
Size: 798066 Color: 4
Size: 197138 Color: 2

Bin 4408: 4802 of cap free
Amount of items: 2
Items: 
Size: 568194 Color: 4
Size: 427005 Color: 0

Bin 4409: 4882 of cap free
Amount of items: 2
Items: 
Size: 568126 Color: 0
Size: 426993 Color: 2

Bin 4410: 4895 of cap free
Amount of items: 2
Items: 
Size: 592326 Color: 4
Size: 402780 Color: 1

Bin 4411: 4919 of cap free
Amount of items: 2
Items: 
Size: 799908 Color: 3
Size: 195174 Color: 4

Bin 4412: 5006 of cap free
Amount of items: 2
Items: 
Size: 790832 Color: 3
Size: 204163 Color: 0

Bin 4413: 5057 of cap free
Amount of items: 2
Items: 
Size: 631282 Color: 2
Size: 363662 Color: 1

Bin 4414: 5098 of cap free
Amount of items: 2
Items: 
Size: 775482 Color: 4
Size: 219421 Color: 3

Bin 4415: 5165 of cap free
Amount of items: 2
Items: 
Size: 623539 Color: 0
Size: 371297 Color: 1

Bin 4416: 5168 of cap free
Amount of items: 2
Items: 
Size: 630910 Color: 1
Size: 363923 Color: 0

Bin 4417: 5173 of cap free
Amount of items: 2
Items: 
Size: 602543 Color: 2
Size: 392285 Color: 3

Bin 4418: 5195 of cap free
Amount of items: 2
Items: 
Size: 685446 Color: 2
Size: 309360 Color: 0

Bin 4419: 5258 of cap free
Amount of items: 2
Items: 
Size: 538251 Color: 3
Size: 456492 Color: 1

Bin 4420: 5327 of cap free
Amount of items: 2
Items: 
Size: 685351 Color: 0
Size: 309323 Color: 2

Bin 4421: 5354 of cap free
Amount of items: 2
Items: 
Size: 685382 Color: 3
Size: 309265 Color: 2

Bin 4422: 5360 of cap free
Amount of items: 2
Items: 
Size: 725240 Color: 4
Size: 269401 Color: 2

Bin 4423: 5365 of cap free
Amount of items: 2
Items: 
Size: 509906 Color: 2
Size: 484730 Color: 1

Bin 4424: 5424 of cap free
Amount of items: 2
Items: 
Size: 573518 Color: 4
Size: 421059 Color: 2

Bin 4425: 5661 of cap free
Amount of items: 2
Items: 
Size: 509728 Color: 0
Size: 484612 Color: 4

Bin 4426: 5904 of cap free
Amount of items: 2
Items: 
Size: 591377 Color: 0
Size: 402720 Color: 1

Bin 4427: 6184 of cap free
Amount of items: 2
Items: 
Size: 591301 Color: 1
Size: 402516 Color: 4

Bin 4428: 6220 of cap free
Amount of items: 2
Items: 
Size: 591299 Color: 0
Size: 402482 Color: 2

Bin 4429: 6230 of cap free
Amount of items: 2
Items: 
Size: 591370 Color: 0
Size: 402401 Color: 4

Bin 4430: 6503 of cap free
Amount of items: 2
Items: 
Size: 591281 Color: 4
Size: 402217 Color: 0

Bin 4431: 6590 of cap free
Amount of items: 2
Items: 
Size: 722050 Color: 3
Size: 271361 Color: 4

Bin 4432: 6642 of cap free
Amount of items: 2
Items: 
Size: 757378 Color: 1
Size: 235981 Color: 2

Bin 4433: 6743 of cap free
Amount of items: 2
Items: 
Size: 566199 Color: 4
Size: 427059 Color: 3

Bin 4434: 7004 of cap free
Amount of items: 2
Items: 
Size: 591023 Color: 4
Size: 401974 Color: 0

Bin 4435: 7400 of cap free
Amount of items: 2
Items: 
Size: 550553 Color: 1
Size: 442048 Color: 4

Bin 4436: 7546 of cap free
Amount of items: 2
Items: 
Size: 507798 Color: 4
Size: 484657 Color: 1

Bin 4437: 7618 of cap free
Amount of items: 2
Items: 
Size: 565328 Color: 1
Size: 427055 Color: 2

Bin 4438: 7666 of cap free
Amount of items: 2
Items: 
Size: 550296 Color: 4
Size: 442039 Color: 3

Bin 4439: 7716 of cap free
Amount of items: 2
Items: 
Size: 796523 Color: 0
Size: 195762 Color: 1

Bin 4440: 8023 of cap free
Amount of items: 2
Items: 
Size: 765731 Color: 2
Size: 226247 Color: 4

Bin 4441: 8405 of cap free
Amount of items: 2
Items: 
Size: 549779 Color: 3
Size: 441817 Color: 4

Bin 4442: 8947 of cap free
Amount of items: 2
Items: 
Size: 795912 Color: 0
Size: 195142 Color: 1

Bin 4443: 9981 of cap free
Amount of items: 2
Items: 
Size: 679742 Color: 2
Size: 310278 Color: 3

Bin 4444: 10114 of cap free
Amount of items: 2
Items: 
Size: 795487 Color: 4
Size: 194400 Color: 0

Bin 4445: 10151 of cap free
Amount of items: 2
Items: 
Size: 795562 Color: 4
Size: 194288 Color: 1

Bin 4446: 10647 of cap free
Amount of items: 2
Items: 
Size: 549672 Color: 3
Size: 439682 Color: 4

Bin 4447: 10792 of cap free
Amount of items: 2
Items: 
Size: 549777 Color: 4
Size: 439432 Color: 3

Bin 4448: 10845 of cap free
Amount of items: 2
Items: 
Size: 505650 Color: 1
Size: 483506 Color: 4

Bin 4449: 11722 of cap free
Amount of items: 2
Items: 
Size: 794875 Color: 1
Size: 193404 Color: 2

Bin 4450: 12191 of cap free
Amount of items: 2
Items: 
Size: 549247 Color: 1
Size: 438563 Color: 0

Bin 4451: 12801 of cap free
Amount of items: 2
Items: 
Size: 723836 Color: 1
Size: 263364 Color: 3

Bin 4452: 12839 of cap free
Amount of items: 3
Items: 
Size: 481793 Color: 3
Size: 324503 Color: 3
Size: 180866 Color: 2

Bin 4453: 14980 of cap free
Amount of items: 2
Items: 
Size: 603086 Color: 0
Size: 381935 Color: 2

Bin 4454: 15030 of cap free
Amount of items: 2
Items: 
Size: 601141 Color: 2
Size: 383830 Color: 3

Bin 4455: 16675 of cap free
Amount of items: 2
Items: 
Size: 507078 Color: 4
Size: 476248 Color: 3

Bin 4456: 18525 of cap free
Amount of items: 2
Items: 
Size: 786920 Color: 0
Size: 194556 Color: 2

Bin 4457: 18744 of cap free
Amount of items: 2
Items: 
Size: 790116 Color: 1
Size: 191141 Color: 4

Bin 4458: 19349 of cap free
Amount of items: 2
Items: 
Size: 632746 Color: 2
Size: 347906 Color: 1

Bin 4459: 19893 of cap free
Amount of items: 2
Items: 
Size: 506954 Color: 4
Size: 473154 Color: 3

Bin 4460: 22656 of cap free
Amount of items: 2
Items: 
Size: 789287 Color: 1
Size: 188058 Color: 2

Bin 4461: 29064 of cap free
Amount of items: 2
Items: 
Size: 592483 Color: 3
Size: 378454 Color: 4

Bin 4462: 32444 of cap free
Amount of items: 2
Items: 
Size: 781626 Color: 4
Size: 185931 Color: 0

Bin 4463: 37175 of cap free
Amount of items: 2
Items: 
Size: 781123 Color: 1
Size: 181703 Color: 2

Bin 4464: 39408 of cap free
Amount of items: 2
Items: 
Size: 698349 Color: 2
Size: 262244 Color: 1

Bin 4465: 41034 of cap free
Amount of items: 2
Items: 
Size: 652172 Color: 4
Size: 306795 Color: 2

Bin 4466: 42766 of cap free
Amount of items: 2
Items: 
Size: 780935 Color: 1
Size: 176300 Color: 2

Bin 4467: 113406 of cap free
Amount of items: 2
Items: 
Size: 692549 Color: 2
Size: 194046 Color: 4

Bin 4468: 121315 of cap free
Amount of items: 3
Items: 
Size: 529274 Color: 3
Size: 175229 Color: 1
Size: 174183 Color: 2

Total size: 4465916901
Total free space: 2087567

