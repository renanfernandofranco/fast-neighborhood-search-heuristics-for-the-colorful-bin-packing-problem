Capicity Bin: 16544
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 8280 Color: 1
Size: 1378 Color: 1
Size: 1376 Color: 1
Size: 1376 Color: 1
Size: 1030 Color: 0
Size: 1022 Color: 1
Size: 890 Color: 0
Size: 864 Color: 0
Size: 328 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 9188 Color: 1
Size: 6824 Color: 0
Size: 332 Color: 0
Size: 200 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9200 Color: 1
Size: 6856 Color: 1
Size: 488 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10054 Color: 1
Size: 5410 Color: 1
Size: 1080 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11148 Color: 0
Size: 4244 Color: 0
Size: 1152 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11728 Color: 1
Size: 4528 Color: 0
Size: 288 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12453 Color: 0
Size: 3789 Color: 1
Size: 302 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12556 Color: 1
Size: 2492 Color: 1
Size: 1496 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12892 Color: 1
Size: 3044 Color: 1
Size: 608 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12912 Color: 0
Size: 3344 Color: 0
Size: 288 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 3270 Color: 0
Size: 370 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 0
Size: 2436 Color: 1
Size: 1192 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13026 Color: 0
Size: 2758 Color: 1
Size: 760 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13142 Color: 0
Size: 2868 Color: 1
Size: 534 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 1
Size: 1840 Color: 0
Size: 1192 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13520 Color: 1
Size: 2710 Color: 1
Size: 314 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14012 Color: 1
Size: 1344 Color: 1
Size: 1188 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 14044 Color: 1
Size: 1964 Color: 1
Size: 536 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 14104 Color: 1
Size: 1360 Color: 1
Size: 1080 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 0
Size: 1524 Color: 0
Size: 824 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14182 Color: 1
Size: 1970 Color: 1
Size: 392 Color: 0

Bin 22: 0 of cap free
Amount of items: 4
Items: 
Size: 14352 Color: 1
Size: 1848 Color: 0
Size: 248 Color: 0
Size: 96 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14379 Color: 1
Size: 1805 Color: 1
Size: 360 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14698 Color: 1
Size: 1522 Color: 1
Size: 324 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14716 Color: 1
Size: 1452 Color: 1
Size: 376 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14748 Color: 1
Size: 1424 Color: 0
Size: 372 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14760 Color: 1
Size: 1556 Color: 1
Size: 228 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 0
Size: 1476 Color: 1
Size: 384 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14780 Color: 1
Size: 1188 Color: 0
Size: 576 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14804 Color: 1
Size: 1088 Color: 1
Size: 652 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14864 Color: 0
Size: 896 Color: 1
Size: 784 Color: 0

Bin 32: 1 of cap free
Amount of items: 13
Items: 
Size: 8273 Color: 0
Size: 960 Color: 1
Size: 944 Color: 1
Size: 944 Color: 1
Size: 890 Color: 1
Size: 886 Color: 1
Size: 608 Color: 0
Size: 608 Color: 0
Size: 576 Color: 0
Size: 544 Color: 0
Size: 540 Color: 0
Size: 438 Color: 0
Size: 332 Color: 1

Bin 33: 1 of cap free
Amount of items: 7
Items: 
Size: 8284 Color: 1
Size: 1676 Color: 1
Size: 1669 Color: 1
Size: 1650 Color: 1
Size: 1376 Color: 0
Size: 1376 Color: 0
Size: 512 Color: 0

Bin 34: 1 of cap free
Amount of items: 7
Items: 
Size: 8304 Color: 1
Size: 1819 Color: 1
Size: 1808 Color: 1
Size: 1500 Color: 0
Size: 1390 Color: 0
Size: 1360 Color: 1
Size: 362 Color: 0

Bin 35: 1 of cap free
Amount of items: 7
Items: 
Size: 8328 Color: 0
Size: 1940 Color: 1
Size: 1856 Color: 1
Size: 1855 Color: 1
Size: 1512 Color: 0
Size: 752 Color: 0
Size: 300 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 11193 Color: 1
Size: 4742 Color: 0
Size: 608 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 11833 Color: 0
Size: 4162 Color: 0
Size: 548 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 1
Size: 2695 Color: 1
Size: 1344 Color: 0

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 13335 Color: 0
Size: 3208 Color: 1

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 13748 Color: 0
Size: 2795 Color: 1

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 13784 Color: 0
Size: 2759 Color: 1

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 14109 Color: 0
Size: 1788 Color: 1
Size: 646 Color: 0

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 14319 Color: 0
Size: 2224 Color: 1

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 14811 Color: 1
Size: 1672 Color: 0
Size: 60 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 11197 Color: 0
Size: 4467 Color: 0
Size: 878 Color: 1

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 0
Size: 3254 Color: 1
Size: 728 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 12590 Color: 0
Size: 2592 Color: 1
Size: 1360 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 1
Size: 3324 Color: 0
Size: 282 Color: 1

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 14042 Color: 1
Size: 1470 Color: 0
Size: 1030 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 1
Size: 1856 Color: 0
Size: 296 Color: 0

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 14557 Color: 0
Size: 1985 Color: 1

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 14744 Color: 1
Size: 1798 Color: 0

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 14782 Color: 1
Size: 1760 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 10357 Color: 1
Size: 5784 Color: 0
Size: 400 Color: 1

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 10417 Color: 0
Size: 6124 Color: 1

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 11971 Color: 0
Size: 4174 Color: 1
Size: 396 Color: 0

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 11975 Color: 1
Size: 4164 Color: 0
Size: 402 Color: 1

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 12080 Color: 0
Size: 4461 Color: 1

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 14566 Color: 1
Size: 1975 Color: 0

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 14649 Color: 0
Size: 1864 Color: 1
Size: 28 Color: 0

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 14723 Color: 1
Size: 1818 Color: 0

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 10740 Color: 1
Size: 5488 Color: 0
Size: 312 Color: 0

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 12445 Color: 1
Size: 3413 Color: 0
Size: 682 Color: 1

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 13662 Color: 0
Size: 2878 Color: 1

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 12106 Color: 1
Size: 4433 Color: 0

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 12118 Color: 1
Size: 4421 Color: 0

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 13701 Color: 1
Size: 2838 Color: 0

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 14150 Color: 1
Size: 2389 Color: 0

Bin 69: 6 of cap free
Amount of items: 3
Items: 
Size: 9078 Color: 1
Size: 6808 Color: 1
Size: 652 Color: 0

Bin 70: 6 of cap free
Amount of items: 4
Items: 
Size: 9086 Color: 0
Size: 6884 Color: 1
Size: 312 Color: 0
Size: 256 Color: 1

Bin 71: 6 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 0
Size: 5414 Color: 1
Size: 352 Color: 1

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 10862 Color: 0
Size: 5676 Color: 1

Bin 73: 6 of cap free
Amount of items: 3
Items: 
Size: 11892 Color: 0
Size: 4016 Color: 1
Size: 630 Color: 1

Bin 74: 6 of cap free
Amount of items: 3
Items: 
Size: 12610 Color: 1
Size: 3524 Color: 0
Size: 404 Color: 1

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 13374 Color: 1
Size: 3164 Color: 0

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 14540 Color: 0
Size: 1998 Color: 1

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 14722 Color: 1
Size: 1816 Color: 0

Bin 78: 7 of cap free
Amount of items: 3
Items: 
Size: 11225 Color: 1
Size: 4648 Color: 0
Size: 664 Color: 1

Bin 79: 8 of cap free
Amount of items: 35
Items: 
Size: 724 Color: 1
Size: 640 Color: 1
Size: 624 Color: 1
Size: 600 Color: 1
Size: 592 Color: 1
Size: 584 Color: 1
Size: 568 Color: 1
Size: 564 Color: 1
Size: 558 Color: 1
Size: 550 Color: 1
Size: 544 Color: 1
Size: 496 Color: 0
Size: 480 Color: 0
Size: 480 Color: 0
Size: 480 Color: 0
Size: 476 Color: 1
Size: 472 Color: 1
Size: 464 Color: 1
Size: 456 Color: 1
Size: 448 Color: 1
Size: 448 Color: 0
Size: 444 Color: 0
Size: 432 Color: 0
Size: 416 Color: 0
Size: 416 Color: 0
Size: 400 Color: 0
Size: 396 Color: 0
Size: 396 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 368 Color: 0
Size: 368 Color: 0
Size: 356 Color: 0
Size: 304 Color: 0
Size: 224 Color: 1

Bin 80: 8 of cap free
Amount of items: 3
Items: 
Size: 11240 Color: 1
Size: 4976 Color: 0
Size: 320 Color: 1

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 0
Size: 3298 Color: 1

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 13904 Color: 1
Size: 2632 Color: 0

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 14240 Color: 0
Size: 2296 Color: 1

Bin 84: 9 of cap free
Amount of items: 11
Items: 
Size: 8277 Color: 1
Size: 1026 Color: 1
Size: 1020 Color: 1
Size: 1008 Color: 1
Size: 992 Color: 1
Size: 960 Color: 1
Size: 682 Color: 0
Size: 672 Color: 0
Size: 648 Color: 0
Size: 626 Color: 0
Size: 624 Color: 0

Bin 85: 9 of cap free
Amount of items: 3
Items: 
Size: 11550 Color: 1
Size: 4457 Color: 0
Size: 528 Color: 1

Bin 86: 9 of cap free
Amount of items: 3
Items: 
Size: 12316 Color: 0
Size: 3927 Color: 1
Size: 292 Color: 0

Bin 87: 9 of cap free
Amount of items: 2
Items: 
Size: 13294 Color: 1
Size: 3241 Color: 0

Bin 88: 9 of cap free
Amount of items: 2
Items: 
Size: 14495 Color: 0
Size: 2040 Color: 1

Bin 89: 10 of cap free
Amount of items: 3
Items: 
Size: 9968 Color: 0
Size: 5121 Color: 1
Size: 1445 Color: 0

Bin 90: 10 of cap free
Amount of items: 2
Items: 
Size: 13046 Color: 0
Size: 3488 Color: 1

Bin 91: 10 of cap free
Amount of items: 2
Items: 
Size: 14163 Color: 1
Size: 2371 Color: 0

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 14308 Color: 1
Size: 2226 Color: 0

Bin 93: 10 of cap free
Amount of items: 4
Items: 
Size: 14800 Color: 1
Size: 1596 Color: 0
Size: 74 Color: 1
Size: 64 Color: 0

Bin 94: 11 of cap free
Amount of items: 3
Items: 
Size: 10353 Color: 1
Size: 5524 Color: 0
Size: 656 Color: 1

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 12889 Color: 1
Size: 3644 Color: 0

Bin 96: 11 of cap free
Amount of items: 2
Items: 
Size: 14131 Color: 1
Size: 2402 Color: 0

Bin 97: 12 of cap free
Amount of items: 3
Items: 
Size: 9356 Color: 1
Size: 6888 Color: 0
Size: 288 Color: 0

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 13911 Color: 0
Size: 2621 Color: 1

Bin 99: 12 of cap free
Amount of items: 2
Items: 
Size: 14363 Color: 1
Size: 2169 Color: 0

Bin 100: 12 of cap free
Amount of items: 4
Items: 
Size: 14392 Color: 1
Size: 1868 Color: 0
Size: 176 Color: 0
Size: 96 Color: 1

Bin 101: 13 of cap free
Amount of items: 3
Items: 
Size: 10401 Color: 0
Size: 5506 Color: 0
Size: 624 Color: 1

Bin 102: 13 of cap free
Amount of items: 2
Items: 
Size: 13915 Color: 1
Size: 2616 Color: 0

Bin 103: 15 of cap free
Amount of items: 2
Items: 
Size: 13372 Color: 1
Size: 3157 Color: 0

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 0
Size: 3816 Color: 1

Bin 105: 18 of cap free
Amount of items: 2
Items: 
Size: 12642 Color: 0
Size: 3884 Color: 1

Bin 106: 18 of cap free
Amount of items: 3
Items: 
Size: 14878 Color: 1
Size: 1616 Color: 0
Size: 32 Color: 1

Bin 107: 19 of cap free
Amount of items: 2
Items: 
Size: 11241 Color: 0
Size: 5284 Color: 1

Bin 108: 19 of cap free
Amount of items: 2
Items: 
Size: 12578 Color: 0
Size: 3947 Color: 1

Bin 109: 19 of cap free
Amount of items: 2
Items: 
Size: 13108 Color: 0
Size: 3417 Color: 1

Bin 110: 20 of cap free
Amount of items: 3
Items: 
Size: 11538 Color: 0
Size: 4730 Color: 1
Size: 256 Color: 0

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 13252 Color: 1
Size: 3272 Color: 0

Bin 112: 21 of cap free
Amount of items: 2
Items: 
Size: 14845 Color: 1
Size: 1678 Color: 0

Bin 113: 23 of cap free
Amount of items: 2
Items: 
Size: 12449 Color: 1
Size: 4072 Color: 0

Bin 114: 24 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 1
Size: 2684 Color: 0

Bin 115: 24 of cap free
Amount of items: 2
Items: 
Size: 14424 Color: 1
Size: 2096 Color: 0

Bin 116: 24 of cap free
Amount of items: 2
Items: 
Size: 14608 Color: 0
Size: 1912 Color: 1

Bin 117: 25 of cap free
Amount of items: 2
Items: 
Size: 14328 Color: 0
Size: 2191 Color: 1

Bin 118: 26 of cap free
Amount of items: 2
Items: 
Size: 13342 Color: 1
Size: 3176 Color: 0

Bin 119: 26 of cap free
Amount of items: 2
Items: 
Size: 14064 Color: 0
Size: 2454 Color: 1

Bin 120: 27 of cap free
Amount of items: 5
Items: 
Size: 8360 Color: 0
Size: 2512 Color: 1
Size: 2260 Color: 1
Size: 1968 Color: 1
Size: 1417 Color: 0

Bin 121: 27 of cap free
Amount of items: 2
Items: 
Size: 13235 Color: 0
Size: 3282 Color: 1

Bin 122: 28 of cap free
Amount of items: 2
Items: 
Size: 10552 Color: 0
Size: 5964 Color: 1

Bin 123: 28 of cap free
Amount of items: 3
Items: 
Size: 11120 Color: 1
Size: 4500 Color: 0
Size: 896 Color: 0

Bin 124: 28 of cap free
Amount of items: 2
Items: 
Size: 12172 Color: 0
Size: 4344 Color: 1

Bin 125: 28 of cap free
Amount of items: 2
Items: 
Size: 13332 Color: 0
Size: 3184 Color: 1

Bin 126: 28 of cap free
Amount of items: 2
Items: 
Size: 13874 Color: 0
Size: 2642 Color: 1

Bin 127: 28 of cap free
Amount of items: 2
Items: 
Size: 14534 Color: 0
Size: 1982 Color: 1

Bin 128: 30 of cap free
Amount of items: 2
Items: 
Size: 11353 Color: 1
Size: 5161 Color: 0

Bin 129: 30 of cap free
Amount of items: 2
Items: 
Size: 14276 Color: 1
Size: 2238 Color: 0

Bin 130: 33 of cap free
Amount of items: 2
Items: 
Size: 14480 Color: 1
Size: 2031 Color: 0

Bin 131: 38 of cap free
Amount of items: 2
Items: 
Size: 13602 Color: 1
Size: 2904 Color: 0

Bin 132: 39 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 0
Size: 4401 Color: 1

Bin 133: 40 of cap free
Amount of items: 2
Items: 
Size: 11424 Color: 0
Size: 5080 Color: 1

Bin 134: 40 of cap free
Amount of items: 2
Items: 
Size: 13232 Color: 0
Size: 3272 Color: 1

Bin 135: 40 of cap free
Amount of items: 2
Items: 
Size: 14192 Color: 1
Size: 2312 Color: 0

Bin 136: 40 of cap free
Amount of items: 2
Items: 
Size: 14220 Color: 0
Size: 2284 Color: 1

Bin 137: 40 of cap free
Amount of items: 2
Items: 
Size: 14552 Color: 1
Size: 1952 Color: 0

Bin 138: 41 of cap free
Amount of items: 2
Items: 
Size: 11556 Color: 0
Size: 4947 Color: 1

Bin 139: 42 of cap free
Amount of items: 2
Items: 
Size: 9608 Color: 1
Size: 6894 Color: 0

Bin 140: 42 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 0
Size: 2596 Color: 1

Bin 141: 43 of cap free
Amount of items: 2
Items: 
Size: 13826 Color: 1
Size: 2675 Color: 0

Bin 142: 44 of cap free
Amount of items: 2
Items: 
Size: 9420 Color: 0
Size: 7080 Color: 1

Bin 143: 45 of cap free
Amount of items: 3
Items: 
Size: 8392 Color: 0
Size: 6588 Color: 1
Size: 1519 Color: 0

Bin 144: 46 of cap free
Amount of items: 2
Items: 
Size: 14166 Color: 0
Size: 2332 Color: 1

Bin 145: 46 of cap free
Amount of items: 2
Items: 
Size: 14636 Color: 0
Size: 1862 Color: 1

Bin 146: 47 of cap free
Amount of items: 2
Items: 
Size: 11336 Color: 1
Size: 5161 Color: 0

Bin 147: 48 of cap free
Amount of items: 2
Items: 
Size: 10368 Color: 1
Size: 6128 Color: 0

Bin 148: 50 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 0
Size: 3718 Color: 1

Bin 149: 52 of cap free
Amount of items: 2
Items: 
Size: 9868 Color: 0
Size: 6624 Color: 1

Bin 150: 52 of cap free
Amount of items: 2
Items: 
Size: 13436 Color: 1
Size: 3056 Color: 0

Bin 151: 54 of cap free
Amount of items: 14
Items: 
Size: 8274 Color: 1
Size: 882 Color: 1
Size: 800 Color: 1
Size: 768 Color: 1
Size: 736 Color: 1
Size: 736 Color: 1
Size: 656 Color: 1
Size: 538 Color: 0
Size: 528 Color: 0
Size: 528 Color: 0
Size: 524 Color: 0
Size: 512 Color: 0
Size: 512 Color: 0
Size: 496 Color: 0

Bin 152: 54 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 1
Size: 2084 Color: 0

Bin 153: 54 of cap free
Amount of items: 2
Items: 
Size: 14404 Color: 0
Size: 2086 Color: 1

Bin 154: 57 of cap free
Amount of items: 2
Items: 
Size: 13943 Color: 1
Size: 2544 Color: 0

Bin 155: 58 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 0
Size: 2934 Color: 1

Bin 156: 60 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 1
Size: 4812 Color: 0

Bin 157: 60 of cap free
Amount of items: 2
Items: 
Size: 12756 Color: 1
Size: 3728 Color: 0

Bin 158: 63 of cap free
Amount of items: 2
Items: 
Size: 11976 Color: 0
Size: 4505 Color: 1

Bin 159: 69 of cap free
Amount of items: 2
Items: 
Size: 14464 Color: 1
Size: 2011 Color: 0

Bin 160: 72 of cap free
Amount of items: 2
Items: 
Size: 10608 Color: 1
Size: 5864 Color: 0

Bin 161: 74 of cap free
Amount of items: 2
Items: 
Size: 10870 Color: 0
Size: 5600 Color: 1

Bin 162: 78 of cap free
Amount of items: 2
Items: 
Size: 12657 Color: 1
Size: 3809 Color: 0

Bin 163: 84 of cap free
Amount of items: 2
Items: 
Size: 11460 Color: 1
Size: 5000 Color: 0

Bin 164: 85 of cap free
Amount of items: 2
Items: 
Size: 12757 Color: 0
Size: 3702 Color: 1

Bin 165: 85 of cap free
Amount of items: 2
Items: 
Size: 14264 Color: 1
Size: 2195 Color: 0

Bin 166: 87 of cap free
Amount of items: 2
Items: 
Size: 13191 Color: 0
Size: 3266 Color: 1

Bin 167: 88 of cap free
Amount of items: 2
Items: 
Size: 12752 Color: 1
Size: 3704 Color: 0

Bin 168: 92 of cap free
Amount of items: 2
Items: 
Size: 10456 Color: 0
Size: 5996 Color: 1

Bin 169: 96 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 1
Size: 3704 Color: 0

Bin 170: 96 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 1
Size: 2644 Color: 0

Bin 171: 97 of cap free
Amount of items: 2
Items: 
Size: 13400 Color: 1
Size: 3047 Color: 0

Bin 172: 97 of cap free
Amount of items: 2
Items: 
Size: 13679 Color: 1
Size: 2768 Color: 0

Bin 173: 100 of cap free
Amount of items: 9
Items: 
Size: 8276 Color: 0
Size: 1240 Color: 1
Size: 1224 Color: 1
Size: 1216 Color: 1
Size: 1184 Color: 1
Size: 840 Color: 0
Size: 832 Color: 0
Size: 832 Color: 0
Size: 800 Color: 0

Bin 174: 102 of cap free
Amount of items: 2
Items: 
Size: 11285 Color: 1
Size: 5157 Color: 0

Bin 175: 109 of cap free
Amount of items: 2
Items: 
Size: 14543 Color: 1
Size: 1892 Color: 0

Bin 176: 110 of cap free
Amount of items: 7
Items: 
Size: 8296 Color: 0
Size: 1657 Color: 1
Size: 1581 Color: 1
Size: 1456 Color: 1
Size: 1244 Color: 0
Size: 1128 Color: 0
Size: 1072 Color: 0

Bin 177: 110 of cap free
Amount of items: 2
Items: 
Size: 12626 Color: 0
Size: 3808 Color: 1

Bin 178: 112 of cap free
Amount of items: 2
Items: 
Size: 13064 Color: 0
Size: 3368 Color: 1

Bin 179: 117 of cap free
Amount of items: 2
Items: 
Size: 13399 Color: 1
Size: 3028 Color: 0

Bin 180: 122 of cap free
Amount of items: 2
Items: 
Size: 10204 Color: 1
Size: 6218 Color: 0

Bin 181: 122 of cap free
Amount of items: 2
Items: 
Size: 10858 Color: 0
Size: 5564 Color: 1

Bin 182: 140 of cap free
Amount of items: 2
Items: 
Size: 9512 Color: 1
Size: 6892 Color: 0

Bin 183: 146 of cap free
Amount of items: 2
Items: 
Size: 11265 Color: 0
Size: 5133 Color: 1

Bin 184: 154 of cap free
Amount of items: 2
Items: 
Size: 9196 Color: 0
Size: 7194 Color: 1

Bin 185: 156 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 0
Size: 2078 Color: 1

Bin 186: 180 of cap free
Amount of items: 2
Items: 
Size: 11257 Color: 1
Size: 5107 Color: 0

Bin 187: 185 of cap free
Amount of items: 2
Items: 
Size: 13311 Color: 0
Size: 3048 Color: 1

Bin 188: 188 of cap free
Amount of items: 10
Items: 
Size: 8278 Color: 1
Size: 1168 Color: 1
Size: 1112 Color: 1
Size: 1056 Color: 1
Size: 1032 Color: 1
Size: 782 Color: 0
Size: 752 Color: 0
Size: 736 Color: 0
Size: 736 Color: 0
Size: 704 Color: 0

Bin 189: 200 of cap free
Amount of items: 2
Items: 
Size: 9740 Color: 1
Size: 6604 Color: 0

Bin 190: 210 of cap free
Amount of items: 2
Items: 
Size: 10385 Color: 0
Size: 5949 Color: 1

Bin 191: 226 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 1
Size: 2518 Color: 0

Bin 192: 238 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 1
Size: 3690 Color: 0

Bin 193: 239 of cap free
Amount of items: 2
Items: 
Size: 9409 Color: 0
Size: 6896 Color: 1

Bin 194: 240 of cap free
Amount of items: 2
Items: 
Size: 13556 Color: 1
Size: 2748 Color: 0

Bin 195: 246 of cap free
Amount of items: 2
Items: 
Size: 9407 Color: 1
Size: 6891 Color: 0

Bin 196: 246 of cap free
Amount of items: 2
Items: 
Size: 10351 Color: 0
Size: 5947 Color: 1

Bin 197: 263 of cap free
Amount of items: 2
Items: 
Size: 9388 Color: 0
Size: 6893 Color: 1

Bin 198: 272 of cap free
Amount of items: 2
Items: 
Size: 10050 Color: 0
Size: 6222 Color: 1

Bin 199: 8414 of cap free
Amount of items: 24
Items: 
Size: 448 Color: 1
Size: 438 Color: 1
Size: 416 Color: 1
Size: 416 Color: 1
Size: 384 Color: 1
Size: 384 Color: 1
Size: 368 Color: 1
Size: 352 Color: 1
Size: 352 Color: 0
Size: 336 Color: 0
Size: 336 Color: 0
Size: 330 Color: 1
Size: 328 Color: 0
Size: 320 Color: 0
Size: 320 Color: 0
Size: 320 Color: 0
Size: 304 Color: 0
Size: 302 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 280 Color: 1
Size: 276 Color: 1
Size: 256 Color: 1

Total size: 3275712
Total free space: 16544

