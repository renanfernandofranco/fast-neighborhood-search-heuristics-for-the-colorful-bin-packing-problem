Capicity Bin: 2064
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 832 Color: 11
Size: 462 Color: 9
Size: 378 Color: 6
Size: 324 Color: 4
Size: 68 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 14
Size: 618 Color: 13
Size: 120 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 13
Size: 476 Color: 17
Size: 42 Color: 13

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 1738 Color: 19
Size: 202 Color: 12
Size: 64 Color: 11
Size: 44 Color: 3
Size: 16 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 16
Size: 244 Color: 4
Size: 40 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 15
Size: 186 Color: 16
Size: 52 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 14
Size: 362 Color: 15
Size: 232 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 2
Size: 851 Color: 8
Size: 168 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 19
Size: 206 Color: 13
Size: 40 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 9
Size: 226 Color: 0
Size: 44 Color: 2

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 1842 Color: 4
Size: 92 Color: 11
Size: 84 Color: 15
Size: 46 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1703 Color: 19
Size: 301 Color: 7
Size: 60 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 4
Size: 625 Color: 14
Size: 32 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 18
Size: 534 Color: 16
Size: 104 Color: 16

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1193 Color: 3
Size: 793 Color: 11
Size: 78 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 0
Size: 274 Color: 11
Size: 156 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1615 Color: 12
Size: 375 Color: 14
Size: 74 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 11
Size: 237 Color: 15
Size: 46 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 2
Size: 737 Color: 4
Size: 146 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 3
Size: 431 Color: 14
Size: 84 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 6
Size: 202 Color: 14
Size: 40 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 2
Size: 258 Color: 9
Size: 48 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 4
Size: 211 Color: 2
Size: 76 Color: 15

Bin 24: 0 of cap free
Amount of items: 4
Items: 
Size: 1424 Color: 13
Size: 428 Color: 2
Size: 168 Color: 9
Size: 44 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1041 Color: 18
Size: 853 Color: 6
Size: 170 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 17
Size: 394 Color: 0
Size: 76 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1427 Color: 1
Size: 531 Color: 19
Size: 106 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 14
Size: 331 Color: 9
Size: 64 Color: 14

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 5
Size: 257 Color: 10
Size: 34 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 19
Size: 678 Color: 7
Size: 132 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 10
Size: 182 Color: 4
Size: 36 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 6
Size: 389 Color: 8
Size: 48 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 15
Size: 293 Color: 15
Size: 58 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 16
Size: 434 Color: 2
Size: 60 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 4
Size: 341 Color: 9
Size: 68 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 15
Size: 223 Color: 13
Size: 44 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 6
Size: 244 Color: 6
Size: 44 Color: 5

Bin 38: 0 of cap free
Amount of items: 4
Items: 
Size: 1811 Color: 15
Size: 241 Color: 11
Size: 8 Color: 19
Size: 4 Color: 19

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 17
Size: 314 Color: 7
Size: 60 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 17
Size: 857 Color: 17
Size: 170 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1341 Color: 16
Size: 603 Color: 14
Size: 120 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1217 Color: 9
Size: 717 Color: 9
Size: 130 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1419 Color: 5
Size: 539 Color: 9
Size: 106 Color: 8

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 13
Size: 802 Color: 2
Size: 228 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 0
Size: 505 Color: 4
Size: 100 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 16
Size: 290 Color: 18
Size: 80 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1537 Color: 12
Size: 441 Color: 10
Size: 86 Color: 5

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 9
Size: 457 Color: 2
Size: 90 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 7
Size: 310 Color: 8
Size: 36 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1106 Color: 11
Size: 862 Color: 8
Size: 96 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1296 Color: 19
Size: 696 Color: 16
Size: 72 Color: 14

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 3
Size: 691 Color: 6
Size: 136 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 18
Size: 606 Color: 10
Size: 108 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1611 Color: 10
Size: 379 Color: 9
Size: 74 Color: 11

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1831 Color: 8
Size: 195 Color: 19
Size: 38 Color: 14

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 11
Size: 414 Color: 2
Size: 36 Color: 5

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 5
Size: 261 Color: 8
Size: 52 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 2
Size: 461 Color: 16
Size: 90 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1033 Color: 0
Size: 861 Color: 19
Size: 170 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1839 Color: 14
Size: 219 Color: 7
Size: 6 Color: 14

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 18
Size: 498 Color: 5
Size: 56 Color: 9

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 15
Size: 303 Color: 15
Size: 44 Color: 5

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 14
Size: 271 Color: 18
Size: 52 Color: 18

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 12
Size: 750 Color: 4
Size: 148 Color: 8

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1315 Color: 2
Size: 625 Color: 1
Size: 124 Color: 8

Total size: 134160
Total free space: 0

