Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 408547 Color: 0
Size: 302656 Color: 1
Size: 288798 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 377625 Color: 17
Size: 335835 Color: 12
Size: 286541 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 473871 Color: 5
Size: 270062 Color: 18
Size: 256068 Color: 8

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 422234 Color: 3
Size: 325840 Color: 0
Size: 251927 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 408549 Color: 17
Size: 309544 Color: 0
Size: 281908 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 390492 Color: 10
Size: 355025 Color: 15
Size: 254484 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 470865 Color: 15
Size: 269963 Color: 4
Size: 259173 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 381863 Color: 19
Size: 340319 Color: 15
Size: 277819 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 454745 Color: 12
Size: 287491 Color: 9
Size: 257765 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 402478 Color: 2
Size: 323980 Color: 15
Size: 273543 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 398826 Color: 0
Size: 326934 Color: 8
Size: 274241 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 467615 Color: 13
Size: 280246 Color: 9
Size: 252140 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 383739 Color: 3
Size: 331243 Color: 15
Size: 285019 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 412817 Color: 13
Size: 332118 Color: 9
Size: 255066 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 384769 Color: 14
Size: 315886 Color: 8
Size: 299346 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 355136 Color: 5
Size: 329543 Color: 19
Size: 315322 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 392802 Color: 18
Size: 346628 Color: 6
Size: 260571 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 388979 Color: 4
Size: 316328 Color: 19
Size: 294694 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 419488 Color: 17
Size: 307692 Color: 1
Size: 272821 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 420906 Color: 11
Size: 298198 Color: 7
Size: 280897 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 369183 Color: 3
Size: 339046 Color: 8
Size: 291772 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 470918 Color: 6
Size: 271967 Color: 4
Size: 257116 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 357006 Color: 8
Size: 340082 Color: 11
Size: 302913 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 359111 Color: 9
Size: 327014 Color: 2
Size: 313876 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 497752 Color: 12
Size: 252139 Color: 9
Size: 250110 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 371107 Color: 6
Size: 323364 Color: 4
Size: 305530 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 434453 Color: 19
Size: 285144 Color: 14
Size: 280404 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 365189 Color: 13
Size: 322927 Color: 10
Size: 311885 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 385788 Color: 12
Size: 311646 Color: 19
Size: 302567 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 356502 Color: 14
Size: 345936 Color: 16
Size: 297563 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 448964 Color: 9
Size: 283425 Color: 8
Size: 267612 Color: 11

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 357409 Color: 17
Size: 346004 Color: 10
Size: 296588 Color: 11

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 431589 Color: 4
Size: 292328 Color: 13
Size: 276084 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 437054 Color: 18
Size: 288055 Color: 2
Size: 274892 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 493690 Color: 1
Size: 254472 Color: 18
Size: 251839 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 404796 Color: 18
Size: 312704 Color: 7
Size: 282501 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 481183 Color: 5
Size: 268243 Color: 13
Size: 250575 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 436452 Color: 4
Size: 283317 Color: 3
Size: 280232 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 401705 Color: 1
Size: 334950 Color: 18
Size: 263346 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 362741 Color: 18
Size: 335691 Color: 16
Size: 301569 Color: 10

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 401297 Color: 13
Size: 345081 Color: 14
Size: 253623 Color: 10

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 396293 Color: 10
Size: 323868 Color: 13
Size: 279840 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 497407 Color: 10
Size: 252181 Color: 2
Size: 250413 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 436274 Color: 5
Size: 297707 Color: 17
Size: 266020 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 424367 Color: 18
Size: 292880 Color: 4
Size: 282754 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 414434 Color: 7
Size: 327600 Color: 16
Size: 257967 Color: 6

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 498240 Color: 10
Size: 251126 Color: 5
Size: 250635 Color: 17

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 455564 Color: 17
Size: 291069 Color: 14
Size: 253368 Color: 16

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 352042 Color: 9
Size: 328537 Color: 13
Size: 319422 Color: 11

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 428390 Color: 19
Size: 302800 Color: 0
Size: 268811 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 463616 Color: 3
Size: 268611 Color: 10
Size: 267774 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 438022 Color: 4
Size: 284405 Color: 13
Size: 277574 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 377393 Color: 16
Size: 336347 Color: 15
Size: 286261 Color: 17

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 367560 Color: 0
Size: 337372 Color: 18
Size: 295069 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 408639 Color: 8
Size: 310593 Color: 15
Size: 280769 Color: 10

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 479145 Color: 9
Size: 261067 Color: 15
Size: 259789 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 445877 Color: 15
Size: 285063 Color: 14
Size: 269061 Color: 10

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 393645 Color: 1
Size: 325426 Color: 11
Size: 280930 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 465353 Color: 6
Size: 278462 Color: 11
Size: 256186 Color: 14

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 338259 Color: 9
Size: 338233 Color: 12
Size: 323509 Color: 8

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 472471 Color: 19
Size: 269079 Color: 12
Size: 258451 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 436165 Color: 17
Size: 291661 Color: 15
Size: 272175 Color: 16

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 439852 Color: 15
Size: 302613 Color: 17
Size: 257536 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 352013 Color: 19
Size: 344241 Color: 1
Size: 303747 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 373662 Color: 4
Size: 363130 Color: 16
Size: 263209 Color: 16

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 362972 Color: 19
Size: 355969 Color: 18
Size: 281060 Color: 7

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 363048 Color: 14
Size: 342811 Color: 7
Size: 294142 Color: 11

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 367017 Color: 7
Size: 352331 Color: 10
Size: 280653 Color: 5

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 369089 Color: 7
Size: 355357 Color: 18
Size: 275555 Color: 18

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 338651 Color: 14
Size: 336456 Color: 4
Size: 324894 Color: 10

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 348241 Color: 10
Size: 345670 Color: 8
Size: 306090 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 353775 Color: 2
Size: 333221 Color: 8
Size: 313005 Color: 15

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 372236 Color: 13
Size: 318850 Color: 4
Size: 308915 Color: 14

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 372691 Color: 0
Size: 315639 Color: 0
Size: 311671 Color: 9

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 371802 Color: 5
Size: 356107 Color: 3
Size: 272092 Color: 17

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 373747 Color: 2
Size: 349665 Color: 10
Size: 276589 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 356467 Color: 14
Size: 322832 Color: 2
Size: 320702 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 374028 Color: 9
Size: 354060 Color: 16
Size: 271913 Color: 7

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 465330 Color: 16
Size: 268294 Color: 0
Size: 266377 Color: 13

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 350121 Color: 16
Size: 330655 Color: 17
Size: 319225 Color: 15

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 375684 Color: 8
Size: 345336 Color: 13
Size: 278981 Color: 6

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 386717 Color: 6
Size: 310804 Color: 6
Size: 302480 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 403595 Color: 1
Size: 345429 Color: 15
Size: 250977 Color: 19

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 366878 Color: 16
Size: 350974 Color: 9
Size: 282149 Color: 19

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 352188 Color: 7
Size: 324056 Color: 1
Size: 323757 Color: 18

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 366758 Color: 8
Size: 358490 Color: 12
Size: 274753 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 359700 Color: 1
Size: 326364 Color: 10
Size: 313937 Color: 14

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 360779 Color: 4
Size: 339286 Color: 17
Size: 299936 Color: 15

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 358826 Color: 16
Size: 340990 Color: 13
Size: 300185 Color: 10

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 361930 Color: 3
Size: 340066 Color: 5
Size: 298005 Color: 4

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 346850 Color: 7
Size: 344328 Color: 0
Size: 308823 Color: 9

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 371543 Color: 17
Size: 320100 Color: 7
Size: 308358 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 370496 Color: 15
Size: 337900 Color: 17
Size: 291605 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 351673 Color: 1
Size: 325015 Color: 4
Size: 323313 Color: 9

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 349165 Color: 12
Size: 327820 Color: 18
Size: 323016 Color: 19

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 369912 Color: 0
Size: 369112 Color: 10
Size: 260977 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 368984 Color: 8
Size: 355232 Color: 8
Size: 275785 Color: 5

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 369469 Color: 3
Size: 364675 Color: 14
Size: 265857 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 351021 Color: 13
Size: 330518 Color: 14
Size: 318462 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 371983 Color: 6
Size: 361273 Color: 0
Size: 266745 Color: 18

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 373168 Color: 2
Size: 354265 Color: 14
Size: 272568 Color: 6

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 373432 Color: 17
Size: 342362 Color: 10
Size: 284207 Color: 10

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 383819 Color: 19
Size: 362124 Color: 6
Size: 254058 Color: 14

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 397753 Color: 8
Size: 350459 Color: 6
Size: 251789 Color: 13

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 350211 Color: 4
Size: 334191 Color: 15
Size: 315599 Color: 13

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 349994 Color: 10
Size: 342189 Color: 16
Size: 307818 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 353598 Color: 13
Size: 323655 Color: 17
Size: 322748 Color: 10

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 360894 Color: 18
Size: 331122 Color: 12
Size: 307985 Color: 5

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 367336 Color: 17
Size: 356432 Color: 18
Size: 276233 Color: 13

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 351913 Color: 14
Size: 327042 Color: 11
Size: 321046 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 347570 Color: 18
Size: 344149 Color: 9
Size: 308282 Color: 14

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 366408 Color: 4
Size: 354040 Color: 18
Size: 279553 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 367609 Color: 3
Size: 362351 Color: 5
Size: 270041 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 364384 Color: 3
Size: 356276 Color: 18
Size: 279341 Color: 17

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 354966 Color: 8
Size: 352814 Color: 2
Size: 292221 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 364681 Color: 1
Size: 318865 Color: 13
Size: 316455 Color: 13

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 341227 Color: 10
Size: 338587 Color: 17
Size: 320187 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 359471 Color: 17
Size: 324293 Color: 8
Size: 316237 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 363585 Color: 18
Size: 358433 Color: 11
Size: 277983 Color: 13

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 355492 Color: 0
Size: 332777 Color: 4
Size: 311732 Color: 2

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 364395 Color: 1
Size: 340888 Color: 2
Size: 294718 Color: 7

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 352732 Color: 14
Size: 348842 Color: 2
Size: 298427 Color: 9

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 349396 Color: 18
Size: 330774 Color: 2
Size: 319831 Color: 4

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 364089 Color: 1
Size: 363908 Color: 6
Size: 272004 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 355754 Color: 19
Size: 346489 Color: 8
Size: 297758 Color: 14

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 355818 Color: 6
Size: 328851 Color: 16
Size: 315332 Color: 14

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 361497 Color: 13
Size: 320744 Color: 1
Size: 317760 Color: 14

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 373175 Color: 9
Size: 344853 Color: 14
Size: 281973 Color: 6

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 354408 Color: 17
Size: 347255 Color: 10
Size: 298338 Color: 14

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 361271 Color: 17
Size: 360196 Color: 13
Size: 278534 Color: 6

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 358482 Color: 9
Size: 349612 Color: 5
Size: 291907 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 356815 Color: 5
Size: 327121 Color: 3
Size: 316065 Color: 4

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 357462 Color: 6
Size: 357385 Color: 2
Size: 285154 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 355447 Color: 11
Size: 330659 Color: 8
Size: 313895 Color: 10

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 362717 Color: 9
Size: 346397 Color: 8
Size: 290887 Color: 18

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 359579 Color: 6
Size: 354855 Color: 9
Size: 285567 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 340259 Color: 17
Size: 332815 Color: 4
Size: 326927 Color: 17

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 351283 Color: 3
Size: 339312 Color: 0
Size: 309406 Color: 9

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 374061 Color: 8
Size: 313281 Color: 16
Size: 312659 Color: 9

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 366981 Color: 4
Size: 358410 Color: 3
Size: 274610 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 375138 Color: 16
Size: 318043 Color: 7
Size: 306820 Color: 16

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 352666 Color: 12
Size: 325194 Color: 13
Size: 322141 Color: 5

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 348345 Color: 7
Size: 328203 Color: 12
Size: 323453 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 362122 Color: 0
Size: 352069 Color: 2
Size: 285810 Color: 7

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 355322 Color: 17
Size: 330791 Color: 2
Size: 313888 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 355915 Color: 7
Size: 341588 Color: 0
Size: 302498 Color: 7

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 351037 Color: 16
Size: 341055 Color: 14
Size: 307909 Color: 18

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 371164 Color: 0
Size: 358410 Color: 4
Size: 270427 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 353719 Color: 10
Size: 336436 Color: 19
Size: 309846 Color: 16

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 364799 Color: 4
Size: 359034 Color: 6
Size: 276168 Color: 2

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 358765 Color: 13
Size: 330239 Color: 11
Size: 310997 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 361809 Color: 6
Size: 332501 Color: 4
Size: 305691 Color: 11

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 357370 Color: 12
Size: 343204 Color: 7
Size: 299427 Color: 7

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 355771 Color: 10
Size: 334685 Color: 5
Size: 309545 Color: 16

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 351264 Color: 6
Size: 337359 Color: 18
Size: 311378 Color: 8

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 345774 Color: 0
Size: 327127 Color: 1
Size: 327100 Color: 10

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 353894 Color: 15
Size: 324834 Color: 17
Size: 321273 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 358546 Color: 11
Size: 327791 Color: 9
Size: 313664 Color: 19

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 345189 Color: 3
Size: 332068 Color: 18
Size: 322744 Color: 12

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 334588 Color: 12
Size: 332914 Color: 4
Size: 332499 Color: 4

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 357433 Color: 5
Size: 323539 Color: 8
Size: 319029 Color: 3

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 354901 Color: 0
Size: 342474 Color: 14
Size: 302626 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 360582 Color: 2
Size: 345023 Color: 1
Size: 294396 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 356815 Color: 13
Size: 333524 Color: 15
Size: 309662 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 339453 Color: 9
Size: 330722 Color: 16
Size: 329826 Color: 5

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 349496 Color: 6
Size: 333330 Color: 9
Size: 317175 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 353903 Color: 12
Size: 340685 Color: 19
Size: 305413 Color: 9

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 345930 Color: 19
Size: 337048 Color: 13
Size: 317023 Color: 6

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 357157 Color: 19
Size: 326036 Color: 11
Size: 316808 Color: 7

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 351002 Color: 6
Size: 326097 Color: 10
Size: 322902 Color: 4

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 349431 Color: 17
Size: 333956 Color: 18
Size: 316614 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 359191 Color: 10
Size: 328280 Color: 7
Size: 312530 Color: 15

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 350863 Color: 18
Size: 333500 Color: 15
Size: 315638 Color: 6

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 347724 Color: 4
Size: 345335 Color: 17
Size: 306942 Color: 15

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 415590 Color: 19
Size: 323167 Color: 16
Size: 261244 Color: 18

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 357316 Color: 7
Size: 338019 Color: 0
Size: 304666 Color: 9

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 349372 Color: 1
Size: 326525 Color: 3
Size: 324104 Color: 4

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 345302 Color: 17
Size: 332737 Color: 2
Size: 321962 Color: 15

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 337847 Color: 16
Size: 332235 Color: 9
Size: 329919 Color: 3

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 334840 Color: 9
Size: 333738 Color: 0
Size: 331423 Color: 13

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 361907 Color: 15
Size: 320641 Color: 2
Size: 317453 Color: 9

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 427819 Color: 6
Size: 302619 Color: 0
Size: 269563 Color: 12

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 344065 Color: 8
Size: 333918 Color: 13
Size: 322018 Color: 8

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 352638 Color: 4
Size: 338821 Color: 17
Size: 308542 Color: 7

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 354073 Color: 4
Size: 348249 Color: 15
Size: 297679 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 348268 Color: 17
Size: 334008 Color: 5
Size: 317725 Color: 18

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 356684 Color: 15
Size: 349342 Color: 12
Size: 293975 Color: 19

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 355117 Color: 4
Size: 326834 Color: 15
Size: 318050 Color: 9

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 366348 Color: 19
Size: 356128 Color: 4
Size: 277525 Color: 7

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 375362 Color: 3
Size: 364800 Color: 19
Size: 259839 Color: 12

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 365477 Color: 8
Size: 350971 Color: 11
Size: 283553 Color: 12

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 373768 Color: 3
Size: 361179 Color: 17
Size: 265054 Color: 3

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 343316 Color: 1
Size: 338825 Color: 14
Size: 317860 Color: 19

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 358399 Color: 14
Size: 355896 Color: 10
Size: 285706 Color: 15

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 356228 Color: 6
Size: 350389 Color: 12
Size: 293384 Color: 3

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 362743 Color: 17
Size: 360578 Color: 1
Size: 276680 Color: 11

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 359196 Color: 13
Size: 355959 Color: 3
Size: 284846 Color: 18

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 351669 Color: 18
Size: 348046 Color: 19
Size: 300286 Color: 17

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 371953 Color: 11
Size: 348419 Color: 18
Size: 279629 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 354409 Color: 4
Size: 323033 Color: 17
Size: 322559 Color: 2

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 358196 Color: 15
Size: 356569 Color: 13
Size: 285236 Color: 2

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 374254 Color: 14
Size: 353777 Color: 10
Size: 271970 Color: 12

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 355317 Color: 16
Size: 354069 Color: 3
Size: 290615 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 353826 Color: 11
Size: 348951 Color: 0
Size: 297224 Color: 18

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 361942 Color: 13
Size: 353396 Color: 14
Size: 284663 Color: 3

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 352730 Color: 16
Size: 352619 Color: 16
Size: 294652 Color: 11

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 360827 Color: 4
Size: 356033 Color: 9
Size: 283141 Color: 17

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 377914 Color: 14
Size: 358617 Color: 8
Size: 263470 Color: 8

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 357014 Color: 1
Size: 356689 Color: 18
Size: 286298 Color: 5

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 393199 Color: 10
Size: 310066 Color: 9
Size: 296736 Color: 12

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 372383 Color: 0
Size: 360451 Color: 7
Size: 267167 Color: 3

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 350711 Color: 11
Size: 350452 Color: 8
Size: 298838 Color: 9

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 356307 Color: 13
Size: 348899 Color: 6
Size: 294795 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 346089 Color: 3
Size: 331916 Color: 7
Size: 321996 Color: 13

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 353469 Color: 10
Size: 348314 Color: 16
Size: 298218 Color: 2

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 348753 Color: 19
Size: 342135 Color: 1
Size: 309113 Color: 8

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 353055 Color: 14
Size: 348941 Color: 0
Size: 298005 Color: 19

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 346378 Color: 17
Size: 330786 Color: 0
Size: 322837 Color: 4

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 383279 Color: 11
Size: 349186 Color: 10
Size: 267536 Color: 18

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 354214 Color: 0
Size: 348882 Color: 10
Size: 296905 Color: 6

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 352850 Color: 0
Size: 348425 Color: 18
Size: 298726 Color: 6

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 348167 Color: 6
Size: 347843 Color: 6
Size: 303991 Color: 12

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 358913 Color: 16
Size: 348134 Color: 11
Size: 292954 Color: 17

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 361775 Color: 9
Size: 347319 Color: 15
Size: 290907 Color: 7

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 359801 Color: 12
Size: 346608 Color: 19
Size: 293592 Color: 10

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 353404 Color: 6
Size: 346367 Color: 19
Size: 300230 Color: 11

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 347907 Color: 1
Size: 346903 Color: 3
Size: 305191 Color: 19

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 345405 Color: 8
Size: 340522 Color: 1
Size: 314074 Color: 8

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 363671 Color: 12
Size: 345817 Color: 15
Size: 290513 Color: 8

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 361317 Color: 12
Size: 348581 Color: 18
Size: 290103 Color: 18

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 374009 Color: 7
Size: 344777 Color: 19
Size: 281215 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 341908 Color: 19
Size: 331600 Color: 4
Size: 326493 Color: 12

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 365383 Color: 14
Size: 347370 Color: 3
Size: 287248 Color: 3

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 350021 Color: 1
Size: 347697 Color: 3
Size: 302283 Color: 6

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 344099 Color: 12
Size: 334231 Color: 5
Size: 321671 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 348175 Color: 15
Size: 347247 Color: 13
Size: 304579 Color: 11

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 345936 Color: 4
Size: 327348 Color: 0
Size: 326717 Color: 17

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 363722 Color: 7
Size: 346480 Color: 3
Size: 289799 Color: 11

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 367386 Color: 19
Size: 345733 Color: 6
Size: 286882 Color: 9

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 355007 Color: 8
Size: 345766 Color: 6
Size: 299228 Color: 2

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 344802 Color: 13
Size: 341980 Color: 18
Size: 313219 Color: 8

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 348018 Color: 18
Size: 345219 Color: 9
Size: 306764 Color: 12

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 344799 Color: 19
Size: 336677 Color: 1
Size: 318525 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 345157 Color: 16
Size: 341141 Color: 14
Size: 313703 Color: 17

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 344655 Color: 18
Size: 332467 Color: 8
Size: 322879 Color: 3

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 345497 Color: 12
Size: 345134 Color: 2
Size: 309370 Color: 7

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 346746 Color: 17
Size: 344396 Color: 19
Size: 308859 Color: 11

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 344792 Color: 10
Size: 344401 Color: 16
Size: 310808 Color: 5

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 353731 Color: 0
Size: 344629 Color: 0
Size: 301641 Color: 8

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 344444 Color: 10
Size: 331138 Color: 13
Size: 324419 Color: 11

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 369128 Color: 16
Size: 364180 Color: 12
Size: 266693 Color: 5

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 350007 Color: 5
Size: 347917 Color: 10
Size: 302077 Color: 16

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 352238 Color: 5
Size: 342599 Color: 18
Size: 305164 Color: 8

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 344447 Color: 11
Size: 335357 Color: 5
Size: 320197 Color: 4

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 359500 Color: 16
Size: 322249 Color: 0
Size: 318252 Color: 12

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 353803 Color: 17
Size: 340657 Color: 7
Size: 305541 Color: 18

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 398397 Color: 9
Size: 342537 Color: 4
Size: 259067 Color: 11

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 344304 Color: 2
Size: 342725 Color: 2
Size: 312972 Color: 5

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 348959 Color: 16
Size: 325772 Color: 6
Size: 325270 Color: 16

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 341057 Color: 11
Size: 333965 Color: 1
Size: 324979 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 344943 Color: 0
Size: 341523 Color: 12
Size: 313535 Color: 15

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 366488 Color: 13
Size: 338330 Color: 19
Size: 295183 Color: 5

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 336650 Color: 19
Size: 331811 Color: 13
Size: 331540 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 344620 Color: 5
Size: 340668 Color: 19
Size: 314713 Color: 3

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 340891 Color: 16
Size: 337915 Color: 3
Size: 321195 Color: 5

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 354535 Color: 6
Size: 339032 Color: 11
Size: 306434 Color: 17

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 340630 Color: 6
Size: 335177 Color: 9
Size: 324194 Color: 7

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 336613 Color: 9
Size: 335789 Color: 15
Size: 327599 Color: 10

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 337894 Color: 19
Size: 332505 Color: 10
Size: 329602 Color: 19

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 338854 Color: 13
Size: 338356 Color: 5
Size: 322791 Color: 15

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 406995 Color: 7
Size: 338892 Color: 4
Size: 254114 Color: 2

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 349940 Color: 17
Size: 337089 Color: 11
Size: 312972 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 344089 Color: 17
Size: 336387 Color: 18
Size: 319525 Color: 9

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 354649 Color: 3
Size: 336529 Color: 2
Size: 308823 Color: 13

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 337055 Color: 17
Size: 336792 Color: 18
Size: 326154 Color: 6

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 351921 Color: 1
Size: 334348 Color: 16
Size: 313732 Color: 5

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 352917 Color: 2
Size: 337497 Color: 6
Size: 309587 Color: 13

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 336241 Color: 6
Size: 335985 Color: 14
Size: 327775 Color: 11

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 335169 Color: 6
Size: 333278 Color: 18
Size: 331554 Color: 8

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 344438 Color: 4
Size: 340538 Color: 13
Size: 315025 Color: 2

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 357407 Color: 14
Size: 343500 Color: 4
Size: 299094 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 357078 Color: 19
Size: 337914 Color: 14
Size: 305009 Color: 7

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 357919 Color: 2
Size: 328934 Color: 1
Size: 313148 Color: 2

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 357999 Color: 19
Size: 332868 Color: 18
Size: 309134 Color: 5

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 358601 Color: 19
Size: 332548 Color: 5
Size: 308852 Color: 14

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 359037 Color: 14
Size: 344672 Color: 18
Size: 296292 Color: 9

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 359088 Color: 6
Size: 332817 Color: 8
Size: 308096 Color: 14

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 358836 Color: 13
Size: 321925 Color: 15
Size: 319240 Color: 12

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 359637 Color: 15
Size: 321215 Color: 2
Size: 319149 Color: 6

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 359747 Color: 8
Size: 333828 Color: 19
Size: 306426 Color: 16

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 359976 Color: 1
Size: 337559 Color: 5
Size: 302466 Color: 14

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 359942 Color: 19
Size: 328638 Color: 7
Size: 311421 Color: 8

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 360234 Color: 4
Size: 323640 Color: 4
Size: 316127 Color: 17

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 360429 Color: 15
Size: 329865 Color: 16
Size: 309707 Color: 12

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 360451 Color: 5
Size: 343466 Color: 17
Size: 296084 Color: 12

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 360475 Color: 15
Size: 324103 Color: 4
Size: 315423 Color: 2

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 359860 Color: 18
Size: 353078 Color: 4
Size: 287063 Color: 17

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 360559 Color: 19
Size: 333728 Color: 13
Size: 305714 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 360626 Color: 19
Size: 357486 Color: 8
Size: 281889 Color: 13

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 360719 Color: 17
Size: 332249 Color: 7
Size: 307033 Color: 5

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 360761 Color: 17
Size: 358157 Color: 14
Size: 281083 Color: 5

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 360926 Color: 3
Size: 333149 Color: 6
Size: 305926 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 361147 Color: 5
Size: 347578 Color: 1
Size: 291276 Color: 11

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 360492 Color: 16
Size: 347439 Color: 12
Size: 292070 Color: 17

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 361407 Color: 15
Size: 345462 Color: 9
Size: 293132 Color: 8

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 360842 Color: 19
Size: 342485 Color: 18
Size: 296674 Color: 2

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 361872 Color: 0
Size: 321029 Color: 1
Size: 317100 Color: 7

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 361768 Color: 14
Size: 345623 Color: 15
Size: 292610 Color: 14

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 361786 Color: 15
Size: 320800 Color: 15
Size: 317415 Color: 14

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 361968 Color: 1
Size: 353997 Color: 9
Size: 284036 Color: 14

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 362050 Color: 7
Size: 349748 Color: 4
Size: 288203 Color: 2

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 362167 Color: 11
Size: 344186 Color: 2
Size: 293648 Color: 11

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 362174 Color: 11
Size: 320314 Color: 15
Size: 317513 Color: 5

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 362326 Color: 7
Size: 333524 Color: 2
Size: 304151 Color: 2

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 362326 Color: 6
Size: 319783 Color: 5
Size: 317892 Color: 8

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 362378 Color: 8
Size: 321069 Color: 18
Size: 316554 Color: 15

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 362391 Color: 13
Size: 333432 Color: 16
Size: 304178 Color: 18

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 362429 Color: 19
Size: 361811 Color: 4
Size: 275761 Color: 9

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 361879 Color: 18
Size: 334895 Color: 7
Size: 303227 Color: 2

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 362544 Color: 15
Size: 321045 Color: 17
Size: 316412 Color: 8

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 362593 Color: 7
Size: 330407 Color: 4
Size: 307001 Color: 14

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 362609 Color: 15
Size: 361137 Color: 10
Size: 276255 Color: 19

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 362736 Color: 7
Size: 347535 Color: 6
Size: 289730 Color: 14

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 362762 Color: 3
Size: 343613 Color: 1
Size: 293626 Color: 7

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 362819 Color: 11
Size: 326116 Color: 8
Size: 311066 Color: 7

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 362881 Color: 5
Size: 345075 Color: 15
Size: 292045 Color: 19

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 362922 Color: 12
Size: 322366 Color: 17
Size: 314713 Color: 9

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 362936 Color: 7
Size: 335927 Color: 12
Size: 301138 Color: 8

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 363100 Color: 12
Size: 362194 Color: 3
Size: 274707 Color: 5

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 363197 Color: 15
Size: 319343 Color: 16
Size: 317461 Color: 18

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 362771 Color: 18
Size: 322621 Color: 19
Size: 314609 Color: 13

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 363343 Color: 16
Size: 352188 Color: 13
Size: 284470 Color: 5

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 363380 Color: 11
Size: 332209 Color: 4
Size: 304412 Color: 19

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 363594 Color: 11
Size: 355720 Color: 7
Size: 280687 Color: 4

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 363682 Color: 3
Size: 336377 Color: 1
Size: 299942 Color: 8

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 363201 Color: 19
Size: 321720 Color: 14
Size: 315080 Color: 14

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 363691 Color: 6
Size: 344114 Color: 11
Size: 292196 Color: 9

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 363854 Color: 15
Size: 324364 Color: 2
Size: 311783 Color: 18

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 363912 Color: 10
Size: 322829 Color: 6
Size: 313260 Color: 2

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 364050 Color: 8
Size: 328589 Color: 6
Size: 307362 Color: 10

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 364091 Color: 2
Size: 322163 Color: 2
Size: 313747 Color: 12

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 364103 Color: 14
Size: 331632 Color: 11
Size: 304266 Color: 2

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 364226 Color: 5
Size: 335408 Color: 3
Size: 300367 Color: 13

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 364237 Color: 10
Size: 350405 Color: 14
Size: 285359 Color: 10

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 363489 Color: 19
Size: 342535 Color: 3
Size: 293977 Color: 15

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 364397 Color: 11
Size: 323626 Color: 14
Size: 311978 Color: 14

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 364419 Color: 10
Size: 362581 Color: 5
Size: 273001 Color: 13

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 364434 Color: 3
Size: 345749 Color: 5
Size: 289818 Color: 15

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 364568 Color: 7
Size: 347910 Color: 12
Size: 287523 Color: 15

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 364679 Color: 2
Size: 329824 Color: 17
Size: 305498 Color: 15

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 364687 Color: 15
Size: 337955 Color: 15
Size: 297359 Color: 9

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 364755 Color: 17
Size: 350248 Color: 17
Size: 284998 Color: 14

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 364909 Color: 13
Size: 347569 Color: 5
Size: 287523 Color: 9

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 364918 Color: 10
Size: 341239 Color: 11
Size: 293844 Color: 6

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 365028 Color: 19
Size: 334026 Color: 12
Size: 300947 Color: 9

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 365063 Color: 11
Size: 329752 Color: 16
Size: 305186 Color: 16

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 365109 Color: 19
Size: 330143 Color: 2
Size: 304749 Color: 2

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 365148 Color: 7
Size: 319321 Color: 18
Size: 315532 Color: 5

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 365218 Color: 0
Size: 338314 Color: 11
Size: 296469 Color: 6

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 365243 Color: 12
Size: 329351 Color: 9
Size: 305407 Color: 3

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 365303 Color: 13
Size: 339444 Color: 3
Size: 295254 Color: 6

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 365190 Color: 6
Size: 321215 Color: 18
Size: 313596 Color: 11

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 365417 Color: 15
Size: 326992 Color: 19
Size: 307592 Color: 2

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 365439 Color: 9
Size: 341053 Color: 0
Size: 293509 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 365456 Color: 17
Size: 346632 Color: 10
Size: 287913 Color: 10

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 365459 Color: 11
Size: 335104 Color: 4
Size: 299438 Color: 8

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 365466 Color: 17
Size: 325896 Color: 17
Size: 308639 Color: 3

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 365495 Color: 10
Size: 362433 Color: 6
Size: 272073 Color: 4

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 365521 Color: 13
Size: 326981 Color: 12
Size: 307499 Color: 17

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 365625 Color: 10
Size: 355621 Color: 13
Size: 278755 Color: 4

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 365857 Color: 12
Size: 345904 Color: 1
Size: 288240 Color: 13

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 365768 Color: 19
Size: 321970 Color: 19
Size: 312263 Color: 16

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 365864 Color: 15
Size: 322904 Color: 8
Size: 311233 Color: 2

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 366047 Color: 13
Size: 322829 Color: 15
Size: 311125 Color: 4

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 366130 Color: 18
Size: 349838 Color: 13
Size: 284033 Color: 14

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 366179 Color: 3
Size: 324235 Color: 10
Size: 309587 Color: 11

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 366211 Color: 14
Size: 317482 Color: 17
Size: 316308 Color: 12

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 366265 Color: 6
Size: 338522 Color: 11
Size: 295214 Color: 12

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 366298 Color: 14
Size: 350182 Color: 9
Size: 283521 Color: 16

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 366219 Color: 18
Size: 326184 Color: 18
Size: 307598 Color: 13

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 366480 Color: 13
Size: 363137 Color: 17
Size: 270384 Color: 6

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 366550 Color: 4
Size: 337686 Color: 19
Size: 295765 Color: 12

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 366561 Color: 0
Size: 360677 Color: 8
Size: 272763 Color: 1

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 366568 Color: 15
Size: 318871 Color: 15
Size: 314562 Color: 10

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 366638 Color: 6
Size: 344927 Color: 14
Size: 288436 Color: 16

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 366660 Color: 0
Size: 337043 Color: 9
Size: 296298 Color: 15

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 366676 Color: 12
Size: 330030 Color: 12
Size: 303295 Color: 19

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 366720 Color: 7
Size: 350003 Color: 0
Size: 283278 Color: 9

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 366886 Color: 11
Size: 346170 Color: 10
Size: 286945 Color: 14

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 366916 Color: 15
Size: 363589 Color: 15
Size: 269496 Color: 9

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 366961 Color: 8
Size: 342856 Color: 3
Size: 290184 Color: 14

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 367088 Color: 5
Size: 320875 Color: 0
Size: 312038 Color: 3

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 367135 Color: 4
Size: 323579 Color: 18
Size: 309287 Color: 5

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 367233 Color: 17
Size: 320106 Color: 16
Size: 312662 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 367274 Color: 5
Size: 343454 Color: 11
Size: 289273 Color: 13

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 367432 Color: 4
Size: 331749 Color: 10
Size: 300820 Color: 14

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 367440 Color: 9
Size: 356580 Color: 0
Size: 275981 Color: 5

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 367452 Color: 15
Size: 322514 Color: 7
Size: 310035 Color: 4

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 367132 Color: 13
Size: 349367 Color: 14
Size: 283502 Color: 8

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 367460 Color: 15
Size: 325555 Color: 2
Size: 306986 Color: 12

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 367474 Color: 5
Size: 316940 Color: 16
Size: 315587 Color: 8

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 367521 Color: 11
Size: 335753 Color: 18
Size: 296727 Color: 8

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 367543 Color: 7
Size: 356547 Color: 11
Size: 275911 Color: 19

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 367558 Color: 7
Size: 341113 Color: 17
Size: 291330 Color: 16

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 367857 Color: 8
Size: 350835 Color: 18
Size: 281309 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 367859 Color: 4
Size: 329697 Color: 13
Size: 302445 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 367861 Color: 19
Size: 326526 Color: 17
Size: 305614 Color: 11

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 367572 Color: 13
Size: 321264 Color: 14
Size: 311165 Color: 17

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 367914 Color: 7
Size: 338491 Color: 0
Size: 293596 Color: 2

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 368076 Color: 9
Size: 362491 Color: 16
Size: 269434 Color: 15

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 368081 Color: 0
Size: 322208 Color: 7
Size: 309712 Color: 19

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 368166 Color: 10
Size: 355005 Color: 11
Size: 276830 Color: 18

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 368082 Color: 14
Size: 343426 Color: 15
Size: 288493 Color: 17

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 368282 Color: 0
Size: 340609 Color: 19
Size: 291110 Color: 13

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 368289 Color: 5
Size: 357984 Color: 1
Size: 273728 Color: 4

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 368329 Color: 16
Size: 347855 Color: 6
Size: 283817 Color: 5

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 368333 Color: 18
Size: 320874 Color: 3
Size: 310794 Color: 15

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 368376 Color: 17
Size: 339391 Color: 7
Size: 292234 Color: 17

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 368394 Color: 19
Size: 325501 Color: 9
Size: 306106 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 368431 Color: 5
Size: 352680 Color: 4
Size: 278890 Color: 11

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 368632 Color: 16
Size: 335032 Color: 10
Size: 296337 Color: 12

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 368655 Color: 9
Size: 326560 Color: 12
Size: 304786 Color: 3

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 368682 Color: 9
Size: 347422 Color: 19
Size: 283897 Color: 10

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 368682 Color: 7
Size: 334889 Color: 1
Size: 296430 Color: 13

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 368805 Color: 16
Size: 336260 Color: 7
Size: 294936 Color: 11

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 368843 Color: 11
Size: 345181 Color: 7
Size: 285977 Color: 19

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 368898 Color: 6
Size: 333682 Color: 0
Size: 297421 Color: 11

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 368932 Color: 3
Size: 352424 Color: 8
Size: 278645 Color: 19

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 368974 Color: 2
Size: 331319 Color: 10
Size: 299708 Color: 14

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 369002 Color: 5
Size: 318071 Color: 0
Size: 312928 Color: 15

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 369003 Color: 12
Size: 325866 Color: 9
Size: 305132 Color: 7

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 369009 Color: 4
Size: 368719 Color: 15
Size: 262273 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 369030 Color: 7
Size: 316136 Color: 16
Size: 314835 Color: 2

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 369206 Color: 7
Size: 326365 Color: 7
Size: 304430 Color: 14

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 369283 Color: 10
Size: 330149 Color: 13
Size: 300569 Color: 8

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 369317 Color: 19
Size: 350233 Color: 7
Size: 280451 Color: 11

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 369447 Color: 1
Size: 318487 Color: 3
Size: 312067 Color: 14

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 369458 Color: 1
Size: 340059 Color: 2
Size: 290484 Color: 9

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 369405 Color: 17
Size: 325538 Color: 18
Size: 305058 Color: 4

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 369434 Color: 15
Size: 346943 Color: 17
Size: 283624 Color: 3

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 369465 Color: 10
Size: 330105 Color: 18
Size: 300431 Color: 7

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 369368 Color: 18
Size: 337036 Color: 19
Size: 293597 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 369612 Color: 1
Size: 332289 Color: 13
Size: 298100 Color: 2

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 369615 Color: 3
Size: 361371 Color: 13
Size: 269015 Color: 9

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 369664 Color: 17
Size: 321605 Color: 12
Size: 308732 Color: 16

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 369693 Color: 11
Size: 364106 Color: 16
Size: 266202 Color: 5

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 369700 Color: 3
Size: 358017 Color: 6
Size: 272284 Color: 11

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 369749 Color: 13
Size: 318981 Color: 16
Size: 311271 Color: 4

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 369809 Color: 7
Size: 342278 Color: 5
Size: 287914 Color: 1

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 369819 Color: 6
Size: 322555 Color: 14
Size: 307627 Color: 5

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 369839 Color: 11
Size: 366104 Color: 16
Size: 264058 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 369894 Color: 17
Size: 353504 Color: 14
Size: 276603 Color: 12

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 369902 Color: 17
Size: 354867 Color: 18
Size: 275232 Color: 16

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 369835 Color: 18
Size: 354890 Color: 19
Size: 275276 Color: 12

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 369954 Color: 15
Size: 315899 Color: 17
Size: 314148 Color: 6

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 370179 Color: 14
Size: 331963 Color: 19
Size: 297859 Color: 6

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 370285 Color: 17
Size: 327515 Color: 13
Size: 302201 Color: 12

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 370299 Color: 2
Size: 365845 Color: 10
Size: 263857 Color: 16

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 370313 Color: 17
Size: 358232 Color: 5
Size: 271456 Color: 16

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 370519 Color: 5
Size: 345757 Color: 13
Size: 283725 Color: 7

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 370338 Color: 13
Size: 332602 Color: 8
Size: 297061 Color: 3

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 370594 Color: 0
Size: 358421 Color: 14
Size: 270986 Color: 5

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 370604 Color: 19
Size: 369677 Color: 7
Size: 259720 Color: 11

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 370624 Color: 0
Size: 319338 Color: 16
Size: 310039 Color: 19

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 370638 Color: 17
Size: 326785 Color: 16
Size: 302578 Color: 12

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 370779 Color: 0
Size: 354998 Color: 17
Size: 274224 Color: 13

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 370808 Color: 0
Size: 355242 Color: 16
Size: 273951 Color: 7

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 370737 Color: 3
Size: 361244 Color: 1
Size: 268020 Color: 5

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 370747 Color: 4
Size: 328647 Color: 4
Size: 300607 Color: 2

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 370778 Color: 5
Size: 315824 Color: 12
Size: 313399 Color: 6

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 370901 Color: 19
Size: 348013 Color: 1
Size: 281087 Color: 11

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 370935 Color: 6
Size: 336120 Color: 0
Size: 292946 Color: 11

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 370949 Color: 8
Size: 328329 Color: 2
Size: 300723 Color: 18

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 370977 Color: 9
Size: 346805 Color: 6
Size: 282219 Color: 17

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 371022 Color: 6
Size: 366658 Color: 15
Size: 262321 Color: 5

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 371069 Color: 16
Size: 348629 Color: 8
Size: 280303 Color: 6

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 371079 Color: 3
Size: 363083 Color: 11
Size: 265839 Color: 7

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 371148 Color: 11
Size: 322777 Color: 9
Size: 306076 Color: 3

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 371250 Color: 6
Size: 320359 Color: 6
Size: 308392 Color: 10

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 371304 Color: 11
Size: 317143 Color: 17
Size: 311554 Color: 19

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 371324 Color: 6
Size: 318486 Color: 13
Size: 310191 Color: 1

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 371340 Color: 1
Size: 331731 Color: 14
Size: 296930 Color: 9

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 371346 Color: 5
Size: 363613 Color: 12
Size: 265042 Color: 18

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 371423 Color: 17
Size: 321537 Color: 2
Size: 307041 Color: 5

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 371465 Color: 2
Size: 324279 Color: 12
Size: 304257 Color: 16

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 371468 Color: 7
Size: 316175 Color: 11
Size: 312358 Color: 17

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 371519 Color: 4
Size: 361672 Color: 2
Size: 266810 Color: 1

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 371561 Color: 13
Size: 330712 Color: 7
Size: 297728 Color: 10

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 371695 Color: 2
Size: 363235 Color: 6
Size: 265071 Color: 9

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 371759 Color: 9
Size: 343369 Color: 13
Size: 284873 Color: 12

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 371794 Color: 16
Size: 359859 Color: 17
Size: 268348 Color: 11

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 371967 Color: 0
Size: 322212 Color: 6
Size: 305822 Color: 2

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 371988 Color: 18
Size: 330705 Color: 14
Size: 297308 Color: 5

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 371658 Color: 17
Size: 327856 Color: 4
Size: 300487 Color: 18

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 371914 Color: 5
Size: 333225 Color: 6
Size: 294862 Color: 16

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 372000 Color: 18
Size: 331401 Color: 1
Size: 296600 Color: 6

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 372007 Color: 9
Size: 369312 Color: 19
Size: 258682 Color: 12

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 372021 Color: 15
Size: 348278 Color: 4
Size: 279702 Color: 12

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 372063 Color: 13
Size: 333538 Color: 14
Size: 294400 Color: 15

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 372109 Color: 14
Size: 340272 Color: 7
Size: 287620 Color: 7

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 372204 Color: 13
Size: 361631 Color: 16
Size: 266166 Color: 5

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 372275 Color: 3
Size: 364255 Color: 18
Size: 263471 Color: 12

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 372376 Color: 14
Size: 360741 Color: 0
Size: 266884 Color: 4

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 372324 Color: 13
Size: 327222 Color: 16
Size: 300455 Color: 0

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 372423 Color: 19
Size: 364578 Color: 15
Size: 263000 Color: 6

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 372496 Color: 11
Size: 350349 Color: 10
Size: 277156 Color: 4

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 372536 Color: 2
Size: 331213 Color: 11
Size: 296252 Color: 3

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 372609 Color: 17
Size: 324654 Color: 18
Size: 302738 Color: 11

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 372623 Color: 15
Size: 338437 Color: 16
Size: 288941 Color: 14

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 372650 Color: 18
Size: 319052 Color: 13
Size: 308299 Color: 0

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 372700 Color: 15
Size: 345525 Color: 18
Size: 281776 Color: 4

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 372789 Color: 4
Size: 355206 Color: 9
Size: 272006 Color: 5

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 372827 Color: 19
Size: 316911 Color: 15
Size: 310263 Color: 19

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 372914 Color: 4
Size: 342785 Color: 10
Size: 284302 Color: 18

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 372989 Color: 1
Size: 358507 Color: 13
Size: 268505 Color: 16

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 372998 Color: 1
Size: 342489 Color: 19
Size: 284514 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 373027 Color: 4
Size: 361096 Color: 9
Size: 265878 Color: 19

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 373065 Color: 17
Size: 337771 Color: 2
Size: 289165 Color: 14

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 373071 Color: 4
Size: 320763 Color: 6
Size: 306167 Color: 19

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 373074 Color: 9
Size: 332218 Color: 5
Size: 294709 Color: 8

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 373082 Color: 11
Size: 367830 Color: 18
Size: 259089 Color: 3

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 373220 Color: 2
Size: 366249 Color: 12
Size: 260532 Color: 10

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 373286 Color: 7
Size: 342026 Color: 19
Size: 284689 Color: 8

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 373314 Color: 5
Size: 345676 Color: 12
Size: 281011 Color: 12

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 373319 Color: 15
Size: 365321 Color: 18
Size: 261361 Color: 5

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 373360 Color: 2
Size: 322312 Color: 4
Size: 304329 Color: 0

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 373390 Color: 4
Size: 361013 Color: 15
Size: 265598 Color: 15

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 373397 Color: 3
Size: 326417 Color: 8
Size: 300187 Color: 2

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 373403 Color: 5
Size: 319039 Color: 1
Size: 307559 Color: 14

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 373434 Color: 1
Size: 318915 Color: 16
Size: 307652 Color: 10

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 373438 Color: 3
Size: 314252 Color: 10
Size: 312311 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 373438 Color: 6
Size: 323682 Color: 2
Size: 302881 Color: 19

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 373470 Color: 14
Size: 315906 Color: 4
Size: 310625 Color: 11

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 373579 Color: 12
Size: 354425 Color: 8
Size: 271997 Color: 8

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 373596 Color: 9
Size: 354926 Color: 2
Size: 271479 Color: 11

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 373642 Color: 11
Size: 334089 Color: 7
Size: 292270 Color: 9

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 373628 Color: 5
Size: 357988 Color: 17
Size: 268385 Color: 5

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 373666 Color: 12
Size: 339605 Color: 16
Size: 286730 Color: 18

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 373692 Color: 8
Size: 349244 Color: 12
Size: 277065 Color: 14

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 373708 Color: 0
Size: 332080 Color: 16
Size: 294213 Color: 18

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 373802 Color: 19
Size: 322434 Color: 16
Size: 303765 Color: 19

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 373873 Color: 13
Size: 356902 Color: 12
Size: 269226 Color: 11

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 373875 Color: 6
Size: 351312 Color: 15
Size: 274814 Color: 7

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 373669 Color: 17
Size: 327572 Color: 4
Size: 298760 Color: 6

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 373887 Color: 10
Size: 341979 Color: 10
Size: 284135 Color: 5

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 373969 Color: 12
Size: 323125 Color: 9
Size: 302907 Color: 7

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 373973 Color: 1
Size: 313223 Color: 18
Size: 312805 Color: 15

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 374007 Color: 1
Size: 335908 Color: 15
Size: 290086 Color: 18

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 374181 Color: 0
Size: 346766 Color: 8
Size: 279054 Color: 7

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 374089 Color: 14
Size: 368389 Color: 19
Size: 257523 Color: 4

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 374110 Color: 8
Size: 334690 Color: 7
Size: 291201 Color: 10

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 374159 Color: 7
Size: 368194 Color: 10
Size: 257648 Color: 14

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 374167 Color: 11
Size: 344607 Color: 10
Size: 281227 Color: 10

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 374239 Color: 19
Size: 318130 Color: 18
Size: 307632 Color: 15

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 374282 Color: 11
Size: 352256 Color: 2
Size: 273463 Color: 14

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 374288 Color: 9
Size: 327100 Color: 15
Size: 298613 Color: 8

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 374290 Color: 11
Size: 373865 Color: 9
Size: 251846 Color: 12

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 374311 Color: 4
Size: 319987 Color: 15
Size: 305703 Color: 10

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 374330 Color: 3
Size: 349303 Color: 18
Size: 276368 Color: 1

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 374502 Color: 1
Size: 330705 Color: 5
Size: 294794 Color: 16

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 374401 Color: 16
Size: 349920 Color: 19
Size: 275680 Color: 13

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 374428 Color: 5
Size: 316355 Color: 11
Size: 309218 Color: 10

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 374489 Color: 8
Size: 354634 Color: 5
Size: 270878 Color: 4

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 374492 Color: 14
Size: 345357 Color: 3
Size: 280152 Color: 2

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 374503 Color: 1
Size: 367573 Color: 18
Size: 257925 Color: 12

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 374522 Color: 14
Size: 354866 Color: 3
Size: 270613 Color: 11

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 374524 Color: 10
Size: 343878 Color: 13
Size: 281599 Color: 10

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 374543 Color: 2
Size: 326233 Color: 6
Size: 299225 Color: 9

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 374554 Color: 13
Size: 337998 Color: 17
Size: 287449 Color: 8

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 374571 Color: 19
Size: 345513 Color: 6
Size: 279917 Color: 6

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 374583 Color: 0
Size: 334673 Color: 14
Size: 290745 Color: 1

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 374610 Color: 2
Size: 365140 Color: 4
Size: 260251 Color: 7

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 374612 Color: 2
Size: 342226 Color: 6
Size: 283163 Color: 14

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 374718 Color: 11
Size: 317454 Color: 8
Size: 307829 Color: 5

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 374772 Color: 12
Size: 364516 Color: 2
Size: 260713 Color: 5

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 374790 Color: 10
Size: 335588 Color: 17
Size: 289623 Color: 9

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 374884 Color: 1
Size: 364466 Color: 13
Size: 260651 Color: 14

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 374872 Color: 9
Size: 342464 Color: 19
Size: 282665 Color: 2

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 374880 Color: 3
Size: 321111 Color: 18
Size: 304010 Color: 5

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 374880 Color: 13
Size: 356227 Color: 12
Size: 268894 Color: 9

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 374903 Color: 4
Size: 332626 Color: 9
Size: 292472 Color: 19

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 374796 Color: 14
Size: 359341 Color: 18
Size: 265864 Color: 7

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 374924 Color: 6
Size: 370607 Color: 2
Size: 254470 Color: 1

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 374988 Color: 15
Size: 368331 Color: 19
Size: 256682 Color: 12

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 374993 Color: 6
Size: 354387 Color: 10
Size: 270621 Color: 5

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 375057 Color: 16
Size: 352744 Color: 15
Size: 272200 Color: 6

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 375078 Color: 7
Size: 326033 Color: 18
Size: 298890 Color: 17

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 375152 Color: 8
Size: 347519 Color: 2
Size: 277330 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 375191 Color: 10
Size: 332806 Color: 10
Size: 292004 Color: 1

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 375210 Color: 12
Size: 354063 Color: 13
Size: 270728 Color: 6

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 375237 Color: 0
Size: 365373 Color: 6
Size: 259391 Color: 9

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 375268 Color: 3
Size: 371752 Color: 14
Size: 252981 Color: 13

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 375344 Color: 5
Size: 349155 Color: 5
Size: 275502 Color: 15

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 375354 Color: 7
Size: 332615 Color: 17
Size: 292032 Color: 2

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 375392 Color: 11
Size: 357697 Color: 3
Size: 266912 Color: 7

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 375417 Color: 9
Size: 315974 Color: 0
Size: 308610 Color: 4

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 375426 Color: 18
Size: 333927 Color: 8
Size: 290648 Color: 13

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 375436 Color: 6
Size: 319499 Color: 17
Size: 305066 Color: 13

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 375495 Color: 9
Size: 319868 Color: 5
Size: 304638 Color: 11

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 375507 Color: 7
Size: 340769 Color: 9
Size: 283725 Color: 2

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 375517 Color: 19
Size: 355202 Color: 0
Size: 269282 Color: 18

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 375542 Color: 19
Size: 332953 Color: 6
Size: 291506 Color: 13

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 375630 Color: 0
Size: 337993 Color: 12
Size: 286378 Color: 8

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 375778 Color: 10
Size: 339201 Color: 1
Size: 285022 Color: 2

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 375810 Color: 10
Size: 330481 Color: 1
Size: 293710 Color: 7

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 375826 Color: 12
Size: 368936 Color: 16
Size: 255239 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 375861 Color: 5
Size: 369579 Color: 4
Size: 254561 Color: 8

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 375938 Color: 13
Size: 333258 Color: 19
Size: 290805 Color: 11

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 375952 Color: 14
Size: 350422 Color: 3
Size: 273627 Color: 9

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 375958 Color: 11
Size: 342803 Color: 13
Size: 281240 Color: 16

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 376010 Color: 15
Size: 365960 Color: 19
Size: 258031 Color: 8

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 376117 Color: 18
Size: 338364 Color: 10
Size: 285520 Color: 15

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 376169 Color: 15
Size: 360179 Color: 8
Size: 263653 Color: 8

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 376183 Color: 15
Size: 325677 Color: 17
Size: 298141 Color: 2

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 376226 Color: 14
Size: 362055 Color: 4
Size: 261720 Color: 6

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 376273 Color: 3
Size: 334162 Color: 14
Size: 289566 Color: 6

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 376291 Color: 11
Size: 325073 Color: 14
Size: 298637 Color: 1

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 376298 Color: 8
Size: 342080 Color: 2
Size: 281623 Color: 14

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 376313 Color: 3
Size: 348311 Color: 13
Size: 275377 Color: 16

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 376321 Color: 11
Size: 327120 Color: 9
Size: 296560 Color: 5

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 376342 Color: 8
Size: 367465 Color: 18
Size: 256194 Color: 3

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 376362 Color: 10
Size: 323893 Color: 13
Size: 299746 Color: 6

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 376424 Color: 7
Size: 346325 Color: 14
Size: 277252 Color: 8

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 376435 Color: 4
Size: 338016 Color: 10
Size: 285550 Color: 7

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 376534 Color: 12
Size: 329167 Color: 8
Size: 294300 Color: 3

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 376588 Color: 10
Size: 365927 Color: 4
Size: 257486 Color: 10

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 376610 Color: 12
Size: 354429 Color: 0
Size: 268962 Color: 9

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 376671 Color: 6
Size: 356542 Color: 5
Size: 266788 Color: 8

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 376676 Color: 0
Size: 333283 Color: 4
Size: 290042 Color: 9

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 376689 Color: 18
Size: 328183 Color: 10
Size: 295129 Color: 6

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 376746 Color: 3
Size: 371306 Color: 4
Size: 251949 Color: 2

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 376790 Color: 8
Size: 335364 Color: 9
Size: 287847 Color: 1

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 376834 Color: 3
Size: 342277 Color: 12
Size: 280890 Color: 10

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 376850 Color: 0
Size: 338909 Color: 19
Size: 284242 Color: 3

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 376852 Color: 5
Size: 332617 Color: 18
Size: 290532 Color: 18

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 376899 Color: 15
Size: 367783 Color: 17
Size: 255319 Color: 12

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 376951 Color: 19
Size: 346066 Color: 2
Size: 276984 Color: 6

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 377099 Color: 1
Size: 356579 Color: 7
Size: 266323 Color: 13

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 377082 Color: 9
Size: 316222 Color: 8
Size: 306697 Color: 19

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 377090 Color: 18
Size: 362486 Color: 5
Size: 260425 Color: 5

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 377097 Color: 7
Size: 349337 Color: 5
Size: 273567 Color: 16

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 377110 Color: 13
Size: 355031 Color: 0
Size: 267860 Color: 18

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 377125 Color: 15
Size: 358396 Color: 12
Size: 264480 Color: 15

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 377266 Color: 2
Size: 347115 Color: 1
Size: 275620 Color: 13

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 377399 Color: 15
Size: 343144 Color: 14
Size: 279458 Color: 19

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 377487 Color: 16
Size: 322499 Color: 7
Size: 300015 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 377532 Color: 17
Size: 355993 Color: 10
Size: 266476 Color: 4

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 377616 Color: 0
Size: 358063 Color: 2
Size: 264322 Color: 13

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 377679 Color: 8
Size: 311686 Color: 1
Size: 310636 Color: 7

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 377681 Color: 18
Size: 352484 Color: 7
Size: 269836 Color: 3

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 377729 Color: 16
Size: 323387 Color: 18
Size: 298885 Color: 17

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 377743 Color: 7
Size: 347149 Color: 10
Size: 275109 Color: 7

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 377761 Color: 13
Size: 327007 Color: 16
Size: 295233 Color: 16

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 377819 Color: 13
Size: 343737 Color: 16
Size: 278445 Color: 13

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 428759 Color: 1
Size: 313725 Color: 15
Size: 257517 Color: 13

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 378086 Color: 12
Size: 314446 Color: 4
Size: 307469 Color: 8

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 378118 Color: 19
Size: 314734 Color: 17
Size: 307149 Color: 17

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 378119 Color: 6
Size: 350919 Color: 10
Size: 270963 Color: 13

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 378181 Color: 1
Size: 341394 Color: 19
Size: 280426 Color: 4

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 378257 Color: 2
Size: 319936 Color: 0
Size: 301808 Color: 10

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 378268 Color: 19
Size: 328683 Color: 19
Size: 293050 Color: 2

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 378286 Color: 19
Size: 330636 Color: 4
Size: 291079 Color: 17

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 378325 Color: 2
Size: 313523 Color: 4
Size: 308153 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 378368 Color: 4
Size: 317203 Color: 15
Size: 304430 Color: 8

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 378385 Color: 14
Size: 320717 Color: 7
Size: 300899 Color: 5

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 378447 Color: 16
Size: 324090 Color: 4
Size: 297464 Color: 12

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 378542 Color: 9
Size: 335892 Color: 3
Size: 285567 Color: 2

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 378550 Color: 5
Size: 348146 Color: 4
Size: 273305 Color: 2

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 378618 Color: 15
Size: 368270 Color: 6
Size: 253113 Color: 14

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 378658 Color: 16
Size: 343147 Color: 14
Size: 278196 Color: 13

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 378700 Color: 5
Size: 355609 Color: 19
Size: 265692 Color: 4

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 378730 Color: 5
Size: 356650 Color: 10
Size: 264621 Color: 9

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 378733 Color: 6
Size: 324247 Color: 10
Size: 297021 Color: 10

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 378794 Color: 16
Size: 362627 Color: 19
Size: 258580 Color: 7

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 378834 Color: 9
Size: 346520 Color: 10
Size: 274647 Color: 8

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 378833 Color: 2
Size: 353421 Color: 15
Size: 267747 Color: 4

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 378852 Color: 10
Size: 359755 Color: 8
Size: 261394 Color: 3

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 378865 Color: 8
Size: 336518 Color: 17
Size: 284618 Color: 12

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 378894 Color: 3
Size: 359352 Color: 17
Size: 261755 Color: 3

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 378928 Color: 5
Size: 333025 Color: 10
Size: 288048 Color: 8

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 378959 Color: 14
Size: 342256 Color: 8
Size: 278786 Color: 17

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 378964 Color: 5
Size: 350756 Color: 2
Size: 270281 Color: 3

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 378994 Color: 14
Size: 315000 Color: 4
Size: 306007 Color: 10

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 379070 Color: 7
Size: 326673 Color: 3
Size: 294258 Color: 12

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 379072 Color: 13
Size: 331981 Color: 15
Size: 288948 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 379078 Color: 12
Size: 354175 Color: 15
Size: 266748 Color: 11

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 379153 Color: 0
Size: 318819 Color: 13
Size: 302029 Color: 17

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 379154 Color: 11
Size: 335660 Color: 12
Size: 285187 Color: 14

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 379256 Color: 6
Size: 321238 Color: 2
Size: 299507 Color: 12

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 379277 Color: 16
Size: 325199 Color: 13
Size: 295525 Color: 16

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 379310 Color: 0
Size: 320330 Color: 13
Size: 300361 Color: 12

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 379313 Color: 18
Size: 359932 Color: 1
Size: 260756 Color: 11

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 379378 Color: 1
Size: 333576 Color: 9
Size: 287047 Color: 9

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 379436 Color: 1
Size: 330277 Color: 7
Size: 290288 Color: 7

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 379477 Color: 0
Size: 356576 Color: 16
Size: 263948 Color: 18

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 379546 Color: 12
Size: 360553 Color: 8
Size: 259902 Color: 6

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 379691 Color: 16
Size: 335147 Color: 6
Size: 285163 Color: 4

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 379698 Color: 3
Size: 358363 Color: 16
Size: 261940 Color: 9

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 379718 Color: 0
Size: 330257 Color: 9
Size: 290026 Color: 6

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 379755 Color: 16
Size: 341750 Color: 0
Size: 278496 Color: 5

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 379904 Color: 2
Size: 327375 Color: 19
Size: 292722 Color: 13

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 379980 Color: 5
Size: 325072 Color: 19
Size: 294949 Color: 13

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 380003 Color: 0
Size: 340298 Color: 18
Size: 279700 Color: 4

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 380025 Color: 7
Size: 343301 Color: 16
Size: 276675 Color: 12

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 380046 Color: 6
Size: 348876 Color: 14
Size: 271079 Color: 12

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 380052 Color: 4
Size: 313685 Color: 18
Size: 306264 Color: 16

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 380069 Color: 12
Size: 352047 Color: 4
Size: 267885 Color: 5

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 380071 Color: 16
Size: 332575 Color: 19
Size: 287355 Color: 9

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 380100 Color: 19
Size: 322557 Color: 0
Size: 297344 Color: 16

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 380104 Color: 0
Size: 348373 Color: 16
Size: 271524 Color: 13

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 380192 Color: 3
Size: 352697 Color: 16
Size: 267112 Color: 8

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 380231 Color: 7
Size: 367155 Color: 13
Size: 252615 Color: 10

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 380245 Color: 16
Size: 356412 Color: 11
Size: 263344 Color: 4

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 380260 Color: 9
Size: 343084 Color: 10
Size: 276657 Color: 10

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 380264 Color: 10
Size: 352577 Color: 1
Size: 267160 Color: 15

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 380308 Color: 6
Size: 317056 Color: 14
Size: 302637 Color: 14

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 380314 Color: 10
Size: 347047 Color: 17
Size: 272640 Color: 0

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 380328 Color: 15
Size: 311982 Color: 11
Size: 307691 Color: 6

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 380328 Color: 0
Size: 353233 Color: 1
Size: 266440 Color: 4

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 380347 Color: 6
Size: 330602 Color: 19
Size: 289052 Color: 13

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 380381 Color: 17
Size: 361972 Color: 9
Size: 257648 Color: 8

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 380399 Color: 18
Size: 342659 Color: 8
Size: 276943 Color: 17

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 380444 Color: 11
Size: 335853 Color: 15
Size: 283704 Color: 10

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 380451 Color: 5
Size: 318895 Color: 16
Size: 300655 Color: 10

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 380522 Color: 10
Size: 345910 Color: 17
Size: 273569 Color: 1

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 380570 Color: 18
Size: 352233 Color: 4
Size: 267198 Color: 13

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 380606 Color: 6
Size: 310931 Color: 12
Size: 308464 Color: 9

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 380615 Color: 5
Size: 312128 Color: 9
Size: 307258 Color: 15

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 380674 Color: 6
Size: 334785 Color: 13
Size: 284542 Color: 8

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 380695 Color: 2
Size: 356642 Color: 11
Size: 262664 Color: 3

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 380728 Color: 1
Size: 363003 Color: 6
Size: 256270 Color: 13

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 380781 Color: 4
Size: 317597 Color: 5
Size: 301623 Color: 10

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 380748 Color: 14
Size: 321666 Color: 10
Size: 297587 Color: 16

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 380791 Color: 14
Size: 365659 Color: 15
Size: 253551 Color: 9

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 380792 Color: 6
Size: 353843 Color: 19
Size: 265366 Color: 18

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 380910 Color: 0
Size: 354120 Color: 1
Size: 264971 Color: 16

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 380938 Color: 15
Size: 340794 Color: 11
Size: 278269 Color: 0

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 380989 Color: 12
Size: 322398 Color: 2
Size: 296614 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 381140 Color: 4
Size: 335727 Color: 5
Size: 283134 Color: 5

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 380999 Color: 3
Size: 334062 Color: 13
Size: 284940 Color: 16

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 381098 Color: 1
Size: 340967 Color: 11
Size: 277936 Color: 2

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 381174 Color: 1
Size: 347016 Color: 17
Size: 271811 Color: 18

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 381189 Color: 2
Size: 325182 Color: 2
Size: 293630 Color: 6

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 381224 Color: 6
Size: 337270 Color: 4
Size: 281507 Color: 19

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 381234 Color: 8
Size: 340062 Color: 9
Size: 278705 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 381264 Color: 1
Size: 324555 Color: 2
Size: 294182 Color: 16

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 381303 Color: 5
Size: 314799 Color: 6
Size: 303899 Color: 12

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 381316 Color: 3
Size: 342191 Color: 19
Size: 276494 Color: 18

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 381324 Color: 10
Size: 314573 Color: 10
Size: 304104 Color: 7

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 381340 Color: 3
Size: 366933 Color: 6
Size: 251728 Color: 18

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 381385 Color: 11
Size: 309432 Color: 12
Size: 309184 Color: 4

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 381387 Color: 14
Size: 310279 Color: 2
Size: 308335 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 381403 Color: 13
Size: 320951 Color: 10
Size: 297647 Color: 0

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 381414 Color: 3
Size: 337938 Color: 7
Size: 280649 Color: 19

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 381474 Color: 3
Size: 341353 Color: 13
Size: 277174 Color: 14

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 381480 Color: 1
Size: 321933 Color: 8
Size: 296588 Color: 18

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 381550 Color: 0
Size: 311684 Color: 9
Size: 306767 Color: 6

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 381554 Color: 9
Size: 355896 Color: 16
Size: 262551 Color: 18

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 381563 Color: 14
Size: 329464 Color: 8
Size: 288974 Color: 5

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 381582 Color: 5
Size: 343697 Color: 18
Size: 274722 Color: 5

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 381616 Color: 15
Size: 321205 Color: 1
Size: 297180 Color: 9

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 381619 Color: 0
Size: 365715 Color: 10
Size: 252667 Color: 6

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 381703 Color: 19
Size: 364854 Color: 0
Size: 253444 Color: 12

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 381772 Color: 7
Size: 329113 Color: 13
Size: 289116 Color: 16

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 381807 Color: 10
Size: 365281 Color: 9
Size: 252913 Color: 1

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 381828 Color: 1
Size: 316500 Color: 9
Size: 301673 Color: 5

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 381857 Color: 3
Size: 335118 Color: 1
Size: 283026 Color: 14

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 381899 Color: 6
Size: 343257 Color: 9
Size: 274845 Color: 10

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 381943 Color: 14
Size: 348002 Color: 16
Size: 270056 Color: 14

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 382001 Color: 8
Size: 313018 Color: 16
Size: 304982 Color: 19

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 382001 Color: 13
Size: 334180 Color: 5
Size: 283820 Color: 19

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 382127 Color: 16
Size: 340950 Color: 2
Size: 276924 Color: 9

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 382174 Color: 14
Size: 312885 Color: 1
Size: 304942 Color: 0

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 382226 Color: 11
Size: 325422 Color: 8
Size: 292353 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 382235 Color: 18
Size: 346560 Color: 9
Size: 271206 Color: 19

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 382265 Color: 6
Size: 331248 Color: 17
Size: 286488 Color: 14

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 382266 Color: 11
Size: 344732 Color: 15
Size: 273003 Color: 19

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 382274 Color: 17
Size: 338408 Color: 9
Size: 279319 Color: 5

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 382297 Color: 9
Size: 320214 Color: 0
Size: 297490 Color: 8

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 382359 Color: 8
Size: 353913 Color: 2
Size: 263729 Color: 5

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 382407 Color: 13
Size: 342153 Color: 4
Size: 275441 Color: 7

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 382423 Color: 19
Size: 311222 Color: 3
Size: 306356 Color: 18

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 382467 Color: 7
Size: 310620 Color: 14
Size: 306914 Color: 8

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 382480 Color: 15
Size: 348002 Color: 6
Size: 269519 Color: 14

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 382504 Color: 2
Size: 321026 Color: 7
Size: 296471 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 382515 Color: 10
Size: 324127 Color: 4
Size: 293359 Color: 15

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 382554 Color: 4
Size: 328875 Color: 8
Size: 288572 Color: 17

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 382560 Color: 17
Size: 327192 Color: 18
Size: 290249 Color: 4

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 382564 Color: 5
Size: 354007 Color: 16
Size: 263430 Color: 1

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 382706 Color: 13
Size: 310417 Color: 19
Size: 306878 Color: 3

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 382743 Color: 4
Size: 310470 Color: 9
Size: 306788 Color: 1

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 382765 Color: 4
Size: 340740 Color: 10
Size: 276496 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 382851 Color: 18
Size: 341453 Color: 18
Size: 275697 Color: 19

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 382852 Color: 19
Size: 317026 Color: 11
Size: 300123 Color: 4

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 382869 Color: 7
Size: 355793 Color: 16
Size: 261339 Color: 17

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 382969 Color: 2
Size: 318086 Color: 17
Size: 298946 Color: 8

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 382983 Color: 6
Size: 330895 Color: 3
Size: 286123 Color: 6

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 383003 Color: 17
Size: 310152 Color: 15
Size: 306846 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 383130 Color: 12
Size: 329074 Color: 1
Size: 287797 Color: 10

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 383131 Color: 4
Size: 341810 Color: 16
Size: 275060 Color: 12

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 383136 Color: 13
Size: 315023 Color: 12
Size: 301842 Color: 2

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 383253 Color: 10
Size: 337422 Color: 19
Size: 279326 Color: 6

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 383283 Color: 1
Size: 360945 Color: 0
Size: 255773 Color: 7

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 383301 Color: 13
Size: 359024 Color: 12
Size: 257676 Color: 5

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 383341 Color: 17
Size: 346910 Color: 16
Size: 269750 Color: 12

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 383350 Color: 5
Size: 327216 Color: 11
Size: 289435 Color: 2

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 383360 Color: 12
Size: 363528 Color: 8
Size: 253113 Color: 17

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 383373 Color: 18
Size: 325297 Color: 16
Size: 291331 Color: 14

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 383453 Color: 10
Size: 327933 Color: 6
Size: 288615 Color: 19

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 383524 Color: 18
Size: 353072 Color: 17
Size: 263405 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 383573 Color: 5
Size: 325974 Color: 19
Size: 290454 Color: 12

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 383575 Color: 14
Size: 310706 Color: 3
Size: 305720 Color: 4

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 383623 Color: 5
Size: 314773 Color: 17
Size: 301605 Color: 8

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 383781 Color: 19
Size: 338844 Color: 14
Size: 277376 Color: 1

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 383828 Color: 18
Size: 308118 Color: 10
Size: 308055 Color: 3

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 383994 Color: 17
Size: 308924 Color: 2
Size: 307083 Color: 9

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 384174 Color: 15
Size: 310054 Color: 17
Size: 305773 Color: 14

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 384251 Color: 6
Size: 324750 Color: 16
Size: 291000 Color: 9

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 384316 Color: 3
Size: 351310 Color: 11
Size: 264375 Color: 4

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 384318 Color: 16
Size: 328557 Color: 5
Size: 287126 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 384393 Color: 19
Size: 328175 Color: 7
Size: 287433 Color: 3

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 384403 Color: 19
Size: 350545 Color: 3
Size: 265053 Color: 12

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 384680 Color: 3
Size: 335392 Color: 10
Size: 279929 Color: 13

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 384703 Color: 18
Size: 341182 Color: 4
Size: 274116 Color: 13

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 384710 Color: 10
Size: 321935 Color: 18
Size: 293356 Color: 3

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 384725 Color: 19
Size: 327702 Color: 0
Size: 287574 Color: 4

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 384768 Color: 1
Size: 351907 Color: 15
Size: 263326 Color: 13

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 384778 Color: 9
Size: 340486 Color: 13
Size: 274737 Color: 6

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 384818 Color: 13
Size: 355182 Color: 16
Size: 260001 Color: 6

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 384934 Color: 19
Size: 308129 Color: 3
Size: 306938 Color: 14

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 384959 Color: 3
Size: 344381 Color: 17
Size: 270661 Color: 18

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 385039 Color: 1
Size: 354109 Color: 4
Size: 260853 Color: 14

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 385064 Color: 5
Size: 323945 Color: 7
Size: 290992 Color: 16

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 385096 Color: 3
Size: 357724 Color: 9
Size: 257181 Color: 2

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 385111 Color: 10
Size: 329214 Color: 17
Size: 285676 Color: 16

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 385130 Color: 5
Size: 336247 Color: 19
Size: 278624 Color: 17

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 385157 Color: 8
Size: 360582 Color: 19
Size: 254262 Color: 0

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 385209 Color: 16
Size: 314875 Color: 10
Size: 299917 Color: 0

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 385247 Color: 1
Size: 319289 Color: 8
Size: 295465 Color: 17

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 385316 Color: 11
Size: 364426 Color: 14
Size: 250259 Color: 5

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 385353 Color: 10
Size: 315067 Color: 6
Size: 299581 Color: 9

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 385406 Color: 19
Size: 363068 Color: 2
Size: 251527 Color: 15

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 385450 Color: 5
Size: 355303 Color: 0
Size: 259248 Color: 10

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 385536 Color: 11
Size: 353170 Color: 6
Size: 261295 Color: 12

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 385543 Color: 15
Size: 313711 Color: 8
Size: 300747 Color: 17

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 385575 Color: 10
Size: 349592 Color: 9
Size: 264834 Color: 13

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 385619 Color: 1
Size: 345119 Color: 11
Size: 269263 Color: 6

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 385620 Color: 2
Size: 316039 Color: 13
Size: 298342 Color: 7

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 385627 Color: 17
Size: 324667 Color: 11
Size: 289707 Color: 8

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 385727 Color: 0
Size: 327626 Color: 6
Size: 286648 Color: 4

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 385634 Color: 16
Size: 316652 Color: 12
Size: 297715 Color: 17

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 385647 Color: 14
Size: 331137 Color: 17
Size: 283217 Color: 3

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 385727 Color: 1
Size: 342961 Color: 19
Size: 271313 Color: 8

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 385741 Color: 8
Size: 323893 Color: 4
Size: 290367 Color: 1

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 385817 Color: 0
Size: 350535 Color: 6
Size: 263649 Color: 3

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 385797 Color: 5
Size: 364069 Color: 9
Size: 250135 Color: 3

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 385825 Color: 4
Size: 322364 Color: 3
Size: 291812 Color: 13

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 385827 Color: 5
Size: 326060 Color: 19
Size: 288114 Color: 17

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 385841 Color: 3
Size: 348936 Color: 17
Size: 265224 Color: 10

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 385898 Color: 2
Size: 307261 Color: 12
Size: 306842 Color: 6

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 385904 Color: 14
Size: 350602 Color: 0
Size: 263495 Color: 3

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 385912 Color: 15
Size: 339591 Color: 18
Size: 274498 Color: 16

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 385916 Color: 10
Size: 319794 Color: 13
Size: 294291 Color: 18

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 385989 Color: 9
Size: 311871 Color: 3
Size: 302141 Color: 5

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 385993 Color: 13
Size: 361915 Color: 5
Size: 252093 Color: 7

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 386003 Color: 10
Size: 360224 Color: 12
Size: 253774 Color: 9

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 386005 Color: 14
Size: 350056 Color: 4
Size: 263940 Color: 6

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 386047 Color: 15
Size: 361715 Color: 14
Size: 252239 Color: 7

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 386048 Color: 17
Size: 317050 Color: 16
Size: 296903 Color: 10

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 386222 Color: 18
Size: 355674 Color: 19
Size: 258105 Color: 14

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 386227 Color: 5
Size: 327959 Color: 0
Size: 285815 Color: 19

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 386252 Color: 15
Size: 313202 Color: 4
Size: 300547 Color: 17

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 386280 Color: 6
Size: 349191 Color: 14
Size: 264530 Color: 10

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 386319 Color: 9
Size: 343717 Color: 0
Size: 269965 Color: 15

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 386344 Color: 7
Size: 312560 Color: 4
Size: 301097 Color: 3

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 386434 Color: 11
Size: 358739 Color: 16
Size: 254828 Color: 11

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 386467 Color: 3
Size: 347394 Color: 2
Size: 266140 Color: 4

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 386565 Color: 15
Size: 346926 Color: 8
Size: 266510 Color: 17

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 386592 Color: 8
Size: 350281 Color: 14
Size: 263128 Color: 3

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 386594 Color: 4
Size: 349510 Color: 0
Size: 263897 Color: 16

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 386595 Color: 9
Size: 348457 Color: 18
Size: 264949 Color: 7

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 386606 Color: 13
Size: 310503 Color: 15
Size: 302892 Color: 5

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 386637 Color: 11
Size: 309260 Color: 7
Size: 304104 Color: 19

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 386656 Color: 13
Size: 359469 Color: 5
Size: 253876 Color: 10

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 386672 Color: 19
Size: 343700 Color: 4
Size: 269629 Color: 9

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 386747 Color: 5
Size: 353972 Color: 7
Size: 259282 Color: 1

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 386781 Color: 16
Size: 321331 Color: 12
Size: 291889 Color: 13

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 386794 Color: 9
Size: 340388 Color: 2
Size: 272819 Color: 7

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 386813 Color: 1
Size: 352289 Color: 1
Size: 260899 Color: 6

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 386827 Color: 13
Size: 313478 Color: 12
Size: 299696 Color: 15

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 386832 Color: 3
Size: 345838 Color: 3
Size: 267331 Color: 19

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 386878 Color: 5
Size: 362224 Color: 11
Size: 250899 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 386903 Color: 4
Size: 316154 Color: 2
Size: 296944 Color: 8

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 386944 Color: 7
Size: 328851 Color: 0
Size: 284206 Color: 15

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 386978 Color: 12
Size: 330458 Color: 2
Size: 282565 Color: 16

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 386978 Color: 11
Size: 330204 Color: 14
Size: 282819 Color: 12

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 386991 Color: 4
Size: 340109 Color: 19
Size: 272901 Color: 18

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 386995 Color: 7
Size: 313910 Color: 0
Size: 299096 Color: 2

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 387025 Color: 7
Size: 320541 Color: 3
Size: 292435 Color: 18

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 387027 Color: 3
Size: 335445 Color: 15
Size: 277529 Color: 16

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 387029 Color: 3
Size: 333124 Color: 8
Size: 279848 Color: 14

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 387155 Color: 6
Size: 316241 Color: 4
Size: 296605 Color: 1

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 387174 Color: 2
Size: 318771 Color: 14
Size: 294056 Color: 4

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 387191 Color: 11
Size: 306762 Color: 0
Size: 306048 Color: 13

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 387247 Color: 18
Size: 361387 Color: 1
Size: 251367 Color: 14

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 387260 Color: 13
Size: 327137 Color: 11
Size: 285604 Color: 13

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 387269 Color: 16
Size: 348002 Color: 1
Size: 264730 Color: 12

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 387273 Color: 9
Size: 313842 Color: 2
Size: 298886 Color: 2

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 387314 Color: 5
Size: 337219 Color: 14
Size: 275468 Color: 8

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 387347 Color: 18
Size: 341110 Color: 7
Size: 271544 Color: 4

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 387393 Color: 6
Size: 310240 Color: 5
Size: 302368 Color: 11

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 387424 Color: 11
Size: 322176 Color: 6
Size: 290401 Color: 3

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 387444 Color: 18
Size: 335513 Color: 1
Size: 277044 Color: 9

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 387471 Color: 18
Size: 339953 Color: 3
Size: 272577 Color: 8

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 387477 Color: 16
Size: 330332 Color: 7
Size: 282192 Color: 17

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 387510 Color: 15
Size: 347658 Color: 1
Size: 264833 Color: 4

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 387540 Color: 11
Size: 357266 Color: 7
Size: 255195 Color: 3

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 387542 Color: 15
Size: 337971 Color: 16
Size: 274488 Color: 7

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 387547 Color: 1
Size: 325921 Color: 9
Size: 286533 Color: 9

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 387567 Color: 5
Size: 334347 Color: 12
Size: 278087 Color: 2

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 387571 Color: 15
Size: 355240 Color: 13
Size: 257190 Color: 5

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 387642 Color: 12
Size: 341668 Color: 4
Size: 270691 Color: 2

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 387743 Color: 17
Size: 334772 Color: 13
Size: 277486 Color: 8

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 387807 Color: 3
Size: 353646 Color: 12
Size: 258548 Color: 9

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 387860 Color: 15
Size: 310826 Color: 15
Size: 301315 Color: 14

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 387860 Color: 10
Size: 334006 Color: 1
Size: 278135 Color: 2

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 387877 Color: 0
Size: 359652 Color: 7
Size: 252472 Color: 6

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 387886 Color: 19
Size: 321835 Color: 3
Size: 290280 Color: 4

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 387911 Color: 10
Size: 322906 Color: 2
Size: 289184 Color: 6

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 387983 Color: 18
Size: 356669 Color: 9
Size: 255349 Color: 9

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 387999 Color: 2
Size: 344625 Color: 12
Size: 267377 Color: 9

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 388002 Color: 10
Size: 331076 Color: 5
Size: 280923 Color: 2

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 388024 Color: 11
Size: 319000 Color: 2
Size: 292977 Color: 6

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 388025 Color: 13
Size: 340006 Color: 8
Size: 271970 Color: 15

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 388059 Color: 14
Size: 329935 Color: 4
Size: 282007 Color: 1

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 388084 Color: 8
Size: 312168 Color: 10
Size: 299749 Color: 11

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 388189 Color: 10
Size: 320478 Color: 15
Size: 291334 Color: 0

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 388237 Color: 2
Size: 311795 Color: 5
Size: 299969 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 388247 Color: 2
Size: 309631 Color: 3
Size: 302123 Color: 17

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 388255 Color: 0
Size: 344263 Color: 7
Size: 267483 Color: 4

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 388280 Color: 9
Size: 306487 Color: 0
Size: 305234 Color: 16

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 388286 Color: 5
Size: 314273 Color: 6
Size: 297442 Color: 11

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 388322 Color: 3
Size: 327656 Color: 10
Size: 284023 Color: 19

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 388342 Color: 7
Size: 351069 Color: 19
Size: 260590 Color: 3

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 388365 Color: 17
Size: 350139 Color: 18
Size: 261497 Color: 12

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 388397 Color: 5
Size: 317963 Color: 3
Size: 293641 Color: 17

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 388407 Color: 7
Size: 308347 Color: 4
Size: 303247 Color: 13

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 388456 Color: 2
Size: 314808 Color: 11
Size: 296737 Color: 3

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 388625 Color: 0
Size: 323001 Color: 11
Size: 288375 Color: 2

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 388540 Color: 16
Size: 342900 Color: 12
Size: 268561 Color: 14

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 388548 Color: 8
Size: 336989 Color: 16
Size: 274464 Color: 18

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 388579 Color: 19
Size: 340661 Color: 17
Size: 270761 Color: 12

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 388586 Color: 10
Size: 314176 Color: 3
Size: 297239 Color: 18

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 388603 Color: 15
Size: 359250 Color: 0
Size: 252148 Color: 1

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 388651 Color: 16
Size: 358819 Color: 3
Size: 252531 Color: 16

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 388652 Color: 5
Size: 311867 Color: 13
Size: 299482 Color: 11

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 388761 Color: 18
Size: 333174 Color: 5
Size: 278066 Color: 9

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 388776 Color: 11
Size: 317319 Color: 11
Size: 293906 Color: 18

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 388777 Color: 14
Size: 332635 Color: 18
Size: 278589 Color: 19

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 388814 Color: 13
Size: 347699 Color: 12
Size: 263488 Color: 8

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 389087 Color: 4
Size: 345766 Color: 8
Size: 265148 Color: 7

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 388910 Color: 19
Size: 328355 Color: 10
Size: 282736 Color: 6

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 388946 Color: 16
Size: 308817 Color: 14
Size: 302238 Color: 1

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 389102 Color: 17
Size: 356545 Color: 6
Size: 254354 Color: 13

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 389107 Color: 4
Size: 307661 Color: 5
Size: 303233 Color: 11

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 389216 Color: 6
Size: 308094 Color: 8
Size: 302691 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 389265 Color: 2
Size: 332155 Color: 11
Size: 278581 Color: 3

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 389311 Color: 10
Size: 334276 Color: 8
Size: 276414 Color: 6

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 389322 Color: 1
Size: 345569 Color: 19
Size: 265110 Color: 17

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 389365 Color: 5
Size: 324498 Color: 12
Size: 286138 Color: 17

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 389381 Color: 13
Size: 307247 Color: 4
Size: 303373 Color: 13

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 389397 Color: 10
Size: 308962 Color: 14
Size: 301642 Color: 18

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 389439 Color: 8
Size: 353133 Color: 1
Size: 257429 Color: 9

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 389458 Color: 19
Size: 305858 Color: 9
Size: 304685 Color: 7

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 389488 Color: 8
Size: 334198 Color: 1
Size: 276315 Color: 9

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 389516 Color: 6
Size: 317583 Color: 18
Size: 292902 Color: 9

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 389530 Color: 14
Size: 334092 Color: 10
Size: 276379 Color: 17

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 389538 Color: 17
Size: 327227 Color: 4
Size: 283236 Color: 16

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 389555 Color: 2
Size: 327147 Color: 19
Size: 283299 Color: 8

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 389578 Color: 16
Size: 306530 Color: 13
Size: 303893 Color: 2

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 389684 Color: 8
Size: 343048 Color: 2
Size: 267269 Color: 19

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 389690 Color: 6
Size: 330111 Color: 17
Size: 280200 Color: 5

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 389698 Color: 15
Size: 349843 Color: 10
Size: 260460 Color: 17

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 451493 Color: 2
Size: 294102 Color: 13
Size: 254406 Color: 16

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 389731 Color: 5
Size: 350236 Color: 17
Size: 260034 Color: 18

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 389798 Color: 9
Size: 344599 Color: 5
Size: 265604 Color: 8

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 389801 Color: 11
Size: 314700 Color: 0
Size: 295500 Color: 9

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 389881 Color: 11
Size: 312809 Color: 8
Size: 297311 Color: 13

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 389968 Color: 13
Size: 322307 Color: 1
Size: 287726 Color: 7

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 390119 Color: 11
Size: 350354 Color: 3
Size: 259528 Color: 5

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 390209 Color: 17
Size: 346789 Color: 1
Size: 263003 Color: 13

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 390218 Color: 3
Size: 344323 Color: 11
Size: 265460 Color: 14

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 390227 Color: 15
Size: 318223 Color: 8
Size: 291551 Color: 8

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 390297 Color: 6
Size: 326256 Color: 7
Size: 283448 Color: 3

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 390334 Color: 12
Size: 312219 Color: 9
Size: 297448 Color: 7

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 390476 Color: 4
Size: 357258 Color: 5
Size: 252267 Color: 10

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 390489 Color: 17
Size: 346477 Color: 4
Size: 263035 Color: 16

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 390550 Color: 15
Size: 322138 Color: 17
Size: 287313 Color: 6

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 390559 Color: 10
Size: 312096 Color: 14
Size: 297346 Color: 14

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 390584 Color: 11
Size: 329435 Color: 14
Size: 279982 Color: 14

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 390626 Color: 16
Size: 307821 Color: 5
Size: 301554 Color: 15

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 390652 Color: 3
Size: 320653 Color: 4
Size: 288696 Color: 2

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 390760 Color: 14
Size: 343868 Color: 12
Size: 265373 Color: 19

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 390770 Color: 19
Size: 336090 Color: 18
Size: 273141 Color: 12

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 390773 Color: 1
Size: 332140 Color: 8
Size: 277088 Color: 10

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 390793 Color: 9
Size: 328103 Color: 17
Size: 281105 Color: 19

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 390803 Color: 6
Size: 317887 Color: 4
Size: 291311 Color: 10

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 390815 Color: 1
Size: 356104 Color: 7
Size: 253082 Color: 13

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 390838 Color: 18
Size: 333606 Color: 2
Size: 275557 Color: 3

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 390884 Color: 10
Size: 345150 Color: 3
Size: 263967 Color: 17

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 390899 Color: 3
Size: 322529 Color: 0
Size: 286573 Color: 19

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 390923 Color: 16
Size: 328296 Color: 8
Size: 280782 Color: 3

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 390924 Color: 11
Size: 339231 Color: 15
Size: 269846 Color: 5

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 391039 Color: 4
Size: 310739 Color: 7
Size: 298223 Color: 19

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 391135 Color: 7
Size: 339138 Color: 12
Size: 269728 Color: 8

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 391151 Color: 14
Size: 327448 Color: 17
Size: 281402 Color: 9

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 391154 Color: 16
Size: 341976 Color: 13
Size: 266871 Color: 10

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 391289 Color: 3
Size: 355703 Color: 12
Size: 253009 Color: 17

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 391292 Color: 19
Size: 339384 Color: 11
Size: 269325 Color: 7

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 391294 Color: 6
Size: 309936 Color: 5
Size: 298771 Color: 4

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 391295 Color: 18
Size: 343221 Color: 9
Size: 265485 Color: 1

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 391321 Color: 0
Size: 311179 Color: 3
Size: 297501 Color: 17

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 391376 Color: 8
Size: 338378 Color: 12
Size: 270247 Color: 11

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 391388 Color: 16
Size: 330442 Color: 1
Size: 278171 Color: 10

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 391465 Color: 17
Size: 309768 Color: 9
Size: 298768 Color: 7

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 391584 Color: 19
Size: 344692 Color: 17
Size: 263725 Color: 18

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 391619 Color: 13
Size: 340234 Color: 2
Size: 268148 Color: 3

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 391631 Color: 8
Size: 344285 Color: 1
Size: 264085 Color: 8

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 391973 Color: 4
Size: 335670 Color: 8
Size: 272358 Color: 9

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 391677 Color: 16
Size: 308795 Color: 13
Size: 299529 Color: 1

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 391696 Color: 1
Size: 346020 Color: 11
Size: 262285 Color: 0

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 391744 Color: 16
Size: 357706 Color: 18
Size: 250551 Color: 11

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 391756 Color: 6
Size: 344072 Color: 11
Size: 264173 Color: 7

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 392014 Color: 4
Size: 336540 Color: 16
Size: 271447 Color: 10

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 391885 Color: 0
Size: 305618 Color: 1
Size: 302498 Color: 18

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 391888 Color: 13
Size: 336641 Color: 8
Size: 271472 Color: 5

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 391892 Color: 5
Size: 337717 Color: 8
Size: 270392 Color: 0

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 392044 Color: 19
Size: 320579 Color: 6
Size: 287378 Color: 4

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 392045 Color: 18
Size: 354868 Color: 7
Size: 253088 Color: 9

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 392069 Color: 10
Size: 323836 Color: 7
Size: 284096 Color: 2

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 392167 Color: 7
Size: 315069 Color: 7
Size: 292765 Color: 5

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 392182 Color: 6
Size: 315808 Color: 0
Size: 292011 Color: 16

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 392196 Color: 16
Size: 337152 Color: 19
Size: 270653 Color: 12

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 392223 Color: 2
Size: 321356 Color: 4
Size: 286422 Color: 12

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 392224 Color: 16
Size: 328991 Color: 1
Size: 278786 Color: 12

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 392227 Color: 3
Size: 314433 Color: 0
Size: 293341 Color: 2

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 392300 Color: 2
Size: 334722 Color: 1
Size: 272979 Color: 17

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 392326 Color: 10
Size: 346041 Color: 16
Size: 261634 Color: 5

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 392333 Color: 10
Size: 315402 Color: 11
Size: 292266 Color: 7

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 392357 Color: 14
Size: 311371 Color: 19
Size: 296273 Color: 4

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 392408 Color: 11
Size: 341843 Color: 1
Size: 265750 Color: 12

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 392414 Color: 13
Size: 337285 Color: 8
Size: 270302 Color: 8

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 392448 Color: 19
Size: 351856 Color: 14
Size: 255697 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 392490 Color: 5
Size: 329271 Color: 16
Size: 278240 Color: 18

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 392656 Color: 7
Size: 334607 Color: 11
Size: 272738 Color: 5

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 392695 Color: 15
Size: 311136 Color: 10
Size: 296170 Color: 1

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 392709 Color: 10
Size: 351884 Color: 16
Size: 255408 Color: 0

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 392833 Color: 6
Size: 320720 Color: 11
Size: 286448 Color: 15

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 392849 Color: 2
Size: 332451 Color: 9
Size: 274701 Color: 12

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 392905 Color: 12
Size: 326615 Color: 13
Size: 280481 Color: 6

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 392923 Color: 14
Size: 307499 Color: 15
Size: 299579 Color: 4

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 392998 Color: 6
Size: 350831 Color: 12
Size: 256172 Color: 7

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 393005 Color: 2
Size: 331644 Color: 15
Size: 275352 Color: 8

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 393029 Color: 9
Size: 332045 Color: 5
Size: 274927 Color: 11

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 393100 Color: 16
Size: 326132 Color: 6
Size: 280769 Color: 0

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 393116 Color: 5
Size: 343569 Color: 19
Size: 263316 Color: 12

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 393119 Color: 11
Size: 328411 Color: 4
Size: 278471 Color: 1

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 393128 Color: 19
Size: 328792 Color: 3
Size: 278081 Color: 8

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 393168 Color: 15
Size: 317636 Color: 12
Size: 289197 Color: 3

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 393225 Color: 15
Size: 334010 Color: 7
Size: 272766 Color: 17

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 393245 Color: 1
Size: 318704 Color: 10
Size: 288052 Color: 8

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 393383 Color: 4
Size: 336821 Color: 0
Size: 269797 Color: 9

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 393344 Color: 15
Size: 352106 Color: 17
Size: 254551 Color: 8

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 393345 Color: 15
Size: 311085 Color: 10
Size: 295571 Color: 10

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 393349 Color: 8
Size: 303874 Color: 0
Size: 302778 Color: 0

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 393356 Color: 5
Size: 304965 Color: 16
Size: 301680 Color: 2

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 393396 Color: 13
Size: 348822 Color: 14
Size: 257783 Color: 3

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 393404 Color: 6
Size: 338228 Color: 12
Size: 268369 Color: 18

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 393419 Color: 6
Size: 308197 Color: 4
Size: 298385 Color: 12

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 393464 Color: 17
Size: 348768 Color: 1
Size: 257769 Color: 18

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 393470 Color: 1
Size: 328702 Color: 13
Size: 277829 Color: 16

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 393472 Color: 2
Size: 347001 Color: 7
Size: 259528 Color: 8

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 393491 Color: 14
Size: 329761 Color: 5
Size: 276749 Color: 11

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 393674 Color: 11
Size: 349141 Color: 4
Size: 257186 Color: 1

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 393693 Color: 18
Size: 314005 Color: 14
Size: 292303 Color: 8

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 393711 Color: 9
Size: 325814 Color: 6
Size: 280476 Color: 11

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 393723 Color: 17
Size: 314488 Color: 11
Size: 291790 Color: 5

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 393748 Color: 16
Size: 344331 Color: 2
Size: 261922 Color: 3

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 393846 Color: 11
Size: 304084 Color: 6
Size: 302071 Color: 8

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 393859 Color: 14
Size: 332073 Color: 15
Size: 274069 Color: 4

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 393878 Color: 2
Size: 311990 Color: 5
Size: 294133 Color: 13

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 393882 Color: 19
Size: 319347 Color: 12
Size: 286772 Color: 9

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 393906 Color: 19
Size: 343725 Color: 11
Size: 262370 Color: 10

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 393914 Color: 3
Size: 352888 Color: 2
Size: 253199 Color: 18

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 393959 Color: 12
Size: 318567 Color: 13
Size: 287475 Color: 10

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 393959 Color: 8
Size: 340178 Color: 5
Size: 265864 Color: 8

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 394004 Color: 11
Size: 305944 Color: 9
Size: 300053 Color: 4

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 394005 Color: 9
Size: 337613 Color: 13
Size: 268383 Color: 2

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 394019 Color: 8
Size: 353578 Color: 18
Size: 252404 Color: 9

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 394044 Color: 15
Size: 322306 Color: 13
Size: 283651 Color: 18

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 394044 Color: 8
Size: 347787 Color: 8
Size: 258170 Color: 19

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 394100 Color: 15
Size: 311099 Color: 15
Size: 294802 Color: 5

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 394192 Color: 7
Size: 325884 Color: 4
Size: 279925 Color: 8

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 394194 Color: 6
Size: 346889 Color: 19
Size: 258918 Color: 10

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 394249 Color: 10
Size: 337983 Color: 8
Size: 267769 Color: 1

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 394263 Color: 10
Size: 317851 Color: 6
Size: 287887 Color: 19

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 394284 Color: 10
Size: 355657 Color: 13
Size: 250060 Color: 7

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 394387 Color: 18
Size: 342593 Color: 8
Size: 263021 Color: 15

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 394392 Color: 13
Size: 304949 Color: 4
Size: 300660 Color: 0

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 394529 Color: 7
Size: 343094 Color: 16
Size: 262378 Color: 8

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 394529 Color: 19
Size: 321845 Color: 10
Size: 283627 Color: 10

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 394562 Color: 9
Size: 328472 Color: 1
Size: 276967 Color: 12

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 394572 Color: 0
Size: 315872 Color: 19
Size: 289557 Color: 16

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 394593 Color: 5
Size: 343552 Color: 10
Size: 261856 Color: 19

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 394722 Color: 18
Size: 353144 Color: 18
Size: 252135 Color: 4

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 394730 Color: 0
Size: 343566 Color: 13
Size: 261705 Color: 1

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 394857 Color: 9
Size: 305860 Color: 13
Size: 299284 Color: 12

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 394861 Color: 15
Size: 306129 Color: 8
Size: 299011 Color: 19

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 394911 Color: 0
Size: 328284 Color: 0
Size: 276806 Color: 12

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 395007 Color: 0
Size: 351253 Color: 16
Size: 253741 Color: 15

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 395010 Color: 12
Size: 351321 Color: 12
Size: 253670 Color: 16

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 395043 Color: 14
Size: 305176 Color: 9
Size: 299782 Color: 4

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 395048 Color: 11
Size: 321028 Color: 6
Size: 283925 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 395074 Color: 12
Size: 316683 Color: 12
Size: 288244 Color: 7

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 395089 Color: 18
Size: 337368 Color: 10
Size: 267544 Color: 14

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 395165 Color: 14
Size: 346747 Color: 17
Size: 258089 Color: 18

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 395178 Color: 10
Size: 306586 Color: 16
Size: 298237 Color: 13

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 395206 Color: 7
Size: 328059 Color: 1
Size: 276736 Color: 4

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 395261 Color: 17
Size: 336766 Color: 5
Size: 267974 Color: 2

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 395380 Color: 9
Size: 304486 Color: 11
Size: 300135 Color: 13

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 395397 Color: 1
Size: 338349 Color: 15
Size: 266255 Color: 5

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 395408 Color: 6
Size: 310980 Color: 7
Size: 293613 Color: 13

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 395416 Color: 5
Size: 337542 Color: 11
Size: 267043 Color: 6

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 395423 Color: 7
Size: 338359 Color: 4
Size: 266219 Color: 3

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 395471 Color: 13
Size: 324179 Color: 16
Size: 280351 Color: 19

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 395541 Color: 0
Size: 336080 Color: 17
Size: 268380 Color: 9

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 395568 Color: 2
Size: 302926 Color: 3
Size: 301507 Color: 1

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 395621 Color: 2
Size: 316268 Color: 10
Size: 288112 Color: 1

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 395639 Color: 1
Size: 343752 Color: 14
Size: 260610 Color: 11

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 395643 Color: 19
Size: 325916 Color: 6
Size: 278442 Color: 11

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 395665 Color: 19
Size: 307986 Color: 4
Size: 296350 Color: 6

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 395747 Color: 19
Size: 331520 Color: 5
Size: 272734 Color: 7

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 395766 Color: 6
Size: 332922 Color: 19
Size: 271313 Color: 18

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 395774 Color: 9
Size: 331314 Color: 7
Size: 272913 Color: 6

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 395778 Color: 9
Size: 322230 Color: 14
Size: 281993 Color: 11

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 395771 Color: 19
Size: 320179 Color: 2
Size: 284051 Color: 10

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 395780 Color: 6
Size: 325675 Color: 4
Size: 278546 Color: 12

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 395791 Color: 18
Size: 341569 Color: 18
Size: 262641 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 395854 Color: 5
Size: 314660 Color: 13
Size: 289487 Color: 11

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 395884 Color: 15
Size: 352135 Color: 8
Size: 251982 Color: 11

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 395886 Color: 5
Size: 333354 Color: 15
Size: 270761 Color: 14

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 395908 Color: 14
Size: 306166 Color: 19
Size: 297927 Color: 18

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 395932 Color: 10
Size: 325366 Color: 0
Size: 278703 Color: 5

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 395954 Color: 17
Size: 346895 Color: 12
Size: 257152 Color: 8

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 395985 Color: 6
Size: 351937 Color: 1
Size: 252079 Color: 15

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 395992 Color: 0
Size: 312301 Color: 8
Size: 291708 Color: 13

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 396058 Color: 11
Size: 313026 Color: 13
Size: 290917 Color: 12

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 396061 Color: 0
Size: 318371 Color: 9
Size: 285569 Color: 5

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 396070 Color: 14
Size: 333603 Color: 12
Size: 270328 Color: 7

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 396071 Color: 18
Size: 305731 Color: 12
Size: 298199 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 396085 Color: 14
Size: 303046 Color: 16
Size: 300870 Color: 9

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 396096 Color: 10
Size: 343021 Color: 9
Size: 260884 Color: 8

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 396132 Color: 6
Size: 321427 Color: 10
Size: 282442 Color: 8

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 396151 Color: 17
Size: 304952 Color: 0
Size: 298898 Color: 0

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 396260 Color: 11
Size: 322264 Color: 0
Size: 281477 Color: 5

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 396337 Color: 12
Size: 306268 Color: 4
Size: 297396 Color: 14

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 396342 Color: 6
Size: 347870 Color: 3
Size: 255789 Color: 12

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 396364 Color: 0
Size: 340648 Color: 2
Size: 262989 Color: 2

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 396406 Color: 15
Size: 321325 Color: 10
Size: 282270 Color: 12

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 396458 Color: 0
Size: 322877 Color: 0
Size: 280666 Color: 5

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 396460 Color: 3
Size: 323355 Color: 15
Size: 280186 Color: 1

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 396484 Color: 4
Size: 340954 Color: 6
Size: 262563 Color: 2

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 396514 Color: 6
Size: 346842 Color: 3
Size: 256645 Color: 14

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 396618 Color: 8
Size: 316533 Color: 16
Size: 286850 Color: 15

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 396627 Color: 9
Size: 306661 Color: 4
Size: 296713 Color: 5

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 396629 Color: 1
Size: 340278 Color: 0
Size: 263094 Color: 1

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 396658 Color: 18
Size: 352618 Color: 6
Size: 250725 Color: 4

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 396753 Color: 6
Size: 338806 Color: 11
Size: 264442 Color: 13

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 396757 Color: 7
Size: 353114 Color: 2
Size: 250130 Color: 7

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 396758 Color: 17
Size: 319600 Color: 9
Size: 283643 Color: 1

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 396777 Color: 4
Size: 311075 Color: 18
Size: 292149 Color: 13

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 396788 Color: 8
Size: 312605 Color: 8
Size: 290608 Color: 5

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 396808 Color: 1
Size: 342408 Color: 13
Size: 260785 Color: 0

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 396860 Color: 6
Size: 305394 Color: 12
Size: 297747 Color: 11

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 396885 Color: 8
Size: 335152 Color: 13
Size: 267964 Color: 1

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 396914 Color: 17
Size: 310963 Color: 12
Size: 292124 Color: 15

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 396965 Color: 11
Size: 307888 Color: 9
Size: 295148 Color: 19

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 396985 Color: 5
Size: 303906 Color: 18
Size: 299110 Color: 1

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 397033 Color: 13
Size: 306853 Color: 12
Size: 296115 Color: 0

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 397044 Color: 3
Size: 343040 Color: 19
Size: 259917 Color: 7

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 397102 Color: 1
Size: 343931 Color: 16
Size: 258968 Color: 3

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 397118 Color: 18
Size: 341547 Color: 15
Size: 261336 Color: 2

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 397138 Color: 7
Size: 335235 Color: 15
Size: 267628 Color: 8

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 397258 Color: 12
Size: 338883 Color: 6
Size: 263860 Color: 6

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 397265 Color: 15
Size: 304256 Color: 2
Size: 298480 Color: 5

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 397278 Color: 13
Size: 348054 Color: 3
Size: 254669 Color: 11

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 397282 Color: 11
Size: 338119 Color: 4
Size: 264600 Color: 6

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 397327 Color: 19
Size: 321543 Color: 4
Size: 281131 Color: 18

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 397329 Color: 6
Size: 338004 Color: 14
Size: 264668 Color: 12

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 397270 Color: 5
Size: 328932 Color: 0
Size: 273799 Color: 6

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 397346 Color: 3
Size: 303502 Color: 14
Size: 299153 Color: 0

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 397401 Color: 8
Size: 307828 Color: 17
Size: 294772 Color: 19

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 397435 Color: 19
Size: 335763 Color: 15
Size: 266803 Color: 16

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 397543 Color: 11
Size: 330438 Color: 11
Size: 272020 Color: 3

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 397562 Color: 2
Size: 312293 Color: 15
Size: 290146 Color: 18

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 397580 Color: 19
Size: 306747 Color: 19
Size: 295674 Color: 6

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 397636 Color: 10
Size: 312088 Color: 1
Size: 290277 Color: 5

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 397659 Color: 8
Size: 315853 Color: 0
Size: 286489 Color: 12

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 397659 Color: 14
Size: 321946 Color: 13
Size: 280396 Color: 15

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 397669 Color: 10
Size: 313548 Color: 11
Size: 288784 Color: 2

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 397699 Color: 3
Size: 347408 Color: 4
Size: 254894 Color: 1

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 397730 Color: 10
Size: 302703 Color: 9
Size: 299568 Color: 9

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 397740 Color: 10
Size: 313455 Color: 18
Size: 288806 Color: 14

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 397773 Color: 17
Size: 340219 Color: 4
Size: 262009 Color: 19

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 397867 Color: 2
Size: 345871 Color: 14
Size: 256263 Color: 1

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 397932 Color: 10
Size: 344086 Color: 18
Size: 257983 Color: 11

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 398053 Color: 12
Size: 331294 Color: 2
Size: 270654 Color: 9

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 398070 Color: 4
Size: 322957 Color: 13
Size: 278974 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 398097 Color: 10
Size: 328896 Color: 8
Size: 273008 Color: 18

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 398162 Color: 14
Size: 304371 Color: 9
Size: 297468 Color: 17

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 398222 Color: 17
Size: 312571 Color: 18
Size: 289208 Color: 12

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 398228 Color: 9
Size: 315709 Color: 16
Size: 286064 Color: 3

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 398232 Color: 16
Size: 307022 Color: 16
Size: 294747 Color: 11

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 398273 Color: 10
Size: 340982 Color: 19
Size: 260746 Color: 5

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 398285 Color: 11
Size: 333957 Color: 0
Size: 267759 Color: 17

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 398285 Color: 10
Size: 329241 Color: 1
Size: 272475 Color: 4

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 398298 Color: 15
Size: 339410 Color: 11
Size: 262293 Color: 4

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 398323 Color: 11
Size: 326013 Color: 18
Size: 275665 Color: 6

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 398386 Color: 18
Size: 325320 Color: 1
Size: 276295 Color: 11

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 398398 Color: 19
Size: 323617 Color: 3
Size: 277986 Color: 2

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 398512 Color: 12
Size: 318023 Color: 12
Size: 283466 Color: 9

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 398514 Color: 3
Size: 339328 Color: 10
Size: 262159 Color: 9

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 398565 Color: 6
Size: 334845 Color: 2
Size: 266591 Color: 18

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 398643 Color: 15
Size: 321230 Color: 13
Size: 280128 Color: 6

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 398644 Color: 13
Size: 316897 Color: 19
Size: 284460 Color: 16

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 398672 Color: 10
Size: 321944 Color: 6
Size: 279385 Color: 18

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 398724 Color: 12
Size: 345968 Color: 18
Size: 255309 Color: 17

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 398725 Color: 8
Size: 343412 Color: 19
Size: 257864 Color: 4

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 398762 Color: 15
Size: 348999 Color: 17
Size: 252240 Color: 9

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 399029 Color: 6
Size: 342113 Color: 18
Size: 258859 Color: 0

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 399157 Color: 4
Size: 325034 Color: 6
Size: 275810 Color: 8

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 399160 Color: 4
Size: 334832 Color: 2
Size: 266009 Color: 10

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 399360 Color: 17
Size: 319943 Color: 4
Size: 280698 Color: 5

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 399432 Color: 16
Size: 337806 Color: 13
Size: 262763 Color: 8

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 399451 Color: 14
Size: 318261 Color: 11
Size: 282289 Color: 10

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 399491 Color: 1
Size: 307197 Color: 14
Size: 293313 Color: 12

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 399507 Color: 6
Size: 308814 Color: 1
Size: 291680 Color: 0

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 399514 Color: 18
Size: 337841 Color: 11
Size: 262646 Color: 17

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 399536 Color: 5
Size: 348771 Color: 1
Size: 251694 Color: 2

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 399546 Color: 19
Size: 300357 Color: 12
Size: 300098 Color: 19

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 399576 Color: 8
Size: 334074 Color: 5
Size: 266351 Color: 5

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 399608 Color: 7
Size: 326681 Color: 3
Size: 273712 Color: 6

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 399641 Color: 12
Size: 347946 Color: 1
Size: 252414 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 399658 Color: 9
Size: 303020 Color: 8
Size: 297323 Color: 1

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 399670 Color: 8
Size: 302200 Color: 14
Size: 298131 Color: 17

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 399705 Color: 2
Size: 301916 Color: 19
Size: 298380 Color: 2

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 399749 Color: 16
Size: 302759 Color: 10
Size: 297493 Color: 8

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 399755 Color: 18
Size: 321101 Color: 15
Size: 279145 Color: 15

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 399776 Color: 7
Size: 313012 Color: 6
Size: 287213 Color: 12

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 399852 Color: 0
Size: 307906 Color: 7
Size: 292243 Color: 4

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 399779 Color: 19
Size: 341089 Color: 3
Size: 259133 Color: 12

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 399781 Color: 13
Size: 300184 Color: 13
Size: 300036 Color: 15

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 399820 Color: 3
Size: 315068 Color: 17
Size: 285113 Color: 7

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 399863 Color: 11
Size: 317951 Color: 2
Size: 282187 Color: 1

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 399992 Color: 18
Size: 345257 Color: 5
Size: 254752 Color: 0

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 400025 Color: 9
Size: 331835 Color: 5
Size: 268141 Color: 19

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 400042 Color: 15
Size: 337176 Color: 19
Size: 262783 Color: 3

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 400068 Color: 2
Size: 327001 Color: 11
Size: 272932 Color: 14

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 400070 Color: 12
Size: 343087 Color: 13
Size: 256844 Color: 14

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 400074 Color: 2
Size: 328219 Color: 4
Size: 271708 Color: 18

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 400184 Color: 0
Size: 311382 Color: 6
Size: 288435 Color: 8

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 400116 Color: 4
Size: 320511 Color: 10
Size: 279374 Color: 8

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 400123 Color: 8
Size: 348878 Color: 2
Size: 251000 Color: 5

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 400181 Color: 19
Size: 337995 Color: 4
Size: 261825 Color: 15

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 400202 Color: 9
Size: 315488 Color: 17
Size: 284311 Color: 3

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 400229 Color: 11
Size: 311384 Color: 13
Size: 288388 Color: 6

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 400282 Color: 0
Size: 319244 Color: 15
Size: 280475 Color: 4

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 400256 Color: 1
Size: 313403 Color: 8
Size: 286342 Color: 19

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 400262 Color: 1
Size: 326868 Color: 1
Size: 272871 Color: 15

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 400332 Color: 16
Size: 312289 Color: 8
Size: 287380 Color: 10

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 400349 Color: 9
Size: 324258 Color: 3
Size: 275394 Color: 3

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 400352 Color: 3
Size: 327411 Color: 15
Size: 272238 Color: 17

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 400413 Color: 1
Size: 346196 Color: 3
Size: 253392 Color: 6

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 400421 Color: 9
Size: 301132 Color: 11
Size: 298448 Color: 0

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 400442 Color: 8
Size: 328025 Color: 12
Size: 271534 Color: 18

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 400451 Color: 17
Size: 314312 Color: 11
Size: 285238 Color: 10

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 400457 Color: 15
Size: 336509 Color: 3
Size: 263035 Color: 13

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 400496 Color: 7
Size: 323586 Color: 7
Size: 275919 Color: 3

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 400503 Color: 3
Size: 319444 Color: 15
Size: 280054 Color: 6

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 400578 Color: 11
Size: 302777 Color: 0
Size: 296646 Color: 3

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 400648 Color: 2
Size: 349243 Color: 16
Size: 250110 Color: 7

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 400697 Color: 19
Size: 301979 Color: 11
Size: 297325 Color: 18

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 400708 Color: 10
Size: 332485 Color: 3
Size: 266808 Color: 14

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 400797 Color: 3
Size: 309674 Color: 10
Size: 289530 Color: 17

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 400805 Color: 4
Size: 320990 Color: 5
Size: 278206 Color: 18

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 400867 Color: 15
Size: 308182 Color: 17
Size: 290952 Color: 5

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 400958 Color: 6
Size: 340784 Color: 1
Size: 258259 Color: 2

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 400871 Color: 15
Size: 306798 Color: 1
Size: 292332 Color: 8

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 400882 Color: 2
Size: 313934 Color: 15
Size: 285185 Color: 13

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 400914 Color: 17
Size: 330620 Color: 10
Size: 268467 Color: 10

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 400927 Color: 10
Size: 304806 Color: 11
Size: 294268 Color: 11

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 401101 Color: 1
Size: 333006 Color: 6
Size: 265894 Color: 3

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 401145 Color: 15
Size: 304670 Color: 13
Size: 294186 Color: 11

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 401147 Color: 9
Size: 326764 Color: 19
Size: 272090 Color: 15

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 401182 Color: 2
Size: 327890 Color: 10
Size: 270929 Color: 15

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 401198 Color: 13
Size: 322066 Color: 3
Size: 276737 Color: 18

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 401270 Color: 9
Size: 304078 Color: 17
Size: 294653 Color: 14

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 401387 Color: 2
Size: 333277 Color: 9
Size: 265337 Color: 6

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 401432 Color: 2
Size: 302155 Color: 16
Size: 296414 Color: 16

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 401506 Color: 1
Size: 337858 Color: 3
Size: 260637 Color: 14

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 401527 Color: 0
Size: 302819 Color: 9
Size: 295655 Color: 18

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 401552 Color: 11
Size: 308093 Color: 9
Size: 290356 Color: 13

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 401588 Color: 12
Size: 312712 Color: 13
Size: 285701 Color: 4

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 401639 Color: 0
Size: 320845 Color: 13
Size: 277517 Color: 10

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 401779 Color: 16
Size: 342765 Color: 7
Size: 255457 Color: 13

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 401917 Color: 0
Size: 300169 Color: 7
Size: 297915 Color: 3

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 402083 Color: 0
Size: 307744 Color: 19
Size: 290174 Color: 15

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 402105 Color: 16
Size: 319605 Color: 18
Size: 278291 Color: 10

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 402106 Color: 17
Size: 322672 Color: 2
Size: 275223 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 402121 Color: 16
Size: 303716 Color: 14
Size: 294164 Color: 15

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 402189 Color: 7
Size: 305032 Color: 15
Size: 292780 Color: 5

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 402211 Color: 4
Size: 319151 Color: 19
Size: 278639 Color: 11

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 402219 Color: 6
Size: 309487 Color: 6
Size: 288295 Color: 2

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 402245 Color: 11
Size: 313754 Color: 10
Size: 284002 Color: 15

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 402248 Color: 1
Size: 308922 Color: 19
Size: 288831 Color: 6

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 402289 Color: 11
Size: 300789 Color: 17
Size: 296923 Color: 0

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 402320 Color: 1
Size: 336318 Color: 18
Size: 261363 Color: 8

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 402335 Color: 19
Size: 343624 Color: 12
Size: 254042 Color: 2

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 402408 Color: 18
Size: 317940 Color: 17
Size: 279653 Color: 1

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 402420 Color: 15
Size: 320802 Color: 12
Size: 276779 Color: 19

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 402456 Color: 7
Size: 347300 Color: 3
Size: 250245 Color: 19

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 402509 Color: 8
Size: 304584 Color: 1
Size: 292908 Color: 1

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 402660 Color: 0
Size: 315877 Color: 11
Size: 281464 Color: 11

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 402524 Color: 7
Size: 304979 Color: 8
Size: 292498 Color: 4

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 402660 Color: 2
Size: 317029 Color: 7
Size: 280312 Color: 8

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 402734 Color: 1
Size: 338069 Color: 9
Size: 259198 Color: 11

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 402785 Color: 6
Size: 310371 Color: 0
Size: 286845 Color: 13

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 402797 Color: 7
Size: 311122 Color: 3
Size: 286082 Color: 18

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 402830 Color: 3
Size: 326865 Color: 11
Size: 270306 Color: 2

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 402851 Color: 1
Size: 338139 Color: 19
Size: 259011 Color: 2

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 402885 Color: 16
Size: 304311 Color: 19
Size: 292805 Color: 13

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 402930 Color: 16
Size: 309556 Color: 6
Size: 287515 Color: 8

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 402937 Color: 12
Size: 302451 Color: 8
Size: 294613 Color: 0

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 403019 Color: 8
Size: 336837 Color: 7
Size: 260145 Color: 14

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 403028 Color: 9
Size: 328378 Color: 17
Size: 268595 Color: 19

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 403078 Color: 17
Size: 330358 Color: 9
Size: 266565 Color: 12

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 403085 Color: 4
Size: 330970 Color: 11
Size: 265946 Color: 10

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 403156 Color: 6
Size: 308119 Color: 7
Size: 288726 Color: 10

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 403231 Color: 2
Size: 323905 Color: 1
Size: 272865 Color: 5

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 403272 Color: 0
Size: 343648 Color: 19
Size: 253081 Color: 11

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 403259 Color: 17
Size: 344631 Color: 9
Size: 252111 Color: 5

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 403269 Color: 15
Size: 334439 Color: 16
Size: 262293 Color: 6

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 403293 Color: 18
Size: 309878 Color: 12
Size: 286830 Color: 2

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 403376 Color: 3
Size: 308222 Color: 18
Size: 288403 Color: 12

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 403407 Color: 10
Size: 339895 Color: 19
Size: 256699 Color: 7

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 403453 Color: 10
Size: 312125 Color: 6
Size: 284423 Color: 7

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 403513 Color: 17
Size: 337022 Color: 17
Size: 259466 Color: 1

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 403518 Color: 4
Size: 314001 Color: 15
Size: 282482 Color: 16

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 403552 Color: 14
Size: 326721 Color: 2
Size: 269728 Color: 17

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 403592 Color: 15
Size: 331725 Color: 7
Size: 264684 Color: 19

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 403792 Color: 14
Size: 341336 Color: 5
Size: 254873 Color: 7

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 403940 Color: 0
Size: 316342 Color: 3
Size: 279719 Color: 17

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 403813 Color: 3
Size: 344108 Color: 7
Size: 252080 Color: 4

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 403858 Color: 15
Size: 303693 Color: 7
Size: 292450 Color: 12

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 404064 Color: 15
Size: 324177 Color: 9
Size: 271760 Color: 1

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 404091 Color: 11
Size: 321641 Color: 17
Size: 274269 Color: 10

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 404109 Color: 4
Size: 330611 Color: 0
Size: 265281 Color: 12

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 404174 Color: 5
Size: 337875 Color: 4
Size: 257952 Color: 19

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 404267 Color: 8
Size: 300747 Color: 15
Size: 294987 Color: 3

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 404285 Color: 18
Size: 331132 Color: 11
Size: 264584 Color: 8

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 404387 Color: 16
Size: 305649 Color: 6
Size: 289965 Color: 2

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 404387 Color: 4
Size: 323319 Color: 8
Size: 272295 Color: 19

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 404446 Color: 9
Size: 325804 Color: 12
Size: 269751 Color: 0

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 404491 Color: 14
Size: 337739 Color: 14
Size: 257771 Color: 8

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 404519 Color: 10
Size: 323619 Color: 6
Size: 271863 Color: 17

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 404528 Color: 16
Size: 308823 Color: 15
Size: 286650 Color: 13

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 404627 Color: 3
Size: 341342 Color: 14
Size: 254032 Color: 1

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 404701 Color: 15
Size: 303574 Color: 19
Size: 291726 Color: 3

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 404718 Color: 9
Size: 306536 Color: 5
Size: 288747 Color: 10

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 404735 Color: 8
Size: 342810 Color: 13
Size: 252456 Color: 1

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 404753 Color: 8
Size: 326678 Color: 2
Size: 268570 Color: 9

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 404813 Color: 16
Size: 329132 Color: 13
Size: 266056 Color: 3

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 404870 Color: 8
Size: 339371 Color: 6
Size: 255760 Color: 19

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 404896 Color: 12
Size: 332552 Color: 1
Size: 262553 Color: 10

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 404970 Color: 1
Size: 322111 Color: 7
Size: 272920 Color: 15

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 404926 Color: 11
Size: 323196 Color: 0
Size: 271879 Color: 3

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 404966 Color: 9
Size: 300561 Color: 14
Size: 294474 Color: 18

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 404981 Color: 6
Size: 333859 Color: 14
Size: 261161 Color: 2

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 405020 Color: 13
Size: 316217 Color: 5
Size: 278764 Color: 3

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 404989 Color: 6
Size: 326416 Color: 19
Size: 268596 Color: 10

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 405039 Color: 12
Size: 335048 Color: 1
Size: 259914 Color: 15

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 405074 Color: 4
Size: 309954 Color: 0
Size: 284973 Color: 14

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 405087 Color: 11
Size: 329333 Color: 17
Size: 265581 Color: 12

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 405090 Color: 5
Size: 321669 Color: 10
Size: 273242 Color: 3

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 405119 Color: 0
Size: 300222 Color: 8
Size: 294660 Color: 1

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 405166 Color: 12
Size: 331333 Color: 0
Size: 263502 Color: 8

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 405255 Color: 4
Size: 299902 Color: 14
Size: 294844 Color: 17

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 405281 Color: 6
Size: 301139 Color: 4
Size: 293581 Color: 4

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 405295 Color: 14
Size: 320852 Color: 9
Size: 273854 Color: 11

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 405297 Color: 6
Size: 329329 Color: 14
Size: 265375 Color: 5

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 405310 Color: 9
Size: 337054 Color: 1
Size: 257637 Color: 18

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 405315 Color: 4
Size: 343330 Color: 0
Size: 251356 Color: 4

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 405464 Color: 5
Size: 313576 Color: 2
Size: 280961 Color: 7

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 405550 Color: 14
Size: 341204 Color: 18
Size: 253247 Color: 10

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 405569 Color: 14
Size: 300382 Color: 5
Size: 294050 Color: 15

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 405674 Color: 4
Size: 328928 Color: 5
Size: 265399 Color: 13

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 405779 Color: 16
Size: 341785 Color: 10
Size: 252437 Color: 11

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 405867 Color: 14
Size: 308569 Color: 1
Size: 285565 Color: 15

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 405872 Color: 13
Size: 303185 Color: 5
Size: 290944 Color: 0

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 405941 Color: 2
Size: 325776 Color: 5
Size: 268284 Color: 7

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 405994 Color: 8
Size: 326657 Color: 7
Size: 267350 Color: 6

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 405998 Color: 5
Size: 337925 Color: 18
Size: 256078 Color: 15

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 406020 Color: 15
Size: 299028 Color: 7
Size: 294953 Color: 19

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 406003 Color: 6
Size: 324657 Color: 18
Size: 269341 Color: 4

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 406025 Color: 10
Size: 323144 Color: 1
Size: 270832 Color: 9

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 406225 Color: 0
Size: 333139 Color: 19
Size: 260637 Color: 4

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 406172 Color: 11
Size: 317616 Color: 9
Size: 276213 Color: 9

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 406180 Color: 12
Size: 297530 Color: 8
Size: 296291 Color: 16

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 406220 Color: 5
Size: 332775 Color: 4
Size: 261006 Color: 16

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 406269 Color: 13
Size: 336933 Color: 1
Size: 256799 Color: 15

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 406311 Color: 19
Size: 325763 Color: 0
Size: 267927 Color: 16

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 406372 Color: 8
Size: 310087 Color: 11
Size: 283542 Color: 17

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 406462 Color: 17
Size: 334515 Color: 7
Size: 259024 Color: 4

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 406643 Color: 16
Size: 335112 Color: 9
Size: 258246 Color: 18

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 406658 Color: 18
Size: 319965 Color: 3
Size: 273378 Color: 6

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 406665 Color: 10
Size: 308613 Color: 6
Size: 284723 Color: 11

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 406766 Color: 14
Size: 305445 Color: 15
Size: 287790 Color: 1

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 406772 Color: 8
Size: 307286 Color: 0
Size: 285943 Color: 12

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 406780 Color: 2
Size: 328839 Color: 7
Size: 264382 Color: 5

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 406806 Color: 5
Size: 319323 Color: 18
Size: 273872 Color: 18

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 406815 Color: 11
Size: 326917 Color: 2
Size: 266269 Color: 4

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 406817 Color: 13
Size: 300081 Color: 17
Size: 293103 Color: 5

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 406838 Color: 18
Size: 306894 Color: 5
Size: 286269 Color: 6

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 406889 Color: 7
Size: 297813 Color: 0
Size: 295299 Color: 13

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 406906 Color: 15
Size: 313757 Color: 4
Size: 279338 Color: 10

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 406929 Color: 8
Size: 325543 Color: 13
Size: 267529 Color: 10

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 406939 Color: 4
Size: 317244 Color: 18
Size: 275818 Color: 17

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 406978 Color: 17
Size: 318374 Color: 15
Size: 274649 Color: 13

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 406995 Color: 13
Size: 315626 Color: 18
Size: 277380 Color: 14

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 407072 Color: 9
Size: 330095 Color: 14
Size: 262834 Color: 16

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 407088 Color: 9
Size: 326235 Color: 16
Size: 266678 Color: 3

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 407107 Color: 15
Size: 305045 Color: 1
Size: 287849 Color: 19

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 407108 Color: 14
Size: 301019 Color: 8
Size: 291874 Color: 10

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 407160 Color: 2
Size: 301934 Color: 14
Size: 290907 Color: 19

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 407169 Color: 11
Size: 337244 Color: 0
Size: 255588 Color: 7

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 407195 Color: 9
Size: 318939 Color: 18
Size: 273867 Color: 15

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 407210 Color: 6
Size: 305747 Color: 16
Size: 287044 Color: 19

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 407239 Color: 1
Size: 297461 Color: 2
Size: 295301 Color: 19

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 407350 Color: 12
Size: 329850 Color: 1
Size: 262801 Color: 11

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 407419 Color: 17
Size: 312909 Color: 11
Size: 279673 Color: 6

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 407427 Color: 11
Size: 297147 Color: 13
Size: 295427 Color: 5

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 407481 Color: 12
Size: 333631 Color: 5
Size: 258889 Color: 0

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 407557 Color: 9
Size: 305035 Color: 16
Size: 287409 Color: 11

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 407569 Color: 1
Size: 336913 Color: 6
Size: 255519 Color: 11

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 407594 Color: 16
Size: 301424 Color: 19
Size: 290983 Color: 5

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 407596 Color: 8
Size: 312451 Color: 14
Size: 279954 Color: 18

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 407633 Color: 12
Size: 340383 Color: 8
Size: 251985 Color: 18

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 407702 Color: 12
Size: 300889 Color: 15
Size: 291410 Color: 6

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 407791 Color: 0
Size: 335174 Color: 17
Size: 257036 Color: 9

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 407709 Color: 4
Size: 335250 Color: 17
Size: 257042 Color: 13

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 407763 Color: 8
Size: 318729 Color: 9
Size: 273509 Color: 7

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 407765 Color: 1
Size: 320394 Color: 15
Size: 271842 Color: 7

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 407870 Color: 17
Size: 321114 Color: 10
Size: 271017 Color: 1

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 407880 Color: 14
Size: 335503 Color: 0
Size: 256618 Color: 8

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 407885 Color: 12
Size: 339482 Color: 17
Size: 252634 Color: 19

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 407885 Color: 10
Size: 335296 Color: 2
Size: 256820 Color: 6

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 407886 Color: 15
Size: 304703 Color: 7
Size: 287412 Color: 9

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 407900 Color: 10
Size: 327245 Color: 19
Size: 264856 Color: 2

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 407923 Color: 13
Size: 329295 Color: 13
Size: 262783 Color: 5

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 407985 Color: 8
Size: 303376 Color: 12
Size: 288640 Color: 6

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 408031 Color: 8
Size: 328154 Color: 1
Size: 263816 Color: 14

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 408062 Color: 13
Size: 296431 Color: 7
Size: 295508 Color: 12

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 408253 Color: 7
Size: 333048 Color: 17
Size: 258700 Color: 8

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 408394 Color: 5
Size: 340947 Color: 13
Size: 250660 Color: 19

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 408412 Color: 7
Size: 321750 Color: 3
Size: 269839 Color: 4

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 408436 Color: 18
Size: 337871 Color: 10
Size: 253694 Color: 1

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 408494 Color: 17
Size: 316748 Color: 8
Size: 274759 Color: 10

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 408503 Color: 15
Size: 316337 Color: 12
Size: 275161 Color: 9

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 408505 Color: 4
Size: 332937 Color: 8
Size: 258559 Color: 9

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 408517 Color: 15
Size: 336682 Color: 10
Size: 254802 Color: 4

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 408601 Color: 11
Size: 305600 Color: 13
Size: 285800 Color: 15

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 408608 Color: 18
Size: 328919 Color: 15
Size: 262474 Color: 2

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 408619 Color: 11
Size: 324594 Color: 7
Size: 266788 Color: 16

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 408651 Color: 9
Size: 308092 Color: 19
Size: 283258 Color: 9

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 408713 Color: 17
Size: 296932 Color: 17
Size: 294356 Color: 11

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 408726 Color: 4
Size: 324092 Color: 5
Size: 267183 Color: 6

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 408812 Color: 12
Size: 326621 Color: 2
Size: 264568 Color: 11

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 408829 Color: 8
Size: 338877 Color: 13
Size: 252295 Color: 6

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 408843 Color: 5
Size: 331427 Color: 16
Size: 259731 Color: 13

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 408985 Color: 3
Size: 325948 Color: 2
Size: 265068 Color: 4

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 409042 Color: 0
Size: 339220 Color: 1
Size: 251739 Color: 5

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 409053 Color: 19
Size: 309762 Color: 13
Size: 281186 Color: 2

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 409057 Color: 10
Size: 310681 Color: 15
Size: 280263 Color: 3

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 409068 Color: 13
Size: 316185 Color: 6
Size: 274748 Color: 16

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 409168 Color: 18
Size: 328848 Color: 8
Size: 261985 Color: 10

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 409204 Color: 18
Size: 312173 Color: 11
Size: 278624 Color: 10

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 409213 Color: 4
Size: 317253 Color: 6
Size: 273535 Color: 17

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 409266 Color: 19
Size: 316488 Color: 15
Size: 274247 Color: 18

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 409338 Color: 19
Size: 324251 Color: 12
Size: 266412 Color: 11

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 409343 Color: 17
Size: 319500 Color: 9
Size: 271158 Color: 19

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 409433 Color: 19
Size: 313648 Color: 7
Size: 276920 Color: 18

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 409500 Color: 2
Size: 318611 Color: 16
Size: 271890 Color: 10

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 409533 Color: 12
Size: 327963 Color: 6
Size: 262505 Color: 2

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 409560 Color: 13
Size: 320121 Color: 14
Size: 270320 Color: 2

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 409582 Color: 18
Size: 322476 Color: 19
Size: 267943 Color: 2

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 409604 Color: 15
Size: 312682 Color: 4
Size: 277715 Color: 13

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 409614 Color: 11
Size: 339269 Color: 13
Size: 251118 Color: 14

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 409690 Color: 2
Size: 296526 Color: 18
Size: 293785 Color: 1

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 409692 Color: 11
Size: 300624 Color: 12
Size: 289685 Color: 5

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 409721 Color: 5
Size: 303472 Color: 4
Size: 286808 Color: 10

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 409821 Color: 1
Size: 299975 Color: 15
Size: 290205 Color: 15

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 409900 Color: 9
Size: 326753 Color: 3
Size: 263348 Color: 2

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 409957 Color: 17
Size: 339786 Color: 15
Size: 250258 Color: 8

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 409992 Color: 9
Size: 321692 Color: 17
Size: 268317 Color: 4

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 410066 Color: 1
Size: 323027 Color: 15
Size: 266908 Color: 10

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 410051 Color: 16
Size: 300940 Color: 1
Size: 289010 Color: 5

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 410064 Color: 2
Size: 297317 Color: 3
Size: 292620 Color: 17

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 410178 Color: 4
Size: 314659 Color: 15
Size: 275164 Color: 19

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 410213 Color: 5
Size: 318706 Color: 3
Size: 271082 Color: 19

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 410214 Color: 3
Size: 319756 Color: 17
Size: 270031 Color: 7

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 410293 Color: 11
Size: 310392 Color: 18
Size: 279316 Color: 7

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 410297 Color: 9
Size: 328300 Color: 3
Size: 261404 Color: 0

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 410440 Color: 1
Size: 333626 Color: 8
Size: 255935 Color: 11

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 410357 Color: 15
Size: 327567 Color: 6
Size: 262077 Color: 17

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 410395 Color: 5
Size: 326905 Color: 8
Size: 262701 Color: 9

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 410476 Color: 10
Size: 319112 Color: 3
Size: 270413 Color: 11

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 410516 Color: 16
Size: 339167 Color: 8
Size: 250318 Color: 19

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 410618 Color: 2
Size: 333426 Color: 6
Size: 255957 Color: 5

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 410782 Color: 1
Size: 314345 Color: 5
Size: 274874 Color: 8

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 410652 Color: 3
Size: 327345 Color: 13
Size: 262004 Color: 10

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 410809 Color: 11
Size: 318163 Color: 12
Size: 271029 Color: 2

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 410848 Color: 6
Size: 329818 Color: 9
Size: 259335 Color: 14

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 410875 Color: 7
Size: 323965 Color: 16
Size: 265161 Color: 6

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 410981 Color: 1
Size: 318468 Color: 5
Size: 270552 Color: 10

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 410904 Color: 4
Size: 337869 Color: 11
Size: 251228 Color: 9

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 410957 Color: 18
Size: 300570 Color: 8
Size: 288474 Color: 19

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 410976 Color: 11
Size: 335566 Color: 16
Size: 253459 Color: 16

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 410980 Color: 17
Size: 303954 Color: 5
Size: 285067 Color: 11

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 411035 Color: 15
Size: 318658 Color: 10
Size: 270308 Color: 17

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 411082 Color: 14
Size: 331165 Color: 13
Size: 257754 Color: 1

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 411171 Color: 6
Size: 318743 Color: 2
Size: 270087 Color: 11

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 411172 Color: 2
Size: 306149 Color: 19
Size: 282680 Color: 2

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 411292 Color: 11
Size: 318787 Color: 5
Size: 269922 Color: 3

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 411404 Color: 9
Size: 308516 Color: 5
Size: 280081 Color: 18

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 411435 Color: 8
Size: 300624 Color: 13
Size: 287942 Color: 11

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 411463 Color: 7
Size: 335068 Color: 10
Size: 253470 Color: 14

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 411522 Color: 1
Size: 322433 Color: 15
Size: 266046 Color: 17

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 411555 Color: 12
Size: 324686 Color: 9
Size: 263760 Color: 14

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 411578 Color: 4
Size: 307460 Color: 15
Size: 280963 Color: 7

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 411680 Color: 5
Size: 313454 Color: 12
Size: 274867 Color: 3

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 411827 Color: 4
Size: 327564 Color: 13
Size: 260610 Color: 17

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 411828 Color: 17
Size: 308804 Color: 12
Size: 279369 Color: 15

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 411889 Color: 13
Size: 325018 Color: 3
Size: 263094 Color: 1

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 411965 Color: 15
Size: 294083 Color: 9
Size: 293953 Color: 8

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 411972 Color: 14
Size: 331110 Color: 0
Size: 256919 Color: 0

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 412011 Color: 8
Size: 320202 Color: 5
Size: 267788 Color: 16

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 412036 Color: 6
Size: 331685 Color: 14
Size: 256280 Color: 8

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 412100 Color: 5
Size: 295660 Color: 14
Size: 292241 Color: 17

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 412119 Color: 7
Size: 315887 Color: 19
Size: 271995 Color: 1

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 412125 Color: 8
Size: 334969 Color: 9
Size: 252907 Color: 19

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 412133 Color: 4
Size: 318614 Color: 18
Size: 269254 Color: 13

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 412162 Color: 0
Size: 298380 Color: 4
Size: 289459 Color: 7

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 412202 Color: 7
Size: 311355 Color: 4
Size: 276444 Color: 15

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 412265 Color: 18
Size: 326334 Color: 0
Size: 261402 Color: 12

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 412267 Color: 12
Size: 336332 Color: 1
Size: 251402 Color: 3

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 412312 Color: 3
Size: 321803 Color: 7
Size: 265886 Color: 8

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 412319 Color: 15
Size: 332603 Color: 16
Size: 255079 Color: 4

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 412323 Color: 12
Size: 330825 Color: 19
Size: 256853 Color: 4

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 412401 Color: 12
Size: 330980 Color: 16
Size: 256620 Color: 3

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 412440 Color: 19
Size: 326859 Color: 0
Size: 260702 Color: 6

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 412493 Color: 7
Size: 307340 Color: 4
Size: 280168 Color: 1

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 412519 Color: 18
Size: 326251 Color: 16
Size: 261231 Color: 12

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 412567 Color: 8
Size: 327325 Color: 15
Size: 260109 Color: 5

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 412632 Color: 5
Size: 319121 Color: 18
Size: 268248 Color: 15

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 412655 Color: 14
Size: 328977 Color: 13
Size: 258369 Color: 6

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 412661 Color: 13
Size: 319387 Color: 4
Size: 267953 Color: 3

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 412684 Color: 3
Size: 332838 Color: 8
Size: 254479 Color: 10

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 412712 Color: 19
Size: 294943 Color: 14
Size: 292346 Color: 1

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 412751 Color: 19
Size: 323876 Color: 14
Size: 263374 Color: 12

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 412784 Color: 12
Size: 303090 Color: 17
Size: 284127 Color: 11

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 412811 Color: 0
Size: 334868 Color: 17
Size: 252322 Color: 9

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 412876 Color: 9
Size: 308501 Color: 14
Size: 278624 Color: 1

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 412929 Color: 18
Size: 293966 Color: 9
Size: 293106 Color: 6

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 412999 Color: 8
Size: 296142 Color: 0
Size: 290860 Color: 10

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 413151 Color: 15
Size: 294366 Color: 3
Size: 292484 Color: 10

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 413290 Color: 17
Size: 315548 Color: 7
Size: 271163 Color: 15

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 413296 Color: 19
Size: 306951 Color: 6
Size: 279754 Color: 8

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 413337 Color: 11
Size: 302216 Color: 12
Size: 284448 Color: 1

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 413360 Color: 10
Size: 302480 Color: 0
Size: 284161 Color: 18

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 413395 Color: 9
Size: 309924 Color: 3
Size: 276682 Color: 18

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 413419 Color: 0
Size: 334124 Color: 14
Size: 252458 Color: 10

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 413474 Color: 14
Size: 303029 Color: 12
Size: 283498 Color: 6

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 413536 Color: 18
Size: 323695 Color: 10
Size: 262770 Color: 11

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 413656 Color: 5
Size: 317674 Color: 16
Size: 268671 Color: 3

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 413703 Color: 12
Size: 328568 Color: 1
Size: 257730 Color: 6

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 413728 Color: 8
Size: 332463 Color: 2
Size: 253810 Color: 18

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 413737 Color: 13
Size: 309992 Color: 16
Size: 276272 Color: 17

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 413751 Color: 13
Size: 302085 Color: 2
Size: 284165 Color: 9

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 413797 Color: 0
Size: 296978 Color: 11
Size: 289226 Color: 15

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 413830 Color: 15
Size: 297273 Color: 19
Size: 288898 Color: 7

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 413857 Color: 19
Size: 329026 Color: 9
Size: 257118 Color: 1

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 413869 Color: 18
Size: 318157 Color: 11
Size: 267975 Color: 5

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 413871 Color: 11
Size: 318276 Color: 17
Size: 267854 Color: 15

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 413884 Color: 2
Size: 325112 Color: 16
Size: 261005 Color: 8

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 413897 Color: 12
Size: 333404 Color: 6
Size: 252700 Color: 4

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 413985 Color: 6
Size: 334781 Color: 0
Size: 251235 Color: 2

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 414115 Color: 13
Size: 326603 Color: 3
Size: 259283 Color: 1

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 414249 Color: 6
Size: 310427 Color: 9
Size: 275325 Color: 11

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 414377 Color: 3
Size: 326350 Color: 10
Size: 259274 Color: 13

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 414431 Color: 3
Size: 306261 Color: 5
Size: 279309 Color: 19

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 414438 Color: 4
Size: 293149 Color: 15
Size: 292414 Color: 16

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 414508 Color: 14
Size: 298404 Color: 16
Size: 287089 Color: 15

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 414537 Color: 10
Size: 310776 Color: 1
Size: 274688 Color: 12

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 414611 Color: 2
Size: 329913 Color: 3
Size: 255477 Color: 6

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 414626 Color: 11
Size: 318123 Color: 16
Size: 267252 Color: 3

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 414636 Color: 0
Size: 321402 Color: 7
Size: 263963 Color: 10

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 414638 Color: 0
Size: 306104 Color: 13
Size: 279259 Color: 13

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 414639 Color: 4
Size: 322434 Color: 3
Size: 262928 Color: 17

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 414675 Color: 5
Size: 311856 Color: 12
Size: 273470 Color: 16

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 414700 Color: 2
Size: 299424 Color: 8
Size: 285877 Color: 14

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 414927 Color: 1
Size: 294613 Color: 5
Size: 290461 Color: 2

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 414764 Color: 0
Size: 330068 Color: 18
Size: 255169 Color: 14

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 414803 Color: 11
Size: 304087 Color: 16
Size: 281111 Color: 17

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 414920 Color: 6
Size: 293856 Color: 17
Size: 291225 Color: 17

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 414932 Color: 1
Size: 308805 Color: 5
Size: 276264 Color: 17

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 414951 Color: 3
Size: 311288 Color: 8
Size: 273762 Color: 8

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 414964 Color: 17
Size: 293962 Color: 10
Size: 291075 Color: 12

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 414965 Color: 19
Size: 317622 Color: 7
Size: 267414 Color: 8

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 414980 Color: 15
Size: 296007 Color: 14
Size: 289014 Color: 8

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 415018 Color: 9
Size: 300548 Color: 4
Size: 284435 Color: 16

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 415021 Color: 12
Size: 326240 Color: 13
Size: 258740 Color: 15

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 415040 Color: 17
Size: 307806 Color: 14
Size: 277155 Color: 9

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 415076 Color: 7
Size: 329163 Color: 4
Size: 255762 Color: 5

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 415089 Color: 7
Size: 296693 Color: 3
Size: 288219 Color: 9

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 415402 Color: 1
Size: 317677 Color: 9
Size: 266922 Color: 16

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 415252 Color: 14
Size: 317428 Color: 15
Size: 267321 Color: 10

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 415355 Color: 3
Size: 331588 Color: 2
Size: 253058 Color: 9

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 415397 Color: 11
Size: 297878 Color: 1
Size: 286726 Color: 16

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 415449 Color: 8
Size: 306119 Color: 3
Size: 278433 Color: 15

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 415543 Color: 14
Size: 325970 Color: 4
Size: 258488 Color: 3

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 415585 Color: 17
Size: 298648 Color: 13
Size: 285768 Color: 10

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 370212 Color: 5
Size: 334960 Color: 6
Size: 294829 Color: 11

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 415614 Color: 9
Size: 327815 Color: 16
Size: 256572 Color: 18

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 415616 Color: 18
Size: 325958 Color: 0
Size: 258427 Color: 1

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 415680 Color: 11
Size: 333647 Color: 2
Size: 250674 Color: 2

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 415713 Color: 17
Size: 333961 Color: 11
Size: 250327 Color: 9

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 415717 Color: 18
Size: 312684 Color: 10
Size: 271600 Color: 18

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 415820 Color: 18
Size: 299182 Color: 10
Size: 284999 Color: 5

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 415879 Color: 9
Size: 308285 Color: 19
Size: 275837 Color: 12

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 415968 Color: 16
Size: 321419 Color: 12
Size: 262614 Color: 1

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 416024 Color: 3
Size: 332464 Color: 14
Size: 251513 Color: 2

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 416089 Color: 2
Size: 294441 Color: 0
Size: 289471 Color: 10

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 416264 Color: 13
Size: 294100 Color: 18
Size: 289637 Color: 3

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 416281 Color: 17
Size: 324814 Color: 6
Size: 258906 Color: 16

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 416381 Color: 11
Size: 315155 Color: 10
Size: 268465 Color: 12

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 416469 Color: 13
Size: 311260 Color: 5
Size: 272272 Color: 14

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 416490 Color: 16
Size: 316000 Color: 14
Size: 267511 Color: 1

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 416533 Color: 4
Size: 312371 Color: 9
Size: 271097 Color: 14

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 416584 Color: 6
Size: 302705 Color: 8
Size: 280712 Color: 13

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 416679 Color: 11
Size: 320427 Color: 10
Size: 262895 Color: 4

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 416714 Color: 5
Size: 295280 Color: 10
Size: 288007 Color: 8

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 416723 Color: 12
Size: 329145 Color: 2
Size: 254133 Color: 0

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 416753 Color: 4
Size: 322019 Color: 10
Size: 261229 Color: 7

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 416804 Color: 19
Size: 317583 Color: 2
Size: 265614 Color: 0

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 416976 Color: 1
Size: 303649 Color: 11
Size: 279376 Color: 14

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 416820 Color: 3
Size: 309322 Color: 12
Size: 273859 Color: 6

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 416856 Color: 11
Size: 328634 Color: 17
Size: 254511 Color: 5

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 416877 Color: 8
Size: 296553 Color: 12
Size: 286571 Color: 4

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 416886 Color: 9
Size: 328566 Color: 14
Size: 254549 Color: 7

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 416974 Color: 7
Size: 317964 Color: 13
Size: 265063 Color: 18

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 417006 Color: 12
Size: 318094 Color: 17
Size: 264901 Color: 16

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 417221 Color: 1
Size: 301769 Color: 2
Size: 281011 Color: 18

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 417054 Color: 2
Size: 300387 Color: 6
Size: 282560 Color: 11

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 417102 Color: 7
Size: 298248 Color: 2
Size: 284651 Color: 8

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 417176 Color: 9
Size: 303144 Color: 3
Size: 279681 Color: 6

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 417210 Color: 6
Size: 319304 Color: 13
Size: 263487 Color: 2

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 417356 Color: 1
Size: 316373 Color: 19
Size: 266272 Color: 13

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 417278 Color: 17
Size: 312643 Color: 12
Size: 270080 Color: 8

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 417284 Color: 11
Size: 294615 Color: 9
Size: 288102 Color: 10

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 417373 Color: 17
Size: 297109 Color: 0
Size: 285519 Color: 2

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 417438 Color: 14
Size: 296686 Color: 16
Size: 285877 Color: 10

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 417470 Color: 12
Size: 313738 Color: 1
Size: 268793 Color: 17

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 417492 Color: 14
Size: 295334 Color: 3
Size: 287175 Color: 16

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 417521 Color: 12
Size: 309081 Color: 2
Size: 273399 Color: 4

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 417569 Color: 6
Size: 332128 Color: 6
Size: 250304 Color: 8

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 417643 Color: 18
Size: 314273 Color: 8
Size: 268085 Color: 3

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 417666 Color: 6
Size: 294538 Color: 9
Size: 287797 Color: 17

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 417733 Color: 1
Size: 293621 Color: 17
Size: 288647 Color: 11

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 417685 Color: 8
Size: 313230 Color: 0
Size: 269086 Color: 18

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 417686 Color: 7
Size: 309301 Color: 15
Size: 273014 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 417691 Color: 6
Size: 321663 Color: 16
Size: 260647 Color: 8

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 417747 Color: 18
Size: 296228 Color: 10
Size: 286026 Color: 12

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 417791 Color: 16
Size: 305095 Color: 15
Size: 277115 Color: 13

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 417802 Color: 8
Size: 330874 Color: 14
Size: 251325 Color: 16

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 417886 Color: 1
Size: 299874 Color: 5
Size: 282241 Color: 6

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 417838 Color: 7
Size: 317633 Color: 0
Size: 264530 Color: 18

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 417868 Color: 10
Size: 311527 Color: 6
Size: 270606 Color: 14

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 417893 Color: 17
Size: 326996 Color: 18
Size: 255112 Color: 12

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 417956 Color: 10
Size: 330033 Color: 19
Size: 252012 Color: 2

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 417996 Color: 18
Size: 305660 Color: 7
Size: 276345 Color: 16

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 418088 Color: 5
Size: 325794 Color: 12
Size: 256119 Color: 6

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 418142 Color: 8
Size: 316620 Color: 13
Size: 265239 Color: 2

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 418262 Color: 4
Size: 325988 Color: 8
Size: 255751 Color: 4

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 418299 Color: 2
Size: 315454 Color: 5
Size: 266248 Color: 12

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 418350 Color: 4
Size: 305537 Color: 7
Size: 276114 Color: 10

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 418406 Color: 9
Size: 306331 Color: 3
Size: 275264 Color: 17

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 418608 Color: 14
Size: 293898 Color: 4
Size: 287495 Color: 5

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 418615 Color: 7
Size: 307405 Color: 6
Size: 273981 Color: 2

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 418791 Color: 1
Size: 325382 Color: 5
Size: 255828 Color: 3

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 418728 Color: 12
Size: 309092 Color: 18
Size: 272181 Color: 3

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 418774 Color: 7
Size: 291245 Color: 5
Size: 289982 Color: 3

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 418802 Color: 8
Size: 330175 Color: 1
Size: 251024 Color: 3

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 418943 Color: 6
Size: 294613 Color: 11
Size: 286445 Color: 13

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 418950 Color: 13
Size: 294189 Color: 14
Size: 286862 Color: 5

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 418984 Color: 5
Size: 311987 Color: 3
Size: 269030 Color: 15

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 418989 Color: 5
Size: 328499 Color: 19
Size: 252513 Color: 15

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 418997 Color: 16
Size: 303407 Color: 3
Size: 277597 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 419024 Color: 2
Size: 322640 Color: 1
Size: 258337 Color: 3

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 419026 Color: 2
Size: 325431 Color: 7
Size: 255544 Color: 17

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 419143 Color: 19
Size: 323263 Color: 6
Size: 257595 Color: 8

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 419157 Color: 15
Size: 310328 Color: 19
Size: 270516 Color: 17

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 419233 Color: 13
Size: 297384 Color: 5
Size: 283384 Color: 4

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 419299 Color: 17
Size: 316290 Color: 5
Size: 264412 Color: 11

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 419341 Color: 12
Size: 295182 Color: 7
Size: 285478 Color: 3

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 419360 Color: 14
Size: 317752 Color: 5
Size: 262889 Color: 1

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 419371 Color: 0
Size: 315588 Color: 14
Size: 265042 Color: 18

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 419398 Color: 11
Size: 298373 Color: 13
Size: 282230 Color: 7

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 419416 Color: 9
Size: 307842 Color: 2
Size: 272743 Color: 10

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 419443 Color: 10
Size: 323563 Color: 15
Size: 256995 Color: 19

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 419476 Color: 19
Size: 310603 Color: 10
Size: 269922 Color: 9

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 419577 Color: 18
Size: 312617 Color: 7
Size: 267807 Color: 9

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 419663 Color: 4
Size: 320086 Color: 11
Size: 260252 Color: 11

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 419676 Color: 0
Size: 303974 Color: 18
Size: 276351 Color: 9

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 419747 Color: 8
Size: 320242 Color: 19
Size: 260012 Color: 11

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 419804 Color: 13
Size: 302370 Color: 9
Size: 277827 Color: 16

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 419827 Color: 4
Size: 312193 Color: 13
Size: 267981 Color: 1

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 419858 Color: 3
Size: 290595 Color: 18
Size: 289548 Color: 6

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 419941 Color: 18
Size: 297915 Color: 3
Size: 282145 Color: 7

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 419956 Color: 14
Size: 326017 Color: 0
Size: 254028 Color: 16

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 420028 Color: 0
Size: 306153 Color: 18
Size: 273820 Color: 6

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 420060 Color: 17
Size: 296569 Color: 11
Size: 283372 Color: 12

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 420085 Color: 12
Size: 294467 Color: 16
Size: 285449 Color: 12

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 420093 Color: 12
Size: 311317 Color: 5
Size: 268591 Color: 1

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 420122 Color: 17
Size: 327261 Color: 10
Size: 252618 Color: 11

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 420229 Color: 16
Size: 304048 Color: 9
Size: 275724 Color: 9

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 420320 Color: 2
Size: 296899 Color: 4
Size: 282782 Color: 13

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 420424 Color: 19
Size: 317770 Color: 2
Size: 261807 Color: 9

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 420431 Color: 10
Size: 291930 Color: 11
Size: 287640 Color: 19

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 420482 Color: 3
Size: 301655 Color: 8
Size: 277864 Color: 1

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 420494 Color: 10
Size: 305945 Color: 0
Size: 273562 Color: 4

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 420507 Color: 17
Size: 313208 Color: 10
Size: 266286 Color: 10

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 420516 Color: 17
Size: 327480 Color: 17
Size: 252005 Color: 5

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 420520 Color: 6
Size: 321987 Color: 18
Size: 257494 Color: 12

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 420577 Color: 15
Size: 301420 Color: 10
Size: 278004 Color: 2

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 420600 Color: 4
Size: 323438 Color: 17
Size: 255963 Color: 1

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 420618 Color: 6
Size: 320862 Color: 13
Size: 258521 Color: 0

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 420691 Color: 2
Size: 311947 Color: 12
Size: 267363 Color: 5

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 420694 Color: 8
Size: 293145 Color: 14
Size: 286162 Color: 11

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 420698 Color: 4
Size: 293134 Color: 17
Size: 286169 Color: 8

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 420799 Color: 14
Size: 294873 Color: 6
Size: 284329 Color: 10

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 420831 Color: 4
Size: 304131 Color: 1
Size: 275039 Color: 2

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 420880 Color: 3
Size: 296787 Color: 7
Size: 282334 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 420899 Color: 6
Size: 308492 Color: 12
Size: 270610 Color: 6

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 421003 Color: 17
Size: 312327 Color: 15
Size: 266671 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 421016 Color: 17
Size: 315569 Color: 15
Size: 263416 Color: 3

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 421113 Color: 9
Size: 290986 Color: 3
Size: 287902 Color: 2

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 421201 Color: 10
Size: 295158 Color: 8
Size: 283642 Color: 1

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 421213 Color: 11
Size: 316642 Color: 12
Size: 262146 Color: 15

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 421220 Color: 16
Size: 328238 Color: 13
Size: 250543 Color: 18

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 421294 Color: 11
Size: 320194 Color: 2
Size: 258513 Color: 18

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 421321 Color: 11
Size: 317278 Color: 5
Size: 261402 Color: 7

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 421401 Color: 11
Size: 304283 Color: 7
Size: 274317 Color: 14

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 421419 Color: 6
Size: 322143 Color: 19
Size: 256439 Color: 1

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 421420 Color: 17
Size: 301507 Color: 11
Size: 277074 Color: 18

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 421439 Color: 5
Size: 315554 Color: 6
Size: 263008 Color: 15

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 421456 Color: 3
Size: 298467 Color: 19
Size: 280078 Color: 17

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 421541 Color: 5
Size: 316705 Color: 5
Size: 261755 Color: 7

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 421554 Color: 12
Size: 315624 Color: 19
Size: 262823 Color: 12

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 421579 Color: 18
Size: 291913 Color: 1
Size: 286509 Color: 9

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 421653 Color: 18
Size: 324543 Color: 4
Size: 253805 Color: 10

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 421673 Color: 19
Size: 318275 Color: 6
Size: 260053 Color: 18

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 421678 Color: 5
Size: 306366 Color: 17
Size: 271957 Color: 9

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 421701 Color: 13
Size: 323437 Color: 5
Size: 254863 Color: 0

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 421720 Color: 11
Size: 292800 Color: 10
Size: 285481 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 421737 Color: 3
Size: 316428 Color: 12
Size: 261836 Color: 5

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 421761 Color: 19
Size: 298410 Color: 1
Size: 279830 Color: 12

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 421854 Color: 12
Size: 315407 Color: 17
Size: 262740 Color: 17

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 422099 Color: 9
Size: 325234 Color: 2
Size: 252668 Color: 15

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 422197 Color: 17
Size: 308278 Color: 15
Size: 269526 Color: 7

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 422264 Color: 11
Size: 290162 Color: 7
Size: 287575 Color: 9

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 422297 Color: 3
Size: 300022 Color: 18
Size: 277682 Color: 1

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 422305 Color: 11
Size: 300817 Color: 0
Size: 276879 Color: 17

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 422369 Color: 10
Size: 318314 Color: 16
Size: 259318 Color: 4

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 422384 Color: 0
Size: 307820 Color: 16
Size: 269797 Color: 14

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 422403 Color: 8
Size: 301664 Color: 8
Size: 275934 Color: 18

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 422421 Color: 2
Size: 310818 Color: 11
Size: 266762 Color: 12

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 422503 Color: 5
Size: 308228 Color: 15
Size: 269270 Color: 13

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 422704 Color: 1
Size: 327208 Color: 18
Size: 250089 Color: 8

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 422628 Color: 6
Size: 312309 Color: 4
Size: 265064 Color: 6

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 348784 Color: 10
Size: 346228 Color: 11
Size: 304989 Color: 18

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 422737 Color: 0
Size: 325965 Color: 5
Size: 251299 Color: 19

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 422745 Color: 17
Size: 308830 Color: 6
Size: 268426 Color: 17

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 422788 Color: 6
Size: 311605 Color: 4
Size: 265608 Color: 9

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 422799 Color: 4
Size: 317166 Color: 1
Size: 260036 Color: 4

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 422971 Color: 4
Size: 306941 Color: 16
Size: 270089 Color: 0

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 423002 Color: 6
Size: 305931 Color: 2
Size: 271068 Color: 4

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 423007 Color: 7
Size: 299365 Color: 15
Size: 277629 Color: 10

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 423035 Color: 7
Size: 289249 Color: 5
Size: 287717 Color: 11

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 423038 Color: 0
Size: 309705 Color: 15
Size: 267258 Color: 11

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 423043 Color: 10
Size: 306315 Color: 18
Size: 270643 Color: 1

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 423087 Color: 7
Size: 299265 Color: 10
Size: 277649 Color: 6

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 423094 Color: 13
Size: 299162 Color: 7
Size: 277745 Color: 7

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 423172 Color: 15
Size: 315993 Color: 17
Size: 260836 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 423277 Color: 15
Size: 321851 Color: 8
Size: 254873 Color: 18

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 423313 Color: 3
Size: 301760 Color: 4
Size: 274928 Color: 2

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 423346 Color: 18
Size: 325942 Color: 16
Size: 250713 Color: 1

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 423348 Color: 8
Size: 319610 Color: 9
Size: 257043 Color: 6

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 423408 Color: 0
Size: 295794 Color: 4
Size: 280799 Color: 4

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 423414 Color: 11
Size: 322567 Color: 19
Size: 254020 Color: 10

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 423517 Color: 0
Size: 321253 Color: 3
Size: 255231 Color: 4

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 423522 Color: 19
Size: 313093 Color: 13
Size: 263386 Color: 8

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 423588 Color: 4
Size: 313124 Color: 1
Size: 263289 Color: 17

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 423629 Color: 13
Size: 321033 Color: 19
Size: 255339 Color: 3

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 423752 Color: 12
Size: 300081 Color: 8
Size: 276168 Color: 15

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 423811 Color: 6
Size: 314024 Color: 2
Size: 262166 Color: 9

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 424018 Color: 7
Size: 320974 Color: 17
Size: 255009 Color: 8

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 424084 Color: 10
Size: 324032 Color: 17
Size: 251885 Color: 2

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 424105 Color: 13
Size: 308730 Color: 9
Size: 267166 Color: 6

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 424133 Color: 15
Size: 303391 Color: 10
Size: 272477 Color: 1

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 424163 Color: 14
Size: 290263 Color: 11
Size: 285575 Color: 3

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 424194 Color: 11
Size: 296428 Color: 17
Size: 279379 Color: 17

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 424231 Color: 10
Size: 316871 Color: 11
Size: 258899 Color: 8

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 424256 Color: 2
Size: 291870 Color: 16
Size: 283875 Color: 9

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 424315 Color: 13
Size: 310446 Color: 14
Size: 265240 Color: 3

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 424326 Color: 2
Size: 304553 Color: 14
Size: 271122 Color: 6

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 424552 Color: 1
Size: 297623 Color: 13
Size: 277826 Color: 7

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 424493 Color: 11
Size: 305104 Color: 9
Size: 270404 Color: 9

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 424535 Color: 4
Size: 309429 Color: 13
Size: 266037 Color: 13

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 424571 Color: 17
Size: 321674 Color: 13
Size: 253756 Color: 7

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 424675 Color: 1
Size: 295093 Color: 18
Size: 280233 Color: 5

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 424639 Color: 18
Size: 321735 Color: 10
Size: 253627 Color: 13

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 424703 Color: 0
Size: 312016 Color: 16
Size: 263282 Color: 14

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 424718 Color: 14
Size: 297242 Color: 5
Size: 278041 Color: 9

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 424722 Color: 10
Size: 310099 Color: 12
Size: 265180 Color: 5

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 424751 Color: 14
Size: 307282 Color: 7
Size: 267968 Color: 12

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 424807 Color: 15
Size: 291534 Color: 3
Size: 283660 Color: 4

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 424832 Color: 4
Size: 323105 Color: 1
Size: 252064 Color: 4

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 424855 Color: 6
Size: 308860 Color: 8
Size: 266286 Color: 14

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 424862 Color: 5
Size: 303576 Color: 3
Size: 271563 Color: 14

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 424895 Color: 10
Size: 287736 Color: 11
Size: 287370 Color: 9

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 424973 Color: 8
Size: 309800 Color: 5
Size: 265228 Color: 19

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 425050 Color: 11
Size: 303215 Color: 9
Size: 271736 Color: 17

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 425277 Color: 6
Size: 321944 Color: 8
Size: 252780 Color: 1

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 425485 Color: 16
Size: 324349 Color: 19
Size: 250167 Color: 15

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 425489 Color: 14
Size: 302539 Color: 7
Size: 271973 Color: 11

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 425491 Color: 2
Size: 318320 Color: 5
Size: 256190 Color: 7

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 425603 Color: 13
Size: 303099 Color: 7
Size: 271299 Color: 11

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 425679 Color: 12
Size: 319662 Color: 8
Size: 254660 Color: 6

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 425784 Color: 14
Size: 314955 Color: 0
Size: 259262 Color: 1

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 425790 Color: 12
Size: 312728 Color: 18
Size: 261483 Color: 10

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 425904 Color: 4
Size: 319197 Color: 17
Size: 254900 Color: 0

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 425981 Color: 15
Size: 292701 Color: 6
Size: 281319 Color: 12

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 426051 Color: 5
Size: 323691 Color: 14
Size: 250259 Color: 3

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 426084 Color: 7
Size: 307479 Color: 19
Size: 266438 Color: 15

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 426090 Color: 5
Size: 323407 Color: 11
Size: 250504 Color: 13

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 426113 Color: 16
Size: 317274 Color: 12
Size: 256614 Color: 8

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 426236 Color: 1
Size: 293362 Color: 2
Size: 280403 Color: 13

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 426144 Color: 0
Size: 290486 Color: 0
Size: 283371 Color: 5

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 426176 Color: 6
Size: 320339 Color: 14
Size: 253486 Color: 10

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 426182 Color: 12
Size: 299807 Color: 2
Size: 274012 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 426185 Color: 12
Size: 302058 Color: 18
Size: 271758 Color: 10

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 426241 Color: 17
Size: 290639 Color: 16
Size: 283121 Color: 18

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 426370 Color: 10
Size: 300598 Color: 17
Size: 273033 Color: 12

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 426267 Color: 19
Size: 288723 Color: 8
Size: 285011 Color: 16

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 426407 Color: 12
Size: 316359 Color: 9
Size: 257235 Color: 9

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 426461 Color: 5
Size: 300631 Color: 9
Size: 272909 Color: 0

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 426519 Color: 16
Size: 312235 Color: 14
Size: 261247 Color: 0

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 426526 Color: 7
Size: 288480 Color: 13
Size: 284995 Color: 10

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 426538 Color: 8
Size: 306994 Color: 1
Size: 266469 Color: 2

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 426563 Color: 2
Size: 321110 Color: 5
Size: 252328 Color: 0

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 426658 Color: 2
Size: 315015 Color: 0
Size: 258328 Color: 12

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 426679 Color: 17
Size: 304565 Color: 4
Size: 268757 Color: 17

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 426689 Color: 5
Size: 320854 Color: 15
Size: 252458 Color: 14

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 426730 Color: 2
Size: 305283 Color: 14
Size: 267988 Color: 10

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 427020 Color: 18
Size: 295031 Color: 2
Size: 277950 Color: 18

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 427031 Color: 18
Size: 287214 Color: 5
Size: 285756 Color: 8

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 427069 Color: 2
Size: 299760 Color: 8
Size: 273172 Color: 15

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 427110 Color: 15
Size: 289758 Color: 7
Size: 283133 Color: 18

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 427150 Color: 4
Size: 289664 Color: 11
Size: 283187 Color: 18

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 427173 Color: 11
Size: 313912 Color: 15
Size: 258916 Color: 2

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 427210 Color: 10
Size: 307777 Color: 11
Size: 265014 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 427206 Color: 14
Size: 314368 Color: 11
Size: 258427 Color: 18

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 427218 Color: 11
Size: 315043 Color: 12
Size: 257740 Color: 19

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 427253 Color: 9
Size: 297300 Color: 16
Size: 275448 Color: 13

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 427276 Color: 14
Size: 302817 Color: 12
Size: 269908 Color: 16

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 427295 Color: 2
Size: 288082 Color: 13
Size: 284624 Color: 8

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 427297 Color: 4
Size: 318030 Color: 7
Size: 254674 Color: 10

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 427314 Color: 6
Size: 287102 Color: 9
Size: 285585 Color: 11

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 427366 Color: 19
Size: 286370 Color: 17
Size: 286265 Color: 1

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 427384 Color: 11
Size: 302691 Color: 2
Size: 269926 Color: 5

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 427453 Color: 14
Size: 318682 Color: 1
Size: 253866 Color: 19

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 427456 Color: 19
Size: 295189 Color: 4
Size: 277356 Color: 13

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 427478 Color: 15
Size: 287981 Color: 17
Size: 284542 Color: 10

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 427487 Color: 13
Size: 313224 Color: 1
Size: 259290 Color: 12

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 427489 Color: 15
Size: 294842 Color: 3
Size: 277670 Color: 14

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 427515 Color: 4
Size: 310904 Color: 9
Size: 261582 Color: 17

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 427562 Color: 18
Size: 318480 Color: 16
Size: 253959 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 427747 Color: 0
Size: 294912 Color: 11
Size: 277342 Color: 17

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 427749 Color: 18
Size: 307913 Color: 10
Size: 264339 Color: 4

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 427757 Color: 2
Size: 305320 Color: 19
Size: 266924 Color: 4

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 427913 Color: 16
Size: 319589 Color: 5
Size: 252499 Color: 15

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 427947 Color: 19
Size: 299005 Color: 17
Size: 273049 Color: 16

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 427981 Color: 9
Size: 301254 Color: 14
Size: 270766 Color: 8

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 427986 Color: 16
Size: 306473 Color: 9
Size: 265542 Color: 10

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 428017 Color: 15
Size: 293495 Color: 2
Size: 278489 Color: 6

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 428032 Color: 13
Size: 302393 Color: 1
Size: 269576 Color: 19

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 428045 Color: 6
Size: 311897 Color: 8
Size: 260059 Color: 14

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 428098 Color: 18
Size: 306250 Color: 4
Size: 265653 Color: 12

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 428101 Color: 15
Size: 290011 Color: 16
Size: 281889 Color: 12

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 428127 Color: 17
Size: 294156 Color: 10
Size: 277718 Color: 7

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 428155 Color: 13
Size: 290009 Color: 12
Size: 281837 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 428156 Color: 0
Size: 314191 Color: 12
Size: 257654 Color: 7

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 428174 Color: 1
Size: 316657 Color: 5
Size: 255170 Color: 3

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 428177 Color: 13
Size: 300719 Color: 2
Size: 271105 Color: 1

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 428416 Color: 1
Size: 303876 Color: 14
Size: 267709 Color: 8

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 428461 Color: 1
Size: 297678 Color: 2
Size: 273862 Color: 19

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 428304 Color: 11
Size: 291537 Color: 17
Size: 280160 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 428375 Color: 4
Size: 309047 Color: 5
Size: 262579 Color: 11

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 428436 Color: 0
Size: 312023 Color: 6
Size: 259542 Color: 5

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 428717 Color: 1
Size: 308455 Color: 18
Size: 262829 Color: 2

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 428467 Color: 15
Size: 292230 Color: 10
Size: 279304 Color: 13

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 428564 Color: 7
Size: 285765 Color: 19
Size: 285672 Color: 0

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 428581 Color: 4
Size: 303595 Color: 2
Size: 267825 Color: 8

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 428613 Color: 7
Size: 297611 Color: 2
Size: 273777 Color: 13

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 428737 Color: 15
Size: 320381 Color: 14
Size: 250883 Color: 13

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 428742 Color: 13
Size: 320252 Color: 14
Size: 251007 Color: 10

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 428761 Color: 14
Size: 318922 Color: 1
Size: 252318 Color: 10

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 428801 Color: 5
Size: 292631 Color: 14
Size: 278569 Color: 15

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 428849 Color: 9
Size: 296200 Color: 4
Size: 274952 Color: 5

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 428887 Color: 15
Size: 290037 Color: 18
Size: 281077 Color: 6

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 428908 Color: 14
Size: 287923 Color: 2
Size: 283170 Color: 9

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 428963 Color: 12
Size: 308399 Color: 0
Size: 262639 Color: 19

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 428988 Color: 4
Size: 287744 Color: 1
Size: 283269 Color: 10

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 429027 Color: 19
Size: 317520 Color: 17
Size: 253454 Color: 17

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 429116 Color: 0
Size: 287066 Color: 2
Size: 283819 Color: 17

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 429215 Color: 14
Size: 319399 Color: 8
Size: 251387 Color: 5

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 429323 Color: 18
Size: 317973 Color: 0
Size: 252705 Color: 14

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 429334 Color: 3
Size: 311440 Color: 2
Size: 259227 Color: 13

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 429361 Color: 11
Size: 310939 Color: 0
Size: 259701 Color: 9

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 429615 Color: 6
Size: 290598 Color: 3
Size: 279788 Color: 7

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 429714 Color: 7
Size: 288430 Color: 13
Size: 281857 Color: 8

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 429758 Color: 9
Size: 317470 Color: 13
Size: 252773 Color: 0

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 429948 Color: 1
Size: 296970 Color: 2
Size: 273083 Color: 2

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 429800 Color: 3
Size: 297097 Color: 13
Size: 273104 Color: 16

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 429853 Color: 7
Size: 292346 Color: 15
Size: 277802 Color: 2

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 429891 Color: 0
Size: 302632 Color: 2
Size: 267478 Color: 15

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 430006 Color: 1
Size: 300351 Color: 8
Size: 269644 Color: 10

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 429918 Color: 19
Size: 315678 Color: 4
Size: 254405 Color: 8

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 429967 Color: 10
Size: 303132 Color: 9
Size: 266902 Color: 8

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 430084 Color: 6
Size: 302549 Color: 7
Size: 267368 Color: 13

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 430097 Color: 6
Size: 309050 Color: 15
Size: 260854 Color: 10

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 430139 Color: 2
Size: 312815 Color: 19
Size: 257047 Color: 1

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 430149 Color: 18
Size: 314225 Color: 0
Size: 255627 Color: 3

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 430174 Color: 16
Size: 304113 Color: 11
Size: 265714 Color: 12

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 430394 Color: 0
Size: 294213 Color: 12
Size: 275394 Color: 6

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 430550 Color: 18
Size: 301542 Color: 1
Size: 267909 Color: 2

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 430480 Color: 12
Size: 291536 Color: 11
Size: 277985 Color: 17

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 430661 Color: 7
Size: 313067 Color: 6
Size: 256273 Color: 13

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 430671 Color: 6
Size: 302639 Color: 15
Size: 266691 Color: 2

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 430692 Color: 5
Size: 304233 Color: 12
Size: 265076 Color: 12

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 430717 Color: 4
Size: 310222 Color: 17
Size: 259062 Color: 2

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 430724 Color: 8
Size: 285976 Color: 13
Size: 283301 Color: 15

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 430903 Color: 11
Size: 316690 Color: 4
Size: 252408 Color: 10

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 430908 Color: 7
Size: 306732 Color: 1
Size: 262361 Color: 14

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 430937 Color: 11
Size: 316006 Color: 3
Size: 253058 Color: 19

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 430942 Color: 8
Size: 317954 Color: 7
Size: 251105 Color: 16

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 430947 Color: 5
Size: 314539 Color: 9
Size: 254515 Color: 6

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 430979 Color: 12
Size: 308857 Color: 0
Size: 260165 Color: 7

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 430996 Color: 16
Size: 314465 Color: 15
Size: 254540 Color: 17

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 431009 Color: 14
Size: 304913 Color: 2
Size: 264079 Color: 7

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 431017 Color: 13
Size: 307436 Color: 4
Size: 261548 Color: 3

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 431141 Color: 1
Size: 307759 Color: 17
Size: 261101 Color: 0

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 431022 Color: 5
Size: 291248 Color: 10
Size: 277731 Color: 7

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 431105 Color: 0
Size: 307626 Color: 8
Size: 261270 Color: 14

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 431112 Color: 18
Size: 316414 Color: 16
Size: 252475 Color: 12

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 431129 Color: 13
Size: 308543 Color: 0
Size: 260329 Color: 4

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 431157 Color: 8
Size: 311654 Color: 0
Size: 257190 Color: 7

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 431257 Color: 1
Size: 305769 Color: 19
Size: 262975 Color: 16

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 431201 Color: 12
Size: 299435 Color: 5
Size: 269365 Color: 8

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 431275 Color: 0
Size: 287683 Color: 2
Size: 281043 Color: 16

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 431281 Color: 13
Size: 293497 Color: 0
Size: 275223 Color: 17

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 431365 Color: 1
Size: 289960 Color: 9
Size: 278676 Color: 18

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 431414 Color: 13
Size: 301319 Color: 6
Size: 267268 Color: 10

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 431451 Color: 9
Size: 305184 Color: 10
Size: 263366 Color: 13

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 431455 Color: 6
Size: 296673 Color: 2
Size: 271873 Color: 17

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 431587 Color: 15
Size: 285253 Color: 12
Size: 283161 Color: 7

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 431700 Color: 4
Size: 294879 Color: 0
Size: 273422 Color: 1

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 431709 Color: 5
Size: 289476 Color: 8
Size: 278816 Color: 8

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 431710 Color: 18
Size: 309305 Color: 18
Size: 258986 Color: 8

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 431743 Color: 7
Size: 295581 Color: 12
Size: 272677 Color: 4

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 431747 Color: 15
Size: 295514 Color: 16
Size: 272740 Color: 6

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 431775 Color: 3
Size: 317517 Color: 9
Size: 250709 Color: 12

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 431827 Color: 15
Size: 290281 Color: 12
Size: 277893 Color: 10

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 431913 Color: 18
Size: 294008 Color: 4
Size: 274080 Color: 7

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 432025 Color: 12
Size: 292517 Color: 16
Size: 275459 Color: 19

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 432075 Color: 0
Size: 291287 Color: 15
Size: 276639 Color: 2

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 432408 Color: 1
Size: 286861 Color: 7
Size: 280732 Color: 3

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 432204 Color: 11
Size: 295918 Color: 17
Size: 271879 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 432609 Color: 8
Size: 292754 Color: 1
Size: 274638 Color: 5

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 432651 Color: 6
Size: 298570 Color: 19
Size: 268780 Color: 14

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 432674 Color: 7
Size: 297634 Color: 8
Size: 269693 Color: 2

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 432886 Color: 0
Size: 291663 Color: 19
Size: 275452 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 432836 Color: 17
Size: 301740 Color: 9
Size: 265425 Color: 2

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 432849 Color: 16
Size: 290282 Color: 2
Size: 276870 Color: 5

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 432875 Color: 10
Size: 289046 Color: 15
Size: 278080 Color: 16

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 432914 Color: 4
Size: 298617 Color: 5
Size: 268470 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 432956 Color: 13
Size: 285874 Color: 13
Size: 281171 Color: 10

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 433050 Color: 18
Size: 300974 Color: 19
Size: 265977 Color: 11

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 433053 Color: 3
Size: 301193 Color: 10
Size: 265755 Color: 18

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 433168 Color: 13
Size: 305786 Color: 11
Size: 261047 Color: 8

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 433172 Color: 8
Size: 290456 Color: 9
Size: 276373 Color: 18

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 433225 Color: 5
Size: 289497 Color: 15
Size: 277279 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 433297 Color: 2
Size: 301912 Color: 4
Size: 264792 Color: 4

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 433376 Color: 12
Size: 297174 Color: 3
Size: 269451 Color: 2

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 433399 Color: 17
Size: 316065 Color: 19
Size: 250537 Color: 13

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 433529 Color: 17
Size: 284287 Color: 1
Size: 282185 Color: 18

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 433583 Color: 19
Size: 295129 Color: 9
Size: 271289 Color: 12

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 433596 Color: 7
Size: 297851 Color: 5
Size: 268554 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 433645 Color: 13
Size: 285419 Color: 6
Size: 280937 Color: 5

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 433684 Color: 6
Size: 305680 Color: 1
Size: 260637 Color: 15

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 433712 Color: 12
Size: 309635 Color: 10
Size: 256654 Color: 16

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 433788 Color: 3
Size: 294570 Color: 7
Size: 271643 Color: 9

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 433875 Color: 18
Size: 299239 Color: 12
Size: 266887 Color: 11

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 433896 Color: 16
Size: 300269 Color: 8
Size: 265836 Color: 3

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 433900 Color: 14
Size: 303389 Color: 17
Size: 262712 Color: 9

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 434001 Color: 0
Size: 308444 Color: 13
Size: 257556 Color: 5

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 433930 Color: 14
Size: 315775 Color: 13
Size: 250296 Color: 15

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 433990 Color: 6
Size: 301362 Color: 4
Size: 264649 Color: 8

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 433993 Color: 19
Size: 314590 Color: 1
Size: 251418 Color: 1

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 434030 Color: 9
Size: 287935 Color: 19
Size: 278036 Color: 10

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 434043 Color: 12
Size: 314816 Color: 1
Size: 251142 Color: 13

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 434084 Color: 9
Size: 291901 Color: 6
Size: 274016 Color: 13

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 434137 Color: 17
Size: 300146 Color: 8
Size: 265718 Color: 4

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 434144 Color: 17
Size: 295371 Color: 3
Size: 270486 Color: 16

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 434176 Color: 4
Size: 299473 Color: 17
Size: 266352 Color: 9

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 434447 Color: 11
Size: 283401 Color: 1
Size: 282153 Color: 17

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 434461 Color: 5
Size: 282933 Color: 13
Size: 282607 Color: 6

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 434530 Color: 11
Size: 295879 Color: 13
Size: 269592 Color: 19

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 434535 Color: 16
Size: 296566 Color: 10
Size: 268900 Color: 13

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 434538 Color: 6
Size: 293117 Color: 18
Size: 272346 Color: 9

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 434684 Color: 12
Size: 314536 Color: 5
Size: 250781 Color: 18

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 434738 Color: 1
Size: 287915 Color: 9
Size: 277348 Color: 5

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 434691 Color: 5
Size: 310440 Color: 0
Size: 254870 Color: 15

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 434751 Color: 17
Size: 307016 Color: 10
Size: 258234 Color: 2

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 434755 Color: 19
Size: 299717 Color: 10
Size: 265529 Color: 10

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 434805 Color: 15
Size: 307180 Color: 17
Size: 258016 Color: 2

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 434821 Color: 11
Size: 282698 Color: 13
Size: 282482 Color: 12

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 434940 Color: 18
Size: 283466 Color: 5
Size: 281595 Color: 4

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 435040 Color: 3
Size: 305607 Color: 17
Size: 259354 Color: 17

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 435074 Color: 0
Size: 303947 Color: 18
Size: 260980 Color: 15

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 435414 Color: 14
Size: 294373 Color: 7
Size: 270214 Color: 4

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 435608 Color: 1
Size: 303167 Color: 19
Size: 261226 Color: 7

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 435559 Color: 15
Size: 289972 Color: 4
Size: 274470 Color: 19

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 435611 Color: 5
Size: 286861 Color: 17
Size: 277529 Color: 1

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 435774 Color: 11
Size: 300692 Color: 5
Size: 263535 Color: 11

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 435831 Color: 0
Size: 286357 Color: 3
Size: 277813 Color: 19

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 435847 Color: 19
Size: 288813 Color: 2
Size: 275341 Color: 7

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 435861 Color: 2
Size: 299230 Color: 7
Size: 264910 Color: 15

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 435898 Color: 16
Size: 301614 Color: 14
Size: 262489 Color: 12

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 435923 Color: 14
Size: 288372 Color: 4
Size: 275706 Color: 3

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 435993 Color: 1
Size: 283493 Color: 12
Size: 280515 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 435950 Color: 15
Size: 293624 Color: 16
Size: 270427 Color: 8

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 436010 Color: 4
Size: 313709 Color: 17
Size: 250282 Color: 6

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 436022 Color: 13
Size: 286832 Color: 16
Size: 277147 Color: 12

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 436063 Color: 3
Size: 289306 Color: 18
Size: 274632 Color: 17

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 476364 Color: 16
Size: 269505 Color: 9
Size: 254132 Color: 15

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 436175 Color: 7
Size: 311551 Color: 8
Size: 252275 Color: 1

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 436213 Color: 7
Size: 305501 Color: 10
Size: 258287 Color: 9

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 436221 Color: 6
Size: 285792 Color: 2
Size: 277988 Color: 3

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 436249 Color: 5
Size: 295861 Color: 17
Size: 267891 Color: 10

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 436460 Color: 3
Size: 310576 Color: 12
Size: 252965 Color: 1

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 436466 Color: 0
Size: 309970 Color: 9
Size: 253565 Color: 9

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 436513 Color: 10
Size: 308555 Color: 14
Size: 254933 Color: 10

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 436614 Color: 5
Size: 303044 Color: 0
Size: 260343 Color: 0

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 436628 Color: 11
Size: 308027 Color: 15
Size: 255346 Color: 19

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 436665 Color: 8
Size: 300694 Color: 0
Size: 262642 Color: 1

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 436739 Color: 4
Size: 288430 Color: 0
Size: 274832 Color: 13

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 436856 Color: 3
Size: 297795 Color: 18
Size: 265350 Color: 14

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 436859 Color: 17
Size: 288824 Color: 5
Size: 274318 Color: 5

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 436974 Color: 12
Size: 288786 Color: 7
Size: 274241 Color: 18

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 436979 Color: 6
Size: 287839 Color: 10
Size: 275183 Color: 5

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 437353 Color: 2
Size: 306750 Color: 19
Size: 255898 Color: 14

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 437377 Color: 3
Size: 285718 Color: 16
Size: 276906 Color: 15

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 437411 Color: 11
Size: 305643 Color: 8
Size: 256947 Color: 7

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 437412 Color: 10
Size: 291909 Color: 6
Size: 270680 Color: 3

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 437488 Color: 7
Size: 284019 Color: 17
Size: 278494 Color: 5

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 437574 Color: 15
Size: 305192 Color: 1
Size: 257235 Color: 12

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 437585 Color: 15
Size: 304616 Color: 0
Size: 257800 Color: 13

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 437601 Color: 7
Size: 285794 Color: 10
Size: 276606 Color: 12

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 437632 Color: 12
Size: 290812 Color: 2
Size: 271557 Color: 2

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 437808 Color: 14
Size: 282057 Color: 16
Size: 280136 Color: 14

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 437814 Color: 3
Size: 299775 Color: 11
Size: 262412 Color: 8

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 437880 Color: 6
Size: 299582 Color: 1
Size: 262539 Color: 14

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 437890 Color: 15
Size: 298112 Color: 0
Size: 263999 Color: 5

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 437906 Color: 12
Size: 289304 Color: 18
Size: 272791 Color: 14

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 437910 Color: 16
Size: 312025 Color: 9
Size: 250066 Color: 17

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 437935 Color: 4
Size: 281373 Color: 2
Size: 280693 Color: 18

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 437946 Color: 4
Size: 289191 Color: 17
Size: 272864 Color: 2

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 437983 Color: 12
Size: 299806 Color: 1
Size: 262212 Color: 19

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 438020 Color: 8
Size: 291585 Color: 14
Size: 270396 Color: 0

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 376623 Color: 4
Size: 361979 Color: 3
Size: 261399 Color: 14

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 438031 Color: 16
Size: 302372 Color: 12
Size: 259598 Color: 9

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 438087 Color: 6
Size: 284481 Color: 19
Size: 277433 Color: 2

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 438203 Color: 13
Size: 298760 Color: 11
Size: 263038 Color: 16

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 438238 Color: 7
Size: 298314 Color: 12
Size: 263449 Color: 9

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 438316 Color: 16
Size: 299967 Color: 19
Size: 261718 Color: 7

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 438363 Color: 9
Size: 285544 Color: 0
Size: 276094 Color: 18

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 438376 Color: 5
Size: 305603 Color: 15
Size: 256022 Color: 11

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 438420 Color: 6
Size: 300270 Color: 19
Size: 261311 Color: 6

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 438478 Color: 3
Size: 307955 Color: 4
Size: 253568 Color: 1

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 438543 Color: 10
Size: 289693 Color: 3
Size: 271765 Color: 5

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 438672 Color: 18
Size: 290733 Color: 9
Size: 270596 Color: 3

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 438701 Color: 18
Size: 303627 Color: 0
Size: 257673 Color: 4

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 438707 Color: 12
Size: 300593 Color: 3
Size: 260701 Color: 8

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 438710 Color: 3
Size: 289845 Color: 8
Size: 271446 Color: 5

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 438736 Color: 13
Size: 304346 Color: 1
Size: 256919 Color: 9

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 438738 Color: 18
Size: 308652 Color: 7
Size: 252611 Color: 15

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 438766 Color: 5
Size: 287415 Color: 11
Size: 273820 Color: 12

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 438826 Color: 16
Size: 281600 Color: 1
Size: 279575 Color: 8

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 438841 Color: 16
Size: 301414 Color: 12
Size: 259746 Color: 19

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 438865 Color: 12
Size: 310396 Color: 3
Size: 250740 Color: 18

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 438935 Color: 9
Size: 299023 Color: 3
Size: 262043 Color: 16

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 438936 Color: 7
Size: 301211 Color: 19
Size: 259854 Color: 17

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 438959 Color: 4
Size: 286967 Color: 5
Size: 274075 Color: 3

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 439133 Color: 8
Size: 310267 Color: 1
Size: 250601 Color: 19

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 439138 Color: 6
Size: 287564 Color: 5
Size: 273299 Color: 13

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 439148 Color: 8
Size: 298746 Color: 17
Size: 262107 Color: 14

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 439153 Color: 8
Size: 298679 Color: 11
Size: 262169 Color: 4

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 439200 Color: 10
Size: 308393 Color: 0
Size: 252408 Color: 4

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 439210 Color: 18
Size: 304089 Color: 13
Size: 256702 Color: 2

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 439219 Color: 0
Size: 281356 Color: 1
Size: 279426 Color: 19

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 439211 Color: 7
Size: 305441 Color: 0
Size: 255349 Color: 8

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 439229 Color: 7
Size: 309796 Color: 19
Size: 250976 Color: 3

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 439257 Color: 7
Size: 286445 Color: 4
Size: 274299 Color: 12

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 439286 Color: 4
Size: 285593 Color: 13
Size: 275122 Color: 7

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 439338 Color: 2
Size: 288940 Color: 5
Size: 271723 Color: 10

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 439350 Color: 2
Size: 306561 Color: 11
Size: 254090 Color: 13

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 439451 Color: 6
Size: 307289 Color: 8
Size: 253261 Color: 14

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 439538 Color: 1
Size: 305405 Color: 16
Size: 255058 Color: 12

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 439495 Color: 15
Size: 300082 Color: 5
Size: 260424 Color: 15

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 439571 Color: 2
Size: 304379 Color: 6
Size: 256051 Color: 5

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 439581 Color: 9
Size: 301497 Color: 4
Size: 258923 Color: 11

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 439592 Color: 2
Size: 292173 Color: 1
Size: 268236 Color: 10

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 439682 Color: 12
Size: 304511 Color: 8
Size: 255808 Color: 18

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 439830 Color: 2
Size: 283204 Color: 14
Size: 276967 Color: 17

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 439832 Color: 15
Size: 294022 Color: 18
Size: 266147 Color: 7

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 439909 Color: 12
Size: 280438 Color: 18
Size: 279654 Color: 17

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 439912 Color: 18
Size: 292520 Color: 4
Size: 267569 Color: 14

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 439920 Color: 4
Size: 292463 Color: 1
Size: 267618 Color: 18

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 439988 Color: 13
Size: 281114 Color: 7
Size: 278899 Color: 4

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 440037 Color: 12
Size: 294331 Color: 15
Size: 265633 Color: 17

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 440043 Color: 5
Size: 289102 Color: 14
Size: 270856 Color: 0

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 440096 Color: 18
Size: 304225 Color: 11
Size: 255680 Color: 6

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 440235 Color: 12
Size: 293911 Color: 2
Size: 265855 Color: 14

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 440308 Color: 13
Size: 281643 Color: 19
Size: 278050 Color: 11

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 440367 Color: 17
Size: 293465 Color: 4
Size: 266169 Color: 14

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 440554 Color: 1
Size: 282558 Color: 3
Size: 276889 Color: 7

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 440447 Color: 10
Size: 308510 Color: 9
Size: 251044 Color: 15

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 440477 Color: 17
Size: 282293 Color: 11
Size: 277231 Color: 3

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 440550 Color: 15
Size: 288232 Color: 8
Size: 271219 Color: 18

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 440623 Color: 11
Size: 304226 Color: 1
Size: 255152 Color: 15

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 440664 Color: 12
Size: 304106 Color: 7
Size: 255231 Color: 19

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 440678 Color: 17
Size: 285580 Color: 18
Size: 273743 Color: 2

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 440691 Color: 10
Size: 301885 Color: 17
Size: 257425 Color: 9

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 440708 Color: 13
Size: 282247 Color: 14
Size: 277046 Color: 11

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 440725 Color: 10
Size: 293493 Color: 4
Size: 265783 Color: 11

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 440754 Color: 8
Size: 304029 Color: 19
Size: 255218 Color: 3

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 440823 Color: 1
Size: 283288 Color: 6
Size: 275890 Color: 15

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 440791 Color: 9
Size: 295434 Color: 0
Size: 263776 Color: 10

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 440801 Color: 15
Size: 280537 Color: 6
Size: 278663 Color: 10

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 440826 Color: 14
Size: 295965 Color: 17
Size: 263210 Color: 5

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 440832 Color: 5
Size: 289277 Color: 11
Size: 269892 Color: 18

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 440969 Color: 11
Size: 301707 Color: 4
Size: 257325 Color: 6

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 441003 Color: 3
Size: 302098 Color: 15
Size: 256900 Color: 9

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 441037 Color: 5
Size: 298075 Color: 8
Size: 260889 Color: 16

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 441194 Color: 1
Size: 294840 Color: 5
Size: 263967 Color: 2

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 441223 Color: 13
Size: 297663 Color: 16
Size: 261115 Color: 19

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 441234 Color: 17
Size: 296191 Color: 11
Size: 262576 Color: 10

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 441270 Color: 3
Size: 300542 Color: 7
Size: 258189 Color: 11

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 441319 Color: 5
Size: 307746 Color: 17
Size: 250936 Color: 5

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 441325 Color: 8
Size: 306028 Color: 15
Size: 252648 Color: 16

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 441420 Color: 0
Size: 293341 Color: 6
Size: 265240 Color: 9

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 441508 Color: 19
Size: 284734 Color: 9
Size: 273759 Color: 16

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 441523 Color: 10
Size: 289657 Color: 5
Size: 268821 Color: 5

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 441542 Color: 9
Size: 306680 Color: 17
Size: 251779 Color: 13

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 441720 Color: 3
Size: 303965 Color: 5
Size: 254316 Color: 19

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 441745 Color: 18
Size: 296472 Color: 8
Size: 261784 Color: 16

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 441770 Color: 5
Size: 298834 Color: 6
Size: 259397 Color: 16

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 441964 Color: 5
Size: 302593 Color: 0
Size: 255444 Color: 13

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 441778 Color: 4
Size: 293163 Color: 19
Size: 265060 Color: 1

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 441866 Color: 4
Size: 295051 Color: 2
Size: 263084 Color: 3

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 442014 Color: 1
Size: 287981 Color: 3
Size: 270006 Color: 19

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 441980 Color: 8
Size: 281924 Color: 8
Size: 276097 Color: 10

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 442021 Color: 4
Size: 291953 Color: 7
Size: 266027 Color: 10

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 442131 Color: 16
Size: 306521 Color: 7
Size: 251349 Color: 4

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 442144 Color: 2
Size: 302443 Color: 14
Size: 255414 Color: 17

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 442158 Color: 12
Size: 288377 Color: 1
Size: 269466 Color: 18

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 442164 Color: 12
Size: 290620 Color: 9
Size: 267217 Color: 10

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 442353 Color: 6
Size: 300038 Color: 12
Size: 257610 Color: 12

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 442378 Color: 15
Size: 305007 Color: 2
Size: 252616 Color: 18

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 442450 Color: 19
Size: 286835 Color: 5
Size: 270716 Color: 6

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 442494 Color: 10
Size: 291991 Color: 4
Size: 265516 Color: 7

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 442515 Color: 15
Size: 296320 Color: 5
Size: 261166 Color: 1

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 442593 Color: 9
Size: 302197 Color: 2
Size: 255211 Color: 19

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 442654 Color: 10
Size: 288735 Color: 11
Size: 268612 Color: 2

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 442698 Color: 0
Size: 284912 Color: 16
Size: 272391 Color: 10

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 442704 Color: 8
Size: 299979 Color: 3
Size: 257318 Color: 12

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 442751 Color: 4
Size: 305483 Color: 19
Size: 251767 Color: 8

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 442794 Color: 5
Size: 302715 Color: 15
Size: 254492 Color: 6

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 442839 Color: 14
Size: 291476 Color: 15
Size: 265686 Color: 9

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 442845 Color: 18
Size: 287950 Color: 19
Size: 269206 Color: 15

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 442852 Color: 8
Size: 306874 Color: 4
Size: 250275 Color: 2

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 443074 Color: 5
Size: 285397 Color: 19
Size: 271530 Color: 8

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 442997 Color: 7
Size: 295029 Color: 2
Size: 261975 Color: 19

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 443093 Color: 15
Size: 291835 Color: 2
Size: 265073 Color: 5

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 443134 Color: 12
Size: 296853 Color: 18
Size: 260014 Color: 16

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 443152 Color: 4
Size: 278826 Color: 16
Size: 278023 Color: 11

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 443172 Color: 15
Size: 288389 Color: 13
Size: 268440 Color: 17

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 443259 Color: 12
Size: 281310 Color: 8
Size: 275432 Color: 16

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 443366 Color: 12
Size: 299550 Color: 9
Size: 257085 Color: 17

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 443368 Color: 8
Size: 285975 Color: 5
Size: 270658 Color: 7

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 443536 Color: 1
Size: 294273 Color: 3
Size: 262192 Color: 13

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 443675 Color: 0
Size: 304102 Color: 17
Size: 252224 Color: 4

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 443705 Color: 12
Size: 286581 Color: 4
Size: 269715 Color: 8

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 443711 Color: 18
Size: 287238 Color: 12
Size: 269052 Color: 12

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 443773 Color: 18
Size: 285406 Color: 7
Size: 270822 Color: 11

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 443966 Color: 1
Size: 292476 Color: 15
Size: 263559 Color: 17

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 444008 Color: 5
Size: 292733 Color: 16
Size: 263260 Color: 15

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 443978 Color: 14
Size: 305344 Color: 4
Size: 250679 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 444177 Color: 12
Size: 303632 Color: 16
Size: 252192 Color: 3

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 444226 Color: 17
Size: 279611 Color: 18
Size: 276164 Color: 6

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 444245 Color: 6
Size: 279452 Color: 19
Size: 276304 Color: 18

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 444289 Color: 0
Size: 292075 Color: 17
Size: 263637 Color: 16

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 444412 Color: 6
Size: 299781 Color: 3
Size: 255808 Color: 5

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 444428 Color: 19
Size: 301115 Color: 18
Size: 254458 Color: 6

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 444450 Color: 8
Size: 286514 Color: 3
Size: 269037 Color: 18

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 444511 Color: 15
Size: 304861 Color: 10
Size: 250629 Color: 6

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 444524 Color: 0
Size: 296265 Color: 12
Size: 259212 Color: 18

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 444528 Color: 12
Size: 278999 Color: 15
Size: 276474 Color: 19

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 444678 Color: 12
Size: 281119 Color: 6
Size: 274204 Color: 5

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 444787 Color: 2
Size: 279201 Color: 18
Size: 276013 Color: 1

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 444851 Color: 13
Size: 289176 Color: 3
Size: 265974 Color: 18

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 444891 Color: 6
Size: 291104 Color: 15
Size: 264006 Color: 10

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 444964 Color: 12
Size: 277712 Color: 10
Size: 277325 Color: 19

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 444979 Color: 11
Size: 280989 Color: 17
Size: 274033 Color: 5

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 445008 Color: 4
Size: 299585 Color: 11
Size: 255408 Color: 12

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 445021 Color: 6
Size: 277782 Color: 17
Size: 277198 Color: 3

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 445023 Color: 3
Size: 279612 Color: 1
Size: 275366 Color: 0

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 445038 Color: 16
Size: 291946 Color: 3
Size: 263017 Color: 6

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 445047 Color: 13
Size: 294818 Color: 10
Size: 260136 Color: 1

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 445090 Color: 1
Size: 297961 Color: 11
Size: 256950 Color: 12

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 445248 Color: 13
Size: 294590 Color: 18
Size: 260163 Color: 8

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 445267 Color: 10
Size: 297778 Color: 2
Size: 256956 Color: 15

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 445272 Color: 1
Size: 294452 Color: 2
Size: 260277 Color: 19

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 445284 Color: 7
Size: 300776 Color: 19
Size: 253941 Color: 19

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 445327 Color: 1
Size: 284584 Color: 19
Size: 270090 Color: 5

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 445394 Color: 11
Size: 296433 Color: 0
Size: 258174 Color: 1

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 445408 Color: 10
Size: 278679 Color: 11
Size: 275914 Color: 14

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 445491 Color: 0
Size: 292981 Color: 8
Size: 261529 Color: 18

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 445502 Color: 17
Size: 294552 Color: 8
Size: 259947 Color: 5

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 445514 Color: 15
Size: 304336 Color: 7
Size: 250151 Color: 14

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 445544 Color: 14
Size: 277913 Color: 15
Size: 276544 Color: 11

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 445552 Color: 10
Size: 295361 Color: 9
Size: 259088 Color: 1

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 445616 Color: 17
Size: 278494 Color: 3
Size: 275891 Color: 18

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 445651 Color: 7
Size: 288537 Color: 5
Size: 265813 Color: 2

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 445658 Color: 3
Size: 299288 Color: 18
Size: 255055 Color: 14

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 445697 Color: 8
Size: 277256 Color: 5
Size: 277048 Color: 18

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 445719 Color: 4
Size: 293872 Color: 9
Size: 260410 Color: 1

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 445834 Color: 17
Size: 281389 Color: 9
Size: 272778 Color: 4

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 445835 Color: 4
Size: 298920 Color: 14
Size: 255246 Color: 11

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 445839 Color: 17
Size: 285510 Color: 16
Size: 268652 Color: 6

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 445868 Color: 18
Size: 289260 Color: 6
Size: 264873 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 445887 Color: 14
Size: 279059 Color: 11
Size: 275055 Color: 17

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 446035 Color: 13
Size: 301031 Color: 16
Size: 252935 Color: 1

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 446106 Color: 13
Size: 299674 Color: 15
Size: 254221 Color: 12

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 446163 Color: 0
Size: 294465 Color: 8
Size: 259373 Color: 15

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 446174 Color: 5
Size: 281668 Color: 10
Size: 272159 Color: 10

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 446220 Color: 15
Size: 302473 Color: 8
Size: 251308 Color: 3

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 446222 Color: 13
Size: 285010 Color: 2
Size: 268769 Color: 2

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 446341 Color: 1
Size: 299760 Color: 18
Size: 253900 Color: 5

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 446335 Color: 10
Size: 287524 Color: 17
Size: 266142 Color: 10

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 446376 Color: 9
Size: 302198 Color: 14
Size: 251427 Color: 3

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 446439 Color: 18
Size: 288970 Color: 10
Size: 264592 Color: 18

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 446473 Color: 6
Size: 297396 Color: 16
Size: 256132 Color: 12

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 446569 Color: 17
Size: 285616 Color: 14
Size: 267816 Color: 7

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 446593 Color: 9
Size: 284569 Color: 6
Size: 268839 Color: 1

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 446784 Color: 0
Size: 276633 Color: 2
Size: 276584 Color: 19

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 446786 Color: 16
Size: 289289 Color: 6
Size: 263926 Color: 19

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 446873 Color: 6
Size: 298954 Color: 2
Size: 254174 Color: 3

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 447076 Color: 10
Size: 283602 Color: 12
Size: 269323 Color: 6

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 447093 Color: 19
Size: 298436 Color: 6
Size: 254472 Color: 3

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 447158 Color: 7
Size: 300374 Color: 6
Size: 252469 Color: 5

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 447197 Color: 8
Size: 284844 Color: 2
Size: 267960 Color: 19

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 447208 Color: 11
Size: 277093 Color: 10
Size: 275700 Color: 18

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 447208 Color: 13
Size: 278276 Color: 2
Size: 274517 Color: 7

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 447303 Color: 12
Size: 281100 Color: 12
Size: 271598 Color: 4

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 447533 Color: 5
Size: 279848 Color: 16
Size: 272620 Color: 15

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 447356 Color: 11
Size: 291208 Color: 16
Size: 261437 Color: 2

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 447373 Color: 6
Size: 280171 Color: 0
Size: 272457 Color: 3

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 447549 Color: 5
Size: 285958 Color: 19
Size: 266494 Color: 3

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 447476 Color: 2
Size: 277821 Color: 10
Size: 274704 Color: 11

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 447785 Color: 1
Size: 288492 Color: 8
Size: 263724 Color: 16

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 447837 Color: 1
Size: 283975 Color: 2
Size: 268189 Color: 18

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 447634 Color: 4
Size: 278498 Color: 15
Size: 273869 Color: 13

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 447686 Color: 4
Size: 296400 Color: 2
Size: 255915 Color: 11

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 447701 Color: 4
Size: 296866 Color: 10
Size: 255434 Color: 3

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 447852 Color: 1
Size: 294341 Color: 18
Size: 257808 Color: 12

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 447744 Color: 4
Size: 285321 Color: 15
Size: 266936 Color: 14

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 447840 Color: 12
Size: 291088 Color: 4
Size: 261073 Color: 7

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 447923 Color: 17
Size: 276417 Color: 13
Size: 275661 Color: 10

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 447933 Color: 17
Size: 288959 Color: 13
Size: 263109 Color: 4

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 447950 Color: 18
Size: 301329 Color: 2
Size: 250722 Color: 5

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 448189 Color: 1
Size: 283439 Color: 12
Size: 268373 Color: 14

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 448101 Color: 16
Size: 292573 Color: 13
Size: 259327 Color: 19

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 448161 Color: 9
Size: 298861 Color: 3
Size: 252979 Color: 3

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 448176 Color: 8
Size: 281864 Color: 9
Size: 269961 Color: 14

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 448213 Color: 1
Size: 283534 Color: 19
Size: 268254 Color: 7

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 448188 Color: 18
Size: 287451 Color: 9
Size: 264362 Color: 13

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 448210 Color: 13
Size: 297051 Color: 19
Size: 254740 Color: 16

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 448230 Color: 11
Size: 287521 Color: 15
Size: 264250 Color: 17

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 448421 Color: 0
Size: 277977 Color: 16
Size: 273603 Color: 2

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 448439 Color: 5
Size: 293954 Color: 11
Size: 257608 Color: 9

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 448451 Color: 14
Size: 279990 Color: 1
Size: 271560 Color: 12

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 448591 Color: 11
Size: 289899 Color: 2
Size: 261511 Color: 13

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 448790 Color: 4
Size: 290359 Color: 11
Size: 260852 Color: 14

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 448955 Color: 12
Size: 295404 Color: 16
Size: 255642 Color: 16

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 448980 Color: 2
Size: 284332 Color: 12
Size: 266689 Color: 19

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 449014 Color: 12
Size: 284672 Color: 6
Size: 266315 Color: 1

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 449038 Color: 14
Size: 295818 Color: 10
Size: 255145 Color: 9

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 449106 Color: 7
Size: 275778 Color: 11
Size: 275117 Color: 18

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 449127 Color: 9
Size: 300055 Color: 5
Size: 250819 Color: 17

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 449177 Color: 14
Size: 291169 Color: 0
Size: 259655 Color: 12

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 449179 Color: 9
Size: 296613 Color: 15
Size: 254209 Color: 6

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 449251 Color: 4
Size: 296920 Color: 0
Size: 253830 Color: 7

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 449310 Color: 16
Size: 295071 Color: 6
Size: 255620 Color: 4

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 449356 Color: 5
Size: 284937 Color: 8
Size: 265708 Color: 3

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 449376 Color: 5
Size: 284510 Color: 13
Size: 266115 Color: 6

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 449446 Color: 5
Size: 276094 Color: 18
Size: 274461 Color: 11

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 449346 Color: 18
Size: 284091 Color: 10
Size: 266564 Color: 0

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 449396 Color: 16
Size: 282739 Color: 11
Size: 267866 Color: 9

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 449492 Color: 17
Size: 280912 Color: 5
Size: 269597 Color: 2

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 449495 Color: 2
Size: 287891 Color: 10
Size: 262615 Color: 17

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 449504 Color: 16
Size: 291806 Color: 7
Size: 258691 Color: 19

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 449543 Color: 6
Size: 284786 Color: 17
Size: 265672 Color: 14

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 449601 Color: 18
Size: 278718 Color: 3
Size: 271682 Color: 2

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 449601 Color: 19
Size: 294911 Color: 18
Size: 255489 Color: 17

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 449668 Color: 4
Size: 283720 Color: 12
Size: 266613 Color: 5

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 449877 Color: 15
Size: 286616 Color: 1
Size: 263508 Color: 12

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 449901 Color: 13
Size: 282797 Color: 12
Size: 267303 Color: 17

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 449904 Color: 3
Size: 277087 Color: 19
Size: 273010 Color: 6

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 449967 Color: 2
Size: 286060 Color: 17
Size: 263974 Color: 4

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 450020 Color: 10
Size: 287608 Color: 3
Size: 262373 Color: 6

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 450062 Color: 18
Size: 285648 Color: 19
Size: 264291 Color: 10

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 450079 Color: 2
Size: 292371 Color: 15
Size: 257551 Color: 7

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 450081 Color: 13
Size: 276625 Color: 13
Size: 273295 Color: 14

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 450292 Color: 10
Size: 291816 Color: 11
Size: 257893 Color: 1

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 450398 Color: 10
Size: 294198 Color: 9
Size: 255405 Color: 12

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 450246 Color: 18
Size: 282333 Color: 19
Size: 267422 Color: 11

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 450307 Color: 8
Size: 287116 Color: 17
Size: 262578 Color: 19

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 450362 Color: 11
Size: 285140 Color: 16
Size: 264499 Color: 14

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 450433 Color: 0
Size: 289081 Color: 15
Size: 260487 Color: 3

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 450437 Color: 6
Size: 294573 Color: 2
Size: 254991 Color: 16

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 450452 Color: 4
Size: 296066 Color: 16
Size: 253483 Color: 0

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 450690 Color: 10
Size: 289992 Color: 2
Size: 259319 Color: 13

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 450498 Color: 1
Size: 279317 Color: 0
Size: 270186 Color: 7

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 450507 Color: 2
Size: 283561 Color: 3
Size: 265933 Color: 15

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 450614 Color: 9
Size: 293801 Color: 18
Size: 255586 Color: 2

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 450782 Color: 17
Size: 298538 Color: 17
Size: 250681 Color: 16

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 450724 Color: 8
Size: 296890 Color: 11
Size: 252387 Color: 5

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 450767 Color: 6
Size: 278793 Color: 0
Size: 270441 Color: 2

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 450879 Color: 3
Size: 280190 Color: 2
Size: 268932 Color: 19

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 450881 Color: 6
Size: 294303 Color: 17
Size: 254817 Color: 14

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 450902 Color: 8
Size: 298318 Color: 12
Size: 250781 Color: 11

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 450965 Color: 12
Size: 288638 Color: 5
Size: 260398 Color: 2

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 450979 Color: 6
Size: 276638 Color: 7
Size: 272384 Color: 11

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 451057 Color: 7
Size: 293506 Color: 1
Size: 255438 Color: 6

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 451066 Color: 13
Size: 276198 Color: 18
Size: 272737 Color: 15

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 451138 Color: 1
Size: 293576 Color: 10
Size: 255287 Color: 1

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 451189 Color: 19
Size: 293242 Color: 17
Size: 255570 Color: 5

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 451274 Color: 18
Size: 290484 Color: 10
Size: 258243 Color: 6

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 451307 Color: 18
Size: 295602 Color: 8
Size: 253092 Color: 7

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 451408 Color: 5
Size: 275037 Color: 19
Size: 273556 Color: 1

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 451445 Color: 12
Size: 275933 Color: 14
Size: 272623 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 451449 Color: 8
Size: 295367 Color: 14
Size: 253185 Color: 2

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 451484 Color: 1
Size: 286544 Color: 6
Size: 261973 Color: 17

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 451517 Color: 1
Size: 288511 Color: 7
Size: 259973 Color: 12

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 451477 Color: 6
Size: 282837 Color: 0
Size: 265687 Color: 7

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 451486 Color: 16
Size: 295614 Color: 5
Size: 252901 Color: 4

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 451492 Color: 12
Size: 293966 Color: 14
Size: 254543 Color: 0

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 451546 Color: 2
Size: 279019 Color: 17
Size: 269436 Color: 8

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 451597 Color: 6
Size: 277759 Color: 6
Size: 270645 Color: 11

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 451830 Color: 1
Size: 294775 Color: 18
Size: 253396 Color: 10

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 451794 Color: 16
Size: 275706 Color: 13
Size: 272501 Color: 18

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 451840 Color: 2
Size: 274176 Color: 3
Size: 273985 Color: 12

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 451910 Color: 8
Size: 274555 Color: 5
Size: 273536 Color: 9

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 451916 Color: 3
Size: 293347 Color: 14
Size: 254738 Color: 0

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 451970 Color: 5
Size: 278530 Color: 1
Size: 269501 Color: 16

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 452148 Color: 19
Size: 279560 Color: 14
Size: 268293 Color: 2

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 452152 Color: 19
Size: 291477 Color: 10
Size: 256372 Color: 6

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 452174 Color: 19
Size: 287705 Color: 9
Size: 260122 Color: 14

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 452202 Color: 11
Size: 287740 Color: 18
Size: 260059 Color: 11

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 452314 Color: 5
Size: 288812 Color: 12
Size: 258875 Color: 5

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 452321 Color: 14
Size: 285601 Color: 1
Size: 262079 Color: 15

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 452383 Color: 4
Size: 291010 Color: 9
Size: 256608 Color: 19

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 452414 Color: 13
Size: 282755 Color: 4
Size: 264832 Color: 4

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 452489 Color: 16
Size: 283413 Color: 9
Size: 264099 Color: 12

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 452506 Color: 3
Size: 282191 Color: 2
Size: 265304 Color: 13

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 452537 Color: 15
Size: 287741 Color: 4
Size: 259723 Color: 18

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 452616 Color: 1
Size: 295011 Color: 18
Size: 252374 Color: 2

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 452608 Color: 7
Size: 287173 Color: 12
Size: 260220 Color: 4

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 452703 Color: 5
Size: 291743 Color: 6
Size: 255555 Color: 2

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 452888 Color: 0
Size: 293777 Color: 11
Size: 253336 Color: 13

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 452895 Color: 19
Size: 274807 Color: 7
Size: 272299 Color: 3

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 453049 Color: 10
Size: 288799 Color: 19
Size: 258153 Color: 2

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 453058 Color: 14
Size: 290041 Color: 2
Size: 256902 Color: 1

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 453104 Color: 17
Size: 287286 Color: 15
Size: 259611 Color: 12

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 453262 Color: 6
Size: 281401 Color: 11
Size: 265338 Color: 11

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 453363 Color: 6
Size: 277447 Color: 11
Size: 269191 Color: 18

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 453431 Color: 12
Size: 280353 Color: 10
Size: 266217 Color: 8

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 453443 Color: 11
Size: 287986 Color: 0
Size: 258572 Color: 10

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 453562 Color: 8
Size: 294576 Color: 2
Size: 251863 Color: 11

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 453687 Color: 15
Size: 277494 Color: 11
Size: 268820 Color: 14

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 453763 Color: 1
Size: 279918 Color: 16
Size: 266320 Color: 12

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 453694 Color: 12
Size: 289111 Color: 17
Size: 257196 Color: 6

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 453731 Color: 0
Size: 283273 Color: 7
Size: 262997 Color: 15

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 453781 Color: 11
Size: 273294 Color: 4
Size: 272926 Color: 10

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 453919 Color: 18
Size: 288876 Color: 8
Size: 257206 Color: 13

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 453940 Color: 3
Size: 275486 Color: 14
Size: 270575 Color: 1

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 454019 Color: 10
Size: 283255 Color: 9
Size: 262727 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 454032 Color: 7
Size: 285124 Color: 9
Size: 260845 Color: 14

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 454058 Color: 10
Size: 285546 Color: 5
Size: 260397 Color: 16

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 454161 Color: 15
Size: 294016 Color: 10
Size: 251824 Color: 9

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 454217 Color: 11
Size: 276722 Color: 0
Size: 269062 Color: 18

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 454257 Color: 6
Size: 286313 Color: 1
Size: 259431 Color: 0

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 454399 Color: 7
Size: 292362 Color: 7
Size: 253240 Color: 10

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 454556 Color: 18
Size: 274287 Color: 6
Size: 271158 Color: 0

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 454606 Color: 6
Size: 284672 Color: 6
Size: 260723 Color: 7

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 454613 Color: 8
Size: 292594 Color: 6
Size: 252794 Color: 6

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 454629 Color: 12
Size: 287543 Color: 6
Size: 257829 Color: 6

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 454763 Color: 7
Size: 274615 Color: 1
Size: 270623 Color: 6

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 454769 Color: 4
Size: 283210 Color: 5
Size: 262022 Color: 17

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 454804 Color: 17
Size: 285185 Color: 19
Size: 260012 Color: 14

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 454868 Color: 4
Size: 287868 Color: 8
Size: 257265 Color: 7

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 454900 Color: 13
Size: 280384 Color: 12
Size: 264717 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 454919 Color: 9
Size: 281984 Color: 13
Size: 263098 Color: 0

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 454943 Color: 17
Size: 284083 Color: 8
Size: 260975 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 454943 Color: 17
Size: 286067 Color: 3
Size: 258991 Color: 10

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 455007 Color: 7
Size: 284801 Color: 13
Size: 260193 Color: 5

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 455133 Color: 19
Size: 277081 Color: 5
Size: 267787 Color: 10

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 455144 Color: 19
Size: 285762 Color: 16
Size: 259095 Color: 19

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 455211 Color: 14
Size: 275174 Color: 6
Size: 269616 Color: 15

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 455236 Color: 6
Size: 279131 Color: 3
Size: 265634 Color: 4

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 455253 Color: 7
Size: 286991 Color: 13
Size: 257757 Color: 6

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 455281 Color: 13
Size: 284821 Color: 4
Size: 259899 Color: 2

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 455367 Color: 11
Size: 286362 Color: 8
Size: 258272 Color: 16

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 455375 Color: 2
Size: 289436 Color: 8
Size: 255190 Color: 17

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 455602 Color: 6
Size: 282780 Color: 3
Size: 261619 Color: 19

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 455445 Color: 3
Size: 274904 Color: 18
Size: 269652 Color: 2

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 455459 Color: 9
Size: 292081 Color: 3
Size: 252461 Color: 15

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 455522 Color: 8
Size: 280649 Color: 17
Size: 263830 Color: 11

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 455569 Color: 5
Size: 294249 Color: 15
Size: 250183 Color: 9

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 455760 Color: 6
Size: 275127 Color: 18
Size: 269114 Color: 4

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 455642 Color: 12
Size: 290740 Color: 16
Size: 253619 Color: 1

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 455788 Color: 14
Size: 278374 Color: 13
Size: 265839 Color: 6

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 455796 Color: 12
Size: 278726 Color: 15
Size: 265479 Color: 10

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 455878 Color: 14
Size: 274330 Color: 5
Size: 269793 Color: 3

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 455903 Color: 16
Size: 273882 Color: 11
Size: 270216 Color: 8

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 455930 Color: 4
Size: 280563 Color: 0
Size: 263508 Color: 19

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 456086 Color: 16
Size: 279881 Color: 9
Size: 264034 Color: 11

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 456140 Color: 10
Size: 287910 Color: 6
Size: 255951 Color: 8

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 456155 Color: 19
Size: 276356 Color: 15
Size: 267490 Color: 17

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 456201 Color: 11
Size: 289845 Color: 18
Size: 253955 Color: 4

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 456207 Color: 10
Size: 284917 Color: 3
Size: 258877 Color: 14

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 456296 Color: 16
Size: 292848 Color: 18
Size: 250857 Color: 5

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 456357 Color: 18
Size: 284361 Color: 19
Size: 259283 Color: 5

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 456386 Color: 7
Size: 272927 Color: 4
Size: 270688 Color: 6

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 456443 Color: 0
Size: 289952 Color: 18
Size: 253606 Color: 16

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 456507 Color: 2
Size: 278166 Color: 11
Size: 265328 Color: 14

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 456532 Color: 2
Size: 286711 Color: 16
Size: 256758 Color: 4

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 456600 Color: 16
Size: 291361 Color: 14
Size: 252040 Color: 18

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 456674 Color: 0
Size: 283948 Color: 19
Size: 259379 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 456681 Color: 16
Size: 287160 Color: 18
Size: 256160 Color: 11

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 456901 Color: 6
Size: 274915 Color: 2
Size: 268185 Color: 0

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 456932 Color: 10
Size: 274805 Color: 14
Size: 268264 Color: 2

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 456947 Color: 4
Size: 273503 Color: 13
Size: 269551 Color: 4

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 457106 Color: 0
Size: 277688 Color: 18
Size: 265207 Color: 7

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 457160 Color: 4
Size: 277577 Color: 16
Size: 265264 Color: 1

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 457181 Color: 9
Size: 277321 Color: 18
Size: 265499 Color: 9

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 457235 Color: 13
Size: 279114 Color: 3
Size: 263652 Color: 11

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 457419 Color: 12
Size: 286105 Color: 13
Size: 256477 Color: 9

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 457570 Color: 6
Size: 284404 Color: 12
Size: 258027 Color: 14

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 457502 Color: 12
Size: 278211 Color: 5
Size: 264288 Color: 15

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 457619 Color: 11
Size: 281121 Color: 16
Size: 261261 Color: 4

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 457738 Color: 17
Size: 290445 Color: 1
Size: 251818 Color: 6

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 457896 Color: 16
Size: 282524 Color: 3
Size: 259581 Color: 3

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 457916 Color: 18
Size: 279715 Color: 16
Size: 262370 Color: 9

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 458010 Color: 19
Size: 280100 Color: 3
Size: 261891 Color: 7

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 458024 Color: 4
Size: 280279 Color: 4
Size: 261698 Color: 8

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 458067 Color: 5
Size: 288720 Color: 1
Size: 253214 Color: 4

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 458129 Color: 13
Size: 271631 Color: 14
Size: 270241 Color: 15

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 458130 Color: 11
Size: 291505 Color: 6
Size: 250366 Color: 3

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 458224 Color: 0
Size: 289221 Color: 1
Size: 252556 Color: 14

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 458312 Color: 11
Size: 288876 Color: 11
Size: 252813 Color: 7

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 458336 Color: 5
Size: 282187 Color: 11
Size: 259478 Color: 2

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 458493 Color: 17
Size: 289017 Color: 2
Size: 252491 Color: 15

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 458516 Color: 12
Size: 290759 Color: 5
Size: 250726 Color: 9

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 458588 Color: 19
Size: 290116 Color: 6
Size: 251297 Color: 12

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 458606 Color: 7
Size: 287715 Color: 15
Size: 253680 Color: 14

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 458751 Color: 8
Size: 272043 Color: 5
Size: 269207 Color: 19

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 458767 Color: 12
Size: 282866 Color: 3
Size: 258368 Color: 9

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 458796 Color: 16
Size: 289266 Color: 18
Size: 251939 Color: 8

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 459224 Color: 18
Size: 282458 Color: 4
Size: 258319 Color: 11

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 459359 Color: 0
Size: 278932 Color: 10
Size: 261710 Color: 2

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 459407 Color: 17
Size: 279159 Color: 5
Size: 261435 Color: 3

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 459485 Color: 15
Size: 283548 Color: 0
Size: 256968 Color: 19

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 459865 Color: 5
Size: 288718 Color: 4
Size: 251418 Color: 9

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 459543 Color: 9
Size: 281211 Color: 13
Size: 259247 Color: 0

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 459588 Color: 8
Size: 275327 Color: 18
Size: 265086 Color: 1

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 459602 Color: 11
Size: 270674 Color: 15
Size: 269725 Color: 16

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 459941 Color: 5
Size: 273822 Color: 12
Size: 266238 Color: 18

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 459760 Color: 15
Size: 273445 Color: 1
Size: 266796 Color: 9

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 459773 Color: 19
Size: 279770 Color: 2
Size: 260458 Color: 11

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 460004 Color: 5
Size: 279457 Color: 0
Size: 260540 Color: 8

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 459952 Color: 2
Size: 286934 Color: 1
Size: 253115 Color: 9

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 460095 Color: 6
Size: 284537 Color: 18
Size: 255369 Color: 19

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 460108 Color: 14
Size: 282749 Color: 4
Size: 257144 Color: 13

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 460110 Color: 8
Size: 281500 Color: 13
Size: 258391 Color: 10

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 460311 Color: 5
Size: 275790 Color: 12
Size: 263900 Color: 18

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 460199 Color: 15
Size: 284111 Color: 13
Size: 255691 Color: 11

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 460243 Color: 2
Size: 274353 Color: 14
Size: 265405 Color: 13

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 460402 Color: 18
Size: 278128 Color: 4
Size: 261471 Color: 7

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 460578 Color: 18
Size: 270822 Color: 5
Size: 268601 Color: 11

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 460664 Color: 9
Size: 279292 Color: 10
Size: 260045 Color: 17

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 460769 Color: 0
Size: 288434 Color: 18
Size: 250798 Color: 8

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 460932 Color: 7
Size: 271760 Color: 12
Size: 267309 Color: 9

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 460936 Color: 0
Size: 283802 Color: 8
Size: 255263 Color: 14

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 460994 Color: 17
Size: 271473 Color: 12
Size: 267534 Color: 17

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 461029 Color: 14
Size: 284823 Color: 3
Size: 254149 Color: 5

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 461051 Color: 16
Size: 283749 Color: 17
Size: 255201 Color: 2

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 461165 Color: 19
Size: 285313 Color: 2
Size: 253523 Color: 18

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 461262 Color: 13
Size: 271424 Color: 7
Size: 267315 Color: 6

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 461307 Color: 0
Size: 274684 Color: 14
Size: 264010 Color: 18

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 461402 Color: 12
Size: 283314 Color: 15
Size: 255285 Color: 6

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 461432 Color: 19
Size: 284824 Color: 4
Size: 253745 Color: 4

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 461488 Color: 11
Size: 286780 Color: 7
Size: 251733 Color: 1

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 461568 Color: 12
Size: 285782 Color: 19
Size: 252651 Color: 8

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 461590 Color: 1
Size: 279083 Color: 18
Size: 259328 Color: 8

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 461600 Color: 9
Size: 274518 Color: 14
Size: 263883 Color: 14

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 461632 Color: 11
Size: 275900 Color: 11
Size: 262469 Color: 4

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 461644 Color: 9
Size: 276512 Color: 1
Size: 261845 Color: 3

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 461750 Color: 3
Size: 276432 Color: 0
Size: 261819 Color: 7

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 461880 Color: 6
Size: 280978 Color: 8
Size: 257143 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 462245 Color: 5
Size: 283051 Color: 14
Size: 254705 Color: 7

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 462240 Color: 10
Size: 280381 Color: 2
Size: 257380 Color: 18

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 462512 Color: 5
Size: 283839 Color: 12
Size: 253650 Color: 8

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 462335 Color: 0
Size: 281393 Color: 11
Size: 256273 Color: 7

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 462454 Color: 11
Size: 278790 Color: 4
Size: 258757 Color: 10

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 462598 Color: 5
Size: 277316 Color: 8
Size: 260087 Color: 4

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 462529 Color: 17
Size: 282031 Color: 1
Size: 255441 Color: 9

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 462554 Color: 8
Size: 273358 Color: 9
Size: 264089 Color: 14

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 462599 Color: 6
Size: 279243 Color: 3
Size: 258159 Color: 17

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 462606 Color: 17
Size: 272897 Color: 1
Size: 264498 Color: 16

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 462658 Color: 5
Size: 272239 Color: 1
Size: 265104 Color: 6

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 462665 Color: 16
Size: 278050 Color: 6
Size: 259286 Color: 8

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 462733 Color: 16
Size: 281003 Color: 0
Size: 256265 Color: 8

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 462741 Color: 14
Size: 286489 Color: 13
Size: 250771 Color: 12

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 462758 Color: 4
Size: 275991 Color: 2
Size: 261252 Color: 9

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 462764 Color: 8
Size: 280419 Color: 8
Size: 256818 Color: 16

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 462767 Color: 13
Size: 287187 Color: 3
Size: 250047 Color: 4

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 462801 Color: 8
Size: 272520 Color: 17
Size: 264680 Color: 5

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 462934 Color: 10
Size: 285664 Color: 12
Size: 251403 Color: 7

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 463070 Color: 11
Size: 271387 Color: 11
Size: 265544 Color: 3

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 463079 Color: 6
Size: 273115 Color: 8
Size: 263807 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 463170 Color: 13
Size: 282309 Color: 8
Size: 254522 Color: 17

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 463183 Color: 14
Size: 272098 Color: 13
Size: 264720 Color: 18

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 463246 Color: 3
Size: 280617 Color: 18
Size: 256138 Color: 5

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 463273 Color: 17
Size: 284042 Color: 19
Size: 252686 Color: 11

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 463408 Color: 6
Size: 275897 Color: 11
Size: 260696 Color: 3

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 463409 Color: 8
Size: 280739 Color: 2
Size: 255853 Color: 14

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 463538 Color: 16
Size: 275035 Color: 6
Size: 261428 Color: 15

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 463577 Color: 9
Size: 280678 Color: 10
Size: 255746 Color: 5

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 463592 Color: 7
Size: 274209 Color: 18
Size: 262200 Color: 2

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 463604 Color: 15
Size: 274410 Color: 13
Size: 261987 Color: 1

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 463614 Color: 18
Size: 281343 Color: 13
Size: 255044 Color: 7

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 463686 Color: 7
Size: 277028 Color: 2
Size: 259287 Color: 14

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 463721 Color: 3
Size: 277803 Color: 18
Size: 258477 Color: 9

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 463733 Color: 0
Size: 282377 Color: 16
Size: 253891 Color: 5

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 463770 Color: 18
Size: 280841 Color: 9
Size: 255390 Color: 7

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 463858 Color: 2
Size: 268151 Color: 1
Size: 267992 Color: 6

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 463923 Color: 9
Size: 272459 Color: 2
Size: 263619 Color: 15

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 463958 Color: 6
Size: 277406 Color: 14
Size: 258637 Color: 14

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 463962 Color: 2
Size: 278460 Color: 15
Size: 257579 Color: 3

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 463995 Color: 15
Size: 282110 Color: 5
Size: 253896 Color: 13

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 464057 Color: 13
Size: 270525 Color: 6
Size: 265419 Color: 14

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 464058 Color: 2
Size: 273805 Color: 11
Size: 262138 Color: 16

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 464088 Color: 9
Size: 276612 Color: 11
Size: 259301 Color: 3

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 464142 Color: 19
Size: 283835 Color: 13
Size: 252024 Color: 11

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 464223 Color: 6
Size: 278268 Color: 13
Size: 257510 Color: 13

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 464341 Color: 19
Size: 279900 Color: 5
Size: 255760 Color: 14

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 464387 Color: 3
Size: 281302 Color: 4
Size: 254312 Color: 19

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 464395 Color: 3
Size: 276667 Color: 2
Size: 258939 Color: 17

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 464455 Color: 9
Size: 273337 Color: 10
Size: 262209 Color: 7

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 464471 Color: 13
Size: 276518 Color: 8
Size: 259012 Color: 12

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 464471 Color: 0
Size: 276743 Color: 15
Size: 258787 Color: 17

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 464494 Color: 10
Size: 283172 Color: 5
Size: 252335 Color: 12

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 464519 Color: 11
Size: 272344 Color: 15
Size: 263138 Color: 17

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 464533 Color: 3
Size: 280550 Color: 17
Size: 254918 Color: 13

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 464575 Color: 9
Size: 270641 Color: 19
Size: 264785 Color: 12

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 464635 Color: 8
Size: 276370 Color: 10
Size: 258996 Color: 1

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 464652 Color: 0
Size: 285239 Color: 2
Size: 250110 Color: 19

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 464653 Color: 11
Size: 282541 Color: 18
Size: 252807 Color: 6

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 464667 Color: 12
Size: 275110 Color: 6
Size: 260224 Color: 5

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 464813 Color: 10
Size: 283414 Color: 1
Size: 251774 Color: 8

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 464823 Color: 7
Size: 273950 Color: 6
Size: 261228 Color: 17

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 464964 Color: 1
Size: 271235 Color: 4
Size: 263802 Color: 9

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 465006 Color: 18
Size: 280718 Color: 7
Size: 254277 Color: 18

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 465038 Color: 18
Size: 272635 Color: 9
Size: 262328 Color: 9

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 465045 Color: 11
Size: 279223 Color: 1
Size: 255733 Color: 16

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 465096 Color: 5
Size: 273753 Color: 16
Size: 261152 Color: 17

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 465062 Color: 4
Size: 283013 Color: 13
Size: 251926 Color: 18

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 465146 Color: 7
Size: 280945 Color: 8
Size: 253910 Color: 3

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 465227 Color: 8
Size: 268358 Color: 4
Size: 266416 Color: 13

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 465231 Color: 17
Size: 275044 Color: 16
Size: 259726 Color: 14

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 465246 Color: 13
Size: 267721 Color: 11
Size: 267034 Color: 5

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 465288 Color: 19
Size: 280593 Color: 18
Size: 254120 Color: 10

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 465456 Color: 10
Size: 271163 Color: 1
Size: 263382 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 465467 Color: 14
Size: 282709 Color: 7
Size: 251825 Color: 8

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 465523 Color: 6
Size: 283694 Color: 11
Size: 250784 Color: 3

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 465577 Color: 8
Size: 274477 Color: 11
Size: 259947 Color: 11

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 465726 Color: 9
Size: 277434 Color: 17
Size: 256841 Color: 7

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 465868 Color: 1
Size: 274470 Color: 17
Size: 259663 Color: 19

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 465913 Color: 17
Size: 283650 Color: 17
Size: 250438 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 465937 Color: 18
Size: 275580 Color: 4
Size: 258484 Color: 18

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 465962 Color: 13
Size: 278939 Color: 9
Size: 255100 Color: 2

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 466095 Color: 18
Size: 281772 Color: 15
Size: 252134 Color: 15

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 466127 Color: 19
Size: 280766 Color: 4
Size: 253108 Color: 9

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 466269 Color: 9
Size: 271029 Color: 19
Size: 262703 Color: 7

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 466504 Color: 5
Size: 273750 Color: 4
Size: 259747 Color: 8

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 466581 Color: 19
Size: 275476 Color: 17
Size: 257944 Color: 4

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 466595 Color: 19
Size: 273061 Color: 19
Size: 260345 Color: 13

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 466806 Color: 2
Size: 268784 Color: 5
Size: 264411 Color: 17

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 466924 Color: 8
Size: 278084 Color: 17
Size: 254993 Color: 2

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 467029 Color: 0
Size: 272437 Color: 7
Size: 260535 Color: 18

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 467095 Color: 18
Size: 272281 Color: 7
Size: 260625 Color: 17

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 467112 Color: 2
Size: 275247 Color: 3
Size: 257642 Color: 16

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 467285 Color: 11
Size: 279802 Color: 19
Size: 252914 Color: 4

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 467346 Color: 18
Size: 280368 Color: 5
Size: 252287 Color: 1

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 467427 Color: 4
Size: 275378 Color: 15
Size: 257196 Color: 19

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 467548 Color: 4
Size: 281959 Color: 10
Size: 250494 Color: 2

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 467818 Color: 16
Size: 272947 Color: 11
Size: 259236 Color: 6

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 467825 Color: 17
Size: 270068 Color: 16
Size: 262108 Color: 7

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 467845 Color: 2
Size: 276244 Color: 8
Size: 255912 Color: 8

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 467990 Color: 10
Size: 276878 Color: 5
Size: 255133 Color: 3

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 468017 Color: 13
Size: 280037 Color: 16
Size: 251947 Color: 1

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 468039 Color: 10
Size: 271190 Color: 7
Size: 260772 Color: 6

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 468053 Color: 17
Size: 273836 Color: 8
Size: 258112 Color: 9

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 468075 Color: 15
Size: 280257 Color: 3
Size: 251669 Color: 7

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 468131 Color: 12
Size: 275009 Color: 10
Size: 256861 Color: 7

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 468175 Color: 12
Size: 268776 Color: 5
Size: 263050 Color: 2

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 468205 Color: 15
Size: 268660 Color: 0
Size: 263136 Color: 14

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 468242 Color: 0
Size: 275770 Color: 11
Size: 255989 Color: 2

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 468521 Color: 10
Size: 279495 Color: 7
Size: 251985 Color: 5

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 468631 Color: 1
Size: 279091 Color: 3
Size: 252279 Color: 9

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 468638 Color: 7
Size: 267893 Color: 19
Size: 263470 Color: 12

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 468646 Color: 6
Size: 280549 Color: 10
Size: 250806 Color: 5

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 468706 Color: 17
Size: 274830 Color: 16
Size: 256465 Color: 18

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 468774 Color: 10
Size: 269469 Color: 1
Size: 261758 Color: 9

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 468982 Color: 9
Size: 265790 Color: 4
Size: 265229 Color: 7

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 468988 Color: 2
Size: 270632 Color: 19
Size: 260381 Color: 12

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 469008 Color: 15
Size: 279369 Color: 17
Size: 251624 Color: 13

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 469150 Color: 11
Size: 278583 Color: 19
Size: 252268 Color: 17

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 469362 Color: 2
Size: 276053 Color: 0
Size: 254586 Color: 10

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 469446 Color: 1
Size: 278060 Color: 19
Size: 252495 Color: 12

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 469452 Color: 6
Size: 273349 Color: 6
Size: 257200 Color: 18

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 469461 Color: 2
Size: 271189 Color: 3
Size: 259351 Color: 17

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 469641 Color: 5
Size: 269437 Color: 13
Size: 260923 Color: 1

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 469582 Color: 11
Size: 276205 Color: 17
Size: 254214 Color: 14

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 469599 Color: 16
Size: 269060 Color: 7
Size: 261342 Color: 2

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 469604 Color: 13
Size: 272821 Color: 17
Size: 257576 Color: 1

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 469614 Color: 3
Size: 267706 Color: 18
Size: 262681 Color: 9

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 469886 Color: 17
Size: 268885 Color: 10
Size: 261230 Color: 12

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 469890 Color: 4
Size: 273861 Color: 3
Size: 256250 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 470005 Color: 15
Size: 270017 Color: 5
Size: 259979 Color: 17

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 470021 Color: 1
Size: 278943 Color: 5
Size: 251037 Color: 7

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 470035 Color: 13
Size: 270684 Color: 11
Size: 259282 Color: 11

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 470054 Color: 9
Size: 278197 Color: 13
Size: 251750 Color: 16

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 470347 Color: 19
Size: 276723 Color: 11
Size: 252931 Color: 11

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 470347 Color: 8
Size: 266139 Color: 12
Size: 263515 Color: 10

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 470385 Color: 14
Size: 276132 Color: 11
Size: 253484 Color: 15

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 470414 Color: 6
Size: 264796 Color: 15
Size: 264791 Color: 17

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 470474 Color: 16
Size: 269362 Color: 5
Size: 260165 Color: 6

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 470518 Color: 3
Size: 273935 Color: 9
Size: 255548 Color: 13

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 470616 Color: 2
Size: 267758 Color: 10
Size: 261627 Color: 0

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 470656 Color: 2
Size: 278931 Color: 9
Size: 250414 Color: 10

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 470663 Color: 19
Size: 267599 Color: 8
Size: 261739 Color: 11

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 470680 Color: 9
Size: 266959 Color: 19
Size: 262362 Color: 13

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 470682 Color: 3
Size: 268541 Color: 5
Size: 260778 Color: 3

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 470687 Color: 0
Size: 271172 Color: 15
Size: 258142 Color: 3

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 470698 Color: 3
Size: 272316 Color: 19
Size: 256987 Color: 19

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 470952 Color: 13
Size: 274725 Color: 19
Size: 254324 Color: 10

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 470982 Color: 13
Size: 268491 Color: 10
Size: 260528 Color: 5

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 470999 Color: 7
Size: 270383 Color: 11
Size: 258619 Color: 16

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 471127 Color: 18
Size: 271696 Color: 6
Size: 257178 Color: 8

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 471164 Color: 10
Size: 278346 Color: 1
Size: 250491 Color: 16

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 471188 Color: 6
Size: 266251 Color: 3
Size: 262562 Color: 2

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 471189 Color: 2
Size: 264985 Color: 16
Size: 263827 Color: 15

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 471258 Color: 11
Size: 272834 Color: 11
Size: 255909 Color: 5

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 471268 Color: 4
Size: 273910 Color: 10
Size: 254823 Color: 6

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 471269 Color: 17
Size: 267754 Color: 4
Size: 260978 Color: 7

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 471273 Color: 3
Size: 270866 Color: 0
Size: 257862 Color: 3

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 471281 Color: 2
Size: 264437 Color: 0
Size: 264283 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 471293 Color: 18
Size: 274870 Color: 16
Size: 253838 Color: 16

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 471383 Color: 16
Size: 275793 Color: 4
Size: 252825 Color: 17

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 471390 Color: 4
Size: 265832 Color: 5
Size: 262779 Color: 10

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 471426 Color: 2
Size: 273025 Color: 8
Size: 255550 Color: 17

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 471436 Color: 15
Size: 271355 Color: 10
Size: 257210 Color: 3

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 471495 Color: 1
Size: 276694 Color: 10
Size: 251812 Color: 13

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 471544 Color: 18
Size: 275372 Color: 7
Size: 253085 Color: 13

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 471661 Color: 14
Size: 264918 Color: 9
Size: 263422 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 471782 Color: 10
Size: 266255 Color: 15
Size: 261964 Color: 15

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 471902 Color: 12
Size: 272814 Color: 8
Size: 255285 Color: 14

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 472216 Color: 5
Size: 277300 Color: 16
Size: 250485 Color: 12

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 472115 Color: 18
Size: 268772 Color: 13
Size: 259114 Color: 16

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 472147 Color: 11
Size: 273312 Color: 0
Size: 254542 Color: 0

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 472206 Color: 17
Size: 271300 Color: 4
Size: 256495 Color: 2

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 472231 Color: 0
Size: 274553 Color: 17
Size: 253217 Color: 5

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 472250 Color: 17
Size: 266508 Color: 6
Size: 261243 Color: 18

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 472298 Color: 16
Size: 273411 Color: 14
Size: 254292 Color: 18

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 472345 Color: 1
Size: 266664 Color: 19
Size: 260992 Color: 11

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 472376 Color: 4
Size: 266074 Color: 19
Size: 261551 Color: 13

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 472388 Color: 2
Size: 268405 Color: 14
Size: 259208 Color: 12

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 472514 Color: 5
Size: 267467 Color: 11
Size: 260020 Color: 12

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 472544 Color: 3
Size: 267733 Color: 19
Size: 259724 Color: 16

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 472546 Color: 10
Size: 265421 Color: 14
Size: 262034 Color: 9

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 472596 Color: 9
Size: 275209 Color: 12
Size: 252196 Color: 14

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 472657 Color: 2
Size: 268844 Color: 18
Size: 258500 Color: 7

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 472665 Color: 11
Size: 264649 Color: 17
Size: 262687 Color: 7

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 472704 Color: 10
Size: 277261 Color: 3
Size: 250036 Color: 5

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 472713 Color: 19
Size: 276551 Color: 1
Size: 250737 Color: 6

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 472746 Color: 9
Size: 271701 Color: 15
Size: 255554 Color: 2

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 472904 Color: 18
Size: 264221 Color: 4
Size: 262876 Color: 2

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 472911 Color: 4
Size: 272221 Color: 15
Size: 254869 Color: 15

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 473001 Color: 6
Size: 273935 Color: 16
Size: 253065 Color: 14

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 473026 Color: 12
Size: 275326 Color: 13
Size: 251649 Color: 5

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 473028 Color: 19
Size: 269616 Color: 4
Size: 257357 Color: 10

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 473205 Color: 11
Size: 276048 Color: 15
Size: 250748 Color: 14

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 473406 Color: 17
Size: 274620 Color: 16
Size: 251975 Color: 1

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 473526 Color: 13
Size: 266354 Color: 2
Size: 260121 Color: 3

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 473527 Color: 17
Size: 263815 Color: 12
Size: 262659 Color: 12

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 473596 Color: 0
Size: 269059 Color: 17
Size: 257346 Color: 9

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 473673 Color: 9
Size: 274677 Color: 11
Size: 251651 Color: 18

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 473675 Color: 8
Size: 269813 Color: 7
Size: 256513 Color: 16

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 473696 Color: 7
Size: 270968 Color: 19
Size: 255337 Color: 10

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 473803 Color: 9
Size: 266018 Color: 2
Size: 260180 Color: 11

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 473887 Color: 19
Size: 275062 Color: 18
Size: 251052 Color: 8

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 473898 Color: 1
Size: 264521 Color: 17
Size: 261582 Color: 3

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 473903 Color: 7
Size: 273984 Color: 15
Size: 252114 Color: 8

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 473996 Color: 1
Size: 271217 Color: 16
Size: 254788 Color: 11

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 474066 Color: 1
Size: 273988 Color: 12
Size: 251947 Color: 9

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 474301 Color: 5
Size: 269107 Color: 2
Size: 256593 Color: 12

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 474293 Color: 0
Size: 264530 Color: 19
Size: 261178 Color: 16

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 474342 Color: 5
Size: 262851 Color: 13
Size: 262808 Color: 3

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 474331 Color: 14
Size: 263911 Color: 16
Size: 261759 Color: 6

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 474334 Color: 1
Size: 272459 Color: 9
Size: 253208 Color: 13

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 474357 Color: 8
Size: 272873 Color: 16
Size: 252771 Color: 8

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 474626 Color: 4
Size: 272100 Color: 7
Size: 253275 Color: 13

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 474642 Color: 7
Size: 267798 Color: 6
Size: 257561 Color: 18

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 474761 Color: 0
Size: 270688 Color: 1
Size: 254552 Color: 3

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 474803 Color: 5
Size: 271093 Color: 3
Size: 254105 Color: 11

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 474928 Color: 13
Size: 273851 Color: 16
Size: 251222 Color: 1

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 474940 Color: 0
Size: 267506 Color: 0
Size: 257555 Color: 16

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 475058 Color: 16
Size: 267702 Color: 15
Size: 257241 Color: 9

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 475121 Color: 2
Size: 267593 Color: 1
Size: 257287 Color: 4

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 475148 Color: 17
Size: 268817 Color: 15
Size: 256036 Color: 12

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 475262 Color: 16
Size: 270001 Color: 5
Size: 254738 Color: 18

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 475291 Color: 12
Size: 270811 Color: 15
Size: 253899 Color: 7

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 475311 Color: 13
Size: 274444 Color: 11
Size: 250246 Color: 4

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 475435 Color: 18
Size: 262650 Color: 15
Size: 261916 Color: 10

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 475454 Color: 13
Size: 264367 Color: 17
Size: 260180 Color: 9

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 475524 Color: 12
Size: 273277 Color: 16
Size: 251200 Color: 18

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 475540 Color: 18
Size: 265943 Color: 5
Size: 258518 Color: 9

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 475604 Color: 17
Size: 262635 Color: 12
Size: 261762 Color: 7

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 475618 Color: 12
Size: 271875 Color: 12
Size: 252508 Color: 9

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 475710 Color: 7
Size: 263277 Color: 3
Size: 261014 Color: 15

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 475966 Color: 16
Size: 270874 Color: 11
Size: 253161 Color: 13

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 475985 Color: 0
Size: 272203 Color: 6
Size: 251813 Color: 10

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 476030 Color: 14
Size: 270282 Color: 10
Size: 253689 Color: 5

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 476033 Color: 8
Size: 268971 Color: 1
Size: 254997 Color: 4

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 476125 Color: 4
Size: 266858 Color: 3
Size: 257018 Color: 13

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 476220 Color: 7
Size: 264542 Color: 9
Size: 259239 Color: 1

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 476318 Color: 14
Size: 269972 Color: 18
Size: 253711 Color: 9

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 476325 Color: 7
Size: 267986 Color: 0
Size: 255690 Color: 19

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 476354 Color: 8
Size: 268953 Color: 5
Size: 254694 Color: 18

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 476395 Color: 3
Size: 266800 Color: 13
Size: 256806 Color: 18

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 476432 Color: 4
Size: 266835 Color: 13
Size: 256734 Color: 13

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 476492 Color: 8
Size: 266317 Color: 1
Size: 257192 Color: 2

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 476684 Color: 17
Size: 265008 Color: 2
Size: 258309 Color: 15

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 476699 Color: 12
Size: 270793 Color: 11
Size: 252509 Color: 2

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 476701 Color: 1
Size: 266760 Color: 15
Size: 256540 Color: 7

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 476780 Color: 5
Size: 265300 Color: 7
Size: 257921 Color: 4

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 476709 Color: 17
Size: 266150 Color: 14
Size: 257142 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 476746 Color: 9
Size: 268930 Color: 11
Size: 254325 Color: 0

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 476790 Color: 6
Size: 263290 Color: 8
Size: 259921 Color: 19

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 476828 Color: 13
Size: 271047 Color: 16
Size: 252126 Color: 2

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 476853 Color: 6
Size: 265386 Color: 12
Size: 257762 Color: 13

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 476856 Color: 1
Size: 270052 Color: 2
Size: 253093 Color: 14

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 476935 Color: 5
Size: 272711 Color: 9
Size: 250355 Color: 4

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 476861 Color: 19
Size: 266451 Color: 4
Size: 256689 Color: 11

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 476965 Color: 7
Size: 272433 Color: 15
Size: 250603 Color: 3

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 477044 Color: 9
Size: 272320 Color: 7
Size: 250637 Color: 2

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 477071 Color: 17
Size: 262589 Color: 5
Size: 260341 Color: 8

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 477174 Color: 2
Size: 269518 Color: 3
Size: 253309 Color: 7

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 477206 Color: 8
Size: 261590 Color: 16
Size: 261205 Color: 17

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 477210 Color: 19
Size: 266266 Color: 16
Size: 256525 Color: 7

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 477214 Color: 10
Size: 268088 Color: 11
Size: 254699 Color: 19

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 477255 Color: 17
Size: 272375 Color: 4
Size: 250371 Color: 18

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 477302 Color: 7
Size: 268345 Color: 1
Size: 254354 Color: 2

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 477369 Color: 16
Size: 265734 Color: 4
Size: 256898 Color: 5

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 477394 Color: 17
Size: 264161 Color: 0
Size: 258446 Color: 1

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 477426 Color: 10
Size: 264711 Color: 0
Size: 257864 Color: 0

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 477450 Color: 16
Size: 264155 Color: 15
Size: 258396 Color: 7

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 477500 Color: 0
Size: 265391 Color: 7
Size: 257110 Color: 2

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 477553 Color: 9
Size: 267894 Color: 8
Size: 254554 Color: 18

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 477633 Color: 3
Size: 269485 Color: 19
Size: 252883 Color: 5

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 477674 Color: 0
Size: 263851 Color: 13
Size: 258476 Color: 0

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 477690 Color: 8
Size: 269911 Color: 7
Size: 252400 Color: 6

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 477846 Color: 11
Size: 266735 Color: 18
Size: 255420 Color: 14

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 477849 Color: 0
Size: 268571 Color: 11
Size: 253581 Color: 16

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 477881 Color: 4
Size: 270159 Color: 6
Size: 251961 Color: 15

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 478068 Color: 11
Size: 269459 Color: 3
Size: 252474 Color: 5

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 478108 Color: 15
Size: 271346 Color: 17
Size: 250547 Color: 16

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 478279 Color: 16
Size: 269895 Color: 7
Size: 251827 Color: 16

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 478314 Color: 1
Size: 266858 Color: 10
Size: 254829 Color: 8

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 478359 Color: 6
Size: 260866 Color: 6
Size: 260776 Color: 18

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 478469 Color: 6
Size: 267523 Color: 3
Size: 254009 Color: 17

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 478523 Color: 13
Size: 266306 Color: 14
Size: 255172 Color: 5

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 478543 Color: 6
Size: 263300 Color: 8
Size: 258158 Color: 14

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 478565 Color: 16
Size: 261233 Color: 17
Size: 260203 Color: 4

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 478570 Color: 19
Size: 267705 Color: 6
Size: 253726 Color: 2

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 478696 Color: 6
Size: 262366 Color: 4
Size: 258939 Color: 6

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 478824 Color: 14
Size: 263093 Color: 18
Size: 258084 Color: 13

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 478866 Color: 11
Size: 260718 Color: 8
Size: 260417 Color: 10

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 478933 Color: 0
Size: 269789 Color: 5
Size: 251279 Color: 4

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 478944 Color: 1
Size: 263059 Color: 13
Size: 257998 Color: 16

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 478980 Color: 12
Size: 269454 Color: 12
Size: 251567 Color: 17

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 479010 Color: 12
Size: 269445 Color: 11
Size: 251546 Color: 7

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 479075 Color: 17
Size: 268850 Color: 0
Size: 252076 Color: 12

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 479089 Color: 18
Size: 263237 Color: 8
Size: 257675 Color: 10

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 479178 Color: 5
Size: 263285 Color: 8
Size: 257538 Color: 17

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 479185 Color: 16
Size: 261526 Color: 9
Size: 259290 Color: 18

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 479204 Color: 16
Size: 266883 Color: 10
Size: 253914 Color: 15

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 479210 Color: 12
Size: 265213 Color: 13
Size: 255578 Color: 14

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 479278 Color: 2
Size: 266556 Color: 19
Size: 254167 Color: 13

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 479332 Color: 3
Size: 262094 Color: 5
Size: 258575 Color: 11

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 479345 Color: 3
Size: 263808 Color: 10
Size: 256848 Color: 16

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 479387 Color: 16
Size: 266778 Color: 10
Size: 253836 Color: 1

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 479416 Color: 10
Size: 266240 Color: 13
Size: 254345 Color: 4

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 479579 Color: 19
Size: 269040 Color: 0
Size: 251382 Color: 15

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 479592 Color: 6
Size: 266273 Color: 7
Size: 254136 Color: 3

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 479654 Color: 16
Size: 266610 Color: 13
Size: 253737 Color: 11

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 479711 Color: 18
Size: 262144 Color: 12
Size: 258146 Color: 3

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 479739 Color: 10
Size: 266918 Color: 15
Size: 253344 Color: 15

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 479873 Color: 5
Size: 266004 Color: 16
Size: 254124 Color: 4

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 479837 Color: 0
Size: 261586 Color: 18
Size: 258578 Color: 17

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 479938 Color: 10
Size: 269789 Color: 0
Size: 250274 Color: 11

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 480066 Color: 16
Size: 260481 Color: 6
Size: 259454 Color: 18

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 480095 Color: 6
Size: 268403 Color: 3
Size: 251503 Color: 3

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 480132 Color: 2
Size: 260917 Color: 11
Size: 258952 Color: 2

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 480578 Color: 19
Size: 263789 Color: 10
Size: 255634 Color: 16

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 480761 Color: 5
Size: 267631 Color: 2
Size: 251609 Color: 11

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 480751 Color: 14
Size: 260620 Color: 17
Size: 258630 Color: 17

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 480817 Color: 11
Size: 261597 Color: 13
Size: 257587 Color: 18

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 480818 Color: 3
Size: 262454 Color: 16
Size: 256729 Color: 17

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 480904 Color: 1
Size: 269089 Color: 18
Size: 250008 Color: 3

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 480978 Color: 6
Size: 261394 Color: 10
Size: 257629 Color: 2

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 481101 Color: 4
Size: 260721 Color: 10
Size: 258179 Color: 15

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 481319 Color: 18
Size: 267715 Color: 4
Size: 250967 Color: 8

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 481333 Color: 12
Size: 263583 Color: 18
Size: 255085 Color: 7

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 481345 Color: 14
Size: 267586 Color: 17
Size: 251070 Color: 10

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 481451 Color: 12
Size: 261790 Color: 3
Size: 256760 Color: 1

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 481616 Color: 5
Size: 265142 Color: 1
Size: 253243 Color: 9

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 481532 Color: 15
Size: 261023 Color: 19
Size: 257446 Color: 13

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 481585 Color: 19
Size: 265101 Color: 11
Size: 253315 Color: 7

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 481660 Color: 10
Size: 267974 Color: 15
Size: 250367 Color: 18

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 481718 Color: 5
Size: 262512 Color: 16
Size: 255771 Color: 17

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 481760 Color: 7
Size: 260249 Color: 14
Size: 257992 Color: 4

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 481844 Color: 7
Size: 267463 Color: 0
Size: 250694 Color: 18

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 482023 Color: 14
Size: 260899 Color: 11
Size: 257079 Color: 1

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 482023 Color: 11
Size: 267451 Color: 12
Size: 250527 Color: 4

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 482077 Color: 18
Size: 265775 Color: 7
Size: 252149 Color: 6

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 482084 Color: 16
Size: 267852 Color: 5
Size: 250065 Color: 4

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 482142 Color: 16
Size: 262145 Color: 6
Size: 255714 Color: 19

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 482253 Color: 16
Size: 260564 Color: 12
Size: 257184 Color: 14

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 482338 Color: 15
Size: 259523 Color: 8
Size: 258140 Color: 12

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 482388 Color: 8
Size: 265756 Color: 15
Size: 251857 Color: 19

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 482458 Color: 13
Size: 259111 Color: 2
Size: 258432 Color: 2

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 482518 Color: 5
Size: 259386 Color: 6
Size: 258097 Color: 13

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 482512 Color: 2
Size: 259577 Color: 16
Size: 257912 Color: 17

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 482527 Color: 16
Size: 263971 Color: 11
Size: 253503 Color: 4

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 482536 Color: 10
Size: 258883 Color: 16
Size: 258582 Color: 7

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 482668 Color: 2
Size: 265950 Color: 15
Size: 251383 Color: 12

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 482693 Color: 14
Size: 259447 Color: 3
Size: 257861 Color: 9

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 482825 Color: 17
Size: 263279 Color: 0
Size: 253897 Color: 6

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 482855 Color: 18
Size: 261007 Color: 3
Size: 256139 Color: 8

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 482955 Color: 5
Size: 260578 Color: 13
Size: 256468 Color: 11

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 482940 Color: 9
Size: 262187 Color: 13
Size: 254874 Color: 7

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 483015 Color: 19
Size: 265396 Color: 3
Size: 251590 Color: 8

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 483077 Color: 3
Size: 260333 Color: 12
Size: 256591 Color: 17

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 483094 Color: 19
Size: 262331 Color: 17
Size: 254576 Color: 15

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 483100 Color: 16
Size: 259897 Color: 9
Size: 257004 Color: 5

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 483293 Color: 9
Size: 260446 Color: 4
Size: 256262 Color: 19

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 483307 Color: 18
Size: 258411 Color: 4
Size: 258283 Color: 8

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 483337 Color: 19
Size: 259306 Color: 1
Size: 257358 Color: 4

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 483356 Color: 15
Size: 261092 Color: 11
Size: 255553 Color: 9

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 483545 Color: 19
Size: 260062 Color: 3
Size: 256394 Color: 16

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 483550 Color: 19
Size: 261994 Color: 11
Size: 254457 Color: 8

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 483592 Color: 4
Size: 258470 Color: 6
Size: 257939 Color: 14

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 483602 Color: 11
Size: 261267 Color: 18
Size: 255132 Color: 15

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 483650 Color: 10
Size: 259990 Color: 15
Size: 256361 Color: 1

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 483793 Color: 5
Size: 265637 Color: 8
Size: 250571 Color: 14

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 483661 Color: 15
Size: 264352 Color: 10
Size: 251988 Color: 13

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 483721 Color: 3
Size: 260254 Color: 18
Size: 256026 Color: 9

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 483745 Color: 3
Size: 261559 Color: 14
Size: 254697 Color: 14

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 483814 Color: 6
Size: 259685 Color: 0
Size: 256502 Color: 8

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 483848 Color: 3
Size: 258932 Color: 10
Size: 257221 Color: 1

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 483896 Color: 8
Size: 265513 Color: 18
Size: 250592 Color: 18

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 483960 Color: 18
Size: 263866 Color: 15
Size: 252175 Color: 0

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 484092 Color: 12
Size: 260546 Color: 4
Size: 255363 Color: 3

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 484279 Color: 10
Size: 261322 Color: 16
Size: 254400 Color: 15

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 484300 Color: 9
Size: 263133 Color: 12
Size: 252568 Color: 16

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 484671 Color: 5
Size: 259206 Color: 3
Size: 256124 Color: 8

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 484358 Color: 9
Size: 265616 Color: 7
Size: 250027 Color: 15

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 484453 Color: 7
Size: 264680 Color: 12
Size: 250868 Color: 13

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 484700 Color: 5
Size: 259700 Color: 3
Size: 255601 Color: 2

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 484709 Color: 7
Size: 259510 Color: 5
Size: 255782 Color: 14

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 484715 Color: 17
Size: 261141 Color: 12
Size: 254145 Color: 9

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 484894 Color: 4
Size: 261677 Color: 7
Size: 253430 Color: 0

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 484966 Color: 8
Size: 258000 Color: 7
Size: 257035 Color: 0

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 485022 Color: 10
Size: 260285 Color: 13
Size: 254694 Color: 12

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 485047 Color: 16
Size: 261341 Color: 13
Size: 253613 Color: 0

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 485136 Color: 4
Size: 264306 Color: 5
Size: 250559 Color: 4

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 485369 Color: 17
Size: 263385 Color: 15
Size: 251247 Color: 14

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 485404 Color: 8
Size: 257715 Color: 18
Size: 256882 Color: 9

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 485526 Color: 3
Size: 260865 Color: 1
Size: 253610 Color: 1

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 485752 Color: 16
Size: 259083 Color: 6
Size: 255166 Color: 13

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 485850 Color: 1
Size: 262471 Color: 17
Size: 251680 Color: 10

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 485856 Color: 1
Size: 257328 Color: 7
Size: 256817 Color: 6

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 485904 Color: 6
Size: 258785 Color: 14
Size: 255312 Color: 5

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 485910 Color: 18
Size: 262645 Color: 19
Size: 251446 Color: 2

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 485979 Color: 6
Size: 260684 Color: 4
Size: 253338 Color: 18

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 486091 Color: 0
Size: 262753 Color: 19
Size: 251157 Color: 1

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 486123 Color: 19
Size: 260499 Color: 13
Size: 253379 Color: 3

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 486182 Color: 14
Size: 263372 Color: 9
Size: 250447 Color: 10

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 486201 Color: 18
Size: 260331 Color: 5
Size: 253469 Color: 12

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 486257 Color: 13
Size: 261419 Color: 14
Size: 252325 Color: 8

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 486270 Color: 14
Size: 258800 Color: 19
Size: 254931 Color: 2

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 486283 Color: 3
Size: 263044 Color: 7
Size: 250674 Color: 7

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 486303 Color: 18
Size: 262544 Color: 0
Size: 251154 Color: 6

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 486347 Color: 6
Size: 261667 Color: 15
Size: 251987 Color: 17

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 486376 Color: 19
Size: 257055 Color: 5
Size: 256570 Color: 8

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 486449 Color: 14
Size: 257673 Color: 13
Size: 255879 Color: 2

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 486471 Color: 2
Size: 257512 Color: 17
Size: 256018 Color: 6

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 486485 Color: 11
Size: 261548 Color: 4
Size: 251968 Color: 8

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 486495 Color: 11
Size: 262283 Color: 7
Size: 251223 Color: 17

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 486616 Color: 13
Size: 257218 Color: 8
Size: 256167 Color: 2

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 486650 Color: 11
Size: 259599 Color: 18
Size: 253752 Color: 2

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 486657 Color: 3
Size: 258491 Color: 10
Size: 254853 Color: 0

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 486722 Color: 9
Size: 257906 Color: 10
Size: 255373 Color: 4

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 486726 Color: 4
Size: 263166 Color: 18
Size: 250109 Color: 8

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 486760 Color: 4
Size: 258038 Color: 1
Size: 255203 Color: 11

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 487036 Color: 5
Size: 261627 Color: 14
Size: 251338 Color: 14

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 486887 Color: 3
Size: 262685 Color: 14
Size: 250429 Color: 0

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 487089 Color: 8
Size: 256813 Color: 9
Size: 256099 Color: 5

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 487142 Color: 9
Size: 260105 Color: 15
Size: 252754 Color: 17

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 487229 Color: 12
Size: 260193 Color: 18
Size: 252579 Color: 3

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 487256 Color: 16
Size: 261990 Color: 4
Size: 250755 Color: 8

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 487302 Color: 18
Size: 260081 Color: 16
Size: 252618 Color: 18

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 487463 Color: 14
Size: 261509 Color: 6
Size: 251029 Color: 2

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 487542 Color: 4
Size: 257979 Color: 8
Size: 254480 Color: 17

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 487625 Color: 9
Size: 257791 Color: 15
Size: 254585 Color: 13

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 487712 Color: 19
Size: 257652 Color: 10
Size: 254637 Color: 17

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 487749 Color: 13
Size: 262244 Color: 7
Size: 250008 Color: 4

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 487755 Color: 17
Size: 258009 Color: 13
Size: 254237 Color: 6

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 487756 Color: 0
Size: 256232 Color: 3
Size: 256013 Color: 6

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 487857 Color: 15
Size: 256254 Color: 4
Size: 255890 Color: 13

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 487904 Color: 17
Size: 256701 Color: 19
Size: 255396 Color: 19

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 488020 Color: 7
Size: 261474 Color: 1
Size: 250507 Color: 6

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 488176 Color: 17
Size: 258773 Color: 6
Size: 253052 Color: 1

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 488099 Color: 16
Size: 260470 Color: 18
Size: 251432 Color: 12

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 488148 Color: 6
Size: 259966 Color: 3
Size: 251887 Color: 9

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 488266 Color: 19
Size: 258600 Color: 17
Size: 253135 Color: 2

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 488300 Color: 3
Size: 261056 Color: 8
Size: 250645 Color: 8

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 488494 Color: 5
Size: 260515 Color: 1
Size: 250992 Color: 13

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 488522 Color: 11
Size: 258416 Color: 18
Size: 253063 Color: 4

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 488540 Color: 19
Size: 259030 Color: 18
Size: 252431 Color: 0

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 488596 Color: 5
Size: 257863 Color: 11
Size: 253542 Color: 18

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 488637 Color: 16
Size: 261260 Color: 1
Size: 250104 Color: 17

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 488708 Color: 5
Size: 258126 Color: 7
Size: 253167 Color: 10

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 488653 Color: 0
Size: 261116 Color: 7
Size: 250232 Color: 12

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 488689 Color: 4
Size: 259636 Color: 11
Size: 251676 Color: 1

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 488692 Color: 14
Size: 259061 Color: 15
Size: 252248 Color: 10

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 488855 Color: 10
Size: 256016 Color: 17
Size: 255130 Color: 12

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 488947 Color: 9
Size: 260566 Color: 14
Size: 250488 Color: 5

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 488984 Color: 10
Size: 257936 Color: 4
Size: 253081 Color: 16

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 488998 Color: 14
Size: 259736 Color: 10
Size: 251267 Color: 12

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 489025 Color: 15
Size: 259383 Color: 7
Size: 251593 Color: 19

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 489045 Color: 12
Size: 259964 Color: 9
Size: 250992 Color: 15

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 489101 Color: 0
Size: 255820 Color: 15
Size: 255080 Color: 15

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 489213 Color: 11
Size: 260672 Color: 18
Size: 250116 Color: 12

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 489245 Color: 13
Size: 258555 Color: 5
Size: 252201 Color: 11

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 489248 Color: 2
Size: 257985 Color: 12
Size: 252768 Color: 16

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 489380 Color: 6
Size: 255657 Color: 10
Size: 254964 Color: 18

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 489384 Color: 17
Size: 257048 Color: 8
Size: 253569 Color: 0

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 489415 Color: 10
Size: 259658 Color: 4
Size: 250928 Color: 2

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 489445 Color: 19
Size: 259833 Color: 15
Size: 250723 Color: 6

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 489450 Color: 3
Size: 259628 Color: 6
Size: 250923 Color: 13

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 489595 Color: 7
Size: 257700 Color: 11
Size: 252706 Color: 12

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 489743 Color: 5
Size: 258439 Color: 12
Size: 251819 Color: 6

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 489708 Color: 4
Size: 257698 Color: 19
Size: 252595 Color: 14

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 489882 Color: 13
Size: 257618 Color: 1
Size: 252501 Color: 15

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 490121 Color: 16
Size: 254971 Color: 8
Size: 254909 Color: 2

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 490283 Color: 0
Size: 257753 Color: 18
Size: 251965 Color: 5

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 490290 Color: 11
Size: 254990 Color: 6
Size: 254721 Color: 2

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 490363 Color: 18
Size: 255103 Color: 0
Size: 254535 Color: 15

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 490451 Color: 19
Size: 258722 Color: 1
Size: 250828 Color: 6

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 490516 Color: 14
Size: 258863 Color: 16
Size: 250622 Color: 12

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 490638 Color: 16
Size: 257432 Color: 13
Size: 251931 Color: 6

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 490656 Color: 0
Size: 256940 Color: 1
Size: 252405 Color: 16

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 490710 Color: 9
Size: 255258 Color: 5
Size: 254033 Color: 8

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 490780 Color: 16
Size: 258070 Color: 0
Size: 251151 Color: 12

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 490794 Color: 19
Size: 256440 Color: 19
Size: 252767 Color: 17

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 490901 Color: 6
Size: 257226 Color: 14
Size: 251874 Color: 4

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 491046 Color: 16
Size: 258410 Color: 15
Size: 250545 Color: 18

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 491052 Color: 18
Size: 255522 Color: 12
Size: 253427 Color: 6

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 491184 Color: 17
Size: 254473 Color: 5
Size: 254344 Color: 19

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 491225 Color: 19
Size: 255433 Color: 13
Size: 253343 Color: 16

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 491281 Color: 19
Size: 257130 Color: 6
Size: 251590 Color: 18

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 491482 Color: 8
Size: 254672 Color: 12
Size: 253847 Color: 9

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 491873 Color: 11
Size: 255967 Color: 6
Size: 252161 Color: 7

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 491887 Color: 15
Size: 257669 Color: 2
Size: 250445 Color: 8

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 491889 Color: 15
Size: 257420 Color: 2
Size: 250692 Color: 13

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 492106 Color: 8
Size: 255229 Color: 12
Size: 252666 Color: 10

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 492123 Color: 7
Size: 256391 Color: 19
Size: 251487 Color: 4

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 492238 Color: 5
Size: 255416 Color: 6
Size: 252347 Color: 12

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 492174 Color: 4
Size: 255628 Color: 1
Size: 252199 Color: 10

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 492331 Color: 2
Size: 255766 Color: 18
Size: 251904 Color: 17

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 492344 Color: 14
Size: 254770 Color: 5
Size: 252887 Color: 7

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 492350 Color: 7
Size: 255831 Color: 4
Size: 251820 Color: 3

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 492763 Color: 1
Size: 254778 Color: 19
Size: 252460 Color: 15

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 492780 Color: 19
Size: 254193 Color: 3
Size: 253028 Color: 8

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 492835 Color: 3
Size: 255648 Color: 10
Size: 251518 Color: 9

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 492835 Color: 19
Size: 254790 Color: 4
Size: 252376 Color: 14

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 492868 Color: 1
Size: 255079 Color: 2
Size: 252054 Color: 11

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 492907 Color: 7
Size: 253904 Color: 15
Size: 253190 Color: 5

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 492976 Color: 7
Size: 256222 Color: 14
Size: 250803 Color: 14

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 492984 Color: 4
Size: 255272 Color: 17
Size: 251745 Color: 13

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 493035 Color: 9
Size: 253533 Color: 12
Size: 253433 Color: 15

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 493229 Color: 2
Size: 254760 Color: 9
Size: 252012 Color: 12

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 493247 Color: 14
Size: 254653 Color: 7
Size: 252101 Color: 7

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 493268 Color: 18
Size: 253694 Color: 17
Size: 253039 Color: 5

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 493373 Color: 1
Size: 256503 Color: 14
Size: 250125 Color: 11

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 493400 Color: 0
Size: 253705 Color: 16
Size: 252896 Color: 6

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 493505 Color: 8
Size: 255299 Color: 14
Size: 251197 Color: 19

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 493595 Color: 13
Size: 255712 Color: 18
Size: 250694 Color: 16

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 493602 Color: 14
Size: 255373 Color: 12
Size: 251026 Color: 19

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 493709 Color: 15
Size: 254724 Color: 14
Size: 251568 Color: 10

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 493832 Color: 2
Size: 254945 Color: 8
Size: 251224 Color: 14

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 493863 Color: 4
Size: 253092 Color: 14
Size: 253046 Color: 18

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 493928 Color: 8
Size: 255298 Color: 9
Size: 250775 Color: 9

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 493987 Color: 10
Size: 254723 Color: 0
Size: 251291 Color: 0

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 493993 Color: 14
Size: 254691 Color: 4
Size: 251317 Color: 18

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 494020 Color: 11
Size: 255849 Color: 5
Size: 250132 Color: 2

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 494058 Color: 8
Size: 254417 Color: 9
Size: 251526 Color: 4

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 494127 Color: 10
Size: 255764 Color: 6
Size: 250110 Color: 19

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 494149 Color: 0
Size: 255097 Color: 1
Size: 250755 Color: 0

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 494302 Color: 15
Size: 255586 Color: 13
Size: 250113 Color: 11

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 494321 Color: 12
Size: 255274 Color: 8
Size: 250406 Color: 17

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 494348 Color: 5
Size: 254150 Color: 10
Size: 251503 Color: 18

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 494363 Color: 1
Size: 255618 Color: 1
Size: 250020 Color: 9

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 494487 Color: 7
Size: 253771 Color: 19
Size: 251743 Color: 8

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 494489 Color: 2
Size: 254689 Color: 7
Size: 250823 Color: 17

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 494586 Color: 10
Size: 253470 Color: 12
Size: 251945 Color: 1

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 494588 Color: 4
Size: 254241 Color: 0
Size: 251172 Color: 18

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 494632 Color: 7
Size: 255140 Color: 7
Size: 250229 Color: 5

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 494663 Color: 15
Size: 253695 Color: 3
Size: 251643 Color: 9

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 494728 Color: 12
Size: 254956 Color: 0
Size: 250317 Color: 3

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 494775 Color: 10
Size: 252856 Color: 12
Size: 252370 Color: 6

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 494797 Color: 19
Size: 253185 Color: 15
Size: 252019 Color: 4

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 494846 Color: 2
Size: 253299 Color: 11
Size: 251856 Color: 13

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 494901 Color: 6
Size: 253159 Color: 11
Size: 251941 Color: 18

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 495183 Color: 5
Size: 253365 Color: 16
Size: 251453 Color: 19

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 495150 Color: 13
Size: 254687 Color: 12
Size: 250164 Color: 15

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 495218 Color: 14
Size: 254273 Color: 19
Size: 250510 Color: 4

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 495249 Color: 4
Size: 253317 Color: 2
Size: 251435 Color: 12

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 495278 Color: 17
Size: 254488 Color: 1
Size: 250235 Color: 10

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 495333 Color: 9
Size: 254199 Color: 10
Size: 250469 Color: 18

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 495370 Color: 12
Size: 253505 Color: 4
Size: 251126 Color: 18

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 495385 Color: 13
Size: 253936 Color: 0
Size: 250680 Color: 13

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 495391 Color: 2
Size: 252864 Color: 10
Size: 251746 Color: 14

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 495465 Color: 16
Size: 254168 Color: 4
Size: 250368 Color: 14

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 495531 Color: 15
Size: 252713 Color: 7
Size: 251757 Color: 4

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 495600 Color: 1
Size: 253049 Color: 15
Size: 251352 Color: 18

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 495656 Color: 17
Size: 252879 Color: 2
Size: 251466 Color: 0

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 495758 Color: 19
Size: 252784 Color: 10
Size: 251459 Color: 11

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 495759 Color: 2
Size: 252830 Color: 10
Size: 251412 Color: 16

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 495873 Color: 13
Size: 253832 Color: 7
Size: 250296 Color: 0

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 496027 Color: 14
Size: 253461 Color: 14
Size: 250513 Color: 11

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 496038 Color: 0
Size: 253645 Color: 5
Size: 250318 Color: 17

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 496081 Color: 15
Size: 252788 Color: 3
Size: 251132 Color: 14

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 496138 Color: 13
Size: 253441 Color: 2
Size: 250422 Color: 13

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 496197 Color: 3
Size: 253158 Color: 7
Size: 250646 Color: 3

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 496464 Color: 3
Size: 252909 Color: 1
Size: 250628 Color: 2

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 496621 Color: 19
Size: 251703 Color: 0
Size: 251677 Color: 10

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 496890 Color: 19
Size: 252214 Color: 19
Size: 250897 Color: 7

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 496912 Color: 10
Size: 252231 Color: 18
Size: 250858 Color: 5

Bin 3126: 0 of cap free
Amount of items: 3
Items: 
Size: 496933 Color: 19
Size: 252195 Color: 14
Size: 250873 Color: 15

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 497104 Color: 12
Size: 251656 Color: 6
Size: 251241 Color: 19

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 497174 Color: 11
Size: 251576 Color: 2
Size: 251251 Color: 2

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 497186 Color: 16
Size: 251639 Color: 14
Size: 251176 Color: 15

Bin 3130: 0 of cap free
Amount of items: 3
Items: 
Size: 497304 Color: 12
Size: 252347 Color: 19
Size: 250350 Color: 11

Bin 3131: 0 of cap free
Amount of items: 3
Items: 
Size: 497321 Color: 5
Size: 252051 Color: 6
Size: 250629 Color: 19

Bin 3132: 0 of cap free
Amount of items: 3
Items: 
Size: 497358 Color: 8
Size: 251401 Color: 3
Size: 251242 Color: 9

Bin 3133: 0 of cap free
Amount of items: 3
Items: 
Size: 497572 Color: 14
Size: 251956 Color: 9
Size: 250473 Color: 9

Bin 3134: 0 of cap free
Amount of items: 3
Items: 
Size: 497633 Color: 15
Size: 251371 Color: 2
Size: 250997 Color: 18

Bin 3135: 0 of cap free
Amount of items: 3
Items: 
Size: 497696 Color: 11
Size: 251319 Color: 10
Size: 250986 Color: 13

Bin 3136: 0 of cap free
Amount of items: 3
Items: 
Size: 497728 Color: 6
Size: 252061 Color: 16
Size: 250212 Color: 5

Bin 3137: 0 of cap free
Amount of items: 3
Items: 
Size: 497737 Color: 1
Size: 251465 Color: 3
Size: 250799 Color: 3

Bin 3138: 0 of cap free
Amount of items: 3
Items: 
Size: 497948 Color: 2
Size: 251691 Color: 8
Size: 250362 Color: 0

Bin 3139: 0 of cap free
Amount of items: 3
Items: 
Size: 498014 Color: 7
Size: 251499 Color: 13
Size: 250488 Color: 11

Bin 3140: 0 of cap free
Amount of items: 3
Items: 
Size: 498027 Color: 3
Size: 251742 Color: 9
Size: 250232 Color: 12

Bin 3141: 0 of cap free
Amount of items: 3
Items: 
Size: 498083 Color: 6
Size: 251771 Color: 2
Size: 250147 Color: 5

Bin 3142: 0 of cap free
Amount of items: 3
Items: 
Size: 498280 Color: 4
Size: 251279 Color: 6
Size: 250442 Color: 1

Bin 3143: 0 of cap free
Amount of items: 3
Items: 
Size: 498357 Color: 2
Size: 251073 Color: 9
Size: 250571 Color: 3

Bin 3144: 0 of cap free
Amount of items: 3
Items: 
Size: 498629 Color: 10
Size: 251130 Color: 18
Size: 250242 Color: 10

Bin 3145: 0 of cap free
Amount of items: 3
Items: 
Size: 498696 Color: 10
Size: 251299 Color: 17
Size: 250006 Color: 7

Bin 3146: 0 of cap free
Amount of items: 3
Items: 
Size: 498764 Color: 18
Size: 250721 Color: 8
Size: 250516 Color: 18

Bin 3147: 0 of cap free
Amount of items: 3
Items: 
Size: 498792 Color: 19
Size: 251112 Color: 0
Size: 250097 Color: 5

Bin 3148: 0 of cap free
Amount of items: 3
Items: 
Size: 499058 Color: 3
Size: 250884 Color: 3
Size: 250059 Color: 15

Bin 3149: 0 of cap free
Amount of items: 3
Items: 
Size: 499247 Color: 2
Size: 250489 Color: 14
Size: 250265 Color: 13

Bin 3150: 0 of cap free
Amount of items: 3
Items: 
Size: 499404 Color: 7
Size: 250445 Color: 13
Size: 250152 Color: 12

Bin 3151: 0 of cap free
Amount of items: 3
Items: 
Size: 499489 Color: 16
Size: 250482 Color: 12
Size: 250030 Color: 10

Bin 3152: 0 of cap free
Amount of items: 3
Items: 
Size: 499513 Color: 0
Size: 250395 Color: 8
Size: 250093 Color: 3

Bin 3153: 0 of cap free
Amount of items: 3
Items: 
Size: 499612 Color: 17
Size: 250264 Color: 2
Size: 250125 Color: 9

Bin 3154: 0 of cap free
Amount of items: 3
Items: 
Size: 499839 Color: 16
Size: 250133 Color: 4
Size: 250029 Color: 14

Bin 3155: 0 of cap free
Amount of items: 3
Items: 
Size: 499967 Color: 18
Size: 250022 Color: 14
Size: 250012 Color: 6

Bin 3156: 1 of cap free
Amount of items: 3
Items: 
Size: 360431 Color: 5
Size: 346812 Color: 5
Size: 292757 Color: 19

Bin 3157: 1 of cap free
Amount of items: 3
Items: 
Size: 363630 Color: 5
Size: 321901 Color: 18
Size: 314469 Color: 0

Bin 3158: 1 of cap free
Amount of items: 3
Items: 
Size: 463291 Color: 7
Size: 280180 Color: 15
Size: 256529 Color: 13

Bin 3159: 1 of cap free
Amount of items: 3
Items: 
Size: 357190 Color: 5
Size: 333782 Color: 10
Size: 309028 Color: 2

Bin 3160: 1 of cap free
Amount of items: 3
Items: 
Size: 358584 Color: 12
Size: 337224 Color: 5
Size: 304192 Color: 4

Bin 3161: 1 of cap free
Amount of items: 3
Items: 
Size: 359520 Color: 19
Size: 331387 Color: 10
Size: 309093 Color: 12

Bin 3162: 1 of cap free
Amount of items: 3
Items: 
Size: 351324 Color: 6
Size: 334143 Color: 9
Size: 314533 Color: 19

Bin 3163: 1 of cap free
Amount of items: 3
Items: 
Size: 360396 Color: 2
Size: 325132 Color: 9
Size: 314472 Color: 17

Bin 3164: 1 of cap free
Amount of items: 3
Items: 
Size: 360492 Color: 17
Size: 353657 Color: 7
Size: 285851 Color: 6

Bin 3165: 1 of cap free
Amount of items: 3
Items: 
Size: 361001 Color: 7
Size: 322799 Color: 4
Size: 316200 Color: 5

Bin 3166: 1 of cap free
Amount of items: 3
Items: 
Size: 363825 Color: 15
Size: 341700 Color: 16
Size: 294475 Color: 5

Bin 3167: 1 of cap free
Amount of items: 3
Items: 
Size: 361604 Color: 10
Size: 344055 Color: 4
Size: 294341 Color: 9

Bin 3168: 1 of cap free
Amount of items: 3
Items: 
Size: 361829 Color: 7
Size: 337673 Color: 3
Size: 300498 Color: 10

Bin 3169: 1 of cap free
Amount of items: 3
Items: 
Size: 362325 Color: 7
Size: 325447 Color: 13
Size: 312228 Color: 2

Bin 3170: 1 of cap free
Amount of items: 3
Items: 
Size: 363440 Color: 7
Size: 319511 Color: 7
Size: 317049 Color: 9

Bin 3171: 1 of cap free
Amount of items: 3
Items: 
Size: 348732 Color: 19
Size: 337152 Color: 9
Size: 314116 Color: 18

Bin 3172: 1 of cap free
Amount of items: 3
Items: 
Size: 359753 Color: 3
Size: 334717 Color: 14
Size: 305530 Color: 1

Bin 3173: 1 of cap free
Amount of items: 3
Items: 
Size: 370298 Color: 13
Size: 326052 Color: 1
Size: 303650 Color: 10

Bin 3174: 1 of cap free
Amount of items: 3
Items: 
Size: 355622 Color: 14
Size: 324284 Color: 0
Size: 320094 Color: 0

Bin 3175: 1 of cap free
Amount of items: 3
Items: 
Size: 373333 Color: 6
Size: 361612 Color: 17
Size: 265055 Color: 12

Bin 3176: 1 of cap free
Amount of items: 3
Items: 
Size: 373764 Color: 7
Size: 361269 Color: 3
Size: 264967 Color: 18

Bin 3177: 1 of cap free
Amount of items: 3
Items: 
Size: 373895 Color: 12
Size: 368869 Color: 19
Size: 257236 Color: 0

Bin 3178: 1 of cap free
Amount of items: 3
Items: 
Size: 374574 Color: 4
Size: 350546 Color: 18
Size: 274880 Color: 17

Bin 3179: 1 of cap free
Amount of items: 3
Items: 
Size: 372425 Color: 14
Size: 359264 Color: 9
Size: 268311 Color: 8

Bin 3180: 1 of cap free
Amount of items: 3
Items: 
Size: 376393 Color: 17
Size: 369033 Color: 1
Size: 254574 Color: 7

Bin 3181: 1 of cap free
Amount of items: 3
Items: 
Size: 378428 Color: 15
Size: 369935 Color: 1
Size: 251637 Color: 13

Bin 3182: 1 of cap free
Amount of items: 3
Items: 
Size: 379459 Color: 4
Size: 359888 Color: 6
Size: 260653 Color: 6

Bin 3183: 1 of cap free
Amount of items: 3
Items: 
Size: 387367 Color: 7
Size: 359871 Color: 10
Size: 252762 Color: 7

Bin 3184: 1 of cap free
Amount of items: 3
Items: 
Size: 352004 Color: 14
Size: 349635 Color: 5
Size: 298361 Color: 5

Bin 3185: 1 of cap free
Amount of items: 3
Items: 
Size: 403655 Color: 7
Size: 313636 Color: 17
Size: 282709 Color: 6

Bin 3186: 1 of cap free
Amount of items: 3
Items: 
Size: 408663 Color: 6
Size: 313659 Color: 18
Size: 277678 Color: 1

Bin 3187: 1 of cap free
Amount of items: 3
Items: 
Size: 418076 Color: 9
Size: 292852 Color: 1
Size: 289072 Color: 12

Bin 3188: 1 of cap free
Amount of items: 3
Items: 
Size: 495655 Color: 11
Size: 252926 Color: 9
Size: 251419 Color: 5

Bin 3189: 1 of cap free
Amount of items: 3
Items: 
Size: 367235 Color: 1
Size: 355745 Color: 5
Size: 277020 Color: 16

Bin 3190: 1 of cap free
Amount of items: 3
Items: 
Size: 350640 Color: 1
Size: 348890 Color: 4
Size: 300470 Color: 12

Bin 3191: 1 of cap free
Amount of items: 3
Items: 
Size: 370531 Color: 16
Size: 368420 Color: 10
Size: 261049 Color: 15

Bin 3192: 1 of cap free
Amount of items: 3
Items: 
Size: 348126 Color: 3
Size: 346333 Color: 0
Size: 305541 Color: 5

Bin 3193: 1 of cap free
Amount of items: 3
Items: 
Size: 366262 Color: 1
Size: 324559 Color: 1
Size: 309179 Color: 10

Bin 3194: 1 of cap free
Amount of items: 3
Items: 
Size: 366258 Color: 14
Size: 352786 Color: 0
Size: 280956 Color: 17

Bin 3195: 1 of cap free
Amount of items: 3
Items: 
Size: 359458 Color: 11
Size: 336258 Color: 9
Size: 304284 Color: 4

Bin 3196: 1 of cap free
Amount of items: 3
Items: 
Size: 351215 Color: 1
Size: 349019 Color: 19
Size: 299766 Color: 16

Bin 3197: 1 of cap free
Amount of items: 3
Items: 
Size: 370642 Color: 3
Size: 345733 Color: 16
Size: 283625 Color: 18

Bin 3198: 1 of cap free
Amount of items: 3
Items: 
Size: 357453 Color: 7
Size: 346940 Color: 1
Size: 295607 Color: 12

Bin 3199: 1 of cap free
Amount of items: 3
Items: 
Size: 358918 Color: 5
Size: 336316 Color: 7
Size: 304766 Color: 17

Bin 3200: 1 of cap free
Amount of items: 3
Items: 
Size: 344464 Color: 8
Size: 327813 Color: 11
Size: 327723 Color: 8

Bin 3201: 1 of cap free
Amount of items: 3
Items: 
Size: 354406 Color: 18
Size: 342769 Color: 18
Size: 302825 Color: 5

Bin 3202: 1 of cap free
Amount of items: 3
Items: 
Size: 341223 Color: 12
Size: 329782 Color: 1
Size: 328995 Color: 9

Bin 3203: 1 of cap free
Amount of items: 3
Items: 
Size: 366629 Color: 0
Size: 346473 Color: 17
Size: 286898 Color: 0

Bin 3204: 1 of cap free
Amount of items: 3
Items: 
Size: 390802 Color: 16
Size: 350757 Color: 6
Size: 258441 Color: 1

Bin 3205: 1 of cap free
Amount of items: 3
Items: 
Size: 363479 Color: 1
Size: 358509 Color: 18
Size: 278012 Color: 4

Bin 3206: 1 of cap free
Amount of items: 3
Items: 
Size: 350496 Color: 8
Size: 337264 Color: 9
Size: 312240 Color: 10

Bin 3207: 1 of cap free
Amount of items: 3
Items: 
Size: 391898 Color: 16
Size: 351345 Color: 1
Size: 256757 Color: 14

Bin 3208: 2 of cap free
Amount of items: 3
Items: 
Size: 346954 Color: 2
Size: 341779 Color: 1
Size: 311266 Color: 9

Bin 3209: 2 of cap free
Amount of items: 3
Items: 
Size: 346303 Color: 19
Size: 345258 Color: 7
Size: 308438 Color: 3

Bin 3210: 2 of cap free
Amount of items: 3
Items: 
Size: 355424 Color: 15
Size: 329986 Color: 1
Size: 314589 Color: 2

Bin 3211: 2 of cap free
Amount of items: 3
Items: 
Size: 343696 Color: 1
Size: 341098 Color: 6
Size: 315205 Color: 18

Bin 3212: 2 of cap free
Amount of items: 3
Items: 
Size: 361376 Color: 14
Size: 332184 Color: 9
Size: 306439 Color: 14

Bin 3213: 2 of cap free
Amount of items: 3
Items: 
Size: 366226 Color: 12
Size: 318462 Color: 19
Size: 315311 Color: 13

Bin 3214: 2 of cap free
Amount of items: 3
Items: 
Size: 368206 Color: 14
Size: 366076 Color: 18
Size: 265717 Color: 2

Bin 3215: 2 of cap free
Amount of items: 3
Items: 
Size: 370821 Color: 15
Size: 345490 Color: 14
Size: 283688 Color: 8

Bin 3216: 2 of cap free
Amount of items: 3
Items: 
Size: 370193 Color: 14
Size: 363425 Color: 17
Size: 266381 Color: 5

Bin 3217: 2 of cap free
Amount of items: 3
Items: 
Size: 422668 Color: 3
Size: 314185 Color: 14
Size: 263146 Color: 3

Bin 3218: 2 of cap free
Amount of items: 3
Items: 
Size: 382290 Color: 19
Size: 360707 Color: 3
Size: 257002 Color: 1

Bin 3219: 2 of cap free
Amount of items: 3
Items: 
Size: 346879 Color: 9
Size: 330084 Color: 16
Size: 323036 Color: 14

Bin 3220: 2 of cap free
Amount of items: 3
Items: 
Size: 368717 Color: 15
Size: 331617 Color: 12
Size: 299665 Color: 11

Bin 3221: 2 of cap free
Amount of items: 3
Items: 
Size: 335685 Color: 0
Size: 335373 Color: 19
Size: 328941 Color: 6

Bin 3222: 2 of cap free
Amount of items: 3
Items: 
Size: 345261 Color: 13
Size: 342177 Color: 12
Size: 312561 Color: 17

Bin 3223: 3 of cap free
Amount of items: 3
Items: 
Size: 348816 Color: 18
Size: 344438 Color: 13
Size: 306744 Color: 19

Bin 3224: 3 of cap free
Amount of items: 3
Items: 
Size: 364106 Color: 0
Size: 361620 Color: 11
Size: 274272 Color: 10

Bin 3225: 3 of cap free
Amount of items: 3
Items: 
Size: 376670 Color: 3
Size: 363576 Color: 0
Size: 259752 Color: 3

Bin 3226: 3 of cap free
Amount of items: 3
Items: 
Size: 378696 Color: 17
Size: 351164 Color: 0
Size: 270138 Color: 12

Bin 3227: 3 of cap free
Amount of items: 3
Items: 
Size: 360576 Color: 3
Size: 324051 Color: 4
Size: 315371 Color: 10

Bin 3228: 3 of cap free
Amount of items: 3
Items: 
Size: 371387 Color: 16
Size: 323733 Color: 0
Size: 304878 Color: 0

Bin 3229: 3 of cap free
Amount of items: 3
Items: 
Size: 366568 Color: 1
Size: 366271 Color: 2
Size: 267159 Color: 12

Bin 3230: 3 of cap free
Amount of items: 3
Items: 
Size: 368535 Color: 14
Size: 341249 Color: 18
Size: 290214 Color: 2

Bin 3231: 3 of cap free
Amount of items: 3
Items: 
Size: 343458 Color: 6
Size: 328333 Color: 15
Size: 328207 Color: 0

Bin 3232: 3 of cap free
Amount of items: 3
Items: 
Size: 367790 Color: 14
Size: 355548 Color: 0
Size: 276660 Color: 14

Bin 3233: 3 of cap free
Amount of items: 3
Items: 
Size: 357688 Color: 15
Size: 337774 Color: 1
Size: 304536 Color: 9

Bin 3234: 3 of cap free
Amount of items: 3
Items: 
Size: 354901 Color: 18
Size: 338460 Color: 8
Size: 306637 Color: 1

Bin 3235: 3 of cap free
Amount of items: 3
Items: 
Size: 339095 Color: 7
Size: 332930 Color: 9
Size: 327973 Color: 2

Bin 3236: 3 of cap free
Amount of items: 3
Items: 
Size: 342021 Color: 4
Size: 342014 Color: 15
Size: 315963 Color: 5

Bin 3237: 3 of cap free
Amount of items: 3
Items: 
Size: 334257 Color: 1
Size: 333914 Color: 16
Size: 331827 Color: 16

Bin 3238: 4 of cap free
Amount of items: 3
Items: 
Size: 356570 Color: 10
Size: 354959 Color: 16
Size: 288468 Color: 17

Bin 3239: 4 of cap free
Amount of items: 3
Items: 
Size: 356355 Color: 18
Size: 355331 Color: 4
Size: 288311 Color: 11

Bin 3240: 4 of cap free
Amount of items: 3
Items: 
Size: 355752 Color: 13
Size: 348715 Color: 1
Size: 295530 Color: 18

Bin 3241: 4 of cap free
Amount of items: 3
Items: 
Size: 353827 Color: 4
Size: 337925 Color: 17
Size: 308245 Color: 12

Bin 3242: 4 of cap free
Amount of items: 3
Items: 
Size: 355296 Color: 6
Size: 345965 Color: 17
Size: 298736 Color: 12

Bin 3243: 4 of cap free
Amount of items: 3
Items: 
Size: 377910 Color: 1
Size: 314385 Color: 8
Size: 307702 Color: 5

Bin 3244: 5 of cap free
Amount of items: 3
Items: 
Size: 354518 Color: 13
Size: 325645 Color: 16
Size: 319833 Color: 4

Bin 3245: 5 of cap free
Amount of items: 3
Items: 
Size: 350027 Color: 6
Size: 333817 Color: 16
Size: 316152 Color: 4

Bin 3246: 5 of cap free
Amount of items: 3
Items: 
Size: 360092 Color: 1
Size: 358383 Color: 14
Size: 281521 Color: 3

Bin 3247: 5 of cap free
Amount of items: 3
Items: 
Size: 364136 Color: 8
Size: 340472 Color: 7
Size: 295388 Color: 2

Bin 3248: 5 of cap free
Amount of items: 3
Items: 
Size: 374341 Color: 10
Size: 344872 Color: 12
Size: 280783 Color: 5

Bin 3249: 5 of cap free
Amount of items: 3
Items: 
Size: 353244 Color: 13
Size: 325882 Color: 8
Size: 320870 Color: 0

Bin 3250: 6 of cap free
Amount of items: 3
Items: 
Size: 371593 Color: 12
Size: 357392 Color: 9
Size: 271010 Color: 14

Bin 3251: 6 of cap free
Amount of items: 3
Items: 
Size: 357460 Color: 7
Size: 337794 Color: 8
Size: 304741 Color: 0

Bin 3252: 7 of cap free
Amount of items: 3
Items: 
Size: 359897 Color: 10
Size: 343072 Color: 10
Size: 297025 Color: 11

Bin 3253: 7 of cap free
Amount of items: 3
Items: 
Size: 392666 Color: 8
Size: 341730 Color: 17
Size: 265598 Color: 17

Bin 3254: 7 of cap free
Amount of items: 3
Items: 
Size: 367691 Color: 11
Size: 364443 Color: 13
Size: 267860 Color: 1

Bin 3255: 7 of cap free
Amount of items: 3
Items: 
Size: 350985 Color: 2
Size: 349072 Color: 19
Size: 299937 Color: 4

Bin 3256: 7 of cap free
Amount of items: 3
Items: 
Size: 398391 Color: 3
Size: 345043 Color: 7
Size: 256560 Color: 13

Bin 3257: 7 of cap free
Amount of items: 3
Items: 
Size: 357557 Color: 7
Size: 352048 Color: 12
Size: 290389 Color: 2

Bin 3258: 7 of cap free
Amount of items: 3
Items: 
Size: 398001 Color: 17
Size: 349393 Color: 2
Size: 252600 Color: 16

Bin 3259: 8 of cap free
Amount of items: 3
Items: 
Size: 337304 Color: 17
Size: 331794 Color: 16
Size: 330895 Color: 2

Bin 3260: 9 of cap free
Amount of items: 3
Items: 
Size: 376109 Color: 12
Size: 359285 Color: 11
Size: 264598 Color: 9

Bin 3261: 9 of cap free
Amount of items: 3
Items: 
Size: 351276 Color: 16
Size: 325810 Color: 17
Size: 322906 Color: 2

Bin 3262: 9 of cap free
Amount of items: 3
Items: 
Size: 364462 Color: 16
Size: 349969 Color: 8
Size: 285561 Color: 9

Bin 3263: 10 of cap free
Amount of items: 3
Items: 
Size: 434362 Color: 11
Size: 307374 Color: 16
Size: 258255 Color: 15

Bin 3264: 10 of cap free
Amount of items: 3
Items: 
Size: 357002 Color: 18
Size: 334934 Color: 10
Size: 308055 Color: 16

Bin 3265: 10 of cap free
Amount of items: 3
Items: 
Size: 381916 Color: 3
Size: 350944 Color: 3
Size: 267131 Color: 18

Bin 3266: 10 of cap free
Amount of items: 3
Items: 
Size: 337715 Color: 10
Size: 331176 Color: 17
Size: 331100 Color: 9

Bin 3267: 10 of cap free
Amount of items: 3
Items: 
Size: 355607 Color: 10
Size: 349656 Color: 14
Size: 294728 Color: 12

Bin 3268: 11 of cap free
Amount of items: 3
Items: 
Size: 334298 Color: 13
Size: 333074 Color: 10
Size: 332618 Color: 13

Bin 3269: 11 of cap free
Amount of items: 3
Items: 
Size: 348272 Color: 4
Size: 333249 Color: 17
Size: 318469 Color: 14

Bin 3270: 12 of cap free
Amount of items: 3
Items: 
Size: 396009 Color: 0
Size: 344854 Color: 7
Size: 259126 Color: 11

Bin 3271: 12 of cap free
Amount of items: 3
Items: 
Size: 352277 Color: 13
Size: 351023 Color: 12
Size: 296689 Color: 10

Bin 3272: 13 of cap free
Amount of items: 3
Items: 
Size: 362458 Color: 1
Size: 339723 Color: 9
Size: 297807 Color: 11

Bin 3273: 13 of cap free
Amount of items: 3
Items: 
Size: 412720 Color: 9
Size: 335495 Color: 11
Size: 251773 Color: 7

Bin 3274: 13 of cap free
Amount of items: 3
Items: 
Size: 340902 Color: 0
Size: 336071 Color: 16
Size: 323015 Color: 17

Bin 3275: 14 of cap free
Amount of items: 3
Items: 
Size: 355234 Color: 7
Size: 345664 Color: 17
Size: 299089 Color: 8

Bin 3276: 14 of cap free
Amount of items: 3
Items: 
Size: 354470 Color: 19
Size: 326988 Color: 6
Size: 318529 Color: 1

Bin 3277: 15 of cap free
Amount of items: 3
Items: 
Size: 359767 Color: 18
Size: 345022 Color: 1
Size: 295197 Color: 12

Bin 3278: 16 of cap free
Amount of items: 3
Items: 
Size: 389714 Color: 8
Size: 351358 Color: 19
Size: 258913 Color: 19

Bin 3279: 16 of cap free
Amount of items: 3
Items: 
Size: 363911 Color: 7
Size: 352952 Color: 3
Size: 283122 Color: 7

Bin 3280: 17 of cap free
Amount of items: 3
Items: 
Size: 445111 Color: 0
Size: 294826 Color: 10
Size: 260047 Color: 6

Bin 3281: 17 of cap free
Amount of items: 3
Items: 
Size: 366492 Color: 16
Size: 365472 Color: 5
Size: 268020 Color: 5

Bin 3282: 20 of cap free
Amount of items: 3
Items: 
Size: 344105 Color: 7
Size: 331563 Color: 6
Size: 324313 Color: 6

Bin 3283: 24 of cap free
Amount of items: 3
Items: 
Size: 346206 Color: 1
Size: 329171 Color: 19
Size: 324600 Color: 1

Bin 3284: 25 of cap free
Amount of items: 3
Items: 
Size: 369908 Color: 11
Size: 366859 Color: 8
Size: 263209 Color: 16

Bin 3285: 26 of cap free
Amount of items: 3
Items: 
Size: 439272 Color: 18
Size: 310421 Color: 2
Size: 250282 Color: 9

Bin 3286: 28 of cap free
Amount of items: 3
Items: 
Size: 339481 Color: 2
Size: 337611 Color: 2
Size: 322881 Color: 16

Bin 3287: 32 of cap free
Amount of items: 3
Items: 
Size: 408155 Color: 0
Size: 328415 Color: 18
Size: 263399 Color: 0

Bin 3288: 32 of cap free
Amount of items: 3
Items: 
Size: 371332 Color: 2
Size: 362440 Color: 0
Size: 266197 Color: 4

Bin 3289: 34 of cap free
Amount of items: 3
Items: 
Size: 350639 Color: 16
Size: 325224 Color: 14
Size: 324104 Color: 13

Bin 3290: 37 of cap free
Amount of items: 3
Items: 
Size: 399923 Color: 4
Size: 347294 Color: 10
Size: 252747 Color: 10

Bin 3291: 37 of cap free
Amount of items: 3
Items: 
Size: 364641 Color: 2
Size: 358343 Color: 2
Size: 276980 Color: 4

Bin 3292: 41 of cap free
Amount of items: 3
Items: 
Size: 357623 Color: 17
Size: 333146 Color: 8
Size: 309191 Color: 4

Bin 3293: 43 of cap free
Amount of items: 3
Items: 
Size: 442069 Color: 1
Size: 303441 Color: 5
Size: 254448 Color: 1

Bin 3294: 43 of cap free
Amount of items: 3
Items: 
Size: 378189 Color: 6
Size: 356370 Color: 6
Size: 265399 Color: 13

Bin 3295: 45 of cap free
Amount of items: 3
Items: 
Size: 334019 Color: 3
Size: 333541 Color: 13
Size: 332396 Color: 3

Bin 3296: 50 of cap free
Amount of items: 3
Items: 
Size: 364373 Color: 16
Size: 360141 Color: 2
Size: 275437 Color: 16

Bin 3297: 51 of cap free
Amount of items: 3
Items: 
Size: 385255 Color: 7
Size: 354702 Color: 17
Size: 259993 Color: 14

Bin 3298: 52 of cap free
Amount of items: 3
Items: 
Size: 445677 Color: 10
Size: 294662 Color: 12
Size: 259610 Color: 3

Bin 3299: 60 of cap free
Amount of items: 3
Items: 
Size: 359999 Color: 16
Size: 358139 Color: 5
Size: 281803 Color: 11

Bin 3300: 65 of cap free
Amount of items: 3
Items: 
Size: 358325 Color: 5
Size: 321919 Color: 2
Size: 319692 Color: 5

Bin 3301: 68 of cap free
Amount of items: 3
Items: 
Size: 361928 Color: 17
Size: 359118 Color: 14
Size: 278887 Color: 0

Bin 3302: 69 of cap free
Amount of items: 3
Items: 
Size: 337588 Color: 17
Size: 336563 Color: 7
Size: 325781 Color: 18

Bin 3303: 70 of cap free
Amount of items: 3
Items: 
Size: 350210 Color: 4
Size: 350180 Color: 15
Size: 299541 Color: 1

Bin 3304: 79 of cap free
Amount of items: 3
Items: 
Size: 405076 Color: 5
Size: 336151 Color: 0
Size: 258695 Color: 7

Bin 3305: 80 of cap free
Amount of items: 3
Items: 
Size: 346635 Color: 0
Size: 346004 Color: 16
Size: 307282 Color: 3

Bin 3306: 98 of cap free
Amount of items: 3
Items: 
Size: 363834 Color: 1
Size: 363266 Color: 8
Size: 272803 Color: 12

Bin 3307: 120 of cap free
Amount of items: 3
Items: 
Size: 376689 Color: 11
Size: 356363 Color: 11
Size: 266829 Color: 6

Bin 3308: 157 of cap free
Amount of items: 3
Items: 
Size: 367989 Color: 14
Size: 364637 Color: 0
Size: 267218 Color: 10

Bin 3309: 158 of cap free
Amount of items: 3
Items: 
Size: 410419 Color: 11
Size: 326278 Color: 15
Size: 263146 Color: 12

Bin 3310: 180 of cap free
Amount of items: 3
Items: 
Size: 364020 Color: 16
Size: 360308 Color: 6
Size: 275493 Color: 13

Bin 3311: 191 of cap free
Amount of items: 3
Items: 
Size: 488041 Color: 5
Size: 256238 Color: 12
Size: 255531 Color: 0

Bin 3312: 207 of cap free
Amount of items: 3
Items: 
Size: 444757 Color: 12
Size: 294920 Color: 0
Size: 260117 Color: 13

Bin 3313: 214 of cap free
Amount of items: 3
Items: 
Size: 347372 Color: 13
Size: 336813 Color: 1
Size: 315602 Color: 11

Bin 3314: 242 of cap free
Amount of items: 3
Items: 
Size: 359747 Color: 1
Size: 358866 Color: 1
Size: 281146 Color: 13

Bin 3315: 259 of cap free
Amount of items: 3
Items: 
Size: 367464 Color: 12
Size: 363203 Color: 4
Size: 269075 Color: 5

Bin 3316: 2026 of cap free
Amount of items: 2
Items: 
Size: 499020 Color: 11
Size: 498955 Color: 4

Bin 3317: 6723 of cap free
Amount of items: 3
Items: 
Size: 456412 Color: 5
Size: 269650 Color: 15
Size: 267216 Color: 16

Bin 3318: 15203 of cap free
Amount of items: 3
Items: 
Size: 360610 Color: 15
Size: 360202 Color: 8
Size: 263986 Color: 2

Bin 3319: 20094 of cap free
Amount of items: 3
Items: 
Size: 358241 Color: 9
Size: 357713 Color: 19
Size: 263953 Color: 2

Bin 3320: 24373 of cap free
Amount of items: 3
Items: 
Size: 356564 Color: 9
Size: 356554 Color: 19
Size: 262510 Color: 11

Bin 3321: 25091 of cap free
Amount of items: 3
Items: 
Size: 356307 Color: 17
Size: 356160 Color: 5
Size: 262443 Color: 16

Bin 3322: 30043 of cap free
Amount of items: 3
Items: 
Size: 447936 Color: 11
Size: 261243 Color: 11
Size: 260779 Color: 13

Bin 3323: 32618 of cap free
Amount of items: 3
Items: 
Size: 354208 Color: 1
Size: 353761 Color: 2
Size: 259414 Color: 1

Bin 3324: 35023 of cap free
Amount of items: 3
Items: 
Size: 353659 Color: 5
Size: 352644 Color: 6
Size: 258675 Color: 9

Bin 3325: 36660 of cap free
Amount of items: 3
Items: 
Size: 352352 Color: 1
Size: 352314 Color: 15
Size: 258675 Color: 6

Bin 3326: 39676 of cap free
Amount of items: 3
Items: 
Size: 351257 Color: 1
Size: 350393 Color: 3
Size: 258675 Color: 14

Bin 3327: 44651 of cap free
Amount of items: 3
Items: 
Size: 349945 Color: 18
Size: 349945 Color: 10
Size: 255460 Color: 0

Bin 3328: 47544 of cap free
Amount of items: 3
Items: 
Size: 348839 Color: 6
Size: 348833 Color: 9
Size: 254785 Color: 0

Bin 3329: 49520 of cap free
Amount of items: 3
Items: 
Size: 442665 Color: 8
Size: 254561 Color: 14
Size: 253255 Color: 17

Bin 3330: 51228 of cap free
Amount of items: 3
Items: 
Size: 348216 Color: 4
Size: 347628 Color: 15
Size: 252929 Color: 9

Bin 3331: 52899 of cap free
Amount of items: 3
Items: 
Size: 441500 Color: 5
Size: 252880 Color: 3
Size: 252722 Color: 1

Bin 3332: 57133 of cap free
Amount of items: 3
Items: 
Size: 346121 Color: 17
Size: 344025 Color: 10
Size: 252722 Color: 10

Bin 3333: 59404 of cap free
Amount of items: 3
Items: 
Size: 436990 Color: 17
Size: 251843 Color: 1
Size: 251764 Color: 6

Bin 3334: 138262 of cap free
Amount of items: 2
Items: 
Size: 431325 Color: 0
Size: 430414 Color: 17

Bin 3335: 228372 of cap free
Amount of items: 2
Items: 
Size: 427858 Color: 18
Size: 343771 Color: 2

Total size: 3334003334
Total free space: 1000001

