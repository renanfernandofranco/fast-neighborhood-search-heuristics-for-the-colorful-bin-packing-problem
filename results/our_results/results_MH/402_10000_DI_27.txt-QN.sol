Capicity Bin: 8184
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4040 Color: 272
Size: 1224 Color: 188
Size: 1112 Color: 179
Size: 624 Color: 129
Size: 520 Color: 115
Size: 512 Color: 113
Size: 152 Color: 18

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 7076 Color: 376
Size: 924 Color: 163
Size: 136 Color: 9
Size: 48 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 356
Size: 1188 Color: 186
Size: 232 Color: 58

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4098 Color: 276
Size: 3406 Color: 266
Size: 680 Color: 136

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 333
Size: 1564 Color: 208
Size: 312 Color: 79

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 318
Size: 1948 Color: 223
Size: 384 Color: 92

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 287
Size: 2950 Color: 254
Size: 588 Color: 121

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4443 Color: 282
Size: 3119 Color: 259
Size: 622 Color: 128

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6746 Color: 354
Size: 1202 Color: 187
Size: 236 Color: 61

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6374 Color: 337
Size: 1510 Color: 204
Size: 300 Color: 76

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6973 Color: 370
Size: 1011 Color: 169
Size: 200 Color: 45

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6941 Color: 366
Size: 1037 Color: 174
Size: 206 Color: 48

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4388 Color: 280
Size: 3164 Color: 262
Size: 632 Color: 131

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 4288 Color: 279
Size: 3256 Color: 264
Size: 456 Color: 105
Size: 184 Color: 36

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4670 Color: 289
Size: 2930 Color: 252
Size: 584 Color: 120

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4444 Color: 283
Size: 3124 Color: 260
Size: 616 Color: 125

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7038 Color: 372
Size: 958 Color: 167
Size: 188 Color: 40

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4475 Color: 284
Size: 3091 Color: 258
Size: 618 Color: 127

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6546 Color: 344
Size: 1450 Color: 201
Size: 188 Color: 38

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6635 Color: 348
Size: 1291 Color: 193
Size: 258 Color: 66

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 355
Size: 1186 Color: 184
Size: 236 Color: 60

Bin 22: 0 of cap free
Amount of items: 4
Items: 
Size: 4095 Color: 275
Size: 3577 Color: 270
Size: 416 Color: 95
Size: 96 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5514 Color: 306
Size: 2494 Color: 242
Size: 176 Color: 32

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5126 Color: 297
Size: 2550 Color: 244
Size: 508 Color: 112

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6924 Color: 365
Size: 1052 Color: 175
Size: 208 Color: 49

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6473 Color: 340
Size: 1427 Color: 200
Size: 284 Color: 72

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 346
Size: 1318 Color: 196
Size: 260 Color: 67

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 303
Size: 2356 Color: 236
Size: 464 Color: 107

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 310
Size: 2154 Color: 231
Size: 428 Color: 100

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5134 Color: 298
Size: 2542 Color: 243
Size: 508 Color: 111

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5961 Color: 321
Size: 1853 Color: 220
Size: 370 Color: 89

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6687 Color: 353
Size: 1249 Color: 189
Size: 248 Color: 62

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7364 Color: 402
Size: 684 Color: 137
Size: 136 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5610 Color: 311
Size: 2146 Color: 229
Size: 428 Color: 99

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 323
Size: 1844 Color: 219
Size: 360 Color: 85

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 369
Size: 1014 Color: 170
Size: 200 Color: 43

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6162 Color: 329
Size: 1686 Color: 211
Size: 336 Color: 82

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5196 Color: 300
Size: 2492 Color: 241
Size: 496 Color: 110

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 335
Size: 1524 Color: 206
Size: 296 Color: 75

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7183 Color: 384
Size: 835 Color: 152
Size: 166 Color: 26

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5301 Color: 302
Size: 2403 Color: 238
Size: 480 Color: 108

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4487 Color: 285
Size: 3081 Color: 257
Size: 616 Color: 126

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5981 Color: 324
Size: 1837 Color: 217
Size: 366 Color: 88

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6923 Color: 364
Size: 1187 Color: 185
Size: 74 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6646 Color: 349
Size: 1282 Color: 192
Size: 256 Color: 65

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7154 Color: 383
Size: 862 Color: 155
Size: 168 Color: 28

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 4756 Color: 291
Size: 2860 Color: 251
Size: 568 Color: 119

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5977 Color: 322
Size: 1841 Color: 218
Size: 366 Color: 87

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 7304 Color: 397
Size: 744 Color: 140
Size: 136 Color: 10

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6837 Color: 360
Size: 1123 Color: 180
Size: 224 Color: 54

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 351
Size: 1256 Color: 191
Size: 248 Color: 64

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7138 Color: 380
Size: 874 Color: 157
Size: 172 Color: 30

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 392
Size: 764 Color: 145
Size: 144 Color: 15

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5562 Color: 307
Size: 2186 Color: 233
Size: 436 Color: 103

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 341
Size: 1412 Color: 199
Size: 280 Color: 71

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 396
Size: 748 Color: 141
Size: 144 Color: 14

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 5618 Color: 314
Size: 2142 Color: 228
Size: 424 Color: 97

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7027 Color: 371
Size: 965 Color: 168
Size: 192 Color: 41

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6366 Color: 336
Size: 1518 Color: 205
Size: 300 Color: 77

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5914 Color: 320
Size: 1894 Color: 221
Size: 376 Color: 91

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 317
Size: 2076 Color: 224
Size: 408 Color: 93

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 7282 Color: 395
Size: 754 Color: 143
Size: 148 Color: 16

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7220 Color: 389
Size: 804 Color: 148
Size: 160 Color: 21

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 379
Size: 881 Color: 159
Size: 176 Color: 33

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6311 Color: 334
Size: 1561 Color: 207
Size: 312 Color: 80

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4923 Color: 292
Size: 2719 Color: 249
Size: 542 Color: 118

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 4939 Color: 293
Size: 2705 Color: 248
Size: 540 Color: 117

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 7126 Color: 378
Size: 882 Color: 160
Size: 176 Color: 31

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 3592 Color: 271
Size: 3016 Color: 256
Size: 1576 Color: 209

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5601 Color: 309
Size: 2153 Color: 230
Size: 430 Color: 101

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6528 Color: 342
Size: 1016 Color: 171
Size: 640 Color: 132

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 5150 Color: 299
Size: 2626 Color: 246
Size: 408 Color: 94

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 375
Size: 938 Color: 164
Size: 184 Color: 37

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6778 Color: 357
Size: 1174 Color: 183
Size: 232 Color: 59

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6390 Color: 338
Size: 1498 Color: 203
Size: 296 Color: 74

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6964 Color: 367
Size: 1020 Color: 173
Size: 200 Color: 46

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 381
Size: 876 Color: 158
Size: 168 Color: 27

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 385
Size: 836 Color: 153
Size: 160 Color: 23

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 5649 Color: 315
Size: 2467 Color: 240
Size: 68 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5617 Color: 313
Size: 2141 Color: 227
Size: 426 Color: 98

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 294
Size: 2660 Color: 247
Size: 528 Color: 116

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 4708 Color: 290
Size: 3252 Color: 263
Size: 224 Color: 56

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5382 Color: 304
Size: 2338 Color: 235
Size: 464 Color: 106

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 7060 Color: 374
Size: 940 Color: 165
Size: 184 Color: 35

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 4094 Color: 274
Size: 3410 Color: 267
Size: 680 Color: 134

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5285 Color: 301
Size: 2417 Color: 239
Size: 482 Color: 109

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7278 Color: 394
Size: 846 Color: 154
Size: 60 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6532 Color: 343
Size: 1380 Color: 198
Size: 272 Color: 70

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4654 Color: 288
Size: 2942 Color: 253
Size: 588 Color: 122

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6303 Color: 332
Size: 1719 Color: 214
Size: 162 Color: 25

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 352
Size: 1252 Color: 190
Size: 248 Color: 63

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 7309 Color: 398
Size: 817 Color: 149
Size: 58 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6828 Color: 359
Size: 1132 Color: 181
Size: 224 Color: 55

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5661 Color: 316
Size: 2103 Color: 226
Size: 420 Color: 96

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 7350 Color: 401
Size: 754 Color: 142
Size: 80 Color: 7

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 5612 Color: 312
Size: 2372 Color: 237
Size: 200 Color: 42

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 386
Size: 914 Color: 162
Size: 76 Color: 6

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 5500 Color: 305
Size: 2244 Color: 234
Size: 440 Color: 104

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 361
Size: 1092 Color: 178
Size: 216 Color: 52

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 7342 Color: 400
Size: 702 Color: 138
Size: 140 Color: 13

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 4598 Color: 286
Size: 2990 Color: 255
Size: 596 Color: 123

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 7146 Color: 382
Size: 866 Color: 156
Size: 172 Color: 29

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 4093 Color: 273
Size: 3411 Color: 268
Size: 680 Color: 135

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6028 Color: 326
Size: 2092 Color: 225
Size: 64 Color: 3

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5078 Color: 295
Size: 2590 Color: 245
Size: 516 Color: 114

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 6158 Color: 328
Size: 1690 Color: 212
Size: 336 Color: 83

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6437 Color: 339
Size: 1457 Color: 202
Size: 290 Color: 73

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 7330 Color: 399
Size: 714 Color: 139
Size: 140 Color: 12

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 4427 Color: 281
Size: 3131 Color: 261
Size: 626 Color: 130

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6878 Color: 362
Size: 1090 Color: 177
Size: 216 Color: 51

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 4100 Color: 277
Size: 3484 Color: 269
Size: 600 Color: 124

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5587 Color: 308
Size: 2165 Color: 232
Size: 432 Color: 102

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 7202 Color: 387
Size: 822 Color: 151
Size: 160 Color: 22

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6233 Color: 330
Size: 1627 Color: 210
Size: 324 Color: 81

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5989 Color: 325
Size: 1831 Color: 216
Size: 364 Color: 86

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 363
Size: 1086 Color: 176
Size: 216 Color: 50

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5084 Color: 296
Size: 2796 Color: 250
Size: 304 Color: 78

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 327
Size: 1700 Color: 213
Size: 336 Color: 84

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 345
Size: 1334 Color: 197
Size: 264 Color: 69

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 368
Size: 1018 Color: 172
Size: 200 Color: 44

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4124 Color: 278
Size: 3388 Color: 265
Size: 672 Color: 133

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6675 Color: 350
Size: 1291 Color: 194
Size: 218 Color: 53

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5910 Color: 319
Size: 1898 Color: 222
Size: 376 Color: 90

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 358
Size: 1141 Color: 182
Size: 228 Color: 57

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 7230 Color: 390
Size: 798 Color: 147
Size: 156 Color: 19

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6243 Color: 331
Size: 1739 Color: 215
Size: 202 Color: 47

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 7053 Color: 373
Size: 943 Color: 166
Size: 188 Color: 39

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6607 Color: 347
Size: 1315 Color: 195
Size: 262 Color: 68

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 7115 Color: 377
Size: 891 Color: 161
Size: 178 Color: 34

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 7203 Color: 388
Size: 819 Color: 150
Size: 162 Color: 24

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 7247 Color: 391
Size: 781 Color: 146
Size: 156 Color: 20

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7277 Color: 393
Size: 757 Color: 144
Size: 150 Color: 17

Total size: 1080288
Total free space: 0

