Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 258 Color: 0
Size: 250 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 0
Size: 279 Color: 1
Size: 267 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 316 Color: 0
Size: 263 Color: 1
Size: 422 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 321 Color: 1
Size: 304 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 317 Color: 0
Size: 295 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 308 Color: 0
Size: 280 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 279 Color: 0
Size: 263 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 286 Color: 0
Size: 250 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 347 Color: 0
Size: 278 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 361 Color: 0
Size: 252 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 314 Color: 1
Size: 306 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 1
Size: 343 Color: 0
Size: 311 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 0
Size: 339 Color: 1
Size: 303 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 350 Color: 0
Size: 254 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 337 Color: 1
Size: 251 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 348 Color: 1
Size: 284 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 296 Color: 1
Size: 265 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 322 Color: 1
Size: 262 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 363 Color: 0
Size: 263 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 333 Color: 1
Size: 277 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 338 Color: 0
Size: 301 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 325 Color: 0
Size: 323 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 1
Size: 307 Color: 0
Size: 345 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 284 Color: 0
Size: 252 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 326 Color: 1
Size: 290 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 1
Size: 335 Color: 0
Size: 326 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 323 Color: 0
Size: 305 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 341 Color: 1
Size: 264 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 0
Size: 323 Color: 0
Size: 320 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 301 Color: 0
Size: 289 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 343 Color: 0
Size: 301 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 291 Color: 0
Size: 266 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 302 Color: 0
Size: 277 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 344 Color: 0
Size: 296 Color: 1

Total size: 34034
Total free space: 0

