Capicity Bin: 1000001
Lower Bound: 233

Bins used: 234
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 511565 Color: 4
Size: 310643 Color: 15
Size: 177793 Color: 11

Bin 2: 1 of cap free
Amount of items: 3
Items: 
Size: 510041 Color: 17
Size: 335996 Color: 16
Size: 153963 Color: 11

Bin 3: 2 of cap free
Amount of items: 3
Items: 
Size: 727569 Color: 6
Size: 158751 Color: 12
Size: 113679 Color: 17

Bin 4: 4 of cap free
Amount of items: 3
Items: 
Size: 666339 Color: 13
Size: 179761 Color: 18
Size: 153897 Color: 13

Bin 5: 7 of cap free
Amount of items: 2
Items: 
Size: 708960 Color: 13
Size: 291034 Color: 7

Bin 6: 11 of cap free
Amount of items: 3
Items: 
Size: 395100 Color: 13
Size: 304608 Color: 13
Size: 300282 Color: 14

Bin 7: 18 of cap free
Amount of items: 2
Items: 
Size: 707729 Color: 7
Size: 292254 Color: 9

Bin 8: 23 of cap free
Amount of items: 3
Items: 
Size: 768293 Color: 0
Size: 130194 Color: 16
Size: 101491 Color: 16

Bin 9: 48 of cap free
Amount of items: 2
Items: 
Size: 533928 Color: 18
Size: 466025 Color: 2

Bin 10: 57 of cap free
Amount of items: 2
Items: 
Size: 781759 Color: 9
Size: 218185 Color: 19

Bin 11: 69 of cap free
Amount of items: 3
Items: 
Size: 559841 Color: 15
Size: 298734 Color: 2
Size: 141357 Color: 8

Bin 12: 72 of cap free
Amount of items: 2
Items: 
Size: 703555 Color: 14
Size: 296374 Color: 3

Bin 13: 76 of cap free
Amount of items: 2
Items: 
Size: 739844 Color: 0
Size: 260081 Color: 2

Bin 14: 83 of cap free
Amount of items: 2
Items: 
Size: 621128 Color: 15
Size: 378790 Color: 13

Bin 15: 84 of cap free
Amount of items: 3
Items: 
Size: 761139 Color: 9
Size: 130443 Color: 0
Size: 108335 Color: 0

Bin 16: 85 of cap free
Amount of items: 3
Items: 
Size: 758054 Color: 1
Size: 122942 Color: 2
Size: 118920 Color: 2

Bin 17: 86 of cap free
Amount of items: 2
Items: 
Size: 617597 Color: 17
Size: 382318 Color: 15

Bin 18: 108 of cap free
Amount of items: 2
Items: 
Size: 634374 Color: 13
Size: 365519 Color: 12

Bin 19: 113 of cap free
Amount of items: 2
Items: 
Size: 676663 Color: 4
Size: 323225 Color: 15

Bin 20: 115 of cap free
Amount of items: 3
Items: 
Size: 502735 Color: 11
Size: 389410 Color: 5
Size: 107741 Color: 1

Bin 21: 128 of cap free
Amount of items: 3
Items: 
Size: 734367 Color: 6
Size: 134688 Color: 15
Size: 130818 Color: 7

Bin 22: 128 of cap free
Amount of items: 2
Items: 
Size: 641003 Color: 1
Size: 358870 Color: 7

Bin 23: 130 of cap free
Amount of items: 3
Items: 
Size: 554834 Color: 19
Size: 297901 Color: 7
Size: 147136 Color: 18

Bin 24: 136 of cap free
Amount of items: 3
Items: 
Size: 761771 Color: 17
Size: 119173 Color: 18
Size: 118921 Color: 17

Bin 25: 142 of cap free
Amount of items: 2
Items: 
Size: 589337 Color: 17
Size: 410522 Color: 3

Bin 26: 146 of cap free
Amount of items: 2
Items: 
Size: 550979 Color: 10
Size: 448876 Color: 11

Bin 27: 146 of cap free
Amount of items: 2
Items: 
Size: 794093 Color: 1
Size: 205762 Color: 7

Bin 28: 150 of cap free
Amount of items: 2
Items: 
Size: 680746 Color: 13
Size: 319105 Color: 1

Bin 29: 170 of cap free
Amount of items: 2
Items: 
Size: 584215 Color: 17
Size: 415616 Color: 13

Bin 30: 172 of cap free
Amount of items: 3
Items: 
Size: 769095 Color: 4
Size: 125154 Color: 13
Size: 105580 Color: 1

Bin 31: 172 of cap free
Amount of items: 2
Items: 
Size: 629593 Color: 12
Size: 370236 Color: 2

Bin 32: 176 of cap free
Amount of items: 3
Items: 
Size: 524687 Color: 0
Size: 296591 Color: 16
Size: 178547 Color: 10

Bin 33: 187 of cap free
Amount of items: 2
Items: 
Size: 694401 Color: 13
Size: 305413 Color: 4

Bin 34: 203 of cap free
Amount of items: 2
Items: 
Size: 514765 Color: 16
Size: 485033 Color: 3

Bin 35: 205 of cap free
Amount of items: 3
Items: 
Size: 521535 Color: 6
Size: 306085 Color: 8
Size: 172176 Color: 13

Bin 36: 210 of cap free
Amount of items: 2
Items: 
Size: 792331 Color: 16
Size: 207460 Color: 2

Bin 37: 217 of cap free
Amount of items: 3
Items: 
Size: 770778 Color: 2
Size: 127871 Color: 6
Size: 101135 Color: 16

Bin 38: 232 of cap free
Amount of items: 2
Items: 
Size: 516054 Color: 1
Size: 483715 Color: 15

Bin 39: 240 of cap free
Amount of items: 3
Items: 
Size: 675779 Color: 15
Size: 166587 Color: 18
Size: 157395 Color: 19

Bin 40: 246 of cap free
Amount of items: 3
Items: 
Size: 527053 Color: 15
Size: 315769 Color: 13
Size: 156933 Color: 15

Bin 41: 253 of cap free
Amount of items: 2
Items: 
Size: 677998 Color: 10
Size: 321750 Color: 1

Bin 42: 264 of cap free
Amount of items: 2
Items: 
Size: 744295 Color: 15
Size: 255442 Color: 19

Bin 43: 278 of cap free
Amount of items: 2
Items: 
Size: 517276 Color: 0
Size: 482447 Color: 19

Bin 44: 283 of cap free
Amount of items: 2
Items: 
Size: 673863 Color: 4
Size: 325855 Color: 9

Bin 45: 295 of cap free
Amount of items: 2
Items: 
Size: 707020 Color: 1
Size: 292686 Color: 13

Bin 46: 302 of cap free
Amount of items: 2
Items: 
Size: 538171 Color: 3
Size: 461528 Color: 1

Bin 47: 312 of cap free
Amount of items: 3
Items: 
Size: 728563 Color: 19
Size: 135857 Color: 13
Size: 135269 Color: 4

Bin 48: 315 of cap free
Amount of items: 2
Items: 
Size: 777749 Color: 8
Size: 221937 Color: 4

Bin 49: 344 of cap free
Amount of items: 2
Items: 
Size: 521086 Color: 19
Size: 478571 Color: 2

Bin 50: 345 of cap free
Amount of items: 3
Items: 
Size: 773691 Color: 3
Size: 118064 Color: 1
Size: 107901 Color: 15

Bin 51: 348 of cap free
Amount of items: 2
Items: 
Size: 706121 Color: 16
Size: 293532 Color: 10

Bin 52: 355 of cap free
Amount of items: 2
Items: 
Size: 729313 Color: 6
Size: 270333 Color: 2

Bin 53: 356 of cap free
Amount of items: 2
Items: 
Size: 576619 Color: 8
Size: 423026 Color: 11

Bin 54: 364 of cap free
Amount of items: 2
Items: 
Size: 605056 Color: 12
Size: 394581 Color: 9

Bin 55: 364 of cap free
Amount of items: 2
Items: 
Size: 684999 Color: 13
Size: 314638 Color: 17

Bin 56: 366 of cap free
Amount of items: 2
Items: 
Size: 516751 Color: 2
Size: 482884 Color: 13

Bin 57: 378 of cap free
Amount of items: 2
Items: 
Size: 679769 Color: 8
Size: 319854 Color: 16

Bin 58: 386 of cap free
Amount of items: 2
Items: 
Size: 508951 Color: 3
Size: 490664 Color: 0

Bin 59: 387 of cap free
Amount of items: 2
Items: 
Size: 506879 Color: 16
Size: 492735 Color: 15

Bin 60: 387 of cap free
Amount of items: 2
Items: 
Size: 656484 Color: 12
Size: 343130 Color: 8

Bin 61: 428 of cap free
Amount of items: 2
Items: 
Size: 519333 Color: 16
Size: 480240 Color: 1

Bin 62: 449 of cap free
Amount of items: 2
Items: 
Size: 565383 Color: 15
Size: 434169 Color: 10

Bin 63: 470 of cap free
Amount of items: 2
Items: 
Size: 672764 Color: 7
Size: 326767 Color: 11

Bin 64: 491 of cap free
Amount of items: 3
Items: 
Size: 449362 Color: 3
Size: 397054 Color: 6
Size: 153094 Color: 14

Bin 65: 494 of cap free
Amount of items: 2
Items: 
Size: 633171 Color: 16
Size: 366336 Color: 1

Bin 66: 494 of cap free
Amount of items: 2
Items: 
Size: 771464 Color: 4
Size: 228043 Color: 5

Bin 67: 518 of cap free
Amount of items: 2
Items: 
Size: 781192 Color: 7
Size: 218291 Color: 8

Bin 68: 523 of cap free
Amount of items: 2
Items: 
Size: 560564 Color: 14
Size: 438914 Color: 9

Bin 69: 534 of cap free
Amount of items: 2
Items: 
Size: 615182 Color: 9
Size: 384285 Color: 1

Bin 70: 554 of cap free
Amount of items: 2
Items: 
Size: 737978 Color: 8
Size: 261469 Color: 2

Bin 71: 556 of cap free
Amount of items: 3
Items: 
Size: 665813 Color: 9
Size: 169479 Color: 18
Size: 164153 Color: 5

Bin 72: 626 of cap free
Amount of items: 2
Items: 
Size: 569259 Color: 15
Size: 430116 Color: 11

Bin 73: 639 of cap free
Amount of items: 2
Items: 
Size: 674902 Color: 12
Size: 324460 Color: 1

Bin 74: 655 of cap free
Amount of items: 3
Items: 
Size: 674222 Color: 1
Size: 162869 Color: 5
Size: 162255 Color: 1

Bin 75: 667 of cap free
Amount of items: 3
Items: 
Size: 523355 Color: 10
Size: 297094 Color: 2
Size: 178885 Color: 19

Bin 76: 686 of cap free
Amount of items: 2
Items: 
Size: 561748 Color: 17
Size: 437567 Color: 10

Bin 77: 690 of cap free
Amount of items: 2
Items: 
Size: 567463 Color: 0
Size: 431848 Color: 9

Bin 78: 698 of cap free
Amount of items: 2
Items: 
Size: 733282 Color: 17
Size: 266021 Color: 11

Bin 79: 705 of cap free
Amount of items: 2
Items: 
Size: 504856 Color: 17
Size: 494440 Color: 7

Bin 80: 719 of cap free
Amount of items: 2
Items: 
Size: 519935 Color: 17
Size: 479347 Color: 3

Bin 81: 731 of cap free
Amount of items: 2
Items: 
Size: 508588 Color: 8
Size: 490682 Color: 14

Bin 82: 752 of cap free
Amount of items: 2
Items: 
Size: 789438 Color: 5
Size: 209811 Color: 17

Bin 83: 762 of cap free
Amount of items: 2
Items: 
Size: 781856 Color: 15
Size: 217383 Color: 17

Bin 84: 764 of cap free
Amount of items: 2
Items: 
Size: 650069 Color: 15
Size: 349168 Color: 17

Bin 85: 766 of cap free
Amount of items: 2
Items: 
Size: 544984 Color: 4
Size: 454251 Color: 14

Bin 86: 768 of cap free
Amount of items: 2
Items: 
Size: 542607 Color: 16
Size: 456626 Color: 9

Bin 87: 773 of cap free
Amount of items: 2
Items: 
Size: 586533 Color: 19
Size: 412695 Color: 16

Bin 88: 776 of cap free
Amount of items: 2
Items: 
Size: 696530 Color: 14
Size: 302695 Color: 11

Bin 89: 810 of cap free
Amount of items: 2
Items: 
Size: 646761 Color: 19
Size: 352430 Color: 7

Bin 90: 816 of cap free
Amount of items: 2
Items: 
Size: 786513 Color: 16
Size: 212672 Color: 5

Bin 91: 818 of cap free
Amount of items: 2
Items: 
Size: 618363 Color: 17
Size: 380820 Color: 15

Bin 92: 825 of cap free
Amount of items: 2
Items: 
Size: 742599 Color: 11
Size: 256577 Color: 2

Bin 93: 838 of cap free
Amount of items: 2
Items: 
Size: 555969 Color: 6
Size: 443194 Color: 10

Bin 94: 856 of cap free
Amount of items: 2
Items: 
Size: 704687 Color: 15
Size: 294458 Color: 19

Bin 95: 870 of cap free
Amount of items: 2
Items: 
Size: 694741 Color: 13
Size: 304390 Color: 18

Bin 96: 887 of cap free
Amount of items: 3
Items: 
Size: 666790 Color: 7
Size: 171958 Color: 14
Size: 160366 Color: 9

Bin 97: 903 of cap free
Amount of items: 2
Items: 
Size: 636765 Color: 19
Size: 362333 Color: 17

Bin 98: 927 of cap free
Amount of items: 2
Items: 
Size: 513337 Color: 3
Size: 485737 Color: 8

Bin 99: 943 of cap free
Amount of items: 2
Items: 
Size: 554105 Color: 8
Size: 444953 Color: 19

Bin 100: 946 of cap free
Amount of items: 2
Items: 
Size: 515123 Color: 9
Size: 483932 Color: 10

Bin 101: 965 of cap free
Amount of items: 2
Items: 
Size: 705686 Color: 5
Size: 293350 Color: 14

Bin 102: 966 of cap free
Amount of items: 2
Items: 
Size: 681127 Color: 1
Size: 317908 Color: 0

Bin 103: 982 of cap free
Amount of items: 2
Items: 
Size: 635676 Color: 16
Size: 363343 Color: 17

Bin 104: 996 of cap free
Amount of items: 2
Items: 
Size: 686695 Color: 15
Size: 312310 Color: 6

Bin 105: 1008 of cap free
Amount of items: 2
Items: 
Size: 710397 Color: 17
Size: 288596 Color: 3

Bin 106: 1060 of cap free
Amount of items: 2
Items: 
Size: 519352 Color: 14
Size: 479589 Color: 0

Bin 107: 1074 of cap free
Amount of items: 2
Items: 
Size: 595265 Color: 0
Size: 403662 Color: 7

Bin 108: 1098 of cap free
Amount of items: 2
Items: 
Size: 596097 Color: 11
Size: 402806 Color: 3

Bin 109: 1120 of cap free
Amount of items: 2
Items: 
Size: 577601 Color: 12
Size: 421280 Color: 16

Bin 110: 1147 of cap free
Amount of items: 2
Items: 
Size: 532961 Color: 1
Size: 465893 Color: 6

Bin 111: 1158 of cap free
Amount of items: 2
Items: 
Size: 714939 Color: 12
Size: 283904 Color: 0

Bin 112: 1162 of cap free
Amount of items: 2
Items: 
Size: 691291 Color: 4
Size: 307548 Color: 15

Bin 113: 1218 of cap free
Amount of items: 2
Items: 
Size: 536147 Color: 9
Size: 462636 Color: 15

Bin 114: 1222 of cap free
Amount of items: 2
Items: 
Size: 587803 Color: 2
Size: 410976 Color: 8

Bin 115: 1237 of cap free
Amount of items: 2
Items: 
Size: 653331 Color: 4
Size: 345433 Color: 2

Bin 116: 1270 of cap free
Amount of items: 2
Items: 
Size: 538196 Color: 16
Size: 460535 Color: 0

Bin 117: 1294 of cap free
Amount of items: 2
Items: 
Size: 715767 Color: 4
Size: 282940 Color: 1

Bin 118: 1302 of cap free
Amount of items: 2
Items: 
Size: 580103 Color: 12
Size: 418596 Color: 18

Bin 119: 1337 of cap free
Amount of items: 2
Items: 
Size: 527999 Color: 19
Size: 470665 Color: 13

Bin 120: 1349 of cap free
Amount of items: 2
Items: 
Size: 790849 Color: 4
Size: 207803 Color: 13

Bin 121: 1398 of cap free
Amount of items: 2
Items: 
Size: 798534 Color: 5
Size: 200069 Color: 8

Bin 122: 1401 of cap free
Amount of items: 3
Items: 
Size: 674239 Color: 6
Size: 165978 Color: 15
Size: 158383 Color: 3

Bin 123: 1404 of cap free
Amount of items: 2
Items: 
Size: 518590 Color: 12
Size: 480007 Color: 10

Bin 124: 1430 of cap free
Amount of items: 2
Items: 
Size: 562249 Color: 4
Size: 436322 Color: 11

Bin 125: 1430 of cap free
Amount of items: 2
Items: 
Size: 579502 Color: 6
Size: 419069 Color: 15

Bin 126: 1474 of cap free
Amount of items: 2
Items: 
Size: 620172 Color: 0
Size: 378355 Color: 17

Bin 127: 1612 of cap free
Amount of items: 2
Items: 
Size: 571711 Color: 13
Size: 426678 Color: 3

Bin 128: 1614 of cap free
Amount of items: 2
Items: 
Size: 597883 Color: 16
Size: 400504 Color: 12

Bin 129: 1629 of cap free
Amount of items: 2
Items: 
Size: 632339 Color: 17
Size: 366033 Color: 19

Bin 130: 1795 of cap free
Amount of items: 2
Items: 
Size: 645510 Color: 1
Size: 352696 Color: 14

Bin 131: 1817 of cap free
Amount of items: 2
Items: 
Size: 774420 Color: 7
Size: 223764 Color: 1

Bin 132: 1838 of cap free
Amount of items: 2
Items: 
Size: 697612 Color: 15
Size: 300551 Color: 9

Bin 133: 1849 of cap free
Amount of items: 2
Items: 
Size: 785491 Color: 15
Size: 212661 Color: 1

Bin 134: 1861 of cap free
Amount of items: 2
Items: 
Size: 740483 Color: 7
Size: 257657 Color: 15

Bin 135: 1894 of cap free
Amount of items: 2
Items: 
Size: 747798 Color: 17
Size: 250309 Color: 3

Bin 136: 1908 of cap free
Amount of items: 2
Items: 
Size: 506563 Color: 5
Size: 491530 Color: 17

Bin 137: 1910 of cap free
Amount of items: 2
Items: 
Size: 621662 Color: 14
Size: 376429 Color: 1

Bin 138: 1995 of cap free
Amount of items: 2
Items: 
Size: 595252 Color: 14
Size: 402754 Color: 8

Bin 139: 2040 of cap free
Amount of items: 2
Items: 
Size: 686381 Color: 6
Size: 311580 Color: 3

Bin 140: 2083 of cap free
Amount of items: 2
Items: 
Size: 688570 Color: 1
Size: 309348 Color: 14

Bin 141: 2086 of cap free
Amount of items: 2
Items: 
Size: 617691 Color: 19
Size: 380224 Color: 3

Bin 142: 2091 of cap free
Amount of items: 2
Items: 
Size: 643249 Color: 0
Size: 354661 Color: 16

Bin 143: 2103 of cap free
Amount of items: 2
Items: 
Size: 734820 Color: 16
Size: 263078 Color: 11

Bin 144: 2232 of cap free
Amount of items: 2
Items: 
Size: 561454 Color: 4
Size: 436315 Color: 6

Bin 145: 2242 of cap free
Amount of items: 2
Items: 
Size: 613041 Color: 18
Size: 384718 Color: 16

Bin 146: 2277 of cap free
Amount of items: 2
Items: 
Size: 547594 Color: 0
Size: 450130 Color: 5

Bin 147: 2343 of cap free
Amount of items: 2
Items: 
Size: 790560 Color: 10
Size: 207098 Color: 9

Bin 148: 2397 of cap free
Amount of items: 2
Items: 
Size: 655687 Color: 0
Size: 341917 Color: 10

Bin 149: 2408 of cap free
Amount of items: 2
Items: 
Size: 647672 Color: 13
Size: 349921 Color: 6

Bin 150: 2467 of cap free
Amount of items: 2
Items: 
Size: 720964 Color: 3
Size: 276570 Color: 17

Bin 151: 2506 of cap free
Amount of items: 2
Items: 
Size: 500762 Color: 14
Size: 496733 Color: 7

Bin 152: 2512 of cap free
Amount of items: 2
Items: 
Size: 513770 Color: 4
Size: 483719 Color: 1

Bin 153: 2774 of cap free
Amount of items: 2
Items: 
Size: 668671 Color: 19
Size: 328556 Color: 17

Bin 154: 2800 of cap free
Amount of items: 2
Items: 
Size: 733588 Color: 13
Size: 263613 Color: 3

Bin 155: 2812 of cap free
Amount of items: 2
Items: 
Size: 629466 Color: 18
Size: 367723 Color: 16

Bin 156: 2834 of cap free
Amount of items: 2
Items: 
Size: 500736 Color: 19
Size: 496431 Color: 4

Bin 157: 2981 of cap free
Amount of items: 2
Items: 
Size: 540549 Color: 17
Size: 456471 Color: 14

Bin 158: 3039 of cap free
Amount of items: 2
Items: 
Size: 696891 Color: 3
Size: 300071 Color: 15

Bin 159: 3167 of cap free
Amount of items: 2
Items: 
Size: 720890 Color: 5
Size: 275944 Color: 13

Bin 160: 3206 of cap free
Amount of items: 2
Items: 
Size: 605550 Color: 19
Size: 391245 Color: 1

Bin 161: 3411 of cap free
Amount of items: 2
Items: 
Size: 784567 Color: 16
Size: 212023 Color: 15

Bin 162: 3485 of cap free
Amount of items: 2
Items: 
Size: 624156 Color: 0
Size: 372360 Color: 4

Bin 163: 3795 of cap free
Amount of items: 2
Items: 
Size: 795016 Color: 13
Size: 201190 Color: 15

Bin 164: 3816 of cap free
Amount of items: 2
Items: 
Size: 720855 Color: 17
Size: 275330 Color: 18

Bin 165: 3894 of cap free
Amount of items: 2
Items: 
Size: 570328 Color: 9
Size: 425779 Color: 17

Bin 166: 4150 of cap free
Amount of items: 2
Items: 
Size: 752215 Color: 6
Size: 243636 Color: 17

Bin 167: 4404 of cap free
Amount of items: 2
Items: 
Size: 745942 Color: 13
Size: 249655 Color: 11

Bin 168: 4621 of cap free
Amount of items: 2
Items: 
Size: 578296 Color: 16
Size: 417084 Color: 12

Bin 169: 4693 of cap free
Amount of items: 2
Items: 
Size: 577582 Color: 14
Size: 417726 Color: 10

Bin 170: 4788 of cap free
Amount of items: 2
Items: 
Size: 596701 Color: 15
Size: 398512 Color: 14

Bin 171: 4981 of cap free
Amount of items: 2
Items: 
Size: 605165 Color: 17
Size: 389855 Color: 0

Bin 172: 5067 of cap free
Amount of items: 2
Items: 
Size: 665997 Color: 12
Size: 328937 Color: 5

Bin 173: 5101 of cap free
Amount of items: 2
Items: 
Size: 501438 Color: 9
Size: 493462 Color: 12

Bin 174: 5506 of cap free
Amount of items: 2
Items: 
Size: 774053 Color: 14
Size: 220442 Color: 8

Bin 175: 5843 of cap free
Amount of items: 2
Items: 
Size: 529282 Color: 16
Size: 464876 Color: 10

Bin 176: 5925 of cap free
Amount of items: 2
Items: 
Size: 540781 Color: 15
Size: 453295 Color: 0

Bin 177: 5968 of cap free
Amount of items: 2
Items: 
Size: 759477 Color: 14
Size: 234556 Color: 1

Bin 178: 6374 of cap free
Amount of items: 2
Items: 
Size: 782646 Color: 3
Size: 210981 Color: 11

Bin 179: 6386 of cap free
Amount of items: 2
Items: 
Size: 617107 Color: 4
Size: 376508 Color: 17

Bin 180: 6818 of cap free
Amount of items: 2
Items: 
Size: 731795 Color: 10
Size: 261388 Color: 13

Bin 181: 7361 of cap free
Amount of items: 2
Items: 
Size: 500680 Color: 6
Size: 491960 Color: 17

Bin 182: 7446 of cap free
Amount of items: 2
Items: 
Size: 731637 Color: 14
Size: 260918 Color: 11

Bin 183: 7521 of cap free
Amount of items: 2
Items: 
Size: 759222 Color: 3
Size: 233258 Color: 10

Bin 184: 7689 of cap free
Amount of items: 2
Items: 
Size: 594439 Color: 1
Size: 397873 Color: 13

Bin 185: 8468 of cap free
Amount of items: 2
Items: 
Size: 643615 Color: 15
Size: 347918 Color: 18

Bin 186: 8526 of cap free
Amount of items: 2
Items: 
Size: 712362 Color: 7
Size: 279113 Color: 5

Bin 187: 8741 of cap free
Amount of items: 2
Items: 
Size: 780505 Color: 0
Size: 210755 Color: 14

Bin 188: 8869 of cap free
Amount of items: 2
Items: 
Size: 710445 Color: 9
Size: 280687 Color: 4

Bin 189: 9489 of cap free
Amount of items: 2
Items: 
Size: 508759 Color: 11
Size: 481753 Color: 14

Bin 190: 9754 of cap free
Amount of items: 2
Items: 
Size: 675732 Color: 1
Size: 314515 Color: 11

Bin 191: 10150 of cap free
Amount of items: 2
Items: 
Size: 643093 Color: 0
Size: 346758 Color: 8

Bin 192: 10207 of cap free
Amount of items: 2
Items: 
Size: 574877 Color: 17
Size: 414917 Color: 6

Bin 193: 10445 of cap free
Amount of items: 2
Items: 
Size: 611217 Color: 9
Size: 378339 Color: 17

Bin 194: 10784 of cap free
Amount of items: 2
Items: 
Size: 525276 Color: 18
Size: 463941 Color: 7

Bin 195: 10973 of cap free
Amount of items: 2
Items: 
Size: 612995 Color: 12
Size: 376033 Color: 8

Bin 196: 11170 of cap free
Amount of items: 2
Items: 
Size: 708827 Color: 11
Size: 280004 Color: 0

Bin 197: 11261 of cap free
Amount of items: 2
Items: 
Size: 652129 Color: 8
Size: 336611 Color: 12

Bin 198: 11729 of cap free
Amount of items: 2
Items: 
Size: 526038 Color: 2
Size: 462234 Color: 10

Bin 199: 11753 of cap free
Amount of items: 2
Items: 
Size: 590788 Color: 17
Size: 397460 Color: 15

Bin 200: 12004 of cap free
Amount of items: 2
Items: 
Size: 571674 Color: 9
Size: 416323 Color: 1

Bin 201: 12351 of cap free
Amount of items: 2
Items: 
Size: 729895 Color: 7
Size: 257755 Color: 15

Bin 202: 12544 of cap free
Amount of items: 2
Items: 
Size: 795437 Color: 2
Size: 192020 Color: 17

Bin 203: 13087 of cap free
Amount of items: 2
Items: 
Size: 637385 Color: 7
Size: 349529 Color: 0

Bin 204: 13668 of cap free
Amount of items: 2
Items: 
Size: 569927 Color: 10
Size: 416406 Color: 18

Bin 205: 13777 of cap free
Amount of items: 2
Items: 
Size: 610779 Color: 0
Size: 375445 Color: 15

Bin 206: 13860 of cap free
Amount of items: 2
Items: 
Size: 798084 Color: 19
Size: 188057 Color: 0

Bin 207: 14992 of cap free
Amount of items: 2
Items: 
Size: 729738 Color: 19
Size: 255271 Color: 6

Bin 208: 15049 of cap free
Amount of items: 2
Items: 
Size: 611801 Color: 15
Size: 373151 Color: 17

Bin 209: 15140 of cap free
Amount of items: 2
Items: 
Size: 711657 Color: 0
Size: 273204 Color: 19

Bin 210: 17109 of cap free
Amount of items: 2
Items: 
Size: 639347 Color: 4
Size: 343545 Color: 3

Bin 211: 17555 of cap free
Amount of items: 2
Items: 
Size: 792966 Color: 9
Size: 189480 Color: 18

Bin 212: 18078 of cap free
Amount of items: 2
Items: 
Size: 793829 Color: 15
Size: 188094 Color: 0

Bin 213: 19301 of cap free
Amount of items: 2
Items: 
Size: 642513 Color: 14
Size: 338187 Color: 10

Bin 214: 20066 of cap free
Amount of items: 2
Items: 
Size: 525225 Color: 17
Size: 454710 Color: 2

Bin 215: 21595 of cap free
Amount of items: 2
Items: 
Size: 525901 Color: 9
Size: 452505 Color: 4

Bin 216: 22690 of cap free
Amount of items: 2
Items: 
Size: 636880 Color: 14
Size: 340431 Color: 12

Bin 217: 23077 of cap free
Amount of items: 2
Items: 
Size: 789395 Color: 16
Size: 187529 Color: 1

Bin 218: 23495 of cap free
Amount of items: 2
Items: 
Size: 732623 Color: 5
Size: 243883 Color: 13

Bin 219: 23497 of cap free
Amount of items: 2
Items: 
Size: 679689 Color: 12
Size: 296815 Color: 19

Bin 220: 24674 of cap free
Amount of items: 2
Items: 
Size: 524679 Color: 5
Size: 450648 Color: 2

Bin 221: 25807 of cap free
Amount of items: 2
Items: 
Size: 789420 Color: 9
Size: 184774 Color: 7

Bin 222: 33531 of cap free
Amount of items: 2
Items: 
Size: 565166 Color: 9
Size: 401304 Color: 19

Bin 223: 38767 of cap free
Amount of items: 2
Items: 
Size: 778398 Color: 13
Size: 182836 Color: 3

Bin 224: 41183 of cap free
Amount of items: 2
Items: 
Size: 772966 Color: 8
Size: 185852 Color: 18

Bin 225: 42496 of cap free
Amount of items: 2
Items: 
Size: 780207 Color: 0
Size: 177298 Color: 4

Bin 226: 45449 of cap free
Amount of items: 2
Items: 
Size: 770521 Color: 7
Size: 184031 Color: 8

Bin 227: 46916 of cap free
Amount of items: 2
Items: 
Size: 770018 Color: 19
Size: 183067 Color: 18

Bin 228: 51091 of cap free
Amount of items: 2
Items: 
Size: 769005 Color: 5
Size: 179905 Color: 3

Bin 229: 61369 of cap free
Amount of items: 2
Items: 
Size: 759079 Color: 6
Size: 179553 Color: 14

Bin 230: 74998 of cap free
Amount of items: 6
Items: 
Size: 272558 Color: 7
Size: 143192 Color: 6
Size: 139069 Color: 10
Size: 126121 Color: 9
Size: 124945 Color: 12
Size: 119118 Color: 12

Bin 231: 86668 of cap free
Amount of items: 3
Items: 
Size: 594488 Color: 14
Size: 161098 Color: 14
Size: 157747 Color: 4

Bin 232: 89727 of cap free
Amount of items: 2
Items: 
Size: 512354 Color: 1
Size: 397920 Color: 10

Bin 233: 91136 of cap free
Amount of items: 3
Items: 
Size: 462811 Color: 14
Size: 300304 Color: 8
Size: 145750 Color: 7

Bin 234: 91376 of cap free
Amount of items: 2
Items: 
Size: 745773 Color: 12
Size: 162852 Color: 16

Total size: 232382718
Total free space: 1617516

