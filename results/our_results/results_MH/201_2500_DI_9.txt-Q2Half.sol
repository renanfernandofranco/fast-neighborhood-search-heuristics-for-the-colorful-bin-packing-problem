Capicity Bin: 2464
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1598 Color: 1
Size: 722 Color: 1
Size: 136 Color: 0
Size: 8 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1696 Color: 1
Size: 576 Color: 1
Size: 96 Color: 0
Size: 88 Color: 1
Size: 8 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1544 Color: 1
Size: 504 Color: 1
Size: 272 Color: 0
Size: 144 Color: 0

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 920 Color: 1
Size: 840 Color: 1
Size: 384 Color: 1
Size: 272 Color: 0
Size: 48 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 1
Size: 372 Color: 1
Size: 72 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 1
Size: 308 Color: 1
Size: 56 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 1
Size: 780 Color: 1
Size: 152 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 1
Size: 458 Color: 1
Size: 88 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 1
Size: 378 Color: 1
Size: 72 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1862 Color: 1
Size: 502 Color: 1
Size: 100 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 1
Size: 342 Color: 1
Size: 68 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 1
Size: 685 Color: 1
Size: 136 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 1
Size: 452 Color: 1
Size: 72 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 1
Size: 758 Color: 1
Size: 148 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2182 Color: 1
Size: 238 Color: 1
Size: 44 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 1
Size: 662 Color: 1
Size: 128 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 1
Size: 604 Color: 1
Size: 120 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2115 Color: 1
Size: 291 Color: 1
Size: 58 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1487 Color: 1
Size: 869 Color: 1
Size: 108 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 1
Size: 484 Color: 1
Size: 96 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 1
Size: 684 Color: 1
Size: 136 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 1
Size: 365 Color: 1
Size: 72 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 1
Size: 473 Color: 1
Size: 94 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 1
Size: 244 Color: 1
Size: 40 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 1
Size: 226 Color: 1
Size: 44 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 1
Size: 252 Color: 1
Size: 48 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 1
Size: 785 Color: 1
Size: 120 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 1
Size: 1026 Color: 1
Size: 204 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 1
Size: 236 Color: 1
Size: 40 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 1
Size: 246 Color: 1
Size: 48 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2045 Color: 1
Size: 351 Color: 1
Size: 68 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2167 Color: 1
Size: 249 Color: 1
Size: 48 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 1
Size: 290 Color: 1
Size: 56 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 1
Size: 540 Color: 1
Size: 104 Color: 0

Bin 35: 0 of cap free
Amount of items: 4
Items: 
Size: 1235 Color: 1
Size: 1093 Color: 1
Size: 72 Color: 0
Size: 64 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 1
Size: 244 Color: 1
Size: 48 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 1
Size: 771 Color: 1
Size: 152 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 1
Size: 558 Color: 1
Size: 108 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 1
Size: 1027 Color: 1
Size: 204 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 1
Size: 789 Color: 1
Size: 18 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 1
Size: 941 Color: 1
Size: 188 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 1
Size: 734 Color: 1
Size: 12 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 1
Size: 404 Color: 1
Size: 72 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 1
Size: 1028 Color: 1
Size: 200 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 1
Size: 513 Color: 1
Size: 102 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 1
Size: 1022 Color: 1
Size: 200 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 1
Size: 846 Color: 1
Size: 168 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 1
Size: 561 Color: 1
Size: 110 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 1
Size: 882 Color: 1
Size: 172 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 1
Size: 874 Color: 1
Size: 172 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 1
Size: 892 Color: 1
Size: 176 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 1
Size: 887 Color: 1
Size: 176 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 1
Size: 882 Color: 1
Size: 176 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 1
Size: 630 Color: 1
Size: 124 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 1
Size: 617 Color: 1
Size: 122 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1937 Color: 1
Size: 441 Color: 1
Size: 86 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1969 Color: 1
Size: 413 Color: 1
Size: 82 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 1
Size: 414 Color: 1
Size: 80 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 1
Size: 387 Color: 1
Size: 76 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2063 Color: 1
Size: 335 Color: 1
Size: 66 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 1
Size: 332 Color: 1
Size: 64 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 1
Size: 314 Color: 1
Size: 60 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2107 Color: 1
Size: 299 Color: 1
Size: 58 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2123 Color: 1
Size: 315 Color: 1
Size: 26 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2151 Color: 1
Size: 261 Color: 1
Size: 52 Color: 0

Total size: 160160
Total free space: 0

