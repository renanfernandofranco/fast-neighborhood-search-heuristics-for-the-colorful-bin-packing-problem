Capicity Bin: 7904
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4479 Color: 1
Size: 3181 Color: 1
Size: 244 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6837 Color: 1
Size: 859 Color: 1
Size: 208 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 1
Size: 1460 Color: 1
Size: 288 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4498 Color: 1
Size: 2084 Color: 0
Size: 1322 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 1
Size: 404 Color: 0
Size: 392 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 1
Size: 828 Color: 1
Size: 256 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5842 Color: 1
Size: 1802 Color: 1
Size: 260 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6166 Color: 1
Size: 1450 Color: 1
Size: 288 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5922 Color: 1
Size: 1654 Color: 1
Size: 328 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 1
Size: 778 Color: 1
Size: 140 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 6094 Color: 1
Size: 1362 Color: 1
Size: 272 Color: 0
Size: 176 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 1
Size: 908 Color: 1
Size: 424 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6948 Color: 1
Size: 804 Color: 1
Size: 152 Color: 0

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 4948 Color: 1
Size: 2148 Color: 1
Size: 656 Color: 0
Size: 152 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5352 Color: 1
Size: 1896 Color: 1
Size: 656 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5889 Color: 1
Size: 1845 Color: 1
Size: 170 Color: 0

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 6490 Color: 1
Size: 1074 Color: 1
Size: 300 Color: 0
Size: 40 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 1
Size: 2484 Color: 1
Size: 88 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4500 Color: 1
Size: 2844 Color: 1
Size: 560 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6939 Color: 1
Size: 805 Color: 1
Size: 160 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6834 Color: 1
Size: 894 Color: 1
Size: 176 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5011 Color: 1
Size: 2411 Color: 1
Size: 482 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6984 Color: 1
Size: 776 Color: 1
Size: 144 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 1
Size: 1076 Color: 1
Size: 328 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 1
Size: 1316 Color: 1
Size: 196 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 3960 Color: 1
Size: 3288 Color: 1
Size: 656 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 1
Size: 762 Color: 1
Size: 244 Color: 0

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 5606 Color: 1
Size: 1722 Color: 1
Size: 416 Color: 0
Size: 160 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5964 Color: 1
Size: 1452 Color: 1
Size: 488 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5043 Color: 1
Size: 2385 Color: 1
Size: 476 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 1
Size: 1028 Color: 1
Size: 256 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 1
Size: 994 Color: 1
Size: 192 Color: 0

Bin 33: 0 of cap free
Amount of items: 4
Items: 
Size: 6726 Color: 1
Size: 714 Color: 1
Size: 288 Color: 0
Size: 176 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4958 Color: 1
Size: 2458 Color: 1
Size: 488 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 1
Size: 792 Color: 1
Size: 144 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6712 Color: 1
Size: 852 Color: 1
Size: 340 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 1
Size: 2792 Color: 1
Size: 192 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4591 Color: 1
Size: 2761 Color: 1
Size: 552 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5444 Color: 1
Size: 2444 Color: 1
Size: 16 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6332 Color: 1
Size: 1004 Color: 1
Size: 568 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 1
Size: 964 Color: 1
Size: 168 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 1
Size: 990 Color: 1
Size: 560 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6884 Color: 1
Size: 780 Color: 1
Size: 240 Color: 0

Bin 44: 0 of cap free
Amount of items: 4
Items: 
Size: 5472 Color: 1
Size: 2132 Color: 1
Size: 152 Color: 0
Size: 148 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 4769 Color: 1
Size: 2655 Color: 1
Size: 480 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6906 Color: 1
Size: 806 Color: 1
Size: 192 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6776 Color: 1
Size: 1000 Color: 1
Size: 128 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6790 Color: 1
Size: 914 Color: 1
Size: 200 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5407 Color: 1
Size: 2081 Color: 1
Size: 416 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 4490 Color: 1
Size: 3082 Color: 1
Size: 332 Color: 0

Bin 51: 0 of cap free
Amount of items: 5
Items: 
Size: 4932 Color: 1
Size: 2136 Color: 1
Size: 432 Color: 1
Size: 208 Color: 0
Size: 196 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6525 Color: 1
Size: 891 Color: 1
Size: 488 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6233 Color: 1
Size: 1393 Color: 1
Size: 278 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4942 Color: 1
Size: 2494 Color: 1
Size: 468 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6088 Color: 1
Size: 1632 Color: 1
Size: 184 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 1
Size: 1204 Color: 1
Size: 384 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 1
Size: 1202 Color: 1
Size: 240 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 1
Size: 3284 Color: 1
Size: 136 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6886 Color: 1
Size: 842 Color: 1
Size: 176 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 3953 Color: 1
Size: 3293 Color: 1
Size: 658 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 1
Size: 1294 Color: 1
Size: 288 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5380 Color: 1
Size: 2044 Color: 1
Size: 480 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 3954 Color: 1
Size: 3294 Color: 1
Size: 656 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6653 Color: 1
Size: 1043 Color: 1
Size: 208 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6164 Color: 1
Size: 1528 Color: 1
Size: 212 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6691 Color: 1
Size: 985 Color: 1
Size: 228 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6676 Color: 1
Size: 1026 Color: 1
Size: 202 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6913 Color: 1
Size: 767 Color: 1
Size: 224 Color: 0

Bin 69: 0 of cap free
Amount of items: 5
Items: 
Size: 3964 Color: 1
Size: 2052 Color: 1
Size: 1180 Color: 1
Size: 544 Color: 0
Size: 164 Color: 0

Bin 70: 0 of cap free
Amount of items: 5
Items: 
Size: 5348 Color: 1
Size: 1151 Color: 1
Size: 937 Color: 1
Size: 288 Color: 0
Size: 180 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6431 Color: 1
Size: 909 Color: 1
Size: 564 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 5462 Color: 1
Size: 2182 Color: 1
Size: 260 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6938 Color: 1
Size: 742 Color: 1
Size: 224 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 3957 Color: 1
Size: 3471 Color: 1
Size: 476 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6468 Color: 1
Size: 1216 Color: 1
Size: 220 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6616 Color: 1
Size: 1272 Color: 1
Size: 16 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5972 Color: 1
Size: 1612 Color: 1
Size: 320 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6987 Color: 1
Size: 765 Color: 1
Size: 152 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 5290 Color: 1
Size: 1734 Color: 1
Size: 880 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 3956 Color: 1
Size: 3292 Color: 1
Size: 656 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5748 Color: 1
Size: 1970 Color: 1
Size: 186 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 1
Size: 1098 Color: 1
Size: 164 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6880 Color: 1
Size: 880 Color: 1
Size: 144 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 1
Size: 948 Color: 1
Size: 208 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5640 Color: 1
Size: 1384 Color: 1
Size: 880 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 4301 Color: 1
Size: 3003 Color: 1
Size: 600 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 1
Size: 1080 Color: 1
Size: 320 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 1
Size: 1176 Color: 1
Size: 268 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6341 Color: 1
Size: 1229 Color: 1
Size: 334 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5412 Color: 1
Size: 2132 Color: 1
Size: 360 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 4488 Color: 1
Size: 3200 Color: 1
Size: 216 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5031 Color: 1
Size: 2395 Color: 1
Size: 478 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 1
Size: 834 Color: 1
Size: 480 Color: 0

Bin 94: 0 of cap free
Amount of items: 5
Items: 
Size: 3958 Color: 1
Size: 1709 Color: 1
Size: 1501 Color: 1
Size: 496 Color: 0
Size: 240 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6720 Color: 1
Size: 1008 Color: 1
Size: 176 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 1
Size: 1054 Color: 1
Size: 142 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 6248 Color: 1
Size: 1303 Color: 1
Size: 352 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 1
Size: 827 Color: 1
Size: 160 Color: 0

Bin 99: 1 of cap free
Amount of items: 5
Items: 
Size: 3290 Color: 1
Size: 2099 Color: 1
Size: 1688 Color: 1
Size: 418 Color: 0
Size: 408 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 6583 Color: 1
Size: 952 Color: 1
Size: 368 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 1
Size: 1101 Color: 1
Size: 184 Color: 0

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 5691 Color: 1
Size: 1804 Color: 1
Size: 408 Color: 0

Bin 103: 1 of cap free
Amount of items: 3
Items: 
Size: 6062 Color: 1
Size: 1681 Color: 1
Size: 160 Color: 0

Bin 104: 1 of cap free
Amount of items: 3
Items: 
Size: 5673 Color: 1
Size: 2038 Color: 1
Size: 192 Color: 0

Bin 105: 2 of cap free
Amount of items: 3
Items: 
Size: 5334 Color: 1
Size: 2424 Color: 1
Size: 144 Color: 0

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 5308 Color: 1
Size: 2430 Color: 1
Size: 164 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 4976 Color: 1
Size: 2842 Color: 1
Size: 84 Color: 0

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 6723 Color: 1
Size: 1011 Color: 1
Size: 168 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 4618 Color: 1
Size: 2852 Color: 1
Size: 432 Color: 0

Bin 110: 2 of cap free
Amount of items: 5
Items: 
Size: 2656 Color: 1
Size: 2484 Color: 1
Size: 1324 Color: 0
Size: 1182 Color: 1
Size: 256 Color: 0

Bin 111: 2 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 1
Size: 1204 Color: 1
Size: 424 Color: 0

Bin 112: 2 of cap free
Amount of items: 3
Items: 
Size: 5000 Color: 1
Size: 2742 Color: 1
Size: 160 Color: 0

Bin 113: 3 of cap free
Amount of items: 3
Items: 
Size: 6105 Color: 1
Size: 1620 Color: 1
Size: 176 Color: 0

Bin 114: 3 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 1
Size: 2193 Color: 1
Size: 232 Color: 0

Bin 115: 3 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 1
Size: 684 Color: 1
Size: 232 Color: 0

Bin 116: 4 of cap free
Amount of items: 3
Items: 
Size: 4980 Color: 1
Size: 2856 Color: 1
Size: 64 Color: 0

Bin 117: 4 of cap free
Amount of items: 3
Items: 
Size: 5732 Color: 1
Size: 1812 Color: 1
Size: 356 Color: 0

Bin 118: 5 of cap free
Amount of items: 3
Items: 
Size: 6875 Color: 1
Size: 808 Color: 1
Size: 216 Color: 0

Bin 119: 5 of cap free
Amount of items: 3
Items: 
Size: 6053 Color: 1
Size: 1510 Color: 1
Size: 336 Color: 0

Bin 120: 9 of cap free
Amount of items: 3
Items: 
Size: 6781 Color: 1
Size: 850 Color: 1
Size: 264 Color: 0

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 7086 Color: 1
Size: 668 Color: 1
Size: 140 Color: 0

Bin 122: 11 of cap free
Amount of items: 3
Items: 
Size: 5387 Color: 1
Size: 2468 Color: 1
Size: 38 Color: 0

Bin 123: 26 of cap free
Amount of items: 3
Items: 
Size: 6936 Color: 1
Size: 834 Color: 1
Size: 108 Color: 0

Bin 124: 36 of cap free
Amount of items: 3
Items: 
Size: 5746 Color: 1
Size: 1824 Color: 1
Size: 298 Color: 0

Bin 125: 448 of cap free
Amount of items: 3
Items: 
Size: 4924 Color: 1
Size: 2164 Color: 1
Size: 368 Color: 0

Bin 126: 820 of cap free
Amount of items: 1
Items: 
Size: 7084 Color: 1

Bin 127: 820 of cap free
Amount of items: 3
Items: 
Size: 5880 Color: 1
Size: 1116 Color: 1
Size: 88 Color: 0

Bin 128: 854 of cap free
Amount of items: 1
Items: 
Size: 7050 Color: 1

Bin 129: 886 of cap free
Amount of items: 1
Items: 
Size: 7018 Color: 1

Bin 130: 910 of cap free
Amount of items: 1
Items: 
Size: 6994 Color: 1

Bin 131: 932 of cap free
Amount of items: 1
Items: 
Size: 6972 Color: 1

Bin 132: 997 of cap free
Amount of items: 1
Items: 
Size: 6907 Color: 1

Bin 133: 1094 of cap free
Amount of items: 1
Items: 
Size: 6810 Color: 1

Total size: 1043328
Total free space: 7904

