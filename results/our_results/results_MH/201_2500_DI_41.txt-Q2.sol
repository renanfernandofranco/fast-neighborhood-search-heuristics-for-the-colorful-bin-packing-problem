Capicity Bin: 2052
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1031 Color: 1
Size: 897 Color: 0
Size: 124 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1138 Color: 0
Size: 854 Color: 1
Size: 60 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 1
Size: 593 Color: 0
Size: 84 Color: 1

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1485 Color: 0
Size: 292 Color: 1
Size: 251 Color: 1
Size: 24 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 0
Size: 389 Color: 1
Size: 72 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 0
Size: 385 Color: 1
Size: 42 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 1
Size: 382 Color: 0
Size: 32 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 1
Size: 313 Color: 0
Size: 62 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 0
Size: 174 Color: 0
Size: 152 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 1
Size: 271 Color: 1
Size: 44 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 1
Size: 239 Color: 1
Size: 68 Color: 0

Bin 12: 0 of cap free
Amount of items: 4
Items: 
Size: 1842 Color: 0
Size: 170 Color: 1
Size: 36 Color: 1
Size: 4 Color: 0

Bin 13: 1 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 1
Size: 751 Color: 0
Size: 70 Color: 1

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 1275 Color: 0
Size: 692 Color: 0
Size: 84 Color: 1

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1420 Color: 0
Size: 579 Color: 0
Size: 52 Color: 1

Bin 16: 1 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 1
Size: 481 Color: 0

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 0
Size: 402 Color: 1
Size: 32 Color: 1

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 0
Size: 293 Color: 1
Size: 80 Color: 0

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1709 Color: 0
Size: 342 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 1
Size: 225 Color: 1
Size: 92 Color: 0

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 0
Size: 257 Color: 1

Bin 22: 2 of cap free
Amount of items: 22
Items: 
Size: 148 Color: 1
Size: 136 Color: 1
Size: 132 Color: 1
Size: 130 Color: 1
Size: 128 Color: 1
Size: 114 Color: 1
Size: 114 Color: 0
Size: 112 Color: 0
Size: 104 Color: 1
Size: 96 Color: 0
Size: 94 Color: 1
Size: 86 Color: 1
Size: 84 Color: 0
Size: 78 Color: 1
Size: 76 Color: 0
Size: 72 Color: 1
Size: 72 Color: 0
Size: 64 Color: 0
Size: 58 Color: 0
Size: 52 Color: 0
Size: 52 Color: 0
Size: 48 Color: 0

Bin 23: 2 of cap free
Amount of items: 3
Items: 
Size: 1111 Color: 1
Size: 845 Color: 0
Size: 94 Color: 1

Bin 24: 2 of cap free
Amount of items: 2
Items: 
Size: 1288 Color: 0
Size: 762 Color: 1

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1359 Color: 1
Size: 472 Color: 0
Size: 219 Color: 1

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 0
Size: 583 Color: 0
Size: 52 Color: 1

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1477 Color: 0
Size: 473 Color: 0
Size: 100 Color: 1

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1583 Color: 1
Size: 467 Color: 0

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1704 Color: 0
Size: 346 Color: 1

Bin 30: 3 of cap free
Amount of items: 6
Items: 
Size: 1027 Color: 0
Size: 422 Color: 0
Size: 228 Color: 1
Size: 168 Color: 1
Size: 148 Color: 0
Size: 56 Color: 1

Bin 31: 3 of cap free
Amount of items: 5
Items: 
Size: 1030 Color: 0
Size: 425 Color: 0
Size: 287 Color: 1
Size: 231 Color: 1
Size: 76 Color: 0

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1153 Color: 0
Size: 828 Color: 0
Size: 68 Color: 1

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1257 Color: 1
Size: 686 Color: 0
Size: 106 Color: 1

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1478 Color: 1
Size: 571 Color: 0

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1535 Color: 0
Size: 514 Color: 1

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1618 Color: 1
Size: 431 Color: 0

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1767 Color: 0
Size: 282 Color: 1

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1775 Color: 0
Size: 274 Color: 1

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1783 Color: 0
Size: 266 Color: 1

Bin 40: 3 of cap free
Amount of items: 4
Items: 
Size: 1791 Color: 1
Size: 214 Color: 0
Size: 40 Color: 1
Size: 4 Color: 0

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 1
Size: 622 Color: 0
Size: 56 Color: 1

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 0
Size: 570 Color: 0
Size: 40 Color: 1

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1546 Color: 0
Size: 502 Color: 1

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1657 Color: 1
Size: 391 Color: 0

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 1
Size: 314 Color: 0
Size: 16 Color: 0

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 1306 Color: 0
Size: 741 Color: 1

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 1829 Color: 1
Size: 218 Color: 0

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 0
Size: 316 Color: 1

Bin 49: 8 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 1
Size: 178 Color: 0
Size: 124 Color: 0

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 1686 Color: 1
Size: 357 Color: 0

Bin 51: 13 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 1
Size: 262 Color: 0
Size: 24 Color: 0

Bin 52: 17 of cap free
Amount of items: 2
Items: 
Size: 1772 Color: 1
Size: 263 Color: 0

Bin 53: 19 of cap free
Amount of items: 2
Items: 
Size: 1846 Color: 0
Size: 187 Color: 1

Bin 54: 20 of cap free
Amount of items: 3
Items: 
Size: 1267 Color: 0
Size: 649 Color: 1
Size: 116 Color: 0

Bin 55: 20 of cap free
Amount of items: 2
Items: 
Size: 1701 Color: 1
Size: 331 Color: 0

Bin 56: 21 of cap free
Amount of items: 2
Items: 
Size: 1506 Color: 0
Size: 525 Color: 1

Bin 57: 22 of cap free
Amount of items: 2
Items: 
Size: 1367 Color: 0
Size: 663 Color: 1

Bin 58: 24 of cap free
Amount of items: 2
Items: 
Size: 1799 Color: 1
Size: 229 Color: 0

Bin 59: 26 of cap free
Amount of items: 2
Items: 
Size: 1598 Color: 1
Size: 428 Color: 0

Bin 60: 26 of cap free
Amount of items: 2
Items: 
Size: 1798 Color: 1
Size: 228 Color: 0

Bin 61: 27 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 0
Size: 362 Color: 1
Size: 240 Color: 1

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1493 Color: 1
Size: 531 Color: 0

Bin 63: 31 of cap free
Amount of items: 2
Items: 
Size: 1543 Color: 0
Size: 478 Color: 1

Bin 64: 34 of cap free
Amount of items: 2
Items: 
Size: 1163 Color: 1
Size: 855 Color: 0

Bin 65: 44 of cap free
Amount of items: 2
Items: 
Size: 1353 Color: 0
Size: 655 Color: 1

Bin 66: 1568 of cap free
Amount of items: 10
Items: 
Size: 68 Color: 1
Size: 52 Color: 0
Size: 50 Color: 1
Size: 48 Color: 1
Size: 48 Color: 0
Size: 46 Color: 1
Size: 46 Color: 1
Size: 46 Color: 0
Size: 40 Color: 0
Size: 40 Color: 0

Total size: 133380
Total free space: 2052

