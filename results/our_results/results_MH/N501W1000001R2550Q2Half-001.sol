Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 411001 Color: 1
Size: 314150 Color: 1
Size: 274850 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 419698 Color: 1
Size: 307448 Color: 1
Size: 272855 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 379239 Color: 1
Size: 359929 Color: 1
Size: 260833 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 430221 Color: 1
Size: 303428 Color: 1
Size: 266352 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 338256 Color: 1
Size: 333808 Color: 1
Size: 327937 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 411823 Color: 1
Size: 301419 Color: 1
Size: 286759 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 417696 Color: 1
Size: 319772 Color: 1
Size: 262533 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 449256 Color: 1
Size: 279504 Color: 1
Size: 271241 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 371285 Color: 1
Size: 362050 Color: 1
Size: 266666 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 382885 Color: 1
Size: 334080 Color: 1
Size: 283036 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 379805 Color: 1
Size: 365660 Color: 1
Size: 254536 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 468234 Color: 1
Size: 278618 Color: 1
Size: 253149 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 489287 Color: 1
Size: 259252 Color: 1
Size: 251462 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 417726 Color: 1
Size: 307814 Color: 1
Size: 274461 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 415201 Color: 1
Size: 318243 Color: 1
Size: 266557 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 436992 Color: 1
Size: 296371 Color: 1
Size: 266638 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 361927 Color: 1
Size: 334616 Color: 1
Size: 303458 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 372866 Color: 1
Size: 318611 Color: 1
Size: 308524 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 453657 Color: 1
Size: 292538 Color: 1
Size: 253806 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 462974 Color: 1
Size: 283524 Color: 1
Size: 253503 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 382976 Color: 1
Size: 363829 Color: 1
Size: 253196 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 477605 Color: 1
Size: 265115 Color: 1
Size: 257281 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 411234 Color: 1
Size: 325981 Color: 1
Size: 262786 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 382123 Color: 1
Size: 316797 Color: 1
Size: 301081 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 365679 Color: 1
Size: 347213 Color: 1
Size: 287109 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 457645 Color: 1
Size: 274721 Color: 1
Size: 267635 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 453000 Color: 1
Size: 295990 Color: 1
Size: 251011 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 432668 Color: 1
Size: 303987 Color: 1
Size: 263346 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 499375 Color: 1
Size: 250068 Color: 0
Size: 250558 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 421266 Color: 1
Size: 292064 Color: 1
Size: 286671 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 424682 Color: 1
Size: 320078 Color: 1
Size: 255241 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 413593 Color: 1
Size: 330647 Color: 1
Size: 255761 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 496749 Color: 1
Size: 251691 Color: 1
Size: 251561 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 380263 Color: 1
Size: 318778 Color: 1
Size: 300960 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 315980 Color: 1
Size: 309120 Color: 0
Size: 374901 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 437984 Color: 1
Size: 305169 Color: 1
Size: 256848 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 393062 Color: 1
Size: 337735 Color: 1
Size: 269204 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 408362 Color: 1
Size: 298218 Color: 1
Size: 293421 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 386431 Color: 1
Size: 317831 Color: 1
Size: 295739 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 436671 Color: 1
Size: 287011 Color: 1
Size: 276319 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 401080 Color: 1
Size: 314236 Color: 1
Size: 284685 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 398938 Color: 1
Size: 315138 Color: 1
Size: 285925 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 372251 Color: 1
Size: 350820 Color: 1
Size: 276930 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 348805 Color: 1
Size: 329220 Color: 1
Size: 321976 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 446291 Color: 1
Size: 283506 Color: 1
Size: 270204 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 431076 Color: 1
Size: 299735 Color: 1
Size: 269190 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 394337 Color: 1
Size: 350772 Color: 1
Size: 254892 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 368204 Color: 1
Size: 352074 Color: 1
Size: 279723 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 489112 Color: 1
Size: 256764 Color: 1
Size: 254125 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 492491 Color: 1
Size: 256979 Color: 1
Size: 250531 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 490300 Color: 1
Size: 256822 Color: 1
Size: 252879 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 364754 Color: 1
Size: 339286 Color: 1
Size: 295961 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 399378 Color: 1
Size: 309562 Color: 1
Size: 291061 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 363434 Color: 1
Size: 342141 Color: 1
Size: 294426 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 455475 Color: 1
Size: 289346 Color: 1
Size: 255180 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 496829 Color: 1
Size: 253019 Color: 1
Size: 250153 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 402605 Color: 1
Size: 311357 Color: 1
Size: 286039 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 482498 Color: 1
Size: 264365 Color: 1
Size: 253138 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 478187 Color: 1
Size: 271541 Color: 1
Size: 250273 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 407175 Color: 1
Size: 309635 Color: 1
Size: 283191 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 415873 Color: 1
Size: 314045 Color: 1
Size: 270083 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 426831 Color: 1
Size: 300039 Color: 1
Size: 273131 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 336322 Color: 1
Size: 333742 Color: 1
Size: 329937 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 371004 Color: 1
Size: 325176 Color: 1
Size: 303821 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 357348 Color: 1
Size: 356712 Color: 1
Size: 285941 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 403044 Color: 1
Size: 328288 Color: 1
Size: 268669 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 480868 Color: 1
Size: 260987 Color: 1
Size: 258146 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 428379 Color: 1
Size: 302156 Color: 1
Size: 269466 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 408993 Color: 1
Size: 324844 Color: 1
Size: 266164 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 471833 Color: 1
Size: 273463 Color: 1
Size: 254705 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 472144 Color: 1
Size: 275933 Color: 1
Size: 251924 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 421555 Color: 1
Size: 298342 Color: 1
Size: 280104 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 367422 Color: 1
Size: 353585 Color: 1
Size: 278994 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 340569 Color: 1
Size: 339512 Color: 1
Size: 319920 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 462803 Color: 1
Size: 286355 Color: 1
Size: 250843 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 426669 Color: 1
Size: 289402 Color: 1
Size: 283930 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 435773 Color: 1
Size: 302224 Color: 1
Size: 262004 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 473760 Color: 1
Size: 272144 Color: 1
Size: 254097 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 480681 Color: 1
Size: 266060 Color: 1
Size: 253260 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 375345 Color: 1
Size: 365811 Color: 1
Size: 258845 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 470078 Color: 1
Size: 270548 Color: 1
Size: 259375 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 349122 Color: 1
Size: 337760 Color: 1
Size: 313119 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 384185 Color: 1
Size: 358012 Color: 1
Size: 257804 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 491739 Color: 1
Size: 254912 Color: 1
Size: 253350 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 377223 Color: 1
Size: 356201 Color: 1
Size: 266577 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 438471 Color: 1
Size: 303473 Color: 1
Size: 258057 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 456617 Color: 1
Size: 292204 Color: 1
Size: 251180 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 459553 Color: 1
Size: 277774 Color: 1
Size: 262674 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 404799 Color: 1
Size: 321128 Color: 1
Size: 274074 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 371996 Color: 1
Size: 327337 Color: 1
Size: 300668 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 347913 Color: 1
Size: 332511 Color: 1
Size: 319577 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 403393 Color: 1
Size: 321963 Color: 1
Size: 274645 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 384714 Color: 1
Size: 340409 Color: 1
Size: 274878 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 365871 Color: 1
Size: 331171 Color: 1
Size: 302959 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 454340 Color: 1
Size: 286003 Color: 1
Size: 259658 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 393624 Color: 1
Size: 340912 Color: 1
Size: 265465 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 359066 Color: 1
Size: 357264 Color: 1
Size: 283671 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 386633 Color: 1
Size: 322709 Color: 1
Size: 290659 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 472262 Color: 1
Size: 266781 Color: 1
Size: 260958 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 412155 Color: 1
Size: 335583 Color: 1
Size: 252263 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 449177 Color: 1
Size: 281111 Color: 1
Size: 269713 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 447735 Color: 1
Size: 276511 Color: 1
Size: 275755 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 498671 Color: 1
Size: 251145 Color: 1
Size: 250185 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 432449 Color: 1
Size: 315934 Color: 1
Size: 251618 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 371014 Color: 1
Size: 368867 Color: 1
Size: 260120 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 374164 Color: 1
Size: 330361 Color: 1
Size: 295476 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 417862 Color: 1
Size: 298359 Color: 1
Size: 283780 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 369409 Color: 1
Size: 328506 Color: 1
Size: 302086 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 464049 Color: 1
Size: 283336 Color: 1
Size: 252616 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 465327 Color: 1
Size: 280839 Color: 1
Size: 253835 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 351669 Color: 1
Size: 326487 Color: 1
Size: 321845 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 416835 Color: 1
Size: 314818 Color: 1
Size: 268348 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 432662 Color: 1
Size: 293545 Color: 1
Size: 273794 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 467263 Color: 1
Size: 281100 Color: 1
Size: 251638 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 396431 Color: 1
Size: 334102 Color: 1
Size: 269468 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 408489 Color: 1
Size: 318412 Color: 1
Size: 273100 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 406332 Color: 1
Size: 331324 Color: 1
Size: 262345 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 423048 Color: 1
Size: 312702 Color: 1
Size: 264251 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 492424 Color: 1
Size: 257131 Color: 1
Size: 250446 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 413389 Color: 1
Size: 303281 Color: 1
Size: 283331 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 409433 Color: 1
Size: 330799 Color: 1
Size: 259769 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 416246 Color: 1
Size: 328023 Color: 1
Size: 255732 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 466658 Color: 1
Size: 272394 Color: 1
Size: 260949 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 420916 Color: 1
Size: 324340 Color: 1
Size: 254745 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 432570 Color: 1
Size: 314181 Color: 1
Size: 253250 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 422093 Color: 1
Size: 320901 Color: 1
Size: 257007 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 375983 Color: 1
Size: 339153 Color: 1
Size: 284865 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 423924 Color: 1
Size: 313940 Color: 1
Size: 262137 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 445958 Color: 1
Size: 278484 Color: 1
Size: 275559 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 421055 Color: 1
Size: 297245 Color: 1
Size: 281701 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 484331 Color: 1
Size: 263267 Color: 1
Size: 252403 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 445818 Color: 1
Size: 303231 Color: 1
Size: 250952 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 364236 Color: 1
Size: 347563 Color: 1
Size: 288202 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 473602 Color: 1
Size: 272892 Color: 1
Size: 253507 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 471997 Color: 1
Size: 275017 Color: 1
Size: 252987 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 404114 Color: 1
Size: 323501 Color: 1
Size: 272386 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 467458 Color: 1
Size: 269632 Color: 1
Size: 262911 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 431361 Color: 1
Size: 299339 Color: 1
Size: 269301 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 444546 Color: 1
Size: 283758 Color: 1
Size: 271697 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 493251 Color: 1
Size: 256122 Color: 1
Size: 250628 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 355895 Color: 1
Size: 347154 Color: 1
Size: 296952 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 372857 Color: 1
Size: 339319 Color: 1
Size: 287825 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 388806 Color: 1
Size: 308145 Color: 1
Size: 303050 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 383232 Color: 1
Size: 360221 Color: 1
Size: 256548 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 379533 Color: 1
Size: 328012 Color: 1
Size: 292456 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 469188 Color: 1
Size: 273533 Color: 1
Size: 257280 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 379926 Color: 1
Size: 315933 Color: 1
Size: 304142 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 382777 Color: 1
Size: 348271 Color: 1
Size: 268953 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 386691 Color: 1
Size: 328000 Color: 1
Size: 285310 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 414653 Color: 1
Size: 331529 Color: 1
Size: 253819 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 441437 Color: 1
Size: 292724 Color: 1
Size: 265840 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 354079 Color: 1
Size: 329245 Color: 1
Size: 316677 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 426275 Color: 1
Size: 300504 Color: 1
Size: 273222 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 422803 Color: 1
Size: 299785 Color: 1
Size: 277413 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 351330 Color: 1
Size: 341985 Color: 1
Size: 306686 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 401729 Color: 1
Size: 304454 Color: 1
Size: 293818 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 381312 Color: 1
Size: 321317 Color: 1
Size: 297372 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 390041 Color: 1
Size: 344580 Color: 1
Size: 265380 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 397209 Color: 1
Size: 327663 Color: 1
Size: 275129 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 401234 Color: 1
Size: 307512 Color: 1
Size: 291255 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 405553 Color: 1
Size: 338312 Color: 1
Size: 256136 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 409187 Color: 1
Size: 317440 Color: 1
Size: 273374 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 427299 Color: 1
Size: 314056 Color: 1
Size: 258646 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 427471 Color: 1
Size: 316491 Color: 1
Size: 256039 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 431229 Color: 1
Size: 292913 Color: 1
Size: 275859 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 437824 Color: 1
Size: 287400 Color: 1
Size: 274777 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 468536 Color: 1
Size: 280606 Color: 1
Size: 250859 Color: 0

Total size: 167000167
Total free space: 0

