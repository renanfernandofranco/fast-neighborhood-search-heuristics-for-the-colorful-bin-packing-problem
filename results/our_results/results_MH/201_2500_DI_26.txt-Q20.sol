Capicity Bin: 2068
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 696 Color: 6
Size: 480 Color: 4
Size: 428 Color: 4
Size: 232 Color: 17
Size: 76 Color: 14
Size: 68 Color: 19
Size: 48 Color: 14
Size: 40 Color: 7

Bin 2: 0 of cap free
Amount of items: 6
Items: 
Size: 1428 Color: 8
Size: 320 Color: 11
Size: 232 Color: 17
Size: 64 Color: 10
Size: 16 Color: 10
Size: 8 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 6
Size: 267 Color: 4
Size: 52 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1719 Color: 4
Size: 291 Color: 6
Size: 58 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 3
Size: 383 Color: 1
Size: 76 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1853 Color: 5
Size: 181 Color: 3
Size: 34 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 15
Size: 346 Color: 1
Size: 68 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1453 Color: 7
Size: 513 Color: 3
Size: 102 Color: 19

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 2
Size: 417 Color: 8
Size: 82 Color: 16

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 5
Size: 282 Color: 15
Size: 56 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1053 Color: 13
Size: 847 Color: 1
Size: 168 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 10
Size: 362 Color: 14
Size: 68 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1445 Color: 13
Size: 521 Color: 7
Size: 102 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 3
Size: 217 Color: 12
Size: 42 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 11
Size: 178 Color: 15
Size: 32 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 4
Size: 262 Color: 10
Size: 48 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 8
Size: 247 Color: 18
Size: 48 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 4
Size: 537 Color: 0
Size: 106 Color: 5

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1817 Color: 5
Size: 211 Color: 15
Size: 40 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 15
Size: 261 Color: 14
Size: 50 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 4
Size: 461 Color: 12
Size: 90 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 8
Size: 630 Color: 7
Size: 124 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 7
Size: 351 Color: 15
Size: 68 Color: 14

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 16
Size: 274 Color: 15
Size: 52 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 19
Size: 218 Color: 18
Size: 40 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 18
Size: 273 Color: 15
Size: 54 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 6
Size: 342 Color: 4
Size: 64 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 7
Size: 193 Color: 14
Size: 38 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 18
Size: 386 Color: 10
Size: 76 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 14
Size: 853 Color: 13
Size: 170 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 6
Size: 331 Color: 16
Size: 64 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 3
Size: 221 Color: 6
Size: 44 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 1
Size: 507 Color: 4
Size: 100 Color: 19

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 13
Size: 201 Color: 13
Size: 38 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 17
Size: 402 Color: 1
Size: 76 Color: 14

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 14
Size: 244 Color: 4
Size: 48 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 9
Size: 439 Color: 12
Size: 24 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 3
Size: 282 Color: 10
Size: 52 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 11
Size: 266 Color: 17
Size: 52 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1373 Color: 13
Size: 581 Color: 2
Size: 114 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 9
Size: 889 Color: 1
Size: 142 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 9
Size: 694 Color: 16
Size: 136 Color: 9

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 17
Size: 354 Color: 11
Size: 68 Color: 16

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 2
Size: 421 Color: 11
Size: 84 Color: 8

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 13
Size: 281 Color: 4
Size: 54 Color: 12

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1289 Color: 12
Size: 651 Color: 10
Size: 128 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1177 Color: 19
Size: 841 Color: 13
Size: 50 Color: 14

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 0
Size: 329 Color: 13
Size: 64 Color: 10

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1185 Color: 11
Size: 737 Color: 14
Size: 146 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1035 Color: 8
Size: 861 Color: 11
Size: 172 Color: 10

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1845 Color: 12
Size: 187 Color: 15
Size: 36 Color: 6

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 5
Size: 438 Color: 5
Size: 8 Color: 19

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 16
Size: 261 Color: 15
Size: 42 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 8
Size: 214 Color: 11
Size: 40 Color: 13

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 3
Size: 693 Color: 19
Size: 138 Color: 5

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1038 Color: 12
Size: 862 Color: 3
Size: 168 Color: 7

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1612 Color: 8
Size: 400 Color: 18
Size: 56 Color: 5

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 12
Size: 770 Color: 19
Size: 152 Color: 18

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 7
Size: 434 Color: 13
Size: 84 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1641 Color: 0
Size: 357 Color: 14
Size: 70 Color: 7

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 12
Size: 609 Color: 17
Size: 120 Color: 18

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 2
Size: 486 Color: 18
Size: 96 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 19
Size: 522 Color: 6
Size: 100 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1378 Color: 2
Size: 578 Color: 10
Size: 112 Color: 11

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 1300 Color: 8
Size: 768 Color: 5

Total size: 134420
Total free space: 0

