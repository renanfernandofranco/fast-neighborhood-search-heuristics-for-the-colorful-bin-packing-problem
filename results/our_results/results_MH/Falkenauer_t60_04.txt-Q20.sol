Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 14
Size: 251 Color: 7
Size: 251 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 3
Size: 256 Color: 18
Size: 250 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 5
Size: 281 Color: 14
Size: 264 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 11
Size: 296 Color: 18
Size: 286 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 14
Size: 315 Color: 11
Size: 284 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 19
Size: 314 Color: 17
Size: 283 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 276 Color: 1
Size: 254 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 5
Size: 291 Color: 14
Size: 284 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8
Size: 310 Color: 16
Size: 281 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 13
Size: 357 Color: 8
Size: 264 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 17
Size: 252 Color: 19
Size: 252 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7
Size: 328 Color: 16
Size: 293 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 14
Size: 287 Color: 12
Size: 279 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 3
Size: 336 Color: 17
Size: 253 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 18
Size: 256 Color: 17
Size: 253 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7
Size: 326 Color: 11
Size: 296 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 14
Size: 304 Color: 0
Size: 282 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 3
Size: 264 Color: 5
Size: 258 Color: 9

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 2
Size: 319 Color: 13
Size: 253 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 10
Size: 346 Color: 6
Size: 252 Color: 6

Total size: 20000
Total free space: 0

