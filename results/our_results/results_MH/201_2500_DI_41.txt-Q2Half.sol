Capicity Bin: 2052
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1288 Color: 1
Size: 692 Color: 1
Size: 72 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 1
Size: 593 Color: 1
Size: 84 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 1
Size: 525 Color: 1
Size: 112 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 1
Size: 583 Color: 1
Size: 46 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1485 Color: 1
Size: 473 Color: 1
Size: 94 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 1
Size: 478 Color: 1
Size: 68 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 1
Size: 389 Color: 1
Size: 128 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 1
Size: 431 Color: 1
Size: 78 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 1
Size: 331 Color: 1
Size: 130 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 1
Size: 402 Color: 1
Size: 52 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 1
Size: 385 Color: 1
Size: 50 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 1
Size: 362 Color: 1
Size: 72 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 1
Size: 357 Color: 1
Size: 70 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 1
Size: 346 Color: 1
Size: 68 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 1
Size: 391 Color: 1
Size: 4 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 1
Size: 282 Color: 1
Size: 92 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 1
Size: 342 Color: 1
Size: 24 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 1
Size: 293 Color: 1
Size: 58 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1709 Color: 1
Size: 257 Color: 1
Size: 86 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 1
Size: 266 Color: 1
Size: 68 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 1
Size: 271 Color: 1
Size: 52 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 1
Size: 314 Color: 1
Size: 4 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 1
Size: 263 Color: 1
Size: 52 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 1
Size: 262 Color: 1
Size: 48 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 1
Size: 231 Color: 1
Size: 76 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 1
Size: 251 Color: 1
Size: 48 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 1
Size: 239 Color: 1
Size: 46 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1772 Color: 1
Size: 218 Color: 1
Size: 62 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 1
Size: 229 Color: 1
Size: 48 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 1
Size: 225 Color: 1
Size: 44 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 1
Size: 219 Color: 1
Size: 42 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 1
Size: 178 Color: 1
Size: 80 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 1
Size: 214 Color: 1
Size: 40 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 1
Size: 187 Color: 1
Size: 36 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 1
Size: 116 Color: 1
Size: 94 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 1
Size: 174 Color: 1
Size: 32 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 1306 Color: 1
Size: 649 Color: 1
Size: 96 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 1420 Color: 1
Size: 579 Color: 1
Size: 52 Color: 0

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 1
Size: 467 Color: 1
Size: 106 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 313 Color: 1
Size: 168 Color: 0

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 1
Size: 422 Color: 1
Size: 46 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 1
Size: 274 Color: 1
Size: 100 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 1
Size: 287 Color: 1
Size: 60 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 1
Size: 228 Color: 0
Size: 24 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 1111 Color: 1
Size: 855 Color: 1
Size: 84 Color: 0

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 1
Size: 751 Color: 1
Size: 136 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 1257 Color: 1
Size: 741 Color: 1
Size: 52 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 1493 Color: 1
Size: 425 Color: 1
Size: 132 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 1
Size: 292 Color: 0
Size: 32 Color: 0

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 1
Size: 663 Color: 1
Size: 16 Color: 0

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 1
Size: 571 Color: 1
Size: 40 Color: 0

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 1
Size: 762 Color: 1
Size: 56 Color: 0

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 1477 Color: 1
Size: 531 Color: 1
Size: 40 Color: 0

Bin 54: 4 of cap free
Amount of items: 2
Items: 
Size: 1546 Color: 1
Size: 502 Color: 0

Bin 55: 5 of cap free
Amount of items: 3
Items: 
Size: 1353 Color: 1
Size: 570 Color: 1
Size: 124 Color: 0

Bin 56: 6 of cap free
Amount of items: 3
Items: 
Size: 1267 Color: 1
Size: 655 Color: 1
Size: 124 Color: 0

Bin 57: 7 of cap free
Amount of items: 3
Items: 
Size: 1275 Color: 1
Size: 686 Color: 1
Size: 84 Color: 0

Bin 58: 7 of cap free
Amount of items: 3
Items: 
Size: 1359 Color: 1
Size: 622 Color: 1
Size: 64 Color: 0

Bin 59: 7 of cap free
Amount of items: 5
Items: 
Size: 1367 Color: 1
Size: 240 Color: 1
Size: 228 Color: 0
Size: 170 Color: 0
Size: 40 Color: 0

Bin 60: 10 of cap free
Amount of items: 3
Items: 
Size: 1031 Color: 1
Size: 897 Color: 1
Size: 114 Color: 0

Bin 61: 15 of cap free
Amount of items: 3
Items: 
Size: 1153 Color: 1
Size: 828 Color: 1
Size: 56 Color: 0

Bin 62: 20 of cap free
Amount of items: 3
Items: 
Size: 1030 Color: 1
Size: 854 Color: 1
Size: 148 Color: 0

Bin 63: 66 of cap free
Amount of items: 3
Items: 
Size: 1027 Color: 1
Size: 845 Color: 1
Size: 114 Color: 0

Bin 64: 100 of cap free
Amount of items: 4
Items: 
Size: 1138 Color: 1
Size: 514 Color: 1
Size: 152 Color: 0
Size: 148 Color: 0

Bin 65: 491 of cap free
Amount of items: 5
Items: 
Size: 481 Color: 1
Size: 472 Color: 1
Size: 428 Color: 1
Size: 104 Color: 0
Size: 76 Color: 0

Bin 66: 1282 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 316 Color: 1
Size: 72 Color: 0

Total size: 133380
Total free space: 2052

