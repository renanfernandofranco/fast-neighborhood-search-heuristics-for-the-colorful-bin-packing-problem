Capicity Bin: 1001
Lower Bound: 222

Bins used: 223
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 5
Size: 410 Color: 10
Size: 103 Color: 9

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 12
Size: 494 Color: 0

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 1
Size: 260 Color: 2

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 12
Size: 220 Color: 8

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 3
Size: 425 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 12
Size: 138 Color: 13
Size: 101 Color: 16

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 12
Size: 381 Color: 5

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 1
Size: 384 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 9
Size: 311 Color: 3
Size: 165 Color: 17

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 17
Size: 313 Color: 10

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 6
Size: 390 Color: 13

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 3
Size: 271 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 727 Color: 2
Size: 161 Color: 13
Size: 113 Color: 17

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 615 Color: 2
Size: 201 Color: 11
Size: 185 Color: 12

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 3
Size: 267 Color: 10

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 6
Size: 389 Color: 18

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 6
Size: 455 Color: 8

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 5
Size: 283 Color: 2

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 19
Size: 331 Color: 18

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 17
Size: 472 Color: 19

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 7
Size: 462 Color: 9

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 12
Size: 403 Color: 3

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 9
Size: 316 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 0
Size: 141 Color: 18
Size: 111 Color: 1

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 14
Size: 499 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 726 Color: 13
Size: 158 Color: 8
Size: 117 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 16
Size: 156 Color: 18
Size: 147 Color: 18

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 17
Size: 215 Color: 19

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 5
Size: 272 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 0
Size: 314 Color: 16
Size: 278 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 617 Color: 19
Size: 209 Color: 14
Size: 175 Color: 19

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 8
Size: 246 Color: 4

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 3
Size: 282 Color: 19

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 13
Size: 302 Color: 13
Size: 205 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 9
Size: 170 Color: 5
Size: 155 Color: 7

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 4
Size: 479 Color: 6

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 1
Size: 347 Color: 16

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 8
Size: 211 Color: 9

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 11
Size: 448 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9
Size: 326 Color: 3
Size: 190 Color: 7

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 0
Size: 252 Color: 6

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 8
Size: 329 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 767 Color: 10
Size: 128 Color: 16
Size: 106 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 5
Size: 326 Color: 1
Size: 278 Color: 5

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 6
Size: 404 Color: 12

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 18
Size: 271 Color: 5

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 5
Size: 334 Color: 10

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 19
Size: 200 Color: 5

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 16
Size: 384 Color: 12

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 11
Size: 254 Color: 13

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 11
Size: 288 Color: 14

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 5
Size: 489 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 648 Color: 3
Size: 179 Color: 19
Size: 174 Color: 6

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 6
Size: 446 Color: 1

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 16
Size: 351 Color: 18

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 18
Size: 317 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 9
Size: 143 Color: 8
Size: 101 Color: 11

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 449 Color: 14

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 14
Size: 258 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 520 Color: 6
Size: 307 Color: 9
Size: 174 Color: 7

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 7
Size: 361 Color: 1

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 2
Size: 306 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 0
Size: 308 Color: 4
Size: 300 Color: 19

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 5
Size: 412 Color: 15

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 6
Size: 460 Color: 14

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 10
Size: 414 Color: 15

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 18
Size: 435 Color: 7

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 4
Size: 305 Color: 15
Size: 236 Color: 10

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 343 Color: 10

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 11
Size: 175 Color: 14
Size: 160 Color: 1

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 13
Size: 322 Color: 5

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 5
Size: 284 Color: 14

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 616 Color: 10
Size: 193 Color: 0
Size: 192 Color: 8

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 4
Size: 130 Color: 5
Size: 125 Color: 19

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 8
Size: 205 Color: 10
Size: 151 Color: 15

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 1
Size: 197 Color: 15
Size: 190 Color: 16

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 3
Size: 499 Color: 6

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 18
Size: 478 Color: 12

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 12
Size: 398 Color: 14

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 5
Size: 389 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 529 Color: 19
Size: 236 Color: 13
Size: 236 Color: 10

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 798 Color: 0
Size: 102 Color: 14
Size: 101 Color: 7

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 1
Size: 185 Color: 17
Size: 113 Color: 4

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 16
Size: 223 Color: 12

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 10
Size: 208 Color: 4

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 14
Size: 285 Color: 15
Size: 192 Color: 17

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 19
Size: 234 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 751 Color: 9
Size: 135 Color: 0
Size: 115 Color: 15

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 2
Size: 159 Color: 11
Size: 108 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 11
Size: 314 Color: 0
Size: 286 Color: 18

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 15
Size: 406 Color: 3

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 3
Size: 417 Color: 16

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 8
Size: 285 Color: 4

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 5
Size: 447 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 11
Size: 265 Color: 19
Size: 123 Color: 16

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 17
Size: 258 Color: 18

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 6
Size: 331 Color: 1

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 19
Size: 255 Color: 7

Bin 99: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 1
Size: 426 Color: 15

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 9
Size: 323 Color: 16
Size: 318 Color: 5

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 14
Size: 383 Color: 2

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 5
Size: 338 Color: 14

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 3
Size: 390 Color: 3
Size: 202 Color: 2

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 5
Size: 413 Color: 13

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 5
Size: 492 Color: 8

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 8
Size: 299 Color: 11
Size: 211 Color: 6

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 7
Size: 432 Color: 9

Bin 108: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 2
Size: 306 Color: 14

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 14
Size: 337 Color: 12
Size: 285 Color: 17

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 16
Size: 360 Color: 11
Size: 276 Color: 18

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 5
Size: 320 Color: 17
Size: 283 Color: 7

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 6
Size: 264 Color: 15
Size: 253 Color: 4

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 7
Size: 496 Color: 16

Bin 114: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 10
Size: 480 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 18
Size: 229 Color: 6
Size: 205 Color: 8

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 7
Size: 435 Color: 3

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 528 Color: 12
Size: 274 Color: 3
Size: 199 Color: 6

Bin 118: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 0
Size: 434 Color: 11

Bin 119: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 7
Size: 388 Color: 0

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 2

Bin 121: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 15
Size: 356 Color: 13

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 3
Size: 353 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 12
Size: 169 Color: 16
Size: 158 Color: 7

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 13
Size: 192 Color: 8
Size: 144 Color: 16

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 7
Size: 319 Color: 15

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 701 Color: 12
Size: 161 Color: 19
Size: 139 Color: 8

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 14
Size: 175 Color: 4
Size: 109 Color: 6

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 1
Size: 149 Color: 9
Size: 123 Color: 1

Bin 129: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 12
Size: 271 Color: 13

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 523 Color: 13
Size: 260 Color: 16
Size: 218 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 4
Size: 148 Color: 10
Size: 108 Color: 10

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 12
Size: 254 Color: 19
Size: 133 Color: 16

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 755 Color: 2
Size: 138 Color: 7
Size: 108 Color: 17

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 4
Size: 188 Color: 0
Size: 150 Color: 8

Bin 135: 1 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 8
Size: 324 Color: 16

Bin 136: 1 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 14
Size: 172 Color: 9
Size: 136 Color: 2

Bin 137: 1 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 17
Size: 251 Color: 0

Bin 138: 1 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 3
Size: 453 Color: 15

Bin 139: 1 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 14
Size: 489 Color: 12

Bin 140: 1 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 16
Size: 423 Color: 5

Bin 141: 1 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 9
Size: 343 Color: 12

Bin 142: 1 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 14
Size: 269 Color: 3

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 13
Size: 214 Color: 11
Size: 119 Color: 6

Bin 144: 1 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 12
Size: 440 Color: 13

Bin 145: 1 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 11
Size: 432 Color: 18

Bin 146: 1 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 8
Size: 289 Color: 3

Bin 147: 1 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 15
Size: 373 Color: 17

Bin 148: 1 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 10
Size: 219 Color: 14

Bin 149: 1 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 11
Size: 404 Color: 9

Bin 150: 1 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 2
Size: 482 Color: 12

Bin 151: 1 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 2
Size: 279 Color: 15

Bin 152: 1 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 12
Size: 352 Color: 11

Bin 153: 1 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 6
Size: 206 Color: 1

Bin 154: 1 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 4
Size: 492 Color: 1

Bin 155: 1 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 202 Color: 11

Bin 156: 1 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 15
Size: 375 Color: 4

Bin 157: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 4
Size: 427 Color: 19

Bin 158: 1 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 8
Size: 436 Color: 16

Bin 159: 1 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 19
Size: 451 Color: 1

Bin 160: 1 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 2
Size: 363 Color: 5

Bin 161: 2 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 12
Size: 461 Color: 0

Bin 162: 2 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 3
Size: 261 Color: 16

Bin 163: 2 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 10
Size: 292 Color: 15

Bin 164: 2 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 7
Size: 360 Color: 18

Bin 165: 2 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 13
Size: 292 Color: 16

Bin 166: 2 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 2
Size: 365 Color: 3

Bin 167: 2 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 17
Size: 323 Color: 10

Bin 168: 2 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 2
Size: 346 Color: 16

Bin 169: 2 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 1
Size: 241 Color: 19

Bin 170: 2 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 3
Size: 342 Color: 6

Bin 171: 2 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 10
Size: 314 Color: 1
Size: 310 Color: 5

Bin 172: 2 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 456 Color: 4

Bin 173: 2 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 16
Size: 410 Color: 9

Bin 174: 2 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 14
Size: 266 Color: 16

Bin 175: 3 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 13
Size: 322 Color: 1

Bin 176: 3 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 12
Size: 351 Color: 18

Bin 177: 3 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 6
Size: 415 Color: 1

Bin 178: 4 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 19
Size: 469 Color: 11

Bin 179: 4 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 19
Size: 430 Color: 5

Bin 180: 4 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 1
Size: 460 Color: 9

Bin 181: 4 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 4
Size: 249 Color: 12

Bin 182: 4 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 15
Size: 209 Color: 0

Bin 183: 4 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 3
Size: 403 Color: 5

Bin 184: 5 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 5
Size: 238 Color: 11

Bin 185: 5 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 18
Size: 291 Color: 0

Bin 186: 5 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 8
Size: 496 Color: 12

Bin 187: 5 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 10
Size: 379 Color: 2

Bin 188: 6 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 16
Size: 341 Color: 1

Bin 189: 6 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 5
Size: 227 Color: 10

Bin 190: 6 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 12
Size: 459 Color: 5

Bin 191: 8 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 5
Size: 359 Color: 18

Bin 192: 8 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 17
Size: 205 Color: 12

Bin 193: 8 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 1
Size: 340 Color: 16

Bin 194: 9 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 11
Size: 445 Color: 15

Bin 195: 10 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 237 Color: 3

Bin 196: 10 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 10
Size: 340 Color: 15

Bin 197: 11 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 14
Size: 359 Color: 6

Bin 198: 11 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 264 Color: 13

Bin 199: 11 of cap free
Amount of items: 2
Items: 
Size: 495 Color: 8
Size: 495 Color: 3

Bin 200: 12 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 4
Size: 359 Color: 17

Bin 201: 12 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 0
Size: 317 Color: 14

Bin 202: 13 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 3
Size: 204 Color: 1

Bin 203: 14 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 14
Size: 358 Color: 18

Bin 204: 15 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 4
Size: 340 Color: 10

Bin 205: 15 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 13
Size: 422 Color: 19

Bin 206: 17 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 6
Size: 421 Color: 10

Bin 207: 18 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 15
Size: 421 Color: 7

Bin 208: 18 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 421 Color: 2

Bin 209: 20 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 14
Size: 419 Color: 11

Bin 210: 21 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 11
Size: 278 Color: 6

Bin 211: 22 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 3
Size: 310 Color: 16

Bin 212: 22 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 14
Size: 235 Color: 18

Bin 213: 23 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 16
Size: 235 Color: 11

Bin 214: 25 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 7
Size: 196 Color: 19

Bin 215: 27 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 13
Size: 232 Color: 5

Bin 216: 27 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 6
Size: 358 Color: 14

Bin 217: 35 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 5
Size: 409 Color: 9

Bin 218: 54 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 12
Size: 170 Color: 7

Bin 219: 56 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 5
Size: 399 Color: 9

Bin 220: 68 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 13
Size: 398 Color: 5

Bin 221: 82 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 11
Size: 396 Color: 15

Bin 222: 85 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 2
Size: 393 Color: 17

Bin 223: 91 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 17
Size: 390 Color: 19

Total size: 222220
Total free space: 1003

