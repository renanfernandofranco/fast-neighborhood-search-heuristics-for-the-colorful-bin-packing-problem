Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 428914 Color: 4
Size: 319645 Color: 1
Size: 251442 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 406694 Color: 13
Size: 335976 Color: 9
Size: 257331 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 375056 Color: 9
Size: 316011 Color: 15
Size: 308934 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 495447 Color: 7
Size: 254531 Color: 8
Size: 250023 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 348817 Color: 7
Size: 347296 Color: 4
Size: 303888 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 392797 Color: 2
Size: 324249 Color: 16
Size: 282955 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 388064 Color: 13
Size: 355712 Color: 1
Size: 256225 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 442885 Color: 2
Size: 299139 Color: 7
Size: 257977 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 438813 Color: 2
Size: 281968 Color: 18
Size: 279220 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 466820 Color: 16
Size: 268038 Color: 19
Size: 265143 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 434564 Color: 6
Size: 314251 Color: 10
Size: 251186 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 497452 Color: 11
Size: 251510 Color: 9
Size: 251039 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 383424 Color: 2
Size: 357375 Color: 19
Size: 259202 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 477929 Color: 17
Size: 267464 Color: 10
Size: 254608 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 374779 Color: 15
Size: 367046 Color: 8
Size: 258176 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 423635 Color: 6
Size: 300091 Color: 8
Size: 276275 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 476957 Color: 18
Size: 270797 Color: 15
Size: 252247 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 356025 Color: 10
Size: 333072 Color: 4
Size: 310904 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 463746 Color: 12
Size: 284246 Color: 15
Size: 252009 Color: 5

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 483408 Color: 9
Size: 260809 Color: 7
Size: 255784 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 401163 Color: 4
Size: 343882 Color: 2
Size: 254956 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 422231 Color: 16
Size: 303293 Color: 0
Size: 274477 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 382442 Color: 11
Size: 362324 Color: 16
Size: 255235 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 387647 Color: 8
Size: 353025 Color: 2
Size: 259329 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 387640 Color: 16
Size: 310087 Color: 12
Size: 302274 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 429973 Color: 9
Size: 291889 Color: 11
Size: 278139 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 494128 Color: 9
Size: 255677 Color: 9
Size: 250196 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 404104 Color: 2
Size: 341165 Color: 0
Size: 254732 Color: 15

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 371691 Color: 19
Size: 317107 Color: 0
Size: 311203 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 360940 Color: 4
Size: 340128 Color: 3
Size: 298933 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 367321 Color: 1
Size: 366887 Color: 19
Size: 265793 Color: 10

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 383598 Color: 9
Size: 331177 Color: 19
Size: 285226 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 374372 Color: 2
Size: 367217 Color: 2
Size: 258412 Color: 19

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 410907 Color: 12
Size: 327878 Color: 9
Size: 261216 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 402698 Color: 14
Size: 341022 Color: 9
Size: 256281 Color: 12

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 350046 Color: 17
Size: 325357 Color: 1
Size: 324598 Color: 12

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 391774 Color: 4
Size: 329588 Color: 19
Size: 278639 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 497951 Color: 15
Size: 251328 Color: 3
Size: 250722 Color: 16

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 388054 Color: 9
Size: 320882 Color: 17
Size: 291065 Color: 7

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 393780 Color: 16
Size: 313989 Color: 17
Size: 292232 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 441103 Color: 0
Size: 302722 Color: 7
Size: 256176 Color: 15

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 389344 Color: 1
Size: 359168 Color: 8
Size: 251489 Color: 15

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 389099 Color: 14
Size: 319592 Color: 8
Size: 291310 Color: 7

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 464093 Color: 15
Size: 270980 Color: 4
Size: 264928 Color: 10

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 380292 Color: 8
Size: 315144 Color: 17
Size: 304565 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 466015 Color: 17
Size: 272793 Color: 16
Size: 261193 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 348971 Color: 17
Size: 347675 Color: 16
Size: 303355 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 424078 Color: 1
Size: 298328 Color: 13
Size: 277595 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 465581 Color: 2
Size: 271217 Color: 1
Size: 263203 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 417203 Color: 2
Size: 295753 Color: 4
Size: 287045 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 489768 Color: 2
Size: 256848 Color: 18
Size: 253385 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 435219 Color: 2
Size: 305809 Color: 18
Size: 258973 Color: 18

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 359900 Color: 19
Size: 335736 Color: 3
Size: 304365 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 495964 Color: 9
Size: 252452 Color: 2
Size: 251585 Color: 9

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 390247 Color: 14
Size: 313569 Color: 14
Size: 296185 Color: 12

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 447219 Color: 15
Size: 297217 Color: 4
Size: 255565 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 498291 Color: 15
Size: 251004 Color: 13
Size: 250706 Color: 15

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 455491 Color: 4
Size: 285087 Color: 2
Size: 259423 Color: 18

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 459779 Color: 14
Size: 276950 Color: 2
Size: 263272 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 371068 Color: 1
Size: 331006 Color: 7
Size: 297927 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 375892 Color: 2
Size: 364964 Color: 18
Size: 259145 Color: 6

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 437235 Color: 17
Size: 288468 Color: 19
Size: 274298 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 380196 Color: 9
Size: 360444 Color: 2
Size: 259361 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 419132 Color: 11
Size: 324843 Color: 18
Size: 256026 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 366102 Color: 10
Size: 352872 Color: 17
Size: 281027 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 423621 Color: 13
Size: 297915 Color: 18
Size: 278465 Color: 7

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 419312 Color: 8
Size: 323732 Color: 16
Size: 256957 Color: 13

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 438131 Color: 2
Size: 307632 Color: 15
Size: 254238 Color: 17

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 379479 Color: 9
Size: 321343 Color: 7
Size: 299179 Color: 17

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 387047 Color: 3
Size: 359126 Color: 4
Size: 253828 Color: 9

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 412007 Color: 15
Size: 334610 Color: 18
Size: 253384 Color: 13

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 481817 Color: 14
Size: 260475 Color: 17
Size: 257709 Color: 18

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 444989 Color: 3
Size: 286883 Color: 14
Size: 268129 Color: 13

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 380151 Color: 14
Size: 338228 Color: 13
Size: 281622 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 434030 Color: 9
Size: 283409 Color: 0
Size: 282562 Color: 10

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 432670 Color: 12
Size: 304404 Color: 19
Size: 262927 Color: 13

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 437487 Color: 13
Size: 291198 Color: 12
Size: 271316 Color: 16

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 413385 Color: 14
Size: 335700 Color: 2
Size: 250916 Color: 9

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 473915 Color: 6
Size: 265322 Color: 15
Size: 260764 Color: 8

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 371458 Color: 12
Size: 340111 Color: 4
Size: 288432 Color: 9

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 454612 Color: 14
Size: 276864 Color: 5
Size: 268525 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 457313 Color: 6
Size: 276308 Color: 15
Size: 266380 Color: 16

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 395469 Color: 7
Size: 340315 Color: 6
Size: 264217 Color: 14

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 440529 Color: 8
Size: 291570 Color: 1
Size: 267902 Color: 18

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 372990 Color: 14
Size: 336330 Color: 5
Size: 290681 Color: 7

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 395001 Color: 4
Size: 350595 Color: 8
Size: 254405 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 370789 Color: 9
Size: 318136 Color: 19
Size: 311076 Color: 10

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 383615 Color: 4
Size: 323777 Color: 5
Size: 292609 Color: 15

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 493323 Color: 10
Size: 255337 Color: 4
Size: 251341 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 480564 Color: 11
Size: 269061 Color: 17
Size: 250376 Color: 8

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 441056 Color: 17
Size: 294810 Color: 3
Size: 264135 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 415989 Color: 5
Size: 317200 Color: 11
Size: 266812 Color: 7

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 410222 Color: 13
Size: 327795 Color: 11
Size: 261984 Color: 9

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 338421 Color: 4
Size: 338140 Color: 16
Size: 323440 Color: 6

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 378747 Color: 14
Size: 342869 Color: 5
Size: 278385 Color: 7

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 428550 Color: 7
Size: 295827 Color: 17
Size: 275624 Color: 10

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 419930 Color: 8
Size: 291207 Color: 9
Size: 288864 Color: 6

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 425421 Color: 13
Size: 300778 Color: 1
Size: 273802 Color: 6

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 414735 Color: 15
Size: 313756 Color: 10
Size: 271510 Color: 7

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 418918 Color: 0
Size: 307231 Color: 6
Size: 273852 Color: 15

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 392589 Color: 7
Size: 319985 Color: 3
Size: 287427 Color: 2

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 454276 Color: 5
Size: 282123 Color: 19
Size: 263602 Color: 15

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 444683 Color: 16
Size: 298456 Color: 12
Size: 256862 Color: 19

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 366298 Color: 10
Size: 336138 Color: 9
Size: 297565 Color: 9

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 368109 Color: 7
Size: 350873 Color: 9
Size: 281019 Color: 7

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 392982 Color: 1
Size: 321129 Color: 16
Size: 285890 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 495061 Color: 13
Size: 253692 Color: 9
Size: 251248 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 493150 Color: 1
Size: 256840 Color: 4
Size: 250011 Color: 17

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 415324 Color: 18
Size: 304745 Color: 14
Size: 279932 Color: 18

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 368131 Color: 18
Size: 332390 Color: 8
Size: 299480 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 340554 Color: 0
Size: 333037 Color: 19
Size: 326410 Color: 7

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 484516 Color: 10
Size: 264338 Color: 16
Size: 251147 Color: 6

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 417929 Color: 11
Size: 323684 Color: 5
Size: 258388 Color: 19

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 360213 Color: 12
Size: 341214 Color: 16
Size: 298574 Color: 9

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 388652 Color: 4
Size: 330855 Color: 3
Size: 280494 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 488256 Color: 17
Size: 261565 Color: 5
Size: 250180 Color: 14

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 435493 Color: 19
Size: 286298 Color: 1
Size: 278210 Color: 14

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 412237 Color: 4
Size: 337721 Color: 17
Size: 250043 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 416766 Color: 2
Size: 308220 Color: 16
Size: 275015 Color: 12

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 386551 Color: 11
Size: 309879 Color: 10
Size: 303571 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 450243 Color: 6
Size: 297454 Color: 16
Size: 252304 Color: 10

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 403406 Color: 14
Size: 308182 Color: 16
Size: 288413 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 409121 Color: 12
Size: 335216 Color: 1
Size: 255664 Color: 6

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 352284 Color: 11
Size: 327191 Color: 10
Size: 320526 Color: 2

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 344970 Color: 12
Size: 331546 Color: 10
Size: 323485 Color: 9

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 443463 Color: 17
Size: 295487 Color: 11
Size: 261051 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 475010 Color: 18
Size: 267704 Color: 19
Size: 257287 Color: 5

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 407943 Color: 13
Size: 318269 Color: 5
Size: 273789 Color: 5

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 490402 Color: 5
Size: 254833 Color: 18
Size: 254766 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 477493 Color: 8
Size: 271616 Color: 0
Size: 250892 Color: 15

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 375772 Color: 13
Size: 321965 Color: 19
Size: 302264 Color: 16

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 423126 Color: 15
Size: 317150 Color: 2
Size: 259725 Color: 17

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 406940 Color: 4
Size: 337817 Color: 4
Size: 255244 Color: 8

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 388101 Color: 8
Size: 348600 Color: 3
Size: 263300 Color: 9

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 396326 Color: 2
Size: 352579 Color: 4
Size: 251096 Color: 5

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 488342 Color: 7
Size: 258469 Color: 17
Size: 253190 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 446872 Color: 10
Size: 302130 Color: 12
Size: 250999 Color: 19

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 416988 Color: 10
Size: 299909 Color: 16
Size: 283104 Color: 16

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 423007 Color: 11
Size: 291369 Color: 5
Size: 285625 Color: 12

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 455813 Color: 19
Size: 278732 Color: 9
Size: 265456 Color: 18

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 491029 Color: 0
Size: 256259 Color: 1
Size: 252713 Color: 5

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 370615 Color: 2
Size: 325821 Color: 0
Size: 303565 Color: 7

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 364098 Color: 2
Size: 328866 Color: 16
Size: 307037 Color: 11

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 412288 Color: 8
Size: 315367 Color: 11
Size: 272346 Color: 16

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 435791 Color: 12
Size: 299451 Color: 6
Size: 264759 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 379082 Color: 12
Size: 325547 Color: 2
Size: 295372 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 420082 Color: 12
Size: 317136 Color: 6
Size: 262783 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 421274 Color: 16
Size: 317089 Color: 15
Size: 261638 Color: 15

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 376448 Color: 15
Size: 368407 Color: 19
Size: 255146 Color: 8

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 389847 Color: 19
Size: 331483 Color: 19
Size: 278671 Color: 13

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 440204 Color: 13
Size: 303622 Color: 5
Size: 256175 Color: 8

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 394947 Color: 11
Size: 354165 Color: 12
Size: 250889 Color: 19

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 390087 Color: 15
Size: 336258 Color: 10
Size: 273656 Color: 10

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 359457 Color: 14
Size: 353177 Color: 14
Size: 287367 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 440738 Color: 14
Size: 279835 Color: 13
Size: 279428 Color: 13

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 474395 Color: 4
Size: 272047 Color: 7
Size: 253559 Color: 13

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 488165 Color: 18
Size: 257035 Color: 17
Size: 254801 Color: 19

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 423856 Color: 4
Size: 308689 Color: 19
Size: 267456 Color: 10

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 442779 Color: 14
Size: 292495 Color: 13
Size: 264727 Color: 16

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 468214 Color: 0
Size: 272494 Color: 12
Size: 259293 Color: 6

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 364312 Color: 19
Size: 353837 Color: 6
Size: 281852 Color: 7

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 452036 Color: 0
Size: 281032 Color: 16
Size: 266933 Color: 12

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 454043 Color: 16
Size: 286497 Color: 10
Size: 259461 Color: 12

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 386062 Color: 7
Size: 328838 Color: 19
Size: 285101 Color: 19

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 397287 Color: 17
Size: 344405 Color: 8
Size: 258309 Color: 8

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 389653 Color: 1
Size: 331682 Color: 3
Size: 278666 Color: 11

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 348098 Color: 3
Size: 346087 Color: 8
Size: 305816 Color: 8

Total size: 167000167
Total free space: 0

