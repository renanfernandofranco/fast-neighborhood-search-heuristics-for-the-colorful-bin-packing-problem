Capicity Bin: 2464
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 840 Color: 3
Size: 504 Color: 1
Size: 384 Color: 3
Size: 272 Color: 3
Size: 272 Color: 1
Size: 72 Color: 4
Size: 64 Color: 4
Size: 48 Color: 4
Size: 8 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 2
Size: 780 Color: 2
Size: 152 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 1
Size: 892 Color: 2
Size: 176 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1859 Color: 4
Size: 505 Color: 2
Size: 100 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 3
Size: 401 Color: 3
Size: 80 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 1
Size: 1026 Color: 2
Size: 204 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2084 Color: 0
Size: 324 Color: 4
Size: 56 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 1
Size: 883 Color: 4
Size: 176 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2099 Color: 2
Size: 305 Color: 1
Size: 60 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1861 Color: 4
Size: 503 Color: 2
Size: 100 Color: 4

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 1696 Color: 4
Size: 576 Color: 2
Size: 96 Color: 3
Size: 96 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 4
Size: 571 Color: 4
Size: 112 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2044 Color: 3
Size: 356 Color: 3
Size: 64 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 1
Size: 882 Color: 4
Size: 172 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1958 Color: 4
Size: 462 Color: 3
Size: 44 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1235 Color: 2
Size: 1025 Color: 0
Size: 204 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2025 Color: 1
Size: 367 Color: 1
Size: 72 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 0
Size: 622 Color: 0
Size: 120 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 2
Size: 246 Color: 2
Size: 48 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2004 Color: 3
Size: 388 Color: 1
Size: 72 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 0
Size: 443 Color: 3
Size: 24 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1850 Color: 3
Size: 546 Color: 1
Size: 68 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 2
Size: 222 Color: 2
Size: 44 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 4
Size: 428 Color: 3
Size: 80 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 2
Size: 682 Color: 4
Size: 136 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 0
Size: 874 Color: 1
Size: 172 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 3
Size: 268 Color: 4
Size: 48 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 0
Size: 322 Color: 3
Size: 64 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1985 Color: 3
Size: 471 Color: 1
Size: 8 Color: 1

Bin 30: 0 of cap free
Amount of items: 4
Items: 
Size: 1740 Color: 2
Size: 628 Color: 4
Size: 88 Color: 0
Size: 8 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 0
Size: 220 Color: 3
Size: 40 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 1
Size: 573 Color: 0
Size: 114 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1930 Color: 0
Size: 446 Color: 4
Size: 88 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2150 Color: 1
Size: 262 Color: 4
Size: 52 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 3
Size: 290 Color: 0
Size: 56 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1860 Color: 2
Size: 516 Color: 0
Size: 88 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 2
Size: 846 Color: 3
Size: 168 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2093 Color: 1
Size: 311 Color: 1
Size: 60 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1671 Color: 2
Size: 661 Color: 4
Size: 132 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 4
Size: 212 Color: 4
Size: 40 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 1
Size: 838 Color: 4
Size: 28 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 4
Size: 468 Color: 3
Size: 88 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1769 Color: 0
Size: 581 Color: 2
Size: 114 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 0
Size: 684 Color: 3
Size: 136 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 1
Size: 763 Color: 1
Size: 152 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1927 Color: 3
Size: 449 Color: 2
Size: 88 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 4
Size: 1028 Color: 2
Size: 200 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 1
Size: 345 Color: 3
Size: 68 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 3
Size: 758 Color: 1
Size: 148 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 1
Size: 1022 Color: 2
Size: 200 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1551 Color: 4
Size: 761 Color: 3
Size: 152 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 0
Size: 881 Color: 2
Size: 176 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1929 Color: 1
Size: 447 Color: 1
Size: 88 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1796 Color: 1
Size: 564 Color: 2
Size: 104 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 3
Size: 562 Color: 0
Size: 112 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 3
Size: 882 Color: 4
Size: 176 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 4
Size: 354 Color: 4
Size: 68 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 2043 Color: 1
Size: 351 Color: 1
Size: 70 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 2
Size: 474 Color: 0
Size: 92 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 1
Size: 390 Color: 2
Size: 76 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 1
Size: 292 Color: 1
Size: 56 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 0
Size: 663 Color: 0
Size: 132 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2133 Color: 3
Size: 277 Color: 0
Size: 54 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 1
Size: 1027 Color: 2
Size: 204 Color: 1

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 2
Size: 920 Color: 3

Total size: 160160
Total free space: 0

