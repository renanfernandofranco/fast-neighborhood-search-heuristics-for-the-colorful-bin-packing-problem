Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 396847 Color: 1
Size: 291544 Color: 0
Size: 311610 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 428616 Color: 1
Size: 289643 Color: 1
Size: 281742 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 357709 Color: 1
Size: 320445 Color: 0
Size: 321847 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 478511 Color: 1
Size: 265901 Color: 1
Size: 255589 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370728 Color: 1
Size: 330075 Color: 1
Size: 299198 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 449485 Color: 1
Size: 289910 Color: 1
Size: 260606 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 402750 Color: 1
Size: 343124 Color: 1
Size: 254127 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 404699 Color: 1
Size: 317924 Color: 1
Size: 277378 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 451554 Color: 1
Size: 294678 Color: 1
Size: 253769 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 437238 Color: 1
Size: 291228 Color: 1
Size: 271535 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 488067 Color: 1
Size: 258682 Color: 1
Size: 253252 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 487344 Color: 1
Size: 259154 Color: 1
Size: 253503 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 486540 Color: 1
Size: 263402 Color: 1
Size: 250059 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 445566 Color: 1
Size: 282566 Color: 1
Size: 271869 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 472737 Color: 1
Size: 275525 Color: 1
Size: 251739 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 405069 Color: 1
Size: 342477 Color: 1
Size: 252455 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 383250 Color: 1
Size: 353858 Color: 1
Size: 262893 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 366411 Color: 1
Size: 378956 Color: 1
Size: 254634 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 429421 Color: 1
Size: 285980 Color: 1
Size: 284600 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 423915 Color: 1
Size: 314362 Color: 1
Size: 261724 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 385565 Color: 1
Size: 331083 Color: 1
Size: 283353 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 389924 Color: 1
Size: 336076 Color: 1
Size: 274001 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 479509 Color: 1
Size: 268515 Color: 1
Size: 251977 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 482753 Color: 1
Size: 259454 Color: 1
Size: 257794 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 341149 Color: 1
Size: 339249 Color: 1
Size: 319603 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 414328 Color: 1
Size: 296819 Color: 1
Size: 288854 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 425262 Color: 1
Size: 320431 Color: 1
Size: 254308 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 498072 Color: 1
Size: 251115 Color: 1
Size: 250814 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 401411 Color: 1
Size: 314876 Color: 1
Size: 283714 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 454307 Color: 1
Size: 290577 Color: 1
Size: 255117 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 426204 Color: 1
Size: 303137 Color: 1
Size: 270660 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 449963 Color: 1
Size: 287403 Color: 1
Size: 262635 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 393719 Color: 1
Size: 309614 Color: 1
Size: 296668 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 432469 Color: 1
Size: 307802 Color: 1
Size: 259730 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 482988 Color: 1
Size: 260241 Color: 1
Size: 256772 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 402168 Color: 1
Size: 314037 Color: 1
Size: 283796 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 434477 Color: 1
Size: 303542 Color: 1
Size: 261982 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 382140 Color: 1
Size: 350118 Color: 1
Size: 267743 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 439250 Color: 1
Size: 290028 Color: 1
Size: 270723 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 434789 Color: 1
Size: 299135 Color: 1
Size: 266077 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 369024 Color: 1
Size: 323037 Color: 1
Size: 307940 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 380316 Color: 1
Size: 363200 Color: 1
Size: 256485 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 379469 Color: 1
Size: 315126 Color: 1
Size: 305406 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 437460 Color: 1
Size: 298192 Color: 1
Size: 264349 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 417424 Color: 1
Size: 307158 Color: 1
Size: 275419 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 460280 Color: 1
Size: 275259 Color: 1
Size: 264462 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 400590 Color: 1
Size: 305405 Color: 1
Size: 294006 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 410616 Color: 1
Size: 337305 Color: 1
Size: 252080 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 395233 Color: 1
Size: 330622 Color: 1
Size: 274146 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 408770 Color: 1
Size: 318844 Color: 1
Size: 272387 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 411729 Color: 1
Size: 308498 Color: 1
Size: 279774 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 383659 Color: 1
Size: 341359 Color: 1
Size: 274983 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 390638 Color: 1
Size: 352134 Color: 1
Size: 257229 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 356619 Color: 1
Size: 325426 Color: 1
Size: 317956 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 413652 Color: 1
Size: 294780 Color: 1
Size: 291569 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 353807 Color: 1
Size: 332956 Color: 1
Size: 313238 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 381364 Color: 1
Size: 333436 Color: 1
Size: 285201 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 388453 Color: 1
Size: 323031 Color: 1
Size: 288517 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 470534 Color: 1
Size: 267697 Color: 1
Size: 261770 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 359774 Color: 1
Size: 341061 Color: 1
Size: 299166 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 371446 Color: 1
Size: 316386 Color: 1
Size: 312169 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 477799 Color: 1
Size: 270943 Color: 1
Size: 251259 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 479419 Color: 1
Size: 268131 Color: 1
Size: 252451 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 458686 Color: 1
Size: 274730 Color: 1
Size: 266585 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 415997 Color: 1
Size: 316243 Color: 1
Size: 267761 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 373319 Color: 1
Size: 330567 Color: 1
Size: 296115 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 429912 Color: 1
Size: 288196 Color: 1
Size: 281893 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 417569 Color: 1
Size: 300073 Color: 1
Size: 282359 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 377655 Color: 1
Size: 362604 Color: 1
Size: 259742 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 400795 Color: 1
Size: 332349 Color: 1
Size: 266857 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 429170 Color: 1
Size: 304627 Color: 1
Size: 266204 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 388623 Color: 1
Size: 350897 Color: 1
Size: 260481 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 395935 Color: 1
Size: 350745 Color: 1
Size: 253321 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 375349 Color: 1
Size: 365272 Color: 1
Size: 259380 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 369829 Color: 1
Size: 338070 Color: 1
Size: 292102 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 371114 Color: 1
Size: 317452 Color: 1
Size: 311435 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 374515 Color: 1
Size: 343814 Color: 1
Size: 281672 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 372158 Color: 1
Size: 360342 Color: 1
Size: 267501 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 369021 Color: 1
Size: 343640 Color: 1
Size: 287340 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 419076 Color: 1
Size: 323011 Color: 1
Size: 257914 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 402005 Color: 1
Size: 331376 Color: 1
Size: 266620 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 350110 Color: 1
Size: 330590 Color: 1
Size: 319301 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 362932 Color: 1
Size: 354435 Color: 1
Size: 282634 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 351813 Color: 1
Size: 333483 Color: 1
Size: 314705 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 375048 Color: 1
Size: 320440 Color: 1
Size: 304513 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 357159 Color: 1
Size: 333992 Color: 1
Size: 308850 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 431343 Color: 1
Size: 299818 Color: 1
Size: 268840 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 368616 Color: 1
Size: 318639 Color: 1
Size: 312746 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 362654 Color: 1
Size: 321417 Color: 1
Size: 315930 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 366921 Color: 1
Size: 319430 Color: 1
Size: 313650 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 371806 Color: 1
Size: 330310 Color: 1
Size: 297885 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 373204 Color: 1
Size: 328990 Color: 1
Size: 297807 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 373621 Color: 1
Size: 334260 Color: 1
Size: 292120 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 374807 Color: 1
Size: 351999 Color: 1
Size: 273195 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 376782 Color: 1
Size: 346872 Color: 1
Size: 276347 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 378745 Color: 1
Size: 317181 Color: 1
Size: 304075 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 382454 Color: 1
Size: 350648 Color: 1
Size: 266899 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 382708 Color: 1
Size: 355755 Color: 1
Size: 261538 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 386380 Color: 1
Size: 358678 Color: 1
Size: 254943 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 390923 Color: 1
Size: 318321 Color: 1
Size: 290757 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 392674 Color: 1
Size: 356537 Color: 1
Size: 250790 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 392870 Color: 1
Size: 324377 Color: 1
Size: 282754 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 392927 Color: 1
Size: 330433 Color: 1
Size: 276641 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 393595 Color: 1
Size: 333333 Color: 1
Size: 273073 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 394116 Color: 1
Size: 327862 Color: 1
Size: 278023 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 395936 Color: 1
Size: 338531 Color: 1
Size: 265534 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 396216 Color: 1
Size: 316278 Color: 1
Size: 287507 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 399742 Color: 1
Size: 347023 Color: 1
Size: 253236 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 405104 Color: 1
Size: 303646 Color: 1
Size: 291251 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 409799 Color: 1
Size: 330023 Color: 1
Size: 260179 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 415152 Color: 1
Size: 329477 Color: 1
Size: 255372 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 416860 Color: 1
Size: 304249 Color: 1
Size: 278892 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 417136 Color: 1
Size: 301048 Color: 1
Size: 281817 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 418480 Color: 1
Size: 292217 Color: 1
Size: 289304 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 419334 Color: 1
Size: 303318 Color: 1
Size: 277349 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 419620 Color: 1
Size: 304749 Color: 1
Size: 275632 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 420202 Color: 1
Size: 294929 Color: 1
Size: 284870 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 423956 Color: 1
Size: 317901 Color: 1
Size: 258144 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 425427 Color: 1
Size: 295237 Color: 1
Size: 279337 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 395070 Color: 1
Size: 354171 Color: 1
Size: 250760 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 428182 Color: 1
Size: 321629 Color: 1
Size: 250190 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 357231 Color: 1
Size: 344679 Color: 1
Size: 298091 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 431608 Color: 1
Size: 288909 Color: 1
Size: 279484 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 431620 Color: 1
Size: 314886 Color: 1
Size: 253495 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 432008 Color: 1
Size: 306023 Color: 1
Size: 261970 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 446050 Color: 1
Size: 299488 Color: 1
Size: 254463 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 435052 Color: 1
Size: 312618 Color: 1
Size: 252331 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 435980 Color: 1
Size: 283866 Color: 1
Size: 280155 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 436882 Color: 1
Size: 304033 Color: 1
Size: 259086 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 437010 Color: 1
Size: 299540 Color: 1
Size: 263451 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 492005 Color: 1
Size: 257987 Color: 1
Size: 250009 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 438489 Color: 1
Size: 291332 Color: 1
Size: 270180 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 439239 Color: 1
Size: 309498 Color: 1
Size: 251264 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 460342 Color: 1
Size: 279533 Color: 1
Size: 260126 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 439616 Color: 1
Size: 284905 Color: 1
Size: 275480 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 440616 Color: 1
Size: 294789 Color: 1
Size: 264596 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 442872 Color: 1
Size: 304155 Color: 1
Size: 252974 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 442879 Color: 1
Size: 296512 Color: 1
Size: 260610 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 443128 Color: 1
Size: 292426 Color: 1
Size: 264447 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 445142 Color: 1
Size: 280810 Color: 1
Size: 274049 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 365897 Color: 1
Size: 354221 Color: 1
Size: 279883 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 445275 Color: 1
Size: 277574 Color: 1
Size: 277152 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 446728 Color: 1
Size: 295354 Color: 1
Size: 257919 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 447858 Color: 1
Size: 282974 Color: 1
Size: 269169 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 438718 Color: 1
Size: 280781 Color: 1
Size: 280502 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 454014 Color: 1
Size: 282230 Color: 1
Size: 263757 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 455642 Color: 1
Size: 287717 Color: 1
Size: 256642 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 459479 Color: 1
Size: 287326 Color: 1
Size: 253196 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 459741 Color: 1
Size: 279644 Color: 1
Size: 260616 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 459750 Color: 1
Size: 278972 Color: 1
Size: 261279 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 460135 Color: 1
Size: 288707 Color: 1
Size: 251159 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 463227 Color: 1
Size: 280457 Color: 1
Size: 256317 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 463731 Color: 1
Size: 276629 Color: 1
Size: 259641 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 465001 Color: 1
Size: 279003 Color: 1
Size: 255997 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 466661 Color: 1
Size: 283271 Color: 1
Size: 250069 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 468881 Color: 1
Size: 280471 Color: 1
Size: 250649 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 469984 Color: 1
Size: 271274 Color: 1
Size: 258743 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 473398 Color: 1
Size: 273187 Color: 1
Size: 253416 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 474700 Color: 1
Size: 272808 Color: 1
Size: 252493 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 482171 Color: 1
Size: 265698 Color: 1
Size: 252132 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 412751 Color: 1
Size: 333822 Color: 1
Size: 253428 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 486642 Color: 1
Size: 263026 Color: 1
Size: 250333 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 489699 Color: 1
Size: 257370 Color: 1
Size: 252932 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 490177 Color: 1
Size: 255653 Color: 1
Size: 254171 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 492177 Color: 1
Size: 255165 Color: 1
Size: 252659 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 495321 Color: 1
Size: 253269 Color: 1
Size: 251411 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 450464 Color: 1
Size: 284800 Color: 1
Size: 264737 Color: 0

Total size: 167000167
Total free space: 0

