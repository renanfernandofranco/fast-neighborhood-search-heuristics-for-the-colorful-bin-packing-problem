Capicity Bin: 1000001
Lower Bound: 229

Bins used: 230
Amount of Colors: 20

Bin 1: 1 of cap free
Amount of items: 3
Items: 
Size: 724853 Color: 3
Size: 171202 Color: 0
Size: 103945 Color: 6

Bin 2: 5 of cap free
Amount of items: 2
Items: 
Size: 726519 Color: 14
Size: 273477 Color: 17

Bin 3: 13 of cap free
Amount of items: 3
Items: 
Size: 464609 Color: 0
Size: 417894 Color: 8
Size: 117485 Color: 10

Bin 4: 15 of cap free
Amount of items: 3
Items: 
Size: 775779 Color: 5
Size: 113369 Color: 16
Size: 110838 Color: 18

Bin 5: 15 of cap free
Amount of items: 3
Items: 
Size: 777494 Color: 2
Size: 117450 Color: 9
Size: 105042 Color: 9

Bin 6: 15 of cap free
Amount of items: 2
Items: 
Size: 724276 Color: 13
Size: 275710 Color: 7

Bin 7: 17 of cap free
Amount of items: 3
Items: 
Size: 750146 Color: 5
Size: 142189 Color: 4
Size: 107649 Color: 19

Bin 8: 19 of cap free
Amount of items: 3
Items: 
Size: 739648 Color: 6
Size: 149298 Color: 18
Size: 111036 Color: 6

Bin 9: 25 of cap free
Amount of items: 2
Items: 
Size: 661704 Color: 7
Size: 338272 Color: 11

Bin 10: 26 of cap free
Amount of items: 3
Items: 
Size: 648701 Color: 16
Size: 186664 Color: 18
Size: 164610 Color: 19

Bin 11: 27 of cap free
Amount of items: 3
Items: 
Size: 760885 Color: 18
Size: 135075 Color: 13
Size: 104014 Color: 4

Bin 12: 33 of cap free
Amount of items: 3
Items: 
Size: 549544 Color: 5
Size: 292959 Color: 13
Size: 157465 Color: 12

Bin 13: 40 of cap free
Amount of items: 2
Items: 
Size: 655219 Color: 6
Size: 344742 Color: 8

Bin 14: 41 of cap free
Amount of items: 3
Items: 
Size: 550749 Color: 0
Size: 292662 Color: 15
Size: 156549 Color: 17

Bin 15: 41 of cap free
Amount of items: 2
Items: 
Size: 621120 Color: 7
Size: 378840 Color: 18

Bin 16: 46 of cap free
Amount of items: 2
Items: 
Size: 568788 Color: 14
Size: 431167 Color: 2

Bin 17: 47 of cap free
Amount of items: 2
Items: 
Size: 724778 Color: 3
Size: 275176 Color: 11

Bin 18: 49 of cap free
Amount of items: 3
Items: 
Size: 505548 Color: 13
Size: 295889 Color: 19
Size: 198515 Color: 1

Bin 19: 52 of cap free
Amount of items: 2
Items: 
Size: 757905 Color: 19
Size: 242044 Color: 17

Bin 20: 53 of cap free
Amount of items: 3
Items: 
Size: 716758 Color: 11
Size: 182388 Color: 10
Size: 100802 Color: 12

Bin 21: 63 of cap free
Amount of items: 2
Items: 
Size: 773328 Color: 9
Size: 226610 Color: 4

Bin 22: 67 of cap free
Amount of items: 3
Items: 
Size: 638107 Color: 12
Size: 190794 Color: 16
Size: 171033 Color: 4

Bin 23: 68 of cap free
Amount of items: 2
Items: 
Size: 583244 Color: 1
Size: 416689 Color: 6

Bin 24: 75 of cap free
Amount of items: 3
Items: 
Size: 681445 Color: 19
Size: 188589 Color: 6
Size: 129892 Color: 12

Bin 25: 79 of cap free
Amount of items: 2
Items: 
Size: 521777 Color: 11
Size: 478145 Color: 19

Bin 26: 83 of cap free
Amount of items: 3
Items: 
Size: 453836 Color: 19
Size: 421283 Color: 19
Size: 124799 Color: 1

Bin 27: 101 of cap free
Amount of items: 3
Items: 
Size: 710979 Color: 5
Size: 183071 Color: 9
Size: 105850 Color: 0

Bin 28: 101 of cap free
Amount of items: 2
Items: 
Size: 723175 Color: 7
Size: 276725 Color: 16

Bin 29: 121 of cap free
Amount of items: 2
Items: 
Size: 592128 Color: 8
Size: 407752 Color: 16

Bin 30: 125 of cap free
Amount of items: 3
Items: 
Size: 761212 Color: 7
Size: 131592 Color: 3
Size: 107072 Color: 7

Bin 31: 126 of cap free
Amount of items: 2
Items: 
Size: 560213 Color: 11
Size: 439662 Color: 1

Bin 32: 127 of cap free
Amount of items: 3
Items: 
Size: 772437 Color: 4
Size: 125116 Color: 7
Size: 102321 Color: 8

Bin 33: 133 of cap free
Amount of items: 2
Items: 
Size: 605572 Color: 16
Size: 394296 Color: 5

Bin 34: 134 of cap free
Amount of items: 2
Items: 
Size: 749895 Color: 9
Size: 249972 Color: 8

Bin 35: 154 of cap free
Amount of items: 2
Items: 
Size: 627638 Color: 13
Size: 372209 Color: 18

Bin 36: 183 of cap free
Amount of items: 3
Items: 
Size: 775801 Color: 2
Size: 116319 Color: 14
Size: 107698 Color: 9

Bin 37: 184 of cap free
Amount of items: 3
Items: 
Size: 723712 Color: 14
Size: 153186 Color: 15
Size: 122919 Color: 14

Bin 38: 205 of cap free
Amount of items: 3
Items: 
Size: 549801 Color: 8
Size: 298880 Color: 12
Size: 151115 Color: 14

Bin 39: 207 of cap free
Amount of items: 2
Items: 
Size: 653661 Color: 7
Size: 346133 Color: 3

Bin 40: 208 of cap free
Amount of items: 2
Items: 
Size: 771431 Color: 3
Size: 228362 Color: 17

Bin 41: 210 of cap free
Amount of items: 2
Items: 
Size: 593717 Color: 19
Size: 406074 Color: 13

Bin 42: 212 of cap free
Amount of items: 2
Items: 
Size: 584130 Color: 17
Size: 415659 Color: 15

Bin 43: 232 of cap free
Amount of items: 3
Items: 
Size: 761895 Color: 15
Size: 135703 Color: 13
Size: 102171 Color: 1

Bin 44: 242 of cap free
Amount of items: 3
Items: 
Size: 466730 Color: 4
Size: 420250 Color: 12
Size: 112779 Color: 8

Bin 45: 247 of cap free
Amount of items: 3
Items: 
Size: 777714 Color: 0
Size: 115933 Color: 13
Size: 106107 Color: 11

Bin 46: 259 of cap free
Amount of items: 2
Items: 
Size: 767456 Color: 11
Size: 232286 Color: 8

Bin 47: 263 of cap free
Amount of items: 2
Items: 
Size: 513698 Color: 14
Size: 486040 Color: 18

Bin 48: 276 of cap free
Amount of items: 2
Items: 
Size: 702186 Color: 19
Size: 297539 Color: 8

Bin 49: 285 of cap free
Amount of items: 3
Items: 
Size: 680518 Color: 12
Size: 182233 Color: 15
Size: 136965 Color: 12

Bin 50: 290 of cap free
Amount of items: 3
Items: 
Size: 461377 Color: 10
Size: 392363 Color: 13
Size: 145971 Color: 6

Bin 51: 293 of cap free
Amount of items: 2
Items: 
Size: 534443 Color: 12
Size: 465265 Color: 2

Bin 52: 296 of cap free
Amount of items: 3
Items: 
Size: 725236 Color: 2
Size: 150794 Color: 1
Size: 123675 Color: 5

Bin 53: 302 of cap free
Amount of items: 2
Items: 
Size: 662343 Color: 6
Size: 337356 Color: 4

Bin 54: 305 of cap free
Amount of items: 2
Items: 
Size: 730113 Color: 8
Size: 269583 Color: 16

Bin 55: 325 of cap free
Amount of items: 2
Items: 
Size: 526178 Color: 17
Size: 473498 Color: 19

Bin 56: 330 of cap free
Amount of items: 2
Items: 
Size: 654762 Color: 0
Size: 344909 Color: 3

Bin 57: 352 of cap free
Amount of items: 2
Items: 
Size: 776000 Color: 17
Size: 223649 Color: 0

Bin 58: 364 of cap free
Amount of items: 3
Items: 
Size: 653117 Color: 15
Size: 189996 Color: 8
Size: 156524 Color: 7

Bin 59: 386 of cap free
Amount of items: 2
Items: 
Size: 554640 Color: 9
Size: 444975 Color: 16

Bin 60: 417 of cap free
Amount of items: 2
Items: 
Size: 670316 Color: 14
Size: 329268 Color: 12

Bin 61: 419 of cap free
Amount of items: 2
Items: 
Size: 590089 Color: 19
Size: 409493 Color: 13

Bin 62: 443 of cap free
Amount of items: 3
Items: 
Size: 712188 Color: 6
Size: 183660 Color: 2
Size: 103710 Color: 6

Bin 63: 445 of cap free
Amount of items: 2
Items: 
Size: 569107 Color: 4
Size: 430449 Color: 7

Bin 64: 465 of cap free
Amount of items: 2
Items: 
Size: 763704 Color: 0
Size: 235832 Color: 12

Bin 65: 480 of cap free
Amount of items: 2
Items: 
Size: 783577 Color: 12
Size: 215944 Color: 1

Bin 66: 480 of cap free
Amount of items: 2
Items: 
Size: 796414 Color: 15
Size: 203107 Color: 7

Bin 67: 481 of cap free
Amount of items: 2
Items: 
Size: 707122 Color: 15
Size: 292398 Color: 3

Bin 68: 501 of cap free
Amount of items: 3
Items: 
Size: 737006 Color: 12
Size: 140663 Color: 11
Size: 121831 Color: 9

Bin 69: 513 of cap free
Amount of items: 2
Items: 
Size: 737942 Color: 3
Size: 261546 Color: 5

Bin 70: 513 of cap free
Amount of items: 3
Items: 
Size: 771744 Color: 6
Size: 118672 Color: 18
Size: 109072 Color: 0

Bin 71: 519 of cap free
Amount of items: 2
Items: 
Size: 541765 Color: 1
Size: 457717 Color: 19

Bin 72: 520 of cap free
Amount of items: 2
Items: 
Size: 719803 Color: 13
Size: 279678 Color: 5

Bin 73: 523 of cap free
Amount of items: 3
Items: 
Size: 638760 Color: 5
Size: 189822 Color: 0
Size: 170896 Color: 11

Bin 74: 530 of cap free
Amount of items: 2
Items: 
Size: 617328 Color: 1
Size: 382143 Color: 13

Bin 75: 537 of cap free
Amount of items: 2
Items: 
Size: 506381 Color: 18
Size: 493083 Color: 2

Bin 76: 554 of cap free
Amount of items: 2
Items: 
Size: 539063 Color: 2
Size: 460384 Color: 9

Bin 77: 559 of cap free
Amount of items: 3
Items: 
Size: 645201 Color: 0
Size: 188435 Color: 0
Size: 165806 Color: 19

Bin 78: 582 of cap free
Amount of items: 2
Items: 
Size: 610185 Color: 11
Size: 389234 Color: 7

Bin 79: 613 of cap free
Amount of items: 2
Items: 
Size: 678413 Color: 7
Size: 320975 Color: 5

Bin 80: 619 of cap free
Amount of items: 2
Items: 
Size: 698272 Color: 1
Size: 301110 Color: 5

Bin 81: 634 of cap free
Amount of items: 2
Items: 
Size: 663926 Color: 0
Size: 335441 Color: 16

Bin 82: 661 of cap free
Amount of items: 2
Items: 
Size: 669340 Color: 5
Size: 330000 Color: 13

Bin 83: 697 of cap free
Amount of items: 2
Items: 
Size: 532245 Color: 0
Size: 467059 Color: 3

Bin 84: 706 of cap free
Amount of items: 2
Items: 
Size: 654589 Color: 19
Size: 344706 Color: 18

Bin 85: 713 of cap free
Amount of items: 2
Items: 
Size: 651500 Color: 7
Size: 347788 Color: 1

Bin 86: 718 of cap free
Amount of items: 3
Items: 
Size: 714947 Color: 1
Size: 151744 Color: 10
Size: 132592 Color: 8

Bin 87: 740 of cap free
Amount of items: 2
Items: 
Size: 738742 Color: 7
Size: 260519 Color: 3

Bin 88: 748 of cap free
Amount of items: 2
Items: 
Size: 507296 Color: 1
Size: 491957 Color: 17

Bin 89: 755 of cap free
Amount of items: 2
Items: 
Size: 691249 Color: 19
Size: 307997 Color: 9

Bin 90: 768 of cap free
Amount of items: 2
Items: 
Size: 539094 Color: 1
Size: 460139 Color: 9

Bin 91: 786 of cap free
Amount of items: 2
Items: 
Size: 547738 Color: 17
Size: 451477 Color: 0

Bin 92: 802 of cap free
Amount of items: 2
Items: 
Size: 712180 Color: 13
Size: 287019 Color: 6

Bin 93: 803 of cap free
Amount of items: 2
Items: 
Size: 649241 Color: 15
Size: 349957 Color: 4

Bin 94: 842 of cap free
Amount of items: 3
Items: 
Size: 715770 Color: 18
Size: 143023 Color: 6
Size: 140366 Color: 1

Bin 95: 889 of cap free
Amount of items: 2
Items: 
Size: 616941 Color: 5
Size: 382171 Color: 7

Bin 96: 894 of cap free
Amount of items: 2
Items: 
Size: 685419 Color: 19
Size: 313688 Color: 14

Bin 97: 900 of cap free
Amount of items: 2
Items: 
Size: 788398 Color: 15
Size: 210703 Color: 9

Bin 98: 932 of cap free
Amount of items: 2
Items: 
Size: 571657 Color: 4
Size: 427412 Color: 8

Bin 99: 940 of cap free
Amount of items: 2
Items: 
Size: 641649 Color: 18
Size: 357412 Color: 8

Bin 100: 941 of cap free
Amount of items: 2
Items: 
Size: 797328 Color: 18
Size: 201732 Color: 4

Bin 101: 956 of cap free
Amount of items: 2
Items: 
Size: 580203 Color: 17
Size: 418842 Color: 9

Bin 102: 983 of cap free
Amount of items: 3
Items: 
Size: 647681 Color: 2
Size: 176170 Color: 11
Size: 175167 Color: 4

Bin 103: 988 of cap free
Amount of items: 2
Items: 
Size: 695883 Color: 15
Size: 303130 Color: 12

Bin 104: 1010 of cap free
Amount of items: 2
Items: 
Size: 614287 Color: 7
Size: 384704 Color: 1

Bin 105: 1016 of cap free
Amount of items: 2
Items: 
Size: 713503 Color: 2
Size: 285482 Color: 17

Bin 106: 1042 of cap free
Amount of items: 2
Items: 
Size: 771003 Color: 0
Size: 227956 Color: 16

Bin 107: 1051 of cap free
Amount of items: 2
Items: 
Size: 756263 Color: 8
Size: 242687 Color: 3

Bin 108: 1128 of cap free
Amount of items: 3
Items: 
Size: 651683 Color: 16
Size: 191058 Color: 12
Size: 156132 Color: 19

Bin 109: 1193 of cap free
Amount of items: 2
Items: 
Size: 780739 Color: 3
Size: 218069 Color: 8

Bin 110: 1207 of cap free
Amount of items: 2
Items: 
Size: 565736 Color: 8
Size: 433058 Color: 10

Bin 111: 1210 of cap free
Amount of items: 2
Items: 
Size: 535227 Color: 19
Size: 463564 Color: 14

Bin 112: 1213 of cap free
Amount of items: 2
Items: 
Size: 790472 Color: 18
Size: 208316 Color: 15

Bin 113: 1230 of cap free
Amount of items: 2
Items: 
Size: 510045 Color: 17
Size: 488726 Color: 0

Bin 114: 1231 of cap free
Amount of items: 2
Items: 
Size: 787233 Color: 0
Size: 211537 Color: 15

Bin 115: 1232 of cap free
Amount of items: 2
Items: 
Size: 605903 Color: 4
Size: 392866 Color: 19

Bin 116: 1266 of cap free
Amount of items: 2
Items: 
Size: 719943 Color: 0
Size: 278792 Color: 13

Bin 117: 1278 of cap free
Amount of items: 2
Items: 
Size: 686954 Color: 15
Size: 311769 Color: 19

Bin 118: 1354 of cap free
Amount of items: 2
Items: 
Size: 634814 Color: 0
Size: 363833 Color: 12

Bin 119: 1358 of cap free
Amount of items: 2
Items: 
Size: 606433 Color: 2
Size: 392210 Color: 12

Bin 120: 1411 of cap free
Amount of items: 2
Items: 
Size: 528108 Color: 1
Size: 470482 Color: 9

Bin 121: 1413 of cap free
Amount of items: 2
Items: 
Size: 634504 Color: 18
Size: 364084 Color: 16

Bin 122: 1508 of cap free
Amount of items: 2
Items: 
Size: 727861 Color: 14
Size: 270632 Color: 8

Bin 123: 1592 of cap free
Amount of items: 2
Items: 
Size: 609676 Color: 3
Size: 388733 Color: 11

Bin 124: 1639 of cap free
Amount of items: 2
Items: 
Size: 774069 Color: 13
Size: 224293 Color: 18

Bin 125: 1667 of cap free
Amount of items: 2
Items: 
Size: 687178 Color: 16
Size: 311156 Color: 2

Bin 126: 1680 of cap free
Amount of items: 2
Items: 
Size: 733352 Color: 6
Size: 264969 Color: 13

Bin 127: 1702 of cap free
Amount of items: 2
Items: 
Size: 718031 Color: 18
Size: 280268 Color: 10

Bin 128: 1707 of cap free
Amount of items: 2
Items: 
Size: 765453 Color: 5
Size: 232841 Color: 16

Bin 129: 1766 of cap free
Amount of items: 2
Items: 
Size: 596310 Color: 12
Size: 401925 Color: 1

Bin 130: 1771 of cap free
Amount of items: 2
Items: 
Size: 540337 Color: 5
Size: 457893 Color: 7

Bin 131: 1837 of cap free
Amount of items: 2
Items: 
Size: 742949 Color: 9
Size: 255215 Color: 0

Bin 132: 1848 of cap free
Amount of items: 2
Items: 
Size: 683447 Color: 16
Size: 314706 Color: 15

Bin 133: 1853 of cap free
Amount of items: 2
Items: 
Size: 587048 Color: 0
Size: 411100 Color: 14

Bin 134: 1884 of cap free
Amount of items: 2
Items: 
Size: 584833 Color: 9
Size: 413284 Color: 6

Bin 135: 1947 of cap free
Amount of items: 2
Items: 
Size: 684750 Color: 2
Size: 313304 Color: 1

Bin 136: 1982 of cap free
Amount of items: 2
Items: 
Size: 528773 Color: 3
Size: 469246 Color: 5

Bin 137: 2004 of cap free
Amount of items: 2
Items: 
Size: 644812 Color: 12
Size: 353185 Color: 0

Bin 138: 2009 of cap free
Amount of items: 2
Items: 
Size: 639361 Color: 19
Size: 358631 Color: 12

Bin 139: 2019 of cap free
Amount of items: 2
Items: 
Size: 584807 Color: 16
Size: 413175 Color: 5

Bin 140: 2042 of cap free
Amount of items: 2
Items: 
Size: 740844 Color: 16
Size: 257115 Color: 0

Bin 141: 2122 of cap free
Amount of items: 2
Items: 
Size: 597331 Color: 13
Size: 400548 Color: 9

Bin 142: 2146 of cap free
Amount of items: 2
Items: 
Size: 538344 Color: 10
Size: 459511 Color: 17

Bin 143: 2237 of cap free
Amount of items: 2
Items: 
Size: 739623 Color: 3
Size: 258141 Color: 19

Bin 144: 2262 of cap free
Amount of items: 2
Items: 
Size: 587350 Color: 7
Size: 410389 Color: 3

Bin 145: 2289 of cap free
Amount of items: 2
Items: 
Size: 521953 Color: 12
Size: 475759 Color: 19

Bin 146: 2314 of cap free
Amount of items: 2
Items: 
Size: 561551 Color: 15
Size: 436136 Color: 9

Bin 147: 2337 of cap free
Amount of items: 2
Items: 
Size: 510102 Color: 10
Size: 487562 Color: 11

Bin 148: 2459 of cap free
Amount of items: 2
Items: 
Size: 662901 Color: 10
Size: 334641 Color: 7

Bin 149: 2471 of cap free
Amount of items: 2
Items: 
Size: 565595 Color: 6
Size: 431935 Color: 7

Bin 150: 2502 of cap free
Amount of items: 3
Items: 
Size: 704652 Color: 16
Size: 159032 Color: 4
Size: 133815 Color: 18

Bin 151: 2546 of cap free
Amount of items: 2
Items: 
Size: 743394 Color: 2
Size: 254061 Color: 10

Bin 152: 2567 of cap free
Amount of items: 2
Items: 
Size: 730897 Color: 8
Size: 266537 Color: 1

Bin 153: 2619 of cap free
Amount of items: 2
Items: 
Size: 512370 Color: 11
Size: 485012 Color: 17

Bin 154: 2653 of cap free
Amount of items: 2
Items: 
Size: 677669 Color: 0
Size: 319679 Color: 1

Bin 155: 2703 of cap free
Amount of items: 2
Items: 
Size: 685217 Color: 18
Size: 312081 Color: 15

Bin 156: 2731 of cap free
Amount of items: 2
Items: 
Size: 569504 Color: 13
Size: 427766 Color: 8

Bin 157: 2732 of cap free
Amount of items: 2
Items: 
Size: 682125 Color: 6
Size: 315144 Color: 0

Bin 158: 2784 of cap free
Amount of items: 2
Items: 
Size: 550502 Color: 15
Size: 446715 Color: 14

Bin 159: 2812 of cap free
Amount of items: 2
Items: 
Size: 501728 Color: 1
Size: 495461 Color: 14

Bin 160: 2830 of cap free
Amount of items: 2
Items: 
Size: 693421 Color: 4
Size: 303750 Color: 16

Bin 161: 2851 of cap free
Amount of items: 2
Items: 
Size: 505522 Color: 16
Size: 491628 Color: 2

Bin 162: 2869 of cap free
Amount of items: 2
Items: 
Size: 751299 Color: 11
Size: 245833 Color: 12

Bin 163: 2876 of cap free
Amount of items: 2
Items: 
Size: 510008 Color: 5
Size: 487117 Color: 10

Bin 164: 2897 of cap free
Amount of items: 2
Items: 
Size: 516569 Color: 0
Size: 480535 Color: 4

Bin 165: 2901 of cap free
Amount of items: 2
Items: 
Size: 789106 Color: 2
Size: 207994 Color: 16

Bin 166: 2953 of cap free
Amount of items: 2
Items: 
Size: 654093 Color: 1
Size: 342955 Color: 15

Bin 167: 3009 of cap free
Amount of items: 2
Items: 
Size: 726644 Color: 16
Size: 270348 Color: 5

Bin 168: 3029 of cap free
Amount of items: 2
Items: 
Size: 765427 Color: 6
Size: 231545 Color: 14

Bin 169: 3167 of cap free
Amount of items: 2
Items: 
Size: 757309 Color: 8
Size: 239525 Color: 10

Bin 170: 3198 of cap free
Amount of items: 2
Items: 
Size: 515139 Color: 18
Size: 481664 Color: 9

Bin 171: 3236 of cap free
Amount of items: 2
Items: 
Size: 739613 Color: 1
Size: 257152 Color: 11

Bin 172: 3306 of cap free
Amount of items: 2
Items: 
Size: 656840 Color: 8
Size: 339855 Color: 4

Bin 173: 3369 of cap free
Amount of items: 2
Items: 
Size: 727342 Color: 9
Size: 269290 Color: 8

Bin 174: 3448 of cap free
Amount of items: 3
Items: 
Size: 738083 Color: 4
Size: 149908 Color: 10
Size: 108562 Color: 0

Bin 175: 3531 of cap free
Amount of items: 2
Items: 
Size: 691728 Color: 13
Size: 304742 Color: 5

Bin 176: 3559 of cap free
Amount of items: 2
Items: 
Size: 575227 Color: 13
Size: 421215 Color: 6

Bin 177: 3709 of cap free
Amount of items: 2
Items: 
Size: 616820 Color: 19
Size: 379472 Color: 1

Bin 178: 3911 of cap free
Amount of items: 2
Items: 
Size: 655350 Color: 14
Size: 340740 Color: 17

Bin 179: 3933 of cap free
Amount of items: 2
Items: 
Size: 596667 Color: 1
Size: 399401 Color: 0

Bin 180: 3944 of cap free
Amount of items: 2
Items: 
Size: 575216 Color: 0
Size: 420841 Color: 2

Bin 181: 3963 of cap free
Amount of items: 2
Items: 
Size: 560390 Color: 6
Size: 435648 Color: 18

Bin 182: 3975 of cap free
Amount of items: 2
Items: 
Size: 553395 Color: 1
Size: 442631 Color: 0

Bin 183: 4074 of cap free
Amount of items: 2
Items: 
Size: 520095 Color: 16
Size: 475832 Color: 17

Bin 184: 4076 of cap free
Amount of items: 2
Items: 
Size: 560171 Color: 3
Size: 435754 Color: 5

Bin 185: 4112 of cap free
Amount of items: 2
Items: 
Size: 585960 Color: 4
Size: 409929 Color: 18

Bin 186: 4176 of cap free
Amount of items: 2
Items: 
Size: 616705 Color: 1
Size: 379120 Color: 0

Bin 187: 4190 of cap free
Amount of items: 2
Items: 
Size: 743469 Color: 10
Size: 252342 Color: 5

Bin 188: 4196 of cap free
Amount of items: 2
Items: 
Size: 595722 Color: 16
Size: 400083 Color: 8

Bin 189: 4314 of cap free
Amount of items: 2
Items: 
Size: 631042 Color: 9
Size: 364645 Color: 7

Bin 190: 4397 of cap free
Amount of items: 2
Items: 
Size: 728274 Color: 5
Size: 267330 Color: 8

Bin 191: 4424 of cap free
Amount of items: 2
Items: 
Size: 540658 Color: 12
Size: 454919 Color: 17

Bin 192: 4617 of cap free
Amount of items: 2
Items: 
Size: 764970 Color: 16
Size: 230414 Color: 15

Bin 193: 4688 of cap free
Amount of items: 2
Items: 
Size: 665143 Color: 7
Size: 330170 Color: 5

Bin 194: 4730 of cap free
Amount of items: 2
Items: 
Size: 509347 Color: 19
Size: 485924 Color: 12

Bin 195: 5058 of cap free
Amount of items: 2
Items: 
Size: 624568 Color: 4
Size: 370375 Color: 16

Bin 196: 5335 of cap free
Amount of items: 2
Items: 
Size: 754631 Color: 15
Size: 240035 Color: 2

Bin 197: 5366 of cap free
Amount of items: 2
Items: 
Size: 726479 Color: 18
Size: 268156 Color: 15

Bin 198: 5393 of cap free
Amount of items: 2
Items: 
Size: 608737 Color: 9
Size: 385871 Color: 16

Bin 199: 5434 of cap free
Amount of items: 2
Items: 
Size: 559802 Color: 14
Size: 434765 Color: 8

Bin 200: 5464 of cap free
Amount of items: 2
Items: 
Size: 525464 Color: 18
Size: 469073 Color: 15

Bin 201: 5651 of cap free
Amount of items: 2
Items: 
Size: 630525 Color: 4
Size: 363825 Color: 6

Bin 202: 5835 of cap free
Amount of items: 2
Items: 
Size: 614345 Color: 19
Size: 379821 Color: 13

Bin 203: 5959 of cap free
Amount of items: 2
Items: 
Size: 624147 Color: 17
Size: 369895 Color: 3

Bin 204: 5998 of cap free
Amount of items: 2
Items: 
Size: 615038 Color: 2
Size: 378965 Color: 9

Bin 205: 6528 of cap free
Amount of items: 3
Items: 
Size: 723390 Color: 10
Size: 154100 Color: 9
Size: 115983 Color: 6

Bin 206: 6537 of cap free
Amount of items: 2
Items: 
Size: 553370 Color: 0
Size: 440094 Color: 2

Bin 207: 6549 of cap free
Amount of items: 2
Items: 
Size: 620598 Color: 3
Size: 372854 Color: 4

Bin 208: 7355 of cap free
Amount of items: 2
Items: 
Size: 661581 Color: 3
Size: 331065 Color: 0

Bin 209: 7824 of cap free
Amount of items: 2
Items: 
Size: 689651 Color: 4
Size: 302526 Color: 7

Bin 210: 8417 of cap free
Amount of items: 2
Items: 
Size: 726316 Color: 2
Size: 265268 Color: 4

Bin 211: 8780 of cap free
Amount of items: 2
Items: 
Size: 514359 Color: 10
Size: 476862 Color: 11

Bin 212: 8781 of cap free
Amount of items: 2
Items: 
Size: 559044 Color: 12
Size: 432176 Color: 16

Bin 213: 9718 of cap free
Amount of items: 2
Items: 
Size: 788843 Color: 4
Size: 201440 Color: 11

Bin 214: 10031 of cap free
Amount of items: 2
Items: 
Size: 593760 Color: 7
Size: 396210 Color: 17

Bin 215: 10682 of cap free
Amount of items: 2
Items: 
Size: 509724 Color: 2
Size: 479595 Color: 9

Bin 216: 10759 of cap free
Amount of items: 2
Items: 
Size: 688954 Color: 10
Size: 300288 Color: 19

Bin 217: 10769 of cap free
Amount of items: 2
Items: 
Size: 652962 Color: 7
Size: 336270 Color: 10

Bin 218: 13202 of cap free
Amount of items: 2
Items: 
Size: 785559 Color: 19
Size: 201240 Color: 5

Bin 219: 13380 of cap free
Amount of items: 2
Items: 
Size: 569095 Color: 14
Size: 417526 Color: 6

Bin 220: 14384 of cap free
Amount of items: 2
Items: 
Size: 593389 Color: 7
Size: 392228 Color: 11

Bin 221: 14854 of cap free
Amount of items: 2
Items: 
Size: 653616 Color: 6
Size: 331531 Color: 12

Bin 222: 15571 of cap free
Amount of items: 2
Items: 
Size: 784656 Color: 7
Size: 199774 Color: 6

Bin 223: 19919 of cap free
Amount of items: 2
Items: 
Size: 783295 Color: 11
Size: 196787 Color: 15

Bin 224: 22486 of cap free
Amount of items: 2
Items: 
Size: 504382 Color: 19
Size: 473133 Color: 4

Bin 225: 72051 of cap free
Amount of items: 2
Items: 
Size: 762968 Color: 18
Size: 164982 Color: 19

Bin 226: 84545 of cap free
Amount of items: 2
Items: 
Size: 499467 Color: 17
Size: 415989 Color: 18

Bin 227: 148339 of cap free
Amount of items: 3
Items: 
Size: 323795 Color: 8
Size: 290120 Color: 10
Size: 237747 Color: 12

Bin 228: 150802 of cap free
Amount of items: 2
Items: 
Size: 552067 Color: 11
Size: 297132 Color: 12

Bin 229: 161064 of cap free
Amount of items: 2
Items: 
Size: 463858 Color: 17
Size: 375079 Color: 2

Bin 230: 164706 of cap free
Amount of items: 2
Items: 
Size: 652015 Color: 11
Size: 183280 Color: 2

Total size: 228666558
Total free space: 1333672

