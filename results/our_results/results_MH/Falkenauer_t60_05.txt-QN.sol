Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 59
Size: 254 Color: 5
Size: 250 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 47
Size: 328 Color: 32
Size: 283 Color: 23

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 56
Size: 265 Color: 11
Size: 252 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 44
Size: 335 Color: 34
Size: 285 Color: 24

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 38
Size: 340 Color: 35
Size: 305 Color: 30

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 42
Size: 327 Color: 31
Size: 301 Color: 28

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 58
Size: 259 Color: 6
Size: 252 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 52
Size: 286 Color: 25
Size: 281 Color: 20

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 51
Size: 290 Color: 26
Size: 278 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 41
Size: 358 Color: 39
Size: 281 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 50
Size: 296 Color: 27
Size: 282 Color: 21

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 55
Size: 269 Color: 13
Size: 262 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 46
Size: 360 Color: 40
Size: 252 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 53
Size: 276 Color: 15
Size: 262 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 48
Size: 334 Color: 33
Size: 270 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 45
Size: 352 Color: 37
Size: 268 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 57
Size: 264 Color: 10
Size: 252 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 54
Size: 276 Color: 16
Size: 261 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 43
Size: 281 Color: 18
Size: 347 Color: 36

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 302 Color: 29
Size: 282 Color: 22
Size: 416 Color: 49

Total size: 20000
Total free space: 0

