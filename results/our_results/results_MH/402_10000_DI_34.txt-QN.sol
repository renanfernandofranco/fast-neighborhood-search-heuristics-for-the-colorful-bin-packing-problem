Capicity Bin: 7744
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3840 Color: 272
Size: 1056 Color: 182
Size: 968 Color: 172
Size: 608 Color: 131
Size: 488 Color: 122
Size: 416 Color: 110
Size: 368 Color: 99

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 6716 Color: 376
Size: 860 Color: 160
Size: 112 Color: 10
Size: 56 Color: 4

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 6192 Color: 331
Size: 1168 Color: 195
Size: 168 Color: 39
Size: 128 Color: 14
Size: 88 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 395
Size: 716 Color: 144
Size: 136 Color: 21

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4827 Color: 290
Size: 2431 Color: 251
Size: 486 Color: 121

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6852 Color: 392
Size: 748 Color: 146
Size: 144 Color: 26

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5948 Color: 320
Size: 1500 Color: 220
Size: 296 Color: 87

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4834 Color: 293
Size: 2426 Color: 249
Size: 484 Color: 120

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 349
Size: 1146 Color: 191
Size: 228 Color: 65

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6238 Color: 333
Size: 1258 Color: 207
Size: 248 Color: 78

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5743 Color: 312
Size: 1669 Color: 229
Size: 332 Color: 95

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6933 Color: 398
Size: 677 Color: 141
Size: 134 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5753 Color: 313
Size: 1661 Color: 228
Size: 330 Color: 93

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 280
Size: 3084 Color: 263
Size: 416 Color: 108

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4642 Color: 289
Size: 2586 Color: 253
Size: 516 Color: 124

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6333 Color: 346
Size: 1177 Color: 196
Size: 234 Color: 68

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5189 Color: 297
Size: 2131 Color: 245
Size: 424 Color: 111

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5493 Color: 306
Size: 1877 Color: 236
Size: 374 Color: 103

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 388
Size: 772 Color: 150
Size: 152 Color: 31

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4393 Color: 281
Size: 2793 Color: 259
Size: 558 Color: 129

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6260 Color: 335
Size: 1244 Color: 205
Size: 240 Color: 74

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5412 Color: 303
Size: 1948 Color: 239
Size: 384 Color: 104

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6830 Color: 390
Size: 762 Color: 149
Size: 152 Color: 30

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5198 Color: 299
Size: 2122 Color: 243
Size: 424 Color: 112

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6305 Color: 340
Size: 1201 Color: 199
Size: 238 Color: 72

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 300
Size: 2108 Color: 242
Size: 416 Color: 109

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4831 Color: 291
Size: 2429 Color: 250
Size: 484 Color: 119

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 394
Size: 756 Color: 147
Size: 112 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5662 Color: 310
Size: 1738 Color: 232
Size: 344 Color: 96

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6220 Color: 332
Size: 1276 Color: 208
Size: 248 Color: 77

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4994 Color: 296
Size: 2294 Color: 246
Size: 456 Color: 115

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6806 Color: 386
Size: 782 Color: 152
Size: 156 Color: 33

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4414 Color: 284
Size: 2778 Color: 257
Size: 552 Color: 127

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6611 Color: 364
Size: 1001 Color: 175
Size: 132 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6835 Color: 391
Size: 759 Color: 148
Size: 150 Color: 29

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4172 Color: 279
Size: 2980 Color: 261
Size: 592 Color: 130

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6285 Color: 338
Size: 1217 Color: 202
Size: 242 Color: 75

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6678 Color: 373
Size: 890 Color: 162
Size: 176 Color: 44

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 387
Size: 933 Color: 168
Size: 2 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5532 Color: 308
Size: 1844 Color: 234
Size: 368 Color: 100

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5191 Color: 298
Size: 2129 Color: 244
Size: 424 Color: 113

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5505 Color: 307
Size: 1867 Color: 235
Size: 372 Color: 101

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6532 Color: 359
Size: 1012 Color: 178
Size: 200 Color: 53

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 326
Size: 1386 Color: 215
Size: 276 Color: 83

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6958 Color: 401
Size: 658 Color: 138
Size: 128 Color: 13

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 317
Size: 1572 Color: 224
Size: 312 Color: 90

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 347
Size: 1162 Color: 194
Size: 228 Color: 64

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 389
Size: 776 Color: 151
Size: 144 Color: 25

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 311
Size: 1724 Color: 231
Size: 344 Color: 97

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 336
Size: 1306 Color: 210
Size: 164 Color: 37

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 362
Size: 994 Color: 174
Size: 196 Color: 50

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 3876 Color: 276
Size: 3396 Color: 271
Size: 472 Color: 117

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 3882 Color: 277
Size: 3222 Color: 266
Size: 640 Color: 133

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 365
Size: 948 Color: 170
Size: 184 Color: 49

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5989 Color: 324
Size: 1463 Color: 216
Size: 292 Color: 85

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6778 Color: 382
Size: 806 Color: 155
Size: 160 Color: 36

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4588 Color: 287
Size: 3068 Color: 262
Size: 88 Color: 7

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6314 Color: 345
Size: 1194 Color: 197
Size: 236 Color: 70

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6903 Color: 396
Size: 701 Color: 143
Size: 140 Color: 23

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6476 Color: 356
Size: 1060 Color: 184
Size: 208 Color: 57

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 400
Size: 662 Color: 139
Size: 132 Color: 18

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6159 Color: 330
Size: 1321 Color: 211
Size: 264 Color: 79

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 383
Size: 801 Color: 154
Size: 160 Color: 34

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6788 Color: 384
Size: 900 Color: 164
Size: 56 Color: 5

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 4832 Color: 292
Size: 2440 Color: 252
Size: 472 Color: 118

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4628 Color: 288
Size: 2604 Color: 254
Size: 512 Color: 123

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 319
Size: 1538 Color: 221
Size: 304 Color: 88

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 328
Size: 1364 Color: 213
Size: 272 Color: 81

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6717 Color: 377
Size: 857 Color: 159
Size: 170 Color: 42

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 301
Size: 2052 Color: 241
Size: 408 Color: 107

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 294
Size: 2364 Color: 248
Size: 464 Color: 116

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 357
Size: 1031 Color: 181
Size: 206 Color: 56

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6058 Color: 325
Size: 1558 Color: 222
Size: 128 Color: 15

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6922 Color: 397
Size: 686 Color: 142
Size: 136 Color: 20

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 4397 Color: 282
Size: 2791 Color: 258
Size: 556 Color: 128

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6143 Color: 329
Size: 1335 Color: 212
Size: 266 Color: 80

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5828 Color: 316
Size: 1604 Color: 225
Size: 312 Color: 91

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 3874 Color: 274
Size: 3226 Color: 268
Size: 644 Color: 136

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 295
Size: 2300 Color: 247
Size: 456 Color: 114

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 374
Size: 876 Color: 161
Size: 168 Color: 40

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 352
Size: 1114 Color: 188
Size: 220 Color: 61

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6420 Color: 354
Size: 1108 Color: 187
Size: 216 Color: 58

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 3884 Color: 278
Size: 3220 Color: 265
Size: 640 Color: 132

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6417 Color: 353
Size: 1107 Color: 186
Size: 220 Color: 62

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 393
Size: 742 Color: 145
Size: 144 Color: 24

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5866 Color: 318
Size: 1566 Color: 223
Size: 312 Color: 89

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 337
Size: 1228 Color: 203
Size: 240 Color: 73

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 342
Size: 1204 Color: 201
Size: 232 Color: 67

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6745 Color: 379
Size: 967 Color: 171
Size: 32 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5489 Color: 305
Size: 1881 Color: 237
Size: 374 Color: 102

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6442 Color: 355
Size: 1086 Color: 185
Size: 216 Color: 59

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6617 Color: 366
Size: 989 Color: 173
Size: 138 Color: 22

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6734 Color: 378
Size: 842 Color: 158
Size: 168 Color: 41

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 371
Size: 908 Color: 165
Size: 176 Color: 43

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 351
Size: 1122 Color: 189
Size: 220 Color: 60

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 381
Size: 820 Color: 156
Size: 160 Color: 35

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 3873 Color: 273
Size: 3227 Color: 269
Size: 644 Color: 134

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6258 Color: 334
Size: 1242 Color: 204
Size: 244 Color: 76

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5346 Color: 302
Size: 2002 Color: 240
Size: 396 Color: 106

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 363
Size: 1154 Color: 192
Size: 28 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6804 Color: 385
Size: 788 Color: 153
Size: 152 Color: 32

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 4398 Color: 283
Size: 3142 Color: 264
Size: 204 Color: 55

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6645 Color: 369
Size: 917 Color: 167
Size: 182 Color: 47

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 360
Size: 1009 Color: 176
Size: 200 Color: 52

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5434 Color: 304
Size: 1926 Color: 238
Size: 384 Color: 105

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 3392 Color: 270
Size: 2872 Color: 260
Size: 1480 Color: 219

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6357 Color: 348
Size: 1157 Color: 193
Size: 230 Color: 66

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6538 Color: 361
Size: 1058 Color: 183
Size: 148 Color: 28

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6106 Color: 327
Size: 1366 Color: 214
Size: 272 Color: 82

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 5977 Color: 322
Size: 1675 Color: 230
Size: 92 Color: 8

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6948 Color: 399
Size: 668 Color: 140
Size: 128 Color: 12

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 4500 Color: 286
Size: 2708 Color: 255
Size: 536 Color: 126

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6753 Color: 380
Size: 827 Color: 157
Size: 164 Color: 38

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 339
Size: 1245 Color: 206
Size: 198 Color: 51

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 285
Size: 2724 Color: 256
Size: 536 Color: 125

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 5638 Color: 309
Size: 1758 Color: 233
Size: 348 Color: 98

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5757 Color: 314
Size: 1657 Color: 227
Size: 330 Color: 94

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 368
Size: 934 Color: 169
Size: 184 Color: 48

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 341
Size: 1202 Color: 200
Size: 236 Color: 69

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 367
Size: 1014 Color: 179
Size: 112 Color: 9

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 323
Size: 1476 Color: 217
Size: 288 Color: 84

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6309 Color: 343
Size: 1197 Color: 198
Size: 238 Color: 71

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5769 Color: 315
Size: 1647 Color: 226
Size: 328 Color: 92

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6523 Color: 358
Size: 1019 Color: 180
Size: 202 Color: 54

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 402
Size: 653 Color: 137
Size: 130 Color: 16

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6701 Color: 375
Size: 1009 Color: 177
Size: 34 Color: 3

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6377 Color: 350
Size: 1141 Color: 190
Size: 226 Color: 63

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 5973 Color: 321
Size: 1477 Color: 218
Size: 294 Color: 86

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 3875 Color: 275
Size: 3225 Color: 267
Size: 644 Color: 135

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6649 Color: 370
Size: 913 Color: 166
Size: 182 Color: 46

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6313 Color: 344
Size: 1285 Color: 209
Size: 146 Color: 27

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6673 Color: 372
Size: 893 Color: 163
Size: 178 Color: 45

Total size: 1022208
Total free space: 0

