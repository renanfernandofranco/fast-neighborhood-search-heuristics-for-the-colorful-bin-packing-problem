Capicity Bin: 15632
Lower Bound: 198

Bins used: 198
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 7728 Color: 1
Size: 2352 Color: 1
Size: 2192 Color: 0
Size: 1296 Color: 0
Size: 944 Color: 1
Size: 736 Color: 0
Size: 288 Color: 0
Size: 96 Color: 1

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 8807 Color: 0
Size: 5689 Color: 1
Size: 864 Color: 0
Size: 272 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 1
Size: 3588 Color: 1
Size: 712 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10056 Color: 0
Size: 4648 Color: 0
Size: 928 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12612 Color: 1
Size: 2668 Color: 1
Size: 352 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 0
Size: 1316 Color: 0
Size: 256 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 1
Size: 1852 Color: 0
Size: 280 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11994 Color: 1
Size: 3034 Color: 0
Size: 604 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12895 Color: 1
Size: 2281 Color: 0
Size: 456 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 1
Size: 1520 Color: 1
Size: 304 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13876 Color: 0
Size: 1468 Color: 0
Size: 288 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11973 Color: 1
Size: 3417 Color: 1
Size: 242 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12979 Color: 0
Size: 2211 Color: 0
Size: 442 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12778 Color: 1
Size: 2382 Color: 0
Size: 472 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11060 Color: 1
Size: 3812 Color: 1
Size: 760 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 0
Size: 2936 Color: 0
Size: 416 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 1
Size: 1932 Color: 0
Size: 384 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13102 Color: 1
Size: 2110 Color: 0
Size: 420 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 0
Size: 1470 Color: 1
Size: 292 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 10259 Color: 1
Size: 4479 Color: 1
Size: 894 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 1
Size: 5688 Color: 0
Size: 1136 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 0
Size: 5276 Color: 1
Size: 1048 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12578 Color: 0
Size: 2546 Color: 0
Size: 508 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13430 Color: 0
Size: 1838 Color: 0
Size: 364 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13118 Color: 0
Size: 2402 Color: 0
Size: 112 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13708 Color: 0
Size: 1604 Color: 0
Size: 320 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 1
Size: 1436 Color: 1
Size: 280 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11784 Color: 1
Size: 3208 Color: 1
Size: 640 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 8204 Color: 1
Size: 6196 Color: 1
Size: 1232 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 1
Size: 1564 Color: 1
Size: 304 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13639 Color: 1
Size: 1661 Color: 1
Size: 332 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13294 Color: 1
Size: 1982 Color: 0
Size: 356 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 1
Size: 5036 Color: 0
Size: 1000 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 1
Size: 2124 Color: 1
Size: 416 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11615 Color: 0
Size: 3349 Color: 1
Size: 668 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 8898 Color: 0
Size: 5770 Color: 1
Size: 964 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 9516 Color: 1
Size: 5100 Color: 0
Size: 1016 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 1
Size: 2360 Color: 0
Size: 464 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 9482 Color: 1
Size: 5126 Color: 1
Size: 1024 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 9619 Color: 1
Size: 5011 Color: 1
Size: 1002 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 11349 Color: 1
Size: 3571 Color: 1
Size: 712 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 8900 Color: 1
Size: 5612 Color: 1
Size: 1120 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 11364 Color: 0
Size: 3564 Color: 0
Size: 704 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 1
Size: 1768 Color: 0
Size: 352 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 0
Size: 2030 Color: 1
Size: 404 Color: 0

Bin 46: 0 of cap free
Amount of items: 4
Items: 
Size: 12560 Color: 1
Size: 1872 Color: 0
Size: 1136 Color: 1
Size: 64 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 11262 Color: 1
Size: 3642 Color: 0
Size: 728 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 7821 Color: 0
Size: 6511 Color: 0
Size: 1300 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14014 Color: 0
Size: 1350 Color: 0
Size: 268 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 1
Size: 1348 Color: 1
Size: 264 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12649 Color: 1
Size: 2487 Color: 1
Size: 496 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 1
Size: 5256 Color: 1
Size: 1040 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13360 Color: 1
Size: 1920 Color: 1
Size: 352 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13094 Color: 1
Size: 2118 Color: 1
Size: 420 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6832 Color: 0
Size: 5808 Color: 0
Size: 2992 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13446 Color: 1
Size: 1822 Color: 0
Size: 364 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12722 Color: 0
Size: 2426 Color: 0
Size: 484 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 11911 Color: 1
Size: 3101 Color: 0
Size: 620 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 12074 Color: 1
Size: 2966 Color: 0
Size: 592 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 10831 Color: 0
Size: 4001 Color: 0
Size: 800 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 7820 Color: 1
Size: 7236 Color: 0
Size: 576 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 9571 Color: 1
Size: 5051 Color: 1
Size: 1010 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13726 Color: 1
Size: 1590 Color: 1
Size: 316 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 0
Size: 2038 Color: 0
Size: 404 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 0
Size: 1820 Color: 0
Size: 360 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13980 Color: 1
Size: 1380 Color: 1
Size: 272 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 10508 Color: 1
Size: 4276 Color: 0
Size: 848 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 9532 Color: 0
Size: 5084 Color: 0
Size: 1016 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13494 Color: 1
Size: 1782 Color: 0
Size: 356 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 9298 Color: 0
Size: 5282 Color: 0
Size: 1052 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 1
Size: 2152 Color: 1
Size: 416 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 1
Size: 2070 Color: 0
Size: 412 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 0
Size: 1700 Color: 0
Size: 336 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13125 Color: 1
Size: 2091 Color: 0
Size: 416 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13211 Color: 1
Size: 2019 Color: 1
Size: 402 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 0
Size: 1336 Color: 0
Size: 256 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 12883 Color: 1
Size: 2291 Color: 0
Size: 458 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 9096 Color: 1
Size: 5448 Color: 1
Size: 1088 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 8834 Color: 0
Size: 5666 Color: 0
Size: 1132 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 9244 Color: 0
Size: 5324 Color: 0
Size: 1064 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13790 Color: 1
Size: 1538 Color: 1
Size: 304 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13813 Color: 1
Size: 1517 Color: 1
Size: 302 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7832 Color: 0
Size: 6504 Color: 1
Size: 1296 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 1
Size: 1448 Color: 1
Size: 288 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 0
Size: 4076 Color: 1
Size: 464 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 0
Size: 1368 Color: 0
Size: 272 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 1
Size: 1724 Color: 0
Size: 344 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13292 Color: 1
Size: 1956 Color: 0
Size: 384 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 9958 Color: 0
Size: 4730 Color: 1
Size: 944 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13039 Color: 1
Size: 2161 Color: 0
Size: 432 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 12858 Color: 1
Size: 2314 Color: 1
Size: 460 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 12820 Color: 0
Size: 2348 Color: 0
Size: 464 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 12443 Color: 0
Size: 2659 Color: 1
Size: 530 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 1
Size: 1560 Color: 0
Size: 240 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 0
Size: 1324 Color: 0
Size: 256 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 13894 Color: 1
Size: 1450 Color: 1
Size: 288 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 1
Size: 2072 Color: 1
Size: 400 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 0
Size: 3912 Color: 0
Size: 608 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 9631 Color: 1
Size: 5001 Color: 0
Size: 1000 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 10258 Color: 1
Size: 4582 Color: 1
Size: 792 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 13706 Color: 0
Size: 1606 Color: 1
Size: 320 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 1
Size: 1630 Color: 0
Size: 124 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 0
Size: 2248 Color: 1
Size: 432 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 10658 Color: 0
Size: 4146 Color: 0
Size: 828 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 13622 Color: 0
Size: 1678 Color: 1
Size: 332 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 12938 Color: 0
Size: 2246 Color: 0
Size: 448 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 12181 Color: 0
Size: 2877 Color: 0
Size: 574 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 13526 Color: 0
Size: 1758 Color: 0
Size: 348 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 8072 Color: 0
Size: 6648 Color: 0
Size: 912 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 10319 Color: 0
Size: 4429 Color: 0
Size: 884 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 7818 Color: 0
Size: 6514 Color: 0
Size: 1300 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 11928 Color: 1
Size: 3096 Color: 1
Size: 608 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 0
Size: 2722 Color: 0
Size: 540 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 11878 Color: 1
Size: 3130 Color: 1
Size: 624 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 1
Size: 3156 Color: 1
Size: 624 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 1
Size: 5180 Color: 0
Size: 1032 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 7828 Color: 0
Size: 6508 Color: 1
Size: 1296 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 11190 Color: 1
Size: 3702 Color: 0
Size: 740 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 8747 Color: 0
Size: 5739 Color: 0
Size: 1146 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 13950 Color: 0
Size: 1402 Color: 0
Size: 280 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 12772 Color: 0
Size: 2388 Color: 1
Size: 472 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 7834 Color: 0
Size: 6502 Color: 1
Size: 1296 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 10307 Color: 1
Size: 4439 Color: 0
Size: 886 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 8738 Color: 1
Size: 5746 Color: 1
Size: 1148 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 13304 Color: 0
Size: 1944 Color: 1
Size: 384 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 10440 Color: 1
Size: 4328 Color: 0
Size: 864 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 13759 Color: 0
Size: 1561 Color: 0
Size: 312 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 13542 Color: 1
Size: 1742 Color: 1
Size: 348 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 13903 Color: 0
Size: 1441 Color: 0
Size: 288 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 13590 Color: 1
Size: 1702 Color: 1
Size: 340 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 11400 Color: 1
Size: 3768 Color: 0
Size: 464 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 0
Size: 5260 Color: 0
Size: 1048 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 13366 Color: 0
Size: 1890 Color: 0
Size: 376 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 12788 Color: 0
Size: 2372 Color: 0
Size: 472 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 1
Size: 1688 Color: 1
Size: 336 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 9612 Color: 0
Size: 5020 Color: 0
Size: 1000 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 7817 Color: 0
Size: 6513 Color: 1
Size: 1302 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 13548 Color: 0
Size: 1740 Color: 0
Size: 344 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 12821 Color: 1
Size: 2343 Color: 1
Size: 468 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 8818 Color: 1
Size: 5682 Color: 1
Size: 1132 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 13702 Color: 0
Size: 1610 Color: 0
Size: 320 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 9756 Color: 0
Size: 4900 Color: 0
Size: 976 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 8882 Color: 1
Size: 5626 Color: 0
Size: 1124 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 11259 Color: 1
Size: 3645 Color: 1
Size: 728 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 13804 Color: 1
Size: 1524 Color: 0
Size: 304 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 12703 Color: 0
Size: 2441 Color: 1
Size: 488 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 14034 Color: 1
Size: 1334 Color: 1
Size: 264 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 12252 Color: 1
Size: 2820 Color: 1
Size: 560 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 0
Size: 1880 Color: 1
Size: 208 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 10818 Color: 1
Size: 4014 Color: 1
Size: 800 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 11238 Color: 1
Size: 3662 Color: 0
Size: 732 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 10420 Color: 0
Size: 4348 Color: 0
Size: 864 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 11094 Color: 0
Size: 3782 Color: 0
Size: 756 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 0
Size: 2148 Color: 1
Size: 424 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 0
Size: 4328 Color: 0
Size: 240 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 1
Size: 3528 Color: 1
Size: 464 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 0
Size: 1896 Color: 0
Size: 368 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 11174 Color: 1
Size: 3718 Color: 1
Size: 740 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 1
Size: 2856 Color: 0
Size: 560 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 12442 Color: 0
Size: 2662 Color: 0
Size: 528 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 10136 Color: 0
Size: 4584 Color: 1
Size: 912 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 12580 Color: 0
Size: 2548 Color: 0
Size: 504 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 1
Size: 2828 Color: 0
Size: 560 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 10808 Color: 0
Size: 4024 Color: 0
Size: 800 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 0
Size: 1608 Color: 0
Size: 304 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 12879 Color: 1
Size: 2295 Color: 0
Size: 458 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 11844 Color: 0
Size: 3164 Color: 1
Size: 624 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 13411 Color: 1
Size: 2181 Color: 1
Size: 40 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 8914 Color: 0
Size: 6242 Color: 1
Size: 476 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 8795 Color: 0
Size: 5699 Color: 0
Size: 1138 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 12658 Color: 0
Size: 2794 Color: 0
Size: 180 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 12274 Color: 0
Size: 2802 Color: 0
Size: 556 Color: 1

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 11689 Color: 1
Size: 3287 Color: 1
Size: 656 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 12285 Color: 0
Size: 2791 Color: 0
Size: 556 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 11598 Color: 1
Size: 3362 Color: 1
Size: 672 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 12520 Color: 1
Size: 2600 Color: 1
Size: 512 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 13589 Color: 0
Size: 1703 Color: 1
Size: 340 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 11738 Color: 1
Size: 3246 Color: 1
Size: 648 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 13147 Color: 0
Size: 2071 Color: 0
Size: 414 Color: 1

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 7826 Color: 1
Size: 6506 Color: 1
Size: 1300 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 13619 Color: 0
Size: 1679 Color: 0
Size: 334 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 12085 Color: 1
Size: 2957 Color: 1
Size: 590 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 12201 Color: 0
Size: 3005 Color: 1
Size: 426 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 13459 Color: 1
Size: 1811 Color: 0
Size: 362 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 13271 Color: 1
Size: 1969 Color: 1
Size: 392 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 8172 Color: 1
Size: 6652 Color: 0
Size: 808 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 12418 Color: 0
Size: 2682 Color: 0
Size: 532 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 12037 Color: 1
Size: 2997 Color: 1
Size: 598 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 10114 Color: 0
Size: 4602 Color: 1
Size: 916 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 13243 Color: 0
Size: 1991 Color: 0
Size: 398 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 13627 Color: 0
Size: 1671 Color: 1
Size: 334 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 13339 Color: 0
Size: 1911 Color: 0
Size: 382 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 13495 Color: 0
Size: 1781 Color: 0
Size: 356 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 12507 Color: 1
Size: 2605 Color: 1
Size: 520 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 13403 Color: 1
Size: 1859 Color: 0
Size: 370 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 10941 Color: 0
Size: 3911 Color: 0
Size: 780 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 13845 Color: 1
Size: 1491 Color: 0
Size: 296 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 13219 Color: 1
Size: 2011 Color: 0
Size: 402 Color: 1

Total size: 3095136
Total free space: 0

