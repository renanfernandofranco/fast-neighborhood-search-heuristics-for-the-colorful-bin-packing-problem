Capicity Bin: 1000001
Lower Bound: 4453

Bins used: 4455
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 173333 Color: 0
Size: 173205 Color: 0
Size: 169025 Color: 1
Size: 168228 Color: 1
Size: 167865 Color: 1
Size: 148345 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 601506 Color: 0
Size: 273936 Color: 0
Size: 124559 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 782876 Color: 1
Size: 108875 Color: 0
Size: 108250 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 627375 Color: 0
Size: 241943 Color: 1
Size: 130683 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 604472 Color: 1
Size: 201161 Color: 1
Size: 194368 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 726117 Color: 0
Size: 166434 Color: 1
Size: 107450 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 585872 Color: 0
Size: 238628 Color: 1
Size: 175501 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 470287 Color: 0
Size: 418092 Color: 1
Size: 111622 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 712090 Color: 0
Size: 178525 Color: 1
Size: 109386 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 539496 Color: 1
Size: 343574 Color: 0
Size: 116931 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 590803 Color: 0
Size: 219837 Color: 0
Size: 189361 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 772861 Color: 0
Size: 127028 Color: 0
Size: 100112 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 624892 Color: 0
Size: 254601 Color: 1
Size: 120508 Color: 0

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 440987 Color: 1
Size: 302782 Color: 0
Size: 150124 Color: 0
Size: 106108 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 690329 Color: 1
Size: 171611 Color: 0
Size: 138061 Color: 0

Bin 16: 0 of cap free
Amount of items: 6
Items: 
Size: 267067 Color: 0
Size: 210627 Color: 0
Size: 131042 Color: 1
Size: 130926 Color: 0
Size: 130692 Color: 1
Size: 129647 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 488611 Color: 0
Size: 270950 Color: 1
Size: 240440 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 751681 Color: 0
Size: 143921 Color: 1
Size: 104399 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 754168 Color: 0
Size: 140248 Color: 1
Size: 105585 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 767640 Color: 0
Size: 118785 Color: 1
Size: 113576 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 730257 Color: 0
Size: 147858 Color: 0
Size: 121886 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 622456 Color: 0
Size: 275426 Color: 1
Size: 102119 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 715289 Color: 1
Size: 181440 Color: 0
Size: 103272 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 625803 Color: 1
Size: 192278 Color: 0
Size: 181920 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 794899 Color: 0
Size: 104315 Color: 1
Size: 100787 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 623056 Color: 1
Size: 209089 Color: 1
Size: 167856 Color: 0

Bin 27: 0 of cap free
Amount of items: 7
Items: 
Size: 157260 Color: 0
Size: 157170 Color: 0
Size: 157071 Color: 0
Size: 150416 Color: 1
Size: 149301 Color: 1
Size: 128669 Color: 1
Size: 100114 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 619150 Color: 0
Size: 245546 Color: 1
Size: 135305 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 696833 Color: 0
Size: 179399 Color: 1
Size: 123769 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 601036 Color: 0
Size: 247904 Color: 0
Size: 151061 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 692443 Color: 1
Size: 160537 Color: 1
Size: 147021 Color: 0

Bin 32: 0 of cap free
Amount of items: 5
Items: 
Size: 252427 Color: 0
Size: 192103 Color: 0
Size: 191595 Color: 0
Size: 189207 Color: 1
Size: 174669 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 717890 Color: 1
Size: 167504 Color: 0
Size: 114607 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 728704 Color: 0
Size: 170490 Color: 1
Size: 100807 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 742414 Color: 1
Size: 132962 Color: 0
Size: 124625 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 575479 Color: 1
Size: 272379 Color: 0
Size: 152143 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 684912 Color: 0
Size: 169300 Color: 1
Size: 145789 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 641511 Color: 0
Size: 231444 Color: 1
Size: 127046 Color: 1

Bin 39: 0 of cap free
Amount of items: 7
Items: 
Size: 170419 Color: 0
Size: 145106 Color: 0
Size: 142160 Color: 1
Size: 141870 Color: 1
Size: 141282 Color: 0
Size: 139656 Color: 1
Size: 119508 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 748897 Color: 0
Size: 146051 Color: 1
Size: 105053 Color: 0

Bin 41: 0 of cap free
Amount of items: 5
Items: 
Size: 222416 Color: 1
Size: 219820 Color: 0
Size: 219539 Color: 0
Size: 181869 Color: 1
Size: 156357 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 589675 Color: 0
Size: 232517 Color: 0
Size: 177809 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 510508 Color: 1
Size: 337850 Color: 0
Size: 151643 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 765537 Color: 0
Size: 132890 Color: 1
Size: 101574 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 593496 Color: 0
Size: 209941 Color: 1
Size: 196564 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 783147 Color: 1
Size: 113077 Color: 0
Size: 103777 Color: 1

Bin 47: 0 of cap free
Amount of items: 5
Items: 
Size: 230870 Color: 0
Size: 228917 Color: 1
Size: 227554 Color: 0
Size: 196391 Color: 1
Size: 116269 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 749492 Color: 0
Size: 129789 Color: 0
Size: 120720 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 690329 Color: 1
Size: 159590 Color: 1
Size: 150082 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 708243 Color: 1
Size: 167209 Color: 1
Size: 124549 Color: 0

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 587708 Color: 1
Size: 412293 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 784179 Color: 1
Size: 115333 Color: 0
Size: 100489 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 520762 Color: 0
Size: 356358 Color: 1
Size: 122881 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 703921 Color: 0
Size: 153794 Color: 0
Size: 142286 Color: 1

Bin 55: 0 of cap free
Amount of items: 7
Items: 
Size: 235824 Color: 0
Size: 182496 Color: 0
Size: 119561 Color: 1
Size: 119495 Color: 0
Size: 116598 Color: 0
Size: 114157 Color: 1
Size: 111870 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 664159 Color: 1
Size: 208706 Color: 0
Size: 127136 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 589690 Color: 1
Size: 266834 Color: 1
Size: 143477 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 413796 Color: 1
Size: 404159 Color: 0
Size: 182046 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 774370 Color: 0
Size: 118727 Color: 1
Size: 106904 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 713610 Color: 1
Size: 174771 Color: 1
Size: 111620 Color: 0

Bin 61: 0 of cap free
Amount of items: 4
Items: 
Size: 266337 Color: 0
Size: 256313 Color: 1
Size: 255790 Color: 0
Size: 221561 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 630435 Color: 0
Size: 231289 Color: 0
Size: 138277 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 619243 Color: 1
Size: 278409 Color: 0
Size: 102349 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 752516 Color: 1
Size: 131157 Color: 0
Size: 116328 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 763757 Color: 0
Size: 119548 Color: 1
Size: 116696 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 616461 Color: 0
Size: 195856 Color: 0
Size: 187684 Color: 1

Bin 67: 0 of cap free
Amount of items: 4
Items: 
Size: 383115 Color: 0
Size: 253562 Color: 1
Size: 253463 Color: 1
Size: 109861 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 775292 Color: 1
Size: 116087 Color: 0
Size: 108622 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 605768 Color: 0
Size: 291425 Color: 1
Size: 102808 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 617908 Color: 1
Size: 243831 Color: 0
Size: 138262 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 709140 Color: 1
Size: 179977 Color: 0
Size: 110884 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 604826 Color: 1
Size: 277918 Color: 0
Size: 117257 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 641932 Color: 1
Size: 225651 Color: 1
Size: 132418 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 388670 Color: 1
Size: 372455 Color: 1
Size: 238876 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 781183 Color: 1
Size: 113086 Color: 0
Size: 105732 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 786093 Color: 0
Size: 113458 Color: 1
Size: 100450 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 792191 Color: 0
Size: 105529 Color: 1
Size: 102281 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 767188 Color: 0
Size: 124920 Color: 1
Size: 107893 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 735345 Color: 0
Size: 136000 Color: 1
Size: 128656 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 792108 Color: 1
Size: 106940 Color: 0
Size: 100953 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 778693 Color: 1
Size: 114849 Color: 0
Size: 106459 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 789838 Color: 1
Size: 105514 Color: 0
Size: 104649 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 708017 Color: 1
Size: 168540 Color: 0
Size: 123444 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 785654 Color: 1
Size: 108310 Color: 0
Size: 106037 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 728764 Color: 1
Size: 143060 Color: 0
Size: 128177 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 789781 Color: 0
Size: 106270 Color: 1
Size: 103950 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 651047 Color: 1
Size: 219120 Color: 1
Size: 129834 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 791031 Color: 1
Size: 107986 Color: 1
Size: 100984 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 683038 Color: 1
Size: 212487 Color: 0
Size: 104476 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 601352 Color: 0
Size: 262540 Color: 1
Size: 136109 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 586229 Color: 1
Size: 296899 Color: 0
Size: 116873 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 772301 Color: 1
Size: 126954 Color: 0
Size: 100746 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 745083 Color: 1
Size: 140495 Color: 0
Size: 114423 Color: 0

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 624878 Color: 1
Size: 375123 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 780818 Color: 0
Size: 115339 Color: 1
Size: 103844 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 757504 Color: 1
Size: 127507 Color: 1
Size: 114990 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 791462 Color: 0
Size: 105476 Color: 0
Size: 103063 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 755544 Color: 1
Size: 123633 Color: 0
Size: 120824 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 701098 Color: 1
Size: 172299 Color: 1
Size: 126604 Color: 0

Bin 100: 0 of cap free
Amount of items: 6
Items: 
Size: 173609 Color: 0
Size: 173521 Color: 0
Size: 170489 Color: 1
Size: 170091 Color: 1
Size: 170016 Color: 1
Size: 142275 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 764023 Color: 1
Size: 122182 Color: 0
Size: 113796 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 537332 Color: 0
Size: 258519 Color: 1
Size: 204150 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 752585 Color: 1
Size: 132201 Color: 0
Size: 115215 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 744796 Color: 0
Size: 145262 Color: 1
Size: 109943 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 583034 Color: 1
Size: 298632 Color: 0
Size: 118335 Color: 1

Bin 106: 0 of cap free
Amount of items: 7
Items: 
Size: 161860 Color: 0
Size: 161834 Color: 0
Size: 154621 Color: 1
Size: 154013 Color: 1
Size: 142671 Color: 0
Size: 119947 Color: 0
Size: 105055 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 699407 Color: 1
Size: 198309 Color: 1
Size: 102285 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 737368 Color: 1
Size: 159708 Color: 0
Size: 102925 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 743536 Color: 1
Size: 128647 Color: 0
Size: 127818 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 791106 Color: 0
Size: 106863 Color: 1
Size: 102032 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 573292 Color: 1
Size: 297044 Color: 0
Size: 129665 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 570429 Color: 1
Size: 301323 Color: 0
Size: 128249 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 773559 Color: 1
Size: 113494 Color: 0
Size: 112948 Color: 0

Bin 114: 0 of cap free
Amount of items: 5
Items: 
Size: 348935 Color: 0
Size: 319351 Color: 0
Size: 113715 Color: 0
Size: 109518 Color: 1
Size: 108482 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 377807 Color: 0
Size: 320892 Color: 1
Size: 301302 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 574657 Color: 0
Size: 215141 Color: 0
Size: 210203 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 587889 Color: 0
Size: 293967 Color: 1
Size: 118145 Color: 1

Bin 118: 0 of cap free
Amount of items: 8
Items: 
Size: 129759 Color: 0
Size: 129205 Color: 0
Size: 128725 Color: 0
Size: 126549 Color: 1
Size: 126217 Color: 1
Size: 125924 Color: 1
Size: 125767 Color: 1
Size: 107855 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 765515 Color: 0
Size: 127077 Color: 1
Size: 107409 Color: 1

Bin 120: 0 of cap free
Amount of items: 5
Items: 
Size: 276317 Color: 0
Size: 257817 Color: 1
Size: 192769 Color: 1
Size: 160144 Color: 0
Size: 112954 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 722569 Color: 0
Size: 144457 Color: 1
Size: 132975 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 797517 Color: 1
Size: 102287 Color: 0
Size: 100197 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 769605 Color: 0
Size: 123800 Color: 0
Size: 106596 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 756775 Color: 0
Size: 126963 Color: 1
Size: 116263 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 749410 Color: 1
Size: 128514 Color: 0
Size: 122077 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 758924 Color: 1
Size: 126663 Color: 0
Size: 114414 Color: 0

Bin 127: 0 of cap free
Amount of items: 5
Items: 
Size: 291904 Color: 0
Size: 206265 Color: 1
Size: 198039 Color: 1
Size: 197590 Color: 0
Size: 106203 Color: 0

Bin 128: 0 of cap free
Amount of items: 4
Items: 
Size: 424596 Color: 1
Size: 237939 Color: 0
Size: 236064 Color: 1
Size: 101402 Color: 0

Bin 129: 0 of cap free
Amount of items: 5
Items: 
Size: 257850 Color: 0
Size: 245169 Color: 1
Size: 244685 Color: 1
Size: 132471 Color: 0
Size: 119826 Color: 1

Bin 130: 0 of cap free
Amount of items: 6
Items: 
Size: 192434 Color: 0
Size: 165309 Color: 0
Size: 165104 Color: 0
Size: 160215 Color: 1
Size: 158785 Color: 1
Size: 158154 Color: 1

Bin 131: 0 of cap free
Amount of items: 5
Items: 
Size: 223029 Color: 1
Size: 195076 Color: 0
Size: 195068 Color: 0
Size: 194978 Color: 0
Size: 191850 Color: 1

Bin 132: 0 of cap free
Amount of items: 5
Items: 
Size: 477932 Color: 0
Size: 161319 Color: 1
Size: 123977 Color: 0
Size: 118510 Color: 1
Size: 118263 Color: 1

Bin 133: 0 of cap free
Amount of items: 5
Items: 
Size: 400245 Color: 1
Size: 197491 Color: 0
Size: 136857 Color: 0
Size: 135995 Color: 1
Size: 129413 Color: 0

Bin 134: 0 of cap free
Amount of items: 7
Items: 
Size: 196172 Color: 1
Size: 135043 Color: 0
Size: 134950 Color: 0
Size: 134290 Color: 0
Size: 133424 Color: 1
Size: 133173 Color: 1
Size: 132949 Color: 1

Bin 135: 0 of cap free
Amount of items: 7
Items: 
Size: 207254 Color: 0
Size: 133001 Color: 0
Size: 132996 Color: 0
Size: 131931 Color: 1
Size: 131832 Color: 1
Size: 131732 Color: 1
Size: 131255 Color: 1

Bin 136: 0 of cap free
Amount of items: 8
Items: 
Size: 211077 Color: 1
Size: 116322 Color: 0
Size: 116280 Color: 0
Size: 115370 Color: 0
Size: 112860 Color: 1
Size: 112335 Color: 1
Size: 112237 Color: 1
Size: 103520 Color: 0

Bin 137: 0 of cap free
Amount of items: 5
Items: 
Size: 287085 Color: 0
Size: 241854 Color: 1
Size: 213334 Color: 1
Size: 133645 Color: 0
Size: 124083 Color: 1

Bin 138: 0 of cap free
Amount of items: 7
Items: 
Size: 149235 Color: 0
Size: 147369 Color: 0
Size: 147029 Color: 0
Size: 143599 Color: 1
Size: 143402 Color: 1
Size: 141468 Color: 1
Size: 127899 Color: 0

Bin 139: 0 of cap free
Amount of items: 5
Items: 
Size: 226788 Color: 0
Size: 225643 Color: 0
Size: 225280 Color: 1
Size: 167700 Color: 0
Size: 154590 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 418882 Color: 1
Size: 350324 Color: 1
Size: 230795 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 420670 Color: 1
Size: 411715 Color: 0
Size: 167616 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 434396 Color: 0
Size: 404954 Color: 0
Size: 160651 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 434731 Color: 0
Size: 356005 Color: 1
Size: 209265 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 434789 Color: 0
Size: 417932 Color: 1
Size: 147280 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 435696 Color: 0
Size: 420825 Color: 1
Size: 143480 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 435716 Color: 0
Size: 389069 Color: 1
Size: 175216 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 436525 Color: 0
Size: 432055 Color: 1
Size: 131421 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 441003 Color: 1
Size: 377408 Color: 0
Size: 181590 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 442004 Color: 1
Size: 373852 Color: 0
Size: 184145 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 442242 Color: 1
Size: 353543 Color: 1
Size: 204216 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 442972 Color: 0
Size: 421339 Color: 1
Size: 135690 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 445180 Color: 1
Size: 438739 Color: 0
Size: 116082 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 445884 Color: 0
Size: 434411 Color: 0
Size: 119706 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 447199 Color: 1
Size: 361839 Color: 0
Size: 190963 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 447731 Color: 0
Size: 378539 Color: 0
Size: 173731 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 450763 Color: 1
Size: 409459 Color: 0
Size: 139779 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 452551 Color: 1
Size: 439638 Color: 0
Size: 107812 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 455045 Color: 0
Size: 427996 Color: 0
Size: 116960 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 456417 Color: 0
Size: 416735 Color: 1
Size: 126849 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 462755 Color: 0
Size: 395090 Color: 0
Size: 142156 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 465977 Color: 0
Size: 415537 Color: 1
Size: 118487 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 468965 Color: 1
Size: 378562 Color: 0
Size: 152474 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 469434 Color: 1
Size: 395140 Color: 0
Size: 135427 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 471654 Color: 1
Size: 374438 Color: 0
Size: 153909 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 472212 Color: 1
Size: 395776 Color: 0
Size: 132013 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 474723 Color: 0
Size: 350051 Color: 1
Size: 175227 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 474758 Color: 0
Size: 341995 Color: 1
Size: 183248 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 474967 Color: 0
Size: 405880 Color: 0
Size: 119154 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 499382 Color: 0
Size: 302563 Color: 0
Size: 198056 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 499562 Color: 0
Size: 353552 Color: 1
Size: 146887 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 500072 Color: 0
Size: 389619 Color: 1
Size: 110310 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 500094 Color: 0
Size: 330090 Color: 0
Size: 169817 Color: 1

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 499962 Color: 1
Size: 340908 Color: 1
Size: 159131 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 509176 Color: 0
Size: 316680 Color: 0
Size: 174145 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 510124 Color: 0
Size: 338597 Color: 0
Size: 151280 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 510147 Color: 0
Size: 303497 Color: 1
Size: 186357 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 511197 Color: 0
Size: 324693 Color: 1
Size: 164111 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 511444 Color: 0
Size: 344278 Color: 0
Size: 144279 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 512073 Color: 1
Size: 324302 Color: 1
Size: 163626 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 512024 Color: 0
Size: 303740 Color: 1
Size: 184237 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 526697 Color: 0
Size: 286637 Color: 0
Size: 186667 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 526706 Color: 0
Size: 300978 Color: 1
Size: 172317 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 526833 Color: 1
Size: 319391 Color: 0
Size: 153777 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 527245 Color: 0
Size: 291574 Color: 1
Size: 181182 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 528164 Color: 0
Size: 325206 Color: 1
Size: 146631 Color: 0

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 529174 Color: 1
Size: 470827 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 532659 Color: 1
Size: 361474 Color: 1
Size: 105868 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 532521 Color: 0
Size: 330016 Color: 0
Size: 137464 Color: 1

Bin 189: 0 of cap free
Amount of items: 2
Items: 
Size: 533108 Color: 0
Size: 466893 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 534218 Color: 0
Size: 352745 Color: 1
Size: 113038 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 534707 Color: 1
Size: 325532 Color: 1
Size: 139762 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 535249 Color: 1
Size: 355919 Color: 1
Size: 108833 Color: 0

Bin 193: 0 of cap free
Amount of items: 2
Items: 
Size: 535310 Color: 1
Size: 464691 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 535801 Color: 1
Size: 306080 Color: 1
Size: 158120 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 536128 Color: 1
Size: 269940 Color: 0
Size: 193933 Color: 1

Bin 196: 0 of cap free
Amount of items: 2
Items: 
Size: 539424 Color: 1
Size: 460577 Color: 0

Bin 197: 0 of cap free
Amount of items: 2
Items: 
Size: 543801 Color: 1
Size: 456200 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 545099 Color: 0
Size: 269328 Color: 0
Size: 185574 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 545193 Color: 1
Size: 260037 Color: 1
Size: 194771 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 545304 Color: 1
Size: 316928 Color: 1
Size: 137769 Color: 0

Bin 201: 0 of cap free
Amount of items: 2
Items: 
Size: 554143 Color: 1
Size: 445858 Color: 0

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 562026 Color: 1
Size: 437975 Color: 0

Bin 203: 0 of cap free
Amount of items: 2
Items: 
Size: 564004 Color: 0
Size: 435997 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 574984 Color: 1
Size: 257319 Color: 1
Size: 167698 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 576190 Color: 1
Size: 244873 Color: 0
Size: 178938 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 577847 Color: 1
Size: 312125 Color: 0
Size: 110029 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 577913 Color: 1
Size: 227849 Color: 0
Size: 194239 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 578154 Color: 1
Size: 287087 Color: 0
Size: 134760 Color: 1

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 578133 Color: 0
Size: 301045 Color: 1
Size: 120823 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 578343 Color: 1
Size: 275401 Color: 0
Size: 146257 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 578330 Color: 0
Size: 255545 Color: 0
Size: 166126 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 578954 Color: 1
Size: 276143 Color: 0
Size: 144904 Color: 1

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 584285 Color: 1
Size: 415716 Color: 0

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 585092 Color: 1
Size: 414909 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 586306 Color: 0
Size: 302132 Color: 0
Size: 111563 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 586433 Color: 0
Size: 272204 Color: 0
Size: 141364 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 586489 Color: 0
Size: 259270 Color: 0
Size: 154242 Color: 1

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 586556 Color: 0
Size: 228875 Color: 1
Size: 184570 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 586631 Color: 0
Size: 267888 Color: 1
Size: 145482 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 586703 Color: 0
Size: 279646 Color: 1
Size: 133652 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 586721 Color: 0
Size: 217232 Color: 1
Size: 196048 Color: 0

Bin 222: 0 of cap free
Amount of items: 2
Items: 
Size: 586978 Color: 0
Size: 413023 Color: 1

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 587710 Color: 0
Size: 301650 Color: 0
Size: 110641 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 587808 Color: 0
Size: 238276 Color: 0
Size: 173917 Color: 1

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 587973 Color: 1
Size: 225745 Color: 0
Size: 186283 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 587975 Color: 1
Size: 269211 Color: 0
Size: 142815 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 587927 Color: 0
Size: 266688 Color: 0
Size: 145386 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 588116 Color: 1
Size: 266110 Color: 0
Size: 145775 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 588286 Color: 1
Size: 291495 Color: 1
Size: 120220 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 588343 Color: 1
Size: 278682 Color: 0
Size: 132976 Color: 1

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 588430 Color: 0
Size: 232425 Color: 0
Size: 179146 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 588727 Color: 0
Size: 301226 Color: 0
Size: 110048 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 588822 Color: 1
Size: 272011 Color: 0
Size: 139168 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 589235 Color: 1
Size: 275292 Color: 0
Size: 135474 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 589189 Color: 0
Size: 290800 Color: 1
Size: 120012 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 589744 Color: 1
Size: 274950 Color: 0
Size: 135307 Color: 1

Bin 237: 0 of cap free
Amount of items: 2
Items: 
Size: 591559 Color: 0
Size: 408442 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 595270 Color: 1
Size: 208437 Color: 1
Size: 196294 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 595214 Color: 0
Size: 266039 Color: 0
Size: 138748 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 595508 Color: 0
Size: 265402 Color: 0
Size: 139091 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 596771 Color: 0
Size: 303192 Color: 0
Size: 100038 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 597747 Color: 1
Size: 286696 Color: 0
Size: 115558 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 597763 Color: 1
Size: 233617 Color: 0
Size: 168621 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 597610 Color: 0
Size: 233576 Color: 0
Size: 168815 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 598213 Color: 0
Size: 279711 Color: 1
Size: 122077 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 598230 Color: 0
Size: 208605 Color: 0
Size: 193166 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 598464 Color: 1
Size: 205438 Color: 0
Size: 196099 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 599621 Color: 1
Size: 237529 Color: 0
Size: 162851 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 599675 Color: 1
Size: 254825 Color: 0
Size: 145501 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 599668 Color: 0
Size: 255618 Color: 0
Size: 144715 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 599791 Color: 1
Size: 275949 Color: 0
Size: 124261 Color: 1

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 601817 Color: 0
Size: 273072 Color: 0
Size: 125112 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 601959 Color: 1
Size: 215666 Color: 1
Size: 182376 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 603899 Color: 1
Size: 198488 Color: 1
Size: 197614 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 603898 Color: 0
Size: 215929 Color: 1
Size: 180174 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 604127 Color: 1
Size: 225762 Color: 0
Size: 170112 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 604057 Color: 0
Size: 247920 Color: 0
Size: 148024 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 604160 Color: 0
Size: 276074 Color: 0
Size: 119767 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 604876 Color: 0
Size: 245875 Color: 0
Size: 149250 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 604954 Color: 1
Size: 261103 Color: 1
Size: 133944 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 604973 Color: 0
Size: 245807 Color: 0
Size: 149221 Color: 1

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 609040 Color: 0
Size: 390961 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 611336 Color: 1
Size: 248022 Color: 0
Size: 140643 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 612927 Color: 1
Size: 232769 Color: 0
Size: 154305 Color: 0

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 614147 Color: 0
Size: 385854 Color: 1

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 616212 Color: 1
Size: 383789 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 616445 Color: 1
Size: 211128 Color: 0
Size: 172428 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 617129 Color: 1
Size: 196156 Color: 0
Size: 186716 Color: 1

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 621890 Color: 1
Size: 189266 Color: 0
Size: 188845 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 621930 Color: 1
Size: 247644 Color: 1
Size: 130427 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 621864 Color: 0
Size: 190867 Color: 0
Size: 187270 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 623088 Color: 0
Size: 195812 Color: 0
Size: 181101 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 624935 Color: 1
Size: 197338 Color: 1
Size: 177728 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 624915 Color: 0
Size: 221645 Color: 1
Size: 153441 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 624996 Color: 1
Size: 189332 Color: 1
Size: 185673 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 624961 Color: 0
Size: 217706 Color: 1
Size: 157334 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 625477 Color: 1
Size: 247986 Color: 0
Size: 126538 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 625746 Color: 0
Size: 191567 Color: 1
Size: 182688 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 627644 Color: 1
Size: 228669 Color: 0
Size: 143688 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 629518 Color: 0
Size: 200379 Color: 1
Size: 170104 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 629875 Color: 1
Size: 208369 Color: 1
Size: 161757 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 629771 Color: 0
Size: 188779 Color: 1
Size: 181451 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 630038 Color: 1
Size: 195001 Color: 0
Size: 174962 Color: 1

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 629906 Color: 0
Size: 191928 Color: 0
Size: 178167 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 630117 Color: 1
Size: 195898 Color: 0
Size: 173986 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 630183 Color: 1
Size: 230115 Color: 0
Size: 139703 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 630188 Color: 1
Size: 221537 Color: 1
Size: 148276 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 630037 Color: 0
Size: 194722 Color: 1
Size: 175242 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 630273 Color: 1
Size: 196611 Color: 0
Size: 173117 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 631267 Color: 0
Size: 185518 Color: 1
Size: 183216 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 631820 Color: 0
Size: 192295 Color: 0
Size: 175886 Color: 1

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 631801 Color: 1
Size: 194332 Color: 0
Size: 173868 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 632060 Color: 0
Size: 190528 Color: 1
Size: 177413 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 632280 Color: 0
Size: 195557 Color: 1
Size: 172164 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 632439 Color: 0
Size: 187858 Color: 1
Size: 179704 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 635356 Color: 0
Size: 192513 Color: 1
Size: 172132 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 635386 Color: 0
Size: 194653 Color: 1
Size: 169962 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 635770 Color: 0
Size: 252602 Color: 1
Size: 111629 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 636038 Color: 0
Size: 188292 Color: 0
Size: 175671 Color: 1

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 636213 Color: 1
Size: 253517 Color: 0
Size: 110271 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 636637 Color: 1
Size: 199205 Color: 1
Size: 164159 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 636598 Color: 0
Size: 224986 Color: 1
Size: 138417 Color: 0

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 636783 Color: 0
Size: 363218 Color: 1

Bin 304: 0 of cap free
Amount of items: 2
Items: 
Size: 637872 Color: 0
Size: 362129 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 638546 Color: 1
Size: 217210 Color: 1
Size: 144245 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 638549 Color: 1
Size: 233345 Color: 0
Size: 128107 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 639013 Color: 1
Size: 186992 Color: 1
Size: 173996 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 639050 Color: 1
Size: 198891 Color: 0
Size: 162060 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 639656 Color: 0
Size: 252603 Color: 1
Size: 107742 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 642114 Color: 1
Size: 197714 Color: 0
Size: 160173 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 642119 Color: 1
Size: 179078 Color: 1
Size: 178804 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 642632 Color: 1
Size: 192358 Color: 0
Size: 165011 Color: 0

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 648873 Color: 1
Size: 351128 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 649961 Color: 0
Size: 182914 Color: 0
Size: 167126 Color: 1

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 650229 Color: 0
Size: 224470 Color: 1
Size: 125302 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 650244 Color: 0
Size: 193629 Color: 1
Size: 156128 Color: 0

Bin 317: 0 of cap free
Amount of items: 2
Items: 
Size: 652144 Color: 1
Size: 347857 Color: 0

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 654982 Color: 1
Size: 345019 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 655802 Color: 0
Size: 188008 Color: 1
Size: 156191 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 656142 Color: 0
Size: 189661 Color: 1
Size: 154198 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 656147 Color: 0
Size: 181971 Color: 1
Size: 161883 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 656470 Color: 1
Size: 199476 Color: 0
Size: 144055 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 656399 Color: 0
Size: 188764 Color: 1
Size: 154838 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 656647 Color: 1
Size: 188355 Color: 1
Size: 154999 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 656975 Color: 1
Size: 198450 Color: 0
Size: 144576 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 661188 Color: 1
Size: 204098 Color: 0
Size: 134715 Color: 1

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 661199 Color: 0
Size: 198028 Color: 0
Size: 140774 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 671782 Color: 0
Size: 183418 Color: 0
Size: 144801 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 671934 Color: 1
Size: 184723 Color: 0
Size: 143344 Color: 1

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 671907 Color: 0
Size: 171632 Color: 1
Size: 156462 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 672133 Color: 1
Size: 199122 Color: 1
Size: 128746 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 672024 Color: 0
Size: 178753 Color: 0
Size: 149224 Color: 1

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 672258 Color: 1
Size: 188373 Color: 1
Size: 139370 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 672359 Color: 1
Size: 165025 Color: 0
Size: 162617 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 673499 Color: 0
Size: 197153 Color: 1
Size: 129349 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 673641 Color: 1
Size: 191228 Color: 0
Size: 135132 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 673690 Color: 0
Size: 180724 Color: 1
Size: 145587 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 673740 Color: 1
Size: 184618 Color: 0
Size: 141643 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 674161 Color: 1
Size: 190544 Color: 0
Size: 135296 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 674258 Color: 0
Size: 196987 Color: 1
Size: 128756 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 674321 Color: 1
Size: 211146 Color: 0
Size: 114534 Color: 1

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 674777 Color: 1
Size: 178282 Color: 1
Size: 146942 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 675302 Color: 1
Size: 197217 Color: 0
Size: 127482 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 684410 Color: 0
Size: 160227 Color: 1
Size: 155364 Color: 1

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 684887 Color: 0
Size: 159529 Color: 1
Size: 155585 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 684674 Color: 1
Size: 187505 Color: 1
Size: 127822 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 684809 Color: 1
Size: 166848 Color: 1
Size: 148344 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 684921 Color: 0
Size: 190943 Color: 1
Size: 124137 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 684866 Color: 1
Size: 164444 Color: 0
Size: 150691 Color: 1

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 685120 Color: 0
Size: 189353 Color: 1
Size: 125528 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 685232 Color: 0
Size: 158333 Color: 1
Size: 156436 Color: 1

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 685251 Color: 0
Size: 182743 Color: 1
Size: 132007 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 685456 Color: 0
Size: 188958 Color: 1
Size: 125587 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 685590 Color: 0
Size: 190554 Color: 0
Size: 123857 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 685755 Color: 0
Size: 200378 Color: 1
Size: 113868 Color: 1

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 688680 Color: 1
Size: 173178 Color: 0
Size: 138143 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 688726 Color: 0
Size: 196959 Color: 1
Size: 114316 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 688822 Color: 0
Size: 180980 Color: 1
Size: 130199 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 688894 Color: 0
Size: 159344 Color: 1
Size: 151763 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 689001 Color: 0
Size: 169208 Color: 0
Size: 141792 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 689011 Color: 0
Size: 166313 Color: 1
Size: 144677 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 689043 Color: 0
Size: 158940 Color: 0
Size: 152018 Color: 1

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 689124 Color: 0
Size: 175571 Color: 1
Size: 135306 Color: 1

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 690354 Color: 0
Size: 161818 Color: 0
Size: 147829 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 690400 Color: 0
Size: 166339 Color: 0
Size: 143262 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 690334 Color: 1
Size: 182917 Color: 1
Size: 126750 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 690637 Color: 0
Size: 159626 Color: 1
Size: 149738 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 690739 Color: 1
Size: 179850 Color: 1
Size: 129412 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 690744 Color: 0
Size: 181333 Color: 0
Size: 127924 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 690803 Color: 1
Size: 169238 Color: 1
Size: 139960 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 691263 Color: 0
Size: 198786 Color: 1
Size: 109952 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 691371 Color: 1
Size: 203611 Color: 0
Size: 105019 Color: 1

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 691378 Color: 0
Size: 174486 Color: 1
Size: 134137 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 691511 Color: 1
Size: 188491 Color: 1
Size: 119999 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 691523 Color: 0
Size: 164391 Color: 0
Size: 144087 Color: 1

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 691559 Color: 1
Size: 166433 Color: 1
Size: 142009 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 691647 Color: 0
Size: 181393 Color: 0
Size: 126961 Color: 1

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 691572 Color: 1
Size: 180252 Color: 0
Size: 128177 Color: 1

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 691818 Color: 0
Size: 195531 Color: 0
Size: 112652 Color: 1

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 691931 Color: 1
Size: 185817 Color: 1
Size: 122253 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 692160 Color: 0
Size: 195490 Color: 0
Size: 112351 Color: 1

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 692359 Color: 1
Size: 185163 Color: 1
Size: 122479 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 692302 Color: 0
Size: 197060 Color: 0
Size: 110639 Color: 1

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 692352 Color: 0
Size: 182341 Color: 0
Size: 125308 Color: 1

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 699055 Color: 1
Size: 168521 Color: 0
Size: 132425 Color: 1

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 699038 Color: 0
Size: 180519 Color: 1
Size: 120444 Color: 0

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 699519 Color: 1
Size: 300482 Color: 0

Bin 388: 0 of cap free
Amount of items: 2
Items: 
Size: 701318 Color: 1
Size: 298683 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 702625 Color: 0
Size: 193037 Color: 1
Size: 104339 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 703169 Color: 1
Size: 151413 Color: 0
Size: 145419 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 703022 Color: 0
Size: 152866 Color: 1
Size: 144113 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 703389 Color: 1
Size: 172647 Color: 1
Size: 123965 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 703225 Color: 0
Size: 151475 Color: 1
Size: 145301 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 703392 Color: 1
Size: 182554 Color: 0
Size: 114055 Color: 1

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 703294 Color: 0
Size: 179190 Color: 0
Size: 117517 Color: 1

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 703435 Color: 1
Size: 151039 Color: 1
Size: 145527 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 703342 Color: 0
Size: 192164 Color: 1
Size: 104495 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 703443 Color: 1
Size: 188371 Color: 0
Size: 108187 Color: 1

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 704090 Color: 0
Size: 295911 Color: 1

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 705369 Color: 1
Size: 173994 Color: 1
Size: 120638 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 705666 Color: 1
Size: 176186 Color: 0
Size: 118149 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 707495 Color: 0
Size: 166492 Color: 0
Size: 126014 Color: 1

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 707787 Color: 1
Size: 161960 Color: 0
Size: 130254 Color: 1

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 708040 Color: 0
Size: 189413 Color: 0
Size: 102548 Color: 1

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 708594 Color: 0
Size: 186385 Color: 0
Size: 105022 Color: 1

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 708615 Color: 1
Size: 147332 Color: 0
Size: 144054 Color: 1

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 709537 Color: 0
Size: 170105 Color: 0
Size: 120359 Color: 1

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 709761 Color: 0
Size: 151891 Color: 1
Size: 138349 Color: 1

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 709795 Color: 0
Size: 171516 Color: 1
Size: 118690 Color: 0

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 709852 Color: 0
Size: 178114 Color: 1
Size: 112035 Color: 1

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 709993 Color: 0
Size: 156249 Color: 0
Size: 133759 Color: 1

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 710032 Color: 0
Size: 168662 Color: 1
Size: 121307 Color: 1

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 710117 Color: 0
Size: 152277 Color: 1
Size: 137607 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 710513 Color: 0
Size: 179586 Color: 1
Size: 109902 Color: 0

Bin 415: 0 of cap free
Amount of items: 2
Items: 
Size: 710848 Color: 1
Size: 289153 Color: 0

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 711028 Color: 0
Size: 288973 Color: 1

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 711292 Color: 0
Size: 162303 Color: 1
Size: 126406 Color: 1

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 711397 Color: 0
Size: 165188 Color: 0
Size: 123416 Color: 1

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 711395 Color: 1
Size: 156692 Color: 0
Size: 131914 Color: 1

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 711424 Color: 0
Size: 151269 Color: 0
Size: 137308 Color: 1

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 711453 Color: 1
Size: 187523 Color: 0
Size: 101025 Color: 1

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 711597 Color: 0
Size: 151455 Color: 0
Size: 136949 Color: 1

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 711703 Color: 0
Size: 145271 Color: 1
Size: 143027 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 711961 Color: 1
Size: 185675 Color: 1
Size: 102365 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 711896 Color: 0
Size: 175971 Color: 1
Size: 112134 Color: 0

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 712037 Color: 1
Size: 172377 Color: 0
Size: 115587 Color: 1

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 711943 Color: 0
Size: 166170 Color: 1
Size: 121888 Color: 0

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 712668 Color: 1
Size: 146627 Color: 1
Size: 140706 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 712746 Color: 0
Size: 152625 Color: 0
Size: 134630 Color: 1

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 712804 Color: 1
Size: 178844 Color: 1
Size: 108353 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 712903 Color: 0
Size: 148763 Color: 0
Size: 138335 Color: 1

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 714395 Color: 1
Size: 169685 Color: 1
Size: 115921 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 714355 Color: 0
Size: 147524 Color: 0
Size: 138122 Color: 1

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 714761 Color: 1
Size: 151294 Color: 1
Size: 133946 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 714769 Color: 1
Size: 146597 Color: 0
Size: 138635 Color: 0

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 717529 Color: 1
Size: 282472 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 720579 Color: 0
Size: 175443 Color: 0
Size: 103979 Color: 1

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 720622 Color: 0
Size: 154080 Color: 1
Size: 125299 Color: 1

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 720957 Color: 0
Size: 168873 Color: 1
Size: 110171 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 721162 Color: 0
Size: 163056 Color: 0
Size: 115783 Color: 1

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 721190 Color: 0
Size: 167989 Color: 1
Size: 110822 Color: 1

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 721282 Color: 0
Size: 152137 Color: 1
Size: 126582 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 721512 Color: 0
Size: 147482 Color: 0
Size: 131007 Color: 1

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 721662 Color: 0
Size: 151944 Color: 1
Size: 126395 Color: 1

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 722134 Color: 0
Size: 277867 Color: 1

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 729628 Color: 1
Size: 143981 Color: 0
Size: 126392 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 729814 Color: 0
Size: 165077 Color: 1
Size: 105110 Color: 1

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 730252 Color: 0
Size: 146941 Color: 0
Size: 122808 Color: 1

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 730289 Color: 1
Size: 150713 Color: 1
Size: 118999 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 730736 Color: 0
Size: 143791 Color: 1
Size: 125474 Color: 1

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 730987 Color: 1
Size: 158784 Color: 1
Size: 110230 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 733307 Color: 0
Size: 159798 Color: 1
Size: 106896 Color: 1

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 733350 Color: 0
Size: 135277 Color: 1
Size: 131374 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 734934 Color: 1
Size: 139709 Color: 1
Size: 125358 Color: 0

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 734914 Color: 0
Size: 152982 Color: 0
Size: 112105 Color: 1

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 735022 Color: 1
Size: 154652 Color: 1
Size: 110327 Color: 0

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 735000 Color: 0
Size: 135712 Color: 1
Size: 129289 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 747895 Color: 1
Size: 148991 Color: 1
Size: 103115 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 748374 Color: 0
Size: 135283 Color: 0
Size: 116344 Color: 1

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 748348 Color: 1
Size: 139262 Color: 0
Size: 112391 Color: 1

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 749670 Color: 0
Size: 141806 Color: 1
Size: 108525 Color: 1

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 750220 Color: 0
Size: 144760 Color: 0
Size: 105021 Color: 1

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 750270 Color: 0
Size: 136838 Color: 1
Size: 112893 Color: 1

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 750572 Color: 0
Size: 130241 Color: 0
Size: 119188 Color: 1

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 750748 Color: 0
Size: 130849 Color: 1
Size: 118404 Color: 1

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 751310 Color: 1
Size: 132761 Color: 1
Size: 115930 Color: 0

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 751622 Color: 0
Size: 131832 Color: 1
Size: 116547 Color: 0

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 756073 Color: 1
Size: 130238 Color: 0
Size: 113690 Color: 1

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 756296 Color: 1
Size: 124539 Color: 0
Size: 119166 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 756508 Color: 1
Size: 122760 Color: 1
Size: 120733 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 758411 Color: 0
Size: 123562 Color: 1
Size: 118028 Color: 1

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 762268 Color: 0
Size: 119879 Color: 0
Size: 117854 Color: 1

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 764861 Color: 0
Size: 127170 Color: 1
Size: 107970 Color: 1

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 764938 Color: 0
Size: 130167 Color: 1
Size: 104896 Color: 1

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 765633 Color: 0
Size: 123888 Color: 1
Size: 110480 Color: 1

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 770878 Color: 1
Size: 229123 Color: 0

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 771615 Color: 0
Size: 124904 Color: 1
Size: 103482 Color: 1

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 771639 Color: 0
Size: 114708 Color: 1
Size: 113654 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 771999 Color: 1
Size: 120669 Color: 0
Size: 107333 Color: 0

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 774262 Color: 1
Size: 225739 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 774993 Color: 1
Size: 120633 Color: 0
Size: 104375 Color: 1

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 775425 Color: 1
Size: 113585 Color: 1
Size: 110991 Color: 0

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 780864 Color: 1
Size: 113124 Color: 1
Size: 106013 Color: 0

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 782069 Color: 0
Size: 112009 Color: 0
Size: 105923 Color: 1

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 782744 Color: 1
Size: 109468 Color: 1
Size: 107789 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 782777 Color: 1
Size: 113600 Color: 1
Size: 103624 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 782855 Color: 1
Size: 116538 Color: 0
Size: 100608 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 782809 Color: 0
Size: 116967 Color: 0
Size: 100225 Color: 1

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 784144 Color: 1
Size: 215857 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 784366 Color: 0
Size: 114180 Color: 1
Size: 101455 Color: 1

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 789024 Color: 1
Size: 105999 Color: 0
Size: 104978 Color: 1

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 789355 Color: 1
Size: 108503 Color: 0
Size: 102143 Color: 0

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 789412 Color: 0
Size: 210589 Color: 1

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 794599 Color: 0
Size: 104440 Color: 1
Size: 100962 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 795664 Color: 1
Size: 102391 Color: 0
Size: 101946 Color: 1

Bin 496: 1 of cap free
Amount of items: 5
Items: 
Size: 253412 Color: 1
Size: 189664 Color: 0
Size: 189604 Color: 0
Size: 183945 Color: 1
Size: 183375 Color: 1

Bin 497: 1 of cap free
Amount of items: 3
Items: 
Size: 600926 Color: 1
Size: 266597 Color: 0
Size: 132477 Color: 1

Bin 498: 1 of cap free
Amount of items: 3
Items: 
Size: 641763 Color: 1
Size: 189469 Color: 0
Size: 168768 Color: 0

Bin 499: 1 of cap free
Amount of items: 3
Items: 
Size: 721153 Color: 0
Size: 166366 Color: 1
Size: 112481 Color: 1

Bin 500: 1 of cap free
Amount of items: 3
Items: 
Size: 588667 Color: 1
Size: 278725 Color: 0
Size: 132608 Color: 1

Bin 501: 1 of cap free
Amount of items: 3
Items: 
Size: 758411 Color: 0
Size: 136317 Color: 0
Size: 105272 Color: 1

Bin 502: 1 of cap free
Amount of items: 3
Items: 
Size: 712478 Color: 1
Size: 165553 Color: 1
Size: 121969 Color: 0

Bin 503: 1 of cap free
Amount of items: 6
Items: 
Size: 173107 Color: 0
Size: 172606 Color: 0
Size: 168222 Color: 1
Size: 167640 Color: 1
Size: 167362 Color: 1
Size: 151063 Color: 0

Bin 504: 1 of cap free
Amount of items: 3
Items: 
Size: 611357 Color: 0
Size: 205431 Color: 1
Size: 183212 Color: 1

Bin 505: 1 of cap free
Amount of items: 3
Items: 
Size: 436037 Color: 0
Size: 398811 Color: 1
Size: 165152 Color: 0

Bin 506: 1 of cap free
Amount of items: 3
Items: 
Size: 604135 Color: 1
Size: 265223 Color: 0
Size: 130642 Color: 1

Bin 507: 1 of cap free
Amount of items: 3
Items: 
Size: 447280 Color: 1
Size: 408887 Color: 0
Size: 143833 Color: 1

Bin 508: 1 of cap free
Amount of items: 3
Items: 
Size: 756513 Color: 1
Size: 132256 Color: 0
Size: 111231 Color: 0

Bin 509: 1 of cap free
Amount of items: 3
Items: 
Size: 588407 Color: 0
Size: 294298 Color: 1
Size: 117295 Color: 0

Bin 510: 1 of cap free
Amount of items: 6
Items: 
Size: 228047 Color: 0
Size: 166448 Color: 0
Size: 166318 Color: 0
Size: 161473 Color: 1
Size: 161338 Color: 1
Size: 116376 Color: 1

Bin 511: 1 of cap free
Amount of items: 3
Items: 
Size: 471599 Color: 1
Size: 427732 Color: 0
Size: 100669 Color: 0

Bin 512: 1 of cap free
Amount of items: 3
Items: 
Size: 762533 Color: 0
Size: 124365 Color: 1
Size: 113102 Color: 1

Bin 513: 1 of cap free
Amount of items: 3
Items: 
Size: 729122 Color: 0
Size: 165599 Color: 1
Size: 105279 Color: 0

Bin 514: 1 of cap free
Amount of items: 3
Items: 
Size: 602346 Color: 1
Size: 232994 Color: 0
Size: 164660 Color: 1

Bin 515: 1 of cap free
Amount of items: 3
Items: 
Size: 623101 Color: 0
Size: 248058 Color: 0
Size: 128841 Color: 1

Bin 516: 1 of cap free
Amount of items: 3
Items: 
Size: 428117 Color: 0
Size: 421713 Color: 1
Size: 150170 Color: 0

Bin 517: 1 of cap free
Amount of items: 2
Items: 
Size: 797433 Color: 1
Size: 202567 Color: 0

Bin 518: 1 of cap free
Amount of items: 3
Items: 
Size: 782135 Color: 1
Size: 117156 Color: 1
Size: 100709 Color: 0

Bin 519: 1 of cap free
Amount of items: 3
Items: 
Size: 586445 Color: 0
Size: 290676 Color: 1
Size: 122879 Color: 1

Bin 520: 1 of cap free
Amount of items: 3
Items: 
Size: 781411 Color: 1
Size: 116635 Color: 1
Size: 101954 Color: 0

Bin 521: 1 of cap free
Amount of items: 3
Items: 
Size: 445356 Color: 1
Size: 437674 Color: 0
Size: 116970 Color: 0

Bin 522: 1 of cap free
Amount of items: 6
Items: 
Size: 202663 Color: 0
Size: 167554 Color: 0
Size: 166468 Color: 0
Size: 162580 Color: 1
Size: 161138 Color: 1
Size: 139597 Color: 1

Bin 523: 1 of cap free
Amount of items: 3
Items: 
Size: 462805 Color: 0
Size: 432664 Color: 1
Size: 104531 Color: 1

Bin 524: 1 of cap free
Amount of items: 3
Items: 
Size: 588216 Color: 0
Size: 230732 Color: 0
Size: 181052 Color: 1

Bin 525: 1 of cap free
Amount of items: 3
Items: 
Size: 762266 Color: 1
Size: 129409 Color: 1
Size: 108325 Color: 0

Bin 526: 1 of cap free
Amount of items: 5
Items: 
Size: 251514 Color: 0
Size: 190289 Color: 0
Size: 190039 Color: 0
Size: 184162 Color: 1
Size: 183996 Color: 1

Bin 527: 1 of cap free
Amount of items: 3
Items: 
Size: 782258 Color: 0
Size: 110579 Color: 1
Size: 107163 Color: 0

Bin 528: 1 of cap free
Amount of items: 3
Items: 
Size: 718420 Color: 0
Size: 160992 Color: 0
Size: 120588 Color: 1

Bin 529: 1 of cap free
Amount of items: 3
Items: 
Size: 578455 Color: 0
Size: 306305 Color: 1
Size: 115240 Color: 0

Bin 530: 1 of cap free
Amount of items: 3
Items: 
Size: 757449 Color: 0
Size: 124668 Color: 1
Size: 117883 Color: 0

Bin 531: 1 of cap free
Amount of items: 3
Items: 
Size: 587596 Color: 1
Size: 286952 Color: 0
Size: 125452 Color: 1

Bin 532: 1 of cap free
Amount of items: 5
Items: 
Size: 298796 Color: 0
Size: 177454 Color: 0
Size: 176587 Color: 0
Size: 174997 Color: 1
Size: 172166 Color: 1

Bin 533: 1 of cap free
Amount of items: 3
Items: 
Size: 431031 Color: 0
Size: 301300 Color: 1
Size: 267669 Color: 1

Bin 534: 1 of cap free
Amount of items: 6
Items: 
Size: 182175 Color: 1
Size: 165683 Color: 0
Size: 165654 Color: 0
Size: 165491 Color: 0
Size: 160623 Color: 1
Size: 160374 Color: 1

Bin 535: 1 of cap free
Amount of items: 3
Items: 
Size: 427750 Color: 1
Size: 411741 Color: 0
Size: 160509 Color: 0

Bin 536: 1 of cap free
Amount of items: 3
Items: 
Size: 434422 Color: 0
Size: 378343 Color: 1
Size: 187235 Color: 1

Bin 537: 1 of cap free
Amount of items: 3
Items: 
Size: 436122 Color: 0
Size: 377651 Color: 0
Size: 186227 Color: 1

Bin 538: 1 of cap free
Amount of items: 3
Items: 
Size: 442450 Color: 1
Size: 416033 Color: 1
Size: 141517 Color: 0

Bin 539: 1 of cap free
Amount of items: 3
Items: 
Size: 442924 Color: 1
Size: 362737 Color: 0
Size: 194339 Color: 1

Bin 540: 1 of cap free
Amount of items: 3
Items: 
Size: 446030 Color: 1
Size: 421357 Color: 1
Size: 132613 Color: 0

Bin 541: 1 of cap free
Amount of items: 3
Items: 
Size: 447349 Color: 1
Size: 405248 Color: 0
Size: 147403 Color: 0

Bin 542: 1 of cap free
Amount of items: 3
Items: 
Size: 448225 Color: 1
Size: 410776 Color: 0
Size: 140999 Color: 1

Bin 543: 1 of cap free
Amount of items: 3
Items: 
Size: 450772 Color: 1
Size: 430052 Color: 0
Size: 119176 Color: 1

Bin 544: 1 of cap free
Amount of items: 3
Items: 
Size: 452308 Color: 1
Size: 420690 Color: 1
Size: 127002 Color: 0

Bin 545: 1 of cap free
Amount of items: 3
Items: 
Size: 462621 Color: 0
Size: 344494 Color: 1
Size: 192885 Color: 1

Bin 546: 1 of cap free
Amount of items: 3
Items: 
Size: 467740 Color: 1
Size: 272169 Color: 0
Size: 260091 Color: 1

Bin 547: 1 of cap free
Amount of items: 3
Items: 
Size: 468997 Color: 1
Size: 337705 Color: 0
Size: 193298 Color: 1

Bin 548: 1 of cap free
Amount of items: 3
Items: 
Size: 469417 Color: 1
Size: 378085 Color: 0
Size: 152498 Color: 0

Bin 549: 1 of cap free
Amount of items: 3
Items: 
Size: 471512 Color: 1
Size: 340579 Color: 0
Size: 187909 Color: 1

Bin 550: 1 of cap free
Amount of items: 3
Items: 
Size: 482111 Color: 0
Size: 324612 Color: 1
Size: 193277 Color: 0

Bin 551: 1 of cap free
Amount of items: 3
Items: 
Size: 483030 Color: 0
Size: 322451 Color: 1
Size: 194519 Color: 1

Bin 552: 1 of cap free
Amount of items: 3
Items: 
Size: 496706 Color: 0
Size: 324768 Color: 1
Size: 178526 Color: 1

Bin 553: 1 of cap free
Amount of items: 3
Items: 
Size: 499415 Color: 0
Size: 377395 Color: 0
Size: 123190 Color: 1

Bin 554: 1 of cap free
Amount of items: 3
Items: 
Size: 500039 Color: 0
Size: 352778 Color: 1
Size: 147183 Color: 0

Bin 555: 1 of cap free
Amount of items: 2
Items: 
Size: 503699 Color: 1
Size: 496301 Color: 0

Bin 556: 1 of cap free
Amount of items: 3
Items: 
Size: 511187 Color: 1
Size: 323681 Color: 1
Size: 165132 Color: 0

Bin 557: 1 of cap free
Amount of items: 2
Items: 
Size: 511911 Color: 0
Size: 488089 Color: 1

Bin 558: 1 of cap free
Amount of items: 3
Items: 
Size: 512263 Color: 1
Size: 323854 Color: 1
Size: 163883 Color: 0

Bin 559: 1 of cap free
Amount of items: 3
Items: 
Size: 512034 Color: 0
Size: 268402 Color: 1
Size: 219564 Color: 0

Bin 560: 1 of cap free
Amount of items: 2
Items: 
Size: 521170 Color: 0
Size: 478830 Color: 1

Bin 561: 1 of cap free
Amount of items: 2
Items: 
Size: 522927 Color: 1
Size: 477073 Color: 0

Bin 562: 1 of cap free
Amount of items: 2
Items: 
Size: 524832 Color: 1
Size: 475168 Color: 0

Bin 563: 1 of cap free
Amount of items: 2
Items: 
Size: 525809 Color: 0
Size: 474191 Color: 1

Bin 564: 1 of cap free
Amount of items: 3
Items: 
Size: 526788 Color: 1
Size: 343570 Color: 0
Size: 129642 Color: 1

Bin 565: 1 of cap free
Amount of items: 3
Items: 
Size: 533902 Color: 1
Size: 303545 Color: 1
Size: 162553 Color: 0

Bin 566: 1 of cap free
Amount of items: 3
Items: 
Size: 535259 Color: 0
Size: 337706 Color: 0
Size: 127035 Color: 1

Bin 567: 1 of cap free
Amount of items: 3
Items: 
Size: 546799 Color: 0
Size: 323842 Color: 1
Size: 129359 Color: 0

Bin 568: 1 of cap free
Amount of items: 2
Items: 
Size: 565805 Color: 0
Size: 434195 Color: 1

Bin 569: 1 of cap free
Amount of items: 3
Items: 
Size: 572420 Color: 0
Size: 302429 Color: 0
Size: 125151 Color: 1

Bin 570: 1 of cap free
Amount of items: 2
Items: 
Size: 572794 Color: 1
Size: 427206 Color: 0

Bin 571: 1 of cap free
Amount of items: 3
Items: 
Size: 577962 Color: 0
Size: 290270 Color: 1
Size: 131768 Color: 0

Bin 572: 1 of cap free
Amount of items: 3
Items: 
Size: 585608 Color: 0
Size: 276260 Color: 0
Size: 138132 Color: 1

Bin 573: 1 of cap free
Amount of items: 2
Items: 
Size: 586414 Color: 0
Size: 413586 Color: 1

Bin 574: 1 of cap free
Amount of items: 3
Items: 
Size: 587869 Color: 1
Size: 230006 Color: 0
Size: 182125 Color: 1

Bin 575: 1 of cap free
Amount of items: 3
Items: 
Size: 588846 Color: 0
Size: 252223 Color: 0
Size: 158931 Color: 1

Bin 576: 1 of cap free
Amount of items: 2
Items: 
Size: 593033 Color: 1
Size: 406967 Color: 0

Bin 577: 1 of cap free
Amount of items: 3
Items: 
Size: 596868 Color: 1
Size: 266848 Color: 1
Size: 136284 Color: 0

Bin 578: 1 of cap free
Amount of items: 3
Items: 
Size: 597771 Color: 1
Size: 204178 Color: 0
Size: 198051 Color: 1

Bin 579: 1 of cap free
Amount of items: 3
Items: 
Size: 597850 Color: 0
Size: 244895 Color: 0
Size: 157255 Color: 1

Bin 580: 1 of cap free
Amount of items: 3
Items: 
Size: 598254 Color: 1
Size: 245789 Color: 0
Size: 155957 Color: 1

Bin 581: 1 of cap free
Amount of items: 3
Items: 
Size: 598485 Color: 1
Size: 263106 Color: 0
Size: 138409 Color: 0

Bin 582: 1 of cap free
Amount of items: 3
Items: 
Size: 598619 Color: 1
Size: 232624 Color: 0
Size: 168757 Color: 1

Bin 583: 1 of cap free
Amount of items: 3
Items: 
Size: 602171 Color: 1
Size: 252241 Color: 0
Size: 145588 Color: 0

Bin 584: 1 of cap free
Amount of items: 3
Items: 
Size: 604878 Color: 0
Size: 279421 Color: 1
Size: 115701 Color: 0

Bin 585: 1 of cap free
Amount of items: 2
Items: 
Size: 607291 Color: 0
Size: 392709 Color: 1

Bin 586: 1 of cap free
Amount of items: 2
Items: 
Size: 609633 Color: 0
Size: 390367 Color: 1

Bin 587: 1 of cap free
Amount of items: 3
Items: 
Size: 609897 Color: 1
Size: 255681 Color: 0
Size: 134422 Color: 1

Bin 588: 1 of cap free
Amount of items: 2
Items: 
Size: 613236 Color: 1
Size: 386764 Color: 0

Bin 589: 1 of cap free
Amount of items: 3
Items: 
Size: 617362 Color: 0
Size: 217774 Color: 1
Size: 164864 Color: 0

Bin 590: 1 of cap free
Amount of items: 3
Items: 
Size: 617430 Color: 0
Size: 279432 Color: 1
Size: 103138 Color: 1

Bin 591: 1 of cap free
Amount of items: 3
Items: 
Size: 618414 Color: 0
Size: 253545 Color: 1
Size: 128041 Color: 1

Bin 592: 1 of cap free
Amount of items: 2
Items: 
Size: 621795 Color: 1
Size: 378205 Color: 0

Bin 593: 1 of cap free
Amount of items: 2
Items: 
Size: 628930 Color: 1
Size: 371070 Color: 0

Bin 594: 1 of cap free
Amount of items: 3
Items: 
Size: 630005 Color: 0
Size: 194806 Color: 1
Size: 175189 Color: 0

Bin 595: 1 of cap free
Amount of items: 3
Items: 
Size: 630394 Color: 1
Size: 263598 Color: 0
Size: 106008 Color: 1

Bin 596: 1 of cap free
Amount of items: 3
Items: 
Size: 631698 Color: 1
Size: 198300 Color: 0
Size: 170002 Color: 1

Bin 597: 1 of cap free
Amount of items: 3
Items: 
Size: 632740 Color: 0
Size: 190989 Color: 1
Size: 176271 Color: 1

Bin 598: 1 of cap free
Amount of items: 3
Items: 
Size: 636112 Color: 0
Size: 221537 Color: 1
Size: 142351 Color: 0

Bin 599: 1 of cap free
Amount of items: 3
Items: 
Size: 641633 Color: 1
Size: 188104 Color: 0
Size: 170263 Color: 1

Bin 600: 1 of cap free
Amount of items: 3
Items: 
Size: 642415 Color: 1
Size: 193167 Color: 1
Size: 164418 Color: 0

Bin 601: 1 of cap free
Amount of items: 3
Items: 
Size: 650254 Color: 0
Size: 190765 Color: 1
Size: 158981 Color: 1

Bin 602: 1 of cap free
Amount of items: 2
Items: 
Size: 653216 Color: 0
Size: 346784 Color: 1

Bin 603: 1 of cap free
Amount of items: 2
Items: 
Size: 657228 Color: 0
Size: 342772 Color: 1

Bin 604: 1 of cap free
Amount of items: 2
Items: 
Size: 664418 Color: 1
Size: 335582 Color: 0

Bin 605: 1 of cap free
Amount of items: 3
Items: 
Size: 685514 Color: 0
Size: 186658 Color: 1
Size: 127828 Color: 1

Bin 606: 1 of cap free
Amount of items: 2
Items: 
Size: 692386 Color: 1
Size: 307614 Color: 0

Bin 607: 1 of cap free
Amount of items: 2
Items: 
Size: 695717 Color: 0
Size: 304283 Color: 1

Bin 608: 1 of cap free
Amount of items: 3
Items: 
Size: 711781 Color: 1
Size: 167250 Color: 1
Size: 120969 Color: 0

Bin 609: 1 of cap free
Amount of items: 2
Items: 
Size: 712343 Color: 1
Size: 287657 Color: 0

Bin 610: 1 of cap free
Amount of items: 2
Items: 
Size: 714923 Color: 1
Size: 285077 Color: 0

Bin 611: 1 of cap free
Amount of items: 2
Items: 
Size: 718780 Color: 0
Size: 281220 Color: 1

Bin 612: 1 of cap free
Amount of items: 3
Items: 
Size: 721430 Color: 0
Size: 158206 Color: 1
Size: 120364 Color: 1

Bin 613: 1 of cap free
Amount of items: 2
Items: 
Size: 728836 Color: 0
Size: 271164 Color: 1

Bin 614: 1 of cap free
Amount of items: 2
Items: 
Size: 729103 Color: 1
Size: 270897 Color: 0

Bin 615: 1 of cap free
Amount of items: 3
Items: 
Size: 730999 Color: 1
Size: 140897 Color: 0
Size: 128104 Color: 0

Bin 616: 1 of cap free
Amount of items: 2
Items: 
Size: 732150 Color: 1
Size: 267850 Color: 0

Bin 617: 1 of cap free
Amount of items: 3
Items: 
Size: 751428 Color: 0
Size: 130499 Color: 1
Size: 118073 Color: 0

Bin 618: 1 of cap free
Amount of items: 2
Items: 
Size: 758722 Color: 1
Size: 241278 Color: 0

Bin 619: 1 of cap free
Amount of items: 3
Items: 
Size: 764802 Color: 0
Size: 134050 Color: 1
Size: 101148 Color: 0

Bin 620: 1 of cap free
Amount of items: 3
Items: 
Size: 775524 Color: 1
Size: 118996 Color: 0
Size: 105480 Color: 0

Bin 621: 1 of cap free
Amount of items: 2
Items: 
Size: 776774 Color: 0
Size: 223226 Color: 1

Bin 622: 1 of cap free
Amount of items: 2
Items: 
Size: 789725 Color: 1
Size: 210275 Color: 0

Bin 623: 2 of cap free
Amount of items: 3
Items: 
Size: 674906 Color: 1
Size: 168109 Color: 0
Size: 156984 Color: 0

Bin 624: 2 of cap free
Amount of items: 3
Items: 
Size: 787303 Color: 0
Size: 109797 Color: 1
Size: 102899 Color: 0

Bin 625: 2 of cap free
Amount of items: 3
Items: 
Size: 578347 Color: 1
Size: 223153 Color: 1
Size: 198499 Color: 0

Bin 626: 2 of cap free
Amount of items: 5
Items: 
Size: 244997 Color: 0
Size: 241818 Color: 1
Size: 231730 Color: 0
Size: 156617 Color: 1
Size: 124837 Color: 1

Bin 627: 2 of cap free
Amount of items: 3
Items: 
Size: 594925 Color: 0
Size: 263962 Color: 0
Size: 141112 Color: 1

Bin 628: 2 of cap free
Amount of items: 3
Items: 
Size: 617227 Color: 0
Size: 235550 Color: 0
Size: 147222 Color: 1

Bin 629: 2 of cap free
Amount of items: 3
Items: 
Size: 345679 Color: 1
Size: 329961 Color: 0
Size: 324359 Color: 1

Bin 630: 2 of cap free
Amount of items: 3
Items: 
Size: 441903 Color: 1
Size: 411915 Color: 0
Size: 146181 Color: 0

Bin 631: 2 of cap free
Amount of items: 3
Items: 
Size: 618955 Color: 1
Size: 246342 Color: 0
Size: 134702 Color: 0

Bin 632: 2 of cap free
Amount of items: 3
Items: 
Size: 664110 Color: 0
Size: 187559 Color: 1
Size: 148330 Color: 1

Bin 633: 2 of cap free
Amount of items: 3
Items: 
Size: 772221 Color: 0
Size: 124453 Color: 1
Size: 103325 Color: 1

Bin 634: 2 of cap free
Amount of items: 6
Items: 
Size: 177830 Color: 0
Size: 177727 Color: 0
Size: 173492 Color: 1
Size: 173192 Color: 1
Size: 172805 Color: 1
Size: 124953 Color: 0

Bin 635: 2 of cap free
Amount of items: 3
Items: 
Size: 776099 Color: 1
Size: 113691 Color: 0
Size: 110209 Color: 0

Bin 636: 2 of cap free
Amount of items: 3
Items: 
Size: 764867 Color: 0
Size: 118070 Color: 0
Size: 117062 Color: 1

Bin 637: 2 of cap free
Amount of items: 5
Items: 
Size: 255034 Color: 0
Size: 254448 Color: 1
Size: 253594 Color: 1
Size: 119644 Color: 0
Size: 117279 Color: 1

Bin 638: 2 of cap free
Amount of items: 7
Items: 
Size: 151293 Color: 0
Size: 150745 Color: 0
Size: 150528 Color: 0
Size: 146141 Color: 1
Size: 145533 Color: 1
Size: 131836 Color: 0
Size: 123923 Color: 1

Bin 639: 2 of cap free
Amount of items: 3
Items: 
Size: 534303 Color: 1
Size: 339254 Color: 0
Size: 126442 Color: 1

Bin 640: 2 of cap free
Amount of items: 3
Items: 
Size: 578105 Color: 1
Size: 296089 Color: 1
Size: 125805 Color: 0

Bin 641: 2 of cap free
Amount of items: 3
Items: 
Size: 343235 Color: 0
Size: 338925 Color: 1
Size: 317839 Color: 0

Bin 642: 2 of cap free
Amount of items: 3
Items: 
Size: 525939 Color: 0
Size: 349877 Color: 1
Size: 124183 Color: 0

Bin 643: 2 of cap free
Amount of items: 3
Items: 
Size: 485544 Color: 0
Size: 396304 Color: 1
Size: 118151 Color: 0

Bin 644: 2 of cap free
Amount of items: 3
Items: 
Size: 464031 Color: 1
Size: 269357 Color: 0
Size: 266611 Color: 0

Bin 645: 2 of cap free
Amount of items: 3
Items: 
Size: 604762 Color: 1
Size: 222944 Color: 1
Size: 172293 Color: 0

Bin 646: 2 of cap free
Amount of items: 3
Items: 
Size: 452138 Color: 1
Size: 427576 Color: 0
Size: 120285 Color: 1

Bin 647: 2 of cap free
Amount of items: 5
Items: 
Size: 290875 Color: 0
Size: 181299 Color: 0
Size: 177568 Color: 1
Size: 176188 Color: 1
Size: 174069 Color: 1

Bin 648: 2 of cap free
Amount of items: 3
Items: 
Size: 467638 Color: 1
Size: 409015 Color: 0
Size: 123346 Color: 0

Bin 649: 2 of cap free
Amount of items: 3
Items: 
Size: 511317 Color: 0
Size: 361505 Color: 1
Size: 127177 Color: 0

Bin 650: 2 of cap free
Amount of items: 3
Items: 
Size: 689234 Color: 0
Size: 208453 Color: 1
Size: 102312 Color: 1

Bin 651: 2 of cap free
Amount of items: 3
Items: 
Size: 533808 Color: 0
Size: 350604 Color: 0
Size: 115587 Color: 1

Bin 652: 2 of cap free
Amount of items: 3
Items: 
Size: 597403 Color: 1
Size: 209461 Color: 1
Size: 193135 Color: 0

Bin 653: 2 of cap free
Amount of items: 3
Items: 
Size: 576183 Color: 1
Size: 275780 Color: 0
Size: 148036 Color: 0

Bin 654: 2 of cap free
Amount of items: 2
Items: 
Size: 784281 Color: 0
Size: 215718 Color: 1

Bin 655: 2 of cap free
Amount of items: 3
Items: 
Size: 494953 Color: 1
Size: 256031 Color: 0
Size: 249015 Color: 0

Bin 656: 2 of cap free
Amount of items: 3
Items: 
Size: 374361 Color: 0
Size: 324314 Color: 1
Size: 301324 Color: 0

Bin 657: 2 of cap free
Amount of items: 3
Items: 
Size: 432762 Color: 1
Size: 373495 Color: 0
Size: 193742 Color: 1

Bin 658: 2 of cap free
Amount of items: 3
Items: 
Size: 435710 Color: 0
Size: 398757 Color: 1
Size: 165532 Color: 0

Bin 659: 2 of cap free
Amount of items: 3
Items: 
Size: 435949 Color: 0
Size: 374281 Color: 0
Size: 189769 Color: 1

Bin 660: 2 of cap free
Amount of items: 3
Items: 
Size: 442917 Color: 0
Size: 389360 Color: 1
Size: 167722 Color: 0

Bin 661: 2 of cap free
Amount of items: 3
Items: 
Size: 445358 Color: 1
Size: 404948 Color: 0
Size: 149693 Color: 1

Bin 662: 2 of cap free
Amount of items: 3
Items: 
Size: 447253 Color: 1
Size: 278400 Color: 0
Size: 274346 Color: 0

Bin 663: 2 of cap free
Amount of items: 3
Items: 
Size: 451647 Color: 1
Size: 356026 Color: 1
Size: 192326 Color: 0

Bin 664: 2 of cap free
Amount of items: 3
Items: 
Size: 451763 Color: 1
Size: 405047 Color: 0
Size: 143189 Color: 0

Bin 665: 2 of cap free
Amount of items: 3
Items: 
Size: 451857 Color: 1
Size: 427936 Color: 0
Size: 120206 Color: 1

Bin 666: 2 of cap free
Amount of items: 3
Items: 
Size: 456181 Color: 0
Size: 378004 Color: 0
Size: 165814 Color: 1

Bin 667: 2 of cap free
Amount of items: 3
Items: 
Size: 456394 Color: 0
Size: 398872 Color: 1
Size: 144733 Color: 1

Bin 668: 2 of cap free
Amount of items: 3
Items: 
Size: 461871 Color: 0
Size: 355816 Color: 1
Size: 182312 Color: 1

Bin 669: 2 of cap free
Amount of items: 3
Items: 
Size: 469467 Color: 1
Size: 405025 Color: 0
Size: 125507 Color: 0

Bin 670: 2 of cap free
Amount of items: 3
Items: 
Size: 500545 Color: 0
Size: 322393 Color: 1
Size: 177061 Color: 0

Bin 671: 2 of cap free
Amount of items: 2
Items: 
Size: 504514 Color: 1
Size: 495485 Color: 0

Bin 672: 2 of cap free
Amount of items: 2
Items: 
Size: 504712 Color: 0
Size: 495287 Color: 1

Bin 673: 2 of cap free
Amount of items: 3
Items: 
Size: 509801 Color: 0
Size: 378522 Color: 0
Size: 111676 Color: 1

Bin 674: 2 of cap free
Amount of items: 3
Items: 
Size: 511164 Color: 1
Size: 345106 Color: 0
Size: 143729 Color: 1

Bin 675: 2 of cap free
Amount of items: 3
Items: 
Size: 511438 Color: 0
Size: 264002 Color: 0
Size: 224559 Color: 1

Bin 676: 2 of cap free
Amount of items: 2
Items: 
Size: 519148 Color: 1
Size: 480851 Color: 0

Bin 677: 2 of cap free
Amount of items: 3
Items: 
Size: 527027 Color: 1
Size: 349785 Color: 1
Size: 123187 Color: 0

Bin 678: 2 of cap free
Amount of items: 2
Items: 
Size: 527460 Color: 0
Size: 472539 Color: 1

Bin 679: 2 of cap free
Amount of items: 2
Items: 
Size: 527654 Color: 1
Size: 472345 Color: 0

Bin 680: 2 of cap free
Amount of items: 2
Items: 
Size: 532181 Color: 0
Size: 467818 Color: 1

Bin 681: 2 of cap free
Amount of items: 2
Items: 
Size: 535367 Color: 1
Size: 464632 Color: 0

Bin 682: 2 of cap free
Amount of items: 3
Items: 
Size: 536041 Color: 1
Size: 290737 Color: 1
Size: 173221 Color: 0

Bin 683: 2 of cap free
Amount of items: 2
Items: 
Size: 542210 Color: 1
Size: 457789 Color: 0

Bin 684: 2 of cap free
Amount of items: 3
Items: 
Size: 545442 Color: 0
Size: 301342 Color: 0
Size: 153215 Color: 1

Bin 685: 2 of cap free
Amount of items: 2
Items: 
Size: 547973 Color: 0
Size: 452026 Color: 1

Bin 686: 2 of cap free
Amount of items: 2
Items: 
Size: 560806 Color: 1
Size: 439193 Color: 0

Bin 687: 2 of cap free
Amount of items: 2
Items: 
Size: 562700 Color: 0
Size: 437299 Color: 1

Bin 688: 2 of cap free
Amount of items: 3
Items: 
Size: 569576 Color: 1
Size: 303796 Color: 0
Size: 126627 Color: 0

Bin 689: 2 of cap free
Amount of items: 3
Items: 
Size: 573760 Color: 1
Size: 232513 Color: 0
Size: 193726 Color: 1

Bin 690: 2 of cap free
Amount of items: 3
Items: 
Size: 578141 Color: 1
Size: 245345 Color: 0
Size: 176513 Color: 0

Bin 691: 2 of cap free
Amount of items: 3
Items: 
Size: 578850 Color: 1
Size: 272298 Color: 0
Size: 148851 Color: 0

Bin 692: 2 of cap free
Amount of items: 3
Items: 
Size: 595249 Color: 0
Size: 296892 Color: 0
Size: 107858 Color: 1

Bin 693: 2 of cap free
Amount of items: 2
Items: 
Size: 597912 Color: 1
Size: 402087 Color: 0

Bin 694: 2 of cap free
Amount of items: 2
Items: 
Size: 606862 Color: 0
Size: 393137 Color: 1

Bin 695: 2 of cap free
Amount of items: 2
Items: 
Size: 615993 Color: 1
Size: 384006 Color: 0

Bin 696: 2 of cap free
Amount of items: 3
Items: 
Size: 618196 Color: 0
Size: 228459 Color: 1
Size: 153344 Color: 0

Bin 697: 2 of cap free
Amount of items: 3
Items: 
Size: 621834 Color: 0
Size: 219353 Color: 0
Size: 158812 Color: 1

Bin 698: 2 of cap free
Amount of items: 3
Items: 
Size: 627468 Color: 1
Size: 230362 Color: 0
Size: 142169 Color: 1

Bin 699: 2 of cap free
Amount of items: 2
Items: 
Size: 628719 Color: 1
Size: 371280 Color: 0

Bin 700: 2 of cap free
Amount of items: 2
Items: 
Size: 630852 Color: 1
Size: 369147 Color: 0

Bin 701: 2 of cap free
Amount of items: 2
Items: 
Size: 646866 Color: 0
Size: 353133 Color: 1

Bin 702: 2 of cap free
Amount of items: 2
Items: 
Size: 652882 Color: 0
Size: 347117 Color: 1

Bin 703: 2 of cap free
Amount of items: 2
Items: 
Size: 666157 Color: 0
Size: 333842 Color: 1

Bin 704: 2 of cap free
Amount of items: 2
Items: 
Size: 666781 Color: 1
Size: 333218 Color: 0

Bin 705: 2 of cap free
Amount of items: 2
Items: 
Size: 668486 Color: 1
Size: 331513 Color: 0

Bin 706: 2 of cap free
Amount of items: 2
Items: 
Size: 673328 Color: 0
Size: 326671 Color: 1

Bin 707: 2 of cap free
Amount of items: 2
Items: 
Size: 690567 Color: 0
Size: 309432 Color: 1

Bin 708: 2 of cap free
Amount of items: 2
Items: 
Size: 718678 Color: 0
Size: 281321 Color: 1

Bin 709: 2 of cap free
Amount of items: 2
Items: 
Size: 726620 Color: 0
Size: 273379 Color: 1

Bin 710: 2 of cap free
Amount of items: 2
Items: 
Size: 734606 Color: 0
Size: 265393 Color: 1

Bin 711: 2 of cap free
Amount of items: 2
Items: 
Size: 751792 Color: 0
Size: 248207 Color: 1

Bin 712: 2 of cap free
Amount of items: 2
Items: 
Size: 757656 Color: 0
Size: 242343 Color: 1

Bin 713: 2 of cap free
Amount of items: 2
Items: 
Size: 761647 Color: 1
Size: 238352 Color: 0

Bin 714: 2 of cap free
Amount of items: 3
Items: 
Size: 775717 Color: 1
Size: 117969 Color: 0
Size: 106313 Color: 1

Bin 715: 2 of cap free
Amount of items: 2
Items: 
Size: 783326 Color: 1
Size: 216673 Color: 0

Bin 716: 2 of cap free
Amount of items: 2
Items: 
Size: 788728 Color: 0
Size: 211271 Color: 1

Bin 717: 3 of cap free
Amount of items: 3
Items: 
Size: 597844 Color: 1
Size: 265181 Color: 0
Size: 136973 Color: 1

Bin 718: 3 of cap free
Amount of items: 3
Items: 
Size: 770014 Color: 0
Size: 115018 Color: 0
Size: 114966 Color: 1

Bin 719: 3 of cap free
Amount of items: 3
Items: 
Size: 468844 Color: 1
Size: 389700 Color: 1
Size: 141454 Color: 0

Bin 720: 3 of cap free
Amount of items: 3
Items: 
Size: 596251 Color: 1
Size: 266061 Color: 0
Size: 137686 Color: 0

Bin 721: 3 of cap free
Amount of items: 3
Items: 
Size: 477248 Color: 0
Size: 388839 Color: 1
Size: 133911 Color: 1

Bin 722: 3 of cap free
Amount of items: 2
Items: 
Size: 754948 Color: 0
Size: 245050 Color: 1

Bin 723: 3 of cap free
Amount of items: 3
Items: 
Size: 577630 Color: 1
Size: 252542 Color: 0
Size: 169826 Color: 0

Bin 724: 3 of cap free
Amount of items: 5
Items: 
Size: 230527 Color: 1
Size: 193994 Color: 0
Size: 193940 Color: 0
Size: 190905 Color: 1
Size: 190632 Color: 1

Bin 725: 3 of cap free
Amount of items: 2
Items: 
Size: 739321 Color: 0
Size: 260677 Color: 1

Bin 726: 3 of cap free
Amount of items: 3
Items: 
Size: 713661 Color: 0
Size: 165219 Color: 1
Size: 121118 Color: 1

Bin 727: 3 of cap free
Amount of items: 3
Items: 
Size: 586410 Color: 0
Size: 293903 Color: 1
Size: 119685 Color: 1

Bin 728: 3 of cap free
Amount of items: 3
Items: 
Size: 642248 Color: 1
Size: 219538 Color: 0
Size: 138212 Color: 0

Bin 729: 3 of cap free
Amount of items: 3
Items: 
Size: 662067 Color: 1
Size: 174416 Color: 0
Size: 163515 Color: 1

Bin 730: 3 of cap free
Amount of items: 3
Items: 
Size: 586996 Color: 0
Size: 273917 Color: 0
Size: 139085 Color: 1

Bin 731: 3 of cap free
Amount of items: 3
Items: 
Size: 638543 Color: 0
Size: 254669 Color: 1
Size: 106786 Color: 1

Bin 732: 3 of cap free
Amount of items: 3
Items: 
Size: 579133 Color: 1
Size: 227550 Color: 0
Size: 193315 Color: 1

Bin 733: 3 of cap free
Amount of items: 3
Items: 
Size: 451621 Color: 1
Size: 411961 Color: 0
Size: 136416 Color: 0

Bin 734: 3 of cap free
Amount of items: 4
Items: 
Size: 374125 Color: 0
Size: 243726 Color: 1
Size: 192401 Color: 0
Size: 189746 Color: 1

Bin 735: 3 of cap free
Amount of items: 3
Items: 
Size: 595477 Color: 1
Size: 244748 Color: 1
Size: 159773 Color: 0

Bin 736: 3 of cap free
Amount of items: 5
Items: 
Size: 323938 Color: 1
Size: 190238 Color: 0
Size: 164336 Color: 0
Size: 164162 Color: 0
Size: 157324 Color: 1

Bin 737: 3 of cap free
Amount of items: 3
Items: 
Size: 546384 Color: 1
Size: 226818 Color: 0
Size: 226796 Color: 0

Bin 738: 3 of cap free
Amount of items: 3
Items: 
Size: 657575 Color: 0
Size: 183730 Color: 1
Size: 158693 Color: 0

Bin 739: 3 of cap free
Amount of items: 3
Items: 
Size: 416212 Color: 1
Size: 395053 Color: 0
Size: 188733 Color: 0

Bin 740: 3 of cap free
Amount of items: 3
Items: 
Size: 437327 Color: 0
Size: 421246 Color: 1
Size: 141425 Color: 1

Bin 741: 3 of cap free
Amount of items: 3
Items: 
Size: 438474 Color: 0
Size: 391876 Color: 0
Size: 169648 Color: 1

Bin 742: 3 of cap free
Amount of items: 3
Items: 
Size: 445559 Color: 1
Size: 392240 Color: 0
Size: 162199 Color: 0

Bin 743: 3 of cap free
Amount of items: 3
Items: 
Size: 446165 Color: 1
Size: 434794 Color: 0
Size: 119039 Color: 1

Bin 744: 3 of cap free
Amount of items: 3
Items: 
Size: 447381 Color: 1
Size: 396247 Color: 1
Size: 156370 Color: 0

Bin 745: 3 of cap free
Amount of items: 3
Items: 
Size: 452061 Color: 1
Size: 405812 Color: 0
Size: 142125 Color: 0

Bin 746: 3 of cap free
Amount of items: 3
Items: 
Size: 500185 Color: 0
Size: 374057 Color: 0
Size: 125756 Color: 1

Bin 747: 3 of cap free
Amount of items: 2
Items: 
Size: 506352 Color: 0
Size: 493646 Color: 1

Bin 748: 3 of cap free
Amount of items: 3
Items: 
Size: 509211 Color: 0
Size: 324013 Color: 1
Size: 166774 Color: 1

Bin 749: 3 of cap free
Amount of items: 3
Items: 
Size: 510981 Color: 1
Size: 325457 Color: 1
Size: 163560 Color: 0

Bin 750: 3 of cap free
Amount of items: 2
Items: 
Size: 520027 Color: 1
Size: 479971 Color: 0

Bin 751: 3 of cap free
Amount of items: 2
Items: 
Size: 525277 Color: 0
Size: 474721 Color: 1

Bin 752: 3 of cap free
Amount of items: 3
Items: 
Size: 527372 Color: 1
Size: 326055 Color: 1
Size: 146571 Color: 0

Bin 753: 3 of cap free
Amount of items: 2
Items: 
Size: 531393 Color: 1
Size: 468605 Color: 0

Bin 754: 3 of cap free
Amount of items: 2
Items: 
Size: 532439 Color: 1
Size: 467559 Color: 0

Bin 755: 3 of cap free
Amount of items: 2
Items: 
Size: 534334 Color: 1
Size: 465664 Color: 0

Bin 756: 3 of cap free
Amount of items: 2
Items: 
Size: 551122 Color: 1
Size: 448876 Color: 0

Bin 757: 3 of cap free
Amount of items: 2
Items: 
Size: 561025 Color: 0
Size: 438973 Color: 1

Bin 758: 3 of cap free
Amount of items: 2
Items: 
Size: 565000 Color: 0
Size: 434998 Color: 1

Bin 759: 3 of cap free
Amount of items: 3
Items: 
Size: 586675 Color: 0
Size: 252692 Color: 1
Size: 160631 Color: 0

Bin 760: 3 of cap free
Amount of items: 3
Items: 
Size: 588083 Color: 0
Size: 256388 Color: 1
Size: 155527 Color: 0

Bin 761: 3 of cap free
Amount of items: 3
Items: 
Size: 588308 Color: 1
Size: 303606 Color: 1
Size: 108084 Color: 0

Bin 762: 3 of cap free
Amount of items: 2
Items: 
Size: 595308 Color: 1
Size: 404690 Color: 0

Bin 763: 3 of cap free
Amount of items: 3
Items: 
Size: 601746 Color: 1
Size: 230113 Color: 0
Size: 168139 Color: 1

Bin 764: 3 of cap free
Amount of items: 3
Items: 
Size: 602428 Color: 1
Size: 245633 Color: 0
Size: 151937 Color: 0

Bin 765: 3 of cap free
Amount of items: 2
Items: 
Size: 605773 Color: 0
Size: 394225 Color: 1

Bin 766: 3 of cap free
Amount of items: 3
Items: 
Size: 630157 Color: 1
Size: 207927 Color: 0
Size: 161914 Color: 0

Bin 767: 3 of cap free
Amount of items: 2
Items: 
Size: 633969 Color: 0
Size: 366029 Color: 1

Bin 768: 3 of cap free
Amount of items: 2
Items: 
Size: 667219 Color: 0
Size: 332779 Color: 1

Bin 769: 3 of cap free
Amount of items: 2
Items: 
Size: 670688 Color: 0
Size: 329310 Color: 1

Bin 770: 3 of cap free
Amount of items: 2
Items: 
Size: 670946 Color: 0
Size: 329052 Color: 1

Bin 771: 3 of cap free
Amount of items: 2
Items: 
Size: 677262 Color: 0
Size: 322736 Color: 1

Bin 772: 3 of cap free
Amount of items: 2
Items: 
Size: 679977 Color: 1
Size: 320021 Color: 0

Bin 773: 3 of cap free
Amount of items: 2
Items: 
Size: 698048 Color: 0
Size: 301950 Color: 1

Bin 774: 3 of cap free
Amount of items: 2
Items: 
Size: 719357 Color: 1
Size: 280641 Color: 0

Bin 775: 3 of cap free
Amount of items: 2
Items: 
Size: 728015 Color: 1
Size: 271983 Color: 0

Bin 776: 3 of cap free
Amount of items: 2
Items: 
Size: 736682 Color: 1
Size: 263316 Color: 0

Bin 777: 3 of cap free
Amount of items: 2
Items: 
Size: 759966 Color: 1
Size: 240032 Color: 0

Bin 778: 3 of cap free
Amount of items: 2
Items: 
Size: 761811 Color: 1
Size: 238187 Color: 0

Bin 779: 3 of cap free
Amount of items: 2
Items: 
Size: 793980 Color: 0
Size: 206018 Color: 1

Bin 780: 4 of cap free
Amount of items: 3
Items: 
Size: 710205 Color: 0
Size: 147194 Color: 0
Size: 142598 Color: 1

Bin 781: 4 of cap free
Amount of items: 3
Items: 
Size: 441640 Color: 1
Size: 417905 Color: 1
Size: 140452 Color: 0

Bin 782: 4 of cap free
Amount of items: 3
Items: 
Size: 595453 Color: 1
Size: 244644 Color: 1
Size: 159900 Color: 0

Bin 783: 4 of cap free
Amount of items: 3
Items: 
Size: 586009 Color: 0
Size: 229352 Color: 1
Size: 184636 Color: 1

Bin 784: 4 of cap free
Amount of items: 3
Items: 
Size: 787731 Color: 0
Size: 106207 Color: 0
Size: 106059 Color: 1

Bin 785: 4 of cap free
Amount of items: 3
Items: 
Size: 787167 Color: 0
Size: 107822 Color: 0
Size: 105008 Color: 1

Bin 786: 4 of cap free
Amount of items: 3
Items: 
Size: 575055 Color: 1
Size: 253592 Color: 0
Size: 171350 Color: 0

Bin 787: 4 of cap free
Amount of items: 3
Items: 
Size: 597402 Color: 0
Size: 266749 Color: 0
Size: 135846 Color: 1

Bin 788: 4 of cap free
Amount of items: 3
Items: 
Size: 472731 Color: 1
Size: 398797 Color: 1
Size: 128469 Color: 0

Bin 789: 4 of cap free
Amount of items: 3
Items: 
Size: 468285 Color: 1
Size: 405001 Color: 0
Size: 126711 Color: 1

Bin 790: 4 of cap free
Amount of items: 2
Items: 
Size: 719987 Color: 0
Size: 280010 Color: 1

Bin 791: 4 of cap free
Amount of items: 7
Items: 
Size: 159274 Color: 0
Size: 158973 Color: 0
Size: 157299 Color: 0
Size: 150906 Color: 1
Size: 150637 Color: 1
Size: 115434 Color: 0
Size: 107474 Color: 1

Bin 792: 4 of cap free
Amount of items: 3
Items: 
Size: 415734 Color: 1
Size: 409041 Color: 0
Size: 175222 Color: 1

Bin 793: 4 of cap free
Amount of items: 3
Items: 
Size: 434547 Color: 0
Size: 317524 Color: 1
Size: 247926 Color: 0

Bin 794: 4 of cap free
Amount of items: 3
Items: 
Size: 445873 Color: 0
Size: 427533 Color: 0
Size: 126591 Color: 1

Bin 795: 4 of cap free
Amount of items: 3
Items: 
Size: 448246 Color: 1
Size: 319279 Color: 0
Size: 232472 Color: 0

Bin 796: 4 of cap free
Amount of items: 3
Items: 
Size: 448597 Color: 1
Size: 411400 Color: 0
Size: 140000 Color: 1

Bin 797: 4 of cap free
Amount of items: 3
Items: 
Size: 462331 Color: 0
Size: 350944 Color: 0
Size: 186722 Color: 1

Bin 798: 4 of cap free
Amount of items: 3
Items: 
Size: 475268 Color: 0
Size: 333652 Color: 1
Size: 191077 Color: 1

Bin 799: 4 of cap free
Amount of items: 2
Items: 
Size: 509969 Color: 1
Size: 490028 Color: 0

Bin 800: 4 of cap free
Amount of items: 2
Items: 
Size: 510555 Color: 1
Size: 489442 Color: 0

Bin 801: 4 of cap free
Amount of items: 2
Items: 
Size: 512572 Color: 1
Size: 487425 Color: 0

Bin 802: 4 of cap free
Amount of items: 2
Items: 
Size: 522270 Color: 0
Size: 477727 Color: 1

Bin 803: 4 of cap free
Amount of items: 3
Items: 
Size: 527086 Color: 0
Size: 322475 Color: 0
Size: 150436 Color: 1

Bin 804: 4 of cap free
Amount of items: 2
Items: 
Size: 528927 Color: 0
Size: 471070 Color: 1

Bin 805: 4 of cap free
Amount of items: 2
Items: 
Size: 531020 Color: 1
Size: 468977 Color: 0

Bin 806: 4 of cap free
Amount of items: 3
Items: 
Size: 536387 Color: 0
Size: 301314 Color: 0
Size: 162296 Color: 1

Bin 807: 4 of cap free
Amount of items: 2
Items: 
Size: 566526 Color: 0
Size: 433471 Color: 1

Bin 808: 4 of cap free
Amount of items: 2
Items: 
Size: 568019 Color: 0
Size: 431978 Color: 1

Bin 809: 4 of cap free
Amount of items: 2
Items: 
Size: 568972 Color: 0
Size: 431025 Color: 1

Bin 810: 4 of cap free
Amount of items: 2
Items: 
Size: 569491 Color: 1
Size: 430506 Color: 0

Bin 811: 4 of cap free
Amount of items: 3
Items: 
Size: 577920 Color: 1
Size: 286938 Color: 0
Size: 135139 Color: 1

Bin 812: 4 of cap free
Amount of items: 3
Items: 
Size: 587654 Color: 0
Size: 230929 Color: 0
Size: 181414 Color: 1

Bin 813: 4 of cap free
Amount of items: 2
Items: 
Size: 590291 Color: 1
Size: 409706 Color: 0

Bin 814: 4 of cap free
Amount of items: 3
Items: 
Size: 595346 Color: 0
Size: 210014 Color: 1
Size: 194637 Color: 0

Bin 815: 4 of cap free
Amount of items: 2
Items: 
Size: 596953 Color: 0
Size: 403044 Color: 1

Bin 816: 4 of cap free
Amount of items: 2
Items: 
Size: 615550 Color: 0
Size: 384447 Color: 1

Bin 817: 4 of cap free
Amount of items: 2
Items: 
Size: 617989 Color: 1
Size: 382008 Color: 0

Bin 818: 4 of cap free
Amount of items: 2
Items: 
Size: 625972 Color: 0
Size: 374025 Color: 1

Bin 819: 4 of cap free
Amount of items: 2
Items: 
Size: 640383 Color: 0
Size: 359614 Color: 1

Bin 820: 4 of cap free
Amount of items: 2
Items: 
Size: 649265 Color: 0
Size: 350732 Color: 1

Bin 821: 4 of cap free
Amount of items: 2
Items: 
Size: 650665 Color: 0
Size: 349332 Color: 1

Bin 822: 4 of cap free
Amount of items: 2
Items: 
Size: 672028 Color: 1
Size: 327969 Color: 0

Bin 823: 4 of cap free
Amount of items: 2
Items: 
Size: 683031 Color: 1
Size: 316966 Color: 0

Bin 824: 4 of cap free
Amount of items: 2
Items: 
Size: 729777 Color: 1
Size: 270220 Color: 0

Bin 825: 4 of cap free
Amount of items: 2
Items: 
Size: 741307 Color: 1
Size: 258690 Color: 0

Bin 826: 4 of cap free
Amount of items: 2
Items: 
Size: 744884 Color: 0
Size: 255113 Color: 1

Bin 827: 4 of cap free
Amount of items: 2
Items: 
Size: 745641 Color: 1
Size: 254356 Color: 0

Bin 828: 4 of cap free
Amount of items: 2
Items: 
Size: 751271 Color: 1
Size: 248726 Color: 0

Bin 829: 4 of cap free
Amount of items: 2
Items: 
Size: 759284 Color: 0
Size: 240713 Color: 1

Bin 830: 4 of cap free
Amount of items: 2
Items: 
Size: 761450 Color: 1
Size: 238547 Color: 0

Bin 831: 4 of cap free
Amount of items: 2
Items: 
Size: 781512 Color: 0
Size: 218485 Color: 1

Bin 832: 4 of cap free
Amount of items: 2
Items: 
Size: 793242 Color: 1
Size: 206755 Color: 0

Bin 833: 5 of cap free
Amount of items: 3
Items: 
Size: 470341 Color: 1
Size: 395826 Color: 0
Size: 133829 Color: 1

Bin 834: 5 of cap free
Amount of items: 3
Items: 
Size: 781938 Color: 1
Size: 112876 Color: 0
Size: 105182 Color: 0

Bin 835: 5 of cap free
Amount of items: 3
Items: 
Size: 649546 Color: 1
Size: 238651 Color: 0
Size: 111799 Color: 1

Bin 836: 5 of cap free
Amount of items: 7
Items: 
Size: 160877 Color: 0
Size: 160740 Color: 0
Size: 160221 Color: 0
Size: 153039 Color: 0
Size: 152957 Color: 1
Size: 108288 Color: 1
Size: 103874 Color: 1

Bin 837: 5 of cap free
Amount of items: 3
Items: 
Size: 756217 Color: 0
Size: 137491 Color: 1
Size: 106288 Color: 0

Bin 838: 5 of cap free
Amount of items: 3
Items: 
Size: 415885 Color: 1
Size: 389172 Color: 1
Size: 194939 Color: 0

Bin 839: 5 of cap free
Amount of items: 3
Items: 
Size: 436683 Color: 0
Size: 378037 Color: 0
Size: 185276 Color: 1

Bin 840: 5 of cap free
Amount of items: 3
Items: 
Size: 441106 Color: 1
Size: 391711 Color: 0
Size: 167179 Color: 0

Bin 841: 5 of cap free
Amount of items: 3
Items: 
Size: 472398 Color: 1
Size: 338616 Color: 0
Size: 188982 Color: 1

Bin 842: 5 of cap free
Amount of items: 3
Items: 
Size: 500709 Color: 0
Size: 378007 Color: 0
Size: 121280 Color: 1

Bin 843: 5 of cap free
Amount of items: 2
Items: 
Size: 503095 Color: 0
Size: 496901 Color: 1

Bin 844: 5 of cap free
Amount of items: 2
Items: 
Size: 503092 Color: 1
Size: 496904 Color: 0

Bin 845: 5 of cap free
Amount of items: 2
Items: 
Size: 510390 Color: 0
Size: 489606 Color: 1

Bin 846: 5 of cap free
Amount of items: 2
Items: 
Size: 531687 Color: 1
Size: 468309 Color: 0

Bin 847: 5 of cap free
Amount of items: 2
Items: 
Size: 534636 Color: 0
Size: 465360 Color: 1

Bin 848: 5 of cap free
Amount of items: 2
Items: 
Size: 572098 Color: 1
Size: 427898 Color: 0

Bin 849: 5 of cap free
Amount of items: 2
Items: 
Size: 595914 Color: 0
Size: 404082 Color: 1

Bin 850: 5 of cap free
Amount of items: 2
Items: 
Size: 611626 Color: 1
Size: 388370 Color: 0

Bin 851: 5 of cap free
Amount of items: 2
Items: 
Size: 612156 Color: 0
Size: 387840 Color: 1

Bin 852: 5 of cap free
Amount of items: 2
Items: 
Size: 623260 Color: 0
Size: 376736 Color: 1

Bin 853: 5 of cap free
Amount of items: 2
Items: 
Size: 638268 Color: 1
Size: 361728 Color: 0

Bin 854: 5 of cap free
Amount of items: 2
Items: 
Size: 639782 Color: 1
Size: 360214 Color: 0

Bin 855: 5 of cap free
Amount of items: 2
Items: 
Size: 667592 Color: 1
Size: 332404 Color: 0

Bin 856: 5 of cap free
Amount of items: 2
Items: 
Size: 669836 Color: 0
Size: 330160 Color: 1

Bin 857: 5 of cap free
Amount of items: 2
Items: 
Size: 703843 Color: 0
Size: 296153 Color: 1

Bin 858: 5 of cap free
Amount of items: 2
Items: 
Size: 710253 Color: 1
Size: 289743 Color: 0

Bin 859: 5 of cap free
Amount of items: 2
Items: 
Size: 716540 Color: 0
Size: 283456 Color: 1

Bin 860: 5 of cap free
Amount of items: 2
Items: 
Size: 726361 Color: 0
Size: 273635 Color: 1

Bin 861: 5 of cap free
Amount of items: 2
Items: 
Size: 751914 Color: 1
Size: 248082 Color: 0

Bin 862: 5 of cap free
Amount of items: 2
Items: 
Size: 767394 Color: 1
Size: 232602 Color: 0

Bin 863: 5 of cap free
Amount of items: 2
Items: 
Size: 779964 Color: 1
Size: 220032 Color: 0

Bin 864: 5 of cap free
Amount of items: 2
Items: 
Size: 783693 Color: 1
Size: 216303 Color: 0

Bin 865: 6 of cap free
Amount of items: 2
Items: 
Size: 757508 Color: 1
Size: 242487 Color: 0

Bin 866: 6 of cap free
Amount of items: 3
Items: 
Size: 377764 Color: 0
Size: 320991 Color: 0
Size: 301240 Color: 1

Bin 867: 6 of cap free
Amount of items: 3
Items: 
Size: 708969 Color: 1
Size: 150319 Color: 0
Size: 140707 Color: 0

Bin 868: 6 of cap free
Amount of items: 3
Items: 
Size: 705614 Color: 1
Size: 186964 Color: 1
Size: 107417 Color: 0

Bin 869: 6 of cap free
Amount of items: 7
Items: 
Size: 250396 Color: 0
Size: 141141 Color: 1
Size: 127736 Color: 0
Size: 127681 Color: 0
Size: 122589 Color: 1
Size: 121232 Color: 1
Size: 109220 Color: 0

Bin 870: 6 of cap free
Amount of items: 5
Items: 
Size: 248416 Color: 1
Size: 191166 Color: 0
Size: 190725 Color: 0
Size: 185338 Color: 1
Size: 184350 Color: 0

Bin 871: 6 of cap free
Amount of items: 3
Items: 
Size: 775860 Color: 1
Size: 115870 Color: 0
Size: 108265 Color: 0

Bin 872: 6 of cap free
Amount of items: 3
Items: 
Size: 536427 Color: 0
Size: 322535 Color: 1
Size: 141033 Color: 0

Bin 873: 6 of cap free
Amount of items: 3
Items: 
Size: 373861 Color: 0
Size: 356026 Color: 1
Size: 270108 Color: 1

Bin 874: 6 of cap free
Amount of items: 3
Items: 
Size: 436476 Color: 0
Size: 431634 Color: 1
Size: 131885 Color: 0

Bin 875: 6 of cap free
Amount of items: 3
Items: 
Size: 442220 Color: 1
Size: 395058 Color: 0
Size: 162717 Color: 0

Bin 876: 6 of cap free
Amount of items: 3
Items: 
Size: 466020 Color: 0
Size: 316754 Color: 1
Size: 217221 Color: 1

Bin 877: 6 of cap free
Amount of items: 3
Items: 
Size: 500129 Color: 0
Size: 333675 Color: 1
Size: 166191 Color: 1

Bin 878: 6 of cap free
Amount of items: 2
Items: 
Size: 502956 Color: 1
Size: 497039 Color: 0

Bin 879: 6 of cap free
Amount of items: 2
Items: 
Size: 535547 Color: 0
Size: 464448 Color: 1

Bin 880: 6 of cap free
Amount of items: 2
Items: 
Size: 537148 Color: 1
Size: 462847 Color: 0

Bin 881: 6 of cap free
Amount of items: 2
Items: 
Size: 546126 Color: 1
Size: 453869 Color: 0

Bin 882: 6 of cap free
Amount of items: 2
Items: 
Size: 552839 Color: 1
Size: 447156 Color: 0

Bin 883: 6 of cap free
Amount of items: 2
Items: 
Size: 566750 Color: 1
Size: 433245 Color: 0

Bin 884: 6 of cap free
Amount of items: 3
Items: 
Size: 578730 Color: 1
Size: 291859 Color: 1
Size: 129406 Color: 0

Bin 885: 6 of cap free
Amount of items: 3
Items: 
Size: 579156 Color: 0
Size: 302606 Color: 0
Size: 118233 Color: 1

Bin 886: 6 of cap free
Amount of items: 3
Items: 
Size: 595491 Color: 1
Size: 253839 Color: 1
Size: 150665 Color: 0

Bin 887: 6 of cap free
Amount of items: 3
Items: 
Size: 600803 Color: 1
Size: 294066 Color: 1
Size: 105126 Color: 0

Bin 888: 6 of cap free
Amount of items: 2
Items: 
Size: 603778 Color: 1
Size: 396217 Color: 0

Bin 889: 6 of cap free
Amount of items: 2
Items: 
Size: 606609 Color: 1
Size: 393386 Color: 0

Bin 890: 6 of cap free
Amount of items: 2
Items: 
Size: 660540 Color: 1
Size: 339455 Color: 0

Bin 891: 6 of cap free
Amount of items: 2
Items: 
Size: 693422 Color: 0
Size: 306573 Color: 1

Bin 892: 6 of cap free
Amount of items: 2
Items: 
Size: 708322 Color: 1
Size: 291673 Color: 0

Bin 893: 6 of cap free
Amount of items: 2
Items: 
Size: 734923 Color: 1
Size: 265072 Color: 0

Bin 894: 6 of cap free
Amount of items: 2
Items: 
Size: 738964 Color: 0
Size: 261031 Color: 1

Bin 895: 6 of cap free
Amount of items: 2
Items: 
Size: 740622 Color: 1
Size: 259373 Color: 0

Bin 896: 6 of cap free
Amount of items: 2
Items: 
Size: 781450 Color: 0
Size: 218545 Color: 1

Bin 897: 6 of cap free
Amount of items: 2
Items: 
Size: 783936 Color: 0
Size: 216059 Color: 1

Bin 898: 6 of cap free
Amount of items: 2
Items: 
Size: 792582 Color: 1
Size: 207413 Color: 0

Bin 899: 6 of cap free
Amount of items: 2
Items: 
Size: 799959 Color: 1
Size: 200036 Color: 0

Bin 900: 7 of cap free
Amount of items: 3
Items: 
Size: 586664 Color: 0
Size: 266749 Color: 1
Size: 146581 Color: 1

Bin 901: 7 of cap free
Amount of items: 5
Items: 
Size: 276137 Color: 1
Size: 255015 Color: 1
Size: 187012 Color: 0
Size: 144433 Color: 1
Size: 137397 Color: 0

Bin 902: 7 of cap free
Amount of items: 3
Items: 
Size: 434713 Color: 0
Size: 432107 Color: 1
Size: 133174 Color: 0

Bin 903: 7 of cap free
Amount of items: 2
Items: 
Size: 513316 Color: 1
Size: 486678 Color: 0

Bin 904: 7 of cap free
Amount of items: 2
Items: 
Size: 518404 Color: 0
Size: 481590 Color: 1

Bin 905: 7 of cap free
Amount of items: 2
Items: 
Size: 520725 Color: 1
Size: 479269 Color: 0

Bin 906: 7 of cap free
Amount of items: 2
Items: 
Size: 520931 Color: 0
Size: 479063 Color: 1

Bin 907: 7 of cap free
Amount of items: 3
Items: 
Size: 526062 Color: 1
Size: 326410 Color: 1
Size: 147522 Color: 0

Bin 908: 7 of cap free
Amount of items: 2
Items: 
Size: 534189 Color: 1
Size: 465805 Color: 0

Bin 909: 7 of cap free
Amount of items: 3
Items: 
Size: 534698 Color: 0
Size: 337555 Color: 0
Size: 127741 Color: 1

Bin 910: 7 of cap free
Amount of items: 2
Items: 
Size: 553712 Color: 1
Size: 446282 Color: 0

Bin 911: 7 of cap free
Amount of items: 2
Items: 
Size: 601917 Color: 1
Size: 398077 Color: 0

Bin 912: 7 of cap free
Amount of items: 2
Items: 
Size: 614470 Color: 0
Size: 385524 Color: 1

Bin 913: 7 of cap free
Amount of items: 2
Items: 
Size: 614750 Color: 0
Size: 385244 Color: 1

Bin 914: 7 of cap free
Amount of items: 2
Items: 
Size: 628611 Color: 0
Size: 371383 Color: 1

Bin 915: 7 of cap free
Amount of items: 2
Items: 
Size: 649810 Color: 0
Size: 350184 Color: 1

Bin 916: 7 of cap free
Amount of items: 2
Items: 
Size: 666004 Color: 0
Size: 333990 Color: 1

Bin 917: 7 of cap free
Amount of items: 2
Items: 
Size: 671204 Color: 0
Size: 328790 Color: 1

Bin 918: 7 of cap free
Amount of items: 2
Items: 
Size: 697907 Color: 1
Size: 302087 Color: 0

Bin 919: 7 of cap free
Amount of items: 2
Items: 
Size: 700896 Color: 0
Size: 299098 Color: 1

Bin 920: 7 of cap free
Amount of items: 2
Items: 
Size: 709569 Color: 1
Size: 290425 Color: 0

Bin 921: 7 of cap free
Amount of items: 2
Items: 
Size: 715276 Color: 0
Size: 284718 Color: 1

Bin 922: 7 of cap free
Amount of items: 2
Items: 
Size: 745256 Color: 0
Size: 254738 Color: 1

Bin 923: 7 of cap free
Amount of items: 2
Items: 
Size: 766039 Color: 1
Size: 233955 Color: 0

Bin 924: 7 of cap free
Amount of items: 2
Items: 
Size: 774616 Color: 1
Size: 225378 Color: 0

Bin 925: 7 of cap free
Amount of items: 2
Items: 
Size: 774791 Color: 1
Size: 225203 Color: 0

Bin 926: 7 of cap free
Amount of items: 2
Items: 
Size: 793813 Color: 0
Size: 206181 Color: 1

Bin 927: 7 of cap free
Amount of items: 2
Items: 
Size: 793912 Color: 0
Size: 206082 Color: 1

Bin 928: 8 of cap free
Amount of items: 2
Items: 
Size: 617938 Color: 0
Size: 382055 Color: 1

Bin 929: 8 of cap free
Amount of items: 5
Items: 
Size: 257339 Color: 0
Size: 188624 Color: 0
Size: 187921 Color: 0
Size: 183260 Color: 1
Size: 182849 Color: 1

Bin 930: 8 of cap free
Amount of items: 6
Items: 
Size: 171070 Color: 0
Size: 169022 Color: 0
Size: 167482 Color: 0
Size: 165150 Color: 1
Size: 163659 Color: 1
Size: 163610 Color: 1

Bin 931: 8 of cap free
Amount of items: 3
Items: 
Size: 511312 Color: 0
Size: 352835 Color: 1
Size: 135846 Color: 0

Bin 932: 8 of cap free
Amount of items: 7
Items: 
Size: 151566 Color: 0
Size: 151555 Color: 0
Size: 147282 Color: 1
Size: 146783 Color: 1
Size: 146176 Color: 1
Size: 141885 Color: 1
Size: 114746 Color: 0

Bin 933: 8 of cap free
Amount of items: 3
Items: 
Size: 589348 Color: 1
Size: 253665 Color: 1
Size: 156980 Color: 0

Bin 934: 8 of cap free
Amount of items: 2
Items: 
Size: 504002 Color: 1
Size: 495991 Color: 0

Bin 935: 8 of cap free
Amount of items: 2
Items: 
Size: 506979 Color: 1
Size: 493014 Color: 0

Bin 936: 8 of cap free
Amount of items: 2
Items: 
Size: 514378 Color: 0
Size: 485615 Color: 1

Bin 937: 8 of cap free
Amount of items: 2
Items: 
Size: 553058 Color: 0
Size: 446935 Color: 1

Bin 938: 8 of cap free
Amount of items: 2
Items: 
Size: 561677 Color: 0
Size: 438316 Color: 1

Bin 939: 8 of cap free
Amount of items: 2
Items: 
Size: 570864 Color: 0
Size: 429129 Color: 1

Bin 940: 8 of cap free
Amount of items: 2
Items: 
Size: 579000 Color: 1
Size: 420993 Color: 0

Bin 941: 8 of cap free
Amount of items: 2
Items: 
Size: 582246 Color: 1
Size: 417747 Color: 0

Bin 942: 8 of cap free
Amount of items: 2
Items: 
Size: 594216 Color: 1
Size: 405777 Color: 0

Bin 943: 8 of cap free
Amount of items: 2
Items: 
Size: 609618 Color: 0
Size: 390375 Color: 1

Bin 944: 8 of cap free
Amount of items: 2
Items: 
Size: 616225 Color: 0
Size: 383768 Color: 1

Bin 945: 8 of cap free
Amount of items: 2
Items: 
Size: 644471 Color: 0
Size: 355522 Color: 1

Bin 946: 8 of cap free
Amount of items: 2
Items: 
Size: 645333 Color: 0
Size: 354660 Color: 1

Bin 947: 8 of cap free
Amount of items: 2
Items: 
Size: 646564 Color: 0
Size: 353429 Color: 1

Bin 948: 8 of cap free
Amount of items: 2
Items: 
Size: 658253 Color: 0
Size: 341740 Color: 1

Bin 949: 8 of cap free
Amount of items: 2
Items: 
Size: 661693 Color: 0
Size: 338300 Color: 1

Bin 950: 8 of cap free
Amount of items: 2
Items: 
Size: 669741 Color: 0
Size: 330252 Color: 1

Bin 951: 8 of cap free
Amount of items: 2
Items: 
Size: 695363 Color: 0
Size: 304630 Color: 1

Bin 952: 8 of cap free
Amount of items: 2
Items: 
Size: 735251 Color: 1
Size: 264742 Color: 0

Bin 953: 8 of cap free
Amount of items: 2
Items: 
Size: 763593 Color: 0
Size: 236400 Color: 1

Bin 954: 9 of cap free
Amount of items: 3
Items: 
Size: 730958 Color: 1
Size: 142809 Color: 1
Size: 126225 Color: 0

Bin 955: 9 of cap free
Amount of items: 7
Items: 
Size: 161883 Color: 1
Size: 140332 Color: 0
Size: 140198 Color: 0
Size: 140143 Color: 0
Size: 140141 Color: 0
Size: 139451 Color: 1
Size: 137844 Color: 1

Bin 956: 9 of cap free
Amount of items: 6
Items: 
Size: 170936 Color: 1
Size: 167892 Color: 0
Size: 167822 Color: 0
Size: 167619 Color: 0
Size: 162895 Color: 1
Size: 162828 Color: 1

Bin 957: 9 of cap free
Amount of items: 5
Items: 
Size: 318186 Color: 0
Size: 181709 Color: 1
Size: 175176 Color: 1
Size: 164399 Color: 0
Size: 160522 Color: 1

Bin 958: 9 of cap free
Amount of items: 3
Items: 
Size: 472636 Color: 1
Size: 414071 Color: 0
Size: 113285 Color: 0

Bin 959: 9 of cap free
Amount of items: 3
Items: 
Size: 454112 Color: 1
Size: 434576 Color: 0
Size: 111304 Color: 0

Bin 960: 9 of cap free
Amount of items: 3
Items: 
Size: 456133 Color: 0
Size: 389597 Color: 1
Size: 154262 Color: 1

Bin 961: 9 of cap free
Amount of items: 3
Items: 
Size: 468446 Color: 1
Size: 378531 Color: 0
Size: 153015 Color: 1

Bin 962: 9 of cap free
Amount of items: 3
Items: 
Size: 452203 Color: 1
Size: 408514 Color: 0
Size: 139275 Color: 0

Bin 963: 9 of cap free
Amount of items: 3
Items: 
Size: 510462 Color: 1
Size: 360470 Color: 1
Size: 129060 Color: 0

Bin 964: 9 of cap free
Amount of items: 3
Items: 
Size: 536315 Color: 0
Size: 287104 Color: 0
Size: 176573 Color: 1

Bin 965: 9 of cap free
Amount of items: 2
Items: 
Size: 549173 Color: 0
Size: 450819 Color: 1

Bin 966: 9 of cap free
Amount of items: 2
Items: 
Size: 555740 Color: 1
Size: 444252 Color: 0

Bin 967: 9 of cap free
Amount of items: 2
Items: 
Size: 556204 Color: 1
Size: 443788 Color: 0

Bin 968: 9 of cap free
Amount of items: 2
Items: 
Size: 573002 Color: 0
Size: 426990 Color: 1

Bin 969: 9 of cap free
Amount of items: 3
Items: 
Size: 577256 Color: 1
Size: 224468 Color: 1
Size: 198268 Color: 0

Bin 970: 9 of cap free
Amount of items: 2
Items: 
Size: 594492 Color: 1
Size: 405500 Color: 0

Bin 971: 9 of cap free
Amount of items: 2
Items: 
Size: 606487 Color: 0
Size: 393505 Color: 1

Bin 972: 9 of cap free
Amount of items: 2
Items: 
Size: 609067 Color: 0
Size: 390925 Color: 1

Bin 973: 9 of cap free
Amount of items: 2
Items: 
Size: 627313 Color: 0
Size: 372679 Color: 1

Bin 974: 9 of cap free
Amount of items: 2
Items: 
Size: 635018 Color: 0
Size: 364974 Color: 1

Bin 975: 9 of cap free
Amount of items: 2
Items: 
Size: 651516 Color: 0
Size: 348476 Color: 1

Bin 976: 9 of cap free
Amount of items: 2
Items: 
Size: 667156 Color: 1
Size: 332836 Color: 0

Bin 977: 9 of cap free
Amount of items: 2
Items: 
Size: 668007 Color: 1
Size: 331985 Color: 0

Bin 978: 9 of cap free
Amount of items: 2
Items: 
Size: 669192 Color: 0
Size: 330800 Color: 1

Bin 979: 9 of cap free
Amount of items: 2
Items: 
Size: 672290 Color: 1
Size: 327702 Color: 0

Bin 980: 9 of cap free
Amount of items: 2
Items: 
Size: 683227 Color: 1
Size: 316765 Color: 0

Bin 981: 9 of cap free
Amount of items: 2
Items: 
Size: 687102 Color: 0
Size: 312890 Color: 1

Bin 982: 9 of cap free
Amount of items: 2
Items: 
Size: 697487 Color: 0
Size: 302505 Color: 1

Bin 983: 9 of cap free
Amount of items: 2
Items: 
Size: 709841 Color: 1
Size: 290151 Color: 0

Bin 984: 9 of cap free
Amount of items: 2
Items: 
Size: 718391 Color: 0
Size: 281601 Color: 1

Bin 985: 9 of cap free
Amount of items: 2
Items: 
Size: 732724 Color: 0
Size: 267268 Color: 1

Bin 986: 9 of cap free
Amount of items: 2
Items: 
Size: 735311 Color: 0
Size: 264681 Color: 1

Bin 987: 9 of cap free
Amount of items: 2
Items: 
Size: 738434 Color: 0
Size: 261558 Color: 1

Bin 988: 9 of cap free
Amount of items: 2
Items: 
Size: 742515 Color: 1
Size: 257477 Color: 0

Bin 989: 9 of cap free
Amount of items: 2
Items: 
Size: 755778 Color: 1
Size: 244214 Color: 0

Bin 990: 9 of cap free
Amount of items: 2
Items: 
Size: 757671 Color: 1
Size: 242321 Color: 0

Bin 991: 9 of cap free
Amount of items: 2
Items: 
Size: 786474 Color: 0
Size: 213518 Color: 1

Bin 992: 9 of cap free
Amount of items: 2
Items: 
Size: 786952 Color: 1
Size: 213040 Color: 0

Bin 993: 9 of cap free
Amount of items: 2
Items: 
Size: 790408 Color: 1
Size: 209584 Color: 0

Bin 994: 10 of cap free
Amount of items: 3
Items: 
Size: 597169 Color: 0
Size: 270775 Color: 0
Size: 132047 Color: 1

Bin 995: 10 of cap free
Amount of items: 5
Items: 
Size: 312486 Color: 0
Size: 293686 Color: 1
Size: 148325 Color: 1
Size: 136119 Color: 1
Size: 109375 Color: 0

Bin 996: 10 of cap free
Amount of items: 3
Items: 
Size: 703553 Color: 1
Size: 159259 Color: 0
Size: 137179 Color: 0

Bin 997: 10 of cap free
Amount of items: 3
Items: 
Size: 729844 Color: 0
Size: 136223 Color: 1
Size: 133924 Color: 0

Bin 998: 10 of cap free
Amount of items: 2
Items: 
Size: 708956 Color: 0
Size: 291035 Color: 1

Bin 999: 10 of cap free
Amount of items: 5
Items: 
Size: 207080 Color: 1
Size: 199145 Color: 1
Size: 198199 Color: 0
Size: 197834 Color: 0
Size: 197733 Color: 0

Bin 1000: 10 of cap free
Amount of items: 6
Items: 
Size: 180295 Color: 0
Size: 174750 Color: 1
Size: 173381 Color: 1
Size: 172328 Color: 0
Size: 171058 Color: 0
Size: 128179 Color: 1

Bin 1001: 10 of cap free
Amount of items: 3
Items: 
Size: 452805 Color: 0
Size: 279995 Color: 0
Size: 267191 Color: 1

Bin 1002: 10 of cap free
Amount of items: 5
Items: 
Size: 302687 Color: 1
Size: 175729 Color: 0
Size: 175524 Color: 0
Size: 175038 Color: 0
Size: 171013 Color: 1

Bin 1003: 10 of cap free
Amount of items: 3
Items: 
Size: 510164 Color: 0
Size: 332686 Color: 1
Size: 157141 Color: 1

Bin 1004: 10 of cap free
Amount of items: 2
Items: 
Size: 501734 Color: 1
Size: 498257 Color: 0

Bin 1005: 10 of cap free
Amount of items: 2
Items: 
Size: 515130 Color: 1
Size: 484861 Color: 0

Bin 1006: 10 of cap free
Amount of items: 2
Items: 
Size: 515711 Color: 1
Size: 484280 Color: 0

Bin 1007: 10 of cap free
Amount of items: 2
Items: 
Size: 523544 Color: 0
Size: 476447 Color: 1

Bin 1008: 10 of cap free
Amount of items: 2
Items: 
Size: 558451 Color: 1
Size: 441540 Color: 0

Bin 1009: 10 of cap free
Amount of items: 2
Items: 
Size: 560244 Color: 0
Size: 439747 Color: 1

Bin 1010: 10 of cap free
Amount of items: 2
Items: 
Size: 560900 Color: 1
Size: 439091 Color: 0

Bin 1011: 10 of cap free
Amount of items: 2
Items: 
Size: 605431 Color: 0
Size: 394560 Color: 1

Bin 1012: 10 of cap free
Amount of items: 2
Items: 
Size: 628978 Color: 1
Size: 371013 Color: 0

Bin 1013: 10 of cap free
Amount of items: 2
Items: 
Size: 633170 Color: 0
Size: 366821 Color: 1

Bin 1014: 10 of cap free
Amount of items: 2
Items: 
Size: 650848 Color: 1
Size: 349143 Color: 0

Bin 1015: 10 of cap free
Amount of items: 2
Items: 
Size: 659339 Color: 0
Size: 340652 Color: 1

Bin 1016: 10 of cap free
Amount of items: 2
Items: 
Size: 667057 Color: 1
Size: 332934 Color: 0

Bin 1017: 10 of cap free
Amount of items: 2
Items: 
Size: 671787 Color: 1
Size: 328204 Color: 0

Bin 1018: 10 of cap free
Amount of items: 2
Items: 
Size: 673879 Color: 0
Size: 326112 Color: 1

Bin 1019: 10 of cap free
Amount of items: 2
Items: 
Size: 683047 Color: 1
Size: 316944 Color: 0

Bin 1020: 10 of cap free
Amount of items: 2
Items: 
Size: 686965 Color: 1
Size: 313026 Color: 0

Bin 1021: 10 of cap free
Amount of items: 2
Items: 
Size: 709885 Color: 1
Size: 290106 Color: 0

Bin 1022: 10 of cap free
Amount of items: 2
Items: 
Size: 728893 Color: 1
Size: 271098 Color: 0

Bin 1023: 10 of cap free
Amount of items: 2
Items: 
Size: 735672 Color: 0
Size: 264319 Color: 1

Bin 1024: 10 of cap free
Amount of items: 2
Items: 
Size: 739866 Color: 0
Size: 260125 Color: 1

Bin 1025: 10 of cap free
Amount of items: 2
Items: 
Size: 759340 Color: 1
Size: 240651 Color: 0

Bin 1026: 10 of cap free
Amount of items: 2
Items: 
Size: 780004 Color: 0
Size: 219987 Color: 1

Bin 1027: 10 of cap free
Amount of items: 2
Items: 
Size: 784875 Color: 1
Size: 215116 Color: 0

Bin 1028: 10 of cap free
Amount of items: 2
Items: 
Size: 785958 Color: 0
Size: 214033 Color: 1

Bin 1029: 10 of cap free
Amount of items: 2
Items: 
Size: 790304 Color: 0
Size: 209687 Color: 1

Bin 1030: 11 of cap free
Amount of items: 3
Items: 
Size: 721705 Color: 1
Size: 146993 Color: 0
Size: 131292 Color: 0

Bin 1031: 11 of cap free
Amount of items: 3
Items: 
Size: 787195 Color: 0
Size: 109274 Color: 1
Size: 103521 Color: 1

Bin 1032: 11 of cap free
Amount of items: 3
Items: 
Size: 428176 Color: 1
Size: 325786 Color: 1
Size: 246028 Color: 0

Bin 1033: 11 of cap free
Amount of items: 2
Items: 
Size: 507146 Color: 0
Size: 492844 Color: 1

Bin 1034: 11 of cap free
Amount of items: 2
Items: 
Size: 508705 Color: 1
Size: 491285 Color: 0

Bin 1035: 11 of cap free
Amount of items: 2
Items: 
Size: 512172 Color: 0
Size: 487818 Color: 1

Bin 1036: 11 of cap free
Amount of items: 2
Items: 
Size: 521269 Color: 1
Size: 478721 Color: 0

Bin 1037: 11 of cap free
Amount of items: 2
Items: 
Size: 524817 Color: 0
Size: 475173 Color: 1

Bin 1038: 11 of cap free
Amount of items: 2
Items: 
Size: 527285 Color: 1
Size: 472705 Color: 0

Bin 1039: 11 of cap free
Amount of items: 2
Items: 
Size: 545936 Color: 1
Size: 454054 Color: 0

Bin 1040: 11 of cap free
Amount of items: 2
Items: 
Size: 564888 Color: 1
Size: 435102 Color: 0

Bin 1041: 11 of cap free
Amount of items: 2
Items: 
Size: 582748 Color: 1
Size: 417242 Color: 0

Bin 1042: 11 of cap free
Amount of items: 2
Items: 
Size: 591082 Color: 1
Size: 408908 Color: 0

Bin 1043: 11 of cap free
Amount of items: 2
Items: 
Size: 615582 Color: 0
Size: 384408 Color: 1

Bin 1044: 11 of cap free
Amount of items: 2
Items: 
Size: 643356 Color: 1
Size: 356634 Color: 0

Bin 1045: 11 of cap free
Amount of items: 2
Items: 
Size: 644633 Color: 1
Size: 355357 Color: 0

Bin 1046: 11 of cap free
Amount of items: 2
Items: 
Size: 658165 Color: 1
Size: 341825 Color: 0

Bin 1047: 11 of cap free
Amount of items: 2
Items: 
Size: 728399 Color: 0
Size: 271591 Color: 1

Bin 1048: 11 of cap free
Amount of items: 2
Items: 
Size: 739061 Color: 1
Size: 260929 Color: 0

Bin 1049: 11 of cap free
Amount of items: 2
Items: 
Size: 758976 Color: 0
Size: 241014 Color: 1

Bin 1050: 11 of cap free
Amount of items: 2
Items: 
Size: 773485 Color: 0
Size: 226505 Color: 1

Bin 1051: 11 of cap free
Amount of items: 2
Items: 
Size: 785861 Color: 0
Size: 214129 Color: 1

Bin 1052: 12 of cap free
Amount of items: 2
Items: 
Size: 695306 Color: 0
Size: 304683 Color: 1

Bin 1053: 12 of cap free
Amount of items: 6
Items: 
Size: 184960 Color: 0
Size: 165845 Color: 0
Size: 165706 Color: 0
Size: 161445 Color: 1
Size: 161107 Color: 1
Size: 160926 Color: 1

Bin 1054: 12 of cap free
Amount of items: 3
Items: 
Size: 517975 Color: 1
Size: 328262 Color: 0
Size: 153752 Color: 0

Bin 1055: 12 of cap free
Amount of items: 2
Items: 
Size: 709943 Color: 1
Size: 290046 Color: 0

Bin 1056: 12 of cap free
Amount of items: 3
Items: 
Size: 375968 Color: 1
Size: 327879 Color: 1
Size: 296142 Color: 0

Bin 1057: 12 of cap free
Amount of items: 3
Items: 
Size: 438554 Color: 0
Size: 417665 Color: 1
Size: 143770 Color: 1

Bin 1058: 12 of cap free
Amount of items: 2
Items: 
Size: 500932 Color: 0
Size: 499057 Color: 1

Bin 1059: 12 of cap free
Amount of items: 2
Items: 
Size: 503953 Color: 1
Size: 496036 Color: 0

Bin 1060: 12 of cap free
Amount of items: 2
Items: 
Size: 514719 Color: 0
Size: 485270 Color: 1

Bin 1061: 12 of cap free
Amount of items: 2
Items: 
Size: 543708 Color: 1
Size: 456281 Color: 0

Bin 1062: 12 of cap free
Amount of items: 2
Items: 
Size: 581699 Color: 0
Size: 418290 Color: 1

Bin 1063: 12 of cap free
Amount of items: 2
Items: 
Size: 584585 Color: 1
Size: 415404 Color: 0

Bin 1064: 12 of cap free
Amount of items: 2
Items: 
Size: 589599 Color: 0
Size: 410390 Color: 1

Bin 1065: 12 of cap free
Amount of items: 2
Items: 
Size: 611580 Color: 1
Size: 388409 Color: 0

Bin 1066: 12 of cap free
Amount of items: 2
Items: 
Size: 632937 Color: 1
Size: 367052 Color: 0

Bin 1067: 12 of cap free
Amount of items: 2
Items: 
Size: 635139 Color: 1
Size: 364850 Color: 0

Bin 1068: 12 of cap free
Amount of items: 2
Items: 
Size: 651010 Color: 0
Size: 348979 Color: 1

Bin 1069: 12 of cap free
Amount of items: 2
Items: 
Size: 651409 Color: 1
Size: 348580 Color: 0

Bin 1070: 12 of cap free
Amount of items: 2
Items: 
Size: 664193 Color: 0
Size: 335796 Color: 1

Bin 1071: 12 of cap free
Amount of items: 2
Items: 
Size: 674067 Color: 1
Size: 325922 Color: 0

Bin 1072: 12 of cap free
Amount of items: 2
Items: 
Size: 697399 Color: 1
Size: 302590 Color: 0

Bin 1073: 12 of cap free
Amount of items: 2
Items: 
Size: 698211 Color: 0
Size: 301778 Color: 1

Bin 1074: 12 of cap free
Amount of items: 2
Items: 
Size: 698768 Color: 0
Size: 301221 Color: 1

Bin 1075: 12 of cap free
Amount of items: 2
Items: 
Size: 729348 Color: 1
Size: 270641 Color: 0

Bin 1076: 12 of cap free
Amount of items: 2
Items: 
Size: 750470 Color: 1
Size: 249519 Color: 0

Bin 1077: 12 of cap free
Amount of items: 2
Items: 
Size: 759906 Color: 1
Size: 240083 Color: 0

Bin 1078: 12 of cap free
Amount of items: 2
Items: 
Size: 776438 Color: 1
Size: 223551 Color: 0

Bin 1079: 12 of cap free
Amount of items: 2
Items: 
Size: 786350 Color: 0
Size: 213639 Color: 1

Bin 1080: 13 of cap free
Amount of items: 3
Items: 
Size: 578092 Color: 1
Size: 276255 Color: 0
Size: 145641 Color: 0

Bin 1081: 13 of cap free
Amount of items: 2
Items: 
Size: 606867 Color: 1
Size: 393121 Color: 0

Bin 1082: 13 of cap free
Amount of items: 3
Items: 
Size: 499414 Color: 0
Size: 315146 Color: 1
Size: 185428 Color: 1

Bin 1083: 13 of cap free
Amount of items: 2
Items: 
Size: 523409 Color: 0
Size: 476579 Color: 1

Bin 1084: 13 of cap free
Amount of items: 2
Items: 
Size: 541322 Color: 1
Size: 458666 Color: 0

Bin 1085: 13 of cap free
Amount of items: 2
Items: 
Size: 559651 Color: 1
Size: 440337 Color: 0

Bin 1086: 13 of cap free
Amount of items: 2
Items: 
Size: 587203 Color: 0
Size: 412785 Color: 1

Bin 1087: 13 of cap free
Amount of items: 2
Items: 
Size: 603056 Color: 0
Size: 396932 Color: 1

Bin 1088: 13 of cap free
Amount of items: 2
Items: 
Size: 606068 Color: 1
Size: 393920 Color: 0

Bin 1089: 13 of cap free
Amount of items: 2
Items: 
Size: 608310 Color: 1
Size: 391678 Color: 0

Bin 1090: 13 of cap free
Amount of items: 2
Items: 
Size: 622138 Color: 1
Size: 377850 Color: 0

Bin 1091: 13 of cap free
Amount of items: 2
Items: 
Size: 628612 Color: 1
Size: 371376 Color: 0

Bin 1092: 13 of cap free
Amount of items: 2
Items: 
Size: 629259 Color: 0
Size: 370729 Color: 1

Bin 1093: 13 of cap free
Amount of items: 2
Items: 
Size: 646227 Color: 0
Size: 353761 Color: 1

Bin 1094: 13 of cap free
Amount of items: 2
Items: 
Size: 661792 Color: 1
Size: 338196 Color: 0

Bin 1095: 13 of cap free
Amount of items: 2
Items: 
Size: 661897 Color: 0
Size: 338091 Color: 1

Bin 1096: 13 of cap free
Amount of items: 2
Items: 
Size: 675611 Color: 0
Size: 324377 Color: 1

Bin 1097: 13 of cap free
Amount of items: 2
Items: 
Size: 687881 Color: 1
Size: 312107 Color: 0

Bin 1098: 13 of cap free
Amount of items: 2
Items: 
Size: 712249 Color: 1
Size: 287739 Color: 0

Bin 1099: 13 of cap free
Amount of items: 2
Items: 
Size: 726963 Color: 1
Size: 273025 Color: 0

Bin 1100: 13 of cap free
Amount of items: 2
Items: 
Size: 740473 Color: 0
Size: 259515 Color: 1

Bin 1101: 13 of cap free
Amount of items: 2
Items: 
Size: 746282 Color: 1
Size: 253706 Color: 0

Bin 1102: 13 of cap free
Amount of items: 2
Items: 
Size: 766098 Color: 1
Size: 233890 Color: 0

Bin 1103: 13 of cap free
Amount of items: 2
Items: 
Size: 768258 Color: 1
Size: 231730 Color: 0

Bin 1104: 13 of cap free
Amount of items: 2
Items: 
Size: 772260 Color: 1
Size: 227728 Color: 0

Bin 1105: 13 of cap free
Amount of items: 2
Items: 
Size: 783451 Color: 0
Size: 216537 Color: 1

Bin 1106: 14 of cap free
Amount of items: 3
Items: 
Size: 408305 Color: 1
Size: 305922 Color: 0
Size: 285760 Color: 1

Bin 1107: 14 of cap free
Amount of items: 5
Items: 
Size: 229077 Color: 1
Size: 216262 Color: 1
Size: 215956 Color: 1
Size: 194086 Color: 0
Size: 144606 Color: 0

Bin 1108: 14 of cap free
Amount of items: 3
Items: 
Size: 430073 Color: 0
Size: 421445 Color: 1
Size: 148469 Color: 1

Bin 1109: 14 of cap free
Amount of items: 3
Items: 
Size: 468173 Color: 1
Size: 428098 Color: 0
Size: 103716 Color: 0

Bin 1110: 14 of cap free
Amount of items: 4
Items: 
Size: 252615 Color: 1
Size: 252551 Color: 1
Size: 252315 Color: 0
Size: 242506 Color: 0

Bin 1111: 14 of cap free
Amount of items: 2
Items: 
Size: 521304 Color: 1
Size: 478683 Color: 0

Bin 1112: 14 of cap free
Amount of items: 2
Items: 
Size: 524064 Color: 0
Size: 475923 Color: 1

Bin 1113: 14 of cap free
Amount of items: 2
Items: 
Size: 526298 Color: 1
Size: 473689 Color: 0

Bin 1114: 14 of cap free
Amount of items: 2
Items: 
Size: 564539 Color: 0
Size: 435448 Color: 1

Bin 1115: 14 of cap free
Amount of items: 2
Items: 
Size: 588577 Color: 0
Size: 411410 Color: 1

Bin 1116: 14 of cap free
Amount of items: 2
Items: 
Size: 598148 Color: 0
Size: 401839 Color: 1

Bin 1117: 14 of cap free
Amount of items: 2
Items: 
Size: 661653 Color: 0
Size: 338334 Color: 1

Bin 1118: 14 of cap free
Amount of items: 2
Items: 
Size: 690537 Color: 0
Size: 309450 Color: 1

Bin 1119: 14 of cap free
Amount of items: 2
Items: 
Size: 692170 Color: 1
Size: 307817 Color: 0

Bin 1120: 14 of cap free
Amount of items: 2
Items: 
Size: 713624 Color: 1
Size: 286363 Color: 0

Bin 1121: 14 of cap free
Amount of items: 2
Items: 
Size: 734922 Color: 0
Size: 265065 Color: 1

Bin 1122: 14 of cap free
Amount of items: 2
Items: 
Size: 775631 Color: 1
Size: 224356 Color: 0

Bin 1123: 14 of cap free
Amount of items: 2
Items: 
Size: 780033 Color: 1
Size: 219954 Color: 0

Bin 1124: 14 of cap free
Amount of items: 2
Items: 
Size: 783328 Color: 0
Size: 216659 Color: 1

Bin 1125: 14 of cap free
Amount of items: 2
Items: 
Size: 795752 Color: 0
Size: 204235 Color: 1

Bin 1126: 15 of cap free
Amount of items: 5
Items: 
Size: 256773 Color: 1
Size: 237693 Color: 0
Size: 195695 Color: 0
Size: 170736 Color: 0
Size: 139089 Color: 1

Bin 1127: 15 of cap free
Amount of items: 3
Items: 
Size: 732178 Color: 1
Size: 141512 Color: 1
Size: 126296 Color: 0

Bin 1128: 15 of cap free
Amount of items: 2
Items: 
Size: 757063 Color: 1
Size: 242923 Color: 0

Bin 1129: 15 of cap free
Amount of items: 5
Items: 
Size: 295341 Color: 1
Size: 180010 Color: 0
Size: 177959 Color: 0
Size: 173409 Color: 1
Size: 173267 Color: 1

Bin 1130: 15 of cap free
Amount of items: 5
Items: 
Size: 284109 Color: 0
Size: 272013 Color: 0
Size: 159311 Color: 0
Size: 152204 Color: 1
Size: 132349 Color: 1

Bin 1131: 15 of cap free
Amount of items: 2
Items: 
Size: 614492 Color: 1
Size: 385494 Color: 0

Bin 1132: 15 of cap free
Amount of items: 2
Items: 
Size: 518000 Color: 1
Size: 481986 Color: 0

Bin 1133: 15 of cap free
Amount of items: 2
Items: 
Size: 519483 Color: 1
Size: 480503 Color: 0

Bin 1134: 15 of cap free
Amount of items: 2
Items: 
Size: 527954 Color: 0
Size: 472032 Color: 1

Bin 1135: 15 of cap free
Amount of items: 2
Items: 
Size: 537383 Color: 0
Size: 462603 Color: 1

Bin 1136: 15 of cap free
Amount of items: 2
Items: 
Size: 548414 Color: 0
Size: 451572 Color: 1

Bin 1137: 15 of cap free
Amount of items: 2
Items: 
Size: 551213 Color: 1
Size: 448773 Color: 0

Bin 1138: 15 of cap free
Amount of items: 2
Items: 
Size: 554953 Color: 0
Size: 445033 Color: 1

Bin 1139: 15 of cap free
Amount of items: 2
Items: 
Size: 558698 Color: 1
Size: 441288 Color: 0

Bin 1140: 15 of cap free
Amount of items: 2
Items: 
Size: 586560 Color: 1
Size: 413426 Color: 0

Bin 1141: 15 of cap free
Amount of items: 2
Items: 
Size: 591424 Color: 1
Size: 408562 Color: 0

Bin 1142: 15 of cap free
Amount of items: 2
Items: 
Size: 605716 Color: 1
Size: 394270 Color: 0

Bin 1143: 15 of cap free
Amount of items: 2
Items: 
Size: 617010 Color: 0
Size: 382976 Color: 1

Bin 1144: 15 of cap free
Amount of items: 2
Items: 
Size: 617926 Color: 1
Size: 382060 Color: 0

Bin 1145: 15 of cap free
Amount of items: 2
Items: 
Size: 631307 Color: 1
Size: 368679 Color: 0

Bin 1146: 15 of cap free
Amount of items: 2
Items: 
Size: 632795 Color: 1
Size: 367191 Color: 0

Bin 1147: 15 of cap free
Amount of items: 2
Items: 
Size: 651335 Color: 0
Size: 348651 Color: 1

Bin 1148: 15 of cap free
Amount of items: 2
Items: 
Size: 666390 Color: 0
Size: 333596 Color: 1

Bin 1149: 15 of cap free
Amount of items: 2
Items: 
Size: 679332 Color: 0
Size: 320654 Color: 1

Bin 1150: 15 of cap free
Amount of items: 2
Items: 
Size: 700243 Color: 1
Size: 299743 Color: 0

Bin 1151: 15 of cap free
Amount of items: 2
Items: 
Size: 701759 Color: 1
Size: 298227 Color: 0

Bin 1152: 15 of cap free
Amount of items: 2
Items: 
Size: 705083 Color: 0
Size: 294903 Color: 1

Bin 1153: 15 of cap free
Amount of items: 2
Items: 
Size: 706682 Color: 1
Size: 293304 Color: 0

Bin 1154: 15 of cap free
Amount of items: 2
Items: 
Size: 718268 Color: 0
Size: 281718 Color: 1

Bin 1155: 15 of cap free
Amount of items: 2
Items: 
Size: 729260 Color: 0
Size: 270726 Color: 1

Bin 1156: 15 of cap free
Amount of items: 2
Items: 
Size: 744203 Color: 0
Size: 255783 Color: 1

Bin 1157: 15 of cap free
Amount of items: 2
Items: 
Size: 749751 Color: 0
Size: 250235 Color: 1

Bin 1158: 15 of cap free
Amount of items: 2
Items: 
Size: 761607 Color: 1
Size: 238379 Color: 0

Bin 1159: 16 of cap free
Amount of items: 5
Items: 
Size: 243036 Color: 1
Size: 194739 Color: 1
Size: 191672 Color: 0
Size: 191485 Color: 0
Size: 179053 Color: 0

Bin 1160: 16 of cap free
Amount of items: 2
Items: 
Size: 658241 Color: 1
Size: 341744 Color: 0

Bin 1161: 16 of cap free
Amount of items: 2
Items: 
Size: 734728 Color: 1
Size: 265257 Color: 0

Bin 1162: 16 of cap free
Amount of items: 3
Items: 
Size: 432580 Color: 1
Size: 350502 Color: 0
Size: 216903 Color: 1

Bin 1163: 16 of cap free
Amount of items: 2
Items: 
Size: 528212 Color: 1
Size: 471773 Color: 0

Bin 1164: 16 of cap free
Amount of items: 2
Items: 
Size: 559550 Color: 1
Size: 440435 Color: 0

Bin 1165: 16 of cap free
Amount of items: 2
Items: 
Size: 570145 Color: 0
Size: 429840 Color: 1

Bin 1166: 16 of cap free
Amount of items: 2
Items: 
Size: 589346 Color: 1
Size: 410639 Color: 0

Bin 1167: 16 of cap free
Amount of items: 2
Items: 
Size: 603769 Color: 0
Size: 396216 Color: 1

Bin 1168: 16 of cap free
Amount of items: 2
Items: 
Size: 612517 Color: 1
Size: 387468 Color: 0

Bin 1169: 16 of cap free
Amount of items: 2
Items: 
Size: 626649 Color: 1
Size: 373336 Color: 0

Bin 1170: 16 of cap free
Amount of items: 2
Items: 
Size: 634369 Color: 1
Size: 365616 Color: 0

Bin 1171: 16 of cap free
Amount of items: 2
Items: 
Size: 655908 Color: 0
Size: 344077 Color: 1

Bin 1172: 16 of cap free
Amount of items: 2
Items: 
Size: 661620 Color: 1
Size: 338365 Color: 0

Bin 1173: 16 of cap free
Amount of items: 2
Items: 
Size: 663162 Color: 0
Size: 336823 Color: 1

Bin 1174: 16 of cap free
Amount of items: 2
Items: 
Size: 700524 Color: 1
Size: 299461 Color: 0

Bin 1175: 16 of cap free
Amount of items: 2
Items: 
Size: 701267 Color: 1
Size: 298718 Color: 0

Bin 1176: 16 of cap free
Amount of items: 2
Items: 
Size: 702251 Color: 1
Size: 297734 Color: 0

Bin 1177: 16 of cap free
Amount of items: 2
Items: 
Size: 712986 Color: 1
Size: 286999 Color: 0

Bin 1178: 16 of cap free
Amount of items: 2
Items: 
Size: 740814 Color: 1
Size: 259171 Color: 0

Bin 1179: 16 of cap free
Amount of items: 2
Items: 
Size: 783230 Color: 0
Size: 216755 Color: 1

Bin 1180: 16 of cap free
Amount of items: 2
Items: 
Size: 785858 Color: 0
Size: 214127 Color: 1

Bin 1181: 16 of cap free
Amount of items: 2
Items: 
Size: 795942 Color: 1
Size: 204043 Color: 0

Bin 1182: 17 of cap free
Amount of items: 2
Items: 
Size: 777919 Color: 1
Size: 222065 Color: 0

Bin 1183: 17 of cap free
Amount of items: 3
Items: 
Size: 766037 Color: 0
Size: 132745 Color: 0
Size: 101202 Color: 1

Bin 1184: 17 of cap free
Amount of items: 2
Items: 
Size: 790833 Color: 0
Size: 209151 Color: 1

Bin 1185: 17 of cap free
Amount of items: 5
Items: 
Size: 255040 Color: 0
Size: 195864 Color: 0
Size: 195382 Color: 0
Size: 189889 Color: 1
Size: 163809 Color: 1

Bin 1186: 17 of cap free
Amount of items: 2
Items: 
Size: 523680 Color: 1
Size: 476304 Color: 0

Bin 1187: 17 of cap free
Amount of items: 2
Items: 
Size: 524495 Color: 0
Size: 475489 Color: 1

Bin 1188: 17 of cap free
Amount of items: 2
Items: 
Size: 526499 Color: 0
Size: 473485 Color: 1

Bin 1189: 17 of cap free
Amount of items: 2
Items: 
Size: 528771 Color: 0
Size: 471213 Color: 1

Bin 1190: 17 of cap free
Amount of items: 2
Items: 
Size: 547769 Color: 1
Size: 452215 Color: 0

Bin 1191: 17 of cap free
Amount of items: 2
Items: 
Size: 551942 Color: 0
Size: 448042 Color: 1

Bin 1192: 17 of cap free
Amount of items: 2
Items: 
Size: 568020 Color: 1
Size: 431964 Color: 0

Bin 1193: 17 of cap free
Amount of items: 2
Items: 
Size: 573489 Color: 0
Size: 426495 Color: 1

Bin 1194: 17 of cap free
Amount of items: 2
Items: 
Size: 614236 Color: 1
Size: 385748 Color: 0

Bin 1195: 17 of cap free
Amount of items: 2
Items: 
Size: 621617 Color: 0
Size: 378367 Color: 1

Bin 1196: 17 of cap free
Amount of items: 2
Items: 
Size: 626888 Color: 0
Size: 373096 Color: 1

Bin 1197: 17 of cap free
Amount of items: 2
Items: 
Size: 628063 Color: 0
Size: 371921 Color: 1

Bin 1198: 17 of cap free
Amount of items: 2
Items: 
Size: 676280 Color: 1
Size: 323704 Color: 0

Bin 1199: 17 of cap free
Amount of items: 2
Items: 
Size: 737779 Color: 1
Size: 262205 Color: 0

Bin 1200: 17 of cap free
Amount of items: 2
Items: 
Size: 794382 Color: 0
Size: 205602 Color: 1

Bin 1201: 18 of cap free
Amount of items: 3
Items: 
Size: 576199 Color: 1
Size: 261127 Color: 0
Size: 162657 Color: 0

Bin 1202: 18 of cap free
Amount of items: 2
Items: 
Size: 637172 Color: 0
Size: 362811 Color: 1

Bin 1203: 18 of cap free
Amount of items: 2
Items: 
Size: 505831 Color: 1
Size: 494152 Color: 0

Bin 1204: 18 of cap free
Amount of items: 2
Items: 
Size: 506139 Color: 1
Size: 493844 Color: 0

Bin 1205: 18 of cap free
Amount of items: 2
Items: 
Size: 524638 Color: 1
Size: 475345 Color: 0

Bin 1206: 18 of cap free
Amount of items: 2
Items: 
Size: 534670 Color: 0
Size: 465313 Color: 1

Bin 1207: 18 of cap free
Amount of items: 2
Items: 
Size: 551821 Color: 0
Size: 448162 Color: 1

Bin 1208: 18 of cap free
Amount of items: 2
Items: 
Size: 566220 Color: 1
Size: 433763 Color: 0

Bin 1209: 18 of cap free
Amount of items: 2
Items: 
Size: 575290 Color: 1
Size: 424693 Color: 0

Bin 1210: 18 of cap free
Amount of items: 2
Items: 
Size: 595998 Color: 0
Size: 403985 Color: 1

Bin 1211: 18 of cap free
Amount of items: 2
Items: 
Size: 628333 Color: 0
Size: 371650 Color: 1

Bin 1212: 18 of cap free
Amount of items: 2
Items: 
Size: 631442 Color: 1
Size: 368541 Color: 0

Bin 1213: 18 of cap free
Amount of items: 2
Items: 
Size: 637306 Color: 0
Size: 362677 Color: 1

Bin 1214: 18 of cap free
Amount of items: 2
Items: 
Size: 648170 Color: 1
Size: 351813 Color: 0

Bin 1215: 18 of cap free
Amount of items: 2
Items: 
Size: 648333 Color: 0
Size: 351650 Color: 1

Bin 1216: 18 of cap free
Amount of items: 2
Items: 
Size: 649687 Color: 1
Size: 350296 Color: 0

Bin 1217: 18 of cap free
Amount of items: 2
Items: 
Size: 686421 Color: 0
Size: 313562 Color: 1

Bin 1218: 18 of cap free
Amount of items: 2
Items: 
Size: 713233 Color: 1
Size: 286750 Color: 0

Bin 1219: 18 of cap free
Amount of items: 2
Items: 
Size: 721193 Color: 0
Size: 278790 Color: 1

Bin 1220: 18 of cap free
Amount of items: 2
Items: 
Size: 791153 Color: 0
Size: 208830 Color: 1

Bin 1221: 18 of cap free
Amount of items: 2
Items: 
Size: 797219 Color: 0
Size: 202764 Color: 1

Bin 1222: 19 of cap free
Amount of items: 3
Items: 
Size: 754833 Color: 0
Size: 131923 Color: 0
Size: 113226 Color: 1

Bin 1223: 19 of cap free
Amount of items: 2
Items: 
Size: 751806 Color: 0
Size: 248176 Color: 1

Bin 1224: 19 of cap free
Amount of items: 2
Items: 
Size: 777079 Color: 0
Size: 222903 Color: 1

Bin 1225: 19 of cap free
Amount of items: 3
Items: 
Size: 653288 Color: 1
Size: 194601 Color: 1
Size: 152093 Color: 0

Bin 1226: 19 of cap free
Amount of items: 2
Items: 
Size: 503925 Color: 0
Size: 496057 Color: 1

Bin 1227: 19 of cap free
Amount of items: 2
Items: 
Size: 504729 Color: 0
Size: 495253 Color: 1

Bin 1228: 19 of cap free
Amount of items: 2
Items: 
Size: 529590 Color: 1
Size: 470392 Color: 0

Bin 1229: 19 of cap free
Amount of items: 2
Items: 
Size: 533238 Color: 1
Size: 466744 Color: 0

Bin 1230: 19 of cap free
Amount of items: 2
Items: 
Size: 557264 Color: 1
Size: 442718 Color: 0

Bin 1231: 19 of cap free
Amount of items: 2
Items: 
Size: 568221 Color: 0
Size: 431761 Color: 1

Bin 1232: 19 of cap free
Amount of items: 2
Items: 
Size: 568565 Color: 0
Size: 431417 Color: 1

Bin 1233: 19 of cap free
Amount of items: 2
Items: 
Size: 582039 Color: 1
Size: 417943 Color: 0

Bin 1234: 19 of cap free
Amount of items: 2
Items: 
Size: 587051 Color: 1
Size: 412931 Color: 0

Bin 1235: 19 of cap free
Amount of items: 2
Items: 
Size: 587017 Color: 0
Size: 412965 Color: 1

Bin 1236: 19 of cap free
Amount of items: 2
Items: 
Size: 590971 Color: 0
Size: 409011 Color: 1

Bin 1237: 19 of cap free
Amount of items: 2
Items: 
Size: 600329 Color: 1
Size: 399653 Color: 0

Bin 1238: 19 of cap free
Amount of items: 2
Items: 
Size: 601721 Color: 1
Size: 398261 Color: 0

Bin 1239: 19 of cap free
Amount of items: 2
Items: 
Size: 603006 Color: 0
Size: 396976 Color: 1

Bin 1240: 19 of cap free
Amount of items: 2
Items: 
Size: 604538 Color: 0
Size: 395444 Color: 1

Bin 1241: 19 of cap free
Amount of items: 2
Items: 
Size: 607221 Color: 1
Size: 392761 Color: 0

Bin 1242: 19 of cap free
Amount of items: 2
Items: 
Size: 607343 Color: 0
Size: 392639 Color: 1

Bin 1243: 19 of cap free
Amount of items: 2
Items: 
Size: 611385 Color: 1
Size: 388597 Color: 0

Bin 1244: 19 of cap free
Amount of items: 2
Items: 
Size: 618747 Color: 1
Size: 381235 Color: 0

Bin 1245: 19 of cap free
Amount of items: 2
Items: 
Size: 635175 Color: 1
Size: 364807 Color: 0

Bin 1246: 19 of cap free
Amount of items: 2
Items: 
Size: 684099 Color: 1
Size: 315883 Color: 0

Bin 1247: 19 of cap free
Amount of items: 2
Items: 
Size: 687551 Color: 0
Size: 312431 Color: 1

Bin 1248: 19 of cap free
Amount of items: 2
Items: 
Size: 688181 Color: 1
Size: 311801 Color: 0

Bin 1249: 19 of cap free
Amount of items: 2
Items: 
Size: 688379 Color: 0
Size: 311603 Color: 1

Bin 1250: 19 of cap free
Amount of items: 2
Items: 
Size: 703696 Color: 0
Size: 296286 Color: 1

Bin 1251: 19 of cap free
Amount of items: 2
Items: 
Size: 710185 Color: 1
Size: 289797 Color: 0

Bin 1252: 19 of cap free
Amount of items: 2
Items: 
Size: 758780 Color: 0
Size: 241202 Color: 1

Bin 1253: 19 of cap free
Amount of items: 2
Items: 
Size: 762157 Color: 0
Size: 237825 Color: 1

Bin 1254: 19 of cap free
Amount of items: 2
Items: 
Size: 762670 Color: 0
Size: 237312 Color: 1

Bin 1255: 19 of cap free
Amount of items: 2
Items: 
Size: 784597 Color: 0
Size: 215385 Color: 1

Bin 1256: 20 of cap free
Amount of items: 2
Items: 
Size: 701752 Color: 0
Size: 298229 Color: 1

Bin 1257: 20 of cap free
Amount of items: 2
Items: 
Size: 690956 Color: 1
Size: 309025 Color: 0

Bin 1258: 20 of cap free
Amount of items: 2
Items: 
Size: 515088 Color: 1
Size: 484893 Color: 0

Bin 1259: 20 of cap free
Amount of items: 2
Items: 
Size: 518037 Color: 1
Size: 481944 Color: 0

Bin 1260: 20 of cap free
Amount of items: 2
Items: 
Size: 538376 Color: 0
Size: 461605 Color: 1

Bin 1261: 20 of cap free
Amount of items: 2
Items: 
Size: 564488 Color: 0
Size: 435493 Color: 1

Bin 1262: 20 of cap free
Amount of items: 2
Items: 
Size: 589072 Color: 1
Size: 410909 Color: 0

Bin 1263: 20 of cap free
Amount of items: 2
Items: 
Size: 601227 Color: 1
Size: 398754 Color: 0

Bin 1264: 20 of cap free
Amount of items: 2
Items: 
Size: 610007 Color: 0
Size: 389974 Color: 1

Bin 1265: 20 of cap free
Amount of items: 2
Items: 
Size: 623605 Color: 0
Size: 376376 Color: 1

Bin 1266: 20 of cap free
Amount of items: 2
Items: 
Size: 627734 Color: 1
Size: 372247 Color: 0

Bin 1267: 20 of cap free
Amount of items: 2
Items: 
Size: 635511 Color: 0
Size: 364470 Color: 1

Bin 1268: 20 of cap free
Amount of items: 2
Items: 
Size: 657341 Color: 0
Size: 342640 Color: 1

Bin 1269: 20 of cap free
Amount of items: 2
Items: 
Size: 658035 Color: 1
Size: 341946 Color: 0

Bin 1270: 20 of cap free
Amount of items: 2
Items: 
Size: 663854 Color: 0
Size: 336127 Color: 1

Bin 1271: 20 of cap free
Amount of items: 2
Items: 
Size: 671621 Color: 0
Size: 328360 Color: 1

Bin 1272: 20 of cap free
Amount of items: 2
Items: 
Size: 679538 Color: 1
Size: 320443 Color: 0

Bin 1273: 20 of cap free
Amount of items: 2
Items: 
Size: 691289 Color: 1
Size: 308692 Color: 0

Bin 1274: 20 of cap free
Amount of items: 2
Items: 
Size: 718820 Color: 1
Size: 281161 Color: 0

Bin 1275: 20 of cap free
Amount of items: 2
Items: 
Size: 752602 Color: 0
Size: 247379 Color: 1

Bin 1276: 20 of cap free
Amount of items: 2
Items: 
Size: 753916 Color: 0
Size: 246065 Color: 1

Bin 1277: 21 of cap free
Amount of items: 3
Items: 
Size: 617200 Color: 1
Size: 200297 Color: 0
Size: 182483 Color: 1

Bin 1278: 21 of cap free
Amount of items: 3
Items: 
Size: 760894 Color: 0
Size: 127290 Color: 0
Size: 111796 Color: 1

Bin 1279: 21 of cap free
Amount of items: 2
Items: 
Size: 587205 Color: 1
Size: 412775 Color: 0

Bin 1280: 21 of cap free
Amount of items: 2
Items: 
Size: 653437 Color: 0
Size: 346543 Color: 1

Bin 1281: 21 of cap free
Amount of items: 2
Items: 
Size: 561795 Color: 1
Size: 438185 Color: 0

Bin 1282: 21 of cap free
Amount of items: 2
Items: 
Size: 577142 Color: 0
Size: 422838 Color: 1

Bin 1283: 21 of cap free
Amount of items: 2
Items: 
Size: 624466 Color: 0
Size: 375514 Color: 1

Bin 1284: 21 of cap free
Amount of items: 2
Items: 
Size: 673683 Color: 1
Size: 326297 Color: 0

Bin 1285: 21 of cap free
Amount of items: 2
Items: 
Size: 679821 Color: 0
Size: 320159 Color: 1

Bin 1286: 21 of cap free
Amount of items: 2
Items: 
Size: 744255 Color: 0
Size: 255725 Color: 1

Bin 1287: 21 of cap free
Amount of items: 2
Items: 
Size: 753289 Color: 0
Size: 246691 Color: 1

Bin 1288: 21 of cap free
Amount of items: 2
Items: 
Size: 756739 Color: 1
Size: 243241 Color: 0

Bin 1289: 21 of cap free
Amount of items: 2
Items: 
Size: 759863 Color: 1
Size: 240117 Color: 0

Bin 1290: 21 of cap free
Amount of items: 2
Items: 
Size: 764850 Color: 1
Size: 235130 Color: 0

Bin 1291: 21 of cap free
Amount of items: 2
Items: 
Size: 774594 Color: 1
Size: 225386 Color: 0

Bin 1292: 21 of cap free
Amount of items: 2
Items: 
Size: 778052 Color: 1
Size: 221928 Color: 0

Bin 1293: 22 of cap free
Amount of items: 2
Items: 
Size: 645571 Color: 0
Size: 354408 Color: 1

Bin 1294: 22 of cap free
Amount of items: 5
Items: 
Size: 203858 Color: 1
Size: 199506 Color: 1
Size: 199383 Color: 1
Size: 198745 Color: 0
Size: 198487 Color: 0

Bin 1295: 22 of cap free
Amount of items: 2
Items: 
Size: 710393 Color: 0
Size: 289586 Color: 1

Bin 1296: 22 of cap free
Amount of items: 2
Items: 
Size: 522310 Color: 0
Size: 477669 Color: 1

Bin 1297: 22 of cap free
Amount of items: 2
Items: 
Size: 544015 Color: 1
Size: 455964 Color: 0

Bin 1298: 22 of cap free
Amount of items: 2
Items: 
Size: 547339 Color: 0
Size: 452640 Color: 1

Bin 1299: 22 of cap free
Amount of items: 2
Items: 
Size: 556562 Color: 0
Size: 443417 Color: 1

Bin 1300: 22 of cap free
Amount of items: 2
Items: 
Size: 557367 Color: 1
Size: 442612 Color: 0

Bin 1301: 22 of cap free
Amount of items: 2
Items: 
Size: 620914 Color: 0
Size: 379065 Color: 1

Bin 1302: 22 of cap free
Amount of items: 2
Items: 
Size: 665550 Color: 1
Size: 334429 Color: 0

Bin 1303: 22 of cap free
Amount of items: 2
Items: 
Size: 686678 Color: 0
Size: 313301 Color: 1

Bin 1304: 22 of cap free
Amount of items: 2
Items: 
Size: 693097 Color: 0
Size: 306882 Color: 1

Bin 1305: 22 of cap free
Amount of items: 2
Items: 
Size: 698457 Color: 1
Size: 301522 Color: 0

Bin 1306: 22 of cap free
Amount of items: 2
Items: 
Size: 707965 Color: 1
Size: 292014 Color: 0

Bin 1307: 22 of cap free
Amount of items: 2
Items: 
Size: 738527 Color: 0
Size: 261452 Color: 1

Bin 1308: 22 of cap free
Amount of items: 2
Items: 
Size: 741574 Color: 1
Size: 258405 Color: 0

Bin 1309: 22 of cap free
Amount of items: 2
Items: 
Size: 741694 Color: 1
Size: 258285 Color: 0

Bin 1310: 22 of cap free
Amount of items: 2
Items: 
Size: 751642 Color: 0
Size: 248337 Color: 1

Bin 1311: 22 of cap free
Amount of items: 2
Items: 
Size: 763435 Color: 1
Size: 236544 Color: 0

Bin 1312: 22 of cap free
Amount of items: 2
Items: 
Size: 773593 Color: 1
Size: 226386 Color: 0

Bin 1313: 22 of cap free
Amount of items: 2
Items: 
Size: 778473 Color: 0
Size: 221506 Color: 1

Bin 1314: 22 of cap free
Amount of items: 2
Items: 
Size: 781592 Color: 0
Size: 218387 Color: 1

Bin 1315: 22 of cap free
Amount of items: 2
Items: 
Size: 785287 Color: 1
Size: 214692 Color: 0

Bin 1316: 23 of cap free
Amount of items: 6
Items: 
Size: 200297 Color: 0
Size: 171403 Color: 0
Size: 171061 Color: 1
Size: 170632 Color: 0
Size: 167734 Color: 1
Size: 118851 Color: 1

Bin 1317: 23 of cap free
Amount of items: 3
Items: 
Size: 742894 Color: 1
Size: 148357 Color: 1
Size: 108727 Color: 0

Bin 1318: 23 of cap free
Amount of items: 2
Items: 
Size: 739289 Color: 1
Size: 260689 Color: 0

Bin 1319: 23 of cap free
Amount of items: 3
Items: 
Size: 706367 Color: 1
Size: 177543 Color: 0
Size: 116068 Color: 1

Bin 1320: 23 of cap free
Amount of items: 2
Items: 
Size: 517654 Color: 1
Size: 482324 Color: 0

Bin 1321: 23 of cap free
Amount of items: 2
Items: 
Size: 518604 Color: 0
Size: 481374 Color: 1

Bin 1322: 23 of cap free
Amount of items: 2
Items: 
Size: 519797 Color: 0
Size: 480181 Color: 1

Bin 1323: 23 of cap free
Amount of items: 2
Items: 
Size: 536955 Color: 0
Size: 463023 Color: 1

Bin 1324: 23 of cap free
Amount of items: 2
Items: 
Size: 547764 Color: 0
Size: 452214 Color: 1

Bin 1325: 23 of cap free
Amount of items: 2
Items: 
Size: 555263 Color: 1
Size: 444715 Color: 0

Bin 1326: 23 of cap free
Amount of items: 2
Items: 
Size: 566331 Color: 0
Size: 433647 Color: 1

Bin 1327: 23 of cap free
Amount of items: 2
Items: 
Size: 581984 Color: 1
Size: 417994 Color: 0

Bin 1328: 23 of cap free
Amount of items: 2
Items: 
Size: 588190 Color: 0
Size: 411788 Color: 1

Bin 1329: 23 of cap free
Amount of items: 2
Items: 
Size: 592096 Color: 0
Size: 407882 Color: 1

Bin 1330: 23 of cap free
Amount of items: 2
Items: 
Size: 598814 Color: 1
Size: 401164 Color: 0

Bin 1331: 23 of cap free
Amount of items: 2
Items: 
Size: 602083 Color: 1
Size: 397895 Color: 0

Bin 1332: 23 of cap free
Amount of items: 2
Items: 
Size: 602215 Color: 0
Size: 397763 Color: 1

Bin 1333: 23 of cap free
Amount of items: 2
Items: 
Size: 615945 Color: 0
Size: 384033 Color: 1

Bin 1334: 23 of cap free
Amount of items: 2
Items: 
Size: 622561 Color: 0
Size: 377417 Color: 1

Bin 1335: 23 of cap free
Amount of items: 2
Items: 
Size: 651094 Color: 1
Size: 348884 Color: 0

Bin 1336: 23 of cap free
Amount of items: 2
Items: 
Size: 657857 Color: 1
Size: 342121 Color: 0

Bin 1337: 23 of cap free
Amount of items: 2
Items: 
Size: 661280 Color: 1
Size: 338698 Color: 0

Bin 1338: 23 of cap free
Amount of items: 2
Items: 
Size: 725824 Color: 1
Size: 274154 Color: 0

Bin 1339: 23 of cap free
Amount of items: 2
Items: 
Size: 739199 Color: 0
Size: 260779 Color: 1

Bin 1340: 23 of cap free
Amount of items: 2
Items: 
Size: 768396 Color: 0
Size: 231582 Color: 1

Bin 1341: 23 of cap free
Amount of items: 2
Items: 
Size: 779740 Color: 0
Size: 220238 Color: 1

Bin 1342: 24 of cap free
Amount of items: 2
Items: 
Size: 547707 Color: 1
Size: 452270 Color: 0

Bin 1343: 24 of cap free
Amount of items: 2
Items: 
Size: 597498 Color: 1
Size: 402479 Color: 0

Bin 1344: 24 of cap free
Amount of items: 3
Items: 
Size: 500371 Color: 0
Size: 359530 Color: 1
Size: 140076 Color: 1

Bin 1345: 24 of cap free
Amount of items: 2
Items: 
Size: 749416 Color: 1
Size: 250561 Color: 0

Bin 1346: 24 of cap free
Amount of items: 3
Items: 
Size: 405310 Color: 0
Size: 304345 Color: 1
Size: 290322 Color: 1

Bin 1347: 24 of cap free
Amount of items: 3
Items: 
Size: 445415 Color: 1
Size: 391770 Color: 0
Size: 162792 Color: 1

Bin 1348: 24 of cap free
Amount of items: 2
Items: 
Size: 524103 Color: 1
Size: 475874 Color: 0

Bin 1349: 24 of cap free
Amount of items: 2
Items: 
Size: 528146 Color: 0
Size: 471831 Color: 1

Bin 1350: 24 of cap free
Amount of items: 2
Items: 
Size: 529675 Color: 0
Size: 470302 Color: 1

Bin 1351: 24 of cap free
Amount of items: 2
Items: 
Size: 533730 Color: 1
Size: 466247 Color: 0

Bin 1352: 24 of cap free
Amount of items: 2
Items: 
Size: 543103 Color: 0
Size: 456874 Color: 1

Bin 1353: 24 of cap free
Amount of items: 2
Items: 
Size: 569314 Color: 0
Size: 430663 Color: 1

Bin 1354: 24 of cap free
Amount of items: 2
Items: 
Size: 584404 Color: 0
Size: 415573 Color: 1

Bin 1355: 24 of cap free
Amount of items: 2
Items: 
Size: 586591 Color: 1
Size: 413386 Color: 0

Bin 1356: 24 of cap free
Amount of items: 2
Items: 
Size: 622826 Color: 0
Size: 377151 Color: 1

Bin 1357: 24 of cap free
Amount of items: 2
Items: 
Size: 657662 Color: 1
Size: 342315 Color: 0

Bin 1358: 24 of cap free
Amount of items: 2
Items: 
Size: 672161 Color: 1
Size: 327816 Color: 0

Bin 1359: 24 of cap free
Amount of items: 2
Items: 
Size: 673853 Color: 1
Size: 326124 Color: 0

Bin 1360: 24 of cap free
Amount of items: 2
Items: 
Size: 689713 Color: 1
Size: 310264 Color: 0

Bin 1361: 24 of cap free
Amount of items: 2
Items: 
Size: 691805 Color: 1
Size: 308172 Color: 0

Bin 1362: 24 of cap free
Amount of items: 2
Items: 
Size: 699316 Color: 0
Size: 300661 Color: 1

Bin 1363: 24 of cap free
Amount of items: 2
Items: 
Size: 706393 Color: 0
Size: 293584 Color: 1

Bin 1364: 24 of cap free
Amount of items: 2
Items: 
Size: 716408 Color: 1
Size: 283569 Color: 0

Bin 1365: 24 of cap free
Amount of items: 2
Items: 
Size: 721123 Color: 1
Size: 278854 Color: 0

Bin 1366: 24 of cap free
Amount of items: 2
Items: 
Size: 724466 Color: 0
Size: 275511 Color: 1

Bin 1367: 24 of cap free
Amount of items: 2
Items: 
Size: 731979 Color: 0
Size: 267998 Color: 1

Bin 1368: 24 of cap free
Amount of items: 2
Items: 
Size: 744144 Color: 1
Size: 255833 Color: 0

Bin 1369: 24 of cap free
Amount of items: 2
Items: 
Size: 775802 Color: 1
Size: 224175 Color: 0

Bin 1370: 25 of cap free
Amount of items: 3
Items: 
Size: 576178 Color: 1
Size: 290510 Color: 1
Size: 133288 Color: 0

Bin 1371: 25 of cap free
Amount of items: 2
Items: 
Size: 758861 Color: 0
Size: 241115 Color: 1

Bin 1372: 25 of cap free
Amount of items: 2
Items: 
Size: 586788 Color: 1
Size: 413188 Color: 0

Bin 1373: 25 of cap free
Amount of items: 2
Items: 
Size: 507424 Color: 1
Size: 492552 Color: 0

Bin 1374: 25 of cap free
Amount of items: 2
Items: 
Size: 513826 Color: 0
Size: 486150 Color: 1

Bin 1375: 25 of cap free
Amount of items: 2
Items: 
Size: 569610 Color: 0
Size: 430366 Color: 1

Bin 1376: 25 of cap free
Amount of items: 2
Items: 
Size: 573071 Color: 1
Size: 426905 Color: 0

Bin 1377: 25 of cap free
Amount of items: 2
Items: 
Size: 592575 Color: 0
Size: 407401 Color: 1

Bin 1378: 25 of cap free
Amount of items: 2
Items: 
Size: 609816 Color: 1
Size: 390160 Color: 0

Bin 1379: 25 of cap free
Amount of items: 2
Items: 
Size: 615885 Color: 0
Size: 384091 Color: 1

Bin 1380: 25 of cap free
Amount of items: 2
Items: 
Size: 648790 Color: 0
Size: 351186 Color: 1

Bin 1381: 25 of cap free
Amount of items: 2
Items: 
Size: 675380 Color: 1
Size: 324596 Color: 0

Bin 1382: 25 of cap free
Amount of items: 2
Items: 
Size: 676622 Color: 1
Size: 323354 Color: 0

Bin 1383: 25 of cap free
Amount of items: 2
Items: 
Size: 693866 Color: 1
Size: 306110 Color: 0

Bin 1384: 25 of cap free
Amount of items: 2
Items: 
Size: 701830 Color: 0
Size: 298146 Color: 1

Bin 1385: 25 of cap free
Amount of items: 2
Items: 
Size: 718091 Color: 0
Size: 281885 Color: 1

Bin 1386: 25 of cap free
Amount of items: 2
Items: 
Size: 756021 Color: 1
Size: 243955 Color: 0

Bin 1387: 25 of cap free
Amount of items: 2
Items: 
Size: 767025 Color: 0
Size: 232951 Color: 1

Bin 1388: 25 of cap free
Amount of items: 2
Items: 
Size: 767150 Color: 1
Size: 232826 Color: 0

Bin 1389: 25 of cap free
Amount of items: 2
Items: 
Size: 767340 Color: 1
Size: 232636 Color: 0

Bin 1390: 25 of cap free
Amount of items: 2
Items: 
Size: 793117 Color: 0
Size: 206859 Color: 1

Bin 1391: 25 of cap free
Amount of items: 2
Items: 
Size: 793860 Color: 0
Size: 206116 Color: 1

Bin 1392: 25 of cap free
Amount of items: 2
Items: 
Size: 798720 Color: 0
Size: 201256 Color: 1

Bin 1393: 26 of cap free
Amount of items: 2
Items: 
Size: 597504 Color: 0
Size: 402471 Color: 1

Bin 1394: 26 of cap free
Amount of items: 2
Items: 
Size: 528762 Color: 1
Size: 471213 Color: 0

Bin 1395: 26 of cap free
Amount of items: 2
Items: 
Size: 540946 Color: 0
Size: 459029 Color: 1

Bin 1396: 26 of cap free
Amount of items: 2
Items: 
Size: 563092 Color: 0
Size: 436883 Color: 1

Bin 1397: 26 of cap free
Amount of items: 2
Items: 
Size: 576723 Color: 1
Size: 423252 Color: 0

Bin 1398: 26 of cap free
Amount of items: 2
Items: 
Size: 594340 Color: 1
Size: 405635 Color: 0

Bin 1399: 26 of cap free
Amount of items: 2
Items: 
Size: 623856 Color: 0
Size: 376119 Color: 1

Bin 1400: 26 of cap free
Amount of items: 2
Items: 
Size: 631978 Color: 1
Size: 367997 Color: 0

Bin 1401: 26 of cap free
Amount of items: 2
Items: 
Size: 632568 Color: 1
Size: 367407 Color: 0

Bin 1402: 26 of cap free
Amount of items: 2
Items: 
Size: 645497 Color: 0
Size: 354478 Color: 1

Bin 1403: 26 of cap free
Amount of items: 2
Items: 
Size: 649880 Color: 0
Size: 350095 Color: 1

Bin 1404: 26 of cap free
Amount of items: 2
Items: 
Size: 691004 Color: 0
Size: 308971 Color: 1

Bin 1405: 26 of cap free
Amount of items: 2
Items: 
Size: 692667 Color: 0
Size: 307308 Color: 1

Bin 1406: 26 of cap free
Amount of items: 2
Items: 
Size: 733537 Color: 0
Size: 266438 Color: 1

Bin 1407: 26 of cap free
Amount of items: 2
Items: 
Size: 734467 Color: 0
Size: 265508 Color: 1

Bin 1408: 26 of cap free
Amount of items: 2
Items: 
Size: 749332 Color: 1
Size: 250643 Color: 0

Bin 1409: 26 of cap free
Amount of items: 2
Items: 
Size: 798030 Color: 0
Size: 201945 Color: 1

Bin 1410: 27 of cap free
Amount of items: 2
Items: 
Size: 788609 Color: 1
Size: 211365 Color: 0

Bin 1411: 27 of cap free
Amount of items: 2
Items: 
Size: 771125 Color: 1
Size: 228849 Color: 0

Bin 1412: 27 of cap free
Amount of items: 2
Items: 
Size: 615140 Color: 1
Size: 384834 Color: 0

Bin 1413: 27 of cap free
Amount of items: 3
Items: 
Size: 421727 Color: 1
Size: 416597 Color: 1
Size: 161650 Color: 0

Bin 1414: 27 of cap free
Amount of items: 2
Items: 
Size: 522863 Color: 0
Size: 477111 Color: 1

Bin 1415: 27 of cap free
Amount of items: 2
Items: 
Size: 528678 Color: 0
Size: 471296 Color: 1

Bin 1416: 27 of cap free
Amount of items: 2
Items: 
Size: 541877 Color: 1
Size: 458097 Color: 0

Bin 1417: 27 of cap free
Amount of items: 2
Items: 
Size: 557243 Color: 0
Size: 442731 Color: 1

Bin 1418: 27 of cap free
Amount of items: 2
Items: 
Size: 559507 Color: 0
Size: 440467 Color: 1

Bin 1419: 27 of cap free
Amount of items: 2
Items: 
Size: 565262 Color: 0
Size: 434712 Color: 1

Bin 1420: 27 of cap free
Amount of items: 2
Items: 
Size: 595751 Color: 0
Size: 404223 Color: 1

Bin 1421: 27 of cap free
Amount of items: 2
Items: 
Size: 627788 Color: 1
Size: 372186 Color: 0

Bin 1422: 27 of cap free
Amount of items: 2
Items: 
Size: 649920 Color: 1
Size: 350054 Color: 0

Bin 1423: 27 of cap free
Amount of items: 2
Items: 
Size: 662915 Color: 0
Size: 337059 Color: 1

Bin 1424: 27 of cap free
Amount of items: 2
Items: 
Size: 681680 Color: 1
Size: 318294 Color: 0

Bin 1425: 27 of cap free
Amount of items: 2
Items: 
Size: 726841 Color: 1
Size: 273133 Color: 0

Bin 1426: 27 of cap free
Amount of items: 2
Items: 
Size: 748121 Color: 0
Size: 251853 Color: 1

Bin 1427: 27 of cap free
Amount of items: 2
Items: 
Size: 763046 Color: 1
Size: 236928 Color: 0

Bin 1428: 27 of cap free
Amount of items: 2
Items: 
Size: 763648 Color: 1
Size: 236326 Color: 0

Bin 1429: 27 of cap free
Amount of items: 2
Items: 
Size: 781510 Color: 0
Size: 218464 Color: 1

Bin 1430: 27 of cap free
Amount of items: 2
Items: 
Size: 790654 Color: 0
Size: 209320 Color: 1

Bin 1431: 28 of cap free
Amount of items: 2
Items: 
Size: 617212 Color: 0
Size: 382761 Color: 1

Bin 1432: 28 of cap free
Amount of items: 3
Items: 
Size: 699435 Color: 1
Size: 169908 Color: 1
Size: 130630 Color: 0

Bin 1433: 28 of cap free
Amount of items: 2
Items: 
Size: 500977 Color: 0
Size: 498996 Color: 1

Bin 1434: 28 of cap free
Amount of items: 2
Items: 
Size: 524231 Color: 0
Size: 475742 Color: 1

Bin 1435: 28 of cap free
Amount of items: 2
Items: 
Size: 528733 Color: 1
Size: 471240 Color: 0

Bin 1436: 28 of cap free
Amount of items: 2
Items: 
Size: 541218 Color: 0
Size: 458755 Color: 1

Bin 1437: 28 of cap free
Amount of items: 2
Items: 
Size: 544651 Color: 0
Size: 455322 Color: 1

Bin 1438: 28 of cap free
Amount of items: 2
Items: 
Size: 547882 Color: 0
Size: 452091 Color: 1

Bin 1439: 28 of cap free
Amount of items: 2
Items: 
Size: 549809 Color: 1
Size: 450164 Color: 0

Bin 1440: 28 of cap free
Amount of items: 2
Items: 
Size: 591312 Color: 0
Size: 408661 Color: 1

Bin 1441: 28 of cap free
Amount of items: 2
Items: 
Size: 604375 Color: 0
Size: 395598 Color: 1

Bin 1442: 28 of cap free
Amount of items: 2
Items: 
Size: 610011 Color: 1
Size: 389962 Color: 0

Bin 1443: 28 of cap free
Amount of items: 2
Items: 
Size: 657906 Color: 0
Size: 342067 Color: 1

Bin 1444: 28 of cap free
Amount of items: 2
Items: 
Size: 659454 Color: 1
Size: 340519 Color: 0

Bin 1445: 28 of cap free
Amount of items: 2
Items: 
Size: 689065 Color: 1
Size: 310908 Color: 0

Bin 1446: 28 of cap free
Amount of items: 2
Items: 
Size: 701889 Color: 1
Size: 298084 Color: 0

Bin 1447: 28 of cap free
Amount of items: 2
Items: 
Size: 701960 Color: 0
Size: 298013 Color: 1

Bin 1448: 28 of cap free
Amount of items: 2
Items: 
Size: 706321 Color: 0
Size: 293652 Color: 1

Bin 1449: 28 of cap free
Amount of items: 2
Items: 
Size: 715457 Color: 1
Size: 284516 Color: 0

Bin 1450: 28 of cap free
Amount of items: 2
Items: 
Size: 720668 Color: 0
Size: 279305 Color: 1

Bin 1451: 28 of cap free
Amount of items: 2
Items: 
Size: 722321 Color: 0
Size: 277652 Color: 1

Bin 1452: 28 of cap free
Amount of items: 2
Items: 
Size: 765108 Color: 1
Size: 234865 Color: 0

Bin 1453: 28 of cap free
Amount of items: 2
Items: 
Size: 769570 Color: 0
Size: 230403 Color: 1

Bin 1454: 29 of cap free
Amount of items: 2
Items: 
Size: 719650 Color: 0
Size: 280322 Color: 1

Bin 1455: 29 of cap free
Amount of items: 2
Items: 
Size: 562171 Color: 1
Size: 437801 Color: 0

Bin 1456: 29 of cap free
Amount of items: 2
Items: 
Size: 762697 Color: 0
Size: 237275 Color: 1

Bin 1457: 29 of cap free
Amount of items: 3
Items: 
Size: 420687 Color: 1
Size: 388801 Color: 1
Size: 190484 Color: 0

Bin 1458: 29 of cap free
Amount of items: 2
Items: 
Size: 531796 Color: 0
Size: 468176 Color: 1

Bin 1459: 29 of cap free
Amount of items: 2
Items: 
Size: 536061 Color: 0
Size: 463911 Color: 1

Bin 1460: 29 of cap free
Amount of items: 2
Items: 
Size: 607473 Color: 0
Size: 392499 Color: 1

Bin 1461: 29 of cap free
Amount of items: 2
Items: 
Size: 612664 Color: 1
Size: 387308 Color: 0

Bin 1462: 29 of cap free
Amount of items: 2
Items: 
Size: 622281 Color: 1
Size: 377691 Color: 0

Bin 1463: 29 of cap free
Amount of items: 2
Items: 
Size: 626612 Color: 1
Size: 373360 Color: 0

Bin 1464: 29 of cap free
Amount of items: 2
Items: 
Size: 631768 Color: 1
Size: 368204 Color: 0

Bin 1465: 29 of cap free
Amount of items: 2
Items: 
Size: 663134 Color: 1
Size: 336838 Color: 0

Bin 1466: 29 of cap free
Amount of items: 2
Items: 
Size: 673343 Color: 1
Size: 326629 Color: 0

Bin 1467: 29 of cap free
Amount of items: 2
Items: 
Size: 681954 Color: 1
Size: 318018 Color: 0

Bin 1468: 29 of cap free
Amount of items: 2
Items: 
Size: 684088 Color: 0
Size: 315884 Color: 1

Bin 1469: 29 of cap free
Amount of items: 2
Items: 
Size: 782027 Color: 1
Size: 217945 Color: 0

Bin 1470: 29 of cap free
Amount of items: 2
Items: 
Size: 790281 Color: 1
Size: 209691 Color: 0

Bin 1471: 30 of cap free
Amount of items: 4
Items: 
Size: 361290 Color: 0
Size: 323886 Color: 1
Size: 191432 Color: 1
Size: 123363 Color: 0

Bin 1472: 30 of cap free
Amount of items: 2
Items: 
Size: 652692 Color: 1
Size: 347279 Color: 0

Bin 1473: 30 of cap free
Amount of items: 3
Items: 
Size: 360389 Color: 1
Size: 337947 Color: 0
Size: 301635 Color: 1

Bin 1474: 30 of cap free
Amount of items: 3
Items: 
Size: 378161 Color: 0
Size: 328113 Color: 0
Size: 293697 Color: 1

Bin 1475: 30 of cap free
Amount of items: 2
Items: 
Size: 519818 Color: 1
Size: 480153 Color: 0

Bin 1476: 30 of cap free
Amount of items: 2
Items: 
Size: 521187 Color: 0
Size: 478784 Color: 1

Bin 1477: 30 of cap free
Amount of items: 2
Items: 
Size: 564184 Color: 0
Size: 435787 Color: 1

Bin 1478: 30 of cap free
Amount of items: 2
Items: 
Size: 571625 Color: 1
Size: 428346 Color: 0

Bin 1479: 30 of cap free
Amount of items: 2
Items: 
Size: 572906 Color: 1
Size: 427065 Color: 0

Bin 1480: 30 of cap free
Amount of items: 2
Items: 
Size: 575086 Color: 0
Size: 424885 Color: 1

Bin 1481: 30 of cap free
Amount of items: 2
Items: 
Size: 577773 Color: 0
Size: 422198 Color: 1

Bin 1482: 30 of cap free
Amount of items: 2
Items: 
Size: 592477 Color: 1
Size: 407494 Color: 0

Bin 1483: 30 of cap free
Amount of items: 2
Items: 
Size: 599956 Color: 1
Size: 400015 Color: 0

Bin 1484: 30 of cap free
Amount of items: 2
Items: 
Size: 602254 Color: 0
Size: 397717 Color: 1

Bin 1485: 30 of cap free
Amount of items: 2
Items: 
Size: 650974 Color: 1
Size: 348997 Color: 0

Bin 1486: 30 of cap free
Amount of items: 2
Items: 
Size: 662474 Color: 0
Size: 337497 Color: 1

Bin 1487: 30 of cap free
Amount of items: 2
Items: 
Size: 666942 Color: 0
Size: 333029 Color: 1

Bin 1488: 30 of cap free
Amount of items: 2
Items: 
Size: 701618 Color: 0
Size: 298353 Color: 1

Bin 1489: 30 of cap free
Amount of items: 2
Items: 
Size: 708978 Color: 1
Size: 290993 Color: 0

Bin 1490: 30 of cap free
Amount of items: 2
Items: 
Size: 742720 Color: 0
Size: 257251 Color: 1

Bin 1491: 30 of cap free
Amount of items: 2
Items: 
Size: 757397 Color: 1
Size: 242574 Color: 0

Bin 1492: 30 of cap free
Amount of items: 2
Items: 
Size: 759273 Color: 0
Size: 240698 Color: 1

Bin 1493: 30 of cap free
Amount of items: 2
Items: 
Size: 792129 Color: 0
Size: 207842 Color: 1

Bin 1494: 31 of cap free
Amount of items: 6
Items: 
Size: 208811 Color: 0
Size: 162950 Color: 0
Size: 162548 Color: 0
Size: 155596 Color: 1
Size: 155401 Color: 1
Size: 154664 Color: 1

Bin 1495: 31 of cap free
Amount of items: 3
Items: 
Size: 374935 Color: 0
Size: 356179 Color: 1
Size: 268856 Color: 1

Bin 1496: 31 of cap free
Amount of items: 2
Items: 
Size: 735993 Color: 0
Size: 263977 Color: 1

Bin 1497: 31 of cap free
Amount of items: 3
Items: 
Size: 415380 Color: 1
Size: 339074 Color: 0
Size: 245516 Color: 0

Bin 1498: 31 of cap free
Amount of items: 2
Items: 
Size: 776005 Color: 1
Size: 223965 Color: 0

Bin 1499: 31 of cap free
Amount of items: 5
Items: 
Size: 330311 Color: 0
Size: 181625 Color: 1
Size: 175429 Color: 1
Size: 175307 Color: 1
Size: 137298 Color: 0

Bin 1500: 31 of cap free
Amount of items: 2
Items: 
Size: 506441 Color: 1
Size: 493529 Color: 0

Bin 1501: 31 of cap free
Amount of items: 2
Items: 
Size: 509512 Color: 1
Size: 490458 Color: 0

Bin 1502: 31 of cap free
Amount of items: 2
Items: 
Size: 518300 Color: 1
Size: 481670 Color: 0

Bin 1503: 31 of cap free
Amount of items: 2
Items: 
Size: 539349 Color: 0
Size: 460621 Color: 1

Bin 1504: 31 of cap free
Amount of items: 2
Items: 
Size: 545980 Color: 0
Size: 453990 Color: 1

Bin 1505: 31 of cap free
Amount of items: 2
Items: 
Size: 574820 Color: 1
Size: 425150 Color: 0

Bin 1506: 31 of cap free
Amount of items: 2
Items: 
Size: 590692 Color: 0
Size: 409278 Color: 1

Bin 1507: 31 of cap free
Amount of items: 2
Items: 
Size: 665605 Color: 1
Size: 334365 Color: 0

Bin 1508: 31 of cap free
Amount of items: 2
Items: 
Size: 698570 Color: 1
Size: 301400 Color: 0

Bin 1509: 31 of cap free
Amount of items: 2
Items: 
Size: 702336 Color: 0
Size: 297634 Color: 1

Bin 1510: 31 of cap free
Amount of items: 2
Items: 
Size: 731301 Color: 0
Size: 268669 Color: 1

Bin 1511: 31 of cap free
Amount of items: 2
Items: 
Size: 750831 Color: 1
Size: 249139 Color: 0

Bin 1512: 31 of cap free
Amount of items: 2
Items: 
Size: 763143 Color: 1
Size: 236827 Color: 0

Bin 1513: 31 of cap free
Amount of items: 2
Items: 
Size: 765474 Color: 0
Size: 234496 Color: 1

Bin 1514: 31 of cap free
Amount of items: 2
Items: 
Size: 795638 Color: 1
Size: 204332 Color: 0

Bin 1515: 32 of cap free
Amount of items: 2
Items: 
Size: 771349 Color: 1
Size: 228620 Color: 0

Bin 1516: 32 of cap free
Amount of items: 2
Items: 
Size: 554016 Color: 1
Size: 445953 Color: 0

Bin 1517: 32 of cap free
Amount of items: 2
Items: 
Size: 727912 Color: 0
Size: 272057 Color: 1

Bin 1518: 32 of cap free
Amount of items: 5
Items: 
Size: 232623 Color: 1
Size: 193757 Color: 0
Size: 193439 Color: 0
Size: 190162 Color: 1
Size: 189988 Color: 1

Bin 1519: 32 of cap free
Amount of items: 2
Items: 
Size: 763470 Color: 0
Size: 236499 Color: 1

Bin 1520: 32 of cap free
Amount of items: 2
Items: 
Size: 555718 Color: 1
Size: 444251 Color: 0

Bin 1521: 32 of cap free
Amount of items: 2
Items: 
Size: 566845 Color: 1
Size: 433124 Color: 0

Bin 1522: 32 of cap free
Amount of items: 2
Items: 
Size: 569468 Color: 1
Size: 430501 Color: 0

Bin 1523: 32 of cap free
Amount of items: 2
Items: 
Size: 574264 Color: 1
Size: 425705 Color: 0

Bin 1524: 32 of cap free
Amount of items: 2
Items: 
Size: 581891 Color: 1
Size: 418078 Color: 0

Bin 1525: 32 of cap free
Amount of items: 2
Items: 
Size: 589314 Color: 0
Size: 410655 Color: 1

Bin 1526: 32 of cap free
Amount of items: 2
Items: 
Size: 593415 Color: 0
Size: 406554 Color: 1

Bin 1527: 32 of cap free
Amount of items: 2
Items: 
Size: 626091 Color: 1
Size: 373878 Color: 0

Bin 1528: 32 of cap free
Amount of items: 2
Items: 
Size: 649328 Color: 0
Size: 350641 Color: 1

Bin 1529: 32 of cap free
Amount of items: 2
Items: 
Size: 651800 Color: 0
Size: 348169 Color: 1

Bin 1530: 32 of cap free
Amount of items: 2
Items: 
Size: 668117 Color: 1
Size: 331852 Color: 0

Bin 1531: 32 of cap free
Amount of items: 2
Items: 
Size: 683128 Color: 1
Size: 316841 Color: 0

Bin 1532: 32 of cap free
Amount of items: 2
Items: 
Size: 684011 Color: 1
Size: 315958 Color: 0

Bin 1533: 32 of cap free
Amount of items: 2
Items: 
Size: 714147 Color: 0
Size: 285822 Color: 1

Bin 1534: 32 of cap free
Amount of items: 2
Items: 
Size: 720626 Color: 1
Size: 279343 Color: 0

Bin 1535: 32 of cap free
Amount of items: 2
Items: 
Size: 728337 Color: 0
Size: 271632 Color: 1

Bin 1536: 32 of cap free
Amount of items: 2
Items: 
Size: 741325 Color: 1
Size: 258644 Color: 0

Bin 1537: 32 of cap free
Amount of items: 2
Items: 
Size: 752531 Color: 0
Size: 247438 Color: 1

Bin 1538: 32 of cap free
Amount of items: 2
Items: 
Size: 754603 Color: 1
Size: 245366 Color: 0

Bin 1539: 32 of cap free
Amount of items: 2
Items: 
Size: 780187 Color: 1
Size: 219782 Color: 0

Bin 1540: 32 of cap free
Amount of items: 2
Items: 
Size: 785148 Color: 0
Size: 214821 Color: 1

Bin 1541: 32 of cap free
Amount of items: 2
Items: 
Size: 785610 Color: 1
Size: 214359 Color: 0

Bin 1542: 33 of cap free
Amount of items: 3
Items: 
Size: 734898 Color: 1
Size: 146610 Color: 0
Size: 118460 Color: 1

Bin 1543: 33 of cap free
Amount of items: 5
Items: 
Size: 287576 Color: 1
Size: 184316 Color: 1
Size: 183845 Color: 0
Size: 178531 Color: 1
Size: 165700 Color: 0

Bin 1544: 33 of cap free
Amount of items: 2
Items: 
Size: 505706 Color: 0
Size: 494262 Color: 1

Bin 1545: 33 of cap free
Amount of items: 2
Items: 
Size: 505604 Color: 1
Size: 494364 Color: 0

Bin 1546: 33 of cap free
Amount of items: 2
Items: 
Size: 511662 Color: 0
Size: 488306 Color: 1

Bin 1547: 33 of cap free
Amount of items: 2
Items: 
Size: 584871 Color: 1
Size: 415097 Color: 0

Bin 1548: 33 of cap free
Amount of items: 2
Items: 
Size: 596165 Color: 0
Size: 403803 Color: 1

Bin 1549: 33 of cap free
Amount of items: 2
Items: 
Size: 615730 Color: 0
Size: 384238 Color: 1

Bin 1550: 33 of cap free
Amount of items: 2
Items: 
Size: 624458 Color: 1
Size: 375510 Color: 0

Bin 1551: 33 of cap free
Amount of items: 2
Items: 
Size: 638247 Color: 1
Size: 361721 Color: 0

Bin 1552: 33 of cap free
Amount of items: 2
Items: 
Size: 651559 Color: 1
Size: 348409 Color: 0

Bin 1553: 33 of cap free
Amount of items: 2
Items: 
Size: 676669 Color: 1
Size: 323299 Color: 0

Bin 1554: 33 of cap free
Amount of items: 2
Items: 
Size: 690913 Color: 1
Size: 309055 Color: 0

Bin 1555: 33 of cap free
Amount of items: 2
Items: 
Size: 732053 Color: 0
Size: 267915 Color: 1

Bin 1556: 33 of cap free
Amount of items: 2
Items: 
Size: 774170 Color: 0
Size: 225798 Color: 1

Bin 1557: 33 of cap free
Amount of items: 2
Items: 
Size: 780300 Color: 0
Size: 219668 Color: 1

Bin 1558: 33 of cap free
Amount of items: 2
Items: 
Size: 780553 Color: 0
Size: 219415 Color: 1

Bin 1559: 34 of cap free
Amount of items: 2
Items: 
Size: 610951 Color: 1
Size: 389016 Color: 0

Bin 1560: 34 of cap free
Amount of items: 2
Items: 
Size: 738091 Color: 1
Size: 261876 Color: 0

Bin 1561: 34 of cap free
Amount of items: 3
Items: 
Size: 379759 Color: 1
Size: 310298 Color: 0
Size: 309910 Color: 0

Bin 1562: 34 of cap free
Amount of items: 2
Items: 
Size: 528424 Color: 0
Size: 471543 Color: 1

Bin 1563: 34 of cap free
Amount of items: 2
Items: 
Size: 528821 Color: 0
Size: 471146 Color: 1

Bin 1564: 34 of cap free
Amount of items: 2
Items: 
Size: 585242 Color: 0
Size: 414725 Color: 1

Bin 1565: 34 of cap free
Amount of items: 2
Items: 
Size: 626186 Color: 0
Size: 373781 Color: 1

Bin 1566: 34 of cap free
Amount of items: 2
Items: 
Size: 675935 Color: 1
Size: 324032 Color: 0

Bin 1567: 34 of cap free
Amount of items: 2
Items: 
Size: 688406 Color: 0
Size: 311561 Color: 1

Bin 1568: 34 of cap free
Amount of items: 2
Items: 
Size: 689947 Color: 1
Size: 310020 Color: 0

Bin 1569: 34 of cap free
Amount of items: 2
Items: 
Size: 690183 Color: 1
Size: 309784 Color: 0

Bin 1570: 34 of cap free
Amount of items: 2
Items: 
Size: 708333 Color: 0
Size: 291634 Color: 1

Bin 1571: 34 of cap free
Amount of items: 2
Items: 
Size: 724364 Color: 0
Size: 275603 Color: 1

Bin 1572: 34 of cap free
Amount of items: 2
Items: 
Size: 728010 Color: 0
Size: 271957 Color: 1

Bin 1573: 34 of cap free
Amount of items: 2
Items: 
Size: 733124 Color: 1
Size: 266843 Color: 0

Bin 1574: 34 of cap free
Amount of items: 2
Items: 
Size: 773866 Color: 0
Size: 226101 Color: 1

Bin 1575: 34 of cap free
Amount of items: 2
Items: 
Size: 787254 Color: 1
Size: 212713 Color: 0

Bin 1576: 35 of cap free
Amount of items: 2
Items: 
Size: 667106 Color: 1
Size: 332860 Color: 0

Bin 1577: 35 of cap free
Amount of items: 2
Items: 
Size: 569431 Color: 0
Size: 430535 Color: 1

Bin 1578: 35 of cap free
Amount of items: 2
Items: 
Size: 569715 Color: 1
Size: 430251 Color: 0

Bin 1579: 35 of cap free
Amount of items: 2
Items: 
Size: 625648 Color: 0
Size: 374318 Color: 1

Bin 1580: 35 of cap free
Amount of items: 2
Items: 
Size: 636978 Color: 0
Size: 362988 Color: 1

Bin 1581: 35 of cap free
Amount of items: 2
Items: 
Size: 648158 Color: 0
Size: 351808 Color: 1

Bin 1582: 35 of cap free
Amount of items: 2
Items: 
Size: 651677 Color: 0
Size: 348289 Color: 1

Bin 1583: 35 of cap free
Amount of items: 2
Items: 
Size: 674161 Color: 1
Size: 325805 Color: 0

Bin 1584: 35 of cap free
Amount of items: 2
Items: 
Size: 695063 Color: 0
Size: 304903 Color: 1

Bin 1585: 35 of cap free
Amount of items: 2
Items: 
Size: 699223 Color: 1
Size: 300743 Color: 0

Bin 1586: 36 of cap free
Amount of items: 2
Items: 
Size: 796716 Color: 0
Size: 203249 Color: 1

Bin 1587: 36 of cap free
Amount of items: 3
Items: 
Size: 776382 Color: 1
Size: 118616 Color: 1
Size: 104967 Color: 0

Bin 1588: 36 of cap free
Amount of items: 2
Items: 
Size: 506850 Color: 0
Size: 493115 Color: 1

Bin 1589: 36 of cap free
Amount of items: 2
Items: 
Size: 524543 Color: 1
Size: 475422 Color: 0

Bin 1590: 36 of cap free
Amount of items: 2
Items: 
Size: 525547 Color: 0
Size: 474418 Color: 1

Bin 1591: 36 of cap free
Amount of items: 2
Items: 
Size: 604292 Color: 1
Size: 395673 Color: 0

Bin 1592: 36 of cap free
Amount of items: 2
Items: 
Size: 647263 Color: 1
Size: 352702 Color: 0

Bin 1593: 36 of cap free
Amount of items: 2
Items: 
Size: 648803 Color: 1
Size: 351162 Color: 0

Bin 1594: 36 of cap free
Amount of items: 2
Items: 
Size: 683421 Color: 0
Size: 316544 Color: 1

Bin 1595: 36 of cap free
Amount of items: 2
Items: 
Size: 696951 Color: 0
Size: 303014 Color: 1

Bin 1596: 36 of cap free
Amount of items: 2
Items: 
Size: 700708 Color: 0
Size: 299257 Color: 1

Bin 1597: 36 of cap free
Amount of items: 2
Items: 
Size: 712197 Color: 1
Size: 287768 Color: 0

Bin 1598: 36 of cap free
Amount of items: 2
Items: 
Size: 740970 Color: 1
Size: 258995 Color: 0

Bin 1599: 36 of cap free
Amount of items: 2
Items: 
Size: 751299 Color: 0
Size: 248666 Color: 1

Bin 1600: 37 of cap free
Amount of items: 3
Items: 
Size: 699996 Color: 1
Size: 151828 Color: 0
Size: 148140 Color: 1

Bin 1601: 37 of cap free
Amount of items: 2
Items: 
Size: 591866 Color: 0
Size: 408098 Color: 1

Bin 1602: 37 of cap free
Amount of items: 2
Items: 
Size: 505535 Color: 0
Size: 494429 Color: 1

Bin 1603: 37 of cap free
Amount of items: 2
Items: 
Size: 513673 Color: 1
Size: 486291 Color: 0

Bin 1604: 37 of cap free
Amount of items: 2
Items: 
Size: 539432 Color: 1
Size: 460532 Color: 0

Bin 1605: 37 of cap free
Amount of items: 2
Items: 
Size: 540414 Color: 0
Size: 459550 Color: 1

Bin 1606: 37 of cap free
Amount of items: 2
Items: 
Size: 541579 Color: 0
Size: 458385 Color: 1

Bin 1607: 37 of cap free
Amount of items: 2
Items: 
Size: 542600 Color: 0
Size: 457364 Color: 1

Bin 1608: 37 of cap free
Amount of items: 2
Items: 
Size: 571584 Color: 1
Size: 428380 Color: 0

Bin 1609: 37 of cap free
Amount of items: 2
Items: 
Size: 581030 Color: 0
Size: 418934 Color: 1

Bin 1610: 37 of cap free
Amount of items: 2
Items: 
Size: 640249 Color: 0
Size: 359715 Color: 1

Bin 1611: 37 of cap free
Amount of items: 2
Items: 
Size: 645088 Color: 0
Size: 354876 Color: 1

Bin 1612: 37 of cap free
Amount of items: 2
Items: 
Size: 667223 Color: 0
Size: 332741 Color: 1

Bin 1613: 37 of cap free
Amount of items: 2
Items: 
Size: 668698 Color: 0
Size: 331266 Color: 1

Bin 1614: 37 of cap free
Amount of items: 2
Items: 
Size: 674175 Color: 0
Size: 325789 Color: 1

Bin 1615: 37 of cap free
Amount of items: 2
Items: 
Size: 704888 Color: 1
Size: 295076 Color: 0

Bin 1616: 37 of cap free
Amount of items: 2
Items: 
Size: 731620 Color: 1
Size: 268344 Color: 0

Bin 1617: 37 of cap free
Amount of items: 2
Items: 
Size: 735098 Color: 0
Size: 264866 Color: 1

Bin 1618: 37 of cap free
Amount of items: 2
Items: 
Size: 751195 Color: 0
Size: 248769 Color: 1

Bin 1619: 37 of cap free
Amount of items: 2
Items: 
Size: 783583 Color: 1
Size: 216381 Color: 0

Bin 1620: 37 of cap free
Amount of items: 2
Items: 
Size: 796575 Color: 0
Size: 203389 Color: 1

Bin 1621: 38 of cap free
Amount of items: 2
Items: 
Size: 794471 Color: 0
Size: 205492 Color: 1

Bin 1622: 38 of cap free
Amount of items: 2
Items: 
Size: 515329 Color: 1
Size: 484634 Color: 0

Bin 1623: 38 of cap free
Amount of items: 3
Items: 
Size: 616956 Color: 0
Size: 217009 Color: 1
Size: 165998 Color: 0

Bin 1624: 38 of cap free
Amount of items: 2
Items: 
Size: 515529 Color: 1
Size: 484434 Color: 0

Bin 1625: 38 of cap free
Amount of items: 2
Items: 
Size: 550719 Color: 1
Size: 449244 Color: 0

Bin 1626: 38 of cap free
Amount of items: 2
Items: 
Size: 554484 Color: 0
Size: 445479 Color: 1

Bin 1627: 38 of cap free
Amount of items: 2
Items: 
Size: 571912 Color: 0
Size: 428051 Color: 1

Bin 1628: 38 of cap free
Amount of items: 2
Items: 
Size: 584670 Color: 1
Size: 415293 Color: 0

Bin 1629: 38 of cap free
Amount of items: 2
Items: 
Size: 593017 Color: 0
Size: 406946 Color: 1

Bin 1630: 38 of cap free
Amount of items: 2
Items: 
Size: 596704 Color: 1
Size: 403259 Color: 0

Bin 1631: 38 of cap free
Amount of items: 2
Items: 
Size: 629047 Color: 1
Size: 370916 Color: 0

Bin 1632: 38 of cap free
Amount of items: 2
Items: 
Size: 631091 Color: 0
Size: 368872 Color: 1

Bin 1633: 38 of cap free
Amount of items: 2
Items: 
Size: 662669 Color: 0
Size: 337294 Color: 1

Bin 1634: 38 of cap free
Amount of items: 2
Items: 
Size: 667960 Color: 1
Size: 332003 Color: 0

Bin 1635: 38 of cap free
Amount of items: 2
Items: 
Size: 693512 Color: 0
Size: 306451 Color: 1

Bin 1636: 38 of cap free
Amount of items: 2
Items: 
Size: 712049 Color: 0
Size: 287914 Color: 1

Bin 1637: 38 of cap free
Amount of items: 2
Items: 
Size: 713946 Color: 1
Size: 286017 Color: 0

Bin 1638: 38 of cap free
Amount of items: 2
Items: 
Size: 768160 Color: 0
Size: 231803 Color: 1

Bin 1639: 38 of cap free
Amount of items: 2
Items: 
Size: 797507 Color: 0
Size: 202456 Color: 1

Bin 1640: 39 of cap free
Amount of items: 2
Items: 
Size: 532647 Color: 0
Size: 467315 Color: 1

Bin 1641: 39 of cap free
Amount of items: 2
Items: 
Size: 538167 Color: 1
Size: 461795 Color: 0

Bin 1642: 39 of cap free
Amount of items: 2
Items: 
Size: 540056 Color: 1
Size: 459906 Color: 0

Bin 1643: 39 of cap free
Amount of items: 2
Items: 
Size: 573716 Color: 0
Size: 426246 Color: 1

Bin 1644: 39 of cap free
Amount of items: 2
Items: 
Size: 577641 Color: 0
Size: 422321 Color: 1

Bin 1645: 39 of cap free
Amount of items: 2
Items: 
Size: 611482 Color: 1
Size: 388480 Color: 0

Bin 1646: 39 of cap free
Amount of items: 2
Items: 
Size: 626059 Color: 0
Size: 373903 Color: 1

Bin 1647: 39 of cap free
Amount of items: 2
Items: 
Size: 626630 Color: 0
Size: 373332 Color: 1

Bin 1648: 39 of cap free
Amount of items: 2
Items: 
Size: 692514 Color: 0
Size: 307448 Color: 1

Bin 1649: 40 of cap free
Amount of items: 3
Items: 
Size: 569963 Color: 1
Size: 280762 Color: 1
Size: 149236 Color: 0

Bin 1650: 40 of cap free
Amount of items: 2
Items: 
Size: 600402 Color: 0
Size: 399559 Color: 1

Bin 1651: 40 of cap free
Amount of items: 2
Items: 
Size: 709935 Color: 1
Size: 290026 Color: 0

Bin 1652: 40 of cap free
Amount of items: 2
Items: 
Size: 783765 Color: 1
Size: 216196 Color: 0

Bin 1653: 40 of cap free
Amount of items: 2
Items: 
Size: 513921 Color: 1
Size: 486040 Color: 0

Bin 1654: 40 of cap free
Amount of items: 2
Items: 
Size: 516192 Color: 1
Size: 483769 Color: 0

Bin 1655: 40 of cap free
Amount of items: 2
Items: 
Size: 545309 Color: 1
Size: 454652 Color: 0

Bin 1656: 40 of cap free
Amount of items: 2
Items: 
Size: 548515 Color: 1
Size: 451446 Color: 0

Bin 1657: 40 of cap free
Amount of items: 2
Items: 
Size: 556400 Color: 1
Size: 443561 Color: 0

Bin 1658: 40 of cap free
Amount of items: 2
Items: 
Size: 575041 Color: 0
Size: 424920 Color: 1

Bin 1659: 40 of cap free
Amount of items: 2
Items: 
Size: 582811 Color: 0
Size: 417150 Color: 1

Bin 1660: 40 of cap free
Amount of items: 2
Items: 
Size: 594389 Color: 1
Size: 405572 Color: 0

Bin 1661: 40 of cap free
Amount of items: 2
Items: 
Size: 621876 Color: 1
Size: 378085 Color: 0

Bin 1662: 40 of cap free
Amount of items: 2
Items: 
Size: 632295 Color: 0
Size: 367666 Color: 1

Bin 1663: 40 of cap free
Amount of items: 2
Items: 
Size: 643334 Color: 0
Size: 356627 Color: 1

Bin 1664: 40 of cap free
Amount of items: 2
Items: 
Size: 662700 Color: 1
Size: 337261 Color: 0

Bin 1665: 40 of cap free
Amount of items: 2
Items: 
Size: 670684 Color: 0
Size: 329277 Color: 1

Bin 1666: 40 of cap free
Amount of items: 2
Items: 
Size: 673460 Color: 1
Size: 326501 Color: 0

Bin 1667: 40 of cap free
Amount of items: 2
Items: 
Size: 723392 Color: 0
Size: 276569 Color: 1

Bin 1668: 40 of cap free
Amount of items: 2
Items: 
Size: 739473 Color: 1
Size: 260488 Color: 0

Bin 1669: 41 of cap free
Amount of items: 3
Items: 
Size: 645506 Color: 1
Size: 210227 Color: 1
Size: 144227 Color: 0

Bin 1670: 41 of cap free
Amount of items: 2
Items: 
Size: 505798 Color: 0
Size: 494162 Color: 1

Bin 1671: 41 of cap free
Amount of items: 2
Items: 
Size: 520573 Color: 1
Size: 479387 Color: 0

Bin 1672: 41 of cap free
Amount of items: 2
Items: 
Size: 525047 Color: 0
Size: 474913 Color: 1

Bin 1673: 41 of cap free
Amount of items: 2
Items: 
Size: 570113 Color: 1
Size: 429847 Color: 0

Bin 1674: 41 of cap free
Amount of items: 2
Items: 
Size: 575484 Color: 1
Size: 424476 Color: 0

Bin 1675: 41 of cap free
Amount of items: 2
Items: 
Size: 609149 Color: 0
Size: 390811 Color: 1

Bin 1676: 41 of cap free
Amount of items: 2
Items: 
Size: 653484 Color: 1
Size: 346476 Color: 0

Bin 1677: 41 of cap free
Amount of items: 2
Items: 
Size: 655440 Color: 0
Size: 344520 Color: 1

Bin 1678: 41 of cap free
Amount of items: 2
Items: 
Size: 664192 Color: 1
Size: 335768 Color: 0

Bin 1679: 41 of cap free
Amount of items: 2
Items: 
Size: 664485 Color: 0
Size: 335475 Color: 1

Bin 1680: 41 of cap free
Amount of items: 2
Items: 
Size: 676375 Color: 0
Size: 323585 Color: 1

Bin 1681: 41 of cap free
Amount of items: 2
Items: 
Size: 682191 Color: 1
Size: 317769 Color: 0

Bin 1682: 41 of cap free
Amount of items: 2
Items: 
Size: 720347 Color: 1
Size: 279613 Color: 0

Bin 1683: 41 of cap free
Amount of items: 2
Items: 
Size: 731042 Color: 0
Size: 268918 Color: 1

Bin 1684: 41 of cap free
Amount of items: 2
Items: 
Size: 779768 Color: 0
Size: 220192 Color: 1

Bin 1685: 41 of cap free
Amount of items: 2
Items: 
Size: 781851 Color: 1
Size: 218109 Color: 0

Bin 1686: 41 of cap free
Amount of items: 2
Items: 
Size: 782958 Color: 1
Size: 217002 Color: 0

Bin 1687: 42 of cap free
Amount of items: 2
Items: 
Size: 747145 Color: 0
Size: 252814 Color: 1

Bin 1688: 42 of cap free
Amount of items: 2
Items: 
Size: 524930 Color: 1
Size: 475029 Color: 0

Bin 1689: 42 of cap free
Amount of items: 2
Items: 
Size: 743471 Color: 1
Size: 256488 Color: 0

Bin 1690: 42 of cap free
Amount of items: 2
Items: 
Size: 589172 Color: 1
Size: 410787 Color: 0

Bin 1691: 42 of cap free
Amount of items: 2
Items: 
Size: 501239 Color: 0
Size: 498720 Color: 1

Bin 1692: 42 of cap free
Amount of items: 2
Items: 
Size: 536862 Color: 0
Size: 463097 Color: 1

Bin 1693: 42 of cap free
Amount of items: 2
Items: 
Size: 553809 Color: 1
Size: 446150 Color: 0

Bin 1694: 42 of cap free
Amount of items: 2
Items: 
Size: 558005 Color: 0
Size: 441954 Color: 1

Bin 1695: 42 of cap free
Amount of items: 2
Items: 
Size: 560217 Color: 1
Size: 439742 Color: 0

Bin 1696: 42 of cap free
Amount of items: 2
Items: 
Size: 592503 Color: 0
Size: 407456 Color: 1

Bin 1697: 42 of cap free
Amount of items: 2
Items: 
Size: 638377 Color: 1
Size: 361582 Color: 0

Bin 1698: 42 of cap free
Amount of items: 2
Items: 
Size: 647175 Color: 1
Size: 352784 Color: 0

Bin 1699: 42 of cap free
Amount of items: 2
Items: 
Size: 682972 Color: 1
Size: 316987 Color: 0

Bin 1700: 42 of cap free
Amount of items: 2
Items: 
Size: 686621 Color: 1
Size: 313338 Color: 0

Bin 1701: 42 of cap free
Amount of items: 2
Items: 
Size: 692498 Color: 1
Size: 307461 Color: 0

Bin 1702: 42 of cap free
Amount of items: 2
Items: 
Size: 699406 Color: 0
Size: 300553 Color: 1

Bin 1703: 42 of cap free
Amount of items: 2
Items: 
Size: 710824 Color: 1
Size: 289135 Color: 0

Bin 1704: 42 of cap free
Amount of items: 2
Items: 
Size: 731880 Color: 0
Size: 268079 Color: 1

Bin 1705: 42 of cap free
Amount of items: 2
Items: 
Size: 763609 Color: 0
Size: 236350 Color: 1

Bin 1706: 42 of cap free
Amount of items: 2
Items: 
Size: 768750 Color: 0
Size: 231209 Color: 1

Bin 1707: 42 of cap free
Amount of items: 2
Items: 
Size: 786030 Color: 0
Size: 213929 Color: 1

Bin 1708: 42 of cap free
Amount of items: 2
Items: 
Size: 786235 Color: 1
Size: 213724 Color: 0

Bin 1709: 43 of cap free
Amount of items: 2
Items: 
Size: 506055 Color: 1
Size: 493903 Color: 0

Bin 1710: 43 of cap free
Amount of items: 2
Items: 
Size: 521934 Color: 1
Size: 478024 Color: 0

Bin 1711: 43 of cap free
Amount of items: 2
Items: 
Size: 537948 Color: 1
Size: 462010 Color: 0

Bin 1712: 43 of cap free
Amount of items: 2
Items: 
Size: 570970 Color: 1
Size: 428988 Color: 0

Bin 1713: 43 of cap free
Amount of items: 2
Items: 
Size: 571372 Color: 0
Size: 428586 Color: 1

Bin 1714: 43 of cap free
Amount of items: 2
Items: 
Size: 593218 Color: 1
Size: 406740 Color: 0

Bin 1715: 43 of cap free
Amount of items: 2
Items: 
Size: 619108 Color: 1
Size: 380850 Color: 0

Bin 1716: 43 of cap free
Amount of items: 2
Items: 
Size: 628976 Color: 0
Size: 370982 Color: 1

Bin 1717: 43 of cap free
Amount of items: 2
Items: 
Size: 722504 Color: 0
Size: 277454 Color: 1

Bin 1718: 43 of cap free
Amount of items: 2
Items: 
Size: 723217 Color: 1
Size: 276741 Color: 0

Bin 1719: 43 of cap free
Amount of items: 2
Items: 
Size: 759237 Color: 1
Size: 240721 Color: 0

Bin 1720: 44 of cap free
Amount of items: 2
Items: 
Size: 594117 Color: 0
Size: 405840 Color: 1

Bin 1721: 44 of cap free
Amount of items: 2
Items: 
Size: 762268 Color: 1
Size: 237689 Color: 0

Bin 1722: 44 of cap free
Amount of items: 2
Items: 
Size: 729757 Color: 1
Size: 270200 Color: 0

Bin 1723: 44 of cap free
Amount of items: 2
Items: 
Size: 705231 Color: 1
Size: 294726 Color: 0

Bin 1724: 44 of cap free
Amount of items: 2
Items: 
Size: 505216 Color: 1
Size: 494741 Color: 0

Bin 1725: 44 of cap free
Amount of items: 2
Items: 
Size: 522899 Color: 0
Size: 477058 Color: 1

Bin 1726: 44 of cap free
Amount of items: 2
Items: 
Size: 542185 Color: 1
Size: 457772 Color: 0

Bin 1727: 44 of cap free
Amount of items: 2
Items: 
Size: 543767 Color: 0
Size: 456190 Color: 1

Bin 1728: 44 of cap free
Amount of items: 2
Items: 
Size: 559275 Color: 0
Size: 440682 Color: 1

Bin 1729: 44 of cap free
Amount of items: 2
Items: 
Size: 573333 Color: 0
Size: 426624 Color: 1

Bin 1730: 44 of cap free
Amount of items: 2
Items: 
Size: 610514 Color: 1
Size: 389443 Color: 0

Bin 1731: 44 of cap free
Amount of items: 2
Items: 
Size: 628551 Color: 0
Size: 371406 Color: 1

Bin 1732: 44 of cap free
Amount of items: 2
Items: 
Size: 635813 Color: 1
Size: 364144 Color: 0

Bin 1733: 44 of cap free
Amount of items: 2
Items: 
Size: 641781 Color: 0
Size: 358176 Color: 1

Bin 1734: 44 of cap free
Amount of items: 2
Items: 
Size: 654226 Color: 0
Size: 345731 Color: 1

Bin 1735: 44 of cap free
Amount of items: 2
Items: 
Size: 680045 Color: 1
Size: 319912 Color: 0

Bin 1736: 44 of cap free
Amount of items: 2
Items: 
Size: 708909 Color: 0
Size: 291048 Color: 1

Bin 1737: 44 of cap free
Amount of items: 2
Items: 
Size: 715975 Color: 0
Size: 283982 Color: 1

Bin 1738: 44 of cap free
Amount of items: 2
Items: 
Size: 736874 Color: 0
Size: 263083 Color: 1

Bin 1739: 44 of cap free
Amount of items: 2
Items: 
Size: 737981 Color: 1
Size: 261976 Color: 0

Bin 1740: 44 of cap free
Amount of items: 2
Items: 
Size: 746700 Color: 1
Size: 253257 Color: 0

Bin 1741: 44 of cap free
Amount of items: 2
Items: 
Size: 753575 Color: 0
Size: 246382 Color: 1

Bin 1742: 44 of cap free
Amount of items: 2
Items: 
Size: 789310 Color: 1
Size: 210647 Color: 0

Bin 1743: 45 of cap free
Amount of items: 2
Items: 
Size: 606846 Color: 0
Size: 393110 Color: 1

Bin 1744: 45 of cap free
Amount of items: 2
Items: 
Size: 638390 Color: 0
Size: 361566 Color: 1

Bin 1745: 45 of cap free
Amount of items: 3
Items: 
Size: 391586 Color: 0
Size: 317507 Color: 1
Size: 290863 Color: 1

Bin 1746: 45 of cap free
Amount of items: 2
Items: 
Size: 544864 Color: 1
Size: 455092 Color: 0

Bin 1747: 45 of cap free
Amount of items: 2
Items: 
Size: 553258 Color: 1
Size: 446698 Color: 0

Bin 1748: 45 of cap free
Amount of items: 2
Items: 
Size: 554739 Color: 0
Size: 445217 Color: 1

Bin 1749: 45 of cap free
Amount of items: 2
Items: 
Size: 565708 Color: 1
Size: 434248 Color: 0

Bin 1750: 45 of cap free
Amount of items: 2
Items: 
Size: 646874 Color: 1
Size: 353082 Color: 0

Bin 1751: 45 of cap free
Amount of items: 2
Items: 
Size: 682599 Color: 1
Size: 317357 Color: 0

Bin 1752: 45 of cap free
Amount of items: 2
Items: 
Size: 685256 Color: 1
Size: 314700 Color: 0

Bin 1753: 45 of cap free
Amount of items: 2
Items: 
Size: 691038 Color: 1
Size: 308918 Color: 0

Bin 1754: 45 of cap free
Amount of items: 2
Items: 
Size: 695355 Color: 0
Size: 304601 Color: 1

Bin 1755: 45 of cap free
Amount of items: 2
Items: 
Size: 721619 Color: 1
Size: 278337 Color: 0

Bin 1756: 45 of cap free
Amount of items: 2
Items: 
Size: 742464 Color: 1
Size: 257492 Color: 0

Bin 1757: 46 of cap free
Amount of items: 5
Items: 
Size: 264722 Color: 0
Size: 186518 Color: 0
Size: 186289 Color: 0
Size: 181450 Color: 1
Size: 180976 Color: 1

Bin 1758: 46 of cap free
Amount of items: 2
Items: 
Size: 508539 Color: 1
Size: 491416 Color: 0

Bin 1759: 46 of cap free
Amount of items: 2
Items: 
Size: 513073 Color: 1
Size: 486882 Color: 0

Bin 1760: 46 of cap free
Amount of items: 2
Items: 
Size: 529733 Color: 1
Size: 470222 Color: 0

Bin 1761: 46 of cap free
Amount of items: 2
Items: 
Size: 559448 Color: 0
Size: 440507 Color: 1

Bin 1762: 46 of cap free
Amount of items: 2
Items: 
Size: 560945 Color: 0
Size: 439010 Color: 1

Bin 1763: 46 of cap free
Amount of items: 2
Items: 
Size: 566103 Color: 1
Size: 433852 Color: 0

Bin 1764: 46 of cap free
Amount of items: 2
Items: 
Size: 593559 Color: 1
Size: 406396 Color: 0

Bin 1765: 46 of cap free
Amount of items: 2
Items: 
Size: 641547 Color: 1
Size: 358408 Color: 0

Bin 1766: 46 of cap free
Amount of items: 2
Items: 
Size: 670422 Color: 1
Size: 329533 Color: 0

Bin 1767: 46 of cap free
Amount of items: 2
Items: 
Size: 671590 Color: 1
Size: 328365 Color: 0

Bin 1768: 46 of cap free
Amount of items: 2
Items: 
Size: 676859 Color: 1
Size: 323096 Color: 0

Bin 1769: 46 of cap free
Amount of items: 2
Items: 
Size: 677049 Color: 0
Size: 322906 Color: 1

Bin 1770: 46 of cap free
Amount of items: 2
Items: 
Size: 693033 Color: 1
Size: 306922 Color: 0

Bin 1771: 46 of cap free
Amount of items: 2
Items: 
Size: 727562 Color: 1
Size: 272393 Color: 0

Bin 1772: 46 of cap free
Amount of items: 2
Items: 
Size: 752306 Color: 1
Size: 247649 Color: 0

Bin 1773: 46 of cap free
Amount of items: 2
Items: 
Size: 783906 Color: 1
Size: 216049 Color: 0

Bin 1774: 46 of cap free
Amount of items: 2
Items: 
Size: 795623 Color: 1
Size: 204332 Color: 0

Bin 1775: 47 of cap free
Amount of items: 3
Items: 
Size: 717715 Color: 1
Size: 144862 Color: 0
Size: 137377 Color: 1

Bin 1776: 47 of cap free
Amount of items: 2
Items: 
Size: 797696 Color: 1
Size: 202258 Color: 0

Bin 1777: 47 of cap free
Amount of items: 2
Items: 
Size: 532752 Color: 0
Size: 467202 Color: 1

Bin 1778: 47 of cap free
Amount of items: 2
Items: 
Size: 532916 Color: 0
Size: 467038 Color: 1

Bin 1779: 47 of cap free
Amount of items: 2
Items: 
Size: 538774 Color: 0
Size: 461180 Color: 1

Bin 1780: 47 of cap free
Amount of items: 2
Items: 
Size: 603995 Color: 0
Size: 395959 Color: 1

Bin 1781: 47 of cap free
Amount of items: 2
Items: 
Size: 613291 Color: 1
Size: 386663 Color: 0

Bin 1782: 47 of cap free
Amount of items: 2
Items: 
Size: 694600 Color: 1
Size: 305354 Color: 0

Bin 1783: 47 of cap free
Amount of items: 2
Items: 
Size: 694848 Color: 0
Size: 305106 Color: 1

Bin 1784: 47 of cap free
Amount of items: 2
Items: 
Size: 707478 Color: 0
Size: 292476 Color: 1

Bin 1785: 47 of cap free
Amount of items: 2
Items: 
Size: 710104 Color: 0
Size: 289850 Color: 1

Bin 1786: 48 of cap free
Amount of items: 2
Items: 
Size: 710368 Color: 0
Size: 289585 Color: 1

Bin 1787: 48 of cap free
Amount of items: 2
Items: 
Size: 600737 Color: 0
Size: 399216 Color: 1

Bin 1788: 48 of cap free
Amount of items: 2
Items: 
Size: 521706 Color: 0
Size: 478247 Color: 1

Bin 1789: 48 of cap free
Amount of items: 2
Items: 
Size: 571947 Color: 1
Size: 428006 Color: 0

Bin 1790: 48 of cap free
Amount of items: 2
Items: 
Size: 596596 Color: 1
Size: 403357 Color: 0

Bin 1791: 48 of cap free
Amount of items: 2
Items: 
Size: 605542 Color: 1
Size: 394411 Color: 0

Bin 1792: 48 of cap free
Amount of items: 2
Items: 
Size: 606780 Color: 0
Size: 393173 Color: 1

Bin 1793: 48 of cap free
Amount of items: 2
Items: 
Size: 620718 Color: 0
Size: 379235 Color: 1

Bin 1794: 48 of cap free
Amount of items: 2
Items: 
Size: 663012 Color: 1
Size: 336941 Color: 0

Bin 1795: 48 of cap free
Amount of items: 2
Items: 
Size: 663903 Color: 0
Size: 336050 Color: 1

Bin 1796: 48 of cap free
Amount of items: 2
Items: 
Size: 674493 Color: 0
Size: 325460 Color: 1

Bin 1797: 48 of cap free
Amount of items: 2
Items: 
Size: 701660 Color: 0
Size: 298293 Color: 1

Bin 1798: 48 of cap free
Amount of items: 2
Items: 
Size: 734818 Color: 0
Size: 265135 Color: 1

Bin 1799: 48 of cap free
Amount of items: 2
Items: 
Size: 767270 Color: 0
Size: 232683 Color: 1

Bin 1800: 48 of cap free
Amount of items: 2
Items: 
Size: 769482 Color: 0
Size: 230471 Color: 1

Bin 1801: 49 of cap free
Amount of items: 2
Items: 
Size: 719914 Color: 0
Size: 280038 Color: 1

Bin 1802: 49 of cap free
Amount of items: 2
Items: 
Size: 688955 Color: 1
Size: 310997 Color: 0

Bin 1803: 49 of cap free
Amount of items: 2
Items: 
Size: 506331 Color: 1
Size: 493621 Color: 0

Bin 1804: 49 of cap free
Amount of items: 2
Items: 
Size: 522087 Color: 1
Size: 477865 Color: 0

Bin 1805: 49 of cap free
Amount of items: 2
Items: 
Size: 533114 Color: 0
Size: 466838 Color: 1

Bin 1806: 49 of cap free
Amount of items: 2
Items: 
Size: 536006 Color: 0
Size: 463946 Color: 1

Bin 1807: 49 of cap free
Amount of items: 2
Items: 
Size: 538511 Color: 0
Size: 461441 Color: 1

Bin 1808: 49 of cap free
Amount of items: 2
Items: 
Size: 597871 Color: 1
Size: 402081 Color: 0

Bin 1809: 49 of cap free
Amount of items: 2
Items: 
Size: 598289 Color: 1
Size: 401663 Color: 0

Bin 1810: 49 of cap free
Amount of items: 2
Items: 
Size: 604637 Color: 0
Size: 395315 Color: 1

Bin 1811: 49 of cap free
Amount of items: 2
Items: 
Size: 612106 Color: 1
Size: 387846 Color: 0

Bin 1812: 49 of cap free
Amount of items: 2
Items: 
Size: 630518 Color: 0
Size: 369434 Color: 1

Bin 1813: 49 of cap free
Amount of items: 2
Items: 
Size: 634416 Color: 1
Size: 365536 Color: 0

Bin 1814: 49 of cap free
Amount of items: 2
Items: 
Size: 636042 Color: 1
Size: 363910 Color: 0

Bin 1815: 49 of cap free
Amount of items: 2
Items: 
Size: 647778 Color: 0
Size: 352174 Color: 1

Bin 1816: 49 of cap free
Amount of items: 2
Items: 
Size: 705504 Color: 1
Size: 294448 Color: 0

Bin 1817: 49 of cap free
Amount of items: 2
Items: 
Size: 760645 Color: 0
Size: 239307 Color: 1

Bin 1818: 49 of cap free
Amount of items: 2
Items: 
Size: 782045 Color: 0
Size: 217907 Color: 1

Bin 1819: 49 of cap free
Amount of items: 2
Items: 
Size: 786231 Color: 0
Size: 213721 Color: 1

Bin 1820: 50 of cap free
Amount of items: 2
Items: 
Size: 799639 Color: 1
Size: 200312 Color: 0

Bin 1821: 50 of cap free
Amount of items: 2
Items: 
Size: 798833 Color: 1
Size: 201118 Color: 0

Bin 1822: 50 of cap free
Amount of items: 2
Items: 
Size: 768314 Color: 0
Size: 231637 Color: 1

Bin 1823: 50 of cap free
Amount of items: 2
Items: 
Size: 509755 Color: 0
Size: 490196 Color: 1

Bin 1824: 50 of cap free
Amount of items: 2
Items: 
Size: 572363 Color: 0
Size: 427588 Color: 1

Bin 1825: 50 of cap free
Amount of items: 2
Items: 
Size: 662153 Color: 0
Size: 337798 Color: 1

Bin 1826: 50 of cap free
Amount of items: 2
Items: 
Size: 677940 Color: 0
Size: 322011 Color: 1

Bin 1827: 50 of cap free
Amount of items: 2
Items: 
Size: 689456 Color: 1
Size: 310495 Color: 0

Bin 1828: 50 of cap free
Amount of items: 2
Items: 
Size: 725984 Color: 0
Size: 273967 Color: 1

Bin 1829: 50 of cap free
Amount of items: 2
Items: 
Size: 726857 Color: 0
Size: 273094 Color: 1

Bin 1830: 50 of cap free
Amount of items: 2
Items: 
Size: 744508 Color: 0
Size: 255443 Color: 1

Bin 1831: 50 of cap free
Amount of items: 2
Items: 
Size: 765851 Color: 1
Size: 234100 Color: 0

Bin 1832: 50 of cap free
Amount of items: 2
Items: 
Size: 765997 Color: 1
Size: 233954 Color: 0

Bin 1833: 51 of cap free
Amount of items: 6
Items: 
Size: 197214 Color: 0
Size: 174580 Color: 0
Size: 163634 Color: 1
Size: 163551 Color: 1
Size: 153452 Color: 0
Size: 147519 Color: 1

Bin 1834: 51 of cap free
Amount of items: 2
Items: 
Size: 614337 Color: 1
Size: 385613 Color: 0

Bin 1835: 51 of cap free
Amount of items: 2
Items: 
Size: 634453 Color: 0
Size: 365497 Color: 1

Bin 1836: 51 of cap free
Amount of items: 2
Items: 
Size: 644679 Color: 1
Size: 355271 Color: 0

Bin 1837: 51 of cap free
Amount of items: 2
Items: 
Size: 676614 Color: 1
Size: 323336 Color: 0

Bin 1838: 51 of cap free
Amount of items: 2
Items: 
Size: 686292 Color: 1
Size: 313658 Color: 0

Bin 1839: 51 of cap free
Amount of items: 2
Items: 
Size: 718892 Color: 0
Size: 281058 Color: 1

Bin 1840: 51 of cap free
Amount of items: 2
Items: 
Size: 763281 Color: 1
Size: 236669 Color: 0

Bin 1841: 51 of cap free
Amount of items: 2
Items: 
Size: 789359 Color: 0
Size: 210591 Color: 1

Bin 1842: 52 of cap free
Amount of items: 2
Items: 
Size: 520035 Color: 1
Size: 479914 Color: 0

Bin 1843: 52 of cap free
Amount of items: 2
Items: 
Size: 523699 Color: 1
Size: 476250 Color: 0

Bin 1844: 52 of cap free
Amount of items: 2
Items: 
Size: 531574 Color: 1
Size: 468375 Color: 0

Bin 1845: 52 of cap free
Amount of items: 2
Items: 
Size: 544984 Color: 0
Size: 454965 Color: 1

Bin 1846: 52 of cap free
Amount of items: 2
Items: 
Size: 602276 Color: 1
Size: 397673 Color: 0

Bin 1847: 52 of cap free
Amount of items: 2
Items: 
Size: 607784 Color: 1
Size: 392165 Color: 0

Bin 1848: 52 of cap free
Amount of items: 2
Items: 
Size: 623228 Color: 1
Size: 376721 Color: 0

Bin 1849: 52 of cap free
Amount of items: 2
Items: 
Size: 627573 Color: 0
Size: 372376 Color: 1

Bin 1850: 52 of cap free
Amount of items: 2
Items: 
Size: 634136 Color: 0
Size: 365813 Color: 1

Bin 1851: 52 of cap free
Amount of items: 2
Items: 
Size: 645814 Color: 0
Size: 354135 Color: 1

Bin 1852: 52 of cap free
Amount of items: 2
Items: 
Size: 661808 Color: 0
Size: 338141 Color: 1

Bin 1853: 52 of cap free
Amount of items: 2
Items: 
Size: 673959 Color: 0
Size: 325990 Color: 1

Bin 1854: 52 of cap free
Amount of items: 2
Items: 
Size: 678366 Color: 0
Size: 321583 Color: 1

Bin 1855: 52 of cap free
Amount of items: 2
Items: 
Size: 688790 Color: 0
Size: 311159 Color: 1

Bin 1856: 52 of cap free
Amount of items: 2
Items: 
Size: 756986 Color: 1
Size: 242963 Color: 0

Bin 1857: 52 of cap free
Amount of items: 2
Items: 
Size: 759819 Color: 0
Size: 240130 Color: 1

Bin 1858: 52 of cap free
Amount of items: 2
Items: 
Size: 789760 Color: 1
Size: 210189 Color: 0

Bin 1859: 53 of cap free
Amount of items: 2
Items: 
Size: 771520 Color: 1
Size: 228428 Color: 0

Bin 1860: 53 of cap free
Amount of items: 2
Items: 
Size: 509422 Color: 1
Size: 490526 Color: 0

Bin 1861: 53 of cap free
Amount of items: 2
Items: 
Size: 607059 Color: 0
Size: 392889 Color: 1

Bin 1862: 53 of cap free
Amount of items: 2
Items: 
Size: 707091 Color: 0
Size: 292857 Color: 1

Bin 1863: 53 of cap free
Amount of items: 2
Items: 
Size: 756327 Color: 0
Size: 243621 Color: 1

Bin 1864: 53 of cap free
Amount of items: 2
Items: 
Size: 777005 Color: 1
Size: 222943 Color: 0

Bin 1865: 53 of cap free
Amount of items: 2
Items: 
Size: 785593 Color: 0
Size: 214355 Color: 1

Bin 1866: 54 of cap free
Amount of items: 3
Items: 
Size: 378431 Color: 0
Size: 317132 Color: 1
Size: 304384 Color: 1

Bin 1867: 54 of cap free
Amount of items: 2
Items: 
Size: 503111 Color: 1
Size: 496836 Color: 0

Bin 1868: 54 of cap free
Amount of items: 2
Items: 
Size: 548564 Color: 1
Size: 451383 Color: 0

Bin 1869: 54 of cap free
Amount of items: 2
Items: 
Size: 592247 Color: 0
Size: 407700 Color: 1

Bin 1870: 54 of cap free
Amount of items: 2
Items: 
Size: 617020 Color: 1
Size: 382927 Color: 0

Bin 1871: 54 of cap free
Amount of items: 2
Items: 
Size: 625517 Color: 0
Size: 374430 Color: 1

Bin 1872: 54 of cap free
Amount of items: 2
Items: 
Size: 637866 Color: 1
Size: 362081 Color: 0

Bin 1873: 54 of cap free
Amount of items: 2
Items: 
Size: 644063 Color: 1
Size: 355884 Color: 0

Bin 1874: 54 of cap free
Amount of items: 2
Items: 
Size: 665673 Color: 0
Size: 334274 Color: 1

Bin 1875: 54 of cap free
Amount of items: 2
Items: 
Size: 666334 Color: 1
Size: 333613 Color: 0

Bin 1876: 54 of cap free
Amount of items: 2
Items: 
Size: 720444 Color: 0
Size: 279503 Color: 1

Bin 1877: 54 of cap free
Amount of items: 2
Items: 
Size: 725239 Color: 0
Size: 274708 Color: 1

Bin 1878: 54 of cap free
Amount of items: 2
Items: 
Size: 731429 Color: 0
Size: 268518 Color: 1

Bin 1879: 55 of cap free
Amount of items: 2
Items: 
Size: 547143 Color: 1
Size: 452803 Color: 0

Bin 1880: 55 of cap free
Amount of items: 2
Items: 
Size: 581945 Color: 0
Size: 418001 Color: 1

Bin 1881: 55 of cap free
Amount of items: 2
Items: 
Size: 502306 Color: 0
Size: 497640 Color: 1

Bin 1882: 55 of cap free
Amount of items: 2
Items: 
Size: 523443 Color: 1
Size: 476503 Color: 0

Bin 1883: 55 of cap free
Amount of items: 2
Items: 
Size: 555072 Color: 1
Size: 444874 Color: 0

Bin 1884: 55 of cap free
Amount of items: 2
Items: 
Size: 599387 Color: 1
Size: 400559 Color: 0

Bin 1885: 55 of cap free
Amount of items: 2
Items: 
Size: 607516 Color: 1
Size: 392430 Color: 0

Bin 1886: 55 of cap free
Amount of items: 2
Items: 
Size: 610583 Color: 1
Size: 389363 Color: 0

Bin 1887: 55 of cap free
Amount of items: 2
Items: 
Size: 633050 Color: 0
Size: 366896 Color: 1

Bin 1888: 55 of cap free
Amount of items: 2
Items: 
Size: 678165 Color: 1
Size: 321781 Color: 0

Bin 1889: 55 of cap free
Amount of items: 2
Items: 
Size: 680198 Color: 1
Size: 319748 Color: 0

Bin 1890: 55 of cap free
Amount of items: 2
Items: 
Size: 751935 Color: 0
Size: 248011 Color: 1

Bin 1891: 55 of cap free
Amount of items: 2
Items: 
Size: 767766 Color: 0
Size: 232180 Color: 1

Bin 1892: 56 of cap free
Amount of items: 2
Items: 
Size: 779135 Color: 1
Size: 220810 Color: 0

Bin 1893: 56 of cap free
Amount of items: 2
Items: 
Size: 534426 Color: 1
Size: 465519 Color: 0

Bin 1894: 56 of cap free
Amount of items: 2
Items: 
Size: 541752 Color: 0
Size: 458193 Color: 1

Bin 1895: 56 of cap free
Amount of items: 2
Items: 
Size: 541766 Color: 1
Size: 458179 Color: 0

Bin 1896: 56 of cap free
Amount of items: 2
Items: 
Size: 556387 Color: 1
Size: 443558 Color: 0

Bin 1897: 56 of cap free
Amount of items: 2
Items: 
Size: 613036 Color: 0
Size: 386909 Color: 1

Bin 1898: 56 of cap free
Amount of items: 2
Items: 
Size: 626343 Color: 1
Size: 373602 Color: 0

Bin 1899: 56 of cap free
Amount of items: 2
Items: 
Size: 655447 Color: 1
Size: 344498 Color: 0

Bin 1900: 56 of cap free
Amount of items: 2
Items: 
Size: 665207 Color: 0
Size: 334738 Color: 1

Bin 1901: 57 of cap free
Amount of items: 2
Items: 
Size: 550148 Color: 0
Size: 449796 Color: 1

Bin 1902: 57 of cap free
Amount of items: 2
Items: 
Size: 561955 Color: 0
Size: 437989 Color: 1

Bin 1903: 57 of cap free
Amount of items: 2
Items: 
Size: 591908 Color: 0
Size: 408036 Color: 1

Bin 1904: 57 of cap free
Amount of items: 2
Items: 
Size: 603204 Color: 0
Size: 396740 Color: 1

Bin 1905: 57 of cap free
Amount of items: 2
Items: 
Size: 616772 Color: 1
Size: 383172 Color: 0

Bin 1906: 57 of cap free
Amount of items: 2
Items: 
Size: 620837 Color: 0
Size: 379107 Color: 1

Bin 1907: 57 of cap free
Amount of items: 2
Items: 
Size: 656943 Color: 0
Size: 343001 Color: 1

Bin 1908: 57 of cap free
Amount of items: 2
Items: 
Size: 693850 Color: 1
Size: 306094 Color: 0

Bin 1909: 57 of cap free
Amount of items: 2
Items: 
Size: 766565 Color: 1
Size: 233379 Color: 0

Bin 1910: 58 of cap free
Amount of items: 7
Items: 
Size: 191237 Color: 1
Size: 135574 Color: 0
Size: 135470 Color: 0
Size: 135081 Color: 0
Size: 134695 Color: 1
Size: 134227 Color: 1
Size: 133659 Color: 1

Bin 1911: 58 of cap free
Amount of items: 2
Items: 
Size: 510681 Color: 1
Size: 489262 Color: 0

Bin 1912: 58 of cap free
Amount of items: 2
Items: 
Size: 513339 Color: 0
Size: 486604 Color: 1

Bin 1913: 58 of cap free
Amount of items: 2
Items: 
Size: 551952 Color: 1
Size: 447991 Color: 0

Bin 1914: 58 of cap free
Amount of items: 2
Items: 
Size: 569800 Color: 1
Size: 430143 Color: 0

Bin 1915: 58 of cap free
Amount of items: 2
Items: 
Size: 607181 Color: 0
Size: 392762 Color: 1

Bin 1916: 58 of cap free
Amount of items: 2
Items: 
Size: 613410 Color: 0
Size: 386533 Color: 1

Bin 1917: 58 of cap free
Amount of items: 2
Items: 
Size: 642377 Color: 0
Size: 357566 Color: 1

Bin 1918: 58 of cap free
Amount of items: 2
Items: 
Size: 662970 Color: 0
Size: 336973 Color: 1

Bin 1919: 58 of cap free
Amount of items: 2
Items: 
Size: 700398 Color: 1
Size: 299545 Color: 0

Bin 1920: 58 of cap free
Amount of items: 2
Items: 
Size: 733596 Color: 1
Size: 266347 Color: 0

Bin 1921: 58 of cap free
Amount of items: 2
Items: 
Size: 738272 Color: 1
Size: 261671 Color: 0

Bin 1922: 59 of cap free
Amount of items: 3
Items: 
Size: 591158 Color: 0
Size: 267754 Color: 1
Size: 141030 Color: 0

Bin 1923: 59 of cap free
Amount of items: 2
Items: 
Size: 794732 Color: 0
Size: 205210 Color: 1

Bin 1924: 59 of cap free
Amount of items: 2
Items: 
Size: 779415 Color: 0
Size: 220527 Color: 1

Bin 1925: 59 of cap free
Amount of items: 2
Items: 
Size: 590822 Color: 0
Size: 409120 Color: 1

Bin 1926: 59 of cap free
Amount of items: 2
Items: 
Size: 592115 Color: 1
Size: 407827 Color: 0

Bin 1927: 59 of cap free
Amount of items: 2
Items: 
Size: 596007 Color: 1
Size: 403935 Color: 0

Bin 1928: 59 of cap free
Amount of items: 2
Items: 
Size: 630565 Color: 1
Size: 369377 Color: 0

Bin 1929: 59 of cap free
Amount of items: 2
Items: 
Size: 640813 Color: 0
Size: 359129 Color: 1

Bin 1930: 59 of cap free
Amount of items: 2
Items: 
Size: 653744 Color: 0
Size: 346198 Color: 1

Bin 1931: 59 of cap free
Amount of items: 2
Items: 
Size: 722646 Color: 0
Size: 277296 Color: 1

Bin 1932: 59 of cap free
Amount of items: 2
Items: 
Size: 765139 Color: 0
Size: 234803 Color: 1

Bin 1933: 59 of cap free
Amount of items: 2
Items: 
Size: 766462 Color: 1
Size: 233480 Color: 0

Bin 1934: 59 of cap free
Amount of items: 2
Items: 
Size: 788700 Color: 0
Size: 211242 Color: 1

Bin 1935: 60 of cap free
Amount of items: 2
Items: 
Size: 679464 Color: 0
Size: 320477 Color: 1

Bin 1936: 60 of cap free
Amount of items: 2
Items: 
Size: 500724 Color: 1
Size: 499217 Color: 0

Bin 1937: 60 of cap free
Amount of items: 2
Items: 
Size: 569351 Color: 0
Size: 430590 Color: 1

Bin 1938: 60 of cap free
Amount of items: 2
Items: 
Size: 669144 Color: 0
Size: 330797 Color: 1

Bin 1939: 60 of cap free
Amount of items: 2
Items: 
Size: 688450 Color: 1
Size: 311491 Color: 0

Bin 1940: 60 of cap free
Amount of items: 2
Items: 
Size: 691661 Color: 1
Size: 308280 Color: 0

Bin 1941: 60 of cap free
Amount of items: 2
Items: 
Size: 757736 Color: 0
Size: 242205 Color: 1

Bin 1942: 60 of cap free
Amount of items: 2
Items: 
Size: 797881 Color: 1
Size: 202060 Color: 0

Bin 1943: 61 of cap free
Amount of items: 3
Items: 
Size: 430868 Color: 1
Size: 427661 Color: 0
Size: 141411 Color: 0

Bin 1944: 61 of cap free
Amount of items: 2
Items: 
Size: 514079 Color: 0
Size: 485861 Color: 1

Bin 1945: 61 of cap free
Amount of items: 2
Items: 
Size: 539788 Color: 1
Size: 460152 Color: 0

Bin 1946: 61 of cap free
Amount of items: 2
Items: 
Size: 543078 Color: 1
Size: 456862 Color: 0

Bin 1947: 61 of cap free
Amount of items: 2
Items: 
Size: 558806 Color: 0
Size: 441134 Color: 1

Bin 1948: 61 of cap free
Amount of items: 2
Items: 
Size: 573847 Color: 0
Size: 426093 Color: 1

Bin 1949: 61 of cap free
Amount of items: 2
Items: 
Size: 587022 Color: 1
Size: 412918 Color: 0

Bin 1950: 61 of cap free
Amount of items: 2
Items: 
Size: 615053 Color: 1
Size: 384887 Color: 0

Bin 1951: 61 of cap free
Amount of items: 2
Items: 
Size: 624453 Color: 0
Size: 375487 Color: 1

Bin 1952: 61 of cap free
Amount of items: 2
Items: 
Size: 649524 Color: 1
Size: 350416 Color: 0

Bin 1953: 61 of cap free
Amount of items: 2
Items: 
Size: 668626 Color: 0
Size: 331314 Color: 1

Bin 1954: 61 of cap free
Amount of items: 2
Items: 
Size: 724362 Color: 1
Size: 275578 Color: 0

Bin 1955: 61 of cap free
Amount of items: 2
Items: 
Size: 725169 Color: 1
Size: 274771 Color: 0

Bin 1956: 61 of cap free
Amount of items: 2
Items: 
Size: 749377 Color: 0
Size: 250563 Color: 1

Bin 1957: 61 of cap free
Amount of items: 2
Items: 
Size: 768093 Color: 1
Size: 231847 Color: 0

Bin 1958: 62 of cap free
Amount of items: 5
Items: 
Size: 204741 Color: 0
Size: 201446 Color: 0
Size: 199477 Color: 1
Size: 197647 Color: 1
Size: 196628 Color: 1

Bin 1959: 62 of cap free
Amount of items: 2
Items: 
Size: 760899 Color: 1
Size: 239040 Color: 0

Bin 1960: 62 of cap free
Amount of items: 3
Items: 
Size: 603790 Color: 1
Size: 229708 Color: 0
Size: 166441 Color: 1

Bin 1961: 62 of cap free
Amount of items: 2
Items: 
Size: 674727 Color: 0
Size: 325212 Color: 1

Bin 1962: 62 of cap free
Amount of items: 2
Items: 
Size: 540530 Color: 1
Size: 459409 Color: 0

Bin 1963: 62 of cap free
Amount of items: 2
Items: 
Size: 554140 Color: 1
Size: 445799 Color: 0

Bin 1964: 62 of cap free
Amount of items: 2
Items: 
Size: 565134 Color: 0
Size: 434805 Color: 1

Bin 1965: 62 of cap free
Amount of items: 2
Items: 
Size: 618503 Color: 1
Size: 381436 Color: 0

Bin 1966: 62 of cap free
Amount of items: 2
Items: 
Size: 691078 Color: 0
Size: 308861 Color: 1

Bin 1967: 62 of cap free
Amount of items: 2
Items: 
Size: 736975 Color: 1
Size: 262964 Color: 0

Bin 1968: 62 of cap free
Amount of items: 2
Items: 
Size: 738652 Color: 1
Size: 261287 Color: 0

Bin 1969: 62 of cap free
Amount of items: 2
Items: 
Size: 740395 Color: 0
Size: 259544 Color: 1

Bin 1970: 63 of cap free
Amount of items: 2
Items: 
Size: 545870 Color: 0
Size: 454068 Color: 1

Bin 1971: 63 of cap free
Amount of items: 2
Items: 
Size: 796257 Color: 1
Size: 203681 Color: 0

Bin 1972: 63 of cap free
Amount of items: 2
Items: 
Size: 539700 Color: 0
Size: 460238 Color: 1

Bin 1973: 63 of cap free
Amount of items: 2
Items: 
Size: 551679 Color: 1
Size: 448259 Color: 0

Bin 1974: 63 of cap free
Amount of items: 2
Items: 
Size: 555836 Color: 1
Size: 444102 Color: 0

Bin 1975: 63 of cap free
Amount of items: 2
Items: 
Size: 562849 Color: 1
Size: 437089 Color: 0

Bin 1976: 63 of cap free
Amount of items: 2
Items: 
Size: 575688 Color: 0
Size: 424250 Color: 1

Bin 1977: 63 of cap free
Amount of items: 2
Items: 
Size: 581022 Color: 1
Size: 418916 Color: 0

Bin 1978: 63 of cap free
Amount of items: 2
Items: 
Size: 626608 Color: 0
Size: 373330 Color: 1

Bin 1979: 63 of cap free
Amount of items: 2
Items: 
Size: 635627 Color: 0
Size: 364311 Color: 1

Bin 1980: 63 of cap free
Amount of items: 2
Items: 
Size: 637555 Color: 1
Size: 362383 Color: 0

Bin 1981: 63 of cap free
Amount of items: 2
Items: 
Size: 658294 Color: 1
Size: 341644 Color: 0

Bin 1982: 63 of cap free
Amount of items: 2
Items: 
Size: 681128 Color: 1
Size: 318810 Color: 0

Bin 1983: 63 of cap free
Amount of items: 2
Items: 
Size: 712200 Color: 0
Size: 287738 Color: 1

Bin 1984: 63 of cap free
Amount of items: 2
Items: 
Size: 743079 Color: 0
Size: 256859 Color: 1

Bin 1985: 63 of cap free
Amount of items: 2
Items: 
Size: 743295 Color: 0
Size: 256643 Color: 1

Bin 1986: 63 of cap free
Amount of items: 2
Items: 
Size: 770180 Color: 0
Size: 229758 Color: 1

Bin 1987: 63 of cap free
Amount of items: 2
Items: 
Size: 796821 Color: 0
Size: 203117 Color: 1

Bin 1988: 64 of cap free
Amount of items: 5
Items: 
Size: 244943 Color: 1
Size: 191153 Color: 0
Size: 191147 Color: 0
Size: 186769 Color: 1
Size: 185925 Color: 1

Bin 1989: 64 of cap free
Amount of items: 2
Items: 
Size: 500825 Color: 1
Size: 499112 Color: 0

Bin 1990: 64 of cap free
Amount of items: 2
Items: 
Size: 501244 Color: 1
Size: 498693 Color: 0

Bin 1991: 64 of cap free
Amount of items: 2
Items: 
Size: 533610 Color: 1
Size: 466327 Color: 0

Bin 1992: 64 of cap free
Amount of items: 2
Items: 
Size: 536634 Color: 1
Size: 463303 Color: 0

Bin 1993: 64 of cap free
Amount of items: 2
Items: 
Size: 548648 Color: 1
Size: 451289 Color: 0

Bin 1994: 64 of cap free
Amount of items: 2
Items: 
Size: 573219 Color: 0
Size: 426718 Color: 1

Bin 1995: 64 of cap free
Amount of items: 2
Items: 
Size: 589622 Color: 1
Size: 410315 Color: 0

Bin 1996: 64 of cap free
Amount of items: 2
Items: 
Size: 592807 Color: 1
Size: 407130 Color: 0

Bin 1997: 64 of cap free
Amount of items: 2
Items: 
Size: 594600 Color: 0
Size: 405337 Color: 1

Bin 1998: 64 of cap free
Amount of items: 2
Items: 
Size: 602510 Color: 1
Size: 397427 Color: 0

Bin 1999: 64 of cap free
Amount of items: 2
Items: 
Size: 645169 Color: 0
Size: 354768 Color: 1

Bin 2000: 64 of cap free
Amount of items: 2
Items: 
Size: 646286 Color: 0
Size: 353651 Color: 1

Bin 2001: 64 of cap free
Amount of items: 2
Items: 
Size: 722498 Color: 0
Size: 277439 Color: 1

Bin 2002: 64 of cap free
Amount of items: 2
Items: 
Size: 723928 Color: 1
Size: 276009 Color: 0

Bin 2003: 64 of cap free
Amount of items: 2
Items: 
Size: 733519 Color: 0
Size: 266418 Color: 1

Bin 2004: 64 of cap free
Amount of items: 2
Items: 
Size: 750146 Color: 0
Size: 249791 Color: 1

Bin 2005: 64 of cap free
Amount of items: 2
Items: 
Size: 786159 Color: 0
Size: 213778 Color: 1

Bin 2006: 65 of cap free
Amount of items: 2
Items: 
Size: 796887 Color: 1
Size: 203049 Color: 0

Bin 2007: 65 of cap free
Amount of items: 2
Items: 
Size: 603983 Color: 1
Size: 395953 Color: 0

Bin 2008: 65 of cap free
Amount of items: 2
Items: 
Size: 743474 Color: 0
Size: 256462 Color: 1

Bin 2009: 65 of cap free
Amount of items: 2
Items: 
Size: 785427 Color: 0
Size: 214509 Color: 1

Bin 2010: 65 of cap free
Amount of items: 2
Items: 
Size: 518552 Color: 1
Size: 481384 Color: 0

Bin 2011: 65 of cap free
Amount of items: 2
Items: 
Size: 529225 Color: 1
Size: 470711 Color: 0

Bin 2012: 65 of cap free
Amount of items: 2
Items: 
Size: 535481 Color: 1
Size: 464455 Color: 0

Bin 2013: 65 of cap free
Amount of items: 2
Items: 
Size: 619060 Color: 0
Size: 380876 Color: 1

Bin 2014: 65 of cap free
Amount of items: 2
Items: 
Size: 655036 Color: 1
Size: 344900 Color: 0

Bin 2015: 65 of cap free
Amount of items: 2
Items: 
Size: 721488 Color: 1
Size: 278448 Color: 0

Bin 2016: 65 of cap free
Amount of items: 2
Items: 
Size: 741794 Color: 0
Size: 258142 Color: 1

Bin 2017: 65 of cap free
Amount of items: 2
Items: 
Size: 752230 Color: 1
Size: 247706 Color: 0

Bin 2018: 65 of cap free
Amount of items: 2
Items: 
Size: 754308 Color: 0
Size: 245628 Color: 1

Bin 2019: 66 of cap free
Amount of items: 2
Items: 
Size: 525456 Color: 0
Size: 474479 Color: 1

Bin 2020: 66 of cap free
Amount of items: 2
Items: 
Size: 623994 Color: 0
Size: 375941 Color: 1

Bin 2021: 66 of cap free
Amount of items: 2
Items: 
Size: 539043 Color: 1
Size: 460892 Color: 0

Bin 2022: 66 of cap free
Amount of items: 2
Items: 
Size: 576454 Color: 0
Size: 423481 Color: 1

Bin 2023: 66 of cap free
Amount of items: 2
Items: 
Size: 594580 Color: 1
Size: 405355 Color: 0

Bin 2024: 66 of cap free
Amount of items: 2
Items: 
Size: 595967 Color: 0
Size: 403968 Color: 1

Bin 2025: 66 of cap free
Amount of items: 2
Items: 
Size: 605573 Color: 0
Size: 394362 Color: 1

Bin 2026: 66 of cap free
Amount of items: 2
Items: 
Size: 625251 Color: 1
Size: 374684 Color: 0

Bin 2027: 66 of cap free
Amount of items: 2
Items: 
Size: 647725 Color: 1
Size: 352210 Color: 0

Bin 2028: 66 of cap free
Amount of items: 2
Items: 
Size: 658717 Color: 0
Size: 341218 Color: 1

Bin 2029: 67 of cap free
Amount of items: 2
Items: 
Size: 754414 Color: 0
Size: 245520 Color: 1

Bin 2030: 67 of cap free
Amount of items: 2
Items: 
Size: 515501 Color: 1
Size: 484433 Color: 0

Bin 2031: 67 of cap free
Amount of items: 2
Items: 
Size: 527511 Color: 1
Size: 472423 Color: 0

Bin 2032: 67 of cap free
Amount of items: 2
Items: 
Size: 530275 Color: 0
Size: 469659 Color: 1

Bin 2033: 67 of cap free
Amount of items: 2
Items: 
Size: 564526 Color: 1
Size: 435408 Color: 0

Bin 2034: 67 of cap free
Amount of items: 2
Items: 
Size: 565960 Color: 0
Size: 433974 Color: 1

Bin 2035: 67 of cap free
Amount of items: 2
Items: 
Size: 617052 Color: 0
Size: 382882 Color: 1

Bin 2036: 67 of cap free
Amount of items: 2
Items: 
Size: 642675 Color: 1
Size: 357259 Color: 0

Bin 2037: 67 of cap free
Amount of items: 2
Items: 
Size: 651023 Color: 0
Size: 348911 Color: 1

Bin 2038: 67 of cap free
Amount of items: 2
Items: 
Size: 679942 Color: 0
Size: 319992 Color: 1

Bin 2039: 67 of cap free
Amount of items: 2
Items: 
Size: 693109 Color: 1
Size: 306825 Color: 0

Bin 2040: 67 of cap free
Amount of items: 2
Items: 
Size: 698474 Color: 0
Size: 301460 Color: 1

Bin 2041: 67 of cap free
Amount of items: 2
Items: 
Size: 737977 Color: 1
Size: 261957 Color: 0

Bin 2042: 67 of cap free
Amount of items: 2
Items: 
Size: 765932 Color: 0
Size: 234002 Color: 1

Bin 2043: 68 of cap free
Amount of items: 2
Items: 
Size: 532369 Color: 1
Size: 467564 Color: 0

Bin 2044: 68 of cap free
Amount of items: 2
Items: 
Size: 572815 Color: 1
Size: 427118 Color: 0

Bin 2045: 68 of cap free
Amount of items: 2
Items: 
Size: 573434 Color: 1
Size: 426499 Color: 0

Bin 2046: 68 of cap free
Amount of items: 2
Items: 
Size: 605335 Color: 1
Size: 394598 Color: 0

Bin 2047: 68 of cap free
Amount of items: 2
Items: 
Size: 630954 Color: 0
Size: 368979 Color: 1

Bin 2048: 68 of cap free
Amount of items: 2
Items: 
Size: 637776 Color: 0
Size: 362157 Color: 1

Bin 2049: 68 of cap free
Amount of items: 2
Items: 
Size: 704774 Color: 0
Size: 295159 Color: 1

Bin 2050: 68 of cap free
Amount of items: 2
Items: 
Size: 739825 Color: 0
Size: 260108 Color: 1

Bin 2051: 68 of cap free
Amount of items: 2
Items: 
Size: 764828 Color: 1
Size: 235105 Color: 0

Bin 2052: 68 of cap free
Amount of items: 2
Items: 
Size: 770661 Color: 1
Size: 229272 Color: 0

Bin 2053: 68 of cap free
Amount of items: 2
Items: 
Size: 799878 Color: 0
Size: 200055 Color: 1

Bin 2054: 69 of cap free
Amount of items: 3
Items: 
Size: 362747 Color: 0
Size: 343705 Color: 0
Size: 293480 Color: 1

Bin 2055: 69 of cap free
Amount of items: 2
Items: 
Size: 669140 Color: 1
Size: 330792 Color: 0

Bin 2056: 69 of cap free
Amount of items: 2
Items: 
Size: 792340 Color: 1
Size: 207592 Color: 0

Bin 2057: 69 of cap free
Amount of items: 2
Items: 
Size: 788957 Color: 0
Size: 210975 Color: 1

Bin 2058: 69 of cap free
Amount of items: 2
Items: 
Size: 506974 Color: 0
Size: 492958 Color: 1

Bin 2059: 69 of cap free
Amount of items: 2
Items: 
Size: 508864 Color: 1
Size: 491068 Color: 0

Bin 2060: 69 of cap free
Amount of items: 2
Items: 
Size: 537922 Color: 1
Size: 462010 Color: 0

Bin 2061: 69 of cap free
Amount of items: 2
Items: 
Size: 573559 Color: 1
Size: 426373 Color: 0

Bin 2062: 69 of cap free
Amount of items: 2
Items: 
Size: 639483 Color: 1
Size: 360449 Color: 0

Bin 2063: 69 of cap free
Amount of items: 2
Items: 
Size: 658913 Color: 0
Size: 341019 Color: 1

Bin 2064: 69 of cap free
Amount of items: 2
Items: 
Size: 660339 Color: 0
Size: 339593 Color: 1

Bin 2065: 69 of cap free
Amount of items: 2
Items: 
Size: 696721 Color: 0
Size: 303211 Color: 1

Bin 2066: 69 of cap free
Amount of items: 2
Items: 
Size: 709918 Color: 0
Size: 290014 Color: 1

Bin 2067: 69 of cap free
Amount of items: 2
Items: 
Size: 712450 Color: 0
Size: 287482 Color: 1

Bin 2068: 69 of cap free
Amount of items: 2
Items: 
Size: 772010 Color: 0
Size: 227922 Color: 1

Bin 2069: 69 of cap free
Amount of items: 2
Items: 
Size: 777501 Color: 0
Size: 222431 Color: 1

Bin 2070: 70 of cap free
Amount of items: 2
Items: 
Size: 758578 Color: 0
Size: 241353 Color: 1

Bin 2071: 70 of cap free
Amount of items: 2
Items: 
Size: 785598 Color: 1
Size: 214333 Color: 0

Bin 2072: 70 of cap free
Amount of items: 2
Items: 
Size: 550398 Color: 1
Size: 449533 Color: 0

Bin 2073: 70 of cap free
Amount of items: 2
Items: 
Size: 570856 Color: 0
Size: 429075 Color: 1

Bin 2074: 70 of cap free
Amount of items: 2
Items: 
Size: 571725 Color: 0
Size: 428206 Color: 1

Bin 2075: 70 of cap free
Amount of items: 2
Items: 
Size: 592723 Color: 0
Size: 407208 Color: 1

Bin 2076: 70 of cap free
Amount of items: 2
Items: 
Size: 594256 Color: 1
Size: 405675 Color: 0

Bin 2077: 70 of cap free
Amount of items: 2
Items: 
Size: 619778 Color: 1
Size: 380153 Color: 0

Bin 2078: 70 of cap free
Amount of items: 2
Items: 
Size: 633132 Color: 0
Size: 366799 Color: 1

Bin 2079: 70 of cap free
Amount of items: 2
Items: 
Size: 645827 Color: 1
Size: 354104 Color: 0

Bin 2080: 70 of cap free
Amount of items: 2
Items: 
Size: 653508 Color: 0
Size: 346423 Color: 1

Bin 2081: 70 of cap free
Amount of items: 2
Items: 
Size: 688310 Color: 1
Size: 311621 Color: 0

Bin 2082: 70 of cap free
Amount of items: 2
Items: 
Size: 748824 Color: 0
Size: 251107 Color: 1

Bin 2083: 71 of cap free
Amount of items: 2
Items: 
Size: 660864 Color: 1
Size: 339066 Color: 0

Bin 2084: 71 of cap free
Amount of items: 2
Items: 
Size: 503290 Color: 0
Size: 496640 Color: 1

Bin 2085: 71 of cap free
Amount of items: 2
Items: 
Size: 525599 Color: 0
Size: 474331 Color: 1

Bin 2086: 71 of cap free
Amount of items: 2
Items: 
Size: 534858 Color: 1
Size: 465072 Color: 0

Bin 2087: 71 of cap free
Amount of items: 2
Items: 
Size: 539945 Color: 0
Size: 459985 Color: 1

Bin 2088: 71 of cap free
Amount of items: 2
Items: 
Size: 550990 Color: 1
Size: 448940 Color: 0

Bin 2089: 71 of cap free
Amount of items: 2
Items: 
Size: 554346 Color: 1
Size: 445584 Color: 0

Bin 2090: 71 of cap free
Amount of items: 2
Items: 
Size: 563050 Color: 1
Size: 436880 Color: 0

Bin 2091: 71 of cap free
Amount of items: 2
Items: 
Size: 574426 Color: 1
Size: 425504 Color: 0

Bin 2092: 71 of cap free
Amount of items: 2
Items: 
Size: 581589 Color: 1
Size: 418341 Color: 0

Bin 2093: 71 of cap free
Amount of items: 2
Items: 
Size: 609317 Color: 0
Size: 390613 Color: 1

Bin 2094: 71 of cap free
Amount of items: 2
Items: 
Size: 610004 Color: 0
Size: 389926 Color: 1

Bin 2095: 71 of cap free
Amount of items: 2
Items: 
Size: 667464 Color: 1
Size: 332466 Color: 0

Bin 2096: 71 of cap free
Amount of items: 2
Items: 
Size: 714918 Color: 1
Size: 285012 Color: 0

Bin 2097: 71 of cap free
Amount of items: 2
Items: 
Size: 724829 Color: 0
Size: 275101 Color: 1

Bin 2098: 71 of cap free
Amount of items: 2
Items: 
Size: 769892 Color: 1
Size: 230038 Color: 0

Bin 2099: 71 of cap free
Amount of items: 2
Items: 
Size: 770814 Color: 1
Size: 229116 Color: 0

Bin 2100: 71 of cap free
Amount of items: 2
Items: 
Size: 777954 Color: 0
Size: 221976 Color: 1

Bin 2101: 72 of cap free
Amount of items: 2
Items: 
Size: 591190 Color: 0
Size: 408739 Color: 1

Bin 2102: 72 of cap free
Amount of items: 2
Items: 
Size: 502731 Color: 1
Size: 497198 Color: 0

Bin 2103: 72 of cap free
Amount of items: 2
Items: 
Size: 509667 Color: 0
Size: 490262 Color: 1

Bin 2104: 72 of cap free
Amount of items: 2
Items: 
Size: 523167 Color: 0
Size: 476762 Color: 1

Bin 2105: 72 of cap free
Amount of items: 2
Items: 
Size: 566130 Color: 0
Size: 433799 Color: 1

Bin 2106: 72 of cap free
Amount of items: 2
Items: 
Size: 567233 Color: 1
Size: 432696 Color: 0

Bin 2107: 72 of cap free
Amount of items: 2
Items: 
Size: 568671 Color: 1
Size: 431258 Color: 0

Bin 2108: 72 of cap free
Amount of items: 2
Items: 
Size: 605051 Color: 0
Size: 394878 Color: 1

Bin 2109: 72 of cap free
Amount of items: 2
Items: 
Size: 605283 Color: 0
Size: 394646 Color: 1

Bin 2110: 72 of cap free
Amount of items: 2
Items: 
Size: 658791 Color: 1
Size: 341138 Color: 0

Bin 2111: 72 of cap free
Amount of items: 2
Items: 
Size: 671094 Color: 1
Size: 328835 Color: 0

Bin 2112: 72 of cap free
Amount of items: 2
Items: 
Size: 685594 Color: 1
Size: 314335 Color: 0

Bin 2113: 72 of cap free
Amount of items: 2
Items: 
Size: 695514 Color: 0
Size: 304415 Color: 1

Bin 2114: 72 of cap free
Amount of items: 2
Items: 
Size: 720848 Color: 1
Size: 279081 Color: 0

Bin 2115: 72 of cap free
Amount of items: 2
Items: 
Size: 757135 Color: 1
Size: 242794 Color: 0

Bin 2116: 72 of cap free
Amount of items: 2
Items: 
Size: 760685 Color: 1
Size: 239244 Color: 0

Bin 2117: 72 of cap free
Amount of items: 2
Items: 
Size: 793854 Color: 0
Size: 206075 Color: 1

Bin 2118: 73 of cap free
Amount of items: 2
Items: 
Size: 700787 Color: 0
Size: 299141 Color: 1

Bin 2119: 73 of cap free
Amount of items: 2
Items: 
Size: 505394 Color: 0
Size: 494534 Color: 1

Bin 2120: 73 of cap free
Amount of items: 2
Items: 
Size: 534496 Color: 1
Size: 465432 Color: 0

Bin 2121: 73 of cap free
Amount of items: 2
Items: 
Size: 613872 Color: 0
Size: 386056 Color: 1

Bin 2122: 73 of cap free
Amount of items: 2
Items: 
Size: 665734 Color: 0
Size: 334194 Color: 1

Bin 2123: 73 of cap free
Amount of items: 2
Items: 
Size: 674913 Color: 0
Size: 325015 Color: 1

Bin 2124: 73 of cap free
Amount of items: 2
Items: 
Size: 726039 Color: 0
Size: 273889 Color: 1

Bin 2125: 73 of cap free
Amount of items: 2
Items: 
Size: 746223 Color: 1
Size: 253705 Color: 0

Bin 2126: 73 of cap free
Amount of items: 2
Items: 
Size: 754922 Color: 1
Size: 245006 Color: 0

Bin 2127: 73 of cap free
Amount of items: 2
Items: 
Size: 799154 Color: 0
Size: 200774 Color: 1

Bin 2128: 74 of cap free
Amount of items: 2
Items: 
Size: 593402 Color: 1
Size: 406525 Color: 0

Bin 2129: 74 of cap free
Amount of items: 2
Items: 
Size: 509480 Color: 0
Size: 490447 Color: 1

Bin 2130: 74 of cap free
Amount of items: 2
Items: 
Size: 569679 Color: 1
Size: 430248 Color: 0

Bin 2131: 74 of cap free
Amount of items: 2
Items: 
Size: 582327 Color: 1
Size: 417600 Color: 0

Bin 2132: 74 of cap free
Amount of items: 2
Items: 
Size: 619688 Color: 0
Size: 380239 Color: 1

Bin 2133: 74 of cap free
Amount of items: 2
Items: 
Size: 643389 Color: 1
Size: 356538 Color: 0

Bin 2134: 74 of cap free
Amount of items: 2
Items: 
Size: 645520 Color: 1
Size: 354407 Color: 0

Bin 2135: 74 of cap free
Amount of items: 2
Items: 
Size: 660123 Color: 1
Size: 339804 Color: 0

Bin 2136: 74 of cap free
Amount of items: 2
Items: 
Size: 713819 Color: 1
Size: 286108 Color: 0

Bin 2137: 74 of cap free
Amount of items: 2
Items: 
Size: 735153 Color: 0
Size: 264774 Color: 1

Bin 2138: 74 of cap free
Amount of items: 2
Items: 
Size: 737812 Color: 1
Size: 262115 Color: 0

Bin 2139: 74 of cap free
Amount of items: 2
Items: 
Size: 750625 Color: 1
Size: 249302 Color: 0

Bin 2140: 74 of cap free
Amount of items: 2
Items: 
Size: 761927 Color: 0
Size: 238000 Color: 1

Bin 2141: 74 of cap free
Amount of items: 2
Items: 
Size: 773390 Color: 1
Size: 226537 Color: 0

Bin 2142: 75 of cap free
Amount of items: 2
Items: 
Size: 687676 Color: 1
Size: 312250 Color: 0

Bin 2143: 75 of cap free
Amount of items: 2
Items: 
Size: 597449 Color: 1
Size: 402477 Color: 0

Bin 2144: 75 of cap free
Amount of items: 2
Items: 
Size: 506676 Color: 0
Size: 493250 Color: 1

Bin 2145: 75 of cap free
Amount of items: 2
Items: 
Size: 548912 Color: 0
Size: 451014 Color: 1

Bin 2146: 75 of cap free
Amount of items: 2
Items: 
Size: 608835 Color: 1
Size: 391091 Color: 0

Bin 2147: 75 of cap free
Amount of items: 2
Items: 
Size: 652788 Color: 0
Size: 347138 Color: 1

Bin 2148: 75 of cap free
Amount of items: 2
Items: 
Size: 656300 Color: 0
Size: 343626 Color: 1

Bin 2149: 75 of cap free
Amount of items: 2
Items: 
Size: 715057 Color: 1
Size: 284869 Color: 0

Bin 2150: 75 of cap free
Amount of items: 2
Items: 
Size: 723069 Color: 1
Size: 276857 Color: 0

Bin 2151: 75 of cap free
Amount of items: 2
Items: 
Size: 726572 Color: 0
Size: 273354 Color: 1

Bin 2152: 75 of cap free
Amount of items: 2
Items: 
Size: 729239 Color: 1
Size: 270687 Color: 0

Bin 2153: 75 of cap free
Amount of items: 2
Items: 
Size: 735746 Color: 0
Size: 264180 Color: 1

Bin 2154: 75 of cap free
Amount of items: 2
Items: 
Size: 740969 Color: 1
Size: 258957 Color: 0

Bin 2155: 76 of cap free
Amount of items: 2
Items: 
Size: 522339 Color: 1
Size: 477586 Color: 0

Bin 2156: 76 of cap free
Amount of items: 2
Items: 
Size: 530134 Color: 0
Size: 469791 Color: 1

Bin 2157: 76 of cap free
Amount of items: 2
Items: 
Size: 535686 Color: 0
Size: 464239 Color: 1

Bin 2158: 76 of cap free
Amount of items: 2
Items: 
Size: 548307 Color: 1
Size: 451618 Color: 0

Bin 2159: 76 of cap free
Amount of items: 2
Items: 
Size: 560010 Color: 0
Size: 439915 Color: 1

Bin 2160: 76 of cap free
Amount of items: 2
Items: 
Size: 647606 Color: 1
Size: 352319 Color: 0

Bin 2161: 76 of cap free
Amount of items: 2
Items: 
Size: 698824 Color: 0
Size: 301101 Color: 1

Bin 2162: 76 of cap free
Amount of items: 2
Items: 
Size: 700124 Color: 0
Size: 299801 Color: 1

Bin 2163: 76 of cap free
Amount of items: 2
Items: 
Size: 737264 Color: 0
Size: 262661 Color: 1

Bin 2164: 76 of cap free
Amount of items: 2
Items: 
Size: 785064 Color: 1
Size: 214861 Color: 0

Bin 2165: 77 of cap free
Amount of items: 2
Items: 
Size: 512957 Color: 1
Size: 486967 Color: 0

Bin 2166: 77 of cap free
Amount of items: 2
Items: 
Size: 606748 Color: 1
Size: 393176 Color: 0

Bin 2167: 77 of cap free
Amount of items: 2
Items: 
Size: 622728 Color: 0
Size: 377196 Color: 1

Bin 2168: 77 of cap free
Amount of items: 2
Items: 
Size: 654672 Color: 1
Size: 345252 Color: 0

Bin 2169: 77 of cap free
Amount of items: 2
Items: 
Size: 727428 Color: 1
Size: 272496 Color: 0

Bin 2170: 77 of cap free
Amount of items: 2
Items: 
Size: 753557 Color: 1
Size: 246367 Color: 0

Bin 2171: 77 of cap free
Amount of items: 2
Items: 
Size: 773740 Color: 1
Size: 226184 Color: 0

Bin 2172: 77 of cap free
Amount of items: 2
Items: 
Size: 778935 Color: 0
Size: 220989 Color: 1

Bin 2173: 78 of cap free
Amount of items: 4
Items: 
Size: 350421 Color: 0
Size: 234444 Color: 1
Size: 231727 Color: 1
Size: 183331 Color: 0

Bin 2174: 78 of cap free
Amount of items: 3
Items: 
Size: 709340 Color: 1
Size: 178585 Color: 1
Size: 111998 Color: 0

Bin 2175: 78 of cap free
Amount of items: 2
Items: 
Size: 586430 Color: 1
Size: 413493 Color: 0

Bin 2176: 78 of cap free
Amount of items: 2
Items: 
Size: 592713 Color: 1
Size: 407210 Color: 0

Bin 2177: 78 of cap free
Amount of items: 2
Items: 
Size: 618492 Color: 1
Size: 381431 Color: 0

Bin 2178: 78 of cap free
Amount of items: 2
Items: 
Size: 645281 Color: 0
Size: 354642 Color: 1

Bin 2179: 78 of cap free
Amount of items: 2
Items: 
Size: 698180 Color: 0
Size: 301743 Color: 1

Bin 2180: 78 of cap free
Amount of items: 2
Items: 
Size: 714513 Color: 0
Size: 285410 Color: 1

Bin 2181: 79 of cap free
Amount of items: 3
Items: 
Size: 681644 Color: 1
Size: 179657 Color: 0
Size: 138621 Color: 0

Bin 2182: 79 of cap free
Amount of items: 3
Items: 
Size: 565890 Color: 1
Size: 293117 Color: 0
Size: 140915 Color: 1

Bin 2183: 79 of cap free
Amount of items: 2
Items: 
Size: 757945 Color: 1
Size: 241977 Color: 0

Bin 2184: 79 of cap free
Amount of items: 2
Items: 
Size: 534414 Color: 1
Size: 465508 Color: 0

Bin 2185: 79 of cap free
Amount of items: 2
Items: 
Size: 550300 Color: 1
Size: 449622 Color: 0

Bin 2186: 79 of cap free
Amount of items: 2
Items: 
Size: 669361 Color: 0
Size: 330561 Color: 1

Bin 2187: 79 of cap free
Amount of items: 2
Items: 
Size: 681927 Color: 0
Size: 317995 Color: 1

Bin 2188: 79 of cap free
Amount of items: 2
Items: 
Size: 689925 Color: 1
Size: 309997 Color: 0

Bin 2189: 80 of cap free
Amount of items: 3
Items: 
Size: 470479 Color: 1
Size: 395589 Color: 0
Size: 133853 Color: 0

Bin 2190: 80 of cap free
Amount of items: 3
Items: 
Size: 724032 Color: 0
Size: 140902 Color: 1
Size: 134987 Color: 1

Bin 2191: 80 of cap free
Amount of items: 3
Items: 
Size: 360930 Color: 1
Size: 319597 Color: 0
Size: 319394 Color: 0

Bin 2192: 80 of cap free
Amount of items: 2
Items: 
Size: 516666 Color: 1
Size: 483255 Color: 0

Bin 2193: 80 of cap free
Amount of items: 2
Items: 
Size: 516583 Color: 0
Size: 483338 Color: 1

Bin 2194: 80 of cap free
Amount of items: 2
Items: 
Size: 519175 Color: 0
Size: 480746 Color: 1

Bin 2195: 80 of cap free
Amount of items: 2
Items: 
Size: 541183 Color: 0
Size: 458738 Color: 1

Bin 2196: 80 of cap free
Amount of items: 2
Items: 
Size: 544813 Color: 0
Size: 455108 Color: 1

Bin 2197: 80 of cap free
Amount of items: 2
Items: 
Size: 563382 Color: 1
Size: 436539 Color: 0

Bin 2198: 80 of cap free
Amount of items: 2
Items: 
Size: 590811 Color: 1
Size: 409110 Color: 0

Bin 2199: 80 of cap free
Amount of items: 2
Items: 
Size: 620116 Color: 1
Size: 379805 Color: 0

Bin 2200: 80 of cap free
Amount of items: 2
Items: 
Size: 679349 Color: 0
Size: 320572 Color: 1

Bin 2201: 80 of cap free
Amount of items: 2
Items: 
Size: 745539 Color: 1
Size: 254382 Color: 0

Bin 2202: 80 of cap free
Amount of items: 2
Items: 
Size: 778332 Color: 1
Size: 221589 Color: 0

Bin 2203: 80 of cap free
Amount of items: 2
Items: 
Size: 779778 Color: 1
Size: 220143 Color: 0

Bin 2204: 80 of cap free
Amount of items: 2
Items: 
Size: 786509 Color: 1
Size: 213412 Color: 0

Bin 2205: 81 of cap free
Amount of items: 2
Items: 
Size: 562144 Color: 0
Size: 437776 Color: 1

Bin 2206: 81 of cap free
Amount of items: 2
Items: 
Size: 704459 Color: 1
Size: 295461 Color: 0

Bin 2207: 81 of cap free
Amount of items: 2
Items: 
Size: 625280 Color: 0
Size: 374640 Color: 1

Bin 2208: 81 of cap free
Amount of items: 2
Items: 
Size: 630713 Color: 1
Size: 369207 Color: 0

Bin 2209: 81 of cap free
Amount of items: 2
Items: 
Size: 655036 Color: 0
Size: 344884 Color: 1

Bin 2210: 81 of cap free
Amount of items: 2
Items: 
Size: 718429 Color: 1
Size: 281491 Color: 0

Bin 2211: 81 of cap free
Amount of items: 2
Items: 
Size: 733700 Color: 1
Size: 266220 Color: 0

Bin 2212: 81 of cap free
Amount of items: 2
Items: 
Size: 757546 Color: 0
Size: 242374 Color: 1

Bin 2213: 82 of cap free
Amount of items: 2
Items: 
Size: 767182 Color: 0
Size: 232737 Color: 1

Bin 2214: 82 of cap free
Amount of items: 2
Items: 
Size: 563212 Color: 1
Size: 436707 Color: 0

Bin 2215: 82 of cap free
Amount of items: 2
Items: 
Size: 554897 Color: 0
Size: 445022 Color: 1

Bin 2216: 82 of cap free
Amount of items: 2
Items: 
Size: 562404 Color: 1
Size: 437515 Color: 0

Bin 2217: 82 of cap free
Amount of items: 2
Items: 
Size: 566297 Color: 0
Size: 433622 Color: 1

Bin 2218: 82 of cap free
Amount of items: 2
Items: 
Size: 574897 Color: 1
Size: 425022 Color: 0

Bin 2219: 82 of cap free
Amount of items: 2
Items: 
Size: 607968 Color: 1
Size: 391951 Color: 0

Bin 2220: 82 of cap free
Amount of items: 2
Items: 
Size: 609364 Color: 1
Size: 390555 Color: 0

Bin 2221: 82 of cap free
Amount of items: 2
Items: 
Size: 627406 Color: 1
Size: 372513 Color: 0

Bin 2222: 82 of cap free
Amount of items: 2
Items: 
Size: 627956 Color: 0
Size: 371963 Color: 1

Bin 2223: 82 of cap free
Amount of items: 2
Items: 
Size: 651230 Color: 0
Size: 348689 Color: 1

Bin 2224: 82 of cap free
Amount of items: 2
Items: 
Size: 653817 Color: 1
Size: 346102 Color: 0

Bin 2225: 82 of cap free
Amount of items: 2
Items: 
Size: 690514 Color: 0
Size: 309405 Color: 1

Bin 2226: 82 of cap free
Amount of items: 2
Items: 
Size: 704994 Color: 0
Size: 294925 Color: 1

Bin 2227: 82 of cap free
Amount of items: 2
Items: 
Size: 735197 Color: 1
Size: 264722 Color: 0

Bin 2228: 82 of cap free
Amount of items: 2
Items: 
Size: 775114 Color: 1
Size: 224805 Color: 0

Bin 2229: 83 of cap free
Amount of items: 2
Items: 
Size: 722141 Color: 0
Size: 277777 Color: 1

Bin 2230: 83 of cap free
Amount of items: 2
Items: 
Size: 511493 Color: 0
Size: 488425 Color: 1

Bin 2231: 83 of cap free
Amount of items: 2
Items: 
Size: 539224 Color: 0
Size: 460694 Color: 1

Bin 2232: 83 of cap free
Amount of items: 2
Items: 
Size: 579444 Color: 0
Size: 420474 Color: 1

Bin 2233: 83 of cap free
Amount of items: 2
Items: 
Size: 598990 Color: 1
Size: 400928 Color: 0

Bin 2234: 83 of cap free
Amount of items: 2
Items: 
Size: 611892 Color: 0
Size: 388026 Color: 1

Bin 2235: 83 of cap free
Amount of items: 2
Items: 
Size: 633473 Color: 0
Size: 366445 Color: 1

Bin 2236: 83 of cap free
Amount of items: 2
Items: 
Size: 659004 Color: 0
Size: 340914 Color: 1

Bin 2237: 83 of cap free
Amount of items: 2
Items: 
Size: 672155 Color: 1
Size: 327763 Color: 0

Bin 2238: 83 of cap free
Amount of items: 2
Items: 
Size: 682026 Color: 1
Size: 317892 Color: 0

Bin 2239: 83 of cap free
Amount of items: 2
Items: 
Size: 725654 Color: 0
Size: 274264 Color: 1

Bin 2240: 83 of cap free
Amount of items: 2
Items: 
Size: 783956 Color: 1
Size: 215962 Color: 0

Bin 2241: 84 of cap free
Amount of items: 2
Items: 
Size: 735469 Color: 0
Size: 264448 Color: 1

Bin 2242: 84 of cap free
Amount of items: 2
Items: 
Size: 732159 Color: 0
Size: 267758 Color: 1

Bin 2243: 84 of cap free
Amount of items: 2
Items: 
Size: 558407 Color: 1
Size: 441510 Color: 0

Bin 2244: 84 of cap free
Amount of items: 2
Items: 
Size: 585041 Color: 0
Size: 414876 Color: 1

Bin 2245: 84 of cap free
Amount of items: 2
Items: 
Size: 621205 Color: 0
Size: 378712 Color: 1

Bin 2246: 84 of cap free
Amount of items: 2
Items: 
Size: 642482 Color: 0
Size: 357435 Color: 1

Bin 2247: 84 of cap free
Amount of items: 2
Items: 
Size: 695741 Color: 0
Size: 304176 Color: 1

Bin 2248: 84 of cap free
Amount of items: 2
Items: 
Size: 715323 Color: 1
Size: 284594 Color: 0

Bin 2249: 84 of cap free
Amount of items: 2
Items: 
Size: 733746 Color: 0
Size: 266171 Color: 1

Bin 2250: 84 of cap free
Amount of items: 2
Items: 
Size: 737262 Color: 1
Size: 262655 Color: 0

Bin 2251: 84 of cap free
Amount of items: 2
Items: 
Size: 737777 Color: 0
Size: 262140 Color: 1

Bin 2252: 84 of cap free
Amount of items: 2
Items: 
Size: 796673 Color: 1
Size: 203244 Color: 0

Bin 2253: 85 of cap free
Amount of items: 2
Items: 
Size: 715846 Color: 1
Size: 284070 Color: 0

Bin 2254: 85 of cap free
Amount of items: 2
Items: 
Size: 540045 Color: 1
Size: 459871 Color: 0

Bin 2255: 85 of cap free
Amount of items: 2
Items: 
Size: 510904 Color: 0
Size: 489012 Color: 1

Bin 2256: 85 of cap free
Amount of items: 2
Items: 
Size: 526452 Color: 1
Size: 473464 Color: 0

Bin 2257: 85 of cap free
Amount of items: 2
Items: 
Size: 537522 Color: 0
Size: 462394 Color: 1

Bin 2258: 85 of cap free
Amount of items: 2
Items: 
Size: 602567 Color: 0
Size: 397349 Color: 1

Bin 2259: 85 of cap free
Amount of items: 2
Items: 
Size: 609585 Color: 0
Size: 390331 Color: 1

Bin 2260: 85 of cap free
Amount of items: 2
Items: 
Size: 615472 Color: 1
Size: 384444 Color: 0

Bin 2261: 85 of cap free
Amount of items: 2
Items: 
Size: 630993 Color: 1
Size: 368923 Color: 0

Bin 2262: 85 of cap free
Amount of items: 2
Items: 
Size: 658575 Color: 0
Size: 341341 Color: 1

Bin 2263: 85 of cap free
Amount of items: 2
Items: 
Size: 716654 Color: 0
Size: 283262 Color: 1

Bin 2264: 85 of cap free
Amount of items: 2
Items: 
Size: 769715 Color: 1
Size: 230201 Color: 0

Bin 2265: 86 of cap free
Amount of items: 2
Items: 
Size: 748131 Color: 1
Size: 251784 Color: 0

Bin 2266: 86 of cap free
Amount of items: 2
Items: 
Size: 584948 Color: 1
Size: 414967 Color: 0

Bin 2267: 86 of cap free
Amount of items: 2
Items: 
Size: 506621 Color: 1
Size: 493294 Color: 0

Bin 2268: 86 of cap free
Amount of items: 2
Items: 
Size: 517574 Color: 0
Size: 482341 Color: 1

Bin 2269: 86 of cap free
Amount of items: 2
Items: 
Size: 573420 Color: 1
Size: 426495 Color: 0

Bin 2270: 86 of cap free
Amount of items: 2
Items: 
Size: 606176 Color: 1
Size: 393739 Color: 0

Bin 2271: 86 of cap free
Amount of items: 2
Items: 
Size: 646559 Color: 0
Size: 353356 Color: 1

Bin 2272: 86 of cap free
Amount of items: 2
Items: 
Size: 712003 Color: 1
Size: 287912 Color: 0

Bin 2273: 86 of cap free
Amount of items: 2
Items: 
Size: 792431 Color: 1
Size: 207484 Color: 0

Bin 2274: 87 of cap free
Amount of items: 2
Items: 
Size: 768801 Color: 0
Size: 231113 Color: 1

Bin 2275: 87 of cap free
Amount of items: 2
Items: 
Size: 508529 Color: 1
Size: 491385 Color: 0

Bin 2276: 87 of cap free
Amount of items: 2
Items: 
Size: 547207 Color: 1
Size: 452707 Color: 0

Bin 2277: 87 of cap free
Amount of items: 2
Items: 
Size: 620986 Color: 1
Size: 378928 Color: 0

Bin 2278: 87 of cap free
Amount of items: 2
Items: 
Size: 650047 Color: 1
Size: 349867 Color: 0

Bin 2279: 87 of cap free
Amount of items: 2
Items: 
Size: 689280 Color: 0
Size: 310634 Color: 1

Bin 2280: 87 of cap free
Amount of items: 2
Items: 
Size: 699219 Color: 1
Size: 300695 Color: 0

Bin 2281: 87 of cap free
Amount of items: 2
Items: 
Size: 772901 Color: 1
Size: 227013 Color: 0

Bin 2282: 87 of cap free
Amount of items: 2
Items: 
Size: 790936 Color: 0
Size: 208978 Color: 1

Bin 2283: 88 of cap free
Amount of items: 2
Items: 
Size: 554246 Color: 0
Size: 445667 Color: 1

Bin 2284: 88 of cap free
Amount of items: 2
Items: 
Size: 556058 Color: 1
Size: 443855 Color: 0

Bin 2285: 88 of cap free
Amount of items: 2
Items: 
Size: 571503 Color: 0
Size: 428410 Color: 1

Bin 2286: 88 of cap free
Amount of items: 2
Items: 
Size: 608679 Color: 1
Size: 391234 Color: 0

Bin 2287: 88 of cap free
Amount of items: 2
Items: 
Size: 649776 Color: 0
Size: 350137 Color: 1

Bin 2288: 88 of cap free
Amount of items: 2
Items: 
Size: 655536 Color: 0
Size: 344377 Color: 1

Bin 2289: 88 of cap free
Amount of items: 2
Items: 
Size: 736126 Color: 1
Size: 263787 Color: 0

Bin 2290: 88 of cap free
Amount of items: 2
Items: 
Size: 764065 Color: 0
Size: 235848 Color: 1

Bin 2291: 88 of cap free
Amount of items: 2
Items: 
Size: 799777 Color: 0
Size: 200136 Color: 1

Bin 2292: 89 of cap free
Amount of items: 2
Items: 
Size: 744458 Color: 1
Size: 255454 Color: 0

Bin 2293: 89 of cap free
Amount of items: 2
Items: 
Size: 522049 Color: 0
Size: 477863 Color: 1

Bin 2294: 89 of cap free
Amount of items: 2
Items: 
Size: 531272 Color: 1
Size: 468640 Color: 0

Bin 2295: 89 of cap free
Amount of items: 2
Items: 
Size: 571226 Color: 0
Size: 428686 Color: 1

Bin 2296: 89 of cap free
Amount of items: 2
Items: 
Size: 626845 Color: 0
Size: 373067 Color: 1

Bin 2297: 89 of cap free
Amount of items: 2
Items: 
Size: 719779 Color: 1
Size: 280133 Color: 0

Bin 2298: 89 of cap free
Amount of items: 2
Items: 
Size: 735310 Color: 1
Size: 264602 Color: 0

Bin 2299: 89 of cap free
Amount of items: 2
Items: 
Size: 762935 Color: 0
Size: 236977 Color: 1

Bin 2300: 90 of cap free
Amount of items: 2
Items: 
Size: 723781 Color: 0
Size: 276130 Color: 1

Bin 2301: 90 of cap free
Amount of items: 2
Items: 
Size: 783751 Color: 1
Size: 216160 Color: 0

Bin 2302: 90 of cap free
Amount of items: 2
Items: 
Size: 781326 Color: 0
Size: 218585 Color: 1

Bin 2303: 90 of cap free
Amount of items: 2
Items: 
Size: 597999 Color: 1
Size: 401912 Color: 0

Bin 2304: 90 of cap free
Amount of items: 2
Items: 
Size: 612355 Color: 0
Size: 387556 Color: 1

Bin 2305: 90 of cap free
Amount of items: 2
Items: 
Size: 656629 Color: 1
Size: 343282 Color: 0

Bin 2306: 90 of cap free
Amount of items: 2
Items: 
Size: 752885 Color: 1
Size: 247026 Color: 0

Bin 2307: 91 of cap free
Amount of items: 2
Items: 
Size: 565455 Color: 1
Size: 434455 Color: 0

Bin 2308: 91 of cap free
Amount of items: 2
Items: 
Size: 658182 Color: 1
Size: 341728 Color: 0

Bin 2309: 91 of cap free
Amount of items: 2
Items: 
Size: 507755 Color: 0
Size: 492155 Color: 1

Bin 2310: 91 of cap free
Amount of items: 2
Items: 
Size: 510580 Color: 1
Size: 489330 Color: 0

Bin 2311: 91 of cap free
Amount of items: 2
Items: 
Size: 514257 Color: 1
Size: 485653 Color: 0

Bin 2312: 91 of cap free
Amount of items: 2
Items: 
Size: 548161 Color: 1
Size: 451749 Color: 0

Bin 2313: 91 of cap free
Amount of items: 2
Items: 
Size: 567786 Color: 1
Size: 432124 Color: 0

Bin 2314: 91 of cap free
Amount of items: 2
Items: 
Size: 589464 Color: 1
Size: 410446 Color: 0

Bin 2315: 91 of cap free
Amount of items: 2
Items: 
Size: 621521 Color: 0
Size: 378389 Color: 1

Bin 2316: 91 of cap free
Amount of items: 2
Items: 
Size: 622444 Color: 1
Size: 377466 Color: 0

Bin 2317: 91 of cap free
Amount of items: 2
Items: 
Size: 669550 Color: 0
Size: 330360 Color: 1

Bin 2318: 91 of cap free
Amount of items: 2
Items: 
Size: 675888 Color: 0
Size: 324022 Color: 1

Bin 2319: 91 of cap free
Amount of items: 2
Items: 
Size: 715317 Color: 1
Size: 284593 Color: 0

Bin 2320: 91 of cap free
Amount of items: 2
Items: 
Size: 746654 Color: 1
Size: 253256 Color: 0

Bin 2321: 91 of cap free
Amount of items: 2
Items: 
Size: 772348 Color: 1
Size: 227562 Color: 0

Bin 2322: 91 of cap free
Amount of items: 2
Items: 
Size: 784645 Color: 1
Size: 215265 Color: 0

Bin 2323: 91 of cap free
Amount of items: 2
Items: 
Size: 791930 Color: 0
Size: 207980 Color: 1

Bin 2324: 91 of cap free
Amount of items: 2
Items: 
Size: 794050 Color: 1
Size: 205860 Color: 0

Bin 2325: 92 of cap free
Amount of items: 2
Items: 
Size: 669137 Color: 1
Size: 330772 Color: 0

Bin 2326: 92 of cap free
Amount of items: 2
Items: 
Size: 673270 Color: 0
Size: 326639 Color: 1

Bin 2327: 92 of cap free
Amount of items: 2
Items: 
Size: 566032 Color: 0
Size: 433877 Color: 1

Bin 2328: 92 of cap free
Amount of items: 2
Items: 
Size: 672361 Color: 0
Size: 327548 Color: 1

Bin 2329: 92 of cap free
Amount of items: 2
Items: 
Size: 503127 Color: 0
Size: 496782 Color: 1

Bin 2330: 92 of cap free
Amount of items: 2
Items: 
Size: 598889 Color: 1
Size: 401020 Color: 0

Bin 2331: 92 of cap free
Amount of items: 2
Items: 
Size: 659206 Color: 0
Size: 340703 Color: 1

Bin 2332: 92 of cap free
Amount of items: 2
Items: 
Size: 680739 Color: 1
Size: 319170 Color: 0

Bin 2333: 92 of cap free
Amount of items: 2
Items: 
Size: 717967 Color: 0
Size: 281942 Color: 1

Bin 2334: 93 of cap free
Amount of items: 2
Items: 
Size: 687294 Color: 1
Size: 312614 Color: 0

Bin 2335: 93 of cap free
Amount of items: 2
Items: 
Size: 538759 Color: 0
Size: 461149 Color: 1

Bin 2336: 93 of cap free
Amount of items: 2
Items: 
Size: 541542 Color: 1
Size: 458366 Color: 0

Bin 2337: 93 of cap free
Amount of items: 2
Items: 
Size: 569842 Color: 0
Size: 430066 Color: 1

Bin 2338: 93 of cap free
Amount of items: 2
Items: 
Size: 593639 Color: 1
Size: 406269 Color: 0

Bin 2339: 93 of cap free
Amount of items: 2
Items: 
Size: 609150 Color: 1
Size: 390758 Color: 0

Bin 2340: 93 of cap free
Amount of items: 2
Items: 
Size: 671462 Color: 0
Size: 328446 Color: 1

Bin 2341: 93 of cap free
Amount of items: 2
Items: 
Size: 672974 Color: 0
Size: 326934 Color: 1

Bin 2342: 93 of cap free
Amount of items: 2
Items: 
Size: 680558 Color: 0
Size: 319350 Color: 1

Bin 2343: 93 of cap free
Amount of items: 2
Items: 
Size: 682693 Color: 0
Size: 317215 Color: 1

Bin 2344: 93 of cap free
Amount of items: 2
Items: 
Size: 786617 Color: 1
Size: 213291 Color: 0

Bin 2345: 93 of cap free
Amount of items: 2
Items: 
Size: 796570 Color: 1
Size: 203338 Color: 0

Bin 2346: 94 of cap free
Amount of items: 4
Items: 
Size: 287733 Color: 0
Size: 281881 Color: 0
Size: 228911 Color: 1
Size: 201382 Color: 1

Bin 2347: 94 of cap free
Amount of items: 2
Items: 
Size: 531917 Color: 0
Size: 467990 Color: 1

Bin 2348: 94 of cap free
Amount of items: 2
Items: 
Size: 511920 Color: 1
Size: 487987 Color: 0

Bin 2349: 94 of cap free
Amount of items: 2
Items: 
Size: 529866 Color: 0
Size: 470041 Color: 1

Bin 2350: 94 of cap free
Amount of items: 2
Items: 
Size: 555598 Color: 0
Size: 444309 Color: 1

Bin 2351: 94 of cap free
Amount of items: 2
Items: 
Size: 577811 Color: 1
Size: 422096 Color: 0

Bin 2352: 94 of cap free
Amount of items: 2
Items: 
Size: 617398 Color: 1
Size: 382509 Color: 0

Bin 2353: 94 of cap free
Amount of items: 2
Items: 
Size: 714420 Color: 1
Size: 285487 Color: 0

Bin 2354: 95 of cap free
Amount of items: 2
Items: 
Size: 558943 Color: 1
Size: 440963 Color: 0

Bin 2355: 95 of cap free
Amount of items: 2
Items: 
Size: 606369 Color: 0
Size: 393537 Color: 1

Bin 2356: 95 of cap free
Amount of items: 2
Items: 
Size: 681817 Color: 1
Size: 318089 Color: 0

Bin 2357: 95 of cap free
Amount of items: 2
Items: 
Size: 697750 Color: 0
Size: 302156 Color: 1

Bin 2358: 95 of cap free
Amount of items: 2
Items: 
Size: 773288 Color: 1
Size: 226618 Color: 0

Bin 2359: 95 of cap free
Amount of items: 2
Items: 
Size: 797661 Color: 0
Size: 202245 Color: 1

Bin 2360: 96 of cap free
Amount of items: 2
Items: 
Size: 624992 Color: 1
Size: 374913 Color: 0

Bin 2361: 96 of cap free
Amount of items: 2
Items: 
Size: 520434 Color: 0
Size: 479471 Color: 1

Bin 2362: 96 of cap free
Amount of items: 2
Items: 
Size: 559288 Color: 1
Size: 440617 Color: 0

Bin 2363: 96 of cap free
Amount of items: 2
Items: 
Size: 571290 Color: 1
Size: 428615 Color: 0

Bin 2364: 96 of cap free
Amount of items: 2
Items: 
Size: 582036 Color: 1
Size: 417869 Color: 0

Bin 2365: 96 of cap free
Amount of items: 2
Items: 
Size: 656623 Color: 1
Size: 343282 Color: 0

Bin 2366: 96 of cap free
Amount of items: 2
Items: 
Size: 658130 Color: 0
Size: 341775 Color: 1

Bin 2367: 96 of cap free
Amount of items: 2
Items: 
Size: 668446 Color: 1
Size: 331459 Color: 0

Bin 2368: 96 of cap free
Amount of items: 2
Items: 
Size: 669126 Color: 0
Size: 330779 Color: 1

Bin 2369: 96 of cap free
Amount of items: 2
Items: 
Size: 690607 Color: 1
Size: 309298 Color: 0

Bin 2370: 96 of cap free
Amount of items: 2
Items: 
Size: 695126 Color: 0
Size: 304779 Color: 1

Bin 2371: 96 of cap free
Amount of items: 2
Items: 
Size: 713143 Color: 0
Size: 286762 Color: 1

Bin 2372: 96 of cap free
Amount of items: 2
Items: 
Size: 784695 Color: 0
Size: 215210 Color: 1

Bin 2373: 97 of cap free
Amount of items: 2
Items: 
Size: 622116 Color: 1
Size: 377788 Color: 0

Bin 2374: 97 of cap free
Amount of items: 2
Items: 
Size: 519917 Color: 1
Size: 479987 Color: 0

Bin 2375: 97 of cap free
Amount of items: 2
Items: 
Size: 542748 Color: 1
Size: 457156 Color: 0

Bin 2376: 97 of cap free
Amount of items: 2
Items: 
Size: 557809 Color: 0
Size: 442095 Color: 1

Bin 2377: 97 of cap free
Amount of items: 2
Items: 
Size: 629246 Color: 1
Size: 370658 Color: 0

Bin 2378: 98 of cap free
Amount of items: 2
Items: 
Size: 799992 Color: 0
Size: 199911 Color: 1

Bin 2379: 98 of cap free
Amount of items: 2
Items: 
Size: 515689 Color: 1
Size: 484214 Color: 0

Bin 2380: 98 of cap free
Amount of items: 2
Items: 
Size: 556724 Color: 0
Size: 443179 Color: 1

Bin 2381: 98 of cap free
Amount of items: 2
Items: 
Size: 589868 Color: 0
Size: 410035 Color: 1

Bin 2382: 98 of cap free
Amount of items: 2
Items: 
Size: 590243 Color: 1
Size: 409660 Color: 0

Bin 2383: 98 of cap free
Amount of items: 2
Items: 
Size: 676466 Color: 0
Size: 323437 Color: 1

Bin 2384: 99 of cap free
Amount of items: 2
Items: 
Size: 513623 Color: 0
Size: 486279 Color: 1

Bin 2385: 99 of cap free
Amount of items: 2
Items: 
Size: 517440 Color: 1
Size: 482462 Color: 0

Bin 2386: 99 of cap free
Amount of items: 2
Items: 
Size: 579923 Color: 0
Size: 419979 Color: 1

Bin 2387: 99 of cap free
Amount of items: 2
Items: 
Size: 594384 Color: 1
Size: 405518 Color: 0

Bin 2388: 99 of cap free
Amount of items: 2
Items: 
Size: 610996 Color: 1
Size: 388906 Color: 0

Bin 2389: 99 of cap free
Amount of items: 2
Items: 
Size: 625039 Color: 0
Size: 374863 Color: 1

Bin 2390: 99 of cap free
Amount of items: 2
Items: 
Size: 651651 Color: 1
Size: 348251 Color: 0

Bin 2391: 99 of cap free
Amount of items: 2
Items: 
Size: 669001 Color: 1
Size: 330901 Color: 0

Bin 2392: 99 of cap free
Amount of items: 2
Items: 
Size: 690220 Color: 0
Size: 309682 Color: 1

Bin 2393: 99 of cap free
Amount of items: 2
Items: 
Size: 761804 Color: 0
Size: 238098 Color: 1

Bin 2394: 100 of cap free
Amount of items: 2
Items: 
Size: 772898 Color: 0
Size: 227003 Color: 1

Bin 2395: 100 of cap free
Amount of items: 2
Items: 
Size: 541385 Color: 0
Size: 458516 Color: 1

Bin 2396: 100 of cap free
Amount of items: 2
Items: 
Size: 546527 Color: 1
Size: 453374 Color: 0

Bin 2397: 100 of cap free
Amount of items: 2
Items: 
Size: 567500 Color: 1
Size: 432401 Color: 0

Bin 2398: 100 of cap free
Amount of items: 2
Items: 
Size: 594430 Color: 0
Size: 405471 Color: 1

Bin 2399: 100 of cap free
Amount of items: 2
Items: 
Size: 666106 Color: 0
Size: 333795 Color: 1

Bin 2400: 100 of cap free
Amount of items: 2
Items: 
Size: 745879 Color: 1
Size: 254022 Color: 0

Bin 2401: 100 of cap free
Amount of items: 2
Items: 
Size: 776742 Color: 1
Size: 223159 Color: 0

Bin 2402: 101 of cap free
Amount of items: 2
Items: 
Size: 707020 Color: 1
Size: 292880 Color: 0

Bin 2403: 101 of cap free
Amount of items: 2
Items: 
Size: 576440 Color: 0
Size: 423460 Color: 1

Bin 2404: 101 of cap free
Amount of items: 2
Items: 
Size: 606162 Color: 1
Size: 393738 Color: 0

Bin 2405: 101 of cap free
Amount of items: 2
Items: 
Size: 610151 Color: 0
Size: 389749 Color: 1

Bin 2406: 101 of cap free
Amount of items: 2
Items: 
Size: 616985 Color: 1
Size: 382915 Color: 0

Bin 2407: 101 of cap free
Amount of items: 2
Items: 
Size: 730406 Color: 0
Size: 269494 Color: 1

Bin 2408: 101 of cap free
Amount of items: 2
Items: 
Size: 766253 Color: 1
Size: 233647 Color: 0

Bin 2409: 102 of cap free
Amount of items: 2
Items: 
Size: 742260 Color: 1
Size: 257639 Color: 0

Bin 2410: 102 of cap free
Amount of items: 2
Items: 
Size: 502465 Color: 0
Size: 497434 Color: 1

Bin 2411: 102 of cap free
Amount of items: 2
Items: 
Size: 540361 Color: 1
Size: 459538 Color: 0

Bin 2412: 102 of cap free
Amount of items: 2
Items: 
Size: 579428 Color: 0
Size: 420471 Color: 1

Bin 2413: 102 of cap free
Amount of items: 2
Items: 
Size: 589309 Color: 1
Size: 410590 Color: 0

Bin 2414: 102 of cap free
Amount of items: 2
Items: 
Size: 628851 Color: 0
Size: 371048 Color: 1

Bin 2415: 102 of cap free
Amount of items: 2
Items: 
Size: 793460 Color: 0
Size: 206439 Color: 1

Bin 2416: 103 of cap free
Amount of items: 2
Items: 
Size: 600035 Color: 1
Size: 399863 Color: 0

Bin 2417: 103 of cap free
Amount of items: 2
Items: 
Size: 675096 Color: 0
Size: 324802 Color: 1

Bin 2418: 103 of cap free
Amount of items: 2
Items: 
Size: 532724 Color: 0
Size: 467174 Color: 1

Bin 2419: 103 of cap free
Amount of items: 2
Items: 
Size: 586851 Color: 0
Size: 413047 Color: 1

Bin 2420: 103 of cap free
Amount of items: 2
Items: 
Size: 604240 Color: 0
Size: 395658 Color: 1

Bin 2421: 103 of cap free
Amount of items: 2
Items: 
Size: 616574 Color: 1
Size: 383324 Color: 0

Bin 2422: 103 of cap free
Amount of items: 2
Items: 
Size: 616739 Color: 1
Size: 383159 Color: 0

Bin 2423: 103 of cap free
Amount of items: 2
Items: 
Size: 617503 Color: 0
Size: 382395 Color: 1

Bin 2424: 103 of cap free
Amount of items: 2
Items: 
Size: 634505 Color: 0
Size: 365393 Color: 1

Bin 2425: 103 of cap free
Amount of items: 2
Items: 
Size: 783531 Color: 1
Size: 216367 Color: 0

Bin 2426: 104 of cap free
Amount of items: 2
Items: 
Size: 594203 Color: 0
Size: 405694 Color: 1

Bin 2427: 104 of cap free
Amount of items: 2
Items: 
Size: 720430 Color: 0
Size: 279467 Color: 1

Bin 2428: 104 of cap free
Amount of items: 2
Items: 
Size: 784099 Color: 1
Size: 215798 Color: 0

Bin 2429: 104 of cap free
Amount of items: 2
Items: 
Size: 520581 Color: 0
Size: 479316 Color: 1

Bin 2430: 104 of cap free
Amount of items: 2
Items: 
Size: 560167 Color: 0
Size: 439730 Color: 1

Bin 2431: 104 of cap free
Amount of items: 2
Items: 
Size: 612089 Color: 0
Size: 387808 Color: 1

Bin 2432: 104 of cap free
Amount of items: 2
Items: 
Size: 647964 Color: 1
Size: 351933 Color: 0

Bin 2433: 104 of cap free
Amount of items: 2
Items: 
Size: 665562 Color: 0
Size: 334335 Color: 1

Bin 2434: 104 of cap free
Amount of items: 2
Items: 
Size: 672710 Color: 0
Size: 327187 Color: 1

Bin 2435: 104 of cap free
Amount of items: 2
Items: 
Size: 718425 Color: 1
Size: 281472 Color: 0

Bin 2436: 104 of cap free
Amount of items: 2
Items: 
Size: 737263 Color: 0
Size: 262634 Color: 1

Bin 2437: 105 of cap free
Amount of items: 2
Items: 
Size: 550397 Color: 0
Size: 449499 Color: 1

Bin 2438: 105 of cap free
Amount of items: 2
Items: 
Size: 578620 Color: 1
Size: 421276 Color: 0

Bin 2439: 105 of cap free
Amount of items: 2
Items: 
Size: 607804 Color: 0
Size: 392092 Color: 1

Bin 2440: 105 of cap free
Amount of items: 2
Items: 
Size: 635433 Color: 0
Size: 364463 Color: 1

Bin 2441: 105 of cap free
Amount of items: 2
Items: 
Size: 695897 Color: 1
Size: 303999 Color: 0

Bin 2442: 105 of cap free
Amount of items: 2
Items: 
Size: 714294 Color: 1
Size: 285602 Color: 0

Bin 2443: 105 of cap free
Amount of items: 2
Items: 
Size: 763271 Color: 0
Size: 236625 Color: 1

Bin 2444: 106 of cap free
Amount of items: 2
Items: 
Size: 645616 Color: 0
Size: 354279 Color: 1

Bin 2445: 106 of cap free
Amount of items: 2
Items: 
Size: 647843 Color: 1
Size: 352052 Color: 0

Bin 2446: 106 of cap free
Amount of items: 2
Items: 
Size: 714849 Color: 0
Size: 285046 Color: 1

Bin 2447: 106 of cap free
Amount of items: 2
Items: 
Size: 753736 Color: 1
Size: 246159 Color: 0

Bin 2448: 107 of cap free
Amount of items: 2
Items: 
Size: 579461 Color: 1
Size: 420433 Color: 0

Bin 2449: 107 of cap free
Amount of items: 2
Items: 
Size: 504771 Color: 1
Size: 495123 Color: 0

Bin 2450: 107 of cap free
Amount of items: 2
Items: 
Size: 594795 Color: 0
Size: 405099 Color: 1

Bin 2451: 107 of cap free
Amount of items: 2
Items: 
Size: 627238 Color: 0
Size: 372656 Color: 1

Bin 2452: 107 of cap free
Amount of items: 2
Items: 
Size: 652239 Color: 0
Size: 347655 Color: 1

Bin 2453: 107 of cap free
Amount of items: 2
Items: 
Size: 667152 Color: 1
Size: 332742 Color: 0

Bin 2454: 107 of cap free
Amount of items: 2
Items: 
Size: 743313 Color: 1
Size: 256581 Color: 0

Bin 2455: 107 of cap free
Amount of items: 2
Items: 
Size: 753344 Color: 0
Size: 246550 Color: 1

Bin 2456: 108 of cap free
Amount of items: 2
Items: 
Size: 556455 Color: 1
Size: 443438 Color: 0

Bin 2457: 108 of cap free
Amount of items: 2
Items: 
Size: 574542 Color: 1
Size: 425351 Color: 0

Bin 2458: 108 of cap free
Amount of items: 2
Items: 
Size: 726186 Color: 0
Size: 273707 Color: 1

Bin 2459: 108 of cap free
Amount of items: 2
Items: 
Size: 770838 Color: 0
Size: 229055 Color: 1

Bin 2460: 108 of cap free
Amount of items: 2
Items: 
Size: 799409 Color: 0
Size: 200484 Color: 1

Bin 2461: 108 of cap free
Amount of items: 2
Items: 
Size: 624819 Color: 1
Size: 375074 Color: 0

Bin 2462: 108 of cap free
Amount of items: 2
Items: 
Size: 501980 Color: 0
Size: 497913 Color: 1

Bin 2463: 108 of cap free
Amount of items: 2
Items: 
Size: 537491 Color: 1
Size: 462402 Color: 0

Bin 2464: 108 of cap free
Amount of items: 2
Items: 
Size: 628738 Color: 1
Size: 371155 Color: 0

Bin 2465: 108 of cap free
Amount of items: 2
Items: 
Size: 639862 Color: 0
Size: 360031 Color: 1

Bin 2466: 108 of cap free
Amount of items: 2
Items: 
Size: 734136 Color: 1
Size: 265757 Color: 0

Bin 2467: 108 of cap free
Amount of items: 2
Items: 
Size: 749701 Color: 1
Size: 250192 Color: 0

Bin 2468: 108 of cap free
Amount of items: 2
Items: 
Size: 797998 Color: 1
Size: 201895 Color: 0

Bin 2469: 109 of cap free
Amount of items: 2
Items: 
Size: 608803 Color: 0
Size: 391089 Color: 1

Bin 2470: 109 of cap free
Amount of items: 2
Items: 
Size: 791114 Color: 0
Size: 208778 Color: 1

Bin 2471: 109 of cap free
Amount of items: 2
Items: 
Size: 540121 Color: 0
Size: 459771 Color: 1

Bin 2472: 109 of cap free
Amount of items: 2
Items: 
Size: 548898 Color: 0
Size: 450994 Color: 1

Bin 2473: 109 of cap free
Amount of items: 2
Items: 
Size: 571082 Color: 0
Size: 428810 Color: 1

Bin 2474: 109 of cap free
Amount of items: 2
Items: 
Size: 595853 Color: 0
Size: 404039 Color: 1

Bin 2475: 109 of cap free
Amount of items: 2
Items: 
Size: 637723 Color: 1
Size: 362169 Color: 0

Bin 2476: 109 of cap free
Amount of items: 2
Items: 
Size: 718009 Color: 1
Size: 281883 Color: 0

Bin 2477: 109 of cap free
Amount of items: 2
Items: 
Size: 757526 Color: 0
Size: 242366 Color: 1

Bin 2478: 109 of cap free
Amount of items: 2
Items: 
Size: 759061 Color: 0
Size: 240831 Color: 1

Bin 2479: 109 of cap free
Amount of items: 2
Items: 
Size: 766718 Color: 0
Size: 233174 Color: 1

Bin 2480: 110 of cap free
Amount of items: 2
Items: 
Size: 637052 Color: 1
Size: 362839 Color: 0

Bin 2481: 110 of cap free
Amount of items: 2
Items: 
Size: 518517 Color: 1
Size: 481374 Color: 0

Bin 2482: 110 of cap free
Amount of items: 2
Items: 
Size: 588247 Color: 0
Size: 411644 Color: 1

Bin 2483: 110 of cap free
Amount of items: 2
Items: 
Size: 617980 Color: 1
Size: 381911 Color: 0

Bin 2484: 110 of cap free
Amount of items: 2
Items: 
Size: 678880 Color: 1
Size: 321011 Color: 0

Bin 2485: 110 of cap free
Amount of items: 2
Items: 
Size: 768000 Color: 0
Size: 231891 Color: 1

Bin 2486: 111 of cap free
Amount of items: 2
Items: 
Size: 566626 Color: 1
Size: 433264 Color: 0

Bin 2487: 111 of cap free
Amount of items: 2
Items: 
Size: 567451 Color: 0
Size: 432439 Color: 1

Bin 2488: 111 of cap free
Amount of items: 2
Items: 
Size: 568644 Color: 1
Size: 431246 Color: 0

Bin 2489: 111 of cap free
Amount of items: 2
Items: 
Size: 601357 Color: 1
Size: 398533 Color: 0

Bin 2490: 111 of cap free
Amount of items: 2
Items: 
Size: 647567 Color: 0
Size: 352323 Color: 1

Bin 2491: 111 of cap free
Amount of items: 2
Items: 
Size: 775222 Color: 1
Size: 224668 Color: 0

Bin 2492: 112 of cap free
Amount of items: 2
Items: 
Size: 708389 Color: 1
Size: 291500 Color: 0

Bin 2493: 112 of cap free
Amount of items: 2
Items: 
Size: 529541 Color: 0
Size: 470348 Color: 1

Bin 2494: 112 of cap free
Amount of items: 2
Items: 
Size: 645054 Color: 1
Size: 354835 Color: 0

Bin 2495: 112 of cap free
Amount of items: 2
Items: 
Size: 647490 Color: 1
Size: 352399 Color: 0

Bin 2496: 112 of cap free
Amount of items: 2
Items: 
Size: 691030 Color: 1
Size: 308859 Color: 0

Bin 2497: 113 of cap free
Amount of items: 2
Items: 
Size: 749069 Color: 0
Size: 250819 Color: 1

Bin 2498: 113 of cap free
Amount of items: 2
Items: 
Size: 762252 Color: 0
Size: 237636 Color: 1

Bin 2499: 113 of cap free
Amount of items: 2
Items: 
Size: 520114 Color: 1
Size: 479774 Color: 0

Bin 2500: 113 of cap free
Amount of items: 2
Items: 
Size: 574881 Color: 0
Size: 425007 Color: 1

Bin 2501: 113 of cap free
Amount of items: 2
Items: 
Size: 651036 Color: 1
Size: 348852 Color: 0

Bin 2502: 113 of cap free
Amount of items: 2
Items: 
Size: 653442 Color: 1
Size: 346446 Color: 0

Bin 2503: 113 of cap free
Amount of items: 2
Items: 
Size: 655530 Color: 0
Size: 344358 Color: 1

Bin 2504: 114 of cap free
Amount of items: 2
Items: 
Size: 757455 Color: 1
Size: 242432 Color: 0

Bin 2505: 114 of cap free
Amount of items: 2
Items: 
Size: 769685 Color: 0
Size: 230202 Color: 1

Bin 2506: 114 of cap free
Amount of items: 2
Items: 
Size: 798713 Color: 0
Size: 201174 Color: 1

Bin 2507: 114 of cap free
Amount of items: 2
Items: 
Size: 524741 Color: 0
Size: 475146 Color: 1

Bin 2508: 114 of cap free
Amount of items: 2
Items: 
Size: 569512 Color: 1
Size: 430375 Color: 0

Bin 2509: 114 of cap free
Amount of items: 2
Items: 
Size: 596251 Color: 0
Size: 403636 Color: 1

Bin 2510: 114 of cap free
Amount of items: 2
Items: 
Size: 607271 Color: 0
Size: 392616 Color: 1

Bin 2511: 114 of cap free
Amount of items: 2
Items: 
Size: 627013 Color: 0
Size: 372874 Color: 1

Bin 2512: 114 of cap free
Amount of items: 2
Items: 
Size: 652572 Color: 1
Size: 347315 Color: 0

Bin 2513: 115 of cap free
Amount of items: 2
Items: 
Size: 565866 Color: 1
Size: 434020 Color: 0

Bin 2514: 115 of cap free
Amount of items: 2
Items: 
Size: 562880 Color: 0
Size: 437006 Color: 1

Bin 2515: 115 of cap free
Amount of items: 2
Items: 
Size: 536461 Color: 0
Size: 463425 Color: 1

Bin 2516: 115 of cap free
Amount of items: 2
Items: 
Size: 555655 Color: 1
Size: 444231 Color: 0

Bin 2517: 115 of cap free
Amount of items: 2
Items: 
Size: 559154 Color: 0
Size: 440732 Color: 1

Bin 2518: 115 of cap free
Amount of items: 2
Items: 
Size: 745516 Color: 1
Size: 254370 Color: 0

Bin 2519: 115 of cap free
Amount of items: 2
Items: 
Size: 782221 Color: 1
Size: 217665 Color: 0

Bin 2520: 116 of cap free
Amount of items: 2
Items: 
Size: 682945 Color: 1
Size: 316940 Color: 0

Bin 2521: 116 of cap free
Amount of items: 2
Items: 
Size: 525046 Color: 0
Size: 474839 Color: 1

Bin 2522: 116 of cap free
Amount of items: 2
Items: 
Size: 542892 Color: 0
Size: 456993 Color: 1

Bin 2523: 116 of cap free
Amount of items: 2
Items: 
Size: 575656 Color: 0
Size: 424229 Color: 1

Bin 2524: 116 of cap free
Amount of items: 2
Items: 
Size: 577341 Color: 0
Size: 422544 Color: 1

Bin 2525: 116 of cap free
Amount of items: 2
Items: 
Size: 633299 Color: 1
Size: 366586 Color: 0

Bin 2526: 116 of cap free
Amount of items: 2
Items: 
Size: 649488 Color: 0
Size: 350397 Color: 1

Bin 2527: 116 of cap free
Amount of items: 2
Items: 
Size: 686585 Color: 0
Size: 313300 Color: 1

Bin 2528: 116 of cap free
Amount of items: 2
Items: 
Size: 688012 Color: 1
Size: 311873 Color: 0

Bin 2529: 116 of cap free
Amount of items: 2
Items: 
Size: 699580 Color: 1
Size: 300305 Color: 0

Bin 2530: 116 of cap free
Amount of items: 2
Items: 
Size: 707474 Color: 0
Size: 292411 Color: 1

Bin 2531: 117 of cap free
Amount of items: 2
Items: 
Size: 685863 Color: 0
Size: 314021 Color: 1

Bin 2532: 117 of cap free
Amount of items: 2
Items: 
Size: 732976 Color: 0
Size: 266908 Color: 1

Bin 2533: 117 of cap free
Amount of items: 2
Items: 
Size: 582734 Color: 0
Size: 417150 Color: 1

Bin 2534: 117 of cap free
Amount of items: 2
Items: 
Size: 644903 Color: 0
Size: 354981 Color: 1

Bin 2535: 117 of cap free
Amount of items: 2
Items: 
Size: 654453 Color: 1
Size: 345431 Color: 0

Bin 2536: 117 of cap free
Amount of items: 2
Items: 
Size: 660495 Color: 1
Size: 339389 Color: 0

Bin 2537: 117 of cap free
Amount of items: 2
Items: 
Size: 664395 Color: 1
Size: 335489 Color: 0

Bin 2538: 117 of cap free
Amount of items: 2
Items: 
Size: 685318 Color: 0
Size: 314566 Color: 1

Bin 2539: 118 of cap free
Amount of items: 2
Items: 
Size: 668622 Color: 0
Size: 331261 Color: 1

Bin 2540: 118 of cap free
Amount of items: 2
Items: 
Size: 696421 Color: 1
Size: 303462 Color: 0

Bin 2541: 118 of cap free
Amount of items: 2
Items: 
Size: 750773 Color: 0
Size: 249110 Color: 1

Bin 2542: 118 of cap free
Amount of items: 2
Items: 
Size: 773046 Color: 1
Size: 226837 Color: 0

Bin 2543: 119 of cap free
Amount of items: 5
Items: 
Size: 282064 Color: 0
Size: 269338 Color: 0
Size: 225285 Color: 1
Size: 118744 Color: 1
Size: 104451 Color: 1

Bin 2544: 119 of cap free
Amount of items: 2
Items: 
Size: 730117 Color: 0
Size: 269765 Color: 1

Bin 2545: 119 of cap free
Amount of items: 2
Items: 
Size: 791618 Color: 1
Size: 208264 Color: 0

Bin 2546: 119 of cap free
Amount of items: 2
Items: 
Size: 534225 Color: 0
Size: 465657 Color: 1

Bin 2547: 119 of cap free
Amount of items: 2
Items: 
Size: 620985 Color: 1
Size: 378897 Color: 0

Bin 2548: 119 of cap free
Amount of items: 2
Items: 
Size: 773101 Color: 0
Size: 226781 Color: 1

Bin 2549: 119 of cap free
Amount of items: 2
Items: 
Size: 787743 Color: 1
Size: 212139 Color: 0

Bin 2550: 120 of cap free
Amount of items: 2
Items: 
Size: 591177 Color: 0
Size: 408704 Color: 1

Bin 2551: 120 of cap free
Amount of items: 3
Items: 
Size: 353281 Color: 1
Size: 330119 Color: 0
Size: 316481 Color: 1

Bin 2552: 120 of cap free
Amount of items: 2
Items: 
Size: 504863 Color: 0
Size: 495018 Color: 1

Bin 2553: 120 of cap free
Amount of items: 2
Items: 
Size: 626593 Color: 1
Size: 373288 Color: 0

Bin 2554: 120 of cap free
Amount of items: 2
Items: 
Size: 639810 Color: 1
Size: 360071 Color: 0

Bin 2555: 120 of cap free
Amount of items: 2
Items: 
Size: 655300 Color: 0
Size: 344581 Color: 1

Bin 2556: 120 of cap free
Amount of items: 2
Items: 
Size: 668048 Color: 0
Size: 331833 Color: 1

Bin 2557: 121 of cap free
Amount of items: 2
Items: 
Size: 798306 Color: 1
Size: 201574 Color: 0

Bin 2558: 121 of cap free
Amount of items: 2
Items: 
Size: 628405 Color: 1
Size: 371475 Color: 0

Bin 2559: 121 of cap free
Amount of items: 2
Items: 
Size: 520409 Color: 1
Size: 479471 Color: 0

Bin 2560: 121 of cap free
Amount of items: 2
Items: 
Size: 520901 Color: 0
Size: 478979 Color: 1

Bin 2561: 121 of cap free
Amount of items: 2
Items: 
Size: 547270 Color: 0
Size: 452610 Color: 1

Bin 2562: 121 of cap free
Amount of items: 2
Items: 
Size: 684040 Color: 0
Size: 315840 Color: 1

Bin 2563: 121 of cap free
Amount of items: 2
Items: 
Size: 748220 Color: 0
Size: 251660 Color: 1

Bin 2564: 121 of cap free
Amount of items: 2
Items: 
Size: 792302 Color: 0
Size: 207578 Color: 1

Bin 2565: 122 of cap free
Amount of items: 2
Items: 
Size: 511767 Color: 0
Size: 488112 Color: 1

Bin 2566: 122 of cap free
Amount of items: 2
Items: 
Size: 554115 Color: 0
Size: 445764 Color: 1

Bin 2567: 122 of cap free
Amount of items: 2
Items: 
Size: 586539 Color: 1
Size: 413340 Color: 0

Bin 2568: 122 of cap free
Amount of items: 2
Items: 
Size: 613497 Color: 0
Size: 386382 Color: 1

Bin 2569: 122 of cap free
Amount of items: 2
Items: 
Size: 663399 Color: 0
Size: 336480 Color: 1

Bin 2570: 122 of cap free
Amount of items: 2
Items: 
Size: 739546 Color: 1
Size: 260333 Color: 0

Bin 2571: 122 of cap free
Amount of items: 2
Items: 
Size: 741897 Color: 1
Size: 257982 Color: 0

Bin 2572: 123 of cap free
Amount of items: 2
Items: 
Size: 690047 Color: 1
Size: 309831 Color: 0

Bin 2573: 123 of cap free
Amount of items: 2
Items: 
Size: 505034 Color: 1
Size: 494844 Color: 0

Bin 2574: 123 of cap free
Amount of items: 2
Items: 
Size: 549840 Color: 0
Size: 450038 Color: 1

Bin 2575: 123 of cap free
Amount of items: 2
Items: 
Size: 570668 Color: 0
Size: 429210 Color: 1

Bin 2576: 123 of cap free
Amount of items: 2
Items: 
Size: 587247 Color: 1
Size: 412631 Color: 0

Bin 2577: 123 of cap free
Amount of items: 2
Items: 
Size: 638346 Color: 0
Size: 361532 Color: 1

Bin 2578: 123 of cap free
Amount of items: 2
Items: 
Size: 642094 Color: 0
Size: 357784 Color: 1

Bin 2579: 123 of cap free
Amount of items: 2
Items: 
Size: 644387 Color: 0
Size: 355491 Color: 1

Bin 2580: 123 of cap free
Amount of items: 2
Items: 
Size: 683688 Color: 0
Size: 316190 Color: 1

Bin 2581: 123 of cap free
Amount of items: 2
Items: 
Size: 792200 Color: 1
Size: 207678 Color: 0

Bin 2582: 124 of cap free
Amount of items: 2
Items: 
Size: 531905 Color: 1
Size: 467972 Color: 0

Bin 2583: 124 of cap free
Amount of items: 2
Items: 
Size: 570498 Color: 1
Size: 429379 Color: 0

Bin 2584: 124 of cap free
Amount of items: 2
Items: 
Size: 603553 Color: 1
Size: 396324 Color: 0

Bin 2585: 124 of cap free
Amount of items: 2
Items: 
Size: 636809 Color: 0
Size: 363068 Color: 1

Bin 2586: 124 of cap free
Amount of items: 2
Items: 
Size: 655812 Color: 1
Size: 344065 Color: 0

Bin 2587: 125 of cap free
Amount of items: 2
Items: 
Size: 783525 Color: 0
Size: 216351 Color: 1

Bin 2588: 125 of cap free
Amount of items: 2
Items: 
Size: 522228 Color: 0
Size: 477648 Color: 1

Bin 2589: 125 of cap free
Amount of items: 2
Items: 
Size: 544606 Color: 0
Size: 455270 Color: 1

Bin 2590: 125 of cap free
Amount of items: 2
Items: 
Size: 601533 Color: 1
Size: 398343 Color: 0

Bin 2591: 125 of cap free
Amount of items: 2
Items: 
Size: 641048 Color: 0
Size: 358828 Color: 1

Bin 2592: 125 of cap free
Amount of items: 2
Items: 
Size: 644683 Color: 0
Size: 355193 Color: 1

Bin 2593: 125 of cap free
Amount of items: 2
Items: 
Size: 647973 Color: 0
Size: 351903 Color: 1

Bin 2594: 125 of cap free
Amount of items: 2
Items: 
Size: 693098 Color: 1
Size: 306778 Color: 0

Bin 2595: 125 of cap free
Amount of items: 2
Items: 
Size: 722851 Color: 1
Size: 277025 Color: 0

Bin 2596: 125 of cap free
Amount of items: 2
Items: 
Size: 758239 Color: 0
Size: 241637 Color: 1

Bin 2597: 125 of cap free
Amount of items: 2
Items: 
Size: 766471 Color: 0
Size: 233405 Color: 1

Bin 2598: 126 of cap free
Amount of items: 2
Items: 
Size: 797688 Color: 1
Size: 202187 Color: 0

Bin 2599: 126 of cap free
Amount of items: 2
Items: 
Size: 559921 Color: 1
Size: 439954 Color: 0

Bin 2600: 126 of cap free
Amount of items: 2
Items: 
Size: 628020 Color: 1
Size: 371855 Color: 0

Bin 2601: 126 of cap free
Amount of items: 2
Items: 
Size: 641209 Color: 0
Size: 358666 Color: 1

Bin 2602: 126 of cap free
Amount of items: 2
Items: 
Size: 646899 Color: 0
Size: 352976 Color: 1

Bin 2603: 126 of cap free
Amount of items: 2
Items: 
Size: 691024 Color: 1
Size: 308851 Color: 0

Bin 2604: 126 of cap free
Amount of items: 2
Items: 
Size: 733088 Color: 1
Size: 266787 Color: 0

Bin 2605: 127 of cap free
Amount of items: 2
Items: 
Size: 692503 Color: 0
Size: 307371 Color: 1

Bin 2606: 127 of cap free
Amount of items: 2
Items: 
Size: 723478 Color: 1
Size: 276396 Color: 0

Bin 2607: 127 of cap free
Amount of items: 2
Items: 
Size: 553314 Color: 0
Size: 446560 Color: 1

Bin 2608: 127 of cap free
Amount of items: 2
Items: 
Size: 582372 Color: 0
Size: 417502 Color: 1

Bin 2609: 127 of cap free
Amount of items: 2
Items: 
Size: 600405 Color: 1
Size: 399469 Color: 0

Bin 2610: 127 of cap free
Amount of items: 2
Items: 
Size: 609831 Color: 0
Size: 390043 Color: 1

Bin 2611: 127 of cap free
Amount of items: 2
Items: 
Size: 614167 Color: 1
Size: 385707 Color: 0

Bin 2612: 128 of cap free
Amount of items: 2
Items: 
Size: 746899 Color: 0
Size: 252974 Color: 1

Bin 2613: 128 of cap free
Amount of items: 2
Items: 
Size: 501055 Color: 0
Size: 498818 Color: 1

Bin 2614: 128 of cap free
Amount of items: 2
Items: 
Size: 530489 Color: 1
Size: 469384 Color: 0

Bin 2615: 128 of cap free
Amount of items: 2
Items: 
Size: 534817 Color: 1
Size: 465056 Color: 0

Bin 2616: 128 of cap free
Amount of items: 2
Items: 
Size: 541542 Color: 0
Size: 458331 Color: 1

Bin 2617: 128 of cap free
Amount of items: 2
Items: 
Size: 616000 Color: 1
Size: 383873 Color: 0

Bin 2618: 128 of cap free
Amount of items: 2
Items: 
Size: 635883 Color: 1
Size: 363990 Color: 0

Bin 2619: 128 of cap free
Amount of items: 2
Items: 
Size: 684194 Color: 0
Size: 315679 Color: 1

Bin 2620: 128 of cap free
Amount of items: 2
Items: 
Size: 693729 Color: 0
Size: 306144 Color: 1

Bin 2621: 128 of cap free
Amount of items: 2
Items: 
Size: 766953 Color: 0
Size: 232920 Color: 1

Bin 2622: 129 of cap free
Amount of items: 2
Items: 
Size: 781339 Color: 1
Size: 218533 Color: 0

Bin 2623: 129 of cap free
Amount of items: 2
Items: 
Size: 502310 Color: 1
Size: 497562 Color: 0

Bin 2624: 129 of cap free
Amount of items: 2
Items: 
Size: 518725 Color: 1
Size: 481147 Color: 0

Bin 2625: 129 of cap free
Amount of items: 2
Items: 
Size: 588958 Color: 0
Size: 410914 Color: 1

Bin 2626: 129 of cap free
Amount of items: 2
Items: 
Size: 602450 Color: 1
Size: 397422 Color: 0

Bin 2627: 129 of cap free
Amount of items: 2
Items: 
Size: 650481 Color: 0
Size: 349391 Color: 1

Bin 2628: 130 of cap free
Amount of items: 2
Items: 
Size: 742564 Color: 1
Size: 257307 Color: 0

Bin 2629: 130 of cap free
Amount of items: 2
Items: 
Size: 530355 Color: 0
Size: 469516 Color: 1

Bin 2630: 130 of cap free
Amount of items: 2
Items: 
Size: 570777 Color: 1
Size: 429094 Color: 0

Bin 2631: 130 of cap free
Amount of items: 2
Items: 
Size: 657161 Color: 1
Size: 342710 Color: 0

Bin 2632: 130 of cap free
Amount of items: 2
Items: 
Size: 707433 Color: 1
Size: 292438 Color: 0

Bin 2633: 130 of cap free
Amount of items: 2
Items: 
Size: 756315 Color: 1
Size: 243556 Color: 0

Bin 2634: 131 of cap free
Amount of items: 2
Items: 
Size: 772621 Color: 1
Size: 227249 Color: 0

Bin 2635: 131 of cap free
Amount of items: 2
Items: 
Size: 617919 Color: 0
Size: 381951 Color: 1

Bin 2636: 131 of cap free
Amount of items: 2
Items: 
Size: 512877 Color: 0
Size: 486993 Color: 1

Bin 2637: 131 of cap free
Amount of items: 2
Items: 
Size: 530632 Color: 0
Size: 469238 Color: 1

Bin 2638: 131 of cap free
Amount of items: 2
Items: 
Size: 629352 Color: 0
Size: 370518 Color: 1

Bin 2639: 131 of cap free
Amount of items: 2
Items: 
Size: 670794 Color: 0
Size: 329076 Color: 1

Bin 2640: 131 of cap free
Amount of items: 2
Items: 
Size: 698902 Color: 1
Size: 300968 Color: 0

Bin 2641: 132 of cap free
Amount of items: 2
Items: 
Size: 625250 Color: 1
Size: 374619 Color: 0

Bin 2642: 133 of cap free
Amount of items: 2
Items: 
Size: 507744 Color: 0
Size: 492124 Color: 1

Bin 2643: 133 of cap free
Amount of items: 2
Items: 
Size: 608467 Color: 0
Size: 391401 Color: 1

Bin 2644: 133 of cap free
Amount of items: 2
Items: 
Size: 798244 Color: 0
Size: 201624 Color: 1

Bin 2645: 134 of cap free
Amount of items: 2
Items: 
Size: 778768 Color: 1
Size: 221099 Color: 0

Bin 2646: 134 of cap free
Amount of items: 2
Items: 
Size: 516079 Color: 0
Size: 483788 Color: 1

Bin 2647: 134 of cap free
Amount of items: 2
Items: 
Size: 548693 Color: 0
Size: 451174 Color: 1

Bin 2648: 134 of cap free
Amount of items: 2
Items: 
Size: 746023 Color: 0
Size: 253844 Color: 1

Bin 2649: 135 of cap free
Amount of items: 2
Items: 
Size: 739913 Color: 1
Size: 259953 Color: 0

Bin 2650: 135 of cap free
Amount of items: 2
Items: 
Size: 521066 Color: 1
Size: 478800 Color: 0

Bin 2651: 135 of cap free
Amount of items: 2
Items: 
Size: 664833 Color: 0
Size: 335033 Color: 1

Bin 2652: 135 of cap free
Amount of items: 2
Items: 
Size: 728513 Color: 0
Size: 271353 Color: 1

Bin 2653: 135 of cap free
Amount of items: 2
Items: 
Size: 741244 Color: 1
Size: 258622 Color: 0

Bin 2654: 135 of cap free
Amount of items: 2
Items: 
Size: 744694 Color: 1
Size: 255172 Color: 0

Bin 2655: 135 of cap free
Amount of items: 2
Items: 
Size: 761132 Color: 0
Size: 238734 Color: 1

Bin 2656: 136 of cap free
Amount of items: 2
Items: 
Size: 512070 Color: 0
Size: 487795 Color: 1

Bin 2657: 136 of cap free
Amount of items: 2
Items: 
Size: 669708 Color: 1
Size: 330157 Color: 0

Bin 2658: 136 of cap free
Amount of items: 2
Items: 
Size: 726668 Color: 0
Size: 273197 Color: 1

Bin 2659: 136 of cap free
Amount of items: 2
Items: 
Size: 532711 Color: 0
Size: 467154 Color: 1

Bin 2660: 136 of cap free
Amount of items: 2
Items: 
Size: 640110 Color: 0
Size: 359755 Color: 1

Bin 2661: 136 of cap free
Amount of items: 2
Items: 
Size: 678879 Color: 0
Size: 320986 Color: 1

Bin 2662: 137 of cap free
Amount of items: 3
Items: 
Size: 638679 Color: 1
Size: 194822 Color: 0
Size: 166363 Color: 1

Bin 2663: 137 of cap free
Amount of items: 2
Items: 
Size: 789264 Color: 1
Size: 210600 Color: 0

Bin 2664: 137 of cap free
Amount of items: 2
Items: 
Size: 510327 Color: 0
Size: 489537 Color: 1

Bin 2665: 137 of cap free
Amount of items: 2
Items: 
Size: 579621 Color: 1
Size: 420243 Color: 0

Bin 2666: 137 of cap free
Amount of items: 2
Items: 
Size: 622635 Color: 1
Size: 377229 Color: 0

Bin 2667: 137 of cap free
Amount of items: 2
Items: 
Size: 639980 Color: 1
Size: 359884 Color: 0

Bin 2668: 137 of cap free
Amount of items: 2
Items: 
Size: 647950 Color: 1
Size: 351914 Color: 0

Bin 2669: 137 of cap free
Amount of items: 2
Items: 
Size: 651493 Color: 1
Size: 348371 Color: 0

Bin 2670: 137 of cap free
Amount of items: 2
Items: 
Size: 679441 Color: 1
Size: 320423 Color: 0

Bin 2671: 138 of cap free
Amount of items: 2
Items: 
Size: 778601 Color: 1
Size: 221262 Color: 0

Bin 2672: 138 of cap free
Amount of items: 2
Items: 
Size: 566911 Color: 1
Size: 432952 Color: 0

Bin 2673: 138 of cap free
Amount of items: 2
Items: 
Size: 591332 Color: 1
Size: 408531 Color: 0

Bin 2674: 138 of cap free
Amount of items: 2
Items: 
Size: 632898 Color: 1
Size: 366965 Color: 0

Bin 2675: 138 of cap free
Amount of items: 2
Items: 
Size: 673017 Color: 1
Size: 326846 Color: 0

Bin 2676: 139 of cap free
Amount of items: 2
Items: 
Size: 703728 Color: 1
Size: 296134 Color: 0

Bin 2677: 139 of cap free
Amount of items: 2
Items: 
Size: 605364 Color: 0
Size: 394498 Color: 1

Bin 2678: 139 of cap free
Amount of items: 2
Items: 
Size: 659801 Color: 1
Size: 340061 Color: 0

Bin 2679: 139 of cap free
Amount of items: 2
Items: 
Size: 760650 Color: 1
Size: 239212 Color: 0

Bin 2680: 140 of cap free
Amount of items: 2
Items: 
Size: 512681 Color: 0
Size: 487180 Color: 1

Bin 2681: 140 of cap free
Amount of items: 2
Items: 
Size: 518506 Color: 1
Size: 481355 Color: 0

Bin 2682: 140 of cap free
Amount of items: 2
Items: 
Size: 533196 Color: 1
Size: 466665 Color: 0

Bin 2683: 140 of cap free
Amount of items: 2
Items: 
Size: 543306 Color: 0
Size: 456555 Color: 1

Bin 2684: 140 of cap free
Amount of items: 2
Items: 
Size: 545552 Color: 1
Size: 454309 Color: 0

Bin 2685: 140 of cap free
Amount of items: 2
Items: 
Size: 549085 Color: 0
Size: 450776 Color: 1

Bin 2686: 140 of cap free
Amount of items: 2
Items: 
Size: 660470 Color: 0
Size: 339391 Color: 1

Bin 2687: 140 of cap free
Amount of items: 2
Items: 
Size: 797406 Color: 0
Size: 202455 Color: 1

Bin 2688: 141 of cap free
Amount of items: 3
Items: 
Size: 415759 Color: 1
Size: 408491 Color: 0
Size: 175610 Color: 0

Bin 2689: 141 of cap free
Amount of items: 2
Items: 
Size: 519132 Color: 0
Size: 480728 Color: 1

Bin 2690: 141 of cap free
Amount of items: 2
Items: 
Size: 675877 Color: 1
Size: 323983 Color: 0

Bin 2691: 141 of cap free
Amount of items: 2
Items: 
Size: 736938 Color: 1
Size: 262922 Color: 0

Bin 2692: 142 of cap free
Amount of items: 2
Items: 
Size: 583500 Color: 0
Size: 416359 Color: 1

Bin 2693: 142 of cap free
Amount of items: 2
Items: 
Size: 621305 Color: 0
Size: 378554 Color: 1

Bin 2694: 142 of cap free
Amount of items: 2
Items: 
Size: 632022 Color: 1
Size: 367837 Color: 0

Bin 2695: 142 of cap free
Amount of items: 2
Items: 
Size: 741884 Color: 0
Size: 257975 Color: 1

Bin 2696: 143 of cap free
Amount of items: 2
Items: 
Size: 513861 Color: 0
Size: 485997 Color: 1

Bin 2697: 143 of cap free
Amount of items: 2
Items: 
Size: 557337 Color: 0
Size: 442521 Color: 1

Bin 2698: 143 of cap free
Amount of items: 2
Items: 
Size: 559460 Color: 1
Size: 440398 Color: 0

Bin 2699: 143 of cap free
Amount of items: 2
Items: 
Size: 654813 Color: 0
Size: 345045 Color: 1

Bin 2700: 143 of cap free
Amount of items: 2
Items: 
Size: 773717 Color: 1
Size: 226141 Color: 0

Bin 2701: 144 of cap free
Amount of items: 2
Items: 
Size: 726576 Color: 1
Size: 273281 Color: 0

Bin 2702: 144 of cap free
Amount of items: 2
Items: 
Size: 783705 Color: 1
Size: 216152 Color: 0

Bin 2703: 144 of cap free
Amount of items: 2
Items: 
Size: 532022 Color: 0
Size: 467835 Color: 1

Bin 2704: 144 of cap free
Amount of items: 2
Items: 
Size: 538364 Color: 0
Size: 461493 Color: 1

Bin 2705: 144 of cap free
Amount of items: 2
Items: 
Size: 568217 Color: 0
Size: 431640 Color: 1

Bin 2706: 144 of cap free
Amount of items: 2
Items: 
Size: 624995 Color: 0
Size: 374862 Color: 1

Bin 2707: 144 of cap free
Amount of items: 2
Items: 
Size: 661891 Color: 0
Size: 337966 Color: 1

Bin 2708: 144 of cap free
Amount of items: 2
Items: 
Size: 708756 Color: 1
Size: 291101 Color: 0

Bin 2709: 144 of cap free
Amount of items: 2
Items: 
Size: 712576 Color: 0
Size: 287281 Color: 1

Bin 2710: 145 of cap free
Amount of items: 2
Items: 
Size: 689560 Color: 1
Size: 310296 Color: 0

Bin 2711: 145 of cap free
Amount of items: 2
Items: 
Size: 524198 Color: 0
Size: 475658 Color: 1

Bin 2712: 145 of cap free
Amount of items: 2
Items: 
Size: 576973 Color: 0
Size: 422883 Color: 1

Bin 2713: 145 of cap free
Amount of items: 2
Items: 
Size: 671455 Color: 0
Size: 328401 Color: 1

Bin 2714: 146 of cap free
Amount of items: 3
Items: 
Size: 483075 Color: 0
Size: 353851 Color: 0
Size: 162929 Color: 1

Bin 2715: 146 of cap free
Amount of items: 2
Items: 
Size: 555710 Color: 0
Size: 444145 Color: 1

Bin 2716: 146 of cap free
Amount of items: 2
Items: 
Size: 596940 Color: 1
Size: 402915 Color: 0

Bin 2717: 146 of cap free
Amount of items: 2
Items: 
Size: 681499 Color: 0
Size: 318356 Color: 1

Bin 2718: 146 of cap free
Amount of items: 2
Items: 
Size: 692497 Color: 1
Size: 307358 Color: 0

Bin 2719: 146 of cap free
Amount of items: 2
Items: 
Size: 759035 Color: 0
Size: 240820 Color: 1

Bin 2720: 147 of cap free
Amount of items: 2
Items: 
Size: 709760 Color: 1
Size: 290094 Color: 0

Bin 2721: 147 of cap free
Amount of items: 3
Items: 
Size: 527597 Color: 0
Size: 315420 Color: 1
Size: 156837 Color: 1

Bin 2722: 147 of cap free
Amount of items: 2
Items: 
Size: 582319 Color: 1
Size: 417535 Color: 0

Bin 2723: 147 of cap free
Amount of items: 2
Items: 
Size: 605325 Color: 1
Size: 394529 Color: 0

Bin 2724: 147 of cap free
Amount of items: 2
Items: 
Size: 777070 Color: 0
Size: 222784 Color: 1

Bin 2725: 148 of cap free
Amount of items: 2
Items: 
Size: 511880 Color: 1
Size: 487973 Color: 0

Bin 2726: 148 of cap free
Amount of items: 2
Items: 
Size: 517262 Color: 0
Size: 482591 Color: 1

Bin 2727: 149 of cap free
Amount of items: 3
Items: 
Size: 597971 Color: 1
Size: 220622 Color: 0
Size: 181259 Color: 0

Bin 2728: 149 of cap free
Amount of items: 2
Items: 
Size: 579263 Color: 1
Size: 420589 Color: 0

Bin 2729: 149 of cap free
Amount of items: 2
Items: 
Size: 649076 Color: 1
Size: 350776 Color: 0

Bin 2730: 149 of cap free
Amount of items: 2
Items: 
Size: 650271 Color: 0
Size: 349581 Color: 1

Bin 2731: 149 of cap free
Amount of items: 2
Items: 
Size: 653557 Color: 1
Size: 346295 Color: 0

Bin 2732: 149 of cap free
Amount of items: 2
Items: 
Size: 667894 Color: 1
Size: 331958 Color: 0

Bin 2733: 149 of cap free
Amount of items: 2
Items: 
Size: 684940 Color: 0
Size: 314912 Color: 1

Bin 2734: 149 of cap free
Amount of items: 2
Items: 
Size: 769300 Color: 1
Size: 230552 Color: 0

Bin 2735: 150 of cap free
Amount of items: 2
Items: 
Size: 550915 Color: 0
Size: 448936 Color: 1

Bin 2736: 150 of cap free
Amount of items: 2
Items: 
Size: 573542 Color: 1
Size: 426309 Color: 0

Bin 2737: 150 of cap free
Amount of items: 2
Items: 
Size: 596515 Color: 1
Size: 403336 Color: 0

Bin 2738: 151 of cap free
Amount of items: 2
Items: 
Size: 576023 Color: 1
Size: 423827 Color: 0

Bin 2739: 151 of cap free
Amount of items: 2
Items: 
Size: 698397 Color: 0
Size: 301453 Color: 1

Bin 2740: 152 of cap free
Amount of items: 2
Items: 
Size: 727680 Color: 0
Size: 272169 Color: 1

Bin 2741: 152 of cap free
Amount of items: 2
Items: 
Size: 548402 Color: 0
Size: 451447 Color: 1

Bin 2742: 152 of cap free
Amount of items: 2
Items: 
Size: 650624 Color: 0
Size: 349225 Color: 1

Bin 2743: 152 of cap free
Amount of items: 2
Items: 
Size: 766946 Color: 0
Size: 232903 Color: 1

Bin 2744: 152 of cap free
Amount of items: 2
Items: 
Size: 786907 Color: 1
Size: 212942 Color: 0

Bin 2745: 153 of cap free
Amount of items: 2
Items: 
Size: 617156 Color: 0
Size: 382692 Color: 1

Bin 2746: 153 of cap free
Amount of items: 2
Items: 
Size: 674250 Color: 0
Size: 325598 Color: 1

Bin 2747: 153 of cap free
Amount of items: 5
Items: 
Size: 268037 Color: 1
Size: 188698 Color: 1
Size: 185650 Color: 0
Size: 185609 Color: 0
Size: 171854 Color: 1

Bin 2748: 153 of cap free
Amount of items: 2
Items: 
Size: 504095 Color: 0
Size: 495753 Color: 1

Bin 2749: 153 of cap free
Amount of items: 2
Items: 
Size: 528739 Color: 0
Size: 471109 Color: 1

Bin 2750: 153 of cap free
Amount of items: 2
Items: 
Size: 552922 Color: 1
Size: 446926 Color: 0

Bin 2751: 153 of cap free
Amount of items: 2
Items: 
Size: 681924 Color: 0
Size: 317924 Color: 1

Bin 2752: 154 of cap free
Amount of items: 3
Items: 
Size: 749000 Color: 1
Size: 130692 Color: 0
Size: 120155 Color: 1

Bin 2753: 154 of cap free
Amount of items: 2
Items: 
Size: 638653 Color: 1
Size: 361194 Color: 0

Bin 2754: 154 of cap free
Amount of items: 2
Items: 
Size: 524539 Color: 1
Size: 475308 Color: 0

Bin 2755: 154 of cap free
Amount of items: 2
Items: 
Size: 529168 Color: 1
Size: 470679 Color: 0

Bin 2756: 154 of cap free
Amount of items: 2
Items: 
Size: 619676 Color: 0
Size: 380171 Color: 1

Bin 2757: 154 of cap free
Amount of items: 2
Items: 
Size: 620512 Color: 0
Size: 379335 Color: 1

Bin 2758: 154 of cap free
Amount of items: 2
Items: 
Size: 630601 Color: 0
Size: 369246 Color: 1

Bin 2759: 154 of cap free
Amount of items: 2
Items: 
Size: 661554 Color: 0
Size: 338293 Color: 1

Bin 2760: 154 of cap free
Amount of items: 2
Items: 
Size: 673455 Color: 1
Size: 326392 Color: 0

Bin 2761: 154 of cap free
Amount of items: 2
Items: 
Size: 677245 Color: 0
Size: 322602 Color: 1

Bin 2762: 154 of cap free
Amount of items: 2
Items: 
Size: 695323 Color: 1
Size: 304524 Color: 0

Bin 2763: 154 of cap free
Amount of items: 2
Items: 
Size: 722609 Color: 1
Size: 277238 Color: 0

Bin 2764: 155 of cap free
Amount of items: 2
Items: 
Size: 794757 Color: 1
Size: 205089 Color: 0

Bin 2765: 155 of cap free
Amount of items: 2
Items: 
Size: 760870 Color: 1
Size: 238976 Color: 0

Bin 2766: 155 of cap free
Amount of items: 2
Items: 
Size: 768831 Color: 1
Size: 231015 Color: 0

Bin 2767: 155 of cap free
Amount of items: 2
Items: 
Size: 533861 Color: 0
Size: 465985 Color: 1

Bin 2768: 155 of cap free
Amount of items: 2
Items: 
Size: 543620 Color: 1
Size: 456226 Color: 0

Bin 2769: 155 of cap free
Amount of items: 2
Items: 
Size: 666759 Color: 1
Size: 333087 Color: 0

Bin 2770: 155 of cap free
Amount of items: 2
Items: 
Size: 742439 Color: 0
Size: 257407 Color: 1

Bin 2771: 156 of cap free
Amount of items: 2
Items: 
Size: 640169 Color: 1
Size: 359676 Color: 0

Bin 2772: 157 of cap free
Amount of items: 2
Items: 
Size: 709517 Color: 0
Size: 290327 Color: 1

Bin 2773: 157 of cap free
Amount of items: 3
Items: 
Size: 361474 Color: 0
Size: 343870 Color: 1
Size: 294500 Color: 1

Bin 2774: 157 of cap free
Amount of items: 2
Items: 
Size: 508058 Color: 0
Size: 491786 Color: 1

Bin 2775: 157 of cap free
Amount of items: 2
Items: 
Size: 654137 Color: 0
Size: 345707 Color: 1

Bin 2776: 157 of cap free
Amount of items: 2
Items: 
Size: 756610 Color: 1
Size: 243234 Color: 0

Bin 2777: 157 of cap free
Amount of items: 2
Items: 
Size: 767608 Color: 1
Size: 232236 Color: 0

Bin 2778: 157 of cap free
Amount of items: 2
Items: 
Size: 793664 Color: 1
Size: 206180 Color: 0

Bin 2779: 158 of cap free
Amount of items: 2
Items: 
Size: 787157 Color: 1
Size: 212686 Color: 0

Bin 2780: 158 of cap free
Amount of items: 2
Items: 
Size: 553625 Color: 1
Size: 446218 Color: 0

Bin 2781: 158 of cap free
Amount of items: 2
Items: 
Size: 718896 Color: 1
Size: 280947 Color: 0

Bin 2782: 158 of cap free
Amount of items: 2
Items: 
Size: 740871 Color: 0
Size: 258972 Color: 1

Bin 2783: 158 of cap free
Amount of items: 2
Items: 
Size: 750365 Color: 0
Size: 249478 Color: 1

Bin 2784: 159 of cap free
Amount of items: 2
Items: 
Size: 677883 Color: 1
Size: 321959 Color: 0

Bin 2785: 159 of cap free
Amount of items: 2
Items: 
Size: 797684 Color: 1
Size: 202158 Color: 0

Bin 2786: 159 of cap free
Amount of items: 2
Items: 
Size: 512650 Color: 1
Size: 487192 Color: 0

Bin 2787: 159 of cap free
Amount of items: 2
Items: 
Size: 614066 Color: 0
Size: 385776 Color: 1

Bin 2788: 159 of cap free
Amount of items: 2
Items: 
Size: 642830 Color: 0
Size: 357012 Color: 1

Bin 2789: 159 of cap free
Amount of items: 2
Items: 
Size: 690993 Color: 1
Size: 308849 Color: 0

Bin 2790: 159 of cap free
Amount of items: 2
Items: 
Size: 713759 Color: 0
Size: 286083 Color: 1

Bin 2791: 159 of cap free
Amount of items: 2
Items: 
Size: 718689 Color: 1
Size: 281153 Color: 0

Bin 2792: 160 of cap free
Amount of items: 7
Items: 
Size: 174148 Color: 1
Size: 151705 Color: 0
Size: 141912 Color: 1
Size: 140182 Color: 1
Size: 139615 Color: 0
Size: 137031 Color: 1
Size: 115248 Color: 0

Bin 2793: 160 of cap free
Amount of items: 2
Items: 
Size: 520038 Color: 0
Size: 479803 Color: 1

Bin 2794: 160 of cap free
Amount of items: 2
Items: 
Size: 570636 Color: 0
Size: 429205 Color: 1

Bin 2795: 160 of cap free
Amount of items: 2
Items: 
Size: 688550 Color: 1
Size: 311291 Color: 0

Bin 2796: 160 of cap free
Amount of items: 2
Items: 
Size: 689203 Color: 1
Size: 310638 Color: 0

Bin 2797: 161 of cap free
Amount of items: 2
Items: 
Size: 503847 Color: 0
Size: 495993 Color: 1

Bin 2798: 161 of cap free
Amount of items: 2
Items: 
Size: 526109 Color: 1
Size: 473731 Color: 0

Bin 2799: 161 of cap free
Amount of items: 2
Items: 
Size: 573386 Color: 0
Size: 426454 Color: 1

Bin 2800: 162 of cap free
Amount of items: 2
Items: 
Size: 609100 Color: 0
Size: 390739 Color: 1

Bin 2801: 162 of cap free
Amount of items: 2
Items: 
Size: 649457 Color: 0
Size: 350382 Color: 1

Bin 2802: 162 of cap free
Amount of items: 2
Items: 
Size: 662641 Color: 0
Size: 337198 Color: 1

Bin 2803: 162 of cap free
Amount of items: 2
Items: 
Size: 695983 Color: 0
Size: 303856 Color: 1

Bin 2804: 163 of cap free
Amount of items: 2
Items: 
Size: 622465 Color: 0
Size: 377373 Color: 1

Bin 2805: 163 of cap free
Amount of items: 2
Items: 
Size: 525434 Color: 1
Size: 474404 Color: 0

Bin 2806: 163 of cap free
Amount of items: 2
Items: 
Size: 795086 Color: 0
Size: 204752 Color: 1

Bin 2807: 164 of cap free
Amount of items: 2
Items: 
Size: 646151 Color: 1
Size: 353686 Color: 0

Bin 2808: 164 of cap free
Amount of items: 2
Items: 
Size: 520771 Color: 1
Size: 479066 Color: 0

Bin 2809: 164 of cap free
Amount of items: 2
Items: 
Size: 522801 Color: 0
Size: 477036 Color: 1

Bin 2810: 164 of cap free
Amount of items: 2
Items: 
Size: 559719 Color: 0
Size: 440118 Color: 1

Bin 2811: 164 of cap free
Amount of items: 2
Items: 
Size: 577813 Color: 0
Size: 422024 Color: 1

Bin 2812: 164 of cap free
Amount of items: 2
Items: 
Size: 624197 Color: 0
Size: 375640 Color: 1

Bin 2813: 164 of cap free
Amount of items: 2
Items: 
Size: 651018 Color: 1
Size: 348819 Color: 0

Bin 2814: 164 of cap free
Amount of items: 2
Items: 
Size: 666753 Color: 0
Size: 333084 Color: 1

Bin 2815: 164 of cap free
Amount of items: 2
Items: 
Size: 787911 Color: 0
Size: 211926 Color: 1

Bin 2816: 165 of cap free
Amount of items: 2
Items: 
Size: 718669 Color: 0
Size: 281167 Color: 1

Bin 2817: 165 of cap free
Amount of items: 2
Items: 
Size: 549272 Color: 1
Size: 450564 Color: 0

Bin 2818: 165 of cap free
Amount of items: 2
Items: 
Size: 776412 Color: 0
Size: 223424 Color: 1

Bin 2819: 165 of cap free
Amount of items: 2
Items: 
Size: 785813 Color: 1
Size: 214023 Color: 0

Bin 2820: 166 of cap free
Amount of items: 2
Items: 
Size: 523921 Color: 0
Size: 475914 Color: 1

Bin 2821: 166 of cap free
Amount of items: 2
Items: 
Size: 747961 Color: 1
Size: 251874 Color: 0

Bin 2822: 167 of cap free
Amount of items: 2
Items: 
Size: 633965 Color: 1
Size: 365869 Color: 0

Bin 2823: 167 of cap free
Amount of items: 2
Items: 
Size: 563700 Color: 1
Size: 436134 Color: 0

Bin 2824: 167 of cap free
Amount of items: 2
Items: 
Size: 716879 Color: 1
Size: 282955 Color: 0

Bin 2825: 168 of cap free
Amount of items: 2
Items: 
Size: 685524 Color: 1
Size: 314309 Color: 0

Bin 2826: 168 of cap free
Amount of items: 2
Items: 
Size: 584485 Color: 0
Size: 415348 Color: 1

Bin 2827: 168 of cap free
Amount of items: 2
Items: 
Size: 501916 Color: 1
Size: 497917 Color: 0

Bin 2828: 168 of cap free
Amount of items: 2
Items: 
Size: 565298 Color: 0
Size: 434535 Color: 1

Bin 2829: 168 of cap free
Amount of items: 2
Items: 
Size: 796060 Color: 1
Size: 203773 Color: 0

Bin 2830: 169 of cap free
Amount of items: 2
Items: 
Size: 755475 Color: 1
Size: 244357 Color: 0

Bin 2831: 169 of cap free
Amount of items: 2
Items: 
Size: 739725 Color: 0
Size: 260107 Color: 1

Bin 2832: 169 of cap free
Amount of items: 2
Items: 
Size: 579954 Color: 1
Size: 419878 Color: 0

Bin 2833: 169 of cap free
Amount of items: 2
Items: 
Size: 619191 Color: 0
Size: 380641 Color: 1

Bin 2834: 169 of cap free
Amount of items: 2
Items: 
Size: 658827 Color: 0
Size: 341005 Color: 1

Bin 2835: 169 of cap free
Amount of items: 2
Items: 
Size: 748615 Color: 1
Size: 251217 Color: 0

Bin 2836: 170 of cap free
Amount of items: 2
Items: 
Size: 708064 Color: 1
Size: 291767 Color: 0

Bin 2837: 170 of cap free
Amount of items: 2
Items: 
Size: 544986 Color: 1
Size: 454845 Color: 0

Bin 2838: 170 of cap free
Amount of items: 2
Items: 
Size: 648286 Color: 1
Size: 351545 Color: 0

Bin 2839: 170 of cap free
Amount of items: 2
Items: 
Size: 698882 Color: 1
Size: 300949 Color: 0

Bin 2840: 170 of cap free
Amount of items: 2
Items: 
Size: 790219 Color: 1
Size: 209612 Color: 0

Bin 2841: 171 of cap free
Amount of items: 2
Items: 
Size: 783880 Color: 0
Size: 215950 Color: 1

Bin 2842: 171 of cap free
Amount of items: 2
Items: 
Size: 601494 Color: 0
Size: 398336 Color: 1

Bin 2843: 171 of cap free
Amount of items: 2
Items: 
Size: 523502 Color: 0
Size: 476328 Color: 1

Bin 2844: 171 of cap free
Amount of items: 2
Items: 
Size: 560985 Color: 1
Size: 438845 Color: 0

Bin 2845: 171 of cap free
Amount of items: 2
Items: 
Size: 607768 Color: 0
Size: 392062 Color: 1

Bin 2846: 171 of cap free
Amount of items: 2
Items: 
Size: 631983 Color: 0
Size: 367847 Color: 1

Bin 2847: 171 of cap free
Amount of items: 2
Items: 
Size: 690365 Color: 1
Size: 309465 Color: 0

Bin 2848: 171 of cap free
Amount of items: 2
Items: 
Size: 763343 Color: 1
Size: 236487 Color: 0

Bin 2849: 171 of cap free
Amount of items: 2
Items: 
Size: 772687 Color: 0
Size: 227143 Color: 1

Bin 2850: 172 of cap free
Amount of items: 2
Items: 
Size: 537047 Color: 0
Size: 462782 Color: 1

Bin 2851: 172 of cap free
Amount of items: 2
Items: 
Size: 691573 Color: 1
Size: 308256 Color: 0

Bin 2852: 172 of cap free
Amount of items: 2
Items: 
Size: 722968 Color: 0
Size: 276861 Color: 1

Bin 2853: 173 of cap free
Amount of items: 2
Items: 
Size: 698018 Color: 1
Size: 301810 Color: 0

Bin 2854: 173 of cap free
Amount of items: 2
Items: 
Size: 690451 Color: 0
Size: 309377 Color: 1

Bin 2855: 173 of cap free
Amount of items: 2
Items: 
Size: 794997 Color: 1
Size: 204831 Color: 0

Bin 2856: 174 of cap free
Amount of items: 2
Items: 
Size: 708330 Color: 1
Size: 291497 Color: 0

Bin 2857: 174 of cap free
Amount of items: 2
Items: 
Size: 710001 Color: 1
Size: 289826 Color: 0

Bin 2858: 174 of cap free
Amount of items: 2
Items: 
Size: 610967 Color: 0
Size: 388860 Color: 1

Bin 2859: 174 of cap free
Amount of items: 2
Items: 
Size: 521355 Color: 1
Size: 478472 Color: 0

Bin 2860: 174 of cap free
Amount of items: 2
Items: 
Size: 529152 Color: 1
Size: 470675 Color: 0

Bin 2861: 174 of cap free
Amount of items: 2
Items: 
Size: 580108 Color: 0
Size: 419719 Color: 1

Bin 2862: 174 of cap free
Amount of items: 2
Items: 
Size: 681259 Color: 0
Size: 318568 Color: 1

Bin 2863: 175 of cap free
Amount of items: 2
Items: 
Size: 665773 Color: 1
Size: 334053 Color: 0

Bin 2864: 175 of cap free
Amount of items: 2
Items: 
Size: 731007 Color: 1
Size: 268819 Color: 0

Bin 2865: 176 of cap free
Amount of items: 2
Items: 
Size: 798621 Color: 1
Size: 201204 Color: 0

Bin 2866: 177 of cap free
Amount of items: 2
Items: 
Size: 720245 Color: 1
Size: 279579 Color: 0

Bin 2867: 177 of cap free
Amount of items: 2
Items: 
Size: 701390 Color: 0
Size: 298434 Color: 1

Bin 2868: 177 of cap free
Amount of items: 2
Items: 
Size: 704777 Color: 1
Size: 295047 Color: 0

Bin 2869: 177 of cap free
Amount of items: 2
Items: 
Size: 731416 Color: 0
Size: 268408 Color: 1

Bin 2870: 177 of cap free
Amount of items: 2
Items: 
Size: 734076 Color: 0
Size: 265748 Color: 1

Bin 2871: 177 of cap free
Amount of items: 2
Items: 
Size: 734355 Color: 0
Size: 265469 Color: 1

Bin 2872: 178 of cap free
Amount of items: 2
Items: 
Size: 631294 Color: 1
Size: 368529 Color: 0

Bin 2873: 178 of cap free
Amount of items: 2
Items: 
Size: 730855 Color: 0
Size: 268968 Color: 1

Bin 2874: 178 of cap free
Amount of items: 2
Items: 
Size: 706735 Color: 1
Size: 293088 Color: 0

Bin 2875: 178 of cap free
Amount of items: 2
Items: 
Size: 535479 Color: 1
Size: 464344 Color: 0

Bin 2876: 178 of cap free
Amount of items: 2
Items: 
Size: 540093 Color: 0
Size: 459730 Color: 1

Bin 2877: 178 of cap free
Amount of items: 2
Items: 
Size: 557155 Color: 1
Size: 442668 Color: 0

Bin 2878: 178 of cap free
Amount of items: 2
Items: 
Size: 603377 Color: 0
Size: 396446 Color: 1

Bin 2879: 178 of cap free
Amount of items: 2
Items: 
Size: 614047 Color: 0
Size: 385776 Color: 1

Bin 2880: 178 of cap free
Amount of items: 2
Items: 
Size: 787525 Color: 1
Size: 212298 Color: 0

Bin 2881: 179 of cap free
Amount of items: 3
Items: 
Size: 565608 Color: 1
Size: 296107 Color: 1
Size: 138107 Color: 0

Bin 2882: 179 of cap free
Amount of items: 2
Items: 
Size: 529681 Color: 1
Size: 470141 Color: 0

Bin 2883: 179 of cap free
Amount of items: 2
Items: 
Size: 505057 Color: 0
Size: 494765 Color: 1

Bin 2884: 180 of cap free
Amount of items: 2
Items: 
Size: 771897 Color: 1
Size: 227924 Color: 0

Bin 2885: 180 of cap free
Amount of items: 2
Items: 
Size: 714025 Color: 1
Size: 285796 Color: 0

Bin 2886: 180 of cap free
Amount of items: 2
Items: 
Size: 548867 Color: 0
Size: 450954 Color: 1

Bin 2887: 180 of cap free
Amount of items: 2
Items: 
Size: 582146 Color: 0
Size: 417675 Color: 1

Bin 2888: 180 of cap free
Amount of items: 2
Items: 
Size: 586906 Color: 1
Size: 412915 Color: 0

Bin 2889: 180 of cap free
Amount of items: 2
Items: 
Size: 589312 Color: 0
Size: 410509 Color: 1

Bin 2890: 180 of cap free
Amount of items: 2
Items: 
Size: 591871 Color: 1
Size: 407950 Color: 0

Bin 2891: 180 of cap free
Amount of items: 2
Items: 
Size: 680248 Color: 0
Size: 319573 Color: 1

Bin 2892: 180 of cap free
Amount of items: 2
Items: 
Size: 740850 Color: 0
Size: 258971 Color: 1

Bin 2893: 180 of cap free
Amount of items: 2
Items: 
Size: 758625 Color: 1
Size: 241196 Color: 0

Bin 2894: 180 of cap free
Amount of items: 2
Items: 
Size: 775623 Color: 0
Size: 224198 Color: 1

Bin 2895: 181 of cap free
Amount of items: 2
Items: 
Size: 541986 Color: 0
Size: 457834 Color: 1

Bin 2896: 181 of cap free
Amount of items: 2
Items: 
Size: 636238 Color: 0
Size: 363582 Color: 1

Bin 2897: 182 of cap free
Amount of items: 2
Items: 
Size: 618917 Color: 1
Size: 380902 Color: 0

Bin 2898: 182 of cap free
Amount of items: 2
Items: 
Size: 582733 Color: 0
Size: 417086 Color: 1

Bin 2899: 182 of cap free
Amount of items: 2
Items: 
Size: 737688 Color: 0
Size: 262131 Color: 1

Bin 2900: 182 of cap free
Amount of items: 2
Items: 
Size: 752495 Color: 0
Size: 247324 Color: 1

Bin 2901: 183 of cap free
Amount of items: 2
Items: 
Size: 667869 Color: 1
Size: 331949 Color: 0

Bin 2902: 184 of cap free
Amount of items: 2
Items: 
Size: 630953 Color: 1
Size: 368864 Color: 0

Bin 2903: 185 of cap free
Amount of items: 2
Items: 
Size: 736535 Color: 0
Size: 263281 Color: 1

Bin 2904: 185 of cap free
Amount of items: 2
Items: 
Size: 676998 Color: 1
Size: 322818 Color: 0

Bin 2905: 185 of cap free
Amount of items: 2
Items: 
Size: 704749 Color: 0
Size: 295067 Color: 1

Bin 2906: 185 of cap free
Amount of items: 2
Items: 
Size: 724794 Color: 0
Size: 275022 Color: 1

Bin 2907: 186 of cap free
Amount of items: 2
Items: 
Size: 616332 Color: 0
Size: 383483 Color: 1

Bin 2908: 186 of cap free
Amount of items: 2
Items: 
Size: 549252 Color: 1
Size: 450563 Color: 0

Bin 2909: 186 of cap free
Amount of items: 2
Items: 
Size: 556425 Color: 0
Size: 443390 Color: 1

Bin 2910: 186 of cap free
Amount of items: 2
Items: 
Size: 602921 Color: 1
Size: 396894 Color: 0

Bin 2911: 186 of cap free
Amount of items: 2
Items: 
Size: 738615 Color: 1
Size: 261200 Color: 0

Bin 2912: 186 of cap free
Amount of items: 2
Items: 
Size: 771822 Color: 0
Size: 227993 Color: 1

Bin 2913: 187 of cap free
Amount of items: 2
Items: 
Size: 500952 Color: 1
Size: 498862 Color: 0

Bin 2914: 187 of cap free
Amount of items: 2
Items: 
Size: 556145 Color: 0
Size: 443669 Color: 1

Bin 2915: 187 of cap free
Amount of items: 2
Items: 
Size: 580780 Color: 0
Size: 419034 Color: 1

Bin 2916: 187 of cap free
Amount of items: 2
Items: 
Size: 666365 Color: 0
Size: 333449 Color: 1

Bin 2917: 187 of cap free
Amount of items: 2
Items: 
Size: 682261 Color: 1
Size: 317553 Color: 0

Bin 2918: 188 of cap free
Amount of items: 2
Items: 
Size: 547248 Color: 0
Size: 452565 Color: 1

Bin 2919: 188 of cap free
Amount of items: 2
Items: 
Size: 557882 Color: 1
Size: 441931 Color: 0

Bin 2920: 189 of cap free
Amount of items: 2
Items: 
Size: 694905 Color: 1
Size: 304907 Color: 0

Bin 2921: 189 of cap free
Amount of items: 2
Items: 
Size: 743600 Color: 1
Size: 256212 Color: 0

Bin 2922: 189 of cap free
Amount of items: 2
Items: 
Size: 758149 Color: 1
Size: 241663 Color: 0

Bin 2923: 189 of cap free
Amount of items: 2
Items: 
Size: 791224 Color: 0
Size: 208588 Color: 1

Bin 2924: 190 of cap free
Amount of items: 2
Items: 
Size: 515496 Color: 0
Size: 484315 Color: 1

Bin 2925: 190 of cap free
Amount of items: 2
Items: 
Size: 599144 Color: 1
Size: 400667 Color: 0

Bin 2926: 190 of cap free
Amount of items: 2
Items: 
Size: 655750 Color: 1
Size: 344061 Color: 0

Bin 2927: 190 of cap free
Amount of items: 2
Items: 
Size: 728794 Color: 0
Size: 271017 Color: 1

Bin 2928: 190 of cap free
Amount of items: 2
Items: 
Size: 795332 Color: 0
Size: 204479 Color: 1

Bin 2929: 191 of cap free
Amount of items: 2
Items: 
Size: 571554 Color: 1
Size: 428256 Color: 0

Bin 2930: 191 of cap free
Amount of items: 3
Items: 
Size: 434586 Color: 0
Size: 393171 Color: 1
Size: 172053 Color: 1

Bin 2931: 192 of cap free
Amount of items: 2
Items: 
Size: 610571 Color: 1
Size: 389238 Color: 0

Bin 2932: 192 of cap free
Amount of items: 2
Items: 
Size: 773366 Color: 0
Size: 226443 Color: 1

Bin 2933: 193 of cap free
Amount of items: 2
Items: 
Size: 566029 Color: 0
Size: 433779 Color: 1

Bin 2934: 193 of cap free
Amount of items: 3
Items: 
Size: 636016 Color: 0
Size: 248726 Color: 0
Size: 115066 Color: 1

Bin 2935: 193 of cap free
Amount of items: 2
Items: 
Size: 600357 Color: 1
Size: 399451 Color: 0

Bin 2936: 193 of cap free
Amount of items: 2
Items: 
Size: 661531 Color: 0
Size: 338277 Color: 1

Bin 2937: 193 of cap free
Amount of items: 2
Items: 
Size: 683289 Color: 0
Size: 316519 Color: 1

Bin 2938: 193 of cap free
Amount of items: 2
Items: 
Size: 714848 Color: 0
Size: 284960 Color: 1

Bin 2939: 194 of cap free
Amount of items: 2
Items: 
Size: 706298 Color: 1
Size: 293509 Color: 0

Bin 2940: 194 of cap free
Amount of items: 2
Items: 
Size: 532309 Color: 1
Size: 467498 Color: 0

Bin 2941: 194 of cap free
Amount of items: 2
Items: 
Size: 579609 Color: 1
Size: 420198 Color: 0

Bin 2942: 194 of cap free
Amount of items: 2
Items: 
Size: 694786 Color: 0
Size: 305021 Color: 1

Bin 2943: 195 of cap free
Amount of items: 2
Items: 
Size: 591825 Color: 0
Size: 407981 Color: 1

Bin 2944: 195 of cap free
Amount of items: 2
Items: 
Size: 630836 Color: 0
Size: 368970 Color: 1

Bin 2945: 196 of cap free
Amount of items: 2
Items: 
Size: 732652 Color: 0
Size: 267153 Color: 1

Bin 2946: 196 of cap free
Amount of items: 2
Items: 
Size: 541330 Color: 0
Size: 458475 Color: 1

Bin 2947: 196 of cap free
Amount of items: 2
Items: 
Size: 679987 Color: 1
Size: 319818 Color: 0

Bin 2948: 196 of cap free
Amount of items: 2
Items: 
Size: 777379 Color: 1
Size: 222426 Color: 0

Bin 2949: 197 of cap free
Amount of items: 3
Items: 
Size: 736070 Color: 0
Size: 154815 Color: 0
Size: 108919 Color: 1

Bin 2950: 197 of cap free
Amount of items: 2
Items: 
Size: 509420 Color: 0
Size: 490384 Color: 1

Bin 2951: 197 of cap free
Amount of items: 2
Items: 
Size: 623181 Color: 0
Size: 376623 Color: 1

Bin 2952: 197 of cap free
Amount of items: 2
Items: 
Size: 688898 Color: 1
Size: 310906 Color: 0

Bin 2953: 198 of cap free
Amount of items: 2
Items: 
Size: 780111 Color: 1
Size: 219692 Color: 0

Bin 2954: 198 of cap free
Amount of items: 3
Items: 
Size: 352940 Color: 1
Size: 346611 Color: 0
Size: 300252 Color: 0

Bin 2955: 198 of cap free
Amount of items: 2
Items: 
Size: 754980 Color: 0
Size: 244823 Color: 1

Bin 2956: 198 of cap free
Amount of items: 2
Items: 
Size: 515042 Color: 0
Size: 484761 Color: 1

Bin 2957: 198 of cap free
Amount of items: 2
Items: 
Size: 695975 Color: 0
Size: 303828 Color: 1

Bin 2958: 198 of cap free
Amount of items: 2
Items: 
Size: 699270 Color: 0
Size: 300533 Color: 1

Bin 2959: 199 of cap free
Amount of items: 2
Items: 
Size: 562814 Color: 0
Size: 436988 Color: 1

Bin 2960: 199 of cap free
Amount of items: 2
Items: 
Size: 540914 Color: 0
Size: 458888 Color: 1

Bin 2961: 199 of cap free
Amount of items: 2
Items: 
Size: 568620 Color: 1
Size: 431182 Color: 0

Bin 2962: 199 of cap free
Amount of items: 2
Items: 
Size: 735640 Color: 0
Size: 264162 Color: 1

Bin 2963: 200 of cap free
Amount of items: 2
Items: 
Size: 600971 Color: 1
Size: 398830 Color: 0

Bin 2964: 200 of cap free
Amount of items: 2
Items: 
Size: 719793 Color: 0
Size: 280008 Color: 1

Bin 2965: 200 of cap free
Amount of items: 2
Items: 
Size: 510555 Color: 0
Size: 489246 Color: 1

Bin 2966: 200 of cap free
Amount of items: 2
Items: 
Size: 571867 Color: 0
Size: 427934 Color: 1

Bin 2967: 200 of cap free
Amount of items: 2
Items: 
Size: 607295 Color: 1
Size: 392506 Color: 0

Bin 2968: 200 of cap free
Amount of items: 2
Items: 
Size: 677223 Color: 0
Size: 322578 Color: 1

Bin 2969: 201 of cap free
Amount of items: 2
Items: 
Size: 765341 Color: 0
Size: 234459 Color: 1

Bin 2970: 201 of cap free
Amount of items: 2
Items: 
Size: 794987 Color: 1
Size: 204813 Color: 0

Bin 2971: 201 of cap free
Amount of items: 2
Items: 
Size: 509926 Color: 1
Size: 489874 Color: 0

Bin 2972: 201 of cap free
Amount of items: 2
Items: 
Size: 590146 Color: 1
Size: 409654 Color: 0

Bin 2973: 201 of cap free
Amount of items: 2
Items: 
Size: 731460 Color: 1
Size: 268340 Color: 0

Bin 2974: 202 of cap free
Amount of items: 2
Items: 
Size: 626733 Color: 1
Size: 373066 Color: 0

Bin 2975: 202 of cap free
Amount of items: 2
Items: 
Size: 633013 Color: 0
Size: 366786 Color: 1

Bin 2976: 202 of cap free
Amount of items: 2
Items: 
Size: 716762 Color: 0
Size: 283037 Color: 1

Bin 2977: 203 of cap free
Amount of items: 2
Items: 
Size: 673263 Color: 0
Size: 326535 Color: 1

Bin 2978: 203 of cap free
Amount of items: 2
Items: 
Size: 612592 Color: 0
Size: 387206 Color: 1

Bin 2979: 203 of cap free
Amount of items: 2
Items: 
Size: 664532 Color: 1
Size: 335266 Color: 0

Bin 2980: 203 of cap free
Amount of items: 2
Items: 
Size: 753666 Color: 1
Size: 246132 Color: 0

Bin 2981: 203 of cap free
Amount of items: 2
Items: 
Size: 760352 Color: 0
Size: 239446 Color: 1

Bin 2982: 204 of cap free
Amount of items: 2
Items: 
Size: 607861 Color: 1
Size: 391936 Color: 0

Bin 2983: 204 of cap free
Amount of items: 2
Items: 
Size: 614462 Color: 0
Size: 385335 Color: 1

Bin 2984: 204 of cap free
Amount of items: 2
Items: 
Size: 665156 Color: 1
Size: 334641 Color: 0

Bin 2985: 205 of cap free
Amount of items: 2
Items: 
Size: 757709 Color: 1
Size: 242087 Color: 0

Bin 2986: 205 of cap free
Amount of items: 2
Items: 
Size: 553105 Color: 1
Size: 446691 Color: 0

Bin 2987: 206 of cap free
Amount of items: 2
Items: 
Size: 712544 Color: 1
Size: 287251 Color: 0

Bin 2988: 206 of cap free
Amount of items: 3
Items: 
Size: 351003 Color: 0
Size: 325335 Color: 1
Size: 323457 Color: 0

Bin 2989: 206 of cap free
Amount of items: 2
Items: 
Size: 560093 Color: 0
Size: 439702 Color: 1

Bin 2990: 206 of cap free
Amount of items: 2
Items: 
Size: 661263 Color: 0
Size: 338532 Color: 1

Bin 2991: 206 of cap free
Amount of items: 2
Items: 
Size: 725161 Color: 0
Size: 274634 Color: 1

Bin 2992: 206 of cap free
Amount of items: 2
Items: 
Size: 737010 Color: 0
Size: 262785 Color: 1

Bin 2993: 207 of cap free
Amount of items: 2
Items: 
Size: 617914 Color: 0
Size: 381880 Color: 1

Bin 2994: 207 of cap free
Amount of items: 3
Items: 
Size: 643095 Color: 1
Size: 185872 Color: 0
Size: 170827 Color: 0

Bin 2995: 207 of cap free
Amount of items: 2
Items: 
Size: 717749 Color: 1
Size: 282045 Color: 0

Bin 2996: 207 of cap free
Amount of items: 2
Items: 
Size: 627158 Color: 0
Size: 372636 Color: 1

Bin 2997: 207 of cap free
Amount of items: 2
Items: 
Size: 659890 Color: 0
Size: 339904 Color: 1

Bin 2998: 207 of cap free
Amount of items: 2
Items: 
Size: 660467 Color: 1
Size: 339327 Color: 0

Bin 2999: 208 of cap free
Amount of items: 2
Items: 
Size: 599169 Color: 0
Size: 400624 Color: 1

Bin 3000: 208 of cap free
Amount of items: 2
Items: 
Size: 625950 Color: 1
Size: 373843 Color: 0

Bin 3001: 208 of cap free
Amount of items: 2
Items: 
Size: 641546 Color: 1
Size: 358247 Color: 0

Bin 3002: 209 of cap free
Amount of items: 2
Items: 
Size: 750030 Color: 0
Size: 249762 Color: 1

Bin 3003: 209 of cap free
Amount of items: 2
Items: 
Size: 549586 Color: 0
Size: 450206 Color: 1

Bin 3004: 209 of cap free
Amount of items: 2
Items: 
Size: 516010 Color: 0
Size: 483782 Color: 1

Bin 3005: 209 of cap free
Amount of items: 2
Items: 
Size: 574777 Color: 1
Size: 425015 Color: 0

Bin 3006: 209 of cap free
Amount of items: 2
Items: 
Size: 695820 Color: 1
Size: 303972 Color: 0

Bin 3007: 210 of cap free
Amount of items: 2
Items: 
Size: 590321 Color: 0
Size: 409470 Color: 1

Bin 3008: 210 of cap free
Amount of items: 2
Items: 
Size: 703659 Color: 1
Size: 296132 Color: 0

Bin 3009: 210 of cap free
Amount of items: 2
Items: 
Size: 775203 Color: 0
Size: 224588 Color: 1

Bin 3010: 211 of cap free
Amount of items: 2
Items: 
Size: 774442 Color: 0
Size: 225348 Color: 1

Bin 3011: 212 of cap free
Amount of items: 2
Items: 
Size: 672645 Color: 0
Size: 327144 Color: 1

Bin 3012: 212 of cap free
Amount of items: 2
Items: 
Size: 748980 Color: 0
Size: 250809 Color: 1

Bin 3013: 212 of cap free
Amount of items: 2
Items: 
Size: 562456 Color: 0
Size: 437333 Color: 1

Bin 3014: 212 of cap free
Amount of items: 2
Items: 
Size: 600023 Color: 0
Size: 399766 Color: 1

Bin 3015: 213 of cap free
Amount of items: 3
Items: 
Size: 706103 Color: 1
Size: 180981 Color: 1
Size: 112704 Color: 0

Bin 3016: 213 of cap free
Amount of items: 2
Items: 
Size: 563294 Color: 0
Size: 436494 Color: 1

Bin 3017: 213 of cap free
Amount of items: 2
Items: 
Size: 528081 Color: 0
Size: 471707 Color: 1

Bin 3018: 213 of cap free
Amount of items: 2
Items: 
Size: 612565 Color: 1
Size: 387223 Color: 0

Bin 3019: 213 of cap free
Amount of items: 2
Items: 
Size: 613953 Color: 1
Size: 385835 Color: 0

Bin 3020: 213 of cap free
Amount of items: 2
Items: 
Size: 632403 Color: 1
Size: 367385 Color: 0

Bin 3021: 214 of cap free
Amount of items: 2
Items: 
Size: 687183 Color: 1
Size: 312604 Color: 0

Bin 3022: 214 of cap free
Amount of items: 2
Items: 
Size: 504401 Color: 1
Size: 495386 Color: 0

Bin 3023: 214 of cap free
Amount of items: 2
Items: 
Size: 539685 Color: 1
Size: 460102 Color: 0

Bin 3024: 215 of cap free
Amount of items: 2
Items: 
Size: 613489 Color: 0
Size: 386297 Color: 1

Bin 3025: 215 of cap free
Amount of items: 2
Items: 
Size: 664101 Color: 0
Size: 335685 Color: 1

Bin 3026: 215 of cap free
Amount of items: 2
Items: 
Size: 537015 Color: 0
Size: 462771 Color: 1

Bin 3027: 215 of cap free
Amount of items: 2
Items: 
Size: 678644 Color: 0
Size: 321142 Color: 1

Bin 3028: 216 of cap free
Amount of items: 2
Items: 
Size: 762708 Color: 1
Size: 237077 Color: 0

Bin 3029: 217 of cap free
Amount of items: 2
Items: 
Size: 715784 Color: 1
Size: 284000 Color: 0

Bin 3030: 217 of cap free
Amount of items: 2
Items: 
Size: 537687 Color: 0
Size: 462097 Color: 1

Bin 3031: 217 of cap free
Amount of items: 2
Items: 
Size: 656920 Color: 0
Size: 342864 Color: 1

Bin 3032: 217 of cap free
Amount of items: 2
Items: 
Size: 668559 Color: 1
Size: 331225 Color: 0

Bin 3033: 217 of cap free
Amount of items: 2
Items: 
Size: 679416 Color: 1
Size: 320368 Color: 0

Bin 3034: 217 of cap free
Amount of items: 2
Items: 
Size: 725690 Color: 1
Size: 274094 Color: 0

Bin 3035: 218 of cap free
Amount of items: 2
Items: 
Size: 658493 Color: 1
Size: 341290 Color: 0

Bin 3036: 218 of cap free
Amount of items: 2
Items: 
Size: 795327 Color: 0
Size: 204456 Color: 1

Bin 3037: 219 of cap free
Amount of items: 2
Items: 
Size: 518835 Color: 0
Size: 480947 Color: 1

Bin 3038: 219 of cap free
Amount of items: 2
Items: 
Size: 538316 Color: 1
Size: 461466 Color: 0

Bin 3039: 220 of cap free
Amount of items: 2
Items: 
Size: 556143 Color: 0
Size: 443638 Color: 1

Bin 3040: 220 of cap free
Amount of items: 2
Items: 
Size: 625509 Color: 0
Size: 374272 Color: 1

Bin 3041: 220 of cap free
Amount of items: 2
Items: 
Size: 721035 Color: 1
Size: 278746 Color: 0

Bin 3042: 220 of cap free
Amount of items: 2
Items: 
Size: 779121 Color: 0
Size: 220660 Color: 1

Bin 3043: 221 of cap free
Amount of items: 2
Items: 
Size: 510776 Color: 0
Size: 489004 Color: 1

Bin 3044: 221 of cap free
Amount of items: 2
Items: 
Size: 558649 Color: 0
Size: 441131 Color: 1

Bin 3045: 221 of cap free
Amount of items: 2
Items: 
Size: 760351 Color: 0
Size: 239429 Color: 1

Bin 3046: 221 of cap free
Amount of items: 2
Items: 
Size: 762924 Color: 0
Size: 236856 Color: 1

Bin 3047: 222 of cap free
Amount of items: 2
Items: 
Size: 582929 Color: 1
Size: 416850 Color: 0

Bin 3048: 222 of cap free
Amount of items: 2
Items: 
Size: 518349 Color: 0
Size: 481430 Color: 1

Bin 3049: 223 of cap free
Amount of items: 2
Items: 
Size: 551452 Color: 0
Size: 448326 Color: 1

Bin 3050: 224 of cap free
Amount of items: 2
Items: 
Size: 784992 Color: 1
Size: 214785 Color: 0

Bin 3051: 224 of cap free
Amount of items: 2
Items: 
Size: 589838 Color: 1
Size: 409939 Color: 0

Bin 3052: 224 of cap free
Amount of items: 2
Items: 
Size: 633451 Color: 0
Size: 366326 Color: 1

Bin 3053: 224 of cap free
Amount of items: 2
Items: 
Size: 786850 Color: 0
Size: 212927 Color: 1

Bin 3054: 225 of cap free
Amount of items: 2
Items: 
Size: 562022 Color: 0
Size: 437754 Color: 1

Bin 3055: 225 of cap free
Amount of items: 2
Items: 
Size: 697390 Color: 0
Size: 302386 Color: 1

Bin 3056: 225 of cap free
Amount of items: 2
Items: 
Size: 799885 Color: 1
Size: 199891 Color: 0

Bin 3057: 226 of cap free
Amount of items: 2
Items: 
Size: 727628 Color: 0
Size: 272147 Color: 1

Bin 3058: 226 of cap free
Amount of items: 2
Items: 
Size: 560659 Color: 0
Size: 439116 Color: 1

Bin 3059: 226 of cap free
Amount of items: 2
Items: 
Size: 573571 Color: 0
Size: 426204 Color: 1

Bin 3060: 226 of cap free
Amount of items: 2
Items: 
Size: 743811 Color: 0
Size: 255964 Color: 1

Bin 3061: 227 of cap free
Amount of items: 2
Items: 
Size: 610267 Color: 1
Size: 389507 Color: 0

Bin 3062: 227 of cap free
Amount of items: 2
Items: 
Size: 625902 Color: 0
Size: 373872 Color: 1

Bin 3063: 227 of cap free
Amount of items: 2
Items: 
Size: 628962 Color: 1
Size: 370812 Color: 0

Bin 3064: 228 of cap free
Amount of items: 2
Items: 
Size: 645165 Color: 0
Size: 354608 Color: 1

Bin 3065: 229 of cap free
Amount of items: 2
Items: 
Size: 709700 Color: 1
Size: 290072 Color: 0

Bin 3066: 229 of cap free
Amount of items: 2
Items: 
Size: 501880 Color: 1
Size: 497892 Color: 0

Bin 3067: 229 of cap free
Amount of items: 2
Items: 
Size: 532287 Color: 1
Size: 467485 Color: 0

Bin 3068: 229 of cap free
Amount of items: 2
Items: 
Size: 551800 Color: 0
Size: 447972 Color: 1

Bin 3069: 229 of cap free
Amount of items: 2
Items: 
Size: 643275 Color: 0
Size: 356497 Color: 1

Bin 3070: 230 of cap free
Amount of items: 2
Items: 
Size: 744803 Color: 0
Size: 254968 Color: 1

Bin 3071: 230 of cap free
Amount of items: 2
Items: 
Size: 563744 Color: 0
Size: 436027 Color: 1

Bin 3072: 230 of cap free
Amount of items: 2
Items: 
Size: 692037 Color: 1
Size: 307734 Color: 0

Bin 3073: 231 of cap free
Amount of items: 2
Items: 
Size: 618712 Color: 0
Size: 381058 Color: 1

Bin 3074: 232 of cap free
Amount of items: 2
Items: 
Size: 528580 Color: 1
Size: 471189 Color: 0

Bin 3075: 232 of cap free
Amount of items: 2
Items: 
Size: 569471 Color: 0
Size: 430298 Color: 1

Bin 3076: 232 of cap free
Amount of items: 2
Items: 
Size: 629506 Color: 0
Size: 370263 Color: 1

Bin 3077: 232 of cap free
Amount of items: 2
Items: 
Size: 676442 Color: 0
Size: 323327 Color: 1

Bin 3078: 233 of cap free
Amount of items: 2
Items: 
Size: 541518 Color: 1
Size: 458250 Color: 0

Bin 3079: 233 of cap free
Amount of items: 2
Items: 
Size: 543260 Color: 0
Size: 456508 Color: 1

Bin 3080: 234 of cap free
Amount of items: 2
Items: 
Size: 516873 Color: 0
Size: 482894 Color: 1

Bin 3081: 234 of cap free
Amount of items: 2
Items: 
Size: 525608 Color: 1
Size: 474159 Color: 0

Bin 3082: 234 of cap free
Amount of items: 2
Items: 
Size: 551518 Color: 1
Size: 448249 Color: 0

Bin 3083: 235 of cap free
Amount of items: 3
Items: 
Size: 581335 Color: 0
Size: 286315 Color: 0
Size: 132116 Color: 1

Bin 3084: 235 of cap free
Amount of items: 2
Items: 
Size: 761835 Color: 1
Size: 237931 Color: 0

Bin 3085: 235 of cap free
Amount of items: 2
Items: 
Size: 557874 Color: 1
Size: 441892 Color: 0

Bin 3086: 235 of cap free
Amount of items: 2
Items: 
Size: 609536 Color: 1
Size: 390230 Color: 0

Bin 3087: 235 of cap free
Amount of items: 2
Items: 
Size: 620745 Color: 1
Size: 379021 Color: 0

Bin 3088: 235 of cap free
Amount of items: 2
Items: 
Size: 724295 Color: 0
Size: 275471 Color: 1

Bin 3089: 237 of cap free
Amount of items: 3
Items: 
Size: 443773 Color: 0
Size: 292760 Color: 0
Size: 263231 Color: 1

Bin 3090: 237 of cap free
Amount of items: 2
Items: 
Size: 507081 Color: 1
Size: 492683 Color: 0

Bin 3091: 237 of cap free
Amount of items: 2
Items: 
Size: 537431 Color: 1
Size: 462333 Color: 0

Bin 3092: 237 of cap free
Amount of items: 2
Items: 
Size: 577340 Color: 0
Size: 422424 Color: 1

Bin 3093: 238 of cap free
Amount of items: 2
Items: 
Size: 507573 Color: 1
Size: 492190 Color: 0

Bin 3094: 238 of cap free
Amount of items: 2
Items: 
Size: 526341 Color: 1
Size: 473422 Color: 0

Bin 3095: 238 of cap free
Amount of items: 2
Items: 
Size: 727152 Color: 1
Size: 272611 Color: 0

Bin 3096: 239 of cap free
Amount of items: 2
Items: 
Size: 535592 Color: 0
Size: 464170 Color: 1

Bin 3097: 239 of cap free
Amount of items: 2
Items: 
Size: 647474 Color: 0
Size: 352288 Color: 1

Bin 3098: 239 of cap free
Amount of items: 2
Items: 
Size: 668871 Color: 1
Size: 330891 Color: 0

Bin 3099: 239 of cap free
Amount of items: 2
Items: 
Size: 681451 Color: 0
Size: 318311 Color: 1

Bin 3100: 239 of cap free
Amount of items: 2
Items: 
Size: 682582 Color: 1
Size: 317180 Color: 0

Bin 3101: 239 of cap free
Amount of items: 2
Items: 
Size: 755525 Color: 0
Size: 244237 Color: 1

Bin 3102: 241 of cap free
Amount of items: 2
Items: 
Size: 676084 Color: 0
Size: 323676 Color: 1

Bin 3103: 241 of cap free
Amount of items: 2
Items: 
Size: 593126 Color: 0
Size: 406634 Color: 1

Bin 3104: 241 of cap free
Amount of items: 2
Items: 
Size: 782167 Color: 1
Size: 217593 Color: 0

Bin 3105: 242 of cap free
Amount of items: 2
Items: 
Size: 690953 Color: 1
Size: 308806 Color: 0

Bin 3106: 242 of cap free
Amount of items: 2
Items: 
Size: 557085 Color: 0
Size: 442674 Color: 1

Bin 3107: 242 of cap free
Amount of items: 2
Items: 
Size: 572553 Color: 1
Size: 427206 Color: 0

Bin 3108: 243 of cap free
Amount of items: 2
Items: 
Size: 798206 Color: 1
Size: 201552 Color: 0

Bin 3109: 243 of cap free
Amount of items: 2
Items: 
Size: 535461 Color: 1
Size: 464297 Color: 0

Bin 3110: 243 of cap free
Amount of items: 2
Items: 
Size: 623627 Color: 1
Size: 376131 Color: 0

Bin 3111: 243 of cap free
Amount of items: 2
Items: 
Size: 667550 Color: 1
Size: 332208 Color: 0

Bin 3112: 244 of cap free
Amount of items: 2
Items: 
Size: 677879 Color: 1
Size: 321878 Color: 0

Bin 3113: 244 of cap free
Amount of items: 2
Items: 
Size: 514639 Color: 0
Size: 485118 Color: 1

Bin 3114: 244 of cap free
Amount of items: 2
Items: 
Size: 554305 Color: 1
Size: 445452 Color: 0

Bin 3115: 244 of cap free
Amount of items: 2
Items: 
Size: 559611 Color: 1
Size: 440146 Color: 0

Bin 3116: 244 of cap free
Amount of items: 2
Items: 
Size: 640480 Color: 1
Size: 359277 Color: 0

Bin 3117: 245 of cap free
Amount of items: 2
Items: 
Size: 645692 Color: 1
Size: 354064 Color: 0

Bin 3118: 245 of cap free
Amount of items: 2
Items: 
Size: 712284 Color: 0
Size: 287472 Color: 1

Bin 3119: 245 of cap free
Amount of items: 2
Items: 
Size: 522779 Color: 1
Size: 476977 Color: 0

Bin 3120: 245 of cap free
Amount of items: 2
Items: 
Size: 633009 Color: 0
Size: 366747 Color: 1

Bin 3121: 245 of cap free
Amount of items: 2
Items: 
Size: 671122 Color: 0
Size: 328634 Color: 1

Bin 3122: 245 of cap free
Amount of items: 2
Items: 
Size: 738137 Color: 1
Size: 261619 Color: 0

Bin 3123: 245 of cap free
Amount of items: 2
Items: 
Size: 744446 Color: 0
Size: 255310 Color: 1

Bin 3124: 246 of cap free
Amount of items: 2
Items: 
Size: 740276 Color: 0
Size: 259479 Color: 1

Bin 3125: 246 of cap free
Amount of items: 2
Items: 
Size: 753771 Color: 0
Size: 245984 Color: 1

Bin 3126: 247 of cap free
Amount of items: 2
Items: 
Size: 779079 Color: 1
Size: 220675 Color: 0

Bin 3127: 247 of cap free
Amount of items: 2
Items: 
Size: 675822 Color: 1
Size: 323932 Color: 0

Bin 3128: 247 of cap free
Amount of items: 2
Items: 
Size: 747050 Color: 1
Size: 252704 Color: 0

Bin 3129: 247 of cap free
Amount of items: 2
Items: 
Size: 775923 Color: 0
Size: 223831 Color: 1

Bin 3130: 248 of cap free
Amount of items: 2
Items: 
Size: 599369 Color: 1
Size: 400384 Color: 0

Bin 3131: 248 of cap free
Amount of items: 2
Items: 
Size: 638027 Color: 0
Size: 361726 Color: 1

Bin 3132: 248 of cap free
Amount of items: 2
Items: 
Size: 784281 Color: 1
Size: 215472 Color: 0

Bin 3133: 249 of cap free
Amount of items: 2
Items: 
Size: 536060 Color: 0
Size: 463692 Color: 1

Bin 3134: 249 of cap free
Amount of items: 2
Items: 
Size: 713733 Color: 0
Size: 286019 Color: 1

Bin 3135: 250 of cap free
Amount of items: 2
Items: 
Size: 532272 Color: 1
Size: 467479 Color: 0

Bin 3136: 251 of cap free
Amount of items: 3
Items: 
Size: 775397 Color: 1
Size: 121476 Color: 0
Size: 102877 Color: 1

Bin 3137: 251 of cap free
Amount of items: 2
Items: 
Size: 756556 Color: 1
Size: 243194 Color: 0

Bin 3138: 252 of cap free
Amount of items: 2
Items: 
Size: 602829 Color: 0
Size: 396920 Color: 1

Bin 3139: 252 of cap free
Amount of items: 2
Items: 
Size: 642290 Color: 1
Size: 357459 Color: 0

Bin 3140: 252 of cap free
Amount of items: 2
Items: 
Size: 651500 Color: 0
Size: 348249 Color: 1

Bin 3141: 252 of cap free
Amount of items: 2
Items: 
Size: 687683 Color: 0
Size: 312066 Color: 1

Bin 3142: 252 of cap free
Amount of items: 2
Items: 
Size: 793663 Color: 1
Size: 206086 Color: 0

Bin 3143: 253 of cap free
Amount of items: 2
Items: 
Size: 628321 Color: 1
Size: 371427 Color: 0

Bin 3144: 254 of cap free
Amount of items: 2
Items: 
Size: 791484 Color: 1
Size: 208263 Color: 0

Bin 3145: 254 of cap free
Amount of items: 2
Items: 
Size: 519351 Color: 1
Size: 480396 Color: 0

Bin 3146: 254 of cap free
Amount of items: 2
Items: 
Size: 521006 Color: 1
Size: 478741 Color: 0

Bin 3147: 254 of cap free
Amount of items: 2
Items: 
Size: 625173 Color: 1
Size: 374574 Color: 0

Bin 3148: 254 of cap free
Amount of items: 2
Items: 
Size: 753020 Color: 1
Size: 246727 Color: 0

Bin 3149: 255 of cap free
Amount of items: 3
Items: 
Size: 405995 Color: 0
Size: 313815 Color: 1
Size: 279936 Color: 1

Bin 3150: 255 of cap free
Amount of items: 2
Items: 
Size: 710329 Color: 0
Size: 289417 Color: 1

Bin 3151: 255 of cap free
Amount of items: 2
Items: 
Size: 783111 Color: 0
Size: 216635 Color: 1

Bin 3152: 255 of cap free
Amount of items: 2
Items: 
Size: 623028 Color: 1
Size: 376718 Color: 0

Bin 3153: 255 of cap free
Amount of items: 2
Items: 
Size: 672918 Color: 1
Size: 326828 Color: 0

Bin 3154: 255 of cap free
Amount of items: 2
Items: 
Size: 693436 Color: 0
Size: 306310 Color: 1

Bin 3155: 256 of cap free
Amount of items: 2
Items: 
Size: 678807 Color: 1
Size: 320938 Color: 0

Bin 3156: 257 of cap free
Amount of items: 3
Items: 
Size: 761786 Color: 1
Size: 124134 Color: 0
Size: 113824 Color: 1

Bin 3157: 257 of cap free
Amount of items: 2
Items: 
Size: 527752 Color: 1
Size: 471992 Color: 0

Bin 3158: 257 of cap free
Amount of items: 2
Items: 
Size: 593109 Color: 1
Size: 406635 Color: 0

Bin 3159: 257 of cap free
Amount of items: 2
Items: 
Size: 759898 Color: 1
Size: 239846 Color: 0

Bin 3160: 258 of cap free
Amount of items: 2
Items: 
Size: 501050 Color: 0
Size: 498693 Color: 1

Bin 3161: 258 of cap free
Amount of items: 2
Items: 
Size: 529138 Color: 1
Size: 470605 Color: 0

Bin 3162: 258 of cap free
Amount of items: 2
Items: 
Size: 637672 Color: 1
Size: 362071 Color: 0

Bin 3163: 259 of cap free
Amount of items: 2
Items: 
Size: 640986 Color: 1
Size: 358756 Color: 0

Bin 3164: 259 of cap free
Amount of items: 2
Items: 
Size: 699113 Color: 1
Size: 300629 Color: 0

Bin 3165: 260 of cap free
Amount of items: 2
Items: 
Size: 653467 Color: 0
Size: 346274 Color: 1

Bin 3166: 260 of cap free
Amount of items: 2
Items: 
Size: 623122 Color: 0
Size: 376619 Color: 1

Bin 3167: 260 of cap free
Amount of items: 2
Items: 
Size: 664385 Color: 0
Size: 335356 Color: 1

Bin 3168: 260 of cap free
Amount of items: 2
Items: 
Size: 687954 Color: 1
Size: 311787 Color: 0

Bin 3169: 261 of cap free
Amount of items: 2
Items: 
Size: 723500 Color: 0
Size: 276240 Color: 1

Bin 3170: 261 of cap free
Amount of items: 2
Items: 
Size: 545438 Color: 1
Size: 454302 Color: 0

Bin 3171: 261 of cap free
Amount of items: 2
Items: 
Size: 659779 Color: 1
Size: 339961 Color: 0

Bin 3172: 262 of cap free
Amount of items: 3
Items: 
Size: 639450 Color: 1
Size: 227007 Color: 1
Size: 133282 Color: 0

Bin 3173: 262 of cap free
Amount of items: 2
Items: 
Size: 589461 Color: 1
Size: 410278 Color: 0

Bin 3174: 262 of cap free
Amount of items: 2
Items: 
Size: 733617 Color: 0
Size: 266122 Color: 1

Bin 3175: 263 of cap free
Amount of items: 2
Items: 
Size: 523499 Color: 1
Size: 476239 Color: 0

Bin 3176: 263 of cap free
Amount of items: 2
Items: 
Size: 539161 Color: 0
Size: 460577 Color: 1

Bin 3177: 263 of cap free
Amount of items: 2
Items: 
Size: 758924 Color: 0
Size: 240814 Color: 1

Bin 3178: 264 of cap free
Amount of items: 2
Items: 
Size: 521876 Color: 1
Size: 477861 Color: 0

Bin 3179: 264 of cap free
Amount of items: 2
Items: 
Size: 501845 Color: 0
Size: 497892 Color: 1

Bin 3180: 264 of cap free
Amount of items: 2
Items: 
Size: 596102 Color: 0
Size: 403635 Color: 1

Bin 3181: 264 of cap free
Amount of items: 2
Items: 
Size: 658486 Color: 1
Size: 341251 Color: 0

Bin 3182: 264 of cap free
Amount of items: 2
Items: 
Size: 784670 Color: 0
Size: 215067 Color: 1

Bin 3183: 265 of cap free
Amount of items: 2
Items: 
Size: 593104 Color: 1
Size: 406632 Color: 0

Bin 3184: 265 of cap free
Amount of items: 2
Items: 
Size: 710712 Color: 0
Size: 289024 Color: 1

Bin 3185: 265 of cap free
Amount of items: 2
Items: 
Size: 788631 Color: 0
Size: 211105 Color: 1

Bin 3186: 266 of cap free
Amount of items: 2
Items: 
Size: 509401 Color: 1
Size: 490334 Color: 0

Bin 3187: 266 of cap free
Amount of items: 2
Items: 
Size: 533138 Color: 1
Size: 466597 Color: 0

Bin 3188: 266 of cap free
Amount of items: 2
Items: 
Size: 715902 Color: 0
Size: 283833 Color: 1

Bin 3189: 267 of cap free
Amount of items: 2
Items: 
Size: 570559 Color: 0
Size: 429175 Color: 1

Bin 3190: 267 of cap free
Amount of items: 2
Items: 
Size: 658252 Color: 0
Size: 341482 Color: 1

Bin 3191: 268 of cap free
Amount of items: 2
Items: 
Size: 561749 Color: 1
Size: 437984 Color: 0

Bin 3192: 269 of cap free
Amount of items: 2
Items: 
Size: 784090 Color: 0
Size: 215642 Color: 1

Bin 3193: 269 of cap free
Amount of items: 2
Items: 
Size: 536507 Color: 1
Size: 463225 Color: 0

Bin 3194: 269 of cap free
Amount of items: 2
Items: 
Size: 634373 Color: 0
Size: 365359 Color: 1

Bin 3195: 269 of cap free
Amount of items: 2
Items: 
Size: 747650 Color: 0
Size: 252082 Color: 1

Bin 3196: 269 of cap free
Amount of items: 2
Items: 
Size: 763752 Color: 1
Size: 235980 Color: 0

Bin 3197: 269 of cap free
Amount of items: 2
Items: 
Size: 787439 Color: 0
Size: 212293 Color: 1

Bin 3198: 270 of cap free
Amount of items: 2
Items: 
Size: 540338 Color: 0
Size: 459393 Color: 1

Bin 3199: 270 of cap free
Amount of items: 2
Items: 
Size: 617389 Color: 1
Size: 382342 Color: 0

Bin 3200: 271 of cap free
Amount of items: 2
Items: 
Size: 583425 Color: 0
Size: 416305 Color: 1

Bin 3201: 271 of cap free
Amount of items: 2
Items: 
Size: 592698 Color: 0
Size: 407032 Color: 1

Bin 3202: 271 of cap free
Amount of items: 2
Items: 
Size: 619609 Color: 0
Size: 380121 Color: 1

Bin 3203: 271 of cap free
Amount of items: 2
Items: 
Size: 632355 Color: 0
Size: 367375 Color: 1

Bin 3204: 272 of cap free
Amount of items: 2
Items: 
Size: 676033 Color: 0
Size: 323696 Color: 1

Bin 3205: 272 of cap free
Amount of items: 2
Items: 
Size: 581927 Color: 1
Size: 417802 Color: 0

Bin 3206: 272 of cap free
Amount of items: 2
Items: 
Size: 691048 Color: 0
Size: 308681 Color: 1

Bin 3207: 273 of cap free
Amount of items: 2
Items: 
Size: 655446 Color: 1
Size: 344282 Color: 0

Bin 3208: 273 of cap free
Amount of items: 2
Items: 
Size: 584080 Color: 1
Size: 415648 Color: 0

Bin 3209: 273 of cap free
Amount of items: 2
Items: 
Size: 700640 Color: 1
Size: 299088 Color: 0

Bin 3210: 274 of cap free
Amount of items: 2
Items: 
Size: 585861 Color: 0
Size: 413866 Color: 1

Bin 3211: 274 of cap free
Amount of items: 2
Items: 
Size: 519520 Color: 0
Size: 480207 Color: 1

Bin 3212: 274 of cap free
Amount of items: 2
Items: 
Size: 546767 Color: 0
Size: 452960 Color: 1

Bin 3213: 275 of cap free
Amount of items: 2
Items: 
Size: 509909 Color: 1
Size: 489817 Color: 0

Bin 3214: 275 of cap free
Amount of items: 2
Items: 
Size: 525921 Color: 0
Size: 473805 Color: 1

Bin 3215: 275 of cap free
Amount of items: 2
Items: 
Size: 629484 Color: 0
Size: 370242 Color: 1

Bin 3216: 276 of cap free
Amount of items: 2
Items: 
Size: 765637 Color: 0
Size: 234088 Color: 1

Bin 3217: 276 of cap free
Amount of items: 2
Items: 
Size: 558606 Color: 0
Size: 441119 Color: 1

Bin 3218: 276 of cap free
Amount of items: 2
Items: 
Size: 746619 Color: 1
Size: 253106 Color: 0

Bin 3219: 277 of cap free
Amount of items: 2
Items: 
Size: 707383 Color: 1
Size: 292341 Color: 0

Bin 3220: 277 of cap free
Amount of items: 2
Items: 
Size: 554102 Color: 0
Size: 445622 Color: 1

Bin 3221: 277 of cap free
Amount of items: 2
Items: 
Size: 650247 Color: 1
Size: 349477 Color: 0

Bin 3222: 277 of cap free
Amount of items: 2
Items: 
Size: 726185 Color: 0
Size: 273539 Color: 1

Bin 3223: 278 of cap free
Amount of items: 2
Items: 
Size: 763389 Color: 0
Size: 236334 Color: 1

Bin 3224: 278 of cap free
Amount of items: 2
Items: 
Size: 638200 Color: 1
Size: 361523 Color: 0

Bin 3225: 279 of cap free
Amount of items: 2
Items: 
Size: 731418 Color: 1
Size: 268304 Color: 0

Bin 3226: 279 of cap free
Amount of items: 2
Items: 
Size: 652566 Color: 1
Size: 347156 Color: 0

Bin 3227: 279 of cap free
Amount of items: 2
Items: 
Size: 770296 Color: 1
Size: 229426 Color: 0

Bin 3228: 279 of cap free
Amount of items: 2
Items: 
Size: 748504 Color: 0
Size: 251218 Color: 1

Bin 3229: 279 of cap free
Amount of items: 2
Items: 
Size: 524945 Color: 0
Size: 474777 Color: 1

Bin 3230: 279 of cap free
Amount of items: 2
Items: 
Size: 779903 Color: 0
Size: 219819 Color: 1

Bin 3231: 280 of cap free
Amount of items: 2
Items: 
Size: 718580 Color: 0
Size: 281141 Color: 1

Bin 3232: 280 of cap free
Amount of items: 2
Items: 
Size: 769637 Color: 0
Size: 230084 Color: 1

Bin 3233: 280 of cap free
Amount of items: 2
Items: 
Size: 706680 Color: 1
Size: 293041 Color: 0

Bin 3234: 280 of cap free
Amount of items: 2
Items: 
Size: 517448 Color: 0
Size: 482273 Color: 1

Bin 3235: 280 of cap free
Amount of items: 2
Items: 
Size: 548899 Color: 1
Size: 450822 Color: 0

Bin 3236: 280 of cap free
Amount of items: 2
Items: 
Size: 698784 Color: 1
Size: 300937 Color: 0

Bin 3237: 280 of cap free
Amount of items: 2
Items: 
Size: 776631 Color: 1
Size: 223090 Color: 0

Bin 3238: 281 of cap free
Amount of items: 2
Items: 
Size: 520285 Color: 0
Size: 479435 Color: 1

Bin 3239: 282 of cap free
Amount of items: 2
Items: 
Size: 736491 Color: 1
Size: 263228 Color: 0

Bin 3240: 283 of cap free
Amount of items: 2
Items: 
Size: 755929 Color: 1
Size: 243789 Color: 0

Bin 3241: 283 of cap free
Amount of items: 2
Items: 
Size: 520982 Color: 1
Size: 478736 Color: 0

Bin 3242: 283 of cap free
Amount of items: 2
Items: 
Size: 718572 Color: 1
Size: 281146 Color: 0

Bin 3243: 284 of cap free
Amount of items: 2
Items: 
Size: 523913 Color: 0
Size: 475804 Color: 1

Bin 3244: 284 of cap free
Amount of items: 2
Items: 
Size: 713110 Color: 0
Size: 286607 Color: 1

Bin 3245: 284 of cap free
Amount of items: 2
Items: 
Size: 715717 Color: 1
Size: 284000 Color: 0

Bin 3246: 285 of cap free
Amount of items: 2
Items: 
Size: 562679 Color: 1
Size: 437037 Color: 0

Bin 3247: 285 of cap free
Amount of items: 2
Items: 
Size: 566882 Color: 1
Size: 432834 Color: 0

Bin 3248: 285 of cap free
Amount of items: 2
Items: 
Size: 602846 Color: 1
Size: 396870 Color: 0

Bin 3249: 285 of cap free
Amount of items: 2
Items: 
Size: 635155 Color: 1
Size: 364561 Color: 0

Bin 3250: 285 of cap free
Amount of items: 2
Items: 
Size: 650719 Color: 1
Size: 348997 Color: 0

Bin 3251: 285 of cap free
Amount of items: 2
Items: 
Size: 662856 Color: 0
Size: 336860 Color: 1

Bin 3252: 285 of cap free
Amount of items: 2
Items: 
Size: 672909 Color: 1
Size: 326807 Color: 0

Bin 3253: 286 of cap free
Amount of items: 2
Items: 
Size: 538359 Color: 0
Size: 461356 Color: 1

Bin 3254: 287 of cap free
Amount of items: 2
Items: 
Size: 641786 Color: 1
Size: 357928 Color: 0

Bin 3255: 287 of cap free
Amount of items: 2
Items: 
Size: 534809 Color: 0
Size: 464905 Color: 1

Bin 3256: 288 of cap free
Amount of items: 2
Items: 
Size: 505581 Color: 1
Size: 494132 Color: 0

Bin 3257: 288 of cap free
Amount of items: 2
Items: 
Size: 553533 Color: 0
Size: 446180 Color: 1

Bin 3258: 288 of cap free
Amount of items: 2
Items: 
Size: 655019 Color: 1
Size: 344694 Color: 0

Bin 3259: 289 of cap free
Amount of items: 2
Items: 
Size: 620045 Color: 1
Size: 379667 Color: 0

Bin 3260: 290 of cap free
Amount of items: 2
Items: 
Size: 789237 Color: 1
Size: 210474 Color: 0

Bin 3261: 290 of cap free
Amount of items: 2
Items: 
Size: 511630 Color: 0
Size: 488081 Color: 1

Bin 3262: 291 of cap free
Amount of items: 2
Items: 
Size: 513781 Color: 0
Size: 485929 Color: 1

Bin 3263: 291 of cap free
Amount of items: 2
Items: 
Size: 526656 Color: 0
Size: 473054 Color: 1

Bin 3264: 291 of cap free
Amount of items: 2
Items: 
Size: 547963 Color: 1
Size: 451747 Color: 0

Bin 3265: 291 of cap free
Amount of items: 2
Items: 
Size: 564627 Color: 1
Size: 435083 Color: 0

Bin 3266: 292 of cap free
Amount of items: 2
Items: 
Size: 792856 Color: 0
Size: 206853 Color: 1

Bin 3267: 292 of cap free
Amount of items: 2
Items: 
Size: 772462 Color: 1
Size: 227247 Color: 0

Bin 3268: 292 of cap free
Amount of items: 2
Items: 
Size: 568451 Color: 0
Size: 431258 Color: 1

Bin 3269: 292 of cap free
Amount of items: 2
Items: 
Size: 599406 Color: 0
Size: 400303 Color: 1

Bin 3270: 292 of cap free
Amount of items: 2
Items: 
Size: 678287 Color: 1
Size: 321422 Color: 0

Bin 3271: 293 of cap free
Amount of items: 2
Items: 
Size: 675366 Color: 1
Size: 324342 Color: 0

Bin 3272: 294 of cap free
Amount of items: 2
Items: 
Size: 516079 Color: 1
Size: 483628 Color: 0

Bin 3273: 294 of cap free
Amount of items: 2
Items: 
Size: 559073 Color: 0
Size: 440634 Color: 1

Bin 3274: 295 of cap free
Amount of items: 2
Items: 
Size: 669118 Color: 1
Size: 330588 Color: 0

Bin 3275: 295 of cap free
Amount of items: 2
Items: 
Size: 585021 Color: 0
Size: 414685 Color: 1

Bin 3276: 295 of cap free
Amount of items: 2
Items: 
Size: 745715 Color: 1
Size: 253991 Color: 0

Bin 3277: 296 of cap free
Amount of items: 2
Items: 
Size: 754178 Color: 0
Size: 245527 Color: 1

Bin 3278: 296 of cap free
Amount of items: 2
Items: 
Size: 728741 Color: 0
Size: 270964 Color: 1

Bin 3279: 297 of cap free
Amount of items: 2
Items: 
Size: 756846 Color: 0
Size: 242858 Color: 1

Bin 3280: 298 of cap free
Amount of items: 2
Items: 
Size: 633288 Color: 1
Size: 366415 Color: 0

Bin 3281: 298 of cap free
Amount of items: 2
Items: 
Size: 733976 Color: 0
Size: 265727 Color: 1

Bin 3282: 299 of cap free
Amount of items: 2
Items: 
Size: 789043 Color: 0
Size: 210659 Color: 1

Bin 3283: 299 of cap free
Amount of items: 2
Items: 
Size: 560004 Color: 0
Size: 439698 Color: 1

Bin 3284: 299 of cap free
Amount of items: 2
Items: 
Size: 663691 Color: 0
Size: 336011 Color: 1

Bin 3285: 299 of cap free
Amount of items: 2
Items: 
Size: 727103 Color: 0
Size: 272599 Color: 1

Bin 3286: 299 of cap free
Amount of items: 2
Items: 
Size: 741780 Color: 0
Size: 257922 Color: 1

Bin 3287: 299 of cap free
Amount of items: 2
Items: 
Size: 753008 Color: 1
Size: 246694 Color: 0

Bin 3288: 299 of cap free
Amount of items: 2
Items: 
Size: 765281 Color: 1
Size: 234421 Color: 0

Bin 3289: 300 of cap free
Amount of items: 2
Items: 
Size: 713914 Color: 1
Size: 285787 Color: 0

Bin 3290: 300 of cap free
Amount of items: 2
Items: 
Size: 513831 Color: 1
Size: 485870 Color: 0

Bin 3291: 300 of cap free
Amount of items: 2
Items: 
Size: 580067 Color: 0
Size: 419634 Color: 1

Bin 3292: 300 of cap free
Amount of items: 2
Items: 
Size: 681331 Color: 1
Size: 318370 Color: 0

Bin 3293: 300 of cap free
Amount of items: 2
Items: 
Size: 732941 Color: 1
Size: 266760 Color: 0

Bin 3294: 301 of cap free
Amount of items: 2
Items: 
Size: 757614 Color: 1
Size: 242086 Color: 0

Bin 3295: 301 of cap free
Amount of items: 2
Items: 
Size: 582908 Color: 1
Size: 416792 Color: 0

Bin 3296: 301 of cap free
Amount of items: 2
Items: 
Size: 549230 Color: 1
Size: 450470 Color: 0

Bin 3297: 301 of cap free
Amount of items: 2
Items: 
Size: 612253 Color: 1
Size: 387447 Color: 0

Bin 3298: 301 of cap free
Amount of items: 2
Items: 
Size: 644905 Color: 1
Size: 354795 Color: 0

Bin 3299: 301 of cap free
Amount of items: 2
Items: 
Size: 688882 Color: 1
Size: 310818 Color: 0

Bin 3300: 304 of cap free
Amount of items: 2
Items: 
Size: 727108 Color: 1
Size: 272589 Color: 0

Bin 3301: 304 of cap free
Amount of items: 2
Items: 
Size: 773033 Color: 0
Size: 226664 Color: 1

Bin 3302: 304 of cap free
Amount of items: 2
Items: 
Size: 799857 Color: 1
Size: 199840 Color: 0

Bin 3303: 305 of cap free
Amount of items: 2
Items: 
Size: 722137 Color: 0
Size: 277559 Color: 1

Bin 3304: 306 of cap free
Amount of items: 2
Items: 
Size: 712495 Color: 1
Size: 287200 Color: 0

Bin 3305: 307 of cap free
Amount of items: 2
Items: 
Size: 696942 Color: 1
Size: 302752 Color: 0

Bin 3306: 307 of cap free
Amount of items: 2
Items: 
Size: 581407 Color: 0
Size: 418287 Color: 1

Bin 3307: 307 of cap free
Amount of items: 2
Items: 
Size: 582294 Color: 1
Size: 417400 Color: 0

Bin 3308: 308 of cap free
Amount of items: 3
Items: 
Size: 591606 Color: 1
Size: 253640 Color: 0
Size: 154447 Color: 1

Bin 3309: 308 of cap free
Amount of items: 2
Items: 
Size: 536945 Color: 0
Size: 462748 Color: 1

Bin 3310: 308 of cap free
Amount of items: 2
Items: 
Size: 732193 Color: 1
Size: 267500 Color: 0

Bin 3311: 308 of cap free
Amount of items: 2
Items: 
Size: 772266 Color: 0
Size: 227427 Color: 1

Bin 3312: 309 of cap free
Amount of items: 2
Items: 
Size: 673212 Color: 0
Size: 326480 Color: 1

Bin 3313: 309 of cap free
Amount of items: 2
Items: 
Size: 689513 Color: 0
Size: 310179 Color: 1

Bin 3314: 310 of cap free
Amount of items: 2
Items: 
Size: 520235 Color: 1
Size: 479456 Color: 0

Bin 3315: 310 of cap free
Amount of items: 2
Items: 
Size: 635552 Color: 0
Size: 364139 Color: 1

Bin 3316: 310 of cap free
Amount of items: 2
Items: 
Size: 670377 Color: 1
Size: 329314 Color: 0

Bin 3317: 311 of cap free
Amount of items: 2
Items: 
Size: 571496 Color: 1
Size: 428194 Color: 0

Bin 3318: 311 of cap free
Amount of items: 2
Items: 
Size: 518787 Color: 0
Size: 480903 Color: 1

Bin 3319: 311 of cap free
Amount of items: 2
Items: 
Size: 542554 Color: 1
Size: 457136 Color: 0

Bin 3320: 311 of cap free
Amount of items: 2
Items: 
Size: 745389 Color: 1
Size: 254301 Color: 0

Bin 3321: 312 of cap free
Amount of items: 2
Items: 
Size: 543934 Color: 1
Size: 455755 Color: 0

Bin 3322: 313 of cap free
Amount of items: 2
Items: 
Size: 529730 Color: 0
Size: 469958 Color: 1

Bin 3323: 313 of cap free
Amount of items: 2
Items: 
Size: 654445 Color: 1
Size: 345243 Color: 0

Bin 3324: 314 of cap free
Amount of items: 2
Items: 
Size: 587012 Color: 0
Size: 412675 Color: 1

Bin 3325: 314 of cap free
Amount of items: 2
Items: 
Size: 591824 Color: 0
Size: 407863 Color: 1

Bin 3326: 314 of cap free
Amount of items: 2
Items: 
Size: 549620 Color: 1
Size: 450067 Color: 0

Bin 3327: 314 of cap free
Amount of items: 2
Items: 
Size: 780934 Color: 0
Size: 218753 Color: 1

Bin 3328: 315 of cap free
Amount of items: 2
Items: 
Size: 577107 Color: 1
Size: 422579 Color: 0

Bin 3329: 316 of cap free
Amount of items: 2
Items: 
Size: 750978 Color: 1
Size: 248707 Color: 0

Bin 3330: 317 of cap free
Amount of items: 2
Items: 
Size: 726427 Color: 1
Size: 273257 Color: 0

Bin 3331: 317 of cap free
Amount of items: 2
Items: 
Size: 558754 Color: 1
Size: 440930 Color: 0

Bin 3332: 318 of cap free
Amount of items: 2
Items: 
Size: 773214 Color: 1
Size: 226469 Color: 0

Bin 3333: 318 of cap free
Amount of items: 2
Items: 
Size: 540654 Color: 1
Size: 459029 Color: 0

Bin 3334: 319 of cap free
Amount of items: 2
Items: 
Size: 519130 Color: 0
Size: 480552 Color: 1

Bin 3335: 319 of cap free
Amount of items: 2
Items: 
Size: 651834 Color: 1
Size: 347848 Color: 0

Bin 3336: 319 of cap free
Amount of items: 2
Items: 
Size: 658921 Color: 1
Size: 340761 Color: 0

Bin 3337: 319 of cap free
Amount of items: 2
Items: 
Size: 681778 Color: 0
Size: 317904 Color: 1

Bin 3338: 320 of cap free
Amount of items: 2
Items: 
Size: 729518 Color: 1
Size: 270163 Color: 0

Bin 3339: 321 of cap free
Amount of items: 2
Items: 
Size: 731714 Color: 1
Size: 267966 Color: 0

Bin 3340: 321 of cap free
Amount of items: 2
Items: 
Size: 527040 Color: 1
Size: 472640 Color: 0

Bin 3341: 321 of cap free
Amount of items: 2
Items: 
Size: 688545 Color: 1
Size: 311135 Color: 0

Bin 3342: 322 of cap free
Amount of items: 2
Items: 
Size: 683541 Color: 0
Size: 316138 Color: 1

Bin 3343: 322 of cap free
Amount of items: 2
Items: 
Size: 792820 Color: 1
Size: 206859 Color: 0

Bin 3344: 323 of cap free
Amount of items: 2
Items: 
Size: 700616 Color: 0
Size: 299062 Color: 1

Bin 3345: 324 of cap free
Amount of items: 2
Items: 
Size: 704614 Color: 0
Size: 295063 Color: 1

Bin 3346: 325 of cap free
Amount of items: 2
Items: 
Size: 788614 Color: 0
Size: 211062 Color: 1

Bin 3347: 325 of cap free
Amount of items: 2
Items: 
Size: 736046 Color: 1
Size: 263630 Color: 0

Bin 3348: 326 of cap free
Amount of items: 2
Items: 
Size: 594071 Color: 0
Size: 405604 Color: 1

Bin 3349: 326 of cap free
Amount of items: 2
Items: 
Size: 612510 Color: 0
Size: 387165 Color: 1

Bin 3350: 327 of cap free
Amount of items: 2
Items: 
Size: 690898 Color: 1
Size: 308776 Color: 0

Bin 3351: 327 of cap free
Amount of items: 2
Items: 
Size: 644062 Color: 1
Size: 355612 Color: 0

Bin 3352: 327 of cap free
Amount of items: 2
Items: 
Size: 674390 Color: 1
Size: 325284 Color: 0

Bin 3353: 327 of cap free
Amount of items: 2
Items: 
Size: 675807 Color: 1
Size: 323867 Color: 0

Bin 3354: 327 of cap free
Amount of items: 2
Items: 
Size: 752122 Color: 1
Size: 247552 Color: 0

Bin 3355: 327 of cap free
Amount of items: 2
Items: 
Size: 759778 Color: 0
Size: 239896 Color: 1

Bin 3356: 328 of cap free
Amount of items: 2
Items: 
Size: 752456 Color: 1
Size: 247217 Color: 0

Bin 3357: 328 of cap free
Amount of items: 2
Items: 
Size: 597389 Color: 0
Size: 402284 Color: 1

Bin 3358: 328 of cap free
Amount of items: 2
Items: 
Size: 503783 Color: 1
Size: 495890 Color: 0

Bin 3359: 328 of cap free
Amount of items: 2
Items: 
Size: 527735 Color: 1
Size: 471938 Color: 0

Bin 3360: 328 of cap free
Amount of items: 2
Items: 
Size: 580155 Color: 1
Size: 419518 Color: 0

Bin 3361: 329 of cap free
Amount of items: 2
Items: 
Size: 708841 Color: 0
Size: 290831 Color: 1

Bin 3362: 329 of cap free
Amount of items: 2
Items: 
Size: 674831 Color: 1
Size: 324841 Color: 0

Bin 3363: 329 of cap free
Amount of items: 2
Items: 
Size: 686088 Color: 1
Size: 313584 Color: 0

Bin 3364: 329 of cap free
Amount of items: 2
Items: 
Size: 784944 Color: 0
Size: 214728 Color: 1

Bin 3365: 330 of cap free
Amount of items: 2
Items: 
Size: 797183 Color: 1
Size: 202488 Color: 0

Bin 3366: 330 of cap free
Amount of items: 2
Items: 
Size: 552049 Color: 0
Size: 447622 Color: 1

Bin 3367: 331 of cap free
Amount of items: 2
Items: 
Size: 575687 Color: 1
Size: 423983 Color: 0

Bin 3368: 331 of cap free
Amount of items: 2
Items: 
Size: 740717 Color: 0
Size: 258953 Color: 1

Bin 3369: 332 of cap free
Amount of items: 2
Items: 
Size: 685817 Color: 0
Size: 313852 Color: 1

Bin 3370: 332 of cap free
Amount of items: 2
Items: 
Size: 569295 Color: 1
Size: 430374 Color: 0

Bin 3371: 332 of cap free
Amount of items: 2
Items: 
Size: 694648 Color: 0
Size: 305021 Color: 1

Bin 3372: 333 of cap free
Amount of items: 3
Items: 
Size: 719323 Color: 0
Size: 164820 Color: 1
Size: 115525 Color: 0

Bin 3373: 333 of cap free
Amount of items: 2
Items: 
Size: 579580 Color: 0
Size: 420088 Color: 1

Bin 3374: 333 of cap free
Amount of items: 2
Items: 
Size: 627792 Color: 0
Size: 371876 Color: 1

Bin 3375: 334 of cap free
Amount of items: 2
Items: 
Size: 604405 Color: 0
Size: 395262 Color: 1

Bin 3376: 335 of cap free
Amount of items: 2
Items: 
Size: 618668 Color: 0
Size: 380998 Color: 1

Bin 3377: 336 of cap free
Amount of items: 2
Items: 
Size: 504047 Color: 0
Size: 495618 Color: 1

Bin 3378: 336 of cap free
Amount of items: 2
Items: 
Size: 790140 Color: 0
Size: 209525 Color: 1

Bin 3379: 339 of cap free
Amount of items: 2
Items: 
Size: 591003 Color: 0
Size: 408659 Color: 1

Bin 3380: 339 of cap free
Amount of items: 2
Items: 
Size: 515936 Color: 0
Size: 483726 Color: 1

Bin 3381: 339 of cap free
Amount of items: 2
Items: 
Size: 650458 Color: 0
Size: 349204 Color: 1

Bin 3382: 340 of cap free
Amount of items: 2
Items: 
Size: 515462 Color: 1
Size: 484199 Color: 0

Bin 3383: 341 of cap free
Amount of items: 2
Items: 
Size: 736444 Color: 1
Size: 263216 Color: 0

Bin 3384: 342 of cap free
Amount of items: 2
Items: 
Size: 598040 Color: 0
Size: 401619 Color: 1

Bin 3385: 343 of cap free
Amount of items: 2
Items: 
Size: 596871 Color: 1
Size: 402787 Color: 0

Bin 3386: 344 of cap free
Amount of items: 2
Items: 
Size: 727857 Color: 1
Size: 271800 Color: 0

Bin 3387: 344 of cap free
Amount of items: 2
Items: 
Size: 531124 Color: 1
Size: 468533 Color: 0

Bin 3388: 344 of cap free
Amount of items: 2
Items: 
Size: 565130 Color: 0
Size: 434527 Color: 1

Bin 3389: 344 of cap free
Amount of items: 2
Items: 
Size: 692919 Color: 0
Size: 306738 Color: 1

Bin 3390: 344 of cap free
Amount of items: 2
Items: 
Size: 742942 Color: 1
Size: 256715 Color: 0

Bin 3391: 345 of cap free
Amount of items: 2
Items: 
Size: 574192 Color: 1
Size: 425464 Color: 0

Bin 3392: 345 of cap free
Amount of items: 2
Items: 
Size: 596454 Color: 1
Size: 403202 Color: 0

Bin 3393: 346 of cap free
Amount of items: 2
Items: 
Size: 628258 Color: 1
Size: 371397 Color: 0

Bin 3394: 346 of cap free
Amount of items: 6
Items: 
Size: 201088 Color: 0
Size: 163609 Color: 0
Size: 163314 Color: 0
Size: 157883 Color: 1
Size: 157026 Color: 1
Size: 156735 Color: 1

Bin 3395: 346 of cap free
Amount of items: 2
Items: 
Size: 531269 Color: 0
Size: 468386 Color: 1

Bin 3396: 347 of cap free
Amount of items: 2
Items: 
Size: 580667 Color: 0
Size: 418987 Color: 1

Bin 3397: 348 of cap free
Amount of items: 2
Items: 
Size: 542725 Color: 0
Size: 456928 Color: 1

Bin 3398: 348 of cap free
Amount of items: 2
Items: 
Size: 643370 Color: 1
Size: 356283 Color: 0

Bin 3399: 348 of cap free
Amount of items: 2
Items: 
Size: 692940 Color: 1
Size: 306713 Color: 0

Bin 3400: 348 of cap free
Amount of items: 2
Items: 
Size: 777754 Color: 0
Size: 221899 Color: 1

Bin 3401: 349 of cap free
Amount of items: 2
Items: 
Size: 602002 Color: 1
Size: 397650 Color: 0

Bin 3402: 349 of cap free
Amount of items: 2
Items: 
Size: 639314 Color: 1
Size: 360338 Color: 0

Bin 3403: 349 of cap free
Amount of items: 2
Items: 
Size: 609532 Color: 1
Size: 390120 Color: 0

Bin 3404: 350 of cap free
Amount of items: 2
Items: 
Size: 701093 Color: 1
Size: 298558 Color: 0

Bin 3405: 350 of cap free
Amount of items: 2
Items: 
Size: 730366 Color: 0
Size: 269285 Color: 1

Bin 3406: 350 of cap free
Amount of items: 2
Items: 
Size: 729322 Color: 0
Size: 270329 Color: 1

Bin 3407: 351 of cap free
Amount of items: 2
Items: 
Size: 603739 Color: 1
Size: 395911 Color: 0

Bin 3408: 353 of cap free
Amount of items: 2
Items: 
Size: 600302 Color: 0
Size: 399346 Color: 1

Bin 3409: 353 of cap free
Amount of items: 2
Items: 
Size: 576503 Color: 1
Size: 423145 Color: 0

Bin 3410: 354 of cap free
Amount of items: 2
Items: 
Size: 608735 Color: 0
Size: 390912 Color: 1

Bin 3411: 354 of cap free
Amount of items: 2
Items: 
Size: 604458 Color: 1
Size: 395189 Color: 0

Bin 3412: 354 of cap free
Amount of items: 2
Items: 
Size: 636888 Color: 1
Size: 362759 Color: 0

Bin 3413: 355 of cap free
Amount of items: 2
Items: 
Size: 660361 Color: 1
Size: 339285 Color: 0

Bin 3414: 355 of cap free
Amount of items: 2
Items: 
Size: 701817 Color: 0
Size: 297829 Color: 1

Bin 3415: 357 of cap free
Amount of items: 2
Items: 
Size: 622449 Color: 0
Size: 377195 Color: 1

Bin 3416: 358 of cap free
Amount of items: 2
Items: 
Size: 703533 Color: 1
Size: 296110 Color: 0

Bin 3417: 360 of cap free
Amount of items: 2
Items: 
Size: 532197 Color: 1
Size: 467444 Color: 0

Bin 3418: 361 of cap free
Amount of items: 2
Items: 
Size: 668448 Color: 0
Size: 331192 Color: 1

Bin 3419: 362 of cap free
Amount of items: 2
Items: 
Size: 717643 Color: 1
Size: 281996 Color: 0

Bin 3420: 362 of cap free
Amount of items: 2
Items: 
Size: 781138 Color: 1
Size: 218501 Color: 0

Bin 3421: 362 of cap free
Amount of items: 2
Items: 
Size: 639789 Color: 1
Size: 359850 Color: 0

Bin 3422: 363 of cap free
Amount of items: 2
Items: 
Size: 755365 Color: 1
Size: 244273 Color: 0

Bin 3423: 363 of cap free
Amount of items: 2
Items: 
Size: 692367 Color: 1
Size: 307271 Color: 0

Bin 3424: 364 of cap free
Amount of items: 2
Items: 
Size: 571832 Color: 0
Size: 427805 Color: 1

Bin 3425: 365 of cap free
Amount of items: 2
Items: 
Size: 525426 Color: 0
Size: 474210 Color: 1

Bin 3426: 365 of cap free
Amount of items: 2
Items: 
Size: 502904 Color: 0
Size: 496732 Color: 1

Bin 3427: 365 of cap free
Amount of items: 2
Items: 
Size: 508713 Color: 0
Size: 490923 Color: 1

Bin 3428: 366 of cap free
Amount of items: 3
Items: 
Size: 699941 Color: 0
Size: 160144 Color: 0
Size: 139550 Color: 1

Bin 3429: 367 of cap free
Amount of items: 2
Items: 
Size: 724752 Color: 0
Size: 274882 Color: 1

Bin 3430: 368 of cap free
Amount of items: 2
Items: 
Size: 774450 Color: 1
Size: 225183 Color: 0

Bin 3431: 370 of cap free
Amount of items: 2
Items: 
Size: 539593 Color: 1
Size: 460038 Color: 0

Bin 3432: 370 of cap free
Amount of items: 2
Items: 
Size: 541480 Color: 1
Size: 458151 Color: 0

Bin 3433: 370 of cap free
Amount of items: 2
Items: 
Size: 544410 Color: 1
Size: 455221 Color: 0

Bin 3434: 372 of cap free
Amount of items: 2
Items: 
Size: 545755 Color: 0
Size: 453874 Color: 1

Bin 3435: 372 of cap free
Amount of items: 2
Items: 
Size: 605894 Color: 1
Size: 393735 Color: 0

Bin 3436: 372 of cap free
Amount of items: 2
Items: 
Size: 618814 Color: 1
Size: 380815 Color: 0

Bin 3437: 373 of cap free
Amount of items: 2
Items: 
Size: 618237 Color: 1
Size: 381391 Color: 0

Bin 3438: 373 of cap free
Amount of items: 2
Items: 
Size: 745576 Color: 0
Size: 254052 Color: 1

Bin 3439: 373 of cap free
Amount of items: 2
Items: 
Size: 784256 Color: 1
Size: 215372 Color: 0

Bin 3440: 374 of cap free
Amount of items: 2
Items: 
Size: 673155 Color: 0
Size: 326472 Color: 1

Bin 3441: 374 of cap free
Amount of items: 2
Items: 
Size: 637626 Color: 1
Size: 362001 Color: 0

Bin 3442: 374 of cap free
Amount of items: 2
Items: 
Size: 512676 Color: 0
Size: 486951 Color: 1

Bin 3443: 374 of cap free
Amount of items: 2
Items: 
Size: 651824 Color: 1
Size: 347803 Color: 0

Bin 3444: 375 of cap free
Amount of items: 2
Items: 
Size: 707386 Color: 0
Size: 292240 Color: 1

Bin 3445: 375 of cap free
Amount of items: 2
Items: 
Size: 533125 Color: 1
Size: 466501 Color: 0

Bin 3446: 376 of cap free
Amount of items: 2
Items: 
Size: 625890 Color: 0
Size: 373735 Color: 1

Bin 3447: 378 of cap free
Amount of items: 3
Items: 
Size: 586804 Color: 0
Size: 256994 Color: 1
Size: 155825 Color: 0

Bin 3448: 378 of cap free
Amount of items: 2
Items: 
Size: 770092 Color: 0
Size: 229531 Color: 1

Bin 3449: 379 of cap free
Amount of items: 2
Items: 
Size: 770680 Color: 0
Size: 228942 Color: 1

Bin 3450: 379 of cap free
Amount of items: 2
Items: 
Size: 625162 Color: 1
Size: 374460 Color: 0

Bin 3451: 381 of cap free
Amount of items: 2
Items: 
Size: 660971 Color: 1
Size: 338649 Color: 0

Bin 3452: 382 of cap free
Amount of items: 2
Items: 
Size: 755788 Color: 0
Size: 243831 Color: 1

Bin 3453: 383 of cap free
Amount of items: 2
Items: 
Size: 685793 Color: 0
Size: 313825 Color: 1

Bin 3454: 383 of cap free
Amount of items: 2
Items: 
Size: 522669 Color: 0
Size: 476949 Color: 1

Bin 3455: 383 of cap free
Amount of items: 2
Items: 
Size: 568396 Color: 0
Size: 431222 Color: 1

Bin 3456: 384 of cap free
Amount of items: 2
Items: 
Size: 717091 Color: 0
Size: 282526 Color: 1

Bin 3457: 384 of cap free
Amount of items: 2
Items: 
Size: 606519 Color: 0
Size: 393098 Color: 1

Bin 3458: 384 of cap free
Amount of items: 2
Items: 
Size: 513181 Color: 1
Size: 486436 Color: 0

Bin 3459: 384 of cap free
Amount of items: 2
Items: 
Size: 575070 Color: 1
Size: 424547 Color: 0

Bin 3460: 385 of cap free
Amount of items: 2
Items: 
Size: 615033 Color: 0
Size: 384583 Color: 1

Bin 3461: 387 of cap free
Amount of items: 2
Items: 
Size: 580035 Color: 0
Size: 419579 Color: 1

Bin 3462: 387 of cap free
Amount of items: 2
Items: 
Size: 619960 Color: 1
Size: 379654 Color: 0

Bin 3463: 387 of cap free
Amount of items: 2
Items: 
Size: 651449 Color: 0
Size: 348165 Color: 1

Bin 3464: 388 of cap free
Amount of items: 2
Items: 
Size: 594923 Color: 0
Size: 404690 Color: 1

Bin 3465: 388 of cap free
Amount of items: 2
Items: 
Size: 666583 Color: 1
Size: 333030 Color: 0

Bin 3466: 388 of cap free
Amount of items: 2
Items: 
Size: 683682 Color: 1
Size: 315931 Color: 0

Bin 3467: 389 of cap free
Amount of items: 2
Items: 
Size: 748404 Color: 1
Size: 251208 Color: 0

Bin 3468: 391 of cap free
Amount of items: 2
Items: 
Size: 619524 Color: 0
Size: 380086 Color: 1

Bin 3469: 391 of cap free
Amount of items: 2
Items: 
Size: 726171 Color: 0
Size: 273439 Color: 1

Bin 3470: 392 of cap free
Amount of items: 2
Items: 
Size: 605890 Color: 1
Size: 393719 Color: 0

Bin 3471: 392 of cap free
Amount of items: 2
Items: 
Size: 590551 Color: 1
Size: 409058 Color: 0

Bin 3472: 395 of cap free
Amount of items: 2
Items: 
Size: 604428 Color: 1
Size: 395178 Color: 0

Bin 3473: 395 of cap free
Amount of items: 2
Items: 
Size: 534766 Color: 0
Size: 464840 Color: 1

Bin 3474: 395 of cap free
Amount of items: 2
Items: 
Size: 529982 Color: 1
Size: 469624 Color: 0

Bin 3475: 396 of cap free
Amount of items: 3
Items: 
Size: 574036 Color: 1
Size: 287124 Color: 0
Size: 138445 Color: 1

Bin 3476: 396 of cap free
Amount of items: 2
Items: 
Size: 720715 Color: 0
Size: 278890 Color: 1

Bin 3477: 398 of cap free
Amount of items: 2
Items: 
Size: 712150 Color: 0
Size: 287453 Color: 1

Bin 3478: 399 of cap free
Amount of items: 2
Items: 
Size: 668429 Color: 0
Size: 331173 Color: 1

Bin 3479: 399 of cap free
Amount of items: 2
Items: 
Size: 632953 Color: 0
Size: 366649 Color: 1

Bin 3480: 400 of cap free
Amount of items: 2
Items: 
Size: 513823 Color: 1
Size: 485778 Color: 0

Bin 3481: 400 of cap free
Amount of items: 2
Items: 
Size: 521120 Color: 0
Size: 478481 Color: 1

Bin 3482: 400 of cap free
Amount of items: 2
Items: 
Size: 544746 Color: 0
Size: 454855 Color: 1

Bin 3483: 401 of cap free
Amount of items: 2
Items: 
Size: 600256 Color: 0
Size: 399344 Color: 1

Bin 3484: 401 of cap free
Amount of items: 2
Items: 
Size: 640550 Color: 0
Size: 359050 Color: 1

Bin 3485: 402 of cap free
Amount of items: 2
Items: 
Size: 750311 Color: 0
Size: 249288 Color: 1

Bin 3486: 405 of cap free
Amount of items: 2
Items: 
Size: 793549 Color: 1
Size: 206047 Color: 0

Bin 3487: 406 of cap free
Amount of items: 2
Items: 
Size: 517390 Color: 1
Size: 482205 Color: 0

Bin 3488: 406 of cap free
Amount of items: 2
Items: 
Size: 596438 Color: 1
Size: 403157 Color: 0

Bin 3489: 407 of cap free
Amount of items: 2
Items: 
Size: 521599 Color: 0
Size: 477995 Color: 1

Bin 3490: 407 of cap free
Amount of items: 2
Items: 
Size: 747639 Color: 0
Size: 251955 Color: 1

Bin 3491: 408 of cap free
Amount of items: 2
Items: 
Size: 781445 Color: 0
Size: 218148 Color: 1

Bin 3492: 409 of cap free
Amount of items: 2
Items: 
Size: 695160 Color: 1
Size: 304432 Color: 0

Bin 3493: 410 of cap free
Amount of items: 2
Items: 
Size: 723266 Color: 1
Size: 276325 Color: 0

Bin 3494: 410 of cap free
Amount of items: 2
Items: 
Size: 782599 Color: 0
Size: 216992 Color: 1

Bin 3495: 410 of cap free
Amount of items: 2
Items: 
Size: 608126 Color: 1
Size: 391465 Color: 0

Bin 3496: 410 of cap free
Amount of items: 2
Items: 
Size: 645159 Color: 0
Size: 354432 Color: 1

Bin 3497: 411 of cap free
Amount of items: 2
Items: 
Size: 795876 Color: 0
Size: 203714 Color: 1

Bin 3498: 411 of cap free
Amount of items: 2
Items: 
Size: 612450 Color: 0
Size: 387140 Color: 1

Bin 3499: 411 of cap free
Amount of items: 2
Items: 
Size: 660323 Color: 1
Size: 339267 Color: 0

Bin 3500: 412 of cap free
Amount of items: 2
Items: 
Size: 788538 Color: 0
Size: 211051 Color: 1

Bin 3501: 413 of cap free
Amount of items: 2
Items: 
Size: 761799 Color: 0
Size: 237789 Color: 1

Bin 3502: 414 of cap free
Amount of items: 2
Items: 
Size: 774420 Color: 1
Size: 225167 Color: 0

Bin 3503: 414 of cap free
Amount of items: 2
Items: 
Size: 746598 Color: 1
Size: 252989 Color: 0

Bin 3504: 415 of cap free
Amount of items: 2
Items: 
Size: 517427 Color: 0
Size: 482159 Color: 1

Bin 3505: 415 of cap free
Amount of items: 2
Items: 
Size: 608651 Color: 1
Size: 390935 Color: 0

Bin 3506: 416 of cap free
Amount of items: 2
Items: 
Size: 555939 Color: 1
Size: 443646 Color: 0

Bin 3507: 416 of cap free
Amount of items: 2
Items: 
Size: 761804 Color: 1
Size: 237781 Color: 0

Bin 3508: 416 of cap free
Amount of items: 2
Items: 
Size: 631802 Color: 0
Size: 367783 Color: 1

Bin 3509: 416 of cap free
Amount of items: 2
Items: 
Size: 676360 Color: 0
Size: 323225 Color: 1

Bin 3510: 418 of cap free
Amount of items: 2
Items: 
Size: 701799 Color: 0
Size: 297784 Color: 1

Bin 3511: 420 of cap free
Amount of items: 2
Items: 
Size: 552884 Color: 0
Size: 446697 Color: 1

Bin 3512: 421 of cap free
Amount of items: 2
Items: 
Size: 686898 Color: 0
Size: 312682 Color: 1

Bin 3513: 422 of cap free
Amount of items: 2
Items: 
Size: 614935 Color: 1
Size: 384644 Color: 0

Bin 3514: 425 of cap free
Amount of items: 2
Items: 
Size: 617902 Color: 0
Size: 381674 Color: 1

Bin 3515: 427 of cap free
Amount of items: 2
Items: 
Size: 545304 Color: 1
Size: 454270 Color: 0

Bin 3516: 429 of cap free
Amount of items: 2
Items: 
Size: 502123 Color: 1
Size: 497449 Color: 0

Bin 3517: 429 of cap free
Amount of items: 2
Items: 
Size: 555434 Color: 0
Size: 444138 Color: 1

Bin 3518: 429 of cap free
Amount of items: 2
Items: 
Size: 560783 Color: 1
Size: 438789 Color: 0

Bin 3519: 429 of cap free
Amount of items: 2
Items: 
Size: 678235 Color: 1
Size: 321337 Color: 0

Bin 3520: 429 of cap free
Amount of items: 2
Items: 
Size: 722389 Color: 1
Size: 277183 Color: 0

Bin 3521: 433 of cap free
Amount of items: 2
Items: 
Size: 713790 Color: 1
Size: 285778 Color: 0

Bin 3522: 434 of cap free
Amount of items: 2
Items: 
Size: 510503 Color: 1
Size: 489064 Color: 0

Bin 3523: 436 of cap free
Amount of items: 2
Items: 
Size: 733981 Color: 1
Size: 265584 Color: 0

Bin 3524: 437 of cap free
Amount of items: 2
Items: 
Size: 699428 Color: 1
Size: 300136 Color: 0

Bin 3525: 438 of cap free
Amount of items: 2
Items: 
Size: 777732 Color: 0
Size: 221831 Color: 1

Bin 3526: 439 of cap free
Amount of items: 2
Items: 
Size: 586675 Color: 1
Size: 412887 Color: 0

Bin 3527: 439 of cap free
Amount of items: 2
Items: 
Size: 604375 Color: 0
Size: 395187 Color: 1

Bin 3528: 439 of cap free
Amount of items: 2
Items: 
Size: 614381 Color: 0
Size: 385181 Color: 1

Bin 3529: 440 of cap free
Amount of items: 2
Items: 
Size: 519510 Color: 0
Size: 480051 Color: 1

Bin 3530: 442 of cap free
Amount of items: 2
Items: 
Size: 517367 Color: 1
Size: 482192 Color: 0

Bin 3531: 442 of cap free
Amount of items: 2
Items: 
Size: 643067 Color: 0
Size: 356492 Color: 1

Bin 3532: 443 of cap free
Amount of items: 2
Items: 
Size: 729492 Color: 1
Size: 270066 Color: 0

Bin 3533: 443 of cap free
Amount of items: 2
Items: 
Size: 759695 Color: 0
Size: 239863 Color: 1

Bin 3534: 443 of cap free
Amount of items: 2
Items: 
Size: 779078 Color: 0
Size: 220480 Color: 1

Bin 3535: 445 of cap free
Amount of items: 2
Items: 
Size: 597374 Color: 0
Size: 402182 Color: 1

Bin 3536: 445 of cap free
Amount of items: 2
Items: 
Size: 790607 Color: 1
Size: 208949 Color: 0

Bin 3537: 445 of cap free
Amount of items: 2
Items: 
Size: 715049 Color: 0
Size: 284507 Color: 1

Bin 3538: 446 of cap free
Amount of items: 2
Items: 
Size: 763752 Color: 1
Size: 235803 Color: 0

Bin 3539: 447 of cap free
Amount of items: 2
Items: 
Size: 728332 Color: 1
Size: 271222 Color: 0

Bin 3540: 447 of cap free
Amount of items: 2
Items: 
Size: 798127 Color: 0
Size: 201427 Color: 1

Bin 3541: 450 of cap free
Amount of items: 2
Items: 
Size: 550695 Color: 0
Size: 448856 Color: 1

Bin 3542: 450 of cap free
Amount of items: 2
Items: 
Size: 654333 Color: 1
Size: 345218 Color: 0

Bin 3543: 451 of cap free
Amount of items: 2
Items: 
Size: 785539 Color: 1
Size: 214011 Color: 0

Bin 3544: 452 of cap free
Amount of items: 2
Items: 
Size: 516758 Color: 0
Size: 482791 Color: 1

Bin 3545: 452 of cap free
Amount of items: 2
Items: 
Size: 541475 Color: 1
Size: 458074 Color: 0

Bin 3546: 452 of cap free
Amount of items: 2
Items: 
Size: 644904 Color: 1
Size: 354645 Color: 0

Bin 3547: 453 of cap free
Amount of items: 3
Items: 
Size: 337654 Color: 0
Size: 337575 Color: 0
Size: 324319 Color: 1

Bin 3548: 453 of cap free
Amount of items: 2
Items: 
Size: 607047 Color: 1
Size: 392501 Color: 0

Bin 3549: 453 of cap free
Amount of items: 2
Items: 
Size: 715042 Color: 0
Size: 284506 Color: 1

Bin 3550: 454 of cap free
Amount of items: 2
Items: 
Size: 628513 Color: 0
Size: 371034 Color: 1

Bin 3551: 455 of cap free
Amount of items: 2
Items: 
Size: 728326 Color: 1
Size: 271220 Color: 0

Bin 3552: 456 of cap free
Amount of items: 2
Items: 
Size: 677002 Color: 0
Size: 322543 Color: 1

Bin 3553: 456 of cap free
Amount of items: 2
Items: 
Size: 546605 Color: 0
Size: 452940 Color: 1

Bin 3554: 459 of cap free
Amount of items: 2
Items: 
Size: 699229 Color: 0
Size: 300313 Color: 1

Bin 3555: 459 of cap free
Amount of items: 2
Items: 
Size: 785951 Color: 0
Size: 213591 Color: 1

Bin 3556: 460 of cap free
Amount of items: 2
Items: 
Size: 717807 Color: 0
Size: 281734 Color: 1

Bin 3557: 462 of cap free
Amount of items: 2
Items: 
Size: 667350 Color: 1
Size: 332189 Color: 0

Bin 3558: 463 of cap free
Amount of items: 2
Items: 
Size: 735450 Color: 1
Size: 264088 Color: 0

Bin 3559: 464 of cap free
Amount of items: 3
Items: 
Size: 675013 Color: 0
Size: 173521 Color: 1
Size: 151003 Color: 1

Bin 3560: 464 of cap free
Amount of items: 2
Items: 
Size: 763116 Color: 1
Size: 236421 Color: 0

Bin 3561: 465 of cap free
Amount of items: 2
Items: 
Size: 580662 Color: 1
Size: 418874 Color: 0

Bin 3562: 465 of cap free
Amount of items: 2
Items: 
Size: 630780 Color: 0
Size: 368756 Color: 1

Bin 3563: 465 of cap free
Amount of items: 2
Items: 
Size: 632378 Color: 1
Size: 367158 Color: 0

Bin 3564: 466 of cap free
Amount of items: 2
Items: 
Size: 784989 Color: 1
Size: 214546 Color: 0

Bin 3565: 466 of cap free
Amount of items: 2
Items: 
Size: 522632 Color: 0
Size: 476903 Color: 1

Bin 3566: 467 of cap free
Amount of items: 2
Items: 
Size: 573277 Color: 1
Size: 426257 Color: 0

Bin 3567: 468 of cap free
Amount of items: 2
Items: 
Size: 702557 Color: 1
Size: 296976 Color: 0

Bin 3568: 468 of cap free
Amount of items: 2
Items: 
Size: 600201 Color: 1
Size: 399332 Color: 0

Bin 3569: 469 of cap free
Amount of items: 2
Items: 
Size: 624976 Color: 0
Size: 374556 Color: 1

Bin 3570: 471 of cap free
Amount of items: 2
Items: 
Size: 507570 Color: 1
Size: 491960 Color: 0

Bin 3571: 471 of cap free
Amount of items: 2
Items: 
Size: 512342 Color: 1
Size: 487188 Color: 0

Bin 3572: 471 of cap free
Amount of items: 2
Items: 
Size: 722843 Color: 0
Size: 276687 Color: 1

Bin 3573: 472 of cap free
Amount of items: 2
Items: 
Size: 563197 Color: 1
Size: 436332 Color: 0

Bin 3574: 473 of cap free
Amount of items: 2
Items: 
Size: 657386 Color: 0
Size: 342142 Color: 1

Bin 3575: 473 of cap free
Amount of items: 2
Items: 
Size: 652126 Color: 0
Size: 347402 Color: 1

Bin 3576: 475 of cap free
Amount of items: 2
Items: 
Size: 628188 Color: 1
Size: 371338 Color: 0

Bin 3577: 475 of cap free
Amount of items: 2
Items: 
Size: 505526 Color: 1
Size: 494000 Color: 0

Bin 3578: 476 of cap free
Amount of items: 2
Items: 
Size: 782159 Color: 1
Size: 217366 Color: 0

Bin 3579: 476 of cap free
Amount of items: 2
Items: 
Size: 534588 Color: 1
Size: 464937 Color: 0

Bin 3580: 477 of cap free
Amount of items: 2
Items: 
Size: 552834 Color: 0
Size: 446690 Color: 1

Bin 3581: 478 of cap free
Amount of items: 2
Items: 
Size: 570974 Color: 0
Size: 428549 Color: 1

Bin 3582: 480 of cap free
Amount of items: 2
Items: 
Size: 781021 Color: 1
Size: 218500 Color: 0

Bin 3583: 480 of cap free
Amount of items: 2
Items: 
Size: 564255 Color: 0
Size: 435266 Color: 1

Bin 3584: 481 of cap free
Amount of items: 2
Items: 
Size: 647400 Color: 0
Size: 352120 Color: 1

Bin 3585: 482 of cap free
Amount of items: 2
Items: 
Size: 586956 Color: 0
Size: 412563 Color: 1

Bin 3586: 482 of cap free
Amount of items: 2
Items: 
Size: 601259 Color: 1
Size: 398260 Color: 0

Bin 3587: 483 of cap free
Amount of items: 2
Items: 
Size: 563135 Color: 0
Size: 436383 Color: 1

Bin 3588: 483 of cap free
Amount of items: 2
Items: 
Size: 777751 Color: 1
Size: 221767 Color: 0

Bin 3589: 484 of cap free
Amount of items: 2
Items: 
Size: 525826 Color: 0
Size: 473691 Color: 1

Bin 3590: 484 of cap free
Amount of items: 2
Items: 
Size: 518173 Color: 1
Size: 481344 Color: 0

Bin 3591: 484 of cap free
Amount of items: 2
Items: 
Size: 522182 Color: 1
Size: 477335 Color: 0

Bin 3592: 484 of cap free
Amount of items: 2
Items: 
Size: 554882 Color: 0
Size: 444635 Color: 1

Bin 3593: 486 of cap free
Amount of items: 2
Items: 
Size: 532172 Color: 1
Size: 467343 Color: 0

Bin 3594: 486 of cap free
Amount of items: 2
Items: 
Size: 779736 Color: 0
Size: 219779 Color: 1

Bin 3595: 488 of cap free
Amount of items: 2
Items: 
Size: 730339 Color: 0
Size: 269174 Color: 1

Bin 3596: 488 of cap free
Amount of items: 2
Items: 
Size: 532705 Color: 0
Size: 466808 Color: 1

Bin 3597: 492 of cap free
Amount of items: 2
Items: 
Size: 597964 Color: 0
Size: 401545 Color: 1

Bin 3598: 492 of cap free
Amount of items: 2
Items: 
Size: 675785 Color: 1
Size: 323724 Color: 0

Bin 3599: 494 of cap free
Amount of items: 2
Items: 
Size: 783402 Color: 1
Size: 216105 Color: 0

Bin 3600: 494 of cap free
Amount of items: 2
Items: 
Size: 730121 Color: 1
Size: 269386 Color: 0

Bin 3601: 495 of cap free
Amount of items: 2
Items: 
Size: 531184 Color: 0
Size: 468322 Color: 1

Bin 3602: 495 of cap free
Amount of items: 2
Items: 
Size: 636161 Color: 1
Size: 363345 Color: 0

Bin 3603: 496 of cap free
Amount of items: 2
Items: 
Size: 507918 Color: 0
Size: 491587 Color: 1

Bin 3604: 496 of cap free
Amount of items: 2
Items: 
Size: 665583 Color: 1
Size: 333922 Color: 0

Bin 3605: 497 of cap free
Amount of items: 2
Items: 
Size: 723202 Color: 1
Size: 276302 Color: 0

Bin 3606: 497 of cap free
Amount of items: 3
Items: 
Size: 603872 Color: 0
Size: 224517 Color: 1
Size: 171115 Color: 1

Bin 3607: 497 of cap free
Amount of items: 2
Items: 
Size: 608623 Color: 1
Size: 390881 Color: 0

Bin 3608: 498 of cap free
Amount of items: 2
Items: 
Size: 620494 Color: 0
Size: 379009 Color: 1

Bin 3609: 500 of cap free
Amount of items: 2
Items: 
Size: 704235 Color: 1
Size: 295266 Color: 0

Bin 3610: 502 of cap free
Amount of items: 2
Items: 
Size: 732362 Color: 0
Size: 267137 Color: 1

Bin 3611: 502 of cap free
Amount of items: 2
Items: 
Size: 737425 Color: 0
Size: 262074 Color: 1

Bin 3612: 505 of cap free
Amount of items: 2
Items: 
Size: 707298 Color: 0
Size: 292198 Color: 1

Bin 3613: 507 of cap free
Amount of items: 2
Items: 
Size: 564235 Color: 0
Size: 435259 Color: 1

Bin 3614: 508 of cap free
Amount of items: 2
Items: 
Size: 510516 Color: 0
Size: 488977 Color: 1

Bin 3615: 509 of cap free
Amount of items: 2
Items: 
Size: 667339 Color: 1
Size: 332153 Color: 0

Bin 3616: 510 of cap free
Amount of items: 2
Items: 
Size: 582533 Color: 0
Size: 416958 Color: 1

Bin 3617: 513 of cap free
Amount of items: 2
Items: 
Size: 784988 Color: 1
Size: 214500 Color: 0

Bin 3618: 513 of cap free
Amount of items: 2
Items: 
Size: 541315 Color: 0
Size: 458173 Color: 1

Bin 3619: 514 of cap free
Amount of items: 2
Items: 
Size: 734892 Color: 1
Size: 264595 Color: 0

Bin 3620: 514 of cap free
Amount of items: 2
Items: 
Size: 525339 Color: 1
Size: 474148 Color: 0

Bin 3621: 514 of cap free
Amount of items: 2
Items: 
Size: 556116 Color: 0
Size: 443371 Color: 1

Bin 3622: 514 of cap free
Amount of items: 2
Items: 
Size: 664242 Color: 1
Size: 335245 Color: 0

Bin 3623: 516 of cap free
Amount of items: 2
Items: 
Size: 617825 Color: 0
Size: 381660 Color: 1

Bin 3624: 516 of cap free
Amount of items: 2
Items: 
Size: 793455 Color: 1
Size: 206030 Color: 0

Bin 3625: 516 of cap free
Amount of items: 2
Items: 
Size: 661477 Color: 1
Size: 338008 Color: 0

Bin 3626: 524 of cap free
Amount of items: 2
Items: 
Size: 638557 Color: 0
Size: 360920 Color: 1

Bin 3627: 524 of cap free
Amount of items: 2
Items: 
Size: 770072 Color: 0
Size: 229405 Color: 1

Bin 3628: 526 of cap free
Amount of items: 2
Items: 
Size: 775826 Color: 0
Size: 223649 Color: 1

Bin 3629: 528 of cap free
Amount of items: 2
Items: 
Size: 760557 Color: 1
Size: 238916 Color: 0

Bin 3630: 528 of cap free
Amount of items: 2
Items: 
Size: 561565 Color: 1
Size: 437908 Color: 0

Bin 3631: 529 of cap free
Amount of items: 2
Items: 
Size: 528305 Color: 1
Size: 471167 Color: 0

Bin 3632: 532 of cap free
Amount of items: 2
Items: 
Size: 773748 Color: 0
Size: 225721 Color: 1

Bin 3633: 532 of cap free
Amount of items: 2
Items: 
Size: 605799 Color: 1
Size: 393670 Color: 0

Bin 3634: 533 of cap free
Amount of items: 2
Items: 
Size: 736217 Color: 0
Size: 263251 Color: 1

Bin 3635: 533 of cap free
Amount of items: 2
Items: 
Size: 637428 Color: 0
Size: 362040 Color: 1

Bin 3636: 534 of cap free
Amount of items: 2
Items: 
Size: 550241 Color: 1
Size: 449226 Color: 0

Bin 3637: 534 of cap free
Amount of items: 2
Items: 
Size: 619878 Color: 1
Size: 379589 Color: 0

Bin 3638: 536 of cap free
Amount of items: 2
Items: 
Size: 532705 Color: 0
Size: 466760 Color: 1

Bin 3639: 537 of cap free
Amount of items: 2
Items: 
Size: 732191 Color: 1
Size: 267273 Color: 0

Bin 3640: 538 of cap free
Amount of items: 2
Items: 
Size: 782496 Color: 0
Size: 216967 Color: 1

Bin 3641: 538 of cap free
Amount of items: 2
Items: 
Size: 570402 Color: 1
Size: 429061 Color: 0

Bin 3642: 539 of cap free
Amount of items: 2
Items: 
Size: 619492 Color: 0
Size: 379970 Color: 1

Bin 3643: 541 of cap free
Amount of items: 2
Items: 
Size: 774393 Color: 1
Size: 225067 Color: 0

Bin 3644: 544 of cap free
Amount of items: 2
Items: 
Size: 669538 Color: 0
Size: 329919 Color: 1

Bin 3645: 545 of cap free
Amount of items: 2
Items: 
Size: 685782 Color: 0
Size: 313674 Color: 1

Bin 3646: 546 of cap free
Amount of items: 2
Items: 
Size: 624103 Color: 1
Size: 375352 Color: 0

Bin 3647: 546 of cap free
Amount of items: 2
Items: 
Size: 654300 Color: 1
Size: 345155 Color: 0

Bin 3648: 547 of cap free
Amount of items: 2
Items: 
Size: 523877 Color: 0
Size: 475577 Color: 1

Bin 3649: 549 of cap free
Amount of items: 2
Items: 
Size: 623027 Color: 1
Size: 376425 Color: 0

Bin 3650: 551 of cap free
Amount of items: 2
Items: 
Size: 636141 Color: 1
Size: 363309 Color: 0

Bin 3651: 552 of cap free
Amount of items: 2
Items: 
Size: 546135 Color: 1
Size: 453314 Color: 0

Bin 3652: 552 of cap free
Amount of items: 2
Items: 
Size: 618484 Color: 0
Size: 380965 Color: 1

Bin 3653: 553 of cap free
Amount of items: 2
Items: 
Size: 743776 Color: 0
Size: 255672 Color: 1

Bin 3654: 553 of cap free
Amount of items: 2
Items: 
Size: 665410 Color: 0
Size: 334038 Color: 1

Bin 3655: 554 of cap free
Amount of items: 2
Items: 
Size: 552030 Color: 0
Size: 447417 Color: 1

Bin 3656: 556 of cap free
Amount of items: 2
Items: 
Size: 616824 Color: 0
Size: 382621 Color: 1

Bin 3657: 556 of cap free
Amount of items: 2
Items: 
Size: 601751 Color: 0
Size: 397694 Color: 1

Bin 3658: 556 of cap free
Amount of items: 2
Items: 
Size: 524175 Color: 1
Size: 475270 Color: 0

Bin 3659: 556 of cap free
Amount of items: 2
Items: 
Size: 683568 Color: 1
Size: 315877 Color: 0

Bin 3660: 557 of cap free
Amount of items: 2
Items: 
Size: 617804 Color: 0
Size: 381640 Color: 1

Bin 3661: 566 of cap free
Amount of items: 2
Items: 
Size: 568322 Color: 1
Size: 431113 Color: 0

Bin 3662: 567 of cap free
Amount of items: 2
Items: 
Size: 740590 Color: 0
Size: 258844 Color: 1

Bin 3663: 567 of cap free
Amount of items: 2
Items: 
Size: 719542 Color: 0
Size: 279892 Color: 1

Bin 3664: 568 of cap free
Amount of items: 2
Items: 
Size: 723147 Color: 1
Size: 276286 Color: 0

Bin 3665: 570 of cap free
Amount of items: 2
Items: 
Size: 684035 Color: 0
Size: 315396 Color: 1

Bin 3666: 571 of cap free
Amount of items: 2
Items: 
Size: 742257 Color: 1
Size: 257173 Color: 0

Bin 3667: 571 of cap free
Amount of items: 2
Items: 
Size: 544269 Color: 1
Size: 455161 Color: 0

Bin 3668: 572 of cap free
Amount of items: 2
Items: 
Size: 560594 Color: 0
Size: 438835 Color: 1

Bin 3669: 572 of cap free
Amount of items: 2
Items: 
Size: 579915 Color: 0
Size: 419514 Color: 1

Bin 3670: 575 of cap free
Amount of items: 2
Items: 
Size: 748209 Color: 0
Size: 251217 Color: 1

Bin 3671: 575 of cap free
Amount of items: 2
Items: 
Size: 540706 Color: 0
Size: 458720 Color: 1

Bin 3672: 579 of cap free
Amount of items: 2
Items: 
Size: 525320 Color: 1
Size: 474102 Color: 0

Bin 3673: 579 of cap free
Amount of items: 2
Items: 
Size: 796344 Color: 0
Size: 203078 Color: 1

Bin 3674: 580 of cap free
Amount of items: 2
Items: 
Size: 713059 Color: 0
Size: 286362 Color: 1

Bin 3675: 581 of cap free
Amount of items: 2
Items: 
Size: 665534 Color: 1
Size: 333886 Color: 0

Bin 3676: 585 of cap free
Amount of items: 2
Items: 
Size: 612309 Color: 0
Size: 387107 Color: 1

Bin 3677: 589 of cap free
Amount of items: 2
Items: 
Size: 591832 Color: 1
Size: 407580 Color: 0

Bin 3678: 590 of cap free
Amount of items: 2
Items: 
Size: 634935 Color: 1
Size: 364476 Color: 0

Bin 3679: 590 of cap free
Amount of items: 2
Items: 
Size: 647093 Color: 1
Size: 352318 Color: 0

Bin 3680: 592 of cap free
Amount of items: 2
Items: 
Size: 725149 Color: 0
Size: 274260 Color: 1

Bin 3681: 594 of cap free
Amount of items: 2
Items: 
Size: 739742 Color: 1
Size: 259665 Color: 0

Bin 3682: 594 of cap free
Amount of items: 2
Items: 
Size: 643237 Color: 1
Size: 356170 Color: 0

Bin 3683: 596 of cap free
Amount of items: 2
Items: 
Size: 576938 Color: 1
Size: 422467 Color: 0

Bin 3684: 596 of cap free
Amount of items: 2
Items: 
Size: 650645 Color: 1
Size: 348760 Color: 0

Bin 3685: 597 of cap free
Amount of items: 2
Items: 
Size: 536914 Color: 0
Size: 462490 Color: 1

Bin 3686: 599 of cap free
Amount of items: 2
Items: 
Size: 671069 Color: 0
Size: 328333 Color: 1

Bin 3687: 601 of cap free
Amount of items: 2
Items: 
Size: 595805 Color: 1
Size: 403595 Color: 0

Bin 3688: 602 of cap free
Amount of items: 2
Items: 
Size: 599102 Color: 0
Size: 400297 Color: 1

Bin 3689: 603 of cap free
Amount of items: 2
Items: 
Size: 584921 Color: 1
Size: 414477 Color: 0

Bin 3690: 603 of cap free
Amount of items: 2
Items: 
Size: 528276 Color: 1
Size: 471122 Color: 0

Bin 3691: 603 of cap free
Amount of items: 2
Items: 
Size: 792763 Color: 1
Size: 206635 Color: 0

Bin 3692: 605 of cap free
Amount of items: 2
Items: 
Size: 676603 Color: 1
Size: 322793 Color: 0

Bin 3693: 610 of cap free
Amount of items: 2
Items: 
Size: 792626 Color: 0
Size: 206765 Color: 1

Bin 3694: 611 of cap free
Amount of items: 2
Items: 
Size: 585684 Color: 0
Size: 413706 Color: 1

Bin 3695: 611 of cap free
Amount of items: 2
Items: 
Size: 602747 Color: 1
Size: 396643 Color: 0

Bin 3696: 611 of cap free
Amount of items: 2
Items: 
Size: 747580 Color: 0
Size: 251810 Color: 1

Bin 3697: 612 of cap free
Amount of items: 2
Items: 
Size: 505380 Color: 0
Size: 494009 Color: 1

Bin 3698: 613 of cap free
Amount of items: 2
Items: 
Size: 762594 Color: 0
Size: 236794 Color: 1

Bin 3699: 613 of cap free
Amount of items: 2
Items: 
Size: 704126 Color: 1
Size: 295262 Color: 0

Bin 3700: 615 of cap free
Amount of items: 2
Items: 
Size: 550634 Color: 0
Size: 448752 Color: 1

Bin 3701: 615 of cap free
Amount of items: 2
Items: 
Size: 551327 Color: 1
Size: 448059 Color: 0

Bin 3702: 615 of cap free
Amount of items: 2
Items: 
Size: 654360 Color: 0
Size: 345026 Color: 1

Bin 3703: 615 of cap free
Amount of items: 2
Items: 
Size: 662938 Color: 1
Size: 336448 Color: 0

Bin 3704: 616 of cap free
Amount of items: 2
Items: 
Size: 574132 Color: 1
Size: 425253 Color: 0

Bin 3705: 616 of cap free
Amount of items: 2
Items: 
Size: 726159 Color: 1
Size: 273226 Color: 0

Bin 3706: 616 of cap free
Amount of items: 2
Items: 
Size: 523868 Color: 0
Size: 475517 Color: 1

Bin 3707: 618 of cap free
Amount of items: 2
Items: 
Size: 774366 Color: 1
Size: 225017 Color: 0

Bin 3708: 620 of cap free
Amount of items: 2
Items: 
Size: 738571 Color: 1
Size: 260810 Color: 0

Bin 3709: 621 of cap free
Amount of items: 2
Items: 
Size: 563024 Color: 0
Size: 436356 Color: 1

Bin 3710: 621 of cap free
Amount of items: 2
Items: 
Size: 683998 Color: 0
Size: 315382 Color: 1

Bin 3711: 624 of cap free
Amount of items: 2
Items: 
Size: 622421 Color: 0
Size: 376956 Color: 1

Bin 3712: 624 of cap free
Amount of items: 2
Items: 
Size: 530393 Color: 1
Size: 468984 Color: 0

Bin 3713: 625 of cap free
Amount of items: 2
Items: 
Size: 564915 Color: 0
Size: 434461 Color: 1

Bin 3714: 625 of cap free
Amount of items: 2
Items: 
Size: 773011 Color: 0
Size: 226365 Color: 1

Bin 3715: 627 of cap free
Amount of items: 2
Items: 
Size: 703482 Color: 1
Size: 295892 Color: 0

Bin 3716: 627 of cap free
Amount of items: 2
Items: 
Size: 750150 Color: 1
Size: 249224 Color: 0

Bin 3717: 628 of cap free
Amount of items: 2
Items: 
Size: 574026 Color: 0
Size: 425347 Color: 1

Bin 3718: 629 of cap free
Amount of items: 2
Items: 
Size: 729170 Color: 0
Size: 270202 Color: 1

Bin 3719: 629 of cap free
Amount of items: 2
Items: 
Size: 741655 Color: 0
Size: 257717 Color: 1

Bin 3720: 630 of cap free
Amount of items: 2
Items: 
Size: 719635 Color: 0
Size: 279736 Color: 1

Bin 3721: 631 of cap free
Amount of items: 2
Items: 
Size: 719546 Color: 1
Size: 279824 Color: 0

Bin 3722: 631 of cap free
Amount of items: 2
Items: 
Size: 564412 Color: 1
Size: 434958 Color: 0

Bin 3723: 633 of cap free
Amount of items: 2
Items: 
Size: 586829 Color: 0
Size: 412539 Color: 1

Bin 3724: 634 of cap free
Amount of items: 2
Items: 
Size: 726152 Color: 1
Size: 273215 Color: 0

Bin 3725: 636 of cap free
Amount of items: 2
Items: 
Size: 709695 Color: 1
Size: 289670 Color: 0

Bin 3726: 637 of cap free
Amount of items: 2
Items: 
Size: 632257 Color: 1
Size: 367107 Color: 0

Bin 3727: 638 of cap free
Amount of items: 2
Items: 
Size: 731721 Color: 0
Size: 267642 Color: 1

Bin 3728: 639 of cap free
Amount of items: 2
Items: 
Size: 746648 Color: 0
Size: 252714 Color: 1

Bin 3729: 639 of cap free
Amount of items: 2
Items: 
Size: 657804 Color: 1
Size: 341558 Color: 0

Bin 3730: 641 of cap free
Amount of items: 2
Items: 
Size: 560617 Color: 1
Size: 438743 Color: 0

Bin 3731: 643 of cap free
Amount of items: 2
Items: 
Size: 528584 Color: 0
Size: 470774 Color: 1

Bin 3732: 646 of cap free
Amount of items: 2
Items: 
Size: 748953 Color: 0
Size: 250402 Color: 1

Bin 3733: 646 of cap free
Amount of items: 2
Items: 
Size: 609297 Color: 1
Size: 390058 Color: 0

Bin 3734: 646 of cap free
Amount of items: 2
Items: 
Size: 652777 Color: 0
Size: 346578 Color: 1

Bin 3735: 646 of cap free
Amount of items: 2
Items: 
Size: 687976 Color: 0
Size: 311379 Color: 1

Bin 3736: 646 of cap free
Amount of items: 2
Items: 
Size: 759661 Color: 1
Size: 239694 Color: 0

Bin 3737: 647 of cap free
Amount of items: 2
Items: 
Size: 520967 Color: 1
Size: 478387 Color: 0

Bin 3738: 648 of cap free
Amount of items: 2
Items: 
Size: 665378 Color: 0
Size: 333975 Color: 1

Bin 3739: 649 of cap free
Amount of items: 2
Items: 
Size: 757272 Color: 1
Size: 242080 Color: 0

Bin 3740: 649 of cap free
Amount of items: 2
Items: 
Size: 737117 Color: 1
Size: 262235 Color: 0

Bin 3741: 654 of cap free
Amount of items: 2
Items: 
Size: 759586 Color: 0
Size: 239761 Color: 1

Bin 3742: 655 of cap free
Amount of items: 2
Items: 
Size: 501798 Color: 0
Size: 497548 Color: 1

Bin 3743: 656 of cap free
Amount of items: 2
Items: 
Size: 570366 Color: 1
Size: 428979 Color: 0

Bin 3744: 656 of cap free
Amount of items: 2
Items: 
Size: 676597 Color: 1
Size: 322748 Color: 0

Bin 3745: 657 of cap free
Amount of items: 2
Items: 
Size: 602540 Color: 0
Size: 396804 Color: 1

Bin 3746: 659 of cap free
Amount of items: 2
Items: 
Size: 793423 Color: 0
Size: 205919 Color: 1

Bin 3747: 660 of cap free
Amount of items: 2
Items: 
Size: 564167 Color: 0
Size: 435174 Color: 1

Bin 3748: 662 of cap free
Amount of items: 2
Items: 
Size: 658044 Color: 0
Size: 341295 Color: 1

Bin 3749: 663 of cap free
Amount of items: 2
Items: 
Size: 605788 Color: 1
Size: 393550 Color: 0

Bin 3750: 668 of cap free
Amount of items: 2
Items: 
Size: 637402 Color: 1
Size: 361931 Color: 0

Bin 3751: 669 of cap free
Amount of items: 2
Items: 
Size: 679220 Color: 1
Size: 320112 Color: 0

Bin 3752: 670 of cap free
Amount of items: 2
Items: 
Size: 690933 Color: 0
Size: 308398 Color: 1

Bin 3753: 673 of cap free
Amount of items: 2
Items: 
Size: 718554 Color: 1
Size: 280774 Color: 0

Bin 3754: 675 of cap free
Amount of items: 2
Items: 
Size: 771672 Color: 1
Size: 227654 Color: 0

Bin 3755: 676 of cap free
Amount of items: 2
Items: 
Size: 776526 Color: 1
Size: 222799 Color: 0

Bin 3756: 677 of cap free
Amount of items: 2
Items: 
Size: 650599 Color: 1
Size: 348725 Color: 0

Bin 3757: 678 of cap free
Amount of items: 2
Items: 
Size: 623932 Color: 0
Size: 375391 Color: 1

Bin 3758: 679 of cap free
Amount of items: 2
Items: 
Size: 740549 Color: 0
Size: 258773 Color: 1

Bin 3759: 680 of cap free
Amount of items: 2
Items: 
Size: 609279 Color: 1
Size: 390042 Color: 0

Bin 3760: 680 of cap free
Amount of items: 2
Items: 
Size: 727496 Color: 0
Size: 271825 Color: 1

Bin 3761: 680 of cap free
Amount of items: 2
Items: 
Size: 672719 Color: 1
Size: 326602 Color: 0

Bin 3762: 683 of cap free
Amount of items: 2
Items: 
Size: 560497 Color: 0
Size: 438821 Color: 1

Bin 3763: 683 of cap free
Amount of items: 2
Items: 
Size: 656564 Color: 0
Size: 342754 Color: 1

Bin 3764: 691 of cap free
Amount of items: 2
Items: 
Size: 515160 Color: 1
Size: 484150 Color: 0

Bin 3765: 691 of cap free
Amount of items: 2
Items: 
Size: 643183 Color: 1
Size: 356127 Color: 0

Bin 3766: 692 of cap free
Amount of items: 2
Items: 
Size: 678492 Color: 0
Size: 320817 Color: 1

Bin 3767: 695 of cap free
Amount of items: 2
Items: 
Size: 760417 Color: 1
Size: 238889 Color: 0

Bin 3768: 695 of cap free
Amount of items: 2
Items: 
Size: 738956 Color: 0
Size: 260350 Color: 1

Bin 3769: 695 of cap free
Amount of items: 2
Items: 
Size: 757405 Color: 0
Size: 241901 Color: 1

Bin 3770: 696 of cap free
Amount of items: 2
Items: 
Size: 564858 Color: 0
Size: 434447 Color: 1

Bin 3771: 699 of cap free
Amount of items: 2
Items: 
Size: 584864 Color: 1
Size: 414438 Color: 0

Bin 3772: 703 of cap free
Amount of items: 2
Items: 
Size: 718540 Color: 0
Size: 280758 Color: 1

Bin 3773: 703 of cap free
Amount of items: 2
Items: 
Size: 713560 Color: 1
Size: 285738 Color: 0

Bin 3774: 703 of cap free
Amount of items: 2
Items: 
Size: 568262 Color: 1
Size: 431036 Color: 0

Bin 3775: 704 of cap free
Amount of items: 2
Items: 
Size: 606275 Color: 0
Size: 393022 Color: 1

Bin 3776: 704 of cap free
Amount of items: 2
Items: 
Size: 745317 Color: 1
Size: 253980 Color: 0

Bin 3777: 704 of cap free
Amount of items: 2
Items: 
Size: 502712 Color: 0
Size: 496585 Color: 1

Bin 3778: 707 of cap free
Amount of items: 2
Items: 
Size: 779732 Color: 0
Size: 219562 Color: 1

Bin 3779: 710 of cap free
Amount of items: 2
Items: 
Size: 544458 Color: 0
Size: 454833 Color: 1

Bin 3780: 713 of cap free
Amount of items: 2
Items: 
Size: 659130 Color: 0
Size: 340158 Color: 1

Bin 3781: 713 of cap free
Amount of items: 2
Items: 
Size: 675702 Color: 1
Size: 323586 Color: 0

Bin 3782: 714 of cap free
Amount of items: 2
Items: 
Size: 736088 Color: 0
Size: 263199 Color: 1

Bin 3783: 714 of cap free
Amount of items: 2
Items: 
Size: 505372 Color: 0
Size: 493915 Color: 1

Bin 3784: 714 of cap free
Amount of items: 2
Items: 
Size: 538004 Color: 0
Size: 461283 Color: 1

Bin 3785: 717 of cap free
Amount of items: 2
Items: 
Size: 690191 Color: 0
Size: 309093 Color: 1

Bin 3786: 718 of cap free
Amount of items: 2
Items: 
Size: 642042 Color: 0
Size: 357241 Color: 1

Bin 3787: 719 of cap free
Amount of items: 2
Items: 
Size: 614983 Color: 0
Size: 384299 Color: 1

Bin 3788: 721 of cap free
Amount of items: 2
Items: 
Size: 694871 Color: 1
Size: 304409 Color: 0

Bin 3789: 721 of cap free
Amount of items: 2
Items: 
Size: 710311 Color: 0
Size: 288969 Color: 1

Bin 3790: 722 of cap free
Amount of items: 2
Items: 
Size: 528178 Color: 1
Size: 471101 Color: 0

Bin 3791: 724 of cap free
Amount of items: 2
Items: 
Size: 539519 Color: 1
Size: 459758 Color: 0

Bin 3792: 725 of cap free
Amount of items: 2
Items: 
Size: 580631 Color: 1
Size: 418645 Color: 0

Bin 3793: 726 of cap free
Amount of items: 2
Items: 
Size: 643167 Color: 1
Size: 356108 Color: 0

Bin 3794: 728 of cap free
Amount of items: 2
Items: 
Size: 612053 Color: 1
Size: 387220 Color: 0

Bin 3795: 729 of cap free
Amount of items: 2
Items: 
Size: 558554 Color: 1
Size: 440718 Color: 0

Bin 3796: 729 of cap free
Amount of items: 2
Items: 
Size: 694503 Color: 0
Size: 304769 Color: 1

Bin 3797: 730 of cap free
Amount of items: 2
Items: 
Size: 716744 Color: 1
Size: 282527 Color: 0

Bin 3798: 730 of cap free
Amount of items: 2
Items: 
Size: 509377 Color: 0
Size: 489894 Color: 1

Bin 3799: 731 of cap free
Amount of items: 2
Items: 
Size: 652405 Color: 1
Size: 346865 Color: 0

Bin 3800: 732 of cap free
Amount of items: 3
Items: 
Size: 667327 Color: 0
Size: 197467 Color: 1
Size: 134475 Color: 0

Bin 3801: 734 of cap free
Amount of items: 2
Items: 
Size: 568393 Color: 0
Size: 430874 Color: 1

Bin 3802: 736 of cap free
Amount of items: 2
Items: 
Size: 700307 Color: 1
Size: 298958 Color: 0

Bin 3803: 738 of cap free
Amount of items: 2
Items: 
Size: 614980 Color: 0
Size: 384283 Color: 1

Bin 3804: 742 of cap free
Amount of items: 2
Items: 
Size: 720189 Color: 1
Size: 279070 Color: 0

Bin 3805: 743 of cap free
Amount of items: 2
Items: 
Size: 623922 Color: 1
Size: 375336 Color: 0

Bin 3806: 747 of cap free
Amount of items: 2
Items: 
Size: 560464 Color: 0
Size: 438790 Color: 1

Bin 3807: 749 of cap free
Amount of items: 2
Items: 
Size: 709649 Color: 1
Size: 289603 Color: 0

Bin 3808: 749 of cap free
Amount of items: 2
Items: 
Size: 517099 Color: 1
Size: 482153 Color: 0

Bin 3809: 750 of cap free
Amount of items: 2
Items: 
Size: 648470 Color: 0
Size: 350781 Color: 1

Bin 3810: 751 of cap free
Amount of items: 2
Items: 
Size: 792586 Color: 0
Size: 206664 Color: 1

Bin 3811: 752 of cap free
Amount of items: 3
Items: 
Size: 411277 Color: 0
Size: 409915 Color: 0
Size: 178057 Color: 1

Bin 3812: 752 of cap free
Amount of items: 2
Items: 
Size: 570841 Color: 0
Size: 428408 Color: 1

Bin 3813: 753 of cap free
Amount of items: 2
Items: 
Size: 643159 Color: 1
Size: 356089 Color: 0

Bin 3814: 753 of cap free
Amount of items: 2
Items: 
Size: 683868 Color: 0
Size: 315380 Color: 1

Bin 3815: 754 of cap free
Amount of items: 2
Items: 
Size: 534412 Color: 1
Size: 464835 Color: 0

Bin 3816: 756 of cap free
Amount of items: 2
Items: 
Size: 793395 Color: 1
Size: 205850 Color: 0

Bin 3817: 757 of cap free
Amount of items: 2
Items: 
Size: 694861 Color: 1
Size: 304383 Color: 0

Bin 3818: 757 of cap free
Amount of items: 2
Items: 
Size: 775825 Color: 0
Size: 223419 Color: 1

Bin 3819: 759 of cap free
Amount of items: 2
Items: 
Size: 794719 Color: 1
Size: 204523 Color: 0

Bin 3820: 761 of cap free
Amount of items: 2
Items: 
Size: 662849 Color: 1
Size: 336391 Color: 0

Bin 3821: 763 of cap free
Amount of items: 2
Items: 
Size: 619655 Color: 1
Size: 379583 Color: 0

Bin 3822: 768 of cap free
Amount of items: 2
Items: 
Size: 637371 Color: 1
Size: 361862 Color: 0

Bin 3823: 769 of cap free
Amount of items: 2
Items: 
Size: 513621 Color: 1
Size: 485611 Color: 0

Bin 3824: 771 of cap free
Amount of items: 2
Items: 
Size: 622808 Color: 1
Size: 376422 Color: 0

Bin 3825: 775 of cap free
Amount of items: 2
Items: 
Size: 510289 Color: 0
Size: 488937 Color: 1

Bin 3826: 776 of cap free
Amount of items: 2
Items: 
Size: 532031 Color: 1
Size: 467194 Color: 0

Bin 3827: 776 of cap free
Amount of items: 2
Items: 
Size: 680185 Color: 0
Size: 319040 Color: 1

Bin 3828: 779 of cap free
Amount of items: 2
Items: 
Size: 765054 Color: 1
Size: 234168 Color: 0

Bin 3829: 782 of cap free
Amount of items: 2
Items: 
Size: 650561 Color: 1
Size: 348658 Color: 0

Bin 3830: 786 of cap free
Amount of items: 2
Items: 
Size: 630582 Color: 0
Size: 368633 Color: 1

Bin 3831: 787 of cap free
Amount of items: 2
Items: 
Size: 559395 Color: 1
Size: 439819 Color: 0

Bin 3832: 788 of cap free
Amount of items: 2
Items: 
Size: 766860 Color: 0
Size: 232353 Color: 1

Bin 3833: 790 of cap free
Amount of items: 2
Items: 
Size: 503758 Color: 0
Size: 495453 Color: 1

Bin 3834: 791 of cap free
Amount of items: 2
Items: 
Size: 717620 Color: 1
Size: 281590 Color: 0

Bin 3835: 792 of cap free
Amount of items: 2
Items: 
Size: 727488 Color: 0
Size: 271721 Color: 1

Bin 3836: 794 of cap free
Amount of items: 2
Items: 
Size: 584751 Color: 0
Size: 414456 Color: 1

Bin 3837: 796 of cap free
Amount of items: 2
Items: 
Size: 738598 Color: 1
Size: 260607 Color: 0

Bin 3838: 800 of cap free
Amount of items: 2
Items: 
Size: 501770 Color: 1
Size: 497431 Color: 0

Bin 3839: 800 of cap free
Amount of items: 2
Items: 
Size: 758746 Color: 0
Size: 240455 Color: 1

Bin 3840: 801 of cap free
Amount of items: 2
Items: 
Size: 551306 Color: 1
Size: 447894 Color: 0

Bin 3841: 802 of cap free
Amount of items: 2
Items: 
Size: 602679 Color: 1
Size: 396520 Color: 0

Bin 3842: 806 of cap free
Amount of items: 2
Items: 
Size: 507330 Color: 1
Size: 491865 Color: 0

Bin 3843: 809 of cap free
Amount of items: 2
Items: 
Size: 601675 Color: 0
Size: 397517 Color: 1

Bin 3844: 810 of cap free
Amount of items: 2
Items: 
Size: 546058 Color: 1
Size: 453133 Color: 0

Bin 3845: 812 of cap free
Amount of items: 2
Items: 
Size: 737233 Color: 0
Size: 261956 Color: 1

Bin 3846: 813 of cap free
Amount of items: 2
Items: 
Size: 661238 Color: 0
Size: 337950 Color: 1

Bin 3847: 817 of cap free
Amount of items: 2
Items: 
Size: 622229 Color: 0
Size: 376955 Color: 1

Bin 3848: 817 of cap free
Amount of items: 2
Items: 
Size: 529673 Color: 0
Size: 469511 Color: 1

Bin 3849: 820 of cap free
Amount of items: 2
Items: 
Size: 651903 Color: 0
Size: 347278 Color: 1

Bin 3850: 821 of cap free
Amount of items: 2
Items: 
Size: 532638 Color: 0
Size: 466542 Color: 1

Bin 3851: 823 of cap free
Amount of items: 2
Items: 
Size: 591816 Color: 0
Size: 407362 Color: 1

Bin 3852: 823 of cap free
Amount of items: 2
Items: 
Size: 535193 Color: 1
Size: 463985 Color: 0

Bin 3853: 824 of cap free
Amount of items: 2
Items: 
Size: 692499 Color: 0
Size: 306678 Color: 1

Bin 3854: 824 of cap free
Amount of items: 2
Items: 
Size: 694414 Color: 0
Size: 304763 Color: 1

Bin 3855: 831 of cap free
Amount of items: 2
Items: 
Size: 507742 Color: 0
Size: 491428 Color: 1

Bin 3856: 831 of cap free
Amount of items: 2
Items: 
Size: 789217 Color: 1
Size: 209953 Color: 0

Bin 3857: 831 of cap free
Amount of items: 2
Items: 
Size: 501790 Color: 0
Size: 497380 Color: 1

Bin 3858: 831 of cap free
Amount of items: 2
Items: 
Size: 684140 Color: 1
Size: 315030 Color: 0

Bin 3859: 832 of cap free
Amount of items: 2
Items: 
Size: 784068 Color: 1
Size: 215101 Color: 0

Bin 3860: 832 of cap free
Amount of items: 2
Items: 
Size: 570246 Color: 1
Size: 428923 Color: 0

Bin 3861: 833 of cap free
Amount of items: 2
Items: 
Size: 515111 Color: 1
Size: 484057 Color: 0

Bin 3862: 835 of cap free
Amount of items: 2
Items: 
Size: 594774 Color: 0
Size: 404392 Color: 1

Bin 3863: 836 of cap free
Amount of items: 2
Items: 
Size: 628695 Color: 1
Size: 370470 Color: 0

Bin 3864: 837 of cap free
Amount of items: 2
Items: 
Size: 610500 Color: 1
Size: 388664 Color: 0

Bin 3865: 841 of cap free
Amount of items: 2
Items: 
Size: 519124 Color: 0
Size: 480036 Color: 1

Bin 3866: 844 of cap free
Amount of items: 2
Items: 
Size: 761554 Color: 0
Size: 237603 Color: 1

Bin 3867: 844 of cap free
Amount of items: 2
Items: 
Size: 798000 Color: 0
Size: 201157 Color: 1

Bin 3868: 844 of cap free
Amount of items: 2
Items: 
Size: 630816 Color: 1
Size: 368341 Color: 0

Bin 3869: 844 of cap free
Amount of items: 2
Items: 
Size: 510359 Color: 1
Size: 488798 Color: 0

Bin 3870: 845 of cap free
Amount of items: 2
Items: 
Size: 643109 Color: 1
Size: 356047 Color: 0

Bin 3871: 847 of cap free
Amount of items: 2
Items: 
Size: 779690 Color: 0
Size: 219464 Color: 1

Bin 3872: 849 of cap free
Amount of items: 2
Items: 
Size: 623879 Color: 1
Size: 375273 Color: 0

Bin 3873: 850 of cap free
Amount of items: 2
Items: 
Size: 622219 Color: 0
Size: 376932 Color: 1

Bin 3874: 851 of cap free
Amount of items: 2
Items: 
Size: 667980 Color: 0
Size: 331170 Color: 1

Bin 3875: 855 of cap free
Amount of items: 2
Items: 
Size: 714843 Color: 0
Size: 284303 Color: 1

Bin 3876: 856 of cap free
Amount of items: 2
Items: 
Size: 609439 Color: 0
Size: 389706 Color: 1

Bin 3877: 858 of cap free
Amount of items: 2
Items: 
Size: 566617 Color: 1
Size: 432526 Color: 0

Bin 3878: 859 of cap free
Amount of items: 2
Items: 
Size: 648391 Color: 0
Size: 350751 Color: 1

Bin 3879: 859 of cap free
Amount of items: 2
Items: 
Size: 626816 Color: 0
Size: 372326 Color: 1

Bin 3880: 863 of cap free
Amount of items: 2
Items: 
Size: 606274 Color: 0
Size: 392864 Color: 1

Bin 3881: 863 of cap free
Amount of items: 2
Items: 
Size: 542665 Color: 0
Size: 456473 Color: 1

Bin 3882: 863 of cap free
Amount of items: 2
Items: 
Size: 551296 Color: 1
Size: 447842 Color: 0

Bin 3883: 873 of cap free
Amount of items: 2
Items: 
Size: 641916 Color: 0
Size: 357212 Color: 1

Bin 3884: 875 of cap free
Amount of items: 2
Items: 
Size: 701385 Color: 0
Size: 297741 Color: 1

Bin 3885: 877 of cap free
Amount of items: 2
Items: 
Size: 593874 Color: 1
Size: 405250 Color: 0

Bin 3886: 877 of cap free
Amount of items: 2
Items: 
Size: 692481 Color: 0
Size: 306643 Color: 1

Bin 3887: 879 of cap free
Amount of items: 2
Items: 
Size: 784648 Color: 0
Size: 214474 Color: 1

Bin 3888: 879 of cap free
Amount of items: 2
Items: 
Size: 528078 Color: 1
Size: 471044 Color: 0

Bin 3889: 883 of cap free
Amount of items: 2
Items: 
Size: 665163 Color: 0
Size: 333955 Color: 1

Bin 3890: 884 of cap free
Amount of items: 2
Items: 
Size: 771793 Color: 0
Size: 227324 Color: 1

Bin 3891: 884 of cap free
Amount of items: 2
Items: 
Size: 518134 Color: 1
Size: 480983 Color: 0

Bin 3892: 885 of cap free
Amount of items: 2
Items: 
Size: 716634 Color: 0
Size: 282482 Color: 1

Bin 3893: 887 of cap free
Amount of items: 2
Items: 
Size: 633399 Color: 0
Size: 365715 Color: 1

Bin 3894: 889 of cap free
Amount of items: 2
Items: 
Size: 592631 Color: 1
Size: 406481 Color: 0

Bin 3895: 892 of cap free
Amount of items: 2
Items: 
Size: 645682 Color: 1
Size: 353427 Color: 0

Bin 3896: 893 of cap free
Amount of items: 2
Items: 
Size: 580500 Color: 1
Size: 418608 Color: 0

Bin 3897: 894 of cap free
Amount of items: 2
Items: 
Size: 755359 Color: 1
Size: 243748 Color: 0

Bin 3898: 897 of cap free
Amount of items: 2
Items: 
Size: 605041 Color: 0
Size: 394063 Color: 1

Bin 3899: 905 of cap free
Amount of items: 2
Items: 
Size: 589757 Color: 0
Size: 409339 Color: 1

Bin 3900: 906 of cap free
Amount of items: 2
Items: 
Size: 584678 Color: 0
Size: 414417 Color: 1

Bin 3901: 909 of cap free
Amount of items: 2
Items: 
Size: 660188 Color: 0
Size: 338904 Color: 1

Bin 3902: 909 of cap free
Amount of items: 2
Items: 
Size: 523092 Color: 1
Size: 476000 Color: 0

Bin 3903: 910 of cap free
Amount of items: 2
Items: 
Size: 637059 Color: 0
Size: 362032 Color: 1

Bin 3904: 911 of cap free
Amount of items: 2
Items: 
Size: 630498 Color: 0
Size: 368592 Color: 1

Bin 3905: 922 of cap free
Amount of items: 2
Items: 
Size: 557473 Color: 1
Size: 441606 Color: 0

Bin 3906: 923 of cap free
Amount of items: 2
Items: 
Size: 753104 Color: 0
Size: 245974 Color: 1

Bin 3907: 927 of cap free
Amount of items: 2
Items: 
Size: 739544 Color: 1
Size: 259530 Color: 0

Bin 3908: 930 of cap free
Amount of items: 2
Items: 
Size: 544261 Color: 0
Size: 454810 Color: 1

Bin 3909: 932 of cap free
Amount of items: 2
Items: 
Size: 744406 Color: 0
Size: 254663 Color: 1

Bin 3910: 934 of cap free
Amount of items: 2
Items: 
Size: 608357 Color: 0
Size: 390710 Color: 1

Bin 3911: 934 of cap free
Amount of items: 2
Items: 
Size: 612032 Color: 0
Size: 387035 Color: 1

Bin 3912: 938 of cap free
Amount of items: 2
Items: 
Size: 554458 Color: 0
Size: 444605 Color: 1

Bin 3913: 940 of cap free
Amount of items: 2
Items: 
Size: 556008 Color: 0
Size: 443053 Color: 1

Bin 3914: 942 of cap free
Amount of items: 2
Items: 
Size: 551265 Color: 1
Size: 447794 Color: 0

Bin 3915: 944 of cap free
Amount of items: 2
Items: 
Size: 572886 Color: 1
Size: 426171 Color: 0

Bin 3916: 945 of cap free
Amount of items: 2
Items: 
Size: 570208 Color: 1
Size: 428848 Color: 0

Bin 3917: 945 of cap free
Amount of items: 2
Items: 
Size: 779686 Color: 0
Size: 219370 Color: 1

Bin 3918: 946 of cap free
Amount of items: 2
Items: 
Size: 775807 Color: 0
Size: 223248 Color: 1

Bin 3919: 946 of cap free
Amount of items: 2
Items: 
Size: 672586 Color: 0
Size: 326469 Color: 1

Bin 3920: 946 of cap free
Amount of items: 2
Items: 
Size: 548636 Color: 1
Size: 450419 Color: 0

Bin 3921: 949 of cap free
Amount of items: 2
Items: 
Size: 789194 Color: 1
Size: 209858 Color: 0

Bin 3922: 952 of cap free
Amount of items: 2
Items: 
Size: 677850 Color: 1
Size: 321199 Color: 0

Bin 3923: 955 of cap free
Amount of items: 2
Items: 
Size: 541940 Color: 1
Size: 457106 Color: 0

Bin 3924: 955 of cap free
Amount of items: 2
Items: 
Size: 670238 Color: 1
Size: 328808 Color: 0

Bin 3925: 955 of cap free
Amount of items: 2
Items: 
Size: 688433 Color: 1
Size: 310613 Color: 0

Bin 3926: 959 of cap free
Amount of items: 2
Items: 
Size: 768249 Color: 0
Size: 230793 Color: 1

Bin 3927: 959 of cap free
Amount of items: 2
Items: 
Size: 513230 Color: 0
Size: 485812 Color: 1

Bin 3928: 959 of cap free
Amount of items: 2
Items: 
Size: 619548 Color: 1
Size: 379494 Color: 0

Bin 3929: 961 of cap free
Amount of items: 2
Items: 
Size: 678286 Color: 0
Size: 320754 Color: 1

Bin 3930: 963 of cap free
Amount of items: 2
Items: 
Size: 692737 Color: 1
Size: 306301 Color: 0

Bin 3931: 963 of cap free
Amount of items: 2
Items: 
Size: 716573 Color: 0
Size: 282465 Color: 1

Bin 3932: 966 of cap free
Amount of items: 2
Items: 
Size: 670766 Color: 0
Size: 328269 Color: 1

Bin 3933: 970 of cap free
Amount of items: 2
Items: 
Size: 785663 Color: 0
Size: 213368 Color: 1

Bin 3934: 971 of cap free
Amount of items: 2
Items: 
Size: 542657 Color: 0
Size: 456373 Color: 1

Bin 3935: 973 of cap free
Amount of items: 2
Items: 
Size: 706853 Color: 0
Size: 292175 Color: 1

Bin 3936: 974 of cap free
Amount of items: 2
Items: 
Size: 520665 Color: 1
Size: 478362 Color: 0

Bin 3937: 976 of cap free
Amount of items: 2
Items: 
Size: 640477 Color: 0
Size: 358548 Color: 1

Bin 3938: 977 of cap free
Amount of items: 2
Items: 
Size: 501623 Color: 1
Size: 497401 Color: 0

Bin 3939: 978 of cap free
Amount of items: 2
Items: 
Size: 741192 Color: 1
Size: 257831 Color: 0

Bin 3940: 978 of cap free
Amount of items: 2
Items: 
Size: 626569 Color: 1
Size: 372454 Color: 0

Bin 3941: 983 of cap free
Amount of items: 2
Items: 
Size: 687671 Color: 0
Size: 311347 Color: 1

Bin 3942: 990 of cap free
Amount of items: 2
Items: 
Size: 744363 Color: 0
Size: 254648 Color: 1

Bin 3943: 991 of cap free
Amount of items: 2
Items: 
Size: 740238 Color: 0
Size: 258772 Color: 1

Bin 3944: 991 of cap free
Amount of items: 2
Items: 
Size: 779643 Color: 0
Size: 219367 Color: 1

Bin 3945: 992 of cap free
Amount of items: 2
Items: 
Size: 694341 Color: 0
Size: 304668 Color: 1

Bin 3946: 998 of cap free
Amount of items: 2
Items: 
Size: 733350 Color: 0
Size: 265653 Color: 1

Bin 3947: 1003 of cap free
Amount of items: 2
Items: 
Size: 687656 Color: 0
Size: 311342 Color: 1

Bin 3948: 1006 of cap free
Amount of items: 2
Items: 
Size: 708747 Color: 0
Size: 290248 Color: 1

Bin 3949: 1008 of cap free
Amount of items: 2
Items: 
Size: 710268 Color: 0
Size: 288725 Color: 1

Bin 3950: 1010 of cap free
Amount of items: 2
Items: 
Size: 756914 Color: 1
Size: 242077 Color: 0

Bin 3951: 1010 of cap free
Amount of items: 2
Items: 
Size: 622610 Color: 1
Size: 376381 Color: 0

Bin 3952: 1014 of cap free
Amount of items: 2
Items: 
Size: 521548 Color: 0
Size: 477439 Color: 1

Bin 3953: 1016 of cap free
Amount of items: 2
Items: 
Size: 584834 Color: 1
Size: 414151 Color: 0

Bin 3954: 1019 of cap free
Amount of items: 2
Items: 
Size: 629109 Color: 0
Size: 369873 Color: 1

Bin 3955: 1020 of cap free
Amount of items: 2
Items: 
Size: 566460 Color: 1
Size: 432521 Color: 0

Bin 3956: 1027 of cap free
Amount of items: 2
Items: 
Size: 670199 Color: 1
Size: 328775 Color: 0

Bin 3957: 1030 of cap free
Amount of items: 2
Items: 
Size: 648357 Color: 0
Size: 350614 Color: 1

Bin 3958: 1032 of cap free
Amount of items: 2
Items: 
Size: 507542 Color: 0
Size: 491427 Color: 1

Bin 3959: 1033 of cap free
Amount of items: 2
Items: 
Size: 543906 Color: 1
Size: 455062 Color: 0

Bin 3960: 1038 of cap free
Amount of items: 2
Items: 
Size: 744330 Color: 0
Size: 254633 Color: 1

Bin 3961: 1039 of cap free
Amount of items: 2
Items: 
Size: 725921 Color: 1
Size: 273041 Color: 0

Bin 3962: 1042 of cap free
Amount of items: 2
Items: 
Size: 614642 Color: 1
Size: 384317 Color: 0

Bin 3963: 1055 of cap free
Amount of items: 2
Items: 
Size: 692363 Color: 0
Size: 306583 Color: 1

Bin 3964: 1057 of cap free
Amount of items: 2
Items: 
Size: 523500 Color: 0
Size: 475444 Color: 1

Bin 3965: 1058 of cap free
Amount of items: 2
Items: 
Size: 763378 Color: 0
Size: 235565 Color: 1

Bin 3966: 1059 of cap free
Amount of items: 2
Items: 
Size: 589627 Color: 0
Size: 409315 Color: 1

Bin 3967: 1059 of cap free
Amount of items: 2
Items: 
Size: 766597 Color: 0
Size: 232345 Color: 1

Bin 3968: 1068 of cap free
Amount of items: 2
Items: 
Size: 620413 Color: 0
Size: 378520 Color: 1

Bin 3969: 1072 of cap free
Amount of items: 2
Items: 
Size: 741555 Color: 0
Size: 257374 Color: 1

Bin 3970: 1075 of cap free
Amount of items: 2
Items: 
Size: 685459 Color: 1
Size: 313467 Color: 0

Bin 3971: 1077 of cap free
Amount of items: 2
Items: 
Size: 734869 Color: 1
Size: 264055 Color: 0

Bin 3972: 1077 of cap free
Amount of items: 2
Items: 
Size: 681072 Color: 0
Size: 317852 Color: 1

Bin 3973: 1078 of cap free
Amount of items: 2
Items: 
Size: 790172 Color: 1
Size: 208751 Color: 0

Bin 3974: 1079 of cap free
Amount of items: 2
Items: 
Size: 667827 Color: 0
Size: 331095 Color: 1

Bin 3975: 1080 of cap free
Amount of items: 2
Items: 
Size: 694265 Color: 0
Size: 304656 Color: 1

Bin 3976: 1080 of cap free
Amount of items: 2
Items: 
Size: 601415 Color: 0
Size: 397506 Color: 1

Bin 3977: 1081 of cap free
Amount of items: 2
Items: 
Size: 589442 Color: 1
Size: 409478 Color: 0

Bin 3978: 1081 of cap free
Amount of items: 2
Items: 
Size: 750997 Color: 0
Size: 247923 Color: 1

Bin 3979: 1086 of cap free
Amount of items: 2
Items: 
Size: 678192 Color: 0
Size: 320723 Color: 1

Bin 3980: 1091 of cap free
Amount of items: 2
Items: 
Size: 797848 Color: 1
Size: 201062 Color: 0

Bin 3981: 1094 of cap free
Amount of items: 2
Items: 
Size: 576473 Color: 1
Size: 422434 Color: 0

Bin 3982: 1094 of cap free
Amount of items: 2
Items: 
Size: 521525 Color: 0
Size: 477382 Color: 1

Bin 3983: 1095 of cap free
Amount of items: 2
Items: 
Size: 621991 Color: 0
Size: 376915 Color: 1

Bin 3984: 1097 of cap free
Amount of items: 2
Items: 
Size: 710185 Color: 0
Size: 288719 Color: 1

Bin 3985: 1104 of cap free
Amount of items: 2
Items: 
Size: 701366 Color: 0
Size: 297531 Color: 1

Bin 3986: 1110 of cap free
Amount of items: 2
Items: 
Size: 618108 Color: 1
Size: 380783 Color: 0

Bin 3987: 1113 of cap free
Amount of items: 2
Items: 
Size: 510122 Color: 0
Size: 488766 Color: 1

Bin 3988: 1124 of cap free
Amount of items: 2
Items: 
Size: 716454 Color: 0
Size: 282423 Color: 1

Bin 3989: 1125 of cap free
Amount of items: 2
Items: 
Size: 507034 Color: 1
Size: 491842 Color: 0

Bin 3990: 1127 of cap free
Amount of items: 2
Items: 
Size: 570514 Color: 0
Size: 428360 Color: 1

Bin 3991: 1128 of cap free
Amount of items: 2
Items: 
Size: 667146 Color: 1
Size: 331727 Color: 0

Bin 3992: 1128 of cap free
Amount of items: 2
Items: 
Size: 514841 Color: 1
Size: 484032 Color: 0

Bin 3993: 1131 of cap free
Amount of items: 3
Items: 
Size: 436715 Color: 0
Size: 395827 Color: 0
Size: 166328 Color: 1

Bin 3994: 1133 of cap free
Amount of items: 2
Items: 
Size: 763091 Color: 1
Size: 235777 Color: 0

Bin 3995: 1137 of cap free
Amount of items: 2
Items: 
Size: 639716 Color: 1
Size: 359148 Color: 0

Bin 3996: 1139 of cap free
Amount of items: 2
Items: 
Size: 596547 Color: 0
Size: 402315 Color: 1

Bin 3997: 1146 of cap free
Amount of items: 2
Items: 
Size: 662447 Color: 0
Size: 336408 Color: 1

Bin 3998: 1149 of cap free
Amount of items: 2
Items: 
Size: 687544 Color: 0
Size: 311308 Color: 1

Bin 3999: 1150 of cap free
Amount of items: 2
Items: 
Size: 784389 Color: 0
Size: 214462 Color: 1

Bin 4000: 1154 of cap free
Amount of items: 2
Items: 
Size: 700292 Color: 1
Size: 298555 Color: 0

Bin 4001: 1157 of cap free
Amount of items: 2
Items: 
Size: 763252 Color: 0
Size: 235592 Color: 1

Bin 4002: 1167 of cap free
Amount of items: 2
Items: 
Size: 566326 Color: 1
Size: 432508 Color: 0

Bin 4003: 1170 of cap free
Amount of items: 2
Items: 
Size: 518065 Color: 1
Size: 480766 Color: 0

Bin 4004: 1171 of cap free
Amount of items: 2
Items: 
Size: 764753 Color: 1
Size: 234077 Color: 0

Bin 4005: 1174 of cap free
Amount of items: 2
Items: 
Size: 704065 Color: 0
Size: 294762 Color: 1

Bin 4006: 1175 of cap free
Amount of items: 2
Items: 
Size: 716308 Color: 1
Size: 282518 Color: 0

Bin 4007: 1181 of cap free
Amount of items: 2
Items: 
Size: 608213 Color: 0
Size: 390607 Color: 1

Bin 4008: 1183 of cap free
Amount of items: 2
Items: 
Size: 626523 Color: 0
Size: 372295 Color: 1

Bin 4009: 1183 of cap free
Amount of items: 2
Items: 
Size: 694439 Color: 1
Size: 304379 Color: 0

Bin 4010: 1188 of cap free
Amount of items: 2
Items: 
Size: 696719 Color: 0
Size: 302094 Color: 1

Bin 4011: 1190 of cap free
Amount of items: 2
Items: 
Size: 705989 Color: 1
Size: 292822 Color: 0

Bin 4012: 1190 of cap free
Amount of items: 2
Items: 
Size: 531814 Color: 1
Size: 466997 Color: 0

Bin 4013: 1194 of cap free
Amount of items: 2
Items: 
Size: 516721 Color: 0
Size: 482086 Color: 1

Bin 4014: 1198 of cap free
Amount of items: 2
Items: 
Size: 569140 Color: 0
Size: 429663 Color: 1

Bin 4015: 1200 of cap free
Amount of items: 2
Items: 
Size: 694248 Color: 0
Size: 304553 Color: 1

Bin 4016: 1201 of cap free
Amount of items: 2
Items: 
Size: 762590 Color: 0
Size: 236210 Color: 1

Bin 4017: 1208 of cap free
Amount of items: 3
Items: 
Size: 705336 Color: 1
Size: 148498 Color: 1
Size: 144959 Color: 0

Bin 4018: 1208 of cap free
Amount of items: 2
Items: 
Size: 510258 Color: 1
Size: 488535 Color: 0

Bin 4019: 1212 of cap free
Amount of items: 2
Items: 
Size: 639700 Color: 1
Size: 359089 Color: 0

Bin 4020: 1214 of cap free
Amount of items: 2
Items: 
Size: 770006 Color: 1
Size: 228781 Color: 0

Bin 4021: 1218 of cap free
Amount of items: 2
Items: 
Size: 539030 Color: 1
Size: 459753 Color: 0

Bin 4022: 1223 of cap free
Amount of items: 2
Items: 
Size: 788920 Color: 1
Size: 209858 Color: 0

Bin 4023: 1225 of cap free
Amount of items: 2
Items: 
Size: 705961 Color: 1
Size: 292815 Color: 0

Bin 4024: 1230 of cap free
Amount of items: 2
Items: 
Size: 630434 Color: 1
Size: 368337 Color: 0

Bin 4025: 1233 of cap free
Amount of items: 2
Items: 
Size: 589699 Color: 0
Size: 409069 Color: 1

Bin 4026: 1235 of cap free
Amount of items: 2
Items: 
Size: 725803 Color: 0
Size: 272963 Color: 1

Bin 4027: 1238 of cap free
Amount of items: 2
Items: 
Size: 787364 Color: 1
Size: 211399 Color: 0

Bin 4028: 1239 of cap free
Amount of items: 2
Items: 
Size: 531809 Color: 1
Size: 466953 Color: 0

Bin 4029: 1242 of cap free
Amount of items: 2
Items: 
Size: 532544 Color: 0
Size: 466215 Color: 1

Bin 4030: 1244 of cap free
Amount of items: 2
Items: 
Size: 555331 Color: 1
Size: 443426 Color: 0

Bin 4031: 1245 of cap free
Amount of items: 2
Items: 
Size: 645360 Color: 1
Size: 353396 Color: 0

Bin 4032: 1252 of cap free
Amount of items: 2
Items: 
Size: 623507 Color: 0
Size: 375242 Color: 1

Bin 4033: 1254 of cap free
Amount of items: 2
Items: 
Size: 667710 Color: 0
Size: 331037 Color: 1

Bin 4034: 1259 of cap free
Amount of items: 2
Items: 
Size: 542405 Color: 0
Size: 456337 Color: 1

Bin 4035: 1259 of cap free
Amount of items: 2
Items: 
Size: 560390 Color: 0
Size: 438352 Color: 1

Bin 4036: 1260 of cap free
Amount of items: 2
Items: 
Size: 763012 Color: 1
Size: 235729 Color: 0

Bin 4037: 1263 of cap free
Amount of items: 2
Items: 
Size: 540039 Color: 0
Size: 458699 Color: 1

Bin 4038: 1265 of cap free
Amount of items: 2
Items: 
Size: 597348 Color: 0
Size: 401388 Color: 1

Bin 4039: 1266 of cap free
Amount of items: 2
Items: 
Size: 525792 Color: 0
Size: 472943 Color: 1

Bin 4040: 1270 of cap free
Amount of items: 2
Items: 
Size: 727414 Color: 0
Size: 271317 Color: 1

Bin 4041: 1282 of cap free
Amount of items: 2
Items: 
Size: 681292 Color: 1
Size: 317427 Color: 0

Bin 4042: 1283 of cap free
Amount of items: 2
Items: 
Size: 550025 Color: 0
Size: 448693 Color: 1

Bin 4043: 1290 of cap free
Amount of items: 2
Items: 
Size: 766460 Color: 0
Size: 232251 Color: 1

Bin 4044: 1291 of cap free
Amount of items: 2
Items: 
Size: 696683 Color: 0
Size: 302027 Color: 1

Bin 4045: 1293 of cap free
Amount of items: 2
Items: 
Size: 700195 Color: 1
Size: 298513 Color: 0

Bin 4046: 1293 of cap free
Amount of items: 2
Items: 
Size: 777726 Color: 1
Size: 220982 Color: 0

Bin 4047: 1311 of cap free
Amount of items: 2
Items: 
Size: 613131 Color: 1
Size: 385559 Color: 0

Bin 4048: 1311 of cap free
Amount of items: 2
Items: 
Size: 700185 Color: 1
Size: 298505 Color: 0

Bin 4049: 1321 of cap free
Amount of items: 2
Items: 
Size: 662570 Color: 1
Size: 336110 Color: 0

Bin 4050: 1324 of cap free
Amount of items: 2
Items: 
Size: 764731 Color: 1
Size: 233946 Color: 0

Bin 4051: 1325 of cap free
Amount of items: 2
Items: 
Size: 542351 Color: 0
Size: 456325 Color: 1

Bin 4052: 1326 of cap free
Amount of items: 2
Items: 
Size: 716215 Color: 1
Size: 282460 Color: 0

Bin 4053: 1333 of cap free
Amount of items: 2
Items: 
Size: 714474 Color: 0
Size: 284194 Color: 1

Bin 4054: 1334 of cap free
Amount of items: 2
Items: 
Size: 703956 Color: 0
Size: 294711 Color: 1

Bin 4055: 1334 of cap free
Amount of items: 2
Items: 
Size: 752696 Color: 0
Size: 245971 Color: 1

Bin 4056: 1337 of cap free
Amount of items: 2
Items: 
Size: 666977 Color: 1
Size: 331687 Color: 0

Bin 4057: 1339 of cap free
Amount of items: 2
Items: 
Size: 611466 Color: 1
Size: 387196 Color: 0

Bin 4058: 1341 of cap free
Amount of items: 2
Items: 
Size: 787366 Color: 1
Size: 211294 Color: 0

Bin 4059: 1346 of cap free
Amount of items: 2
Items: 
Size: 531719 Color: 1
Size: 466936 Color: 0

Bin 4060: 1353 of cap free
Amount of items: 2
Items: 
Size: 517980 Color: 1
Size: 480668 Color: 0

Bin 4061: 1359 of cap free
Amount of items: 2
Items: 
Size: 605320 Color: 1
Size: 393322 Color: 0

Bin 4062: 1364 of cap free
Amount of items: 2
Items: 
Size: 670632 Color: 0
Size: 328005 Color: 1

Bin 4063: 1369 of cap free
Amount of items: 2
Items: 
Size: 611601 Color: 0
Size: 387031 Color: 1

Bin 4064: 1373 of cap free
Amount of items: 2
Items: 
Size: 696650 Color: 0
Size: 301978 Color: 1

Bin 4065: 1381 of cap free
Amount of items: 2
Items: 
Size: 617875 Color: 1
Size: 380745 Color: 0

Bin 4066: 1386 of cap free
Amount of items: 2
Items: 
Size: 572542 Color: 1
Size: 426073 Color: 0

Bin 4067: 1400 of cap free
Amount of items: 2
Items: 
Size: 772893 Color: 0
Size: 225708 Color: 1

Bin 4068: 1410 of cap free
Amount of items: 2
Items: 
Size: 570408 Color: 0
Size: 428183 Color: 1

Bin 4069: 1417 of cap free
Amount of items: 2
Items: 
Size: 741232 Color: 0
Size: 257352 Color: 1

Bin 4070: 1437 of cap free
Amount of items: 2
Items: 
Size: 749856 Color: 1
Size: 248708 Color: 0

Bin 4071: 1440 of cap free
Amount of items: 2
Items: 
Size: 545933 Color: 1
Size: 452628 Color: 0

Bin 4072: 1440 of cap free
Amount of items: 2
Items: 
Size: 630328 Color: 0
Size: 368233 Color: 1

Bin 4073: 1440 of cap free
Amount of items: 2
Items: 
Size: 670571 Color: 0
Size: 327990 Color: 1

Bin 4074: 1463 of cap free
Amount of items: 2
Items: 
Size: 572487 Color: 1
Size: 426051 Color: 0

Bin 4075: 1466 of cap free
Amount of items: 3
Items: 
Size: 430245 Color: 0
Size: 296496 Color: 1
Size: 271794 Color: 1

Bin 4076: 1467 of cap free
Amount of items: 2
Items: 
Size: 701026 Color: 0
Size: 297508 Color: 1

Bin 4077: 1469 of cap free
Amount of items: 2
Items: 
Size: 685334 Color: 1
Size: 313198 Color: 0

Bin 4078: 1477 of cap free
Amount of items: 2
Items: 
Size: 677712 Color: 1
Size: 320812 Color: 0

Bin 4079: 1482 of cap free
Amount of items: 2
Items: 
Size: 560327 Color: 0
Size: 438192 Color: 1

Bin 4080: 1483 of cap free
Amount of items: 2
Items: 
Size: 611425 Color: 1
Size: 387093 Color: 0

Bin 4081: 1486 of cap free
Amount of items: 2
Items: 
Size: 613095 Color: 1
Size: 385420 Color: 0

Bin 4082: 1492 of cap free
Amount of items: 2
Items: 
Size: 751854 Color: 1
Size: 246655 Color: 0

Bin 4083: 1496 of cap free
Amount of items: 2
Items: 
Size: 693960 Color: 0
Size: 304545 Color: 1

Bin 4084: 1498 of cap free
Amount of items: 2
Items: 
Size: 714399 Color: 0
Size: 284104 Color: 1

Bin 4085: 1498 of cap free
Amount of items: 2
Items: 
Size: 647943 Color: 0
Size: 350560 Color: 1

Bin 4086: 1500 of cap free
Amount of items: 2
Items: 
Size: 539487 Color: 1
Size: 459014 Color: 0

Bin 4087: 1506 of cap free
Amount of items: 2
Items: 
Size: 705841 Color: 1
Size: 292654 Color: 0

Bin 4088: 1506 of cap free
Amount of items: 2
Items: 
Size: 545896 Color: 1
Size: 452599 Color: 0

Bin 4089: 1513 of cap free
Amount of items: 2
Items: 
Size: 670055 Color: 1
Size: 328433 Color: 0

Bin 4090: 1515 of cap free
Amount of items: 2
Items: 
Size: 652254 Color: 1
Size: 346232 Color: 0

Bin 4091: 1519 of cap free
Amount of items: 2
Items: 
Size: 572471 Color: 1
Size: 426011 Color: 0

Bin 4092: 1520 of cap free
Amount of items: 2
Items: 
Size: 797624 Color: 1
Size: 200857 Color: 0

Bin 4093: 1530 of cap free
Amount of items: 2
Items: 
Size: 506865 Color: 1
Size: 491606 Color: 0

Bin 4094: 1536 of cap free
Amount of items: 2
Items: 
Size: 667541 Color: 0
Size: 330924 Color: 1

Bin 4095: 1541 of cap free
Amount of items: 2
Items: 
Size: 759597 Color: 1
Size: 238863 Color: 0

Bin 4096: 1547 of cap free
Amount of items: 2
Items: 
Size: 764640 Color: 1
Size: 233814 Color: 0

Bin 4097: 1548 of cap free
Amount of items: 2
Items: 
Size: 678042 Color: 0
Size: 320411 Color: 1

Bin 4098: 1556 of cap free
Amount of items: 2
Items: 
Size: 636721 Color: 0
Size: 361724 Color: 1

Bin 4099: 1557 of cap free
Amount of items: 2
Items: 
Size: 670521 Color: 0
Size: 327923 Color: 1

Bin 4100: 1563 of cap free
Amount of items: 2
Items: 
Size: 681016 Color: 1
Size: 317422 Color: 0

Bin 4101: 1569 of cap free
Amount of items: 2
Items: 
Size: 683474 Color: 1
Size: 314958 Color: 0

Bin 4102: 1579 of cap free
Amount of items: 2
Items: 
Size: 552388 Color: 1
Size: 446034 Color: 0

Bin 4103: 1582 of cap free
Amount of items: 3
Items: 
Size: 433456 Color: 1
Size: 288658 Color: 0
Size: 276305 Color: 0

Bin 4104: 1582 of cap free
Amount of items: 2
Items: 
Size: 542339 Color: 0
Size: 456080 Color: 1

Bin 4105: 1587 of cap free
Amount of items: 2
Items: 
Size: 705762 Color: 1
Size: 292652 Color: 0

Bin 4106: 1596 of cap free
Amount of items: 3
Items: 
Size: 756625 Color: 0
Size: 130678 Color: 0
Size: 111102 Color: 1

Bin 4107: 1600 of cap free
Amount of items: 2
Items: 
Size: 687928 Color: 1
Size: 310473 Color: 0

Bin 4108: 1602 of cap free
Amount of items: 2
Items: 
Size: 736999 Color: 0
Size: 261400 Color: 1

Bin 4109: 1603 of cap free
Amount of items: 2
Items: 
Size: 501432 Color: 1
Size: 496966 Color: 0

Bin 4110: 1604 of cap free
Amount of items: 2
Items: 
Size: 529888 Color: 1
Size: 468509 Color: 0

Bin 4111: 1607 of cap free
Amount of items: 2
Items: 
Size: 564055 Color: 0
Size: 434339 Color: 1

Bin 4112: 1610 of cap free
Amount of items: 2
Items: 
Size: 670514 Color: 0
Size: 327877 Color: 1

Bin 4113: 1612 of cap free
Amount of items: 2
Items: 
Size: 766310 Color: 0
Size: 232079 Color: 1

Bin 4114: 1616 of cap free
Amount of items: 2
Items: 
Size: 788419 Color: 0
Size: 209966 Color: 1

Bin 4115: 1624 of cap free
Amount of items: 2
Items: 
Size: 555199 Color: 1
Size: 443178 Color: 0

Bin 4116: 1624 of cap free
Amount of items: 2
Items: 
Size: 574012 Color: 0
Size: 424365 Color: 1

Bin 4117: 1630 of cap free
Amount of items: 2
Items: 
Size: 703465 Color: 1
Size: 294906 Color: 0

Bin 4118: 1636 of cap free
Amount of items: 2
Items: 
Size: 690027 Color: 0
Size: 308338 Color: 1

Bin 4119: 1637 of cap free
Amount of items: 2
Items: 
Size: 678115 Color: 0
Size: 320249 Color: 1

Bin 4120: 1642 of cap free
Amount of items: 2
Items: 
Size: 752470 Color: 0
Size: 245889 Color: 1

Bin 4121: 1643 of cap free
Amount of items: 2
Items: 
Size: 602635 Color: 1
Size: 395723 Color: 0

Bin 4122: 1647 of cap free
Amount of items: 2
Items: 
Size: 564036 Color: 0
Size: 434318 Color: 1

Bin 4123: 1653 of cap free
Amount of items: 2
Items: 
Size: 555016 Color: 1
Size: 443332 Color: 0

Bin 4124: 1660 of cap free
Amount of items: 2
Items: 
Size: 696822 Color: 1
Size: 301519 Color: 0

Bin 4125: 1660 of cap free
Amount of items: 2
Items: 
Size: 771349 Color: 1
Size: 226992 Color: 0

Bin 4126: 1664 of cap free
Amount of items: 2
Items: 
Size: 749696 Color: 1
Size: 248641 Color: 0

Bin 4127: 1666 of cap free
Amount of items: 2
Items: 
Size: 632177 Color: 1
Size: 366158 Color: 0

Bin 4128: 1677 of cap free
Amount of items: 2
Items: 
Size: 503628 Color: 0
Size: 494696 Color: 1

Bin 4129: 1678 of cap free
Amount of items: 2
Items: 
Size: 501387 Color: 1
Size: 496936 Color: 0

Bin 4130: 1679 of cap free
Amount of items: 2
Items: 
Size: 564024 Color: 0
Size: 434298 Color: 1

Bin 4131: 1687 of cap free
Amount of items: 2
Items: 
Size: 614040 Color: 0
Size: 384274 Color: 1

Bin 4132: 1697 of cap free
Amount of items: 2
Items: 
Size: 507443 Color: 0
Size: 490861 Color: 1

Bin 4133: 1700 of cap free
Amount of items: 2
Items: 
Size: 516324 Color: 0
Size: 481977 Color: 1

Bin 4134: 1707 of cap free
Amount of items: 2
Items: 
Size: 606009 Color: 0
Size: 392285 Color: 1

Bin 4135: 1714 of cap free
Amount of items: 2
Items: 
Size: 700992 Color: 0
Size: 297295 Color: 1

Bin 4136: 1714 of cap free
Amount of items: 2
Items: 
Size: 509780 Color: 1
Size: 488507 Color: 0

Bin 4137: 1733 of cap free
Amount of items: 2
Items: 
Size: 758567 Color: 0
Size: 239701 Color: 1

Bin 4138: 1741 of cap free
Amount of items: 2
Items: 
Size: 514736 Color: 1
Size: 483524 Color: 0

Bin 4139: 1744 of cap free
Amount of items: 2
Items: 
Size: 766195 Color: 0
Size: 232062 Color: 1

Bin 4140: 1749 of cap free
Amount of items: 2
Items: 
Size: 596940 Color: 0
Size: 401312 Color: 1

Bin 4141: 1749 of cap free
Amount of items: 2
Items: 
Size: 700987 Color: 0
Size: 297265 Color: 1

Bin 4142: 1754 of cap free
Amount of items: 2
Items: 
Size: 762663 Color: 1
Size: 235584 Color: 0

Bin 4143: 1760 of cap free
Amount of items: 2
Items: 
Size: 722114 Color: 0
Size: 276127 Color: 1

Bin 4144: 1762 of cap free
Amount of items: 2
Items: 
Size: 542190 Color: 0
Size: 456049 Color: 1

Bin 4145: 1766 of cap free
Amount of items: 2
Items: 
Size: 523487 Color: 0
Size: 474748 Color: 1

Bin 4146: 1775 of cap free
Amount of items: 2
Items: 
Size: 565997 Color: 1
Size: 432229 Color: 0

Bin 4147: 1780 of cap free
Amount of items: 2
Items: 
Size: 617667 Color: 1
Size: 380554 Color: 0

Bin 4148: 1780 of cap free
Amount of items: 2
Items: 
Size: 614010 Color: 0
Size: 384211 Color: 1

Bin 4149: 1788 of cap free
Amount of items: 2
Items: 
Size: 749614 Color: 1
Size: 248599 Color: 0

Bin 4150: 1790 of cap free
Amount of items: 2
Items: 
Size: 549562 Color: 0
Size: 448649 Color: 1

Bin 4151: 1802 of cap free
Amount of items: 2
Items: 
Size: 509754 Color: 1
Size: 488445 Color: 0

Bin 4152: 1811 of cap free
Amount of items: 2
Items: 
Size: 700151 Color: 1
Size: 298039 Color: 0

Bin 4153: 1816 of cap free
Amount of items: 2
Items: 
Size: 759454 Color: 1
Size: 238731 Color: 0

Bin 4154: 1837 of cap free
Amount of items: 2
Items: 
Size: 506792 Color: 1
Size: 491372 Color: 0

Bin 4155: 1841 of cap free
Amount of items: 2
Items: 
Size: 509748 Color: 1
Size: 488412 Color: 0

Bin 4156: 1849 of cap free
Amount of items: 2
Items: 
Size: 687895 Color: 1
Size: 310257 Color: 0

Bin 4157: 1855 of cap free
Amount of items: 2
Items: 
Size: 644860 Color: 1
Size: 353286 Color: 0

Bin 4158: 1878 of cap free
Amount of items: 2
Items: 
Size: 689923 Color: 0
Size: 308200 Color: 1

Bin 4159: 1882 of cap free
Amount of items: 2
Items: 
Size: 778840 Color: 0
Size: 219279 Color: 1

Bin 4160: 1888 of cap free
Amount of items: 2
Items: 
Size: 680978 Color: 1
Size: 317135 Color: 0

Bin 4161: 1888 of cap free
Amount of items: 2
Items: 
Size: 544254 Color: 0
Size: 453859 Color: 1

Bin 4162: 1891 of cap free
Amount of items: 2
Items: 
Size: 709557 Color: 1
Size: 288553 Color: 0

Bin 4163: 1892 of cap free
Amount of items: 2
Items: 
Size: 536847 Color: 0
Size: 461262 Color: 1

Bin 4164: 1897 of cap free
Amount of items: 2
Items: 
Size: 554957 Color: 1
Size: 443147 Color: 0

Bin 4165: 1901 of cap free
Amount of items: 2
Items: 
Size: 700075 Color: 1
Size: 298025 Color: 0

Bin 4166: 1903 of cap free
Amount of items: 2
Items: 
Size: 641648 Color: 0
Size: 356450 Color: 1

Bin 4167: 1907 of cap free
Amount of items: 2
Items: 
Size: 797206 Color: 1
Size: 200888 Color: 0

Bin 4168: 1909 of cap free
Amount of items: 2
Items: 
Size: 506742 Color: 1
Size: 491350 Color: 0

Bin 4169: 1911 of cap free
Amount of items: 2
Items: 
Size: 786786 Color: 1
Size: 211304 Color: 0

Bin 4170: 1911 of cap free
Amount of items: 2
Items: 
Size: 531669 Color: 1
Size: 466421 Color: 0

Bin 4171: 1913 of cap free
Amount of items: 2
Items: 
Size: 778813 Color: 0
Size: 219275 Color: 1

Bin 4172: 1926 of cap free
Amount of items: 2
Items: 
Size: 534244 Color: 1
Size: 463831 Color: 0

Bin 4173: 1928 of cap free
Amount of items: 2
Items: 
Size: 544230 Color: 0
Size: 453843 Color: 1

Bin 4174: 1930 of cap free
Amount of items: 2
Items: 
Size: 523403 Color: 0
Size: 474668 Color: 1

Bin 4175: 1930 of cap free
Amount of items: 2
Items: 
Size: 591681 Color: 1
Size: 406390 Color: 0

Bin 4176: 1942 of cap free
Amount of items: 2
Items: 
Size: 644854 Color: 1
Size: 353205 Color: 0

Bin 4177: 1951 of cap free
Amount of items: 2
Items: 
Size: 557335 Color: 1
Size: 440715 Color: 0

Bin 4178: 1952 of cap free
Amount of items: 2
Items: 
Size: 514565 Color: 1
Size: 483484 Color: 0

Bin 4179: 1960 of cap free
Amount of items: 2
Items: 
Size: 778796 Color: 0
Size: 219245 Color: 1

Bin 4180: 1965 of cap free
Amount of items: 2
Items: 
Size: 632923 Color: 0
Size: 365113 Color: 1

Bin 4181: 1965 of cap free
Amount of items: 2
Items: 
Size: 721930 Color: 0
Size: 276106 Color: 1

Bin 4182: 1968 of cap free
Amount of items: 2
Items: 
Size: 762568 Color: 0
Size: 235465 Color: 1

Bin 4183: 1985 of cap free
Amount of items: 2
Items: 
Size: 591813 Color: 1
Size: 406203 Color: 0

Bin 4184: 1996 of cap free
Amount of items: 2
Items: 
Size: 617322 Color: 1
Size: 380683 Color: 0

Bin 4185: 2010 of cap free
Amount of items: 2
Items: 
Size: 705750 Color: 1
Size: 292241 Color: 0

Bin 4186: 2017 of cap free
Amount of items: 2
Items: 
Size: 677728 Color: 0
Size: 320256 Color: 1

Bin 4187: 2019 of cap free
Amount of items: 2
Items: 
Size: 799420 Color: 1
Size: 198562 Color: 0

Bin 4188: 2038 of cap free
Amount of items: 2
Items: 
Size: 694151 Color: 1
Size: 303812 Color: 0

Bin 4189: 2039 of cap free
Amount of items: 2
Items: 
Size: 644365 Color: 0
Size: 353597 Color: 1

Bin 4190: 2053 of cap free
Amount of items: 2
Items: 
Size: 709591 Color: 1
Size: 288357 Color: 0

Bin 4191: 2055 of cap free
Amount of items: 2
Items: 
Size: 554846 Color: 1
Size: 443100 Color: 0

Bin 4192: 2057 of cap free
Amount of items: 2
Items: 
Size: 700976 Color: 0
Size: 296968 Color: 1

Bin 4193: 2057 of cap free
Amount of items: 2
Items: 
Size: 539809 Color: 0
Size: 458135 Color: 1

Bin 4194: 2059 of cap free
Amount of items: 2
Items: 
Size: 501184 Color: 1
Size: 496758 Color: 0

Bin 4195: 2060 of cap free
Amount of items: 5
Items: 
Size: 290641 Color: 1
Size: 197343 Color: 0
Size: 196318 Color: 1
Size: 171824 Color: 0
Size: 141815 Color: 1

Bin 4196: 2061 of cap free
Amount of items: 2
Items: 
Size: 680133 Color: 0
Size: 317807 Color: 1

Bin 4197: 2063 of cap free
Amount of items: 2
Items: 
Size: 545755 Color: 1
Size: 452183 Color: 0

Bin 4198: 2084 of cap free
Amount of items: 6
Items: 
Size: 173072 Color: 0
Size: 172803 Color: 0
Size: 167303 Color: 1
Size: 166628 Color: 1
Size: 166069 Color: 1
Size: 152042 Color: 0

Bin 4199: 2091 of cap free
Amount of items: 2
Items: 
Size: 749567 Color: 1
Size: 248343 Color: 0

Bin 4200: 2093 of cap free
Amount of items: 2
Items: 
Size: 709488 Color: 1
Size: 288420 Color: 0

Bin 4201: 2103 of cap free
Amount of items: 2
Items: 
Size: 680114 Color: 0
Size: 317784 Color: 1

Bin 4202: 2107 of cap free
Amount of items: 2
Items: 
Size: 738530 Color: 1
Size: 259364 Color: 0

Bin 4203: 2117 of cap free
Amount of items: 2
Items: 
Size: 721856 Color: 0
Size: 276028 Color: 1

Bin 4204: 2119 of cap free
Amount of items: 2
Items: 
Size: 738530 Color: 1
Size: 259352 Color: 0

Bin 4205: 2131 of cap free
Amount of items: 2
Items: 
Size: 514502 Color: 1
Size: 483368 Color: 0

Bin 4206: 2133 of cap free
Amount of items: 2
Items: 
Size: 722319 Color: 1
Size: 275549 Color: 0

Bin 4207: 2133 of cap free
Amount of items: 2
Items: 
Size: 657139 Color: 1
Size: 340729 Color: 0

Bin 4208: 2148 of cap free
Amount of items: 2
Items: 
Size: 686577 Color: 0
Size: 311276 Color: 1

Bin 4209: 2151 of cap free
Amount of items: 2
Items: 
Size: 608148 Color: 0
Size: 389702 Color: 1

Bin 4210: 2152 of cap free
Amount of items: 2
Items: 
Size: 534178 Color: 1
Size: 463671 Color: 0

Bin 4211: 2152 of cap free
Amount of items: 2
Items: 
Size: 583906 Color: 1
Size: 413943 Color: 0

Bin 4212: 2155 of cap free
Amount of items: 2
Items: 
Size: 647295 Color: 0
Size: 350551 Color: 1

Bin 4213: 2162 of cap free
Amount of items: 2
Items: 
Size: 515885 Color: 0
Size: 481954 Color: 1

Bin 4214: 2168 of cap free
Amount of items: 2
Items: 
Size: 590956 Color: 0
Size: 406877 Color: 1

Bin 4215: 2173 of cap free
Amount of items: 2
Items: 
Size: 538815 Color: 1
Size: 459013 Color: 0

Bin 4216: 2178 of cap free
Amount of items: 2
Items: 
Size: 758549 Color: 0
Size: 239274 Color: 1

Bin 4217: 2190 of cap free
Amount of items: 2
Items: 
Size: 506554 Color: 1
Size: 491257 Color: 0

Bin 4218: 2191 of cap free
Amount of items: 2
Items: 
Size: 536744 Color: 0
Size: 461066 Color: 1

Bin 4219: 2202 of cap free
Amount of items: 2
Items: 
Size: 538793 Color: 1
Size: 459006 Color: 0

Bin 4220: 2210 of cap free
Amount of items: 2
Items: 
Size: 657082 Color: 1
Size: 340709 Color: 0

Bin 4221: 2215 of cap free
Amount of items: 2
Items: 
Size: 685325 Color: 1
Size: 312461 Color: 0

Bin 4222: 2216 of cap free
Amount of items: 2
Items: 
Size: 699971 Color: 1
Size: 297814 Color: 0

Bin 4223: 2219 of cap free
Amount of items: 2
Items: 
Size: 786503 Color: 1
Size: 211279 Color: 0

Bin 4224: 2223 of cap free
Amount of items: 2
Items: 
Size: 632923 Color: 0
Size: 364855 Color: 1

Bin 4225: 2235 of cap free
Amount of items: 2
Items: 
Size: 639136 Color: 1
Size: 358630 Color: 0

Bin 4226: 2236 of cap free
Amount of items: 2
Items: 
Size: 519630 Color: 1
Size: 478135 Color: 0

Bin 4227: 2236 of cap free
Amount of items: 2
Items: 
Size: 515878 Color: 0
Size: 481887 Color: 1

Bin 4228: 2236 of cap free
Amount of items: 2
Items: 
Size: 531527 Color: 1
Size: 466238 Color: 0

Bin 4229: 2276 of cap free
Amount of items: 2
Items: 
Size: 662439 Color: 0
Size: 335286 Color: 1

Bin 4230: 2284 of cap free
Amount of items: 2
Items: 
Size: 743720 Color: 0
Size: 253997 Color: 1

Bin 4231: 2298 of cap free
Amount of items: 2
Items: 
Size: 762546 Color: 0
Size: 235157 Color: 1

Bin 4232: 2307 of cap free
Amount of items: 2
Items: 
Size: 797098 Color: 0
Size: 200596 Color: 1

Bin 4233: 2310 of cap free
Amount of items: 2
Items: 
Size: 506499 Color: 1
Size: 491192 Color: 0

Bin 4234: 2314 of cap free
Amount of items: 2
Items: 
Size: 534124 Color: 1
Size: 463563 Color: 0

Bin 4235: 2315 of cap free
Amount of items: 2
Items: 
Size: 596745 Color: 0
Size: 400941 Color: 1

Bin 4236: 2330 of cap free
Amount of items: 2
Items: 
Size: 541876 Color: 0
Size: 455795 Color: 1

Bin 4237: 2337 of cap free
Amount of items: 2
Items: 
Size: 531509 Color: 1
Size: 466155 Color: 0

Bin 4238: 2350 of cap free
Amount of items: 2
Items: 
Size: 514496 Color: 1
Size: 483155 Color: 0

Bin 4239: 2362 of cap free
Amount of items: 2
Items: 
Size: 699954 Color: 1
Size: 297685 Color: 0

Bin 4240: 2362 of cap free
Amount of items: 2
Items: 
Size: 666494 Color: 1
Size: 331145 Color: 0

Bin 4241: 2378 of cap free
Amount of items: 2
Items: 
Size: 797014 Color: 1
Size: 200609 Color: 0

Bin 4242: 2394 of cap free
Amount of items: 2
Items: 
Size: 554580 Color: 1
Size: 443027 Color: 0

Bin 4243: 2401 of cap free
Amount of items: 6
Items: 
Size: 180718 Color: 0
Size: 180151 Color: 0
Size: 174014 Color: 1
Size: 173812 Color: 1
Size: 166027 Color: 1
Size: 122878 Color: 0

Bin 4244: 2409 of cap free
Amount of items: 2
Items: 
Size: 583866 Color: 1
Size: 413726 Color: 0

Bin 4245: 2409 of cap free
Amount of items: 2
Items: 
Size: 651401 Color: 1
Size: 346191 Color: 0

Bin 4246: 2416 of cap free
Amount of items: 2
Items: 
Size: 631976 Color: 1
Size: 365609 Color: 0

Bin 4247: 2423 of cap free
Amount of items: 2
Items: 
Size: 520827 Color: 0
Size: 476751 Color: 1

Bin 4248: 2438 of cap free
Amount of items: 2
Items: 
Size: 787233 Color: 0
Size: 210330 Color: 1

Bin 4249: 2454 of cap free
Amount of items: 2
Items: 
Size: 745263 Color: 1
Size: 252284 Color: 0

Bin 4250: 2455 of cap free
Amount of items: 2
Items: 
Size: 724630 Color: 0
Size: 272916 Color: 1

Bin 4251: 2466 of cap free
Amount of items: 2
Items: 
Size: 700586 Color: 0
Size: 296949 Color: 1

Bin 4252: 2468 of cap free
Amount of items: 2
Items: 
Size: 679931 Color: 0
Size: 317602 Color: 1

Bin 4253: 2488 of cap free
Amount of items: 2
Items: 
Size: 797070 Color: 0
Size: 200443 Color: 1

Bin 4254: 2501 of cap free
Amount of items: 2
Items: 
Size: 786222 Color: 1
Size: 211278 Color: 0

Bin 4255: 2507 of cap free
Amount of items: 2
Items: 
Size: 651229 Color: 0
Size: 346265 Color: 1

Bin 4256: 2509 of cap free
Amount of items: 2
Items: 
Size: 700545 Color: 0
Size: 296947 Color: 1

Bin 4257: 2516 of cap free
Amount of items: 2
Items: 
Size: 796824 Color: 1
Size: 200661 Color: 0

Bin 4258: 2529 of cap free
Amount of items: 2
Items: 
Size: 509255 Color: 1
Size: 488217 Color: 0

Bin 4259: 2539 of cap free
Amount of items: 2
Items: 
Size: 736084 Color: 0
Size: 261378 Color: 1

Bin 4260: 2542 of cap free
Amount of items: 2
Items: 
Size: 797039 Color: 0
Size: 200420 Color: 1

Bin 4261: 2549 of cap free
Amount of items: 2
Items: 
Size: 689410 Color: 0
Size: 308042 Color: 1

Bin 4262: 2556 of cap free
Amount of items: 3
Items: 
Size: 529041 Color: 1
Size: 271284 Color: 0
Size: 197120 Color: 0

Bin 4263: 2558 of cap free
Amount of items: 3
Items: 
Size: 769404 Color: 0
Size: 126711 Color: 0
Size: 101328 Color: 1

Bin 4264: 2560 of cap free
Amount of items: 2
Items: 
Size: 679853 Color: 0
Size: 317588 Color: 1

Bin 4265: 2568 of cap free
Amount of items: 2
Items: 
Size: 605157 Color: 1
Size: 392276 Color: 0

Bin 4266: 2578 of cap free
Amount of items: 2
Items: 
Size: 538776 Color: 1
Size: 458647 Color: 0

Bin 4267: 2590 of cap free
Amount of items: 2
Items: 
Size: 766091 Color: 0
Size: 231320 Color: 1

Bin 4268: 2590 of cap free
Amount of items: 2
Items: 
Size: 632762 Color: 0
Size: 364649 Color: 1

Bin 4269: 2591 of cap free
Amount of items: 2
Items: 
Size: 740214 Color: 0
Size: 257196 Color: 1

Bin 4270: 2592 of cap free
Amount of items: 2
Items: 
Size: 674241 Color: 0
Size: 323168 Color: 1

Bin 4271: 2602 of cap free
Amount of items: 2
Items: 
Size: 631902 Color: 1
Size: 365497 Color: 0

Bin 4272: 2645 of cap free
Amount of items: 2
Items: 
Size: 595494 Color: 1
Size: 401862 Color: 0

Bin 4273: 2657 of cap free
Amount of items: 2
Items: 
Size: 616862 Color: 1
Size: 380482 Color: 0

Bin 4274: 2663 of cap free
Amount of items: 2
Items: 
Size: 613023 Color: 1
Size: 384315 Color: 0

Bin 4275: 2683 of cap free
Amount of items: 2
Items: 
Size: 709254 Color: 1
Size: 288064 Color: 0

Bin 4276: 2685 of cap free
Amount of items: 2
Items: 
Size: 583681 Color: 1
Size: 413635 Color: 0

Bin 4277: 2687 of cap free
Amount of items: 2
Items: 
Size: 651182 Color: 0
Size: 346132 Color: 1

Bin 4278: 2700 of cap free
Amount of items: 2
Items: 
Size: 705992 Color: 0
Size: 291309 Color: 1

Bin 4279: 2714 of cap free
Amount of items: 2
Items: 
Size: 759006 Color: 1
Size: 238281 Color: 0

Bin 4280: 2725 of cap free
Amount of items: 2
Items: 
Size: 700414 Color: 0
Size: 296862 Color: 1

Bin 4281: 2754 of cap free
Amount of items: 2
Items: 
Size: 583620 Color: 1
Size: 413627 Color: 0

Bin 4282: 2767 of cap free
Amount of items: 2
Items: 
Size: 662311 Color: 0
Size: 334923 Color: 1

Bin 4283: 2771 of cap free
Amount of items: 2
Items: 
Size: 534094 Color: 1
Size: 463136 Color: 0

Bin 4284: 2822 of cap free
Amount of items: 2
Items: 
Size: 548275 Color: 1
Size: 448904 Color: 0

Bin 4285: 2831 of cap free
Amount of items: 2
Items: 
Size: 797262 Color: 0
Size: 199908 Color: 1

Bin 4286: 2840 of cap free
Amount of items: 2
Items: 
Size: 503422 Color: 0
Size: 493739 Color: 1

Bin 4287: 2852 of cap free
Amount of items: 2
Items: 
Size: 700306 Color: 0
Size: 296843 Color: 1

Bin 4288: 2869 of cap free
Amount of items: 2
Items: 
Size: 709318 Color: 1
Size: 287814 Color: 0

Bin 4289: 2869 of cap free
Amount of items: 2
Items: 
Size: 515296 Color: 0
Size: 481836 Color: 1

Bin 4290: 2898 of cap free
Amount of items: 2
Items: 
Size: 669291 Color: 0
Size: 327812 Color: 1

Bin 4291: 2934 of cap free
Amount of items: 2
Items: 
Size: 700234 Color: 0
Size: 296833 Color: 1

Bin 4292: 2980 of cap free
Amount of items: 2
Items: 
Size: 783217 Color: 1
Size: 213804 Color: 0

Bin 4293: 2984 of cap free
Amount of items: 3
Items: 
Size: 395289 Color: 0
Size: 383870 Color: 0
Size: 217858 Color: 1

Bin 4294: 2994 of cap free
Amount of items: 2
Items: 
Size: 644802 Color: 1
Size: 352205 Color: 0

Bin 4295: 2998 of cap free
Amount of items: 2
Items: 
Size: 562724 Color: 0
Size: 434279 Color: 1

Bin 4296: 3003 of cap free
Amount of items: 2
Items: 
Size: 662113 Color: 0
Size: 334885 Color: 1

Bin 4297: 3015 of cap free
Amount of items: 2
Items: 
Size: 520284 Color: 0
Size: 476702 Color: 1

Bin 4298: 3019 of cap free
Amount of items: 2
Items: 
Size: 758516 Color: 0
Size: 238466 Color: 1

Bin 4299: 3022 of cap free
Amount of items: 2
Items: 
Size: 689179 Color: 0
Size: 307800 Color: 1

Bin 4300: 3023 of cap free
Amount of items: 2
Items: 
Size: 776370 Color: 1
Size: 220608 Color: 0

Bin 4301: 3066 of cap free
Amount of items: 2
Items: 
Size: 792605 Color: 1
Size: 204330 Color: 0

Bin 4302: 3075 of cap free
Amount of items: 2
Items: 
Size: 758487 Color: 1
Size: 238439 Color: 0

Bin 4303: 3091 of cap free
Amount of items: 2
Items: 
Size: 699948 Color: 1
Size: 296962 Color: 0

Bin 4304: 3103 of cap free
Amount of items: 2
Items: 
Size: 604979 Color: 0
Size: 391919 Color: 1

Bin 4305: 3123 of cap free
Amount of items: 2
Items: 
Size: 573114 Color: 0
Size: 423764 Color: 1

Bin 4306: 3135 of cap free
Amount of items: 2
Items: 
Size: 534053 Color: 1
Size: 462813 Color: 0

Bin 4307: 3142 of cap free
Amount of items: 2
Items: 
Size: 596607 Color: 0
Size: 400252 Color: 1

Bin 4308: 3149 of cap free
Amount of items: 2
Items: 
Size: 644650 Color: 1
Size: 352202 Color: 0

Bin 4309: 3172 of cap free
Amount of items: 2
Items: 
Size: 604973 Color: 0
Size: 391856 Color: 1

Bin 4310: 3179 of cap free
Amount of items: 2
Items: 
Size: 788359 Color: 0
Size: 208463 Color: 1

Bin 4311: 3181 of cap free
Amount of items: 2
Items: 
Size: 613405 Color: 0
Size: 383415 Color: 1

Bin 4312: 3183 of cap free
Amount of items: 2
Items: 
Size: 792457 Color: 0
Size: 204361 Color: 1

Bin 4313: 3218 of cap free
Amount of items: 2
Items: 
Size: 716124 Color: 1
Size: 280659 Color: 0

Bin 4314: 3243 of cap free
Amount of items: 2
Items: 
Size: 565859 Color: 1
Size: 430899 Color: 0

Bin 4315: 3243 of cap free
Amount of items: 2
Items: 
Size: 640434 Color: 0
Size: 356324 Color: 1

Bin 4316: 3265 of cap free
Amount of items: 2
Items: 
Size: 735425 Color: 0
Size: 261311 Color: 1

Bin 4317: 3314 of cap free
Amount of items: 2
Items: 
Size: 758460 Color: 1
Size: 238227 Color: 0

Bin 4318: 3319 of cap free
Amount of items: 2
Items: 
Size: 666239 Color: 1
Size: 330443 Color: 0

Bin 4319: 3363 of cap free
Amount of items: 2
Items: 
Size: 782856 Color: 1
Size: 213782 Color: 0

Bin 4320: 3364 of cap free
Amount of items: 2
Items: 
Size: 714362 Color: 0
Size: 282275 Color: 1

Bin 4321: 3381 of cap free
Amount of items: 2
Items: 
Size: 716052 Color: 1
Size: 280568 Color: 0

Bin 4322: 3395 of cap free
Amount of items: 2
Items: 
Size: 778772 Color: 0
Size: 217834 Color: 1

Bin 4323: 3397 of cap free
Amount of items: 2
Items: 
Size: 505449 Color: 1
Size: 491155 Color: 0

Bin 4324: 3403 of cap free
Amount of items: 2
Items: 
Size: 690716 Color: 1
Size: 305882 Color: 0

Bin 4325: 3443 of cap free
Amount of items: 2
Items: 
Size: 650527 Color: 1
Size: 346031 Color: 0

Bin 4326: 3488 of cap free
Amount of items: 2
Items: 
Size: 778675 Color: 0
Size: 217838 Color: 1

Bin 4327: 3492 of cap free
Amount of items: 3
Items: 
Size: 382667 Color: 1
Size: 351906 Color: 0
Size: 261936 Color: 0

Bin 4328: 3555 of cap free
Amount of items: 2
Items: 
Size: 797175 Color: 0
Size: 199271 Color: 1

Bin 4329: 3560 of cap free
Amount of items: 2
Items: 
Size: 700240 Color: 0
Size: 296201 Color: 1

Bin 4330: 3588 of cap free
Amount of items: 2
Items: 
Size: 573000 Color: 0
Size: 423413 Color: 1

Bin 4331: 3591 of cap free
Amount of items: 2
Items: 
Size: 502696 Color: 0
Size: 493714 Color: 1

Bin 4332: 3600 of cap free
Amount of items: 3
Items: 
Size: 345809 Color: 0
Size: 333657 Color: 1
Size: 316935 Color: 1

Bin 4333: 3611 of cap free
Amount of items: 2
Items: 
Size: 562671 Color: 0
Size: 433719 Color: 1

Bin 4334: 3652 of cap free
Amount of items: 2
Items: 
Size: 502676 Color: 0
Size: 493673 Color: 1

Bin 4335: 3654 of cap free
Amount of items: 2
Items: 
Size: 514588 Color: 0
Size: 481759 Color: 1

Bin 4336: 3689 of cap free
Amount of items: 2
Items: 
Size: 727016 Color: 1
Size: 269296 Color: 0

Bin 4337: 3723 of cap free
Amount of items: 2
Items: 
Size: 775779 Color: 1
Size: 220499 Color: 0

Bin 4338: 3732 of cap free
Amount of items: 2
Items: 
Size: 583391 Color: 1
Size: 412878 Color: 0

Bin 4339: 3747 of cap free
Amount of items: 2
Items: 
Size: 631840 Color: 1
Size: 364414 Color: 0

Bin 4340: 3764 of cap free
Amount of items: 2
Items: 
Size: 640064 Color: 0
Size: 356173 Color: 1

Bin 4341: 3775 of cap free
Amount of items: 2
Items: 
Size: 583380 Color: 1
Size: 412846 Color: 0

Bin 4342: 3817 of cap free
Amount of items: 2
Items: 
Size: 505208 Color: 1
Size: 490976 Color: 0

Bin 4343: 3827 of cap free
Amount of items: 2
Items: 
Size: 612973 Color: 0
Size: 383201 Color: 1

Bin 4344: 3839 of cap free
Amount of items: 2
Items: 
Size: 562453 Color: 0
Size: 433709 Color: 1

Bin 4345: 3905 of cap free
Amount of items: 2
Items: 
Size: 626371 Color: 0
Size: 369725 Color: 1

Bin 4346: 3911 of cap free
Amount of items: 2
Items: 
Size: 596080 Color: 0
Size: 400010 Color: 1

Bin 4347: 3933 of cap free
Amount of items: 2
Items: 
Size: 796806 Color: 1
Size: 199262 Color: 0

Bin 4348: 3938 of cap free
Amount of items: 2
Items: 
Size: 699870 Color: 0
Size: 296193 Color: 1

Bin 4349: 4023 of cap free
Amount of items: 2
Items: 
Size: 770005 Color: 1
Size: 225973 Color: 0

Bin 4350: 4076 of cap free
Amount of items: 2
Items: 
Size: 505178 Color: 1
Size: 490747 Color: 0

Bin 4351: 4080 of cap free
Amount of items: 2
Items: 
Size: 572824 Color: 0
Size: 423097 Color: 1

Bin 4352: 4082 of cap free
Amount of items: 2
Items: 
Size: 562420 Color: 0
Size: 433499 Color: 1

Bin 4353: 4114 of cap free
Amount of items: 2
Items: 
Size: 583290 Color: 1
Size: 412597 Color: 0

Bin 4354: 4137 of cap free
Amount of items: 2
Items: 
Size: 740938 Color: 1
Size: 254926 Color: 0

Bin 4355: 4248 of cap free
Amount of items: 2
Items: 
Size: 583251 Color: 1
Size: 412502 Color: 0

Bin 4356: 4277 of cap free
Amount of items: 2
Items: 
Size: 538763 Color: 1
Size: 456961 Color: 0

Bin 4357: 4288 of cap free
Amount of items: 2
Items: 
Size: 631807 Color: 1
Size: 363906 Color: 0

Bin 4358: 4345 of cap free
Amount of items: 2
Items: 
Size: 748988 Color: 1
Size: 246668 Color: 0

Bin 4359: 4356 of cap free
Amount of items: 2
Items: 
Size: 562311 Color: 0
Size: 433334 Color: 1

Bin 4360: 4401 of cap free
Amount of items: 2
Items: 
Size: 649650 Color: 1
Size: 345950 Color: 0

Bin 4361: 4413 of cap free
Amount of items: 2
Items: 
Size: 572711 Color: 0
Size: 422877 Color: 1

Bin 4362: 4421 of cap free
Amount of items: 2
Items: 
Size: 611355 Color: 1
Size: 384225 Color: 0

Bin 4363: 4559 of cap free
Amount of items: 2
Items: 
Size: 665047 Color: 1
Size: 330395 Color: 0

Bin 4364: 4633 of cap free
Amount of items: 2
Items: 
Size: 501741 Color: 0
Size: 493627 Color: 1

Bin 4365: 4704 of cap free
Amount of items: 2
Items: 
Size: 786752 Color: 1
Size: 208545 Color: 0

Bin 4366: 4719 of cap free
Amount of items: 2
Items: 
Size: 664923 Color: 1
Size: 330359 Color: 0

Bin 4367: 4756 of cap free
Amount of items: 2
Items: 
Size: 582837 Color: 1
Size: 412408 Color: 0

Bin 4368: 4777 of cap free
Amount of items: 2
Items: 
Size: 582882 Color: 1
Size: 412342 Color: 0

Bin 4369: 4806 of cap free
Amount of items: 2
Items: 
Size: 562266 Color: 0
Size: 432929 Color: 1

Bin 4370: 4829 of cap free
Amount of items: 2
Items: 
Size: 501664 Color: 0
Size: 493508 Color: 1

Bin 4371: 4851 of cap free
Amount of items: 2
Items: 
Size: 778519 Color: 0
Size: 216631 Color: 1

Bin 4372: 4886 of cap free
Amount of items: 2
Items: 
Size: 667421 Color: 0
Size: 327694 Color: 1

Bin 4373: 4953 of cap free
Amount of items: 2
Items: 
Size: 595779 Color: 0
Size: 399269 Color: 1

Bin 4374: 4990 of cap free
Amount of items: 2
Items: 
Size: 501568 Color: 0
Size: 493443 Color: 1

Bin 4375: 4992 of cap free
Amount of items: 2
Items: 
Size: 667205 Color: 0
Size: 327804 Color: 1

Bin 4376: 5011 of cap free
Amount of items: 2
Items: 
Size: 595833 Color: 0
Size: 399157 Color: 1

Bin 4377: 5032 of cap free
Amount of items: 2
Items: 
Size: 582778 Color: 1
Size: 412191 Color: 0

Bin 4378: 5060 of cap free
Amount of items: 2
Items: 
Size: 572648 Color: 0
Size: 422293 Color: 1

Bin 4379: 5076 of cap free
Amount of items: 2
Items: 
Size: 501522 Color: 0
Size: 493403 Color: 1

Bin 4380: 5101 of cap free
Amount of items: 2
Items: 
Size: 582735 Color: 1
Size: 412165 Color: 0

Bin 4381: 5191 of cap free
Amount of items: 2
Items: 
Size: 595625 Color: 0
Size: 399185 Color: 1

Bin 4382: 5209 of cap free
Amount of items: 2
Items: 
Size: 572559 Color: 0
Size: 422233 Color: 1

Bin 4383: 5249 of cap free
Amount of items: 2
Items: 
Size: 721691 Color: 1
Size: 273061 Color: 0

Bin 4384: 5273 of cap free
Amount of items: 3
Items: 
Size: 589286 Color: 0
Size: 216142 Color: 1
Size: 189300 Color: 1

Bin 4385: 5313 of cap free
Amount of items: 2
Items: 
Size: 582656 Color: 1
Size: 412032 Color: 0

Bin 4386: 5328 of cap free
Amount of items: 2
Items: 
Size: 582642 Color: 1
Size: 412031 Color: 0

Bin 4387: 5509 of cap free
Amount of items: 2
Items: 
Size: 667034 Color: 0
Size: 327458 Color: 1

Bin 4388: 5555 of cap free
Amount of items: 2
Items: 
Size: 748378 Color: 1
Size: 246068 Color: 0

Bin 4389: 5560 of cap free
Amount of items: 2
Items: 
Size: 610238 Color: 1
Size: 384203 Color: 0

Bin 4390: 5565 of cap free
Amount of items: 2
Items: 
Size: 655389 Color: 1
Size: 339047 Color: 0

Bin 4391: 5572 of cap free
Amount of items: 2
Items: 
Size: 513062 Color: 0
Size: 481367 Color: 1

Bin 4392: 5615 of cap free
Amount of items: 2
Items: 
Size: 642947 Color: 1
Size: 351439 Color: 0

Bin 4393: 5629 of cap free
Amount of items: 2
Items: 
Size: 501430 Color: 0
Size: 492942 Color: 1

Bin 4394: 5648 of cap free
Amount of items: 2
Items: 
Size: 795719 Color: 0
Size: 198634 Color: 1

Bin 4395: 5816 of cap free
Amount of items: 2
Items: 
Size: 796858 Color: 1
Size: 197327 Color: 0

Bin 4396: 5885 of cap free
Amount of items: 3
Items: 
Size: 399304 Color: 1
Size: 344422 Color: 0
Size: 250390 Color: 1

Bin 4397: 5962 of cap free
Amount of items: 2
Items: 
Size: 501375 Color: 0
Size: 492664 Color: 1

Bin 4398: 6204 of cap free
Amount of items: 2
Items: 
Size: 554278 Color: 1
Size: 439519 Color: 0

Bin 4399: 6219 of cap free
Amount of items: 2
Items: 
Size: 565679 Color: 1
Size: 428103 Color: 0

Bin 4400: 6227 of cap free
Amount of items: 2
Items: 
Size: 747840 Color: 1
Size: 245934 Color: 0

Bin 4401: 6244 of cap free
Amount of items: 2
Items: 
Size: 790082 Color: 1
Size: 203675 Color: 0

Bin 4402: 6317 of cap free
Amount of items: 2
Items: 
Size: 581669 Color: 1
Size: 412015 Color: 0

Bin 4403: 6330 of cap free
Amount of items: 2
Items: 
Size: 796478 Color: 1
Size: 197193 Color: 0

Bin 4404: 6465 of cap free
Amount of items: 2
Items: 
Size: 512633 Color: 0
Size: 480903 Color: 1

Bin 4405: 6525 of cap free
Amount of items: 2
Items: 
Size: 685760 Color: 0
Size: 307716 Color: 1

Bin 4406: 6575 of cap free
Amount of items: 2
Items: 
Size: 581442 Color: 1
Size: 411984 Color: 0

Bin 4407: 6589 of cap free
Amount of items: 2
Items: 
Size: 738545 Color: 1
Size: 254867 Color: 0

Bin 4408: 6713 of cap free
Amount of items: 2
Items: 
Size: 639711 Color: 0
Size: 353577 Color: 1

Bin 4409: 6726 of cap free
Amount of items: 2
Items: 
Size: 539436 Color: 0
Size: 453839 Color: 1

Bin 4410: 6743 of cap free
Amount of items: 3
Items: 
Size: 380752 Color: 0
Size: 324276 Color: 1
Size: 288230 Color: 0

Bin 4411: 6792 of cap free
Amount of items: 6
Items: 
Size: 193806 Color: 0
Size: 170961 Color: 0
Size: 165234 Color: 1
Size: 163697 Color: 0
Size: 151092 Color: 1
Size: 148419 Color: 1

Bin 4412: 6861 of cap free
Amount of items: 2
Items: 
Size: 505017 Color: 1
Size: 488123 Color: 0

Bin 4413: 6922 of cap free
Amount of items: 2
Items: 
Size: 650962 Color: 0
Size: 342117 Color: 1

Bin 4414: 7001 of cap free
Amount of items: 2
Items: 
Size: 594033 Color: 0
Size: 398967 Color: 1

Bin 4415: 7112 of cap free
Amount of items: 2
Items: 
Size: 504948 Color: 1
Size: 487941 Color: 0

Bin 4416: 7119 of cap free
Amount of items: 2
Items: 
Size: 593939 Color: 0
Size: 398943 Color: 1

Bin 4417: 7175 of cap free
Amount of items: 2
Items: 
Size: 593858 Color: 0
Size: 398968 Color: 1

Bin 4418: 7178 of cap free
Amount of items: 2
Items: 
Size: 559898 Color: 0
Size: 432925 Color: 1

Bin 4419: 7383 of cap free
Amount of items: 2
Items: 
Size: 512584 Color: 0
Size: 480034 Color: 1

Bin 4420: 7680 of cap free
Amount of items: 2
Items: 
Size: 559664 Color: 0
Size: 432657 Color: 1

Bin 4421: 7759 of cap free
Amount of items: 2
Items: 
Size: 539432 Color: 0
Size: 452810 Color: 1

Bin 4422: 7816 of cap free
Amount of items: 2
Items: 
Size: 504283 Color: 1
Size: 487902 Color: 0

Bin 4423: 8249 of cap free
Amount of items: 2
Items: 
Size: 538964 Color: 0
Size: 452788 Color: 1

Bin 4424: 8491 of cap free
Amount of items: 2
Items: 
Size: 795782 Color: 0
Size: 195728 Color: 1

Bin 4425: 8492 of cap free
Amount of items: 3
Items: 
Size: 613007 Color: 0
Size: 222990 Color: 1
Size: 155512 Color: 0

Bin 4426: 8597 of cap free
Amount of items: 2
Items: 
Size: 504224 Color: 1
Size: 487180 Color: 0

Bin 4427: 8608 of cap free
Amount of items: 2
Items: 
Size: 514467 Color: 1
Size: 476926 Color: 0

Bin 4428: 8946 of cap free
Amount of items: 2
Items: 
Size: 795861 Color: 0
Size: 195194 Color: 1

Bin 4429: 9125 of cap free
Amount of items: 2
Items: 
Size: 503714 Color: 1
Size: 487162 Color: 0

Bin 4430: 9248 of cap free
Amount of items: 2
Items: 
Size: 666627 Color: 0
Size: 324126 Color: 1

Bin 4431: 9310 of cap free
Amount of items: 2
Items: 
Size: 503598 Color: 1
Size: 487093 Color: 0

Bin 4432: 9636 of cap free
Amount of items: 2
Items: 
Size: 593625 Color: 0
Size: 396740 Color: 1

Bin 4433: 10535 of cap free
Amount of items: 2
Items: 
Size: 610058 Color: 1
Size: 379408 Color: 0

Bin 4434: 11074 of cap free
Amount of items: 2
Items: 
Size: 503358 Color: 1
Size: 485569 Color: 0

Bin 4435: 11404 of cap free
Amount of items: 2
Items: 
Size: 609971 Color: 1
Size: 378626 Color: 0

Bin 4436: 11821 of cap free
Amount of items: 2
Items: 
Size: 791928 Color: 1
Size: 196252 Color: 0

Bin 4437: 12102 of cap free
Amount of items: 2
Items: 
Size: 535561 Color: 0
Size: 452338 Color: 1

Bin 4438: 13469 of cap free
Amount of items: 2
Items: 
Size: 669652 Color: 1
Size: 316880 Color: 0

Bin 4439: 14358 of cap free
Amount of items: 2
Items: 
Size: 527011 Color: 1
Size: 458632 Color: 0

Bin 4440: 14358 of cap free
Amount of items: 2
Items: 
Size: 791467 Color: 1
Size: 194176 Color: 0

Bin 4441: 14930 of cap free
Amount of items: 2
Items: 
Size: 576192 Color: 1
Size: 408879 Color: 0

Bin 4442: 15420 of cap free
Amount of items: 3
Items: 
Size: 758690 Color: 0
Size: 113926 Color: 0
Size: 111965 Color: 1

Bin 4443: 16108 of cap free
Amount of items: 2
Items: 
Size: 666993 Color: 0
Size: 316900 Color: 1

Bin 4444: 17274 of cap free
Amount of items: 2
Items: 
Size: 791912 Color: 0
Size: 190815 Color: 1

Bin 4445: 22605 of cap free
Amount of items: 2
Items: 
Size: 500747 Color: 0
Size: 476649 Color: 1

Bin 4446: 25151 of cap free
Amount of items: 2
Items: 
Size: 782690 Color: 1
Size: 192160 Color: 0

Bin 4447: 25728 of cap free
Amount of items: 2
Items: 
Size: 787341 Color: 0
Size: 186932 Color: 1

Bin 4448: 26736 of cap free
Amount of items: 2
Items: 
Size: 782003 Color: 1
Size: 191262 Color: 0

Bin 4449: 29216 of cap free
Amount of items: 2
Items: 
Size: 593646 Color: 0
Size: 377139 Color: 1

Bin 4450: 30058 of cap free
Amount of items: 2
Items: 
Size: 781568 Color: 1
Size: 188375 Color: 0

Bin 4451: 31403 of cap free
Amount of items: 2
Items: 
Size: 780742 Color: 1
Size: 187856 Color: 0

Bin 4452: 31428 of cap free
Amount of items: 2
Items: 
Size: 780790 Color: 1
Size: 187783 Color: 0

Bin 4453: 35464 of cap free
Amount of items: 2
Items: 
Size: 785589 Color: 0
Size: 178948 Color: 1

Bin 4454: 39729 of cap free
Amount of items: 2
Items: 
Size: 784373 Color: 0
Size: 175899 Color: 1

Bin 4455: 45453 of cap free
Amount of items: 2
Items: 
Size: 775002 Color: 1
Size: 179546 Color: 0

Total size: 4452614214
Total free space: 2390241

