Capicity Bin: 8344
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 7468 Color: 401
Size: 732 Color: 138
Size: 144 Color: 15

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 4136 Color: 272
Size: 1128 Color: 172
Size: 1032 Color: 161
Size: 664 Color: 128
Size: 512 Color: 111
Size: 400 Color: 92
Size: 240 Color: 51
Size: 128 Color: 9
Size: 104 Color: 7

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 6896 Color: 361
Size: 1272 Color: 184
Size: 120 Color: 8
Size: 56 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6926 Color: 363
Size: 1182 Color: 177
Size: 236 Color: 47

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 346
Size: 1406 Color: 195
Size: 280 Color: 66

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6201 Color: 329
Size: 1787 Color: 213
Size: 356 Color: 79

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 381
Size: 978 Color: 156
Size: 192 Color: 33

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7285 Color: 388
Size: 883 Color: 150
Size: 176 Color: 26

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6996 Color: 367
Size: 1284 Color: 186
Size: 64 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4261 Color: 282
Size: 3403 Color: 261
Size: 680 Color: 129

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 290
Size: 2964 Color: 251
Size: 584 Color: 120

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7388 Color: 396
Size: 804 Color: 144
Size: 152 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 353
Size: 1301 Color: 187
Size: 260 Color: 57

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5625 Color: 308
Size: 2267 Color: 234
Size: 452 Color: 101

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6699 Color: 349
Size: 1371 Color: 192
Size: 274 Color: 63

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7434 Color: 399
Size: 762 Color: 140
Size: 148 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4177 Color: 275
Size: 3473 Color: 267
Size: 694 Color: 136

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4198 Color: 280
Size: 3458 Color: 263
Size: 688 Color: 131

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6441 Color: 338
Size: 1587 Color: 202
Size: 316 Color: 71

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5508 Color: 303
Size: 2364 Color: 239
Size: 472 Color: 107

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7235 Color: 383
Size: 925 Color: 154
Size: 184 Color: 31

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5881 Color: 312
Size: 2053 Color: 229
Size: 410 Color: 96

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 364
Size: 1162 Color: 176
Size: 232 Color: 46

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 341
Size: 1545 Color: 199
Size: 238 Color: 49

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6664 Color: 348
Size: 1208 Color: 179
Size: 472 Color: 106

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5545 Color: 304
Size: 2333 Color: 238
Size: 466 Color: 105

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4785 Color: 288
Size: 2967 Color: 252
Size: 592 Color: 123

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6873 Color: 359
Size: 1227 Color: 181
Size: 244 Color: 52

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5994 Color: 319
Size: 1962 Color: 222
Size: 388 Color: 88

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 360
Size: 1212 Color: 180
Size: 240 Color: 50

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5921 Color: 314
Size: 2021 Color: 227
Size: 402 Color: 94

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 3664 Color: 269
Size: 3096 Color: 259
Size: 1584 Color: 201

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6921 Color: 362
Size: 1187 Color: 178
Size: 236 Color: 48

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 283
Size: 3340 Color: 260
Size: 664 Color: 127

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 330
Size: 1724 Color: 212
Size: 336 Color: 78

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5124 Color: 296
Size: 2684 Color: 246
Size: 536 Color: 116

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7170 Color: 379
Size: 982 Color: 157
Size: 192 Color: 32

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 323
Size: 1882 Color: 219
Size: 372 Color: 85

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 387
Size: 986 Color: 158
Size: 84 Color: 5

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5068 Color: 295
Size: 3004 Color: 256
Size: 272 Color: 61

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4668 Color: 284
Size: 3068 Color: 258
Size: 608 Color: 126

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7025 Color: 369
Size: 1101 Color: 170
Size: 218 Color: 42

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7332 Color: 391
Size: 844 Color: 147
Size: 168 Color: 23

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6643 Color: 344
Size: 1419 Color: 196
Size: 282 Color: 67

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5569 Color: 305
Size: 2313 Color: 237
Size: 462 Color: 104

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5905 Color: 313
Size: 2033 Color: 228
Size: 406 Color: 95

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6585 Color: 342
Size: 1467 Color: 198
Size: 292 Color: 68

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 7350 Color: 393
Size: 830 Color: 146
Size: 164 Color: 22

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 380
Size: 1076 Color: 166
Size: 96 Color: 6

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 7236 Color: 384
Size: 924 Color: 153
Size: 184 Color: 30

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 7085 Color: 374
Size: 1051 Color: 163
Size: 208 Color: 38

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 310
Size: 2242 Color: 232
Size: 448 Color: 99

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 315
Size: 2012 Color: 226
Size: 400 Color: 93

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6425 Color: 336
Size: 1601 Color: 204
Size: 318 Color: 72

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 366
Size: 1133 Color: 173
Size: 226 Color: 44

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 335
Size: 1612 Color: 205
Size: 320 Color: 74

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4201 Color: 281
Size: 3453 Color: 262
Size: 690 Color: 132

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 375
Size: 1036 Color: 162
Size: 200 Color: 34

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 7314 Color: 390
Size: 862 Color: 148
Size: 168 Color: 24

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5205 Color: 298
Size: 2617 Color: 244
Size: 522 Color: 114

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6636 Color: 343
Size: 1668 Color: 211
Size: 40 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5250 Color: 301
Size: 2582 Color: 241
Size: 512 Color: 110

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 378
Size: 1058 Color: 164
Size: 136 Color: 11

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 337
Size: 1650 Color: 209
Size: 268 Color: 60

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6830 Color: 356
Size: 1262 Color: 183
Size: 252 Color: 54

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5945 Color: 316
Size: 2001 Color: 225
Size: 398 Color: 91

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6124 Color: 324
Size: 1852 Color: 218
Size: 368 Color: 84

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6707 Color: 350
Size: 1365 Color: 191
Size: 272 Color: 62

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 357
Size: 1306 Color: 188
Size: 184 Color: 29

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 320
Size: 1964 Color: 223
Size: 384 Color: 87

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 7041 Color: 370
Size: 1087 Color: 169
Size: 216 Color: 41

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6154 Color: 325
Size: 1826 Color: 217
Size: 364 Color: 83

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6857 Color: 358
Size: 1241 Color: 182
Size: 246 Color: 53

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5226 Color: 300
Size: 2602 Color: 242
Size: 516 Color: 112

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 302
Size: 2500 Color: 240
Size: 496 Color: 108

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 7074 Color: 373
Size: 1062 Color: 165
Size: 208 Color: 37

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7494 Color: 402
Size: 714 Color: 137
Size: 136 Color: 12

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 347
Size: 1402 Color: 194
Size: 280 Color: 65

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6401 Color: 333
Size: 1621 Color: 207
Size: 322 Color: 75

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6404 Color: 334
Size: 1620 Color: 206
Size: 320 Color: 73

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 352
Size: 1316 Color: 189
Size: 256 Color: 56

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6361 Color: 331
Size: 1653 Color: 210
Size: 330 Color: 77

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 311
Size: 2156 Color: 231
Size: 424 Color: 97

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 4774 Color: 287
Size: 2978 Color: 253
Size: 592 Color: 122

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7143 Color: 377
Size: 1001 Color: 159
Size: 200 Color: 35

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5181 Color: 297
Size: 2637 Color: 245
Size: 526 Color: 115

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 5609 Color: 307
Size: 2281 Color: 235
Size: 454 Color: 102

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 7423 Color: 397
Size: 769 Color: 142
Size: 152 Color: 19

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 278
Size: 3468 Color: 265
Size: 688 Color: 130

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 326
Size: 1821 Color: 216
Size: 362 Color: 82

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 371
Size: 1084 Color: 168
Size: 216 Color: 40

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6826 Color: 355
Size: 1390 Color: 193
Size: 128 Color: 10

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 7428 Color: 398
Size: 764 Color: 141
Size: 152 Color: 18

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 4190 Color: 279
Size: 3462 Color: 264
Size: 692 Color: 135

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4182 Color: 277
Size: 3470 Color: 266
Size: 692 Color: 133

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 354
Size: 1273 Color: 185
Size: 254 Color: 55

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6644 Color: 345
Size: 1420 Color: 197
Size: 280 Color: 64

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 4180 Color: 276
Size: 4020 Color: 271
Size: 144 Color: 13

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 7466 Color: 400
Size: 734 Color: 139
Size: 144 Color: 14

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 4173 Color: 273
Size: 3673 Color: 270
Size: 498 Color: 109

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 5961 Color: 317
Size: 1987 Color: 224
Size: 396 Color: 90

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 6458 Color: 340
Size: 1574 Color: 200
Size: 312 Color: 70

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 7111 Color: 376
Size: 1029 Color: 160
Size: 204 Color: 36

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 351
Size: 1364 Color: 190
Size: 264 Color: 59

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 4174 Color: 274
Size: 3478 Color: 268
Size: 692 Color: 134

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 4858 Color: 292
Size: 2906 Color: 248
Size: 580 Color: 118

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 7382 Color: 395
Size: 802 Color: 143
Size: 160 Color: 20

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5630 Color: 309
Size: 2262 Color: 233
Size: 452 Color: 100

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 4745 Color: 285
Size: 3001 Color: 255
Size: 598 Color: 125

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 7050 Color: 372
Size: 1082 Color: 167
Size: 212 Color: 39

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 365
Size: 1153 Color: 175
Size: 230 Color: 45

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5056 Color: 294
Size: 2744 Color: 247
Size: 544 Color: 117

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6010 Color: 321
Size: 1946 Color: 221
Size: 388 Color: 89

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 7375 Color: 394
Size: 809 Color: 145
Size: 160 Color: 21

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 6178 Color: 327
Size: 1806 Color: 215
Size: 360 Color: 81

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 7001 Color: 368
Size: 1121 Color: 171
Size: 222 Color: 43

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 4769 Color: 286
Size: 2981 Color: 254
Size: 594 Color: 124

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 7345 Color: 392
Size: 947 Color: 155
Size: 52 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 5970 Color: 318
Size: 2114 Color: 230
Size: 260 Color: 58

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 4850 Color: 291
Size: 2914 Color: 249
Size: 580 Color: 119

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6450 Color: 339
Size: 1594 Color: 203
Size: 300 Color: 69

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5585 Color: 306
Size: 2301 Color: 236
Size: 458 Color: 103

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6050 Color: 322
Size: 1914 Color: 220
Size: 380 Color: 86

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 5221 Color: 299
Size: 2603 Color: 243
Size: 520 Color: 113

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 4794 Color: 289
Size: 2962 Color: 250
Size: 588 Color: 121

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 332
Size: 1633 Color: 208
Size: 326 Color: 76

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 328
Size: 1801 Color: 214
Size: 358 Color: 80

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 385
Size: 914 Color: 152
Size: 180 Color: 28

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 4890 Color: 293
Size: 3006 Color: 257
Size: 448 Color: 98

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 7263 Color: 386
Size: 901 Color: 151
Size: 180 Color: 27

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 7305 Color: 389
Size: 867 Color: 149
Size: 172 Color: 25

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7177 Color: 382
Size: 1141 Color: 174
Size: 26 Color: 0

Total size: 1101408
Total free space: 0

