Capicity Bin: 15296
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11336 Color: 482
Size: 2966 Color: 309
Size: 994 Color: 173

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11366 Color: 484
Size: 3578 Color: 330
Size: 352 Color: 58

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11370 Color: 485
Size: 3260 Color: 316
Size: 666 Color: 132

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11371 Color: 486
Size: 3029 Color: 311
Size: 896 Color: 164

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11378 Color: 487
Size: 3266 Color: 318
Size: 652 Color: 129

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11386 Color: 488
Size: 2918 Color: 306
Size: 992 Color: 170

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11388 Color: 489
Size: 3256 Color: 315
Size: 652 Color: 128

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11729 Color: 494
Size: 2973 Color: 310
Size: 594 Color: 120

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11809 Color: 500
Size: 2611 Color: 292
Size: 876 Color: 162

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11814 Color: 501
Size: 2902 Color: 302
Size: 580 Color: 114

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11816 Color: 502
Size: 2326 Color: 275
Size: 1154 Color: 192

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12144 Color: 509
Size: 1740 Color: 240
Size: 1412 Color: 213

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12166 Color: 512
Size: 2610 Color: 291
Size: 520 Color: 103

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 514
Size: 2600 Color: 290
Size: 512 Color: 101

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12276 Color: 515
Size: 2864 Color: 299
Size: 156 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12439 Color: 521
Size: 1961 Color: 255
Size: 896 Color: 163

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 523
Size: 2328 Color: 276
Size: 512 Color: 100

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12496 Color: 525
Size: 2152 Color: 268
Size: 648 Color: 126

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12578 Color: 527
Size: 2244 Color: 274
Size: 474 Color: 92

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 528
Size: 2044 Color: 262
Size: 668 Color: 133

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12660 Color: 531
Size: 2616 Color: 293
Size: 20 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12715 Color: 534
Size: 2151 Color: 267
Size: 430 Color: 84

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12720 Color: 535
Size: 1936 Color: 253
Size: 640 Color: 125

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12828 Color: 539
Size: 1636 Color: 228
Size: 832 Color: 152

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 540
Size: 1848 Color: 246
Size: 604 Color: 121

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 546
Size: 1736 Color: 239
Size: 624 Color: 124

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12940 Color: 547
Size: 1404 Color: 212
Size: 952 Color: 168

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 549
Size: 1496 Color: 220
Size: 824 Color: 150

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12990 Color: 550
Size: 1922 Color: 252
Size: 384 Color: 72

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12992 Color: 551
Size: 1724 Color: 238
Size: 580 Color: 116

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13004 Color: 552
Size: 1644 Color: 230
Size: 648 Color: 127

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 553
Size: 1696 Color: 234
Size: 580 Color: 115

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13068 Color: 554
Size: 1860 Color: 247
Size: 368 Color: 64

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 555
Size: 1272 Color: 199
Size: 928 Color: 165

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13111 Color: 557
Size: 1721 Color: 237
Size: 464 Color: 90

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13174 Color: 560
Size: 1384 Color: 211
Size: 738 Color: 142

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 561
Size: 1584 Color: 226
Size: 512 Color: 99

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13208 Color: 562
Size: 1696 Color: 235
Size: 392 Color: 75

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13210 Color: 563
Size: 1566 Color: 224
Size: 520 Color: 104

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13212 Color: 564
Size: 1316 Color: 204
Size: 768 Color: 145

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 573
Size: 1352 Color: 207
Size: 608 Color: 123

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13402 Color: 576
Size: 1028 Color: 177
Size: 866 Color: 160

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13418 Color: 577
Size: 1286 Color: 202
Size: 592 Color: 119

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 579
Size: 1264 Color: 197
Size: 580 Color: 117

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13488 Color: 581
Size: 1456 Color: 217
Size: 352 Color: 59

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13540 Color: 583
Size: 1468 Color: 219
Size: 288 Color: 29

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13542 Color: 584
Size: 1370 Color: 210
Size: 384 Color: 69

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13590 Color: 587
Size: 1354 Color: 208
Size: 352 Color: 60

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13616 Color: 589
Size: 1424 Color: 215
Size: 256 Color: 11

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13620 Color: 590
Size: 1100 Color: 186
Size: 576 Color: 113

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 593
Size: 1308 Color: 203
Size: 320 Color: 46

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13674 Color: 594
Size: 1150 Color: 190
Size: 472 Color: 91

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13684 Color: 595
Size: 1248 Color: 194
Size: 364 Color: 63

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 596
Size: 1096 Color: 185
Size: 512 Color: 102

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13718 Color: 597
Size: 1002 Color: 174
Size: 576 Color: 110

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13724 Color: 598
Size: 1040 Color: 180
Size: 532 Color: 106

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13732 Color: 599
Size: 1132 Color: 188
Size: 432 Color: 85

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 11798 Color: 498
Size: 2907 Color: 304
Size: 590 Color: 118

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 11806 Color: 499
Size: 1827 Color: 245
Size: 1662 Color: 232

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12165 Color: 511
Size: 2394 Color: 283
Size: 736 Color: 140

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 12338 Color: 517
Size: 2957 Color: 308

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 12449 Color: 522
Size: 1582 Color: 225
Size: 1264 Color: 196

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 12674 Color: 532
Size: 2621 Color: 294

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12697 Color: 533
Size: 1990 Color: 259
Size: 608 Color: 122

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 12943 Color: 548
Size: 2352 Color: 277

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13269 Color: 569
Size: 1364 Color: 209
Size: 662 Color: 131

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 572
Size: 1971 Color: 257

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 588
Size: 1691 Color: 233

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 8744 Color: 425
Size: 6550 Color: 402

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 9498 Color: 437
Size: 5444 Color: 380
Size: 352 Color: 56

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 10166 Color: 452
Size: 4816 Color: 366
Size: 312 Color: 40

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 10238 Color: 456
Size: 4764 Color: 364
Size: 292 Color: 35

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 11532 Color: 491
Size: 3762 Color: 339

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 11793 Color: 497
Size: 1900 Color: 249
Size: 1601 Color: 227

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 12390 Color: 519
Size: 2904 Color: 303

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 12654 Color: 530
Size: 2640 Color: 295

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 12872 Color: 542
Size: 2422 Color: 284

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 12931 Color: 545
Size: 2363 Color: 278

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13127 Color: 558
Size: 2167 Color: 271

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 565
Size: 2070 Color: 265

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 13318 Color: 571
Size: 1976 Color: 258

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 578
Size: 1870 Color: 248

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 585
Size: 1742 Color: 241

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 13654 Color: 592
Size: 1640 Color: 229

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 600
Size: 1540 Color: 223

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 8700 Color: 423
Size: 3331 Color: 323
Size: 3262 Color: 317

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 10850 Color: 468
Size: 4175 Color: 349
Size: 268 Color: 19

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 12153 Color: 510
Size: 3140 Color: 313

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 13233 Color: 567
Size: 2060 Color: 263

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 13264 Color: 568
Size: 2029 Color: 261

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 13302 Color: 570
Size: 1991 Color: 260

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 11888 Color: 505
Size: 3404 Color: 325

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 13228 Color: 566
Size: 2064 Color: 264

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 13340 Color: 574
Size: 1952 Color: 254

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 10099 Color: 448
Size: 5192 Color: 376

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 12910 Color: 544
Size: 2381 Color: 282

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 13105 Color: 556
Size: 2186 Color: 272

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 13375 Color: 575
Size: 1916 Color: 251

Bin 99: 6 of cap free
Amount of items: 9
Items: 
Size: 7650 Color: 405
Size: 1272 Color: 200
Size: 1272 Color: 198
Size: 1264 Color: 195
Size: 1248 Color: 193
Size: 680 Color: 135
Size: 672 Color: 134
Size: 656 Color: 130
Size: 576 Color: 111

Bin 100: 6 of cap free
Amount of items: 7
Items: 
Size: 7654 Color: 408
Size: 1964 Color: 256
Size: 1912 Color: 250
Size: 1770 Color: 242
Size: 1006 Color: 175
Size: 496 Color: 97
Size: 488 Color: 96

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 11738 Color: 495
Size: 3520 Color: 328
Size: 32 Color: 1

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 12380 Color: 518
Size: 2910 Color: 305

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 12814 Color: 538
Size: 2476 Color: 286

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13138 Color: 559
Size: 2152 Color: 269

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 591
Size: 1650 Color: 231

Bin 106: 7 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 459
Size: 4693 Color: 362
Size: 288 Color: 30

Bin 107: 7 of cap free
Amount of items: 3
Items: 
Size: 11822 Color: 503
Size: 3435 Color: 326
Size: 32 Color: 2

Bin 108: 8 of cap free
Amount of items: 3
Items: 
Size: 8716 Color: 424
Size: 6176 Color: 392
Size: 396 Color: 77

Bin 109: 8 of cap free
Amount of items: 3
Items: 
Size: 10924 Color: 472
Size: 4108 Color: 344
Size: 256 Color: 15

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 12168 Color: 513
Size: 3120 Color: 312

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 12400 Color: 520
Size: 2888 Color: 300

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 582
Size: 1776 Color: 243

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13576 Color: 586
Size: 1712 Color: 236

Bin 114: 9 of cap free
Amount of items: 3
Items: 
Size: 10859 Color: 470
Size: 4164 Color: 348
Size: 264 Color: 17

Bin 115: 9 of cap free
Amount of items: 3
Items: 
Size: 10879 Color: 471
Size: 4148 Color: 346
Size: 260 Color: 16

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 12743 Color: 536
Size: 2544 Color: 289

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 13478 Color: 580
Size: 1809 Color: 244

Bin 118: 10 of cap free
Amount of items: 3
Items: 
Size: 8562 Color: 419
Size: 6328 Color: 393
Size: 396 Color: 78

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 11064 Color: 475
Size: 4222 Color: 351

Bin 120: 11 of cap free
Amount of items: 2
Items: 
Size: 10013 Color: 445
Size: 5272 Color: 379

Bin 121: 11 of cap free
Amount of items: 2
Items: 
Size: 12909 Color: 543
Size: 2376 Color: 281

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 537
Size: 2524 Color: 288

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 12848 Color: 541
Size: 2436 Color: 285

Bin 124: 13 of cap free
Amount of items: 3
Items: 
Size: 10672 Color: 465
Size: 4339 Color: 358
Size: 272 Color: 22

Bin 125: 13 of cap free
Amount of items: 4
Items: 
Size: 11319 Color: 481
Size: 3736 Color: 338
Size: 128 Color: 5
Size: 100 Color: 4

Bin 126: 14 of cap free
Amount of items: 5
Items: 
Size: 7668 Color: 411
Size: 3304 Color: 320
Size: 2898 Color: 301
Size: 964 Color: 169
Size: 448 Color: 89

Bin 127: 14 of cap free
Amount of items: 5
Items: 
Size: 7688 Color: 412
Size: 3317 Color: 322
Size: 3315 Color: 321
Size: 522 Color: 105
Size: 440 Color: 88

Bin 128: 15 of cap free
Amount of items: 3
Items: 
Size: 10858 Color: 469
Size: 4159 Color: 347
Size: 264 Color: 18

Bin 129: 16 of cap free
Amount of items: 3
Items: 
Size: 8498 Color: 418
Size: 6374 Color: 399
Size: 408 Color: 79

Bin 130: 18 of cap free
Amount of items: 3
Items: 
Size: 10184 Color: 453
Size: 4782 Color: 365
Size: 312 Color: 39

Bin 131: 19 of cap free
Amount of items: 6
Items: 
Size: 7656 Color: 409
Size: 2368 Color: 279
Size: 2160 Color: 270
Size: 2129 Color: 266
Size: 484 Color: 95
Size: 480 Color: 94

Bin 132: 19 of cap free
Amount of items: 4
Items: 
Size: 9600 Color: 441
Size: 5017 Color: 371
Size: 332 Color: 49
Size: 328 Color: 48

Bin 133: 19 of cap free
Amount of items: 2
Items: 
Size: 11749 Color: 496
Size: 3528 Color: 329

Bin 134: 20 of cap free
Amount of items: 2
Items: 
Size: 12520 Color: 526
Size: 2756 Color: 297

Bin 135: 21 of cap free
Amount of items: 3
Items: 
Size: 9168 Color: 431
Size: 5723 Color: 388
Size: 384 Color: 67

Bin 136: 22 of cap free
Amount of items: 3
Items: 
Size: 10120 Color: 450
Size: 4834 Color: 367
Size: 320 Color: 43

Bin 137: 22 of cap free
Amount of items: 2
Items: 
Size: 11568 Color: 492
Size: 3706 Color: 337

Bin 138: 22 of cap free
Amount of items: 2
Items: 
Size: 12612 Color: 529
Size: 2662 Color: 296

Bin 139: 23 of cap free
Amount of items: 2
Items: 
Size: 10033 Color: 446
Size: 5240 Color: 378

Bin 140: 23 of cap free
Amount of items: 2
Items: 
Size: 12461 Color: 524
Size: 2812 Color: 298

Bin 141: 24 of cap free
Amount of items: 3
Items: 
Size: 9048 Color: 429
Size: 5840 Color: 391
Size: 384 Color: 70

Bin 142: 24 of cap free
Amount of items: 3
Items: 
Size: 10782 Color: 466
Size: 4218 Color: 350
Size: 272 Color: 21

Bin 143: 24 of cap free
Amount of items: 3
Items: 
Size: 11036 Color: 474
Size: 3980 Color: 342
Size: 256 Color: 13

Bin 144: 24 of cap free
Amount of items: 2
Items: 
Size: 11832 Color: 504
Size: 3440 Color: 327

Bin 145: 25 of cap free
Amount of items: 2
Items: 
Size: 11924 Color: 506
Size: 3347 Color: 324

Bin 146: 26 of cap free
Amount of items: 2
Items: 
Size: 11996 Color: 507
Size: 3274 Color: 319

Bin 147: 26 of cap free
Amount of items: 2
Items: 
Size: 12102 Color: 508
Size: 3168 Color: 314

Bin 148: 26 of cap free
Amount of items: 2
Items: 
Size: 12332 Color: 516
Size: 2938 Color: 307

Bin 149: 32 of cap free
Amount of items: 3
Items: 
Size: 11363 Color: 483
Size: 3833 Color: 340
Size: 68 Color: 3

Bin 150: 36 of cap free
Amount of items: 3
Items: 
Size: 10988 Color: 473
Size: 4016 Color: 343
Size: 256 Color: 14

Bin 151: 37 of cap free
Amount of items: 2
Items: 
Size: 10107 Color: 449
Size: 5152 Color: 375

Bin 152: 37 of cap free
Amount of items: 2
Items: 
Size: 11663 Color: 493
Size: 3596 Color: 331

Bin 153: 40 of cap free
Amount of items: 2
Items: 
Size: 11400 Color: 490
Size: 3856 Color: 341

Bin 154: 42 of cap free
Amount of items: 3
Items: 
Size: 10230 Color: 455
Size: 4724 Color: 363
Size: 300 Color: 36

Bin 155: 45 of cap free
Amount of items: 3
Items: 
Size: 9337 Color: 436
Size: 5562 Color: 386
Size: 352 Color: 57

Bin 156: 47 of cap free
Amount of items: 7
Items: 
Size: 7653 Color: 407
Size: 1520 Color: 222
Size: 1518 Color: 221
Size: 1462 Color: 218
Size: 1448 Color: 216
Size: 1152 Color: 191
Size: 496 Color: 98

Bin 157: 55 of cap free
Amount of items: 2
Items: 
Size: 8626 Color: 420
Size: 6615 Color: 403

Bin 158: 55 of cap free
Amount of items: 3
Items: 
Size: 9080 Color: 430
Size: 5777 Color: 390
Size: 384 Color: 68

Bin 159: 55 of cap free
Amount of items: 4
Items: 
Size: 9588 Color: 440
Size: 4981 Color: 370
Size: 336 Color: 51
Size: 336 Color: 50

Bin 160: 58 of cap free
Amount of items: 9
Items: 
Size: 7652 Color: 406
Size: 1422 Color: 214
Size: 1348 Color: 206
Size: 1318 Color: 205
Size: 1274 Color: 201
Size: 576 Color: 112
Size: 560 Color: 109
Size: 544 Color: 108
Size: 544 Color: 107

Bin 161: 63 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 444
Size: 5033 Color: 373
Size: 320 Color: 44

Bin 162: 69 of cap free
Amount of items: 2
Items: 
Size: 10091 Color: 447
Size: 5136 Color: 374

Bin 163: 72 of cap free
Amount of items: 4
Items: 
Size: 10324 Color: 460
Size: 4328 Color: 356
Size: 288 Color: 28
Size: 284 Color: 27

Bin 164: 73 of cap free
Amount of items: 3
Items: 
Size: 11281 Color: 478
Size: 3694 Color: 334
Size: 248 Color: 9

Bin 165: 75 of cap free
Amount of items: 4
Items: 
Size: 9562 Color: 439
Size: 4973 Color: 369
Size: 344 Color: 53
Size: 342 Color: 52

Bin 166: 77 of cap free
Amount of items: 3
Items: 
Size: 9872 Color: 443
Size: 5027 Color: 372
Size: 320 Color: 45

Bin 167: 77 of cap free
Amount of items: 3
Items: 
Size: 10560 Color: 464
Size: 4387 Color: 359
Size: 272 Color: 23

Bin 168: 79 of cap free
Amount of items: 3
Items: 
Size: 10824 Color: 467
Size: 4121 Color: 345
Size: 272 Color: 20

Bin 169: 86 of cap free
Amount of items: 3
Items: 
Size: 8429 Color: 417
Size: 6373 Color: 398
Size: 408 Color: 80

Bin 170: 89 of cap free
Amount of items: 4
Items: 
Size: 9552 Color: 438
Size: 4967 Color: 368
Size: 344 Color: 55
Size: 344 Color: 54

Bin 171: 91 of cap free
Amount of items: 5
Items: 
Size: 7664 Color: 410
Size: 2488 Color: 287
Size: 2373 Color: 280
Size: 2204 Color: 273
Size: 476 Color: 93

Bin 172: 109 of cap free
Amount of items: 3
Items: 
Size: 9329 Color: 435
Size: 5506 Color: 385
Size: 352 Color: 61

Bin 173: 113 of cap free
Amount of items: 3
Items: 
Size: 9321 Color: 434
Size: 5502 Color: 384
Size: 360 Color: 62

Bin 174: 115 of cap free
Amount of items: 3
Items: 
Size: 11298 Color: 479
Size: 3699 Color: 335
Size: 184 Color: 8

Bin 175: 116 of cap free
Amount of items: 3
Items: 
Size: 10372 Color: 462
Size: 4528 Color: 361
Size: 280 Color: 25

Bin 176: 117 of cap free
Amount of items: 4
Items: 
Size: 10307 Color: 458
Size: 4296 Color: 355
Size: 288 Color: 32
Size: 288 Color: 31

Bin 177: 119 of cap free
Amount of items: 3
Items: 
Size: 11299 Color: 480
Size: 3702 Color: 336
Size: 176 Color: 7

Bin 178: 127 of cap free
Amount of items: 3
Items: 
Size: 8385 Color: 416
Size: 6372 Color: 397
Size: 412 Color: 81

Bin 179: 132 of cap free
Amount of items: 3
Items: 
Size: 9628 Color: 442
Size: 5208 Color: 377
Size: 328 Color: 47

Bin 180: 135 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 428
Size: 5761 Color: 389
Size: 384 Color: 71

Bin 181: 143 of cap free
Amount of items: 3
Items: 
Size: 9277 Color: 433
Size: 5500 Color: 383
Size: 376 Color: 65

Bin 182: 145 of cap free
Amount of items: 3
Items: 
Size: 8365 Color: 415
Size: 6370 Color: 396
Size: 416 Color: 82

Bin 183: 145 of cap free
Amount of items: 3
Items: 
Size: 10351 Color: 461
Size: 4520 Color: 360
Size: 280 Color: 26

Bin 184: 147 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 477
Size: 3681 Color: 333
Size: 256 Color: 10

Bin 185: 155 of cap free
Amount of items: 4
Items: 
Size: 10287 Color: 457
Size: 4278 Color: 354
Size: 288 Color: 34
Size: 288 Color: 33

Bin 186: 179 of cap free
Amount of items: 3
Items: 
Size: 9257 Color: 432
Size: 5484 Color: 382
Size: 376 Color: 66

Bin 187: 181 of cap free
Amount of items: 3
Items: 
Size: 10512 Color: 463
Size: 4331 Color: 357
Size: 272 Color: 24

Bin 188: 204 of cap free
Amount of items: 3
Items: 
Size: 8304 Color: 414
Size: 6364 Color: 395
Size: 424 Color: 83

Bin 189: 208 of cap free
Amount of items: 4
Items: 
Size: 10216 Color: 454
Size: 4264 Color: 353
Size: 304 Color: 38
Size: 304 Color: 37

Bin 190: 212 of cap free
Amount of items: 3
Items: 
Size: 11184 Color: 476
Size: 3644 Color: 332
Size: 256 Color: 12

Bin 191: 214 of cap free
Amount of items: 2
Items: 
Size: 8698 Color: 422
Size: 6384 Color: 401

Bin 192: 230 of cap free
Amount of items: 2
Items: 
Size: 8690 Color: 421
Size: 6376 Color: 400

Bin 193: 254 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 427
Size: 5666 Color: 387
Size: 392 Color: 73

Bin 194: 256 of cap free
Amount of items: 4
Items: 
Size: 10152 Color: 451
Size: 4248 Color: 352
Size: 320 Color: 42
Size: 320 Color: 41

Bin 195: 282 of cap free
Amount of items: 4
Items: 
Size: 8764 Color: 426
Size: 5464 Color: 381
Size: 394 Color: 76
Size: 392 Color: 74

Bin 196: 323 of cap free
Amount of items: 9
Items: 
Size: 7649 Color: 404
Size: 1144 Color: 189
Size: 1108 Color: 187
Size: 1096 Color: 184
Size: 1096 Color: 183
Size: 736 Color: 139
Size: 728 Color: 138
Size: 712 Color: 137
Size: 704 Color: 136

Bin 197: 364 of cap free
Amount of items: 4
Items: 
Size: 7720 Color: 413
Size: 6344 Color: 394
Size: 436 Color: 87
Size: 432 Color: 86

Bin 198: 796 of cap free
Amount of items: 16
Items: 
Size: 1088 Color: 182
Size: 1088 Color: 181
Size: 1040 Color: 179
Size: 1040 Color: 178
Size: 1024 Color: 176
Size: 994 Color: 172
Size: 992 Color: 171
Size: 944 Color: 167
Size: 830 Color: 151
Size: 824 Color: 149
Size: 824 Color: 148
Size: 816 Color: 147
Size: 768 Color: 146
Size: 752 Color: 144
Size: 740 Color: 143
Size: 736 Color: 141

Bin 199: 7572 of cap free
Amount of items: 9
Items: 
Size: 944 Color: 166
Size: 866 Color: 161
Size: 852 Color: 159
Size: 848 Color: 158
Size: 848 Color: 157
Size: 848 Color: 156
Size: 844 Color: 155
Size: 840 Color: 154
Size: 834 Color: 153

Total size: 3028608
Total free space: 15296

