Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 9848 Color: 9
Size: 5094 Color: 11
Size: 3950 Color: 4
Size: 424 Color: 3
Size: 332 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 13
Size: 8144 Color: 11
Size: 592 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 0
Size: 6896 Color: 1
Size: 464 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 11
Size: 6992 Color: 12
Size: 332 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12579 Color: 4
Size: 6333 Color: 6
Size: 736 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 5
Size: 6384 Color: 10
Size: 672 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13152 Color: 7
Size: 5954 Color: 19
Size: 542 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13552 Color: 16
Size: 5208 Color: 1
Size: 888 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14296 Color: 1
Size: 4960 Color: 17
Size: 392 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14236 Color: 2
Size: 4544 Color: 15
Size: 868 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14430 Color: 0
Size: 4350 Color: 9
Size: 868 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14803 Color: 9
Size: 3433 Color: 10
Size: 1412 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14878 Color: 11
Size: 3512 Color: 13
Size: 1258 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14910 Color: 11
Size: 4620 Color: 4
Size: 118 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14942 Color: 15
Size: 4358 Color: 6
Size: 348 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15192 Color: 1
Size: 3022 Color: 12
Size: 1434 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15136 Color: 8
Size: 4192 Color: 18
Size: 320 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15147 Color: 3
Size: 4399 Color: 11
Size: 102 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15180 Color: 16
Size: 3476 Color: 11
Size: 992 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15264 Color: 1
Size: 4016 Color: 18
Size: 368 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15237 Color: 4
Size: 3677 Color: 19
Size: 734 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15529 Color: 18
Size: 2273 Color: 9
Size: 1846 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 15536 Color: 10
Size: 3680 Color: 12
Size: 432 Color: 14

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 15560 Color: 12
Size: 3168 Color: 10
Size: 920 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 15678 Color: 15
Size: 2962 Color: 13
Size: 1008 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 15728 Color: 1
Size: 3056 Color: 12
Size: 864 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 15898 Color: 15
Size: 3280 Color: 9
Size: 470 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16022 Color: 19
Size: 3530 Color: 11
Size: 96 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16246 Color: 4
Size: 2838 Color: 5
Size: 564 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16400 Color: 2
Size: 2592 Color: 13
Size: 656 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16432 Color: 2
Size: 1872 Color: 18
Size: 1344 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16484 Color: 17
Size: 2140 Color: 16
Size: 1024 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16538 Color: 19
Size: 2566 Color: 6
Size: 544 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16570 Color: 16
Size: 2454 Color: 16
Size: 624 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16712 Color: 13
Size: 2448 Color: 9
Size: 488 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16720 Color: 13
Size: 1662 Color: 13
Size: 1266 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16740 Color: 16
Size: 2428 Color: 19
Size: 480 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16848 Color: 12
Size: 1600 Color: 4
Size: 1200 Color: 16

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16864 Color: 3
Size: 2352 Color: 10
Size: 432 Color: 15

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16958 Color: 13
Size: 1392 Color: 13
Size: 1298 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16984 Color: 10
Size: 2232 Color: 11
Size: 432 Color: 18

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17016 Color: 3
Size: 2004 Color: 16
Size: 628 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17094 Color: 19
Size: 1678 Color: 4
Size: 876 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17104 Color: 10
Size: 2240 Color: 15
Size: 304 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17120 Color: 4
Size: 1344 Color: 1
Size: 1184 Color: 9

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17244 Color: 3
Size: 1216 Color: 15
Size: 1188 Color: 14

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17268 Color: 18
Size: 1720 Color: 14
Size: 660 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17304 Color: 1
Size: 1492 Color: 2
Size: 852 Color: 11

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17396 Color: 7
Size: 1884 Color: 2
Size: 368 Color: 14

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17418 Color: 16
Size: 1750 Color: 11
Size: 480 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17524 Color: 10
Size: 1772 Color: 5
Size: 352 Color: 16

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17536 Color: 6
Size: 1280 Color: 11
Size: 832 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 17592 Color: 19
Size: 1424 Color: 8
Size: 632 Color: 17

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 17632 Color: 9
Size: 1376 Color: 7
Size: 640 Color: 6

Bin 55: 1 of cap free
Amount of items: 7
Items: 
Size: 9832 Color: 6
Size: 2455 Color: 1
Size: 2000 Color: 6
Size: 1914 Color: 10
Size: 1862 Color: 14
Size: 880 Color: 0
Size: 704 Color: 3

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 11122 Color: 12
Size: 8525 Color: 14

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12043 Color: 14
Size: 7044 Color: 1
Size: 560 Color: 10

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12684 Color: 6
Size: 6291 Color: 13
Size: 672 Color: 6

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 14049 Color: 1
Size: 4472 Color: 17
Size: 1126 Color: 10

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 13
Size: 4299 Color: 5
Size: 1020 Color: 1

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 15520 Color: 0
Size: 2719 Color: 11
Size: 1408 Color: 8

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 15871 Color: 7
Size: 3310 Color: 14
Size: 466 Color: 19

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 16278 Color: 11
Size: 2993 Color: 0
Size: 376 Color: 9

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 17045 Color: 16
Size: 2602 Color: 18

Bin 65: 2 of cap free
Amount of items: 34
Items: 
Size: 832 Color: 4
Size: 806 Color: 3
Size: 800 Color: 15
Size: 744 Color: 8
Size: 704 Color: 19
Size: 704 Color: 16
Size: 704 Color: 11
Size: 704 Color: 7
Size: 686 Color: 5
Size: 672 Color: 19
Size: 672 Color: 19
Size: 672 Color: 15
Size: 672 Color: 12
Size: 598 Color: 15
Size: 584 Color: 3
Size: 576 Color: 18
Size: 576 Color: 12
Size: 576 Color: 9
Size: 544 Color: 11
Size: 524 Color: 9
Size: 516 Color: 15
Size: 516 Color: 7
Size: 512 Color: 17
Size: 512 Color: 9
Size: 512 Color: 8
Size: 510 Color: 2
Size: 482 Color: 16
Size: 480 Color: 17
Size: 400 Color: 13
Size: 400 Color: 12
Size: 400 Color: 2
Size: 384 Color: 13
Size: 352 Color: 5
Size: 320 Color: 7

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 13538 Color: 12
Size: 6108 Color: 18

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 18
Size: 4110 Color: 11
Size: 1428 Color: 4

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 14454 Color: 11
Size: 4536 Color: 8
Size: 656 Color: 1

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 14816 Color: 14
Size: 4382 Color: 7
Size: 448 Color: 1

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 15414 Color: 3
Size: 4232 Color: 2

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 15710 Color: 1
Size: 3416 Color: 0
Size: 520 Color: 0

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 16094 Color: 9
Size: 3552 Color: 14

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 16288 Color: 12
Size: 2174 Color: 15
Size: 1184 Color: 1

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 16908 Color: 17
Size: 1858 Color: 1
Size: 880 Color: 12

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 16910 Color: 3
Size: 2736 Color: 4

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 17354 Color: 6
Size: 2292 Color: 5

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 17582 Color: 18
Size: 2064 Color: 3

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 17638 Color: 14
Size: 1408 Color: 11
Size: 600 Color: 5

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 11037 Color: 11
Size: 8160 Color: 10
Size: 448 Color: 13

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 11093 Color: 16
Size: 8184 Color: 17
Size: 368 Color: 1

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 16585 Color: 6
Size: 2128 Color: 11
Size: 932 Color: 19

Bin 82: 4 of cap free
Amount of items: 13
Items: 
Size: 9826 Color: 4
Size: 1024 Color: 10
Size: 1016 Color: 15
Size: 1010 Color: 8
Size: 920 Color: 4
Size: 896 Color: 3
Size: 884 Color: 4
Size: 864 Color: 14
Size: 736 Color: 7
Size: 704 Color: 5
Size: 704 Color: 2
Size: 660 Color: 1
Size: 400 Color: 13

Bin 83: 4 of cap free
Amount of items: 5
Items: 
Size: 9840 Color: 9
Size: 2760 Color: 17
Size: 2644 Color: 19
Size: 2618 Color: 13
Size: 1782 Color: 16

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 9904 Color: 6
Size: 4880 Color: 6
Size: 4860 Color: 1

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 14
Size: 4592 Color: 18
Size: 3864 Color: 14

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 14422 Color: 13
Size: 4614 Color: 16
Size: 608 Color: 18

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 14576 Color: 9
Size: 5068 Color: 6

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 15776 Color: 14
Size: 3720 Color: 9
Size: 148 Color: 7

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 17514 Color: 4
Size: 2130 Color: 14

Bin 90: 5 of cap free
Amount of items: 3
Items: 
Size: 13474 Color: 9
Size: 5565 Color: 16
Size: 604 Color: 5

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 16706 Color: 2
Size: 2937 Color: 0

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 16751 Color: 12
Size: 2892 Color: 5

Bin 93: 6 of cap free
Amount of items: 3
Items: 
Size: 11006 Color: 1
Size: 8182 Color: 10
Size: 454 Color: 4

Bin 94: 6 of cap free
Amount of items: 3
Items: 
Size: 11090 Color: 5
Size: 8168 Color: 10
Size: 384 Color: 4

Bin 95: 6 of cap free
Amount of items: 3
Items: 
Size: 11240 Color: 16
Size: 7202 Color: 9
Size: 1200 Color: 5

Bin 96: 6 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 12
Size: 6322 Color: 13
Size: 1600 Color: 4

Bin 97: 6 of cap free
Amount of items: 3
Items: 
Size: 13254 Color: 16
Size: 5556 Color: 2
Size: 832 Color: 13

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 16674 Color: 1
Size: 2968 Color: 19

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 17654 Color: 13
Size: 1988 Color: 12

Bin 100: 7 of cap free
Amount of items: 3
Items: 
Size: 13585 Color: 6
Size: 5928 Color: 13
Size: 128 Color: 9

Bin 101: 7 of cap free
Amount of items: 3
Items: 
Size: 13641 Color: 18
Size: 5360 Color: 1
Size: 640 Color: 5

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 15208 Color: 11
Size: 4433 Color: 6

Bin 103: 7 of cap free
Amount of items: 2
Items: 
Size: 15602 Color: 13
Size: 4039 Color: 16

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 17226 Color: 16
Size: 2415 Color: 8

Bin 105: 8 of cap free
Amount of items: 3
Items: 
Size: 12448 Color: 1
Size: 6616 Color: 3
Size: 576 Color: 16

Bin 106: 8 of cap free
Amount of items: 3
Items: 
Size: 14820 Color: 19
Size: 4620 Color: 16
Size: 200 Color: 8

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 15832 Color: 9
Size: 3808 Color: 3

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 15960 Color: 2
Size: 3680 Color: 12

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 16792 Color: 2
Size: 2848 Color: 5

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 17680 Color: 7
Size: 1960 Color: 12

Bin 111: 9 of cap free
Amount of items: 3
Items: 
Size: 16057 Color: 10
Size: 1886 Color: 1
Size: 1696 Color: 6

Bin 112: 9 of cap free
Amount of items: 3
Items: 
Size: 16387 Color: 7
Size: 3188 Color: 3
Size: 64 Color: 12

Bin 113: 10 of cap free
Amount of items: 2
Items: 
Size: 15216 Color: 9
Size: 4422 Color: 16

Bin 114: 10 of cap free
Amount of items: 3
Items: 
Size: 15322 Color: 0
Size: 4028 Color: 13
Size: 288 Color: 16

Bin 115: 11 of cap free
Amount of items: 2
Items: 
Size: 12506 Color: 13
Size: 7131 Color: 5

Bin 116: 11 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 9
Size: 5053 Color: 17

Bin 117: 11 of cap free
Amount of items: 2
Items: 
Size: 16488 Color: 19
Size: 3149 Color: 10

Bin 118: 11 of cap free
Amount of items: 2
Items: 
Size: 17084 Color: 0
Size: 2553 Color: 16

Bin 119: 12 of cap free
Amount of items: 2
Items: 
Size: 16348 Color: 14
Size: 3288 Color: 11

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 16608 Color: 7
Size: 3028 Color: 15

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 17042 Color: 5
Size: 2594 Color: 13

Bin 122: 13 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 0
Size: 5635 Color: 6
Size: 192 Color: 8

Bin 123: 13 of cap free
Amount of items: 2
Items: 
Size: 15119 Color: 3
Size: 4516 Color: 6

Bin 124: 14 of cap free
Amount of items: 3
Items: 
Size: 13370 Color: 15
Size: 6008 Color: 6
Size: 256 Color: 10

Bin 125: 14 of cap free
Amount of items: 3
Items: 
Size: 13506 Color: 11
Size: 4968 Color: 19
Size: 1160 Color: 5

Bin 126: 15 of cap free
Amount of items: 3
Items: 
Size: 11026 Color: 0
Size: 8183 Color: 14
Size: 424 Color: 7

Bin 127: 15 of cap free
Amount of items: 2
Items: 
Size: 16823 Color: 14
Size: 2810 Color: 17

Bin 128: 16 of cap free
Amount of items: 3
Items: 
Size: 12464 Color: 6
Size: 6720 Color: 8
Size: 448 Color: 9

Bin 129: 16 of cap free
Amount of items: 3
Items: 
Size: 14112 Color: 10
Size: 5104 Color: 6
Size: 416 Color: 13

Bin 130: 16 of cap free
Amount of items: 3
Items: 
Size: 14342 Color: 17
Size: 5122 Color: 7
Size: 168 Color: 15

Bin 131: 16 of cap free
Amount of items: 3
Items: 
Size: 15344 Color: 4
Size: 3072 Color: 7
Size: 1216 Color: 1

Bin 132: 16 of cap free
Amount of items: 2
Items: 
Size: 15392 Color: 3
Size: 4240 Color: 19

Bin 133: 16 of cap free
Amount of items: 3
Items: 
Size: 16016 Color: 7
Size: 3552 Color: 6
Size: 64 Color: 1

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 17176 Color: 12
Size: 2456 Color: 11

Bin 135: 18 of cap free
Amount of items: 2
Items: 
Size: 13728 Color: 4
Size: 5902 Color: 1

Bin 136: 18 of cap free
Amount of items: 3
Items: 
Size: 17422 Color: 9
Size: 2176 Color: 0
Size: 32 Color: 12

Bin 137: 19 of cap free
Amount of items: 2
Items: 
Size: 12988 Color: 14
Size: 6641 Color: 17

Bin 138: 19 of cap free
Amount of items: 2
Items: 
Size: 16503 Color: 6
Size: 3126 Color: 4

Bin 139: 20 of cap free
Amount of items: 3
Items: 
Size: 14860 Color: 12
Size: 4640 Color: 19
Size: 128 Color: 0

Bin 140: 20 of cap free
Amount of items: 2
Items: 
Size: 15904 Color: 11
Size: 3724 Color: 0

Bin 141: 20 of cap free
Amount of items: 2
Items: 
Size: 17386 Color: 17
Size: 2242 Color: 3

Bin 142: 22 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 1
Size: 7186 Color: 3

Bin 143: 22 of cap free
Amount of items: 3
Items: 
Size: 12474 Color: 6
Size: 6000 Color: 1
Size: 1152 Color: 12

Bin 144: 22 of cap free
Amount of items: 2
Items: 
Size: 15704 Color: 5
Size: 3922 Color: 11

Bin 145: 22 of cap free
Amount of items: 2
Items: 
Size: 16020 Color: 11
Size: 3606 Color: 8

Bin 146: 22 of cap free
Amount of items: 2
Items: 
Size: 16344 Color: 14
Size: 3282 Color: 19

Bin 147: 23 of cap free
Amount of items: 2
Items: 
Size: 16921 Color: 11
Size: 2704 Color: 13

Bin 148: 24 of cap free
Amount of items: 2
Items: 
Size: 13820 Color: 8
Size: 5804 Color: 11

Bin 149: 24 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 16
Size: 5234 Color: 7

Bin 150: 24 of cap free
Amount of items: 2
Items: 
Size: 17480 Color: 1
Size: 2144 Color: 12

Bin 151: 24 of cap free
Amount of items: 2
Items: 
Size: 17424 Color: 5
Size: 2200 Color: 6

Bin 152: 24 of cap free
Amount of items: 3
Items: 
Size: 17550 Color: 10
Size: 2042 Color: 3
Size: 32 Color: 5

Bin 153: 24 of cap free
Amount of items: 2
Items: 
Size: 17552 Color: 8
Size: 2072 Color: 2

Bin 154: 25 of cap free
Amount of items: 11
Items: 
Size: 9825 Color: 3
Size: 1176 Color: 0
Size: 1112 Color: 6
Size: 1104 Color: 18
Size: 1044 Color: 0
Size: 1024 Color: 17
Size: 964 Color: 12
Size: 960 Color: 4
Size: 858 Color: 7
Size: 788 Color: 5
Size: 768 Color: 0

Bin 155: 26 of cap free
Amount of items: 2
Items: 
Size: 12570 Color: 8
Size: 7052 Color: 11

Bin 156: 27 of cap free
Amount of items: 2
Items: 
Size: 12887 Color: 19
Size: 6734 Color: 14

Bin 157: 28 of cap free
Amount of items: 2
Items: 
Size: 13572 Color: 11
Size: 6048 Color: 3

Bin 158: 28 of cap free
Amount of items: 2
Items: 
Size: 16180 Color: 4
Size: 3440 Color: 0

Bin 159: 28 of cap free
Amount of items: 2
Items: 
Size: 17668 Color: 8
Size: 1952 Color: 10

Bin 160: 29 of cap free
Amount of items: 2
Items: 
Size: 14491 Color: 12
Size: 5128 Color: 8

Bin 161: 29 of cap free
Amount of items: 2
Items: 
Size: 16245 Color: 5
Size: 3374 Color: 4

Bin 162: 32 of cap free
Amount of items: 2
Items: 
Size: 14456 Color: 7
Size: 5160 Color: 10

Bin 163: 36 of cap free
Amount of items: 9
Items: 
Size: 9836 Color: 3
Size: 1632 Color: 9
Size: 1620 Color: 16
Size: 1436 Color: 16
Size: 1408 Color: 1
Size: 1312 Color: 17
Size: 960 Color: 15
Size: 896 Color: 5
Size: 512 Color: 13

Bin 164: 36 of cap free
Amount of items: 2
Items: 
Size: 16956 Color: 10
Size: 2656 Color: 6

Bin 165: 36 of cap free
Amount of items: 2
Items: 
Size: 17328 Color: 17
Size: 2284 Color: 13

Bin 166: 38 of cap free
Amount of items: 2
Items: 
Size: 11408 Color: 2
Size: 8202 Color: 8

Bin 167: 38 of cap free
Amount of items: 3
Items: 
Size: 11744 Color: 9
Size: 7146 Color: 3
Size: 720 Color: 4

Bin 168: 38 of cap free
Amount of items: 2
Items: 
Size: 16530 Color: 4
Size: 3080 Color: 11

Bin 169: 40 of cap free
Amount of items: 2
Items: 
Size: 13568 Color: 17
Size: 6040 Color: 9

Bin 170: 43 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 4
Size: 5889 Color: 15
Size: 220 Color: 13

Bin 171: 53 of cap free
Amount of items: 2
Items: 
Size: 12971 Color: 16
Size: 6624 Color: 19

Bin 172: 54 of cap free
Amount of items: 2
Items: 
Size: 17202 Color: 19
Size: 2392 Color: 0

Bin 173: 54 of cap free
Amount of items: 2
Items: 
Size: 17312 Color: 18
Size: 2282 Color: 3

Bin 174: 56 of cap free
Amount of items: 3
Items: 
Size: 11570 Color: 18
Size: 7638 Color: 17
Size: 384 Color: 16

Bin 175: 60 of cap free
Amount of items: 5
Items: 
Size: 9834 Color: 9
Size: 2756 Color: 12
Size: 2621 Color: 7
Size: 2355 Color: 9
Size: 2022 Color: 15

Bin 176: 60 of cap free
Amount of items: 2
Items: 
Size: 12127 Color: 1
Size: 7461 Color: 11

Bin 177: 60 of cap free
Amount of items: 2
Items: 
Size: 15828 Color: 19
Size: 3760 Color: 13

Bin 178: 62 of cap free
Amount of items: 2
Items: 
Size: 14114 Color: 12
Size: 5472 Color: 11

Bin 179: 63 of cap free
Amount of items: 2
Items: 
Size: 12408 Color: 19
Size: 7177 Color: 18

Bin 180: 68 of cap free
Amount of items: 2
Items: 
Size: 15484 Color: 11
Size: 4096 Color: 9

Bin 181: 81 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 8
Size: 6339 Color: 7
Size: 896 Color: 9

Bin 182: 85 of cap free
Amount of items: 3
Items: 
Size: 10695 Color: 6
Size: 8180 Color: 14
Size: 688 Color: 9

Bin 183: 86 of cap free
Amount of items: 2
Items: 
Size: 14352 Color: 12
Size: 5210 Color: 16

Bin 184: 96 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 13
Size: 7016 Color: 10

Bin 185: 102 of cap free
Amount of items: 3
Items: 
Size: 11058 Color: 5
Size: 7704 Color: 11
Size: 784 Color: 9

Bin 186: 102 of cap free
Amount of items: 2
Items: 
Size: 14216 Color: 6
Size: 5330 Color: 0

Bin 187: 106 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 16
Size: 4330 Color: 14
Size: 4016 Color: 13

Bin 188: 117 of cap free
Amount of items: 2
Items: 
Size: 14864 Color: 17
Size: 4667 Color: 5

Bin 189: 119 of cap free
Amount of items: 7
Items: 
Size: 9829 Color: 4
Size: 1856 Color: 8
Size: 1816 Color: 7
Size: 1780 Color: 10
Size: 1648 Color: 2
Size: 1632 Color: 13
Size: 968 Color: 5

Bin 190: 132 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 5
Size: 6732 Color: 19

Bin 191: 136 of cap free
Amount of items: 2
Items: 
Size: 12350 Color: 12
Size: 7162 Color: 5

Bin 192: 142 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 10
Size: 6082 Color: 15

Bin 193: 144 of cap free
Amount of items: 3
Items: 
Size: 9888 Color: 8
Size: 8176 Color: 16
Size: 1440 Color: 1

Bin 194: 164 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 19
Size: 7808 Color: 18
Size: 404 Color: 5

Bin 195: 180 of cap free
Amount of items: 2
Items: 
Size: 11280 Color: 16
Size: 8188 Color: 3

Bin 196: 200 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 14
Size: 6032 Color: 10

Bin 197: 221 of cap free
Amount of items: 2
Items: 
Size: 12099 Color: 17
Size: 7328 Color: 19

Bin 198: 240 of cap free
Amount of items: 8
Items: 
Size: 9828 Color: 18
Size: 1636 Color: 6
Size: 1632 Color: 19
Size: 1632 Color: 18
Size: 1632 Color: 16
Size: 1632 Color: 11
Size: 1064 Color: 3
Size: 352 Color: 4

Bin 199: 15104 of cap free
Amount of items: 12
Items: 
Size: 456 Color: 14
Size: 456 Color: 10
Size: 448 Color: 0
Size: 384 Color: 13
Size: 384 Color: 6
Size: 380 Color: 13
Size: 356 Color: 17
Size: 352 Color: 5
Size: 352 Color: 2
Size: 336 Color: 12
Size: 320 Color: 18
Size: 320 Color: 8

Total size: 3890304
Total free space: 19648

