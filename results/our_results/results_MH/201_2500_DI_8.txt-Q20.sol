Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1452 Color: 2
Size: 840 Color: 2
Size: 164 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1547 Color: 9
Size: 841 Color: 11
Size: 68 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 7
Size: 745 Color: 8
Size: 148 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1668 Color: 2
Size: 634 Color: 1
Size: 154 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 14
Size: 674 Color: 14
Size: 72 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 9
Size: 605 Color: 1
Size: 120 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 12
Size: 611 Color: 17
Size: 120 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 2
Size: 604 Color: 12
Size: 112 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 18
Size: 550 Color: 1
Size: 92 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1844 Color: 6
Size: 540 Color: 16
Size: 72 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 15
Size: 482 Color: 16
Size: 92 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 11
Size: 466 Color: 16
Size: 76 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 9
Size: 374 Color: 5
Size: 88 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 2
Size: 444 Color: 0
Size: 56 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 3
Size: 410 Color: 2
Size: 80 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2052 Color: 9
Size: 364 Color: 10
Size: 40 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 14
Size: 402 Color: 12
Size: 76 Color: 19

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2010 Color: 0
Size: 280 Color: 16
Size: 166 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 9
Size: 334 Color: 10
Size: 64 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 13
Size: 330 Color: 10
Size: 64 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 9
Size: 210 Color: 17
Size: 132 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 9
Size: 228 Color: 7
Size: 110 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2132 Color: 8
Size: 300 Color: 14
Size: 24 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 6
Size: 172 Color: 3
Size: 112 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 9
Size: 214 Color: 13
Size: 40 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 17
Size: 200 Color: 3
Size: 64 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 7
Size: 244 Color: 9
Size: 8 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2206 Color: 18
Size: 242 Color: 10
Size: 8 Color: 7

Bin 29: 1 of cap free
Amount of items: 5
Items: 
Size: 1229 Color: 3
Size: 538 Color: 15
Size: 512 Color: 13
Size: 96 Color: 2
Size: 80 Color: 5

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 9
Size: 921 Color: 6
Size: 108 Color: 2

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1492 Color: 4
Size: 923 Color: 9
Size: 40 Color: 2

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 1531 Color: 18
Size: 924 Color: 7

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 1580 Color: 17
Size: 671 Color: 15
Size: 204 Color: 8

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 1651 Color: 16
Size: 804 Color: 18

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 9
Size: 732 Color: 6
Size: 64 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 12
Size: 553 Color: 9
Size: 144 Color: 14

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 13
Size: 1018 Color: 3
Size: 200 Color: 8

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 18
Size: 568 Color: 9
Size: 204 Color: 17

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 11
Size: 516 Color: 2
Size: 40 Color: 13

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1793 Color: 5
Size: 660 Color: 13

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 9
Size: 827 Color: 18
Size: 160 Color: 2

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 2
Size: 665 Color: 3
Size: 232 Color: 1

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 9
Size: 386 Color: 13
Size: 134 Color: 15

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 2166 Color: 17
Size: 286 Color: 8

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1351 Color: 15
Size: 1020 Color: 3
Size: 80 Color: 6

Bin 46: 6 of cap free
Amount of items: 4
Items: 
Size: 1349 Color: 19
Size: 1021 Color: 16
Size: 48 Color: 13
Size: 32 Color: 8

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 1812 Color: 16
Size: 638 Color: 19

Bin 48: 6 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 11
Size: 246 Color: 18
Size: 16 Color: 13

Bin 49: 8 of cap free
Amount of items: 2
Items: 
Size: 2028 Color: 3
Size: 420 Color: 11

Bin 50: 8 of cap free
Amount of items: 2
Items: 
Size: 2108 Color: 4
Size: 340 Color: 11

Bin 51: 9 of cap free
Amount of items: 4
Items: 
Size: 1231 Color: 4
Size: 980 Color: 10
Size: 148 Color: 5
Size: 88 Color: 11

Bin 52: 9 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 13
Size: 751 Color: 11

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 1694 Color: 1
Size: 750 Color: 13

Bin 54: 12 of cap free
Amount of items: 2
Items: 
Size: 1798 Color: 0
Size: 646 Color: 5

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 2162 Color: 13
Size: 282 Color: 12

Bin 56: 14 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 9
Size: 1022 Color: 12
Size: 56 Color: 9

Bin 57: 15 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 3
Size: 841 Color: 11
Size: 64 Color: 3

Bin 58: 20 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 2
Size: 862 Color: 0

Bin 59: 23 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 11
Size: 771 Color: 0
Size: 384 Color: 13
Size: 48 Color: 1

Bin 60: 23 of cap free
Amount of items: 2
Items: 
Size: 1410 Color: 14
Size: 1023 Color: 16

Bin 61: 24 of cap free
Amount of items: 2
Items: 
Size: 1558 Color: 6
Size: 874 Color: 18

Bin 62: 24 of cap free
Amount of items: 2
Items: 
Size: 2098 Color: 2
Size: 334 Color: 11

Bin 63: 26 of cap free
Amount of items: 15
Items: 
Size: 276 Color: 5
Size: 272 Color: 6
Size: 212 Color: 18
Size: 204 Color: 18
Size: 184 Color: 6
Size: 184 Color: 3
Size: 168 Color: 7
Size: 150 Color: 11
Size: 144 Color: 3
Size: 128 Color: 19
Size: 128 Color: 13
Size: 124 Color: 19
Size: 104 Color: 10
Size: 104 Color: 5
Size: 48 Color: 8

Bin 64: 26 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 0
Size: 738 Color: 8
Size: 454 Color: 9

Bin 65: 31 of cap free
Amount of items: 2
Items: 
Size: 1449 Color: 10
Size: 976 Color: 18

Bin 66: 2104 of cap free
Amount of items: 6
Items: 
Size: 88 Color: 0
Size: 80 Color: 6
Size: 48 Color: 16
Size: 48 Color: 11
Size: 48 Color: 9
Size: 40 Color: 12

Total size: 159640
Total free space: 2456

