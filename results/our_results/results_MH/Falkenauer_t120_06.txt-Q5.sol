Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 269 Color: 0
Size: 263 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 292 Color: 1
Size: 257 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 2
Size: 339 Color: 0
Size: 301 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 325 Color: 3
Size: 284 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 359 Color: 2
Size: 266 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 4
Size: 289 Color: 0
Size: 258 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 311 Color: 3
Size: 299 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 278 Color: 2
Size: 253 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 2
Size: 299 Color: 4
Size: 276 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 0
Size: 323 Color: 1
Size: 297 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 370 Color: 3
Size: 252 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 4
Size: 289 Color: 1
Size: 254 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 311 Color: 1
Size: 292 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 257 Color: 3
Size: 250 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 258 Color: 2
Size: 251 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 326 Color: 4
Size: 300 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 303 Color: 3
Size: 327 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 2
Size: 303 Color: 1
Size: 295 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 297 Color: 4
Size: 255 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 2
Size: 257 Color: 3
Size: 252 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 321 Color: 4
Size: 304 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 275 Color: 0
Size: 254 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 364 Color: 3
Size: 265 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 350 Color: 4
Size: 273 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 299 Color: 4
Size: 260 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 277 Color: 1
Size: 262 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 285 Color: 0
Size: 260 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 2
Size: 357 Color: 0
Size: 286 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 3
Size: 250 Color: 4
Size: 285 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 330 Color: 4
Size: 290 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 323 Color: 1
Size: 257 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 2
Size: 319 Color: 4
Size: 254 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 2
Size: 318 Color: 1
Size: 253 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 358 Color: 2
Size: 269 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 281 Color: 0
Size: 260 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 325 Color: 4
Size: 297 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 336 Color: 2
Size: 260 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 3
Size: 359 Color: 1
Size: 281 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 320 Color: 2
Size: 252 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 363 Color: 3
Size: 271 Color: 1

Total size: 40000
Total free space: 0

