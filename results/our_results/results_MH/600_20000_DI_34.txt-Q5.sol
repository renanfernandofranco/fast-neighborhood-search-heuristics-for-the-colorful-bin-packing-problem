Capicity Bin: 16544
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 8277 Color: 3
Size: 1596 Color: 4
Size: 1524 Color: 4
Size: 1522 Color: 2
Size: 1445 Color: 0
Size: 1188 Color: 1
Size: 992 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9968 Color: 2
Size: 6128 Color: 0
Size: 448 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11120 Color: 4
Size: 4344 Color: 0
Size: 1080 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11148 Color: 1
Size: 4500 Color: 3
Size: 896 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11265 Color: 0
Size: 4401 Color: 2
Size: 878 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11892 Color: 2
Size: 3884 Color: 2
Size: 768 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11975 Color: 3
Size: 3417 Color: 2
Size: 1152 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 2
Size: 4164 Color: 3
Size: 276 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12118 Color: 3
Size: 3690 Color: 2
Size: 736 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 2
Size: 3704 Color: 4
Size: 280 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12590 Color: 0
Size: 3272 Color: 0
Size: 682 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 1
Size: 3298 Color: 2
Size: 630 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12756 Color: 2
Size: 3164 Color: 3
Size: 624 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12757 Color: 2
Size: 2675 Color: 1
Size: 1112 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 3
Size: 3344 Color: 2
Size: 456 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 3
Size: 2748 Color: 3
Size: 1020 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12889 Color: 3
Size: 2759 Color: 2
Size: 896 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 4
Size: 3044 Color: 0
Size: 584 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13026 Color: 0
Size: 2758 Color: 4
Size: 760 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13252 Color: 1
Size: 2684 Color: 4
Size: 608 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 0
Size: 2868 Color: 4
Size: 304 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13399 Color: 3
Size: 2621 Color: 3
Size: 524 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13520 Color: 2
Size: 2512 Color: 4
Size: 512 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 3
Size: 2544 Color: 2
Size: 488 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13552 Color: 2
Size: 1864 Color: 4
Size: 1128 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13804 Color: 3
Size: 2260 Color: 4
Size: 480 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13874 Color: 4
Size: 2238 Color: 3
Size: 432 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13911 Color: 3
Size: 1417 Color: 4
Size: 1216 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14042 Color: 4
Size: 1856 Color: 0
Size: 646 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14064 Color: 0
Size: 1952 Color: 1
Size: 528 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14150 Color: 0
Size: 1856 Color: 3
Size: 538 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14166 Color: 2
Size: 1940 Color: 0
Size: 438 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 1
Size: 1798 Color: 2
Size: 550 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14276 Color: 3
Size: 1788 Color: 4
Size: 480 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14264 Color: 4
Size: 1912 Color: 0
Size: 368 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14310 Color: 0
Size: 1818 Color: 0
Size: 416 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 1
Size: 1512 Color: 3
Size: 704 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14363 Color: 0
Size: 1819 Color: 4
Size: 362 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14392 Color: 2
Size: 1192 Color: 3
Size: 960 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14464 Color: 0
Size: 1344 Color: 0
Size: 736 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14480 Color: 0
Size: 1968 Color: 2
Size: 96 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 0
Size: 1476 Color: 1
Size: 534 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14566 Color: 2
Size: 1088 Color: 3
Size: 890 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14608 Color: 4
Size: 1862 Color: 0
Size: 74 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 3
Size: 1456 Color: 0
Size: 404 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14716 Color: 3
Size: 1500 Color: 0
Size: 328 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14744 Color: 2
Size: 1496 Color: 1
Size: 304 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14780 Color: 2
Size: 1452 Color: 3
Size: 312 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14782 Color: 2
Size: 1080 Color: 1
Size: 682 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14722 Color: 0
Size: 1470 Color: 3
Size: 352 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14800 Color: 0
Size: 1072 Color: 2
Size: 672 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14878 Color: 1
Size: 1026 Color: 4
Size: 640 Color: 3

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 11550 Color: 3
Size: 4457 Color: 0
Size: 536 Color: 3

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 2
Size: 2389 Color: 1
Size: 1650 Color: 4

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 12445 Color: 1
Size: 3702 Color: 4
Size: 396 Color: 4

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 12657 Color: 2
Size: 2642 Color: 3
Size: 1244 Color: 3

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 13191 Color: 2
Size: 3028 Color: 0
Size: 324 Color: 3

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 13235 Color: 4
Size: 1964 Color: 2
Size: 1344 Color: 1

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 13335 Color: 0
Size: 2878 Color: 0
Size: 330 Color: 2

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 13679 Color: 4
Size: 1982 Color: 2
Size: 882 Color: 0

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13904 Color: 1
Size: 2191 Color: 2
Size: 448 Color: 3

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13943 Color: 4
Size: 1360 Color: 3
Size: 1240 Color: 1

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 14109 Color: 1
Size: 1970 Color: 3
Size: 464 Color: 4

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 14319 Color: 2
Size: 2224 Color: 4

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 14557 Color: 0
Size: 1616 Color: 3
Size: 370 Color: 1

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 14543 Color: 4
Size: 1376 Color: 0
Size: 624 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 14845 Color: 3
Size: 1378 Color: 2
Size: 320 Color: 0

Bin 68: 2 of cap free
Amount of items: 5
Items: 
Size: 8296 Color: 3
Size: 4244 Color: 4
Size: 1556 Color: 1
Size: 1424 Color: 4
Size: 1022 Color: 1

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 9078 Color: 2
Size: 6888 Color: 0
Size: 576 Color: 2

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 9356 Color: 3
Size: 6894 Color: 4
Size: 292 Color: 4

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 3
Size: 5410 Color: 1
Size: 360 Color: 0

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 10862 Color: 0
Size: 5080 Color: 3
Size: 600 Color: 2

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 11241 Color: 1
Size: 4461 Color: 0
Size: 840 Color: 4

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 11336 Color: 0
Size: 4648 Color: 4
Size: 558 Color: 3

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 11424 Color: 4
Size: 4742 Color: 0
Size: 376 Color: 2

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 11556 Color: 0
Size: 4730 Color: 4
Size: 256 Color: 1

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 11728 Color: 0
Size: 4162 Color: 3
Size: 652 Color: 2

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11976 Color: 0
Size: 4174 Color: 3
Size: 392 Color: 3

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 0
Size: 3176 Color: 2
Size: 302 Color: 4

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 4
Size: 2838 Color: 4
Size: 472 Color: 2

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 13311 Color: 4
Size: 1855 Color: 2
Size: 1376 Color: 4

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 13662 Color: 2
Size: 2592 Color: 3
Size: 288 Color: 4

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 13826 Color: 2
Size: 2402 Color: 3
Size: 314 Color: 4

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 14182 Color: 1
Size: 1848 Color: 0
Size: 512 Color: 3

Bin 85: 3 of cap free
Amount of items: 13
Items: 
Size: 8273 Color: 4
Size: 944 Color: 0
Size: 864 Color: 4
Size: 832 Color: 4
Size: 800 Color: 2
Size: 800 Color: 0
Size: 724 Color: 1
Size: 656 Color: 3
Size: 656 Color: 1
Size: 608 Color: 1
Size: 592 Color: 3
Size: 396 Color: 4
Size: 396 Color: 3

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 10050 Color: 4
Size: 5947 Color: 3
Size: 544 Color: 2

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 10417 Color: 1
Size: 5676 Color: 0
Size: 448 Color: 1

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 12080 Color: 4
Size: 3809 Color: 2
Size: 652 Color: 4

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 12316 Color: 3
Size: 2644 Color: 4
Size: 1581 Color: 0

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 12752 Color: 4
Size: 3789 Color: 1

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 14649 Color: 0
Size: 1892 Color: 3

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 9188 Color: 0
Size: 6856 Color: 3
Size: 496 Color: 1

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 10204 Color: 0
Size: 4976 Color: 3
Size: 1360 Color: 4

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 10740 Color: 3
Size: 5488 Color: 0
Size: 312 Color: 4

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 12172 Color: 0
Size: 4072 Color: 2
Size: 296 Color: 3

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 13332 Color: 0
Size: 3208 Color: 3

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 14104 Color: 0
Size: 2436 Color: 1

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 12106 Color: 1
Size: 4433 Color: 0

Bin 99: 5 of cap free
Amount of items: 3
Items: 
Size: 12449 Color: 4
Size: 3808 Color: 3
Size: 282 Color: 2

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 10552 Color: 3
Size: 5506 Color: 0
Size: 480 Color: 3

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 11193 Color: 4
Size: 5121 Color: 1
Size: 224 Color: 0

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 11538 Color: 1
Size: 5000 Color: 2

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 12578 Color: 2
Size: 3704 Color: 4
Size: 256 Color: 1

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 13784 Color: 1
Size: 2454 Color: 0
Size: 300 Color: 3

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 2
Size: 2632 Color: 4

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 14698 Color: 4
Size: 1840 Color: 2

Bin 107: 7 of cap free
Amount of items: 3
Items: 
Size: 9409 Color: 2
Size: 6588 Color: 3
Size: 540 Color: 0

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 11285 Color: 2
Size: 3254 Color: 2
Size: 1998 Color: 0

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 12610 Color: 0
Size: 3927 Color: 1

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 14804 Color: 1
Size: 1669 Color: 4
Size: 64 Color: 1

Bin 111: 8 of cap free
Amount of items: 3
Items: 
Size: 11225 Color: 1
Size: 4421 Color: 4
Size: 890 Color: 0

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 12892 Color: 3
Size: 3644 Color: 1

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13602 Color: 3
Size: 2934 Color: 4

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 0
Size: 2492 Color: 4

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 14240 Color: 1
Size: 2296 Color: 0

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 14864 Color: 4
Size: 1672 Color: 1

Bin 117: 9 of cap free
Amount of items: 3
Items: 
Size: 9407 Color: 0
Size: 6808 Color: 3
Size: 320 Color: 2

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 13294 Color: 0
Size: 3241 Color: 1

Bin 119: 10 of cap free
Amount of items: 6
Items: 
Size: 8278 Color: 2
Size: 2226 Color: 2
Size: 1808 Color: 0
Size: 1678 Color: 0
Size: 1376 Color: 1
Size: 1168 Color: 1

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 9086 Color: 3
Size: 6824 Color: 1
Size: 624 Color: 2

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 9740 Color: 1
Size: 6218 Color: 0
Size: 576 Color: 3

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 10456 Color: 0
Size: 5414 Color: 3
Size: 664 Color: 3

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 14163 Color: 2
Size: 2371 Color: 1

Bin 124: 11 of cap free
Amount of items: 4
Items: 
Size: 8284 Color: 1
Size: 4467 Color: 4
Size: 3270 Color: 4
Size: 512 Color: 2

Bin 125: 12 of cap free
Amount of items: 3
Items: 
Size: 9512 Color: 0
Size: 6604 Color: 4
Size: 416 Color: 2

Bin 126: 13 of cap free
Amount of items: 8
Items: 
Size: 8276 Color: 0
Size: 1519 Color: 4
Size: 1390 Color: 3
Size: 1376 Color: 2
Size: 1360 Color: 0
Size: 1032 Color: 3
Size: 1030 Color: 1
Size: 548 Color: 0

Bin 127: 13 of cap free
Amount of items: 3
Items: 
Size: 9196 Color: 1
Size: 6891 Color: 3
Size: 444 Color: 0

Bin 128: 13 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 0
Size: 2695 Color: 4

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 13915 Color: 1
Size: 2616 Color: 2

Bin 130: 14 of cap free
Amount of items: 3
Items: 
Size: 8392 Color: 2
Size: 7194 Color: 0
Size: 944 Color: 2

Bin 131: 14 of cap free
Amount of items: 3
Items: 
Size: 11197 Color: 3
Size: 5133 Color: 1
Size: 200 Color: 2

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 14012 Color: 0
Size: 2518 Color: 1

Bin 133: 15 of cap free
Amount of items: 4
Items: 
Size: 14748 Color: 4
Size: 1657 Color: 2
Size: 96 Color: 3
Size: 28 Color: 1

Bin 134: 16 of cap free
Amount of items: 3
Items: 
Size: 10351 Color: 2
Size: 5949 Color: 3
Size: 228 Color: 2

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 4
Size: 3816 Color: 3

Bin 136: 16 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 1
Size: 2078 Color: 0
Size: 60 Color: 1

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 14723 Color: 4
Size: 1805 Color: 1

Bin 138: 17 of cap free
Amount of items: 3
Items: 
Size: 10357 Color: 4
Size: 5284 Color: 0
Size: 886 Color: 3

Bin 139: 17 of cap free
Amount of items: 2
Items: 
Size: 14552 Color: 2
Size: 1975 Color: 1

Bin 140: 18 of cap free
Amount of items: 3
Items: 
Size: 9388 Color: 0
Size: 5107 Color: 1
Size: 2031 Color: 2

Bin 141: 18 of cap free
Amount of items: 2
Items: 
Size: 13342 Color: 3
Size: 3184 Color: 4

Bin 142: 19 of cap free
Amount of items: 2
Items: 
Size: 14540 Color: 3
Size: 1985 Color: 2

Bin 143: 20 of cap free
Amount of items: 3
Items: 
Size: 10054 Color: 4
Size: 6222 Color: 1
Size: 248 Color: 0

Bin 144: 20 of cap free
Amount of items: 3
Items: 
Size: 12912 Color: 0
Size: 3324 Color: 1
Size: 288 Color: 2

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 14192 Color: 1
Size: 2332 Color: 0

Bin 146: 23 of cap free
Amount of items: 3
Items: 
Size: 10401 Color: 4
Size: 5864 Color: 2
Size: 256 Color: 0

Bin 147: 23 of cap free
Amount of items: 2
Items: 
Size: 14352 Color: 2
Size: 2169 Color: 4

Bin 148: 24 of cap free
Amount of items: 31
Items: 
Size: 784 Color: 4
Size: 782 Color: 4
Size: 752 Color: 4
Size: 752 Color: 0
Size: 736 Color: 2
Size: 736 Color: 2
Size: 728 Color: 2
Size: 648 Color: 2
Size: 626 Color: 0
Size: 608 Color: 2
Size: 608 Color: 2
Size: 568 Color: 3
Size: 564 Color: 4
Size: 544 Color: 1
Size: 528 Color: 2
Size: 528 Color: 1
Size: 496 Color: 0
Size: 476 Color: 3
Size: 438 Color: 1
Size: 416 Color: 3
Size: 416 Color: 1
Size: 402 Color: 3
Size: 400 Color: 3
Size: 400 Color: 2
Size: 384 Color: 4
Size: 384 Color: 3
Size: 384 Color: 3
Size: 384 Color: 0
Size: 368 Color: 0
Size: 352 Color: 1
Size: 328 Color: 1

Bin 149: 24 of cap free
Amount of items: 3
Items: 
Size: 12626 Color: 3
Size: 3718 Color: 0
Size: 176 Color: 3

Bin 150: 24 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 0
Size: 3282 Color: 4

Bin 151: 24 of cap free
Amount of items: 2
Items: 
Size: 14424 Color: 1
Size: 2096 Color: 4

Bin 152: 24 of cap free
Amount of items: 2
Items: 
Size: 14760 Color: 3
Size: 1760 Color: 1

Bin 153: 25 of cap free
Amount of items: 4
Items: 
Size: 8280 Color: 4
Size: 3266 Color: 2
Size: 3157 Color: 2
Size: 1816 Color: 0

Bin 154: 25 of cap free
Amount of items: 3
Items: 
Size: 14811 Color: 3
Size: 1676 Color: 4
Size: 32 Color: 2

Bin 155: 28 of cap free
Amount of items: 2
Items: 
Size: 13748 Color: 3
Size: 2768 Color: 0

Bin 156: 30 of cap free
Amount of items: 2
Items: 
Size: 11353 Color: 2
Size: 5161 Color: 4

Bin 157: 34 of cap free
Amount of items: 2
Items: 
Size: 13142 Color: 1
Size: 3368 Color: 0

Bin 158: 34 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 1
Size: 2710 Color: 0

Bin 159: 35 of cap free
Amount of items: 2
Items: 
Size: 10385 Color: 3
Size: 6124 Color: 2

Bin 160: 38 of cap free
Amount of items: 2
Items: 
Size: 14495 Color: 2
Size: 2011 Color: 1

Bin 161: 40 of cap free
Amount of items: 2
Items: 
Size: 14220 Color: 4
Size: 2284 Color: 0

Bin 162: 40 of cap free
Amount of items: 2
Items: 
Size: 14636 Color: 1
Size: 1868 Color: 2

Bin 163: 41 of cap free
Amount of items: 2
Items: 
Size: 12556 Color: 3
Size: 3947 Color: 1

Bin 164: 41 of cap free
Amount of items: 2
Items: 
Size: 14308 Color: 2
Size: 2195 Color: 0

Bin 165: 48 of cap free
Amount of items: 2
Items: 
Size: 13701 Color: 4
Size: 2795 Color: 0

Bin 166: 52 of cap free
Amount of items: 2
Items: 
Size: 9608 Color: 1
Size: 6884 Color: 2

Bin 167: 52 of cap free
Amount of items: 2
Items: 
Size: 9868 Color: 4
Size: 6624 Color: 1

Bin 168: 52 of cap free
Amount of items: 2
Items: 
Size: 13436 Color: 3
Size: 3056 Color: 0

Bin 169: 52 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 2
Size: 2086 Color: 4

Bin 170: 56 of cap free
Amount of items: 3
Items: 
Size: 8328 Color: 4
Size: 5564 Color: 2
Size: 2596 Color: 4

Bin 171: 56 of cap free
Amount of items: 2
Items: 
Size: 14404 Color: 0
Size: 2084 Color: 1

Bin 172: 60 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 1
Size: 4812 Color: 2

Bin 173: 68 of cap free
Amount of items: 2
Items: 
Size: 11971 Color: 1
Size: 4505 Color: 3

Bin 174: 74 of cap free
Amount of items: 2
Items: 
Size: 10870 Color: 4
Size: 5600 Color: 1

Bin 175: 75 of cap free
Amount of items: 2
Items: 
Size: 12453 Color: 0
Size: 4016 Color: 1

Bin 176: 84 of cap free
Amount of items: 2
Items: 
Size: 12936 Color: 0
Size: 3524 Color: 3

Bin 177: 84 of cap free
Amount of items: 2
Items: 
Size: 13556 Color: 1
Size: 2904 Color: 3

Bin 178: 85 of cap free
Amount of items: 2
Items: 
Size: 13046 Color: 0
Size: 3413 Color: 1

Bin 179: 96 of cap free
Amount of items: 2
Items: 
Size: 13400 Color: 0
Size: 3048 Color: 1

Bin 180: 99 of cap free
Amount of items: 3
Items: 
Size: 8360 Color: 2
Size: 6893 Color: 0
Size: 1192 Color: 4

Bin 181: 101 of cap free
Amount of items: 2
Items: 
Size: 14131 Color: 1
Size: 2312 Color: 4

Bin 182: 123 of cap free
Amount of items: 2
Items: 
Size: 13374 Color: 3
Size: 3047 Color: 0

Bin 183: 125 of cap free
Amount of items: 2
Items: 
Size: 14379 Color: 3
Size: 2040 Color: 2

Bin 184: 130 of cap free
Amount of items: 2
Items: 
Size: 11257 Color: 3
Size: 5157 Color: 4

Bin 185: 137 of cap free
Amount of items: 2
Items: 
Size: 11460 Color: 3
Size: 4947 Color: 2

Bin 186: 143 of cap free
Amount of items: 2
Items: 
Size: 11240 Color: 1
Size: 5161 Color: 3

Bin 187: 152 of cap free
Amount of items: 9
Items: 
Size: 8274 Color: 2
Size: 1224 Color: 0
Size: 1184 Color: 0
Size: 1056 Color: 4
Size: 1030 Color: 3
Size: 1008 Color: 3
Size: 960 Color: 4
Size: 832 Color: 1
Size: 824 Color: 1

Bin 188: 152 of cap free
Amount of items: 2
Items: 
Size: 10608 Color: 2
Size: 5784 Color: 3

Bin 189: 152 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 1
Size: 3488 Color: 0

Bin 190: 160 of cap free
Amount of items: 3
Items: 
Size: 8304 Color: 0
Size: 6892 Color: 0
Size: 1188 Color: 3

Bin 191: 162 of cap free
Amount of items: 2
Items: 
Size: 10858 Color: 2
Size: 5524 Color: 1

Bin 192: 164 of cap free
Amount of items: 2
Items: 
Size: 13108 Color: 1
Size: 3272 Color: 4

Bin 193: 174 of cap free
Amount of items: 2
Items: 
Size: 12642 Color: 3
Size: 3728 Color: 4

Bin 194: 183 of cap free
Amount of items: 2
Items: 
Size: 11833 Color: 4
Size: 4528 Color: 1

Bin 195: 195 of cap free
Amount of items: 2
Items: 
Size: 10353 Color: 4
Size: 5996 Color: 2

Bin 196: 212 of cap free
Amount of items: 2
Items: 
Size: 10368 Color: 2
Size: 5964 Color: 1

Bin 197: 228 of cap free
Amount of items: 2
Items: 
Size: 9420 Color: 3
Size: 6896 Color: 1

Bin 198: 264 of cap free
Amount of items: 2
Items: 
Size: 9200 Color: 2
Size: 7080 Color: 3

Bin 199: 11282 of cap free
Amount of items: 16
Items: 
Size: 384 Color: 3
Size: 372 Color: 4
Size: 368 Color: 2
Size: 356 Color: 2
Size: 352 Color: 0
Size: 336 Color: 4
Size: 336 Color: 3
Size: 332 Color: 4
Size: 332 Color: 0
Size: 320 Color: 1
Size: 320 Color: 1
Size: 302 Color: 0
Size: 288 Color: 3
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0

Total size: 3275712
Total free space: 16544

