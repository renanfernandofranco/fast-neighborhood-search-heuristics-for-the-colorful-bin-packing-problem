Capicity Bin: 7824
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 31
Items: 
Size: 368 Color: 1
Size: 360 Color: 1
Size: 352 Color: 1
Size: 352 Color: 1
Size: 336 Color: 1
Size: 328 Color: 1
Size: 316 Color: 1
Size: 304 Color: 1
Size: 280 Color: 0
Size: 272 Color: 1
Size: 272 Color: 0
Size: 256 Color: 1
Size: 256 Color: 0
Size: 256 Color: 0
Size: 248 Color: 1
Size: 232 Color: 1
Size: 232 Color: 1
Size: 228 Color: 0
Size: 224 Color: 0
Size: 224 Color: 0
Size: 216 Color: 0
Size: 216 Color: 0
Size: 216 Color: 0
Size: 208 Color: 0
Size: 192 Color: 0
Size: 192 Color: 0
Size: 192 Color: 0
Size: 188 Color: 1
Size: 184 Color: 0
Size: 164 Color: 1
Size: 160 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4344 Color: 1
Size: 3258 Color: 0
Size: 222 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4696 Color: 1
Size: 2616 Color: 0
Size: 512 Color: 1

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 4928 Color: 1
Size: 2896 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 1
Size: 2168 Color: 0
Size: 144 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5672 Color: 0
Size: 1922 Color: 1
Size: 230 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5912 Color: 0
Size: 1528 Color: 1
Size: 384 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 0
Size: 712 Color: 1
Size: 700 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 1
Size: 970 Color: 1
Size: 444 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6477 Color: 0
Size: 1157 Color: 1
Size: 190 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6600 Color: 0
Size: 724 Color: 1
Size: 500 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6671 Color: 0
Size: 961 Color: 1
Size: 192 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 0
Size: 648 Color: 1
Size: 432 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6720 Color: 1
Size: 968 Color: 0
Size: 136 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 0
Size: 880 Color: 1
Size: 194 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 1
Size: 650 Color: 1
Size: 352 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6862 Color: 1
Size: 732 Color: 0
Size: 230 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 0
Size: 774 Color: 1
Size: 152 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6918 Color: 0
Size: 698 Color: 1
Size: 208 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 0
Size: 552 Color: 1
Size: 304 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 0
Size: 542 Color: 0
Size: 292 Color: 1

Bin 22: 1 of cap free
Amount of items: 9
Items: 
Size: 3917 Color: 1
Size: 648 Color: 1
Size: 640 Color: 1
Size: 596 Color: 1
Size: 576 Color: 0
Size: 552 Color: 0
Size: 464 Color: 0
Size: 272 Color: 1
Size: 158 Color: 0

Bin 23: 1 of cap free
Amount of items: 7
Items: 
Size: 3928 Color: 1
Size: 1042 Color: 0
Size: 927 Color: 1
Size: 912 Color: 1
Size: 710 Color: 0
Size: 160 Color: 1
Size: 144 Color: 0

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 4822 Color: 1
Size: 2341 Color: 0
Size: 660 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 5652 Color: 0
Size: 1887 Color: 0
Size: 284 Color: 1

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 5846 Color: 0
Size: 1977 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 1
Size: 1691 Color: 1
Size: 232 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 1
Size: 1084 Color: 0
Size: 730 Color: 1

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 6072 Color: 1
Size: 1751 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 1
Size: 1161 Color: 1
Size: 200 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6481 Color: 1
Size: 934 Color: 1
Size: 408 Color: 0

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 5210 Color: 1
Size: 2428 Color: 0
Size: 184 Color: 1

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 5522 Color: 0
Size: 2060 Color: 1
Size: 240 Color: 0

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 5782 Color: 1
Size: 1800 Color: 0
Size: 240 Color: 0

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 6081 Color: 1
Size: 1453 Color: 0
Size: 288 Color: 1

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 6662 Color: 1
Size: 1160 Color: 0

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 6736 Color: 0
Size: 1086 Color: 1

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 6865 Color: 0
Size: 957 Color: 1

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 6978 Color: 1
Size: 844 Color: 0

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 6984 Color: 1
Size: 838 Color: 0

Bin 41: 3 of cap free
Amount of items: 11
Items: 
Size: 3913 Color: 0
Size: 544 Color: 1
Size: 488 Color: 1
Size: 472 Color: 1
Size: 466 Color: 1
Size: 464 Color: 0
Size: 432 Color: 0
Size: 394 Color: 0
Size: 320 Color: 0
Size: 192 Color: 0
Size: 136 Color: 1

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 4237 Color: 0
Size: 3244 Color: 1
Size: 340 Color: 0

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 6437 Color: 0
Size: 892 Color: 1
Size: 492 Color: 0

Bin 44: 3 of cap free
Amount of items: 2
Items: 
Size: 6706 Color: 1
Size: 1115 Color: 0

Bin 45: 3 of cap free
Amount of items: 2
Items: 
Size: 6901 Color: 1
Size: 920 Color: 0

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 4567 Color: 1
Size: 3253 Color: 0

Bin 47: 4 of cap free
Amount of items: 2
Items: 
Size: 4916 Color: 1
Size: 2904 Color: 0

Bin 48: 4 of cap free
Amount of items: 4
Items: 
Size: 5392 Color: 0
Size: 1400 Color: 1
Size: 948 Color: 1
Size: 80 Color: 0

Bin 49: 4 of cap free
Amount of items: 2
Items: 
Size: 6247 Color: 0
Size: 1573 Color: 1

Bin 50: 5 of cap free
Amount of items: 2
Items: 
Size: 6054 Color: 1
Size: 1765 Color: 0

Bin 51: 5 of cap free
Amount of items: 2
Items: 
Size: 6141 Color: 0
Size: 1678 Color: 1

Bin 52: 5 of cap free
Amount of items: 2
Items: 
Size: 6508 Color: 1
Size: 1311 Color: 0

Bin 53: 5 of cap free
Amount of items: 2
Items: 
Size: 6618 Color: 1
Size: 1201 Color: 0

Bin 54: 5 of cap free
Amount of items: 2
Items: 
Size: 6698 Color: 0
Size: 1121 Color: 1

Bin 55: 5 of cap free
Amount of items: 4
Items: 
Size: 6950 Color: 0
Size: 801 Color: 1
Size: 64 Color: 0
Size: 4 Color: 1

Bin 56: 6 of cap free
Amount of items: 13
Items: 
Size: 3914 Color: 1
Size: 436 Color: 1
Size: 400 Color: 1
Size: 384 Color: 1
Size: 376 Color: 1
Size: 372 Color: 1
Size: 312 Color: 0
Size: 296 Color: 0
Size: 290 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 286 Color: 0
Size: 176 Color: 1

Bin 57: 6 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 1
Size: 1452 Color: 0
Size: 332 Color: 0

Bin 58: 6 of cap free
Amount of items: 2
Items: 
Size: 6524 Color: 1
Size: 1294 Color: 0

Bin 59: 6 of cap free
Amount of items: 2
Items: 
Size: 6680 Color: 1
Size: 1138 Color: 0

Bin 60: 6 of cap free
Amount of items: 2
Items: 
Size: 6812 Color: 0
Size: 1006 Color: 1

Bin 61: 7 of cap free
Amount of items: 2
Items: 
Size: 3918 Color: 1
Size: 3899 Color: 0

Bin 62: 7 of cap free
Amount of items: 4
Items: 
Size: 3932 Color: 0
Size: 2991 Color: 1
Size: 742 Color: 0
Size: 152 Color: 1

Bin 63: 7 of cap free
Amount of items: 3
Items: 
Size: 3948 Color: 0
Size: 3701 Color: 1
Size: 168 Color: 1

Bin 64: 7 of cap free
Amount of items: 2
Items: 
Size: 5797 Color: 1
Size: 2020 Color: 0

Bin 65: 8 of cap free
Amount of items: 7
Items: 
Size: 3916 Color: 0
Size: 728 Color: 1
Size: 728 Color: 1
Size: 648 Color: 0
Size: 648 Color: 0
Size: 640 Color: 0
Size: 508 Color: 1

Bin 66: 8 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 0
Size: 2376 Color: 0
Size: 238 Color: 1

Bin 67: 8 of cap free
Amount of items: 2
Items: 
Size: 5992 Color: 1
Size: 1824 Color: 0

Bin 68: 8 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 1
Size: 1604 Color: 0
Size: 128 Color: 0

Bin 69: 8 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 0
Size: 1542 Color: 1

Bin 70: 8 of cap free
Amount of items: 2
Items: 
Size: 6574 Color: 1
Size: 1242 Color: 0

Bin 71: 8 of cap free
Amount of items: 2
Items: 
Size: 6652 Color: 1
Size: 1164 Color: 0

Bin 72: 8 of cap free
Amount of items: 2
Items: 
Size: 6872 Color: 1
Size: 944 Color: 0

Bin 73: 8 of cap free
Amount of items: 3
Items: 
Size: 7036 Color: 1
Size: 764 Color: 0
Size: 16 Color: 1

Bin 74: 9 of cap free
Amount of items: 3
Items: 
Size: 3929 Color: 1
Size: 3236 Color: 0
Size: 650 Color: 1

Bin 75: 9 of cap free
Amount of items: 2
Items: 
Size: 6385 Color: 1
Size: 1430 Color: 0

Bin 76: 10 of cap free
Amount of items: 3
Items: 
Size: 4524 Color: 0
Size: 3058 Color: 1
Size: 232 Color: 0

Bin 77: 10 of cap free
Amount of items: 2
Items: 
Size: 5356 Color: 1
Size: 2458 Color: 0

Bin 78: 10 of cap free
Amount of items: 2
Items: 
Size: 5700 Color: 1
Size: 2114 Color: 0

Bin 79: 10 of cap free
Amount of items: 2
Items: 
Size: 6934 Color: 1
Size: 880 Color: 0

Bin 80: 11 of cap free
Amount of items: 2
Items: 
Size: 5017 Color: 1
Size: 2796 Color: 0

Bin 81: 11 of cap free
Amount of items: 2
Items: 
Size: 6713 Color: 0
Size: 1100 Color: 1

Bin 82: 14 of cap free
Amount of items: 2
Items: 
Size: 6348 Color: 0
Size: 1462 Color: 1

Bin 83: 14 of cap free
Amount of items: 2
Items: 
Size: 6440 Color: 1
Size: 1370 Color: 0

Bin 84: 14 of cap free
Amount of items: 2
Items: 
Size: 6868 Color: 1
Size: 942 Color: 0

Bin 85: 15 of cap free
Amount of items: 5
Items: 
Size: 3921 Color: 0
Size: 1308 Color: 1
Size: 1196 Color: 1
Size: 1060 Color: 0
Size: 324 Color: 0

Bin 86: 15 of cap free
Amount of items: 2
Items: 
Size: 5453 Color: 0
Size: 2356 Color: 1

Bin 87: 18 of cap free
Amount of items: 4
Items: 
Size: 5367 Color: 0
Size: 1572 Color: 1
Size: 771 Color: 1
Size: 96 Color: 0

Bin 88: 18 of cap free
Amount of items: 2
Items: 
Size: 6312 Color: 1
Size: 1494 Color: 0

Bin 89: 18 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 1
Size: 898 Color: 0

Bin 90: 20 of cap free
Amount of items: 2
Items: 
Size: 6188 Color: 0
Size: 1616 Color: 1

Bin 91: 20 of cap free
Amount of items: 2
Items: 
Size: 6588 Color: 0
Size: 1216 Color: 1

Bin 92: 20 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 1
Size: 808 Color: 0
Size: 8 Color: 1

Bin 93: 22 of cap free
Amount of items: 2
Items: 
Size: 6152 Color: 1
Size: 1650 Color: 0

Bin 94: 24 of cap free
Amount of items: 2
Items: 
Size: 5404 Color: 0
Size: 2396 Color: 1

Bin 95: 24 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 1
Size: 804 Color: 0
Size: 40 Color: 1

Bin 96: 25 of cap free
Amount of items: 2
Items: 
Size: 6396 Color: 0
Size: 1403 Color: 1

Bin 97: 28 of cap free
Amount of items: 3
Items: 
Size: 4775 Color: 0
Size: 2541 Color: 1
Size: 480 Color: 0

Bin 98: 30 of cap free
Amount of items: 2
Items: 
Size: 6522 Color: 0
Size: 1272 Color: 1

Bin 99: 32 of cap free
Amount of items: 2
Items: 
Size: 5707 Color: 0
Size: 2085 Color: 1

Bin 100: 32 of cap free
Amount of items: 2
Items: 
Size: 6428 Color: 1
Size: 1364 Color: 0

Bin 101: 34 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 1
Size: 802 Color: 0
Size: 36 Color: 1

Bin 102: 36 of cap free
Amount of items: 2
Items: 
Size: 6692 Color: 0
Size: 1096 Color: 1

Bin 103: 36 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 0
Size: 1032 Color: 1

Bin 104: 38 of cap free
Amount of items: 2
Items: 
Size: 6110 Color: 1
Size: 1676 Color: 0

Bin 105: 43 of cap free
Amount of items: 2
Items: 
Size: 5919 Color: 0
Size: 1862 Color: 1

Bin 106: 48 of cap free
Amount of items: 2
Items: 
Size: 5594 Color: 0
Size: 2182 Color: 1

Bin 107: 51 of cap free
Amount of items: 2
Items: 
Size: 5149 Color: 1
Size: 2624 Color: 0

Bin 108: 52 of cap free
Amount of items: 2
Items: 
Size: 6070 Color: 0
Size: 1702 Color: 1

Bin 109: 53 of cap free
Amount of items: 2
Items: 
Size: 6182 Color: 0
Size: 1589 Color: 1

Bin 110: 53 of cap free
Amount of items: 2
Items: 
Size: 6334 Color: 0
Size: 1437 Color: 1

Bin 111: 58 of cap free
Amount of items: 2
Items: 
Size: 6786 Color: 1
Size: 980 Color: 0

Bin 112: 61 of cap free
Amount of items: 2
Items: 
Size: 6677 Color: 1
Size: 1086 Color: 0

Bin 113: 64 of cap free
Amount of items: 2
Items: 
Size: 4878 Color: 0
Size: 2882 Color: 1

Bin 114: 64 of cap free
Amount of items: 2
Items: 
Size: 5004 Color: 1
Size: 2756 Color: 0

Bin 115: 64 of cap free
Amount of items: 2
Items: 
Size: 5988 Color: 0
Size: 1772 Color: 1

Bin 116: 66 of cap free
Amount of items: 2
Items: 
Size: 4498 Color: 1
Size: 3260 Color: 0

Bin 117: 72 of cap free
Amount of items: 2
Items: 
Size: 5940 Color: 1
Size: 1812 Color: 0

Bin 118: 75 of cap free
Amount of items: 2
Items: 
Size: 5518 Color: 0
Size: 2231 Color: 1

Bin 119: 77 of cap free
Amount of items: 2
Items: 
Size: 5561 Color: 1
Size: 2186 Color: 0

Bin 120: 82 of cap free
Amount of items: 2
Items: 
Size: 5814 Color: 1
Size: 1928 Color: 0

Bin 121: 83 of cap free
Amount of items: 2
Items: 
Size: 6433 Color: 0
Size: 1308 Color: 1

Bin 122: 86 of cap free
Amount of items: 2
Items: 
Size: 4476 Color: 0
Size: 3262 Color: 1

Bin 123: 94 of cap free
Amount of items: 2
Items: 
Size: 4956 Color: 0
Size: 2774 Color: 1

Bin 124: 98 of cap free
Amount of items: 2
Items: 
Size: 5224 Color: 1
Size: 2502 Color: 0

Bin 125: 100 of cap free
Amount of items: 2
Items: 
Size: 6260 Color: 1
Size: 1464 Color: 0

Bin 126: 113 of cap free
Amount of items: 2
Items: 
Size: 4450 Color: 0
Size: 3261 Color: 1

Bin 127: 115 of cap free
Amount of items: 2
Items: 
Size: 6101 Color: 1
Size: 1608 Color: 0

Bin 128: 118 of cap free
Amount of items: 2
Items: 
Size: 6948 Color: 1
Size: 758 Color: 0

Bin 129: 122 of cap free
Amount of items: 2
Items: 
Size: 6520 Color: 1
Size: 1182 Color: 0

Bin 130: 125 of cap free
Amount of items: 2
Items: 
Size: 4984 Color: 1
Size: 2715 Color: 0

Bin 131: 126 of cap free
Amount of items: 2
Items: 
Size: 4442 Color: 0
Size: 3256 Color: 1

Bin 132: 127 of cap free
Amount of items: 2
Items: 
Size: 6517 Color: 1
Size: 1180 Color: 0

Bin 133: 4688 of cap free
Amount of items: 20
Items: 
Size: 184 Color: 1
Size: 176 Color: 1
Size: 176 Color: 1
Size: 176 Color: 1
Size: 176 Color: 1
Size: 176 Color: 0
Size: 168 Color: 0
Size: 160 Color: 1
Size: 160 Color: 1
Size: 160 Color: 0
Size: 152 Color: 0
Size: 152 Color: 0
Size: 148 Color: 1
Size: 148 Color: 0
Size: 144 Color: 1
Size: 144 Color: 0
Size: 144 Color: 0
Size: 136 Color: 1
Size: 128 Color: 0
Size: 128 Color: 0

Total size: 1032768
Total free space: 7824

