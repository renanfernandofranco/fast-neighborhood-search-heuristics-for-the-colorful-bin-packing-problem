Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 251870 Color: 1
Size: 363702 Color: 4
Size: 384429 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 328741 Color: 4
Size: 290070 Color: 1
Size: 381190 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 461749 Color: 3
Size: 279667 Color: 0
Size: 258585 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 382155 Color: 1
Size: 356169 Color: 0
Size: 261677 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 377102 Color: 3
Size: 357435 Color: 0
Size: 265464 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 423699 Color: 1
Size: 303386 Color: 4
Size: 272916 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 424254 Color: 0
Size: 316423 Color: 4
Size: 259324 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 478265 Color: 4
Size: 271047 Color: 3
Size: 250689 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 380033 Color: 0
Size: 334654 Color: 2
Size: 285314 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 424341 Color: 4
Size: 320350 Color: 4
Size: 255310 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 447483 Color: 4
Size: 287270 Color: 2
Size: 265248 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 471791 Color: 4
Size: 264876 Color: 1
Size: 263334 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 386874 Color: 2
Size: 318682 Color: 1
Size: 294445 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 356174 Color: 4
Size: 334927 Color: 2
Size: 308900 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 414749 Color: 0
Size: 322547 Color: 0
Size: 262705 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 359067 Color: 0
Size: 351939 Color: 1
Size: 288995 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 383131 Color: 1
Size: 346586 Color: 4
Size: 270284 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 419620 Color: 2
Size: 294363 Color: 1
Size: 286018 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 359224 Color: 3
Size: 324030 Color: 2
Size: 316747 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 383346 Color: 2
Size: 358966 Color: 3
Size: 257689 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 488910 Color: 1
Size: 261078 Color: 0
Size: 250013 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 358158 Color: 4
Size: 327795 Color: 4
Size: 314048 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 375007 Color: 1
Size: 343956 Color: 0
Size: 281038 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 389692 Color: 3
Size: 343528 Color: 2
Size: 266781 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 468093 Color: 2
Size: 279174 Color: 3
Size: 252734 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 451216 Color: 0
Size: 265889 Color: 0
Size: 282896 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 488659 Color: 2
Size: 257495 Color: 0
Size: 253847 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 486464 Color: 3
Size: 262117 Color: 2
Size: 251420 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 476205 Color: 2
Size: 263743 Color: 3
Size: 260053 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 412856 Color: 4
Size: 307723 Color: 3
Size: 279422 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 435492 Color: 0
Size: 287459 Color: 3
Size: 277050 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 432872 Color: 4
Size: 317107 Color: 0
Size: 250022 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 380475 Color: 4
Size: 338962 Color: 2
Size: 280564 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 387630 Color: 1
Size: 354509 Color: 2
Size: 257862 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 407087 Color: 0
Size: 323950 Color: 1
Size: 268964 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 428812 Color: 4
Size: 299162 Color: 3
Size: 272027 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 457646 Color: 0
Size: 289964 Color: 0
Size: 252391 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 311680 Color: 0
Size: 297068 Color: 2
Size: 391253 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 458950 Color: 0
Size: 283576 Color: 4
Size: 257475 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 402270 Color: 2
Size: 338955 Color: 0
Size: 258776 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 477261 Color: 0
Size: 267211 Color: 4
Size: 255529 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 491845 Color: 4
Size: 257450 Color: 1
Size: 250706 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 475365 Color: 3
Size: 263560 Color: 3
Size: 261076 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 447411 Color: 1
Size: 300867 Color: 2
Size: 251723 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 380095 Color: 3
Size: 343720 Color: 0
Size: 276186 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 415091 Color: 3
Size: 321697 Color: 0
Size: 263213 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 417626 Color: 4
Size: 315768 Color: 2
Size: 266607 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 499174 Color: 4
Size: 250682 Color: 3
Size: 250145 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 401046 Color: 2
Size: 307786 Color: 0
Size: 291169 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 388924 Color: 0
Size: 313655 Color: 4
Size: 297422 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 462287 Color: 2
Size: 273141 Color: 3
Size: 264573 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 468827 Color: 1
Size: 274219 Color: 0
Size: 256955 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 436182 Color: 3
Size: 288052 Color: 1
Size: 275767 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 465222 Color: 4
Size: 281457 Color: 3
Size: 253322 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 460140 Color: 2
Size: 271874 Color: 3
Size: 267987 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 381236 Color: 1
Size: 363712 Color: 1
Size: 255053 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 353510 Color: 0
Size: 334646 Color: 2
Size: 311845 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 420597 Color: 4
Size: 304701 Color: 0
Size: 274703 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 362946 Color: 4
Size: 348519 Color: 4
Size: 288536 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 355478 Color: 0
Size: 339194 Color: 1
Size: 305329 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 396933 Color: 2
Size: 305148 Color: 4
Size: 297920 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 380742 Color: 4
Size: 325670 Color: 4
Size: 293589 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 488996 Color: 4
Size: 260954 Color: 1
Size: 250051 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 467594 Color: 1
Size: 281933 Color: 1
Size: 250474 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 491661 Color: 2
Size: 255409 Color: 0
Size: 252931 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 352524 Color: 0
Size: 331628 Color: 1
Size: 315849 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 459213 Color: 1
Size: 290583 Color: 1
Size: 250205 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 407053 Color: 4
Size: 311613 Color: 1
Size: 281335 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 471310 Color: 1
Size: 267246 Color: 0
Size: 261445 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 495952 Color: 3
Size: 252913 Color: 1
Size: 251136 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 365093 Color: 1
Size: 345161 Color: 0
Size: 289747 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 338612 Color: 2
Size: 334283 Color: 4
Size: 327106 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 422644 Color: 2
Size: 299147 Color: 2
Size: 278210 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 415958 Color: 0
Size: 293010 Color: 3
Size: 291033 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 485733 Color: 3
Size: 262325 Color: 3
Size: 251943 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 353246 Color: 0
Size: 346559 Color: 2
Size: 300196 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 431101 Color: 3
Size: 302439 Color: 3
Size: 266461 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 490326 Color: 1
Size: 258320 Color: 4
Size: 251355 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 474407 Color: 4
Size: 264721 Color: 3
Size: 260873 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 342331 Color: 3
Size: 331254 Color: 0
Size: 326416 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 352154 Color: 0
Size: 349277 Color: 3
Size: 298570 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 393348 Color: 0
Size: 337952 Color: 3
Size: 268701 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 342548 Color: 0
Size: 340484 Color: 4
Size: 316969 Color: 4

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 406795 Color: 2
Size: 339941 Color: 0
Size: 253265 Color: 3

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 420258 Color: 3
Size: 295572 Color: 1
Size: 284171 Color: 4

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 483859 Color: 0
Size: 258119 Color: 4
Size: 258023 Color: 4

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 386944 Color: 2
Size: 352274 Color: 3
Size: 260783 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 400070 Color: 2
Size: 321557 Color: 3
Size: 278374 Color: 2

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 371784 Color: 3
Size: 318366 Color: 4
Size: 309851 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 464518 Color: 0
Size: 273660 Color: 3
Size: 261823 Color: 2

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 385939 Color: 0
Size: 333537 Color: 1
Size: 280525 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 441762 Color: 1
Size: 302325 Color: 1
Size: 255914 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 470762 Color: 2
Size: 265902 Color: 1
Size: 263337 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 364934 Color: 3
Size: 339169 Color: 2
Size: 295898 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 467199 Color: 3
Size: 269642 Color: 0
Size: 263160 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 418663 Color: 2
Size: 323001 Color: 4
Size: 258337 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 360018 Color: 4
Size: 323592 Color: 1
Size: 316391 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 422140 Color: 2
Size: 308806 Color: 3
Size: 269055 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 455874 Color: 2
Size: 293087 Color: 4
Size: 251040 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 415539 Color: 0
Size: 322455 Color: 0
Size: 262007 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 381415 Color: 2
Size: 347806 Color: 4
Size: 270780 Color: 4

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 491821 Color: 1
Size: 254762 Color: 1
Size: 253418 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 433175 Color: 2
Size: 309225 Color: 1
Size: 257601 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 385774 Color: 3
Size: 364032 Color: 2
Size: 250195 Color: 4

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 386751 Color: 2
Size: 340836 Color: 1
Size: 272414 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 347995 Color: 2
Size: 332074 Color: 0
Size: 319932 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 384945 Color: 0
Size: 334327 Color: 3
Size: 280729 Color: 4

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 383462 Color: 2
Size: 364204 Color: 3
Size: 252335 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 481806 Color: 3
Size: 267180 Color: 1
Size: 251015 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 341742 Color: 3
Size: 332296 Color: 3
Size: 325963 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 428285 Color: 1
Size: 315707 Color: 4
Size: 256009 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 495178 Color: 4
Size: 254354 Color: 2
Size: 250469 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 427637 Color: 1
Size: 311359 Color: 1
Size: 261005 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 416176 Color: 3
Size: 326474 Color: 1
Size: 257351 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 431574 Color: 4
Size: 284631 Color: 4
Size: 283796 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 400796 Color: 0
Size: 313580 Color: 2
Size: 285625 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 380715 Color: 1
Size: 321535 Color: 0
Size: 297751 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 412797 Color: 1
Size: 322795 Color: 1
Size: 264409 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 432089 Color: 4
Size: 287005 Color: 0
Size: 280907 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 393046 Color: 4
Size: 306149 Color: 1
Size: 300806 Color: 4

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 378426 Color: 4
Size: 323054 Color: 4
Size: 298521 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 459862 Color: 3
Size: 279216 Color: 2
Size: 260923 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 386112 Color: 0
Size: 311057 Color: 3
Size: 302832 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 414140 Color: 0
Size: 335691 Color: 3
Size: 250170 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 405654 Color: 4
Size: 315677 Color: 0
Size: 278670 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 432087 Color: 3
Size: 286219 Color: 3
Size: 281695 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 403816 Color: 1
Size: 343885 Color: 0
Size: 252300 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 385999 Color: 3
Size: 353827 Color: 2
Size: 260175 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 360643 Color: 3
Size: 335003 Color: 4
Size: 304355 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 374682 Color: 0
Size: 349291 Color: 0
Size: 276028 Color: 4

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 396518 Color: 0
Size: 342598 Color: 3
Size: 260885 Color: 3

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 495306 Color: 4
Size: 254388 Color: 2
Size: 250307 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 476390 Color: 2
Size: 265576 Color: 2
Size: 258035 Color: 3

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 399363 Color: 2
Size: 311283 Color: 2
Size: 289355 Color: 3

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 486825 Color: 4
Size: 262736 Color: 2
Size: 250440 Color: 3

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 457719 Color: 4
Size: 274214 Color: 4
Size: 268068 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 458202 Color: 2
Size: 279377 Color: 3
Size: 262422 Color: 3

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 452181 Color: 4
Size: 281470 Color: 0
Size: 266350 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 499726 Color: 1
Size: 250150 Color: 3
Size: 250125 Color: 2

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 358673 Color: 3
Size: 354391 Color: 2
Size: 286937 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 464889 Color: 2
Size: 278661 Color: 2
Size: 256451 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 408571 Color: 2
Size: 313506 Color: 4
Size: 277924 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 495057 Color: 1
Size: 253044 Color: 0
Size: 251900 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 452065 Color: 2
Size: 283026 Color: 1
Size: 264910 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 481919 Color: 3
Size: 264498 Color: 3
Size: 253584 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 460098 Color: 4
Size: 280299 Color: 1
Size: 259604 Color: 4

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 402865 Color: 1
Size: 315563 Color: 2
Size: 281573 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 499270 Color: 4
Size: 250423 Color: 0
Size: 250308 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 470067 Color: 3
Size: 266893 Color: 0
Size: 263041 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 426216 Color: 3
Size: 292463 Color: 3
Size: 281322 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 451139 Color: 2
Size: 285213 Color: 2
Size: 263649 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 411781 Color: 1
Size: 320296 Color: 4
Size: 267924 Color: 3

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 499052 Color: 4
Size: 250764 Color: 0
Size: 250185 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 361144 Color: 3
Size: 330999 Color: 0
Size: 307858 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 368704 Color: 2
Size: 353295 Color: 1
Size: 278002 Color: 4

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 485322 Color: 4
Size: 261031 Color: 0
Size: 253648 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 368700 Color: 3
Size: 350615 Color: 4
Size: 280686 Color: 2

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 380548 Color: 2
Size: 351031 Color: 0
Size: 268422 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 419478 Color: 1
Size: 315651 Color: 2
Size: 264872 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 394760 Color: 2
Size: 303585 Color: 3
Size: 301656 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 406332 Color: 3
Size: 316043 Color: 0
Size: 277626 Color: 3

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 461621 Color: 2
Size: 277945 Color: 4
Size: 260435 Color: 3

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 410900 Color: 1
Size: 318993 Color: 4
Size: 270108 Color: 2

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 492324 Color: 4
Size: 254657 Color: 0
Size: 253020 Color: 4

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 419366 Color: 4
Size: 304956 Color: 3
Size: 275679 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 418657 Color: 0
Size: 295590 Color: 3
Size: 285754 Color: 4

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 458690 Color: 0
Size: 285795 Color: 4
Size: 255516 Color: 4

Total size: 167000167
Total free space: 0

