Capicity Bin: 1000001
Lower Bound: 43

Bins used: 44
Amount of Colors: 5

Bin 1: 338 of cap free
Amount of items: 2
Items: 
Size: 740749 Color: 3
Size: 258914 Color: 2

Bin 2: 357 of cap free
Amount of items: 2
Items: 
Size: 610485 Color: 0
Size: 389159 Color: 2

Bin 3: 517 of cap free
Amount of items: 3
Items: 
Size: 543668 Color: 4
Size: 282922 Color: 1
Size: 172894 Color: 0

Bin 4: 736 of cap free
Amount of items: 2
Items: 
Size: 615157 Color: 3
Size: 384108 Color: 1

Bin 5: 1106 of cap free
Amount of items: 3
Items: 
Size: 534684 Color: 0
Size: 255780 Color: 3
Size: 208431 Color: 0

Bin 6: 1253 of cap free
Amount of items: 2
Items: 
Size: 751346 Color: 3
Size: 247402 Color: 1

Bin 7: 1523 of cap free
Amount of items: 3
Items: 
Size: 557059 Color: 2
Size: 299230 Color: 1
Size: 142189 Color: 2

Bin 8: 1712 of cap free
Amount of items: 2
Items: 
Size: 595170 Color: 4
Size: 403119 Color: 1

Bin 9: 1763 of cap free
Amount of items: 2
Items: 
Size: 709936 Color: 1
Size: 288302 Color: 2

Bin 10: 2120 of cap free
Amount of items: 2
Items: 
Size: 569972 Color: 2
Size: 427909 Color: 4

Bin 11: 2749 of cap free
Amount of items: 3
Items: 
Size: 532094 Color: 2
Size: 249463 Color: 1
Size: 215695 Color: 3

Bin 12: 3059 of cap free
Amount of items: 2
Items: 
Size: 759236 Color: 2
Size: 237706 Color: 3

Bin 13: 3071 of cap free
Amount of items: 2
Items: 
Size: 729941 Color: 1
Size: 266989 Color: 4

Bin 14: 3616 of cap free
Amount of items: 3
Items: 
Size: 533057 Color: 2
Size: 256535 Color: 0
Size: 206793 Color: 2

Bin 15: 3858 of cap free
Amount of items: 2
Items: 
Size: 641027 Color: 3
Size: 355116 Color: 0

Bin 16: 4464 of cap free
Amount of items: 3
Items: 
Size: 542318 Color: 2
Size: 281292 Color: 3
Size: 171927 Color: 4

Bin 17: 4541 of cap free
Amount of items: 2
Items: 
Size: 513410 Color: 4
Size: 482050 Color: 1

Bin 18: 4640 of cap free
Amount of items: 3
Items: 
Size: 560009 Color: 0
Size: 305884 Color: 2
Size: 129468 Color: 3

Bin 19: 4746 of cap free
Amount of items: 2
Items: 
Size: 695856 Color: 1
Size: 299399 Color: 3

Bin 20: 5889 of cap free
Amount of items: 3
Items: 
Size: 542836 Color: 3
Size: 268047 Color: 1
Size: 183229 Color: 3

Bin 21: 7983 of cap free
Amount of items: 2
Items: 
Size: 597173 Color: 3
Size: 394845 Color: 1

Bin 22: 8037 of cap free
Amount of items: 2
Items: 
Size: 656394 Color: 3
Size: 335570 Color: 1

Bin 23: 8678 of cap free
Amount of items: 2
Items: 
Size: 734676 Color: 1
Size: 256647 Color: 0

Bin 24: 9010 of cap free
Amount of items: 2
Items: 
Size: 776140 Color: 4
Size: 214851 Color: 0

Bin 25: 9085 of cap free
Amount of items: 2
Items: 
Size: 576460 Color: 0
Size: 414456 Color: 3

Bin 26: 9376 of cap free
Amount of items: 2
Items: 
Size: 564623 Color: 2
Size: 426002 Color: 0

Bin 27: 9651 of cap free
Amount of items: 2
Items: 
Size: 653613 Color: 1
Size: 336737 Color: 3

Bin 28: 10156 of cap free
Amount of items: 2
Items: 
Size: 496019 Color: 3
Size: 493826 Color: 1

Bin 29: 10679 of cap free
Amount of items: 2
Items: 
Size: 764251 Color: 2
Size: 225071 Color: 3

Bin 30: 11024 of cap free
Amount of items: 4
Items: 
Size: 529778 Color: 3
Size: 182129 Color: 2
Size: 171074 Color: 2
Size: 105996 Color: 1

Bin 31: 11730 of cap free
Amount of items: 2
Items: 
Size: 507674 Color: 1
Size: 480597 Color: 2

Bin 32: 14069 of cap free
Amount of items: 2
Items: 
Size: 591588 Color: 2
Size: 394344 Color: 3

Bin 33: 16880 of cap free
Amount of items: 2
Items: 
Size: 787021 Color: 1
Size: 196100 Color: 3

Bin 34: 20962 of cap free
Amount of items: 2
Items: 
Size: 788908 Color: 3
Size: 190131 Color: 1

Bin 35: 21848 of cap free
Amount of items: 2
Items: 
Size: 506714 Color: 0
Size: 471439 Color: 1

Bin 36: 22473 of cap free
Amount of items: 2
Items: 
Size: 763222 Color: 4
Size: 214306 Color: 1

Bin 37: 22924 of cap free
Amount of items: 2
Items: 
Size: 650858 Color: 4
Size: 326219 Color: 2

Bin 38: 37429 of cap free
Amount of items: 3
Items: 
Size: 559853 Color: 3
Size: 302020 Color: 1
Size: 100699 Color: 4

Bin 39: 39984 of cap free
Amount of items: 2
Items: 
Size: 572973 Color: 0
Size: 387044 Color: 2

Bin 40: 40347 of cap free
Amount of items: 2
Items: 
Size: 638825 Color: 3
Size: 320829 Color: 1

Bin 41: 47534 of cap free
Amount of items: 2
Items: 
Size: 633572 Color: 4
Size: 318895 Color: 2

Bin 42: 48852 of cap free
Amount of items: 6
Items: 
Size: 167604 Color: 3
Size: 167271 Color: 3
Size: 165250 Color: 1
Size: 155621 Color: 3
Size: 154985 Color: 0
Size: 140418 Color: 0

Bin 43: 64478 of cap free
Amount of items: 2
Items: 
Size: 622406 Color: 0
Size: 313117 Color: 1

Bin 44: 899353 of cap free
Amount of items: 1
Items: 
Size: 100648 Color: 0

Total size: 42545444
Total free space: 1454600

