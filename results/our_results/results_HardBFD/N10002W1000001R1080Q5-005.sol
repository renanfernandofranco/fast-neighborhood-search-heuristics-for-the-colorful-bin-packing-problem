Capicity Bin: 1000001
Lower Bound: 4500

Bins used: 4514
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 500313 Color: 2
Size: 499688 Color: 4

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 504569 Color: 0
Size: 495432 Color: 1

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 521081 Color: 3
Size: 478920 Color: 0

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 521375 Color: 1
Size: 478626 Color: 3

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 529426 Color: 0
Size: 470575 Color: 1

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 534551 Color: 2
Size: 465450 Color: 1

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 542221 Color: 2
Size: 457780 Color: 0

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 548539 Color: 0
Size: 451462 Color: 3

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 551762 Color: 1
Size: 448239 Color: 2

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 554754 Color: 3
Size: 445247 Color: 1

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 557517 Color: 3
Size: 442484 Color: 2

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 558647 Color: 4
Size: 441354 Color: 3

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 561082 Color: 3
Size: 438919 Color: 1

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 562333 Color: 4
Size: 437668 Color: 1

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 573648 Color: 4
Size: 426353 Color: 0

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 577537 Color: 1
Size: 422464 Color: 4

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 585130 Color: 3
Size: 414871 Color: 1

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 585877 Color: 2
Size: 414124 Color: 4

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 589104 Color: 1
Size: 410897 Color: 3

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 594145 Color: 3
Size: 405856 Color: 2

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 604317 Color: 1
Size: 395684 Color: 0

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 604882 Color: 1
Size: 395119 Color: 2

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 614563 Color: 2
Size: 385438 Color: 4

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 615511 Color: 2
Size: 384490 Color: 0

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 622032 Color: 1
Size: 377969 Color: 4

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 650043 Color: 2
Size: 349958 Color: 0

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 652067 Color: 3
Size: 347934 Color: 4

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 656459 Color: 4
Size: 343542 Color: 3

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 656564 Color: 3
Size: 343437 Color: 1

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 658690 Color: 0
Size: 341311 Color: 1

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 668323 Color: 4
Size: 331678 Color: 2

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 669602 Color: 1
Size: 330399 Color: 4

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 688285 Color: 1
Size: 311716 Color: 3

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 688789 Color: 4
Size: 311212 Color: 2

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 700103 Color: 0
Size: 299898 Color: 1

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 701758 Color: 4
Size: 298243 Color: 0

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 704220 Color: 0
Size: 295781 Color: 4

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 708072 Color: 3
Size: 291929 Color: 1

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 709485 Color: 4
Size: 290516 Color: 3

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 719173 Color: 4
Size: 280828 Color: 2

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 725488 Color: 4
Size: 274513 Color: 3

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 726229 Color: 4
Size: 273772 Color: 2

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 727698 Color: 2
Size: 272303 Color: 0

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 736349 Color: 2
Size: 263652 Color: 4

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 741125 Color: 4
Size: 258876 Color: 3

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 745852 Color: 2
Size: 254149 Color: 0

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 757922 Color: 1
Size: 242079 Color: 4

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 767541 Color: 3
Size: 232460 Color: 0

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 768724 Color: 1
Size: 231277 Color: 3

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 774452 Color: 4
Size: 225549 Color: 0

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 781701 Color: 4
Size: 218300 Color: 3

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 793188 Color: 1
Size: 206813 Color: 2

Bin 53: 1 of cap free
Amount of items: 6
Items: 
Size: 173856 Color: 4
Size: 173731 Color: 0
Size: 173655 Color: 1
Size: 173538 Color: 0
Size: 173519 Color: 1
Size: 131701 Color: 4

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 500394 Color: 4
Size: 499606 Color: 0

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 502191 Color: 3
Size: 497809 Color: 0

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 508632 Color: 0
Size: 491368 Color: 3

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 518490 Color: 0
Size: 481510 Color: 1

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 525233 Color: 4
Size: 474767 Color: 2

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 531248 Color: 0
Size: 468752 Color: 4

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 538702 Color: 0
Size: 461298 Color: 4

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 539299 Color: 4
Size: 460701 Color: 3

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 551701 Color: 3
Size: 448299 Color: 0

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 554196 Color: 2
Size: 445804 Color: 3

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 557743 Color: 0
Size: 442257 Color: 3

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 562035 Color: 3
Size: 437965 Color: 2

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 563087 Color: 3
Size: 436913 Color: 1

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 580361 Color: 4
Size: 419639 Color: 0

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 602175 Color: 1
Size: 397825 Color: 2

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 606991 Color: 2
Size: 393009 Color: 4

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 608468 Color: 0
Size: 391532 Color: 2

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 618394 Color: 3
Size: 381606 Color: 4

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 619426 Color: 3
Size: 195421 Color: 1
Size: 185153 Color: 3

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 620461 Color: 2
Size: 379539 Color: 1

Bin 74: 1 of cap free
Amount of items: 2
Items: 
Size: 634107 Color: 4
Size: 365893 Color: 1

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 634226 Color: 4
Size: 365774 Color: 1

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 646208 Color: 3
Size: 353792 Color: 2

Bin 77: 1 of cap free
Amount of items: 2
Items: 
Size: 646780 Color: 4
Size: 353220 Color: 3

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 647421 Color: 4
Size: 352579 Color: 3

Bin 79: 1 of cap free
Amount of items: 2
Items: 
Size: 654020 Color: 3
Size: 345980 Color: 4

Bin 80: 1 of cap free
Amount of items: 2
Items: 
Size: 663025 Color: 3
Size: 336975 Color: 2

Bin 81: 1 of cap free
Amount of items: 2
Items: 
Size: 669677 Color: 0
Size: 330323 Color: 1

Bin 82: 1 of cap free
Amount of items: 2
Items: 
Size: 687308 Color: 1
Size: 312692 Color: 2

Bin 83: 1 of cap free
Amount of items: 2
Items: 
Size: 687393 Color: 3
Size: 312607 Color: 4

Bin 84: 1 of cap free
Amount of items: 2
Items: 
Size: 704787 Color: 1
Size: 295213 Color: 3

Bin 85: 1 of cap free
Amount of items: 2
Items: 
Size: 709094 Color: 4
Size: 290906 Color: 2

Bin 86: 1 of cap free
Amount of items: 2
Items: 
Size: 710699 Color: 0
Size: 289301 Color: 3

Bin 87: 1 of cap free
Amount of items: 2
Items: 
Size: 716521 Color: 4
Size: 283479 Color: 3

Bin 88: 1 of cap free
Amount of items: 2
Items: 
Size: 723326 Color: 1
Size: 276674 Color: 4

Bin 89: 1 of cap free
Amount of items: 2
Items: 
Size: 723725 Color: 2
Size: 276275 Color: 3

Bin 90: 1 of cap free
Amount of items: 2
Items: 
Size: 725881 Color: 4
Size: 274119 Color: 3

Bin 91: 1 of cap free
Amount of items: 2
Items: 
Size: 734531 Color: 2
Size: 265469 Color: 1

Bin 92: 1 of cap free
Amount of items: 2
Items: 
Size: 734661 Color: 0
Size: 265339 Color: 4

Bin 93: 1 of cap free
Amount of items: 2
Items: 
Size: 736043 Color: 1
Size: 263957 Color: 0

Bin 94: 1 of cap free
Amount of items: 2
Items: 
Size: 744852 Color: 3
Size: 255148 Color: 2

Bin 95: 1 of cap free
Amount of items: 2
Items: 
Size: 749507 Color: 3
Size: 250493 Color: 0

Bin 96: 1 of cap free
Amount of items: 2
Items: 
Size: 750824 Color: 3
Size: 249176 Color: 1

Bin 97: 1 of cap free
Amount of items: 2
Items: 
Size: 758790 Color: 0
Size: 241210 Color: 2

Bin 98: 1 of cap free
Amount of items: 2
Items: 
Size: 759003 Color: 3
Size: 240997 Color: 4

Bin 99: 1 of cap free
Amount of items: 2
Items: 
Size: 763678 Color: 0
Size: 236322 Color: 4

Bin 100: 1 of cap free
Amount of items: 2
Items: 
Size: 779091 Color: 3
Size: 220909 Color: 4

Bin 101: 1 of cap free
Amount of items: 2
Items: 
Size: 799975 Color: 3
Size: 200025 Color: 0

Bin 102: 2 of cap free
Amount of items: 2
Items: 
Size: 509514 Color: 1
Size: 490485 Color: 0

Bin 103: 2 of cap free
Amount of items: 2
Items: 
Size: 515687 Color: 2
Size: 484312 Color: 0

Bin 104: 2 of cap free
Amount of items: 2
Items: 
Size: 516078 Color: 0
Size: 483921 Color: 2

Bin 105: 2 of cap free
Amount of items: 2
Items: 
Size: 535737 Color: 1
Size: 464262 Color: 0

Bin 106: 2 of cap free
Amount of items: 2
Items: 
Size: 540389 Color: 1
Size: 459610 Color: 3

Bin 107: 2 of cap free
Amount of items: 2
Items: 
Size: 541822 Color: 0
Size: 458177 Color: 4

Bin 108: 2 of cap free
Amount of items: 2
Items: 
Size: 550745 Color: 1
Size: 449254 Color: 3

Bin 109: 2 of cap free
Amount of items: 2
Items: 
Size: 553061 Color: 1
Size: 446938 Color: 3

Bin 110: 2 of cap free
Amount of items: 2
Items: 
Size: 555415 Color: 0
Size: 444584 Color: 3

Bin 111: 2 of cap free
Amount of items: 2
Items: 
Size: 557837 Color: 3
Size: 442162 Color: 1

Bin 112: 2 of cap free
Amount of items: 2
Items: 
Size: 560862 Color: 1
Size: 439137 Color: 4

Bin 113: 2 of cap free
Amount of items: 2
Items: 
Size: 579319 Color: 0
Size: 420680 Color: 1

Bin 114: 2 of cap free
Amount of items: 2
Items: 
Size: 580253 Color: 4
Size: 419746 Color: 3

Bin 115: 2 of cap free
Amount of items: 2
Items: 
Size: 580743 Color: 3
Size: 419256 Color: 1

Bin 116: 2 of cap free
Amount of items: 2
Items: 
Size: 587453 Color: 1
Size: 412546 Color: 2

Bin 117: 2 of cap free
Amount of items: 2
Items: 
Size: 588206 Color: 1
Size: 411793 Color: 2

Bin 118: 2 of cap free
Amount of items: 2
Items: 
Size: 593176 Color: 4
Size: 406823 Color: 0

Bin 119: 2 of cap free
Amount of items: 2
Items: 
Size: 593379 Color: 3
Size: 406620 Color: 1

Bin 120: 2 of cap free
Amount of items: 2
Items: 
Size: 596538 Color: 4
Size: 403461 Color: 1

Bin 121: 2 of cap free
Amount of items: 2
Items: 
Size: 601468 Color: 1
Size: 398531 Color: 3

Bin 122: 2 of cap free
Amount of items: 3
Items: 
Size: 619450 Color: 4
Size: 195560 Color: 2
Size: 184989 Color: 0

Bin 123: 2 of cap free
Amount of items: 2
Items: 
Size: 638613 Color: 3
Size: 361386 Color: 2

Bin 124: 2 of cap free
Amount of items: 2
Items: 
Size: 638974 Color: 3
Size: 361025 Color: 2

Bin 125: 2 of cap free
Amount of items: 2
Items: 
Size: 647337 Color: 3
Size: 352662 Color: 2

Bin 126: 2 of cap free
Amount of items: 2
Items: 
Size: 653746 Color: 0
Size: 346253 Color: 3

Bin 127: 2 of cap free
Amount of items: 2
Items: 
Size: 662468 Color: 3
Size: 337531 Color: 2

Bin 128: 2 of cap free
Amount of items: 2
Items: 
Size: 675854 Color: 3
Size: 324145 Color: 4

Bin 129: 2 of cap free
Amount of items: 2
Items: 
Size: 680893 Color: 1
Size: 319106 Color: 3

Bin 130: 2 of cap free
Amount of items: 2
Items: 
Size: 719635 Color: 3
Size: 280364 Color: 2

Bin 131: 2 of cap free
Amount of items: 2
Items: 
Size: 719972 Color: 4
Size: 280027 Color: 1

Bin 132: 2 of cap free
Amount of items: 2
Items: 
Size: 736748 Color: 0
Size: 263251 Color: 4

Bin 133: 2 of cap free
Amount of items: 2
Items: 
Size: 741880 Color: 3
Size: 258119 Color: 2

Bin 134: 2 of cap free
Amount of items: 2
Items: 
Size: 751313 Color: 0
Size: 248686 Color: 2

Bin 135: 2 of cap free
Amount of items: 2
Items: 
Size: 755277 Color: 0
Size: 244722 Color: 4

Bin 136: 2 of cap free
Amount of items: 2
Items: 
Size: 756041 Color: 2
Size: 243958 Color: 3

Bin 137: 2 of cap free
Amount of items: 2
Items: 
Size: 774098 Color: 4
Size: 225901 Color: 3

Bin 138: 2 of cap free
Amount of items: 2
Items: 
Size: 781610 Color: 1
Size: 218389 Color: 0

Bin 139: 2 of cap free
Amount of items: 2
Items: 
Size: 782250 Color: 4
Size: 217749 Color: 1

Bin 140: 2 of cap free
Amount of items: 2
Items: 
Size: 788591 Color: 3
Size: 211408 Color: 4

Bin 141: 2 of cap free
Amount of items: 2
Items: 
Size: 796511 Color: 3
Size: 203488 Color: 1

Bin 142: 3 of cap free
Amount of items: 6
Items: 
Size: 168384 Color: 2
Size: 168369 Color: 4
Size: 168341 Color: 3
Size: 168333 Color: 4
Size: 168300 Color: 4
Size: 158271 Color: 3

Bin 143: 3 of cap free
Amount of items: 2
Items: 
Size: 505292 Color: 3
Size: 494706 Color: 0

Bin 144: 3 of cap free
Amount of items: 2
Items: 
Size: 509520 Color: 4
Size: 490478 Color: 2

Bin 145: 3 of cap free
Amount of items: 2
Items: 
Size: 525964 Color: 4
Size: 474034 Color: 2

Bin 146: 3 of cap free
Amount of items: 2
Items: 
Size: 526515 Color: 4
Size: 473483 Color: 0

Bin 147: 3 of cap free
Amount of items: 2
Items: 
Size: 540373 Color: 4
Size: 459625 Color: 2

Bin 148: 3 of cap free
Amount of items: 2
Items: 
Size: 540863 Color: 3
Size: 459135 Color: 1

Bin 149: 3 of cap free
Amount of items: 2
Items: 
Size: 541362 Color: 3
Size: 458636 Color: 4

Bin 150: 3 of cap free
Amount of items: 2
Items: 
Size: 542387 Color: 4
Size: 457611 Color: 1

Bin 151: 3 of cap free
Amount of items: 2
Items: 
Size: 547335 Color: 0
Size: 452663 Color: 4

Bin 152: 3 of cap free
Amount of items: 2
Items: 
Size: 547353 Color: 1
Size: 452645 Color: 2

Bin 153: 3 of cap free
Amount of items: 2
Items: 
Size: 557909 Color: 0
Size: 442089 Color: 2

Bin 154: 3 of cap free
Amount of items: 2
Items: 
Size: 563968 Color: 1
Size: 436030 Color: 0

Bin 155: 3 of cap free
Amount of items: 2
Items: 
Size: 566945 Color: 0
Size: 433053 Color: 4

Bin 156: 3 of cap free
Amount of items: 2
Items: 
Size: 576829 Color: 2
Size: 423169 Color: 3

Bin 157: 3 of cap free
Amount of items: 2
Items: 
Size: 598065 Color: 1
Size: 401933 Color: 2

Bin 158: 3 of cap free
Amount of items: 2
Items: 
Size: 605986 Color: 4
Size: 394012 Color: 3

Bin 159: 3 of cap free
Amount of items: 2
Items: 
Size: 614347 Color: 4
Size: 385651 Color: 1

Bin 160: 3 of cap free
Amount of items: 2
Items: 
Size: 615445 Color: 2
Size: 384553 Color: 3

Bin 161: 3 of cap free
Amount of items: 2
Items: 
Size: 617803 Color: 4
Size: 382195 Color: 0

Bin 162: 3 of cap free
Amount of items: 2
Items: 
Size: 626361 Color: 2
Size: 373637 Color: 1

Bin 163: 3 of cap free
Amount of items: 2
Items: 
Size: 628108 Color: 3
Size: 371890 Color: 1

Bin 164: 3 of cap free
Amount of items: 2
Items: 
Size: 629312 Color: 0
Size: 370686 Color: 4

Bin 165: 3 of cap free
Amount of items: 2
Items: 
Size: 644253 Color: 4
Size: 355745 Color: 1

Bin 166: 3 of cap free
Amount of items: 2
Items: 
Size: 650851 Color: 2
Size: 349147 Color: 3

Bin 167: 3 of cap free
Amount of items: 2
Items: 
Size: 660264 Color: 3
Size: 339734 Color: 1

Bin 168: 3 of cap free
Amount of items: 2
Items: 
Size: 673121 Color: 3
Size: 326877 Color: 4

Bin 169: 3 of cap free
Amount of items: 2
Items: 
Size: 676628 Color: 2
Size: 323370 Color: 4

Bin 170: 3 of cap free
Amount of items: 2
Items: 
Size: 678813 Color: 0
Size: 321185 Color: 3

Bin 171: 3 of cap free
Amount of items: 2
Items: 
Size: 687473 Color: 1
Size: 312525 Color: 4

Bin 172: 3 of cap free
Amount of items: 2
Items: 
Size: 689919 Color: 2
Size: 310079 Color: 1

Bin 173: 3 of cap free
Amount of items: 2
Items: 
Size: 696807 Color: 0
Size: 303191 Color: 2

Bin 174: 3 of cap free
Amount of items: 2
Items: 
Size: 700019 Color: 1
Size: 299979 Color: 4

Bin 175: 3 of cap free
Amount of items: 2
Items: 
Size: 709421 Color: 2
Size: 290577 Color: 0

Bin 176: 3 of cap free
Amount of items: 2
Items: 
Size: 717109 Color: 0
Size: 282889 Color: 2

Bin 177: 3 of cap free
Amount of items: 2
Items: 
Size: 717606 Color: 1
Size: 282392 Color: 2

Bin 178: 3 of cap free
Amount of items: 2
Items: 
Size: 726717 Color: 3
Size: 273281 Color: 2

Bin 179: 3 of cap free
Amount of items: 2
Items: 
Size: 731449 Color: 0
Size: 268549 Color: 1

Bin 180: 3 of cap free
Amount of items: 2
Items: 
Size: 745717 Color: 0
Size: 254281 Color: 1

Bin 181: 3 of cap free
Amount of items: 2
Items: 
Size: 746842 Color: 3
Size: 253156 Color: 0

Bin 182: 3 of cap free
Amount of items: 2
Items: 
Size: 748342 Color: 3
Size: 251656 Color: 4

Bin 183: 3 of cap free
Amount of items: 2
Items: 
Size: 749204 Color: 0
Size: 250794 Color: 3

Bin 184: 3 of cap free
Amount of items: 2
Items: 
Size: 757378 Color: 0
Size: 242620 Color: 3

Bin 185: 3 of cap free
Amount of items: 2
Items: 
Size: 762820 Color: 1
Size: 237178 Color: 2

Bin 186: 3 of cap free
Amount of items: 2
Items: 
Size: 768514 Color: 4
Size: 231484 Color: 3

Bin 187: 3 of cap free
Amount of items: 2
Items: 
Size: 775581 Color: 3
Size: 224417 Color: 2

Bin 188: 3 of cap free
Amount of items: 2
Items: 
Size: 780156 Color: 3
Size: 219842 Color: 0

Bin 189: 3 of cap free
Amount of items: 2
Items: 
Size: 785355 Color: 3
Size: 214643 Color: 4

Bin 190: 3 of cap free
Amount of items: 2
Items: 
Size: 787477 Color: 1
Size: 212521 Color: 2

Bin 191: 4 of cap free
Amount of items: 6
Items: 
Size: 166902 Color: 2
Size: 166878 Color: 0
Size: 166837 Color: 1
Size: 166796 Color: 3
Size: 166673 Color: 0
Size: 165911 Color: 1

Bin 192: 4 of cap free
Amount of items: 2
Items: 
Size: 500910 Color: 0
Size: 499087 Color: 3

Bin 193: 4 of cap free
Amount of items: 2
Items: 
Size: 503511 Color: 2
Size: 496486 Color: 0

Bin 194: 4 of cap free
Amount of items: 2
Items: 
Size: 509432 Color: 1
Size: 490565 Color: 4

Bin 195: 4 of cap free
Amount of items: 2
Items: 
Size: 521450 Color: 2
Size: 478547 Color: 0

Bin 196: 4 of cap free
Amount of items: 2
Items: 
Size: 524983 Color: 0
Size: 475014 Color: 2

Bin 197: 4 of cap free
Amount of items: 2
Items: 
Size: 527790 Color: 3
Size: 472207 Color: 4

Bin 198: 4 of cap free
Amount of items: 2
Items: 
Size: 540038 Color: 3
Size: 459959 Color: 4

Bin 199: 4 of cap free
Amount of items: 2
Items: 
Size: 566326 Color: 4
Size: 433671 Color: 2

Bin 200: 4 of cap free
Amount of items: 2
Items: 
Size: 571664 Color: 2
Size: 428333 Color: 3

Bin 201: 4 of cap free
Amount of items: 2
Items: 
Size: 575049 Color: 1
Size: 424948 Color: 2

Bin 202: 4 of cap free
Amount of items: 2
Items: 
Size: 575825 Color: 3
Size: 424172 Color: 4

Bin 203: 4 of cap free
Amount of items: 2
Items: 
Size: 578265 Color: 3
Size: 421732 Color: 0

Bin 204: 4 of cap free
Amount of items: 2
Items: 
Size: 579977 Color: 3
Size: 420020 Color: 1

Bin 205: 4 of cap free
Amount of items: 2
Items: 
Size: 589305 Color: 4
Size: 410692 Color: 3

Bin 206: 4 of cap free
Amount of items: 2
Items: 
Size: 596772 Color: 1
Size: 403225 Color: 4

Bin 207: 4 of cap free
Amount of items: 2
Items: 
Size: 602632 Color: 0
Size: 397365 Color: 4

Bin 208: 4 of cap free
Amount of items: 2
Items: 
Size: 614492 Color: 3
Size: 385505 Color: 2

Bin 209: 4 of cap free
Amount of items: 2
Items: 
Size: 618727 Color: 3
Size: 381270 Color: 4

Bin 210: 4 of cap free
Amount of items: 2
Items: 
Size: 621676 Color: 3
Size: 378321 Color: 2

Bin 211: 4 of cap free
Amount of items: 2
Items: 
Size: 626218 Color: 1
Size: 373779 Color: 4

Bin 212: 4 of cap free
Amount of items: 2
Items: 
Size: 644029 Color: 2
Size: 355968 Color: 1

Bin 213: 4 of cap free
Amount of items: 2
Items: 
Size: 663162 Color: 3
Size: 336835 Color: 1

Bin 214: 4 of cap free
Amount of items: 2
Items: 
Size: 666203 Color: 3
Size: 333794 Color: 2

Bin 215: 4 of cap free
Amount of items: 2
Items: 
Size: 669294 Color: 0
Size: 330703 Color: 1

Bin 216: 4 of cap free
Amount of items: 2
Items: 
Size: 674343 Color: 4
Size: 325654 Color: 2

Bin 217: 4 of cap free
Amount of items: 2
Items: 
Size: 674589 Color: 0
Size: 325408 Color: 3

Bin 218: 4 of cap free
Amount of items: 2
Items: 
Size: 678572 Color: 1
Size: 321425 Color: 4

Bin 219: 4 of cap free
Amount of items: 2
Items: 
Size: 680045 Color: 0
Size: 319952 Color: 3

Bin 220: 4 of cap free
Amount of items: 2
Items: 
Size: 687111 Color: 1
Size: 312886 Color: 3

Bin 221: 4 of cap free
Amount of items: 2
Items: 
Size: 690890 Color: 2
Size: 309107 Color: 0

Bin 222: 4 of cap free
Amount of items: 2
Items: 
Size: 695382 Color: 3
Size: 304615 Color: 1

Bin 223: 4 of cap free
Amount of items: 2
Items: 
Size: 701680 Color: 0
Size: 298317 Color: 1

Bin 224: 4 of cap free
Amount of items: 2
Items: 
Size: 705345 Color: 2
Size: 294652 Color: 1

Bin 225: 4 of cap free
Amount of items: 2
Items: 
Size: 708547 Color: 2
Size: 291450 Color: 1

Bin 226: 4 of cap free
Amount of items: 2
Items: 
Size: 737238 Color: 0
Size: 262759 Color: 2

Bin 227: 4 of cap free
Amount of items: 2
Items: 
Size: 737319 Color: 1
Size: 262678 Color: 4

Bin 228: 4 of cap free
Amount of items: 2
Items: 
Size: 741689 Color: 4
Size: 258308 Color: 1

Bin 229: 4 of cap free
Amount of items: 2
Items: 
Size: 754355 Color: 2
Size: 245642 Color: 3

Bin 230: 4 of cap free
Amount of items: 2
Items: 
Size: 770376 Color: 3
Size: 229621 Color: 2

Bin 231: 4 of cap free
Amount of items: 2
Items: 
Size: 771810 Color: 0
Size: 228187 Color: 4

Bin 232: 4 of cap free
Amount of items: 2
Items: 
Size: 782142 Color: 1
Size: 217855 Color: 0

Bin 233: 4 of cap free
Amount of items: 2
Items: 
Size: 791663 Color: 0
Size: 208334 Color: 3

Bin 234: 4 of cap free
Amount of items: 2
Items: 
Size: 792775 Color: 0
Size: 207222 Color: 4

Bin 235: 5 of cap free
Amount of items: 2
Items: 
Size: 510000 Color: 1
Size: 489996 Color: 3

Bin 236: 5 of cap free
Amount of items: 2
Items: 
Size: 513000 Color: 2
Size: 486996 Color: 4

Bin 237: 5 of cap free
Amount of items: 2
Items: 
Size: 514680 Color: 3
Size: 485316 Color: 0

Bin 238: 5 of cap free
Amount of items: 2
Items: 
Size: 515531 Color: 1
Size: 484465 Color: 4

Bin 239: 5 of cap free
Amount of items: 2
Items: 
Size: 553547 Color: 4
Size: 446449 Color: 1

Bin 240: 5 of cap free
Amount of items: 2
Items: 
Size: 562436 Color: 1
Size: 437560 Color: 4

Bin 241: 5 of cap free
Amount of items: 2
Items: 
Size: 567541 Color: 1
Size: 432455 Color: 3

Bin 242: 5 of cap free
Amount of items: 2
Items: 
Size: 600274 Color: 3
Size: 399722 Color: 1

Bin 243: 5 of cap free
Amount of items: 2
Items: 
Size: 600677 Color: 4
Size: 399319 Color: 2

Bin 244: 5 of cap free
Amount of items: 2
Items: 
Size: 601489 Color: 3
Size: 398507 Color: 2

Bin 245: 5 of cap free
Amount of items: 3
Items: 
Size: 619499 Color: 3
Size: 195692 Color: 2
Size: 184805 Color: 2

Bin 246: 5 of cap free
Amount of items: 2
Items: 
Size: 633556 Color: 2
Size: 366440 Color: 0

Bin 247: 5 of cap free
Amount of items: 2
Items: 
Size: 643913 Color: 1
Size: 356083 Color: 0

Bin 248: 5 of cap free
Amount of items: 2
Items: 
Size: 644829 Color: 3
Size: 355167 Color: 4

Bin 249: 5 of cap free
Amount of items: 2
Items: 
Size: 645592 Color: 0
Size: 354404 Color: 2

Bin 250: 5 of cap free
Amount of items: 2
Items: 
Size: 651928 Color: 3
Size: 348068 Color: 1

Bin 251: 5 of cap free
Amount of items: 2
Items: 
Size: 657755 Color: 4
Size: 342241 Color: 3

Bin 252: 5 of cap free
Amount of items: 2
Items: 
Size: 668576 Color: 1
Size: 331420 Color: 4

Bin 253: 5 of cap free
Amount of items: 2
Items: 
Size: 679932 Color: 0
Size: 320064 Color: 2

Bin 254: 5 of cap free
Amount of items: 2
Items: 
Size: 681481 Color: 1
Size: 318515 Color: 0

Bin 255: 5 of cap free
Amount of items: 2
Items: 
Size: 693045 Color: 0
Size: 306951 Color: 1

Bin 256: 5 of cap free
Amount of items: 2
Items: 
Size: 702210 Color: 3
Size: 297786 Color: 1

Bin 257: 5 of cap free
Amount of items: 2
Items: 
Size: 705172 Color: 1
Size: 294824 Color: 4

Bin 258: 5 of cap free
Amount of items: 2
Items: 
Size: 711582 Color: 1
Size: 288414 Color: 2

Bin 259: 5 of cap free
Amount of items: 2
Items: 
Size: 713430 Color: 4
Size: 286566 Color: 3

Bin 260: 5 of cap free
Amount of items: 2
Items: 
Size: 715997 Color: 4
Size: 283999 Color: 2

Bin 261: 5 of cap free
Amount of items: 2
Items: 
Size: 718621 Color: 2
Size: 281375 Color: 1

Bin 262: 5 of cap free
Amount of items: 2
Items: 
Size: 719248 Color: 0
Size: 280748 Color: 4

Bin 263: 5 of cap free
Amount of items: 2
Items: 
Size: 719684 Color: 3
Size: 280312 Color: 4

Bin 264: 5 of cap free
Amount of items: 2
Items: 
Size: 721065 Color: 0
Size: 278931 Color: 4

Bin 265: 5 of cap free
Amount of items: 2
Items: 
Size: 759944 Color: 4
Size: 240052 Color: 1

Bin 266: 5 of cap free
Amount of items: 2
Items: 
Size: 760927 Color: 2
Size: 239069 Color: 4

Bin 267: 5 of cap free
Amount of items: 2
Items: 
Size: 763949 Color: 2
Size: 236047 Color: 3

Bin 268: 5 of cap free
Amount of items: 2
Items: 
Size: 767464 Color: 2
Size: 232532 Color: 0

Bin 269: 5 of cap free
Amount of items: 2
Items: 
Size: 779998 Color: 3
Size: 219998 Color: 1

Bin 270: 5 of cap free
Amount of items: 2
Items: 
Size: 798057 Color: 0
Size: 201939 Color: 4

Bin 271: 6 of cap free
Amount of items: 7
Items: 
Size: 148841 Color: 3
Size: 148818 Color: 1
Size: 148601 Color: 1
Size: 148577 Color: 0
Size: 148517 Color: 3
Size: 148374 Color: 2
Size: 108267 Color: 1

Bin 272: 6 of cap free
Amount of items: 2
Items: 
Size: 502369 Color: 4
Size: 497626 Color: 3

Bin 273: 6 of cap free
Amount of items: 2
Items: 
Size: 520367 Color: 3
Size: 479628 Color: 2

Bin 274: 6 of cap free
Amount of items: 2
Items: 
Size: 520510 Color: 0
Size: 479485 Color: 3

Bin 275: 6 of cap free
Amount of items: 2
Items: 
Size: 522418 Color: 1
Size: 477577 Color: 3

Bin 276: 6 of cap free
Amount of items: 2
Items: 
Size: 525276 Color: 4
Size: 474719 Color: 1

Bin 277: 6 of cap free
Amount of items: 2
Items: 
Size: 527360 Color: 4
Size: 472635 Color: 2

Bin 278: 6 of cap free
Amount of items: 2
Items: 
Size: 529411 Color: 4
Size: 470584 Color: 3

Bin 279: 6 of cap free
Amount of items: 2
Items: 
Size: 534358 Color: 0
Size: 465637 Color: 4

Bin 280: 6 of cap free
Amount of items: 2
Items: 
Size: 536261 Color: 0
Size: 463734 Color: 2

Bin 281: 6 of cap free
Amount of items: 2
Items: 
Size: 539576 Color: 3
Size: 460419 Color: 0

Bin 282: 6 of cap free
Amount of items: 2
Items: 
Size: 540883 Color: 1
Size: 459112 Color: 2

Bin 283: 6 of cap free
Amount of items: 2
Items: 
Size: 554946 Color: 2
Size: 445049 Color: 0

Bin 284: 6 of cap free
Amount of items: 2
Items: 
Size: 558579 Color: 3
Size: 441416 Color: 4

Bin 285: 6 of cap free
Amount of items: 2
Items: 
Size: 570017 Color: 2
Size: 429978 Color: 4

Bin 286: 6 of cap free
Amount of items: 2
Items: 
Size: 573119 Color: 3
Size: 426876 Color: 2

Bin 287: 6 of cap free
Amount of items: 2
Items: 
Size: 588701 Color: 0
Size: 411294 Color: 4

Bin 288: 6 of cap free
Amount of items: 2
Items: 
Size: 600250 Color: 3
Size: 399745 Color: 0

Bin 289: 6 of cap free
Amount of items: 2
Items: 
Size: 605837 Color: 1
Size: 394158 Color: 2

Bin 290: 6 of cap free
Amount of items: 2
Items: 
Size: 607590 Color: 0
Size: 392405 Color: 2

Bin 291: 6 of cap free
Amount of items: 2
Items: 
Size: 612848 Color: 1
Size: 387147 Color: 3

Bin 292: 6 of cap free
Amount of items: 2
Items: 
Size: 633913 Color: 1
Size: 366082 Color: 3

Bin 293: 6 of cap free
Amount of items: 2
Items: 
Size: 642310 Color: 2
Size: 357685 Color: 4

Bin 294: 6 of cap free
Amount of items: 2
Items: 
Size: 642419 Color: 3
Size: 357576 Color: 4

Bin 295: 6 of cap free
Amount of items: 2
Items: 
Size: 648825 Color: 4
Size: 351170 Color: 2

Bin 296: 6 of cap free
Amount of items: 2
Items: 
Size: 650372 Color: 0
Size: 349623 Color: 2

Bin 297: 6 of cap free
Amount of items: 2
Items: 
Size: 658074 Color: 0
Size: 341921 Color: 2

Bin 298: 6 of cap free
Amount of items: 2
Items: 
Size: 664495 Color: 3
Size: 335500 Color: 4

Bin 299: 6 of cap free
Amount of items: 2
Items: 
Size: 668930 Color: 4
Size: 331065 Color: 3

Bin 300: 6 of cap free
Amount of items: 2
Items: 
Size: 675343 Color: 4
Size: 324652 Color: 1

Bin 301: 6 of cap free
Amount of items: 2
Items: 
Size: 675863 Color: 3
Size: 324132 Color: 2

Bin 302: 6 of cap free
Amount of items: 2
Items: 
Size: 680010 Color: 4
Size: 319985 Color: 3

Bin 303: 6 of cap free
Amount of items: 2
Items: 
Size: 694882 Color: 4
Size: 305113 Color: 2

Bin 304: 6 of cap free
Amount of items: 2
Items: 
Size: 701144 Color: 0
Size: 298851 Color: 4

Bin 305: 6 of cap free
Amount of items: 2
Items: 
Size: 707191 Color: 4
Size: 292804 Color: 3

Bin 306: 6 of cap free
Amount of items: 2
Items: 
Size: 710492 Color: 0
Size: 289503 Color: 3

Bin 307: 6 of cap free
Amount of items: 2
Items: 
Size: 717507 Color: 2
Size: 282488 Color: 0

Bin 308: 6 of cap free
Amount of items: 2
Items: 
Size: 718036 Color: 0
Size: 281959 Color: 2

Bin 309: 6 of cap free
Amount of items: 2
Items: 
Size: 719254 Color: 3
Size: 280741 Color: 2

Bin 310: 6 of cap free
Amount of items: 2
Items: 
Size: 738082 Color: 0
Size: 261913 Color: 2

Bin 311: 6 of cap free
Amount of items: 2
Items: 
Size: 745555 Color: 0
Size: 254440 Color: 3

Bin 312: 6 of cap free
Amount of items: 2
Items: 
Size: 754323 Color: 3
Size: 245672 Color: 0

Bin 313: 6 of cap free
Amount of items: 2
Items: 
Size: 767457 Color: 4
Size: 232538 Color: 3

Bin 314: 6 of cap free
Amount of items: 2
Items: 
Size: 776406 Color: 2
Size: 223589 Color: 1

Bin 315: 6 of cap free
Amount of items: 2
Items: 
Size: 780310 Color: 4
Size: 219685 Color: 3

Bin 316: 6 of cap free
Amount of items: 2
Items: 
Size: 791528 Color: 1
Size: 208467 Color: 3

Bin 317: 6 of cap free
Amount of items: 2
Items: 
Size: 795214 Color: 2
Size: 204781 Color: 0

Bin 318: 7 of cap free
Amount of items: 2
Items: 
Size: 502041 Color: 4
Size: 497953 Color: 3

Bin 319: 7 of cap free
Amount of items: 2
Items: 
Size: 505271 Color: 0
Size: 494723 Color: 1

Bin 320: 7 of cap free
Amount of items: 2
Items: 
Size: 505796 Color: 4
Size: 494198 Color: 3

Bin 321: 7 of cap free
Amount of items: 2
Items: 
Size: 512312 Color: 1
Size: 487682 Color: 4

Bin 322: 7 of cap free
Amount of items: 4
Items: 
Size: 515109 Color: 3
Size: 189552 Color: 4
Size: 189435 Color: 0
Size: 105898 Color: 2

Bin 323: 7 of cap free
Amount of items: 2
Items: 
Size: 515445 Color: 3
Size: 484549 Color: 2

Bin 324: 7 of cap free
Amount of items: 2
Items: 
Size: 518437 Color: 2
Size: 481557 Color: 4

Bin 325: 7 of cap free
Amount of items: 2
Items: 
Size: 519320 Color: 4
Size: 480674 Color: 1

Bin 326: 7 of cap free
Amount of items: 2
Items: 
Size: 522870 Color: 1
Size: 477124 Color: 3

Bin 327: 7 of cap free
Amount of items: 2
Items: 
Size: 540613 Color: 4
Size: 459381 Color: 1

Bin 328: 7 of cap free
Amount of items: 2
Items: 
Size: 542485 Color: 4
Size: 457509 Color: 3

Bin 329: 7 of cap free
Amount of items: 2
Items: 
Size: 548177 Color: 1
Size: 451817 Color: 2

Bin 330: 7 of cap free
Amount of items: 2
Items: 
Size: 560807 Color: 3
Size: 439187 Color: 2

Bin 331: 7 of cap free
Amount of items: 2
Items: 
Size: 565825 Color: 0
Size: 434169 Color: 3

Bin 332: 7 of cap free
Amount of items: 2
Items: 
Size: 571699 Color: 3
Size: 428295 Color: 4

Bin 333: 7 of cap free
Amount of items: 2
Items: 
Size: 571901 Color: 4
Size: 428093 Color: 1

Bin 334: 7 of cap free
Amount of items: 2
Items: 
Size: 582772 Color: 0
Size: 417222 Color: 1

Bin 335: 7 of cap free
Amount of items: 2
Items: 
Size: 586405 Color: 2
Size: 413589 Color: 3

Bin 336: 7 of cap free
Amount of items: 2
Items: 
Size: 592254 Color: 4
Size: 407740 Color: 3

Bin 337: 7 of cap free
Amount of items: 2
Items: 
Size: 596053 Color: 4
Size: 403941 Color: 2

Bin 338: 7 of cap free
Amount of items: 2
Items: 
Size: 610524 Color: 3
Size: 389470 Color: 0

Bin 339: 7 of cap free
Amount of items: 2
Items: 
Size: 612365 Color: 2
Size: 387629 Color: 1

Bin 340: 7 of cap free
Amount of items: 2
Items: 
Size: 614672 Color: 3
Size: 385322 Color: 1

Bin 341: 7 of cap free
Amount of items: 2
Items: 
Size: 620233 Color: 1
Size: 379761 Color: 4

Bin 342: 7 of cap free
Amount of items: 2
Items: 
Size: 630318 Color: 2
Size: 369676 Color: 4

Bin 343: 7 of cap free
Amount of items: 2
Items: 
Size: 636347 Color: 4
Size: 363647 Color: 2

Bin 344: 7 of cap free
Amount of items: 2
Items: 
Size: 636758 Color: 0
Size: 363236 Color: 1

Bin 345: 7 of cap free
Amount of items: 2
Items: 
Size: 645247 Color: 1
Size: 354747 Color: 2

Bin 346: 7 of cap free
Amount of items: 2
Items: 
Size: 666039 Color: 3
Size: 333955 Color: 4

Bin 347: 7 of cap free
Amount of items: 2
Items: 
Size: 669984 Color: 0
Size: 330010 Color: 2

Bin 348: 7 of cap free
Amount of items: 2
Items: 
Size: 670127 Color: 1
Size: 329867 Color: 3

Bin 349: 7 of cap free
Amount of items: 2
Items: 
Size: 680207 Color: 0
Size: 319787 Color: 3

Bin 350: 7 of cap free
Amount of items: 2
Items: 
Size: 684675 Color: 3
Size: 315319 Color: 1

Bin 351: 7 of cap free
Amount of items: 2
Items: 
Size: 686370 Color: 3
Size: 313624 Color: 0

Bin 352: 7 of cap free
Amount of items: 2
Items: 
Size: 690220 Color: 4
Size: 309774 Color: 1

Bin 353: 7 of cap free
Amount of items: 2
Items: 
Size: 709059 Color: 2
Size: 290935 Color: 1

Bin 354: 7 of cap free
Amount of items: 2
Items: 
Size: 715887 Color: 4
Size: 284107 Color: 3

Bin 355: 7 of cap free
Amount of items: 2
Items: 
Size: 721463 Color: 1
Size: 278531 Color: 4

Bin 356: 7 of cap free
Amount of items: 2
Items: 
Size: 725446 Color: 0
Size: 274548 Color: 3

Bin 357: 7 of cap free
Amount of items: 2
Items: 
Size: 729873 Color: 1
Size: 270121 Color: 0

Bin 358: 7 of cap free
Amount of items: 2
Items: 
Size: 734101 Color: 2
Size: 265893 Color: 1

Bin 359: 7 of cap free
Amount of items: 2
Items: 
Size: 740618 Color: 2
Size: 259376 Color: 4

Bin 360: 7 of cap free
Amount of items: 2
Items: 
Size: 750205 Color: 2
Size: 249789 Color: 1

Bin 361: 7 of cap free
Amount of items: 2
Items: 
Size: 759460 Color: 3
Size: 240534 Color: 1

Bin 362: 7 of cap free
Amount of items: 2
Items: 
Size: 765649 Color: 2
Size: 234345 Color: 4

Bin 363: 7 of cap free
Amount of items: 2
Items: 
Size: 767793 Color: 1
Size: 232201 Color: 4

Bin 364: 7 of cap free
Amount of items: 2
Items: 
Size: 768036 Color: 4
Size: 231958 Color: 1

Bin 365: 7 of cap free
Amount of items: 2
Items: 
Size: 769892 Color: 1
Size: 230102 Color: 4

Bin 366: 7 of cap free
Amount of items: 2
Items: 
Size: 775783 Color: 3
Size: 224211 Color: 4

Bin 367: 8 of cap free
Amount of items: 4
Items: 
Size: 506338 Color: 4
Size: 188799 Color: 0
Size: 188604 Color: 4
Size: 116252 Color: 1

Bin 368: 8 of cap free
Amount of items: 2
Items: 
Size: 522892 Color: 2
Size: 477101 Color: 0

Bin 369: 8 of cap free
Amount of items: 2
Items: 
Size: 529383 Color: 1
Size: 470610 Color: 4

Bin 370: 8 of cap free
Amount of items: 2
Items: 
Size: 536890 Color: 2
Size: 463103 Color: 3

Bin 371: 8 of cap free
Amount of items: 2
Items: 
Size: 539418 Color: 2
Size: 460575 Color: 1

Bin 372: 8 of cap free
Amount of items: 2
Items: 
Size: 554550 Color: 2
Size: 445443 Color: 1

Bin 373: 8 of cap free
Amount of items: 2
Items: 
Size: 556055 Color: 1
Size: 443938 Color: 0

Bin 374: 8 of cap free
Amount of items: 2
Items: 
Size: 566416 Color: 3
Size: 433577 Color: 1

Bin 375: 8 of cap free
Amount of items: 2
Items: 
Size: 571548 Color: 3
Size: 428445 Color: 2

Bin 376: 8 of cap free
Amount of items: 2
Items: 
Size: 585206 Color: 3
Size: 414787 Color: 2

Bin 377: 8 of cap free
Amount of items: 2
Items: 
Size: 596456 Color: 4
Size: 403537 Color: 3

Bin 378: 8 of cap free
Amount of items: 2
Items: 
Size: 601366 Color: 2
Size: 398627 Color: 0

Bin 379: 8 of cap free
Amount of items: 2
Items: 
Size: 604028 Color: 2
Size: 395965 Color: 4

Bin 380: 8 of cap free
Amount of items: 2
Items: 
Size: 612504 Color: 1
Size: 387489 Color: 4

Bin 381: 8 of cap free
Amount of items: 2
Items: 
Size: 627131 Color: 2
Size: 372862 Color: 0

Bin 382: 8 of cap free
Amount of items: 2
Items: 
Size: 632565 Color: 3
Size: 367428 Color: 0

Bin 383: 8 of cap free
Amount of items: 2
Items: 
Size: 650858 Color: 3
Size: 349135 Color: 1

Bin 384: 8 of cap free
Amount of items: 2
Items: 
Size: 651694 Color: 0
Size: 348299 Color: 4

Bin 385: 8 of cap free
Amount of items: 2
Items: 
Size: 663228 Color: 4
Size: 336765 Color: 3

Bin 386: 8 of cap free
Amount of items: 2
Items: 
Size: 663727 Color: 3
Size: 336266 Color: 1

Bin 387: 8 of cap free
Amount of items: 2
Items: 
Size: 685171 Color: 3
Size: 314822 Color: 4

Bin 388: 8 of cap free
Amount of items: 2
Items: 
Size: 701878 Color: 4
Size: 298115 Color: 2

Bin 389: 8 of cap free
Amount of items: 2
Items: 
Size: 743805 Color: 2
Size: 256188 Color: 4

Bin 390: 8 of cap free
Amount of items: 2
Items: 
Size: 744098 Color: 2
Size: 255895 Color: 0

Bin 391: 8 of cap free
Amount of items: 2
Items: 
Size: 762061 Color: 0
Size: 237932 Color: 4

Bin 392: 8 of cap free
Amount of items: 2
Items: 
Size: 762916 Color: 3
Size: 237077 Color: 1

Bin 393: 8 of cap free
Amount of items: 2
Items: 
Size: 783616 Color: 2
Size: 216377 Color: 4

Bin 394: 8 of cap free
Amount of items: 2
Items: 
Size: 786620 Color: 2
Size: 213373 Color: 0

Bin 395: 8 of cap free
Amount of items: 2
Items: 
Size: 795918 Color: 3
Size: 204075 Color: 0

Bin 396: 8 of cap free
Amount of items: 2
Items: 
Size: 798369 Color: 3
Size: 201624 Color: 2

Bin 397: 9 of cap free
Amount of items: 7
Items: 
Size: 144767 Color: 4
Size: 144711 Color: 2
Size: 144661 Color: 3
Size: 144624 Color: 4
Size: 144580 Color: 3
Size: 144537 Color: 2
Size: 132112 Color: 4

Bin 398: 9 of cap free
Amount of items: 4
Items: 
Size: 516586 Color: 3
Size: 190748 Color: 4
Size: 190722 Color: 4
Size: 101936 Color: 3

Bin 399: 9 of cap free
Amount of items: 2
Items: 
Size: 526003 Color: 3
Size: 473989 Color: 1

Bin 400: 9 of cap free
Amount of items: 2
Items: 
Size: 533348 Color: 2
Size: 466644 Color: 1

Bin 401: 9 of cap free
Amount of items: 2
Items: 
Size: 540504 Color: 2
Size: 459488 Color: 4

Bin 402: 9 of cap free
Amount of items: 2
Items: 
Size: 559394 Color: 4
Size: 440598 Color: 3

Bin 403: 9 of cap free
Amount of items: 2
Items: 
Size: 564074 Color: 2
Size: 435918 Color: 3

Bin 404: 9 of cap free
Amount of items: 2
Items: 
Size: 569875 Color: 1
Size: 430117 Color: 3

Bin 405: 9 of cap free
Amount of items: 2
Items: 
Size: 573886 Color: 3
Size: 426106 Color: 2

Bin 406: 9 of cap free
Amount of items: 2
Items: 
Size: 576052 Color: 2
Size: 423940 Color: 4

Bin 407: 9 of cap free
Amount of items: 2
Items: 
Size: 606221 Color: 4
Size: 393771 Color: 2

Bin 408: 9 of cap free
Amount of items: 2
Items: 
Size: 611813 Color: 0
Size: 388179 Color: 3

Bin 409: 9 of cap free
Amount of items: 2
Items: 
Size: 612300 Color: 0
Size: 387692 Color: 2

Bin 410: 9 of cap free
Amount of items: 2
Items: 
Size: 617511 Color: 3
Size: 382481 Color: 0

Bin 411: 9 of cap free
Amount of items: 2
Items: 
Size: 619932 Color: 3
Size: 380060 Color: 2

Bin 412: 9 of cap free
Amount of items: 2
Items: 
Size: 631026 Color: 2
Size: 368966 Color: 1

Bin 413: 9 of cap free
Amount of items: 2
Items: 
Size: 631981 Color: 1
Size: 368011 Color: 3

Bin 414: 9 of cap free
Amount of items: 2
Items: 
Size: 632829 Color: 4
Size: 367163 Color: 0

Bin 415: 9 of cap free
Amount of items: 2
Items: 
Size: 640939 Color: 4
Size: 359053 Color: 2

Bin 416: 9 of cap free
Amount of items: 2
Items: 
Size: 655391 Color: 0
Size: 344601 Color: 2

Bin 417: 9 of cap free
Amount of items: 2
Items: 
Size: 699134 Color: 4
Size: 300858 Color: 2

Bin 418: 9 of cap free
Amount of items: 2
Items: 
Size: 706635 Color: 0
Size: 293357 Color: 2

Bin 419: 9 of cap free
Amount of items: 2
Items: 
Size: 713773 Color: 4
Size: 286219 Color: 0

Bin 420: 9 of cap free
Amount of items: 2
Items: 
Size: 716282 Color: 0
Size: 283710 Color: 2

Bin 421: 9 of cap free
Amount of items: 2
Items: 
Size: 719031 Color: 1
Size: 280961 Color: 0

Bin 422: 9 of cap free
Amount of items: 2
Items: 
Size: 727568 Color: 2
Size: 272424 Color: 3

Bin 423: 9 of cap free
Amount of items: 2
Items: 
Size: 733006 Color: 0
Size: 266986 Color: 2

Bin 424: 9 of cap free
Amount of items: 2
Items: 
Size: 737117 Color: 0
Size: 262875 Color: 2

Bin 425: 9 of cap free
Amount of items: 2
Items: 
Size: 746187 Color: 0
Size: 253805 Color: 3

Bin 426: 9 of cap free
Amount of items: 2
Items: 
Size: 751136 Color: 2
Size: 248856 Color: 1

Bin 427: 9 of cap free
Amount of items: 2
Items: 
Size: 757668 Color: 2
Size: 242324 Color: 1

Bin 428: 9 of cap free
Amount of items: 2
Items: 
Size: 759384 Color: 3
Size: 240608 Color: 2

Bin 429: 9 of cap free
Amount of items: 2
Items: 
Size: 761123 Color: 1
Size: 238869 Color: 4

Bin 430: 9 of cap free
Amount of items: 2
Items: 
Size: 765379 Color: 3
Size: 234613 Color: 4

Bin 431: 9 of cap free
Amount of items: 2
Items: 
Size: 765457 Color: 0
Size: 234535 Color: 4

Bin 432: 9 of cap free
Amount of items: 2
Items: 
Size: 766760 Color: 4
Size: 233232 Color: 1

Bin 433: 9 of cap free
Amount of items: 2
Items: 
Size: 772639 Color: 2
Size: 227353 Color: 1

Bin 434: 9 of cap free
Amount of items: 2
Items: 
Size: 775153 Color: 4
Size: 224839 Color: 3

Bin 435: 9 of cap free
Amount of items: 2
Items: 
Size: 776650 Color: 4
Size: 223342 Color: 3

Bin 436: 9 of cap free
Amount of items: 2
Items: 
Size: 778170 Color: 4
Size: 221822 Color: 2

Bin 437: 9 of cap free
Amount of items: 2
Items: 
Size: 781070 Color: 2
Size: 218922 Color: 3

Bin 438: 10 of cap free
Amount of items: 6
Items: 
Size: 179899 Color: 3
Size: 179753 Color: 0
Size: 179728 Color: 2
Size: 179492 Color: 4
Size: 179472 Color: 0
Size: 101647 Color: 4

Bin 439: 10 of cap free
Amount of items: 2
Items: 
Size: 505965 Color: 4
Size: 494026 Color: 2

Bin 440: 10 of cap free
Amount of items: 2
Items: 
Size: 507967 Color: 1
Size: 492024 Color: 4

Bin 441: 10 of cap free
Amount of items: 2
Items: 
Size: 510719 Color: 0
Size: 489272 Color: 3

Bin 442: 10 of cap free
Amount of items: 2
Items: 
Size: 511036 Color: 1
Size: 488955 Color: 3

Bin 443: 10 of cap free
Amount of items: 2
Items: 
Size: 513258 Color: 1
Size: 486733 Color: 0

Bin 444: 10 of cap free
Amount of items: 2
Items: 
Size: 519210 Color: 2
Size: 480781 Color: 3

Bin 445: 10 of cap free
Amount of items: 2
Items: 
Size: 519512 Color: 0
Size: 480479 Color: 1

Bin 446: 10 of cap free
Amount of items: 2
Items: 
Size: 534575 Color: 3
Size: 465416 Color: 2

Bin 447: 10 of cap free
Amount of items: 2
Items: 
Size: 535035 Color: 4
Size: 464956 Color: 0

Bin 448: 10 of cap free
Amount of items: 2
Items: 
Size: 536433 Color: 4
Size: 463558 Color: 3

Bin 449: 10 of cap free
Amount of items: 2
Items: 
Size: 536656 Color: 3
Size: 463335 Color: 2

Bin 450: 10 of cap free
Amount of items: 2
Items: 
Size: 538912 Color: 0
Size: 461079 Color: 1

Bin 451: 10 of cap free
Amount of items: 2
Items: 
Size: 547754 Color: 0
Size: 452237 Color: 1

Bin 452: 10 of cap free
Amount of items: 2
Items: 
Size: 557562 Color: 4
Size: 442429 Color: 3

Bin 453: 10 of cap free
Amount of items: 2
Items: 
Size: 571594 Color: 1
Size: 428397 Color: 3

Bin 454: 10 of cap free
Amount of items: 2
Items: 
Size: 583168 Color: 4
Size: 416823 Color: 1

Bin 455: 10 of cap free
Amount of items: 2
Items: 
Size: 588526 Color: 4
Size: 411465 Color: 2

Bin 456: 10 of cap free
Amount of items: 2
Items: 
Size: 596941 Color: 4
Size: 403050 Color: 0

Bin 457: 10 of cap free
Amount of items: 2
Items: 
Size: 617644 Color: 2
Size: 382347 Color: 0

Bin 458: 10 of cap free
Amount of items: 2
Items: 
Size: 640302 Color: 3
Size: 359689 Color: 4

Bin 459: 10 of cap free
Amount of items: 2
Items: 
Size: 643406 Color: 0
Size: 356585 Color: 3

Bin 460: 10 of cap free
Amount of items: 2
Items: 
Size: 655259 Color: 2
Size: 344732 Color: 0

Bin 461: 10 of cap free
Amount of items: 2
Items: 
Size: 667182 Color: 4
Size: 332809 Color: 0

Bin 462: 10 of cap free
Amount of items: 2
Items: 
Size: 669840 Color: 0
Size: 330151 Color: 2

Bin 463: 10 of cap free
Amount of items: 2
Items: 
Size: 673876 Color: 0
Size: 326115 Color: 4

Bin 464: 10 of cap free
Amount of items: 2
Items: 
Size: 676870 Color: 4
Size: 323121 Color: 2

Bin 465: 10 of cap free
Amount of items: 2
Items: 
Size: 678066 Color: 0
Size: 321925 Color: 3

Bin 466: 10 of cap free
Amount of items: 2
Items: 
Size: 691593 Color: 4
Size: 308398 Color: 1

Bin 467: 10 of cap free
Amount of items: 2
Items: 
Size: 705789 Color: 3
Size: 294202 Color: 0

Bin 468: 10 of cap free
Amount of items: 2
Items: 
Size: 713447 Color: 1
Size: 286544 Color: 0

Bin 469: 10 of cap free
Amount of items: 2
Items: 
Size: 714102 Color: 0
Size: 285889 Color: 2

Bin 470: 10 of cap free
Amount of items: 2
Items: 
Size: 715862 Color: 4
Size: 284129 Color: 0

Bin 471: 10 of cap free
Amount of items: 2
Items: 
Size: 718084 Color: 1
Size: 281907 Color: 4

Bin 472: 10 of cap free
Amount of items: 2
Items: 
Size: 723002 Color: 3
Size: 276989 Color: 2

Bin 473: 10 of cap free
Amount of items: 2
Items: 
Size: 729662 Color: 2
Size: 270329 Color: 4

Bin 474: 10 of cap free
Amount of items: 2
Items: 
Size: 730864 Color: 2
Size: 269127 Color: 0

Bin 475: 10 of cap free
Amount of items: 2
Items: 
Size: 739629 Color: 2
Size: 260362 Color: 4

Bin 476: 10 of cap free
Amount of items: 2
Items: 
Size: 741770 Color: 4
Size: 258221 Color: 3

Bin 477: 10 of cap free
Amount of items: 2
Items: 
Size: 750745 Color: 1
Size: 249246 Color: 2

Bin 478: 10 of cap free
Amount of items: 2
Items: 
Size: 755194 Color: 4
Size: 244797 Color: 2

Bin 479: 10 of cap free
Amount of items: 2
Items: 
Size: 757783 Color: 2
Size: 242208 Color: 3

Bin 480: 10 of cap free
Amount of items: 2
Items: 
Size: 762795 Color: 4
Size: 237196 Color: 3

Bin 481: 10 of cap free
Amount of items: 2
Items: 
Size: 765715 Color: 3
Size: 234276 Color: 0

Bin 482: 10 of cap free
Amount of items: 2
Items: 
Size: 774036 Color: 4
Size: 225955 Color: 0

Bin 483: 10 of cap free
Amount of items: 2
Items: 
Size: 777115 Color: 3
Size: 222876 Color: 2

Bin 484: 10 of cap free
Amount of items: 2
Items: 
Size: 787556 Color: 0
Size: 212435 Color: 2

Bin 485: 10 of cap free
Amount of items: 2
Items: 
Size: 793628 Color: 0
Size: 206363 Color: 4

Bin 486: 10 of cap free
Amount of items: 2
Items: 
Size: 799708 Color: 0
Size: 200283 Color: 2

Bin 487: 11 of cap free
Amount of items: 2
Items: 
Size: 510456 Color: 2
Size: 489534 Color: 0

Bin 488: 11 of cap free
Amount of items: 2
Items: 
Size: 515067 Color: 2
Size: 484923 Color: 3

Bin 489: 11 of cap free
Amount of items: 2
Items: 
Size: 521104 Color: 1
Size: 478886 Color: 2

Bin 490: 11 of cap free
Amount of items: 2
Items: 
Size: 531970 Color: 0
Size: 468020 Color: 4

Bin 491: 11 of cap free
Amount of items: 2
Items: 
Size: 553228 Color: 2
Size: 446762 Color: 3

Bin 492: 11 of cap free
Amount of items: 2
Items: 
Size: 574189 Color: 4
Size: 425801 Color: 3

Bin 493: 11 of cap free
Amount of items: 2
Items: 
Size: 578324 Color: 4
Size: 421666 Color: 1

Bin 494: 11 of cap free
Amount of items: 2
Items: 
Size: 581677 Color: 1
Size: 418313 Color: 4

Bin 495: 11 of cap free
Amount of items: 2
Items: 
Size: 581801 Color: 0
Size: 418189 Color: 4

Bin 496: 11 of cap free
Amount of items: 2
Items: 
Size: 587033 Color: 2
Size: 412957 Color: 3

Bin 497: 11 of cap free
Amount of items: 2
Items: 
Size: 591300 Color: 2
Size: 408690 Color: 3

Bin 498: 11 of cap free
Amount of items: 2
Items: 
Size: 594623 Color: 0
Size: 405367 Color: 1

Bin 499: 11 of cap free
Amount of items: 2
Items: 
Size: 601313 Color: 2
Size: 398677 Color: 3

Bin 500: 11 of cap free
Amount of items: 3
Items: 
Size: 619455 Color: 0
Size: 195624 Color: 2
Size: 184911 Color: 0

Bin 501: 11 of cap free
Amount of items: 2
Items: 
Size: 626230 Color: 0
Size: 373760 Color: 3

Bin 502: 11 of cap free
Amount of items: 2
Items: 
Size: 661047 Color: 4
Size: 338943 Color: 0

Bin 503: 11 of cap free
Amount of items: 2
Items: 
Size: 661394 Color: 4
Size: 338596 Color: 3

Bin 504: 11 of cap free
Amount of items: 2
Items: 
Size: 671883 Color: 2
Size: 328107 Color: 4

Bin 505: 11 of cap free
Amount of items: 2
Items: 
Size: 677026 Color: 3
Size: 322964 Color: 0

Bin 506: 11 of cap free
Amount of items: 2
Items: 
Size: 687697 Color: 4
Size: 312293 Color: 2

Bin 507: 11 of cap free
Amount of items: 2
Items: 
Size: 692791 Color: 2
Size: 307199 Color: 4

Bin 508: 11 of cap free
Amount of items: 2
Items: 
Size: 695436 Color: 2
Size: 304554 Color: 1

Bin 509: 11 of cap free
Amount of items: 2
Items: 
Size: 697205 Color: 0
Size: 302785 Color: 1

Bin 510: 11 of cap free
Amount of items: 2
Items: 
Size: 722949 Color: 2
Size: 277041 Color: 1

Bin 511: 11 of cap free
Amount of items: 2
Items: 
Size: 729549 Color: 3
Size: 270441 Color: 1

Bin 512: 11 of cap free
Amount of items: 2
Items: 
Size: 734337 Color: 2
Size: 265653 Color: 0

Bin 513: 11 of cap free
Amount of items: 2
Items: 
Size: 752704 Color: 0
Size: 247286 Color: 2

Bin 514: 11 of cap free
Amount of items: 2
Items: 
Size: 763228 Color: 4
Size: 236762 Color: 3

Bin 515: 11 of cap free
Amount of items: 2
Items: 
Size: 764968 Color: 3
Size: 235022 Color: 4

Bin 516: 11 of cap free
Amount of items: 2
Items: 
Size: 795376 Color: 3
Size: 204614 Color: 2

Bin 517: 12 of cap free
Amount of items: 6
Items: 
Size: 177404 Color: 1
Size: 177319 Color: 0
Size: 177292 Color: 1
Size: 177277 Color: 0
Size: 177264 Color: 4
Size: 113433 Color: 0

Bin 518: 12 of cap free
Amount of items: 2
Items: 
Size: 503627 Color: 2
Size: 496362 Color: 3

Bin 519: 12 of cap free
Amount of items: 2
Items: 
Size: 518266 Color: 2
Size: 481723 Color: 3

Bin 520: 12 of cap free
Amount of items: 2
Items: 
Size: 518972 Color: 1
Size: 481017 Color: 0

Bin 521: 12 of cap free
Amount of items: 2
Items: 
Size: 525823 Color: 2
Size: 474166 Color: 1

Bin 522: 12 of cap free
Amount of items: 2
Items: 
Size: 530249 Color: 4
Size: 469740 Color: 0

Bin 523: 12 of cap free
Amount of items: 2
Items: 
Size: 540693 Color: 0
Size: 459296 Color: 2

Bin 524: 12 of cap free
Amount of items: 2
Items: 
Size: 549975 Color: 0
Size: 450014 Color: 4

Bin 525: 12 of cap free
Amount of items: 2
Items: 
Size: 606745 Color: 0
Size: 393244 Color: 4

Bin 526: 12 of cap free
Amount of items: 2
Items: 
Size: 610032 Color: 0
Size: 389957 Color: 4

Bin 527: 12 of cap free
Amount of items: 2
Items: 
Size: 615822 Color: 3
Size: 384167 Color: 4

Bin 528: 12 of cap free
Amount of items: 2
Items: 
Size: 627742 Color: 3
Size: 372247 Color: 4

Bin 529: 12 of cap free
Amount of items: 2
Items: 
Size: 628867 Color: 4
Size: 371122 Color: 3

Bin 530: 12 of cap free
Amount of items: 2
Items: 
Size: 659257 Color: 4
Size: 340732 Color: 3

Bin 531: 12 of cap free
Amount of items: 2
Items: 
Size: 666541 Color: 3
Size: 333448 Color: 4

Bin 532: 12 of cap free
Amount of items: 2
Items: 
Size: 676006 Color: 4
Size: 323983 Color: 0

Bin 533: 12 of cap free
Amount of items: 2
Items: 
Size: 678936 Color: 4
Size: 321053 Color: 0

Bin 534: 12 of cap free
Amount of items: 2
Items: 
Size: 680679 Color: 3
Size: 319310 Color: 1

Bin 535: 12 of cap free
Amount of items: 2
Items: 
Size: 685142 Color: 3
Size: 314847 Color: 0

Bin 536: 12 of cap free
Amount of items: 2
Items: 
Size: 688310 Color: 0
Size: 311679 Color: 2

Bin 537: 12 of cap free
Amount of items: 2
Items: 
Size: 690035 Color: 3
Size: 309954 Color: 2

Bin 538: 12 of cap free
Amount of items: 2
Items: 
Size: 704526 Color: 0
Size: 295463 Color: 1

Bin 539: 12 of cap free
Amount of items: 2
Items: 
Size: 710451 Color: 4
Size: 289538 Color: 2

Bin 540: 12 of cap free
Amount of items: 2
Items: 
Size: 712808 Color: 1
Size: 287181 Color: 3

Bin 541: 12 of cap free
Amount of items: 2
Items: 
Size: 719779 Color: 0
Size: 280210 Color: 3

Bin 542: 12 of cap free
Amount of items: 2
Items: 
Size: 736541 Color: 0
Size: 263448 Color: 1

Bin 543: 12 of cap free
Amount of items: 2
Items: 
Size: 743342 Color: 0
Size: 256647 Color: 3

Bin 544: 12 of cap free
Amount of items: 2
Items: 
Size: 750416 Color: 0
Size: 249573 Color: 2

Bin 545: 12 of cap free
Amount of items: 2
Items: 
Size: 751415 Color: 2
Size: 248574 Color: 1

Bin 546: 12 of cap free
Amount of items: 2
Items: 
Size: 755779 Color: 2
Size: 244210 Color: 4

Bin 547: 12 of cap free
Amount of items: 2
Items: 
Size: 761030 Color: 3
Size: 238959 Color: 0

Bin 548: 12 of cap free
Amount of items: 2
Items: 
Size: 766372 Color: 4
Size: 233617 Color: 2

Bin 549: 12 of cap free
Amount of items: 2
Items: 
Size: 770031 Color: 0
Size: 229958 Color: 4

Bin 550: 12 of cap free
Amount of items: 2
Items: 
Size: 773726 Color: 2
Size: 226263 Color: 4

Bin 551: 12 of cap free
Amount of items: 2
Items: 
Size: 783055 Color: 2
Size: 216934 Color: 1

Bin 552: 12 of cap free
Amount of items: 2
Items: 
Size: 785109 Color: 4
Size: 214880 Color: 2

Bin 553: 12 of cap free
Amount of items: 2
Items: 
Size: 789724 Color: 3
Size: 210265 Color: 1

Bin 554: 12 of cap free
Amount of items: 2
Items: 
Size: 790320 Color: 4
Size: 209669 Color: 2

Bin 555: 12 of cap free
Amount of items: 2
Items: 
Size: 797477 Color: 2
Size: 202512 Color: 0

Bin 556: 13 of cap free
Amount of items: 8
Items: 
Size: 125929 Color: 3
Size: 125892 Color: 3
Size: 125856 Color: 1
Size: 125816 Color: 1
Size: 125658 Color: 2
Size: 125546 Color: 0
Size: 125397 Color: 4
Size: 119894 Color: 0

Bin 557: 13 of cap free
Amount of items: 2
Items: 
Size: 505860 Color: 3
Size: 494128 Color: 0

Bin 558: 13 of cap free
Amount of items: 2
Items: 
Size: 507227 Color: 4
Size: 492761 Color: 2

Bin 559: 13 of cap free
Amount of items: 2
Items: 
Size: 532642 Color: 0
Size: 467346 Color: 4

Bin 560: 13 of cap free
Amount of items: 2
Items: 
Size: 542561 Color: 4
Size: 457427 Color: 0

Bin 561: 13 of cap free
Amount of items: 2
Items: 
Size: 548370 Color: 0
Size: 451618 Color: 2

Bin 562: 13 of cap free
Amount of items: 2
Items: 
Size: 560167 Color: 3
Size: 439821 Color: 1

Bin 563: 13 of cap free
Amount of items: 2
Items: 
Size: 562726 Color: 4
Size: 437262 Color: 0

Bin 564: 13 of cap free
Amount of items: 2
Items: 
Size: 567631 Color: 0
Size: 432357 Color: 3

Bin 565: 13 of cap free
Amount of items: 2
Items: 
Size: 568544 Color: 0
Size: 431444 Color: 2

Bin 566: 13 of cap free
Amount of items: 2
Items: 
Size: 572194 Color: 3
Size: 427794 Color: 4

Bin 567: 13 of cap free
Amount of items: 2
Items: 
Size: 578176 Color: 0
Size: 421812 Color: 1

Bin 568: 13 of cap free
Amount of items: 2
Items: 
Size: 593002 Color: 1
Size: 406986 Color: 0

Bin 569: 13 of cap free
Amount of items: 2
Items: 
Size: 599207 Color: 3
Size: 400781 Color: 2

Bin 570: 13 of cap free
Amount of items: 2
Items: 
Size: 601002 Color: 2
Size: 398986 Color: 1

Bin 571: 13 of cap free
Amount of items: 2
Items: 
Size: 608033 Color: 0
Size: 391955 Color: 1

Bin 572: 13 of cap free
Amount of items: 2
Items: 
Size: 609486 Color: 2
Size: 390502 Color: 1

Bin 573: 13 of cap free
Amount of items: 2
Items: 
Size: 615960 Color: 0
Size: 384028 Color: 1

Bin 574: 13 of cap free
Amount of items: 2
Items: 
Size: 628755 Color: 3
Size: 371233 Color: 1

Bin 575: 13 of cap free
Amount of items: 2
Items: 
Size: 634399 Color: 1
Size: 365589 Color: 0

Bin 576: 13 of cap free
Amount of items: 2
Items: 
Size: 636546 Color: 3
Size: 363442 Color: 0

Bin 577: 13 of cap free
Amount of items: 2
Items: 
Size: 642328 Color: 2
Size: 357660 Color: 0

Bin 578: 13 of cap free
Amount of items: 2
Items: 
Size: 642561 Color: 2
Size: 357427 Color: 0

Bin 579: 13 of cap free
Amount of items: 2
Items: 
Size: 643141 Color: 1
Size: 356847 Color: 0

Bin 580: 13 of cap free
Amount of items: 2
Items: 
Size: 643813 Color: 1
Size: 356175 Color: 0

Bin 581: 13 of cap free
Amount of items: 2
Items: 
Size: 647003 Color: 4
Size: 352985 Color: 3

Bin 582: 13 of cap free
Amount of items: 2
Items: 
Size: 649970 Color: 2
Size: 350018 Color: 3

Bin 583: 13 of cap free
Amount of items: 2
Items: 
Size: 651000 Color: 1
Size: 348988 Color: 0

Bin 584: 13 of cap free
Amount of items: 2
Items: 
Size: 653872 Color: 3
Size: 346116 Color: 1

Bin 585: 13 of cap free
Amount of items: 2
Items: 
Size: 680019 Color: 1
Size: 319969 Color: 0

Bin 586: 13 of cap free
Amount of items: 2
Items: 
Size: 691583 Color: 2
Size: 308405 Color: 4

Bin 587: 13 of cap free
Amount of items: 2
Items: 
Size: 698182 Color: 4
Size: 301806 Color: 1

Bin 588: 13 of cap free
Amount of items: 2
Items: 
Size: 712663 Color: 4
Size: 287325 Color: 2

Bin 589: 13 of cap free
Amount of items: 2
Items: 
Size: 723661 Color: 2
Size: 276327 Color: 1

Bin 590: 13 of cap free
Amount of items: 2
Items: 
Size: 733654 Color: 3
Size: 266334 Color: 0

Bin 591: 13 of cap free
Amount of items: 2
Items: 
Size: 746599 Color: 0
Size: 253389 Color: 4

Bin 592: 13 of cap free
Amount of items: 2
Items: 
Size: 759928 Color: 3
Size: 240060 Color: 1

Bin 593: 13 of cap free
Amount of items: 2
Items: 
Size: 761187 Color: 0
Size: 238801 Color: 4

Bin 594: 13 of cap free
Amount of items: 2
Items: 
Size: 764294 Color: 1
Size: 235694 Color: 2

Bin 595: 13 of cap free
Amount of items: 2
Items: 
Size: 788453 Color: 2
Size: 211535 Color: 0

Bin 596: 13 of cap free
Amount of items: 2
Items: 
Size: 793697 Color: 0
Size: 206291 Color: 3

Bin 597: 13 of cap free
Amount of items: 2
Items: 
Size: 799058 Color: 0
Size: 200930 Color: 2

Bin 598: 13 of cap free
Amount of items: 2
Items: 
Size: 799905 Color: 2
Size: 200083 Color: 1

Bin 599: 14 of cap free
Amount of items: 7
Items: 
Size: 144524 Color: 1
Size: 144496 Color: 0
Size: 144467 Color: 2
Size: 144442 Color: 4
Size: 144409 Color: 4
Size: 144198 Color: 2
Size: 133451 Color: 4

Bin 600: 14 of cap free
Amount of items: 7
Items: 
Size: 149266 Color: 1
Size: 149256 Color: 3
Size: 149229 Color: 1
Size: 149097 Color: 0
Size: 148922 Color: 0
Size: 148875 Color: 3
Size: 105342 Color: 4

Bin 601: 14 of cap free
Amount of items: 6
Items: 
Size: 175840 Color: 2
Size: 175824 Color: 4
Size: 175711 Color: 0
Size: 175690 Color: 1
Size: 175688 Color: 2
Size: 121234 Color: 3

Bin 602: 14 of cap free
Amount of items: 6
Items: 
Size: 176159 Color: 3
Size: 176152 Color: 2
Size: 176111 Color: 3
Size: 176011 Color: 0
Size: 175840 Color: 4
Size: 119714 Color: 0

Bin 603: 14 of cap free
Amount of items: 2
Items: 
Size: 510016 Color: 0
Size: 489971 Color: 4

Bin 604: 14 of cap free
Amount of items: 2
Items: 
Size: 515333 Color: 4
Size: 484654 Color: 3

Bin 605: 14 of cap free
Amount of items: 2
Items: 
Size: 524536 Color: 3
Size: 475451 Color: 2

Bin 606: 14 of cap free
Amount of items: 2
Items: 
Size: 527147 Color: 2
Size: 472840 Color: 3

Bin 607: 14 of cap free
Amount of items: 2
Items: 
Size: 537244 Color: 3
Size: 462743 Color: 0

Bin 608: 14 of cap free
Amount of items: 2
Items: 
Size: 547233 Color: 2
Size: 452754 Color: 0

Bin 609: 14 of cap free
Amount of items: 2
Items: 
Size: 547348 Color: 1
Size: 452639 Color: 2

Bin 610: 14 of cap free
Amount of items: 2
Items: 
Size: 554054 Color: 0
Size: 445933 Color: 1

Bin 611: 14 of cap free
Amount of items: 2
Items: 
Size: 566515 Color: 2
Size: 433472 Color: 0

Bin 612: 14 of cap free
Amount of items: 2
Items: 
Size: 576842 Color: 0
Size: 423145 Color: 2

Bin 613: 14 of cap free
Amount of items: 2
Items: 
Size: 582622 Color: 1
Size: 417365 Color: 3

Bin 614: 14 of cap free
Amount of items: 2
Items: 
Size: 595849 Color: 1
Size: 404138 Color: 4

Bin 615: 14 of cap free
Amount of items: 2
Items: 
Size: 600884 Color: 0
Size: 399103 Color: 1

Bin 616: 14 of cap free
Amount of items: 2
Items: 
Size: 604943 Color: 4
Size: 395044 Color: 0

Bin 617: 14 of cap free
Amount of items: 2
Items: 
Size: 618025 Color: 4
Size: 381962 Color: 3

Bin 618: 14 of cap free
Amount of items: 2
Items: 
Size: 621263 Color: 3
Size: 378724 Color: 0

Bin 619: 14 of cap free
Amount of items: 2
Items: 
Size: 631885 Color: 2
Size: 368102 Color: 0

Bin 620: 14 of cap free
Amount of items: 2
Items: 
Size: 640532 Color: 1
Size: 359455 Color: 3

Bin 621: 14 of cap free
Amount of items: 2
Items: 
Size: 652831 Color: 4
Size: 347156 Color: 3

Bin 622: 14 of cap free
Amount of items: 2
Items: 
Size: 686045 Color: 4
Size: 313942 Color: 0

Bin 623: 14 of cap free
Amount of items: 2
Items: 
Size: 695243 Color: 4
Size: 304744 Color: 1

Bin 624: 14 of cap free
Amount of items: 2
Items: 
Size: 716864 Color: 0
Size: 283123 Color: 4

Bin 625: 14 of cap free
Amount of items: 2
Items: 
Size: 725726 Color: 4
Size: 274261 Color: 1

Bin 626: 14 of cap free
Amount of items: 2
Items: 
Size: 760372 Color: 0
Size: 239615 Color: 1

Bin 627: 14 of cap free
Amount of items: 2
Items: 
Size: 761774 Color: 1
Size: 238213 Color: 3

Bin 628: 14 of cap free
Amount of items: 2
Items: 
Size: 773883 Color: 0
Size: 226104 Color: 4

Bin 629: 14 of cap free
Amount of items: 2
Items: 
Size: 776557 Color: 0
Size: 223430 Color: 3

Bin 630: 14 of cap free
Amount of items: 2
Items: 
Size: 781876 Color: 2
Size: 218111 Color: 0

Bin 631: 14 of cap free
Amount of items: 2
Items: 
Size: 787474 Color: 1
Size: 212513 Color: 0

Bin 632: 14 of cap free
Amount of items: 2
Items: 
Size: 795253 Color: 2
Size: 204734 Color: 1

Bin 633: 14 of cap free
Amount of items: 2
Items: 
Size: 797785 Color: 4
Size: 202202 Color: 1

Bin 634: 15 of cap free
Amount of items: 2
Items: 
Size: 502172 Color: 2
Size: 497814 Color: 0

Bin 635: 15 of cap free
Amount of items: 2
Items: 
Size: 541973 Color: 0
Size: 458013 Color: 3

Bin 636: 15 of cap free
Amount of items: 2
Items: 
Size: 542450 Color: 1
Size: 457536 Color: 3

Bin 637: 15 of cap free
Amount of items: 2
Items: 
Size: 546840 Color: 3
Size: 453146 Color: 0

Bin 638: 15 of cap free
Amount of items: 2
Items: 
Size: 565020 Color: 4
Size: 434966 Color: 3

Bin 639: 15 of cap free
Amount of items: 2
Items: 
Size: 582784 Color: 3
Size: 417202 Color: 2

Bin 640: 15 of cap free
Amount of items: 2
Items: 
Size: 587286 Color: 3
Size: 412700 Color: 4

Bin 641: 15 of cap free
Amount of items: 2
Items: 
Size: 598034 Color: 0
Size: 401952 Color: 1

Bin 642: 15 of cap free
Amount of items: 2
Items: 
Size: 599334 Color: 4
Size: 400652 Color: 3

Bin 643: 15 of cap free
Amount of items: 2
Items: 
Size: 603708 Color: 1
Size: 396278 Color: 3

Bin 644: 15 of cap free
Amount of items: 2
Items: 
Size: 609258 Color: 2
Size: 390728 Color: 3

Bin 645: 15 of cap free
Amount of items: 3
Items: 
Size: 619567 Color: 3
Size: 195780 Color: 4
Size: 184639 Color: 2

Bin 646: 15 of cap free
Amount of items: 2
Items: 
Size: 643222 Color: 0
Size: 356764 Color: 3

Bin 647: 15 of cap free
Amount of items: 2
Items: 
Size: 675941 Color: 3
Size: 324045 Color: 1

Bin 648: 15 of cap free
Amount of items: 2
Items: 
Size: 677743 Color: 3
Size: 322243 Color: 2

Bin 649: 15 of cap free
Amount of items: 2
Items: 
Size: 693414 Color: 2
Size: 306572 Color: 3

Bin 650: 15 of cap free
Amount of items: 2
Items: 
Size: 727206 Color: 0
Size: 272780 Color: 4

Bin 651: 15 of cap free
Amount of items: 2
Items: 
Size: 734717 Color: 2
Size: 265269 Color: 0

Bin 652: 15 of cap free
Amount of items: 2
Items: 
Size: 764409 Color: 2
Size: 235577 Color: 4

Bin 653: 15 of cap free
Amount of items: 2
Items: 
Size: 766260 Color: 0
Size: 233726 Color: 3

Bin 654: 15 of cap free
Amount of items: 2
Items: 
Size: 777537 Color: 4
Size: 222449 Color: 2

Bin 655: 15 of cap free
Amount of items: 2
Items: 
Size: 778888 Color: 4
Size: 221098 Color: 3

Bin 656: 15 of cap free
Amount of items: 2
Items: 
Size: 785930 Color: 2
Size: 214056 Color: 1

Bin 657: 16 of cap free
Amount of items: 2
Items: 
Size: 507162 Color: 0
Size: 492823 Color: 4

Bin 658: 16 of cap free
Amount of items: 2
Items: 
Size: 537119 Color: 3
Size: 462866 Color: 0

Bin 659: 16 of cap free
Amount of items: 2
Items: 
Size: 543054 Color: 4
Size: 456931 Color: 3

Bin 660: 16 of cap free
Amount of items: 2
Items: 
Size: 569571 Color: 4
Size: 430414 Color: 3

Bin 661: 16 of cap free
Amount of items: 2
Items: 
Size: 570646 Color: 0
Size: 429339 Color: 4

Bin 662: 16 of cap free
Amount of items: 2
Items: 
Size: 598281 Color: 0
Size: 401704 Color: 4

Bin 663: 16 of cap free
Amount of items: 2
Items: 
Size: 606654 Color: 2
Size: 393331 Color: 0

Bin 664: 16 of cap free
Amount of items: 2
Items: 
Size: 607791 Color: 3
Size: 392194 Color: 0

Bin 665: 16 of cap free
Amount of items: 2
Items: 
Size: 637378 Color: 3
Size: 362607 Color: 4

Bin 666: 16 of cap free
Amount of items: 2
Items: 
Size: 642935 Color: 2
Size: 357050 Color: 4

Bin 667: 16 of cap free
Amount of items: 2
Items: 
Size: 648092 Color: 1
Size: 351893 Color: 4

Bin 668: 16 of cap free
Amount of items: 2
Items: 
Size: 652227 Color: 3
Size: 347758 Color: 0

Bin 669: 16 of cap free
Amount of items: 2
Items: 
Size: 655401 Color: 1
Size: 344584 Color: 0

Bin 670: 16 of cap free
Amount of items: 2
Items: 
Size: 663840 Color: 1
Size: 336145 Color: 0

Bin 671: 16 of cap free
Amount of items: 2
Items: 
Size: 672257 Color: 3
Size: 327728 Color: 2

Bin 672: 16 of cap free
Amount of items: 2
Items: 
Size: 679861 Color: 3
Size: 320124 Color: 4

Bin 673: 16 of cap free
Amount of items: 2
Items: 
Size: 704207 Color: 4
Size: 295778 Color: 2

Bin 674: 16 of cap free
Amount of items: 2
Items: 
Size: 711004 Color: 3
Size: 288981 Color: 2

Bin 675: 16 of cap free
Amount of items: 2
Items: 
Size: 711242 Color: 3
Size: 288743 Color: 1

Bin 676: 16 of cap free
Amount of items: 2
Items: 
Size: 717799 Color: 0
Size: 282186 Color: 4

Bin 677: 16 of cap free
Amount of items: 2
Items: 
Size: 719860 Color: 4
Size: 280125 Color: 3

Bin 678: 16 of cap free
Amount of items: 2
Items: 
Size: 724556 Color: 3
Size: 275429 Color: 0

Bin 679: 16 of cap free
Amount of items: 2
Items: 
Size: 726998 Color: 0
Size: 272987 Color: 4

Bin 680: 16 of cap free
Amount of items: 2
Items: 
Size: 728200 Color: 0
Size: 271785 Color: 2

Bin 681: 16 of cap free
Amount of items: 2
Items: 
Size: 739030 Color: 4
Size: 260955 Color: 3

Bin 682: 16 of cap free
Amount of items: 2
Items: 
Size: 753460 Color: 2
Size: 246525 Color: 3

Bin 683: 16 of cap free
Amount of items: 2
Items: 
Size: 774166 Color: 3
Size: 225819 Color: 4

Bin 684: 16 of cap free
Amount of items: 2
Items: 
Size: 780685 Color: 3
Size: 219300 Color: 4

Bin 685: 16 of cap free
Amount of items: 2
Items: 
Size: 784145 Color: 4
Size: 215840 Color: 3

Bin 686: 16 of cap free
Amount of items: 2
Items: 
Size: 794857 Color: 4
Size: 205128 Color: 1

Bin 687: 16 of cap free
Amount of items: 2
Items: 
Size: 798196 Color: 1
Size: 201789 Color: 4

Bin 688: 17 of cap free
Amount of items: 6
Items: 
Size: 177820 Color: 0
Size: 177749 Color: 1
Size: 177720 Color: 0
Size: 177657 Color: 3
Size: 177534 Color: 3
Size: 111504 Color: 3

Bin 689: 17 of cap free
Amount of items: 2
Items: 
Size: 504990 Color: 3
Size: 494994 Color: 2

Bin 690: 17 of cap free
Amount of items: 4
Items: 
Size: 515201 Color: 3
Size: 189910 Color: 1
Size: 189887 Color: 2
Size: 104986 Color: 3

Bin 691: 17 of cap free
Amount of items: 2
Items: 
Size: 515508 Color: 1
Size: 484476 Color: 4

Bin 692: 17 of cap free
Amount of items: 2
Items: 
Size: 527927 Color: 2
Size: 472057 Color: 3

Bin 693: 17 of cap free
Amount of items: 2
Items: 
Size: 528465 Color: 2
Size: 471519 Color: 0

Bin 694: 17 of cap free
Amount of items: 2
Items: 
Size: 539960 Color: 3
Size: 460024 Color: 2

Bin 695: 17 of cap free
Amount of items: 2
Items: 
Size: 552345 Color: 0
Size: 447639 Color: 4

Bin 696: 17 of cap free
Amount of items: 2
Items: 
Size: 578968 Color: 2
Size: 421016 Color: 4

Bin 697: 17 of cap free
Amount of items: 2
Items: 
Size: 597531 Color: 2
Size: 402453 Color: 4

Bin 698: 17 of cap free
Amount of items: 2
Items: 
Size: 599537 Color: 3
Size: 400447 Color: 0

Bin 699: 17 of cap free
Amount of items: 2
Items: 
Size: 629912 Color: 4
Size: 370072 Color: 1

Bin 700: 17 of cap free
Amount of items: 2
Items: 
Size: 630311 Color: 0
Size: 369673 Color: 4

Bin 701: 17 of cap free
Amount of items: 2
Items: 
Size: 655628 Color: 1
Size: 344356 Color: 2

Bin 702: 17 of cap free
Amount of items: 2
Items: 
Size: 680820 Color: 0
Size: 319164 Color: 3

Bin 703: 17 of cap free
Amount of items: 2
Items: 
Size: 702052 Color: 0
Size: 297932 Color: 2

Bin 704: 17 of cap free
Amount of items: 2
Items: 
Size: 712645 Color: 4
Size: 287339 Color: 3

Bin 705: 17 of cap free
Amount of items: 2
Items: 
Size: 712867 Color: 1
Size: 287117 Color: 0

Bin 706: 17 of cap free
Amount of items: 2
Items: 
Size: 715066 Color: 0
Size: 284918 Color: 4

Bin 707: 17 of cap free
Amount of items: 2
Items: 
Size: 715620 Color: 1
Size: 284364 Color: 2

Bin 708: 17 of cap free
Amount of items: 2
Items: 
Size: 720139 Color: 0
Size: 279845 Color: 2

Bin 709: 17 of cap free
Amount of items: 2
Items: 
Size: 725336 Color: 4
Size: 274648 Color: 3

Bin 710: 17 of cap free
Amount of items: 2
Items: 
Size: 737683 Color: 1
Size: 262301 Color: 4

Bin 711: 17 of cap free
Amount of items: 2
Items: 
Size: 780658 Color: 4
Size: 219326 Color: 1

Bin 712: 17 of cap free
Amount of items: 2
Items: 
Size: 798519 Color: 1
Size: 201465 Color: 4

Bin 713: 18 of cap free
Amount of items: 4
Items: 
Size: 515184 Color: 0
Size: 190301 Color: 3
Size: 190209 Color: 4
Size: 104289 Color: 3

Bin 714: 18 of cap free
Amount of items: 2
Items: 
Size: 535169 Color: 1
Size: 464814 Color: 4

Bin 715: 18 of cap free
Amount of items: 2
Items: 
Size: 536121 Color: 1
Size: 463862 Color: 3

Bin 716: 18 of cap free
Amount of items: 2
Items: 
Size: 547434 Color: 0
Size: 452549 Color: 3

Bin 717: 18 of cap free
Amount of items: 2
Items: 
Size: 551097 Color: 4
Size: 448886 Color: 2

Bin 718: 18 of cap free
Amount of items: 2
Items: 
Size: 561433 Color: 0
Size: 438550 Color: 3

Bin 719: 18 of cap free
Amount of items: 2
Items: 
Size: 563384 Color: 0
Size: 436599 Color: 4

Bin 720: 18 of cap free
Amount of items: 2
Items: 
Size: 572030 Color: 2
Size: 427953 Color: 0

Bin 721: 18 of cap free
Amount of items: 2
Items: 
Size: 576315 Color: 0
Size: 423668 Color: 2

Bin 722: 18 of cap free
Amount of items: 2
Items: 
Size: 576803 Color: 3
Size: 423180 Color: 4

Bin 723: 18 of cap free
Amount of items: 2
Items: 
Size: 619537 Color: 0
Size: 380446 Color: 1

Bin 724: 18 of cap free
Amount of items: 2
Items: 
Size: 636060 Color: 2
Size: 363923 Color: 4

Bin 725: 18 of cap free
Amount of items: 2
Items: 
Size: 665931 Color: 1
Size: 334052 Color: 0

Bin 726: 18 of cap free
Amount of items: 2
Items: 
Size: 689697 Color: 4
Size: 310286 Color: 3

Bin 727: 18 of cap free
Amount of items: 2
Items: 
Size: 702327 Color: 1
Size: 297656 Color: 2

Bin 728: 18 of cap free
Amount of items: 2
Items: 
Size: 703787 Color: 0
Size: 296196 Color: 4

Bin 729: 18 of cap free
Amount of items: 2
Items: 
Size: 713734 Color: 3
Size: 286249 Color: 1

Bin 730: 18 of cap free
Amount of items: 2
Items: 
Size: 714164 Color: 4
Size: 285819 Color: 3

Bin 731: 18 of cap free
Amount of items: 2
Items: 
Size: 733958 Color: 2
Size: 266025 Color: 0

Bin 732: 18 of cap free
Amount of items: 2
Items: 
Size: 740695 Color: 1
Size: 259288 Color: 4

Bin 733: 18 of cap free
Amount of items: 2
Items: 
Size: 743285 Color: 0
Size: 256698 Color: 3

Bin 734: 18 of cap free
Amount of items: 2
Items: 
Size: 746239 Color: 4
Size: 253744 Color: 2

Bin 735: 18 of cap free
Amount of items: 2
Items: 
Size: 751474 Color: 4
Size: 248509 Color: 3

Bin 736: 18 of cap free
Amount of items: 2
Items: 
Size: 757969 Color: 1
Size: 242014 Color: 0

Bin 737: 18 of cap free
Amount of items: 2
Items: 
Size: 761746 Color: 1
Size: 238237 Color: 3

Bin 738: 18 of cap free
Amount of items: 2
Items: 
Size: 766496 Color: 2
Size: 233487 Color: 1

Bin 739: 18 of cap free
Amount of items: 2
Items: 
Size: 769159 Color: 0
Size: 230824 Color: 2

Bin 740: 18 of cap free
Amount of items: 2
Items: 
Size: 780322 Color: 1
Size: 219661 Color: 2

Bin 741: 19 of cap free
Amount of items: 2
Items: 
Size: 502723 Color: 4
Size: 497259 Color: 2

Bin 742: 19 of cap free
Amount of items: 2
Items: 
Size: 502978 Color: 1
Size: 497004 Color: 2

Bin 743: 19 of cap free
Amount of items: 2
Items: 
Size: 505937 Color: 2
Size: 494045 Color: 3

Bin 744: 19 of cap free
Amount of items: 2
Items: 
Size: 507584 Color: 4
Size: 492398 Color: 3

Bin 745: 19 of cap free
Amount of items: 2
Items: 
Size: 514486 Color: 2
Size: 485496 Color: 0

Bin 746: 19 of cap free
Amount of items: 2
Items: 
Size: 528177 Color: 2
Size: 471805 Color: 1

Bin 747: 19 of cap free
Amount of items: 2
Items: 
Size: 541645 Color: 0
Size: 458337 Color: 1

Bin 748: 19 of cap free
Amount of items: 2
Items: 
Size: 546418 Color: 4
Size: 453564 Color: 3

Bin 749: 19 of cap free
Amount of items: 2
Items: 
Size: 548903 Color: 1
Size: 451079 Color: 3

Bin 750: 19 of cap free
Amount of items: 2
Items: 
Size: 588770 Color: 2
Size: 411212 Color: 0

Bin 751: 19 of cap free
Amount of items: 2
Items: 
Size: 597382 Color: 2
Size: 402600 Color: 4

Bin 752: 19 of cap free
Amount of items: 2
Items: 
Size: 603827 Color: 2
Size: 396155 Color: 3

Bin 753: 19 of cap free
Amount of items: 2
Items: 
Size: 604755 Color: 2
Size: 395227 Color: 1

Bin 754: 19 of cap free
Amount of items: 2
Items: 
Size: 606862 Color: 2
Size: 393120 Color: 4

Bin 755: 19 of cap free
Amount of items: 2
Items: 
Size: 620689 Color: 3
Size: 379293 Color: 0

Bin 756: 19 of cap free
Amount of items: 2
Items: 
Size: 647689 Color: 2
Size: 352293 Color: 0

Bin 757: 19 of cap free
Amount of items: 2
Items: 
Size: 656166 Color: 1
Size: 343816 Color: 0

Bin 758: 19 of cap free
Amount of items: 2
Items: 
Size: 660827 Color: 4
Size: 339155 Color: 3

Bin 759: 19 of cap free
Amount of items: 2
Items: 
Size: 671895 Color: 2
Size: 328087 Color: 3

Bin 760: 19 of cap free
Amount of items: 2
Items: 
Size: 675345 Color: 1
Size: 324637 Color: 4

Bin 761: 19 of cap free
Amount of items: 2
Items: 
Size: 679408 Color: 3
Size: 320574 Color: 4

Bin 762: 19 of cap free
Amount of items: 2
Items: 
Size: 700677 Color: 2
Size: 299305 Color: 1

Bin 763: 19 of cap free
Amount of items: 2
Items: 
Size: 705300 Color: 2
Size: 294682 Color: 0

Bin 764: 19 of cap free
Amount of items: 2
Items: 
Size: 707789 Color: 4
Size: 292193 Color: 3

Bin 765: 19 of cap free
Amount of items: 2
Items: 
Size: 713612 Color: 0
Size: 286370 Color: 1

Bin 766: 19 of cap free
Amount of items: 2
Items: 
Size: 721946 Color: 4
Size: 278036 Color: 0

Bin 767: 19 of cap free
Amount of items: 2
Items: 
Size: 731337 Color: 3
Size: 268645 Color: 1

Bin 768: 19 of cap free
Amount of items: 2
Items: 
Size: 735776 Color: 2
Size: 264206 Color: 3

Bin 769: 19 of cap free
Amount of items: 2
Items: 
Size: 752780 Color: 2
Size: 247202 Color: 1

Bin 770: 19 of cap free
Amount of items: 2
Items: 
Size: 779056 Color: 0
Size: 220926 Color: 1

Bin 771: 19 of cap free
Amount of items: 2
Items: 
Size: 783987 Color: 1
Size: 215995 Color: 0

Bin 772: 19 of cap free
Amount of items: 2
Items: 
Size: 794551 Color: 0
Size: 205431 Color: 4

Bin 773: 20 of cap free
Amount of items: 6
Items: 
Size: 176711 Color: 0
Size: 176701 Color: 1
Size: 176571 Color: 1
Size: 176563 Color: 3
Size: 176544 Color: 1
Size: 116891 Color: 3

Bin 774: 20 of cap free
Amount of items: 6
Items: 
Size: 179456 Color: 0
Size: 179407 Color: 0
Size: 179241 Color: 4
Size: 179237 Color: 0
Size: 179176 Color: 3
Size: 103464 Color: 1

Bin 775: 20 of cap free
Amount of items: 2
Items: 
Size: 531904 Color: 1
Size: 468077 Color: 4

Bin 776: 20 of cap free
Amount of items: 2
Items: 
Size: 548328 Color: 4
Size: 451653 Color: 0

Bin 777: 20 of cap free
Amount of items: 2
Items: 
Size: 553204 Color: 3
Size: 446777 Color: 4

Bin 778: 20 of cap free
Amount of items: 2
Items: 
Size: 553689 Color: 0
Size: 446292 Color: 4

Bin 779: 20 of cap free
Amount of items: 2
Items: 
Size: 556409 Color: 2
Size: 443572 Color: 1

Bin 780: 20 of cap free
Amount of items: 2
Items: 
Size: 574857 Color: 2
Size: 425124 Color: 4

Bin 781: 20 of cap free
Amount of items: 2
Items: 
Size: 587256 Color: 2
Size: 412725 Color: 3

Bin 782: 20 of cap free
Amount of items: 2
Items: 
Size: 598012 Color: 1
Size: 401969 Color: 3

Bin 783: 20 of cap free
Amount of items: 2
Items: 
Size: 598468 Color: 3
Size: 401513 Color: 1

Bin 784: 20 of cap free
Amount of items: 2
Items: 
Size: 616011 Color: 2
Size: 383970 Color: 4

Bin 785: 20 of cap free
Amount of items: 2
Items: 
Size: 621000 Color: 4
Size: 378981 Color: 2

Bin 786: 20 of cap free
Amount of items: 2
Items: 
Size: 643297 Color: 2
Size: 356684 Color: 4

Bin 787: 20 of cap free
Amount of items: 2
Items: 
Size: 643439 Color: 2
Size: 356542 Color: 0

Bin 788: 20 of cap free
Amount of items: 2
Items: 
Size: 658829 Color: 4
Size: 341152 Color: 3

Bin 789: 20 of cap free
Amount of items: 2
Items: 
Size: 659308 Color: 1
Size: 340673 Color: 0

Bin 790: 20 of cap free
Amount of items: 2
Items: 
Size: 689001 Color: 3
Size: 310980 Color: 4

Bin 791: 20 of cap free
Amount of items: 2
Items: 
Size: 694751 Color: 2
Size: 305230 Color: 4

Bin 792: 20 of cap free
Amount of items: 2
Items: 
Size: 700577 Color: 4
Size: 299404 Color: 0

Bin 793: 20 of cap free
Amount of items: 2
Items: 
Size: 709806 Color: 3
Size: 290175 Color: 2

Bin 794: 20 of cap free
Amount of items: 2
Items: 
Size: 712356 Color: 1
Size: 287625 Color: 3

Bin 795: 20 of cap free
Amount of items: 2
Items: 
Size: 739483 Color: 3
Size: 260498 Color: 2

Bin 796: 20 of cap free
Amount of items: 2
Items: 
Size: 773626 Color: 3
Size: 226355 Color: 1

Bin 797: 20 of cap free
Amount of items: 2
Items: 
Size: 786799 Color: 3
Size: 213182 Color: 4

Bin 798: 20 of cap free
Amount of items: 2
Items: 
Size: 791741 Color: 1
Size: 208240 Color: 2

Bin 799: 20 of cap free
Amount of items: 2
Items: 
Size: 794764 Color: 2
Size: 205217 Color: 4

Bin 800: 21 of cap free
Amount of items: 2
Items: 
Size: 501137 Color: 0
Size: 498843 Color: 3

Bin 801: 21 of cap free
Amount of items: 2
Items: 
Size: 528843 Color: 3
Size: 471137 Color: 2

Bin 802: 21 of cap free
Amount of items: 2
Items: 
Size: 529332 Color: 3
Size: 470648 Color: 2

Bin 803: 21 of cap free
Amount of items: 2
Items: 
Size: 537840 Color: 4
Size: 462140 Color: 2

Bin 804: 21 of cap free
Amount of items: 2
Items: 
Size: 548656 Color: 3
Size: 451324 Color: 0

Bin 805: 21 of cap free
Amount of items: 2
Items: 
Size: 549373 Color: 3
Size: 450607 Color: 4

Bin 806: 21 of cap free
Amount of items: 2
Items: 
Size: 551584 Color: 2
Size: 448396 Color: 4

Bin 807: 21 of cap free
Amount of items: 2
Items: 
Size: 552728 Color: 0
Size: 447252 Color: 4

Bin 808: 21 of cap free
Amount of items: 2
Items: 
Size: 569341 Color: 4
Size: 430639 Color: 2

Bin 809: 21 of cap free
Amount of items: 2
Items: 
Size: 580865 Color: 4
Size: 419115 Color: 0

Bin 810: 21 of cap free
Amount of items: 2
Items: 
Size: 586419 Color: 2
Size: 413561 Color: 4

Bin 811: 21 of cap free
Amount of items: 2
Items: 
Size: 589670 Color: 2
Size: 410310 Color: 1

Bin 812: 21 of cap free
Amount of items: 2
Items: 
Size: 598414 Color: 1
Size: 401566 Color: 0

Bin 813: 21 of cap free
Amount of items: 2
Items: 
Size: 600360 Color: 4
Size: 399620 Color: 0

Bin 814: 21 of cap free
Amount of items: 2
Items: 
Size: 606893 Color: 3
Size: 393087 Color: 2

Bin 815: 21 of cap free
Amount of items: 2
Items: 
Size: 624762 Color: 2
Size: 375218 Color: 4

Bin 816: 21 of cap free
Amount of items: 2
Items: 
Size: 627290 Color: 3
Size: 372690 Color: 0

Bin 817: 21 of cap free
Amount of items: 2
Items: 
Size: 629335 Color: 2
Size: 370645 Color: 4

Bin 818: 21 of cap free
Amount of items: 2
Items: 
Size: 636655 Color: 2
Size: 363325 Color: 3

Bin 819: 21 of cap free
Amount of items: 2
Items: 
Size: 644068 Color: 2
Size: 355912 Color: 3

Bin 820: 21 of cap free
Amount of items: 2
Items: 
Size: 655272 Color: 0
Size: 344708 Color: 4

Bin 821: 21 of cap free
Amount of items: 2
Items: 
Size: 675131 Color: 4
Size: 324849 Color: 0

Bin 822: 21 of cap free
Amount of items: 2
Items: 
Size: 678450 Color: 0
Size: 321530 Color: 4

Bin 823: 21 of cap free
Amount of items: 2
Items: 
Size: 678605 Color: 4
Size: 321375 Color: 3

Bin 824: 21 of cap free
Amount of items: 2
Items: 
Size: 688822 Color: 0
Size: 311158 Color: 2

Bin 825: 21 of cap free
Amount of items: 2
Items: 
Size: 694847 Color: 0
Size: 305133 Color: 4

Bin 826: 21 of cap free
Amount of items: 2
Items: 
Size: 695434 Color: 2
Size: 304546 Color: 1

Bin 827: 21 of cap free
Amount of items: 2
Items: 
Size: 700839 Color: 2
Size: 299141 Color: 1

Bin 828: 21 of cap free
Amount of items: 2
Items: 
Size: 711724 Color: 1
Size: 288256 Color: 2

Bin 829: 21 of cap free
Amount of items: 2
Items: 
Size: 720759 Color: 3
Size: 279221 Color: 4

Bin 830: 21 of cap free
Amount of items: 2
Items: 
Size: 726821 Color: 0
Size: 273159 Color: 2

Bin 831: 21 of cap free
Amount of items: 2
Items: 
Size: 739263 Color: 3
Size: 260717 Color: 1

Bin 832: 21 of cap free
Amount of items: 2
Items: 
Size: 739461 Color: 0
Size: 260519 Color: 3

Bin 833: 21 of cap free
Amount of items: 2
Items: 
Size: 765202 Color: 3
Size: 234778 Color: 2

Bin 834: 21 of cap free
Amount of items: 2
Items: 
Size: 766190 Color: 1
Size: 233790 Color: 2

Bin 835: 21 of cap free
Amount of items: 2
Items: 
Size: 773030 Color: 2
Size: 226950 Color: 3

Bin 836: 21 of cap free
Amount of items: 2
Items: 
Size: 796248 Color: 4
Size: 203732 Color: 0

Bin 837: 22 of cap free
Amount of items: 2
Items: 
Size: 501389 Color: 2
Size: 498590 Color: 1

Bin 838: 22 of cap free
Amount of items: 2
Items: 
Size: 510359 Color: 3
Size: 489620 Color: 4

Bin 839: 22 of cap free
Amount of items: 2
Items: 
Size: 512535 Color: 1
Size: 487444 Color: 0

Bin 840: 22 of cap free
Amount of items: 2
Items: 
Size: 521104 Color: 3
Size: 478875 Color: 4

Bin 841: 22 of cap free
Amount of items: 2
Items: 
Size: 525490 Color: 1
Size: 474489 Color: 4

Bin 842: 22 of cap free
Amount of items: 2
Items: 
Size: 526753 Color: 4
Size: 473226 Color: 0

Bin 843: 22 of cap free
Amount of items: 2
Items: 
Size: 538235 Color: 4
Size: 461744 Color: 2

Bin 844: 22 of cap free
Amount of items: 2
Items: 
Size: 542318 Color: 2
Size: 457661 Color: 3

Bin 845: 22 of cap free
Amount of items: 2
Items: 
Size: 567498 Color: 2
Size: 432481 Color: 0

Bin 846: 22 of cap free
Amount of items: 2
Items: 
Size: 571504 Color: 4
Size: 428475 Color: 1

Bin 847: 22 of cap free
Amount of items: 2
Items: 
Size: 571641 Color: 1
Size: 428338 Color: 4

Bin 848: 22 of cap free
Amount of items: 2
Items: 
Size: 580139 Color: 0
Size: 419840 Color: 3

Bin 849: 22 of cap free
Amount of items: 2
Items: 
Size: 593666 Color: 1
Size: 406313 Color: 2

Bin 850: 22 of cap free
Amount of items: 2
Items: 
Size: 603957 Color: 3
Size: 396022 Color: 0

Bin 851: 22 of cap free
Amount of items: 2
Items: 
Size: 613004 Color: 3
Size: 386975 Color: 1

Bin 852: 22 of cap free
Amount of items: 2
Items: 
Size: 624887 Color: 2
Size: 375092 Color: 1

Bin 853: 22 of cap free
Amount of items: 3
Items: 
Size: 630671 Color: 0
Size: 195901 Color: 2
Size: 173407 Color: 2

Bin 854: 22 of cap free
Amount of items: 2
Items: 
Size: 632463 Color: 4
Size: 367516 Color: 0

Bin 855: 22 of cap free
Amount of items: 2
Items: 
Size: 645499 Color: 3
Size: 354480 Color: 2

Bin 856: 22 of cap free
Amount of items: 2
Items: 
Size: 649931 Color: 2
Size: 350048 Color: 0

Bin 857: 22 of cap free
Amount of items: 2
Items: 
Size: 663501 Color: 2
Size: 336478 Color: 0

Bin 858: 22 of cap free
Amount of items: 2
Items: 
Size: 666072 Color: 3
Size: 333907 Color: 1

Bin 859: 22 of cap free
Amount of items: 2
Items: 
Size: 666876 Color: 1
Size: 333103 Color: 0

Bin 860: 22 of cap free
Amount of items: 2
Items: 
Size: 676624 Color: 4
Size: 323355 Color: 2

Bin 861: 22 of cap free
Amount of items: 2
Items: 
Size: 681315 Color: 3
Size: 318664 Color: 4

Bin 862: 22 of cap free
Amount of items: 2
Items: 
Size: 693278 Color: 3
Size: 306701 Color: 0

Bin 863: 22 of cap free
Amount of items: 2
Items: 
Size: 732423 Color: 3
Size: 267556 Color: 0

Bin 864: 22 of cap free
Amount of items: 2
Items: 
Size: 739897 Color: 0
Size: 260082 Color: 4

Bin 865: 23 of cap free
Amount of items: 6
Items: 
Size: 170111 Color: 1
Size: 170035 Color: 2
Size: 170021 Color: 0
Size: 170012 Color: 0
Size: 169909 Color: 2
Size: 149890 Color: 4

Bin 866: 23 of cap free
Amount of items: 2
Items: 
Size: 505468 Color: 3
Size: 494510 Color: 2

Bin 867: 23 of cap free
Amount of items: 2
Items: 
Size: 506395 Color: 3
Size: 493583 Color: 4

Bin 868: 23 of cap free
Amount of items: 2
Items: 
Size: 540065 Color: 1
Size: 459913 Color: 3

Bin 869: 23 of cap free
Amount of items: 2
Items: 
Size: 542077 Color: 3
Size: 457901 Color: 1

Bin 870: 23 of cap free
Amount of items: 2
Items: 
Size: 552773 Color: 4
Size: 447205 Color: 2

Bin 871: 23 of cap free
Amount of items: 2
Items: 
Size: 559175 Color: 2
Size: 440803 Color: 3

Bin 872: 23 of cap free
Amount of items: 2
Items: 
Size: 559885 Color: 1
Size: 440093 Color: 0

Bin 873: 23 of cap free
Amount of items: 2
Items: 
Size: 564595 Color: 4
Size: 435383 Color: 3

Bin 874: 23 of cap free
Amount of items: 2
Items: 
Size: 573759 Color: 0
Size: 426219 Color: 3

Bin 875: 23 of cap free
Amount of items: 2
Items: 
Size: 603922 Color: 0
Size: 396056 Color: 2

Bin 876: 23 of cap free
Amount of items: 2
Items: 
Size: 608403 Color: 0
Size: 391575 Color: 4

Bin 877: 23 of cap free
Amount of items: 2
Items: 
Size: 615406 Color: 1
Size: 384572 Color: 3

Bin 878: 23 of cap free
Amount of items: 2
Items: 
Size: 619710 Color: 3
Size: 380268 Color: 4

Bin 879: 23 of cap free
Amount of items: 3
Items: 
Size: 630745 Color: 3
Size: 196069 Color: 1
Size: 173164 Color: 2

Bin 880: 23 of cap free
Amount of items: 2
Items: 
Size: 637498 Color: 0
Size: 362480 Color: 4

Bin 881: 23 of cap free
Amount of items: 2
Items: 
Size: 653517 Color: 4
Size: 346461 Color: 2

Bin 882: 23 of cap free
Amount of items: 2
Items: 
Size: 664502 Color: 1
Size: 335476 Color: 3

Bin 883: 23 of cap free
Amount of items: 2
Items: 
Size: 677464 Color: 0
Size: 322514 Color: 3

Bin 884: 23 of cap free
Amount of items: 2
Items: 
Size: 703380 Color: 4
Size: 296598 Color: 1

Bin 885: 23 of cap free
Amount of items: 2
Items: 
Size: 706906 Color: 2
Size: 293072 Color: 1

Bin 886: 23 of cap free
Amount of items: 2
Items: 
Size: 714961 Color: 2
Size: 285017 Color: 4

Bin 887: 23 of cap free
Amount of items: 2
Items: 
Size: 715338 Color: 2
Size: 284640 Color: 1

Bin 888: 23 of cap free
Amount of items: 2
Items: 
Size: 715547 Color: 0
Size: 284431 Color: 2

Bin 889: 23 of cap free
Amount of items: 2
Items: 
Size: 715790 Color: 3
Size: 284188 Color: 2

Bin 890: 23 of cap free
Amount of items: 2
Items: 
Size: 720823 Color: 0
Size: 279155 Color: 1

Bin 891: 23 of cap free
Amount of items: 2
Items: 
Size: 725848 Color: 2
Size: 274130 Color: 0

Bin 892: 23 of cap free
Amount of items: 2
Items: 
Size: 747308 Color: 1
Size: 252670 Color: 4

Bin 893: 23 of cap free
Amount of items: 2
Items: 
Size: 755317 Color: 0
Size: 244661 Color: 3

Bin 894: 23 of cap free
Amount of items: 2
Items: 
Size: 762752 Color: 2
Size: 237226 Color: 3

Bin 895: 23 of cap free
Amount of items: 2
Items: 
Size: 772102 Color: 4
Size: 227876 Color: 3

Bin 896: 23 of cap free
Amount of items: 2
Items: 
Size: 775753 Color: 1
Size: 224225 Color: 0

Bin 897: 23 of cap free
Amount of items: 2
Items: 
Size: 790883 Color: 0
Size: 209095 Color: 2

Bin 898: 23 of cap free
Amount of items: 2
Items: 
Size: 796158 Color: 0
Size: 203820 Color: 2

Bin 899: 23 of cap free
Amount of items: 2
Items: 
Size: 797173 Color: 3
Size: 202805 Color: 2

Bin 900: 24 of cap free
Amount of items: 2
Items: 
Size: 505727 Color: 3
Size: 494250 Color: 4

Bin 901: 24 of cap free
Amount of items: 2
Items: 
Size: 507647 Color: 1
Size: 492330 Color: 0

Bin 902: 24 of cap free
Amount of items: 4
Items: 
Size: 512187 Color: 3
Size: 189404 Color: 1
Size: 189393 Color: 1
Size: 108993 Color: 0

Bin 903: 24 of cap free
Amount of items: 4
Items: 
Size: 515158 Color: 0
Size: 190195 Color: 3
Size: 190053 Color: 1
Size: 104571 Color: 3

Bin 904: 24 of cap free
Amount of items: 2
Items: 
Size: 519164 Color: 2
Size: 480813 Color: 3

Bin 905: 24 of cap free
Amount of items: 2
Items: 
Size: 519331 Color: 0
Size: 480646 Color: 3

Bin 906: 24 of cap free
Amount of items: 2
Items: 
Size: 528226 Color: 3
Size: 471751 Color: 4

Bin 907: 24 of cap free
Amount of items: 2
Items: 
Size: 531240 Color: 3
Size: 468737 Color: 2

Bin 908: 24 of cap free
Amount of items: 2
Items: 
Size: 569789 Color: 0
Size: 430188 Color: 4

Bin 909: 24 of cap free
Amount of items: 2
Items: 
Size: 596846 Color: 1
Size: 403131 Color: 3

Bin 910: 24 of cap free
Amount of items: 2
Items: 
Size: 598597 Color: 1
Size: 401380 Color: 3

Bin 911: 24 of cap free
Amount of items: 2
Items: 
Size: 602537 Color: 4
Size: 397440 Color: 0

Bin 912: 24 of cap free
Amount of items: 2
Items: 
Size: 641755 Color: 2
Size: 358222 Color: 0

Bin 913: 24 of cap free
Amount of items: 2
Items: 
Size: 642488 Color: 1
Size: 357489 Color: 4

Bin 914: 24 of cap free
Amount of items: 2
Items: 
Size: 653588 Color: 0
Size: 346389 Color: 3

Bin 915: 24 of cap free
Amount of items: 2
Items: 
Size: 668231 Color: 2
Size: 331746 Color: 0

Bin 916: 24 of cap free
Amount of items: 2
Items: 
Size: 670716 Color: 4
Size: 329261 Color: 3

Bin 917: 24 of cap free
Amount of items: 2
Items: 
Size: 696883 Color: 3
Size: 303094 Color: 4

Bin 918: 24 of cap free
Amount of items: 2
Items: 
Size: 719045 Color: 1
Size: 280932 Color: 2

Bin 919: 24 of cap free
Amount of items: 2
Items: 
Size: 742025 Color: 3
Size: 257952 Color: 1

Bin 920: 24 of cap free
Amount of items: 2
Items: 
Size: 745055 Color: 0
Size: 254922 Color: 1

Bin 921: 24 of cap free
Amount of items: 2
Items: 
Size: 750443 Color: 0
Size: 249534 Color: 3

Bin 922: 24 of cap free
Amount of items: 2
Items: 
Size: 753125 Color: 4
Size: 246852 Color: 0

Bin 923: 24 of cap free
Amount of items: 2
Items: 
Size: 757447 Color: 1
Size: 242530 Color: 3

Bin 924: 24 of cap free
Amount of items: 2
Items: 
Size: 772339 Color: 1
Size: 227638 Color: 3

Bin 925: 24 of cap free
Amount of items: 2
Items: 
Size: 777739 Color: 3
Size: 222238 Color: 4

Bin 926: 24 of cap free
Amount of items: 2
Items: 
Size: 790275 Color: 0
Size: 209702 Color: 2

Bin 927: 25 of cap free
Amount of items: 9
Items: 
Size: 112582 Color: 3
Size: 112574 Color: 1
Size: 112555 Color: 2
Size: 112466 Color: 0
Size: 112451 Color: 4
Size: 112353 Color: 4
Size: 112285 Color: 0
Size: 112269 Color: 4
Size: 100441 Color: 0

Bin 928: 25 of cap free
Amount of items: 6
Items: 
Size: 168188 Color: 2
Size: 168110 Color: 2
Size: 168074 Color: 4
Size: 168070 Color: 4
Size: 168063 Color: 0
Size: 159471 Color: 3

Bin 929: 25 of cap free
Amount of items: 6
Items: 
Size: 171380 Color: 0
Size: 171339 Color: 2
Size: 171159 Color: 1
Size: 171045 Color: 1
Size: 170939 Color: 1
Size: 144114 Color: 2

Bin 930: 25 of cap free
Amount of items: 6
Items: 
Size: 173492 Color: 0
Size: 173357 Color: 4
Size: 173343 Color: 3
Size: 173319 Color: 2
Size: 173295 Color: 2
Size: 133170 Color: 1

Bin 931: 25 of cap free
Amount of items: 6
Items: 
Size: 174740 Color: 3
Size: 174715 Color: 0
Size: 174648 Color: 4
Size: 174644 Color: 3
Size: 174614 Color: 2
Size: 126615 Color: 4

Bin 932: 25 of cap free
Amount of items: 2
Items: 
Size: 511285 Color: 1
Size: 488691 Color: 0

Bin 933: 25 of cap free
Amount of items: 2
Items: 
Size: 512283 Color: 4
Size: 487693 Color: 3

Bin 934: 25 of cap free
Amount of items: 2
Items: 
Size: 517180 Color: 3
Size: 482796 Color: 4

Bin 935: 25 of cap free
Amount of items: 2
Items: 
Size: 535630 Color: 2
Size: 464346 Color: 0

Bin 936: 25 of cap free
Amount of items: 2
Items: 
Size: 548151 Color: 0
Size: 451825 Color: 4

Bin 937: 25 of cap free
Amount of items: 2
Items: 
Size: 568134 Color: 1
Size: 431842 Color: 2

Bin 938: 25 of cap free
Amount of items: 2
Items: 
Size: 607467 Color: 3
Size: 392509 Color: 2

Bin 939: 25 of cap free
Amount of items: 2
Items: 
Size: 610387 Color: 2
Size: 389589 Color: 0

Bin 940: 25 of cap free
Amount of items: 2
Items: 
Size: 613102 Color: 1
Size: 386874 Color: 0

Bin 941: 25 of cap free
Amount of items: 2
Items: 
Size: 645410 Color: 2
Size: 354566 Color: 4

Bin 942: 25 of cap free
Amount of items: 2
Items: 
Size: 682429 Color: 2
Size: 317547 Color: 3

Bin 943: 25 of cap free
Amount of items: 2
Items: 
Size: 691884 Color: 1
Size: 308092 Color: 0

Bin 944: 25 of cap free
Amount of items: 2
Items: 
Size: 697832 Color: 3
Size: 302144 Color: 2

Bin 945: 25 of cap free
Amount of items: 2
Items: 
Size: 700265 Color: 0
Size: 299711 Color: 3

Bin 946: 25 of cap free
Amount of items: 2
Items: 
Size: 704566 Color: 2
Size: 295410 Color: 0

Bin 947: 25 of cap free
Amount of items: 2
Items: 
Size: 709922 Color: 4
Size: 290054 Color: 3

Bin 948: 25 of cap free
Amount of items: 2
Items: 
Size: 726710 Color: 4
Size: 273266 Color: 3

Bin 949: 25 of cap free
Amount of items: 2
Items: 
Size: 733500 Color: 3
Size: 266476 Color: 0

Bin 950: 25 of cap free
Amount of items: 2
Items: 
Size: 752018 Color: 0
Size: 247958 Color: 1

Bin 951: 25 of cap free
Amount of items: 2
Items: 
Size: 771521 Color: 1
Size: 228455 Color: 2

Bin 952: 25 of cap free
Amount of items: 2
Items: 
Size: 771955 Color: 3
Size: 228021 Color: 0

Bin 953: 25 of cap free
Amount of items: 2
Items: 
Size: 777410 Color: 2
Size: 222566 Color: 3

Bin 954: 25 of cap free
Amount of items: 2
Items: 
Size: 787869 Color: 3
Size: 212107 Color: 1

Bin 955: 25 of cap free
Amount of items: 2
Items: 
Size: 799013 Color: 0
Size: 200963 Color: 2

Bin 956: 26 of cap free
Amount of items: 8
Items: 
Size: 125374 Color: 2
Size: 125300 Color: 3
Size: 125221 Color: 1
Size: 125060 Color: 0
Size: 125031 Color: 2
Size: 124998 Color: 2
Size: 124981 Color: 4
Size: 124010 Color: 0

Bin 957: 26 of cap free
Amount of items: 2
Items: 
Size: 523295 Color: 1
Size: 476680 Color: 0

Bin 958: 26 of cap free
Amount of items: 2
Items: 
Size: 524287 Color: 2
Size: 475688 Color: 1

Bin 959: 26 of cap free
Amount of items: 2
Items: 
Size: 530697 Color: 0
Size: 469278 Color: 4

Bin 960: 26 of cap free
Amount of items: 2
Items: 
Size: 535055 Color: 2
Size: 464920 Color: 4

Bin 961: 26 of cap free
Amount of items: 2
Items: 
Size: 541839 Color: 3
Size: 458136 Color: 2

Bin 962: 26 of cap free
Amount of items: 2
Items: 
Size: 558751 Color: 0
Size: 441224 Color: 4

Bin 963: 26 of cap free
Amount of items: 2
Items: 
Size: 571443 Color: 3
Size: 428532 Color: 2

Bin 964: 26 of cap free
Amount of items: 2
Items: 
Size: 578778 Color: 2
Size: 421197 Color: 1

Bin 965: 26 of cap free
Amount of items: 2
Items: 
Size: 609526 Color: 1
Size: 390449 Color: 4

Bin 966: 26 of cap free
Amount of items: 2
Items: 
Size: 609686 Color: 3
Size: 390289 Color: 1

Bin 967: 26 of cap free
Amount of items: 2
Items: 
Size: 619451 Color: 3
Size: 380524 Color: 0

Bin 968: 26 of cap free
Amount of items: 2
Items: 
Size: 625655 Color: 4
Size: 374320 Color: 3

Bin 969: 26 of cap free
Amount of items: 2
Items: 
Size: 626001 Color: 0
Size: 373974 Color: 3

Bin 970: 26 of cap free
Amount of items: 2
Items: 
Size: 650011 Color: 1
Size: 349964 Color: 3

Bin 971: 26 of cap free
Amount of items: 2
Items: 
Size: 691347 Color: 3
Size: 308628 Color: 4

Bin 972: 26 of cap free
Amount of items: 2
Items: 
Size: 693846 Color: 0
Size: 306129 Color: 3

Bin 973: 26 of cap free
Amount of items: 2
Items: 
Size: 697654 Color: 3
Size: 302321 Color: 1

Bin 974: 26 of cap free
Amount of items: 2
Items: 
Size: 699871 Color: 1
Size: 300104 Color: 2

Bin 975: 26 of cap free
Amount of items: 2
Items: 
Size: 709639 Color: 1
Size: 290336 Color: 2

Bin 976: 26 of cap free
Amount of items: 2
Items: 
Size: 710808 Color: 4
Size: 289167 Color: 3

Bin 977: 26 of cap free
Amount of items: 2
Items: 
Size: 712831 Color: 0
Size: 287144 Color: 2

Bin 978: 26 of cap free
Amount of items: 2
Items: 
Size: 725084 Color: 4
Size: 274891 Color: 2

Bin 979: 26 of cap free
Amount of items: 2
Items: 
Size: 744911 Color: 3
Size: 255064 Color: 0

Bin 980: 26 of cap free
Amount of items: 2
Items: 
Size: 764884 Color: 0
Size: 235091 Color: 2

Bin 981: 26 of cap free
Amount of items: 2
Items: 
Size: 767283 Color: 1
Size: 232692 Color: 2

Bin 982: 26 of cap free
Amount of items: 2
Items: 
Size: 782069 Color: 0
Size: 217906 Color: 3

Bin 983: 26 of cap free
Amount of items: 2
Items: 
Size: 787705 Color: 0
Size: 212270 Color: 3

Bin 984: 27 of cap free
Amount of items: 2
Items: 
Size: 507514 Color: 1
Size: 492460 Color: 2

Bin 985: 27 of cap free
Amount of items: 2
Items: 
Size: 512883 Color: 3
Size: 487091 Color: 0

Bin 986: 27 of cap free
Amount of items: 2
Items: 
Size: 518078 Color: 2
Size: 481896 Color: 1

Bin 987: 27 of cap free
Amount of items: 2
Items: 
Size: 519574 Color: 2
Size: 480400 Color: 0

Bin 988: 27 of cap free
Amount of items: 2
Items: 
Size: 532183 Color: 1
Size: 467791 Color: 0

Bin 989: 27 of cap free
Amount of items: 2
Items: 
Size: 545265 Color: 1
Size: 454709 Color: 2

Bin 990: 27 of cap free
Amount of items: 2
Items: 
Size: 569004 Color: 0
Size: 430970 Color: 2

Bin 991: 27 of cap free
Amount of items: 2
Items: 
Size: 584364 Color: 0
Size: 415610 Color: 3

Bin 992: 27 of cap free
Amount of items: 2
Items: 
Size: 594155 Color: 3
Size: 405819 Color: 1

Bin 993: 27 of cap free
Amount of items: 2
Items: 
Size: 632875 Color: 4
Size: 367099 Color: 3

Bin 994: 27 of cap free
Amount of items: 2
Items: 
Size: 638237 Color: 2
Size: 361737 Color: 0

Bin 995: 27 of cap free
Amount of items: 2
Items: 
Size: 645556 Color: 0
Size: 354418 Color: 2

Bin 996: 27 of cap free
Amount of items: 2
Items: 
Size: 663767 Color: 0
Size: 336207 Color: 3

Bin 997: 27 of cap free
Amount of items: 2
Items: 
Size: 666662 Color: 2
Size: 333312 Color: 0

Bin 998: 27 of cap free
Amount of items: 2
Items: 
Size: 686668 Color: 0
Size: 313306 Color: 4

Bin 999: 27 of cap free
Amount of items: 2
Items: 
Size: 690823 Color: 0
Size: 309151 Color: 2

Bin 1000: 27 of cap free
Amount of items: 2
Items: 
Size: 699674 Color: 2
Size: 300300 Color: 1

Bin 1001: 27 of cap free
Amount of items: 2
Items: 
Size: 700500 Color: 2
Size: 299474 Color: 1

Bin 1002: 27 of cap free
Amount of items: 2
Items: 
Size: 705094 Color: 2
Size: 294880 Color: 1

Bin 1003: 27 of cap free
Amount of items: 2
Items: 
Size: 720277 Color: 0
Size: 279697 Color: 3

Bin 1004: 27 of cap free
Amount of items: 2
Items: 
Size: 735131 Color: 1
Size: 264843 Color: 2

Bin 1005: 27 of cap free
Amount of items: 2
Items: 
Size: 746107 Color: 0
Size: 253867 Color: 4

Bin 1006: 27 of cap free
Amount of items: 2
Items: 
Size: 753014 Color: 3
Size: 246960 Color: 1

Bin 1007: 27 of cap free
Amount of items: 2
Items: 
Size: 764526 Color: 2
Size: 235448 Color: 4

Bin 1008: 27 of cap free
Amount of items: 2
Items: 
Size: 774988 Color: 4
Size: 224986 Color: 0

Bin 1009: 27 of cap free
Amount of items: 2
Items: 
Size: 782248 Color: 4
Size: 217726 Color: 2

Bin 1010: 28 of cap free
Amount of items: 2
Items: 
Size: 529326 Color: 3
Size: 470647 Color: 0

Bin 1011: 28 of cap free
Amount of items: 2
Items: 
Size: 538836 Color: 0
Size: 461137 Color: 2

Bin 1012: 28 of cap free
Amount of items: 2
Items: 
Size: 544757 Color: 3
Size: 455216 Color: 4

Bin 1013: 28 of cap free
Amount of items: 2
Items: 
Size: 569611 Color: 3
Size: 430362 Color: 2

Bin 1014: 28 of cap free
Amount of items: 2
Items: 
Size: 578877 Color: 2
Size: 421096 Color: 4

Bin 1015: 28 of cap free
Amount of items: 2
Items: 
Size: 579411 Color: 3
Size: 420562 Color: 4

Bin 1016: 28 of cap free
Amount of items: 2
Items: 
Size: 595843 Color: 3
Size: 404130 Color: 1

Bin 1017: 28 of cap free
Amount of items: 2
Items: 
Size: 604344 Color: 4
Size: 395629 Color: 1

Bin 1018: 28 of cap free
Amount of items: 2
Items: 
Size: 610431 Color: 1
Size: 389542 Color: 3

Bin 1019: 28 of cap free
Amount of items: 2
Items: 
Size: 615587 Color: 4
Size: 384386 Color: 1

Bin 1020: 28 of cap free
Amount of items: 3
Items: 
Size: 624603 Color: 0
Size: 195785 Color: 1
Size: 179585 Color: 2

Bin 1021: 28 of cap free
Amount of items: 2
Items: 
Size: 627599 Color: 3
Size: 372374 Color: 0

Bin 1022: 28 of cap free
Amount of items: 2
Items: 
Size: 640712 Color: 2
Size: 359261 Color: 3

Bin 1023: 28 of cap free
Amount of items: 2
Items: 
Size: 649316 Color: 1
Size: 350657 Color: 0

Bin 1024: 28 of cap free
Amount of items: 2
Items: 
Size: 660544 Color: 0
Size: 339429 Color: 1

Bin 1025: 28 of cap free
Amount of items: 2
Items: 
Size: 701601 Color: 0
Size: 298372 Color: 1

Bin 1026: 28 of cap free
Amount of items: 2
Items: 
Size: 707164 Color: 0
Size: 292809 Color: 4

Bin 1027: 28 of cap free
Amount of items: 2
Items: 
Size: 730154 Color: 1
Size: 269819 Color: 0

Bin 1028: 28 of cap free
Amount of items: 2
Items: 
Size: 750937 Color: 3
Size: 249036 Color: 1

Bin 1029: 28 of cap free
Amount of items: 2
Items: 
Size: 753583 Color: 0
Size: 246390 Color: 1

Bin 1030: 28 of cap free
Amount of items: 2
Items: 
Size: 781093 Color: 3
Size: 218880 Color: 0

Bin 1031: 28 of cap free
Amount of items: 2
Items: 
Size: 781768 Color: 2
Size: 218205 Color: 3

Bin 1032: 29 of cap free
Amount of items: 7
Items: 
Size: 148180 Color: 4
Size: 148107 Color: 3
Size: 148039 Color: 1
Size: 148009 Color: 0
Size: 147982 Color: 1
Size: 147957 Color: 1
Size: 111698 Color: 1

Bin 1033: 29 of cap free
Amount of items: 2
Items: 
Size: 501416 Color: 2
Size: 498556 Color: 3

Bin 1034: 29 of cap free
Amount of items: 2
Items: 
Size: 512519 Color: 0
Size: 487453 Color: 1

Bin 1035: 29 of cap free
Amount of items: 2
Items: 
Size: 542414 Color: 2
Size: 457558 Color: 3

Bin 1036: 29 of cap free
Amount of items: 2
Items: 
Size: 553146 Color: 4
Size: 446826 Color: 3

Bin 1037: 29 of cap free
Amount of items: 2
Items: 
Size: 557548 Color: 0
Size: 442424 Color: 1

Bin 1038: 29 of cap free
Amount of items: 2
Items: 
Size: 570440 Color: 4
Size: 429532 Color: 1

Bin 1039: 29 of cap free
Amount of items: 2
Items: 
Size: 570925 Color: 2
Size: 429047 Color: 4

Bin 1040: 29 of cap free
Amount of items: 2
Items: 
Size: 587534 Color: 1
Size: 412438 Color: 4

Bin 1041: 29 of cap free
Amount of items: 2
Items: 
Size: 602658 Color: 3
Size: 397314 Color: 4

Bin 1042: 29 of cap free
Amount of items: 2
Items: 
Size: 610506 Color: 0
Size: 389466 Color: 2

Bin 1043: 29 of cap free
Amount of items: 3
Items: 
Size: 618964 Color: 4
Size: 194751 Color: 2
Size: 186257 Color: 2

Bin 1044: 29 of cap free
Amount of items: 2
Items: 
Size: 637370 Color: 3
Size: 362602 Color: 1

Bin 1045: 29 of cap free
Amount of items: 2
Items: 
Size: 643520 Color: 1
Size: 356452 Color: 0

Bin 1046: 29 of cap free
Amount of items: 2
Items: 
Size: 650427 Color: 0
Size: 349545 Color: 3

Bin 1047: 29 of cap free
Amount of items: 2
Items: 
Size: 650989 Color: 3
Size: 348983 Color: 1

Bin 1048: 29 of cap free
Amount of items: 2
Items: 
Size: 651238 Color: 4
Size: 348734 Color: 2

Bin 1049: 29 of cap free
Amount of items: 2
Items: 
Size: 655740 Color: 4
Size: 344232 Color: 3

Bin 1050: 29 of cap free
Amount of items: 2
Items: 
Size: 665501 Color: 3
Size: 334471 Color: 2

Bin 1051: 29 of cap free
Amount of items: 2
Items: 
Size: 672725 Color: 3
Size: 327247 Color: 0

Bin 1052: 29 of cap free
Amount of items: 2
Items: 
Size: 673411 Color: 1
Size: 326561 Color: 0

Bin 1053: 29 of cap free
Amount of items: 2
Items: 
Size: 679252 Color: 1
Size: 320720 Color: 0

Bin 1054: 29 of cap free
Amount of items: 2
Items: 
Size: 684724 Color: 1
Size: 315248 Color: 4

Bin 1055: 29 of cap free
Amount of items: 2
Items: 
Size: 690377 Color: 0
Size: 309595 Color: 1

Bin 1056: 29 of cap free
Amount of items: 2
Items: 
Size: 691491 Color: 0
Size: 308481 Color: 3

Bin 1057: 29 of cap free
Amount of items: 2
Items: 
Size: 694551 Color: 1
Size: 305421 Color: 3

Bin 1058: 29 of cap free
Amount of items: 2
Items: 
Size: 701891 Color: 2
Size: 298081 Color: 1

Bin 1059: 29 of cap free
Amount of items: 2
Items: 
Size: 717399 Color: 1
Size: 282573 Color: 3

Bin 1060: 29 of cap free
Amount of items: 2
Items: 
Size: 727426 Color: 4
Size: 272546 Color: 0

Bin 1061: 29 of cap free
Amount of items: 2
Items: 
Size: 736654 Color: 3
Size: 263318 Color: 1

Bin 1062: 29 of cap free
Amount of items: 2
Items: 
Size: 738720 Color: 1
Size: 261252 Color: 3

Bin 1063: 29 of cap free
Amount of items: 2
Items: 
Size: 745707 Color: 4
Size: 254265 Color: 3

Bin 1064: 29 of cap free
Amount of items: 2
Items: 
Size: 799163 Color: 2
Size: 200809 Color: 0

Bin 1065: 30 of cap free
Amount of items: 2
Items: 
Size: 500557 Color: 0
Size: 499414 Color: 3

Bin 1066: 30 of cap free
Amount of items: 2
Items: 
Size: 504654 Color: 2
Size: 495317 Color: 1

Bin 1067: 30 of cap free
Amount of items: 2
Items: 
Size: 517510 Color: 0
Size: 482461 Color: 2

Bin 1068: 30 of cap free
Amount of items: 2
Items: 
Size: 518153 Color: 4
Size: 481818 Color: 2

Bin 1069: 30 of cap free
Amount of items: 2
Items: 
Size: 526054 Color: 2
Size: 473917 Color: 4

Bin 1070: 30 of cap free
Amount of items: 2
Items: 
Size: 530164 Color: 1
Size: 469807 Color: 2

Bin 1071: 30 of cap free
Amount of items: 2
Items: 
Size: 548394 Color: 1
Size: 451577 Color: 3

Bin 1072: 30 of cap free
Amount of items: 2
Items: 
Size: 583271 Color: 2
Size: 416700 Color: 0

Bin 1073: 30 of cap free
Amount of items: 2
Items: 
Size: 596041 Color: 3
Size: 403930 Color: 1

Bin 1074: 30 of cap free
Amount of items: 2
Items: 
Size: 605285 Color: 4
Size: 394686 Color: 2

Bin 1075: 30 of cap free
Amount of items: 2
Items: 
Size: 605997 Color: 1
Size: 393974 Color: 3

Bin 1076: 30 of cap free
Amount of items: 2
Items: 
Size: 608561 Color: 0
Size: 391410 Color: 1

Bin 1077: 30 of cap free
Amount of items: 2
Items: 
Size: 627008 Color: 2
Size: 372963 Color: 0

Bin 1078: 30 of cap free
Amount of items: 2
Items: 
Size: 632792 Color: 0
Size: 367179 Color: 3

Bin 1079: 30 of cap free
Amount of items: 2
Items: 
Size: 637169 Color: 1
Size: 362802 Color: 4

Bin 1080: 30 of cap free
Amount of items: 2
Items: 
Size: 638543 Color: 0
Size: 361428 Color: 4

Bin 1081: 30 of cap free
Amount of items: 2
Items: 
Size: 660994 Color: 0
Size: 338977 Color: 1

Bin 1082: 30 of cap free
Amount of items: 2
Items: 
Size: 661354 Color: 2
Size: 338617 Color: 3

Bin 1083: 30 of cap free
Amount of items: 2
Items: 
Size: 673478 Color: 1
Size: 326493 Color: 3

Bin 1084: 30 of cap free
Amount of items: 2
Items: 
Size: 686369 Color: 4
Size: 313602 Color: 3

Bin 1085: 30 of cap free
Amount of items: 2
Items: 
Size: 692653 Color: 2
Size: 307318 Color: 4

Bin 1086: 30 of cap free
Amount of items: 2
Items: 
Size: 701541 Color: 4
Size: 298430 Color: 2

Bin 1087: 30 of cap free
Amount of items: 2
Items: 
Size: 745592 Color: 2
Size: 254379 Color: 1

Bin 1088: 30 of cap free
Amount of items: 2
Items: 
Size: 756209 Color: 0
Size: 243762 Color: 1

Bin 1089: 30 of cap free
Amount of items: 2
Items: 
Size: 759066 Color: 1
Size: 240905 Color: 2

Bin 1090: 30 of cap free
Amount of items: 2
Items: 
Size: 760752 Color: 4
Size: 239219 Color: 0

Bin 1091: 30 of cap free
Amount of items: 2
Items: 
Size: 771706 Color: 1
Size: 228265 Color: 3

Bin 1092: 30 of cap free
Amount of items: 2
Items: 
Size: 774193 Color: 1
Size: 225778 Color: 2

Bin 1093: 30 of cap free
Amount of items: 2
Items: 
Size: 786347 Color: 0
Size: 213624 Color: 3

Bin 1094: 30 of cap free
Amount of items: 2
Items: 
Size: 790019 Color: 3
Size: 209952 Color: 0

Bin 1095: 31 of cap free
Amount of items: 2
Items: 
Size: 525983 Color: 4
Size: 473987 Color: 3

Bin 1096: 31 of cap free
Amount of items: 2
Items: 
Size: 549281 Color: 1
Size: 450689 Color: 0

Bin 1097: 31 of cap free
Amount of items: 2
Items: 
Size: 562551 Color: 3
Size: 437419 Color: 4

Bin 1098: 31 of cap free
Amount of items: 2
Items: 
Size: 565052 Color: 1
Size: 434918 Color: 4

Bin 1099: 31 of cap free
Amount of items: 2
Items: 
Size: 574416 Color: 3
Size: 425554 Color: 0

Bin 1100: 31 of cap free
Amount of items: 2
Items: 
Size: 579878 Color: 0
Size: 420092 Color: 2

Bin 1101: 31 of cap free
Amount of items: 2
Items: 
Size: 595268 Color: 0
Size: 404702 Color: 3

Bin 1102: 31 of cap free
Amount of items: 2
Items: 
Size: 599604 Color: 4
Size: 400366 Color: 3

Bin 1103: 31 of cap free
Amount of items: 2
Items: 
Size: 610910 Color: 0
Size: 389060 Color: 3

Bin 1104: 31 of cap free
Amount of items: 2
Items: 
Size: 631137 Color: 0
Size: 368833 Color: 3

Bin 1105: 31 of cap free
Amount of items: 2
Items: 
Size: 632110 Color: 2
Size: 367860 Color: 3

Bin 1106: 31 of cap free
Amount of items: 2
Items: 
Size: 637792 Color: 2
Size: 362178 Color: 3

Bin 1107: 31 of cap free
Amount of items: 2
Items: 
Size: 672384 Color: 3
Size: 327586 Color: 2

Bin 1108: 31 of cap free
Amount of items: 2
Items: 
Size: 673073 Color: 0
Size: 326897 Color: 1

Bin 1109: 31 of cap free
Amount of items: 2
Items: 
Size: 687680 Color: 4
Size: 312290 Color: 3

Bin 1110: 31 of cap free
Amount of items: 2
Items: 
Size: 689766 Color: 1
Size: 310204 Color: 0

Bin 1111: 31 of cap free
Amount of items: 2
Items: 
Size: 692422 Color: 2
Size: 307548 Color: 4

Bin 1112: 31 of cap free
Amount of items: 2
Items: 
Size: 704932 Color: 3
Size: 295038 Color: 4

Bin 1113: 31 of cap free
Amount of items: 2
Items: 
Size: 717650 Color: 0
Size: 282320 Color: 3

Bin 1114: 31 of cap free
Amount of items: 2
Items: 
Size: 725663 Color: 1
Size: 274307 Color: 0

Bin 1115: 31 of cap free
Amount of items: 2
Items: 
Size: 736155 Color: 2
Size: 263815 Color: 0

Bin 1116: 31 of cap free
Amount of items: 2
Items: 
Size: 760952 Color: 0
Size: 239018 Color: 1

Bin 1117: 31 of cap free
Amount of items: 2
Items: 
Size: 781936 Color: 2
Size: 218034 Color: 3

Bin 1118: 31 of cap free
Amount of items: 2
Items: 
Size: 792630 Color: 4
Size: 207340 Color: 1

Bin 1119: 31 of cap free
Amount of items: 2
Items: 
Size: 792724 Color: 0
Size: 207246 Color: 2

Bin 1120: 31 of cap free
Amount of items: 2
Items: 
Size: 794193 Color: 0
Size: 205777 Color: 2

Bin 1121: 32 of cap free
Amount of items: 7
Items: 
Size: 146042 Color: 1
Size: 145983 Color: 4
Size: 145954 Color: 0
Size: 145657 Color: 1
Size: 145656 Color: 1
Size: 145530 Color: 1
Size: 125147 Color: 2

Bin 1122: 32 of cap free
Amount of items: 2
Items: 
Size: 501751 Color: 3
Size: 498218 Color: 0

Bin 1123: 32 of cap free
Amount of items: 2
Items: 
Size: 515906 Color: 4
Size: 484063 Color: 2

Bin 1124: 32 of cap free
Amount of items: 2
Items: 
Size: 579133 Color: 3
Size: 420836 Color: 0

Bin 1125: 32 of cap free
Amount of items: 2
Items: 
Size: 588761 Color: 4
Size: 411208 Color: 2

Bin 1126: 32 of cap free
Amount of items: 2
Items: 
Size: 613483 Color: 1
Size: 386486 Color: 4

Bin 1127: 32 of cap free
Amount of items: 2
Items: 
Size: 615371 Color: 0
Size: 384598 Color: 2

Bin 1128: 32 of cap free
Amount of items: 2
Items: 
Size: 640656 Color: 4
Size: 359313 Color: 0

Bin 1129: 32 of cap free
Amount of items: 2
Items: 
Size: 645108 Color: 1
Size: 354861 Color: 3

Bin 1130: 32 of cap free
Amount of items: 2
Items: 
Size: 649315 Color: 4
Size: 350654 Color: 0

Bin 1131: 32 of cap free
Amount of items: 2
Items: 
Size: 650308 Color: 3
Size: 349661 Color: 4

Bin 1132: 32 of cap free
Amount of items: 2
Items: 
Size: 654274 Color: 3
Size: 345695 Color: 1

Bin 1133: 32 of cap free
Amount of items: 2
Items: 
Size: 660240 Color: 3
Size: 339729 Color: 0

Bin 1134: 32 of cap free
Amount of items: 2
Items: 
Size: 669222 Color: 1
Size: 330747 Color: 3

Bin 1135: 32 of cap free
Amount of items: 2
Items: 
Size: 676468 Color: 3
Size: 323501 Color: 1

Bin 1136: 32 of cap free
Amount of items: 2
Items: 
Size: 688644 Color: 0
Size: 311325 Color: 4

Bin 1137: 32 of cap free
Amount of items: 2
Items: 
Size: 704146 Color: 3
Size: 295823 Color: 0

Bin 1138: 32 of cap free
Amount of items: 2
Items: 
Size: 704398 Color: 2
Size: 295571 Color: 3

Bin 1139: 32 of cap free
Amount of items: 2
Items: 
Size: 710966 Color: 3
Size: 289003 Color: 2

Bin 1140: 32 of cap free
Amount of items: 2
Items: 
Size: 713320 Color: 1
Size: 286649 Color: 2

Bin 1141: 32 of cap free
Amount of items: 2
Items: 
Size: 724095 Color: 2
Size: 275874 Color: 4

Bin 1142: 32 of cap free
Amount of items: 2
Items: 
Size: 733089 Color: 2
Size: 266880 Color: 3

Bin 1143: 32 of cap free
Amount of items: 2
Items: 
Size: 761223 Color: 3
Size: 238746 Color: 4

Bin 1144: 32 of cap free
Amount of items: 2
Items: 
Size: 762956 Color: 3
Size: 237013 Color: 4

Bin 1145: 32 of cap free
Amount of items: 2
Items: 
Size: 776260 Color: 2
Size: 223709 Color: 0

Bin 1146: 32 of cap free
Amount of items: 2
Items: 
Size: 793683 Color: 2
Size: 206286 Color: 3

Bin 1147: 33 of cap free
Amount of items: 2
Items: 
Size: 533312 Color: 3
Size: 466656 Color: 2

Bin 1148: 33 of cap free
Amount of items: 2
Items: 
Size: 545333 Color: 1
Size: 454635 Color: 2

Bin 1149: 33 of cap free
Amount of items: 2
Items: 
Size: 546211 Color: 0
Size: 453757 Color: 4

Bin 1150: 33 of cap free
Amount of items: 2
Items: 
Size: 547925 Color: 4
Size: 452043 Color: 2

Bin 1151: 33 of cap free
Amount of items: 2
Items: 
Size: 554725 Color: 2
Size: 445243 Color: 0

Bin 1152: 33 of cap free
Amount of items: 2
Items: 
Size: 558106 Color: 1
Size: 441862 Color: 2

Bin 1153: 33 of cap free
Amount of items: 2
Items: 
Size: 560385 Color: 3
Size: 439583 Color: 0

Bin 1154: 33 of cap free
Amount of items: 2
Items: 
Size: 573922 Color: 2
Size: 426046 Color: 0

Bin 1155: 33 of cap free
Amount of items: 2
Items: 
Size: 575723 Color: 1
Size: 424245 Color: 2

Bin 1156: 33 of cap free
Amount of items: 2
Items: 
Size: 584257 Color: 1
Size: 415711 Color: 4

Bin 1157: 33 of cap free
Amount of items: 2
Items: 
Size: 601404 Color: 1
Size: 398564 Color: 3

Bin 1158: 33 of cap free
Amount of items: 2
Items: 
Size: 615200 Color: 3
Size: 384768 Color: 1

Bin 1159: 33 of cap free
Amount of items: 2
Items: 
Size: 617990 Color: 1
Size: 381978 Color: 0

Bin 1160: 33 of cap free
Amount of items: 3
Items: 
Size: 619511 Color: 0
Size: 195742 Color: 1
Size: 184715 Color: 2

Bin 1161: 33 of cap free
Amount of items: 2
Items: 
Size: 620317 Color: 4
Size: 379651 Color: 0

Bin 1162: 33 of cap free
Amount of items: 2
Items: 
Size: 636681 Color: 2
Size: 363287 Color: 1

Bin 1163: 33 of cap free
Amount of items: 2
Items: 
Size: 672159 Color: 3
Size: 327809 Color: 4

Bin 1164: 33 of cap free
Amount of items: 2
Items: 
Size: 697497 Color: 3
Size: 302471 Color: 0

Bin 1165: 33 of cap free
Amount of items: 2
Items: 
Size: 715700 Color: 4
Size: 284268 Color: 0

Bin 1166: 33 of cap free
Amount of items: 2
Items: 
Size: 718787 Color: 1
Size: 281181 Color: 3

Bin 1167: 33 of cap free
Amount of items: 2
Items: 
Size: 742482 Color: 2
Size: 257486 Color: 0

Bin 1168: 33 of cap free
Amount of items: 2
Items: 
Size: 764911 Color: 4
Size: 235057 Color: 0

Bin 1169: 33 of cap free
Amount of items: 2
Items: 
Size: 784528 Color: 1
Size: 215440 Color: 2

Bin 1170: 34 of cap free
Amount of items: 9
Items: 
Size: 111618 Color: 0
Size: 111598 Color: 1
Size: 111593 Color: 0
Size: 111405 Color: 3
Size: 111397 Color: 1
Size: 111371 Color: 1
Size: 111231 Color: 1
Size: 111187 Color: 2
Size: 108567 Color: 2

Bin 1171: 34 of cap free
Amount of items: 2
Items: 
Size: 530972 Color: 1
Size: 468995 Color: 3

Bin 1172: 34 of cap free
Amount of items: 2
Items: 
Size: 538583 Color: 2
Size: 461384 Color: 4

Bin 1173: 34 of cap free
Amount of items: 2
Items: 
Size: 553499 Color: 1
Size: 446468 Color: 2

Bin 1174: 34 of cap free
Amount of items: 2
Items: 
Size: 555813 Color: 3
Size: 444154 Color: 0

Bin 1175: 34 of cap free
Amount of items: 2
Items: 
Size: 555912 Color: 2
Size: 444055 Color: 3

Bin 1176: 34 of cap free
Amount of items: 2
Items: 
Size: 639289 Color: 4
Size: 360678 Color: 1

Bin 1177: 34 of cap free
Amount of items: 2
Items: 
Size: 644965 Color: 4
Size: 355002 Color: 2

Bin 1178: 34 of cap free
Amount of items: 2
Items: 
Size: 651830 Color: 2
Size: 348137 Color: 3

Bin 1179: 34 of cap free
Amount of items: 2
Items: 
Size: 654381 Color: 0
Size: 345586 Color: 2

Bin 1180: 34 of cap free
Amount of items: 2
Items: 
Size: 687086 Color: 2
Size: 312881 Color: 3

Bin 1181: 34 of cap free
Amount of items: 2
Items: 
Size: 695798 Color: 2
Size: 304169 Color: 4

Bin 1182: 34 of cap free
Amount of items: 2
Items: 
Size: 718969 Color: 3
Size: 280998 Color: 0

Bin 1183: 34 of cap free
Amount of items: 2
Items: 
Size: 719526 Color: 3
Size: 280441 Color: 0

Bin 1184: 34 of cap free
Amount of items: 2
Items: 
Size: 728497 Color: 2
Size: 271470 Color: 0

Bin 1185: 34 of cap free
Amount of items: 2
Items: 
Size: 739399 Color: 3
Size: 260568 Color: 4

Bin 1186: 34 of cap free
Amount of items: 2
Items: 
Size: 743325 Color: 2
Size: 256642 Color: 3

Bin 1187: 34 of cap free
Amount of items: 2
Items: 
Size: 746514 Color: 1
Size: 253453 Color: 2

Bin 1188: 34 of cap free
Amount of items: 2
Items: 
Size: 768227 Color: 0
Size: 231740 Color: 1

Bin 1189: 34 of cap free
Amount of items: 2
Items: 
Size: 777055 Color: 1
Size: 222912 Color: 3

Bin 1190: 34 of cap free
Amount of items: 2
Items: 
Size: 786796 Color: 4
Size: 213171 Color: 3

Bin 1191: 34 of cap free
Amount of items: 2
Items: 
Size: 791621 Color: 4
Size: 208346 Color: 3

Bin 1192: 34 of cap free
Amount of items: 2
Items: 
Size: 795578 Color: 4
Size: 204389 Color: 1

Bin 1193: 35 of cap free
Amount of items: 7
Items: 
Size: 145218 Color: 3
Size: 145187 Color: 4
Size: 145114 Color: 1
Size: 145079 Color: 2
Size: 144829 Color: 3
Size: 144779 Color: 2
Size: 129760 Color: 1

Bin 1194: 35 of cap free
Amount of items: 6
Items: 
Size: 167554 Color: 0
Size: 167395 Color: 1
Size: 167212 Color: 4
Size: 167143 Color: 4
Size: 167007 Color: 3
Size: 163655 Color: 1

Bin 1195: 35 of cap free
Amount of items: 2
Items: 
Size: 510619 Color: 1
Size: 489347 Color: 2

Bin 1196: 35 of cap free
Amount of items: 2
Items: 
Size: 513060 Color: 0
Size: 486906 Color: 4

Bin 1197: 35 of cap free
Amount of items: 2
Items: 
Size: 519777 Color: 0
Size: 480189 Color: 3

Bin 1198: 35 of cap free
Amount of items: 2
Items: 
Size: 540201 Color: 3
Size: 459765 Color: 0

Bin 1199: 35 of cap free
Amount of items: 2
Items: 
Size: 573847 Color: 3
Size: 426119 Color: 0

Bin 1200: 35 of cap free
Amount of items: 2
Items: 
Size: 587337 Color: 0
Size: 412629 Color: 4

Bin 1201: 35 of cap free
Amount of items: 2
Items: 
Size: 596933 Color: 4
Size: 403033 Color: 1

Bin 1202: 35 of cap free
Amount of items: 2
Items: 
Size: 604449 Color: 1
Size: 395517 Color: 0

Bin 1203: 35 of cap free
Amount of items: 2
Items: 
Size: 606233 Color: 4
Size: 393733 Color: 0

Bin 1204: 35 of cap free
Amount of items: 2
Items: 
Size: 614170 Color: 3
Size: 385796 Color: 2

Bin 1205: 35 of cap free
Amount of items: 2
Items: 
Size: 619347 Color: 2
Size: 380619 Color: 4

Bin 1206: 35 of cap free
Amount of items: 2
Items: 
Size: 641494 Color: 4
Size: 358472 Color: 3

Bin 1207: 35 of cap free
Amount of items: 2
Items: 
Size: 656444 Color: 0
Size: 343522 Color: 1

Bin 1208: 35 of cap free
Amount of items: 2
Items: 
Size: 657208 Color: 0
Size: 342758 Color: 1

Bin 1209: 35 of cap free
Amount of items: 2
Items: 
Size: 664126 Color: 1
Size: 335840 Color: 2

Bin 1210: 35 of cap free
Amount of items: 2
Items: 
Size: 674897 Color: 2
Size: 325069 Color: 4

Bin 1211: 35 of cap free
Amount of items: 2
Items: 
Size: 678338 Color: 4
Size: 321628 Color: 3

Bin 1212: 35 of cap free
Amount of items: 2
Items: 
Size: 679499 Color: 1
Size: 320467 Color: 2

Bin 1213: 35 of cap free
Amount of items: 2
Items: 
Size: 681570 Color: 4
Size: 318396 Color: 0

Bin 1214: 35 of cap free
Amount of items: 2
Items: 
Size: 682923 Color: 3
Size: 317043 Color: 1

Bin 1215: 35 of cap free
Amount of items: 2
Items: 
Size: 718194 Color: 1
Size: 281772 Color: 2

Bin 1216: 35 of cap free
Amount of items: 2
Items: 
Size: 725529 Color: 3
Size: 274437 Color: 2

Bin 1217: 35 of cap free
Amount of items: 2
Items: 
Size: 731542 Color: 2
Size: 268424 Color: 3

Bin 1218: 35 of cap free
Amount of items: 2
Items: 
Size: 740119 Color: 0
Size: 259847 Color: 2

Bin 1219: 35 of cap free
Amount of items: 2
Items: 
Size: 740870 Color: 4
Size: 259096 Color: 2

Bin 1220: 35 of cap free
Amount of items: 2
Items: 
Size: 768768 Color: 2
Size: 231198 Color: 4

Bin 1221: 35 of cap free
Amount of items: 2
Items: 
Size: 790105 Color: 2
Size: 209861 Color: 3

Bin 1222: 36 of cap free
Amount of items: 2
Items: 
Size: 523914 Color: 0
Size: 476051 Color: 1

Bin 1223: 36 of cap free
Amount of items: 2
Items: 
Size: 524189 Color: 3
Size: 475776 Color: 4

Bin 1224: 36 of cap free
Amount of items: 2
Items: 
Size: 528262 Color: 2
Size: 471703 Color: 1

Bin 1225: 36 of cap free
Amount of items: 2
Items: 
Size: 528805 Color: 3
Size: 471160 Color: 2

Bin 1226: 36 of cap free
Amount of items: 2
Items: 
Size: 537735 Color: 2
Size: 462230 Color: 0

Bin 1227: 36 of cap free
Amount of items: 2
Items: 
Size: 549231 Color: 0
Size: 450734 Color: 1

Bin 1228: 36 of cap free
Amount of items: 2
Items: 
Size: 555332 Color: 1
Size: 444633 Color: 4

Bin 1229: 36 of cap free
Amount of items: 2
Items: 
Size: 563077 Color: 1
Size: 436888 Color: 4

Bin 1230: 36 of cap free
Amount of items: 2
Items: 
Size: 571992 Color: 3
Size: 427973 Color: 2

Bin 1231: 36 of cap free
Amount of items: 2
Items: 
Size: 575629 Color: 4
Size: 424336 Color: 1

Bin 1232: 36 of cap free
Amount of items: 2
Items: 
Size: 615581 Color: 0
Size: 384384 Color: 1

Bin 1233: 36 of cap free
Amount of items: 2
Items: 
Size: 633141 Color: 0
Size: 366824 Color: 1

Bin 1234: 36 of cap free
Amount of items: 2
Items: 
Size: 650810 Color: 3
Size: 349155 Color: 2

Bin 1235: 36 of cap free
Amount of items: 2
Items: 
Size: 653507 Color: 4
Size: 346458 Color: 2

Bin 1236: 36 of cap free
Amount of items: 2
Items: 
Size: 668574 Color: 4
Size: 331391 Color: 3

Bin 1237: 36 of cap free
Amount of items: 2
Items: 
Size: 668677 Color: 0
Size: 331288 Color: 4

Bin 1238: 36 of cap free
Amount of items: 2
Items: 
Size: 708384 Color: 2
Size: 291581 Color: 1

Bin 1239: 36 of cap free
Amount of items: 2
Items: 
Size: 726431 Color: 4
Size: 273534 Color: 2

Bin 1240: 36 of cap free
Amount of items: 2
Items: 
Size: 742386 Color: 4
Size: 257579 Color: 2

Bin 1241: 36 of cap free
Amount of items: 2
Items: 
Size: 754746 Color: 4
Size: 245219 Color: 3

Bin 1242: 36 of cap free
Amount of items: 2
Items: 
Size: 758414 Color: 4
Size: 241551 Color: 3

Bin 1243: 36 of cap free
Amount of items: 2
Items: 
Size: 766618 Color: 3
Size: 233347 Color: 4

Bin 1244: 36 of cap free
Amount of items: 2
Items: 
Size: 770480 Color: 2
Size: 229485 Color: 0

Bin 1245: 36 of cap free
Amount of items: 2
Items: 
Size: 776195 Color: 4
Size: 223770 Color: 1

Bin 1246: 36 of cap free
Amount of items: 2
Items: 
Size: 785612 Color: 0
Size: 214353 Color: 1

Bin 1247: 37 of cap free
Amount of items: 6
Items: 
Size: 169854 Color: 1
Size: 169799 Color: 3
Size: 169780 Color: 1
Size: 169701 Color: 4
Size: 169672 Color: 2
Size: 151158 Color: 4

Bin 1248: 37 of cap free
Amount of items: 6
Items: 
Size: 172750 Color: 3
Size: 172726 Color: 2
Size: 172456 Color: 2
Size: 172436 Color: 3
Size: 172398 Color: 2
Size: 137198 Color: 4

Bin 1249: 37 of cap free
Amount of items: 2
Items: 
Size: 508245 Color: 4
Size: 491719 Color: 2

Bin 1250: 37 of cap free
Amount of items: 2
Items: 
Size: 513560 Color: 0
Size: 486404 Color: 3

Bin 1251: 37 of cap free
Amount of items: 2
Items: 
Size: 531379 Color: 1
Size: 468585 Color: 3

Bin 1252: 37 of cap free
Amount of items: 2
Items: 
Size: 561789 Color: 3
Size: 438175 Color: 4

Bin 1253: 37 of cap free
Amount of items: 2
Items: 
Size: 562082 Color: 1
Size: 437882 Color: 4

Bin 1254: 37 of cap free
Amount of items: 2
Items: 
Size: 563188 Color: 2
Size: 436776 Color: 1

Bin 1255: 37 of cap free
Amount of items: 2
Items: 
Size: 567652 Color: 4
Size: 432312 Color: 1

Bin 1256: 37 of cap free
Amount of items: 2
Items: 
Size: 571213 Color: 0
Size: 428751 Color: 3

Bin 1257: 37 of cap free
Amount of items: 2
Items: 
Size: 578764 Color: 1
Size: 421200 Color: 2

Bin 1258: 37 of cap free
Amount of items: 2
Items: 
Size: 624058 Color: 0
Size: 375906 Color: 4

Bin 1259: 37 of cap free
Amount of items: 2
Items: 
Size: 625998 Color: 1
Size: 373966 Color: 4

Bin 1260: 37 of cap free
Amount of items: 2
Items: 
Size: 627476 Color: 3
Size: 372488 Color: 1

Bin 1261: 37 of cap free
Amount of items: 2
Items: 
Size: 639866 Color: 4
Size: 360098 Color: 3

Bin 1262: 37 of cap free
Amount of items: 2
Items: 
Size: 650904 Color: 1
Size: 349060 Color: 3

Bin 1263: 37 of cap free
Amount of items: 2
Items: 
Size: 662550 Color: 2
Size: 337414 Color: 3

Bin 1264: 37 of cap free
Amount of items: 2
Items: 
Size: 662676 Color: 2
Size: 337288 Color: 4

Bin 1265: 37 of cap free
Amount of items: 2
Items: 
Size: 663452 Color: 1
Size: 336512 Color: 3

Bin 1266: 37 of cap free
Amount of items: 2
Items: 
Size: 669003 Color: 0
Size: 330961 Color: 1

Bin 1267: 37 of cap free
Amount of items: 2
Items: 
Size: 686994 Color: 0
Size: 312970 Color: 1

Bin 1268: 37 of cap free
Amount of items: 2
Items: 
Size: 689558 Color: 0
Size: 310406 Color: 4

Bin 1269: 37 of cap free
Amount of items: 2
Items: 
Size: 692897 Color: 4
Size: 307067 Color: 2

Bin 1270: 37 of cap free
Amount of items: 2
Items: 
Size: 713266 Color: 1
Size: 286698 Color: 0

Bin 1271: 37 of cap free
Amount of items: 2
Items: 
Size: 752953 Color: 3
Size: 247011 Color: 2

Bin 1272: 37 of cap free
Amount of items: 2
Items: 
Size: 769990 Color: 4
Size: 229974 Color: 0

Bin 1273: 38 of cap free
Amount of items: 2
Items: 
Size: 520124 Color: 1
Size: 479839 Color: 2

Bin 1274: 38 of cap free
Amount of items: 2
Items: 
Size: 528935 Color: 0
Size: 471028 Color: 4

Bin 1275: 38 of cap free
Amount of items: 2
Items: 
Size: 548081 Color: 4
Size: 451882 Color: 2

Bin 1276: 38 of cap free
Amount of items: 2
Items: 
Size: 549836 Color: 1
Size: 450127 Color: 4

Bin 1277: 38 of cap free
Amount of items: 2
Items: 
Size: 563851 Color: 3
Size: 436112 Color: 4

Bin 1278: 38 of cap free
Amount of items: 2
Items: 
Size: 592552 Color: 0
Size: 407411 Color: 1

Bin 1279: 38 of cap free
Amount of items: 2
Items: 
Size: 619215 Color: 3
Size: 380748 Color: 2

Bin 1280: 38 of cap free
Amount of items: 2
Items: 
Size: 645199 Color: 3
Size: 354764 Color: 4

Bin 1281: 38 of cap free
Amount of items: 2
Items: 
Size: 646521 Color: 4
Size: 353442 Color: 0

Bin 1282: 38 of cap free
Amount of items: 2
Items: 
Size: 658043 Color: 3
Size: 341920 Color: 0

Bin 1283: 38 of cap free
Amount of items: 2
Items: 
Size: 668320 Color: 0
Size: 331643 Color: 3

Bin 1284: 38 of cap free
Amount of items: 2
Items: 
Size: 669082 Color: 2
Size: 330881 Color: 4

Bin 1285: 38 of cap free
Amount of items: 2
Items: 
Size: 671539 Color: 2
Size: 328424 Color: 0

Bin 1286: 38 of cap free
Amount of items: 2
Items: 
Size: 709559 Color: 2
Size: 290404 Color: 0

Bin 1287: 38 of cap free
Amount of items: 2
Items: 
Size: 711815 Color: 0
Size: 288148 Color: 2

Bin 1288: 38 of cap free
Amount of items: 2
Items: 
Size: 734090 Color: 4
Size: 265873 Color: 3

Bin 1289: 38 of cap free
Amount of items: 2
Items: 
Size: 744988 Color: 2
Size: 254975 Color: 3

Bin 1290: 38 of cap free
Amount of items: 2
Items: 
Size: 756900 Color: 0
Size: 243063 Color: 3

Bin 1291: 38 of cap free
Amount of items: 2
Items: 
Size: 768420 Color: 3
Size: 231543 Color: 0

Bin 1292: 38 of cap free
Amount of items: 2
Items: 
Size: 785446 Color: 2
Size: 214517 Color: 1

Bin 1293: 38 of cap free
Amount of items: 2
Items: 
Size: 786346 Color: 2
Size: 213617 Color: 3

Bin 1294: 39 of cap free
Amount of items: 2
Items: 
Size: 506933 Color: 1
Size: 493029 Color: 3

Bin 1295: 39 of cap free
Amount of items: 2
Items: 
Size: 522214 Color: 3
Size: 477748 Color: 4

Bin 1296: 39 of cap free
Amount of items: 2
Items: 
Size: 527162 Color: 4
Size: 472800 Color: 1

Bin 1297: 39 of cap free
Amount of items: 2
Items: 
Size: 573477 Color: 4
Size: 426485 Color: 0

Bin 1298: 39 of cap free
Amount of items: 2
Items: 
Size: 573743 Color: 3
Size: 426219 Color: 2

Bin 1299: 39 of cap free
Amount of items: 2
Items: 
Size: 579570 Color: 3
Size: 420392 Color: 4

Bin 1300: 39 of cap free
Amount of items: 2
Items: 
Size: 591755 Color: 0
Size: 408207 Color: 2

Bin 1301: 39 of cap free
Amount of items: 2
Items: 
Size: 592350 Color: 2
Size: 407612 Color: 3

Bin 1302: 39 of cap free
Amount of items: 2
Items: 
Size: 599225 Color: 4
Size: 400737 Color: 0

Bin 1303: 39 of cap free
Amount of items: 2
Items: 
Size: 609314 Color: 1
Size: 390648 Color: 4

Bin 1304: 39 of cap free
Amount of items: 2
Items: 
Size: 614702 Color: 1
Size: 385260 Color: 2

Bin 1305: 39 of cap free
Amount of items: 2
Items: 
Size: 616660 Color: 0
Size: 383302 Color: 4

Bin 1306: 39 of cap free
Amount of items: 2
Items: 
Size: 618159 Color: 2
Size: 381803 Color: 4

Bin 1307: 39 of cap free
Amount of items: 2
Items: 
Size: 626536 Color: 2
Size: 373426 Color: 0

Bin 1308: 39 of cap free
Amount of items: 2
Items: 
Size: 649719 Color: 1
Size: 350243 Color: 3

Bin 1309: 39 of cap free
Amount of items: 2
Items: 
Size: 682342 Color: 1
Size: 317620 Color: 4

Bin 1310: 39 of cap free
Amount of items: 2
Items: 
Size: 700492 Color: 2
Size: 299470 Color: 4

Bin 1311: 39 of cap free
Amount of items: 2
Items: 
Size: 702359 Color: 3
Size: 297603 Color: 0

Bin 1312: 39 of cap free
Amount of items: 2
Items: 
Size: 710121 Color: 3
Size: 289841 Color: 4

Bin 1313: 39 of cap free
Amount of items: 2
Items: 
Size: 710997 Color: 2
Size: 288965 Color: 3

Bin 1314: 39 of cap free
Amount of items: 2
Items: 
Size: 727753 Color: 4
Size: 272209 Color: 1

Bin 1315: 39 of cap free
Amount of items: 2
Items: 
Size: 729472 Color: 0
Size: 270490 Color: 3

Bin 1316: 39 of cap free
Amount of items: 2
Items: 
Size: 735589 Color: 0
Size: 264373 Color: 4

Bin 1317: 39 of cap free
Amount of items: 2
Items: 
Size: 736731 Color: 2
Size: 263231 Color: 1

Bin 1318: 39 of cap free
Amount of items: 2
Items: 
Size: 752717 Color: 1
Size: 247245 Color: 0

Bin 1319: 39 of cap free
Amount of items: 2
Items: 
Size: 753484 Color: 1
Size: 246478 Color: 4

Bin 1320: 39 of cap free
Amount of items: 2
Items: 
Size: 753872 Color: 3
Size: 246090 Color: 2

Bin 1321: 39 of cap free
Amount of items: 2
Items: 
Size: 758621 Color: 1
Size: 241341 Color: 4

Bin 1322: 40 of cap free
Amount of items: 4
Items: 
Size: 495600 Color: 1
Size: 188569 Color: 4
Size: 188566 Color: 2
Size: 127226 Color: 1

Bin 1323: 40 of cap free
Amount of items: 2
Items: 
Size: 500157 Color: 2
Size: 499804 Color: 4

Bin 1324: 40 of cap free
Amount of items: 2
Items: 
Size: 504791 Color: 3
Size: 495170 Color: 0

Bin 1325: 40 of cap free
Amount of items: 2
Items: 
Size: 511136 Color: 3
Size: 488825 Color: 2

Bin 1326: 40 of cap free
Amount of items: 2
Items: 
Size: 545978 Color: 3
Size: 453983 Color: 0

Bin 1327: 40 of cap free
Amount of items: 2
Items: 
Size: 552568 Color: 2
Size: 447393 Color: 4

Bin 1328: 40 of cap free
Amount of items: 2
Items: 
Size: 558193 Color: 0
Size: 441768 Color: 2

Bin 1329: 40 of cap free
Amount of items: 2
Items: 
Size: 561991 Color: 3
Size: 437970 Color: 0

Bin 1330: 40 of cap free
Amount of items: 2
Items: 
Size: 577706 Color: 4
Size: 422255 Color: 3

Bin 1331: 40 of cap free
Amount of items: 2
Items: 
Size: 580234 Color: 3
Size: 419727 Color: 2

Bin 1332: 40 of cap free
Amount of items: 2
Items: 
Size: 580848 Color: 1
Size: 419113 Color: 2

Bin 1333: 40 of cap free
Amount of items: 2
Items: 
Size: 587833 Color: 3
Size: 412128 Color: 2

Bin 1334: 40 of cap free
Amount of items: 2
Items: 
Size: 591856 Color: 3
Size: 408105 Color: 0

Bin 1335: 40 of cap free
Amount of items: 2
Items: 
Size: 601913 Color: 0
Size: 398048 Color: 1

Bin 1336: 40 of cap free
Amount of items: 2
Items: 
Size: 607361 Color: 1
Size: 392600 Color: 3

Bin 1337: 40 of cap free
Amount of items: 2
Items: 
Size: 615821 Color: 3
Size: 384140 Color: 0

Bin 1338: 40 of cap free
Amount of items: 2
Items: 
Size: 643338 Color: 4
Size: 356623 Color: 3

Bin 1339: 40 of cap free
Amount of items: 2
Items: 
Size: 667699 Color: 1
Size: 332262 Color: 2

Bin 1340: 40 of cap free
Amount of items: 2
Items: 
Size: 670762 Color: 4
Size: 329199 Color: 2

Bin 1341: 40 of cap free
Amount of items: 2
Items: 
Size: 673314 Color: 4
Size: 326647 Color: 0

Bin 1342: 40 of cap free
Amount of items: 2
Items: 
Size: 681781 Color: 3
Size: 318180 Color: 4

Bin 1343: 40 of cap free
Amount of items: 2
Items: 
Size: 699775 Color: 3
Size: 300186 Color: 2

Bin 1344: 40 of cap free
Amount of items: 2
Items: 
Size: 719837 Color: 1
Size: 280124 Color: 3

Bin 1345: 40 of cap free
Amount of items: 2
Items: 
Size: 721786 Color: 4
Size: 278175 Color: 1

Bin 1346: 40 of cap free
Amount of items: 2
Items: 
Size: 766245 Color: 2
Size: 233716 Color: 0

Bin 1347: 40 of cap free
Amount of items: 2
Items: 
Size: 774558 Color: 1
Size: 225403 Color: 0

Bin 1348: 41 of cap free
Amount of items: 2
Items: 
Size: 502032 Color: 2
Size: 497928 Color: 0

Bin 1349: 41 of cap free
Amount of items: 2
Items: 
Size: 502550 Color: 2
Size: 497410 Color: 1

Bin 1350: 41 of cap free
Amount of items: 2
Items: 
Size: 503395 Color: 1
Size: 496565 Color: 2

Bin 1351: 41 of cap free
Amount of items: 2
Items: 
Size: 504435 Color: 0
Size: 495525 Color: 2

Bin 1352: 41 of cap free
Amount of items: 2
Items: 
Size: 507981 Color: 4
Size: 491979 Color: 1

Bin 1353: 41 of cap free
Amount of items: 2
Items: 
Size: 534556 Color: 0
Size: 465404 Color: 3

Bin 1354: 41 of cap free
Amount of items: 2
Items: 
Size: 544657 Color: 3
Size: 455303 Color: 4

Bin 1355: 41 of cap free
Amount of items: 2
Items: 
Size: 580445 Color: 4
Size: 419515 Color: 0

Bin 1356: 41 of cap free
Amount of items: 2
Items: 
Size: 581342 Color: 2
Size: 418618 Color: 0

Bin 1357: 41 of cap free
Amount of items: 2
Items: 
Size: 582415 Color: 3
Size: 417545 Color: 4

Bin 1358: 41 of cap free
Amount of items: 2
Items: 
Size: 596756 Color: 1
Size: 403204 Color: 4

Bin 1359: 41 of cap free
Amount of items: 2
Items: 
Size: 601823 Color: 1
Size: 398137 Color: 3

Bin 1360: 41 of cap free
Amount of items: 2
Items: 
Size: 623270 Color: 1
Size: 376690 Color: 2

Bin 1361: 41 of cap free
Amount of items: 2
Items: 
Size: 629492 Color: 3
Size: 370468 Color: 0

Bin 1362: 41 of cap free
Amount of items: 2
Items: 
Size: 632704 Color: 4
Size: 367256 Color: 1

Bin 1363: 41 of cap free
Amount of items: 2
Items: 
Size: 634451 Color: 4
Size: 365509 Color: 2

Bin 1364: 41 of cap free
Amount of items: 2
Items: 
Size: 639613 Color: 4
Size: 360347 Color: 1

Bin 1365: 41 of cap free
Amount of items: 2
Items: 
Size: 652525 Color: 2
Size: 347435 Color: 1

Bin 1366: 41 of cap free
Amount of items: 2
Items: 
Size: 674075 Color: 3
Size: 325885 Color: 4

Bin 1367: 41 of cap free
Amount of items: 2
Items: 
Size: 687965 Color: 2
Size: 311995 Color: 1

Bin 1368: 41 of cap free
Amount of items: 2
Items: 
Size: 697609 Color: 0
Size: 302351 Color: 4

Bin 1369: 41 of cap free
Amount of items: 2
Items: 
Size: 707281 Color: 0
Size: 292679 Color: 4

Bin 1370: 41 of cap free
Amount of items: 2
Items: 
Size: 736648 Color: 4
Size: 263312 Color: 0

Bin 1371: 41 of cap free
Amount of items: 2
Items: 
Size: 744077 Color: 1
Size: 255883 Color: 0

Bin 1372: 41 of cap free
Amount of items: 2
Items: 
Size: 746297 Color: 0
Size: 253663 Color: 3

Bin 1373: 41 of cap free
Amount of items: 2
Items: 
Size: 758969 Color: 4
Size: 240991 Color: 1

Bin 1374: 41 of cap free
Amount of items: 2
Items: 
Size: 782931 Color: 0
Size: 217029 Color: 4

Bin 1375: 41 of cap free
Amount of items: 2
Items: 
Size: 784954 Color: 1
Size: 215006 Color: 2

Bin 1376: 41 of cap free
Amount of items: 2
Items: 
Size: 794841 Color: 3
Size: 205119 Color: 4

Bin 1377: 42 of cap free
Amount of items: 6
Items: 
Size: 178576 Color: 3
Size: 178523 Color: 3
Size: 178493 Color: 4
Size: 178468 Color: 2
Size: 178313 Color: 2
Size: 107586 Color: 1

Bin 1378: 42 of cap free
Amount of items: 2
Items: 
Size: 505662 Color: 4
Size: 494297 Color: 0

Bin 1379: 42 of cap free
Amount of items: 2
Items: 
Size: 506490 Color: 1
Size: 493469 Color: 4

Bin 1380: 42 of cap free
Amount of items: 2
Items: 
Size: 517976 Color: 1
Size: 481983 Color: 0

Bin 1381: 42 of cap free
Amount of items: 2
Items: 
Size: 530175 Color: 2
Size: 469784 Color: 1

Bin 1382: 42 of cap free
Amount of items: 2
Items: 
Size: 546965 Color: 3
Size: 452994 Color: 0

Bin 1383: 42 of cap free
Amount of items: 2
Items: 
Size: 569466 Color: 4
Size: 430493 Color: 1

Bin 1384: 42 of cap free
Amount of items: 2
Items: 
Size: 582154 Color: 2
Size: 417805 Color: 1

Bin 1385: 42 of cap free
Amount of items: 2
Items: 
Size: 593039 Color: 1
Size: 406920 Color: 2

Bin 1386: 42 of cap free
Amount of items: 2
Items: 
Size: 601457 Color: 1
Size: 398502 Color: 2

Bin 1387: 42 of cap free
Amount of items: 2
Items: 
Size: 613317 Color: 2
Size: 386642 Color: 4

Bin 1388: 42 of cap free
Amount of items: 2
Items: 
Size: 664424 Color: 4
Size: 335535 Color: 3

Bin 1389: 42 of cap free
Amount of items: 2
Items: 
Size: 702709 Color: 4
Size: 297250 Color: 3

Bin 1390: 42 of cap free
Amount of items: 2
Items: 
Size: 704770 Color: 1
Size: 295189 Color: 4

Bin 1391: 42 of cap free
Amount of items: 2
Items: 
Size: 711099 Color: 3
Size: 288860 Color: 1

Bin 1392: 42 of cap free
Amount of items: 2
Items: 
Size: 725947 Color: 3
Size: 274012 Color: 2

Bin 1393: 42 of cap free
Amount of items: 2
Items: 
Size: 762947 Color: 0
Size: 237012 Color: 4

Bin 1394: 42 of cap free
Amount of items: 2
Items: 
Size: 766162 Color: 4
Size: 233797 Color: 1

Bin 1395: 42 of cap free
Amount of items: 2
Items: 
Size: 783336 Color: 2
Size: 216623 Color: 0

Bin 1396: 43 of cap free
Amount of items: 7
Items: 
Size: 146239 Color: 2
Size: 146196 Color: 2
Size: 146140 Color: 0
Size: 146169 Color: 2
Size: 146085 Color: 0
Size: 146050 Color: 4
Size: 123079 Color: 3

Bin 1397: 43 of cap free
Amount of items: 2
Items: 
Size: 500088 Color: 3
Size: 499870 Color: 0

Bin 1398: 43 of cap free
Amount of items: 2
Items: 
Size: 516030 Color: 1
Size: 483928 Color: 0

Bin 1399: 43 of cap free
Amount of items: 2
Items: 
Size: 536234 Color: 4
Size: 463724 Color: 0

Bin 1400: 43 of cap free
Amount of items: 2
Items: 
Size: 540270 Color: 3
Size: 459688 Color: 2

Bin 1401: 43 of cap free
Amount of items: 2
Items: 
Size: 560559 Color: 3
Size: 439399 Color: 0

Bin 1402: 43 of cap free
Amount of items: 2
Items: 
Size: 588088 Color: 2
Size: 411870 Color: 1

Bin 1403: 43 of cap free
Amount of items: 2
Items: 
Size: 609383 Color: 1
Size: 390575 Color: 0

Bin 1404: 43 of cap free
Amount of items: 2
Items: 
Size: 614902 Color: 4
Size: 385056 Color: 3

Bin 1405: 43 of cap free
Amount of items: 2
Items: 
Size: 614992 Color: 4
Size: 384966 Color: 0

Bin 1406: 43 of cap free
Amount of items: 2
Items: 
Size: 626088 Color: 3
Size: 373870 Color: 1

Bin 1407: 43 of cap free
Amount of items: 2
Items: 
Size: 673110 Color: 1
Size: 326848 Color: 2

Bin 1408: 43 of cap free
Amount of items: 2
Items: 
Size: 674231 Color: 3
Size: 325727 Color: 4

Bin 1409: 43 of cap free
Amount of items: 2
Items: 
Size: 678925 Color: 4
Size: 321033 Color: 3

Bin 1410: 43 of cap free
Amount of items: 2
Items: 
Size: 680769 Color: 0
Size: 319189 Color: 4

Bin 1411: 43 of cap free
Amount of items: 2
Items: 
Size: 694045 Color: 0
Size: 305913 Color: 4

Bin 1412: 43 of cap free
Amount of items: 2
Items: 
Size: 698730 Color: 1
Size: 301228 Color: 0

Bin 1413: 43 of cap free
Amount of items: 2
Items: 
Size: 706174 Color: 1
Size: 293784 Color: 3

Bin 1414: 43 of cap free
Amount of items: 2
Items: 
Size: 728858 Color: 0
Size: 271100 Color: 1

Bin 1415: 43 of cap free
Amount of items: 2
Items: 
Size: 730191 Color: 3
Size: 269767 Color: 1

Bin 1416: 43 of cap free
Amount of items: 2
Items: 
Size: 799339 Color: 4
Size: 200619 Color: 1

Bin 1417: 44 of cap free
Amount of items: 2
Items: 
Size: 500618 Color: 4
Size: 499339 Color: 3

Bin 1418: 44 of cap free
Amount of items: 2
Items: 
Size: 510150 Color: 1
Size: 489807 Color: 4

Bin 1419: 44 of cap free
Amount of items: 2
Items: 
Size: 517841 Color: 3
Size: 482116 Color: 0

Bin 1420: 44 of cap free
Amount of items: 2
Items: 
Size: 544084 Color: 3
Size: 455873 Color: 4

Bin 1421: 44 of cap free
Amount of items: 2
Items: 
Size: 548986 Color: 4
Size: 450971 Color: 3

Bin 1422: 44 of cap free
Amount of items: 2
Items: 
Size: 556022 Color: 1
Size: 443935 Color: 3

Bin 1423: 44 of cap free
Amount of items: 2
Items: 
Size: 568707 Color: 3
Size: 431250 Color: 2

Bin 1424: 44 of cap free
Amount of items: 2
Items: 
Size: 578322 Color: 4
Size: 421635 Color: 2

Bin 1425: 44 of cap free
Amount of items: 2
Items: 
Size: 590336 Color: 4
Size: 409621 Color: 0

Bin 1426: 44 of cap free
Amount of items: 2
Items: 
Size: 590881 Color: 3
Size: 409076 Color: 2

Bin 1427: 44 of cap free
Amount of items: 2
Items: 
Size: 600199 Color: 3
Size: 399758 Color: 2

Bin 1428: 44 of cap free
Amount of items: 2
Items: 
Size: 600899 Color: 0
Size: 399058 Color: 1

Bin 1429: 44 of cap free
Amount of items: 2
Items: 
Size: 608608 Color: 2
Size: 391349 Color: 4

Bin 1430: 44 of cap free
Amount of items: 2
Items: 
Size: 617574 Color: 2
Size: 382383 Color: 1

Bin 1431: 44 of cap free
Amount of items: 2
Items: 
Size: 668140 Color: 0
Size: 331817 Color: 4

Bin 1432: 44 of cap free
Amount of items: 2
Items: 
Size: 670959 Color: 0
Size: 328998 Color: 4

Bin 1433: 44 of cap free
Amount of items: 2
Items: 
Size: 677443 Color: 4
Size: 322514 Color: 2

Bin 1434: 44 of cap free
Amount of items: 2
Items: 
Size: 682186 Color: 2
Size: 317771 Color: 0

Bin 1435: 44 of cap free
Amount of items: 2
Items: 
Size: 684622 Color: 2
Size: 315335 Color: 0

Bin 1436: 44 of cap free
Amount of items: 2
Items: 
Size: 691344 Color: 1
Size: 308613 Color: 4

Bin 1437: 44 of cap free
Amount of items: 2
Items: 
Size: 717143 Color: 3
Size: 282814 Color: 0

Bin 1438: 44 of cap free
Amount of items: 2
Items: 
Size: 732301 Color: 2
Size: 267656 Color: 1

Bin 1439: 44 of cap free
Amount of items: 2
Items: 
Size: 787615 Color: 0
Size: 212342 Color: 1

Bin 1440: 44 of cap free
Amount of items: 2
Items: 
Size: 790880 Color: 0
Size: 209077 Color: 4

Bin 1441: 44 of cap free
Amount of items: 2
Items: 
Size: 798004 Color: 2
Size: 201953 Color: 1

Bin 1442: 45 of cap free
Amount of items: 2
Items: 
Size: 502826 Color: 1
Size: 497130 Color: 0

Bin 1443: 45 of cap free
Amount of items: 2
Items: 
Size: 528031 Color: 4
Size: 471925 Color: 3

Bin 1444: 45 of cap free
Amount of items: 2
Items: 
Size: 529730 Color: 0
Size: 470226 Color: 2

Bin 1445: 45 of cap free
Amount of items: 2
Items: 
Size: 537193 Color: 1
Size: 462763 Color: 0

Bin 1446: 45 of cap free
Amount of items: 2
Items: 
Size: 569495 Color: 1
Size: 430461 Color: 0

Bin 1447: 45 of cap free
Amount of items: 2
Items: 
Size: 570821 Color: 1
Size: 429135 Color: 3

Bin 1448: 45 of cap free
Amount of items: 2
Items: 
Size: 571040 Color: 2
Size: 428916 Color: 1

Bin 1449: 45 of cap free
Amount of items: 2
Items: 
Size: 597367 Color: 1
Size: 402589 Color: 0

Bin 1450: 45 of cap free
Amount of items: 2
Items: 
Size: 627004 Color: 1
Size: 372952 Color: 4

Bin 1451: 45 of cap free
Amount of items: 2
Items: 
Size: 670150 Color: 3
Size: 329806 Color: 0

Bin 1452: 45 of cap free
Amount of items: 2
Items: 
Size: 682408 Color: 1
Size: 317548 Color: 2

Bin 1453: 45 of cap free
Amount of items: 2
Items: 
Size: 693747 Color: 2
Size: 306209 Color: 4

Bin 1454: 45 of cap free
Amount of items: 2
Items: 
Size: 702857 Color: 3
Size: 297099 Color: 1

Bin 1455: 45 of cap free
Amount of items: 2
Items: 
Size: 708801 Color: 0
Size: 291155 Color: 3

Bin 1456: 45 of cap free
Amount of items: 2
Items: 
Size: 708944 Color: 1
Size: 291012 Color: 3

Bin 1457: 45 of cap free
Amount of items: 2
Items: 
Size: 720273 Color: 2
Size: 279683 Color: 1

Bin 1458: 45 of cap free
Amount of items: 2
Items: 
Size: 726854 Color: 2
Size: 273102 Color: 0

Bin 1459: 45 of cap free
Amount of items: 2
Items: 
Size: 746906 Color: 4
Size: 253050 Color: 2

Bin 1460: 45 of cap free
Amount of items: 2
Items: 
Size: 747885 Color: 1
Size: 252071 Color: 2

Bin 1461: 45 of cap free
Amount of items: 2
Items: 
Size: 753621 Color: 2
Size: 246335 Color: 1

Bin 1462: 45 of cap free
Amount of items: 2
Items: 
Size: 773697 Color: 3
Size: 226259 Color: 0

Bin 1463: 45 of cap free
Amount of items: 2
Items: 
Size: 784796 Color: 1
Size: 215160 Color: 3

Bin 1464: 45 of cap free
Amount of items: 2
Items: 
Size: 799489 Color: 1
Size: 200467 Color: 2

Bin 1465: 46 of cap free
Amount of items: 7
Items: 
Size: 143591 Color: 0
Size: 143588 Color: 4
Size: 143546 Color: 0
Size: 143543 Color: 1
Size: 143488 Color: 2
Size: 143267 Color: 4
Size: 138932 Color: 4

Bin 1466: 46 of cap free
Amount of items: 6
Items: 
Size: 179229 Color: 0
Size: 179138 Color: 0
Size: 179089 Color: 3
Size: 179048 Color: 4
Size: 178958 Color: 1
Size: 104493 Color: 2

Bin 1467: 46 of cap free
Amount of items: 2
Items: 
Size: 525191 Color: 2
Size: 474764 Color: 0

Bin 1468: 46 of cap free
Amount of items: 2
Items: 
Size: 532826 Color: 1
Size: 467129 Color: 4

Bin 1469: 46 of cap free
Amount of items: 2
Items: 
Size: 542773 Color: 0
Size: 457182 Color: 4

Bin 1470: 46 of cap free
Amount of items: 2
Items: 
Size: 553744 Color: 2
Size: 446211 Color: 4

Bin 1471: 46 of cap free
Amount of items: 2
Items: 
Size: 594703 Color: 0
Size: 405252 Color: 3

Bin 1472: 46 of cap free
Amount of items: 2
Items: 
Size: 613695 Color: 0
Size: 386260 Color: 1

Bin 1473: 46 of cap free
Amount of items: 2
Items: 
Size: 620938 Color: 1
Size: 379017 Color: 4

Bin 1474: 46 of cap free
Amount of items: 2
Items: 
Size: 639067 Color: 0
Size: 360888 Color: 4

Bin 1475: 46 of cap free
Amount of items: 2
Items: 
Size: 649513 Color: 2
Size: 350442 Color: 1

Bin 1476: 46 of cap free
Amount of items: 2
Items: 
Size: 676503 Color: 0
Size: 323452 Color: 2

Bin 1477: 46 of cap free
Amount of items: 2
Items: 
Size: 687188 Color: 0
Size: 312767 Color: 2

Bin 1478: 46 of cap free
Amount of items: 2
Items: 
Size: 716493 Color: 3
Size: 283462 Color: 1

Bin 1479: 46 of cap free
Amount of items: 2
Items: 
Size: 717515 Color: 1
Size: 282440 Color: 0

Bin 1480: 46 of cap free
Amount of items: 2
Items: 
Size: 734962 Color: 1
Size: 264993 Color: 3

Bin 1481: 46 of cap free
Amount of items: 2
Items: 
Size: 771056 Color: 3
Size: 228899 Color: 1

Bin 1482: 46 of cap free
Amount of items: 2
Items: 
Size: 794957 Color: 1
Size: 204998 Color: 4

Bin 1483: 46 of cap free
Amount of items: 2
Items: 
Size: 796703 Color: 0
Size: 203252 Color: 3

Bin 1484: 47 of cap free
Amount of items: 2
Items: 
Size: 509654 Color: 4
Size: 490300 Color: 1

Bin 1485: 47 of cap free
Amount of items: 2
Items: 
Size: 521848 Color: 4
Size: 478106 Color: 3

Bin 1486: 47 of cap free
Amount of items: 2
Items: 
Size: 546685 Color: 4
Size: 453269 Color: 1

Bin 1487: 47 of cap free
Amount of items: 2
Items: 
Size: 572251 Color: 2
Size: 427703 Color: 0

Bin 1488: 47 of cap free
Amount of items: 2
Items: 
Size: 575981 Color: 0
Size: 423973 Color: 3

Bin 1489: 47 of cap free
Amount of items: 2
Items: 
Size: 587725 Color: 3
Size: 412229 Color: 4

Bin 1490: 47 of cap free
Amount of items: 2
Items: 
Size: 589181 Color: 2
Size: 410773 Color: 3

Bin 1491: 47 of cap free
Amount of items: 2
Items: 
Size: 620554 Color: 3
Size: 379400 Color: 0

Bin 1492: 47 of cap free
Amount of items: 2
Items: 
Size: 625293 Color: 4
Size: 374661 Color: 2

Bin 1493: 47 of cap free
Amount of items: 2
Items: 
Size: 630405 Color: 0
Size: 369549 Color: 2

Bin 1494: 47 of cap free
Amount of items: 2
Items: 
Size: 718612 Color: 2
Size: 281342 Color: 0

Bin 1495: 47 of cap free
Amount of items: 2
Items: 
Size: 729701 Color: 4
Size: 270253 Color: 0

Bin 1496: 47 of cap free
Amount of items: 2
Items: 
Size: 762586 Color: 4
Size: 237368 Color: 0

Bin 1497: 47 of cap free
Amount of items: 2
Items: 
Size: 779743 Color: 3
Size: 220211 Color: 0

Bin 1498: 47 of cap free
Amount of items: 2
Items: 
Size: 779989 Color: 1
Size: 219965 Color: 4

Bin 1499: 47 of cap free
Amount of items: 2
Items: 
Size: 798305 Color: 0
Size: 201649 Color: 3

Bin 1500: 48 of cap free
Amount of items: 2
Items: 
Size: 518921 Color: 1
Size: 481032 Color: 3

Bin 1501: 48 of cap free
Amount of items: 2
Items: 
Size: 542916 Color: 1
Size: 457037 Color: 4

Bin 1502: 48 of cap free
Amount of items: 2
Items: 
Size: 546278 Color: 4
Size: 453675 Color: 3

Bin 1503: 48 of cap free
Amount of items: 2
Items: 
Size: 549884 Color: 2
Size: 450069 Color: 3

Bin 1504: 48 of cap free
Amount of items: 2
Items: 
Size: 550808 Color: 2
Size: 449145 Color: 0

Bin 1505: 48 of cap free
Amount of items: 2
Items: 
Size: 561726 Color: 2
Size: 438227 Color: 4

Bin 1506: 48 of cap free
Amount of items: 2
Items: 
Size: 562287 Color: 2
Size: 437666 Color: 3

Bin 1507: 48 of cap free
Amount of items: 2
Items: 
Size: 567946 Color: 1
Size: 432007 Color: 2

Bin 1508: 48 of cap free
Amount of items: 2
Items: 
Size: 577142 Color: 4
Size: 422811 Color: 1

Bin 1509: 48 of cap free
Amount of items: 2
Items: 
Size: 602142 Color: 0
Size: 397811 Color: 1

Bin 1510: 48 of cap free
Amount of items: 2
Items: 
Size: 620666 Color: 3
Size: 379287 Color: 2

Bin 1511: 48 of cap free
Amount of items: 2
Items: 
Size: 649583 Color: 1
Size: 350370 Color: 2

Bin 1512: 48 of cap free
Amount of items: 2
Items: 
Size: 659067 Color: 1
Size: 340886 Color: 2

Bin 1513: 48 of cap free
Amount of items: 2
Items: 
Size: 661604 Color: 1
Size: 338349 Color: 3

Bin 1514: 48 of cap free
Amount of items: 2
Items: 
Size: 715590 Color: 0
Size: 284363 Color: 3

Bin 1515: 48 of cap free
Amount of items: 2
Items: 
Size: 716717 Color: 0
Size: 283236 Color: 1

Bin 1516: 48 of cap free
Amount of items: 2
Items: 
Size: 717015 Color: 1
Size: 282938 Color: 2

Bin 1517: 48 of cap free
Amount of items: 2
Items: 
Size: 717901 Color: 3
Size: 282052 Color: 4

Bin 1518: 48 of cap free
Amount of items: 2
Items: 
Size: 736402 Color: 1
Size: 263551 Color: 4

Bin 1519: 48 of cap free
Amount of items: 2
Items: 
Size: 736520 Color: 4
Size: 263433 Color: 2

Bin 1520: 48 of cap free
Amount of items: 2
Items: 
Size: 739204 Color: 1
Size: 260749 Color: 3

Bin 1521: 48 of cap free
Amount of items: 2
Items: 
Size: 767347 Color: 0
Size: 232606 Color: 3

Bin 1522: 48 of cap free
Amount of items: 2
Items: 
Size: 778150 Color: 4
Size: 221803 Color: 3

Bin 1523: 48 of cap free
Amount of items: 2
Items: 
Size: 785928 Color: 2
Size: 214025 Color: 4

Bin 1524: 48 of cap free
Amount of items: 2
Items: 
Size: 794123 Color: 3
Size: 205830 Color: 0

Bin 1525: 48 of cap free
Amount of items: 2
Items: 
Size: 794321 Color: 2
Size: 205632 Color: 1

Bin 1526: 48 of cap free
Amount of items: 2
Items: 
Size: 796047 Color: 1
Size: 203906 Color: 4

Bin 1527: 48 of cap free
Amount of items: 2
Items: 
Size: 799835 Color: 1
Size: 200118 Color: 0

Bin 1528: 48 of cap free
Amount of items: 2
Items: 
Size: 799942 Color: 0
Size: 200011 Color: 3

Bin 1529: 49 of cap free
Amount of items: 2
Items: 
Size: 501291 Color: 0
Size: 498661 Color: 2

Bin 1530: 49 of cap free
Amount of items: 2
Items: 
Size: 503919 Color: 2
Size: 496033 Color: 4

Bin 1531: 49 of cap free
Amount of items: 2
Items: 
Size: 515321 Color: 0
Size: 484631 Color: 3

Bin 1532: 49 of cap free
Amount of items: 2
Items: 
Size: 528730 Color: 3
Size: 471222 Color: 2

Bin 1533: 49 of cap free
Amount of items: 2
Items: 
Size: 532694 Color: 1
Size: 467258 Color: 0

Bin 1534: 49 of cap free
Amount of items: 2
Items: 
Size: 547366 Color: 1
Size: 452586 Color: 4

Bin 1535: 49 of cap free
Amount of items: 2
Items: 
Size: 553024 Color: 1
Size: 446928 Color: 4

Bin 1536: 49 of cap free
Amount of items: 2
Items: 
Size: 558241 Color: 0
Size: 441711 Color: 2

Bin 1537: 49 of cap free
Amount of items: 2
Items: 
Size: 570518 Color: 1
Size: 429434 Color: 2

Bin 1538: 49 of cap free
Amount of items: 2
Items: 
Size: 606947 Color: 2
Size: 393005 Color: 4

Bin 1539: 49 of cap free
Amount of items: 2
Items: 
Size: 628388 Color: 1
Size: 371564 Color: 3

Bin 1540: 49 of cap free
Amount of items: 2
Items: 
Size: 633902 Color: 4
Size: 366050 Color: 1

Bin 1541: 49 of cap free
Amount of items: 2
Items: 
Size: 641337 Color: 2
Size: 358615 Color: 4

Bin 1542: 49 of cap free
Amount of items: 2
Items: 
Size: 651231 Color: 1
Size: 348721 Color: 3

Bin 1543: 49 of cap free
Amount of items: 2
Items: 
Size: 676232 Color: 0
Size: 323720 Color: 4

Bin 1544: 49 of cap free
Amount of items: 2
Items: 
Size: 677276 Color: 0
Size: 322676 Color: 1

Bin 1545: 49 of cap free
Amount of items: 2
Items: 
Size: 682184 Color: 2
Size: 317768 Color: 0

Bin 1546: 49 of cap free
Amount of items: 2
Items: 
Size: 687583 Color: 1
Size: 312369 Color: 4

Bin 1547: 49 of cap free
Amount of items: 2
Items: 
Size: 717891 Color: 1
Size: 282061 Color: 3

Bin 1548: 49 of cap free
Amount of items: 2
Items: 
Size: 742534 Color: 3
Size: 257418 Color: 2

Bin 1549: 49 of cap free
Amount of items: 2
Items: 
Size: 799286 Color: 2
Size: 200666 Color: 4

Bin 1550: 50 of cap free
Amount of items: 2
Items: 
Size: 501547 Color: 1
Size: 498404 Color: 0

Bin 1551: 50 of cap free
Amount of items: 2
Items: 
Size: 531379 Color: 3
Size: 468572 Color: 0

Bin 1552: 50 of cap free
Amount of items: 2
Items: 
Size: 543194 Color: 2
Size: 456757 Color: 1

Bin 1553: 50 of cap free
Amount of items: 2
Items: 
Size: 551706 Color: 4
Size: 448245 Color: 3

Bin 1554: 50 of cap free
Amount of items: 2
Items: 
Size: 577252 Color: 2
Size: 422699 Color: 0

Bin 1555: 50 of cap free
Amount of items: 2
Items: 
Size: 599728 Color: 0
Size: 400223 Color: 1

Bin 1556: 50 of cap free
Amount of items: 2
Items: 
Size: 605889 Color: 0
Size: 394062 Color: 2

Bin 1557: 50 of cap free
Amount of items: 2
Items: 
Size: 625124 Color: 2
Size: 374827 Color: 3

Bin 1558: 50 of cap free
Amount of items: 2
Items: 
Size: 635925 Color: 3
Size: 364026 Color: 4

Bin 1559: 50 of cap free
Amount of items: 2
Items: 
Size: 648809 Color: 4
Size: 351142 Color: 0

Bin 1560: 50 of cap free
Amount of items: 2
Items: 
Size: 659912 Color: 2
Size: 340039 Color: 4

Bin 1561: 50 of cap free
Amount of items: 2
Items: 
Size: 701872 Color: 2
Size: 298079 Color: 0

Bin 1562: 50 of cap free
Amount of items: 2
Items: 
Size: 702294 Color: 2
Size: 297657 Color: 1

Bin 1563: 50 of cap free
Amount of items: 2
Items: 
Size: 704969 Color: 4
Size: 294982 Color: 2

Bin 1564: 50 of cap free
Amount of items: 2
Items: 
Size: 708725 Color: 0
Size: 291226 Color: 3

Bin 1565: 50 of cap free
Amount of items: 2
Items: 
Size: 709832 Color: 4
Size: 290119 Color: 0

Bin 1566: 50 of cap free
Amount of items: 2
Items: 
Size: 745163 Color: 0
Size: 254788 Color: 1

Bin 1567: 50 of cap free
Amount of items: 2
Items: 
Size: 771414 Color: 2
Size: 228537 Color: 4

Bin 1568: 51 of cap free
Amount of items: 6
Items: 
Size: 172370 Color: 1
Size: 172285 Color: 4
Size: 172274 Color: 1
Size: 172268 Color: 1
Size: 172254 Color: 0
Size: 138499 Color: 2

Bin 1569: 51 of cap free
Amount of items: 2
Items: 
Size: 526444 Color: 0
Size: 473506 Color: 4

Bin 1570: 51 of cap free
Amount of items: 2
Items: 
Size: 543866 Color: 1
Size: 456084 Color: 0

Bin 1571: 51 of cap free
Amount of items: 2
Items: 
Size: 545913 Color: 0
Size: 454037 Color: 1

Bin 1572: 51 of cap free
Amount of items: 2
Items: 
Size: 576955 Color: 4
Size: 422995 Color: 3

Bin 1573: 51 of cap free
Amount of items: 2
Items: 
Size: 598474 Color: 1
Size: 401476 Color: 0

Bin 1574: 51 of cap free
Amount of items: 2
Items: 
Size: 623910 Color: 3
Size: 376040 Color: 4

Bin 1575: 51 of cap free
Amount of items: 2
Items: 
Size: 629818 Color: 4
Size: 370132 Color: 1

Bin 1576: 51 of cap free
Amount of items: 2
Items: 
Size: 635196 Color: 0
Size: 364754 Color: 3

Bin 1577: 51 of cap free
Amount of items: 2
Items: 
Size: 758473 Color: 3
Size: 241477 Color: 4

Bin 1578: 51 of cap free
Amount of items: 2
Items: 
Size: 758763 Color: 0
Size: 241187 Color: 1

Bin 1579: 51 of cap free
Amount of items: 2
Items: 
Size: 789031 Color: 0
Size: 210919 Color: 1

Bin 1580: 52 of cap free
Amount of items: 2
Items: 
Size: 537898 Color: 3
Size: 462051 Color: 2

Bin 1581: 52 of cap free
Amount of items: 2
Items: 
Size: 551206 Color: 3
Size: 448743 Color: 0

Bin 1582: 52 of cap free
Amount of items: 2
Items: 
Size: 572861 Color: 3
Size: 427088 Color: 1

Bin 1583: 52 of cap free
Amount of items: 2
Items: 
Size: 575290 Color: 4
Size: 424659 Color: 0

Bin 1584: 52 of cap free
Amount of items: 2
Items: 
Size: 576870 Color: 0
Size: 423079 Color: 1

Bin 1585: 52 of cap free
Amount of items: 2
Items: 
Size: 580355 Color: 2
Size: 419594 Color: 1

Bin 1586: 52 of cap free
Amount of items: 2
Items: 
Size: 584819 Color: 4
Size: 415130 Color: 1

Bin 1587: 52 of cap free
Amount of items: 2
Items: 
Size: 589601 Color: 3
Size: 410348 Color: 4

Bin 1588: 52 of cap free
Amount of items: 2
Items: 
Size: 594037 Color: 4
Size: 405912 Color: 0

Bin 1589: 52 of cap free
Amount of items: 2
Items: 
Size: 599795 Color: 0
Size: 400154 Color: 1

Bin 1590: 52 of cap free
Amount of items: 2
Items: 
Size: 603453 Color: 0
Size: 396496 Color: 1

Bin 1591: 52 of cap free
Amount of items: 2
Items: 
Size: 613848 Color: 2
Size: 386101 Color: 4

Bin 1592: 52 of cap free
Amount of items: 2
Items: 
Size: 634579 Color: 3
Size: 365370 Color: 0

Bin 1593: 52 of cap free
Amount of items: 2
Items: 
Size: 648651 Color: 2
Size: 351298 Color: 0

Bin 1594: 52 of cap free
Amount of items: 2
Items: 
Size: 661926 Color: 4
Size: 338023 Color: 2

Bin 1595: 52 of cap free
Amount of items: 2
Items: 
Size: 688569 Color: 4
Size: 311380 Color: 1

Bin 1596: 52 of cap free
Amount of items: 2
Items: 
Size: 709332 Color: 0
Size: 290617 Color: 2

Bin 1597: 52 of cap free
Amount of items: 2
Items: 
Size: 714362 Color: 0
Size: 285587 Color: 3

Bin 1598: 52 of cap free
Amount of items: 2
Items: 
Size: 763285 Color: 1
Size: 236664 Color: 3

Bin 1599: 52 of cap free
Amount of items: 2
Items: 
Size: 766704 Color: 1
Size: 233245 Color: 4

Bin 1600: 52 of cap free
Amount of items: 2
Items: 
Size: 777620 Color: 3
Size: 222329 Color: 4

Bin 1601: 52 of cap free
Amount of items: 2
Items: 
Size: 788659 Color: 1
Size: 211290 Color: 3

Bin 1602: 52 of cap free
Amount of items: 2
Items: 
Size: 788787 Color: 0
Size: 211162 Color: 1

Bin 1603: 52 of cap free
Amount of items: 2
Items: 
Size: 799159 Color: 4
Size: 200790 Color: 2

Bin 1604: 53 of cap free
Amount of items: 2
Items: 
Size: 514423 Color: 4
Size: 485525 Color: 0

Bin 1605: 53 of cap free
Amount of items: 2
Items: 
Size: 522549 Color: 3
Size: 477399 Color: 4

Bin 1606: 53 of cap free
Amount of items: 2
Items: 
Size: 524901 Color: 4
Size: 475047 Color: 0

Bin 1607: 53 of cap free
Amount of items: 2
Items: 
Size: 525318 Color: 4
Size: 474630 Color: 1

Bin 1608: 53 of cap free
Amount of items: 2
Items: 
Size: 546490 Color: 4
Size: 453458 Color: 1

Bin 1609: 53 of cap free
Amount of items: 2
Items: 
Size: 563791 Color: 0
Size: 436157 Color: 1

Bin 1610: 53 of cap free
Amount of items: 2
Items: 
Size: 585773 Color: 3
Size: 414175 Color: 0

Bin 1611: 53 of cap free
Amount of items: 2
Items: 
Size: 617556 Color: 0
Size: 382392 Color: 2

Bin 1612: 53 of cap free
Amount of items: 2
Items: 
Size: 617895 Color: 2
Size: 382053 Color: 0

Bin 1613: 53 of cap free
Amount of items: 2
Items: 
Size: 619335 Color: 3
Size: 380613 Color: 0

Bin 1614: 53 of cap free
Amount of items: 2
Items: 
Size: 631813 Color: 4
Size: 368135 Color: 0

Bin 1615: 53 of cap free
Amount of items: 2
Items: 
Size: 642065 Color: 4
Size: 357883 Color: 3

Bin 1616: 53 of cap free
Amount of items: 2
Items: 
Size: 643621 Color: 0
Size: 356327 Color: 4

Bin 1617: 53 of cap free
Amount of items: 2
Items: 
Size: 650180 Color: 0
Size: 349768 Color: 1

Bin 1618: 53 of cap free
Amount of items: 2
Items: 
Size: 654713 Color: 2
Size: 345235 Color: 3

Bin 1619: 53 of cap free
Amount of items: 2
Items: 
Size: 668360 Color: 1
Size: 331588 Color: 4

Bin 1620: 53 of cap free
Amount of items: 2
Items: 
Size: 672380 Color: 2
Size: 327568 Color: 4

Bin 1621: 53 of cap free
Amount of items: 2
Items: 
Size: 700565 Color: 2
Size: 299383 Color: 4

Bin 1622: 53 of cap free
Amount of items: 2
Items: 
Size: 702538 Color: 0
Size: 297410 Color: 2

Bin 1623: 53 of cap free
Amount of items: 2
Items: 
Size: 722212 Color: 2
Size: 277736 Color: 1

Bin 1624: 53 of cap free
Amount of items: 2
Items: 
Size: 726026 Color: 4
Size: 273922 Color: 3

Bin 1625: 53 of cap free
Amount of items: 2
Items: 
Size: 728559 Color: 3
Size: 271389 Color: 4

Bin 1626: 53 of cap free
Amount of items: 2
Items: 
Size: 734457 Color: 0
Size: 265491 Color: 2

Bin 1627: 53 of cap free
Amount of items: 2
Items: 
Size: 760098 Color: 4
Size: 239850 Color: 2

Bin 1628: 53 of cap free
Amount of items: 2
Items: 
Size: 772213 Color: 2
Size: 227735 Color: 1

Bin 1629: 53 of cap free
Amount of items: 2
Items: 
Size: 786553 Color: 0
Size: 213395 Color: 3

Bin 1630: 53 of cap free
Amount of items: 2
Items: 
Size: 790598 Color: 3
Size: 209350 Color: 0

Bin 1631: 53 of cap free
Amount of items: 2
Items: 
Size: 793376 Color: 3
Size: 206572 Color: 1

Bin 1632: 54 of cap free
Amount of items: 2
Items: 
Size: 511278 Color: 1
Size: 488669 Color: 2

Bin 1633: 54 of cap free
Amount of items: 2
Items: 
Size: 517598 Color: 4
Size: 482349 Color: 2

Bin 1634: 54 of cap free
Amount of items: 2
Items: 
Size: 546634 Color: 1
Size: 453313 Color: 4

Bin 1635: 54 of cap free
Amount of items: 2
Items: 
Size: 592213 Color: 4
Size: 407734 Color: 1

Bin 1636: 54 of cap free
Amount of items: 2
Items: 
Size: 594926 Color: 0
Size: 405021 Color: 3

Bin 1637: 54 of cap free
Amount of items: 2
Items: 
Size: 595253 Color: 0
Size: 404694 Color: 3

Bin 1638: 54 of cap free
Amount of items: 2
Items: 
Size: 655947 Color: 4
Size: 344000 Color: 1

Bin 1639: 54 of cap free
Amount of items: 2
Items: 
Size: 668260 Color: 4
Size: 331687 Color: 3

Bin 1640: 54 of cap free
Amount of items: 2
Items: 
Size: 670351 Color: 0
Size: 329596 Color: 3

Bin 1641: 54 of cap free
Amount of items: 2
Items: 
Size: 686121 Color: 2
Size: 313826 Color: 4

Bin 1642: 54 of cap free
Amount of items: 2
Items: 
Size: 726978 Color: 3
Size: 272969 Color: 0

Bin 1643: 54 of cap free
Amount of items: 2
Items: 
Size: 732065 Color: 0
Size: 267882 Color: 2

Bin 1644: 54 of cap free
Amount of items: 2
Items: 
Size: 769663 Color: 2
Size: 230284 Color: 3

Bin 1645: 54 of cap free
Amount of items: 2
Items: 
Size: 785593 Color: 3
Size: 214354 Color: 0

Bin 1646: 55 of cap free
Amount of items: 7
Items: 
Size: 147890 Color: 4
Size: 147876 Color: 4
Size: 147849 Color: 2
Size: 147770 Color: 2
Size: 147760 Color: 4
Size: 147651 Color: 0
Size: 113150 Color: 0

Bin 1647: 55 of cap free
Amount of items: 2
Items: 
Size: 572902 Color: 1
Size: 427044 Color: 2

Bin 1648: 55 of cap free
Amount of items: 2
Items: 
Size: 604884 Color: 2
Size: 395062 Color: 3

Bin 1649: 55 of cap free
Amount of items: 2
Items: 
Size: 607029 Color: 4
Size: 392917 Color: 3

Bin 1650: 55 of cap free
Amount of items: 2
Items: 
Size: 724384 Color: 3
Size: 275562 Color: 0

Bin 1651: 55 of cap free
Amount of items: 2
Items: 
Size: 747134 Color: 3
Size: 252812 Color: 0

Bin 1652: 55 of cap free
Amount of items: 2
Items: 
Size: 756890 Color: 4
Size: 243056 Color: 2

Bin 1653: 55 of cap free
Amount of items: 2
Items: 
Size: 790877 Color: 0
Size: 209069 Color: 2

Bin 1654: 55 of cap free
Amount of items: 2
Items: 
Size: 792737 Color: 2
Size: 207209 Color: 1

Bin 1655: 55 of cap free
Amount of items: 2
Items: 
Size: 796816 Color: 1
Size: 203130 Color: 4

Bin 1656: 56 of cap free
Amount of items: 2
Items: 
Size: 581423 Color: 1
Size: 418522 Color: 3

Bin 1657: 56 of cap free
Amount of items: 2
Items: 
Size: 604973 Color: 0
Size: 394972 Color: 1

Bin 1658: 56 of cap free
Amount of items: 2
Items: 
Size: 625382 Color: 1
Size: 374563 Color: 0

Bin 1659: 56 of cap free
Amount of items: 2
Items: 
Size: 662044 Color: 0
Size: 337901 Color: 3

Bin 1660: 56 of cap free
Amount of items: 2
Items: 
Size: 668439 Color: 3
Size: 331506 Color: 4

Bin 1661: 56 of cap free
Amount of items: 2
Items: 
Size: 671369 Color: 0
Size: 328576 Color: 4

Bin 1662: 56 of cap free
Amount of items: 2
Items: 
Size: 690118 Color: 0
Size: 309827 Color: 3

Bin 1663: 56 of cap free
Amount of items: 2
Items: 
Size: 743313 Color: 1
Size: 256632 Color: 4

Bin 1664: 56 of cap free
Amount of items: 2
Items: 
Size: 750294 Color: 3
Size: 249651 Color: 0

Bin 1665: 56 of cap free
Amount of items: 2
Items: 
Size: 780968 Color: 0
Size: 218977 Color: 4

Bin 1666: 56 of cap free
Amount of items: 2
Items: 
Size: 787647 Color: 1
Size: 212298 Color: 2

Bin 1667: 57 of cap free
Amount of items: 6
Items: 
Size: 174944 Color: 0
Size: 174937 Color: 0
Size: 174793 Color: 4
Size: 174790 Color: 3
Size: 174789 Color: 3
Size: 125691 Color: 2

Bin 1668: 57 of cap free
Amount of items: 2
Items: 
Size: 501135 Color: 2
Size: 498809 Color: 4

Bin 1669: 57 of cap free
Amount of items: 2
Items: 
Size: 501675 Color: 4
Size: 498269 Color: 3

Bin 1670: 57 of cap free
Amount of items: 2
Items: 
Size: 504284 Color: 0
Size: 495660 Color: 2

Bin 1671: 57 of cap free
Amount of items: 2
Items: 
Size: 553186 Color: 3
Size: 446758 Color: 1

Bin 1672: 57 of cap free
Amount of items: 2
Items: 
Size: 593742 Color: 3
Size: 406202 Color: 1

Bin 1673: 57 of cap free
Amount of items: 2
Items: 
Size: 611155 Color: 0
Size: 388789 Color: 3

Bin 1674: 57 of cap free
Amount of items: 2
Items: 
Size: 621809 Color: 1
Size: 378135 Color: 3

Bin 1675: 57 of cap free
Amount of items: 2
Items: 
Size: 624124 Color: 2
Size: 375820 Color: 4

Bin 1676: 57 of cap free
Amount of items: 2
Items: 
Size: 655595 Color: 0
Size: 344349 Color: 2

Bin 1677: 57 of cap free
Amount of items: 2
Items: 
Size: 680710 Color: 1
Size: 319234 Color: 0

Bin 1678: 57 of cap free
Amount of items: 2
Items: 
Size: 686599 Color: 1
Size: 313345 Color: 0

Bin 1679: 57 of cap free
Amount of items: 2
Items: 
Size: 694155 Color: 1
Size: 305789 Color: 3

Bin 1680: 57 of cap free
Amount of items: 2
Items: 
Size: 702771 Color: 1
Size: 297173 Color: 0

Bin 1681: 57 of cap free
Amount of items: 2
Items: 
Size: 706239 Color: 3
Size: 293705 Color: 1

Bin 1682: 57 of cap free
Amount of items: 2
Items: 
Size: 719389 Color: 2
Size: 280555 Color: 4

Bin 1683: 57 of cap free
Amount of items: 2
Items: 
Size: 729986 Color: 4
Size: 269958 Color: 0

Bin 1684: 57 of cap free
Amount of items: 2
Items: 
Size: 750383 Color: 0
Size: 249561 Color: 1

Bin 1685: 57 of cap free
Amount of items: 2
Items: 
Size: 750727 Color: 3
Size: 249217 Color: 0

Bin 1686: 57 of cap free
Amount of items: 2
Items: 
Size: 766098 Color: 2
Size: 233846 Color: 4

Bin 1687: 58 of cap free
Amount of items: 2
Items: 
Size: 515435 Color: 4
Size: 484508 Color: 3

Bin 1688: 58 of cap free
Amount of items: 2
Items: 
Size: 537558 Color: 1
Size: 462385 Color: 0

Bin 1689: 58 of cap free
Amount of items: 2
Items: 
Size: 556625 Color: 1
Size: 443318 Color: 3

Bin 1690: 58 of cap free
Amount of items: 2
Items: 
Size: 605934 Color: 2
Size: 394009 Color: 4

Bin 1691: 58 of cap free
Amount of items: 2
Items: 
Size: 608285 Color: 1
Size: 391658 Color: 2

Bin 1692: 58 of cap free
Amount of items: 2
Items: 
Size: 656292 Color: 0
Size: 343651 Color: 2

Bin 1693: 58 of cap free
Amount of items: 2
Items: 
Size: 658402 Color: 3
Size: 341541 Color: 0

Bin 1694: 58 of cap free
Amount of items: 2
Items: 
Size: 677495 Color: 3
Size: 322448 Color: 1

Bin 1695: 58 of cap free
Amount of items: 2
Items: 
Size: 695306 Color: 2
Size: 304637 Color: 1

Bin 1696: 58 of cap free
Amount of items: 2
Items: 
Size: 729300 Color: 1
Size: 270643 Color: 3

Bin 1697: 58 of cap free
Amount of items: 2
Items: 
Size: 742678 Color: 1
Size: 257265 Color: 2

Bin 1698: 58 of cap free
Amount of items: 2
Items: 
Size: 760154 Color: 1
Size: 239789 Color: 3

Bin 1699: 58 of cap free
Amount of items: 2
Items: 
Size: 775611 Color: 2
Size: 224332 Color: 0

Bin 1700: 58 of cap free
Amount of items: 2
Items: 
Size: 793197 Color: 0
Size: 206746 Color: 1

Bin 1701: 59 of cap free
Amount of items: 7
Items: 
Size: 146924 Color: 4
Size: 146896 Color: 4
Size: 146890 Color: 0
Size: 146721 Color: 1
Size: 146701 Color: 1
Size: 146618 Color: 0
Size: 119192 Color: 1

Bin 1702: 59 of cap free
Amount of items: 2
Items: 
Size: 546057 Color: 2
Size: 453885 Color: 4

Bin 1703: 59 of cap free
Amount of items: 2
Items: 
Size: 547151 Color: 4
Size: 452791 Color: 1

Bin 1704: 59 of cap free
Amount of items: 2
Items: 
Size: 575037 Color: 4
Size: 424905 Color: 3

Bin 1705: 59 of cap free
Amount of items: 2
Items: 
Size: 582750 Color: 1
Size: 417192 Color: 0

Bin 1706: 59 of cap free
Amount of items: 2
Items: 
Size: 612354 Color: 1
Size: 387588 Color: 4

Bin 1707: 59 of cap free
Amount of items: 2
Items: 
Size: 624643 Color: 2
Size: 375299 Color: 3

Bin 1708: 59 of cap free
Amount of items: 2
Items: 
Size: 629283 Color: 3
Size: 370659 Color: 2

Bin 1709: 59 of cap free
Amount of items: 2
Items: 
Size: 637570 Color: 4
Size: 362372 Color: 2

Bin 1710: 59 of cap free
Amount of items: 2
Items: 
Size: 640532 Color: 1
Size: 359410 Color: 3

Bin 1711: 59 of cap free
Amount of items: 2
Items: 
Size: 646842 Color: 4
Size: 353100 Color: 0

Bin 1712: 59 of cap free
Amount of items: 2
Items: 
Size: 661226 Color: 1
Size: 338716 Color: 2

Bin 1713: 59 of cap free
Amount of items: 2
Items: 
Size: 667196 Color: 3
Size: 332746 Color: 4

Bin 1714: 59 of cap free
Amount of items: 2
Items: 
Size: 706759 Color: 1
Size: 293183 Color: 2

Bin 1715: 59 of cap free
Amount of items: 2
Items: 
Size: 731322 Color: 3
Size: 268620 Color: 4

Bin 1716: 59 of cap free
Amount of items: 2
Items: 
Size: 781767 Color: 4
Size: 218175 Color: 2

Bin 1717: 60 of cap free
Amount of items: 2
Items: 
Size: 545506 Color: 3
Size: 454435 Color: 4

Bin 1718: 60 of cap free
Amount of items: 2
Items: 
Size: 566859 Color: 2
Size: 433082 Color: 4

Bin 1719: 60 of cap free
Amount of items: 2
Items: 
Size: 573467 Color: 0
Size: 426474 Color: 3

Bin 1720: 60 of cap free
Amount of items: 2
Items: 
Size: 576149 Color: 2
Size: 423792 Color: 3

Bin 1721: 60 of cap free
Amount of items: 2
Items: 
Size: 594210 Color: 2
Size: 405731 Color: 4

Bin 1722: 60 of cap free
Amount of items: 2
Items: 
Size: 597995 Color: 4
Size: 401946 Color: 1

Bin 1723: 60 of cap free
Amount of items: 2
Items: 
Size: 616153 Color: 1
Size: 383788 Color: 0

Bin 1724: 60 of cap free
Amount of items: 3
Items: 
Size: 619442 Color: 4
Size: 195557 Color: 1
Size: 184942 Color: 0

Bin 1725: 60 of cap free
Amount of items: 2
Items: 
Size: 620208 Color: 1
Size: 379733 Color: 0

Bin 1726: 60 of cap free
Amount of items: 2
Items: 
Size: 655605 Color: 2
Size: 344336 Color: 0

Bin 1727: 60 of cap free
Amount of items: 2
Items: 
Size: 671535 Color: 2
Size: 328406 Color: 1

Bin 1728: 60 of cap free
Amount of items: 2
Items: 
Size: 686841 Color: 0
Size: 313100 Color: 3

Bin 1729: 60 of cap free
Amount of items: 2
Items: 
Size: 691079 Color: 3
Size: 308862 Color: 0

Bin 1730: 60 of cap free
Amount of items: 2
Items: 
Size: 700621 Color: 1
Size: 299320 Color: 2

Bin 1731: 60 of cap free
Amount of items: 2
Items: 
Size: 712166 Color: 3
Size: 287775 Color: 2

Bin 1732: 60 of cap free
Amount of items: 2
Items: 
Size: 713890 Color: 2
Size: 286051 Color: 3

Bin 1733: 60 of cap free
Amount of items: 2
Items: 
Size: 727279 Color: 4
Size: 272662 Color: 1

Bin 1734: 60 of cap free
Amount of items: 2
Items: 
Size: 730211 Color: 1
Size: 269730 Color: 3

Bin 1735: 60 of cap free
Amount of items: 2
Items: 
Size: 765067 Color: 0
Size: 234874 Color: 4

Bin 1736: 60 of cap free
Amount of items: 2
Items: 
Size: 770068 Color: 0
Size: 229873 Color: 4

Bin 1737: 60 of cap free
Amount of items: 2
Items: 
Size: 790738 Color: 3
Size: 209203 Color: 1

Bin 1738: 61 of cap free
Amount of items: 2
Items: 
Size: 510508 Color: 1
Size: 489432 Color: 3

Bin 1739: 61 of cap free
Amount of items: 4
Items: 
Size: 515309 Color: 3
Size: 190656 Color: 4
Size: 190346 Color: 4
Size: 103629 Color: 2

Bin 1740: 61 of cap free
Amount of items: 2
Items: 
Size: 516724 Color: 4
Size: 483216 Color: 0

Bin 1741: 61 of cap free
Amount of items: 2
Items: 
Size: 525415 Color: 2
Size: 474525 Color: 4

Bin 1742: 61 of cap free
Amount of items: 2
Items: 
Size: 538950 Color: 4
Size: 460990 Color: 2

Bin 1743: 61 of cap free
Amount of items: 2
Items: 
Size: 539608 Color: 1
Size: 460332 Color: 0

Bin 1744: 61 of cap free
Amount of items: 2
Items: 
Size: 541452 Color: 0
Size: 458488 Color: 4

Bin 1745: 61 of cap free
Amount of items: 2
Items: 
Size: 556274 Color: 0
Size: 443666 Color: 4

Bin 1746: 61 of cap free
Amount of items: 2
Items: 
Size: 575512 Color: 3
Size: 424428 Color: 0

Bin 1747: 61 of cap free
Amount of items: 2
Items: 
Size: 605657 Color: 1
Size: 394283 Color: 2

Bin 1748: 61 of cap free
Amount of items: 2
Items: 
Size: 610548 Color: 4
Size: 389392 Color: 0

Bin 1749: 61 of cap free
Amount of items: 2
Items: 
Size: 610853 Color: 1
Size: 389087 Color: 0

Bin 1750: 61 of cap free
Amount of items: 2
Items: 
Size: 613247 Color: 1
Size: 386693 Color: 4

Bin 1751: 61 of cap free
Amount of items: 2
Items: 
Size: 642774 Color: 4
Size: 357166 Color: 2

Bin 1752: 61 of cap free
Amount of items: 2
Items: 
Size: 644102 Color: 2
Size: 355838 Color: 1

Bin 1753: 61 of cap free
Amount of items: 2
Items: 
Size: 644944 Color: 0
Size: 354996 Color: 4

Bin 1754: 61 of cap free
Amount of items: 2
Items: 
Size: 665572 Color: 2
Size: 334368 Color: 3

Bin 1755: 61 of cap free
Amount of items: 2
Items: 
Size: 703070 Color: 3
Size: 296870 Color: 0

Bin 1756: 61 of cap free
Amount of items: 2
Items: 
Size: 731845 Color: 3
Size: 268095 Color: 2

Bin 1757: 61 of cap free
Amount of items: 2
Items: 
Size: 737643 Color: 2
Size: 262297 Color: 3

Bin 1758: 61 of cap free
Amount of items: 2
Items: 
Size: 751580 Color: 4
Size: 248360 Color: 3

Bin 1759: 61 of cap free
Amount of items: 2
Items: 
Size: 761397 Color: 4
Size: 238543 Color: 2

Bin 1760: 61 of cap free
Amount of items: 2
Items: 
Size: 765232 Color: 4
Size: 234708 Color: 2

Bin 1761: 61 of cap free
Amount of items: 2
Items: 
Size: 789859 Color: 0
Size: 210081 Color: 1

Bin 1762: 61 of cap free
Amount of items: 2
Items: 
Size: 796625 Color: 3
Size: 203315 Color: 0

Bin 1763: 62 of cap free
Amount of items: 8
Items: 
Size: 127540 Color: 1
Size: 127431 Color: 4
Size: 127408 Color: 2
Size: 127211 Color: 4
Size: 127129 Color: 1
Size: 127032 Color: 2
Size: 127023 Color: 1
Size: 109165 Color: 3

Bin 1764: 62 of cap free
Amount of items: 2
Items: 
Size: 506582 Color: 0
Size: 493357 Color: 1

Bin 1765: 62 of cap free
Amount of items: 2
Items: 
Size: 595665 Color: 1
Size: 404274 Color: 2

Bin 1766: 62 of cap free
Amount of items: 2
Items: 
Size: 628101 Color: 4
Size: 371838 Color: 3

Bin 1767: 62 of cap free
Amount of items: 2
Items: 
Size: 652222 Color: 2
Size: 347717 Color: 3

Bin 1768: 62 of cap free
Amount of items: 2
Items: 
Size: 652887 Color: 0
Size: 347052 Color: 3

Bin 1769: 62 of cap free
Amount of items: 2
Items: 
Size: 682622 Color: 0
Size: 317317 Color: 1

Bin 1770: 62 of cap free
Amount of items: 2
Items: 
Size: 784175 Color: 4
Size: 215764 Color: 3

Bin 1771: 62 of cap free
Amount of items: 2
Items: 
Size: 787120 Color: 2
Size: 212819 Color: 4

Bin 1772: 62 of cap free
Amount of items: 2
Items: 
Size: 794746 Color: 3
Size: 205193 Color: 4

Bin 1773: 63 of cap free
Amount of items: 7
Items: 
Size: 143846 Color: 4
Size: 143800 Color: 2
Size: 143772 Color: 3
Size: 143749 Color: 2
Size: 143721 Color: 0
Size: 143687 Color: 4
Size: 137363 Color: 4

Bin 1774: 63 of cap free
Amount of items: 2
Items: 
Size: 501130 Color: 1
Size: 498808 Color: 0

Bin 1775: 63 of cap free
Amount of items: 2
Items: 
Size: 538012 Color: 3
Size: 461926 Color: 4

Bin 1776: 63 of cap free
Amount of items: 2
Items: 
Size: 546555 Color: 2
Size: 453383 Color: 0

Bin 1777: 63 of cap free
Amount of items: 2
Items: 
Size: 553620 Color: 4
Size: 446318 Color: 3

Bin 1778: 63 of cap free
Amount of items: 2
Items: 
Size: 560008 Color: 1
Size: 439930 Color: 4

Bin 1779: 63 of cap free
Amount of items: 2
Items: 
Size: 574047 Color: 4
Size: 425891 Color: 0

Bin 1780: 63 of cap free
Amount of items: 2
Items: 
Size: 594752 Color: 2
Size: 405186 Color: 0

Bin 1781: 63 of cap free
Amount of items: 2
Items: 
Size: 629719 Color: 2
Size: 370219 Color: 4

Bin 1782: 63 of cap free
Amount of items: 2
Items: 
Size: 635604 Color: 3
Size: 364334 Color: 2

Bin 1783: 63 of cap free
Amount of items: 2
Items: 
Size: 650883 Color: 4
Size: 349055 Color: 0

Bin 1784: 63 of cap free
Amount of items: 2
Items: 
Size: 670011 Color: 4
Size: 329927 Color: 1

Bin 1785: 63 of cap free
Amount of items: 2
Items: 
Size: 691663 Color: 1
Size: 308275 Color: 0

Bin 1786: 63 of cap free
Amount of items: 2
Items: 
Size: 694473 Color: 2
Size: 305465 Color: 3

Bin 1787: 63 of cap free
Amount of items: 2
Items: 
Size: 708538 Color: 3
Size: 291400 Color: 2

Bin 1788: 63 of cap free
Amount of items: 2
Items: 
Size: 710016 Color: 1
Size: 289922 Color: 0

Bin 1789: 63 of cap free
Amount of items: 2
Items: 
Size: 730404 Color: 3
Size: 269534 Color: 2

Bin 1790: 63 of cap free
Amount of items: 2
Items: 
Size: 735513 Color: 3
Size: 264425 Color: 0

Bin 1791: 63 of cap free
Amount of items: 2
Items: 
Size: 739100 Color: 0
Size: 260838 Color: 4

Bin 1792: 63 of cap free
Amount of items: 2
Items: 
Size: 745872 Color: 2
Size: 254066 Color: 1

Bin 1793: 63 of cap free
Amount of items: 2
Items: 
Size: 748744 Color: 0
Size: 251194 Color: 1

Bin 1794: 63 of cap free
Amount of items: 2
Items: 
Size: 768501 Color: 0
Size: 231437 Color: 3

Bin 1795: 63 of cap free
Amount of items: 2
Items: 
Size: 777336 Color: 4
Size: 222602 Color: 0

Bin 1796: 63 of cap free
Amount of items: 2
Items: 
Size: 779170 Color: 1
Size: 220768 Color: 3

Bin 1797: 63 of cap free
Amount of items: 2
Items: 
Size: 783350 Color: 0
Size: 216588 Color: 2

Bin 1798: 63 of cap free
Amount of items: 2
Items: 
Size: 785486 Color: 3
Size: 214452 Color: 0

Bin 1799: 63 of cap free
Amount of items: 2
Items: 
Size: 796341 Color: 0
Size: 203597 Color: 4

Bin 1800: 63 of cap free
Amount of items: 2
Items: 
Size: 797263 Color: 4
Size: 202675 Color: 3

Bin 1801: 64 of cap free
Amount of items: 7
Items: 
Size: 145529 Color: 3
Size: 145511 Color: 4
Size: 145423 Color: 4
Size: 145352 Color: 4
Size: 145260 Color: 1
Size: 145311 Color: 4
Size: 127551 Color: 3

Bin 1802: 64 of cap free
Amount of items: 2
Items: 
Size: 528811 Color: 2
Size: 471126 Color: 1

Bin 1803: 64 of cap free
Amount of items: 2
Items: 
Size: 542657 Color: 2
Size: 457280 Color: 3

Bin 1804: 64 of cap free
Amount of items: 2
Items: 
Size: 565895 Color: 3
Size: 434042 Color: 2

Bin 1805: 64 of cap free
Amount of items: 2
Items: 
Size: 586402 Color: 0
Size: 413535 Color: 1

Bin 1806: 64 of cap free
Amount of items: 2
Items: 
Size: 591235 Color: 1
Size: 408702 Color: 2

Bin 1807: 64 of cap free
Amount of items: 2
Items: 
Size: 601699 Color: 3
Size: 398238 Color: 4

Bin 1808: 64 of cap free
Amount of items: 2
Items: 
Size: 608023 Color: 0
Size: 391914 Color: 4

Bin 1809: 64 of cap free
Amount of items: 2
Items: 
Size: 611369 Color: 3
Size: 388568 Color: 4

Bin 1810: 64 of cap free
Amount of items: 2
Items: 
Size: 653821 Color: 2
Size: 346116 Color: 0

Bin 1811: 64 of cap free
Amount of items: 2
Items: 
Size: 656436 Color: 3
Size: 343501 Color: 1

Bin 1812: 64 of cap free
Amount of items: 2
Items: 
Size: 656766 Color: 4
Size: 343171 Color: 2

Bin 1813: 64 of cap free
Amount of items: 2
Items: 
Size: 663977 Color: 1
Size: 335960 Color: 4

Bin 1814: 64 of cap free
Amount of items: 2
Items: 
Size: 718599 Color: 1
Size: 281338 Color: 0

Bin 1815: 64 of cap free
Amount of items: 2
Items: 
Size: 758602 Color: 2
Size: 241335 Color: 4

Bin 1816: 64 of cap free
Amount of items: 2
Items: 
Size: 758609 Color: 4
Size: 241328 Color: 2

Bin 1817: 64 of cap free
Amount of items: 2
Items: 
Size: 763187 Color: 3
Size: 236750 Color: 4

Bin 1818: 64 of cap free
Amount of items: 2
Items: 
Size: 788941 Color: 0
Size: 210996 Color: 4

Bin 1819: 65 of cap free
Amount of items: 2
Items: 
Size: 559693 Color: 2
Size: 440243 Color: 4

Bin 1820: 65 of cap free
Amount of items: 2
Items: 
Size: 564196 Color: 3
Size: 435740 Color: 1

Bin 1821: 65 of cap free
Amount of items: 2
Items: 
Size: 567188 Color: 1
Size: 432748 Color: 3

Bin 1822: 65 of cap free
Amount of items: 2
Items: 
Size: 569718 Color: 4
Size: 430218 Color: 0

Bin 1823: 65 of cap free
Amount of items: 2
Items: 
Size: 588614 Color: 3
Size: 411322 Color: 1

Bin 1824: 65 of cap free
Amount of items: 2
Items: 
Size: 608443 Color: 4
Size: 391493 Color: 3

Bin 1825: 65 of cap free
Amount of items: 3
Items: 
Size: 619104 Color: 2
Size: 195168 Color: 3
Size: 185664 Color: 1

Bin 1826: 65 of cap free
Amount of items: 2
Items: 
Size: 657497 Color: 0
Size: 342439 Color: 3

Bin 1827: 65 of cap free
Amount of items: 2
Items: 
Size: 660119 Color: 0
Size: 339817 Color: 3

Bin 1828: 65 of cap free
Amount of items: 2
Items: 
Size: 666373 Color: 3
Size: 333563 Color: 2

Bin 1829: 65 of cap free
Amount of items: 2
Items: 
Size: 703582 Color: 4
Size: 296354 Color: 3

Bin 1830: 65 of cap free
Amount of items: 2
Items: 
Size: 716252 Color: 1
Size: 283684 Color: 0

Bin 1831: 65 of cap free
Amount of items: 2
Items: 
Size: 719078 Color: 0
Size: 280858 Color: 3

Bin 1832: 65 of cap free
Amount of items: 2
Items: 
Size: 719979 Color: 0
Size: 279957 Color: 2

Bin 1833: 65 of cap free
Amount of items: 2
Items: 
Size: 727196 Color: 0
Size: 272740 Color: 1

Bin 1834: 65 of cap free
Amount of items: 2
Items: 
Size: 749622 Color: 0
Size: 250314 Color: 2

Bin 1835: 65 of cap free
Amount of items: 2
Items: 
Size: 766872 Color: 3
Size: 233064 Color: 0

Bin 1836: 65 of cap free
Amount of items: 2
Items: 
Size: 795451 Color: 4
Size: 204485 Color: 1

Bin 1837: 66 of cap free
Amount of items: 6
Items: 
Size: 167801 Color: 2
Size: 167797 Color: 0
Size: 167793 Color: 4
Size: 167596 Color: 1
Size: 167576 Color: 4
Size: 161372 Color: 2

Bin 1838: 66 of cap free
Amount of items: 2
Items: 
Size: 506083 Color: 0
Size: 493852 Color: 3

Bin 1839: 66 of cap free
Amount of items: 2
Items: 
Size: 527399 Color: 2
Size: 472536 Color: 0

Bin 1840: 66 of cap free
Amount of items: 2
Items: 
Size: 530416 Color: 2
Size: 469519 Color: 3

Bin 1841: 66 of cap free
Amount of items: 2
Items: 
Size: 554387 Color: 3
Size: 445548 Color: 4

Bin 1842: 66 of cap free
Amount of items: 2
Items: 
Size: 559262 Color: 3
Size: 440673 Color: 0

Bin 1843: 66 of cap free
Amount of items: 2
Items: 
Size: 574701 Color: 3
Size: 425234 Color: 1

Bin 1844: 66 of cap free
Amount of items: 2
Items: 
Size: 621046 Color: 2
Size: 378889 Color: 4

Bin 1845: 66 of cap free
Amount of items: 2
Items: 
Size: 626864 Color: 0
Size: 373071 Color: 1

Bin 1846: 66 of cap free
Amount of items: 2
Items: 
Size: 651359 Color: 0
Size: 348576 Color: 1

Bin 1847: 66 of cap free
Amount of items: 2
Items: 
Size: 659970 Color: 2
Size: 339965 Color: 3

Bin 1848: 66 of cap free
Amount of items: 2
Items: 
Size: 680430 Color: 4
Size: 319505 Color: 3

Bin 1849: 66 of cap free
Amount of items: 2
Items: 
Size: 688960 Color: 3
Size: 310975 Color: 1

Bin 1850: 66 of cap free
Amount of items: 2
Items: 
Size: 690504 Color: 3
Size: 309431 Color: 0

Bin 1851: 66 of cap free
Amount of items: 2
Items: 
Size: 715832 Color: 3
Size: 284103 Color: 0

Bin 1852: 66 of cap free
Amount of items: 2
Items: 
Size: 738377 Color: 4
Size: 261558 Color: 1

Bin 1853: 66 of cap free
Amount of items: 2
Items: 
Size: 784496 Color: 1
Size: 215439 Color: 2

Bin 1854: 67 of cap free
Amount of items: 2
Items: 
Size: 506112 Color: 3
Size: 493822 Color: 4

Bin 1855: 67 of cap free
Amount of items: 2
Items: 
Size: 535961 Color: 4
Size: 463973 Color: 1

Bin 1856: 67 of cap free
Amount of items: 2
Items: 
Size: 571856 Color: 3
Size: 428078 Color: 4

Bin 1857: 67 of cap free
Amount of items: 2
Items: 
Size: 579873 Color: 3
Size: 420061 Color: 1

Bin 1858: 67 of cap free
Amount of items: 2
Items: 
Size: 637335 Color: 2
Size: 362599 Color: 0

Bin 1859: 67 of cap free
Amount of items: 2
Items: 
Size: 660857 Color: 1
Size: 339077 Color: 3

Bin 1860: 67 of cap free
Amount of items: 2
Items: 
Size: 670548 Color: 2
Size: 329386 Color: 1

Bin 1861: 67 of cap free
Amount of items: 2
Items: 
Size: 671701 Color: 0
Size: 328233 Color: 3

Bin 1862: 67 of cap free
Amount of items: 2
Items: 
Size: 723386 Color: 0
Size: 276548 Color: 1

Bin 1863: 67 of cap free
Amount of items: 2
Items: 
Size: 731954 Color: 2
Size: 267980 Color: 0

Bin 1864: 67 of cap free
Amount of items: 2
Items: 
Size: 779521 Color: 0
Size: 220413 Color: 1

Bin 1865: 67 of cap free
Amount of items: 2
Items: 
Size: 783048 Color: 3
Size: 216886 Color: 0

Bin 1866: 67 of cap free
Amount of items: 2
Items: 
Size: 787439 Color: 1
Size: 212495 Color: 4

Bin 1867: 68 of cap free
Amount of items: 2
Items: 
Size: 500443 Color: 0
Size: 499490 Color: 3

Bin 1868: 68 of cap free
Amount of items: 2
Items: 
Size: 517081 Color: 3
Size: 482852 Color: 1

Bin 1869: 68 of cap free
Amount of items: 2
Items: 
Size: 554428 Color: 4
Size: 445505 Color: 1

Bin 1870: 68 of cap free
Amount of items: 2
Items: 
Size: 554940 Color: 1
Size: 444993 Color: 4

Bin 1871: 68 of cap free
Amount of items: 2
Items: 
Size: 561584 Color: 4
Size: 438349 Color: 3

Bin 1872: 68 of cap free
Amount of items: 2
Items: 
Size: 582315 Color: 0
Size: 417618 Color: 3

Bin 1873: 68 of cap free
Amount of items: 2
Items: 
Size: 604427 Color: 2
Size: 395506 Color: 0

Bin 1874: 68 of cap free
Amount of items: 2
Items: 
Size: 607552 Color: 4
Size: 392381 Color: 0

Bin 1875: 68 of cap free
Amount of items: 2
Items: 
Size: 641927 Color: 0
Size: 358006 Color: 3

Bin 1876: 68 of cap free
Amount of items: 2
Items: 
Size: 669761 Color: 2
Size: 330172 Color: 4

Bin 1877: 68 of cap free
Amount of items: 2
Items: 
Size: 713014 Color: 2
Size: 286919 Color: 1

Bin 1878: 68 of cap free
Amount of items: 2
Items: 
Size: 721052 Color: 1
Size: 278881 Color: 2

Bin 1879: 68 of cap free
Amount of items: 2
Items: 
Size: 721637 Color: 0
Size: 278296 Color: 2

Bin 1880: 69 of cap free
Amount of items: 2
Items: 
Size: 516870 Color: 1
Size: 483062 Color: 3

Bin 1881: 69 of cap free
Amount of items: 2
Items: 
Size: 522934 Color: 1
Size: 476998 Color: 0

Bin 1882: 69 of cap free
Amount of items: 2
Items: 
Size: 536501 Color: 1
Size: 463431 Color: 0

Bin 1883: 69 of cap free
Amount of items: 2
Items: 
Size: 540923 Color: 3
Size: 459009 Color: 0

Bin 1884: 69 of cap free
Amount of items: 2
Items: 
Size: 543485 Color: 1
Size: 456447 Color: 0

Bin 1885: 69 of cap free
Amount of items: 2
Items: 
Size: 554311 Color: 1
Size: 445621 Color: 2

Bin 1886: 69 of cap free
Amount of items: 2
Items: 
Size: 562136 Color: 4
Size: 437796 Color: 2

Bin 1887: 69 of cap free
Amount of items: 2
Items: 
Size: 570010 Color: 0
Size: 429922 Color: 2

Bin 1888: 69 of cap free
Amount of items: 2
Items: 
Size: 587054 Color: 2
Size: 412878 Color: 3

Bin 1889: 69 of cap free
Amount of items: 2
Items: 
Size: 627663 Color: 0
Size: 372269 Color: 3

Bin 1890: 69 of cap free
Amount of items: 2
Items: 
Size: 653764 Color: 0
Size: 346168 Color: 2

Bin 1891: 69 of cap free
Amount of items: 2
Items: 
Size: 667025 Color: 1
Size: 332907 Color: 3

Bin 1892: 69 of cap free
Amount of items: 2
Items: 
Size: 716506 Color: 1
Size: 283426 Color: 0

Bin 1893: 69 of cap free
Amount of items: 2
Items: 
Size: 718663 Color: 0
Size: 281269 Color: 1

Bin 1894: 69 of cap free
Amount of items: 2
Items: 
Size: 770390 Color: 0
Size: 229542 Color: 2

Bin 1895: 69 of cap free
Amount of items: 2
Items: 
Size: 777923 Color: 2
Size: 222009 Color: 3

Bin 1896: 69 of cap free
Amount of items: 2
Items: 
Size: 792080 Color: 4
Size: 207852 Color: 2

Bin 1897: 70 of cap free
Amount of items: 6
Items: 
Size: 175542 Color: 0
Size: 175516 Color: 3
Size: 175462 Color: 2
Size: 175387 Color: 2
Size: 175340 Color: 3
Size: 122684 Color: 0

Bin 1898: 70 of cap free
Amount of items: 2
Items: 
Size: 508392 Color: 3
Size: 491539 Color: 1

Bin 1899: 70 of cap free
Amount of items: 2
Items: 
Size: 541798 Color: 0
Size: 458133 Color: 3

Bin 1900: 70 of cap free
Amount of items: 2
Items: 
Size: 550481 Color: 2
Size: 449450 Color: 3

Bin 1901: 70 of cap free
Amount of items: 2
Items: 
Size: 572074 Color: 2
Size: 427857 Color: 1

Bin 1902: 70 of cap free
Amount of items: 2
Items: 
Size: 579246 Color: 1
Size: 420685 Color: 3

Bin 1903: 70 of cap free
Amount of items: 2
Items: 
Size: 584346 Color: 4
Size: 415585 Color: 1

Bin 1904: 70 of cap free
Amount of items: 2
Items: 
Size: 590978 Color: 2
Size: 408953 Color: 3

Bin 1905: 70 of cap free
Amount of items: 2
Items: 
Size: 595825 Color: 0
Size: 404106 Color: 2

Bin 1906: 70 of cap free
Amount of items: 2
Items: 
Size: 600271 Color: 3
Size: 399660 Color: 2

Bin 1907: 70 of cap free
Amount of items: 2
Items: 
Size: 605394 Color: 3
Size: 394537 Color: 2

Bin 1908: 70 of cap free
Amount of items: 2
Items: 
Size: 611534 Color: 0
Size: 388397 Color: 4

Bin 1909: 70 of cap free
Amount of items: 2
Items: 
Size: 614798 Color: 0
Size: 385133 Color: 4

Bin 1910: 70 of cap free
Amount of items: 2
Items: 
Size: 636842 Color: 4
Size: 363089 Color: 1

Bin 1911: 70 of cap free
Amount of items: 2
Items: 
Size: 672996 Color: 3
Size: 326935 Color: 0

Bin 1912: 70 of cap free
Amount of items: 2
Items: 
Size: 681091 Color: 4
Size: 318840 Color: 0

Bin 1913: 70 of cap free
Amount of items: 2
Items: 
Size: 694642 Color: 4
Size: 305289 Color: 2

Bin 1914: 70 of cap free
Amount of items: 2
Items: 
Size: 709260 Color: 0
Size: 290671 Color: 2

Bin 1915: 70 of cap free
Amount of items: 2
Items: 
Size: 741565 Color: 2
Size: 258366 Color: 0

Bin 1916: 71 of cap free
Amount of items: 2
Items: 
Size: 503509 Color: 2
Size: 496421 Color: 3

Bin 1917: 71 of cap free
Amount of items: 2
Items: 
Size: 504776 Color: 0
Size: 495154 Color: 1

Bin 1918: 71 of cap free
Amount of items: 2
Items: 
Size: 528256 Color: 3
Size: 471674 Color: 4

Bin 1919: 71 of cap free
Amount of items: 2
Items: 
Size: 538285 Color: 2
Size: 461645 Color: 3

Bin 1920: 71 of cap free
Amount of items: 2
Items: 
Size: 568071 Color: 0
Size: 431859 Color: 1

Bin 1921: 71 of cap free
Amount of items: 2
Items: 
Size: 598122 Color: 2
Size: 401808 Color: 4

Bin 1922: 71 of cap free
Amount of items: 2
Items: 
Size: 655367 Color: 0
Size: 344563 Color: 1

Bin 1923: 71 of cap free
Amount of items: 2
Items: 
Size: 678714 Color: 0
Size: 321216 Color: 4

Bin 1924: 71 of cap free
Amount of items: 2
Items: 
Size: 706008 Color: 0
Size: 293922 Color: 4

Bin 1925: 71 of cap free
Amount of items: 2
Items: 
Size: 732222 Color: 4
Size: 267708 Color: 3

Bin 1926: 71 of cap free
Amount of items: 2
Items: 
Size: 762150 Color: 3
Size: 237780 Color: 0

Bin 1927: 71 of cap free
Amount of items: 2
Items: 
Size: 765894 Color: 3
Size: 234036 Color: 4

Bin 1928: 71 of cap free
Amount of items: 2
Items: 
Size: 781515 Color: 2
Size: 218415 Color: 0

Bin 1929: 71 of cap free
Amount of items: 2
Items: 
Size: 786086 Color: 2
Size: 213844 Color: 4

Bin 1930: 71 of cap free
Amount of items: 2
Items: 
Size: 792853 Color: 0
Size: 207077 Color: 2

Bin 1931: 72 of cap free
Amount of items: 2
Items: 
Size: 603605 Color: 2
Size: 396324 Color: 0

Bin 1932: 72 of cap free
Amount of items: 2
Items: 
Size: 610876 Color: 0
Size: 389053 Color: 1

Bin 1933: 72 of cap free
Amount of items: 2
Items: 
Size: 611847 Color: 4
Size: 388082 Color: 3

Bin 1934: 72 of cap free
Amount of items: 2
Items: 
Size: 618368 Color: 1
Size: 381561 Color: 3

Bin 1935: 72 of cap free
Amount of items: 2
Items: 
Size: 638292 Color: 4
Size: 361637 Color: 2

Bin 1936: 72 of cap free
Amount of items: 2
Items: 
Size: 671217 Color: 4
Size: 328712 Color: 2

Bin 1937: 72 of cap free
Amount of items: 2
Items: 
Size: 733361 Color: 1
Size: 266568 Color: 3

Bin 1938: 72 of cap free
Amount of items: 2
Items: 
Size: 738065 Color: 4
Size: 261864 Color: 1

Bin 1939: 72 of cap free
Amount of items: 2
Items: 
Size: 748236 Color: 1
Size: 251693 Color: 0

Bin 1940: 72 of cap free
Amount of items: 2
Items: 
Size: 778400 Color: 1
Size: 221529 Color: 4

Bin 1941: 72 of cap free
Amount of items: 2
Items: 
Size: 798542 Color: 1
Size: 201387 Color: 2

Bin 1942: 72 of cap free
Amount of items: 2
Items: 
Size: 799078 Color: 4
Size: 200851 Color: 3

Bin 1943: 72 of cap free
Amount of items: 2
Items: 
Size: 799553 Color: 0
Size: 200376 Color: 4

Bin 1944: 73 of cap free
Amount of items: 2
Items: 
Size: 512902 Color: 0
Size: 487026 Color: 2

Bin 1945: 73 of cap free
Amount of items: 2
Items: 
Size: 524149 Color: 1
Size: 475779 Color: 3

Bin 1946: 73 of cap free
Amount of items: 2
Items: 
Size: 532054 Color: 1
Size: 467874 Color: 4

Bin 1947: 73 of cap free
Amount of items: 2
Items: 
Size: 539400 Color: 4
Size: 460528 Color: 1

Bin 1948: 73 of cap free
Amount of items: 2
Items: 
Size: 551424 Color: 2
Size: 448504 Color: 3

Bin 1949: 73 of cap free
Amount of items: 2
Items: 
Size: 589587 Color: 0
Size: 410341 Color: 2

Bin 1950: 73 of cap free
Amount of items: 2
Items: 
Size: 607620 Color: 0
Size: 392308 Color: 2

Bin 1951: 73 of cap free
Amount of items: 2
Items: 
Size: 619076 Color: 4
Size: 380852 Color: 2

Bin 1952: 73 of cap free
Amount of items: 2
Items: 
Size: 628166 Color: 1
Size: 371762 Color: 0

Bin 1953: 73 of cap free
Amount of items: 2
Items: 
Size: 646320 Color: 2
Size: 353608 Color: 3

Bin 1954: 73 of cap free
Amount of items: 2
Items: 
Size: 650800 Color: 1
Size: 349128 Color: 0

Bin 1955: 73 of cap free
Amount of items: 2
Items: 
Size: 656617 Color: 1
Size: 343311 Color: 4

Bin 1956: 73 of cap free
Amount of items: 2
Items: 
Size: 657845 Color: 4
Size: 342083 Color: 2

Bin 1957: 73 of cap free
Amount of items: 2
Items: 
Size: 678952 Color: 3
Size: 320976 Color: 2

Bin 1958: 73 of cap free
Amount of items: 2
Items: 
Size: 685623 Color: 4
Size: 314305 Color: 1

Bin 1959: 73 of cap free
Amount of items: 2
Items: 
Size: 701383 Color: 3
Size: 298545 Color: 0

Bin 1960: 73 of cap free
Amount of items: 2
Items: 
Size: 705802 Color: 3
Size: 294126 Color: 2

Bin 1961: 73 of cap free
Amount of items: 2
Items: 
Size: 739695 Color: 4
Size: 260233 Color: 3

Bin 1962: 73 of cap free
Amount of items: 2
Items: 
Size: 750483 Color: 1
Size: 249445 Color: 3

Bin 1963: 73 of cap free
Amount of items: 2
Items: 
Size: 787956 Color: 0
Size: 211972 Color: 2

Bin 1964: 73 of cap free
Amount of items: 2
Items: 
Size: 798746 Color: 0
Size: 201182 Color: 4

Bin 1965: 74 of cap free
Amount of items: 2
Items: 
Size: 502351 Color: 0
Size: 497576 Color: 2

Bin 1966: 74 of cap free
Amount of items: 2
Items: 
Size: 538865 Color: 0
Size: 461062 Color: 2

Bin 1967: 74 of cap free
Amount of items: 2
Items: 
Size: 542650 Color: 3
Size: 457277 Color: 2

Bin 1968: 74 of cap free
Amount of items: 2
Items: 
Size: 552638 Color: 2
Size: 447289 Color: 0

Bin 1969: 74 of cap free
Amount of items: 2
Items: 
Size: 562711 Color: 0
Size: 437216 Color: 2

Bin 1970: 74 of cap free
Amount of items: 2
Items: 
Size: 563153 Color: 4
Size: 436774 Color: 2

Bin 1971: 74 of cap free
Amount of items: 2
Items: 
Size: 589946 Color: 1
Size: 409981 Color: 4

Bin 1972: 74 of cap free
Amount of items: 2
Items: 
Size: 604337 Color: 0
Size: 395590 Color: 2

Bin 1973: 74 of cap free
Amount of items: 2
Items: 
Size: 611163 Color: 3
Size: 388764 Color: 2

Bin 1974: 74 of cap free
Amount of items: 2
Items: 
Size: 643853 Color: 3
Size: 356074 Color: 2

Bin 1975: 74 of cap free
Amount of items: 2
Items: 
Size: 671087 Color: 3
Size: 328840 Color: 4

Bin 1976: 74 of cap free
Amount of items: 2
Items: 
Size: 707765 Color: 4
Size: 292162 Color: 2

Bin 1977: 74 of cap free
Amount of items: 2
Items: 
Size: 714773 Color: 3
Size: 285154 Color: 1

Bin 1978: 74 of cap free
Amount of items: 2
Items: 
Size: 729394 Color: 2
Size: 270533 Color: 3

Bin 1979: 74 of cap free
Amount of items: 2
Items: 
Size: 733233 Color: 4
Size: 266694 Color: 1

Bin 1980: 74 of cap free
Amount of items: 2
Items: 
Size: 739092 Color: 4
Size: 260835 Color: 2

Bin 1981: 74 of cap free
Amount of items: 2
Items: 
Size: 740347 Color: 2
Size: 259580 Color: 4

Bin 1982: 74 of cap free
Amount of items: 2
Items: 
Size: 742442 Color: 1
Size: 257485 Color: 3

Bin 1983: 74 of cap free
Amount of items: 2
Items: 
Size: 765654 Color: 4
Size: 234273 Color: 1

Bin 1984: 74 of cap free
Amount of items: 2
Items: 
Size: 778049 Color: 1
Size: 221878 Color: 4

Bin 1985: 74 of cap free
Amount of items: 2
Items: 
Size: 780278 Color: 0
Size: 219649 Color: 1

Bin 1986: 74 of cap free
Amount of items: 2
Items: 
Size: 798400 Color: 2
Size: 201527 Color: 4

Bin 1987: 75 of cap free
Amount of items: 2
Items: 
Size: 531139 Color: 0
Size: 468787 Color: 3

Bin 1988: 75 of cap free
Amount of items: 2
Items: 
Size: 535017 Color: 4
Size: 464909 Color: 3

Bin 1989: 75 of cap free
Amount of items: 2
Items: 
Size: 564864 Color: 3
Size: 435062 Color: 0

Bin 1990: 75 of cap free
Amount of items: 2
Items: 
Size: 567067 Color: 4
Size: 432859 Color: 1

Bin 1991: 75 of cap free
Amount of items: 2
Items: 
Size: 606529 Color: 2
Size: 393397 Color: 0

Bin 1992: 75 of cap free
Amount of items: 2
Items: 
Size: 623976 Color: 4
Size: 375950 Color: 2

Bin 1993: 75 of cap free
Amount of items: 2
Items: 
Size: 625852 Color: 3
Size: 374074 Color: 2

Bin 1994: 75 of cap free
Amount of items: 2
Items: 
Size: 634672 Color: 0
Size: 365254 Color: 2

Bin 1995: 75 of cap free
Amount of items: 2
Items: 
Size: 649506 Color: 2
Size: 350420 Color: 4

Bin 1996: 75 of cap free
Amount of items: 2
Items: 
Size: 691856 Color: 4
Size: 308070 Color: 3

Bin 1997: 76 of cap free
Amount of items: 2
Items: 
Size: 517756 Color: 4
Size: 482169 Color: 2

Bin 1998: 76 of cap free
Amount of items: 2
Items: 
Size: 540445 Color: 4
Size: 459480 Color: 2

Bin 1999: 76 of cap free
Amount of items: 2
Items: 
Size: 587359 Color: 4
Size: 412566 Color: 1

Bin 2000: 76 of cap free
Amount of items: 2
Items: 
Size: 603776 Color: 0
Size: 396149 Color: 1

Bin 2001: 76 of cap free
Amount of items: 2
Items: 
Size: 615736 Color: 2
Size: 384189 Color: 0

Bin 2002: 76 of cap free
Amount of items: 2
Items: 
Size: 616791 Color: 4
Size: 383134 Color: 3

Bin 2003: 76 of cap free
Amount of items: 2
Items: 
Size: 635791 Color: 3
Size: 364134 Color: 0

Bin 2004: 76 of cap free
Amount of items: 2
Items: 
Size: 663187 Color: 0
Size: 336738 Color: 1

Bin 2005: 76 of cap free
Amount of items: 2
Items: 
Size: 701650 Color: 3
Size: 298275 Color: 2

Bin 2006: 76 of cap free
Amount of items: 2
Items: 
Size: 723357 Color: 3
Size: 276568 Color: 0

Bin 2007: 76 of cap free
Amount of items: 2
Items: 
Size: 748103 Color: 1
Size: 251822 Color: 4

Bin 2008: 76 of cap free
Amount of items: 2
Items: 
Size: 757057 Color: 1
Size: 242868 Color: 2

Bin 2009: 76 of cap free
Amount of items: 2
Items: 
Size: 798386 Color: 4
Size: 201539 Color: 2

Bin 2010: 77 of cap free
Amount of items: 2
Items: 
Size: 535135 Color: 1
Size: 464789 Color: 3

Bin 2011: 77 of cap free
Amount of items: 2
Items: 
Size: 537024 Color: 2
Size: 462900 Color: 1

Bin 2012: 77 of cap free
Amount of items: 2
Items: 
Size: 549138 Color: 0
Size: 450786 Color: 3

Bin 2013: 77 of cap free
Amount of items: 2
Items: 
Size: 566849 Color: 1
Size: 433075 Color: 4

Bin 2014: 77 of cap free
Amount of items: 2
Items: 
Size: 574824 Color: 0
Size: 425100 Color: 3

Bin 2015: 77 of cap free
Amount of items: 2
Items: 
Size: 663724 Color: 2
Size: 336200 Color: 0

Bin 2016: 77 of cap free
Amount of items: 2
Items: 
Size: 666965 Color: 0
Size: 332959 Color: 1

Bin 2017: 77 of cap free
Amount of items: 2
Items: 
Size: 670211 Color: 4
Size: 329713 Color: 2

Bin 2018: 77 of cap free
Amount of items: 2
Items: 
Size: 679565 Color: 3
Size: 320359 Color: 1

Bin 2019: 77 of cap free
Amount of items: 2
Items: 
Size: 686482 Color: 2
Size: 313442 Color: 1

Bin 2020: 77 of cap free
Amount of items: 2
Items: 
Size: 696997 Color: 1
Size: 302927 Color: 3

Bin 2021: 77 of cap free
Amount of items: 2
Items: 
Size: 701381 Color: 1
Size: 298543 Color: 0

Bin 2022: 77 of cap free
Amount of items: 2
Items: 
Size: 711170 Color: 4
Size: 288754 Color: 3

Bin 2023: 77 of cap free
Amount of items: 2
Items: 
Size: 744603 Color: 0
Size: 255321 Color: 2

Bin 2024: 77 of cap free
Amount of items: 2
Items: 
Size: 758938 Color: 1
Size: 240986 Color: 2

Bin 2025: 77 of cap free
Amount of items: 2
Items: 
Size: 759744 Color: 4
Size: 240180 Color: 3

Bin 2026: 77 of cap free
Amount of items: 2
Items: 
Size: 782905 Color: 0
Size: 217019 Color: 1

Bin 2027: 77 of cap free
Amount of items: 2
Items: 
Size: 792488 Color: 2
Size: 207436 Color: 3

Bin 2028: 78 of cap free
Amount of items: 6
Items: 
Size: 168013 Color: 2
Size: 167981 Color: 2
Size: 167944 Color: 0
Size: 167875 Color: 2
Size: 167797 Color: 3
Size: 160313 Color: 3

Bin 2029: 78 of cap free
Amount of items: 6
Items: 
Size: 170832 Color: 2
Size: 170751 Color: 1
Size: 170671 Color: 3
Size: 170434 Color: 0
Size: 170263 Color: 1
Size: 146972 Color: 3

Bin 2030: 78 of cap free
Amount of items: 2
Items: 
Size: 503702 Color: 0
Size: 496221 Color: 3

Bin 2031: 78 of cap free
Amount of items: 2
Items: 
Size: 533291 Color: 0
Size: 466632 Color: 4

Bin 2032: 78 of cap free
Amount of items: 2
Items: 
Size: 539181 Color: 3
Size: 460742 Color: 4

Bin 2033: 78 of cap free
Amount of items: 2
Items: 
Size: 571148 Color: 3
Size: 428775 Color: 0

Bin 2034: 78 of cap free
Amount of items: 2
Items: 
Size: 579115 Color: 0
Size: 420808 Color: 3

Bin 2035: 78 of cap free
Amount of items: 2
Items: 
Size: 580132 Color: 4
Size: 419791 Color: 3

Bin 2036: 78 of cap free
Amount of items: 2
Items: 
Size: 676862 Color: 0
Size: 323061 Color: 2

Bin 2037: 78 of cap free
Amount of items: 2
Items: 
Size: 714564 Color: 3
Size: 285359 Color: 4

Bin 2038: 78 of cap free
Amount of items: 2
Items: 
Size: 743709 Color: 4
Size: 256214 Color: 0

Bin 2039: 78 of cap free
Amount of items: 2
Items: 
Size: 752210 Color: 2
Size: 247713 Color: 0

Bin 2040: 78 of cap free
Amount of items: 2
Items: 
Size: 762987 Color: 4
Size: 236936 Color: 0

Bin 2041: 78 of cap free
Amount of items: 2
Items: 
Size: 781925 Color: 1
Size: 217998 Color: 0

Bin 2042: 79 of cap free
Amount of items: 2
Items: 
Size: 504634 Color: 2
Size: 495288 Color: 4

Bin 2043: 79 of cap free
Amount of items: 2
Items: 
Size: 524745 Color: 4
Size: 475177 Color: 1

Bin 2044: 79 of cap free
Amount of items: 2
Items: 
Size: 546944 Color: 4
Size: 452978 Color: 1

Bin 2045: 79 of cap free
Amount of items: 2
Items: 
Size: 587813 Color: 1
Size: 412109 Color: 3

Bin 2046: 79 of cap free
Amount of items: 2
Items: 
Size: 654121 Color: 3
Size: 345801 Color: 2

Bin 2047: 79 of cap free
Amount of items: 2
Items: 
Size: 687415 Color: 4
Size: 312507 Color: 3

Bin 2048: 79 of cap free
Amount of items: 2
Items: 
Size: 688875 Color: 1
Size: 311047 Color: 3

Bin 2049: 79 of cap free
Amount of items: 2
Items: 
Size: 704316 Color: 2
Size: 295606 Color: 4

Bin 2050: 79 of cap free
Amount of items: 2
Items: 
Size: 712227 Color: 3
Size: 287695 Color: 0

Bin 2051: 79 of cap free
Amount of items: 2
Items: 
Size: 746969 Color: 4
Size: 252953 Color: 1

Bin 2052: 79 of cap free
Amount of items: 2
Items: 
Size: 764166 Color: 0
Size: 235756 Color: 4

Bin 2053: 79 of cap free
Amount of items: 2
Items: 
Size: 774387 Color: 3
Size: 225535 Color: 4

Bin 2054: 79 of cap free
Amount of items: 2
Items: 
Size: 785027 Color: 1
Size: 214895 Color: 4

Bin 2055: 80 of cap free
Amount of items: 2
Items: 
Size: 503909 Color: 0
Size: 496012 Color: 3

Bin 2056: 80 of cap free
Amount of items: 2
Items: 
Size: 516243 Color: 1
Size: 483678 Color: 0

Bin 2057: 80 of cap free
Amount of items: 2
Items: 
Size: 532971 Color: 4
Size: 466950 Color: 3

Bin 2058: 80 of cap free
Amount of items: 2
Items: 
Size: 541252 Color: 2
Size: 458669 Color: 4

Bin 2059: 80 of cap free
Amount of items: 2
Items: 
Size: 590693 Color: 1
Size: 409228 Color: 4

Bin 2060: 80 of cap free
Amount of items: 2
Items: 
Size: 601897 Color: 3
Size: 398024 Color: 2

Bin 2061: 80 of cap free
Amount of items: 2
Items: 
Size: 602038 Color: 2
Size: 397883 Color: 1

Bin 2062: 80 of cap free
Amount of items: 2
Items: 
Size: 615980 Color: 4
Size: 383941 Color: 3

Bin 2063: 80 of cap free
Amount of items: 2
Items: 
Size: 619995 Color: 0
Size: 379926 Color: 3

Bin 2064: 80 of cap free
Amount of items: 2
Items: 
Size: 639735 Color: 4
Size: 360186 Color: 3

Bin 2065: 80 of cap free
Amount of items: 2
Items: 
Size: 656908 Color: 4
Size: 343013 Color: 1

Bin 2066: 80 of cap free
Amount of items: 2
Items: 
Size: 673452 Color: 1
Size: 326469 Color: 3

Bin 2067: 80 of cap free
Amount of items: 2
Items: 
Size: 683006 Color: 2
Size: 316915 Color: 4

Bin 2068: 80 of cap free
Amount of items: 2
Items: 
Size: 699114 Color: 0
Size: 300807 Color: 1

Bin 2069: 80 of cap free
Amount of items: 2
Items: 
Size: 732409 Color: 0
Size: 267512 Color: 3

Bin 2070: 80 of cap free
Amount of items: 2
Items: 
Size: 736909 Color: 1
Size: 263012 Color: 4

Bin 2071: 81 of cap free
Amount of items: 3
Items: 
Size: 619291 Color: 3
Size: 195148 Color: 1
Size: 185481 Color: 0

Bin 2072: 81 of cap free
Amount of items: 2
Items: 
Size: 621965 Color: 1
Size: 377955 Color: 0

Bin 2073: 81 of cap free
Amount of items: 2
Items: 
Size: 646425 Color: 4
Size: 353495 Color: 1

Bin 2074: 81 of cap free
Amount of items: 2
Items: 
Size: 709029 Color: 4
Size: 290891 Color: 0

Bin 2075: 81 of cap free
Amount of items: 2
Items: 
Size: 720791 Color: 0
Size: 279129 Color: 4

Bin 2076: 81 of cap free
Amount of items: 2
Items: 
Size: 734049 Color: 4
Size: 265871 Color: 2

Bin 2077: 81 of cap free
Amount of items: 2
Items: 
Size: 783169 Color: 4
Size: 216751 Color: 3

Bin 2078: 81 of cap free
Amount of items: 2
Items: 
Size: 783649 Color: 4
Size: 216271 Color: 3

Bin 2079: 81 of cap free
Amount of items: 2
Items: 
Size: 790731 Color: 4
Size: 209189 Color: 1

Bin 2080: 82 of cap free
Amount of items: 7
Items: 
Size: 144171 Color: 2
Size: 144165 Color: 3
Size: 144048 Color: 1
Size: 144004 Color: 3
Size: 143938 Color: 1
Size: 143861 Color: 2
Size: 135732 Color: 1

Bin 2081: 82 of cap free
Amount of items: 2
Items: 
Size: 511454 Color: 0
Size: 488465 Color: 3

Bin 2082: 82 of cap free
Amount of items: 2
Items: 
Size: 566299 Color: 4
Size: 433620 Color: 0

Bin 2083: 82 of cap free
Amount of items: 2
Items: 
Size: 567650 Color: 0
Size: 432269 Color: 3

Bin 2084: 82 of cap free
Amount of items: 2
Items: 
Size: 571501 Color: 1
Size: 428418 Color: 2

Bin 2085: 82 of cap free
Amount of items: 2
Items: 
Size: 589940 Color: 4
Size: 409979 Color: 1

Bin 2086: 82 of cap free
Amount of items: 2
Items: 
Size: 615546 Color: 2
Size: 384373 Color: 4

Bin 2087: 82 of cap free
Amount of items: 2
Items: 
Size: 624361 Color: 4
Size: 375558 Color: 1

Bin 2088: 82 of cap free
Amount of items: 2
Items: 
Size: 739989 Color: 3
Size: 259930 Color: 4

Bin 2089: 82 of cap free
Amount of items: 2
Items: 
Size: 770050 Color: 4
Size: 229869 Color: 2

Bin 2090: 82 of cap free
Amount of items: 2
Items: 
Size: 773430 Color: 2
Size: 226489 Color: 0

Bin 2091: 82 of cap free
Amount of items: 2
Items: 
Size: 792498 Color: 3
Size: 207421 Color: 2

Bin 2092: 82 of cap free
Amount of items: 2
Items: 
Size: 795449 Color: 1
Size: 204470 Color: 3

Bin 2093: 83 of cap free
Amount of items: 2
Items: 
Size: 557281 Color: 1
Size: 442637 Color: 0

Bin 2094: 83 of cap free
Amount of items: 2
Items: 
Size: 560961 Color: 2
Size: 438957 Color: 1

Bin 2095: 83 of cap free
Amount of items: 2
Items: 
Size: 565673 Color: 3
Size: 434245 Color: 1

Bin 2096: 83 of cap free
Amount of items: 2
Items: 
Size: 649652 Color: 3
Size: 350266 Color: 1

Bin 2097: 83 of cap free
Amount of items: 2
Items: 
Size: 683769 Color: 4
Size: 316149 Color: 3

Bin 2098: 83 of cap free
Amount of items: 2
Items: 
Size: 713733 Color: 0
Size: 286185 Color: 1

Bin 2099: 83 of cap free
Amount of items: 2
Items: 
Size: 774056 Color: 1
Size: 225862 Color: 4

Bin 2100: 84 of cap free
Amount of items: 2
Items: 
Size: 515949 Color: 4
Size: 483968 Color: 1

Bin 2101: 84 of cap free
Amount of items: 2
Items: 
Size: 596749 Color: 0
Size: 403168 Color: 1

Bin 2102: 84 of cap free
Amount of items: 3
Items: 
Size: 619066 Color: 3
Size: 194827 Color: 0
Size: 186024 Color: 4

Bin 2103: 84 of cap free
Amount of items: 2
Items: 
Size: 668551 Color: 0
Size: 331366 Color: 4

Bin 2104: 84 of cap free
Amount of items: 2
Items: 
Size: 672278 Color: 3
Size: 327639 Color: 2

Bin 2105: 84 of cap free
Amount of items: 2
Items: 
Size: 677646 Color: 1
Size: 322271 Color: 4

Bin 2106: 84 of cap free
Amount of items: 2
Items: 
Size: 685873 Color: 3
Size: 314044 Color: 1

Bin 2107: 84 of cap free
Amount of items: 2
Items: 
Size: 735345 Color: 3
Size: 264572 Color: 1

Bin 2108: 84 of cap free
Amount of items: 2
Items: 
Size: 741782 Color: 2
Size: 258135 Color: 4

Bin 2109: 84 of cap free
Amount of items: 2
Items: 
Size: 747006 Color: 1
Size: 252911 Color: 3

Bin 2110: 84 of cap free
Amount of items: 2
Items: 
Size: 751988 Color: 2
Size: 247929 Color: 0

Bin 2111: 84 of cap free
Amount of items: 2
Items: 
Size: 769734 Color: 0
Size: 230183 Color: 1

Bin 2112: 84 of cap free
Amount of items: 2
Items: 
Size: 788927 Color: 0
Size: 210990 Color: 1

Bin 2113: 85 of cap free
Amount of items: 6
Items: 
Size: 176456 Color: 3
Size: 176412 Color: 1
Size: 176349 Color: 2
Size: 176302 Color: 1
Size: 176239 Color: 4
Size: 118158 Color: 0

Bin 2114: 85 of cap free
Amount of items: 2
Items: 
Size: 550985 Color: 1
Size: 448931 Color: 2

Bin 2115: 85 of cap free
Amount of items: 2
Items: 
Size: 571351 Color: 2
Size: 428565 Color: 1

Bin 2116: 85 of cap free
Amount of items: 2
Items: 
Size: 612724 Color: 2
Size: 387192 Color: 1

Bin 2117: 85 of cap free
Amount of items: 2
Items: 
Size: 617553 Color: 2
Size: 382363 Color: 3

Bin 2118: 85 of cap free
Amount of items: 2
Items: 
Size: 687333 Color: 1
Size: 312583 Color: 4

Bin 2119: 85 of cap free
Amount of items: 2
Items: 
Size: 695672 Color: 1
Size: 304244 Color: 2

Bin 2120: 85 of cap free
Amount of items: 2
Items: 
Size: 723718 Color: 0
Size: 276198 Color: 3

Bin 2121: 85 of cap free
Amount of items: 2
Items: 
Size: 725224 Color: 0
Size: 274692 Color: 1

Bin 2122: 85 of cap free
Amount of items: 2
Items: 
Size: 758256 Color: 4
Size: 241660 Color: 1

Bin 2123: 85 of cap free
Amount of items: 2
Items: 
Size: 772995 Color: 0
Size: 226921 Color: 3

Bin 2124: 86 of cap free
Amount of items: 2
Items: 
Size: 523321 Color: 0
Size: 476594 Color: 4

Bin 2125: 86 of cap free
Amount of items: 2
Items: 
Size: 526566 Color: 1
Size: 473349 Color: 3

Bin 2126: 86 of cap free
Amount of items: 2
Items: 
Size: 554709 Color: 4
Size: 445206 Color: 0

Bin 2127: 86 of cap free
Amount of items: 2
Items: 
Size: 573539 Color: 3
Size: 426376 Color: 1

Bin 2128: 86 of cap free
Amount of items: 2
Items: 
Size: 633333 Color: 3
Size: 366582 Color: 1

Bin 2129: 86 of cap free
Amount of items: 2
Items: 
Size: 692625 Color: 2
Size: 307290 Color: 0

Bin 2130: 86 of cap free
Amount of items: 2
Items: 
Size: 794531 Color: 2
Size: 205384 Color: 3

Bin 2131: 86 of cap free
Amount of items: 2
Items: 
Size: 795221 Color: 3
Size: 204694 Color: 4

Bin 2132: 87 of cap free
Amount of items: 2
Items: 
Size: 509887 Color: 1
Size: 490027 Color: 0

Bin 2133: 87 of cap free
Amount of items: 2
Items: 
Size: 558432 Color: 4
Size: 441482 Color: 2

Bin 2134: 87 of cap free
Amount of items: 2
Items: 
Size: 561123 Color: 3
Size: 438791 Color: 1

Bin 2135: 87 of cap free
Amount of items: 2
Items: 
Size: 627550 Color: 0
Size: 372364 Color: 1

Bin 2136: 87 of cap free
Amount of items: 2
Items: 
Size: 653037 Color: 4
Size: 346877 Color: 3

Bin 2137: 87 of cap free
Amount of items: 2
Items: 
Size: 662183 Color: 2
Size: 337731 Color: 4

Bin 2138: 87 of cap free
Amount of items: 2
Items: 
Size: 662380 Color: 0
Size: 337534 Color: 3

Bin 2139: 87 of cap free
Amount of items: 2
Items: 
Size: 678589 Color: 1
Size: 321325 Color: 2

Bin 2140: 87 of cap free
Amount of items: 2
Items: 
Size: 701067 Color: 2
Size: 298847 Color: 1

Bin 2141: 87 of cap free
Amount of items: 2
Items: 
Size: 732166 Color: 0
Size: 267748 Color: 4

Bin 2142: 87 of cap free
Amount of items: 2
Items: 
Size: 737076 Color: 2
Size: 262838 Color: 0

Bin 2143: 88 of cap free
Amount of items: 2
Items: 
Size: 520623 Color: 3
Size: 479290 Color: 2

Bin 2144: 88 of cap free
Amount of items: 2
Items: 
Size: 542847 Color: 3
Size: 457066 Color: 1

Bin 2145: 88 of cap free
Amount of items: 2
Items: 
Size: 557038 Color: 2
Size: 442875 Color: 3

Bin 2146: 88 of cap free
Amount of items: 2
Items: 
Size: 617719 Color: 0
Size: 382194 Color: 2

Bin 2147: 88 of cap free
Amount of items: 2
Items: 
Size: 623500 Color: 0
Size: 376413 Color: 2

Bin 2148: 88 of cap free
Amount of items: 2
Items: 
Size: 657294 Color: 3
Size: 342619 Color: 0

Bin 2149: 88 of cap free
Amount of items: 2
Items: 
Size: 684005 Color: 0
Size: 315908 Color: 3

Bin 2150: 88 of cap free
Amount of items: 2
Items: 
Size: 694722 Color: 1
Size: 305191 Color: 4

Bin 2151: 88 of cap free
Amount of items: 2
Items: 
Size: 716491 Color: 4
Size: 283422 Color: 2

Bin 2152: 88 of cap free
Amount of items: 2
Items: 
Size: 735970 Color: 3
Size: 263943 Color: 1

Bin 2153: 88 of cap free
Amount of items: 2
Items: 
Size: 755738 Color: 1
Size: 244175 Color: 0

Bin 2154: 88 of cap free
Amount of items: 2
Items: 
Size: 777510 Color: 2
Size: 222403 Color: 4

Bin 2155: 89 of cap free
Amount of items: 4
Items: 
Size: 515142 Color: 3
Size: 189605 Color: 2
Size: 189598 Color: 1
Size: 105567 Color: 2

Bin 2156: 89 of cap free
Amount of items: 2
Items: 
Size: 518422 Color: 2
Size: 481490 Color: 1

Bin 2157: 89 of cap free
Amount of items: 2
Items: 
Size: 540024 Color: 0
Size: 459888 Color: 1

Bin 2158: 89 of cap free
Amount of items: 2
Items: 
Size: 550797 Color: 4
Size: 449115 Color: 0

Bin 2159: 89 of cap free
Amount of items: 2
Items: 
Size: 628968 Color: 0
Size: 370944 Color: 1

Bin 2160: 89 of cap free
Amount of items: 2
Items: 
Size: 700251 Color: 2
Size: 299661 Color: 1

Bin 2161: 89 of cap free
Amount of items: 2
Items: 
Size: 749675 Color: 2
Size: 250237 Color: 3

Bin 2162: 89 of cap free
Amount of items: 2
Items: 
Size: 789434 Color: 2
Size: 210478 Color: 4

Bin 2163: 90 of cap free
Amount of items: 2
Items: 
Size: 514161 Color: 4
Size: 485750 Color: 0

Bin 2164: 90 of cap free
Amount of items: 2
Items: 
Size: 519070 Color: 3
Size: 480841 Color: 0

Bin 2165: 90 of cap free
Amount of items: 2
Items: 
Size: 563795 Color: 1
Size: 436116 Color: 3

Bin 2166: 90 of cap free
Amount of items: 2
Items: 
Size: 577929 Color: 2
Size: 421982 Color: 4

Bin 2167: 90 of cap free
Amount of items: 2
Items: 
Size: 607533 Color: 3
Size: 392378 Color: 0

Bin 2168: 90 of cap free
Amount of items: 2
Items: 
Size: 644247 Color: 4
Size: 355664 Color: 3

Bin 2169: 90 of cap free
Amount of items: 2
Items: 
Size: 725813 Color: 2
Size: 274098 Color: 3

Bin 2170: 90 of cap free
Amount of items: 2
Items: 
Size: 733845 Color: 1
Size: 266066 Color: 3

Bin 2171: 91 of cap free
Amount of items: 2
Items: 
Size: 510263 Color: 0
Size: 489647 Color: 3

Bin 2172: 91 of cap free
Amount of items: 2
Items: 
Size: 561444 Color: 3
Size: 438466 Color: 4

Bin 2173: 91 of cap free
Amount of items: 2
Items: 
Size: 620114 Color: 0
Size: 379796 Color: 3

Bin 2174: 91 of cap free
Amount of items: 2
Items: 
Size: 636415 Color: 2
Size: 363495 Color: 1

Bin 2175: 91 of cap free
Amount of items: 2
Items: 
Size: 696826 Color: 2
Size: 303084 Color: 4

Bin 2176: 91 of cap free
Amount of items: 2
Items: 
Size: 789676 Color: 0
Size: 210234 Color: 1

Bin 2177: 92 of cap free
Amount of items: 2
Items: 
Size: 502626 Color: 3
Size: 497283 Color: 4

Bin 2178: 92 of cap free
Amount of items: 2
Items: 
Size: 506700 Color: 4
Size: 493209 Color: 1

Bin 2179: 92 of cap free
Amount of items: 2
Items: 
Size: 524052 Color: 4
Size: 475857 Color: 1

Bin 2180: 92 of cap free
Amount of items: 2
Items: 
Size: 527814 Color: 1
Size: 472095 Color: 2

Bin 2181: 92 of cap free
Amount of items: 2
Items: 
Size: 536087 Color: 3
Size: 463822 Color: 4

Bin 2182: 92 of cap free
Amount of items: 2
Items: 
Size: 537383 Color: 1
Size: 462526 Color: 3

Bin 2183: 92 of cap free
Amount of items: 2
Items: 
Size: 543475 Color: 2
Size: 456434 Color: 0

Bin 2184: 92 of cap free
Amount of items: 2
Items: 
Size: 581723 Color: 3
Size: 418186 Color: 0

Bin 2185: 92 of cap free
Amount of items: 2
Items: 
Size: 582314 Color: 3
Size: 417595 Color: 2

Bin 2186: 92 of cap free
Amount of items: 2
Items: 
Size: 582725 Color: 0
Size: 417184 Color: 2

Bin 2187: 92 of cap free
Amount of items: 2
Items: 
Size: 601673 Color: 3
Size: 398236 Color: 4

Bin 2188: 92 of cap free
Amount of items: 2
Items: 
Size: 637744 Color: 0
Size: 362165 Color: 1

Bin 2189: 92 of cap free
Amount of items: 2
Items: 
Size: 648308 Color: 2
Size: 351601 Color: 3

Bin 2190: 92 of cap free
Amount of items: 2
Items: 
Size: 661390 Color: 4
Size: 338519 Color: 3

Bin 2191: 92 of cap free
Amount of items: 2
Items: 
Size: 711783 Color: 1
Size: 288126 Color: 4

Bin 2192: 92 of cap free
Amount of items: 2
Items: 
Size: 770761 Color: 3
Size: 229148 Color: 4

Bin 2193: 92 of cap free
Amount of items: 2
Items: 
Size: 772382 Color: 3
Size: 227527 Color: 0

Bin 2194: 92 of cap free
Amount of items: 2
Items: 
Size: 776867 Color: 2
Size: 223042 Color: 1

Bin 2195: 93 of cap free
Amount of items: 2
Items: 
Size: 505074 Color: 1
Size: 494834 Color: 0

Bin 2196: 93 of cap free
Amount of items: 2
Items: 
Size: 513464 Color: 2
Size: 486444 Color: 4

Bin 2197: 93 of cap free
Amount of items: 2
Items: 
Size: 539390 Color: 1
Size: 460518 Color: 0

Bin 2198: 93 of cap free
Amount of items: 2
Items: 
Size: 575148 Color: 0
Size: 424760 Color: 4

Bin 2199: 93 of cap free
Amount of items: 2
Items: 
Size: 625457 Color: 3
Size: 374451 Color: 2

Bin 2200: 93 of cap free
Amount of items: 2
Items: 
Size: 626579 Color: 4
Size: 373329 Color: 0

Bin 2201: 93 of cap free
Amount of items: 2
Items: 
Size: 648747 Color: 1
Size: 351161 Color: 4

Bin 2202: 93 of cap free
Amount of items: 2
Items: 
Size: 653156 Color: 2
Size: 346752 Color: 0

Bin 2203: 93 of cap free
Amount of items: 2
Items: 
Size: 690652 Color: 1
Size: 309256 Color: 3

Bin 2204: 93 of cap free
Amount of items: 2
Items: 
Size: 705278 Color: 3
Size: 294630 Color: 4

Bin 2205: 93 of cap free
Amount of items: 2
Items: 
Size: 759620 Color: 2
Size: 240288 Color: 0

Bin 2206: 94 of cap free
Amount of items: 2
Items: 
Size: 548462 Color: 1
Size: 451445 Color: 0

Bin 2207: 94 of cap free
Amount of items: 2
Items: 
Size: 566186 Color: 3
Size: 433721 Color: 1

Bin 2208: 94 of cap free
Amount of items: 2
Items: 
Size: 608588 Color: 1
Size: 391319 Color: 3

Bin 2209: 94 of cap free
Amount of items: 2
Items: 
Size: 626605 Color: 0
Size: 373302 Color: 3

Bin 2210: 94 of cap free
Amount of items: 2
Items: 
Size: 678171 Color: 2
Size: 321736 Color: 3

Bin 2211: 94 of cap free
Amount of items: 2
Items: 
Size: 693516 Color: 0
Size: 306391 Color: 3

Bin 2212: 94 of cap free
Amount of items: 2
Items: 
Size: 756174 Color: 4
Size: 243733 Color: 0

Bin 2213: 95 of cap free
Amount of items: 2
Items: 
Size: 544966 Color: 0
Size: 454940 Color: 4

Bin 2214: 95 of cap free
Amount of items: 2
Items: 
Size: 555771 Color: 0
Size: 444135 Color: 3

Bin 2215: 95 of cap free
Amount of items: 2
Items: 
Size: 562707 Color: 4
Size: 437199 Color: 2

Bin 2216: 95 of cap free
Amount of items: 2
Items: 
Size: 583051 Color: 0
Size: 416855 Color: 1

Bin 2217: 95 of cap free
Amount of items: 2
Items: 
Size: 609066 Color: 0
Size: 390840 Color: 3

Bin 2218: 95 of cap free
Amount of items: 2
Items: 
Size: 659034 Color: 2
Size: 340872 Color: 3

Bin 2219: 95 of cap free
Amount of items: 2
Items: 
Size: 702087 Color: 3
Size: 297819 Color: 2

Bin 2220: 95 of cap free
Amount of items: 2
Items: 
Size: 709898 Color: 0
Size: 290008 Color: 3

Bin 2221: 95 of cap free
Amount of items: 2
Items: 
Size: 740430 Color: 3
Size: 259476 Color: 4

Bin 2222: 96 of cap free
Amount of items: 2
Items: 
Size: 633349 Color: 1
Size: 366556 Color: 2

Bin 2223: 96 of cap free
Amount of items: 2
Items: 
Size: 654899 Color: 4
Size: 345006 Color: 3

Bin 2224: 96 of cap free
Amount of items: 2
Items: 
Size: 699826 Color: 3
Size: 300079 Color: 2

Bin 2225: 96 of cap free
Amount of items: 2
Items: 
Size: 776731 Color: 4
Size: 223174 Color: 1

Bin 2226: 97 of cap free
Amount of items: 2
Items: 
Size: 550222 Color: 2
Size: 449682 Color: 4

Bin 2227: 97 of cap free
Amount of items: 2
Items: 
Size: 571492 Color: 2
Size: 428412 Color: 0

Bin 2228: 97 of cap free
Amount of items: 2
Items: 
Size: 594919 Color: 2
Size: 404985 Color: 0

Bin 2229: 97 of cap free
Amount of items: 2
Items: 
Size: 620645 Color: 0
Size: 379259 Color: 4

Bin 2230: 98 of cap free
Amount of items: 2
Items: 
Size: 589177 Color: 2
Size: 410726 Color: 3

Bin 2231: 98 of cap free
Amount of items: 2
Items: 
Size: 592497 Color: 3
Size: 407406 Color: 0

Bin 2232: 98 of cap free
Amount of items: 2
Items: 
Size: 616967 Color: 0
Size: 382936 Color: 2

Bin 2233: 98 of cap free
Amount of items: 2
Items: 
Size: 623266 Color: 0
Size: 376637 Color: 4

Bin 2234: 98 of cap free
Amount of items: 2
Items: 
Size: 646769 Color: 3
Size: 353134 Color: 4

Bin 2235: 98 of cap free
Amount of items: 2
Items: 
Size: 707843 Color: 3
Size: 292060 Color: 2

Bin 2236: 99 of cap free
Amount of items: 2
Items: 
Size: 557411 Color: 0
Size: 442491 Color: 4

Bin 2237: 99 of cap free
Amount of items: 2
Items: 
Size: 569601 Color: 4
Size: 430301 Color: 1

Bin 2238: 99 of cap free
Amount of items: 2
Items: 
Size: 590062 Color: 0
Size: 409840 Color: 4

Bin 2239: 99 of cap free
Amount of items: 2
Items: 
Size: 627287 Color: 2
Size: 372615 Color: 3

Bin 2240: 99 of cap free
Amount of items: 2
Items: 
Size: 681500 Color: 3
Size: 318402 Color: 4

Bin 2241: 99 of cap free
Amount of items: 2
Items: 
Size: 691178 Color: 0
Size: 308724 Color: 1

Bin 2242: 99 of cap free
Amount of items: 2
Items: 
Size: 694030 Color: 3
Size: 305872 Color: 4

Bin 2243: 99 of cap free
Amount of items: 2
Items: 
Size: 697777 Color: 0
Size: 302125 Color: 4

Bin 2244: 99 of cap free
Amount of items: 2
Items: 
Size: 700866 Color: 2
Size: 299036 Color: 4

Bin 2245: 99 of cap free
Amount of items: 2
Items: 
Size: 703213 Color: 2
Size: 296689 Color: 4

Bin 2246: 99 of cap free
Amount of items: 2
Items: 
Size: 729937 Color: 0
Size: 269965 Color: 4

Bin 2247: 99 of cap free
Amount of items: 2
Items: 
Size: 732854 Color: 3
Size: 267048 Color: 4

Bin 2248: 99 of cap free
Amount of items: 2
Items: 
Size: 767976 Color: 0
Size: 231926 Color: 3

Bin 2249: 99 of cap free
Amount of items: 2
Items: 
Size: 774533 Color: 2
Size: 225369 Color: 3

Bin 2250: 99 of cap free
Amount of items: 2
Items: 
Size: 780765 Color: 2
Size: 219137 Color: 0

Bin 2251: 100 of cap free
Amount of items: 2
Items: 
Size: 503892 Color: 4
Size: 496009 Color: 2

Bin 2252: 100 of cap free
Amount of items: 2
Items: 
Size: 534763 Color: 0
Size: 465138 Color: 4

Bin 2253: 100 of cap free
Amount of items: 2
Items: 
Size: 555959 Color: 3
Size: 443942 Color: 1

Bin 2254: 100 of cap free
Amount of items: 2
Items: 
Size: 571830 Color: 3
Size: 428071 Color: 4

Bin 2255: 100 of cap free
Amount of items: 2
Items: 
Size: 623500 Color: 3
Size: 376401 Color: 0

Bin 2256: 100 of cap free
Amount of items: 2
Items: 
Size: 686647 Color: 0
Size: 313254 Color: 3

Bin 2257: 100 of cap free
Amount of items: 2
Items: 
Size: 709448 Color: 3
Size: 290453 Color: 0

Bin 2258: 100 of cap free
Amount of items: 2
Items: 
Size: 758144 Color: 1
Size: 241757 Color: 4

Bin 2259: 100 of cap free
Amount of items: 2
Items: 
Size: 777861 Color: 4
Size: 222040 Color: 2

Bin 2260: 101 of cap free
Amount of items: 2
Items: 
Size: 639115 Color: 1
Size: 360785 Color: 0

Bin 2261: 101 of cap free
Amount of items: 2
Items: 
Size: 665793 Color: 3
Size: 334107 Color: 2

Bin 2262: 101 of cap free
Amount of items: 2
Items: 
Size: 678319 Color: 0
Size: 321581 Color: 2

Bin 2263: 101 of cap free
Amount of items: 2
Items: 
Size: 765579 Color: 1
Size: 234321 Color: 4

Bin 2264: 102 of cap free
Amount of items: 2
Items: 
Size: 519974 Color: 0
Size: 479925 Color: 4

Bin 2265: 102 of cap free
Amount of items: 2
Items: 
Size: 529050 Color: 1
Size: 470849 Color: 0

Bin 2266: 102 of cap free
Amount of items: 2
Items: 
Size: 530398 Color: 3
Size: 469501 Color: 1

Bin 2267: 102 of cap free
Amount of items: 2
Items: 
Size: 532389 Color: 1
Size: 467510 Color: 3

Bin 2268: 102 of cap free
Amount of items: 2
Items: 
Size: 554689 Color: 2
Size: 445210 Color: 4

Bin 2269: 102 of cap free
Amount of items: 2
Items: 
Size: 605224 Color: 1
Size: 394675 Color: 2

Bin 2270: 102 of cap free
Amount of items: 2
Items: 
Size: 623651 Color: 1
Size: 376248 Color: 0

Bin 2271: 102 of cap free
Amount of items: 2
Items: 
Size: 626182 Color: 0
Size: 373717 Color: 2

Bin 2272: 102 of cap free
Amount of items: 2
Items: 
Size: 653473 Color: 4
Size: 346426 Color: 2

Bin 2273: 102 of cap free
Amount of items: 2
Items: 
Size: 679545 Color: 4
Size: 320354 Color: 0

Bin 2274: 103 of cap free
Amount of items: 2
Items: 
Size: 536085 Color: 0
Size: 463813 Color: 2

Bin 2275: 103 of cap free
Amount of items: 2
Items: 
Size: 586092 Color: 2
Size: 413806 Color: 1

Bin 2276: 103 of cap free
Amount of items: 2
Items: 
Size: 598715 Color: 2
Size: 401183 Color: 4

Bin 2277: 103 of cap free
Amount of items: 2
Items: 
Size: 662370 Color: 0
Size: 337528 Color: 1

Bin 2278: 103 of cap free
Amount of items: 2
Items: 
Size: 667002 Color: 1
Size: 332896 Color: 0

Bin 2279: 103 of cap free
Amount of items: 2
Items: 
Size: 775712 Color: 2
Size: 224186 Color: 3

Bin 2280: 104 of cap free
Amount of items: 2
Items: 
Size: 585258 Color: 2
Size: 414639 Color: 4

Bin 2281: 104 of cap free
Amount of items: 2
Items: 
Size: 599023 Color: 4
Size: 400874 Color: 0

Bin 2282: 104 of cap free
Amount of items: 2
Items: 
Size: 630239 Color: 2
Size: 369658 Color: 3

Bin 2283: 104 of cap free
Amount of items: 2
Items: 
Size: 634428 Color: 2
Size: 365469 Color: 1

Bin 2284: 104 of cap free
Amount of items: 2
Items: 
Size: 729859 Color: 4
Size: 270038 Color: 0

Bin 2285: 104 of cap free
Amount of items: 2
Items: 
Size: 741152 Color: 3
Size: 258745 Color: 2

Bin 2286: 104 of cap free
Amount of items: 2
Items: 
Size: 748494 Color: 2
Size: 251403 Color: 0

Bin 2287: 104 of cap free
Amount of items: 2
Items: 
Size: 759214 Color: 0
Size: 240683 Color: 2

Bin 2288: 104 of cap free
Amount of items: 2
Items: 
Size: 778140 Color: 3
Size: 221757 Color: 1

Bin 2289: 105 of cap free
Amount of items: 2
Items: 
Size: 512658 Color: 4
Size: 487238 Color: 2

Bin 2290: 105 of cap free
Amount of items: 2
Items: 
Size: 543590 Color: 1
Size: 456306 Color: 3

Bin 2291: 105 of cap free
Amount of items: 2
Items: 
Size: 544918 Color: 3
Size: 454978 Color: 0

Bin 2292: 105 of cap free
Amount of items: 2
Items: 
Size: 583532 Color: 1
Size: 416364 Color: 2

Bin 2293: 105 of cap free
Amount of items: 2
Items: 
Size: 600618 Color: 1
Size: 399278 Color: 0

Bin 2294: 105 of cap free
Amount of items: 2
Items: 
Size: 658192 Color: 2
Size: 341704 Color: 1

Bin 2295: 105 of cap free
Amount of items: 2
Items: 
Size: 660748 Color: 4
Size: 339148 Color: 2

Bin 2296: 105 of cap free
Amount of items: 2
Items: 
Size: 786511 Color: 0
Size: 213385 Color: 1

Bin 2297: 106 of cap free
Amount of items: 9
Items: 
Size: 112102 Color: 0
Size: 112088 Color: 0
Size: 111933 Color: 3
Size: 111932 Color: 2
Size: 111860 Color: 0
Size: 111846 Color: 0
Size: 111715 Color: 1
Size: 111631 Color: 3
Size: 104788 Color: 3

Bin 2298: 106 of cap free
Amount of items: 2
Items: 
Size: 513801 Color: 1
Size: 486094 Color: 0

Bin 2299: 106 of cap free
Amount of items: 2
Items: 
Size: 535598 Color: 1
Size: 464297 Color: 4

Bin 2300: 106 of cap free
Amount of items: 2
Items: 
Size: 540124 Color: 4
Size: 459771 Color: 3

Bin 2301: 106 of cap free
Amount of items: 2
Items: 
Size: 561607 Color: 3
Size: 438288 Color: 0

Bin 2302: 106 of cap free
Amount of items: 2
Items: 
Size: 563947 Color: 3
Size: 435948 Color: 0

Bin 2303: 106 of cap free
Amount of items: 2
Items: 
Size: 583041 Color: 1
Size: 416854 Color: 2

Bin 2304: 106 of cap free
Amount of items: 2
Items: 
Size: 617107 Color: 4
Size: 382788 Color: 1

Bin 2305: 106 of cap free
Amount of items: 2
Items: 
Size: 637736 Color: 4
Size: 362159 Color: 3

Bin 2306: 106 of cap free
Amount of items: 2
Items: 
Size: 674607 Color: 3
Size: 325288 Color: 2

Bin 2307: 106 of cap free
Amount of items: 2
Items: 
Size: 684870 Color: 3
Size: 315025 Color: 1

Bin 2308: 106 of cap free
Amount of items: 2
Items: 
Size: 695275 Color: 2
Size: 304620 Color: 3

Bin 2309: 106 of cap free
Amount of items: 2
Items: 
Size: 697246 Color: 3
Size: 302649 Color: 2

Bin 2310: 106 of cap free
Amount of items: 2
Items: 
Size: 782795 Color: 0
Size: 217100 Color: 2

Bin 2311: 107 of cap free
Amount of items: 6
Items: 
Size: 169185 Color: 3
Size: 169153 Color: 0
Size: 169082 Color: 2
Size: 169077 Color: 4
Size: 168974 Color: 4
Size: 154423 Color: 2

Bin 2312: 107 of cap free
Amount of items: 2
Items: 
Size: 506194 Color: 0
Size: 493700 Color: 4

Bin 2313: 107 of cap free
Amount of items: 2
Items: 
Size: 590974 Color: 2
Size: 408920 Color: 0

Bin 2314: 107 of cap free
Amount of items: 2
Items: 
Size: 664555 Color: 3
Size: 335339 Color: 4

Bin 2315: 107 of cap free
Amount of items: 2
Items: 
Size: 714235 Color: 0
Size: 285659 Color: 3

Bin 2316: 107 of cap free
Amount of items: 2
Items: 
Size: 745671 Color: 2
Size: 254223 Color: 4

Bin 2317: 107 of cap free
Amount of items: 2
Items: 
Size: 749822 Color: 0
Size: 250072 Color: 4

Bin 2318: 107 of cap free
Amount of items: 2
Items: 
Size: 755295 Color: 3
Size: 244599 Color: 1

Bin 2319: 107 of cap free
Amount of items: 2
Items: 
Size: 768355 Color: 4
Size: 231539 Color: 3

Bin 2320: 107 of cap free
Amount of items: 2
Items: 
Size: 784031 Color: 2
Size: 215863 Color: 4

Bin 2321: 108 of cap free
Amount of items: 2
Items: 
Size: 583403 Color: 0
Size: 416490 Color: 4

Bin 2322: 108 of cap free
Amount of items: 2
Items: 
Size: 619735 Color: 2
Size: 380158 Color: 4

Bin 2323: 108 of cap free
Amount of items: 2
Items: 
Size: 625595 Color: 1
Size: 374298 Color: 2

Bin 2324: 108 of cap free
Amount of items: 2
Items: 
Size: 672248 Color: 2
Size: 327645 Color: 3

Bin 2325: 108 of cap free
Amount of items: 2
Items: 
Size: 717483 Color: 4
Size: 282410 Color: 3

Bin 2326: 108 of cap free
Amount of items: 2
Items: 
Size: 724922 Color: 3
Size: 274971 Color: 2

Bin 2327: 109 of cap free
Amount of items: 8
Items: 
Size: 126910 Color: 4
Size: 126891 Color: 3
Size: 126886 Color: 0
Size: 126710 Color: 2
Size: 126612 Color: 3
Size: 126608 Color: 1
Size: 126595 Color: 4
Size: 112680 Color: 2

Bin 2328: 109 of cap free
Amount of items: 6
Items: 
Size: 174552 Color: 0
Size: 174518 Color: 2
Size: 174510 Color: 1
Size: 174415 Color: 1
Size: 173971 Color: 4
Size: 127926 Color: 4

Bin 2329: 109 of cap free
Amount of items: 2
Items: 
Size: 504863 Color: 1
Size: 495029 Color: 0

Bin 2330: 109 of cap free
Amount of items: 2
Items: 
Size: 525813 Color: 3
Size: 474079 Color: 0

Bin 2331: 109 of cap free
Amount of items: 2
Items: 
Size: 553923 Color: 4
Size: 445969 Color: 3

Bin 2332: 109 of cap free
Amount of items: 2
Items: 
Size: 568448 Color: 3
Size: 431444 Color: 0

Bin 2333: 109 of cap free
Amount of items: 2
Items: 
Size: 624118 Color: 4
Size: 375774 Color: 0

Bin 2334: 109 of cap free
Amount of items: 2
Items: 
Size: 629528 Color: 0
Size: 370364 Color: 1

Bin 2335: 109 of cap free
Amount of items: 2
Items: 
Size: 698237 Color: 1
Size: 301655 Color: 3

Bin 2336: 109 of cap free
Amount of items: 2
Items: 
Size: 773803 Color: 0
Size: 226089 Color: 3

Bin 2337: 109 of cap free
Amount of items: 2
Items: 
Size: 794509 Color: 0
Size: 205383 Color: 2

Bin 2338: 110 of cap free
Amount of items: 4
Items: 
Size: 514813 Color: 4
Size: 189817 Color: 3
Size: 189711 Color: 0
Size: 105550 Color: 0

Bin 2339: 110 of cap free
Amount of items: 2
Items: 
Size: 585735 Color: 2
Size: 414156 Color: 1

Bin 2340: 110 of cap free
Amount of items: 2
Items: 
Size: 620738 Color: 4
Size: 379153 Color: 3

Bin 2341: 110 of cap free
Amount of items: 2
Items: 
Size: 728702 Color: 4
Size: 271189 Color: 1

Bin 2342: 110 of cap free
Amount of items: 2
Items: 
Size: 780956 Color: 0
Size: 218935 Color: 2

Bin 2343: 111 of cap free
Amount of items: 2
Items: 
Size: 506681 Color: 3
Size: 493209 Color: 1

Bin 2344: 111 of cap free
Amount of items: 2
Items: 
Size: 537988 Color: 2
Size: 461902 Color: 0

Bin 2345: 111 of cap free
Amount of items: 2
Items: 
Size: 544340 Color: 0
Size: 455550 Color: 1

Bin 2346: 111 of cap free
Amount of items: 2
Items: 
Size: 574821 Color: 3
Size: 425069 Color: 0

Bin 2347: 111 of cap free
Amount of items: 2
Items: 
Size: 647628 Color: 2
Size: 352262 Color: 1

Bin 2348: 111 of cap free
Amount of items: 2
Items: 
Size: 731521 Color: 3
Size: 268369 Color: 1

Bin 2349: 111 of cap free
Amount of items: 2
Items: 
Size: 769644 Color: 2
Size: 230246 Color: 0

Bin 2350: 112 of cap free
Amount of items: 2
Items: 
Size: 526562 Color: 4
Size: 473327 Color: 2

Bin 2351: 112 of cap free
Amount of items: 2
Items: 
Size: 589811 Color: 2
Size: 410078 Color: 4

Bin 2352: 112 of cap free
Amount of items: 2
Items: 
Size: 593297 Color: 3
Size: 406592 Color: 4

Bin 2353: 112 of cap free
Amount of items: 2
Items: 
Size: 597008 Color: 0
Size: 402881 Color: 4

Bin 2354: 112 of cap free
Amount of items: 2
Items: 
Size: 612836 Color: 4
Size: 387053 Color: 2

Bin 2355: 112 of cap free
Amount of items: 2
Items: 
Size: 636637 Color: 1
Size: 363252 Color: 0

Bin 2356: 112 of cap free
Amount of items: 2
Items: 
Size: 651348 Color: 2
Size: 348541 Color: 1

Bin 2357: 112 of cap free
Amount of items: 2
Items: 
Size: 666230 Color: 2
Size: 333659 Color: 1

Bin 2358: 112 of cap free
Amount of items: 2
Items: 
Size: 675197 Color: 1
Size: 324692 Color: 2

Bin 2359: 112 of cap free
Amount of items: 2
Items: 
Size: 685001 Color: 1
Size: 314888 Color: 0

Bin 2360: 112 of cap free
Amount of items: 2
Items: 
Size: 686251 Color: 3
Size: 313638 Color: 0

Bin 2361: 112 of cap free
Amount of items: 2
Items: 
Size: 744143 Color: 1
Size: 255746 Color: 0

Bin 2362: 113 of cap free
Amount of items: 2
Items: 
Size: 505926 Color: 3
Size: 493962 Color: 2

Bin 2363: 113 of cap free
Amount of items: 2
Items: 
Size: 516113 Color: 4
Size: 483775 Color: 1

Bin 2364: 113 of cap free
Amount of items: 2
Items: 
Size: 535810 Color: 3
Size: 464078 Color: 2

Bin 2365: 113 of cap free
Amount of items: 2
Items: 
Size: 580810 Color: 3
Size: 419078 Color: 1

Bin 2366: 113 of cap free
Amount of items: 2
Items: 
Size: 597173 Color: 1
Size: 402715 Color: 2

Bin 2367: 113 of cap free
Amount of items: 2
Items: 
Size: 656549 Color: 0
Size: 343339 Color: 1

Bin 2368: 113 of cap free
Amount of items: 2
Items: 
Size: 682736 Color: 4
Size: 317152 Color: 0

Bin 2369: 113 of cap free
Amount of items: 2
Items: 
Size: 780278 Color: 1
Size: 219610 Color: 0

Bin 2370: 114 of cap free
Amount of items: 2
Items: 
Size: 594190 Color: 0
Size: 405697 Color: 3

Bin 2371: 114 of cap free
Amount of items: 2
Items: 
Size: 631445 Color: 3
Size: 368442 Color: 1

Bin 2372: 114 of cap free
Amount of items: 2
Items: 
Size: 642766 Color: 2
Size: 357121 Color: 1

Bin 2373: 114 of cap free
Amount of items: 2
Items: 
Size: 686235 Color: 4
Size: 313652 Color: 3

Bin 2374: 114 of cap free
Amount of items: 2
Items: 
Size: 726484 Color: 0
Size: 273403 Color: 4

Bin 2375: 114 of cap free
Amount of items: 2
Items: 
Size: 738534 Color: 1
Size: 261353 Color: 3

Bin 2376: 114 of cap free
Amount of items: 2
Items: 
Size: 783855 Color: 4
Size: 216032 Color: 0

Bin 2377: 115 of cap free
Amount of items: 2
Items: 
Size: 611322 Color: 1
Size: 388564 Color: 3

Bin 2378: 115 of cap free
Amount of items: 2
Items: 
Size: 696982 Color: 3
Size: 302904 Color: 4

Bin 2379: 115 of cap free
Amount of items: 2
Items: 
Size: 793822 Color: 0
Size: 206064 Color: 3

Bin 2380: 116 of cap free
Amount of items: 2
Items: 
Size: 553887 Color: 1
Size: 445998 Color: 4

Bin 2381: 116 of cap free
Amount of items: 2
Items: 
Size: 556446 Color: 3
Size: 443439 Color: 1

Bin 2382: 116 of cap free
Amount of items: 2
Items: 
Size: 608280 Color: 4
Size: 391605 Color: 3

Bin 2383: 116 of cap free
Amount of items: 2
Items: 
Size: 641333 Color: 2
Size: 358552 Color: 0

Bin 2384: 116 of cap free
Amount of items: 2
Items: 
Size: 644500 Color: 2
Size: 355385 Color: 1

Bin 2385: 116 of cap free
Amount of items: 2
Items: 
Size: 691446 Color: 2
Size: 308439 Color: 4

Bin 2386: 116 of cap free
Amount of items: 2
Items: 
Size: 787093 Color: 4
Size: 212792 Color: 2

Bin 2387: 117 of cap free
Amount of items: 2
Items: 
Size: 538085 Color: 0
Size: 461799 Color: 4

Bin 2388: 117 of cap free
Amount of items: 2
Items: 
Size: 544541 Color: 0
Size: 455343 Color: 3

Bin 2389: 117 of cap free
Amount of items: 2
Items: 
Size: 567215 Color: 3
Size: 432669 Color: 0

Bin 2390: 117 of cap free
Amount of items: 2
Items: 
Size: 599004 Color: 1
Size: 400880 Color: 4

Bin 2391: 117 of cap free
Amount of items: 2
Items: 
Size: 602531 Color: 3
Size: 397353 Color: 1

Bin 2392: 117 of cap free
Amount of items: 2
Items: 
Size: 632585 Color: 1
Size: 367299 Color: 0

Bin 2393: 117 of cap free
Amount of items: 2
Items: 
Size: 651978 Color: 1
Size: 347906 Color: 2

Bin 2394: 117 of cap free
Amount of items: 2
Items: 
Size: 656751 Color: 4
Size: 343133 Color: 2

Bin 2395: 117 of cap free
Amount of items: 2
Items: 
Size: 704475 Color: 2
Size: 295409 Color: 3

Bin 2396: 117 of cap free
Amount of items: 2
Items: 
Size: 707842 Color: 1
Size: 292042 Color: 4

Bin 2397: 117 of cap free
Amount of items: 2
Items: 
Size: 773762 Color: 4
Size: 226122 Color: 0

Bin 2398: 118 of cap free
Amount of items: 6
Items: 
Size: 171485 Color: 0
Size: 171450 Color: 1
Size: 171433 Color: 4
Size: 171386 Color: 2
Size: 171385 Color: 1
Size: 142744 Color: 4

Bin 2399: 118 of cap free
Amount of items: 2
Items: 
Size: 572071 Color: 2
Size: 427812 Color: 4

Bin 2400: 118 of cap free
Amount of items: 2
Items: 
Size: 683312 Color: 4
Size: 316571 Color: 3

Bin 2401: 118 of cap free
Amount of items: 2
Items: 
Size: 684711 Color: 1
Size: 315172 Color: 0

Bin 2402: 118 of cap free
Amount of items: 2
Items: 
Size: 755274 Color: 1
Size: 244609 Color: 3

Bin 2403: 119 of cap free
Amount of items: 2
Items: 
Size: 550785 Color: 2
Size: 449097 Color: 1

Bin 2404: 119 of cap free
Amount of items: 2
Items: 
Size: 581644 Color: 1
Size: 418238 Color: 3

Bin 2405: 119 of cap free
Amount of items: 2
Items: 
Size: 631087 Color: 1
Size: 368795 Color: 0

Bin 2406: 119 of cap free
Amount of items: 2
Items: 
Size: 665355 Color: 4
Size: 334527 Color: 2

Bin 2407: 119 of cap free
Amount of items: 2
Items: 
Size: 708204 Color: 4
Size: 291678 Color: 0

Bin 2408: 119 of cap free
Amount of items: 2
Items: 
Size: 733115 Color: 3
Size: 266767 Color: 4

Bin 2409: 119 of cap free
Amount of items: 2
Items: 
Size: 797229 Color: 4
Size: 202653 Color: 0

Bin 2410: 120 of cap free
Amount of items: 2
Items: 
Size: 555762 Color: 0
Size: 444119 Color: 3

Bin 2411: 120 of cap free
Amount of items: 2
Items: 
Size: 596591 Color: 0
Size: 403290 Color: 3

Bin 2412: 120 of cap free
Amount of items: 2
Items: 
Size: 691629 Color: 1
Size: 308252 Color: 3

Bin 2413: 120 of cap free
Amount of items: 2
Items: 
Size: 726935 Color: 3
Size: 272946 Color: 0

Bin 2414: 120 of cap free
Amount of items: 2
Items: 
Size: 741758 Color: 2
Size: 258123 Color: 0

Bin 2415: 120 of cap free
Amount of items: 2
Items: 
Size: 742812 Color: 1
Size: 257069 Color: 0

Bin 2416: 121 of cap free
Amount of items: 6
Items: 
Size: 178756 Color: 4
Size: 178727 Color: 4
Size: 178670 Color: 2
Size: 178722 Color: 4
Size: 178636 Color: 2
Size: 106369 Color: 0

Bin 2417: 121 of cap free
Amount of items: 2
Items: 
Size: 761175 Color: 0
Size: 238705 Color: 3

Bin 2418: 121 of cap free
Amount of items: 2
Items: 
Size: 783645 Color: 4
Size: 216235 Color: 1

Bin 2419: 122 of cap free
Amount of items: 2
Items: 
Size: 505518 Color: 0
Size: 494361 Color: 4

Bin 2420: 122 of cap free
Amount of items: 2
Items: 
Size: 534304 Color: 2
Size: 465575 Color: 4

Bin 2421: 122 of cap free
Amount of items: 2
Items: 
Size: 552979 Color: 1
Size: 446900 Color: 0

Bin 2422: 122 of cap free
Amount of items: 2
Items: 
Size: 602012 Color: 4
Size: 397867 Color: 0

Bin 2423: 122 of cap free
Amount of items: 2
Items: 
Size: 613688 Color: 1
Size: 386191 Color: 2

Bin 2424: 122 of cap free
Amount of items: 2
Items: 
Size: 652082 Color: 2
Size: 347797 Color: 4

Bin 2425: 122 of cap free
Amount of items: 2
Items: 
Size: 694459 Color: 1
Size: 305420 Color: 3

Bin 2426: 122 of cap free
Amount of items: 2
Items: 
Size: 698230 Color: 0
Size: 301649 Color: 2

Bin 2427: 122 of cap free
Amount of items: 2
Items: 
Size: 749506 Color: 2
Size: 250373 Color: 0

Bin 2428: 123 of cap free
Amount of items: 2
Items: 
Size: 591965 Color: 1
Size: 407913 Color: 0

Bin 2429: 123 of cap free
Amount of items: 2
Items: 
Size: 746686 Color: 4
Size: 253192 Color: 3

Bin 2430: 123 of cap free
Amount of items: 2
Items: 
Size: 747553 Color: 3
Size: 252325 Color: 4

Bin 2431: 123 of cap free
Amount of items: 2
Items: 
Size: 748892 Color: 4
Size: 250986 Color: 3

Bin 2432: 123 of cap free
Amount of items: 2
Items: 
Size: 789026 Color: 0
Size: 210852 Color: 4

Bin 2433: 124 of cap free
Amount of items: 2
Items: 
Size: 513211 Color: 1
Size: 486666 Color: 2

Bin 2434: 124 of cap free
Amount of items: 2
Items: 
Size: 533568 Color: 2
Size: 466309 Color: 0

Bin 2435: 124 of cap free
Amount of items: 2
Items: 
Size: 551968 Color: 1
Size: 447909 Color: 4

Bin 2436: 124 of cap free
Amount of items: 2
Items: 
Size: 584328 Color: 3
Size: 415549 Color: 1

Bin 2437: 124 of cap free
Amount of items: 2
Items: 
Size: 636006 Color: 1
Size: 363871 Color: 4

Bin 2438: 124 of cap free
Amount of items: 2
Items: 
Size: 707842 Color: 4
Size: 292035 Color: 2

Bin 2439: 124 of cap free
Amount of items: 2
Items: 
Size: 744326 Color: 1
Size: 255551 Color: 2

Bin 2440: 124 of cap free
Amount of items: 2
Items: 
Size: 753837 Color: 2
Size: 246040 Color: 4

Bin 2441: 125 of cap free
Amount of items: 2
Items: 
Size: 505251 Color: 4
Size: 494625 Color: 0

Bin 2442: 125 of cap free
Amount of items: 2
Items: 
Size: 513785 Color: 4
Size: 486091 Color: 2

Bin 2443: 125 of cap free
Amount of items: 4
Items: 
Size: 514046 Color: 1
Size: 189374 Color: 2
Size: 188936 Color: 4
Size: 107520 Color: 3

Bin 2444: 125 of cap free
Amount of items: 2
Items: 
Size: 607296 Color: 3
Size: 392580 Color: 0

Bin 2445: 125 of cap free
Amount of items: 2
Items: 
Size: 608578 Color: 1
Size: 391298 Color: 4

Bin 2446: 125 of cap free
Amount of items: 2
Items: 
Size: 684999 Color: 4
Size: 314877 Color: 0

Bin 2447: 125 of cap free
Amount of items: 2
Items: 
Size: 758406 Color: 2
Size: 241470 Color: 0

Bin 2448: 126 of cap free
Amount of items: 6
Items: 
Size: 169660 Color: 2
Size: 169591 Color: 2
Size: 169530 Color: 0
Size: 169504 Color: 1
Size: 169335 Color: 2
Size: 152255 Color: 0

Bin 2449: 126 of cap free
Amount of items: 2
Items: 
Size: 501522 Color: 4
Size: 498353 Color: 1

Bin 2450: 126 of cap free
Amount of items: 2
Items: 
Size: 565384 Color: 0
Size: 434491 Color: 3

Bin 2451: 126 of cap free
Amount of items: 2
Items: 
Size: 569129 Color: 4
Size: 430746 Color: 3

Bin 2452: 126 of cap free
Amount of items: 2
Items: 
Size: 652465 Color: 0
Size: 347410 Color: 2

Bin 2453: 126 of cap free
Amount of items: 2
Items: 
Size: 699023 Color: 1
Size: 300852 Color: 0

Bin 2454: 126 of cap free
Amount of items: 2
Items: 
Size: 713703 Color: 0
Size: 286172 Color: 3

Bin 2455: 127 of cap free
Amount of items: 6
Items: 
Size: 177225 Color: 3
Size: 177034 Color: 4
Size: 176932 Color: 0
Size: 176929 Color: 2
Size: 176732 Color: 3
Size: 115022 Color: 3

Bin 2456: 127 of cap free
Amount of items: 2
Items: 
Size: 521977 Color: 0
Size: 477897 Color: 2

Bin 2457: 127 of cap free
Amount of items: 2
Items: 
Size: 523272 Color: 2
Size: 476602 Color: 0

Bin 2458: 127 of cap free
Amount of items: 2
Items: 
Size: 711167 Color: 4
Size: 288707 Color: 1

Bin 2459: 127 of cap free
Amount of items: 2
Items: 
Size: 711892 Color: 2
Size: 287982 Color: 3

Bin 2460: 127 of cap free
Amount of items: 2
Items: 
Size: 714287 Color: 3
Size: 285587 Color: 2

Bin 2461: 127 of cap free
Amount of items: 2
Items: 
Size: 721912 Color: 4
Size: 277962 Color: 1

Bin 2462: 127 of cap free
Amount of items: 2
Items: 
Size: 738700 Color: 2
Size: 261174 Color: 4

Bin 2463: 127 of cap free
Amount of items: 2
Items: 
Size: 738792 Color: 4
Size: 261082 Color: 1

Bin 2464: 127 of cap free
Amount of items: 2
Items: 
Size: 748198 Color: 4
Size: 251676 Color: 3

Bin 2465: 127 of cap free
Amount of items: 2
Items: 
Size: 753225 Color: 0
Size: 246649 Color: 3

Bin 2466: 127 of cap free
Amount of items: 2
Items: 
Size: 782309 Color: 3
Size: 217565 Color: 2

Bin 2467: 128 of cap free
Amount of items: 2
Items: 
Size: 632603 Color: 0
Size: 367270 Color: 4

Bin 2468: 128 of cap free
Amount of items: 2
Items: 
Size: 673045 Color: 0
Size: 326828 Color: 2

Bin 2469: 128 of cap free
Amount of items: 2
Items: 
Size: 682483 Color: 4
Size: 317390 Color: 2

Bin 2470: 128 of cap free
Amount of items: 2
Items: 
Size: 722975 Color: 2
Size: 276898 Color: 1

Bin 2471: 128 of cap free
Amount of items: 2
Items: 
Size: 724332 Color: 4
Size: 275541 Color: 1

Bin 2472: 129 of cap free
Amount of items: 2
Items: 
Size: 504849 Color: 1
Size: 495023 Color: 0

Bin 2473: 129 of cap free
Amount of items: 2
Items: 
Size: 568864 Color: 0
Size: 431008 Color: 3

Bin 2474: 129 of cap free
Amount of items: 2
Items: 
Size: 612687 Color: 2
Size: 387185 Color: 0

Bin 2475: 129 of cap free
Amount of items: 2
Items: 
Size: 647268 Color: 2
Size: 352604 Color: 1

Bin 2476: 129 of cap free
Amount of items: 2
Items: 
Size: 694003 Color: 1
Size: 305869 Color: 2

Bin 2477: 129 of cap free
Amount of items: 2
Items: 
Size: 744822 Color: 2
Size: 255050 Color: 0

Bin 2478: 130 of cap free
Amount of items: 2
Items: 
Size: 579361 Color: 0
Size: 420510 Color: 3

Bin 2479: 130 of cap free
Amount of items: 2
Items: 
Size: 621461 Color: 0
Size: 378410 Color: 4

Bin 2480: 130 of cap free
Amount of items: 2
Items: 
Size: 626317 Color: 2
Size: 373554 Color: 1

Bin 2481: 130 of cap free
Amount of items: 2
Items: 
Size: 665635 Color: 3
Size: 334236 Color: 0

Bin 2482: 130 of cap free
Amount of items: 2
Items: 
Size: 670816 Color: 2
Size: 329055 Color: 3

Bin 2483: 130 of cap free
Amount of items: 2
Items: 
Size: 713530 Color: 3
Size: 286341 Color: 2

Bin 2484: 130 of cap free
Amount of items: 2
Items: 
Size: 720073 Color: 2
Size: 279798 Color: 0

Bin 2485: 130 of cap free
Amount of items: 2
Items: 
Size: 764870 Color: 1
Size: 235001 Color: 2

Bin 2486: 130 of cap free
Amount of items: 2
Items: 
Size: 767582 Color: 4
Size: 232289 Color: 0

Bin 2487: 131 of cap free
Amount of items: 2
Items: 
Size: 521163 Color: 1
Size: 478707 Color: 4

Bin 2488: 131 of cap free
Amount of items: 2
Items: 
Size: 622604 Color: 3
Size: 377266 Color: 1

Bin 2489: 131 of cap free
Amount of items: 2
Items: 
Size: 685341 Color: 3
Size: 314529 Color: 0

Bin 2490: 131 of cap free
Amount of items: 2
Items: 
Size: 741982 Color: 2
Size: 257888 Color: 0

Bin 2491: 131 of cap free
Amount of items: 2
Items: 
Size: 751944 Color: 2
Size: 247926 Color: 0

Bin 2492: 132 of cap free
Amount of items: 7
Items: 
Size: 146596 Color: 3
Size: 146491 Color: 2
Size: 146471 Color: 2
Size: 146471 Color: 0
Size: 146404 Color: 1
Size: 146383 Color: 0
Size: 121053 Color: 3

Bin 2493: 132 of cap free
Amount of items: 2
Items: 
Size: 504629 Color: 0
Size: 495240 Color: 4

Bin 2494: 132 of cap free
Amount of items: 2
Items: 
Size: 508388 Color: 1
Size: 491481 Color: 0

Bin 2495: 132 of cap free
Amount of items: 2
Items: 
Size: 521012 Color: 0
Size: 478857 Color: 4

Bin 2496: 132 of cap free
Amount of items: 2
Items: 
Size: 564810 Color: 2
Size: 435059 Color: 0

Bin 2497: 132 of cap free
Amount of items: 2
Items: 
Size: 586606 Color: 0
Size: 413263 Color: 2

Bin 2498: 132 of cap free
Amount of items: 2
Items: 
Size: 588016 Color: 3
Size: 411853 Color: 0

Bin 2499: 132 of cap free
Amount of items: 2
Items: 
Size: 589579 Color: 3
Size: 410290 Color: 1

Bin 2500: 132 of cap free
Amount of items: 2
Items: 
Size: 610652 Color: 3
Size: 389217 Color: 1

Bin 2501: 132 of cap free
Amount of items: 2
Items: 
Size: 625248 Color: 3
Size: 374621 Color: 2

Bin 2502: 132 of cap free
Amount of items: 2
Items: 
Size: 651812 Color: 0
Size: 348057 Color: 1

Bin 2503: 132 of cap free
Amount of items: 2
Items: 
Size: 686541 Color: 1
Size: 313328 Color: 0

Bin 2504: 132 of cap free
Amount of items: 2
Items: 
Size: 696256 Color: 4
Size: 303613 Color: 3

Bin 2505: 132 of cap free
Amount of items: 2
Items: 
Size: 726931 Color: 3
Size: 272938 Color: 0

Bin 2506: 132 of cap free
Amount of items: 2
Items: 
Size: 748391 Color: 0
Size: 251478 Color: 2

Bin 2507: 133 of cap free
Amount of items: 2
Items: 
Size: 523277 Color: 0
Size: 476591 Color: 3

Bin 2508: 133 of cap free
Amount of items: 2
Items: 
Size: 547598 Color: 0
Size: 452270 Color: 3

Bin 2509: 133 of cap free
Amount of items: 2
Items: 
Size: 657419 Color: 4
Size: 342449 Color: 0

Bin 2510: 133 of cap free
Amount of items: 2
Items: 
Size: 657703 Color: 4
Size: 342165 Color: 1

Bin 2511: 133 of cap free
Amount of items: 2
Items: 
Size: 733329 Color: 4
Size: 266539 Color: 2

Bin 2512: 133 of cap free
Amount of items: 2
Items: 
Size: 781075 Color: 3
Size: 218793 Color: 4

Bin 2513: 134 of cap free
Amount of items: 2
Items: 
Size: 534100 Color: 1
Size: 465767 Color: 0

Bin 2514: 134 of cap free
Amount of items: 2
Items: 
Size: 534741 Color: 1
Size: 465126 Color: 2

Bin 2515: 134 of cap free
Amount of items: 2
Items: 
Size: 571793 Color: 4
Size: 428074 Color: 3

Bin 2516: 134 of cap free
Amount of items: 2
Items: 
Size: 626862 Color: 2
Size: 373005 Color: 0

Bin 2517: 134 of cap free
Amount of items: 2
Items: 
Size: 676838 Color: 1
Size: 323029 Color: 0

Bin 2518: 134 of cap free
Amount of items: 2
Items: 
Size: 698478 Color: 0
Size: 301389 Color: 2

Bin 2519: 134 of cap free
Amount of items: 2
Items: 
Size: 702049 Color: 4
Size: 297818 Color: 0

Bin 2520: 134 of cap free
Amount of items: 2
Items: 
Size: 730153 Color: 1
Size: 269714 Color: 3

Bin 2521: 134 of cap free
Amount of items: 2
Items: 
Size: 761370 Color: 3
Size: 238497 Color: 1

Bin 2522: 135 of cap free
Amount of items: 2
Items: 
Size: 584435 Color: 1
Size: 415431 Color: 3

Bin 2523: 135 of cap free
Amount of items: 2
Items: 
Size: 594352 Color: 3
Size: 405514 Color: 4

Bin 2524: 135 of cap free
Amount of items: 2
Items: 
Size: 647757 Color: 0
Size: 352109 Color: 4

Bin 2525: 135 of cap free
Amount of items: 2
Items: 
Size: 651065 Color: 1
Size: 348801 Color: 0

Bin 2526: 135 of cap free
Amount of items: 2
Items: 
Size: 776172 Color: 3
Size: 223694 Color: 0

Bin 2527: 136 of cap free
Amount of items: 6
Items: 
Size: 168610 Color: 4
Size: 168594 Color: 1
Size: 168471 Color: 1
Size: 168456 Color: 4
Size: 168431 Color: 0
Size: 157303 Color: 3

Bin 2528: 136 of cap free
Amount of items: 2
Items: 
Size: 501221 Color: 0
Size: 498644 Color: 3

Bin 2529: 136 of cap free
Amount of items: 2
Items: 
Size: 527751 Color: 0
Size: 472114 Color: 1

Bin 2530: 136 of cap free
Amount of items: 2
Items: 
Size: 592484 Color: 4
Size: 407381 Color: 0

Bin 2531: 136 of cap free
Amount of items: 2
Items: 
Size: 603860 Color: 2
Size: 396005 Color: 0

Bin 2532: 136 of cap free
Amount of items: 2
Items: 
Size: 614327 Color: 0
Size: 385538 Color: 4

Bin 2533: 136 of cap free
Amount of items: 2
Items: 
Size: 659278 Color: 3
Size: 340587 Color: 0

Bin 2534: 136 of cap free
Amount of items: 2
Items: 
Size: 679374 Color: 0
Size: 320491 Color: 1

Bin 2535: 136 of cap free
Amount of items: 2
Items: 
Size: 720091 Color: 0
Size: 279774 Color: 1

Bin 2536: 136 of cap free
Amount of items: 2
Items: 
Size: 764601 Color: 3
Size: 235264 Color: 0

Bin 2537: 137 of cap free
Amount of items: 2
Items: 
Size: 597414 Color: 2
Size: 402450 Color: 0

Bin 2538: 137 of cap free
Amount of items: 2
Items: 
Size: 606626 Color: 2
Size: 393238 Color: 0

Bin 2539: 137 of cap free
Amount of items: 2
Items: 
Size: 749811 Color: 0
Size: 250053 Color: 3

Bin 2540: 137 of cap free
Amount of items: 2
Items: 
Size: 768129 Color: 4
Size: 231735 Color: 0

Bin 2541: 138 of cap free
Amount of items: 2
Items: 
Size: 533289 Color: 3
Size: 466574 Color: 4

Bin 2542: 138 of cap free
Amount of items: 2
Items: 
Size: 654507 Color: 4
Size: 345356 Color: 0

Bin 2543: 138 of cap free
Amount of items: 2
Items: 
Size: 686914 Color: 0
Size: 312949 Color: 3

Bin 2544: 138 of cap free
Amount of items: 2
Items: 
Size: 695859 Color: 1
Size: 304004 Color: 3

Bin 2545: 138 of cap free
Amount of items: 2
Items: 
Size: 715030 Color: 1
Size: 284833 Color: 4

Bin 2546: 138 of cap free
Amount of items: 2
Items: 
Size: 746912 Color: 2
Size: 252951 Color: 1

Bin 2547: 139 of cap free
Amount of items: 2
Items: 
Size: 519057 Color: 4
Size: 480805 Color: 1

Bin 2548: 139 of cap free
Amount of items: 2
Items: 
Size: 525962 Color: 4
Size: 473900 Color: 2

Bin 2549: 139 of cap free
Amount of items: 2
Items: 
Size: 632954 Color: 3
Size: 366908 Color: 2

Bin 2550: 139 of cap free
Amount of items: 2
Items: 
Size: 633814 Color: 3
Size: 366048 Color: 2

Bin 2551: 139 of cap free
Amount of items: 2
Items: 
Size: 636377 Color: 4
Size: 363485 Color: 0

Bin 2552: 139 of cap free
Amount of items: 2
Items: 
Size: 687113 Color: 3
Size: 312749 Color: 4

Bin 2553: 139 of cap free
Amount of items: 2
Items: 
Size: 704935 Color: 4
Size: 294927 Color: 1

Bin 2554: 139 of cap free
Amount of items: 2
Items: 
Size: 745668 Color: 3
Size: 254194 Color: 4

Bin 2555: 140 of cap free
Amount of items: 2
Items: 
Size: 508596 Color: 2
Size: 491265 Color: 1

Bin 2556: 140 of cap free
Amount of items: 2
Items: 
Size: 622359 Color: 3
Size: 377502 Color: 1

Bin 2557: 140 of cap free
Amount of items: 2
Items: 
Size: 628342 Color: 1
Size: 371519 Color: 2

Bin 2558: 140 of cap free
Amount of items: 2
Items: 
Size: 704237 Color: 0
Size: 295624 Color: 2

Bin 2559: 140 of cap free
Amount of items: 2
Items: 
Size: 715256 Color: 3
Size: 284605 Color: 2

Bin 2560: 141 of cap free
Amount of items: 2
Items: 
Size: 500291 Color: 0
Size: 499569 Color: 2

Bin 2561: 141 of cap free
Amount of items: 2
Items: 
Size: 635645 Color: 2
Size: 364215 Color: 0

Bin 2562: 141 of cap free
Amount of items: 2
Items: 
Size: 671310 Color: 1
Size: 328550 Color: 3

Bin 2563: 141 of cap free
Amount of items: 2
Items: 
Size: 674572 Color: 0
Size: 325288 Color: 3

Bin 2564: 141 of cap free
Amount of items: 2
Items: 
Size: 703378 Color: 0
Size: 296482 Color: 1

Bin 2565: 141 of cap free
Amount of items: 2
Items: 
Size: 729681 Color: 2
Size: 270179 Color: 1

Bin 2566: 141 of cap free
Amount of items: 2
Items: 
Size: 751116 Color: 3
Size: 248744 Color: 2

Bin 2567: 141 of cap free
Amount of items: 2
Items: 
Size: 753213 Color: 2
Size: 246647 Color: 4

Bin 2568: 141 of cap free
Amount of items: 2
Items: 
Size: 754289 Color: 1
Size: 245571 Color: 3

Bin 2569: 141 of cap free
Amount of items: 2
Items: 
Size: 795723 Color: 2
Size: 204137 Color: 3

Bin 2570: 142 of cap free
Amount of items: 6
Items: 
Size: 178311 Color: 2
Size: 178161 Color: 2
Size: 177911 Color: 1
Size: 178032 Color: 2
Size: 177832 Color: 1
Size: 109612 Color: 0

Bin 2571: 142 of cap free
Amount of items: 2
Items: 
Size: 504091 Color: 3
Size: 495768 Color: 4

Bin 2572: 142 of cap free
Amount of items: 2
Items: 
Size: 544198 Color: 3
Size: 455661 Color: 0

Bin 2573: 142 of cap free
Amount of items: 2
Items: 
Size: 565204 Color: 0
Size: 434655 Color: 2

Bin 2574: 142 of cap free
Amount of items: 2
Items: 
Size: 578810 Color: 2
Size: 421049 Color: 3

Bin 2575: 142 of cap free
Amount of items: 2
Items: 
Size: 639116 Color: 0
Size: 360743 Color: 3

Bin 2576: 142 of cap free
Amount of items: 2
Items: 
Size: 690208 Color: 2
Size: 309651 Color: 0

Bin 2577: 142 of cap free
Amount of items: 2
Items: 
Size: 743444 Color: 0
Size: 256415 Color: 1

Bin 2578: 142 of cap free
Amount of items: 2
Items: 
Size: 768742 Color: 1
Size: 231117 Color: 3

Bin 2579: 142 of cap free
Amount of items: 2
Items: 
Size: 786499 Color: 0
Size: 213360 Color: 1

Bin 2580: 143 of cap free
Amount of items: 2
Items: 
Size: 570102 Color: 2
Size: 429756 Color: 0

Bin 2581: 143 of cap free
Amount of items: 2
Items: 
Size: 626583 Color: 0
Size: 373275 Color: 3

Bin 2582: 143 of cap free
Amount of items: 2
Items: 
Size: 645173 Color: 0
Size: 354685 Color: 1

Bin 2583: 143 of cap free
Amount of items: 2
Items: 
Size: 732533 Color: 0
Size: 267325 Color: 1

Bin 2584: 144 of cap free
Amount of items: 2
Items: 
Size: 612827 Color: 0
Size: 387030 Color: 2

Bin 2585: 144 of cap free
Amount of items: 2
Items: 
Size: 621940 Color: 0
Size: 377917 Color: 1

Bin 2586: 144 of cap free
Amount of items: 2
Items: 
Size: 628091 Color: 0
Size: 371766 Color: 1

Bin 2587: 144 of cap free
Amount of items: 2
Items: 
Size: 635048 Color: 0
Size: 364809 Color: 1

Bin 2588: 144 of cap free
Amount of items: 2
Items: 
Size: 650469 Color: 0
Size: 349388 Color: 4

Bin 2589: 144 of cap free
Amount of items: 2
Items: 
Size: 655222 Color: 1
Size: 344635 Color: 3

Bin 2590: 144 of cap free
Amount of items: 2
Items: 
Size: 746507 Color: 1
Size: 253350 Color: 0

Bin 2591: 145 of cap free
Amount of items: 2
Items: 
Size: 541919 Color: 3
Size: 457937 Color: 2

Bin 2592: 145 of cap free
Amount of items: 2
Items: 
Size: 565893 Color: 4
Size: 433963 Color: 3

Bin 2593: 145 of cap free
Amount of items: 2
Items: 
Size: 615352 Color: 3
Size: 384504 Color: 4

Bin 2594: 145 of cap free
Amount of items: 2
Items: 
Size: 627255 Color: 1
Size: 372601 Color: 4

Bin 2595: 145 of cap free
Amount of items: 2
Items: 
Size: 681061 Color: 2
Size: 318795 Color: 4

Bin 2596: 146 of cap free
Amount of items: 6
Items: 
Size: 172094 Color: 1
Size: 172054 Color: 2
Size: 172012 Color: 0
Size: 171920 Color: 3
Size: 171559 Color: 0
Size: 140216 Color: 2

Bin 2597: 146 of cap free
Amount of items: 2
Items: 
Size: 584776 Color: 1
Size: 415079 Color: 3

Bin 2598: 146 of cap free
Amount of items: 2
Items: 
Size: 612047 Color: 3
Size: 387808 Color: 0

Bin 2599: 146 of cap free
Amount of items: 2
Items: 
Size: 633314 Color: 0
Size: 366541 Color: 4

Bin 2600: 146 of cap free
Amount of items: 2
Items: 
Size: 707321 Color: 4
Size: 292534 Color: 0

Bin 2601: 146 of cap free
Amount of items: 2
Items: 
Size: 711406 Color: 3
Size: 288449 Color: 0

Bin 2602: 146 of cap free
Amount of items: 2
Items: 
Size: 718318 Color: 4
Size: 281537 Color: 1

Bin 2603: 146 of cap free
Amount of items: 2
Items: 
Size: 769189 Color: 1
Size: 230666 Color: 3

Bin 2604: 146 of cap free
Amount of items: 2
Items: 
Size: 794488 Color: 4
Size: 205367 Color: 3

Bin 2605: 147 of cap free
Amount of items: 2
Items: 
Size: 502822 Color: 3
Size: 497032 Color: 1

Bin 2606: 147 of cap free
Amount of items: 2
Items: 
Size: 562852 Color: 3
Size: 437002 Color: 1

Bin 2607: 147 of cap free
Amount of items: 2
Items: 
Size: 606229 Color: 2
Size: 393625 Color: 4

Bin 2608: 147 of cap free
Amount of items: 2
Items: 
Size: 679170 Color: 0
Size: 320684 Color: 4

Bin 2609: 147 of cap free
Amount of items: 2
Items: 
Size: 742788 Color: 0
Size: 257066 Color: 3

Bin 2610: 147 of cap free
Amount of items: 2
Items: 
Size: 749629 Color: 2
Size: 250225 Color: 3

Bin 2611: 147 of cap free
Amount of items: 2
Items: 
Size: 756124 Color: 2
Size: 243730 Color: 0

Bin 2612: 148 of cap free
Amount of items: 2
Items: 
Size: 512416 Color: 3
Size: 487437 Color: 0

Bin 2613: 148 of cap free
Amount of items: 2
Items: 
Size: 637939 Color: 3
Size: 361914 Color: 1

Bin 2614: 148 of cap free
Amount of items: 2
Items: 
Size: 651788 Color: 3
Size: 348065 Color: 0

Bin 2615: 148 of cap free
Amount of items: 2
Items: 
Size: 740377 Color: 4
Size: 259476 Color: 0

Bin 2616: 148 of cap free
Amount of items: 2
Items: 
Size: 794480 Color: 3
Size: 205373 Color: 4

Bin 2617: 149 of cap free
Amount of items: 2
Items: 
Size: 561093 Color: 3
Size: 438759 Color: 2

Bin 2618: 149 of cap free
Amount of items: 2
Items: 
Size: 591708 Color: 4
Size: 408144 Color: 3

Bin 2619: 149 of cap free
Amount of items: 2
Items: 
Size: 621296 Color: 3
Size: 378556 Color: 2

Bin 2620: 149 of cap free
Amount of items: 2
Items: 
Size: 627831 Color: 4
Size: 372021 Color: 2

Bin 2621: 149 of cap free
Amount of items: 2
Items: 
Size: 675653 Color: 3
Size: 324199 Color: 4

Bin 2622: 149 of cap free
Amount of items: 2
Items: 
Size: 683747 Color: 4
Size: 316105 Color: 2

Bin 2623: 150 of cap free
Amount of items: 2
Items: 
Size: 502093 Color: 1
Size: 497758 Color: 4

Bin 2624: 150 of cap free
Amount of items: 2
Items: 
Size: 653928 Color: 4
Size: 345923 Color: 3

Bin 2625: 150 of cap free
Amount of items: 2
Items: 
Size: 692231 Color: 4
Size: 307620 Color: 1

Bin 2626: 150 of cap free
Amount of items: 2
Items: 
Size: 695494 Color: 1
Size: 304357 Color: 4

Bin 2627: 150 of cap free
Amount of items: 2
Items: 
Size: 701867 Color: 3
Size: 297984 Color: 1

Bin 2628: 150 of cap free
Amount of items: 2
Items: 
Size: 728869 Color: 1
Size: 270982 Color: 0

Bin 2629: 150 of cap free
Amount of items: 2
Items: 
Size: 735481 Color: 0
Size: 264370 Color: 4

Bin 2630: 151 of cap free
Amount of items: 2
Items: 
Size: 511607 Color: 2
Size: 488243 Color: 3

Bin 2631: 151 of cap free
Amount of items: 2
Items: 
Size: 524730 Color: 0
Size: 475120 Color: 3

Bin 2632: 151 of cap free
Amount of items: 2
Items: 
Size: 551648 Color: 4
Size: 448202 Color: 3

Bin 2633: 151 of cap free
Amount of items: 2
Items: 
Size: 574381 Color: 2
Size: 425469 Color: 1

Bin 2634: 151 of cap free
Amount of items: 2
Items: 
Size: 591495 Color: 1
Size: 408355 Color: 3

Bin 2635: 151 of cap free
Amount of items: 2
Items: 
Size: 600158 Color: 2
Size: 399692 Color: 3

Bin 2636: 151 of cap free
Amount of items: 2
Items: 
Size: 604875 Color: 2
Size: 394975 Color: 0

Bin 2637: 151 of cap free
Amount of items: 2
Items: 
Size: 643761 Color: 2
Size: 356089 Color: 3

Bin 2638: 151 of cap free
Amount of items: 2
Items: 
Size: 689317 Color: 0
Size: 310533 Color: 4

Bin 2639: 151 of cap free
Amount of items: 2
Items: 
Size: 691790 Color: 2
Size: 308060 Color: 3

Bin 2640: 151 of cap free
Amount of items: 2
Items: 
Size: 705990 Color: 3
Size: 293860 Color: 2

Bin 2641: 151 of cap free
Amount of items: 2
Items: 
Size: 789838 Color: 3
Size: 210012 Color: 0

Bin 2642: 152 of cap free
Amount of items: 2
Items: 
Size: 519256 Color: 0
Size: 480593 Color: 2

Bin 2643: 152 of cap free
Amount of items: 2
Items: 
Size: 550774 Color: 2
Size: 449075 Color: 4

Bin 2644: 152 of cap free
Amount of items: 2
Items: 
Size: 560479 Color: 2
Size: 439370 Color: 0

Bin 2645: 152 of cap free
Amount of items: 2
Items: 
Size: 698810 Color: 3
Size: 301039 Color: 0

Bin 2646: 153 of cap free
Amount of items: 2
Items: 
Size: 664634 Color: 4
Size: 335214 Color: 1

Bin 2647: 153 of cap free
Amount of items: 2
Items: 
Size: 695688 Color: 2
Size: 304160 Color: 1

Bin 2648: 153 of cap free
Amount of items: 2
Items: 
Size: 745823 Color: 4
Size: 254025 Color: 3

Bin 2649: 154 of cap free
Amount of items: 2
Items: 
Size: 511103 Color: 1
Size: 488744 Color: 2

Bin 2650: 154 of cap free
Amount of items: 2
Items: 
Size: 538571 Color: 2
Size: 461276 Color: 4

Bin 2651: 154 of cap free
Amount of items: 2
Items: 
Size: 564578 Color: 1
Size: 435269 Color: 4

Bin 2652: 154 of cap free
Amount of items: 2
Items: 
Size: 577897 Color: 2
Size: 421950 Color: 0

Bin 2653: 154 of cap free
Amount of items: 2
Items: 
Size: 734281 Color: 4
Size: 265566 Color: 3

Bin 2654: 155 of cap free
Amount of items: 2
Items: 
Size: 604379 Color: 2
Size: 395467 Color: 1

Bin 2655: 155 of cap free
Amount of items: 2
Items: 
Size: 655583 Color: 3
Size: 344263 Color: 1

Bin 2656: 155 of cap free
Amount of items: 2
Items: 
Size: 664026 Color: 4
Size: 335820 Color: 3

Bin 2657: 156 of cap free
Amount of items: 2
Items: 
Size: 622327 Color: 2
Size: 377518 Color: 3

Bin 2658: 156 of cap free
Amount of items: 2
Items: 
Size: 646993 Color: 3
Size: 352852 Color: 4

Bin 2659: 156 of cap free
Amount of items: 2
Items: 
Size: 739840 Color: 2
Size: 260005 Color: 3

Bin 2660: 157 of cap free
Amount of items: 2
Items: 
Size: 589572 Color: 3
Size: 410272 Color: 0

Bin 2661: 157 of cap free
Amount of items: 2
Items: 
Size: 600875 Color: 1
Size: 398969 Color: 4

Bin 2662: 157 of cap free
Amount of items: 2
Items: 
Size: 610630 Color: 1
Size: 389214 Color: 3

Bin 2663: 157 of cap free
Amount of items: 2
Items: 
Size: 624873 Color: 3
Size: 374971 Color: 0

Bin 2664: 157 of cap free
Amount of items: 2
Items: 
Size: 714528 Color: 2
Size: 285316 Color: 0

Bin 2665: 158 of cap free
Amount of items: 2
Items: 
Size: 500058 Color: 3
Size: 499785 Color: 1

Bin 2666: 158 of cap free
Amount of items: 2
Items: 
Size: 511779 Color: 1
Size: 488064 Color: 4

Bin 2667: 158 of cap free
Amount of items: 2
Items: 
Size: 569333 Color: 2
Size: 430510 Color: 4

Bin 2668: 158 of cap free
Amount of items: 2
Items: 
Size: 651331 Color: 4
Size: 348512 Color: 0

Bin 2669: 158 of cap free
Amount of items: 2
Items: 
Size: 667747 Color: 0
Size: 332096 Color: 4

Bin 2670: 158 of cap free
Amount of items: 2
Items: 
Size: 697132 Color: 1
Size: 302711 Color: 3

Bin 2671: 158 of cap free
Amount of items: 2
Items: 
Size: 714941 Color: 0
Size: 284902 Color: 1

Bin 2672: 159 of cap free
Amount of items: 2
Items: 
Size: 543869 Color: 0
Size: 455973 Color: 3

Bin 2673: 159 of cap free
Amount of items: 2
Items: 
Size: 573373 Color: 1
Size: 426469 Color: 2

Bin 2674: 159 of cap free
Amount of items: 2
Items: 
Size: 582123 Color: 0
Size: 417719 Color: 3

Bin 2675: 159 of cap free
Amount of items: 2
Items: 
Size: 629273 Color: 3
Size: 370569 Color: 4

Bin 2676: 159 of cap free
Amount of items: 2
Items: 
Size: 703675 Color: 4
Size: 296167 Color: 2

Bin 2677: 160 of cap free
Amount of items: 2
Items: 
Size: 524526 Color: 1
Size: 475315 Color: 3

Bin 2678: 160 of cap free
Amount of items: 2
Items: 
Size: 546919 Color: 2
Size: 452922 Color: 0

Bin 2679: 160 of cap free
Amount of items: 2
Items: 
Size: 549550 Color: 2
Size: 450291 Color: 3

Bin 2680: 160 of cap free
Amount of items: 2
Items: 
Size: 562668 Color: 2
Size: 437173 Color: 4

Bin 2681: 160 of cap free
Amount of items: 2
Items: 
Size: 581335 Color: 1
Size: 418506 Color: 4

Bin 2682: 160 of cap free
Amount of items: 2
Items: 
Size: 649987 Color: 2
Size: 349854 Color: 4

Bin 2683: 160 of cap free
Amount of items: 2
Items: 
Size: 662122 Color: 4
Size: 337719 Color: 1

Bin 2684: 160 of cap free
Amount of items: 2
Items: 
Size: 713700 Color: 0
Size: 286141 Color: 4

Bin 2685: 161 of cap free
Amount of items: 2
Items: 
Size: 561681 Color: 0
Size: 438159 Color: 1

Bin 2686: 161 of cap free
Amount of items: 2
Items: 
Size: 630378 Color: 4
Size: 369462 Color: 3

Bin 2687: 161 of cap free
Amount of items: 2
Items: 
Size: 736307 Color: 0
Size: 263533 Color: 1

Bin 2688: 162 of cap free
Amount of items: 2
Items: 
Size: 509852 Color: 1
Size: 489987 Color: 0

Bin 2689: 162 of cap free
Amount of items: 2
Items: 
Size: 589225 Color: 3
Size: 410614 Color: 0

Bin 2690: 162 of cap free
Amount of items: 2
Items: 
Size: 617508 Color: 0
Size: 382331 Color: 4

Bin 2691: 162 of cap free
Amount of items: 2
Items: 
Size: 640329 Color: 3
Size: 359510 Color: 0

Bin 2692: 162 of cap free
Amount of items: 2
Items: 
Size: 659784 Color: 0
Size: 340055 Color: 2

Bin 2693: 162 of cap free
Amount of items: 2
Items: 
Size: 662352 Color: 4
Size: 337487 Color: 2

Bin 2694: 162 of cap free
Amount of items: 2
Items: 
Size: 725427 Color: 0
Size: 274412 Color: 1

Bin 2695: 162 of cap free
Amount of items: 2
Items: 
Size: 729659 Color: 4
Size: 270180 Color: 2

Bin 2696: 162 of cap free
Amount of items: 2
Items: 
Size: 761681 Color: 4
Size: 238158 Color: 2

Bin 2697: 162 of cap free
Amount of items: 2
Items: 
Size: 767237 Color: 2
Size: 232602 Color: 4

Bin 2698: 162 of cap free
Amount of items: 2
Items: 
Size: 777878 Color: 2
Size: 221961 Color: 4

Bin 2699: 163 of cap free
Amount of items: 2
Items: 
Size: 607271 Color: 2
Size: 392567 Color: 3

Bin 2700: 163 of cap free
Amount of items: 2
Items: 
Size: 644341 Color: 1
Size: 355497 Color: 2

Bin 2701: 163 of cap free
Amount of items: 2
Items: 
Size: 665145 Color: 3
Size: 334693 Color: 4

Bin 2702: 163 of cap free
Amount of items: 2
Items: 
Size: 737855 Color: 2
Size: 261983 Color: 1

Bin 2703: 163 of cap free
Amount of items: 2
Items: 
Size: 760684 Color: 3
Size: 239154 Color: 0

Bin 2704: 164 of cap free
Amount of items: 2
Items: 
Size: 532984 Color: 3
Size: 466853 Color: 2

Bin 2705: 164 of cap free
Amount of items: 2
Items: 
Size: 536641 Color: 0
Size: 463196 Color: 3

Bin 2706: 164 of cap free
Amount of items: 2
Items: 
Size: 541434 Color: 1
Size: 458403 Color: 4

Bin 2707: 164 of cap free
Amount of items: 2
Items: 
Size: 570095 Color: 3
Size: 429742 Color: 2

Bin 2708: 164 of cap free
Amount of items: 2
Items: 
Size: 588791 Color: 2
Size: 411046 Color: 0

Bin 2709: 164 of cap free
Amount of items: 2
Items: 
Size: 596582 Color: 4
Size: 403255 Color: 1

Bin 2710: 164 of cap free
Amount of items: 2
Items: 
Size: 614713 Color: 2
Size: 385124 Color: 0

Bin 2711: 164 of cap free
Amount of items: 2
Items: 
Size: 733320 Color: 3
Size: 266517 Color: 2

Bin 2712: 164 of cap free
Amount of items: 2
Items: 
Size: 744323 Color: 2
Size: 255514 Color: 1

Bin 2713: 165 of cap free
Amount of items: 2
Items: 
Size: 567999 Color: 0
Size: 431837 Color: 4

Bin 2714: 165 of cap free
Amount of items: 2
Items: 
Size: 613657 Color: 4
Size: 386179 Color: 2

Bin 2715: 165 of cap free
Amount of items: 2
Items: 
Size: 671644 Color: 2
Size: 328192 Color: 3

Bin 2716: 165 of cap free
Amount of items: 2
Items: 
Size: 706692 Color: 3
Size: 293144 Color: 2

Bin 2717: 165 of cap free
Amount of items: 2
Items: 
Size: 799237 Color: 1
Size: 200599 Color: 0

Bin 2718: 166 of cap free
Amount of items: 2
Items: 
Size: 591222 Color: 0
Size: 408613 Color: 2

Bin 2719: 166 of cap free
Amount of items: 2
Items: 
Size: 653418 Color: 2
Size: 346417 Color: 1

Bin 2720: 167 of cap free
Amount of items: 2
Items: 
Size: 638667 Color: 4
Size: 361167 Color: 1

Bin 2721: 167 of cap free
Amount of items: 2
Items: 
Size: 659546 Color: 1
Size: 340288 Color: 0

Bin 2722: 167 of cap free
Amount of items: 2
Items: 
Size: 675816 Color: 2
Size: 324018 Color: 0

Bin 2723: 167 of cap free
Amount of items: 2
Items: 
Size: 679153 Color: 1
Size: 320681 Color: 4

Bin 2724: 167 of cap free
Amount of items: 2
Items: 
Size: 741129 Color: 3
Size: 258705 Color: 4

Bin 2725: 168 of cap free
Amount of items: 2
Items: 
Size: 565203 Color: 3
Size: 434630 Color: 0

Bin 2726: 168 of cap free
Amount of items: 2
Items: 
Size: 718352 Color: 1
Size: 281481 Color: 2

Bin 2727: 169 of cap free
Amount of items: 2
Items: 
Size: 635541 Color: 0
Size: 364291 Color: 2

Bin 2728: 169 of cap free
Amount of items: 2
Items: 
Size: 698212 Color: 4
Size: 301620 Color: 0

Bin 2729: 169 of cap free
Amount of items: 2
Items: 
Size: 722721 Color: 0
Size: 277111 Color: 2

Bin 2730: 169 of cap free
Amount of items: 2
Items: 
Size: 737018 Color: 1
Size: 262814 Color: 3

Bin 2731: 169 of cap free
Amount of items: 2
Items: 
Size: 775528 Color: 2
Size: 224304 Color: 0

Bin 2732: 170 of cap free
Amount of items: 2
Items: 
Size: 565381 Color: 1
Size: 434450 Color: 0

Bin 2733: 170 of cap free
Amount of items: 2
Items: 
Size: 615935 Color: 4
Size: 383896 Color: 2

Bin 2734: 170 of cap free
Amount of items: 2
Items: 
Size: 621919 Color: 2
Size: 377912 Color: 3

Bin 2735: 170 of cap free
Amount of items: 2
Items: 
Size: 738504 Color: 4
Size: 261327 Color: 1

Bin 2736: 170 of cap free
Amount of items: 2
Items: 
Size: 766296 Color: 1
Size: 233535 Color: 4

Bin 2737: 171 of cap free
Amount of items: 2
Items: 
Size: 541718 Color: 4
Size: 458112 Color: 2

Bin 2738: 171 of cap free
Amount of items: 2
Items: 
Size: 624309 Color: 4
Size: 375521 Color: 3

Bin 2739: 171 of cap free
Amount of items: 2
Items: 
Size: 676047 Color: 1
Size: 323783 Color: 0

Bin 2740: 171 of cap free
Amount of items: 2
Items: 
Size: 693372 Color: 2
Size: 306458 Color: 0

Bin 2741: 171 of cap free
Amount of items: 2
Items: 
Size: 752820 Color: 2
Size: 247010 Color: 1

Bin 2742: 171 of cap free
Amount of items: 2
Items: 
Size: 784316 Color: 0
Size: 215514 Color: 4

Bin 2743: 172 of cap free
Amount of items: 2
Items: 
Size: 527494 Color: 2
Size: 472335 Color: 4

Bin 2744: 172 of cap free
Amount of items: 2
Items: 
Size: 557723 Color: 3
Size: 442106 Color: 2

Bin 2745: 172 of cap free
Amount of items: 2
Items: 
Size: 586208 Color: 2
Size: 413621 Color: 4

Bin 2746: 172 of cap free
Amount of items: 2
Items: 
Size: 627810 Color: 3
Size: 372019 Color: 1

Bin 2747: 172 of cap free
Amount of items: 2
Items: 
Size: 751561 Color: 2
Size: 248268 Color: 1

Bin 2748: 173 of cap free
Amount of items: 2
Items: 
Size: 511381 Color: 2
Size: 488447 Color: 4

Bin 2749: 173 of cap free
Amount of items: 2
Items: 
Size: 516803 Color: 0
Size: 483025 Color: 2

Bin 2750: 174 of cap free
Amount of items: 2
Items: 
Size: 558832 Color: 0
Size: 440995 Color: 4

Bin 2751: 174 of cap free
Amount of items: 2
Items: 
Size: 632040 Color: 3
Size: 367787 Color: 2

Bin 2752: 174 of cap free
Amount of items: 2
Items: 
Size: 657915 Color: 2
Size: 341912 Color: 4

Bin 2753: 174 of cap free
Amount of items: 2
Items: 
Size: 725415 Color: 2
Size: 274412 Color: 1

Bin 2754: 174 of cap free
Amount of items: 2
Items: 
Size: 774192 Color: 0
Size: 225635 Color: 4

Bin 2755: 175 of cap free
Amount of items: 2
Items: 
Size: 526174 Color: 1
Size: 473652 Color: 0

Bin 2756: 175 of cap free
Amount of items: 2
Items: 
Size: 580420 Color: 1
Size: 419406 Color: 4

Bin 2757: 175 of cap free
Amount of items: 2
Items: 
Size: 595404 Color: 1
Size: 404422 Color: 2

Bin 2758: 175 of cap free
Amount of items: 2
Items: 
Size: 676799 Color: 3
Size: 323027 Color: 1

Bin 2759: 175 of cap free
Amount of items: 2
Items: 
Size: 780733 Color: 4
Size: 219093 Color: 3

Bin 2760: 176 of cap free
Amount of items: 2
Items: 
Size: 525753 Color: 4
Size: 474072 Color: 3

Bin 2761: 176 of cap free
Amount of items: 2
Items: 
Size: 601183 Color: 4
Size: 398642 Color: 1

Bin 2762: 176 of cap free
Amount of items: 2
Items: 
Size: 618350 Color: 3
Size: 381475 Color: 2

Bin 2763: 176 of cap free
Amount of items: 2
Items: 
Size: 799045 Color: 2
Size: 200780 Color: 4

Bin 2764: 177 of cap free
Amount of items: 2
Items: 
Size: 512188 Color: 3
Size: 487636 Color: 4

Bin 2765: 177 of cap free
Amount of items: 2
Items: 
Size: 567181 Color: 3
Size: 432643 Color: 0

Bin 2766: 177 of cap free
Amount of items: 2
Items: 
Size: 650791 Color: 3
Size: 349033 Color: 2

Bin 2767: 177 of cap free
Amount of items: 2
Items: 
Size: 673497 Color: 3
Size: 326327 Color: 4

Bin 2768: 177 of cap free
Amount of items: 2
Items: 
Size: 754972 Color: 3
Size: 244852 Color: 0

Bin 2769: 177 of cap free
Amount of items: 2
Items: 
Size: 758939 Color: 2
Size: 240885 Color: 0

Bin 2770: 177 of cap free
Amount of items: 2
Items: 
Size: 765701 Color: 1
Size: 234123 Color: 3

Bin 2771: 177 of cap free
Amount of items: 2
Items: 
Size: 770719 Color: 3
Size: 229105 Color: 1

Bin 2772: 178 of cap free
Amount of items: 2
Items: 
Size: 537457 Color: 3
Size: 462366 Color: 2

Bin 2773: 178 of cap free
Amount of items: 2
Items: 
Size: 590632 Color: 4
Size: 409191 Color: 1

Bin 2774: 178 of cap free
Amount of items: 2
Items: 
Size: 648267 Color: 0
Size: 351556 Color: 3

Bin 2775: 178 of cap free
Amount of items: 2
Items: 
Size: 657460 Color: 0
Size: 342363 Color: 2

Bin 2776: 178 of cap free
Amount of items: 2
Items: 
Size: 750721 Color: 0
Size: 249102 Color: 1

Bin 2777: 178 of cap free
Amount of items: 2
Items: 
Size: 775873 Color: 0
Size: 223950 Color: 1

Bin 2778: 179 of cap free
Amount of items: 2
Items: 
Size: 517028 Color: 3
Size: 482794 Color: 1

Bin 2779: 179 of cap free
Amount of items: 2
Items: 
Size: 566218 Color: 1
Size: 433604 Color: 3

Bin 2780: 179 of cap free
Amount of items: 2
Items: 
Size: 590437 Color: 4
Size: 409385 Color: 2

Bin 2781: 179 of cap free
Amount of items: 2
Items: 
Size: 594139 Color: 0
Size: 405683 Color: 3

Bin 2782: 179 of cap free
Amount of items: 2
Items: 
Size: 610141 Color: 0
Size: 389681 Color: 3

Bin 2783: 179 of cap free
Amount of items: 2
Items: 
Size: 657670 Color: 0
Size: 342152 Color: 4

Bin 2784: 179 of cap free
Amount of items: 2
Items: 
Size: 703666 Color: 2
Size: 296156 Color: 1

Bin 2785: 179 of cap free
Amount of items: 2
Items: 
Size: 752157 Color: 2
Size: 247665 Color: 0

Bin 2786: 179 of cap free
Amount of items: 2
Items: 
Size: 762911 Color: 0
Size: 236911 Color: 2

Bin 2787: 181 of cap free
Amount of items: 2
Items: 
Size: 563906 Color: 4
Size: 435914 Color: 0

Bin 2788: 181 of cap free
Amount of items: 2
Items: 
Size: 636601 Color: 0
Size: 363219 Color: 3

Bin 2789: 181 of cap free
Amount of items: 2
Items: 
Size: 671301 Color: 4
Size: 328519 Color: 0

Bin 2790: 181 of cap free
Amount of items: 2
Items: 
Size: 722204 Color: 2
Size: 277616 Color: 1

Bin 2791: 182 of cap free
Amount of items: 2
Items: 
Size: 557975 Color: 3
Size: 441844 Color: 1

Bin 2792: 182 of cap free
Amount of items: 2
Items: 
Size: 567439 Color: 0
Size: 432380 Color: 4

Bin 2793: 182 of cap free
Amount of items: 2
Items: 
Size: 643272 Color: 0
Size: 356547 Color: 2

Bin 2794: 182 of cap free
Amount of items: 2
Items: 
Size: 677390 Color: 3
Size: 322429 Color: 1

Bin 2795: 182 of cap free
Amount of items: 2
Items: 
Size: 717464 Color: 3
Size: 282355 Color: 1

Bin 2796: 182 of cap free
Amount of items: 2
Items: 
Size: 731942 Color: 0
Size: 267877 Color: 1

Bin 2797: 183 of cap free
Amount of items: 2
Items: 
Size: 616402 Color: 0
Size: 383416 Color: 1

Bin 2798: 183 of cap free
Amount of items: 2
Items: 
Size: 655513 Color: 1
Size: 344305 Color: 3

Bin 2799: 183 of cap free
Amount of items: 2
Items: 
Size: 701291 Color: 0
Size: 298527 Color: 1

Bin 2800: 184 of cap free
Amount of items: 2
Items: 
Size: 572837 Color: 4
Size: 426980 Color: 1

Bin 2801: 184 of cap free
Amount of items: 2
Items: 
Size: 603371 Color: 0
Size: 396446 Color: 2

Bin 2802: 184 of cap free
Amount of items: 2
Items: 
Size: 641600 Color: 0
Size: 358217 Color: 3

Bin 2803: 186 of cap free
Amount of items: 6
Items: 
Size: 175336 Color: 4
Size: 175265 Color: 0
Size: 175047 Color: 1
Size: 175036 Color: 0
Size: 174998 Color: 0
Size: 124133 Color: 2

Bin 2804: 186 of cap free
Amount of items: 2
Items: 
Size: 527735 Color: 3
Size: 472080 Color: 2

Bin 2805: 186 of cap free
Amount of items: 2
Items: 
Size: 755879 Color: 0
Size: 243936 Color: 2

Bin 2806: 187 of cap free
Amount of items: 2
Items: 
Size: 791623 Color: 3
Size: 208191 Color: 1

Bin 2807: 188 of cap free
Amount of items: 8
Items: 
Size: 126519 Color: 0
Size: 126346 Color: 2
Size: 126230 Color: 4
Size: 126099 Color: 1
Size: 126095 Color: 2
Size: 126087 Color: 0
Size: 126070 Color: 0
Size: 116367 Color: 0

Bin 2808: 188 of cap free
Amount of items: 2
Items: 
Size: 503694 Color: 3
Size: 496119 Color: 0

Bin 2809: 188 of cap free
Amount of items: 2
Items: 
Size: 632911 Color: 2
Size: 366902 Color: 3

Bin 2810: 188 of cap free
Amount of items: 2
Items: 
Size: 668319 Color: 1
Size: 331494 Color: 4

Bin 2811: 189 of cap free
Amount of items: 2
Items: 
Size: 681725 Color: 2
Size: 318087 Color: 3

Bin 2812: 189 of cap free
Amount of items: 2
Items: 
Size: 688382 Color: 0
Size: 311430 Color: 4

Bin 2813: 189 of cap free
Amount of items: 2
Items: 
Size: 701287 Color: 3
Size: 298525 Color: 2

Bin 2814: 189 of cap free
Amount of items: 2
Items: 
Size: 795711 Color: 0
Size: 204101 Color: 2

Bin 2815: 190 of cap free
Amount of items: 2
Items: 
Size: 516172 Color: 1
Size: 483639 Color: 2

Bin 2816: 190 of cap free
Amount of items: 2
Items: 
Size: 550970 Color: 1
Size: 448841 Color: 3

Bin 2817: 190 of cap free
Amount of items: 2
Items: 
Size: 594128 Color: 2
Size: 405683 Color: 3

Bin 2818: 190 of cap free
Amount of items: 2
Items: 
Size: 609363 Color: 1
Size: 390448 Color: 3

Bin 2819: 190 of cap free
Amount of items: 2
Items: 
Size: 617508 Color: 3
Size: 382303 Color: 4

Bin 2820: 190 of cap free
Amount of items: 2
Items: 
Size: 637905 Color: 2
Size: 361906 Color: 0

Bin 2821: 190 of cap free
Amount of items: 2
Items: 
Size: 665122 Color: 3
Size: 334689 Color: 2

Bin 2822: 190 of cap free
Amount of items: 2
Items: 
Size: 705589 Color: 1
Size: 294222 Color: 0

Bin 2823: 191 of cap free
Amount of items: 2
Items: 
Size: 513366 Color: 3
Size: 486444 Color: 2

Bin 2824: 191 of cap free
Amount of items: 2
Items: 
Size: 679536 Color: 2
Size: 320274 Color: 4

Bin 2825: 191 of cap free
Amount of items: 2
Items: 
Size: 727640 Color: 2
Size: 272170 Color: 3

Bin 2826: 191 of cap free
Amount of items: 2
Items: 
Size: 755898 Color: 2
Size: 243912 Color: 0

Bin 2827: 192 of cap free
Amount of items: 2
Items: 
Size: 566809 Color: 4
Size: 433000 Color: 2

Bin 2828: 192 of cap free
Amount of items: 2
Items: 
Size: 669778 Color: 4
Size: 330031 Color: 3

Bin 2829: 193 of cap free
Amount of items: 2
Items: 
Size: 562840 Color: 1
Size: 436968 Color: 2

Bin 2830: 193 of cap free
Amount of items: 2
Items: 
Size: 574750 Color: 1
Size: 425058 Color: 4

Bin 2831: 193 of cap free
Amount of items: 2
Items: 
Size: 666946 Color: 0
Size: 332862 Color: 3

Bin 2832: 193 of cap free
Amount of items: 2
Items: 
Size: 776638 Color: 0
Size: 223170 Color: 3

Bin 2833: 194 of cap free
Amount of items: 2
Items: 
Size: 542638 Color: 1
Size: 457169 Color: 2

Bin 2834: 194 of cap free
Amount of items: 2
Items: 
Size: 547325 Color: 4
Size: 452482 Color: 3

Bin 2835: 194 of cap free
Amount of items: 2
Items: 
Size: 602249 Color: 4
Size: 397558 Color: 0

Bin 2836: 194 of cap free
Amount of items: 2
Items: 
Size: 609634 Color: 2
Size: 390173 Color: 3

Bin 2837: 194 of cap free
Amount of items: 2
Items: 
Size: 650155 Color: 2
Size: 349652 Color: 3

Bin 2838: 194 of cap free
Amount of items: 2
Items: 
Size: 685795 Color: 4
Size: 314012 Color: 3

Bin 2839: 194 of cap free
Amount of items: 2
Items: 
Size: 712114 Color: 0
Size: 287693 Color: 2

Bin 2840: 194 of cap free
Amount of items: 2
Items: 
Size: 760112 Color: 2
Size: 239695 Color: 1

Bin 2841: 195 of cap free
Amount of items: 2
Items: 
Size: 508985 Color: 0
Size: 490821 Color: 3

Bin 2842: 195 of cap free
Amount of items: 2
Items: 
Size: 659022 Color: 4
Size: 340784 Color: 1

Bin 2843: 195 of cap free
Amount of items: 2
Items: 
Size: 712910 Color: 4
Size: 286896 Color: 1

Bin 2844: 195 of cap free
Amount of items: 2
Items: 
Size: 740547 Color: 0
Size: 259259 Color: 3

Bin 2845: 196 of cap free
Amount of items: 2
Items: 
Size: 549975 Color: 2
Size: 449830 Color: 0

Bin 2846: 196 of cap free
Amount of items: 2
Items: 
Size: 584765 Color: 2
Size: 415040 Color: 1

Bin 2847: 196 of cap free
Amount of items: 2
Items: 
Size: 735880 Color: 2
Size: 263925 Color: 1

Bin 2848: 197 of cap free
Amount of items: 2
Items: 
Size: 558403 Color: 1
Size: 441401 Color: 2

Bin 2849: 197 of cap free
Amount of items: 2
Items: 
Size: 611632 Color: 4
Size: 388172 Color: 3

Bin 2850: 198 of cap free
Amount of items: 2
Items: 
Size: 654624 Color: 0
Size: 345179 Color: 1

Bin 2851: 199 of cap free
Amount of items: 2
Items: 
Size: 503011 Color: 2
Size: 496791 Color: 1

Bin 2852: 199 of cap free
Amount of items: 2
Items: 
Size: 566621 Color: 1
Size: 433181 Color: 4

Bin 2853: 199 of cap free
Amount of items: 2
Items: 
Size: 778849 Color: 1
Size: 220953 Color: 0

Bin 2854: 200 of cap free
Amount of items: 2
Items: 
Size: 519916 Color: 0
Size: 479885 Color: 2

Bin 2855: 200 of cap free
Amount of items: 2
Items: 
Size: 617485 Color: 1
Size: 382316 Color: 3

Bin 2856: 200 of cap free
Amount of items: 2
Items: 
Size: 638657 Color: 3
Size: 361144 Color: 2

Bin 2857: 201 of cap free
Amount of items: 2
Items: 
Size: 530062 Color: 3
Size: 469738 Color: 0

Bin 2858: 201 of cap free
Amount of items: 2
Items: 
Size: 612625 Color: 1
Size: 387175 Color: 3

Bin 2859: 201 of cap free
Amount of items: 2
Items: 
Size: 643265 Color: 0
Size: 356535 Color: 2

Bin 2860: 201 of cap free
Amount of items: 2
Items: 
Size: 680695 Color: 2
Size: 319105 Color: 0

Bin 2861: 201 of cap free
Amount of items: 2
Items: 
Size: 730353 Color: 1
Size: 269447 Color: 3

Bin 2862: 202 of cap free
Amount of items: 2
Items: 
Size: 622775 Color: 3
Size: 377024 Color: 2

Bin 2863: 202 of cap free
Amount of items: 2
Items: 
Size: 723277 Color: 0
Size: 276522 Color: 4

Bin 2864: 203 of cap free
Amount of items: 2
Items: 
Size: 585182 Color: 0
Size: 414616 Color: 1

Bin 2865: 203 of cap free
Amount of items: 2
Items: 
Size: 661196 Color: 0
Size: 338602 Color: 4

Bin 2866: 203 of cap free
Amount of items: 2
Items: 
Size: 706653 Color: 0
Size: 293145 Color: 3

Bin 2867: 203 of cap free
Amount of items: 2
Items: 
Size: 726759 Color: 1
Size: 273039 Color: 3

Bin 2868: 203 of cap free
Amount of items: 2
Items: 
Size: 765119 Color: 4
Size: 234679 Color: 1

Bin 2869: 204 of cap free
Amount of items: 2
Items: 
Size: 513408 Color: 2
Size: 486389 Color: 1

Bin 2870: 204 of cap free
Amount of items: 2
Items: 
Size: 526681 Color: 3
Size: 473116 Color: 0

Bin 2871: 204 of cap free
Amount of items: 2
Items: 
Size: 630689 Color: 0
Size: 369108 Color: 3

Bin 2872: 204 of cap free
Amount of items: 2
Items: 
Size: 658168 Color: 3
Size: 341629 Color: 0

Bin 2873: 204 of cap free
Amount of items: 2
Items: 
Size: 764519 Color: 4
Size: 235278 Color: 3

Bin 2874: 205 of cap free
Amount of items: 2
Items: 
Size: 532303 Color: 2
Size: 467493 Color: 3

Bin 2875: 205 of cap free
Amount of items: 2
Items: 
Size: 608529 Color: 1
Size: 391267 Color: 3

Bin 2876: 205 of cap free
Amount of items: 2
Items: 
Size: 613238 Color: 4
Size: 386558 Color: 1

Bin 2877: 205 of cap free
Amount of items: 2
Items: 
Size: 627447 Color: 2
Size: 372349 Color: 0

Bin 2878: 206 of cap free
Amount of items: 2
Items: 
Size: 505434 Color: 4
Size: 494361 Color: 2

Bin 2879: 206 of cap free
Amount of items: 2
Items: 
Size: 567176 Color: 1
Size: 432619 Color: 0

Bin 2880: 206 of cap free
Amount of items: 2
Items: 
Size: 568638 Color: 4
Size: 431157 Color: 3

Bin 2881: 206 of cap free
Amount of items: 2
Items: 
Size: 796760 Color: 4
Size: 203035 Color: 0

Bin 2882: 207 of cap free
Amount of items: 2
Items: 
Size: 502815 Color: 3
Size: 496979 Color: 2

Bin 2883: 207 of cap free
Amount of items: 2
Items: 
Size: 744768 Color: 4
Size: 255026 Color: 1

Bin 2884: 208 of cap free
Amount of items: 8
Items: 
Size: 128524 Color: 4
Size: 128490 Color: 2
Size: 128471 Color: 3
Size: 128287 Color: 0
Size: 128258 Color: 3
Size: 127817 Color: 0
Size: 127644 Color: 4
Size: 102302 Color: 0

Bin 2885: 208 of cap free
Amount of items: 2
Items: 
Size: 665333 Color: 2
Size: 334460 Color: 0

Bin 2886: 208 of cap free
Amount of items: 2
Items: 
Size: 696214 Color: 4
Size: 303579 Color: 1

Bin 2887: 209 of cap free
Amount of items: 2
Items: 
Size: 598986 Color: 1
Size: 400806 Color: 0

Bin 2888: 209 of cap free
Amount of items: 2
Items: 
Size: 719302 Color: 1
Size: 280490 Color: 3

Bin 2889: 209 of cap free
Amount of items: 2
Items: 
Size: 782299 Color: 0
Size: 217493 Color: 3

Bin 2890: 209 of cap free
Amount of items: 2
Items: 
Size: 797915 Color: 1
Size: 201877 Color: 0

Bin 2891: 210 of cap free
Amount of items: 2
Items: 
Size: 584729 Color: 0
Size: 415062 Color: 2

Bin 2892: 210 of cap free
Amount of items: 2
Items: 
Size: 626859 Color: 1
Size: 372932 Color: 2

Bin 2893: 210 of cap free
Amount of items: 2
Items: 
Size: 658175 Color: 0
Size: 341616 Color: 1

Bin 2894: 210 of cap free
Amount of items: 2
Items: 
Size: 711363 Color: 2
Size: 288428 Color: 1

Bin 2895: 210 of cap free
Amount of items: 2
Items: 
Size: 718583 Color: 0
Size: 281208 Color: 1

Bin 2896: 210 of cap free
Amount of items: 2
Items: 
Size: 741445 Color: 3
Size: 258346 Color: 2

Bin 2897: 211 of cap free
Amount of items: 2
Items: 
Size: 550183 Color: 1
Size: 449607 Color: 3

Bin 2898: 211 of cap free
Amount of items: 2
Items: 
Size: 560314 Color: 3
Size: 439476 Color: 2

Bin 2899: 211 of cap free
Amount of items: 2
Items: 
Size: 634711 Color: 2
Size: 365079 Color: 4

Bin 2900: 211 of cap free
Amount of items: 2
Items: 
Size: 690937 Color: 0
Size: 308853 Color: 4

Bin 2901: 212 of cap free
Amount of items: 2
Items: 
Size: 517052 Color: 1
Size: 482737 Color: 3

Bin 2902: 212 of cap free
Amount of items: 2
Items: 
Size: 645974 Color: 0
Size: 353815 Color: 1

Bin 2903: 212 of cap free
Amount of items: 2
Items: 
Size: 721194 Color: 2
Size: 278595 Color: 1

Bin 2904: 212 of cap free
Amount of items: 2
Items: 
Size: 736772 Color: 2
Size: 263017 Color: 1

Bin 2905: 213 of cap free
Amount of items: 2
Items: 
Size: 517722 Color: 1
Size: 482066 Color: 2

Bin 2906: 213 of cap free
Amount of items: 2
Items: 
Size: 597342 Color: 0
Size: 402446 Color: 2

Bin 2907: 213 of cap free
Amount of items: 2
Items: 
Size: 701639 Color: 3
Size: 298149 Color: 1

Bin 2908: 214 of cap free
Amount of items: 2
Items: 
Size: 525040 Color: 2
Size: 474747 Color: 0

Bin 2909: 214 of cap free
Amount of items: 2
Items: 
Size: 527706 Color: 2
Size: 472081 Color: 3

Bin 2910: 214 of cap free
Amount of items: 2
Items: 
Size: 619875 Color: 4
Size: 379912 Color: 1

Bin 2911: 214 of cap free
Amount of items: 2
Items: 
Size: 732516 Color: 1
Size: 267271 Color: 3

Bin 2912: 214 of cap free
Amount of items: 2
Items: 
Size: 743449 Color: 1
Size: 256338 Color: 4

Bin 2913: 214 of cap free
Amount of items: 2
Items: 
Size: 782793 Color: 3
Size: 216994 Color: 0

Bin 2914: 215 of cap free
Amount of items: 2
Items: 
Size: 541122 Color: 4
Size: 458664 Color: 1

Bin 2915: 215 of cap free
Amount of items: 2
Items: 
Size: 638196 Color: 4
Size: 361590 Color: 3

Bin 2916: 216 of cap free
Amount of items: 2
Items: 
Size: 604856 Color: 1
Size: 394929 Color: 4

Bin 2917: 216 of cap free
Amount of items: 2
Items: 
Size: 654998 Color: 3
Size: 344787 Color: 1

Bin 2918: 216 of cap free
Amount of items: 2
Items: 
Size: 747236 Color: 1
Size: 252549 Color: 4

Bin 2919: 217 of cap free
Amount of items: 2
Items: 
Size: 587998 Color: 1
Size: 411786 Color: 3

Bin 2920: 217 of cap free
Amount of items: 2
Items: 
Size: 724051 Color: 0
Size: 275733 Color: 1

Bin 2921: 217 of cap free
Amount of items: 2
Items: 
Size: 724475 Color: 3
Size: 275309 Color: 2

Bin 2922: 217 of cap free
Amount of items: 2
Items: 
Size: 748147 Color: 4
Size: 251637 Color: 0

Bin 2923: 217 of cap free
Amount of items: 2
Items: 
Size: 769181 Color: 2
Size: 230603 Color: 4

Bin 2924: 217 of cap free
Amount of items: 2
Items: 
Size: 774674 Color: 0
Size: 225110 Color: 3

Bin 2925: 218 of cap free
Amount of items: 2
Items: 
Size: 523209 Color: 1
Size: 476574 Color: 4

Bin 2926: 218 of cap free
Amount of items: 2
Items: 
Size: 687083 Color: 0
Size: 312700 Color: 4

Bin 2927: 219 of cap free
Amount of items: 2
Items: 
Size: 537078 Color: 1
Size: 462704 Color: 3

Bin 2928: 219 of cap free
Amount of items: 2
Items: 
Size: 570409 Color: 4
Size: 429373 Color: 2

Bin 2929: 219 of cap free
Amount of items: 2
Items: 
Size: 665808 Color: 2
Size: 333974 Color: 3

Bin 2930: 220 of cap free
Amount of items: 2
Items: 
Size: 642310 Color: 3
Size: 357471 Color: 0

Bin 2931: 220 of cap free
Amount of items: 2
Items: 
Size: 647959 Color: 1
Size: 351822 Color: 2

Bin 2932: 220 of cap free
Amount of items: 2
Items: 
Size: 668037 Color: 0
Size: 331744 Color: 3

Bin 2933: 220 of cap free
Amount of items: 2
Items: 
Size: 672144 Color: 1
Size: 327637 Color: 2

Bin 2934: 221 of cap free
Amount of items: 2
Items: 
Size: 589161 Color: 4
Size: 410619 Color: 3

Bin 2935: 221 of cap free
Amount of items: 2
Items: 
Size: 648260 Color: 0
Size: 351520 Color: 2

Bin 2936: 222 of cap free
Amount of items: 2
Items: 
Size: 523208 Color: 4
Size: 476571 Color: 2

Bin 2937: 222 of cap free
Amount of items: 2
Items: 
Size: 629083 Color: 1
Size: 370696 Color: 3

Bin 2938: 222 of cap free
Amount of items: 2
Items: 
Size: 761584 Color: 3
Size: 238195 Color: 4

Bin 2939: 222 of cap free
Amount of items: 2
Items: 
Size: 779754 Color: 0
Size: 220025 Color: 1

Bin 2940: 222 of cap free
Amount of items: 2
Items: 
Size: 792589 Color: 3
Size: 207190 Color: 2

Bin 2941: 223 of cap free
Amount of items: 2
Items: 
Size: 600606 Color: 1
Size: 399172 Color: 2

Bin 2942: 223 of cap free
Amount of items: 2
Items: 
Size: 646667 Color: 0
Size: 353111 Color: 4

Bin 2943: 223 of cap free
Amount of items: 2
Items: 
Size: 671711 Color: 3
Size: 328067 Color: 2

Bin 2944: 223 of cap free
Amount of items: 2
Items: 
Size: 687317 Color: 4
Size: 312461 Color: 2

Bin 2945: 223 of cap free
Amount of items: 2
Items: 
Size: 697494 Color: 2
Size: 302284 Color: 1

Bin 2946: 223 of cap free
Amount of items: 2
Items: 
Size: 721192 Color: 2
Size: 278586 Color: 0

Bin 2947: 223 of cap free
Amount of items: 2
Items: 
Size: 727604 Color: 4
Size: 272174 Color: 2

Bin 2948: 223 of cap free
Amount of items: 2
Items: 
Size: 786471 Color: 4
Size: 213307 Color: 3

Bin 2949: 224 of cap free
Amount of items: 2
Items: 
Size: 619199 Color: 1
Size: 380578 Color: 3

Bin 2950: 224 of cap free
Amount of items: 2
Items: 
Size: 693330 Color: 2
Size: 306447 Color: 0

Bin 2951: 225 of cap free
Amount of items: 2
Items: 
Size: 603340 Color: 1
Size: 396436 Color: 4

Bin 2952: 225 of cap free
Amount of items: 2
Items: 
Size: 621883 Color: 1
Size: 377893 Color: 4

Bin 2953: 225 of cap free
Amount of items: 2
Items: 
Size: 653409 Color: 1
Size: 346367 Color: 4

Bin 2954: 225 of cap free
Amount of items: 2
Items: 
Size: 663087 Color: 4
Size: 336689 Color: 2

Bin 2955: 225 of cap free
Amount of items: 2
Items: 
Size: 710275 Color: 1
Size: 289501 Color: 3

Bin 2956: 225 of cap free
Amount of items: 2
Items: 
Size: 777273 Color: 3
Size: 222503 Color: 1

Bin 2957: 226 of cap free
Amount of items: 2
Items: 
Size: 544566 Color: 3
Size: 455209 Color: 0

Bin 2958: 226 of cap free
Amount of items: 2
Items: 
Size: 589562 Color: 3
Size: 410213 Color: 4

Bin 2959: 227 of cap free
Amount of items: 2
Items: 
Size: 567171 Color: 2
Size: 432603 Color: 0

Bin 2960: 227 of cap free
Amount of items: 2
Items: 
Size: 567940 Color: 3
Size: 431834 Color: 0

Bin 2961: 227 of cap free
Amount of items: 2
Items: 
Size: 634396 Color: 4
Size: 365378 Color: 3

Bin 2962: 227 of cap free
Amount of items: 2
Items: 
Size: 710566 Color: 4
Size: 289208 Color: 1

Bin 2963: 227 of cap free
Amount of items: 2
Items: 
Size: 721724 Color: 2
Size: 278050 Color: 4

Bin 2964: 228 of cap free
Amount of items: 2
Items: 
Size: 666773 Color: 2
Size: 333000 Color: 0

Bin 2965: 228 of cap free
Amount of items: 2
Items: 
Size: 762859 Color: 4
Size: 236914 Color: 0

Bin 2966: 228 of cap free
Amount of items: 2
Items: 
Size: 783270 Color: 2
Size: 216503 Color: 1

Bin 2967: 228 of cap free
Amount of items: 2
Items: 
Size: 793362 Color: 0
Size: 206411 Color: 3

Bin 2968: 229 of cap free
Amount of items: 2
Items: 
Size: 500284 Color: 3
Size: 499488 Color: 2

Bin 2969: 229 of cap free
Amount of items: 2
Items: 
Size: 654613 Color: 0
Size: 345159 Color: 1

Bin 2970: 229 of cap free
Amount of items: 2
Items: 
Size: 733999 Color: 0
Size: 265773 Color: 4

Bin 2971: 230 of cap free
Amount of items: 2
Items: 
Size: 576573 Color: 2
Size: 423198 Color: 3

Bin 2972: 230 of cap free
Amount of items: 2
Items: 
Size: 580719 Color: 2
Size: 419052 Color: 1

Bin 2973: 230 of cap free
Amount of items: 2
Items: 
Size: 706984 Color: 3
Size: 292787 Color: 4

Bin 2974: 230 of cap free
Amount of items: 2
Items: 
Size: 751503 Color: 3
Size: 248268 Color: 2

Bin 2975: 231 of cap free
Amount of items: 2
Items: 
Size: 532147 Color: 4
Size: 467623 Color: 2

Bin 2976: 232 of cap free
Amount of items: 2
Items: 
Size: 572827 Color: 3
Size: 426942 Color: 4

Bin 2977: 232 of cap free
Amount of items: 2
Items: 
Size: 625837 Color: 2
Size: 373932 Color: 3

Bin 2978: 233 of cap free
Amount of items: 2
Items: 
Size: 501000 Color: 2
Size: 498768 Color: 0

Bin 2979: 233 of cap free
Amount of items: 2
Items: 
Size: 592165 Color: 2
Size: 407603 Color: 4

Bin 2980: 233 of cap free
Amount of items: 2
Items: 
Size: 716673 Color: 0
Size: 283095 Color: 4

Bin 2981: 233 of cap free
Amount of items: 2
Items: 
Size: 730703 Color: 4
Size: 269065 Color: 3

Bin 2982: 233 of cap free
Amount of items: 2
Items: 
Size: 739375 Color: 2
Size: 260393 Color: 1

Bin 2983: 233 of cap free
Amount of items: 2
Items: 
Size: 750928 Color: 4
Size: 248840 Color: 3

Bin 2984: 234 of cap free
Amount of items: 2
Items: 
Size: 577109 Color: 1
Size: 422658 Color: 2

Bin 2985: 234 of cap free
Amount of items: 2
Items: 
Size: 631369 Color: 2
Size: 368398 Color: 4

Bin 2986: 234 of cap free
Amount of items: 2
Items: 
Size: 756714 Color: 0
Size: 243053 Color: 3

Bin 2987: 235 of cap free
Amount of items: 2
Items: 
Size: 646025 Color: 1
Size: 353741 Color: 0

Bin 2988: 235 of cap free
Amount of items: 2
Items: 
Size: 664788 Color: 1
Size: 334978 Color: 0

Bin 2989: 235 of cap free
Amount of items: 2
Items: 
Size: 690577 Color: 1
Size: 309189 Color: 0

Bin 2990: 236 of cap free
Amount of items: 2
Items: 
Size: 663970 Color: 2
Size: 335795 Color: 1

Bin 2991: 237 of cap free
Amount of items: 2
Items: 
Size: 521925 Color: 4
Size: 477839 Color: 1

Bin 2992: 237 of cap free
Amount of items: 2
Items: 
Size: 623258 Color: 2
Size: 376506 Color: 1

Bin 2993: 237 of cap free
Amount of items: 2
Items: 
Size: 733071 Color: 0
Size: 266693 Color: 4

Bin 2994: 237 of cap free
Amount of items: 2
Items: 
Size: 772943 Color: 4
Size: 226821 Color: 2

Bin 2995: 238 of cap free
Amount of items: 2
Items: 
Size: 739824 Color: 4
Size: 259939 Color: 3

Bin 2996: 239 of cap free
Amount of items: 2
Items: 
Size: 537631 Color: 2
Size: 462131 Color: 3

Bin 2997: 239 of cap free
Amount of items: 2
Items: 
Size: 557700 Color: 1
Size: 442062 Color: 4

Bin 2998: 239 of cap free
Amount of items: 2
Items: 
Size: 719999 Color: 2
Size: 279763 Color: 3

Bin 2999: 239 of cap free
Amount of items: 2
Items: 
Size: 750243 Color: 3
Size: 249519 Color: 2

Bin 3000: 240 of cap free
Amount of items: 2
Items: 
Size: 551569 Color: 2
Size: 448192 Color: 0

Bin 3001: 240 of cap free
Amount of items: 2
Items: 
Size: 714872 Color: 2
Size: 284889 Color: 1

Bin 3002: 241 of cap free
Amount of items: 2
Items: 
Size: 508330 Color: 4
Size: 491430 Color: 1

Bin 3003: 241 of cap free
Amount of items: 2
Items: 
Size: 577123 Color: 2
Size: 422637 Color: 0

Bin 3004: 241 of cap free
Amount of items: 2
Items: 
Size: 641069 Color: 2
Size: 358691 Color: 1

Bin 3005: 241 of cap free
Amount of items: 2
Items: 
Size: 650788 Color: 0
Size: 348972 Color: 4

Bin 3006: 241 of cap free
Amount of items: 2
Items: 
Size: 676292 Color: 3
Size: 323468 Color: 0

Bin 3007: 241 of cap free
Amount of items: 2
Items: 
Size: 681011 Color: 1
Size: 318749 Color: 3

Bin 3008: 241 of cap free
Amount of items: 2
Items: 
Size: 790093 Color: 1
Size: 209667 Color: 2

Bin 3009: 242 of cap free
Amount of items: 2
Items: 
Size: 554305 Color: 1
Size: 445454 Color: 3

Bin 3010: 243 of cap free
Amount of items: 2
Items: 
Size: 768060 Color: 3
Size: 231698 Color: 4

Bin 3011: 244 of cap free
Amount of items: 2
Items: 
Size: 573411 Color: 2
Size: 426346 Color: 0

Bin 3012: 244 of cap free
Amount of items: 2
Items: 
Size: 614264 Color: 1
Size: 385493 Color: 3

Bin 3013: 245 of cap free
Amount of items: 2
Items: 
Size: 683675 Color: 2
Size: 316081 Color: 3

Bin 3014: 245 of cap free
Amount of items: 2
Items: 
Size: 716361 Color: 1
Size: 283395 Color: 4

Bin 3015: 246 of cap free
Amount of items: 2
Items: 
Size: 524006 Color: 2
Size: 475749 Color: 1

Bin 3016: 246 of cap free
Amount of items: 2
Items: 
Size: 556938 Color: 1
Size: 442817 Color: 3

Bin 3017: 246 of cap free
Amount of items: 2
Items: 
Size: 785421 Color: 0
Size: 214334 Color: 2

Bin 3018: 246 of cap free
Amount of items: 2
Items: 
Size: 796709 Color: 3
Size: 203046 Color: 4

Bin 3019: 247 of cap free
Amount of items: 2
Items: 
Size: 622762 Color: 4
Size: 376992 Color: 2

Bin 3020: 247 of cap free
Amount of items: 2
Items: 
Size: 679472 Color: 1
Size: 320282 Color: 2

Bin 3021: 247 of cap free
Amount of items: 2
Items: 
Size: 718298 Color: 4
Size: 281456 Color: 3

Bin 3022: 247 of cap free
Amount of items: 2
Items: 
Size: 771580 Color: 0
Size: 228174 Color: 4

Bin 3023: 248 of cap free
Amount of items: 2
Items: 
Size: 502266 Color: 1
Size: 497487 Color: 4

Bin 3024: 248 of cap free
Amount of items: 2
Items: 
Size: 651283 Color: 3
Size: 348470 Color: 1

Bin 3025: 248 of cap free
Amount of items: 2
Items: 
Size: 793273 Color: 3
Size: 206480 Color: 0

Bin 3026: 249 of cap free
Amount of items: 2
Items: 
Size: 665065 Color: 4
Size: 334687 Color: 3

Bin 3027: 250 of cap free
Amount of items: 2
Items: 
Size: 515784 Color: 4
Size: 483967 Color: 1

Bin 3028: 250 of cap free
Amount of items: 2
Items: 
Size: 601154 Color: 2
Size: 398597 Color: 1

Bin 3029: 250 of cap free
Amount of items: 2
Items: 
Size: 613684 Color: 2
Size: 386067 Color: 3

Bin 3030: 250 of cap free
Amount of items: 2
Items: 
Size: 634683 Color: 2
Size: 365068 Color: 3

Bin 3031: 250 of cap free
Amount of items: 2
Items: 
Size: 776146 Color: 3
Size: 223605 Color: 1

Bin 3032: 251 of cap free
Amount of items: 2
Items: 
Size: 526148 Color: 1
Size: 473602 Color: 0

Bin 3033: 251 of cap free
Amount of items: 2
Items: 
Size: 698798 Color: 0
Size: 300952 Color: 1

Bin 3034: 251 of cap free
Amount of items: 2
Items: 
Size: 723659 Color: 3
Size: 276091 Color: 0

Bin 3035: 252 of cap free
Amount of items: 2
Items: 
Size: 510229 Color: 0
Size: 489520 Color: 2

Bin 3036: 252 of cap free
Amount of items: 2
Items: 
Size: 535508 Color: 2
Size: 464241 Color: 1

Bin 3037: 252 of cap free
Amount of items: 2
Items: 
Size: 607748 Color: 1
Size: 392001 Color: 3

Bin 3038: 252 of cap free
Amount of items: 2
Items: 
Size: 665315 Color: 4
Size: 334434 Color: 0

Bin 3039: 252 of cap free
Amount of items: 2
Items: 
Size: 674154 Color: 4
Size: 325595 Color: 3

Bin 3040: 252 of cap free
Amount of items: 2
Items: 
Size: 757237 Color: 3
Size: 242512 Color: 1

Bin 3041: 252 of cap free
Amount of items: 2
Items: 
Size: 790686 Color: 0
Size: 209063 Color: 2

Bin 3042: 253 of cap free
Amount of items: 2
Items: 
Size: 513360 Color: 0
Size: 486388 Color: 1

Bin 3043: 254 of cap free
Amount of items: 2
Items: 
Size: 733055 Color: 2
Size: 266692 Color: 3

Bin 3044: 255 of cap free
Amount of items: 2
Items: 
Size: 599787 Color: 3
Size: 399959 Color: 0

Bin 3045: 255 of cap free
Amount of items: 2
Items: 
Size: 625299 Color: 2
Size: 374447 Color: 4

Bin 3046: 255 of cap free
Amount of items: 2
Items: 
Size: 632584 Color: 2
Size: 367162 Color: 3

Bin 3047: 255 of cap free
Amount of items: 2
Items: 
Size: 717799 Color: 1
Size: 281947 Color: 3

Bin 3048: 255 of cap free
Amount of items: 2
Items: 
Size: 749750 Color: 3
Size: 249996 Color: 4

Bin 3049: 256 of cap free
Amount of items: 2
Items: 
Size: 522883 Color: 3
Size: 476862 Color: 2

Bin 3050: 256 of cap free
Amount of items: 2
Items: 
Size: 556343 Color: 3
Size: 443402 Color: 1

Bin 3051: 256 of cap free
Amount of items: 2
Items: 
Size: 593436 Color: 4
Size: 406309 Color: 3

Bin 3052: 256 of cap free
Amount of items: 2
Items: 
Size: 722266 Color: 1
Size: 277479 Color: 4

Bin 3053: 256 of cap free
Amount of items: 2
Items: 
Size: 786785 Color: 4
Size: 212960 Color: 2

Bin 3054: 256 of cap free
Amount of items: 2
Items: 
Size: 795420 Color: 4
Size: 204325 Color: 3

Bin 3055: 257 of cap free
Amount of items: 2
Items: 
Size: 541900 Color: 3
Size: 457844 Color: 1

Bin 3056: 257 of cap free
Amount of items: 2
Items: 
Size: 645401 Color: 2
Size: 354343 Color: 4

Bin 3057: 257 of cap free
Amount of items: 2
Items: 
Size: 728174 Color: 1
Size: 271570 Color: 4

Bin 3058: 257 of cap free
Amount of items: 2
Items: 
Size: 760890 Color: 3
Size: 238854 Color: 2

Bin 3059: 258 of cap free
Amount of items: 2
Items: 
Size: 500017 Color: 4
Size: 499726 Color: 2

Bin 3060: 258 of cap free
Amount of items: 2
Items: 
Size: 619849 Color: 1
Size: 379894 Color: 3

Bin 3061: 258 of cap free
Amount of items: 2
Items: 
Size: 652380 Color: 4
Size: 347363 Color: 1

Bin 3062: 259 of cap free
Amount of items: 2
Items: 
Size: 552862 Color: 1
Size: 446880 Color: 3

Bin 3063: 259 of cap free
Amount of items: 2
Items: 
Size: 622776 Color: 2
Size: 376966 Color: 1

Bin 3064: 259 of cap free
Amount of items: 2
Items: 
Size: 704902 Color: 4
Size: 294840 Color: 3

Bin 3065: 259 of cap free
Amount of items: 2
Items: 
Size: 711642 Color: 0
Size: 288100 Color: 2

Bin 3066: 260 of cap free
Amount of items: 2
Items: 
Size: 628325 Color: 3
Size: 371416 Color: 1

Bin 3067: 260 of cap free
Amount of items: 2
Items: 
Size: 646956 Color: 1
Size: 352785 Color: 0

Bin 3068: 260 of cap free
Amount of items: 2
Items: 
Size: 668997 Color: 4
Size: 330744 Color: 0

Bin 3069: 260 of cap free
Amount of items: 2
Items: 
Size: 742361 Color: 2
Size: 257380 Color: 1

Bin 3070: 260 of cap free
Amount of items: 2
Items: 
Size: 744720 Color: 1
Size: 255021 Color: 3

Bin 3071: 260 of cap free
Amount of items: 2
Items: 
Size: 789802 Color: 3
Size: 209939 Color: 1

Bin 3072: 261 of cap free
Amount of items: 2
Items: 
Size: 575105 Color: 1
Size: 424635 Color: 0

Bin 3073: 261 of cap free
Amount of items: 2
Items: 
Size: 776155 Color: 1
Size: 223585 Color: 2

Bin 3074: 262 of cap free
Amount of items: 2
Items: 
Size: 597824 Color: 1
Size: 401915 Color: 2

Bin 3075: 262 of cap free
Amount of items: 2
Items: 
Size: 615882 Color: 3
Size: 383857 Color: 0

Bin 3076: 262 of cap free
Amount of items: 2
Items: 
Size: 692874 Color: 3
Size: 306865 Color: 0

Bin 3077: 262 of cap free
Amount of items: 2
Items: 
Size: 741079 Color: 2
Size: 258660 Color: 1

Bin 3078: 263 of cap free
Amount of items: 2
Items: 
Size: 549090 Color: 0
Size: 450648 Color: 3

Bin 3079: 263 of cap free
Amount of items: 2
Items: 
Size: 568590 Color: 2
Size: 431148 Color: 4

Bin 3080: 263 of cap free
Amount of items: 2
Items: 
Size: 708346 Color: 1
Size: 291392 Color: 2

Bin 3081: 264 of cap free
Amount of items: 2
Items: 
Size: 517954 Color: 2
Size: 481783 Color: 0

Bin 3082: 264 of cap free
Amount of items: 2
Items: 
Size: 712898 Color: 3
Size: 286839 Color: 2

Bin 3083: 264 of cap free
Amount of items: 2
Items: 
Size: 741069 Color: 4
Size: 258668 Color: 2

Bin 3084: 265 of cap free
Amount of items: 7
Items: 
Size: 147557 Color: 2
Size: 147398 Color: 1
Size: 147377 Color: 0
Size: 147283 Color: 3
Size: 147232 Color: 2
Size: 147195 Color: 2
Size: 115694 Color: 4

Bin 3085: 265 of cap free
Amount of items: 2
Items: 
Size: 604793 Color: 2
Size: 394943 Color: 1

Bin 3086: 265 of cap free
Amount of items: 2
Items: 
Size: 678156 Color: 1
Size: 321580 Color: 4

Bin 3087: 265 of cap free
Amount of items: 2
Items: 
Size: 712076 Color: 2
Size: 287660 Color: 3

Bin 3088: 266 of cap free
Amount of items: 2
Items: 
Size: 512572 Color: 0
Size: 487163 Color: 2

Bin 3089: 266 of cap free
Amount of items: 2
Items: 
Size: 577788 Color: 2
Size: 421947 Color: 4

Bin 3090: 266 of cap free
Amount of items: 2
Items: 
Size: 753425 Color: 1
Size: 246310 Color: 2

Bin 3091: 267 of cap free
Amount of items: 2
Items: 
Size: 641563 Color: 4
Size: 358171 Color: 0

Bin 3092: 267 of cap free
Amount of items: 2
Items: 
Size: 716668 Color: 1
Size: 283066 Color: 3

Bin 3093: 268 of cap free
Amount of items: 2
Items: 
Size: 642277 Color: 0
Size: 357456 Color: 2

Bin 3094: 268 of cap free
Amount of items: 2
Items: 
Size: 662296 Color: 1
Size: 337437 Color: 2

Bin 3095: 268 of cap free
Amount of items: 2
Items: 
Size: 664588 Color: 4
Size: 335145 Color: 1

Bin 3096: 268 of cap free
Amount of items: 2
Items: 
Size: 672101 Color: 1
Size: 327632 Color: 2

Bin 3097: 268 of cap free
Amount of items: 2
Items: 
Size: 737780 Color: 2
Size: 261953 Color: 3

Bin 3098: 268 of cap free
Amount of items: 2
Items: 
Size: 785394 Color: 4
Size: 214339 Color: 0

Bin 3099: 269 of cap free
Amount of items: 2
Items: 
Size: 657159 Color: 1
Size: 342573 Color: 4

Bin 3100: 269 of cap free
Amount of items: 2
Items: 
Size: 681715 Color: 0
Size: 318017 Color: 1

Bin 3101: 269 of cap free
Amount of items: 2
Items: 
Size: 759162 Color: 4
Size: 240570 Color: 3

Bin 3102: 270 of cap free
Amount of items: 2
Items: 
Size: 508971 Color: 0
Size: 490760 Color: 1

Bin 3103: 270 of cap free
Amount of items: 2
Items: 
Size: 561069 Color: 0
Size: 438662 Color: 4

Bin 3104: 270 of cap free
Amount of items: 2
Items: 
Size: 571037 Color: 4
Size: 428694 Color: 1

Bin 3105: 270 of cap free
Amount of items: 2
Items: 
Size: 586118 Color: 1
Size: 413613 Color: 4

Bin 3106: 270 of cap free
Amount of items: 2
Items: 
Size: 605548 Color: 1
Size: 394183 Color: 4

Bin 3107: 270 of cap free
Amount of items: 2
Items: 
Size: 738663 Color: 1
Size: 261068 Color: 4

Bin 3108: 270 of cap free
Amount of items: 2
Items: 
Size: 764770 Color: 4
Size: 234961 Color: 2

Bin 3109: 272 of cap free
Amount of items: 2
Items: 
Size: 602526 Color: 1
Size: 397203 Color: 3

Bin 3110: 272 of cap free
Amount of items: 2
Items: 
Size: 708331 Color: 3
Size: 291398 Color: 1

Bin 3111: 272 of cap free
Amount of items: 2
Items: 
Size: 770664 Color: 2
Size: 229065 Color: 3

Bin 3112: 273 of cap free
Amount of items: 2
Items: 
Size: 789710 Color: 1
Size: 210018 Color: 3

Bin 3113: 274 of cap free
Amount of items: 2
Items: 
Size: 724867 Color: 4
Size: 274860 Color: 3

Bin 3114: 275 of cap free
Amount of items: 2
Items: 
Size: 510082 Color: 1
Size: 489644 Color: 0

Bin 3115: 275 of cap free
Amount of items: 2
Items: 
Size: 739445 Color: 1
Size: 260281 Color: 4

Bin 3116: 276 of cap free
Amount of items: 2
Items: 
Size: 575484 Color: 0
Size: 424241 Color: 1

Bin 3117: 276 of cap free
Amount of items: 2
Items: 
Size: 582637 Color: 4
Size: 417088 Color: 2

Bin 3118: 276 of cap free
Amount of items: 2
Items: 
Size: 784938 Color: 0
Size: 214787 Color: 2

Bin 3119: 277 of cap free
Amount of items: 2
Items: 
Size: 778943 Color: 0
Size: 220781 Color: 1

Bin 3120: 279 of cap free
Amount of items: 2
Items: 
Size: 541070 Color: 2
Size: 458652 Color: 3

Bin 3121: 279 of cap free
Amount of items: 2
Items: 
Size: 550325 Color: 3
Size: 449397 Color: 4

Bin 3122: 279 of cap free
Amount of items: 2
Items: 
Size: 701209 Color: 3
Size: 298513 Color: 2

Bin 3123: 279 of cap free
Amount of items: 2
Items: 
Size: 729824 Color: 3
Size: 269898 Color: 1

Bin 3124: 280 of cap free
Amount of items: 2
Items: 
Size: 507928 Color: 4
Size: 491793 Color: 0

Bin 3125: 280 of cap free
Amount of items: 2
Items: 
Size: 612041 Color: 3
Size: 387680 Color: 1

Bin 3126: 280 of cap free
Amount of items: 2
Items: 
Size: 617849 Color: 2
Size: 381872 Color: 3

Bin 3127: 281 of cap free
Amount of items: 2
Items: 
Size: 506060 Color: 2
Size: 493660 Color: 4

Bin 3128: 281 of cap free
Amount of items: 2
Items: 
Size: 693946 Color: 3
Size: 305774 Color: 4

Bin 3129: 281 of cap free
Amount of items: 2
Items: 
Size: 713206 Color: 2
Size: 286514 Color: 1

Bin 3130: 282 of cap free
Amount of items: 2
Items: 
Size: 620360 Color: 3
Size: 379359 Color: 4

Bin 3131: 282 of cap free
Amount of items: 2
Items: 
Size: 775563 Color: 0
Size: 224156 Color: 3

Bin 3132: 283 of cap free
Amount of items: 2
Items: 
Size: 506623 Color: 1
Size: 493095 Color: 0

Bin 3133: 283 of cap free
Amount of items: 2
Items: 
Size: 587984 Color: 1
Size: 411734 Color: 2

Bin 3134: 284 of cap free
Amount of items: 2
Items: 
Size: 727554 Color: 2
Size: 272163 Color: 1

Bin 3135: 285 of cap free
Amount of items: 2
Items: 
Size: 571746 Color: 1
Size: 427970 Color: 2

Bin 3136: 285 of cap free
Amount of items: 2
Items: 
Size: 677068 Color: 0
Size: 322648 Color: 2

Bin 3137: 285 of cap free
Amount of items: 2
Items: 
Size: 799471 Color: 3
Size: 200245 Color: 1

Bin 3138: 286 of cap free
Amount of items: 2
Items: 
Size: 700478 Color: 3
Size: 299237 Color: 0

Bin 3139: 287 of cap free
Amount of items: 2
Items: 
Size: 520789 Color: 1
Size: 478925 Color: 0

Bin 3140: 287 of cap free
Amount of items: 2
Items: 
Size: 655865 Color: 4
Size: 343849 Color: 1

Bin 3141: 287 of cap free
Amount of items: 2
Items: 
Size: 692529 Color: 4
Size: 307185 Color: 1

Bin 3142: 287 of cap free
Amount of items: 2
Items: 
Size: 725310 Color: 0
Size: 274404 Color: 3

Bin 3143: 287 of cap free
Amount of items: 2
Items: 
Size: 744749 Color: 3
Size: 254965 Color: 1

Bin 3144: 287 of cap free
Amount of items: 2
Items: 
Size: 788570 Color: 2
Size: 211144 Color: 4

Bin 3145: 288 of cap free
Amount of items: 2
Items: 
Size: 642901 Color: 1
Size: 356812 Color: 0

Bin 3146: 289 of cap free
Amount of items: 2
Items: 
Size: 539903 Color: 2
Size: 459809 Color: 4

Bin 3147: 289 of cap free
Amount of items: 2
Items: 
Size: 604249 Color: 4
Size: 395463 Color: 2

Bin 3148: 289 of cap free
Amount of items: 2
Items: 
Size: 663572 Color: 1
Size: 336140 Color: 2

Bin 3149: 289 of cap free
Amount of items: 2
Items: 
Size: 753402 Color: 3
Size: 246310 Color: 0

Bin 3150: 290 of cap free
Amount of items: 2
Items: 
Size: 514558 Color: 1
Size: 485153 Color: 4

Bin 3151: 291 of cap free
Amount of items: 2
Items: 
Size: 744271 Color: 1
Size: 255439 Color: 0

Bin 3152: 292 of cap free
Amount of items: 2
Items: 
Size: 571348 Color: 2
Size: 428361 Color: 3

Bin 3153: 293 of cap free
Amount of items: 2
Items: 
Size: 528690 Color: 4
Size: 471018 Color: 0

Bin 3154: 293 of cap free
Amount of items: 2
Items: 
Size: 578715 Color: 1
Size: 420993 Color: 4

Bin 3155: 293 of cap free
Amount of items: 2
Items: 
Size: 752200 Color: 0
Size: 247508 Color: 3

Bin 3156: 294 of cap free
Amount of items: 3
Items: 
Size: 630938 Color: 1
Size: 195997 Color: 2
Size: 172772 Color: 0

Bin 3157: 295 of cap free
Amount of items: 2
Items: 
Size: 671660 Color: 3
Size: 328046 Color: 1

Bin 3158: 295 of cap free
Amount of items: 2
Items: 
Size: 722679 Color: 0
Size: 277027 Color: 1

Bin 3159: 295 of cap free
Amount of items: 2
Items: 
Size: 726757 Color: 4
Size: 272949 Color: 3

Bin 3160: 296 of cap free
Amount of items: 2
Items: 
Size: 592861 Color: 1
Size: 406844 Color: 2

Bin 3161: 296 of cap free
Amount of items: 2
Items: 
Size: 764463 Color: 1
Size: 235242 Color: 0

Bin 3162: 297 of cap free
Amount of items: 2
Items: 
Size: 692224 Color: 0
Size: 307480 Color: 3

Bin 3163: 298 of cap free
Amount of items: 2
Items: 
Size: 556543 Color: 1
Size: 443160 Color: 2

Bin 3164: 298 of cap free
Amount of items: 2
Items: 
Size: 688208 Color: 2
Size: 311495 Color: 0

Bin 3165: 298 of cap free
Amount of items: 2
Items: 
Size: 745752 Color: 4
Size: 253951 Color: 3

Bin 3166: 299 of cap free
Amount of items: 2
Items: 
Size: 578701 Color: 2
Size: 421001 Color: 1

Bin 3167: 299 of cap free
Amount of items: 2
Items: 
Size: 630984 Color: 1
Size: 368718 Color: 0

Bin 3168: 300 of cap free
Amount of items: 2
Items: 
Size: 529406 Color: 1
Size: 470295 Color: 3

Bin 3169: 300 of cap free
Amount of items: 2
Items: 
Size: 586616 Color: 2
Size: 413085 Color: 4

Bin 3170: 300 of cap free
Amount of items: 2
Items: 
Size: 774667 Color: 0
Size: 225034 Color: 1

Bin 3171: 301 of cap free
Amount of items: 2
Items: 
Size: 723618 Color: 2
Size: 276082 Color: 0

Bin 3172: 302 of cap free
Amount of items: 2
Items: 
Size: 513329 Color: 2
Size: 486370 Color: 4

Bin 3173: 302 of cap free
Amount of items: 2
Items: 
Size: 537006 Color: 4
Size: 462693 Color: 0

Bin 3174: 302 of cap free
Amount of items: 2
Items: 
Size: 649166 Color: 2
Size: 350533 Color: 0

Bin 3175: 302 of cap free
Amount of items: 2
Items: 
Size: 674000 Color: 0
Size: 325699 Color: 4

Bin 3176: 302 of cap free
Amount of items: 2
Items: 
Size: 780077 Color: 0
Size: 219622 Color: 1

Bin 3177: 302 of cap free
Amount of items: 2
Items: 
Size: 787054 Color: 4
Size: 212645 Color: 3

Bin 3178: 303 of cap free
Amount of items: 2
Items: 
Size: 558384 Color: 3
Size: 441314 Color: 0

Bin 3179: 303 of cap free
Amount of items: 2
Items: 
Size: 595282 Color: 3
Size: 404416 Color: 0

Bin 3180: 304 of cap free
Amount of items: 2
Items: 
Size: 545049 Color: 4
Size: 454648 Color: 1

Bin 3181: 304 of cap free
Amount of items: 2
Items: 
Size: 616347 Color: 0
Size: 383350 Color: 3

Bin 3182: 304 of cap free
Amount of items: 2
Items: 
Size: 651215 Color: 1
Size: 348482 Color: 3

Bin 3183: 304 of cap free
Amount of items: 2
Items: 
Size: 724250 Color: 1
Size: 275447 Color: 3

Bin 3184: 305 of cap free
Amount of items: 2
Items: 
Size: 530602 Color: 1
Size: 469094 Color: 3

Bin 3185: 305 of cap free
Amount of items: 2
Items: 
Size: 616346 Color: 1
Size: 383350 Color: 2

Bin 3186: 305 of cap free
Amount of items: 2
Items: 
Size: 646937 Color: 3
Size: 352759 Color: 0

Bin 3187: 305 of cap free
Amount of items: 2
Items: 
Size: 647620 Color: 0
Size: 352076 Color: 3

Bin 3188: 305 of cap free
Amount of items: 2
Items: 
Size: 658912 Color: 2
Size: 340784 Color: 0

Bin 3189: 305 of cap free
Amount of items: 2
Items: 
Size: 714850 Color: 0
Size: 284846 Color: 1

Bin 3190: 306 of cap free
Amount of items: 2
Items: 
Size: 635607 Color: 2
Size: 364088 Color: 3

Bin 3191: 307 of cap free
Amount of items: 2
Items: 
Size: 514109 Color: 1
Size: 485585 Color: 3

Bin 3192: 307 of cap free
Amount of items: 2
Items: 
Size: 521904 Color: 3
Size: 477790 Color: 1

Bin 3193: 307 of cap free
Amount of items: 2
Items: 
Size: 597293 Color: 3
Size: 402401 Color: 2

Bin 3194: 307 of cap free
Amount of items: 2
Items: 
Size: 627415 Color: 4
Size: 372279 Color: 0

Bin 3195: 307 of cap free
Amount of items: 2
Items: 
Size: 650393 Color: 3
Size: 349301 Color: 0

Bin 3196: 308 of cap free
Amount of items: 2
Items: 
Size: 652959 Color: 4
Size: 346734 Color: 0

Bin 3197: 309 of cap free
Amount of items: 2
Items: 
Size: 575969 Color: 0
Size: 423723 Color: 1

Bin 3198: 309 of cap free
Amount of items: 2
Items: 
Size: 662298 Color: 2
Size: 337394 Color: 4

Bin 3199: 309 of cap free
Amount of items: 2
Items: 
Size: 673447 Color: 1
Size: 326245 Color: 4

Bin 3200: 310 of cap free
Amount of items: 2
Items: 
Size: 707230 Color: 4
Size: 292461 Color: 3

Bin 3201: 311 of cap free
Amount of items: 2
Items: 
Size: 704911 Color: 3
Size: 294779 Color: 0

Bin 3202: 311 of cap free
Amount of items: 2
Items: 
Size: 780704 Color: 2
Size: 218986 Color: 0

Bin 3203: 312 of cap free
Amount of items: 2
Items: 
Size: 702599 Color: 4
Size: 297090 Color: 1

Bin 3204: 312 of cap free
Amount of items: 2
Items: 
Size: 728141 Color: 2
Size: 271548 Color: 1

Bin 3205: 313 of cap free
Amount of items: 2
Items: 
Size: 563776 Color: 2
Size: 435912 Color: 1

Bin 3206: 313 of cap free
Amount of items: 2
Items: 
Size: 663959 Color: 1
Size: 335729 Color: 4

Bin 3207: 314 of cap free
Amount of items: 2
Items: 
Size: 523180 Color: 3
Size: 476507 Color: 1

Bin 3208: 314 of cap free
Amount of items: 2
Items: 
Size: 714162 Color: 1
Size: 285525 Color: 3

Bin 3209: 315 of cap free
Amount of items: 2
Items: 
Size: 543767 Color: 2
Size: 455919 Color: 3

Bin 3210: 315 of cap free
Amount of items: 2
Items: 
Size: 674538 Color: 1
Size: 325148 Color: 4

Bin 3211: 317 of cap free
Amount of items: 2
Items: 
Size: 596099 Color: 2
Size: 403585 Color: 0

Bin 3212: 317 of cap free
Amount of items: 2
Items: 
Size: 627765 Color: 0
Size: 371919 Color: 3

Bin 3213: 317 of cap free
Amount of items: 2
Items: 
Size: 678131 Color: 2
Size: 321553 Color: 3

Bin 3214: 318 of cap free
Amount of items: 2
Items: 
Size: 522458 Color: 1
Size: 477225 Color: 4

Bin 3215: 318 of cap free
Amount of items: 2
Items: 
Size: 616339 Color: 4
Size: 383344 Color: 2

Bin 3216: 318 of cap free
Amount of items: 2
Items: 
Size: 682554 Color: 2
Size: 317129 Color: 0

Bin 3217: 318 of cap free
Amount of items: 2
Items: 
Size: 797133 Color: 0
Size: 202550 Color: 3

Bin 3218: 319 of cap free
Amount of items: 2
Items: 
Size: 555916 Color: 3
Size: 443766 Color: 2

Bin 3219: 319 of cap free
Amount of items: 2
Items: 
Size: 589899 Color: 4
Size: 409783 Color: 2

Bin 3220: 319 of cap free
Amount of items: 2
Items: 
Size: 795688 Color: 0
Size: 203994 Color: 4

Bin 3221: 320 of cap free
Amount of items: 2
Items: 
Size: 527342 Color: 3
Size: 472339 Color: 2

Bin 3222: 320 of cap free
Amount of items: 2
Items: 
Size: 529995 Color: 4
Size: 469686 Color: 0

Bin 3223: 320 of cap free
Amount of items: 2
Items: 
Size: 584661 Color: 3
Size: 415020 Color: 2

Bin 3224: 320 of cap free
Amount of items: 2
Items: 
Size: 706612 Color: 1
Size: 293069 Color: 3

Bin 3225: 321 of cap free
Amount of items: 2
Items: 
Size: 589939 Color: 2
Size: 409741 Color: 1

Bin 3226: 321 of cap free
Amount of items: 2
Items: 
Size: 605821 Color: 0
Size: 393859 Color: 4

Bin 3227: 322 of cap free
Amount of items: 2
Items: 
Size: 592894 Color: 2
Size: 406785 Color: 1

Bin 3228: 322 of cap free
Amount of items: 2
Items: 
Size: 598352 Color: 4
Size: 401327 Color: 1

Bin 3229: 322 of cap free
Amount of items: 2
Items: 
Size: 703539 Color: 2
Size: 296140 Color: 4

Bin 3230: 324 of cap free
Amount of items: 2
Items: 
Size: 574262 Color: 1
Size: 425415 Color: 0

Bin 3231: 326 of cap free
Amount of items: 2
Items: 
Size: 517924 Color: 2
Size: 481751 Color: 0

Bin 3232: 326 of cap free
Amount of items: 2
Items: 
Size: 533438 Color: 4
Size: 466237 Color: 2

Bin 3233: 326 of cap free
Amount of items: 2
Items: 
Size: 695164 Color: 2
Size: 304511 Color: 0

Bin 3234: 327 of cap free
Amount of items: 2
Items: 
Size: 569528 Color: 0
Size: 430146 Color: 3

Bin 3235: 328 of cap free
Amount of items: 2
Items: 
Size: 552797 Color: 2
Size: 446876 Color: 0

Bin 3236: 328 of cap free
Amount of items: 2
Items: 
Size: 676653 Color: 1
Size: 323020 Color: 2

Bin 3237: 328 of cap free
Amount of items: 2
Items: 
Size: 747453 Color: 4
Size: 252220 Color: 3

Bin 3238: 329 of cap free
Amount of items: 2
Items: 
Size: 569936 Color: 1
Size: 429736 Color: 4

Bin 3239: 329 of cap free
Amount of items: 2
Items: 
Size: 609257 Color: 1
Size: 390415 Color: 4

Bin 3240: 329 of cap free
Amount of items: 2
Items: 
Size: 751438 Color: 0
Size: 248234 Color: 2

Bin 3241: 330 of cap free
Amount of items: 2
Items: 
Size: 634335 Color: 2
Size: 365336 Color: 4

Bin 3242: 332 of cap free
Amount of items: 2
Items: 
Size: 618288 Color: 1
Size: 381381 Color: 3

Bin 3243: 332 of cap free
Amount of items: 2
Items: 
Size: 709434 Color: 1
Size: 290235 Color: 2

Bin 3244: 333 of cap free
Amount of items: 2
Items: 
Size: 511019 Color: 1
Size: 488649 Color: 0

Bin 3245: 333 of cap free
Amount of items: 2
Items: 
Size: 586059 Color: 4
Size: 413609 Color: 2

Bin 3246: 333 of cap free
Amount of items: 2
Items: 
Size: 689312 Color: 4
Size: 310356 Color: 1

Bin 3247: 333 of cap free
Amount of items: 2
Items: 
Size: 782191 Color: 1
Size: 217477 Color: 4

Bin 3248: 334 of cap free
Amount of items: 2
Items: 
Size: 507875 Color: 1
Size: 491792 Color: 2

Bin 3249: 334 of cap free
Amount of items: 2
Items: 
Size: 524618 Color: 3
Size: 475049 Color: 4

Bin 3250: 334 of cap free
Amount of items: 2
Items: 
Size: 557678 Color: 2
Size: 441989 Color: 3

Bin 3251: 334 of cap free
Amount of items: 2
Items: 
Size: 603797 Color: 1
Size: 395870 Color: 0

Bin 3252: 335 of cap free
Amount of items: 2
Items: 
Size: 586418 Color: 1
Size: 413248 Color: 2

Bin 3253: 335 of cap free
Amount of items: 2
Items: 
Size: 589078 Color: 3
Size: 410588 Color: 0

Bin 3254: 335 of cap free
Amount of items: 2
Items: 
Size: 646563 Color: 3
Size: 353103 Color: 4

Bin 3255: 335 of cap free
Amount of items: 2
Items: 
Size: 664391 Color: 2
Size: 335275 Color: 4

Bin 3256: 335 of cap free
Amount of items: 2
Items: 
Size: 721546 Color: 3
Size: 278120 Color: 2

Bin 3257: 337 of cap free
Amount of items: 7
Items: 
Size: 149956 Color: 2
Size: 149772 Color: 2
Size: 149582 Color: 0
Size: 149524 Color: 4
Size: 149341 Color: 1
Size: 149280 Color: 3
Size: 102209 Color: 3

Bin 3258: 337 of cap free
Amount of items: 2
Items: 
Size: 713525 Color: 0
Size: 286139 Color: 2

Bin 3259: 338 of cap free
Amount of items: 2
Items: 
Size: 564438 Color: 1
Size: 435225 Color: 4

Bin 3260: 339 of cap free
Amount of items: 2
Items: 
Size: 581546 Color: 4
Size: 418116 Color: 0

Bin 3261: 340 of cap free
Amount of items: 2
Items: 
Size: 566113 Color: 3
Size: 433548 Color: 2

Bin 3262: 341 of cap free
Amount of items: 2
Items: 
Size: 500336 Color: 2
Size: 499324 Color: 4

Bin 3263: 342 of cap free
Amount of items: 6
Items: 
Size: 168849 Color: 4
Size: 168811 Color: 2
Size: 168750 Color: 1
Size: 168701 Color: 4
Size: 168682 Color: 2
Size: 155866 Color: 4

Bin 3264: 342 of cap free
Amount of items: 2
Items: 
Size: 648206 Color: 2
Size: 351453 Color: 3

Bin 3265: 342 of cap free
Amount of items: 2
Items: 
Size: 756698 Color: 4
Size: 242961 Color: 1

Bin 3266: 343 of cap free
Amount of items: 2
Items: 
Size: 554257 Color: 1
Size: 445401 Color: 0

Bin 3267: 343 of cap free
Amount of items: 2
Items: 
Size: 617789 Color: 2
Size: 381869 Color: 0

Bin 3268: 343 of cap free
Amount of items: 2
Items: 
Size: 797390 Color: 3
Size: 202268 Color: 2

Bin 3269: 344 of cap free
Amount of items: 2
Items: 
Size: 549141 Color: 3
Size: 450516 Color: 4

Bin 3270: 344 of cap free
Amount of items: 2
Items: 
Size: 655435 Color: 1
Size: 344222 Color: 3

Bin 3271: 344 of cap free
Amount of items: 2
Items: 
Size: 661170 Color: 0
Size: 338487 Color: 1

Bin 3272: 344 of cap free
Amount of items: 2
Items: 
Size: 681008 Color: 1
Size: 318649 Color: 4

Bin 3273: 344 of cap free
Amount of items: 2
Items: 
Size: 696760 Color: 1
Size: 302897 Color: 2

Bin 3274: 344 of cap free
Amount of items: 2
Items: 
Size: 714097 Color: 0
Size: 285560 Color: 1

Bin 3275: 344 of cap free
Amount of items: 2
Items: 
Size: 769098 Color: 0
Size: 230559 Color: 1

Bin 3276: 347 of cap free
Amount of items: 2
Items: 
Size: 576666 Color: 3
Size: 422988 Color: 2

Bin 3277: 347 of cap free
Amount of items: 2
Items: 
Size: 616946 Color: 2
Size: 382708 Color: 0

Bin 3278: 347 of cap free
Amount of items: 2
Items: 
Size: 644429 Color: 2
Size: 355225 Color: 4

Bin 3279: 347 of cap free
Amount of items: 2
Items: 
Size: 742643 Color: 4
Size: 257011 Color: 0

Bin 3280: 348 of cap free
Amount of items: 2
Items: 
Size: 692176 Color: 0
Size: 307477 Color: 3

Bin 3281: 348 of cap free
Amount of items: 2
Items: 
Size: 774646 Color: 1
Size: 225007 Color: 4

Bin 3282: 349 of cap free
Amount of items: 2
Items: 
Size: 630203 Color: 1
Size: 369449 Color: 4

Bin 3283: 350 of cap free
Amount of items: 2
Items: 
Size: 527330 Color: 4
Size: 472321 Color: 2

Bin 3284: 350 of cap free
Amount of items: 2
Items: 
Size: 612633 Color: 3
Size: 387018 Color: 2

Bin 3285: 350 of cap free
Amount of items: 2
Items: 
Size: 648190 Color: 3
Size: 351461 Color: 2

Bin 3286: 350 of cap free
Amount of items: 2
Items: 
Size: 753796 Color: 1
Size: 245855 Color: 0

Bin 3287: 351 of cap free
Amount of items: 2
Items: 
Size: 634316 Color: 4
Size: 365334 Color: 3

Bin 3288: 351 of cap free
Amount of items: 2
Items: 
Size: 642738 Color: 3
Size: 356912 Color: 1

Bin 3289: 351 of cap free
Amount of items: 2
Items: 
Size: 693934 Color: 3
Size: 305716 Color: 4

Bin 3290: 352 of cap free
Amount of items: 2
Items: 
Size: 640762 Color: 1
Size: 358887 Color: 2

Bin 3291: 352 of cap free
Amount of items: 2
Items: 
Size: 648570 Color: 1
Size: 351079 Color: 2

Bin 3292: 352 of cap free
Amount of items: 2
Items: 
Size: 705962 Color: 4
Size: 293687 Color: 2

Bin 3293: 354 of cap free
Amount of items: 2
Items: 
Size: 529992 Color: 2
Size: 469655 Color: 0

Bin 3294: 354 of cap free
Amount of items: 2
Items: 
Size: 611618 Color: 2
Size: 388029 Color: 4

Bin 3295: 354 of cap free
Amount of items: 2
Items: 
Size: 779669 Color: 0
Size: 219978 Color: 1

Bin 3296: 355 of cap free
Amount of items: 2
Items: 
Size: 739800 Color: 1
Size: 259846 Color: 3

Bin 3297: 355 of cap free
Amount of items: 2
Items: 
Size: 771510 Color: 4
Size: 228136 Color: 3

Bin 3298: 357 of cap free
Amount of items: 2
Items: 
Size: 549086 Color: 1
Size: 450558 Color: 3

Bin 3299: 359 of cap free
Amount of items: 2
Items: 
Size: 584633 Color: 1
Size: 415009 Color: 3

Bin 3300: 359 of cap free
Amount of items: 2
Items: 
Size: 650002 Color: 4
Size: 349640 Color: 2

Bin 3301: 359 of cap free
Amount of items: 2
Items: 
Size: 746734 Color: 3
Size: 252908 Color: 2

Bin 3302: 360 of cap free
Amount of items: 2
Items: 
Size: 580431 Color: 4
Size: 419210 Color: 2

Bin 3303: 361 of cap free
Amount of items: 2
Items: 
Size: 503524 Color: 3
Size: 496116 Color: 0

Bin 3304: 361 of cap free
Amount of items: 2
Items: 
Size: 519897 Color: 2
Size: 479743 Color: 3

Bin 3305: 361 of cap free
Amount of items: 2
Items: 
Size: 628269 Color: 3
Size: 371371 Color: 4

Bin 3306: 361 of cap free
Amount of items: 2
Items: 
Size: 748660 Color: 2
Size: 250980 Color: 1

Bin 3307: 361 of cap free
Amount of items: 2
Items: 
Size: 750198 Color: 3
Size: 249442 Color: 1

Bin 3308: 363 of cap free
Amount of items: 2
Items: 
Size: 535465 Color: 1
Size: 464173 Color: 3

Bin 3309: 363 of cap free
Amount of items: 2
Items: 
Size: 752658 Color: 2
Size: 246980 Color: 3

Bin 3310: 364 of cap free
Amount of items: 2
Items: 
Size: 544528 Color: 3
Size: 455109 Color: 4

Bin 3311: 364 of cap free
Amount of items: 2
Items: 
Size: 648626 Color: 2
Size: 351011 Color: 4

Bin 3312: 364 of cap free
Amount of items: 2
Items: 
Size: 680566 Color: 2
Size: 319071 Color: 4

Bin 3313: 365 of cap free
Amount of items: 2
Items: 
Size: 530590 Color: 2
Size: 469046 Color: 0

Bin 3314: 365 of cap free
Amount of items: 2
Items: 
Size: 673441 Color: 4
Size: 326195 Color: 0

Bin 3315: 365 of cap free
Amount of items: 2
Items: 
Size: 782189 Color: 4
Size: 217447 Color: 3

Bin 3316: 366 of cap free
Amount of items: 2
Items: 
Size: 593446 Color: 3
Size: 406189 Color: 1

Bin 3317: 368 of cap free
Amount of items: 2
Items: 
Size: 688376 Color: 0
Size: 311257 Color: 4

Bin 3318: 369 of cap free
Amount of items: 2
Items: 
Size: 742215 Color: 3
Size: 257417 Color: 2

Bin 3319: 370 of cap free
Amount of items: 2
Items: 
Size: 608227 Color: 3
Size: 391404 Color: 1

Bin 3320: 370 of cap free
Amount of items: 2
Items: 
Size: 618228 Color: 0
Size: 381403 Color: 1

Bin 3321: 370 of cap free
Amount of items: 2
Items: 
Size: 620508 Color: 4
Size: 379123 Color: 2

Bin 3322: 370 of cap free
Amount of items: 2
Items: 
Size: 794377 Color: 0
Size: 205254 Color: 3

Bin 3323: 371 of cap free
Amount of items: 2
Items: 
Size: 510023 Color: 4
Size: 489607 Color: 0

Bin 3324: 371 of cap free
Amount of items: 2
Items: 
Size: 532564 Color: 1
Size: 467066 Color: 2

Bin 3325: 371 of cap free
Amount of items: 2
Items: 
Size: 561676 Color: 0
Size: 437954 Color: 2

Bin 3326: 371 of cap free
Amount of items: 2
Items: 
Size: 683601 Color: 3
Size: 316029 Color: 0

Bin 3327: 371 of cap free
Amount of items: 2
Items: 
Size: 708641 Color: 2
Size: 290989 Color: 3

Bin 3328: 371 of cap free
Amount of items: 2
Items: 
Size: 750197 Color: 1
Size: 249433 Color: 3

Bin 3329: 372 of cap free
Amount of items: 2
Items: 
Size: 591692 Color: 0
Size: 407937 Color: 1

Bin 3330: 373 of cap free
Amount of items: 2
Items: 
Size: 514538 Color: 1
Size: 485090 Color: 4

Bin 3331: 373 of cap free
Amount of items: 2
Items: 
Size: 765044 Color: 0
Size: 234584 Color: 4

Bin 3332: 374 of cap free
Amount of items: 2
Items: 
Size: 556875 Color: 2
Size: 442752 Color: 1

Bin 3333: 374 of cap free
Amount of items: 2
Items: 
Size: 582573 Color: 3
Size: 417054 Color: 4

Bin 3334: 374 of cap free
Amount of items: 2
Items: 
Size: 624841 Color: 0
Size: 374786 Color: 2

Bin 3335: 375 of cap free
Amount of items: 2
Items: 
Size: 566611 Color: 0
Size: 433015 Color: 4

Bin 3336: 375 of cap free
Amount of items: 2
Items: 
Size: 601068 Color: 4
Size: 398558 Color: 1

Bin 3337: 376 of cap free
Amount of items: 2
Items: 
Size: 507868 Color: 0
Size: 491757 Color: 1

Bin 3338: 378 of cap free
Amount of items: 2
Items: 
Size: 623904 Color: 2
Size: 375719 Color: 3

Bin 3339: 378 of cap free
Amount of items: 2
Items: 
Size: 681665 Color: 2
Size: 317958 Color: 0

Bin 3340: 378 of cap free
Amount of items: 2
Items: 
Size: 798840 Color: 3
Size: 200783 Color: 2

Bin 3341: 380 of cap free
Amount of items: 2
Items: 
Size: 693919 Color: 3
Size: 305702 Color: 2

Bin 3342: 381 of cap free
Amount of items: 2
Items: 
Size: 667983 Color: 0
Size: 331637 Color: 1

Bin 3343: 382 of cap free
Amount of items: 2
Items: 
Size: 534264 Color: 2
Size: 465355 Color: 0

Bin 3344: 383 of cap free
Amount of items: 2
Items: 
Size: 649980 Color: 3
Size: 349638 Color: 2

Bin 3345: 383 of cap free
Amount of items: 2
Items: 
Size: 776588 Color: 3
Size: 223030 Color: 2

Bin 3346: 383 of cap free
Amount of items: 2
Items: 
Size: 792454 Color: 1
Size: 207164 Color: 0

Bin 3347: 384 of cap free
Amount of items: 2
Items: 
Size: 681692 Color: 0
Size: 317925 Color: 3

Bin 3348: 384 of cap free
Amount of items: 2
Items: 
Size: 774650 Color: 4
Size: 224967 Color: 2

Bin 3349: 384 of cap free
Amount of items: 2
Items: 
Size: 776091 Color: 0
Size: 223526 Color: 4

Bin 3350: 385 of cap free
Amount of items: 2
Items: 
Size: 598332 Color: 3
Size: 401284 Color: 2

Bin 3351: 385 of cap free
Amount of items: 2
Items: 
Size: 737755 Color: 4
Size: 261861 Color: 3

Bin 3352: 386 of cap free
Amount of items: 2
Items: 
Size: 523879 Color: 4
Size: 475736 Color: 0

Bin 3353: 386 of cap free
Amount of items: 2
Items: 
Size: 544493 Color: 2
Size: 455122 Color: 3

Bin 3354: 386 of cap free
Amount of items: 2
Items: 
Size: 735259 Color: 1
Size: 264356 Color: 3

Bin 3355: 387 of cap free
Amount of items: 2
Items: 
Size: 586559 Color: 2
Size: 413055 Color: 3

Bin 3356: 387 of cap free
Amount of items: 2
Items: 
Size: 610575 Color: 0
Size: 389039 Color: 3

Bin 3357: 388 of cap free
Amount of items: 2
Items: 
Size: 754804 Color: 3
Size: 244809 Color: 0

Bin 3358: 388 of cap free
Amount of items: 2
Items: 
Size: 762895 Color: 0
Size: 236718 Color: 1

Bin 3359: 389 of cap free
Amount of items: 2
Items: 
Size: 628254 Color: 2
Size: 371358 Color: 3

Bin 3360: 391 of cap free
Amount of items: 2
Items: 
Size: 601612 Color: 2
Size: 397998 Color: 1

Bin 3361: 391 of cap free
Amount of items: 2
Items: 
Size: 660673 Color: 2
Size: 338937 Color: 3

Bin 3362: 391 of cap free
Amount of items: 2
Items: 
Size: 727514 Color: 2
Size: 272096 Color: 4

Bin 3363: 391 of cap free
Amount of items: 2
Items: 
Size: 797791 Color: 1
Size: 201819 Color: 0

Bin 3364: 392 of cap free
Amount of items: 2
Items: 
Size: 759143 Color: 1
Size: 240466 Color: 0

Bin 3365: 393 of cap free
Amount of items: 2
Items: 
Size: 674087 Color: 4
Size: 325521 Color: 3

Bin 3366: 394 of cap free
Amount of items: 2
Items: 
Size: 764949 Color: 2
Size: 234658 Color: 0

Bin 3367: 395 of cap free
Amount of items: 2
Items: 
Size: 700385 Color: 4
Size: 299221 Color: 0

Bin 3368: 396 of cap free
Amount of items: 2
Items: 
Size: 639427 Color: 0
Size: 360178 Color: 4

Bin 3369: 396 of cap free
Amount of items: 2
Items: 
Size: 731749 Color: 0
Size: 267856 Color: 3

Bin 3370: 397 of cap free
Amount of items: 2
Items: 
Size: 774638 Color: 0
Size: 224966 Color: 3

Bin 3371: 398 of cap free
Amount of items: 2
Items: 
Size: 537977 Color: 1
Size: 461626 Color: 0

Bin 3372: 398 of cap free
Amount of items: 2
Items: 
Size: 565157 Color: 3
Size: 434446 Color: 4

Bin 3373: 398 of cap free
Amount of items: 2
Items: 
Size: 734615 Color: 3
Size: 264988 Color: 0

Bin 3374: 399 of cap free
Amount of items: 2
Items: 
Size: 598931 Color: 2
Size: 400671 Color: 0

Bin 3375: 399 of cap free
Amount of items: 2
Items: 
Size: 666713 Color: 4
Size: 332889 Color: 0

Bin 3376: 401 of cap free
Amount of items: 2
Items: 
Size: 773743 Color: 2
Size: 225857 Color: 3

Bin 3377: 401 of cap free
Amount of items: 2
Items: 
Size: 791497 Color: 4
Size: 208103 Color: 1

Bin 3378: 403 of cap free
Amount of items: 2
Items: 
Size: 625740 Color: 4
Size: 373858 Color: 0

Bin 3379: 403 of cap free
Amount of items: 2
Items: 
Size: 719976 Color: 4
Size: 279622 Color: 2

Bin 3380: 403 of cap free
Amount of items: 2
Items: 
Size: 780069 Color: 0
Size: 219529 Color: 2

Bin 3381: 404 of cap free
Amount of items: 2
Items: 
Size: 551555 Color: 2
Size: 448042 Color: 4

Bin 3382: 404 of cap free
Amount of items: 2
Items: 
Size: 788454 Color: 0
Size: 211143 Color: 2

Bin 3383: 408 of cap free
Amount of items: 2
Items: 
Size: 630196 Color: 2
Size: 369397 Color: 4

Bin 3384: 408 of cap free
Amount of items: 2
Items: 
Size: 636387 Color: 0
Size: 363206 Color: 2

Bin 3385: 409 of cap free
Amount of items: 2
Items: 
Size: 680544 Color: 4
Size: 319048 Color: 1

Bin 3386: 409 of cap free
Amount of items: 2
Items: 
Size: 704815 Color: 3
Size: 294777 Color: 0

Bin 3387: 412 of cap free
Amount of items: 2
Items: 
Size: 694337 Color: 0
Size: 305252 Color: 1

Bin 3388: 412 of cap free
Amount of items: 2
Items: 
Size: 755870 Color: 4
Size: 243719 Color: 3

Bin 3389: 412 of cap free
Amount of items: 2
Items: 
Size: 790666 Color: 4
Size: 208923 Color: 1

Bin 3390: 413 of cap free
Amount of items: 2
Items: 
Size: 508948 Color: 0
Size: 490640 Color: 4

Bin 3391: 413 of cap free
Amount of items: 2
Items: 
Size: 592806 Color: 3
Size: 406782 Color: 4

Bin 3392: 414 of cap free
Amount of items: 2
Items: 
Size: 525700 Color: 0
Size: 473887 Color: 4

Bin 3393: 415 of cap free
Amount of items: 2
Items: 
Size: 595202 Color: 3
Size: 404384 Color: 1

Bin 3394: 415 of cap free
Amount of items: 2
Items: 
Size: 643828 Color: 3
Size: 355758 Color: 4

Bin 3395: 416 of cap free
Amount of items: 2
Items: 
Size: 504606 Color: 0
Size: 494979 Color: 4

Bin 3396: 416 of cap free
Amount of items: 2
Items: 
Size: 702509 Color: 4
Size: 297076 Color: 0

Bin 3397: 416 of cap free
Amount of items: 2
Items: 
Size: 784936 Color: 4
Size: 214649 Color: 2

Bin 3398: 418 of cap free
Amount of items: 2
Items: 
Size: 591673 Color: 2
Size: 407910 Color: 3

Bin 3399: 418 of cap free
Amount of items: 2
Items: 
Size: 683578 Color: 1
Size: 316005 Color: 3

Bin 3400: 419 of cap free
Amount of items: 2
Items: 
Size: 752121 Color: 3
Size: 247461 Color: 0

Bin 3401: 420 of cap free
Amount of items: 2
Items: 
Size: 766069 Color: 4
Size: 233512 Color: 0

Bin 3402: 421 of cap free
Amount of items: 2
Items: 
Size: 507821 Color: 3
Size: 491759 Color: 0

Bin 3403: 421 of cap free
Amount of items: 2
Items: 
Size: 560229 Color: 0
Size: 439351 Color: 1

Bin 3404: 421 of cap free
Amount of items: 2
Items: 
Size: 730650 Color: 0
Size: 268930 Color: 2

Bin 3405: 421 of cap free
Amount of items: 2
Items: 
Size: 787360 Color: 0
Size: 212220 Color: 3

Bin 3406: 422 of cap free
Amount of items: 2
Items: 
Size: 675816 Color: 3
Size: 323763 Color: 0

Bin 3407: 425 of cap free
Amount of items: 2
Items: 
Size: 514526 Color: 1
Size: 485050 Color: 4

Bin 3408: 425 of cap free
Amount of items: 2
Items: 
Size: 609163 Color: 2
Size: 390413 Color: 4

Bin 3409: 425 of cap free
Amount of items: 2
Items: 
Size: 758401 Color: 3
Size: 241175 Color: 4

Bin 3410: 426 of cap free
Amount of items: 2
Items: 
Size: 519028 Color: 1
Size: 480547 Color: 4

Bin 3411: 426 of cap free
Amount of items: 2
Items: 
Size: 689300 Color: 4
Size: 310275 Color: 1

Bin 3412: 427 of cap free
Amount of items: 2
Items: 
Size: 534249 Color: 2
Size: 465325 Color: 4

Bin 3413: 427 of cap free
Amount of items: 2
Items: 
Size: 638578 Color: 4
Size: 360996 Color: 3

Bin 3414: 428 of cap free
Amount of items: 2
Items: 
Size: 568572 Color: 3
Size: 431001 Color: 4

Bin 3415: 428 of cap free
Amount of items: 2
Items: 
Size: 694639 Color: 1
Size: 304934 Color: 3

Bin 3416: 429 of cap free
Amount of items: 2
Items: 
Size: 588987 Color: 1
Size: 410585 Color: 0

Bin 3417: 430 of cap free
Amount of items: 2
Items: 
Size: 563723 Color: 4
Size: 435848 Color: 2

Bin 3418: 431 of cap free
Amount of items: 2
Items: 
Size: 528572 Color: 4
Size: 470998 Color: 2

Bin 3419: 431 of cap free
Amount of items: 2
Items: 
Size: 603147 Color: 1
Size: 396423 Color: 2

Bin 3420: 431 of cap free
Amount of items: 2
Items: 
Size: 710958 Color: 1
Size: 288612 Color: 2

Bin 3421: 434 of cap free
Amount of items: 2
Items: 
Size: 680523 Color: 4
Size: 319044 Color: 1

Bin 3422: 435 of cap free
Amount of items: 2
Items: 
Size: 519894 Color: 1
Size: 479672 Color: 0

Bin 3423: 435 of cap free
Amount of items: 2
Items: 
Size: 565145 Color: 0
Size: 434421 Color: 3

Bin 3424: 435 of cap free
Amount of items: 2
Items: 
Size: 674513 Color: 1
Size: 325053 Color: 0

Bin 3425: 437 of cap free
Amount of items: 2
Items: 
Size: 548198 Color: 3
Size: 451366 Color: 4

Bin 3426: 437 of cap free
Amount of items: 2
Items: 
Size: 642759 Color: 1
Size: 356805 Color: 2

Bin 3427: 437 of cap free
Amount of items: 2
Items: 
Size: 695091 Color: 0
Size: 304473 Color: 1

Bin 3428: 438 of cap free
Amount of items: 2
Items: 
Size: 744254 Color: 0
Size: 255309 Color: 4

Bin 3429: 439 of cap free
Amount of items: 2
Items: 
Size: 772787 Color: 0
Size: 226775 Color: 2

Bin 3430: 443 of cap free
Amount of items: 2
Items: 
Size: 532137 Color: 3
Size: 467421 Color: 1

Bin 3431: 444 of cap free
Amount of items: 2
Items: 
Size: 533377 Color: 4
Size: 466180 Color: 1

Bin 3432: 444 of cap free
Amount of items: 2
Items: 
Size: 769058 Color: 3
Size: 230499 Color: 0

Bin 3433: 446 of cap free
Amount of items: 2
Items: 
Size: 705456 Color: 1
Size: 294099 Color: 2

Bin 3434: 446 of cap free
Amount of items: 2
Items: 
Size: 737721 Color: 2
Size: 261834 Color: 4

Bin 3435: 447 of cap free
Amount of items: 2
Items: 
Size: 731704 Color: 1
Size: 267850 Color: 4

Bin 3436: 449 of cap free
Amount of items: 2
Items: 
Size: 566087 Color: 0
Size: 433465 Color: 2

Bin 3437: 449 of cap free
Amount of items: 2
Items: 
Size: 706595 Color: 0
Size: 292957 Color: 3

Bin 3438: 450 of cap free
Amount of items: 2
Items: 
Size: 510931 Color: 1
Size: 488620 Color: 2

Bin 3439: 450 of cap free
Amount of items: 2
Items: 
Size: 693837 Color: 0
Size: 305714 Color: 3

Bin 3440: 450 of cap free
Amount of items: 2
Items: 
Size: 777811 Color: 3
Size: 221740 Color: 2

Bin 3441: 451 of cap free
Amount of items: 2
Items: 
Size: 521809 Color: 3
Size: 477741 Color: 4

Bin 3442: 452 of cap free
Amount of items: 2
Items: 
Size: 669651 Color: 4
Size: 329898 Color: 1

Bin 3443: 452 of cap free
Amount of items: 2
Items: 
Size: 727502 Color: 1
Size: 272047 Color: 2

Bin 3444: 453 of cap free
Amount of items: 2
Items: 
Size: 526433 Color: 3
Size: 473115 Color: 1

Bin 3445: 454 of cap free
Amount of items: 2
Items: 
Size: 579315 Color: 3
Size: 420232 Color: 1

Bin 3446: 455 of cap free
Amount of items: 2
Items: 
Size: 614215 Color: 1
Size: 385331 Color: 4

Bin 3447: 456 of cap free
Amount of items: 2
Items: 
Size: 740319 Color: 4
Size: 259226 Color: 3

Bin 3448: 458 of cap free
Amount of items: 2
Items: 
Size: 743149 Color: 3
Size: 256394 Color: 1

Bin 3449: 458 of cap free
Amount of items: 2
Items: 
Size: 753752 Color: 3
Size: 245791 Color: 2

Bin 3450: 459 of cap free
Amount of items: 2
Items: 
Size: 505916 Color: 3
Size: 493626 Color: 4

Bin 3451: 459 of cap free
Amount of items: 2
Items: 
Size: 545909 Color: 0
Size: 453633 Color: 4

Bin 3452: 459 of cap free
Amount of items: 2
Items: 
Size: 554235 Color: 4
Size: 445307 Color: 2

Bin 3453: 459 of cap free
Amount of items: 2
Items: 
Size: 577657 Color: 1
Size: 421885 Color: 3

Bin 3454: 459 of cap free
Amount of items: 2
Items: 
Size: 603100 Color: 4
Size: 396442 Color: 1

Bin 3455: 461 of cap free
Amount of items: 2
Items: 
Size: 645846 Color: 0
Size: 353694 Color: 1

Bin 3456: 461 of cap free
Amount of items: 2
Items: 
Size: 697689 Color: 1
Size: 301851 Color: 0

Bin 3457: 461 of cap free
Amount of items: 2
Items: 
Size: 767944 Color: 3
Size: 231596 Color: 4

Bin 3458: 462 of cap free
Amount of items: 2
Items: 
Size: 572616 Color: 0
Size: 426923 Color: 1

Bin 3459: 462 of cap free
Amount of items: 2
Items: 
Size: 592786 Color: 4
Size: 406753 Color: 3

Bin 3460: 464 of cap free
Amount of items: 2
Items: 
Size: 562663 Color: 3
Size: 436874 Color: 4

Bin 3461: 464 of cap free
Amount of items: 2
Items: 
Size: 722139 Color: 1
Size: 277398 Color: 0

Bin 3462: 465 of cap free
Amount of items: 2
Items: 
Size: 570268 Color: 3
Size: 429268 Color: 2

Bin 3463: 465 of cap free
Amount of items: 2
Items: 
Size: 786242 Color: 3
Size: 213294 Color: 2

Bin 3464: 470 of cap free
Amount of items: 2
Items: 
Size: 520882 Color: 0
Size: 478649 Color: 2

Bin 3465: 470 of cap free
Amount of items: 2
Items: 
Size: 760831 Color: 0
Size: 238700 Color: 2

Bin 3466: 472 of cap free
Amount of items: 2
Items: 
Size: 735223 Color: 4
Size: 264306 Color: 3

Bin 3467: 473 of cap free
Amount of items: 2
Items: 
Size: 774600 Color: 3
Size: 224928 Color: 0

Bin 3468: 476 of cap free
Amount of items: 2
Items: 
Size: 594601 Color: 2
Size: 404924 Color: 3

Bin 3469: 476 of cap free
Amount of items: 2
Items: 
Size: 778778 Color: 2
Size: 220747 Color: 3

Bin 3470: 477 of cap free
Amount of items: 2
Items: 
Size: 596037 Color: 4
Size: 403487 Color: 1

Bin 3471: 478 of cap free
Amount of items: 2
Items: 
Size: 558351 Color: 4
Size: 441172 Color: 2

Bin 3472: 478 of cap free
Amount of items: 2
Items: 
Size: 630138 Color: 1
Size: 369385 Color: 4

Bin 3473: 480 of cap free
Amount of items: 2
Items: 
Size: 521798 Color: 4
Size: 477723 Color: 1

Bin 3474: 480 of cap free
Amount of items: 2
Items: 
Size: 530545 Color: 2
Size: 468976 Color: 4

Bin 3475: 480 of cap free
Amount of items: 2
Items: 
Size: 560873 Color: 2
Size: 438648 Color: 1

Bin 3476: 480 of cap free
Amount of items: 2
Items: 
Size: 793187 Color: 3
Size: 206334 Color: 2

Bin 3477: 481 of cap free
Amount of items: 2
Items: 
Size: 690140 Color: 3
Size: 309380 Color: 1

Bin 3478: 481 of cap free
Amount of items: 2
Items: 
Size: 796540 Color: 1
Size: 202980 Color: 0

Bin 3479: 483 of cap free
Amount of items: 2
Items: 
Size: 720503 Color: 3
Size: 279015 Color: 2

Bin 3480: 483 of cap free
Amount of items: 2
Items: 
Size: 783023 Color: 1
Size: 216495 Color: 2

Bin 3481: 484 of cap free
Amount of items: 2
Items: 
Size: 508929 Color: 3
Size: 490588 Color: 0

Bin 3482: 485 of cap free
Amount of items: 2
Items: 
Size: 628948 Color: 0
Size: 370568 Color: 2

Bin 3483: 485 of cap free
Amount of items: 2
Items: 
Size: 784090 Color: 4
Size: 215426 Color: 0

Bin 3484: 486 of cap free
Amount of items: 2
Items: 
Size: 526406 Color: 2
Size: 473109 Color: 0

Bin 3485: 487 of cap free
Amount of items: 2
Items: 
Size: 638469 Color: 1
Size: 361045 Color: 4

Bin 3486: 487 of cap free
Amount of items: 2
Items: 
Size: 734550 Color: 2
Size: 264964 Color: 1

Bin 3487: 489 of cap free
Amount of items: 2
Items: 
Size: 671986 Color: 4
Size: 327526 Color: 3

Bin 3488: 490 of cap free
Amount of items: 2
Items: 
Size: 745661 Color: 2
Size: 253850 Color: 1

Bin 3489: 491 of cap free
Amount of items: 2
Items: 
Size: 621708 Color: 0
Size: 377802 Color: 2

Bin 3490: 492 of cap free
Amount of items: 2
Items: 
Size: 507816 Color: 3
Size: 491693 Color: 2

Bin 3491: 492 of cap free
Amount of items: 2
Items: 
Size: 512372 Color: 3
Size: 487137 Color: 0

Bin 3492: 494 of cap free
Amount of items: 2
Items: 
Size: 628944 Color: 0
Size: 370563 Color: 4

Bin 3493: 495 of cap free
Amount of items: 2
Items: 
Size: 586053 Color: 2
Size: 413453 Color: 1

Bin 3494: 495 of cap free
Amount of items: 2
Items: 
Size: 655336 Color: 3
Size: 344170 Color: 0

Bin 3495: 496 of cap free
Amount of items: 2
Items: 
Size: 504576 Color: 4
Size: 494929 Color: 3

Bin 3496: 497 of cap free
Amount of items: 2
Items: 
Size: 530560 Color: 4
Size: 468944 Color: 3

Bin 3497: 498 of cap free
Amount of items: 2
Items: 
Size: 555759 Color: 3
Size: 443744 Color: 1

Bin 3498: 498 of cap free
Amount of items: 2
Items: 
Size: 558322 Color: 2
Size: 441181 Color: 4

Bin 3499: 498 of cap free
Amount of items: 2
Items: 
Size: 615815 Color: 1
Size: 383688 Color: 4

Bin 3500: 498 of cap free
Amount of items: 2
Items: 
Size: 621158 Color: 1
Size: 378345 Color: 0

Bin 3501: 499 of cap free
Amount of items: 2
Items: 
Size: 624739 Color: 2
Size: 374763 Color: 3

Bin 3502: 502 of cap free
Amount of items: 2
Items: 
Size: 653735 Color: 3
Size: 345764 Color: 4

Bin 3503: 503 of cap free
Amount of items: 2
Items: 
Size: 623890 Color: 2
Size: 375608 Color: 4

Bin 3504: 504 of cap free
Amount of items: 2
Items: 
Size: 734534 Color: 1
Size: 264963 Color: 3

Bin 3505: 506 of cap free
Amount of items: 2
Items: 
Size: 524474 Color: 0
Size: 475021 Color: 2

Bin 3506: 506 of cap free
Amount of items: 2
Items: 
Size: 762871 Color: 0
Size: 236624 Color: 3

Bin 3507: 507 of cap free
Amount of items: 2
Items: 
Size: 576534 Color: 2
Size: 422960 Color: 1

Bin 3508: 508 of cap free
Amount of items: 2
Items: 
Size: 572586 Color: 1
Size: 426907 Color: 0

Bin 3509: 513 of cap free
Amount of items: 2
Items: 
Size: 614630 Color: 4
Size: 384858 Color: 1

Bin 3510: 513 of cap free
Amount of items: 2
Items: 
Size: 726619 Color: 3
Size: 272869 Color: 1

Bin 3511: 514 of cap free
Amount of items: 2
Items: 
Size: 598876 Color: 4
Size: 400611 Color: 2

Bin 3512: 514 of cap free
Amount of items: 2
Items: 
Size: 755878 Color: 3
Size: 243609 Color: 1

Bin 3513: 515 of cap free
Amount of items: 2
Items: 
Size: 713468 Color: 1
Size: 286018 Color: 4

Bin 3514: 516 of cap free
Amount of items: 2
Items: 
Size: 577689 Color: 3
Size: 421796 Color: 4

Bin 3515: 518 of cap free
Amount of items: 2
Items: 
Size: 725200 Color: 0
Size: 274283 Color: 2

Bin 3516: 522 of cap free
Amount of items: 2
Items: 
Size: 680503 Color: 3
Size: 318976 Color: 1

Bin 3517: 523 of cap free
Amount of items: 2
Items: 
Size: 771387 Color: 4
Size: 228091 Color: 0

Bin 3518: 524 of cap free
Amount of items: 2
Items: 
Size: 597701 Color: 0
Size: 401776 Color: 1

Bin 3519: 524 of cap free
Amount of items: 2
Items: 
Size: 641317 Color: 1
Size: 358160 Color: 0

Bin 3520: 525 of cap free
Amount of items: 2
Items: 
Size: 762757 Color: 3
Size: 236719 Color: 0

Bin 3521: 530 of cap free
Amount of items: 2
Items: 
Size: 761923 Color: 1
Size: 237548 Color: 4

Bin 3522: 531 of cap free
Amount of items: 2
Items: 
Size: 560859 Color: 4
Size: 438611 Color: 0

Bin 3523: 537 of cap free
Amount of items: 2
Items: 
Size: 789673 Color: 4
Size: 209791 Color: 1

Bin 3524: 538 of cap free
Amount of items: 2
Items: 
Size: 691437 Color: 1
Size: 308026 Color: 0

Bin 3525: 540 of cap free
Amount of items: 2
Items: 
Size: 664358 Color: 3
Size: 335103 Color: 1

Bin 3526: 540 of cap free
Amount of items: 2
Items: 
Size: 696146 Color: 3
Size: 303315 Color: 1

Bin 3527: 541 of cap free
Amount of items: 2
Items: 
Size: 669566 Color: 0
Size: 329894 Color: 3

Bin 3528: 543 of cap free
Amount of items: 2
Items: 
Size: 674518 Color: 0
Size: 324940 Color: 1

Bin 3529: 544 of cap free
Amount of items: 2
Items: 
Size: 739645 Color: 4
Size: 259812 Color: 3

Bin 3530: 545 of cap free
Amount of items: 2
Items: 
Size: 696156 Color: 1
Size: 303300 Color: 4

Bin 3531: 546 of cap free
Amount of items: 2
Items: 
Size: 552772 Color: 2
Size: 446683 Color: 1

Bin 3532: 546 of cap free
Amount of items: 2
Items: 
Size: 674463 Color: 3
Size: 324992 Color: 0

Bin 3533: 546 of cap free
Amount of items: 2
Items: 
Size: 767034 Color: 2
Size: 232421 Color: 3

Bin 3534: 548 of cap free
Amount of items: 2
Items: 
Size: 545832 Color: 1
Size: 453621 Color: 3

Bin 3535: 548 of cap free
Amount of items: 2
Items: 
Size: 595187 Color: 2
Size: 404266 Color: 1

Bin 3536: 552 of cap free
Amount of items: 2
Items: 
Size: 541673 Color: 0
Size: 457776 Color: 1

Bin 3537: 552 of cap free
Amount of items: 2
Items: 
Size: 654470 Color: 1
Size: 344979 Color: 3

Bin 3538: 554 of cap free
Amount of items: 2
Items: 
Size: 529230 Color: 1
Size: 470217 Color: 4

Bin 3539: 554 of cap free
Amount of items: 2
Items: 
Size: 584561 Color: 3
Size: 414886 Color: 0

Bin 3540: 554 of cap free
Amount of items: 2
Items: 
Size: 639357 Color: 0
Size: 360090 Color: 2

Bin 3541: 556 of cap free
Amount of items: 2
Items: 
Size: 652135 Color: 4
Size: 347310 Color: 3

Bin 3542: 556 of cap free
Amount of items: 2
Items: 
Size: 743236 Color: 1
Size: 256209 Color: 2

Bin 3543: 556 of cap free
Amount of items: 2
Items: 
Size: 757052 Color: 2
Size: 242393 Color: 0

Bin 3544: 557 of cap free
Amount of items: 2
Items: 
Size: 590276 Color: 1
Size: 409168 Color: 4

Bin 3545: 558 of cap free
Amount of items: 2
Items: 
Size: 688289 Color: 0
Size: 311154 Color: 1

Bin 3546: 560 of cap free
Amount of items: 2
Items: 
Size: 572575 Color: 3
Size: 426866 Color: 0

Bin 3547: 560 of cap free
Amount of items: 2
Items: 
Size: 730692 Color: 2
Size: 268749 Color: 0

Bin 3548: 561 of cap free
Amount of items: 2
Items: 
Size: 780688 Color: 4
Size: 218752 Color: 0

Bin 3549: 562 of cap free
Amount of items: 2
Items: 
Size: 504481 Color: 0
Size: 494958 Color: 4

Bin 3550: 564 of cap free
Amount of items: 2
Items: 
Size: 691073 Color: 4
Size: 308364 Color: 1

Bin 3551: 566 of cap free
Amount of items: 2
Items: 
Size: 568435 Color: 1
Size: 431000 Color: 2

Bin 3552: 571 of cap free
Amount of items: 2
Items: 
Size: 706938 Color: 3
Size: 292492 Color: 4

Bin 3553: 572 of cap free
Amount of items: 2
Items: 
Size: 619634 Color: 2
Size: 379795 Color: 4

Bin 3554: 573 of cap free
Amount of items: 2
Items: 
Size: 530487 Color: 1
Size: 468941 Color: 2

Bin 3555: 573 of cap free
Amount of items: 2
Items: 
Size: 575316 Color: 0
Size: 424112 Color: 4

Bin 3556: 573 of cap free
Amount of items: 2
Items: 
Size: 745659 Color: 4
Size: 253769 Color: 2

Bin 3557: 575 of cap free
Amount of items: 2
Items: 
Size: 661162 Color: 3
Size: 338264 Color: 2

Bin 3558: 576 of cap free
Amount of items: 2
Items: 
Size: 602394 Color: 0
Size: 397031 Color: 1

Bin 3559: 577 of cap free
Amount of items: 2
Items: 
Size: 636338 Color: 2
Size: 363086 Color: 4

Bin 3560: 580 of cap free
Amount of items: 2
Items: 
Size: 637286 Color: 0
Size: 362135 Color: 1

Bin 3561: 582 of cap free
Amount of items: 2
Items: 
Size: 501939 Color: 1
Size: 497480 Color: 2

Bin 3562: 583 of cap free
Amount of items: 2
Items: 
Size: 627405 Color: 1
Size: 372013 Color: 0

Bin 3563: 583 of cap free
Amount of items: 2
Items: 
Size: 748630 Color: 0
Size: 250788 Color: 1

Bin 3564: 584 of cap free
Amount of items: 2
Items: 
Size: 653706 Color: 4
Size: 345711 Color: 3

Bin 3565: 587 of cap free
Amount of items: 2
Items: 
Size: 515655 Color: 3
Size: 483759 Color: 1

Bin 3566: 587 of cap free
Amount of items: 2
Items: 
Size: 701597 Color: 3
Size: 297817 Color: 0

Bin 3567: 588 of cap free
Amount of items: 2
Items: 
Size: 576455 Color: 0
Size: 422958 Color: 1

Bin 3568: 589 of cap free
Amount of items: 2
Items: 
Size: 751926 Color: 0
Size: 247486 Color: 3

Bin 3569: 591 of cap free
Amount of items: 2
Items: 
Size: 513130 Color: 4
Size: 486280 Color: 3

Bin 3570: 591 of cap free
Amount of items: 2
Items: 
Size: 676123 Color: 0
Size: 323287 Color: 1

Bin 3571: 593 of cap free
Amount of items: 2
Items: 
Size: 563694 Color: 3
Size: 435714 Color: 1

Bin 3572: 595 of cap free
Amount of items: 2
Items: 
Size: 702349 Color: 3
Size: 297057 Color: 4

Bin 3573: 599 of cap free
Amount of items: 2
Items: 
Size: 749420 Color: 0
Size: 249982 Color: 4

Bin 3574: 601 of cap free
Amount of items: 2
Items: 
Size: 554206 Color: 3
Size: 445194 Color: 0

Bin 3575: 601 of cap free
Amount of items: 2
Items: 
Size: 772626 Color: 0
Size: 226774 Color: 2

Bin 3576: 602 of cap free
Amount of items: 2
Items: 
Size: 592733 Color: 0
Size: 406666 Color: 3

Bin 3577: 602 of cap free
Amount of items: 2
Items: 
Size: 684561 Color: 2
Size: 314838 Color: 0

Bin 3578: 603 of cap free
Amount of items: 2
Items: 
Size: 550044 Color: 0
Size: 449354 Color: 1

Bin 3579: 605 of cap free
Amount of items: 2
Items: 
Size: 792341 Color: 4
Size: 207055 Color: 2

Bin 3580: 607 of cap free
Amount of items: 2
Items: 
Size: 618202 Color: 4
Size: 381192 Color: 1

Bin 3581: 612 of cap free
Amount of items: 2
Items: 
Size: 608207 Color: 1
Size: 391182 Color: 0

Bin 3582: 612 of cap free
Amount of items: 2
Items: 
Size: 783972 Color: 4
Size: 215417 Color: 1

Bin 3583: 613 of cap free
Amount of items: 2
Items: 
Size: 639351 Color: 0
Size: 360037 Color: 3

Bin 3584: 614 of cap free
Amount of items: 2
Items: 
Size: 700374 Color: 1
Size: 299013 Color: 4

Bin 3585: 618 of cap free
Amount of items: 2
Items: 
Size: 653726 Color: 3
Size: 345657 Color: 0

Bin 3586: 623 of cap free
Amount of items: 2
Items: 
Size: 688170 Color: 4
Size: 311208 Color: 0

Bin 3587: 626 of cap free
Amount of items: 2
Items: 
Size: 500272 Color: 2
Size: 499103 Color: 3

Bin 3588: 626 of cap free
Amount of items: 2
Items: 
Size: 533200 Color: 4
Size: 466175 Color: 3

Bin 3589: 626 of cap free
Amount of items: 2
Items: 
Size: 563646 Color: 2
Size: 435729 Color: 3

Bin 3590: 626 of cap free
Amount of items: 2
Items: 
Size: 607495 Color: 0
Size: 391880 Color: 1

Bin 3591: 628 of cap free
Amount of items: 2
Items: 
Size: 768906 Color: 4
Size: 230467 Color: 2

Bin 3592: 628 of cap free
Amount of items: 2
Items: 
Size: 779495 Color: 4
Size: 219878 Color: 0

Bin 3593: 629 of cap free
Amount of items: 2
Items: 
Size: 737540 Color: 2
Size: 261832 Color: 1

Bin 3594: 632 of cap free
Amount of items: 7
Items: 
Size: 143023 Color: 1
Size: 142895 Color: 1
Size: 142855 Color: 0
Size: 142693 Color: 3
Size: 142673 Color: 4
Size: 142633 Color: 3
Size: 142597 Color: 4

Bin 3595: 634 of cap free
Amount of items: 2
Items: 
Size: 688260 Color: 0
Size: 311107 Color: 1

Bin 3596: 635 of cap free
Amount of items: 2
Items: 
Size: 536743 Color: 3
Size: 462623 Color: 1

Bin 3597: 639 of cap free
Amount of items: 2
Items: 
Size: 632574 Color: 3
Size: 366788 Color: 1

Bin 3598: 641 of cap free
Amount of items: 2
Items: 
Size: 679111 Color: 0
Size: 320249 Color: 2

Bin 3599: 642 of cap free
Amount of items: 2
Items: 
Size: 535408 Color: 3
Size: 463951 Color: 1

Bin 3600: 643 of cap free
Amount of items: 2
Items: 
Size: 575070 Color: 3
Size: 424288 Color: 0

Bin 3601: 643 of cap free
Amount of items: 2
Items: 
Size: 793086 Color: 0
Size: 206272 Color: 1

Bin 3602: 646 of cap free
Amount of items: 2
Items: 
Size: 572542 Color: 4
Size: 426813 Color: 3

Bin 3603: 647 of cap free
Amount of items: 2
Items: 
Size: 622526 Color: 2
Size: 376828 Color: 1

Bin 3604: 649 of cap free
Amount of items: 2
Items: 
Size: 505186 Color: 2
Size: 494166 Color: 1

Bin 3605: 653 of cap free
Amount of items: 2
Items: 
Size: 721446 Color: 2
Size: 277902 Color: 1

Bin 3606: 655 of cap free
Amount of items: 2
Items: 
Size: 531975 Color: 4
Size: 467371 Color: 1

Bin 3607: 659 of cap free
Amount of items: 2
Items: 
Size: 749409 Color: 1
Size: 249933 Color: 4

Bin 3608: 663 of cap free
Amount of items: 2
Items: 
Size: 686415 Color: 4
Size: 312923 Color: 1

Bin 3609: 666 of cap free
Amount of items: 2
Items: 
Size: 747393 Color: 4
Size: 251942 Color: 0

Bin 3610: 668 of cap free
Amount of items: 2
Items: 
Size: 500015 Color: 1
Size: 499318 Color: 2

Bin 3611: 669 of cap free
Amount of items: 2
Items: 
Size: 520771 Color: 4
Size: 478561 Color: 2

Bin 3612: 670 of cap free
Amount of items: 2
Items: 
Size: 649143 Color: 0
Size: 350188 Color: 4

Bin 3613: 673 of cap free
Amount of items: 2
Items: 
Size: 671163 Color: 0
Size: 328165 Color: 3

Bin 3614: 676 of cap free
Amount of items: 2
Items: 
Size: 612567 Color: 2
Size: 386758 Color: 4

Bin 3615: 676 of cap free
Amount of items: 2
Items: 
Size: 732824 Color: 2
Size: 266501 Color: 3

Bin 3616: 680 of cap free
Amount of items: 2
Items: 
Size: 714838 Color: 1
Size: 284483 Color: 4

Bin 3617: 681 of cap free
Amount of items: 2
Items: 
Size: 790655 Color: 2
Size: 208665 Color: 3

Bin 3618: 683 of cap free
Amount of items: 2
Items: 
Size: 525692 Color: 4
Size: 473626 Color: 1

Bin 3619: 686 of cap free
Amount of items: 2
Items: 
Size: 508882 Color: 1
Size: 490433 Color: 2

Bin 3620: 690 of cap free
Amount of items: 2
Items: 
Size: 751882 Color: 4
Size: 247429 Color: 1

Bin 3621: 690 of cap free
Amount of items: 2
Items: 
Size: 768914 Color: 2
Size: 230397 Color: 0

Bin 3622: 691 of cap free
Amount of items: 2
Items: 
Size: 664342 Color: 2
Size: 334968 Color: 3

Bin 3623: 692 of cap free
Amount of items: 2
Items: 
Size: 678114 Color: 4
Size: 321195 Color: 1

Bin 3624: 692 of cap free
Amount of items: 2
Items: 
Size: 783933 Color: 0
Size: 215376 Color: 4

Bin 3625: 694 of cap free
Amount of items: 2
Items: 
Size: 528535 Color: 0
Size: 470772 Color: 1

Bin 3626: 694 of cap free
Amount of items: 2
Items: 
Size: 636356 Color: 4
Size: 362951 Color: 3

Bin 3627: 694 of cap free
Amount of items: 2
Items: 
Size: 697486 Color: 0
Size: 301821 Color: 2

Bin 3628: 695 of cap free
Amount of items: 2
Items: 
Size: 710245 Color: 4
Size: 289061 Color: 0

Bin 3629: 697 of cap free
Amount of items: 2
Items: 
Size: 641215 Color: 1
Size: 358089 Color: 0

Bin 3630: 697 of cap free
Amount of items: 2
Items: 
Size: 645725 Color: 3
Size: 353579 Color: 4

Bin 3631: 698 of cap free
Amount of items: 2
Items: 
Size: 683432 Color: 1
Size: 315871 Color: 0

Bin 3632: 698 of cap free
Amount of items: 2
Items: 
Size: 741929 Color: 4
Size: 257374 Color: 0

Bin 3633: 699 of cap free
Amount of items: 2
Items: 
Size: 653663 Color: 0
Size: 345639 Color: 2

Bin 3634: 704 of cap free
Amount of items: 2
Items: 
Size: 572551 Color: 3
Size: 426746 Color: 0

Bin 3635: 705 of cap free
Amount of items: 2
Items: 
Size: 661146 Color: 3
Size: 338150 Color: 4

Bin 3636: 705 of cap free
Amount of items: 2
Items: 
Size: 679066 Color: 0
Size: 320230 Color: 1

Bin 3637: 706 of cap free
Amount of items: 2
Items: 
Size: 554170 Color: 4
Size: 445125 Color: 2

Bin 3638: 707 of cap free
Amount of items: 2
Items: 
Size: 563644 Color: 3
Size: 435650 Color: 0

Bin 3639: 708 of cap free
Amount of items: 2
Items: 
Size: 780552 Color: 0
Size: 218741 Color: 3

Bin 3640: 712 of cap free
Amount of items: 2
Items: 
Size: 711692 Color: 2
Size: 287597 Color: 3

Bin 3641: 714 of cap free
Amount of items: 2
Items: 
Size: 521572 Color: 2
Size: 477715 Color: 1

Bin 3642: 716 of cap free
Amount of items: 2
Items: 
Size: 560690 Color: 1
Size: 438595 Color: 3

Bin 3643: 717 of cap free
Amount of items: 2
Items: 
Size: 520656 Color: 2
Size: 478628 Color: 4

Bin 3644: 717 of cap free
Amount of items: 2
Items: 
Size: 682350 Color: 4
Size: 316934 Color: 2

Bin 3645: 719 of cap free
Amount of items: 2
Items: 
Size: 530395 Color: 1
Size: 468887 Color: 3

Bin 3646: 721 of cap free
Amount of items: 2
Items: 
Size: 602860 Color: 1
Size: 396420 Color: 2

Bin 3647: 722 of cap free
Amount of items: 2
Items: 
Size: 690097 Color: 4
Size: 309182 Color: 2

Bin 3648: 723 of cap free
Amount of items: 2
Items: 
Size: 562538 Color: 0
Size: 436740 Color: 3

Bin 3649: 723 of cap free
Amount of items: 2
Items: 
Size: 563630 Color: 1
Size: 435648 Color: 3

Bin 3650: 729 of cap free
Amount of items: 2
Items: 
Size: 527294 Color: 4
Size: 471978 Color: 2

Bin 3651: 730 of cap free
Amount of items: 2
Items: 
Size: 719698 Color: 0
Size: 279573 Color: 2

Bin 3652: 731 of cap free
Amount of items: 2
Items: 
Size: 732848 Color: 3
Size: 266422 Color: 0

Bin 3653: 732 of cap free
Amount of items: 2
Items: 
Size: 626453 Color: 0
Size: 372816 Color: 3

Bin 3654: 732 of cap free
Amount of items: 2
Items: 
Size: 649105 Color: 3
Size: 350164 Color: 1

Bin 3655: 732 of cap free
Amount of items: 2
Items: 
Size: 709891 Color: 0
Size: 289378 Color: 4

Bin 3656: 732 of cap free
Amount of items: 2
Items: 
Size: 755676 Color: 4
Size: 243593 Color: 0

Bin 3657: 734 of cap free
Amount of items: 2
Items: 
Size: 517894 Color: 2
Size: 481373 Color: 4

Bin 3658: 734 of cap free
Amount of items: 2
Items: 
Size: 521604 Color: 1
Size: 477663 Color: 0

Bin 3659: 734 of cap free
Amount of items: 2
Items: 
Size: 554142 Color: 1
Size: 445125 Color: 0

Bin 3660: 734 of cap free
Amount of items: 2
Items: 
Size: 653645 Color: 2
Size: 345622 Color: 3

Bin 3661: 737 of cap free
Amount of items: 2
Items: 
Size: 509730 Color: 4
Size: 489534 Color: 0

Bin 3662: 738 of cap free
Amount of items: 2
Items: 
Size: 504434 Color: 1
Size: 494829 Color: 2

Bin 3663: 740 of cap free
Amount of items: 2
Items: 
Size: 525643 Color: 2
Size: 473618 Color: 1

Bin 3664: 741 of cap free
Amount of items: 2
Items: 
Size: 548008 Color: 0
Size: 451252 Color: 4

Bin 3665: 741 of cap free
Amount of items: 2
Items: 
Size: 554137 Color: 4
Size: 445123 Color: 2

Bin 3666: 741 of cap free
Amount of items: 2
Items: 
Size: 601452 Color: 3
Size: 397808 Color: 2

Bin 3667: 744 of cap free
Amount of items: 2
Items: 
Size: 678081 Color: 0
Size: 321176 Color: 2

Bin 3668: 744 of cap free
Amount of items: 2
Items: 
Size: 771297 Color: 2
Size: 227960 Color: 3

Bin 3669: 748 of cap free
Amount of items: 2
Items: 
Size: 688162 Color: 2
Size: 311091 Color: 1

Bin 3670: 748 of cap free
Amount of items: 2
Items: 
Size: 762639 Color: 2
Size: 236614 Color: 1

Bin 3671: 750 of cap free
Amount of items: 2
Items: 
Size: 586048 Color: 1
Size: 413203 Color: 2

Bin 3672: 753 of cap free
Amount of items: 2
Items: 
Size: 792294 Color: 4
Size: 206954 Color: 2

Bin 3673: 754 of cap free
Amount of items: 2
Items: 
Size: 530361 Color: 0
Size: 468886 Color: 4

Bin 3674: 754 of cap free
Amount of items: 2
Items: 
Size: 563600 Color: 4
Size: 435647 Color: 2

Bin 3675: 759 of cap free
Amount of items: 2
Items: 
Size: 784941 Color: 2
Size: 214301 Color: 0

Bin 3676: 760 of cap free
Amount of items: 2
Items: 
Size: 616150 Color: 4
Size: 383091 Color: 3

Bin 3677: 766 of cap free
Amount of items: 2
Items: 
Size: 740930 Color: 0
Size: 258305 Color: 2

Bin 3678: 768 of cap free
Amount of items: 2
Items: 
Size: 642725 Color: 3
Size: 356508 Color: 2

Bin 3679: 769 of cap free
Amount of items: 2
Items: 
Size: 771238 Color: 4
Size: 227994 Color: 2

Bin 3680: 769 of cap free
Amount of items: 2
Items: 
Size: 795426 Color: 3
Size: 203806 Color: 1

Bin 3681: 771 of cap free
Amount of items: 2
Items: 
Size: 701519 Color: 3
Size: 297711 Color: 4

Bin 3682: 774 of cap free
Amount of items: 2
Items: 
Size: 734278 Color: 4
Size: 264949 Color: 3

Bin 3683: 775 of cap free
Amount of items: 2
Items: 
Size: 567450 Color: 4
Size: 431776 Color: 0

Bin 3684: 776 of cap free
Amount of items: 2
Items: 
Size: 634208 Color: 0
Size: 365017 Color: 1

Bin 3685: 776 of cap free
Amount of items: 2
Items: 
Size: 641810 Color: 0
Size: 357415 Color: 1

Bin 3686: 777 of cap free
Amount of items: 2
Items: 
Size: 646828 Color: 4
Size: 352396 Color: 3

Bin 3687: 779 of cap free
Amount of items: 2
Items: 
Size: 552643 Color: 0
Size: 446579 Color: 3

Bin 3688: 779 of cap free
Amount of items: 2
Items: 
Size: 734271 Color: 2
Size: 264951 Color: 4

Bin 3689: 780 of cap free
Amount of items: 2
Items: 
Size: 725812 Color: 3
Size: 273409 Color: 0

Bin 3690: 786 of cap free
Amount of items: 2
Items: 
Size: 714784 Color: 1
Size: 284431 Color: 0

Bin 3691: 787 of cap free
Amount of items: 2
Items: 
Size: 757955 Color: 0
Size: 241259 Color: 3

Bin 3692: 792 of cap free
Amount of items: 2
Items: 
Size: 524426 Color: 0
Size: 474783 Color: 2

Bin 3693: 792 of cap free
Amount of items: 2
Items: 
Size: 618149 Color: 4
Size: 381060 Color: 2

Bin 3694: 794 of cap free
Amount of items: 2
Items: 
Size: 711632 Color: 2
Size: 287575 Color: 4

Bin 3695: 796 of cap free
Amount of items: 2
Items: 
Size: 622293 Color: 1
Size: 376912 Color: 2

Bin 3696: 804 of cap free
Amount of items: 2
Items: 
Size: 747270 Color: 4
Size: 251927 Color: 0

Bin 3697: 804 of cap free
Amount of items: 2
Items: 
Size: 772378 Color: 2
Size: 226819 Color: 0

Bin 3698: 814 of cap free
Amount of items: 2
Items: 
Size: 539671 Color: 2
Size: 459516 Color: 4

Bin 3699: 823 of cap free
Amount of items: 2
Items: 
Size: 678019 Color: 3
Size: 321159 Color: 0

Bin 3700: 824 of cap free
Amount of items: 2
Items: 
Size: 507786 Color: 1
Size: 491391 Color: 2

Bin 3701: 824 of cap free
Amount of items: 2
Items: 
Size: 730289 Color: 0
Size: 268888 Color: 2

Bin 3702: 824 of cap free
Amount of items: 2
Items: 
Size: 790523 Color: 3
Size: 208654 Color: 0

Bin 3703: 826 of cap free
Amount of items: 2
Items: 
Size: 522780 Color: 3
Size: 476395 Color: 1

Bin 3704: 826 of cap free
Amount of items: 2
Items: 
Size: 739389 Color: 1
Size: 259786 Color: 0

Bin 3705: 829 of cap free
Amount of items: 2
Items: 
Size: 669463 Color: 1
Size: 329709 Color: 4

Bin 3706: 830 of cap free
Amount of items: 2
Items: 
Size: 704752 Color: 3
Size: 294419 Color: 1

Bin 3707: 833 of cap free
Amount of items: 2
Items: 
Size: 682417 Color: 2
Size: 316751 Color: 0

Bin 3708: 834 of cap free
Amount of items: 2
Items: 
Size: 619535 Color: 4
Size: 379632 Color: 3

Bin 3709: 834 of cap free
Amount of items: 2
Items: 
Size: 700165 Color: 4
Size: 299002 Color: 1

Bin 3710: 834 of cap free
Amount of items: 2
Items: 
Size: 762671 Color: 1
Size: 236496 Color: 4

Bin 3711: 836 of cap free
Amount of items: 2
Items: 
Size: 584321 Color: 0
Size: 414844 Color: 3

Bin 3712: 837 of cap free
Amount of items: 2
Items: 
Size: 732810 Color: 3
Size: 266354 Color: 1

Bin 3713: 838 of cap free
Amount of items: 2
Items: 
Size: 667909 Color: 4
Size: 331254 Color: 2

Bin 3714: 843 of cap free
Amount of items: 2
Items: 
Size: 658657 Color: 2
Size: 340501 Color: 0

Bin 3715: 847 of cap free
Amount of items: 2
Items: 
Size: 669449 Color: 3
Size: 329705 Color: 1

Bin 3716: 848 of cap free
Amount of items: 2
Items: 
Size: 558176 Color: 4
Size: 440977 Color: 1

Bin 3717: 851 of cap free
Amount of items: 2
Items: 
Size: 686795 Color: 1
Size: 312355 Color: 3

Bin 3718: 863 of cap free
Amount of items: 2
Items: 
Size: 509743 Color: 0
Size: 489395 Color: 2

Bin 3719: 864 of cap free
Amount of items: 2
Items: 
Size: 683306 Color: 2
Size: 315831 Color: 3

Bin 3720: 865 of cap free
Amount of items: 2
Items: 
Size: 571327 Color: 1
Size: 427809 Color: 2

Bin 3721: 867 of cap free
Amount of items: 2
Items: 
Size: 667672 Color: 0
Size: 331462 Color: 4

Bin 3722: 877 of cap free
Amount of items: 2
Items: 
Size: 788305 Color: 0
Size: 210819 Color: 3

Bin 3723: 878 of cap free
Amount of items: 2
Items: 
Size: 513123 Color: 4
Size: 486000 Color: 2

Bin 3724: 879 of cap free
Amount of items: 2
Items: 
Size: 751892 Color: 1
Size: 247230 Color: 2

Bin 3725: 880 of cap free
Amount of items: 2
Items: 
Size: 688097 Color: 4
Size: 311024 Color: 3

Bin 3726: 881 of cap free
Amount of items: 2
Items: 
Size: 509713 Color: 4
Size: 489407 Color: 0

Bin 3727: 884 of cap free
Amount of items: 2
Items: 
Size: 525544 Color: 4
Size: 473573 Color: 2

Bin 3728: 885 of cap free
Amount of items: 2
Items: 
Size: 634128 Color: 4
Size: 364988 Color: 2

Bin 3729: 885 of cap free
Amount of items: 2
Items: 
Size: 771167 Color: 4
Size: 227949 Color: 2

Bin 3730: 886 of cap free
Amount of items: 2
Items: 
Size: 622281 Color: 1
Size: 376834 Color: 2

Bin 3731: 891 of cap free
Amount of items: 2
Items: 
Size: 792094 Color: 2
Size: 207016 Color: 4

Bin 3732: 896 of cap free
Amount of items: 2
Items: 
Size: 562482 Color: 0
Size: 436623 Color: 1

Bin 3733: 897 of cap free
Amount of items: 2
Items: 
Size: 634125 Color: 3
Size: 364979 Color: 2

Bin 3734: 898 of cap free
Amount of items: 2
Items: 
Size: 749280 Color: 4
Size: 249823 Color: 1

Bin 3735: 904 of cap free
Amount of items: 2
Items: 
Size: 550037 Color: 0
Size: 449060 Color: 2

Bin 3736: 905 of cap free
Amount of items: 2
Items: 
Size: 677981 Color: 2
Size: 321115 Color: 1

Bin 3737: 907 of cap free
Amount of items: 2
Items: 
Size: 554114 Color: 3
Size: 444980 Color: 1

Bin 3738: 910 of cap free
Amount of items: 2
Items: 
Size: 690086 Color: 2
Size: 309005 Color: 0

Bin 3739: 912 of cap free
Amount of items: 2
Items: 
Size: 575055 Color: 3
Size: 424034 Color: 2

Bin 3740: 914 of cap free
Amount of items: 2
Items: 
Size: 515588 Color: 2
Size: 483499 Color: 4

Bin 3741: 915 of cap free
Amount of items: 2
Items: 
Size: 582038 Color: 4
Size: 417048 Color: 2

Bin 3742: 915 of cap free
Amount of items: 2
Items: 
Size: 771145 Color: 1
Size: 227941 Color: 2

Bin 3743: 916 of cap free
Amount of items: 2
Items: 
Size: 705398 Color: 1
Size: 293687 Color: 4

Bin 3744: 921 of cap free
Amount of items: 2
Items: 
Size: 515603 Color: 4
Size: 483477 Color: 2

Bin 3745: 923 of cap free
Amount of items: 2
Items: 
Size: 511422 Color: 4
Size: 487656 Color: 3

Bin 3746: 924 of cap free
Amount of items: 2
Items: 
Size: 507704 Color: 4
Size: 491373 Color: 2

Bin 3747: 928 of cap free
Amount of items: 2
Items: 
Size: 639015 Color: 3
Size: 360058 Color: 0

Bin 3748: 932 of cap free
Amount of items: 2
Items: 
Size: 781736 Color: 0
Size: 217333 Color: 3

Bin 3749: 933 of cap free
Amount of items: 2
Items: 
Size: 782098 Color: 3
Size: 216970 Color: 1

Bin 3750: 937 of cap free
Amount of items: 2
Items: 
Size: 743085 Color: 1
Size: 255979 Color: 0

Bin 3751: 940 of cap free
Amount of items: 6
Items: 
Size: 166637 Color: 1
Size: 166578 Color: 3
Size: 166551 Color: 1
Size: 166541 Color: 1
Size: 166385 Color: 0
Size: 166369 Color: 0

Bin 3752: 949 of cap free
Amount of items: 2
Items: 
Size: 607274 Color: 3
Size: 391778 Color: 1

Bin 3753: 950 of cap free
Amount of items: 2
Items: 
Size: 646795 Color: 4
Size: 352256 Color: 0

Bin 3754: 950 of cap free
Amount of items: 2
Items: 
Size: 757944 Color: 0
Size: 241107 Color: 1

Bin 3755: 951 of cap free
Amount of items: 2
Items: 
Size: 755479 Color: 2
Size: 243571 Color: 1

Bin 3756: 956 of cap free
Amount of items: 2
Items: 
Size: 520537 Color: 1
Size: 478508 Color: 2

Bin 3757: 959 of cap free
Amount of items: 2
Items: 
Size: 739325 Color: 0
Size: 259717 Color: 3

Bin 3758: 960 of cap free
Amount of items: 2
Items: 
Size: 663333 Color: 1
Size: 335708 Color: 3

Bin 3759: 962 of cap free
Amount of items: 2
Items: 
Size: 517723 Color: 2
Size: 481316 Color: 3

Bin 3760: 963 of cap free
Amount of items: 2
Items: 
Size: 539608 Color: 3
Size: 459430 Color: 4

Bin 3761: 965 of cap free
Amount of items: 2
Items: 
Size: 536431 Color: 0
Size: 462605 Color: 1

Bin 3762: 966 of cap free
Amount of items: 2
Items: 
Size: 591477 Color: 4
Size: 407558 Color: 1

Bin 3763: 966 of cap free
Amount of items: 2
Items: 
Size: 611447 Color: 3
Size: 387588 Color: 4

Bin 3764: 968 of cap free
Amount of items: 2
Items: 
Size: 790408 Color: 0
Size: 208625 Color: 1

Bin 3765: 969 of cap free
Amount of items: 2
Items: 
Size: 714712 Color: 3
Size: 284320 Color: 2

Bin 3766: 982 of cap free
Amount of items: 2
Items: 
Size: 535234 Color: 3
Size: 463785 Color: 1

Bin 3767: 993 of cap free
Amount of items: 2
Items: 
Size: 588496 Color: 3
Size: 410512 Color: 0

Bin 3768: 999 of cap free
Amount of items: 2
Items: 
Size: 704694 Color: 0
Size: 294308 Color: 1

Bin 3769: 1000 of cap free
Amount of items: 2
Items: 
Size: 580973 Color: 1
Size: 418028 Color: 0

Bin 3770: 1001 of cap free
Amount of items: 2
Items: 
Size: 680372 Color: 1
Size: 318628 Color: 3

Bin 3771: 1004 of cap free
Amount of items: 2
Items: 
Size: 611121 Color: 4
Size: 387876 Color: 3

Bin 3772: 1005 of cap free
Amount of items: 2
Items: 
Size: 592720 Color: 4
Size: 406276 Color: 3

Bin 3773: 1006 of cap free
Amount of items: 2
Items: 
Size: 632549 Color: 0
Size: 366446 Color: 4

Bin 3774: 1006 of cap free
Amount of items: 2
Items: 
Size: 737222 Color: 4
Size: 261773 Color: 0

Bin 3775: 1009 of cap free
Amount of items: 2
Items: 
Size: 627757 Color: 0
Size: 371235 Color: 3

Bin 3776: 1011 of cap free
Amount of items: 2
Items: 
Size: 517709 Color: 1
Size: 481281 Color: 2

Bin 3777: 1013 of cap free
Amount of items: 2
Items: 
Size: 580230 Color: 0
Size: 418758 Color: 1

Bin 3778: 1018 of cap free
Amount of items: 2
Items: 
Size: 755640 Color: 1
Size: 243343 Color: 4

Bin 3779: 1020 of cap free
Amount of items: 2
Items: 
Size: 673292 Color: 0
Size: 325689 Color: 4

Bin 3780: 1021 of cap free
Amount of items: 2
Items: 
Size: 697403 Color: 2
Size: 301577 Color: 3

Bin 3781: 1022 of cap free
Amount of items: 2
Items: 
Size: 680379 Color: 3
Size: 318600 Color: 1

Bin 3782: 1022 of cap free
Amount of items: 2
Items: 
Size: 691007 Color: 4
Size: 307972 Color: 2

Bin 3783: 1024 of cap free
Amount of items: 2
Items: 
Size: 654990 Color: 3
Size: 343987 Color: 4

Bin 3784: 1024 of cap free
Amount of items: 2
Items: 
Size: 768898 Color: 0
Size: 230079 Color: 3

Bin 3785: 1029 of cap free
Amount of items: 2
Items: 
Size: 764460 Color: 4
Size: 234512 Color: 1

Bin 3786: 1029 of cap free
Amount of items: 2
Items: 
Size: 772270 Color: 3
Size: 226702 Color: 1

Bin 3787: 1032 of cap free
Amount of items: 2
Items: 
Size: 677832 Color: 0
Size: 321137 Color: 2

Bin 3788: 1038 of cap free
Amount of items: 2
Items: 
Size: 632505 Color: 3
Size: 366458 Color: 0

Bin 3789: 1043 of cap free
Amount of items: 2
Items: 
Size: 531896 Color: 0
Size: 467062 Color: 2

Bin 3790: 1048 of cap free
Amount of items: 2
Items: 
Size: 714710 Color: 0
Size: 284243 Color: 1

Bin 3791: 1049 of cap free
Amount of items: 2
Items: 
Size: 657071 Color: 3
Size: 341881 Color: 1

Bin 3792: 1052 of cap free
Amount of items: 2
Items: 
Size: 718101 Color: 0
Size: 280848 Color: 2

Bin 3793: 1054 of cap free
Amount of items: 2
Items: 
Size: 640966 Color: 2
Size: 357981 Color: 0

Bin 3794: 1059 of cap free
Amount of items: 2
Items: 
Size: 509586 Color: 3
Size: 489356 Color: 1

Bin 3795: 1059 of cap free
Amount of items: 2
Items: 
Size: 788367 Color: 3
Size: 210575 Color: 0

Bin 3796: 1064 of cap free
Amount of items: 2
Items: 
Size: 535224 Color: 3
Size: 463713 Color: 2

Bin 3797: 1066 of cap free
Amount of items: 2
Items: 
Size: 545790 Color: 1
Size: 453145 Color: 3

Bin 3798: 1067 of cap free
Amount of items: 2
Items: 
Size: 693834 Color: 3
Size: 305100 Color: 1

Bin 3799: 1072 of cap free
Amount of items: 2
Items: 
Size: 688006 Color: 1
Size: 310923 Color: 0

Bin 3800: 1074 of cap free
Amount of items: 2
Items: 
Size: 683155 Color: 0
Size: 315772 Color: 3

Bin 3801: 1077 of cap free
Amount of items: 2
Items: 
Size: 730149 Color: 0
Size: 268775 Color: 2

Bin 3802: 1079 of cap free
Amount of items: 2
Items: 
Size: 768886 Color: 0
Size: 230036 Color: 1

Bin 3803: 1082 of cap free
Amount of items: 2
Items: 
Size: 520503 Color: 1
Size: 478416 Color: 2

Bin 3804: 1084 of cap free
Amount of items: 2
Items: 
Size: 706463 Color: 0
Size: 292454 Color: 4

Bin 3805: 1088 of cap free
Amount of items: 2
Items: 
Size: 677912 Color: 2
Size: 321001 Color: 3

Bin 3806: 1093 of cap free
Amount of items: 2
Items: 
Size: 732723 Color: 3
Size: 266185 Color: 1

Bin 3807: 1098 of cap free
Amount of items: 2
Items: 
Size: 605383 Color: 0
Size: 393520 Color: 2

Bin 3808: 1103 of cap free
Amount of items: 2
Items: 
Size: 568316 Color: 1
Size: 430582 Color: 2

Bin 3809: 1103 of cap free
Amount of items: 2
Items: 
Size: 753173 Color: 2
Size: 245725 Color: 4

Bin 3810: 1111 of cap free
Amount of items: 2
Items: 
Size: 770977 Color: 1
Size: 227913 Color: 4

Bin 3811: 1112 of cap free
Amount of items: 2
Items: 
Size: 746670 Color: 2
Size: 252219 Color: 4

Bin 3812: 1116 of cap free
Amount of items: 2
Items: 
Size: 641472 Color: 0
Size: 357413 Color: 2

Bin 3813: 1117 of cap free
Amount of items: 2
Items: 
Size: 732505 Color: 2
Size: 266379 Color: 3

Bin 3814: 1126 of cap free
Amount of items: 2
Items: 
Size: 692037 Color: 1
Size: 306838 Color: 3

Bin 3815: 1137 of cap free
Amount of items: 2
Items: 
Size: 653336 Color: 3
Size: 345528 Color: 1

Bin 3816: 1141 of cap free
Amount of items: 2
Items: 
Size: 603709 Color: 3
Size: 395151 Color: 2

Bin 3817: 1155 of cap free
Amount of items: 2
Items: 
Size: 512873 Color: 2
Size: 485973 Color: 1

Bin 3818: 1160 of cap free
Amount of items: 2
Items: 
Size: 764348 Color: 1
Size: 234493 Color: 0

Bin 3819: 1166 of cap free
Amount of items: 2
Items: 
Size: 611295 Color: 3
Size: 387540 Color: 1

Bin 3820: 1174 of cap free
Amount of items: 2
Items: 
Size: 555697 Color: 2
Size: 443130 Color: 1

Bin 3821: 1177 of cap free
Amount of items: 2
Items: 
Size: 686401 Color: 0
Size: 312423 Color: 1

Bin 3822: 1181 of cap free
Amount of items: 2
Items: 
Size: 517566 Color: 1
Size: 481254 Color: 2

Bin 3823: 1184 of cap free
Amount of items: 2
Items: 
Size: 673229 Color: 2
Size: 325588 Color: 4

Bin 3824: 1195 of cap free
Amount of items: 2
Items: 
Size: 560214 Color: 4
Size: 438592 Color: 1

Bin 3825: 1201 of cap free
Amount of items: 2
Items: 
Size: 601124 Color: 1
Size: 397676 Color: 4

Bin 3826: 1203 of cap free
Amount of items: 2
Items: 
Size: 549966 Color: 2
Size: 448832 Color: 4

Bin 3827: 1214 of cap free
Amount of items: 2
Items: 
Size: 522455 Color: 3
Size: 476332 Color: 2

Bin 3828: 1225 of cap free
Amount of items: 2
Items: 
Size: 755255 Color: 2
Size: 243521 Color: 1

Bin 3829: 1227 of cap free
Amount of items: 2
Items: 
Size: 721169 Color: 0
Size: 277605 Color: 1

Bin 3830: 1230 of cap free
Amount of items: 2
Items: 
Size: 614086 Color: 4
Size: 384685 Color: 2

Bin 3831: 1231 of cap free
Amount of items: 2
Items: 
Size: 714454 Color: 3
Size: 284316 Color: 0

Bin 3832: 1234 of cap free
Amount of items: 2
Items: 
Size: 539558 Color: 0
Size: 459209 Color: 1

Bin 3833: 1234 of cap free
Amount of items: 2
Items: 
Size: 634070 Color: 2
Size: 364697 Color: 3

Bin 3834: 1236 of cap free
Amount of items: 2
Items: 
Size: 611251 Color: 3
Size: 387514 Color: 0

Bin 3835: 1245 of cap free
Amount of items: 2
Items: 
Size: 570992 Color: 2
Size: 427764 Color: 3

Bin 3836: 1247 of cap free
Amount of items: 2
Items: 
Size: 517546 Color: 2
Size: 481208 Color: 1

Bin 3837: 1250 of cap free
Amount of items: 2
Items: 
Size: 552461 Color: 1
Size: 446290 Color: 3

Bin 3838: 1251 of cap free
Amount of items: 2
Items: 
Size: 632317 Color: 0
Size: 366433 Color: 1

Bin 3839: 1271 of cap free
Amount of items: 2
Items: 
Size: 755248 Color: 4
Size: 243482 Color: 1

Bin 3840: 1272 of cap free
Amount of items: 2
Items: 
Size: 634064 Color: 2
Size: 364665 Color: 0

Bin 3841: 1274 of cap free
Amount of items: 2
Items: 
Size: 543765 Color: 4
Size: 454962 Color: 0

Bin 3842: 1287 of cap free
Amount of items: 2
Items: 
Size: 611027 Color: 4
Size: 387687 Color: 3

Bin 3843: 1296 of cap free
Amount of items: 2
Items: 
Size: 795179 Color: 0
Size: 203526 Color: 3

Bin 3844: 1304 of cap free
Amount of items: 2
Items: 
Size: 549875 Color: 4
Size: 448822 Color: 2

Bin 3845: 1312 of cap free
Amount of items: 2
Items: 
Size: 717783 Color: 1
Size: 280906 Color: 0

Bin 3846: 1313 of cap free
Amount of items: 2
Items: 
Size: 732509 Color: 3
Size: 266179 Color: 2

Bin 3847: 1316 of cap free
Amount of items: 2
Items: 
Size: 723998 Color: 3
Size: 274687 Color: 0

Bin 3848: 1317 of cap free
Amount of items: 2
Items: 
Size: 706417 Color: 4
Size: 292267 Color: 1

Bin 3849: 1322 of cap free
Amount of items: 2
Items: 
Size: 665063 Color: 0
Size: 333616 Color: 3

Bin 3850: 1331 of cap free
Amount of items: 2
Items: 
Size: 574636 Color: 3
Size: 424034 Color: 1

Bin 3851: 1337 of cap free
Amount of items: 2
Items: 
Size: 529878 Color: 4
Size: 468786 Color: 3

Bin 3852: 1344 of cap free
Amount of items: 2
Items: 
Size: 768528 Color: 3
Size: 230129 Color: 0

Bin 3853: 1346 of cap free
Amount of items: 2
Items: 
Size: 665054 Color: 4
Size: 333601 Color: 3

Bin 3854: 1347 of cap free
Amount of items: 2
Items: 
Size: 660650 Color: 4
Size: 338004 Color: 0

Bin 3855: 1350 of cap free
Amount of items: 2
Items: 
Size: 517449 Color: 0
Size: 481202 Color: 3

Bin 3856: 1351 of cap free
Amount of items: 2
Items: 
Size: 693730 Color: 1
Size: 304920 Color: 0

Bin 3857: 1356 of cap free
Amount of items: 2
Items: 
Size: 592638 Color: 3
Size: 406007 Color: 4

Bin 3858: 1357 of cap free
Amount of items: 2
Items: 
Size: 622239 Color: 0
Size: 376405 Color: 3

Bin 3859: 1365 of cap free
Amount of items: 2
Items: 
Size: 588353 Color: 1
Size: 410283 Color: 3

Bin 3860: 1366 of cap free
Amount of items: 2
Items: 
Size: 714536 Color: 0
Size: 284099 Color: 4

Bin 3861: 1370 of cap free
Amount of items: 2
Items: 
Size: 671107 Color: 4
Size: 327524 Color: 2

Bin 3862: 1372 of cap free
Amount of items: 2
Items: 
Size: 574600 Color: 2
Size: 424029 Color: 0

Bin 3863: 1372 of cap free
Amount of items: 2
Items: 
Size: 657012 Color: 3
Size: 341617 Color: 0

Bin 3864: 1372 of cap free
Amount of items: 2
Items: 
Size: 709716 Color: 2
Size: 288913 Color: 1

Bin 3865: 1372 of cap free
Amount of items: 2
Items: 
Size: 753074 Color: 4
Size: 245555 Color: 2

Bin 3866: 1378 of cap free
Amount of items: 2
Items: 
Size: 706379 Color: 3
Size: 292244 Color: 1

Bin 3867: 1380 of cap free
Amount of items: 2
Items: 
Size: 633958 Color: 0
Size: 364663 Color: 1

Bin 3868: 1381 of cap free
Amount of items: 2
Items: 
Size: 581952 Color: 0
Size: 416668 Color: 4

Bin 3869: 1382 of cap free
Amount of items: 2
Items: 
Size: 580027 Color: 0
Size: 418592 Color: 1

Bin 3870: 1383 of cap free
Amount of items: 2
Items: 
Size: 507379 Color: 4
Size: 491239 Color: 2

Bin 3871: 1393 of cap free
Amount of items: 2
Items: 
Size: 557652 Color: 3
Size: 440956 Color: 2

Bin 3872: 1393 of cap free
Amount of items: 2
Items: 
Size: 646422 Color: 4
Size: 352186 Color: 0

Bin 3873: 1399 of cap free
Amount of items: 2
Items: 
Size: 569509 Color: 0
Size: 429093 Color: 2

Bin 3874: 1399 of cap free
Amount of items: 2
Items: 
Size: 622202 Color: 4
Size: 376400 Color: 0

Bin 3875: 1399 of cap free
Amount of items: 2
Items: 
Size: 753110 Color: 2
Size: 245492 Color: 3

Bin 3876: 1401 of cap free
Amount of items: 2
Items: 
Size: 539272 Color: 3
Size: 459328 Color: 0

Bin 3877: 1411 of cap free
Amount of items: 2
Items: 
Size: 660606 Color: 1
Size: 337984 Color: 4

Bin 3878: 1414 of cap free
Amount of items: 2
Items: 
Size: 687768 Color: 4
Size: 310819 Color: 0

Bin 3879: 1426 of cap free
Amount of items: 2
Items: 
Size: 624728 Color: 0
Size: 373847 Color: 3

Bin 3880: 1434 of cap free
Amount of items: 2
Items: 
Size: 501866 Color: 1
Size: 496701 Color: 2

Bin 3881: 1452 of cap free
Amount of items: 2
Items: 
Size: 507320 Color: 0
Size: 491229 Color: 2

Bin 3882: 1452 of cap free
Amount of items: 2
Items: 
Size: 755217 Color: 3
Size: 243332 Color: 2

Bin 3883: 1454 of cap free
Amount of items: 2
Items: 
Size: 764360 Color: 0
Size: 234187 Color: 1

Bin 3884: 1456 of cap free
Amount of items: 2
Items: 
Size: 559976 Color: 3
Size: 438569 Color: 1

Bin 3885: 1457 of cap free
Amount of items: 2
Items: 
Size: 784706 Color: 4
Size: 213838 Color: 0

Bin 3886: 1461 of cap free
Amount of items: 2
Items: 
Size: 528381 Color: 2
Size: 470159 Color: 0

Bin 3887: 1479 of cap free
Amount of items: 2
Items: 
Size: 782065 Color: 3
Size: 216457 Color: 1

Bin 3888: 1484 of cap free
Amount of items: 2
Items: 
Size: 725723 Color: 3
Size: 272794 Color: 0

Bin 3889: 1488 of cap free
Amount of items: 2
Items: 
Size: 732354 Color: 2
Size: 266159 Color: 1

Bin 3890: 1488 of cap free
Amount of items: 2
Items: 
Size: 741516 Color: 2
Size: 256997 Color: 3

Bin 3891: 1494 of cap free
Amount of items: 2
Items: 
Size: 732348 Color: 3
Size: 266159 Color: 0

Bin 3892: 1535 of cap free
Amount of items: 2
Items: 
Size: 640613 Color: 1
Size: 357853 Color: 0

Bin 3893: 1544 of cap free
Amount of items: 2
Items: 
Size: 668873 Color: 0
Size: 329584 Color: 2

Bin 3894: 1551 of cap free
Amount of items: 2
Items: 
Size: 622223 Color: 0
Size: 376227 Color: 1

Bin 3895: 1563 of cap free
Amount of items: 2
Items: 
Size: 636325 Color: 1
Size: 362113 Color: 2

Bin 3896: 1565 of cap free
Amount of items: 2
Items: 
Size: 570957 Color: 1
Size: 427479 Color: 3

Bin 3897: 1566 of cap free
Amount of items: 2
Items: 
Size: 770600 Color: 3
Size: 227835 Color: 2

Bin 3898: 1569 of cap free
Amount of items: 2
Items: 
Size: 539233 Color: 4
Size: 459199 Color: 1

Bin 3899: 1596 of cap free
Amount of items: 2
Items: 
Size: 580414 Color: 1
Size: 417991 Color: 0

Bin 3900: 1597 of cap free
Amount of items: 2
Items: 
Size: 549728 Color: 3
Size: 448676 Color: 0

Bin 3901: 1599 of cap free
Amount of items: 2
Items: 
Size: 721129 Color: 1
Size: 277273 Color: 0

Bin 3902: 1604 of cap free
Amount of items: 2
Items: 
Size: 764287 Color: 1
Size: 234110 Color: 4

Bin 3903: 1608 of cap free
Amount of items: 2
Items: 
Size: 716462 Color: 4
Size: 281931 Color: 1

Bin 3904: 1613 of cap free
Amount of items: 2
Items: 
Size: 668762 Color: 2
Size: 329626 Color: 0

Bin 3905: 1621 of cap free
Amount of items: 2
Items: 
Size: 539229 Color: 4
Size: 459151 Color: 2

Bin 3906: 1625 of cap free
Amount of items: 2
Items: 
Size: 704737 Color: 1
Size: 293639 Color: 2

Bin 3907: 1630 of cap free
Amount of items: 2
Items: 
Size: 555629 Color: 0
Size: 442742 Color: 4

Bin 3908: 1637 of cap free
Amount of items: 2
Items: 
Size: 633804 Color: 0
Size: 364560 Color: 2

Bin 3909: 1643 of cap free
Amount of items: 2
Items: 
Size: 579813 Color: 2
Size: 418545 Color: 1

Bin 3910: 1644 of cap free
Amount of items: 2
Items: 
Size: 775463 Color: 0
Size: 222894 Color: 1

Bin 3911: 1648 of cap free
Amount of items: 2
Items: 
Size: 736711 Color: 3
Size: 261642 Color: 1

Bin 3912: 1650 of cap free
Amount of items: 2
Items: 
Size: 640530 Color: 2
Size: 357821 Color: 0

Bin 3913: 1650 of cap free
Amount of items: 2
Items: 
Size: 781900 Color: 3
Size: 216451 Color: 1

Bin 3914: 1655 of cap free
Amount of items: 2
Items: 
Size: 615678 Color: 4
Size: 382668 Color: 3

Bin 3915: 1661 of cap free
Amount of items: 2
Items: 
Size: 539135 Color: 0
Size: 459205 Color: 4

Bin 3916: 1667 of cap free
Amount of items: 2
Items: 
Size: 794920 Color: 4
Size: 203414 Color: 1

Bin 3917: 1669 of cap free
Amount of items: 9
Items: 
Size: 111108 Color: 4
Size: 111061 Color: 3
Size: 111058 Color: 3
Size: 111049 Color: 3
Size: 111004 Color: 0
Size: 110868 Color: 4
Size: 110862 Color: 0
Size: 110673 Color: 2
Size: 110649 Color: 2

Bin 3918: 1680 of cap free
Amount of items: 2
Items: 
Size: 696755 Color: 0
Size: 301566 Color: 4

Bin 3919: 1681 of cap free
Amount of items: 2
Items: 
Size: 660613 Color: 4
Size: 337707 Color: 3

Bin 3920: 1686 of cap free
Amount of items: 2
Items: 
Size: 736751 Color: 1
Size: 261564 Color: 4

Bin 3921: 1689 of cap free
Amount of items: 2
Items: 
Size: 615658 Color: 4
Size: 382654 Color: 1

Bin 3922: 1693 of cap free
Amount of items: 2
Items: 
Size: 649053 Color: 2
Size: 349255 Color: 0

Bin 3923: 1694 of cap free
Amount of items: 2
Items: 
Size: 794828 Color: 2
Size: 203479 Color: 4

Bin 3924: 1699 of cap free
Amount of items: 2
Items: 
Size: 668775 Color: 0
Size: 329527 Color: 1

Bin 3925: 1701 of cap free
Amount of items: 2
Items: 
Size: 529813 Color: 0
Size: 468487 Color: 2

Bin 3926: 1703 of cap free
Amount of items: 2
Items: 
Size: 519743 Color: 0
Size: 478555 Color: 1

Bin 3927: 1712 of cap free
Amount of items: 2
Items: 
Size: 720952 Color: 2
Size: 277337 Color: 1

Bin 3928: 1718 of cap free
Amount of items: 2
Items: 
Size: 776045 Color: 1
Size: 222238 Color: 3

Bin 3929: 1720 of cap free
Amount of items: 2
Items: 
Size: 729557 Color: 1
Size: 268724 Color: 3

Bin 3930: 1724 of cap free
Amount of items: 2
Items: 
Size: 607219 Color: 2
Size: 391058 Color: 1

Bin 3931: 1730 of cap free
Amount of items: 2
Items: 
Size: 746395 Color: 3
Size: 251876 Color: 1

Bin 3932: 1732 of cap free
Amount of items: 2
Items: 
Size: 627083 Color: 3
Size: 371186 Color: 1

Bin 3933: 1739 of cap free
Amount of items: 2
Items: 
Size: 693818 Color: 0
Size: 304444 Color: 1

Bin 3934: 1740 of cap free
Amount of items: 2
Items: 
Size: 543704 Color: 3
Size: 454557 Color: 1

Bin 3935: 1752 of cap free
Amount of items: 2
Items: 
Size: 739066 Color: 3
Size: 259183 Color: 0

Bin 3936: 1754 of cap free
Amount of items: 2
Items: 
Size: 529790 Color: 0
Size: 468457 Color: 3

Bin 3937: 1770 of cap free
Amount of items: 2
Items: 
Size: 581571 Color: 0
Size: 416660 Color: 1

Bin 3938: 1775 of cap free
Amount of items: 2
Items: 
Size: 539162 Color: 4
Size: 459064 Color: 3

Bin 3939: 1789 of cap free
Amount of items: 2
Items: 
Size: 776016 Color: 1
Size: 222196 Color: 2

Bin 3940: 1795 of cap free
Amount of items: 2
Items: 
Size: 775443 Color: 0
Size: 222763 Color: 1

Bin 3941: 1824 of cap free
Amount of items: 2
Items: 
Size: 638190 Color: 1
Size: 359987 Color: 4

Bin 3942: 1840 of cap free
Amount of items: 2
Items: 
Size: 574135 Color: 2
Size: 424026 Color: 0

Bin 3943: 1880 of cap free
Amount of items: 2
Items: 
Size: 588412 Color: 3
Size: 409709 Color: 1

Bin 3944: 1888 of cap free
Amount of items: 2
Items: 
Size: 636284 Color: 3
Size: 361829 Color: 0

Bin 3945: 1889 of cap free
Amount of items: 2
Items: 
Size: 682340 Color: 3
Size: 315772 Color: 0

Bin 3946: 1897 of cap free
Amount of items: 2
Items: 
Size: 508803 Color: 0
Size: 489301 Color: 4

Bin 3947: 1905 of cap free
Amount of items: 2
Items: 
Size: 519637 Color: 2
Size: 478459 Color: 1

Bin 3948: 1910 of cap free
Amount of items: 2
Items: 
Size: 636276 Color: 4
Size: 361815 Color: 2

Bin 3949: 1919 of cap free
Amount of items: 2
Items: 
Size: 543696 Color: 1
Size: 454386 Color: 2

Bin 3950: 1941 of cap free
Amount of items: 2
Items: 
Size: 632035 Color: 3
Size: 366025 Color: 0

Bin 3951: 1944 of cap free
Amount of items: 2
Items: 
Size: 555583 Color: 1
Size: 442474 Color: 3

Bin 3952: 1948 of cap free
Amount of items: 2
Items: 
Size: 499064 Color: 3
Size: 498989 Color: 2

Bin 3953: 1969 of cap free
Amount of items: 2
Items: 
Size: 504053 Color: 1
Size: 493979 Color: 3

Bin 3954: 1988 of cap free
Amount of items: 2
Items: 
Size: 600513 Color: 2
Size: 397500 Color: 0

Bin 3955: 1988 of cap free
Amount of items: 2
Items: 
Size: 642704 Color: 3
Size: 355309 Color: 2

Bin 3956: 2004 of cap free
Amount of items: 2
Items: 
Size: 757763 Color: 1
Size: 240234 Color: 4

Bin 3957: 2012 of cap free
Amount of items: 2
Items: 
Size: 499017 Color: 3
Size: 498972 Color: 2

Bin 3958: 2024 of cap free
Amount of items: 2
Items: 
Size: 690005 Color: 1
Size: 307972 Color: 0

Bin 3959: 2027 of cap free
Amount of items: 2
Items: 
Size: 524999 Color: 2
Size: 472975 Color: 4

Bin 3960: 2032 of cap free
Amount of items: 2
Items: 
Size: 560210 Color: 1
Size: 437759 Color: 3

Bin 3961: 2048 of cap free
Amount of items: 2
Items: 
Size: 640188 Color: 4
Size: 357765 Color: 0

Bin 3962: 2064 of cap free
Amount of items: 2
Items: 
Size: 660587 Color: 3
Size: 337350 Color: 4

Bin 3963: 2065 of cap free
Amount of items: 2
Items: 
Size: 536352 Color: 3
Size: 461584 Color: 4

Bin 3964: 2076 of cap free
Amount of items: 2
Items: 
Size: 736294 Color: 0
Size: 261631 Color: 1

Bin 3965: 2088 of cap free
Amount of items: 2
Items: 
Size: 682209 Color: 0
Size: 315704 Color: 2

Bin 3966: 2091 of cap free
Amount of items: 2
Items: 
Size: 672995 Color: 0
Size: 324915 Color: 2

Bin 3967: 2098 of cap free
Amount of items: 2
Items: 
Size: 746378 Color: 1
Size: 251525 Color: 0

Bin 3968: 2104 of cap free
Amount of items: 2
Items: 
Size: 538931 Color: 2
Size: 458966 Color: 3

Bin 3969: 2110 of cap free
Amount of items: 2
Items: 
Size: 720703 Color: 2
Size: 277188 Color: 0

Bin 3970: 2117 of cap free
Amount of items: 2
Items: 
Size: 640132 Color: 3
Size: 357752 Color: 0

Bin 3971: 2134 of cap free
Amount of items: 2
Items: 
Size: 626721 Color: 3
Size: 371146 Color: 2

Bin 3972: 2139 of cap free
Amount of items: 2
Items: 
Size: 660523 Color: 3
Size: 337339 Color: 1

Bin 3973: 2177 of cap free
Amount of items: 2
Items: 
Size: 543589 Color: 2
Size: 454235 Color: 1

Bin 3974: 2185 of cap free
Amount of items: 2
Items: 
Size: 648570 Color: 4
Size: 349246 Color: 3

Bin 3975: 2193 of cap free
Amount of items: 2
Items: 
Size: 672915 Color: 1
Size: 324893 Color: 2

Bin 3976: 2200 of cap free
Amount of items: 2
Items: 
Size: 538867 Color: 2
Size: 458934 Color: 0

Bin 3977: 2209 of cap free
Amount of items: 2
Items: 
Size: 648563 Color: 2
Size: 349229 Color: 4

Bin 3978: 2238 of cap free
Amount of items: 2
Items: 
Size: 555437 Color: 1
Size: 442326 Color: 3

Bin 3979: 2241 of cap free
Amount of items: 2
Items: 
Size: 682177 Color: 2
Size: 315583 Color: 0

Bin 3980: 2243 of cap free
Amount of items: 2
Items: 
Size: 682153 Color: 1
Size: 315605 Color: 2

Bin 3981: 2243 of cap free
Amount of items: 2
Items: 
Size: 794793 Color: 4
Size: 202965 Color: 0

Bin 3982: 2244 of cap free
Amount of items: 2
Items: 
Size: 724837 Color: 0
Size: 272920 Color: 3

Bin 3983: 2272 of cap free
Amount of items: 2
Items: 
Size: 731693 Color: 0
Size: 266036 Color: 2

Bin 3984: 2281 of cap free
Amount of items: 2
Items: 
Size: 636233 Color: 4
Size: 361487 Color: 1

Bin 3985: 2283 of cap free
Amount of items: 2
Items: 
Size: 642614 Color: 3
Size: 355104 Color: 1

Bin 3986: 2309 of cap free
Amount of items: 2
Items: 
Size: 693809 Color: 0
Size: 303883 Color: 3

Bin 3987: 2331 of cap free
Amount of items: 2
Items: 
Size: 549046 Color: 0
Size: 448624 Color: 4

Bin 3988: 2336 of cap free
Amount of items: 2
Items: 
Size: 682116 Color: 4
Size: 315549 Color: 3

Bin 3989: 2357 of cap free
Amount of items: 2
Items: 
Size: 693710 Color: 3
Size: 303934 Color: 0

Bin 3990: 2394 of cap free
Amount of items: 2
Items: 
Size: 709240 Color: 1
Size: 288367 Color: 2

Bin 3991: 2425 of cap free
Amount of items: 2
Items: 
Size: 783636 Color: 2
Size: 213940 Color: 4

Bin 3992: 2429 of cap free
Amount of items: 8
Items: 
Size: 124905 Color: 3
Size: 124905 Color: 2
Size: 124813 Color: 2
Size: 124703 Color: 4
Size: 124690 Color: 3
Size: 124547 Color: 1
Size: 124545 Color: 1
Size: 124464 Color: 3

Bin 3993: 2432 of cap free
Amount of items: 2
Items: 
Size: 508299 Color: 2
Size: 489270 Color: 1

Bin 3994: 2435 of cap free
Amount of items: 2
Items: 
Size: 622111 Color: 2
Size: 375455 Color: 0

Bin 3995: 2439 of cap free
Amount of items: 2
Items: 
Size: 660288 Color: 1
Size: 337274 Color: 3

Bin 3996: 2450 of cap free
Amount of items: 2
Items: 
Size: 559918 Color: 3
Size: 437633 Color: 4

Bin 3997: 2470 of cap free
Amount of items: 2
Items: 
Size: 548933 Color: 2
Size: 448598 Color: 1

Bin 3998: 2526 of cap free
Amount of items: 2
Items: 
Size: 663945 Color: 3
Size: 333530 Color: 0

Bin 3999: 2548 of cap free
Amount of items: 2
Items: 
Size: 672873 Color: 3
Size: 324580 Color: 2

Bin 4000: 2567 of cap free
Amount of items: 2
Items: 
Size: 524480 Color: 2
Size: 472954 Color: 0

Bin 4001: 2589 of cap free
Amount of items: 2
Items: 
Size: 672866 Color: 4
Size: 324546 Color: 1

Bin 4002: 2593 of cap free
Amount of items: 2
Items: 
Size: 648181 Color: 3
Size: 349227 Color: 4

Bin 4003: 2609 of cap free
Amount of items: 2
Items: 
Size: 548848 Color: 0
Size: 448544 Color: 2

Bin 4004: 2617 of cap free
Amount of items: 2
Items: 
Size: 538733 Color: 3
Size: 458651 Color: 4

Bin 4005: 2617 of cap free
Amount of items: 2
Items: 
Size: 794463 Color: 3
Size: 202921 Color: 0

Bin 4006: 2624 of cap free
Amount of items: 2
Items: 
Size: 538768 Color: 4
Size: 458609 Color: 3

Bin 4007: 2638 of cap free
Amount of items: 2
Items: 
Size: 568284 Color: 2
Size: 429079 Color: 4

Bin 4008: 2746 of cap free
Amount of items: 2
Items: 
Size: 610428 Color: 0
Size: 386827 Color: 2

Bin 4009: 2782 of cap free
Amount of items: 2
Items: 
Size: 775417 Color: 2
Size: 221802 Color: 3

Bin 4010: 2810 of cap free
Amount of items: 2
Items: 
Size: 660072 Color: 1
Size: 337119 Color: 3

Bin 4011: 2812 of cap free
Amount of items: 2
Items: 
Size: 672762 Color: 2
Size: 324427 Color: 1

Bin 4012: 2852 of cap free
Amount of items: 2
Items: 
Size: 709689 Color: 2
Size: 287460 Color: 3

Bin 4013: 2861 of cap free
Amount of items: 2
Items: 
Size: 587974 Color: 3
Size: 409166 Color: 1

Bin 4014: 2867 of cap free
Amount of items: 2
Items: 
Size: 689992 Color: 2
Size: 307142 Color: 1

Bin 4015: 2893 of cap free
Amount of items: 2
Items: 
Size: 519555 Color: 3
Size: 477553 Color: 1

Bin 4016: 2898 of cap free
Amount of items: 2
Items: 
Size: 667516 Color: 3
Size: 329587 Color: 0

Bin 4017: 2924 of cap free
Amount of items: 2
Items: 
Size: 587949 Color: 3
Size: 409128 Color: 0

Bin 4018: 2940 of cap free
Amount of items: 2
Items: 
Size: 770370 Color: 1
Size: 226691 Color: 2

Bin 4019: 2966 of cap free
Amount of items: 2
Items: 
Size: 667505 Color: 3
Size: 329530 Color: 0

Bin 4020: 3030 of cap free
Amount of items: 2
Items: 
Size: 746264 Color: 0
Size: 250707 Color: 4

Bin 4021: 3048 of cap free
Amount of items: 2
Items: 
Size: 672584 Color: 1
Size: 324369 Color: 2

Bin 4022: 3058 of cap free
Amount of items: 2
Items: 
Size: 770355 Color: 2
Size: 226588 Color: 4

Bin 4023: 3061 of cap free
Amount of items: 2
Items: 
Size: 653215 Color: 0
Size: 343725 Color: 4

Bin 4024: 3065 of cap free
Amount of items: 2
Items: 
Size: 507717 Color: 2
Size: 489219 Color: 3

Bin 4025: 3068 of cap free
Amount of items: 2
Items: 
Size: 519844 Color: 1
Size: 477089 Color: 3

Bin 4026: 3077 of cap free
Amount of items: 2
Items: 
Size: 538542 Color: 4
Size: 458382 Color: 3

Bin 4027: 3079 of cap free
Amount of items: 2
Items: 
Size: 548881 Color: 2
Size: 448041 Color: 1

Bin 4028: 3083 of cap free
Amount of items: 6
Items: 
Size: 166321 Color: 1
Size: 166316 Color: 4
Size: 166307 Color: 1
Size: 166002 Color: 1
Size: 165998 Color: 4
Size: 165974 Color: 2

Bin 4029: 3139 of cap free
Amount of items: 2
Items: 
Size: 498494 Color: 3
Size: 498368 Color: 4

Bin 4030: 3145 of cap free
Amount of items: 2
Items: 
Size: 626417 Color: 1
Size: 370439 Color: 0

Bin 4031: 3151 of cap free
Amount of items: 2
Items: 
Size: 667333 Color: 0
Size: 329517 Color: 2

Bin 4032: 3176 of cap free
Amount of items: 2
Items: 
Size: 751353 Color: 3
Size: 245472 Color: 2

Bin 4033: 3216 of cap free
Amount of items: 2
Items: 
Size: 667325 Color: 0
Size: 329460 Color: 1

Bin 4034: 3218 of cap free
Amount of items: 2
Items: 
Size: 738488 Color: 3
Size: 258295 Color: 2

Bin 4035: 3227 of cap free
Amount of items: 2
Items: 
Size: 794298 Color: 4
Size: 202476 Color: 3

Bin 4036: 3242 of cap free
Amount of items: 2
Items: 
Size: 587900 Color: 3
Size: 408859 Color: 1

Bin 4037: 3248 of cap free
Amount of items: 2
Items: 
Size: 741455 Color: 2
Size: 255298 Color: 3

Bin 4038: 3250 of cap free
Amount of items: 2
Items: 
Size: 620989 Color: 3
Size: 375762 Color: 2

Bin 4039: 3254 of cap free
Amount of items: 2
Items: 
Size: 528360 Color: 0
Size: 468387 Color: 2

Bin 4040: 3258 of cap free
Amount of items: 2
Items: 
Size: 587894 Color: 1
Size: 408849 Color: 3

Bin 4041: 3309 of cap free
Amount of items: 2
Items: 
Size: 770318 Color: 0
Size: 226374 Color: 3

Bin 4042: 3311 of cap free
Amount of items: 2
Items: 
Size: 723978 Color: 2
Size: 272712 Color: 4

Bin 4043: 3340 of cap free
Amount of items: 2
Items: 
Size: 609975 Color: 2
Size: 386686 Color: 4

Bin 4044: 3349 of cap free
Amount of items: 2
Items: 
Size: 741746 Color: 3
Size: 254906 Color: 4

Bin 4045: 3367 of cap free
Amount of items: 2
Items: 
Size: 659532 Color: 3
Size: 337102 Color: 0

Bin 4046: 3373 of cap free
Amount of items: 2
Items: 
Size: 626320 Color: 1
Size: 370308 Color: 2

Bin 4047: 3381 of cap free
Amount of items: 2
Items: 
Size: 709378 Color: 2
Size: 287242 Color: 3

Bin 4048: 3399 of cap free
Amount of items: 2
Items: 
Size: 609966 Color: 2
Size: 386636 Color: 4

Bin 4049: 3413 of cap free
Amount of items: 2
Items: 
Size: 507305 Color: 4
Size: 489283 Color: 2

Bin 4050: 3435 of cap free
Amount of items: 2
Items: 
Size: 723952 Color: 1
Size: 272614 Color: 4

Bin 4051: 3444 of cap free
Amount of items: 2
Items: 
Size: 719524 Color: 1
Size: 277033 Color: 0

Bin 4052: 3446 of cap free
Amount of items: 2
Items: 
Size: 548832 Color: 1
Size: 447723 Color: 2

Bin 4053: 3460 of cap free
Amount of items: 2
Items: 
Size: 652966 Color: 0
Size: 343575 Color: 4

Bin 4054: 3463 of cap free
Amount of items: 2
Items: 
Size: 659463 Color: 0
Size: 337075 Color: 2

Bin 4055: 3465 of cap free
Amount of items: 2
Items: 
Size: 704525 Color: 3
Size: 292011 Color: 1

Bin 4056: 3484 of cap free
Amount of items: 2
Items: 
Size: 548810 Color: 4
Size: 447707 Color: 3

Bin 4057: 3486 of cap free
Amount of items: 7
Items: 
Size: 142516 Color: 3
Size: 142488 Color: 1
Size: 142448 Color: 0
Size: 142356 Color: 4
Size: 142282 Color: 3
Size: 142275 Color: 0
Size: 142150 Color: 3

Bin 4058: 3495 of cap free
Amount of items: 2
Items: 
Size: 600105 Color: 4
Size: 396401 Color: 3

Bin 4059: 3497 of cap free
Amount of items: 2
Items: 
Size: 609983 Color: 4
Size: 386521 Color: 3

Bin 4060: 3513 of cap free
Amount of items: 2
Items: 
Size: 685616 Color: 3
Size: 310872 Color: 4

Bin 4061: 3525 of cap free
Amount of items: 2
Items: 
Size: 723939 Color: 4
Size: 272537 Color: 1

Bin 4062: 3535 of cap free
Amount of items: 2
Items: 
Size: 672492 Color: 0
Size: 323974 Color: 3

Bin 4063: 3557 of cap free
Amount of items: 2
Items: 
Size: 528084 Color: 1
Size: 468360 Color: 3

Bin 4064: 3572 of cap free
Amount of items: 2
Items: 
Size: 609963 Color: 0
Size: 386466 Color: 2

Bin 4065: 3618 of cap free
Amount of items: 2
Items: 
Size: 587810 Color: 4
Size: 408573 Color: 3

Bin 4066: 3657 of cap free
Amount of items: 2
Items: 
Size: 731466 Color: 1
Size: 264878 Color: 2

Bin 4067: 3676 of cap free
Amount of items: 2
Items: 
Size: 681492 Color: 4
Size: 314833 Color: 1

Bin 4068: 3676 of cap free
Amount of items: 2
Items: 
Size: 709144 Color: 1
Size: 287181 Color: 0

Bin 4069: 3687 of cap free
Amount of items: 2
Items: 
Size: 626169 Color: 1
Size: 370145 Color: 4

Bin 4070: 3691 of cap free
Amount of items: 2
Items: 
Size: 659243 Color: 0
Size: 337067 Color: 1

Bin 4071: 3723 of cap free
Amount of items: 2
Items: 
Size: 559592 Color: 4
Size: 436686 Color: 0

Bin 4072: 3810 of cap free
Amount of items: 2
Items: 
Size: 507016 Color: 0
Size: 489175 Color: 4

Bin 4073: 3833 of cap free
Amount of items: 2
Items: 
Size: 794440 Color: 3
Size: 201728 Color: 1

Bin 4074: 3845 of cap free
Amount of items: 2
Items: 
Size: 636264 Color: 1
Size: 359892 Color: 4

Bin 4075: 3851 of cap free
Amount of items: 2
Items: 
Size: 519834 Color: 1
Size: 476316 Color: 2

Bin 4076: 3882 of cap free
Amount of items: 2
Items: 
Size: 555240 Color: 1
Size: 440879 Color: 4

Bin 4077: 3895 of cap free
Amount of items: 2
Items: 
Size: 704189 Color: 0
Size: 291917 Color: 2

Bin 4078: 3901 of cap free
Amount of items: 2
Items: 
Size: 719231 Color: 4
Size: 276869 Color: 1

Bin 4079: 3911 of cap free
Amount of items: 2
Items: 
Size: 686022 Color: 4
Size: 310068 Color: 1

Bin 4080: 3925 of cap free
Amount of items: 2
Items: 
Size: 708892 Color: 3
Size: 287184 Color: 1

Bin 4081: 3938 of cap free
Amount of items: 2
Items: 
Size: 555228 Color: 2
Size: 440835 Color: 1

Bin 4082: 4002 of cap free
Amount of items: 2
Items: 
Size: 794255 Color: 1
Size: 201744 Color: 3

Bin 4083: 4004 of cap free
Amount of items: 2
Items: 
Size: 609879 Color: 0
Size: 386118 Color: 2

Bin 4084: 4012 of cap free
Amount of items: 2
Items: 
Size: 704134 Color: 2
Size: 291855 Color: 0

Bin 4085: 4035 of cap free
Amount of items: 2
Items: 
Size: 752633 Color: 2
Size: 243333 Color: 3

Bin 4086: 4040 of cap free
Amount of items: 2
Items: 
Size: 741048 Color: 2
Size: 254913 Color: 3

Bin 4087: 4117 of cap free
Amount of items: 2
Items: 
Size: 783787 Color: 4
Size: 212097 Color: 0

Bin 4088: 4180 of cap free
Amount of items: 2
Items: 
Size: 548278 Color: 4
Size: 447543 Color: 2

Bin 4089: 4182 of cap free
Amount of items: 2
Items: 
Size: 555171 Color: 4
Size: 440648 Color: 2

Bin 4090: 4193 of cap free
Amount of items: 2
Items: 
Size: 519533 Color: 0
Size: 476275 Color: 4

Bin 4091: 4199 of cap free
Amount of items: 2
Items: 
Size: 770282 Color: 0
Size: 225520 Color: 3

Bin 4092: 4217 of cap free
Amount of items: 2
Items: 
Size: 685524 Color: 1
Size: 310260 Color: 4

Bin 4093: 4217 of cap free
Amount of items: 2
Items: 
Size: 770267 Color: 2
Size: 225517 Color: 3

Bin 4094: 4226 of cap free
Amount of items: 2
Items: 
Size: 609831 Color: 3
Size: 385944 Color: 2

Bin 4095: 4291 of cap free
Amount of items: 2
Items: 
Size: 555148 Color: 4
Size: 440562 Color: 0

Bin 4096: 4297 of cap free
Amount of items: 2
Items: 
Size: 736492 Color: 1
Size: 259212 Color: 3

Bin 4097: 4298 of cap free
Amount of items: 2
Items: 
Size: 794422 Color: 3
Size: 201281 Color: 4

Bin 4098: 4358 of cap free
Amount of items: 2
Items: 
Size: 783783 Color: 4
Size: 211860 Color: 2

Bin 4099: 4359 of cap free
Amount of items: 2
Items: 
Size: 781850 Color: 3
Size: 213792 Color: 4

Bin 4100: 4363 of cap free
Amount of items: 2
Items: 
Size: 555127 Color: 4
Size: 440511 Color: 1

Bin 4101: 4376 of cap free
Amount of items: 2
Items: 
Size: 506451 Color: 0
Size: 489174 Color: 1

Bin 4102: 4382 of cap free
Amount of items: 2
Items: 
Size: 658993 Color: 0
Size: 336626 Color: 1

Bin 4103: 4415 of cap free
Amount of items: 2
Items: 
Size: 506977 Color: 1
Size: 488609 Color: 2

Bin 4104: 4442 of cap free
Amount of items: 2
Items: 
Size: 527381 Color: 2
Size: 468178 Color: 4

Bin 4105: 4443 of cap free
Amount of items: 2
Items: 
Size: 672553 Color: 3
Size: 323005 Color: 4

Bin 4106: 4456 of cap free
Amount of items: 2
Items: 
Size: 555103 Color: 4
Size: 440442 Color: 0

Bin 4107: 4479 of cap free
Amount of items: 2
Items: 
Size: 527291 Color: 4
Size: 468231 Color: 2

Bin 4108: 4491 of cap free
Amount of items: 2
Items: 
Size: 600088 Color: 0
Size: 395422 Color: 3

Bin 4109: 4541 of cap free
Amount of items: 2
Items: 
Size: 794229 Color: 2
Size: 201231 Color: 0

Bin 4110: 4610 of cap free
Amount of items: 2
Items: 
Size: 519249 Color: 4
Size: 476142 Color: 3

Bin 4111: 4628 of cap free
Amount of items: 2
Items: 
Size: 736229 Color: 2
Size: 259144 Color: 3

Bin 4112: 4636 of cap free
Amount of items: 2
Items: 
Size: 685328 Color: 2
Size: 310037 Color: 4

Bin 4113: 4639 of cap free
Amount of items: 2
Items: 
Size: 646407 Color: 4
Size: 348955 Color: 3

Bin 4114: 4663 of cap free
Amount of items: 2
Items: 
Size: 600436 Color: 3
Size: 394902 Color: 2

Bin 4115: 4741 of cap free
Amount of items: 2
Items: 
Size: 587704 Color: 3
Size: 407556 Color: 0

Bin 4116: 4745 of cap free
Amount of items: 2
Items: 
Size: 587702 Color: 1
Size: 407554 Color: 2

Bin 4117: 4794 of cap free
Amount of items: 2
Items: 
Size: 600355 Color: 3
Size: 394852 Color: 2

Bin 4118: 4909 of cap free
Amount of items: 2
Items: 
Size: 793930 Color: 3
Size: 201162 Color: 4

Bin 4119: 4911 of cap free
Amount of items: 2
Items: 
Size: 624806 Color: 3
Size: 370284 Color: 1

Bin 4120: 5021 of cap free
Amount of items: 2
Items: 
Size: 587661 Color: 1
Size: 407319 Color: 3

Bin 4121: 5033 of cap free
Amount of items: 2
Items: 
Size: 498215 Color: 0
Size: 496753 Color: 1

Bin 4122: 5035 of cap free
Amount of items: 2
Items: 
Size: 568269 Color: 4
Size: 426697 Color: 0

Bin 4123: 5054 of cap free
Amount of items: 2
Items: 
Size: 548767 Color: 2
Size: 446180 Color: 1

Bin 4124: 5064 of cap free
Amount of items: 2
Items: 
Size: 498198 Color: 0
Size: 496739 Color: 1

Bin 4125: 5171 of cap free
Amount of items: 2
Items: 
Size: 624728 Color: 1
Size: 370102 Color: 2

Bin 4126: 5194 of cap free
Amount of items: 2
Items: 
Size: 600074 Color: 4
Size: 394733 Color: 1

Bin 4127: 5273 of cap free
Amount of items: 2
Items: 
Size: 794187 Color: 4
Size: 200541 Color: 2

Bin 4128: 5356 of cap free
Amount of items: 2
Items: 
Size: 658584 Color: 4
Size: 336061 Color: 1

Bin 4129: 5362 of cap free
Amount of items: 2
Items: 
Size: 794108 Color: 4
Size: 200531 Color: 0

Bin 4130: 5366 of cap free
Amount of items: 2
Items: 
Size: 781685 Color: 0
Size: 212950 Color: 4

Bin 4131: 5378 of cap free
Amount of items: 6
Items: 
Size: 165926 Color: 3
Size: 165893 Color: 3
Size: 165829 Color: 0
Size: 165719 Color: 4
Size: 165655 Color: 1
Size: 165601 Color: 3

Bin 4132: 5398 of cap free
Amount of items: 2
Items: 
Size: 567904 Color: 0
Size: 426699 Color: 4

Bin 4133: 5450 of cap free
Amount of items: 2
Items: 
Size: 624723 Color: 3
Size: 369828 Color: 1

Bin 4134: 5535 of cap free
Amount of items: 2
Items: 
Size: 567769 Color: 0
Size: 426697 Color: 4

Bin 4135: 5564 of cap free
Amount of items: 2
Items: 
Size: 536334 Color: 3
Size: 458103 Color: 0

Bin 4136: 5582 of cap free
Amount of items: 2
Items: 
Size: 793832 Color: 3
Size: 200587 Color: 4

Bin 4137: 5622 of cap free
Amount of items: 2
Items: 
Size: 781587 Color: 3
Size: 212792 Color: 4

Bin 4138: 5785 of cap free
Amount of items: 2
Items: 
Size: 609729 Color: 3
Size: 384487 Color: 4

Bin 4139: 5811 of cap free
Amount of items: 2
Items: 
Size: 751337 Color: 3
Size: 242853 Color: 2

Bin 4140: 5849 of cap free
Amount of items: 2
Items: 
Size: 506604 Color: 1
Size: 487548 Color: 3

Bin 4141: 5900 of cap free
Amount of items: 2
Items: 
Size: 547913 Color: 0
Size: 446188 Color: 2

Bin 4142: 5916 of cap free
Amount of items: 2
Items: 
Size: 624717 Color: 2
Size: 369368 Color: 4

Bin 4143: 5999 of cap free
Amount of items: 2
Items: 
Size: 793810 Color: 4
Size: 200192 Color: 0

Bin 4144: 6001 of cap free
Amount of items: 2
Items: 
Size: 624639 Color: 3
Size: 369361 Color: 4

Bin 4145: 6149 of cap free
Amount of items: 2
Items: 
Size: 793677 Color: 1
Size: 200175 Color: 0

Bin 4146: 6163 of cap free
Amount of items: 2
Items: 
Size: 645033 Color: 4
Size: 348805 Color: 1

Bin 4147: 6168 of cap free
Amount of items: 2
Items: 
Size: 519241 Color: 4
Size: 474592 Color: 2

Bin 4148: 6192 of cap free
Amount of items: 2
Items: 
Size: 587586 Color: 1
Size: 406223 Color: 3

Bin 4149: 6302 of cap free
Amount of items: 2
Items: 
Size: 751297 Color: 3
Size: 242402 Color: 2

Bin 4150: 6310 of cap free
Amount of items: 2
Items: 
Size: 548764 Color: 2
Size: 444927 Color: 1

Bin 4151: 6320 of cap free
Amount of items: 2
Items: 
Size: 548759 Color: 2
Size: 444922 Color: 3

Bin 4152: 6338 of cap free
Amount of items: 2
Items: 
Size: 644944 Color: 2
Size: 348719 Color: 1

Bin 4153: 6347 of cap free
Amount of items: 2
Items: 
Size: 567341 Color: 0
Size: 426313 Color: 1

Bin 4154: 6351 of cap free
Amount of items: 2
Items: 
Size: 751258 Color: 1
Size: 242392 Color: 0

Bin 4155: 6353 of cap free
Amount of items: 2
Items: 
Size: 527351 Color: 2
Size: 466297 Color: 4

Bin 4156: 6401 of cap free
Amount of items: 2
Items: 
Size: 587569 Color: 4
Size: 406031 Color: 3

Bin 4157: 6412 of cap free
Amount of items: 2
Items: 
Size: 607525 Color: 1
Size: 386064 Color: 3

Bin 4158: 6429 of cap free
Amount of items: 2
Items: 
Size: 729375 Color: 3
Size: 264197 Color: 2

Bin 4159: 6548 of cap free
Amount of items: 2
Items: 
Size: 781559 Color: 0
Size: 211894 Color: 4

Bin 4160: 6604 of cap free
Amount of items: 2
Items: 
Size: 527198 Color: 1
Size: 466199 Color: 4

Bin 4161: 6609 of cap free
Amount of items: 2
Items: 
Size: 527218 Color: 4
Size: 466174 Color: 0

Bin 4162: 6623 of cap free
Amount of items: 2
Items: 
Size: 587501 Color: 1
Size: 405877 Color: 0

Bin 4163: 6647 of cap free
Amount of items: 2
Items: 
Size: 729282 Color: 3
Size: 264072 Color: 2

Bin 4164: 6695 of cap free
Amount of items: 2
Items: 
Size: 781551 Color: 0
Size: 211755 Color: 1

Bin 4165: 6698 of cap free
Amount of items: 2
Items: 
Size: 567016 Color: 2
Size: 426287 Color: 0

Bin 4166: 6703 of cap free
Amount of items: 9
Items: 
Size: 110604 Color: 1
Size: 110603 Color: 1
Size: 110573 Color: 2
Size: 110476 Color: 3
Size: 110464 Color: 3
Size: 110198 Color: 0
Size: 110142 Color: 4
Size: 110127 Color: 2
Size: 110111 Color: 3

Bin 4167: 6721 of cap free
Amount of items: 2
Items: 
Size: 496673 Color: 0
Size: 496607 Color: 2

Bin 4168: 6733 of cap free
Amount of items: 2
Items: 
Size: 599662 Color: 1
Size: 393606 Color: 0

Bin 4169: 6735 of cap free
Amount of items: 2
Items: 
Size: 567169 Color: 0
Size: 426097 Color: 3

Bin 4170: 6756 of cap free
Amount of items: 2
Items: 
Size: 781535 Color: 0
Size: 211710 Color: 2

Bin 4171: 6848 of cap free
Amount of items: 2
Items: 
Size: 567056 Color: 0
Size: 426097 Color: 2

Bin 4172: 6891 of cap free
Amount of items: 2
Items: 
Size: 644828 Color: 1
Size: 348282 Color: 4

Bin 4173: 6908 of cap free
Amount of items: 6
Items: 
Size: 165571 Color: 2
Size: 165567 Color: 4
Size: 165546 Color: 0
Size: 165496 Color: 0
Size: 165463 Color: 2
Size: 165450 Color: 0

Bin 4174: 6940 of cap free
Amount of items: 7
Items: 
Size: 142070 Color: 0
Size: 142022 Color: 4
Size: 141874 Color: 4
Size: 141834 Color: 0
Size: 141831 Color: 0
Size: 141741 Color: 4
Size: 141689 Color: 4

Bin 4175: 6969 of cap free
Amount of items: 2
Items: 
Size: 599677 Color: 0
Size: 393355 Color: 2

Bin 4176: 7022 of cap free
Amount of items: 2
Items: 
Size: 587329 Color: 0
Size: 405650 Color: 2

Bin 4177: 7218 of cap free
Amount of items: 2
Items: 
Size: 587315 Color: 2
Size: 405468 Color: 3

Bin 4178: 7307 of cap free
Amount of items: 2
Items: 
Size: 566594 Color: 1
Size: 426100 Color: 0

Bin 4179: 7323 of cap free
Amount of items: 2
Items: 
Size: 587249 Color: 3
Size: 405429 Color: 0

Bin 4180: 7457 of cap free
Amount of items: 2
Items: 
Size: 566537 Color: 0
Size: 426007 Color: 1

Bin 4181: 7492 of cap free
Amount of items: 2
Items: 
Size: 566551 Color: 1
Size: 425958 Color: 0

Bin 4182: 7546 of cap free
Amount of items: 2
Items: 
Size: 587232 Color: 0
Size: 405223 Color: 2

Bin 4183: 7564 of cap free
Amount of items: 2
Items: 
Size: 566443 Color: 2
Size: 425994 Color: 1

Bin 4184: 7985 of cap free
Amount of items: 2
Items: 
Size: 566046 Color: 0
Size: 425970 Color: 1

Bin 4185: 8009 of cap free
Amount of items: 2
Items: 
Size: 658465 Color: 3
Size: 333527 Color: 1

Bin 4186: 8083 of cap free
Amount of items: 2
Items: 
Size: 792041 Color: 2
Size: 199877 Color: 3

Bin 4187: 8100 of cap free
Amount of items: 2
Items: 
Size: 519021 Color: 4
Size: 472880 Color: 0

Bin 4188: 8209 of cap free
Amount of items: 2
Items: 
Size: 781379 Color: 2
Size: 210413 Color: 4

Bin 4189: 8253 of cap free
Amount of items: 2
Items: 
Size: 791982 Color: 3
Size: 199766 Color: 4

Bin 4190: 8279 of cap free
Amount of items: 2
Items: 
Size: 791959 Color: 4
Size: 199763 Color: 1

Bin 4191: 8321 of cap free
Amount of items: 2
Items: 
Size: 565888 Color: 4
Size: 425792 Color: 2

Bin 4192: 8328 of cap free
Amount of items: 2
Items: 
Size: 791936 Color: 1
Size: 199737 Color: 4

Bin 4193: 8338 of cap free
Amount of items: 2
Items: 
Size: 565875 Color: 3
Size: 425788 Color: 2

Bin 4194: 8342 of cap free
Amount of items: 2
Items: 
Size: 607182 Color: 2
Size: 384477 Color: 1

Bin 4195: 8378 of cap free
Amount of items: 2
Items: 
Size: 607176 Color: 4
Size: 384447 Color: 2

Bin 4196: 8564 of cap free
Amount of items: 6
Items: 
Size: 165344 Color: 1
Size: 165340 Color: 4
Size: 165293 Color: 2
Size: 165241 Color: 2
Size: 165129 Color: 0
Size: 165090 Color: 0

Bin 4197: 8705 of cap free
Amount of items: 2
Items: 
Size: 791853 Color: 1
Size: 199443 Color: 4

Bin 4198: 8748 of cap free
Amount of items: 8
Items: 
Size: 124420 Color: 4
Size: 124388 Color: 0
Size: 124094 Color: 3
Size: 124092 Color: 2
Size: 123896 Color: 3
Size: 123614 Color: 0
Size: 123452 Color: 3
Size: 123297 Color: 0

Bin 4199: 9343 of cap free
Amount of items: 2
Items: 
Size: 791400 Color: 4
Size: 199258 Color: 0

Bin 4200: 9409 of cap free
Amount of items: 2
Items: 
Size: 736216 Color: 2
Size: 254376 Color: 4

Bin 4201: 9434 of cap free
Amount of items: 2
Items: 
Size: 736214 Color: 4
Size: 254353 Color: 0

Bin 4202: 9446 of cap free
Amount of items: 2
Items: 
Size: 736206 Color: 3
Size: 254349 Color: 1

Bin 4203: 9619 of cap free
Amount of items: 2
Items: 
Size: 680328 Color: 1
Size: 310054 Color: 2

Bin 4204: 9670 of cap free
Amount of items: 7
Items: 
Size: 141661 Color: 1
Size: 141629 Color: 4
Size: 141567 Color: 0
Size: 141525 Color: 2
Size: 141331 Color: 3
Size: 141312 Color: 0
Size: 141306 Color: 1

Bin 4205: 10083 of cap free
Amount of items: 2
Items: 
Size: 736192 Color: 4
Size: 253726 Color: 0

Bin 4206: 10232 of cap free
Amount of items: 2
Items: 
Size: 641321 Color: 0
Size: 348448 Color: 1

Bin 4207: 10283 of cap free
Amount of items: 2
Items: 
Size: 565811 Color: 2
Size: 423907 Color: 0

Bin 4208: 10321 of cap free
Amount of items: 2
Items: 
Size: 735994 Color: 1
Size: 253686 Color: 0

Bin 4209: 10419 of cap free
Amount of items: 2
Items: 
Size: 790357 Color: 2
Size: 199225 Color: 4

Bin 4210: 10497 of cap free
Amount of items: 2
Items: 
Size: 495931 Color: 1
Size: 493573 Color: 4

Bin 4211: 10740 of cap free
Amount of items: 6
Items: 
Size: 164991 Color: 3
Size: 164970 Color: 0
Size: 164925 Color: 2
Size: 164899 Color: 0
Size: 164855 Color: 2
Size: 164621 Color: 0

Bin 4212: 10902 of cap free
Amount of items: 2
Items: 
Size: 606473 Color: 2
Size: 382626 Color: 0

Bin 4213: 11144 of cap free
Amount of items: 2
Items: 
Size: 789646 Color: 4
Size: 199211 Color: 3

Bin 4214: 11305 of cap free
Amount of items: 2
Items: 
Size: 565789 Color: 0
Size: 422907 Color: 1

Bin 4215: 11476 of cap free
Amount of items: 2
Items: 
Size: 789495 Color: 4
Size: 199030 Color: 2

Bin 4216: 11515 of cap free
Amount of items: 2
Items: 
Size: 640248 Color: 0
Size: 348238 Color: 1

Bin 4217: 11537 of cap free
Amount of items: 2
Items: 
Size: 547946 Color: 2
Size: 440518 Color: 4

Bin 4218: 11807 of cap free
Amount of items: 2
Items: 
Size: 565662 Color: 0
Size: 422532 Color: 1

Bin 4219: 11894 of cap free
Amount of items: 2
Items: 
Size: 565585 Color: 2
Size: 422522 Color: 3

Bin 4220: 11925 of cap free
Amount of items: 2
Items: 
Size: 547836 Color: 3
Size: 440240 Color: 4

Bin 4221: 11952 of cap free
Amount of items: 2
Items: 
Size: 547835 Color: 3
Size: 440214 Color: 4

Bin 4222: 12275 of cap free
Amount of items: 2
Items: 
Size: 547551 Color: 1
Size: 440175 Color: 4

Bin 4223: 12328 of cap free
Amount of items: 2
Items: 
Size: 547548 Color: 0
Size: 440125 Color: 4

Bin 4224: 12386 of cap free
Amount of items: 2
Items: 
Size: 565140 Color: 1
Size: 422475 Color: 3

Bin 4225: 12464 of cap free
Amount of items: 7
Items: 
Size: 141248 Color: 3
Size: 141227 Color: 0
Size: 141215 Color: 1
Size: 141051 Color: 4
Size: 140971 Color: 3
Size: 140918 Color: 2
Size: 140907 Color: 0

Bin 4226: 12770 of cap free
Amount of items: 2
Items: 
Size: 564786 Color: 2
Size: 422445 Color: 1

Bin 4227: 12917 of cap free
Amount of items: 2
Items: 
Size: 680261 Color: 0
Size: 306823 Color: 3

Bin 4228: 12963 of cap free
Amount of items: 2
Items: 
Size: 680236 Color: 0
Size: 306802 Color: 2

Bin 4229: 13093 of cap free
Amount of items: 2
Items: 
Size: 680143 Color: 3
Size: 306765 Color: 1

Bin 4230: 13365 of cap free
Amount of items: 2
Items: 
Size: 680063 Color: 3
Size: 306573 Color: 2

Bin 4231: 13415 of cap free
Amount of items: 6
Items: 
Size: 164554 Color: 3
Size: 164468 Color: 2
Size: 164465 Color: 1
Size: 164384 Color: 3
Size: 164375 Color: 2
Size: 164340 Color: 2

Bin 4232: 13573 of cap free
Amount of items: 2
Items: 
Size: 680062 Color: 4
Size: 306366 Color: 2

Bin 4233: 13611 of cap free
Amount of items: 3
Items: 
Size: 597143 Color: 3
Size: 194688 Color: 4
Size: 194559 Color: 2

Bin 4234: 13919 of cap free
Amount of items: 9
Items: 
Size: 109905 Color: 1
Size: 109816 Color: 2
Size: 109566 Color: 3
Size: 109510 Color: 4
Size: 109500 Color: 3
Size: 109484 Color: 0
Size: 109454 Color: 3
Size: 109452 Color: 4
Size: 109395 Color: 3

Bin 4235: 14225 of cap free
Amount of items: 3
Items: 
Size: 596877 Color: 4
Size: 194532 Color: 0
Size: 194367 Color: 0

Bin 4236: 14388 of cap free
Amount of items: 8
Items: 
Size: 123267 Color: 1
Size: 123264 Color: 2
Size: 123260 Color: 1
Size: 123220 Color: 0
Size: 123208 Color: 2
Size: 123176 Color: 0
Size: 123139 Color: 0
Size: 123079 Color: 0

Bin 4237: 14881 of cap free
Amount of items: 3
Items: 
Size: 596526 Color: 2
Size: 194344 Color: 1
Size: 194250 Color: 1

Bin 4238: 14999 of cap free
Amount of items: 2
Items: 
Size: 518999 Color: 1
Size: 466003 Color: 2

Bin 4239: 15119 of cap free
Amount of items: 2
Items: 
Size: 547257 Color: 1
Size: 437625 Color: 4

Bin 4240: 15163 of cap free
Amount of items: 2
Items: 
Size: 518912 Color: 3
Size: 465926 Color: 2

Bin 4241: 15473 of cap free
Amount of items: 3
Items: 
Size: 596814 Color: 1
Size: 193870 Color: 0
Size: 193844 Color: 1

Bin 4242: 15544 of cap free
Amount of items: 2
Items: 
Size: 547802 Color: 4
Size: 436655 Color: 0

Bin 4243: 15655 of cap free
Amount of items: 2
Items: 
Size: 518870 Color: 3
Size: 465476 Color: 2

Bin 4244: 15816 of cap free
Amount of items: 7
Items: 
Size: 140841 Color: 0
Size: 140741 Color: 3
Size: 140709 Color: 4
Size: 140610 Color: 1
Size: 140483 Color: 4
Size: 140416 Color: 4
Size: 140385 Color: 0

Bin 4245: 15836 of cap free
Amount of items: 2
Items: 
Size: 636140 Color: 3
Size: 348025 Color: 0

Bin 4246: 15944 of cap free
Amount of items: 3
Items: 
Size: 595990 Color: 4
Size: 194112 Color: 1
Size: 193955 Color: 2

Bin 4247: 16026 of cap free
Amount of items: 3
Items: 
Size: 596804 Color: 1
Size: 193592 Color: 2
Size: 193579 Color: 0

Bin 4248: 16055 of cap free
Amount of items: 2
Items: 
Size: 518867 Color: 4
Size: 465079 Color: 2

Bin 4249: 16161 of cap free
Amount of items: 2
Items: 
Size: 518834 Color: 2
Size: 465006 Color: 1

Bin 4250: 16183 of cap free
Amount of items: 2
Items: 
Size: 636138 Color: 0
Size: 347680 Color: 4

Bin 4251: 16320 of cap free
Amount of items: 6
Items: 
Size: 164212 Color: 0
Size: 164100 Color: 0
Size: 164084 Color: 1
Size: 163879 Color: 1
Size: 163776 Color: 1
Size: 163630 Color: 4

Bin 4252: 16338 of cap free
Amount of items: 2
Items: 
Size: 547088 Color: 3
Size: 436575 Color: 4

Bin 4253: 16433 of cap free
Amount of items: 2
Items: 
Size: 547037 Color: 0
Size: 436531 Color: 1

Bin 4254: 16677 of cap free
Amount of items: 3
Items: 
Size: 595794 Color: 4
Size: 193822 Color: 1
Size: 193708 Color: 1

Bin 4255: 16683 of cap free
Amount of items: 2
Items: 
Size: 680051 Color: 2
Size: 303267 Color: 4

Bin 4256: 16920 of cap free
Amount of items: 3
Items: 
Size: 595753 Color: 2
Size: 193675 Color: 1
Size: 193653 Color: 3

Bin 4257: 17246 of cap free
Amount of items: 2
Items: 
Size: 635506 Color: 3
Size: 347249 Color: 1

Bin 4258: 17299 of cap free
Amount of items: 2
Items: 
Size: 635470 Color: 0
Size: 347232 Color: 1

Bin 4259: 17713 of cap free
Amount of items: 2
Items: 
Size: 517415 Color: 2
Size: 464873 Color: 3

Bin 4260: 17848 of cap free
Amount of items: 2
Items: 
Size: 517384 Color: 4
Size: 464769 Color: 3

Bin 4261: 17906 of cap free
Amount of items: 3
Items: 
Size: 595089 Color: 2
Size: 193519 Color: 3
Size: 193487 Color: 0

Bin 4262: 17973 of cap free
Amount of items: 2
Items: 
Size: 517381 Color: 1
Size: 464647 Color: 3

Bin 4263: 18222 of cap free
Amount of items: 3
Items: 
Size: 595118 Color: 3
Size: 193354 Color: 1
Size: 193307 Color: 3

Bin 4264: 18557 of cap free
Amount of items: 2
Items: 
Size: 517288 Color: 1
Size: 464156 Color: 3

Bin 4265: 18615 of cap free
Amount of items: 2
Items: 
Size: 545747 Color: 1
Size: 435639 Color: 3

Bin 4266: 18655 of cap free
Amount of items: 2
Items: 
Size: 680037 Color: 0
Size: 301309 Color: 3

Bin 4267: 18917 of cap free
Amount of items: 2
Items: 
Size: 517016 Color: 1
Size: 464068 Color: 3

Bin 4268: 19011 of cap free
Amount of items: 7
Items: 
Size: 140371 Color: 4
Size: 140366 Color: 0
Size: 140194 Color: 0
Size: 140114 Color: 4
Size: 140090 Color: 2
Size: 139993 Color: 4
Size: 139862 Color: 2

Bin 4269: 19115 of cap free
Amount of items: 2
Items: 
Size: 680036 Color: 1
Size: 300850 Color: 0

Bin 4270: 19181 of cap free
Amount of items: 9
Items: 
Size: 109376 Color: 1
Size: 109280 Color: 3
Size: 109020 Color: 1
Size: 108983 Color: 1
Size: 108893 Color: 0
Size: 108960 Color: 1
Size: 108716 Color: 4
Size: 108959 Color: 1
Size: 108633 Color: 4

Bin 4271: 19236 of cap free
Amount of items: 2
Items: 
Size: 598819 Color: 0
Size: 381946 Color: 2

Bin 4272: 19246 of cap free
Amount of items: 2
Items: 
Size: 633629 Color: 0
Size: 347126 Color: 3

Bin 4273: 19275 of cap free
Amount of items: 3
Items: 
Size: 595054 Color: 3
Size: 192844 Color: 4
Size: 192828 Color: 3

Bin 4274: 19354 of cap free
Amount of items: 8
Items: 
Size: 122967 Color: 3
Size: 122792 Color: 4
Size: 122650 Color: 2
Size: 122626 Color: 1
Size: 122585 Color: 0
Size: 122480 Color: 0
Size: 122394 Color: 4
Size: 122153 Color: 1

Bin 4275: 19373 of cap free
Amount of items: 2
Items: 
Size: 679878 Color: 2
Size: 300750 Color: 3

Bin 4276: 19457 of cap free
Amount of items: 2
Items: 
Size: 679856 Color: 0
Size: 300688 Color: 4

Bin 4277: 19670 of cap free
Amount of items: 2
Items: 
Size: 781306 Color: 2
Size: 199025 Color: 4

Bin 4278: 19816 of cap free
Amount of items: 2
Items: 
Size: 729385 Color: 2
Size: 250800 Color: 0

Bin 4279: 19954 of cap free
Amount of items: 6
Items: 
Size: 163595 Color: 4
Size: 163536 Color: 0
Size: 163472 Color: 3
Size: 163207 Color: 0
Size: 163136 Color: 4
Size: 163101 Color: 0

Bin 4280: 20039 of cap free
Amount of items: 2
Items: 
Size: 597698 Color: 2
Size: 382264 Color: 0

Bin 4281: 20085 of cap free
Amount of items: 2
Items: 
Size: 597665 Color: 1
Size: 382251 Color: 0

Bin 4282: 20468 of cap free
Amount of items: 2
Items: 
Size: 780537 Color: 4
Size: 198996 Color: 3

Bin 4283: 20519 of cap free
Amount of items: 2
Items: 
Size: 780498 Color: 0
Size: 198984 Color: 4

Bin 4284: 20774 of cap free
Amount of items: 2
Items: 
Size: 543590 Color: 1
Size: 435637 Color: 4

Bin 4285: 21006 of cap free
Amount of items: 2
Items: 
Size: 597636 Color: 0
Size: 381359 Color: 4

Bin 4286: 21142 of cap free
Amount of items: 2
Items: 
Size: 631753 Color: 4
Size: 347106 Color: 0

Bin 4287: 21306 of cap free
Amount of items: 2
Items: 
Size: 597573 Color: 2
Size: 381122 Color: 4

Bin 4288: 21436 of cap free
Amount of items: 2
Items: 
Size: 729273 Color: 3
Size: 249292 Color: 0

Bin 4289: 21600 of cap free
Amount of items: 2
Items: 
Size: 677773 Color: 1
Size: 300628 Color: 3

Bin 4290: 21697 of cap free
Amount of items: 2
Items: 
Size: 729207 Color: 0
Size: 249097 Color: 3

Bin 4291: 21735 of cap free
Amount of items: 2
Items: 
Size: 779301 Color: 2
Size: 198965 Color: 0

Bin 4292: 21814 of cap free
Amount of items: 2
Items: 
Size: 729105 Color: 2
Size: 249082 Color: 4

Bin 4293: 21964 of cap free
Amount of items: 2
Items: 
Size: 677537 Color: 1
Size: 300500 Color: 2

Bin 4294: 22171 of cap free
Amount of items: 2
Items: 
Size: 677355 Color: 3
Size: 300475 Color: 4

Bin 4295: 22213 of cap free
Amount of items: 7
Items: 
Size: 139853 Color: 2
Size: 139780 Color: 3
Size: 139778 Color: 1
Size: 139682 Color: 2
Size: 139627 Color: 3
Size: 139567 Color: 1
Size: 139501 Color: 2

Bin 4296: 22402 of cap free
Amount of items: 2
Items: 
Size: 677172 Color: 2
Size: 300427 Color: 3

Bin 4297: 22650 of cap free
Amount of items: 2
Items: 
Size: 778645 Color: 1
Size: 198706 Color: 2

Bin 4298: 22677 of cap free
Amount of items: 2
Items: 
Size: 778750 Color: 2
Size: 198574 Color: 1

Bin 4299: 22832 of cap free
Amount of items: 2
Items: 
Size: 778520 Color: 0
Size: 198649 Color: 2

Bin 4300: 22996 of cap free
Amount of items: 2
Items: 
Size: 778515 Color: 2
Size: 198490 Color: 4

Bin 4301: 23436 of cap free
Amount of items: 6
Items: 
Size: 162954 Color: 3
Size: 162796 Color: 2
Size: 162779 Color: 0
Size: 162701 Color: 1
Size: 162691 Color: 3
Size: 162644 Color: 1

Bin 4302: 24875 of cap free
Amount of items: 8
Items: 
Size: 122062 Color: 1
Size: 122060 Color: 4
Size: 122039 Color: 1
Size: 121986 Color: 1
Size: 121980 Color: 4
Size: 121722 Color: 3
Size: 121692 Color: 0
Size: 121585 Color: 2

Bin 4303: 25217 of cap free
Amount of items: 2
Items: 
Size: 517023 Color: 3
Size: 457761 Color: 1

Bin 4304: 25303 of cap free
Amount of items: 7
Items: 
Size: 139359 Color: 2
Size: 139275 Color: 2
Size: 139274 Color: 4
Size: 139271 Color: 2
Size: 139168 Color: 3
Size: 139188 Color: 2
Size: 139163 Color: 3

Bin 4305: 25378 of cap free
Amount of items: 9
Items: 
Size: 108565 Color: 4
Size: 108534 Color: 4
Size: 108461 Color: 2
Size: 108434 Color: 3
Size: 108397 Color: 4
Size: 108162 Color: 3
Size: 108042 Color: 3
Size: 108026 Color: 0
Size: 108002 Color: 2

Bin 4306: 25876 of cap free
Amount of items: 6
Items: 
Size: 162506 Color: 1
Size: 162492 Color: 3
Size: 162367 Color: 4
Size: 162287 Color: 0
Size: 162253 Color: 2
Size: 162220 Color: 1

Bin 4307: 25977 of cap free
Amount of items: 3
Items: 
Size: 587163 Color: 0
Size: 193448 Color: 3
Size: 193413 Color: 2

Bin 4308: 26146 of cap free
Amount of items: 2
Items: 
Size: 775409 Color: 0
Size: 198446 Color: 4

Bin 4309: 26388 of cap free
Amount of items: 2
Items: 
Size: 775217 Color: 2
Size: 198396 Color: 1

Bin 4310: 26479 of cap free
Amount of items: 3
Items: 
Size: 587134 Color: 4
Size: 193277 Color: 3
Size: 193111 Color: 2

Bin 4311: 26595 of cap free
Amount of items: 2
Items: 
Size: 775199 Color: 1
Size: 198207 Color: 0

Bin 4312: 26907 of cap free
Amount of items: 2
Items: 
Size: 775166 Color: 0
Size: 197928 Color: 2

Bin 4313: 26950 of cap free
Amount of items: 3
Items: 
Size: 586991 Color: 0
Size: 193074 Color: 3
Size: 192986 Color: 3

Bin 4314: 27048 of cap free
Amount of items: 2
Items: 
Size: 775104 Color: 4
Size: 197849 Color: 1

Bin 4315: 27183 of cap free
Amount of items: 2
Items: 
Size: 775089 Color: 0
Size: 197729 Color: 4

Bin 4316: 27212 of cap free
Amount of items: 6
Items: 
Size: 162220 Color: 0
Size: 162179 Color: 2
Size: 162159 Color: 2
Size: 162097 Color: 3
Size: 162073 Color: 0
Size: 162061 Color: 2

Bin 4317: 27881 of cap free
Amount of items: 7
Items: 
Size: 139061 Color: 4
Size: 138900 Color: 2
Size: 138899 Color: 1
Size: 138862 Color: 3
Size: 138860 Color: 1
Size: 138776 Color: 0
Size: 138762 Color: 2

Bin 4318: 28421 of cap free
Amount of items: 3
Items: 
Size: 586025 Color: 1
Size: 192791 Color: 2
Size: 192764 Color: 0

Bin 4319: 28541 of cap free
Amount of items: 2
Items: 
Size: 671077 Color: 0
Size: 300383 Color: 4

Bin 4320: 28597 of cap free
Amount of items: 2
Items: 
Size: 729031 Color: 0
Size: 242373 Color: 2

Bin 4321: 28617 of cap free
Amount of items: 2
Items: 
Size: 671026 Color: 4
Size: 300358 Color: 0

Bin 4322: 28698 of cap free
Amount of items: 3
Items: 
Size: 586022 Color: 3
Size: 192705 Color: 0
Size: 192576 Color: 1

Bin 4323: 28723 of cap free
Amount of items: 8
Items: 
Size: 121563 Color: 1
Size: 121500 Color: 0
Size: 121489 Color: 1
Size: 121425 Color: 4
Size: 121419 Color: 1
Size: 121330 Color: 3
Size: 121302 Color: 1
Size: 121250 Color: 3

Bin 4324: 28777 of cap free
Amount of items: 2
Items: 
Size: 728860 Color: 1
Size: 242364 Color: 4

Bin 4325: 28822 of cap free
Amount of items: 2
Items: 
Size: 773427 Color: 3
Size: 197752 Color: 0

Bin 4326: 28907 of cap free
Amount of items: 2
Items: 
Size: 728828 Color: 1
Size: 242266 Color: 0

Bin 4327: 28908 of cap free
Amount of items: 6
Items: 
Size: 162036 Color: 2
Size: 161915 Color: 0
Size: 161882 Color: 3
Size: 161846 Color: 1
Size: 161734 Color: 1
Size: 161680 Color: 4

Bin 4328: 28953 of cap free
Amount of items: 2
Items: 
Size: 671064 Color: 0
Size: 299984 Color: 3

Bin 4329: 28987 of cap free
Amount of items: 3
Items: 
Size: 585993 Color: 3
Size: 192541 Color: 0
Size: 192480 Color: 0

Bin 4330: 29050 of cap free
Amount of items: 2
Items: 
Size: 728628 Color: 0
Size: 242323 Color: 1

Bin 4331: 29293 of cap free
Amount of items: 3
Items: 
Size: 585842 Color: 1
Size: 192449 Color: 4
Size: 192417 Color: 3

Bin 4332: 29392 of cap free
Amount of items: 2
Items: 
Size: 670645 Color: 4
Size: 299964 Color: 1

Bin 4333: 29500 of cap free
Amount of items: 3
Items: 
Size: 585766 Color: 1
Size: 192416 Color: 3
Size: 192319 Color: 2

Bin 4334: 29543 of cap free
Amount of items: 2
Items: 
Size: 670620 Color: 1
Size: 299838 Color: 2

Bin 4335: 29804 of cap free
Amount of items: 7
Items: 
Size: 138728 Color: 1
Size: 138722 Color: 0
Size: 138686 Color: 0
Size: 138593 Color: 3
Size: 138561 Color: 3
Size: 138455 Color: 2
Size: 138452 Color: 0

Bin 4336: 30017 of cap free
Amount of items: 3
Items: 
Size: 585506 Color: 2
Size: 192249 Color: 3
Size: 192229 Color: 0

Bin 4337: 30241 of cap free
Amount of items: 3
Items: 
Size: 585607 Color: 3
Size: 192110 Color: 0
Size: 192043 Color: 1

Bin 4338: 30697 of cap free
Amount of items: 2
Items: 
Size: 670490 Color: 1
Size: 298814 Color: 0

Bin 4339: 30862 of cap free
Amount of items: 9
Items: 
Size: 107917 Color: 0
Size: 107901 Color: 4
Size: 107819 Color: 2
Size: 107782 Color: 1
Size: 107711 Color: 1
Size: 107698 Color: 0
Size: 107499 Color: 1
Size: 107417 Color: 0
Size: 107395 Color: 1

Bin 4340: 30993 of cap free
Amount of items: 6
Items: 
Size: 161652 Color: 4
Size: 161649 Color: 2
Size: 161647 Color: 4
Size: 161582 Color: 0
Size: 161284 Color: 4
Size: 161194 Color: 3

Bin 4341: 31031 of cap free
Amount of items: 3
Items: 
Size: 585496 Color: 4
Size: 191888 Color: 0
Size: 191586 Color: 2

Bin 4342: 31199 of cap free
Amount of items: 2
Items: 
Size: 728634 Color: 1
Size: 240168 Color: 2

Bin 4343: 31277 of cap free
Amount of items: 2
Items: 
Size: 728626 Color: 3
Size: 240098 Color: 4

Bin 4344: 31398 of cap free
Amount of items: 2
Items: 
Size: 728554 Color: 3
Size: 240049 Color: 1

Bin 4345: 31488 of cap free
Amount of items: 2
Items: 
Size: 728478 Color: 0
Size: 240035 Color: 2

Bin 4346: 31496 of cap free
Amount of items: 3
Items: 
Size: 585432 Color: 1
Size: 191543 Color: 4
Size: 191530 Color: 3

Bin 4347: 31589 of cap free
Amount of items: 3
Items: 
Size: 585409 Color: 3
Size: 191520 Color: 4
Size: 191483 Color: 2

Bin 4348: 31711 of cap free
Amount of items: 3
Items: 
Size: 585481 Color: 4
Size: 191427 Color: 1
Size: 191382 Color: 3

Bin 4349: 31785 of cap free
Amount of items: 7
Items: 
Size: 138424 Color: 1
Size: 138397 Color: 3
Size: 138387 Color: 4
Size: 138360 Color: 3
Size: 138342 Color: 2
Size: 138167 Color: 0
Size: 138139 Color: 2

Bin 4350: 32078 of cap free
Amount of items: 2
Items: 
Size: 770249 Color: 4
Size: 197674 Color: 1

Bin 4351: 32231 of cap free
Amount of items: 2
Items: 
Size: 770224 Color: 1
Size: 197546 Color: 4

Bin 4352: 32299 of cap free
Amount of items: 3
Items: 
Size: 585389 Color: 0
Size: 191162 Color: 1
Size: 191151 Color: 4

Bin 4353: 32372 of cap free
Amount of items: 2
Items: 
Size: 770153 Color: 1
Size: 197476 Color: 4

Bin 4354: 32419 of cap free
Amount of items: 2
Items: 
Size: 728097 Color: 2
Size: 239485 Color: 0

Bin 4355: 32438 of cap free
Amount of items: 2
Items: 
Size: 728037 Color: 1
Size: 239526 Color: 2

Bin 4356: 32512 of cap free
Amount of items: 2
Items: 
Size: 728006 Color: 3
Size: 239483 Color: 1

Bin 4357: 32531 of cap free
Amount of items: 3
Items: 
Size: 585396 Color: 1
Size: 191056 Color: 0
Size: 191018 Color: 0

Bin 4358: 32623 of cap free
Amount of items: 2
Items: 
Size: 727964 Color: 2
Size: 239414 Color: 4

Bin 4359: 32858 of cap free
Amount of items: 3
Items: 
Size: 585185 Color: 1
Size: 190989 Color: 4
Size: 190969 Color: 1

Bin 4360: 33741 of cap free
Amount of items: 3
Items: 
Size: 584525 Color: 3
Size: 190888 Color: 1
Size: 190847 Color: 0

Bin 4361: 33890 of cap free
Amount of items: 2
Items: 
Size: 667157 Color: 3
Size: 298954 Color: 1

Bin 4362: 34084 of cap free
Amount of items: 3
Items: 
Size: 584319 Color: 3
Size: 190815 Color: 2
Size: 190783 Color: 4

Bin 4363: 34119 of cap free
Amount of items: 2
Items: 
Size: 543501 Color: 0
Size: 422381 Color: 2

Bin 4364: 34287 of cap free
Amount of items: 2
Items: 
Size: 768482 Color: 0
Size: 197232 Color: 1

Bin 4365: 34442 of cap free
Amount of items: 3
Items: 
Size: 584027 Color: 1
Size: 190769 Color: 3
Size: 190763 Color: 2

Bin 4366: 34583 of cap free
Amount of items: 8
Items: 
Size: 120976 Color: 1
Size: 120875 Color: 0
Size: 120718 Color: 1
Size: 120693 Color: 3
Size: 120600 Color: 4
Size: 120582 Color: 4
Size: 120545 Color: 1
Size: 120429 Color: 0

Bin 4367: 34686 of cap free
Amount of items: 2
Items: 
Size: 543584 Color: 2
Size: 421731 Color: 0

Bin 4368: 34690 of cap free
Amount of items: 6
Items: 
Size: 161150 Color: 0
Size: 161118 Color: 1
Size: 161012 Color: 1
Size: 160831 Color: 2
Size: 160681 Color: 0
Size: 160519 Color: 2

Bin 4369: 34828 of cap free
Amount of items: 2
Items: 
Size: 767849 Color: 1
Size: 197324 Color: 0

Bin 4370: 34899 of cap free
Amount of items: 2
Items: 
Size: 666604 Color: 3
Size: 298498 Color: 2

Bin 4371: 34970 of cap free
Amount of items: 2
Items: 
Size: 543413 Color: 3
Size: 421618 Color: 0

Bin 4372: 35051 of cap free
Amount of items: 2
Items: 
Size: 767816 Color: 0
Size: 197134 Color: 1

Bin 4373: 35079 of cap free
Amount of items: 2
Items: 
Size: 666555 Color: 1
Size: 298367 Color: 3

Bin 4374: 35102 of cap free
Amount of items: 2
Items: 
Size: 543357 Color: 4
Size: 421542 Color: 3

Bin 4375: 35140 of cap free
Amount of items: 7
Items: 
Size: 138039 Color: 3
Size: 137935 Color: 4
Size: 137915 Color: 2
Size: 137880 Color: 2
Size: 137759 Color: 1
Size: 137682 Color: 3
Size: 137651 Color: 2

Bin 4376: 35280 of cap free
Amount of items: 2
Items: 
Size: 767625 Color: 0
Size: 197096 Color: 2

Bin 4377: 35339 of cap free
Amount of items: 2
Items: 
Size: 543257 Color: 0
Size: 421405 Color: 2

Bin 4378: 35414 of cap free
Amount of items: 2
Items: 
Size: 543179 Color: 4
Size: 421408 Color: 0

Bin 4379: 35901 of cap free
Amount of items: 2
Items: 
Size: 767023 Color: 1
Size: 197077 Color: 3

Bin 4380: 35936 of cap free
Amount of items: 2
Items: 
Size: 543140 Color: 1
Size: 420925 Color: 2

Bin 4381: 36065 of cap free
Amount of items: 2
Items: 
Size: 767019 Color: 3
Size: 196917 Color: 4

Bin 4382: 36142 of cap free
Amount of items: 2
Items: 
Size: 766979 Color: 3
Size: 196880 Color: 1

Bin 4383: 36164 of cap free
Amount of items: 2
Items: 
Size: 542846 Color: 0
Size: 420991 Color: 1

Bin 4384: 36177 of cap free
Amount of items: 2
Items: 
Size: 766962 Color: 3
Size: 196862 Color: 1

Bin 4385: 36240 of cap free
Amount of items: 2
Items: 
Size: 766866 Color: 0
Size: 196895 Color: 3

Bin 4386: 36650 of cap free
Amount of items: 2
Items: 
Size: 542517 Color: 3
Size: 420834 Color: 0

Bin 4387: 36674 of cap free
Amount of items: 2
Items: 
Size: 766576 Color: 0
Size: 196751 Color: 1

Bin 4388: 36711 of cap free
Amount of items: 2
Items: 
Size: 542599 Color: 0
Size: 420691 Color: 1

Bin 4389: 36976 of cap free
Amount of items: 2
Items: 
Size: 723614 Color: 1
Size: 239411 Color: 0

Bin 4390: 37302 of cap free
Amount of items: 2
Items: 
Size: 723285 Color: 4
Size: 239414 Color: 1

Bin 4391: 37361 of cap free
Amount of items: 2
Items: 
Size: 542441 Color: 3
Size: 420199 Color: 4

Bin 4392: 37406 of cap free
Amount of items: 2
Items: 
Size: 723255 Color: 0
Size: 239340 Color: 4

Bin 4393: 37411 of cap free
Amount of items: 2
Items: 
Size: 723256 Color: 4
Size: 239334 Color: 0

Bin 4394: 37562 of cap free
Amount of items: 6
Items: 
Size: 160487 Color: 4
Size: 160476 Color: 1
Size: 160471 Color: 4
Size: 160406 Color: 4
Size: 160347 Color: 3
Size: 160252 Color: 3

Bin 4395: 38427 of cap free
Amount of items: 2
Items: 
Size: 723160 Color: 4
Size: 238414 Color: 3

Bin 4396: 38666 of cap free
Amount of items: 2
Items: 
Size: 663028 Color: 1
Size: 298307 Color: 3

Bin 4397: 38910 of cap free
Amount of items: 8
Items: 
Size: 120375 Color: 0
Size: 120259 Color: 4
Size: 120142 Color: 0
Size: 120120 Color: 2
Size: 120067 Color: 4
Size: 120053 Color: 2
Size: 120044 Color: 4
Size: 120031 Color: 3

Bin 4398: 38999 of cap free
Amount of items: 2
Items: 
Size: 764347 Color: 4
Size: 196655 Color: 2

Bin 4399: 39021 of cap free
Amount of items: 7
Items: 
Size: 137510 Color: 3
Size: 137491 Color: 1
Size: 137473 Color: 3
Size: 137248 Color: 1
Size: 137168 Color: 2
Size: 137090 Color: 3
Size: 137000 Color: 2

Bin 4400: 39034 of cap free
Amount of items: 2
Items: 
Size: 764342 Color: 4
Size: 196625 Color: 2

Bin 4401: 39110 of cap free
Amount of items: 2
Items: 
Size: 764267 Color: 2
Size: 196624 Color: 1

Bin 4402: 39123 of cap free
Amount of items: 2
Items: 
Size: 764256 Color: 2
Size: 196622 Color: 1

Bin 4403: 39338 of cap free
Amount of items: 2
Items: 
Size: 764155 Color: 3
Size: 196508 Color: 0

Bin 4404: 39575 of cap free
Amount of items: 2
Items: 
Size: 763963 Color: 0
Size: 196463 Color: 3

Bin 4405: 39616 of cap free
Amount of items: 2
Items: 
Size: 763923 Color: 4
Size: 196462 Color: 0

Bin 4406: 39778 of cap free
Amount of items: 9
Items: 
Size: 107240 Color: 0
Size: 107159 Color: 3
Size: 106922 Color: 0
Size: 106828 Color: 0
Size: 106414 Color: 2
Size: 106549 Color: 0
Size: 106296 Color: 1
Size: 106533 Color: 0
Size: 106282 Color: 4

Bin 4407: 39795 of cap free
Amount of items: 2
Items: 
Size: 763850 Color: 3
Size: 196356 Color: 1

Bin 4408: 39974 of cap free
Amount of items: 2
Items: 
Size: 763709 Color: 2
Size: 196318 Color: 3

Bin 4409: 40059 of cap free
Amount of items: 2
Items: 
Size: 763801 Color: 3
Size: 196141 Color: 4

Bin 4410: 40305 of cap free
Amount of items: 2
Items: 
Size: 763687 Color: 1
Size: 196009 Color: 0

Bin 4411: 40403 of cap free
Amount of items: 6
Items: 
Size: 160147 Color: 1
Size: 160112 Color: 4
Size: 160107 Color: 4
Size: 159931 Color: 2
Size: 159661 Color: 3
Size: 159640 Color: 1

Bin 4412: 40914 of cap free
Amount of items: 2
Items: 
Size: 662941 Color: 1
Size: 296146 Color: 2

Bin 4413: 41003 of cap free
Amount of items: 8
Items: 
Size: 119997 Color: 2
Size: 119963 Color: 3
Size: 119939 Color: 3
Size: 119871 Color: 4
Size: 119856 Color: 1
Size: 119846 Color: 1
Size: 119768 Color: 2
Size: 119758 Color: 0

Bin 4414: 41349 of cap free
Amount of items: 2
Items: 
Size: 542316 Color: 1
Size: 416336 Color: 2

Bin 4415: 42115 of cap free
Amount of items: 2
Items: 
Size: 719229 Color: 2
Size: 238657 Color: 4

Bin 4416: 42333 of cap free
Amount of items: 2
Items: 
Size: 719209 Color: 2
Size: 238459 Color: 4

Bin 4417: 42437 of cap free
Amount of items: 6
Items: 
Size: 159640 Color: 1
Size: 159629 Color: 3
Size: 159596 Color: 4
Size: 159594 Color: 0
Size: 159592 Color: 0
Size: 159513 Color: 1

Bin 4418: 42882 of cap free
Amount of items: 7
Items: 
Size: 136990 Color: 3
Size: 136981 Color: 2
Size: 136797 Color: 2
Size: 136679 Color: 4
Size: 136675 Color: 3
Size: 136558 Color: 1
Size: 136439 Color: 2

Bin 4419: 43040 of cap free
Amount of items: 2
Items: 
Size: 719190 Color: 4
Size: 237771 Color: 1

Bin 4420: 44484 of cap free
Amount of items: 8
Items: 
Size: 119745 Color: 0
Size: 119729 Color: 1
Size: 119461 Color: 3
Size: 119425 Color: 2
Size: 119359 Color: 4
Size: 119342 Color: 1
Size: 119276 Color: 0
Size: 119180 Color: 2

Bin 4421: 45194 of cap free
Amount of items: 9
Items: 
Size: 106513 Color: 0
Size: 106221 Color: 2
Size: 106200 Color: 3
Size: 106074 Color: 4
Size: 106014 Color: 4
Size: 106001 Color: 1
Size: 105988 Color: 1
Size: 105937 Color: 2
Size: 105859 Color: 3

Bin 4422: 45287 of cap free
Amount of items: 2
Items: 
Size: 658646 Color: 1
Size: 296068 Color: 4

Bin 4423: 45536 of cap free
Amount of items: 2
Items: 
Size: 658398 Color: 1
Size: 296067 Color: 0

Bin 4424: 45626 of cap free
Amount of items: 6
Items: 
Size: 159251 Color: 2
Size: 159177 Color: 3
Size: 159082 Color: 3
Size: 159032 Color: 3
Size: 159025 Color: 0
Size: 158808 Color: 2

Bin 4425: 46077 of cap free
Amount of items: 2
Items: 
Size: 716199 Color: 2
Size: 237725 Color: 1

Bin 4426: 46208 of cap free
Amount of items: 2
Items: 
Size: 716106 Color: 0
Size: 237687 Color: 1

Bin 4427: 46306 of cap free
Amount of items: 2
Items: 
Size: 716229 Color: 1
Size: 237466 Color: 4

Bin 4428: 46328 of cap free
Amount of items: 7
Items: 
Size: 136429 Color: 3
Size: 136390 Color: 4
Size: 136319 Color: 2
Size: 136244 Color: 2
Size: 136116 Color: 4
Size: 136108 Color: 1
Size: 136067 Color: 4

Bin 4429: 47536 of cap free
Amount of items: 2
Items: 
Size: 656369 Color: 4
Size: 296096 Color: 1

Bin 4430: 49944 of cap free
Amount of items: 8
Items: 
Size: 119122 Color: 3
Size: 119067 Color: 1
Size: 118955 Color: 0
Size: 118746 Color: 1
Size: 118662 Color: 2
Size: 118568 Color: 4
Size: 118535 Color: 4
Size: 118402 Color: 0

Bin 4431: 50157 of cap free
Amount of items: 6
Items: 
Size: 158990 Color: 3
Size: 158626 Color: 3
Size: 158416 Color: 2
Size: 158092 Color: 2
Size: 157886 Color: 4
Size: 157834 Color: 2

Bin 4432: 50308 of cap free
Amount of items: 2
Items: 
Size: 713178 Color: 4
Size: 236515 Color: 1

Bin 4433: 52003 of cap free
Amount of items: 7
Items: 
Size: 135895 Color: 3
Size: 135843 Color: 2
Size: 135665 Color: 1
Size: 135260 Color: 0
Size: 135123 Color: 4
Size: 135115 Color: 4
Size: 135097 Color: 0

Bin 4434: 53236 of cap free
Amount of items: 9
Items: 
Size: 105474 Color: 4
Size: 105338 Color: 1
Size: 105313 Color: 2
Size: 105305 Color: 1
Size: 105192 Color: 0
Size: 105160 Color: 0
Size: 105133 Color: 2
Size: 104934 Color: 1
Size: 104916 Color: 4

Bin 4435: 53992 of cap free
Amount of items: 6
Items: 
Size: 157822 Color: 4
Size: 157725 Color: 1
Size: 157709 Color: 3
Size: 157665 Color: 1
Size: 157563 Color: 3
Size: 157525 Color: 2

Bin 4436: 55951 of cap free
Amount of items: 7
Items: 
Size: 135093 Color: 3
Size: 134980 Color: 1
Size: 134979 Color: 1
Size: 134898 Color: 2
Size: 134724 Color: 3
Size: 134710 Color: 1
Size: 134666 Color: 0

Bin 4437: 56112 of cap free
Amount of items: 8
Items: 
Size: 118388 Color: 1
Size: 118362 Color: 0
Size: 118152 Color: 1
Size: 117989 Color: 3
Size: 117821 Color: 3
Size: 117754 Color: 1
Size: 117731 Color: 2
Size: 117692 Color: 0

Bin 4438: 58157 of cap free
Amount of items: 6
Items: 
Size: 157230 Color: 0
Size: 157187 Color: 3
Size: 156888 Color: 1
Size: 156857 Color: 1
Size: 156853 Color: 0
Size: 156829 Color: 3

Bin 4439: 58170 of cap free
Amount of items: 5
Items: 
Size: 188529 Color: 3
Size: 188470 Color: 0
Size: 188422 Color: 3
Size: 188395 Color: 1
Size: 188015 Color: 4

Bin 4440: 58249 of cap free
Amount of items: 7
Items: 
Size: 134650 Color: 0
Size: 134595 Color: 1
Size: 134584 Color: 0
Size: 134511 Color: 4
Size: 134491 Color: 3
Size: 134462 Color: 0
Size: 134459 Color: 2

Bin 4441: 59211 of cap free
Amount of items: 9
Items: 
Size: 104756 Color: 3
Size: 104742 Color: 0
Size: 104707 Color: 0
Size: 104612 Color: 4
Size: 104482 Color: 0
Size: 104427 Color: 0
Size: 104366 Color: 3
Size: 104359 Color: 0
Size: 104339 Color: 3

Bin 4442: 59764 of cap free
Amount of items: 8
Items: 
Size: 117691 Color: 1
Size: 117665 Color: 3
Size: 117647 Color: 1
Size: 117579 Color: 3
Size: 117518 Color: 4
Size: 117465 Color: 3
Size: 117460 Color: 2
Size: 117212 Color: 1

Bin 4443: 60106 of cap free
Amount of items: 6
Items: 
Size: 156817 Color: 4
Size: 156691 Color: 4
Size: 156667 Color: 0
Size: 156648 Color: 3
Size: 156567 Color: 3
Size: 156505 Color: 0

Bin 4444: 61334 of cap free
Amount of items: 5
Items: 
Size: 187842 Color: 1
Size: 187820 Color: 2
Size: 187810 Color: 1
Size: 187657 Color: 2
Size: 187538 Color: 1

Bin 4445: 61792 of cap free
Amount of items: 7
Items: 
Size: 134426 Color: 1
Size: 134421 Color: 0
Size: 134257 Color: 2
Size: 133872 Color: 3
Size: 133763 Color: 0
Size: 133752 Color: 2
Size: 133718 Color: 3

Bin 4446: 63016 of cap free
Amount of items: 5
Items: 
Size: 187502 Color: 4
Size: 187400 Color: 1
Size: 187368 Color: 4
Size: 187365 Color: 4
Size: 187350 Color: 0

Bin 4447: 63634 of cap free
Amount of items: 5
Items: 
Size: 187326 Color: 4
Size: 187292 Color: 1
Size: 187284 Color: 4
Size: 187253 Color: 4
Size: 187212 Color: 2

Bin 4448: 63789 of cap free
Amount of items: 6
Items: 
Size: 156408 Color: 0
Size: 156356 Color: 4
Size: 156211 Color: 1
Size: 155863 Color: 0
Size: 155769 Color: 4
Size: 155605 Color: 2

Bin 4449: 64631 of cap free
Amount of items: 7
Items: 
Size: 133698 Color: 2
Size: 133679 Color: 2
Size: 133655 Color: 0
Size: 133679 Color: 2
Size: 133594 Color: 3
Size: 133561 Color: 1
Size: 133504 Color: 0

Bin 4450: 64761 of cap free
Amount of items: 5
Items: 
Size: 187125 Color: 2
Size: 187099 Color: 1
Size: 187053 Color: 1
Size: 187030 Color: 4
Size: 186933 Color: 0

Bin 4451: 65174 of cap free
Amount of items: 8
Items: 
Size: 117149 Color: 1
Size: 117138 Color: 0
Size: 116872 Color: 3
Size: 116744 Color: 2
Size: 116740 Color: 3
Size: 116738 Color: 1
Size: 116727 Color: 1
Size: 116719 Color: 3

Bin 4452: 66032 of cap free
Amount of items: 9
Items: 
Size: 104273 Color: 4
Size: 104198 Color: 2
Size: 104061 Color: 0
Size: 103815 Color: 0
Size: 103717 Color: 3
Size: 103560 Color: 0
Size: 103478 Color: 0
Size: 103447 Color: 3
Size: 103420 Color: 2

Bin 4453: 66983 of cap free
Amount of items: 2
Items: 
Size: 516740 Color: 0
Size: 416278 Color: 2

Bin 4454: 67067 of cap free
Amount of items: 5
Items: 
Size: 186804 Color: 0
Size: 186727 Color: 1
Size: 186516 Color: 2
Size: 186515 Color: 0
Size: 186372 Color: 4

Bin 4455: 67132 of cap free
Amount of items: 2
Items: 
Size: 516703 Color: 3
Size: 416166 Color: 1

Bin 4456: 67453 of cap free
Amount of items: 6
Items: 
Size: 155555 Color: 2
Size: 155490 Color: 0
Size: 155461 Color: 4
Size: 155376 Color: 4
Size: 155340 Color: 3
Size: 155326 Color: 4

Bin 4457: 67577 of cap free
Amount of items: 7
Items: 
Size: 133423 Color: 2
Size: 133403 Color: 2
Size: 133269 Color: 4
Size: 133381 Color: 2
Size: 133254 Color: 4
Size: 132919 Color: 2
Size: 132775 Color: 1

Bin 4458: 67975 of cap free
Amount of items: 8
Items: 
Size: 116699 Color: 4
Size: 116642 Color: 0
Size: 116611 Color: 0
Size: 116600 Color: 1
Size: 116561 Color: 3
Size: 116350 Color: 2
Size: 116330 Color: 1
Size: 116233 Color: 1

Bin 4459: 69066 of cap free
Amount of items: 6
Items: 
Size: 155258 Color: 1
Size: 155197 Color: 3
Size: 155164 Color: 2
Size: 155119 Color: 0
Size: 155103 Color: 2
Size: 155094 Color: 0

Bin 4460: 69484 of cap free
Amount of items: 5
Items: 
Size: 186294 Color: 1
Size: 186224 Color: 3
Size: 186182 Color: 2
Size: 185918 Color: 0
Size: 185899 Color: 1

Bin 4461: 70438 of cap free
Amount of items: 2
Items: 
Size: 633578 Color: 0
Size: 295985 Color: 1

Bin 4462: 71409 of cap free
Amount of items: 5
Items: 
Size: 185852 Color: 0
Size: 185817 Color: 1
Size: 185661 Color: 4
Size: 185652 Color: 4
Size: 185610 Color: 1

Bin 4463: 71661 of cap free
Amount of items: 6
Items: 
Size: 154934 Color: 4
Size: 154865 Color: 0
Size: 154729 Color: 2
Size: 154635 Color: 2
Size: 154621 Color: 1
Size: 154556 Color: 1

Bin 4464: 71723 of cap free
Amount of items: 7
Items: 
Size: 132809 Color: 2
Size: 132707 Color: 2
Size: 132682 Color: 1
Size: 132587 Color: 4
Size: 132503 Color: 2
Size: 132500 Color: 1
Size: 132490 Color: 4

Bin 4465: 71819 of cap free
Amount of items: 2
Items: 
Size: 632256 Color: 0
Size: 295926 Color: 1

Bin 4466: 73023 of cap free
Amount of items: 9
Items: 
Size: 103297 Color: 4
Size: 103132 Color: 2
Size: 103080 Color: 0
Size: 103020 Color: 3
Size: 103006 Color: 3
Size: 102971 Color: 2
Size: 102934 Color: 4
Size: 102778 Color: 0
Size: 102760 Color: 3

Bin 4467: 73672 of cap free
Amount of items: 5
Items: 
Size: 185579 Color: 3
Size: 185442 Color: 3
Size: 185134 Color: 1
Size: 185100 Color: 1
Size: 185074 Color: 4

Bin 4468: 73796 of cap free
Amount of items: 6
Items: 
Size: 154533 Color: 2
Size: 154388 Color: 0
Size: 154382 Color: 4
Size: 154316 Color: 2
Size: 154309 Color: 2
Size: 154277 Color: 1

Bin 4469: 74161 of cap free
Amount of items: 7
Items: 
Size: 132440 Color: 3
Size: 132405 Color: 4
Size: 132332 Color: 0
Size: 132286 Color: 0
Size: 132157 Color: 4
Size: 132151 Color: 1
Size: 132069 Color: 0

Bin 4470: 74776 of cap free
Amount of items: 8
Items: 
Size: 116117 Color: 1
Size: 116011 Color: 3
Size: 115653 Color: 3
Size: 115575 Color: 4
Size: 115570 Color: 0
Size: 115472 Color: 0
Size: 115458 Color: 3
Size: 115369 Color: 3

Bin 4471: 75617 of cap free
Amount of items: 6
Items: 
Size: 154199 Color: 3
Size: 154197 Color: 0
Size: 154132 Color: 1
Size: 154050 Color: 3
Size: 153965 Color: 4
Size: 153841 Color: 0

Bin 4472: 75987 of cap free
Amount of items: 2
Items: 
Size: 632216 Color: 0
Size: 291798 Color: 4

Bin 4473: 76309 of cap free
Amount of items: 5
Items: 
Size: 185066 Color: 4
Size: 185034 Color: 1
Size: 184553 Color: 2
Size: 184539 Color: 1
Size: 184500 Color: 0

Bin 4474: 76350 of cap free
Amount of items: 7
Items: 
Size: 131999 Color: 1
Size: 131992 Color: 0
Size: 131980 Color: 1
Size: 131975 Color: 4
Size: 131911 Color: 4
Size: 131902 Color: 0
Size: 131892 Color: 3

Bin 4475: 77136 of cap free
Amount of items: 2
Items: 
Size: 631749 Color: 3
Size: 291116 Color: 2

Bin 4476: 78313 of cap free
Amount of items: 6
Items: 
Size: 153735 Color: 4
Size: 153714 Color: 1
Size: 153661 Color: 2
Size: 153585 Color: 0
Size: 153505 Color: 3
Size: 153488 Color: 1

Bin 4477: 79116 of cap free
Amount of items: 7
Items: 
Size: 131892 Color: 3
Size: 131863 Color: 2
Size: 131829 Color: 0
Size: 131528 Color: 1
Size: 131305 Color: 4
Size: 131254 Color: 0
Size: 131214 Color: 3

Bin 4478: 79185 of cap free
Amount of items: 5
Items: 
Size: 184231 Color: 4
Size: 184181 Color: 1
Size: 184168 Color: 4
Size: 184162 Color: 1
Size: 184074 Color: 2

Bin 4479: 79235 of cap free
Amount of items: 8
Items: 
Size: 115324 Color: 3
Size: 115284 Color: 3
Size: 115244 Color: 4
Size: 115224 Color: 1
Size: 114956 Color: 4
Size: 114936 Color: 1
Size: 114899 Color: 4
Size: 114899 Color: 2

Bin 4480: 79553 of cap free
Amount of items: 9
Items: 
Size: 102709 Color: 1
Size: 102669 Color: 2
Size: 102661 Color: 2
Size: 102594 Color: 1
Size: 102174 Color: 4
Size: 102152 Color: 3
Size: 102147 Color: 4
Size: 101883 Color: 2
Size: 101459 Color: 4

Bin 4481: 80692 of cap free
Amount of items: 6
Items: 
Size: 153470 Color: 4
Size: 153388 Color: 3
Size: 153272 Color: 2
Size: 153135 Color: 2
Size: 153034 Color: 0
Size: 153010 Color: 2

Bin 4482: 81202 of cap free
Amount of items: 5
Items: 
Size: 183966 Color: 4
Size: 183779 Color: 1
Size: 183756 Color: 0
Size: 183661 Color: 0
Size: 183637 Color: 4

Bin 4483: 81588 of cap free
Amount of items: 2
Items: 
Size: 631316 Color: 4
Size: 287097 Color: 3

Bin 4484: 81801 of cap free
Amount of items: 8
Items: 
Size: 114878 Color: 0
Size: 114852 Color: 4
Size: 114829 Color: 1
Size: 114808 Color: 4
Size: 114737 Color: 4
Size: 114722 Color: 2
Size: 114693 Color: 1
Size: 114681 Color: 4

Bin 4485: 82319 of cap free
Amount of items: 2
Items: 
Size: 631667 Color: 3
Size: 286015 Color: 4

Bin 4486: 82599 of cap free
Amount of items: 7
Items: 
Size: 131147 Color: 2
Size: 131142 Color: 2
Size: 131129 Color: 3
Size: 131122 Color: 0
Size: 130995 Color: 0
Size: 130963 Color: 2
Size: 130904 Color: 0

Bin 4487: 82688 of cap free
Amount of items: 2
Items: 
Size: 631307 Color: 2
Size: 286006 Color: 4

Bin 4488: 83331 of cap free
Amount of items: 6
Items: 
Size: 152969 Color: 0
Size: 152924 Color: 3
Size: 152849 Color: 4
Size: 152787 Color: 1
Size: 152582 Color: 4
Size: 152559 Color: 0

Bin 4489: 83951 of cap free
Amount of items: 5
Items: 
Size: 183428 Color: 4
Size: 183209 Color: 0
Size: 183148 Color: 2
Size: 183135 Color: 1
Size: 183130 Color: 0

Bin 4490: 84883 of cap free
Amount of items: 7
Items: 
Size: 130902 Color: 2
Size: 130866 Color: 4
Size: 130814 Color: 1
Size: 130698 Color: 4
Size: 130674 Color: 2
Size: 130624 Color: 1
Size: 130540 Color: 1

Bin 4491: 85213 of cap free
Amount of items: 5
Items: 
Size: 183103 Color: 1
Size: 183096 Color: 4
Size: 182960 Color: 3
Size: 182851 Color: 0
Size: 182778 Color: 2

Bin 4492: 85697 of cap free
Amount of items: 8
Items: 
Size: 114606 Color: 1
Size: 114433 Color: 1
Size: 114296 Color: 3
Size: 114336 Color: 1
Size: 114215 Color: 3
Size: 114193 Color: 0
Size: 114134 Color: 0
Size: 114091 Color: 2

Bin 4493: 86120 of cap free
Amount of items: 6
Items: 
Size: 152518 Color: 2
Size: 152368 Color: 2
Size: 152368 Color: 0
Size: 152243 Color: 4
Size: 152213 Color: 4
Size: 152171 Color: 3

Bin 4494: 86858 of cap free
Amount of items: 5
Items: 
Size: 182755 Color: 2
Size: 182727 Color: 1
Size: 182711 Color: 4
Size: 182616 Color: 2
Size: 182334 Color: 1

Bin 4495: 87448 of cap free
Amount of items: 7
Items: 
Size: 130515 Color: 2
Size: 130471 Color: 4
Size: 130429 Color: 1
Size: 130386 Color: 2
Size: 130337 Color: 2
Size: 130244 Color: 3
Size: 130171 Color: 3

Bin 4496: 88565 of cap free
Amount of items: 9
Items: 
Size: 101442 Color: 4
Size: 101437 Color: 4
Size: 101395 Color: 3
Size: 101338 Color: 4
Size: 101329 Color: 1
Size: 101150 Color: 3
Size: 101139 Color: 2
Size: 101124 Color: 0
Size: 101082 Color: 2

Bin 4497: 89027 of cap free
Amount of items: 6
Items: 
Size: 152058 Color: 3
Size: 152028 Color: 3
Size: 151816 Color: 4
Size: 151737 Color: 4
Size: 151701 Color: 4
Size: 151634 Color: 1

Bin 4498: 89220 of cap free
Amount of items: 5
Items: 
Size: 182294 Color: 4
Size: 182182 Color: 3
Size: 182153 Color: 4
Size: 182113 Color: 0
Size: 182039 Color: 1

Bin 4499: 90678 of cap free
Amount of items: 7
Items: 
Size: 130087 Color: 3
Size: 129982 Color: 4
Size: 129978 Color: 3
Size: 129959 Color: 3
Size: 129862 Color: 4
Size: 129745 Color: 4
Size: 129710 Color: 1

Bin 4500: 90834 of cap free
Amount of items: 5
Items: 
Size: 181951 Color: 4
Size: 181951 Color: 0
Size: 181850 Color: 2
Size: 181718 Color: 1
Size: 181697 Color: 4

Bin 4501: 91256 of cap free
Amount of items: 8
Items: 
Size: 114044 Color: 2
Size: 114041 Color: 4
Size: 113864 Color: 2
Size: 113454 Color: 3
Size: 113380 Color: 4
Size: 113348 Color: 0
Size: 113341 Color: 2
Size: 113273 Color: 0

Bin 4502: 92789 of cap free
Amount of items: 5
Items: 
Size: 181683 Color: 2
Size: 181593 Color: 1
Size: 181581 Color: 4
Size: 181225 Color: 3
Size: 181130 Color: 2

Bin 4503: 93036 of cap free
Amount of items: 7
Items: 
Size: 129687 Color: 3
Size: 129664 Color: 1
Size: 129653 Color: 3
Size: 129616 Color: 4
Size: 129523 Color: 4
Size: 129439 Color: 4
Size: 129383 Color: 2

Bin 4504: 93375 of cap free
Amount of items: 9
Items: 
Size: 100986 Color: 1
Size: 100972 Color: 4
Size: 100935 Color: 0
Size: 100769 Color: 4
Size: 100686 Color: 1
Size: 100644 Color: 3
Size: 100563 Color: 4
Size: 100556 Color: 4
Size: 100515 Color: 4

Bin 4505: 94192 of cap free
Amount of items: 6
Items: 
Size: 151245 Color: 4
Size: 151118 Color: 4
Size: 151018 Color: 2
Size: 151098 Color: 4
Size: 150882 Color: 2
Size: 150448 Color: 1

Bin 4506: 94778 of cap free
Amount of items: 5
Items: 
Size: 181116 Color: 1
Size: 181090 Color: 4
Size: 181071 Color: 2
Size: 181010 Color: 3
Size: 180936 Color: 1

Bin 4507: 95541 of cap free
Amount of items: 6
Items: 
Size: 151024 Color: 4
Size: 150941 Color: 4
Size: 150674 Color: 3
Size: 150728 Color: 4
Size: 150654 Color: 0
Size: 150439 Color: 3

Bin 4508: 96043 of cap free
Amount of items: 8
Items: 
Size: 113250 Color: 4
Size: 113145 Color: 1
Size: 113108 Color: 0
Size: 113084 Color: 3
Size: 112973 Color: 4
Size: 112879 Color: 3
Size: 112858 Color: 3
Size: 112661 Color: 3

Bin 4509: 96185 of cap free
Amount of items: 5
Items: 
Size: 180888 Color: 0
Size: 180756 Color: 3
Size: 180746 Color: 1
Size: 180717 Color: 1
Size: 180709 Color: 4

Bin 4510: 98017 of cap free
Amount of items: 7
Items: 
Size: 129358 Color: 3
Size: 129152 Color: 0
Size: 128863 Color: 3
Size: 128812 Color: 0
Size: 128632 Color: 3
Size: 128620 Color: 3
Size: 128547 Color: 2

Bin 4511: 98055 of cap free
Amount of items: 5
Items: 
Size: 180578 Color: 2
Size: 180413 Color: 0
Size: 180387 Color: 0
Size: 180330 Color: 2
Size: 180238 Color: 4

Bin 4512: 98509 of cap free
Amount of items: 6
Items: 
Size: 150465 Color: 4
Size: 150384 Color: 4
Size: 150257 Color: 0
Size: 150208 Color: 4
Size: 150177 Color: 0
Size: 150001 Color: 2

Bin 4513: 99809 of cap free
Amount of items: 5
Items: 
Size: 180181 Color: 3
Size: 180129 Color: 4
Size: 180063 Color: 3
Size: 179913 Color: 4
Size: 179906 Color: 2

Bin 4514: 298142 of cap free
Amount of items: 7
Items: 
Size: 100440 Color: 2
Size: 100314 Color: 1
Size: 100278 Color: 4
Size: 100271 Color: 2
Size: 100193 Color: 0
Size: 100183 Color: 1
Size: 100180 Color: 2

Total size: 4499400029
Total free space: 14604485

