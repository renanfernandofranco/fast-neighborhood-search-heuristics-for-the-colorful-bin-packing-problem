Capicity Bin: 1001
Lower Bound: 43

Bins used: 44
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 2
Size: 399 Color: 3
Size: 195 Color: 4

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 3
Size: 376 Color: 1

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 288 Color: 2

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 2
Size: 239 Color: 4

Bin 5: 1 of cap free
Amount of items: 4
Items: 
Size: 283 Color: 4
Size: 281 Color: 3
Size: 273 Color: 3
Size: 163 Color: 4

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 4
Size: 392 Color: 1

Bin 7: 1 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 4
Size: 336 Color: 0

Bin 8: 1 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 282 Color: 3

Bin 9: 2 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 2
Size: 398 Color: 3
Size: 200 Color: 2

Bin 10: 2 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 4
Size: 408 Color: 1
Size: 180 Color: 3

Bin 11: 2 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 0
Size: 395 Color: 4

Bin 12: 2 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 294 Color: 1

Bin 13: 3 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 419 Color: 0
Size: 158 Color: 0

Bin 14: 3 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 2
Size: 422 Color: 4
Size: 148 Color: 3

Bin 15: 3 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 1
Size: 483 Color: 4

Bin 16: 4 of cap free
Amount of items: 7
Items: 
Size: 241 Color: 3
Size: 153 Color: 4
Size: 129 Color: 2
Size: 118 Color: 4
Size: 142 Color: 3
Size: 111 Color: 4
Size: 103 Color: 4

Bin 17: 4 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 2
Size: 308 Color: 4

Bin 18: 5 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 1
Size: 218 Color: 0

Bin 19: 6 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 3
Size: 344 Color: 4

Bin 20: 7 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 1
Size: 364 Color: 0

Bin 21: 7 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 2
Size: 247 Color: 1

Bin 22: 8 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 2
Size: 451 Color: 0

Bin 23: 11 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 383 Color: 0
Size: 217 Color: 3

Bin 24: 11 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 0
Size: 435 Color: 4

Bin 25: 11 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 4
Size: 245 Color: 1

Bin 26: 12 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 4
Size: 316 Color: 0

Bin 27: 16 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 2
Size: 263 Color: 0

Bin 28: 19 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 2
Size: 469 Color: 3

Bin 29: 21 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 0
Size: 244 Color: 3

Bin 30: 21 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 3
Size: 236 Color: 2

Bin 31: 23 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 4
Size: 306 Color: 0

Bin 32: 23 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 4
Size: 208 Color: 1

Bin 33: 30 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 2
Size: 433 Color: 1

Bin 34: 34 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 1
Size: 195 Color: 4

Bin 35: 36 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 428 Color: 3

Bin 36: 36 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 429 Color: 2

Bin 37: 47 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 0
Size: 292 Color: 2

Bin 38: 52 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 4
Size: 289 Color: 0

Bin 39: 56 of cap free
Amount of items: 2
Items: 
Size: 478 Color: 4
Size: 467 Color: 3

Bin 40: 65 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 2
Size: 175 Color: 3

Bin 41: 74 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 3
Size: 187 Color: 4

Bin 42: 78 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 0
Size: 285 Color: 3

Bin 43: 86 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 3
Size: 175 Color: 1

Bin 44: 640 of cap free
Amount of items: 3
Items: 
Size: 131 Color: 3
Size: 110 Color: 4
Size: 120 Color: 3

Total size: 42580
Total free space: 1464

