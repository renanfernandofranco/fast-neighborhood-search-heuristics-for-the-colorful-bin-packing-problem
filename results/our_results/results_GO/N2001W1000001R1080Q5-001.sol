Capicity Bin: 1000001
Lower Bound: 906

Bins used: 911
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 515163 Color: 1
Size: 484838 Color: 2

Bin 2: 1 of cap free
Amount of items: 2
Items: 
Size: 696545 Color: 1
Size: 303455 Color: 3

Bin 3: 1 of cap free
Amount of items: 2
Items: 
Size: 759872 Color: 4
Size: 240128 Color: 0

Bin 4: 1 of cap free
Amount of items: 2
Items: 
Size: 776338 Color: 2
Size: 223662 Color: 3

Bin 5: 5 of cap free
Amount of items: 2
Items: 
Size: 745354 Color: 3
Size: 254642 Color: 4

Bin 6: 5 of cap free
Amount of items: 2
Items: 
Size: 751826 Color: 1
Size: 248170 Color: 2

Bin 7: 7 of cap free
Amount of items: 2
Items: 
Size: 747256 Color: 2
Size: 252738 Color: 4

Bin 8: 8 of cap free
Amount of items: 2
Items: 
Size: 665426 Color: 3
Size: 334567 Color: 1

Bin 9: 8 of cap free
Amount of items: 2
Items: 
Size: 763956 Color: 4
Size: 236037 Color: 2

Bin 10: 10 of cap free
Amount of items: 2
Items: 
Size: 526352 Color: 1
Size: 473639 Color: 2

Bin 11: 10 of cap free
Amount of items: 2
Items: 
Size: 741210 Color: 1
Size: 258781 Color: 0

Bin 12: 10 of cap free
Amount of items: 2
Items: 
Size: 767549 Color: 2
Size: 232442 Color: 3

Bin 13: 11 of cap free
Amount of items: 2
Items: 
Size: 658169 Color: 2
Size: 341821 Color: 4

Bin 14: 11 of cap free
Amount of items: 2
Items: 
Size: 751648 Color: 3
Size: 248342 Color: 2

Bin 15: 12 of cap free
Amount of items: 2
Items: 
Size: 578416 Color: 3
Size: 421573 Color: 0

Bin 16: 12 of cap free
Amount of items: 2
Items: 
Size: 702042 Color: 0
Size: 297947 Color: 2

Bin 17: 13 of cap free
Amount of items: 2
Items: 
Size: 737271 Color: 2
Size: 262717 Color: 3

Bin 18: 14 of cap free
Amount of items: 2
Items: 
Size: 501471 Color: 3
Size: 498516 Color: 0

Bin 19: 15 of cap free
Amount of items: 2
Items: 
Size: 622574 Color: 1
Size: 377412 Color: 3

Bin 20: 16 of cap free
Amount of items: 2
Items: 
Size: 726571 Color: 0
Size: 273414 Color: 2

Bin 21: 18 of cap free
Amount of items: 2
Items: 
Size: 563946 Color: 3
Size: 436037 Color: 4

Bin 22: 18 of cap free
Amount of items: 3
Items: 
Size: 662034 Color: 3
Size: 190650 Color: 4
Size: 147299 Color: 1

Bin 23: 18 of cap free
Amount of items: 2
Items: 
Size: 677563 Color: 3
Size: 322420 Color: 4

Bin 24: 19 of cap free
Amount of items: 3
Items: 
Size: 441638 Color: 3
Size: 434844 Color: 0
Size: 123500 Color: 2

Bin 25: 19 of cap free
Amount of items: 2
Items: 
Size: 765325 Color: 4
Size: 234657 Color: 1

Bin 26: 20 of cap free
Amount of items: 2
Items: 
Size: 574220 Color: 1
Size: 425761 Color: 0

Bin 27: 20 of cap free
Amount of items: 2
Items: 
Size: 728671 Color: 4
Size: 271310 Color: 2

Bin 28: 20 of cap free
Amount of items: 2
Items: 
Size: 741353 Color: 2
Size: 258628 Color: 1

Bin 29: 21 of cap free
Amount of items: 2
Items: 
Size: 750997 Color: 0
Size: 248983 Color: 4

Bin 30: 22 of cap free
Amount of items: 2
Items: 
Size: 685172 Color: 1
Size: 314807 Color: 3

Bin 31: 23 of cap free
Amount of items: 2
Items: 
Size: 556472 Color: 1
Size: 443506 Color: 4

Bin 32: 23 of cap free
Amount of items: 2
Items: 
Size: 755308 Color: 0
Size: 244670 Color: 1

Bin 33: 24 of cap free
Amount of items: 2
Items: 
Size: 581833 Color: 3
Size: 418144 Color: 4

Bin 34: 24 of cap free
Amount of items: 2
Items: 
Size: 692425 Color: 0
Size: 307552 Color: 4

Bin 35: 24 of cap free
Amount of items: 2
Items: 
Size: 729192 Color: 1
Size: 270785 Color: 4

Bin 36: 24 of cap free
Amount of items: 2
Items: 
Size: 762403 Color: 2
Size: 237574 Color: 3

Bin 37: 24 of cap free
Amount of items: 2
Items: 
Size: 777331 Color: 1
Size: 222646 Color: 3

Bin 38: 25 of cap free
Amount of items: 2
Items: 
Size: 565682 Color: 1
Size: 434294 Color: 4

Bin 39: 26 of cap free
Amount of items: 3
Items: 
Size: 690629 Color: 1
Size: 197453 Color: 0
Size: 111893 Color: 1

Bin 40: 27 of cap free
Amount of items: 2
Items: 
Size: 560220 Color: 1
Size: 439754 Color: 2

Bin 41: 28 of cap free
Amount of items: 2
Items: 
Size: 782058 Color: 3
Size: 217915 Color: 1

Bin 42: 28 of cap free
Amount of items: 2
Items: 
Size: 787994 Color: 0
Size: 211979 Color: 4

Bin 43: 29 of cap free
Amount of items: 2
Items: 
Size: 654610 Color: 4
Size: 345362 Color: 2

Bin 44: 33 of cap free
Amount of items: 2
Items: 
Size: 762516 Color: 2
Size: 237452 Color: 1

Bin 45: 33 of cap free
Amount of items: 2
Items: 
Size: 773867 Color: 2
Size: 226101 Color: 3

Bin 46: 34 of cap free
Amount of items: 2
Items: 
Size: 761777 Color: 2
Size: 238190 Color: 1

Bin 47: 35 of cap free
Amount of items: 2
Items: 
Size: 522226 Color: 3
Size: 477740 Color: 0

Bin 48: 38 of cap free
Amount of items: 2
Items: 
Size: 538049 Color: 1
Size: 461914 Color: 4

Bin 49: 38 of cap free
Amount of items: 2
Items: 
Size: 563743 Color: 4
Size: 436220 Color: 0

Bin 50: 38 of cap free
Amount of items: 3
Items: 
Size: 662413 Color: 0
Size: 192970 Color: 4
Size: 144580 Color: 1

Bin 51: 40 of cap free
Amount of items: 2
Items: 
Size: 508556 Color: 3
Size: 491405 Color: 0

Bin 52: 40 of cap free
Amount of items: 2
Items: 
Size: 604152 Color: 2
Size: 395809 Color: 4

Bin 53: 41 of cap free
Amount of items: 2
Items: 
Size: 568628 Color: 2
Size: 431332 Color: 3

Bin 54: 41 of cap free
Amount of items: 2
Items: 
Size: 729502 Color: 0
Size: 270458 Color: 2

Bin 55: 42 of cap free
Amount of items: 2
Items: 
Size: 519314 Color: 3
Size: 480645 Color: 1

Bin 56: 42 of cap free
Amount of items: 2
Items: 
Size: 568312 Color: 4
Size: 431647 Color: 3

Bin 57: 42 of cap free
Amount of items: 2
Items: 
Size: 579807 Color: 3
Size: 420152 Color: 2

Bin 58: 42 of cap free
Amount of items: 2
Items: 
Size: 592622 Color: 4
Size: 407337 Color: 3

Bin 59: 42 of cap free
Amount of items: 2
Items: 
Size: 648816 Color: 3
Size: 351143 Color: 4

Bin 60: 43 of cap free
Amount of items: 2
Items: 
Size: 538900 Color: 4
Size: 461058 Color: 0

Bin 61: 43 of cap free
Amount of items: 2
Items: 
Size: 602524 Color: 0
Size: 397434 Color: 3

Bin 62: 44 of cap free
Amount of items: 2
Items: 
Size: 656669 Color: 1
Size: 343288 Color: 2

Bin 63: 45 of cap free
Amount of items: 3
Items: 
Size: 659280 Color: 0
Size: 189464 Color: 2
Size: 151212 Color: 1

Bin 64: 45 of cap free
Amount of items: 3
Items: 
Size: 690478 Color: 0
Size: 196906 Color: 2
Size: 112572 Color: 1

Bin 65: 45 of cap free
Amount of items: 2
Items: 
Size: 746458 Color: 0
Size: 253498 Color: 3

Bin 66: 46 of cap free
Amount of items: 2
Items: 
Size: 664939 Color: 2
Size: 335016 Color: 1

Bin 67: 47 of cap free
Amount of items: 2
Items: 
Size: 728372 Color: 1
Size: 271582 Color: 3

Bin 68: 48 of cap free
Amount of items: 2
Items: 
Size: 735892 Color: 1
Size: 264061 Color: 0

Bin 69: 49 of cap free
Amount of items: 2
Items: 
Size: 722111 Color: 1
Size: 277841 Color: 4

Bin 70: 49 of cap free
Amount of items: 2
Items: 
Size: 746810 Color: 4
Size: 253142 Color: 1

Bin 71: 51 of cap free
Amount of items: 2
Items: 
Size: 575974 Color: 1
Size: 423976 Color: 0

Bin 72: 51 of cap free
Amount of items: 2
Items: 
Size: 587739 Color: 0
Size: 412211 Color: 3

Bin 73: 51 of cap free
Amount of items: 3
Items: 
Size: 658340 Color: 2
Size: 188142 Color: 3
Size: 153468 Color: 2

Bin 74: 52 of cap free
Amount of items: 2
Items: 
Size: 549220 Color: 1
Size: 450729 Color: 4

Bin 75: 52 of cap free
Amount of items: 2
Items: 
Size: 552104 Color: 0
Size: 447845 Color: 1

Bin 76: 52 of cap free
Amount of items: 2
Items: 
Size: 621687 Color: 3
Size: 378262 Color: 4

Bin 77: 52 of cap free
Amount of items: 2
Items: 
Size: 669621 Color: 4
Size: 330328 Color: 3

Bin 78: 52 of cap free
Amount of items: 3
Items: 
Size: 689014 Color: 1
Size: 196327 Color: 4
Size: 114608 Color: 4

Bin 79: 53 of cap free
Amount of items: 2
Items: 
Size: 781736 Color: 3
Size: 218212 Color: 4

Bin 80: 54 of cap free
Amount of items: 2
Items: 
Size: 626501 Color: 2
Size: 373446 Color: 0

Bin 81: 54 of cap free
Amount of items: 2
Items: 
Size: 758504 Color: 4
Size: 241443 Color: 0

Bin 82: 55 of cap free
Amount of items: 2
Items: 
Size: 602057 Color: 2
Size: 397889 Color: 0

Bin 83: 55 of cap free
Amount of items: 2
Items: 
Size: 608688 Color: 2
Size: 391258 Color: 4

Bin 84: 55 of cap free
Amount of items: 2
Items: 
Size: 708251 Color: 0
Size: 291695 Color: 4

Bin 85: 57 of cap free
Amount of items: 2
Items: 
Size: 560931 Color: 2
Size: 439013 Color: 1

Bin 86: 57 of cap free
Amount of items: 2
Items: 
Size: 612001 Color: 4
Size: 387943 Color: 0

Bin 87: 58 of cap free
Amount of items: 2
Items: 
Size: 515435 Color: 1
Size: 484508 Color: 4

Bin 88: 58 of cap free
Amount of items: 2
Items: 
Size: 727501 Color: 1
Size: 272442 Color: 3

Bin 89: 59 of cap free
Amount of items: 2
Items: 
Size: 550336 Color: 0
Size: 449606 Color: 2

Bin 90: 59 of cap free
Amount of items: 2
Items: 
Size: 644816 Color: 0
Size: 355126 Color: 2

Bin 91: 59 of cap free
Amount of items: 2
Items: 
Size: 702687 Color: 1
Size: 297255 Color: 2

Bin 92: 59 of cap free
Amount of items: 2
Items: 
Size: 720865 Color: 0
Size: 279077 Color: 2

Bin 93: 59 of cap free
Amount of items: 2
Items: 
Size: 758654 Color: 0
Size: 241288 Color: 2

Bin 94: 61 of cap free
Amount of items: 2
Items: 
Size: 744911 Color: 2
Size: 255029 Color: 3

Bin 95: 62 of cap free
Amount of items: 2
Items: 
Size: 592416 Color: 3
Size: 407523 Color: 0

Bin 96: 62 of cap free
Amount of items: 2
Items: 
Size: 695714 Color: 2
Size: 304225 Color: 3

Bin 97: 62 of cap free
Amount of items: 2
Items: 
Size: 793120 Color: 4
Size: 206819 Color: 0

Bin 98: 65 of cap free
Amount of items: 2
Items: 
Size: 637469 Color: 2
Size: 362467 Color: 4

Bin 99: 66 of cap free
Amount of items: 2
Items: 
Size: 605572 Color: 4
Size: 394363 Color: 3

Bin 100: 66 of cap free
Amount of items: 2
Items: 
Size: 690491 Color: 3
Size: 309444 Color: 2

Bin 101: 66 of cap free
Amount of items: 2
Items: 
Size: 798268 Color: 1
Size: 201667 Color: 4

Bin 102: 69 of cap free
Amount of items: 2
Items: 
Size: 502365 Color: 4
Size: 497567 Color: 0

Bin 103: 69 of cap free
Amount of items: 2
Items: 
Size: 593857 Color: 1
Size: 406075 Color: 0

Bin 104: 69 of cap free
Amount of items: 2
Items: 
Size: 734090 Color: 3
Size: 265842 Color: 4

Bin 105: 70 of cap free
Amount of items: 2
Items: 
Size: 563736 Color: 2
Size: 436195 Color: 1

Bin 106: 70 of cap free
Amount of items: 2
Items: 
Size: 740162 Color: 3
Size: 259769 Color: 2

Bin 107: 70 of cap free
Amount of items: 2
Items: 
Size: 774801 Color: 2
Size: 225130 Color: 1

Bin 108: 71 of cap free
Amount of items: 2
Items: 
Size: 579829 Color: 4
Size: 420101 Color: 3

Bin 109: 71 of cap free
Amount of items: 2
Items: 
Size: 710107 Color: 1
Size: 289823 Color: 3

Bin 110: 71 of cap free
Amount of items: 2
Items: 
Size: 760375 Color: 1
Size: 239555 Color: 0

Bin 111: 72 of cap free
Amount of items: 2
Items: 
Size: 672660 Color: 2
Size: 327269 Color: 4

Bin 112: 72 of cap free
Amount of items: 2
Items: 
Size: 682698 Color: 3
Size: 317231 Color: 4

Bin 113: 73 of cap free
Amount of items: 2
Items: 
Size: 694449 Color: 4
Size: 305479 Color: 0

Bin 114: 73 of cap free
Amount of items: 2
Items: 
Size: 762309 Color: 1
Size: 237619 Color: 0

Bin 115: 75 of cap free
Amount of items: 2
Items: 
Size: 525840 Color: 0
Size: 474086 Color: 2

Bin 116: 76 of cap free
Amount of items: 2
Items: 
Size: 609107 Color: 4
Size: 390818 Color: 3

Bin 117: 76 of cap free
Amount of items: 2
Items: 
Size: 628570 Color: 1
Size: 371355 Color: 0

Bin 118: 77 of cap free
Amount of items: 2
Items: 
Size: 536486 Color: 1
Size: 463438 Color: 0

Bin 119: 79 of cap free
Amount of items: 2
Items: 
Size: 505328 Color: 1
Size: 494594 Color: 2

Bin 120: 79 of cap free
Amount of items: 2
Items: 
Size: 579475 Color: 4
Size: 420447 Color: 0

Bin 121: 79 of cap free
Amount of items: 2
Items: 
Size: 625469 Color: 0
Size: 374453 Color: 2

Bin 122: 80 of cap free
Amount of items: 3
Items: 
Size: 434770 Color: 2
Size: 431513 Color: 1
Size: 133638 Color: 2

Bin 123: 80 of cap free
Amount of items: 2
Items: 
Size: 640914 Color: 3
Size: 359007 Color: 1

Bin 124: 81 of cap free
Amount of items: 2
Items: 
Size: 572469 Color: 3
Size: 427451 Color: 4

Bin 125: 82 of cap free
Amount of items: 2
Items: 
Size: 636982 Color: 0
Size: 362937 Color: 4

Bin 126: 82 of cap free
Amount of items: 2
Items: 
Size: 700658 Color: 4
Size: 299261 Color: 0

Bin 127: 83 of cap free
Amount of items: 2
Items: 
Size: 526877 Color: 0
Size: 473041 Color: 2

Bin 128: 83 of cap free
Amount of items: 2
Items: 
Size: 619972 Color: 0
Size: 379946 Color: 3

Bin 129: 84 of cap free
Amount of items: 2
Items: 
Size: 685466 Color: 2
Size: 314451 Color: 1

Bin 130: 87 of cap free
Amount of items: 2
Items: 
Size: 539452 Color: 2
Size: 460462 Color: 3

Bin 131: 88 of cap free
Amount of items: 2
Items: 
Size: 614013 Color: 2
Size: 385900 Color: 0

Bin 132: 88 of cap free
Amount of items: 2
Items: 
Size: 718362 Color: 2
Size: 281551 Color: 0

Bin 133: 90 of cap free
Amount of items: 2
Items: 
Size: 522591 Color: 1
Size: 477320 Color: 4

Bin 134: 90 of cap free
Amount of items: 2
Items: 
Size: 582761 Color: 3
Size: 417150 Color: 1

Bin 135: 90 of cap free
Amount of items: 2
Items: 
Size: 618272 Color: 3
Size: 381639 Color: 2

Bin 136: 90 of cap free
Amount of items: 3
Items: 
Size: 657654 Color: 2
Size: 187854 Color: 1
Size: 154403 Color: 4

Bin 137: 91 of cap free
Amount of items: 2
Items: 
Size: 621759 Color: 1
Size: 378151 Color: 4

Bin 138: 92 of cap free
Amount of items: 2
Items: 
Size: 708816 Color: 2
Size: 291093 Color: 0

Bin 139: 92 of cap free
Amount of items: 2
Items: 
Size: 747678 Color: 3
Size: 252231 Color: 2

Bin 140: 95 of cap free
Amount of items: 2
Items: 
Size: 732638 Color: 2
Size: 267268 Color: 1

Bin 141: 96 of cap free
Amount of items: 2
Items: 
Size: 526776 Color: 1
Size: 473129 Color: 3

Bin 142: 96 of cap free
Amount of items: 2
Items: 
Size: 617828 Color: 2
Size: 382077 Color: 3

Bin 143: 97 of cap free
Amount of items: 2
Items: 
Size: 708911 Color: 1
Size: 290993 Color: 2

Bin 144: 97 of cap free
Amount of items: 2
Items: 
Size: 764251 Color: 3
Size: 235653 Color: 1

Bin 145: 98 of cap free
Amount of items: 6
Items: 
Size: 169400 Color: 4
Size: 168586 Color: 1
Size: 168131 Color: 4
Size: 167924 Color: 1
Size: 167369 Color: 0
Size: 158493 Color: 2

Bin 146: 98 of cap free
Amount of items: 2
Items: 
Size: 642360 Color: 4
Size: 357543 Color: 1

Bin 147: 99 of cap free
Amount of items: 2
Items: 
Size: 506729 Color: 3
Size: 493173 Color: 1

Bin 148: 100 of cap free
Amount of items: 2
Items: 
Size: 648197 Color: 3
Size: 351704 Color: 1

Bin 149: 100 of cap free
Amount of items: 2
Items: 
Size: 725082 Color: 3
Size: 274819 Color: 1

Bin 150: 101 of cap free
Amount of items: 2
Items: 
Size: 702248 Color: 0
Size: 297652 Color: 3

Bin 151: 103 of cap free
Amount of items: 2
Items: 
Size: 557277 Color: 0
Size: 442621 Color: 4

Bin 152: 103 of cap free
Amount of items: 2
Items: 
Size: 644515 Color: 1
Size: 355383 Color: 2

Bin 153: 103 of cap free
Amount of items: 2
Items: 
Size: 660790 Color: 0
Size: 339108 Color: 1

Bin 154: 103 of cap free
Amount of items: 2
Items: 
Size: 672873 Color: 1
Size: 327025 Color: 2

Bin 155: 104 of cap free
Amount of items: 2
Items: 
Size: 516415 Color: 4
Size: 483482 Color: 3

Bin 156: 104 of cap free
Amount of items: 2
Items: 
Size: 698893 Color: 1
Size: 301004 Color: 0

Bin 157: 105 of cap free
Amount of items: 8
Items: 
Size: 127383 Color: 4
Size: 127898 Color: 3
Size: 126823 Color: 4
Size: 125989 Color: 0
Size: 125256 Color: 3
Size: 125178 Color: 4
Size: 125120 Color: 2
Size: 116249 Color: 4

Bin 158: 106 of cap free
Amount of items: 2
Items: 
Size: 584527 Color: 1
Size: 415368 Color: 2

Bin 159: 107 of cap free
Amount of items: 2
Items: 
Size: 799826 Color: 4
Size: 200068 Color: 0

Bin 160: 108 of cap free
Amount of items: 2
Items: 
Size: 550309 Color: 0
Size: 449584 Color: 2

Bin 161: 109 of cap free
Amount of items: 2
Items: 
Size: 563314 Color: 3
Size: 436578 Color: 2

Bin 162: 110 of cap free
Amount of items: 6
Items: 
Size: 179165 Color: 0
Size: 178796 Color: 2
Size: 178492 Color: 3
Size: 178373 Color: 0
Size: 177571 Color: 4
Size: 107494 Color: 0

Bin 163: 110 of cap free
Amount of items: 2
Items: 
Size: 646800 Color: 3
Size: 353091 Color: 4

Bin 164: 111 of cap free
Amount of items: 2
Items: 
Size: 508206 Color: 4
Size: 491684 Color: 0

Bin 165: 111 of cap free
Amount of items: 2
Items: 
Size: 531352 Color: 4
Size: 468538 Color: 3

Bin 166: 111 of cap free
Amount of items: 2
Items: 
Size: 641619 Color: 4
Size: 358271 Color: 0

Bin 167: 111 of cap free
Amount of items: 2
Items: 
Size: 794651 Color: 0
Size: 205239 Color: 4

Bin 168: 112 of cap free
Amount of items: 2
Items: 
Size: 684575 Color: 2
Size: 315314 Color: 4

Bin 169: 113 of cap free
Amount of items: 2
Items: 
Size: 745547 Color: 1
Size: 254341 Color: 4

Bin 170: 113 of cap free
Amount of items: 2
Items: 
Size: 796099 Color: 1
Size: 203789 Color: 4

Bin 171: 114 of cap free
Amount of items: 2
Items: 
Size: 529417 Color: 3
Size: 470470 Color: 4

Bin 172: 114 of cap free
Amount of items: 2
Items: 
Size: 760565 Color: 2
Size: 239322 Color: 4

Bin 173: 115 of cap free
Amount of items: 2
Items: 
Size: 571926 Color: 3
Size: 427960 Color: 4

Bin 174: 116 of cap free
Amount of items: 2
Items: 
Size: 520122 Color: 0
Size: 479763 Color: 3

Bin 175: 116 of cap free
Amount of items: 2
Items: 
Size: 689147 Color: 0
Size: 310738 Color: 1

Bin 176: 116 of cap free
Amount of items: 2
Items: 
Size: 704157 Color: 1
Size: 295728 Color: 4

Bin 177: 117 of cap free
Amount of items: 2
Items: 
Size: 514598 Color: 1
Size: 485286 Color: 3

Bin 178: 118 of cap free
Amount of items: 2
Items: 
Size: 584013 Color: 4
Size: 415870 Color: 0

Bin 179: 120 of cap free
Amount of items: 6
Items: 
Size: 171722 Color: 4
Size: 171643 Color: 3
Size: 170862 Color: 0
Size: 170494 Color: 1
Size: 169662 Color: 0
Size: 145498 Color: 0

Bin 180: 120 of cap free
Amount of items: 2
Items: 
Size: 561197 Color: 2
Size: 438684 Color: 4

Bin 181: 120 of cap free
Amount of items: 2
Items: 
Size: 620781 Color: 4
Size: 379100 Color: 2

Bin 182: 121 of cap free
Amount of items: 2
Items: 
Size: 544902 Color: 3
Size: 454978 Color: 2

Bin 183: 122 of cap free
Amount of items: 2
Items: 
Size: 556135 Color: 3
Size: 443744 Color: 0

Bin 184: 123 of cap free
Amount of items: 3
Items: 
Size: 662381 Color: 3
Size: 192452 Color: 4
Size: 145045 Color: 0

Bin 185: 123 of cap free
Amount of items: 2
Items: 
Size: 693958 Color: 0
Size: 305920 Color: 2

Bin 186: 124 of cap free
Amount of items: 2
Items: 
Size: 534551 Color: 1
Size: 465326 Color: 2

Bin 187: 124 of cap free
Amount of items: 2
Items: 
Size: 535650 Color: 0
Size: 464227 Color: 4

Bin 188: 124 of cap free
Amount of items: 3
Items: 
Size: 688235 Color: 2
Size: 194381 Color: 4
Size: 117261 Color: 3

Bin 189: 125 of cap free
Amount of items: 2
Items: 
Size: 576327 Color: 4
Size: 423549 Color: 3

Bin 190: 125 of cap free
Amount of items: 2
Items: 
Size: 584681 Color: 3
Size: 415195 Color: 4

Bin 191: 125 of cap free
Amount of items: 2
Items: 
Size: 746224 Color: 1
Size: 253652 Color: 4

Bin 192: 126 of cap free
Amount of items: 2
Items: 
Size: 602594 Color: 0
Size: 397281 Color: 2

Bin 193: 127 of cap free
Amount of items: 2
Items: 
Size: 618828 Color: 2
Size: 381046 Color: 4

Bin 194: 128 of cap free
Amount of items: 2
Items: 
Size: 528427 Color: 2
Size: 471446 Color: 4

Bin 195: 128 of cap free
Amount of items: 2
Items: 
Size: 575003 Color: 4
Size: 424870 Color: 3

Bin 196: 129 of cap free
Amount of items: 2
Items: 
Size: 570297 Color: 1
Size: 429575 Color: 3

Bin 197: 130 of cap free
Amount of items: 2
Items: 
Size: 550107 Color: 4
Size: 449764 Color: 0

Bin 198: 132 of cap free
Amount of items: 2
Items: 
Size: 538638 Color: 0
Size: 461231 Color: 4

Bin 199: 134 of cap free
Amount of items: 2
Items: 
Size: 582236 Color: 0
Size: 417631 Color: 4

Bin 200: 136 of cap free
Amount of items: 6
Items: 
Size: 173885 Color: 1
Size: 172314 Color: 4
Size: 172110 Color: 0
Size: 171865 Color: 3
Size: 171769 Color: 1
Size: 137922 Color: 4

Bin 201: 136 of cap free
Amount of items: 2
Items: 
Size: 670980 Color: 0
Size: 328885 Color: 2

Bin 202: 136 of cap free
Amount of items: 2
Items: 
Size: 706483 Color: 4
Size: 293382 Color: 2

Bin 203: 137 of cap free
Amount of items: 2
Items: 
Size: 519680 Color: 1
Size: 480184 Color: 0

Bin 204: 137 of cap free
Amount of items: 2
Items: 
Size: 760136 Color: 4
Size: 239728 Color: 2

Bin 205: 138 of cap free
Amount of items: 2
Items: 
Size: 529848 Color: 2
Size: 470015 Color: 3

Bin 206: 138 of cap free
Amount of items: 2
Items: 
Size: 557375 Color: 4
Size: 442488 Color: 1

Bin 207: 142 of cap free
Amount of items: 2
Items: 
Size: 502538 Color: 2
Size: 497321 Color: 1

Bin 208: 142 of cap free
Amount of items: 2
Items: 
Size: 640301 Color: 3
Size: 359558 Color: 1

Bin 209: 142 of cap free
Amount of items: 2
Items: 
Size: 680106 Color: 3
Size: 319753 Color: 0

Bin 210: 143 of cap free
Amount of items: 2
Items: 
Size: 682796 Color: 4
Size: 317062 Color: 2

Bin 211: 144 of cap free
Amount of items: 2
Items: 
Size: 542556 Color: 2
Size: 457301 Color: 1

Bin 212: 146 of cap free
Amount of items: 2
Items: 
Size: 656204 Color: 2
Size: 343651 Color: 0

Bin 213: 149 of cap free
Amount of items: 2
Items: 
Size: 591949 Color: 3
Size: 407903 Color: 4

Bin 214: 150 of cap free
Amount of items: 3
Items: 
Size: 687985 Color: 2
Size: 194802 Color: 0
Size: 117064 Color: 3

Bin 215: 152 of cap free
Amount of items: 2
Items: 
Size: 575576 Color: 1
Size: 424273 Color: 4

Bin 216: 153 of cap free
Amount of items: 2
Items: 
Size: 765529 Color: 0
Size: 234319 Color: 3

Bin 217: 154 of cap free
Amount of items: 2
Items: 
Size: 502954 Color: 2
Size: 496893 Color: 0

Bin 218: 155 of cap free
Amount of items: 2
Items: 
Size: 504610 Color: 3
Size: 495236 Color: 1

Bin 219: 156 of cap free
Amount of items: 2
Items: 
Size: 599080 Color: 0
Size: 400765 Color: 4

Bin 220: 156 of cap free
Amount of items: 2
Items: 
Size: 747508 Color: 1
Size: 252337 Color: 2

Bin 221: 157 of cap free
Amount of items: 2
Items: 
Size: 521906 Color: 4
Size: 477938 Color: 2

Bin 222: 157 of cap free
Amount of items: 2
Items: 
Size: 571919 Color: 2
Size: 427925 Color: 0

Bin 223: 157 of cap free
Amount of items: 2
Items: 
Size: 628709 Color: 2
Size: 371135 Color: 3

Bin 224: 157 of cap free
Amount of items: 3
Items: 
Size: 659636 Color: 1
Size: 190254 Color: 4
Size: 149954 Color: 4

Bin 225: 157 of cap free
Amount of items: 2
Items: 
Size: 782160 Color: 2
Size: 217684 Color: 1

Bin 226: 158 of cap free
Amount of items: 2
Items: 
Size: 692369 Color: 2
Size: 307474 Color: 3

Bin 227: 159 of cap free
Amount of items: 2
Items: 
Size: 547635 Color: 0
Size: 452207 Color: 4

Bin 228: 160 of cap free
Amount of items: 2
Items: 
Size: 709770 Color: 3
Size: 290071 Color: 0

Bin 229: 160 of cap free
Amount of items: 2
Items: 
Size: 719526 Color: 1
Size: 280315 Color: 2

Bin 230: 161 of cap free
Amount of items: 2
Items: 
Size: 592612 Color: 1
Size: 407228 Color: 4

Bin 231: 163 of cap free
Amount of items: 2
Items: 
Size: 646939 Color: 3
Size: 352899 Color: 2

Bin 232: 165 of cap free
Amount of items: 7
Items: 
Size: 146104 Color: 2
Size: 143738 Color: 4
Size: 144178 Color: 0
Size: 143496 Color: 1
Size: 143476 Color: 4
Size: 143035 Color: 1
Size: 135809 Color: 4

Bin 233: 165 of cap free
Amount of items: 2
Items: 
Size: 521157 Color: 0
Size: 478679 Color: 3

Bin 234: 165 of cap free
Amount of items: 2
Items: 
Size: 723226 Color: 0
Size: 276610 Color: 1

Bin 235: 165 of cap free
Amount of items: 2
Items: 
Size: 735543 Color: 2
Size: 264293 Color: 4

Bin 236: 167 of cap free
Amount of items: 2
Items: 
Size: 717789 Color: 2
Size: 282045 Color: 0

Bin 237: 171 of cap free
Amount of items: 2
Items: 
Size: 627664 Color: 4
Size: 372166 Color: 1

Bin 238: 171 of cap free
Amount of items: 2
Items: 
Size: 693608 Color: 0
Size: 306222 Color: 3

Bin 239: 172 of cap free
Amount of items: 2
Items: 
Size: 657810 Color: 3
Size: 342019 Color: 0

Bin 240: 173 of cap free
Amount of items: 2
Items: 
Size: 563508 Color: 0
Size: 436320 Color: 2

Bin 241: 173 of cap free
Amount of items: 2
Items: 
Size: 577637 Color: 4
Size: 422191 Color: 0

Bin 242: 173 of cap free
Amount of items: 2
Items: 
Size: 585389 Color: 0
Size: 414439 Color: 4

Bin 243: 173 of cap free
Amount of items: 2
Items: 
Size: 796214 Color: 0
Size: 203614 Color: 2

Bin 244: 174 of cap free
Amount of items: 2
Items: 
Size: 548446 Color: 1
Size: 451381 Color: 2

Bin 245: 175 of cap free
Amount of items: 2
Items: 
Size: 663404 Color: 3
Size: 336422 Color: 4

Bin 246: 175 of cap free
Amount of items: 2
Items: 
Size: 764008 Color: 4
Size: 235818 Color: 3

Bin 247: 176 of cap free
Amount of items: 2
Items: 
Size: 586943 Color: 4
Size: 412882 Color: 1

Bin 248: 176 of cap free
Amount of items: 2
Items: 
Size: 592262 Color: 2
Size: 407563 Color: 3

Bin 249: 176 of cap free
Amount of items: 2
Items: 
Size: 676392 Color: 2
Size: 323433 Color: 1

Bin 250: 176 of cap free
Amount of items: 2
Items: 
Size: 721266 Color: 0
Size: 278559 Color: 3

Bin 251: 176 of cap free
Amount of items: 2
Items: 
Size: 762138 Color: 0
Size: 237687 Color: 4

Bin 252: 176 of cap free
Amount of items: 2
Items: 
Size: 783289 Color: 2
Size: 216536 Color: 0

Bin 253: 177 of cap free
Amount of items: 2
Items: 
Size: 750895 Color: 0
Size: 248929 Color: 3

Bin 254: 178 of cap free
Amount of items: 2
Items: 
Size: 738199 Color: 2
Size: 261624 Color: 3

Bin 255: 179 of cap free
Amount of items: 2
Items: 
Size: 550710 Color: 2
Size: 449112 Color: 0

Bin 256: 180 of cap free
Amount of items: 2
Items: 
Size: 519425 Color: 1
Size: 480396 Color: 4

Bin 257: 180 of cap free
Amount of items: 2
Items: 
Size: 678345 Color: 0
Size: 321476 Color: 1

Bin 258: 182 of cap free
Amount of items: 3
Items: 
Size: 669677 Color: 3
Size: 193800 Color: 4
Size: 136342 Color: 4

Bin 259: 182 of cap free
Amount of items: 2
Items: 
Size: 689344 Color: 1
Size: 310475 Color: 4

Bin 260: 185 of cap free
Amount of items: 2
Items: 
Size: 627275 Color: 2
Size: 372541 Color: 0

Bin 261: 185 of cap free
Amount of items: 2
Items: 
Size: 658060 Color: 2
Size: 341756 Color: 0

Bin 262: 186 of cap free
Amount of items: 2
Items: 
Size: 579101 Color: 1
Size: 420714 Color: 0

Bin 263: 186 of cap free
Amount of items: 3
Items: 
Size: 662352 Color: 3
Size: 190935 Color: 4
Size: 146528 Color: 4

Bin 264: 190 of cap free
Amount of items: 2
Items: 
Size: 533076 Color: 4
Size: 466735 Color: 3

Bin 265: 190 of cap free
Amount of items: 2
Items: 
Size: 658097 Color: 0
Size: 341714 Color: 1

Bin 266: 191 of cap free
Amount of items: 2
Items: 
Size: 560654 Color: 3
Size: 439156 Color: 4

Bin 267: 191 of cap free
Amount of items: 2
Items: 
Size: 766579 Color: 2
Size: 233231 Color: 0

Bin 268: 195 of cap free
Amount of items: 2
Items: 
Size: 717777 Color: 0
Size: 282029 Color: 3

Bin 269: 196 of cap free
Amount of items: 2
Items: 
Size: 606794 Color: 3
Size: 393011 Color: 1

Bin 270: 196 of cap free
Amount of items: 3
Items: 
Size: 659358 Color: 2
Size: 189616 Color: 1
Size: 150831 Color: 2

Bin 271: 197 of cap free
Amount of items: 2
Items: 
Size: 530294 Color: 0
Size: 469510 Color: 4

Bin 272: 197 of cap free
Amount of items: 2
Items: 
Size: 737507 Color: 4
Size: 262297 Color: 0

Bin 273: 201 of cap free
Amount of items: 2
Items: 
Size: 539442 Color: 4
Size: 460358 Color: 1

Bin 274: 203 of cap free
Amount of items: 2
Items: 
Size: 578581 Color: 0
Size: 421217 Color: 4

Bin 275: 206 of cap free
Amount of items: 2
Items: 
Size: 688300 Color: 1
Size: 311495 Color: 2

Bin 276: 208 of cap free
Amount of items: 2
Items: 
Size: 678517 Color: 0
Size: 321276 Color: 4

Bin 277: 212 of cap free
Amount of items: 2
Items: 
Size: 576739 Color: 0
Size: 423050 Color: 1

Bin 278: 213 of cap free
Amount of items: 2
Items: 
Size: 789138 Color: 1
Size: 210650 Color: 4

Bin 279: 214 of cap free
Amount of items: 3
Items: 
Size: 688923 Color: 4
Size: 196141 Color: 2
Size: 114723 Color: 2

Bin 280: 214 of cap free
Amount of items: 2
Items: 
Size: 724313 Color: 1
Size: 275474 Color: 0

Bin 281: 214 of cap free
Amount of items: 2
Items: 
Size: 783834 Color: 3
Size: 215953 Color: 1

Bin 282: 215 of cap free
Amount of items: 2
Items: 
Size: 598741 Color: 0
Size: 401045 Color: 4

Bin 283: 215 of cap free
Amount of items: 2
Items: 
Size: 686761 Color: 1
Size: 313025 Color: 4

Bin 284: 215 of cap free
Amount of items: 2
Items: 
Size: 762897 Color: 3
Size: 236889 Color: 0

Bin 285: 216 of cap free
Amount of items: 2
Items: 
Size: 703378 Color: 3
Size: 296407 Color: 1

Bin 286: 219 of cap free
Amount of items: 2
Items: 
Size: 692611 Color: 0
Size: 307171 Color: 1

Bin 287: 219 of cap free
Amount of items: 2
Items: 
Size: 711592 Color: 4
Size: 288190 Color: 2

Bin 288: 220 of cap free
Amount of items: 2
Items: 
Size: 515826 Color: 3
Size: 483955 Color: 4

Bin 289: 221 of cap free
Amount of items: 2
Items: 
Size: 682162 Color: 4
Size: 317618 Color: 2

Bin 290: 224 of cap free
Amount of items: 2
Items: 
Size: 562845 Color: 4
Size: 436932 Color: 0

Bin 291: 224 of cap free
Amount of items: 2
Items: 
Size: 753703 Color: 0
Size: 246074 Color: 4

Bin 292: 227 of cap free
Amount of items: 2
Items: 
Size: 707075 Color: 4
Size: 292699 Color: 3

Bin 293: 228 of cap free
Amount of items: 2
Items: 
Size: 766386 Color: 2
Size: 233387 Color: 3

Bin 294: 228 of cap free
Amount of items: 2
Items: 
Size: 797863 Color: 4
Size: 201910 Color: 2

Bin 295: 229 of cap free
Amount of items: 2
Items: 
Size: 664667 Color: 3
Size: 335105 Color: 1

Bin 296: 229 of cap free
Amount of items: 2
Items: 
Size: 780211 Color: 3
Size: 219561 Color: 2

Bin 297: 230 of cap free
Amount of items: 2
Items: 
Size: 652627 Color: 2
Size: 347144 Color: 3

Bin 298: 232 of cap free
Amount of items: 2
Items: 
Size: 661007 Color: 4
Size: 338762 Color: 2

Bin 299: 233 of cap free
Amount of items: 2
Items: 
Size: 714673 Color: 1
Size: 285095 Color: 4

Bin 300: 234 of cap free
Amount of items: 2
Items: 
Size: 681308 Color: 0
Size: 318459 Color: 2

Bin 301: 234 of cap free
Amount of items: 2
Items: 
Size: 759430 Color: 3
Size: 240337 Color: 4

Bin 302: 235 of cap free
Amount of items: 2
Items: 
Size: 626028 Color: 2
Size: 373738 Color: 0

Bin 303: 237 of cap free
Amount of items: 2
Items: 
Size: 518274 Color: 3
Size: 481490 Color: 1

Bin 304: 237 of cap free
Amount of items: 2
Items: 
Size: 519650 Color: 2
Size: 480114 Color: 4

Bin 305: 237 of cap free
Amount of items: 2
Items: 
Size: 572948 Color: 2
Size: 426816 Color: 1

Bin 306: 238 of cap free
Amount of items: 2
Items: 
Size: 507858 Color: 3
Size: 491905 Color: 4

Bin 307: 239 of cap free
Amount of items: 2
Items: 
Size: 510797 Color: 4
Size: 488965 Color: 2

Bin 308: 239 of cap free
Amount of items: 2
Items: 
Size: 619915 Color: 1
Size: 379847 Color: 2

Bin 309: 239 of cap free
Amount of items: 2
Items: 
Size: 682961 Color: 3
Size: 316801 Color: 4

Bin 310: 239 of cap free
Amount of items: 2
Items: 
Size: 735949 Color: 1
Size: 263813 Color: 3

Bin 311: 240 of cap free
Amount of items: 2
Items: 
Size: 754273 Color: 3
Size: 245488 Color: 2

Bin 312: 241 of cap free
Amount of items: 2
Items: 
Size: 731561 Color: 2
Size: 268199 Color: 0

Bin 313: 244 of cap free
Amount of items: 2
Items: 
Size: 662964 Color: 4
Size: 336793 Color: 0

Bin 314: 246 of cap free
Amount of items: 2
Items: 
Size: 650597 Color: 1
Size: 349158 Color: 3

Bin 315: 247 of cap free
Amount of items: 2
Items: 
Size: 621937 Color: 1
Size: 377817 Color: 4

Bin 316: 248 of cap free
Amount of items: 2
Items: 
Size: 646142 Color: 4
Size: 353611 Color: 3

Bin 317: 249 of cap free
Amount of items: 2
Items: 
Size: 590820 Color: 3
Size: 408932 Color: 0

Bin 318: 252 of cap free
Amount of items: 2
Items: 
Size: 649527 Color: 4
Size: 350222 Color: 0

Bin 319: 254 of cap free
Amount of items: 2
Items: 
Size: 751522 Color: 0
Size: 248225 Color: 4

Bin 320: 255 of cap free
Amount of items: 2
Items: 
Size: 639068 Color: 0
Size: 360678 Color: 3

Bin 321: 259 of cap free
Amount of items: 2
Items: 
Size: 566410 Color: 2
Size: 433332 Color: 1

Bin 322: 259 of cap free
Amount of items: 2
Items: 
Size: 596543 Color: 3
Size: 403199 Color: 1

Bin 323: 260 of cap free
Amount of items: 2
Items: 
Size: 502339 Color: 2
Size: 497402 Color: 0

Bin 324: 260 of cap free
Amount of items: 2
Items: 
Size: 769509 Color: 1
Size: 230232 Color: 3

Bin 325: 261 of cap free
Amount of items: 2
Items: 
Size: 524422 Color: 2
Size: 475318 Color: 4

Bin 326: 261 of cap free
Amount of items: 2
Items: 
Size: 729116 Color: 4
Size: 270624 Color: 1

Bin 327: 262 of cap free
Amount of items: 3
Items: 
Size: 662325 Color: 0
Size: 191243 Color: 3
Size: 146171 Color: 3

Bin 328: 262 of cap free
Amount of items: 2
Items: 
Size: 758924 Color: 1
Size: 240815 Color: 2

Bin 329: 263 of cap free
Amount of items: 2
Items: 
Size: 531941 Color: 1
Size: 467797 Color: 4

Bin 330: 263 of cap free
Amount of items: 2
Items: 
Size: 635300 Color: 1
Size: 364438 Color: 3

Bin 331: 264 of cap free
Amount of items: 2
Items: 
Size: 781180 Color: 0
Size: 218557 Color: 3

Bin 332: 265 of cap free
Amount of items: 2
Items: 
Size: 539652 Color: 4
Size: 460084 Color: 2

Bin 333: 266 of cap free
Amount of items: 2
Items: 
Size: 572626 Color: 0
Size: 427109 Color: 2

Bin 334: 266 of cap free
Amount of items: 2
Items: 
Size: 676621 Color: 3
Size: 323114 Color: 1

Bin 335: 267 of cap free
Amount of items: 2
Items: 
Size: 555739 Color: 0
Size: 443995 Color: 4

Bin 336: 269 of cap free
Amount of items: 2
Items: 
Size: 561780 Color: 1
Size: 437952 Color: 3

Bin 337: 273 of cap free
Amount of items: 2
Items: 
Size: 604451 Color: 2
Size: 395277 Color: 0

Bin 338: 274 of cap free
Amount of items: 2
Items: 
Size: 601335 Color: 0
Size: 398392 Color: 4

Bin 339: 276 of cap free
Amount of items: 2
Items: 
Size: 629516 Color: 1
Size: 370209 Color: 2

Bin 340: 276 of cap free
Amount of items: 2
Items: 
Size: 726196 Color: 4
Size: 273529 Color: 2

Bin 341: 277 of cap free
Amount of items: 2
Items: 
Size: 666535 Color: 3
Size: 333189 Color: 4

Bin 342: 278 of cap free
Amount of items: 2
Items: 
Size: 732514 Color: 3
Size: 267209 Color: 2

Bin 343: 279 of cap free
Amount of items: 2
Items: 
Size: 668846 Color: 3
Size: 330876 Color: 0

Bin 344: 280 of cap free
Amount of items: 2
Items: 
Size: 614656 Color: 3
Size: 385065 Color: 1

Bin 345: 281 of cap free
Amount of items: 2
Items: 
Size: 642844 Color: 1
Size: 356876 Color: 2

Bin 346: 281 of cap free
Amount of items: 2
Items: 
Size: 748941 Color: 2
Size: 250779 Color: 1

Bin 347: 282 of cap free
Amount of items: 6
Items: 
Size: 176293 Color: 0
Size: 176140 Color: 4
Size: 175482 Color: 3
Size: 175162 Color: 0
Size: 174544 Color: 3
Size: 122098 Color: 0

Bin 348: 283 of cap free
Amount of items: 2
Items: 
Size: 757129 Color: 4
Size: 242589 Color: 1

Bin 349: 286 of cap free
Amount of items: 2
Items: 
Size: 763004 Color: 0
Size: 236711 Color: 3

Bin 350: 289 of cap free
Amount of items: 2
Items: 
Size: 504189 Color: 1
Size: 495523 Color: 2

Bin 351: 289 of cap free
Amount of items: 2
Items: 
Size: 594847 Color: 0
Size: 404865 Color: 2

Bin 352: 289 of cap free
Amount of items: 2
Items: 
Size: 677473 Color: 4
Size: 322239 Color: 2

Bin 353: 290 of cap free
Amount of items: 2
Items: 
Size: 696933 Color: 1
Size: 302778 Color: 4

Bin 354: 293 of cap free
Amount of items: 2
Items: 
Size: 639518 Color: 1
Size: 360190 Color: 2

Bin 355: 293 of cap free
Amount of items: 2
Items: 
Size: 641900 Color: 4
Size: 357808 Color: 0

Bin 356: 298 of cap free
Amount of items: 3
Items: 
Size: 685825 Color: 1
Size: 194226 Color: 4
Size: 119652 Color: 1

Bin 357: 301 of cap free
Amount of items: 2
Items: 
Size: 650944 Color: 2
Size: 348756 Color: 0

Bin 358: 303 of cap free
Amount of items: 2
Items: 
Size: 781521 Color: 1
Size: 218177 Color: 2

Bin 359: 306 of cap free
Amount of items: 2
Items: 
Size: 613861 Color: 1
Size: 385834 Color: 2

Bin 360: 306 of cap free
Amount of items: 2
Items: 
Size: 622340 Color: 3
Size: 377355 Color: 2

Bin 361: 307 of cap free
Amount of items: 2
Items: 
Size: 516858 Color: 2
Size: 482836 Color: 0

Bin 362: 307 of cap free
Amount of items: 2
Items: 
Size: 542126 Color: 2
Size: 457568 Color: 4

Bin 363: 307 of cap free
Amount of items: 2
Items: 
Size: 545387 Color: 4
Size: 454307 Color: 2

Bin 364: 307 of cap free
Amount of items: 2
Items: 
Size: 605687 Color: 0
Size: 394007 Color: 2

Bin 365: 308 of cap free
Amount of items: 2
Items: 
Size: 546800 Color: 4
Size: 452893 Color: 3

Bin 366: 309 of cap free
Amount of items: 2
Items: 
Size: 626317 Color: 1
Size: 373375 Color: 2

Bin 367: 309 of cap free
Amount of items: 2
Items: 
Size: 773289 Color: 2
Size: 226403 Color: 0

Bin 368: 310 of cap free
Amount of items: 2
Items: 
Size: 784467 Color: 2
Size: 215224 Color: 0

Bin 369: 313 of cap free
Amount of items: 2
Items: 
Size: 602557 Color: 3
Size: 397131 Color: 1

Bin 370: 314 of cap free
Amount of items: 2
Items: 
Size: 528744 Color: 1
Size: 470943 Color: 2

Bin 371: 314 of cap free
Amount of items: 2
Items: 
Size: 551910 Color: 3
Size: 447777 Color: 2

Bin 372: 315 of cap free
Amount of items: 2
Items: 
Size: 641785 Color: 2
Size: 357901 Color: 1

Bin 373: 317 of cap free
Amount of items: 2
Items: 
Size: 550332 Color: 4
Size: 449352 Color: 3

Bin 374: 317 of cap free
Amount of items: 2
Items: 
Size: 703641 Color: 2
Size: 296043 Color: 4

Bin 375: 320 of cap free
Amount of items: 2
Items: 
Size: 713506 Color: 3
Size: 286175 Color: 2

Bin 376: 321 of cap free
Amount of items: 2
Items: 
Size: 521764 Color: 0
Size: 477916 Color: 1

Bin 377: 321 of cap free
Amount of items: 3
Items: 
Size: 689843 Color: 4
Size: 196639 Color: 3
Size: 113198 Color: 0

Bin 378: 325 of cap free
Amount of items: 2
Items: 
Size: 533409 Color: 0
Size: 466267 Color: 1

Bin 379: 325 of cap free
Amount of items: 2
Items: 
Size: 609954 Color: 4
Size: 389722 Color: 3

Bin 380: 325 of cap free
Amount of items: 3
Items: 
Size: 658276 Color: 1
Size: 188436 Color: 2
Size: 152964 Color: 3

Bin 381: 327 of cap free
Amount of items: 2
Items: 
Size: 768111 Color: 1
Size: 231563 Color: 2

Bin 382: 329 of cap free
Amount of items: 2
Items: 
Size: 763614 Color: 0
Size: 236058 Color: 4

Bin 383: 329 of cap free
Amount of items: 2
Items: 
Size: 783230 Color: 4
Size: 216442 Color: 0

Bin 384: 331 of cap free
Amount of items: 2
Items: 
Size: 538813 Color: 3
Size: 460857 Color: 2

Bin 385: 332 of cap free
Amount of items: 2
Items: 
Size: 611104 Color: 1
Size: 388565 Color: 4

Bin 386: 332 of cap free
Amount of items: 2
Items: 
Size: 788098 Color: 0
Size: 211571 Color: 1

Bin 387: 336 of cap free
Amount of items: 2
Items: 
Size: 548972 Color: 2
Size: 450693 Color: 0

Bin 388: 337 of cap free
Amount of items: 2
Items: 
Size: 637331 Color: 0
Size: 362333 Color: 3

Bin 389: 338 of cap free
Amount of items: 2
Items: 
Size: 613161 Color: 3
Size: 386502 Color: 4

Bin 390: 339 of cap free
Amount of items: 2
Items: 
Size: 729902 Color: 4
Size: 269760 Color: 3

Bin 391: 340 of cap free
Amount of items: 2
Items: 
Size: 785651 Color: 2
Size: 214010 Color: 3

Bin 392: 344 of cap free
Amount of items: 2
Items: 
Size: 513967 Color: 1
Size: 485690 Color: 4

Bin 393: 346 of cap free
Amount of items: 2
Items: 
Size: 704697 Color: 3
Size: 294958 Color: 4

Bin 394: 347 of cap free
Amount of items: 2
Items: 
Size: 535027 Color: 2
Size: 464627 Color: 3

Bin 395: 351 of cap free
Amount of items: 2
Items: 
Size: 777714 Color: 0
Size: 221936 Color: 2

Bin 396: 352 of cap free
Amount of items: 2
Items: 
Size: 501659 Color: 3
Size: 497990 Color: 1

Bin 397: 352 of cap free
Amount of items: 2
Items: 
Size: 559326 Color: 3
Size: 440323 Color: 1

Bin 398: 352 of cap free
Amount of items: 2
Items: 
Size: 765111 Color: 1
Size: 234538 Color: 4

Bin 399: 354 of cap free
Amount of items: 2
Items: 
Size: 613145 Color: 4
Size: 386502 Color: 0

Bin 400: 355 of cap free
Amount of items: 2
Items: 
Size: 613842 Color: 3
Size: 385804 Color: 0

Bin 401: 362 of cap free
Amount of items: 2
Items: 
Size: 527326 Color: 4
Size: 472313 Color: 3

Bin 402: 365 of cap free
Amount of items: 2
Items: 
Size: 547458 Color: 2
Size: 452178 Color: 4

Bin 403: 366 of cap free
Amount of items: 2
Items: 
Size: 732247 Color: 2
Size: 267388 Color: 3

Bin 404: 374 of cap free
Amount of items: 2
Items: 
Size: 681251 Color: 1
Size: 318376 Color: 0

Bin 405: 374 of cap free
Amount of items: 2
Items: 
Size: 716504 Color: 1
Size: 283123 Color: 2

Bin 406: 375 of cap free
Amount of items: 2
Items: 
Size: 522500 Color: 3
Size: 477126 Color: 2

Bin 407: 375 of cap free
Amount of items: 2
Items: 
Size: 775911 Color: 1
Size: 223715 Color: 0

Bin 408: 376 of cap free
Amount of items: 2
Items: 
Size: 665286 Color: 4
Size: 334339 Color: 1

Bin 409: 376 of cap free
Amount of items: 2
Items: 
Size: 748017 Color: 4
Size: 251608 Color: 2

Bin 410: 377 of cap free
Amount of items: 2
Items: 
Size: 771331 Color: 2
Size: 228293 Color: 3

Bin 411: 382 of cap free
Amount of items: 2
Items: 
Size: 634296 Color: 3
Size: 365323 Color: 0

Bin 412: 384 of cap free
Amount of items: 2
Items: 
Size: 501037 Color: 2
Size: 498580 Color: 3

Bin 413: 387 of cap free
Amount of items: 2
Items: 
Size: 667137 Color: 1
Size: 332477 Color: 4

Bin 414: 392 of cap free
Amount of items: 2
Items: 
Size: 715826 Color: 0
Size: 283783 Color: 1

Bin 415: 395 of cap free
Amount of items: 2
Items: 
Size: 535610 Color: 4
Size: 463996 Color: 2

Bin 416: 397 of cap free
Amount of items: 2
Items: 
Size: 786826 Color: 1
Size: 212778 Color: 3

Bin 417: 400 of cap free
Amount of items: 3
Items: 
Size: 658394 Color: 2
Size: 188303 Color: 3
Size: 152904 Color: 1

Bin 418: 401 of cap free
Amount of items: 2
Items: 
Size: 753092 Color: 1
Size: 246508 Color: 3

Bin 419: 401 of cap free
Amount of items: 2
Items: 
Size: 770588 Color: 0
Size: 229012 Color: 1

Bin 420: 401 of cap free
Amount of items: 2
Items: 
Size: 796879 Color: 4
Size: 202721 Color: 2

Bin 421: 404 of cap free
Amount of items: 2
Items: 
Size: 745817 Color: 0
Size: 253780 Color: 3

Bin 422: 407 of cap free
Amount of items: 2
Items: 
Size: 582730 Color: 3
Size: 416864 Color: 2

Bin 423: 414 of cap free
Amount of items: 2
Items: 
Size: 540717 Color: 1
Size: 458870 Color: 2

Bin 424: 414 of cap free
Amount of items: 2
Items: 
Size: 597652 Color: 0
Size: 401935 Color: 4

Bin 425: 415 of cap free
Amount of items: 3
Items: 
Size: 661661 Color: 1
Size: 190416 Color: 2
Size: 147509 Color: 1

Bin 426: 421 of cap free
Amount of items: 2
Items: 
Size: 733999 Color: 4
Size: 265581 Color: 3

Bin 427: 423 of cap free
Amount of items: 2
Items: 
Size: 699006 Color: 1
Size: 300572 Color: 4

Bin 428: 432 of cap free
Amount of items: 2
Items: 
Size: 571739 Color: 1
Size: 427830 Color: 4

Bin 429: 433 of cap free
Amount of items: 2
Items: 
Size: 586746 Color: 2
Size: 412822 Color: 3

Bin 430: 434 of cap free
Amount of items: 2
Items: 
Size: 543187 Color: 3
Size: 456380 Color: 1

Bin 431: 438 of cap free
Amount of items: 2
Items: 
Size: 558791 Color: 2
Size: 440772 Color: 1

Bin 432: 440 of cap free
Amount of items: 2
Items: 
Size: 524802 Color: 1
Size: 474759 Color: 2

Bin 433: 443 of cap free
Amount of items: 2
Items: 
Size: 520825 Color: 2
Size: 478733 Color: 0

Bin 434: 444 of cap free
Amount of items: 2
Items: 
Size: 754973 Color: 1
Size: 244584 Color: 4

Bin 435: 444 of cap free
Amount of items: 2
Items: 
Size: 757403 Color: 2
Size: 242154 Color: 4

Bin 436: 447 of cap free
Amount of items: 2
Items: 
Size: 789983 Color: 3
Size: 209571 Color: 1

Bin 437: 452 of cap free
Amount of items: 2
Items: 
Size: 574968 Color: 2
Size: 424581 Color: 3

Bin 438: 456 of cap free
Amount of items: 2
Items: 
Size: 587922 Color: 0
Size: 411623 Color: 4

Bin 439: 456 of cap free
Amount of items: 2
Items: 
Size: 693621 Color: 3
Size: 305924 Color: 0

Bin 440: 465 of cap free
Amount of items: 2
Items: 
Size: 681199 Color: 1
Size: 318337 Color: 3

Bin 441: 466 of cap free
Amount of items: 2
Items: 
Size: 725199 Color: 4
Size: 274336 Color: 2

Bin 442: 467 of cap free
Amount of items: 2
Items: 
Size: 695482 Color: 2
Size: 304052 Color: 3

Bin 443: 472 of cap free
Amount of items: 2
Items: 
Size: 723137 Color: 0
Size: 276392 Color: 4

Bin 444: 473 of cap free
Amount of items: 2
Items: 
Size: 799745 Color: 1
Size: 199783 Color: 4

Bin 445: 476 of cap free
Amount of items: 2
Items: 
Size: 750634 Color: 3
Size: 248891 Color: 1

Bin 446: 479 of cap free
Amount of items: 2
Items: 
Size: 591665 Color: 2
Size: 407857 Color: 1

Bin 447: 482 of cap free
Amount of items: 2
Items: 
Size: 706941 Color: 1
Size: 292578 Color: 3

Bin 448: 483 of cap free
Amount of items: 2
Items: 
Size: 563332 Color: 2
Size: 436186 Color: 4

Bin 449: 484 of cap free
Amount of items: 2
Items: 
Size: 754261 Color: 1
Size: 245256 Color: 4

Bin 450: 486 of cap free
Amount of items: 2
Items: 
Size: 709959 Color: 1
Size: 289556 Color: 0

Bin 451: 486 of cap free
Amount of items: 2
Items: 
Size: 780164 Color: 2
Size: 219351 Color: 0

Bin 452: 487 of cap free
Amount of items: 2
Items: 
Size: 743711 Color: 3
Size: 255803 Color: 2

Bin 453: 490 of cap free
Amount of items: 2
Items: 
Size: 622776 Color: 0
Size: 376735 Color: 1

Bin 454: 497 of cap free
Amount of items: 2
Items: 
Size: 697430 Color: 0
Size: 302074 Color: 2

Bin 455: 499 of cap free
Amount of items: 2
Items: 
Size: 574211 Color: 3
Size: 425291 Color: 4

Bin 456: 500 of cap free
Amount of items: 2
Items: 
Size: 614472 Color: 3
Size: 385029 Color: 2

Bin 457: 507 of cap free
Amount of items: 2
Items: 
Size: 635543 Color: 3
Size: 363951 Color: 1

Bin 458: 507 of cap free
Amount of items: 2
Items: 
Size: 644496 Color: 2
Size: 354998 Color: 4

Bin 459: 516 of cap free
Amount of items: 2
Items: 
Size: 704175 Color: 3
Size: 295310 Color: 1

Bin 460: 518 of cap free
Amount of items: 2
Items: 
Size: 528560 Color: 2
Size: 470923 Color: 3

Bin 461: 518 of cap free
Amount of items: 2
Items: 
Size: 585977 Color: 3
Size: 413506 Color: 2

Bin 462: 518 of cap free
Amount of items: 2
Items: 
Size: 773065 Color: 3
Size: 226418 Color: 1

Bin 463: 519 of cap free
Amount of items: 2
Items: 
Size: 615610 Color: 1
Size: 383872 Color: 4

Bin 464: 521 of cap free
Amount of items: 2
Items: 
Size: 659972 Color: 1
Size: 339508 Color: 3

Bin 465: 522 of cap free
Amount of items: 3
Items: 
Size: 688912 Color: 2
Size: 195928 Color: 3
Size: 114639 Color: 2

Bin 466: 522 of cap free
Amount of items: 2
Items: 
Size: 704812 Color: 0
Size: 294667 Color: 2

Bin 467: 523 of cap free
Amount of items: 2
Items: 
Size: 727942 Color: 4
Size: 271536 Color: 3

Bin 468: 529 of cap free
Amount of items: 2
Items: 
Size: 655309 Color: 3
Size: 344163 Color: 4

Bin 469: 532 of cap free
Amount of items: 2
Items: 
Size: 566455 Color: 1
Size: 433014 Color: 4

Bin 470: 533 of cap free
Amount of items: 2
Items: 
Size: 500258 Color: 0
Size: 499210 Color: 2

Bin 471: 533 of cap free
Amount of items: 2
Items: 
Size: 562079 Color: 2
Size: 437389 Color: 3

Bin 472: 537 of cap free
Amount of items: 3
Items: 
Size: 683972 Color: 4
Size: 192717 Color: 1
Size: 122775 Color: 1

Bin 473: 543 of cap free
Amount of items: 2
Items: 
Size: 518570 Color: 2
Size: 480888 Color: 4

Bin 474: 543 of cap free
Amount of items: 2
Items: 
Size: 744716 Color: 2
Size: 254742 Color: 4

Bin 475: 548 of cap free
Amount of items: 2
Items: 
Size: 602479 Color: 2
Size: 396974 Color: 3

Bin 476: 556 of cap free
Amount of items: 2
Items: 
Size: 647577 Color: 1
Size: 351868 Color: 0

Bin 477: 560 of cap free
Amount of items: 2
Items: 
Size: 717089 Color: 0
Size: 282352 Color: 2

Bin 478: 568 of cap free
Amount of items: 2
Items: 
Size: 760984 Color: 2
Size: 238449 Color: 4

Bin 479: 574 of cap free
Amount of items: 2
Items: 
Size: 694267 Color: 3
Size: 305160 Color: 0

Bin 480: 575 of cap free
Amount of items: 2
Items: 
Size: 712609 Color: 2
Size: 286817 Color: 0

Bin 481: 578 of cap free
Amount of items: 2
Items: 
Size: 748766 Color: 2
Size: 250657 Color: 1

Bin 482: 580 of cap free
Amount of items: 2
Items: 
Size: 545908 Color: 1
Size: 453513 Color: 3

Bin 483: 583 of cap free
Amount of items: 2
Items: 
Size: 516675 Color: 3
Size: 482743 Color: 1

Bin 484: 584 of cap free
Amount of items: 2
Items: 
Size: 646633 Color: 1
Size: 352784 Color: 4

Bin 485: 585 of cap free
Amount of items: 2
Items: 
Size: 607211 Color: 3
Size: 392205 Color: 0

Bin 486: 586 of cap free
Amount of items: 2
Items: 
Size: 696581 Color: 4
Size: 302834 Color: 1

Bin 487: 589 of cap free
Amount of items: 2
Items: 
Size: 567025 Color: 4
Size: 432387 Color: 3

Bin 488: 590 of cap free
Amount of items: 2
Items: 
Size: 730757 Color: 4
Size: 268654 Color: 1

Bin 489: 597 of cap free
Amount of items: 2
Items: 
Size: 538689 Color: 4
Size: 460715 Color: 0

Bin 490: 598 of cap free
Amount of items: 2
Items: 
Size: 595892 Color: 3
Size: 403511 Color: 1

Bin 491: 598 of cap free
Amount of items: 2
Items: 
Size: 646626 Color: 3
Size: 352777 Color: 2

Bin 492: 601 of cap free
Amount of items: 2
Items: 
Size: 556353 Color: 0
Size: 443047 Color: 3

Bin 493: 604 of cap free
Amount of items: 2
Items: 
Size: 583897 Color: 2
Size: 415500 Color: 0

Bin 494: 614 of cap free
Amount of items: 2
Items: 
Size: 616402 Color: 4
Size: 382985 Color: 3

Bin 495: 614 of cap free
Amount of items: 2
Items: 
Size: 756096 Color: 3
Size: 243291 Color: 0

Bin 496: 618 of cap free
Amount of items: 3
Items: 
Size: 690494 Color: 2
Size: 197031 Color: 1
Size: 111858 Color: 2

Bin 497: 623 of cap free
Amount of items: 2
Items: 
Size: 514114 Color: 4
Size: 485264 Color: 1

Bin 498: 626 of cap free
Amount of items: 2
Items: 
Size: 632996 Color: 0
Size: 366379 Color: 3

Bin 499: 629 of cap free
Amount of items: 2
Items: 
Size: 604367 Color: 3
Size: 395005 Color: 2

Bin 500: 630 of cap free
Amount of items: 2
Items: 
Size: 693523 Color: 1
Size: 305848 Color: 4

Bin 501: 636 of cap free
Amount of items: 2
Items: 
Size: 545887 Color: 1
Size: 453478 Color: 4

Bin 502: 639 of cap free
Amount of items: 2
Items: 
Size: 636323 Color: 2
Size: 363039 Color: 3

Bin 503: 645 of cap free
Amount of items: 2
Items: 
Size: 537278 Color: 2
Size: 462078 Color: 0

Bin 504: 647 of cap free
Amount of items: 2
Items: 
Size: 670601 Color: 1
Size: 328753 Color: 4

Bin 505: 658 of cap free
Amount of items: 2
Items: 
Size: 593833 Color: 2
Size: 405510 Color: 1

Bin 506: 662 of cap free
Amount of items: 2
Items: 
Size: 740515 Color: 3
Size: 258824 Color: 0

Bin 507: 663 of cap free
Amount of items: 2
Items: 
Size: 742856 Color: 4
Size: 256482 Color: 2

Bin 508: 664 of cap free
Amount of items: 2
Items: 
Size: 628679 Color: 4
Size: 370658 Color: 3

Bin 509: 667 of cap free
Amount of items: 2
Items: 
Size: 722505 Color: 4
Size: 276829 Color: 0

Bin 510: 673 of cap free
Amount of items: 2
Items: 
Size: 747078 Color: 2
Size: 252250 Color: 3

Bin 511: 674 of cap free
Amount of items: 2
Items: 
Size: 798707 Color: 4
Size: 200620 Color: 1

Bin 512: 677 of cap free
Amount of items: 2
Items: 
Size: 784105 Color: 4
Size: 215219 Color: 2

Bin 513: 679 of cap free
Amount of items: 2
Items: 
Size: 607207 Color: 3
Size: 392115 Color: 4

Bin 514: 688 of cap free
Amount of items: 2
Items: 
Size: 521465 Color: 2
Size: 477848 Color: 3

Bin 515: 689 of cap free
Amount of items: 2
Items: 
Size: 511634 Color: 1
Size: 487678 Color: 0

Bin 516: 689 of cap free
Amount of items: 2
Items: 
Size: 662605 Color: 4
Size: 336707 Color: 2

Bin 517: 690 of cap free
Amount of items: 2
Items: 
Size: 512739 Color: 3
Size: 486572 Color: 4

Bin 518: 690 of cap free
Amount of items: 2
Items: 
Size: 785363 Color: 4
Size: 213948 Color: 1

Bin 519: 711 of cap free
Amount of items: 2
Items: 
Size: 500832 Color: 2
Size: 498458 Color: 4

Bin 520: 721 of cap free
Amount of items: 2
Items: 
Size: 762073 Color: 4
Size: 237207 Color: 0

Bin 521: 723 of cap free
Amount of items: 3
Items: 
Size: 667994 Color: 4
Size: 191257 Color: 1
Size: 140027 Color: 3

Bin 522: 732 of cap free
Amount of items: 2
Items: 
Size: 696490 Color: 3
Size: 302779 Color: 1

Bin 523: 733 of cap free
Amount of items: 2
Items: 
Size: 590743 Color: 0
Size: 408525 Color: 3

Bin 524: 741 of cap free
Amount of items: 2
Items: 
Size: 724039 Color: 0
Size: 275221 Color: 4

Bin 525: 744 of cap free
Amount of items: 2
Items: 
Size: 714306 Color: 3
Size: 284951 Color: 4

Bin 526: 753 of cap free
Amount of items: 2
Items: 
Size: 626056 Color: 0
Size: 373192 Color: 4

Bin 527: 753 of cap free
Amount of items: 2
Items: 
Size: 779940 Color: 4
Size: 219308 Color: 0

Bin 528: 756 of cap free
Amount of items: 3
Items: 
Size: 685705 Color: 1
Size: 193900 Color: 3
Size: 119640 Color: 0

Bin 529: 759 of cap free
Amount of items: 9
Items: 
Size: 114393 Color: 1
Size: 113114 Color: 4
Size: 113000 Color: 3
Size: 112555 Color: 4
Size: 111644 Color: 3
Size: 111536 Color: 2
Size: 111250 Color: 0
Size: 110892 Color: 4
Size: 100858 Color: 2

Bin 530: 767 of cap free
Amount of items: 2
Items: 
Size: 755997 Color: 4
Size: 243237 Color: 3

Bin 531: 770 of cap free
Amount of items: 2
Items: 
Size: 505185 Color: 4
Size: 494046 Color: 3

Bin 532: 771 of cap free
Amount of items: 2
Items: 
Size: 695580 Color: 0
Size: 303650 Color: 4

Bin 533: 772 of cap free
Amount of items: 2
Items: 
Size: 775780 Color: 0
Size: 223449 Color: 4

Bin 534: 777 of cap free
Amount of items: 2
Items: 
Size: 631698 Color: 4
Size: 367526 Color: 1

Bin 535: 779 of cap free
Amount of items: 2
Items: 
Size: 724150 Color: 4
Size: 275072 Color: 0

Bin 536: 780 of cap free
Amount of items: 2
Items: 
Size: 790780 Color: 1
Size: 208441 Color: 3

Bin 537: 786 of cap free
Amount of items: 2
Items: 
Size: 530440 Color: 4
Size: 468775 Color: 0

Bin 538: 787 of cap free
Amount of items: 2
Items: 
Size: 559752 Color: 3
Size: 439462 Color: 0

Bin 539: 789 of cap free
Amount of items: 2
Items: 
Size: 578545 Color: 3
Size: 420667 Color: 2

Bin 540: 791 of cap free
Amount of items: 2
Items: 
Size: 585092 Color: 4
Size: 414118 Color: 3

Bin 541: 792 of cap free
Amount of items: 2
Items: 
Size: 507744 Color: 4
Size: 491465 Color: 3

Bin 542: 793 of cap free
Amount of items: 2
Items: 
Size: 577132 Color: 3
Size: 422076 Color: 1

Bin 543: 801 of cap free
Amount of items: 2
Items: 
Size: 578236 Color: 0
Size: 420964 Color: 3

Bin 544: 801 of cap free
Amount of items: 2
Items: 
Size: 678012 Color: 0
Size: 321188 Color: 2

Bin 545: 804 of cap free
Amount of items: 2
Items: 
Size: 757293 Color: 3
Size: 241904 Color: 2

Bin 546: 805 of cap free
Amount of items: 2
Items: 
Size: 651801 Color: 0
Size: 347395 Color: 2

Bin 547: 806 of cap free
Amount of items: 2
Items: 
Size: 579366 Color: 2
Size: 419829 Color: 3

Bin 548: 809 of cap free
Amount of items: 2
Items: 
Size: 582400 Color: 2
Size: 416792 Color: 3

Bin 549: 809 of cap free
Amount of items: 2
Items: 
Size: 670618 Color: 4
Size: 328574 Color: 2

Bin 550: 811 of cap free
Amount of items: 2
Items: 
Size: 552852 Color: 0
Size: 446338 Color: 3

Bin 551: 811 of cap free
Amount of items: 3
Items: 
Size: 662287 Color: 3
Size: 190677 Color: 2
Size: 146226 Color: 1

Bin 552: 818 of cap free
Amount of items: 2
Items: 
Size: 652332 Color: 2
Size: 346851 Color: 0

Bin 553: 819 of cap free
Amount of items: 2
Items: 
Size: 554381 Color: 3
Size: 444801 Color: 0

Bin 554: 828 of cap free
Amount of items: 2
Items: 
Size: 519263 Color: 1
Size: 479910 Color: 2

Bin 555: 828 of cap free
Amount of items: 2
Items: 
Size: 726630 Color: 0
Size: 272543 Color: 1

Bin 556: 829 of cap free
Amount of items: 2
Items: 
Size: 700006 Color: 0
Size: 299166 Color: 1

Bin 557: 832 of cap free
Amount of items: 2
Items: 
Size: 588005 Color: 4
Size: 411164 Color: 2

Bin 558: 833 of cap free
Amount of items: 2
Items: 
Size: 681192 Color: 1
Size: 317976 Color: 4

Bin 559: 833 of cap free
Amount of items: 2
Items: 
Size: 739665 Color: 3
Size: 259503 Color: 2

Bin 560: 837 of cap free
Amount of items: 2
Items: 
Size: 527260 Color: 3
Size: 471904 Color: 0

Bin 561: 845 of cap free
Amount of items: 2
Items: 
Size: 541216 Color: 1
Size: 457940 Color: 3

Bin 562: 865 of cap free
Amount of items: 2
Items: 
Size: 505551 Color: 3
Size: 493585 Color: 2

Bin 563: 878 of cap free
Amount of items: 2
Items: 
Size: 521362 Color: 4
Size: 477761 Color: 3

Bin 564: 880 of cap free
Amount of items: 2
Items: 
Size: 650589 Color: 4
Size: 348532 Color: 1

Bin 565: 888 of cap free
Amount of items: 2
Items: 
Size: 551523 Color: 0
Size: 447590 Color: 2

Bin 566: 891 of cap free
Amount of items: 2
Items: 
Size: 578525 Color: 3
Size: 420585 Color: 2

Bin 567: 892 of cap free
Amount of items: 2
Items: 
Size: 630651 Color: 2
Size: 368458 Color: 0

Bin 568: 895 of cap free
Amount of items: 2
Items: 
Size: 716514 Color: 2
Size: 282592 Color: 4

Bin 569: 898 of cap free
Amount of items: 2
Items: 
Size: 500827 Color: 2
Size: 498276 Color: 4

Bin 570: 905 of cap free
Amount of items: 2
Items: 
Size: 796536 Color: 2
Size: 202560 Color: 1

Bin 571: 908 of cap free
Amount of items: 2
Items: 
Size: 653526 Color: 3
Size: 345567 Color: 4

Bin 572: 909 of cap free
Amount of items: 2
Items: 
Size: 545701 Color: 4
Size: 453391 Color: 2

Bin 573: 909 of cap free
Amount of items: 2
Items: 
Size: 662942 Color: 2
Size: 336150 Color: 4

Bin 574: 912 of cap free
Amount of items: 2
Items: 
Size: 686969 Color: 4
Size: 312120 Color: 1

Bin 575: 913 of cap free
Amount of items: 2
Items: 
Size: 511603 Color: 4
Size: 487485 Color: 2

Bin 576: 915 of cap free
Amount of items: 2
Items: 
Size: 624243 Color: 0
Size: 374843 Color: 3

Bin 577: 918 of cap free
Amount of items: 2
Items: 
Size: 712545 Color: 1
Size: 286538 Color: 2

Bin 578: 919 of cap free
Amount of items: 2
Items: 
Size: 712438 Color: 2
Size: 286644 Color: 0

Bin 579: 925 of cap free
Amount of items: 2
Items: 
Size: 750227 Color: 1
Size: 248849 Color: 4

Bin 580: 932 of cap free
Amount of items: 2
Items: 
Size: 642479 Color: 4
Size: 356590 Color: 2

Bin 581: 940 of cap free
Amount of items: 2
Items: 
Size: 765959 Color: 1
Size: 233102 Color: 0

Bin 582: 946 of cap free
Amount of items: 3
Items: 
Size: 688768 Color: 4
Size: 195793 Color: 0
Size: 114494 Color: 1

Bin 583: 947 of cap free
Amount of items: 2
Items: 
Size: 752129 Color: 2
Size: 246925 Color: 0

Bin 584: 953 of cap free
Amount of items: 2
Items: 
Size: 796492 Color: 3
Size: 202556 Color: 2

Bin 585: 960 of cap free
Amount of items: 2
Items: 
Size: 677830 Color: 2
Size: 321211 Color: 1

Bin 586: 974 of cap free
Amount of items: 2
Items: 
Size: 604222 Color: 4
Size: 394805 Color: 2

Bin 587: 997 of cap free
Amount of items: 2
Items: 
Size: 567817 Color: 1
Size: 431187 Color: 0

Bin 588: 1000 of cap free
Amount of items: 2
Items: 
Size: 601401 Color: 0
Size: 397600 Color: 1

Bin 589: 1004 of cap free
Amount of items: 2
Items: 
Size: 694432 Color: 4
Size: 304565 Color: 2

Bin 590: 1006 of cap free
Amount of items: 2
Items: 
Size: 632922 Color: 0
Size: 366073 Color: 2

Bin 591: 1015 of cap free
Amount of items: 7
Items: 
Size: 148241 Color: 2
Size: 148107 Color: 0
Size: 148136 Color: 2
Size: 145929 Color: 3
Size: 145768 Color: 2
Size: 144892 Color: 0
Size: 117913 Color: 1

Bin 592: 1021 of cap free
Amount of items: 2
Items: 
Size: 785279 Color: 1
Size: 213701 Color: 4

Bin 593: 1027 of cap free
Amount of items: 2
Items: 
Size: 659659 Color: 2
Size: 339315 Color: 3

Bin 594: 1043 of cap free
Amount of items: 2
Items: 
Size: 638304 Color: 1
Size: 360654 Color: 3

Bin 595: 1045 of cap free
Amount of items: 2
Items: 
Size: 782595 Color: 1
Size: 216361 Color: 0

Bin 596: 1060 of cap free
Amount of items: 2
Items: 
Size: 535419 Color: 0
Size: 463522 Color: 4

Bin 597: 1072 of cap free
Amount of items: 2
Items: 
Size: 701842 Color: 3
Size: 297087 Color: 1

Bin 598: 1075 of cap free
Amount of items: 2
Items: 
Size: 755565 Color: 1
Size: 243361 Color: 4

Bin 599: 1084 of cap free
Amount of items: 2
Items: 
Size: 604258 Color: 2
Size: 394659 Color: 3

Bin 600: 1094 of cap free
Amount of items: 2
Items: 
Size: 537167 Color: 2
Size: 461740 Color: 3

Bin 601: 1103 of cap free
Amount of items: 2
Items: 
Size: 609227 Color: 1
Size: 389671 Color: 3

Bin 602: 1112 of cap free
Amount of items: 2
Items: 
Size: 668011 Color: 2
Size: 330878 Color: 4

Bin 603: 1133 of cap free
Amount of items: 2
Items: 
Size: 573367 Color: 0
Size: 425501 Color: 3

Bin 604: 1134 of cap free
Amount of items: 2
Items: 
Size: 630509 Color: 2
Size: 368358 Color: 0

Bin 605: 1139 of cap free
Amount of items: 2
Items: 
Size: 772499 Color: 3
Size: 226363 Color: 0

Bin 606: 1149 of cap free
Amount of items: 2
Items: 
Size: 519147 Color: 4
Size: 479705 Color: 3

Bin 607: 1165 of cap free
Amount of items: 2
Items: 
Size: 584955 Color: 0
Size: 413881 Color: 3

Bin 608: 1167 of cap free
Amount of items: 2
Items: 
Size: 504975 Color: 2
Size: 493859 Color: 3

Bin 609: 1177 of cap free
Amount of items: 2
Items: 
Size: 765470 Color: 1
Size: 233354 Color: 3

Bin 610: 1182 of cap free
Amount of items: 2
Items: 
Size: 704166 Color: 4
Size: 294653 Color: 0

Bin 611: 1202 of cap free
Amount of items: 2
Items: 
Size: 705638 Color: 0
Size: 293161 Color: 4

Bin 612: 1212 of cap free
Amount of items: 2
Items: 
Size: 509406 Color: 1
Size: 489383 Color: 3

Bin 613: 1217 of cap free
Amount of items: 2
Items: 
Size: 738299 Color: 3
Size: 260485 Color: 2

Bin 614: 1219 of cap free
Amount of items: 2
Items: 
Size: 507441 Color: 2
Size: 491341 Color: 1

Bin 615: 1231 of cap free
Amount of items: 2
Items: 
Size: 625636 Color: 0
Size: 373134 Color: 2

Bin 616: 1233 of cap free
Amount of items: 2
Items: 
Size: 715231 Color: 4
Size: 283537 Color: 2

Bin 617: 1239 of cap free
Amount of items: 2
Items: 
Size: 686396 Color: 0
Size: 312366 Color: 4

Bin 618: 1242 of cap free
Amount of items: 2
Items: 
Size: 567805 Color: 1
Size: 430954 Color: 2

Bin 619: 1254 of cap free
Amount of items: 2
Items: 
Size: 720368 Color: 0
Size: 278379 Color: 3

Bin 620: 1260 of cap free
Amount of items: 6
Items: 
Size: 180674 Color: 0
Size: 179777 Color: 4
Size: 179473 Color: 2
Size: 179434 Color: 0
Size: 178987 Color: 2
Size: 100396 Color: 0

Bin 621: 1264 of cap free
Amount of items: 2
Items: 
Size: 559395 Color: 1
Size: 439342 Color: 0

Bin 622: 1268 of cap free
Amount of items: 2
Items: 
Size: 696037 Color: 4
Size: 302696 Color: 2

Bin 623: 1274 of cap free
Amount of items: 2
Items: 
Size: 738138 Color: 4
Size: 260589 Color: 3

Bin 624: 1280 of cap free
Amount of items: 2
Items: 
Size: 575836 Color: 2
Size: 422885 Color: 3

Bin 625: 1283 of cap free
Amount of items: 2
Items: 
Size: 699718 Color: 2
Size: 299000 Color: 1

Bin 626: 1284 of cap free
Amount of items: 2
Items: 
Size: 709370 Color: 3
Size: 289347 Color: 4

Bin 627: 1293 of cap free
Amount of items: 2
Items: 
Size: 585272 Color: 3
Size: 413436 Color: 1

Bin 628: 1299 of cap free
Amount of items: 2
Items: 
Size: 638665 Color: 0
Size: 360037 Color: 4

Bin 629: 1302 of cap free
Amount of items: 2
Items: 
Size: 745732 Color: 0
Size: 252967 Color: 1

Bin 630: 1303 of cap free
Amount of items: 2
Items: 
Size: 679572 Color: 4
Size: 319126 Color: 0

Bin 631: 1304 of cap free
Amount of items: 2
Items: 
Size: 646041 Color: 0
Size: 352656 Color: 3

Bin 632: 1305 of cap free
Amount of items: 2
Items: 
Size: 634762 Color: 0
Size: 363934 Color: 3

Bin 633: 1310 of cap free
Amount of items: 2
Items: 
Size: 643877 Color: 3
Size: 354814 Color: 0

Bin 634: 1327 of cap free
Amount of items: 2
Items: 
Size: 651792 Color: 1
Size: 346882 Color: 2

Bin 635: 1327 of cap free
Amount of items: 2
Items: 
Size: 655266 Color: 3
Size: 343408 Color: 4

Bin 636: 1328 of cap free
Amount of items: 2
Items: 
Size: 758630 Color: 1
Size: 240043 Color: 0

Bin 637: 1361 of cap free
Amount of items: 2
Items: 
Size: 587704 Color: 1
Size: 410936 Color: 4

Bin 638: 1361 of cap free
Amount of items: 2
Items: 
Size: 771079 Color: 3
Size: 227561 Color: 1

Bin 639: 1383 of cap free
Amount of items: 2
Items: 
Size: 755448 Color: 4
Size: 243170 Color: 2

Bin 640: 1384 of cap free
Amount of items: 2
Items: 
Size: 720353 Color: 2
Size: 278264 Color: 3

Bin 641: 1393 of cap free
Amount of items: 2
Items: 
Size: 532492 Color: 1
Size: 466116 Color: 2

Bin 642: 1415 of cap free
Amount of items: 2
Items: 
Size: 790564 Color: 4
Size: 208022 Color: 3

Bin 643: 1421 of cap free
Amount of items: 2
Items: 
Size: 671670 Color: 0
Size: 326910 Color: 1

Bin 644: 1434 of cap free
Amount of items: 2
Items: 
Size: 749845 Color: 4
Size: 248722 Color: 3

Bin 645: 1438 of cap free
Amount of items: 2
Items: 
Size: 536988 Color: 4
Size: 461575 Color: 3

Bin 646: 1444 of cap free
Amount of items: 2
Items: 
Size: 566298 Color: 0
Size: 432259 Color: 2

Bin 647: 1444 of cap free
Amount of items: 2
Items: 
Size: 791214 Color: 3
Size: 207343 Color: 4

Bin 648: 1450 of cap free
Amount of items: 2
Items: 
Size: 608969 Color: 3
Size: 389582 Color: 1

Bin 649: 1450 of cap free
Amount of items: 2
Items: 
Size: 638543 Color: 3
Size: 360008 Color: 1

Bin 650: 1452 of cap free
Amount of items: 2
Items: 
Size: 595213 Color: 2
Size: 403336 Color: 3

Bin 651: 1456 of cap free
Amount of items: 2
Items: 
Size: 691483 Color: 1
Size: 307062 Color: 0

Bin 652: 1475 of cap free
Amount of items: 2
Items: 
Size: 677419 Color: 1
Size: 321107 Color: 0

Bin 653: 1496 of cap free
Amount of items: 2
Items: 
Size: 516655 Color: 3
Size: 481850 Color: 0

Bin 654: 1506 of cap free
Amount of items: 2
Items: 
Size: 526481 Color: 1
Size: 472014 Color: 3

Bin 655: 1517 of cap free
Amount of items: 2
Items: 
Size: 552789 Color: 0
Size: 445695 Color: 3

Bin 656: 1522 of cap free
Amount of items: 2
Items: 
Size: 581803 Color: 2
Size: 416676 Color: 0

Bin 657: 1541 of cap free
Amount of items: 2
Items: 
Size: 628702 Color: 3
Size: 369758 Color: 4

Bin 658: 1551 of cap free
Amount of items: 2
Items: 
Size: 775513 Color: 1
Size: 222937 Color: 2

Bin 659: 1564 of cap free
Amount of items: 2
Items: 
Size: 593001 Color: 1
Size: 405436 Color: 3

Bin 660: 1585 of cap free
Amount of items: 2
Items: 
Size: 645937 Color: 1
Size: 352479 Color: 3

Bin 661: 1594 of cap free
Amount of items: 2
Items: 
Size: 664110 Color: 4
Size: 334297 Color: 1

Bin 662: 1607 of cap free
Amount of items: 2
Items: 
Size: 545560 Color: 2
Size: 452834 Color: 1

Bin 663: 1627 of cap free
Amount of items: 2
Items: 
Size: 772591 Color: 0
Size: 225783 Color: 4

Bin 664: 1636 of cap free
Amount of items: 2
Items: 
Size: 665227 Color: 0
Size: 333138 Color: 4

Bin 665: 1650 of cap free
Amount of items: 2
Items: 
Size: 524023 Color: 4
Size: 474328 Color: 0

Bin 666: 1669 of cap free
Amount of items: 2
Items: 
Size: 571462 Color: 3
Size: 426870 Color: 2

Bin 667: 1680 of cap free
Amount of items: 2
Items: 
Size: 713878 Color: 2
Size: 284443 Color: 4

Bin 668: 1682 of cap free
Amount of items: 2
Items: 
Size: 701478 Color: 4
Size: 296841 Color: 0

Bin 669: 1709 of cap free
Amount of items: 2
Items: 
Size: 540500 Color: 0
Size: 457792 Color: 2

Bin 670: 1711 of cap free
Amount of items: 2
Items: 
Size: 726063 Color: 4
Size: 272227 Color: 2

Bin 671: 1740 of cap free
Amount of items: 2
Items: 
Size: 603916 Color: 1
Size: 394345 Color: 2

Bin 672: 1746 of cap free
Amount of items: 2
Items: 
Size: 725903 Color: 0
Size: 272352 Color: 4

Bin 673: 1754 of cap free
Amount of items: 2
Items: 
Size: 790376 Color: 1
Size: 207871 Color: 3

Bin 674: 1788 of cap free
Amount of items: 2
Items: 
Size: 719485 Color: 4
Size: 278728 Color: 2

Bin 675: 1811 of cap free
Amount of items: 2
Items: 
Size: 597577 Color: 0
Size: 400613 Color: 4

Bin 676: 1826 of cap free
Amount of items: 2
Items: 
Size: 641619 Color: 0
Size: 356556 Color: 3

Bin 677: 1831 of cap free
Amount of items: 2
Items: 
Size: 526310 Color: 4
Size: 471860 Color: 1

Bin 678: 1853 of cap free
Amount of items: 3
Items: 
Size: 451065 Color: 0
Size: 442463 Color: 4
Size: 104620 Color: 2

Bin 679: 1853 of cap free
Amount of items: 2
Items: 
Size: 632149 Color: 1
Size: 365999 Color: 4

Bin 680: 1867 of cap free
Amount of items: 2
Items: 
Size: 562070 Color: 1
Size: 436064 Color: 0

Bin 681: 1877 of cap free
Amount of items: 2
Items: 
Size: 681091 Color: 1
Size: 317033 Color: 2

Bin 682: 1884 of cap free
Amount of items: 2
Items: 
Size: 731672 Color: 0
Size: 266445 Color: 3

Bin 683: 1888 of cap free
Amount of items: 2
Items: 
Size: 795618 Color: 1
Size: 202495 Color: 2

Bin 684: 1927 of cap free
Amount of items: 2
Items: 
Size: 554075 Color: 3
Size: 443999 Color: 0

Bin 685: 1927 of cap free
Amount of items: 2
Items: 
Size: 628481 Color: 0
Size: 369593 Color: 4

Bin 686: 1957 of cap free
Amount of items: 2
Items: 
Size: 555548 Color: 0
Size: 442496 Color: 4

Bin 687: 1967 of cap free
Amount of items: 2
Items: 
Size: 646201 Color: 3
Size: 351833 Color: 1

Bin 688: 1977 of cap free
Amount of items: 2
Items: 
Size: 742566 Color: 3
Size: 255458 Color: 2

Bin 689: 1993 of cap free
Amount of items: 3
Items: 
Size: 683546 Color: 4
Size: 192222 Color: 1
Size: 122240 Color: 2

Bin 690: 2004 of cap free
Amount of items: 2
Items: 
Size: 578223 Color: 2
Size: 419774 Color: 3

Bin 691: 2006 of cap free
Amount of items: 2
Items: 
Size: 701404 Color: 4
Size: 296591 Color: 3

Bin 692: 2021 of cap free
Amount of items: 2
Items: 
Size: 506592 Color: 0
Size: 491388 Color: 2

Bin 693: 2035 of cap free
Amount of items: 2
Items: 
Size: 645872 Color: 0
Size: 352094 Color: 3

Bin 694: 2039 of cap free
Amount of items: 2
Items: 
Size: 565627 Color: 4
Size: 432335 Color: 0

Bin 695: 2049 of cap free
Amount of items: 2
Items: 
Size: 701314 Color: 3
Size: 296638 Color: 0

Bin 696: 2064 of cap free
Amount of items: 2
Items: 
Size: 637929 Color: 2
Size: 360008 Color: 0

Bin 697: 2093 of cap free
Amount of items: 3
Items: 
Size: 657521 Color: 1
Size: 187761 Color: 2
Size: 152626 Color: 4

Bin 698: 2113 of cap free
Amount of items: 2
Items: 
Size: 795643 Color: 4
Size: 202245 Color: 0

Bin 699: 2114 of cap free
Amount of items: 3
Items: 
Size: 668019 Color: 4
Size: 191716 Color: 2
Size: 138152 Color: 0

Bin 700: 2118 of cap free
Amount of items: 2
Items: 
Size: 664955 Color: 1
Size: 332928 Color: 2

Bin 701: 2124 of cap free
Amount of items: 2
Items: 
Size: 612622 Color: 1
Size: 385255 Color: 0

Bin 702: 2125 of cap free
Amount of items: 2
Items: 
Size: 578175 Color: 3
Size: 419701 Color: 2

Bin 703: 2134 of cap free
Amount of items: 2
Items: 
Size: 590661 Color: 3
Size: 407206 Color: 4

Bin 704: 2186 of cap free
Amount of items: 3
Items: 
Size: 659558 Color: 3
Size: 189861 Color: 2
Size: 148396 Color: 1

Bin 705: 2212 of cap free
Amount of items: 2
Items: 
Size: 795473 Color: 0
Size: 202316 Color: 2

Bin 706: 2237 of cap free
Amount of items: 2
Items: 
Size: 643487 Color: 3
Size: 354277 Color: 1

Bin 707: 2243 of cap free
Amount of items: 2
Items: 
Size: 772484 Color: 3
Size: 225274 Color: 0

Bin 708: 2266 of cap free
Amount of items: 2
Items: 
Size: 526062 Color: 2
Size: 471673 Color: 3

Bin 709: 2283 of cap free
Amount of items: 2
Items: 
Size: 625192 Color: 0
Size: 372526 Color: 3

Bin 710: 2289 of cap free
Amount of items: 2
Items: 
Size: 720132 Color: 2
Size: 277580 Color: 3

Bin 711: 2313 of cap free
Amount of items: 2
Items: 
Size: 729003 Color: 2
Size: 268685 Color: 4

Bin 712: 2328 of cap free
Amount of items: 2
Items: 
Size: 534272 Color: 4
Size: 463401 Color: 1

Bin 713: 2332 of cap free
Amount of items: 2
Items: 
Size: 641334 Color: 4
Size: 356335 Color: 3

Bin 714: 2365 of cap free
Amount of items: 2
Items: 
Size: 570966 Color: 1
Size: 426670 Color: 2

Bin 715: 2387 of cap free
Amount of items: 2
Items: 
Size: 583813 Color: 4
Size: 413801 Color: 3

Bin 716: 2410 of cap free
Amount of items: 2
Items: 
Size: 625080 Color: 3
Size: 372511 Color: 0

Bin 717: 2453 of cap free
Amount of items: 2
Items: 
Size: 531475 Color: 2
Size: 466073 Color: 4

Bin 718: 2483 of cap free
Amount of items: 2
Items: 
Size: 708706 Color: 0
Size: 288812 Color: 2

Bin 719: 2488 of cap free
Amount of items: 2
Items: 
Size: 534270 Color: 0
Size: 463243 Color: 3

Bin 720: 2509 of cap free
Amount of items: 2
Items: 
Size: 534152 Color: 3
Size: 463340 Color: 1

Bin 721: 2510 of cap free
Amount of items: 2
Items: 
Size: 559211 Color: 1
Size: 438280 Color: 4

Bin 722: 2541 of cap free
Amount of items: 2
Items: 
Size: 539971 Color: 4
Size: 457489 Color: 2

Bin 723: 2585 of cap free
Amount of items: 2
Items: 
Size: 561424 Color: 4
Size: 435992 Color: 3

Bin 724: 2598 of cap free
Amount of items: 2
Items: 
Size: 500773 Color: 2
Size: 496630 Color: 0

Bin 725: 2603 of cap free
Amount of items: 2
Items: 
Size: 730687 Color: 4
Size: 266711 Color: 0

Bin 726: 2651 of cap free
Amount of items: 2
Items: 
Size: 620536 Color: 0
Size: 376814 Color: 2

Bin 727: 2699 of cap free
Amount of items: 2
Items: 
Size: 520399 Color: 4
Size: 476903 Color: 0

Bin 728: 2714 of cap free
Amount of items: 3
Items: 
Size: 657317 Color: 2
Size: 187758 Color: 1
Size: 152212 Color: 2

Bin 729: 2770 of cap free
Amount of items: 2
Items: 
Size: 500076 Color: 0
Size: 497155 Color: 2

Bin 730: 2815 of cap free
Amount of items: 2
Items: 
Size: 676088 Color: 0
Size: 321098 Color: 4

Bin 731: 2908 of cap free
Amount of items: 2
Items: 
Size: 637210 Color: 0
Size: 359883 Color: 2

Bin 732: 2963 of cap free
Amount of items: 2
Items: 
Size: 511037 Color: 4
Size: 486001 Color: 1

Bin 733: 3003 of cap free
Amount of items: 2
Items: 
Size: 705419 Color: 3
Size: 291579 Color: 1

Bin 734: 3016 of cap free
Amount of items: 2
Items: 
Size: 664058 Color: 2
Size: 332927 Color: 0

Bin 735: 3025 of cap free
Amount of items: 2
Items: 
Size: 780885 Color: 1
Size: 216091 Color: 3

Bin 736: 3069 of cap free
Amount of items: 2
Items: 
Size: 499967 Color: 1
Size: 496965 Color: 2

Bin 737: 3085 of cap free
Amount of items: 2
Items: 
Size: 742418 Color: 2
Size: 254498 Color: 0

Bin 738: 3100 of cap free
Amount of items: 2
Items: 
Size: 534018 Color: 2
Size: 462883 Color: 4

Bin 739: 3188 of cap free
Amount of items: 2
Items: 
Size: 669974 Color: 4
Size: 326839 Color: 1

Bin 740: 3367 of cap free
Amount of items: 2
Items: 
Size: 500222 Color: 2
Size: 496412 Color: 3

Bin 741: 3378 of cap free
Amount of items: 2
Items: 
Size: 550088 Color: 1
Size: 446535 Color: 0

Bin 742: 3395 of cap free
Amount of items: 2
Items: 
Size: 586683 Color: 0
Size: 409923 Color: 1

Bin 743: 3396 of cap free
Amount of items: 2
Items: 
Size: 612739 Color: 0
Size: 383866 Color: 3

Bin 744: 3399 of cap free
Amount of items: 2
Items: 
Size: 760928 Color: 4
Size: 235674 Color: 3

Bin 745: 3428 of cap free
Amount of items: 2
Items: 
Size: 669994 Color: 1
Size: 326579 Color: 4

Bin 746: 3429 of cap free
Amount of items: 2
Items: 
Size: 586671 Color: 3
Size: 409901 Color: 4

Bin 747: 3610 of cap free
Amount of items: 2
Items: 
Size: 725071 Color: 2
Size: 271320 Color: 4

Bin 748: 3667 of cap free
Amount of items: 2
Items: 
Size: 719381 Color: 1
Size: 276953 Color: 4

Bin 749: 3672 of cap free
Amount of items: 2
Items: 
Size: 525621 Color: 0
Size: 470708 Color: 3

Bin 750: 3673 of cap free
Amount of items: 2
Items: 
Size: 675411 Color: 1
Size: 320917 Color: 0

Bin 751: 3686 of cap free
Amount of items: 2
Items: 
Size: 675352 Color: 4
Size: 320963 Color: 1

Bin 752: 3718 of cap free
Amount of items: 2
Items: 
Size: 737172 Color: 0
Size: 259111 Color: 3

Bin 753: 3732 of cap free
Amount of items: 2
Items: 
Size: 640509 Color: 4
Size: 355760 Color: 3

Bin 754: 3777 of cap free
Amount of items: 2
Items: 
Size: 725787 Color: 4
Size: 270437 Color: 2

Bin 755: 3824 of cap free
Amount of items: 2
Items: 
Size: 606600 Color: 0
Size: 389577 Color: 2

Bin 756: 3850 of cap free
Amount of items: 2
Items: 
Size: 540224 Color: 2
Size: 455927 Color: 4

Bin 757: 3959 of cap free
Amount of items: 2
Items: 
Size: 624404 Color: 3
Size: 371638 Color: 4

Bin 758: 4114 of cap free
Amount of items: 2
Items: 
Size: 544759 Color: 0
Size: 451128 Color: 3

Bin 759: 4121 of cap free
Amount of items: 2
Items: 
Size: 510653 Color: 4
Size: 485227 Color: 2

Bin 760: 4164 of cap free
Amount of items: 7
Items: 
Size: 143929 Color: 2
Size: 142431 Color: 3
Size: 143033 Color: 1
Size: 142270 Color: 3
Size: 142107 Color: 1
Size: 141204 Color: 3
Size: 140863 Color: 2

Bin 761: 4188 of cap free
Amount of items: 2
Items: 
Size: 510630 Color: 3
Size: 485183 Color: 0

Bin 762: 4193 of cap free
Amount of items: 2
Items: 
Size: 798186 Color: 4
Size: 197622 Color: 3

Bin 763: 4290 of cap free
Amount of items: 2
Items: 
Size: 794441 Color: 0
Size: 201270 Color: 4

Bin 764: 4499 of cap free
Amount of items: 2
Items: 
Size: 741946 Color: 2
Size: 253556 Color: 0

Bin 765: 4545 of cap free
Amount of items: 2
Items: 
Size: 623483 Color: 4
Size: 371973 Color: 3

Bin 766: 4597 of cap free
Amount of items: 2
Items: 
Size: 794294 Color: 3
Size: 201110 Color: 4

Bin 767: 4628 of cap free
Amount of items: 2
Items: 
Size: 623448 Color: 2
Size: 371925 Color: 3

Bin 768: 4645 of cap free
Amount of items: 2
Items: 
Size: 606013 Color: 2
Size: 389343 Color: 0

Bin 769: 4681 of cap free
Amount of items: 2
Items: 
Size: 623725 Color: 3
Size: 371595 Color: 0

Bin 770: 4739 of cap free
Amount of items: 2
Items: 
Size: 674505 Color: 0
Size: 320757 Color: 1

Bin 771: 4802 of cap free
Amount of items: 2
Items: 
Size: 674900 Color: 1
Size: 320299 Color: 4

Bin 772: 4960 of cap free
Amount of items: 2
Items: 
Size: 623346 Color: 1
Size: 371695 Color: 3

Bin 773: 5209 of cap free
Amount of items: 2
Items: 
Size: 585076 Color: 3
Size: 409716 Color: 0

Bin 774: 5265 of cap free
Amount of items: 2
Items: 
Size: 674648 Color: 1
Size: 320088 Color: 4

Bin 775: 5300 of cap free
Amount of items: 2
Items: 
Size: 558743 Color: 3
Size: 435958 Color: 4

Bin 776: 5396 of cap free
Amount of items: 2
Items: 
Size: 524344 Color: 0
Size: 470261 Color: 3

Bin 777: 5525 of cap free
Amount of items: 6
Items: 
Size: 166690 Color: 2
Size: 166215 Color: 4
Size: 165816 Color: 1
Size: 165270 Color: 0
Size: 165316 Color: 1
Size: 165169 Color: 4

Bin 778: 5615 of cap free
Amount of items: 2
Items: 
Size: 509221 Color: 1
Size: 485165 Color: 3

Bin 779: 5857 of cap free
Amount of items: 2
Items: 
Size: 574776 Color: 3
Size: 419368 Color: 1

Bin 780: 5981 of cap free
Amount of items: 2
Items: 
Size: 605932 Color: 2
Size: 388088 Color: 1

Bin 781: 6111 of cap free
Amount of items: 2
Items: 
Size: 558461 Color: 1
Size: 435429 Color: 4

Bin 782: 6182 of cap free
Amount of items: 2
Items: 
Size: 558681 Color: 3
Size: 435138 Color: 2

Bin 783: 6265 of cap free
Amount of items: 2
Items: 
Size: 558658 Color: 4
Size: 435078 Color: 1

Bin 784: 6547 of cap free
Amount of items: 2
Items: 
Size: 674280 Color: 1
Size: 319174 Color: 4

Bin 785: 6799 of cap free
Amount of items: 2
Items: 
Size: 792846 Color: 1
Size: 200356 Color: 4

Bin 786: 6905 of cap free
Amount of items: 2
Items: 
Size: 529761 Color: 2
Size: 463335 Color: 1

Bin 787: 7001 of cap free
Amount of items: 2
Items: 
Size: 603479 Color: 1
Size: 389521 Color: 2

Bin 788: 7096 of cap free
Amount of items: 2
Items: 
Size: 637283 Color: 2
Size: 355622 Color: 3

Bin 789: 7214 of cap free
Amount of items: 2
Items: 
Size: 673816 Color: 1
Size: 318971 Color: 0

Bin 790: 7267 of cap free
Amount of items: 2
Items: 
Size: 749731 Color: 3
Size: 243003 Color: 1

Bin 791: 7423 of cap free
Amount of items: 2
Items: 
Size: 700364 Color: 1
Size: 292214 Color: 3

Bin 792: 7533 of cap free
Amount of items: 2
Items: 
Size: 749522 Color: 2
Size: 242946 Color: 1

Bin 793: 7617 of cap free
Amount of items: 2
Items: 
Size: 496084 Color: 1
Size: 496300 Color: 3

Bin 794: 7890 of cap free
Amount of items: 2
Items: 
Size: 640577 Color: 3
Size: 351534 Color: 4

Bin 795: 8203 of cap free
Amount of items: 9
Items: 
Size: 113335 Color: 1
Size: 111162 Color: 0
Size: 110646 Color: 4
Size: 110285 Color: 2
Size: 109623 Color: 4
Size: 109272 Color: 3
Size: 109356 Color: 4
Size: 109100 Color: 2
Size: 109019 Color: 4

Bin 796: 8295 of cap free
Amount of items: 2
Items: 
Size: 794191 Color: 4
Size: 197515 Color: 0

Bin 797: 8303 of cap free
Amount of items: 2
Items: 
Size: 602494 Color: 3
Size: 389204 Color: 2

Bin 798: 8435 of cap free
Amount of items: 2
Items: 
Size: 581862 Color: 0
Size: 409704 Color: 1

Bin 799: 8483 of cap free
Amount of items: 2
Items: 
Size: 673476 Color: 2
Size: 318042 Color: 1

Bin 800: 8592 of cap free
Amount of items: 2
Items: 
Size: 549348 Color: 1
Size: 442061 Color: 0

Bin 801: 8927 of cap free
Amount of items: 2
Items: 
Size: 749390 Color: 3
Size: 241684 Color: 0

Bin 802: 9097 of cap free
Amount of items: 2
Items: 
Size: 495425 Color: 3
Size: 495479 Color: 2

Bin 803: 9542 of cap free
Amount of items: 2
Items: 
Size: 790912 Color: 3
Size: 199547 Color: 4

Bin 804: 9637 of cap free
Amount of items: 2
Items: 
Size: 495299 Color: 3
Size: 495065 Color: 0

Bin 805: 9674 of cap free
Amount of items: 2
Items: 
Size: 601138 Color: 0
Size: 389189 Color: 2

Bin 806: 9765 of cap free
Amount of items: 2
Items: 
Size: 747883 Color: 3
Size: 242353 Color: 1

Bin 807: 9953 of cap free
Amount of items: 2
Items: 
Size: 621229 Color: 2
Size: 368819 Color: 4

Bin 808: 10328 of cap free
Amount of items: 2
Items: 
Size: 581239 Color: 1
Size: 408434 Color: 3

Bin 809: 11941 of cap free
Amount of items: 2
Items: 
Size: 495036 Color: 4
Size: 493024 Color: 0

Bin 810: 12042 of cap free
Amount of items: 2
Items: 
Size: 699051 Color: 4
Size: 288908 Color: 0

Bin 811: 12258 of cap free
Amount of items: 2
Items: 
Size: 620339 Color: 2
Size: 367404 Color: 1

Bin 812: 12561 of cap free
Amount of items: 2
Items: 
Size: 698590 Color: 3
Size: 288850 Color: 0

Bin 813: 13055 of cap free
Amount of items: 2
Items: 
Size: 620188 Color: 2
Size: 366758 Color: 1

Bin 814: 13364 of cap free
Amount of items: 2
Items: 
Size: 698065 Color: 3
Size: 288572 Color: 0

Bin 815: 13703 of cap free
Amount of items: 2
Items: 
Size: 787923 Color: 2
Size: 198375 Color: 4

Bin 816: 14276 of cap free
Amount of items: 2
Items: 
Size: 669809 Color: 3
Size: 315916 Color: 4

Bin 817: 14599 of cap free
Amount of items: 2
Items: 
Size: 619535 Color: 3
Size: 365867 Color: 1

Bin 818: 15426 of cap free
Amount of items: 2
Items: 
Size: 600845 Color: 4
Size: 383730 Color: 1

Bin 819: 15487 of cap free
Amount of items: 2
Items: 
Size: 696395 Color: 2
Size: 288119 Color: 4

Bin 820: 15559 of cap free
Amount of items: 2
Items: 
Size: 618453 Color: 3
Size: 365989 Color: 4

Bin 821: 16119 of cap free
Amount of items: 2
Items: 
Size: 742312 Color: 0
Size: 241570 Color: 4

Bin 822: 16568 of cap free
Amount of items: 2
Items: 
Size: 618189 Color: 4
Size: 365244 Color: 1

Bin 823: 16821 of cap free
Amount of items: 2
Items: 
Size: 694865 Color: 2
Size: 288315 Color: 0

Bin 824: 16858 of cap free
Amount of items: 2
Items: 
Size: 600297 Color: 4
Size: 382846 Color: 1

Bin 825: 17133 of cap free
Amount of items: 2
Items: 
Size: 618005 Color: 3
Size: 364863 Color: 0

Bin 826: 17279 of cap free
Amount of items: 2
Items: 
Size: 600104 Color: 4
Size: 382618 Color: 0

Bin 827: 17386 of cap free
Amount of items: 6
Items: 
Size: 164471 Color: 3
Size: 164613 Color: 4
Size: 163829 Color: 1
Size: 163321 Color: 4
Size: 163666 Color: 1
Size: 162715 Color: 0

Bin 828: 17517 of cap free
Amount of items: 2
Items: 
Size: 741481 Color: 3
Size: 241003 Color: 1

Bin 829: 17674 of cap free
Amount of items: 2
Items: 
Size: 599943 Color: 4
Size: 382384 Color: 2

Bin 830: 17767 of cap free
Amount of items: 2
Items: 
Size: 694230 Color: 1
Size: 288004 Color: 0

Bin 831: 17902 of cap free
Amount of items: 2
Items: 
Size: 741421 Color: 3
Size: 240678 Color: 1

Bin 832: 18193 of cap free
Amount of items: 2
Items: 
Size: 617926 Color: 4
Size: 363882 Color: 1

Bin 833: 18654 of cap free
Amount of items: 2
Items: 
Size: 617597 Color: 2
Size: 363750 Color: 1

Bin 834: 18810 of cap free
Amount of items: 2
Items: 
Size: 572936 Color: 2
Size: 408255 Color: 3

Bin 835: 19284 of cap free
Amount of items: 2
Items: 
Size: 741084 Color: 0
Size: 239633 Color: 1

Bin 836: 19411 of cap free
Amount of items: 2
Items: 
Size: 617770 Color: 1
Size: 362820 Color: 2

Bin 837: 21794 of cap free
Amount of items: 2
Items: 
Size: 573948 Color: 3
Size: 404259 Color: 2

Bin 838: 21819 of cap free
Amount of items: 2
Items: 
Size: 570947 Color: 1
Size: 407235 Color: 3

Bin 839: 22473 of cap free
Amount of items: 2
Items: 
Size: 693101 Color: 1
Size: 284427 Color: 4

Bin 840: 23897 of cap free
Amount of items: 2
Items: 
Size: 738051 Color: 3
Size: 238053 Color: 4

Bin 841: 24071 of cap free
Amount of items: 2
Items: 
Size: 490978 Color: 3
Size: 484952 Color: 0

Bin 842: 24878 of cap free
Amount of items: 2
Items: 
Size: 691113 Color: 3
Size: 284010 Color: 4

Bin 843: 26523 of cap free
Amount of items: 2
Items: 
Size: 737010 Color: 1
Size: 236468 Color: 4

Bin 844: 28859 of cap free
Amount of items: 2
Items: 
Size: 736184 Color: 3
Size: 234958 Color: 1

Bin 845: 28975 of cap free
Amount of items: 2
Items: 
Size: 735793 Color: 2
Size: 235233 Color: 3

Bin 846: 29308 of cap free
Amount of items: 6
Items: 
Size: 163216 Color: 1
Size: 162486 Color: 4
Size: 162111 Color: 3
Size: 161725 Color: 2
Size: 160610 Color: 3
Size: 160545 Color: 1

Bin 847: 29994 of cap free
Amount of items: 2
Items: 
Size: 735088 Color: 3
Size: 234919 Color: 0

Bin 848: 31945 of cap free
Amount of items: 2
Items: 
Size: 734909 Color: 0
Size: 233147 Color: 3

Bin 849: 32100 of cap free
Amount of items: 2
Items: 
Size: 484208 Color: 0
Size: 483693 Color: 4

Bin 850: 32227 of cap free
Amount of items: 2
Items: 
Size: 734957 Color: 4
Size: 232817 Color: 2

Bin 851: 33341 of cap free
Amount of items: 2
Items: 
Size: 734496 Color: 0
Size: 232164 Color: 2

Bin 852: 33697 of cap free
Amount of items: 2
Items: 
Size: 733787 Color: 3
Size: 232517 Color: 0

Bin 853: 34056 of cap free
Amount of items: 2
Items: 
Size: 733772 Color: 0
Size: 232173 Color: 3

Bin 854: 34934 of cap free
Amount of items: 2
Items: 
Size: 733585 Color: 2
Size: 231482 Color: 0

Bin 855: 38441 of cap free
Amount of items: 2
Items: 
Size: 731391 Color: 0
Size: 230169 Color: 2

Bin 856: 38964 of cap free
Amount of items: 2
Items: 
Size: 729836 Color: 4
Size: 231201 Color: 0

Bin 857: 39773 of cap free
Amount of items: 7
Items: 
Size: 140989 Color: 3
Size: 137563 Color: 2
Size: 136931 Color: 4
Size: 136608 Color: 1
Size: 136079 Color: 1
Size: 136035 Color: 3
Size: 136023 Color: 2

Bin 858: 39840 of cap free
Amount of items: 9
Items: 
Size: 108483 Color: 0
Size: 108356 Color: 4
Size: 108069 Color: 0
Size: 108012 Color: 2
Size: 106977 Color: 4
Size: 106640 Color: 2
Size: 106613 Color: 1
Size: 103737 Color: 4
Size: 103274 Color: 2

Bin 859: 40609 of cap free
Amount of items: 2
Items: 
Size: 728813 Color: 2
Size: 230579 Color: 0

Bin 860: 40783 of cap free
Amount of items: 2
Items: 
Size: 482290 Color: 1
Size: 476928 Color: 2

Bin 861: 40809 of cap free
Amount of items: 6
Items: 
Size: 160463 Color: 0
Size: 160064 Color: 1
Size: 160044 Color: 3
Size: 159565 Color: 4
Size: 159733 Color: 3
Size: 159323 Color: 0

Bin 862: 42128 of cap free
Amount of items: 2
Items: 
Size: 482148 Color: 1
Size: 475725 Color: 4

Bin 863: 49248 of cap free
Amount of items: 8
Items: 
Size: 121870 Color: 1
Size: 121634 Color: 0
Size: 121329 Color: 3
Size: 120611 Color: 0
Size: 118336 Color: 0
Size: 116090 Color: 1
Size: 116508 Color: 3
Size: 114375 Color: 0

Bin 864: 49392 of cap free
Amount of items: 6
Items: 
Size: 159303 Color: 4
Size: 158692 Color: 2
Size: 158691 Color: 3
Size: 158109 Color: 3
Size: 158213 Color: 2
Size: 157601 Color: 0

Bin 865: 50225 of cap free
Amount of items: 2
Items: 
Size: 599837 Color: 2
Size: 349939 Color: 3

Bin 866: 50839 of cap free
Amount of items: 2
Items: 
Size: 597920 Color: 4
Size: 351242 Color: 2

Bin 867: 51452 of cap free
Amount of items: 2
Items: 
Size: 723836 Color: 0
Size: 224713 Color: 1

Bin 868: 52587 of cap free
Amount of items: 2
Items: 
Size: 597547 Color: 2
Size: 349867 Color: 4

Bin 869: 54527 of cap free
Amount of items: 2
Items: 
Size: 597411 Color: 2
Size: 348063 Color: 1

Bin 870: 54619 of cap free
Amount of items: 2
Items: 
Size: 723784 Color: 3
Size: 221598 Color: 4

Bin 871: 54694 of cap free
Amount of items: 2
Items: 
Size: 723758 Color: 3
Size: 221549 Color: 2

Bin 872: 55060 of cap free
Amount of items: 2
Items: 
Size: 476227 Color: 1
Size: 468714 Color: 2

Bin 873: 55820 of cap free
Amount of items: 7
Items: 
Size: 135699 Color: 2
Size: 135636 Color: 1
Size: 135203 Color: 2
Size: 135115 Color: 3
Size: 134381 Color: 4
Size: 134838 Color: 3
Size: 133309 Color: 4

Bin 874: 56094 of cap free
Amount of items: 2
Items: 
Size: 597365 Color: 2
Size: 346542 Color: 1

Bin 875: 58066 of cap free
Amount of items: 2
Items: 
Size: 595095 Color: 2
Size: 346840 Color: 0

Bin 876: 59404 of cap free
Amount of items: 2
Items: 
Size: 719221 Color: 3
Size: 221376 Color: 4

Bin 877: 59723 of cap free
Amount of items: 2
Items: 
Size: 719381 Color: 4
Size: 220897 Color: 3

Bin 878: 59758 of cap free
Amount of items: 2
Items: 
Size: 594766 Color: 2
Size: 345477 Color: 4

Bin 879: 60122 of cap free
Amount of items: 2
Items: 
Size: 719356 Color: 4
Size: 220523 Color: 0

Bin 880: 60171 of cap free
Amount of items: 6
Items: 
Size: 157566 Color: 3
Size: 157468 Color: 2
Size: 156680 Color: 0
Size: 156597 Color: 4
Size: 156489 Color: 3
Size: 155030 Color: 0

Bin 881: 60546 of cap free
Amount of items: 2
Items: 
Size: 718952 Color: 1
Size: 220503 Color: 2

Bin 882: 61080 of cap free
Amount of items: 2
Items: 
Size: 718815 Color: 0
Size: 220106 Color: 3

Bin 883: 63221 of cap free
Amount of items: 2
Items: 
Size: 718681 Color: 2
Size: 218099 Color: 1

Bin 884: 65610 of cap free
Amount of items: 2
Items: 
Size: 718005 Color: 4
Size: 216386 Color: 1

Bin 885: 66746 of cap free
Amount of items: 2
Items: 
Size: 718066 Color: 1
Size: 215189 Color: 4

Bin 886: 69365 of cap free
Amount of items: 2
Items: 
Size: 714272 Color: 4
Size: 216364 Color: 1

Bin 887: 72131 of cap free
Amount of items: 5
Items: 
Size: 186738 Color: 4
Size: 186401 Color: 3
Size: 185214 Color: 4
Size: 185187 Color: 3
Size: 184330 Color: 0

Bin 888: 72948 of cap free
Amount of items: 2
Items: 
Size: 712067 Color: 4
Size: 214986 Color: 0

Bin 889: 75654 of cap free
Amount of items: 7
Items: 
Size: 133602 Color: 2
Size: 132652 Color: 0
Size: 132438 Color: 2
Size: 131630 Color: 1
Size: 131851 Color: 2
Size: 131160 Color: 3
Size: 131014 Color: 0

Bin 890: 80408 of cap free
Amount of items: 5
Items: 
Size: 184281 Color: 3
Size: 183975 Color: 1
Size: 183784 Color: 0
Size: 183821 Color: 1
Size: 183732 Color: 4

Bin 891: 83071 of cap free
Amount of items: 5
Items: 
Size: 183507 Color: 3
Size: 183684 Color: 4
Size: 183401 Color: 3
Size: 183274 Color: 2
Size: 183064 Color: 3

Bin 892: 83970 of cap free
Amount of items: 2
Items: 
Size: 570744 Color: 4
Size: 345287 Color: 3

Bin 893: 84136 of cap free
Amount of items: 2
Items: 
Size: 570629 Color: 3
Size: 345236 Color: 4

Bin 894: 84533 of cap free
Amount of items: 2
Items: 
Size: 570560 Color: 1
Size: 344908 Color: 3

Bin 895: 85467 of cap free
Amount of items: 2
Items: 
Size: 569466 Color: 3
Size: 345068 Color: 4

Bin 896: 85788 of cap free
Amount of items: 2
Items: 
Size: 570037 Color: 4
Size: 344176 Color: 3

Bin 897: 86633 of cap free
Amount of items: 5
Items: 
Size: 183058 Color: 1
Size: 183004 Color: 0
Size: 182583 Color: 3
Size: 182556 Color: 2
Size: 182167 Color: 0

Bin 898: 87881 of cap free
Amount of items: 2
Items: 
Size: 569250 Color: 1
Size: 342870 Color: 3

Bin 899: 88184 of cap free
Amount of items: 2
Items: 
Size: 456064 Color: 2
Size: 455753 Color: 1

Bin 900: 89935 of cap free
Amount of items: 6
Items: 
Size: 152036 Color: 1
Size: 152016 Color: 3
Size: 151996 Color: 4
Size: 151729 Color: 4
Size: 151187 Color: 0
Size: 151102 Color: 3

Bin 901: 91788 of cap free
Amount of items: 5
Items: 
Size: 182078 Color: 4
Size: 181669 Color: 0
Size: 182064 Color: 4
Size: 181472 Color: 1
Size: 180930 Color: 4

Bin 902: 93190 of cap free
Amount of items: 2
Items: 
Size: 693222 Color: 4
Size: 213589 Color: 2

Bin 903: 93338 of cap free
Amount of items: 2
Items: 
Size: 454705 Color: 0
Size: 451958 Color: 4

Bin 904: 94243 of cap free
Amount of items: 2
Items: 
Size: 692961 Color: 4
Size: 212797 Color: 2

Bin 905: 95404 of cap free
Amount of items: 2
Items: 
Size: 454691 Color: 0
Size: 449906 Color: 1

Bin 906: 95605 of cap free
Amount of items: 4
Items: 
Size: 342743 Color: 3
Size: 187464 Color: 4
Size: 187208 Color: 1
Size: 186981 Color: 2

Bin 907: 95813 of cap free
Amount of items: 7
Items: 
Size: 130478 Color: 3
Size: 129996 Color: 0
Size: 129689 Color: 2
Size: 128931 Color: 4
Size: 128733 Color: 3
Size: 128294 Color: 2
Size: 128067 Color: 3

Bin 908: 96204 of cap free
Amount of items: 2
Items: 
Size: 691025 Color: 2
Size: 212772 Color: 0

Bin 909: 96579 of cap free
Amount of items: 2
Items: 
Size: 690796 Color: 0
Size: 212626 Color: 3

Bin 910: 96815 of cap free
Amount of items: 2
Items: 
Size: 690763 Color: 4
Size: 212423 Color: 1

Bin 911: 283341 of cap free
Amount of items: 7
Items: 
Size: 103619 Color: 4
Size: 103083 Color: 0
Size: 103560 Color: 4
Size: 102178 Color: 3
Size: 101835 Color: 1
Size: 101666 Color: 0
Size: 100719 Color: 2

Total size: 905097039
Total free space: 5903872

