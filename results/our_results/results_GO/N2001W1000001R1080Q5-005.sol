Capicity Bin: 1000001
Lower Bound: 875

Bins used: 883
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 550669 Color: 2
Size: 449332 Color: 3

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 703163 Color: 1
Size: 296838 Color: 4

Bin 3: 2 of cap free
Amount of items: 2
Items: 
Size: 607517 Color: 4
Size: 392482 Color: 1

Bin 4: 2 of cap free
Amount of items: 2
Items: 
Size: 663034 Color: 1
Size: 336965 Color: 2

Bin 5: 3 of cap free
Amount of items: 8
Items: 
Size: 127329 Color: 3
Size: 127294 Color: 1
Size: 126481 Color: 3
Size: 125026 Color: 2
Size: 124696 Color: 3
Size: 124670 Color: 4
Size: 124473 Color: 2
Size: 120029 Color: 3

Bin 6: 3 of cap free
Amount of items: 2
Items: 
Size: 629328 Color: 3
Size: 370670 Color: 4

Bin 7: 4 of cap free
Amount of items: 2
Items: 
Size: 509038 Color: 1
Size: 490959 Color: 3

Bin 8: 4 of cap free
Amount of items: 2
Items: 
Size: 551540 Color: 2
Size: 448457 Color: 0

Bin 9: 4 of cap free
Amount of items: 2
Items: 
Size: 676167 Color: 3
Size: 323830 Color: 4

Bin 10: 5 of cap free
Amount of items: 2
Items: 
Size: 790131 Color: 0
Size: 209865 Color: 2

Bin 11: 6 of cap free
Amount of items: 2
Items: 
Size: 514029 Color: 4
Size: 485966 Color: 2

Bin 12: 6 of cap free
Amount of items: 2
Items: 
Size: 670876 Color: 1
Size: 329119 Color: 0

Bin 13: 7 of cap free
Amount of items: 2
Items: 
Size: 585934 Color: 1
Size: 414060 Color: 2

Bin 14: 8 of cap free
Amount of items: 2
Items: 
Size: 556761 Color: 3
Size: 443232 Color: 1

Bin 15: 8 of cap free
Amount of items: 2
Items: 
Size: 562754 Color: 3
Size: 437239 Color: 2

Bin 16: 8 of cap free
Amount of items: 2
Items: 
Size: 643521 Color: 2
Size: 356472 Color: 1

Bin 17: 8 of cap free
Amount of items: 2
Items: 
Size: 662812 Color: 4
Size: 337181 Color: 1

Bin 18: 8 of cap free
Amount of items: 2
Items: 
Size: 683483 Color: 0
Size: 316510 Color: 1

Bin 19: 8 of cap free
Amount of items: 2
Items: 
Size: 700092 Color: 0
Size: 299901 Color: 2

Bin 20: 9 of cap free
Amount of items: 2
Items: 
Size: 688800 Color: 3
Size: 311192 Color: 0

Bin 21: 10 of cap free
Amount of items: 2
Items: 
Size: 550459 Color: 1
Size: 449532 Color: 2

Bin 22: 10 of cap free
Amount of items: 2
Items: 
Size: 720395 Color: 0
Size: 279596 Color: 2

Bin 23: 11 of cap free
Amount of items: 3
Items: 
Size: 367620 Color: 1
Size: 339084 Color: 0
Size: 293286 Color: 4

Bin 24: 11 of cap free
Amount of items: 2
Items: 
Size: 672970 Color: 2
Size: 327020 Color: 0

Bin 25: 12 of cap free
Amount of items: 2
Items: 
Size: 705752 Color: 3
Size: 294237 Color: 4

Bin 26: 13 of cap free
Amount of items: 2
Items: 
Size: 616199 Color: 3
Size: 383789 Color: 1

Bin 27: 15 of cap free
Amount of items: 2
Items: 
Size: 655311 Color: 0
Size: 344675 Color: 1

Bin 28: 16 of cap free
Amount of items: 3
Items: 
Size: 426629 Color: 2
Size: 420941 Color: 3
Size: 152415 Color: 3

Bin 29: 16 of cap free
Amount of items: 2
Items: 
Size: 770542 Color: 2
Size: 229443 Color: 4

Bin 30: 17 of cap free
Amount of items: 2
Items: 
Size: 525389 Color: 2
Size: 474595 Color: 3

Bin 31: 17 of cap free
Amount of items: 2
Items: 
Size: 710499 Color: 3
Size: 289485 Color: 0

Bin 32: 18 of cap free
Amount of items: 5
Items: 
Size: 212930 Color: 2
Size: 206285 Color: 0
Size: 205170 Color: 0
Size: 197334 Color: 2
Size: 178264 Color: 1

Bin 33: 19 of cap free
Amount of items: 2
Items: 
Size: 574448 Color: 1
Size: 425534 Color: 4

Bin 34: 20 of cap free
Amount of items: 2
Items: 
Size: 537543 Color: 4
Size: 462438 Color: 1

Bin 35: 21 of cap free
Amount of items: 2
Items: 
Size: 542473 Color: 1
Size: 457507 Color: 3

Bin 36: 21 of cap free
Amount of items: 2
Items: 
Size: 561480 Color: 3
Size: 438500 Color: 1

Bin 37: 22 of cap free
Amount of items: 2
Items: 
Size: 605297 Color: 3
Size: 394682 Color: 1

Bin 38: 23 of cap free
Amount of items: 2
Items: 
Size: 563728 Color: 1
Size: 436250 Color: 4

Bin 39: 23 of cap free
Amount of items: 2
Items: 
Size: 574547 Color: 4
Size: 425431 Color: 3

Bin 40: 23 of cap free
Amount of items: 2
Items: 
Size: 620086 Color: 2
Size: 379892 Color: 0

Bin 41: 24 of cap free
Amount of items: 2
Items: 
Size: 722641 Color: 3
Size: 277336 Color: 0

Bin 42: 26 of cap free
Amount of items: 2
Items: 
Size: 625467 Color: 3
Size: 374508 Color: 4

Bin 43: 26 of cap free
Amount of items: 2
Items: 
Size: 626474 Color: 0
Size: 373501 Color: 1

Bin 44: 26 of cap free
Amount of items: 2
Items: 
Size: 755642 Color: 1
Size: 244333 Color: 4

Bin 45: 26 of cap free
Amount of items: 2
Items: 
Size: 772878 Color: 3
Size: 227097 Color: 2

Bin 46: 28 of cap free
Amount of items: 2
Items: 
Size: 622547 Color: 2
Size: 377426 Color: 3

Bin 47: 28 of cap free
Amount of items: 2
Items: 
Size: 638722 Color: 2
Size: 361251 Color: 3

Bin 48: 29 of cap free
Amount of items: 2
Items: 
Size: 699807 Color: 4
Size: 300165 Color: 3

Bin 49: 29 of cap free
Amount of items: 2
Items: 
Size: 714664 Color: 3
Size: 285308 Color: 0

Bin 50: 30 of cap free
Amount of items: 6
Items: 
Size: 175077 Color: 3
Size: 174929 Color: 4
Size: 175028 Color: 3
Size: 174398 Color: 4
Size: 173779 Color: 0
Size: 126760 Color: 2

Bin 51: 30 of cap free
Amount of items: 2
Items: 
Size: 671798 Color: 1
Size: 328173 Color: 3

Bin 52: 32 of cap free
Amount of items: 5
Items: 
Size: 246713 Color: 2
Size: 214114 Color: 4
Size: 214140 Color: 2
Size: 212097 Color: 3
Size: 112905 Color: 0

Bin 53: 33 of cap free
Amount of items: 2
Items: 
Size: 551663 Color: 1
Size: 448305 Color: 2

Bin 54: 35 of cap free
Amount of items: 2
Items: 
Size: 586630 Color: 2
Size: 413336 Color: 3

Bin 55: 36 of cap free
Amount of items: 2
Items: 
Size: 620905 Color: 2
Size: 379060 Color: 4

Bin 56: 37 of cap free
Amount of items: 2
Items: 
Size: 680480 Color: 1
Size: 319484 Color: 2

Bin 57: 38 of cap free
Amount of items: 2
Items: 
Size: 573829 Color: 3
Size: 426134 Color: 0

Bin 58: 38 of cap free
Amount of items: 2
Items: 
Size: 661621 Color: 4
Size: 338342 Color: 3

Bin 59: 39 of cap free
Amount of items: 2
Items: 
Size: 681069 Color: 4
Size: 318893 Color: 3

Bin 60: 40 of cap free
Amount of items: 2
Items: 
Size: 549565 Color: 0
Size: 450396 Color: 1

Bin 61: 40 of cap free
Amount of items: 2
Items: 
Size: 577351 Color: 1
Size: 422610 Color: 0

Bin 62: 40 of cap free
Amount of items: 2
Items: 
Size: 602973 Color: 0
Size: 396988 Color: 2

Bin 63: 40 of cap free
Amount of items: 2
Items: 
Size: 639463 Color: 1
Size: 360498 Color: 0

Bin 64: 42 of cap free
Amount of items: 2
Items: 
Size: 747072 Color: 4
Size: 252887 Color: 0

Bin 65: 42 of cap free
Amount of items: 2
Items: 
Size: 795885 Color: 3
Size: 204074 Color: 4

Bin 66: 43 of cap free
Amount of items: 2
Items: 
Size: 618008 Color: 3
Size: 381950 Color: 0

Bin 67: 43 of cap free
Amount of items: 2
Items: 
Size: 707927 Color: 1
Size: 292031 Color: 0

Bin 68: 45 of cap free
Amount of items: 2
Items: 
Size: 629450 Color: 4
Size: 370506 Color: 0

Bin 69: 48 of cap free
Amount of items: 3
Items: 
Size: 447356 Color: 3
Size: 447061 Color: 0
Size: 105536 Color: 3

Bin 70: 48 of cap free
Amount of items: 2
Items: 
Size: 632406 Color: 3
Size: 367547 Color: 2

Bin 71: 48 of cap free
Amount of items: 2
Items: 
Size: 723551 Color: 4
Size: 276402 Color: 1

Bin 72: 48 of cap free
Amount of items: 2
Items: 
Size: 740232 Color: 3
Size: 259721 Color: 2

Bin 73: 49 of cap free
Amount of items: 2
Items: 
Size: 708109 Color: 0
Size: 291843 Color: 1

Bin 74: 50 of cap free
Amount of items: 2
Items: 
Size: 607951 Color: 4
Size: 392000 Color: 1

Bin 75: 52 of cap free
Amount of items: 2
Items: 
Size: 531973 Color: 2
Size: 467976 Color: 4

Bin 76: 52 of cap free
Amount of items: 2
Items: 
Size: 593263 Color: 3
Size: 406686 Color: 4

Bin 77: 52 of cap free
Amount of items: 2
Items: 
Size: 727547 Color: 0
Size: 272402 Color: 2

Bin 78: 52 of cap free
Amount of items: 2
Items: 
Size: 747379 Color: 0
Size: 252570 Color: 4

Bin 79: 53 of cap free
Amount of items: 3
Items: 
Size: 369377 Color: 4
Size: 370370 Color: 0
Size: 260201 Color: 0

Bin 80: 54 of cap free
Amount of items: 2
Items: 
Size: 715083 Color: 0
Size: 284864 Color: 4

Bin 81: 55 of cap free
Amount of items: 2
Items: 
Size: 515399 Color: 4
Size: 484547 Color: 0

Bin 82: 55 of cap free
Amount of items: 2
Items: 
Size: 609248 Color: 2
Size: 390698 Color: 3

Bin 83: 55 of cap free
Amount of items: 2
Items: 
Size: 655562 Color: 2
Size: 344384 Color: 4

Bin 84: 56 of cap free
Amount of items: 2
Items: 
Size: 577049 Color: 0
Size: 422896 Color: 1

Bin 85: 56 of cap free
Amount of items: 2
Items: 
Size: 768677 Color: 3
Size: 231268 Color: 2

Bin 86: 57 of cap free
Amount of items: 2
Items: 
Size: 713472 Color: 2
Size: 286472 Color: 4

Bin 87: 58 of cap free
Amount of items: 2
Items: 
Size: 622084 Color: 1
Size: 377859 Color: 3

Bin 88: 58 of cap free
Amount of items: 2
Items: 
Size: 767664 Color: 4
Size: 232279 Color: 2

Bin 89: 59 of cap free
Amount of items: 2
Items: 
Size: 724478 Color: 2
Size: 275464 Color: 0

Bin 90: 59 of cap free
Amount of items: 2
Items: 
Size: 777790 Color: 4
Size: 222152 Color: 1

Bin 91: 60 of cap free
Amount of items: 2
Items: 
Size: 683065 Color: 3
Size: 316876 Color: 0

Bin 92: 61 of cap free
Amount of items: 2
Items: 
Size: 501890 Color: 2
Size: 498050 Color: 0

Bin 93: 61 of cap free
Amount of items: 2
Items: 
Size: 641648 Color: 2
Size: 358292 Color: 3

Bin 94: 62 of cap free
Amount of items: 2
Items: 
Size: 576968 Color: 4
Size: 422971 Color: 2

Bin 95: 62 of cap free
Amount of items: 2
Items: 
Size: 632255 Color: 3
Size: 367684 Color: 0

Bin 96: 64 of cap free
Amount of items: 2
Items: 
Size: 662414 Color: 3
Size: 337523 Color: 2

Bin 97: 65 of cap free
Amount of items: 2
Items: 
Size: 513678 Color: 0
Size: 486258 Color: 2

Bin 98: 65 of cap free
Amount of items: 2
Items: 
Size: 628210 Color: 4
Size: 371726 Color: 1

Bin 99: 65 of cap free
Amount of items: 2
Items: 
Size: 725172 Color: 2
Size: 274764 Color: 3

Bin 100: 66 of cap free
Amount of items: 2
Items: 
Size: 610096 Color: 1
Size: 389839 Color: 3

Bin 101: 67 of cap free
Amount of items: 2
Items: 
Size: 752211 Color: 4
Size: 247723 Color: 3

Bin 102: 68 of cap free
Amount of items: 2
Items: 
Size: 647782 Color: 1
Size: 352151 Color: 2

Bin 103: 68 of cap free
Amount of items: 2
Items: 
Size: 728742 Color: 4
Size: 271191 Color: 1

Bin 104: 70 of cap free
Amount of items: 2
Items: 
Size: 501060 Color: 3
Size: 498871 Color: 0

Bin 105: 70 of cap free
Amount of items: 2
Items: 
Size: 533308 Color: 2
Size: 466623 Color: 3

Bin 106: 71 of cap free
Amount of items: 2
Items: 
Size: 653691 Color: 1
Size: 346239 Color: 2

Bin 107: 71 of cap free
Amount of items: 2
Items: 
Size: 659942 Color: 4
Size: 339988 Color: 3

Bin 108: 72 of cap free
Amount of items: 2
Items: 
Size: 589231 Color: 2
Size: 410698 Color: 3

Bin 109: 72 of cap free
Amount of items: 2
Items: 
Size: 606278 Color: 3
Size: 393651 Color: 4

Bin 110: 73 of cap free
Amount of items: 2
Items: 
Size: 657969 Color: 2
Size: 341959 Color: 0

Bin 111: 73 of cap free
Amount of items: 2
Items: 
Size: 754250 Color: 1
Size: 245678 Color: 3

Bin 112: 74 of cap free
Amount of items: 2
Items: 
Size: 672550 Color: 3
Size: 327377 Color: 4

Bin 113: 76 of cap free
Amount of items: 2
Items: 
Size: 532979 Color: 0
Size: 466946 Color: 4

Bin 114: 76 of cap free
Amount of items: 2
Items: 
Size: 659264 Color: 2
Size: 340661 Color: 3

Bin 115: 77 of cap free
Amount of items: 3
Items: 
Size: 426849 Color: 2
Size: 426593 Color: 1
Size: 146482 Color: 3

Bin 116: 77 of cap free
Amount of items: 2
Items: 
Size: 723991 Color: 2
Size: 275933 Color: 0

Bin 117: 78 of cap free
Amount of items: 2
Items: 
Size: 501760 Color: 4
Size: 498163 Color: 2

Bin 118: 78 of cap free
Amount of items: 2
Items: 
Size: 636584 Color: 3
Size: 363339 Color: 1

Bin 119: 79 of cap free
Amount of items: 2
Items: 
Size: 700423 Color: 1
Size: 299499 Color: 4

Bin 120: 80 of cap free
Amount of items: 2
Items: 
Size: 656382 Color: 0
Size: 343539 Color: 4

Bin 121: 80 of cap free
Amount of items: 2
Items: 
Size: 774978 Color: 3
Size: 224943 Color: 2

Bin 122: 81 of cap free
Amount of items: 2
Items: 
Size: 734870 Color: 0
Size: 265050 Color: 4

Bin 123: 82 of cap free
Amount of items: 2
Items: 
Size: 509720 Color: 2
Size: 490199 Color: 3

Bin 124: 82 of cap free
Amount of items: 2
Items: 
Size: 560469 Color: 3
Size: 439450 Color: 1

Bin 125: 82 of cap free
Amount of items: 2
Items: 
Size: 648311 Color: 1
Size: 351608 Color: 3

Bin 126: 82 of cap free
Amount of items: 2
Items: 
Size: 755052 Color: 1
Size: 244867 Color: 3

Bin 127: 82 of cap free
Amount of items: 2
Items: 
Size: 768536 Color: 1
Size: 231383 Color: 4

Bin 128: 83 of cap free
Amount of items: 3
Items: 
Size: 373353 Color: 4
Size: 373482 Color: 1
Size: 253083 Color: 3

Bin 129: 83 of cap free
Amount of items: 2
Items: 
Size: 639729 Color: 0
Size: 360189 Color: 3

Bin 130: 84 of cap free
Amount of items: 2
Items: 
Size: 539818 Color: 3
Size: 460099 Color: 4

Bin 131: 84 of cap free
Amount of items: 2
Items: 
Size: 676727 Color: 3
Size: 323190 Color: 2

Bin 132: 85 of cap free
Amount of items: 2
Items: 
Size: 503398 Color: 0
Size: 496518 Color: 2

Bin 133: 85 of cap free
Amount of items: 2
Items: 
Size: 591075 Color: 3
Size: 408841 Color: 4

Bin 134: 85 of cap free
Amount of items: 2
Items: 
Size: 640033 Color: 2
Size: 359883 Color: 3

Bin 135: 86 of cap free
Amount of items: 2
Items: 
Size: 583364 Color: 4
Size: 416551 Color: 2

Bin 136: 87 of cap free
Amount of items: 2
Items: 
Size: 568776 Color: 0
Size: 431138 Color: 3

Bin 137: 87 of cap free
Amount of items: 2
Items: 
Size: 598638 Color: 3
Size: 401276 Color: 4

Bin 138: 87 of cap free
Amount of items: 2
Items: 
Size: 669949 Color: 2
Size: 329965 Color: 4

Bin 139: 89 of cap free
Amount of items: 3
Items: 
Size: 369795 Color: 0
Size: 368326 Color: 4
Size: 261791 Color: 1

Bin 140: 89 of cap free
Amount of items: 2
Items: 
Size: 549190 Color: 3
Size: 450722 Color: 2

Bin 141: 90 of cap free
Amount of items: 2
Items: 
Size: 561199 Color: 3
Size: 438712 Color: 2

Bin 142: 90 of cap free
Amount of items: 2
Items: 
Size: 726671 Color: 1
Size: 273240 Color: 0

Bin 143: 91 of cap free
Amount of items: 2
Items: 
Size: 562318 Color: 3
Size: 437592 Color: 2

Bin 144: 91 of cap free
Amount of items: 2
Items: 
Size: 582024 Color: 2
Size: 417886 Color: 4

Bin 145: 94 of cap free
Amount of items: 2
Items: 
Size: 563665 Color: 3
Size: 436242 Color: 0

Bin 146: 94 of cap free
Amount of items: 2
Items: 
Size: 566613 Color: 1
Size: 433294 Color: 0

Bin 147: 94 of cap free
Amount of items: 2
Items: 
Size: 612130 Color: 4
Size: 387777 Color: 2

Bin 148: 94 of cap free
Amount of items: 2
Items: 
Size: 645597 Color: 0
Size: 354310 Color: 4

Bin 149: 95 of cap free
Amount of items: 2
Items: 
Size: 602325 Color: 2
Size: 397581 Color: 0

Bin 150: 95 of cap free
Amount of items: 2
Items: 
Size: 637814 Color: 0
Size: 362092 Color: 4

Bin 151: 95 of cap free
Amount of items: 2
Items: 
Size: 668091 Color: 2
Size: 331815 Color: 0

Bin 152: 96 of cap free
Amount of items: 2
Items: 
Size: 528022 Color: 2
Size: 471883 Color: 3

Bin 153: 96 of cap free
Amount of items: 2
Items: 
Size: 577136 Color: 4
Size: 422769 Color: 2

Bin 154: 96 of cap free
Amount of items: 2
Items: 
Size: 612253 Color: 4
Size: 387652 Color: 0

Bin 155: 96 of cap free
Amount of items: 2
Items: 
Size: 654461 Color: 1
Size: 345444 Color: 0

Bin 156: 97 of cap free
Amount of items: 2
Items: 
Size: 506410 Color: 4
Size: 493494 Color: 1

Bin 157: 97 of cap free
Amount of items: 2
Items: 
Size: 567256 Color: 3
Size: 432648 Color: 1

Bin 158: 98 of cap free
Amount of items: 2
Items: 
Size: 570479 Color: 1
Size: 429424 Color: 4

Bin 159: 98 of cap free
Amount of items: 2
Items: 
Size: 702921 Color: 4
Size: 296982 Color: 2

Bin 160: 99 of cap free
Amount of items: 2
Items: 
Size: 570238 Color: 2
Size: 429664 Color: 4

Bin 161: 99 of cap free
Amount of items: 2
Items: 
Size: 636276 Color: 2
Size: 363626 Color: 0

Bin 162: 99 of cap free
Amount of items: 2
Items: 
Size: 727634 Color: 4
Size: 272268 Color: 2

Bin 163: 99 of cap free
Amount of items: 2
Items: 
Size: 742470 Color: 1
Size: 257432 Color: 4

Bin 164: 99 of cap free
Amount of items: 2
Items: 
Size: 783447 Color: 1
Size: 216455 Color: 0

Bin 165: 100 of cap free
Amount of items: 2
Items: 
Size: 535840 Color: 1
Size: 464061 Color: 2

Bin 166: 101 of cap free
Amount of items: 2
Items: 
Size: 613519 Color: 3
Size: 386381 Color: 2

Bin 167: 101 of cap free
Amount of items: 2
Items: 
Size: 669614 Color: 4
Size: 330286 Color: 1

Bin 168: 103 of cap free
Amount of items: 2
Items: 
Size: 621677 Color: 2
Size: 378221 Color: 0

Bin 169: 105 of cap free
Amount of items: 2
Items: 
Size: 676045 Color: 4
Size: 323851 Color: 0

Bin 170: 105 of cap free
Amount of items: 2
Items: 
Size: 728346 Color: 1
Size: 271550 Color: 3

Bin 171: 108 of cap free
Amount of items: 2
Items: 
Size: 769777 Color: 4
Size: 230116 Color: 0

Bin 172: 110 of cap free
Amount of items: 2
Items: 
Size: 522288 Color: 1
Size: 477603 Color: 0

Bin 173: 110 of cap free
Amount of items: 2
Items: 
Size: 523181 Color: 1
Size: 476710 Color: 3

Bin 174: 111 of cap free
Amount of items: 2
Items: 
Size: 514352 Color: 1
Size: 485538 Color: 2

Bin 175: 111 of cap free
Amount of items: 2
Items: 
Size: 627648 Color: 0
Size: 372242 Color: 4

Bin 176: 113 of cap free
Amount of items: 2
Items: 
Size: 727504 Color: 0
Size: 272384 Color: 4

Bin 177: 114 of cap free
Amount of items: 2
Items: 
Size: 794468 Color: 1
Size: 205419 Color: 2

Bin 178: 115 of cap free
Amount of items: 2
Items: 
Size: 765134 Color: 2
Size: 234752 Color: 4

Bin 179: 118 of cap free
Amount of items: 2
Items: 
Size: 523376 Color: 1
Size: 476507 Color: 4

Bin 180: 118 of cap free
Amount of items: 2
Items: 
Size: 628968 Color: 3
Size: 370915 Color: 1

Bin 181: 119 of cap free
Amount of items: 2
Items: 
Size: 557785 Color: 0
Size: 442097 Color: 3

Bin 182: 119 of cap free
Amount of items: 2
Items: 
Size: 569957 Color: 4
Size: 429925 Color: 0

Bin 183: 119 of cap free
Amount of items: 2
Items: 
Size: 756219 Color: 0
Size: 243663 Color: 3

Bin 184: 121 of cap free
Amount of items: 2
Items: 
Size: 645799 Color: 2
Size: 354081 Color: 0

Bin 185: 121 of cap free
Amount of items: 2
Items: 
Size: 649818 Color: 0
Size: 350062 Color: 2

Bin 186: 121 of cap free
Amount of items: 2
Items: 
Size: 776928 Color: 3
Size: 222952 Color: 4

Bin 187: 122 of cap free
Amount of items: 2
Items: 
Size: 687579 Color: 1
Size: 312300 Color: 0

Bin 188: 123 of cap free
Amount of items: 2
Items: 
Size: 588128 Color: 1
Size: 411750 Color: 2

Bin 189: 124 of cap free
Amount of items: 2
Items: 
Size: 685680 Color: 3
Size: 314197 Color: 1

Bin 190: 125 of cap free
Amount of items: 2
Items: 
Size: 672830 Color: 2
Size: 327046 Color: 1

Bin 191: 126 of cap free
Amount of items: 2
Items: 
Size: 523501 Color: 1
Size: 476374 Color: 0

Bin 192: 126 of cap free
Amount of items: 2
Items: 
Size: 639856 Color: 1
Size: 360019 Color: 0

Bin 193: 126 of cap free
Amount of items: 2
Items: 
Size: 724456 Color: 1
Size: 275419 Color: 2

Bin 194: 127 of cap free
Amount of items: 2
Items: 
Size: 530353 Color: 2
Size: 469521 Color: 3

Bin 195: 127 of cap free
Amount of items: 2
Items: 
Size: 671377 Color: 2
Size: 328497 Color: 0

Bin 196: 127 of cap free
Amount of items: 2
Items: 
Size: 748354 Color: 2
Size: 251520 Color: 4

Bin 197: 129 of cap free
Amount of items: 6
Items: 
Size: 169152 Color: 1
Size: 169514 Color: 4
Size: 169012 Color: 1
Size: 168579 Color: 4
Size: 168549 Color: 3
Size: 155066 Color: 4

Bin 198: 129 of cap free
Amount of items: 2
Items: 
Size: 526783 Color: 2
Size: 473089 Color: 0

Bin 199: 131 of cap free
Amount of items: 2
Items: 
Size: 553454 Color: 3
Size: 446416 Color: 4

Bin 200: 131 of cap free
Amount of items: 2
Items: 
Size: 744438 Color: 4
Size: 255432 Color: 0

Bin 201: 132 of cap free
Amount of items: 2
Items: 
Size: 784720 Color: 2
Size: 215149 Color: 4

Bin 202: 133 of cap free
Amount of items: 2
Items: 
Size: 683364 Color: 1
Size: 316504 Color: 4

Bin 203: 134 of cap free
Amount of items: 2
Items: 
Size: 590528 Color: 2
Size: 409339 Color: 4

Bin 204: 135 of cap free
Amount of items: 2
Items: 
Size: 595390 Color: 3
Size: 404476 Color: 0

Bin 205: 137 of cap free
Amount of items: 2
Items: 
Size: 500209 Color: 2
Size: 499655 Color: 3

Bin 206: 137 of cap free
Amount of items: 2
Items: 
Size: 759364 Color: 4
Size: 240500 Color: 0

Bin 207: 138 of cap free
Amount of items: 2
Items: 
Size: 634754 Color: 0
Size: 365109 Color: 4

Bin 208: 138 of cap free
Amount of items: 2
Items: 
Size: 724157 Color: 1
Size: 275706 Color: 2

Bin 209: 138 of cap free
Amount of items: 2
Items: 
Size: 754683 Color: 1
Size: 245180 Color: 2

Bin 210: 141 of cap free
Amount of items: 2
Items: 
Size: 695933 Color: 4
Size: 303927 Color: 1

Bin 211: 142 of cap free
Amount of items: 2
Items: 
Size: 520900 Color: 4
Size: 478959 Color: 1

Bin 212: 142 of cap free
Amount of items: 2
Items: 
Size: 539043 Color: 4
Size: 460816 Color: 1

Bin 213: 142 of cap free
Amount of items: 2
Items: 
Size: 546716 Color: 1
Size: 453143 Color: 2

Bin 214: 142 of cap free
Amount of items: 2
Items: 
Size: 681805 Color: 0
Size: 318054 Color: 1

Bin 215: 142 of cap free
Amount of items: 2
Items: 
Size: 751263 Color: 4
Size: 248596 Color: 2

Bin 216: 143 of cap free
Amount of items: 2
Items: 
Size: 577570 Color: 0
Size: 422288 Color: 3

Bin 217: 143 of cap free
Amount of items: 2
Items: 
Size: 719567 Color: 2
Size: 280291 Color: 3

Bin 218: 143 of cap free
Amount of items: 2
Items: 
Size: 730840 Color: 2
Size: 269018 Color: 1

Bin 219: 144 of cap free
Amount of items: 2
Items: 
Size: 598906 Color: 4
Size: 400951 Color: 1

Bin 220: 145 of cap free
Amount of items: 2
Items: 
Size: 525721 Color: 1
Size: 474135 Color: 4

Bin 221: 145 of cap free
Amount of items: 2
Items: 
Size: 749319 Color: 4
Size: 250537 Color: 1

Bin 222: 146 of cap free
Amount of items: 2
Items: 
Size: 515733 Color: 1
Size: 484122 Color: 2

Bin 223: 146 of cap free
Amount of items: 2
Items: 
Size: 557575 Color: 1
Size: 442280 Color: 4

Bin 224: 146 of cap free
Amount of items: 2
Items: 
Size: 644487 Color: 4
Size: 355368 Color: 3

Bin 225: 147 of cap free
Amount of items: 2
Items: 
Size: 567383 Color: 2
Size: 432471 Color: 0

Bin 226: 147 of cap free
Amount of items: 2
Items: 
Size: 629960 Color: 4
Size: 369894 Color: 2

Bin 227: 147 of cap free
Amount of items: 2
Items: 
Size: 758459 Color: 4
Size: 241395 Color: 1

Bin 228: 147 of cap free
Amount of items: 2
Items: 
Size: 789932 Color: 1
Size: 209922 Color: 0

Bin 229: 148 of cap free
Amount of items: 2
Items: 
Size: 542427 Color: 2
Size: 457426 Color: 1

Bin 230: 149 of cap free
Amount of items: 2
Items: 
Size: 569517 Color: 3
Size: 430335 Color: 0

Bin 231: 150 of cap free
Amount of items: 2
Items: 
Size: 606077 Color: 0
Size: 393774 Color: 4

Bin 232: 150 of cap free
Amount of items: 2
Items: 
Size: 713253 Color: 4
Size: 286598 Color: 2

Bin 233: 150 of cap free
Amount of items: 2
Items: 
Size: 733790 Color: 4
Size: 266061 Color: 2

Bin 234: 151 of cap free
Amount of items: 2
Items: 
Size: 560818 Color: 0
Size: 439032 Color: 4

Bin 235: 153 of cap free
Amount of items: 2
Items: 
Size: 568211 Color: 1
Size: 431637 Color: 3

Bin 236: 155 of cap free
Amount of items: 2
Items: 
Size: 565223 Color: 4
Size: 434623 Color: 2

Bin 237: 156 of cap free
Amount of items: 3
Items: 
Size: 376857 Color: 3
Size: 374148 Color: 4
Size: 248840 Color: 3

Bin 238: 156 of cap free
Amount of items: 2
Items: 
Size: 543979 Color: 0
Size: 455866 Color: 3

Bin 239: 156 of cap free
Amount of items: 2
Items: 
Size: 653288 Color: 0
Size: 346557 Color: 1

Bin 240: 158 of cap free
Amount of items: 2
Items: 
Size: 677871 Color: 0
Size: 321972 Color: 4

Bin 241: 158 of cap free
Amount of items: 2
Items: 
Size: 706430 Color: 4
Size: 293413 Color: 2

Bin 242: 158 of cap free
Amount of items: 2
Items: 
Size: 758884 Color: 0
Size: 240959 Color: 1

Bin 243: 160 of cap free
Amount of items: 2
Items: 
Size: 550639 Color: 0
Size: 449202 Color: 2

Bin 244: 160 of cap free
Amount of items: 2
Items: 
Size: 684489 Color: 2
Size: 315352 Color: 1

Bin 245: 160 of cap free
Amount of items: 2
Items: 
Size: 728516 Color: 0
Size: 271325 Color: 4

Bin 246: 161 of cap free
Amount of items: 2
Items: 
Size: 689214 Color: 2
Size: 310626 Color: 0

Bin 247: 161 of cap free
Amount of items: 2
Items: 
Size: 751544 Color: 1
Size: 248296 Color: 3

Bin 248: 162 of cap free
Amount of items: 2
Items: 
Size: 722017 Color: 0
Size: 277822 Color: 3

Bin 249: 164 of cap free
Amount of items: 2
Items: 
Size: 799220 Color: 0
Size: 200617 Color: 1

Bin 250: 165 of cap free
Amount of items: 2
Items: 
Size: 601050 Color: 2
Size: 398786 Color: 3

Bin 251: 166 of cap free
Amount of items: 2
Items: 
Size: 630958 Color: 1
Size: 368877 Color: 3

Bin 252: 166 of cap free
Amount of items: 2
Items: 
Size: 679798 Color: 4
Size: 320037 Color: 3

Bin 253: 166 of cap free
Amount of items: 2
Items: 
Size: 757719 Color: 2
Size: 242116 Color: 3

Bin 254: 167 of cap free
Amount of items: 2
Items: 
Size: 772801 Color: 1
Size: 227033 Color: 4

Bin 255: 168 of cap free
Amount of items: 3
Items: 
Size: 449164 Color: 4
Size: 448530 Color: 1
Size: 102139 Color: 1

Bin 256: 168 of cap free
Amount of items: 2
Items: 
Size: 680381 Color: 4
Size: 319452 Color: 2

Bin 257: 171 of cap free
Amount of items: 2
Items: 
Size: 546061 Color: 3
Size: 453769 Color: 2

Bin 258: 171 of cap free
Amount of items: 2
Items: 
Size: 745506 Color: 2
Size: 254324 Color: 1

Bin 259: 173 of cap free
Amount of items: 2
Items: 
Size: 520455 Color: 1
Size: 479373 Color: 0

Bin 260: 174 of cap free
Amount of items: 2
Items: 
Size: 773171 Color: 1
Size: 226656 Color: 4

Bin 261: 175 of cap free
Amount of items: 3
Items: 
Size: 432993 Color: 1
Size: 430942 Color: 2
Size: 135891 Color: 0

Bin 262: 175 of cap free
Amount of items: 2
Items: 
Size: 506048 Color: 3
Size: 493778 Color: 4

Bin 263: 176 of cap free
Amount of items: 2
Items: 
Size: 593815 Color: 0
Size: 406010 Color: 1

Bin 264: 177 of cap free
Amount of items: 2
Items: 
Size: 519023 Color: 4
Size: 480801 Color: 1

Bin 265: 177 of cap free
Amount of items: 2
Items: 
Size: 719884 Color: 1
Size: 279940 Color: 4

Bin 266: 178 of cap free
Amount of items: 2
Items: 
Size: 572151 Color: 4
Size: 427672 Color: 3

Bin 267: 179 of cap free
Amount of items: 2
Items: 
Size: 665552 Color: 2
Size: 334270 Color: 1

Bin 268: 179 of cap free
Amount of items: 2
Items: 
Size: 770396 Color: 1
Size: 229426 Color: 3

Bin 269: 180 of cap free
Amount of items: 2
Items: 
Size: 501679 Color: 3
Size: 498142 Color: 0

Bin 270: 181 of cap free
Amount of items: 2
Items: 
Size: 517149 Color: 1
Size: 482671 Color: 3

Bin 271: 181 of cap free
Amount of items: 2
Items: 
Size: 767133 Color: 0
Size: 232687 Color: 2

Bin 272: 181 of cap free
Amount of items: 2
Items: 
Size: 782714 Color: 1
Size: 217106 Color: 2

Bin 273: 184 of cap free
Amount of items: 2
Items: 
Size: 590516 Color: 3
Size: 409301 Color: 0

Bin 274: 184 of cap free
Amount of items: 2
Items: 
Size: 649227 Color: 0
Size: 350590 Color: 3

Bin 275: 185 of cap free
Amount of items: 2
Items: 
Size: 623017 Color: 3
Size: 376799 Color: 2

Bin 276: 186 of cap free
Amount of items: 2
Items: 
Size: 526922 Color: 4
Size: 472893 Color: 3

Bin 277: 186 of cap free
Amount of items: 2
Items: 
Size: 680871 Color: 1
Size: 318944 Color: 4

Bin 278: 186 of cap free
Amount of items: 2
Items: 
Size: 757413 Color: 4
Size: 242402 Color: 0

Bin 279: 189 of cap free
Amount of items: 2
Items: 
Size: 619210 Color: 3
Size: 380602 Color: 2

Bin 280: 190 of cap free
Amount of items: 2
Items: 
Size: 534885 Color: 4
Size: 464926 Color: 2

Bin 281: 190 of cap free
Amount of items: 2
Items: 
Size: 593549 Color: 1
Size: 406262 Color: 4

Bin 282: 192 of cap free
Amount of items: 2
Items: 
Size: 666172 Color: 2
Size: 333637 Color: 0

Bin 283: 192 of cap free
Amount of items: 2
Items: 
Size: 702036 Color: 1
Size: 297773 Color: 4

Bin 284: 193 of cap free
Amount of items: 2
Items: 
Size: 661450 Color: 2
Size: 338358 Color: 1

Bin 285: 193 of cap free
Amount of items: 2
Items: 
Size: 764605 Color: 0
Size: 235203 Color: 3

Bin 286: 194 of cap free
Amount of items: 2
Items: 
Size: 733129 Color: 0
Size: 266678 Color: 1

Bin 287: 199 of cap free
Amount of items: 2
Items: 
Size: 660062 Color: 2
Size: 339740 Color: 4

Bin 288: 200 of cap free
Amount of items: 2
Items: 
Size: 561064 Color: 0
Size: 438737 Color: 1

Bin 289: 200 of cap free
Amount of items: 2
Items: 
Size: 700434 Color: 2
Size: 299367 Color: 3

Bin 290: 201 of cap free
Amount of items: 2
Items: 
Size: 526307 Color: 1
Size: 473493 Color: 4

Bin 291: 201 of cap free
Amount of items: 2
Items: 
Size: 744967 Color: 2
Size: 254833 Color: 4

Bin 292: 202 of cap free
Amount of items: 2
Items: 
Size: 771582 Color: 1
Size: 228217 Color: 2

Bin 293: 203 of cap free
Amount of items: 2
Items: 
Size: 568199 Color: 0
Size: 431599 Color: 4

Bin 294: 203 of cap free
Amount of items: 2
Items: 
Size: 724773 Color: 1
Size: 275025 Color: 0

Bin 295: 209 of cap free
Amount of items: 2
Items: 
Size: 589590 Color: 2
Size: 410202 Color: 1

Bin 296: 211 of cap free
Amount of items: 2
Items: 
Size: 794382 Color: 4
Size: 205408 Color: 0

Bin 297: 212 of cap free
Amount of items: 2
Items: 
Size: 615246 Color: 3
Size: 384543 Color: 0

Bin 298: 215 of cap free
Amount of items: 2
Items: 
Size: 653760 Color: 2
Size: 346026 Color: 0

Bin 299: 215 of cap free
Amount of items: 2
Items: 
Size: 681314 Color: 2
Size: 318472 Color: 0

Bin 300: 215 of cap free
Amount of items: 2
Items: 
Size: 752739 Color: 4
Size: 247047 Color: 1

Bin 301: 219 of cap free
Amount of items: 2
Items: 
Size: 570432 Color: 2
Size: 429350 Color: 4

Bin 302: 220 of cap free
Amount of items: 3
Items: 
Size: 434432 Color: 2
Size: 433713 Color: 1
Size: 131636 Color: 3

Bin 303: 221 of cap free
Amount of items: 2
Items: 
Size: 511214 Color: 4
Size: 488566 Color: 2

Bin 304: 221 of cap free
Amount of items: 2
Items: 
Size: 667143 Color: 1
Size: 332637 Color: 3

Bin 305: 221 of cap free
Amount of items: 2
Items: 
Size: 691288 Color: 1
Size: 308492 Color: 3

Bin 306: 223 of cap free
Amount of items: 2
Items: 
Size: 529104 Color: 0
Size: 470674 Color: 4

Bin 307: 223 of cap free
Amount of items: 2
Items: 
Size: 680160 Color: 3
Size: 319618 Color: 4

Bin 308: 224 of cap free
Amount of items: 2
Items: 
Size: 546871 Color: 2
Size: 452906 Color: 4

Bin 309: 224 of cap free
Amount of items: 2
Items: 
Size: 681265 Color: 3
Size: 318512 Color: 2

Bin 310: 224 of cap free
Amount of items: 2
Items: 
Size: 735657 Color: 0
Size: 264120 Color: 4

Bin 311: 224 of cap free
Amount of items: 2
Items: 
Size: 777642 Color: 4
Size: 222135 Color: 2

Bin 312: 227 of cap free
Amount of items: 2
Items: 
Size: 686282 Color: 0
Size: 313492 Color: 2

Bin 313: 232 of cap free
Amount of items: 2
Items: 
Size: 534832 Color: 0
Size: 464937 Color: 4

Bin 314: 232 of cap free
Amount of items: 2
Items: 
Size: 786826 Color: 4
Size: 212943 Color: 0

Bin 315: 233 of cap free
Amount of items: 2
Items: 
Size: 778370 Color: 1
Size: 221398 Color: 2

Bin 316: 234 of cap free
Amount of items: 2
Items: 
Size: 590498 Color: 4
Size: 409269 Color: 1

Bin 317: 236 of cap free
Amount of items: 2
Items: 
Size: 715809 Color: 4
Size: 283956 Color: 2

Bin 318: 237 of cap free
Amount of items: 2
Items: 
Size: 684266 Color: 3
Size: 315498 Color: 2

Bin 319: 240 of cap free
Amount of items: 2
Items: 
Size: 607928 Color: 0
Size: 391833 Color: 2

Bin 320: 241 of cap free
Amount of items: 2
Items: 
Size: 763807 Color: 0
Size: 235953 Color: 1

Bin 321: 242 of cap free
Amount of items: 2
Items: 
Size: 637413 Color: 0
Size: 362346 Color: 1

Bin 322: 243 of cap free
Amount of items: 2
Items: 
Size: 510607 Color: 2
Size: 489151 Color: 3

Bin 323: 243 of cap free
Amount of items: 2
Items: 
Size: 551122 Color: 1
Size: 448636 Color: 0

Bin 324: 243 of cap free
Amount of items: 2
Items: 
Size: 622684 Color: 0
Size: 377074 Color: 2

Bin 325: 244 of cap free
Amount of items: 2
Items: 
Size: 624334 Color: 0
Size: 375423 Color: 2

Bin 326: 246 of cap free
Amount of items: 2
Items: 
Size: 509940 Color: 0
Size: 489815 Color: 2

Bin 327: 246 of cap free
Amount of items: 2
Items: 
Size: 656229 Color: 3
Size: 343526 Color: 0

Bin 328: 251 of cap free
Amount of items: 3
Items: 
Size: 334591 Color: 3
Size: 334026 Color: 4
Size: 331133 Color: 4

Bin 329: 256 of cap free
Amount of items: 2
Items: 
Size: 787680 Color: 1
Size: 212065 Color: 2

Bin 330: 257 of cap free
Amount of items: 2
Items: 
Size: 757984 Color: 1
Size: 241760 Color: 0

Bin 331: 262 of cap free
Amount of items: 2
Items: 
Size: 524685 Color: 3
Size: 475054 Color: 4

Bin 332: 262 of cap free
Amount of items: 2
Items: 
Size: 769410 Color: 2
Size: 230329 Color: 0

Bin 333: 264 of cap free
Amount of items: 2
Items: 
Size: 726053 Color: 2
Size: 273684 Color: 0

Bin 334: 265 of cap free
Amount of items: 2
Items: 
Size: 590913 Color: 1
Size: 408823 Color: 3

Bin 335: 265 of cap free
Amount of items: 2
Items: 
Size: 736722 Color: 2
Size: 263014 Color: 0

Bin 336: 269 of cap free
Amount of items: 2
Items: 
Size: 691848 Color: 1
Size: 307884 Color: 4

Bin 337: 269 of cap free
Amount of items: 2
Items: 
Size: 700995 Color: 3
Size: 298737 Color: 1

Bin 338: 270 of cap free
Amount of items: 2
Items: 
Size: 516108 Color: 2
Size: 483623 Color: 4

Bin 339: 271 of cap free
Amount of items: 2
Items: 
Size: 566882 Color: 4
Size: 432848 Color: 0

Bin 340: 271 of cap free
Amount of items: 2
Items: 
Size: 663595 Color: 3
Size: 336135 Color: 0

Bin 341: 273 of cap free
Amount of items: 2
Items: 
Size: 750594 Color: 0
Size: 249134 Color: 2

Bin 342: 275 of cap free
Amount of items: 6
Items: 
Size: 172407 Color: 2
Size: 171112 Color: 0
Size: 170873 Color: 1
Size: 170754 Color: 3
Size: 170194 Color: 4
Size: 144386 Color: 0

Bin 343: 276 of cap free
Amount of items: 2
Items: 
Size: 758426 Color: 3
Size: 241299 Color: 0

Bin 344: 277 of cap free
Amount of items: 2
Items: 
Size: 653187 Color: 2
Size: 346537 Color: 4

Bin 345: 280 of cap free
Amount of items: 2
Items: 
Size: 699444 Color: 0
Size: 300277 Color: 2

Bin 346: 283 of cap free
Amount of items: 3
Items: 
Size: 439009 Color: 0
Size: 434897 Color: 1
Size: 125812 Color: 4

Bin 347: 284 of cap free
Amount of items: 2
Items: 
Size: 694515 Color: 1
Size: 305202 Color: 2

Bin 348: 285 of cap free
Amount of items: 2
Items: 
Size: 527644 Color: 4
Size: 472072 Color: 0

Bin 349: 288 of cap free
Amount of items: 2
Items: 
Size: 635358 Color: 4
Size: 364355 Color: 0

Bin 350: 289 of cap free
Amount of items: 2
Items: 
Size: 777571 Color: 3
Size: 222141 Color: 4

Bin 351: 290 of cap free
Amount of items: 2
Items: 
Size: 617356 Color: 3
Size: 382355 Color: 2

Bin 352: 292 of cap free
Amount of items: 2
Items: 
Size: 656736 Color: 1
Size: 342973 Color: 0

Bin 353: 293 of cap free
Amount of items: 2
Items: 
Size: 571308 Color: 4
Size: 428400 Color: 2

Bin 354: 294 of cap free
Amount of items: 2
Items: 
Size: 652891 Color: 2
Size: 346816 Color: 0

Bin 355: 296 of cap free
Amount of items: 2
Items: 
Size: 533166 Color: 3
Size: 466539 Color: 1

Bin 356: 296 of cap free
Amount of items: 2
Items: 
Size: 564326 Color: 0
Size: 435379 Color: 2

Bin 357: 298 of cap free
Amount of items: 2
Items: 
Size: 742519 Color: 4
Size: 257184 Color: 0

Bin 358: 300 of cap free
Amount of items: 2
Items: 
Size: 559135 Color: 1
Size: 440566 Color: 0

Bin 359: 301 of cap free
Amount of items: 2
Items: 
Size: 649950 Color: 1
Size: 349750 Color: 3

Bin 360: 301 of cap free
Amount of items: 2
Items: 
Size: 740413 Color: 0
Size: 259287 Color: 1

Bin 361: 304 of cap free
Amount of items: 2
Items: 
Size: 534427 Color: 0
Size: 465270 Color: 1

Bin 362: 304 of cap free
Amount of items: 2
Items: 
Size: 593167 Color: 4
Size: 406530 Color: 3

Bin 363: 304 of cap free
Amount of items: 2
Items: 
Size: 643574 Color: 4
Size: 356123 Color: 2

Bin 364: 306 of cap free
Amount of items: 2
Items: 
Size: 518976 Color: 1
Size: 480719 Color: 4

Bin 365: 306 of cap free
Amount of items: 2
Items: 
Size: 672062 Color: 2
Size: 327633 Color: 4

Bin 366: 307 of cap free
Amount of items: 2
Items: 
Size: 583024 Color: 4
Size: 416670 Color: 0

Bin 367: 309 of cap free
Amount of items: 2
Items: 
Size: 594620 Color: 2
Size: 405072 Color: 3

Bin 368: 309 of cap free
Amount of items: 2
Items: 
Size: 638123 Color: 1
Size: 361569 Color: 2

Bin 369: 310 of cap free
Amount of items: 2
Items: 
Size: 759853 Color: 3
Size: 239838 Color: 0

Bin 370: 313 of cap free
Amount of items: 2
Items: 
Size: 788836 Color: 3
Size: 210852 Color: 4

Bin 371: 314 of cap free
Amount of items: 2
Items: 
Size: 563338 Color: 2
Size: 436349 Color: 3

Bin 372: 314 of cap free
Amount of items: 2
Items: 
Size: 776399 Color: 2
Size: 223288 Color: 1

Bin 373: 318 of cap free
Amount of items: 2
Items: 
Size: 590085 Color: 2
Size: 409598 Color: 0

Bin 374: 320 of cap free
Amount of items: 2
Items: 
Size: 752190 Color: 0
Size: 247491 Color: 4

Bin 375: 322 of cap free
Amount of items: 2
Items: 
Size: 581009 Color: 4
Size: 418670 Color: 3

Bin 376: 322 of cap free
Amount of items: 2
Items: 
Size: 646924 Color: 1
Size: 352755 Color: 4

Bin 377: 323 of cap free
Amount of items: 2
Items: 
Size: 559283 Color: 0
Size: 440395 Color: 3

Bin 378: 323 of cap free
Amount of items: 2
Items: 
Size: 620626 Color: 0
Size: 379052 Color: 3

Bin 379: 324 of cap free
Amount of items: 2
Items: 
Size: 538575 Color: 2
Size: 461102 Color: 1

Bin 380: 325 of cap free
Amount of items: 2
Items: 
Size: 509542 Color: 2
Size: 490134 Color: 3

Bin 381: 329 of cap free
Amount of items: 2
Items: 
Size: 535977 Color: 1
Size: 463695 Color: 2

Bin 382: 329 of cap free
Amount of items: 2
Items: 
Size: 709273 Color: 2
Size: 290399 Color: 1

Bin 383: 330 of cap free
Amount of items: 2
Items: 
Size: 528249 Color: 1
Size: 471422 Color: 3

Bin 384: 332 of cap free
Amount of items: 2
Items: 
Size: 548321 Color: 2
Size: 451348 Color: 4

Bin 385: 332 of cap free
Amount of items: 2
Items: 
Size: 718146 Color: 1
Size: 281523 Color: 2

Bin 386: 332 of cap free
Amount of items: 2
Items: 
Size: 765566 Color: 3
Size: 234103 Color: 4

Bin 387: 334 of cap free
Amount of items: 2
Items: 
Size: 695038 Color: 1
Size: 304629 Color: 3

Bin 388: 336 of cap free
Amount of items: 2
Items: 
Size: 677122 Color: 4
Size: 322543 Color: 2

Bin 389: 337 of cap free
Amount of items: 2
Items: 
Size: 599529 Color: 2
Size: 400135 Color: 0

Bin 390: 337 of cap free
Amount of items: 2
Items: 
Size: 787276 Color: 4
Size: 212388 Color: 3

Bin 391: 339 of cap free
Amount of items: 2
Items: 
Size: 698784 Color: 1
Size: 300878 Color: 4

Bin 392: 340 of cap free
Amount of items: 7
Items: 
Size: 151152 Color: 2
Size: 150762 Color: 0
Size: 150839 Color: 2
Size: 148470 Color: 0
Size: 148423 Color: 4
Size: 148280 Color: 2
Size: 101735 Color: 2

Bin 393: 340 of cap free
Amount of items: 3
Items: 
Size: 373127 Color: 3
Size: 373207 Color: 1
Size: 253327 Color: 0

Bin 394: 340 of cap free
Amount of items: 2
Items: 
Size: 662143 Color: 4
Size: 337518 Color: 3

Bin 395: 345 of cap free
Amount of items: 2
Items: 
Size: 507510 Color: 1
Size: 492146 Color: 2

Bin 396: 345 of cap free
Amount of items: 2
Items: 
Size: 778680 Color: 0
Size: 220976 Color: 2

Bin 397: 347 of cap free
Amount of items: 2
Items: 
Size: 653662 Color: 4
Size: 345992 Color: 1

Bin 398: 350 of cap free
Amount of items: 2
Items: 
Size: 500080 Color: 0
Size: 499571 Color: 4

Bin 399: 350 of cap free
Amount of items: 2
Items: 
Size: 685907 Color: 3
Size: 313744 Color: 0

Bin 400: 350 of cap free
Amount of items: 2
Items: 
Size: 720934 Color: 0
Size: 278717 Color: 1

Bin 401: 353 of cap free
Amount of items: 2
Items: 
Size: 512467 Color: 4
Size: 487181 Color: 2

Bin 402: 353 of cap free
Amount of items: 2
Items: 
Size: 618748 Color: 0
Size: 380900 Color: 1

Bin 403: 354 of cap free
Amount of items: 2
Items: 
Size: 589805 Color: 3
Size: 409842 Color: 2

Bin 404: 355 of cap free
Amount of items: 2
Items: 
Size: 670682 Color: 4
Size: 328964 Color: 1

Bin 405: 359 of cap free
Amount of items: 2
Items: 
Size: 538029 Color: 0
Size: 461613 Color: 4

Bin 406: 363 of cap free
Amount of items: 2
Items: 
Size: 789509 Color: 0
Size: 210129 Color: 2

Bin 407: 367 of cap free
Amount of items: 2
Items: 
Size: 784027 Color: 2
Size: 215607 Color: 3

Bin 408: 368 of cap free
Amount of items: 6
Items: 
Size: 178154 Color: 2
Size: 178130 Color: 3
Size: 177584 Color: 1
Size: 176918 Color: 4
Size: 177501 Color: 1
Size: 111346 Color: 4

Bin 409: 368 of cap free
Amount of items: 2
Items: 
Size: 786062 Color: 2
Size: 213571 Color: 1

Bin 410: 369 of cap free
Amount of items: 2
Items: 
Size: 703895 Color: 3
Size: 295737 Color: 1

Bin 411: 373 of cap free
Amount of items: 2
Items: 
Size: 504754 Color: 2
Size: 494874 Color: 4

Bin 412: 374 of cap free
Amount of items: 2
Items: 
Size: 736704 Color: 3
Size: 262923 Color: 1

Bin 413: 380 of cap free
Amount of items: 2
Items: 
Size: 536997 Color: 4
Size: 462624 Color: 1

Bin 414: 383 of cap free
Amount of items: 2
Items: 
Size: 616616 Color: 0
Size: 383002 Color: 3

Bin 415: 383 of cap free
Amount of items: 2
Items: 
Size: 759457 Color: 0
Size: 240161 Color: 2

Bin 416: 384 of cap free
Amount of items: 2
Items: 
Size: 559736 Color: 3
Size: 439881 Color: 2

Bin 417: 385 of cap free
Amount of items: 2
Items: 
Size: 511727 Color: 2
Size: 487889 Color: 3

Bin 418: 386 of cap free
Amount of items: 2
Items: 
Size: 506618 Color: 1
Size: 492997 Color: 2

Bin 419: 387 of cap free
Amount of items: 2
Items: 
Size: 565880 Color: 0
Size: 433734 Color: 3

Bin 420: 397 of cap free
Amount of items: 7
Items: 
Size: 146133 Color: 0
Size: 145225 Color: 4
Size: 145091 Color: 2
Size: 144856 Color: 1
Size: 144036 Color: 4
Size: 142729 Color: 3
Size: 131534 Color: 2

Bin 421: 400 of cap free
Amount of items: 2
Items: 
Size: 601645 Color: 2
Size: 397956 Color: 4

Bin 422: 401 of cap free
Amount of items: 2
Items: 
Size: 585962 Color: 3
Size: 413638 Color: 2

Bin 423: 403 of cap free
Amount of items: 2
Items: 
Size: 535415 Color: 4
Size: 464183 Color: 2

Bin 424: 408 of cap free
Amount of items: 2
Items: 
Size: 625266 Color: 2
Size: 374327 Color: 3

Bin 425: 411 of cap free
Amount of items: 2
Items: 
Size: 686936 Color: 1
Size: 312654 Color: 3

Bin 426: 415 of cap free
Amount of items: 2
Items: 
Size: 798345 Color: 4
Size: 201241 Color: 0

Bin 427: 416 of cap free
Amount of items: 2
Items: 
Size: 729785 Color: 2
Size: 269800 Color: 0

Bin 428: 419 of cap free
Amount of items: 2
Items: 
Size: 521533 Color: 4
Size: 478049 Color: 0

Bin 429: 419 of cap free
Amount of items: 2
Items: 
Size: 543296 Color: 0
Size: 456286 Color: 2

Bin 430: 419 of cap free
Amount of items: 2
Items: 
Size: 657203 Color: 4
Size: 342379 Color: 2

Bin 431: 421 of cap free
Amount of items: 2
Items: 
Size: 592547 Color: 3
Size: 407033 Color: 1

Bin 432: 422 of cap free
Amount of items: 2
Items: 
Size: 575681 Color: 1
Size: 423898 Color: 2

Bin 433: 423 of cap free
Amount of items: 2
Items: 
Size: 782607 Color: 3
Size: 216971 Color: 1

Bin 434: 424 of cap free
Amount of items: 2
Items: 
Size: 501614 Color: 0
Size: 497963 Color: 2

Bin 435: 428 of cap free
Amount of items: 3
Items: 
Size: 427559 Color: 2
Size: 426720 Color: 0
Size: 145294 Color: 2

Bin 436: 428 of cap free
Amount of items: 2
Items: 
Size: 747350 Color: 3
Size: 252223 Color: 2

Bin 437: 430 of cap free
Amount of items: 2
Items: 
Size: 741498 Color: 3
Size: 258073 Color: 1

Bin 438: 431 of cap free
Amount of items: 2
Items: 
Size: 667506 Color: 0
Size: 332064 Color: 3

Bin 439: 436 of cap free
Amount of items: 2
Items: 
Size: 625964 Color: 4
Size: 373601 Color: 3

Bin 440: 438 of cap free
Amount of items: 2
Items: 
Size: 502104 Color: 1
Size: 497459 Color: 4

Bin 441: 438 of cap free
Amount of items: 2
Items: 
Size: 568170 Color: 3
Size: 431393 Color: 4

Bin 442: 438 of cap free
Amount of items: 2
Items: 
Size: 795882 Color: 0
Size: 203681 Color: 3

Bin 443: 442 of cap free
Amount of items: 2
Items: 
Size: 593320 Color: 3
Size: 406239 Color: 1

Bin 444: 444 of cap free
Amount of items: 2
Items: 
Size: 562047 Color: 1
Size: 437510 Color: 3

Bin 445: 445 of cap free
Amount of items: 2
Items: 
Size: 640748 Color: 0
Size: 358808 Color: 4

Bin 446: 447 of cap free
Amount of items: 2
Items: 
Size: 503102 Color: 1
Size: 496452 Color: 0

Bin 447: 449 of cap free
Amount of items: 2
Items: 
Size: 624440 Color: 2
Size: 375112 Color: 0

Bin 448: 451 of cap free
Amount of items: 2
Items: 
Size: 731884 Color: 0
Size: 267666 Color: 3

Bin 449: 456 of cap free
Amount of items: 2
Items: 
Size: 522266 Color: 1
Size: 477279 Color: 0

Bin 450: 463 of cap free
Amount of items: 2
Items: 
Size: 506583 Color: 0
Size: 492955 Color: 1

Bin 451: 463 of cap free
Amount of items: 2
Items: 
Size: 659881 Color: 0
Size: 339657 Color: 4

Bin 452: 463 of cap free
Amount of items: 2
Items: 
Size: 742422 Color: 4
Size: 257116 Color: 0

Bin 453: 463 of cap free
Amount of items: 2
Items: 
Size: 752180 Color: 0
Size: 247358 Color: 2

Bin 454: 464 of cap free
Amount of items: 2
Items: 
Size: 602623 Color: 4
Size: 396914 Color: 0

Bin 455: 467 of cap free
Amount of items: 2
Items: 
Size: 574639 Color: 3
Size: 424895 Color: 4

Bin 456: 479 of cap free
Amount of items: 2
Items: 
Size: 557600 Color: 4
Size: 441922 Color: 2

Bin 457: 480 of cap free
Amount of items: 2
Items: 
Size: 760207 Color: 0
Size: 239314 Color: 2

Bin 458: 484 of cap free
Amount of items: 2
Items: 
Size: 673116 Color: 0
Size: 326401 Color: 1

Bin 459: 486 of cap free
Amount of items: 2
Items: 
Size: 653076 Color: 0
Size: 346439 Color: 2

Bin 460: 488 of cap free
Amount of items: 2
Items: 
Size: 682825 Color: 2
Size: 316688 Color: 0

Bin 461: 491 of cap free
Amount of items: 2
Items: 
Size: 797164 Color: 0
Size: 202346 Color: 3

Bin 462: 494 of cap free
Amount of items: 2
Items: 
Size: 531348 Color: 0
Size: 468159 Color: 1

Bin 463: 503 of cap free
Amount of items: 2
Items: 
Size: 777539 Color: 3
Size: 221959 Color: 0

Bin 464: 510 of cap free
Amount of items: 2
Items: 
Size: 606640 Color: 2
Size: 392851 Color: 4

Bin 465: 511 of cap free
Amount of items: 2
Items: 
Size: 644058 Color: 0
Size: 355432 Color: 4

Bin 466: 516 of cap free
Amount of items: 2
Items: 
Size: 555393 Color: 2
Size: 444092 Color: 1

Bin 467: 522 of cap free
Amount of items: 2
Items: 
Size: 594176 Color: 2
Size: 405303 Color: 1

Bin 468: 525 of cap free
Amount of items: 2
Items: 
Size: 654368 Color: 4
Size: 345108 Color: 3

Bin 469: 528 of cap free
Amount of items: 2
Items: 
Size: 786795 Color: 1
Size: 212678 Color: 4

Bin 470: 534 of cap free
Amount of items: 2
Items: 
Size: 510887 Color: 2
Size: 488580 Color: 3

Bin 471: 535 of cap free
Amount of items: 2
Items: 
Size: 674185 Color: 0
Size: 325281 Color: 2

Bin 472: 546 of cap free
Amount of items: 2
Items: 
Size: 646336 Color: 4
Size: 353119 Color: 1

Bin 473: 547 of cap free
Amount of items: 2
Items: 
Size: 784506 Color: 1
Size: 214948 Color: 0

Bin 474: 553 of cap free
Amount of items: 2
Items: 
Size: 678973 Color: 2
Size: 320475 Color: 4

Bin 475: 566 of cap free
Amount of items: 2
Items: 
Size: 710566 Color: 0
Size: 288869 Color: 2

Bin 476: 568 of cap free
Amount of items: 3
Items: 
Size: 445318 Color: 0
Size: 438945 Color: 1
Size: 115170 Color: 0

Bin 477: 573 of cap free
Amount of items: 2
Items: 
Size: 592365 Color: 0
Size: 407063 Color: 3

Bin 478: 575 of cap free
Amount of items: 2
Items: 
Size: 645484 Color: 4
Size: 353942 Color: 3

Bin 479: 576 of cap free
Amount of items: 2
Items: 
Size: 530335 Color: 1
Size: 469090 Color: 3

Bin 480: 576 of cap free
Amount of items: 2
Items: 
Size: 776128 Color: 1
Size: 223297 Color: 2

Bin 481: 586 of cap free
Amount of items: 2
Items: 
Size: 600831 Color: 4
Size: 398584 Color: 0

Bin 482: 591 of cap free
Amount of items: 3
Items: 
Size: 433139 Color: 1
Size: 432753 Color: 0
Size: 133518 Color: 3

Bin 483: 591 of cap free
Amount of items: 3
Items: 
Size: 448617 Color: 4
Size: 448476 Color: 0
Size: 102317 Color: 3

Bin 484: 591 of cap free
Amount of items: 2
Items: 
Size: 781049 Color: 4
Size: 218361 Color: 1

Bin 485: 593 of cap free
Amount of items: 2
Items: 
Size: 744803 Color: 0
Size: 254605 Color: 4

Bin 486: 596 of cap free
Amount of items: 6
Items: 
Size: 176843 Color: 2
Size: 176308 Color: 0
Size: 175710 Color: 2
Size: 175417 Color: 3
Size: 175084 Color: 2
Size: 120043 Color: 1

Bin 487: 597 of cap free
Amount of items: 2
Items: 
Size: 577940 Color: 0
Size: 421464 Color: 1

Bin 488: 598 of cap free
Amount of items: 2
Items: 
Size: 650762 Color: 0
Size: 348641 Color: 2

Bin 489: 603 of cap free
Amount of items: 2
Items: 
Size: 668626 Color: 4
Size: 330772 Color: 1

Bin 490: 609 of cap free
Amount of items: 3
Items: 
Size: 447827 Color: 1
Size: 447349 Color: 2
Size: 104216 Color: 4

Bin 491: 610 of cap free
Amount of items: 2
Items: 
Size: 732244 Color: 3
Size: 267147 Color: 4

Bin 492: 612 of cap free
Amount of items: 2
Items: 
Size: 508435 Color: 0
Size: 490954 Color: 2

Bin 493: 612 of cap free
Amount of items: 2
Items: 
Size: 743979 Color: 2
Size: 255410 Color: 1

Bin 494: 613 of cap free
Amount of items: 2
Items: 
Size: 713003 Color: 4
Size: 286385 Color: 2

Bin 495: 615 of cap free
Amount of items: 2
Items: 
Size: 697391 Color: 2
Size: 301995 Color: 1

Bin 496: 617 of cap free
Amount of items: 2
Items: 
Size: 779802 Color: 4
Size: 219582 Color: 0

Bin 497: 620 of cap free
Amount of items: 2
Items: 
Size: 581903 Color: 0
Size: 417478 Color: 4

Bin 498: 626 of cap free
Amount of items: 2
Items: 
Size: 771224 Color: 1
Size: 228151 Color: 0

Bin 499: 628 of cap free
Amount of items: 2
Items: 
Size: 722437 Color: 0
Size: 276936 Color: 3

Bin 500: 637 of cap free
Amount of items: 2
Items: 
Size: 781125 Color: 1
Size: 218239 Color: 4

Bin 501: 638 of cap free
Amount of items: 2
Items: 
Size: 503097 Color: 0
Size: 496266 Color: 1

Bin 502: 638 of cap free
Amount of items: 2
Items: 
Size: 596294 Color: 4
Size: 403069 Color: 3

Bin 503: 639 of cap free
Amount of items: 2
Items: 
Size: 791620 Color: 3
Size: 207742 Color: 4

Bin 504: 659 of cap free
Amount of items: 2
Items: 
Size: 705674 Color: 3
Size: 293668 Color: 0

Bin 505: 663 of cap free
Amount of items: 2
Items: 
Size: 571172 Color: 1
Size: 428166 Color: 2

Bin 506: 665 of cap free
Amount of items: 3
Items: 
Size: 338770 Color: 1
Size: 338325 Color: 4
Size: 322241 Color: 1

Bin 507: 665 of cap free
Amount of items: 2
Items: 
Size: 777507 Color: 3
Size: 221829 Color: 4

Bin 508: 668 of cap free
Amount of items: 2
Items: 
Size: 612688 Color: 2
Size: 386645 Color: 1

Bin 509: 670 of cap free
Amount of items: 2
Items: 
Size: 543982 Color: 3
Size: 455349 Color: 4

Bin 510: 673 of cap free
Amount of items: 2
Items: 
Size: 557533 Color: 4
Size: 441795 Color: 1

Bin 511: 678 of cap free
Amount of items: 2
Items: 
Size: 513357 Color: 2
Size: 485966 Color: 1

Bin 512: 681 of cap free
Amount of items: 2
Items: 
Size: 601646 Color: 1
Size: 397674 Color: 2

Bin 513: 687 of cap free
Amount of items: 2
Items: 
Size: 551855 Color: 1
Size: 447459 Color: 3

Bin 514: 687 of cap free
Amount of items: 2
Items: 
Size: 725778 Color: 2
Size: 273536 Color: 0

Bin 515: 691 of cap free
Amount of items: 2
Items: 
Size: 705671 Color: 3
Size: 293639 Color: 0

Bin 516: 692 of cap free
Amount of items: 2
Items: 
Size: 795752 Color: 0
Size: 203557 Color: 3

Bin 517: 696 of cap free
Amount of items: 2
Items: 
Size: 520791 Color: 0
Size: 478514 Color: 1

Bin 518: 705 of cap free
Amount of items: 2
Items: 
Size: 694182 Color: 1
Size: 305114 Color: 4

Bin 519: 707 of cap free
Amount of items: 2
Items: 
Size: 795020 Color: 3
Size: 204274 Color: 2

Bin 520: 710 of cap free
Amount of items: 2
Items: 
Size: 563052 Color: 4
Size: 436239 Color: 3

Bin 521: 712 of cap free
Amount of items: 2
Items: 
Size: 670541 Color: 4
Size: 328748 Color: 1

Bin 522: 715 of cap free
Amount of items: 2
Items: 
Size: 512147 Color: 1
Size: 487139 Color: 2

Bin 523: 717 of cap free
Amount of items: 2
Items: 
Size: 715526 Color: 2
Size: 283758 Color: 0

Bin 524: 717 of cap free
Amount of items: 2
Items: 
Size: 748461 Color: 4
Size: 250823 Color: 1

Bin 525: 724 of cap free
Amount of items: 2
Items: 
Size: 699022 Color: 4
Size: 300255 Color: 3

Bin 526: 728 of cap free
Amount of items: 2
Items: 
Size: 516038 Color: 1
Size: 483235 Color: 3

Bin 527: 729 of cap free
Amount of items: 2
Items: 
Size: 798310 Color: 3
Size: 200962 Color: 2

Bin 528: 732 of cap free
Amount of items: 2
Items: 
Size: 677090 Color: 4
Size: 322179 Color: 3

Bin 529: 732 of cap free
Amount of items: 2
Items: 
Size: 733680 Color: 1
Size: 265589 Color: 4

Bin 530: 738 of cap free
Amount of items: 2
Items: 
Size: 594334 Color: 3
Size: 404929 Color: 2

Bin 531: 740 of cap free
Amount of items: 2
Items: 
Size: 519950 Color: 2
Size: 479311 Color: 3

Bin 532: 741 of cap free
Amount of items: 2
Items: 
Size: 555081 Color: 0
Size: 444179 Color: 2

Bin 533: 742 of cap free
Amount of items: 2
Items: 
Size: 794039 Color: 4
Size: 205220 Color: 0

Bin 534: 745 of cap free
Amount of items: 2
Items: 
Size: 602425 Color: 3
Size: 396831 Color: 1

Bin 535: 758 of cap free
Amount of items: 2
Items: 
Size: 555251 Color: 2
Size: 443992 Color: 4

Bin 536: 761 of cap free
Amount of items: 2
Items: 
Size: 763671 Color: 1
Size: 235569 Color: 2

Bin 537: 767 of cap free
Amount of items: 2
Items: 
Size: 574486 Color: 2
Size: 424748 Color: 4

Bin 538: 771 of cap free
Amount of items: 5
Items: 
Size: 213536 Color: 2
Size: 211594 Color: 0
Size: 211413 Color: 0
Size: 209747 Color: 2
Size: 152940 Color: 3

Bin 539: 775 of cap free
Amount of items: 2
Items: 
Size: 532696 Color: 4
Size: 466530 Color: 3

Bin 540: 779 of cap free
Amount of items: 2
Items: 
Size: 541990 Color: 2
Size: 457232 Color: 1

Bin 541: 791 of cap free
Amount of items: 2
Items: 
Size: 594030 Color: 4
Size: 405180 Color: 1

Bin 542: 795 of cap free
Amount of items: 2
Items: 
Size: 506404 Color: 3
Size: 492802 Color: 2

Bin 543: 798 of cap free
Amount of items: 2
Items: 
Size: 726701 Color: 0
Size: 272502 Color: 4

Bin 544: 805 of cap free
Amount of items: 2
Items: 
Size: 614860 Color: 3
Size: 384336 Color: 4

Bin 545: 816 of cap free
Amount of items: 2
Items: 
Size: 531140 Color: 0
Size: 468045 Color: 2

Bin 546: 821 of cap free
Amount of items: 3
Items: 
Size: 426554 Color: 2
Size: 377367 Color: 3
Size: 195259 Color: 0

Bin 547: 822 of cap free
Amount of items: 2
Items: 
Size: 709007 Color: 2
Size: 290172 Color: 1

Bin 548: 828 of cap free
Amount of items: 2
Items: 
Size: 700894 Color: 2
Size: 298279 Color: 0

Bin 549: 831 of cap free
Amount of items: 2
Items: 
Size: 616298 Color: 2
Size: 382872 Color: 4

Bin 550: 835 of cap free
Amount of items: 2
Items: 
Size: 571863 Color: 2
Size: 427303 Color: 3

Bin 551: 836 of cap free
Amount of items: 2
Items: 
Size: 654271 Color: 0
Size: 344894 Color: 3

Bin 552: 844 of cap free
Amount of items: 2
Items: 
Size: 539554 Color: 0
Size: 459603 Color: 3

Bin 553: 847 of cap free
Amount of items: 2
Items: 
Size: 711719 Color: 2
Size: 287435 Color: 3

Bin 554: 852 of cap free
Amount of items: 2
Items: 
Size: 656167 Color: 3
Size: 342982 Color: 4

Bin 555: 862 of cap free
Amount of items: 2
Items: 
Size: 693351 Color: 0
Size: 305788 Color: 1

Bin 556: 882 of cap free
Amount of items: 2
Items: 
Size: 759611 Color: 2
Size: 239508 Color: 0

Bin 557: 882 of cap free
Amount of items: 2
Items: 
Size: 780845 Color: 4
Size: 218274 Color: 1

Bin 558: 891 of cap free
Amount of items: 2
Items: 
Size: 773325 Color: 1
Size: 225785 Color: 2

Bin 559: 892 of cap free
Amount of items: 2
Items: 
Size: 624836 Color: 0
Size: 374273 Color: 1

Bin 560: 892 of cap free
Amount of items: 2
Items: 
Size: 700084 Color: 0
Size: 299025 Color: 2

Bin 561: 894 of cap free
Amount of items: 2
Items: 
Size: 505428 Color: 4
Size: 493679 Color: 3

Bin 562: 895 of cap free
Amount of items: 2
Items: 
Size: 607543 Color: 2
Size: 391563 Color: 1

Bin 563: 895 of cap free
Amount of items: 2
Items: 
Size: 626930 Color: 1
Size: 372176 Color: 4

Bin 564: 903 of cap free
Amount of items: 2
Items: 
Size: 680701 Color: 4
Size: 318397 Color: 3

Bin 565: 907 of cap free
Amount of items: 2
Items: 
Size: 791419 Color: 4
Size: 207675 Color: 1

Bin 566: 917 of cap free
Amount of items: 2
Items: 
Size: 528712 Color: 4
Size: 470372 Color: 2

Bin 567: 919 of cap free
Amount of items: 4
Items: 
Size: 297503 Color: 2
Size: 281514 Color: 1
Size: 270724 Color: 2
Size: 149341 Color: 4

Bin 568: 926 of cap free
Amount of items: 2
Items: 
Size: 673881 Color: 4
Size: 325194 Color: 1

Bin 569: 927 of cap free
Amount of items: 6
Items: 
Size: 179938 Color: 0
Size: 179046 Color: 3
Size: 179166 Color: 0
Size: 178723 Color: 3
Size: 178308 Color: 0
Size: 103893 Color: 3

Bin 570: 931 of cap free
Amount of items: 2
Items: 
Size: 575184 Color: 4
Size: 423886 Color: 1

Bin 571: 942 of cap free
Amount of items: 2
Items: 
Size: 762209 Color: 3
Size: 236850 Color: 4

Bin 572: 947 of cap free
Amount of items: 2
Items: 
Size: 676912 Color: 4
Size: 322142 Color: 3

Bin 573: 948 of cap free
Amount of items: 2
Items: 
Size: 662564 Color: 2
Size: 336489 Color: 4

Bin 574: 953 of cap free
Amount of items: 2
Items: 
Size: 632780 Color: 3
Size: 366268 Color: 2

Bin 575: 967 of cap free
Amount of items: 4
Items: 
Size: 301957 Color: 3
Size: 298499 Color: 1
Size: 298193 Color: 3
Size: 100385 Color: 0

Bin 576: 982 of cap free
Amount of items: 2
Items: 
Size: 577570 Color: 3
Size: 421449 Color: 4

Bin 577: 982 of cap free
Amount of items: 2
Items: 
Size: 772735 Color: 0
Size: 226284 Color: 4

Bin 578: 983 of cap free
Amount of items: 6
Items: 
Size: 167522 Color: 1
Size: 168045 Color: 3
Size: 167120 Color: 4
Size: 167042 Color: 3
Size: 166079 Color: 0
Size: 163210 Color: 4

Bin 579: 992 of cap free
Amount of items: 2
Items: 
Size: 627871 Color: 4
Size: 371138 Color: 2

Bin 580: 998 of cap free
Amount of items: 2
Items: 
Size: 616167 Color: 2
Size: 382836 Color: 4

Bin 581: 1004 of cap free
Amount of items: 2
Items: 
Size: 540979 Color: 4
Size: 458018 Color: 2

Bin 582: 1020 of cap free
Amount of items: 2
Items: 
Size: 557283 Color: 2
Size: 441698 Color: 4

Bin 583: 1026 of cap free
Amount of items: 2
Items: 
Size: 756033 Color: 0
Size: 242942 Color: 4

Bin 584: 1033 of cap free
Amount of items: 2
Items: 
Size: 635186 Color: 1
Size: 363782 Color: 3

Bin 585: 1035 of cap free
Amount of items: 2
Items: 
Size: 523968 Color: 4
Size: 474998 Color: 1

Bin 586: 1037 of cap free
Amount of items: 2
Items: 
Size: 759894 Color: 0
Size: 239070 Color: 4

Bin 587: 1048 of cap free
Amount of items: 2
Items: 
Size: 633045 Color: 3
Size: 365908 Color: 4

Bin 588: 1051 of cap free
Amount of items: 2
Items: 
Size: 508302 Color: 2
Size: 490648 Color: 1

Bin 589: 1051 of cap free
Amount of items: 2
Items: 
Size: 643903 Color: 1
Size: 355047 Color: 4

Bin 590: 1058 of cap free
Amount of items: 2
Items: 
Size: 600461 Color: 1
Size: 398482 Color: 4

Bin 591: 1070 of cap free
Amount of items: 2
Items: 
Size: 736457 Color: 2
Size: 262474 Color: 1

Bin 592: 1071 of cap free
Amount of items: 2
Items: 
Size: 567532 Color: 4
Size: 431398 Color: 3

Bin 593: 1083 of cap free
Amount of items: 2
Items: 
Size: 534279 Color: 3
Size: 464639 Color: 0

Bin 594: 1093 of cap free
Amount of items: 2
Items: 
Size: 526021 Color: 4
Size: 472887 Color: 2

Bin 595: 1099 of cap free
Amount of items: 3
Items: 
Size: 445558 Color: 1
Size: 445386 Color: 2
Size: 107958 Color: 2

Bin 596: 1103 of cap free
Amount of items: 2
Items: 
Size: 729470 Color: 3
Size: 269428 Color: 2

Bin 597: 1114 of cap free
Amount of items: 2
Items: 
Size: 673715 Color: 0
Size: 325172 Color: 4

Bin 598: 1120 of cap free
Amount of items: 2
Items: 
Size: 778256 Color: 1
Size: 220625 Color: 2

Bin 599: 1129 of cap free
Amount of items: 2
Items: 
Size: 553146 Color: 0
Size: 445726 Color: 1

Bin 600: 1151 of cap free
Amount of items: 2
Items: 
Size: 569437 Color: 0
Size: 429413 Color: 2

Bin 601: 1161 of cap free
Amount of items: 7
Items: 
Size: 149064 Color: 2
Size: 146649 Color: 0
Size: 147035 Color: 2
Size: 146582 Color: 0
Size: 146561 Color: 0
Size: 145918 Color: 4
Size: 117031 Color: 3

Bin 602: 1167 of cap free
Amount of items: 2
Items: 
Size: 549093 Color: 1
Size: 449741 Color: 3

Bin 603: 1176 of cap free
Amount of items: 6
Items: 
Size: 173087 Color: 1
Size: 173076 Color: 2
Size: 172954 Color: 0
Size: 172733 Color: 2
Size: 171732 Color: 1
Size: 135243 Color: 2

Bin 604: 1180 of cap free
Amount of items: 2
Items: 
Size: 788732 Color: 3
Size: 210089 Color: 4

Bin 605: 1181 of cap free
Amount of items: 2
Items: 
Size: 602519 Color: 1
Size: 396301 Color: 3

Bin 606: 1187 of cap free
Amount of items: 2
Items: 
Size: 611077 Color: 4
Size: 387737 Color: 2

Bin 607: 1189 of cap free
Amount of items: 2
Items: 
Size: 709958 Color: 2
Size: 288854 Color: 1

Bin 608: 1192 of cap free
Amount of items: 2
Items: 
Size: 562818 Color: 3
Size: 435991 Color: 1

Bin 609: 1194 of cap free
Amount of items: 2
Items: 
Size: 666166 Color: 1
Size: 332641 Color: 4

Bin 610: 1204 of cap free
Amount of items: 2
Items: 
Size: 596174 Color: 2
Size: 402623 Color: 4

Bin 611: 1212 of cap free
Amount of items: 2
Items: 
Size: 698607 Color: 1
Size: 300182 Color: 4

Bin 612: 1223 of cap free
Amount of items: 2
Items: 
Size: 650698 Color: 3
Size: 348080 Color: 4

Bin 613: 1227 of cap free
Amount of items: 2
Items: 
Size: 555011 Color: 2
Size: 443763 Color: 3

Bin 614: 1233 of cap free
Amount of items: 2
Items: 
Size: 668541 Color: 1
Size: 330227 Color: 4

Bin 615: 1255 of cap free
Amount of items: 2
Items: 
Size: 682435 Color: 3
Size: 316311 Color: 1

Bin 616: 1265 of cap free
Amount of items: 2
Items: 
Size: 678443 Color: 4
Size: 320293 Color: 1

Bin 617: 1280 of cap free
Amount of items: 2
Items: 
Size: 654242 Color: 3
Size: 344479 Color: 1

Bin 618: 1285 of cap free
Amount of items: 2
Items: 
Size: 506402 Color: 4
Size: 492314 Color: 1

Bin 619: 1286 of cap free
Amount of items: 2
Items: 
Size: 569398 Color: 1
Size: 429317 Color: 2

Bin 620: 1302 of cap free
Amount of items: 2
Items: 
Size: 654229 Color: 2
Size: 344470 Color: 0

Bin 621: 1310 of cap free
Amount of items: 2
Items: 
Size: 715150 Color: 4
Size: 283541 Color: 0

Bin 622: 1330 of cap free
Amount of items: 2
Items: 
Size: 696598 Color: 3
Size: 302073 Color: 2

Bin 623: 1338 of cap free
Amount of items: 2
Items: 
Size: 552956 Color: 0
Size: 445707 Color: 4

Bin 624: 1338 of cap free
Amount of items: 2
Items: 
Size: 715183 Color: 2
Size: 283480 Color: 1

Bin 625: 1348 of cap free
Amount of items: 2
Items: 
Size: 771130 Color: 2
Size: 227523 Color: 3

Bin 626: 1356 of cap free
Amount of items: 2
Items: 
Size: 763599 Color: 4
Size: 235046 Color: 2

Bin 627: 1374 of cap free
Amount of items: 2
Items: 
Size: 698583 Color: 1
Size: 300044 Color: 4

Bin 628: 1398 of cap free
Amount of items: 2
Items: 
Size: 791029 Color: 3
Size: 207574 Color: 0

Bin 629: 1399 of cap free
Amount of items: 2
Items: 
Size: 691115 Color: 3
Size: 307487 Color: 0

Bin 630: 1400 of cap free
Amount of items: 2
Items: 
Size: 676413 Color: 3
Size: 322188 Color: 4

Bin 631: 1446 of cap free
Amount of items: 2
Items: 
Size: 748213 Color: 2
Size: 250342 Color: 4

Bin 632: 1450 of cap free
Amount of items: 2
Items: 
Size: 624334 Color: 4
Size: 374217 Color: 1

Bin 633: 1463 of cap free
Amount of items: 2
Items: 
Size: 607544 Color: 1
Size: 390994 Color: 0

Bin 634: 1486 of cap free
Amount of items: 2
Items: 
Size: 769305 Color: 1
Size: 229210 Color: 0

Bin 635: 1488 of cap free
Amount of items: 2
Items: 
Size: 587133 Color: 3
Size: 411380 Color: 1

Bin 636: 1511 of cap free
Amount of items: 2
Items: 
Size: 726680 Color: 0
Size: 271810 Color: 4

Bin 637: 1518 of cap free
Amount of items: 2
Items: 
Size: 748212 Color: 4
Size: 250271 Color: 2

Bin 638: 1520 of cap free
Amount of items: 2
Items: 
Size: 523763 Color: 4
Size: 474718 Color: 1

Bin 639: 1537 of cap free
Amount of items: 5
Items: 
Size: 213837 Color: 2
Size: 211965 Color: 1
Size: 211709 Color: 2
Size: 211757 Color: 1
Size: 149196 Color: 1

Bin 640: 1557 of cap free
Amount of items: 2
Items: 
Size: 678410 Color: 4
Size: 320034 Color: 0

Bin 641: 1562 of cap free
Amount of items: 2
Items: 
Size: 538908 Color: 2
Size: 459531 Color: 0

Bin 642: 1566 of cap free
Amount of items: 2
Items: 
Size: 523779 Color: 4
Size: 474656 Color: 0

Bin 643: 1570 of cap free
Amount of items: 2
Items: 
Size: 571164 Color: 0
Size: 427267 Color: 4

Bin 644: 1571 of cap free
Amount of items: 2
Items: 
Size: 534237 Color: 1
Size: 464193 Color: 4

Bin 645: 1579 of cap free
Amount of items: 2
Items: 
Size: 670461 Color: 1
Size: 327961 Color: 4

Bin 646: 1589 of cap free
Amount of items: 2
Items: 
Size: 761472 Color: 3
Size: 236940 Color: 2

Bin 647: 1591 of cap free
Amount of items: 2
Items: 
Size: 638602 Color: 3
Size: 359808 Color: 2

Bin 648: 1591 of cap free
Amount of items: 2
Items: 
Size: 668325 Color: 2
Size: 330085 Color: 4

Bin 649: 1593 of cap free
Amount of items: 2
Items: 
Size: 661944 Color: 0
Size: 336464 Color: 2

Bin 650: 1622 of cap free
Amount of items: 2
Items: 
Size: 709873 Color: 0
Size: 288506 Color: 3

Bin 651: 1627 of cap free
Amount of items: 2
Items: 
Size: 703817 Color: 4
Size: 294557 Color: 2

Bin 652: 1641 of cap free
Amount of items: 2
Items: 
Size: 744166 Color: 1
Size: 254194 Color: 2

Bin 653: 1656 of cap free
Amount of items: 2
Items: 
Size: 690267 Color: 0
Size: 308078 Color: 3

Bin 654: 1656 of cap free
Amount of items: 2
Items: 
Size: 790996 Color: 4
Size: 207349 Color: 2

Bin 655: 1670 of cap free
Amount of items: 2
Items: 
Size: 619529 Color: 2
Size: 378802 Color: 1

Bin 656: 1741 of cap free
Amount of items: 2
Items: 
Size: 554735 Color: 4
Size: 443525 Color: 1

Bin 657: 1759 of cap free
Amount of items: 2
Items: 
Size: 534170 Color: 4
Size: 464072 Color: 1

Bin 658: 1760 of cap free
Amount of items: 2
Items: 
Size: 748003 Color: 4
Size: 250238 Color: 1

Bin 659: 1768 of cap free
Amount of items: 2
Items: 
Size: 585228 Color: 4
Size: 413005 Color: 3

Bin 660: 1799 of cap free
Amount of items: 2
Items: 
Size: 543601 Color: 2
Size: 454601 Color: 0

Bin 661: 1811 of cap free
Amount of items: 2
Items: 
Size: 596176 Color: 4
Size: 402014 Color: 3

Bin 662: 1834 of cap free
Amount of items: 2
Items: 
Size: 574332 Color: 2
Size: 423835 Color: 4

Bin 663: 1854 of cap free
Amount of items: 2
Items: 
Size: 634728 Color: 4
Size: 363419 Color: 2

Bin 664: 1862 of cap free
Amount of items: 2
Items: 
Size: 766541 Color: 4
Size: 231598 Color: 1

Bin 665: 1885 of cap free
Amount of items: 2
Items: 
Size: 627671 Color: 4
Size: 370445 Color: 0

Bin 666: 1892 of cap free
Amount of items: 2
Items: 
Size: 500764 Color: 2
Size: 497345 Color: 4

Bin 667: 1911 of cap free
Amount of items: 2
Items: 
Size: 670446 Color: 3
Size: 327644 Color: 2

Bin 668: 1915 of cap free
Amount of items: 2
Items: 
Size: 668004 Color: 1
Size: 330082 Color: 2

Bin 669: 1922 of cap free
Amount of items: 2
Items: 
Size: 772846 Color: 4
Size: 225233 Color: 0

Bin 670: 1924 of cap free
Amount of items: 2
Items: 
Size: 619461 Color: 0
Size: 378616 Color: 2

Bin 671: 1933 of cap free
Amount of items: 2
Items: 
Size: 600369 Color: 0
Size: 397699 Color: 4

Bin 672: 1939 of cap free
Amount of items: 2
Items: 
Size: 661767 Color: 3
Size: 336295 Color: 2

Bin 673: 1944 of cap free
Amount of items: 2
Items: 
Size: 584668 Color: 3
Size: 413389 Color: 2

Bin 674: 1971 of cap free
Amount of items: 2
Items: 
Size: 607386 Color: 4
Size: 390644 Color: 3

Bin 675: 1992 of cap free
Amount of items: 2
Items: 
Size: 687491 Color: 3
Size: 310518 Color: 2

Bin 676: 2007 of cap free
Amount of items: 2
Items: 
Size: 797159 Color: 4
Size: 200835 Color: 0

Bin 677: 2022 of cap free
Amount of items: 2
Items: 
Size: 720800 Color: 4
Size: 277179 Color: 0

Bin 678: 2026 of cap free
Amount of items: 2
Items: 
Size: 543072 Color: 1
Size: 454903 Color: 2

Bin 679: 2036 of cap free
Amount of items: 2
Items: 
Size: 580912 Color: 4
Size: 417053 Color: 2

Bin 680: 2048 of cap free
Amount of items: 2
Items: 
Size: 643351 Color: 3
Size: 354602 Color: 1

Bin 681: 2084 of cap free
Amount of items: 2
Items: 
Size: 681735 Color: 2
Size: 316182 Color: 3

Bin 682: 2086 of cap free
Amount of items: 2
Items: 
Size: 772231 Color: 3
Size: 225684 Color: 4

Bin 683: 2087 of cap free
Amount of items: 2
Items: 
Size: 649546 Color: 1
Size: 348368 Color: 2

Bin 684: 2111 of cap free
Amount of items: 2
Items: 
Size: 759270 Color: 4
Size: 238620 Color: 2

Bin 685: 2208 of cap free
Amount of items: 2
Items: 
Size: 556626 Color: 3
Size: 441167 Color: 2

Bin 686: 2209 of cap free
Amount of items: 2
Items: 
Size: 519891 Color: 2
Size: 477901 Color: 3

Bin 687: 2209 of cap free
Amount of items: 2
Items: 
Size: 739235 Color: 3
Size: 258557 Color: 4

Bin 688: 2262 of cap free
Amount of items: 2
Items: 
Size: 534215 Color: 1
Size: 463524 Color: 0

Bin 689: 2270 of cap free
Amount of items: 2
Items: 
Size: 581358 Color: 2
Size: 416373 Color: 0

Bin 690: 2275 of cap free
Amount of items: 2
Items: 
Size: 584366 Color: 1
Size: 413360 Color: 2

Bin 691: 2277 of cap free
Amount of items: 2
Items: 
Size: 748051 Color: 1
Size: 249673 Color: 3

Bin 692: 2280 of cap free
Amount of items: 2
Items: 
Size: 799691 Color: 0
Size: 198030 Color: 1

Bin 693: 2320 of cap free
Amount of items: 2
Items: 
Size: 537443 Color: 3
Size: 460238 Color: 1

Bin 694: 2328 of cap free
Amount of items: 2
Items: 
Size: 614247 Color: 3
Size: 383426 Color: 2

Bin 695: 2347 of cap free
Amount of items: 2
Items: 
Size: 735761 Color: 4
Size: 261893 Color: 2

Bin 696: 2370 of cap free
Amount of items: 2
Items: 
Size: 637912 Color: 4
Size: 359719 Color: 3

Bin 697: 2428 of cap free
Amount of items: 2
Items: 
Size: 790835 Color: 4
Size: 206738 Color: 3

Bin 698: 2464 of cap free
Amount of items: 2
Items: 
Size: 561999 Color: 3
Size: 435538 Color: 1

Bin 699: 2479 of cap free
Amount of items: 2
Items: 
Size: 607475 Color: 3
Size: 390047 Color: 2

Bin 700: 2512 of cap free
Amount of items: 2
Items: 
Size: 661756 Color: 2
Size: 335733 Color: 3

Bin 701: 2520 of cap free
Amount of items: 2
Items: 
Size: 618909 Color: 1
Size: 378572 Color: 4

Bin 702: 2533 of cap free
Amount of items: 2
Items: 
Size: 779356 Color: 2
Size: 218112 Color: 3

Bin 703: 2542 of cap free
Amount of items: 2
Items: 
Size: 799898 Color: 3
Size: 197561 Color: 4

Bin 704: 2568 of cap free
Amount of items: 2
Items: 
Size: 543014 Color: 2
Size: 454419 Color: 4

Bin 705: 2598 of cap free
Amount of items: 2
Items: 
Size: 675650 Color: 4
Size: 321753 Color: 0

Bin 706: 2618 of cap free
Amount of items: 2
Items: 
Size: 642839 Color: 4
Size: 354544 Color: 0

Bin 707: 2647 of cap free
Amount of items: 2
Items: 
Size: 714996 Color: 0
Size: 282358 Color: 1

Bin 708: 2673 of cap free
Amount of items: 2
Items: 
Size: 519574 Color: 2
Size: 477754 Color: 0

Bin 709: 2691 of cap free
Amount of items: 2
Items: 
Size: 533696 Color: 2
Size: 463614 Color: 1

Bin 710: 2733 of cap free
Amount of items: 2
Items: 
Size: 672422 Color: 4
Size: 324846 Color: 3

Bin 711: 2739 of cap free
Amount of items: 2
Items: 
Size: 725249 Color: 4
Size: 272013 Color: 0

Bin 712: 2757 of cap free
Amount of items: 2
Items: 
Size: 790690 Color: 2
Size: 206554 Color: 4

Bin 713: 2782 of cap free
Amount of items: 2
Items: 
Size: 649438 Color: 0
Size: 347781 Color: 1

Bin 714: 2820 of cap free
Amount of items: 2
Items: 
Size: 708845 Color: 4
Size: 288336 Color: 0

Bin 715: 2826 of cap free
Amount of items: 2
Items: 
Size: 714870 Color: 4
Size: 282305 Color: 1

Bin 716: 2889 of cap free
Amount of items: 2
Items: 
Size: 735398 Color: 4
Size: 261714 Color: 0

Bin 717: 2995 of cap free
Amount of items: 2
Items: 
Size: 607340 Color: 4
Size: 389666 Color: 1

Bin 718: 3022 of cap free
Amount of items: 2
Items: 
Size: 720141 Color: 3
Size: 276838 Color: 1

Bin 719: 3139 of cap free
Amount of items: 2
Items: 
Size: 607171 Color: 1
Size: 389691 Color: 3

Bin 720: 3166 of cap free
Amount of items: 2
Items: 
Size: 533596 Color: 1
Size: 463239 Color: 0

Bin 721: 3201 of cap free
Amount of items: 2
Items: 
Size: 515419 Color: 0
Size: 481381 Color: 1

Bin 722: 3212 of cap free
Amount of items: 2
Items: 
Size: 720339 Color: 1
Size: 276450 Color: 4

Bin 723: 3225 of cap free
Amount of items: 2
Items: 
Size: 790324 Color: 2
Size: 206452 Color: 3

Bin 724: 3238 of cap free
Amount of items: 4
Items: 
Size: 281108 Color: 2
Size: 249286 Color: 0
Size: 246989 Color: 4
Size: 219380 Color: 2

Bin 725: 3247 of cap free
Amount of items: 2
Items: 
Size: 738864 Color: 2
Size: 257890 Color: 4

Bin 726: 3252 of cap free
Amount of items: 2
Items: 
Size: 714871 Color: 1
Size: 281878 Color: 0

Bin 727: 3273 of cap free
Amount of items: 2
Items: 
Size: 703755 Color: 3
Size: 292973 Color: 0

Bin 728: 3330 of cap free
Amount of items: 2
Items: 
Size: 652530 Color: 3
Size: 344141 Color: 4

Bin 729: 3349 of cap free
Amount of items: 2
Items: 
Size: 734982 Color: 2
Size: 261670 Color: 1

Bin 730: 3370 of cap free
Amount of items: 2
Items: 
Size: 790121 Color: 2
Size: 206510 Color: 4

Bin 731: 3475 of cap free
Amount of items: 3
Items: 
Size: 368943 Color: 1
Size: 369884 Color: 0
Size: 257699 Color: 0

Bin 732: 3524 of cap free
Amount of items: 2
Items: 
Size: 724958 Color: 0
Size: 271519 Color: 2

Bin 733: 3551 of cap free
Amount of items: 2
Items: 
Size: 661710 Color: 0
Size: 334740 Color: 1

Bin 734: 3651 of cap free
Amount of items: 2
Items: 
Size: 729275 Color: 2
Size: 267075 Color: 4

Bin 735: 3719 of cap free
Amount of items: 2
Items: 
Size: 649148 Color: 0
Size: 347134 Color: 3

Bin 736: 3751 of cap free
Amount of items: 2
Items: 
Size: 583274 Color: 0
Size: 412976 Color: 1

Bin 737: 3814 of cap free
Amount of items: 2
Items: 
Size: 594051 Color: 1
Size: 402136 Color: 4

Bin 738: 3837 of cap free
Amount of items: 2
Items: 
Size: 679993 Color: 4
Size: 316171 Color: 2

Bin 739: 3860 of cap free
Amount of items: 2
Items: 
Size: 703212 Color: 4
Size: 292929 Color: 0

Bin 740: 3956 of cap free
Amount of items: 2
Items: 
Size: 724761 Color: 2
Size: 271284 Color: 0

Bin 741: 3974 of cap free
Amount of items: 2
Items: 
Size: 759240 Color: 3
Size: 236787 Color: 0

Bin 742: 4001 of cap free
Amount of items: 2
Items: 
Size: 719408 Color: 0
Size: 276592 Color: 1

Bin 743: 4019 of cap free
Amount of items: 2
Items: 
Size: 708275 Color: 3
Size: 287707 Color: 0

Bin 744: 4064 of cap free
Amount of items: 2
Items: 
Size: 708581 Color: 0
Size: 287356 Color: 2

Bin 745: 4083 of cap free
Amount of items: 2
Items: 
Size: 591429 Color: 4
Size: 404489 Color: 1

Bin 746: 4229 of cap free
Amount of items: 2
Items: 
Size: 606471 Color: 4
Size: 389301 Color: 1

Bin 747: 4268 of cap free
Amount of items: 7
Items: 
Size: 143250 Color: 4
Size: 142587 Color: 3
Size: 142204 Color: 4
Size: 142146 Color: 2
Size: 141963 Color: 0
Size: 141637 Color: 2
Size: 141946 Color: 0

Bin 748: 4348 of cap free
Amount of items: 2
Items: 
Size: 632665 Color: 1
Size: 362988 Color: 4

Bin 749: 4413 of cap free
Amount of items: 9
Items: 
Size: 111869 Color: 1
Size: 112209 Color: 4
Size: 111864 Color: 1
Size: 110964 Color: 2
Size: 110943 Color: 3
Size: 110890 Color: 1
Size: 109915 Color: 0
Size: 109508 Color: 0
Size: 107426 Color: 4

Bin 750: 4421 of cap free
Amount of items: 2
Items: 
Size: 593822 Color: 1
Size: 401758 Color: 4

Bin 751: 4425 of cap free
Amount of items: 2
Items: 
Size: 758869 Color: 0
Size: 236707 Color: 2

Bin 752: 4449 of cap free
Amount of items: 2
Items: 
Size: 642610 Color: 0
Size: 352942 Color: 1

Bin 753: 4457 of cap free
Amount of items: 2
Items: 
Size: 538513 Color: 1
Size: 457031 Color: 2

Bin 754: 4521 of cap free
Amount of items: 2
Items: 
Size: 724361 Color: 2
Size: 271119 Color: 1

Bin 755: 4607 of cap free
Amount of items: 2
Items: 
Size: 747343 Color: 0
Size: 248051 Color: 1

Bin 756: 4699 of cap free
Amount of items: 2
Items: 
Size: 719196 Color: 2
Size: 276106 Color: 4

Bin 757: 4732 of cap free
Amount of items: 2
Items: 
Size: 574192 Color: 1
Size: 421077 Color: 0

Bin 758: 4762 of cap free
Amount of items: 2
Items: 
Size: 680113 Color: 2
Size: 315126 Color: 3

Bin 759: 4859 of cap free
Amount of items: 2
Items: 
Size: 499198 Color: 2
Size: 495944 Color: 4

Bin 760: 4927 of cap free
Amount of items: 2
Items: 
Size: 776066 Color: 1
Size: 219008 Color: 2

Bin 761: 4931 of cap free
Amount of items: 2
Items: 
Size: 574027 Color: 3
Size: 421043 Color: 1

Bin 762: 4981 of cap free
Amount of items: 2
Items: 
Size: 573988 Color: 1
Size: 421032 Color: 0

Bin 763: 5032 of cap free
Amount of items: 2
Items: 
Size: 738563 Color: 0
Size: 256406 Color: 4

Bin 764: 5033 of cap free
Amount of items: 2
Items: 
Size: 766033 Color: 1
Size: 228935 Color: 3

Bin 765: 5081 of cap free
Amount of items: 2
Items: 
Size: 758391 Color: 2
Size: 236529 Color: 1

Bin 766: 5168 of cap free
Amount of items: 2
Items: 
Size: 573889 Color: 4
Size: 420944 Color: 2

Bin 767: 5201 of cap free
Amount of items: 2
Items: 
Size: 693225 Color: 3
Size: 301575 Color: 2

Bin 768: 5252 of cap free
Amount of items: 2
Items: 
Size: 707623 Color: 0
Size: 287126 Color: 3

Bin 769: 5271 of cap free
Amount of items: 2
Items: 
Size: 708031 Color: 3
Size: 286699 Color: 4

Bin 770: 5379 of cap free
Amount of items: 2
Items: 
Size: 733071 Color: 4
Size: 261551 Color: 2

Bin 771: 5419 of cap free
Amount of items: 2
Items: 
Size: 797133 Color: 2
Size: 197449 Color: 0

Bin 772: 5459 of cap free
Amount of items: 2
Items: 
Size: 797073 Color: 2
Size: 197469 Color: 1

Bin 773: 5608 of cap free
Amount of items: 2
Items: 
Size: 642100 Color: 2
Size: 352293 Color: 0

Bin 774: 5637 of cap free
Amount of items: 2
Items: 
Size: 796996 Color: 4
Size: 197368 Color: 0

Bin 775: 5738 of cap free
Amount of items: 2
Items: 
Size: 605250 Color: 2
Size: 389013 Color: 0

Bin 776: 5768 of cap free
Amount of items: 2
Items: 
Size: 530685 Color: 3
Size: 463548 Color: 1

Bin 777: 6164 of cap free
Amount of items: 2
Items: 
Size: 757137 Color: 3
Size: 236700 Color: 2

Bin 778: 6823 of cap free
Amount of items: 2
Items: 
Size: 649029 Color: 4
Size: 344149 Color: 3

Bin 779: 6924 of cap free
Amount of items: 2
Items: 
Size: 775295 Color: 1
Size: 217782 Color: 2

Bin 780: 6968 of cap free
Amount of items: 2
Items: 
Size: 591269 Color: 4
Size: 401764 Color: 1

Bin 781: 7193 of cap free
Amount of items: 2
Items: 
Size: 580045 Color: 3
Size: 412763 Color: 2

Bin 782: 7202 of cap free
Amount of items: 2
Items: 
Size: 731460 Color: 4
Size: 261339 Color: 0

Bin 783: 7250 of cap free
Amount of items: 6
Items: 
Size: 166860 Color: 3
Size: 165709 Color: 1
Size: 166553 Color: 3
Size: 165099 Color: 0
Size: 164196 Color: 4
Size: 164334 Color: 0

Bin 784: 8014 of cap free
Amount of items: 2
Items: 
Size: 706139 Color: 0
Size: 285848 Color: 3

Bin 785: 8045 of cap free
Amount of items: 2
Items: 
Size: 604363 Color: 4
Size: 387593 Color: 1

Bin 786: 8089 of cap free
Amount of items: 2
Items: 
Size: 649010 Color: 1
Size: 342902 Color: 2

Bin 787: 8126 of cap free
Amount of items: 2
Items: 
Size: 640464 Color: 1
Size: 351411 Color: 4

Bin 788: 8401 of cap free
Amount of items: 2
Items: 
Size: 579940 Color: 1
Size: 411660 Color: 2

Bin 789: 8575 of cap free
Amount of items: 2
Items: 
Size: 756386 Color: 4
Size: 235040 Color: 3

Bin 790: 8733 of cap free
Amount of items: 2
Items: 
Size: 537531 Color: 1
Size: 453737 Color: 0

Bin 791: 9214 of cap free
Amount of items: 2
Items: 
Size: 496092 Color: 2
Size: 494695 Color: 0

Bin 792: 9285 of cap free
Amount of items: 2
Items: 
Size: 648933 Color: 4
Size: 341783 Color: 0

Bin 793: 9961 of cap free
Amount of items: 2
Items: 
Size: 495747 Color: 2
Size: 494293 Color: 4

Bin 794: 10090 of cap free
Amount of items: 2
Items: 
Size: 772182 Color: 3
Size: 217729 Color: 4

Bin 795: 10101 of cap free
Amount of items: 2
Items: 
Size: 795070 Color: 2
Size: 194830 Color: 1

Bin 796: 10632 of cap free
Amount of items: 2
Items: 
Size: 604655 Color: 1
Size: 384714 Color: 3

Bin 797: 10866 of cap free
Amount of items: 2
Items: 
Size: 637738 Color: 1
Size: 351397 Color: 2

Bin 798: 11010 of cap free
Amount of items: 2
Items: 
Size: 525948 Color: 3
Size: 463043 Color: 1

Bin 799: 11153 of cap free
Amount of items: 2
Items: 
Size: 771081 Color: 3
Size: 217767 Color: 2

Bin 800: 11394 of cap free
Amount of items: 2
Items: 
Size: 702803 Color: 0
Size: 285804 Color: 1

Bin 801: 11419 of cap free
Amount of items: 8
Items: 
Size: 126105 Color: 3
Size: 124058 Color: 1
Size: 123803 Color: 0
Size: 123049 Color: 3
Size: 123027 Color: 4
Size: 123017 Color: 2
Size: 122778 Color: 4
Size: 122745 Color: 1

Bin 802: 11985 of cap free
Amount of items: 2
Items: 
Size: 702518 Color: 4
Size: 285498 Color: 0

Bin 803: 12889 of cap free
Amount of items: 2
Items: 
Size: 730989 Color: 4
Size: 256123 Color: 3

Bin 804: 13914 of cap free
Amount of items: 2
Items: 
Size: 604070 Color: 3
Size: 382017 Color: 1

Bin 805: 14209 of cap free
Amount of items: 5
Items: 
Size: 207099 Color: 2
Size: 194880 Color: 0
Size: 194491 Color: 4
Size: 194844 Color: 0
Size: 194478 Color: 4

Bin 806: 14574 of cap free
Amount of items: 2
Items: 
Size: 604043 Color: 3
Size: 381384 Color: 2

Bin 807: 14650 of cap free
Amount of items: 2
Items: 
Size: 495689 Color: 2
Size: 489662 Color: 0

Bin 808: 14822 of cap free
Amount of items: 2
Items: 
Size: 755968 Color: 3
Size: 229211 Color: 1

Bin 809: 14848 of cap free
Amount of items: 2
Items: 
Size: 633887 Color: 4
Size: 351266 Color: 0

Bin 810: 15758 of cap free
Amount of items: 2
Items: 
Size: 494727 Color: 2
Size: 489516 Color: 3

Bin 811: 15771 of cap free
Amount of items: 2
Items: 
Size: 494724 Color: 2
Size: 489506 Color: 3

Bin 812: 15989 of cap free
Amount of items: 2
Items: 
Size: 701838 Color: 2
Size: 282174 Color: 1

Bin 813: 17046 of cap free
Amount of items: 2
Items: 
Size: 765335 Color: 1
Size: 217620 Color: 0

Bin 814: 17674 of cap free
Amount of items: 2
Items: 
Size: 528685 Color: 1
Size: 453642 Color: 4

Bin 815: 18290 of cap free
Amount of items: 2
Items: 
Size: 600379 Color: 4
Size: 381332 Color: 2

Bin 816: 18732 of cap free
Amount of items: 2
Items: 
Size: 579813 Color: 1
Size: 401456 Color: 0

Bin 817: 19178 of cap free
Amount of items: 2
Items: 
Size: 579430 Color: 4
Size: 401393 Color: 3

Bin 818: 20847 of cap free
Amount of items: 7
Items: 
Size: 141598 Color: 1
Size: 141125 Color: 2
Size: 139508 Color: 1
Size: 139443 Color: 2
Size: 139290 Color: 3
Size: 139228 Color: 2
Size: 138962 Color: 3

Bin 819: 21322 of cap free
Amount of items: 2
Items: 
Size: 600264 Color: 3
Size: 378415 Color: 2

Bin 820: 22410 of cap free
Amount of items: 2
Items: 
Size: 599828 Color: 0
Size: 377763 Color: 4

Bin 821: 22869 of cap free
Amount of items: 2
Items: 
Size: 599558 Color: 0
Size: 377574 Color: 4

Bin 822: 23221 of cap free
Amount of items: 2
Items: 
Size: 599378 Color: 2
Size: 377402 Color: 3

Bin 823: 24093 of cap free
Amount of items: 2
Items: 
Size: 599300 Color: 3
Size: 376608 Color: 0

Bin 824: 25649 of cap free
Amount of items: 6
Items: 
Size: 163950 Color: 0
Size: 163194 Color: 4
Size: 162681 Color: 1
Size: 162373 Color: 0
Size: 161172 Color: 2
Size: 160982 Color: 1

Bin 825: 25683 of cap free
Amount of items: 2
Items: 
Size: 486650 Color: 2
Size: 487668 Color: 3

Bin 826: 25685 of cap free
Amount of items: 2
Items: 
Size: 632613 Color: 3
Size: 341703 Color: 4

Bin 827: 26275 of cap free
Amount of items: 2
Items: 
Size: 632239 Color: 1
Size: 341487 Color: 3

Bin 828: 26710 of cap free
Amount of items: 2
Items: 
Size: 755763 Color: 1
Size: 217528 Color: 3

Bin 829: 27248 of cap free
Amount of items: 2
Items: 
Size: 487022 Color: 3
Size: 485731 Color: 4

Bin 830: 27498 of cap free
Amount of items: 2
Items: 
Size: 755614 Color: 1
Size: 216889 Color: 0

Bin 831: 27720 of cap free
Amount of items: 2
Items: 
Size: 755533 Color: 1
Size: 216748 Color: 3

Bin 832: 29919 of cap free
Amount of items: 2
Items: 
Size: 753959 Color: 1
Size: 216123 Color: 2

Bin 833: 29996 of cap free
Amount of items: 2
Items: 
Size: 753923 Color: 1
Size: 216082 Color: 4

Bin 834: 30012 of cap free
Amount of items: 2
Items: 
Size: 485857 Color: 1
Size: 484132 Color: 0

Bin 835: 30176 of cap free
Amount of items: 9
Items: 
Size: 110987 Color: 1
Size: 109400 Color: 0
Size: 107276 Color: 1
Size: 109303 Color: 0
Size: 107014 Color: 2
Size: 106699 Color: 3
Size: 106499 Color: 2
Size: 106400 Color: 4
Size: 106247 Color: 3

Bin 836: 30397 of cap free
Amount of items: 5
Items: 
Size: 194358 Color: 2
Size: 193850 Color: 3
Size: 194123 Color: 2
Size: 193707 Color: 1
Size: 193566 Color: 2

Bin 837: 31300 of cap free
Amount of items: 2
Items: 
Size: 753931 Color: 4
Size: 214770 Color: 2

Bin 838: 32788 of cap free
Amount of items: 7
Items: 
Size: 138773 Color: 0
Size: 138706 Color: 2
Size: 138389 Color: 0
Size: 138529 Color: 2
Size: 137230 Color: 4
Size: 138502 Color: 2
Size: 137084 Color: 0

Bin 839: 35419 of cap free
Amount of items: 8
Items: 
Size: 122196 Color: 3
Size: 122117 Color: 1
Size: 120764 Color: 4
Size: 120690 Color: 2
Size: 120538 Color: 2
Size: 119931 Color: 1
Size: 119772 Color: 4
Size: 118574 Color: 1

Bin 840: 35667 of cap free
Amount of items: 2
Items: 
Size: 483008 Color: 0
Size: 481326 Color: 3

Bin 841: 35895 of cap free
Amount of items: 3
Items: 
Size: 333990 Color: 4
Size: 315194 Color: 2
Size: 314922 Color: 0

Bin 842: 37688 of cap free
Amount of items: 3
Items: 
Size: 332274 Color: 4
Size: 315124 Color: 2
Size: 314915 Color: 3

Bin 843: 38869 of cap free
Amount of items: 2
Items: 
Size: 480614 Color: 2
Size: 480518 Color: 3

Bin 844: 39320 of cap free
Amount of items: 5
Items: 
Size: 192674 Color: 4
Size: 192535 Color: 3
Size: 192168 Color: 0
Size: 191625 Color: 4
Size: 191679 Color: 0

Bin 845: 41507 of cap free
Amount of items: 6
Items: 
Size: 160890 Color: 2
Size: 160809 Color: 3
Size: 159271 Color: 4
Size: 160400 Color: 3
Size: 157753 Color: 0
Size: 159371 Color: 3

Bin 846: 42427 of cap free
Amount of items: 3
Items: 
Size: 329611 Color: 4
Size: 314602 Color: 1
Size: 313361 Color: 3

Bin 847: 46961 of cap free
Amount of items: 2
Items: 
Size: 480321 Color: 2
Size: 472719 Color: 3

Bin 848: 48656 of cap free
Amount of items: 5
Items: 
Size: 190940 Color: 3
Size: 190499 Color: 0
Size: 190364 Color: 3
Size: 189572 Color: 1
Size: 189970 Color: 3

Bin 849: 49458 of cap free
Amount of items: 7
Items: 
Size: 137361 Color: 2
Size: 136999 Color: 1
Size: 136617 Color: 4
Size: 135179 Color: 0
Size: 134845 Color: 1
Size: 134859 Color: 0
Size: 134683 Color: 2

Bin 850: 49921 of cap free
Amount of items: 3
Items: 
Size: 323739 Color: 4
Size: 313256 Color: 0
Size: 313085 Color: 3

Bin 851: 50610 of cap free
Amount of items: 2
Items: 
Size: 573675 Color: 3
Size: 375716 Color: 2

Bin 852: 54885 of cap free
Amount of items: 4
Items: 
Size: 269043 Color: 2
Size: 246685 Color: 0
Size: 214722 Color: 1
Size: 214666 Color: 2

Bin 853: 56224 of cap free
Amount of items: 5
Items: 
Size: 189271 Color: 1
Size: 189169 Color: 4
Size: 188662 Color: 1
Size: 188469 Color: 4
Size: 188206 Color: 3

Bin 854: 57903 of cap free
Amount of items: 3
Items: 
Size: 320300 Color: 4
Size: 311341 Color: 3
Size: 310457 Color: 0

Bin 855: 61548 of cap free
Amount of items: 5
Items: 
Size: 188094 Color: 1
Size: 187688 Color: 0
Size: 187582 Color: 4
Size: 187540 Color: 2
Size: 187549 Color: 4

Bin 856: 62883 of cap free
Amount of items: 6
Items: 
Size: 156552 Color: 1
Size: 157549 Color: 3
Size: 156095 Color: 1
Size: 156030 Color: 3
Size: 155925 Color: 0
Size: 154967 Color: 0

Bin 857: 64650 of cap free
Amount of items: 7
Items: 
Size: 134324 Color: 0
Size: 134326 Color: 2
Size: 134185 Color: 0
Size: 133420 Color: 2
Size: 133432 Color: 3
Size: 132989 Color: 1
Size: 132675 Color: 4

Bin 858: 65835 of cap free
Amount of items: 5
Items: 
Size: 186946 Color: 0
Size: 186976 Color: 4
Size: 186900 Color: 2
Size: 186804 Color: 0
Size: 186540 Color: 3

Bin 859: 66762 of cap free
Amount of items: 8
Items: 
Size: 118305 Color: 0
Size: 118523 Color: 1
Size: 117155 Color: 1
Size: 116864 Color: 4
Size: 115997 Color: 3
Size: 116408 Color: 4
Size: 115121 Color: 2
Size: 114866 Color: 4

Bin 860: 71620 of cap free
Amount of items: 3
Items: 
Size: 310404 Color: 3
Size: 310254 Color: 1
Size: 307723 Color: 3

Bin 861: 73192 of cap free
Amount of items: 5
Items: 
Size: 186475 Color: 1
Size: 186115 Color: 4
Size: 184313 Color: 2
Size: 185818 Color: 4
Size: 184088 Color: 0

Bin 862: 78732 of cap free
Amount of items: 6
Items: 
Size: 154232 Color: 3
Size: 154124 Color: 1
Size: 154110 Color: 4
Size: 153789 Color: 0
Size: 152552 Color: 0
Size: 152462 Color: 0

Bin 863: 78892 of cap free
Amount of items: 3
Items: 
Size: 307299 Color: 4
Size: 306905 Color: 0
Size: 306905 Color: 4

Bin 864: 79169 of cap free
Amount of items: 5
Items: 
Size: 185636 Color: 4
Size: 184086 Color: 1
Size: 184067 Color: 3
Size: 183671 Color: 4
Size: 183372 Color: 2

Bin 865: 79763 of cap free
Amount of items: 7
Items: 
Size: 132658 Color: 0
Size: 132614 Color: 4
Size: 131496 Color: 0
Size: 131116 Color: 3
Size: 130948 Color: 0
Size: 130834 Color: 2
Size: 130572 Color: 0

Bin 866: 86390 of cap free
Amount of items: 5
Items: 
Size: 183532 Color: 4
Size: 183217 Color: 0
Size: 183063 Color: 4
Size: 182032 Color: 3
Size: 181767 Color: 0

Bin 867: 86780 of cap free
Amount of items: 2
Items: 
Size: 459719 Color: 1
Size: 453502 Color: 2

Bin 868: 87870 of cap free
Amount of items: 2
Items: 
Size: 458633 Color: 1
Size: 453498 Color: 0

Bin 869: 88684 of cap free
Amount of items: 6
Items: 
Size: 152288 Color: 3
Size: 152216 Color: 2
Size: 152169 Color: 3
Size: 151500 Color: 0
Size: 151880 Color: 3
Size: 151264 Color: 0

Bin 870: 92932 of cap free
Amount of items: 8
Items: 
Size: 114618 Color: 0
Size: 114040 Color: 1
Size: 113957 Color: 4
Size: 113720 Color: 1
Size: 113103 Color: 1
Size: 112903 Color: 4
Size: 112204 Color: 1
Size: 112524 Color: 4

Bin 871: 93868 of cap free
Amount of items: 5
Items: 
Size: 181558 Color: 2
Size: 181392 Color: 1
Size: 181078 Color: 3
Size: 181053 Color: 0
Size: 181052 Color: 1

Bin 872: 94217 of cap free
Amount of items: 3
Items: 
Size: 304439 Color: 3
Size: 301410 Color: 1
Size: 299935 Color: 4

Bin 873: 94524 of cap free
Amount of items: 2
Items: 
Size: 452741 Color: 2
Size: 452736 Color: 0

Bin 874: 95346 of cap free
Amount of items: 2
Items: 
Size: 451947 Color: 1
Size: 452708 Color: 0

Bin 875: 95783 of cap free
Amount of items: 2
Items: 
Size: 451732 Color: 4
Size: 452486 Color: 0

Bin 876: 97471 of cap free
Amount of items: 5
Items: 
Size: 180849 Color: 4
Size: 180864 Color: 1
Size: 180848 Color: 3
Size: 179942 Color: 1
Size: 180027 Color: 3

Bin 877: 99332 of cap free
Amount of items: 2
Items: 
Size: 451206 Color: 2
Size: 449463 Color: 1

Bin 878: 99877 of cap free
Amount of items: 7
Items: 
Size: 129840 Color: 1
Size: 128972 Color: 2
Size: 128928 Color: 4
Size: 128498 Color: 1
Size: 128302 Color: 0
Size: 128113 Color: 3
Size: 127471 Color: 0

Bin 879: 591118 of cap free
Amount of items: 4
Items: 
Size: 106167 Color: 4
Size: 101274 Color: 3
Size: 101192 Color: 2
Size: 100250 Color: 0

Bin 880: 894707 of cap free
Amount of items: 1
Items: 
Size: 105294 Color: 4

Bin 881: 894744 of cap free
Amount of items: 1
Items: 
Size: 105257 Color: 4

Bin 882: 894998 of cap free
Amount of items: 1
Items: 
Size: 105003 Color: 4

Bin 883: 896574 of cap free
Amount of items: 1
Items: 
Size: 103427 Color: 4

Total size: 874417442
Total free space: 8583441

