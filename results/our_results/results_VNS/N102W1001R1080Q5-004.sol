Capicity Bin: 1001
Lower Bound: 46

Bins used: 46
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 3
Size: 285 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 3
Size: 180 Color: 1
Size: 124 Color: 2

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 435 Color: 4

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 415 Color: 3

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 252 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 318 Color: 4
Size: 254 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 3
Size: 391 Color: 0
Size: 153 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 426 Color: 4
Size: 129 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 0
Size: 385 Color: 3
Size: 166 Color: 0

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 212 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 2
Size: 428 Color: 4
Size: 132 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 1
Size: 131 Color: 0
Size: 112 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 727 Color: 2
Size: 140 Color: 3
Size: 134 Color: 0

Bin 14: 1 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 422 Color: 1

Bin 15: 1 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 4
Size: 234 Color: 3

Bin 16: 1 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 2
Size: 404 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 0
Size: 219 Color: 1
Size: 149 Color: 1

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 3
Size: 267 Color: 2

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 629 Color: 4
Size: 198 Color: 2
Size: 173 Color: 0

Bin 20: 4 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 2
Size: 430 Color: 3

Bin 21: 4 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 465 Color: 1

Bin 22: 4 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 3
Size: 448 Color: 0

Bin 23: 4 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 391 Color: 0

Bin 24: 5 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 4
Size: 312 Color: 3

Bin 25: 7 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 333 Color: 0

Bin 26: 7 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 1
Size: 449 Color: 3

Bin 27: 8 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 2
Size: 467 Color: 0

Bin 28: 10 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 0
Size: 476 Color: 1

Bin 29: 10 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 4
Size: 232 Color: 1

Bin 30: 11 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 1
Size: 215 Color: 4

Bin 31: 12 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 4
Size: 347 Color: 0

Bin 32: 13 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 0
Size: 379 Color: 3

Bin 33: 13 of cap free
Amount of items: 2
Items: 
Size: 497 Color: 2
Size: 491 Color: 0

Bin 34: 14 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 0
Size: 402 Color: 4

Bin 35: 14 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 1
Size: 409 Color: 0

Bin 36: 14 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 3
Size: 311 Color: 1

Bin 37: 14 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 261 Color: 2

Bin 38: 19 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 308 Color: 2

Bin 39: 28 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 2
Size: 186 Color: 1

Bin 40: 29 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 1
Size: 337 Color: 2

Bin 41: 30 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 0
Size: 371 Color: 4

Bin 42: 46 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 0
Size: 230 Color: 3

Bin 43: 52 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 3
Size: 227 Color: 4

Bin 44: 102 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 1
Size: 149 Color: 0

Bin 45: 103 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 4
Size: 119 Color: 2

Bin 46: 133 of cap free
Amount of items: 2
Items: 
Size: 286 Color: 2
Size: 582 Color: 0

Total size: 45330
Total free space: 716

