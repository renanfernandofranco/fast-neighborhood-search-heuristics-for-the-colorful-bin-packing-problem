Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 9840 Color: 19
Size: 4278 Color: 18
Size: 4130 Color: 6
Size: 704 Color: 1
Size: 696 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10416 Color: 0
Size: 8176 Color: 5
Size: 1056 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11168 Color: 13
Size: 8160 Color: 4
Size: 320 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11128 Color: 14
Size: 7696 Color: 2
Size: 824 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11266 Color: 16
Size: 7984 Color: 7
Size: 398 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12080 Color: 18
Size: 6136 Color: 7
Size: 1432 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12256 Color: 13
Size: 7072 Color: 11
Size: 320 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13152 Color: 4
Size: 5424 Color: 17
Size: 1072 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13140 Color: 6
Size: 6176 Color: 2
Size: 332 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 9
Size: 5360 Color: 16
Size: 1056 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 18
Size: 4398 Color: 19
Size: 1396 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 15
Size: 3841 Color: 7
Size: 1937 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13856 Color: 14
Size: 5372 Color: 8
Size: 420 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 11
Size: 5428 Color: 9
Size: 228 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14332 Color: 5
Size: 4844 Color: 5
Size: 472 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 14352 Color: 15
Size: 4368 Color: 8
Size: 928 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14374 Color: 8
Size: 4818 Color: 3
Size: 456 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 14480 Color: 11
Size: 4776 Color: 3
Size: 392 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 14496 Color: 14
Size: 4832 Color: 5
Size: 320 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14550 Color: 2
Size: 4250 Color: 12
Size: 848 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14564 Color: 11
Size: 3516 Color: 18
Size: 1568 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14724 Color: 3
Size: 4048 Color: 15
Size: 876 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14800 Color: 6
Size: 4280 Color: 3
Size: 568 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14864 Color: 5
Size: 2656 Color: 14
Size: 2128 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 15006 Color: 2
Size: 3864 Color: 12
Size: 778 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 15008 Color: 3
Size: 4320 Color: 11
Size: 320 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 15009 Color: 7
Size: 3867 Color: 16
Size: 772 Color: 19

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 15023 Color: 14
Size: 3855 Color: 15
Size: 770 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 15064 Color: 16
Size: 3608 Color: 15
Size: 976 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 15128 Color: 8
Size: 3872 Color: 18
Size: 648 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 15236 Color: 14
Size: 3560 Color: 15
Size: 852 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 15396 Color: 6
Size: 3436 Color: 10
Size: 816 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 15500 Color: 11
Size: 3832 Color: 7
Size: 316 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 15507 Color: 9
Size: 2885 Color: 13
Size: 1256 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 15564 Color: 15
Size: 3232 Color: 17
Size: 852 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 15612 Color: 19
Size: 3364 Color: 16
Size: 672 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 15696 Color: 10
Size: 3484 Color: 13
Size: 468 Color: 7

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 15742 Color: 15
Size: 3468 Color: 10
Size: 438 Color: 18

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 15840 Color: 10
Size: 3312 Color: 8
Size: 496 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 15939 Color: 19
Size: 3091 Color: 4
Size: 618 Color: 12

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16112 Color: 5
Size: 2852 Color: 17
Size: 684 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 16122 Color: 5
Size: 1830 Color: 5
Size: 1696 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 16160 Color: 12
Size: 3048 Color: 15
Size: 440 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 16244 Color: 17
Size: 2844 Color: 7
Size: 560 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 16304 Color: 12
Size: 2576 Color: 7
Size: 768 Color: 10

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 16314 Color: 8
Size: 2102 Color: 0
Size: 1232 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 16474 Color: 3
Size: 2646 Color: 12
Size: 528 Color: 16

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 16480 Color: 2
Size: 1632 Color: 17
Size: 1536 Color: 10

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 16627 Color: 11
Size: 2655 Color: 1
Size: 366 Color: 5

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 16800 Color: 14
Size: 1632 Color: 2
Size: 1216 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 16812 Color: 4
Size: 1636 Color: 3
Size: 1200 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 16828 Color: 11
Size: 1956 Color: 10
Size: 864 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 16888 Color: 14
Size: 2208 Color: 5
Size: 552 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 16936 Color: 3
Size: 2210 Color: 4
Size: 502 Color: 6

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 17040 Color: 15
Size: 1768 Color: 8
Size: 840 Color: 12

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 16976 Color: 18
Size: 1512 Color: 0
Size: 1160 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 16982 Color: 8
Size: 2222 Color: 5
Size: 444 Color: 7

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 16998 Color: 10
Size: 1594 Color: 6
Size: 1056 Color: 12

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 17104 Color: 15
Size: 1312 Color: 6
Size: 1232 Color: 14

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 17056 Color: 3
Size: 2256 Color: 17
Size: 336 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 17093 Color: 7
Size: 2111 Color: 2
Size: 444 Color: 9

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 17240 Color: 2
Size: 1632 Color: 9
Size: 776 Color: 18

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 17413 Color: 16
Size: 1863 Color: 14
Size: 372 Color: 17

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 17442 Color: 14
Size: 1960 Color: 2
Size: 246 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 17504 Color: 0
Size: 1824 Color: 7
Size: 320 Color: 10

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 17518 Color: 7
Size: 1378 Color: 2
Size: 752 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 17544 Color: 18
Size: 1682 Color: 9
Size: 422 Color: 15

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 17608 Color: 10
Size: 1592 Color: 13
Size: 448 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 17616 Color: 10
Size: 1424 Color: 14
Size: 608 Color: 6

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 17632 Color: 11
Size: 1248 Color: 13
Size: 768 Color: 15

Bin 71: 1 of cap free
Amount of items: 6
Items: 
Size: 9828 Color: 9
Size: 2616 Color: 3
Size: 2393 Color: 12
Size: 2364 Color: 5
Size: 1408 Color: 10
Size: 1038 Color: 2

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12092 Color: 12
Size: 5987 Color: 11
Size: 1568 Color: 19

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13411 Color: 14
Size: 6076 Color: 16
Size: 160 Color: 14

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13687 Color: 7
Size: 5416 Color: 8
Size: 544 Color: 15

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 14459 Color: 13
Size: 4244 Color: 12
Size: 944 Color: 17

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 15039 Color: 11
Size: 4608 Color: 0

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 15539 Color: 2
Size: 3948 Color: 13
Size: 160 Color: 8

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 15923 Color: 2
Size: 3436 Color: 17
Size: 288 Color: 5

Bin 79: 1 of cap free
Amount of items: 2
Items: 
Size: 16196 Color: 14
Size: 3451 Color: 19

Bin 80: 1 of cap free
Amount of items: 2
Items: 
Size: 16463 Color: 13
Size: 3184 Color: 16

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 16591 Color: 1
Size: 2928 Color: 18
Size: 128 Color: 6

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 16907 Color: 11
Size: 2356 Color: 18
Size: 384 Color: 0

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 17253 Color: 3
Size: 1198 Color: 19
Size: 1196 Color: 1

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 10566 Color: 14
Size: 7872 Color: 12
Size: 1208 Color: 11

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12449 Color: 4
Size: 6001 Color: 17
Size: 1196 Color: 8

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 12462 Color: 15
Size: 6720 Color: 15
Size: 464 Color: 5

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 1
Size: 4296 Color: 11
Size: 2238 Color: 5

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 17
Size: 3425 Color: 6
Size: 1831 Color: 14

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 14500 Color: 17
Size: 5146 Color: 11

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 3
Size: 4032 Color: 12
Size: 1080 Color: 19

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 15016 Color: 12
Size: 4432 Color: 2
Size: 198 Color: 11

Bin 92: 2 of cap free
Amount of items: 2
Items: 
Size: 15022 Color: 11
Size: 4624 Color: 8

Bin 93: 2 of cap free
Amount of items: 2
Items: 
Size: 15384 Color: 0
Size: 4262 Color: 18

Bin 94: 2 of cap free
Amount of items: 2
Items: 
Size: 15414 Color: 5
Size: 4232 Color: 3

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 15992 Color: 7
Size: 3552 Color: 6
Size: 102 Color: 16

Bin 96: 2 of cap free
Amount of items: 2
Items: 
Size: 17454 Color: 8
Size: 2192 Color: 12

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 7
Size: 5239 Color: 12

Bin 98: 3 of cap free
Amount of items: 2
Items: 
Size: 15320 Color: 6
Size: 4325 Color: 17

Bin 99: 3 of cap free
Amount of items: 2
Items: 
Size: 17126 Color: 16
Size: 2519 Color: 3

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 17285 Color: 4
Size: 1656 Color: 14
Size: 704 Color: 15

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 17568 Color: 17
Size: 1997 Color: 6
Size: 80 Color: 14

Bin 102: 4 of cap free
Amount of items: 9
Items: 
Size: 9826 Color: 13
Size: 1632 Color: 14
Size: 1600 Color: 2
Size: 1436 Color: 14
Size: 1376 Color: 3
Size: 1044 Color: 0
Size: 1002 Color: 1
Size: 928 Color: 6
Size: 800 Color: 14

Bin 103: 4 of cap free
Amount of items: 7
Items: 
Size: 9832 Color: 1
Size: 2350 Color: 16
Size: 2264 Color: 13
Size: 2096 Color: 12
Size: 1778 Color: 13
Size: 960 Color: 0
Size: 364 Color: 17

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 12232 Color: 19
Size: 3870 Color: 18
Size: 3542 Color: 15

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 15
Size: 6576 Color: 2
Size: 772 Color: 14

Bin 106: 4 of cap free
Amount of items: 3
Items: 
Size: 13788 Color: 15
Size: 5472 Color: 13
Size: 384 Color: 12

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 13844 Color: 6
Size: 5448 Color: 14
Size: 352 Color: 16

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 17584 Color: 6
Size: 1964 Color: 16
Size: 96 Color: 4

Bin 109: 4 of cap free
Amount of items: 2
Items: 
Size: 17666 Color: 1
Size: 1978 Color: 9

Bin 110: 5 of cap free
Amount of items: 3
Items: 
Size: 12208 Color: 18
Size: 4339 Color: 1
Size: 3096 Color: 5

Bin 111: 5 of cap free
Amount of items: 3
Items: 
Size: 17115 Color: 8
Size: 1312 Color: 9
Size: 1216 Color: 15

Bin 112: 5 of cap free
Amount of items: 3
Items: 
Size: 17451 Color: 0
Size: 2176 Color: 18
Size: 16 Color: 15

Bin 113: 5 of cap free
Amount of items: 2
Items: 
Size: 17467 Color: 17
Size: 2176 Color: 10

Bin 114: 5 of cap free
Amount of items: 2
Items: 
Size: 17672 Color: 10
Size: 1971 Color: 13

Bin 115: 6 of cap free
Amount of items: 5
Items: 
Size: 9834 Color: 15
Size: 4016 Color: 15
Size: 2912 Color: 11
Size: 1632 Color: 3
Size: 1248 Color: 12

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 2
Size: 6974 Color: 10
Size: 640 Color: 1

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 12144 Color: 15
Size: 6986 Color: 4
Size: 512 Color: 14

Bin 118: 6 of cap free
Amount of items: 2
Items: 
Size: 14443 Color: 15
Size: 5199 Color: 2

Bin 119: 6 of cap free
Amount of items: 2
Items: 
Size: 15344 Color: 4
Size: 4298 Color: 13

Bin 120: 6 of cap free
Amount of items: 3
Items: 
Size: 15402 Color: 10
Size: 3600 Color: 15
Size: 640 Color: 14

Bin 121: 6 of cap free
Amount of items: 3
Items: 
Size: 17304 Color: 5
Size: 2286 Color: 18
Size: 52 Color: 0

Bin 122: 6 of cap free
Amount of items: 2
Items: 
Size: 17634 Color: 18
Size: 2008 Color: 14

Bin 123: 7 of cap free
Amount of items: 3
Items: 
Size: 14379 Color: 2
Size: 4382 Color: 9
Size: 880 Color: 7

Bin 124: 7 of cap free
Amount of items: 2
Items: 
Size: 17046 Color: 17
Size: 2595 Color: 3

Bin 125: 8 of cap free
Amount of items: 3
Items: 
Size: 11010 Color: 12
Size: 8182 Color: 5
Size: 448 Color: 17

Bin 126: 8 of cap free
Amount of items: 3
Items: 
Size: 11760 Color: 11
Size: 7112 Color: 2
Size: 768 Color: 14

Bin 127: 8 of cap free
Amount of items: 2
Items: 
Size: 15532 Color: 3
Size: 4108 Color: 2

Bin 128: 8 of cap free
Amount of items: 3
Items: 
Size: 16520 Color: 5
Size: 1712 Color: 16
Size: 1408 Color: 15

Bin 129: 9 of cap free
Amount of items: 5
Items: 
Size: 9836 Color: 9
Size: 6248 Color: 4
Size: 2131 Color: 10
Size: 848 Color: 19
Size: 576 Color: 18

Bin 130: 10 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 6
Size: 6256 Color: 7

Bin 131: 10 of cap free
Amount of items: 2
Items: 
Size: 14416 Color: 16
Size: 5222 Color: 3

Bin 132: 10 of cap free
Amount of items: 2
Items: 
Size: 15456 Color: 19
Size: 4182 Color: 0

Bin 133: 11 of cap free
Amount of items: 2
Items: 
Size: 16855 Color: 11
Size: 2782 Color: 8

Bin 134: 12 of cap free
Amount of items: 2
Items: 
Size: 14520 Color: 3
Size: 5116 Color: 4

Bin 135: 12 of cap free
Amount of items: 2
Items: 
Size: 16228 Color: 16
Size: 3408 Color: 10

Bin 136: 13 of cap free
Amount of items: 3
Items: 
Size: 12465 Color: 0
Size: 6640 Color: 10
Size: 530 Color: 18

Bin 137: 14 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 10
Size: 6882 Color: 19
Size: 584 Color: 9

Bin 138: 14 of cap free
Amount of items: 2
Items: 
Size: 12364 Color: 17
Size: 7270 Color: 6

Bin 139: 16 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 13
Size: 8184 Color: 18
Size: 336 Color: 14

Bin 140: 16 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 11
Size: 5136 Color: 6
Size: 464 Color: 19

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 15468 Color: 3
Size: 4164 Color: 6

Bin 142: 16 of cap free
Amount of items: 2
Items: 
Size: 16560 Color: 0
Size: 3072 Color: 17

Bin 143: 16 of cap free
Amount of items: 3
Items: 
Size: 17168 Color: 12
Size: 2400 Color: 10
Size: 64 Color: 13

Bin 144: 17 of cap free
Amount of items: 2
Items: 
Size: 13611 Color: 6
Size: 6020 Color: 12

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 12428 Color: 2
Size: 7202 Color: 17

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 14942 Color: 16
Size: 4688 Color: 10

Bin 147: 18 of cap free
Amount of items: 3
Items: 
Size: 16830 Color: 16
Size: 2736 Color: 19
Size: 64 Color: 15

Bin 148: 18 of cap free
Amount of items: 2
Items: 
Size: 16966 Color: 10
Size: 2664 Color: 9

Bin 149: 19 of cap free
Amount of items: 2
Items: 
Size: 14660 Color: 7
Size: 4969 Color: 15

Bin 150: 20 of cap free
Amount of items: 2
Items: 
Size: 15192 Color: 7
Size: 4436 Color: 9

Bin 151: 20 of cap free
Amount of items: 2
Items: 
Size: 17009 Color: 8
Size: 2619 Color: 2

Bin 152: 22 of cap free
Amount of items: 2
Items: 
Size: 16187 Color: 18
Size: 3439 Color: 16

Bin 153: 22 of cap free
Amount of items: 2
Items: 
Size: 16368 Color: 8
Size: 3258 Color: 19

Bin 154: 23 of cap free
Amount of items: 2
Items: 
Size: 13635 Color: 17
Size: 5990 Color: 18

Bin 155: 24 of cap free
Amount of items: 3
Items: 
Size: 11028 Color: 5
Size: 8180 Color: 16
Size: 416 Color: 9

Bin 156: 24 of cap free
Amount of items: 3
Items: 
Size: 13488 Color: 6
Size: 4728 Color: 11
Size: 1408 Color: 9

Bin 157: 24 of cap free
Amount of items: 2
Items: 
Size: 15856 Color: 17
Size: 3768 Color: 5

Bin 158: 25 of cap free
Amount of items: 3
Items: 
Size: 9904 Color: 7
Size: 9271 Color: 4
Size: 448 Color: 2

Bin 159: 25 of cap free
Amount of items: 4
Items: 
Size: 10093 Color: 2
Size: 7570 Color: 19
Size: 1632 Color: 9
Size: 328 Color: 5

Bin 160: 26 of cap free
Amount of items: 2
Items: 
Size: 13398 Color: 17
Size: 6224 Color: 14

Bin 161: 27 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 4
Size: 6885 Color: 13
Size: 448 Color: 18

Bin 162: 27 of cap free
Amount of items: 2
Items: 
Size: 15998 Color: 2
Size: 3623 Color: 17

Bin 163: 28 of cap free
Amount of items: 3
Items: 
Size: 15944 Color: 1
Size: 3548 Color: 18
Size: 128 Color: 15

Bin 164: 28 of cap free
Amount of items: 2
Items: 
Size: 17308 Color: 8
Size: 2312 Color: 4

Bin 165: 29 of cap free
Amount of items: 3
Items: 
Size: 13504 Color: 6
Size: 5475 Color: 4
Size: 640 Color: 17

Bin 166: 30 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 12
Size: 8626 Color: 10
Size: 832 Color: 1

Bin 167: 31 of cap free
Amount of items: 3
Items: 
Size: 10077 Color: 14
Size: 9296 Color: 15
Size: 244 Color: 14

Bin 168: 31 of cap free
Amount of items: 3
Items: 
Size: 17300 Color: 14
Size: 2285 Color: 4
Size: 32 Color: 1

Bin 169: 32 of cap free
Amount of items: 2
Items: 
Size: 11696 Color: 5
Size: 7920 Color: 8

Bin 170: 36 of cap free
Amount of items: 27
Items: 
Size: 1006 Color: 3
Size: 992 Color: 19
Size: 978 Color: 15
Size: 964 Color: 8
Size: 960 Color: 17
Size: 878 Color: 8
Size: 876 Color: 3
Size: 864 Color: 11
Size: 824 Color: 3
Size: 768 Color: 7
Size: 768 Color: 0
Size: 752 Color: 14
Size: 720 Color: 13
Size: 704 Color: 15
Size: 704 Color: 0
Size: 696 Color: 6
Size: 686 Color: 11
Size: 616 Color: 4
Size: 608 Color: 10
Size: 576 Color: 11
Size: 576 Color: 8
Size: 552 Color: 9
Size: 552 Color: 6
Size: 544 Color: 9
Size: 512 Color: 10
Size: 512 Color: 4
Size: 424 Color: 1

Bin 171: 38 of cap free
Amount of items: 2
Items: 
Size: 12478 Color: 4
Size: 7132 Color: 6

Bin 172: 40 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 8
Size: 6384 Color: 6

Bin 173: 46 of cap free
Amount of items: 3
Items: 
Size: 12218 Color: 18
Size: 7128 Color: 2
Size: 256 Color: 13

Bin 174: 47 of cap free
Amount of items: 4
Items: 
Size: 9888 Color: 4
Size: 7963 Color: 18
Size: 1046 Color: 1
Size: 704 Color: 8

Bin 175: 51 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 4
Size: 7977 Color: 18
Size: 528 Color: 1

Bin 176: 53 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 0
Size: 5011 Color: 19

Bin 177: 55 of cap free
Amount of items: 8
Items: 
Size: 9827 Color: 8
Size: 1760 Color: 8
Size: 1744 Color: 19
Size: 1704 Color: 5
Size: 1654 Color: 11
Size: 1208 Color: 9
Size: 1088 Color: 6
Size: 608 Color: 10

Bin 178: 56 of cap free
Amount of items: 3
Items: 
Size: 11282 Color: 18
Size: 5368 Color: 0
Size: 2942 Color: 1

Bin 179: 57 of cap free
Amount of items: 11
Items: 
Size: 9825 Color: 19
Size: 1248 Color: 4
Size: 1196 Color: 15
Size: 1072 Color: 7
Size: 1024 Color: 14
Size: 1024 Color: 7
Size: 960 Color: 0
Size: 866 Color: 11
Size: 864 Color: 1
Size: 832 Color: 10
Size: 680 Color: 9

Bin 180: 57 of cap free
Amount of items: 2
Items: 
Size: 15523 Color: 18
Size: 4068 Color: 8

Bin 181: 58 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 9
Size: 6046 Color: 0
Size: 384 Color: 4

Bin 182: 66 of cap free
Amount of items: 2
Items: 
Size: 12394 Color: 15
Size: 7188 Color: 10

Bin 183: 69 of cap free
Amount of items: 2
Items: 
Size: 13395 Color: 15
Size: 6184 Color: 17

Bin 184: 71 of cap free
Amount of items: 2
Items: 
Size: 12678 Color: 3
Size: 6899 Color: 16

Bin 185: 72 of cap free
Amount of items: 2
Items: 
Size: 14692 Color: 0
Size: 4884 Color: 17

Bin 186: 73 of cap free
Amount of items: 2
Items: 
Size: 11387 Color: 12
Size: 8188 Color: 8

Bin 187: 75 of cap free
Amount of items: 3
Items: 
Size: 13363 Color: 3
Size: 5810 Color: 8
Size: 400 Color: 4

Bin 188: 79 of cap free
Amount of items: 3
Items: 
Size: 13347 Color: 6
Size: 4830 Color: 16
Size: 1392 Color: 9

Bin 189: 80 of cap free
Amount of items: 2
Items: 
Size: 15436 Color: 12
Size: 4132 Color: 8

Bin 190: 86 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 13
Size: 5323 Color: 18
Size: 4391 Color: 19

Bin 191: 92 of cap free
Amount of items: 2
Items: 
Size: 11371 Color: 5
Size: 8185 Color: 17

Bin 192: 94 of cap free
Amount of items: 2
Items: 
Size: 16456 Color: 2
Size: 3098 Color: 19

Bin 193: 98 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 19
Size: 6844 Color: 4
Size: 690 Color: 9

Bin 194: 99 of cap free
Amount of items: 2
Items: 
Size: 14518 Color: 18
Size: 5031 Color: 0

Bin 195: 110 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 12
Size: 5610 Color: 9

Bin 196: 124 of cap free
Amount of items: 2
Items: 
Size: 13204 Color: 9
Size: 6320 Color: 14

Bin 197: 168 of cap free
Amount of items: 3
Items: 
Size: 10096 Color: 5
Size: 8168 Color: 6
Size: 1216 Color: 8

Bin 198: 180 of cap free
Amount of items: 2
Items: 
Size: 13168 Color: 19
Size: 6300 Color: 12

Bin 199: 16392 of cap free
Amount of items: 8
Items: 
Size: 512 Color: 17
Size: 416 Color: 19
Size: 416 Color: 18
Size: 416 Color: 6
Size: 408 Color: 13
Size: 384 Color: 10
Size: 384 Color: 9
Size: 320 Color: 1

Total size: 3890304
Total free space: 19648

