Capicity Bin: 7928
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 3990 Color: 1
Size: 2426 Color: 8
Size: 1208 Color: 10
Size: 200 Color: 14
Size: 104 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5620 Color: 7
Size: 2004 Color: 10
Size: 304 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 15
Size: 1838 Color: 19
Size: 364 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5734 Color: 1
Size: 1822 Color: 0
Size: 372 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5884 Color: 4
Size: 1796 Color: 15
Size: 248 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6042 Color: 10
Size: 1574 Color: 6
Size: 312 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6044 Color: 5
Size: 943 Color: 17
Size: 941 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6045 Color: 8
Size: 1571 Color: 18
Size: 312 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6235 Color: 12
Size: 1411 Color: 3
Size: 282 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6358 Color: 5
Size: 1414 Color: 6
Size: 156 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6375 Color: 15
Size: 989 Color: 12
Size: 564 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6399 Color: 7
Size: 1249 Color: 5
Size: 280 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6406 Color: 11
Size: 986 Color: 14
Size: 536 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 15
Size: 1080 Color: 4
Size: 388 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6533 Color: 3
Size: 1059 Color: 16
Size: 336 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 18
Size: 1145 Color: 4
Size: 228 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6558 Color: 6
Size: 1142 Color: 6
Size: 228 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 16
Size: 762 Color: 7
Size: 456 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 15
Size: 616 Color: 2
Size: 574 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6770 Color: 6
Size: 678 Color: 7
Size: 480 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 13
Size: 948 Color: 8
Size: 184 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6847 Color: 7
Size: 869 Color: 1
Size: 212 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6919 Color: 9
Size: 857 Color: 7
Size: 152 Color: 5

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6885 Color: 0
Size: 855 Color: 4
Size: 188 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6887 Color: 3
Size: 831 Color: 16
Size: 210 Color: 6

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6933 Color: 9
Size: 815 Color: 11
Size: 180 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 3
Size: 872 Color: 5
Size: 158 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 5
Size: 660 Color: 19
Size: 360 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6951 Color: 9
Size: 781 Color: 6
Size: 196 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6973 Color: 13
Size: 783 Color: 4
Size: 172 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6982 Color: 12
Size: 738 Color: 11
Size: 208 Color: 11

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7005 Color: 8
Size: 771 Color: 2
Size: 152 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7007 Color: 1
Size: 769 Color: 14
Size: 152 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7058 Color: 3
Size: 660 Color: 16
Size: 210 Color: 10

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 13
Size: 670 Color: 7
Size: 168 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7126 Color: 9
Size: 572 Color: 2
Size: 230 Color: 16

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 4917 Color: 0
Size: 2838 Color: 13
Size: 172 Color: 10

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 10
Size: 1833 Color: 19
Size: 352 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6037 Color: 12
Size: 1618 Color: 14
Size: 272 Color: 7

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6211 Color: 9
Size: 1276 Color: 15
Size: 440 Color: 15

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6231 Color: 9
Size: 1212 Color: 2
Size: 484 Color: 7

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6180 Color: 4
Size: 1055 Color: 6
Size: 692 Color: 6

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6190 Color: 4
Size: 1405 Color: 19
Size: 332 Color: 7

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6496 Color: 5
Size: 1431 Color: 16

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6643 Color: 16
Size: 756 Color: 10
Size: 528 Color: 4

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6845 Color: 0
Size: 866 Color: 3
Size: 216 Color: 10

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6882 Color: 2
Size: 1045 Color: 11

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6989 Color: 19
Size: 656 Color: 9
Size: 282 Color: 17

Bin 49: 2 of cap free
Amount of items: 5
Items: 
Size: 3965 Color: 4
Size: 2217 Color: 8
Size: 1052 Color: 3
Size: 444 Color: 16
Size: 248 Color: 1

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 3972 Color: 9
Size: 2211 Color: 17
Size: 1743 Color: 15

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5010 Color: 19
Size: 2604 Color: 2
Size: 312 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 6006 Color: 8
Size: 1572 Color: 0
Size: 348 Color: 8

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 9
Size: 1208 Color: 2
Size: 484 Color: 11

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 6414 Color: 15
Size: 852 Color: 17
Size: 660 Color: 9

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 6476 Color: 3
Size: 862 Color: 13
Size: 588 Color: 9

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 6743 Color: 19
Size: 1071 Color: 7
Size: 112 Color: 9

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6842 Color: 0
Size: 1084 Color: 15

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 4903 Color: 9
Size: 2802 Color: 19
Size: 220 Color: 3

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 5686 Color: 9
Size: 1951 Color: 8
Size: 288 Color: 5

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 5766 Color: 12
Size: 1967 Color: 9
Size: 192 Color: 12

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 5955 Color: 16
Size: 1802 Color: 18
Size: 168 Color: 15

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 6243 Color: 13
Size: 1450 Color: 9
Size: 232 Color: 18

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 6430 Color: 8
Size: 1439 Color: 2
Size: 56 Color: 17

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 6663 Color: 8
Size: 1262 Color: 5

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6675 Color: 18
Size: 1250 Color: 4

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 6797 Color: 10
Size: 940 Color: 9
Size: 188 Color: 4

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 4485 Color: 14
Size: 3303 Color: 9
Size: 136 Color: 17

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 5591 Color: 6
Size: 1949 Color: 2
Size: 384 Color: 8

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 6383 Color: 6
Size: 1425 Color: 9
Size: 116 Color: 10

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6404 Color: 18
Size: 1520 Color: 5

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 6614 Color: 13
Size: 1310 Color: 16

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 6991 Color: 2
Size: 901 Color: 4
Size: 32 Color: 13

Bin 73: 5 of cap free
Amount of items: 4
Items: 
Size: 3966 Color: 19
Size: 2422 Color: 2
Size: 1295 Color: 0
Size: 240 Color: 18

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 4477 Color: 0
Size: 3302 Color: 5
Size: 144 Color: 6

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 5837 Color: 8
Size: 2086 Color: 17

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 10
Size: 1429 Color: 1
Size: 96 Color: 11

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 18
Size: 1255 Color: 15

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 7100 Color: 13
Size: 822 Color: 0

Bin 79: 7 of cap free
Amount of items: 3
Items: 
Size: 5269 Color: 3
Size: 1862 Color: 14
Size: 790 Color: 9

Bin 80: 7 of cap free
Amount of items: 2
Items: 
Size: 5277 Color: 4
Size: 2644 Color: 15

Bin 81: 7 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 2
Size: 1645 Color: 4

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 6659 Color: 2
Size: 758 Color: 15
Size: 504 Color: 9

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 4756 Color: 17
Size: 3052 Color: 15
Size: 112 Color: 4

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 7014 Color: 8
Size: 906 Color: 6

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 7046 Color: 7
Size: 874 Color: 4

Bin 86: 9 of cap free
Amount of items: 2
Items: 
Size: 6539 Color: 12
Size: 1380 Color: 10

Bin 87: 9 of cap free
Amount of items: 2
Items: 
Size: 6896 Color: 18
Size: 1023 Color: 17

Bin 88: 10 of cap free
Amount of items: 2
Items: 
Size: 4636 Color: 9
Size: 3282 Color: 5

Bin 89: 11 of cap free
Amount of items: 3
Items: 
Size: 4036 Color: 4
Size: 2466 Color: 0
Size: 1415 Color: 19

Bin 90: 11 of cap free
Amount of items: 3
Items: 
Size: 5188 Color: 8
Size: 2545 Color: 12
Size: 184 Color: 13

Bin 91: 13 of cap free
Amount of items: 3
Items: 
Size: 3967 Color: 9
Size: 3300 Color: 19
Size: 648 Color: 4

Bin 92: 13 of cap free
Amount of items: 2
Items: 
Size: 7118 Color: 11
Size: 797 Color: 0

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 6636 Color: 4
Size: 1278 Color: 2

Bin 94: 14 of cap free
Amount of items: 2
Items: 
Size: 6804 Color: 12
Size: 1110 Color: 1

Bin 95: 15 of cap free
Amount of items: 3
Items: 
Size: 5263 Color: 12
Size: 1870 Color: 3
Size: 780 Color: 10

Bin 96: 15 of cap free
Amount of items: 2
Items: 
Size: 5396 Color: 7
Size: 2517 Color: 1

Bin 97: 16 of cap free
Amount of items: 2
Items: 
Size: 6946 Color: 15
Size: 966 Color: 3

Bin 98: 17 of cap free
Amount of items: 2
Items: 
Size: 5587 Color: 13
Size: 2324 Color: 4

Bin 99: 17 of cap free
Amount of items: 2
Items: 
Size: 6203 Color: 18
Size: 1708 Color: 10

Bin 100: 17 of cap free
Amount of items: 2
Items: 
Size: 6917 Color: 5
Size: 994 Color: 1

Bin 101: 19 of cap free
Amount of items: 2
Items: 
Size: 6746 Color: 18
Size: 1163 Color: 13

Bin 102: 20 of cap free
Amount of items: 2
Items: 
Size: 6598 Color: 16
Size: 1310 Color: 18

Bin 103: 20 of cap free
Amount of items: 2
Items: 
Size: 6749 Color: 13
Size: 1159 Color: 8

Bin 104: 20 of cap free
Amount of items: 2
Items: 
Size: 6890 Color: 4
Size: 1018 Color: 1

Bin 105: 24 of cap free
Amount of items: 2
Items: 
Size: 4700 Color: 19
Size: 3204 Color: 12

Bin 106: 25 of cap free
Amount of items: 2
Items: 
Size: 5026 Color: 19
Size: 2877 Color: 18

Bin 107: 26 of cap free
Amount of items: 3
Items: 
Size: 4812 Color: 9
Size: 2434 Color: 19
Size: 656 Color: 16

Bin 108: 26 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 3
Size: 2122 Color: 6

Bin 109: 29 of cap free
Amount of items: 2
Items: 
Size: 5831 Color: 5
Size: 2068 Color: 11

Bin 110: 29 of cap free
Amount of items: 2
Items: 
Size: 6996 Color: 5
Size: 903 Color: 2

Bin 111: 29 of cap free
Amount of items: 2
Items: 
Size: 7028 Color: 5
Size: 871 Color: 2

Bin 112: 31 of cap free
Amount of items: 2
Items: 
Size: 5386 Color: 0
Size: 2511 Color: 15

Bin 113: 31 of cap free
Amount of items: 2
Items: 
Size: 6320 Color: 12
Size: 1577 Color: 17

Bin 114: 31 of cap free
Amount of items: 2
Items: 
Size: 6799 Color: 6
Size: 1098 Color: 11

Bin 115: 32 of cap free
Amount of items: 2
Items: 
Size: 4652 Color: 9
Size: 3244 Color: 12

Bin 116: 34 of cap free
Amount of items: 3
Items: 
Size: 5426 Color: 7
Size: 2364 Color: 17
Size: 104 Color: 19

Bin 117: 36 of cap free
Amount of items: 2
Items: 
Size: 4526 Color: 8
Size: 3366 Color: 13

Bin 118: 37 of cap free
Amount of items: 2
Items: 
Size: 6431 Color: 11
Size: 1460 Color: 1

Bin 119: 39 of cap free
Amount of items: 2
Items: 
Size: 5018 Color: 16
Size: 2871 Color: 13

Bin 120: 40 of cap free
Amount of items: 2
Items: 
Size: 5524 Color: 14
Size: 2364 Color: 1

Bin 121: 46 of cap free
Amount of items: 3
Items: 
Size: 4970 Color: 11
Size: 2748 Color: 10
Size: 164 Color: 9

Bin 122: 50 of cap free
Amount of items: 2
Items: 
Size: 3974 Color: 14
Size: 3904 Color: 16

Bin 123: 50 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 4
Size: 3472 Color: 16
Size: 258 Color: 10

Bin 124: 50 of cap free
Amount of items: 3
Items: 
Size: 4566 Color: 0
Size: 3156 Color: 9
Size: 156 Color: 10

Bin 125: 69 of cap free
Amount of items: 2
Items: 
Size: 6423 Color: 0
Size: 1436 Color: 5

Bin 126: 71 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 7
Size: 2221 Color: 4
Size: 168 Color: 9

Bin 127: 75 of cap free
Amount of items: 2
Items: 
Size: 5569 Color: 17
Size: 2284 Color: 15

Bin 128: 83 of cap free
Amount of items: 2
Items: 
Size: 4909 Color: 13
Size: 2936 Color: 12

Bin 129: 88 of cap free
Amount of items: 2
Items: 
Size: 5148 Color: 7
Size: 2692 Color: 19

Bin 130: 93 of cap free
Amount of items: 3
Items: 
Size: 4092 Color: 0
Size: 3301 Color: 15
Size: 442 Color: 14

Bin 131: 98 of cap free
Amount of items: 28
Items: 
Size: 456 Color: 17
Size: 456 Color: 12
Size: 420 Color: 16
Size: 400 Color: 17
Size: 392 Color: 7
Size: 390 Color: 13
Size: 364 Color: 13
Size: 328 Color: 13
Size: 314 Color: 12
Size: 288 Color: 2
Size: 286 Color: 7
Size: 286 Color: 0
Size: 280 Color: 11
Size: 264 Color: 0
Size: 252 Color: 9
Size: 252 Color: 0
Size: 250 Color: 6
Size: 240 Color: 17
Size: 224 Color: 8
Size: 224 Color: 2
Size: 208 Color: 10
Size: 208 Color: 4
Size: 196 Color: 14
Size: 196 Color: 1
Size: 180 Color: 14
Size: 180 Color: 6
Size: 160 Color: 18
Size: 136 Color: 10

Bin 132: 102 of cap free
Amount of items: 16
Items: 
Size: 984 Color: 14
Size: 702 Color: 4
Size: 632 Color: 5
Size: 624 Color: 5
Size: 560 Color: 7
Size: 544 Color: 1
Size: 512 Color: 15
Size: 502 Color: 1
Size: 500 Color: 1
Size: 492 Color: 19
Size: 480 Color: 11
Size: 416 Color: 13
Size: 260 Color: 6
Size: 248 Color: 10
Size: 214 Color: 10
Size: 156 Color: 13

Bin 133: 6110 of cap free
Amount of items: 12
Items: 
Size: 172 Color: 15
Size: 172 Color: 11
Size: 162 Color: 17
Size: 160 Color: 19
Size: 156 Color: 14
Size: 156 Color: 12
Size: 152 Color: 16
Size: 152 Color: 6
Size: 144 Color: 18
Size: 132 Color: 10
Size: 132 Color: 8
Size: 128 Color: 5

Total size: 1046496
Total free space: 7928

