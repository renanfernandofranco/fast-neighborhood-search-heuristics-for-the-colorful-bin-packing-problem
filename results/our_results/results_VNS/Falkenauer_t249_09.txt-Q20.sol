Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 9
Size: 307 Color: 6
Size: 255 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 11
Size: 356 Color: 6
Size: 275 Color: 11

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 9
Size: 369 Color: 17
Size: 261 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 6
Size: 291 Color: 1
Size: 269 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 264 Color: 4
Size: 260 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 356 Color: 18
Size: 258 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 0
Size: 289 Color: 7
Size: 273 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 14
Size: 258 Color: 5
Size: 255 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 14
Size: 340 Color: 18
Size: 263 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 13
Size: 328 Color: 2
Size: 295 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 3
Size: 268 Color: 19
Size: 250 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 12
Size: 281 Color: 0
Size: 268 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 15
Size: 255 Color: 16
Size: 251 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 312 Color: 0
Size: 290 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 5
Size: 274 Color: 3
Size: 257 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 8
Size: 285 Color: 17
Size: 255 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 15
Size: 357 Color: 3
Size: 264 Color: 19

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 4
Size: 274 Color: 9
Size: 256 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 19
Size: 318 Color: 19
Size: 257 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 5
Size: 282 Color: 0
Size: 274 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 10
Size: 279 Color: 18
Size: 261 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9
Size: 270 Color: 10
Size: 260 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 19
Size: 301 Color: 3
Size: 262 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 5
Size: 362 Color: 10
Size: 263 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 15
Size: 317 Color: 11
Size: 256 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 291 Color: 14
Size: 274 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 2
Size: 332 Color: 8
Size: 303 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 19
Size: 308 Color: 14
Size: 266 Color: 18

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 275 Color: 15
Size: 267 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 5
Size: 268 Color: 7
Size: 252 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 18
Size: 302 Color: 1
Size: 355 Color: 19

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 6
Size: 327 Color: 2
Size: 296 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 5
Size: 257 Color: 14
Size: 252 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 17
Size: 349 Color: 15
Size: 270 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 15
Size: 260 Color: 16
Size: 252 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 10
Size: 303 Color: 2
Size: 259 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 2
Size: 326 Color: 2
Size: 305 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 312 Color: 14
Size: 292 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 10
Size: 365 Color: 12
Size: 257 Color: 10

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 9
Size: 273 Color: 5
Size: 253 Color: 14

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 13
Size: 324 Color: 9
Size: 312 Color: 5

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 7
Size: 345 Color: 14
Size: 255 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 8
Size: 309 Color: 7
Size: 303 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 15
Size: 337 Color: 10
Size: 269 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 6
Size: 335 Color: 10
Size: 283 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 19
Size: 298 Color: 17
Size: 266 Color: 8

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 17
Size: 294 Color: 3
Size: 282 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 6
Size: 299 Color: 12
Size: 255 Color: 12

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 10
Size: 297 Color: 4
Size: 286 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 15
Size: 289 Color: 6
Size: 262 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 6
Size: 353 Color: 8
Size: 275 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 6
Size: 271 Color: 13
Size: 270 Color: 15

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 352 Color: 5
Size: 264 Color: 10

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 7
Size: 287 Color: 13
Size: 250 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 12
Size: 273 Color: 4
Size: 250 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 15
Size: 294 Color: 17
Size: 292 Color: 10

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 17
Size: 288 Color: 14
Size: 252 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 18
Size: 329 Color: 17
Size: 252 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 19
Size: 295 Color: 1
Size: 259 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 4
Size: 280 Color: 4
Size: 262 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 289 Color: 6
Size: 256 Color: 15

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 15
Size: 338 Color: 15
Size: 251 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 339 Color: 8
Size: 295 Color: 15

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 15
Size: 350 Color: 19
Size: 274 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 6
Size: 271 Color: 17
Size: 263 Color: 12

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 11
Size: 314 Color: 17
Size: 269 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 0
Size: 264 Color: 12
Size: 258 Color: 13

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 18
Size: 329 Color: 1
Size: 260 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 9
Size: 315 Color: 18
Size: 305 Color: 18

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 13
Size: 361 Color: 4
Size: 275 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 267 Color: 5
Size: 262 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 3
Size: 333 Color: 0
Size: 252 Color: 15

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 4
Size: 302 Color: 10
Size: 287 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 323 Color: 2
Size: 289 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 14
Size: 259 Color: 14
Size: 250 Color: 17

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 18
Size: 302 Color: 7
Size: 271 Color: 5

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 17
Size: 340 Color: 19
Size: 319 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 291 Color: 13
Size: 275 Color: 13

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 14
Size: 307 Color: 19
Size: 269 Color: 12

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 5
Size: 297 Color: 12
Size: 267 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 4
Size: 280 Color: 14
Size: 280 Color: 12

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 18
Size: 273 Color: 19
Size: 270 Color: 13

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 17
Size: 279 Color: 2
Size: 251 Color: 16

Total size: 83000
Total free space: 0

