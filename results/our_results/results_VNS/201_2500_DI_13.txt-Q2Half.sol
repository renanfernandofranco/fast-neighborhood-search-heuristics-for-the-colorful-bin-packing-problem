Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 1
Size: 436 Color: 1
Size: 202 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1382 Color: 1
Size: 974 Color: 1
Size: 100 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 1
Size: 526 Color: 1
Size: 200 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 1
Size: 310 Color: 1
Size: 56 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 1
Size: 300 Color: 1
Size: 128 Color: 0

Bin 6: 0 of cap free
Amount of items: 7
Items: 
Size: 903 Color: 1
Size: 642 Color: 1
Size: 478 Color: 1
Size: 257 Color: 1
Size: 72 Color: 0
Size: 56 Color: 0
Size: 48 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2134 Color: 1
Size: 254 Color: 1
Size: 68 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 1
Size: 345 Color: 1
Size: 176 Color: 0

Bin 9: 0 of cap free
Amount of items: 4
Items: 
Size: 1686 Color: 1
Size: 572 Color: 1
Size: 102 Color: 0
Size: 96 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 1
Size: 738 Color: 1
Size: 144 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 1
Size: 604 Color: 1
Size: 120 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2165 Color: 1
Size: 243 Color: 1
Size: 48 Color: 0

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 1892 Color: 1
Size: 396 Color: 1
Size: 88 Color: 0
Size: 80 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 1
Size: 588 Color: 1
Size: 56 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 1
Size: 244 Color: 1
Size: 40 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 1
Size: 228 Color: 1
Size: 64 Color: 0

Bin 17: 0 of cap free
Amount of items: 5
Items: 
Size: 1230 Color: 1
Size: 1022 Color: 1
Size: 136 Color: 0
Size: 52 Color: 0
Size: 16 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 890 Color: 1
Size: 30 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 1
Size: 334 Color: 1
Size: 88 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 1
Size: 399 Color: 1
Size: 78 Color: 0

Bin 21: 0 of cap free
Amount of items: 7
Items: 
Size: 1234 Color: 1
Size: 479 Color: 1
Size: 317 Color: 1
Size: 244 Color: 0
Size: 84 Color: 0
Size: 56 Color: 0
Size: 42 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 1
Size: 984 Color: 1
Size: 76 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 1
Size: 364 Color: 1
Size: 104 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 1
Size: 222 Color: 1
Size: 176 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 1
Size: 270 Color: 1
Size: 86 Color: 0

Bin 26: 0 of cap free
Amount of items: 4
Items: 
Size: 1626 Color: 1
Size: 402 Color: 1
Size: 384 Color: 0
Size: 44 Color: 0

Bin 27: 0 of cap free
Amount of items: 5
Items: 
Size: 1236 Color: 1
Size: 606 Color: 1
Size: 430 Color: 1
Size: 152 Color: 0
Size: 32 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1883 Color: 1
Size: 511 Color: 1
Size: 62 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1688 Color: 1
Size: 684 Color: 1
Size: 84 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 1
Size: 840 Color: 1
Size: 84 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 1
Size: 438 Color: 1
Size: 120 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 1
Size: 370 Color: 1
Size: 112 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 1
Size: 694 Color: 1
Size: 272 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1959 Color: 1
Size: 415 Color: 1
Size: 82 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 1
Size: 551 Color: 1
Size: 110 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 1
Size: 805 Color: 1
Size: 160 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 1
Size: 386 Color: 1
Size: 128 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2015 Color: 1
Size: 439 Color: 1
Size: 2 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 1
Size: 723 Color: 1
Size: 144 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1671 Color: 1
Size: 655 Color: 1
Size: 130 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 1
Size: 772 Color: 1
Size: 40 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 1
Size: 293 Color: 1
Size: 94 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 1
Size: 1042 Color: 1
Size: 184 Color: 0

Bin 44: 1 of cap free
Amount of items: 4
Items: 
Size: 2154 Color: 1
Size: 213 Color: 1
Size: 48 Color: 0
Size: 40 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 1
Size: 435 Color: 1
Size: 80 Color: 0

Bin 46: 1 of cap free
Amount of items: 5
Items: 
Size: 1233 Color: 1
Size: 806 Color: 1
Size: 272 Color: 1
Size: 80 Color: 0
Size: 64 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 1
Size: 200 Color: 1
Size: 64 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 1
Size: 1021 Color: 1
Size: 236 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 1870 Color: 1
Size: 576 Color: 1
Size: 8 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 1828 Color: 1
Size: 562 Color: 1
Size: 64 Color: 0

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 2043 Color: 1
Size: 402 Color: 1
Size: 8 Color: 0

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 1739 Color: 1
Size: 554 Color: 1
Size: 160 Color: 0

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 1
Size: 324 Color: 1
Size: 50 Color: 0

Bin 54: 5 of cap free
Amount of items: 3
Items: 
Size: 1373 Color: 1
Size: 1020 Color: 1
Size: 58 Color: 0

Bin 55: 6 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 1
Size: 599 Color: 1
Size: 8 Color: 0

Bin 56: 8 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 1
Size: 504 Color: 1
Size: 118 Color: 0

Bin 57: 12 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 1
Size: 490 Color: 1
Size: 20 Color: 0

Bin 58: 18 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 1
Size: 476 Color: 0
Size: 180 Color: 1

Bin 59: 86 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 1
Size: 884 Color: 1
Size: 96 Color: 0

Bin 60: 255 of cap free
Amount of items: 1
Items: 
Size: 2201 Color: 1

Bin 61: 268 of cap free
Amount of items: 1
Items: 
Size: 2188 Color: 1

Bin 62: 276 of cap free
Amount of items: 1
Items: 
Size: 2180 Color: 1

Bin 63: 307 of cap free
Amount of items: 1
Items: 
Size: 2149 Color: 1

Bin 64: 351 of cap free
Amount of items: 1
Items: 
Size: 2105 Color: 1

Bin 65: 379 of cap free
Amount of items: 1
Items: 
Size: 2077 Color: 1

Bin 66: 462 of cap free
Amount of items: 1
Items: 
Size: 1994 Color: 1

Total size: 159640
Total free space: 2456

