Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 1
Size: 1022 Color: 3
Size: 204 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 0
Size: 1028 Color: 2
Size: 200 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 4
Size: 1018 Color: 1
Size: 200 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 1
Size: 842 Color: 3
Size: 68 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 0
Size: 702 Color: 1
Size: 52 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1766 Color: 1
Size: 644 Color: 0
Size: 62 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 2
Size: 567 Color: 1
Size: 112 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 2
Size: 489 Color: 1
Size: 96 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1951 Color: 3
Size: 435 Color: 1
Size: 86 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1964 Color: 4
Size: 396 Color: 1
Size: 112 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 2
Size: 402 Color: 1
Size: 76 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2026 Color: 2
Size: 374 Color: 1
Size: 72 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 3
Size: 401 Color: 2
Size: 20 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2057 Color: 2
Size: 397 Color: 2
Size: 18 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 2
Size: 313 Color: 3
Size: 98 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2084 Color: 3
Size: 272 Color: 1
Size: 116 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2162 Color: 1
Size: 262 Color: 2
Size: 48 Color: 3

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 2108 Color: 2
Size: 324 Color: 0
Size: 32 Color: 1
Size: 8 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 2
Size: 267 Color: 3
Size: 52 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 1
Size: 172 Color: 2
Size: 102 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 3
Size: 244 Color: 0
Size: 48 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 4
Size: 244 Color: 2
Size: 40 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 0
Size: 836 Color: 1
Size: 80 Color: 2

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 2
Size: 548 Color: 1
Size: 148 Color: 4

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1860 Color: 1
Size: 459 Color: 2
Size: 152 Color: 4

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1879 Color: 4
Size: 516 Color: 2
Size: 76 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 3
Size: 389 Color: 1
Size: 128 Color: 2

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 2004 Color: 4
Size: 419 Color: 0
Size: 48 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 2097 Color: 1
Size: 366 Color: 2
Size: 8 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 2066 Color: 2
Size: 289 Color: 1
Size: 116 Color: 3

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 2127 Color: 1
Size: 272 Color: 4
Size: 72 Color: 3

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 1
Size: 956 Color: 0
Size: 52 Color: 3

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1838 Color: 0
Size: 428 Color: 2
Size: 204 Color: 1

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 1
Size: 342 Color: 2
Size: 230 Color: 2

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 2086 Color: 2
Size: 384 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 2142 Color: 4
Size: 308 Color: 0
Size: 20 Color: 1

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 2
Size: 298 Color: 4

Bin 38: 3 of cap free
Amount of items: 4
Items: 
Size: 1239 Color: 2
Size: 576 Color: 1
Size: 572 Color: 3
Size: 82 Color: 1

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 2
Size: 732 Color: 2
Size: 64 Color: 0

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1700 Color: 2
Size: 665 Color: 3
Size: 104 Color: 1

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1788 Color: 3
Size: 647 Color: 0
Size: 34 Color: 2

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 1906 Color: 1
Size: 511 Color: 2
Size: 52 Color: 0

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 2
Size: 343 Color: 4
Size: 8 Color: 1

Bin 44: 5 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 4
Size: 762 Color: 1
Size: 152 Color: 2

Bin 45: 6 of cap free
Amount of items: 4
Items: 
Size: 1237 Color: 2
Size: 1029 Color: 4
Size: 144 Color: 0
Size: 56 Color: 1

Bin 46: 6 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 2
Size: 1000 Color: 2
Size: 44 Color: 1

Bin 47: 7 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 4
Size: 622 Color: 1
Size: 168 Color: 2

Bin 48: 9 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 1
Size: 581 Color: 0
Size: 204 Color: 2

Bin 49: 10 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 1
Size: 662 Color: 3
Size: 96 Color: 0

Bin 50: 11 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 3
Size: 767 Color: 1
Size: 92 Color: 0

Bin 51: 11 of cap free
Amount of items: 2
Items: 
Size: 1979 Color: 1
Size: 482 Color: 0

Bin 52: 14 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 4
Size: 878 Color: 1
Size: 176 Color: 3

Bin 53: 15 of cap free
Amount of items: 3
Items: 
Size: 2007 Color: 2
Size: 434 Color: 4
Size: 16 Color: 0

Bin 54: 19 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 1
Size: 840 Color: 4
Size: 204 Color: 3

Bin 55: 19 of cap free
Amount of items: 3
Items: 
Size: 1596 Color: 2
Size: 765 Color: 1
Size: 92 Color: 4

Bin 56: 20 of cap free
Amount of items: 2
Items: 
Size: 1726 Color: 2
Size: 726 Color: 0

Bin 57: 23 of cap free
Amount of items: 13
Items: 
Size: 504 Color: 1
Size: 495 Color: 4
Size: 278 Color: 2
Size: 252 Color: 4
Size: 160 Color: 4
Size: 144 Color: 0
Size: 132 Color: 4
Size: 132 Color: 3
Size: 112 Color: 1
Size: 64 Color: 0
Size: 64 Color: 0
Size: 56 Color: 0
Size: 56 Color: 0

Bin 58: 23 of cap free
Amount of items: 2
Items: 
Size: 1859 Color: 0
Size: 590 Color: 4

Bin 59: 26 of cap free
Amount of items: 2
Items: 
Size: 1916 Color: 4
Size: 530 Color: 2

Bin 60: 27 of cap free
Amount of items: 3
Items: 
Size: 1476 Color: 3
Size: 885 Color: 1
Size: 84 Color: 4

Bin 61: 27 of cap free
Amount of items: 2
Items: 
Size: 1971 Color: 4
Size: 474 Color: 3

Bin 62: 28 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 1030 Color: 2
Size: 176 Color: 2

Bin 63: 30 of cap free
Amount of items: 2
Items: 
Size: 1411 Color: 2
Size: 1031 Color: 3

Bin 64: 32 of cap free
Amount of items: 2
Items: 
Size: 1773 Color: 1
Size: 667 Color: 3

Bin 65: 33 of cap free
Amount of items: 2
Items: 
Size: 1552 Color: 4
Size: 887 Color: 2

Bin 66: 2032 of cap free
Amount of items: 4
Items: 
Size: 132 Color: 2
Size: 124 Color: 4
Size: 96 Color: 1
Size: 88 Color: 1

Total size: 160680
Total free space: 2472

