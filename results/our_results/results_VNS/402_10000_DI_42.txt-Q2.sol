Capicity Bin: 6624
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 34
Items: 
Size: 280 Color: 1
Size: 272 Color: 1
Size: 254 Color: 1
Size: 252 Color: 1
Size: 244 Color: 1
Size: 232 Color: 1
Size: 228 Color: 0
Size: 224 Color: 1
Size: 220 Color: 0
Size: 216 Color: 1
Size: 214 Color: 1
Size: 208 Color: 0
Size: 208 Color: 0
Size: 200 Color: 1
Size: 198 Color: 0
Size: 196 Color: 1
Size: 192 Color: 1
Size: 192 Color: 0
Size: 192 Color: 0
Size: 184 Color: 0
Size: 184 Color: 0
Size: 180 Color: 1
Size: 178 Color: 0
Size: 174 Color: 1
Size: 168 Color: 0
Size: 168 Color: 0
Size: 160 Color: 0
Size: 156 Color: 1
Size: 152 Color: 1
Size: 152 Color: 0
Size: 150 Color: 0
Size: 144 Color: 1
Size: 140 Color: 0
Size: 112 Color: 0

Bin 2: 0 of cap free
Amount of items: 21
Items: 
Size: 456 Color: 1
Size: 440 Color: 1
Size: 416 Color: 1
Size: 360 Color: 1
Size: 338 Color: 1
Size: 336 Color: 1
Size: 336 Color: 0
Size: 332 Color: 0
Size: 328 Color: 0
Size: 320 Color: 1
Size: 306 Color: 0
Size: 300 Color: 0
Size: 292 Color: 1
Size: 286 Color: 1
Size: 284 Color: 0
Size: 280 Color: 0
Size: 272 Color: 1
Size: 264 Color: 0
Size: 244 Color: 0
Size: 240 Color: 0
Size: 194 Color: 0

Bin 3: 0 of cap free
Amount of items: 6
Items: 
Size: 3317 Color: 0
Size: 2085 Color: 1
Size: 550 Color: 1
Size: 424 Color: 0
Size: 136 Color: 1
Size: 112 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3940 Color: 0
Size: 2480 Color: 1
Size: 204 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4228 Color: 1
Size: 2220 Color: 0
Size: 176 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4266 Color: 0
Size: 2182 Color: 0
Size: 176 Color: 1

Bin 7: 0 of cap free
Amount of items: 5
Items: 
Size: 4564 Color: 1
Size: 666 Color: 1
Size: 602 Color: 1
Size: 580 Color: 0
Size: 212 Color: 0

Bin 8: 0 of cap free
Amount of items: 5
Items: 
Size: 4598 Color: 1
Size: 706 Color: 1
Size: 613 Color: 0
Size: 575 Color: 1
Size: 132 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 1
Size: 1564 Color: 0
Size: 144 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 1
Size: 1299 Color: 1
Size: 258 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5304 Color: 0
Size: 1204 Color: 1
Size: 116 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5444 Color: 1
Size: 800 Color: 0
Size: 380 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5431 Color: 0
Size: 1079 Color: 0
Size: 114 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5530 Color: 0
Size: 902 Color: 0
Size: 192 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5552 Color: 1
Size: 904 Color: 0
Size: 168 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5553 Color: 0
Size: 589 Color: 1
Size: 482 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 1
Size: 820 Color: 0
Size: 158 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5665 Color: 0
Size: 739 Color: 1
Size: 220 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5669 Color: 1
Size: 643 Color: 1
Size: 312 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5666 Color: 0
Size: 582 Color: 1
Size: 376 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5739 Color: 0
Size: 645 Color: 0
Size: 240 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5826 Color: 1
Size: 550 Color: 0
Size: 248 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5817 Color: 0
Size: 581 Color: 0
Size: 226 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5906 Color: 0
Size: 548 Color: 1
Size: 170 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5914 Color: 0
Size: 594 Color: 1
Size: 116 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 3322 Color: 1
Size: 2753 Color: 1
Size: 548 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 4621 Color: 1
Size: 1906 Color: 0
Size: 96 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 1
Size: 1179 Color: 0
Size: 168 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 5282 Color: 1
Size: 1045 Color: 0
Size: 296 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 5515 Color: 1
Size: 588 Color: 0
Size: 520 Color: 1

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 5577 Color: 1
Size: 1046 Color: 0

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 5709 Color: 1
Size: 914 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 5924 Color: 1
Size: 673 Color: 0
Size: 26 Color: 1

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 3725 Color: 1
Size: 2393 Color: 0
Size: 504 Color: 0

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 4458 Color: 0
Size: 2004 Color: 1
Size: 160 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 4593 Color: 0
Size: 1883 Color: 1
Size: 146 Color: 0

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 4698 Color: 0
Size: 1690 Color: 1
Size: 234 Color: 0

Bin 38: 2 of cap free
Amount of items: 4
Items: 
Size: 4910 Color: 1
Size: 876 Color: 0
Size: 708 Color: 1
Size: 128 Color: 0

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 5239 Color: 0
Size: 1383 Color: 1

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 5338 Color: 1
Size: 1196 Color: 0
Size: 88 Color: 1

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 5516 Color: 0
Size: 1106 Color: 1

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 5702 Color: 0
Size: 920 Color: 1

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 5898 Color: 0
Size: 724 Color: 1

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 3866 Color: 0
Size: 2383 Color: 1
Size: 372 Color: 1

Bin 45: 3 of cap free
Amount of items: 2
Items: 
Size: 5356 Color: 1
Size: 1265 Color: 0

Bin 46: 3 of cap free
Amount of items: 2
Items: 
Size: 5371 Color: 1
Size: 1250 Color: 0

Bin 47: 3 of cap free
Amount of items: 2
Items: 
Size: 5397 Color: 0
Size: 1224 Color: 1

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 1
Size: 717 Color: 0
Size: 70 Color: 0

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 1
Size: 738 Color: 0
Size: 40 Color: 1

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 5919 Color: 1
Size: 662 Color: 0
Size: 40 Color: 1

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 3348 Color: 0
Size: 2888 Color: 0
Size: 384 Color: 1

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 3632 Color: 0
Size: 2732 Color: 0
Size: 256 Color: 1

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 0
Size: 1940 Color: 1
Size: 140 Color: 0

Bin 54: 4 of cap free
Amount of items: 2
Items: 
Size: 4814 Color: 1
Size: 1806 Color: 0

Bin 55: 4 of cap free
Amount of items: 2
Items: 
Size: 5546 Color: 0
Size: 1074 Color: 1

Bin 56: 4 of cap free
Amount of items: 2
Items: 
Size: 5762 Color: 1
Size: 858 Color: 0

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 5818 Color: 1
Size: 802 Color: 0

Bin 58: 5 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 0
Size: 1693 Color: 1
Size: 72 Color: 1

Bin 59: 5 of cap free
Amount of items: 2
Items: 
Size: 5742 Color: 1
Size: 877 Color: 0

Bin 60: 5 of cap free
Amount of items: 2
Items: 
Size: 5738 Color: 0
Size: 881 Color: 1

Bin 61: 5 of cap free
Amount of items: 2
Items: 
Size: 5765 Color: 0
Size: 854 Color: 1

Bin 62: 5 of cap free
Amount of items: 2
Items: 
Size: 5932 Color: 1
Size: 687 Color: 0

Bin 63: 6 of cap free
Amount of items: 3
Items: 
Size: 3314 Color: 1
Size: 2754 Color: 0
Size: 550 Color: 1

Bin 64: 6 of cap free
Amount of items: 3
Items: 
Size: 3753 Color: 1
Size: 2761 Color: 0
Size: 104 Color: 1

Bin 65: 6 of cap free
Amount of items: 2
Items: 
Size: 4338 Color: 1
Size: 2280 Color: 0

Bin 66: 6 of cap free
Amount of items: 2
Items: 
Size: 4974 Color: 0
Size: 1644 Color: 1

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 5188 Color: 0
Size: 1430 Color: 1

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 5396 Color: 0
Size: 1222 Color: 1

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 5459 Color: 1
Size: 1159 Color: 0

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 5644 Color: 1
Size: 974 Color: 0

Bin 71: 7 of cap free
Amount of items: 9
Items: 
Size: 3313 Color: 0
Size: 544 Color: 1
Size: 488 Color: 1
Size: 478 Color: 1
Size: 456 Color: 1
Size: 420 Color: 0
Size: 392 Color: 0
Size: 392 Color: 0
Size: 134 Color: 1

Bin 72: 7 of cap free
Amount of items: 3
Items: 
Size: 4907 Color: 0
Size: 1606 Color: 1
Size: 104 Color: 0

Bin 73: 7 of cap free
Amount of items: 3
Items: 
Size: 5107 Color: 1
Size: 962 Color: 0
Size: 548 Color: 0

Bin 74: 7 of cap free
Amount of items: 2
Items: 
Size: 5816 Color: 1
Size: 801 Color: 0

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 5858 Color: 0
Size: 759 Color: 1

Bin 76: 8 of cap free
Amount of items: 2
Items: 
Size: 4748 Color: 1
Size: 1868 Color: 0

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 5759 Color: 1
Size: 857 Color: 0

Bin 78: 8 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 0
Size: 782 Color: 1
Size: 78 Color: 1

Bin 79: 9 of cap free
Amount of items: 2
Items: 
Size: 5690 Color: 1
Size: 925 Color: 0

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 5935 Color: 0
Size: 680 Color: 1

Bin 81: 10 of cap free
Amount of items: 2
Items: 
Size: 3476 Color: 1
Size: 3138 Color: 0

Bin 82: 10 of cap free
Amount of items: 2
Items: 
Size: 5045 Color: 0
Size: 1569 Color: 1

Bin 83: 11 of cap free
Amount of items: 2
Items: 
Size: 5927 Color: 1
Size: 686 Color: 0

Bin 84: 12 of cap free
Amount of items: 4
Items: 
Size: 4783 Color: 1
Size: 851 Color: 0
Size: 818 Color: 0
Size: 160 Color: 1

Bin 85: 12 of cap free
Amount of items: 2
Items: 
Size: 5370 Color: 1
Size: 1242 Color: 0

Bin 86: 12 of cap free
Amount of items: 2
Items: 
Size: 5801 Color: 0
Size: 811 Color: 1

Bin 87: 13 of cap free
Amount of items: 2
Items: 
Size: 5076 Color: 0
Size: 1535 Color: 1

Bin 88: 13 of cap free
Amount of items: 2
Items: 
Size: 5889 Color: 1
Size: 722 Color: 0

Bin 89: 14 of cap free
Amount of items: 4
Items: 
Size: 3325 Color: 0
Size: 2417 Color: 1
Size: 476 Color: 0
Size: 392 Color: 1

Bin 90: 14 of cap free
Amount of items: 3
Items: 
Size: 3650 Color: 0
Size: 2804 Color: 1
Size: 156 Color: 1

Bin 91: 15 of cap free
Amount of items: 2
Items: 
Size: 4365 Color: 1
Size: 2244 Color: 0

Bin 92: 15 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 1
Size: 1861 Color: 0
Size: 96 Color: 0

Bin 93: 15 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 1
Size: 961 Color: 0
Size: 644 Color: 0

Bin 94: 16 of cap free
Amount of items: 2
Items: 
Size: 5580 Color: 0
Size: 1028 Color: 1

Bin 95: 16 of cap free
Amount of items: 2
Items: 
Size: 5684 Color: 0
Size: 924 Color: 1

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 5715 Color: 0
Size: 893 Color: 1

Bin 97: 16 of cap free
Amount of items: 2
Items: 
Size: 5887 Color: 1
Size: 721 Color: 0

Bin 98: 17 of cap free
Amount of items: 3
Items: 
Size: 4811 Color: 1
Size: 1724 Color: 0
Size: 72 Color: 1

Bin 99: 17 of cap free
Amount of items: 2
Items: 
Size: 5251 Color: 1
Size: 1356 Color: 0

Bin 100: 17 of cap free
Amount of items: 2
Items: 
Size: 5852 Color: 1
Size: 755 Color: 0

Bin 101: 18 of cap free
Amount of items: 2
Items: 
Size: 4935 Color: 0
Size: 1671 Color: 1

Bin 102: 18 of cap free
Amount of items: 2
Items: 
Size: 5331 Color: 0
Size: 1275 Color: 1

Bin 103: 19 of cap free
Amount of items: 2
Items: 
Size: 4123 Color: 0
Size: 2482 Color: 1

Bin 104: 19 of cap free
Amount of items: 2
Items: 
Size: 5095 Color: 0
Size: 1510 Color: 1

Bin 105: 20 of cap free
Amount of items: 2
Items: 
Size: 5126 Color: 0
Size: 1478 Color: 1

Bin 106: 20 of cap free
Amount of items: 2
Items: 
Size: 5778 Color: 1
Size: 826 Color: 0

Bin 107: 20 of cap free
Amount of items: 2
Items: 
Size: 5930 Color: 0
Size: 674 Color: 1

Bin 108: 21 of cap free
Amount of items: 2
Items: 
Size: 5458 Color: 0
Size: 1145 Color: 1

Bin 109: 22 of cap free
Amount of items: 2
Items: 
Size: 4300 Color: 0
Size: 2302 Color: 1

Bin 110: 23 of cap free
Amount of items: 2
Items: 
Size: 3844 Color: 0
Size: 2757 Color: 1

Bin 111: 23 of cap free
Amount of items: 2
Items: 
Size: 4093 Color: 1
Size: 2508 Color: 0

Bin 112: 23 of cap free
Amount of items: 2
Items: 
Size: 5606 Color: 0
Size: 995 Color: 1

Bin 113: 29 of cap free
Amount of items: 2
Items: 
Size: 5471 Color: 1
Size: 1124 Color: 0

Bin 114: 31 of cap free
Amount of items: 2
Items: 
Size: 5162 Color: 1
Size: 1431 Color: 0

Bin 115: 31 of cap free
Amount of items: 2
Items: 
Size: 5605 Color: 1
Size: 988 Color: 0

Bin 116: 31 of cap free
Amount of items: 2
Items: 
Size: 5851 Color: 0
Size: 742 Color: 1

Bin 117: 32 of cap free
Amount of items: 2
Items: 
Size: 3964 Color: 1
Size: 2628 Color: 0

Bin 118: 32 of cap free
Amount of items: 2
Items: 
Size: 5470 Color: 1
Size: 1122 Color: 0

Bin 119: 32 of cap free
Amount of items: 2
Items: 
Size: 5569 Color: 0
Size: 1023 Color: 1

Bin 120: 34 of cap free
Amount of items: 2
Items: 
Size: 5298 Color: 1
Size: 1292 Color: 0

Bin 121: 35 of cap free
Amount of items: 2
Items: 
Size: 5211 Color: 1
Size: 1378 Color: 0

Bin 122: 44 of cap free
Amount of items: 2
Items: 
Size: 3316 Color: 1
Size: 3264 Color: 0

Bin 123: 45 of cap free
Amount of items: 2
Items: 
Size: 5602 Color: 0
Size: 977 Color: 1

Bin 124: 51 of cap free
Amount of items: 3
Items: 
Size: 3330 Color: 0
Size: 2751 Color: 0
Size: 492 Color: 1

Bin 125: 55 of cap free
Amount of items: 2
Items: 
Size: 5160 Color: 0
Size: 1409 Color: 1

Bin 126: 56 of cap free
Amount of items: 2
Items: 
Size: 5597 Color: 1
Size: 971 Color: 0

Bin 127: 56 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 0
Size: 788 Color: 1

Bin 128: 58 of cap free
Amount of items: 2
Items: 
Size: 4062 Color: 1
Size: 2504 Color: 0

Bin 129: 58 of cap free
Amount of items: 2
Items: 
Size: 5138 Color: 1
Size: 1428 Color: 0

Bin 130: 95 of cap free
Amount of items: 2
Items: 
Size: 4391 Color: 0
Size: 2138 Color: 1

Bin 131: 97 of cap free
Amount of items: 2
Items: 
Size: 3765 Color: 1
Size: 2762 Color: 0

Bin 132: 110 of cap free
Amount of items: 4
Items: 
Size: 3321 Color: 0
Size: 2111 Color: 1
Size: 642 Color: 1
Size: 440 Color: 0

Bin 133: 4858 of cap free
Amount of items: 14
Items: 
Size: 144 Color: 1
Size: 144 Color: 1
Size: 142 Color: 1
Size: 136 Color: 0
Size: 132 Color: 0
Size: 128 Color: 1
Size: 128 Color: 1
Size: 128 Color: 0
Size: 124 Color: 0
Size: 122 Color: 0
Size: 116 Color: 1
Size: 116 Color: 1
Size: 112 Color: 0
Size: 94 Color: 0

Total size: 874368
Total free space: 6624

