Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 1230 Color: 0
Size: 421 Color: 1
Size: 377 Color: 1
Size: 244 Color: 0
Size: 100 Color: 0
Size: 84 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 0
Size: 828 Color: 0
Size: 72 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 0
Size: 622 Color: 1
Size: 120 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1916 Color: 0
Size: 452 Color: 0
Size: 88 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 0
Size: 380 Color: 1
Size: 56 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2021 Color: 1
Size: 363 Color: 0
Size: 72 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 1
Size: 342 Color: 1
Size: 64 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 0
Size: 212 Color: 0
Size: 176 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 1
Size: 272 Color: 0
Size: 66 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 0
Size: 300 Color: 1
Size: 16 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 1
Size: 168 Color: 0
Size: 100 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 0
Size: 258 Color: 1
Size: 32 Color: 0

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 2202 Color: 0
Size: 226 Color: 1
Size: 20 Color: 1
Size: 8 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 1
Size: 204 Color: 1
Size: 60 Color: 0

Bin 15: 1 of cap free
Amount of items: 5
Items: 
Size: 1236 Color: 0
Size: 514 Color: 1
Size: 339 Color: 1
Size: 280 Color: 0
Size: 86 Color: 0

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 0
Size: 976 Color: 1
Size: 74 Color: 0

Bin 17: 1 of cap free
Amount of items: 5
Items: 
Size: 1644 Color: 1
Size: 439 Color: 0
Size: 292 Color: 0
Size: 40 Color: 1
Size: 40 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1841 Color: 0
Size: 512 Color: 1
Size: 102 Color: 0

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1914 Color: 0
Size: 541 Color: 1

Bin 20: 2 of cap free
Amount of items: 4
Items: 
Size: 1238 Color: 0
Size: 1018 Color: 1
Size: 150 Color: 0
Size: 48 Color: 1

Bin 21: 2 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 621 Color: 1
Size: 160 Color: 0

Bin 22: 2 of cap free
Amount of items: 2
Items: 
Size: 1713 Color: 1
Size: 741 Color: 0

Bin 23: 2 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 1
Size: 330 Color: 0
Size: 16 Color: 1

Bin 24: 2 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 1
Size: 282 Color: 0

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 0
Size: 200 Color: 0
Size: 108 Color: 1

Bin 26: 3 of cap free
Amount of items: 6
Items: 
Size: 1231 Color: 0
Size: 468 Color: 1
Size: 422 Color: 1
Size: 244 Color: 0
Size: 48 Color: 1
Size: 40 Color: 0

Bin 27: 3 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 1
Size: 620 Color: 0
Size: 144 Color: 0

Bin 28: 3 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 0
Size: 684 Color: 1
Size: 40 Color: 1

Bin 29: 3 of cap free
Amount of items: 2
Items: 
Size: 1764 Color: 1
Size: 689 Color: 0

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 1
Size: 324 Color: 1
Size: 204 Color: 0

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 1
Size: 378 Color: 0
Size: 24 Color: 0

Bin 32: 4 of cap free
Amount of items: 2
Items: 
Size: 1430 Color: 1
Size: 1022 Color: 0

Bin 33: 4 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 862 Color: 1
Size: 54 Color: 0

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 1
Size: 850 Color: 0

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 0
Size: 756 Color: 1

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1884 Color: 1
Size: 568 Color: 0

Bin 37: 4 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 0
Size: 384 Color: 1
Size: 32 Color: 0

Bin 38: 5 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 0
Size: 858 Color: 1
Size: 172 Color: 0

Bin 39: 5 of cap free
Amount of items: 3
Items: 
Size: 1951 Color: 0
Size: 484 Color: 1
Size: 16 Color: 1

Bin 40: 6 of cap free
Amount of items: 2
Items: 
Size: 2006 Color: 0
Size: 444 Color: 1

Bin 41: 6 of cap free
Amount of items: 2
Items: 
Size: 2086 Color: 1
Size: 364 Color: 0

Bin 42: 7 of cap free
Amount of items: 2
Items: 
Size: 1586 Color: 0
Size: 863 Color: 1

Bin 43: 7 of cap free
Amount of items: 2
Items: 
Size: 1842 Color: 1
Size: 607 Color: 0

Bin 44: 7 of cap free
Amount of items: 3
Items: 
Size: 1931 Color: 1
Size: 454 Color: 0
Size: 64 Color: 0

Bin 45: 8 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 1
Size: 916 Color: 1
Size: 64 Color: 0

Bin 46: 8 of cap free
Amount of items: 2
Items: 
Size: 1693 Color: 0
Size: 755 Color: 1

Bin 47: 8 of cap free
Amount of items: 2
Items: 
Size: 1935 Color: 1
Size: 513 Color: 0

Bin 48: 8 of cap free
Amount of items: 2
Items: 
Size: 2005 Color: 1
Size: 443 Color: 0

Bin 49: 8 of cap free
Amount of items: 2
Items: 
Size: 2186 Color: 0
Size: 262 Color: 1

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 1426 Color: 1
Size: 1021 Color: 0

Bin 51: 9 of cap free
Amount of items: 3
Items: 
Size: 1551 Color: 1
Size: 840 Color: 0
Size: 56 Color: 1

Bin 52: 9 of cap free
Amount of items: 2
Items: 
Size: 1806 Color: 1
Size: 641 Color: 0

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 1567 Color: 1
Size: 877 Color: 0

Bin 54: 12 of cap free
Amount of items: 2
Items: 
Size: 1807 Color: 0
Size: 637 Color: 1

Bin 55: 12 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 0
Size: 232 Color: 1
Size: 8 Color: 0

Bin 56: 15 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 1
Size: 467 Color: 0

Bin 57: 16 of cap free
Amount of items: 2
Items: 
Size: 1852 Color: 1
Size: 588 Color: 0

Bin 58: 17 of cap free
Amount of items: 2
Items: 
Size: 1853 Color: 0
Size: 586 Color: 1

Bin 59: 18 of cap free
Amount of items: 19
Items: 
Size: 214 Color: 1
Size: 204 Color: 1
Size: 200 Color: 1
Size: 174 Color: 1
Size: 148 Color: 0
Size: 140 Color: 0
Size: 128 Color: 0
Size: 126 Color: 0
Size: 126 Color: 0
Size: 122 Color: 0
Size: 120 Color: 1
Size: 120 Color: 1
Size: 112 Color: 1
Size: 102 Color: 0
Size: 94 Color: 1
Size: 88 Color: 1
Size: 88 Color: 0
Size: 88 Color: 0
Size: 44 Color: 0

Bin 60: 21 of cap free
Amount of items: 2
Items: 
Size: 1932 Color: 1
Size: 503 Color: 0

Bin 61: 24 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 1
Size: 1020 Color: 0
Size: 48 Color: 0

Bin 62: 26 of cap free
Amount of items: 2
Items: 
Size: 1716 Color: 1
Size: 714 Color: 0

Bin 63: 36 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 1
Size: 1023 Color: 1
Size: 168 Color: 0

Bin 64: 39 of cap free
Amount of items: 2
Items: 
Size: 1837 Color: 1
Size: 580 Color: 0

Bin 65: 39 of cap free
Amount of items: 2
Items: 
Size: 1900 Color: 0
Size: 517 Color: 1

Bin 66: 2000 of cap free
Amount of items: 6
Items: 
Size: 88 Color: 0
Size: 88 Color: 0
Size: 88 Color: 0
Size: 80 Color: 1
Size: 72 Color: 1
Size: 40 Color: 1

Total size: 159640
Total free space: 2456

