Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 5
Size: 262 Color: 9
Size: 261 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 326 Color: 6
Size: 317 Color: 12
Size: 357 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 260 Color: 15
Size: 484 Color: 0
Size: 256 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 14
Size: 352 Color: 18
Size: 268 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 18
Size: 350 Color: 17
Size: 282 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 18
Size: 252 Color: 13
Size: 250 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 18
Size: 355 Color: 3
Size: 251 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 352 Color: 11
Size: 282 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 4
Size: 341 Color: 15
Size: 300 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 10
Size: 309 Color: 8
Size: 305 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 3
Size: 358 Color: 19
Size: 257 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 5
Size: 296 Color: 9
Size: 263 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 19
Size: 277 Color: 17
Size: 263 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 5
Size: 309 Color: 5
Size: 260 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 0
Size: 362 Color: 0
Size: 276 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 15
Size: 342 Color: 3
Size: 256 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 12
Size: 263 Color: 14
Size: 250 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 315 Color: 9
Size: 258 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8
Size: 306 Color: 19
Size: 268 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 13
Size: 349 Color: 9
Size: 266 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 290 Color: 6
Size: 274 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 2
Size: 363 Color: 13
Size: 263 Color: 16

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 11
Size: 309 Color: 12
Size: 274 Color: 5

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 5
Size: 340 Color: 9
Size: 260 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 16
Size: 343 Color: 15
Size: 259 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 10
Size: 332 Color: 4
Size: 296 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 347 Color: 1
Size: 301 Color: 18

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 16
Size: 361 Color: 0
Size: 258 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 14
Size: 349 Color: 18
Size: 274 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 359 Color: 10
Size: 275 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 8
Size: 283 Color: 2
Size: 261 Color: 8

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 276 Color: 1
Size: 260 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 2
Size: 296 Color: 3
Size: 250 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 10
Size: 281 Color: 19
Size: 277 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 17
Size: 279 Color: 6
Size: 261 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 2
Size: 314 Color: 0
Size: 269 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 18
Size: 285 Color: 2
Size: 280 Color: 12

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 7
Size: 268 Color: 8
Size: 250 Color: 12

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 13
Size: 335 Color: 4
Size: 289 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 324 Color: 2
Size: 297 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 352 Color: 1
Size: 273 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 11
Size: 302 Color: 7
Size: 255 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 17
Size: 319 Color: 16
Size: 296 Color: 5

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 17
Size: 325 Color: 15
Size: 309 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 5
Size: 316 Color: 8
Size: 269 Color: 12

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 4
Size: 324 Color: 2
Size: 260 Color: 18

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 6
Size: 354 Color: 13
Size: 268 Color: 9

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9
Size: 258 Color: 18
Size: 252 Color: 16

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 327 Color: 14
Size: 295 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 6
Size: 330 Color: 0
Size: 271 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 13
Size: 268 Color: 11
Size: 264 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 12
Size: 268 Color: 19
Size: 260 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 4
Size: 254 Color: 15
Size: 253 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 9
Size: 303 Color: 16
Size: 261 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 3
Size: 308 Color: 14
Size: 252 Color: 7

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 19
Size: 311 Color: 19
Size: 287 Color: 6

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 18
Size: 278 Color: 11
Size: 260 Color: 9

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 19
Size: 281 Color: 3
Size: 254 Color: 16

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 358 Color: 11
Size: 275 Color: 11

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 11
Size: 258 Color: 17
Size: 250 Color: 5

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 0
Size: 352 Color: 18
Size: 295 Color: 17

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 19
Size: 256 Color: 3
Size: 250 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7
Size: 365 Color: 10
Size: 258 Color: 16

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 3
Size: 257 Color: 0
Size: 251 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 13
Size: 328 Color: 5
Size: 286 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 17
Size: 331 Color: 12
Size: 262 Color: 5

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 273 Color: 14
Size: 257 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 4
Size: 288 Color: 6
Size: 277 Color: 11

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 357 Color: 11
Size: 253 Color: 15

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 14
Size: 355 Color: 1
Size: 261 Color: 14

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 11
Size: 291 Color: 7
Size: 282 Color: 19

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 13
Size: 256 Color: 18
Size: 255 Color: 13

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 17
Size: 323 Color: 12
Size: 312 Color: 17

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8
Size: 297 Color: 14
Size: 291 Color: 11

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 6
Size: 326 Color: 12
Size: 259 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 10
Size: 264 Color: 5
Size: 256 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 17
Size: 290 Color: 10
Size: 275 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 339 Color: 13
Size: 263 Color: 10

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 313 Color: 3
Size: 285 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 2
Size: 288 Color: 13
Size: 288 Color: 8

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 15
Size: 275 Color: 4
Size: 254 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 9
Size: 333 Color: 15
Size: 301 Color: 7

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 2
Size: 336 Color: 0
Size: 292 Color: 2

Total size: 83000
Total free space: 0

