Capicity Bin: 2048
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1319 Color: 2
Size: 609 Color: 4
Size: 120 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 1
Size: 551 Color: 2
Size: 108 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 2
Size: 550 Color: 3
Size: 60 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 2
Size: 487 Color: 4
Size: 96 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 3
Size: 542 Color: 3
Size: 36 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 2
Size: 429 Color: 0
Size: 84 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 4
Size: 388 Color: 1
Size: 126 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 4
Size: 424 Color: 4
Size: 42 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 4
Size: 385 Color: 3
Size: 76 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 1
Size: 338 Color: 2
Size: 120 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 0
Size: 342 Color: 2
Size: 68 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 3
Size: 281 Color: 2
Size: 108 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 2
Size: 303 Color: 4
Size: 72 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 1
Size: 246 Color: 2
Size: 120 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 2
Size: 263 Color: 0
Size: 52 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 2
Size: 160 Color: 4
Size: 146 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 1
Size: 262 Color: 3
Size: 48 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 4
Size: 265 Color: 1
Size: 40 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 1
Size: 242 Color: 2
Size: 48 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 0
Size: 213 Color: 4
Size: 54 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 2
Size: 186 Color: 4
Size: 48 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 1
Size: 170 Color: 4
Size: 68 Color: 2

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 1818 Color: 4
Size: 208 Color: 0
Size: 20 Color: 1
Size: 2 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 3
Size: 126 Color: 4
Size: 100 Color: 2

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1169 Color: 0
Size: 802 Color: 2
Size: 76 Color: 3

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1283 Color: 2
Size: 706 Color: 3
Size: 58 Color: 4

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1314 Color: 4
Size: 733 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 2
Size: 463 Color: 2
Size: 168 Color: 4

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 1
Size: 382 Color: 2
Size: 36 Color: 4

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 2
Size: 190 Color: 0
Size: 144 Color: 1

Bin 31: 2 of cap free
Amount of items: 4
Items: 
Size: 1031 Color: 1
Size: 903 Color: 3
Size: 64 Color: 1
Size: 48 Color: 0

Bin 32: 2 of cap free
Amount of items: 4
Items: 
Size: 1280 Color: 3
Size: 682 Color: 1
Size: 48 Color: 2
Size: 36 Color: 0

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1463 Color: 3
Size: 553 Color: 1
Size: 30 Color: 1

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 2
Size: 435 Color: 0
Size: 84 Color: 4

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 1
Size: 472 Color: 3

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1721 Color: 0
Size: 325 Color: 4

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1826 Color: 2
Size: 220 Color: 4

Bin 38: 3 of cap free
Amount of items: 15
Items: 
Size: 313 Color: 1
Size: 258 Color: 3
Size: 198 Color: 2
Size: 194 Color: 0
Size: 168 Color: 0
Size: 132 Color: 2
Size: 124 Color: 0
Size: 108 Color: 4
Size: 96 Color: 4
Size: 96 Color: 3
Size: 88 Color: 3
Size: 86 Color: 2
Size: 76 Color: 4
Size: 72 Color: 2
Size: 36 Color: 3

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 4
Size: 696 Color: 3
Size: 62 Color: 2

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1385 Color: 0
Size: 614 Color: 1
Size: 46 Color: 4

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 3
Size: 351 Color: 0
Size: 20 Color: 0

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1646 Color: 4
Size: 398 Color: 1

Bin 43: 5 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 2
Size: 635 Color: 4
Size: 110 Color: 1

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 3
Size: 357 Color: 0

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1761 Color: 0
Size: 281 Color: 4

Bin 46: 6 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 3
Size: 241 Color: 0
Size: 8 Color: 4

Bin 47: 7 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 4
Size: 731 Color: 3
Size: 76 Color: 1

Bin 48: 9 of cap free
Amount of items: 4
Items: 
Size: 1165 Color: 2
Size: 482 Color: 4
Size: 316 Color: 1
Size: 76 Color: 3

Bin 49: 9 of cap free
Amount of items: 2
Items: 
Size: 1593 Color: 3
Size: 446 Color: 0

Bin 50: 11 of cap free
Amount of items: 2
Items: 
Size: 1398 Color: 3
Size: 639 Color: 1

Bin 51: 11 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 1
Size: 275 Color: 3
Size: 8 Color: 2

Bin 52: 17 of cap free
Amount of items: 5
Items: 
Size: 1025 Color: 1
Size: 390 Color: 0
Size: 378 Color: 4
Size: 202 Color: 0
Size: 36 Color: 3

Bin 53: 17 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 3
Size: 631 Color: 4
Size: 314 Color: 1

Bin 54: 18 of cap free
Amount of items: 2
Items: 
Size: 1202 Color: 4
Size: 828 Color: 3

Bin 55: 19 of cap free
Amount of items: 2
Items: 
Size: 1711 Color: 0
Size: 318 Color: 1

Bin 56: 20 of cap free
Amount of items: 2
Items: 
Size: 1291 Color: 4
Size: 737 Color: 0

Bin 57: 20 of cap free
Amount of items: 2
Items: 
Size: 1598 Color: 1
Size: 430 Color: 2

Bin 58: 21 of cap free
Amount of items: 3
Items: 
Size: 1027 Color: 2
Size: 854 Color: 2
Size: 146 Color: 1

Bin 59: 24 of cap free
Amount of items: 2
Items: 
Size: 1173 Color: 2
Size: 851 Color: 0

Bin 60: 24 of cap free
Amount of items: 2
Items: 
Size: 1514 Color: 4
Size: 510 Color: 3

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1531 Color: 3
Size: 489 Color: 1

Bin 62: 28 of cap free
Amount of items: 3
Items: 
Size: 1621 Color: 4
Size: 273 Color: 3
Size: 126 Color: 3

Bin 63: 29 of cap free
Amount of items: 2
Items: 
Size: 1588 Color: 0
Size: 431 Color: 1

Bin 64: 32 of cap free
Amount of items: 2
Items: 
Size: 1390 Color: 3
Size: 626 Color: 0

Bin 65: 33 of cap free
Amount of items: 3
Items: 
Size: 1026 Color: 0
Size: 849 Color: 2
Size: 140 Color: 1

Bin 66: 1612 of cap free
Amount of items: 7
Items: 
Size: 76 Color: 1
Size: 70 Color: 3
Size: 64 Color: 1
Size: 60 Color: 2
Size: 56 Color: 4
Size: 56 Color: 0
Size: 54 Color: 4

Total size: 133120
Total free space: 2048

