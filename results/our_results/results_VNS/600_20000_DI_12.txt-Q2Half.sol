Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11649 Color: 1
Size: 6667 Color: 1
Size: 1332 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 12690 Color: 1
Size: 4622 Color: 1
Size: 1632 Color: 0
Size: 704 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10258 Color: 1
Size: 7826 Color: 1
Size: 1564 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 9832 Color: 1
Size: 4872 Color: 1
Size: 4104 Color: 0
Size: 840 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 9672 Color: 1
Size: 6084 Color: 1
Size: 2736 Color: 0
Size: 1156 Color: 0

Bin 6: 0 of cap free
Amount of items: 5
Items: 
Size: 9827 Color: 1
Size: 5495 Color: 1
Size: 2406 Color: 1
Size: 1632 Color: 0
Size: 288 Color: 0

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 9880 Color: 1
Size: 6896 Color: 1
Size: 1540 Color: 0
Size: 1332 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 9828 Color: 1
Size: 8188 Color: 1
Size: 1632 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 9826 Color: 1
Size: 8186 Color: 1
Size: 1636 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 9840 Color: 1
Size: 8176 Color: 1
Size: 1632 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 9904 Color: 1
Size: 8144 Color: 1
Size: 1600 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10402 Color: 1
Size: 7706 Color: 1
Size: 1540 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11280 Color: 1
Size: 6992 Color: 1
Size: 1376 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 1
Size: 6968 Color: 1
Size: 1376 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11544 Color: 1
Size: 6760 Color: 1
Size: 1344 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11634 Color: 1
Size: 6378 Color: 1
Size: 1636 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 1
Size: 6642 Color: 1
Size: 1356 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11698 Color: 1
Size: 6626 Color: 1
Size: 1324 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11714 Color: 1
Size: 6996 Color: 1
Size: 938 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 1
Size: 6624 Color: 1
Size: 736 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12448 Color: 1
Size: 6720 Color: 1
Size: 480 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12546 Color: 1
Size: 5922 Color: 1
Size: 1180 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 1
Size: 5848 Color: 1
Size: 1152 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12728 Color: 1
Size: 5388 Color: 1
Size: 1532 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12986 Color: 1
Size: 5554 Color: 1
Size: 1108 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 1
Size: 6300 Color: 1
Size: 256 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13152 Color: 1
Size: 6048 Color: 1
Size: 448 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13427 Color: 1
Size: 5185 Color: 1
Size: 1036 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13456 Color: 1
Size: 5168 Color: 1
Size: 1024 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13552 Color: 1
Size: 5904 Color: 1
Size: 192 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13568 Color: 1
Size: 5472 Color: 1
Size: 608 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13728 Color: 1
Size: 4960 Color: 1
Size: 960 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13756 Color: 1
Size: 4724 Color: 1
Size: 1168 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13814 Color: 1
Size: 4862 Color: 1
Size: 972 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 1
Size: 4424 Color: 1
Size: 1408 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 1
Size: 4952 Color: 1
Size: 804 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 1
Size: 4724 Color: 1
Size: 936 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 1
Size: 4804 Color: 1
Size: 824 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 1
Size: 4668 Color: 1
Size: 928 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14064 Color: 1
Size: 4656 Color: 1
Size: 928 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14112 Color: 1
Size: 5016 Color: 1
Size: 520 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14166 Color: 1
Size: 4570 Color: 1
Size: 912 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14313 Color: 1
Size: 4447 Color: 1
Size: 888 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 1
Size: 4240 Color: 1
Size: 832 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 1
Size: 4204 Color: 1
Size: 832 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 1
Size: 4200 Color: 1
Size: 832 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 1
Size: 4168 Color: 1
Size: 816 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14666 Color: 1
Size: 4154 Color: 1
Size: 828 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14714 Color: 1
Size: 3586 Color: 1
Size: 1348 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14751 Color: 1
Size: 4081 Color: 1
Size: 816 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14760 Color: 1
Size: 4136 Color: 1
Size: 752 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14780 Color: 1
Size: 3524 Color: 1
Size: 1344 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14810 Color: 1
Size: 3914 Color: 1
Size: 924 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14816 Color: 1
Size: 4064 Color: 1
Size: 768 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14876 Color: 1
Size: 3996 Color: 1
Size: 776 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14892 Color: 1
Size: 4140 Color: 1
Size: 616 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14901 Color: 1
Size: 3957 Color: 1
Size: 790 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 15005 Color: 1
Size: 3871 Color: 1
Size: 772 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 15018 Color: 1
Size: 3546 Color: 1
Size: 1084 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 15136 Color: 1
Size: 4096 Color: 1
Size: 416 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 15240 Color: 1
Size: 3688 Color: 1
Size: 720 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 15264 Color: 1
Size: 3680 Color: 1
Size: 704 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 15336 Color: 1
Size: 3608 Color: 1
Size: 704 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 15346 Color: 1
Size: 3878 Color: 1
Size: 424 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 15392 Color: 1
Size: 3552 Color: 1
Size: 704 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 15420 Color: 1
Size: 2904 Color: 1
Size: 1324 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 15440 Color: 1
Size: 3536 Color: 1
Size: 672 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 15476 Color: 1
Size: 3784 Color: 1
Size: 388 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 15520 Color: 1
Size: 3792 Color: 1
Size: 336 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 15682 Color: 1
Size: 2974 Color: 1
Size: 992 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 15696 Color: 1
Size: 3312 Color: 1
Size: 640 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 15730 Color: 1
Size: 3116 Color: 1
Size: 802 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 15820 Color: 1
Size: 3196 Color: 1
Size: 632 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 15874 Color: 1
Size: 3146 Color: 1
Size: 628 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 15920 Color: 1
Size: 3208 Color: 1
Size: 520 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 15948 Color: 1
Size: 3084 Color: 1
Size: 616 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 15964 Color: 1
Size: 3076 Color: 1
Size: 608 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 16008 Color: 1
Size: 2820 Color: 1
Size: 820 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 16072 Color: 1
Size: 2984 Color: 1
Size: 592 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 16082 Color: 1
Size: 2612 Color: 1
Size: 954 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 16168 Color: 1
Size: 1924 Color: 1
Size: 1556 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 16224 Color: 1
Size: 3072 Color: 1
Size: 352 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 16240 Color: 1
Size: 2912 Color: 1
Size: 496 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 16268 Color: 1
Size: 3120 Color: 1
Size: 260 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 16288 Color: 1
Size: 3048 Color: 1
Size: 312 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 16368 Color: 1
Size: 2864 Color: 1
Size: 416 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 16430 Color: 1
Size: 2682 Color: 1
Size: 536 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 16484 Color: 1
Size: 2644 Color: 1
Size: 520 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 16496 Color: 1
Size: 3024 Color: 1
Size: 128 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 16500 Color: 1
Size: 2448 Color: 1
Size: 700 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 16516 Color: 1
Size: 2588 Color: 1
Size: 544 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 16548 Color: 1
Size: 2540 Color: 1
Size: 560 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 16584 Color: 1
Size: 2412 Color: 1
Size: 652 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 16642 Color: 1
Size: 2298 Color: 1
Size: 708 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 16680 Color: 1
Size: 2568 Color: 1
Size: 400 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 16720 Color: 1
Size: 1744 Color: 1
Size: 1184 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 16730 Color: 1
Size: 2518 Color: 1
Size: 400 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 16756 Color: 1
Size: 1928 Color: 1
Size: 964 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 16864 Color: 1
Size: 2272 Color: 1
Size: 512 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 16920 Color: 1
Size: 2248 Color: 1
Size: 480 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 16928 Color: 1
Size: 2400 Color: 1
Size: 320 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 16930 Color: 1
Size: 2142 Color: 1
Size: 576 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 16956 Color: 1
Size: 2628 Color: 1
Size: 64 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 17000 Color: 1
Size: 2224 Color: 1
Size: 424 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 17008 Color: 1
Size: 2096 Color: 1
Size: 544 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 17028 Color: 1
Size: 2072 Color: 1
Size: 548 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 17056 Color: 1
Size: 1824 Color: 1
Size: 768 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 17076 Color: 1
Size: 2108 Color: 1
Size: 464 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 17082 Color: 1
Size: 1954 Color: 1
Size: 612 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 17096 Color: 1
Size: 2488 Color: 1
Size: 64 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 17124 Color: 1
Size: 2148 Color: 1
Size: 376 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 17136 Color: 1
Size: 2192 Color: 1
Size: 320 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 17190 Color: 1
Size: 2050 Color: 1
Size: 408 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 17264 Color: 1
Size: 2240 Color: 1
Size: 144 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 17306 Color: 1
Size: 1904 Color: 1
Size: 438 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 17336 Color: 1
Size: 1856 Color: 1
Size: 456 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 17340 Color: 1
Size: 1892 Color: 1
Size: 416 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 17352 Color: 1
Size: 2208 Color: 1
Size: 88 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 17370 Color: 1
Size: 1902 Color: 1
Size: 376 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 17380 Color: 1
Size: 1768 Color: 1
Size: 500 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 17392 Color: 1
Size: 2176 Color: 1
Size: 80 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 17428 Color: 1
Size: 1852 Color: 1
Size: 368 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 17446 Color: 1
Size: 1838 Color: 1
Size: 364 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 17494 Color: 1
Size: 1798 Color: 1
Size: 356 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 17534 Color: 1
Size: 1846 Color: 1
Size: 268 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 17536 Color: 1
Size: 1320 Color: 0
Size: 792 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 17544 Color: 1
Size: 1688 Color: 1
Size: 416 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 17558 Color: 1
Size: 2042 Color: 1
Size: 48 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 17574 Color: 1
Size: 1730 Color: 1
Size: 344 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 17584 Color: 1
Size: 1600 Color: 1
Size: 464 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 17608 Color: 1
Size: 1704 Color: 1
Size: 336 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 17632 Color: 1
Size: 1696 Color: 1
Size: 320 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 17636 Color: 1
Size: 1684 Color: 1
Size: 328 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 17640 Color: 1
Size: 1072 Color: 0
Size: 936 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 17670 Color: 1
Size: 1650 Color: 1
Size: 328 Color: 0

Bin 136: 1 of cap free
Amount of items: 3
Items: 
Size: 11554 Color: 1
Size: 7141 Color: 1
Size: 952 Color: 0

Bin 137: 1 of cap free
Amount of items: 3
Items: 
Size: 12259 Color: 1
Size: 7132 Color: 1
Size: 256 Color: 0

Bin 138: 1 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 1
Size: 6159 Color: 1
Size: 896 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 12631 Color: 1
Size: 5768 Color: 1
Size: 1248 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 13122 Color: 1
Size: 5849 Color: 1
Size: 676 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 13188 Color: 1
Size: 5483 Color: 1
Size: 976 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 1
Size: 4777 Color: 1
Size: 1230 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 13865 Color: 1
Size: 5334 Color: 1
Size: 448 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 14013 Color: 1
Size: 5442 Color: 1
Size: 192 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 1
Size: 4017 Color: 1
Size: 1528 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 14829 Color: 1
Size: 4034 Color: 1
Size: 784 Color: 0

Bin 147: 1 of cap free
Amount of items: 4
Items: 
Size: 13917 Color: 1
Size: 2758 Color: 1
Size: 1548 Color: 0
Size: 1424 Color: 0

Bin 148: 2 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 1
Size: 7666 Color: 1
Size: 888 Color: 0

Bin 149: 2 of cap free
Amount of items: 3
Items: 
Size: 11260 Color: 1
Size: 7682 Color: 1
Size: 704 Color: 0

Bin 150: 2 of cap free
Amount of items: 3
Items: 
Size: 11408 Color: 1
Size: 6706 Color: 1
Size: 1532 Color: 0

Bin 151: 2 of cap free
Amount of items: 3
Items: 
Size: 11602 Color: 1
Size: 7660 Color: 1
Size: 384 Color: 0

Bin 152: 2 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 1
Size: 6786 Color: 1
Size: 512 Color: 0

Bin 153: 2 of cap free
Amount of items: 3
Items: 
Size: 13028 Color: 1
Size: 5978 Color: 1
Size: 640 Color: 0

Bin 154: 2 of cap free
Amount of items: 3
Items: 
Size: 13058 Color: 1
Size: 5524 Color: 1
Size: 1064 Color: 0

Bin 155: 2 of cap free
Amount of items: 3
Items: 
Size: 14133 Color: 1
Size: 4697 Color: 1
Size: 816 Color: 0

Bin 156: 2 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 1
Size: 4544 Color: 1
Size: 512 Color: 0

Bin 157: 2 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 1
Size: 4822 Color: 1
Size: 96 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 14866 Color: 1
Size: 3862 Color: 1
Size: 918 Color: 0

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 14940 Color: 1
Size: 4114 Color: 1
Size: 592 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 15048 Color: 1
Size: 3266 Color: 1
Size: 1332 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 15120 Color: 1
Size: 4142 Color: 1
Size: 384 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 15442 Color: 1
Size: 3488 Color: 1
Size: 716 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 15474 Color: 1
Size: 3484 Color: 1
Size: 688 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 15800 Color: 1
Size: 2506 Color: 1
Size: 1340 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 16342 Color: 1
Size: 2280 Color: 1
Size: 1024 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 15916 Color: 1
Size: 3506 Color: 1
Size: 224 Color: 0

Bin 167: 3 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 1
Size: 8187 Color: 1
Size: 1152 Color: 0

Bin 168: 3 of cap free
Amount of items: 3
Items: 
Size: 11081 Color: 1
Size: 8180 Color: 1
Size: 384 Color: 0

Bin 169: 3 of cap free
Amount of items: 3
Items: 
Size: 13069 Color: 1
Size: 5872 Color: 1
Size: 704 Color: 0

Bin 170: 4 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 1
Size: 6666 Color: 1
Size: 384 Color: 0

Bin 171: 4 of cap free
Amount of items: 3
Items: 
Size: 9836 Color: 1
Size: 6000 Color: 1
Size: 3808 Color: 0

Bin 172: 4 of cap free
Amount of items: 3
Items: 
Size: 10466 Color: 1
Size: 7786 Color: 1
Size: 1392 Color: 0

Bin 173: 4 of cap free
Amount of items: 3
Items: 
Size: 11100 Color: 1
Size: 7328 Color: 1
Size: 1216 Color: 0

Bin 174: 4 of cap free
Amount of items: 3
Items: 
Size: 11682 Color: 1
Size: 6682 Color: 1
Size: 1280 Color: 0

Bin 175: 4 of cap free
Amount of items: 3
Items: 
Size: 13250 Color: 1
Size: 5802 Color: 1
Size: 592 Color: 0

Bin 176: 4 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 1
Size: 5284 Color: 1
Size: 640 Color: 0

Bin 177: 4 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 1
Size: 4640 Color: 1
Size: 320 Color: 0

Bin 178: 4 of cap free
Amount of items: 3
Items: 
Size: 15112 Color: 1
Size: 3924 Color: 1
Size: 608 Color: 0

Bin 179: 6 of cap free
Amount of items: 3
Items: 
Size: 10386 Color: 1
Size: 8160 Color: 1
Size: 1096 Color: 0

Bin 180: 6 of cap free
Amount of items: 3
Items: 
Size: 10450 Color: 1
Size: 8040 Color: 1
Size: 1152 Color: 0

Bin 181: 6 of cap free
Amount of items: 3
Items: 
Size: 11448 Color: 1
Size: 7746 Color: 1
Size: 448 Color: 0

Bin 182: 6 of cap free
Amount of items: 3
Items: 
Size: 11744 Color: 1
Size: 6746 Color: 1
Size: 1152 Color: 0

Bin 183: 8 of cap free
Amount of items: 3
Items: 
Size: 10354 Color: 1
Size: 8902 Color: 1
Size: 384 Color: 0

Bin 184: 8 of cap free
Amount of items: 3
Items: 
Size: 11506 Color: 1
Size: 7654 Color: 1
Size: 480 Color: 0

Bin 185: 8 of cap free
Amount of items: 2
Items: 
Size: 17504 Color: 1
Size: 2136 Color: 0

Bin 186: 9 of cap free
Amount of items: 3
Items: 
Size: 9834 Color: 1
Size: 8169 Color: 1
Size: 1636 Color: 0

Bin 187: 15 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 1
Size: 8185 Color: 1
Size: 1600 Color: 0

Bin 188: 20 of cap free
Amount of items: 3
Items: 
Size: 12464 Color: 1
Size: 6252 Color: 1
Size: 912 Color: 0

Bin 189: 20 of cap free
Amount of items: 3
Items: 
Size: 12148 Color: 1
Size: 6840 Color: 1
Size: 640 Color: 0

Bin 190: 24 of cap free
Amount of items: 3
Items: 
Size: 9888 Color: 1
Size: 9608 Color: 1
Size: 128 Color: 0

Bin 191: 30 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 1
Size: 8184 Color: 1
Size: 1426 Color: 0

Bin 192: 46 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 1
Size: 7808 Color: 1
Size: 1360 Color: 0

Bin 193: 54 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 1
Size: 7722 Color: 1
Size: 960 Color: 0

Bin 194: 66 of cap free
Amount of items: 3
Items: 
Size: 15394 Color: 1
Size: 3964 Color: 1
Size: 224 Color: 0

Bin 195: 93 of cap free
Amount of items: 3
Items: 
Size: 13862 Color: 1
Size: 4597 Color: 1
Size: 1096 Color: 0

Bin 196: 126 of cap free
Amount of items: 3
Items: 
Size: 16894 Color: 1
Size: 2244 Color: 1
Size: 384 Color: 0

Bin 197: 361 of cap free
Amount of items: 3
Items: 
Size: 9825 Color: 1
Size: 6614 Color: 1
Size: 2848 Color: 0

Bin 198: 3211 of cap free
Amount of items: 3
Items: 
Size: 11041 Color: 1
Size: 4692 Color: 1
Size: 704 Color: 0

Bin 199: 15430 of cap free
Amount of items: 1
Items: 
Size: 4218 Color: 1

Total size: 3890304
Total free space: 19648

