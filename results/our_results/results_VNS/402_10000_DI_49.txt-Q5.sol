Capicity Bin: 7904
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3957 Color: 4
Size: 1384 Color: 0
Size: 1011 Color: 3
Size: 684 Color: 1
Size: 476 Color: 1
Size: 216 Color: 3
Size: 176 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3960 Color: 4
Size: 3288 Color: 1
Size: 656 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3964 Color: 0
Size: 3284 Color: 0
Size: 656 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4498 Color: 2
Size: 2842 Color: 0
Size: 564 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 4
Size: 2484 Color: 3
Size: 488 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 4
Size: 2084 Color: 0
Size: 488 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5407 Color: 4
Size: 2081 Color: 0
Size: 416 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5972 Color: 0
Size: 1804 Color: 2
Size: 128 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5964 Color: 4
Size: 1460 Color: 0
Size: 480 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6094 Color: 4
Size: 1510 Color: 0
Size: 300 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6233 Color: 3
Size: 1501 Color: 0
Size: 170 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 1
Size: 1322 Color: 0
Size: 260 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 0
Size: 1202 Color: 3
Size: 240 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6468 Color: 0
Size: 1004 Color: 1
Size: 432 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6490 Color: 0
Size: 1272 Color: 2
Size: 142 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 0
Size: 1204 Color: 2
Size: 196 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6525 Color: 0
Size: 1043 Color: 4
Size: 336 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 2
Size: 1074 Color: 1
Size: 240 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6616 Color: 3
Size: 806 Color: 3
Size: 482 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 4
Size: 1054 Color: 0
Size: 208 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 0
Size: 778 Color: 2
Size: 408 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6712 Color: 3
Size: 852 Color: 0
Size: 340 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6720 Color: 1
Size: 1000 Color: 0
Size: 184 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6781 Color: 0
Size: 767 Color: 4
Size: 356 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 2
Size: 948 Color: 0
Size: 208 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6834 Color: 0
Size: 842 Color: 2
Size: 228 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6837 Color: 0
Size: 827 Color: 1
Size: 240 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6790 Color: 3
Size: 894 Color: 3
Size: 220 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6880 Color: 0
Size: 880 Color: 3
Size: 144 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6884 Color: 0
Size: 742 Color: 1
Size: 278 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6875 Color: 1
Size: 765 Color: 3
Size: 264 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6906 Color: 0
Size: 834 Color: 2
Size: 164 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 2
Size: 804 Color: 0
Size: 202 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6948 Color: 0
Size: 552 Color: 2
Size: 404 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6936 Color: 3
Size: 952 Color: 1
Size: 16 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 0
Size: 792 Color: 1
Size: 140 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6939 Color: 2
Size: 805 Color: 1
Size: 160 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6984 Color: 3
Size: 880 Color: 4
Size: 40 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 1
Size: 880 Color: 0
Size: 38 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6994 Color: 3
Size: 762 Color: 1
Size: 148 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 4
Size: 478 Color: 0
Size: 408 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7084 Color: 2
Size: 560 Color: 0
Size: 260 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 3
Size: 780 Color: 4
Size: 16 Color: 0

Bin 44: 1 of cap free
Amount of items: 7
Items: 
Size: 3956 Color: 3
Size: 990 Color: 0
Size: 937 Color: 0
Size: 828 Color: 3
Size: 480 Color: 2
Size: 468 Color: 1
Size: 244 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 0
Size: 2395 Color: 4
Size: 160 Color: 1

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5352 Color: 2
Size: 2411 Color: 1
Size: 140 Color: 2

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5673 Color: 1
Size: 2038 Color: 3
Size: 192 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5746 Color: 1
Size: 1681 Color: 4
Size: 476 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 5842 Color: 1
Size: 1845 Color: 0
Size: 216 Color: 4

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6053 Color: 3
Size: 1654 Color: 0
Size: 196 Color: 2

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 4
Size: 1151 Color: 0
Size: 180 Color: 4

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 2
Size: 859 Color: 4
Size: 424 Color: 0

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6691 Color: 4
Size: 1028 Color: 3
Size: 184 Color: 0

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 6723 Color: 2
Size: 1180 Color: 1

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6913 Color: 4
Size: 656 Color: 0
Size: 334 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 4958 Color: 4
Size: 2656 Color: 1
Size: 288 Color: 0

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5290 Color: 4
Size: 2468 Color: 0
Size: 144 Color: 1

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5472 Color: 0
Size: 2132 Color: 2
Size: 298 Color: 1

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 2
Size: 2182 Color: 0
Size: 244 Color: 2

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 4
Size: 2132 Color: 3
Size: 164 Color: 4

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5732 Color: 4
Size: 1802 Color: 0
Size: 368 Color: 1

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 6105 Color: 0
Size: 1709 Color: 1
Size: 88 Color: 4

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 0
Size: 834 Color: 1
Size: 714 Color: 4

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 6332 Color: 2
Size: 1026 Color: 2
Size: 544 Color: 3

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 1
Size: 1176 Color: 3

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 6938 Color: 3
Size: 964 Color: 2

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 4488 Color: 1
Size: 3181 Color: 4
Size: 232 Color: 0

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 4976 Color: 0
Size: 2761 Color: 3
Size: 164 Color: 2

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5380 Color: 2
Size: 2385 Color: 0
Size: 136 Color: 3

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 6431 Color: 0
Size: 1294 Color: 2
Size: 176 Color: 3

Bin 71: 3 of cap free
Amount of items: 2
Items: 
Size: 6907 Color: 3
Size: 994 Color: 1

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 3
Size: 985 Color: 2

Bin 73: 3 of cap free
Amount of items: 2
Items: 
Size: 6987 Color: 3
Size: 914 Color: 2

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 5000 Color: 1
Size: 2484 Color: 0
Size: 416 Color: 2

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 5334 Color: 2
Size: 2148 Color: 0
Size: 418 Color: 1

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 6088 Color: 3
Size: 1812 Color: 4

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 6166 Color: 3
Size: 1734 Color: 4

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 6820 Color: 1
Size: 1080 Color: 4

Bin 79: 4 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 2
Size: 850 Color: 4

Bin 80: 5 of cap free
Amount of items: 9
Items: 
Size: 3954 Color: 0
Size: 891 Color: 0
Size: 776 Color: 2
Size: 668 Color: 4
Size: 658 Color: 2
Size: 328 Color: 1
Size: 320 Color: 1
Size: 160 Color: 1
Size: 144 Color: 2

Bin 81: 5 of cap free
Amount of items: 3
Items: 
Size: 3953 Color: 2
Size: 3290 Color: 2
Size: 656 Color: 3

Bin 82: 5 of cap free
Amount of items: 2
Items: 
Size: 6583 Color: 3
Size: 1316 Color: 4

Bin 83: 6 of cap free
Amount of items: 3
Items: 
Size: 6248 Color: 0
Size: 1450 Color: 3
Size: 200 Color: 3

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 5011 Color: 0
Size: 2742 Color: 3
Size: 144 Color: 4

Bin 85: 7 of cap free
Amount of items: 3
Items: 
Size: 5043 Color: 4
Size: 2494 Color: 3
Size: 360 Color: 1

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 6341 Color: 0
Size: 1324 Color: 3
Size: 232 Color: 2

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 5880 Color: 4
Size: 1688 Color: 4
Size: 328 Color: 2

Bin 88: 9 of cap free
Amount of items: 3
Items: 
Size: 4500 Color: 0
Size: 3003 Color: 1
Size: 392 Color: 4

Bin 89: 9 of cap free
Amount of items: 3
Items: 
Size: 5444 Color: 3
Size: 2099 Color: 2
Size: 352 Color: 0

Bin 90: 10 of cap free
Amount of items: 25
Items: 
Size: 656 Color: 2
Size: 600 Color: 4
Size: 568 Color: 2
Size: 560 Color: 0
Size: 480 Color: 4
Size: 384 Color: 4
Size: 368 Color: 0
Size: 332 Color: 4
Size: 320 Color: 3
Size: 288 Color: 4
Size: 288 Color: 1
Size: 272 Color: 0
Size: 268 Color: 1
Size: 256 Color: 3
Size: 256 Color: 3
Size: 224 Color: 3
Size: 224 Color: 3
Size: 212 Color: 0
Size: 208 Color: 2
Size: 208 Color: 1
Size: 192 Color: 3
Size: 192 Color: 3
Size: 186 Color: 2
Size: 176 Color: 1
Size: 176 Color: 1

Bin 91: 10 of cap free
Amount of items: 2
Items: 
Size: 6886 Color: 3
Size: 1008 Color: 1

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 7086 Color: 2
Size: 808 Color: 1

Bin 93: 11 of cap free
Amount of items: 3
Items: 
Size: 5889 Color: 1
Size: 1896 Color: 4
Size: 108 Color: 0

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 1
Size: 1393 Color: 2

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 6985 Color: 1
Size: 908 Color: 4

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 5922 Color: 1
Size: 1970 Color: 3

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 6676 Color: 4
Size: 1216 Color: 2

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 6776 Color: 3
Size: 1116 Color: 1

Bin 99: 14 of cap free
Amount of items: 2
Items: 
Size: 6708 Color: 2
Size: 1182 Color: 3

Bin 100: 16 of cap free
Amount of items: 3
Items: 
Size: 5640 Color: 2
Size: 2164 Color: 1
Size: 84 Color: 0

Bin 101: 18 of cap free
Amount of items: 2
Items: 
Size: 5462 Color: 4
Size: 2424 Color: 2

Bin 102: 18 of cap free
Amount of items: 2
Items: 
Size: 6062 Color: 1
Size: 1824 Color: 2

Bin 103: 18 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 4
Size: 1612 Color: 1

Bin 104: 18 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 2
Size: 1204 Color: 1
Size: 64 Color: 0

Bin 105: 18 of cap free
Amount of items: 2
Items: 
Size: 6810 Color: 4
Size: 1076 Color: 3

Bin 106: 19 of cap free
Amount of items: 4
Items: 
Size: 4479 Color: 0
Size: 2052 Color: 0
Size: 1098 Color: 1
Size: 256 Color: 2

Bin 107: 19 of cap free
Amount of items: 3
Items: 
Size: 4942 Color: 2
Size: 2655 Color: 0
Size: 288 Color: 3

Bin 108: 20 of cap free
Amount of items: 2
Items: 
Size: 5691 Color: 2
Size: 2193 Color: 4

Bin 109: 20 of cap free
Amount of items: 2
Items: 
Size: 5748 Color: 2
Size: 2136 Color: 3

Bin 110: 20 of cap free
Amount of items: 3
Items: 
Size: 6164 Color: 0
Size: 1632 Color: 2
Size: 88 Color: 2

Bin 111: 21 of cap free
Amount of items: 2
Items: 
Size: 5031 Color: 3
Size: 2852 Color: 1

Bin 112: 22 of cap free
Amount of items: 3
Items: 
Size: 4301 Color: 0
Size: 3293 Color: 1
Size: 288 Color: 2

Bin 113: 22 of cap free
Amount of items: 2
Items: 
Size: 6653 Color: 1
Size: 1229 Color: 4

Bin 114: 25 of cap free
Amount of items: 3
Items: 
Size: 4591 Color: 1
Size: 2856 Color: 0
Size: 432 Color: 3

Bin 115: 26 of cap free
Amount of items: 2
Items: 
Size: 6156 Color: 1
Size: 1722 Color: 2

Bin 116: 27 of cap free
Amount of items: 2
Items: 
Size: 6968 Color: 0
Size: 909 Color: 1

Bin 117: 31 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 3
Size: 1101 Color: 1

Bin 118: 48 of cap free
Amount of items: 2
Items: 
Size: 5412 Color: 4
Size: 2444 Color: 1

Bin 119: 51 of cap free
Amount of items: 3
Items: 
Size: 3958 Color: 3
Size: 3471 Color: 1
Size: 424 Color: 3

Bin 120: 53 of cap free
Amount of items: 2
Items: 
Size: 4769 Color: 3
Size: 3082 Color: 1

Bin 121: 54 of cap free
Amount of items: 3
Items: 
Size: 4924 Color: 2
Size: 2430 Color: 0
Size: 496 Color: 3

Bin 122: 59 of cap free
Amount of items: 2
Items: 
Size: 5387 Color: 3
Size: 2458 Color: 4

Bin 123: 60 of cap free
Amount of items: 2
Items: 
Size: 6316 Color: 2
Size: 1528 Color: 4

Bin 124: 60 of cap free
Amount of items: 2
Items: 
Size: 6392 Color: 1
Size: 1452 Color: 2

Bin 125: 61 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 1
Size: 1620 Color: 4
Size: 1303 Color: 0

Bin 126: 64 of cap free
Amount of items: 3
Items: 
Size: 5308 Color: 0
Size: 2044 Color: 2
Size: 488 Color: 1

Bin 127: 82 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 1
Size: 1362 Color: 4

Bin 128: 86 of cap free
Amount of items: 2
Items: 
Size: 4618 Color: 1
Size: 3200 Color: 2

Bin 129: 112 of cap free
Amount of items: 2
Items: 
Size: 4948 Color: 4
Size: 2844 Color: 1

Bin 130: 120 of cap free
Amount of items: 2
Items: 
Size: 4490 Color: 3
Size: 3294 Color: 2

Bin 131: 128 of cap free
Amount of items: 2
Items: 
Size: 4484 Color: 0
Size: 3292 Color: 2

Bin 132: 132 of cap free
Amount of items: 2
Items: 
Size: 4980 Color: 1
Size: 2792 Color: 4

Bin 133: 6096 of cap free
Amount of items: 11
Items: 
Size: 192 Color: 0
Size: 176 Color: 1
Size: 176 Color: 1
Size: 168 Color: 1
Size: 168 Color: 0
Size: 160 Color: 2
Size: 160 Color: 2
Size: 152 Color: 4
Size: 152 Color: 4
Size: 152 Color: 2
Size: 152 Color: 0

Total size: 1043328
Total free space: 7904

