Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 328
Size: 356 Color: 323
Size: 287 Color: 212

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 357
Size: 367 Color: 346
Size: 258 Color: 88

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 419
Size: 313 Color: 263
Size: 254 Color: 62

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 459
Size: 268 Color: 139
Size: 259 Color: 101

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 424
Size: 293 Color: 227
Size: 270 Color: 148

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 460
Size: 273 Color: 163
Size: 254 Color: 59

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 494
Size: 254 Color: 61
Size: 250 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 379
Size: 350 Color: 310
Size: 256 Color: 76

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 388
Size: 338 Color: 291
Size: 258 Color: 91

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 398
Size: 324 Color: 275
Size: 261 Color: 110

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 412
Size: 318 Color: 269
Size: 259 Color: 95

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 334
Size: 346 Color: 304
Size: 292 Color: 226

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 384
Size: 314 Color: 265
Size: 287 Color: 214

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 302 Color: 250
Size: 426 Color: 417
Size: 272 Color: 159

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 437
Size: 287 Color: 210
Size: 267 Color: 137

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 337
Size: 362 Color: 333
Size: 275 Color: 182

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 468
Size: 270 Color: 147
Size: 251 Color: 20

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 285 Color: 208
Size: 259 Color: 100
Size: 456 Color: 444

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 471
Size: 262 Color: 116
Size: 258 Color: 82

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 359
Size: 342 Color: 297
Size: 282 Color: 200

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 418
Size: 296 Color: 235
Size: 274 Color: 174

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 482
Size: 258 Color: 89
Size: 254 Color: 58

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 491
Size: 253 Color: 48
Size: 252 Color: 26

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 382
Size: 350 Color: 314
Size: 253 Color: 41

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 363
Size: 343 Color: 300
Size: 276 Color: 186

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 493
Size: 253 Color: 36
Size: 251 Color: 22

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 352
Size: 337 Color: 290
Size: 291 Color: 224

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 394
Size: 321 Color: 273
Size: 269 Color: 141

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 477
Size: 263 Color: 120
Size: 252 Color: 33

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 470
Size: 261 Color: 111
Size: 259 Color: 96

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 488
Size: 254 Color: 52
Size: 253 Color: 39

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 481
Size: 259 Color: 92
Size: 253 Color: 43

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 353
Size: 369 Color: 348
Size: 259 Color: 104

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 351
Size: 340 Color: 293
Size: 290 Color: 219

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 421
Size: 303 Color: 251
Size: 263 Color: 119

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 365
Size: 361 Color: 332
Size: 258 Color: 86

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 396
Size: 307 Color: 254
Size: 280 Color: 190

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 425
Size: 306 Color: 253
Size: 255 Color: 71

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 469
Size: 264 Color: 125
Size: 256 Color: 74

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 446
Size: 282 Color: 198
Size: 259 Color: 97

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 492
Size: 253 Color: 38
Size: 252 Color: 24

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 429
Size: 298 Color: 241
Size: 259 Color: 103

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 411
Size: 320 Color: 271
Size: 258 Color: 90

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 458
Size: 278 Color: 189
Size: 250 Color: 11

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 420
Size: 285 Color: 207
Size: 282 Color: 197

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 372
Size: 317 Color: 267
Size: 291 Color: 223

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 402
Size: 310 Color: 259
Size: 272 Color: 161

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 453
Size: 273 Color: 164
Size: 260 Color: 106

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 341
Size: 359 Color: 330
Size: 275 Color: 179

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 484
Size: 256 Color: 73
Size: 254 Color: 56

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 413
Size: 318 Color: 270
Size: 257 Color: 80

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 343
Size: 363 Color: 336
Size: 271 Color: 152

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 318
Size: 351 Color: 316
Size: 297 Color: 237

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 486
Size: 259 Color: 98
Size: 250 Color: 8

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 356
Size: 333 Color: 285
Size: 292 Color: 225

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 436
Size: 299 Color: 245
Size: 255 Color: 72

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 361
Size: 366 Color: 344
Size: 257 Color: 78

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 392
Size: 318 Color: 268
Size: 275 Color: 180

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 465
Size: 262 Color: 117
Size: 260 Color: 108

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 500
Size: 252 Color: 28
Size: 250 Color: 12

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 464
Size: 267 Color: 135
Size: 255 Color: 64

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 321
Size: 347 Color: 306
Size: 298 Color: 242

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 474
Size: 263 Color: 121
Size: 253 Color: 44

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 478
Size: 260 Color: 105
Size: 255 Color: 69

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 408
Size: 308 Color: 258
Size: 272 Color: 162

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 490
Size: 254 Color: 50
Size: 251 Color: 19

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 475
Size: 262 Color: 113
Size: 253 Color: 45

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 445
Size: 283 Color: 202
Size: 260 Color: 107

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 467
Size: 269 Color: 144
Size: 252 Color: 27

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 497
Size: 253 Color: 40
Size: 250 Color: 9

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 495
Size: 253 Color: 46
Size: 250 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 327
Size: 356 Color: 325
Size: 287 Color: 211

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 362
Size: 357 Color: 329
Size: 264 Color: 126

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 487
Size: 255 Color: 70
Size: 253 Color: 37

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 431
Size: 301 Color: 248
Size: 254 Color: 55

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 381
Size: 334 Color: 288
Size: 271 Color: 151

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 376
Size: 340 Color: 295
Size: 267 Color: 132

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 354
Size: 342 Color: 298
Size: 285 Color: 209

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 401
Size: 313 Color: 262
Size: 269 Color: 146

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 385
Size: 329 Color: 280
Size: 271 Color: 156

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 391
Size: 314 Color: 264
Size: 280 Color: 192

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 457
Size: 271 Color: 150
Size: 258 Color: 81

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 386
Size: 330 Color: 281
Size: 269 Color: 145

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 320
Size: 352 Color: 317
Size: 294 Color: 231

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 366
Size: 344 Color: 301
Size: 275 Color: 181

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 375
Size: 333 Color: 287
Size: 275 Color: 177

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 406
Size: 326 Color: 278
Size: 255 Color: 67

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 461
Size: 273 Color: 166
Size: 253 Color: 42

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 415
Size: 301 Color: 249
Size: 274 Color: 176

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 439
Size: 298 Color: 239
Size: 252 Color: 31

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 473
Size: 259 Color: 99
Size: 258 Color: 85

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 339
Size: 347 Color: 305
Size: 288 Color: 217

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 345
Size: 366 Color: 340
Size: 267 Color: 133

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 399
Size: 312 Color: 261
Size: 271 Color: 155

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 472
Size: 267 Color: 134
Size: 252 Color: 34

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 440
Size: 282 Color: 201
Size: 267 Color: 136

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 342
Size: 346 Color: 303
Size: 288 Color: 216

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 456
Size: 274 Color: 169
Size: 256 Color: 75

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 395
Size: 333 Color: 286
Size: 255 Color: 66

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 404
Size: 320 Color: 272
Size: 262 Color: 112

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 389
Size: 344 Color: 302
Size: 251 Color: 15

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 476
Size: 265 Color: 128
Size: 250 Color: 14

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 380
Size: 333 Color: 284
Size: 272 Color: 158

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 350
Size: 331 Color: 283
Size: 299 Color: 244

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 434
Size: 284 Color: 205
Size: 271 Color: 157

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 383
Size: 308 Color: 257
Size: 294 Color: 232

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 450
Size: 271 Color: 154
Size: 265 Color: 127

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 322
Size: 354 Color: 319
Size: 291 Color: 221

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 397
Size: 323 Color: 274
Size: 263 Color: 118

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 485
Size: 255 Color: 68
Size: 254 Color: 54

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 368
Size: 349 Color: 309
Size: 268 Color: 138

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 374
Size: 356 Color: 324
Size: 252 Color: 32

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 360
Size: 347 Color: 308
Size: 276 Color: 185

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 483
Size: 259 Color: 94
Size: 251 Color: 21

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 387
Size: 329 Color: 279
Size: 269 Color: 142

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 358
Size: 369 Color: 349
Size: 255 Color: 65

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 369
Size: 340 Color: 294
Size: 275 Color: 178

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 479
Size: 259 Color: 102
Size: 254 Color: 60

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 454
Size: 280 Color: 191
Size: 253 Color: 49

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 447
Size: 290 Color: 220
Size: 250 Color: 13

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 498
Size: 252 Color: 35
Size: 250 Color: 3

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 414
Size: 301 Color: 247
Size: 274 Color: 173

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 463
Size: 274 Color: 170
Size: 251 Color: 18

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 403
Size: 295 Color: 233
Size: 287 Color: 215

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 448
Size: 287 Color: 213
Size: 251 Color: 16

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 433
Size: 285 Color: 206
Size: 270 Color: 149

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 373
Size: 350 Color: 311
Size: 258 Color: 84

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 499
Size: 252 Color: 30
Size: 250 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 480
Size: 258 Color: 83
Size: 255 Color: 63

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 442
Size: 294 Color: 229
Size: 250 Color: 2

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 422
Size: 295 Color: 234
Size: 271 Color: 153

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 455
Size: 269 Color: 140
Size: 262 Color: 114

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 441
Size: 293 Color: 228
Size: 252 Color: 25

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 407
Size: 312 Color: 260
Size: 269 Color: 143

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 428
Size: 307 Color: 256
Size: 251 Color: 17

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 335
Size: 339 Color: 292
Size: 298 Color: 240

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 449
Size: 283 Color: 203
Size: 254 Color: 53

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 409
Size: 298 Color: 243
Size: 281 Color: 194

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 443
Size: 280 Color: 193
Size: 264 Color: 124

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 364
Size: 343 Color: 299
Size: 276 Color: 187

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 438
Size: 277 Color: 188
Size: 276 Color: 184

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 410
Size: 315 Color: 266
Size: 264 Color: 123

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 432
Size: 281 Color: 195
Size: 274 Color: 168

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 378
Size: 347 Color: 307
Size: 259 Color: 93

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 393
Size: 325 Color: 277
Size: 265 Color: 129

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 400
Size: 294 Color: 230
Size: 289 Color: 218

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 405
Size: 297 Color: 238
Size: 284 Color: 204

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 452
Size: 273 Color: 167
Size: 261 Color: 109

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 466
Size: 266 Color: 130
Size: 256 Color: 77

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 426
Size: 305 Color: 252
Size: 254 Color: 51

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 435
Size: 296 Color: 236
Size: 258 Color: 87

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 416
Size: 324 Color: 276
Size: 250 Color: 5

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 427
Size: 307 Color: 255
Size: 252 Color: 29

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 347
Size: 350 Color: 312
Size: 281 Color: 196

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 377
Size: 356 Color: 326
Size: 251 Color: 23

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 462
Size: 275 Color: 183
Size: 250 Color: 10

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 489
Size: 257 Color: 79
Size: 250 Color: 4

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 390
Size: 331 Color: 282
Size: 263 Color: 122

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 315
Size: 350 Color: 313
Size: 299 Color: 246

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 371
Size: 336 Color: 289
Size: 274 Color: 175

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 451
Size: 273 Color: 165
Size: 262 Color: 115

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 430
Size: 282 Color: 199
Size: 274 Color: 171

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 367
Size: 364 Color: 338
Size: 254 Color: 57

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 496
Size: 253 Color: 47
Size: 250 Color: 7

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 423
Size: 291 Color: 222
Size: 272 Color: 160

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 370
Size: 340 Color: 296
Size: 274 Color: 172

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 355
Size: 359 Color: 331
Size: 266 Color: 131

Total size: 167000
Total free space: 0

