Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 252 Color: 1
Size: 250 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 259 Color: 1
Size: 251 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 269 Color: 0
Size: 251 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 1
Size: 341 Color: 0
Size: 315 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 261 Color: 1
Size: 257 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 363 Color: 0
Size: 267 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 265 Color: 1
Size: 255 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 289 Color: 1
Size: 276 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 303 Color: 0
Size: 252 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 0
Size: 306 Color: 1
Size: 254 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 0
Size: 295 Color: 1
Size: 281 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 312 Color: 0
Size: 259 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 269 Color: 0
Size: 261 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 262 Color: 0
Size: 256 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 272 Color: 0
Size: 255 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 296 Color: 0
Size: 251 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 265 Color: 1
Size: 263 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 257 Color: 1
Size: 254 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 316 Color: 0
Size: 272 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 346 Color: 0
Size: 294 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 278 Color: 0
Size: 258 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 300 Color: 1
Size: 272 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 288 Color: 0
Size: 285 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 323 Color: 0
Size: 259 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 272 Color: 0
Size: 268 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 0
Size: 326 Color: 1
Size: 319 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 314 Color: 0
Size: 274 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 365 Color: 0
Size: 253 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 285 Color: 0
Size: 263 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 325 Color: 0
Size: 250 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 259 Color: 0
Size: 255 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 287 Color: 1
Size: 251 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 262 Color: 0
Size: 259 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 318 Color: 0
Size: 294 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 274 Color: 1
Size: 258 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 360 Color: 0
Size: 266 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 315 Color: 0
Size: 250 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 344 Color: 0
Size: 281 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 318 Color: 0
Size: 297 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 302 Color: 1
Size: 252 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 329 Color: 1
Size: 279 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 285 Color: 1
Size: 284 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 257 Color: 1
Size: 251 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 277 Color: 0
Size: 257 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 294 Color: 1
Size: 290 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 309 Color: 1
Size: 287 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 314 Color: 0
Size: 291 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 273 Color: 0
Size: 257 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 0
Size: 276 Color: 0
Size: 258 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 256 Color: 0
Size: 254 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 277 Color: 0
Size: 261 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 275 Color: 0
Size: 261 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 0
Size: 289 Color: 1
Size: 250 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 319 Color: 0
Size: 275 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 0
Size: 273 Color: 0
Size: 250 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 0
Size: 267 Color: 0
Size: 266 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 348 Color: 1
Size: 273 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 340 Color: 1
Size: 279 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 354 Color: 1
Size: 276 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 262 Color: 1
Size: 258 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 283 Color: 1
Size: 251 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 0
Size: 370 Color: 0
Size: 257 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 339 Color: 0
Size: 250 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 0
Size: 277 Color: 1
Size: 262 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 345 Color: 0
Size: 286 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 318 Color: 1
Size: 276 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 353 Color: 0
Size: 250 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 253 Color: 1
Size: 252 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 266 Color: 0
Size: 255 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 371 Color: 0
Size: 252 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 318 Color: 1
Size: 267 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 0
Size: 313 Color: 1
Size: 293 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 258 Color: 1
Size: 258 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 296 Color: 0
Size: 251 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 278 Color: 0
Size: 262 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 257 Color: 1
Size: 252 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 0
Size: 281 Color: 1
Size: 252 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 368 Color: 0
Size: 258 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 277 Color: 0
Size: 256 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 282 Color: 0
Size: 257 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 315 Color: 1
Size: 271 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 0
Size: 330 Color: 1
Size: 322 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 293 Color: 0
Size: 266 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 283 Color: 1
Size: 268 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 339 Color: 0
Size: 256 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 371 Color: 1
Size: 255 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 284 Color: 1
Size: 281 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 0
Size: 344 Color: 1
Size: 283 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 364 Color: 0
Size: 259 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 342 Color: 0
Size: 303 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 354 Color: 1
Size: 289 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 335 Color: 1
Size: 269 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 267 Color: 1
Size: 260 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 341 Color: 1
Size: 288 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 291 Color: 0
Size: 258 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 319 Color: 0
Size: 297 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 0
Size: 297 Color: 1
Size: 270 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 263 Color: 1
Size: 252 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 339 Color: 1
Size: 287 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 287 Color: 1
Size: 259 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 355 Color: 0
Size: 252 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 351 Color: 1
Size: 263 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 356 Color: 0
Size: 278 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 257 Color: 0
Size: 252 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 328 Color: 0
Size: 293 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 271 Color: 0
Size: 258 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 276 Color: 1
Size: 256 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 308 Color: 0
Size: 251 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 367 Color: 1
Size: 250 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 303 Color: 1
Size: 254 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 0
Size: 302 Color: 0
Size: 274 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 262 Color: 1
Size: 250 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 275 Color: 1
Size: 263 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 0
Size: 354 Color: 1
Size: 283 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 305 Color: 0
Size: 253 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 0
Size: 250 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 0
Size: 302 Color: 1
Size: 291 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 259 Color: 1
Size: 252 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 265 Color: 0
Size: 264 Color: 1
Size: 471 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 0
Size: 282 Color: 1
Size: 272 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 330 Color: 0
Size: 259 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 276 Color: 0
Size: 256 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 263 Color: 0
Size: 257 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 345 Color: 1
Size: 298 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 320 Color: 0
Size: 309 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 362 Color: 1
Size: 267 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 369 Color: 0
Size: 261 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 341 Color: 0
Size: 289 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 346 Color: 1
Size: 279 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 1
Size: 344 Color: 0
Size: 312 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 353 Color: 1
Size: 271 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 351 Color: 0
Size: 271 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 348 Color: 1
Size: 273 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 362 Color: 0
Size: 252 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 0
Size: 354 Color: 0
Size: 259 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 354 Color: 0
Size: 254 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 304 Color: 0
Size: 302 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 343 Color: 0
Size: 260 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 331 Color: 1
Size: 263 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 320 Color: 1
Size: 270 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 361 Color: 1
Size: 276 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 319 Color: 0
Size: 266 Color: 1
Size: 415 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 326 Color: 0
Size: 258 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 311 Color: 1
Size: 285 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 319 Color: 1
Size: 250 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 308 Color: 0
Size: 262 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 352 Color: 0
Size: 287 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 296 Color: 1
Size: 269 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 298 Color: 0
Size: 265 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 281 Color: 1
Size: 271 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 255 Color: 1
Size: 251 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 279 Color: 1
Size: 269 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 292 Color: 1
Size: 254 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 287 Color: 0
Size: 255 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 273 Color: 1
Size: 262 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 282 Color: 0
Size: 250 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 278 Color: 0
Size: 253 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 321 Color: 1
Size: 251 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 264 Color: 1
Size: 264 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 329 Color: 1
Size: 276 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 282 Color: 0
Size: 260 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 269 Color: 0
Size: 251 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 264 Color: 0
Size: 260 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 0
Size: 262 Color: 1
Size: 257 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 258 Color: 1
Size: 256 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 262 Color: 1
Size: 254 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 264 Color: 0
Size: 252 Color: 1

Total size: 167000
Total free space: 0

