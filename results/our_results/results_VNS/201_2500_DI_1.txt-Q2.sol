Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1396 Color: 1
Size: 902 Color: 0
Size: 90 Color: 0
Size: 76 Color: 1

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 0
Size: 920 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 1
Size: 758 Color: 0
Size: 76 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 1
Size: 626 Color: 1
Size: 124 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 1
Size: 684 Color: 0
Size: 40 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 0
Size: 498 Color: 0
Size: 44 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 0
Size: 490 Color: 1
Size: 40 Color: 1

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 1978 Color: 0
Size: 398 Color: 1
Size: 48 Color: 0
Size: 40 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 0
Size: 338 Color: 1
Size: 120 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 1
Size: 378 Color: 1
Size: 88 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 0
Size: 384 Color: 1
Size: 66 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 1
Size: 308 Color: 0
Size: 88 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2161 Color: 0
Size: 253 Color: 0
Size: 50 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2127 Color: 1
Size: 281 Color: 0
Size: 56 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 1
Size: 172 Color: 1
Size: 120 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 1
Size: 204 Color: 0
Size: 72 Color: 1

Bin 17: 0 of cap free
Amount of items: 5
Items: 
Size: 2210 Color: 0
Size: 236 Color: 1
Size: 8 Color: 0
Size: 8 Color: 0
Size: 2 Color: 1

Bin 18: 1 of cap free
Amount of items: 5
Items: 
Size: 1722 Color: 1
Size: 371 Color: 0
Size: 282 Color: 0
Size: 48 Color: 1
Size: 40 Color: 0

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 0
Size: 698 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1847 Color: 1
Size: 576 Color: 1
Size: 40 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 1
Size: 515 Color: 0
Size: 64 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1971 Color: 1
Size: 444 Color: 1
Size: 48 Color: 0

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 2059 Color: 1
Size: 404 Color: 0

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 2099 Color: 1
Size: 364 Color: 0

Bin 25: 2 of cap free
Amount of items: 2
Items: 
Size: 1383 Color: 1
Size: 1079 Color: 0

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 0
Size: 780 Color: 1
Size: 84 Color: 1

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 1
Size: 840 Color: 1
Size: 64 Color: 0

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1870 Color: 1
Size: 484 Color: 0
Size: 108 Color: 0

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 0
Size: 182 Color: 1
Size: 172 Color: 1

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 0
Size: 152 Color: 1
Size: 152 Color: 1

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 0
Size: 258 Color: 1
Size: 40 Color: 0

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 1
Size: 583 Color: 1
Size: 72 Color: 0

Bin 33: 4 of cap free
Amount of items: 2
Items: 
Size: 1942 Color: 1
Size: 518 Color: 0

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 2054 Color: 1
Size: 406 Color: 0

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 2090 Color: 0
Size: 370 Color: 1

Bin 36: 5 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 0
Size: 881 Color: 1
Size: 48 Color: 1

Bin 37: 5 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 1
Size: 763 Color: 0

Bin 38: 6 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 1
Size: 882 Color: 0
Size: 339 Color: 1

Bin 39: 6 of cap free
Amount of items: 2
Items: 
Size: 1409 Color: 1
Size: 1049 Color: 0

Bin 40: 6 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 0
Size: 665 Color: 1
Size: 244 Color: 0

Bin 41: 6 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 1
Size: 332 Color: 0
Size: 28 Color: 0

Bin 42: 6 of cap free
Amount of items: 2
Items: 
Size: 2186 Color: 1
Size: 272 Color: 0

Bin 43: 8 of cap free
Amount of items: 4
Items: 
Size: 1236 Color: 0
Size: 622 Color: 1
Size: 550 Color: 1
Size: 48 Color: 0

Bin 44: 9 of cap free
Amount of items: 2
Items: 
Size: 1915 Color: 1
Size: 540 Color: 0

Bin 45: 10 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 0
Size: 874 Color: 0
Size: 338 Color: 1

Bin 46: 10 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 1
Size: 482 Color: 0
Size: 152 Color: 0

Bin 47: 11 of cap free
Amount of items: 3
Items: 
Size: 2019 Color: 1
Size: 382 Color: 0
Size: 52 Color: 1

Bin 48: 12 of cap free
Amount of items: 2
Items: 
Size: 2062 Color: 1
Size: 390 Color: 0

Bin 49: 12 of cap free
Amount of items: 2
Items: 
Size: 2180 Color: 0
Size: 272 Color: 1

Bin 50: 14 of cap free
Amount of items: 2
Items: 
Size: 1846 Color: 0
Size: 604 Color: 1

Bin 51: 15 of cap free
Amount of items: 2
Items: 
Size: 1667 Color: 0
Size: 782 Color: 1

Bin 52: 15 of cap free
Amount of items: 2
Items: 
Size: 1990 Color: 0
Size: 459 Color: 1

Bin 53: 20 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 0
Size: 504 Color: 1

Bin 54: 20 of cap free
Amount of items: 2
Items: 
Size: 2130 Color: 0
Size: 314 Color: 1

Bin 55: 22 of cap free
Amount of items: 2
Items: 
Size: 1988 Color: 0
Size: 454 Color: 1

Bin 56: 23 of cap free
Amount of items: 2
Items: 
Size: 1418 Color: 1
Size: 1023 Color: 0

Bin 57: 25 of cap free
Amount of items: 2
Items: 
Size: 2028 Color: 0
Size: 411 Color: 1

Bin 58: 26 of cap free
Amount of items: 6
Items: 
Size: 1233 Color: 0
Size: 305 Color: 1
Size: 252 Color: 0
Size: 244 Color: 0
Size: 204 Color: 1
Size: 200 Color: 1

Bin 59: 26 of cap free
Amount of items: 2
Items: 
Size: 1410 Color: 0
Size: 1028 Color: 1

Bin 60: 28 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 1
Size: 1026 Color: 1
Size: 176 Color: 0

Bin 61: 30 of cap free
Amount of items: 19
Items: 
Size: 234 Color: 0
Size: 214 Color: 0
Size: 200 Color: 1
Size: 174 Color: 0
Size: 148 Color: 1
Size: 136 Color: 1
Size: 136 Color: 0
Size: 132 Color: 1
Size: 116 Color: 1
Size: 112 Color: 1
Size: 104 Color: 1
Size: 102 Color: 0
Size: 100 Color: 1
Size: 96 Color: 0
Size: 96 Color: 0
Size: 96 Color: 0
Size: 82 Color: 0
Size: 80 Color: 1
Size: 76 Color: 1

Bin 62: 36 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 1
Size: 1022 Color: 0

Bin 63: 36 of cap free
Amount of items: 2
Items: 
Size: 1450 Color: 1
Size: 978 Color: 0

Bin 64: 38 of cap free
Amount of items: 2
Items: 
Size: 1644 Color: 1
Size: 782 Color: 0

Bin 65: 40 of cap free
Amount of items: 2
Items: 
Size: 1532 Color: 0
Size: 892 Color: 1

Bin 66: 1902 of cap free
Amount of items: 8
Items: 
Size: 80 Color: 0
Size: 80 Color: 0
Size: 74 Color: 1
Size: 72 Color: 1
Size: 72 Color: 1
Size: 64 Color: 1
Size: 60 Color: 0
Size: 60 Color: 0

Total size: 160160
Total free space: 2464

