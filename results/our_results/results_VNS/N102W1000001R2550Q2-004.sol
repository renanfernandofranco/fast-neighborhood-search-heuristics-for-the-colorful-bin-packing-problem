Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 470626 Color: 0
Size: 264989 Color: 1
Size: 264386 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 380385 Color: 0
Size: 337563 Color: 1
Size: 282053 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 393903 Color: 1
Size: 347572 Color: 0
Size: 258526 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 439009 Color: 0
Size: 287763 Color: 1
Size: 273229 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 464546 Color: 1
Size: 273738 Color: 0
Size: 261717 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 381242 Color: 0
Size: 339174 Color: 1
Size: 279585 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 378489 Color: 0
Size: 332403 Color: 1
Size: 289109 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 448097 Color: 1
Size: 289713 Color: 1
Size: 262191 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 411035 Color: 0
Size: 295579 Color: 0
Size: 293387 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 392324 Color: 1
Size: 351446 Color: 0
Size: 256231 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 396986 Color: 0
Size: 318078 Color: 0
Size: 284937 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 370196 Color: 1
Size: 351855 Color: 0
Size: 277950 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 370565 Color: 0
Size: 321556 Color: 0
Size: 307880 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 361459 Color: 1
Size: 325982 Color: 0
Size: 312560 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 405193 Color: 0
Size: 297666 Color: 0
Size: 297142 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 360729 Color: 0
Size: 328886 Color: 1
Size: 310386 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 432634 Color: 1
Size: 310104 Color: 1
Size: 257263 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 372787 Color: 0
Size: 342207 Color: 0
Size: 285007 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 430134 Color: 1
Size: 299744 Color: 0
Size: 270123 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 373193 Color: 1
Size: 348898 Color: 0
Size: 277910 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 425078 Color: 1
Size: 317850 Color: 0
Size: 257073 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 382821 Color: 0
Size: 322607 Color: 0
Size: 294573 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 398227 Color: 1
Size: 346861 Color: 0
Size: 254913 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 499313 Color: 0
Size: 250582 Color: 0
Size: 250106 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 429748 Color: 1
Size: 293827 Color: 0
Size: 276426 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 491679 Color: 1
Size: 254415 Color: 0
Size: 253907 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 496151 Color: 1
Size: 253632 Color: 0
Size: 250218 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 369107 Color: 1
Size: 324633 Color: 1
Size: 306261 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 408103 Color: 1
Size: 328462 Color: 1
Size: 263436 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 431102 Color: 1
Size: 312454 Color: 0
Size: 256445 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 440540 Color: 0
Size: 259247 Color: 1
Size: 300214 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 460874 Color: 0
Size: 278963 Color: 1
Size: 260164 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 461069 Color: 0
Size: 286465 Color: 1
Size: 252467 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 462349 Color: 0
Size: 286352 Color: 1
Size: 251300 Color: 1

Total size: 34000034
Total free space: 0

