Capicity Bin: 16800
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8412 Color: 412
Size: 6986 Color: 389
Size: 632 Color: 127
Size: 386 Color: 65
Size: 384 Color: 63

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12979 Color: 483
Size: 3185 Color: 320
Size: 636 Color: 128

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 13054 Color: 487
Size: 3378 Color: 325
Size: 368 Color: 56

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 488
Size: 2982 Color: 308
Size: 758 Color: 146

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13226 Color: 494
Size: 1932 Color: 247
Size: 1642 Color: 223

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 497
Size: 3160 Color: 316
Size: 368 Color: 57

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 499
Size: 2244 Color: 272
Size: 1192 Color: 186

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13365 Color: 500
Size: 3085 Color: 312
Size: 350 Color: 47

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13374 Color: 501
Size: 2858 Color: 303
Size: 568 Color: 115

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13534 Color: 507
Size: 1860 Color: 242
Size: 1406 Color: 203

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 510
Size: 2484 Color: 286
Size: 656 Color: 129

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 515
Size: 2624 Color: 295
Size: 432 Color: 77

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13860 Color: 520
Size: 2492 Color: 287
Size: 448 Color: 83

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 521
Size: 2262 Color: 274
Size: 658 Color: 130

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 522
Size: 1464 Color: 208
Size: 1444 Color: 207

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13906 Color: 523
Size: 2602 Color: 292
Size: 292 Color: 23

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 528
Size: 2168 Color: 266
Size: 576 Color: 117

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 531
Size: 2244 Color: 273
Size: 448 Color: 84

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 532
Size: 2239 Color: 271
Size: 446 Color: 82

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14199 Color: 536
Size: 1925 Color: 244
Size: 676 Color: 133

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14270 Color: 540
Size: 1628 Color: 221
Size: 902 Color: 162

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14406 Color: 547
Size: 2044 Color: 254
Size: 350 Color: 48

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14412 Color: 548
Size: 1652 Color: 225
Size: 736 Color: 142

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14472 Color: 551
Size: 1792 Color: 236
Size: 536 Color: 109

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14475 Color: 552
Size: 1757 Color: 232
Size: 568 Color: 116

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14478 Color: 553
Size: 1938 Color: 248
Size: 384 Color: 64

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14483 Color: 554
Size: 1931 Color: 246
Size: 386 Color: 66

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 555
Size: 1916 Color: 243
Size: 400 Color: 68

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 556
Size: 1232 Color: 189
Size: 1080 Color: 178

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14572 Color: 559
Size: 1736 Color: 231
Size: 492 Color: 97

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14578 Color: 560
Size: 1854 Color: 241
Size: 368 Color: 58

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14649 Color: 564
Size: 1631 Color: 222
Size: 520 Color: 104

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14656 Color: 565
Size: 1116 Color: 182
Size: 1028 Color: 174

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 569
Size: 1288 Color: 192
Size: 816 Color: 156

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14701 Color: 570
Size: 2085 Color: 258
Size: 14 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14792 Color: 573
Size: 1688 Color: 229
Size: 320 Color: 37

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14799 Color: 574
Size: 1533 Color: 216
Size: 468 Color: 89

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14804 Color: 575
Size: 1232 Color: 190
Size: 764 Color: 147

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14820 Color: 577
Size: 1428 Color: 204
Size: 552 Color: 113

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14830 Color: 578
Size: 1518 Color: 213
Size: 452 Color: 85

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14836 Color: 579
Size: 1644 Color: 224
Size: 320 Color: 35

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14886 Color: 581
Size: 1598 Color: 220
Size: 316 Color: 32

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14887 Color: 582
Size: 1595 Color: 219
Size: 318 Color: 33

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14904 Color: 583
Size: 1168 Color: 185
Size: 728 Color: 140

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14972 Color: 586
Size: 1396 Color: 198
Size: 432 Color: 76

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14980 Color: 587
Size: 1072 Color: 177
Size: 748 Color: 143

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14982 Color: 588
Size: 1478 Color: 209
Size: 340 Color: 46

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14990 Color: 589
Size: 1548 Color: 217
Size: 262 Color: 15

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14995 Color: 590
Size: 1505 Color: 210
Size: 300 Color: 26

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 15009 Color: 592
Size: 1665 Color: 226
Size: 126 Color: 5

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 15048 Color: 594
Size: 1080 Color: 179
Size: 672 Color: 132

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 15068 Color: 595
Size: 1396 Color: 199
Size: 336 Color: 45

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 15069 Color: 596
Size: 1443 Color: 206
Size: 288 Color: 21

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 15070 Color: 597
Size: 1442 Color: 205
Size: 288 Color: 22

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 15076 Color: 598
Size: 1392 Color: 195
Size: 332 Color: 42

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 15092 Color: 599
Size: 976 Color: 171
Size: 732 Color: 141

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 15114 Color: 600
Size: 1208 Color: 187
Size: 478 Color: 90

Bin 58: 1 of cap free
Amount of items: 11
Items: 
Size: 8401 Color: 405
Size: 1136 Color: 184
Size: 1110 Color: 181
Size: 1096 Color: 180
Size: 1056 Color: 176
Size: 1056 Color: 175
Size: 1008 Color: 173
Size: 664 Color: 131
Size: 432 Color: 78
Size: 420 Color: 75
Size: 420 Color: 74

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12843 Color: 481
Size: 3172 Color: 319
Size: 784 Color: 149

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 13145 Color: 492
Size: 3166 Color: 317
Size: 488 Color: 96

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 14007 Color: 527
Size: 2792 Color: 300

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 14668 Color: 566
Size: 2131 Color: 262

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 14693 Color: 568
Size: 2106 Color: 260

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 12728 Color: 477
Size: 3974 Color: 342
Size: 96 Color: 3

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 13408 Color: 503
Size: 3390 Color: 326

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 13460 Color: 504
Size: 3338 Color: 324

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 13976 Color: 525
Size: 1702 Color: 230
Size: 1120 Color: 183

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 14076 Color: 529
Size: 2722 Color: 298

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 14438 Color: 550
Size: 2360 Color: 279

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 14948 Color: 584
Size: 1842 Color: 240
Size: 8 Color: 0

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 15030 Color: 593
Size: 1768 Color: 233

Bin 72: 3 of cap free
Amount of items: 3
Items: 
Size: 13650 Color: 509
Size: 2841 Color: 302
Size: 306 Color: 31

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 11528 Color: 448
Size: 4968 Color: 368
Size: 300 Color: 25

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 475
Size: 4088 Color: 349
Size: 144 Color: 6

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 13118 Color: 490
Size: 3678 Color: 335

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 511
Size: 3124 Color: 315

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 518
Size: 2968 Color: 306

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 14232 Color: 539
Size: 2564 Color: 289

Bin 79: 4 of cap free
Amount of items: 2
Items: 
Size: 14275 Color: 541
Size: 2521 Color: 288

Bin 80: 4 of cap free
Amount of items: 2
Items: 
Size: 14330 Color: 542
Size: 2466 Color: 285

Bin 81: 4 of cap free
Amount of items: 2
Items: 
Size: 14356 Color: 544
Size: 2440 Color: 283

Bin 82: 5 of cap free
Amount of items: 5
Items: 
Size: 8418 Color: 414
Size: 6993 Color: 391
Size: 624 Color: 125
Size: 384 Color: 60
Size: 376 Color: 59

Bin 83: 5 of cap free
Amount of items: 2
Items: 
Size: 11381 Color: 446
Size: 5414 Color: 375

Bin 84: 5 of cap free
Amount of items: 2
Items: 
Size: 14961 Color: 585
Size: 1834 Color: 239

Bin 85: 6 of cap free
Amount of items: 3
Items: 
Size: 12302 Color: 468
Size: 4252 Color: 352
Size: 240 Color: 11

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 12734 Color: 478
Size: 4060 Color: 346

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 13842 Color: 519
Size: 2952 Color: 305

Bin 88: 6 of cap free
Amount of items: 2
Items: 
Size: 14178 Color: 534
Size: 2616 Color: 294

Bin 89: 6 of cap free
Amount of items: 2
Items: 
Size: 14342 Color: 543
Size: 2452 Color: 284

Bin 90: 6 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 546
Size: 2414 Color: 281

Bin 91: 6 of cap free
Amount of items: 2
Items: 
Size: 14590 Color: 561
Size: 2204 Color: 270

Bin 92: 7 of cap free
Amount of items: 5
Items: 
Size: 10082 Color: 428
Size: 3168 Color: 318
Size: 2399 Color: 280
Size: 816 Color: 155
Size: 328 Color: 39

Bin 93: 7 of cap free
Amount of items: 2
Items: 
Size: 12741 Color: 479
Size: 4052 Color: 345

Bin 94: 7 of cap free
Amount of items: 2
Items: 
Size: 13002 Color: 485
Size: 3791 Color: 339

Bin 95: 7 of cap free
Amount of items: 2
Items: 
Size: 13393 Color: 502
Size: 3400 Color: 327

Bin 96: 7 of cap free
Amount of items: 2
Items: 
Size: 14365 Color: 545
Size: 2428 Color: 282

Bin 97: 7 of cap free
Amount of items: 2
Items: 
Size: 15000 Color: 591
Size: 1793 Color: 237

Bin 98: 8 of cap free
Amount of items: 2
Items: 
Size: 13724 Color: 514
Size: 3068 Color: 311

Bin 99: 8 of cap free
Amount of items: 2
Items: 
Size: 14806 Color: 576
Size: 1986 Color: 251

Bin 100: 9 of cap free
Amount of items: 2
Items: 
Size: 13190 Color: 493
Size: 3601 Color: 333

Bin 101: 9 of cap free
Amount of items: 2
Items: 
Size: 13923 Color: 524
Size: 2868 Color: 304

Bin 102: 9 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 535
Size: 2603 Color: 293

Bin 103: 10 of cap free
Amount of items: 9
Items: 
Size: 8402 Color: 406
Size: 1396 Color: 197
Size: 1396 Color: 196
Size: 1392 Color: 194
Size: 1392 Color: 193
Size: 1224 Color: 188
Size: 756 Color: 145
Size: 416 Color: 73
Size: 416 Color: 72

Bin 104: 10 of cap free
Amount of items: 3
Items: 
Size: 11074 Color: 442
Size: 5412 Color: 374
Size: 304 Color: 29

Bin 105: 10 of cap free
Amount of items: 2
Items: 
Size: 14164 Color: 533
Size: 2626 Color: 296

Bin 106: 10 of cap free
Amount of items: 2
Items: 
Size: 14728 Color: 571
Size: 2062 Color: 256

Bin 107: 11 of cap free
Amount of items: 2
Items: 
Size: 11901 Color: 456
Size: 4888 Color: 367

Bin 108: 11 of cap free
Amount of items: 3
Items: 
Size: 12251 Color: 466
Size: 4282 Color: 354
Size: 256 Color: 13

Bin 109: 11 of cap free
Amount of items: 2
Items: 
Size: 14205 Color: 538
Size: 2584 Color: 291

Bin 110: 11 of cap free
Amount of items: 2
Items: 
Size: 14758 Color: 572
Size: 2031 Color: 253

Bin 111: 11 of cap free
Amount of items: 2
Items: 
Size: 14845 Color: 580
Size: 1944 Color: 250

Bin 112: 12 of cap free
Amount of items: 5
Items: 
Size: 8413 Color: 413
Size: 6991 Color: 390
Size: 616 Color: 124
Size: 384 Color: 62
Size: 384 Color: 61

Bin 113: 12 of cap free
Amount of items: 3
Items: 
Size: 11916 Color: 457
Size: 2767 Color: 299
Size: 2105 Color: 259

Bin 114: 12 of cap free
Amount of items: 2
Items: 
Size: 12380 Color: 470
Size: 4408 Color: 357

Bin 115: 12 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 517
Size: 2988 Color: 309

Bin 116: 12 of cap free
Amount of items: 2
Items: 
Size: 14602 Color: 563
Size: 2186 Color: 269

Bin 117: 13 of cap free
Amount of items: 2
Items: 
Size: 13682 Color: 513
Size: 3105 Color: 314

Bin 118: 13 of cap free
Amount of items: 2
Items: 
Size: 14491 Color: 557
Size: 2296 Color: 276

Bin 119: 14 of cap free
Amount of items: 7
Items: 
Size: 8405 Color: 408
Size: 1669 Color: 228
Size: 1668 Color: 227
Size: 1592 Color: 218
Size: 1524 Color: 215
Size: 1524 Color: 214
Size: 404 Color: 69

Bin 120: 14 of cap free
Amount of items: 2
Items: 
Size: 10340 Color: 433
Size: 6446 Color: 388

Bin 121: 14 of cap free
Amount of items: 3
Items: 
Size: 12479 Color: 473
Size: 4083 Color: 348
Size: 224 Color: 8

Bin 122: 14 of cap free
Amount of items: 2
Items: 
Size: 12996 Color: 484
Size: 3790 Color: 338

Bin 123: 14 of cap free
Amount of items: 2
Items: 
Size: 14086 Color: 530
Size: 2700 Color: 297

Bin 124: 15 of cap free
Amount of items: 3
Items: 
Size: 11140 Color: 443
Size: 3595 Color: 331
Size: 2050 Color: 255

Bin 125: 16 of cap free
Amount of items: 3
Items: 
Size: 12247 Color: 465
Size: 4281 Color: 353
Size: 256 Color: 14

Bin 126: 16 of cap free
Amount of items: 2
Items: 
Size: 13240 Color: 496
Size: 3544 Color: 330

Bin 127: 16 of cap free
Amount of items: 2
Items: 
Size: 13775 Color: 516
Size: 3009 Color: 310

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 14674 Color: 567
Size: 2110 Color: 261

Bin 129: 18 of cap free
Amount of items: 3
Items: 
Size: 9453 Color: 422
Size: 7001 Color: 396
Size: 328 Color: 41

Bin 130: 18 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 491
Size: 3658 Color: 334

Bin 131: 18 of cap free
Amount of items: 2
Items: 
Size: 13978 Color: 526
Size: 2804 Color: 301

Bin 132: 19 of cap free
Amount of items: 2
Items: 
Size: 13481 Color: 506
Size: 3300 Color: 323

Bin 133: 19 of cap free
Amount of items: 2
Items: 
Size: 14427 Color: 549
Size: 2354 Color: 278

Bin 134: 19 of cap free
Amount of items: 2
Items: 
Size: 14601 Color: 562
Size: 2180 Color: 268

Bin 135: 20 of cap free
Amount of items: 3
Items: 
Size: 11864 Color: 454
Size: 2977 Color: 307
Size: 1939 Color: 249

Bin 136: 20 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 467
Size: 4284 Color: 355
Size: 242 Color: 12

Bin 137: 20 of cap free
Amount of items: 2
Items: 
Size: 14508 Color: 558
Size: 2272 Color: 275

Bin 138: 21 of cap free
Amount of items: 2
Items: 
Size: 10631 Color: 434
Size: 6148 Color: 383

Bin 139: 22 of cap free
Amount of items: 2
Items: 
Size: 12110 Color: 463
Size: 4668 Color: 360

Bin 140: 22 of cap free
Amount of items: 2
Items: 
Size: 13564 Color: 508
Size: 3214 Color: 321

Bin 141: 24 of cap free
Amount of items: 2
Items: 
Size: 13092 Color: 489
Size: 3684 Color: 336

Bin 142: 24 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 537
Size: 2576 Color: 290

Bin 143: 26 of cap free
Amount of items: 2
Items: 
Size: 11172 Color: 444
Size: 5602 Color: 378

Bin 144: 27 of cap free
Amount of items: 2
Items: 
Size: 13293 Color: 498
Size: 3480 Color: 328

Bin 145: 29 of cap free
Amount of items: 2
Items: 
Size: 10728 Color: 435
Size: 6043 Color: 382

Bin 146: 31 of cap free
Amount of items: 2
Items: 
Size: 13677 Color: 512
Size: 3092 Color: 313

Bin 147: 32 of cap free
Amount of items: 2
Items: 
Size: 9720 Color: 426
Size: 7048 Color: 399

Bin 148: 32 of cap free
Amount of items: 2
Items: 
Size: 10856 Color: 437
Size: 5912 Color: 380

Bin 149: 32 of cap free
Amount of items: 3
Items: 
Size: 11704 Color: 452
Size: 4776 Color: 364
Size: 288 Color: 20

Bin 150: 32 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 476
Size: 4040 Color: 344
Size: 96 Color: 4

Bin 151: 34 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 486
Size: 3750 Color: 337

Bin 152: 35 of cap free
Amount of items: 3
Items: 
Size: 11960 Color: 460
Size: 4517 Color: 358
Size: 288 Color: 18

Bin 153: 36 of cap free
Amount of items: 4
Items: 
Size: 9066 Color: 418
Size: 6994 Color: 392
Size: 352 Color: 51
Size: 352 Color: 50

Bin 154: 36 of cap free
Amount of items: 3
Items: 
Size: 9428 Color: 421
Size: 7000 Color: 395
Size: 336 Color: 43

Bin 155: 36 of cap free
Amount of items: 3
Items: 
Size: 10952 Color: 438
Size: 5492 Color: 376
Size: 320 Color: 34

Bin 156: 37 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 474
Size: 4019 Color: 343
Size: 192 Color: 7

Bin 157: 37 of cap free
Amount of items: 2
Items: 
Size: 13464 Color: 505
Size: 3299 Color: 322

Bin 158: 38 of cap free
Amount of items: 5
Items: 
Size: 10008 Color: 427
Size: 2329 Color: 277
Size: 2169 Color: 267
Size: 1928 Color: 245
Size: 328 Color: 40

Bin 159: 38 of cap free
Amount of items: 3
Items: 
Size: 11964 Color: 461
Size: 4518 Color: 359
Size: 280 Color: 17

Bin 160: 39 of cap free
Amount of items: 2
Items: 
Size: 13229 Color: 495
Size: 3532 Color: 329

Bin 161: 40 of cap free
Amount of items: 2
Items: 
Size: 10790 Color: 436
Size: 5970 Color: 381

Bin 162: 40 of cap free
Amount of items: 3
Items: 
Size: 11764 Color: 453
Size: 4708 Color: 361
Size: 288 Color: 19

Bin 163: 42 of cap free
Amount of items: 3
Items: 
Size: 11204 Color: 445
Size: 5250 Color: 372
Size: 304 Color: 28

Bin 164: 42 of cap free
Amount of items: 3
Items: 
Size: 11896 Color: 455
Size: 3598 Color: 332
Size: 1264 Color: 191

Bin 165: 42 of cap free
Amount of items: 2
Items: 
Size: 12034 Color: 462
Size: 4724 Color: 362

Bin 166: 46 of cap free
Amount of items: 2
Items: 
Size: 12844 Color: 482
Size: 3910 Color: 341

Bin 167: 48 of cap free
Amount of items: 21
Items: 
Size: 1000 Color: 172
Size: 960 Color: 170
Size: 960 Color: 169
Size: 958 Color: 168
Size: 952 Color: 167
Size: 952 Color: 166
Size: 936 Color: 165
Size: 928 Color: 164
Size: 920 Color: 163
Size: 900 Color: 161
Size: 868 Color: 160
Size: 864 Color: 159
Size: 856 Color: 158
Size: 854 Color: 157
Size: 808 Color: 154
Size: 808 Color: 153
Size: 464 Color: 87
Size: 464 Color: 86
Size: 436 Color: 81
Size: 432 Color: 80
Size: 432 Color: 79

Bin 168: 48 of cap free
Amount of items: 2
Items: 
Size: 11948 Color: 459
Size: 4804 Color: 366

Bin 169: 50 of cap free
Amount of items: 3
Items: 
Size: 12414 Color: 472
Size: 4104 Color: 350
Size: 232 Color: 9

Bin 170: 64 of cap free
Amount of items: 3
Items: 
Size: 11044 Color: 441
Size: 5388 Color: 373
Size: 304 Color: 30

Bin 171: 65 of cap free
Amount of items: 3
Items: 
Size: 11665 Color: 451
Size: 4774 Color: 363
Size: 296 Color: 24

Bin 172: 66 of cap free
Amount of items: 2
Items: 
Size: 12376 Color: 469
Size: 4358 Color: 356

Bin 173: 67 of cap free
Amount of items: 2
Items: 
Size: 11932 Color: 458
Size: 4801 Color: 365

Bin 174: 68 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 430
Size: 6200 Color: 385
Size: 320 Color: 36

Bin 175: 74 of cap free
Amount of items: 2
Items: 
Size: 11662 Color: 450
Size: 5064 Color: 370

Bin 176: 77 of cap free
Amount of items: 2
Items: 
Size: 9638 Color: 425
Size: 7085 Color: 400

Bin 177: 85 of cap free
Amount of items: 2
Items: 
Size: 11574 Color: 449
Size: 5141 Color: 371

Bin 178: 86 of cap free
Amount of items: 2
Items: 
Size: 11042 Color: 440
Size: 5672 Color: 379

Bin 179: 88 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 480
Size: 3830 Color: 340
Size: 84 Color: 2

Bin 180: 89 of cap free
Amount of items: 5
Items: 
Size: 8409 Color: 410
Size: 2163 Color: 265
Size: 2154 Color: 264
Size: 2152 Color: 263
Size: 1833 Color: 238

Bin 181: 96 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 416
Size: 7842 Color: 403
Size: 358 Color: 53

Bin 182: 98 of cap free
Amount of items: 3
Items: 
Size: 12206 Color: 464
Size: 4216 Color: 351
Size: 280 Color: 16

Bin 183: 99 of cap free
Amount of items: 3
Items: 
Size: 9368 Color: 420
Size: 6997 Color: 394
Size: 336 Color: 44

Bin 184: 102 of cap free
Amount of items: 2
Items: 
Size: 8410 Color: 411
Size: 8288 Color: 404

Bin 185: 102 of cap free
Amount of items: 3
Items: 
Size: 12390 Color: 471
Size: 4076 Color: 347
Size: 232 Color: 10

Bin 186: 108 of cap free
Amount of items: 3
Items: 
Size: 11382 Color: 447
Size: 5010 Color: 369
Size: 300 Color: 27

Bin 187: 140 of cap free
Amount of items: 3
Items: 
Size: 9312 Color: 419
Size: 6996 Color: 393
Size: 352 Color: 49

Bin 188: 145 of cap free
Amount of items: 3
Items: 
Size: 10139 Color: 429
Size: 6192 Color: 384
Size: 324 Color: 38

Bin 189: 176 of cap free
Amount of items: 3
Items: 
Size: 8696 Color: 417
Size: 7576 Color: 402
Size: 352 Color: 52

Bin 190: 204 of cap free
Amount of items: 4
Items: 
Size: 8426 Color: 415
Size: 7440 Color: 401
Size: 366 Color: 55
Size: 364 Color: 54

Bin 191: 208 of cap free
Amount of items: 2
Items: 
Size: 11041 Color: 439
Size: 5551 Color: 377

Bin 192: 236 of cap free
Amount of items: 2
Items: 
Size: 10308 Color: 432
Size: 6256 Color: 387

Bin 193: 242 of cap free
Amount of items: 27
Items: 
Size: 800 Color: 152
Size: 800 Color: 151
Size: 792 Color: 150
Size: 780 Color: 148
Size: 752 Color: 144
Size: 720 Color: 139
Size: 720 Color: 138
Size: 720 Color: 137
Size: 704 Color: 136
Size: 704 Color: 135
Size: 688 Color: 134
Size: 632 Color: 126
Size: 616 Color: 123
Size: 608 Color: 122
Size: 600 Color: 121
Size: 516 Color: 103
Size: 512 Color: 102
Size: 512 Color: 101
Size: 508 Color: 100
Size: 504 Color: 99
Size: 498 Color: 98
Size: 488 Color: 95
Size: 480 Color: 94
Size: 480 Color: 93
Size: 480 Color: 92
Size: 480 Color: 91
Size: 464 Color: 88

Bin 194: 247 of cap free
Amount of items: 2
Items: 
Size: 9549 Color: 424
Size: 7004 Color: 398

Bin 195: 274 of cap free
Amount of items: 2
Items: 
Size: 9524 Color: 423
Size: 7002 Color: 397

Bin 196: 274 of cap free
Amount of items: 2
Items: 
Size: 10306 Color: 431
Size: 6220 Color: 386

Bin 197: 338 of cap free
Amount of items: 6
Items: 
Size: 8408 Color: 409
Size: 2080 Color: 257
Size: 2020 Color: 252
Size: 1780 Color: 235
Size: 1774 Color: 234
Size: 400 Color: 67

Bin 198: 364 of cap free
Amount of items: 8
Items: 
Size: 8404 Color: 407
Size: 1512 Color: 212
Size: 1510 Color: 211
Size: 1398 Color: 202
Size: 1398 Color: 201
Size: 1398 Color: 200
Size: 408 Color: 71
Size: 408 Color: 70

Bin 199: 10696 of cap free
Amount of items: 11
Items: 
Size: 594 Color: 120
Size: 592 Color: 119
Size: 592 Color: 118
Size: 566 Color: 114
Size: 550 Color: 112
Size: 544 Color: 111
Size: 544 Color: 110
Size: 534 Color: 108
Size: 532 Color: 107
Size: 532 Color: 106
Size: 524 Color: 105

Total size: 3326400
Total free space: 16800

