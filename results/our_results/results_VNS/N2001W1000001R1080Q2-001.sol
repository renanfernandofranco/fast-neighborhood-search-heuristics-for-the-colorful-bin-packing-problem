Capicity Bin: 1000001
Lower Bound: 903

Bins used: 909
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 658337 Color: 0
Size: 185622 Color: 1
Size: 156042 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 761295 Color: 0
Size: 137342 Color: 1
Size: 101364 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 650366 Color: 0
Size: 194645 Color: 0
Size: 154990 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 786166 Color: 1
Size: 111170 Color: 0
Size: 102665 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 653697 Color: 0
Size: 230842 Color: 0
Size: 115462 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 791820 Color: 1
Size: 107796 Color: 1
Size: 100385 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 416617 Color: 1
Size: 352029 Color: 0
Size: 231355 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 379304 Color: 1
Size: 376750 Color: 1
Size: 243947 Color: 0

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 634524 Color: 1
Size: 365477 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 676762 Color: 1
Size: 187670 Color: 0
Size: 135569 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 772040 Color: 0
Size: 116584 Color: 0
Size: 111377 Color: 1

Bin 12: 1 of cap free
Amount of items: 2
Items: 
Size: 618477 Color: 0
Size: 381523 Color: 1

Bin 13: 1 of cap free
Amount of items: 3
Items: 
Size: 679853 Color: 0
Size: 160223 Color: 0
Size: 159924 Color: 1

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 707818 Color: 0
Size: 154812 Color: 0
Size: 137370 Color: 1

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 643489 Color: 0
Size: 238832 Color: 1
Size: 117679 Color: 1

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 735046 Color: 1
Size: 144583 Color: 0
Size: 120371 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 453554 Color: 1
Size: 441996 Color: 0
Size: 104450 Color: 1

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 781071 Color: 1
Size: 115498 Color: 1
Size: 103431 Color: 0

Bin 19: 2 of cap free
Amount of items: 3
Items: 
Size: 705446 Color: 1
Size: 165737 Color: 0
Size: 128816 Color: 1

Bin 20: 2 of cap free
Amount of items: 2
Items: 
Size: 714056 Color: 1
Size: 285943 Color: 0

Bin 21: 2 of cap free
Amount of items: 3
Items: 
Size: 726283 Color: 0
Size: 141458 Color: 1
Size: 132258 Color: 0

Bin 22: 2 of cap free
Amount of items: 3
Items: 
Size: 706094 Color: 1
Size: 165003 Color: 0
Size: 128902 Color: 1

Bin 23: 2 of cap free
Amount of items: 3
Items: 
Size: 686325 Color: 1
Size: 192076 Color: 0
Size: 121598 Color: 1

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 772222 Color: 0
Size: 119284 Color: 1
Size: 108493 Color: 1

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 563338 Color: 1
Size: 325891 Color: 0
Size: 110770 Color: 0

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 726902 Color: 1
Size: 145035 Color: 0
Size: 128062 Color: 1

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 558711 Color: 1
Size: 302205 Color: 0
Size: 139083 Color: 1

Bin 28: 3 of cap free
Amount of items: 3
Items: 
Size: 629912 Color: 0
Size: 201249 Color: 0
Size: 168837 Color: 1

Bin 29: 3 of cap free
Amount of items: 3
Items: 
Size: 657710 Color: 0
Size: 188836 Color: 0
Size: 153452 Color: 1

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 657982 Color: 0
Size: 184984 Color: 0
Size: 157032 Color: 1

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 686798 Color: 0
Size: 195833 Color: 0
Size: 117367 Color: 1

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 695953 Color: 1
Size: 304045 Color: 0

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 719368 Color: 1
Size: 152692 Color: 0
Size: 127938 Color: 1

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 751734 Color: 0
Size: 143819 Color: 1
Size: 104445 Color: 1

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 725502 Color: 0
Size: 151303 Color: 1
Size: 123193 Color: 0

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 420763 Color: 1
Size: 343183 Color: 0
Size: 236052 Color: 1

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 720544 Color: 1
Size: 145960 Color: 0
Size: 133494 Color: 1

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 733156 Color: 0
Size: 150890 Color: 1
Size: 115952 Color: 0

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 547909 Color: 1
Size: 452088 Color: 0

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 704846 Color: 0
Size: 185208 Color: 0
Size: 109943 Color: 1

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 713977 Color: 1
Size: 165424 Color: 0
Size: 120596 Color: 1

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 733999 Color: 0
Size: 138501 Color: 0
Size: 127497 Color: 1

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 598019 Color: 1
Size: 219739 Color: 0
Size: 182239 Color: 1

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 766865 Color: 0
Size: 126510 Color: 0
Size: 106622 Color: 1

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 473884 Color: 0
Size: 334441 Color: 0
Size: 191671 Color: 1

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 657774 Color: 1
Size: 195801 Color: 0
Size: 146421 Color: 1

Bin 47: 5 of cap free
Amount of items: 3
Items: 
Size: 702106 Color: 0
Size: 155212 Color: 0
Size: 142678 Color: 1

Bin 48: 5 of cap free
Amount of items: 2
Items: 
Size: 723103 Color: 1
Size: 276893 Color: 0

Bin 49: 5 of cap free
Amount of items: 3
Items: 
Size: 733181 Color: 0
Size: 154624 Color: 0
Size: 112191 Color: 1

Bin 50: 5 of cap free
Amount of items: 3
Items: 
Size: 761589 Color: 1
Size: 123806 Color: 0
Size: 114601 Color: 1

Bin 51: 5 of cap free
Amount of items: 3
Items: 
Size: 772603 Color: 1
Size: 126405 Color: 1
Size: 100988 Color: 0

Bin 52: 5 of cap free
Amount of items: 3
Items: 
Size: 732970 Color: 0
Size: 151874 Color: 1
Size: 115152 Color: 1

Bin 53: 5 of cap free
Amount of items: 3
Items: 
Size: 743568 Color: 1
Size: 152740 Color: 0
Size: 103688 Color: 1

Bin 54: 6 of cap free
Amount of items: 3
Items: 
Size: 642297 Color: 0
Size: 199131 Color: 0
Size: 158567 Color: 1

Bin 55: 6 of cap free
Amount of items: 3
Items: 
Size: 658126 Color: 1
Size: 184830 Color: 0
Size: 157039 Color: 1

Bin 56: 7 of cap free
Amount of items: 3
Items: 
Size: 470529 Color: 0
Size: 335318 Color: 0
Size: 194147 Color: 1

Bin 57: 7 of cap free
Amount of items: 2
Items: 
Size: 579942 Color: 1
Size: 420052 Color: 0

Bin 58: 7 of cap free
Amount of items: 3
Items: 
Size: 652812 Color: 1
Size: 180998 Color: 0
Size: 166184 Color: 1

Bin 59: 7 of cap free
Amount of items: 3
Items: 
Size: 710782 Color: 0
Size: 146132 Color: 1
Size: 143080 Color: 0

Bin 60: 7 of cap free
Amount of items: 2
Items: 
Size: 774889 Color: 0
Size: 225105 Color: 1

Bin 61: 7 of cap free
Amount of items: 3
Items: 
Size: 726818 Color: 1
Size: 151785 Color: 0
Size: 121391 Color: 1

Bin 62: 7 of cap free
Amount of items: 2
Items: 
Size: 743163 Color: 0
Size: 256831 Color: 1

Bin 63: 8 of cap free
Amount of items: 2
Items: 
Size: 626804 Color: 0
Size: 373189 Color: 1

Bin 64: 8 of cap free
Amount of items: 3
Items: 
Size: 661994 Color: 0
Size: 191279 Color: 1
Size: 146720 Color: 1

Bin 65: 8 of cap free
Amount of items: 3
Items: 
Size: 746065 Color: 0
Size: 133933 Color: 0
Size: 119995 Color: 1

Bin 66: 8 of cap free
Amount of items: 3
Items: 
Size: 414196 Color: 1
Size: 365532 Color: 0
Size: 220265 Color: 1

Bin 67: 9 of cap free
Amount of items: 2
Items: 
Size: 514796 Color: 1
Size: 485196 Color: 0

Bin 68: 9 of cap free
Amount of items: 2
Items: 
Size: 761427 Color: 0
Size: 238565 Color: 1

Bin 69: 9 of cap free
Amount of items: 3
Items: 
Size: 592655 Color: 0
Size: 218159 Color: 1
Size: 189178 Color: 1

Bin 70: 10 of cap free
Amount of items: 2
Items: 
Size: 560656 Color: 0
Size: 439335 Color: 1

Bin 71: 10 of cap free
Amount of items: 3
Items: 
Size: 703941 Color: 0
Size: 162305 Color: 0
Size: 133745 Color: 1

Bin 72: 10 of cap free
Amount of items: 3
Items: 
Size: 705513 Color: 1
Size: 151526 Color: 0
Size: 142952 Color: 1

Bin 73: 10 of cap free
Amount of items: 3
Items: 
Size: 659325 Color: 1
Size: 186746 Color: 0
Size: 153920 Color: 1

Bin 74: 10 of cap free
Amount of items: 3
Items: 
Size: 680541 Color: 1
Size: 199791 Color: 0
Size: 119659 Color: 0

Bin 75: 10 of cap free
Amount of items: 3
Items: 
Size: 752177 Color: 1
Size: 142046 Color: 0
Size: 105768 Color: 0

Bin 76: 11 of cap free
Amount of items: 2
Items: 
Size: 586125 Color: 0
Size: 413865 Color: 1

Bin 77: 11 of cap free
Amount of items: 3
Items: 
Size: 650388 Color: 0
Size: 182989 Color: 0
Size: 166613 Color: 1

Bin 78: 11 of cap free
Amount of items: 3
Items: 
Size: 676906 Color: 1
Size: 202213 Color: 0
Size: 120871 Color: 0

Bin 79: 12 of cap free
Amount of items: 2
Items: 
Size: 711618 Color: 1
Size: 288371 Color: 0

Bin 80: 12 of cap free
Amount of items: 3
Items: 
Size: 785338 Color: 1
Size: 114450 Color: 1
Size: 100201 Color: 0

Bin 81: 12 of cap free
Amount of items: 2
Items: 
Size: 751449 Color: 0
Size: 248540 Color: 1

Bin 82: 12 of cap free
Amount of items: 3
Items: 
Size: 675460 Color: 0
Size: 181400 Color: 1
Size: 143129 Color: 1

Bin 83: 12 of cap free
Amount of items: 3
Items: 
Size: 379648 Color: 1
Size: 363339 Color: 0
Size: 257002 Color: 1

Bin 84: 13 of cap free
Amount of items: 3
Items: 
Size: 653408 Color: 0
Size: 231509 Color: 0
Size: 115071 Color: 1

Bin 85: 13 of cap free
Amount of items: 3
Items: 
Size: 764883 Color: 1
Size: 126002 Color: 1
Size: 109103 Color: 0

Bin 86: 13 of cap free
Amount of items: 3
Items: 
Size: 702566 Color: 1
Size: 174655 Color: 1
Size: 122767 Color: 0

Bin 87: 13 of cap free
Amount of items: 3
Items: 
Size: 470049 Color: 0
Size: 278118 Color: 1
Size: 251821 Color: 1

Bin 88: 14 of cap free
Amount of items: 3
Items: 
Size: 701738 Color: 0
Size: 175912 Color: 0
Size: 122337 Color: 1

Bin 89: 14 of cap free
Amount of items: 2
Items: 
Size: 759428 Color: 0
Size: 240559 Color: 1

Bin 90: 14 of cap free
Amount of items: 3
Items: 
Size: 675968 Color: 0
Size: 194696 Color: 0
Size: 129323 Color: 1

Bin 91: 14 of cap free
Amount of items: 3
Items: 
Size: 586955 Color: 1
Size: 217691 Color: 1
Size: 195341 Color: 0

Bin 92: 15 of cap free
Amount of items: 3
Items: 
Size: 470139 Color: 0
Size: 292192 Color: 1
Size: 237655 Color: 1

Bin 93: 15 of cap free
Amount of items: 3
Items: 
Size: 381427 Color: 1
Size: 349233 Color: 0
Size: 269326 Color: 1

Bin 94: 16 of cap free
Amount of items: 3
Items: 
Size: 704515 Color: 0
Size: 165977 Color: 0
Size: 129493 Color: 1

Bin 95: 16 of cap free
Amount of items: 3
Items: 
Size: 741913 Color: 1
Size: 139994 Color: 0
Size: 118078 Color: 0

Bin 96: 16 of cap free
Amount of items: 3
Items: 
Size: 732210 Color: 0
Size: 136601 Color: 0
Size: 131174 Color: 1

Bin 97: 16 of cap free
Amount of items: 3
Items: 
Size: 379970 Color: 1
Size: 355403 Color: 0
Size: 264612 Color: 1

Bin 98: 17 of cap free
Amount of items: 2
Items: 
Size: 542647 Color: 0
Size: 457337 Color: 1

Bin 99: 17 of cap free
Amount of items: 2
Items: 
Size: 679346 Color: 1
Size: 320638 Color: 0

Bin 100: 17 of cap free
Amount of items: 2
Items: 
Size: 747747 Color: 1
Size: 252237 Color: 0

Bin 101: 17 of cap free
Amount of items: 2
Items: 
Size: 756354 Color: 0
Size: 243630 Color: 1

Bin 102: 17 of cap free
Amount of items: 3
Items: 
Size: 725400 Color: 1
Size: 167890 Color: 0
Size: 106694 Color: 0

Bin 103: 18 of cap free
Amount of items: 2
Items: 
Size: 611432 Color: 0
Size: 388551 Color: 1

Bin 104: 18 of cap free
Amount of items: 3
Items: 
Size: 756567 Color: 0
Size: 139252 Color: 0
Size: 104164 Color: 1

Bin 105: 18 of cap free
Amount of items: 4
Items: 
Size: 592361 Color: 1
Size: 180005 Color: 0
Size: 121075 Color: 0
Size: 106542 Color: 1

Bin 106: 18 of cap free
Amount of items: 3
Items: 
Size: 380534 Color: 1
Size: 360932 Color: 0
Size: 258517 Color: 0

Bin 107: 18 of cap free
Amount of items: 3
Items: 
Size: 374563 Color: 1
Size: 349632 Color: 0
Size: 275788 Color: 0

Bin 108: 19 of cap free
Amount of items: 2
Items: 
Size: 744685 Color: 0
Size: 255297 Color: 1

Bin 109: 19 of cap free
Amount of items: 2
Items: 
Size: 745287 Color: 0
Size: 254695 Color: 1

Bin 110: 19 of cap free
Amount of items: 3
Items: 
Size: 734807 Color: 1
Size: 133342 Color: 0
Size: 131833 Color: 1

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 586450 Color: 1
Size: 413531 Color: 0

Bin 112: 20 of cap free
Amount of items: 2
Items: 
Size: 671979 Color: 0
Size: 328002 Color: 1

Bin 113: 20 of cap free
Amount of items: 3
Items: 
Size: 771826 Color: 0
Size: 118815 Color: 0
Size: 109340 Color: 1

Bin 114: 20 of cap free
Amount of items: 3
Items: 
Size: 695999 Color: 0
Size: 154985 Color: 1
Size: 148997 Color: 0

Bin 115: 20 of cap free
Amount of items: 3
Items: 
Size: 744255 Color: 1
Size: 128115 Color: 1
Size: 127611 Color: 0

Bin 116: 21 of cap free
Amount of items: 2
Items: 
Size: 555072 Color: 1
Size: 444908 Color: 0

Bin 117: 21 of cap free
Amount of items: 2
Items: 
Size: 691930 Color: 0
Size: 308050 Color: 1

Bin 118: 21 of cap free
Amount of items: 2
Items: 
Size: 592399 Color: 1
Size: 407581 Color: 0

Bin 119: 21 of cap free
Amount of items: 3
Items: 
Size: 414278 Color: 1
Size: 354143 Color: 0
Size: 231559 Color: 1

Bin 120: 21 of cap free
Amount of items: 3
Items: 
Size: 732241 Color: 0
Size: 154389 Color: 0
Size: 113350 Color: 1

Bin 121: 21 of cap free
Amount of items: 3
Items: 
Size: 727162 Color: 1
Size: 162640 Color: 0
Size: 110178 Color: 0

Bin 122: 21 of cap free
Amount of items: 2
Items: 
Size: 529627 Color: 0
Size: 470353 Color: 1

Bin 123: 22 of cap free
Amount of items: 3
Items: 
Size: 757441 Color: 0
Size: 123672 Color: 1
Size: 118866 Color: 0

Bin 124: 22 of cap free
Amount of items: 2
Items: 
Size: 786962 Color: 0
Size: 213017 Color: 1

Bin 125: 22 of cap free
Amount of items: 3
Items: 
Size: 725473 Color: 1
Size: 165078 Color: 0
Size: 109428 Color: 1

Bin 126: 23 of cap free
Amount of items: 2
Items: 
Size: 511985 Color: 1
Size: 487993 Color: 0

Bin 127: 24 of cap free
Amount of items: 3
Items: 
Size: 654774 Color: 1
Size: 181899 Color: 0
Size: 163304 Color: 1

Bin 128: 24 of cap free
Amount of items: 2
Items: 
Size: 750412 Color: 0
Size: 249565 Color: 1

Bin 129: 24 of cap free
Amount of items: 3
Items: 
Size: 455660 Color: 1
Size: 355445 Color: 0
Size: 188872 Color: 0

Bin 130: 25 of cap free
Amount of items: 3
Items: 
Size: 592741 Color: 1
Size: 219831 Color: 0
Size: 187404 Color: 1

Bin 131: 25 of cap free
Amount of items: 2
Items: 
Size: 751664 Color: 1
Size: 248312 Color: 0

Bin 132: 25 of cap free
Amount of items: 3
Items: 
Size: 378344 Color: 1
Size: 354063 Color: 0
Size: 267569 Color: 0

Bin 133: 26 of cap free
Amount of items: 3
Items: 
Size: 702243 Color: 0
Size: 183672 Color: 0
Size: 114060 Color: 1

Bin 134: 26 of cap free
Amount of items: 3
Items: 
Size: 487527 Color: 0
Size: 333289 Color: 0
Size: 179159 Color: 1

Bin 135: 26 of cap free
Amount of items: 3
Items: 
Size: 478226 Color: 1
Size: 329137 Color: 0
Size: 192612 Color: 1

Bin 136: 27 of cap free
Amount of items: 3
Items: 
Size: 598579 Color: 0
Size: 220805 Color: 0
Size: 180590 Color: 1

Bin 137: 27 of cap free
Amount of items: 3
Items: 
Size: 656002 Color: 0
Size: 191298 Color: 0
Size: 152674 Color: 1

Bin 138: 27 of cap free
Amount of items: 2
Items: 
Size: 794500 Color: 0
Size: 205474 Color: 1

Bin 139: 28 of cap free
Amount of items: 2
Items: 
Size: 586421 Color: 1
Size: 413552 Color: 0

Bin 140: 28 of cap free
Amount of items: 3
Items: 
Size: 673776 Color: 0
Size: 215749 Color: 0
Size: 110448 Color: 1

Bin 141: 29 of cap free
Amount of items: 3
Items: 
Size: 639554 Color: 0
Size: 182238 Color: 1
Size: 178180 Color: 1

Bin 142: 29 of cap free
Amount of items: 3
Items: 
Size: 730823 Color: 1
Size: 144352 Color: 0
Size: 124797 Color: 0

Bin 143: 30 of cap free
Amount of items: 2
Items: 
Size: 615258 Color: 0
Size: 384713 Color: 1

Bin 144: 30 of cap free
Amount of items: 3
Items: 
Size: 734546 Color: 0
Size: 153740 Color: 1
Size: 111685 Color: 1

Bin 145: 31 of cap free
Amount of items: 3
Items: 
Size: 736867 Color: 1
Size: 150624 Color: 0
Size: 112479 Color: 1

Bin 146: 32 of cap free
Amount of items: 2
Items: 
Size: 696111 Color: 1
Size: 303858 Color: 0

Bin 147: 33 of cap free
Amount of items: 2
Items: 
Size: 560387 Color: 0
Size: 439581 Color: 1

Bin 148: 33 of cap free
Amount of items: 3
Items: 
Size: 757075 Color: 1
Size: 128215 Color: 1
Size: 114678 Color: 0

Bin 149: 34 of cap free
Amount of items: 3
Items: 
Size: 472820 Color: 0
Size: 342180 Color: 0
Size: 184967 Color: 1

Bin 150: 34 of cap free
Amount of items: 3
Items: 
Size: 689444 Color: 0
Size: 172238 Color: 1
Size: 138285 Color: 1

Bin 151: 34 of cap free
Amount of items: 3
Items: 
Size: 368332 Color: 0
Size: 334132 Color: 0
Size: 297503 Color: 1

Bin 152: 37 of cap free
Amount of items: 3
Items: 
Size: 639981 Color: 0
Size: 186341 Color: 1
Size: 173642 Color: 0

Bin 153: 37 of cap free
Amount of items: 3
Items: 
Size: 699578 Color: 0
Size: 197952 Color: 0
Size: 102434 Color: 1

Bin 154: 37 of cap free
Amount of items: 3
Items: 
Size: 379839 Color: 1
Size: 352315 Color: 0
Size: 267810 Color: 1

Bin 155: 38 of cap free
Amount of items: 2
Items: 
Size: 612590 Color: 1
Size: 387373 Color: 0

Bin 156: 38 of cap free
Amount of items: 3
Items: 
Size: 650045 Color: 1
Size: 180948 Color: 1
Size: 168970 Color: 0

Bin 157: 38 of cap free
Amount of items: 3
Items: 
Size: 713454 Color: 0
Size: 162116 Color: 0
Size: 124393 Color: 1

Bin 158: 39 of cap free
Amount of items: 3
Items: 
Size: 418066 Color: 1
Size: 346240 Color: 0
Size: 235656 Color: 1

Bin 159: 39 of cap free
Amount of items: 3
Items: 
Size: 651595 Color: 0
Size: 194756 Color: 0
Size: 153611 Color: 1

Bin 160: 40 of cap free
Amount of items: 2
Items: 
Size: 557182 Color: 1
Size: 442779 Color: 0

Bin 161: 40 of cap free
Amount of items: 2
Items: 
Size: 673898 Color: 0
Size: 326063 Color: 1

Bin 162: 41 of cap free
Amount of items: 2
Items: 
Size: 566559 Color: 1
Size: 433401 Color: 0

Bin 163: 41 of cap free
Amount of items: 2
Items: 
Size: 777316 Color: 0
Size: 222644 Color: 1

Bin 164: 41 of cap free
Amount of items: 3
Items: 
Size: 728390 Color: 1
Size: 137790 Color: 1
Size: 133780 Color: 0

Bin 165: 42 of cap free
Amount of items: 3
Items: 
Size: 724905 Color: 1
Size: 149346 Color: 0
Size: 125708 Color: 1

Bin 166: 42 of cap free
Amount of items: 2
Items: 
Size: 521114 Color: 1
Size: 478845 Color: 0

Bin 167: 42 of cap free
Amount of items: 3
Items: 
Size: 598941 Color: 1
Size: 219498 Color: 0
Size: 181520 Color: 0

Bin 168: 42 of cap free
Amount of items: 3
Items: 
Size: 528431 Color: 0
Size: 318215 Color: 0
Size: 153313 Color: 1

Bin 169: 43 of cap free
Amount of items: 2
Items: 
Size: 642072 Color: 1
Size: 357886 Color: 0

Bin 170: 44 of cap free
Amount of items: 3
Items: 
Size: 724124 Color: 0
Size: 157351 Color: 0
Size: 118482 Color: 1

Bin 171: 44 of cap free
Amount of items: 3
Items: 
Size: 388178 Color: 1
Size: 355216 Color: 0
Size: 256563 Color: 1

Bin 172: 45 of cap free
Amount of items: 2
Items: 
Size: 666949 Color: 1
Size: 333007 Color: 0

Bin 173: 46 of cap free
Amount of items: 2
Items: 
Size: 670278 Color: 0
Size: 329677 Color: 1

Bin 174: 47 of cap free
Amount of items: 2
Items: 
Size: 700702 Color: 1
Size: 299252 Color: 0

Bin 175: 47 of cap free
Amount of items: 3
Items: 
Size: 746471 Color: 0
Size: 127328 Color: 0
Size: 126155 Color: 1

Bin 176: 48 of cap free
Amount of items: 3
Items: 
Size: 732716 Color: 1
Size: 163142 Color: 1
Size: 104095 Color: 0

Bin 177: 48 of cap free
Amount of items: 2
Items: 
Size: 506979 Color: 0
Size: 492974 Color: 1

Bin 178: 48 of cap free
Amount of items: 3
Items: 
Size: 630679 Color: 0
Size: 204300 Color: 1
Size: 164974 Color: 1

Bin 179: 48 of cap free
Amount of items: 3
Items: 
Size: 702446 Color: 1
Size: 188312 Color: 1
Size: 109195 Color: 0

Bin 180: 51 of cap free
Amount of items: 2
Items: 
Size: 506723 Color: 1
Size: 493227 Color: 0

Bin 181: 51 of cap free
Amount of items: 3
Items: 
Size: 741626 Color: 0
Size: 131251 Color: 1
Size: 127073 Color: 0

Bin 182: 52 of cap free
Amount of items: 3
Items: 
Size: 371182 Color: 0
Size: 344916 Color: 0
Size: 283851 Color: 1

Bin 183: 52 of cap free
Amount of items: 2
Items: 
Size: 505607 Color: 1
Size: 494342 Color: 0

Bin 184: 52 of cap free
Amount of items: 2
Items: 
Size: 616111 Color: 0
Size: 383838 Color: 1

Bin 185: 52 of cap free
Amount of items: 2
Items: 
Size: 696101 Color: 1
Size: 303848 Color: 0

Bin 186: 53 of cap free
Amount of items: 2
Items: 
Size: 690574 Color: 0
Size: 309374 Color: 1

Bin 187: 54 of cap free
Amount of items: 3
Items: 
Size: 655627 Color: 0
Size: 201401 Color: 0
Size: 142919 Color: 1

Bin 188: 54 of cap free
Amount of items: 3
Items: 
Size: 419668 Color: 1
Size: 341780 Color: 0
Size: 238499 Color: 1

Bin 189: 54 of cap free
Amount of items: 3
Items: 
Size: 587539 Color: 1
Size: 239979 Color: 0
Size: 172429 Color: 1

Bin 190: 55 of cap free
Amount of items: 2
Items: 
Size: 663508 Color: 0
Size: 336438 Color: 1

Bin 191: 55 of cap free
Amount of items: 3
Items: 
Size: 380979 Color: 0
Size: 325739 Color: 0
Size: 293228 Color: 1

Bin 192: 57 of cap free
Amount of items: 2
Items: 
Size: 784444 Color: 0
Size: 215500 Color: 1

Bin 193: 58 of cap free
Amount of items: 3
Items: 
Size: 548959 Color: 0
Size: 255792 Color: 1
Size: 195192 Color: 1

Bin 194: 59 of cap free
Amount of items: 3
Items: 
Size: 418633 Color: 1
Size: 343047 Color: 0
Size: 238262 Color: 1

Bin 195: 59 of cap free
Amount of items: 3
Items: 
Size: 461664 Color: 1
Size: 331846 Color: 0
Size: 206432 Color: 1

Bin 196: 60 of cap free
Amount of items: 2
Items: 
Size: 504894 Color: 0
Size: 495047 Color: 1

Bin 197: 61 of cap free
Amount of items: 2
Items: 
Size: 684362 Color: 1
Size: 315578 Color: 0

Bin 198: 61 of cap free
Amount of items: 2
Items: 
Size: 769926 Color: 0
Size: 230014 Color: 1

Bin 199: 62 of cap free
Amount of items: 2
Items: 
Size: 648183 Color: 0
Size: 351756 Color: 1

Bin 200: 63 of cap free
Amount of items: 3
Items: 
Size: 433909 Color: 0
Size: 356160 Color: 0
Size: 209869 Color: 1

Bin 201: 64 of cap free
Amount of items: 2
Items: 
Size: 564598 Color: 1
Size: 435339 Color: 0

Bin 202: 65 of cap free
Amount of items: 3
Items: 
Size: 593035 Color: 0
Size: 222359 Color: 0
Size: 184542 Color: 1

Bin 203: 66 of cap free
Amount of items: 2
Items: 
Size: 642876 Color: 1
Size: 357059 Color: 0

Bin 204: 67 of cap free
Amount of items: 2
Items: 
Size: 772954 Color: 0
Size: 226980 Color: 1

Bin 205: 67 of cap free
Amount of items: 3
Items: 
Size: 368195 Color: 0
Size: 346031 Color: 0
Size: 285708 Color: 1

Bin 206: 69 of cap free
Amount of items: 2
Items: 
Size: 526803 Color: 1
Size: 473129 Color: 0

Bin 207: 69 of cap free
Amount of items: 2
Items: 
Size: 744075 Color: 0
Size: 255857 Color: 1

Bin 208: 70 of cap free
Amount of items: 2
Items: 
Size: 552942 Color: 0
Size: 446989 Color: 1

Bin 209: 70 of cap free
Amount of items: 2
Items: 
Size: 722663 Color: 0
Size: 277268 Color: 1

Bin 210: 70 of cap free
Amount of items: 2
Items: 
Size: 581818 Color: 0
Size: 418113 Color: 1

Bin 211: 70 of cap free
Amount of items: 3
Items: 
Size: 584691 Color: 1
Size: 216345 Color: 1
Size: 198895 Color: 0

Bin 212: 71 of cap free
Amount of items: 2
Items: 
Size: 680968 Color: 1
Size: 318962 Color: 0

Bin 213: 73 of cap free
Amount of items: 2
Items: 
Size: 790243 Color: 0
Size: 209685 Color: 1

Bin 214: 73 of cap free
Amount of items: 2
Items: 
Size: 795086 Color: 1
Size: 204842 Color: 0

Bin 215: 74 of cap free
Amount of items: 2
Items: 
Size: 622443 Color: 0
Size: 377484 Color: 1

Bin 216: 75 of cap free
Amount of items: 2
Items: 
Size: 637824 Color: 1
Size: 362102 Color: 0

Bin 217: 75 of cap free
Amount of items: 3
Items: 
Size: 650925 Color: 0
Size: 187319 Color: 1
Size: 161682 Color: 0

Bin 218: 75 of cap free
Amount of items: 3
Items: 
Size: 584191 Color: 0
Size: 298090 Color: 0
Size: 117645 Color: 1

Bin 219: 76 of cap free
Amount of items: 3
Items: 
Size: 423500 Color: 1
Size: 325555 Color: 0
Size: 250870 Color: 1

Bin 220: 76 of cap free
Amount of items: 3
Items: 
Size: 364334 Color: 0
Size: 364013 Color: 0
Size: 271578 Color: 1

Bin 221: 77 of cap free
Amount of items: 2
Items: 
Size: 658955 Color: 1
Size: 340969 Color: 0

Bin 222: 78 of cap free
Amount of items: 3
Items: 
Size: 593343 Color: 1
Size: 229550 Color: 0
Size: 177030 Color: 0

Bin 223: 78 of cap free
Amount of items: 2
Items: 
Size: 503384 Color: 0
Size: 496539 Color: 1

Bin 224: 80 of cap free
Amount of items: 2
Items: 
Size: 564059 Color: 0
Size: 435862 Color: 1

Bin 225: 80 of cap free
Amount of items: 3
Items: 
Size: 591144 Color: 0
Size: 233063 Color: 0
Size: 175714 Color: 1

Bin 226: 80 of cap free
Amount of items: 2
Items: 
Size: 633047 Color: 0
Size: 366874 Color: 1

Bin 227: 81 of cap free
Amount of items: 2
Items: 
Size: 551640 Color: 1
Size: 448280 Color: 0

Bin 228: 81 of cap free
Amount of items: 2
Items: 
Size: 619079 Color: 1
Size: 380841 Color: 0

Bin 229: 82 of cap free
Amount of items: 2
Items: 
Size: 580241 Color: 1
Size: 419678 Color: 0

Bin 230: 82 of cap free
Amount of items: 3
Items: 
Size: 549373 Color: 1
Size: 301015 Color: 0
Size: 149531 Color: 0

Bin 231: 83 of cap free
Amount of items: 3
Items: 
Size: 703692 Color: 1
Size: 163917 Color: 0
Size: 132309 Color: 0

Bin 232: 83 of cap free
Amount of items: 2
Items: 
Size: 542877 Color: 1
Size: 457041 Color: 0

Bin 233: 84 of cap free
Amount of items: 2
Items: 
Size: 625037 Color: 1
Size: 374880 Color: 0

Bin 234: 84 of cap free
Amount of items: 2
Items: 
Size: 667684 Color: 1
Size: 332233 Color: 0

Bin 235: 85 of cap free
Amount of items: 3
Items: 
Size: 651264 Color: 1
Size: 182881 Color: 1
Size: 165771 Color: 0

Bin 236: 85 of cap free
Amount of items: 2
Items: 
Size: 676634 Color: 1
Size: 323282 Color: 0

Bin 237: 85 of cap free
Amount of items: 2
Items: 
Size: 695876 Color: 1
Size: 304040 Color: 0

Bin 238: 86 of cap free
Amount of items: 3
Items: 
Size: 599225 Color: 0
Size: 219114 Color: 0
Size: 181576 Color: 1

Bin 239: 86 of cap free
Amount of items: 3
Items: 
Size: 470104 Color: 0
Size: 277080 Color: 1
Size: 252731 Color: 1

Bin 240: 87 of cap free
Amount of items: 3
Items: 
Size: 474870 Color: 0
Size: 332548 Color: 0
Size: 192496 Color: 1

Bin 241: 87 of cap free
Amount of items: 3
Items: 
Size: 459296 Color: 1
Size: 429441 Color: 0
Size: 111177 Color: 0

Bin 242: 88 of cap free
Amount of items: 2
Items: 
Size: 629308 Color: 0
Size: 370605 Color: 1

Bin 243: 88 of cap free
Amount of items: 2
Items: 
Size: 786155 Color: 0
Size: 213758 Color: 1

Bin 244: 88 of cap free
Amount of items: 3
Items: 
Size: 742445 Color: 1
Size: 156496 Color: 1
Size: 100972 Color: 0

Bin 245: 88 of cap free
Amount of items: 3
Items: 
Size: 589661 Color: 1
Size: 213182 Color: 1
Size: 197070 Color: 0

Bin 246: 89 of cap free
Amount of items: 2
Items: 
Size: 532210 Color: 0
Size: 467702 Color: 1

Bin 247: 89 of cap free
Amount of items: 2
Items: 
Size: 570858 Color: 1
Size: 429054 Color: 0

Bin 248: 90 of cap free
Amount of items: 3
Items: 
Size: 588269 Color: 1
Size: 243252 Color: 0
Size: 168390 Color: 1

Bin 249: 90 of cap free
Amount of items: 3
Items: 
Size: 484775 Color: 0
Size: 324690 Color: 0
Size: 190446 Color: 1

Bin 250: 91 of cap free
Amount of items: 2
Items: 
Size: 525423 Color: 1
Size: 474487 Color: 0

Bin 251: 91 of cap free
Amount of items: 3
Items: 
Size: 370733 Color: 0
Size: 351648 Color: 0
Size: 277529 Color: 1

Bin 252: 92 of cap free
Amount of items: 2
Items: 
Size: 612906 Color: 0
Size: 387003 Color: 1

Bin 253: 92 of cap free
Amount of items: 3
Items: 
Size: 411097 Color: 1
Size: 365435 Color: 0
Size: 223377 Color: 0

Bin 254: 94 of cap free
Amount of items: 3
Items: 
Size: 477769 Color: 1
Size: 325415 Color: 0
Size: 196723 Color: 1

Bin 255: 94 of cap free
Amount of items: 2
Items: 
Size: 598789 Color: 1
Size: 401118 Color: 0

Bin 256: 94 of cap free
Amount of items: 2
Items: 
Size: 799703 Color: 1
Size: 200204 Color: 0

Bin 257: 96 of cap free
Amount of items: 3
Items: 
Size: 592635 Color: 0
Size: 210411 Color: 1
Size: 196859 Color: 0

Bin 258: 97 of cap free
Amount of items: 2
Items: 
Size: 532367 Color: 0
Size: 467537 Color: 1

Bin 259: 97 of cap free
Amount of items: 2
Items: 
Size: 721683 Color: 1
Size: 278221 Color: 0

Bin 260: 98 of cap free
Amount of items: 2
Items: 
Size: 605462 Color: 0
Size: 394441 Color: 1

Bin 261: 98 of cap free
Amount of items: 2
Items: 
Size: 541865 Color: 0
Size: 458038 Color: 1

Bin 262: 99 of cap free
Amount of items: 2
Items: 
Size: 672729 Color: 0
Size: 327173 Color: 1

Bin 263: 100 of cap free
Amount of items: 2
Items: 
Size: 779031 Color: 0
Size: 220870 Color: 1

Bin 264: 100 of cap free
Amount of items: 3
Items: 
Size: 352897 Color: 0
Size: 348300 Color: 0
Size: 298704 Color: 1

Bin 265: 101 of cap free
Amount of items: 3
Items: 
Size: 454608 Color: 1
Size: 373741 Color: 0
Size: 171551 Color: 0

Bin 266: 102 of cap free
Amount of items: 2
Items: 
Size: 777097 Color: 1
Size: 222802 Color: 0

Bin 267: 103 of cap free
Amount of items: 2
Items: 
Size: 600360 Color: 0
Size: 399538 Color: 1

Bin 268: 105 of cap free
Amount of items: 3
Items: 
Size: 659256 Color: 0
Size: 179990 Color: 1
Size: 160650 Color: 0

Bin 269: 105 of cap free
Amount of items: 2
Items: 
Size: 520388 Color: 0
Size: 479508 Color: 1

Bin 270: 105 of cap free
Amount of items: 2
Items: 
Size: 520626 Color: 0
Size: 479270 Color: 1

Bin 271: 105 of cap free
Amount of items: 2
Items: 
Size: 768052 Color: 0
Size: 231844 Color: 1

Bin 272: 109 of cap free
Amount of items: 2
Items: 
Size: 560626 Color: 1
Size: 439266 Color: 0

Bin 273: 110 of cap free
Amount of items: 2
Items: 
Size: 571175 Color: 0
Size: 428716 Color: 1

Bin 274: 111 of cap free
Amount of items: 2
Items: 
Size: 515128 Color: 1
Size: 484762 Color: 0

Bin 275: 113 of cap free
Amount of items: 3
Items: 
Size: 415235 Color: 1
Size: 346511 Color: 0
Size: 238142 Color: 1

Bin 276: 115 of cap free
Amount of items: 3
Items: 
Size: 486795 Color: 0
Size: 326925 Color: 0
Size: 186166 Color: 1

Bin 277: 116 of cap free
Amount of items: 2
Items: 
Size: 768261 Color: 1
Size: 231624 Color: 0

Bin 278: 116 of cap free
Amount of items: 2
Items: 
Size: 519650 Color: 0
Size: 480235 Color: 1

Bin 279: 116 of cap free
Amount of items: 2
Items: 
Size: 604977 Color: 1
Size: 394908 Color: 0

Bin 280: 116 of cap free
Amount of items: 2
Items: 
Size: 674692 Color: 0
Size: 325193 Color: 1

Bin 281: 117 of cap free
Amount of items: 3
Items: 
Size: 643011 Color: 1
Size: 179574 Color: 1
Size: 177299 Color: 0

Bin 282: 117 of cap free
Amount of items: 2
Items: 
Size: 771918 Color: 0
Size: 227966 Color: 1

Bin 283: 118 of cap free
Amount of items: 2
Items: 
Size: 548309 Color: 1
Size: 451574 Color: 0

Bin 284: 118 of cap free
Amount of items: 3
Items: 
Size: 746745 Color: 1
Size: 151198 Color: 0
Size: 101940 Color: 1

Bin 285: 119 of cap free
Amount of items: 2
Items: 
Size: 696627 Color: 1
Size: 303255 Color: 0

Bin 286: 119 of cap free
Amount of items: 2
Items: 
Size: 583755 Color: 0
Size: 416127 Color: 1

Bin 287: 120 of cap free
Amount of items: 2
Items: 
Size: 781975 Color: 0
Size: 217906 Color: 1

Bin 288: 121 of cap free
Amount of items: 3
Items: 
Size: 471312 Color: 0
Size: 293442 Color: 1
Size: 235126 Color: 1

Bin 289: 122 of cap free
Amount of items: 3
Items: 
Size: 379959 Color: 1
Size: 355399 Color: 0
Size: 264521 Color: 1

Bin 290: 123 of cap free
Amount of items: 2
Items: 
Size: 639253 Color: 0
Size: 360625 Color: 1

Bin 291: 124 of cap free
Amount of items: 3
Items: 
Size: 417312 Color: 1
Size: 344596 Color: 0
Size: 237969 Color: 1

Bin 292: 124 of cap free
Amount of items: 2
Items: 
Size: 683854 Color: 0
Size: 316023 Color: 1

Bin 293: 124 of cap free
Amount of items: 2
Items: 
Size: 687894 Color: 1
Size: 311983 Color: 0

Bin 294: 125 of cap free
Amount of items: 3
Items: 
Size: 716159 Color: 1
Size: 151581 Color: 0
Size: 132136 Color: 0

Bin 295: 126 of cap free
Amount of items: 2
Items: 
Size: 553503 Color: 0
Size: 446372 Color: 1

Bin 296: 127 of cap free
Amount of items: 2
Items: 
Size: 645737 Color: 1
Size: 354137 Color: 0

Bin 297: 128 of cap free
Amount of items: 2
Items: 
Size: 646266 Color: 0
Size: 353607 Color: 1

Bin 298: 128 of cap free
Amount of items: 3
Items: 
Size: 368544 Color: 0
Size: 347286 Color: 0
Size: 284043 Color: 1

Bin 299: 130 of cap free
Amount of items: 2
Items: 
Size: 719414 Color: 0
Size: 280457 Color: 1

Bin 300: 131 of cap free
Amount of items: 2
Items: 
Size: 705841 Color: 0
Size: 294029 Color: 1

Bin 301: 132 of cap free
Amount of items: 3
Items: 
Size: 678810 Color: 0
Size: 185220 Color: 0
Size: 135839 Color: 1

Bin 302: 134 of cap free
Amount of items: 2
Items: 
Size: 652869 Color: 1
Size: 346998 Color: 0

Bin 303: 138 of cap free
Amount of items: 2
Items: 
Size: 669591 Color: 1
Size: 330272 Color: 0

Bin 304: 140 of cap free
Amount of items: 2
Items: 
Size: 623729 Color: 0
Size: 376132 Color: 1

Bin 305: 142 of cap free
Amount of items: 2
Items: 
Size: 604446 Color: 0
Size: 395413 Color: 1

Bin 306: 142 of cap free
Amount of items: 2
Items: 
Size: 797829 Color: 1
Size: 202030 Color: 0

Bin 307: 143 of cap free
Amount of items: 2
Items: 
Size: 708560 Color: 1
Size: 291298 Color: 0

Bin 308: 144 of cap free
Amount of items: 2
Items: 
Size: 640452 Color: 1
Size: 359405 Color: 0

Bin 309: 145 of cap free
Amount of items: 2
Items: 
Size: 674003 Color: 1
Size: 325853 Color: 0

Bin 310: 150 of cap free
Amount of items: 2
Items: 
Size: 538227 Color: 0
Size: 461624 Color: 1

Bin 311: 150 of cap free
Amount of items: 2
Items: 
Size: 609294 Color: 0
Size: 390557 Color: 1

Bin 312: 150 of cap free
Amount of items: 3
Items: 
Size: 456359 Color: 1
Size: 334477 Color: 0
Size: 209015 Color: 1

Bin 313: 155 of cap free
Amount of items: 3
Items: 
Size: 558928 Color: 0
Size: 299861 Color: 0
Size: 141057 Color: 1

Bin 314: 155 of cap free
Amount of items: 2
Items: 
Size: 784120 Color: 0
Size: 215726 Color: 1

Bin 315: 156 of cap free
Amount of items: 3
Items: 
Size: 728865 Color: 0
Size: 147001 Color: 1
Size: 123979 Color: 0

Bin 316: 157 of cap free
Amount of items: 2
Items: 
Size: 715522 Color: 0
Size: 284322 Color: 1

Bin 317: 158 of cap free
Amount of items: 2
Items: 
Size: 646443 Color: 0
Size: 353400 Color: 1

Bin 318: 159 of cap free
Amount of items: 2
Items: 
Size: 758632 Color: 0
Size: 241210 Color: 1

Bin 319: 160 of cap free
Amount of items: 2
Items: 
Size: 705816 Color: 1
Size: 294025 Color: 0

Bin 320: 161 of cap free
Amount of items: 2
Items: 
Size: 549066 Color: 1
Size: 450774 Color: 0

Bin 321: 161 of cap free
Amount of items: 2
Items: 
Size: 645553 Color: 1
Size: 354287 Color: 0

Bin 322: 161 of cap free
Amount of items: 3
Items: 
Size: 412105 Color: 1
Size: 364868 Color: 0
Size: 222867 Color: 1

Bin 323: 162 of cap free
Amount of items: 2
Items: 
Size: 552662 Color: 0
Size: 447177 Color: 1

Bin 324: 162 of cap free
Amount of items: 3
Items: 
Size: 376754 Color: 1
Size: 352877 Color: 0
Size: 270208 Color: 1

Bin 325: 165 of cap free
Amount of items: 2
Items: 
Size: 628997 Color: 0
Size: 370839 Color: 1

Bin 326: 166 of cap free
Amount of items: 2
Items: 
Size: 514164 Color: 1
Size: 485671 Color: 0

Bin 327: 168 of cap free
Amount of items: 2
Items: 
Size: 506178 Color: 0
Size: 493655 Color: 1

Bin 328: 168 of cap free
Amount of items: 2
Items: 
Size: 565110 Color: 1
Size: 434723 Color: 0

Bin 329: 168 of cap free
Amount of items: 2
Items: 
Size: 789708 Color: 1
Size: 210125 Color: 0

Bin 330: 170 of cap free
Amount of items: 2
Items: 
Size: 631844 Color: 0
Size: 367987 Color: 1

Bin 331: 170 of cap free
Amount of items: 2
Items: 
Size: 648055 Color: 1
Size: 351776 Color: 0

Bin 332: 171 of cap free
Amount of items: 2
Items: 
Size: 656481 Color: 1
Size: 343349 Color: 0

Bin 333: 171 of cap free
Amount of items: 2
Items: 
Size: 658667 Color: 0
Size: 341163 Color: 1

Bin 334: 174 of cap free
Amount of items: 2
Items: 
Size: 618782 Color: 0
Size: 381045 Color: 1

Bin 335: 176 of cap free
Amount of items: 3
Items: 
Size: 668418 Color: 1
Size: 166706 Color: 0
Size: 164701 Color: 1

Bin 336: 176 of cap free
Amount of items: 2
Items: 
Size: 700141 Color: 1
Size: 299684 Color: 0

Bin 337: 177 of cap free
Amount of items: 2
Items: 
Size: 517937 Color: 1
Size: 481887 Color: 0

Bin 338: 179 of cap free
Amount of items: 2
Items: 
Size: 636724 Color: 1
Size: 363098 Color: 0

Bin 339: 181 of cap free
Amount of items: 3
Items: 
Size: 689987 Color: 0
Size: 160357 Color: 1
Size: 149476 Color: 0

Bin 340: 184 of cap free
Amount of items: 2
Items: 
Size: 530505 Color: 0
Size: 469312 Color: 1

Bin 341: 184 of cap free
Amount of items: 2
Items: 
Size: 602215 Color: 1
Size: 397602 Color: 0

Bin 342: 184 of cap free
Amount of items: 2
Items: 
Size: 619612 Color: 1
Size: 380205 Color: 0

Bin 343: 184 of cap free
Amount of items: 2
Items: 
Size: 765888 Color: 0
Size: 233929 Color: 1

Bin 344: 185 of cap free
Amount of items: 2
Items: 
Size: 501689 Color: 0
Size: 498127 Color: 1

Bin 345: 186 of cap free
Amount of items: 2
Items: 
Size: 636985 Color: 1
Size: 362830 Color: 0

Bin 346: 186 of cap free
Amount of items: 2
Items: 
Size: 647824 Color: 0
Size: 351991 Color: 1

Bin 347: 191 of cap free
Amount of items: 2
Items: 
Size: 738313 Color: 0
Size: 261497 Color: 1

Bin 348: 196 of cap free
Amount of items: 2
Items: 
Size: 678158 Color: 0
Size: 321647 Color: 1

Bin 349: 199 of cap free
Amount of items: 2
Items: 
Size: 627981 Color: 1
Size: 371821 Color: 0

Bin 350: 200 of cap free
Amount of items: 2
Items: 
Size: 736264 Color: 1
Size: 263537 Color: 0

Bin 351: 204 of cap free
Amount of items: 2
Items: 
Size: 548538 Color: 0
Size: 451259 Color: 1

Bin 352: 208 of cap free
Amount of items: 2
Items: 
Size: 761018 Color: 1
Size: 238775 Color: 0

Bin 353: 209 of cap free
Amount of items: 2
Items: 
Size: 739350 Color: 1
Size: 260442 Color: 0

Bin 354: 209 of cap free
Amount of items: 2
Items: 
Size: 775145 Color: 1
Size: 224647 Color: 0

Bin 355: 211 of cap free
Amount of items: 2
Items: 
Size: 499918 Color: 1
Size: 499872 Color: 0

Bin 356: 212 of cap free
Amount of items: 2
Items: 
Size: 784437 Color: 0
Size: 215352 Color: 1

Bin 357: 213 of cap free
Amount of items: 2
Items: 
Size: 593914 Color: 0
Size: 405874 Color: 1

Bin 358: 213 of cap free
Amount of items: 2
Items: 
Size: 598691 Color: 0
Size: 401097 Color: 1

Bin 359: 213 of cap free
Amount of items: 3
Items: 
Size: 654792 Color: 0
Size: 174175 Color: 1
Size: 170821 Color: 1

Bin 360: 213 of cap free
Amount of items: 2
Items: 
Size: 699212 Color: 1
Size: 300576 Color: 0

Bin 361: 214 of cap free
Amount of items: 2
Items: 
Size: 736403 Color: 0
Size: 263384 Color: 1

Bin 362: 215 of cap free
Amount of items: 2
Items: 
Size: 531641 Color: 1
Size: 468145 Color: 0

Bin 363: 216 of cap free
Amount of items: 2
Items: 
Size: 668705 Color: 1
Size: 331080 Color: 0

Bin 364: 220 of cap free
Amount of items: 2
Items: 
Size: 718451 Color: 0
Size: 281330 Color: 1

Bin 365: 221 of cap free
Amount of items: 2
Items: 
Size: 593585 Color: 0
Size: 406195 Color: 1

Bin 366: 223 of cap free
Amount of items: 2
Items: 
Size: 538874 Color: 0
Size: 460904 Color: 1

Bin 367: 225 of cap free
Amount of items: 2
Items: 
Size: 512618 Color: 0
Size: 487158 Color: 1

Bin 368: 225 of cap free
Amount of items: 2
Items: 
Size: 620876 Color: 0
Size: 378900 Color: 1

Bin 369: 225 of cap free
Amount of items: 3
Items: 
Size: 381949 Color: 1
Size: 363387 Color: 0
Size: 254440 Color: 0

Bin 370: 226 of cap free
Amount of items: 2
Items: 
Size: 596195 Color: 0
Size: 403580 Color: 1

Bin 371: 229 of cap free
Amount of items: 2
Items: 
Size: 620874 Color: 0
Size: 378898 Color: 1

Bin 372: 229 of cap free
Amount of items: 2
Items: 
Size: 689884 Color: 1
Size: 309888 Color: 0

Bin 373: 231 of cap free
Amount of items: 2
Items: 
Size: 517084 Color: 1
Size: 482686 Color: 0

Bin 374: 231 of cap free
Amount of items: 3
Items: 
Size: 380712 Color: 1
Size: 361118 Color: 0
Size: 257940 Color: 0

Bin 375: 232 of cap free
Amount of items: 2
Items: 
Size: 505957 Color: 1
Size: 493812 Color: 0

Bin 376: 238 of cap free
Amount of items: 2
Items: 
Size: 791870 Color: 1
Size: 207893 Color: 0

Bin 377: 241 of cap free
Amount of items: 2
Items: 
Size: 773191 Color: 1
Size: 226569 Color: 0

Bin 378: 245 of cap free
Amount of items: 2
Items: 
Size: 596716 Color: 1
Size: 403040 Color: 0

Bin 379: 246 of cap free
Amount of items: 2
Items: 
Size: 726217 Color: 0
Size: 273538 Color: 1

Bin 380: 246 of cap free
Amount of items: 2
Items: 
Size: 555985 Color: 1
Size: 443770 Color: 0

Bin 381: 246 of cap free
Amount of items: 2
Items: 
Size: 669916 Color: 0
Size: 329839 Color: 1

Bin 382: 247 of cap free
Amount of items: 2
Items: 
Size: 551408 Color: 0
Size: 448346 Color: 1

Bin 383: 247 of cap free
Amount of items: 2
Items: 
Size: 768816 Color: 1
Size: 230938 Color: 0

Bin 384: 248 of cap free
Amount of items: 2
Items: 
Size: 752055 Color: 0
Size: 247698 Color: 1

Bin 385: 252 of cap free
Amount of items: 2
Items: 
Size: 557583 Color: 1
Size: 442166 Color: 0

Bin 386: 253 of cap free
Amount of items: 2
Items: 
Size: 535504 Color: 1
Size: 464244 Color: 0

Bin 387: 254 of cap free
Amount of items: 2
Items: 
Size: 544955 Color: 0
Size: 454792 Color: 1

Bin 388: 254 of cap free
Amount of items: 2
Items: 
Size: 621646 Color: 1
Size: 378101 Color: 0

Bin 389: 254 of cap free
Amount of items: 2
Items: 
Size: 758495 Color: 1
Size: 241252 Color: 0

Bin 390: 254 of cap free
Amount of items: 3
Items: 
Size: 547201 Color: 0
Size: 255476 Color: 1
Size: 197070 Color: 1

Bin 391: 255 of cap free
Amount of items: 2
Items: 
Size: 544271 Color: 1
Size: 455475 Color: 0

Bin 392: 255 of cap free
Amount of items: 2
Items: 
Size: 545387 Color: 1
Size: 454359 Color: 0

Bin 393: 256 of cap free
Amount of items: 2
Items: 
Size: 524710 Color: 0
Size: 475035 Color: 1

Bin 394: 258 of cap free
Amount of items: 3
Items: 
Size: 674014 Color: 0
Size: 172919 Color: 0
Size: 152810 Color: 1

Bin 395: 259 of cap free
Amount of items: 2
Items: 
Size: 666031 Color: 1
Size: 333711 Color: 0

Bin 396: 260 of cap free
Amount of items: 2
Items: 
Size: 624763 Color: 0
Size: 374978 Color: 1

Bin 397: 264 of cap free
Amount of items: 2
Items: 
Size: 784285 Color: 1
Size: 215452 Color: 0

Bin 398: 265 of cap free
Amount of items: 2
Items: 
Size: 565346 Color: 1
Size: 434390 Color: 0

Bin 399: 265 of cap free
Amount of items: 2
Items: 
Size: 566482 Color: 0
Size: 433254 Color: 1

Bin 400: 268 of cap free
Amount of items: 2
Items: 
Size: 718060 Color: 1
Size: 281673 Color: 0

Bin 401: 271 of cap free
Amount of items: 2
Items: 
Size: 576263 Color: 1
Size: 423467 Color: 0

Bin 402: 271 of cap free
Amount of items: 2
Items: 
Size: 708507 Color: 1
Size: 291223 Color: 0

Bin 403: 277 of cap free
Amount of items: 2
Items: 
Size: 530388 Color: 1
Size: 469336 Color: 0

Bin 404: 278 of cap free
Amount of items: 2
Items: 
Size: 748498 Color: 0
Size: 251225 Color: 1

Bin 405: 278 of cap free
Amount of items: 2
Items: 
Size: 769778 Color: 0
Size: 229945 Color: 1

Bin 406: 280 of cap free
Amount of items: 2
Items: 
Size: 671925 Color: 1
Size: 327796 Color: 0

Bin 407: 280 of cap free
Amount of items: 2
Items: 
Size: 685448 Color: 0
Size: 314273 Color: 1

Bin 408: 281 of cap free
Amount of items: 2
Items: 
Size: 787290 Color: 0
Size: 212430 Color: 1

Bin 409: 285 of cap free
Amount of items: 2
Items: 
Size: 560056 Color: 0
Size: 439660 Color: 1

Bin 410: 285 of cap free
Amount of items: 2
Items: 
Size: 578917 Color: 0
Size: 420799 Color: 1

Bin 411: 288 of cap free
Amount of items: 2
Items: 
Size: 582206 Color: 1
Size: 417507 Color: 0

Bin 412: 292 of cap free
Amount of items: 2
Items: 
Size: 707489 Color: 1
Size: 292220 Color: 0

Bin 413: 298 of cap free
Amount of items: 2
Items: 
Size: 739165 Color: 0
Size: 260538 Color: 1

Bin 414: 300 of cap free
Amount of items: 2
Items: 
Size: 558973 Color: 1
Size: 440728 Color: 0

Bin 415: 301 of cap free
Amount of items: 2
Items: 
Size: 754151 Color: 0
Size: 245549 Color: 1

Bin 416: 302 of cap free
Amount of items: 2
Items: 
Size: 786814 Color: 1
Size: 212885 Color: 0

Bin 417: 303 of cap free
Amount of items: 3
Items: 
Size: 698413 Color: 0
Size: 151507 Color: 1
Size: 149778 Color: 0

Bin 418: 306 of cap free
Amount of items: 2
Items: 
Size: 581087 Color: 1
Size: 418608 Color: 0

Bin 419: 307 of cap free
Amount of items: 2
Items: 
Size: 759416 Color: 1
Size: 240278 Color: 0

Bin 420: 311 of cap free
Amount of items: 2
Items: 
Size: 794652 Color: 1
Size: 205038 Color: 0

Bin 421: 316 of cap free
Amount of items: 2
Items: 
Size: 533548 Color: 0
Size: 466137 Color: 1

Bin 422: 318 of cap free
Amount of items: 3
Items: 
Size: 555088 Color: 0
Size: 302153 Color: 0
Size: 142442 Color: 1

Bin 423: 322 of cap free
Amount of items: 2
Items: 
Size: 557996 Color: 0
Size: 441683 Color: 1

Bin 424: 322 of cap free
Amount of items: 2
Items: 
Size: 660287 Color: 1
Size: 339392 Color: 0

Bin 425: 322 of cap free
Amount of items: 2
Items: 
Size: 732676 Color: 1
Size: 267003 Color: 0

Bin 426: 324 of cap free
Amount of items: 2
Items: 
Size: 564405 Color: 1
Size: 435272 Color: 0

Bin 427: 326 of cap free
Amount of items: 2
Items: 
Size: 706291 Color: 1
Size: 293384 Color: 0

Bin 428: 327 of cap free
Amount of items: 2
Items: 
Size: 703521 Color: 1
Size: 296153 Color: 0

Bin 429: 330 of cap free
Amount of items: 2
Items: 
Size: 513237 Color: 0
Size: 486434 Color: 1

Bin 430: 330 of cap free
Amount of items: 2
Items: 
Size: 615137 Color: 1
Size: 384534 Color: 0

Bin 431: 333 of cap free
Amount of items: 2
Items: 
Size: 578351 Color: 0
Size: 421317 Color: 1

Bin 432: 334 of cap free
Amount of items: 2
Items: 
Size: 643930 Color: 0
Size: 355737 Color: 1

Bin 433: 335 of cap free
Amount of items: 2
Items: 
Size: 557357 Color: 0
Size: 442309 Color: 1

Bin 434: 337 of cap free
Amount of items: 2
Items: 
Size: 670347 Color: 1
Size: 329317 Color: 0

Bin 435: 338 of cap free
Amount of items: 2
Items: 
Size: 553635 Color: 0
Size: 446028 Color: 1

Bin 436: 339 of cap free
Amount of items: 2
Items: 
Size: 634019 Color: 1
Size: 365643 Color: 0

Bin 437: 341 of cap free
Amount of items: 2
Items: 
Size: 602401 Color: 0
Size: 397259 Color: 1

Bin 438: 342 of cap free
Amount of items: 2
Items: 
Size: 678720 Color: 1
Size: 320939 Color: 0

Bin 439: 343 of cap free
Amount of items: 2
Items: 
Size: 540888 Color: 0
Size: 458770 Color: 1

Bin 440: 345 of cap free
Amount of items: 2
Items: 
Size: 665513 Color: 0
Size: 334143 Color: 1

Bin 441: 356 of cap free
Amount of items: 2
Items: 
Size: 517350 Color: 0
Size: 482295 Color: 1

Bin 442: 359 of cap free
Amount of items: 2
Items: 
Size: 755923 Color: 1
Size: 243719 Color: 0

Bin 443: 375 of cap free
Amount of items: 2
Items: 
Size: 568972 Color: 1
Size: 430654 Color: 0

Bin 444: 376 of cap free
Amount of items: 2
Items: 
Size: 520176 Color: 0
Size: 479449 Color: 1

Bin 445: 377 of cap free
Amount of items: 2
Items: 
Size: 560430 Color: 1
Size: 439194 Color: 0

Bin 446: 378 of cap free
Amount of items: 2
Items: 
Size: 776448 Color: 0
Size: 223175 Color: 1

Bin 447: 380 of cap free
Amount of items: 2
Items: 
Size: 553103 Color: 0
Size: 446518 Color: 1

Bin 448: 380 of cap free
Amount of items: 2
Items: 
Size: 649403 Color: 0
Size: 350218 Color: 1

Bin 449: 385 of cap free
Amount of items: 2
Items: 
Size: 546699 Color: 1
Size: 452917 Color: 0

Bin 450: 385 of cap free
Amount of items: 2
Items: 
Size: 711287 Color: 0
Size: 288329 Color: 1

Bin 451: 389 of cap free
Amount of items: 2
Items: 
Size: 619550 Color: 1
Size: 380062 Color: 0

Bin 452: 390 of cap free
Amount of items: 2
Items: 
Size: 709791 Color: 0
Size: 289820 Color: 1

Bin 453: 394 of cap free
Amount of items: 2
Items: 
Size: 562867 Color: 0
Size: 436740 Color: 1

Bin 454: 395 of cap free
Amount of items: 2
Items: 
Size: 736383 Color: 0
Size: 263223 Color: 1

Bin 455: 404 of cap free
Amount of items: 2
Items: 
Size: 760454 Color: 1
Size: 239143 Color: 0

Bin 456: 406 of cap free
Amount of items: 2
Items: 
Size: 522857 Color: 0
Size: 476738 Color: 1

Bin 457: 407 of cap free
Amount of items: 2
Items: 
Size: 536929 Color: 0
Size: 462665 Color: 1

Bin 458: 407 of cap free
Amount of items: 2
Items: 
Size: 537454 Color: 0
Size: 462140 Color: 1

Bin 459: 408 of cap free
Amount of items: 2
Items: 
Size: 585025 Color: 1
Size: 414568 Color: 0

Bin 460: 410 of cap free
Amount of items: 2
Items: 
Size: 704455 Color: 1
Size: 295136 Color: 0

Bin 461: 412 of cap free
Amount of items: 2
Items: 
Size: 554602 Color: 0
Size: 444987 Color: 1

Bin 462: 414 of cap free
Amount of items: 2
Items: 
Size: 798755 Color: 0
Size: 200832 Color: 1

Bin 463: 415 of cap free
Amount of items: 2
Items: 
Size: 614046 Color: 0
Size: 385540 Color: 1

Bin 464: 420 of cap free
Amount of items: 3
Items: 
Size: 639719 Color: 1
Size: 199223 Color: 0
Size: 160639 Color: 0

Bin 465: 420 of cap free
Amount of items: 2
Items: 
Size: 549135 Color: 0
Size: 450446 Color: 1

Bin 466: 420 of cap free
Amount of items: 2
Items: 
Size: 717997 Color: 1
Size: 281584 Color: 0

Bin 467: 421 of cap free
Amount of items: 3
Items: 
Size: 547881 Color: 0
Size: 307557 Color: 0
Size: 144142 Color: 1

Bin 468: 423 of cap free
Amount of items: 2
Items: 
Size: 759230 Color: 0
Size: 240348 Color: 1

Bin 469: 430 of cap free
Amount of items: 2
Items: 
Size: 580033 Color: 0
Size: 419538 Color: 1

Bin 470: 431 of cap free
Amount of items: 2
Items: 
Size: 562269 Color: 1
Size: 437301 Color: 0

Bin 471: 439 of cap free
Amount of items: 2
Items: 
Size: 735652 Color: 0
Size: 263910 Color: 1

Bin 472: 442 of cap free
Amount of items: 2
Items: 
Size: 604071 Color: 1
Size: 395488 Color: 0

Bin 473: 443 of cap free
Amount of items: 2
Items: 
Size: 756051 Color: 0
Size: 243507 Color: 1

Bin 474: 445 of cap free
Amount of items: 2
Items: 
Size: 508938 Color: 0
Size: 490618 Color: 1

Bin 475: 445 of cap free
Amount of items: 2
Items: 
Size: 573346 Color: 1
Size: 426210 Color: 0

Bin 476: 446 of cap free
Amount of items: 3
Items: 
Size: 581559 Color: 0
Size: 299001 Color: 0
Size: 118995 Color: 1

Bin 477: 448 of cap free
Amount of items: 2
Items: 
Size: 566988 Color: 0
Size: 432565 Color: 1

Bin 478: 450 of cap free
Amount of items: 2
Items: 
Size: 572921 Color: 0
Size: 426630 Color: 1

Bin 479: 451 of cap free
Amount of items: 2
Items: 
Size: 645440 Color: 0
Size: 354110 Color: 1

Bin 480: 461 of cap free
Amount of items: 2
Items: 
Size: 601996 Color: 1
Size: 397544 Color: 0

Bin 481: 463 of cap free
Amount of items: 2
Items: 
Size: 773823 Color: 0
Size: 225715 Color: 1

Bin 482: 474 of cap free
Amount of items: 2
Items: 
Size: 580985 Color: 1
Size: 418542 Color: 0

Bin 483: 478 of cap free
Amount of items: 2
Items: 
Size: 721184 Color: 1
Size: 278339 Color: 0

Bin 484: 483 of cap free
Amount of items: 2
Items: 
Size: 507718 Color: 1
Size: 491800 Color: 0

Bin 485: 484 of cap free
Amount of items: 2
Items: 
Size: 571834 Color: 1
Size: 427683 Color: 0

Bin 486: 484 of cap free
Amount of items: 2
Items: 
Size: 656660 Color: 0
Size: 342857 Color: 1

Bin 487: 484 of cap free
Amount of items: 2
Items: 
Size: 769633 Color: 1
Size: 229884 Color: 0

Bin 488: 490 of cap free
Amount of items: 2
Items: 
Size: 571825 Color: 0
Size: 427686 Color: 1

Bin 489: 492 of cap free
Amount of items: 2
Items: 
Size: 622325 Color: 0
Size: 377184 Color: 1

Bin 490: 502 of cap free
Amount of items: 2
Items: 
Size: 664630 Color: 0
Size: 334869 Color: 1

Bin 491: 505 of cap free
Amount of items: 2
Items: 
Size: 725327 Color: 1
Size: 274169 Color: 0

Bin 492: 507 of cap free
Amount of items: 2
Items: 
Size: 784991 Color: 0
Size: 214503 Color: 1

Bin 493: 510 of cap free
Amount of items: 2
Items: 
Size: 754016 Color: 0
Size: 245475 Color: 1

Bin 494: 517 of cap free
Amount of items: 2
Items: 
Size: 620819 Color: 0
Size: 378665 Color: 1

Bin 495: 520 of cap free
Amount of items: 2
Items: 
Size: 505931 Color: 0
Size: 493550 Color: 1

Bin 496: 523 of cap free
Amount of items: 2
Items: 
Size: 532563 Color: 1
Size: 466915 Color: 0

Bin 497: 523 of cap free
Amount of items: 2
Items: 
Size: 778254 Color: 1
Size: 221224 Color: 0

Bin 498: 526 of cap free
Amount of items: 2
Items: 
Size: 782505 Color: 1
Size: 216970 Color: 0

Bin 499: 526 of cap free
Amount of items: 2
Items: 
Size: 772109 Color: 1
Size: 227366 Color: 0

Bin 500: 528 of cap free
Amount of items: 2
Items: 
Size: 770780 Color: 1
Size: 228693 Color: 0

Bin 501: 528 of cap free
Amount of items: 2
Items: 
Size: 668811 Color: 0
Size: 330662 Color: 1

Bin 502: 534 of cap free
Amount of items: 2
Items: 
Size: 719916 Color: 0
Size: 279551 Color: 1

Bin 503: 537 of cap free
Amount of items: 2
Items: 
Size: 697319 Color: 1
Size: 302145 Color: 0

Bin 504: 537 of cap free
Amount of items: 3
Items: 
Size: 371790 Color: 0
Size: 349712 Color: 0
Size: 277962 Color: 1

Bin 505: 538 of cap free
Amount of items: 2
Items: 
Size: 568955 Color: 0
Size: 430508 Color: 1

Bin 506: 538 of cap free
Amount of items: 2
Items: 
Size: 799376 Color: 1
Size: 200087 Color: 0

Bin 507: 544 of cap free
Amount of items: 3
Items: 
Size: 469841 Color: 0
Size: 334448 Color: 0
Size: 195168 Color: 1

Bin 508: 546 of cap free
Amount of items: 2
Items: 
Size: 641172 Color: 1
Size: 358283 Color: 0

Bin 509: 546 of cap free
Amount of items: 2
Items: 
Size: 791062 Color: 1
Size: 208393 Color: 0

Bin 510: 550 of cap free
Amount of items: 2
Items: 
Size: 613246 Color: 0
Size: 386205 Color: 1

Bin 511: 561 of cap free
Amount of items: 2
Items: 
Size: 519222 Color: 1
Size: 480218 Color: 0

Bin 512: 565 of cap free
Amount of items: 2
Items: 
Size: 690520 Color: 1
Size: 308916 Color: 0

Bin 513: 565 of cap free
Amount of items: 2
Items: 
Size: 799233 Color: 0
Size: 200203 Color: 1

Bin 514: 567 of cap free
Amount of items: 2
Items: 
Size: 597926 Color: 1
Size: 401508 Color: 0

Bin 515: 569 of cap free
Amount of items: 2
Items: 
Size: 578326 Color: 0
Size: 421106 Color: 1

Bin 516: 570 of cap free
Amount of items: 3
Items: 
Size: 591301 Color: 1
Size: 222775 Color: 0
Size: 185355 Color: 1

Bin 517: 574 of cap free
Amount of items: 2
Items: 
Size: 567920 Color: 1
Size: 431507 Color: 0

Bin 518: 577 of cap free
Amount of items: 2
Items: 
Size: 506959 Color: 0
Size: 492465 Color: 1

Bin 519: 578 of cap free
Amount of items: 2
Items: 
Size: 682868 Color: 1
Size: 316555 Color: 0

Bin 520: 581 of cap free
Amount of items: 2
Items: 
Size: 574726 Color: 0
Size: 424694 Color: 1

Bin 521: 587 of cap free
Amount of items: 2
Items: 
Size: 667231 Color: 0
Size: 332183 Color: 1

Bin 522: 589 of cap free
Amount of items: 2
Items: 
Size: 639904 Color: 0
Size: 359508 Color: 1

Bin 523: 593 of cap free
Amount of items: 2
Items: 
Size: 665904 Color: 0
Size: 333504 Color: 1

Bin 524: 593 of cap free
Amount of items: 2
Items: 
Size: 689698 Color: 1
Size: 309710 Color: 0

Bin 525: 595 of cap free
Amount of items: 2
Items: 
Size: 638180 Color: 0
Size: 361226 Color: 1

Bin 526: 597 of cap free
Amount of items: 2
Items: 
Size: 610186 Color: 1
Size: 389218 Color: 0

Bin 527: 602 of cap free
Amount of items: 2
Items: 
Size: 579440 Color: 1
Size: 419959 Color: 0

Bin 528: 607 of cap free
Amount of items: 2
Items: 
Size: 586987 Color: 0
Size: 412407 Color: 1

Bin 529: 612 of cap free
Amount of items: 2
Items: 
Size: 580897 Color: 1
Size: 418492 Color: 0

Bin 530: 614 of cap free
Amount of items: 2
Items: 
Size: 595529 Color: 1
Size: 403858 Color: 0

Bin 531: 621 of cap free
Amount of items: 2
Items: 
Size: 636508 Color: 0
Size: 362872 Color: 1

Bin 532: 621 of cap free
Amount of items: 2
Items: 
Size: 794342 Color: 0
Size: 205038 Color: 1

Bin 533: 622 of cap free
Amount of items: 2
Items: 
Size: 726932 Color: 0
Size: 272447 Color: 1

Bin 534: 624 of cap free
Amount of items: 2
Items: 
Size: 526399 Color: 1
Size: 472978 Color: 0

Bin 535: 628 of cap free
Amount of items: 2
Items: 
Size: 622210 Color: 0
Size: 377163 Color: 1

Bin 536: 630 of cap free
Amount of items: 2
Items: 
Size: 628905 Color: 1
Size: 370466 Color: 0

Bin 537: 640 of cap free
Amount of items: 2
Items: 
Size: 745649 Color: 0
Size: 253712 Color: 1

Bin 538: 640 of cap free
Amount of items: 2
Items: 
Size: 793459 Color: 1
Size: 205902 Color: 0

Bin 539: 645 of cap free
Amount of items: 2
Items: 
Size: 667560 Color: 1
Size: 331796 Color: 0

Bin 540: 649 of cap free
Amount of items: 2
Items: 
Size: 723592 Color: 1
Size: 275760 Color: 0

Bin 541: 658 of cap free
Amount of items: 2
Items: 
Size: 513976 Color: 1
Size: 485367 Color: 0

Bin 542: 666 of cap free
Amount of items: 2
Items: 
Size: 702628 Color: 1
Size: 296707 Color: 0

Bin 543: 670 of cap free
Amount of items: 2
Items: 
Size: 660172 Color: 0
Size: 339159 Color: 1

Bin 544: 674 of cap free
Amount of items: 2
Items: 
Size: 694691 Color: 1
Size: 304636 Color: 0

Bin 545: 676 of cap free
Amount of items: 2
Items: 
Size: 621439 Color: 1
Size: 377886 Color: 0

Bin 546: 679 of cap free
Amount of items: 2
Items: 
Size: 759804 Color: 0
Size: 239518 Color: 1

Bin 547: 684 of cap free
Amount of items: 2
Items: 
Size: 622585 Color: 1
Size: 376732 Color: 0

Bin 548: 690 of cap free
Amount of items: 2
Items: 
Size: 687619 Color: 1
Size: 311692 Color: 0

Bin 549: 691 of cap free
Amount of items: 2
Items: 
Size: 791978 Color: 0
Size: 207332 Color: 1

Bin 550: 693 of cap free
Amount of items: 2
Items: 
Size: 552759 Color: 1
Size: 446549 Color: 0

Bin 551: 696 of cap free
Amount of items: 2
Items: 
Size: 629703 Color: 1
Size: 369602 Color: 0

Bin 552: 697 of cap free
Amount of items: 2
Items: 
Size: 645389 Color: 0
Size: 353915 Color: 1

Bin 553: 699 of cap free
Amount of items: 2
Items: 
Size: 671816 Color: 1
Size: 327486 Color: 0

Bin 554: 701 of cap free
Amount of items: 2
Items: 
Size: 562224 Color: 1
Size: 437076 Color: 0

Bin 555: 703 of cap free
Amount of items: 2
Items: 
Size: 586721 Color: 1
Size: 412577 Color: 0

Bin 556: 705 of cap free
Amount of items: 2
Items: 
Size: 564239 Color: 0
Size: 435057 Color: 1

Bin 557: 711 of cap free
Amount of items: 2
Items: 
Size: 696090 Color: 1
Size: 303200 Color: 0

Bin 558: 715 of cap free
Amount of items: 2
Items: 
Size: 716722 Color: 1
Size: 282564 Color: 0

Bin 559: 732 of cap free
Amount of items: 2
Items: 
Size: 499638 Color: 1
Size: 499631 Color: 0

Bin 560: 738 of cap free
Amount of items: 2
Items: 
Size: 546831 Color: 0
Size: 452432 Color: 1

Bin 561: 742 of cap free
Amount of items: 2
Items: 
Size: 661580 Color: 0
Size: 337679 Color: 1

Bin 562: 749 of cap free
Amount of items: 2
Items: 
Size: 616496 Color: 0
Size: 382756 Color: 1

Bin 563: 753 of cap free
Amount of items: 2
Items: 
Size: 672330 Color: 0
Size: 326918 Color: 1

Bin 564: 754 of cap free
Amount of items: 2
Items: 
Size: 575079 Color: 1
Size: 424168 Color: 0

Bin 565: 759 of cap free
Amount of items: 2
Items: 
Size: 737452 Color: 0
Size: 261790 Color: 1

Bin 566: 760 of cap free
Amount of items: 2
Items: 
Size: 721951 Color: 1
Size: 277290 Color: 0

Bin 567: 762 of cap free
Amount of items: 2
Items: 
Size: 728775 Color: 0
Size: 270464 Color: 1

Bin 568: 770 of cap free
Amount of items: 2
Items: 
Size: 670566 Color: 0
Size: 328665 Color: 1

Bin 569: 777 of cap free
Amount of items: 2
Items: 
Size: 797535 Color: 1
Size: 201689 Color: 0

Bin 570: 784 of cap free
Amount of items: 2
Items: 
Size: 752110 Color: 1
Size: 247107 Color: 0

Bin 571: 786 of cap free
Amount of items: 2
Items: 
Size: 747281 Color: 1
Size: 251934 Color: 0

Bin 572: 789 of cap free
Amount of items: 2
Items: 
Size: 753782 Color: 0
Size: 245430 Color: 1

Bin 573: 792 of cap free
Amount of items: 2
Items: 
Size: 564203 Color: 0
Size: 435006 Color: 1

Bin 574: 793 of cap free
Amount of items: 2
Items: 
Size: 706055 Color: 1
Size: 293153 Color: 0

Bin 575: 800 of cap free
Amount of items: 2
Items: 
Size: 787839 Color: 1
Size: 211362 Color: 0

Bin 576: 803 of cap free
Amount of items: 3
Items: 
Size: 477559 Color: 1
Size: 329176 Color: 0
Size: 192463 Color: 1

Bin 577: 805 of cap free
Amount of items: 2
Items: 
Size: 675599 Color: 0
Size: 323597 Color: 1

Bin 578: 811 of cap free
Amount of items: 2
Items: 
Size: 519050 Color: 1
Size: 480140 Color: 0

Bin 579: 823 of cap free
Amount of items: 2
Items: 
Size: 731731 Color: 1
Size: 267447 Color: 0

Bin 580: 833 of cap free
Amount of items: 2
Items: 
Size: 543849 Color: 1
Size: 455319 Color: 0

Bin 581: 836 of cap free
Amount of items: 2
Items: 
Size: 735267 Color: 1
Size: 263898 Color: 0

Bin 582: 841 of cap free
Amount of items: 2
Items: 
Size: 643427 Color: 0
Size: 355733 Color: 1

Bin 583: 845 of cap free
Amount of items: 2
Items: 
Size: 593488 Color: 0
Size: 405668 Color: 1

Bin 584: 849 of cap free
Amount of items: 2
Items: 
Size: 637711 Color: 1
Size: 361441 Color: 0

Bin 585: 850 of cap free
Amount of items: 2
Items: 
Size: 577412 Color: 1
Size: 421739 Color: 0

Bin 586: 861 of cap free
Amount of items: 2
Items: 
Size: 712546 Color: 0
Size: 286594 Color: 1

Bin 587: 872 of cap free
Amount of items: 2
Items: 
Size: 626441 Color: 0
Size: 372688 Color: 1

Bin 588: 877 of cap free
Amount of items: 2
Items: 
Size: 701185 Color: 1
Size: 297939 Color: 0

Bin 589: 877 of cap free
Amount of items: 2
Items: 
Size: 681602 Color: 0
Size: 317522 Color: 1

Bin 590: 904 of cap free
Amount of items: 2
Items: 
Size: 721880 Color: 1
Size: 277217 Color: 0

Bin 591: 915 of cap free
Amount of items: 2
Items: 
Size: 665898 Color: 1
Size: 333188 Color: 0

Bin 592: 918 of cap free
Amount of items: 2
Items: 
Size: 584152 Color: 0
Size: 414931 Color: 1

Bin 593: 924 of cap free
Amount of items: 2
Items: 
Size: 615586 Color: 1
Size: 383491 Color: 0

Bin 594: 925 of cap free
Amount of items: 2
Items: 
Size: 547882 Color: 1
Size: 451194 Color: 0

Bin 595: 930 of cap free
Amount of items: 2
Items: 
Size: 553920 Color: 1
Size: 445151 Color: 0

Bin 596: 931 of cap free
Amount of items: 2
Items: 
Size: 634509 Color: 0
Size: 364561 Color: 1

Bin 597: 935 of cap free
Amount of items: 2
Items: 
Size: 688160 Color: 0
Size: 310906 Color: 1

Bin 598: 935 of cap free
Amount of items: 2
Items: 
Size: 541690 Color: 1
Size: 457376 Color: 0

Bin 599: 944 of cap free
Amount of items: 2
Items: 
Size: 519022 Color: 1
Size: 480035 Color: 0

Bin 600: 946 of cap free
Amount of items: 2
Items: 
Size: 715015 Color: 0
Size: 284040 Color: 1

Bin 601: 949 of cap free
Amount of items: 2
Items: 
Size: 787858 Color: 0
Size: 211194 Color: 1

Bin 602: 950 of cap free
Amount of items: 2
Items: 
Size: 755836 Color: 1
Size: 243215 Color: 0

Bin 603: 952 of cap free
Amount of items: 2
Items: 
Size: 541316 Color: 0
Size: 457733 Color: 1

Bin 604: 953 of cap free
Amount of items: 2
Items: 
Size: 556332 Color: 0
Size: 442716 Color: 1

Bin 605: 953 of cap free
Amount of items: 2
Items: 
Size: 603013 Color: 0
Size: 396035 Color: 1

Bin 606: 954 of cap free
Amount of items: 2
Items: 
Size: 536957 Color: 1
Size: 462090 Color: 0

Bin 607: 984 of cap free
Amount of items: 2
Items: 
Size: 583215 Color: 1
Size: 415802 Color: 0

Bin 608: 989 of cap free
Amount of items: 2
Items: 
Size: 721871 Color: 1
Size: 277141 Color: 0

Bin 609: 998 of cap free
Amount of items: 2
Items: 
Size: 629219 Color: 0
Size: 369784 Color: 1

Bin 610: 1001 of cap free
Amount of items: 2
Items: 
Size: 794050 Color: 0
Size: 204950 Color: 1

Bin 611: 1001 of cap free
Amount of items: 2
Items: 
Size: 624577 Color: 0
Size: 374423 Color: 1

Bin 612: 1004 of cap free
Amount of items: 2
Items: 
Size: 563161 Color: 1
Size: 435836 Color: 0

Bin 613: 1005 of cap free
Amount of items: 2
Items: 
Size: 669908 Color: 1
Size: 329088 Color: 0

Bin 614: 1007 of cap free
Amount of items: 2
Items: 
Size: 741330 Color: 1
Size: 257664 Color: 0

Bin 615: 1010 of cap free
Amount of items: 2
Items: 
Size: 675058 Color: 1
Size: 323933 Color: 0

Bin 616: 1018 of cap free
Amount of items: 2
Items: 
Size: 627367 Color: 1
Size: 371616 Color: 0

Bin 617: 1023 of cap free
Amount of items: 2
Items: 
Size: 524972 Color: 0
Size: 474006 Color: 1

Bin 618: 1024 of cap free
Amount of items: 2
Items: 
Size: 773818 Color: 1
Size: 225159 Color: 0

Bin 619: 1025 of cap free
Amount of items: 2
Items: 
Size: 692619 Color: 0
Size: 306357 Color: 1

Bin 620: 1031 of cap free
Amount of items: 2
Items: 
Size: 520954 Color: 1
Size: 478016 Color: 0

Bin 621: 1034 of cap free
Amount of items: 2
Items: 
Size: 623303 Color: 0
Size: 375664 Color: 1

Bin 622: 1053 of cap free
Amount of items: 2
Items: 
Size: 589110 Color: 1
Size: 409838 Color: 0

Bin 623: 1054 of cap free
Amount of items: 2
Items: 
Size: 543782 Color: 1
Size: 455165 Color: 0

Bin 624: 1058 of cap free
Amount of items: 2
Items: 
Size: 671459 Color: 1
Size: 327484 Color: 0

Bin 625: 1060 of cap free
Amount of items: 2
Items: 
Size: 616948 Color: 1
Size: 381993 Color: 0

Bin 626: 1061 of cap free
Amount of items: 2
Items: 
Size: 569982 Color: 0
Size: 428958 Color: 1

Bin 627: 1063 of cap free
Amount of items: 2
Items: 
Size: 680882 Color: 1
Size: 318056 Color: 0

Bin 628: 1065 of cap free
Amount of items: 2
Items: 
Size: 612877 Color: 0
Size: 386059 Color: 1

Bin 629: 1085 of cap free
Amount of items: 2
Items: 
Size: 519969 Color: 0
Size: 478947 Color: 1

Bin 630: 1090 of cap free
Amount of items: 2
Items: 
Size: 738253 Color: 1
Size: 260658 Color: 0

Bin 631: 1104 of cap free
Amount of items: 2
Items: 
Size: 601090 Color: 0
Size: 397807 Color: 1

Bin 632: 1106 of cap free
Amount of items: 2
Items: 
Size: 726800 Color: 1
Size: 272095 Color: 0

Bin 633: 1118 of cap free
Amount of items: 2
Items: 
Size: 704296 Color: 1
Size: 294587 Color: 0

Bin 634: 1126 of cap free
Amount of items: 2
Items: 
Size: 689233 Color: 1
Size: 309642 Color: 0

Bin 635: 1129 of cap free
Amount of items: 2
Items: 
Size: 751889 Color: 1
Size: 246983 Color: 0

Bin 636: 1144 of cap free
Amount of items: 2
Items: 
Size: 523071 Color: 1
Size: 475786 Color: 0

Bin 637: 1155 of cap free
Amount of items: 2
Items: 
Size: 574567 Color: 0
Size: 424279 Color: 1

Bin 638: 1167 of cap free
Amount of items: 2
Items: 
Size: 774303 Color: 0
Size: 224531 Color: 1

Bin 639: 1177 of cap free
Amount of items: 2
Items: 
Size: 692549 Color: 0
Size: 306275 Color: 1

Bin 640: 1183 of cap free
Amount of items: 2
Items: 
Size: 687277 Color: 1
Size: 311541 Color: 0

Bin 641: 1185 of cap free
Amount of items: 2
Items: 
Size: 573268 Color: 1
Size: 425548 Color: 0

Bin 642: 1192 of cap free
Amount of items: 2
Items: 
Size: 542796 Color: 0
Size: 456013 Color: 1

Bin 643: 1196 of cap free
Amount of items: 2
Items: 
Size: 512892 Color: 0
Size: 485913 Color: 1

Bin 644: 1197 of cap free
Amount of items: 2
Items: 
Size: 740349 Color: 0
Size: 258455 Color: 1

Bin 645: 1202 of cap free
Amount of items: 2
Items: 
Size: 724004 Color: 0
Size: 274795 Color: 1

Bin 646: 1233 of cap free
Amount of items: 2
Items: 
Size: 682438 Color: 1
Size: 316330 Color: 0

Bin 647: 1235 of cap free
Amount of items: 2
Items: 
Size: 507019 Color: 1
Size: 491747 Color: 0

Bin 648: 1240 of cap free
Amount of items: 2
Items: 
Size: 622239 Color: 1
Size: 376522 Color: 0

Bin 649: 1245 of cap free
Amount of items: 2
Items: 
Size: 583086 Color: 1
Size: 415670 Color: 0

Bin 650: 1245 of cap free
Amount of items: 2
Items: 
Size: 770179 Color: 0
Size: 228577 Color: 1

Bin 651: 1258 of cap free
Amount of items: 2
Items: 
Size: 503962 Color: 0
Size: 494781 Color: 1

Bin 652: 1268 of cap free
Amount of items: 2
Items: 
Size: 716798 Color: 0
Size: 281935 Color: 1

Bin 653: 1274 of cap free
Amount of items: 2
Items: 
Size: 690198 Color: 0
Size: 308529 Color: 1

Bin 654: 1282 of cap free
Amount of items: 2
Items: 
Size: 553613 Color: 1
Size: 445106 Color: 0

Bin 655: 1287 of cap free
Amount of items: 2
Items: 
Size: 729489 Color: 1
Size: 269225 Color: 0

Bin 656: 1292 of cap free
Amount of items: 2
Items: 
Size: 532693 Color: 0
Size: 466016 Color: 1

Bin 657: 1294 of cap free
Amount of items: 2
Items: 
Size: 502511 Color: 1
Size: 496196 Color: 0

Bin 658: 1308 of cap free
Amount of items: 2
Items: 
Size: 560813 Color: 1
Size: 437880 Color: 0

Bin 659: 1308 of cap free
Amount of items: 2
Items: 
Size: 781795 Color: 1
Size: 216898 Color: 0

Bin 660: 1318 of cap free
Amount of items: 2
Items: 
Size: 500700 Color: 1
Size: 497983 Color: 0

Bin 661: 1324 of cap free
Amount of items: 2
Items: 
Size: 508772 Color: 1
Size: 489905 Color: 0

Bin 662: 1324 of cap free
Amount of items: 2
Items: 
Size: 547660 Color: 1
Size: 451017 Color: 0

Bin 663: 1335 of cap free
Amount of items: 2
Items: 
Size: 757679 Color: 1
Size: 240987 Color: 0

Bin 664: 1336 of cap free
Amount of items: 2
Items: 
Size: 578169 Color: 0
Size: 420496 Color: 1

Bin 665: 1338 of cap free
Amount of items: 2
Items: 
Size: 603963 Color: 1
Size: 394700 Color: 0

Bin 666: 1344 of cap free
Amount of items: 2
Items: 
Size: 531301 Color: 0
Size: 467356 Color: 1

Bin 667: 1350 of cap free
Amount of items: 2
Items: 
Size: 587732 Color: 0
Size: 410919 Color: 1

Bin 668: 1350 of cap free
Amount of items: 2
Items: 
Size: 738955 Color: 0
Size: 259696 Color: 1

Bin 669: 1363 of cap free
Amount of items: 2
Items: 
Size: 666985 Color: 0
Size: 331653 Color: 1

Bin 670: 1365 of cap free
Amount of items: 2
Items: 
Size: 601240 Color: 1
Size: 397396 Color: 0

Bin 671: 1377 of cap free
Amount of items: 2
Items: 
Size: 617284 Color: 0
Size: 381340 Color: 1

Bin 672: 1409 of cap free
Amount of items: 2
Items: 
Size: 684710 Color: 0
Size: 313882 Color: 1

Bin 673: 1413 of cap free
Amount of items: 3
Items: 
Size: 381656 Color: 0
Size: 366694 Color: 0
Size: 250238 Color: 1

Bin 674: 1419 of cap free
Amount of items: 2
Items: 
Size: 651593 Color: 0
Size: 346989 Color: 1

Bin 675: 1428 of cap free
Amount of items: 3
Items: 
Size: 529675 Color: 0
Size: 313978 Color: 0
Size: 154920 Color: 1

Bin 676: 1439 of cap free
Amount of items: 2
Items: 
Size: 571063 Color: 1
Size: 427499 Color: 0

Bin 677: 1448 of cap free
Amount of items: 2
Items: 
Size: 644780 Color: 0
Size: 353773 Color: 1

Bin 678: 1449 of cap free
Amount of items: 2
Items: 
Size: 560437 Color: 0
Size: 438115 Color: 1

Bin 679: 1507 of cap free
Amount of items: 2
Items: 
Size: 624382 Color: 0
Size: 374112 Color: 1

Bin 680: 1564 of cap free
Amount of items: 2
Items: 
Size: 708825 Color: 0
Size: 289612 Color: 1

Bin 681: 1576 of cap free
Amount of items: 2
Items: 
Size: 519850 Color: 0
Size: 478575 Color: 1

Bin 682: 1581 of cap free
Amount of items: 2
Items: 
Size: 797034 Color: 1
Size: 201386 Color: 0

Bin 683: 1583 of cap free
Amount of items: 2
Items: 
Size: 534372 Color: 1
Size: 464046 Color: 0

Bin 684: 1591 of cap free
Amount of items: 2
Items: 
Size: 673380 Color: 0
Size: 325030 Color: 1

Bin 685: 1610 of cap free
Amount of items: 2
Items: 
Size: 536536 Color: 1
Size: 461855 Color: 0

Bin 686: 1612 of cap free
Amount of items: 2
Items: 
Size: 508915 Color: 0
Size: 489474 Color: 1

Bin 687: 1640 of cap free
Amount of items: 2
Items: 
Size: 636922 Color: 1
Size: 361439 Color: 0

Bin 688: 1651 of cap free
Amount of items: 2
Items: 
Size: 684550 Color: 1
Size: 313800 Color: 0

Bin 689: 1659 of cap free
Amount of items: 2
Items: 
Size: 565331 Color: 1
Size: 433011 Color: 0

Bin 690: 1660 of cap free
Amount of items: 2
Items: 
Size: 604628 Color: 0
Size: 393713 Color: 1

Bin 691: 1678 of cap free
Amount of items: 2
Items: 
Size: 639671 Color: 0
Size: 358652 Color: 1

Bin 692: 1682 of cap free
Amount of items: 2
Items: 
Size: 523006 Color: 1
Size: 475313 Color: 0

Bin 693: 1684 of cap free
Amount of items: 2
Items: 
Size: 611476 Color: 1
Size: 386841 Color: 0

Bin 694: 1702 of cap free
Amount of items: 2
Items: 
Size: 628543 Color: 0
Size: 369756 Color: 1

Bin 695: 1703 of cap free
Amount of items: 2
Items: 
Size: 580103 Color: 1
Size: 418195 Color: 0

Bin 696: 1710 of cap free
Amount of items: 2
Items: 
Size: 660874 Color: 0
Size: 337417 Color: 1

Bin 697: 1717 of cap free
Amount of items: 2
Items: 
Size: 728221 Color: 0
Size: 270063 Color: 1

Bin 698: 1740 of cap free
Amount of items: 2
Items: 
Size: 582709 Color: 1
Size: 415552 Color: 0

Bin 699: 1755 of cap free
Amount of items: 2
Items: 
Size: 773757 Color: 0
Size: 224489 Color: 1

Bin 700: 1773 of cap free
Amount of items: 3
Items: 
Size: 641353 Color: 0
Size: 187904 Color: 1
Size: 168971 Color: 1

Bin 701: 1780 of cap free
Amount of items: 2
Items: 
Size: 684539 Color: 1
Size: 313682 Color: 0

Bin 702: 1786 of cap free
Amount of items: 2
Items: 
Size: 778947 Color: 0
Size: 219268 Color: 1

Bin 703: 1830 of cap free
Amount of items: 2
Items: 
Size: 677616 Color: 0
Size: 320555 Color: 1

Bin 704: 1839 of cap free
Amount of items: 2
Items: 
Size: 565018 Color: 0
Size: 433144 Color: 1

Bin 705: 1839 of cap free
Amount of items: 2
Items: 
Size: 543477 Color: 1
Size: 454685 Color: 0

Bin 706: 1843 of cap free
Amount of items: 2
Items: 
Size: 783756 Color: 0
Size: 214402 Color: 1

Bin 707: 1894 of cap free
Amount of items: 2
Items: 
Size: 651189 Color: 0
Size: 346918 Color: 1

Bin 708: 1902 of cap free
Amount of items: 2
Items: 
Size: 547329 Color: 1
Size: 450770 Color: 0

Bin 709: 1933 of cap free
Amount of items: 2
Items: 
Size: 636175 Color: 0
Size: 361893 Color: 1

Bin 710: 1945 of cap free
Amount of items: 2
Items: 
Size: 536474 Color: 1
Size: 461582 Color: 0

Bin 711: 1957 of cap free
Amount of items: 2
Items: 
Size: 783141 Color: 1
Size: 214903 Color: 0

Bin 712: 1972 of cap free
Amount of items: 2
Items: 
Size: 720903 Color: 1
Size: 277126 Color: 0

Bin 713: 1977 of cap free
Amount of items: 2
Items: 
Size: 723079 Color: 1
Size: 274945 Color: 0

Bin 714: 1997 of cap free
Amount of items: 2
Items: 
Size: 767110 Color: 0
Size: 230894 Color: 1

Bin 715: 2002 of cap free
Amount of items: 2
Items: 
Size: 513342 Color: 1
Size: 484657 Color: 0

Bin 716: 2007 of cap free
Amount of items: 2
Items: 
Size: 608989 Color: 0
Size: 389005 Color: 1

Bin 717: 2013 of cap free
Amount of items: 2
Items: 
Size: 786898 Color: 0
Size: 211090 Color: 1

Bin 718: 2024 of cap free
Amount of items: 2
Items: 
Size: 499623 Color: 0
Size: 498354 Color: 1

Bin 719: 2025 of cap free
Amount of items: 2
Items: 
Size: 516774 Color: 0
Size: 481202 Color: 1

Bin 720: 2051 of cap free
Amount of items: 2
Items: 
Size: 588874 Color: 1
Size: 409076 Color: 0

Bin 721: 2073 of cap free
Amount of items: 2
Items: 
Size: 684260 Color: 0
Size: 313668 Color: 1

Bin 722: 2084 of cap free
Amount of items: 2
Items: 
Size: 711518 Color: 1
Size: 286399 Color: 0

Bin 723: 2119 of cap free
Amount of items: 2
Items: 
Size: 600703 Color: 0
Size: 397179 Color: 1

Bin 724: 2132 of cap free
Amount of items: 2
Items: 
Size: 600642 Color: 1
Size: 397227 Color: 0

Bin 725: 2136 of cap free
Amount of items: 2
Items: 
Size: 711951 Color: 0
Size: 285914 Color: 1

Bin 726: 2139 of cap free
Amount of items: 2
Items: 
Size: 651179 Color: 0
Size: 346683 Color: 1

Bin 727: 2154 of cap free
Amount of items: 2
Items: 
Size: 798222 Color: 0
Size: 199625 Color: 1

Bin 728: 2212 of cap free
Amount of items: 2
Items: 
Size: 720841 Color: 1
Size: 276948 Color: 0

Bin 729: 2350 of cap free
Amount of items: 2
Items: 
Size: 595323 Color: 0
Size: 402328 Color: 1

Bin 730: 2356 of cap free
Amount of items: 2
Items: 
Size: 689071 Color: 1
Size: 308574 Color: 0

Bin 731: 2377 of cap free
Amount of items: 2
Items: 
Size: 612680 Color: 0
Size: 384944 Color: 1

Bin 732: 2406 of cap free
Amount of items: 2
Items: 
Size: 573480 Color: 0
Size: 424115 Color: 1

Bin 733: 2423 of cap free
Amount of items: 2
Items: 
Size: 677710 Color: 1
Size: 319868 Color: 0

Bin 734: 2449 of cap free
Amount of items: 2
Items: 
Size: 711479 Color: 1
Size: 286073 Color: 0

Bin 735: 2495 of cap free
Amount of items: 2
Items: 
Size: 644448 Color: 0
Size: 353058 Color: 1

Bin 736: 2499 of cap free
Amount of items: 2
Items: 
Size: 547098 Color: 1
Size: 450404 Color: 0

Bin 737: 2509 of cap free
Amount of items: 2
Items: 
Size: 610866 Color: 1
Size: 386626 Color: 0

Bin 738: 2532 of cap free
Amount of items: 2
Items: 
Size: 684245 Color: 0
Size: 313224 Color: 1

Bin 739: 2547 of cap free
Amount of items: 2
Items: 
Size: 505752 Color: 1
Size: 491702 Color: 0

Bin 740: 2579 of cap free
Amount of items: 2
Items: 
Size: 627730 Color: 0
Size: 369692 Color: 1

Bin 741: 2586 of cap free
Amount of items: 2
Items: 
Size: 579304 Color: 1
Size: 418111 Color: 0

Bin 742: 2587 of cap free
Amount of items: 2
Items: 
Size: 603831 Color: 1
Size: 393583 Color: 0

Bin 743: 2653 of cap free
Amount of items: 2
Items: 
Size: 708429 Color: 0
Size: 288919 Color: 1

Bin 744: 2654 of cap free
Amount of items: 2
Items: 
Size: 547759 Color: 0
Size: 449588 Color: 1

Bin 745: 2665 of cap free
Amount of items: 2
Items: 
Size: 778210 Color: 0
Size: 219126 Color: 1

Bin 746: 2688 of cap free
Amount of items: 2
Items: 
Size: 600322 Color: 1
Size: 396991 Color: 0

Bin 747: 2689 of cap free
Amount of items: 2
Items: 
Size: 621084 Color: 1
Size: 376228 Color: 0

Bin 748: 2787 of cap free
Amount of items: 2
Items: 
Size: 789659 Color: 1
Size: 207555 Color: 0

Bin 749: 2826 of cap free
Amount of items: 2
Items: 
Size: 684093 Color: 0
Size: 313082 Color: 1

Bin 750: 2860 of cap free
Amount of items: 2
Items: 
Size: 519616 Color: 0
Size: 477525 Color: 1

Bin 751: 2868 of cap free
Amount of items: 2
Items: 
Size: 778195 Color: 0
Size: 218938 Color: 1

Bin 752: 2916 of cap free
Amount of items: 2
Items: 
Size: 734219 Color: 1
Size: 262866 Color: 0

Bin 753: 2934 of cap free
Amount of items: 2
Items: 
Size: 711193 Color: 0
Size: 285874 Color: 1

Bin 754: 2939 of cap free
Amount of items: 2
Items: 
Size: 636033 Color: 0
Size: 361029 Color: 1

Bin 755: 3012 of cap free
Amount of items: 2
Items: 
Size: 659570 Color: 1
Size: 337419 Color: 0

Bin 756: 3029 of cap free
Amount of items: 2
Items: 
Size: 555499 Color: 0
Size: 441473 Color: 1

Bin 757: 3044 of cap free
Amount of items: 2
Items: 
Size: 595005 Color: 0
Size: 401952 Color: 1

Bin 758: 3046 of cap free
Amount of items: 2
Items: 
Size: 559912 Color: 1
Size: 437043 Color: 0

Bin 759: 3048 of cap free
Amount of items: 2
Items: 
Size: 707333 Color: 1
Size: 289620 Color: 0

Bin 760: 3084 of cap free
Amount of items: 2
Items: 
Size: 692255 Color: 0
Size: 304662 Color: 1

Bin 761: 3086 of cap free
Amount of items: 2
Items: 
Size: 536292 Color: 1
Size: 460623 Color: 0

Bin 762: 3089 of cap free
Amount of items: 2
Items: 
Size: 697443 Color: 0
Size: 299469 Color: 1

Bin 763: 3140 of cap free
Amount of items: 2
Items: 
Size: 594984 Color: 0
Size: 401877 Color: 1

Bin 764: 3236 of cap free
Amount of items: 2
Items: 
Size: 677205 Color: 1
Size: 319560 Color: 0

Bin 765: 3252 of cap free
Amount of items: 2
Items: 
Size: 738874 Color: 0
Size: 257875 Color: 1

Bin 766: 3357 of cap free
Amount of items: 2
Items: 
Size: 515490 Color: 0
Size: 481154 Color: 1

Bin 767: 3409 of cap free
Amount of items: 2
Items: 
Size: 563555 Color: 0
Size: 433037 Color: 1

Bin 768: 3463 of cap free
Amount of items: 2
Items: 
Size: 676840 Color: 0
Size: 319698 Color: 1

Bin 769: 3621 of cap free
Amount of items: 2
Items: 
Size: 799919 Color: 1
Size: 196461 Color: 0

Bin 770: 3631 of cap free
Amount of items: 2
Items: 
Size: 791609 Color: 0
Size: 204761 Color: 1

Bin 771: 3690 of cap free
Amount of items: 2
Items: 
Size: 665596 Color: 1
Size: 330715 Color: 0

Bin 772: 3708 of cap free
Amount of items: 2
Items: 
Size: 555081 Color: 0
Size: 441212 Color: 1

Bin 773: 3717 of cap free
Amount of items: 2
Items: 
Size: 588251 Color: 1
Size: 408033 Color: 0

Bin 774: 3722 of cap free
Amount of items: 2
Items: 
Size: 688808 Color: 1
Size: 307471 Color: 0

Bin 775: 3742 of cap free
Amount of items: 2
Items: 
Size: 516783 Color: 1
Size: 479476 Color: 0

Bin 776: 3786 of cap free
Amount of items: 2
Items: 
Size: 515184 Color: 0
Size: 481031 Color: 1

Bin 777: 3803 of cap free
Amount of items: 2
Items: 
Size: 676569 Color: 0
Size: 319629 Color: 1

Bin 778: 3866 of cap free
Amount of items: 2
Items: 
Size: 545757 Color: 1
Size: 450378 Color: 0

Bin 779: 3891 of cap free
Amount of items: 2
Items: 
Size: 747250 Color: 1
Size: 248860 Color: 0

Bin 780: 3936 of cap free
Amount of items: 2
Items: 
Size: 788824 Color: 1
Size: 207241 Color: 0

Bin 781: 3951 of cap free
Amount of items: 2
Items: 
Size: 683251 Color: 0
Size: 312799 Color: 1

Bin 782: 3961 of cap free
Amount of items: 2
Items: 
Size: 573448 Color: 0
Size: 422592 Color: 1

Bin 783: 4035 of cap free
Amount of items: 2
Items: 
Size: 614462 Color: 1
Size: 381504 Color: 0

Bin 784: 4068 of cap free
Amount of items: 2
Items: 
Size: 796928 Color: 0
Size: 199005 Color: 1

Bin 785: 4141 of cap free
Amount of items: 2
Items: 
Size: 718164 Color: 0
Size: 277696 Color: 1

Bin 786: 4307 of cap free
Amount of items: 2
Items: 
Size: 644821 Color: 1
Size: 350873 Color: 0

Bin 787: 4317 of cap free
Amount of items: 2
Items: 
Size: 595266 Color: 1
Size: 400418 Color: 0

Bin 788: 4384 of cap free
Amount of items: 2
Items: 
Size: 559873 Color: 1
Size: 435744 Color: 0

Bin 789: 4388 of cap free
Amount of items: 2
Items: 
Size: 609757 Color: 1
Size: 385856 Color: 0

Bin 790: 4444 of cap free
Amount of items: 2
Items: 
Size: 753281 Color: 0
Size: 242276 Color: 1

Bin 791: 4475 of cap free
Amount of items: 2
Items: 
Size: 626114 Color: 0
Size: 369412 Color: 1

Bin 792: 4594 of cap free
Amount of items: 2
Items: 
Size: 777747 Color: 0
Size: 217660 Color: 1

Bin 793: 4597 of cap free
Amount of items: 2
Items: 
Size: 720643 Color: 1
Size: 274761 Color: 0

Bin 794: 4819 of cap free
Amount of items: 2
Items: 
Size: 514621 Color: 0
Size: 480561 Color: 1

Bin 795: 4953 of cap free
Amount of items: 2
Items: 
Size: 540264 Color: 0
Size: 454784 Color: 1

Bin 796: 5104 of cap free
Amount of items: 2
Items: 
Size: 652380 Color: 1
Size: 342517 Color: 0

Bin 797: 5141 of cap free
Amount of items: 2
Items: 
Size: 775645 Color: 1
Size: 219215 Color: 0

Bin 798: 5264 of cap free
Amount of items: 2
Items: 
Size: 562470 Color: 0
Size: 432267 Color: 1

Bin 799: 5293 of cap free
Amount of items: 2
Items: 
Size: 606316 Color: 0
Size: 388392 Color: 1

Bin 800: 5378 of cap free
Amount of items: 2
Items: 
Size: 554568 Color: 0
Size: 440055 Color: 1

Bin 801: 5455 of cap free
Amount of items: 2
Items: 
Size: 615070 Color: 0
Size: 379476 Color: 1

Bin 802: 5464 of cap free
Amount of items: 2
Items: 
Size: 554537 Color: 0
Size: 440000 Color: 1

Bin 803: 5496 of cap free
Amount of items: 2
Items: 
Size: 594380 Color: 1
Size: 400125 Color: 0

Bin 804: 5506 of cap free
Amount of items: 2
Items: 
Size: 506677 Color: 0
Size: 487818 Color: 1

Bin 805: 5512 of cap free
Amount of items: 2
Items: 
Size: 752407 Color: 0
Size: 242082 Color: 1

Bin 806: 5553 of cap free
Amount of items: 2
Items: 
Size: 737034 Color: 0
Size: 257414 Color: 1

Bin 807: 5598 of cap free
Amount of items: 2
Items: 
Size: 744751 Color: 0
Size: 249652 Color: 1

Bin 808: 5646 of cap free
Amount of items: 2
Items: 
Size: 664006 Color: 0
Size: 330349 Color: 1

Bin 809: 5755 of cap free
Amount of items: 2
Items: 
Size: 689660 Color: 0
Size: 304586 Color: 1

Bin 810: 5905 of cap free
Amount of items: 2
Items: 
Size: 633297 Color: 0
Size: 360799 Color: 1

Bin 811: 6041 of cap free
Amount of items: 2
Items: 
Size: 528427 Color: 0
Size: 465533 Color: 1

Bin 812: 6187 of cap free
Amount of items: 2
Items: 
Size: 687127 Color: 1
Size: 306687 Color: 0

Bin 813: 6231 of cap free
Amount of items: 2
Items: 
Size: 516092 Color: 1
Size: 477678 Color: 0

Bin 814: 6297 of cap free
Amount of items: 2
Items: 
Size: 534030 Color: 1
Size: 459674 Color: 0

Bin 815: 6418 of cap free
Amount of items: 2
Items: 
Size: 593775 Color: 1
Size: 399808 Color: 0

Bin 816: 6546 of cap free
Amount of items: 2
Items: 
Size: 686940 Color: 1
Size: 306515 Color: 0

Bin 817: 6608 of cap free
Amount of items: 2
Items: 
Size: 528213 Color: 0
Size: 465180 Color: 1

Bin 818: 6642 of cap free
Amount of items: 2
Items: 
Size: 558252 Color: 1
Size: 435107 Color: 0

Bin 819: 6817 of cap free
Amount of items: 2
Items: 
Size: 533785 Color: 1
Size: 459399 Color: 0

Bin 820: 6819 of cap free
Amount of items: 2
Items: 
Size: 572689 Color: 0
Size: 420493 Color: 1

Bin 821: 6950 of cap free
Amount of items: 2
Items: 
Size: 736266 Color: 0
Size: 256785 Color: 1

Bin 822: 6952 of cap free
Amount of items: 2
Items: 
Size: 632548 Color: 0
Size: 360501 Color: 1

Bin 823: 6986 of cap free
Amount of items: 2
Items: 
Size: 632536 Color: 0
Size: 360479 Color: 1

Bin 824: 7102 of cap free
Amount of items: 2
Items: 
Size: 689197 Color: 0
Size: 303702 Color: 1

Bin 825: 7204 of cap free
Amount of items: 2
Items: 
Size: 543242 Color: 1
Size: 449555 Color: 0

Bin 826: 7547 of cap free
Amount of items: 2
Items: 
Size: 736145 Color: 0
Size: 256309 Color: 1

Bin 827: 7593 of cap free
Amount of items: 2
Items: 
Size: 543177 Color: 1
Size: 449231 Color: 0

Bin 828: 7598 of cap free
Amount of items: 2
Items: 
Size: 798776 Color: 1
Size: 193627 Color: 0

Bin 829: 7737 of cap free
Amount of items: 2
Items: 
Size: 527196 Color: 0
Size: 465068 Color: 1

Bin 830: 7884 of cap free
Amount of items: 2
Items: 
Size: 708290 Color: 0
Size: 283827 Color: 1

Bin 831: 8114 of cap free
Amount of items: 2
Items: 
Size: 711394 Color: 1
Size: 280493 Color: 0

Bin 832: 8214 of cap free
Amount of items: 2
Items: 
Size: 746933 Color: 1
Size: 244854 Color: 0

Bin 833: 8282 of cap free
Amount of items: 2
Items: 
Size: 786795 Color: 1
Size: 204924 Color: 0

Bin 834: 8333 of cap free
Amount of items: 2
Items: 
Size: 663245 Color: 0
Size: 328423 Color: 1

Bin 835: 8827 of cap free
Amount of items: 2
Items: 
Size: 786718 Color: 1
Size: 204456 Color: 0

Bin 836: 8855 of cap free
Amount of items: 2
Items: 
Size: 505418 Color: 0
Size: 485728 Color: 1

Bin 837: 8908 of cap free
Amount of items: 2
Items: 
Size: 662978 Color: 0
Size: 328115 Color: 1

Bin 838: 9126 of cap free
Amount of items: 2
Items: 
Size: 687892 Color: 0
Size: 302983 Color: 1

Bin 839: 9180 of cap free
Amount of items: 2
Items: 
Size: 786588 Color: 1
Size: 204233 Color: 0

Bin 840: 9260 of cap free
Amount of items: 2
Items: 
Size: 542759 Color: 1
Size: 447982 Color: 0

Bin 841: 9324 of cap free
Amount of items: 2
Items: 
Size: 609603 Color: 1
Size: 381074 Color: 0

Bin 842: 9547 of cap free
Amount of items: 2
Items: 
Size: 552422 Color: 0
Size: 438032 Color: 1

Bin 843: 9553 of cap free
Amount of items: 2
Items: 
Size: 515375 Color: 1
Size: 475073 Color: 0

Bin 844: 9638 of cap free
Amount of items: 2
Items: 
Size: 531567 Color: 1
Size: 458796 Color: 0

Bin 845: 9693 of cap free
Amount of items: 2
Items: 
Size: 552312 Color: 0
Size: 437996 Color: 1

Bin 846: 9895 of cap free
Amount of items: 2
Items: 
Size: 593652 Color: 1
Size: 396454 Color: 0

Bin 847: 9910 of cap free
Amount of items: 2
Items: 
Size: 609385 Color: 1
Size: 380706 Color: 0

Bin 848: 10060 of cap free
Amount of items: 2
Items: 
Size: 684149 Color: 1
Size: 305792 Color: 0

Bin 849: 10795 of cap free
Amount of items: 2
Items: 
Size: 608586 Color: 1
Size: 380620 Color: 0

Bin 850: 10826 of cap free
Amount of items: 2
Items: 
Size: 497577 Color: 1
Size: 491598 Color: 0

Bin 851: 10888 of cap free
Amount of items: 2
Items: 
Size: 541452 Color: 1
Size: 447661 Color: 0

Bin 852: 11116 of cap free
Amount of items: 2
Items: 
Size: 524601 Color: 0
Size: 464284 Color: 1

Bin 853: 11546 of cap free
Amount of items: 2
Items: 
Size: 796512 Color: 1
Size: 191943 Color: 0

Bin 854: 11696 of cap free
Amount of items: 2
Items: 
Size: 600645 Color: 0
Size: 387660 Color: 1

Bin 855: 11759 of cap free
Amount of items: 2
Items: 
Size: 496700 Color: 1
Size: 491542 Color: 0

Bin 856: 11897 of cap free
Amount of items: 2
Items: 
Size: 607571 Color: 1
Size: 380533 Color: 0

Bin 857: 11972 of cap free
Amount of items: 2
Items: 
Size: 607517 Color: 1
Size: 380512 Color: 0

Bin 858: 12001 of cap free
Amount of items: 2
Items: 
Size: 631193 Color: 0
Size: 356807 Color: 1

Bin 859: 12026 of cap free
Amount of items: 2
Items: 
Size: 557438 Color: 1
Size: 430537 Color: 0

Bin 860: 12202 of cap free
Amount of items: 2
Items: 
Size: 557363 Color: 1
Size: 430436 Color: 0

Bin 861: 12556 of cap free
Amount of items: 2
Items: 
Size: 523360 Color: 0
Size: 464085 Color: 1

Bin 862: 13140 of cap free
Amount of items: 2
Items: 
Size: 684020 Color: 1
Size: 302841 Color: 0

Bin 863: 14129 of cap free
Amount of items: 2
Items: 
Size: 522754 Color: 0
Size: 463118 Color: 1

Bin 864: 14535 of cap free
Amount of items: 2
Items: 
Size: 496128 Color: 1
Size: 489338 Color: 0

Bin 865: 15071 of cap free
Amount of items: 2
Items: 
Size: 496065 Color: 1
Size: 488865 Color: 0

Bin 866: 15085 of cap free
Amount of items: 2
Items: 
Size: 658575 Color: 1
Size: 326341 Color: 0

Bin 867: 17084 of cap free
Amount of items: 2
Items: 
Size: 551215 Color: 0
Size: 431702 Color: 1

Bin 868: 17106 of cap free
Amount of items: 2
Items: 
Size: 606930 Color: 1
Size: 375965 Color: 0

Bin 869: 17117 of cap free
Amount of items: 2
Items: 
Size: 572653 Color: 0
Size: 410231 Color: 1

Bin 870: 17317 of cap free
Amount of items: 2
Items: 
Size: 494619 Color: 1
Size: 488065 Color: 0

Bin 871: 17945 of cap free
Amount of items: 2
Items: 
Size: 606566 Color: 1
Size: 375490 Color: 0

Bin 872: 18344 of cap free
Amount of items: 2
Items: 
Size: 680716 Color: 1
Size: 300941 Color: 0

Bin 873: 20343 of cap free
Amount of items: 2
Items: 
Size: 662870 Color: 0
Size: 316788 Color: 1

Bin 874: 23503 of cap free
Amount of items: 2
Items: 
Size: 531495 Color: 1
Size: 445003 Color: 0

Bin 875: 24035 of cap free
Amount of items: 2
Items: 
Size: 531417 Color: 1
Size: 444549 Color: 0

Bin 876: 25116 of cap free
Amount of items: 2
Items: 
Size: 785954 Color: 1
Size: 188931 Color: 0

Bin 877: 25640 of cap free
Amount of items: 2
Items: 
Size: 546791 Color: 0
Size: 427570 Color: 1

Bin 878: 26300 of cap free
Amount of items: 2
Items: 
Size: 530805 Color: 1
Size: 442896 Color: 0

Bin 879: 39282 of cap free
Amount of items: 2
Items: 
Size: 530782 Color: 1
Size: 429937 Color: 0

Bin 880: 41294 of cap free
Amount of items: 2
Items: 
Size: 584463 Color: 1
Size: 374244 Color: 0

Bin 881: 46332 of cap free
Amount of items: 2
Items: 
Size: 772785 Color: 0
Size: 180884 Color: 1

Bin 882: 47249 of cap free
Amount of items: 2
Items: 
Size: 599955 Color: 0
Size: 352797 Color: 1

Bin 883: 47721 of cap free
Amount of items: 2
Items: 
Size: 651872 Color: 1
Size: 300408 Color: 0

Bin 884: 48624 of cap free
Amount of items: 2
Items: 
Size: 577172 Color: 1
Size: 374205 Color: 0

Bin 885: 49384 of cap free
Amount of items: 2
Items: 
Size: 576693 Color: 1
Size: 373924 Color: 0

Bin 886: 52032 of cap free
Amount of items: 2
Items: 
Size: 574658 Color: 1
Size: 373311 Color: 0

Bin 887: 57819 of cap free
Amount of items: 2
Items: 
Size: 769390 Color: 1
Size: 172792 Color: 0

Bin 888: 58883 of cap free
Amount of items: 2
Items: 
Size: 574541 Color: 1
Size: 366577 Color: 0

Bin 889: 59558 of cap free
Amount of items: 2
Items: 
Size: 766330 Color: 0
Size: 174113 Color: 1

Bin 890: 64288 of cap free
Amount of items: 2
Items: 
Size: 570978 Color: 1
Size: 364735 Color: 0

Bin 891: 84916 of cap free
Amount of items: 2
Items: 
Size: 485654 Color: 1
Size: 429431 Color: 0

Bin 892: 230619 of cap free
Amount of items: 1
Items: 
Size: 769382 Color: 1

Bin 893: 231204 of cap free
Amount of items: 1
Items: 
Size: 768797 Color: 1

Bin 894: 233594 of cap free
Amount of items: 1
Items: 
Size: 766407 Color: 1

Bin 895: 234766 of cap free
Amount of items: 1
Items: 
Size: 765235 Color: 1

Bin 896: 235403 of cap free
Amount of items: 1
Items: 
Size: 764598 Color: 0

Bin 897: 236175 of cap free
Amount of items: 1
Items: 
Size: 763826 Color: 1

Bin 898: 237932 of cap free
Amount of items: 1
Items: 
Size: 762069 Color: 1

Bin 899: 238973 of cap free
Amount of items: 1
Items: 
Size: 761028 Color: 0

Bin 900: 240113 of cap free
Amount of items: 1
Items: 
Size: 759888 Color: 1

Bin 901: 243030 of cap free
Amount of items: 1
Items: 
Size: 756971 Color: 1

Bin 902: 248642 of cap free
Amount of items: 1
Items: 
Size: 751359 Color: 0

Bin 903: 254179 of cap free
Amount of items: 1
Items: 
Size: 745822 Color: 1

Bin 904: 257293 of cap free
Amount of items: 1
Items: 
Size: 742708 Color: 1

Bin 905: 266465 of cap free
Amount of items: 1
Items: 
Size: 733536 Color: 1

Bin 906: 266972 of cap free
Amount of items: 1
Items: 
Size: 733029 Color: 1

Bin 907: 273876 of cap free
Amount of items: 1
Items: 
Size: 726125 Color: 0

Bin 908: 274667 of cap free
Amount of items: 1
Items: 
Size: 725334 Color: 0

Bin 909: 292920 of cap free
Amount of items: 1
Items: 
Size: 707081 Color: 1

Total size: 902398336
Total free space: 6602573

