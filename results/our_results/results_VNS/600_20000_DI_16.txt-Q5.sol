Capicity Bin: 16336
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9698 Color: 2
Size: 6354 Color: 4
Size: 284 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9720 Color: 0
Size: 5832 Color: 3
Size: 784 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10808 Color: 4
Size: 5240 Color: 1
Size: 288 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11128 Color: 4
Size: 4680 Color: 1
Size: 528 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 0
Size: 4616 Color: 1
Size: 508 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11978 Color: 1
Size: 4008 Color: 3
Size: 350 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12097 Color: 1
Size: 2711 Color: 3
Size: 1528 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 3
Size: 4008 Color: 1
Size: 288 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12049 Color: 4
Size: 3957 Color: 0
Size: 330 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 3
Size: 2564 Color: 3
Size: 1352 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12468 Color: 1
Size: 3592 Color: 2
Size: 276 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12481 Color: 1
Size: 3213 Color: 0
Size: 642 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12513 Color: 1
Size: 3507 Color: 2
Size: 316 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12606 Color: 4
Size: 3252 Color: 0
Size: 478 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 1
Size: 2046 Color: 0
Size: 1482 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 1
Size: 2392 Color: 2
Size: 1072 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13054 Color: 2
Size: 2810 Color: 0
Size: 472 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 2
Size: 2888 Color: 0
Size: 288 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 2
Size: 2648 Color: 0
Size: 432 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 3
Size: 2132 Color: 0
Size: 936 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13277 Color: 4
Size: 2399 Color: 0
Size: 660 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 2
Size: 2566 Color: 4
Size: 454 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13396 Color: 0
Size: 2364 Color: 2
Size: 576 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13447 Color: 0
Size: 2689 Color: 1
Size: 200 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13480 Color: 0
Size: 1696 Color: 3
Size: 1160 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13442 Color: 3
Size: 2414 Color: 2
Size: 480 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13492 Color: 0
Size: 2432 Color: 3
Size: 412 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13508 Color: 0
Size: 2548 Color: 4
Size: 280 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13607 Color: 4
Size: 2381 Color: 0
Size: 348 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 0
Size: 2296 Color: 1
Size: 304 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13782 Color: 0
Size: 1386 Color: 4
Size: 1168 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13759 Color: 4
Size: 2081 Color: 0
Size: 496 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 2
Size: 1940 Color: 2
Size: 580 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13824 Color: 2
Size: 2130 Color: 0
Size: 382 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13850 Color: 0
Size: 1834 Color: 3
Size: 652 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13882 Color: 0
Size: 1414 Color: 3
Size: 1040 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 2
Size: 1898 Color: 2
Size: 542 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13897 Color: 0
Size: 1879 Color: 3
Size: 560 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14012 Color: 0
Size: 1216 Color: 4
Size: 1108 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 0
Size: 1360 Color: 2
Size: 924 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14083 Color: 0
Size: 1749 Color: 3
Size: 504 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 0
Size: 1264 Color: 2
Size: 872 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 0
Size: 1782 Color: 1
Size: 352 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14186 Color: 4
Size: 1360 Color: 2
Size: 790 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14280 Color: 0
Size: 1784 Color: 1
Size: 272 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14284 Color: 0
Size: 1678 Color: 4
Size: 374 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14239 Color: 4
Size: 1905 Color: 0
Size: 192 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14319 Color: 2
Size: 1581 Color: 2
Size: 436 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14320 Color: 2
Size: 1152 Color: 0
Size: 864 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14391 Color: 0
Size: 1481 Color: 4
Size: 464 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 2
Size: 1600 Color: 0
Size: 392 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 0
Size: 1562 Color: 1
Size: 370 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14406 Color: 0
Size: 1546 Color: 3
Size: 384 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14410 Color: 3
Size: 1606 Color: 0
Size: 320 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14439 Color: 3
Size: 1521 Color: 0
Size: 376 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14444 Color: 2
Size: 1556 Color: 1
Size: 336 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14482 Color: 0
Size: 1494 Color: 1
Size: 360 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14520 Color: 0
Size: 1426 Color: 4
Size: 390 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 4
Size: 1078 Color: 0
Size: 744 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14546 Color: 4
Size: 1328 Color: 0
Size: 462 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14580 Color: 0
Size: 1468 Color: 3
Size: 288 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14569 Color: 3
Size: 1473 Color: 2
Size: 294 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 4
Size: 1458 Color: 3
Size: 288 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14626 Color: 0
Size: 1110 Color: 4
Size: 600 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14674 Color: 0
Size: 1352 Color: 4
Size: 310 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14680 Color: 4
Size: 1116 Color: 0
Size: 540 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 14692 Color: 3
Size: 1352 Color: 0
Size: 292 Color: 4

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 9675 Color: 4
Size: 5764 Color: 0
Size: 896 Color: 2

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 9867 Color: 3
Size: 5820 Color: 0
Size: 648 Color: 2

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12374 Color: 1
Size: 3533 Color: 0
Size: 428 Color: 0

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12433 Color: 1
Size: 3120 Color: 3
Size: 782 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12855 Color: 1
Size: 2288 Color: 0
Size: 1192 Color: 2

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 12858 Color: 0
Size: 3253 Color: 0
Size: 224 Color: 1

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 4
Size: 1915 Color: 1
Size: 1448 Color: 4

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13081 Color: 4
Size: 2750 Color: 1
Size: 504 Color: 0

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13229 Color: 4
Size: 2738 Color: 3
Size: 368 Color: 0

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13350 Color: 4
Size: 2431 Color: 0
Size: 554 Color: 3

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 13559 Color: 3
Size: 1928 Color: 3
Size: 848 Color: 0

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 13727 Color: 0
Size: 2258 Color: 4
Size: 350 Color: 3

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 13750 Color: 0
Size: 2329 Color: 3
Size: 256 Color: 4

Bin 81: 1 of cap free
Amount of items: 2
Items: 
Size: 13845 Color: 2
Size: 2490 Color: 3

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 13991 Color: 2
Size: 1848 Color: 0
Size: 496 Color: 4

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 14039 Color: 3
Size: 1416 Color: 1
Size: 880 Color: 0

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 4
Size: 1580 Color: 0
Size: 640 Color: 1

Bin 85: 1 of cap free
Amount of items: 2
Items: 
Size: 14684 Color: 2
Size: 1651 Color: 4

Bin 86: 2 of cap free
Amount of items: 4
Items: 
Size: 8180 Color: 0
Size: 5546 Color: 3
Size: 1902 Color: 1
Size: 706 Color: 4

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 9202 Color: 1
Size: 6764 Color: 3
Size: 368 Color: 0

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 11637 Color: 4
Size: 4185 Color: 3
Size: 512 Color: 1

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 11844 Color: 3
Size: 4062 Color: 4
Size: 428 Color: 1

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 12090 Color: 1
Size: 3784 Color: 3
Size: 460 Color: 4

Bin 91: 2 of cap free
Amount of items: 2
Items: 
Size: 11960 Color: 3
Size: 4374 Color: 2

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 1
Size: 3228 Color: 0
Size: 650 Color: 0

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 12718 Color: 1
Size: 3240 Color: 2
Size: 376 Color: 2

Bin 94: 2 of cap free
Amount of items: 2
Items: 
Size: 13962 Color: 3
Size: 2372 Color: 2

Bin 95: 2 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 2
Size: 2310 Color: 3

Bin 96: 2 of cap free
Amount of items: 2
Items: 
Size: 14159 Color: 3
Size: 2175 Color: 4

Bin 97: 2 of cap free
Amount of items: 2
Items: 
Size: 14476 Color: 4
Size: 1858 Color: 3

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 9634 Color: 0
Size: 6265 Color: 1
Size: 434 Color: 3

Bin 99: 3 of cap free
Amount of items: 2
Items: 
Size: 11180 Color: 3
Size: 5153 Color: 4

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 12129 Color: 0
Size: 3964 Color: 3
Size: 240 Color: 0

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 13049 Color: 0
Size: 2648 Color: 1
Size: 636 Color: 0

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 13085 Color: 3
Size: 1868 Color: 2
Size: 1380 Color: 0

Bin 103: 3 of cap free
Amount of items: 2
Items: 
Size: 14378 Color: 3
Size: 1955 Color: 2

Bin 104: 4 of cap free
Amount of items: 4
Items: 
Size: 8212 Color: 2
Size: 6104 Color: 1
Size: 1720 Color: 0
Size: 296 Color: 0

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 9300 Color: 1
Size: 6728 Color: 0
Size: 304 Color: 2

Bin 106: 4 of cap free
Amount of items: 3
Items: 
Size: 9356 Color: 0
Size: 6064 Color: 1
Size: 912 Color: 2

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 10056 Color: 3
Size: 5562 Color: 3
Size: 714 Color: 1

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 11028 Color: 2
Size: 5016 Color: 1
Size: 288 Color: 3

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 0
Size: 4300 Color: 2
Size: 868 Color: 1

Bin 110: 4 of cap free
Amount of items: 3
Items: 
Size: 13038 Color: 3
Size: 1682 Color: 4
Size: 1612 Color: 1

Bin 111: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 4
Size: 2504 Color: 1

Bin 112: 5 of cap free
Amount of items: 3
Items: 
Size: 11589 Color: 4
Size: 3634 Color: 0
Size: 1108 Color: 1

Bin 113: 5 of cap free
Amount of items: 2
Items: 
Size: 13780 Color: 1
Size: 2551 Color: 2

Bin 114: 5 of cap free
Amount of items: 2
Items: 
Size: 14235 Color: 1
Size: 2096 Color: 4

Bin 115: 6 of cap free
Amount of items: 20
Items: 
Size: 1308 Color: 1
Size: 1008 Color: 0
Size: 992 Color: 4
Size: 992 Color: 1
Size: 992 Color: 0
Size: 856 Color: 1
Size: 836 Color: 3
Size: 812 Color: 1
Size: 784 Color: 2
Size: 776 Color: 4
Size: 776 Color: 3
Size: 752 Color: 1
Size: 752 Color: 0
Size: 728 Color: 3
Size: 724 Color: 4
Size: 704 Color: 3
Size: 704 Color: 2
Size: 700 Color: 0
Size: 654 Color: 0
Size: 480 Color: 2

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 8184 Color: 0
Size: 6802 Color: 2
Size: 1344 Color: 2

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 9666 Color: 1
Size: 6392 Color: 2
Size: 272 Color: 3

Bin 118: 6 of cap free
Amount of items: 3
Items: 
Size: 13630 Color: 1
Size: 2568 Color: 2
Size: 132 Color: 3

Bin 119: 6 of cap free
Amount of items: 4
Items: 
Size: 14100 Color: 3
Size: 2040 Color: 4
Size: 110 Color: 4
Size: 80 Color: 0

Bin 120: 7 of cap free
Amount of items: 3
Items: 
Size: 11090 Color: 0
Size: 4823 Color: 4
Size: 416 Color: 1

Bin 121: 7 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 1
Size: 2673 Color: 3

Bin 122: 7 of cap free
Amount of items: 2
Items: 
Size: 14252 Color: 2
Size: 2077 Color: 4

Bin 123: 8 of cap free
Amount of items: 3
Items: 
Size: 8196 Color: 0
Size: 6772 Color: 1
Size: 1360 Color: 0

Bin 124: 8 of cap free
Amount of items: 3
Items: 
Size: 11669 Color: 4
Size: 4351 Color: 3
Size: 308 Color: 1

Bin 125: 8 of cap free
Amount of items: 2
Items: 
Size: 14588 Color: 2
Size: 1740 Color: 1

Bin 126: 9 of cap free
Amount of items: 3
Items: 
Size: 10328 Color: 2
Size: 5551 Color: 3
Size: 448 Color: 1

Bin 127: 9 of cap free
Amount of items: 2
Items: 
Size: 10364 Color: 0
Size: 5963 Color: 1

Bin 128: 9 of cap free
Amount of items: 3
Items: 
Size: 10787 Color: 4
Size: 5054 Color: 2
Size: 486 Color: 1

Bin 129: 9 of cap free
Amount of items: 2
Items: 
Size: 12436 Color: 4
Size: 3891 Color: 2

Bin 130: 10 of cap free
Amount of items: 5
Items: 
Size: 8171 Color: 0
Size: 3442 Color: 1
Size: 2741 Color: 4
Size: 1460 Color: 2
Size: 512 Color: 0

Bin 131: 10 of cap free
Amount of items: 2
Items: 
Size: 12753 Color: 0
Size: 3573 Color: 4

Bin 132: 10 of cap free
Amount of items: 3
Items: 
Size: 13033 Color: 0
Size: 2781 Color: 2
Size: 512 Color: 1

Bin 133: 10 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 1
Size: 3150 Color: 3

Bin 134: 10 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 4
Size: 2158 Color: 3
Size: 48 Color: 4

Bin 135: 10 of cap free
Amount of items: 2
Items: 
Size: 14326 Color: 3
Size: 2000 Color: 1

Bin 136: 10 of cap free
Amount of items: 2
Items: 
Size: 14511 Color: 2
Size: 1815 Color: 4

Bin 137: 12 of cap free
Amount of items: 5
Items: 
Size: 8170 Color: 4
Size: 2713 Color: 2
Size: 2275 Color: 4
Size: 1794 Color: 1
Size: 1372 Color: 1

Bin 138: 12 of cap free
Amount of items: 3
Items: 
Size: 8680 Color: 3
Size: 6788 Color: 1
Size: 856 Color: 4

Bin 139: 12 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 4
Size: 3898 Color: 1
Size: 964 Color: 0

Bin 140: 12 of cap free
Amount of items: 2
Items: 
Size: 14416 Color: 1
Size: 1908 Color: 2

Bin 141: 13 of cap free
Amount of items: 3
Items: 
Size: 8264 Color: 3
Size: 7511 Color: 2
Size: 548 Color: 0

Bin 142: 13 of cap free
Amount of items: 3
Items: 
Size: 8479 Color: 2
Size: 7540 Color: 4
Size: 304 Color: 1

Bin 143: 13 of cap free
Amount of items: 3
Items: 
Size: 9181 Color: 2
Size: 6806 Color: 3
Size: 336 Color: 0

Bin 144: 13 of cap free
Amount of items: 2
Items: 
Size: 14062 Color: 1
Size: 2261 Color: 4

Bin 145: 13 of cap free
Amount of items: 2
Items: 
Size: 14642 Color: 4
Size: 1681 Color: 1

Bin 146: 14 of cap free
Amount of items: 2
Items: 
Size: 14355 Color: 3
Size: 1967 Color: 4

Bin 147: 15 of cap free
Amount of items: 3
Items: 
Size: 10549 Color: 2
Size: 5036 Color: 2
Size: 736 Color: 0

Bin 148: 15 of cap free
Amount of items: 2
Items: 
Size: 11117 Color: 2
Size: 5204 Color: 0

Bin 149: 15 of cap free
Amount of items: 2
Items: 
Size: 13334 Color: 4
Size: 2987 Color: 1

Bin 150: 15 of cap free
Amount of items: 2
Items: 
Size: 13419 Color: 1
Size: 2902 Color: 3

Bin 151: 15 of cap free
Amount of items: 2
Items: 
Size: 14562 Color: 4
Size: 1759 Color: 1

Bin 152: 16 of cap free
Amount of items: 2
Items: 
Size: 14469 Color: 1
Size: 1851 Color: 3

Bin 153: 17 of cap free
Amount of items: 2
Items: 
Size: 13566 Color: 1
Size: 2753 Color: 4

Bin 154: 20 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 3
Size: 1716 Color: 1

Bin 155: 24 of cap free
Amount of items: 2
Items: 
Size: 11884 Color: 3
Size: 4428 Color: 0

Bin 156: 24 of cap free
Amount of items: 2
Items: 
Size: 14561 Color: 2
Size: 1751 Color: 1

Bin 157: 29 of cap free
Amount of items: 3
Items: 
Size: 10718 Color: 4
Size: 3917 Color: 1
Size: 1672 Color: 2

Bin 158: 30 of cap free
Amount of items: 2
Items: 
Size: 14058 Color: 4
Size: 2248 Color: 3

Bin 159: 30 of cap free
Amount of items: 2
Items: 
Size: 14138 Color: 2
Size: 2168 Color: 1

Bin 160: 32 of cap free
Amount of items: 2
Items: 
Size: 14322 Color: 1
Size: 1982 Color: 2

Bin 161: 34 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 2
Size: 3018 Color: 1

Bin 162: 35 of cap free
Amount of items: 3
Items: 
Size: 9234 Color: 4
Size: 6807 Color: 3
Size: 260 Color: 4

Bin 163: 35 of cap free
Amount of items: 2
Items: 
Size: 14227 Color: 4
Size: 2074 Color: 2

Bin 164: 36 of cap free
Amount of items: 2
Items: 
Size: 13479 Color: 4
Size: 2821 Color: 2

Bin 165: 37 of cap free
Amount of items: 2
Items: 
Size: 13639 Color: 1
Size: 2660 Color: 2

Bin 166: 37 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 0
Size: 2149 Color: 2
Size: 40 Color: 3

Bin 167: 41 of cap free
Amount of items: 2
Items: 
Size: 11315 Color: 2
Size: 4980 Color: 4

Bin 168: 42 of cap free
Amount of items: 3
Items: 
Size: 13258 Color: 3
Size: 2876 Color: 4
Size: 160 Color: 2

Bin 169: 44 of cap free
Amount of items: 2
Items: 
Size: 13977 Color: 3
Size: 2315 Color: 1

Bin 170: 45 of cap free
Amount of items: 3
Items: 
Size: 10274 Color: 0
Size: 5369 Color: 2
Size: 648 Color: 1

Bin 171: 45 of cap free
Amount of items: 2
Items: 
Size: 13104 Color: 4
Size: 3187 Color: 2

Bin 172: 45 of cap free
Amount of items: 2
Items: 
Size: 13839 Color: 4
Size: 2452 Color: 2

Bin 173: 46 of cap free
Amount of items: 2
Items: 
Size: 13389 Color: 2
Size: 2901 Color: 3

Bin 174: 47 of cap free
Amount of items: 7
Items: 
Size: 8169 Color: 2
Size: 1634 Color: 1
Size: 1610 Color: 1
Size: 1344 Color: 3
Size: 1344 Color: 1
Size: 1188 Color: 3
Size: 1000 Color: 3

Bin 175: 47 of cap free
Amount of items: 2
Items: 
Size: 10703 Color: 2
Size: 5586 Color: 4

Bin 176: 48 of cap free
Amount of items: 3
Items: 
Size: 11084 Color: 2
Size: 4276 Color: 3
Size: 928 Color: 3

Bin 177: 48 of cap free
Amount of items: 2
Items: 
Size: 13336 Color: 1
Size: 2952 Color: 3

Bin 178: 49 of cap free
Amount of items: 2
Items: 
Size: 11662 Color: 4
Size: 4625 Color: 0

Bin 179: 54 of cap free
Amount of items: 2
Items: 
Size: 12558 Color: 2
Size: 3724 Color: 3

Bin 180: 61 of cap free
Amount of items: 2
Items: 
Size: 9471 Color: 3
Size: 6804 Color: 1

Bin 181: 62 of cap free
Amount of items: 2
Items: 
Size: 13459 Color: 4
Size: 2815 Color: 2

Bin 182: 67 of cap free
Amount of items: 2
Items: 
Size: 9895 Color: 2
Size: 6374 Color: 4

Bin 183: 67 of cap free
Amount of items: 2
Items: 
Size: 13001 Color: 2
Size: 3268 Color: 0

Bin 184: 81 of cap free
Amount of items: 2
Items: 
Size: 12953 Color: 1
Size: 3302 Color: 2

Bin 185: 84 of cap free
Amount of items: 2
Items: 
Size: 8172 Color: 1
Size: 8080 Color: 3

Bin 186: 90 of cap free
Amount of items: 2
Items: 
Size: 10300 Color: 0
Size: 5946 Color: 2

Bin 187: 105 of cap free
Amount of items: 2
Items: 
Size: 9682 Color: 2
Size: 6549 Color: 1

Bin 188: 108 of cap free
Amount of items: 2
Items: 
Size: 9420 Color: 1
Size: 6808 Color: 2

Bin 189: 110 of cap free
Amount of items: 2
Items: 
Size: 11544 Color: 2
Size: 4682 Color: 4

Bin 190: 124 of cap free
Amount of items: 2
Items: 
Size: 11868 Color: 3
Size: 4344 Color: 2

Bin 191: 148 of cap free
Amount of items: 3
Items: 
Size: 8228 Color: 3
Size: 5868 Color: 2
Size: 2092 Color: 0

Bin 192: 168 of cap free
Amount of items: 2
Items: 
Size: 9016 Color: 0
Size: 7152 Color: 4

Bin 193: 182 of cap free
Amount of items: 2
Items: 
Size: 12892 Color: 2
Size: 3262 Color: 0

Bin 194: 198 of cap free
Amount of items: 2
Items: 
Size: 12422 Color: 3
Size: 3716 Color: 4

Bin 195: 217 of cap free
Amount of items: 2
Items: 
Size: 10728 Color: 3
Size: 5391 Color: 2

Bin 196: 220 of cap free
Amount of items: 2
Items: 
Size: 11800 Color: 3
Size: 4316 Color: 4

Bin 197: 259 of cap free
Amount of items: 4
Items: 
Size: 8174 Color: 2
Size: 3542 Color: 1
Size: 2804 Color: 3
Size: 1557 Color: 4

Bin 198: 280 of cap free
Amount of items: 34
Items: 
Size: 640 Color: 1
Size: 628 Color: 3
Size: 624 Color: 0
Size: 600 Color: 2
Size: 596 Color: 3
Size: 576 Color: 4
Size: 576 Color: 2
Size: 568 Color: 0
Size: 562 Color: 0
Size: 550 Color: 4
Size: 546 Color: 0
Size: 544 Color: 2
Size: 528 Color: 1
Size: 488 Color: 0
Size: 476 Color: 4
Size: 464 Color: 0
Size: 434 Color: 3
Size: 432 Color: 4
Size: 424 Color: 2
Size: 424 Color: 1
Size: 416 Color: 4
Size: 416 Color: 0
Size: 414 Color: 2
Size: 408 Color: 2
Size: 400 Color: 4
Size: 392 Color: 3
Size: 384 Color: 2
Size: 376 Color: 3
Size: 368 Color: 3
Size: 368 Color: 1
Size: 364 Color: 2
Size: 362 Color: 3
Size: 356 Color: 1
Size: 352 Color: 2

Bin 199: 12178 of cap free
Amount of items: 13
Items: 
Size: 344 Color: 1
Size: 336 Color: 4
Size: 332 Color: 4
Size: 332 Color: 3
Size: 324 Color: 3
Size: 320 Color: 4
Size: 320 Color: 3
Size: 320 Color: 2
Size: 320 Color: 1
Size: 320 Color: 1
Size: 312 Color: 0
Size: 294 Color: 0
Size: 284 Color: 0

Total size: 3234528
Total free space: 16336

