Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 5
Size: 250 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 18
Size: 276 Color: 8
Size: 262 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 2
Size: 347 Color: 7
Size: 281 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 276 Color: 15
Size: 261 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 19
Size: 340 Color: 14
Size: 305 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 335 Color: 16
Size: 285 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 10
Size: 252 Color: 19
Size: 264 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 2
Size: 360 Color: 16
Size: 252 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 7
Size: 269 Color: 1
Size: 262 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 3
Size: 259 Color: 7
Size: 252 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 9
Size: 334 Color: 9
Size: 270 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 15
Size: 302 Color: 10
Size: 282 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 3
Size: 328 Color: 6
Size: 283 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 16
Size: 290 Color: 2
Size: 278 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 6
Size: 265 Color: 7
Size: 252 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7
Size: 327 Color: 2
Size: 301 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 14
Size: 286 Color: 9
Size: 281 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 10
Size: 358 Color: 16
Size: 281 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 13
Size: 352 Color: 19
Size: 268 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 296 Color: 8
Size: 282 Color: 0

Total size: 20000
Total free space: 0

