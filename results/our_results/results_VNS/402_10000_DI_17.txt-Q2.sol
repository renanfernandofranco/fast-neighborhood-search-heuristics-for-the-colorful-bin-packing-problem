Capicity Bin: 8264
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 33
Items: 
Size: 344 Color: 0
Size: 336 Color: 0
Size: 336 Color: 0
Size: 334 Color: 0
Size: 334 Color: 0
Size: 308 Color: 1
Size: 302 Color: 1
Size: 292 Color: 1
Size: 288 Color: 1
Size: 272 Color: 1
Size: 264 Color: 1
Size: 264 Color: 1
Size: 256 Color: 1
Size: 252 Color: 0
Size: 248 Color: 1
Size: 248 Color: 0
Size: 246 Color: 0
Size: 244 Color: 1
Size: 236 Color: 1
Size: 232 Color: 1
Size: 232 Color: 0
Size: 226 Color: 1
Size: 224 Color: 0
Size: 222 Color: 1
Size: 216 Color: 0
Size: 208 Color: 1
Size: 204 Color: 1
Size: 200 Color: 1
Size: 192 Color: 0
Size: 190 Color: 0
Size: 186 Color: 0
Size: 184 Color: 0
Size: 144 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 0
Size: 3764 Color: 1
Size: 184 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4730 Color: 0
Size: 3338 Color: 1
Size: 196 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 1
Size: 2596 Color: 1
Size: 512 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5362 Color: 1
Size: 2716 Color: 0
Size: 186 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5417 Color: 1
Size: 2373 Color: 0
Size: 474 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6019 Color: 1
Size: 2097 Color: 1
Size: 148 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 1
Size: 1996 Color: 1
Size: 158 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6436 Color: 0
Size: 1692 Color: 0
Size: 136 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6852 Color: 1
Size: 724 Color: 0
Size: 688 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 0
Size: 1114 Color: 1
Size: 248 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7141 Color: 1
Size: 751 Color: 1
Size: 372 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7154 Color: 1
Size: 568 Color: 1
Size: 542 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7214 Color: 1
Size: 854 Color: 1
Size: 196 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7364 Color: 1
Size: 732 Color: 0
Size: 168 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7388 Color: 0
Size: 716 Color: 0
Size: 160 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7404 Color: 1
Size: 688 Color: 1
Size: 172 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 4681 Color: 1
Size: 3422 Color: 0
Size: 160 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 5001 Color: 0
Size: 2962 Color: 1
Size: 300 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 5174 Color: 1
Size: 2713 Color: 1
Size: 376 Color: 0

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 5403 Color: 1
Size: 2860 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 5574 Color: 1
Size: 2385 Color: 0
Size: 304 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 5749 Color: 0
Size: 2284 Color: 1
Size: 230 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 0
Size: 2091 Color: 1
Size: 176 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 1
Size: 1788 Color: 0
Size: 216 Color: 1

Bin 26: 1 of cap free
Amount of items: 4
Items: 
Size: 6412 Color: 0
Size: 854 Color: 1
Size: 841 Color: 1
Size: 156 Color: 0

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 6580 Color: 0
Size: 1683 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 1
Size: 1227 Color: 0
Size: 304 Color: 1

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 6793 Color: 1
Size: 1470 Color: 0

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 7322 Color: 0
Size: 941 Color: 1

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 4140 Color: 1
Size: 3438 Color: 0
Size: 684 Color: 1

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 4262 Color: 0
Size: 3584 Color: 0
Size: 416 Color: 1

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 5009 Color: 0
Size: 2993 Color: 1
Size: 260 Color: 0

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 5532 Color: 0
Size: 2578 Color: 1
Size: 152 Color: 0

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 5850 Color: 0
Size: 2412 Color: 1

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 6236 Color: 1
Size: 1862 Color: 0
Size: 164 Color: 1

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 6459 Color: 0
Size: 1665 Color: 0
Size: 138 Color: 1

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 6686 Color: 1
Size: 1576 Color: 0

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 7020 Color: 1
Size: 1242 Color: 0

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 7082 Color: 1
Size: 1180 Color: 0

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 7164 Color: 0
Size: 1098 Color: 1

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 7298 Color: 1
Size: 964 Color: 0

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 7354 Color: 1
Size: 908 Color: 0

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 7418 Color: 0
Size: 844 Color: 1

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 4134 Color: 1
Size: 3443 Color: 0
Size: 684 Color: 1

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 0
Size: 1505 Color: 1
Size: 136 Color: 0

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 6907 Color: 0
Size: 878 Color: 1
Size: 476 Color: 1

Bin 48: 3 of cap free
Amount of items: 2
Items: 
Size: 7137 Color: 1
Size: 1124 Color: 0

Bin 49: 3 of cap free
Amount of items: 2
Items: 
Size: 7359 Color: 1
Size: 902 Color: 0

Bin 50: 4 of cap free
Amount of items: 3
Items: 
Size: 5012 Color: 1
Size: 3104 Color: 1
Size: 144 Color: 0

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 1
Size: 2422 Color: 0
Size: 162 Color: 1

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 6619 Color: 1
Size: 1361 Color: 0
Size: 280 Color: 1

Bin 53: 4 of cap free
Amount of items: 2
Items: 
Size: 7274 Color: 0
Size: 986 Color: 1

Bin 54: 5 of cap free
Amount of items: 7
Items: 
Size: 4135 Color: 0
Size: 790 Color: 1
Size: 782 Color: 1
Size: 740 Color: 1
Size: 684 Color: 0
Size: 680 Color: 0
Size: 448 Color: 0

Bin 55: 5 of cap free
Amount of items: 3
Items: 
Size: 4673 Color: 1
Size: 3430 Color: 1
Size: 156 Color: 0

Bin 56: 5 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 0
Size: 1798 Color: 0
Size: 194 Color: 1

Bin 57: 5 of cap free
Amount of items: 3
Items: 
Size: 6605 Color: 1
Size: 868 Color: 1
Size: 786 Color: 0

Bin 58: 5 of cap free
Amount of items: 2
Items: 
Size: 6970 Color: 1
Size: 1289 Color: 0

Bin 59: 5 of cap free
Amount of items: 2
Items: 
Size: 7093 Color: 0
Size: 1166 Color: 1

Bin 60: 6 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 0
Size: 2242 Color: 1
Size: 276 Color: 0

Bin 61: 6 of cap free
Amount of items: 3
Items: 
Size: 6451 Color: 1
Size: 1671 Color: 0
Size: 136 Color: 1

Bin 62: 6 of cap free
Amount of items: 2
Items: 
Size: 6734 Color: 0
Size: 1524 Color: 1

Bin 63: 6 of cap free
Amount of items: 2
Items: 
Size: 6924 Color: 0
Size: 1334 Color: 1

Bin 64: 6 of cap free
Amount of items: 2
Items: 
Size: 7281 Color: 1
Size: 977 Color: 0

Bin 65: 7 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 1
Size: 2707 Color: 0
Size: 274 Color: 1

Bin 66: 7 of cap free
Amount of items: 2
Items: 
Size: 7236 Color: 1
Size: 1021 Color: 0

Bin 67: 7 of cap free
Amount of items: 2
Items: 
Size: 7257 Color: 1
Size: 1000 Color: 0

Bin 68: 7 of cap free
Amount of items: 3
Items: 
Size: 7412 Color: 1
Size: 821 Color: 0
Size: 24 Color: 0

Bin 69: 8 of cap free
Amount of items: 3
Items: 
Size: 4516 Color: 0
Size: 3542 Color: 1
Size: 198 Color: 0

Bin 70: 8 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 0
Size: 1730 Color: 1

Bin 71: 8 of cap free
Amount of items: 2
Items: 
Size: 6978 Color: 0
Size: 1278 Color: 1

Bin 72: 8 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 1
Size: 1018 Color: 0
Size: 56 Color: 0

Bin 73: 9 of cap free
Amount of items: 3
Items: 
Size: 4150 Color: 0
Size: 3441 Color: 1
Size: 664 Color: 1

Bin 74: 9 of cap free
Amount of items: 2
Items: 
Size: 6883 Color: 0
Size: 1372 Color: 1

Bin 75: 9 of cap free
Amount of items: 2
Items: 
Size: 7117 Color: 0
Size: 1138 Color: 1

Bin 76: 10 of cap free
Amount of items: 3
Items: 
Size: 6245 Color: 1
Size: 1865 Color: 0
Size: 144 Color: 1

Bin 77: 10 of cap free
Amount of items: 2
Items: 
Size: 7330 Color: 1
Size: 924 Color: 0

Bin 78: 10 of cap free
Amount of items: 2
Items: 
Size: 7428 Color: 0
Size: 826 Color: 1

Bin 79: 12 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 1
Size: 1379 Color: 0
Size: 112 Color: 0

Bin 80: 12 of cap free
Amount of items: 2
Items: 
Size: 7004 Color: 1
Size: 1248 Color: 0

Bin 81: 12 of cap free
Amount of items: 2
Items: 
Size: 7365 Color: 1
Size: 887 Color: 0

Bin 82: 12 of cap free
Amount of items: 2
Items: 
Size: 7394 Color: 1
Size: 858 Color: 0

Bin 83: 13 of cap free
Amount of items: 3
Items: 
Size: 6414 Color: 0
Size: 1111 Color: 0
Size: 726 Color: 1

Bin 84: 15 of cap free
Amount of items: 2
Items: 
Size: 5757 Color: 0
Size: 2492 Color: 1

Bin 85: 15 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 0
Size: 1383 Color: 1

Bin 86: 15 of cap free
Amount of items: 3
Items: 
Size: 7380 Color: 1
Size: 813 Color: 0
Size: 56 Color: 1

Bin 87: 16 of cap free
Amount of items: 2
Items: 
Size: 5730 Color: 1
Size: 2518 Color: 0

Bin 88: 16 of cap free
Amount of items: 2
Items: 
Size: 7325 Color: 1
Size: 923 Color: 0

Bin 89: 17 of cap free
Amount of items: 3
Items: 
Size: 4714 Color: 1
Size: 3115 Color: 1
Size: 418 Color: 0

Bin 90: 17 of cap free
Amount of items: 3
Items: 
Size: 5566 Color: 1
Size: 2381 Color: 0
Size: 300 Color: 0

Bin 91: 17 of cap free
Amount of items: 2
Items: 
Size: 6963 Color: 1
Size: 1284 Color: 0

Bin 92: 18 of cap free
Amount of items: 3
Items: 
Size: 6446 Color: 1
Size: 1320 Color: 0
Size: 480 Color: 1

Bin 93: 18 of cap free
Amount of items: 2
Items: 
Size: 7289 Color: 0
Size: 957 Color: 1

Bin 94: 18 of cap free
Amount of items: 2
Items: 
Size: 7309 Color: 0
Size: 937 Color: 1

Bin 95: 19 of cap free
Amount of items: 8
Items: 
Size: 4133 Color: 0
Size: 756 Color: 1
Size: 598 Color: 0
Size: 588 Color: 0
Size: 552 Color: 0
Size: 542 Color: 1
Size: 540 Color: 1
Size: 536 Color: 1

Bin 96: 19 of cap free
Amount of items: 2
Items: 
Size: 7201 Color: 1
Size: 1044 Color: 0

Bin 97: 21 of cap free
Amount of items: 2
Items: 
Size: 5876 Color: 0
Size: 2367 Color: 1

Bin 98: 22 of cap free
Amount of items: 2
Items: 
Size: 7157 Color: 0
Size: 1085 Color: 1

Bin 99: 22 of cap free
Amount of items: 2
Items: 
Size: 7260 Color: 0
Size: 982 Color: 1

Bin 100: 23 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 1
Size: 1151 Color: 0

Bin 101: 27 of cap free
Amount of items: 2
Items: 
Size: 6719 Color: 0
Size: 1518 Color: 1

Bin 102: 28 of cap free
Amount of items: 2
Items: 
Size: 4938 Color: 1
Size: 3298 Color: 0

Bin 103: 28 of cap free
Amount of items: 2
Items: 
Size: 6931 Color: 0
Size: 1305 Color: 1

Bin 104: 29 of cap free
Amount of items: 2
Items: 
Size: 6785 Color: 1
Size: 1450 Color: 0

Bin 105: 29 of cap free
Amount of items: 2
Items: 
Size: 7002 Color: 0
Size: 1233 Color: 1

Bin 106: 30 of cap free
Amount of items: 2
Items: 
Size: 7180 Color: 0
Size: 1054 Color: 1

Bin 107: 32 of cap free
Amount of items: 4
Items: 
Size: 7306 Color: 1
Size: 802 Color: 0
Size: 76 Color: 0
Size: 48 Color: 1

Bin 108: 34 of cap free
Amount of items: 2
Items: 
Size: 4158 Color: 0
Size: 4072 Color: 1

Bin 109: 36 of cap free
Amount of items: 3
Items: 
Size: 7238 Color: 0
Size: 926 Color: 1
Size: 64 Color: 0

Bin 110: 37 of cap free
Amount of items: 2
Items: 
Size: 7108 Color: 0
Size: 1119 Color: 1

Bin 111: 38 of cap free
Amount of items: 2
Items: 
Size: 6212 Color: 0
Size: 2014 Color: 1

Bin 112: 40 of cap free
Amount of items: 3
Items: 
Size: 4836 Color: 0
Size: 3124 Color: 1
Size: 264 Color: 0

Bin 113: 42 of cap free
Amount of items: 2
Items: 
Size: 6680 Color: 1
Size: 1542 Color: 0

Bin 114: 42 of cap free
Amount of items: 2
Items: 
Size: 7046 Color: 0
Size: 1176 Color: 1

Bin 115: 47 of cap free
Amount of items: 2
Items: 
Size: 6611 Color: 0
Size: 1606 Color: 1

Bin 116: 50 of cap free
Amount of items: 2
Items: 
Size: 6666 Color: 0
Size: 1548 Color: 1

Bin 117: 52 of cap free
Amount of items: 3
Items: 
Size: 6190 Color: 1
Size: 1318 Color: 0
Size: 704 Color: 0

Bin 118: 54 of cap free
Amount of items: 3
Items: 
Size: 4142 Color: 0
Size: 3444 Color: 0
Size: 624 Color: 1

Bin 119: 55 of cap free
Amount of items: 2
Items: 
Size: 6338 Color: 0
Size: 1871 Color: 1

Bin 120: 64 of cap free
Amount of items: 2
Items: 
Size: 7069 Color: 1
Size: 1131 Color: 0

Bin 121: 65 of cap free
Amount of items: 2
Items: 
Size: 5425 Color: 0
Size: 2774 Color: 1

Bin 122: 83 of cap free
Amount of items: 2
Items: 
Size: 6502 Color: 0
Size: 1679 Color: 1

Bin 123: 93 of cap free
Amount of items: 2
Items: 
Size: 6774 Color: 1
Size: 1397 Color: 0

Bin 124: 95 of cap free
Amount of items: 2
Items: 
Size: 6765 Color: 0
Size: 1404 Color: 1

Bin 125: 105 of cap free
Amount of items: 2
Items: 
Size: 6648 Color: 1
Size: 1511 Color: 0

Bin 126: 109 of cap free
Amount of items: 2
Items: 
Size: 7039 Color: 0
Size: 1116 Color: 1

Bin 127: 112 of cap free
Amount of items: 19
Items: 
Size: 588 Color: 0
Size: 512 Color: 0
Size: 496 Color: 0
Size: 488 Color: 0
Size: 474 Color: 0
Size: 472 Color: 1
Size: 468 Color: 0
Size: 448 Color: 1
Size: 448 Color: 1
Size: 420 Color: 1
Size: 416 Color: 0
Size: 400 Color: 0
Size: 392 Color: 1
Size: 376 Color: 1
Size: 374 Color: 0
Size: 372 Color: 1
Size: 356 Color: 1
Size: 332 Color: 1
Size: 320 Color: 1

Bin 128: 120 of cap free
Amount of items: 2
Items: 
Size: 6030 Color: 1
Size: 2114 Color: 0

Bin 129: 121 of cap free
Amount of items: 2
Items: 
Size: 6251 Color: 1
Size: 1892 Color: 0

Bin 130: 126 of cap free
Amount of items: 5
Items: 
Size: 5017 Color: 1
Size: 841 Color: 1
Size: 797 Color: 1
Size: 783 Color: 0
Size: 700 Color: 0

Bin 131: 129 of cap free
Amount of items: 2
Items: 
Size: 6027 Color: 1
Size: 2108 Color: 0

Bin 132: 134 of cap free
Amount of items: 2
Items: 
Size: 5409 Color: 1
Size: 2721 Color: 0

Bin 133: 5560 of cap free
Amount of items: 16
Items: 
Size: 200 Color: 1
Size: 192 Color: 1
Size: 184 Color: 0
Size: 180 Color: 1
Size: 180 Color: 0
Size: 176 Color: 1
Size: 176 Color: 1
Size: 176 Color: 1
Size: 172 Color: 1
Size: 166 Color: 0
Size: 162 Color: 0
Size: 156 Color: 1
Size: 152 Color: 0
Size: 144 Color: 0
Size: 144 Color: 0
Size: 144 Color: 0

Total size: 1090848
Total free space: 8264

