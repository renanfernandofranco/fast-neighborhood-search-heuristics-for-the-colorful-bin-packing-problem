Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 315
Size: 2278 Color: 238
Size: 78 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5790 Color: 316
Size: 1932 Color: 223
Size: 414 Color: 98

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 334
Size: 1495 Color: 200
Size: 374 Color: 87

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 336
Size: 1687 Color: 212
Size: 166 Color: 25

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6286 Color: 337
Size: 1638 Color: 209
Size: 212 Color: 44

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 339
Size: 1020 Color: 164
Size: 774 Color: 146

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 343
Size: 1148 Color: 177
Size: 528 Color: 117

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6470 Color: 344
Size: 1274 Color: 186
Size: 392 Color: 92

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6510 Color: 346
Size: 1238 Color: 181
Size: 388 Color: 91

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6513 Color: 347
Size: 1353 Color: 191
Size: 270 Color: 65

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 348
Size: 1202 Color: 179
Size: 418 Color: 99

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 349
Size: 1008 Color: 162
Size: 608 Color: 127

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 353
Size: 1510 Color: 203
Size: 20 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6670 Color: 358
Size: 782 Color: 147
Size: 684 Color: 137

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6694 Color: 359
Size: 1302 Color: 190
Size: 140 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 362
Size: 1059 Color: 169
Size: 318 Color: 75

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 365
Size: 1142 Color: 176
Size: 198 Color: 36

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6806 Color: 367
Size: 1110 Color: 172
Size: 220 Color: 48

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 370
Size: 1041 Color: 165
Size: 228 Color: 51

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 374
Size: 860 Color: 153
Size: 360 Color: 85

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6945 Color: 376
Size: 1149 Color: 178
Size: 42 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 377
Size: 882 Color: 155
Size: 292 Color: 69

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6969 Color: 378
Size: 1047 Color: 167
Size: 120 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 381
Size: 668 Color: 131
Size: 440 Color: 101

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7030 Color: 382
Size: 922 Color: 157
Size: 184 Color: 30

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7162 Color: 389
Size: 722 Color: 140
Size: 252 Color: 59

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 390
Size: 748 Color: 143
Size: 208 Color: 40

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 391
Size: 790 Color: 148
Size: 156 Color: 22

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7232 Color: 394
Size: 508 Color: 113
Size: 396 Color: 93

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 395
Size: 676 Color: 136
Size: 216 Color: 47

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7246 Color: 396
Size: 580 Color: 124
Size: 310 Color: 73

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7264 Color: 398
Size: 530 Color: 118
Size: 342 Color: 81

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 399
Size: 656 Color: 129
Size: 210 Color: 42

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7302 Color: 401
Size: 576 Color: 123
Size: 258 Color: 60

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7316 Color: 402
Size: 598 Color: 126
Size: 222 Color: 49

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 4935 Color: 289
Size: 3040 Color: 260
Size: 160 Color: 24

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5667 Color: 311
Size: 1390 Color: 195
Size: 1078 Color: 170

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6097 Color: 327
Size: 1958 Color: 225
Size: 80 Color: 5

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 328
Size: 1811 Color: 219
Size: 216 Color: 46

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 6113 Color: 329
Size: 2022 Color: 228

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6227 Color: 332
Size: 1564 Color: 206
Size: 344 Color: 82

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6326 Color: 338
Size: 1809 Color: 218

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6579 Color: 352
Size: 1356 Color: 192
Size: 200 Color: 37

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6695 Color: 360
Size: 1272 Color: 185
Size: 168 Color: 26

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6799 Color: 366
Size: 672 Color: 132
Size: 664 Color: 130

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6836 Color: 368
Size: 1299 Color: 189

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 7011 Color: 379
Size: 1124 Color: 175

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 7020 Color: 380
Size: 1115 Color: 173

Bin 49: 2 of cap free
Amount of items: 5
Items: 
Size: 4126 Color: 279
Size: 3322 Color: 263
Size: 300 Color: 71
Size: 194 Color: 35
Size: 192 Color: 34

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 294
Size: 2910 Color: 257
Size: 152 Color: 20

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 301
Size: 2091 Color: 233
Size: 676 Color: 134

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6887 Color: 373
Size: 1247 Color: 183

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 7082 Color: 384
Size: 1052 Color: 168

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 7220 Color: 393
Size: 914 Color: 156

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 7260 Color: 397
Size: 874 Color: 154

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 5903 Color: 322
Size: 2230 Color: 235

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 6073 Color: 325
Size: 2060 Color: 231

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 6542 Color: 350
Size: 1591 Color: 208

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 6574 Color: 351
Size: 1559 Color: 205

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 387
Size: 993 Color: 161

Bin 61: 4 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 305
Size: 2568 Color: 249
Size: 136 Color: 11

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 5446 Color: 306
Size: 2550 Color: 246
Size: 136 Color: 10

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 5750 Color: 314
Size: 2278 Color: 237
Size: 104 Color: 6

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 6411 Color: 341
Size: 1721 Color: 216

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 354
Size: 1520 Color: 204

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 6633 Color: 355
Size: 1499 Color: 202

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 6654 Color: 357
Size: 1478 Color: 199

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 385
Size: 1042 Color: 166

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 7122 Color: 386
Size: 1010 Color: 163

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 7286 Color: 400
Size: 846 Color: 152

Bin 71: 5 of cap free
Amount of items: 2
Items: 
Size: 5462 Color: 307
Size: 2669 Color: 253

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 312
Size: 2347 Color: 242
Size: 116 Color: 7

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6086 Color: 326
Size: 2045 Color: 229

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 6471 Color: 345
Size: 1660 Color: 210

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 6719 Color: 361
Size: 1412 Color: 196

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 5078 Color: 295
Size: 2900 Color: 256
Size: 152 Color: 19

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 5627 Color: 309
Size: 2503 Color: 245

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 5830 Color: 320
Size: 2260 Color: 236
Size: 40 Color: 2

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6071 Color: 324
Size: 2059 Color: 230

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 331
Size: 1964 Color: 226
Size: 18 Color: 0

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6886 Color: 372
Size: 1244 Color: 182

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 7148 Color: 388
Size: 982 Color: 160

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 7198 Color: 392
Size: 932 Color: 159

Bin 84: 7 of cap free
Amount of items: 9
Items: 
Size: 4069 Color: 273
Size: 698 Color: 138
Size: 676 Color: 135
Size: 676 Color: 133
Size: 640 Color: 128
Size: 596 Color: 125
Size: 348 Color: 84
Size: 214 Color: 45
Size: 212 Color: 43

Bin 85: 7 of cap free
Amount of items: 3
Items: 
Size: 5355 Color: 300
Size: 1498 Color: 201
Size: 1276 Color: 187

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 5643 Color: 310
Size: 1682 Color: 211
Size: 804 Color: 149

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 335
Size: 1861 Color: 220

Bin 88: 7 of cap free
Amount of items: 2
Items: 
Size: 6876 Color: 371
Size: 1253 Color: 184

Bin 89: 7 of cap free
Amount of items: 2
Items: 
Size: 6926 Color: 375
Size: 1203 Color: 180

Bin 90: 8 of cap free
Amount of items: 3
Items: 
Size: 5388 Color: 303
Size: 2596 Color: 250
Size: 144 Color: 14

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 5822 Color: 319
Size: 2305 Color: 240

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 6435 Color: 342
Size: 1692 Color: 213

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 6251 Color: 333
Size: 1875 Color: 221

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 7042 Color: 383
Size: 1084 Color: 171

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 5806 Color: 317
Size: 2319 Color: 241

Bin 96: 11 of cap free
Amount of items: 2
Items: 
Size: 6046 Color: 323
Size: 2079 Color: 232

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 6766 Color: 364
Size: 1358 Color: 194

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 6846 Color: 369
Size: 1278 Color: 188

Bin 99: 14 of cap free
Amount of items: 2
Items: 
Size: 6380 Color: 340
Size: 1742 Color: 217

Bin 100: 15 of cap free
Amount of items: 2
Items: 
Size: 6764 Color: 363
Size: 1357 Color: 193

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 4916 Color: 288
Size: 3204 Color: 262

Bin 102: 16 of cap free
Amount of items: 2
Items: 
Size: 6652 Color: 356
Size: 1468 Color: 198

Bin 103: 17 of cap free
Amount of items: 2
Items: 
Size: 5683 Color: 313
Size: 2436 Color: 244

Bin 104: 19 of cap free
Amount of items: 5
Items: 
Size: 4076 Color: 276
Size: 1710 Color: 214
Size: 1571 Color: 207
Size: 560 Color: 122
Size: 200 Color: 38

Bin 105: 19 of cap free
Amount of items: 3
Items: 
Size: 4557 Color: 284
Size: 3388 Color: 266
Size: 172 Color: 27

Bin 106: 19 of cap free
Amount of items: 3
Items: 
Size: 5289 Color: 299
Size: 2684 Color: 254
Size: 144 Color: 16

Bin 107: 21 of cap free
Amount of items: 2
Items: 
Size: 5118 Color: 296
Size: 2997 Color: 259

Bin 108: 24 of cap free
Amount of items: 2
Items: 
Size: 5820 Color: 318
Size: 2292 Color: 239

Bin 109: 26 of cap free
Amount of items: 2
Items: 
Size: 4086 Color: 278
Size: 4024 Color: 272

Bin 110: 28 of cap free
Amount of items: 3
Items: 
Size: 4965 Color: 291
Size: 2983 Color: 258
Size: 160 Color: 23

Bin 111: 28 of cap free
Amount of items: 3
Items: 
Size: 5406 Color: 304
Size: 2566 Color: 248
Size: 136 Color: 12

Bin 112: 28 of cap free
Amount of items: 2
Items: 
Size: 6118 Color: 330
Size: 1990 Color: 227

Bin 113: 31 of cap free
Amount of items: 2
Items: 
Size: 5887 Color: 321
Size: 2218 Color: 234

Bin 114: 32 of cap free
Amount of items: 3
Items: 
Size: 5058 Color: 293
Size: 2890 Color: 255
Size: 156 Color: 21

Bin 115: 41 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 283
Size: 3382 Color: 265
Size: 172 Color: 28

Bin 116: 47 of cap free
Amount of items: 3
Items: 
Size: 4949 Color: 290
Size: 1721 Color: 215
Size: 1419 Color: 197

Bin 117: 56 of cap free
Amount of items: 2
Items: 
Size: 5028 Color: 292
Size: 3052 Color: 261

Bin 118: 59 of cap free
Amount of items: 2
Items: 
Size: 4686 Color: 287
Size: 3391 Color: 269

Bin 119: 59 of cap free
Amount of items: 3
Items: 
Size: 5371 Color: 302
Size: 2562 Color: 247
Size: 144 Color: 15

Bin 120: 84 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 281
Size: 3576 Color: 271
Size: 184 Color: 31

Bin 121: 86 of cap free
Amount of items: 19
Items: 
Size: 536 Color: 121
Size: 532 Color: 120
Size: 532 Color: 119
Size: 520 Color: 116
Size: 512 Color: 115
Size: 512 Color: 114
Size: 496 Color: 112
Size: 480 Color: 111
Size: 480 Color: 110
Size: 462 Color: 109
Size: 460 Color: 108
Size: 456 Color: 107
Size: 456 Color: 106
Size: 452 Color: 105
Size: 240 Color: 55
Size: 240 Color: 54
Size: 230 Color: 53
Size: 230 Color: 52
Size: 224 Color: 50

Bin 122: 86 of cap free
Amount of items: 2
Items: 
Size: 4660 Color: 286
Size: 3390 Color: 268

Bin 123: 91 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 298
Size: 2657 Color: 252
Size: 144 Color: 17

Bin 124: 92 of cap free
Amount of items: 4
Items: 
Size: 4150 Color: 280
Size: 3518 Color: 270
Size: 192 Color: 33
Size: 184 Color: 32

Bin 125: 101 of cap free
Amount of items: 2
Items: 
Size: 4646 Color: 285
Size: 3389 Color: 267

Bin 126: 129 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 297
Size: 2643 Color: 251
Size: 144 Color: 18

Bin 127: 130 of cap free
Amount of items: 3
Items: 
Size: 5478 Color: 308
Size: 2412 Color: 243
Size: 116 Color: 8

Bin 128: 138 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 282
Size: 3342 Color: 264
Size: 180 Color: 29

Bin 129: 164 of cap free
Amount of items: 7
Items: 
Size: 4070 Color: 274
Size: 764 Color: 145
Size: 760 Color: 144
Size: 732 Color: 142
Size: 728 Color: 141
Size: 710 Color: 139
Size: 208 Color: 41

Bin 130: 171 of cap free
Amount of items: 6
Items: 
Size: 4071 Color: 275
Size: 1120 Color: 174
Size: 924 Color: 158
Size: 828 Color: 151
Size: 814 Color: 150
Size: 208 Color: 39

Bin 131: 194 of cap free
Amount of items: 3
Items: 
Size: 4078 Color: 277
Size: 1942 Color: 224
Size: 1922 Color: 222

Bin 132: 220 of cap free
Amount of items: 23
Items: 
Size: 448 Color: 104
Size: 448 Color: 103
Size: 444 Color: 102
Size: 422 Color: 100
Size: 412 Color: 97
Size: 410 Color: 96
Size: 408 Color: 95
Size: 408 Color: 94
Size: 388 Color: 90
Size: 384 Color: 89
Size: 384 Color: 88
Size: 372 Color: 86
Size: 344 Color: 83
Size: 288 Color: 68
Size: 282 Color: 67
Size: 276 Color: 66
Size: 268 Color: 64
Size: 264 Color: 63
Size: 264 Color: 62
Size: 260 Color: 61
Size: 250 Color: 58
Size: 248 Color: 57
Size: 244 Color: 56

Bin 133: 5546 of cap free
Amount of items: 8
Items: 
Size: 340 Color: 80
Size: 336 Color: 79
Size: 336 Color: 78
Size: 336 Color: 77
Size: 328 Color: 76
Size: 314 Color: 74
Size: 304 Color: 72
Size: 296 Color: 70

Total size: 1073952
Total free space: 8136

