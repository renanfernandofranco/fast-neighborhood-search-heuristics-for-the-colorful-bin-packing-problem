Capicity Bin: 8016
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4498 Color: 3
Size: 3338 Color: 0
Size: 180 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5068 Color: 0
Size: 2374 Color: 3
Size: 574 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5215 Color: 3
Size: 2329 Color: 1
Size: 472 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5412 Color: 4
Size: 2468 Color: 3
Size: 136 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 3
Size: 2356 Color: 3
Size: 136 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 3
Size: 2172 Color: 3
Size: 144 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 1
Size: 1658 Color: 0
Size: 384 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6012 Color: 4
Size: 1804 Color: 1
Size: 200 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6220 Color: 1
Size: 1500 Color: 3
Size: 296 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6227 Color: 1
Size: 1491 Color: 2
Size: 298 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 0
Size: 1498 Color: 1
Size: 244 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6310 Color: 4
Size: 1522 Color: 4
Size: 184 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 1
Size: 1164 Color: 2
Size: 456 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 1
Size: 1339 Color: 0
Size: 266 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6446 Color: 4
Size: 1420 Color: 0
Size: 150 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6546 Color: 0
Size: 1282 Color: 2
Size: 188 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 4
Size: 800 Color: 4
Size: 692 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 3
Size: 1042 Color: 0
Size: 354 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6709 Color: 0
Size: 1091 Color: 2
Size: 216 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 0
Size: 914 Color: 4
Size: 340 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6766 Color: 0
Size: 666 Color: 4
Size: 584 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 3
Size: 876 Color: 0
Size: 368 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 1
Size: 1004 Color: 0
Size: 200 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 0
Size: 710 Color: 3
Size: 408 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6805 Color: 3
Size: 851 Color: 0
Size: 360 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 4
Size: 690 Color: 1
Size: 354 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 0
Size: 648 Color: 2
Size: 348 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6999 Color: 3
Size: 753 Color: 4
Size: 264 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7056 Color: 0
Size: 788 Color: 2
Size: 172 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7047 Color: 3
Size: 801 Color: 2
Size: 168 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 0
Size: 490 Color: 1
Size: 464 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 4
Size: 724 Color: 0
Size: 216 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7089 Color: 1
Size: 731 Color: 2
Size: 196 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 4
Size: 750 Color: 2
Size: 168 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 0
Size: 420 Color: 3
Size: 408 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 0
Size: 560 Color: 4
Size: 266 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 2
Size: 678 Color: 0
Size: 182 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7206 Color: 4
Size: 640 Color: 0
Size: 170 Color: 2

Bin 39: 1 of cap free
Amount of items: 4
Items: 
Size: 4012 Color: 4
Size: 2335 Color: 0
Size: 1220 Color: 2
Size: 448 Color: 3

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 4962 Color: 2
Size: 2873 Color: 0
Size: 180 Color: 3

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5885 Color: 3
Size: 2002 Color: 1
Size: 128 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5897 Color: 2
Size: 1758 Color: 1
Size: 360 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6138 Color: 1
Size: 1765 Color: 4
Size: 112 Color: 4

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 2
Size: 1191 Color: 0
Size: 482 Color: 3

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6591 Color: 1
Size: 1216 Color: 3
Size: 208 Color: 0

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6803 Color: 2
Size: 1212 Color: 4

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6885 Color: 4
Size: 664 Color: 0
Size: 466 Color: 2

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6919 Color: 1
Size: 836 Color: 0
Size: 260 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6935 Color: 4
Size: 572 Color: 0
Size: 508 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6995 Color: 1
Size: 924 Color: 4
Size: 96 Color: 4

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 7081 Color: 1
Size: 934 Color: 3

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 7166 Color: 1
Size: 849 Color: 2

Bin 53: 2 of cap free
Amount of items: 4
Items: 
Size: 4021 Color: 0
Size: 2339 Color: 3
Size: 1398 Color: 0
Size: 256 Color: 1

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 4502 Color: 2
Size: 3512 Color: 1

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5023 Color: 2
Size: 2415 Color: 0
Size: 576 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 5115 Color: 0
Size: 1853 Color: 2
Size: 1046 Color: 0

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 3
Size: 2546 Color: 0
Size: 328 Color: 1

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 5170 Color: 1
Size: 2844 Color: 2

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5196 Color: 1
Size: 2594 Color: 4
Size: 224 Color: 2

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5794 Color: 3
Size: 2084 Color: 1
Size: 136 Color: 3

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5965 Color: 4
Size: 1761 Color: 0
Size: 288 Color: 2

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 6447 Color: 4
Size: 1567 Color: 3

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 2
Size: 1226 Color: 3
Size: 136 Color: 0

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 6658 Color: 4
Size: 1356 Color: 3

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 7113 Color: 2
Size: 901 Color: 4

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 4472 Color: 4
Size: 3333 Color: 2
Size: 208 Color: 3

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 5211 Color: 4
Size: 2460 Color: 0
Size: 342 Color: 2

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 5398 Color: 1
Size: 2451 Color: 2
Size: 164 Color: 4

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5889 Color: 0
Size: 1772 Color: 0
Size: 352 Color: 4

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 5910 Color: 0
Size: 2011 Color: 4
Size: 92 Color: 1

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 4569 Color: 3
Size: 2867 Color: 0
Size: 576 Color: 1

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 4612 Color: 0
Size: 3260 Color: 4
Size: 140 Color: 1

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 5119 Color: 1
Size: 2685 Color: 4
Size: 208 Color: 2

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 5614 Color: 4
Size: 2118 Color: 1
Size: 280 Color: 2

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 5937 Color: 3
Size: 1309 Color: 3
Size: 766 Color: 1

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 6235 Color: 3
Size: 1777 Color: 2

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 7093 Color: 3
Size: 919 Color: 2

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 3
Size: 1189 Color: 2
Size: 970 Color: 0

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 6231 Color: 0
Size: 1676 Color: 0
Size: 104 Color: 1

Bin 80: 5 of cap free
Amount of items: 3
Items: 
Size: 6405 Color: 3
Size: 1124 Color: 3
Size: 482 Color: 0

Bin 81: 5 of cap free
Amount of items: 2
Items: 
Size: 6415 Color: 3
Size: 1596 Color: 1

Bin 82: 5 of cap free
Amount of items: 2
Items: 
Size: 6676 Color: 1
Size: 1335 Color: 2

Bin 83: 6 of cap free
Amount of items: 3
Items: 
Size: 4013 Color: 1
Size: 3331 Color: 4
Size: 666 Color: 2

Bin 84: 6 of cap free
Amount of items: 3
Items: 
Size: 4108 Color: 4
Size: 3342 Color: 0
Size: 560 Color: 1

Bin 85: 6 of cap free
Amount of items: 3
Items: 
Size: 5905 Color: 4
Size: 1801 Color: 0
Size: 304 Color: 2

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 6556 Color: 2
Size: 1454 Color: 3

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 7018 Color: 4
Size: 992 Color: 2

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 6915 Color: 3
Size: 1093 Color: 2

Bin 89: 9 of cap free
Amount of items: 5
Items: 
Size: 4010 Color: 0
Size: 1702 Color: 4
Size: 1186 Color: 4
Size: 781 Color: 2
Size: 328 Color: 0

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 5123 Color: 1
Size: 2884 Color: 3

Bin 91: 10 of cap free
Amount of items: 3
Items: 
Size: 5855 Color: 1
Size: 1485 Color: 0
Size: 666 Color: 3

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 6478 Color: 4
Size: 1528 Color: 3

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 7141 Color: 1
Size: 865 Color: 4

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 5029 Color: 1
Size: 2976 Color: 4

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 5071 Color: 1
Size: 2934 Color: 2

Bin 96: 11 of cap free
Amount of items: 3
Items: 
Size: 5892 Color: 0
Size: 1713 Color: 1
Size: 400 Color: 4

Bin 97: 11 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 3
Size: 1011 Color: 1
Size: 32 Color: 4

Bin 98: 12 of cap free
Amount of items: 3
Items: 
Size: 4017 Color: 1
Size: 2930 Color: 0
Size: 1057 Color: 3

Bin 99: 12 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 2
Size: 1096 Color: 3

Bin 100: 13 of cap free
Amount of items: 3
Items: 
Size: 5223 Color: 0
Size: 2404 Color: 2
Size: 376 Color: 3

Bin 101: 14 of cap free
Amount of items: 4
Items: 
Size: 4014 Color: 0
Size: 2804 Color: 1
Size: 834 Color: 3
Size: 350 Color: 4

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 5961 Color: 2
Size: 2041 Color: 4

Bin 103: 16 of cap free
Amount of items: 2
Items: 
Size: 7057 Color: 1
Size: 943 Color: 2

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 7118 Color: 2
Size: 882 Color: 4

Bin 105: 17 of cap free
Amount of items: 2
Items: 
Size: 6705 Color: 3
Size: 1294 Color: 1

Bin 106: 18 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 2
Size: 1356 Color: 4
Size: 48 Color: 3

Bin 107: 20 of cap free
Amount of items: 3
Items: 
Size: 5075 Color: 2
Size: 2455 Color: 3
Size: 466 Color: 0

Bin 108: 21 of cap free
Amount of items: 3
Items: 
Size: 5605 Color: 4
Size: 2182 Color: 0
Size: 208 Color: 1

Bin 109: 21 of cap free
Amount of items: 3
Items: 
Size: 6587 Color: 2
Size: 1350 Color: 4
Size: 58 Color: 0

Bin 110: 22 of cap free
Amount of items: 2
Items: 
Size: 6854 Color: 1
Size: 1140 Color: 4

Bin 111: 24 of cap free
Amount of items: 2
Items: 
Size: 4652 Color: 2
Size: 3340 Color: 3

Bin 112: 25 of cap free
Amount of items: 2
Items: 
Size: 7010 Color: 4
Size: 981 Color: 1

Bin 113: 29 of cap free
Amount of items: 4
Items: 
Size: 4906 Color: 2
Size: 2825 Color: 4
Size: 160 Color: 0
Size: 96 Color: 4

Bin 114: 30 of cap free
Amount of items: 2
Items: 
Size: 5567 Color: 4
Size: 2419 Color: 2

Bin 115: 35 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 2
Size: 1527 Color: 1
Size: 56 Color: 0

Bin 116: 39 of cap free
Amount of items: 2
Items: 
Size: 4009 Color: 3
Size: 3968 Color: 0

Bin 117: 42 of cap free
Amount of items: 2
Items: 
Size: 6316 Color: 4
Size: 1658 Color: 2

Bin 118: 42 of cap free
Amount of items: 2
Items: 
Size: 6408 Color: 4
Size: 1566 Color: 2

Bin 119: 43 of cap free
Amount of items: 2
Items: 
Size: 6839 Color: 2
Size: 1134 Color: 3

Bin 120: 47 of cap free
Amount of items: 2
Items: 
Size: 5478 Color: 2
Size: 2491 Color: 4

Bin 121: 47 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 4
Size: 1343 Color: 3

Bin 122: 51 of cap free
Amount of items: 2
Items: 
Size: 6254 Color: 1
Size: 1711 Color: 3

Bin 123: 54 of cap free
Amount of items: 2
Items: 
Size: 6030 Color: 3
Size: 1932 Color: 0

Bin 124: 54 of cap free
Amount of items: 2
Items: 
Size: 6108 Color: 0
Size: 1854 Color: 4

Bin 125: 58 of cap free
Amount of items: 2
Items: 
Size: 6185 Color: 0
Size: 1773 Color: 4

Bin 126: 66 of cap free
Amount of items: 2
Items: 
Size: 5092 Color: 2
Size: 2858 Color: 4

Bin 127: 70 of cap free
Amount of items: 2
Items: 
Size: 4978 Color: 1
Size: 2968 Color: 3

Bin 128: 71 of cap free
Amount of items: 12
Items: 
Size: 915 Color: 2
Size: 809 Color: 3
Size: 798 Color: 1
Size: 773 Color: 4
Size: 664 Color: 2
Size: 664 Color: 1
Size: 664 Color: 1
Size: 584 Color: 3
Size: 574 Color: 1
Size: 516 Color: 0
Size: 496 Color: 3
Size: 488 Color: 2

Bin 129: 78 of cap free
Amount of items: 27
Items: 
Size: 496 Color: 3
Size: 490 Color: 0
Size: 472 Color: 2
Size: 436 Color: 4
Size: 432 Color: 4
Size: 400 Color: 0
Size: 340 Color: 0
Size: 314 Color: 1
Size: 312 Color: 4
Size: 312 Color: 3
Size: 296 Color: 4
Size: 276 Color: 0
Size: 268 Color: 4
Size: 268 Color: 0
Size: 266 Color: 0
Size: 240 Color: 1
Size: 238 Color: 2
Size: 236 Color: 3
Size: 236 Color: 1
Size: 232 Color: 3
Size: 224 Color: 2
Size: 218 Color: 2
Size: 218 Color: 0
Size: 192 Color: 1
Size: 184 Color: 4
Size: 182 Color: 3
Size: 160 Color: 2

Bin 130: 102 of cap free
Amount of items: 2
Items: 
Size: 4577 Color: 2
Size: 3337 Color: 1

Bin 131: 110 of cap free
Amount of items: 3
Items: 
Size: 4565 Color: 0
Size: 2877 Color: 2
Size: 464 Color: 4

Bin 132: 119 of cap free
Amount of items: 2
Items: 
Size: 4556 Color: 1
Size: 3341 Color: 2

Bin 133: 6326 of cap free
Amount of items: 11
Items: 
Size: 168 Color: 4
Size: 160 Color: 1
Size: 158 Color: 2
Size: 156 Color: 3
Size: 156 Color: 2
Size: 154 Color: 4
Size: 154 Color: 1
Size: 152 Color: 4
Size: 152 Color: 3
Size: 148 Color: 3
Size: 132 Color: 2

Total size: 1058112
Total free space: 8016

