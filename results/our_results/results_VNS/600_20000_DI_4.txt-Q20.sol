Capicity Bin: 16432
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 10
Size: 6612 Color: 18
Size: 504 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10005 Color: 16
Size: 6077 Color: 10
Size: 350 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10218 Color: 3
Size: 5938 Color: 14
Size: 276 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10484 Color: 2
Size: 4924 Color: 12
Size: 1024 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10536 Color: 2
Size: 4920 Color: 18
Size: 976 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10725 Color: 17
Size: 5277 Color: 1
Size: 430 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10982 Color: 7
Size: 5012 Color: 16
Size: 438 Color: 8

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11292 Color: 5
Size: 4824 Color: 15
Size: 316 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11325 Color: 13
Size: 3733 Color: 18
Size: 1374 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11576 Color: 7
Size: 4056 Color: 11
Size: 800 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 7
Size: 3582 Color: 8
Size: 746 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 0
Size: 3748 Color: 13
Size: 484 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 17
Size: 3960 Color: 16
Size: 256 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 6
Size: 3528 Color: 6
Size: 448 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 7
Size: 3528 Color: 3
Size: 356 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12566 Color: 14
Size: 2418 Color: 1
Size: 1448 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12856 Color: 18
Size: 2216 Color: 1
Size: 1360 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12938 Color: 2
Size: 3218 Color: 8
Size: 276 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13012 Color: 18
Size: 2488 Color: 6
Size: 932 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13028 Color: 17
Size: 2852 Color: 19
Size: 552 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13138 Color: 4
Size: 2532 Color: 19
Size: 762 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13154 Color: 13
Size: 2734 Color: 12
Size: 544 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13333 Color: 8
Size: 2431 Color: 16
Size: 668 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 17
Size: 2568 Color: 6
Size: 496 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13420 Color: 10
Size: 2516 Color: 15
Size: 496 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13422 Color: 11
Size: 2778 Color: 19
Size: 232 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13440 Color: 3
Size: 2084 Color: 7
Size: 908 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13669 Color: 0
Size: 2303 Color: 5
Size: 460 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13699 Color: 14
Size: 2157 Color: 12
Size: 576 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13734 Color: 14
Size: 2284 Color: 18
Size: 414 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13784 Color: 3
Size: 2324 Color: 2
Size: 324 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13845 Color: 3
Size: 2143 Color: 7
Size: 444 Color: 9

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 19
Size: 2044 Color: 7
Size: 448 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13943 Color: 1
Size: 1689 Color: 14
Size: 800 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13983 Color: 1
Size: 2041 Color: 14
Size: 408 Color: 11

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14058 Color: 10
Size: 1886 Color: 4
Size: 488 Color: 5

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14025 Color: 5
Size: 2037 Color: 17
Size: 370 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 16
Size: 1982 Color: 18
Size: 400 Color: 12

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 5
Size: 2016 Color: 10
Size: 360 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14089 Color: 11
Size: 2043 Color: 4
Size: 300 Color: 19

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 14
Size: 1896 Color: 13
Size: 396 Color: 16

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14166 Color: 10
Size: 1442 Color: 17
Size: 824 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14188 Color: 6
Size: 1524 Color: 15
Size: 720 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14263 Color: 7
Size: 1511 Color: 17
Size: 658 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14267 Color: 0
Size: 1653 Color: 1
Size: 512 Color: 5

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14278 Color: 7
Size: 1650 Color: 16
Size: 504 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 10
Size: 1542 Color: 16
Size: 500 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14340 Color: 15
Size: 1612 Color: 6
Size: 480 Color: 18

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14398 Color: 5
Size: 1184 Color: 0
Size: 850 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14436 Color: 10
Size: 1280 Color: 5
Size: 716 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14449 Color: 7
Size: 1607 Color: 19
Size: 376 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 11
Size: 1652 Color: 10
Size: 328 Color: 19

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14485 Color: 7
Size: 1623 Color: 8
Size: 324 Color: 17

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14498 Color: 13
Size: 1198 Color: 16
Size: 736 Color: 16

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14548 Color: 10
Size: 1368 Color: 12
Size: 516 Color: 12

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 7
Size: 1560 Color: 4
Size: 304 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14574 Color: 4
Size: 1570 Color: 10
Size: 288 Color: 19

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14582 Color: 12
Size: 1498 Color: 9
Size: 352 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14605 Color: 19
Size: 1721 Color: 13
Size: 106 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 18
Size: 1368 Color: 10
Size: 452 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 15
Size: 1214 Color: 19
Size: 580 Color: 11

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14643 Color: 19
Size: 1491 Color: 1
Size: 298 Color: 19

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14654 Color: 17
Size: 1510 Color: 10
Size: 268 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 6
Size: 1360 Color: 9
Size: 376 Color: 12

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14702 Color: 11
Size: 992 Color: 13
Size: 738 Color: 10

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 9253 Color: 13
Size: 6842 Color: 10
Size: 336 Color: 7

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 10998 Color: 11
Size: 4873 Color: 4
Size: 560 Color: 17

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12009 Color: 19
Size: 3222 Color: 17
Size: 1200 Color: 18

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12419 Color: 1
Size: 3724 Color: 7
Size: 288 Color: 9

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 10
Size: 3283 Color: 8
Size: 644 Color: 14

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12477 Color: 2
Size: 3586 Color: 14
Size: 368 Color: 15

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12803 Color: 9
Size: 3284 Color: 4
Size: 344 Color: 1

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 12930 Color: 1
Size: 2813 Color: 18
Size: 688 Color: 16

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13073 Color: 10
Size: 3014 Color: 11
Size: 344 Color: 16

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 13404 Color: 7
Size: 3027 Color: 14

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13438 Color: 9
Size: 2801 Color: 15
Size: 192 Color: 3

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13503 Color: 14
Size: 1876 Color: 15
Size: 1052 Color: 6

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 13517 Color: 1
Size: 2524 Color: 17
Size: 390 Color: 10

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 13711 Color: 16
Size: 2556 Color: 17
Size: 164 Color: 5

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 13797 Color: 18
Size: 1598 Color: 10
Size: 1036 Color: 14

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 14221 Color: 0
Size: 1890 Color: 7
Size: 320 Color: 6

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 14385 Color: 10
Size: 1070 Color: 3
Size: 976 Color: 15

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 14557 Color: 11
Size: 1394 Color: 4
Size: 480 Color: 19

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 12574 Color: 11
Size: 3288 Color: 10
Size: 568 Color: 19

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12946 Color: 6
Size: 2844 Color: 14
Size: 640 Color: 11

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 10
Size: 2906 Color: 13
Size: 544 Color: 1

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 13826 Color: 19
Size: 2604 Color: 13

Bin 88: 2 of cap free
Amount of items: 2
Items: 
Size: 13918 Color: 4
Size: 2512 Color: 1

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 14454 Color: 17
Size: 1948 Color: 8
Size: 28 Color: 4

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 14466 Color: 5
Size: 1964 Color: 1

Bin 91: 2 of cap free
Amount of items: 2
Items: 
Size: 14660 Color: 1
Size: 1770 Color: 7

Bin 92: 2 of cap free
Amount of items: 2
Items: 
Size: 14762 Color: 7
Size: 1668 Color: 1

Bin 93: 2 of cap free
Amount of items: 2
Items: 
Size: 14774 Color: 13
Size: 1656 Color: 18

Bin 94: 3 of cap free
Amount of items: 4
Items: 
Size: 8219 Color: 12
Size: 4542 Color: 9
Size: 3252 Color: 15
Size: 416 Color: 19

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 8744 Color: 16
Size: 5997 Color: 3
Size: 1688 Color: 6

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 10101 Color: 16
Size: 6040 Color: 3
Size: 288 Color: 12

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 11940 Color: 2
Size: 4177 Color: 2
Size: 312 Color: 8

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 10
Size: 2317 Color: 0
Size: 1196 Color: 18

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 13861 Color: 5
Size: 2088 Color: 18
Size: 480 Color: 10

Bin 100: 3 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 17
Size: 2501 Color: 16

Bin 101: 3 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 15
Size: 2441 Color: 0

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 8
Size: 5932 Color: 1
Size: 392 Color: 5

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 10516 Color: 6
Size: 4888 Color: 18
Size: 1024 Color: 10

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 11324 Color: 7
Size: 4248 Color: 7
Size: 856 Color: 13

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 19
Size: 4010 Color: 2
Size: 518 Color: 13

Bin 106: 4 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 16
Size: 2884 Color: 2
Size: 320 Color: 10

Bin 107: 4 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 4
Size: 2592 Color: 15

Bin 108: 4 of cap free
Amount of items: 2
Items: 
Size: 14630 Color: 6
Size: 1798 Color: 9

Bin 109: 4 of cap free
Amount of items: 2
Items: 
Size: 14786 Color: 4
Size: 1642 Color: 13

Bin 110: 5 of cap free
Amount of items: 3
Items: 
Size: 10117 Color: 4
Size: 5942 Color: 2
Size: 368 Color: 14

Bin 111: 5 of cap free
Amount of items: 3
Items: 
Size: 10532 Color: 5
Size: 3721 Color: 15
Size: 2174 Color: 3

Bin 112: 5 of cap free
Amount of items: 3
Items: 
Size: 11421 Color: 10
Size: 4550 Color: 9
Size: 456 Color: 5

Bin 113: 5 of cap free
Amount of items: 2
Items: 
Size: 14622 Color: 18
Size: 1805 Color: 13

Bin 114: 6 of cap free
Amount of items: 3
Items: 
Size: 11610 Color: 7
Size: 3244 Color: 10
Size: 1572 Color: 15

Bin 115: 6 of cap free
Amount of items: 3
Items: 
Size: 14621 Color: 4
Size: 1741 Color: 6
Size: 64 Color: 5

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 13644 Color: 0
Size: 2781 Color: 11

Bin 117: 8 of cap free
Amount of items: 3
Items: 
Size: 11953 Color: 8
Size: 4131 Color: 16
Size: 340 Color: 10

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 14264 Color: 12
Size: 2160 Color: 11

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 14663 Color: 12
Size: 1761 Color: 2

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 14676 Color: 9
Size: 1748 Color: 13

Bin 121: 9 of cap free
Amount of items: 3
Items: 
Size: 9237 Color: 19
Size: 5684 Color: 3
Size: 1502 Color: 12

Bin 122: 9 of cap free
Amount of items: 3
Items: 
Size: 12493 Color: 2
Size: 2746 Color: 7
Size: 1184 Color: 10

Bin 123: 9 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 0
Size: 2767 Color: 3

Bin 124: 9 of cap free
Amount of items: 2
Items: 
Size: 14321 Color: 4
Size: 2102 Color: 7

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 11236 Color: 12
Size: 5186 Color: 7

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 12138 Color: 17
Size: 4284 Color: 2

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 13534 Color: 5
Size: 2888 Color: 2

Bin 128: 10 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 19
Size: 2680 Color: 16
Size: 42 Color: 0

Bin 129: 10 of cap free
Amount of items: 2
Items: 
Size: 14153 Color: 6
Size: 2269 Color: 4

Bin 130: 10 of cap free
Amount of items: 2
Items: 
Size: 14500 Color: 13
Size: 1922 Color: 2

Bin 131: 11 of cap free
Amount of items: 2
Items: 
Size: 9141 Color: 10
Size: 7280 Color: 16

Bin 132: 11 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 10
Size: 6845 Color: 12
Size: 240 Color: 9

Bin 133: 12 of cap free
Amount of items: 3
Items: 
Size: 9224 Color: 17
Size: 6844 Color: 14
Size: 352 Color: 1

Bin 134: 12 of cap free
Amount of items: 2
Items: 
Size: 9612 Color: 17
Size: 6808 Color: 4

Bin 135: 12 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 1
Size: 2914 Color: 14
Size: 42 Color: 15

Bin 136: 12 of cap free
Amount of items: 2
Items: 
Size: 13910 Color: 15
Size: 2510 Color: 3

Bin 137: 12 of cap free
Amount of items: 2
Items: 
Size: 14170 Color: 5
Size: 2250 Color: 18

Bin 138: 13 of cap free
Amount of items: 9
Items: 
Size: 8217 Color: 3
Size: 1344 Color: 15
Size: 1184 Color: 3
Size: 1144 Color: 9
Size: 1136 Color: 13
Size: 1054 Color: 1
Size: 908 Color: 8
Size: 720 Color: 0
Size: 712 Color: 12

Bin 139: 13 of cap free
Amount of items: 2
Items: 
Size: 13487 Color: 16
Size: 2932 Color: 8

Bin 140: 13 of cap free
Amount of items: 2
Items: 
Size: 14518 Color: 17
Size: 1901 Color: 2

Bin 141: 14 of cap free
Amount of items: 3
Items: 
Size: 9310 Color: 14
Size: 6836 Color: 13
Size: 272 Color: 7

Bin 142: 15 of cap free
Amount of items: 3
Items: 
Size: 10821 Color: 18
Size: 5168 Color: 15
Size: 428 Color: 12

Bin 143: 15 of cap free
Amount of items: 2
Items: 
Size: 13072 Color: 13
Size: 3345 Color: 5

Bin 144: 15 of cap free
Amount of items: 2
Items: 
Size: 14412 Color: 3
Size: 2005 Color: 17

Bin 145: 16 of cap free
Amount of items: 2
Items: 
Size: 10408 Color: 2
Size: 6008 Color: 1

Bin 146: 16 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 18
Size: 1816 Color: 6

Bin 147: 17 of cap free
Amount of items: 2
Items: 
Size: 11857 Color: 6
Size: 4558 Color: 19

Bin 148: 17 of cap free
Amount of items: 3
Items: 
Size: 12105 Color: 19
Size: 4022 Color: 14
Size: 288 Color: 10

Bin 149: 17 of cap free
Amount of items: 2
Items: 
Size: 12436 Color: 17
Size: 3979 Color: 3

Bin 150: 18 of cap free
Amount of items: 3
Items: 
Size: 12968 Color: 15
Size: 3320 Color: 16
Size: 126 Color: 11

Bin 151: 19 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 8
Size: 2597 Color: 17

Bin 152: 21 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 11
Size: 6847 Color: 0
Size: 240 Color: 8

Bin 153: 22 of cap free
Amount of items: 3
Items: 
Size: 11969 Color: 4
Size: 4257 Color: 3
Size: 184 Color: 11

Bin 154: 22 of cap free
Amount of items: 2
Items: 
Size: 13113 Color: 3
Size: 3297 Color: 15

Bin 155: 23 of cap free
Amount of items: 3
Items: 
Size: 10837 Color: 2
Size: 4964 Color: 13
Size: 608 Color: 10

Bin 156: 23 of cap free
Amount of items: 2
Items: 
Size: 11477 Color: 18
Size: 4932 Color: 7

Bin 157: 23 of cap free
Amount of items: 2
Items: 
Size: 12801 Color: 9
Size: 3608 Color: 18

Bin 158: 23 of cap free
Amount of items: 2
Items: 
Size: 14456 Color: 3
Size: 1953 Color: 6

Bin 159: 24 of cap free
Amount of items: 8
Items: 
Size: 8218 Color: 15
Size: 1468 Color: 17
Size: 1382 Color: 3
Size: 1368 Color: 12
Size: 1368 Color: 5
Size: 1368 Color: 4
Size: 908 Color: 0
Size: 328 Color: 1

Bin 160: 24 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 11
Size: 2098 Color: 12

Bin 161: 26 of cap free
Amount of items: 2
Items: 
Size: 11224 Color: 15
Size: 5182 Color: 18

Bin 162: 27 of cap free
Amount of items: 4
Items: 
Size: 8236 Color: 3
Size: 4530 Color: 4
Size: 2583 Color: 5
Size: 1056 Color: 7

Bin 163: 27 of cap free
Amount of items: 2
Items: 
Size: 13317 Color: 8
Size: 3088 Color: 7

Bin 164: 28 of cap free
Amount of items: 2
Items: 
Size: 14076 Color: 0
Size: 2328 Color: 12

Bin 165: 29 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 3
Size: 5983 Color: 8

Bin 166: 32 of cap free
Amount of items: 3
Items: 
Size: 11000 Color: 3
Size: 4344 Color: 10
Size: 1056 Color: 9

Bin 167: 32 of cap free
Amount of items: 2
Items: 
Size: 14168 Color: 18
Size: 2232 Color: 19

Bin 168: 32 of cap free
Amount of items: 2
Items: 
Size: 14408 Color: 15
Size: 1992 Color: 6

Bin 169: 33 of cap free
Amount of items: 2
Items: 
Size: 14073 Color: 3
Size: 2326 Color: 7

Bin 170: 35 of cap free
Amount of items: 2
Items: 
Size: 13372 Color: 7
Size: 3025 Color: 0

Bin 171: 36 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 2
Size: 3080 Color: 1

Bin 172: 36 of cap free
Amount of items: 2
Items: 
Size: 13642 Color: 19
Size: 2754 Color: 12

Bin 173: 38 of cap free
Amount of items: 7
Items: 
Size: 8222 Color: 10
Size: 1702 Color: 12
Size: 1684 Color: 19
Size: 1550 Color: 16
Size: 1528 Color: 13
Size: 1484 Color: 18
Size: 224 Color: 17

Bin 174: 38 of cap free
Amount of items: 2
Items: 
Size: 9548 Color: 2
Size: 6846 Color: 16

Bin 175: 38 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 3
Size: 3320 Color: 4
Size: 112 Color: 6

Bin 176: 39 of cap free
Amount of items: 2
Items: 
Size: 14407 Color: 9
Size: 1986 Color: 7

Bin 177: 40 of cap free
Amount of items: 3
Items: 
Size: 12500 Color: 18
Size: 3780 Color: 7
Size: 112 Color: 7

Bin 178: 40 of cap free
Amount of items: 2
Items: 
Size: 13470 Color: 13
Size: 2922 Color: 2

Bin 179: 42 of cap free
Amount of items: 2
Items: 
Size: 12130 Color: 17
Size: 4260 Color: 9

Bin 180: 43 of cap free
Amount of items: 2
Items: 
Size: 13057 Color: 14
Size: 3332 Color: 15

Bin 181: 44 of cap free
Amount of items: 2
Items: 
Size: 10648 Color: 6
Size: 5740 Color: 16

Bin 182: 48 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 13
Size: 6840 Color: 0
Size: 1264 Color: 15

Bin 183: 53 of cap free
Amount of items: 2
Items: 
Size: 11622 Color: 15
Size: 4757 Color: 1

Bin 184: 55 of cap free
Amount of items: 3
Items: 
Size: 10210 Color: 3
Size: 5263 Color: 5
Size: 904 Color: 10

Bin 185: 56 of cap free
Amount of items: 3
Items: 
Size: 10240 Color: 10
Size: 5144 Color: 8
Size: 992 Color: 12

Bin 186: 62 of cap free
Amount of items: 3
Items: 
Size: 9306 Color: 6
Size: 6408 Color: 13
Size: 656 Color: 0

Bin 187: 64 of cap free
Amount of items: 2
Items: 
Size: 11336 Color: 7
Size: 5032 Color: 4

Bin 188: 81 of cap free
Amount of items: 2
Items: 
Size: 11688 Color: 5
Size: 4663 Color: 1

Bin 189: 81 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 2
Size: 3607 Color: 17

Bin 190: 84 of cap free
Amount of items: 2
Items: 
Size: 8220 Color: 4
Size: 8128 Color: 7

Bin 191: 87 of cap free
Amount of items: 2
Items: 
Size: 12532 Color: 18
Size: 3813 Color: 19

Bin 192: 101 of cap free
Amount of items: 2
Items: 
Size: 10974 Color: 16
Size: 5357 Color: 13

Bin 193: 104 of cap free
Amount of items: 2
Items: 
Size: 10264 Color: 7
Size: 6064 Color: 6

Bin 194: 112 of cap free
Amount of items: 20
Items: 
Size: 1040 Color: 17
Size: 1032 Color: 17
Size: 1000 Color: 10
Size: 984 Color: 19
Size: 984 Color: 14
Size: 960 Color: 15
Size: 950 Color: 1
Size: 864 Color: 6
Size: 864 Color: 4
Size: 848 Color: 13
Size: 848 Color: 12
Size: 834 Color: 13
Size: 816 Color: 12
Size: 800 Color: 10
Size: 784 Color: 16
Size: 648 Color: 8
Size: 580 Color: 9
Size: 558 Color: 8
Size: 548 Color: 0
Size: 378 Color: 4

Bin 195: 128 of cap free
Amount of items: 2
Items: 
Size: 11972 Color: 16
Size: 4332 Color: 5

Bin 196: 143 of cap free
Amount of items: 6
Items: 
Size: 8232 Color: 2
Size: 2197 Color: 14
Size: 2075 Color: 1
Size: 1843 Color: 10
Size: 1614 Color: 0
Size: 328 Color: 8

Bin 197: 146 of cap free
Amount of items: 31
Items: 
Size: 752 Color: 12
Size: 744 Color: 1
Size: 742 Color: 13
Size: 704 Color: 2
Size: 664 Color: 19
Size: 656 Color: 17
Size: 648 Color: 14
Size: 640 Color: 6
Size: 640 Color: 2
Size: 604 Color: 12
Size: 604 Color: 3
Size: 584 Color: 3
Size: 580 Color: 18
Size: 568 Color: 10
Size: 562 Color: 5
Size: 528 Color: 8
Size: 496 Color: 7
Size: 464 Color: 9
Size: 464 Color: 1
Size: 448 Color: 0
Size: 432 Color: 17
Size: 432 Color: 0
Size: 420 Color: 13
Size: 416 Color: 2
Size: 408 Color: 9
Size: 392 Color: 18
Size: 384 Color: 11
Size: 336 Color: 6
Size: 336 Color: 4
Size: 330 Color: 4
Size: 308 Color: 8

Bin 198: 178 of cap free
Amount of items: 2
Items: 
Size: 10966 Color: 8
Size: 5288 Color: 19

Bin 199: 13348 of cap free
Amount of items: 10
Items: 
Size: 384 Color: 0
Size: 368 Color: 16
Size: 320 Color: 5
Size: 308 Color: 9
Size: 304 Color: 4
Size: 300 Color: 8
Size: 300 Color: 1
Size: 296 Color: 14
Size: 296 Color: 6
Size: 208 Color: 15

Total size: 3253536
Total free space: 16432

