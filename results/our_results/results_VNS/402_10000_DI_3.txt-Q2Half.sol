Capicity Bin: 7360
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5085 Color: 1
Size: 1897 Color: 1
Size: 378 Color: 0

Bin 2: 0 of cap free
Amount of items: 6
Items: 
Size: 4872 Color: 1
Size: 856 Color: 1
Size: 780 Color: 1
Size: 478 Color: 0
Size: 192 Color: 0
Size: 182 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6366 Color: 1
Size: 866 Color: 1
Size: 128 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6616 Color: 1
Size: 480 Color: 0
Size: 264 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5748 Color: 1
Size: 1348 Color: 1
Size: 264 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5448 Color: 1
Size: 1704 Color: 1
Size: 208 Color: 0

Bin 7: 0 of cap free
Amount of items: 5
Items: 
Size: 3688 Color: 1
Size: 2124 Color: 1
Size: 1252 Color: 1
Size: 156 Color: 0
Size: 140 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5320 Color: 1
Size: 1696 Color: 1
Size: 344 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 1
Size: 432 Color: 1
Size: 338 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5897 Color: 1
Size: 1221 Color: 1
Size: 242 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5176 Color: 1
Size: 1944 Color: 1
Size: 240 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6456 Color: 1
Size: 792 Color: 1
Size: 112 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5470 Color: 1
Size: 1386 Color: 1
Size: 504 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6540 Color: 1
Size: 684 Color: 1
Size: 136 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4794 Color: 1
Size: 2238 Color: 1
Size: 328 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6486 Color: 1
Size: 730 Color: 1
Size: 144 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5880 Color: 1
Size: 1320 Color: 1
Size: 160 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6020 Color: 1
Size: 1036 Color: 1
Size: 304 Color: 0

Bin 19: 0 of cap free
Amount of items: 4
Items: 
Size: 6021 Color: 1
Size: 843 Color: 1
Size: 368 Color: 0
Size: 128 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4608 Color: 1
Size: 2568 Color: 1
Size: 184 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 1
Size: 991 Color: 1
Size: 110 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 1
Size: 1336 Color: 1
Size: 148 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 1
Size: 632 Color: 1
Size: 304 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6275 Color: 1
Size: 905 Color: 1
Size: 180 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4123 Color: 1
Size: 2699 Color: 1
Size: 538 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5337 Color: 1
Size: 1207 Color: 1
Size: 816 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5418 Color: 1
Size: 1622 Color: 1
Size: 320 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5032 Color: 1
Size: 1716 Color: 1
Size: 612 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4505 Color: 1
Size: 2381 Color: 1
Size: 474 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 1
Size: 942 Color: 1
Size: 272 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5308 Color: 1
Size: 1516 Color: 1
Size: 536 Color: 0

Bin 32: 0 of cap free
Amount of items: 5
Items: 
Size: 3962 Color: 1
Size: 1578 Color: 1
Size: 1272 Color: 1
Size: 352 Color: 0
Size: 196 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5142 Color: 1
Size: 1850 Color: 1
Size: 368 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5101 Color: 1
Size: 1883 Color: 1
Size: 376 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5944 Color: 1
Size: 1080 Color: 1
Size: 336 Color: 0

Bin 36: 0 of cap free
Amount of items: 4
Items: 
Size: 6130 Color: 1
Size: 830 Color: 1
Size: 208 Color: 0
Size: 192 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 1
Size: 2712 Color: 1
Size: 308 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 3690 Color: 1
Size: 3378 Color: 1
Size: 292 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 3928 Color: 1
Size: 2872 Color: 1
Size: 560 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 1
Size: 948 Color: 1
Size: 264 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5848 Color: 1
Size: 984 Color: 1
Size: 528 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 1
Size: 1136 Color: 1
Size: 152 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6007 Color: 1
Size: 1117 Color: 1
Size: 236 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 4120 Color: 1
Size: 3064 Color: 1
Size: 176 Color: 0

Bin 45: 0 of cap free
Amount of items: 5
Items: 
Size: 5292 Color: 1
Size: 1102 Color: 1
Size: 758 Color: 1
Size: 176 Color: 0
Size: 32 Color: 0

Bin 46: 0 of cap free
Amount of items: 5
Items: 
Size: 4521 Color: 1
Size: 1551 Color: 1
Size: 964 Color: 1
Size: 192 Color: 0
Size: 132 Color: 0

Bin 47: 0 of cap free
Amount of items: 5
Items: 
Size: 3683 Color: 1
Size: 1836 Color: 1
Size: 1437 Color: 1
Size: 256 Color: 0
Size: 148 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 3681 Color: 1
Size: 3067 Color: 1
Size: 612 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 1
Size: 1852 Color: 1
Size: 232 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 1
Size: 935 Color: 1
Size: 142 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4680 Color: 1
Size: 2496 Color: 1
Size: 184 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 1
Size: 760 Color: 1
Size: 416 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 4196 Color: 1
Size: 2644 Color: 1
Size: 520 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6531 Color: 1
Size: 691 Color: 1
Size: 138 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5934 Color: 1
Size: 954 Color: 1
Size: 472 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 1
Size: 1536 Color: 1
Size: 312 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 1
Size: 2532 Color: 1
Size: 276 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6200 Color: 1
Size: 888 Color: 1
Size: 272 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 1
Size: 1496 Color: 1
Size: 204 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 1
Size: 695 Color: 1
Size: 492 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 3692 Color: 1
Size: 3060 Color: 1
Size: 608 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6459 Color: 1
Size: 869 Color: 1
Size: 32 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4308 Color: 1
Size: 2828 Color: 1
Size: 224 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 1
Size: 1608 Color: 1
Size: 204 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6239 Color: 1
Size: 785 Color: 1
Size: 336 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6401 Color: 1
Size: 799 Color: 1
Size: 160 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 1
Size: 1374 Color: 1
Size: 272 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5913 Color: 1
Size: 1103 Color: 1
Size: 344 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4489 Color: 1
Size: 2367 Color: 1
Size: 504 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 1
Size: 771 Color: 1
Size: 154 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 1
Size: 968 Color: 1
Size: 158 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 4194 Color: 1
Size: 2642 Color: 1
Size: 524 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 3684 Color: 1
Size: 3068 Color: 1
Size: 608 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5607 Color: 1
Size: 1331 Color: 1
Size: 422 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 3682 Color: 1
Size: 3066 Color: 1
Size: 612 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6157 Color: 1
Size: 1081 Color: 1
Size: 122 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5768 Color: 1
Size: 1544 Color: 1
Size: 48 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6483 Color: 1
Size: 731 Color: 1
Size: 146 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 5653 Color: 1
Size: 1423 Color: 1
Size: 284 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 4844 Color: 1
Size: 2100 Color: 1
Size: 416 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5082 Color: 1
Size: 1902 Color: 1
Size: 376 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5071 Color: 1
Size: 1909 Color: 1
Size: 380 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 4436 Color: 1
Size: 2504 Color: 1
Size: 420 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 1
Size: 971 Color: 1
Size: 264 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5305 Color: 1
Size: 1713 Color: 1
Size: 342 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5576 Color: 1
Size: 1488 Color: 1
Size: 296 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4820 Color: 1
Size: 1724 Color: 1
Size: 816 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5765 Color: 1
Size: 1031 Color: 1
Size: 564 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6085 Color: 1
Size: 1063 Color: 1
Size: 212 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 1
Size: 742 Color: 1
Size: 164 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5950 Color: 1
Size: 1190 Color: 1
Size: 220 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 4682 Color: 1
Size: 2234 Color: 1
Size: 444 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6228 Color: 1
Size: 940 Color: 1
Size: 192 Color: 0

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 6372 Color: 1
Size: 801 Color: 1
Size: 186 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 5767 Color: 1
Size: 1192 Color: 1
Size: 400 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 6470 Color: 1
Size: 665 Color: 1
Size: 224 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 5088 Color: 1
Size: 2103 Color: 1
Size: 168 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 1
Size: 1461 Color: 1
Size: 200 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 4821 Color: 1
Size: 2482 Color: 1
Size: 56 Color: 0

Bin 100: 1 of cap free
Amount of items: 5
Items: 
Size: 4546 Color: 1
Size: 1329 Color: 1
Size: 1028 Color: 1
Size: 240 Color: 0
Size: 216 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 6296 Color: 1
Size: 919 Color: 1
Size: 144 Color: 0

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 4386 Color: 1
Size: 2685 Color: 1
Size: 288 Color: 0

Bin 103: 1 of cap free
Amount of items: 3
Items: 
Size: 6037 Color: 1
Size: 1178 Color: 1
Size: 144 Color: 0

Bin 104: 1 of cap free
Amount of items: 5
Items: 
Size: 2548 Color: 1
Size: 2117 Color: 1
Size: 2088 Color: 1
Size: 384 Color: 0
Size: 222 Color: 0

Bin 105: 2 of cap free
Amount of items: 3
Items: 
Size: 6218 Color: 1
Size: 1124 Color: 1
Size: 16 Color: 0

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 6349 Color: 1
Size: 705 Color: 1
Size: 304 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 4837 Color: 1
Size: 2393 Color: 1
Size: 128 Color: 0

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 6419 Color: 1
Size: 699 Color: 1
Size: 240 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 5321 Color: 1
Size: 1701 Color: 1
Size: 336 Color: 0

Bin 110: 2 of cap free
Amount of items: 3
Items: 
Size: 6197 Color: 1
Size: 1129 Color: 1
Size: 32 Color: 0

Bin 111: 4 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 1
Size: 1832 Color: 1
Size: 360 Color: 0

Bin 112: 5 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 1
Size: 642 Color: 1
Size: 158 Color: 0

Bin 113: 6 of cap free
Amount of items: 3
Items: 
Size: 6523 Color: 1
Size: 719 Color: 1
Size: 112 Color: 0

Bin 114: 6 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 1
Size: 1026 Color: 1
Size: 468 Color: 0

Bin 115: 6 of cap free
Amount of items: 3
Items: 
Size: 4280 Color: 1
Size: 2834 Color: 1
Size: 240 Color: 0

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 5501 Color: 1
Size: 1740 Color: 1
Size: 112 Color: 0

Bin 117: 14 of cap free
Amount of items: 3
Items: 
Size: 2752 Color: 0
Size: 2346 Color: 1
Size: 2248 Color: 1

Bin 118: 15 of cap free
Amount of items: 3
Items: 
Size: 6499 Color: 1
Size: 654 Color: 1
Size: 192 Color: 0

Bin 119: 16 of cap free
Amount of items: 3
Items: 
Size: 4121 Color: 1
Size: 2975 Color: 1
Size: 248 Color: 0

Bin 120: 18 of cap free
Amount of items: 3
Items: 
Size: 4139 Color: 1
Size: 3065 Color: 1
Size: 138 Color: 0

Bin 121: 19 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 1
Size: 917 Color: 1
Size: 292 Color: 0

Bin 122: 21 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 1
Size: 1687 Color: 1
Size: 512 Color: 0

Bin 123: 28 of cap free
Amount of items: 3
Items: 
Size: 5584 Color: 1
Size: 1460 Color: 1
Size: 288 Color: 0

Bin 124: 174 of cap free
Amount of items: 2
Items: 
Size: 6578 Color: 1
Size: 608 Color: 0

Bin 125: 193 of cap free
Amount of items: 3
Items: 
Size: 5637 Color: 1
Size: 1244 Color: 1
Size: 286 Color: 0

Bin 126: 404 of cap free
Amount of items: 3
Items: 
Size: 4324 Color: 1
Size: 2444 Color: 1
Size: 188 Color: 0

Bin 127: 797 of cap free
Amount of items: 1
Items: 
Size: 6563 Color: 1

Bin 128: 845 of cap free
Amount of items: 1
Items: 
Size: 6515 Color: 1

Bin 129: 872 of cap free
Amount of items: 1
Items: 
Size: 6488 Color: 1

Bin 130: 932 of cap free
Amount of items: 1
Items: 
Size: 6428 Color: 1

Bin 131: 957 of cap free
Amount of items: 1
Items: 
Size: 6403 Color: 1

Bin 132: 978 of cap free
Amount of items: 1
Items: 
Size: 6382 Color: 1

Bin 133: 1020 of cap free
Amount of items: 1
Items: 
Size: 6340 Color: 1

Total size: 971520
Total free space: 7360

