Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2001

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 386634 Color: 1538
Size: 355361 Color: 1342
Size: 258006 Color: 238

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 376205 Color: 1476
Size: 356459 Color: 1350
Size: 267337 Color: 397

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 399969 Color: 1611
Size: 348327 Color: 1278
Size: 251705 Color: 66

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 499687 Color: 1999
Size: 250186 Color: 8
Size: 250128 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 395599 Color: 1583
Size: 354337 Color: 1333
Size: 250065 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 404364 Color: 1627
Size: 339920 Color: 1212
Size: 255717 Color: 178

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 408348 Color: 1645
Size: 312775 Color: 964
Size: 278878 Color: 576

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 400603 Color: 1613
Size: 333205 Color: 1137
Size: 266193 Color: 376

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 399650 Color: 1610
Size: 341528 Color: 1226
Size: 258823 Color: 261

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 401142 Color: 1616
Size: 340560 Color: 1215
Size: 258299 Color: 247

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 389118 Color: 1557
Size: 348851 Color: 1285
Size: 262032 Color: 318

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 388388 Color: 1551
Size: 359091 Color: 1367
Size: 252522 Color: 93

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 409825 Color: 1656
Size: 339125 Color: 1203
Size: 251051 Color: 42

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 399242 Color: 1604
Size: 342669 Color: 1241
Size: 258090 Color: 242

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 408872 Color: 1648
Size: 329715 Color: 1116
Size: 261414 Color: 309

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 374415 Color: 1468
Size: 357788 Color: 1358
Size: 267798 Color: 404

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 369813 Color: 1441
Size: 365322 Color: 1403
Size: 264866 Color: 361

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 373605 Color: 1466
Size: 355293 Color: 1341
Size: 271103 Color: 461

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 356413 Color: 1349
Size: 355759 Color: 1343
Size: 287829 Color: 692

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375367 Color: 1472
Size: 360909 Color: 1386
Size: 263725 Color: 344

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 408550 Color: 1646
Size: 335442 Color: 1165
Size: 256009 Color: 186

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 409899 Color: 1657
Size: 298014 Color: 822
Size: 292088 Color: 751

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 407580 Color: 1641
Size: 322229 Color: 1061
Size: 270192 Color: 443

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 375995 Color: 1474
Size: 355964 Color: 1345
Size: 268042 Color: 410

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 399393 Color: 1607
Size: 335344 Color: 1163
Size: 265264 Color: 363

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 366534 Color: 1414
Size: 318495 Color: 1017
Size: 314972 Color: 985

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 339859 Color: 1211
Size: 339616 Color: 1208
Size: 320526 Color: 1042

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 398979 Color: 1601
Size: 331824 Color: 1131
Size: 269198 Color: 429

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 362792 Color: 1394
Size: 345949 Color: 1264
Size: 291260 Color: 741

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 383802 Color: 1530
Size: 335597 Color: 1169
Size: 280602 Color: 589

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 392745 Color: 1572
Size: 348621 Color: 1281
Size: 258635 Color: 253

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 376497 Color: 1479
Size: 363199 Color: 1397
Size: 260305 Color: 294

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 403161 Color: 1623
Size: 329710 Color: 1115
Size: 267130 Color: 391

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 396764 Color: 1592
Size: 329425 Color: 1113
Size: 273812 Color: 510

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 366129 Color: 1410
Size: 350092 Color: 1294
Size: 283780 Color: 639

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 408860 Color: 1647
Size: 338639 Color: 1201
Size: 252502 Color: 92

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 368539 Color: 1430
Size: 334484 Color: 1150
Size: 296978 Color: 805

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 394876 Color: 1580
Size: 319324 Color: 1028
Size: 285801 Color: 666

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 373240 Color: 1462
Size: 329956 Color: 1118
Size: 296805 Color: 801

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 388288 Color: 1550
Size: 328117 Color: 1103
Size: 283596 Color: 635

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 381003 Color: 1510
Size: 336340 Color: 1178
Size: 282658 Color: 620

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 393012 Color: 1576
Size: 337370 Color: 1187
Size: 269619 Color: 435

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 393092 Color: 1577
Size: 335480 Color: 1167
Size: 271429 Color: 467

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 370551 Color: 1448
Size: 319031 Color: 1023
Size: 310419 Color: 941

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 393194 Color: 1578
Size: 313985 Color: 974
Size: 292822 Color: 756

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 367951 Color: 1422
Size: 342980 Color: 1245
Size: 289070 Color: 710

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 390450 Color: 1562
Size: 356275 Color: 1348
Size: 253276 Color: 112

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 390012 Color: 1559
Size: 343507 Color: 1249
Size: 266482 Color: 381

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 365484 Color: 1406
Size: 350941 Color: 1306
Size: 283576 Color: 634

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 400854 Color: 1615
Size: 341564 Color: 1228
Size: 257583 Color: 226

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 376368 Color: 1478
Size: 334797 Color: 1155
Size: 288836 Color: 708

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 398706 Color: 1600
Size: 348829 Color: 1284
Size: 252466 Color: 90

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 386894 Color: 1540
Size: 321512 Color: 1054
Size: 291595 Color: 744

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 347183 Color: 1271
Size: 337646 Color: 1191
Size: 315172 Color: 990

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 376687 Color: 1482
Size: 334747 Color: 1153
Size: 288567 Color: 704

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 377543 Color: 1488
Size: 347940 Color: 1274
Size: 274518 Color: 524

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 392869 Color: 1575
Size: 352436 Color: 1316
Size: 254696 Color: 151

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 387142 Color: 1542
Size: 349177 Color: 1290
Size: 263682 Color: 341

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 384058 Color: 1531
Size: 335128 Color: 1157
Size: 280815 Color: 594

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 410730 Color: 1661
Size: 334050 Color: 1143
Size: 255221 Color: 161

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 383660 Color: 1527
Size: 319731 Color: 1036
Size: 296610 Color: 797

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 377703 Color: 1489
Size: 319130 Color: 1024
Size: 303168 Color: 872

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 354069 Color: 1331
Size: 351078 Color: 1308
Size: 294854 Color: 776

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 368408 Color: 1429
Size: 348282 Color: 1277
Size: 283311 Color: 632

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 384779 Color: 1534
Size: 360913 Color: 1387
Size: 254309 Color: 138

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 379337 Color: 1498
Size: 336971 Color: 1182
Size: 283693 Color: 637

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 383285 Color: 1526
Size: 350850 Color: 1305
Size: 265866 Color: 373

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 408111 Color: 1643
Size: 304212 Color: 884
Size: 287678 Color: 691

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 368385 Color: 1428
Size: 348540 Color: 1279
Size: 283076 Color: 627

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 342514 Color: 1239
Size: 338506 Color: 1200
Size: 318981 Color: 1020

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 380974 Color: 1509
Size: 319467 Color: 1030
Size: 299560 Color: 837

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 381469 Color: 1517
Size: 318989 Color: 1021
Size: 299543 Color: 836

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 380002 Color: 1504
Size: 343442 Color: 1248
Size: 276557 Color: 546

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 378729 Color: 1495
Size: 336231 Color: 1176
Size: 285041 Color: 655

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 412463 Color: 1667
Size: 335980 Color: 1174
Size: 251558 Color: 60

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 377066 Color: 1485
Size: 360701 Color: 1385
Size: 262234 Color: 322

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 366754 Color: 1415
Size: 360220 Color: 1377
Size: 273027 Color: 499

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 368251 Color: 1427
Size: 343177 Color: 1247
Size: 288573 Color: 705

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 381210 Color: 1513
Size: 367375 Color: 1420
Size: 251416 Color: 55

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 404985 Color: 1630
Size: 339369 Color: 1207
Size: 255647 Color: 175

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 387805 Color: 1545
Size: 312246 Color: 960
Size: 299950 Color: 840

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 377077 Color: 1486
Size: 314856 Color: 983
Size: 308068 Color: 919

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 390436 Color: 1561
Size: 348583 Color: 1280
Size: 260982 Color: 304

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 366256 Color: 1412
Size: 358989 Color: 1366
Size: 274756 Color: 529

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 376009 Color: 1475
Size: 327125 Color: 1097
Size: 296867 Color: 804

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 378495 Color: 1494
Size: 326470 Color: 1094
Size: 295036 Color: 779

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 379407 Color: 1500
Size: 367459 Color: 1421
Size: 253135 Color: 109

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 361213 Color: 1389
Size: 360633 Color: 1383
Size: 278155 Color: 567

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 380562 Color: 1507
Size: 332521 Color: 1134
Size: 286918 Color: 679

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 369687 Color: 1438
Size: 336332 Color: 1177
Size: 293982 Color: 769

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 404935 Color: 1629
Size: 309140 Color: 931
Size: 285926 Color: 669

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 388176 Color: 1548
Size: 341277 Color: 1224
Size: 270548 Color: 448

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 398170 Color: 1599
Size: 306611 Color: 900
Size: 295220 Color: 781

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 381712 Color: 1519
Size: 344916 Color: 1259
Size: 273373 Color: 505

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 388720 Color: 1553
Size: 351257 Color: 1309
Size: 260024 Color: 287

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 369747 Color: 1439
Size: 355764 Color: 1344
Size: 274490 Color: 523

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 370147 Color: 1445
Size: 321337 Color: 1051
Size: 308517 Color: 922

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 405170 Color: 1631
Size: 334073 Color: 1145
Size: 260758 Color: 302

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 381094 Color: 1511
Size: 338251 Color: 1194
Size: 280656 Color: 591

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 367131 Color: 1419
Size: 341818 Color: 1232
Size: 291052 Color: 735

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 382496 Color: 1522
Size: 347715 Color: 1273
Size: 269790 Color: 437

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 405520 Color: 1633
Size: 325093 Color: 1081
Size: 269388 Color: 432

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 393497 Color: 1579
Size: 307606 Color: 910
Size: 298898 Color: 827

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 389859 Color: 1558
Size: 343954 Color: 1251
Size: 266188 Color: 375

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 377829 Color: 1492
Size: 368133 Color: 1426
Size: 254039 Color: 132

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 408293 Color: 1644
Size: 337207 Color: 1184
Size: 254501 Color: 144

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 386776 Color: 1539
Size: 317527 Color: 1007
Size: 295698 Color: 784

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 366897 Color: 1417
Size: 328927 Color: 1108
Size: 304177 Color: 883

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 391684 Color: 1567
Size: 321771 Color: 1056
Size: 286546 Color: 676

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 356197 Color: 1347
Size: 349660 Color: 1293
Size: 294144 Color: 772

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 365622 Color: 1407
Size: 332231 Color: 1132
Size: 302148 Color: 862

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 400286 Color: 1612
Size: 331140 Color: 1126
Size: 268575 Color: 417

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 376661 Color: 1480
Size: 350380 Color: 1297
Size: 272960 Color: 497

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 379101 Color: 1497
Size: 363267 Color: 1398
Size: 257633 Color: 229

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 391194 Color: 1566
Size: 334253 Color: 1147
Size: 274554 Color: 527

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 360067 Color: 1375
Size: 354240 Color: 1332
Size: 285694 Color: 663

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 373110 Color: 1461
Size: 372842 Color: 1459
Size: 254049 Color: 133

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 388217 Color: 1549
Size: 352577 Color: 1320
Size: 259207 Color: 275

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 358966 Color: 1365
Size: 341796 Color: 1231
Size: 299239 Color: 835

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 410663 Color: 1660
Size: 302709 Color: 867
Size: 286629 Color: 677

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 366863 Color: 1416
Size: 351522 Color: 1313
Size: 281616 Color: 608

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 388499 Color: 1552
Size: 340984 Color: 1220
Size: 270518 Color: 445

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 379786 Color: 1502
Size: 363687 Color: 1399
Size: 256528 Color: 201

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 357438 Color: 1355
Size: 355020 Color: 1338
Size: 287543 Color: 690

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 387239 Color: 1543
Size: 355055 Color: 1339
Size: 257707 Color: 231

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 409128 Color: 1651
Size: 304344 Color: 885
Size: 286529 Color: 675

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 497513 Color: 1988
Size: 252310 Color: 83
Size: 250178 Color: 7

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 381219 Color: 1514
Size: 328302 Color: 1105
Size: 290480 Color: 725

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 399289 Color: 1605
Size: 340655 Color: 1216
Size: 260057 Color: 289

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 358078 Color: 1360
Size: 327913 Color: 1100
Size: 314010 Color: 975

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 406149 Color: 1637
Size: 315078 Color: 987
Size: 278774 Color: 575

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 354560 Color: 1335
Size: 353716 Color: 1328
Size: 291725 Color: 747

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 392789 Color: 1573
Size: 352659 Color: 1321
Size: 254553 Color: 148

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 392609 Color: 1571
Size: 349655 Color: 1292
Size: 257737 Color: 232

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 355980 Color: 1346
Size: 352783 Color: 1323
Size: 291238 Color: 740

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 372392 Color: 1457
Size: 352811 Color: 1324
Size: 274798 Color: 531

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 395895 Color: 1585
Size: 325967 Color: 1089
Size: 278139 Color: 566

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 342013 Color: 1233
Size: 335175 Color: 1158
Size: 322813 Color: 1065

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 406223 Color: 1638
Size: 299905 Color: 839
Size: 293873 Color: 768

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 384061 Color: 1532
Size: 328972 Color: 1109
Size: 286968 Color: 681

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 382830 Color: 1523
Size: 353621 Color: 1327
Size: 263550 Color: 340

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 346634 Color: 1268
Size: 345999 Color: 1265
Size: 307368 Color: 906

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 345170 Color: 1260
Size: 344751 Color: 1258
Size: 310080 Color: 938

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 374974 Color: 1471
Size: 371223 Color: 1450
Size: 253804 Color: 124

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 376909 Color: 1484
Size: 342077 Color: 1236
Size: 281015 Color: 601

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 347950 Color: 1275
Size: 339815 Color: 1210
Size: 312236 Color: 959

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 350967 Color: 1307
Size: 341187 Color: 1222
Size: 307847 Color: 914

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 347705 Color: 1272
Size: 345600 Color: 1263
Size: 306696 Color: 901

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 402894 Color: 1622
Size: 328319 Color: 1106
Size: 268788 Color: 421

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 361156 Color: 1388
Size: 359413 Color: 1371
Size: 279432 Color: 583

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 350426 Color: 1298
Size: 334485 Color: 1151
Size: 315090 Color: 988

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 359619 Color: 1372
Size: 358320 Color: 1362
Size: 282062 Color: 613

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 405486 Color: 1632
Size: 342827 Color: 1242
Size: 251688 Color: 65

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 362403 Color: 1392
Size: 324759 Color: 1078
Size: 312839 Color: 965

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 388069 Color: 1547
Size: 342249 Color: 1238
Size: 269683 Color: 436

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 352438 Color: 1317
Size: 333447 Color: 1140
Size: 314116 Color: 976

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 411973 Color: 1666
Size: 315062 Color: 986
Size: 272966 Color: 498

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 373462 Color: 1465
Size: 344534 Color: 1253
Size: 282005 Color: 612

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 342234 Color: 1237
Size: 341532 Color: 1227
Size: 316235 Color: 999

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 341253 Color: 1223
Size: 340497 Color: 1214
Size: 318251 Color: 1014

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 348743 Color: 1282
Size: 338382 Color: 1197
Size: 312876 Color: 966

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 413308 Color: 1669
Size: 318721 Color: 1019
Size: 267972 Color: 409

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 413388 Color: 1670
Size: 303236 Color: 873
Size: 283377 Color: 633

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 413409 Color: 1671
Size: 319017 Color: 1022
Size: 267575 Color: 399

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 413603 Color: 1672
Size: 328064 Color: 1102
Size: 258334 Color: 249

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 413793 Color: 1673
Size: 314174 Color: 979
Size: 272034 Color: 476

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 413948 Color: 1675
Size: 335457 Color: 1166
Size: 250596 Color: 29

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 414026 Color: 1676
Size: 322106 Color: 1060
Size: 263869 Color: 347

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 414340 Color: 1677
Size: 323878 Color: 1074
Size: 261783 Color: 311

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 414496 Color: 1678
Size: 334472 Color: 1149
Size: 251033 Color: 40

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 414575 Color: 1679
Size: 293678 Color: 765
Size: 291748 Color: 748

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 414888 Color: 1680
Size: 312189 Color: 958
Size: 272924 Color: 496

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 415196 Color: 1681
Size: 314132 Color: 978
Size: 270673 Color: 451

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 415222 Color: 1682
Size: 318516 Color: 1018
Size: 266263 Color: 378

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 416021 Color: 1684
Size: 316259 Color: 1000
Size: 267721 Color: 402

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 416208 Color: 1686
Size: 292782 Color: 755
Size: 291011 Color: 732

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 416529 Color: 1688
Size: 326089 Color: 1090
Size: 257383 Color: 221

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 416867 Color: 1690
Size: 307307 Color: 904
Size: 275827 Color: 537

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 417783 Color: 1692
Size: 314348 Color: 981
Size: 267870 Color: 407

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 417783 Color: 1693
Size: 328046 Color: 1101
Size: 254172 Color: 136

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 417854 Color: 1694
Size: 296993 Color: 806
Size: 285154 Color: 658

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 418788 Color: 1695
Size: 323959 Color: 1076
Size: 257254 Color: 215

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 418841 Color: 1696
Size: 302636 Color: 866
Size: 278524 Color: 571

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 418899 Color: 1697
Size: 297097 Color: 808
Size: 284005 Color: 644

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 419038 Color: 1698
Size: 309826 Color: 936
Size: 271137 Color: 462

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 419053 Color: 1699
Size: 302818 Color: 868
Size: 278130 Color: 565

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 419752 Color: 1701
Size: 305493 Color: 892
Size: 274756 Color: 530

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 419876 Color: 1702
Size: 309937 Color: 937
Size: 270188 Color: 442

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 419928 Color: 1703
Size: 326154 Color: 1091
Size: 253919 Color: 130

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 420212 Color: 1704
Size: 320772 Color: 1047
Size: 259017 Color: 267

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 420352 Color: 1705
Size: 305700 Color: 894
Size: 273949 Color: 513

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 420777 Color: 1706
Size: 291131 Color: 737
Size: 288093 Color: 694

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 421207 Color: 1707
Size: 323335 Color: 1070
Size: 255459 Color: 168

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 421300 Color: 1708
Size: 303445 Color: 876
Size: 275256 Color: 532

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 421774 Color: 1709
Size: 306096 Color: 897
Size: 272131 Color: 481

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 422028 Color: 1710
Size: 311760 Color: 955
Size: 266213 Color: 377

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 422160 Color: 1711
Size: 289329 Color: 714
Size: 288512 Color: 702

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 422270 Color: 1712
Size: 320620 Color: 1043
Size: 257111 Color: 210

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 422401 Color: 1713
Size: 296081 Color: 790
Size: 281519 Color: 607

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 423172 Color: 1714
Size: 321887 Color: 1058
Size: 254942 Color: 158

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 423218 Color: 1715
Size: 293494 Color: 761
Size: 283289 Color: 631

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 423515 Color: 1716
Size: 310575 Color: 943
Size: 265911 Color: 374

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 423550 Color: 1717
Size: 321115 Color: 1050
Size: 255336 Color: 163

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 423984 Color: 1718
Size: 307366 Color: 905
Size: 268651 Color: 419

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 424388 Color: 1719
Size: 308294 Color: 920
Size: 267319 Color: 395

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 424820 Color: 1720
Size: 302458 Color: 865
Size: 272723 Color: 492

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 424884 Color: 1721
Size: 316045 Color: 998
Size: 259072 Color: 269

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 425166 Color: 1722
Size: 289966 Color: 722
Size: 284869 Color: 652

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 425892 Color: 1723
Size: 315212 Color: 991
Size: 258897 Color: 263

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 425943 Color: 1725
Size: 288164 Color: 698
Size: 285894 Color: 668

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 425969 Color: 1726
Size: 293213 Color: 758
Size: 280819 Color: 595

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 426157 Color: 1727
Size: 318458 Color: 1016
Size: 255386 Color: 164

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 426789 Color: 1728
Size: 289109 Color: 711
Size: 284103 Color: 645

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 426992 Color: 1729
Size: 300934 Color: 849
Size: 272075 Color: 477

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 427014 Color: 1730
Size: 305583 Color: 893
Size: 267404 Color: 398

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 427038 Color: 1731
Size: 296162 Color: 791
Size: 276801 Color: 549

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 427818 Color: 1733
Size: 293531 Color: 762
Size: 278652 Color: 573

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 428031 Color: 1734
Size: 288902 Color: 709
Size: 283068 Color: 626

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 428043 Color: 1735
Size: 308918 Color: 929
Size: 263040 Color: 333

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 428121 Color: 1737
Size: 315370 Color: 992
Size: 256510 Color: 200

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 428413 Color: 1738
Size: 314127 Color: 977
Size: 257461 Color: 223

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 428823 Color: 1739
Size: 299060 Color: 831
Size: 272118 Color: 480

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 429401 Color: 1741
Size: 288324 Color: 700
Size: 282276 Color: 616

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 429711 Color: 1742
Size: 285234 Color: 659
Size: 285056 Color: 656

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 430419 Color: 1743
Size: 316554 Color: 1002
Size: 253028 Color: 107

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 430501 Color: 1744
Size: 291997 Color: 750
Size: 277503 Color: 559

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 430686 Color: 1745
Size: 297739 Color: 819
Size: 271576 Color: 470

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 430754 Color: 1746
Size: 297243 Color: 811
Size: 272004 Color: 475

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 431241 Color: 1749
Size: 309757 Color: 934
Size: 259003 Color: 266

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 431582 Color: 1750
Size: 287382 Color: 687
Size: 281037 Color: 602

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 431591 Color: 1751
Size: 297500 Color: 813
Size: 270910 Color: 457

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 431628 Color: 1752
Size: 301372 Color: 855
Size: 267001 Color: 388

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 431897 Color: 1753
Size: 295613 Color: 783
Size: 272491 Color: 488

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 432009 Color: 1754
Size: 297610 Color: 815
Size: 270382 Color: 444

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 432019 Color: 1755
Size: 307497 Color: 907
Size: 260485 Color: 298

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 432146 Color: 1756
Size: 296288 Color: 792
Size: 271567 Color: 469

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 432284 Color: 1757
Size: 310416 Color: 940
Size: 257301 Color: 219

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 433117 Color: 1758
Size: 288516 Color: 703
Size: 278368 Color: 569

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 433670 Color: 1759
Size: 289646 Color: 719
Size: 276685 Color: 548

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 433875 Color: 1760
Size: 289467 Color: 716
Size: 276659 Color: 547

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 433953 Color: 1762
Size: 298993 Color: 828
Size: 267055 Color: 390

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 434437 Color: 1763
Size: 291927 Color: 749
Size: 273637 Color: 509

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 434449 Color: 1764
Size: 284417 Color: 649
Size: 281135 Color: 603

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 435130 Color: 1765
Size: 301853 Color: 859
Size: 263018 Color: 332

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 435231 Color: 1766
Size: 282859 Color: 621
Size: 281911 Color: 610

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 435331 Color: 1767
Size: 310224 Color: 939
Size: 254446 Color: 142

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 435568 Color: 1768
Size: 313873 Color: 973
Size: 250560 Color: 27

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 436129 Color: 1769
Size: 306719 Color: 902
Size: 257153 Color: 211

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 436364 Color: 1770
Size: 289323 Color: 713
Size: 274314 Color: 519

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 436434 Color: 1771
Size: 296304 Color: 793
Size: 267263 Color: 394

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 436460 Color: 1772
Size: 284958 Color: 653
Size: 278583 Color: 572

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 436505 Color: 1773
Size: 288117 Color: 695
Size: 275379 Color: 534

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 436908 Color: 1774
Size: 311337 Color: 951
Size: 251756 Color: 71

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 437110 Color: 1775
Size: 311293 Color: 950
Size: 251598 Color: 62

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 437369 Color: 1777
Size: 291177 Color: 738
Size: 271455 Color: 468

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 437501 Color: 1778
Size: 303404 Color: 875
Size: 259096 Color: 271

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 438229 Color: 1782
Size: 288184 Color: 699
Size: 273588 Color: 508

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 438336 Color: 1783
Size: 293861 Color: 767
Size: 267804 Color: 405

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 438419 Color: 1784
Size: 296779 Color: 800
Size: 264803 Color: 360

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 438449 Color: 1785
Size: 300805 Color: 848
Size: 260747 Color: 301

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 438454 Color: 1786
Size: 284183 Color: 646
Size: 277364 Color: 557

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 439065 Color: 1787
Size: 299071 Color: 832
Size: 261865 Color: 313

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 439193 Color: 1788
Size: 302038 Color: 860
Size: 258770 Color: 258

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 439321 Color: 1789
Size: 282963 Color: 624
Size: 277717 Color: 560

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 440041 Color: 1790
Size: 297032 Color: 807
Size: 262928 Color: 328

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 440089 Color: 1791
Size: 287515 Color: 689
Size: 272397 Color: 486

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 440106 Color: 1792
Size: 294366 Color: 773
Size: 265529 Color: 366

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 440622 Color: 1793
Size: 299025 Color: 830
Size: 260354 Color: 296

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 441387 Color: 1794
Size: 287897 Color: 693
Size: 270717 Color: 454

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 441559 Color: 1795
Size: 305019 Color: 889
Size: 253423 Color: 116

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 441702 Color: 1796
Size: 284418 Color: 650
Size: 273881 Color: 512

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 441968 Color: 1797
Size: 296018 Color: 788
Size: 262015 Color: 316

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 443239 Color: 1799
Size: 300202 Color: 843
Size: 256560 Color: 202

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 443442 Color: 1800
Size: 286011 Color: 671
Size: 270548 Color: 447

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 444025 Color: 1801
Size: 303364 Color: 874
Size: 252612 Color: 96

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 444276 Color: 1802
Size: 282603 Color: 619
Size: 273122 Color: 501

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 445176 Color: 1803
Size: 290865 Color: 729
Size: 263960 Color: 349

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 445251 Color: 1804
Size: 283907 Color: 640
Size: 270843 Color: 455

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 445413 Color: 1805
Size: 289951 Color: 721
Size: 264637 Color: 358

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 445966 Color: 1806
Size: 297799 Color: 820
Size: 256236 Color: 193

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 446296 Color: 1807
Size: 295731 Color: 785
Size: 257974 Color: 237

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 446306 Color: 1808
Size: 281250 Color: 606
Size: 272445 Color: 487

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 446824 Color: 1809
Size: 285858 Color: 667
Size: 267319 Color: 396

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 446919 Color: 1810
Size: 282387 Color: 617
Size: 270695 Color: 453

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 447761 Color: 1811
Size: 299792 Color: 838
Size: 252448 Color: 89

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 447790 Color: 1812
Size: 285595 Color: 661
Size: 266616 Color: 382

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 447802 Color: 1813
Size: 281618 Color: 609
Size: 270581 Color: 450

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 447853 Color: 1814
Size: 289198 Color: 712
Size: 262950 Color: 330

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 447907 Color: 1815
Size: 280500 Color: 588
Size: 271594 Color: 471

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 447908 Color: 1816
Size: 277402 Color: 558
Size: 274691 Color: 528

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 447915 Color: 1817
Size: 296866 Color: 803
Size: 255220 Color: 160

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 448134 Color: 1818
Size: 276332 Color: 543
Size: 275535 Color: 536

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 448376 Color: 1819
Size: 283758 Color: 638
Size: 267867 Color: 406

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 448566 Color: 1820
Size: 291640 Color: 746
Size: 259795 Color: 284

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 448773 Color: 1821
Size: 278018 Color: 564
Size: 273210 Color: 503

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 448871 Color: 1824
Size: 293534 Color: 763
Size: 257596 Color: 227

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 449042 Color: 1825
Size: 292654 Color: 753
Size: 258305 Color: 248

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 449126 Color: 1826
Size: 294786 Color: 775
Size: 256089 Color: 191

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 450028 Color: 1827
Size: 275914 Color: 538
Size: 274059 Color: 516

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 450267 Color: 1828
Size: 280713 Color: 593
Size: 269021 Color: 426

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 450746 Color: 1829
Size: 290020 Color: 724
Size: 259235 Color: 276

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 450861 Color: 1830
Size: 296429 Color: 794
Size: 252711 Color: 98

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 451336 Color: 1831
Size: 276482 Color: 545
Size: 272183 Color: 482

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 452023 Color: 1832
Size: 277295 Color: 555
Size: 270683 Color: 452

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 452047 Color: 1833
Size: 297239 Color: 810
Size: 250715 Color: 34

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 452115 Color: 1834
Size: 282992 Color: 625
Size: 264894 Color: 362

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 452145 Color: 1835
Size: 292455 Color: 752
Size: 255401 Color: 166

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 452273 Color: 1836
Size: 291010 Color: 731
Size: 256718 Color: 205

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 453309 Color: 1837
Size: 280952 Color: 599
Size: 265740 Color: 370

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 453463 Color: 1838
Size: 284379 Color: 648
Size: 262159 Color: 320

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 454445 Color: 1839
Size: 293686 Color: 766
Size: 251870 Color: 74

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 454545 Color: 1840
Size: 280881 Color: 597
Size: 264575 Color: 356

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 454861 Color: 1841
Size: 288132 Color: 697
Size: 257008 Color: 209

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 455377 Color: 1842
Size: 285934 Color: 670
Size: 258690 Color: 255

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 455677 Color: 1843
Size: 277858 Color: 562
Size: 266466 Color: 380

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 455826 Color: 1844
Size: 290852 Color: 728
Size: 253323 Color: 113

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 456329 Color: 1845
Size: 287374 Color: 686
Size: 256298 Color: 196

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 456816 Color: 1846
Size: 287214 Color: 685
Size: 255971 Color: 185

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 457104 Color: 1847
Size: 272338 Color: 484
Size: 270559 Color: 449

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 457318 Color: 1848
Size: 284666 Color: 651
Size: 258017 Color: 239

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 457640 Color: 1849
Size: 290656 Color: 726
Size: 251705 Color: 67

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 458250 Color: 1850
Size: 284276 Color: 647
Size: 257475 Color: 224

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 458403 Color: 1851
Size: 275922 Color: 539
Size: 265676 Color: 369

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 458500 Color: 1852
Size: 291089 Color: 736
Size: 250412 Color: 20

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 458700 Color: 1853
Size: 274362 Color: 520
Size: 266939 Color: 387

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 458718 Color: 1854
Size: 282178 Color: 615
Size: 259105 Color: 272

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 459172 Color: 1855
Size: 276856 Color: 550
Size: 263973 Color: 350

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 459415 Color: 1856
Size: 289973 Color: 723
Size: 250613 Color: 30

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 459486 Color: 1857
Size: 280988 Color: 600
Size: 259527 Color: 281

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 459555 Color: 1858
Size: 277104 Color: 553
Size: 263342 Color: 337

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 459766 Color: 1859
Size: 283982 Color: 642
Size: 256253 Color: 194

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 460388 Color: 1860
Size: 285619 Color: 662
Size: 253994 Color: 131

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 460395 Color: 1861
Size: 283964 Color: 641
Size: 255642 Color: 174

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 460859 Color: 1863
Size: 288724 Color: 706
Size: 250418 Color: 21

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 460903 Color: 1864
Size: 285589 Color: 660
Size: 253509 Color: 119

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 461236 Color: 1865
Size: 281224 Color: 604
Size: 257541 Color: 225

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 462007 Color: 1866
Size: 270949 Color: 458
Size: 267045 Color: 389

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 462279 Color: 1867
Size: 276262 Color: 542
Size: 261460 Color: 310

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 462419 Color: 1868
Size: 279301 Color: 581
Size: 258281 Color: 246

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 462531 Color: 1869
Size: 275446 Color: 535
Size: 262024 Color: 317

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 463516 Color: 1870
Size: 272290 Color: 483
Size: 264195 Color: 354

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 463795 Color: 1871
Size: 280643 Color: 590
Size: 255563 Color: 172

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 463892 Color: 1872
Size: 277074 Color: 552
Size: 259035 Color: 268

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 463978 Color: 1873
Size: 272599 Color: 491
Size: 263424 Color: 339

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 464046 Color: 1874
Size: 280486 Color: 587
Size: 255469 Color: 169

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 464350 Color: 1875
Size: 279515 Color: 584
Size: 256136 Color: 192

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 464409 Color: 1876
Size: 278907 Color: 577
Size: 256685 Color: 203

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 464832 Color: 1877
Size: 282934 Color: 623
Size: 252235 Color: 79

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 465468 Color: 1878
Size: 275279 Color: 533
Size: 259254 Color: 277

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 465538 Color: 1879
Size: 281918 Color: 611
Size: 252545 Color: 94

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 466107 Color: 1880
Size: 274536 Color: 526
Size: 259358 Color: 278

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 466173 Color: 1881
Size: 278440 Color: 570
Size: 255388 Color: 165

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 466479 Color: 1882
Size: 283276 Color: 630
Size: 250246 Color: 10

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 466637 Color: 1883
Size: 272103 Color: 478
Size: 261261 Color: 306

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 466655 Color: 1884
Size: 267746 Color: 403
Size: 265600 Color: 367

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 466715 Color: 1885
Size: 269915 Color: 438
Size: 263371 Color: 338

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 467795 Color: 1886
Size: 273420 Color: 507
Size: 258786 Color: 259

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 468482 Color: 1887
Size: 268570 Color: 416
Size: 262949 Color: 329

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 468911 Color: 1888
Size: 268871 Color: 423
Size: 262219 Color: 321

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 469605 Color: 1889
Size: 276043 Color: 540
Size: 254353 Color: 139

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 470028 Color: 1890
Size: 270894 Color: 456
Size: 259079 Color: 270

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 470302 Color: 1891
Size: 271881 Color: 474
Size: 257818 Color: 235

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 470606 Color: 1892
Size: 271750 Color: 472
Size: 257645 Color: 230

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 470704 Color: 1893
Size: 271216 Color: 463
Size: 258081 Color: 241

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 471172 Color: 1894
Size: 276948 Color: 551
Size: 251881 Color: 75

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 471607 Color: 1895
Size: 265310 Color: 364
Size: 263084 Color: 334

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 471716 Color: 1896
Size: 267908 Color: 408
Size: 260377 Color: 297

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 472007 Color: 1897
Size: 274160 Color: 518
Size: 253834 Color: 126

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 472368 Color: 1898
Size: 268926 Color: 424
Size: 258707 Color: 256

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 473051 Color: 1899
Size: 266835 Color: 385
Size: 260115 Color: 291

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 473216 Color: 1900
Size: 273238 Color: 504
Size: 253547 Color: 120

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 473632 Color: 1901
Size: 273819 Color: 511
Size: 252550 Color: 95

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 473634 Color: 1902
Size: 272532 Color: 489
Size: 253835 Color: 127

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 473682 Color: 1903
Size: 265823 Color: 371
Size: 260496 Color: 299

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 473987 Color: 1904
Size: 269970 Color: 440
Size: 256044 Color: 189

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 474203 Color: 1905
Size: 271297 Color: 466
Size: 254501 Color: 143

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 474612 Color: 1906
Size: 266928 Color: 386
Size: 258461 Color: 252

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 475008 Color: 1907
Size: 271100 Color: 460
Size: 253893 Color: 128

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 475434 Color: 1908
Size: 264496 Color: 355
Size: 260071 Color: 290

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 475518 Color: 1909
Size: 266724 Color: 384
Size: 257759 Color: 234

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 475756 Color: 1910
Size: 266426 Color: 379
Size: 257819 Color: 236

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 476085 Color: 1911
Size: 272366 Color: 485
Size: 251550 Color: 59

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 476676 Color: 1913
Size: 268774 Color: 420
Size: 254551 Color: 147

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 477560 Color: 1914
Size: 267683 Color: 401
Size: 254758 Color: 154

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 477650 Color: 1915
Size: 269956 Color: 439
Size: 252395 Color: 86

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 478095 Color: 1916
Size: 263846 Color: 346
Size: 258060 Color: 240

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 478257 Color: 1917
Size: 271293 Color: 465
Size: 250451 Color: 23

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 478352 Color: 1918
Size: 262144 Color: 319
Size: 259505 Color: 280

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 478835 Color: 1919
Size: 268811 Color: 422
Size: 252355 Color: 84

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 479401 Color: 1920
Size: 263878 Color: 348
Size: 256722 Color: 206

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 479427 Color: 1921
Size: 269117 Color: 428
Size: 251457 Color: 57

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 479492 Color: 1922
Size: 268148 Color: 411
Size: 252361 Color: 85

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 479530 Color: 1923
Size: 260886 Color: 303
Size: 259585 Color: 282

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 479788 Color: 1924
Size: 266659 Color: 383
Size: 253554 Color: 121

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 479888 Color: 1925
Size: 269426 Color: 433
Size: 250687 Color: 32

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 479901 Color: 1926
Size: 268266 Color: 412
Size: 251834 Color: 72

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 480231 Color: 1928
Size: 265864 Color: 372
Size: 253906 Color: 129

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 480361 Color: 1929
Size: 269319 Color: 431
Size: 250321 Color: 15

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 480530 Color: 1930
Size: 268437 Color: 414
Size: 251034 Color: 41

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 480940 Color: 1931
Size: 268975 Color: 425
Size: 250086 Color: 2

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 480970 Color: 1932
Size: 265634 Color: 368
Size: 253397 Color: 115

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 481186 Color: 1933
Size: 262772 Color: 327
Size: 256043 Color: 188

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 481765 Color: 1934
Size: 265369 Color: 365
Size: 252867 Color: 102

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 482357 Color: 1935
Size: 261834 Color: 312
Size: 255810 Color: 181

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 482449 Color: 1936
Size: 267182 Color: 392
Size: 250370 Color: 16

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 482507 Color: 1937
Size: 260296 Color: 293
Size: 257198 Color: 213

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 482985 Color: 1938
Size: 258845 Color: 262
Size: 258171 Color: 243

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 483596 Color: 1939
Size: 258987 Color: 265
Size: 257418 Color: 222

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 483801 Color: 1940
Size: 264614 Color: 357
Size: 251586 Color: 61

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 484542 Color: 1941
Size: 261369 Color: 308
Size: 254090 Color: 134

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 484706 Color: 1942
Size: 264171 Color: 353
Size: 251124 Color: 47

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 484714 Color: 1943
Size: 262492 Color: 325
Size: 252795 Color: 100

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 485189 Color: 1944
Size: 261905 Color: 314
Size: 252907 Color: 104

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 485581 Color: 1945
Size: 264024 Color: 351
Size: 250396 Color: 18

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 486161 Color: 1947
Size: 258354 Color: 251
Size: 255486 Color: 171

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 486285 Color: 1948
Size: 258231 Color: 245
Size: 255485 Color: 170

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 486575 Color: 1949
Size: 258716 Color: 257
Size: 254710 Color: 152

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 486732 Color: 1950
Size: 262306 Color: 323
Size: 250963 Color: 38

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 487258 Color: 1951
Size: 256701 Color: 204
Size: 256042 Color: 187

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 487710 Color: 1952
Size: 260149 Color: 292
Size: 252142 Color: 78

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 488262 Color: 1954
Size: 260029 Color: 288
Size: 251710 Color: 69

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 488335 Color: 1955
Size: 258648 Color: 254
Size: 253018 Color: 106

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 488447 Color: 1956
Size: 258216 Color: 244
Size: 253338 Color: 114

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 488553 Color: 1957
Size: 261156 Color: 305
Size: 250292 Color: 12

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 488607 Color: 1958
Size: 259106 Color: 273
Size: 252288 Color: 82

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 489044 Color: 1959
Size: 257288 Color: 217
Size: 253669 Color: 123

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 489240 Color: 1960
Size: 257297 Color: 218
Size: 253464 Color: 117

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 489577 Color: 1961
Size: 255638 Color: 173
Size: 254786 Color: 155

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 490011 Color: 1962
Size: 255869 Color: 182
Size: 254121 Color: 135

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 490051 Color: 1963
Size: 256286 Color: 195
Size: 253664 Color: 122

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 490142 Color: 1964
Size: 255130 Color: 159
Size: 254729 Color: 153

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 490406 Color: 1965
Size: 257199 Color: 214
Size: 252396 Color: 87

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 490460 Color: 1966
Size: 257268 Color: 216
Size: 252273 Color: 81

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 490793 Color: 1967
Size: 254667 Color: 149
Size: 254541 Color: 146

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 490815 Color: 1968
Size: 256371 Color: 197
Size: 252815 Color: 101

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 490830 Color: 1969
Size: 254797 Color: 156
Size: 254374 Color: 141

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 491091 Color: 1970
Size: 255920 Color: 184
Size: 252990 Color: 105

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 491383 Color: 1971
Size: 257746 Color: 233
Size: 250872 Color: 37

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 491733 Color: 1972
Size: 255795 Color: 180
Size: 252473 Color: 91

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 492022 Color: 1973
Size: 254510 Color: 145
Size: 253469 Color: 118

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 492112 Color: 1974
Size: 257318 Color: 220
Size: 250571 Color: 28

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 492415 Color: 1975
Size: 255878 Color: 183
Size: 251708 Color: 68

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 493025 Color: 1976
Size: 254363 Color: 140
Size: 252613 Color: 97

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 493119 Color: 1977
Size: 255227 Color: 162
Size: 251655 Color: 64

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 493259 Color: 1978
Size: 255650 Color: 177
Size: 251092 Color: 45

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 493989 Color: 1979
Size: 253260 Color: 110
Size: 252752 Color: 99

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 494599 Color: 1980
Size: 254223 Color: 137
Size: 251179 Color: 48

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 494914 Color: 1981
Size: 253117 Color: 108
Size: 251970 Color: 77

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 495127 Color: 1982
Size: 253819 Color: 125
Size: 251055 Color: 44

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 496631 Color: 1984
Size: 252246 Color: 80
Size: 251124 Color: 46

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 496975 Color: 1985
Size: 251603 Color: 63
Size: 251423 Color: 56

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 497054 Color: 1986
Size: 251894 Color: 76
Size: 251053 Color: 43

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 497433 Color: 1987
Size: 251380 Color: 53
Size: 251188 Color: 50

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 497590 Color: 1989
Size: 251715 Color: 70
Size: 250696 Color: 33

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 497611 Color: 1990
Size: 251413 Color: 54
Size: 250977 Color: 39

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 498164 Color: 1991
Size: 251519 Color: 58
Size: 250318 Color: 14

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 498378 Color: 1992
Size: 251224 Color: 51
Size: 250399 Color: 19

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 498620 Color: 1993
Size: 251259 Color: 52
Size: 250122 Color: 4

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 498910 Color: 1994
Size: 250552 Color: 25
Size: 250539 Color: 24

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 499133 Color: 1995
Size: 250559 Color: 26
Size: 250309 Color: 13

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 499169 Color: 1996
Size: 250734 Color: 35
Size: 250098 Color: 3

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 499282 Color: 1997
Size: 250430 Color: 22
Size: 250289 Color: 11

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 499467 Color: 1998
Size: 250389 Color: 17
Size: 250145 Color: 6

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 399506 Color: 1609
Size: 350480 Color: 1302
Size: 250014 Color: 0

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 412817 Color: 1668
Size: 334302 Color: 1148
Size: 252881 Color: 103

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 416498 Color: 1687
Size: 304457 Color: 887
Size: 279045 Color: 578

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 416561 Color: 1689
Size: 321486 Color: 1052
Size: 261953 Color: 315

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 417678 Color: 1691
Size: 319607 Color: 1032
Size: 262715 Color: 326

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 419637 Color: 1700
Size: 307196 Color: 903
Size: 273167 Color: 502

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 425897 Color: 1724
Size: 291634 Color: 745
Size: 282469 Color: 618

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 427271 Color: 1732
Size: 313525 Color: 970
Size: 259204 Color: 274

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 428102 Color: 1736
Size: 308662 Color: 924
Size: 263236 Color: 336

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 429146 Color: 1740
Size: 291208 Color: 739
Size: 279646 Color: 585

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 431163 Color: 1748
Size: 296725 Color: 798
Size: 272112 Color: 479

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 437162 Color: 1776
Size: 299121 Color: 833
Size: 263717 Color: 342

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 437781 Color: 1779
Size: 286077 Color: 672
Size: 276142 Color: 541

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 437995 Color: 1780
Size: 290732 Color: 727
Size: 271273 Color: 464

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 441970 Color: 1798
Size: 289394 Color: 715
Size: 268636 Color: 418

Bin 479: 1 of cap free
Amount of items: 3
Items: 
Size: 448808 Color: 1822
Size: 294373 Color: 774
Size: 256819 Color: 207

Bin 480: 1 of cap free
Amount of items: 3
Items: 
Size: 448821 Color: 1823
Size: 287460 Color: 688
Size: 263719 Color: 343

Bin 481: 1 of cap free
Amount of items: 3
Items: 
Size: 480174 Color: 1927
Size: 264074 Color: 352
Size: 255752 Color: 179

Bin 482: 1 of cap free
Amount of items: 3
Items: 
Size: 485956 Color: 1946
Size: 257609 Color: 228
Size: 256435 Color: 199

Bin 483: 1 of cap free
Amount of items: 3
Items: 
Size: 488170 Color: 1953
Size: 256381 Color: 198
Size: 255449 Color: 167

Bin 484: 1 of cap free
Amount of items: 3
Items: 
Size: 390071 Color: 1560
Size: 357483 Color: 1356
Size: 252446 Color: 88

Bin 485: 1 of cap free
Amount of items: 3
Items: 
Size: 403979 Color: 1626
Size: 331285 Color: 1128
Size: 264736 Color: 359

Bin 486: 1 of cap free
Amount of items: 3
Items: 
Size: 411344 Color: 1664
Size: 305409 Color: 891
Size: 283247 Color: 629

Bin 487: 1 of cap free
Amount of items: 3
Items: 
Size: 337987 Color: 1193
Size: 336859 Color: 1181
Size: 325154 Color: 1082

Bin 488: 1 of cap free
Amount of items: 3
Items: 
Size: 396834 Color: 1593
Size: 334066 Color: 1144
Size: 269100 Color: 427

Bin 489: 1 of cap free
Amount of items: 3
Items: 
Size: 370449 Color: 1447
Size: 359364 Color: 1370
Size: 270187 Color: 441

Bin 490: 1 of cap free
Amount of items: 3
Items: 
Size: 402037 Color: 1619
Size: 337447 Color: 1188
Size: 260516 Color: 300

Bin 491: 1 of cap free
Amount of items: 3
Items: 
Size: 397413 Color: 1596
Size: 335400 Color: 1164
Size: 267187 Color: 393

Bin 492: 1 of cap free
Amount of items: 3
Items: 
Size: 372861 Color: 1460
Size: 337503 Color: 1189
Size: 289636 Color: 718

Bin 493: 1 of cap free
Amount of items: 3
Items: 
Size: 395317 Color: 1582
Size: 330263 Color: 1121
Size: 274420 Color: 521

Bin 494: 1 of cap free
Amount of items: 3
Items: 
Size: 395132 Color: 1581
Size: 325699 Color: 1086
Size: 279169 Color: 579

Bin 495: 1 of cap free
Amount of items: 3
Items: 
Size: 368769 Color: 1433
Size: 358333 Color: 1363
Size: 272898 Color: 495

Bin 496: 1 of cap free
Amount of items: 3
Items: 
Size: 397947 Color: 1598
Size: 343114 Color: 1246
Size: 258939 Color: 264

Bin 497: 1 of cap free
Amount of items: 3
Items: 
Size: 367977 Color: 1423
Size: 329748 Color: 1117
Size: 302275 Color: 864

Bin 498: 1 of cap free
Amount of items: 3
Items: 
Size: 390640 Color: 1563
Size: 316629 Color: 1004
Size: 292731 Color: 754

Bin 499: 1 of cap free
Amount of items: 3
Items: 
Size: 399053 Color: 1602
Size: 322238 Color: 1062
Size: 278709 Color: 574

Bin 500: 1 of cap free
Amount of items: 3
Items: 
Size: 380171 Color: 1505
Size: 317691 Color: 1009
Size: 302138 Color: 861

Bin 501: 1 of cap free
Amount of items: 3
Items: 
Size: 379352 Color: 1499
Size: 317720 Color: 1010
Size: 302928 Color: 870

Bin 502: 1 of cap free
Amount of items: 3
Items: 
Size: 381173 Color: 1512
Size: 320037 Color: 1038
Size: 298790 Color: 825

Bin 503: 1 of cap free
Amount of items: 3
Items: 
Size: 406118 Color: 1636
Size: 330875 Color: 1124
Size: 263007 Color: 331

Bin 504: 1 of cap free
Amount of items: 3
Items: 
Size: 362348 Color: 1391
Size: 351356 Color: 1311
Size: 286296 Color: 673

Bin 505: 1 of cap free
Amount of items: 3
Items: 
Size: 402849 Color: 1621
Size: 346915 Color: 1269
Size: 250236 Color: 9

Bin 506: 1 of cap free
Amount of items: 3
Items: 
Size: 384458 Color: 1533
Size: 321914 Color: 1059
Size: 293628 Color: 764

Bin 507: 1 of cap free
Amount of items: 3
Items: 
Size: 365230 Color: 1402
Size: 349076 Color: 1289
Size: 285694 Color: 664

Bin 508: 1 of cap free
Amount of items: 3
Items: 
Size: 387669 Color: 1544
Size: 355155 Color: 1340
Size: 257176 Color: 212

Bin 509: 1 of cap free
Amount of items: 3
Items: 
Size: 377783 Color: 1491
Size: 326253 Color: 1093
Size: 295964 Color: 787

Bin 510: 1 of cap free
Amount of items: 3
Items: 
Size: 369936 Color: 1442
Size: 321703 Color: 1055
Size: 308361 Color: 921

Bin 511: 1 of cap free
Amount of items: 3
Items: 
Size: 375809 Color: 1473
Size: 315617 Color: 994
Size: 308574 Color: 923

Bin 512: 1 of cap free
Amount of items: 3
Items: 
Size: 378354 Color: 1493
Size: 352443 Color: 1318
Size: 269203 Color: 430

Bin 513: 1 of cap free
Amount of items: 3
Items: 
Size: 366325 Color: 1413
Size: 323899 Color: 1075
Size: 309776 Color: 935

Bin 514: 1 of cap free
Amount of items: 3
Items: 
Size: 389062 Color: 1556
Size: 309615 Color: 932
Size: 301323 Color: 854

Bin 515: 1 of cap free
Amount of items: 3
Items: 
Size: 356506 Color: 1351
Size: 327836 Color: 1099
Size: 315658 Color: 995

Bin 516: 1 of cap free
Amount of items: 3
Items: 
Size: 365338 Color: 1404
Size: 333413 Color: 1139
Size: 301249 Color: 852

Bin 517: 1 of cap free
Amount of items: 3
Items: 
Size: 381240 Color: 1515
Size: 350233 Color: 1296
Size: 268527 Color: 415

Bin 518: 1 of cap free
Amount of items: 3
Items: 
Size: 372198 Color: 1456
Size: 340731 Color: 1217
Size: 287071 Color: 683

Bin 519: 1 of cap free
Amount of items: 3
Items: 
Size: 372037 Color: 1454
Size: 342966 Color: 1244
Size: 284997 Color: 654

Bin 520: 1 of cap free
Amount of items: 3
Items: 
Size: 380543 Color: 1506
Size: 341182 Color: 1221
Size: 278275 Color: 568

Bin 521: 1 of cap free
Amount of items: 3
Items: 
Size: 433915 Color: 1761
Size: 307741 Color: 913
Size: 258344 Color: 250

Bin 522: 2 of cap free
Amount of items: 3
Items: 
Size: 415540 Color: 1683
Size: 296077 Color: 789
Size: 288382 Color: 701

Bin 523: 2 of cap free
Amount of items: 3
Items: 
Size: 416029 Color: 1685
Size: 296777 Color: 799
Size: 287193 Color: 684

Bin 524: 2 of cap free
Amount of items: 3
Items: 
Size: 438182 Color: 1781
Size: 293470 Color: 760
Size: 268347 Color: 413

Bin 525: 2 of cap free
Amount of items: 3
Items: 
Size: 411582 Color: 1665
Size: 312054 Color: 956
Size: 276363 Color: 544

Bin 526: 2 of cap free
Amount of items: 3
Items: 
Size: 406827 Color: 1640
Size: 337523 Color: 1190
Size: 255649 Color: 176

Bin 527: 2 of cap free
Amount of items: 3
Items: 
Size: 399090 Color: 1603
Size: 307947 Color: 915
Size: 292962 Color: 757

Bin 528: 2 of cap free
Amount of items: 3
Items: 
Size: 396450 Color: 1587
Size: 335935 Color: 1173
Size: 267614 Color: 400

Bin 529: 2 of cap free
Amount of items: 3
Items: 
Size: 350770 Color: 1304
Size: 344576 Color: 1255
Size: 304653 Color: 888

Bin 530: 2 of cap free
Amount of items: 3
Items: 
Size: 368611 Color: 1431
Size: 320026 Color: 1037
Size: 311362 Color: 952

Bin 531: 2 of cap free
Amount of items: 3
Items: 
Size: 370111 Color: 1443
Size: 346954 Color: 1270
Size: 282934 Color: 622

Bin 532: 2 of cap free
Amount of items: 3
Items: 
Size: 392496 Color: 1569
Size: 304443 Color: 886
Size: 303060 Color: 871

Bin 533: 2 of cap free
Amount of items: 3
Items: 
Size: 385740 Color: 1536
Size: 344745 Color: 1257
Size: 269514 Color: 434

Bin 534: 2 of cap free
Amount of items: 3
Items: 
Size: 397116 Color: 1595
Size: 342576 Color: 1240
Size: 260307 Color: 295

Bin 535: 2 of cap free
Amount of items: 3
Items: 
Size: 351346 Color: 1310
Size: 330575 Color: 1123
Size: 318078 Color: 1012

Bin 536: 2 of cap free
Amount of items: 3
Items: 
Size: 372012 Color: 1452
Size: 314557 Color: 982
Size: 313430 Color: 969

Bin 537: 2 of cap free
Amount of items: 3
Items: 
Size: 373298 Color: 1463
Size: 329214 Color: 1111
Size: 297487 Color: 812

Bin 538: 2 of cap free
Amount of items: 3
Items: 
Size: 369551 Color: 1437
Size: 331256 Color: 1127
Size: 299192 Color: 834

Bin 539: 2 of cap free
Amount of items: 3
Items: 
Size: 383738 Color: 1528
Size: 317382 Color: 1006
Size: 298879 Color: 826

Bin 540: 2 of cap free
Amount of items: 3
Items: 
Size: 349064 Color: 1288
Size: 331627 Color: 1130
Size: 319308 Color: 1027

Bin 541: 2 of cap free
Amount of items: 3
Items: 
Size: 354061 Color: 1330
Size: 338284 Color: 1196
Size: 307654 Color: 912

Bin 542: 2 of cap free
Amount of items: 3
Items: 
Size: 350445 Color: 1300
Size: 338675 Color: 1202
Size: 310879 Color: 947

Bin 543: 2 of cap free
Amount of items: 3
Items: 
Size: 342066 Color: 1235
Size: 341330 Color: 1225
Size: 316603 Color: 1003

Bin 544: 3 of cap free
Amount of items: 3
Items: 
Size: 460829 Color: 1862
Size: 280348 Color: 586
Size: 258821 Color: 260

Bin 545: 3 of cap free
Amount of items: 3
Items: 
Size: 396007 Color: 1586
Size: 344185 Color: 1252
Size: 259806 Color: 285

Bin 546: 3 of cap free
Amount of items: 3
Items: 
Size: 396633 Color: 1589
Size: 332377 Color: 1133
Size: 270988 Color: 459

Bin 547: 3 of cap free
Amount of items: 3
Items: 
Size: 390920 Color: 1565
Size: 335701 Color: 1171
Size: 273377 Color: 506

Bin 548: 3 of cap free
Amount of items: 3
Items: 
Size: 369135 Color: 1436
Size: 339320 Color: 1206
Size: 291543 Color: 742

Bin 549: 3 of cap free
Amount of items: 3
Items: 
Size: 382223 Color: 1521
Size: 328152 Color: 1104
Size: 289623 Color: 717

Bin 550: 3 of cap free
Amount of items: 3
Items: 
Size: 388981 Color: 1555
Size: 333883 Color: 1142
Size: 277134 Color: 554

Bin 551: 3 of cap free
Amount of items: 3
Items: 
Size: 381704 Color: 1518
Size: 335215 Color: 1160
Size: 283079 Color: 628

Bin 552: 3 of cap free
Amount of items: 3
Items: 
Size: 353381 Color: 1326
Size: 323566 Color: 1072
Size: 323051 Color: 1067

Bin 553: 3 of cap free
Amount of items: 3
Items: 
Size: 368959 Color: 1434
Size: 322984 Color: 1066
Size: 308055 Color: 918

Bin 554: 3 of cap free
Amount of items: 3
Items: 
Size: 406535 Color: 1639
Size: 303610 Color: 879
Size: 289853 Color: 720

Bin 555: 3 of cap free
Amount of items: 3
Items: 
Size: 373422 Color: 1464
Size: 324997 Color: 1080
Size: 301579 Color: 856

Bin 556: 3 of cap free
Amount of items: 3
Items: 
Size: 370172 Color: 1446
Size: 325962 Color: 1088
Size: 303864 Color: 881

Bin 557: 3 of cap free
Amount of items: 3
Items: 
Size: 376668 Color: 1481
Size: 368654 Color: 1432
Size: 254676 Color: 150

Bin 558: 3 of cap free
Amount of items: 3
Items: 
Size: 350456 Color: 1301
Size: 338464 Color: 1199
Size: 311078 Color: 948

Bin 559: 3 of cap free
Amount of items: 3
Items: 
Size: 405756 Color: 1635
Size: 320129 Color: 1040
Size: 274113 Color: 517

Bin 560: 3 of cap free
Amount of items: 3
Items: 
Size: 360368 Color: 1381
Size: 360365 Color: 1380
Size: 279265 Color: 580

Bin 561: 3 of cap free
Amount of items: 3
Items: 
Size: 360351 Color: 1379
Size: 360296 Color: 1378
Size: 279351 Color: 582

Bin 562: 3 of cap free
Amount of items: 3
Items: 
Size: 431057 Color: 1747
Size: 318308 Color: 1015
Size: 250633 Color: 31

Bin 563: 3 of cap free
Amount of items: 3
Items: 
Size: 359201 Color: 1369
Size: 320703 Color: 1046
Size: 320094 Color: 1039

Bin 564: 3 of cap free
Amount of items: 3
Items: 
Size: 351997 Color: 1314
Size: 328503 Color: 1107
Size: 319498 Color: 1031

Bin 565: 3 of cap free
Amount of items: 3
Items: 
Size: 388885 Color: 1554
Size: 307556 Color: 909
Size: 303557 Color: 878

Bin 566: 3 of cap free
Amount of items: 3
Items: 
Size: 341613 Color: 1230
Size: 335304 Color: 1161
Size: 323081 Color: 1068

Bin 567: 3 of cap free
Amount of items: 3
Items: 
Size: 396755 Color: 1591
Size: 343808 Color: 1250
Size: 259435 Color: 279

Bin 568: 4 of cap free
Amount of items: 3
Items: 
Size: 387999 Color: 1546
Size: 334222 Color: 1146
Size: 277776 Color: 561

Bin 569: 4 of cap free
Amount of items: 3
Items: 
Size: 396498 Color: 1588
Size: 305840 Color: 895
Size: 297659 Color: 816

Bin 570: 4 of cap free
Amount of items: 3
Items: 
Size: 383201 Color: 1525
Size: 319671 Color: 1034
Size: 297125 Color: 809

Bin 571: 4 of cap free
Amount of items: 3
Items: 
Size: 386217 Color: 1537
Size: 326862 Color: 1095
Size: 286918 Color: 680

Bin 572: 4 of cap free
Amount of items: 3
Items: 
Size: 352776 Color: 1322
Size: 333803 Color: 1141
Size: 313418 Color: 968

Bin 573: 4 of cap free
Amount of items: 3
Items: 
Size: 368093 Color: 1425
Size: 325602 Color: 1085
Size: 306302 Color: 898

Bin 574: 4 of cap free
Amount of items: 3
Items: 
Size: 377394 Color: 1487
Size: 344732 Color: 1256
Size: 277871 Color: 563

Bin 575: 4 of cap free
Amount of items: 3
Items: 
Size: 399445 Color: 1608
Size: 319713 Color: 1035
Size: 280839 Color: 596

Bin 576: 5 of cap free
Amount of items: 3
Items: 
Size: 366136 Color: 1411
Size: 319645 Color: 1033
Size: 314215 Color: 980

Bin 577: 5 of cap free
Amount of items: 3
Items: 
Size: 359155 Color: 1368
Size: 324830 Color: 1079
Size: 316011 Color: 997

Bin 578: 5 of cap free
Amount of items: 3
Items: 
Size: 409736 Color: 1655
Size: 317682 Color: 1008
Size: 272578 Color: 490

Bin 579: 5 of cap free
Amount of items: 3
Items: 
Size: 349233 Color: 1291
Size: 339924 Color: 1213
Size: 310839 Color: 946

Bin 580: 5 of cap free
Amount of items: 3
Items: 
Size: 401744 Color: 1618
Size: 300535 Color: 847
Size: 297717 Color: 818

Bin 581: 5 of cap free
Amount of items: 3
Items: 
Size: 372177 Color: 1455
Size: 315407 Color: 993
Size: 312412 Color: 961

Bin 582: 6 of cap free
Amount of items: 3
Items: 
Size: 410762 Color: 1663
Size: 305239 Color: 890
Size: 283994 Color: 643

Bin 583: 6 of cap free
Amount of items: 3
Items: 
Size: 400675 Color: 1614
Size: 313611 Color: 971
Size: 285709 Color: 665

Bin 584: 6 of cap free
Amount of items: 3
Items: 
Size: 366913 Color: 1418
Size: 335177 Color: 1159
Size: 297905 Color: 821

Bin 585: 6 of cap free
Amount of items: 3
Items: 
Size: 379811 Color: 1503
Size: 329171 Color: 1110
Size: 291013 Color: 733

Bin 586: 6 of cap free
Amount of items: 3
Items: 
Size: 363070 Color: 1396
Size: 336684 Color: 1180
Size: 300241 Color: 844

Bin 587: 7 of cap free
Amount of items: 3
Items: 
Size: 408940 Color: 1650
Size: 318019 Color: 1011
Size: 273035 Color: 500

Bin 588: 7 of cap free
Amount of items: 3
Items: 
Size: 413877 Color: 1674
Size: 308787 Color: 927
Size: 277330 Color: 556

Bin 589: 7 of cap free
Amount of items: 3
Items: 
Size: 365743 Color: 1408
Size: 359817 Color: 1373
Size: 274434 Color: 522

Bin 590: 7 of cap free
Amount of items: 3
Items: 
Size: 383751 Color: 1529
Size: 312699 Color: 962
Size: 303544 Color: 877

Bin 591: 7 of cap free
Amount of items: 3
Items: 
Size: 381794 Color: 1520
Size: 312120 Color: 957
Size: 306080 Color: 896

Bin 592: 7 of cap free
Amount of items: 3
Items: 
Size: 357731 Color: 1357
Size: 349034 Color: 1287
Size: 293229 Color: 759

Bin 593: 8 of cap free
Amount of items: 3
Items: 
Size: 397442 Color: 1597
Size: 350698 Color: 1303
Size: 251853 Color: 73

Bin 594: 8 of cap free
Amount of items: 3
Items: 
Size: 374419 Color: 1469
Size: 374387 Color: 1467
Size: 251187 Color: 49

Bin 595: 8 of cap free
Amount of items: 3
Items: 
Size: 346281 Color: 1267
Size: 330133 Color: 1119
Size: 323579 Color: 1073

Bin 596: 8 of cap free
Amount of items: 3
Items: 
Size: 366085 Color: 1409
Size: 325177 Color: 1083
Size: 308731 Color: 926

Bin 597: 8 of cap free
Amount of items: 3
Items: 
Size: 403710 Color: 1625
Size: 301071 Color: 850
Size: 295212 Color: 780

Bin 598: 8 of cap free
Amount of items: 3
Items: 
Size: 369768 Color: 1440
Size: 321508 Color: 1053
Size: 308717 Color: 925

Bin 599: 9 of cap free
Amount of items: 3
Items: 
Size: 368034 Color: 1424
Size: 360115 Color: 1376
Size: 271843 Color: 473

Bin 600: 9 of cap free
Amount of items: 3
Items: 
Size: 401686 Color: 1617
Size: 335914 Color: 1172
Size: 262392 Color: 324

Bin 601: 9 of cap free
Amount of items: 3
Items: 
Size: 348970 Color: 1286
Size: 338279 Color: 1195
Size: 312743 Color: 963

Bin 602: 9 of cap free
Amount of items: 3
Items: 
Size: 379636 Color: 1501
Size: 318143 Color: 1013
Size: 302213 Color: 863

Bin 603: 9 of cap free
Amount of items: 3
Items: 
Size: 342049 Color: 1234
Size: 337244 Color: 1185
Size: 320699 Color: 1045

Bin 604: 10 of cap free
Amount of items: 3
Items: 
Size: 383128 Color: 1524
Size: 342863 Color: 1243
Size: 274000 Color: 514

Bin 605: 10 of cap free
Amount of items: 3
Items: 
Size: 360660 Color: 1384
Size: 357266 Color: 1354
Size: 282065 Color: 614

Bin 606: 11 of cap free
Amount of items: 3
Items: 
Size: 410733 Color: 1662
Size: 308020 Color: 917
Size: 281237 Color: 605

Bin 607: 11 of cap free
Amount of items: 3
Items: 
Size: 395794 Color: 1584
Size: 323291 Color: 1069
Size: 280905 Color: 598

Bin 608: 11 of cap free
Amount of items: 3
Items: 
Size: 362881 Color: 1395
Size: 325391 Color: 1084
Size: 311718 Color: 954

Bin 609: 11 of cap free
Amount of items: 3
Items: 
Size: 372015 Color: 1453
Size: 339244 Color: 1205
Size: 288731 Color: 707

Bin 610: 12 of cap free
Amount of items: 3
Items: 
Size: 372425 Color: 1458
Size: 325886 Color: 1087
Size: 301678 Color: 858

Bin 611: 13 of cap free
Amount of items: 3
Items: 
Size: 403482 Color: 1624
Size: 333340 Color: 1138
Size: 263166 Color: 335

Bin 612: 13 of cap free
Amount of items: 3
Items: 
Size: 409202 Color: 1652
Size: 316746 Color: 1005
Size: 274040 Color: 515

Bin 613: 13 of cap free
Amount of items: 3
Items: 
Size: 392599 Color: 1570
Size: 334602 Color: 1152
Size: 272787 Color: 494

Bin 614: 14 of cap free
Amount of items: 3
Items: 
Size: 391705 Color: 1568
Size: 307982 Color: 916
Size: 300300 Color: 845

Bin 615: 14 of cap free
Amount of items: 3
Items: 
Size: 340779 Color: 1218
Size: 336664 Color: 1179
Size: 322544 Color: 1064

Bin 616: 15 of cap free
Amount of items: 3
Items: 
Size: 368978 Color: 1435
Size: 336086 Color: 1175
Size: 294922 Color: 777

Bin 617: 15 of cap free
Amount of items: 3
Items: 
Size: 410570 Color: 1659
Size: 301292 Color: 853
Size: 288124 Color: 696

Bin 618: 16 of cap free
Amount of items: 3
Items: 
Size: 409530 Color: 1654
Size: 303946 Color: 882
Size: 286509 Color: 674

Bin 619: 16 of cap free
Amount of items: 3
Items: 
Size: 397026 Color: 1594
Size: 330236 Color: 1120
Size: 272723 Color: 493

Bin 620: 16 of cap free
Amount of items: 3
Items: 
Size: 364739 Color: 1401
Size: 364723 Color: 1400
Size: 270523 Color: 446

Bin 621: 17 of cap free
Amount of items: 3
Items: 
Size: 410505 Color: 1658
Size: 314950 Color: 984
Size: 274529 Color: 525

Bin 622: 17 of cap free
Amount of items: 3
Items: 
Size: 358204 Color: 1361
Size: 322523 Color: 1063
Size: 319257 Color: 1026

Bin 623: 18 of cap free
Amount of items: 3
Items: 
Size: 357834 Color: 1359
Size: 348120 Color: 1276
Size: 294029 Color: 771

Bin 624: 19 of cap free
Amount of items: 3
Items: 
Size: 390748 Color: 1564
Size: 310592 Color: 944
Size: 298642 Color: 824

Bin 625: 19 of cap free
Amount of items: 3
Items: 
Size: 354665 Color: 1336
Size: 350095 Color: 1295
Size: 295222 Color: 782

Bin 626: 22 of cap free
Amount of items: 3
Items: 
Size: 384785 Color: 1535
Size: 331563 Color: 1129
Size: 283631 Color: 636

Bin 627: 25 of cap free
Amount of items: 3
Items: 
Size: 378920 Color: 1496
Size: 321074 Color: 1049
Size: 299982 Color: 842

Bin 628: 28 of cap free
Amount of items: 3
Items: 
Size: 392858 Color: 1574
Size: 352307 Color: 1315
Size: 254808 Color: 157

Bin 629: 29 of cap free
Amount of items: 3
Items: 
Size: 362786 Color: 1393
Size: 346236 Color: 1266
Size: 290950 Color: 730

Bin 630: 32 of cap free
Amount of items: 3
Items: 
Size: 409471 Color: 1653
Size: 296502 Color: 795
Size: 293996 Color: 770

Bin 631: 32 of cap free
Amount of items: 3
Items: 
Size: 376880 Color: 1483
Size: 320172 Color: 1041
Size: 302917 Color: 869

Bin 632: 33 of cap free
Amount of items: 3
Items: 
Size: 348785 Color: 1283
Size: 330496 Color: 1122
Size: 320687 Color: 1044

Bin 633: 34 of cap free
Amount of items: 3
Items: 
Size: 408933 Color: 1649
Size: 334960 Color: 1156
Size: 256074 Color: 190

Bin 634: 39 of cap free
Amount of items: 3
Items: 
Size: 354809 Color: 1337
Size: 329261 Color: 1112
Size: 315892 Color: 996

Bin 635: 41 of cap free
Amount of items: 3
Items: 
Size: 408110 Color: 1642
Size: 311191 Color: 949
Size: 280659 Color: 592

Bin 636: 42 of cap free
Amount of items: 3
Items: 
Size: 405735 Color: 1634
Size: 307532 Color: 908
Size: 286692 Color: 678

Bin 637: 44 of cap free
Amount of items: 3
Items: 
Size: 371136 Color: 1449
Size: 327225 Color: 1098
Size: 301596 Color: 857

Bin 638: 50 of cap free
Amount of items: 3
Items: 
Size: 402444 Color: 1620
Size: 299969 Color: 841
Size: 297538 Color: 814

Bin 639: 51 of cap free
Amount of items: 3
Items: 
Size: 404533 Color: 1628
Size: 335668 Color: 1170
Size: 259749 Color: 283

Bin 640: 60 of cap free
Amount of items: 3
Items: 
Size: 359872 Color: 1374
Size: 339715 Color: 1209
Size: 300354 Color: 846

Bin 641: 62 of cap free
Amount of items: 3
Items: 
Size: 356833 Color: 1353
Size: 323972 Color: 1077
Size: 319134 Color: 1025

Bin 642: 67 of cap free
Amount of items: 3
Items: 
Size: 399340 Color: 1606
Size: 339234 Color: 1204
Size: 261360 Color: 307

Bin 643: 82 of cap free
Amount of items: 3
Items: 
Size: 352569 Color: 1319
Size: 331027 Color: 1125
Size: 316323 Color: 1001

Bin 644: 86 of cap free
Amount of items: 3
Items: 
Size: 495813 Color: 1983
Size: 253266 Color: 111
Size: 250836 Color: 36

Bin 645: 91 of cap free
Amount of items: 3
Items: 
Size: 370145 Color: 1444
Size: 334772 Color: 1154
Size: 294993 Color: 778

Bin 646: 94 of cap free
Amount of items: 3
Items: 
Size: 365462 Color: 1405
Size: 337872 Color: 1192
Size: 296573 Color: 796

Bin 647: 100 of cap free
Amount of items: 3
Items: 
Size: 353122 Color: 1325
Size: 337087 Color: 1183
Size: 309692 Color: 933

Bin 648: 107 of cap free
Amount of items: 3
Items: 
Size: 371524 Color: 1451
Size: 319466 Color: 1029
Size: 308904 Color: 928

Bin 649: 117 of cap free
Amount of items: 3
Items: 
Size: 356710 Color: 1352
Size: 344539 Color: 1254
Size: 298635 Color: 823

Bin 650: 126 of cap free
Amount of items: 3
Items: 
Size: 345427 Color: 1262
Size: 332643 Color: 1136
Size: 321805 Color: 1057

Bin 651: 143 of cap free
Amount of items: 3
Items: 
Size: 350443 Color: 1299
Size: 335580 Color: 1168
Size: 313835 Color: 972

Bin 652: 162 of cap free
Amount of items: 3
Items: 
Size: 377740 Color: 1490
Size: 311428 Color: 953
Size: 310671 Color: 945

Bin 653: 164 of cap free
Amount of items: 3
Items: 
Size: 380722 Color: 1508
Size: 323357 Color: 1071
Size: 295758 Color: 786

Bin 654: 170 of cap free
Amount of items: 3
Items: 
Size: 361339 Color: 1390
Size: 329430 Color: 1114
Size: 309062 Color: 930

Bin 655: 184 of cap free
Amount of items: 3
Items: 
Size: 376232 Color: 1477
Size: 338456 Color: 1198
Size: 285129 Color: 657

Bin 656: 185 of cap free
Amount of items: 3
Items: 
Size: 476135 Color: 1912
Size: 263742 Color: 345
Size: 259939 Color: 286

Bin 657: 207 of cap free
Amount of items: 3
Items: 
Size: 354410 Color: 1334
Size: 353830 Color: 1329
Size: 291554 Color: 743

Bin 658: 216 of cap free
Amount of items: 3
Items: 
Size: 386945 Color: 1541
Size: 315162 Color: 989
Size: 297678 Color: 817

Bin 659: 244 of cap free
Amount of items: 3
Items: 
Size: 341611 Color: 1229
Size: 337285 Color: 1186
Size: 320861 Color: 1048

Bin 660: 418 of cap free
Amount of items: 3
Items: 
Size: 345340 Color: 1261
Size: 340980 Color: 1219
Size: 313263 Color: 967

Bin 661: 446 of cap free
Amount of items: 3
Items: 
Size: 360538 Color: 1382
Size: 332592 Color: 1135
Size: 306425 Color: 899

Bin 662: 607 of cap free
Amount of items: 3
Items: 
Size: 396644 Color: 1590
Size: 303748 Color: 880
Size: 299002 Color: 829

Bin 663: 611 of cap free
Amount of items: 3
Items: 
Size: 381266 Color: 1516
Size: 310501 Color: 942
Size: 307623 Color: 911

Bin 664: 1385 of cap free
Amount of items: 3
Items: 
Size: 374809 Color: 1470
Size: 326958 Color: 1096
Size: 296849 Color: 802

Bin 665: 5001 of cap free
Amount of items: 3
Items: 
Size: 358543 Color: 1364
Size: 335333 Color: 1162
Size: 301124 Color: 851

Bin 666: 31288 of cap free
Amount of items: 3
Items: 
Size: 351435 Color: 1312
Size: 326230 Color: 1092
Size: 291048 Color: 734

Bin 667: 213242 of cap free
Amount of items: 2
Items: 
Size: 499741 Color: 2000
Size: 287018 Color: 682

Bin 668: 743122 of cap free
Amount of items: 1
Items: 
Size: 256879 Color: 208

Total size: 667000667
Total free space: 1000001

