Capicity Bin: 1864
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 941 Color: 2
Size: 737 Color: 4
Size: 186 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 2
Size: 602 Color: 3
Size: 44 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 0
Size: 529 Color: 3
Size: 104 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1286 Color: 0
Size: 542 Color: 3
Size: 36 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 2
Size: 388 Color: 4
Size: 86 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1443 Color: 4
Size: 349 Color: 0
Size: 72 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1498 Color: 1
Size: 246 Color: 4
Size: 120 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 1
Size: 302 Color: 1
Size: 56 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1538 Color: 0
Size: 202 Color: 3
Size: 124 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1565 Color: 0
Size: 251 Color: 4
Size: 48 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 4
Size: 274 Color: 2
Size: 20 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1619 Color: 3
Size: 181 Color: 4
Size: 64 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 4
Size: 204 Color: 3
Size: 34 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 2
Size: 193 Color: 3
Size: 38 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1636 Color: 1
Size: 182 Color: 3
Size: 46 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1642 Color: 3
Size: 146 Color: 1
Size: 76 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 0
Size: 157 Color: 0
Size: 58 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 3
Size: 163 Color: 1
Size: 32 Color: 4

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 981 Color: 1
Size: 738 Color: 3
Size: 144 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 989 Color: 4
Size: 778 Color: 1
Size: 96 Color: 4

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1109 Color: 0
Size: 694 Color: 1
Size: 60 Color: 3

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1168 Color: 4
Size: 619 Color: 1
Size: 76 Color: 3

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 4
Size: 631 Color: 3
Size: 30 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 3
Size: 359 Color: 0
Size: 88 Color: 2

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1435 Color: 3
Size: 428 Color: 4

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1447 Color: 0
Size: 380 Color: 4
Size: 36 Color: 2

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 1
Size: 288 Color: 3
Size: 84 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1511 Color: 4
Size: 208 Color: 1
Size: 144 Color: 3

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 0
Size: 152 Color: 4
Size: 132 Color: 3

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 1
Size: 261 Color: 3
Size: 8 Color: 2

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 0
Size: 221 Color: 2
Size: 8 Color: 1

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 1637 Color: 0
Size: 226 Color: 4

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 1650 Color: 2
Size: 213 Color: 0

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 2
Size: 205 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 2
Size: 150 Color: 1
Size: 36 Color: 3

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1551 Color: 1
Size: 311 Color: 0

Bin 37: 3 of cap free
Amount of items: 12
Items: 
Size: 375 Color: 1
Size: 370 Color: 1
Size: 306 Color: 1
Size: 136 Color: 2
Size: 116 Color: 2
Size: 108 Color: 4
Size: 104 Color: 4
Size: 100 Color: 3
Size: 70 Color: 1
Size: 68 Color: 4
Size: 56 Color: 0
Size: 52 Color: 3

Bin 38: 3 of cap free
Amount of items: 5
Items: 
Size: 933 Color: 2
Size: 632 Color: 1
Size: 154 Color: 3
Size: 74 Color: 0
Size: 68 Color: 4

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 982 Color: 0
Size: 879 Color: 3

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1135 Color: 2
Size: 666 Color: 2
Size: 60 Color: 4

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 2
Size: 443 Color: 3
Size: 88 Color: 4

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 2
Size: 422 Color: 3
Size: 84 Color: 1

Bin 43: 3 of cap free
Amount of items: 4
Items: 
Size: 1358 Color: 4
Size: 431 Color: 1
Size: 40 Color: 4
Size: 32 Color: 3

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 1519 Color: 0
Size: 330 Color: 2
Size: 12 Color: 1

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 4
Size: 250 Color: 0
Size: 12 Color: 0

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 1621 Color: 4
Size: 239 Color: 0

Bin 47: 5 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 2
Size: 611 Color: 4
Size: 102 Color: 1

Bin 48: 5 of cap free
Amount of items: 3
Items: 
Size: 1333 Color: 3
Size: 482 Color: 4
Size: 44 Color: 2

Bin 49: 5 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 0
Size: 382 Color: 2
Size: 62 Color: 3

Bin 50: 6 of cap free
Amount of items: 2
Items: 
Size: 1249 Color: 3
Size: 609 Color: 0

Bin 51: 8 of cap free
Amount of items: 3
Items: 
Size: 961 Color: 2
Size: 773 Color: 1
Size: 122 Color: 3

Bin 52: 9 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 0
Size: 194 Color: 4
Size: 191 Color: 4

Bin 53: 9 of cap free
Amount of items: 2
Items: 
Size: 1566 Color: 2
Size: 289 Color: 3

Bin 54: 10 of cap free
Amount of items: 4
Items: 
Size: 1034 Color: 4
Size: 752 Color: 2
Size: 36 Color: 1
Size: 32 Color: 2

Bin 55: 10 of cap free
Amount of items: 2
Items: 
Size: 1347 Color: 1
Size: 507 Color: 0

Bin 56: 12 of cap free
Amount of items: 5
Items: 
Size: 1123 Color: 1
Size: 295 Color: 0
Size: 196 Color: 0
Size: 194 Color: 2
Size: 44 Color: 2

Bin 57: 12 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 3
Size: 446 Color: 4

Bin 58: 17 of cap free
Amount of items: 2
Items: 
Size: 1422 Color: 2
Size: 425 Color: 3

Bin 59: 18 of cap free
Amount of items: 2
Items: 
Size: 1292 Color: 2
Size: 554 Color: 0

Bin 60: 20 of cap free
Amount of items: 3
Items: 
Size: 937 Color: 1
Size: 753 Color: 4
Size: 154 Color: 3

Bin 61: 21 of cap free
Amount of items: 2
Items: 
Size: 1066 Color: 2
Size: 777 Color: 4

Bin 62: 21 of cap free
Amount of items: 3
Items: 
Size: 1131 Color: 1
Size: 398 Color: 0
Size: 314 Color: 0

Bin 63: 23 of cap free
Amount of items: 2
Items: 
Size: 1490 Color: 0
Size: 351 Color: 3

Bin 64: 24 of cap free
Amount of items: 3
Items: 
Size: 1257 Color: 3
Size: 513 Color: 2
Size: 70 Color: 4

Bin 65: 25 of cap free
Amount of items: 4
Items: 
Size: 934 Color: 2
Size: 731 Color: 4
Size: 122 Color: 1
Size: 52 Color: 3

Bin 66: 1554 of cap free
Amount of items: 6
Items: 
Size: 68 Color: 2
Size: 68 Color: 0
Size: 48 Color: 4
Size: 48 Color: 3
Size: 48 Color: 3
Size: 30 Color: 2

Total size: 121160
Total free space: 1864

