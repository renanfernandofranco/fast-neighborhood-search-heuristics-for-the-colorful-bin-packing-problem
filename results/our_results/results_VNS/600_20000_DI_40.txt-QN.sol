Capicity Bin: 15600
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 460
Size: 3378 Color: 336
Size: 978 Color: 175

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11512 Color: 467
Size: 2116 Color: 273
Size: 1972 Color: 265

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11794 Color: 478
Size: 3402 Color: 337
Size: 404 Color: 78

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12068 Color: 485
Size: 3308 Color: 334
Size: 224 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12120 Color: 486
Size: 2948 Color: 320
Size: 532 Color: 114

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12226 Color: 489
Size: 2962 Color: 322
Size: 412 Color: 80

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12347 Color: 492
Size: 2961 Color: 321
Size: 292 Color: 29

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12367 Color: 494
Size: 2661 Color: 306
Size: 572 Color: 122

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12371 Color: 495
Size: 2695 Color: 310
Size: 534 Color: 116

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12454 Color: 500
Size: 1732 Color: 246
Size: 1414 Color: 211

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12460 Color: 501
Size: 2500 Color: 296
Size: 640 Color: 137

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12478 Color: 502
Size: 2874 Color: 316
Size: 248 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12492 Color: 503
Size: 1562 Color: 227
Size: 1546 Color: 225

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12563 Color: 506
Size: 2679 Color: 308
Size: 358 Color: 64

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12600 Color: 507
Size: 2480 Color: 294
Size: 520 Color: 108

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 512
Size: 2504 Color: 297
Size: 352 Color: 62

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12746 Color: 513
Size: 2568 Color: 301
Size: 286 Color: 24

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12756 Color: 514
Size: 2348 Color: 287
Size: 496 Color: 103

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12951 Color: 522
Size: 2209 Color: 280
Size: 440 Color: 88

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 523
Size: 2620 Color: 304
Size: 24 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13036 Color: 528
Size: 2260 Color: 282
Size: 304 Color: 36

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13159 Color: 534
Size: 2035 Color: 270
Size: 406 Color: 79

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13191 Color: 535
Size: 1513 Color: 222
Size: 896 Color: 168

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 540
Size: 1304 Color: 203
Size: 1040 Color: 183

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13346 Color: 544
Size: 1800 Color: 250
Size: 454 Color: 90

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13426 Color: 546
Size: 1298 Color: 202
Size: 876 Color: 166

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 548
Size: 1882 Color: 257
Size: 282 Color: 22

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13444 Color: 549
Size: 1476 Color: 219
Size: 680 Color: 142

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13447 Color: 550
Size: 1811 Color: 253
Size: 342 Color: 59

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13486 Color: 553
Size: 1390 Color: 209
Size: 724 Color: 150

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 554
Size: 1528 Color: 223
Size: 558 Color: 120

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13567 Color: 558
Size: 1605 Color: 231
Size: 428 Color: 84

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 559
Size: 1644 Color: 237
Size: 384 Color: 70

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13575 Color: 560
Size: 1553 Color: 226
Size: 472 Color: 97

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13598 Color: 562
Size: 1506 Color: 220
Size: 496 Color: 100

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13636 Color: 565
Size: 1332 Color: 206
Size: 632 Color: 133

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13654 Color: 567
Size: 1434 Color: 213
Size: 512 Color: 106

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13659 Color: 568
Size: 1419 Color: 212
Size: 522 Color: 109

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13678 Color: 571
Size: 1602 Color: 229
Size: 320 Color: 46

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13684 Color: 572
Size: 1324 Color: 205
Size: 592 Color: 128

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 574
Size: 1565 Color: 228
Size: 312 Color: 43

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13726 Color: 575
Size: 1368 Color: 208
Size: 506 Color: 104

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13746 Color: 577
Size: 1144 Color: 191
Size: 710 Color: 147

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 578
Size: 1532 Color: 224
Size: 304 Color: 37

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13821 Color: 583
Size: 1459 Color: 215
Size: 320 Color: 44

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 584
Size: 1460 Color: 216
Size: 304 Color: 38

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 586
Size: 1296 Color: 199
Size: 456 Color: 92

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13851 Color: 587
Size: 1439 Color: 214
Size: 310 Color: 40

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13982 Color: 596
Size: 1296 Color: 197
Size: 322 Color: 49

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14004 Color: 597
Size: 1072 Color: 184
Size: 524 Color: 110

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14018 Color: 598
Size: 998 Color: 179
Size: 584 Color: 125

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 599
Size: 984 Color: 178
Size: 596 Color: 129

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 600
Size: 1144 Color: 190
Size: 416 Color: 81

Bin 54: 1 of cap free
Amount of items: 7
Items: 
Size: 7809 Color: 410
Size: 1789 Color: 248
Size: 1635 Color: 236
Size: 1619 Color: 234
Size: 1611 Color: 233
Size: 784 Color: 156
Size: 352 Color: 61

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 10600 Color: 449
Size: 4999 Color: 375

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 462
Size: 2204 Color: 279
Size: 2123 Color: 274

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 12391 Color: 497
Size: 3208 Color: 331

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12842 Color: 517
Size: 2213 Color: 281
Size: 544 Color: 119

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 12977 Color: 524
Size: 2622 Color: 305

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 12997 Color: 526
Size: 2602 Color: 303

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 13068 Color: 530
Size: 2531 Color: 298

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 13154 Color: 533
Size: 2445 Color: 292

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13319 Color: 543
Size: 2280 Color: 284

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13455 Color: 552
Size: 2144 Color: 275

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13539 Color: 556
Size: 1604 Color: 230
Size: 456 Color: 91

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 13639 Color: 566
Size: 1960 Color: 264

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 13667 Color: 569
Size: 1932 Color: 263

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 13785 Color: 580
Size: 1814 Color: 254

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 11724 Color: 473
Size: 2892 Color: 317
Size: 982 Color: 177

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 11892 Color: 479
Size: 3706 Color: 347

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 12050 Color: 484
Size: 3156 Color: 327
Size: 392 Color: 76

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 13226 Color: 538
Size: 2372 Color: 290

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 13427 Color: 547
Size: 2171 Color: 277

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13524 Color: 555
Size: 2074 Color: 272

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13606 Color: 564
Size: 1992 Color: 267

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13675 Color: 570
Size: 1923 Color: 262

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13794 Color: 582
Size: 1804 Color: 251

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13976 Color: 595
Size: 1622 Color: 235

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 10939 Color: 456
Size: 4402 Color: 362
Size: 256 Color: 10

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 12023 Color: 482
Size: 3574 Color: 342

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 12132 Color: 487
Size: 3465 Color: 339

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 12243 Color: 490
Size: 3354 Color: 335

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 12410 Color: 499
Size: 3187 Color: 329

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 13737 Color: 576
Size: 1860 Color: 256

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 13793 Color: 581
Size: 1804 Color: 252

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 13842 Color: 585
Size: 1755 Color: 247

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13908 Color: 592
Size: 1689 Color: 242

Bin 88: 4 of cap free
Amount of items: 5
Items: 
Size: 7816 Color: 413
Size: 6472 Color: 393
Size: 624 Color: 132
Size: 344 Color: 60
Size: 340 Color: 58

Bin 89: 4 of cap free
Amount of items: 5
Items: 
Size: 9735 Color: 437
Size: 4889 Color: 369
Size: 372 Color: 67
Size: 300 Color: 31
Size: 300 Color: 30

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 10396 Color: 446
Size: 4932 Color: 374
Size: 268 Color: 17

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 10756 Color: 450
Size: 4840 Color: 367

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 12360 Color: 493
Size: 3236 Color: 332

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 13114 Color: 531
Size: 2482 Color: 295

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 13934 Color: 594
Size: 1662 Color: 238

Bin 95: 5 of cap free
Amount of items: 11
Items: 
Size: 7801 Color: 405
Size: 1008 Color: 181
Size: 1000 Color: 180
Size: 978 Color: 176
Size: 976 Color: 174
Size: 976 Color: 173
Size: 960 Color: 172
Size: 728 Color: 151
Size: 392 Color: 75
Size: 392 Color: 74
Size: 384 Color: 73

Bin 96: 5 of cap free
Amount of items: 3
Items: 
Size: 10457 Color: 447
Size: 4874 Color: 368
Size: 264 Color: 16

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 13293 Color: 542
Size: 2302 Color: 286

Bin 98: 6 of cap free
Amount of items: 21
Items: 
Size: 960 Color: 171
Size: 912 Color: 170
Size: 912 Color: 169
Size: 888 Color: 167
Size: 876 Color: 165
Size: 864 Color: 164
Size: 864 Color: 163
Size: 856 Color: 162
Size: 848 Color: 161
Size: 832 Color: 160
Size: 800 Color: 159
Size: 794 Color: 158
Size: 784 Color: 157
Size: 776 Color: 155
Size: 776 Color: 154
Size: 740 Color: 152
Size: 432 Color: 86
Size: 432 Color: 85
Size: 424 Color: 83
Size: 424 Color: 82
Size: 400 Color: 77

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 12846 Color: 518
Size: 2748 Color: 313

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 13026 Color: 527
Size: 2568 Color: 300

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 13448 Color: 551
Size: 2146 Color: 276

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 13875 Color: 588
Size: 1719 Color: 245

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 13899 Color: 590
Size: 1695 Color: 243

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 591
Size: 1688 Color: 241

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 593
Size: 1670 Color: 240

Bin 106: 7 of cap free
Amount of items: 9
Items: 
Size: 7805 Color: 408
Size: 1412 Color: 210
Size: 1322 Color: 204
Size: 1298 Color: 201
Size: 1298 Color: 200
Size: 1296 Color: 198
Size: 440 Color: 87
Size: 362 Color: 66
Size: 360 Color: 65

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 11253 Color: 461
Size: 4340 Color: 360

Bin 108: 7 of cap free
Amount of items: 2
Items: 
Size: 11777 Color: 477
Size: 3816 Color: 348

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 12622 Color: 509
Size: 2971 Color: 323

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 12779 Color: 515
Size: 2814 Color: 315

Bin 111: 8 of cap free
Amount of items: 9
Items: 
Size: 7804 Color: 407
Size: 1296 Color: 196
Size: 1296 Color: 195
Size: 1280 Color: 194
Size: 1280 Color: 193
Size: 1216 Color: 192
Size: 656 Color: 139
Size: 384 Color: 69
Size: 380 Color: 68

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 11636 Color: 470
Size: 3956 Color: 352

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13550 Color: 557
Size: 2042 Color: 271

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13882 Color: 589
Size: 1710 Color: 244

Bin 115: 9 of cap free
Amount of items: 2
Items: 
Size: 11706 Color: 472
Size: 3885 Color: 349

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 537
Size: 2367 Color: 289

Bin 117: 10 of cap free
Amount of items: 3
Items: 
Size: 10926 Color: 455
Size: 4408 Color: 363
Size: 256 Color: 11

Bin 118: 10 of cap free
Amount of items: 3
Items: 
Size: 11443 Color: 466
Size: 3971 Color: 353
Size: 176 Color: 4

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 13768 Color: 579
Size: 1822 Color: 255

Bin 120: 11 of cap free
Amount of items: 2
Items: 
Size: 13405 Color: 545
Size: 2184 Color: 278

Bin 121: 11 of cap free
Amount of items: 2
Items: 
Size: 13688 Color: 573
Size: 1901 Color: 261

Bin 122: 12 of cap free
Amount of items: 4
Items: 
Size: 9324 Color: 427
Size: 5640 Color: 382
Size: 312 Color: 42
Size: 312 Color: 41

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 12037 Color: 483
Size: 3551 Color: 340

Bin 124: 12 of cap free
Amount of items: 2
Items: 
Size: 12387 Color: 496
Size: 3201 Color: 330

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 12496 Color: 504
Size: 3092 Color: 326

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 12926 Color: 521
Size: 2662 Color: 307

Bin 127: 13 of cap free
Amount of items: 3
Items: 
Size: 9707 Color: 433
Size: 5578 Color: 380
Size: 302 Color: 32

Bin 128: 13 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 516
Size: 2799 Color: 314

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 13053 Color: 529
Size: 2534 Color: 299

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 13139 Color: 532
Size: 2448 Color: 293

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 13236 Color: 539
Size: 2351 Color: 288

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 11332 Color: 464
Size: 4254 Color: 358

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 13204 Color: 536
Size: 2382 Color: 291

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 563
Size: 1982 Color: 266

Bin 135: 15 of cap free
Amount of items: 3
Items: 
Size: 10218 Color: 441
Size: 5083 Color: 377
Size: 284 Color: 23

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 12604 Color: 508
Size: 2981 Color: 324

Bin 137: 15 of cap free
Amount of items: 2
Items: 
Size: 12873 Color: 520
Size: 2712 Color: 312

Bin 138: 15 of cap free
Amount of items: 2
Items: 
Size: 13576 Color: 561
Size: 2009 Color: 269

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 12020 Color: 481
Size: 3564 Color: 341

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 505
Size: 3048 Color: 325

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 12672 Color: 511
Size: 2912 Color: 319

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 11960 Color: 480
Size: 3623 Color: 344

Bin 143: 17 of cap free
Amount of items: 2
Items: 
Size: 12872 Color: 519
Size: 2711 Color: 311

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 541
Size: 2298 Color: 285

Bin 145: 19 of cap free
Amount of items: 4
Items: 
Size: 9320 Color: 426
Size: 5621 Color: 381
Size: 320 Color: 47
Size: 320 Color: 45

Bin 146: 19 of cap free
Amount of items: 3
Items: 
Size: 11226 Color: 459
Size: 2687 Color: 309
Size: 1668 Color: 239

Bin 147: 19 of cap free
Amount of items: 2
Items: 
Size: 12407 Color: 498
Size: 3174 Color: 328

Bin 148: 20 of cap free
Amount of items: 2
Items: 
Size: 11682 Color: 471
Size: 3898 Color: 350

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 12984 Color: 525
Size: 2596 Color: 302

Bin 150: 26 of cap free
Amount of items: 2
Items: 
Size: 12308 Color: 491
Size: 3266 Color: 333

Bin 151: 28 of cap free
Amount of items: 3
Items: 
Size: 10498 Color: 448
Size: 4810 Color: 366
Size: 264 Color: 15

Bin 152: 29 of cap free
Amount of items: 2
Items: 
Size: 11284 Color: 463
Size: 4287 Color: 359

Bin 153: 29 of cap free
Amount of items: 2
Items: 
Size: 12667 Color: 510
Size: 2904 Color: 318

Bin 154: 30 of cap free
Amount of items: 2
Items: 
Size: 12154 Color: 488
Size: 3416 Color: 338

Bin 155: 34 of cap free
Amount of items: 3
Items: 
Size: 9830 Color: 439
Size: 5448 Color: 379
Size: 288 Color: 27

Bin 156: 37 of cap free
Amount of items: 3
Items: 
Size: 9603 Color: 431
Size: 5656 Color: 384
Size: 304 Color: 33

Bin 157: 38 of cap free
Amount of items: 2
Items: 
Size: 11518 Color: 468
Size: 4044 Color: 355

Bin 158: 40 of cap free
Amount of items: 2
Items: 
Size: 9684 Color: 432
Size: 5876 Color: 390

Bin 159: 40 of cap free
Amount of items: 3
Items: 
Size: 10860 Color: 454
Size: 4444 Color: 364
Size: 256 Color: 12

Bin 160: 43 of cap free
Amount of items: 7
Items: 
Size: 7806 Color: 409
Size: 1608 Color: 232
Size: 1507 Color: 221
Size: 1466 Color: 218
Size: 1464 Color: 217
Size: 1350 Color: 207
Size: 356 Color: 63

Bin 161: 44 of cap free
Amount of items: 3
Items: 
Size: 8733 Color: 419
Size: 6497 Color: 397
Size: 326 Color: 51

Bin 162: 44 of cap free
Amount of items: 2
Items: 
Size: 9732 Color: 436
Size: 5824 Color: 389

Bin 163: 45 of cap free
Amount of items: 4
Items: 
Size: 10088 Color: 440
Size: 4891 Color: 370
Size: 288 Color: 26
Size: 288 Color: 25

Bin 164: 46 of cap free
Amount of items: 3
Items: 
Size: 10268 Color: 442
Size: 5006 Color: 376
Size: 280 Color: 21

Bin 165: 46 of cap free
Amount of items: 2
Items: 
Size: 11578 Color: 469
Size: 3976 Color: 354

Bin 166: 48 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 458
Size: 4174 Color: 357
Size: 224 Color: 6

Bin 167: 53 of cap free
Amount of items: 3
Items: 
Size: 8726 Color: 418
Size: 6493 Color: 396
Size: 328 Color: 52

Bin 168: 54 of cap free
Amount of items: 3
Items: 
Size: 8722 Color: 417
Size: 6492 Color: 395
Size: 332 Color: 53

Bin 169: 58 of cap free
Amount of items: 3
Items: 
Size: 9594 Color: 430
Size: 5644 Color: 383
Size: 304 Color: 34

Bin 170: 65 of cap free
Amount of items: 3
Items: 
Size: 9501 Color: 429
Size: 5730 Color: 386
Size: 304 Color: 35

Bin 171: 72 of cap free
Amount of items: 3
Items: 
Size: 11754 Color: 475
Size: 3646 Color: 345
Size: 128 Color: 2

Bin 172: 77 of cap free
Amount of items: 3
Items: 
Size: 8700 Color: 416
Size: 6491 Color: 394
Size: 332 Color: 54

Bin 173: 78 of cap free
Amount of items: 3
Items: 
Size: 9196 Color: 425
Size: 6004 Color: 391
Size: 322 Color: 48

Bin 174: 79 of cap free
Amount of items: 3
Items: 
Size: 9490 Color: 428
Size: 5723 Color: 385
Size: 308 Color: 39

Bin 175: 86 of cap free
Amount of items: 3
Items: 
Size: 10335 Color: 445
Size: 4911 Color: 373
Size: 268 Color: 18

Bin 176: 91 of cap free
Amount of items: 2
Items: 
Size: 7813 Color: 412
Size: 7696 Color: 404

Bin 177: 96 of cap free
Amount of items: 3
Items: 
Size: 8296 Color: 415
Size: 6872 Color: 403
Size: 336 Color: 55

Bin 178: 101 of cap free
Amount of items: 3
Items: 
Size: 10328 Color: 444
Size: 4895 Color: 372
Size: 276 Color: 19

Bin 179: 101 of cap free
Amount of items: 3
Items: 
Size: 11339 Color: 465
Size: 3944 Color: 351
Size: 216 Color: 5

Bin 180: 104 of cap free
Amount of items: 3
Items: 
Size: 11752 Color: 474
Size: 3604 Color: 343
Size: 140 Color: 3

Bin 181: 106 of cap free
Amount of items: 3
Items: 
Size: 10322 Color: 443
Size: 4892 Color: 371
Size: 280 Color: 20

Bin 182: 108 of cap free
Amount of items: 3
Items: 
Size: 9080 Color: 424
Size: 6088 Color: 392
Size: 324 Color: 50

Bin 183: 109 of cap free
Amount of items: 3
Items: 
Size: 11759 Color: 476
Size: 3652 Color: 346
Size: 80 Color: 1

Bin 184: 113 of cap free
Amount of items: 2
Items: 
Size: 9731 Color: 435
Size: 5756 Color: 388

Bin 185: 115 of cap free
Amount of items: 3
Items: 
Size: 10840 Color: 453
Size: 4389 Color: 361
Size: 256 Color: 13

Bin 186: 139 of cap free
Amount of items: 2
Items: 
Size: 9727 Color: 434
Size: 5734 Color: 387

Bin 187: 160 of cap free
Amount of items: 3
Items: 
Size: 11032 Color: 457
Size: 4168 Color: 356
Size: 240 Color: 8

Bin 188: 165 of cap free
Amount of items: 2
Items: 
Size: 10835 Color: 452
Size: 4600 Color: 365

Bin 189: 214 of cap free
Amount of items: 4
Items: 
Size: 7848 Color: 414
Size: 6864 Color: 402
Size: 338 Color: 57
Size: 336 Color: 56

Bin 190: 222 of cap free
Amount of items: 2
Items: 
Size: 8876 Color: 423
Size: 6502 Color: 401

Bin 191: 241 of cap free
Amount of items: 4
Items: 
Size: 10822 Color: 451
Size: 2273 Color: 283
Size: 2004 Color: 268
Size: 260 Color: 14

Bin 192: 244 of cap free
Amount of items: 2
Items: 
Size: 8855 Color: 422
Size: 6501 Color: 400

Bin 193: 260 of cap free
Amount of items: 2
Items: 
Size: 8840 Color: 421
Size: 6500 Color: 399

Bin 194: 266 of cap free
Amount of items: 26
Items: 
Size: 752 Color: 153
Size: 720 Color: 149
Size: 712 Color: 148
Size: 704 Color: 146
Size: 704 Color: 145
Size: 692 Color: 144
Size: 688 Color: 143
Size: 672 Color: 141
Size: 668 Color: 140
Size: 652 Color: 138
Size: 640 Color: 136
Size: 640 Color: 135
Size: 636 Color: 134
Size: 616 Color: 131
Size: 528 Color: 111
Size: 520 Color: 107
Size: 508 Color: 105
Size: 496 Color: 102
Size: 496 Color: 101
Size: 488 Color: 99
Size: 480 Color: 98
Size: 472 Color: 96
Size: 470 Color: 95
Size: 468 Color: 94
Size: 464 Color: 93
Size: 448 Color: 89

Bin 195: 274 of cap free
Amount of items: 2
Items: 
Size: 8828 Color: 420
Size: 6498 Color: 398

Bin 196: 274 of cap free
Amount of items: 3
Items: 
Size: 9800 Color: 438
Size: 5236 Color: 378
Size: 290 Color: 28

Bin 197: 324 of cap free
Amount of items: 5
Items: 
Size: 7812 Color: 411
Size: 1891 Color: 260
Size: 1890 Color: 259
Size: 1888 Color: 258
Size: 1795 Color: 249

Bin 198: 354 of cap free
Amount of items: 9
Items: 
Size: 7802 Color: 406
Size: 1144 Color: 189
Size: 1144 Color: 188
Size: 1128 Color: 187
Size: 1124 Color: 186
Size: 1120 Color: 185
Size: 1016 Color: 182
Size: 384 Color: 72
Size: 384 Color: 71

Bin 199: 9436 of cap free
Amount of items: 11
Items: 
Size: 604 Color: 130
Size: 592 Color: 127
Size: 588 Color: 126
Size: 576 Color: 124
Size: 576 Color: 123
Size: 560 Color: 121
Size: 542 Color: 118
Size: 538 Color: 117
Size: 532 Color: 115
Size: 528 Color: 113
Size: 528 Color: 112

Total size: 3088800
Total free space: 15600

