Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 6
Size: 261 Color: 13
Size: 251 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 7
Size: 259 Color: 18
Size: 273 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 8
Size: 269 Color: 0
Size: 250 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 6
Size: 264 Color: 11
Size: 263 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 12
Size: 365 Color: 3
Size: 258 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 19
Size: 330 Color: 7
Size: 254 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 16
Size: 320 Color: 1
Size: 286 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 17
Size: 270 Color: 3
Size: 250 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 319 Color: 10
Size: 271 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 8
Size: 346 Color: 5
Size: 291 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 354 Color: 18
Size: 272 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 13
Size: 306 Color: 7
Size: 297 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 16
Size: 275 Color: 0
Size: 263 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 12
Size: 255 Color: 2
Size: 251 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 18
Size: 266 Color: 16
Size: 255 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 10
Size: 259 Color: 1
Size: 252 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 18
Size: 252 Color: 17
Size: 250 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 17
Size: 341 Color: 11
Size: 262 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9
Size: 256 Color: 17
Size: 254 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 17
Size: 262 Color: 8
Size: 261 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 18
Size: 265 Color: 10
Size: 251 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 6
Size: 262 Color: 6
Size: 258 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 15
Size: 357 Color: 4
Size: 262 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 3
Size: 302 Color: 4
Size: 258 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7
Size: 339 Color: 10
Size: 288 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 4
Size: 287 Color: 14
Size: 251 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 10
Size: 281 Color: 19
Size: 251 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 6
Size: 297 Color: 7
Size: 254 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 19
Size: 302 Color: 18
Size: 283 Color: 9

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9
Size: 274 Color: 1
Size: 264 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 266 Color: 19
Size: 264 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 13
Size: 274 Color: 2
Size: 260 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 8
Size: 261 Color: 4
Size: 259 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 6
Size: 318 Color: 12
Size: 311 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 14
Size: 371 Color: 0
Size: 250 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 14
Size: 304 Color: 19
Size: 268 Color: 12

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 18
Size: 296 Color: 11
Size: 256 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 3
Size: 266 Color: 19
Size: 252 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 19
Size: 282 Color: 0
Size: 251 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 11
Size: 266 Color: 10
Size: 252 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 5
Size: 294 Color: 10
Size: 252 Color: 18

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 15
Size: 318 Color: 8
Size: 258 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9
Size: 251 Color: 15
Size: 250 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8
Size: 300 Color: 8
Size: 289 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 17
Size: 288 Color: 18
Size: 252 Color: 9

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 16
Size: 298 Color: 15
Size: 257 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 13
Size: 276 Color: 0
Size: 255 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 12
Size: 287 Color: 0
Size: 283 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 17
Size: 361 Color: 12
Size: 273 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 14
Size: 357 Color: 6
Size: 272 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 319 Color: 15
Size: 318 Color: 17

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 6
Size: 258 Color: 0
Size: 252 Color: 14

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 8
Size: 262 Color: 10
Size: 258 Color: 5

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 15
Size: 309 Color: 5
Size: 250 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 18
Size: 362 Color: 11
Size: 253 Color: 14

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 345 Color: 11
Size: 276 Color: 12

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7
Size: 344 Color: 14
Size: 281 Color: 19

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 3
Size: 344 Color: 5
Size: 287 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 19
Size: 277 Color: 8
Size: 265 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 10
Size: 330 Color: 18
Size: 282 Color: 18

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 9
Size: 348 Color: 7
Size: 298 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 18
Size: 325 Color: 9
Size: 292 Color: 12

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 6
Size: 348 Color: 1
Size: 274 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 12
Size: 348 Color: 1
Size: 289 Color: 8

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 11
Size: 255 Color: 9
Size: 254 Color: 5

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 18
Size: 296 Color: 5
Size: 279 Color: 16

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 16
Size: 302 Color: 9
Size: 263 Color: 14

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 3
Size: 312 Color: 9
Size: 257 Color: 13

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 13
Size: 269 Color: 17
Size: 252 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 341 Color: 13
Size: 273 Color: 5

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 360 Color: 12
Size: 276 Color: 15

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 326 Color: 2
Size: 262 Color: 10

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 11
Size: 297 Color: 14
Size: 279 Color: 16

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 16
Size: 308 Color: 4
Size: 259 Color: 13

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 256 Color: 11
Size: 253 Color: 19

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 12
Size: 328 Color: 17
Size: 260 Color: 11

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 14
Size: 353 Color: 5
Size: 273 Color: 12

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 15
Size: 259 Color: 5
Size: 255 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 4
Size: 316 Color: 9
Size: 269 Color: 5

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 303 Color: 12
Size: 256 Color: 17

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 9
Size: 352 Color: 3
Size: 256 Color: 15

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 9
Size: 343 Color: 9
Size: 287 Color: 8

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 15
Size: 345 Color: 12
Size: 285 Color: 6

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9
Size: 278 Color: 5
Size: 257 Color: 18

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 7
Size: 289 Color: 1
Size: 250 Color: 16

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 5
Size: 335 Color: 1
Size: 294 Color: 18

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 10
Size: 329 Color: 15
Size: 267 Color: 5

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 12
Size: 257 Color: 6
Size: 254 Color: 4

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 15
Size: 277 Color: 9
Size: 257 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9
Size: 265 Color: 6
Size: 263 Color: 14

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 7
Size: 319 Color: 6
Size: 267 Color: 18

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 13
Size: 303 Color: 5
Size: 251 Color: 6

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 315 Color: 16
Size: 281 Color: 19

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 5
Size: 273 Color: 10
Size: 259 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 4
Size: 326 Color: 4
Size: 258 Color: 9

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 19
Size: 281 Color: 7
Size: 255 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 4
Size: 340 Color: 13
Size: 266 Color: 18

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 16
Size: 264 Color: 4
Size: 250 Color: 17

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 17
Size: 269 Color: 16
Size: 264 Color: 13

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 11
Size: 353 Color: 11
Size: 293 Color: 4

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 4
Size: 271 Color: 1
Size: 257 Color: 13

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 5
Size: 354 Color: 13
Size: 291 Color: 14

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 4
Size: 278 Color: 15
Size: 269 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 17
Size: 368 Color: 2
Size: 263 Color: 9

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 10
Size: 346 Color: 17
Size: 284 Color: 15

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 13
Size: 344 Color: 2
Size: 272 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 18
Size: 320 Color: 2
Size: 275 Color: 2

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 18
Size: 314 Color: 3
Size: 291 Color: 9

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 6
Size: 293 Color: 8
Size: 272 Color: 13

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 16
Size: 283 Color: 14
Size: 282 Color: 19

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 0
Size: 341 Color: 1
Size: 252 Color: 6

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 283 Color: 7
Size: 259 Color: 6

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 11
Size: 285 Color: 12
Size: 262 Color: 13

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 16
Size: 281 Color: 16
Size: 258 Color: 6

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 2
Size: 282 Color: 0
Size: 267 Color: 16

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 6
Size: 296 Color: 10
Size: 276 Color: 2

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 277 Color: 8
Size: 257 Color: 10

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 19
Size: 303 Color: 4
Size: 262 Color: 11

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 276 Color: 0
Size: 256 Color: 3

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 9
Size: 322 Color: 11
Size: 251 Color: 5

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 16
Size: 285 Color: 2
Size: 263 Color: 12

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 276 Color: 16
Size: 257 Color: 9

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 17
Size: 354 Color: 17
Size: 253 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 9
Size: 361 Color: 19
Size: 272 Color: 15

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 315 Color: 15
Size: 279 Color: 16

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 3
Size: 342 Color: 13
Size: 262 Color: 8

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 6
Size: 256 Color: 2
Size: 252 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 5
Size: 351 Color: 19
Size: 287 Color: 8

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 278 Color: 14
Size: 268 Color: 8

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 12
Size: 262 Color: 18
Size: 254 Color: 9

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 14
Size: 267 Color: 0
Size: 257 Color: 18

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 14
Size: 319 Color: 4
Size: 308 Color: 16

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 15
Size: 356 Color: 4
Size: 252 Color: 18

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 13
Size: 351 Color: 7
Size: 275 Color: 16

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 15
Size: 277 Color: 9
Size: 250 Color: 19

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 279 Color: 6
Size: 269 Color: 16

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 339 Color: 18
Size: 255 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 18
Size: 344 Color: 0
Size: 261 Color: 9

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 17
Size: 374 Color: 3
Size: 251 Color: 5

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 331 Color: 17
Size: 258 Color: 18

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 19
Size: 302 Color: 18
Size: 267 Color: 16

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 323 Color: 4
Size: 295 Color: 5

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 13
Size: 293 Color: 7
Size: 289 Color: 17

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 8
Size: 261 Color: 11
Size: 254 Color: 19

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 16
Size: 318 Color: 0
Size: 312 Color: 9

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 6
Size: 259 Color: 13
Size: 257 Color: 7

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 6
Size: 314 Color: 19
Size: 257 Color: 7

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 15
Size: 290 Color: 4
Size: 250 Color: 15

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 313 Color: 14
Size: 250 Color: 9

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 355 Color: 8
Size: 259 Color: 14

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 18
Size: 271 Color: 3
Size: 259 Color: 14

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9
Size: 270 Color: 5
Size: 250 Color: 6

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 14
Size: 305 Color: 13
Size: 252 Color: 11

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 339 Color: 11
Size: 284 Color: 5

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 18
Size: 278 Color: 18
Size: 251 Color: 11

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 10
Size: 344 Color: 4
Size: 285 Color: 5

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 14
Size: 271 Color: 6
Size: 258 Color: 7

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 276 Color: 18
Size: 263 Color: 19

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 13
Size: 321 Color: 19
Size: 319 Color: 9

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 15
Size: 276 Color: 0
Size: 260 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 315 Color: 19
Size: 309 Color: 9

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 5
Size: 370 Color: 4
Size: 251 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 9
Size: 355 Color: 16
Size: 258 Color: 12

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 14
Size: 329 Color: 14
Size: 265 Color: 13

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 11
Size: 287 Color: 14
Size: 271 Color: 9

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 16
Size: 294 Color: 0
Size: 260 Color: 7

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 10
Size: 253 Color: 14
Size: 252 Color: 11

Total size: 167000
Total free space: 0

