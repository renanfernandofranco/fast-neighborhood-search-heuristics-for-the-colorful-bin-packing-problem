Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 369 Color: 0
Size: 253 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 306 Color: 0
Size: 294 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 305 Color: 0
Size: 282 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 0
Size: 335 Color: 0
Size: 329 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 312 Color: 1
Size: 281 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 0
Size: 253 Color: 1
Size: 251 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 1
Size: 330 Color: 1
Size: 328 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 327 Color: 1
Size: 270 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 341 Color: 0
Size: 291 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 353 Color: 1
Size: 294 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 292 Color: 0
Size: 250 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 259 Color: 1
Size: 251 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 332 Color: 1
Size: 304 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 320 Color: 1
Size: 263 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 331 Color: 1
Size: 264 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 346 Color: 1
Size: 250 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 0
Size: 360 Color: 1
Size: 254 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 314 Color: 1
Size: 256 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 0
Size: 308 Color: 1
Size: 268 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 311 Color: 1
Size: 253 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 0
Size: 367 Color: 1
Size: 254 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 273 Color: 1
Size: 260 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 0
Size: 265 Color: 1
Size: 260 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 304 Color: 1
Size: 278 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 276 Color: 1
Size: 276 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 0
Size: 264 Color: 1
Size: 259 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 334 Color: 1
Size: 286 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 0
Size: 268 Color: 1
Size: 255 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 297 Color: 0
Size: 258 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 337 Color: 1
Size: 289 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 350 Color: 0
Size: 272 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 318 Color: 1
Size: 255 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 0
Size: 335 Color: 1
Size: 317 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 282 Color: 1
Size: 260 Color: 1

Total size: 34034
Total free space: 0

