Capicity Bin: 1000001
Lower Bound: 223

Bins used: 226
Amount of Colors: 5

Bin 1: 14 of cap free
Amount of items: 2
Items: 
Size: 596303 Color: 1
Size: 403684 Color: 2

Bin 2: 19 of cap free
Amount of items: 2
Items: 
Size: 667804 Color: 0
Size: 332178 Color: 3

Bin 3: 24 of cap free
Amount of items: 3
Items: 
Size: 659640 Color: 1
Size: 173652 Color: 2
Size: 166685 Color: 2

Bin 4: 30 of cap free
Amount of items: 3
Items: 
Size: 668931 Color: 1
Size: 167437 Color: 2
Size: 163603 Color: 4

Bin 5: 38 of cap free
Amount of items: 3
Items: 
Size: 349213 Color: 2
Size: 343292 Color: 1
Size: 307458 Color: 0

Bin 6: 47 of cap free
Amount of items: 2
Items: 
Size: 757510 Color: 1
Size: 242444 Color: 4

Bin 7: 54 of cap free
Amount of items: 2
Items: 
Size: 585548 Color: 4
Size: 414399 Color: 1

Bin 8: 71 of cap free
Amount of items: 3
Items: 
Size: 715502 Color: 4
Size: 144807 Color: 0
Size: 139621 Color: 4

Bin 9: 80 of cap free
Amount of items: 2
Items: 
Size: 620636 Color: 3
Size: 379285 Color: 4

Bin 10: 96 of cap free
Amount of items: 2
Items: 
Size: 744778 Color: 3
Size: 255127 Color: 1

Bin 11: 104 of cap free
Amount of items: 2
Items: 
Size: 536181 Color: 0
Size: 463716 Color: 1

Bin 12: 107 of cap free
Amount of items: 2
Items: 
Size: 526675 Color: 3
Size: 473219 Color: 0

Bin 13: 107 of cap free
Amount of items: 2
Items: 
Size: 629447 Color: 4
Size: 370447 Color: 2

Bin 14: 109 of cap free
Amount of items: 2
Items: 
Size: 723059 Color: 2
Size: 276833 Color: 4

Bin 15: 112 of cap free
Amount of items: 2
Items: 
Size: 687216 Color: 4
Size: 312673 Color: 2

Bin 16: 116 of cap free
Amount of items: 3
Items: 
Size: 654012 Color: 0
Size: 173430 Color: 2
Size: 172443 Color: 1

Bin 17: 127 of cap free
Amount of items: 2
Items: 
Size: 755802 Color: 3
Size: 244072 Color: 1

Bin 18: 128 of cap free
Amount of items: 2
Items: 
Size: 516930 Color: 4
Size: 482943 Color: 0

Bin 19: 129 of cap free
Amount of items: 3
Items: 
Size: 775571 Color: 4
Size: 117493 Color: 3
Size: 106808 Color: 1

Bin 20: 130 of cap free
Amount of items: 3
Items: 
Size: 759340 Color: 4
Size: 122473 Color: 1
Size: 118058 Color: 2

Bin 21: 147 of cap free
Amount of items: 2
Items: 
Size: 725564 Color: 0
Size: 274290 Color: 2

Bin 22: 154 of cap free
Amount of items: 2
Items: 
Size: 526453 Color: 4
Size: 473394 Color: 0

Bin 23: 157 of cap free
Amount of items: 3
Items: 
Size: 791770 Color: 2
Size: 106832 Color: 3
Size: 101242 Color: 1

Bin 24: 161 of cap free
Amount of items: 3
Items: 
Size: 717845 Color: 0
Size: 142927 Color: 1
Size: 139068 Color: 2

Bin 25: 174 of cap free
Amount of items: 2
Items: 
Size: 774129 Color: 1
Size: 225698 Color: 4

Bin 26: 179 of cap free
Amount of items: 2
Items: 
Size: 745429 Color: 3
Size: 254393 Color: 2

Bin 27: 201 of cap free
Amount of items: 2
Items: 
Size: 656750 Color: 0
Size: 343050 Color: 3

Bin 28: 202 of cap free
Amount of items: 3
Items: 
Size: 639062 Color: 1
Size: 182415 Color: 0
Size: 178322 Color: 3

Bin 29: 214 of cap free
Amount of items: 3
Items: 
Size: 748041 Color: 1
Size: 126727 Color: 1
Size: 125019 Color: 0

Bin 30: 256 of cap free
Amount of items: 2
Items: 
Size: 560029 Color: 1
Size: 439716 Color: 4

Bin 31: 260 of cap free
Amount of items: 2
Items: 
Size: 733422 Color: 4
Size: 266319 Color: 0

Bin 32: 275 of cap free
Amount of items: 2
Items: 
Size: 540746 Color: 1
Size: 458980 Color: 2

Bin 33: 300 of cap free
Amount of items: 2
Items: 
Size: 694738 Color: 0
Size: 304963 Color: 2

Bin 34: 319 of cap free
Amount of items: 2
Items: 
Size: 736389 Color: 1
Size: 263293 Color: 2

Bin 35: 329 of cap free
Amount of items: 2
Items: 
Size: 632790 Color: 1
Size: 366882 Color: 3

Bin 36: 339 of cap free
Amount of items: 2
Items: 
Size: 679414 Color: 1
Size: 320248 Color: 3

Bin 37: 387 of cap free
Amount of items: 3
Items: 
Size: 638098 Color: 2
Size: 192529 Color: 2
Size: 168987 Color: 1

Bin 38: 390 of cap free
Amount of items: 2
Items: 
Size: 592218 Color: 3
Size: 407393 Color: 1

Bin 39: 391 of cap free
Amount of items: 2
Items: 
Size: 505737 Color: 2
Size: 493873 Color: 3

Bin 40: 392 of cap free
Amount of items: 2
Items: 
Size: 766692 Color: 3
Size: 232917 Color: 4

Bin 41: 465 of cap free
Amount of items: 2
Items: 
Size: 749341 Color: 2
Size: 250195 Color: 3

Bin 42: 467 of cap free
Amount of items: 2
Items: 
Size: 696537 Color: 4
Size: 302997 Color: 0

Bin 43: 505 of cap free
Amount of items: 3
Items: 
Size: 507507 Color: 3
Size: 298117 Color: 0
Size: 193872 Color: 1

Bin 44: 506 of cap free
Amount of items: 2
Items: 
Size: 577784 Color: 2
Size: 421711 Color: 4

Bin 45: 511 of cap free
Amount of items: 2
Items: 
Size: 606379 Color: 4
Size: 393111 Color: 3

Bin 46: 519 of cap free
Amount of items: 2
Items: 
Size: 556707 Color: 3
Size: 442775 Color: 1

Bin 47: 525 of cap free
Amount of items: 3
Items: 
Size: 669118 Color: 3
Size: 165286 Color: 2
Size: 165072 Color: 3

Bin 48: 543 of cap free
Amount of items: 2
Items: 
Size: 690830 Color: 2
Size: 308628 Color: 0

Bin 49: 548 of cap free
Amount of items: 3
Items: 
Size: 613672 Color: 3
Size: 193320 Color: 0
Size: 192461 Color: 3

Bin 50: 569 of cap free
Amount of items: 3
Items: 
Size: 645504 Color: 4
Size: 187942 Color: 2
Size: 165986 Color: 1

Bin 51: 577 of cap free
Amount of items: 3
Items: 
Size: 556253 Color: 0
Size: 236727 Color: 1
Size: 206444 Color: 4

Bin 52: 587 of cap free
Amount of items: 2
Items: 
Size: 689018 Color: 3
Size: 310396 Color: 1

Bin 53: 599 of cap free
Amount of items: 2
Items: 
Size: 703659 Color: 1
Size: 295743 Color: 4

Bin 54: 602 of cap free
Amount of items: 3
Items: 
Size: 590432 Color: 4
Size: 214767 Color: 0
Size: 194200 Color: 4

Bin 55: 609 of cap free
Amount of items: 2
Items: 
Size: 538549 Color: 0
Size: 460843 Color: 1

Bin 56: 609 of cap free
Amount of items: 2
Items: 
Size: 780056 Color: 2
Size: 219336 Color: 0

Bin 57: 626 of cap free
Amount of items: 2
Items: 
Size: 686493 Color: 0
Size: 312882 Color: 4

Bin 58: 639 of cap free
Amount of items: 3
Items: 
Size: 483746 Color: 3
Size: 314323 Color: 0
Size: 201293 Color: 1

Bin 59: 651 of cap free
Amount of items: 3
Items: 
Size: 715055 Color: 2
Size: 145383 Color: 3
Size: 138912 Color: 4

Bin 60: 678 of cap free
Amount of items: 3
Items: 
Size: 796392 Color: 0
Size: 102425 Color: 1
Size: 100506 Color: 2

Bin 61: 681 of cap free
Amount of items: 3
Items: 
Size: 533776 Color: 0
Size: 236008 Color: 1
Size: 229536 Color: 4

Bin 62: 690 of cap free
Amount of items: 2
Items: 
Size: 516753 Color: 0
Size: 482558 Color: 1

Bin 63: 696 of cap free
Amount of items: 2
Items: 
Size: 736512 Color: 0
Size: 262793 Color: 3

Bin 64: 738 of cap free
Amount of items: 2
Items: 
Size: 793873 Color: 1
Size: 205390 Color: 3

Bin 65: 774 of cap free
Amount of items: 2
Items: 
Size: 641124 Color: 3
Size: 358103 Color: 0

Bin 66: 788 of cap free
Amount of items: 2
Items: 
Size: 540707 Color: 2
Size: 458506 Color: 4

Bin 67: 803 of cap free
Amount of items: 3
Items: 
Size: 647306 Color: 2
Size: 178893 Color: 0
Size: 172999 Color: 1

Bin 68: 973 of cap free
Amount of items: 3
Items: 
Size: 763649 Color: 0
Size: 133781 Color: 3
Size: 101598 Color: 1

Bin 69: 1003 of cap free
Amount of items: 2
Items: 
Size: 745967 Color: 0
Size: 253031 Color: 2

Bin 70: 1044 of cap free
Amount of items: 2
Items: 
Size: 761921 Color: 1
Size: 237036 Color: 3

Bin 71: 1085 of cap free
Amount of items: 2
Items: 
Size: 729585 Color: 3
Size: 269331 Color: 1

Bin 72: 1204 of cap free
Amount of items: 2
Items: 
Size: 582941 Color: 3
Size: 415856 Color: 0

Bin 73: 1262 of cap free
Amount of items: 2
Items: 
Size: 776943 Color: 2
Size: 221796 Color: 0

Bin 74: 1292 of cap free
Amount of items: 3
Items: 
Size: 365612 Color: 2
Size: 317626 Color: 3
Size: 315471 Color: 0

Bin 75: 1343 of cap free
Amount of items: 2
Items: 
Size: 789498 Color: 3
Size: 209160 Color: 0

Bin 76: 1349 of cap free
Amount of items: 3
Items: 
Size: 629099 Color: 0
Size: 191457 Color: 1
Size: 178096 Color: 2

Bin 77: 1383 of cap free
Amount of items: 2
Items: 
Size: 677923 Color: 0
Size: 320695 Color: 2

Bin 78: 1412 of cap free
Amount of items: 2
Items: 
Size: 676506 Color: 3
Size: 322083 Color: 1

Bin 79: 1452 of cap free
Amount of items: 2
Items: 
Size: 765600 Color: 4
Size: 232949 Color: 0

Bin 80: 1457 of cap free
Amount of items: 2
Items: 
Size: 742745 Color: 2
Size: 255799 Color: 0

Bin 81: 1460 of cap free
Amount of items: 2
Items: 
Size: 626756 Color: 4
Size: 371785 Color: 2

Bin 82: 1471 of cap free
Amount of items: 3
Items: 
Size: 447571 Color: 3
Size: 322127 Color: 2
Size: 228832 Color: 1

Bin 83: 1478 of cap free
Amount of items: 2
Items: 
Size: 569023 Color: 4
Size: 429500 Color: 0

Bin 84: 1538 of cap free
Amount of items: 2
Items: 
Size: 730159 Color: 2
Size: 268304 Color: 4

Bin 85: 1558 of cap free
Amount of items: 2
Items: 
Size: 616721 Color: 3
Size: 381722 Color: 1

Bin 86: 1565 of cap free
Amount of items: 3
Items: 
Size: 705881 Color: 4
Size: 148642 Color: 1
Size: 143913 Color: 0

Bin 87: 1566 of cap free
Amount of items: 2
Items: 
Size: 528896 Color: 4
Size: 469539 Color: 2

Bin 88: 1576 of cap free
Amount of items: 3
Items: 
Size: 353731 Color: 2
Size: 332586 Color: 3
Size: 312108 Color: 0

Bin 89: 1591 of cap free
Amount of items: 3
Items: 
Size: 637149 Color: 0
Size: 191677 Color: 1
Size: 169584 Color: 4

Bin 90: 1619 of cap free
Amount of items: 2
Items: 
Size: 558734 Color: 2
Size: 439648 Color: 0

Bin 91: 1707 of cap free
Amount of items: 3
Items: 
Size: 731256 Color: 1
Size: 133601 Color: 3
Size: 133437 Color: 0

Bin 92: 1750 of cap free
Amount of items: 2
Items: 
Size: 646569 Color: 2
Size: 351682 Color: 4

Bin 93: 1772 of cap free
Amount of items: 2
Items: 
Size: 546367 Color: 4
Size: 451862 Color: 0

Bin 94: 1777 of cap free
Amount of items: 2
Items: 
Size: 776566 Color: 1
Size: 221658 Color: 2

Bin 95: 1839 of cap free
Amount of items: 2
Items: 
Size: 561771 Color: 3
Size: 436391 Color: 2

Bin 96: 1839 of cap free
Amount of items: 2
Items: 
Size: 670557 Color: 2
Size: 327605 Color: 3

Bin 97: 1898 of cap free
Amount of items: 2
Items: 
Size: 500726 Color: 2
Size: 497377 Color: 3

Bin 98: 1976 of cap free
Amount of items: 2
Items: 
Size: 565437 Color: 1
Size: 432588 Color: 4

Bin 99: 1996 of cap free
Amount of items: 3
Items: 
Size: 675321 Color: 1
Size: 163751 Color: 2
Size: 158933 Color: 2

Bin 100: 2006 of cap free
Amount of items: 3
Items: 
Size: 708423 Color: 3
Size: 151850 Color: 1
Size: 137722 Color: 2

Bin 101: 2067 of cap free
Amount of items: 3
Items: 
Size: 782489 Color: 1
Size: 111215 Color: 2
Size: 104230 Color: 4

Bin 102: 2085 of cap free
Amount of items: 2
Items: 
Size: 614586 Color: 3
Size: 383330 Color: 1

Bin 103: 2100 of cap free
Amount of items: 3
Items: 
Size: 565242 Color: 0
Size: 224954 Color: 1
Size: 207705 Color: 3

Bin 104: 2195 of cap free
Amount of items: 2
Items: 
Size: 545256 Color: 4
Size: 452550 Color: 2

Bin 105: 2296 of cap free
Amount of items: 2
Items: 
Size: 732470 Color: 1
Size: 265235 Color: 2

Bin 106: 2333 of cap free
Amount of items: 2
Items: 
Size: 696384 Color: 4
Size: 301284 Color: 0

Bin 107: 2341 of cap free
Amount of items: 2
Items: 
Size: 747552 Color: 4
Size: 250108 Color: 3

Bin 108: 2341 of cap free
Amount of items: 2
Items: 
Size: 763200 Color: 1
Size: 234460 Color: 4

Bin 109: 2398 of cap free
Amount of items: 3
Items: 
Size: 343747 Color: 2
Size: 329528 Color: 1
Size: 324328 Color: 4

Bin 110: 2440 of cap free
Amount of items: 2
Items: 
Size: 718478 Color: 0
Size: 279083 Color: 3

Bin 111: 2447 of cap free
Amount of items: 2
Items: 
Size: 602451 Color: 1
Size: 395103 Color: 3

Bin 112: 2476 of cap free
Amount of items: 3
Items: 
Size: 604830 Color: 3
Size: 196778 Color: 0
Size: 195917 Color: 2

Bin 113: 2490 of cap free
Amount of items: 2
Items: 
Size: 575955 Color: 4
Size: 421556 Color: 0

Bin 114: 2504 of cap free
Amount of items: 3
Items: 
Size: 377336 Color: 3
Size: 312178 Color: 0
Size: 307983 Color: 4

Bin 115: 2506 of cap free
Amount of items: 3
Items: 
Size: 782029 Color: 1
Size: 112604 Color: 1
Size: 102862 Color: 3

Bin 116: 2591 of cap free
Amount of items: 2
Items: 
Size: 611760 Color: 4
Size: 385650 Color: 0

Bin 117: 2627 of cap free
Amount of items: 2
Items: 
Size: 698443 Color: 0
Size: 298931 Color: 3

Bin 118: 2897 of cap free
Amount of items: 2
Items: 
Size: 722059 Color: 3
Size: 275045 Color: 1

Bin 119: 2917 of cap free
Amount of items: 2
Items: 
Size: 636492 Color: 2
Size: 360592 Color: 1

Bin 120: 2978 of cap free
Amount of items: 2
Items: 
Size: 663673 Color: 3
Size: 333350 Color: 0

Bin 121: 2993 of cap free
Amount of items: 2
Items: 
Size: 561865 Color: 2
Size: 435143 Color: 0

Bin 122: 3022 of cap free
Amount of items: 2
Items: 
Size: 692860 Color: 2
Size: 304119 Color: 0

Bin 123: 3047 of cap free
Amount of items: 2
Items: 
Size: 750403 Color: 4
Size: 246551 Color: 2

Bin 124: 3079 of cap free
Amount of items: 2
Items: 
Size: 617421 Color: 2
Size: 379501 Color: 4

Bin 125: 3085 of cap free
Amount of items: 2
Items: 
Size: 681960 Color: 4
Size: 314956 Color: 2

Bin 126: 3329 of cap free
Amount of items: 2
Items: 
Size: 535945 Color: 1
Size: 460727 Color: 2

Bin 127: 3331 of cap free
Amount of items: 2
Items: 
Size: 703311 Color: 2
Size: 293359 Color: 3

Bin 128: 3377 of cap free
Amount of items: 2
Items: 
Size: 574956 Color: 3
Size: 421668 Color: 1

Bin 129: 3409 of cap free
Amount of items: 2
Items: 
Size: 579524 Color: 4
Size: 417068 Color: 1

Bin 130: 3454 of cap free
Amount of items: 3
Items: 
Size: 708765 Color: 4
Size: 150165 Color: 0
Size: 137617 Color: 2

Bin 131: 3482 of cap free
Amount of items: 2
Items: 
Size: 546189 Color: 2
Size: 450330 Color: 4

Bin 132: 3560 of cap free
Amount of items: 2
Items: 
Size: 499995 Color: 2
Size: 496446 Color: 3

Bin 133: 3676 of cap free
Amount of items: 2
Items: 
Size: 650513 Color: 2
Size: 345812 Color: 3

Bin 134: 3726 of cap free
Amount of items: 2
Items: 
Size: 775482 Color: 2
Size: 220793 Color: 1

Bin 135: 3743 of cap free
Amount of items: 2
Items: 
Size: 752016 Color: 3
Size: 244242 Color: 1

Bin 136: 3746 of cap free
Amount of items: 2
Items: 
Size: 601419 Color: 3
Size: 394836 Color: 0

Bin 137: 3920 of cap free
Amount of items: 2
Items: 
Size: 741285 Color: 4
Size: 254796 Color: 3

Bin 138: 3999 of cap free
Amount of items: 2
Items: 
Size: 707020 Color: 3
Size: 288982 Color: 2

Bin 139: 4009 of cap free
Amount of items: 2
Items: 
Size: 747538 Color: 1
Size: 248454 Color: 4

Bin 140: 4083 of cap free
Amount of items: 2
Items: 
Size: 568640 Color: 2
Size: 427278 Color: 0

Bin 141: 4170 of cap free
Amount of items: 2
Items: 
Size: 663623 Color: 3
Size: 332208 Color: 4

Bin 142: 4196 of cap free
Amount of items: 2
Items: 
Size: 503439 Color: 3
Size: 492366 Color: 1

Bin 143: 4240 of cap free
Amount of items: 2
Items: 
Size: 708751 Color: 1
Size: 287010 Color: 0

Bin 144: 4407 of cap free
Amount of items: 2
Items: 
Size: 508980 Color: 1
Size: 486614 Color: 3

Bin 145: 4438 of cap free
Amount of items: 2
Items: 
Size: 694312 Color: 0
Size: 301251 Color: 4

Bin 146: 4565 of cap free
Amount of items: 2
Items: 
Size: 558282 Color: 1
Size: 437154 Color: 3

Bin 147: 4631 of cap free
Amount of items: 2
Items: 
Size: 623782 Color: 2
Size: 371588 Color: 3

Bin 148: 5037 of cap free
Amount of items: 2
Items: 
Size: 500459 Color: 4
Size: 494505 Color: 0

Bin 149: 5046 of cap free
Amount of items: 2
Items: 
Size: 723187 Color: 4
Size: 271768 Color: 0

Bin 150: 5121 of cap free
Amount of items: 2
Items: 
Size: 534435 Color: 3
Size: 460445 Color: 2

Bin 151: 5135 of cap free
Amount of items: 3
Items: 
Size: 406498 Color: 3
Size: 322710 Color: 0
Size: 265658 Color: 1

Bin 152: 5183 of cap free
Amount of items: 3
Items: 
Size: 602831 Color: 2
Size: 196236 Color: 0
Size: 195751 Color: 4

Bin 153: 5329 of cap free
Amount of items: 2
Items: 
Size: 696451 Color: 2
Size: 298221 Color: 3

Bin 154: 5340 of cap free
Amount of items: 2
Items: 
Size: 735595 Color: 4
Size: 259066 Color: 3

Bin 155: 5478 of cap free
Amount of items: 2
Items: 
Size: 729358 Color: 3
Size: 265165 Color: 0

Bin 156: 5490 of cap free
Amount of items: 2
Items: 
Size: 594437 Color: 2
Size: 400074 Color: 0

Bin 157: 5799 of cap free
Amount of items: 2
Items: 
Size: 715434 Color: 0
Size: 278768 Color: 2

Bin 158: 5923 of cap free
Amount of items: 2
Items: 
Size: 765167 Color: 4
Size: 228911 Color: 1

Bin 159: 5988 of cap free
Amount of items: 2
Items: 
Size: 537424 Color: 0
Size: 456589 Color: 3

Bin 160: 5998 of cap free
Amount of items: 2
Items: 
Size: 554706 Color: 3
Size: 439297 Color: 0

Bin 161: 6127 of cap free
Amount of items: 2
Items: 
Size: 503526 Color: 3
Size: 490348 Color: 2

Bin 162: 6326 of cap free
Amount of items: 3
Items: 
Size: 686034 Color: 3
Size: 155954 Color: 3
Size: 151687 Color: 2

Bin 163: 6410 of cap free
Amount of items: 2
Items: 
Size: 517970 Color: 4
Size: 475621 Color: 0

Bin 164: 6630 of cap free
Amount of items: 2
Items: 
Size: 668068 Color: 1
Size: 325303 Color: 0

Bin 165: 6711 of cap free
Amount of items: 2
Items: 
Size: 596035 Color: 4
Size: 397255 Color: 1

Bin 166: 7004 of cap free
Amount of items: 3
Items: 
Size: 761284 Color: 0
Size: 124115 Color: 2
Size: 107598 Color: 1

Bin 167: 7619 of cap free
Amount of items: 2
Items: 
Size: 526117 Color: 1
Size: 466265 Color: 2

Bin 168: 7619 of cap free
Amount of items: 2
Items: 
Size: 637995 Color: 2
Size: 354387 Color: 3

Bin 169: 7798 of cap free
Amount of items: 2
Items: 
Size: 737806 Color: 2
Size: 254397 Color: 0

Bin 170: 7937 of cap free
Amount of items: 2
Items: 
Size: 518318 Color: 0
Size: 473746 Color: 1

Bin 171: 7965 of cap free
Amount of items: 2
Items: 
Size: 795317 Color: 0
Size: 196719 Color: 3

Bin 172: 8204 of cap free
Amount of items: 2
Items: 
Size: 723032 Color: 1
Size: 268765 Color: 2

Bin 173: 8218 of cap free
Amount of items: 2
Items: 
Size: 613613 Color: 1
Size: 378170 Color: 2

Bin 174: 8378 of cap free
Amount of items: 2
Items: 
Size: 566132 Color: 2
Size: 425491 Color: 3

Bin 175: 8553 of cap free
Amount of items: 3
Items: 
Size: 517531 Color: 3
Size: 258319 Color: 2
Size: 215598 Color: 1

Bin 176: 8751 of cap free
Amount of items: 2
Items: 
Size: 626390 Color: 2
Size: 364860 Color: 0

Bin 177: 9350 of cap free
Amount of items: 2
Items: 
Size: 692321 Color: 0
Size: 298330 Color: 4

Bin 178: 9410 of cap free
Amount of items: 2
Items: 
Size: 574817 Color: 3
Size: 415774 Color: 1

Bin 179: 10338 of cap free
Amount of items: 2
Items: 
Size: 676027 Color: 3
Size: 313636 Color: 1

Bin 180: 10426 of cap free
Amount of items: 2
Items: 
Size: 750162 Color: 1
Size: 239413 Color: 0

Bin 181: 10429 of cap free
Amount of items: 2
Items: 
Size: 717289 Color: 4
Size: 272283 Color: 3

Bin 182: 10463 of cap free
Amount of items: 2
Items: 
Size: 539994 Color: 3
Size: 449544 Color: 1

Bin 183: 10920 of cap free
Amount of items: 2
Items: 
Size: 625345 Color: 0
Size: 363736 Color: 1

Bin 184: 11055 of cap free
Amount of items: 2
Items: 
Size: 573333 Color: 3
Size: 415613 Color: 4

Bin 185: 11085 of cap free
Amount of items: 2
Items: 
Size: 774824 Color: 4
Size: 214092 Color: 3

Bin 186: 12814 of cap free
Amount of items: 2
Items: 
Size: 637014 Color: 0
Size: 350173 Color: 3

Bin 187: 13493 of cap free
Amount of items: 2
Items: 
Size: 692090 Color: 0
Size: 294418 Color: 1

Bin 188: 14338 of cap free
Amount of items: 3
Items: 
Size: 759301 Color: 3
Size: 122977 Color: 2
Size: 103385 Color: 1

Bin 189: 14465 of cap free
Amount of items: 2
Items: 
Size: 509877 Color: 0
Size: 475659 Color: 2

Bin 190: 14662 of cap free
Amount of items: 2
Items: 
Size: 589249 Color: 3
Size: 396090 Color: 1

Bin 191: 15267 of cap free
Amount of items: 3
Items: 
Size: 338654 Color: 0
Size: 323768 Color: 2
Size: 322312 Color: 3

Bin 192: 15666 of cap free
Amount of items: 2
Items: 
Size: 539571 Color: 3
Size: 444764 Color: 4

Bin 193: 16045 of cap free
Amount of items: 2
Items: 
Size: 712630 Color: 1
Size: 271326 Color: 3

Bin 194: 17098 of cap free
Amount of items: 3
Items: 
Size: 530620 Color: 1
Size: 259906 Color: 2
Size: 192377 Color: 2

Bin 195: 19100 of cap free
Amount of items: 2
Items: 
Size: 490833 Color: 0
Size: 490068 Color: 1

Bin 196: 20362 of cap free
Amount of items: 2
Items: 
Size: 504799 Color: 1
Size: 474840 Color: 2

Bin 197: 22899 of cap free
Amount of items: 2
Items: 
Size: 633775 Color: 4
Size: 343327 Color: 3

Bin 198: 23019 of cap free
Amount of items: 2
Items: 
Size: 511962 Color: 4
Size: 465020 Color: 1

Bin 199: 25846 of cap free
Amount of items: 3
Items: 
Size: 513247 Color: 0
Size: 235250 Color: 1
Size: 225658 Color: 4

Bin 200: 26503 of cap free
Amount of items: 2
Items: 
Size: 705372 Color: 1
Size: 268126 Color: 3

Bin 201: 26547 of cap free
Amount of items: 2
Items: 
Size: 566291 Color: 1
Size: 407163 Color: 2

Bin 202: 27209 of cap free
Amount of items: 2
Items: 
Size: 701965 Color: 2
Size: 270827 Color: 3

Bin 203: 27652 of cap free
Amount of items: 2
Items: 
Size: 706341 Color: 3
Size: 266008 Color: 1

Bin 204: 27750 of cap free
Amount of items: 2
Items: 
Size: 618591 Color: 0
Size: 353660 Color: 1

Bin 205: 28720 of cap free
Amount of items: 2
Items: 
Size: 708207 Color: 4
Size: 263074 Color: 0

Bin 206: 31625 of cap free
Amount of items: 2
Items: 
Size: 703950 Color: 0
Size: 264426 Color: 2

Bin 207: 38957 of cap free
Amount of items: 2
Items: 
Size: 699260 Color: 0
Size: 261784 Color: 2

Bin 208: 40904 of cap free
Amount of items: 2
Items: 
Size: 578731 Color: 3
Size: 380366 Color: 0

Bin 209: 45209 of cap free
Amount of items: 2
Items: 
Size: 755762 Color: 0
Size: 199030 Color: 2

Bin 210: 45790 of cap free
Amount of items: 2
Items: 
Size: 574179 Color: 4
Size: 380032 Color: 0

Bin 211: 59906 of cap free
Amount of items: 2
Items: 
Size: 679757 Color: 1
Size: 260338 Color: 2

Bin 212: 64590 of cap free
Amount of items: 2
Items: 
Size: 494799 Color: 3
Size: 440612 Color: 2

Bin 213: 77401 of cap free
Amount of items: 2
Items: 
Size: 544561 Color: 3
Size: 378039 Color: 0

Bin 214: 79336 of cap free
Amount of items: 2
Items: 
Size: 691158 Color: 0
Size: 229507 Color: 3

Bin 215: 84257 of cap free
Amount of items: 2
Items: 
Size: 479686 Color: 4
Size: 436058 Color: 1

Bin 216: 93273 of cap free
Amount of items: 2
Items: 
Size: 529429 Color: 3
Size: 377299 Color: 0

Bin 217: 104679 of cap free
Amount of items: 2
Items: 
Size: 482108 Color: 0
Size: 413214 Color: 3

Bin 218: 109915 of cap free
Amount of items: 2
Items: 
Size: 489348 Color: 1
Size: 400738 Color: 2

Bin 219: 123141 of cap free
Amount of items: 2
Items: 
Size: 482280 Color: 0
Size: 394580 Color: 2

Bin 220: 150294 of cap free
Amount of items: 2
Items: 
Size: 476166 Color: 4
Size: 373541 Color: 2

Bin 221: 172570 of cap free
Amount of items: 2
Items: 
Size: 435853 Color: 1
Size: 391578 Color: 3

Bin 222: 195323 of cap free
Amount of items: 2
Items: 
Size: 438941 Color: 4
Size: 365737 Color: 2

Bin 223: 237980 of cap free
Amount of items: 2
Items: 
Size: 404044 Color: 4
Size: 357977 Color: 0

Bin 224: 244599 of cap free
Amount of items: 2
Items: 
Size: 388729 Color: 3
Size: 366673 Color: 4

Bin 225: 246869 of cap free
Amount of items: 2
Items: 
Size: 389182 Color: 1
Size: 363950 Color: 2

Bin 226: 281277 of cap free
Amount of items: 2
Items: 
Size: 376868 Color: 3
Size: 341856 Color: 1

Total size: 222554922
Total free space: 3445304

