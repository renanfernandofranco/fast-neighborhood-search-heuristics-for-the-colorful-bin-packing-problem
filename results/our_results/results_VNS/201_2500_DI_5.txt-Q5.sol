Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1229 Color: 0
Size: 1023 Color: 0
Size: 164 Color: 4
Size: 40 Color: 3

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1236 Color: 0
Size: 1020 Color: 4
Size: 172 Color: 3
Size: 28 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 0
Size: 1018 Color: 4
Size: 74 Color: 3

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1701 Color: 2
Size: 631 Color: 3
Size: 76 Color: 3
Size: 48 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 2
Size: 684 Color: 4
Size: 56 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 0
Size: 580 Color: 2
Size: 24 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 0
Size: 436 Color: 4
Size: 126 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 3
Size: 476 Color: 1
Size: 88 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 2
Size: 443 Color: 4
Size: 88 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 4
Size: 460 Color: 0
Size: 56 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 2
Size: 386 Color: 0
Size: 92 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1991 Color: 4
Size: 449 Color: 2
Size: 16 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 1
Size: 398 Color: 4
Size: 64 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2002 Color: 3
Size: 342 Color: 3
Size: 112 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 2
Size: 228 Color: 3
Size: 200 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 4
Size: 280 Color: 3
Size: 126 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 1
Size: 300 Color: 3
Size: 56 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2111 Color: 2
Size: 289 Color: 3
Size: 56 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 2
Size: 204 Color: 3
Size: 106 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 1
Size: 204 Color: 1
Size: 104 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 1
Size: 253 Color: 1
Size: 50 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 1
Size: 282 Color: 2
Size: 8 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 0
Size: 262 Color: 1
Size: 8 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 2
Size: 204 Color: 4
Size: 64 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 4
Size: 148 Color: 2
Size: 106 Color: 3

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 4
Size: 861 Color: 3
Size: 152 Color: 4

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1909 Color: 4
Size: 458 Color: 0
Size: 88 Color: 3

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 4
Size: 828 Color: 3
Size: 200 Color: 2

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 4
Size: 751 Color: 0
Size: 120 Color: 4

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 4
Size: 654 Color: 4
Size: 104 Color: 0

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 1
Size: 554 Color: 0
Size: 90 Color: 1

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 2001 Color: 1
Size: 453 Color: 3

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 2106 Color: 1
Size: 348 Color: 2

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 1
Size: 260 Color: 2
Size: 2 Color: 4

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 0
Size: 729 Color: 2
Size: 80 Color: 3

Bin 36: 3 of cap free
Amount of items: 4
Items: 
Size: 1693 Color: 2
Size: 470 Color: 3
Size: 242 Color: 4
Size: 48 Color: 0

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1697 Color: 3
Size: 756 Color: 1

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 1
Size: 618 Color: 0
Size: 20 Color: 2

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 4
Size: 272 Color: 3
Size: 120 Color: 0

Bin 40: 4 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 0
Size: 774 Color: 1
Size: 404 Color: 1
Size: 44 Color: 3

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 1
Size: 539 Color: 0
Size: 331 Color: 4

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1910 Color: 3
Size: 542 Color: 2

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 2062 Color: 2
Size: 389 Color: 4

Bin 44: 6 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 0
Size: 866 Color: 3
Size: 48 Color: 0

Bin 45: 6 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 1
Size: 512 Color: 3
Size: 144 Color: 3

Bin 46: 7 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 4
Size: 637 Color: 0
Size: 48 Color: 4

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 2005 Color: 0
Size: 444 Color: 2

Bin 48: 8 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 0
Size: 568 Color: 2
Size: 457 Color: 4

Bin 49: 8 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 3
Size: 916 Color: 0
Size: 64 Color: 1

Bin 50: 8 of cap free
Amount of items: 2
Items: 
Size: 1530 Color: 3
Size: 918 Color: 1

Bin 51: 8 of cap free
Amount of items: 2
Items: 
Size: 1718 Color: 4
Size: 730 Color: 0

Bin 52: 8 of cap free
Amount of items: 2
Items: 
Size: 1924 Color: 3
Size: 524 Color: 4

Bin 53: 8 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 4
Size: 330 Color: 0

Bin 54: 9 of cap free
Amount of items: 2
Items: 
Size: 1827 Color: 4
Size: 620 Color: 2

Bin 55: 10 of cap free
Amount of items: 2
Items: 
Size: 2044 Color: 1
Size: 402 Color: 2

Bin 56: 11 of cap free
Amount of items: 3
Items: 
Size: 1557 Color: 4
Size: 840 Color: 1
Size: 48 Color: 0

Bin 57: 12 of cap free
Amount of items: 2
Items: 
Size: 1811 Color: 3
Size: 633 Color: 1

Bin 58: 13 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 1
Size: 1022 Color: 3

Bin 59: 13 of cap free
Amount of items: 2
Items: 
Size: 1556 Color: 3
Size: 887 Color: 2

Bin 60: 13 of cap free
Amount of items: 2
Items: 
Size: 1908 Color: 4
Size: 535 Color: 2

Bin 61: 17 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 2
Size: 384 Color: 4
Size: 381 Color: 3

Bin 62: 18 of cap free
Amount of items: 2
Items: 
Size: 1913 Color: 4
Size: 525 Color: 3

Bin 63: 21 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 4
Size: 1021 Color: 3
Size: 176 Color: 1

Bin 64: 22 of cap free
Amount of items: 18
Items: 
Size: 330 Color: 1
Size: 232 Color: 0
Size: 226 Color: 0
Size: 160 Color: 2
Size: 148 Color: 4
Size: 144 Color: 4
Size: 144 Color: 2
Size: 128 Color: 4
Size: 128 Color: 3
Size: 124 Color: 2
Size: 108 Color: 2
Size: 96 Color: 0
Size: 90 Color: 4
Size: 88 Color: 3
Size: 88 Color: 3
Size: 88 Color: 1
Size: 80 Color: 1
Size: 32 Color: 4

Bin 65: 23 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 1
Size: 976 Color: 0
Size: 226 Color: 1

Bin 66: 2152 of cap free
Amount of items: 4
Items: 
Size: 88 Color: 0
Size: 76 Color: 3
Size: 76 Color: 3
Size: 64 Color: 2

Total size: 159640
Total free space: 2456

