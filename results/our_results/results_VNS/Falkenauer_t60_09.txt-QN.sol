Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 58
Size: 273 Color: 15
Size: 259 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 43
Size: 316 Color: 29
Size: 308 Color: 28

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 51
Size: 298 Color: 23
Size: 276 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 52
Size: 319 Color: 30
Size: 252 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 57
Size: 289 Color: 21
Size: 258 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 59
Size: 263 Color: 12
Size: 254 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 42
Size: 263 Color: 13
Size: 364 Color: 39

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 45
Size: 359 Color: 37
Size: 261 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 41
Size: 369 Color: 40
Size: 262 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 56
Size: 288 Color: 20
Size: 261 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 46
Size: 359 Color: 36
Size: 259 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 47
Size: 351 Color: 35
Size: 258 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 49
Size: 332 Color: 32
Size: 256 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 53
Size: 304 Color: 26
Size: 254 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 48
Size: 337 Color: 33
Size: 266 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 50
Size: 307 Color: 27
Size: 276 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 55
Size: 280 Color: 19
Size: 275 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 54
Size: 304 Color: 24
Size: 253 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 38
Size: 343 Color: 34
Size: 294 Color: 22

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 44
Size: 319 Color: 31
Size: 304 Color: 25

Total size: 20000
Total free space: 0

