Capicity Bin: 2028
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 1
Size: 242 Color: 1
Size: 48 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 1
Size: 220 Color: 1
Size: 60 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 1
Size: 186 Color: 1
Size: 44 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1255 Color: 1
Size: 659 Color: 1
Size: 114 Color: 0

Bin 5: 0 of cap free
Amount of items: 5
Items: 
Size: 1357 Color: 1
Size: 316 Color: 1
Size: 245 Color: 1
Size: 66 Color: 0
Size: 44 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 1
Size: 362 Color: 1
Size: 52 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 1
Size: 423 Color: 1
Size: 84 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 1
Size: 409 Color: 1
Size: 160 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 1
Size: 250 Color: 1
Size: 128 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 1
Size: 382 Color: 1
Size: 236 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 1
Size: 582 Color: 1
Size: 72 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 1
Size: 369 Color: 1
Size: 120 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1361 Color: 1
Size: 573 Color: 1
Size: 94 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 1
Size: 282 Color: 1
Size: 76 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 1
Size: 446 Color: 1
Size: 52 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 1
Size: 281 Color: 1
Size: 82 Color: 0

Bin 17: 0 of cap free
Amount of items: 5
Items: 
Size: 1239 Color: 1
Size: 557 Color: 1
Size: 92 Color: 0
Size: 80 Color: 0
Size: 60 Color: 1

Bin 18: 0 of cap free
Amount of items: 5
Items: 
Size: 843 Color: 1
Size: 714 Color: 1
Size: 367 Color: 1
Size: 68 Color: 0
Size: 36 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1715 Color: 1
Size: 257 Color: 1
Size: 56 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1454 Color: 1
Size: 482 Color: 1
Size: 92 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1341 Color: 1
Size: 655 Color: 1
Size: 32 Color: 0

Bin 22: 0 of cap free
Amount of items: 4
Items: 
Size: 1377 Color: 1
Size: 475 Color: 1
Size: 128 Color: 0
Size: 48 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1276 Color: 1
Size: 642 Color: 1
Size: 110 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 1
Size: 303 Color: 1
Size: 98 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 1
Size: 168 Color: 0
Size: 58 Color: 0

Bin 26: 0 of cap free
Amount of items: 4
Items: 
Size: 1050 Color: 1
Size: 782 Color: 1
Size: 152 Color: 0
Size: 44 Color: 0

Bin 27: 0 of cap free
Amount of items: 4
Items: 
Size: 1750 Color: 1
Size: 194 Color: 1
Size: 48 Color: 0
Size: 36 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 1
Size: 234 Color: 1
Size: 64 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 1
Size: 261 Color: 1
Size: 16 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 1
Size: 329 Color: 1
Size: 110 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1153 Color: 1
Size: 731 Color: 1
Size: 144 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1018 Color: 1
Size: 906 Color: 1
Size: 104 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 1
Size: 318 Color: 1
Size: 116 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1258 Color: 1
Size: 546 Color: 1
Size: 224 Color: 0

Bin 35: 0 of cap free
Amount of items: 4
Items: 
Size: 1475 Color: 1
Size: 335 Color: 1
Size: 168 Color: 0
Size: 50 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 1
Size: 398 Color: 1
Size: 76 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1735 Color: 1
Size: 245 Color: 1
Size: 48 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1015 Color: 1
Size: 752 Color: 0
Size: 261 Color: 1

Bin 39: 0 of cap free
Amount of items: 5
Items: 
Size: 1017 Color: 1
Size: 491 Color: 1
Size: 472 Color: 1
Size: 44 Color: 0
Size: 4 Color: 0

Bin 40: 0 of cap free
Amount of items: 5
Items: 
Size: 1474 Color: 1
Size: 302 Color: 1
Size: 100 Color: 0
Size: 88 Color: 1
Size: 64 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 1
Size: 418 Color: 1
Size: 36 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 1
Size: 462 Color: 1
Size: 72 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 1
Size: 645 Color: 1
Size: 52 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 1
Size: 243 Color: 1
Size: 130 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 1
Size: 190 Color: 1
Size: 80 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 1
Size: 420 Color: 1
Size: 72 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 1
Size: 262 Color: 1
Size: 48 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 1400 Color: 1
Size: 561 Color: 1
Size: 66 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 1
Size: 331 Color: 1
Size: 108 Color: 0

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 1
Size: 326 Color: 1
Size: 56 Color: 0

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 1
Size: 227 Color: 1
Size: 60 Color: 0

Bin 52: 5 of cap free
Amount of items: 3
Items: 
Size: 1174 Color: 1
Size: 757 Color: 1
Size: 92 Color: 0

Bin 53: 7 of cap free
Amount of items: 3
Items: 
Size: 1631 Color: 1
Size: 346 Color: 1
Size: 44 Color: 0

Bin 54: 8 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 1
Size: 321 Color: 1
Size: 20 Color: 0

Bin 55: 13 of cap free
Amount of items: 5
Items: 
Size: 684 Color: 1
Size: 543 Color: 1
Size: 518 Color: 1
Size: 140 Color: 0
Size: 130 Color: 0

Bin 56: 15 of cap free
Amount of items: 3
Items: 
Size: 1151 Color: 1
Size: 818 Color: 1
Size: 44 Color: 0

Bin 57: 17 of cap free
Amount of items: 3
Items: 
Size: 1094 Color: 1
Size: 845 Color: 1
Size: 72 Color: 0

Bin 58: 19 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 1
Size: 295 Color: 1
Size: 20 Color: 0

Bin 59: 20 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 1
Size: 461 Color: 1
Size: 108 Color: 0

Bin 60: 146 of cap free
Amount of items: 3
Items: 
Size: 1243 Color: 1
Size: 411 Color: 1
Size: 228 Color: 0

Bin 61: 222 of cap free
Amount of items: 1
Items: 
Size: 1806 Color: 1

Bin 62: 264 of cap free
Amount of items: 1
Items: 
Size: 1764 Color: 1

Bin 63: 273 of cap free
Amount of items: 1
Items: 
Size: 1755 Color: 1

Bin 64: 314 of cap free
Amount of items: 1
Items: 
Size: 1714 Color: 1

Bin 65: 337 of cap free
Amount of items: 1
Items: 
Size: 1691 Color: 1

Bin 66: 353 of cap free
Amount of items: 1
Items: 
Size: 1675 Color: 1

Total size: 131820
Total free space: 2028

