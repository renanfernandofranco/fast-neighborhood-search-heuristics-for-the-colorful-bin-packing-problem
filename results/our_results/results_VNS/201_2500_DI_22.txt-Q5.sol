Capicity Bin: 2036
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1019 Color: 3
Size: 845 Color: 4
Size: 140 Color: 2
Size: 32 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1022 Color: 4
Size: 890 Color: 2
Size: 124 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 2
Size: 692 Color: 1
Size: 106 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1366 Color: 0
Size: 622 Color: 4
Size: 48 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 4
Size: 438 Color: 0
Size: 209 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 1
Size: 442 Color: 2
Size: 88 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 0
Size: 441 Color: 2
Size: 88 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1566 Color: 3
Size: 246 Color: 3
Size: 224 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 2
Size: 407 Color: 3
Size: 20 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 4
Size: 361 Color: 2
Size: 58 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 4
Size: 326 Color: 2
Size: 48 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 4
Size: 298 Color: 3
Size: 40 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1719 Color: 3
Size: 265 Color: 4
Size: 52 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 2
Size: 254 Color: 2
Size: 56 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 3
Size: 282 Color: 0
Size: 20 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 0
Size: 190 Color: 4
Size: 104 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 2
Size: 242 Color: 1
Size: 32 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 4
Size: 227 Color: 2
Size: 44 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 2
Size: 178 Color: 4
Size: 56 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 0
Size: 182 Color: 0
Size: 40 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 0
Size: 185 Color: 1
Size: 36 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 4
Size: 187 Color: 0
Size: 36 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 3
Size: 160 Color: 2
Size: 50 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 4
Size: 820 Color: 3
Size: 48 Color: 3

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1408 Color: 3
Size: 531 Color: 2
Size: 96 Color: 4

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 4
Size: 490 Color: 1
Size: 64 Color: 3

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1475 Color: 1
Size: 468 Color: 4
Size: 92 Color: 2

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 1
Size: 362 Color: 4
Size: 124 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 4
Size: 321 Color: 2
Size: 112 Color: 2

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1620 Color: 0
Size: 407 Color: 3
Size: 8 Color: 2

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1664 Color: 4
Size: 327 Color: 0
Size: 44 Color: 3

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 4
Size: 247 Color: 1
Size: 106 Color: 2

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 2
Size: 314 Color: 1
Size: 70 Color: 4

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 3
Size: 230 Color: 2
Size: 108 Color: 4

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 1
Size: 290 Color: 4
Size: 16 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 1785 Color: 1
Size: 126 Color: 2
Size: 124 Color: 4

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 3
Size: 168 Color: 0
Size: 80 Color: 4

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 1810 Color: 0
Size: 225 Color: 2

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 1393 Color: 4
Size: 537 Color: 0
Size: 104 Color: 1

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 2
Size: 569 Color: 4
Size: 64 Color: 3

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 1645 Color: 1
Size: 389 Color: 3

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 4
Size: 316 Color: 1
Size: 28 Color: 2

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 1741 Color: 4
Size: 293 Color: 1

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 2
Size: 743 Color: 0
Size: 224 Color: 4

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 1
Size: 463 Color: 0
Size: 56 Color: 1

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 4
Size: 394 Color: 0
Size: 68 Color: 1

Bin 47: 3 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 0
Size: 348 Color: 1

Bin 48: 3 of cap free
Amount of items: 2
Items: 
Size: 1750 Color: 3
Size: 283 Color: 0

Bin 49: 4 of cap free
Amount of items: 3
Items: 
Size: 1178 Color: 2
Size: 810 Color: 0
Size: 44 Color: 4

Bin 50: 5 of cap free
Amount of items: 3
Items: 
Size: 1289 Color: 1
Size: 666 Color: 2
Size: 76 Color: 3

Bin 51: 6 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 3
Size: 721 Color: 4
Size: 144 Color: 2

Bin 52: 6 of cap free
Amount of items: 3
Items: 
Size: 1276 Color: 4
Size: 718 Color: 1
Size: 36 Color: 2

Bin 53: 7 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 2
Size: 541 Color: 4
Size: 198 Color: 1

Bin 54: 7 of cap free
Amount of items: 2
Items: 
Size: 1605 Color: 2
Size: 424 Color: 1

Bin 55: 7 of cap free
Amount of items: 2
Items: 
Size: 1767 Color: 3
Size: 262 Color: 0

Bin 56: 7 of cap free
Amount of items: 2
Items: 
Size: 1818 Color: 1
Size: 211 Color: 0

Bin 57: 15 of cap free
Amount of items: 4
Items: 
Size: 1023 Color: 2
Size: 538 Color: 4
Size: 316 Color: 4
Size: 144 Color: 1

Bin 58: 15 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 4
Size: 367 Color: 3
Size: 8 Color: 3

Bin 59: 18 of cap free
Amount of items: 2
Items: 
Size: 1171 Color: 0
Size: 847 Color: 1

Bin 60: 18 of cap free
Amount of items: 2
Items: 
Size: 1291 Color: 2
Size: 727 Color: 4

Bin 61: 19 of cap free
Amount of items: 2
Items: 
Size: 1394 Color: 3
Size: 623 Color: 1

Bin 62: 20 of cap free
Amount of items: 16
Items: 
Size: 525 Color: 2
Size: 257 Color: 3
Size: 190 Color: 1
Size: 168 Color: 4
Size: 132 Color: 1
Size: 88 Color: 4
Size: 84 Color: 2
Size: 76 Color: 2
Size: 76 Color: 2
Size: 76 Color: 0
Size: 72 Color: 2
Size: 64 Color: 1
Size: 60 Color: 1
Size: 52 Color: 0
Size: 52 Color: 0
Size: 44 Color: 0

Bin 63: 24 of cap free
Amount of items: 2
Items: 
Size: 1355 Color: 3
Size: 657 Color: 0

Bin 64: 24 of cap free
Amount of items: 2
Items: 
Size: 1450 Color: 2
Size: 562 Color: 0

Bin 65: 26 of cap free
Amount of items: 2
Items: 
Size: 1021 Color: 4
Size: 989 Color: 1

Bin 66: 1768 of cap free
Amount of items: 6
Items: 
Size: 56 Color: 4
Size: 56 Color: 2
Size: 48 Color: 1
Size: 36 Color: 3
Size: 36 Color: 0
Size: 36 Color: 0

Total size: 132340
Total free space: 2036

