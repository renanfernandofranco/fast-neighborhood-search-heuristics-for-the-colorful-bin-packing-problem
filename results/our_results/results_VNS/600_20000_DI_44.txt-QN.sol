Capicity Bin: 15632
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 9298 Color: 429
Size: 5260 Color: 375
Size: 398 Color: 70
Size: 340 Color: 46
Size: 336 Color: 44

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10808 Color: 455
Size: 4584 Color: 360
Size: 240 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11738 Color: 477
Size: 2152 Color: 277
Size: 1742 Color: 242

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11878 Color: 481
Size: 2148 Color: 276
Size: 1606 Color: 228

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 491
Size: 2352 Color: 290
Size: 1064 Color: 183

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 492
Size: 2372 Color: 292
Size: 1016 Color: 175

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12285 Color: 496
Size: 2791 Color: 308
Size: 556 Color: 116

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 503
Size: 2600 Color: 301
Size: 472 Color: 101

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12778 Color: 512
Size: 2382 Color: 293
Size: 472 Color: 102

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12858 Color: 517
Size: 2662 Color: 304
Size: 112 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12879 Color: 518
Size: 2295 Color: 286
Size: 458 Color: 92

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13039 Color: 524
Size: 2281 Color: 284
Size: 312 Color: 35

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 527
Size: 1932 Color: 259
Size: 608 Color: 126

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13094 Color: 528
Size: 1890 Color: 255
Size: 648 Color: 133

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13102 Color: 529
Size: 2110 Color: 273
Size: 420 Color: 83

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13118 Color: 530
Size: 1982 Color: 263
Size: 532 Color: 113

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 533
Size: 1702 Color: 238
Size: 780 Color: 148

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 534
Size: 2348 Color: 289
Size: 124 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13292 Color: 541
Size: 1324 Color: 204
Size: 1016 Color: 176

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13294 Color: 542
Size: 2070 Color: 269
Size: 268 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13304 Color: 543
Size: 1944 Color: 260
Size: 384 Color: 67

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13360 Color: 546
Size: 1296 Color: 198
Size: 976 Color: 169

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13403 Color: 549
Size: 1781 Color: 245
Size: 448 Color: 89

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 553
Size: 1132 Color: 187
Size: 1048 Color: 180

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13494 Color: 555
Size: 1138 Color: 191
Size: 1000 Color: 171

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13495 Color: 556
Size: 1561 Color: 224
Size: 576 Color: 121

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13542 Color: 560
Size: 1630 Color: 231
Size: 460 Color: 93

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 563
Size: 1604 Color: 227
Size: 464 Color: 95

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13589 Color: 564
Size: 1661 Color: 232
Size: 382 Color: 65

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13622 Color: 569
Size: 1010 Color: 174
Size: 1000 Color: 170

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13627 Color: 570
Size: 1517 Color: 219
Size: 488 Color: 105

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13702 Color: 572
Size: 1538 Color: 222
Size: 392 Color: 69

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13706 Color: 573
Size: 1302 Color: 202
Size: 624 Color: 131

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 583
Size: 1520 Color: 220
Size: 280 Color: 20

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13876 Color: 586
Size: 1132 Color: 188
Size: 624 Color: 130

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 587
Size: 1350 Color: 208
Size: 404 Color: 74

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13894 Color: 588
Size: 1402 Color: 211
Size: 336 Color: 45

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 589
Size: 1448 Color: 214
Size: 288 Color: 22

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 591
Size: 1436 Color: 212
Size: 280 Color: 19

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13980 Color: 593
Size: 1300 Color: 201
Size: 352 Color: 54

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 594
Size: 1000 Color: 172
Size: 640 Color: 132

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14014 Color: 595
Size: 1088 Color: 184
Size: 530 Color: 112

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14034 Color: 597
Size: 894 Color: 161
Size: 704 Color: 137

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 599
Size: 1124 Color: 186
Size: 456 Color: 90

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 600
Size: 1146 Color: 192
Size: 426 Color: 85

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 10259 Color: 448
Size: 5100 Color: 371
Size: 272 Color: 17

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 11994 Color: 485
Size: 2605 Color: 302
Size: 1032 Color: 178

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 12181 Color: 489
Size: 3130 Color: 324
Size: 320 Color: 37

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 12274 Color: 494
Size: 2997 Color: 319
Size: 360 Color: 58

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 12703 Color: 509
Size: 1880 Color: 254
Size: 1048 Color: 181

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 12883 Color: 519
Size: 1956 Color: 261
Size: 792 Color: 149

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 12979 Color: 523
Size: 1852 Color: 251
Size: 800 Color: 152

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 13147 Color: 532
Size: 1348 Color: 207
Size: 1136 Color: 190

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 13190 Color: 535
Size: 2441 Color: 297

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 13211 Color: 537
Size: 1688 Color: 236
Size: 732 Color: 142

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 13243 Color: 539
Size: 2388 Color: 294

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 13271 Color: 540
Size: 2360 Color: 291

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 575
Size: 1911 Color: 257

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 13759 Color: 577
Size: 1872 Color: 253

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 10307 Color: 449
Size: 5051 Color: 369
Size: 272 Color: 16

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 11094 Color: 462
Size: 4328 Color: 354
Size: 208 Color: 6

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 11238 Color: 466
Size: 3564 Color: 334
Size: 828 Color: 154

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 483
Size: 3702 Color: 340

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 12074 Color: 487
Size: 2936 Color: 315
Size: 620 Color: 128

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 544
Size: 2314 Color: 287

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 13339 Color: 545
Size: 2291 Color: 285

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 558
Size: 2118 Color: 274

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 13619 Color: 568
Size: 2011 Color: 265

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 13639 Color: 571
Size: 1991 Color: 264

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 581
Size: 1822 Color: 249

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 14020 Color: 596
Size: 1610 Color: 230

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 598
Size: 1590 Color: 226

Bin 73: 3 of cap free
Amount of items: 3
Items: 
Size: 10818 Color: 456
Size: 3287 Color: 329
Size: 1524 Color: 221

Bin 74: 3 of cap free
Amount of items: 3
Items: 
Size: 10831 Color: 457
Size: 3662 Color: 339
Size: 1136 Color: 189

Bin 75: 3 of cap free
Amount of items: 2
Items: 
Size: 11190 Color: 465
Size: 4439 Color: 357

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 11615 Color: 474
Size: 4014 Color: 348

Bin 77: 3 of cap free
Amount of items: 2
Items: 
Size: 11911 Color: 482
Size: 3718 Color: 341

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 495
Size: 3349 Color: 330

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 12578 Color: 504
Size: 1671 Color: 233
Size: 1380 Color: 210

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 13950 Color: 592
Size: 1679 Color: 235

Bin 81: 4 of cap free
Amount of items: 27
Items: 
Size: 740 Color: 145
Size: 740 Color: 144
Size: 736 Color: 143
Size: 728 Color: 141
Size: 728 Color: 140
Size: 712 Color: 139
Size: 712 Color: 138
Size: 672 Color: 136
Size: 668 Color: 135
Size: 656 Color: 134
Size: 608 Color: 127
Size: 604 Color: 125
Size: 598 Color: 124
Size: 592 Color: 123
Size: 560 Color: 117
Size: 496 Color: 106
Size: 484 Color: 104
Size: 476 Color: 103
Size: 472 Color: 100
Size: 468 Color: 99
Size: 464 Color: 98
Size: 464 Color: 97
Size: 464 Color: 96
Size: 464 Color: 94
Size: 458 Color: 91
Size: 432 Color: 87
Size: 432 Color: 86

Bin 82: 4 of cap free
Amount of items: 5
Items: 
Size: 9308 Color: 430
Size: 5276 Color: 376
Size: 376 Color: 64
Size: 334 Color: 43
Size: 334 Color: 42

Bin 83: 4 of cap free
Amount of items: 3
Items: 
Size: 9516 Color: 435
Size: 5808 Color: 390
Size: 304 Color: 33

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 10658 Color: 454
Size: 4730 Color: 363
Size: 240 Color: 8

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 12772 Color: 511
Size: 2856 Color: 313

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 12808 Color: 514
Size: 2820 Color: 311

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 565
Size: 2038 Color: 268

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 13708 Color: 574
Size: 1920 Color: 258

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 13790 Color: 579
Size: 1838 Color: 250

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 585
Size: 1758 Color: 243

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 8795 Color: 418
Size: 6832 Color: 402

Bin 92: 5 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 461
Size: 3571 Color: 335
Size: 964 Color: 168

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 13446 Color: 552
Size: 2181 Color: 279

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 13608 Color: 567
Size: 2019 Color: 266

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 13845 Color: 584
Size: 1782 Color: 246

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 13903 Color: 590
Size: 1724 Color: 240

Bin 97: 6 of cap free
Amount of items: 21
Items: 
Size: 944 Color: 167
Size: 944 Color: 166
Size: 928 Color: 165
Size: 912 Color: 163
Size: 912 Color: 162
Size: 886 Color: 160
Size: 884 Color: 159
Size: 864 Color: 157
Size: 864 Color: 156
Size: 848 Color: 155
Size: 808 Color: 153
Size: 800 Color: 151
Size: 800 Color: 150
Size: 760 Color: 147
Size: 756 Color: 146
Size: 624 Color: 129
Size: 424 Color: 84
Size: 420 Color: 82
Size: 416 Color: 81
Size: 416 Color: 80
Size: 416 Color: 79

Bin 98: 6 of cap free
Amount of items: 5
Items: 
Size: 9244 Color: 428
Size: 5256 Color: 374
Size: 442 Color: 88
Size: 344 Color: 48
Size: 340 Color: 47

Bin 99: 6 of cap free
Amount of items: 3
Items: 
Size: 10056 Color: 444
Size: 5282 Color: 377
Size: 288 Color: 24

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 11844 Color: 479
Size: 3782 Color: 343

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 12418 Color: 498
Size: 3208 Color: 327

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 13596 Color: 566
Size: 2030 Color: 267

Bin 103: 7 of cap free
Amount of items: 2
Items: 
Size: 12037 Color: 486
Size: 3588 Color: 336

Bin 104: 8 of cap free
Amount of items: 2
Items: 
Size: 12658 Color: 508
Size: 2966 Color: 317

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 13198 Color: 536
Size: 2426 Color: 296

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 557
Size: 2124 Color: 275

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 580
Size: 1820 Color: 248

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 13813 Color: 582
Size: 1811 Color: 247

Bin 109: 9 of cap free
Amount of items: 2
Items: 
Size: 12821 Color: 516
Size: 2802 Color: 310

Bin 110: 9 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 578
Size: 1859 Color: 252

Bin 111: 10 of cap free
Amount of items: 3
Items: 
Size: 8834 Color: 422
Size: 5020 Color: 367
Size: 1768 Color: 244

Bin 112: 10 of cap free
Amount of items: 3
Items: 
Size: 9631 Color: 441
Size: 5699 Color: 386
Size: 292 Color: 27

Bin 113: 10 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 460
Size: 3642 Color: 337
Size: 916 Color: 164

Bin 114: 10 of cap free
Amount of items: 2
Items: 
Size: 11598 Color: 473
Size: 4024 Color: 349

Bin 115: 10 of cap free
Amount of items: 2
Items: 
Size: 13411 Color: 550
Size: 2211 Color: 281

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 13430 Color: 551
Size: 2192 Color: 280

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 13726 Color: 576
Size: 1896 Color: 256

Bin 118: 11 of cap free
Amount of items: 3
Items: 
Size: 9571 Color: 437
Size: 5746 Color: 388
Size: 304 Color: 31

Bin 119: 11 of cap free
Amount of items: 2
Items: 
Size: 12520 Color: 502
Size: 3101 Color: 323

Bin 120: 11 of cap free
Amount of items: 2
Items: 
Size: 13219 Color: 538
Size: 2402 Color: 295

Bin 121: 12 of cap free
Amount of items: 7
Items: 
Size: 7826 Color: 409
Size: 1700 Color: 237
Size: 1678 Color: 234
Size: 1608 Color: 229
Size: 1560 Color: 223
Size: 864 Color: 158
Size: 384 Color: 66

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 11852 Color: 480
Size: 3768 Color: 342

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 12938 Color: 521
Size: 2682 Color: 306

Bin 124: 12 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 522
Size: 2668 Color: 305

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 13459 Color: 554
Size: 2161 Color: 278

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 562
Size: 2072 Color: 271

Bin 127: 13 of cap free
Amount of items: 3
Items: 
Size: 10319 Color: 450
Size: 5036 Color: 368
Size: 264 Color: 14

Bin 128: 14 of cap free
Amount of items: 9
Items: 
Size: 7818 Color: 406
Size: 1300 Color: 199
Size: 1296 Color: 197
Size: 1296 Color: 196
Size: 1296 Color: 195
Size: 1232 Color: 194
Size: 574 Color: 120
Size: 404 Color: 75
Size: 402 Color: 73

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 11973 Color: 484
Size: 3645 Color: 338

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 12201 Color: 490
Size: 3417 Color: 332

Bin 131: 15 of cap free
Amount of items: 2
Items: 
Size: 12612 Color: 506
Size: 3005 Color: 320

Bin 132: 15 of cap free
Amount of items: 2
Items: 
Size: 12895 Color: 520
Size: 2722 Color: 307

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 13526 Color: 559
Size: 2091 Color: 272

Bin 134: 16 of cap free
Amount of items: 3
Items: 
Size: 11060 Color: 459
Size: 2992 Color: 318
Size: 1564 Color: 225

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 12370 Color: 497
Size: 3246 Color: 328

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 513
Size: 2828 Color: 312

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 13368 Color: 548
Size: 2248 Color: 283

Bin 138: 17 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 561
Size: 2071 Color: 270

Bin 139: 18 of cap free
Amount of items: 3
Items: 
Size: 10258 Color: 447
Size: 5084 Color: 370
Size: 272 Color: 18

Bin 140: 18 of cap free
Amount of items: 2
Items: 
Size: 12252 Color: 493
Size: 3362 Color: 331

Bin 141: 18 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 505
Size: 3034 Color: 321

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 12820 Color: 515
Size: 2794 Color: 309

Bin 143: 19 of cap free
Amount of items: 2
Items: 
Size: 12085 Color: 488
Size: 3528 Color: 333

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 13064 Color: 526
Size: 2548 Color: 300

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 13125 Color: 531
Size: 2487 Color: 298

Bin 146: 20 of cap free
Amount of items: 2
Items: 
Size: 13366 Color: 547
Size: 2246 Color: 282

Bin 147: 22 of cap free
Amount of items: 2
Items: 
Size: 9096 Color: 427
Size: 6514 Color: 399

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 11262 Color: 468
Size: 4348 Color: 355

Bin 149: 22 of cap free
Amount of items: 3
Items: 
Size: 11400 Color: 472
Size: 4146 Color: 351
Size: 64 Color: 1

Bin 150: 24 of cap free
Amount of items: 4
Items: 
Size: 10508 Color: 453
Size: 4602 Color: 361
Size: 256 Color: 10
Size: 242 Color: 9

Bin 151: 24 of cap free
Amount of items: 2
Items: 
Size: 11332 Color: 469
Size: 4276 Color: 352

Bin 152: 25 of cap free
Amount of items: 2
Items: 
Size: 12443 Color: 500
Size: 3164 Color: 326

Bin 153: 26 of cap free
Amount of items: 3
Items: 
Size: 8747 Color: 417
Size: 6511 Color: 397
Size: 348 Color: 51

Bin 154: 26 of cap free
Amount of items: 3
Items: 
Size: 9532 Color: 436
Size: 5770 Color: 389
Size: 304 Color: 32

Bin 155: 26 of cap free
Amount of items: 2
Items: 
Size: 12649 Color: 507
Size: 2957 Color: 316

Bin 156: 26 of cap free
Amount of items: 2
Items: 
Size: 13060 Color: 525
Size: 2546 Color: 299

Bin 157: 28 of cap free
Amount of items: 3
Items: 
Size: 9619 Color: 440
Size: 5689 Color: 385
Size: 296 Color: 28

Bin 158: 29 of cap free
Amount of items: 2
Items: 
Size: 11174 Color: 464
Size: 4429 Color: 356

Bin 159: 29 of cap free
Amount of items: 2
Items: 
Size: 12507 Color: 501
Size: 3096 Color: 322

Bin 160: 30 of cap free
Amount of items: 4
Items: 
Size: 9336 Color: 432
Size: 5626 Color: 381
Size: 320 Color: 39
Size: 320 Color: 38

Bin 161: 30 of cap free
Amount of items: 3
Items: 
Size: 9612 Color: 439
Size: 5688 Color: 384
Size: 302 Color: 29

Bin 162: 31 of cap free
Amount of items: 2
Items: 
Size: 11689 Color: 476
Size: 3912 Color: 346

Bin 163: 32 of cap free
Amount of items: 4
Items: 
Size: 9324 Color: 431
Size: 5612 Color: 380
Size: 332 Color: 41
Size: 332 Color: 40

Bin 164: 33 of cap free
Amount of items: 2
Items: 
Size: 12722 Color: 510
Size: 2877 Color: 314

Bin 165: 34 of cap free
Amount of items: 3
Items: 
Size: 8738 Color: 416
Size: 6508 Color: 396
Size: 352 Color: 52

Bin 166: 34 of cap free
Amount of items: 2
Items: 
Size: 12442 Color: 499
Size: 3156 Color: 325

Bin 167: 36 of cap free
Amount of items: 3
Items: 
Size: 10136 Color: 446
Size: 5180 Color: 373
Size: 280 Color: 21

Bin 168: 36 of cap free
Amount of items: 3
Items: 
Size: 10440 Color: 452
Size: 4900 Color: 364
Size: 256 Color: 11

Bin 169: 36 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 478
Size: 3812 Color: 344

Bin 170: 41 of cap free
Amount of items: 2
Items: 
Size: 11112 Color: 463
Size: 4479 Color: 358

Bin 171: 41 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 475
Size: 3911 Color: 345
Size: 40 Color: 0

Bin 172: 43 of cap free
Amount of items: 2
Items: 
Size: 10941 Color: 458
Size: 4648 Color: 362

Bin 173: 45 of cap free
Amount of items: 2
Items: 
Size: 11259 Color: 467
Size: 4328 Color: 353

Bin 174: 50 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 438
Size: 5682 Color: 383
Size: 304 Color: 30

Bin 175: 62 of cap free
Amount of items: 3
Items: 
Size: 9958 Color: 443
Size: 5324 Color: 378
Size: 288 Color: 25

Bin 176: 63 of cap free
Amount of items: 3
Items: 
Size: 8818 Color: 421
Size: 5011 Color: 366
Size: 1740 Color: 241

Bin 177: 72 of cap free
Amount of items: 2
Items: 
Size: 7832 Color: 411
Size: 7728 Color: 404

Bin 178: 82 of cap free
Amount of items: 2
Items: 
Size: 8898 Color: 424
Size: 6652 Color: 401

Bin 179: 95 of cap free
Amount of items: 6
Items: 
Size: 7828 Color: 410
Size: 2659 Color: 303
Size: 2343 Color: 288
Size: 1969 Color: 262
Size: 370 Color: 63
Size: 368 Color: 62

Bin 180: 96 of cap free
Amount of items: 3
Items: 
Size: 11364 Color: 471
Size: 4076 Color: 350
Size: 96 Color: 2

Bin 181: 102 of cap free
Amount of items: 2
Items: 
Size: 8882 Color: 423
Size: 6648 Color: 400

Bin 182: 102 of cap free
Amount of items: 3
Items: 
Size: 11349 Color: 470
Size: 4001 Color: 347
Size: 180 Color: 5

Bin 183: 104 of cap free
Amount of items: 3
Items: 
Size: 10114 Color: 445
Size: 5126 Color: 372
Size: 288 Color: 23

Bin 184: 107 of cap free
Amount of items: 7
Items: 
Size: 7821 Color: 408
Size: 1491 Color: 218
Size: 1470 Color: 217
Size: 1468 Color: 216
Size: 1450 Color: 215
Size: 1441 Color: 213
Size: 384 Color: 68

Bin 185: 107 of cap free
Amount of items: 3
Items: 
Size: 9482 Color: 434
Size: 5739 Color: 387
Size: 304 Color: 34

Bin 186: 110 of cap free
Amount of items: 4
Items: 
Size: 10420 Color: 451
Size: 4582 Color: 359
Size: 264 Color: 13
Size: 256 Color: 12

Bin 187: 120 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 420
Size: 5001 Color: 365
Size: 1703 Color: 239

Bin 188: 132 of cap free
Amount of items: 3
Items: 
Size: 8914 Color: 426
Size: 6242 Color: 392
Size: 344 Color: 49

Bin 189: 140 of cap free
Amount of items: 3
Items: 
Size: 9756 Color: 442
Size: 5448 Color: 379
Size: 288 Color: 26

Bin 190: 187 of cap free
Amount of items: 10
Items: 
Size: 7817 Color: 405
Size: 1148 Color: 193
Size: 1120 Color: 185
Size: 1052 Color: 182
Size: 1040 Color: 179
Size: 1024 Color: 177
Size: 1002 Color: 173
Size: 416 Color: 78
Size: 414 Color: 77
Size: 412 Color: 76

Bin 191: 188 of cap free
Amount of items: 3
Items: 
Size: 8900 Color: 425
Size: 6196 Color: 391
Size: 348 Color: 50

Bin 192: 198 of cap free
Amount of items: 3
Items: 
Size: 7834 Color: 412
Size: 7236 Color: 403
Size: 364 Color: 61

Bin 193: 214 of cap free
Amount of items: 4
Items: 
Size: 8204 Color: 415
Size: 6506 Color: 395
Size: 356 Color: 55
Size: 352 Color: 53

Bin 194: 230 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 433
Size: 5666 Color: 382
Size: 316 Color: 36

Bin 195: 244 of cap free
Amount of items: 4
Items: 
Size: 8172 Color: 414
Size: 6504 Color: 394
Size: 356 Color: 57
Size: 356 Color: 56

Bin 196: 312 of cap free
Amount of items: 2
Items: 
Size: 8807 Color: 419
Size: 6513 Color: 398

Bin 197: 332 of cap free
Amount of items: 4
Items: 
Size: 8072 Color: 413
Size: 6502 Color: 393
Size: 364 Color: 60
Size: 362 Color: 59

Bin 198: 356 of cap free
Amount of items: 8
Items: 
Size: 7820 Color: 407
Size: 1368 Color: 209
Size: 1336 Color: 206
Size: 1334 Color: 205
Size: 1316 Color: 203
Size: 1300 Color: 200
Size: 402 Color: 72
Size: 400 Color: 71

Bin 199: 10254 of cap free
Amount of items: 10
Items: 
Size: 590 Color: 122
Size: 560 Color: 119
Size: 560 Color: 118
Size: 556 Color: 115
Size: 540 Color: 114
Size: 528 Color: 111
Size: 520 Color: 110
Size: 512 Color: 109
Size: 508 Color: 108
Size: 504 Color: 107

Total size: 3095136
Total free space: 15632

