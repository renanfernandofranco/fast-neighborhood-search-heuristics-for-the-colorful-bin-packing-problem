Capicity Bin: 16320
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 8178 Color: 1
Size: 1758 Color: 1
Size: 1562 Color: 1
Size: 1549 Color: 0
Size: 1491 Color: 0
Size: 1378 Color: 0
Size: 404 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 1
Size: 3568 Color: 0
Size: 324 Color: 1

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 12210 Color: 0
Size: 3704 Color: 1
Size: 374 Color: 1
Size: 32 Color: 0

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 8212 Color: 1
Size: 5832 Color: 0
Size: 1000 Color: 0
Size: 732 Color: 1
Size: 544 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8385 Color: 1
Size: 6613 Color: 1
Size: 1322 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10998 Color: 1
Size: 5026 Color: 0
Size: 296 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 1
Size: 4932 Color: 0
Size: 308 Color: 1

Bin 8: 0 of cap free
Amount of items: 15
Items: 
Size: 8164 Color: 1
Size: 612 Color: 1
Size: 608 Color: 1
Size: 608 Color: 0
Size: 604 Color: 0
Size: 602 Color: 1
Size: 588 Color: 0
Size: 584 Color: 0
Size: 576 Color: 1
Size: 576 Color: 0
Size: 576 Color: 0
Size: 560 Color: 1
Size: 560 Color: 0
Size: 558 Color: 1
Size: 544 Color: 0

Bin 9: 0 of cap free
Amount of items: 8
Items: 
Size: 8173 Color: 1
Size: 1684 Color: 0
Size: 1521 Color: 1
Size: 1344 Color: 0
Size: 1342 Color: 1
Size: 1152 Color: 1
Size: 832 Color: 0
Size: 272 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11182 Color: 1
Size: 4774 Color: 1
Size: 364 Color: 0

Bin 11: 0 of cap free
Amount of items: 13
Items: 
Size: 8168 Color: 0
Size: 752 Color: 1
Size: 736 Color: 1
Size: 736 Color: 1
Size: 684 Color: 1
Size: 674 Color: 1
Size: 668 Color: 0
Size: 656 Color: 0
Size: 656 Color: 0
Size: 656 Color: 0
Size: 648 Color: 0
Size: 646 Color: 1
Size: 640 Color: 0

Bin 12: 0 of cap free
Amount of items: 8
Items: 
Size: 8165 Color: 0
Size: 2125 Color: 1
Size: 1798 Color: 0
Size: 992 Color: 1
Size: 992 Color: 1
Size: 984 Color: 0
Size: 952 Color: 0
Size: 312 Color: 1

Bin 13: 0 of cap free
Amount of items: 9
Items: 
Size: 8169 Color: 1
Size: 1657 Color: 0
Size: 1172 Color: 0
Size: 1152 Color: 0
Size: 1152 Color: 0
Size: 1150 Color: 1
Size: 1004 Color: 1
Size: 520 Color: 1
Size: 344 Color: 0

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 11586 Color: 0
Size: 1792 Color: 0
Size: 1584 Color: 1
Size: 1358 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14424 Color: 0
Size: 1610 Color: 0
Size: 286 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 1
Size: 1344 Color: 0
Size: 360 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11152 Color: 0
Size: 3506 Color: 1
Size: 1662 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 1
Size: 2302 Color: 0
Size: 32 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 1
Size: 3602 Color: 0
Size: 716 Color: 1

Bin 20: 0 of cap free
Amount of items: 7
Items: 
Size: 8176 Color: 1
Size: 1380 Color: 1
Size: 1358 Color: 0
Size: 1358 Color: 0
Size: 1352 Color: 0
Size: 1352 Color: 0
Size: 1344 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11828 Color: 1
Size: 4168 Color: 0
Size: 324 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14520 Color: 0
Size: 1160 Color: 0
Size: 640 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14448 Color: 1
Size: 1592 Color: 0
Size: 280 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 10290 Color: 0
Size: 5904 Color: 1
Size: 126 Color: 1

Bin 25: 0 of cap free
Amount of items: 7
Items: 
Size: 8161 Color: 0
Size: 2351 Color: 1
Size: 1462 Color: 1
Size: 1437 Color: 0
Size: 1367 Color: 0
Size: 1158 Color: 1
Size: 384 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 0
Size: 1432 Color: 1
Size: 856 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 0
Size: 3748 Color: 1
Size: 132 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14230 Color: 1
Size: 2002 Color: 0
Size: 88 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 0
Size: 2568 Color: 1
Size: 976 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13178 Color: 1
Size: 3022 Color: 0
Size: 120 Color: 0

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 11788 Color: 0
Size: 3240 Color: 0
Size: 1164 Color: 1
Size: 128 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14372 Color: 1
Size: 998 Color: 0
Size: 950 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 0
Size: 2204 Color: 1
Size: 316 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 1
Size: 6808 Color: 0
Size: 176 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 0
Size: 5836 Color: 1
Size: 176 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13328 Color: 1
Size: 2690 Color: 0
Size: 302 Color: 0

Bin 37: 0 of cap free
Amount of items: 4
Items: 
Size: 9286 Color: 0
Size: 5032 Color: 1
Size: 1730 Color: 1
Size: 272 Color: 0

Bin 38: 0 of cap free
Amount of items: 37
Items: 
Size: 538 Color: 0
Size: 536 Color: 0
Size: 522 Color: 0
Size: 520 Color: 0
Size: 512 Color: 1
Size: 498 Color: 1
Size: 496 Color: 1
Size: 496 Color: 1
Size: 492 Color: 1
Size: 480 Color: 1
Size: 480 Color: 0
Size: 472 Color: 1
Size: 472 Color: 1
Size: 452 Color: 1
Size: 448 Color: 1
Size: 448 Color: 1
Size: 444 Color: 1
Size: 444 Color: 1
Size: 436 Color: 0
Size: 432 Color: 0
Size: 432 Color: 0
Size: 424 Color: 0
Size: 416 Color: 1
Size: 416 Color: 0
Size: 416 Color: 0
Size: 416 Color: 0
Size: 402 Color: 1
Size: 400 Color: 1
Size: 400 Color: 1
Size: 400 Color: 0
Size: 400 Color: 0
Size: 388 Color: 0
Size: 376 Color: 1
Size: 368 Color: 0
Size: 368 Color: 0
Size: 348 Color: 0
Size: 332 Color: 1

Bin 39: 0 of cap free
Amount of items: 12
Items: 
Size: 8162 Color: 0
Size: 884 Color: 1
Size: 864 Color: 1
Size: 848 Color: 1
Size: 790 Color: 0
Size: 788 Color: 1
Size: 764 Color: 1
Size: 752 Color: 0
Size: 744 Color: 0
Size: 736 Color: 0
Size: 700 Color: 0
Size: 288 Color: 1

Bin 40: 0 of cap free
Amount of items: 5
Items: 
Size: 8170 Color: 0
Size: 4908 Color: 1
Size: 1572 Color: 0
Size: 1152 Color: 1
Size: 518 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 9264 Color: 0
Size: 6726 Color: 0
Size: 330 Color: 1

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 10240 Color: 1
Size: 6080 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 10649 Color: 1
Size: 5331 Color: 0
Size: 340 Color: 0

Bin 44: 0 of cap free
Amount of items: 5
Items: 
Size: 11730 Color: 0
Size: 2434 Color: 0
Size: 1604 Color: 1
Size: 328 Color: 1
Size: 224 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12290 Color: 1
Size: 3682 Color: 0
Size: 348 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 0
Size: 2190 Color: 1
Size: 1356 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12694 Color: 1
Size: 3562 Color: 1
Size: 64 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 1
Size: 2456 Color: 0
Size: 848 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13220 Color: 0
Size: 2884 Color: 1
Size: 216 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 1
Size: 1512 Color: 0
Size: 1356 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 0
Size: 2164 Color: 1
Size: 484 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 0
Size: 1790 Color: 0
Size: 480 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14182 Color: 1
Size: 1146 Color: 1
Size: 992 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14136 Color: 0
Size: 1352 Color: 0
Size: 832 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14296 Color: 0
Size: 1668 Color: 0
Size: 356 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14320 Color: 1
Size: 1412 Color: 0
Size: 588 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 0
Size: 1140 Color: 1
Size: 856 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14436 Color: 0
Size: 1452 Color: 1
Size: 432 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14628 Color: 1
Size: 984 Color: 0
Size: 708 Color: 0

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 1
Size: 4967 Color: 0
Size: 256 Color: 1

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13897 Color: 0
Size: 1680 Color: 0
Size: 742 Color: 1

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 14495 Color: 0
Size: 1824 Color: 1

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 14681 Color: 0
Size: 1638 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 8184 Color: 0
Size: 6587 Color: 1
Size: 1548 Color: 1

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11320 Color: 1
Size: 4727 Color: 0
Size: 272 Color: 0

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12443 Color: 0
Size: 3780 Color: 1
Size: 96 Color: 1

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 10296 Color: 0
Size: 5711 Color: 0
Size: 312 Color: 1

Bin 68: 1 of cap free
Amount of items: 23
Items: 
Size: 6794 Color: 1
Size: 1517 Color: 0
Size: 1000 Color: 1
Size: 748 Color: 1
Size: 432 Color: 1
Size: 356 Color: 1
Size: 354 Color: 1
Size: 352 Color: 1
Size: 352 Color: 1
Size: 352 Color: 0
Size: 342 Color: 1
Size: 336 Color: 1
Size: 336 Color: 0
Size: 320 Color: 0
Size: 320 Color: 0
Size: 320 Color: 0
Size: 312 Color: 0
Size: 304 Color: 1
Size: 304 Color: 0
Size: 304 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 14214 Color: 1
Size: 1832 Color: 0
Size: 272 Color: 1

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 8417 Color: 0
Size: 7149 Color: 0
Size: 752 Color: 1

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 11880 Color: 1
Size: 4438 Color: 0

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 12372 Color: 0
Size: 3946 Color: 1

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 13172 Color: 0
Size: 2004 Color: 1
Size: 1142 Color: 0

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13912 Color: 1
Size: 2406 Color: 0

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 9790 Color: 1
Size: 6064 Color: 0
Size: 464 Color: 0

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 12965 Color: 0
Size: 3352 Color: 1

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 10276 Color: 1
Size: 5753 Color: 1
Size: 288 Color: 0

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 13481 Color: 0
Size: 1828 Color: 1
Size: 1008 Color: 0

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 0
Size: 6793 Color: 0
Size: 1344 Color: 1

Bin 80: 4 of cap free
Amount of items: 3
Items: 
Size: 12114 Color: 1
Size: 3746 Color: 0
Size: 456 Color: 0

Bin 81: 4 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 0
Size: 2622 Color: 1

Bin 82: 4 of cap free
Amount of items: 2
Items: 
Size: 14272 Color: 0
Size: 2044 Color: 1

Bin 83: 5 of cap free
Amount of items: 4
Items: 
Size: 8196 Color: 1
Size: 4964 Color: 1
Size: 1583 Color: 0
Size: 1572 Color: 0

Bin 84: 5 of cap free
Amount of items: 2
Items: 
Size: 13646 Color: 1
Size: 2669 Color: 0

Bin 85: 6 of cap free
Amount of items: 2
Items: 
Size: 13094 Color: 1
Size: 3220 Color: 0

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 14036 Color: 0
Size: 2278 Color: 1

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 14533 Color: 1
Size: 1781 Color: 0

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 12888 Color: 0
Size: 3424 Color: 1

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 0
Size: 2760 Color: 1

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 13916 Color: 1
Size: 2396 Color: 0

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 14365 Color: 0
Size: 1947 Color: 1

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 14570 Color: 1
Size: 1742 Color: 0

Bin 93: 8 of cap free
Amount of items: 3
Items: 
Size: 11752 Color: 0
Size: 3244 Color: 0
Size: 1316 Color: 1

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 13724 Color: 1
Size: 2588 Color: 0

Bin 95: 8 of cap free
Amount of items: 2
Items: 
Size: 11264 Color: 1
Size: 5048 Color: 0

Bin 96: 9 of cap free
Amount of items: 3
Items: 
Size: 8250 Color: 1
Size: 6717 Color: 0
Size: 1344 Color: 1

Bin 97: 10 of cap free
Amount of items: 2
Items: 
Size: 14622 Color: 0
Size: 1688 Color: 1

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 14300 Color: 1
Size: 2008 Color: 0

Bin 99: 12 of cap free
Amount of items: 2
Items: 
Size: 14597 Color: 0
Size: 1711 Color: 1

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 10594 Color: 1
Size: 5713 Color: 0

Bin 101: 14 of cap free
Amount of items: 2
Items: 
Size: 13434 Color: 1
Size: 2872 Color: 0

Bin 102: 15 of cap free
Amount of items: 2
Items: 
Size: 13985 Color: 1
Size: 2320 Color: 0

Bin 103: 15 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 0
Size: 4753 Color: 1
Size: 1392 Color: 1

Bin 104: 15 of cap free
Amount of items: 2
Items: 
Size: 14501 Color: 1
Size: 1804 Color: 0

Bin 105: 16 of cap free
Amount of items: 2
Items: 
Size: 13837 Color: 1
Size: 2467 Color: 0

Bin 106: 16 of cap free
Amount of items: 2
Items: 
Size: 14134 Color: 1
Size: 2170 Color: 0

Bin 107: 16 of cap free
Amount of items: 2
Items: 
Size: 14532 Color: 0
Size: 1772 Color: 1

Bin 108: 17 of cap free
Amount of items: 2
Items: 
Size: 14073 Color: 1
Size: 2230 Color: 0

Bin 109: 17 of cap free
Amount of items: 2
Items: 
Size: 14672 Color: 0
Size: 1631 Color: 1

Bin 110: 18 of cap free
Amount of items: 2
Items: 
Size: 12050 Color: 0
Size: 4252 Color: 1

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 1
Size: 2800 Color: 0

Bin 112: 21 of cap free
Amount of items: 2
Items: 
Size: 14132 Color: 1
Size: 2167 Color: 0

Bin 113: 22 of cap free
Amount of items: 2
Items: 
Size: 10436 Color: 0
Size: 5862 Color: 1

Bin 114: 22 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 0
Size: 2490 Color: 1

Bin 115: 22 of cap free
Amount of items: 2
Items: 
Size: 14404 Color: 1
Size: 1894 Color: 0

Bin 116: 23 of cap free
Amount of items: 2
Items: 
Size: 13384 Color: 0
Size: 2913 Color: 1

Bin 117: 23 of cap free
Amount of items: 2
Items: 
Size: 13201 Color: 1
Size: 3096 Color: 0

Bin 118: 25 of cap free
Amount of items: 2
Items: 
Size: 12336 Color: 0
Size: 3959 Color: 1

Bin 119: 25 of cap free
Amount of items: 3
Items: 
Size: 9433 Color: 0
Size: 6582 Color: 0
Size: 280 Color: 1

Bin 120: 26 of cap free
Amount of items: 3
Items: 
Size: 9465 Color: 1
Size: 5741 Color: 0
Size: 1088 Color: 1

Bin 121: 26 of cap free
Amount of items: 2
Items: 
Size: 14358 Color: 0
Size: 1936 Color: 1

Bin 122: 27 of cap free
Amount of items: 2
Items: 
Size: 14580 Color: 1
Size: 1713 Color: 0

Bin 123: 28 of cap free
Amount of items: 2
Items: 
Size: 13334 Color: 0
Size: 2958 Color: 1

Bin 124: 29 of cap free
Amount of items: 2
Items: 
Size: 13361 Color: 1
Size: 2930 Color: 0

Bin 125: 29 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 0
Size: 2701 Color: 1

Bin 126: 30 of cap free
Amount of items: 2
Items: 
Size: 14468 Color: 0
Size: 1822 Color: 1

Bin 127: 33 of cap free
Amount of items: 2
Items: 
Size: 14269 Color: 0
Size: 2018 Color: 1

Bin 128: 34 of cap free
Amount of items: 2
Items: 
Size: 12460 Color: 1
Size: 3826 Color: 0

Bin 129: 34 of cap free
Amount of items: 2
Items: 
Size: 14265 Color: 1
Size: 2021 Color: 0

Bin 130: 35 of cap free
Amount of items: 2
Items: 
Size: 13684 Color: 0
Size: 2601 Color: 1

Bin 131: 35 of cap free
Amount of items: 2
Items: 
Size: 13929 Color: 1
Size: 2356 Color: 0

Bin 132: 35 of cap free
Amount of items: 2
Items: 
Size: 13918 Color: 0
Size: 2367 Color: 1

Bin 133: 38 of cap free
Amount of items: 3
Items: 
Size: 12163 Color: 0
Size: 3715 Color: 0
Size: 404 Color: 1

Bin 134: 38 of cap free
Amount of items: 2
Items: 
Size: 11906 Color: 1
Size: 4376 Color: 0

Bin 135: 42 of cap free
Amount of items: 2
Items: 
Size: 13718 Color: 1
Size: 2560 Color: 0

Bin 136: 42 of cap free
Amount of items: 2
Items: 
Size: 14174 Color: 0
Size: 2104 Color: 1

Bin 137: 49 of cap free
Amount of items: 2
Items: 
Size: 11227 Color: 1
Size: 5044 Color: 0

Bin 138: 49 of cap free
Amount of items: 2
Items: 
Size: 13329 Color: 0
Size: 2942 Color: 1

Bin 139: 50 of cap free
Amount of items: 3
Items: 
Size: 14166 Color: 0
Size: 1160 Color: 1
Size: 944 Color: 1

Bin 140: 50 of cap free
Amount of items: 2
Items: 
Size: 12976 Color: 1
Size: 3294 Color: 0

Bin 141: 50 of cap free
Amount of items: 2
Items: 
Size: 9469 Color: 0
Size: 6801 Color: 1

Bin 142: 52 of cap free
Amount of items: 2
Items: 
Size: 12560 Color: 1
Size: 3708 Color: 0

Bin 143: 53 of cap free
Amount of items: 2
Items: 
Size: 14246 Color: 1
Size: 2021 Color: 0

Bin 144: 56 of cap free
Amount of items: 3
Items: 
Size: 10352 Color: 0
Size: 5536 Color: 1
Size: 376 Color: 0

Bin 145: 56 of cap free
Amount of items: 2
Items: 
Size: 11220 Color: 0
Size: 5044 Color: 1

Bin 146: 56 of cap free
Amount of items: 2
Items: 
Size: 13771 Color: 1
Size: 2493 Color: 0

Bin 147: 57 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 1
Size: 2223 Color: 0

Bin 148: 57 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 1
Size: 1873 Color: 0

Bin 149: 58 of cap free
Amount of items: 2
Items: 
Size: 12836 Color: 1
Size: 3426 Color: 0

Bin 150: 60 of cap free
Amount of items: 2
Items: 
Size: 14164 Color: 0
Size: 2096 Color: 1

Bin 151: 68 of cap free
Amount of items: 2
Items: 
Size: 10404 Color: 1
Size: 5848 Color: 0

Bin 152: 75 of cap free
Amount of items: 2
Items: 
Size: 14333 Color: 0
Size: 1912 Color: 1

Bin 153: 78 of cap free
Amount of items: 2
Items: 
Size: 13876 Color: 0
Size: 2366 Color: 1

Bin 154: 82 of cap free
Amount of items: 2
Items: 
Size: 13177 Color: 1
Size: 3061 Color: 0

Bin 155: 85 of cap free
Amount of items: 2
Items: 
Size: 11571 Color: 0
Size: 4664 Color: 1

Bin 156: 86 of cap free
Amount of items: 2
Items: 
Size: 14326 Color: 0
Size: 1908 Color: 1

Bin 157: 87 of cap free
Amount of items: 2
Items: 
Size: 13081 Color: 0
Size: 3152 Color: 1

Bin 158: 91 of cap free
Amount of items: 2
Items: 
Size: 13653 Color: 0
Size: 2576 Color: 1

Bin 159: 94 of cap free
Amount of items: 2
Items: 
Size: 11221 Color: 1
Size: 5005 Color: 0

Bin 160: 103 of cap free
Amount of items: 2
Items: 
Size: 9417 Color: 0
Size: 6800 Color: 1

Bin 161: 109 of cap free
Amount of items: 2
Items: 
Size: 13583 Color: 1
Size: 2628 Color: 0

Bin 162: 116 of cap free
Amount of items: 2
Items: 
Size: 12825 Color: 1
Size: 3379 Color: 0

Bin 163: 118 of cap free
Amount of items: 2
Items: 
Size: 12647 Color: 0
Size: 3555 Color: 1

Bin 164: 118 of cap free
Amount of items: 2
Items: 
Size: 10372 Color: 1
Size: 5830 Color: 0

Bin 165: 121 of cap free
Amount of items: 2
Items: 
Size: 13402 Color: 1
Size: 2797 Color: 0

Bin 166: 129 of cap free
Amount of items: 2
Items: 
Size: 14463 Color: 0
Size: 1728 Color: 1

Bin 167: 134 of cap free
Amount of items: 2
Items: 
Size: 12370 Color: 1
Size: 3816 Color: 0

Bin 168: 135 of cap free
Amount of items: 3
Items: 
Size: 14185 Color: 1
Size: 1418 Color: 0
Size: 582 Color: 0

Bin 169: 136 of cap free
Amount of items: 2
Items: 
Size: 13563 Color: 1
Size: 2621 Color: 0

Bin 170: 144 of cap free
Amount of items: 2
Items: 
Size: 13895 Color: 1
Size: 2281 Color: 0

Bin 171: 152 of cap free
Amount of items: 2
Items: 
Size: 11876 Color: 0
Size: 4292 Color: 1

Bin 172: 152 of cap free
Amount of items: 2
Items: 
Size: 12806 Color: 1
Size: 3362 Color: 0

Bin 173: 156 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 0
Size: 2908 Color: 1

Bin 174: 158 of cap free
Amount of items: 2
Items: 
Size: 11826 Color: 1
Size: 4336 Color: 0

Bin 175: 159 of cap free
Amount of items: 2
Items: 
Size: 9364 Color: 0
Size: 6797 Color: 1

Bin 176: 167 of cap free
Amount of items: 2
Items: 
Size: 9361 Color: 0
Size: 6792 Color: 1

Bin 177: 168 of cap free
Amount of items: 2
Items: 
Size: 12860 Color: 0
Size: 3292 Color: 1

Bin 178: 172 of cap free
Amount of items: 2
Items: 
Size: 11172 Color: 1
Size: 4976 Color: 0

Bin 179: 175 of cap free
Amount of items: 2
Items: 
Size: 11863 Color: 0
Size: 4282 Color: 1

Bin 180: 182 of cap free
Amount of items: 2
Items: 
Size: 9334 Color: 1
Size: 6804 Color: 0

Bin 181: 184 of cap free
Amount of items: 2
Items: 
Size: 9348 Color: 0
Size: 6788 Color: 1

Bin 182: 192 of cap free
Amount of items: 2
Items: 
Size: 9326 Color: 1
Size: 6802 Color: 0

Bin 183: 193 of cap free
Amount of items: 2
Items: 
Size: 10315 Color: 1
Size: 5812 Color: 0

Bin 184: 208 of cap free
Amount of items: 2
Items: 
Size: 10268 Color: 0
Size: 5844 Color: 1

Bin 185: 214 of cap free
Amount of items: 2
Items: 
Size: 13890 Color: 1
Size: 2216 Color: 0

Bin 186: 232 of cap free
Amount of items: 2
Items: 
Size: 9316 Color: 0
Size: 6772 Color: 1

Bin 187: 236 of cap free
Amount of items: 2
Items: 
Size: 9320 Color: 1
Size: 6764 Color: 0

Bin 188: 239 of cap free
Amount of items: 2
Items: 
Size: 10280 Color: 1
Size: 5801 Color: 0

Bin 189: 261 of cap free
Amount of items: 2
Items: 
Size: 12267 Color: 1
Size: 3792 Color: 0

Bin 190: 261 of cap free
Amount of items: 2
Items: 
Size: 10617 Color: 1
Size: 5442 Color: 0

Bin 191: 264 of cap free
Amount of items: 2
Items: 
Size: 12312 Color: 0
Size: 3744 Color: 1

Bin 192: 277 of cap free
Amount of items: 2
Items: 
Size: 11792 Color: 0
Size: 4251 Color: 1

Bin 193: 279 of cap free
Amount of items: 2
Items: 
Size: 10237 Color: 0
Size: 5804 Color: 1

Bin 194: 285 of cap free
Amount of items: 2
Items: 
Size: 8261 Color: 1
Size: 7774 Color: 0

Bin 195: 299 of cap free
Amount of items: 2
Items: 
Size: 12790 Color: 1
Size: 3231 Color: 0

Bin 196: 1650 of cap free
Amount of items: 1
Items: 
Size: 14670 Color: 1

Bin 197: 1652 of cap free
Amount of items: 1
Items: 
Size: 14668 Color: 1

Bin 198: 1874 of cap free
Amount of items: 1
Items: 
Size: 14446 Color: 0

Bin 199: 1899 of cap free
Amount of items: 1
Items: 
Size: 14421 Color: 0

Total size: 3231360
Total free space: 16320

