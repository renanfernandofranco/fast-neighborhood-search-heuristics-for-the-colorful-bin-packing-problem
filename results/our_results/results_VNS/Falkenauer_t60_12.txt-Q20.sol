Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 16
Size: 304 Color: 18
Size: 301 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 352 Color: 11
Size: 281 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 18
Size: 286 Color: 12
Size: 274 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 6
Size: 309 Color: 7
Size: 256 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 17
Size: 279 Color: 2
Size: 271 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 17
Size: 341 Color: 0
Size: 270 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 7
Size: 286 Color: 3
Size: 252 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 329 Color: 2
Size: 405 Color: 19
Size: 266 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 11
Size: 314 Color: 0
Size: 266 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 8
Size: 261 Color: 19
Size: 267 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 13
Size: 321 Color: 17
Size: 255 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 299 Color: 15
Size: 268 Color: 8
Size: 433 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 12
Size: 304 Color: 4
Size: 260 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 6
Size: 339 Color: 19
Size: 268 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 18
Size: 337 Color: 0
Size: 272 Color: 5

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 11
Size: 372 Color: 12
Size: 255 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 3
Size: 276 Color: 5
Size: 254 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8
Size: 312 Color: 1
Size: 250 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 2
Size: 254 Color: 3
Size: 251 Color: 5

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 6
Size: 302 Color: 3
Size: 256 Color: 2

Total size: 20000
Total free space: 0

