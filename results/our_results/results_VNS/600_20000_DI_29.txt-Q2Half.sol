Capicity Bin: 16800
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 10728 Color: 1
Size: 2767 Color: 1
Size: 2521 Color: 1
Size: 480 Color: 0
Size: 304 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 13145 Color: 1
Size: 3105 Color: 1
Size: 550 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 1
Size: 5492 Color: 1
Size: 1096 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9312 Color: 1
Size: 7000 Color: 1
Size: 488 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13481 Color: 1
Size: 2031 Color: 1
Size: 1288 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 8696 Color: 1
Size: 7048 Color: 1
Size: 1056 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9428 Color: 1
Size: 7004 Color: 1
Size: 368 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 8426 Color: 1
Size: 3974 Color: 1
Size: 3168 Color: 0
Size: 1232 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8402 Color: 1
Size: 7002 Color: 1
Size: 1396 Color: 0

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 8408 Color: 1
Size: 5602 Color: 1
Size: 1398 Color: 0
Size: 1392 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8409 Color: 1
Size: 6993 Color: 1
Size: 1398 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 1
Size: 6994 Color: 1
Size: 1396 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8412 Color: 1
Size: 6996 Color: 1
Size: 1392 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 8413 Color: 1
Size: 6991 Color: 1
Size: 1396 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8418 Color: 1
Size: 6986 Color: 1
Size: 1396 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 1
Size: 7576 Color: 1
Size: 720 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 1
Size: 6256 Color: 1
Size: 536 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 10139 Color: 1
Size: 5551 Color: 1
Size: 1110 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 1
Size: 5414 Color: 1
Size: 1080 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 1
Size: 5412 Color: 1
Size: 1080 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 10856 Color: 1
Size: 4968 Color: 1
Size: 976 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11044 Color: 1
Size: 4358 Color: 1
Size: 1398 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11074 Color: 1
Size: 4774 Color: 1
Size: 952 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11172 Color: 1
Size: 4708 Color: 1
Size: 920 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11204 Color: 1
Size: 4668 Color: 1
Size: 928 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11381 Color: 1
Size: 4517 Color: 1
Size: 902 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11528 Color: 1
Size: 4888 Color: 1
Size: 384 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11662 Color: 1
Size: 4282 Color: 1
Size: 856 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11665 Color: 1
Size: 4019 Color: 1
Size: 1116 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11704 Color: 1
Size: 4776 Color: 1
Size: 320 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11764 Color: 1
Size: 4076 Color: 1
Size: 960 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11864 Color: 1
Size: 4216 Color: 1
Size: 720 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11901 Color: 1
Size: 4083 Color: 1
Size: 816 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11916 Color: 1
Size: 4252 Color: 1
Size: 632 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 1
Size: 3684 Color: 1
Size: 1168 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11960 Color: 1
Size: 4408 Color: 1
Size: 432 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11964 Color: 1
Size: 4052 Color: 1
Size: 784 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12110 Color: 1
Size: 3910 Color: 1
Size: 780 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12251 Color: 1
Size: 3791 Color: 1
Size: 758 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 1
Size: 3790 Color: 1
Size: 756 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12302 Color: 1
Size: 3598 Color: 1
Size: 900 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 1
Size: 4040 Color: 1
Size: 384 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12390 Color: 1
Size: 3658 Color: 1
Size: 752 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12414 Color: 1
Size: 3378 Color: 1
Size: 1008 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12479 Color: 1
Size: 3601 Color: 1
Size: 720 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 1
Size: 3544 Color: 1
Size: 704 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 1
Size: 3300 Color: 1
Size: 936 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12728 Color: 1
Size: 3480 Color: 1
Size: 592 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12734 Color: 1
Size: 3390 Color: 1
Size: 676 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12741 Color: 1
Size: 3595 Color: 1
Size: 464 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12843 Color: 1
Size: 3299 Color: 1
Size: 658 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12996 Color: 1
Size: 3068 Color: 1
Size: 736 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 1
Size: 2968 Color: 1
Size: 816 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13054 Color: 1
Size: 3214 Color: 1
Size: 532 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 1
Size: 3124 Color: 1
Size: 616 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 1
Size: 3092 Color: 1
Size: 616 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13226 Color: 1
Size: 2982 Color: 1
Size: 592 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13229 Color: 1
Size: 3185 Color: 1
Size: 386 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13240 Color: 1
Size: 2602 Color: 1
Size: 958 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 1
Size: 2952 Color: 1
Size: 576 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13293 Color: 1
Size: 3009 Color: 1
Size: 498 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13374 Color: 1
Size: 2858 Color: 1
Size: 568 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13393 Color: 1
Size: 2841 Color: 1
Size: 566 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13408 Color: 1
Size: 2868 Color: 1
Size: 524 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13460 Color: 1
Size: 2988 Color: 1
Size: 352 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 1
Size: 2792 Color: 1
Size: 544 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13534 Color: 1
Size: 2466 Color: 1
Size: 800 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 1
Size: 2564 Color: 1
Size: 672 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13650 Color: 1
Size: 2616 Color: 1
Size: 534 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 1
Size: 2804 Color: 1
Size: 336 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 1
Size: 2584 Color: 1
Size: 544 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13682 Color: 1
Size: 2354 Color: 1
Size: 764 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13724 Color: 1
Size: 2452 Color: 1
Size: 624 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 1
Size: 2624 Color: 1
Size: 432 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 1
Size: 2700 Color: 1
Size: 300 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13860 Color: 1
Size: 2428 Color: 1
Size: 512 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 1
Size: 2484 Color: 1
Size: 436 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 1
Size: 2244 Color: 1
Size: 664 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13923 Color: 1
Size: 2399 Color: 1
Size: 478 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13976 Color: 1
Size: 2272 Color: 1
Size: 552 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13978 Color: 1
Size: 2414 Color: 1
Size: 408 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 14007 Color: 1
Size: 2329 Color: 1
Size: 464 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 1
Size: 2360 Color: 1
Size: 384 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 14076 Color: 1
Size: 2492 Color: 1
Size: 232 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 14086 Color: 1
Size: 2262 Color: 1
Size: 452 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 1
Size: 1944 Color: 1
Size: 748 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 1
Size: 2239 Color: 1
Size: 446 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 1
Size: 2296 Color: 1
Size: 340 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 14178 Color: 1
Size: 2110 Color: 1
Size: 512 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 14188 Color: 1
Size: 2244 Color: 1
Size: 368 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 14199 Color: 1
Size: 2169 Color: 1
Size: 432 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 1
Size: 2168 Color: 1
Size: 432 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 14205 Color: 1
Size: 2131 Color: 1
Size: 464 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 14232 Color: 1
Size: 2152 Color: 1
Size: 416 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14275 Color: 1
Size: 2105 Color: 1
Size: 420 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 14330 Color: 1
Size: 2062 Color: 1
Size: 408 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 14342 Color: 1
Size: 1938 Color: 1
Size: 520 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 1
Size: 1916 Color: 1
Size: 504 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 14406 Color: 1
Size: 2044 Color: 1
Size: 350 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14438 Color: 1
Size: 2106 Color: 1
Size: 256 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 14472 Color: 1
Size: 1928 Color: 1
Size: 400 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 14475 Color: 1
Size: 1669 Color: 1
Size: 656 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 14478 Color: 1
Size: 1842 Color: 1
Size: 480 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 14483 Color: 1
Size: 1931 Color: 1
Size: 386 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 1
Size: 1932 Color: 1
Size: 384 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 1
Size: 2080 Color: 1
Size: 232 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14508 Color: 1
Size: 1860 Color: 1
Size: 432 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14572 Color: 1
Size: 1736 Color: 1
Size: 492 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14578 Color: 1
Size: 1854 Color: 1
Size: 368 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 1
Size: 1834 Color: 1
Size: 376 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14601 Color: 1
Size: 1833 Color: 1
Size: 366 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14656 Color: 1
Size: 1792 Color: 1
Size: 352 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14668 Color: 1
Size: 1524 Color: 1
Size: 608 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14674 Color: 1
Size: 1774 Color: 1
Size: 352 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14693 Color: 1
Size: 1757 Color: 1
Size: 350 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 1
Size: 2020 Color: 1
Size: 84 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14701 Color: 1
Size: 2085 Color: 1
Size: 14 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 1
Size: 1652 Color: 1
Size: 420 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14758 Color: 1
Size: 1780 Color: 1
Size: 262 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14799 Color: 1
Size: 1665 Color: 1
Size: 336 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14804 Color: 1
Size: 1668 Color: 1
Size: 328 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14806 Color: 1
Size: 1986 Color: 1
Size: 8 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14820 Color: 1
Size: 1688 Color: 1
Size: 292 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14830 Color: 1
Size: 1642 Color: 1
Size: 328 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14836 Color: 1
Size: 1628 Color: 1
Size: 336 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14845 Color: 1
Size: 1631 Color: 1
Size: 324 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14886 Color: 1
Size: 1598 Color: 1
Size: 316 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14904 Color: 1
Size: 1592 Color: 1
Size: 304 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14948 Color: 1
Size: 1548 Color: 1
Size: 304 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14961 Color: 1
Size: 1533 Color: 1
Size: 306 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14972 Color: 1
Size: 1464 Color: 1
Size: 364 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 14980 Color: 1
Size: 1524 Color: 1
Size: 296 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 14982 Color: 1
Size: 1518 Color: 1
Size: 300 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14990 Color: 1
Size: 1478 Color: 1
Size: 332 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 14995 Color: 1
Size: 1505 Color: 1
Size: 300 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 15000 Color: 1
Size: 1442 Color: 1
Size: 358 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 15030 Color: 1
Size: 1644 Color: 1
Size: 126 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 15048 Color: 1
Size: 1512 Color: 1
Size: 240 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 15068 Color: 1
Size: 1264 Color: 1
Size: 468 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 15069 Color: 1
Size: 1443 Color: 1
Size: 288 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 15070 Color: 1
Size: 1136 Color: 1
Size: 594 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 15076 Color: 1
Size: 1406 Color: 1
Size: 318 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 15092 Color: 1
Size: 1428 Color: 1
Size: 280 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 15114 Color: 1
Size: 1444 Color: 1
Size: 242 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 9524 Color: 1
Size: 6043 Color: 1
Size: 1232 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 12247 Color: 1
Size: 4104 Color: 1
Size: 448 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 12979 Color: 1
Size: 3532 Color: 1
Size: 288 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 13124 Color: 1
Size: 2603 Color: 1
Size: 1072 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 1
Size: 2977 Color: 1
Size: 632 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 13365 Color: 1
Size: 3338 Color: 1
Size: 96 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 13677 Color: 1
Size: 2722 Color: 1
Size: 400 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 13775 Color: 1
Size: 2576 Color: 1
Size: 448 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 1
Size: 2163 Color: 1
Size: 808 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 14365 Color: 1
Size: 2050 Color: 1
Size: 384 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 14412 Color: 1
Size: 1595 Color: 1
Size: 792 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 14427 Color: 1
Size: 2180 Color: 1
Size: 192 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 14602 Color: 1
Size: 1793 Color: 1
Size: 404 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 14887 Color: 1
Size: 1768 Color: 1
Size: 144 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 15009 Color: 1
Size: 1510 Color: 1
Size: 280 Color: 0

Bin 160: 1 of cap free
Amount of items: 3
Items: 
Size: 9066 Color: 1
Size: 7001 Color: 1
Size: 732 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 9638 Color: 1
Size: 6200 Color: 1
Size: 960 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 1
Size: 5250 Color: 1
Size: 1208 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 10790 Color: 1
Size: 5912 Color: 1
Size: 96 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 11140 Color: 1
Size: 4804 Color: 1
Size: 854 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 11382 Color: 1
Size: 5064 Color: 1
Size: 352 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 11896 Color: 1
Size: 3678 Color: 1
Size: 1224 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 12034 Color: 1
Size: 4284 Color: 1
Size: 480 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 12206 Color: 1
Size: 4060 Color: 1
Size: 532 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 1
Size: 3750 Color: 1
Size: 416 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 1
Size: 3400 Color: 1
Size: 600 Color: 0

Bin 171: 2 of cap free
Amount of items: 3
Items: 
Size: 13002 Color: 1
Size: 3160 Color: 1
Size: 636 Color: 0

Bin 172: 2 of cap free
Amount of items: 3
Items: 
Size: 13118 Color: 1
Size: 3172 Color: 1
Size: 508 Color: 0

Bin 173: 2 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 1
Size: 2626 Color: 1
Size: 808 Color: 0

Bin 174: 2 of cap free
Amount of items: 3
Items: 
Size: 13842 Color: 1
Size: 2440 Color: 1
Size: 516 Color: 0

Bin 175: 2 of cap free
Amount of items: 3
Items: 
Size: 13906 Color: 1
Size: 2204 Color: 1
Size: 688 Color: 0

Bin 176: 2 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 1
Size: 2186 Color: 1
Size: 256 Color: 0

Bin 177: 2 of cap free
Amount of items: 3
Items: 
Size: 14649 Color: 1
Size: 1925 Color: 1
Size: 224 Color: 0

Bin 178: 3 of cap free
Amount of items: 3
Items: 
Size: 9549 Color: 1
Size: 6192 Color: 1
Size: 1056 Color: 0

Bin 179: 3 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 1
Size: 3085 Color: 1
Size: 868 Color: 0

Bin 180: 3 of cap free
Amount of items: 3
Items: 
Size: 8405 Color: 1
Size: 7440 Color: 1
Size: 952 Color: 0

Bin 181: 4 of cap free
Amount of items: 3
Items: 
Size: 10952 Color: 1
Size: 4724 Color: 1
Size: 1120 Color: 0

Bin 182: 4 of cap free
Amount of items: 3
Items: 
Size: 11574 Color: 1
Size: 4518 Color: 1
Size: 704 Color: 0

Bin 183: 4 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 1
Size: 4088 Color: 1
Size: 328 Color: 0

Bin 184: 7 of cap free
Amount of items: 3
Items: 
Size: 8404 Color: 1
Size: 6997 Color: 1
Size: 1392 Color: 0

Bin 185: 7 of cap free
Amount of items: 3
Items: 
Size: 9453 Color: 1
Size: 6148 Color: 1
Size: 1192 Color: 0

Bin 186: 9 of cap free
Amount of items: 3
Items: 
Size: 10631 Color: 1
Size: 5672 Color: 1
Size: 488 Color: 0

Bin 187: 10 of cap free
Amount of items: 3
Items: 
Size: 11932 Color: 1
Size: 3830 Color: 1
Size: 1028 Color: 0

Bin 188: 18 of cap free
Amount of items: 3
Items: 
Size: 14792 Color: 1
Size: 1702 Color: 1
Size: 288 Color: 0

Bin 189: 18 of cap free
Amount of items: 3
Items: 
Size: 10082 Color: 1
Size: 6220 Color: 1
Size: 480 Color: 0

Bin 190: 20 of cap free
Amount of items: 3
Items: 
Size: 11042 Color: 1
Size: 5010 Color: 1
Size: 728 Color: 0

Bin 191: 27 of cap free
Amount of items: 3
Items: 
Size: 9368 Color: 1
Size: 7085 Color: 1
Size: 320 Color: 0

Bin 192: 50 of cap free
Amount of items: 3
Items: 
Size: 14491 Color: 1
Size: 1939 Color: 1
Size: 320 Color: 0

Bin 193: 51 of cap free
Amount of items: 3
Items: 
Size: 11041 Color: 1
Size: 5388 Color: 1
Size: 320 Color: 0

Bin 194: 88 of cap free
Amount of items: 3
Items: 
Size: 14270 Color: 1
Size: 2154 Color: 1
Size: 288 Color: 0

Bin 195: 110 of cap free
Amount of items: 3
Items: 
Size: 9720 Color: 1
Size: 5970 Color: 1
Size: 1000 Color: 0

Bin 196: 269 of cap free
Amount of items: 3
Items: 
Size: 8401 Color: 1
Size: 7842 Color: 1
Size: 288 Color: 0

Bin 197: 1202 of cap free
Amount of items: 3
Items: 
Size: 8288 Color: 1
Size: 6446 Color: 1
Size: 864 Color: 0

Bin 198: 6058 of cap free
Amount of items: 3
Items: 
Size: 5141 Color: 1
Size: 4801 Color: 1
Size: 800 Color: 0

Bin 199: 8785 of cap free
Amount of items: 3
Items: 
Size: 4281 Color: 1
Size: 3166 Color: 1
Size: 568 Color: 0

Total size: 3326400
Total free space: 16800

