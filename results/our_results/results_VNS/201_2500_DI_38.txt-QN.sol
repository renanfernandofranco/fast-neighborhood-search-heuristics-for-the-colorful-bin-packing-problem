Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1636 Color: 156
Size: 796 Color: 122
Size: 40 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 160
Size: 620 Color: 112
Size: 148 Color: 55

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 173
Size: 542 Color: 106
Size: 32 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 176
Size: 398 Color: 91
Size: 136 Color: 53

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 181
Size: 430 Color: 95
Size: 44 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 182
Size: 393 Color: 90
Size: 78 Color: 28

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 186
Size: 310 Color: 82
Size: 100 Color: 41

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 189
Size: 238 Color: 73
Size: 132 Color: 51

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 190
Size: 340 Color: 85
Size: 24 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 191
Size: 246 Color: 74
Size: 116 Color: 46

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 192
Size: 314 Color: 83
Size: 40 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 196
Size: 222 Color: 70
Size: 60 Color: 18

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 199
Size: 172 Color: 60
Size: 96 Color: 39

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2206 Color: 200
Size: 218 Color: 69
Size: 48 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2214 Color: 201
Size: 162 Color: 58
Size: 96 Color: 40

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 159
Size: 618 Color: 111
Size: 152 Color: 56

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 1771 Color: 165
Size: 700 Color: 118

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1867 Color: 170
Size: 556 Color: 108
Size: 48 Color: 14

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 177
Size: 452 Color: 98
Size: 78 Color: 29

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1958 Color: 178
Size: 505 Color: 104
Size: 8 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1993 Color: 180
Size: 384 Color: 88
Size: 94 Color: 38

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 2028 Color: 185
Size: 443 Color: 96

Bin 23: 2 of cap free
Amount of items: 3
Items: 
Size: 1388 Color: 144
Size: 1018 Color: 132
Size: 64 Color: 21

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1862 Color: 169
Size: 504 Color: 103
Size: 104 Color: 42

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 172
Size: 481 Color: 100
Size: 92 Color: 37

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1932 Color: 175
Size: 538 Color: 105

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 179
Size: 482 Color: 101
Size: 8 Color: 2

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 2198 Color: 198
Size: 272 Color: 77

Bin 29: 3 of cap free
Amount of items: 4
Items: 
Size: 1732 Color: 162
Size: 681 Color: 117
Size: 40 Color: 8
Size: 16 Color: 3

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 171
Size: 585 Color: 110
Size: 8 Color: 0

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 2068 Color: 187
Size: 401 Color: 92

Bin 32: 4 of cap free
Amount of items: 6
Items: 
Size: 1238 Color: 139
Size: 378 Color: 87
Size: 372 Color: 86
Size: 308 Color: 81
Size: 88 Color: 32
Size: 84 Color: 31

Bin 33: 4 of cap free
Amount of items: 2
Items: 
Size: 1817 Color: 167
Size: 651 Color: 115

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 1921 Color: 174
Size: 547 Color: 107

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 2022 Color: 184
Size: 446 Color: 97

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 2082 Color: 188
Size: 386 Color: 89

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 2166 Color: 194
Size: 302 Color: 80

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 2178 Color: 195
Size: 290 Color: 79

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 2196 Color: 197
Size: 272 Color: 76

Bin 40: 5 of cap free
Amount of items: 3
Items: 
Size: 1524 Color: 150
Size: 883 Color: 128
Size: 60 Color: 17

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 2006 Color: 183
Size: 461 Color: 99

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 1413 Color: 146
Size: 989 Color: 130
Size: 64 Color: 20

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 1422 Color: 147
Size: 1044 Color: 137

Bin 44: 6 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 166
Size: 638 Color: 113
Size: 16 Color: 4

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 2140 Color: 193
Size: 326 Color: 84

Bin 46: 7 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 154
Size: 842 Color: 126
Size: 44 Color: 12

Bin 47: 7 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 155
Size: 819 Color: 124
Size: 44 Color: 11

Bin 48: 7 of cap free
Amount of items: 3
Items: 
Size: 1755 Color: 164
Size: 662 Color: 116
Size: 48 Color: 13

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 1822 Color: 168
Size: 643 Color: 114

Bin 50: 10 of cap free
Amount of items: 16
Items: 
Size: 204 Color: 68
Size: 204 Color: 67
Size: 204 Color: 66
Size: 204 Color: 65
Size: 200 Color: 64
Size: 184 Color: 63
Size: 176 Color: 62
Size: 176 Color: 61
Size: 168 Color: 59
Size: 120 Color: 48
Size: 120 Color: 47
Size: 108 Color: 45
Size: 108 Color: 44
Size: 108 Color: 43
Size: 90 Color: 36
Size: 88 Color: 35

Bin 51: 10 of cap free
Amount of items: 2
Items: 
Size: 1462 Color: 148
Size: 1000 Color: 131

Bin 52: 11 of cap free
Amount of items: 5
Items: 
Size: 1239 Color: 140
Size: 576 Color: 109
Size: 412 Color: 93
Size: 154 Color: 57
Size: 80 Color: 30

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 1552 Color: 152
Size: 908 Color: 129

Bin 54: 12 of cap free
Amount of items: 2
Items: 
Size: 1734 Color: 163
Size: 726 Color: 119

Bin 55: 13 of cap free
Amount of items: 2
Items: 
Size: 1657 Color: 157
Size: 802 Color: 123

Bin 56: 14 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 153
Size: 840 Color: 125
Size: 56 Color: 16

Bin 57: 15 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 151
Size: 500 Color: 102
Size: 418 Color: 94

Bin 58: 15 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 158
Size: 779 Color: 121

Bin 59: 17 of cap free
Amount of items: 2
Items: 
Size: 1710 Color: 161
Size: 745 Color: 120

Bin 60: 36 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 145
Size: 1031 Color: 136

Bin 61: 39 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 149
Size: 878 Color: 127
Size: 64 Color: 19

Bin 62: 49 of cap free
Amount of items: 7
Items: 
Size: 1237 Color: 138
Size: 284 Color: 78
Size: 260 Color: 75
Size: 236 Color: 72
Size: 230 Color: 71
Size: 88 Color: 34
Size: 88 Color: 33

Bin 63: 50 of cap free
Amount of items: 4
Items: 
Size: 1254 Color: 143
Size: 1030 Color: 135
Size: 72 Color: 23
Size: 66 Color: 22

Bin 64: 52 of cap free
Amount of items: 4
Items: 
Size: 1244 Color: 141
Size: 1022 Color: 133
Size: 78 Color: 27
Size: 76 Color: 26

Bin 65: 53 of cap free
Amount of items: 4
Items: 
Size: 1246 Color: 142
Size: 1029 Color: 134
Size: 72 Color: 25
Size: 72 Color: 24

Bin 66: 1942 of cap free
Amount of items: 4
Items: 
Size: 144 Color: 54
Size: 134 Color: 52
Size: 128 Color: 50
Size: 124 Color: 49

Total size: 160680
Total free space: 2472

