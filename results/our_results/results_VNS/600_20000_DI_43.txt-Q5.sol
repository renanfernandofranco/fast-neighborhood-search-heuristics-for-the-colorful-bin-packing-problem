Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8228 Color: 3
Size: 7676 Color: 2
Size: 384 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8692 Color: 4
Size: 6776 Color: 2
Size: 820 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9262 Color: 4
Size: 5858 Color: 2
Size: 1168 Color: 0

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 10240 Color: 1
Size: 6048 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10352 Color: 2
Size: 5456 Color: 0
Size: 480 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10653 Color: 0
Size: 3222 Color: 2
Size: 2413 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11316 Color: 1
Size: 4620 Color: 4
Size: 352 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 3
Size: 3964 Color: 1
Size: 308 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12140 Color: 2
Size: 3776 Color: 3
Size: 372 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12183 Color: 2
Size: 3421 Color: 0
Size: 684 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 3
Size: 3792 Color: 1
Size: 296 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 3
Size: 3244 Color: 2
Size: 648 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12432 Color: 1
Size: 3568 Color: 4
Size: 288 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 2
Size: 3392 Color: 3
Size: 304 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12567 Color: 1
Size: 2801 Color: 2
Size: 920 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12700 Color: 2
Size: 2996 Color: 1
Size: 592 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12804 Color: 2
Size: 3112 Color: 0
Size: 372 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12912 Color: 0
Size: 2832 Color: 2
Size: 544 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 3
Size: 2804 Color: 2
Size: 560 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13017 Color: 4
Size: 2727 Color: 0
Size: 544 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13116 Color: 0
Size: 2324 Color: 2
Size: 848 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13172 Color: 1
Size: 1608 Color: 3
Size: 1508 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 2
Size: 2208 Color: 3
Size: 784 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13314 Color: 4
Size: 2482 Color: 1
Size: 492 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 2
Size: 2512 Color: 1
Size: 460 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13394 Color: 2
Size: 1512 Color: 4
Size: 1382 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13397 Color: 0
Size: 2411 Color: 2
Size: 480 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 3
Size: 1964 Color: 4
Size: 924 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13524 Color: 1
Size: 2314 Color: 2
Size: 450 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13556 Color: 0
Size: 2408 Color: 1
Size: 324 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13616 Color: 0
Size: 2188 Color: 2
Size: 484 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13626 Color: 4
Size: 1474 Color: 4
Size: 1188 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13649 Color: 2
Size: 2505 Color: 0
Size: 134 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 4
Size: 2108 Color: 2
Size: 512 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13706 Color: 2
Size: 2024 Color: 3
Size: 558 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 4
Size: 2152 Color: 2
Size: 416 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 4
Size: 1564 Color: 0
Size: 960 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13830 Color: 0
Size: 1824 Color: 2
Size: 634 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 0
Size: 2010 Color: 2
Size: 442 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 2
Size: 2062 Color: 0
Size: 348 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13868 Color: 0
Size: 2044 Color: 1
Size: 376 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 1
Size: 1768 Color: 2
Size: 620 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13921 Color: 4
Size: 1973 Color: 4
Size: 394 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13978 Color: 0
Size: 2190 Color: 1
Size: 120 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 2
Size: 1902 Color: 0
Size: 400 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 2
Size: 1562 Color: 4
Size: 700 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 2
Size: 1884 Color: 1
Size: 376 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 3
Size: 1584 Color: 0
Size: 672 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14092 Color: 4
Size: 1180 Color: 3
Size: 1016 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 4
Size: 1818 Color: 2
Size: 360 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 1144 Color: 2
Size: 976 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14186 Color: 1
Size: 1670 Color: 2
Size: 432 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 0
Size: 1742 Color: 2
Size: 344 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14220 Color: 4
Size: 1428 Color: 2
Size: 640 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14252 Color: 2
Size: 1724 Color: 3
Size: 312 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 3
Size: 1088 Color: 4
Size: 888 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14334 Color: 4
Size: 1464 Color: 4
Size: 490 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 1
Size: 1442 Color: 2
Size: 486 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14418 Color: 2
Size: 1172 Color: 3
Size: 698 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 4
Size: 992 Color: 0
Size: 908 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14420 Color: 2
Size: 1468 Color: 0
Size: 400 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14458 Color: 3
Size: 1526 Color: 2
Size: 304 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14486 Color: 1
Size: 1356 Color: 2
Size: 446 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14504 Color: 4
Size: 1336 Color: 3
Size: 448 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14536 Color: 3
Size: 1352 Color: 0
Size: 400 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 2
Size: 1344 Color: 4
Size: 332 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 2
Size: 1352 Color: 4
Size: 320 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 14578 Color: 0
Size: 1426 Color: 3
Size: 284 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 14634 Color: 2
Size: 1304 Color: 0
Size: 350 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 4
Size: 1352 Color: 0
Size: 292 Color: 2

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 9932 Color: 1
Size: 5907 Color: 4
Size: 448 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 10182 Color: 1
Size: 5309 Color: 0
Size: 796 Color: 3

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 10591 Color: 1
Size: 5416 Color: 0
Size: 280 Color: 2

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 10864 Color: 2
Size: 5247 Color: 4
Size: 176 Color: 4

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 11723 Color: 2
Size: 3988 Color: 1
Size: 576 Color: 4

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 2
Size: 3495 Color: 4
Size: 224 Color: 3

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13261 Color: 4
Size: 2604 Color: 2
Size: 422 Color: 0

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 13365 Color: 0
Size: 2650 Color: 2
Size: 272 Color: 0

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 4
Size: 2261 Color: 2
Size: 512 Color: 3

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 13601 Color: 2
Size: 2210 Color: 4
Size: 476 Color: 4

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 13633 Color: 4
Size: 1886 Color: 2
Size: 768 Color: 4

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 13749 Color: 2
Size: 2020 Color: 0
Size: 518 Color: 0

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 13818 Color: 1
Size: 2117 Color: 3
Size: 352 Color: 2

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 10070 Color: 4
Size: 5724 Color: 2
Size: 492 Color: 0

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 10328 Color: 3
Size: 5958 Color: 1

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 10838 Color: 3
Size: 4936 Color: 3
Size: 512 Color: 0

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 10950 Color: 0
Size: 4968 Color: 3
Size: 368 Color: 1

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 0
Size: 4454 Color: 1
Size: 720 Color: 4

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 11506 Color: 0
Size: 3324 Color: 2
Size: 1456 Color: 1

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 4
Size: 4542 Color: 0
Size: 272 Color: 3

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 11524 Color: 2
Size: 3928 Color: 2
Size: 834 Color: 3

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 11658 Color: 1
Size: 3972 Color: 2
Size: 656 Color: 0

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 3
Size: 3612 Color: 2
Size: 592 Color: 1

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 2
Size: 3506 Color: 1
Size: 352 Color: 3

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 2
Size: 2050 Color: 1
Size: 1460 Color: 0

Bin 96: 2 of cap free
Amount of items: 3
Items: 
Size: 12929 Color: 2
Size: 3101 Color: 1
Size: 256 Color: 4

Bin 97: 2 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 4
Size: 3130 Color: 0
Size: 140 Color: 3

Bin 98: 2 of cap free
Amount of items: 3
Items: 
Size: 13140 Color: 2
Size: 1952 Color: 0
Size: 1194 Color: 1

Bin 99: 2 of cap free
Amount of items: 3
Items: 
Size: 13341 Color: 1
Size: 2305 Color: 2
Size: 640 Color: 4

Bin 100: 3 of cap free
Amount of items: 9
Items: 
Size: 8145 Color: 0
Size: 1356 Color: 1
Size: 1356 Color: 1
Size: 1344 Color: 4
Size: 1152 Color: 1
Size: 1112 Color: 2
Size: 948 Color: 3
Size: 440 Color: 3
Size: 432 Color: 2

Bin 101: 3 of cap free
Amount of items: 7
Items: 
Size: 8147 Color: 3
Size: 1860 Color: 2
Size: 1816 Color: 3
Size: 1630 Color: 1
Size: 1620 Color: 4
Size: 748 Color: 1
Size: 464 Color: 4

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 9201 Color: 4
Size: 6708 Color: 2
Size: 376 Color: 0

Bin 103: 3 of cap free
Amount of items: 3
Items: 
Size: 11797 Color: 0
Size: 4104 Color: 2
Size: 384 Color: 0

Bin 104: 3 of cap free
Amount of items: 3
Items: 
Size: 12799 Color: 4
Size: 2910 Color: 2
Size: 576 Color: 0

Bin 105: 3 of cap free
Amount of items: 3
Items: 
Size: 13173 Color: 2
Size: 2296 Color: 1
Size: 816 Color: 0

Bin 106: 3 of cap free
Amount of items: 2
Items: 
Size: 13110 Color: 0
Size: 3175 Color: 4

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 8452 Color: 2
Size: 6772 Color: 1
Size: 1060 Color: 0

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 9113 Color: 3
Size: 6787 Color: 3
Size: 384 Color: 2

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 1
Size: 5084 Color: 0
Size: 580 Color: 1

Bin 110: 4 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 0
Size: 3986 Color: 3
Size: 768 Color: 2

Bin 111: 4 of cap free
Amount of items: 3
Items: 
Size: 11498 Color: 4
Size: 3994 Color: 1
Size: 792 Color: 0

Bin 112: 4 of cap free
Amount of items: 2
Items: 
Size: 11956 Color: 1
Size: 4328 Color: 3

Bin 113: 4 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 2
Size: 3416 Color: 3
Size: 560 Color: 3

Bin 114: 4 of cap free
Amount of items: 2
Items: 
Size: 12479 Color: 4
Size: 3805 Color: 1

Bin 115: 4 of cap free
Amount of items: 2
Items: 
Size: 12910 Color: 4
Size: 3374 Color: 1

Bin 116: 4 of cap free
Amount of items: 2
Items: 
Size: 14540 Color: 1
Size: 1744 Color: 2

Bin 117: 5 of cap free
Amount of items: 2
Items: 
Size: 13809 Color: 1
Size: 2474 Color: 4

Bin 118: 6 of cap free
Amount of items: 3
Items: 
Size: 10746 Color: 2
Size: 4528 Color: 3
Size: 1008 Color: 0

Bin 119: 6 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 1
Size: 2222 Color: 0

Bin 120: 6 of cap free
Amount of items: 2
Items: 
Size: 14286 Color: 1
Size: 1996 Color: 0

Bin 121: 7 of cap free
Amount of items: 3
Items: 
Size: 11592 Color: 0
Size: 4177 Color: 3
Size: 512 Color: 2

Bin 122: 7 of cap free
Amount of items: 3
Items: 
Size: 12095 Color: 2
Size: 2584 Color: 1
Size: 1602 Color: 3

Bin 123: 8 of cap free
Amount of items: 3
Items: 
Size: 8154 Color: 0
Size: 6782 Color: 2
Size: 1344 Color: 1

Bin 124: 8 of cap free
Amount of items: 3
Items: 
Size: 9142 Color: 2
Size: 6786 Color: 0
Size: 352 Color: 0

Bin 125: 8 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 0
Size: 6332 Color: 2
Size: 352 Color: 0

Bin 126: 8 of cap free
Amount of items: 2
Items: 
Size: 13864 Color: 4
Size: 2416 Color: 0

Bin 127: 8 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 1
Size: 2160 Color: 0

Bin 128: 8 of cap free
Amount of items: 2
Items: 
Size: 14224 Color: 0
Size: 2056 Color: 4

Bin 129: 8 of cap free
Amount of items: 2
Items: 
Size: 14580 Color: 0
Size: 1700 Color: 4

Bin 130: 9 of cap free
Amount of items: 3
Items: 
Size: 9919 Color: 4
Size: 5784 Color: 1
Size: 576 Color: 0

Bin 131: 9 of cap free
Amount of items: 3
Items: 
Size: 10376 Color: 0
Size: 3686 Color: 3
Size: 2217 Color: 2

Bin 132: 10 of cap free
Amount of items: 3
Items: 
Size: 10836 Color: 0
Size: 5090 Color: 1
Size: 352 Color: 4

Bin 133: 10 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 2
Size: 4622 Color: 3
Size: 288 Color: 2

Bin 134: 10 of cap free
Amount of items: 2
Items: 
Size: 11674 Color: 0
Size: 4604 Color: 3

Bin 135: 10 of cap free
Amount of items: 2
Items: 
Size: 14348 Color: 3
Size: 1930 Color: 0

Bin 136: 10 of cap free
Amount of items: 2
Items: 
Size: 14522 Color: 0
Size: 1756 Color: 4

Bin 137: 10 of cap free
Amount of items: 3
Items: 
Size: 14562 Color: 3
Size: 1656 Color: 4
Size: 60 Color: 0

Bin 138: 11 of cap free
Amount of items: 2
Items: 
Size: 12534 Color: 4
Size: 3743 Color: 1

Bin 139: 12 of cap free
Amount of items: 3
Items: 
Size: 8244 Color: 4
Size: 6768 Color: 2
Size: 1264 Color: 0

Bin 140: 12 of cap free
Amount of items: 3
Items: 
Size: 9352 Color: 0
Size: 6532 Color: 1
Size: 392 Color: 2

Bin 141: 13 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 0
Size: 3220 Color: 2
Size: 2647 Color: 1

Bin 142: 13 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 3
Size: 4623 Color: 2
Size: 456 Color: 0

Bin 143: 14 of cap free
Amount of items: 8
Items: 
Size: 8164 Color: 1
Size: 1588 Color: 1
Size: 1404 Color: 4
Size: 1400 Color: 0
Size: 1152 Color: 3
Size: 1048 Color: 3
Size: 1036 Color: 3
Size: 482 Color: 0

Bin 144: 15 of cap free
Amount of items: 3
Items: 
Size: 11232 Color: 2
Size: 4103 Color: 3
Size: 938 Color: 0

Bin 145: 16 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 4
Size: 2728 Color: 0

Bin 146: 18 of cap free
Amount of items: 5
Items: 
Size: 8148 Color: 0
Size: 2644 Color: 2
Size: 2414 Color: 2
Size: 2008 Color: 0
Size: 1056 Color: 1

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 13362 Color: 4
Size: 2908 Color: 0

Bin 148: 18 of cap free
Amount of items: 2
Items: 
Size: 14366 Color: 0
Size: 1904 Color: 4

Bin 149: 18 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 4
Size: 1662 Color: 3
Size: 32 Color: 1

Bin 150: 19 of cap free
Amount of items: 3
Items: 
Size: 9744 Color: 2
Size: 5981 Color: 1
Size: 544 Color: 0

Bin 151: 19 of cap free
Amount of items: 3
Items: 
Size: 9993 Color: 3
Size: 5988 Color: 2
Size: 288 Color: 4

Bin 152: 19 of cap free
Amount of items: 2
Items: 
Size: 11365 Color: 2
Size: 4904 Color: 0

Bin 153: 19 of cap free
Amount of items: 2
Items: 
Size: 13832 Color: 4
Size: 2437 Color: 3

Bin 154: 20 of cap free
Amount of items: 2
Items: 
Size: 9040 Color: 0
Size: 7228 Color: 3

Bin 155: 20 of cap free
Amount of items: 2
Items: 
Size: 12024 Color: 1
Size: 4244 Color: 4

Bin 156: 20 of cap free
Amount of items: 2
Items: 
Size: 12422 Color: 4
Size: 3846 Color: 3

Bin 157: 20 of cap free
Amount of items: 2
Items: 
Size: 13712 Color: 4
Size: 2556 Color: 3

Bin 158: 20 of cap free
Amount of items: 2
Items: 
Size: 13984 Color: 1
Size: 2284 Color: 3

Bin 159: 22 of cap free
Amount of items: 3
Items: 
Size: 11792 Color: 1
Size: 4282 Color: 4
Size: 192 Color: 3

Bin 160: 22 of cap free
Amount of items: 3
Items: 
Size: 13577 Color: 1
Size: 2457 Color: 3
Size: 232 Color: 1

Bin 161: 24 of cap free
Amount of items: 2
Items: 
Size: 10792 Color: 4
Size: 5472 Color: 3

Bin 162: 24 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 1
Size: 2628 Color: 3

Bin 163: 25 of cap free
Amount of items: 5
Items: 
Size: 8152 Color: 0
Size: 4584 Color: 1
Size: 2971 Color: 2
Size: 284 Color: 3
Size: 272 Color: 2

Bin 164: 28 of cap free
Amount of items: 2
Items: 
Size: 10196 Color: 4
Size: 6064 Color: 2

Bin 165: 30 of cap free
Amount of items: 2
Items: 
Size: 12242 Color: 1
Size: 4016 Color: 4

Bin 166: 33 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 0
Size: 2067 Color: 3

Bin 167: 37 of cap free
Amount of items: 2
Items: 
Size: 14010 Color: 3
Size: 2241 Color: 0

Bin 168: 38 of cap free
Amount of items: 2
Items: 
Size: 9254 Color: 3
Size: 6996 Color: 0

Bin 169: 39 of cap free
Amount of items: 2
Items: 
Size: 12725 Color: 0
Size: 3524 Color: 1

Bin 170: 39 of cap free
Amount of items: 2
Items: 
Size: 13313 Color: 0
Size: 2936 Color: 3

Bin 171: 42 of cap free
Amount of items: 4
Items: 
Size: 8484 Color: 4
Size: 4976 Color: 0
Size: 2442 Color: 4
Size: 344 Color: 2

Bin 172: 43 of cap free
Amount of items: 2
Items: 
Size: 11277 Color: 2
Size: 4968 Color: 1

Bin 173: 48 of cap free
Amount of items: 2
Items: 
Size: 10440 Color: 1
Size: 5800 Color: 4

Bin 174: 48 of cap free
Amount of items: 2
Items: 
Size: 14416 Color: 0
Size: 1824 Color: 4

Bin 175: 50 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 1
Size: 3440 Color: 0

Bin 176: 50 of cap free
Amount of items: 2
Items: 
Size: 14484 Color: 3
Size: 1754 Color: 1

Bin 177: 51 of cap free
Amount of items: 2
Items: 
Size: 11540 Color: 1
Size: 4697 Color: 3

Bin 178: 54 of cap free
Amount of items: 2
Items: 
Size: 13018 Color: 1
Size: 3216 Color: 3

Bin 179: 56 of cap free
Amount of items: 2
Items: 
Size: 11508 Color: 0
Size: 4724 Color: 2

Bin 180: 57 of cap free
Amount of items: 2
Items: 
Size: 13322 Color: 3
Size: 2909 Color: 4

Bin 181: 61 of cap free
Amount of items: 2
Items: 
Size: 11478 Color: 3
Size: 4749 Color: 4

Bin 182: 62 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 0
Size: 2726 Color: 3

Bin 183: 62 of cap free
Amount of items: 2
Items: 
Size: 13629 Color: 1
Size: 2597 Color: 0

Bin 184: 64 of cap free
Amount of items: 2
Items: 
Size: 13696 Color: 1
Size: 2528 Color: 4

Bin 185: 71 of cap free
Amount of items: 3
Items: 
Size: 10741 Color: 3
Size: 5300 Color: 2
Size: 176 Color: 4

Bin 186: 77 of cap free
Amount of items: 2
Items: 
Size: 13393 Color: 1
Size: 2818 Color: 4

Bin 187: 80 of cap free
Amount of items: 2
Items: 
Size: 9420 Color: 1
Size: 6788 Color: 2

Bin 188: 84 of cap free
Amount of items: 2
Items: 
Size: 12194 Color: 1
Size: 4010 Color: 0

Bin 189: 84 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 3
Size: 2308 Color: 1

Bin 190: 90 of cap free
Amount of items: 3
Items: 
Size: 10344 Color: 0
Size: 5182 Color: 4
Size: 672 Color: 2

Bin 191: 108 of cap free
Amount of items: 2
Items: 
Size: 13968 Color: 1
Size: 2212 Color: 3

Bin 192: 110 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 4
Size: 4450 Color: 0
Size: 3560 Color: 2

Bin 193: 129 of cap free
Amount of items: 2
Items: 
Size: 13192 Color: 1
Size: 2967 Color: 0

Bin 194: 139 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 2
Size: 6785 Color: 2
Size: 1184 Color: 0

Bin 195: 146 of cap free
Amount of items: 10
Items: 
Size: 8146 Color: 1
Size: 976 Color: 4
Size: 976 Color: 2
Size: 944 Color: 2
Size: 912 Color: 3
Size: 896 Color: 3
Size: 848 Color: 3
Size: 848 Color: 0
Size: 800 Color: 4
Size: 796 Color: 3

Bin 196: 154 of cap free
Amount of items: 3
Items: 
Size: 8176 Color: 2
Size: 5862 Color: 2
Size: 2096 Color: 0

Bin 197: 160 of cap free
Amount of items: 2
Items: 
Size: 9336 Color: 0
Size: 6792 Color: 3

Bin 198: 220 of cap free
Amount of items: 29
Items: 
Size: 800 Color: 1
Size: 792 Color: 4
Size: 760 Color: 0
Size: 704 Color: 4
Size: 704 Color: 3
Size: 704 Color: 1
Size: 704 Color: 0
Size: 644 Color: 3
Size: 624 Color: 3
Size: 624 Color: 0
Size: 608 Color: 3
Size: 608 Color: 2
Size: 580 Color: 1
Size: 544 Color: 1
Size: 528 Color: 3
Size: 528 Color: 2
Size: 520 Color: 1
Size: 480 Color: 0
Size: 480 Color: 0
Size: 440 Color: 0
Size: 416 Color: 4
Size: 416 Color: 2
Size: 416 Color: 2
Size: 412 Color: 4
Size: 408 Color: 3
Size: 408 Color: 3
Size: 408 Color: 1
Size: 408 Color: 0
Size: 400 Color: 1

Bin 199: 12896 of cap free
Amount of items: 11
Items: 
Size: 344 Color: 4
Size: 336 Color: 4
Size: 328 Color: 0
Size: 320 Color: 4
Size: 320 Color: 2
Size: 320 Color: 2
Size: 320 Color: 1
Size: 320 Color: 1
Size: 272 Color: 3
Size: 256 Color: 2
Size: 256 Color: 1

Total size: 3225024
Total free space: 16288

