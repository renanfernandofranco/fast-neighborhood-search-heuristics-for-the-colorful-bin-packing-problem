Capicity Bin: 1000001
Lower Bound: 45

Bins used: 46
Amount of Colors: 20

Bin 1: 56 of cap free
Amount of items: 2
Items: 
Size: 639541 Color: 9
Size: 360404 Color: 7

Bin 2: 74 of cap free
Amount of items: 3
Items: 
Size: 432485 Color: 15
Size: 391759 Color: 19
Size: 175683 Color: 16

Bin 3: 104 of cap free
Amount of items: 3
Items: 
Size: 686590 Color: 13
Size: 183447 Color: 1
Size: 129860 Color: 1

Bin 4: 347 of cap free
Amount of items: 3
Items: 
Size: 689939 Color: 7
Size: 199270 Color: 17
Size: 110445 Color: 3

Bin 5: 585 of cap free
Amount of items: 2
Items: 
Size: 555548 Color: 2
Size: 443868 Color: 15

Bin 6: 720 of cap free
Amount of items: 2
Items: 
Size: 616715 Color: 17
Size: 382566 Color: 15

Bin 7: 1007 of cap free
Amount of items: 2
Items: 
Size: 757226 Color: 11
Size: 241768 Color: 8

Bin 8: 1173 of cap free
Amount of items: 2
Items: 
Size: 719500 Color: 18
Size: 279328 Color: 5

Bin 9: 2132 of cap free
Amount of items: 2
Items: 
Size: 540046 Color: 1
Size: 457823 Color: 19

Bin 10: 2204 of cap free
Amount of items: 2
Items: 
Size: 722385 Color: 3
Size: 275412 Color: 12

Bin 11: 2389 of cap free
Amount of items: 3
Items: 
Size: 736385 Color: 9
Size: 157580 Color: 6
Size: 103647 Color: 0

Bin 12: 2418 of cap free
Amount of items: 3
Items: 
Size: 533481 Color: 7
Size: 347048 Color: 2
Size: 117054 Color: 14

Bin 13: 2562 of cap free
Amount of items: 3
Items: 
Size: 664292 Color: 10
Size: 210823 Color: 5
Size: 122324 Color: 5

Bin 14: 2719 of cap free
Amount of items: 2
Items: 
Size: 700700 Color: 8
Size: 296582 Color: 0

Bin 15: 2738 of cap free
Amount of items: 2
Items: 
Size: 792421 Color: 3
Size: 204842 Color: 1

Bin 16: 3216 of cap free
Amount of items: 2
Items: 
Size: 558386 Color: 10
Size: 438399 Color: 15

Bin 17: 3721 of cap free
Amount of items: 2
Items: 
Size: 634830 Color: 7
Size: 361450 Color: 17

Bin 18: 3779 of cap free
Amount of items: 3
Items: 
Size: 481047 Color: 19
Size: 338693 Color: 4
Size: 176482 Color: 0

Bin 19: 4144 of cap free
Amount of items: 2
Items: 
Size: 708376 Color: 4
Size: 287481 Color: 5

Bin 20: 4510 of cap free
Amount of items: 3
Items: 
Size: 663417 Color: 5
Size: 169635 Color: 6
Size: 162439 Color: 9

Bin 21: 4867 of cap free
Amount of items: 2
Items: 
Size: 603416 Color: 19
Size: 391718 Color: 2

Bin 22: 6731 of cap free
Amount of items: 2
Items: 
Size: 706124 Color: 13
Size: 287146 Color: 9

Bin 23: 7877 of cap free
Amount of items: 2
Items: 
Size: 673899 Color: 16
Size: 318225 Color: 15

Bin 24: 8724 of cap free
Amount of items: 2
Items: 
Size: 518515 Color: 6
Size: 472762 Color: 15

Bin 25: 10644 of cap free
Amount of items: 2
Items: 
Size: 729084 Color: 5
Size: 260273 Color: 12

Bin 26: 11693 of cap free
Amount of items: 2
Items: 
Size: 715596 Color: 1
Size: 272712 Color: 12

Bin 27: 12981 of cap free
Amount of items: 2
Items: 
Size: 728254 Color: 11
Size: 258766 Color: 0

Bin 28: 13036 of cap free
Amount of items: 2
Items: 
Size: 673457 Color: 17
Size: 313508 Color: 10

Bin 29: 14649 of cap free
Amount of items: 2
Items: 
Size: 493847 Color: 6
Size: 491505 Color: 1

Bin 30: 16148 of cap free
Amount of items: 2
Items: 
Size: 769881 Color: 14
Size: 213972 Color: 12

Bin 31: 18787 of cap free
Amount of items: 2
Items: 
Size: 748746 Color: 4
Size: 232468 Color: 13

Bin 32: 22864 of cap free
Amount of items: 2
Items: 
Size: 728813 Color: 13
Size: 248324 Color: 2

Bin 33: 26129 of cap free
Amount of items: 2
Items: 
Size: 532505 Color: 13
Size: 441367 Color: 4

Bin 34: 26942 of cap free
Amount of items: 2
Items: 
Size: 737154 Color: 19
Size: 235905 Color: 15

Bin 35: 31965 of cap free
Amount of items: 2
Items: 
Size: 720979 Color: 12
Size: 247057 Color: 17

Bin 36: 32447 of cap free
Amount of items: 2
Items: 
Size: 542878 Color: 12
Size: 424676 Color: 19

Bin 37: 32742 of cap free
Amount of items: 3
Items: 
Size: 479679 Color: 2
Size: 351865 Color: 13
Size: 135715 Color: 10

Bin 38: 33710 of cap free
Amount of items: 2
Items: 
Size: 532157 Color: 3
Size: 434134 Color: 11

Bin 39: 54031 of cap free
Amount of items: 3
Items: 
Size: 572798 Color: 13
Size: 193996 Color: 10
Size: 179176 Color: 4

Bin 40: 65122 of cap free
Amount of items: 2
Items: 
Size: 473688 Color: 4
Size: 461191 Color: 3

Bin 41: 65227 of cap free
Amount of items: 2
Items: 
Size: 696346 Color: 9
Size: 238428 Color: 11

Bin 42: 137509 of cap free
Amount of items: 2
Items: 
Size: 451536 Color: 0
Size: 410956 Color: 1

Bin 43: 161399 of cap free
Amount of items: 2
Items: 
Size: 425760 Color: 8
Size: 412842 Color: 6

Bin 44: 231073 of cap free
Amount of items: 2
Items: 
Size: 391736 Color: 7
Size: 377192 Color: 2

Bin 45: 254470 of cap free
Amount of items: 2
Items: 
Size: 395150 Color: 15
Size: 350381 Color: 18

Bin 46: 261385 of cap free
Amount of items: 2
Items: 
Size: 395759 Color: 13
Size: 342857 Color: 12

Total size: 44396296
Total free space: 1603750

