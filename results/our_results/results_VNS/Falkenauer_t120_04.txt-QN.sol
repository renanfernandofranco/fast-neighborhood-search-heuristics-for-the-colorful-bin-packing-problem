Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 117
Size: 258 Color: 17
Size: 251 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 97
Size: 314 Color: 64
Size: 260 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 108
Size: 270 Color: 37
Size: 265 Color: 26

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 86
Size: 328 Color: 71
Size: 288 Color: 51

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 91
Size: 308 Color: 62
Size: 289 Color: 53

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 104
Size: 298 Color: 58
Size: 250 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 110
Size: 266 Color: 29
Size: 261 Color: 20

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 93
Size: 301 Color: 59
Size: 289 Color: 54

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 105
Size: 285 Color: 49
Size: 255 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 119
Size: 251 Color: 4
Size: 250 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 102
Size: 289 Color: 52
Size: 265 Color: 27

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 109
Size: 279 Color: 46
Size: 252 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 115
Size: 266 Color: 30
Size: 250 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 88
Size: 350 Color: 78
Size: 254 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 77
Size: 333 Color: 72
Size: 317 Color: 66

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 92
Size: 324 Color: 69
Size: 267 Color: 32

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 103
Size: 279 Color: 45
Size: 274 Color: 41

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 100
Size: 286 Color: 50
Size: 280 Color: 47

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 107
Size: 269 Color: 35
Size: 267 Color: 31

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 82
Size: 359 Color: 80
Size: 273 Color: 40

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 89
Size: 335 Color: 74
Size: 269 Color: 36

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 101
Size: 296 Color: 55
Size: 268 Color: 34

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 98
Size: 298 Color: 56
Size: 272 Color: 38

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 85
Size: 350 Color: 76
Size: 268 Color: 33

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 96
Size: 298 Color: 57
Size: 278 Color: 44

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 114
Size: 263 Color: 24
Size: 253 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 111
Size: 261 Color: 21
Size: 259 Color: 18

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 113
Size: 266 Color: 28
Size: 251 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 116
Size: 258 Color: 16
Size: 254 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 118
Size: 252 Color: 7
Size: 251 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 84
Size: 370 Color: 83
Size: 257 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 99
Size: 312 Color: 63
Size: 256 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 90
Size: 334 Color: 73
Size: 265 Color: 25

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 94
Size: 303 Color: 60
Size: 283 Color: 48

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 79
Size: 322 Color: 68
Size: 321 Color: 67

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 87
Size: 337 Color: 75
Size: 272 Color: 39

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 95
Size: 306 Color: 61
Size: 275 Color: 42

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 112
Size: 263 Color: 23
Size: 256 Color: 13

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 81
Size: 325 Color: 70
Size: 315 Color: 65

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 106
Size: 276 Color: 43
Size: 262 Color: 22

Total size: 40000
Total free space: 0

