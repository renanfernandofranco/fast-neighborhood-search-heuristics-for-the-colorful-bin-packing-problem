Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 371148 Color: 3
Size: 324555 Color: 0
Size: 304298 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 489873 Color: 2
Size: 256146 Color: 2
Size: 253982 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 407394 Color: 3
Size: 318658 Color: 4
Size: 273949 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 365424 Color: 2
Size: 343084 Color: 4
Size: 291493 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 497458 Color: 1
Size: 252526 Color: 2
Size: 250017 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 379452 Color: 1
Size: 367253 Color: 3
Size: 253296 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 461306 Color: 4
Size: 269091 Color: 2
Size: 269604 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 352924 Color: 1
Size: 328260 Color: 4
Size: 318817 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 460195 Color: 1
Size: 275213 Color: 0
Size: 264593 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 464794 Color: 3
Size: 276521 Color: 0
Size: 258686 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 485537 Color: 2
Size: 263730 Color: 4
Size: 250734 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 399152 Color: 2
Size: 308686 Color: 4
Size: 292163 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 376925 Color: 1
Size: 320986 Color: 3
Size: 302090 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 369037 Color: 4
Size: 354902 Color: 0
Size: 276062 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 378715 Color: 0
Size: 350099 Color: 1
Size: 271187 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 434755 Color: 4
Size: 283708 Color: 1
Size: 281538 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 428260 Color: 1
Size: 307253 Color: 1
Size: 264488 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 452697 Color: 1
Size: 295243 Color: 4
Size: 252061 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 437348 Color: 0
Size: 306953 Color: 3
Size: 255700 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 391202 Color: 2
Size: 352203 Color: 0
Size: 256596 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 401121 Color: 1
Size: 312829 Color: 3
Size: 286051 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 470820 Color: 3
Size: 265925 Color: 1
Size: 263256 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 386206 Color: 2
Size: 324878 Color: 1
Size: 288917 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 448252 Color: 4
Size: 299817 Color: 0
Size: 251932 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 416432 Color: 2
Size: 292063 Color: 4
Size: 291506 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 451704 Color: 4
Size: 284468 Color: 3
Size: 263829 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 413403 Color: 4
Size: 327834 Color: 1
Size: 258764 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 378638 Color: 2
Size: 318093 Color: 3
Size: 303270 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 411179 Color: 4
Size: 328896 Color: 1
Size: 259926 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 409285 Color: 3
Size: 297440 Color: 1
Size: 293276 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 350801 Color: 4
Size: 331241 Color: 3
Size: 317959 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 387402 Color: 0
Size: 317253 Color: 0
Size: 295346 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 390955 Color: 4
Size: 307700 Color: 2
Size: 301346 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 416903 Color: 2
Size: 321088 Color: 4
Size: 262010 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 375925 Color: 2
Size: 338023 Color: 4
Size: 286053 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 429183 Color: 4
Size: 296000 Color: 4
Size: 274818 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 495975 Color: 3
Size: 252071 Color: 1
Size: 251955 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 376959 Color: 4
Size: 364030 Color: 3
Size: 259012 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 354402 Color: 0
Size: 322913 Color: 3
Size: 322686 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 450196 Color: 1
Size: 276771 Color: 1
Size: 273034 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 350149 Color: 1
Size: 342058 Color: 2
Size: 307794 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 345908 Color: 1
Size: 342893 Color: 0
Size: 311200 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 492015 Color: 0
Size: 255497 Color: 4
Size: 252489 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 403542 Color: 1
Size: 311341 Color: 0
Size: 285118 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 379517 Color: 2
Size: 368156 Color: 3
Size: 252328 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 427903 Color: 4
Size: 305600 Color: 1
Size: 266498 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 385521 Color: 2
Size: 340344 Color: 4
Size: 274136 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 349955 Color: 1
Size: 326361 Color: 3
Size: 323685 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 405966 Color: 4
Size: 314014 Color: 4
Size: 280021 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 422954 Color: 0
Size: 321355 Color: 1
Size: 255692 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 474937 Color: 4
Size: 266367 Color: 0
Size: 258697 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 499695 Color: 4
Size: 250170 Color: 0
Size: 250136 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 400125 Color: 0
Size: 329670 Color: 0
Size: 270206 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 413407 Color: 4
Size: 324570 Color: 1
Size: 262024 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 418127 Color: 0
Size: 295115 Color: 3
Size: 286759 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 477089 Color: 3
Size: 263658 Color: 0
Size: 259254 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 418558 Color: 4
Size: 292538 Color: 1
Size: 288905 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 458400 Color: 3
Size: 274665 Color: 3
Size: 266936 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 379011 Color: 1
Size: 333292 Color: 0
Size: 287698 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 431234 Color: 3
Size: 291794 Color: 2
Size: 276973 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 381362 Color: 4
Size: 353167 Color: 1
Size: 265472 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 435717 Color: 2
Size: 302195 Color: 4
Size: 262089 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 402878 Color: 3
Size: 329767 Color: 1
Size: 267356 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 411890 Color: 1
Size: 331153 Color: 2
Size: 256958 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 468848 Color: 4
Size: 271059 Color: 3
Size: 260094 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 443309 Color: 0
Size: 292284 Color: 1
Size: 264408 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 391154 Color: 4
Size: 342013 Color: 2
Size: 266834 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 361393 Color: 3
Size: 343634 Color: 4
Size: 294974 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 476521 Color: 0
Size: 269795 Color: 0
Size: 253685 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 381348 Color: 4
Size: 331765 Color: 2
Size: 286888 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 373581 Color: 0
Size: 360139 Color: 4
Size: 266281 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 460174 Color: 3
Size: 274784 Color: 1
Size: 265043 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 425502 Color: 3
Size: 317991 Color: 1
Size: 256508 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 380842 Color: 1
Size: 350559 Color: 1
Size: 268600 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 430893 Color: 3
Size: 291312 Color: 4
Size: 277796 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 347592 Color: 0
Size: 326350 Color: 4
Size: 326059 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 430482 Color: 2
Size: 294529 Color: 0
Size: 274990 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 398067 Color: 0
Size: 332684 Color: 0
Size: 269250 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 430741 Color: 0
Size: 304496 Color: 4
Size: 264764 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 496432 Color: 2
Size: 251991 Color: 4
Size: 251578 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 419488 Color: 1
Size: 318315 Color: 4
Size: 262198 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 364137 Color: 4
Size: 360304 Color: 1
Size: 275560 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 356247 Color: 0
Size: 356032 Color: 2
Size: 287722 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 459428 Color: 4
Size: 271444 Color: 3
Size: 269129 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 420264 Color: 4
Size: 318151 Color: 4
Size: 261586 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 459114 Color: 4
Size: 277428 Color: 2
Size: 263459 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 438142 Color: 3
Size: 281047 Color: 2
Size: 280812 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 449474 Color: 3
Size: 283197 Color: 1
Size: 267330 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 384648 Color: 4
Size: 311648 Color: 1
Size: 303705 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 469183 Color: 3
Size: 273312 Color: 2
Size: 257506 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 353865 Color: 1
Size: 335895 Color: 2
Size: 310241 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 479645 Color: 1
Size: 267906 Color: 0
Size: 252450 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 411611 Color: 3
Size: 309897 Color: 1
Size: 278493 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 410674 Color: 1
Size: 309080 Color: 2
Size: 280247 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 404646 Color: 1
Size: 308547 Color: 0
Size: 286808 Color: 2

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 425261 Color: 0
Size: 314728 Color: 1
Size: 260012 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 363934 Color: 3
Size: 352988 Color: 0
Size: 283079 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 396318 Color: 3
Size: 328826 Color: 4
Size: 274857 Color: 4

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 427332 Color: 3
Size: 291570 Color: 0
Size: 281099 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 429408 Color: 4
Size: 308068 Color: 2
Size: 262525 Color: 4

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 417913 Color: 2
Size: 312464 Color: 1
Size: 269624 Color: 4

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 403475 Color: 4
Size: 322300 Color: 3
Size: 274226 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 350212 Color: 4
Size: 345266 Color: 1
Size: 304523 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 455122 Color: 0
Size: 275595 Color: 1
Size: 269284 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 463349 Color: 4
Size: 282586 Color: 1
Size: 254066 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 433886 Color: 3
Size: 312393 Color: 1
Size: 253722 Color: 2

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 381617 Color: 2
Size: 334460 Color: 4
Size: 283924 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 379677 Color: 2
Size: 367329 Color: 3
Size: 252995 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 420635 Color: 1
Size: 301510 Color: 2
Size: 277856 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 448706 Color: 2
Size: 276379 Color: 1
Size: 274916 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 389707 Color: 0
Size: 331601 Color: 4
Size: 278693 Color: 3

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 394737 Color: 3
Size: 314121 Color: 2
Size: 291143 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 379597 Color: 0
Size: 340870 Color: 0
Size: 279534 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 377308 Color: 4
Size: 348063 Color: 1
Size: 274630 Color: 3

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 356677 Color: 3
Size: 331055 Color: 4
Size: 312269 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 384164 Color: 3
Size: 362080 Color: 1
Size: 253757 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 476236 Color: 1
Size: 263010 Color: 2
Size: 260755 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 426970 Color: 4
Size: 308854 Color: 0
Size: 264177 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 480018 Color: 4
Size: 260543 Color: 0
Size: 259440 Color: 3

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 431057 Color: 0
Size: 299388 Color: 4
Size: 269556 Color: 3

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 359247 Color: 0
Size: 330731 Color: 1
Size: 310023 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 427429 Color: 2
Size: 317545 Color: 1
Size: 255027 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 476822 Color: 2
Size: 270111 Color: 1
Size: 253068 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 415655 Color: 2
Size: 294430 Color: 0
Size: 289916 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 391220 Color: 2
Size: 334495 Color: 0
Size: 274286 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 423418 Color: 3
Size: 300101 Color: 3
Size: 276482 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 467114 Color: 3
Size: 279813 Color: 4
Size: 253074 Color: 3

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 454181 Color: 2
Size: 286270 Color: 4
Size: 259550 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 412049 Color: 2
Size: 303045 Color: 2
Size: 284907 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 343801 Color: 2
Size: 340732 Color: 4
Size: 315468 Color: 2

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 393289 Color: 0
Size: 330721 Color: 0
Size: 275991 Color: 3

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 378298 Color: 3
Size: 335321 Color: 3
Size: 286382 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 404532 Color: 4
Size: 331180 Color: 2
Size: 264289 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 360583 Color: 4
Size: 321666 Color: 1
Size: 317752 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 419802 Color: 4
Size: 312043 Color: 1
Size: 268156 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 493097 Color: 3
Size: 255514 Color: 2
Size: 251390 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 467899 Color: 2
Size: 269176 Color: 3
Size: 262926 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 425099 Color: 4
Size: 315292 Color: 0
Size: 259610 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 411502 Color: 0
Size: 304893 Color: 0
Size: 283606 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 387408 Color: 1
Size: 361742 Color: 0
Size: 250851 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 397266 Color: 4
Size: 321074 Color: 3
Size: 281661 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 395403 Color: 0
Size: 332433 Color: 4
Size: 272165 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 469426 Color: 4
Size: 269911 Color: 1
Size: 260664 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 401253 Color: 4
Size: 332767 Color: 3
Size: 265981 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 449333 Color: 1
Size: 286713 Color: 2
Size: 263955 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 400780 Color: 0
Size: 322047 Color: 2
Size: 277174 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 440904 Color: 1
Size: 299146 Color: 4
Size: 259951 Color: 4

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 391097 Color: 3
Size: 352230 Color: 0
Size: 256674 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 373214 Color: 3
Size: 329516 Color: 3
Size: 297271 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 429808 Color: 1
Size: 299440 Color: 0
Size: 270753 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 375987 Color: 2
Size: 357820 Color: 3
Size: 266194 Color: 4

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 389365 Color: 0
Size: 314113 Color: 4
Size: 296523 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 487796 Color: 0
Size: 259631 Color: 4
Size: 252574 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 419089 Color: 2
Size: 328422 Color: 4
Size: 252490 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 367175 Color: 1
Size: 350475 Color: 2
Size: 282351 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 382267 Color: 0
Size: 346169 Color: 2
Size: 271565 Color: 2

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 444110 Color: 3
Size: 289844 Color: 2
Size: 266047 Color: 4

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 406692 Color: 4
Size: 337283 Color: 4
Size: 256026 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 462527 Color: 0
Size: 285385 Color: 1
Size: 252089 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 468480 Color: 1
Size: 266279 Color: 3
Size: 265242 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 490944 Color: 2
Size: 256819 Color: 1
Size: 252238 Color: 3

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 348835 Color: 0
Size: 327919 Color: 2
Size: 323247 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 353311 Color: 4
Size: 333904 Color: 3
Size: 312786 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 366545 Color: 4
Size: 334856 Color: 1
Size: 298600 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 398783 Color: 0
Size: 350238 Color: 3
Size: 250980 Color: 4

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 421754 Color: 3
Size: 319535 Color: 2
Size: 258712 Color: 3

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 421183 Color: 0
Size: 301626 Color: 0
Size: 277192 Color: 4

Total size: 167000167
Total free space: 0

