Capicity Bin: 16432
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 36
Items: 
Size: 580 Color: 1
Size: 576 Color: 1
Size: 568 Color: 0
Size: 558 Color: 0
Size: 552 Color: 0
Size: 544 Color: 0
Size: 528 Color: 1
Size: 516 Color: 1
Size: 500 Color: 0
Size: 496 Color: 1
Size: 488 Color: 1
Size: 484 Color: 1
Size: 480 Color: 1
Size: 480 Color: 0
Size: 480 Color: 0
Size: 460 Color: 1
Size: 456 Color: 1
Size: 452 Color: 0
Size: 448 Color: 1
Size: 448 Color: 1
Size: 448 Color: 0
Size: 432 Color: 1
Size: 432 Color: 1
Size: 420 Color: 0
Size: 416 Color: 0
Size: 416 Color: 0
Size: 414 Color: 0
Size: 396 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 378 Color: 1
Size: 376 Color: 1
Size: 370 Color: 1
Size: 368 Color: 1
Size: 360 Color: 0
Size: 344 Color: 0

Bin 2: 0 of cap free
Amount of items: 14
Items: 
Size: 8128 Color: 1
Size: 720 Color: 0
Size: 704 Color: 1
Size: 664 Color: 1
Size: 658 Color: 1
Size: 656 Color: 0
Size: 656 Color: 0
Size: 648 Color: 1
Size: 640 Color: 1
Size: 640 Color: 0
Size: 604 Color: 0
Size: 604 Color: 0
Size: 562 Color: 0
Size: 548 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8236 Color: 0
Size: 6836 Color: 1
Size: 1360 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8744 Color: 1
Size: 7280 Color: 1
Size: 408 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9141 Color: 0
Size: 6847 Color: 1
Size: 444 Color: 0

Bin 6: 0 of cap free
Amount of items: 6
Items: 
Size: 9306 Color: 1
Size: 1656 Color: 0
Size: 1652 Color: 0
Size: 1650 Color: 0
Size: 1368 Color: 1
Size: 800 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9224 Color: 0
Size: 6008 Color: 0
Size: 1200 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 1
Size: 6808 Color: 1
Size: 288 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11000 Color: 0
Size: 4920 Color: 1
Size: 512 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11622 Color: 0
Size: 4542 Color: 0
Size: 268 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11610 Color: 1
Size: 4550 Color: 0
Size: 272 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 0
Size: 3780 Color: 0
Size: 752 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11969 Color: 1
Size: 3721 Color: 1
Size: 742 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11940 Color: 0
Size: 3748 Color: 0
Size: 744 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11972 Color: 1
Size: 4022 Color: 0
Size: 438 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 1
Size: 3320 Color: 0
Size: 608 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12532 Color: 0
Size: 3332 Color: 0
Size: 568 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 0
Size: 3244 Color: 1
Size: 640 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 1
Size: 3284 Color: 0
Size: 232 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12968 Color: 0
Size: 2016 Color: 1
Size: 1448 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 0
Size: 2084 Color: 1
Size: 1368 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 1
Size: 2778 Color: 0
Size: 430 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 1
Size: 2524 Color: 0
Size: 504 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13422 Color: 1
Size: 2734 Color: 0
Size: 276 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13644 Color: 1
Size: 2512 Color: 1
Size: 276 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13642 Color: 0
Size: 2044 Color: 1
Size: 746 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13861 Color: 0
Size: 2075 Color: 1
Size: 496 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 1
Size: 1468 Color: 1
Size: 976 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14073 Color: 1
Size: 2043 Color: 1
Size: 316 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 0
Size: 1992 Color: 1
Size: 390 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14263 Color: 1
Size: 1761 Color: 0
Size: 408 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14264 Color: 0
Size: 1524 Color: 1
Size: 644 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14436 Color: 1
Size: 1036 Color: 1
Size: 960 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 0
Size: 1264 Color: 0
Size: 716 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14449 Color: 1
Size: 1607 Color: 0
Size: 376 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14454 Color: 0
Size: 1070 Color: 0
Size: 908 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14548 Color: 1
Size: 1484 Color: 1
Size: 400 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 1
Size: 1196 Color: 0
Size: 668 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14582 Color: 1
Size: 1550 Color: 0
Size: 300 Color: 1

Bin 40: 0 of cap free
Amount of items: 4
Items: 
Size: 14612 Color: 0
Size: 1614 Color: 1
Size: 164 Color: 0
Size: 42 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 1
Size: 1184 Color: 1
Size: 648 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 1
Size: 1442 Color: 0
Size: 352 Color: 1

Bin 43: 1 of cap free
Amount of items: 11
Items: 
Size: 8219 Color: 1
Size: 1032 Color: 0
Size: 984 Color: 1
Size: 984 Color: 1
Size: 976 Color: 1
Size: 908 Color: 0
Size: 908 Color: 0
Size: 904 Color: 1
Size: 864 Color: 0
Size: 344 Color: 0
Size: 308 Color: 1

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 8218 Color: 0
Size: 6845 Color: 1
Size: 1368 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 9253 Color: 1
Size: 6842 Color: 1
Size: 336 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 10974 Color: 1
Size: 4873 Color: 1
Size: 584 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 11324 Color: 1
Size: 4757 Color: 1
Size: 350 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 11857 Color: 1
Size: 4056 Color: 0
Size: 518 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 12493 Color: 1
Size: 3218 Color: 1
Size: 720 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 12938 Color: 1
Size: 2501 Color: 1
Size: 992 Color: 0

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 13154 Color: 1
Size: 2813 Color: 1
Size: 464 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 13910 Color: 1
Size: 2197 Color: 0
Size: 324 Color: 0

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 14025 Color: 0
Size: 1382 Color: 1
Size: 1024 Color: 0

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 14221 Color: 1
Size: 1986 Color: 1
Size: 224 Color: 0

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 14385 Color: 0
Size: 1542 Color: 1
Size: 504 Color: 0

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 14663 Color: 1
Size: 1560 Color: 0
Size: 208 Color: 1

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 10005 Color: 1
Size: 5997 Color: 0
Size: 428 Color: 1

Bin 58: 2 of cap free
Amount of items: 5
Items: 
Size: 10218 Color: 0
Size: 3222 Color: 1
Size: 1498 Color: 1
Size: 932 Color: 0
Size: 560 Color: 1

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 10210 Color: 1
Size: 5932 Color: 0
Size: 288 Color: 1

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 11292 Color: 1
Size: 4558 Color: 0
Size: 580 Color: 0

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 12566 Color: 1
Size: 3528 Color: 1
Size: 336 Color: 0

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 13826 Color: 0
Size: 2604 Color: 1

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 13845 Color: 1
Size: 2041 Color: 1
Size: 544 Color: 0

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 14762 Color: 0
Size: 1668 Color: 1

Bin 65: 3 of cap free
Amount of items: 12
Items: 
Size: 8217 Color: 1
Size: 864 Color: 1
Size: 856 Color: 0
Size: 850 Color: 0
Size: 848 Color: 0
Size: 848 Color: 0
Size: 800 Color: 1
Size: 800 Color: 1
Size: 784 Color: 1
Size: 762 Color: 0
Size: 496 Color: 1
Size: 304 Color: 0

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 9237 Color: 1
Size: 6840 Color: 1
Size: 352 Color: 0

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 10725 Color: 1
Size: 4888 Color: 1
Size: 816 Color: 0

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 10821 Color: 0
Size: 5144 Color: 1
Size: 464 Color: 0

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 10837 Color: 1
Size: 5012 Color: 1
Size: 580 Color: 0

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 12419 Color: 0
Size: 4010 Color: 1

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 14557 Color: 0
Size: 1572 Color: 1
Size: 300 Color: 0

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 10117 Color: 0
Size: 5983 Color: 0
Size: 328 Color: 1

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 13918 Color: 1
Size: 2510 Color: 0

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 1
Size: 2488 Color: 0

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 14340 Color: 1
Size: 2088 Color: 0

Bin 76: 4 of cap free
Amount of items: 3
Items: 
Size: 14574 Color: 0
Size: 1598 Color: 1
Size: 256 Color: 0

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 14630 Color: 1
Size: 1798 Color: 0

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 14786 Color: 0
Size: 1642 Color: 1

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 14774 Color: 0
Size: 1653 Color: 1

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 10420 Color: 0
Size: 5182 Color: 1
Size: 824 Color: 0

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 10484 Color: 0
Size: 5942 Color: 1

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 12930 Color: 0
Size: 2005 Color: 1
Size: 1491 Color: 0

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 13503 Color: 0
Size: 2922 Color: 1

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 1
Size: 1843 Color: 0
Size: 192 Color: 1

Bin 85: 8 of cap free
Amount of items: 3
Items: 
Size: 11953 Color: 1
Size: 3733 Color: 1
Size: 738 Color: 0

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 14140 Color: 1
Size: 2284 Color: 0

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 14267 Color: 1
Size: 2157 Color: 0

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 14654 Color: 1
Size: 1770 Color: 0

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 14676 Color: 0
Size: 1748 Color: 1

Bin 90: 9 of cap free
Amount of items: 3
Items: 
Size: 13012 Color: 1
Size: 2037 Color: 1
Size: 1374 Color: 0

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 0
Size: 2767 Color: 1

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 13669 Color: 0
Size: 2754 Color: 1

Bin 93: 9 of cap free
Amount of items: 2
Items: 
Size: 14321 Color: 1
Size: 2102 Color: 0

Bin 94: 9 of cap free
Amount of items: 3
Items: 
Size: 14622 Color: 0
Size: 1689 Color: 1
Size: 112 Color: 0

Bin 95: 9 of cap free
Amount of items: 2
Items: 
Size: 14702 Color: 0
Size: 1721 Color: 1

Bin 96: 10 of cap free
Amount of items: 2
Items: 
Size: 11236 Color: 1
Size: 5186 Color: 0

Bin 97: 10 of cap free
Amount of items: 3
Items: 
Size: 12477 Color: 0
Size: 2801 Color: 1
Size: 1144 Color: 0

Bin 98: 10 of cap free
Amount of items: 2
Items: 
Size: 14153 Color: 0
Size: 2269 Color: 1

Bin 99: 10 of cap free
Amount of items: 2
Items: 
Size: 14500 Color: 0
Size: 1922 Color: 1

Bin 100: 11 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 1
Size: 5277 Color: 1
Size: 736 Color: 0

Bin 101: 11 of cap free
Amount of items: 2
Items: 
Size: 13138 Color: 0
Size: 3283 Color: 1

Bin 102: 11 of cap free
Amount of items: 2
Items: 
Size: 13333 Color: 0
Size: 3088 Color: 1

Bin 103: 11 of cap free
Amount of items: 2
Items: 
Size: 14278 Color: 1
Size: 2143 Color: 0

Bin 104: 12 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 0
Size: 6040 Color: 0
Size: 1056 Color: 1

Bin 105: 12 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 0
Size: 2746 Color: 0
Size: 712 Color: 1

Bin 106: 12 of cap free
Amount of items: 3
Items: 
Size: 12946 Color: 1
Size: 2418 Color: 1
Size: 1056 Color: 0

Bin 107: 12 of cap free
Amount of items: 2
Items: 
Size: 14170 Color: 0
Size: 2250 Color: 1

Bin 108: 12 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 0
Size: 2232 Color: 1

Bin 109: 13 of cap free
Amount of items: 2
Items: 
Size: 14466 Color: 0
Size: 1953 Color: 1

Bin 110: 13 of cap free
Amount of items: 2
Items: 
Size: 14518 Color: 1
Size: 1901 Color: 0

Bin 111: 14 of cap free
Amount of items: 3
Items: 
Size: 10240 Color: 0
Size: 5938 Color: 0
Size: 240 Color: 1

Bin 112: 14 of cap free
Amount of items: 3
Items: 
Size: 10516 Color: 0
Size: 4332 Color: 1
Size: 1570 Color: 0

Bin 113: 14 of cap free
Amount of items: 3
Items: 
Size: 12500 Color: 0
Size: 3582 Color: 0
Size: 336 Color: 1

Bin 114: 14 of cap free
Amount of items: 2
Items: 
Size: 13534 Color: 0
Size: 2884 Color: 1

Bin 115: 15 of cap free
Amount of items: 2
Items: 
Size: 14089 Color: 0
Size: 2328 Color: 1

Bin 116: 16 of cap free
Amount of items: 2
Items: 
Size: 12456 Color: 1
Size: 3960 Color: 0

Bin 117: 17 of cap free
Amount of items: 7
Items: 
Size: 8232 Color: 1
Size: 1528 Color: 0
Size: 1511 Color: 0
Size: 1510 Color: 0
Size: 1368 Color: 0
Size: 1214 Color: 1
Size: 1052 Color: 1

Bin 118: 17 of cap free
Amount of items: 2
Items: 
Size: 12436 Color: 0
Size: 3979 Color: 1

Bin 119: 17 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 1
Size: 2303 Color: 0
Size: 184 Color: 1

Bin 120: 18 of cap free
Amount of items: 2
Items: 
Size: 12130 Color: 0
Size: 4284 Color: 1

Bin 121: 18 of cap free
Amount of items: 2
Items: 
Size: 13983 Color: 1
Size: 2431 Color: 0

Bin 122: 20 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 1
Size: 6408 Color: 1
Size: 688 Color: 0

Bin 123: 20 of cap free
Amount of items: 4
Items: 
Size: 14621 Color: 0
Size: 1623 Color: 1
Size: 126 Color: 0
Size: 42 Color: 1

Bin 124: 20 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 1
Size: 1688 Color: 0
Size: 28 Color: 0

Bin 125: 22 of cap free
Amount of items: 9
Items: 
Size: 8220 Color: 1
Size: 1184 Color: 0
Size: 1136 Color: 0
Size: 1054 Color: 0
Size: 1040 Color: 0
Size: 1000 Color: 1
Size: 992 Color: 1
Size: 950 Color: 0
Size: 834 Color: 1

Bin 126: 22 of cap free
Amount of items: 2
Items: 
Size: 12803 Color: 1
Size: 3607 Color: 0

Bin 127: 23 of cap free
Amount of items: 3
Items: 
Size: 14407 Color: 1
Size: 1890 Color: 0
Size: 112 Color: 1

Bin 128: 23 of cap free
Amount of items: 3
Items: 
Size: 14643 Color: 0
Size: 1702 Color: 1
Size: 64 Color: 0

Bin 129: 24 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 1
Size: 2592 Color: 0

Bin 130: 24 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 1
Size: 2098 Color: 0

Bin 131: 27 of cap free
Amount of items: 2
Items: 
Size: 13517 Color: 1
Size: 2888 Color: 0

Bin 132: 28 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 1
Size: 6844 Color: 1
Size: 1280 Color: 0

Bin 133: 28 of cap free
Amount of items: 2
Items: 
Size: 14456 Color: 0
Size: 1948 Color: 1

Bin 134: 30 of cap free
Amount of items: 4
Items: 
Size: 10264 Color: 0
Size: 3724 Color: 1
Size: 2174 Color: 0
Size: 240 Color: 1

Bin 135: 30 of cap free
Amount of items: 2
Items: 
Size: 13057 Color: 1
Size: 3345 Color: 0

Bin 136: 30 of cap free
Amount of items: 2
Items: 
Size: 14076 Color: 0
Size: 2326 Color: 1

Bin 137: 31 of cap free
Amount of items: 6
Items: 
Size: 9310 Color: 1
Size: 1816 Color: 0
Size: 1805 Color: 0
Size: 1684 Color: 0
Size: 1394 Color: 1
Size: 392 Color: 1

Bin 138: 31 of cap free
Amount of items: 2
Items: 
Size: 11477 Color: 0
Size: 4924 Color: 1

Bin 139: 31 of cap free
Amount of items: 2
Items: 
Size: 13113 Color: 1
Size: 3288 Color: 0

Bin 140: 31 of cap free
Amount of items: 2
Items: 
Size: 14660 Color: 0
Size: 1741 Color: 1

Bin 141: 32 of cap free
Amount of items: 2
Items: 
Size: 11576 Color: 0
Size: 4824 Color: 1

Bin 142: 33 of cap free
Amount of items: 2
Items: 
Size: 13372 Color: 1
Size: 3027 Color: 0

Bin 143: 34 of cap free
Amount of items: 2
Items: 
Size: 12138 Color: 1
Size: 4260 Color: 0

Bin 144: 35 of cap free
Amount of items: 2
Items: 
Size: 13317 Color: 1
Size: 3080 Color: 0

Bin 145: 36 of cap free
Amount of items: 8
Items: 
Size: 8222 Color: 1
Size: 1502 Color: 0
Size: 1368 Color: 0
Size: 1360 Color: 0
Size: 1344 Color: 0
Size: 1184 Color: 1
Size: 1024 Color: 1
Size: 392 Color: 1

Bin 146: 36 of cap free
Amount of items: 2
Items: 
Size: 13464 Color: 0
Size: 2932 Color: 1

Bin 147: 38 of cap free
Amount of items: 2
Items: 
Size: 9548 Color: 1
Size: 6846 Color: 0

Bin 148: 38 of cap free
Amount of items: 2
Items: 
Size: 14412 Color: 1
Size: 1982 Color: 0

Bin 149: 38 of cap free
Amount of items: 2
Items: 
Size: 14498 Color: 1
Size: 1896 Color: 0

Bin 150: 39 of cap free
Amount of items: 2
Items: 
Size: 12216 Color: 1
Size: 4177 Color: 0

Bin 151: 39 of cap free
Amount of items: 2
Items: 
Size: 13073 Color: 0
Size: 3320 Color: 1

Bin 152: 39 of cap free
Amount of items: 2
Items: 
Size: 13368 Color: 1
Size: 3025 Color: 0

Bin 153: 40 of cap free
Amount of items: 2
Items: 
Size: 11224 Color: 1
Size: 5168 Color: 0

Bin 154: 40 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 0
Size: 2556 Color: 1

Bin 155: 41 of cap free
Amount of items: 2
Items: 
Size: 13711 Color: 1
Size: 2680 Color: 0

Bin 156: 45 of cap free
Amount of items: 2
Items: 
Size: 12574 Color: 0
Size: 3813 Color: 1

Bin 157: 45 of cap free
Amount of items: 2
Items: 
Size: 12801 Color: 1
Size: 3586 Color: 0

Bin 158: 47 of cap free
Amount of items: 2
Items: 
Size: 11421 Color: 0
Size: 4964 Color: 1

Bin 159: 48 of cap free
Amount of items: 2
Items: 
Size: 12856 Color: 1
Size: 3528 Color: 0

Bin 160: 48 of cap free
Amount of items: 2
Items: 
Size: 13943 Color: 0
Size: 2441 Color: 1

Bin 161: 48 of cap free
Amount of items: 2
Items: 
Size: 14168 Color: 1
Size: 2216 Color: 0

Bin 162: 50 of cap free
Amount of items: 2
Items: 
Size: 14058 Color: 0
Size: 2324 Color: 1

Bin 163: 51 of cap free
Amount of items: 2
Items: 
Size: 13784 Color: 0
Size: 2597 Color: 1

Bin 164: 52 of cap free
Amount of items: 2
Items: 
Size: 13797 Color: 1
Size: 2583 Color: 0

Bin 165: 56 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 1
Size: 4530 Color: 0
Size: 1198 Color: 0

Bin 166: 59 of cap free
Amount of items: 2
Items: 
Size: 14056 Color: 1
Size: 2317 Color: 0

Bin 167: 60 of cap free
Amount of items: 2
Items: 
Size: 14408 Color: 1
Size: 1964 Color: 0

Bin 168: 61 of cap free
Amount of items: 2
Items: 
Size: 14485 Color: 0
Size: 1886 Color: 1

Bin 169: 63 of cap free
Amount of items: 2
Items: 
Size: 13072 Color: 0
Size: 3297 Color: 1

Bin 170: 64 of cap free
Amount of items: 2
Items: 
Size: 11336 Color: 1
Size: 5032 Color: 0

Bin 171: 71 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 0
Size: 4257 Color: 1

Bin 172: 77 of cap free
Amount of items: 2
Items: 
Size: 10998 Color: 1
Size: 5357 Color: 0

Bin 173: 78 of cap free
Amount of items: 2
Items: 
Size: 13440 Color: 0
Size: 2914 Color: 1

Bin 174: 79 of cap free
Amount of items: 2
Items: 
Size: 12009 Color: 1
Size: 4344 Color: 0

Bin 175: 79 of cap free
Amount of items: 2
Items: 
Size: 12105 Color: 1
Size: 4248 Color: 0

Bin 176: 80 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 0
Size: 3608 Color: 1

Bin 177: 81 of cap free
Amount of items: 2
Items: 
Size: 11688 Color: 0
Size: 4663 Color: 1

Bin 178: 88 of cap free
Amount of items: 2
Items: 
Size: 13438 Color: 0
Size: 2906 Color: 1

Bin 179: 93 of cap free
Amount of items: 2
Items: 
Size: 13487 Color: 1
Size: 2852 Color: 0

Bin 180: 101 of cap free
Amount of items: 2
Items: 
Size: 12200 Color: 0
Size: 4131 Color: 1

Bin 181: 102 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 1
Size: 3014 Color: 0

Bin 182: 106 of cap free
Amount of items: 2
Items: 
Size: 14166 Color: 0
Size: 2160 Color: 1

Bin 183: 109 of cap free
Amount of items: 3
Items: 
Size: 14605 Color: 0
Size: 1612 Color: 1
Size: 106 Color: 0

Bin 184: 152 of cap free
Amount of items: 2
Items: 
Size: 13028 Color: 0
Size: 3252 Color: 1

Bin 185: 156 of cap free
Amount of items: 2
Items: 
Size: 10536 Color: 0
Size: 5740 Color: 1

Bin 186: 158 of cap free
Amount of items: 2
Items: 
Size: 14398 Color: 1
Size: 1876 Color: 0

Bin 187: 164 of cap free
Amount of items: 2
Items: 
Size: 13700 Color: 1
Size: 2568 Color: 0

Bin 188: 166 of cap free
Amount of items: 2
Items: 
Size: 13734 Color: 0
Size: 2532 Color: 1

Bin 189: 168 of cap free
Amount of items: 2
Items: 
Size: 13420 Color: 0
Size: 2844 Color: 1

Bin 190: 175 of cap free
Amount of items: 2
Items: 
Size: 11325 Color: 1
Size: 4932 Color: 0

Bin 191: 178 of cap free
Amount of items: 2
Items: 
Size: 10966 Color: 0
Size: 5288 Color: 1

Bin 192: 181 of cap free
Amount of items: 2
Items: 
Size: 13470 Color: 1
Size: 2781 Color: 0

Bin 193: 187 of cap free
Amount of items: 2
Items: 
Size: 10982 Color: 1
Size: 5263 Color: 0

Bin 194: 208 of cap free
Amount of items: 2
Items: 
Size: 9612 Color: 1
Size: 6612 Color: 0

Bin 195: 216 of cap free
Amount of items: 2
Items: 
Size: 10532 Color: 0
Size: 5684 Color: 1

Bin 196: 217 of cap free
Amount of items: 2
Items: 
Size: 13699 Color: 0
Size: 2516 Color: 1

Bin 197: 251 of cap free
Amount of items: 2
Items: 
Size: 10104 Color: 1
Size: 6077 Color: 0

Bin 198: 267 of cap free
Amount of items: 2
Items: 
Size: 10101 Color: 1
Size: 6064 Color: 0

Bin 199: 10040 of cap free
Amount of items: 20
Items: 
Size: 368 Color: 1
Size: 368 Color: 1
Size: 356 Color: 1
Size: 340 Color: 0
Size: 330 Color: 0
Size: 328 Color: 1
Size: 328 Color: 0
Size: 324 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 312 Color: 0
Size: 308 Color: 0
Size: 304 Color: 0
Size: 300 Color: 1
Size: 298 Color: 1
Size: 296 Color: 0
Size: 296 Color: 0
Size: 288 Color: 1
Size: 288 Color: 1

Total size: 3253536
Total free space: 16432

