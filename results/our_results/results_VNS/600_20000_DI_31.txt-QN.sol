Capicity Bin: 16608
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 20
Items: 
Size: 1056 Color: 170
Size: 1040 Color: 169
Size: 1040 Color: 168
Size: 1040 Color: 167
Size: 986 Color: 163
Size: 976 Color: 162
Size: 960 Color: 161
Size: 956 Color: 160
Size: 920 Color: 159
Size: 912 Color: 156
Size: 908 Color: 155
Size: 860 Color: 150
Size: 688 Color: 130
Size: 688 Color: 129
Size: 624 Color: 123
Size: 614 Color: 120
Size: 604 Color: 119
Size: 592 Color: 118
Size: 572 Color: 116
Size: 572 Color: 115

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 8312 Color: 409
Size: 2405 Color: 282
Size: 2276 Color: 274
Size: 2071 Color: 253
Size: 560 Color: 113
Size: 496 Color: 99
Size: 488 Color: 98

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 9470 Color: 426
Size: 5854 Color: 372
Size: 448 Color: 85
Size: 420 Color: 77
Size: 416 Color: 74

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10260 Color: 441
Size: 6014 Color: 382
Size: 334 Color: 45

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12356 Color: 476
Size: 3548 Color: 330
Size: 704 Color: 134

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 485
Size: 3148 Color: 320
Size: 624 Color: 124

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12846 Color: 486
Size: 3138 Color: 318
Size: 624 Color: 122

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 491
Size: 3144 Color: 319
Size: 464 Color: 90

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13260 Color: 499
Size: 2260 Color: 272
Size: 1088 Color: 174

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 501
Size: 3124 Color: 317
Size: 212 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 502
Size: 2822 Color: 305
Size: 510 Color: 100

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 503
Size: 1932 Color: 243
Size: 1380 Color: 197

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13321 Color: 504
Size: 2983 Color: 311
Size: 304 Color: 35

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 505
Size: 2988 Color: 312
Size: 296 Color: 30

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13329 Color: 506
Size: 2733 Color: 298
Size: 546 Color: 109

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13373 Color: 508
Size: 1737 Color: 232
Size: 1498 Color: 211

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 516
Size: 2056 Color: 251
Size: 912 Color: 157

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 518
Size: 2228 Color: 268
Size: 720 Color: 136

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13694 Color: 519
Size: 2194 Color: 265
Size: 720 Color: 137

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 527
Size: 2090 Color: 258
Size: 640 Color: 126

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 531
Size: 2404 Color: 281
Size: 264 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13942 Color: 532
Size: 1642 Color: 220
Size: 1024 Color: 166

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 533
Size: 1800 Color: 235
Size: 864 Color: 151

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 537
Size: 1312 Color: 192
Size: 1240 Color: 189

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 538
Size: 1912 Color: 242
Size: 628 Color: 125

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14091 Color: 540
Size: 2099 Color: 260
Size: 418 Color: 75

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14099 Color: 541
Size: 2091 Color: 259
Size: 418 Color: 76

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 543
Size: 2126 Color: 262
Size: 368 Color: 57

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 544
Size: 1821 Color: 237
Size: 672 Color: 127

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14119 Color: 545
Size: 2075 Color: 255
Size: 414 Color: 70

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14122 Color: 546
Size: 1296 Color: 191
Size: 1190 Color: 186

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14123 Color: 547
Size: 1621 Color: 219
Size: 864 Color: 152

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14127 Color: 548
Size: 2423 Color: 284
Size: 58 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14152 Color: 550
Size: 1976 Color: 245
Size: 480 Color: 96

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14228 Color: 554
Size: 1576 Color: 217
Size: 804 Color: 144

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14290 Color: 556
Size: 1182 Color: 178
Size: 1136 Color: 176

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 557
Size: 2244 Color: 270
Size: 72 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 566
Size: 1742 Color: 233
Size: 414 Color: 72

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14556 Color: 571
Size: 1644 Color: 221
Size: 408 Color: 67

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14596 Color: 574
Size: 1672 Color: 224
Size: 340 Color: 48

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 577
Size: 1520 Color: 212
Size: 450 Color: 86

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 578
Size: 1684 Color: 226
Size: 280 Color: 21

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14714 Color: 582
Size: 1190 Color: 185
Size: 704 Color: 135

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14727 Color: 583
Size: 1649 Color: 222
Size: 232 Color: 12

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 584
Size: 1320 Color: 193
Size: 560 Color: 112

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14740 Color: 585
Size: 1188 Color: 183
Size: 680 Color: 128

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14765 Color: 587
Size: 1651 Color: 223
Size: 192 Color: 9

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14769 Color: 588
Size: 1531 Color: 213
Size: 308 Color: 36

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14812 Color: 591
Size: 1532 Color: 214
Size: 264 Color: 16

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14814 Color: 592
Size: 904 Color: 154
Size: 890 Color: 153

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14817 Color: 593
Size: 1493 Color: 209
Size: 298 Color: 31

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14824 Color: 594
Size: 1232 Color: 188
Size: 552 Color: 110

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14844 Color: 595
Size: 1380 Color: 199
Size: 384 Color: 60

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14849 Color: 596
Size: 1467 Color: 207
Size: 292 Color: 28

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14886 Color: 597
Size: 1184 Color: 180
Size: 538 Color: 106

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14946 Color: 600
Size: 1382 Color: 201
Size: 280 Color: 22

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 12565 Color: 482
Size: 4042 Color: 340

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12883 Color: 488
Size: 2812 Color: 304
Size: 912 Color: 158

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 13174 Color: 494
Size: 1995 Color: 247
Size: 1438 Color: 205

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 496
Size: 3073 Color: 315
Size: 312 Color: 38

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 13236 Color: 497
Size: 3371 Color: 324

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 498
Size: 2677 Color: 294
Size: 692 Color: 132

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13265 Color: 500
Size: 2344 Color: 278
Size: 998 Color: 165

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13345 Color: 507
Size: 2810 Color: 303
Size: 452 Color: 87

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13393 Color: 509
Size: 2594 Color: 289
Size: 620 Color: 121

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13649 Color: 517
Size: 2606 Color: 291
Size: 352 Color: 54

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 13866 Color: 526
Size: 2741 Color: 300

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 14319 Color: 559
Size: 2288 Color: 277

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 14396 Color: 564
Size: 2211 Color: 267

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 14525 Color: 570
Size: 2082 Color: 257

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 14697 Color: 580
Size: 1582 Color: 218
Size: 328 Color: 44

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 14712 Color: 581
Size: 1895 Color: 241

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 14908 Color: 599
Size: 1699 Color: 229

Bin 74: 2 of cap free
Amount of items: 5
Items: 
Size: 9496 Color: 430
Size: 5942 Color: 377
Size: 392 Color: 64
Size: 392 Color: 63
Size: 384 Color: 62

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 9635 Color: 433
Size: 6607 Color: 390
Size: 364 Color: 55

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 483
Size: 3846 Color: 336

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13158 Color: 493
Size: 3448 Color: 326

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13958 Color: 535
Size: 2648 Color: 293

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14004 Color: 536
Size: 2602 Color: 290

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14328 Color: 560
Size: 2278 Color: 275

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14374 Color: 562
Size: 2232 Color: 269

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14470 Color: 567
Size: 2136 Color: 263

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 14558 Color: 572
Size: 2048 Color: 250

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 10358 Color: 442
Size: 5919 Color: 373
Size: 328 Color: 43

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 13397 Color: 510
Size: 3208 Color: 323

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 13437 Color: 512
Size: 3168 Color: 322

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 530
Size: 2681 Color: 295

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 14081 Color: 539
Size: 2524 Color: 288

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 14601 Color: 575
Size: 2004 Color: 248

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13588 Color: 515
Size: 3016 Color: 313

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 525
Size: 2796 Color: 302

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13901 Color: 529
Size: 2703 Color: 297

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 552
Size: 2418 Color: 283

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14616 Color: 576
Size: 1988 Color: 246

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 14742 Color: 586
Size: 1862 Color: 240

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 14800 Color: 590
Size: 1804 Color: 236

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 14888 Color: 598
Size: 1716 Color: 231

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 10156 Color: 438
Size: 6103 Color: 383
Size: 344 Color: 49

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 12830 Color: 484
Size: 3773 Color: 334

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 13741 Color: 523
Size: 2862 Color: 308

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 14212 Color: 553
Size: 2391 Color: 280

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 14423 Color: 565
Size: 2180 Color: 264

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 14581 Color: 573
Size: 2022 Color: 249

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 12450 Color: 478
Size: 4004 Color: 337
Size: 148 Color: 5

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13724 Color: 522
Size: 2878 Color: 309

Bin 106: 7 of cap free
Amount of items: 3
Items: 
Size: 12137 Color: 472
Size: 3466 Color: 328
Size: 998 Color: 164

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 12344 Color: 475
Size: 4257 Color: 344

Bin 108: 7 of cap free
Amount of items: 2
Items: 
Size: 13192 Color: 495
Size: 3409 Color: 325

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 14522 Color: 569
Size: 2079 Color: 256

Bin 110: 8 of cap free
Amount of items: 3
Items: 
Size: 11170 Color: 457
Size: 5142 Color: 360
Size: 288 Color: 25

Bin 111: 8 of cap free
Amount of items: 3
Items: 
Size: 11826 Color: 468
Size: 4534 Color: 349
Size: 240 Color: 13

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 13146 Color: 492
Size: 3454 Color: 327

Bin 113: 8 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 524
Size: 2776 Color: 301
Size: 24 Color: 0

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13957 Color: 534
Size: 2643 Color: 292

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 14248 Color: 555
Size: 2352 Color: 279

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 563
Size: 2210 Color: 266

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 14665 Color: 579
Size: 1934 Color: 244

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 14773 Color: 589
Size: 1826 Color: 238

Bin 119: 10 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 464
Size: 4580 Color: 351
Size: 256 Color: 15

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 14102 Color: 542
Size: 2496 Color: 287

Bin 121: 11 of cap free
Amount of items: 2
Items: 
Size: 14335 Color: 561
Size: 2262 Color: 273

Bin 122: 12 of cap free
Amount of items: 5
Items: 
Size: 9467 Color: 425
Size: 5681 Color: 368
Size: 576 Color: 117
Size: 440 Color: 79
Size: 432 Color: 78

Bin 123: 12 of cap free
Amount of items: 3
Items: 
Size: 10024 Color: 437
Size: 6228 Color: 387
Size: 344 Color: 50

Bin 124: 12 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 454
Size: 5210 Color: 361
Size: 294 Color: 29

Bin 125: 12 of cap free
Amount of items: 3
Items: 
Size: 12472 Color: 480
Size: 4028 Color: 339
Size: 96 Color: 3

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 13710 Color: 520
Size: 2886 Color: 310

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 558
Size: 2286 Color: 276

Bin 128: 13 of cap free
Amount of items: 2
Items: 
Size: 12977 Color: 490
Size: 3618 Color: 331

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 568
Size: 2107 Color: 261

Bin 130: 14 of cap free
Amount of items: 3
Items: 
Size: 12270 Color: 474
Size: 4152 Color: 342
Size: 172 Color: 6

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 14164 Color: 551
Size: 2430 Color: 285

Bin 132: 16 of cap free
Amount of items: 3
Items: 
Size: 10861 Color: 452
Size: 5431 Color: 366
Size: 300 Color: 32

Bin 133: 16 of cap free
Amount of items: 3
Items: 
Size: 11778 Color: 465
Size: 2740 Color: 299
Size: 2074 Color: 254

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 12232 Color: 473
Size: 4360 Color: 347

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 14132 Color: 549
Size: 2460 Color: 286

Bin 136: 17 of cap free
Amount of items: 7
Items: 
Size: 8308 Color: 407
Size: 1691 Color: 228
Size: 1691 Color: 227
Size: 1673 Color: 225
Size: 1564 Color: 216
Size: 1136 Color: 175
Size: 528 Color: 102

Bin 137: 17 of cap free
Amount of items: 3
Items: 
Size: 11501 Color: 461
Size: 2833 Color: 306
Size: 2257 Color: 271

Bin 138: 17 of cap free
Amount of items: 2
Items: 
Size: 12380 Color: 477
Size: 4211 Color: 343

Bin 139: 17 of cap free
Amount of items: 2
Items: 
Size: 13486 Color: 513
Size: 3105 Color: 316

Bin 140: 17 of cap free
Amount of items: 2
Items: 
Size: 13894 Color: 528
Size: 2697 Color: 296

Bin 141: 19 of cap free
Amount of items: 2
Items: 
Size: 13562 Color: 514
Size: 3027 Color: 314

Bin 142: 20 of cap free
Amount of items: 3
Items: 
Size: 11384 Color: 459
Size: 4920 Color: 356
Size: 284 Color: 23

Bin 143: 20 of cap free
Amount of items: 3
Items: 
Size: 12466 Color: 479
Size: 4026 Color: 338
Size: 96 Color: 4

Bin 144: 21 of cap free
Amount of items: 2
Items: 
Size: 12860 Color: 487
Size: 3727 Color: 333

Bin 145: 22 of cap free
Amount of items: 3
Items: 
Size: 11848 Color: 469
Size: 4546 Color: 350
Size: 192 Color: 10

Bin 146: 22 of cap free
Amount of items: 3
Items: 
Size: 11994 Color: 470
Size: 4400 Color: 348
Size: 192 Color: 8

Bin 147: 26 of cap free
Amount of items: 2
Items: 
Size: 11086 Color: 453
Size: 5496 Color: 367

Bin 148: 26 of cap free
Amount of items: 2
Items: 
Size: 13432 Color: 511
Size: 3150 Color: 321

Bin 149: 29 of cap free
Amount of items: 3
Items: 
Size: 11447 Color: 460
Size: 4856 Color: 355
Size: 276 Color: 20

Bin 150: 29 of cap free
Amount of items: 2
Items: 
Size: 13723 Color: 521
Size: 2856 Color: 307

Bin 151: 31 of cap free
Amount of items: 2
Items: 
Size: 12921 Color: 489
Size: 3656 Color: 332

Bin 152: 33 of cap free
Amount of items: 3
Items: 
Size: 12081 Color: 471
Size: 4318 Color: 346
Size: 176 Color: 7

Bin 153: 33 of cap free
Amount of items: 2
Items: 
Size: 12519 Color: 481
Size: 4056 Color: 341

Bin 154: 35 of cap free
Amount of items: 3
Items: 
Size: 10442 Color: 444
Size: 5811 Color: 371
Size: 320 Color: 41

Bin 155: 35 of cap free
Amount of items: 3
Items: 
Size: 11344 Color: 458
Size: 4941 Color: 357
Size: 288 Color: 24

Bin 156: 36 of cap free
Amount of items: 2
Items: 
Size: 10613 Color: 447
Size: 5959 Color: 381

Bin 157: 37 of cap free
Amount of items: 2
Items: 
Size: 11780 Color: 466
Size: 4791 Color: 354

Bin 158: 44 of cap free
Amount of items: 2
Items: 
Size: 10609 Color: 446
Size: 5955 Color: 380

Bin 159: 55 of cap free
Amount of items: 3
Items: 
Size: 10545 Color: 445
Size: 5688 Color: 369
Size: 320 Color: 40

Bin 160: 64 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 418
Size: 7296 Color: 401
Size: 440 Color: 83

Bin 161: 64 of cap free
Amount of items: 3
Items: 
Size: 9188 Color: 421
Size: 6916 Color: 396
Size: 440 Color: 80

Bin 162: 68 of cap free
Amount of items: 2
Items: 
Size: 8316 Color: 412
Size: 8224 Color: 404

Bin 163: 89 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 443
Size: 5763 Color: 370
Size: 322 Color: 42

Bin 164: 92 of cap free
Amount of items: 16
Items: 
Size: 1376 Color: 195
Size: 1376 Color: 194
Size: 1240 Color: 190
Size: 1200 Color: 187
Size: 1190 Color: 184
Size: 1188 Color: 182
Size: 1186 Color: 181
Size: 1184 Color: 179
Size: 1162 Color: 177
Size: 1072 Color: 173
Size: 1064 Color: 172
Size: 1064 Color: 171
Size: 564 Color: 114
Size: 560 Color: 111
Size: 546 Color: 108
Size: 544 Color: 107

Bin 165: 97 of cap free
Amount of items: 3
Items: 
Size: 9655 Color: 434
Size: 6504 Color: 389
Size: 352 Color: 53

Bin 166: 100 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 463
Size: 4604 Color: 353
Size: 264 Color: 18

Bin 167: 109 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 440
Size: 5951 Color: 379
Size: 336 Color: 46

Bin 168: 114 of cap free
Amount of items: 3
Items: 
Size: 9140 Color: 420
Size: 6914 Color: 395
Size: 440 Color: 81

Bin 169: 126 of cap free
Amount of items: 3
Items: 
Size: 10196 Color: 439
Size: 5950 Color: 378
Size: 336 Color: 47

Bin 170: 131 of cap free
Amount of items: 3
Items: 
Size: 9124 Color: 419
Size: 6913 Color: 394
Size: 440 Color: 82

Bin 171: 132 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 451
Size: 5380 Color: 365
Size: 304 Color: 33

Bin 172: 150 of cap free
Amount of items: 4
Items: 
Size: 9531 Color: 432
Size: 6187 Color: 385
Size: 372 Color: 58
Size: 368 Color: 56

Bin 173: 165 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 456
Size: 5001 Color: 359
Size: 288 Color: 26

Bin 174: 177 of cap free
Amount of items: 3
Items: 
Size: 11557 Color: 462
Size: 4602 Color: 352
Size: 272 Color: 19

Bin 175: 195 of cap free
Amount of items: 4
Items: 
Size: 9507 Color: 431
Size: 6144 Color: 384
Size: 384 Color: 61
Size: 378 Color: 59

Bin 176: 207 of cap free
Amount of items: 3
Items: 
Size: 11116 Color: 455
Size: 4997 Color: 358
Size: 288 Color: 27

Bin 177: 219 of cap free
Amount of items: 3
Items: 
Size: 8681 Color: 417
Size: 7260 Color: 400
Size: 448 Color: 84

Bin 178: 223 of cap free
Amount of items: 3
Items: 
Size: 8330 Color: 416
Size: 7603 Color: 402
Size: 452 Color: 88

Bin 179: 223 of cap free
Amount of items: 2
Items: 
Size: 9463 Color: 424
Size: 6922 Color: 399

Bin 180: 228 of cap free
Amount of items: 2
Items: 
Size: 9459 Color: 423
Size: 6921 Color: 398

Bin 181: 229 of cap free
Amount of items: 3
Items: 
Size: 9791 Color: 436
Size: 6244 Color: 388
Size: 344 Color: 51

Bin 182: 238 of cap free
Amount of items: 2
Items: 
Size: 8314 Color: 411
Size: 8056 Color: 403

Bin 183: 244 of cap free
Amount of items: 3
Items: 
Size: 10712 Color: 450
Size: 5348 Color: 364
Size: 304 Color: 34

Bin 184: 247 of cap free
Amount of items: 3
Items: 
Size: 11804 Color: 467
Size: 4301 Color: 345
Size: 256 Color: 14

Bin 185: 255 of cap free
Amount of items: 3
Items: 
Size: 10709 Color: 449
Size: 5332 Color: 363
Size: 312 Color: 37

Bin 186: 290 of cap free
Amount of items: 3
Items: 
Size: 9784 Color: 435
Size: 6188 Color: 386
Size: 346 Color: 52

Bin 187: 297 of cap free
Amount of items: 2
Items: 
Size: 9394 Color: 422
Size: 6917 Color: 397

Bin 188: 315 of cap free
Amount of items: 3
Items: 
Size: 10681 Color: 448
Size: 5292 Color: 362
Size: 320 Color: 39

Bin 189: 329 of cap free
Amount of items: 8
Items: 
Size: 8305 Color: 405
Size: 1386 Color: 203
Size: 1382 Color: 202
Size: 1382 Color: 200
Size: 1380 Color: 198
Size: 1376 Color: 196
Size: 534 Color: 105
Size: 534 Color: 104

Bin 190: 363 of cap free
Amount of items: 4
Items: 
Size: 9483 Color: 428
Size: 5938 Color: 375
Size: 412 Color: 69
Size: 412 Color: 68

Bin 191: 372 of cap free
Amount of items: 4
Items: 
Size: 9478 Color: 427
Size: 5928 Color: 374
Size: 416 Color: 73
Size: 414 Color: 71

Bin 192: 376 of cap free
Amount of items: 7
Items: 
Size: 8306 Color: 406
Size: 1558 Color: 215
Size: 1496 Color: 210
Size: 1476 Color: 208
Size: 1448 Color: 206
Size: 1420 Color: 204
Size: 528 Color: 103

Bin 193: 383 of cap free
Amount of items: 4
Items: 
Size: 9486 Color: 429
Size: 5939 Color: 376
Size: 400 Color: 66
Size: 400 Color: 65

Bin 194: 389 of cap free
Amount of items: 6
Items: 
Size: 8309 Color: 408
Size: 2068 Color: 252
Size: 1844 Color: 239
Size: 1768 Color: 234
Size: 1710 Color: 230
Size: 520 Color: 101

Bin 195: 429 of cap free
Amount of items: 4
Items: 
Size: 8317 Color: 413
Size: 6902 Color: 391
Size: 480 Color: 95
Size: 480 Color: 94

Bin 196: 438 of cap free
Amount of items: 4
Items: 
Size: 8322 Color: 414
Size: 6904 Color: 392
Size: 476 Color: 93
Size: 468 Color: 92

Bin 197: 454 of cap free
Amount of items: 4
Items: 
Size: 8328 Color: 415
Size: 6906 Color: 393
Size: 464 Color: 91
Size: 456 Color: 89

Bin 198: 471 of cap free
Amount of items: 4
Items: 
Size: 8313 Color: 410
Size: 3816 Color: 335
Size: 3524 Color: 329
Size: 484 Color: 97

Bin 199: 6406 of cap free
Amount of items: 13
Items: 
Size: 850 Color: 149
Size: 850 Color: 148
Size: 840 Color: 147
Size: 816 Color: 146
Size: 804 Color: 145
Size: 800 Color: 143
Size: 800 Color: 142
Size: 784 Color: 141
Size: 768 Color: 140
Size: 754 Color: 139
Size: 744 Color: 138
Size: 704 Color: 133
Size: 688 Color: 131

Total size: 3288384
Total free space: 16608

