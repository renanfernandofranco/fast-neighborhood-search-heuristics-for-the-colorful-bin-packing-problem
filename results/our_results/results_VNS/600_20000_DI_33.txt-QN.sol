Capicity Bin: 16400
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11964 Color: 468
Size: 3700 Color: 335
Size: 736 Color: 141

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11986 Color: 469
Size: 3682 Color: 333
Size: 732 Color: 139

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 470
Size: 3682 Color: 334
Size: 716 Color: 134

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12008 Color: 471
Size: 2920 Color: 301
Size: 1472 Color: 211

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12050 Color: 473
Size: 3626 Color: 330
Size: 724 Color: 137

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12163 Color: 482
Size: 2989 Color: 306
Size: 1248 Color: 194

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12178 Color: 484
Size: 3666 Color: 332
Size: 556 Color: 102

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12194 Color: 485
Size: 3922 Color: 343
Size: 284 Color: 22

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12483 Color: 489
Size: 3265 Color: 317
Size: 652 Color: 120

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12596 Color: 494
Size: 3416 Color: 319
Size: 388 Color: 60

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12996 Color: 504
Size: 2974 Color: 305
Size: 430 Color: 71

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 505
Size: 2844 Color: 300
Size: 536 Color: 99

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13046 Color: 506
Size: 1716 Color: 227
Size: 1638 Color: 223

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13078 Color: 508
Size: 2450 Color: 282
Size: 872 Color: 159

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13285 Color: 515
Size: 2597 Color: 290
Size: 518 Color: 94

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13286 Color: 516
Size: 2170 Color: 263
Size: 944 Color: 173

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13300 Color: 517
Size: 2728 Color: 295
Size: 372 Color: 56

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 519
Size: 2448 Color: 281
Size: 632 Color: 115

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13321 Color: 520
Size: 2567 Color: 286
Size: 512 Color: 88

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13441 Color: 523
Size: 2293 Color: 271
Size: 666 Color: 122

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 525
Size: 1544 Color: 217
Size: 1360 Color: 199

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13604 Color: 528
Size: 2568 Color: 287
Size: 228 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13617 Color: 529
Size: 1791 Color: 234
Size: 992 Color: 175

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13633 Color: 532
Size: 2251 Color: 268
Size: 516 Color: 93

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13638 Color: 533
Size: 2302 Color: 272
Size: 460 Color: 78

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13676 Color: 534
Size: 2564 Color: 285
Size: 160 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13686 Color: 535
Size: 2116 Color: 256
Size: 598 Color: 110

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 537
Size: 1964 Color: 247
Size: 716 Color: 135

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13843 Color: 541
Size: 2105 Color: 255
Size: 452 Color: 77

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13847 Color: 542
Size: 2131 Color: 258
Size: 422 Color: 69

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13859 Color: 543
Size: 1743 Color: 231
Size: 798 Color: 152

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13868 Color: 544
Size: 2070 Color: 251
Size: 462 Color: 80

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 546
Size: 2152 Color: 260
Size: 352 Color: 47

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 549
Size: 1878 Color: 242
Size: 534 Color: 98

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 550
Size: 2095 Color: 254
Size: 310 Color: 33

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14036 Color: 553
Size: 1852 Color: 240
Size: 512 Color: 90

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14180 Color: 560
Size: 1512 Color: 213
Size: 708 Color: 133

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14183 Color: 561
Size: 1929 Color: 243
Size: 288 Color: 23

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 562
Size: 1768 Color: 232
Size: 432 Color: 72

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14201 Color: 563
Size: 1805 Color: 236
Size: 394 Color: 61

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14204 Color: 564
Size: 1280 Color: 196
Size: 916 Color: 171

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14252 Color: 568
Size: 1204 Color: 192
Size: 944 Color: 172

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14341 Color: 573
Size: 1599 Color: 220
Size: 460 Color: 79

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14348 Color: 574
Size: 1716 Color: 228
Size: 336 Color: 40

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14376 Color: 576
Size: 1136 Color: 184
Size: 888 Color: 161

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14383 Color: 577
Size: 1681 Color: 224
Size: 336 Color: 41

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14438 Color: 578
Size: 1702 Color: 226
Size: 260 Color: 11

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14483 Color: 581
Size: 1559 Color: 218
Size: 358 Color: 49

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 582
Size: 1280 Color: 195
Size: 636 Color: 116

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14510 Color: 583
Size: 1630 Color: 222
Size: 260 Color: 12

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 584
Size: 1364 Color: 201
Size: 512 Color: 91

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14552 Color: 586
Size: 1360 Color: 200
Size: 488 Color: 85

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14571 Color: 588
Size: 1525 Color: 215
Size: 304 Color: 28

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14621 Color: 591
Size: 1483 Color: 212
Size: 296 Color: 26

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14640 Color: 592
Size: 992 Color: 176
Size: 768 Color: 148

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14669 Color: 593
Size: 1443 Color: 209
Size: 288 Color: 25

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14676 Color: 594
Size: 1018 Color: 178
Size: 706 Color: 130

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14706 Color: 596
Size: 910 Color: 168
Size: 784 Color: 151

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14724 Color: 598
Size: 1152 Color: 187
Size: 524 Color: 95

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14750 Color: 599
Size: 1178 Color: 189
Size: 472 Color: 83

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14754 Color: 600
Size: 1072 Color: 183
Size: 574 Color: 105

Bin 62: 1 of cap free
Amount of items: 5
Items: 
Size: 9222 Color: 422
Size: 5901 Color: 378
Size: 464 Color: 81
Size: 412 Color: 64
Size: 400 Color: 63

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11672 Color: 458
Size: 3539 Color: 325
Size: 1188 Color: 190

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 465
Size: 2638 Color: 293
Size: 1849 Color: 239

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 11954 Color: 467
Size: 4445 Color: 355

Bin 66: 1 of cap free
Amount of items: 4
Items: 
Size: 12130 Color: 479
Size: 3991 Color: 347
Size: 150 Color: 4
Size: 128 Color: 3

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12171 Color: 483
Size: 3852 Color: 341
Size: 376 Color: 57

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12553 Color: 491
Size: 3202 Color: 314
Size: 644 Color: 118

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 493
Size: 3525 Color: 323
Size: 280 Color: 21

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 12877 Color: 501
Size: 3522 Color: 322

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13077 Color: 507
Size: 2962 Color: 304
Size: 360 Color: 50

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13313 Color: 518
Size: 1742 Color: 230
Size: 1344 Color: 198

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 13628 Color: 531
Size: 2771 Color: 297

Bin 74: 1 of cap free
Amount of items: 2
Items: 
Size: 13811 Color: 539
Size: 2588 Color: 289

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 14251 Color: 567
Size: 1412 Color: 206
Size: 736 Color: 142

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 14280 Color: 570
Size: 2119 Color: 257

Bin 77: 1 of cap free
Amount of items: 2
Items: 
Size: 14682 Color: 595
Size: 1717 Color: 229

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 12034 Color: 472
Size: 4364 Color: 354

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 12210 Color: 486
Size: 3740 Color: 337
Size: 448 Color: 74

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 503
Size: 3494 Color: 320

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14150 Color: 557
Size: 2248 Color: 267

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 572
Size: 2088 Color: 253

Bin 83: 3 of cap free
Amount of items: 5
Items: 
Size: 10970 Color: 446
Size: 4495 Color: 356
Size: 324 Color: 38
Size: 304 Color: 30
Size: 304 Color: 29

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 11563 Color: 456
Size: 4594 Color: 362
Size: 240 Color: 10

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 11916 Color: 466
Size: 3583 Color: 328
Size: 898 Color: 164

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 14027 Color: 552
Size: 2370 Color: 278

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 11906 Color: 464
Size: 3706 Color: 336
Size: 784 Color: 149

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 12834 Color: 499
Size: 3562 Color: 327

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 13558 Color: 526
Size: 2820 Color: 299
Size: 18 Color: 0

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13798 Color: 538
Size: 2598 Color: 291

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 590
Size: 1796 Color: 235

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 14708 Color: 597
Size: 1688 Color: 225

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 12849 Color: 500
Size: 3546 Color: 326

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 14063 Color: 555
Size: 2332 Color: 276

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 14446 Color: 579
Size: 1949 Color: 246

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 14531 Color: 585
Size: 1864 Color: 241

Bin 97: 6 of cap free
Amount of items: 3
Items: 
Size: 9356 Color: 428
Size: 6678 Color: 393
Size: 360 Color: 51

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 437
Size: 5764 Color: 376
Size: 324 Color: 37

Bin 99: 6 of cap free
Amount of items: 3
Items: 
Size: 10996 Color: 449
Size: 5118 Color: 370
Size: 280 Color: 20

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 11687 Color: 459
Size: 3531 Color: 324
Size: 1176 Color: 188

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 11868 Color: 463
Size: 4526 Color: 360

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 13624 Color: 530
Size: 2770 Color: 296

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 13998 Color: 551
Size: 2396 Color: 279

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 14235 Color: 566
Size: 2159 Color: 261

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 14262 Color: 569
Size: 2132 Color: 259

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 14558 Color: 587
Size: 1836 Color: 238

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 14572 Color: 589
Size: 1822 Color: 237

Bin 108: 7 of cap free
Amount of items: 9
Items: 
Size: 8201 Color: 405
Size: 1374 Color: 203
Size: 1366 Color: 202
Size: 1298 Color: 197
Size: 1230 Color: 193
Size: 1196 Color: 191
Size: 624 Color: 112
Size: 552 Color: 101
Size: 552 Color: 100

Bin 109: 7 of cap free
Amount of items: 3
Items: 
Size: 9484 Color: 430
Size: 6557 Color: 392
Size: 352 Color: 46

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 12312 Color: 487
Size: 4081 Color: 349

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 13392 Color: 522
Size: 3001 Color: 307

Bin 112: 7 of cap free
Amount of items: 2
Items: 
Size: 13462 Color: 524
Size: 2931 Color: 302

Bin 113: 7 of cap free
Amount of items: 2
Items: 
Size: 14052 Color: 554
Size: 2341 Color: 277

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 14155 Color: 558
Size: 2238 Color: 266

Bin 115: 7 of cap free
Amount of items: 2
Items: 
Size: 14309 Color: 571
Size: 2084 Color: 252

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 14168 Color: 559
Size: 2224 Color: 265

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 14453 Color: 580
Size: 1939 Color: 245

Bin 118: 9 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 452
Size: 4947 Color: 365
Size: 280 Color: 17

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 12885 Color: 502
Size: 3506 Color: 321

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 10408 Color: 439
Size: 5982 Color: 381

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 10938 Color: 445
Size: 5148 Color: 371
Size: 304 Color: 31

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 11694 Color: 460
Size: 3172 Color: 312
Size: 1524 Color: 214

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 12066 Color: 474
Size: 4324 Color: 353

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 514
Size: 3152 Color: 311

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 13966 Color: 548
Size: 2424 Color: 280

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 14358 Color: 575
Size: 2032 Color: 250

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 13591 Color: 527
Size: 2798 Color: 298

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 540
Size: 2573 Color: 288

Bin 129: 11 of cap free
Amount of items: 2
Items: 
Size: 14082 Color: 556
Size: 2307 Color: 273

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 12444 Color: 488
Size: 3944 Color: 345

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 13088 Color: 509
Size: 3300 Color: 318

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 12801 Color: 498
Size: 3586 Color: 329

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 13875 Color: 545
Size: 2512 Color: 284

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 10428 Color: 440
Size: 5958 Color: 380

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 13144 Color: 510
Size: 3242 Color: 316

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 13179 Color: 511
Size: 3207 Color: 315

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 13701 Color: 536
Size: 2685 Color: 294

Bin 138: 15 of cap free
Amount of items: 2
Items: 
Size: 12743 Color: 497
Size: 3642 Color: 331

Bin 139: 15 of cap free
Amount of items: 2
Items: 
Size: 13237 Color: 513
Size: 3148 Color: 310

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 13918 Color: 547
Size: 2467 Color: 283

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 14214 Color: 565
Size: 2171 Color: 264

Bin 142: 16 of cap free
Amount of items: 3
Items: 
Size: 11007 Color: 450
Size: 5097 Color: 369
Size: 280 Color: 19

Bin 143: 16 of cap free
Amount of items: 2
Items: 
Size: 12632 Color: 496
Size: 3752 Color: 339

Bin 144: 16 of cap free
Amount of items: 2
Items: 
Size: 13210 Color: 512
Size: 3174 Color: 313

Bin 145: 18 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 421
Size: 6792 Color: 394
Size: 416 Color: 65

Bin 146: 18 of cap free
Amount of items: 3
Items: 
Size: 9533 Color: 431
Size: 6501 Color: 390
Size: 348 Color: 45

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 12562 Color: 492
Size: 3820 Color: 340

Bin 148: 19 of cap free
Amount of items: 3
Items: 
Size: 8933 Color: 417
Size: 7022 Color: 399
Size: 426 Color: 70

Bin 149: 19 of cap free
Amount of items: 3
Items: 
Size: 10338 Color: 438
Size: 5723 Color: 375
Size: 320 Color: 36

Bin 150: 20 of cap free
Amount of items: 7
Items: 
Size: 8204 Color: 407
Size: 1782 Color: 233
Size: 1612 Color: 221
Size: 1578 Color: 219
Size: 1538 Color: 216
Size: 1152 Color: 186
Size: 514 Color: 92

Bin 151: 20 of cap free
Amount of items: 3
Items: 
Size: 10228 Color: 435
Size: 5816 Color: 377
Size: 336 Color: 39

Bin 152: 20 of cap free
Amount of items: 2
Items: 
Size: 11780 Color: 461
Size: 4600 Color: 363

Bin 153: 22 of cap free
Amount of items: 3
Items: 
Size: 10014 Color: 433
Size: 6022 Color: 383
Size: 342 Color: 43

Bin 154: 22 of cap free
Amount of items: 2
Items: 
Size: 12098 Color: 476
Size: 4280 Color: 352

Bin 155: 24 of cap free
Amount of items: 2
Items: 
Size: 8264 Color: 412
Size: 8112 Color: 404

Bin 156: 26 of cap free
Amount of items: 2
Items: 
Size: 11820 Color: 462
Size: 4554 Color: 361

Bin 157: 26 of cap free
Amount of items: 2
Items: 
Size: 12628 Color: 495
Size: 3746 Color: 338

Bin 158: 27 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 521
Size: 3049 Color: 308

Bin 159: 34 of cap free
Amount of items: 2
Items: 
Size: 12514 Color: 490
Size: 3852 Color: 342

Bin 160: 35 of cap free
Amount of items: 3
Items: 
Size: 11611 Color: 457
Size: 4520 Color: 359
Size: 234 Color: 9

Bin 161: 37 of cap free
Amount of items: 3
Items: 
Size: 10107 Color: 434
Size: 5916 Color: 379
Size: 340 Color: 42

Bin 162: 38 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 444
Size: 3144 Color: 309
Size: 2328 Color: 275

Bin 163: 41 of cap free
Amount of items: 3
Items: 
Size: 8601 Color: 413
Size: 7310 Color: 401
Size: 448 Color: 76

Bin 164: 42 of cap free
Amount of items: 7
Items: 
Size: 8205 Color: 408
Size: 2168 Color: 262
Size: 1979 Color: 248
Size: 1934 Color: 244
Size: 1064 Color: 182
Size: 512 Color: 89
Size: 496 Color: 87

Bin 165: 43 of cap free
Amount of items: 3
Items: 
Size: 10987 Color: 448
Size: 5082 Color: 368
Size: 288 Color: 24

Bin 166: 48 of cap free
Amount of items: 3
Items: 
Size: 8704 Color: 416
Size: 7216 Color: 400
Size: 432 Color: 73

Bin 167: 53 of cap free
Amount of items: 3
Items: 
Size: 11067 Color: 451
Size: 5000 Color: 367
Size: 280 Color: 18

Bin 168: 57 of cap free
Amount of items: 2
Items: 
Size: 12079 Color: 475
Size: 4264 Color: 351

Bin 169: 60 of cap free
Amount of items: 4
Items: 
Size: 10465 Color: 441
Size: 5245 Color: 372
Size: 318 Color: 35
Size: 312 Color: 34

Bin 170: 70 of cap free
Amount of items: 8
Items: 
Size: 8202 Color: 406
Size: 1444 Color: 210
Size: 1434 Color: 208
Size: 1414 Color: 207
Size: 1404 Color: 205
Size: 1378 Color: 204
Size: 528 Color: 97
Size: 526 Color: 96

Bin 171: 76 of cap free
Amount of items: 3
Items: 
Size: 9432 Color: 429
Size: 6536 Color: 391
Size: 356 Color: 48

Bin 172: 78 of cap free
Amount of items: 4
Items: 
Size: 9270 Color: 424
Size: 6300 Color: 386
Size: 384 Color: 58
Size: 368 Color: 55

Bin 173: 80 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 454
Size: 4776 Color: 364
Size: 272 Color: 14

Bin 174: 83 of cap free
Amount of items: 2
Items: 
Size: 10285 Color: 436
Size: 6032 Color: 384

Bin 175: 94 of cap free
Amount of items: 3
Items: 
Size: 10680 Color: 442
Size: 5322 Color: 373
Size: 304 Color: 32

Bin 176: 95 of cap free
Amount of items: 3
Items: 
Size: 12155 Color: 481
Size: 4102 Color: 350
Size: 48 Color: 1

Bin 177: 98 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 432
Size: 5994 Color: 382
Size: 348 Color: 44

Bin 178: 115 of cap free
Amount of items: 2
Items: 
Size: 8664 Color: 414
Size: 7621 Color: 402

Bin 179: 120 of cap free
Amount of items: 5
Items: 
Size: 8212 Color: 410
Size: 2961 Color: 303
Size: 2637 Color: 292
Size: 2002 Color: 249
Size: 468 Color: 82

Bin 180: 127 of cap free
Amount of items: 3
Items: 
Size: 12146 Color: 480
Size: 4031 Color: 348
Size: 96 Color: 2

Bin 181: 128 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 478
Size: 3976 Color: 346
Size: 192 Color: 6

Bin 182: 132 of cap free
Amount of items: 3
Items: 
Size: 10984 Color: 447
Size: 4980 Color: 366
Size: 304 Color: 27

Bin 183: 135 of cap free
Amount of items: 3
Items: 
Size: 11482 Color: 455
Size: 4511 Color: 358
Size: 272 Color: 13

Bin 184: 136 of cap free
Amount of items: 4
Items: 
Size: 11212 Color: 453
Size: 4508 Color: 357
Size: 272 Color: 16
Size: 272 Color: 15

Bin 185: 139 of cap free
Amount of items: 3
Items: 
Size: 9009 Color: 420
Size: 6836 Color: 398
Size: 416 Color: 66

Bin 186: 144 of cap free
Amount of items: 2
Items: 
Size: 10888 Color: 443
Size: 5368 Color: 374

Bin 187: 150 of cap free
Amount of items: 3
Items: 
Size: 9000 Color: 419
Size: 6834 Color: 397
Size: 416 Color: 67

Bin 188: 161 of cap free
Amount of items: 3
Items: 
Size: 8986 Color: 418
Size: 6833 Color: 396
Size: 420 Color: 68

Bin 189: 174 of cap free
Amount of items: 3
Items: 
Size: 12101 Color: 477
Size: 3929 Color: 344
Size: 196 Color: 7

Bin 190: 201 of cap free
Amount of items: 4
Items: 
Size: 9254 Color: 423
Size: 6161 Color: 385
Size: 400 Color: 62
Size: 384 Color: 59

Bin 191: 214 of cap free
Amount of items: 22
Items: 
Size: 880 Color: 160
Size: 864 Color: 158
Size: 864 Color: 157
Size: 848 Color: 156
Size: 832 Color: 155
Size: 816 Color: 154
Size: 806 Color: 153
Size: 784 Color: 150
Size: 760 Color: 147
Size: 748 Color: 146
Size: 744 Color: 145
Size: 700 Color: 127
Size: 696 Color: 126
Size: 680 Color: 125
Size: 680 Color: 124
Size: 672 Color: 123
Size: 656 Color: 121
Size: 652 Color: 119
Size: 640 Color: 117
Size: 632 Color: 114
Size: 624 Color: 113
Size: 608 Color: 111

Bin 192: 254 of cap free
Amount of items: 3
Items: 
Size: 9321 Color: 427
Size: 6461 Color: 389
Size: 364 Color: 52

Bin 193: 268 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 426
Size: 6456 Color: 388
Size: 368 Color: 53

Bin 194: 343 of cap free
Amount of items: 3
Items: 
Size: 9273 Color: 425
Size: 6416 Color: 387
Size: 368 Color: 54

Bin 195: 352 of cap free
Amount of items: 19
Items: 
Size: 1144 Color: 185
Size: 1048 Color: 181
Size: 1040 Color: 180
Size: 1024 Color: 179
Size: 1012 Color: 177
Size: 988 Color: 174
Size: 912 Color: 170
Size: 912 Color: 169
Size: 908 Color: 167
Size: 904 Color: 166
Size: 902 Color: 165
Size: 896 Color: 163
Size: 896 Color: 162
Size: 592 Color: 109
Size: 590 Color: 108
Size: 584 Color: 107
Size: 576 Color: 106
Size: 560 Color: 104
Size: 560 Color: 103

Bin 196: 359 of cap free
Amount of items: 6
Items: 
Size: 8210 Color: 409
Size: 2321 Color: 274
Size: 2276 Color: 270
Size: 2262 Color: 269
Size: 492 Color: 86
Size: 480 Color: 84

Bin 197: 400 of cap free
Amount of items: 2
Items: 
Size: 8216 Color: 411
Size: 7784 Color: 403

Bin 198: 426 of cap free
Amount of items: 3
Items: 
Size: 8698 Color: 415
Size: 6828 Color: 395
Size: 448 Color: 75

Bin 199: 9906 of cap free
Amount of items: 9
Items: 
Size: 744 Color: 144
Size: 740 Color: 143
Size: 736 Color: 140
Size: 732 Color: 138
Size: 724 Color: 136
Size: 708 Color: 132
Size: 706 Color: 131
Size: 704 Color: 129
Size: 700 Color: 128

Total size: 3247200
Total free space: 16400

