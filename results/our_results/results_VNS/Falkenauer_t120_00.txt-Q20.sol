Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 11
Size: 292 Color: 9
Size: 263 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 16
Size: 275 Color: 18
Size: 252 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 11
Size: 315 Color: 6
Size: 290 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 7
Size: 288 Color: 17
Size: 302 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 14
Size: 303 Color: 2
Size: 253 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 11
Size: 295 Color: 14
Size: 260 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 18
Size: 267 Color: 18
Size: 287 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 17
Size: 298 Color: 6
Size: 252 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 19
Size: 357 Color: 14
Size: 282 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 7
Size: 362 Color: 17
Size: 272 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 253 Color: 1
Size: 250 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 313 Color: 15
Size: 253 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 271 Color: 12
Size: 263 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 19
Size: 276 Color: 17
Size: 254 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 13
Size: 301 Color: 17
Size: 269 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 13
Size: 350 Color: 17
Size: 299 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 8
Size: 274 Color: 8
Size: 252 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 4
Size: 325 Color: 11
Size: 275 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 10
Size: 350 Color: 10
Size: 294 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 18
Size: 314 Color: 15
Size: 266 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 10
Size: 350 Color: 18
Size: 298 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 15
Size: 254 Color: 3
Size: 251 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 11
Size: 333 Color: 2
Size: 262 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 356 Color: 4
Size: 272 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 17
Size: 336 Color: 6
Size: 250 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 10
Size: 366 Color: 5
Size: 268 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 13
Size: 298 Color: 3
Size: 283 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 9
Size: 252 Color: 14
Size: 251 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8
Size: 329 Color: 3
Size: 259 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 13
Size: 269 Color: 1
Size: 259 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 12
Size: 307 Color: 14
Size: 254 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 261 Color: 18
Size: 259 Color: 12

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 357 Color: 9
Size: 273 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 14
Size: 262 Color: 17
Size: 253 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 14
Size: 347 Color: 7
Size: 256 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 5
Size: 267 Color: 15
Size: 255 Color: 12

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 7
Size: 363 Color: 18
Size: 271 Color: 17

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 14
Size: 355 Color: 0
Size: 282 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 366 Color: 11
Size: 258 Color: 19

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 2
Size: 320 Color: 0
Size: 273 Color: 15

Total size: 40000
Total free space: 0

