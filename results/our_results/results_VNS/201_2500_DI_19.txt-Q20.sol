Capicity Bin: 2048
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1029 Color: 4
Size: 851 Color: 4
Size: 168 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 3
Size: 605 Color: 6
Size: 64 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 9
Size: 398 Color: 19
Size: 235 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 18
Size: 554 Color: 11
Size: 68 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 19
Size: 494 Color: 10
Size: 52 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 7
Size: 389 Color: 19
Size: 80 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 1
Size: 388 Color: 8
Size: 46 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 15
Size: 313 Color: 18
Size: 72 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 0
Size: 317 Color: 12
Size: 62 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 6
Size: 314 Color: 12
Size: 60 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 16
Size: 271 Color: 4
Size: 76 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 6
Size: 208 Color: 17
Size: 98 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 8
Size: 170 Color: 12
Size: 108 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 6
Size: 211 Color: 0
Size: 46 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 15
Size: 231 Color: 16
Size: 42 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 6
Size: 190 Color: 18
Size: 44 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 0
Size: 198 Color: 9
Size: 40 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 0
Size: 146 Color: 13
Size: 84 Color: 6

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 2
Size: 491 Color: 0
Size: 140 Color: 18

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 6
Size: 477 Color: 15
Size: 112 Color: 15

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 12
Size: 424 Color: 0
Size: 48 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 15
Size: 395 Color: 6
Size: 70 Color: 18

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 7
Size: 362 Color: 16
Size: 12 Color: 9

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1703 Color: 11
Size: 302 Color: 6
Size: 42 Color: 19

Bin 25: 2 of cap free
Amount of items: 4
Items: 
Size: 1457 Color: 11
Size: 493 Color: 3
Size: 56 Color: 17
Size: 40 Color: 6

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 9
Size: 472 Color: 6
Size: 52 Color: 0

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 14
Size: 406 Color: 1
Size: 78 Color: 9

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1725 Color: 1
Size: 321 Color: 8

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 15
Size: 220 Color: 2
Size: 4 Color: 15

Bin 30: 3 of cap free
Amount of items: 5
Items: 
Size: 1026 Color: 16
Size: 562 Color: 6
Size: 289 Color: 10
Size: 96 Color: 17
Size: 72 Color: 3

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 4
Size: 706 Color: 3
Size: 140 Color: 12

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 19
Size: 607 Color: 4
Size: 64 Color: 18

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1523 Color: 8
Size: 522 Color: 13

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1654 Color: 3
Size: 391 Color: 18

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1754 Color: 11
Size: 291 Color: 2

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1763 Color: 12
Size: 282 Color: 5

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1771 Color: 14
Size: 274 Color: 15

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1806 Color: 11
Size: 239 Color: 13

Bin 39: 4 of cap free
Amount of items: 5
Items: 
Size: 1025 Color: 9
Size: 529 Color: 11
Size: 202 Color: 19
Size: 168 Color: 14
Size: 120 Color: 14

Bin 40: 4 of cap free
Amount of items: 4
Items: 
Size: 1665 Color: 17
Size: 351 Color: 15
Size: 20 Color: 10
Size: 8 Color: 19

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1714 Color: 3
Size: 330 Color: 0

Bin 42: 5 of cap free
Amount of items: 3
Items: 
Size: 1285 Color: 6
Size: 696 Color: 6
Size: 62 Color: 12

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1629 Color: 14
Size: 414 Color: 10

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1690 Color: 5
Size: 353 Color: 4

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 1722 Color: 10
Size: 321 Color: 17

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1767 Color: 19
Size: 274 Color: 12

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1795 Color: 11
Size: 246 Color: 9

Bin 48: 8 of cap free
Amount of items: 4
Items: 
Size: 1625 Color: 12
Size: 261 Color: 14
Size: 146 Color: 6
Size: 8 Color: 16

Bin 49: 12 of cap free
Amount of items: 2
Items: 
Size: 1234 Color: 19
Size: 802 Color: 8

Bin 50: 12 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 2
Size: 316 Color: 2
Size: 146 Color: 6

Bin 51: 16 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 4
Size: 731 Color: 3
Size: 215 Color: 5

Bin 52: 18 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 14
Size: 735 Color: 6
Size: 132 Color: 12

Bin 53: 18 of cap free
Amount of items: 2
Items: 
Size: 1202 Color: 12
Size: 828 Color: 18

Bin 54: 18 of cap free
Amount of items: 2
Items: 
Size: 1321 Color: 1
Size: 709 Color: 12

Bin 55: 18 of cap free
Amount of items: 2
Items: 
Size: 1588 Color: 5
Size: 442 Color: 8

Bin 56: 20 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 6
Size: 682 Color: 8
Size: 48 Color: 14

Bin 57: 23 of cap free
Amount of items: 2
Items: 
Size: 1171 Color: 0
Size: 854 Color: 5

Bin 58: 23 of cap free
Amount of items: 2
Items: 
Size: 1527 Color: 11
Size: 498 Color: 0

Bin 59: 25 of cap free
Amount of items: 2
Items: 
Size: 1386 Color: 0
Size: 637 Color: 4

Bin 60: 26 of cap free
Amount of items: 2
Items: 
Size: 1583 Color: 9
Size: 439 Color: 2

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1167 Color: 10
Size: 853 Color: 14

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1461 Color: 10
Size: 559 Color: 7

Bin 63: 29 of cap free
Amount of items: 2
Items: 
Size: 1280 Color: 4
Size: 739 Color: 18

Bin 64: 32 of cap free
Amount of items: 18
Items: 
Size: 258 Color: 15
Size: 202 Color: 10
Size: 194 Color: 13
Size: 160 Color: 5
Size: 126 Color: 14
Size: 120 Color: 8
Size: 110 Color: 9
Size: 104 Color: 12
Size: 100 Color: 18
Size: 96 Color: 3
Size: 86 Color: 11
Size: 78 Color: 14
Size: 76 Color: 19
Size: 76 Color: 16
Size: 76 Color: 3
Size: 62 Color: 2
Size: 56 Color: 8
Size: 36 Color: 6

Bin 65: 35 of cap free
Amount of items: 2
Items: 
Size: 1383 Color: 14
Size: 630 Color: 5

Bin 66: 1570 of cap free
Amount of items: 10
Items: 
Size: 60 Color: 0
Size: 56 Color: 19
Size: 56 Color: 5
Size: 52 Color: 7
Size: 52 Color: 3
Size: 48 Color: 13
Size: 46 Color: 12
Size: 36 Color: 18
Size: 36 Color: 16
Size: 36 Color: 6

Total size: 133120
Total free space: 2048

