Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 9828 Color: 0
Size: 2969 Color: 3
Size: 2388 Color: 4
Size: 2387 Color: 0
Size: 1056 Color: 1
Size: 1020 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11865 Color: 1
Size: 6487 Color: 2
Size: 1296 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11952 Color: 4
Size: 7216 Color: 1
Size: 480 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12010 Color: 2
Size: 6366 Color: 4
Size: 1272 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 1
Size: 6720 Color: 4
Size: 640 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12384 Color: 0
Size: 5616 Color: 2
Size: 1648 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 0
Size: 5984 Color: 1
Size: 368 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13424 Color: 0
Size: 5224 Color: 1
Size: 1000 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13792 Color: 4
Size: 5176 Color: 3
Size: 680 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13796 Color: 2
Size: 5408 Color: 4
Size: 444 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14259 Color: 3
Size: 4797 Color: 0
Size: 592 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14285 Color: 4
Size: 4295 Color: 3
Size: 1068 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14626 Color: 3
Size: 4186 Color: 0
Size: 836 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14688 Color: 3
Size: 4816 Color: 0
Size: 144 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14936 Color: 2
Size: 3080 Color: 4
Size: 1632 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15216 Color: 3
Size: 3464 Color: 4
Size: 968 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15364 Color: 3
Size: 3924 Color: 0
Size: 360 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15560 Color: 3
Size: 3416 Color: 0
Size: 672 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15740 Color: 4
Size: 3220 Color: 4
Size: 688 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15915 Color: 2
Size: 3085 Color: 2
Size: 648 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15939 Color: 1
Size: 3091 Color: 4
Size: 618 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15960 Color: 2
Size: 2416 Color: 0
Size: 1272 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16008 Color: 0
Size: 2976 Color: 4
Size: 664 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16072 Color: 2
Size: 2296 Color: 0
Size: 1280 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16136 Color: 4
Size: 2912 Color: 2
Size: 600 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16224 Color: 4
Size: 3072 Color: 3
Size: 352 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16096 Color: 3
Size: 2992 Color: 0
Size: 560 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16252 Color: 4
Size: 2836 Color: 0
Size: 560 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16432 Color: 4
Size: 2936 Color: 3
Size: 280 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16441 Color: 1
Size: 2919 Color: 4
Size: 288 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16488 Color: 3
Size: 2648 Color: 0
Size: 512 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16565 Color: 3
Size: 2571 Color: 0
Size: 512 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16650 Color: 0
Size: 2516 Color: 4
Size: 482 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16784 Color: 2
Size: 1856 Color: 0
Size: 1008 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16800 Color: 0
Size: 1632 Color: 1
Size: 1216 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16904 Color: 0
Size: 1408 Color: 1
Size: 1336 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16884 Color: 1
Size: 1420 Color: 3
Size: 1344 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 17008 Color: 0
Size: 1536 Color: 2
Size: 1104 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 17012 Color: 0
Size: 1676 Color: 3
Size: 960 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16978 Color: 1
Size: 1702 Color: 2
Size: 968 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 17140 Color: 0
Size: 1916 Color: 3
Size: 592 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17184 Color: 0
Size: 1632 Color: 1
Size: 832 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17136 Color: 1
Size: 2080 Color: 0
Size: 432 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17196 Color: 1
Size: 1744 Color: 4
Size: 708 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17226 Color: 0
Size: 1422 Color: 3
Size: 1000 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17264 Color: 3
Size: 2000 Color: 0
Size: 384 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17326 Color: 2
Size: 1946 Color: 0
Size: 376 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17356 Color: 3
Size: 1600 Color: 3
Size: 692 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17388 Color: 4
Size: 2068 Color: 0
Size: 192 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17418 Color: 1
Size: 1718 Color: 0
Size: 512 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17478 Color: 3
Size: 1418 Color: 0
Size: 752 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17504 Color: 1
Size: 1768 Color: 3
Size: 376 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 17544 Color: 1
Size: 1418 Color: 4
Size: 686 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 17616 Color: 1
Size: 1136 Color: 4
Size: 896 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 17596 Color: 3
Size: 1636 Color: 2
Size: 416 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 17622 Color: 2
Size: 1408 Color: 0
Size: 618 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 17652 Color: 2
Size: 1404 Color: 0
Size: 592 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 17680 Color: 1
Size: 1280 Color: 0
Size: 688 Color: 3

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12472 Color: 0
Size: 6493 Color: 4
Size: 682 Color: 1

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 0
Size: 5007 Color: 0
Size: 392 Color: 3

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 14480 Color: 3
Size: 3855 Color: 2
Size: 1312 Color: 2

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 14519 Color: 0
Size: 4512 Color: 2
Size: 616 Color: 1

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 14940 Color: 2
Size: 4275 Color: 0
Size: 432 Color: 3

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 15023 Color: 1
Size: 4368 Color: 0
Size: 256 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 15520 Color: 0
Size: 3775 Color: 2
Size: 352 Color: 3

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 15559 Color: 0
Size: 3880 Color: 0
Size: 208 Color: 3

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 15527 Color: 2
Size: 2960 Color: 0
Size: 1160 Color: 2

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 15551 Color: 3
Size: 4096 Color: 4

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 15878 Color: 4
Size: 2393 Color: 2
Size: 1376 Color: 3

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 16453 Color: 1
Size: 2378 Color: 0
Size: 816 Color: 4

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 16461 Color: 1
Size: 1884 Color: 0
Size: 1302 Color: 2

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 16777 Color: 1
Size: 2632 Color: 2
Size: 238 Color: 3

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 16785 Color: 0
Size: 1870 Color: 2
Size: 992 Color: 1

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 10848 Color: 0
Size: 8176 Color: 4
Size: 622 Color: 3

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 11596 Color: 3
Size: 7026 Color: 1
Size: 1024 Color: 3

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 11628 Color: 4
Size: 7122 Color: 0
Size: 896 Color: 4

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 12670 Color: 0
Size: 6640 Color: 1
Size: 336 Color: 3

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13568 Color: 4
Size: 3670 Color: 0
Size: 2408 Color: 1

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 15246 Color: 1
Size: 3992 Color: 3
Size: 408 Color: 4

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 15382 Color: 3
Size: 2528 Color: 2
Size: 1736 Color: 2

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 15558 Color: 1
Size: 2226 Color: 0
Size: 1862 Color: 2

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 16236 Color: 3
Size: 3410 Color: 4

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 16258 Color: 4
Size: 3048 Color: 0
Size: 340 Color: 2

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 16504 Color: 2
Size: 3142 Color: 1

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 16510 Color: 2
Size: 2278 Color: 0
Size: 858 Color: 1

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 17144 Color: 1
Size: 2502 Color: 4

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 17274 Color: 4
Size: 2308 Color: 3
Size: 64 Color: 2

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 17590 Color: 3
Size: 2008 Color: 1
Size: 48 Color: 1

Bin 89: 3 of cap free
Amount of items: 11
Items: 
Size: 9825 Color: 3
Size: 1414 Color: 2
Size: 1392 Color: 2
Size: 1056 Color: 2
Size: 1056 Color: 2
Size: 1024 Color: 4
Size: 1006 Color: 3
Size: 854 Color: 1
Size: 784 Color: 1
Size: 754 Color: 4
Size: 480 Color: 1

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 11680 Color: 0
Size: 7073 Color: 2
Size: 892 Color: 1

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 14228 Color: 3
Size: 5033 Color: 4
Size: 384 Color: 2

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 14527 Color: 3
Size: 4490 Color: 2
Size: 628 Color: 1

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 14864 Color: 2
Size: 4269 Color: 4
Size: 512 Color: 3

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 16788 Color: 4
Size: 2681 Color: 2
Size: 176 Color: 4

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 11137 Color: 2
Size: 7093 Color: 1
Size: 1414 Color: 0

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 11996 Color: 0
Size: 7392 Color: 3
Size: 256 Color: 3

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 2
Size: 6896 Color: 0
Size: 416 Color: 3

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 13168 Color: 2
Size: 6112 Color: 1
Size: 364 Color: 2

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 13578 Color: 1
Size: 5296 Color: 3
Size: 770 Color: 2

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 2
Size: 5236 Color: 3
Size: 580 Color: 4

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 14304 Color: 0
Size: 4884 Color: 2
Size: 456 Color: 4

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 4
Size: 4496 Color: 3
Size: 464 Color: 1

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 16012 Color: 3
Size: 3632 Color: 4

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 16274 Color: 2
Size: 3018 Color: 0
Size: 352 Color: 2

Bin 105: 4 of cap free
Amount of items: 2
Items: 
Size: 16533 Color: 1
Size: 3111 Color: 2

Bin 106: 4 of cap free
Amount of items: 2
Items: 
Size: 17404 Color: 2
Size: 2240 Color: 4

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 17516 Color: 0
Size: 2096 Color: 2
Size: 32 Color: 2

Bin 108: 5 of cap free
Amount of items: 2
Items: 
Size: 15119 Color: 0
Size: 4524 Color: 4

Bin 109: 5 of cap free
Amount of items: 2
Items: 
Size: 15947 Color: 4
Size: 3696 Color: 1

Bin 110: 6 of cap free
Amount of items: 9
Items: 
Size: 9826 Color: 2
Size: 1768 Color: 3
Size: 1632 Color: 4
Size: 1632 Color: 2
Size: 1632 Color: 0
Size: 1200 Color: 0
Size: 1040 Color: 1
Size: 576 Color: 4
Size: 336 Color: 1

Bin 111: 6 of cap free
Amount of items: 4
Items: 
Size: 11105 Color: 1
Size: 7121 Color: 0
Size: 1040 Color: 3
Size: 376 Color: 4

Bin 112: 6 of cap free
Amount of items: 3
Items: 
Size: 11857 Color: 3
Size: 7081 Color: 1
Size: 704 Color: 4

Bin 113: 6 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 0
Size: 5818 Color: 2
Size: 452 Color: 1

Bin 114: 6 of cap free
Amount of items: 3
Items: 
Size: 17672 Color: 1
Size: 1938 Color: 4
Size: 32 Color: 2

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 11153 Color: 2
Size: 8168 Color: 0
Size: 320 Color: 3

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 11833 Color: 4
Size: 7808 Color: 2

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 12805 Color: 0
Size: 5200 Color: 0
Size: 1636 Color: 3

Bin 118: 7 of cap free
Amount of items: 3
Items: 
Size: 13641 Color: 2
Size: 5424 Color: 3
Size: 576 Color: 1

Bin 119: 8 of cap free
Amount of items: 4
Items: 
Size: 9904 Color: 1
Size: 8182 Color: 2
Size: 1024 Color: 3
Size: 530 Color: 4

Bin 120: 8 of cap free
Amount of items: 3
Items: 
Size: 12996 Color: 2
Size: 6380 Color: 0
Size: 264 Color: 1

Bin 121: 8 of cap free
Amount of items: 3
Items: 
Size: 15102 Color: 4
Size: 4004 Color: 0
Size: 534 Color: 3

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 16231 Color: 4
Size: 3409 Color: 3

Bin 123: 8 of cap free
Amount of items: 3
Items: 
Size: 17488 Color: 1
Size: 2088 Color: 3
Size: 64 Color: 1

Bin 124: 9 of cap free
Amount of items: 3
Items: 
Size: 13234 Color: 0
Size: 5989 Color: 1
Size: 416 Color: 3

Bin 125: 9 of cap free
Amount of items: 2
Items: 
Size: 13956 Color: 2
Size: 5683 Color: 0

Bin 126: 9 of cap free
Amount of items: 3
Items: 
Size: 14416 Color: 3
Size: 3415 Color: 1
Size: 1808 Color: 4

Bin 127: 9 of cap free
Amount of items: 3
Items: 
Size: 16745 Color: 1
Size: 2618 Color: 2
Size: 276 Color: 0

Bin 128: 10 of cap free
Amount of items: 29
Items: 
Size: 896 Color: 4
Size: 896 Color: 4
Size: 864 Color: 0
Size: 864 Color: 0
Size: 854 Color: 3
Size: 852 Color: 0
Size: 824 Color: 4
Size: 784 Color: 2
Size: 768 Color: 3
Size: 768 Color: 3
Size: 756 Color: 3
Size: 732 Color: 4
Size: 704 Color: 3
Size: 704 Color: 2
Size: 704 Color: 2
Size: 680 Color: 3
Size: 640 Color: 2
Size: 622 Color: 4
Size: 608 Color: 2
Size: 608 Color: 1
Size: 576 Color: 1
Size: 576 Color: 0
Size: 568 Color: 4
Size: 528 Color: 2
Size: 520 Color: 1
Size: 496 Color: 1
Size: 478 Color: 1
Size: 448 Color: 0
Size: 320 Color: 4

Bin 129: 10 of cap free
Amount of items: 2
Items: 
Size: 16080 Color: 1
Size: 3558 Color: 2

Bin 130: 10 of cap free
Amount of items: 2
Items: 
Size: 17464 Color: 0
Size: 2174 Color: 3

Bin 131: 10 of cap free
Amount of items: 3
Items: 
Size: 17576 Color: 3
Size: 1982 Color: 4
Size: 80 Color: 3

Bin 132: 11 of cap free
Amount of items: 7
Items: 
Size: 9827 Color: 1
Size: 2204 Color: 4
Size: 1900 Color: 0
Size: 1810 Color: 3
Size: 1780 Color: 2
Size: 1184 Color: 4
Size: 932 Color: 1

Bin 133: 11 of cap free
Amount of items: 3
Items: 
Size: 15517 Color: 0
Size: 3928 Color: 4
Size: 192 Color: 3

Bin 134: 11 of cap free
Amount of items: 3
Items: 
Size: 15931 Color: 1
Size: 2922 Color: 3
Size: 784 Color: 3

Bin 135: 12 of cap free
Amount of items: 5
Items: 
Size: 9832 Color: 3
Size: 3280 Color: 1
Size: 3050 Color: 3
Size: 2176 Color: 0
Size: 1298 Color: 2

Bin 136: 12 of cap free
Amount of items: 2
Items: 
Size: 15496 Color: 1
Size: 4140 Color: 0

Bin 137: 13 of cap free
Amount of items: 3
Items: 
Size: 11440 Color: 0
Size: 7459 Color: 1
Size: 736 Color: 4

Bin 138: 14 of cap free
Amount of items: 5
Items: 
Size: 9834 Color: 3
Size: 3765 Color: 4
Size: 3435 Color: 4
Size: 1832 Color: 1
Size: 768 Color: 3

Bin 139: 14 of cap free
Amount of items: 2
Items: 
Size: 14738 Color: 1
Size: 4896 Color: 2

Bin 140: 14 of cap free
Amount of items: 2
Items: 
Size: 16146 Color: 1
Size: 3488 Color: 3

Bin 141: 14 of cap free
Amount of items: 3
Items: 
Size: 17462 Color: 2
Size: 2044 Color: 3
Size: 128 Color: 2

Bin 142: 15 of cap free
Amount of items: 3
Items: 
Size: 11169 Color: 0
Size: 8144 Color: 2
Size: 320 Color: 3

Bin 143: 16 of cap free
Amount of items: 2
Items: 
Size: 11408 Color: 2
Size: 8224 Color: 0

Bin 144: 16 of cap free
Amount of items: 2
Items: 
Size: 12944 Color: 0
Size: 6688 Color: 2

Bin 145: 16 of cap free
Amount of items: 2
Items: 
Size: 13216 Color: 4
Size: 6416 Color: 3

Bin 146: 17 of cap free
Amount of items: 3
Items: 
Size: 13303 Color: 0
Size: 4504 Color: 2
Size: 1824 Color: 4

Bin 147: 18 of cap free
Amount of items: 4
Items: 
Size: 11106 Color: 0
Size: 7084 Color: 2
Size: 1088 Color: 3
Size: 352 Color: 4

Bin 148: 20 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 1
Size: 5428 Color: 0

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 16592 Color: 3
Size: 3036 Color: 4

Bin 150: 20 of cap free
Amount of items: 2
Items: 
Size: 17536 Color: 3
Size: 2092 Color: 2

Bin 151: 22 of cap free
Amount of items: 3
Items: 
Size: 11102 Color: 2
Size: 8184 Color: 1
Size: 340 Color: 4

Bin 152: 22 of cap free
Amount of items: 2
Items: 
Size: 17606 Color: 3
Size: 2020 Color: 4

Bin 153: 23 of cap free
Amount of items: 2
Items: 
Size: 16776 Color: 4
Size: 2849 Color: 0

Bin 154: 24 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 2
Size: 6684 Color: 2
Size: 1636 Color: 1

Bin 155: 28 of cap free
Amount of items: 4
Items: 
Size: 9836 Color: 3
Size: 6968 Color: 2
Size: 2400 Color: 1
Size: 416 Color: 4

Bin 156: 28 of cap free
Amount of items: 2
Items: 
Size: 16636 Color: 4
Size: 2984 Color: 1

Bin 157: 30 of cap free
Amount of items: 2
Items: 
Size: 17042 Color: 1
Size: 2576 Color: 3

Bin 158: 32 of cap free
Amount of items: 2
Items: 
Size: 16840 Color: 4
Size: 2776 Color: 3

Bin 159: 36 of cap free
Amount of items: 2
Items: 
Size: 16798 Color: 1
Size: 2814 Color: 4

Bin 160: 36 of cap free
Amount of items: 2
Items: 
Size: 16928 Color: 2
Size: 2684 Color: 4

Bin 161: 40 of cap free
Amount of items: 2
Items: 
Size: 14262 Color: 0
Size: 5346 Color: 4

Bin 162: 44 of cap free
Amount of items: 3
Items: 
Size: 11234 Color: 1
Size: 8050 Color: 0
Size: 320 Color: 3

Bin 163: 46 of cap free
Amount of items: 2
Items: 
Size: 14054 Color: 0
Size: 5548 Color: 4

Bin 164: 46 of cap free
Amount of items: 2
Items: 
Size: 15131 Color: 2
Size: 4471 Color: 0

Bin 165: 47 of cap free
Amount of items: 2
Items: 
Size: 13088 Color: 2
Size: 6513 Color: 3

Bin 166: 47 of cap free
Amount of items: 2
Items: 
Size: 13609 Color: 4
Size: 5992 Color: 3

Bin 167: 48 of cap free
Amount of items: 2
Items: 
Size: 16478 Color: 1
Size: 3122 Color: 2

Bin 168: 49 of cap free
Amount of items: 2
Items: 
Size: 15047 Color: 2
Size: 4552 Color: 1

Bin 169: 52 of cap free
Amount of items: 2
Items: 
Size: 17372 Color: 1
Size: 2224 Color: 3

Bin 170: 60 of cap free
Amount of items: 2
Items: 
Size: 15396 Color: 0
Size: 4192 Color: 2

Bin 171: 61 of cap free
Amount of items: 2
Items: 
Size: 15907 Color: 0
Size: 3680 Color: 2

Bin 172: 71 of cap free
Amount of items: 2
Items: 
Size: 14288 Color: 4
Size: 5289 Color: 2

Bin 173: 76 of cap free
Amount of items: 2
Items: 
Size: 15478 Color: 3
Size: 4094 Color: 1

Bin 174: 77 of cap free
Amount of items: 2
Items: 
Size: 16914 Color: 2
Size: 2657 Color: 4

Bin 175: 81 of cap free
Amount of items: 3
Items: 
Size: 12829 Color: 4
Size: 3478 Color: 1
Size: 3260 Color: 4

Bin 176: 91 of cap free
Amount of items: 2
Items: 
Size: 14495 Color: 1
Size: 5062 Color: 0

Bin 177: 93 of cap free
Amount of items: 2
Items: 
Size: 16436 Color: 3
Size: 3119 Color: 4

Bin 178: 96 of cap free
Amount of items: 2
Items: 
Size: 15536 Color: 0
Size: 4016 Color: 4

Bin 179: 100 of cap free
Amount of items: 2
Items: 
Size: 13448 Color: 2
Size: 6100 Color: 0

Bin 180: 103 of cap free
Amount of items: 2
Items: 
Size: 15264 Color: 0
Size: 4281 Color: 2

Bin 181: 104 of cap free
Amount of items: 2
Items: 
Size: 15000 Color: 0
Size: 4544 Color: 2

Bin 182: 110 of cap free
Amount of items: 3
Items: 
Size: 9840 Color: 3
Size: 7101 Color: 0
Size: 2597 Color: 0

Bin 183: 110 of cap free
Amount of items: 2
Items: 
Size: 15990 Color: 3
Size: 3548 Color: 2

Bin 184: 110 of cap free
Amount of items: 2
Items: 
Size: 17256 Color: 4
Size: 2282 Color: 3

Bin 185: 114 of cap free
Amount of items: 2
Items: 
Size: 14872 Color: 1
Size: 4662 Color: 2

Bin 186: 116 of cap free
Amount of items: 2
Items: 
Size: 16433 Color: 3
Size: 3099 Color: 1

Bin 187: 130 of cap free
Amount of items: 2
Items: 
Size: 15728 Color: 4
Size: 3790 Color: 3

Bin 188: 132 of cap free
Amount of items: 2
Items: 
Size: 11696 Color: 3
Size: 7820 Color: 4

Bin 189: 192 of cap free
Amount of items: 3
Items: 
Size: 11161 Color: 1
Size: 4852 Color: 3
Size: 3443 Color: 0

Bin 190: 196 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 3
Size: 8180 Color: 1
Size: 1424 Color: 2

Bin 191: 220 of cap free
Amount of items: 2
Items: 
Size: 11240 Color: 1
Size: 8188 Color: 3

Bin 192: 224 of cap free
Amount of items: 2
Items: 
Size: 12408 Color: 2
Size: 7016 Color: 3

Bin 193: 224 of cap free
Amount of items: 2
Items: 
Size: 13384 Color: 3
Size: 6040 Color: 4

Bin 194: 243 of cap free
Amount of items: 2
Items: 
Size: 11218 Color: 2
Size: 8187 Color: 1

Bin 195: 245 of cap free
Amount of items: 3
Items: 
Size: 11129 Color: 1
Size: 7122 Color: 4
Size: 1152 Color: 3

Bin 196: 287 of cap free
Amount of items: 3
Items: 
Size: 9888 Color: 1
Size: 7052 Color: 0
Size: 2421 Color: 0

Bin 197: 298 of cap free
Amount of items: 2
Items: 
Size: 11164 Color: 4
Size: 8186 Color: 2

Bin 198: 307 of cap free
Amount of items: 2
Items: 
Size: 11156 Color: 0
Size: 8185 Color: 4

Bin 199: 14094 of cap free
Amount of items: 13
Items: 
Size: 518 Color: 2
Size: 496 Color: 3
Size: 476 Color: 4
Size: 472 Color: 1
Size: 472 Color: 1
Size: 448 Color: 3
Size: 448 Color: 0
Size: 384 Color: 3
Size: 384 Color: 2
Size: 384 Color: 0
Size: 384 Color: 0
Size: 372 Color: 1
Size: 316 Color: 4

Total size: 3890304
Total free space: 19648

