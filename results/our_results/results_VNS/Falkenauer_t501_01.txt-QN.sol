Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 420
Size: 295 Color: 231
Size: 270 Color: 135

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 421
Size: 309 Color: 266
Size: 256 Color: 56

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 413
Size: 297 Color: 240
Size: 272 Color: 146

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 445
Size: 294 Color: 230
Size: 251 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 476
Size: 263 Color: 99
Size: 260 Color: 79

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 379
Size: 300 Color: 248
Size: 296 Color: 232

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 349
Size: 323 Color: 290
Size: 307 Color: 263

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 409
Size: 324 Color: 292
Size: 250 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 491
Size: 261 Color: 81
Size: 251 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 353
Size: 371 Color: 350
Size: 255 Color: 48

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 363
Size: 362 Color: 342
Size: 251 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 428
Size: 286 Color: 209
Size: 271 Color: 140

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 492
Size: 259 Color: 75
Size: 251 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 412
Size: 296 Color: 237
Size: 276 Color: 162

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 422
Size: 305 Color: 259
Size: 258 Color: 74

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 485
Size: 264 Color: 106
Size: 250 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 386
Size: 339 Color: 310
Size: 254 Color: 38

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 472
Size: 272 Color: 145
Size: 253 Color: 30

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 341
Size: 360 Color: 336
Size: 279 Color: 176

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 498
Size: 253 Color: 34
Size: 252 Color: 26

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 366
Size: 351 Color: 318
Size: 258 Color: 73

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 406
Size: 292 Color: 227
Size: 284 Color: 199

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 360
Size: 331 Color: 303
Size: 283 Color: 196

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 387
Size: 331 Color: 302
Size: 258 Color: 68

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 365
Size: 330 Color: 301
Size: 280 Color: 183

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 432
Size: 281 Color: 186
Size: 274 Color: 154

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 407
Size: 290 Color: 223
Size: 286 Color: 211

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 451
Size: 283 Color: 197
Size: 257 Color: 62

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 337
Size: 356 Color: 327
Size: 284 Color: 198

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 443
Size: 286 Color: 213
Size: 261 Color: 83

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 463
Size: 277 Color: 165
Size: 255 Color: 52

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 383
Size: 321 Color: 286
Size: 272 Color: 144

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 440
Size: 289 Color: 220
Size: 260 Color: 80

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 435
Size: 298 Color: 241
Size: 254 Color: 42

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 361
Size: 313 Color: 272
Size: 301 Color: 250

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 448
Size: 285 Color: 204
Size: 258 Color: 69

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 369
Size: 327 Color: 296
Size: 280 Color: 181

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 473
Size: 271 Color: 138
Size: 253 Color: 35

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 454
Size: 278 Color: 173
Size: 261 Color: 91

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 436
Size: 279 Color: 177
Size: 272 Color: 149

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 487
Size: 258 Color: 66
Size: 255 Color: 50

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 457
Size: 282 Color: 194
Size: 255 Color: 53

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 464
Size: 275 Color: 157
Size: 256 Color: 54

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 319
Size: 345 Color: 313
Size: 303 Color: 258

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 354
Size: 321 Color: 284
Size: 305 Color: 260

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 470
Size: 274 Color: 156
Size: 251 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 370
Size: 312 Color: 271
Size: 293 Color: 229

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 346
Size: 353 Color: 323
Size: 280 Color: 184

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 419
Size: 299 Color: 246
Size: 266 Color: 117

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 340
Size: 355 Color: 326
Size: 285 Color: 205

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 325
Size: 353 Color: 324
Size: 293 Color: 228

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 393
Size: 312 Color: 270
Size: 273 Color: 152

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 367
Size: 327 Color: 297
Size: 281 Color: 189

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 402
Size: 298 Color: 244
Size: 278 Color: 172

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 477
Size: 262 Color: 94
Size: 257 Color: 65

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 461
Size: 281 Color: 185
Size: 252 Color: 23

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 466
Size: 270 Color: 132
Size: 258 Color: 70

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 495
Size: 256 Color: 55
Size: 251 Color: 9

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 394
Size: 314 Color: 273
Size: 270 Color: 134

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 405
Size: 310 Color: 267
Size: 266 Color: 116

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 450
Size: 276 Color: 159
Size: 265 Color: 113

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 483
Size: 261 Color: 88
Size: 254 Color: 40

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 480
Size: 267 Color: 125
Size: 251 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 415
Size: 297 Color: 239
Size: 271 Color: 141

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 460
Size: 282 Color: 190
Size: 252 Color: 22

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 490
Size: 260 Color: 77
Size: 252 Color: 24

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 320
Size: 350 Color: 316
Size: 298 Color: 242

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 500
Size: 252 Color: 19
Size: 250 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 357
Size: 359 Color: 331
Size: 257 Color: 58

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 381
Size: 321 Color: 285
Size: 273 Color: 150

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 418
Size: 287 Color: 216
Size: 279 Color: 174

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 414
Size: 307 Color: 265
Size: 261 Color: 84

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 499
Size: 252 Color: 29
Size: 252 Color: 17

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 376
Size: 322 Color: 288
Size: 277 Color: 167

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 425
Size: 299 Color: 247
Size: 261 Color: 85

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 351
Size: 360 Color: 338
Size: 269 Color: 127

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 497
Size: 254 Color: 43
Size: 252 Color: 25

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 459
Size: 278 Color: 169
Size: 257 Color: 60

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 364
Size: 329 Color: 298
Size: 282 Color: 191

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 479
Size: 261 Color: 86
Size: 257 Color: 63

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 408
Size: 312 Color: 268
Size: 262 Color: 98

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 378
Size: 300 Color: 249
Size: 296 Color: 235

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 423
Size: 289 Color: 221
Size: 271 Color: 139

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 390
Size: 336 Color: 308
Size: 251 Color: 11

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 404
Size: 319 Color: 279
Size: 257 Color: 64

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 398
Size: 306 Color: 261
Size: 275 Color: 158

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 388
Size: 318 Color: 278
Size: 271 Color: 142

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 335
Size: 359 Color: 333
Size: 281 Color: 187

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 444
Size: 291 Color: 225
Size: 254 Color: 37

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 358
Size: 329 Color: 300
Size: 286 Color: 208

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 467
Size: 273 Color: 151
Size: 253 Color: 33

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 478
Size: 267 Color: 122
Size: 251 Color: 14

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 488
Size: 261 Color: 87
Size: 251 Color: 15

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 469
Size: 271 Color: 143
Size: 254 Color: 45

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 475
Size: 272 Color: 147
Size: 252 Color: 28

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 458
Size: 274 Color: 155
Size: 262 Color: 97

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 411
Size: 320 Color: 282
Size: 252 Color: 18

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 334
Size: 359 Color: 332
Size: 282 Color: 193

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 392
Size: 323 Color: 289
Size: 263 Color: 101

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 399
Size: 302 Color: 254
Size: 276 Color: 160

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 400
Size: 316 Color: 277
Size: 262 Color: 96

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 372
Size: 326 Color: 295
Size: 278 Color: 170

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 494
Size: 255 Color: 47
Size: 254 Color: 41

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 352
Size: 341 Color: 312
Size: 286 Color: 210

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 455
Size: 271 Color: 137
Size: 267 Color: 121

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 347
Size: 360 Color: 339
Size: 271 Color: 136

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 368
Size: 323 Color: 291
Size: 285 Color: 206

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 356
Size: 353 Color: 322
Size: 264 Color: 102

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 417
Size: 286 Color: 207
Size: 280 Color: 180

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 344
Size: 347 Color: 314
Size: 288 Color: 218

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 471
Size: 264 Color: 104
Size: 261 Color: 89

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 441
Size: 284 Color: 202
Size: 264 Color: 112

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 439
Size: 282 Color: 192
Size: 267 Color: 120

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 317
Size: 350 Color: 315
Size: 299 Color: 245

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 453
Size: 270 Color: 133
Size: 269 Color: 129

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 496
Size: 254 Color: 39
Size: 252 Color: 20

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 403
Size: 307 Color: 264
Size: 269 Color: 130

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 375
Size: 314 Color: 274
Size: 288 Color: 217

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 330
Size: 353 Color: 321
Size: 289 Color: 219

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 355
Size: 332 Color: 305
Size: 290 Color: 222

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 482
Size: 264 Color: 109
Size: 251 Color: 8

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 427
Size: 303 Color: 256
Size: 255 Color: 46

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 442
Size: 279 Color: 178
Size: 268 Color: 126

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 382
Size: 321 Color: 287
Size: 272 Color: 148

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 373
Size: 319 Color: 281
Size: 284 Color: 200

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 385
Size: 340 Color: 311
Size: 253 Color: 31

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 430
Size: 307 Color: 262
Size: 250 Color: 3

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 449
Size: 278 Color: 168
Size: 264 Color: 103

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 456
Size: 274 Color: 153
Size: 264 Color: 111

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 438
Size: 280 Color: 182
Size: 269 Color: 128

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 329
Size: 357 Color: 328
Size: 286 Color: 212

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 462
Size: 276 Color: 161
Size: 257 Color: 57

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 377
Size: 319 Color: 280
Size: 277 Color: 166

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 481
Size: 262 Color: 93
Size: 255 Color: 49

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 452
Size: 276 Color: 163
Size: 263 Color: 100

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 486
Size: 264 Color: 107
Size: 250 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 401
Size: 298 Color: 243
Size: 279 Color: 179

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 374
Size: 325 Color: 293
Size: 277 Color: 164

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 465
Size: 267 Color: 119
Size: 264 Color: 105

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 389
Size: 337 Color: 309
Size: 252 Color: 21

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 410
Size: 312 Color: 269
Size: 262 Color: 95

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 484
Size: 261 Color: 82
Size: 254 Color: 36

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 424
Size: 302 Color: 253
Size: 258 Color: 72

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 391
Size: 320 Color: 283
Size: 267 Color: 124

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 433
Size: 296 Color: 233
Size: 257 Color: 61

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 468
Size: 264 Color: 108
Size: 262 Color: 92

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 489
Size: 258 Color: 71
Size: 254 Color: 44

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 345
Size: 364 Color: 343
Size: 270 Color: 131

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 397
Size: 296 Color: 236
Size: 285 Color: 203

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 493
Size: 258 Color: 67
Size: 252 Color: 27

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 359
Size: 329 Color: 299
Size: 286 Color: 214

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 395
Size: 302 Color: 255
Size: 281 Color: 188

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 429
Size: 291 Color: 226
Size: 266 Color: 118

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 384
Size: 302 Color: 252
Size: 291 Color: 224

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 446
Size: 284 Color: 201
Size: 261 Color: 90

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 371
Size: 326 Color: 294
Size: 278 Color: 171

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 416
Size: 315 Color: 275
Size: 252 Color: 16

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 474
Size: 264 Color: 110
Size: 260 Color: 76

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 447
Size: 279 Color: 175
Size: 265 Color: 114

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 396
Size: 316 Color: 276
Size: 267 Color: 123

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 426
Size: 301 Color: 251
Size: 257 Color: 59

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 380
Size: 335 Color: 307
Size: 260 Color: 78

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 434
Size: 287 Color: 215
Size: 266 Color: 115

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 431
Size: 303 Color: 257
Size: 253 Color: 32

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 348
Size: 334 Color: 306
Size: 296 Color: 234

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 437
Size: 296 Color: 238
Size: 255 Color: 51

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 362
Size: 331 Color: 304
Size: 283 Color: 195

Total size: 167000
Total free space: 0

