Capicity Bin: 14688
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 7352 Color: 0
Size: 2408 Color: 7
Size: 2323 Color: 6
Size: 1925 Color: 15
Size: 680 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 7354 Color: 16
Size: 6114 Color: 0
Size: 1220 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7896 Color: 18
Size: 6448 Color: 11
Size: 344 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10165 Color: 6
Size: 3833 Color: 6
Size: 690 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 18
Size: 3336 Color: 5
Size: 1120 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10523 Color: 18
Size: 3399 Color: 11
Size: 766 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10596 Color: 15
Size: 3784 Color: 16
Size: 308 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10639 Color: 12
Size: 3371 Color: 15
Size: 678 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10708 Color: 0
Size: 3364 Color: 16
Size: 616 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10782 Color: 15
Size: 3324 Color: 10
Size: 582 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10991 Color: 7
Size: 2921 Color: 13
Size: 776 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11225 Color: 6
Size: 2415 Color: 15
Size: 1048 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11494 Color: 9
Size: 2924 Color: 10
Size: 270 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11575 Color: 16
Size: 2595 Color: 10
Size: 518 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11580 Color: 3
Size: 2596 Color: 7
Size: 512 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11582 Color: 0
Size: 2278 Color: 15
Size: 828 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11670 Color: 0
Size: 2498 Color: 15
Size: 520 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11744 Color: 16
Size: 2168 Color: 13
Size: 776 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11878 Color: 13
Size: 2662 Color: 19
Size: 148 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11892 Color: 19
Size: 1764 Color: 11
Size: 1032 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 4
Size: 2388 Color: 15
Size: 400 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12053 Color: 0
Size: 2181 Color: 3
Size: 454 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 15
Size: 1928 Color: 7
Size: 656 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12113 Color: 5
Size: 2147 Color: 13
Size: 428 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12124 Color: 0
Size: 1924 Color: 12
Size: 640 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12156 Color: 15
Size: 2140 Color: 8
Size: 392 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12194 Color: 9
Size: 2082 Color: 2
Size: 412 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12201 Color: 19
Size: 2073 Color: 0
Size: 414 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12218 Color: 4
Size: 2062 Color: 19
Size: 408 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12289 Color: 5
Size: 2067 Color: 14
Size: 332 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12294 Color: 17
Size: 1730 Color: 18
Size: 664 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12409 Color: 15
Size: 1431 Color: 7
Size: 848 Color: 9

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 14
Size: 1322 Color: 4
Size: 990 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12430 Color: 15
Size: 1998 Color: 18
Size: 260 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 13
Size: 1882 Color: 9
Size: 386 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 9
Size: 1420 Color: 11
Size: 812 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12537 Color: 15
Size: 2081 Color: 4
Size: 70 Color: 10

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12494 Color: 11
Size: 1442 Color: 5
Size: 752 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12529 Color: 12
Size: 1431 Color: 7
Size: 728 Color: 12

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 10
Size: 1864 Color: 14
Size: 272 Color: 19

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12652 Color: 15
Size: 1332 Color: 3
Size: 704 Color: 8

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12614 Color: 7
Size: 1130 Color: 18
Size: 944 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 7
Size: 1216 Color: 0
Size: 856 Color: 19

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12685 Color: 15
Size: 1335 Color: 8
Size: 668 Color: 9

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12661 Color: 0
Size: 1503 Color: 14
Size: 524 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12666 Color: 12
Size: 1686 Color: 10
Size: 336 Color: 7

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 15
Size: 1528 Color: 18
Size: 384 Color: 12

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12755 Color: 3
Size: 1405 Color: 4
Size: 528 Color: 19

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12840 Color: 19
Size: 1272 Color: 8
Size: 576 Color: 10

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 15
Size: 1336 Color: 4
Size: 396 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12968 Color: 4
Size: 1444 Color: 10
Size: 276 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12973 Color: 9
Size: 1357 Color: 1
Size: 358 Color: 18

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 9
Size: 1444 Color: 15
Size: 268 Color: 5

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13021 Color: 19
Size: 1443 Color: 11
Size: 224 Color: 11

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13026 Color: 10
Size: 1346 Color: 11
Size: 316 Color: 15

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13069 Color: 9
Size: 1351 Color: 5
Size: 268 Color: 11

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13074 Color: 12
Size: 912 Color: 18
Size: 702 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13130 Color: 15
Size: 1222 Color: 8
Size: 336 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13095 Color: 14
Size: 1329 Color: 18
Size: 264 Color: 5

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 14
Size: 1176 Color: 19
Size: 416 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13139 Color: 15
Size: 1291 Color: 12
Size: 258 Color: 17

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 6
Size: 928 Color: 14
Size: 584 Color: 8

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 3
Size: 958 Color: 17
Size: 550 Color: 15

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 7729 Color: 3
Size: 6750 Color: 15
Size: 208 Color: 14

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 10467 Color: 10
Size: 3916 Color: 17
Size: 304 Color: 18

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 10611 Color: 2
Size: 3876 Color: 6
Size: 200 Color: 16

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11050 Color: 17
Size: 3349 Color: 15
Size: 288 Color: 3

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 11480 Color: 1
Size: 2759 Color: 4
Size: 448 Color: 13

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 11572 Color: 12
Size: 3115 Color: 10

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 11745 Color: 14
Size: 2600 Color: 8
Size: 342 Color: 15

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 11800 Color: 13
Size: 2887 Color: 4

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12025 Color: 11
Size: 2342 Color: 19
Size: 320 Color: 3

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 12193 Color: 6
Size: 2116 Color: 10
Size: 378 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 12209 Color: 16
Size: 2156 Color: 5
Size: 322 Color: 13

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 12363 Color: 15
Size: 1428 Color: 16
Size: 896 Color: 2

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 12379 Color: 2
Size: 1892 Color: 12
Size: 416 Color: 7

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 12609 Color: 11
Size: 1784 Color: 17
Size: 294 Color: 11

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 12779 Color: 7
Size: 1908 Color: 13

Bin 79: 1 of cap free
Amount of items: 2
Items: 
Size: 12852 Color: 10
Size: 1835 Color: 14

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 12923 Color: 11
Size: 1448 Color: 17
Size: 316 Color: 6

Bin 81: 1 of cap free
Amount of items: 2
Items: 
Size: 12951 Color: 7
Size: 1736 Color: 8

Bin 82: 2 of cap free
Amount of items: 5
Items: 
Size: 7356 Color: 0
Size: 3646 Color: 2
Size: 3144 Color: 7
Size: 272 Color: 19
Size: 268 Color: 17

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 1
Size: 5166 Color: 5
Size: 248 Color: 2

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 9734 Color: 8
Size: 4514 Color: 13
Size: 438 Color: 12

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 10543 Color: 16
Size: 3375 Color: 3
Size: 768 Color: 0

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 10824 Color: 18
Size: 2616 Color: 2
Size: 1246 Color: 1

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 12885 Color: 17
Size: 1801 Color: 11

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 13058 Color: 5
Size: 1532 Color: 14
Size: 96 Color: 1

Bin 89: 3 of cap free
Amount of items: 9
Items: 
Size: 7345 Color: 10
Size: 1216 Color: 19
Size: 1184 Color: 11
Size: 1110 Color: 19
Size: 1048 Color: 1
Size: 928 Color: 15
Size: 842 Color: 0
Size: 724 Color: 9
Size: 288 Color: 2

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 8285 Color: 11
Size: 6120 Color: 12
Size: 280 Color: 3

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 8388 Color: 13
Size: 5829 Color: 3
Size: 468 Color: 5

Bin 92: 3 of cap free
Amount of items: 2
Items: 
Size: 9210 Color: 19
Size: 5475 Color: 3

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 10152 Color: 5
Size: 4149 Color: 7
Size: 384 Color: 11

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 10314 Color: 14
Size: 4211 Color: 17
Size: 160 Color: 12

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 10643 Color: 13
Size: 3794 Color: 15
Size: 248 Color: 2

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 17
Size: 3081 Color: 16
Size: 416 Color: 4

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 11753 Color: 7
Size: 2932 Color: 9

Bin 98: 3 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 8
Size: 1545 Color: 1

Bin 99: 4 of cap free
Amount of items: 7
Items: 
Size: 7348 Color: 17
Size: 1530 Color: 7
Size: 1404 Color: 10
Size: 1402 Color: 19
Size: 1362 Color: 17
Size: 1314 Color: 18
Size: 324 Color: 11

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 8936 Color: 15
Size: 5258 Color: 16
Size: 490 Color: 19

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 9956 Color: 8
Size: 4728 Color: 18

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 11542 Color: 13
Size: 2862 Color: 6
Size: 280 Color: 2

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 12724 Color: 8
Size: 1960 Color: 13

Bin 104: 4 of cap free
Amount of items: 2
Items: 
Size: 12838 Color: 12
Size: 1846 Color: 0

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 2
Size: 1216 Color: 15
Size: 624 Color: 1

Bin 106: 4 of cap free
Amount of items: 2
Items: 
Size: 12854 Color: 13
Size: 1830 Color: 16

Bin 107: 4 of cap free
Amount of items: 2
Items: 
Size: 12980 Color: 13
Size: 1704 Color: 2

Bin 108: 5 of cap free
Amount of items: 7
Items: 
Size: 7349 Color: 7
Size: 1852 Color: 15
Size: 1788 Color: 3
Size: 1598 Color: 15
Size: 928 Color: 2
Size: 656 Color: 6
Size: 512 Color: 10

Bin 109: 5 of cap free
Amount of items: 2
Items: 
Size: 8739 Color: 6
Size: 5944 Color: 11

Bin 110: 5 of cap free
Amount of items: 3
Items: 
Size: 8935 Color: 3
Size: 5420 Color: 15
Size: 328 Color: 5

Bin 111: 5 of cap free
Amount of items: 2
Items: 
Size: 12782 Color: 1
Size: 1901 Color: 9

Bin 112: 5 of cap free
Amount of items: 2
Items: 
Size: 13012 Color: 13
Size: 1671 Color: 7

Bin 113: 5 of cap free
Amount of items: 2
Items: 
Size: 13061 Color: 5
Size: 1622 Color: 13

Bin 114: 5 of cap free
Amount of items: 2
Items: 
Size: 13092 Color: 13
Size: 1591 Color: 7

Bin 115: 5 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 1
Size: 1471 Color: 16

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 9996 Color: 6
Size: 4130 Color: 8
Size: 556 Color: 17

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 7
Size: 1260 Color: 13
Size: 648 Color: 15

Bin 118: 7 of cap free
Amount of items: 2
Items: 
Size: 12392 Color: 2
Size: 2289 Color: 1

Bin 119: 7 of cap free
Amount of items: 2
Items: 
Size: 12742 Color: 7
Size: 1939 Color: 14

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 11258 Color: 13
Size: 3422 Color: 12

Bin 121: 9 of cap free
Amount of items: 2
Items: 
Size: 9118 Color: 8
Size: 5561 Color: 4

Bin 122: 9 of cap free
Amount of items: 2
Items: 
Size: 10671 Color: 3
Size: 4008 Color: 10

Bin 123: 9 of cap free
Amount of items: 2
Items: 
Size: 13068 Color: 2
Size: 1611 Color: 0

Bin 124: 10 of cap free
Amount of items: 3
Items: 
Size: 9810 Color: 16
Size: 4500 Color: 17
Size: 368 Color: 15

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 10138 Color: 16
Size: 4540 Color: 0

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 11560 Color: 0
Size: 3118 Color: 7

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 11998 Color: 11
Size: 2680 Color: 16

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 12073 Color: 11
Size: 2604 Color: 19

Bin 129: 11 of cap free
Amount of items: 2
Items: 
Size: 12964 Color: 19
Size: 1713 Color: 9

Bin 130: 11 of cap free
Amount of items: 2
Items: 
Size: 13087 Color: 6
Size: 1590 Color: 1

Bin 131: 12 of cap free
Amount of items: 3
Items: 
Size: 8382 Color: 11
Size: 5672 Color: 6
Size: 622 Color: 1

Bin 132: 12 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 4
Size: 3900 Color: 6
Size: 784 Color: 0

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 11774 Color: 1
Size: 2902 Color: 5

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 12708 Color: 8
Size: 1967 Color: 11

Bin 135: 15 of cap free
Amount of items: 3
Items: 
Size: 10012 Color: 10
Size: 3967 Color: 2
Size: 694 Color: 19

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 12553 Color: 4
Size: 2120 Color: 0

Bin 137: 16 of cap free
Amount of items: 3
Items: 
Size: 11118 Color: 14
Size: 2622 Color: 3
Size: 932 Color: 2

Bin 138: 17 of cap free
Amount of items: 2
Items: 
Size: 10951 Color: 17
Size: 3720 Color: 11

Bin 139: 17 of cap free
Amount of items: 3
Items: 
Size: 11615 Color: 18
Size: 2936 Color: 6
Size: 120 Color: 2

Bin 140: 17 of cap free
Amount of items: 2
Items: 
Size: 12153 Color: 18
Size: 2518 Color: 8

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 12474 Color: 4
Size: 2197 Color: 12

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 9711 Color: 10
Size: 4959 Color: 9

Bin 143: 19 of cap free
Amount of items: 2
Items: 
Size: 12108 Color: 6
Size: 2561 Color: 0

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 11943 Color: 12
Size: 2725 Color: 19

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 11901 Color: 0
Size: 2766 Color: 5

Bin 146: 22 of cap free
Amount of items: 3
Items: 
Size: 8017 Color: 19
Size: 6117 Color: 2
Size: 532 Color: 13

Bin 147: 22 of cap free
Amount of items: 3
Items: 
Size: 9032 Color: 8
Size: 4642 Color: 2
Size: 992 Color: 0

Bin 148: 23 of cap free
Amount of items: 2
Items: 
Size: 12872 Color: 16
Size: 1793 Color: 2

Bin 149: 26 of cap free
Amount of items: 2
Items: 
Size: 13010 Color: 11
Size: 1652 Color: 8

Bin 150: 27 of cap free
Amount of items: 2
Items: 
Size: 12329 Color: 19
Size: 2332 Color: 1

Bin 151: 29 of cap free
Amount of items: 2
Items: 
Size: 9635 Color: 5
Size: 5024 Color: 11

Bin 152: 29 of cap free
Amount of items: 2
Items: 
Size: 12468 Color: 0
Size: 2191 Color: 5

Bin 153: 30 of cap free
Amount of items: 2
Items: 
Size: 13114 Color: 0
Size: 1544 Color: 9

Bin 154: 33 of cap free
Amount of items: 2
Items: 
Size: 10089 Color: 5
Size: 4566 Color: 19

Bin 155: 34 of cap free
Amount of items: 2
Items: 
Size: 13198 Color: 14
Size: 1456 Color: 5

Bin 156: 35 of cap free
Amount of items: 2
Items: 
Size: 12644 Color: 0
Size: 2009 Color: 11

Bin 157: 35 of cap free
Amount of items: 2
Items: 
Size: 12962 Color: 2
Size: 1691 Color: 7

Bin 158: 37 of cap free
Amount of items: 2
Items: 
Size: 11132 Color: 13
Size: 3519 Color: 15

Bin 159: 37 of cap free
Amount of items: 2
Items: 
Size: 13109 Color: 14
Size: 1542 Color: 5

Bin 160: 40 of cap free
Amount of items: 2
Items: 
Size: 10582 Color: 13
Size: 4066 Color: 6

Bin 161: 41 of cap free
Amount of items: 2
Items: 
Size: 11176 Color: 3
Size: 3471 Color: 12

Bin 162: 41 of cap free
Amount of items: 2
Items: 
Size: 13003 Color: 13
Size: 1644 Color: 6

Bin 163: 42 of cap free
Amount of items: 2
Items: 
Size: 12404 Color: 13
Size: 2242 Color: 6

Bin 164: 42 of cap free
Amount of items: 2
Items: 
Size: 13106 Color: 13
Size: 1540 Color: 16

Bin 165: 44 of cap free
Amount of items: 2
Items: 
Size: 10696 Color: 10
Size: 3948 Color: 14

Bin 166: 45 of cap free
Amount of items: 2
Items: 
Size: 11419 Color: 2
Size: 3224 Color: 17

Bin 167: 45 of cap free
Amount of items: 2
Items: 
Size: 13194 Color: 10
Size: 1449 Color: 9

Bin 168: 46 of cap free
Amount of items: 2
Items: 
Size: 7362 Color: 0
Size: 7280 Color: 4

Bin 169: 48 of cap free
Amount of items: 2
Items: 
Size: 11185 Color: 12
Size: 3455 Color: 3

Bin 170: 51 of cap free
Amount of items: 2
Items: 
Size: 11379 Color: 7
Size: 3258 Color: 19

Bin 171: 52 of cap free
Amount of items: 3
Items: 
Size: 7560 Color: 15
Size: 6116 Color: 10
Size: 960 Color: 12

Bin 172: 55 of cap free
Amount of items: 2
Items: 
Size: 12633 Color: 17
Size: 2000 Color: 6

Bin 173: 61 of cap free
Amount of items: 2
Items: 
Size: 12835 Color: 7
Size: 1792 Color: 19

Bin 174: 64 of cap free
Amount of items: 2
Items: 
Size: 11646 Color: 0
Size: 2978 Color: 7

Bin 175: 68 of cap free
Amount of items: 2
Items: 
Size: 10333 Color: 18
Size: 4287 Color: 9

Bin 176: 74 of cap free
Amount of items: 2
Items: 
Size: 8490 Color: 10
Size: 6124 Color: 7

Bin 177: 83 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 4
Size: 2453 Color: 9

Bin 178: 84 of cap free
Amount of items: 21
Items: 
Size: 928 Color: 14
Size: 904 Color: 4
Size: 896 Color: 17
Size: 888 Color: 12
Size: 832 Color: 6
Size: 824 Color: 11
Size: 784 Color: 8
Size: 768 Color: 16
Size: 756 Color: 5
Size: 736 Color: 8
Size: 684 Color: 0
Size: 674 Color: 14
Size: 674 Color: 1
Size: 592 Color: 19
Size: 592 Color: 9
Size: 576 Color: 18
Size: 576 Color: 6
Size: 576 Color: 1
Size: 464 Color: 5
Size: 456 Color: 2
Size: 424 Color: 9

Bin 179: 84 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 4
Size: 2964 Color: 19

Bin 180: 87 of cap free
Amount of items: 3
Items: 
Size: 8420 Color: 16
Size: 4795 Color: 18
Size: 1386 Color: 2

Bin 181: 87 of cap free
Amount of items: 2
Items: 
Size: 12380 Color: 6
Size: 2221 Color: 13

Bin 182: 90 of cap free
Amount of items: 2
Items: 
Size: 8712 Color: 15
Size: 5886 Color: 4

Bin 183: 92 of cap free
Amount of items: 3
Items: 
Size: 9244 Color: 14
Size: 4808 Color: 9
Size: 544 Color: 15

Bin 184: 100 of cap free
Amount of items: 2
Items: 
Size: 10660 Color: 9
Size: 3928 Color: 6

Bin 185: 104 of cap free
Amount of items: 2
Items: 
Size: 11172 Color: 3
Size: 3412 Color: 17

Bin 186: 108 of cap free
Amount of items: 2
Items: 
Size: 9896 Color: 1
Size: 4684 Color: 16

Bin 187: 110 of cap free
Amount of items: 39
Items: 
Size: 568 Color: 18
Size: 512 Color: 8
Size: 512 Color: 7
Size: 500 Color: 4
Size: 488 Color: 19
Size: 480 Color: 1
Size: 464 Color: 11
Size: 448 Color: 18
Size: 442 Color: 13
Size: 434 Color: 12
Size: 424 Color: 7
Size: 416 Color: 3
Size: 414 Color: 19
Size: 412 Color: 10
Size: 384 Color: 5
Size: 376 Color: 13
Size: 376 Color: 10
Size: 376 Color: 9
Size: 368 Color: 10
Size: 368 Color: 9
Size: 364 Color: 14
Size: 358 Color: 16
Size: 352 Color: 16
Size: 336 Color: 9
Size: 336 Color: 2
Size: 318 Color: 0
Size: 308 Color: 10
Size: 304 Color: 12
Size: 304 Color: 3
Size: 304 Color: 0
Size: 300 Color: 15
Size: 300 Color: 0
Size: 288 Color: 2
Size: 284 Color: 15
Size: 284 Color: 6
Size: 280 Color: 17
Size: 280 Color: 5
Size: 276 Color: 2
Size: 240 Color: 3

Bin 188: 111 of cap free
Amount of items: 2
Items: 
Size: 12130 Color: 3
Size: 2447 Color: 2

Bin 189: 117 of cap free
Amount of items: 2
Items: 
Size: 9906 Color: 2
Size: 4665 Color: 0

Bin 190: 124 of cap free
Amount of items: 2
Items: 
Size: 10044 Color: 16
Size: 4520 Color: 14

Bin 191: 137 of cap free
Amount of items: 2
Items: 
Size: 10920 Color: 16
Size: 3631 Color: 3

Bin 192: 144 of cap free
Amount of items: 2
Items: 
Size: 9292 Color: 16
Size: 5252 Color: 3

Bin 193: 146 of cap free
Amount of items: 7
Items: 
Size: 7346 Color: 14
Size: 1302 Color: 4
Size: 1292 Color: 19
Size: 1244 Color: 5
Size: 1242 Color: 7
Size: 1220 Color: 7
Size: 896 Color: 2

Bin 194: 157 of cap free
Amount of items: 2
Items: 
Size: 9091 Color: 9
Size: 5440 Color: 15

Bin 195: 159 of cap free
Amount of items: 2
Items: 
Size: 9545 Color: 18
Size: 4984 Color: 13

Bin 196: 173 of cap free
Amount of items: 2
Items: 
Size: 7626 Color: 5
Size: 6889 Color: 3

Bin 197: 184 of cap free
Amount of items: 2
Items: 
Size: 9076 Color: 13
Size: 5428 Color: 0

Bin 198: 194 of cap free
Amount of items: 2
Items: 
Size: 8372 Color: 11
Size: 6122 Color: 5

Bin 199: 10222 of cap free
Amount of items: 18
Items: 
Size: 276 Color: 6
Size: 272 Color: 1
Size: 268 Color: 17
Size: 266 Color: 4
Size: 264 Color: 13
Size: 260 Color: 5
Size: 256 Color: 19
Size: 256 Color: 12
Size: 256 Color: 10
Size: 256 Color: 5
Size: 256 Color: 2
Size: 248 Color: 9
Size: 240 Color: 17
Size: 232 Color: 12
Size: 224 Color: 18
Size: 224 Color: 13
Size: 208 Color: 3
Size: 204 Color: 16

Total size: 2908224
Total free space: 14688

