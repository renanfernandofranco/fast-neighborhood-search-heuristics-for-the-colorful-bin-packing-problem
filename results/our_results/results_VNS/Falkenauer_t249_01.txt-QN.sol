Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 220
Size: 284 Color: 97
Size: 273 Color: 73

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 188
Size: 329 Color: 142
Size: 279 Color: 84

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 227
Size: 293 Color: 108
Size: 255 Color: 34

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 181
Size: 363 Color: 163
Size: 255 Color: 32

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 195
Size: 331 Color: 143
Size: 262 Color: 53

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 218
Size: 291 Color: 103
Size: 269 Color: 66

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 225
Size: 282 Color: 90
Size: 267 Color: 61

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 223
Size: 278 Color: 81
Size: 273 Color: 74

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 179
Size: 316 Color: 130
Size: 302 Color: 116

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 236
Size: 276 Color: 76
Size: 255 Color: 33

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 210
Size: 295 Color: 109
Size: 277 Color: 79

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 170
Size: 359 Color: 158
Size: 269 Color: 64

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 165
Size: 323 Color: 137
Size: 312 Color: 128

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 213
Size: 309 Color: 124
Size: 259 Color: 46

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 182
Size: 325 Color: 138
Size: 292 Color: 105

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 197
Size: 334 Color: 149
Size: 257 Color: 38

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 200
Size: 331 Color: 144
Size: 254 Color: 27

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 198
Size: 301 Color: 115
Size: 289 Color: 100

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 247
Size: 252 Color: 16
Size: 251 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 221
Size: 289 Color: 101
Size: 267 Color: 62

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 211
Size: 288 Color: 99
Size: 281 Color: 89

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 239
Size: 258 Color: 40
Size: 254 Color: 26

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 189
Size: 354 Color: 156
Size: 253 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 177
Size: 361 Color: 162
Size: 258 Color: 42

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 226
Size: 298 Color: 112
Size: 250 Color: 6

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 233
Size: 283 Color: 92
Size: 253 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 246
Size: 252 Color: 14
Size: 251 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 167
Size: 333 Color: 147
Size: 299 Color: 113

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 204
Size: 319 Color: 132
Size: 257 Color: 39

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 191
Size: 323 Color: 136
Size: 279 Color: 86

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 214
Size: 308 Color: 121
Size: 258 Color: 45

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 240
Size: 258 Color: 41
Size: 253 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 184
Size: 343 Color: 153
Size: 272 Color: 72

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 207
Size: 325 Color: 139
Size: 250 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 180
Size: 364 Color: 164
Size: 254 Color: 25

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 248
Size: 251 Color: 9
Size: 250 Color: 7

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 228
Size: 293 Color: 106
Size: 250 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 242
Size: 255 Color: 31
Size: 254 Color: 28

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 219
Size: 288 Color: 98
Size: 271 Color: 70

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 238
Size: 260 Color: 49
Size: 253 Color: 21

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 175
Size: 370 Color: 169
Size: 250 Color: 8

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 166
Size: 361 Color: 161
Size: 271 Color: 69

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 190
Size: 334 Color: 148
Size: 269 Color: 63

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 178
Size: 321 Color: 133
Size: 297 Color: 111

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 222
Size: 284 Color: 96
Size: 270 Color: 67

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 224
Size: 284 Color: 95
Size: 265 Color: 58

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 245
Size: 253 Color: 22
Size: 250 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 187
Size: 340 Color: 152
Size: 269 Color: 65

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 243
Size: 254 Color: 29
Size: 254 Color: 23

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 232
Size: 277 Color: 78
Size: 259 Color: 47

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 244
Size: 256 Color: 35
Size: 250 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 202
Size: 311 Color: 127
Size: 267 Color: 59

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 196
Size: 336 Color: 151
Size: 257 Color: 37

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 237
Size: 260 Color: 51
Size: 260 Color: 48

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 234
Size: 276 Color: 77
Size: 258 Color: 44

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 173
Size: 332 Color: 145
Size: 291 Color: 104

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 199
Size: 335 Color: 150
Size: 250 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 193
Size: 303 Color: 118
Size: 296 Color: 110

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 176
Size: 310 Color: 125
Size: 309 Color: 123

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 203
Size: 328 Color: 140
Size: 250 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 235
Size: 280 Color: 87
Size: 252 Color: 13

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 216
Size: 306 Color: 120
Size: 257 Color: 36

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 208
Size: 322 Color: 135
Size: 252 Color: 15

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 206
Size: 311 Color: 126
Size: 264 Color: 57

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 172
Size: 369 Color: 168
Size: 255 Color: 30

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 171
Size: 358 Color: 157
Size: 270 Color: 68

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 183
Size: 332 Color: 146
Size: 283 Color: 93

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 215
Size: 282 Color: 91
Size: 281 Color: 88

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 209
Size: 309 Color: 122
Size: 263 Color: 54

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 186
Size: 360 Color: 159
Size: 253 Color: 20

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 194
Size: 305 Color: 119
Size: 291 Color: 102

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 231
Size: 279 Color: 83
Size: 260 Color: 50

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 212
Size: 293 Color: 107
Size: 276 Color: 75

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 241
Size: 258 Color: 43
Size: 251 Color: 11

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 229
Size: 278 Color: 82
Size: 263 Color: 55

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 230
Size: 277 Color: 80
Size: 263 Color: 56

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 174
Size: 360 Color: 160
Size: 261 Color: 52

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 201
Size: 317 Color: 131
Size: 267 Color: 60

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 155
Size: 344 Color: 154
Size: 303 Color: 117

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 217
Size: 283 Color: 94
Size: 279 Color: 85

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 205
Size: 321 Color: 134
Size: 254 Color: 24

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 185
Size: 314 Color: 129
Size: 301 Color: 114

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 192
Size: 329 Color: 141
Size: 271 Color: 71

Total size: 83000
Total free space: 0

