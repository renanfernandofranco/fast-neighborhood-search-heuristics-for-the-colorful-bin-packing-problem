Capicity Bin: 15616
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 7818 Color: 19
Size: 6502 Color: 5
Size: 1296 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10088 Color: 14
Size: 5352 Color: 18
Size: 176 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10708 Color: 14
Size: 4616 Color: 7
Size: 292 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11300 Color: 19
Size: 4040 Color: 1
Size: 276 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11352 Color: 8
Size: 3770 Color: 19
Size: 494 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11512 Color: 1
Size: 3784 Color: 2
Size: 320 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11614 Color: 6
Size: 3338 Color: 2
Size: 664 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11923 Color: 9
Size: 3365 Color: 11
Size: 328 Color: 7

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11994 Color: 10
Size: 3022 Color: 7
Size: 600 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12289 Color: 2
Size: 2863 Color: 15
Size: 464 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 0
Size: 2648 Color: 1
Size: 528 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12508 Color: 11
Size: 2890 Color: 9
Size: 218 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12657 Color: 3
Size: 2467 Color: 16
Size: 492 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12686 Color: 5
Size: 2442 Color: 7
Size: 488 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12724 Color: 13
Size: 2596 Color: 18
Size: 296 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 1
Size: 2342 Color: 18
Size: 530 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12792 Color: 1
Size: 2360 Color: 10
Size: 464 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12796 Color: 17
Size: 2356 Color: 17
Size: 464 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12810 Color: 8
Size: 2022 Color: 13
Size: 784 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12862 Color: 18
Size: 2298 Color: 8
Size: 456 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12846 Color: 16
Size: 2310 Color: 10
Size: 460 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12967 Color: 7
Size: 2021 Color: 0
Size: 628 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 10
Size: 1612 Color: 18
Size: 1024 Color: 15

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 5
Size: 2088 Color: 1
Size: 496 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13052 Color: 16
Size: 2060 Color: 12
Size: 504 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13102 Color: 18
Size: 1522 Color: 6
Size: 992 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13132 Color: 13
Size: 2076 Color: 2
Size: 408 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13148 Color: 7
Size: 1892 Color: 18
Size: 576 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 4
Size: 2090 Color: 14
Size: 334 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13196 Color: 10
Size: 1300 Color: 4
Size: 1120 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13325 Color: 18
Size: 1467 Color: 9
Size: 824 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13297 Color: 13
Size: 1775 Color: 0
Size: 544 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 9
Size: 1458 Color: 10
Size: 824 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13398 Color: 18
Size: 1300 Color: 14
Size: 918 Color: 5

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13428 Color: 7
Size: 1784 Color: 2
Size: 404 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13442 Color: 8
Size: 1550 Color: 18
Size: 624 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13480 Color: 12
Size: 1296 Color: 19
Size: 840 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 18
Size: 1528 Color: 14
Size: 516 Color: 12

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13524 Color: 16
Size: 1420 Color: 12
Size: 672 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13559 Color: 3
Size: 1715 Color: 6
Size: 342 Color: 12

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 18
Size: 1320 Color: 16
Size: 688 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13611 Color: 0
Size: 1581 Color: 2
Size: 424 Color: 14

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13639 Color: 0
Size: 1513 Color: 1
Size: 464 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13684 Color: 16
Size: 1404 Color: 15
Size: 528 Color: 6

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13719 Color: 18
Size: 1525 Color: 9
Size: 372 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13728 Color: 14
Size: 1584 Color: 7
Size: 304 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13756 Color: 3
Size: 1248 Color: 6
Size: 612 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 4
Size: 1528 Color: 6
Size: 288 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 18
Size: 1364 Color: 6
Size: 416 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 14
Size: 1358 Color: 12
Size: 388 Color: 19

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 12
Size: 1448 Color: 12
Size: 288 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13926 Color: 14
Size: 1422 Color: 15
Size: 268 Color: 10

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13932 Color: 0
Size: 1330 Color: 10
Size: 354 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13948 Color: 5
Size: 1300 Color: 18
Size: 368 Color: 9

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13990 Color: 6
Size: 1324 Color: 10
Size: 302 Color: 8

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 18
Size: 1184 Color: 2
Size: 404 Color: 16

Bin 57: 1 of cap free
Amount of items: 4
Items: 
Size: 9804 Color: 10
Size: 5121 Color: 0
Size: 530 Color: 5
Size: 160 Color: 7

Bin 58: 1 of cap free
Amount of items: 5
Items: 
Size: 10312 Color: 18
Size: 3725 Color: 19
Size: 752 Color: 5
Size: 440 Color: 12
Size: 386 Color: 14

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 10548 Color: 11
Size: 5067 Color: 8

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11131 Color: 10
Size: 4148 Color: 17
Size: 336 Color: 9

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 11579 Color: 0
Size: 3688 Color: 10
Size: 348 Color: 19

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 11784 Color: 18
Size: 3375 Color: 8
Size: 456 Color: 1

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11939 Color: 6
Size: 2668 Color: 2
Size: 1008 Color: 6

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12282 Color: 3
Size: 2647 Color: 11
Size: 686 Color: 13

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12429 Color: 3
Size: 2782 Color: 18
Size: 404 Color: 17

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12574 Color: 13
Size: 2657 Color: 17
Size: 384 Color: 19

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12757 Color: 6
Size: 1710 Color: 14
Size: 1148 Color: 1

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 13017 Color: 16
Size: 2598 Color: 11

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13203 Color: 8
Size: 1390 Color: 15
Size: 1022 Color: 7

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13487 Color: 15
Size: 1708 Color: 10
Size: 420 Color: 17

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 13517 Color: 17
Size: 2098 Color: 7

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13607 Color: 10
Size: 1384 Color: 1
Size: 624 Color: 10

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 13682 Color: 12
Size: 1933 Color: 3

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13777 Color: 11
Size: 1478 Color: 13
Size: 360 Color: 18

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 13787 Color: 1
Size: 1828 Color: 5

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 9496 Color: 8
Size: 5142 Color: 10
Size: 976 Color: 13

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 9820 Color: 3
Size: 5328 Color: 2
Size: 466 Color: 0

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 18
Size: 4394 Color: 11
Size: 140 Color: 13

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11094 Color: 8
Size: 3372 Color: 12
Size: 1148 Color: 10

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11378 Color: 19
Size: 3052 Color: 2
Size: 1184 Color: 18

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 11563 Color: 1
Size: 3735 Color: 12
Size: 316 Color: 6

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 11834 Color: 15
Size: 3780 Color: 17

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 13474 Color: 0
Size: 2140 Color: 7

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 13710 Color: 7
Size: 1904 Color: 9

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 14022 Color: 2
Size: 1592 Color: 5

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 10754 Color: 14
Size: 4595 Color: 1
Size: 264 Color: 3

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 5
Size: 2881 Color: 9
Size: 704 Color: 6

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 12044 Color: 18
Size: 2661 Color: 9
Size: 908 Color: 2

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 15
Size: 1893 Color: 16

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 13801 Color: 5
Size: 1812 Color: 7

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 14010 Color: 0
Size: 1603 Color: 7

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 10006 Color: 3
Size: 5112 Color: 1
Size: 494 Color: 14

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 10564 Color: 10
Size: 5048 Color: 8

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 10584 Color: 3
Size: 4836 Color: 15
Size: 192 Color: 3

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 11135 Color: 9
Size: 4157 Color: 11
Size: 320 Color: 2

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 12088 Color: 17
Size: 3524 Color: 5

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 12420 Color: 10
Size: 3192 Color: 15

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 13444 Color: 1
Size: 2168 Color: 9

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 12480 Color: 3
Size: 3131 Color: 18

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 13272 Color: 12
Size: 2339 Color: 4

Bin 101: 6 of cap free
Amount of items: 22
Items: 
Size: 880 Color: 13
Size: 880 Color: 0
Size: 876 Color: 3
Size: 840 Color: 3
Size: 832 Color: 11
Size: 830 Color: 11
Size: 820 Color: 3
Size: 816 Color: 8
Size: 784 Color: 19
Size: 758 Color: 0
Size: 752 Color: 4
Size: 746 Color: 12
Size: 744 Color: 19
Size: 720 Color: 9
Size: 672 Color: 13
Size: 626 Color: 19
Size: 614 Color: 17
Size: 576 Color: 6
Size: 576 Color: 2
Size: 436 Color: 5
Size: 416 Color: 19
Size: 416 Color: 1

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 8782 Color: 18
Size: 6508 Color: 1
Size: 320 Color: 10

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 13846 Color: 8
Size: 1764 Color: 19

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 17
Size: 1622 Color: 11

Bin 105: 7 of cap free
Amount of items: 3
Items: 
Size: 8892 Color: 10
Size: 6507 Color: 15
Size: 210 Color: 3

Bin 106: 7 of cap free
Amount of items: 3
Items: 
Size: 11499 Color: 3
Size: 3496 Color: 0
Size: 614 Color: 18

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 13508 Color: 0
Size: 2101 Color: 10

Bin 108: 7 of cap free
Amount of items: 2
Items: 
Size: 13960 Color: 7
Size: 1649 Color: 11

Bin 109: 8 of cap free
Amount of items: 5
Items: 
Size: 7816 Color: 17
Size: 3065 Color: 18
Size: 2167 Color: 10
Size: 2024 Color: 8
Size: 536 Color: 1

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 11067 Color: 7
Size: 4541 Color: 14

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 11380 Color: 1
Size: 4228 Color: 13

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 11396 Color: 9
Size: 4212 Color: 3

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13857 Color: 1
Size: 1751 Color: 13

Bin 114: 10 of cap free
Amount of items: 3
Items: 
Size: 11927 Color: 19
Size: 2065 Color: 18
Size: 1614 Color: 14

Bin 115: 10 of cap free
Amount of items: 2
Items: 
Size: 12990 Color: 16
Size: 2616 Color: 1

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 13068 Color: 8
Size: 2538 Color: 5

Bin 117: 11 of cap free
Amount of items: 10
Items: 
Size: 7809 Color: 13
Size: 1056 Color: 19
Size: 1012 Color: 15
Size: 1008 Color: 10
Size: 968 Color: 15
Size: 968 Color: 6
Size: 960 Color: 9
Size: 912 Color: 0
Size: 552 Color: 1
Size: 360 Color: 2

Bin 118: 11 of cap free
Amount of items: 2
Items: 
Size: 12425 Color: 7
Size: 3180 Color: 16

Bin 119: 11 of cap free
Amount of items: 2
Items: 
Size: 13693 Color: 11
Size: 1912 Color: 4

Bin 120: 12 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 18
Size: 4132 Color: 10
Size: 680 Color: 2

Bin 121: 12 of cap free
Amount of items: 3
Items: 
Size: 13598 Color: 0
Size: 1960 Color: 14
Size: 46 Color: 11

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 13916 Color: 12
Size: 1688 Color: 15

Bin 123: 13 of cap free
Amount of items: 2
Items: 
Size: 12159 Color: 19
Size: 3444 Color: 13

Bin 124: 13 of cap free
Amount of items: 2
Items: 
Size: 13112 Color: 5
Size: 2491 Color: 1

Bin 125: 13 of cap free
Amount of items: 2
Items: 
Size: 13191 Color: 3
Size: 2412 Color: 7

Bin 126: 14 of cap free
Amount of items: 3
Items: 
Size: 8717 Color: 5
Size: 6505 Color: 6
Size: 380 Color: 10

Bin 127: 14 of cap free
Amount of items: 3
Items: 
Size: 9208 Color: 6
Size: 5698 Color: 1
Size: 696 Color: 4

Bin 128: 15 of cap free
Amount of items: 3
Items: 
Size: 8719 Color: 0
Size: 6506 Color: 5
Size: 376 Color: 2

Bin 129: 15 of cap free
Amount of items: 3
Items: 
Size: 9537 Color: 4
Size: 5744 Color: 6
Size: 320 Color: 5

Bin 130: 15 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 1
Size: 1561 Color: 15

Bin 131: 16 of cap free
Amount of items: 3
Items: 
Size: 11260 Color: 15
Size: 4200 Color: 18
Size: 140 Color: 10

Bin 132: 16 of cap free
Amount of items: 2
Items: 
Size: 11964 Color: 12
Size: 3636 Color: 5

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 13566 Color: 1
Size: 2034 Color: 19

Bin 134: 18 of cap free
Amount of items: 2
Items: 
Size: 8718 Color: 7
Size: 6880 Color: 16

Bin 135: 18 of cap free
Amount of items: 2
Items: 
Size: 11859 Color: 3
Size: 3739 Color: 17

Bin 136: 18 of cap free
Amount of items: 2
Items: 
Size: 13190 Color: 10
Size: 2408 Color: 5

Bin 137: 19 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 18
Size: 5749 Color: 8
Size: 272 Color: 2

Bin 138: 19 of cap free
Amount of items: 2
Items: 
Size: 12645 Color: 19
Size: 2952 Color: 11

Bin 139: 19 of cap free
Amount of items: 3
Items: 
Size: 12811 Color: 4
Size: 2670 Color: 11
Size: 116 Color: 19

Bin 140: 20 of cap free
Amount of items: 2
Items: 
Size: 9450 Color: 0
Size: 6146 Color: 9

Bin 141: 20 of cap free
Amount of items: 2
Items: 
Size: 11432 Color: 18
Size: 4164 Color: 12

Bin 142: 20 of cap free
Amount of items: 2
Items: 
Size: 13790 Color: 9
Size: 1806 Color: 7

Bin 143: 21 of cap free
Amount of items: 2
Items: 
Size: 11492 Color: 9
Size: 4103 Color: 11

Bin 144: 21 of cap free
Amount of items: 2
Items: 
Size: 12441 Color: 8
Size: 3154 Color: 5

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 13270 Color: 8
Size: 2325 Color: 14

Bin 146: 22 of cap free
Amount of items: 2
Items: 
Size: 13912 Color: 16
Size: 1682 Color: 1

Bin 147: 22 of cap free
Amount of items: 2
Items: 
Size: 13958 Color: 17
Size: 1636 Color: 16

Bin 148: 23 of cap free
Amount of items: 7
Items: 
Size: 7812 Color: 4
Size: 2020 Color: 2
Size: 1958 Color: 8
Size: 1867 Color: 18
Size: 1556 Color: 6
Size: 220 Color: 12
Size: 160 Color: 13

Bin 149: 23 of cap free
Amount of items: 2
Items: 
Size: 13845 Color: 11
Size: 1748 Color: 4

Bin 150: 25 of cap free
Amount of items: 2
Items: 
Size: 11800 Color: 11
Size: 3791 Color: 19

Bin 151: 26 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 1
Size: 1850 Color: 7
Size: 80 Color: 11

Bin 152: 30 of cap free
Amount of items: 2
Items: 
Size: 13377 Color: 8
Size: 2209 Color: 17

Bin 153: 31 of cap free
Amount of items: 2
Items: 
Size: 13674 Color: 1
Size: 1911 Color: 3

Bin 154: 31 of cap free
Amount of items: 2
Items: 
Size: 13780 Color: 6
Size: 1805 Color: 15

Bin 155: 33 of cap free
Amount of items: 2
Items: 
Size: 10167 Color: 17
Size: 5416 Color: 12

Bin 156: 34 of cap free
Amount of items: 2
Items: 
Size: 13454 Color: 9
Size: 2128 Color: 12

Bin 157: 35 of cap free
Amount of items: 2
Items: 
Size: 12150 Color: 16
Size: 3431 Color: 19

Bin 158: 35 of cap free
Amount of items: 2
Items: 
Size: 12502 Color: 3
Size: 3079 Color: 0

Bin 159: 35 of cap free
Amount of items: 2
Items: 
Size: 13910 Color: 11
Size: 1671 Color: 14

Bin 160: 36 of cap free
Amount of items: 3
Items: 
Size: 10346 Color: 4
Size: 4802 Color: 11
Size: 432 Color: 2

Bin 161: 36 of cap free
Amount of items: 2
Items: 
Size: 13778 Color: 16
Size: 1802 Color: 1

Bin 162: 38 of cap free
Amount of items: 3
Items: 
Size: 8400 Color: 5
Size: 6504 Color: 18
Size: 674 Color: 3

Bin 163: 39 of cap free
Amount of items: 6
Items: 
Size: 7811 Color: 7
Size: 1814 Color: 18
Size: 1590 Color: 16
Size: 1534 Color: 9
Size: 1532 Color: 18
Size: 1296 Color: 8

Bin 164: 40 of cap free
Amount of items: 2
Items: 
Size: 13095 Color: 14
Size: 2481 Color: 2

Bin 165: 41 of cap free
Amount of items: 2
Items: 
Size: 13451 Color: 17
Size: 2124 Color: 13

Bin 166: 45 of cap free
Amount of items: 2
Items: 
Size: 11147 Color: 8
Size: 4424 Color: 3

Bin 167: 45 of cap free
Amount of items: 2
Items: 
Size: 13950 Color: 13
Size: 1621 Color: 15

Bin 168: 52 of cap free
Amount of items: 8
Items: 
Size: 7810 Color: 16
Size: 1484 Color: 15
Size: 1382 Color: 3
Size: 1342 Color: 3
Size: 1296 Color: 1
Size: 1072 Color: 7
Size: 746 Color: 2
Size: 432 Color: 6

Bin 169: 53 of cap free
Amount of items: 2
Items: 
Size: 12488 Color: 19
Size: 3075 Color: 10

Bin 170: 55 of cap free
Amount of items: 2
Items: 
Size: 13178 Color: 11
Size: 2383 Color: 14

Bin 171: 56 of cap free
Amount of items: 3
Items: 
Size: 10644 Color: 0
Size: 4092 Color: 8
Size: 824 Color: 2

Bin 172: 57 of cap free
Amount of items: 3
Items: 
Size: 10629 Color: 8
Size: 3534 Color: 3
Size: 1396 Color: 8

Bin 173: 60 of cap free
Amount of items: 2
Items: 
Size: 11948 Color: 8
Size: 3608 Color: 17

Bin 174: 60 of cap free
Amount of items: 2
Items: 
Size: 12827 Color: 2
Size: 2729 Color: 9

Bin 175: 60 of cap free
Amount of items: 2
Items: 
Size: 13079 Color: 16
Size: 2477 Color: 10

Bin 176: 63 of cap free
Amount of items: 2
Items: 
Size: 10693 Color: 1
Size: 4860 Color: 0

Bin 177: 64 of cap free
Amount of items: 3
Items: 
Size: 9140 Color: 4
Size: 5604 Color: 9
Size: 808 Color: 2

Bin 178: 64 of cap free
Amount of items: 2
Items: 
Size: 12140 Color: 12
Size: 3412 Color: 9

Bin 179: 64 of cap free
Amount of items: 2
Items: 
Size: 13348 Color: 0
Size: 2204 Color: 8

Bin 180: 65 of cap free
Amount of items: 2
Items: 
Size: 12343 Color: 14
Size: 3208 Color: 4

Bin 181: 68 of cap free
Amount of items: 2
Items: 
Size: 7820 Color: 14
Size: 7728 Color: 3

Bin 182: 75 of cap free
Amount of items: 2
Items: 
Size: 12641 Color: 0
Size: 2900 Color: 17

Bin 183: 76 of cap free
Amount of items: 2
Items: 
Size: 9128 Color: 11
Size: 6412 Color: 15

Bin 184: 77 of cap free
Amount of items: 2
Items: 
Size: 9788 Color: 1
Size: 5751 Color: 4

Bin 185: 90 of cap free
Amount of items: 2
Items: 
Size: 13336 Color: 12
Size: 2190 Color: 11

Bin 186: 98 of cap free
Amount of items: 2
Items: 
Size: 9250 Color: 17
Size: 6268 Color: 3

Bin 187: 104 of cap free
Amount of items: 2
Items: 
Size: 11572 Color: 14
Size: 3940 Color: 13

Bin 188: 109 of cap free
Amount of items: 2
Items: 
Size: 10103 Color: 0
Size: 5404 Color: 16

Bin 189: 110 of cap free
Amount of items: 2
Items: 
Size: 8884 Color: 5
Size: 6622 Color: 19

Bin 190: 111 of cap free
Amount of items: 2
Items: 
Size: 9473 Color: 18
Size: 6032 Color: 6

Bin 191: 112 of cap free
Amount of items: 3
Items: 
Size: 8104 Color: 9
Size: 6264 Color: 1
Size: 1136 Color: 16

Bin 192: 112 of cap free
Amount of items: 2
Items: 
Size: 10660 Color: 16
Size: 4844 Color: 8

Bin 193: 116 of cap free
Amount of items: 2
Items: 
Size: 8980 Color: 19
Size: 6520 Color: 14

Bin 194: 126 of cap free
Amount of items: 3
Items: 
Size: 10628 Color: 17
Size: 4054 Color: 15
Size: 808 Color: 5

Bin 195: 132 of cap free
Amount of items: 3
Items: 
Size: 7912 Color: 5
Size: 6500 Color: 19
Size: 1072 Color: 2

Bin 196: 134 of cap free
Amount of items: 2
Items: 
Size: 12414 Color: 14
Size: 3068 Color: 12

Bin 197: 136 of cap free
Amount of items: 2
Items: 
Size: 11567 Color: 10
Size: 3913 Color: 4

Bin 198: 160 of cap free
Amount of items: 39
Items: 
Size: 600 Color: 17
Size: 576 Color: 15
Size: 576 Color: 4
Size: 532 Color: 9
Size: 528 Color: 18
Size: 512 Color: 12
Size: 512 Color: 12
Size: 480 Color: 18
Size: 476 Color: 13
Size: 472 Color: 2
Size: 464 Color: 8
Size: 464 Color: 4
Size: 424 Color: 16
Size: 408 Color: 11
Size: 400 Color: 2
Size: 400 Color: 0
Size: 376 Color: 2
Size: 368 Color: 16
Size: 368 Color: 15
Size: 360 Color: 6
Size: 360 Color: 6
Size: 360 Color: 1
Size: 352 Color: 11
Size: 348 Color: 12
Size: 344 Color: 19
Size: 344 Color: 13
Size: 340 Color: 5
Size: 336 Color: 18
Size: 336 Color: 13
Size: 320 Color: 9
Size: 320 Color: 3
Size: 320 Color: 1
Size: 316 Color: 6
Size: 304 Color: 17
Size: 304 Color: 14
Size: 304 Color: 5
Size: 288 Color: 1
Size: 284 Color: 4
Size: 280 Color: 5

Bin 199: 11492 of cap free
Amount of items: 15
Items: 
Size: 304 Color: 15
Size: 304 Color: 6
Size: 304 Color: 0
Size: 292 Color: 8
Size: 288 Color: 1
Size: 280 Color: 10
Size: 276 Color: 13
Size: 276 Color: 10
Size: 272 Color: 17
Size: 272 Color: 17
Size: 264 Color: 12
Size: 264 Color: 5
Size: 264 Color: 4
Size: 256 Color: 2
Size: 208 Color: 8

Total size: 3091968
Total free space: 15616

