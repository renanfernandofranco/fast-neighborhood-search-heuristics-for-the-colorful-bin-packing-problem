Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1788 Color: 164
Size: 572 Color: 109
Size: 112 Color: 42

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1860 Color: 169
Size: 596 Color: 111
Size: 16 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1877 Color: 170
Size: 497 Color: 103
Size: 98 Color: 39

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 174
Size: 446 Color: 99
Size: 88 Color: 36

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 175
Size: 365 Color: 88
Size: 160 Color: 57

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 176
Size: 491 Color: 102
Size: 26 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2013 Color: 179
Size: 383 Color: 93
Size: 76 Color: 28

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 181
Size: 297 Color: 80
Size: 148 Color: 54

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 186
Size: 364 Color: 87
Size: 40 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 190
Size: 340 Color: 85
Size: 22 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2137 Color: 192
Size: 281 Color: 78
Size: 54 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 193
Size: 200 Color: 65
Size: 132 Color: 50

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2145 Color: 195
Size: 273 Color: 75
Size: 54 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 196
Size: 244 Color: 71
Size: 80 Color: 29

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 199
Size: 150 Color: 56
Size: 120 Color: 45

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 200
Size: 196 Color: 64
Size: 72 Color: 24

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2218 Color: 201
Size: 180 Color: 62
Size: 74 Color: 27

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 165
Size: 662 Color: 116
Size: 16 Color: 2

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1826 Color: 168
Size: 645 Color: 113

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 177
Size: 379 Color: 92
Size: 136 Color: 52

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 2035 Color: 184
Size: 436 Color: 97

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 187
Size: 384 Color: 94
Size: 8 Color: 1

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 2105 Color: 189
Size: 366 Color: 89

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 2142 Color: 194
Size: 329 Color: 84

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 2153 Color: 197
Size: 318 Color: 83

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1691 Color: 160
Size: 747 Color: 120
Size: 32 Color: 7

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 173
Size: 476 Color: 101
Size: 60 Color: 17

Bin 28: 3 of cap free
Amount of items: 2
Items: 
Size: 1415 Color: 147
Size: 1054 Color: 137

Bin 29: 3 of cap free
Amount of items: 2
Items: 
Size: 1708 Color: 163
Size: 761 Color: 122

Bin 30: 3 of cap free
Amount of items: 2
Items: 
Size: 1799 Color: 166
Size: 670 Color: 117

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1818 Color: 167
Size: 651 Color: 114

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1885 Color: 171
Size: 576 Color: 110
Size: 8 Color: 0

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1908 Color: 172
Size: 561 Color: 107

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 2019 Color: 180
Size: 450 Color: 100

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 2030 Color: 182
Size: 439 Color: 98

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 2044 Color: 185
Size: 425 Color: 95

Bin 37: 4 of cap free
Amount of items: 15
Items: 
Size: 258 Color: 72
Size: 226 Color: 70
Size: 204 Color: 68
Size: 204 Color: 67
Size: 204 Color: 66
Size: 192 Color: 63
Size: 176 Color: 60
Size: 160 Color: 59
Size: 160 Color: 58
Size: 128 Color: 47
Size: 120 Color: 46
Size: 112 Color: 44
Size: 112 Color: 43
Size: 108 Color: 41
Size: 104 Color: 40

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 145
Size: 1000 Color: 132
Size: 64 Color: 20

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 2097 Color: 188
Size: 371 Color: 91

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 2166 Color: 198
Size: 302 Color: 81

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 154
Size: 830 Color: 125
Size: 60 Color: 18

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1963 Color: 178
Size: 504 Color: 104

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 191
Size: 349 Color: 86

Bin 44: 7 of cap free
Amount of items: 2
Items: 
Size: 2034 Color: 183
Size: 431 Color: 96

Bin 45: 9 of cap free
Amount of items: 7
Items: 
Size: 1237 Color: 138
Size: 276 Color: 76
Size: 272 Color: 74
Size: 272 Color: 73
Size: 214 Color: 69
Size: 96 Color: 38
Size: 96 Color: 37

Bin 46: 10 of cap free
Amount of items: 2
Items: 
Size: 1484 Color: 149
Size: 978 Color: 130

Bin 47: 11 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 153
Size: 828 Color: 124
Size: 64 Color: 19

Bin 48: 12 of cap free
Amount of items: 2
Items: 
Size: 1650 Color: 156
Size: 810 Color: 123

Bin 49: 13 of cap free
Amount of items: 7
Items: 
Size: 1238 Color: 139
Size: 307 Color: 82
Size: 284 Color: 79
Size: 278 Color: 77
Size: 176 Color: 61
Size: 88 Color: 35
Size: 88 Color: 34

Bin 50: 13 of cap free
Amount of items: 4
Items: 
Size: 1302 Color: 144
Size: 1029 Color: 135
Size: 64 Color: 22
Size: 64 Color: 21

Bin 51: 20 of cap free
Amount of items: 4
Items: 
Size: 1686 Color: 159
Size: 686 Color: 118
Size: 44 Color: 10
Size: 36 Color: 8

Bin 52: 20 of cap free
Amount of items: 2
Items: 
Size: 1699 Color: 161
Size: 753 Color: 121

Bin 53: 20 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 162
Size: 724 Color: 119
Size: 24 Color: 5

Bin 54: 22 of cap free
Amount of items: 4
Items: 
Size: 1278 Color: 143
Size: 1028 Color: 134
Size: 72 Color: 25
Size: 72 Color: 23

Bin 55: 22 of cap free
Amount of items: 2
Items: 
Size: 1502 Color: 150
Size: 948 Color: 129

Bin 56: 28 of cap free
Amount of items: 2
Items: 
Size: 1413 Color: 146
Size: 1031 Color: 136

Bin 57: 28 of cap free
Amount of items: 2
Items: 
Size: 1561 Color: 152
Size: 883 Color: 128

Bin 58: 28 of cap free
Amount of items: 2
Items: 
Size: 1604 Color: 155
Size: 840 Color: 126

Bin 59: 39 of cap free
Amount of items: 2
Items: 
Size: 1552 Color: 151
Size: 881 Color: 127

Bin 60: 40 of cap free
Amount of items: 4
Items: 
Size: 1678 Color: 158
Size: 658 Color: 115
Size: 48 Color: 12
Size: 48 Color: 11

Bin 61: 46 of cap free
Amount of items: 4
Items: 
Size: 1246 Color: 142
Size: 1022 Color: 133
Size: 84 Color: 30
Size: 74 Color: 26

Bin 62: 53 of cap free
Amount of items: 3
Items: 
Size: 1482 Color: 148
Size: 567 Color: 108
Size: 370 Color: 90

Bin 63: 57 of cap free
Amount of items: 4
Items: 
Size: 1239 Color: 140
Size: 546 Color: 106
Size: 542 Color: 105
Size: 88 Color: 33

Bin 64: 58 of cap free
Amount of items: 4
Items: 
Size: 1244 Color: 141
Size: 998 Color: 131
Size: 86 Color: 32
Size: 86 Color: 31

Bin 65: 58 of cap free
Amount of items: 4
Items: 
Size: 1670 Color: 157
Size: 644 Color: 112
Size: 52 Color: 14
Size: 48 Color: 13

Bin 66: 1788 of cap free
Amount of items: 5
Items: 
Size: 150 Color: 55
Size: 144 Color: 53
Size: 132 Color: 51
Size: 130 Color: 49
Size: 128 Color: 48

Total size: 160680
Total free space: 2472

