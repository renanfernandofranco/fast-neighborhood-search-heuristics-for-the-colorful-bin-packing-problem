Capicity Bin: 2032
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1019 Color: 2
Size: 483 Color: 2
Size: 322 Color: 2
Size: 144 Color: 4
Size: 64 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1408 Color: 3
Size: 570 Color: 2
Size: 54 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 3
Size: 578 Color: 1
Size: 36 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 1
Size: 426 Color: 2
Size: 84 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 4
Size: 377 Color: 4
Size: 74 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 2
Size: 374 Color: 4
Size: 32 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 0
Size: 373 Color: 3
Size: 74 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 1
Size: 410 Color: 3
Size: 36 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1631 Color: 2
Size: 335 Color: 1
Size: 66 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 2
Size: 342 Color: 3
Size: 44 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 0
Size: 257 Color: 2
Size: 112 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 0
Size: 251 Color: 2
Size: 80 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1705 Color: 0
Size: 277 Color: 2
Size: 50 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 2
Size: 266 Color: 3
Size: 60 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 1
Size: 246 Color: 2
Size: 64 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 0
Size: 202 Color: 2
Size: 92 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 1
Size: 186 Color: 2
Size: 72 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 2
Size: 212 Color: 3
Size: 40 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1779 Color: 4
Size: 233 Color: 1
Size: 20 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 1
Size: 210 Color: 2
Size: 40 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 2
Size: 194 Color: 4
Size: 48 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 0
Size: 203 Color: 2
Size: 22 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 4
Size: 174 Color: 0
Size: 32 Color: 2

Bin 24: 1 of cap free
Amount of items: 4
Items: 
Size: 1042 Color: 4
Size: 845 Color: 0
Size: 108 Color: 2
Size: 36 Color: 3

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 2
Size: 468 Color: 3
Size: 42 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 2
Size: 382 Color: 1
Size: 72 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 3
Size: 381 Color: 2
Size: 72 Color: 4

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1695 Color: 2
Size: 168 Color: 4
Size: 168 Color: 3

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1725 Color: 2
Size: 306 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 3
Size: 202 Color: 1
Size: 76 Color: 2

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 2
Size: 204 Color: 3
Size: 56 Color: 3

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 1757 Color: 4
Size: 274 Color: 1

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 1
Size: 182 Color: 2
Size: 46 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 3
Size: 205 Color: 0
Size: 8 Color: 4

Bin 35: 2 of cap free
Amount of items: 4
Items: 
Size: 1018 Color: 1
Size: 824 Color: 4
Size: 148 Color: 4
Size: 40 Color: 1

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1810 Color: 2
Size: 220 Color: 1

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 2
Size: 219 Color: 3
Size: 8 Color: 4

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1102 Color: 1
Size: 886 Color: 2
Size: 40 Color: 4

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1474 Color: 2
Size: 514 Color: 1
Size: 40 Color: 4

Bin 40: 5 of cap free
Amount of items: 3
Items: 
Size: 1186 Color: 1
Size: 729 Color: 1
Size: 112 Color: 2

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1277 Color: 1
Size: 710 Color: 3
Size: 40 Color: 1

Bin 42: 7 of cap free
Amount of items: 3
Items: 
Size: 1342 Color: 0
Size: 631 Color: 1
Size: 52 Color: 4

Bin 43: 7 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 1
Size: 633 Color: 4
Size: 42 Color: 2

Bin 44: 7 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 1
Size: 231 Color: 3

Bin 45: 8 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 2
Size: 281 Color: 1
Size: 74 Color: 0

Bin 46: 9 of cap free
Amount of items: 2
Items: 
Size: 1714 Color: 0
Size: 309 Color: 3

Bin 47: 10 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 4
Size: 551 Color: 2
Size: 96 Color: 3

Bin 48: 10 of cap free
Amount of items: 2
Items: 
Size: 1733 Color: 0
Size: 289 Color: 4

Bin 49: 11 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 2
Size: 191 Color: 3
Size: 164 Color: 4

Bin 50: 12 of cap free
Amount of items: 3
Items: 
Size: 1142 Color: 3
Size: 826 Color: 0
Size: 52 Color: 2

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1593 Color: 3
Size: 427 Color: 1

Bin 52: 13 of cap free
Amount of items: 13
Items: 
Size: 312 Color: 2
Size: 282 Color: 2
Size: 262 Color: 2
Size: 211 Color: 2
Size: 144 Color: 4
Size: 128 Color: 4
Size: 128 Color: 4
Size: 126 Color: 0
Size: 124 Color: 3
Size: 96 Color: 3
Size: 84 Color: 0
Size: 84 Color: 0
Size: 38 Color: 3

Bin 53: 13 of cap free
Amount of items: 3
Items: 
Size: 1453 Color: 2
Size: 466 Color: 3
Size: 100 Color: 0

Bin 54: 13 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 2
Size: 367 Color: 4
Size: 110 Color: 0

Bin 55: 15 of cap free
Amount of items: 2
Items: 
Size: 1371 Color: 2
Size: 646 Color: 0

Bin 56: 17 of cap free
Amount of items: 2
Items: 
Size: 1157 Color: 4
Size: 858 Color: 2

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 1272 Color: 2
Size: 742 Color: 0

Bin 58: 26 of cap free
Amount of items: 2
Items: 
Size: 1159 Color: 1
Size: 847 Color: 0

Bin 59: 27 of cap free
Amount of items: 2
Items: 
Size: 1787 Color: 2
Size: 218 Color: 3

Bin 60: 28 of cap free
Amount of items: 2
Items: 
Size: 1273 Color: 1
Size: 731 Color: 2

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1455 Color: 3
Size: 549 Color: 0

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1523 Color: 0
Size: 481 Color: 1

Bin 63: 30 of cap free
Amount of items: 3
Items: 
Size: 1258 Color: 3
Size: 692 Color: 1
Size: 52 Color: 2

Bin 64: 31 of cap free
Amount of items: 2
Items: 
Size: 1698 Color: 1
Size: 303 Color: 3

Bin 65: 34 of cap free
Amount of items: 4
Items: 
Size: 1017 Color: 1
Size: 425 Color: 2
Size: 420 Color: 2
Size: 136 Color: 0

Bin 66: 1582 of cap free
Amount of items: 8
Items: 
Size: 72 Color: 3
Size: 72 Color: 0
Size: 60 Color: 3
Size: 60 Color: 0
Size: 52 Color: 1
Size: 48 Color: 4
Size: 48 Color: 1
Size: 38 Color: 0

Total size: 132080
Total free space: 2032

