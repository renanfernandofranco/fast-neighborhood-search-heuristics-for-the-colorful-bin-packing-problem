Capicity Bin: 1888
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1184 Color: 148
Size: 491 Color: 113
Size: 213 Color: 80

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 162
Size: 530 Color: 119
Size: 32 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 163
Size: 522 Color: 117
Size: 32 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1367 Color: 164
Size: 501 Color: 116
Size: 20 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 167
Size: 398 Color: 107
Size: 76 Color: 43

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 170
Size: 358 Color: 103
Size: 68 Color: 36

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 175
Size: 322 Color: 97
Size: 60 Color: 32

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 176
Size: 327 Color: 99
Size: 48 Color: 23

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 183
Size: 253 Color: 85
Size: 58 Color: 31

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 185
Size: 261 Color: 88
Size: 42 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 195
Size: 199 Color: 75
Size: 38 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 197
Size: 191 Color: 73
Size: 36 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 198
Size: 182 Color: 71
Size: 44 Color: 20

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 201
Size: 134 Color: 64
Size: 68 Color: 39

Bin 15: 1 of cap free
Amount of items: 5
Items: 
Size: 947 Color: 140
Size: 329 Color: 100
Size: 292 Color: 93
Size: 269 Color: 90
Size: 50 Color: 25

Bin 16: 1 of cap free
Amount of items: 5
Items: 
Size: 1089 Color: 145
Size: 666 Color: 127
Size: 44 Color: 21
Size: 44 Color: 19
Size: 44 Color: 18

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 1394 Color: 165
Size: 493 Color: 114

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1425 Color: 168
Size: 462 Color: 112

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1451 Color: 169
Size: 436 Color: 111

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 171
Size: 409 Color: 108
Size: 8 Color: 1

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1473 Color: 172
Size: 414 Color: 109

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1495 Color: 173
Size: 392 Color: 106

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1500 Color: 174
Size: 387 Color: 105

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1537 Color: 179
Size: 350 Color: 102

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 180
Size: 206 Color: 76
Size: 132 Color: 60

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 184
Size: 208 Color: 78
Size: 98 Color: 50

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1613 Color: 189
Size: 274 Color: 91

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1622 Color: 190
Size: 265 Color: 89

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1633 Color: 192
Size: 254 Color: 86

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 194
Size: 208 Color: 77
Size: 32 Color: 8

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 1670 Color: 199
Size: 217 Color: 81

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 200
Size: 209 Color: 79

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1293 Color: 158
Size: 567 Color: 120
Size: 26 Color: 4

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1297 Color: 159
Size: 573 Color: 121
Size: 16 Color: 2

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 160
Size: 577 Color: 122
Size: 8 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 166
Size: 435 Color: 110
Size: 52 Color: 28

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1562 Color: 181
Size: 324 Color: 98

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 1593 Color: 187
Size: 293 Color: 94

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 1629 Color: 191
Size: 257 Color: 87

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1077 Color: 143
Size: 760 Color: 134
Size: 48 Color: 22

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 154
Size: 640 Color: 126
Size: 36 Color: 10

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 155
Size: 497 Color: 115
Size: 134 Color: 63

Bin 43: 3 of cap free
Amount of items: 2
Items: 
Size: 1304 Color: 161
Size: 581 Color: 123

Bin 44: 3 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 188
Size: 283 Color: 92

Bin 45: 3 of cap free
Amount of items: 2
Items: 
Size: 1654 Color: 196
Size: 231 Color: 83

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 1571 Color: 182
Size: 313 Color: 96

Bin 47: 4 of cap free
Amount of items: 2
Items: 
Size: 1586 Color: 186
Size: 298 Color: 95

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 1642 Color: 193
Size: 242 Color: 84

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 1517 Color: 177
Size: 365 Color: 104

Bin 50: 7 of cap free
Amount of items: 9
Items: 
Size: 945 Color: 138
Size: 170 Color: 69
Size: 156 Color: 68
Size: 156 Color: 67
Size: 156 Color: 66
Size: 124 Color: 59
Size: 62 Color: 33
Size: 56 Color: 30
Size: 56 Color: 29

Bin 51: 7 of cap free
Amount of items: 2
Items: 
Size: 1534 Color: 178
Size: 347 Color: 101

Bin 52: 10 of cap free
Amount of items: 2
Items: 
Size: 1197 Color: 152
Size: 681 Color: 132

Bin 53: 11 of cap free
Amount of items: 4
Items: 
Size: 1287 Color: 157
Size: 522 Color: 118
Size: 36 Color: 9
Size: 32 Color: 5

Bin 54: 13 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 153
Size: 638 Color: 125
Size: 36 Color: 11

Bin 55: 15 of cap free
Amount of items: 4
Items: 
Size: 1126 Color: 147
Size: 667 Color: 128
Size: 40 Color: 15
Size: 40 Color: 14

Bin 56: 18 of cap free
Amount of items: 3
Items: 
Size: 1090 Color: 146
Size: 738 Color: 133
Size: 42 Color: 16

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 1193 Color: 151
Size: 677 Color: 131

Bin 58: 20 of cap free
Amount of items: 2
Items: 
Size: 1081 Color: 144
Size: 787 Color: 137

Bin 59: 20 of cap free
Amount of items: 2
Items: 
Size: 1262 Color: 156
Size: 606 Color: 124

Bin 60: 25 of cap free
Amount of items: 2
Items: 
Size: 1190 Color: 150
Size: 673 Color: 130

Bin 61: 29 of cap free
Amount of items: 2
Items: 
Size: 1073 Color: 142
Size: 786 Color: 136

Bin 62: 32 of cap free
Amount of items: 2
Items: 
Size: 1187 Color: 149
Size: 669 Color: 129

Bin 63: 47 of cap free
Amount of items: 3
Items: 
Size: 1006 Color: 141
Size: 785 Color: 135
Size: 50 Color: 24

Bin 64: 52 of cap free
Amount of items: 7
Items: 
Size: 946 Color: 139
Size: 222 Color: 82
Size: 198 Color: 74
Size: 190 Color: 72
Size: 178 Color: 70
Size: 52 Color: 27
Size: 50 Color: 26

Bin 65: 54 of cap free
Amount of items: 19
Items: 
Size: 144 Color: 65
Size: 134 Color: 62
Size: 132 Color: 61
Size: 114 Color: 58
Size: 114 Color: 57
Size: 114 Color: 56
Size: 112 Color: 55
Size: 104 Color: 54
Size: 104 Color: 53
Size: 100 Color: 52
Size: 98 Color: 51
Size: 80 Color: 44
Size: 76 Color: 42
Size: 72 Color: 41
Size: 72 Color: 40
Size: 68 Color: 38
Size: 68 Color: 37
Size: 64 Color: 35
Size: 64 Color: 34

Bin 66: 1442 of cap free
Amount of items: 5
Items: 
Size: 96 Color: 49
Size: 92 Color: 48
Size: 92 Color: 47
Size: 86 Color: 46
Size: 80 Color: 45

Total size: 122720
Total free space: 1888

