Capicity Bin: 1000001
Lower Bound: 48

Bins used: 53
Amount of Colors: 2

Bin 1: 14 of cap free
Amount of items: 3
Items: 
Size: 392726 Color: 0
Size: 347440 Color: 0
Size: 259821 Color: 1

Bin 2: 19 of cap free
Amount of items: 3
Items: 
Size: 633046 Color: 1
Size: 225457 Color: 0
Size: 141479 Color: 1

Bin 3: 126 of cap free
Amount of items: 3
Items: 
Size: 649739 Color: 1
Size: 207209 Color: 1
Size: 142927 Color: 0

Bin 4: 172 of cap free
Amount of items: 3
Items: 
Size: 580287 Color: 0
Size: 288889 Color: 1
Size: 130653 Color: 0

Bin 5: 279 of cap free
Amount of items: 3
Items: 
Size: 405672 Color: 0
Size: 393610 Color: 0
Size: 200440 Color: 1

Bin 6: 308 of cap free
Amount of items: 3
Items: 
Size: 634074 Color: 1
Size: 208939 Color: 1
Size: 156680 Color: 0

Bin 7: 550 of cap free
Amount of items: 3
Items: 
Size: 645176 Color: 1
Size: 191525 Color: 0
Size: 162750 Color: 0

Bin 8: 615 of cap free
Amount of items: 2
Items: 
Size: 527056 Color: 0
Size: 472330 Color: 1

Bin 9: 783 of cap free
Amount of items: 3
Items: 
Size: 507523 Color: 0
Size: 312521 Color: 1
Size: 179174 Color: 1

Bin 10: 977 of cap free
Amount of items: 2
Items: 
Size: 597926 Color: 0
Size: 401098 Color: 1

Bin 11: 1098 of cap free
Amount of items: 3
Items: 
Size: 527032 Color: 0
Size: 306116 Color: 1
Size: 165755 Color: 0

Bin 12: 1115 of cap free
Amount of items: 3
Items: 
Size: 622480 Color: 1
Size: 194630 Color: 0
Size: 181776 Color: 0

Bin 13: 1163 of cap free
Amount of items: 3
Items: 
Size: 616965 Color: 0
Size: 191588 Color: 1
Size: 190285 Color: 0

Bin 14: 1307 of cap free
Amount of items: 3
Items: 
Size: 502562 Color: 0
Size: 265801 Color: 1
Size: 230331 Color: 0

Bin 15: 1714 of cap free
Amount of items: 3
Items: 
Size: 609366 Color: 1
Size: 251905 Color: 0
Size: 137016 Color: 0

Bin 16: 1762 of cap free
Amount of items: 2
Items: 
Size: 552439 Color: 0
Size: 445800 Color: 1

Bin 17: 3152 of cap free
Amount of items: 3
Items: 
Size: 481574 Color: 0
Size: 394286 Color: 0
Size: 120989 Color: 1

Bin 18: 4212 of cap free
Amount of items: 3
Items: 
Size: 532044 Color: 0
Size: 320184 Color: 0
Size: 143561 Color: 1

Bin 19: 4399 of cap free
Amount of items: 2
Items: 
Size: 646036 Color: 0
Size: 349566 Color: 1

Bin 20: 4611 of cap free
Amount of items: 2
Items: 
Size: 557130 Color: 0
Size: 438260 Color: 1

Bin 21: 5593 of cap free
Amount of items: 2
Items: 
Size: 541444 Color: 0
Size: 452964 Color: 1

Bin 22: 6250 of cap free
Amount of items: 2
Items: 
Size: 525559 Color: 1
Size: 468192 Color: 0

Bin 23: 7533 of cap free
Amount of items: 3
Items: 
Size: 504071 Color: 0
Size: 376158 Color: 0
Size: 112239 Color: 1

Bin 24: 7997 of cap free
Amount of items: 2
Items: 
Size: 518895 Color: 0
Size: 473109 Color: 1

Bin 25: 8734 of cap free
Amount of items: 2
Items: 
Size: 549524 Color: 1
Size: 441743 Color: 0

Bin 26: 12658 of cap free
Amount of items: 2
Items: 
Size: 668163 Color: 0
Size: 319180 Color: 1

Bin 27: 14193 of cap free
Amount of items: 2
Items: 
Size: 641125 Color: 1
Size: 344683 Color: 0

Bin 28: 22600 of cap free
Amount of items: 2
Items: 
Size: 549325 Color: 0
Size: 428076 Color: 1

Bin 29: 25539 of cap free
Amount of items: 2
Items: 
Size: 582352 Color: 0
Size: 392110 Color: 1

Bin 30: 38384 of cap free
Amount of items: 2
Items: 
Size: 482026 Color: 1
Size: 479591 Color: 0

Bin 31: 54518 of cap free
Amount of items: 2
Items: 
Size: 575812 Color: 0
Size: 369671 Color: 1

Bin 32: 80703 of cap free
Amount of items: 2
Items: 
Size: 574426 Color: 0
Size: 344872 Color: 1

Bin 33: 114856 of cap free
Amount of items: 2
Items: 
Size: 451081 Color: 1
Size: 434064 Color: 0

Bin 34: 202011 of cap free
Amount of items: 1
Items: 
Size: 797990 Color: 1

Bin 35: 203088 of cap free
Amount of items: 1
Items: 
Size: 796913 Color: 0

Bin 36: 212463 of cap free
Amount of items: 1
Items: 
Size: 787538 Color: 1

Bin 37: 212910 of cap free
Amount of items: 1
Items: 
Size: 787091 Color: 1

Bin 38: 214583 of cap free
Amount of items: 1
Items: 
Size: 785418 Color: 1

Bin 39: 219926 of cap free
Amount of items: 1
Items: 
Size: 780075 Color: 1

Bin 40: 228411 of cap free
Amount of items: 1
Items: 
Size: 771590 Color: 0

Bin 41: 238949 of cap free
Amount of items: 1
Items: 
Size: 761052 Color: 0

Bin 42: 240945 of cap free
Amount of items: 1
Items: 
Size: 759056 Color: 1

Bin 43: 244907 of cap free
Amount of items: 1
Items: 
Size: 755094 Color: 1

Bin 44: 256749 of cap free
Amount of items: 1
Items: 
Size: 743252 Color: 0

Bin 45: 258137 of cap free
Amount of items: 1
Items: 
Size: 741864 Color: 1

Bin 46: 259301 of cap free
Amount of items: 1
Items: 
Size: 740700 Color: 0

Bin 47: 263974 of cap free
Amount of items: 1
Items: 
Size: 736027 Color: 1

Bin 48: 273267 of cap free
Amount of items: 1
Items: 
Size: 726734 Color: 1

Bin 49: 296455 of cap free
Amount of items: 1
Items: 
Size: 703546 Color: 0

Bin 50: 304717 of cap free
Amount of items: 1
Items: 
Size: 695284 Color: 0

Bin 51: 318038 of cap free
Amount of items: 1
Items: 
Size: 681963 Color: 1

Bin 52: 337199 of cap free
Amount of items: 1
Items: 
Size: 662802 Color: 0

Bin 53: 341737 of cap free
Amount of items: 1
Items: 
Size: 658264 Color: 1

Total size: 47444352
Total free space: 5555701

