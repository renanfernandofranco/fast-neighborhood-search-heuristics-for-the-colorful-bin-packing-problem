Capicity Bin: 1001
Lower Bound: 667

Bins used: 667
Amount of Colors: 2001

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1997
Size: 250 Color: 3
Size: 250 Color: 29

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1593
Size: 302 Color: 873
Size: 299 Color: 847

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1388
Size: 344 Color: 1246
Size: 294 Color: 782

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1677
Size: 293 Color: 763
Size: 289 Color: 724

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1706
Size: 296 Color: 801
Size: 280 Color: 618

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1624
Size: 317 Color: 1021
Size: 278 Color: 588

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1503
Size: 347 Color: 1271
Size: 270 Color: 477

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1699
Size: 294 Color: 780
Size: 283 Color: 646

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1635
Size: 337 Color: 1195
Size: 255 Color: 149

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1866
Size: 273 Color: 518
Size: 263 Color: 338

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1844
Size: 272 Color: 513
Size: 270 Color: 478

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1675
Size: 299 Color: 844
Size: 283 Color: 649

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1556
Size: 343 Color: 1242
Size: 264 Color: 360

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 1259
Size: 334 Color: 1164
Size: 322 Color: 1070

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1456
Size: 318 Color: 1032
Size: 307 Color: 911

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1979
Size: 255 Color: 157
Size: 253 Color: 100

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1660
Size: 318 Color: 1025
Size: 267 Color: 420

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1555
Size: 313 Color: 976
Size: 294 Color: 781

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 1220
Size: 334 Color: 1174
Size: 328 Color: 1115

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1618
Size: 336 Color: 1187
Size: 259 Color: 235

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1331
Size: 342 Color: 1233
Size: 305 Color: 891

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 338 Color: 1206
Size: 332 Color: 1153
Size: 331 Color: 1133

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1799
Size: 302 Color: 867
Size: 252 Color: 76

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1447
Size: 353 Color: 1309
Size: 274 Color: 539

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 1250
Size: 338 Color: 1209
Size: 318 Color: 1028

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1644
Size: 334 Color: 1169
Size: 256 Color: 183

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1429
Size: 345 Color: 1255
Size: 284 Color: 658

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 1244
Size: 334 Color: 1170
Size: 324 Color: 1096

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1832
Size: 284 Color: 661
Size: 262 Color: 327

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1483
Size: 371 Color: 1426
Size: 250 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1326
Size: 339 Color: 1215
Size: 308 Color: 931

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1958
Size: 261 Color: 293
Size: 253 Color: 92

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1548
Size: 320 Color: 1044
Size: 289 Color: 726

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1751
Size: 284 Color: 664
Size: 282 Color: 633

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1792
Size: 281 Color: 622
Size: 274 Color: 543

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1874
Size: 275 Color: 546
Size: 259 Color: 249

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1439
Size: 319 Color: 1042
Size: 309 Color: 935

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1808
Size: 284 Color: 669
Size: 268 Color: 445

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1811
Size: 299 Color: 845
Size: 252 Color: 62

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1592
Size: 303 Color: 880
Size: 298 Color: 828

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1678
Size: 299 Color: 843
Size: 283 Color: 644

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1904
Size: 264 Color: 366
Size: 262 Color: 317

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1968
Size: 261 Color: 281
Size: 250 Color: 11

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1619
Size: 337 Color: 1190
Size: 258 Color: 204

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1361
Size: 338 Color: 1205
Size: 304 Color: 882

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1602
Size: 300 Color: 850
Size: 299 Color: 841

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1829
Size: 280 Color: 612
Size: 267 Color: 418

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1468
Size: 350 Color: 1286
Size: 273 Color: 519

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1425
Size: 320 Color: 1045
Size: 310 Color: 957

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1989
Size: 253 Color: 102
Size: 251 Color: 57

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1571
Size: 312 Color: 968
Size: 293 Color: 768

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1527
Size: 307 Color: 913
Size: 306 Color: 907

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1899
Size: 266 Color: 403
Size: 261 Color: 288

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1869
Size: 270 Color: 471
Size: 265 Color: 376

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1517
Size: 348 Color: 1281
Size: 266 Color: 402

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1712
Size: 316 Color: 1007
Size: 258 Color: 216

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1370
Size: 353 Color: 1308
Size: 288 Color: 711

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1614
Size: 337 Color: 1201
Size: 260 Color: 260

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1855
Size: 275 Color: 554
Size: 263 Color: 344

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1613
Size: 346 Color: 1266
Size: 251 Color: 55

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1449
Size: 321 Color: 1058
Size: 306 Color: 902

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1344
Size: 325 Color: 1102
Size: 319 Color: 1038

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1683
Size: 321 Color: 1066
Size: 259 Color: 232

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1621
Size: 333 Color: 1159
Size: 262 Color: 326

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1496
Size: 363 Color: 1390
Size: 256 Color: 182

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1394
Size: 359 Color: 1349
Size: 278 Color: 594

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1910
Size: 267 Color: 428
Size: 257 Color: 197

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1740
Size: 305 Color: 890
Size: 264 Color: 349

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1512
Size: 357 Color: 1345
Size: 259 Color: 242

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1509
Size: 314 Color: 988
Size: 303 Color: 879

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1802
Size: 289 Color: 723
Size: 264 Color: 367

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1662
Size: 334 Color: 1167
Size: 251 Color: 35

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1689
Size: 296 Color: 809
Size: 284 Color: 666

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1949
Size: 264 Color: 365
Size: 252 Color: 71

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 1222
Size: 332 Color: 1147
Size: 329 Color: 1125

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1575
Size: 347 Color: 1276
Size: 257 Color: 199

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1742
Size: 307 Color: 910
Size: 261 Color: 294

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1739
Size: 318 Color: 1027
Size: 251 Color: 44

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1813
Size: 277 Color: 572
Size: 273 Color: 515

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1533
Size: 326 Color: 1111
Size: 285 Color: 675

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1681
Size: 311 Color: 962
Size: 270 Color: 468

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1578
Size: 337 Color: 1196
Size: 267 Color: 414

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1918
Size: 266 Color: 401
Size: 257 Color: 187

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1348
Size: 331 Color: 1138
Size: 312 Color: 973

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1454
Size: 374 Color: 1450
Size: 252 Color: 64

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1500
Size: 354 Color: 1330
Size: 264 Color: 352

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1955
Size: 260 Color: 261
Size: 255 Color: 141

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1830
Size: 285 Color: 676
Size: 261 Color: 275

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1759
Size: 288 Color: 709
Size: 275 Color: 549

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1528
Size: 355 Color: 1333
Size: 257 Color: 191

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1638
Size: 307 Color: 922
Size: 284 Color: 660

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1508
Size: 339 Color: 1218
Size: 278 Color: 595

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1563
Size: 330 Color: 1128
Size: 276 Color: 562

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1585
Size: 318 Color: 1033
Size: 284 Color: 667

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1878
Size: 268 Color: 431
Size: 264 Color: 358

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1962
Size: 262 Color: 328
Size: 251 Color: 50

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1582
Size: 326 Color: 1108
Size: 277 Color: 582

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1697
Size: 327 Color: 1113
Size: 250 Color: 33

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1647
Size: 298 Color: 834
Size: 291 Color: 750

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1954
Size: 265 Color: 389
Size: 250 Color: 4

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1412
Size: 331 Color: 1132
Size: 302 Color: 872

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1999
Size: 500 Color: 1994

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1514
Size: 338 Color: 1211
Size: 277 Color: 576

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1931
Size: 268 Color: 435
Size: 252 Color: 77

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1990
Size: 253 Color: 87
Size: 251 Color: 46

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1806
Size: 297 Color: 816
Size: 255 Color: 152

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1606
Size: 345 Color: 1251
Size: 254 Color: 135

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1747
Size: 307 Color: 918
Size: 260 Color: 273

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1758
Size: 290 Color: 737
Size: 273 Color: 521

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1366
Size: 337 Color: 1191
Size: 304 Color: 884

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1441
Size: 322 Color: 1077
Size: 306 Color: 903

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1537
Size: 308 Color: 932
Size: 302 Color: 861

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1828
Size: 276 Color: 558
Size: 271 Color: 494

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1516
Size: 355 Color: 1335
Size: 260 Color: 263

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1583
Size: 305 Color: 895
Size: 298 Color: 829

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1801
Size: 298 Color: 832
Size: 256 Color: 173

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1893
Size: 265 Color: 381
Size: 264 Color: 351

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1488
Size: 352 Color: 1302
Size: 268 Color: 442

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1465
Size: 332 Color: 1151
Size: 291 Color: 746

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1652
Size: 322 Color: 1076
Size: 266 Color: 405

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1981
Size: 254 Color: 127
Size: 253 Color: 89

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1915
Size: 262 Color: 321
Size: 262 Color: 311

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1437
Size: 340 Color: 1221
Size: 288 Color: 715

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1655
Size: 297 Color: 823
Size: 289 Color: 729

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1591
Size: 308 Color: 934
Size: 293 Color: 766

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1438
Size: 352 Color: 1307
Size: 276 Color: 560

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1596
Size: 304 Color: 883
Size: 297 Color: 813

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1734
Size: 307 Color: 916
Size: 263 Color: 332

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1770
Size: 308 Color: 927
Size: 253 Color: 109

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1935
Size: 263 Color: 331
Size: 257 Color: 202

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1860
Size: 269 Color: 459
Size: 268 Color: 436

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 1282
Size: 342 Color: 1234
Size: 311 Color: 959

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1879
Size: 267 Color: 417
Size: 265 Color: 380

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1842
Size: 283 Color: 654
Size: 261 Color: 289

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1986
Size: 255 Color: 161
Size: 251 Color: 39

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1920
Size: 266 Color: 404
Size: 256 Color: 172

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1679
Size: 314 Color: 986
Size: 268 Color: 437

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1941
Size: 262 Color: 302
Size: 257 Color: 192

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1310
Size: 332 Color: 1149
Size: 316 Color: 1005

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1723
Size: 310 Color: 956
Size: 263 Color: 340

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1547
Size: 330 Color: 1126
Size: 279 Color: 606

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1992
Size: 252 Color: 73
Size: 250 Color: 7

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1843
Size: 272 Color: 509
Size: 271 Color: 481

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1982
Size: 254 Color: 113
Size: 253 Color: 108

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1362
Size: 335 Color: 1177
Size: 307 Color: 915

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1948
Size: 262 Color: 324
Size: 254 Color: 114

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1891
Size: 275 Color: 550
Size: 254 Color: 126

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1318
Size: 345 Color: 1252
Size: 303 Color: 877

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1411
Size: 345 Color: 1257
Size: 288 Color: 718

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1942
Size: 264 Color: 359
Size: 254 Color: 140

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1825
Size: 279 Color: 611
Size: 269 Color: 453

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1732
Size: 309 Color: 943
Size: 262 Color: 304

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1534
Size: 336 Color: 1188
Size: 275 Color: 544

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1913
Size: 266 Color: 399
Size: 258 Color: 209

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1708
Size: 293 Color: 773
Size: 282 Color: 632

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1603
Size: 315 Color: 997
Size: 284 Color: 657

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1570
Size: 304 Color: 887
Size: 301 Color: 856

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1424
Size: 345 Color: 1254
Size: 285 Color: 681

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1363
Size: 335 Color: 1184
Size: 306 Color: 909

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1897
Size: 269 Color: 467
Size: 259 Color: 250

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1803
Size: 284 Color: 663
Size: 269 Color: 460

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1648
Size: 310 Color: 949
Size: 279 Color: 601

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1859
Size: 285 Color: 686
Size: 252 Color: 82

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1369
Size: 346 Color: 1261
Size: 295 Color: 798

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1922
Size: 263 Color: 334
Size: 259 Color: 248

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1768
Size: 310 Color: 948
Size: 252 Color: 84

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1782
Size: 286 Color: 692
Size: 272 Color: 503

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1761
Size: 295 Color: 799
Size: 268 Color: 448

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1656
Size: 298 Color: 830
Size: 288 Color: 713

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1804
Size: 292 Color: 761
Size: 261 Color: 299

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1499
Size: 320 Color: 1053
Size: 298 Color: 835

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1407
Size: 319 Color: 1036
Size: 314 Color: 990

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1564
Size: 340 Color: 1226
Size: 266 Color: 400

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1342
Size: 355 Color: 1337
Size: 290 Color: 743

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1372
Size: 346 Color: 1270
Size: 294 Color: 786

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1607
Size: 345 Color: 1258
Size: 253 Color: 101

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1821
Size: 298 Color: 825
Size: 250 Color: 12

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1384
Size: 321 Color: 1060
Size: 318 Color: 1030

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1862
Size: 271 Color: 495
Size: 265 Color: 382

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 1264
Size: 334 Color: 1166
Size: 321 Color: 1063

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1895
Size: 264 Color: 370
Size: 264 Color: 362

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1294
Size: 334 Color: 1165
Size: 317 Color: 1017

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1625
Size: 338 Color: 1208
Size: 257 Color: 201

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1416
Size: 344 Color: 1249
Size: 288 Color: 721

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1867
Size: 269 Color: 458
Size: 266 Color: 394

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1916
Size: 268 Color: 443
Size: 256 Color: 179

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1703
Size: 308 Color: 926
Size: 268 Color: 440

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1888
Size: 267 Color: 416
Size: 262 Color: 316

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1405
Size: 323 Color: 1086
Size: 311 Color: 963

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1890
Size: 274 Color: 531
Size: 255 Color: 154

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1340
Size: 333 Color: 1161
Size: 312 Color: 964

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1701
Size: 319 Color: 1041
Size: 257 Color: 193

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1777
Size: 310 Color: 952
Size: 250 Color: 6

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1848
Size: 272 Color: 510
Size: 269 Color: 461

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1446
Size: 331 Color: 1139
Size: 296 Color: 808

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1433
Size: 332 Color: 1150
Size: 297 Color: 821

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1374
Size: 358 Color: 1346
Size: 282 Color: 638

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1513
Size: 362 Color: 1386
Size: 253 Color: 96

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1486
Size: 350 Color: 1292
Size: 271 Color: 490

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1343
Size: 354 Color: 1327
Size: 291 Color: 748

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1851
Size: 270 Color: 473
Size: 270 Color: 474

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1673
Size: 328 Color: 1117
Size: 255 Color: 145

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1932
Size: 268 Color: 441
Size: 252 Color: 68

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 1248
Size: 336 Color: 1189
Size: 321 Color: 1064

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1669
Size: 331 Color: 1136
Size: 253 Color: 85

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1951
Size: 258 Color: 223
Size: 258 Color: 215

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1774
Size: 293 Color: 769
Size: 267 Color: 415

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1796
Size: 302 Color: 866
Size: 252 Color: 65

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1876
Size: 268 Color: 439
Size: 265 Color: 375

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1746
Size: 316 Color: 1008
Size: 251 Color: 51

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1793
Size: 280 Color: 616
Size: 275 Color: 557

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1400
Size: 343 Color: 1238
Size: 292 Color: 755

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1784
Size: 283 Color: 653
Size: 274 Color: 529

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1907
Size: 265 Color: 384
Size: 260 Color: 272

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1481
Size: 324 Color: 1089
Size: 297 Color: 822

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1520
Size: 359 Color: 1354
Size: 254 Color: 138

Bin 217: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 52
Size: 250 Color: 24
Size: 250 Color: 15
Size: 250 Color: 9

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1930
Size: 264 Color: 364
Size: 257 Color: 198

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1906
Size: 275 Color: 555
Size: 251 Color: 45

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1637
Size: 296 Color: 806
Size: 295 Color: 791

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1584
Size: 309 Color: 939
Size: 294 Color: 779

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1651
Size: 333 Color: 1156
Size: 255 Color: 160

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1530
Size: 335 Color: 1180
Size: 277 Color: 570

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1332
Size: 336 Color: 1186
Size: 310 Color: 954

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1471
Size: 328 Color: 1114
Size: 295 Color: 790

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1692
Size: 322 Color: 1071
Size: 257 Color: 194

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1380
Size: 359 Color: 1356
Size: 280 Color: 617

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1672
Size: 293 Color: 767
Size: 290 Color: 740

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1601
Size: 349 Color: 1284
Size: 250 Color: 18

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1539
Size: 316 Color: 1002
Size: 294 Color: 789

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1938
Size: 262 Color: 307
Size: 257 Color: 203

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1590
Size: 316 Color: 1006
Size: 285 Color: 674

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1448
Size: 351 Color: 1300
Size: 276 Color: 559

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1817
Size: 290 Color: 736
Size: 260 Color: 257

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1984
Size: 255 Color: 162
Size: 251 Color: 38

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1444
Size: 368 Color: 1410
Size: 259 Color: 244

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1724
Size: 322 Color: 1073
Size: 251 Color: 36

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1785
Size: 290 Color: 732
Size: 267 Color: 411

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1476
Size: 359 Color: 1352
Size: 263 Color: 342

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1936
Size: 260 Color: 268
Size: 260 Color: 252

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1288
Size: 335 Color: 1183
Size: 316 Color: 1003

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1663
Size: 332 Color: 1145
Size: 252 Color: 79

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1383
Size: 337 Color: 1197
Size: 302 Color: 859

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1814
Size: 285 Color: 682
Size: 265 Color: 374

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1707
Size: 317 Color: 1016
Size: 258 Color: 221

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1991
Size: 252 Color: 81
Size: 252 Color: 69

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1462
Size: 374 Color: 1445
Size: 250 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1630
Size: 302 Color: 869
Size: 291 Color: 751

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1809
Size: 279 Color: 610
Size: 272 Color: 506

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1473
Size: 353 Color: 1319
Size: 269 Color: 454

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 263 Color: 343
Size: 262 Color: 318
Size: 476 Color: 1908

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1736
Size: 299 Color: 838
Size: 271 Color: 488

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1379
Size: 345 Color: 1260
Size: 294 Color: 784

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1577
Size: 342 Color: 1237
Size: 262 Color: 309

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1776
Size: 310 Color: 950
Size: 250 Color: 20

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1653
Size: 309 Color: 942
Size: 278 Color: 598

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1728
Size: 288 Color: 716
Size: 284 Color: 670

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1727
Size: 318 Color: 1029
Size: 254 Color: 133

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1357
Size: 351 Color: 1298
Size: 291 Color: 752

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1494
Size: 310 Color: 953
Size: 309 Color: 941

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1353
Size: 333 Color: 1163
Size: 309 Color: 936

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1783
Size: 294 Color: 787
Size: 264 Color: 354

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1779
Size: 302 Color: 871
Size: 257 Color: 186

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1850
Size: 278 Color: 586
Size: 263 Color: 330

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1415
Size: 332 Color: 1142
Size: 300 Color: 851

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1857
Size: 273 Color: 520
Size: 264 Color: 353

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1561
Size: 312 Color: 966
Size: 294 Color: 776

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1943
Size: 262 Color: 315
Size: 256 Color: 175

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1729
Size: 292 Color: 758
Size: 279 Color: 605

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1413
Size: 352 Color: 1303
Size: 281 Color: 629

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1797
Size: 279 Color: 602
Size: 275 Color: 551

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1467
Size: 343 Color: 1239
Size: 280 Color: 614

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1773
Size: 283 Color: 647
Size: 277 Color: 578

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1526
Size: 317 Color: 1010
Size: 296 Color: 811

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1972
Size: 258 Color: 225
Size: 253 Color: 104

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1957
Size: 257 Color: 196
Size: 257 Color: 190

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1921
Size: 261 Color: 287
Size: 261 Color: 276

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1816
Size: 297 Color: 817
Size: 253 Color: 99

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1609
Size: 320 Color: 1051
Size: 278 Color: 592

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1550
Size: 354 Color: 1328
Size: 255 Color: 142

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 1185
Size: 333 Color: 1157
Size: 332 Color: 1154

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1474
Size: 329 Color: 1123
Size: 293 Color: 771

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1338
Size: 352 Color: 1305
Size: 293 Color: 772

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1873
Size: 273 Color: 524
Size: 261 Color: 295

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 1275
Size: 343 Color: 1241
Size: 311 Color: 960

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1377
Size: 326 Color: 1110
Size: 314 Color: 984

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1950
Size: 260 Color: 267
Size: 256 Color: 163

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1902
Size: 275 Color: 556
Size: 252 Color: 78

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1717
Size: 289 Color: 728
Size: 285 Color: 678

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1569
Size: 315 Color: 993
Size: 290 Color: 738

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1469
Size: 371 Color: 1427
Size: 252 Color: 83

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1995
Size: 251 Color: 47
Size: 250 Color: 2

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1544
Size: 324 Color: 1093
Size: 285 Color: 684

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1933
Size: 260 Color: 265
Size: 260 Color: 259

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1883
Size: 268 Color: 438
Size: 263 Color: 347

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1642
Size: 319 Color: 1035
Size: 271 Color: 487

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1402
Size: 330 Color: 1130
Size: 305 Color: 900

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1519
Size: 349 Color: 1283
Size: 264 Color: 372

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1720
Size: 321 Color: 1056
Size: 253 Color: 94

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1682
Size: 329 Color: 1122
Size: 252 Color: 66

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1976
Size: 255 Color: 158
Size: 254 Color: 132

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1807
Size: 283 Color: 650
Size: 269 Color: 463

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1434
Size: 372 Color: 1432
Size: 257 Color: 195

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1289
Size: 332 Color: 1143
Size: 319 Color: 1040

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1579
Size: 320 Color: 1043
Size: 284 Color: 659

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1834
Size: 277 Color: 581
Size: 268 Color: 446

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1631
Size: 341 Color: 1228
Size: 252 Color: 74

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1695
Size: 303 Color: 878
Size: 274 Color: 532

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1864
Size: 280 Color: 615
Size: 256 Color: 164

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1538
Size: 350 Color: 1295
Size: 260 Color: 253

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1833
Size: 288 Color: 717
Size: 258 Color: 218

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1698
Size: 314 Color: 991
Size: 263 Color: 346

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1781
Size: 286 Color: 688
Size: 272 Color: 502

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1524
Size: 339 Color: 1214
Size: 274 Color: 535

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1713
Size: 307 Color: 912
Size: 267 Color: 426

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1861
Size: 277 Color: 580
Size: 260 Color: 251

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1540
Size: 359 Color: 1358
Size: 251 Color: 54

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1545
Size: 307 Color: 914
Size: 302 Color: 874

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1536
Size: 307 Color: 917
Size: 303 Color: 875

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1334
Size: 351 Color: 1297
Size: 295 Color: 794

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1743
Size: 317 Color: 1012
Size: 251 Color: 43

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1952
Size: 261 Color: 297
Size: 255 Color: 155

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1491
Size: 333 Color: 1155
Size: 287 Color: 703

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1551
Size: 323 Color: 1088
Size: 286 Color: 695

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1436
Size: 343 Color: 1245
Size: 286 Color: 690

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 1265
Size: 330 Color: 1129
Size: 325 Color: 1099

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1819
Size: 280 Color: 619
Size: 270 Color: 476

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1515
Size: 352 Color: 1306
Size: 263 Color: 348

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1498
Size: 359 Color: 1350
Size: 259 Color: 233

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 1285
Size: 341 Color: 1229
Size: 311 Color: 961

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1845
Size: 274 Color: 540
Size: 268 Color: 444

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1865
Size: 284 Color: 672
Size: 252 Color: 61

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1604
Size: 315 Color: 992
Size: 284 Color: 665

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1711
Size: 295 Color: 793
Size: 280 Color: 613

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1778
Size: 281 Color: 623
Size: 278 Color: 587

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1733
Size: 294 Color: 778
Size: 277 Color: 575

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1977
Size: 256 Color: 168
Size: 253 Color: 95

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1837
Size: 275 Color: 548
Size: 270 Color: 479

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1827
Size: 296 Color: 812
Size: 251 Color: 42

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1633
Size: 305 Color: 897
Size: 288 Color: 719

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1744
Size: 284 Color: 662
Size: 284 Color: 655

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1559
Size: 312 Color: 967
Size: 294 Color: 783

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1939
Size: 263 Color: 329
Size: 256 Color: 170

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1293
Size: 341 Color: 1231
Size: 310 Color: 951

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1731
Size: 297 Color: 820
Size: 274 Color: 542

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1988
Size: 254 Color: 130
Size: 250 Color: 31

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1608
Size: 310 Color: 946
Size: 288 Color: 714

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1475
Size: 365 Color: 1397
Size: 257 Color: 200

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1421
Size: 360 Color: 1367
Size: 271 Color: 492

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 1280
Size: 337 Color: 1198
Size: 316 Color: 1001

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1790
Size: 282 Color: 636
Size: 273 Color: 522

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1464
Size: 346 Color: 1262
Size: 277 Color: 568

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1645
Size: 305 Color: 901
Size: 285 Color: 680

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1381
Size: 329 Color: 1124
Size: 310 Color: 945

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1395
Size: 320 Color: 1048
Size: 317 Color: 1014

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1996
Size: 250 Color: 30
Size: 250 Color: 14

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1522
Size: 323 Color: 1081
Size: 290 Color: 739

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1980
Size: 254 Color: 121
Size: 254 Color: 110

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1875
Size: 274 Color: 527
Size: 260 Color: 271

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1466
Size: 372 Color: 1430
Size: 251 Color: 41

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1597
Size: 321 Color: 1061
Size: 279 Color: 600

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1557
Size: 355 Color: 1336
Size: 252 Color: 75

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1769
Size: 293 Color: 765
Size: 268 Color: 434

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1919
Size: 269 Color: 466
Size: 254 Color: 136

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1418
Size: 342 Color: 1235
Size: 289 Color: 731

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1442
Size: 331 Color: 1131
Size: 296 Color: 802

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1668
Size: 323 Color: 1082
Size: 261 Color: 283

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1529
Size: 312 Color: 970
Size: 300 Color: 855

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1826
Size: 281 Color: 625
Size: 267 Color: 421

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1737
Size: 286 Color: 689
Size: 283 Color: 652

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1661
Size: 325 Color: 1101
Size: 260 Color: 269

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1506
Size: 317 Color: 1015
Size: 300 Color: 852

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1973
Size: 256 Color: 184
Size: 254 Color: 129

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1917
Size: 262 Color: 310
Size: 261 Color: 278

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1710
Size: 321 Color: 1057
Size: 254 Color: 123

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1398
Size: 318 Color: 1034
Size: 317 Color: 1019

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1877
Size: 283 Color: 643
Size: 250 Color: 21

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1378
Size: 323 Color: 1079
Size: 316 Color: 999

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1767
Size: 308 Color: 925
Size: 254 Color: 115

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1457
Size: 329 Color: 1120
Size: 296 Color: 810

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1750
Size: 312 Color: 969
Size: 255 Color: 143

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1960
Size: 259 Color: 234
Size: 254 Color: 137

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1763
Size: 289 Color: 725
Size: 274 Color: 530

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1543
Size: 359 Color: 1355
Size: 250 Color: 16

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1894
Size: 266 Color: 395
Size: 262 Color: 303

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1665
Size: 321 Color: 1055
Size: 263 Color: 345

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1775
Size: 302 Color: 870
Size: 258 Color: 214

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1868
Size: 268 Color: 432
Size: 267 Color: 412

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1688
Size: 315 Color: 994
Size: 265 Color: 385

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1786
Size: 278 Color: 593
Size: 278 Color: 584

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1566
Size: 338 Color: 1210
Size: 268 Color: 451

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1482
Size: 316 Color: 1004
Size: 305 Color: 899

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1762
Size: 313 Color: 982
Size: 250 Color: 25

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1572
Size: 353 Color: 1315
Size: 252 Color: 63

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1484
Size: 325 Color: 1097
Size: 296 Color: 804

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1947
Size: 261 Color: 292
Size: 256 Color: 181

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1896
Size: 276 Color: 566
Size: 252 Color: 70

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1351
Size: 322 Color: 1068
Size: 320 Color: 1049

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1858
Size: 272 Color: 512
Size: 265 Color: 388

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1772
Size: 309 Color: 937
Size: 251 Color: 56

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1558
Size: 353 Color: 1316
Size: 254 Color: 131

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1822
Size: 283 Color: 641
Size: 265 Color: 390

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1359
Size: 343 Color: 1240
Size: 299 Color: 837

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1722
Size: 323 Color: 1087
Size: 250 Color: 19

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1554
Size: 326 Color: 1103
Size: 282 Color: 631

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1726
Size: 314 Color: 989
Size: 258 Color: 219

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1725
Size: 298 Color: 836
Size: 275 Color: 545

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1849
Size: 282 Color: 639
Size: 259 Color: 239

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1396
Size: 350 Color: 1291
Size: 286 Color: 694

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1599
Size: 335 Color: 1178
Size: 265 Color: 387

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1623
Size: 339 Color: 1212
Size: 256 Color: 169

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1296
Size: 328 Color: 1116
Size: 322 Color: 1072

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1657
Size: 310 Color: 955
Size: 276 Color: 565

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1632
Size: 306 Color: 908
Size: 287 Color: 700

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1780
Size: 282 Color: 634
Size: 277 Color: 574

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1565
Size: 332 Color: 1141
Size: 274 Color: 528

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1889
Size: 266 Color: 398
Size: 263 Color: 336

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1617
Size: 322 Color: 1074
Size: 274 Color: 534

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1871
Size: 277 Color: 569
Size: 258 Color: 207

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1360
Size: 334 Color: 1171
Size: 308 Color: 930

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1923
Size: 261 Color: 286
Size: 261 Color: 277

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1684
Size: 324 Color: 1095
Size: 256 Color: 166

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1670
Size: 305 Color: 894
Size: 278 Color: 597

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1574
Size: 350 Color: 1287
Size: 255 Color: 153

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1364
Size: 334 Color: 1172
Size: 307 Color: 921

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1702
Size: 295 Color: 795
Size: 281 Color: 628

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1937
Size: 267 Color: 422
Size: 253 Color: 107

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 1247
Size: 335 Color: 1182
Size: 322 Color: 1069

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1620
Size: 304 Color: 886
Size: 291 Color: 744

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1518
Size: 354 Color: 1329
Size: 259 Color: 247

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1325
Size: 339 Color: 1216
Size: 308 Color: 928

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1459
Size: 353 Color: 1320
Size: 272 Color: 505

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1463
Size: 342 Color: 1236
Size: 282 Color: 630

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1549
Size: 310 Color: 947
Size: 299 Color: 839

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1900
Size: 266 Color: 406
Size: 261 Color: 296

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1324
Size: 353 Color: 1322
Size: 294 Color: 774

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1754
Size: 287 Color: 699
Size: 279 Color: 609

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1417
Size: 362 Color: 1385
Size: 270 Color: 480

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1956
Size: 260 Color: 258
Size: 255 Color: 156

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1667
Size: 320 Color: 1047
Size: 264 Color: 368

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1927
Size: 267 Color: 410
Size: 254 Color: 125

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1399
Size: 362 Color: 1382
Size: 273 Color: 525

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1443
Size: 320 Color: 1050
Size: 307 Color: 923

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1553
Size: 308 Color: 924
Size: 300 Color: 849

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 2000
Size: 250 Color: 26
Size: 250 Color: 1

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1490
Size: 340 Color: 1224
Size: 280 Color: 620

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1612
Size: 321 Color: 1059
Size: 277 Color: 571

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1940
Size: 266 Color: 396
Size: 253 Color: 106

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1504
Size: 324 Color: 1091
Size: 293 Color: 764

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1925
Size: 271 Color: 484
Size: 250 Color: 17

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1409
Size: 346 Color: 1268
Size: 287 Color: 701

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1882
Size: 273 Color: 517
Size: 258 Color: 213

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1649
Size: 335 Color: 1181
Size: 254 Color: 118

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1479
Size: 337 Color: 1192
Size: 284 Color: 656

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1835
Size: 277 Color: 583
Size: 268 Color: 433

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1945
Size: 259 Color: 237
Size: 259 Color: 229

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1643
Size: 299 Color: 846
Size: 291 Color: 753

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1721
Size: 304 Color: 888
Size: 269 Color: 455

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1622
Size: 305 Color: 896
Size: 290 Color: 742

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1798
Size: 302 Color: 868
Size: 252 Color: 60

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1872
Size: 281 Color: 627
Size: 253 Color: 88

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1505
Size: 323 Color: 1083
Size: 294 Color: 785

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1451
Size: 320 Color: 1052
Size: 306 Color: 906

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1588
Size: 315 Color: 996
Size: 286 Color: 693

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1993
Size: 252 Color: 72
Size: 250 Color: 27

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1715
Size: 290 Color: 741
Size: 284 Color: 668

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1911
Size: 262 Color: 323
Size: 262 Color: 314

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1700
Size: 323 Color: 1085
Size: 254 Color: 119

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1480
Size: 347 Color: 1277
Size: 274 Color: 538

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1901
Size: 266 Color: 392
Size: 261 Color: 274

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1680
Size: 306 Color: 904
Size: 275 Color: 547

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1389
Size: 353 Color: 1313
Size: 285 Color: 685

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1472
Size: 328 Color: 1119
Size: 295 Color: 800

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1440
Size: 361 Color: 1375
Size: 267 Color: 419

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1820
Size: 286 Color: 696
Size: 262 Color: 305

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1944
Size: 260 Color: 262
Size: 258 Color: 205

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1562
Size: 331 Color: 1134
Size: 275 Color: 552

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1627
Size: 309 Color: 944
Size: 285 Color: 677

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1953
Size: 261 Color: 279
Size: 254 Color: 112

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 1243
Size: 341 Color: 1232
Size: 317 Color: 1023

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1839
Size: 286 Color: 697
Size: 259 Color: 245

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1690
Size: 315 Color: 998
Size: 264 Color: 363

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1961
Size: 257 Color: 189
Size: 256 Color: 165

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1535
Size: 313 Color: 981
Size: 297 Color: 818

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1431
Size: 353 Color: 1317
Size: 276 Color: 561

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1650
Size: 306 Color: 905
Size: 283 Color: 648

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1985
Size: 255 Color: 144
Size: 251 Color: 40

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1757
Size: 295 Color: 797
Size: 269 Color: 464

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1847
Size: 290 Color: 733
Size: 251 Color: 48

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1493
Size: 324 Color: 1094
Size: 296 Color: 803

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1654
Size: 299 Color: 842
Size: 288 Color: 707

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1403
Size: 323 Color: 1078
Size: 312 Color: 972

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1975
Size: 257 Color: 188
Size: 253 Color: 98

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1541
Size: 354 Color: 1323
Size: 256 Color: 167

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1964
Size: 258 Color: 228
Size: 255 Color: 146

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1521
Size: 346 Color: 1263
Size: 267 Color: 424

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1598
Size: 338 Color: 1204
Size: 262 Color: 301

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1611
Size: 320 Color: 1046
Size: 278 Color: 589

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1987
Size: 255 Color: 150
Size: 250 Color: 5

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1671
Size: 311 Color: 958
Size: 272 Color: 501

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1856
Size: 286 Color: 687
Size: 252 Color: 80

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1841
Size: 279 Color: 603
Size: 265 Color: 377

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1616
Size: 318 Color: 1031
Size: 278 Color: 591

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1748
Size: 317 Color: 1018
Size: 250 Color: 8

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1787
Size: 298 Color: 833
Size: 258 Color: 222

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1705
Size: 326 Color: 1106
Size: 250 Color: 32

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1458
Size: 363 Color: 1387
Size: 262 Color: 319

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1903
Size: 264 Color: 355
Size: 263 Color: 339

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1687
Size: 299 Color: 840
Size: 281 Color: 624

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1502
Size: 339 Color: 1219
Size: 279 Color: 608

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1983
Size: 254 Color: 128
Size: 253 Color: 97

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1741
Size: 319 Color: 1039
Size: 250 Color: 34

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1745
Size: 313 Color: 978
Size: 254 Color: 116

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1301
Size: 345 Color: 1256
Size: 304 Color: 885

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1628
Size: 340 Color: 1225
Size: 254 Color: 122

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1887
Size: 268 Color: 449
Size: 261 Color: 284

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1428
Size: 328 Color: 1118
Size: 302 Color: 864

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1568
Size: 353 Color: 1314
Size: 252 Color: 59

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1800
Size: 283 Color: 640
Size: 271 Color: 491

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1709
Size: 321 Color: 1067
Size: 254 Color: 124

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1795
Size: 287 Color: 706
Size: 267 Color: 423

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1836
Size: 274 Color: 526
Size: 271 Color: 486

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1714
Size: 305 Color: 898
Size: 269 Color: 456

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1912
Size: 272 Color: 499
Size: 252 Color: 67

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1840
Size: 276 Color: 563
Size: 268 Color: 450

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1595
Size: 331 Color: 1137
Size: 270 Color: 470

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1738
Size: 313 Color: 977
Size: 256 Color: 174

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1753
Size: 288 Color: 710
Size: 278 Color: 585

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1341
Size: 353 Color: 1321
Size: 292 Color: 756

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1898
Size: 266 Color: 407
Size: 261 Color: 291

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1586
Size: 339 Color: 1217
Size: 263 Color: 333

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1730
Size: 307 Color: 920
Size: 264 Color: 357

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1998
Size: 250 Color: 28
Size: 250 Color: 23

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1815
Size: 285 Color: 683
Size: 265 Color: 386

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1634
Size: 332 Color: 1144
Size: 260 Color: 256

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1914
Size: 271 Color: 483
Size: 253 Color: 90

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1974
Size: 256 Color: 180
Size: 254 Color: 111

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1693
Size: 307 Color: 919
Size: 272 Color: 498

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1371
Size: 325 Color: 1100
Size: 315 Color: 995

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1347
Size: 346 Color: 1267
Size: 297 Color: 814

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1497
Size: 347 Color: 1274
Size: 272 Color: 508

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1966
Size: 258 Color: 208
Size: 254 Color: 139

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1674
Size: 321 Color: 1054
Size: 262 Color: 308

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1477
Size: 333 Color: 1160
Size: 289 Color: 727

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1626
Size: 302 Color: 862
Size: 292 Color: 762

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1970
Size: 259 Color: 240
Size: 252 Color: 58

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1810
Size: 297 Color: 824
Size: 254 Color: 120

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1419
Size: 337 Color: 1199
Size: 294 Color: 788

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1909
Size: 266 Color: 393
Size: 259 Color: 230

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1567
Size: 347 Color: 1272
Size: 258 Color: 206

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1852
Size: 271 Color: 493
Size: 268 Color: 447

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1691
Size: 296 Color: 805
Size: 283 Color: 642

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1594
Size: 312 Color: 971
Size: 289 Color: 730

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1791
Size: 279 Color: 604
Size: 276 Color: 567

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1408
Size: 317 Color: 1013
Size: 316 Color: 1000

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1892
Size: 276 Color: 564
Size: 253 Color: 93

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1455
Size: 338 Color: 1203
Size: 287 Color: 705

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1414
Size: 345 Color: 1253
Size: 288 Color: 722

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1771
Size: 290 Color: 735
Size: 270 Color: 472

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 1175
Size: 267 Color: 427
Size: 399 Color: 1587

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1641
Size: 298 Color: 831
Size: 292 Color: 759

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1531
Size: 308 Color: 933
Size: 304 Color: 881

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1704
Size: 309 Color: 938
Size: 267 Color: 425

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1766
Size: 297 Color: 815
Size: 265 Color: 378

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 1202
Size: 332 Color: 1152
Size: 332 Color: 1146

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1470
Size: 335 Color: 1179
Size: 288 Color: 720

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1589
Size: 348 Color: 1279
Size: 253 Color: 91

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1666
Size: 293 Color: 770
Size: 291 Color: 745

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1299
Size: 326 Color: 1104
Size: 324 Color: 1090

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1489
Size: 335 Color: 1176
Size: 285 Color: 673

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1926
Size: 263 Color: 335
Size: 258 Color: 212

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1646
Size: 325 Color: 1098
Size: 264 Color: 369

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1581
Size: 305 Color: 893
Size: 298 Color: 827

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1959
Size: 259 Color: 236
Size: 255 Color: 147

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1605
Size: 334 Color: 1173
Size: 265 Color: 379

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1853
Size: 282 Color: 637
Size: 256 Color: 178

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1639
Size: 297 Color: 819
Size: 294 Color: 777

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1905
Size: 265 Color: 383
Size: 261 Color: 298

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1576
Size: 317 Color: 1022
Size: 287 Color: 698

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1492
Size: 333 Color: 1162
Size: 287 Color: 702

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1963
Size: 262 Color: 322
Size: 251 Color: 37

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1676
Size: 326 Color: 1107
Size: 256 Color: 177

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1510
Size: 337 Color: 1193
Size: 279 Color: 607

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1393
Size: 353 Color: 1311
Size: 284 Color: 671

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1542
Size: 338 Color: 1207
Size: 272 Color: 507

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1735
Size: 303 Color: 876
Size: 267 Color: 430

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1573
Size: 309 Color: 940
Size: 296 Color: 807

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1924
Size: 265 Color: 391
Size: 256 Color: 176

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1452
Size: 364 Color: 1392
Size: 262 Color: 320

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1863
Size: 274 Color: 533
Size: 262 Color: 313

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1823
Size: 277 Color: 577
Size: 271 Color: 497

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1485
Size: 347 Color: 1273
Size: 274 Color: 537

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1532
Size: 352 Color: 1304
Size: 259 Color: 238

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1818
Size: 288 Color: 712
Size: 262 Color: 300

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1610
Size: 327 Color: 1112
Size: 271 Color: 482

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1312
Size: 329 Color: 1121
Size: 319 Color: 1037

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1812
Size: 294 Color: 775
Size: 256 Color: 171

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1789
Size: 295 Color: 792
Size: 260 Color: 255

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1423
Size: 364 Color: 1391
Size: 266 Color: 397

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1971
Size: 261 Color: 282
Size: 250 Color: 10

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1884
Size: 266 Color: 408
Size: 265 Color: 373

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1629
Size: 339 Color: 1213
Size: 255 Color: 151

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1640
Size: 312 Color: 974
Size: 278 Color: 590

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1406
Size: 356 Color: 1339
Size: 277 Color: 579

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1460
Size: 313 Color: 980
Size: 312 Color: 965

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1685
Size: 326 Color: 1109
Size: 254 Color: 134

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1928
Size: 262 Color: 325
Size: 259 Color: 246

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1636
Size: 332 Color: 1148
Size: 259 Color: 243

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1495
Size: 360 Color: 1365
Size: 259 Color: 241

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1719
Size: 305 Color: 889
Size: 269 Color: 457

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1453
Size: 314 Color: 987
Size: 312 Color: 975

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1659
Size: 302 Color: 865
Size: 283 Color: 645

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1664
Size: 317 Color: 1011
Size: 267 Color: 429

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1718
Size: 292 Color: 757
Size: 282 Color: 635

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1794
Size: 291 Color: 754
Size: 264 Color: 371

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1461
Size: 334 Color: 1168
Size: 290 Color: 734

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1404
Size: 333 Color: 1158
Size: 301 Color: 857

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1435
Size: 321 Color: 1065
Size: 308 Color: 929

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1881
Size: 273 Color: 514
Size: 258 Color: 224

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1600
Size: 300 Color: 854
Size: 300 Color: 853

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1615
Size: 313 Color: 983
Size: 283 Color: 651

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1756
Size: 305 Color: 892
Size: 260 Color: 266

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1885
Size: 266 Color: 409
Size: 264 Color: 350

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1580
Size: 341 Color: 1230
Size: 262 Color: 306

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1846
Size: 273 Color: 523
Size: 268 Color: 452

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1965
Size: 257 Color: 185
Size: 255 Color: 159

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1752
Size: 302 Color: 860
Size: 264 Color: 361

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1967
Size: 258 Color: 227
Size: 253 Color: 105

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1546
Size: 337 Color: 1200
Size: 272 Color: 504

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1764
Size: 291 Color: 747
Size: 272 Color: 511

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1507
Size: 347 Color: 1278
Size: 270 Color: 475

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1401
Size: 318 Color: 1026
Size: 317 Color: 1024

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1716
Size: 323 Color: 1080
Size: 251 Color: 49

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1824
Size: 295 Color: 796
Size: 253 Color: 86

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1749
Size: 317 Color: 1009
Size: 250 Color: 22

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1501
Size: 360 Color: 1368
Size: 258 Color: 211

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1946
Size: 260 Color: 270
Size: 258 Color: 226

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1929
Size: 263 Color: 337
Size: 258 Color: 220

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1886
Size: 269 Color: 465
Size: 261 Color: 290

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1552
Size: 322 Color: 1075
Size: 286 Color: 691

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1831
Size: 275 Color: 553
Size: 271 Color: 489

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1525
Size: 340 Color: 1223
Size: 273 Color: 516

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1420
Size: 350 Color: 1290
Size: 281 Color: 626

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1694
Size: 292 Color: 760
Size: 287 Color: 704

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1523
Size: 314 Color: 985
Size: 299 Color: 848

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1765
Size: 301 Color: 858
Size: 261 Color: 285

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1838
Size: 274 Color: 536
Size: 271 Color: 485

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1978
Size: 255 Color: 148
Size: 253 Color: 103

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1422
Size: 361 Color: 1376
Size: 270 Color: 469

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1805
Size: 298 Color: 826
Size: 254 Color: 117

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1969
Size: 260 Color: 254
Size: 251 Color: 53

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1560
Size: 326 Color: 1105
Size: 280 Color: 621

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1755
Size: 288 Color: 708
Size: 277 Color: 573

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1854
Size: 271 Color: 496
Size: 267 Color: 413

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1934
Size: 262 Color: 312
Size: 258 Color: 210

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1880
Size: 274 Color: 541
Size: 258 Color: 217

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1478
Size: 361 Color: 1373
Size: 260 Color: 264

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1511
Size: 331 Color: 1135
Size: 285 Color: 679

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1870
Size: 272 Color: 500
Size: 263 Color: 341

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 1227
Size: 337 Color: 1194
Size: 323 Color: 1084

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1487
Size: 330 Color: 1127
Size: 291 Color: 749

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 1269
Size: 331 Color: 1140
Size: 324 Color: 1092

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1686
Size: 321 Color: 1062
Size: 259 Color: 231

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1658
Size: 317 Color: 1020
Size: 269 Color: 462

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1760
Size: 302 Color: 863
Size: 261 Color: 280

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1696
Size: 313 Color: 979
Size: 264 Color: 356

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1788
Size: 278 Color: 599
Size: 278 Color: 596

Total size: 667667
Total free space: 0

