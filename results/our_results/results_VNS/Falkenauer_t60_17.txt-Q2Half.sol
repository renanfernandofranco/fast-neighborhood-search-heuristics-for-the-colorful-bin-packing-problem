Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 268 Color: 0
Size: 279 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 302 Color: 1
Size: 292 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 264 Color: 1
Size: 258 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 394 Color: 1
Size: 251 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 281 Color: 1
Size: 260 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 333 Color: 1
Size: 256 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 342 Color: 1
Size: 255 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 294 Color: 1
Size: 251 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 267 Color: 1
Size: 264 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 306 Color: 1
Size: 266 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 309 Color: 1
Size: 269 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 294 Color: 1
Size: 270 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 285 Color: 1
Size: 278 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 285 Color: 0
Size: 290 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 279 Color: 1
Size: 262 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 278 Color: 1
Size: 255 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 258 Color: 1
Size: 253 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 257 Color: 1
Size: 251 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 1
Size: 250 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 1
Size: 250 Color: 0

Total size: 20000
Total free space: 0

