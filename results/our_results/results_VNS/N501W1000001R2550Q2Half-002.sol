Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 390543 Color: 1
Size: 320767 Color: 1
Size: 288691 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 412092 Color: 1
Size: 300658 Color: 1
Size: 287251 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 489837 Color: 1
Size: 257537 Color: 1
Size: 252627 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 465186 Color: 1
Size: 267753 Color: 1
Size: 267062 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 421330 Color: 1
Size: 325489 Color: 1
Size: 253182 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 383927 Color: 1
Size: 317680 Color: 1
Size: 298394 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 477553 Color: 1
Size: 260329 Color: 0
Size: 262119 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 401167 Color: 1
Size: 345058 Color: 1
Size: 253776 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 496399 Color: 1
Size: 252718 Color: 1
Size: 250884 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 374908 Color: 1
Size: 326375 Color: 1
Size: 298718 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 407104 Color: 1
Size: 303058 Color: 1
Size: 289839 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 438373 Color: 1
Size: 285972 Color: 1
Size: 275656 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 405896 Color: 1
Size: 342572 Color: 1
Size: 251533 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 390505 Color: 1
Size: 317536 Color: 1
Size: 291960 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 421556 Color: 1
Size: 306592 Color: 1
Size: 271853 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 481615 Color: 1
Size: 265087 Color: 1
Size: 253299 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 497026 Color: 1
Size: 251115 Color: 0
Size: 251860 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 439072 Color: 1
Size: 289264 Color: 1
Size: 271665 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 467870 Color: 1
Size: 279751 Color: 1
Size: 252380 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 405939 Color: 1
Size: 338154 Color: 1
Size: 255908 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 474832 Color: 1
Size: 263814 Color: 1
Size: 261355 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 378259 Color: 1
Size: 342878 Color: 1
Size: 278864 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 387151 Color: 1
Size: 335189 Color: 1
Size: 277661 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 496994 Color: 1
Size: 252309 Color: 1
Size: 250698 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 400522 Color: 1
Size: 303929 Color: 1
Size: 295550 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 369574 Color: 1
Size: 332196 Color: 1
Size: 298231 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 356285 Color: 1
Size: 337030 Color: 1
Size: 306686 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 384634 Color: 1
Size: 328968 Color: 1
Size: 286399 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 485282 Color: 1
Size: 262919 Color: 1
Size: 251800 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 413325 Color: 1
Size: 303155 Color: 1
Size: 283521 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 396120 Color: 1
Size: 349141 Color: 1
Size: 254740 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 417238 Color: 1
Size: 299186 Color: 1
Size: 283577 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 481771 Color: 1
Size: 259660 Color: 1
Size: 258570 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 368721 Color: 1
Size: 340770 Color: 1
Size: 290510 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 413676 Color: 1
Size: 295402 Color: 1
Size: 290923 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 494743 Color: 1
Size: 253814 Color: 1
Size: 251444 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 480072 Color: 1
Size: 267815 Color: 1
Size: 252114 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 404614 Color: 1
Size: 330195 Color: 1
Size: 265192 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 459535 Color: 1
Size: 288773 Color: 1
Size: 251693 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 492641 Color: 1
Size: 255994 Color: 1
Size: 251366 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 435168 Color: 1
Size: 294059 Color: 1
Size: 270774 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 370485 Color: 1
Size: 322105 Color: 1
Size: 307411 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 387740 Color: 1
Size: 341311 Color: 1
Size: 270950 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 437692 Color: 1
Size: 283533 Color: 1
Size: 278776 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 419157 Color: 1
Size: 311557 Color: 1
Size: 269287 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 411171 Color: 1
Size: 294592 Color: 1
Size: 294238 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 436031 Color: 1
Size: 303424 Color: 1
Size: 260546 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 416282 Color: 1
Size: 294669 Color: 1
Size: 289050 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 400908 Color: 1
Size: 307354 Color: 1
Size: 291739 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 409756 Color: 1
Size: 331998 Color: 1
Size: 258247 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 440381 Color: 1
Size: 293912 Color: 1
Size: 265708 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 388949 Color: 1
Size: 351713 Color: 1
Size: 259339 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 462945 Color: 1
Size: 272668 Color: 1
Size: 264388 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 449068 Color: 1
Size: 295995 Color: 1
Size: 254938 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 412176 Color: 1
Size: 317698 Color: 1
Size: 270127 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 459884 Color: 1
Size: 271706 Color: 1
Size: 268411 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 367698 Color: 1
Size: 324017 Color: 1
Size: 308286 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 382427 Color: 1
Size: 311053 Color: 1
Size: 306521 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 386519 Color: 1
Size: 355689 Color: 1
Size: 257793 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 393000 Color: 1
Size: 345452 Color: 1
Size: 261549 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 489446 Color: 1
Size: 258670 Color: 1
Size: 251885 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 380035 Color: 1
Size: 341810 Color: 1
Size: 278156 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 462514 Color: 1
Size: 286589 Color: 1
Size: 250898 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 425355 Color: 1
Size: 306245 Color: 1
Size: 268401 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 472836 Color: 1
Size: 268758 Color: 1
Size: 258407 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 427589 Color: 1
Size: 295387 Color: 1
Size: 277025 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 402645 Color: 1
Size: 312122 Color: 1
Size: 285234 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 389521 Color: 1
Size: 333415 Color: 1
Size: 277065 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 344720 Color: 1
Size: 333409 Color: 1
Size: 321872 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 442684 Color: 1
Size: 289055 Color: 1
Size: 268262 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 356270 Color: 1
Size: 346201 Color: 1
Size: 297530 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 492539 Color: 1
Size: 255763 Color: 1
Size: 251699 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 409608 Color: 1
Size: 331972 Color: 1
Size: 258421 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 475772 Color: 1
Size: 265141 Color: 1
Size: 259088 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 393155 Color: 1
Size: 355369 Color: 1
Size: 251477 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 474316 Color: 1
Size: 272300 Color: 1
Size: 253385 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 391979 Color: 1
Size: 326568 Color: 1
Size: 281454 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 410864 Color: 1
Size: 328526 Color: 1
Size: 260611 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 386281 Color: 1
Size: 352260 Color: 1
Size: 261460 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 466404 Color: 1
Size: 269242 Color: 1
Size: 264355 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 458736 Color: 1
Size: 287397 Color: 1
Size: 253868 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 406709 Color: 1
Size: 331028 Color: 1
Size: 262264 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 369193 Color: 1
Size: 333118 Color: 1
Size: 297690 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 396325 Color: 1
Size: 332195 Color: 1
Size: 271481 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 412441 Color: 1
Size: 322800 Color: 1
Size: 264760 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 419235 Color: 1
Size: 324683 Color: 1
Size: 256083 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 467560 Color: 1
Size: 277994 Color: 1
Size: 254447 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 423753 Color: 1
Size: 319128 Color: 1
Size: 257120 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 358030 Color: 1
Size: 332803 Color: 1
Size: 309168 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 382088 Color: 1
Size: 352152 Color: 1
Size: 265761 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 478630 Color: 1
Size: 267868 Color: 1
Size: 253503 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 391179 Color: 1
Size: 328067 Color: 1
Size: 280755 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 394404 Color: 1
Size: 353578 Color: 1
Size: 252019 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 375640 Color: 1
Size: 357611 Color: 1
Size: 266750 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 460155 Color: 1
Size: 274468 Color: 1
Size: 265378 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 472800 Color: 1
Size: 269668 Color: 1
Size: 257533 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 417037 Color: 1
Size: 325890 Color: 1
Size: 257074 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 428998 Color: 1
Size: 293085 Color: 1
Size: 277918 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 377992 Color: 1
Size: 370613 Color: 1
Size: 251396 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 398482 Color: 1
Size: 347713 Color: 1
Size: 253806 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 377831 Color: 1
Size: 325361 Color: 1
Size: 296809 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 390937 Color: 1
Size: 306293 Color: 1
Size: 302771 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 405713 Color: 1
Size: 310863 Color: 1
Size: 283425 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 478831 Color: 1
Size: 265909 Color: 1
Size: 255261 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 397675 Color: 1
Size: 331031 Color: 1
Size: 271295 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 351159 Color: 1
Size: 340441 Color: 1
Size: 308401 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 412999 Color: 1
Size: 303141 Color: 1
Size: 283861 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 420604 Color: 1
Size: 327190 Color: 1
Size: 252207 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 467593 Color: 1
Size: 278929 Color: 1
Size: 253479 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 359933 Color: 1
Size: 355749 Color: 1
Size: 284319 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 418747 Color: 1
Size: 304560 Color: 1
Size: 276694 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 422241 Color: 1
Size: 292444 Color: 1
Size: 285316 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 421501 Color: 1
Size: 308961 Color: 1
Size: 269539 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 405012 Color: 1
Size: 298978 Color: 1
Size: 296011 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 339555 Color: 1
Size: 337052 Color: 1
Size: 323394 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 377105 Color: 1
Size: 367763 Color: 1
Size: 255133 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 462491 Color: 1
Size: 282459 Color: 1
Size: 255051 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 380238 Color: 1
Size: 360781 Color: 1
Size: 258982 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 378196 Color: 1
Size: 328652 Color: 1
Size: 293153 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 360189 Color: 1
Size: 359075 Color: 1
Size: 280737 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 354224 Color: 1
Size: 331916 Color: 1
Size: 313861 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 454266 Color: 1
Size: 288240 Color: 1
Size: 257495 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 386726 Color: 1
Size: 351662 Color: 1
Size: 261613 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 375156 Color: 1
Size: 328775 Color: 1
Size: 296070 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 391055 Color: 1
Size: 308090 Color: 1
Size: 300856 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 358199 Color: 1
Size: 338529 Color: 1
Size: 303273 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 360858 Color: 1
Size: 329770 Color: 1
Size: 309373 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 437472 Color: 1
Size: 298240 Color: 1
Size: 264289 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 443359 Color: 1
Size: 289804 Color: 1
Size: 266838 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 381942 Color: 1
Size: 365074 Color: 1
Size: 252985 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 372917 Color: 1
Size: 372760 Color: 1
Size: 254324 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 451407 Color: 1
Size: 293019 Color: 1
Size: 255575 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 465190 Color: 1
Size: 270859 Color: 1
Size: 263952 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 375909 Color: 1
Size: 360056 Color: 1
Size: 264036 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 449793 Color: 1
Size: 299746 Color: 1
Size: 250462 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 414544 Color: 1
Size: 312625 Color: 1
Size: 272832 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 344003 Color: 1
Size: 333147 Color: 1
Size: 322851 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 384358 Color: 1
Size: 313295 Color: 1
Size: 302348 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 457485 Color: 1
Size: 276367 Color: 1
Size: 266149 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 350947 Color: 1
Size: 330680 Color: 1
Size: 318374 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 458565 Color: 1
Size: 283847 Color: 1
Size: 257589 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 485395 Color: 1
Size: 258998 Color: 1
Size: 255608 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 368683 Color: 1
Size: 316218 Color: 1
Size: 315100 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 420936 Color: 1
Size: 319430 Color: 1
Size: 259635 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 367239 Color: 1
Size: 348839 Color: 1
Size: 283923 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 397228 Color: 1
Size: 345517 Color: 1
Size: 257256 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 346663 Color: 1
Size: 338700 Color: 1
Size: 314638 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 410573 Color: 1
Size: 329871 Color: 1
Size: 259557 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 370927 Color: 1
Size: 355123 Color: 1
Size: 273951 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 363115 Color: 1
Size: 344399 Color: 1
Size: 292487 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 357284 Color: 1
Size: 353561 Color: 1
Size: 289156 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 355276 Color: 1
Size: 331638 Color: 1
Size: 313087 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 373107 Color: 1
Size: 334811 Color: 1
Size: 292083 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 373965 Color: 1
Size: 346250 Color: 1
Size: 279786 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 379992 Color: 1
Size: 350472 Color: 1
Size: 269537 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 385480 Color: 1
Size: 335455 Color: 1
Size: 279066 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 397752 Color: 1
Size: 341752 Color: 1
Size: 260497 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 399087 Color: 1
Size: 344334 Color: 1
Size: 256580 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 401010 Color: 1
Size: 307785 Color: 1
Size: 291206 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 418295 Color: 1
Size: 330750 Color: 1
Size: 250956 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 384231 Color: 1
Size: 308213 Color: 1
Size: 307557 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 432948 Color: 1
Size: 316880 Color: 1
Size: 250173 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 441769 Color: 1
Size: 301443 Color: 1
Size: 256789 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 464137 Color: 1
Size: 281915 Color: 1
Size: 253949 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 485292 Color: 1
Size: 261711 Color: 1
Size: 252998 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 486846 Color: 1
Size: 259246 Color: 1
Size: 253909 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 497606 Color: 1
Size: 251977 Color: 1
Size: 250418 Color: 0

Total size: 167000167
Total free space: 0

