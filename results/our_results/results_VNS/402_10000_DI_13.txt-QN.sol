Capicity Bin: 8224
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 4114 Color: 273
Size: 738 Color: 139
Size: 696 Color: 136
Size: 684 Color: 135
Size: 652 Color: 130
Size: 576 Color: 121
Size: 304 Color: 79
Size: 232 Color: 53
Size: 228 Color: 52

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 4122 Color: 277
Size: 1563 Color: 208
Size: 1429 Color: 201
Size: 902 Color: 152
Size: 208 Color: 45

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 4738 Color: 287
Size: 2902 Color: 254
Size: 208 Color: 46
Size: 192 Color: 36
Size: 184 Color: 31

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 5168 Color: 295
Size: 3056 Color: 263

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5680 Color: 308
Size: 1612 Color: 212
Size: 932 Color: 161

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5912 Color: 317
Size: 1800 Color: 219
Size: 512 Color: 115

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6299 Color: 329
Size: 1605 Color: 210
Size: 320 Color: 86

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6490 Color: 338
Size: 1446 Color: 203
Size: 288 Color: 77

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6582 Color: 342
Size: 1370 Color: 198
Size: 272 Color: 71

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6632 Color: 345
Size: 1174 Color: 182
Size: 418 Color: 101

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 347
Size: 1180 Color: 184
Size: 376 Color: 94

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6725 Color: 350
Size: 1329 Color: 196
Size: 170 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 354
Size: 1022 Color: 168
Size: 400 Color: 96

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 356
Size: 948 Color: 163
Size: 464 Color: 107

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 357
Size: 915 Color: 156
Size: 492 Color: 113

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 359
Size: 684 Color: 134
Size: 680 Color: 133

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 360
Size: 1175 Color: 183
Size: 182 Color: 29

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 361
Size: 680 Color: 132
Size: 672 Color: 131

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6977 Color: 366
Size: 1041 Color: 173
Size: 206 Color: 44

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 368
Size: 999 Color: 165
Size: 240 Color: 59

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 369
Size: 810 Color: 143
Size: 428 Color: 104

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6993 Color: 370
Size: 1033 Color: 171
Size: 198 Color: 37

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6994 Color: 371
Size: 1026 Color: 169
Size: 204 Color: 40

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7107 Color: 377
Size: 909 Color: 153
Size: 208 Color: 47

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 378
Size: 772 Color: 141
Size: 344 Color: 88

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7112 Color: 379
Size: 712 Color: 138
Size: 400 Color: 97

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7115 Color: 380
Size: 925 Color: 158
Size: 184 Color: 33

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7126 Color: 381
Size: 912 Color: 155
Size: 186 Color: 34

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 382
Size: 861 Color: 146
Size: 236 Color: 58

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7134 Color: 383
Size: 910 Color: 154
Size: 180 Color: 24

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 385
Size: 898 Color: 150
Size: 184 Color: 32

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7143 Color: 386
Size: 901 Color: 151
Size: 180 Color: 26

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7168 Color: 388
Size: 776 Color: 142
Size: 280 Color: 72

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 392
Size: 708 Color: 137
Size: 312 Color: 84

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 394
Size: 598 Color: 128
Size: 376 Color: 93

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7300 Color: 396
Size: 580 Color: 122
Size: 344 Color: 89

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7338 Color: 398
Size: 580 Color: 123
Size: 306 Color: 82

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7380 Color: 400
Size: 588 Color: 124
Size: 256 Color: 65

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7384 Color: 401
Size: 608 Color: 129
Size: 232 Color: 54

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7400 Color: 402
Size: 592 Color: 125
Size: 232 Color: 55

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 324
Size: 2091 Color: 231

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 327
Size: 1300 Color: 194
Size: 742 Color: 140

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6342 Color: 332
Size: 1881 Color: 222

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6489 Color: 337
Size: 1528 Color: 207
Size: 206 Color: 43

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6511 Color: 340
Size: 1712 Color: 215

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6776 Color: 352
Size: 1447 Color: 204

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6972 Color: 365
Size: 1251 Color: 190

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 376
Size: 1131 Color: 180

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 5059 Color: 291
Size: 2987 Color: 259
Size: 176 Color: 23

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 6631 Color: 344
Size: 1125 Color: 178
Size: 466 Color: 108

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 6706 Color: 348
Size: 1324 Color: 195
Size: 192 Color: 35

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6818 Color: 358
Size: 1404 Color: 199

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 387
Size: 1064 Color: 176
Size: 8 Color: 0

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 7188 Color: 390
Size: 1034 Color: 172

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 7304 Color: 397
Size: 918 Color: 157

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 7342 Color: 399
Size: 880 Color: 148

Bin 57: 3 of cap free
Amount of items: 11
Items: 
Size: 4113 Color: 272
Size: 576 Color: 120
Size: 544 Color: 119
Size: 526 Color: 118
Size: 526 Color: 117
Size: 524 Color: 116
Size: 448 Color: 105
Size: 248 Color: 61
Size: 244 Color: 60
Size: 236 Color: 57
Size: 236 Color: 56

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 292
Size: 2978 Color: 258
Size: 176 Color: 22

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 5961 Color: 318
Size: 2260 Color: 235

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 6334 Color: 331
Size: 1887 Color: 223

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 343
Size: 1000 Color: 166
Size: 598 Color: 127

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6786 Color: 353
Size: 1435 Color: 202

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 5706 Color: 311
Size: 2514 Color: 245

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 6106 Color: 323
Size: 2114 Color: 233

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 6292 Color: 328
Size: 1928 Color: 226

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 6472 Color: 336
Size: 1748 Color: 216

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 6978 Color: 367
Size: 1242 Color: 189

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 7176 Color: 389
Size: 1044 Color: 175

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 7193 Color: 391
Size: 1027 Color: 170

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 6173 Color: 326
Size: 2046 Color: 228

Bin 71: 5 of cap free
Amount of items: 2
Items: 
Size: 6803 Color: 355
Size: 1416 Color: 200

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 7135 Color: 384
Size: 1084 Color: 177

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 7207 Color: 393
Size: 1012 Color: 167

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 5650 Color: 307
Size: 2568 Color: 246

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 6072 Color: 322
Size: 2146 Color: 234

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 351
Size: 1464 Color: 205
Size: 16 Color: 1

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 6952 Color: 364
Size: 1266 Color: 192

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 7032 Color: 374
Size: 1186 Color: 186

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 7254 Color: 395
Size: 964 Color: 164

Bin 80: 7 of cap free
Amount of items: 2
Items: 
Size: 6717 Color: 349
Size: 1500 Color: 206

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 6312 Color: 330
Size: 1904 Color: 224

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 7076 Color: 375
Size: 1140 Color: 181

Bin 83: 10 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 320
Size: 2106 Color: 232
Size: 112 Color: 3

Bin 84: 10 of cap free
Amount of items: 3
Items: 
Size: 6046 Color: 321
Size: 2088 Color: 230
Size: 80 Color: 2

Bin 85: 11 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 309
Size: 1927 Color: 225
Size: 596 Color: 126

Bin 86: 12 of cap free
Amount of items: 3
Items: 
Size: 5429 Color: 304
Size: 2639 Color: 250
Size: 144 Color: 10

Bin 87: 12 of cap free
Amount of items: 2
Items: 
Size: 6932 Color: 363
Size: 1280 Color: 193

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 7027 Color: 373
Size: 1185 Color: 185

Bin 89: 13 of cap free
Amount of items: 2
Items: 
Size: 5210 Color: 296
Size: 3001 Color: 261

Bin 90: 14 of cap free
Amount of items: 2
Items: 
Size: 6392 Color: 334
Size: 1818 Color: 220

Bin 91: 14 of cap free
Amount of items: 2
Items: 
Size: 6875 Color: 362
Size: 1335 Color: 197

Bin 92: 14 of cap free
Amount of items: 2
Items: 
Size: 7002 Color: 372
Size: 1208 Color: 188

Bin 93: 15 of cap free
Amount of items: 2
Items: 
Size: 6165 Color: 325
Size: 2044 Color: 227

Bin 94: 15 of cap free
Amount of items: 2
Items: 
Size: 6349 Color: 333
Size: 1860 Color: 221

Bin 95: 16 of cap free
Amount of items: 7
Items: 
Size: 4115 Color: 274
Size: 888 Color: 149
Size: 868 Color: 147
Size: 849 Color: 145
Size: 814 Color: 144
Size: 448 Color: 106
Size: 226 Color: 51

Bin 96: 18 of cap free
Amount of items: 2
Items: 
Size: 6503 Color: 339
Size: 1703 Color: 214

Bin 97: 18 of cap free
Amount of items: 2
Items: 
Size: 6636 Color: 346
Size: 1570 Color: 209

Bin 98: 22 of cap free
Amount of items: 3
Items: 
Size: 5112 Color: 294
Size: 2914 Color: 256
Size: 176 Color: 20

Bin 99: 22 of cap free
Amount of items: 3
Items: 
Size: 5274 Color: 301
Size: 2768 Color: 252
Size: 160 Color: 13

Bin 100: 22 of cap free
Amount of items: 2
Items: 
Size: 6428 Color: 335
Size: 1774 Color: 218

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 6540 Color: 341
Size: 1662 Color: 213

Bin 102: 23 of cap free
Amount of items: 3
Items: 
Size: 5736 Color: 314
Size: 2337 Color: 238
Size: 128 Color: 6

Bin 103: 27 of cap free
Amount of items: 2
Items: 
Size: 5715 Color: 312
Size: 2482 Color: 244

Bin 104: 28 of cap free
Amount of items: 3
Items: 
Size: 4572 Color: 280
Size: 3420 Color: 265
Size: 204 Color: 41

Bin 105: 28 of cap free
Amount of items: 2
Items: 
Size: 5250 Color: 297
Size: 2946 Color: 257

Bin 106: 28 of cap free
Amount of items: 3
Items: 
Size: 5421 Color: 303
Size: 2631 Color: 249
Size: 144 Color: 11

Bin 107: 42 of cap free
Amount of items: 3
Items: 
Size: 5723 Color: 313
Size: 2331 Color: 237
Size: 128 Color: 7

Bin 108: 44 of cap free
Amount of items: 3
Items: 
Size: 4760 Color: 289
Size: 3240 Color: 264
Size: 180 Color: 27

Bin 109: 44 of cap free
Amount of items: 4
Items: 
Size: 5260 Color: 299
Size: 2600 Color: 247
Size: 160 Color: 16
Size: 160 Color: 15

Bin 110: 47 of cap free
Amount of items: 6
Items: 
Size: 4116 Color: 275
Size: 1042 Color: 174
Size: 936 Color: 162
Size: 931 Color: 160
Size: 928 Color: 159
Size: 224 Color: 50

Bin 111: 47 of cap free
Amount of items: 3
Items: 
Size: 5400 Color: 302
Size: 2625 Color: 248
Size: 152 Color: 12

Bin 112: 48 of cap free
Amount of items: 2
Items: 
Size: 5770 Color: 315
Size: 2406 Color: 239

Bin 113: 50 of cap free
Amount of items: 2
Items: 
Size: 5698 Color: 310
Size: 2476 Color: 243

Bin 114: 52 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 290
Size: 3044 Color: 262
Size: 180 Color: 25

Bin 115: 52 of cap free
Amount of items: 3
Items: 
Size: 5967 Color: 319
Size: 2085 Color: 229
Size: 120 Color: 4

Bin 116: 56 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 316
Size: 2264 Color: 236
Size: 124 Color: 5

Bin 117: 66 of cap free
Amount of items: 3
Items: 
Size: 5266 Color: 300
Size: 2732 Color: 251
Size: 160 Color: 14

Bin 118: 67 of cap free
Amount of items: 3
Items: 
Size: 5075 Color: 293
Size: 2906 Color: 255
Size: 176 Color: 21

Bin 119: 69 of cap free
Amount of items: 6
Items: 
Size: 4120 Color: 276
Size: 1257 Color: 191
Size: 1202 Color: 187
Size: 1128 Color: 179
Size: 224 Color: 49
Size: 224 Color: 48

Bin 120: 76 of cap free
Amount of items: 2
Items: 
Size: 4690 Color: 285
Size: 3458 Color: 270

Bin 121: 91 of cap free
Amount of items: 4
Items: 
Size: 4568 Color: 279
Size: 1753 Color: 217
Size: 1608 Color: 211
Size: 204 Color: 42

Bin 122: 98 of cap free
Amount of items: 3
Items: 
Size: 5516 Color: 306
Size: 2474 Color: 242
Size: 136 Color: 8

Bin 123: 102 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 305
Size: 2466 Color: 241
Size: 144 Color: 9

Bin 124: 112 of cap free
Amount of items: 2
Items: 
Size: 4124 Color: 278
Size: 3988 Color: 271

Bin 125: 121 of cap free
Amount of items: 4
Items: 
Size: 4746 Color: 288
Size: 2993 Color: 260
Size: 184 Color: 30
Size: 180 Color: 28

Bin 126: 138 of cap free
Amount of items: 2
Items: 
Size: 4654 Color: 284
Size: 3432 Color: 269

Bin 127: 156 of cap free
Amount of items: 2
Items: 
Size: 4641 Color: 283
Size: 3427 Color: 268

Bin 128: 166 of cap free
Amount of items: 2
Items: 
Size: 4633 Color: 282
Size: 3425 Color: 267

Bin 129: 168 of cap free
Amount of items: 4
Items: 
Size: 5258 Color: 298
Size: 2462 Color: 240
Size: 168 Color: 18
Size: 168 Color: 17

Bin 130: 177 of cap free
Amount of items: 2
Items: 
Size: 4625 Color: 281
Size: 3422 Color: 266

Bin 131: 202 of cap free
Amount of items: 4
Items: 
Size: 4730 Color: 286
Size: 2888 Color: 253
Size: 204 Color: 39
Size: 200 Color: 38

Bin 132: 224 of cap free
Amount of items: 22
Items: 
Size: 500 Color: 114
Size: 492 Color: 112
Size: 492 Color: 111
Size: 488 Color: 110
Size: 488 Color: 109
Size: 420 Color: 103
Size: 420 Color: 102
Size: 416 Color: 100
Size: 416 Color: 99
Size: 408 Color: 98
Size: 384 Color: 95
Size: 368 Color: 92
Size: 360 Color: 91
Size: 284 Color: 73
Size: 266 Color: 70
Size: 264 Color: 69
Size: 264 Color: 68
Size: 256 Color: 67
Size: 256 Color: 66
Size: 256 Color: 64
Size: 252 Color: 63
Size: 250 Color: 62

Bin 133: 5134 of cap free
Amount of items: 10
Items: 
Size: 352 Color: 90
Size: 340 Color: 87
Size: 320 Color: 85
Size: 312 Color: 83
Size: 304 Color: 81
Size: 304 Color: 80
Size: 296 Color: 78
Size: 288 Color: 76
Size: 288 Color: 75
Size: 286 Color: 74

Total size: 1085568
Total free space: 8224

