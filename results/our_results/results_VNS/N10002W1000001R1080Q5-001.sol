Capicity Bin: 1000001
Lower Bound: 4514

Bins used: 4530
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 416665 Color: 3
Size: 315356 Color: 3
Size: 267980 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 677622 Color: 1
Size: 161386 Color: 0
Size: 160993 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 653057 Color: 2
Size: 237668 Color: 4
Size: 109276 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 786473 Color: 1
Size: 107723 Color: 3
Size: 105805 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 426322 Color: 1
Size: 312273 Color: 3
Size: 261406 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 710103 Color: 1
Size: 167066 Color: 0
Size: 122832 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 749892 Color: 1
Size: 128450 Color: 1
Size: 121659 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 640142 Color: 2
Size: 199949 Color: 1
Size: 159910 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 747397 Color: 2
Size: 132786 Color: 3
Size: 119818 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 788545 Color: 2
Size: 109550 Color: 3
Size: 101906 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 769807 Color: 1
Size: 121600 Color: 0
Size: 108594 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 787304 Color: 3
Size: 106515 Color: 2
Size: 106182 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 720366 Color: 2
Size: 178481 Color: 2
Size: 101154 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 641676 Color: 2
Size: 231574 Color: 2
Size: 126751 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 702996 Color: 0
Size: 150029 Color: 4
Size: 146976 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 758391 Color: 4
Size: 122636 Color: 4
Size: 118974 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 763725 Color: 0
Size: 119200 Color: 0
Size: 117076 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 689724 Color: 4
Size: 205266 Color: 0
Size: 105011 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 690480 Color: 4
Size: 157193 Color: 1
Size: 152328 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 755542 Color: 4
Size: 122522 Color: 3
Size: 121937 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 732947 Color: 0
Size: 155836 Color: 2
Size: 111218 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 686980 Color: 0
Size: 156918 Color: 2
Size: 156103 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 749803 Color: 4
Size: 130478 Color: 4
Size: 119720 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 749349 Color: 1
Size: 145132 Color: 1
Size: 105520 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 719797 Color: 2
Size: 177355 Color: 3
Size: 102849 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 652781 Color: 2
Size: 195378 Color: 2
Size: 151842 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 633459 Color: 2
Size: 226284 Color: 0
Size: 140258 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 717129 Color: 2
Size: 141703 Color: 1
Size: 141169 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 742552 Color: 4
Size: 145769 Color: 1
Size: 111680 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 755858 Color: 2
Size: 122498 Color: 4
Size: 121645 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 780506 Color: 1
Size: 112895 Color: 0
Size: 106600 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 786310 Color: 2
Size: 111303 Color: 1
Size: 102388 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 706692 Color: 4
Size: 167944 Color: 3
Size: 125365 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 679421 Color: 4
Size: 160396 Color: 1
Size: 160184 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 699792 Color: 1
Size: 152332 Color: 4
Size: 147877 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 745133 Color: 4
Size: 138368 Color: 0
Size: 116500 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 760529 Color: 4
Size: 119840 Color: 3
Size: 119632 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 767454 Color: 3
Size: 118722 Color: 4
Size: 113825 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 703435 Color: 4
Size: 150019 Color: 4
Size: 146547 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 712193 Color: 4
Size: 146209 Color: 3
Size: 141599 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 776858 Color: 1
Size: 120319 Color: 4
Size: 102824 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 787970 Color: 3
Size: 107061 Color: 3
Size: 104970 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 388159 Color: 1
Size: 316177 Color: 2
Size: 295665 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 740134 Color: 0
Size: 140581 Color: 1
Size: 119286 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 788667 Color: 2
Size: 110088 Color: 0
Size: 101246 Color: 3

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 642801 Color: 4
Size: 357200 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 661296 Color: 3
Size: 169729 Color: 3
Size: 168976 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 721103 Color: 3
Size: 165009 Color: 0
Size: 113889 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 782199 Color: 2
Size: 111187 Color: 1
Size: 106615 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 388278 Color: 3
Size: 339389 Color: 1
Size: 272334 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 719457 Color: 3
Size: 141526 Color: 0
Size: 139018 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 769388 Color: 0
Size: 118430 Color: 2
Size: 112183 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 663301 Color: 4
Size: 218567 Color: 1
Size: 118133 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 686765 Color: 3
Size: 165566 Color: 2
Size: 147670 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 724083 Color: 0
Size: 142055 Color: 4
Size: 133863 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 708027 Color: 0
Size: 187648 Color: 3
Size: 104326 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 624301 Color: 4
Size: 191386 Color: 2
Size: 184314 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 729768 Color: 4
Size: 135899 Color: 1
Size: 134334 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 734437 Color: 3
Size: 134824 Color: 2
Size: 130740 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 738394 Color: 1
Size: 147205 Color: 3
Size: 114402 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 778927 Color: 2
Size: 111990 Color: 0
Size: 109084 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 729577 Color: 1
Size: 144448 Color: 2
Size: 125976 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 753882 Color: 2
Size: 137265 Color: 3
Size: 108854 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 768520 Color: 0
Size: 117912 Color: 3
Size: 113569 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 597290 Color: 0
Size: 201366 Color: 4
Size: 201345 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 610126 Color: 1
Size: 195585 Color: 3
Size: 194290 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 695596 Color: 3
Size: 152394 Color: 3
Size: 152011 Color: 4

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 722176 Color: 3
Size: 139055 Color: 0
Size: 138770 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 717718 Color: 1
Size: 147979 Color: 2
Size: 134304 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 749866 Color: 2
Size: 125165 Color: 0
Size: 124970 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 761701 Color: 2
Size: 132939 Color: 1
Size: 105361 Color: 0

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 573566 Color: 0
Size: 426435 Color: 2

Bin 73: 0 of cap free
Amount of items: 4
Items: 
Size: 592332 Color: 0
Size: 203830 Color: 2
Size: 103427 Color: 3
Size: 100412 Color: 3

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 694941 Color: 3
Size: 305060 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 762895 Color: 4
Size: 135921 Color: 4
Size: 101185 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 744843 Color: 3
Size: 140083 Color: 2
Size: 115075 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 740772 Color: 3
Size: 131057 Color: 0
Size: 128172 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 782796 Color: 2
Size: 110858 Color: 1
Size: 106347 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 709018 Color: 3
Size: 154749 Color: 2
Size: 136234 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 706521 Color: 2
Size: 153514 Color: 0
Size: 139966 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 729555 Color: 3
Size: 154327 Color: 1
Size: 116119 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 761422 Color: 2
Size: 119782 Color: 0
Size: 118797 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 768139 Color: 2
Size: 117222 Color: 1
Size: 114640 Color: 4

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 754846 Color: 1
Size: 129472 Color: 2
Size: 115683 Color: 3

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 541453 Color: 0
Size: 458548 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 709585 Color: 3
Size: 148842 Color: 1
Size: 141574 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 754040 Color: 2
Size: 127432 Color: 1
Size: 118529 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 754081 Color: 2
Size: 128216 Color: 1
Size: 117704 Color: 4

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 787653 Color: 4
Size: 110597 Color: 0
Size: 101751 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 781414 Color: 4
Size: 111576 Color: 0
Size: 107011 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 710569 Color: 2
Size: 147872 Color: 0
Size: 141560 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 751449 Color: 3
Size: 130399 Color: 4
Size: 118153 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 544227 Color: 1
Size: 252012 Color: 2
Size: 203762 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 735489 Color: 2
Size: 140212 Color: 0
Size: 124300 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 756682 Color: 0
Size: 132273 Color: 2
Size: 111046 Color: 3

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 578955 Color: 2
Size: 421046 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 746176 Color: 1
Size: 152255 Color: 0
Size: 101570 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 783004 Color: 4
Size: 114473 Color: 3
Size: 102524 Color: 2

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 788869 Color: 1
Size: 110990 Color: 0
Size: 100142 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 725312 Color: 3
Size: 138419 Color: 2
Size: 136270 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 703544 Color: 3
Size: 157770 Color: 0
Size: 138687 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 662095 Color: 4
Size: 169527 Color: 1
Size: 168379 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 703850 Color: 2
Size: 150287 Color: 3
Size: 145864 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 718765 Color: 0
Size: 161445 Color: 1
Size: 119791 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 760091 Color: 1
Size: 127539 Color: 0
Size: 112371 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 751379 Color: 1
Size: 125149 Color: 1
Size: 123473 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 768391 Color: 1
Size: 123863 Color: 2
Size: 107747 Color: 4

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 762554 Color: 3
Size: 125886 Color: 1
Size: 111561 Color: 2

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 703642 Color: 3
Size: 180371 Color: 4
Size: 115988 Color: 3

Bin 110: 0 of cap free
Amount of items: 4
Items: 
Size: 384258 Color: 0
Size: 338689 Color: 0
Size: 156116 Color: 1
Size: 120938 Color: 4

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 589807 Color: 1
Size: 410194 Color: 2

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 613288 Color: 3
Size: 194830 Color: 3
Size: 191883 Color: 4

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 678347 Color: 3
Size: 171414 Color: 3
Size: 150240 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 729719 Color: 1
Size: 136307 Color: 1
Size: 133975 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 708526 Color: 2
Size: 160955 Color: 0
Size: 130520 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 738656 Color: 2
Size: 131036 Color: 2
Size: 130309 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 760685 Color: 3
Size: 136497 Color: 0
Size: 102819 Color: 0

Bin 118: 0 of cap free
Amount of items: 4
Items: 
Size: 428613 Color: 3
Size: 313424 Color: 0
Size: 151592 Color: 3
Size: 106372 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 737778 Color: 2
Size: 138100 Color: 4
Size: 124123 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 715407 Color: 0
Size: 142471 Color: 3
Size: 142123 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 753392 Color: 0
Size: 126153 Color: 1
Size: 120456 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 573787 Color: 3
Size: 245828 Color: 2
Size: 180386 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 701711 Color: 1
Size: 149554 Color: 1
Size: 148736 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 704098 Color: 0
Size: 150835 Color: 2
Size: 145068 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 724557 Color: 2
Size: 138738 Color: 0
Size: 136706 Color: 2

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 726994 Color: 3
Size: 136643 Color: 1
Size: 136364 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 744167 Color: 1
Size: 130130 Color: 2
Size: 125704 Color: 3

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 761599 Color: 4
Size: 137094 Color: 4
Size: 101308 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 774773 Color: 2
Size: 119543 Color: 2
Size: 105685 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 762499 Color: 2
Size: 135431 Color: 3
Size: 102071 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 426255 Color: 2
Size: 318913 Color: 0
Size: 254833 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 679552 Color: 2
Size: 175761 Color: 0
Size: 144688 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 573783 Color: 0
Size: 227394 Color: 0
Size: 198824 Color: 3

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 744105 Color: 0
Size: 139454 Color: 2
Size: 116442 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 772680 Color: 3
Size: 115138 Color: 1
Size: 112183 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 792189 Color: 2
Size: 105734 Color: 4
Size: 102078 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 670471 Color: 2
Size: 205542 Color: 4
Size: 123988 Color: 4

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 743861 Color: 0
Size: 151975 Color: 1
Size: 104165 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 735130 Color: 0
Size: 151736 Color: 3
Size: 113135 Color: 2

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 756143 Color: 2
Size: 124325 Color: 3
Size: 119533 Color: 3

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 766111 Color: 2
Size: 127904 Color: 1
Size: 105986 Color: 4

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 780521 Color: 4
Size: 114986 Color: 1
Size: 104494 Color: 4

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 691928 Color: 0
Size: 207448 Color: 3
Size: 100625 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 751250 Color: 4
Size: 134459 Color: 1
Size: 114292 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 654438 Color: 2
Size: 173161 Color: 1
Size: 172402 Color: 4

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 671940 Color: 4
Size: 171190 Color: 3
Size: 156871 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 536319 Color: 1
Size: 276934 Color: 2
Size: 186748 Color: 0

Bin 148: 0 of cap free
Amount of items: 4
Items: 
Size: 435718 Color: 3
Size: 294560 Color: 2
Size: 135540 Color: 1
Size: 134183 Color: 2

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 675731 Color: 4
Size: 162843 Color: 3
Size: 161427 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 632564 Color: 4
Size: 219235 Color: 0
Size: 148202 Color: 3

Bin 151: 0 of cap free
Amount of items: 4
Items: 
Size: 392709 Color: 0
Size: 344744 Color: 4
Size: 159259 Color: 3
Size: 103289 Color: 2

Bin 152: 0 of cap free
Amount of items: 2
Items: 
Size: 590422 Color: 0
Size: 409579 Color: 1

Bin 153: 0 of cap free
Amount of items: 2
Items: 
Size: 760410 Color: 2
Size: 239591 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 648237 Color: 3
Size: 175954 Color: 3
Size: 175810 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 697117 Color: 2
Size: 173160 Color: 1
Size: 129724 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 743631 Color: 1
Size: 129310 Color: 2
Size: 127060 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 752602 Color: 1
Size: 124805 Color: 1
Size: 122594 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 723514 Color: 0
Size: 173554 Color: 2
Size: 102933 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 717440 Color: 2
Size: 181278 Color: 4
Size: 101283 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 733315 Color: 2
Size: 157743 Color: 0
Size: 108943 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 749339 Color: 0
Size: 130466 Color: 1
Size: 120196 Color: 4

Bin 162: 0 of cap free
Amount of items: 2
Items: 
Size: 777278 Color: 0
Size: 222723 Color: 4

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 664974 Color: 3
Size: 189815 Color: 3
Size: 145212 Color: 2

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 693323 Color: 2
Size: 175839 Color: 1
Size: 130839 Color: 3

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 750453 Color: 2
Size: 145184 Color: 0
Size: 104364 Color: 4

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 755905 Color: 0
Size: 244096 Color: 3

Bin 167: 0 of cap free
Amount of items: 4
Items: 
Size: 370653 Color: 0
Size: 296044 Color: 3
Size: 177840 Color: 0
Size: 155464 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 678041 Color: 3
Size: 161009 Color: 3
Size: 160951 Color: 0

Bin 169: 0 of cap free
Amount of items: 4
Items: 
Size: 431084 Color: 4
Size: 319161 Color: 4
Size: 133934 Color: 0
Size: 115822 Color: 2

Bin 170: 0 of cap free
Amount of items: 4
Items: 
Size: 505102 Color: 0
Size: 245906 Color: 1
Size: 125687 Color: 1
Size: 123306 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 615445 Color: 2
Size: 193841 Color: 0
Size: 190715 Color: 4

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 670421 Color: 1
Size: 166958 Color: 4
Size: 162622 Color: 2

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 600977 Color: 4
Size: 199575 Color: 1
Size: 199449 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 680946 Color: 4
Size: 159654 Color: 2
Size: 159401 Color: 3

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 755839 Color: 0
Size: 122230 Color: 4
Size: 121932 Color: 2

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 743218 Color: 3
Size: 145536 Color: 2
Size: 111247 Color: 3

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 758594 Color: 0
Size: 132202 Color: 3
Size: 109205 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 763592 Color: 4
Size: 124372 Color: 1
Size: 112037 Color: 2

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 696826 Color: 0
Size: 151769 Color: 4
Size: 151406 Color: 4

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 737136 Color: 0
Size: 143033 Color: 2
Size: 119832 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 779194 Color: 0
Size: 120162 Color: 1
Size: 100645 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 745304 Color: 0
Size: 151068 Color: 0
Size: 103629 Color: 4

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 693105 Color: 4
Size: 205761 Color: 0
Size: 101135 Color: 3

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 756704 Color: 1
Size: 123177 Color: 3
Size: 120120 Color: 0

Bin 185: 0 of cap free
Amount of items: 4
Items: 
Size: 566481 Color: 3
Size: 220332 Color: 0
Size: 110725 Color: 4
Size: 102463 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 781607 Color: 2
Size: 109312 Color: 4
Size: 109082 Color: 3

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 775601 Color: 2
Size: 119685 Color: 1
Size: 104715 Color: 2

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 770948 Color: 3
Size: 127990 Color: 3
Size: 101063 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 670364 Color: 2
Size: 201882 Color: 2
Size: 127755 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 741491 Color: 2
Size: 129982 Color: 2
Size: 128528 Color: 4

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 752423 Color: 2
Size: 127593 Color: 0
Size: 119985 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 555177 Color: 3
Size: 224950 Color: 1
Size: 219874 Color: 3

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 787857 Color: 2
Size: 110901 Color: 1
Size: 101243 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 785188 Color: 0
Size: 111419 Color: 3
Size: 103394 Color: 2

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 767877 Color: 1
Size: 121251 Color: 0
Size: 110873 Color: 4

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 761193 Color: 4
Size: 124218 Color: 0
Size: 114590 Color: 4

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 632459 Color: 0
Size: 200338 Color: 1
Size: 167204 Color: 2

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 689214 Color: 1
Size: 171007 Color: 1
Size: 139780 Color: 3

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 711494 Color: 0
Size: 166443 Color: 0
Size: 122064 Color: 4

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 670379 Color: 4
Size: 228675 Color: 1
Size: 100947 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 767184 Color: 2
Size: 118923 Color: 1
Size: 113894 Color: 4

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 686177 Color: 2
Size: 161145 Color: 0
Size: 152679 Color: 4

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 696123 Color: 4
Size: 184656 Color: 4
Size: 119222 Color: 0

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 723293 Color: 0
Size: 276708 Color: 3

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 714289 Color: 4
Size: 159492 Color: 2
Size: 126220 Color: 4

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 738048 Color: 3
Size: 142574 Color: 4
Size: 119379 Color: 2

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 717719 Color: 1
Size: 166369 Color: 4
Size: 115913 Color: 2

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 700769 Color: 0
Size: 181659 Color: 4
Size: 117573 Color: 2

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 788622 Color: 2
Size: 107735 Color: 0
Size: 103644 Color: 4

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 789566 Color: 2
Size: 105879 Color: 0
Size: 104556 Color: 2

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 794351 Color: 1
Size: 103992 Color: 0
Size: 101658 Color: 1

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 579961 Color: 0
Size: 420040 Color: 1

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 617229 Color: 1
Size: 201350 Color: 3
Size: 181422 Color: 2

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 719102 Color: 3
Size: 280899 Color: 4

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 640038 Color: 0
Size: 206772 Color: 1
Size: 153191 Color: 2

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 646088 Color: 0
Size: 242802 Color: 4
Size: 111111 Color: 2

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 776685 Color: 4
Size: 114370 Color: 4
Size: 108946 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 763401 Color: 4
Size: 125021 Color: 2
Size: 111579 Color: 3

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 645777 Color: 2
Size: 180584 Color: 3
Size: 173640 Color: 2

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 385884 Color: 2
Size: 351688 Color: 0
Size: 262429 Color: 3

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 688909 Color: 3
Size: 192753 Color: 1
Size: 118339 Color: 2

Bin 222: 0 of cap free
Amount of items: 4
Items: 
Size: 376206 Color: 3
Size: 313481 Color: 4
Size: 186178 Color: 1
Size: 124136 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 752862 Color: 2
Size: 123877 Color: 0
Size: 123262 Color: 4

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 698238 Color: 3
Size: 150925 Color: 3
Size: 150838 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 687951 Color: 2
Size: 158222 Color: 4
Size: 153828 Color: 3

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 523044 Color: 4
Size: 245329 Color: 3
Size: 231628 Color: 0

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 602620 Color: 0
Size: 397381 Color: 2

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 729524 Color: 1
Size: 142862 Color: 2
Size: 127615 Color: 2

Bin 229: 0 of cap free
Amount of items: 4
Items: 
Size: 577843 Color: 4
Size: 207117 Color: 0
Size: 113429 Color: 1
Size: 101612 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 639257 Color: 0
Size: 204388 Color: 1
Size: 156356 Color: 1

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 766596 Color: 3
Size: 127318 Color: 0
Size: 106087 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 667605 Color: 1
Size: 192722 Color: 1
Size: 139674 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 763951 Color: 4
Size: 122436 Color: 0
Size: 113614 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 631045 Color: 3
Size: 254912 Color: 3
Size: 114044 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 737762 Color: 0
Size: 131298 Color: 4
Size: 130941 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 670357 Color: 0
Size: 213584 Color: 3
Size: 116060 Color: 3

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 791214 Color: 3
Size: 107571 Color: 2
Size: 101216 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 522042 Color: 0
Size: 374703 Color: 0
Size: 103256 Color: 2

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 732220 Color: 1
Size: 152184 Color: 3
Size: 115597 Color: 2

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 719020 Color: 1
Size: 159392 Color: 4
Size: 121589 Color: 1

Bin 241: 0 of cap free
Amount of items: 4
Items: 
Size: 557551 Color: 4
Size: 227479 Color: 3
Size: 112161 Color: 1
Size: 102810 Color: 4

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 782890 Color: 1
Size: 108786 Color: 2
Size: 108325 Color: 4

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 691309 Color: 1
Size: 201277 Color: 2
Size: 107415 Color: 4

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 636984 Color: 4
Size: 363017 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 705981 Color: 2
Size: 160957 Color: 1
Size: 133063 Color: 3

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 702201 Color: 0
Size: 173739 Color: 3
Size: 124061 Color: 2

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 683382 Color: 0
Size: 200786 Color: 2
Size: 115833 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 748897 Color: 1
Size: 136352 Color: 3
Size: 114752 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 742786 Color: 3
Size: 133047 Color: 2
Size: 124168 Color: 4

Bin 250: 0 of cap free
Amount of items: 4
Items: 
Size: 571519 Color: 3
Size: 214286 Color: 2
Size: 108801 Color: 4
Size: 105395 Color: 2

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 502620 Color: 0
Size: 497381 Color: 2

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 601979 Color: 2
Size: 398022 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 760903 Color: 4
Size: 121248 Color: 2
Size: 117850 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 771112 Color: 1
Size: 125988 Color: 1
Size: 102901 Color: 4

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 760655 Color: 1
Size: 137316 Color: 3
Size: 102030 Color: 4

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 718140 Color: 3
Size: 167731 Color: 2
Size: 114130 Color: 2

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 724158 Color: 1
Size: 165163 Color: 2
Size: 110680 Color: 4

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 665349 Color: 0
Size: 208023 Color: 2
Size: 126629 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 711418 Color: 3
Size: 156757 Color: 4
Size: 131826 Color: 0

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 564907 Color: 4
Size: 435094 Color: 2

Bin 261: 0 of cap free
Amount of items: 2
Items: 
Size: 573156 Color: 0
Size: 426845 Color: 3

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 686816 Color: 1
Size: 168436 Color: 3
Size: 144749 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 589437 Color: 3
Size: 208412 Color: 4
Size: 202152 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 726318 Color: 4
Size: 152480 Color: 2
Size: 121203 Color: 3

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 768803 Color: 2
Size: 126022 Color: 1
Size: 105176 Color: 1

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 793984 Color: 2
Size: 103858 Color: 0
Size: 102159 Color: 2

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 718159 Color: 0
Size: 179325 Color: 1
Size: 102517 Color: 3

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 747326 Color: 4
Size: 150063 Color: 0
Size: 102612 Color: 4

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 521985 Color: 2
Size: 278319 Color: 2
Size: 199697 Color: 3

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 695421 Color: 4
Size: 152547 Color: 1
Size: 152033 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 761106 Color: 0
Size: 121828 Color: 2
Size: 117067 Color: 2

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 376156 Color: 1
Size: 346777 Color: 3
Size: 277068 Color: 4

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 714047 Color: 0
Size: 144401 Color: 3
Size: 141553 Color: 2

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 780992 Color: 4
Size: 118692 Color: 2
Size: 100317 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 722083 Color: 1
Size: 142135 Color: 0
Size: 135783 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 726812 Color: 1
Size: 167768 Color: 2
Size: 105421 Color: 3

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 746139 Color: 1
Size: 127878 Color: 4
Size: 125984 Color: 3

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 716037 Color: 4
Size: 163744 Color: 0
Size: 120220 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 767376 Color: 1
Size: 121156 Color: 2
Size: 111469 Color: 4

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 720252 Color: 1
Size: 163050 Color: 2
Size: 116699 Color: 4

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 727738 Color: 2
Size: 137445 Color: 3
Size: 134818 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 725371 Color: 2
Size: 155721 Color: 2
Size: 118909 Color: 4

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 597439 Color: 1
Size: 201531 Color: 0
Size: 201031 Color: 4

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 685210 Color: 0
Size: 201326 Color: 0
Size: 113465 Color: 4

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 718323 Color: 4
Size: 150609 Color: 1
Size: 131069 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 486634 Color: 1
Size: 274311 Color: 3
Size: 239056 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 659274 Color: 4
Size: 171009 Color: 0
Size: 169718 Color: 2

Bin 288: 0 of cap free
Amount of items: 2
Items: 
Size: 746263 Color: 4
Size: 253738 Color: 1

Bin 289: 0 of cap free
Amount of items: 4
Items: 
Size: 492562 Color: 1
Size: 253044 Color: 1
Size: 148939 Color: 2
Size: 105456 Color: 2

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 638940 Color: 0
Size: 239125 Color: 3
Size: 121936 Color: 4

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 695120 Color: 1
Size: 155171 Color: 1
Size: 149710 Color: 3

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 731332 Color: 1
Size: 136581 Color: 4
Size: 132088 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 749556 Color: 2
Size: 131919 Color: 0
Size: 118526 Color: 3

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 729165 Color: 2
Size: 140290 Color: 3
Size: 130546 Color: 3

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 781668 Color: 3
Size: 109647 Color: 4
Size: 108686 Color: 2

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 736142 Color: 4
Size: 156148 Color: 1
Size: 107711 Color: 2

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 499715 Color: 0
Size: 254424 Color: 0
Size: 245862 Color: 2

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 706590 Color: 1
Size: 175799 Color: 0
Size: 117612 Color: 2

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 785515 Color: 4
Size: 107684 Color: 2
Size: 106802 Color: 2

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 651122 Color: 1
Size: 228977 Color: 3
Size: 119902 Color: 2

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 713757 Color: 1
Size: 160388 Color: 0
Size: 125856 Color: 4

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 733919 Color: 4
Size: 133578 Color: 0
Size: 132504 Color: 3

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 786755 Color: 1
Size: 111877 Color: 1
Size: 101369 Color: 4

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 426037 Color: 4
Size: 321697 Color: 0
Size: 252267 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 677750 Color: 0
Size: 219487 Color: 2
Size: 102764 Color: 3

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 752550 Color: 0
Size: 124686 Color: 2
Size: 122765 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 759107 Color: 4
Size: 133607 Color: 4
Size: 107287 Color: 3

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 635192 Color: 2
Size: 240722 Color: 1
Size: 124087 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 667174 Color: 0
Size: 174477 Color: 1
Size: 158350 Color: 2

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 721797 Color: 1
Size: 144072 Color: 0
Size: 134132 Color: 2

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 715217 Color: 3
Size: 142942 Color: 0
Size: 141842 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 790101 Color: 0
Size: 109352 Color: 1
Size: 100548 Color: 0

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 654806 Color: 3
Size: 345195 Color: 1

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 371285 Color: 4
Size: 368831 Color: 4
Size: 259885 Color: 2

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 744545 Color: 4
Size: 255456 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 773808 Color: 3
Size: 114476 Color: 0
Size: 111717 Color: 3

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 357245 Color: 0
Size: 322086 Color: 1
Size: 320670 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 779313 Color: 4
Size: 110538 Color: 3
Size: 110150 Color: 2

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 736116 Color: 0
Size: 151583 Color: 4
Size: 112302 Color: 3

Bin 320: 0 of cap free
Amount of items: 2
Items: 
Size: 643821 Color: 3
Size: 356180 Color: 4

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 780191 Color: 1
Size: 109940 Color: 0
Size: 109870 Color: 3

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 601401 Color: 4
Size: 199360 Color: 4
Size: 199240 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 521968 Color: 1
Size: 369963 Color: 0
Size: 108070 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 691345 Color: 3
Size: 203285 Color: 1
Size: 105371 Color: 3

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 778740 Color: 4
Size: 115965 Color: 3
Size: 105296 Color: 4

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 369255 Color: 3
Size: 335493 Color: 0
Size: 295253 Color: 3

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 735273 Color: 2
Size: 143132 Color: 0
Size: 121596 Color: 4

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 794951 Color: 0
Size: 103772 Color: 4
Size: 101278 Color: 4

Bin 329: 0 of cap free
Amount of items: 2
Items: 
Size: 661957 Color: 3
Size: 338044 Color: 2

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 749235 Color: 0
Size: 134605 Color: 3
Size: 116161 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 676311 Color: 1
Size: 213040 Color: 2
Size: 110650 Color: 1

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 625955 Color: 3
Size: 239652 Color: 4
Size: 134394 Color: 2

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 686648 Color: 4
Size: 202164 Color: 1
Size: 111189 Color: 2

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 734145 Color: 0
Size: 133641 Color: 1
Size: 132215 Color: 4

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 758853 Color: 4
Size: 121182 Color: 3
Size: 119966 Color: 2

Bin 336: 0 of cap free
Amount of items: 2
Items: 
Size: 504782 Color: 3
Size: 495219 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 754877 Color: 1
Size: 132221 Color: 0
Size: 112903 Color: 2

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 724184 Color: 4
Size: 161040 Color: 3
Size: 114777 Color: 3

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 790879 Color: 2
Size: 107729 Color: 0
Size: 101393 Color: 2

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 733126 Color: 1
Size: 153951 Color: 3
Size: 112924 Color: 4

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 368632 Color: 3
Size: 334549 Color: 1
Size: 296820 Color: 4

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 746670 Color: 1
Size: 129281 Color: 0
Size: 124050 Color: 3

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 503163 Color: 2
Size: 368444 Color: 0
Size: 128394 Color: 2

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 469883 Color: 3
Size: 428852 Color: 1
Size: 101266 Color: 3

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 791627 Color: 0
Size: 106805 Color: 2
Size: 101569 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 575498 Color: 4
Size: 215371 Color: 0
Size: 209132 Color: 2

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 713969 Color: 0
Size: 286032 Color: 3

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 670472 Color: 0
Size: 212633 Color: 4
Size: 116896 Color: 3

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 414030 Color: 3
Size: 300918 Color: 1
Size: 285053 Color: 2

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 493104 Color: 3
Size: 300190 Color: 1
Size: 206707 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 762048 Color: 4
Size: 120600 Color: 4
Size: 117353 Color: 3

Bin 352: 0 of cap free
Amount of items: 2
Items: 
Size: 574667 Color: 3
Size: 425334 Color: 2

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 737125 Color: 1
Size: 137600 Color: 2
Size: 125276 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 763545 Color: 4
Size: 122460 Color: 3
Size: 113996 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 650974 Color: 4
Size: 177060 Color: 1
Size: 171967 Color: 3

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 696433 Color: 3
Size: 152891 Color: 3
Size: 150677 Color: 2

Bin 357: 0 of cap free
Amount of items: 4
Items: 
Size: 559869 Color: 0
Size: 220265 Color: 1
Size: 113650 Color: 2
Size: 106217 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 684378 Color: 3
Size: 203038 Color: 4
Size: 112585 Color: 4

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 595919 Color: 1
Size: 404082 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 755338 Color: 1
Size: 128426 Color: 0
Size: 116237 Color: 4

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 665374 Color: 3
Size: 216102 Color: 2
Size: 118525 Color: 3

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 486950 Color: 3
Size: 397530 Color: 2
Size: 115521 Color: 3

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 507851 Color: 2
Size: 370548 Color: 1
Size: 121602 Color: 4

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 368806 Color: 4
Size: 364673 Color: 4
Size: 266522 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 354097 Color: 4
Size: 337822 Color: 2
Size: 308082 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 351059 Color: 4
Size: 339453 Color: 1
Size: 309489 Color: 2

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 367267 Color: 2
Size: 336161 Color: 4
Size: 296573 Color: 2

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 375297 Color: 1
Size: 315708 Color: 4
Size: 308996 Color: 3

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 377499 Color: 2
Size: 352806 Color: 2
Size: 269696 Color: 4

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 394458 Color: 2
Size: 321276 Color: 3
Size: 284267 Color: 4

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 394970 Color: 3
Size: 311137 Color: 1
Size: 293894 Color: 3

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 430013 Color: 3
Size: 293647 Color: 3
Size: 276341 Color: 0

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 502591 Color: 4
Size: 497410 Color: 3

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 512718 Color: 3
Size: 487283 Color: 2

Bin 375: 0 of cap free
Amount of items: 2
Items: 
Size: 537444 Color: 3
Size: 462557 Color: 4

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 540549 Color: 0
Size: 459452 Color: 1

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 558686 Color: 1
Size: 441315 Color: 3

Bin 378: 0 of cap free
Amount of items: 2
Items: 
Size: 559967 Color: 0
Size: 440034 Color: 2

Bin 379: 0 of cap free
Amount of items: 2
Items: 
Size: 568134 Color: 3
Size: 431867 Color: 4

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 572802 Color: 0
Size: 215648 Color: 1
Size: 211551 Color: 2

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 574320 Color: 2
Size: 425681 Color: 1

Bin 382: 0 of cap free
Amount of items: 2
Items: 
Size: 575012 Color: 0
Size: 424989 Color: 3

Bin 383: 0 of cap free
Amount of items: 2
Items: 
Size: 576566 Color: 0
Size: 423435 Color: 2

Bin 384: 0 of cap free
Amount of items: 2
Items: 
Size: 577833 Color: 0
Size: 422168 Color: 1

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 592404 Color: 4
Size: 407597 Color: 3

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 601225 Color: 3
Size: 398776 Color: 4

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 605085 Color: 0
Size: 198003 Color: 3
Size: 196913 Color: 2

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 617335 Color: 3
Size: 193588 Color: 0
Size: 189078 Color: 2

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 617459 Color: 3
Size: 193309 Color: 1
Size: 189233 Color: 1

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 621189 Color: 3
Size: 192839 Color: 4
Size: 185973 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 621973 Color: 3
Size: 190777 Color: 4
Size: 187251 Color: 0

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 623607 Color: 2
Size: 376394 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 626142 Color: 3
Size: 188952 Color: 0
Size: 184907 Color: 4

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 627516 Color: 3
Size: 186571 Color: 1
Size: 185914 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 627562 Color: 3
Size: 188856 Color: 2
Size: 183583 Color: 1

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 627909 Color: 3
Size: 188760 Color: 1
Size: 183332 Color: 2

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 628920 Color: 3
Size: 186981 Color: 0
Size: 184100 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 629885 Color: 1
Size: 188738 Color: 3
Size: 181378 Color: 0

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 630928 Color: 3
Size: 186856 Color: 4
Size: 182217 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 631695 Color: 4
Size: 186569 Color: 0
Size: 181737 Color: 1

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 635451 Color: 4
Size: 364550 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 637240 Color: 3
Size: 183871 Color: 1
Size: 178890 Color: 2

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 640589 Color: 3
Size: 179935 Color: 4
Size: 179477 Color: 4

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 640819 Color: 3
Size: 180195 Color: 1
Size: 178987 Color: 4

Bin 405: 0 of cap free
Amount of items: 2
Items: 
Size: 641031 Color: 1
Size: 358970 Color: 4

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 641645 Color: 3
Size: 181477 Color: 2
Size: 176879 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 644019 Color: 3
Size: 179608 Color: 0
Size: 176374 Color: 4

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 644465 Color: 3
Size: 178285 Color: 2
Size: 177251 Color: 1

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 644765 Color: 3
Size: 177636 Color: 1
Size: 177600 Color: 1

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 650199 Color: 0
Size: 349802 Color: 1

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 659311 Color: 1
Size: 340690 Color: 4

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 665512 Color: 2
Size: 334489 Color: 4

Bin 413: 0 of cap free
Amount of items: 2
Items: 
Size: 666483 Color: 4
Size: 333518 Color: 2

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 668608 Color: 2
Size: 166455 Color: 3
Size: 164938 Color: 1

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 670578 Color: 3
Size: 164885 Color: 1
Size: 164538 Color: 0

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 679011 Color: 1
Size: 320990 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 681299 Color: 4
Size: 159352 Color: 3
Size: 159350 Color: 1

Bin 418: 0 of cap free
Amount of items: 2
Items: 
Size: 682204 Color: 2
Size: 317797 Color: 1

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 717719 Color: 1
Size: 282282 Color: 3

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 743366 Color: 2
Size: 128374 Color: 1
Size: 128261 Color: 1

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 745733 Color: 1
Size: 254268 Color: 4

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 750549 Color: 2
Size: 125479 Color: 1
Size: 123973 Color: 1

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 754291 Color: 1
Size: 245710 Color: 0

Bin 424: 0 of cap free
Amount of items: 2
Items: 
Size: 755674 Color: 0
Size: 244327 Color: 2

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 758041 Color: 3
Size: 121194 Color: 1
Size: 120766 Color: 3

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 760254 Color: 0
Size: 239747 Color: 4

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 764342 Color: 1
Size: 118586 Color: 2
Size: 117073 Color: 4

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 767491 Color: 2
Size: 116353 Color: 0
Size: 116157 Color: 1

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 768879 Color: 4
Size: 231122 Color: 1

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 774097 Color: 2
Size: 114036 Color: 4
Size: 111868 Color: 4

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 774358 Color: 2
Size: 113776 Color: 0
Size: 111867 Color: 3

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 784309 Color: 3
Size: 108430 Color: 4
Size: 107262 Color: 2

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 791382 Color: 4
Size: 208619 Color: 1

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 793972 Color: 2
Size: 104147 Color: 0
Size: 101882 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 794841 Color: 2
Size: 103219 Color: 4
Size: 101941 Color: 0

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 798388 Color: 2
Size: 201613 Color: 0

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 799816 Color: 2
Size: 200185 Color: 1

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 392926 Color: 0
Size: 343760 Color: 1
Size: 263314 Color: 4

Bin 439: 1 of cap free
Amount of items: 2
Items: 
Size: 501555 Color: 2
Size: 498445 Color: 3

Bin 440: 1 of cap free
Amount of items: 2
Items: 
Size: 507231 Color: 1
Size: 492769 Color: 0

Bin 441: 1 of cap free
Amount of items: 2
Items: 
Size: 510728 Color: 4
Size: 489272 Color: 2

Bin 442: 1 of cap free
Amount of items: 2
Items: 
Size: 522003 Color: 4
Size: 477997 Color: 3

Bin 443: 1 of cap free
Amount of items: 2
Items: 
Size: 530484 Color: 1
Size: 469516 Color: 2

Bin 444: 1 of cap free
Amount of items: 2
Items: 
Size: 536056 Color: 1
Size: 463944 Color: 4

Bin 445: 1 of cap free
Amount of items: 2
Items: 
Size: 540790 Color: 0
Size: 459210 Color: 4

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 541799 Color: 0
Size: 230447 Color: 2
Size: 227754 Color: 4

Bin 447: 1 of cap free
Amount of items: 2
Items: 
Size: 567623 Color: 4
Size: 432377 Color: 1

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 569262 Color: 0
Size: 215763 Color: 2
Size: 214975 Color: 1

Bin 449: 1 of cap free
Amount of items: 2
Items: 
Size: 571813 Color: 3
Size: 428187 Color: 2

Bin 450: 1 of cap free
Amount of items: 2
Items: 
Size: 575763 Color: 1
Size: 424237 Color: 4

Bin 451: 1 of cap free
Amount of items: 2
Items: 
Size: 588344 Color: 1
Size: 411656 Color: 3

Bin 452: 1 of cap free
Amount of items: 2
Items: 
Size: 588802 Color: 4
Size: 411198 Color: 1

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 592072 Color: 0
Size: 204138 Color: 1
Size: 203790 Color: 3

Bin 454: 1 of cap free
Amount of items: 2
Items: 
Size: 595683 Color: 4
Size: 404317 Color: 3

Bin 455: 1 of cap free
Amount of items: 2
Items: 
Size: 600182 Color: 3
Size: 399818 Color: 0

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 614721 Color: 0
Size: 193800 Color: 4
Size: 191479 Color: 2

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 621433 Color: 3
Size: 189372 Color: 2
Size: 189195 Color: 1

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 623450 Color: 3
Size: 189927 Color: 2
Size: 186623 Color: 4

Bin 459: 1 of cap free
Amount of items: 2
Items: 
Size: 629041 Color: 4
Size: 370959 Color: 3

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 630200 Color: 3
Size: 188202 Color: 2
Size: 181598 Color: 1

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 631416 Color: 1
Size: 187569 Color: 0
Size: 181015 Color: 4

Bin 462: 1 of cap free
Amount of items: 2
Items: 
Size: 636552 Color: 3
Size: 363448 Color: 1

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 641463 Color: 3
Size: 179796 Color: 4
Size: 178741 Color: 1

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 643475 Color: 3
Size: 178406 Color: 1
Size: 178119 Color: 2

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 646098 Color: 0
Size: 178068 Color: 3
Size: 175834 Color: 0

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 653109 Color: 2
Size: 173718 Color: 3
Size: 173173 Color: 4

Bin 467: 1 of cap free
Amount of items: 2
Items: 
Size: 654359 Color: 2
Size: 345641 Color: 4

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 662416 Color: 3
Size: 169265 Color: 1
Size: 168319 Color: 1

Bin 469: 1 of cap free
Amount of items: 2
Items: 
Size: 666150 Color: 2
Size: 333850 Color: 1

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 669276 Color: 3
Size: 165760 Color: 1
Size: 164964 Color: 1

Bin 471: 1 of cap free
Amount of items: 2
Items: 
Size: 679399 Color: 3
Size: 320601 Color: 1

Bin 472: 1 of cap free
Amount of items: 2
Items: 
Size: 685953 Color: 1
Size: 314047 Color: 0

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 691996 Color: 0
Size: 154090 Color: 4
Size: 153914 Color: 3

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 701872 Color: 3
Size: 149110 Color: 4
Size: 149018 Color: 4

Bin 475: 1 of cap free
Amount of items: 2
Items: 
Size: 713701 Color: 4
Size: 286299 Color: 3

Bin 476: 1 of cap free
Amount of items: 2
Items: 
Size: 719266 Color: 4
Size: 280734 Color: 0

Bin 477: 1 of cap free
Amount of items: 2
Items: 
Size: 719572 Color: 0
Size: 280428 Color: 4

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 744350 Color: 2
Size: 128732 Color: 4
Size: 126918 Color: 0

Bin 479: 1 of cap free
Amount of items: 2
Items: 
Size: 745294 Color: 0
Size: 254706 Color: 4

Bin 480: 1 of cap free
Amount of items: 2
Items: 
Size: 757899 Color: 0
Size: 242101 Color: 1

Bin 481: 1 of cap free
Amount of items: 2
Items: 
Size: 778393 Color: 4
Size: 221607 Color: 0

Bin 482: 1 of cap free
Amount of items: 3
Items: 
Size: 783306 Color: 4
Size: 108690 Color: 3
Size: 108004 Color: 2

Bin 483: 1 of cap free
Amount of items: 3
Items: 
Size: 628790 Color: 4
Size: 189544 Color: 3
Size: 181666 Color: 2

Bin 484: 1 of cap free
Amount of items: 2
Items: 
Size: 556204 Color: 2
Size: 443796 Color: 0

Bin 485: 1 of cap free
Amount of items: 3
Items: 
Size: 753439 Color: 4
Size: 145672 Color: 4
Size: 100889 Color: 1

Bin 486: 1 of cap free
Amount of items: 3
Items: 
Size: 769666 Color: 1
Size: 116627 Color: 1
Size: 113707 Color: 3

Bin 487: 1 of cap free
Amount of items: 3
Items: 
Size: 742677 Color: 1
Size: 129330 Color: 2
Size: 127993 Color: 1

Bin 488: 1 of cap free
Amount of items: 3
Items: 
Size: 623724 Color: 3
Size: 189341 Color: 2
Size: 186935 Color: 0

Bin 489: 1 of cap free
Amount of items: 3
Items: 
Size: 641284 Color: 3
Size: 181179 Color: 4
Size: 177537 Color: 1

Bin 490: 1 of cap free
Amount of items: 3
Items: 
Size: 650588 Color: 4
Size: 176301 Color: 2
Size: 173111 Color: 1

Bin 491: 1 of cap free
Amount of items: 3
Items: 
Size: 754895 Color: 4
Size: 122770 Color: 2
Size: 122335 Color: 3

Bin 492: 1 of cap free
Amount of items: 3
Items: 
Size: 759567 Color: 0
Size: 126579 Color: 3
Size: 113854 Color: 1

Bin 493: 1 of cap free
Amount of items: 3
Items: 
Size: 648377 Color: 1
Size: 176291 Color: 2
Size: 175332 Color: 0

Bin 494: 1 of cap free
Amount of items: 3
Items: 
Size: 611743 Color: 1
Size: 195525 Color: 0
Size: 192732 Color: 4

Bin 495: 1 of cap free
Amount of items: 3
Items: 
Size: 685636 Color: 3
Size: 157242 Color: 3
Size: 157122 Color: 0

Bin 496: 1 of cap free
Amount of items: 3
Items: 
Size: 690491 Color: 3
Size: 154891 Color: 3
Size: 154618 Color: 2

Bin 497: 1 of cap free
Amount of items: 3
Items: 
Size: 426381 Color: 4
Size: 314148 Color: 0
Size: 259471 Color: 4

Bin 498: 1 of cap free
Amount of items: 3
Items: 
Size: 605771 Color: 0
Size: 197453 Color: 2
Size: 196776 Color: 0

Bin 499: 1 of cap free
Amount of items: 3
Items: 
Size: 426328 Color: 2
Size: 297707 Color: 1
Size: 275965 Color: 1

Bin 500: 1 of cap free
Amount of items: 3
Items: 
Size: 615486 Color: 4
Size: 193175 Color: 1
Size: 191339 Color: 4

Bin 501: 1 of cap free
Amount of items: 3
Items: 
Size: 658705 Color: 2
Size: 171122 Color: 0
Size: 170173 Color: 0

Bin 502: 1 of cap free
Amount of items: 3
Items: 
Size: 697901 Color: 1
Size: 151244 Color: 4
Size: 150855 Color: 3

Bin 503: 1 of cap free
Amount of items: 3
Items: 
Size: 426073 Color: 2
Size: 321293 Color: 4
Size: 252634 Color: 4

Bin 504: 1 of cap free
Amount of items: 3
Items: 
Size: 566528 Color: 4
Size: 221793 Color: 0
Size: 211679 Color: 0

Bin 505: 1 of cap free
Amount of items: 3
Items: 
Size: 651670 Color: 4
Size: 174541 Color: 0
Size: 173789 Color: 3

Bin 506: 1 of cap free
Amount of items: 3
Items: 
Size: 729990 Color: 2
Size: 135202 Color: 3
Size: 134808 Color: 3

Bin 507: 1 of cap free
Amount of items: 3
Items: 
Size: 652903 Color: 3
Size: 174671 Color: 2
Size: 172426 Color: 1

Bin 508: 1 of cap free
Amount of items: 3
Items: 
Size: 672421 Color: 4
Size: 163891 Color: 4
Size: 163688 Color: 2

Bin 509: 1 of cap free
Amount of items: 3
Items: 
Size: 720893 Color: 3
Size: 139586 Color: 2
Size: 139521 Color: 4

Bin 510: 1 of cap free
Amount of items: 2
Items: 
Size: 770630 Color: 3
Size: 229370 Color: 4

Bin 511: 1 of cap free
Amount of items: 3
Items: 
Size: 676556 Color: 0
Size: 162247 Color: 2
Size: 161197 Color: 1

Bin 512: 1 of cap free
Amount of items: 3
Items: 
Size: 775122 Color: 0
Size: 115901 Color: 2
Size: 108977 Color: 2

Bin 513: 1 of cap free
Amount of items: 3
Items: 
Size: 735180 Color: 1
Size: 132628 Color: 0
Size: 132192 Color: 0

Bin 514: 1 of cap free
Amount of items: 3
Items: 
Size: 558220 Color: 3
Size: 226736 Color: 1
Size: 215044 Color: 2

Bin 515: 1 of cap free
Amount of items: 3
Items: 
Size: 426320 Color: 2
Size: 304604 Color: 3
Size: 269076 Color: 2

Bin 516: 1 of cap free
Amount of items: 2
Items: 
Size: 741490 Color: 2
Size: 258510 Color: 3

Bin 517: 1 of cap free
Amount of items: 3
Items: 
Size: 429594 Color: 2
Size: 315643 Color: 0
Size: 254763 Color: 3

Bin 518: 1 of cap free
Amount of items: 3
Items: 
Size: 567800 Color: 3
Size: 220303 Color: 1
Size: 211897 Color: 2

Bin 519: 1 of cap free
Amount of items: 3
Items: 
Size: 649617 Color: 1
Size: 176008 Color: 1
Size: 174375 Color: 3

Bin 520: 1 of cap free
Amount of items: 3
Items: 
Size: 593949 Color: 1
Size: 203043 Color: 1
Size: 203008 Color: 3

Bin 521: 1 of cap free
Amount of items: 3
Items: 
Size: 723858 Color: 0
Size: 138536 Color: 2
Size: 137606 Color: 1

Bin 522: 1 of cap free
Amount of items: 2
Items: 
Size: 794484 Color: 2
Size: 205516 Color: 0

Bin 523: 1 of cap free
Amount of items: 3
Items: 
Size: 376336 Color: 2
Size: 354664 Color: 0
Size: 269000 Color: 3

Bin 524: 1 of cap free
Amount of items: 3
Items: 
Size: 626515 Color: 2
Size: 190802 Color: 2
Size: 182683 Color: 0

Bin 525: 1 of cap free
Amount of items: 4
Items: 
Size: 526214 Color: 1
Size: 247637 Color: 3
Size: 118979 Color: 3
Size: 107170 Color: 4

Bin 526: 1 of cap free
Amount of items: 3
Items: 
Size: 573972 Color: 0
Size: 216459 Color: 4
Size: 209569 Color: 2

Bin 527: 1 of cap free
Amount of items: 3
Items: 
Size: 769618 Color: 4
Size: 119171 Color: 4
Size: 111211 Color: 1

Bin 528: 1 of cap free
Amount of items: 2
Items: 
Size: 697167 Color: 3
Size: 302833 Color: 4

Bin 529: 1 of cap free
Amount of items: 2
Items: 
Size: 660595 Color: 1
Size: 339405 Color: 0

Bin 530: 1 of cap free
Amount of items: 3
Items: 
Size: 377378 Color: 2
Size: 367634 Color: 0
Size: 254988 Color: 0

Bin 531: 1 of cap free
Amount of items: 3
Items: 
Size: 374442 Color: 4
Size: 329850 Color: 3
Size: 295708 Color: 1

Bin 532: 1 of cap free
Amount of items: 3
Items: 
Size: 721615 Color: 4
Size: 148966 Color: 1
Size: 129419 Color: 3

Bin 533: 1 of cap free
Amount of items: 2
Items: 
Size: 763064 Color: 1
Size: 236936 Color: 2

Bin 534: 1 of cap free
Amount of items: 3
Items: 
Size: 675894 Color: 4
Size: 162531 Color: 3
Size: 161575 Color: 1

Bin 535: 1 of cap free
Amount of items: 3
Items: 
Size: 708849 Color: 3
Size: 145656 Color: 3
Size: 145495 Color: 4

Bin 536: 1 of cap free
Amount of items: 3
Items: 
Size: 772632 Color: 4
Size: 115791 Color: 2
Size: 111577 Color: 2

Bin 537: 1 of cap free
Amount of items: 2
Items: 
Size: 618673 Color: 4
Size: 381327 Color: 2

Bin 538: 1 of cap free
Amount of items: 3
Items: 
Size: 375395 Color: 0
Size: 334968 Color: 0
Size: 289637 Color: 3

Bin 539: 1 of cap free
Amount of items: 3
Items: 
Size: 372131 Color: 1
Size: 316853 Color: 2
Size: 311016 Color: 1

Bin 540: 1 of cap free
Amount of items: 3
Items: 
Size: 378586 Color: 2
Size: 352858 Color: 0
Size: 268556 Color: 2

Bin 541: 1 of cap free
Amount of items: 3
Items: 
Size: 631169 Color: 0
Size: 188125 Color: 3
Size: 180706 Color: 4

Bin 542: 1 of cap free
Amount of items: 2
Items: 
Size: 614365 Color: 2
Size: 385635 Color: 0

Bin 543: 1 of cap free
Amount of items: 3
Items: 
Size: 691454 Color: 1
Size: 201793 Color: 4
Size: 106753 Color: 0

Bin 544: 1 of cap free
Amount of items: 2
Items: 
Size: 676735 Color: 4
Size: 323265 Color: 3

Bin 545: 1 of cap free
Amount of items: 3
Items: 
Size: 732519 Color: 0
Size: 162652 Color: 0
Size: 104829 Color: 1

Bin 546: 1 of cap free
Amount of items: 2
Items: 
Size: 749679 Color: 2
Size: 250321 Color: 3

Bin 547: 1 of cap free
Amount of items: 3
Items: 
Size: 663147 Color: 1
Size: 169858 Color: 3
Size: 166995 Color: 0

Bin 548: 1 of cap free
Amount of items: 3
Items: 
Size: 595187 Color: 2
Size: 202546 Color: 3
Size: 202267 Color: 0

Bin 549: 1 of cap free
Amount of items: 2
Items: 
Size: 636223 Color: 1
Size: 363777 Color: 4

Bin 550: 1 of cap free
Amount of items: 3
Items: 
Size: 336500 Color: 4
Size: 333652 Color: 1
Size: 329848 Color: 3

Bin 551: 1 of cap free
Amount of items: 2
Items: 
Size: 673950 Color: 2
Size: 326050 Color: 3

Bin 552: 1 of cap free
Amount of items: 3
Items: 
Size: 754049 Color: 2
Size: 127703 Color: 2
Size: 118248 Color: 3

Bin 553: 1 of cap free
Amount of items: 3
Items: 
Size: 743146 Color: 1
Size: 129099 Color: 2
Size: 127755 Color: 0

Bin 554: 1 of cap free
Amount of items: 2
Items: 
Size: 721009 Color: 2
Size: 278991 Color: 3

Bin 555: 1 of cap free
Amount of items: 2
Items: 
Size: 640769 Color: 4
Size: 359231 Color: 0

Bin 556: 1 of cap free
Amount of items: 2
Items: 
Size: 716267 Color: 1
Size: 283733 Color: 2

Bin 557: 1 of cap free
Amount of items: 2
Items: 
Size: 687359 Color: 1
Size: 312641 Color: 2

Bin 558: 1 of cap free
Amount of items: 3
Items: 
Size: 523013 Color: 2
Size: 248053 Color: 2
Size: 228934 Color: 3

Bin 559: 1 of cap free
Amount of items: 2
Items: 
Size: 743118 Color: 2
Size: 256882 Color: 1

Bin 560: 1 of cap free
Amount of items: 3
Items: 
Size: 786447 Color: 4
Size: 107290 Color: 3
Size: 106263 Color: 1

Bin 561: 1 of cap free
Amount of items: 2
Items: 
Size: 692273 Color: 3
Size: 307727 Color: 0

Bin 562: 1 of cap free
Amount of items: 3
Items: 
Size: 574412 Color: 4
Size: 216148 Color: 0
Size: 209440 Color: 2

Bin 563: 1 of cap free
Amount of items: 3
Items: 
Size: 691786 Color: 0
Size: 204303 Color: 4
Size: 103911 Color: 4

Bin 564: 1 of cap free
Amount of items: 3
Items: 
Size: 574709 Color: 4
Size: 212693 Color: 4
Size: 212598 Color: 0

Bin 565: 1 of cap free
Amount of items: 2
Items: 
Size: 712431 Color: 2
Size: 287569 Color: 3

Bin 566: 1 of cap free
Amount of items: 3
Items: 
Size: 504041 Color: 0
Size: 249263 Color: 2
Size: 246696 Color: 3

Bin 567: 1 of cap free
Amount of items: 3
Items: 
Size: 758437 Color: 2
Size: 126957 Color: 1
Size: 114606 Color: 1

Bin 568: 1 of cap free
Amount of items: 2
Items: 
Size: 670224 Color: 3
Size: 329776 Color: 2

Bin 569: 1 of cap free
Amount of items: 2
Items: 
Size: 588878 Color: 4
Size: 411122 Color: 2

Bin 570: 1 of cap free
Amount of items: 2
Items: 
Size: 542948 Color: 0
Size: 457052 Color: 3

Bin 571: 1 of cap free
Amount of items: 2
Items: 
Size: 550860 Color: 1
Size: 449140 Color: 0

Bin 572: 1 of cap free
Amount of items: 3
Items: 
Size: 368694 Color: 2
Size: 355933 Color: 0
Size: 275373 Color: 3

Bin 573: 1 of cap free
Amount of items: 3
Items: 
Size: 358844 Color: 1
Size: 342003 Color: 2
Size: 299153 Color: 2

Bin 574: 1 of cap free
Amount of items: 3
Items: 
Size: 358949 Color: 4
Size: 340068 Color: 2
Size: 300983 Color: 3

Bin 575: 1 of cap free
Amount of items: 3
Items: 
Size: 339576 Color: 3
Size: 336251 Color: 2
Size: 324173 Color: 0

Bin 576: 1 of cap free
Amount of items: 3
Items: 
Size: 338385 Color: 3
Size: 333246 Color: 2
Size: 328369 Color: 2

Bin 577: 2 of cap free
Amount of items: 3
Items: 
Size: 387869 Color: 3
Size: 337792 Color: 3
Size: 274338 Color: 4

Bin 578: 2 of cap free
Amount of items: 2
Items: 
Size: 524702 Color: 0
Size: 475297 Color: 2

Bin 579: 2 of cap free
Amount of items: 2
Items: 
Size: 530276 Color: 1
Size: 469723 Color: 3

Bin 580: 2 of cap free
Amount of items: 2
Items: 
Size: 531436 Color: 2
Size: 468563 Color: 1

Bin 581: 2 of cap free
Amount of items: 2
Items: 
Size: 534753 Color: 4
Size: 465246 Color: 1

Bin 582: 2 of cap free
Amount of items: 2
Items: 
Size: 541614 Color: 1
Size: 458385 Color: 3

Bin 583: 2 of cap free
Amount of items: 3
Items: 
Size: 558748 Color: 0
Size: 223227 Color: 3
Size: 218024 Color: 3

Bin 584: 2 of cap free
Amount of items: 2
Items: 
Size: 560394 Color: 0
Size: 439605 Color: 1

Bin 585: 2 of cap free
Amount of items: 2
Items: 
Size: 589070 Color: 0
Size: 410929 Color: 1

Bin 586: 2 of cap free
Amount of items: 3
Items: 
Size: 593562 Color: 4
Size: 203247 Color: 3
Size: 203190 Color: 1

Bin 587: 2 of cap free
Amount of items: 2
Items: 
Size: 596967 Color: 4
Size: 403032 Color: 1

Bin 588: 2 of cap free
Amount of items: 2
Items: 
Size: 600552 Color: 3
Size: 399447 Color: 2

Bin 589: 2 of cap free
Amount of items: 3
Items: 
Size: 628557 Color: 3
Size: 188691 Color: 4
Size: 182751 Color: 2

Bin 590: 2 of cap free
Amount of items: 2
Items: 
Size: 632777 Color: 2
Size: 367222 Color: 1

Bin 591: 2 of cap free
Amount of items: 3
Items: 
Size: 642300 Color: 3
Size: 180173 Color: 0
Size: 177526 Color: 2

Bin 592: 2 of cap free
Amount of items: 3
Items: 
Size: 642339 Color: 3
Size: 179105 Color: 4
Size: 178555 Color: 2

Bin 593: 2 of cap free
Amount of items: 3
Items: 
Size: 653407 Color: 4
Size: 174100 Color: 1
Size: 172492 Color: 2

Bin 594: 2 of cap free
Amount of items: 3
Items: 
Size: 658827 Color: 3
Size: 171136 Color: 4
Size: 170036 Color: 2

Bin 595: 2 of cap free
Amount of items: 2
Items: 
Size: 663219 Color: 1
Size: 336780 Color: 4

Bin 596: 2 of cap free
Amount of items: 2
Items: 
Size: 666618 Color: 1
Size: 333381 Color: 4

Bin 597: 2 of cap free
Amount of items: 3
Items: 
Size: 678641 Color: 3
Size: 160685 Color: 2
Size: 160673 Color: 4

Bin 598: 2 of cap free
Amount of items: 3
Items: 
Size: 684472 Color: 2
Size: 157772 Color: 3
Size: 157755 Color: 2

Bin 599: 2 of cap free
Amount of items: 2
Items: 
Size: 690266 Color: 0
Size: 309733 Color: 2

Bin 600: 2 of cap free
Amount of items: 2
Items: 
Size: 696568 Color: 2
Size: 303431 Color: 1

Bin 601: 2 of cap free
Amount of items: 2
Items: 
Size: 713751 Color: 4
Size: 286248 Color: 3

Bin 602: 2 of cap free
Amount of items: 3
Items: 
Size: 721421 Color: 3
Size: 139526 Color: 4
Size: 139052 Color: 4

Bin 603: 2 of cap free
Amount of items: 2
Items: 
Size: 727771 Color: 2
Size: 272228 Color: 1

Bin 604: 2 of cap free
Amount of items: 2
Items: 
Size: 734016 Color: 4
Size: 265983 Color: 1

Bin 605: 2 of cap free
Amount of items: 3
Items: 
Size: 745548 Color: 2
Size: 127451 Color: 3
Size: 127000 Color: 3

Bin 606: 2 of cap free
Amount of items: 2
Items: 
Size: 756632 Color: 1
Size: 243367 Color: 4

Bin 607: 2 of cap free
Amount of items: 2
Items: 
Size: 783395 Color: 2
Size: 216604 Color: 4

Bin 608: 2 of cap free
Amount of items: 2
Items: 
Size: 784983 Color: 0
Size: 215016 Color: 3

Bin 609: 2 of cap free
Amount of items: 2
Items: 
Size: 793316 Color: 0
Size: 206683 Color: 2

Bin 610: 2 of cap free
Amount of items: 2
Items: 
Size: 795280 Color: 3
Size: 204719 Color: 1

Bin 611: 2 of cap free
Amount of items: 3
Items: 
Size: 603824 Color: 4
Size: 198445 Color: 1
Size: 197730 Color: 1

Bin 612: 2 of cap free
Amount of items: 3
Items: 
Size: 651379 Color: 3
Size: 174627 Color: 2
Size: 173993 Color: 0

Bin 613: 2 of cap free
Amount of items: 3
Items: 
Size: 757097 Color: 1
Size: 135314 Color: 1
Size: 107588 Color: 2

Bin 614: 2 of cap free
Amount of items: 3
Items: 
Size: 386894 Color: 2
Size: 344983 Color: 0
Size: 268122 Color: 4

Bin 615: 2 of cap free
Amount of items: 3
Items: 
Size: 733768 Color: 1
Size: 156270 Color: 2
Size: 109961 Color: 1

Bin 616: 2 of cap free
Amount of items: 3
Items: 
Size: 664132 Color: 3
Size: 182663 Color: 3
Size: 153204 Color: 4

Bin 617: 2 of cap free
Amount of items: 3
Items: 
Size: 709148 Color: 1
Size: 160743 Color: 4
Size: 130108 Color: 0

Bin 618: 2 of cap free
Amount of items: 2
Items: 
Size: 656981 Color: 0
Size: 343018 Color: 4

Bin 619: 2 of cap free
Amount of items: 3
Items: 
Size: 735373 Color: 2
Size: 137344 Color: 2
Size: 127282 Color: 4

Bin 620: 2 of cap free
Amount of items: 3
Items: 
Size: 645949 Color: 2
Size: 178018 Color: 3
Size: 176032 Color: 1

Bin 621: 2 of cap free
Amount of items: 3
Items: 
Size: 711985 Color: 4
Size: 179537 Color: 3
Size: 108477 Color: 3

Bin 622: 2 of cap free
Amount of items: 3
Items: 
Size: 769622 Color: 2
Size: 116286 Color: 2
Size: 114091 Color: 0

Bin 623: 2 of cap free
Amount of items: 2
Items: 
Size: 507972 Color: 1
Size: 492027 Color: 3

Bin 624: 2 of cap free
Amount of items: 3
Items: 
Size: 726558 Color: 3
Size: 137888 Color: 2
Size: 135553 Color: 4

Bin 625: 2 of cap free
Amount of items: 3
Items: 
Size: 618740 Color: 3
Size: 190848 Color: 2
Size: 190411 Color: 1

Bin 626: 2 of cap free
Amount of items: 3
Items: 
Size: 721924 Color: 3
Size: 139321 Color: 2
Size: 138754 Color: 0

Bin 627: 2 of cap free
Amount of items: 3
Items: 
Size: 486651 Color: 0
Size: 261429 Color: 0
Size: 251919 Color: 4

Bin 628: 2 of cap free
Amount of items: 3
Items: 
Size: 422390 Color: 1
Size: 322652 Color: 2
Size: 254957 Color: 4

Bin 629: 2 of cap free
Amount of items: 3
Items: 
Size: 699145 Color: 1
Size: 150431 Color: 3
Size: 150423 Color: 1

Bin 630: 2 of cap free
Amount of items: 3
Items: 
Size: 734515 Color: 1
Size: 133370 Color: 2
Size: 132114 Color: 3

Bin 631: 2 of cap free
Amount of items: 3
Items: 
Size: 629450 Color: 2
Size: 189114 Color: 4
Size: 181435 Color: 3

Bin 632: 2 of cap free
Amount of items: 3
Items: 
Size: 702620 Color: 0
Size: 148986 Color: 2
Size: 148393 Color: 4

Bin 633: 2 of cap free
Amount of items: 2
Items: 
Size: 633532 Color: 3
Size: 366467 Color: 1

Bin 634: 2 of cap free
Amount of items: 3
Items: 
Size: 712460 Color: 0
Size: 143912 Color: 2
Size: 143627 Color: 4

Bin 635: 2 of cap free
Amount of items: 3
Items: 
Size: 603781 Color: 2
Size: 199100 Color: 4
Size: 197118 Color: 1

Bin 636: 2 of cap free
Amount of items: 3
Items: 
Size: 706635 Color: 3
Size: 147116 Color: 3
Size: 146248 Color: 1

Bin 637: 2 of cap free
Amount of items: 3
Items: 
Size: 375780 Color: 4
Size: 331445 Color: 2
Size: 292774 Color: 3

Bin 638: 2 of cap free
Amount of items: 2
Items: 
Size: 508654 Color: 2
Size: 491345 Color: 1

Bin 639: 2 of cap free
Amount of items: 3
Items: 
Size: 395034 Color: 3
Size: 331845 Color: 0
Size: 273120 Color: 1

Bin 640: 2 of cap free
Amount of items: 3
Items: 
Size: 726380 Color: 1
Size: 145856 Color: 1
Size: 127763 Color: 2

Bin 641: 2 of cap free
Amount of items: 3
Items: 
Size: 500744 Color: 0
Size: 253450 Color: 0
Size: 245805 Color: 2

Bin 642: 2 of cap free
Amount of items: 3
Items: 
Size: 718691 Color: 2
Size: 140716 Color: 4
Size: 140592 Color: 1

Bin 643: 2 of cap free
Amount of items: 3
Items: 
Size: 656769 Color: 0
Size: 171775 Color: 0
Size: 171455 Color: 3

Bin 644: 2 of cap free
Amount of items: 3
Items: 
Size: 626848 Color: 1
Size: 189103 Color: 3
Size: 184048 Color: 3

Bin 645: 2 of cap free
Amount of items: 2
Items: 
Size: 680428 Color: 1
Size: 319571 Color: 4

Bin 646: 2 of cap free
Amount of items: 3
Items: 
Size: 740944 Color: 2
Size: 129914 Color: 0
Size: 129141 Color: 1

Bin 647: 2 of cap free
Amount of items: 2
Items: 
Size: 542423 Color: 0
Size: 457576 Color: 4

Bin 648: 2 of cap free
Amount of items: 3
Items: 
Size: 617170 Color: 4
Size: 193772 Color: 4
Size: 189057 Color: 1

Bin 649: 2 of cap free
Amount of items: 3
Items: 
Size: 427534 Color: 0
Size: 316787 Color: 4
Size: 255678 Color: 2

Bin 650: 2 of cap free
Amount of items: 2
Items: 
Size: 781842 Color: 0
Size: 218157 Color: 1

Bin 651: 2 of cap free
Amount of items: 3
Items: 
Size: 375336 Color: 0
Size: 313560 Color: 4
Size: 311103 Color: 4

Bin 652: 2 of cap free
Amount of items: 2
Items: 
Size: 763843 Color: 2
Size: 236156 Color: 3

Bin 653: 2 of cap free
Amount of items: 3
Items: 
Size: 543068 Color: 3
Size: 228530 Color: 0
Size: 228401 Color: 4

Bin 654: 2 of cap free
Amount of items: 2
Items: 
Size: 515333 Color: 2
Size: 484666 Color: 1

Bin 655: 2 of cap free
Amount of items: 3
Items: 
Size: 504917 Color: 2
Size: 247911 Color: 4
Size: 247171 Color: 3

Bin 656: 2 of cap free
Amount of items: 3
Items: 
Size: 368076 Color: 0
Size: 340671 Color: 3
Size: 291252 Color: 2

Bin 657: 2 of cap free
Amount of items: 2
Items: 
Size: 793658 Color: 2
Size: 206341 Color: 3

Bin 658: 2 of cap free
Amount of items: 3
Items: 
Size: 695897 Color: 2
Size: 170553 Color: 0
Size: 133549 Color: 3

Bin 659: 2 of cap free
Amount of items: 3
Items: 
Size: 703712 Color: 1
Size: 157723 Color: 2
Size: 138564 Color: 4

Bin 660: 2 of cap free
Amount of items: 3
Items: 
Size: 729702 Color: 0
Size: 135510 Color: 3
Size: 134787 Color: 0

Bin 661: 2 of cap free
Amount of items: 3
Items: 
Size: 375102 Color: 1
Size: 330315 Color: 0
Size: 294582 Color: 3

Bin 662: 2 of cap free
Amount of items: 2
Items: 
Size: 626959 Color: 3
Size: 373040 Color: 1

Bin 663: 2 of cap free
Amount of items: 3
Items: 
Size: 639879 Color: 3
Size: 181665 Color: 4
Size: 178455 Color: 0

Bin 664: 2 of cap free
Amount of items: 2
Items: 
Size: 571012 Color: 0
Size: 428987 Color: 2

Bin 665: 2 of cap free
Amount of items: 3
Items: 
Size: 760643 Color: 0
Size: 139181 Color: 0
Size: 100175 Color: 3

Bin 666: 2 of cap free
Amount of items: 2
Items: 
Size: 696215 Color: 4
Size: 303784 Color: 3

Bin 667: 2 of cap free
Amount of items: 3
Items: 
Size: 478074 Color: 3
Size: 262897 Color: 1
Size: 259028 Color: 1

Bin 668: 2 of cap free
Amount of items: 3
Items: 
Size: 376341 Color: 2
Size: 341344 Color: 0
Size: 282314 Color: 1

Bin 669: 2 of cap free
Amount of items: 2
Items: 
Size: 665045 Color: 1
Size: 334954 Color: 3

Bin 670: 2 of cap free
Amount of items: 2
Items: 
Size: 614688 Color: 0
Size: 385311 Color: 4

Bin 671: 2 of cap free
Amount of items: 2
Items: 
Size: 621079 Color: 2
Size: 378920 Color: 3

Bin 672: 2 of cap free
Amount of items: 2
Items: 
Size: 766216 Color: 1
Size: 233783 Color: 3

Bin 673: 2 of cap free
Amount of items: 2
Items: 
Size: 747043 Color: 3
Size: 252956 Color: 1

Bin 674: 2 of cap free
Amount of items: 2
Items: 
Size: 669752 Color: 2
Size: 330247 Color: 3

Bin 675: 2 of cap free
Amount of items: 2
Items: 
Size: 727499 Color: 4
Size: 272500 Color: 3

Bin 676: 2 of cap free
Amount of items: 3
Items: 
Size: 426123 Color: 3
Size: 318880 Color: 0
Size: 254996 Color: 1

Bin 677: 2 of cap free
Amount of items: 3
Items: 
Size: 376686 Color: 3
Size: 354772 Color: 1
Size: 268541 Color: 2

Bin 678: 2 of cap free
Amount of items: 3
Items: 
Size: 373609 Color: 4
Size: 321050 Color: 1
Size: 305340 Color: 3

Bin 679: 2 of cap free
Amount of items: 3
Items: 
Size: 369982 Color: 1
Size: 368169 Color: 1
Size: 261848 Color: 0

Bin 680: 3 of cap free
Amount of items: 3
Items: 
Size: 377537 Color: 4
Size: 311338 Color: 4
Size: 311123 Color: 1

Bin 681: 3 of cap free
Amount of items: 3
Items: 
Size: 427678 Color: 1
Size: 296442 Color: 0
Size: 275878 Color: 4

Bin 682: 3 of cap free
Amount of items: 2
Items: 
Size: 507220 Color: 4
Size: 492778 Color: 1

Bin 683: 3 of cap free
Amount of items: 2
Items: 
Size: 508302 Color: 1
Size: 491696 Color: 3

Bin 684: 3 of cap free
Amount of items: 2
Items: 
Size: 521706 Color: 2
Size: 478292 Color: 0

Bin 685: 3 of cap free
Amount of items: 2
Items: 
Size: 528135 Color: 0
Size: 471863 Color: 1

Bin 686: 3 of cap free
Amount of items: 2
Items: 
Size: 540093 Color: 4
Size: 459905 Color: 1

Bin 687: 3 of cap free
Amount of items: 2
Items: 
Size: 540335 Color: 2
Size: 459663 Color: 1

Bin 688: 3 of cap free
Amount of items: 2
Items: 
Size: 557992 Color: 2
Size: 442006 Color: 4

Bin 689: 3 of cap free
Amount of items: 2
Items: 
Size: 579073 Color: 4
Size: 420925 Color: 3

Bin 690: 3 of cap free
Amount of items: 3
Items: 
Size: 603247 Color: 0
Size: 198588 Color: 2
Size: 198163 Color: 3

Bin 691: 3 of cap free
Amount of items: 2
Items: 
Size: 604031 Color: 2
Size: 395967 Color: 1

Bin 692: 3 of cap free
Amount of items: 2
Items: 
Size: 607691 Color: 3
Size: 392307 Color: 1

Bin 693: 3 of cap free
Amount of items: 2
Items: 
Size: 607874 Color: 0
Size: 392124 Color: 4

Bin 694: 3 of cap free
Amount of items: 3
Items: 
Size: 608342 Color: 3
Size: 195872 Color: 2
Size: 195784 Color: 1

Bin 695: 3 of cap free
Amount of items: 3
Items: 
Size: 610029 Color: 0
Size: 196377 Color: 1
Size: 193592 Color: 2

Bin 696: 3 of cap free
Amount of items: 2
Items: 
Size: 611567 Color: 4
Size: 388431 Color: 1

Bin 697: 3 of cap free
Amount of items: 2
Items: 
Size: 625001 Color: 0
Size: 374997 Color: 2

Bin 698: 3 of cap free
Amount of items: 3
Items: 
Size: 628300 Color: 3
Size: 187591 Color: 2
Size: 184107 Color: 4

Bin 699: 3 of cap free
Amount of items: 3
Items: 
Size: 630766 Color: 3
Size: 186081 Color: 0
Size: 183151 Color: 2

Bin 700: 3 of cap free
Amount of items: 3
Items: 
Size: 632042 Color: 1
Size: 184012 Color: 1
Size: 183944 Color: 0

Bin 701: 3 of cap free
Amount of items: 2
Items: 
Size: 633085 Color: 2
Size: 366913 Color: 4

Bin 702: 3 of cap free
Amount of items: 2
Items: 
Size: 636069 Color: 1
Size: 363929 Color: 3

Bin 703: 3 of cap free
Amount of items: 3
Items: 
Size: 640625 Color: 3
Size: 179790 Color: 1
Size: 179583 Color: 2

Bin 704: 3 of cap free
Amount of items: 3
Items: 
Size: 644575 Color: 3
Size: 177930 Color: 2
Size: 177493 Color: 2

Bin 705: 3 of cap free
Amount of items: 3
Items: 
Size: 647761 Color: 3
Size: 176128 Color: 0
Size: 176109 Color: 4

Bin 706: 3 of cap free
Amount of items: 3
Items: 
Size: 660728 Color: 3
Size: 170406 Color: 1
Size: 168864 Color: 1

Bin 707: 3 of cap free
Amount of items: 3
Items: 
Size: 664509 Color: 3
Size: 168669 Color: 4
Size: 166820 Color: 4

Bin 708: 3 of cap free
Amount of items: 3
Items: 
Size: 671253 Color: 3
Size: 164436 Color: 0
Size: 164309 Color: 0

Bin 709: 3 of cap free
Amount of items: 2
Items: 
Size: 685628 Color: 2
Size: 314370 Color: 4

Bin 710: 3 of cap free
Amount of items: 2
Items: 
Size: 710485 Color: 0
Size: 289513 Color: 4

Bin 711: 3 of cap free
Amount of items: 2
Items: 
Size: 712812 Color: 4
Size: 287186 Color: 0

Bin 712: 3 of cap free
Amount of items: 2
Items: 
Size: 714291 Color: 4
Size: 285707 Color: 0

Bin 713: 3 of cap free
Amount of items: 2
Items: 
Size: 728502 Color: 2
Size: 271496 Color: 4

Bin 714: 3 of cap free
Amount of items: 3
Items: 
Size: 735795 Color: 2
Size: 132147 Color: 4
Size: 132056 Color: 1

Bin 715: 3 of cap free
Amount of items: 2
Items: 
Size: 751671 Color: 3
Size: 248327 Color: 4

Bin 716: 3 of cap free
Amount of items: 2
Items: 
Size: 793199 Color: 0
Size: 206799 Color: 4

Bin 717: 3 of cap free
Amount of items: 3
Items: 
Size: 743183 Color: 2
Size: 128909 Color: 1
Size: 127906 Color: 4

Bin 718: 3 of cap free
Amount of items: 3
Items: 
Size: 634080 Color: 1
Size: 216504 Color: 2
Size: 149414 Color: 2

Bin 719: 3 of cap free
Amount of items: 3
Items: 
Size: 664978 Color: 0
Size: 167717 Color: 2
Size: 167303 Color: 2

Bin 720: 3 of cap free
Amount of items: 3
Items: 
Size: 605583 Color: 1
Size: 197431 Color: 3
Size: 196984 Color: 2

Bin 721: 3 of cap free
Amount of items: 3
Items: 
Size: 755405 Color: 3
Size: 125818 Color: 0
Size: 118775 Color: 1

Bin 722: 3 of cap free
Amount of items: 2
Items: 
Size: 515455 Color: 1
Size: 484543 Color: 3

Bin 723: 3 of cap free
Amount of items: 3
Items: 
Size: 652740 Color: 4
Size: 174380 Color: 2
Size: 172878 Color: 0

Bin 724: 3 of cap free
Amount of items: 3
Items: 
Size: 558607 Color: 0
Size: 221134 Color: 3
Size: 220257 Color: 0

Bin 725: 3 of cap free
Amount of items: 3
Items: 
Size: 660985 Color: 0
Size: 170658 Color: 2
Size: 168355 Color: 3

Bin 726: 3 of cap free
Amount of items: 3
Items: 
Size: 426551 Color: 1
Size: 310828 Color: 2
Size: 262619 Color: 2

Bin 727: 3 of cap free
Amount of items: 3
Items: 
Size: 662688 Color: 0
Size: 170015 Color: 3
Size: 167295 Color: 1

Bin 728: 3 of cap free
Amount of items: 3
Items: 
Size: 710104 Color: 1
Size: 153889 Color: 2
Size: 136005 Color: 0

Bin 729: 3 of cap free
Amount of items: 2
Items: 
Size: 791821 Color: 2
Size: 208177 Color: 0

Bin 730: 3 of cap free
Amount of items: 2
Items: 
Size: 537541 Color: 0
Size: 462457 Color: 4

Bin 731: 3 of cap free
Amount of items: 3
Items: 
Size: 653244 Color: 3
Size: 174000 Color: 0
Size: 172754 Color: 2

Bin 732: 3 of cap free
Amount of items: 3
Items: 
Size: 678400 Color: 4
Size: 160876 Color: 0
Size: 160722 Color: 0

Bin 733: 3 of cap free
Amount of items: 3
Items: 
Size: 727767 Color: 1
Size: 136541 Color: 1
Size: 135690 Color: 0

Bin 734: 3 of cap free
Amount of items: 3
Items: 
Size: 656972 Color: 2
Size: 174238 Color: 4
Size: 168788 Color: 1

Bin 735: 3 of cap free
Amount of items: 3
Items: 
Size: 611807 Color: 2
Size: 195144 Color: 1
Size: 193047 Color: 0

Bin 736: 3 of cap free
Amount of items: 3
Items: 
Size: 693482 Color: 3
Size: 153274 Color: 2
Size: 153242 Color: 3

Bin 737: 3 of cap free
Amount of items: 3
Items: 
Size: 718110 Color: 4
Size: 141209 Color: 0
Size: 140679 Color: 2

Bin 738: 3 of cap free
Amount of items: 3
Items: 
Size: 681543 Color: 4
Size: 159304 Color: 0
Size: 159151 Color: 1

Bin 739: 3 of cap free
Amount of items: 2
Items: 
Size: 687044 Color: 2
Size: 312954 Color: 0

Bin 740: 3 of cap free
Amount of items: 2
Items: 
Size: 737838 Color: 2
Size: 262160 Color: 0

Bin 741: 3 of cap free
Amount of items: 2
Items: 
Size: 709434 Color: 3
Size: 290564 Color: 4

Bin 742: 3 of cap free
Amount of items: 3
Items: 
Size: 499937 Color: 4
Size: 254074 Color: 1
Size: 245987 Color: 3

Bin 743: 3 of cap free
Amount of items: 3
Items: 
Size: 705127 Color: 0
Size: 147550 Color: 3
Size: 147321 Color: 3

Bin 744: 3 of cap free
Amount of items: 2
Items: 
Size: 613979 Color: 4
Size: 386019 Color: 0

Bin 745: 3 of cap free
Amount of items: 3
Items: 
Size: 721702 Color: 1
Size: 143590 Color: 1
Size: 134706 Color: 0

Bin 746: 3 of cap free
Amount of items: 3
Items: 
Size: 577722 Color: 4
Size: 215601 Color: 1
Size: 206675 Color: 2

Bin 747: 3 of cap free
Amount of items: 3
Items: 
Size: 714661 Color: 2
Size: 142975 Color: 0
Size: 142362 Color: 4

Bin 748: 3 of cap free
Amount of items: 3
Items: 
Size: 705482 Color: 1
Size: 147387 Color: 3
Size: 147129 Color: 1

Bin 749: 3 of cap free
Amount of items: 2
Items: 
Size: 656275 Color: 0
Size: 343723 Color: 1

Bin 750: 3 of cap free
Amount of items: 2
Items: 
Size: 600419 Color: 0
Size: 399579 Color: 4

Bin 751: 3 of cap free
Amount of items: 2
Items: 
Size: 534041 Color: 4
Size: 465957 Color: 2

Bin 752: 3 of cap free
Amount of items: 2
Items: 
Size: 642178 Color: 2
Size: 357820 Color: 4

Bin 753: 3 of cap free
Amount of items: 2
Items: 
Size: 590661 Color: 0
Size: 409337 Color: 3

Bin 754: 3 of cap free
Amount of items: 3
Items: 
Size: 502810 Color: 2
Size: 251570 Color: 1
Size: 245618 Color: 3

Bin 755: 3 of cap free
Amount of items: 2
Items: 
Size: 500692 Color: 2
Size: 499306 Color: 4

Bin 756: 3 of cap free
Amount of items: 2
Items: 
Size: 690435 Color: 3
Size: 309563 Color: 4

Bin 757: 3 of cap free
Amount of items: 3
Items: 
Size: 675385 Color: 3
Size: 162727 Color: 1
Size: 161886 Color: 1

Bin 758: 3 of cap free
Amount of items: 2
Items: 
Size: 758817 Color: 0
Size: 241181 Color: 4

Bin 759: 3 of cap free
Amount of items: 2
Items: 
Size: 515466 Color: 4
Size: 484532 Color: 3

Bin 760: 3 of cap free
Amount of items: 3
Items: 
Size: 503312 Color: 1
Size: 248734 Color: 4
Size: 247952 Color: 1

Bin 761: 3 of cap free
Amount of items: 2
Items: 
Size: 505456 Color: 0
Size: 494542 Color: 2

Bin 762: 3 of cap free
Amount of items: 3
Items: 
Size: 504126 Color: 0
Size: 250117 Color: 0
Size: 245755 Color: 4

Bin 763: 3 of cap free
Amount of items: 2
Items: 
Size: 715007 Color: 2
Size: 284991 Color: 0

Bin 764: 3 of cap free
Amount of items: 3
Items: 
Size: 365811 Color: 1
Size: 361647 Color: 0
Size: 272540 Color: 3

Bin 765: 4 of cap free
Amount of items: 3
Items: 
Size: 394905 Color: 3
Size: 336245 Color: 0
Size: 268847 Color: 4

Bin 766: 4 of cap free
Amount of items: 3
Items: 
Size: 430737 Color: 4
Size: 311795 Color: 0
Size: 257465 Color: 1

Bin 767: 4 of cap free
Amount of items: 3
Items: 
Size: 428066 Color: 1
Size: 307752 Color: 3
Size: 264179 Color: 0

Bin 768: 4 of cap free
Amount of items: 2
Items: 
Size: 512935 Color: 1
Size: 487062 Color: 0

Bin 769: 4 of cap free
Amount of items: 2
Items: 
Size: 538732 Color: 2
Size: 461265 Color: 0

Bin 770: 4 of cap free
Amount of items: 2
Items: 
Size: 562649 Color: 0
Size: 437348 Color: 1

Bin 771: 4 of cap free
Amount of items: 2
Items: 
Size: 565289 Color: 0
Size: 434708 Color: 2

Bin 772: 4 of cap free
Amount of items: 2
Items: 
Size: 576518 Color: 1
Size: 423479 Color: 3

Bin 773: 4 of cap free
Amount of items: 2
Items: 
Size: 578189 Color: 1
Size: 421808 Color: 0

Bin 774: 4 of cap free
Amount of items: 2
Items: 
Size: 583533 Color: 0
Size: 416464 Color: 2

Bin 775: 4 of cap free
Amount of items: 2
Items: 
Size: 591740 Color: 3
Size: 408257 Color: 2

Bin 776: 4 of cap free
Amount of items: 2
Items: 
Size: 600196 Color: 3
Size: 399801 Color: 4

Bin 777: 4 of cap free
Amount of items: 2
Items: 
Size: 602293 Color: 4
Size: 397704 Color: 1

Bin 778: 4 of cap free
Amount of items: 3
Items: 
Size: 620437 Color: 3
Size: 190830 Color: 0
Size: 188730 Color: 0

Bin 779: 4 of cap free
Amount of items: 2
Items: 
Size: 628897 Color: 1
Size: 371100 Color: 4

Bin 780: 4 of cap free
Amount of items: 2
Items: 
Size: 639101 Color: 2
Size: 360896 Color: 4

Bin 781: 4 of cap free
Amount of items: 3
Items: 
Size: 647155 Color: 3
Size: 176705 Color: 2
Size: 176137 Color: 0

Bin 782: 4 of cap free
Amount of items: 3
Items: 
Size: 657964 Color: 3
Size: 171970 Color: 4
Size: 170063 Color: 0

Bin 783: 4 of cap free
Amount of items: 3
Items: 
Size: 666688 Color: 3
Size: 167147 Color: 0
Size: 166162 Color: 0

Bin 784: 4 of cap free
Amount of items: 2
Items: 
Size: 701153 Color: 3
Size: 298844 Color: 2

Bin 785: 4 of cap free
Amount of items: 2
Items: 
Size: 713317 Color: 3
Size: 286680 Color: 0

Bin 786: 4 of cap free
Amount of items: 2
Items: 
Size: 739950 Color: 0
Size: 260047 Color: 1

Bin 787: 4 of cap free
Amount of items: 3
Items: 
Size: 746428 Color: 2
Size: 127543 Color: 4
Size: 126026 Color: 0

Bin 788: 4 of cap free
Amount of items: 3
Items: 
Size: 748865 Color: 2
Size: 125611 Color: 4
Size: 125521 Color: 0

Bin 789: 4 of cap free
Amount of items: 2
Items: 
Size: 789844 Color: 4
Size: 210153 Color: 1

Bin 790: 4 of cap free
Amount of items: 2
Items: 
Size: 793420 Color: 3
Size: 206577 Color: 0

Bin 791: 4 of cap free
Amount of items: 3
Items: 
Size: 605972 Color: 1
Size: 197149 Color: 0
Size: 196876 Color: 3

Bin 792: 4 of cap free
Amount of items: 3
Items: 
Size: 376066 Color: 4
Size: 351185 Color: 0
Size: 272746 Color: 2

Bin 793: 4 of cap free
Amount of items: 2
Items: 
Size: 657513 Color: 1
Size: 342484 Color: 3

Bin 794: 4 of cap free
Amount of items: 3
Items: 
Size: 629551 Color: 3
Size: 187032 Color: 3
Size: 183414 Color: 2

Bin 795: 4 of cap free
Amount of items: 3
Items: 
Size: 614242 Color: 4
Size: 194495 Color: 3
Size: 191260 Color: 3

Bin 796: 4 of cap free
Amount of items: 3
Items: 
Size: 372804 Color: 1
Size: 316279 Color: 2
Size: 310914 Color: 0

Bin 797: 4 of cap free
Amount of items: 3
Items: 
Size: 738530 Color: 3
Size: 131613 Color: 2
Size: 129854 Color: 4

Bin 798: 4 of cap free
Amount of items: 3
Items: 
Size: 653591 Color: 0
Size: 173470 Color: 1
Size: 172936 Color: 1

Bin 799: 4 of cap free
Amount of items: 3
Items: 
Size: 759683 Color: 4
Size: 120712 Color: 3
Size: 119602 Color: 2

Bin 800: 4 of cap free
Amount of items: 3
Items: 
Size: 617185 Color: 4
Size: 274192 Color: 4
Size: 108620 Color: 0

Bin 801: 4 of cap free
Amount of items: 3
Items: 
Size: 432324 Color: 3
Size: 299310 Color: 3
Size: 268363 Color: 1

Bin 802: 4 of cap free
Amount of items: 2
Items: 
Size: 516014 Color: 1
Size: 483983 Color: 3

Bin 803: 4 of cap free
Amount of items: 2
Items: 
Size: 586984 Color: 2
Size: 413013 Color: 3

Bin 804: 4 of cap free
Amount of items: 3
Items: 
Size: 632969 Color: 3
Size: 186344 Color: 3
Size: 180684 Color: 1

Bin 805: 4 of cap free
Amount of items: 3
Items: 
Size: 648370 Color: 2
Size: 176718 Color: 3
Size: 174909 Color: 0

Bin 806: 4 of cap free
Amount of items: 2
Items: 
Size: 766815 Color: 4
Size: 233182 Color: 2

Bin 807: 4 of cap free
Amount of items: 3
Items: 
Size: 558194 Color: 3
Size: 226298 Color: 1
Size: 215505 Color: 2

Bin 808: 4 of cap free
Amount of items: 3
Items: 
Size: 691801 Color: 3
Size: 154756 Color: 2
Size: 153440 Color: 3

Bin 809: 4 of cap free
Amount of items: 3
Items: 
Size: 622024 Color: 1
Size: 192795 Color: 2
Size: 185178 Color: 2

Bin 810: 4 of cap free
Amount of items: 3
Items: 
Size: 686651 Color: 4
Size: 212142 Color: 1
Size: 101204 Color: 2

Bin 811: 4 of cap free
Amount of items: 2
Items: 
Size: 614062 Color: 4
Size: 385935 Color: 2

Bin 812: 4 of cap free
Amount of items: 2
Items: 
Size: 526198 Color: 0
Size: 473799 Color: 4

Bin 813: 4 of cap free
Amount of items: 2
Items: 
Size: 620840 Color: 0
Size: 379157 Color: 3

Bin 814: 4 of cap free
Amount of items: 3
Items: 
Size: 368972 Color: 4
Size: 318690 Color: 0
Size: 312335 Color: 3

Bin 815: 4 of cap free
Amount of items: 2
Items: 
Size: 595284 Color: 1
Size: 404713 Color: 0

Bin 816: 4 of cap free
Amount of items: 2
Items: 
Size: 787342 Color: 4
Size: 212655 Color: 1

Bin 817: 4 of cap free
Amount of items: 2
Items: 
Size: 601173 Color: 0
Size: 398824 Color: 1

Bin 818: 4 of cap free
Amount of items: 2
Items: 
Size: 552453 Color: 4
Size: 447544 Color: 1

Bin 819: 4 of cap free
Amount of items: 3
Items: 
Size: 367905 Color: 4
Size: 353241 Color: 0
Size: 278851 Color: 4

Bin 820: 4 of cap free
Amount of items: 3
Items: 
Size: 361636 Color: 3
Size: 342932 Color: 0
Size: 295429 Color: 1

Bin 821: 5 of cap free
Amount of items: 3
Items: 
Size: 378258 Color: 1
Size: 325368 Color: 4
Size: 296370 Color: 3

Bin 822: 5 of cap free
Amount of items: 2
Items: 
Size: 509517 Color: 3
Size: 490479 Color: 4

Bin 823: 5 of cap free
Amount of items: 2
Items: 
Size: 513993 Color: 3
Size: 486003 Color: 0

Bin 824: 5 of cap free
Amount of items: 2
Items: 
Size: 524154 Color: 3
Size: 475842 Color: 1

Bin 825: 5 of cap free
Amount of items: 2
Items: 
Size: 536531 Color: 1
Size: 463465 Color: 4

Bin 826: 5 of cap free
Amount of items: 2
Items: 
Size: 560046 Color: 1
Size: 439950 Color: 3

Bin 827: 5 of cap free
Amount of items: 2
Items: 
Size: 560375 Color: 0
Size: 439621 Color: 2

Bin 828: 5 of cap free
Amount of items: 2
Items: 
Size: 584127 Color: 1
Size: 415869 Color: 0

Bin 829: 5 of cap free
Amount of items: 2
Items: 
Size: 585468 Color: 1
Size: 414528 Color: 3

Bin 830: 5 of cap free
Amount of items: 2
Items: 
Size: 594013 Color: 2
Size: 405983 Color: 1

Bin 831: 5 of cap free
Amount of items: 2
Items: 
Size: 607038 Color: 3
Size: 392958 Color: 2

Bin 832: 5 of cap free
Amount of items: 3
Items: 
Size: 613503 Color: 0
Size: 194301 Color: 2
Size: 192192 Color: 1

Bin 833: 5 of cap free
Amount of items: 2
Items: 
Size: 620635 Color: 1
Size: 379361 Color: 3

Bin 834: 5 of cap free
Amount of items: 3
Items: 
Size: 630835 Color: 3
Size: 185419 Color: 0
Size: 183742 Color: 2

Bin 835: 5 of cap free
Amount of items: 2
Items: 
Size: 630851 Color: 4
Size: 369145 Color: 0

Bin 836: 5 of cap free
Amount of items: 2
Items: 
Size: 636817 Color: 1
Size: 363179 Color: 0

Bin 837: 5 of cap free
Amount of items: 3
Items: 
Size: 640007 Color: 3
Size: 180095 Color: 2
Size: 179894 Color: 2

Bin 838: 5 of cap free
Amount of items: 2
Items: 
Size: 646638 Color: 1
Size: 353358 Color: 0

Bin 839: 5 of cap free
Amount of items: 3
Items: 
Size: 654899 Color: 3
Size: 172613 Color: 2
Size: 172484 Color: 2

Bin 840: 5 of cap free
Amount of items: 2
Items: 
Size: 655067 Color: 1
Size: 344929 Color: 0

Bin 841: 5 of cap free
Amount of items: 2
Items: 
Size: 660959 Color: 0
Size: 339037 Color: 2

Bin 842: 5 of cap free
Amount of items: 2
Items: 
Size: 664758 Color: 2
Size: 335238 Color: 3

Bin 843: 5 of cap free
Amount of items: 2
Items: 
Size: 678293 Color: 2
Size: 321703 Color: 4

Bin 844: 5 of cap free
Amount of items: 2
Items: 
Size: 680117 Color: 1
Size: 319879 Color: 4

Bin 845: 5 of cap free
Amount of items: 3
Items: 
Size: 687368 Color: 0
Size: 156531 Color: 1
Size: 156097 Color: 0

Bin 846: 5 of cap free
Amount of items: 3
Items: 
Size: 699659 Color: 1
Size: 150233 Color: 0
Size: 150104 Color: 4

Bin 847: 5 of cap free
Amount of items: 3
Items: 
Size: 713173 Color: 4
Size: 143424 Color: 1
Size: 143399 Color: 2

Bin 848: 5 of cap free
Amount of items: 2
Items: 
Size: 714113 Color: 0
Size: 285883 Color: 1

Bin 849: 5 of cap free
Amount of items: 2
Items: 
Size: 754851 Color: 2
Size: 245145 Color: 3

Bin 850: 5 of cap free
Amount of items: 2
Items: 
Size: 755973 Color: 3
Size: 244023 Color: 1

Bin 851: 5 of cap free
Amount of items: 2
Items: 
Size: 770751 Color: 3
Size: 229245 Color: 0

Bin 852: 5 of cap free
Amount of items: 3
Items: 
Size: 776036 Color: 0
Size: 112619 Color: 3
Size: 111341 Color: 0

Bin 853: 5 of cap free
Amount of items: 2
Items: 
Size: 608117 Color: 0
Size: 391879 Color: 3

Bin 854: 5 of cap free
Amount of items: 3
Items: 
Size: 739222 Color: 3
Size: 131007 Color: 1
Size: 129767 Color: 3

Bin 855: 5 of cap free
Amount of items: 3
Items: 
Size: 723192 Color: 2
Size: 138621 Color: 3
Size: 138183 Color: 2

Bin 856: 5 of cap free
Amount of items: 3
Items: 
Size: 716667 Color: 2
Size: 144028 Color: 2
Size: 139301 Color: 0

Bin 857: 5 of cap free
Amount of items: 3
Items: 
Size: 683040 Color: 3
Size: 159213 Color: 0
Size: 157743 Color: 3

Bin 858: 5 of cap free
Amount of items: 3
Items: 
Size: 670505 Color: 1
Size: 165568 Color: 3
Size: 163923 Color: 2

Bin 859: 5 of cap free
Amount of items: 2
Items: 
Size: 644746 Color: 3
Size: 355250 Color: 4

Bin 860: 5 of cap free
Amount of items: 3
Items: 
Size: 392320 Color: 3
Size: 311149 Color: 2
Size: 296527 Color: 1

Bin 861: 5 of cap free
Amount of items: 3
Items: 
Size: 375170 Color: 0
Size: 324540 Color: 1
Size: 300286 Color: 4

Bin 862: 5 of cap free
Amount of items: 3
Items: 
Size: 716134 Color: 3
Size: 141962 Color: 1
Size: 141900 Color: 3

Bin 863: 5 of cap free
Amount of items: 2
Items: 
Size: 675473 Color: 3
Size: 324523 Color: 0

Bin 864: 5 of cap free
Amount of items: 3
Items: 
Size: 543599 Color: 2
Size: 228827 Color: 0
Size: 227570 Color: 1

Bin 865: 5 of cap free
Amount of items: 3
Items: 
Size: 667219 Color: 1
Size: 166923 Color: 1
Size: 165854 Color: 2

Bin 866: 5 of cap free
Amount of items: 3
Items: 
Size: 593062 Color: 0
Size: 203619 Color: 0
Size: 203315 Color: 3

Bin 867: 5 of cap free
Amount of items: 3
Items: 
Size: 721587 Color: 4
Size: 139562 Color: 4
Size: 138847 Color: 1

Bin 868: 5 of cap free
Amount of items: 2
Items: 
Size: 588178 Color: 3
Size: 411818 Color: 2

Bin 869: 5 of cap free
Amount of items: 3
Items: 
Size: 637463 Color: 2
Size: 184345 Color: 1
Size: 178188 Color: 2

Bin 870: 5 of cap free
Amount of items: 2
Items: 
Size: 558481 Color: 0
Size: 441515 Color: 1

Bin 871: 5 of cap free
Amount of items: 2
Items: 
Size: 613545 Color: 0
Size: 386451 Color: 1

Bin 872: 5 of cap free
Amount of items: 2
Items: 
Size: 676689 Color: 4
Size: 323307 Color: 1

Bin 873: 5 of cap free
Amount of items: 2
Items: 
Size: 726551 Color: 2
Size: 273445 Color: 3

Bin 874: 5 of cap free
Amount of items: 3
Items: 
Size: 430547 Color: 4
Size: 295128 Color: 0
Size: 274321 Color: 2

Bin 875: 5 of cap free
Amount of items: 2
Items: 
Size: 721599 Color: 1
Size: 278397 Color: 2

Bin 876: 5 of cap free
Amount of items: 3
Items: 
Size: 625097 Color: 4
Size: 191404 Color: 3
Size: 183495 Color: 0

Bin 877: 5 of cap free
Amount of items: 3
Items: 
Size: 627015 Color: 1
Size: 190667 Color: 3
Size: 182314 Color: 0

Bin 878: 5 of cap free
Amount of items: 2
Items: 
Size: 541433 Color: 3
Size: 458563 Color: 0

Bin 879: 5 of cap free
Amount of items: 2
Items: 
Size: 673644 Color: 0
Size: 326352 Color: 3

Bin 880: 5 of cap free
Amount of items: 2
Items: 
Size: 550969 Color: 4
Size: 449027 Color: 0

Bin 881: 5 of cap free
Amount of items: 2
Items: 
Size: 646833 Color: 4
Size: 353163 Color: 0

Bin 882: 5 of cap free
Amount of items: 3
Items: 
Size: 372616 Color: 3
Size: 370695 Color: 3
Size: 256685 Color: 2

Bin 883: 5 of cap free
Amount of items: 3
Items: 
Size: 368089 Color: 4
Size: 367208 Color: 0
Size: 264699 Color: 3

Bin 884: 5 of cap free
Amount of items: 3
Items: 
Size: 368258 Color: 2
Size: 353692 Color: 0
Size: 278046 Color: 2

Bin 885: 5 of cap free
Amount of items: 3
Items: 
Size: 351839 Color: 1
Size: 343600 Color: 0
Size: 304557 Color: 0

Bin 886: 5 of cap free
Amount of items: 3
Items: 
Size: 345055 Color: 0
Size: 343619 Color: 3
Size: 311322 Color: 3

Bin 887: 6 of cap free
Amount of items: 3
Items: 
Size: 649142 Color: 3
Size: 176314 Color: 4
Size: 174539 Color: 1

Bin 888: 6 of cap free
Amount of items: 3
Items: 
Size: 655111 Color: 4
Size: 172850 Color: 1
Size: 172034 Color: 1

Bin 889: 6 of cap free
Amount of items: 3
Items: 
Size: 712169 Color: 2
Size: 145000 Color: 0
Size: 142826 Color: 3

Bin 890: 6 of cap free
Amount of items: 3
Items: 
Size: 377425 Color: 0
Size: 324330 Color: 1
Size: 298240 Color: 3

Bin 891: 6 of cap free
Amount of items: 3
Items: 
Size: 392685 Color: 0
Size: 330924 Color: 0
Size: 276386 Color: 2

Bin 892: 6 of cap free
Amount of items: 3
Items: 
Size: 422137 Color: 1
Size: 293661 Color: 2
Size: 284197 Color: 4

Bin 893: 6 of cap free
Amount of items: 2
Items: 
Size: 502427 Color: 0
Size: 497568 Color: 2

Bin 894: 6 of cap free
Amount of items: 2
Items: 
Size: 509587 Color: 4
Size: 490408 Color: 0

Bin 895: 6 of cap free
Amount of items: 2
Items: 
Size: 510713 Color: 3
Size: 489282 Color: 0

Bin 896: 6 of cap free
Amount of items: 2
Items: 
Size: 512525 Color: 0
Size: 487470 Color: 1

Bin 897: 6 of cap free
Amount of items: 2
Items: 
Size: 532684 Color: 4
Size: 467311 Color: 1

Bin 898: 6 of cap free
Amount of items: 2
Items: 
Size: 532952 Color: 1
Size: 467043 Color: 2

Bin 899: 6 of cap free
Amount of items: 2
Items: 
Size: 559602 Color: 1
Size: 440393 Color: 0

Bin 900: 6 of cap free
Amount of items: 2
Items: 
Size: 584808 Color: 4
Size: 415187 Color: 2

Bin 901: 6 of cap free
Amount of items: 2
Items: 
Size: 586926 Color: 1
Size: 413069 Color: 0

Bin 902: 6 of cap free
Amount of items: 2
Items: 
Size: 594017 Color: 0
Size: 405978 Color: 2

Bin 903: 6 of cap free
Amount of items: 2
Items: 
Size: 603702 Color: 1
Size: 396293 Color: 2

Bin 904: 6 of cap free
Amount of items: 3
Items: 
Size: 604587 Color: 2
Size: 198579 Color: 0
Size: 196829 Color: 1

Bin 905: 6 of cap free
Amount of items: 3
Items: 
Size: 605665 Color: 4
Size: 197335 Color: 0
Size: 196995 Color: 3

Bin 906: 6 of cap free
Amount of items: 2
Items: 
Size: 614980 Color: 4
Size: 385015 Color: 3

Bin 907: 6 of cap free
Amount of items: 3
Items: 
Size: 621520 Color: 3
Size: 192222 Color: 2
Size: 186253 Color: 4

Bin 908: 6 of cap free
Amount of items: 3
Items: 
Size: 644703 Color: 2
Size: 178498 Color: 4
Size: 176794 Color: 2

Bin 909: 6 of cap free
Amount of items: 2
Items: 
Size: 645279 Color: 4
Size: 354716 Color: 2

Bin 910: 6 of cap free
Amount of items: 3
Items: 
Size: 652423 Color: 3
Size: 173819 Color: 1
Size: 173753 Color: 0

Bin 911: 6 of cap free
Amount of items: 2
Items: 
Size: 652937 Color: 0
Size: 347058 Color: 4

Bin 912: 6 of cap free
Amount of items: 2
Items: 
Size: 660069 Color: 0
Size: 339926 Color: 3

Bin 913: 6 of cap free
Amount of items: 3
Items: 
Size: 684039 Color: 1
Size: 158195 Color: 0
Size: 157761 Color: 3

Bin 914: 6 of cap free
Amount of items: 2
Items: 
Size: 684506 Color: 4
Size: 315489 Color: 0

Bin 915: 6 of cap free
Amount of items: 3
Items: 
Size: 706953 Color: 0
Size: 146984 Color: 1
Size: 146058 Color: 3

Bin 916: 6 of cap free
Amount of items: 2
Items: 
Size: 709812 Color: 2
Size: 290183 Color: 0

Bin 917: 6 of cap free
Amount of items: 3
Items: 
Size: 713729 Color: 3
Size: 143151 Color: 4
Size: 143115 Color: 4

Bin 918: 6 of cap free
Amount of items: 2
Items: 
Size: 742719 Color: 0
Size: 257276 Color: 3

Bin 919: 6 of cap free
Amount of items: 2
Items: 
Size: 767184 Color: 0
Size: 232811 Color: 4

Bin 920: 6 of cap free
Amount of items: 2
Items: 
Size: 779773 Color: 0
Size: 220222 Color: 1

Bin 921: 6 of cap free
Amount of items: 3
Items: 
Size: 388286 Color: 1
Size: 341931 Color: 1
Size: 269778 Color: 4

Bin 922: 6 of cap free
Amount of items: 3
Items: 
Size: 675382 Color: 3
Size: 163278 Color: 4
Size: 161335 Color: 2

Bin 923: 6 of cap free
Amount of items: 3
Items: 
Size: 655124 Color: 2
Size: 172792 Color: 3
Size: 172079 Color: 2

Bin 924: 6 of cap free
Amount of items: 3
Items: 
Size: 725853 Color: 4
Size: 138433 Color: 3
Size: 135709 Color: 0

Bin 925: 6 of cap free
Amount of items: 2
Items: 
Size: 691530 Color: 2
Size: 308465 Color: 3

Bin 926: 6 of cap free
Amount of items: 3
Items: 
Size: 691783 Color: 3
Size: 173789 Color: 1
Size: 134423 Color: 2

Bin 927: 6 of cap free
Amount of items: 2
Items: 
Size: 762332 Color: 3
Size: 237663 Color: 0

Bin 928: 6 of cap free
Amount of items: 2
Items: 
Size: 583404 Color: 2
Size: 416591 Color: 1

Bin 929: 6 of cap free
Amount of items: 2
Items: 
Size: 683635 Color: 3
Size: 316360 Color: 2

Bin 930: 6 of cap free
Amount of items: 2
Items: 
Size: 780826 Color: 3
Size: 219169 Color: 1

Bin 931: 6 of cap free
Amount of items: 3
Items: 
Size: 660310 Color: 2
Size: 170890 Color: 4
Size: 168795 Color: 2

Bin 932: 6 of cap free
Amount of items: 2
Items: 
Size: 608943 Color: 2
Size: 391052 Color: 0

Bin 933: 6 of cap free
Amount of items: 3
Items: 
Size: 709938 Color: 2
Size: 146856 Color: 4
Size: 143201 Color: 0

Bin 934: 6 of cap free
Amount of items: 3
Items: 
Size: 570118 Color: 3
Size: 218523 Color: 0
Size: 211354 Color: 2

Bin 935: 6 of cap free
Amount of items: 2
Items: 
Size: 650645 Color: 1
Size: 349350 Color: 3

Bin 936: 6 of cap free
Amount of items: 2
Items: 
Size: 540621 Color: 0
Size: 459374 Color: 2

Bin 937: 6 of cap free
Amount of items: 3
Items: 
Size: 588745 Color: 3
Size: 213706 Color: 3
Size: 197544 Color: 1

Bin 938: 6 of cap free
Amount of items: 3
Items: 
Size: 522769 Color: 4
Size: 278165 Color: 4
Size: 199061 Color: 0

Bin 939: 6 of cap free
Amount of items: 3
Items: 
Size: 698764 Color: 1
Size: 150699 Color: 1
Size: 150532 Color: 0

Bin 940: 6 of cap free
Amount of items: 3
Items: 
Size: 370591 Color: 4
Size: 316809 Color: 2
Size: 312595 Color: 2

Bin 941: 6 of cap free
Amount of items: 2
Items: 
Size: 685032 Color: 0
Size: 314963 Color: 3

Bin 942: 6 of cap free
Amount of items: 3
Items: 
Size: 572194 Color: 2
Size: 215269 Color: 4
Size: 212532 Color: 1

Bin 943: 6 of cap free
Amount of items: 3
Items: 
Size: 426524 Color: 3
Size: 313391 Color: 0
Size: 260080 Color: 0

Bin 944: 6 of cap free
Amount of items: 3
Items: 
Size: 738448 Color: 4
Size: 140909 Color: 2
Size: 120638 Color: 1

Bin 945: 6 of cap free
Amount of items: 2
Items: 
Size: 505499 Color: 3
Size: 494496 Color: 1

Bin 946: 6 of cap free
Amount of items: 2
Items: 
Size: 719354 Color: 2
Size: 280641 Color: 4

Bin 947: 6 of cap free
Amount of items: 3
Items: 
Size: 557300 Color: 3
Size: 227113 Color: 0
Size: 215582 Color: 4

Bin 948: 6 of cap free
Amount of items: 3
Items: 
Size: 385360 Color: 3
Size: 357071 Color: 2
Size: 257564 Color: 4

Bin 949: 6 of cap free
Amount of items: 3
Items: 
Size: 433358 Color: 2
Size: 296536 Color: 0
Size: 270101 Color: 0

Bin 950: 6 of cap free
Amount of items: 2
Items: 
Size: 505737 Color: 4
Size: 494258 Color: 3

Bin 951: 6 of cap free
Amount of items: 2
Items: 
Size: 715534 Color: 3
Size: 284461 Color: 0

Bin 952: 6 of cap free
Amount of items: 3
Items: 
Size: 502013 Color: 3
Size: 251956 Color: 4
Size: 246026 Color: 3

Bin 953: 6 of cap free
Amount of items: 3
Items: 
Size: 367861 Color: 3
Size: 341695 Color: 3
Size: 290439 Color: 4

Bin 954: 6 of cap free
Amount of items: 3
Items: 
Size: 347373 Color: 4
Size: 338022 Color: 0
Size: 314600 Color: 2

Bin 955: 6 of cap free
Amount of items: 3
Items: 
Size: 338267 Color: 1
Size: 333403 Color: 1
Size: 328325 Color: 0

Bin 956: 7 of cap free
Amount of items: 3
Items: 
Size: 697218 Color: 1
Size: 151426 Color: 0
Size: 151350 Color: 3

Bin 957: 7 of cap free
Amount of items: 3
Items: 
Size: 651822 Color: 3
Size: 174182 Color: 2
Size: 173990 Color: 0

Bin 958: 7 of cap free
Amount of items: 2
Items: 
Size: 745055 Color: 2
Size: 254939 Color: 1

Bin 959: 7 of cap free
Amount of items: 3
Items: 
Size: 374956 Color: 1
Size: 322950 Color: 3
Size: 302088 Color: 3

Bin 960: 7 of cap free
Amount of items: 3
Items: 
Size: 378525 Color: 2
Size: 323775 Color: 0
Size: 297694 Color: 3

Bin 961: 7 of cap free
Amount of items: 2
Items: 
Size: 512839 Color: 0
Size: 487155 Color: 1

Bin 962: 7 of cap free
Amount of items: 2
Items: 
Size: 523975 Color: 4
Size: 476019 Color: 0

Bin 963: 7 of cap free
Amount of items: 2
Items: 
Size: 532223 Color: 1
Size: 467771 Color: 0

Bin 964: 7 of cap free
Amount of items: 2
Items: 
Size: 541494 Color: 2
Size: 458500 Color: 1

Bin 965: 7 of cap free
Amount of items: 2
Items: 
Size: 549797 Color: 2
Size: 450197 Color: 3

Bin 966: 7 of cap free
Amount of items: 2
Items: 
Size: 555507 Color: 1
Size: 444487 Color: 4

Bin 967: 7 of cap free
Amount of items: 2
Items: 
Size: 562850 Color: 4
Size: 437144 Color: 1

Bin 968: 7 of cap free
Amount of items: 3
Items: 
Size: 601990 Color: 0
Size: 199393 Color: 3
Size: 198611 Color: 2

Bin 969: 7 of cap free
Amount of items: 2
Items: 
Size: 602313 Color: 3
Size: 397681 Color: 4

Bin 970: 7 of cap free
Amount of items: 2
Items: 
Size: 621775 Color: 2
Size: 378219 Color: 0

Bin 971: 7 of cap free
Amount of items: 2
Items: 
Size: 633847 Color: 2
Size: 366147 Color: 1

Bin 972: 7 of cap free
Amount of items: 3
Items: 
Size: 636205 Color: 4
Size: 185029 Color: 4
Size: 178760 Color: 0

Bin 973: 7 of cap free
Amount of items: 3
Items: 
Size: 640982 Color: 3
Size: 180428 Color: 4
Size: 178584 Color: 4

Bin 974: 7 of cap free
Amount of items: 2
Items: 
Size: 654645 Color: 2
Size: 345349 Color: 1

Bin 975: 7 of cap free
Amount of items: 2
Items: 
Size: 662522 Color: 0
Size: 337472 Color: 1

Bin 976: 7 of cap free
Amount of items: 2
Items: 
Size: 668028 Color: 2
Size: 331966 Color: 0

Bin 977: 7 of cap free
Amount of items: 3
Items: 
Size: 668107 Color: 3
Size: 166104 Color: 2
Size: 165783 Color: 4

Bin 978: 7 of cap free
Amount of items: 2
Items: 
Size: 672401 Color: 2
Size: 327593 Color: 1

Bin 979: 7 of cap free
Amount of items: 2
Items: 
Size: 679581 Color: 1
Size: 320413 Color: 4

Bin 980: 7 of cap free
Amount of items: 2
Items: 
Size: 688725 Color: 2
Size: 311269 Color: 1

Bin 981: 7 of cap free
Amount of items: 2
Items: 
Size: 738762 Color: 0
Size: 261232 Color: 4

Bin 982: 7 of cap free
Amount of items: 2
Items: 
Size: 749586 Color: 0
Size: 250408 Color: 1

Bin 983: 7 of cap free
Amount of items: 3
Items: 
Size: 753739 Color: 0
Size: 124042 Color: 0
Size: 122213 Color: 1

Bin 984: 7 of cap free
Amount of items: 2
Items: 
Size: 791268 Color: 1
Size: 208726 Color: 3

Bin 985: 7 of cap free
Amount of items: 3
Items: 
Size: 625703 Color: 0
Size: 189395 Color: 0
Size: 184896 Color: 4

Bin 986: 7 of cap free
Amount of items: 2
Items: 
Size: 678964 Color: 4
Size: 321030 Color: 3

Bin 987: 7 of cap free
Amount of items: 2
Items: 
Size: 559885 Color: 3
Size: 440109 Color: 4

Bin 988: 7 of cap free
Amount of items: 3
Items: 
Size: 661191 Color: 1
Size: 170522 Color: 4
Size: 168281 Color: 1

Bin 989: 7 of cap free
Amount of items: 2
Items: 
Size: 649628 Color: 2
Size: 350366 Color: 0

Bin 990: 7 of cap free
Amount of items: 3
Items: 
Size: 653616 Color: 4
Size: 173659 Color: 1
Size: 172719 Color: 4

Bin 991: 7 of cap free
Amount of items: 3
Items: 
Size: 613698 Color: 1
Size: 194565 Color: 0
Size: 191731 Color: 4

Bin 992: 7 of cap free
Amount of items: 2
Items: 
Size: 659171 Color: 3
Size: 340823 Color: 1

Bin 993: 7 of cap free
Amount of items: 3
Items: 
Size: 602572 Color: 0
Size: 199137 Color: 0
Size: 198285 Color: 4

Bin 994: 7 of cap free
Amount of items: 3
Items: 
Size: 631211 Color: 1
Size: 187517 Color: 4
Size: 181266 Color: 1

Bin 995: 7 of cap free
Amount of items: 3
Items: 
Size: 659966 Color: 4
Size: 183589 Color: 2
Size: 156439 Color: 2

Bin 996: 7 of cap free
Amount of items: 3
Items: 
Size: 525887 Color: 0
Size: 245844 Color: 2
Size: 228263 Color: 0

Bin 997: 7 of cap free
Amount of items: 3
Items: 
Size: 709375 Color: 1
Size: 145317 Color: 2
Size: 145302 Color: 1

Bin 998: 7 of cap free
Amount of items: 2
Items: 
Size: 517564 Color: 3
Size: 482430 Color: 0

Bin 999: 7 of cap free
Amount of items: 3
Items: 
Size: 607812 Color: 4
Size: 196534 Color: 4
Size: 195648 Color: 0

Bin 1000: 7 of cap free
Amount of items: 2
Items: 
Size: 726460 Color: 3
Size: 273534 Color: 1

Bin 1001: 7 of cap free
Amount of items: 2
Items: 
Size: 583885 Color: 1
Size: 416109 Color: 2

Bin 1002: 7 of cap free
Amount of items: 3
Items: 
Size: 489806 Color: 3
Size: 256178 Color: 1
Size: 254010 Color: 3

Bin 1003: 7 of cap free
Amount of items: 2
Items: 
Size: 671284 Color: 2
Size: 328710 Color: 1

Bin 1004: 7 of cap free
Amount of items: 3
Items: 
Size: 741786 Color: 0
Size: 129162 Color: 2
Size: 129046 Color: 3

Bin 1005: 7 of cap free
Amount of items: 2
Items: 
Size: 796763 Color: 1
Size: 203231 Color: 2

Bin 1006: 7 of cap free
Amount of items: 2
Items: 
Size: 553966 Color: 3
Size: 446028 Color: 2

Bin 1007: 7 of cap free
Amount of items: 2
Items: 
Size: 663870 Color: 2
Size: 336124 Color: 1

Bin 1008: 7 of cap free
Amount of items: 3
Items: 
Size: 335402 Color: 2
Size: 333721 Color: 3
Size: 330871 Color: 2

Bin 1009: 7 of cap free
Amount of items: 2
Items: 
Size: 715859 Color: 0
Size: 284135 Color: 3

Bin 1010: 7 of cap free
Amount of items: 2
Items: 
Size: 709280 Color: 2
Size: 290714 Color: 1

Bin 1011: 7 of cap free
Amount of items: 2
Items: 
Size: 689091 Color: 1
Size: 310903 Color: 2

Bin 1012: 7 of cap free
Amount of items: 2
Items: 
Size: 556231 Color: 1
Size: 443763 Color: 0

Bin 1013: 7 of cap free
Amount of items: 2
Items: 
Size: 663602 Color: 3
Size: 336392 Color: 2

Bin 1014: 7 of cap free
Amount of items: 3
Items: 
Size: 370575 Color: 1
Size: 324408 Color: 2
Size: 305011 Color: 1

Bin 1015: 7 of cap free
Amount of items: 2
Items: 
Size: 615891 Color: 4
Size: 384103 Color: 2

Bin 1016: 7 of cap free
Amount of items: 2
Items: 
Size: 755858 Color: 0
Size: 244136 Color: 4

Bin 1017: 7 of cap free
Amount of items: 3
Items: 
Size: 368963 Color: 3
Size: 368201 Color: 4
Size: 262830 Color: 3

Bin 1018: 7 of cap free
Amount of items: 3
Items: 
Size: 350168 Color: 4
Size: 339330 Color: 3
Size: 310496 Color: 0

Bin 1019: 7 of cap free
Amount of items: 3
Items: 
Size: 342917 Color: 2
Size: 340218 Color: 4
Size: 316859 Color: 0

Bin 1020: 8 of cap free
Amount of items: 2
Items: 
Size: 786807 Color: 0
Size: 213186 Color: 2

Bin 1021: 8 of cap free
Amount of items: 2
Items: 
Size: 662981 Color: 1
Size: 337012 Color: 4

Bin 1022: 8 of cap free
Amount of items: 3
Items: 
Size: 385870 Color: 2
Size: 330301 Color: 4
Size: 283822 Color: 3

Bin 1023: 8 of cap free
Amount of items: 2
Items: 
Size: 510358 Color: 2
Size: 489635 Color: 3

Bin 1024: 8 of cap free
Amount of items: 2
Items: 
Size: 526583 Color: 4
Size: 473410 Color: 2

Bin 1025: 8 of cap free
Amount of items: 2
Items: 
Size: 543610 Color: 1
Size: 456383 Color: 0

Bin 1026: 8 of cap free
Amount of items: 2
Items: 
Size: 550410 Color: 3
Size: 449583 Color: 0

Bin 1027: 8 of cap free
Amount of items: 2
Items: 
Size: 553052 Color: 1
Size: 446941 Color: 3

Bin 1028: 8 of cap free
Amount of items: 2
Items: 
Size: 553809 Color: 3
Size: 446184 Color: 2

Bin 1029: 8 of cap free
Amount of items: 2
Items: 
Size: 562888 Color: 1
Size: 437105 Color: 3

Bin 1030: 8 of cap free
Amount of items: 2
Items: 
Size: 596881 Color: 3
Size: 403112 Color: 1

Bin 1031: 8 of cap free
Amount of items: 2
Items: 
Size: 616539 Color: 3
Size: 383454 Color: 1

Bin 1032: 8 of cap free
Amount of items: 3
Items: 
Size: 623606 Color: 1
Size: 192415 Color: 3
Size: 183972 Color: 2

Bin 1033: 8 of cap free
Amount of items: 2
Items: 
Size: 633897 Color: 4
Size: 366096 Color: 0

Bin 1034: 8 of cap free
Amount of items: 2
Items: 
Size: 633884 Color: 3
Size: 366109 Color: 0

Bin 1035: 8 of cap free
Amount of items: 2
Items: 
Size: 635088 Color: 0
Size: 364905 Color: 4

Bin 1036: 8 of cap free
Amount of items: 3
Items: 
Size: 638042 Color: 3
Size: 181338 Color: 0
Size: 180613 Color: 4

Bin 1037: 8 of cap free
Amount of items: 2
Items: 
Size: 638678 Color: 4
Size: 361315 Color: 1

Bin 1038: 8 of cap free
Amount of items: 3
Items: 
Size: 657420 Color: 3
Size: 171775 Color: 1
Size: 170798 Color: 2

Bin 1039: 8 of cap free
Amount of items: 2
Items: 
Size: 661395 Color: 4
Size: 338598 Color: 2

Bin 1040: 8 of cap free
Amount of items: 2
Items: 
Size: 664856 Color: 0
Size: 335137 Color: 2

Bin 1041: 8 of cap free
Amount of items: 2
Items: 
Size: 667539 Color: 2
Size: 332454 Color: 0

Bin 1042: 8 of cap free
Amount of items: 2
Items: 
Size: 670392 Color: 4
Size: 329601 Color: 3

Bin 1043: 8 of cap free
Amount of items: 3
Items: 
Size: 692940 Color: 3
Size: 153638 Color: 2
Size: 153415 Color: 2

Bin 1044: 8 of cap free
Amount of items: 3
Items: 
Size: 703478 Color: 3
Size: 148258 Color: 4
Size: 148257 Color: 4

Bin 1045: 8 of cap free
Amount of items: 2
Items: 
Size: 709980 Color: 0
Size: 290013 Color: 2

Bin 1046: 8 of cap free
Amount of items: 3
Items: 
Size: 713532 Color: 3
Size: 143541 Color: 0
Size: 142920 Color: 2

Bin 1047: 8 of cap free
Amount of items: 2
Items: 
Size: 714868 Color: 1
Size: 285125 Color: 2

Bin 1048: 8 of cap free
Amount of items: 2
Items: 
Size: 725155 Color: 2
Size: 274838 Color: 4

Bin 1049: 8 of cap free
Amount of items: 2
Items: 
Size: 725401 Color: 4
Size: 274592 Color: 0

Bin 1050: 8 of cap free
Amount of items: 2
Items: 
Size: 731030 Color: 2
Size: 268963 Color: 4

Bin 1051: 8 of cap free
Amount of items: 3
Items: 
Size: 732804 Color: 3
Size: 133790 Color: 2
Size: 133399 Color: 0

Bin 1052: 8 of cap free
Amount of items: 2
Items: 
Size: 744893 Color: 2
Size: 255100 Color: 1

Bin 1053: 8 of cap free
Amount of items: 2
Items: 
Size: 749443 Color: 4
Size: 250550 Color: 3

Bin 1054: 8 of cap free
Amount of items: 2
Items: 
Size: 771177 Color: 3
Size: 228816 Color: 4

Bin 1055: 8 of cap free
Amount of items: 2
Items: 
Size: 772584 Color: 4
Size: 227409 Color: 0

Bin 1056: 8 of cap free
Amount of items: 2
Items: 
Size: 757241 Color: 4
Size: 242752 Color: 1

Bin 1057: 8 of cap free
Amount of items: 2
Items: 
Size: 646330 Color: 3
Size: 353663 Color: 4

Bin 1058: 8 of cap free
Amount of items: 3
Items: 
Size: 657034 Color: 2
Size: 176151 Color: 0
Size: 166808 Color: 3

Bin 1059: 8 of cap free
Amount of items: 2
Items: 
Size: 611804 Color: 0
Size: 388189 Color: 2

Bin 1060: 8 of cap free
Amount of items: 3
Items: 
Size: 690325 Color: 0
Size: 155078 Color: 3
Size: 154590 Color: 2

Bin 1061: 8 of cap free
Amount of items: 3
Items: 
Size: 558217 Color: 1
Size: 226948 Color: 0
Size: 214828 Color: 2

Bin 1062: 8 of cap free
Amount of items: 2
Items: 
Size: 683891 Color: 3
Size: 316102 Color: 4

Bin 1063: 8 of cap free
Amount of items: 2
Items: 
Size: 778704 Color: 2
Size: 221289 Color: 1

Bin 1064: 8 of cap free
Amount of items: 2
Items: 
Size: 782558 Color: 4
Size: 217435 Color: 0

Bin 1065: 8 of cap free
Amount of items: 2
Items: 
Size: 569541 Color: 3
Size: 430452 Color: 1

Bin 1066: 8 of cap free
Amount of items: 2
Items: 
Size: 671235 Color: 3
Size: 328758 Color: 1

Bin 1067: 8 of cap free
Amount of items: 2
Items: 
Size: 567420 Color: 4
Size: 432573 Color: 0

Bin 1068: 8 of cap free
Amount of items: 3
Items: 
Size: 708308 Color: 1
Size: 175030 Color: 1
Size: 116655 Color: 4

Bin 1069: 8 of cap free
Amount of items: 3
Items: 
Size: 617699 Color: 4
Size: 193383 Color: 0
Size: 188911 Color: 4

Bin 1070: 8 of cap free
Amount of items: 2
Items: 
Size: 704033 Color: 2
Size: 295960 Color: 3

Bin 1071: 8 of cap free
Amount of items: 3
Items: 
Size: 711593 Color: 2
Size: 144230 Color: 3
Size: 144170 Color: 1

Bin 1072: 8 of cap free
Amount of items: 3
Items: 
Size: 639464 Color: 2
Size: 182798 Color: 2
Size: 177731 Color: 4

Bin 1073: 8 of cap free
Amount of items: 2
Items: 
Size: 746885 Color: 0
Size: 253108 Color: 1

Bin 1074: 8 of cap free
Amount of items: 2
Items: 
Size: 517137 Color: 2
Size: 482856 Color: 4

Bin 1075: 8 of cap free
Amount of items: 3
Items: 
Size: 611287 Color: 2
Size: 195835 Color: 4
Size: 192871 Color: 1

Bin 1076: 8 of cap free
Amount of items: 3
Items: 
Size: 541648 Color: 4
Size: 229688 Color: 4
Size: 228657 Color: 1

Bin 1077: 8 of cap free
Amount of items: 3
Items: 
Size: 374729 Color: 0
Size: 321335 Color: 2
Size: 303929 Color: 0

Bin 1078: 8 of cap free
Amount of items: 3
Items: 
Size: 524971 Color: 0
Size: 246457 Color: 0
Size: 228565 Color: 4

Bin 1079: 8 of cap free
Amount of items: 2
Items: 
Size: 569009 Color: 3
Size: 430984 Color: 4

Bin 1080: 8 of cap free
Amount of items: 2
Items: 
Size: 763824 Color: 1
Size: 236169 Color: 0

Bin 1081: 8 of cap free
Amount of items: 2
Items: 
Size: 745553 Color: 3
Size: 254440 Color: 0

Bin 1082: 8 of cap free
Amount of items: 2
Items: 
Size: 659254 Color: 3
Size: 340739 Color: 1

Bin 1083: 8 of cap free
Amount of items: 2
Items: 
Size: 645915 Color: 0
Size: 354078 Color: 3

Bin 1084: 8 of cap free
Amount of items: 2
Items: 
Size: 502682 Color: 1
Size: 497311 Color: 2

Bin 1085: 8 of cap free
Amount of items: 2
Items: 
Size: 753320 Color: 4
Size: 246673 Color: 2

Bin 1086: 8 of cap free
Amount of items: 2
Items: 
Size: 709195 Color: 4
Size: 290798 Color: 2

Bin 1087: 8 of cap free
Amount of items: 2
Items: 
Size: 787222 Color: 1
Size: 212771 Color: 3

Bin 1088: 8 of cap free
Amount of items: 2
Items: 
Size: 720218 Color: 2
Size: 279775 Color: 3

Bin 1089: 8 of cap free
Amount of items: 2
Items: 
Size: 600023 Color: 3
Size: 399970 Color: 0

Bin 1090: 8 of cap free
Amount of items: 2
Items: 
Size: 630860 Color: 0
Size: 369133 Color: 3

Bin 1091: 9 of cap free
Amount of items: 2
Items: 
Size: 589801 Color: 4
Size: 410191 Color: 2

Bin 1092: 9 of cap free
Amount of items: 3
Items: 
Size: 686077 Color: 3
Size: 157818 Color: 1
Size: 156097 Color: 1

Bin 1093: 9 of cap free
Amount of items: 3
Items: 
Size: 374534 Color: 4
Size: 323461 Color: 3
Size: 301997 Color: 3

Bin 1094: 9 of cap free
Amount of items: 3
Items: 
Size: 380291 Color: 2
Size: 325080 Color: 2
Size: 294621 Color: 3

Bin 1095: 9 of cap free
Amount of items: 3
Items: 
Size: 478779 Color: 2
Size: 267684 Color: 0
Size: 253529 Color: 4

Bin 1096: 9 of cap free
Amount of items: 2
Items: 
Size: 510929 Color: 1
Size: 489063 Color: 4

Bin 1097: 9 of cap free
Amount of items: 2
Items: 
Size: 526414 Color: 2
Size: 473578 Color: 1

Bin 1098: 9 of cap free
Amount of items: 2
Items: 
Size: 576427 Color: 2
Size: 423565 Color: 4

Bin 1099: 9 of cap free
Amount of items: 2
Items: 
Size: 576882 Color: 0
Size: 423110 Color: 1

Bin 1100: 9 of cap free
Amount of items: 2
Items: 
Size: 597675 Color: 3
Size: 402317 Color: 4

Bin 1101: 9 of cap free
Amount of items: 2
Items: 
Size: 610399 Color: 1
Size: 389593 Color: 4

Bin 1102: 9 of cap free
Amount of items: 2
Items: 
Size: 623462 Color: 4
Size: 376530 Color: 1

Bin 1103: 9 of cap free
Amount of items: 3
Items: 
Size: 623606 Color: 3
Size: 190336 Color: 4
Size: 186050 Color: 1

Bin 1104: 9 of cap free
Amount of items: 3
Items: 
Size: 666506 Color: 3
Size: 166793 Color: 1
Size: 166693 Color: 1

Bin 1105: 9 of cap free
Amount of items: 2
Items: 
Size: 667220 Color: 4
Size: 332772 Color: 1

Bin 1106: 9 of cap free
Amount of items: 2
Items: 
Size: 685452 Color: 4
Size: 314540 Color: 0

Bin 1107: 9 of cap free
Amount of items: 2
Items: 
Size: 704144 Color: 0
Size: 295848 Color: 4

Bin 1108: 9 of cap free
Amount of items: 2
Items: 
Size: 708931 Color: 2
Size: 291061 Color: 4

Bin 1109: 9 of cap free
Amount of items: 2
Items: 
Size: 718045 Color: 0
Size: 281947 Color: 1

Bin 1110: 9 of cap free
Amount of items: 2
Items: 
Size: 718605 Color: 2
Size: 281387 Color: 1

Bin 1111: 9 of cap free
Amount of items: 2
Items: 
Size: 798712 Color: 3
Size: 201280 Color: 2

Bin 1112: 9 of cap free
Amount of items: 3
Items: 
Size: 628055 Color: 0
Size: 189376 Color: 4
Size: 182561 Color: 0

Bin 1113: 9 of cap free
Amount of items: 2
Items: 
Size: 610110 Color: 0
Size: 389882 Color: 2

Bin 1114: 9 of cap free
Amount of items: 3
Items: 
Size: 517395 Color: 0
Size: 253685 Color: 1
Size: 228912 Color: 1

Bin 1115: 9 of cap free
Amount of items: 3
Items: 
Size: 624429 Color: 1
Size: 191010 Color: 0
Size: 184553 Color: 0

Bin 1116: 9 of cap free
Amount of items: 2
Items: 
Size: 693124 Color: 3
Size: 306868 Color: 2

Bin 1117: 9 of cap free
Amount of items: 2
Items: 
Size: 507767 Color: 1
Size: 492225 Color: 2

Bin 1118: 9 of cap free
Amount of items: 2
Items: 
Size: 579228 Color: 2
Size: 420764 Color: 0

Bin 1119: 9 of cap free
Amount of items: 3
Items: 
Size: 626807 Color: 2
Size: 189506 Color: 4
Size: 183679 Color: 0

Bin 1120: 9 of cap free
Amount of items: 3
Items: 
Size: 689942 Color: 3
Size: 155173 Color: 0
Size: 154877 Color: 2

Bin 1121: 9 of cap free
Amount of items: 3
Items: 
Size: 385412 Color: 0
Size: 315202 Color: 1
Size: 299378 Color: 3

Bin 1122: 9 of cap free
Amount of items: 2
Items: 
Size: 610029 Color: 3
Size: 389963 Color: 4

Bin 1123: 9 of cap free
Amount of items: 2
Items: 
Size: 764946 Color: 0
Size: 235046 Color: 1

Bin 1124: 9 of cap free
Amount of items: 3
Items: 
Size: 671323 Color: 4
Size: 164636 Color: 0
Size: 164033 Color: 4

Bin 1125: 9 of cap free
Amount of items: 2
Items: 
Size: 519297 Color: 4
Size: 480695 Color: 2

Bin 1126: 9 of cap free
Amount of items: 3
Items: 
Size: 487003 Color: 2
Size: 377839 Color: 3
Size: 135150 Color: 2

Bin 1127: 9 of cap free
Amount of items: 3
Items: 
Size: 678838 Color: 1
Size: 164530 Color: 2
Size: 156624 Color: 2

Bin 1128: 9 of cap free
Amount of items: 3
Items: 
Size: 648289 Color: 1
Size: 177087 Color: 3
Size: 174616 Color: 3

Bin 1129: 9 of cap free
Amount of items: 3
Items: 
Size: 556242 Color: 0
Size: 223520 Color: 4
Size: 220230 Color: 4

Bin 1130: 9 of cap free
Amount of items: 3
Items: 
Size: 683005 Color: 4
Size: 158621 Color: 1
Size: 158366 Color: 1

Bin 1131: 9 of cap free
Amount of items: 2
Items: 
Size: 690869 Color: 1
Size: 309123 Color: 3

Bin 1132: 9 of cap free
Amount of items: 2
Items: 
Size: 504238 Color: 1
Size: 495754 Color: 4

Bin 1133: 9 of cap free
Amount of items: 2
Items: 
Size: 766956 Color: 2
Size: 233036 Color: 1

Bin 1134: 9 of cap free
Amount of items: 2
Items: 
Size: 758937 Color: 2
Size: 241055 Color: 1

Bin 1135: 9 of cap free
Amount of items: 3
Items: 
Size: 375952 Color: 3
Size: 345720 Color: 4
Size: 278320 Color: 0

Bin 1136: 9 of cap free
Amount of items: 3
Items: 
Size: 334675 Color: 4
Size: 333084 Color: 2
Size: 332233 Color: 3

Bin 1137: 10 of cap free
Amount of items: 3
Items: 
Size: 635958 Color: 0
Size: 184089 Color: 3
Size: 179944 Color: 4

Bin 1138: 10 of cap free
Amount of items: 3
Items: 
Size: 394471 Color: 2
Size: 343501 Color: 0
Size: 262019 Color: 1

Bin 1139: 10 of cap free
Amount of items: 2
Items: 
Size: 521120 Color: 2
Size: 478871 Color: 3

Bin 1140: 10 of cap free
Amount of items: 2
Items: 
Size: 534429 Color: 3
Size: 465562 Color: 4

Bin 1141: 10 of cap free
Amount of items: 2
Items: 
Size: 537917 Color: 4
Size: 462074 Color: 2

Bin 1142: 10 of cap free
Amount of items: 2
Items: 
Size: 560827 Color: 4
Size: 439164 Color: 0

Bin 1143: 10 of cap free
Amount of items: 2
Items: 
Size: 582719 Color: 0
Size: 417272 Color: 2

Bin 1144: 10 of cap free
Amount of items: 2
Items: 
Size: 598500 Color: 2
Size: 401491 Color: 1

Bin 1145: 10 of cap free
Amount of items: 2
Items: 
Size: 611460 Color: 1
Size: 388531 Color: 2

Bin 1146: 10 of cap free
Amount of items: 2
Items: 
Size: 638921 Color: 1
Size: 361070 Color: 2

Bin 1147: 10 of cap free
Amount of items: 2
Items: 
Size: 641966 Color: 1
Size: 358025 Color: 2

Bin 1148: 10 of cap free
Amount of items: 2
Items: 
Size: 645461 Color: 0
Size: 354530 Color: 3

Bin 1149: 10 of cap free
Amount of items: 3
Items: 
Size: 650435 Color: 3
Size: 175247 Color: 2
Size: 174309 Color: 2

Bin 1150: 10 of cap free
Amount of items: 2
Items: 
Size: 668528 Color: 4
Size: 331463 Color: 2

Bin 1151: 10 of cap free
Amount of items: 2
Items: 
Size: 694282 Color: 0
Size: 305709 Color: 2

Bin 1152: 10 of cap free
Amount of items: 2
Items: 
Size: 695515 Color: 1
Size: 304476 Color: 4

Bin 1153: 10 of cap free
Amount of items: 2
Items: 
Size: 740884 Color: 4
Size: 259107 Color: 0

Bin 1154: 10 of cap free
Amount of items: 2
Items: 
Size: 746871 Color: 1
Size: 253120 Color: 3

Bin 1155: 10 of cap free
Amount of items: 3
Items: 
Size: 430234 Color: 0
Size: 296383 Color: 1
Size: 273374 Color: 1

Bin 1156: 10 of cap free
Amount of items: 2
Items: 
Size: 668004 Color: 4
Size: 331987 Color: 2

Bin 1157: 10 of cap free
Amount of items: 3
Items: 
Size: 704317 Color: 0
Size: 148111 Color: 4
Size: 147563 Color: 0

Bin 1158: 10 of cap free
Amount of items: 3
Items: 
Size: 695205 Color: 2
Size: 157759 Color: 4
Size: 147027 Color: 4

Bin 1159: 10 of cap free
Amount of items: 3
Items: 
Size: 376462 Color: 4
Size: 369725 Color: 0
Size: 253804 Color: 3

Bin 1160: 10 of cap free
Amount of items: 2
Items: 
Size: 751740 Color: 4
Size: 248251 Color: 2

Bin 1161: 10 of cap free
Amount of items: 3
Items: 
Size: 478122 Color: 3
Size: 268303 Color: 1
Size: 253566 Color: 4

Bin 1162: 10 of cap free
Amount of items: 2
Items: 
Size: 708649 Color: 1
Size: 291342 Color: 3

Bin 1163: 10 of cap free
Amount of items: 2
Items: 
Size: 729510 Color: 2
Size: 270481 Color: 1

Bin 1164: 10 of cap free
Amount of items: 3
Items: 
Size: 525021 Color: 0
Size: 248527 Color: 4
Size: 226443 Color: 4

Bin 1165: 10 of cap free
Amount of items: 2
Items: 
Size: 600550 Color: 0
Size: 399441 Color: 1

Bin 1166: 10 of cap free
Amount of items: 2
Items: 
Size: 511161 Color: 2
Size: 488830 Color: 3

Bin 1167: 10 of cap free
Amount of items: 2
Items: 
Size: 547833 Color: 4
Size: 452158 Color: 3

Bin 1168: 10 of cap free
Amount of items: 3
Items: 
Size: 607784 Color: 1
Size: 196335 Color: 3
Size: 195872 Color: 0

Bin 1169: 10 of cap free
Amount of items: 3
Items: 
Size: 611629 Color: 2
Size: 194360 Color: 1
Size: 194002 Color: 3

Bin 1170: 10 of cap free
Amount of items: 2
Items: 
Size: 648628 Color: 3
Size: 351363 Color: 2

Bin 1171: 10 of cap free
Amount of items: 2
Items: 
Size: 530835 Color: 3
Size: 469156 Color: 2

Bin 1172: 10 of cap free
Amount of items: 2
Items: 
Size: 736065 Color: 0
Size: 263926 Color: 4

Bin 1173: 10 of cap free
Amount of items: 3
Items: 
Size: 439774 Color: 1
Size: 284412 Color: 0
Size: 275805 Color: 2

Bin 1174: 10 of cap free
Amount of items: 2
Items: 
Size: 544462 Color: 3
Size: 455529 Color: 1

Bin 1175: 10 of cap free
Amount of items: 2
Items: 
Size: 628186 Color: 4
Size: 371805 Color: 3

Bin 1176: 10 of cap free
Amount of items: 3
Items: 
Size: 364851 Color: 1
Size: 350598 Color: 1
Size: 284542 Color: 3

Bin 1177: 10 of cap free
Amount of items: 3
Items: 
Size: 338547 Color: 3
Size: 333443 Color: 1
Size: 328001 Color: 0

Bin 1178: 11 of cap free
Amount of items: 2
Items: 
Size: 611841 Color: 4
Size: 388149 Color: 3

Bin 1179: 11 of cap free
Amount of items: 3
Items: 
Size: 376532 Color: 4
Size: 321386 Color: 4
Size: 302072 Color: 3

Bin 1180: 11 of cap free
Amount of items: 3
Items: 
Size: 388216 Color: 3
Size: 318794 Color: 1
Size: 292980 Color: 3

Bin 1181: 11 of cap free
Amount of items: 2
Items: 
Size: 506463 Color: 0
Size: 493527 Color: 3

Bin 1182: 11 of cap free
Amount of items: 2
Items: 
Size: 517343 Color: 2
Size: 482647 Color: 4

Bin 1183: 11 of cap free
Amount of items: 2
Items: 
Size: 537838 Color: 2
Size: 462152 Color: 3

Bin 1184: 11 of cap free
Amount of items: 2
Items: 
Size: 569098 Color: 1
Size: 430892 Color: 0

Bin 1185: 11 of cap free
Amount of items: 2
Items: 
Size: 575024 Color: 2
Size: 424966 Color: 3

Bin 1186: 11 of cap free
Amount of items: 3
Items: 
Size: 604252 Color: 0
Size: 198527 Color: 3
Size: 197211 Color: 4

Bin 1187: 11 of cap free
Amount of items: 2
Items: 
Size: 618417 Color: 4
Size: 381573 Color: 1

Bin 1188: 11 of cap free
Amount of items: 2
Items: 
Size: 657754 Color: 1
Size: 342236 Color: 4

Bin 1189: 11 of cap free
Amount of items: 2
Items: 
Size: 677113 Color: 0
Size: 322877 Color: 3

Bin 1190: 11 of cap free
Amount of items: 3
Items: 
Size: 678594 Color: 3
Size: 160749 Color: 4
Size: 160647 Color: 4

Bin 1191: 11 of cap free
Amount of items: 2
Items: 
Size: 745801 Color: 1
Size: 254189 Color: 3

Bin 1192: 11 of cap free
Amount of items: 2
Items: 
Size: 748884 Color: 0
Size: 251106 Color: 1

Bin 1193: 11 of cap free
Amount of items: 2
Items: 
Size: 780012 Color: 1
Size: 219978 Color: 0

Bin 1194: 11 of cap free
Amount of items: 2
Items: 
Size: 783873 Color: 4
Size: 216117 Color: 1

Bin 1195: 11 of cap free
Amount of items: 2
Items: 
Size: 788922 Color: 1
Size: 211068 Color: 0

Bin 1196: 11 of cap free
Amount of items: 2
Items: 
Size: 791757 Color: 2
Size: 208233 Color: 3

Bin 1197: 11 of cap free
Amount of items: 2
Items: 
Size: 799785 Color: 0
Size: 200205 Color: 1

Bin 1198: 11 of cap free
Amount of items: 3
Items: 
Size: 629535 Color: 0
Size: 189256 Color: 3
Size: 181199 Color: 2

Bin 1199: 11 of cap free
Amount of items: 3
Items: 
Size: 751159 Color: 3
Size: 126083 Color: 2
Size: 122748 Color: 4

Bin 1200: 11 of cap free
Amount of items: 3
Items: 
Size: 563159 Color: 0
Size: 219860 Color: 3
Size: 216971 Color: 2

Bin 1201: 11 of cap free
Amount of items: 2
Items: 
Size: 616803 Color: 1
Size: 383187 Color: 4

Bin 1202: 11 of cap free
Amount of items: 3
Items: 
Size: 708585 Color: 2
Size: 146467 Color: 2
Size: 144938 Color: 1

Bin 1203: 11 of cap free
Amount of items: 2
Items: 
Size: 615523 Color: 0
Size: 384467 Color: 3

Bin 1204: 11 of cap free
Amount of items: 2
Items: 
Size: 710844 Color: 3
Size: 289146 Color: 0

Bin 1205: 11 of cap free
Amount of items: 2
Items: 
Size: 798841 Color: 2
Size: 201149 Color: 0

Bin 1206: 11 of cap free
Amount of items: 2
Items: 
Size: 694551 Color: 4
Size: 305439 Color: 2

Bin 1207: 11 of cap free
Amount of items: 3
Items: 
Size: 427987 Color: 3
Size: 311928 Color: 4
Size: 260075 Color: 2

Bin 1208: 11 of cap free
Amount of items: 2
Items: 
Size: 616225 Color: 3
Size: 383765 Color: 0

Bin 1209: 11 of cap free
Amount of items: 3
Items: 
Size: 610824 Color: 3
Size: 196020 Color: 4
Size: 193146 Color: 1

Bin 1210: 11 of cap free
Amount of items: 3
Items: 
Size: 681210 Color: 4
Size: 192999 Color: 0
Size: 125781 Color: 2

Bin 1211: 11 of cap free
Amount of items: 2
Items: 
Size: 612660 Color: 0
Size: 387330 Color: 3

Bin 1212: 11 of cap free
Amount of items: 2
Items: 
Size: 693916 Color: 1
Size: 306074 Color: 2

Bin 1213: 11 of cap free
Amount of items: 2
Items: 
Size: 617966 Color: 4
Size: 382024 Color: 2

Bin 1214: 11 of cap free
Amount of items: 2
Items: 
Size: 705816 Color: 0
Size: 294174 Color: 2

Bin 1215: 11 of cap free
Amount of items: 2
Items: 
Size: 592936 Color: 2
Size: 407054 Color: 1

Bin 1216: 11 of cap free
Amount of items: 3
Items: 
Size: 417326 Color: 3
Size: 327406 Color: 0
Size: 255258 Color: 4

Bin 1217: 11 of cap free
Amount of items: 3
Items: 
Size: 374382 Color: 0
Size: 340897 Color: 3
Size: 284711 Color: 3

Bin 1218: 11 of cap free
Amount of items: 2
Items: 
Size: 754046 Color: 0
Size: 245944 Color: 2

Bin 1219: 11 of cap free
Amount of items: 3
Items: 
Size: 368316 Color: 4
Size: 354133 Color: 0
Size: 277541 Color: 4

Bin 1220: 12 of cap free
Amount of items: 3
Items: 
Size: 378061 Color: 2
Size: 332564 Color: 0
Size: 289364 Color: 3

Bin 1221: 12 of cap free
Amount of items: 3
Items: 
Size: 388530 Color: 1
Size: 316630 Color: 2
Size: 294829 Color: 3

Bin 1222: 12 of cap free
Amount of items: 2
Items: 
Size: 505529 Color: 4
Size: 494460 Color: 1

Bin 1223: 12 of cap free
Amount of items: 2
Items: 
Size: 523226 Color: 0
Size: 476763 Color: 1

Bin 1224: 12 of cap free
Amount of items: 2
Items: 
Size: 529364 Color: 3
Size: 470625 Color: 2

Bin 1225: 12 of cap free
Amount of items: 2
Items: 
Size: 565602 Color: 1
Size: 434387 Color: 0

Bin 1226: 12 of cap free
Amount of items: 2
Items: 
Size: 568663 Color: 1
Size: 431326 Color: 4

Bin 1227: 12 of cap free
Amount of items: 3
Items: 
Size: 573417 Color: 1
Size: 215043 Color: 0
Size: 211529 Color: 2

Bin 1228: 12 of cap free
Amount of items: 2
Items: 
Size: 619686 Color: 4
Size: 380303 Color: 2

Bin 1229: 12 of cap free
Amount of items: 3
Items: 
Size: 626162 Color: 3
Size: 189666 Color: 4
Size: 184161 Color: 2

Bin 1230: 12 of cap free
Amount of items: 2
Items: 
Size: 630939 Color: 3
Size: 369050 Color: 1

Bin 1231: 12 of cap free
Amount of items: 2
Items: 
Size: 692623 Color: 4
Size: 307366 Color: 2

Bin 1232: 12 of cap free
Amount of items: 3
Items: 
Size: 721446 Color: 3
Size: 140017 Color: 0
Size: 138526 Color: 1

Bin 1233: 12 of cap free
Amount of items: 2
Items: 
Size: 747024 Color: 0
Size: 252965 Color: 4

Bin 1234: 12 of cap free
Amount of items: 2
Items: 
Size: 774167 Color: 1
Size: 225822 Color: 3

Bin 1235: 12 of cap free
Amount of items: 2
Items: 
Size: 660270 Color: 3
Size: 339719 Color: 0

Bin 1236: 12 of cap free
Amount of items: 2
Items: 
Size: 561202 Color: 0
Size: 438787 Color: 2

Bin 1237: 12 of cap free
Amount of items: 3
Items: 
Size: 717082 Color: 3
Size: 141604 Color: 3
Size: 141303 Color: 1

Bin 1238: 12 of cap free
Amount of items: 3
Items: 
Size: 426942 Color: 4
Size: 316893 Color: 1
Size: 256154 Color: 0

Bin 1239: 12 of cap free
Amount of items: 2
Items: 
Size: 633499 Color: 0
Size: 366490 Color: 2

Bin 1240: 12 of cap free
Amount of items: 3
Items: 
Size: 430180 Color: 1
Size: 293248 Color: 2
Size: 276561 Color: 0

Bin 1241: 12 of cap free
Amount of items: 2
Items: 
Size: 782167 Color: 3
Size: 217822 Color: 4

Bin 1242: 12 of cap free
Amount of items: 2
Items: 
Size: 646160 Color: 1
Size: 353829 Color: 3

Bin 1243: 12 of cap free
Amount of items: 2
Items: 
Size: 555973 Color: 0
Size: 444016 Color: 2

Bin 1244: 12 of cap free
Amount of items: 2
Items: 
Size: 569471 Color: 2
Size: 430518 Color: 4

Bin 1245: 12 of cap free
Amount of items: 3
Items: 
Size: 432736 Color: 1
Size: 293724 Color: 2
Size: 273529 Color: 2

Bin 1246: 12 of cap free
Amount of items: 2
Items: 
Size: 514994 Color: 2
Size: 484995 Color: 0

Bin 1247: 12 of cap free
Amount of items: 2
Items: 
Size: 573635 Color: 4
Size: 426354 Color: 3

Bin 1248: 13 of cap free
Amount of items: 3
Items: 
Size: 384465 Color: 1
Size: 339572 Color: 2
Size: 275951 Color: 0

Bin 1249: 13 of cap free
Amount of items: 2
Items: 
Size: 525260 Color: 4
Size: 474728 Color: 0

Bin 1250: 13 of cap free
Amount of items: 2
Items: 
Size: 525299 Color: 4
Size: 474689 Color: 0

Bin 1251: 13 of cap free
Amount of items: 2
Items: 
Size: 523494 Color: 0
Size: 476494 Color: 4

Bin 1252: 13 of cap free
Amount of items: 2
Items: 
Size: 532629 Color: 0
Size: 467359 Color: 2

Bin 1253: 13 of cap free
Amount of items: 2
Items: 
Size: 534186 Color: 3
Size: 465802 Color: 2

Bin 1254: 13 of cap free
Amount of items: 2
Items: 
Size: 534610 Color: 2
Size: 465378 Color: 1

Bin 1255: 13 of cap free
Amount of items: 2
Items: 
Size: 542562 Color: 2
Size: 457426 Color: 1

Bin 1256: 13 of cap free
Amount of items: 2
Items: 
Size: 572037 Color: 4
Size: 427951 Color: 3

Bin 1257: 13 of cap free
Amount of items: 2
Items: 
Size: 576607 Color: 0
Size: 423381 Color: 3

Bin 1258: 13 of cap free
Amount of items: 2
Items: 
Size: 580711 Color: 1
Size: 419277 Color: 2

Bin 1259: 13 of cap free
Amount of items: 2
Items: 
Size: 600381 Color: 1
Size: 399607 Color: 3

Bin 1260: 13 of cap free
Amount of items: 2
Items: 
Size: 607554 Color: 2
Size: 392434 Color: 4

Bin 1261: 13 of cap free
Amount of items: 3
Items: 
Size: 613466 Color: 0
Size: 194832 Color: 1
Size: 191690 Color: 1

Bin 1262: 13 of cap free
Amount of items: 3
Items: 
Size: 616304 Color: 0
Size: 192078 Color: 4
Size: 191606 Color: 1

Bin 1263: 13 of cap free
Amount of items: 3
Items: 
Size: 618752 Color: 4
Size: 192466 Color: 0
Size: 188770 Color: 2

Bin 1264: 13 of cap free
Amount of items: 2
Items: 
Size: 646707 Color: 2
Size: 353281 Color: 4

Bin 1265: 13 of cap free
Amount of items: 2
Items: 
Size: 656843 Color: 2
Size: 343145 Color: 0

Bin 1266: 13 of cap free
Amount of items: 3
Items: 
Size: 663155 Color: 2
Size: 169809 Color: 3
Size: 167024 Color: 1

Bin 1267: 13 of cap free
Amount of items: 2
Items: 
Size: 680102 Color: 2
Size: 319886 Color: 0

Bin 1268: 13 of cap free
Amount of items: 2
Items: 
Size: 682801 Color: 4
Size: 317187 Color: 2

Bin 1269: 13 of cap free
Amount of items: 2
Items: 
Size: 693242 Color: 0
Size: 306746 Color: 4

Bin 1270: 13 of cap free
Amount of items: 2
Items: 
Size: 711310 Color: 3
Size: 288678 Color: 2

Bin 1271: 13 of cap free
Amount of items: 2
Items: 
Size: 716406 Color: 0
Size: 283582 Color: 1

Bin 1272: 13 of cap free
Amount of items: 2
Items: 
Size: 754878 Color: 4
Size: 245110 Color: 0

Bin 1273: 13 of cap free
Amount of items: 2
Items: 
Size: 760197 Color: 3
Size: 239791 Color: 0

Bin 1274: 13 of cap free
Amount of items: 2
Items: 
Size: 771145 Color: 2
Size: 228843 Color: 1

Bin 1275: 13 of cap free
Amount of items: 2
Items: 
Size: 779086 Color: 0
Size: 220902 Color: 1

Bin 1276: 13 of cap free
Amount of items: 2
Items: 
Size: 782200 Color: 0
Size: 217788 Color: 1

Bin 1277: 13 of cap free
Amount of items: 2
Items: 
Size: 787798 Color: 1
Size: 212190 Color: 3

Bin 1278: 13 of cap free
Amount of items: 2
Items: 
Size: 788165 Color: 3
Size: 211823 Color: 0

Bin 1279: 13 of cap free
Amount of items: 3
Items: 
Size: 637009 Color: 2
Size: 181662 Color: 3
Size: 181317 Color: 2

Bin 1280: 13 of cap free
Amount of items: 2
Items: 
Size: 762820 Color: 0
Size: 237168 Color: 2

Bin 1281: 13 of cap free
Amount of items: 3
Items: 
Size: 492547 Color: 3
Size: 255398 Color: 0
Size: 252043 Color: 3

Bin 1282: 13 of cap free
Amount of items: 3
Items: 
Size: 694582 Color: 4
Size: 163476 Color: 2
Size: 141930 Color: 3

Bin 1283: 13 of cap free
Amount of items: 3
Items: 
Size: 738484 Color: 1
Size: 132060 Color: 2
Size: 129444 Color: 0

Bin 1284: 13 of cap free
Amount of items: 2
Items: 
Size: 666829 Color: 0
Size: 333159 Color: 3

Bin 1285: 13 of cap free
Amount of items: 3
Items: 
Size: 578135 Color: 3
Size: 211708 Color: 2
Size: 210145 Color: 2

Bin 1286: 13 of cap free
Amount of items: 3
Items: 
Size: 643262 Color: 0
Size: 180202 Color: 3
Size: 176524 Color: 1

Bin 1287: 13 of cap free
Amount of items: 2
Items: 
Size: 751823 Color: 0
Size: 248165 Color: 3

Bin 1288: 13 of cap free
Amount of items: 2
Items: 
Size: 692785 Color: 3
Size: 307203 Color: 2

Bin 1289: 13 of cap free
Amount of items: 2
Items: 
Size: 605968 Color: 4
Size: 394020 Color: 0

Bin 1290: 13 of cap free
Amount of items: 2
Items: 
Size: 509830 Color: 1
Size: 490158 Color: 4

Bin 1291: 13 of cap free
Amount of items: 3
Items: 
Size: 439635 Color: 3
Size: 297671 Color: 2
Size: 262682 Color: 2

Bin 1292: 13 of cap free
Amount of items: 2
Items: 
Size: 751336 Color: 2
Size: 248652 Color: 1

Bin 1293: 13 of cap free
Amount of items: 3
Items: 
Size: 665205 Color: 2
Size: 168330 Color: 2
Size: 166453 Color: 4

Bin 1294: 13 of cap free
Amount of items: 3
Items: 
Size: 647684 Color: 2
Size: 177934 Color: 0
Size: 174370 Color: 4

Bin 1295: 13 of cap free
Amount of items: 2
Items: 
Size: 777783 Color: 0
Size: 222205 Color: 2

Bin 1296: 13 of cap free
Amount of items: 2
Items: 
Size: 734438 Color: 1
Size: 265550 Color: 0

Bin 1297: 13 of cap free
Amount of items: 3
Items: 
Size: 726606 Color: 2
Size: 137673 Color: 2
Size: 135709 Color: 0

Bin 1298: 13 of cap free
Amount of items: 2
Items: 
Size: 763854 Color: 2
Size: 236134 Color: 1

Bin 1299: 13 of cap free
Amount of items: 3
Items: 
Size: 413923 Color: 1
Size: 318280 Color: 2
Size: 267785 Color: 2

Bin 1300: 13 of cap free
Amount of items: 2
Items: 
Size: 637047 Color: 0
Size: 362941 Color: 4

Bin 1301: 13 of cap free
Amount of items: 3
Items: 
Size: 375517 Color: 1
Size: 351780 Color: 4
Size: 272691 Color: 2

Bin 1302: 13 of cap free
Amount of items: 2
Items: 
Size: 590065 Color: 4
Size: 409923 Color: 0

Bin 1303: 13 of cap free
Amount of items: 2
Items: 
Size: 772702 Color: 0
Size: 227286 Color: 1

Bin 1304: 14 of cap free
Amount of items: 3
Items: 
Size: 695805 Color: 4
Size: 152333 Color: 0
Size: 151849 Color: 4

Bin 1305: 14 of cap free
Amount of items: 2
Items: 
Size: 502470 Color: 2
Size: 497517 Color: 3

Bin 1306: 14 of cap free
Amount of items: 2
Items: 
Size: 507311 Color: 2
Size: 492676 Color: 1

Bin 1307: 14 of cap free
Amount of items: 2
Items: 
Size: 543656 Color: 1
Size: 456331 Color: 0

Bin 1308: 14 of cap free
Amount of items: 2
Items: 
Size: 549832 Color: 1
Size: 450155 Color: 2

Bin 1309: 14 of cap free
Amount of items: 2
Items: 
Size: 552269 Color: 1
Size: 447718 Color: 4

Bin 1310: 14 of cap free
Amount of items: 2
Items: 
Size: 554776 Color: 3
Size: 445211 Color: 0

Bin 1311: 14 of cap free
Amount of items: 2
Items: 
Size: 586550 Color: 4
Size: 413437 Color: 3

Bin 1312: 14 of cap free
Amount of items: 2
Items: 
Size: 604644 Color: 2
Size: 395343 Color: 4

Bin 1313: 14 of cap free
Amount of items: 2
Items: 
Size: 611724 Color: 3
Size: 388263 Color: 0

Bin 1314: 14 of cap free
Amount of items: 2
Items: 
Size: 629795 Color: 2
Size: 370192 Color: 1

Bin 1315: 14 of cap free
Amount of items: 2
Items: 
Size: 638629 Color: 1
Size: 361358 Color: 3

Bin 1316: 14 of cap free
Amount of items: 2
Items: 
Size: 666938 Color: 4
Size: 333049 Color: 2

Bin 1317: 14 of cap free
Amount of items: 2
Items: 
Size: 672952 Color: 2
Size: 327035 Color: 0

Bin 1318: 14 of cap free
Amount of items: 2
Items: 
Size: 745784 Color: 3
Size: 254203 Color: 4

Bin 1319: 14 of cap free
Amount of items: 2
Items: 
Size: 759441 Color: 0
Size: 240546 Color: 2

Bin 1320: 14 of cap free
Amount of items: 2
Items: 
Size: 770713 Color: 0
Size: 229274 Color: 2

Bin 1321: 14 of cap free
Amount of items: 2
Items: 
Size: 776136 Color: 3
Size: 223851 Color: 2

Bin 1322: 14 of cap free
Amount of items: 2
Items: 
Size: 782987 Color: 4
Size: 217000 Color: 0

Bin 1323: 14 of cap free
Amount of items: 2
Items: 
Size: 783273 Color: 0
Size: 216714 Color: 3

Bin 1324: 14 of cap free
Amount of items: 2
Items: 
Size: 799904 Color: 0
Size: 200083 Color: 3

Bin 1325: 14 of cap free
Amount of items: 2
Items: 
Size: 594791 Color: 4
Size: 405196 Color: 0

Bin 1326: 14 of cap free
Amount of items: 2
Items: 
Size: 563732 Color: 2
Size: 436255 Color: 0

Bin 1327: 14 of cap free
Amount of items: 2
Items: 
Size: 613081 Color: 2
Size: 386906 Color: 0

Bin 1328: 14 of cap free
Amount of items: 2
Items: 
Size: 554439 Color: 0
Size: 445548 Color: 3

Bin 1329: 14 of cap free
Amount of items: 3
Items: 
Size: 612452 Color: 4
Size: 195415 Color: 3
Size: 192120 Color: 1

Bin 1330: 14 of cap free
Amount of items: 2
Items: 
Size: 738513 Color: 0
Size: 261474 Color: 3

Bin 1331: 14 of cap free
Amount of items: 2
Items: 
Size: 555623 Color: 1
Size: 444364 Color: 0

Bin 1332: 14 of cap free
Amount of items: 3
Items: 
Size: 745009 Color: 4
Size: 128798 Color: 3
Size: 126180 Color: 4

Bin 1333: 14 of cap free
Amount of items: 3
Items: 
Size: 734883 Color: 2
Size: 133000 Color: 2
Size: 132104 Color: 0

Bin 1334: 14 of cap free
Amount of items: 3
Items: 
Size: 619781 Color: 4
Size: 192385 Color: 1
Size: 187821 Color: 4

Bin 1335: 14 of cap free
Amount of items: 2
Items: 
Size: 518145 Color: 0
Size: 481842 Color: 3

Bin 1336: 14 of cap free
Amount of items: 3
Items: 
Size: 671858 Color: 0
Size: 164408 Color: 4
Size: 163721 Color: 0

Bin 1337: 14 of cap free
Amount of items: 2
Items: 
Size: 791971 Color: 2
Size: 208016 Color: 1

Bin 1338: 14 of cap free
Amount of items: 2
Items: 
Size: 796996 Color: 4
Size: 202991 Color: 0

Bin 1339: 14 of cap free
Amount of items: 3
Items: 
Size: 416299 Color: 3
Size: 321026 Color: 2
Size: 262662 Color: 1

Bin 1340: 14 of cap free
Amount of items: 2
Items: 
Size: 754107 Color: 3
Size: 245880 Color: 2

Bin 1341: 14 of cap free
Amount of items: 2
Items: 
Size: 537035 Color: 0
Size: 462952 Color: 1

Bin 1342: 14 of cap free
Amount of items: 3
Items: 
Size: 670968 Color: 3
Size: 165027 Color: 4
Size: 163992 Color: 0

Bin 1343: 14 of cap free
Amount of items: 3
Items: 
Size: 692034 Color: 4
Size: 154105 Color: 1
Size: 153848 Color: 1

Bin 1344: 14 of cap free
Amount of items: 2
Items: 
Size: 608185 Color: 3
Size: 391802 Color: 4

Bin 1345: 14 of cap free
Amount of items: 2
Items: 
Size: 550629 Color: 3
Size: 449358 Color: 0

Bin 1346: 14 of cap free
Amount of items: 2
Items: 
Size: 714545 Color: 3
Size: 285442 Color: 1

Bin 1347: 14 of cap free
Amount of items: 2
Items: 
Size: 781502 Color: 2
Size: 218485 Color: 1

Bin 1348: 14 of cap free
Amount of items: 2
Items: 
Size: 798503 Color: 4
Size: 201484 Color: 1

Bin 1349: 15 of cap free
Amount of items: 3
Items: 
Size: 643385 Color: 4
Size: 178782 Color: 3
Size: 177819 Color: 1

Bin 1350: 15 of cap free
Amount of items: 2
Items: 
Size: 642790 Color: 2
Size: 357196 Color: 3

Bin 1351: 15 of cap free
Amount of items: 3
Items: 
Size: 400625 Color: 2
Size: 322381 Color: 0
Size: 276980 Color: 2

Bin 1352: 15 of cap free
Amount of items: 2
Items: 
Size: 502719 Color: 3
Size: 497267 Color: 1

Bin 1353: 15 of cap free
Amount of items: 2
Items: 
Size: 538908 Color: 2
Size: 461078 Color: 0

Bin 1354: 15 of cap free
Amount of items: 3
Items: 
Size: 557632 Color: 0
Size: 222257 Color: 3
Size: 220097 Color: 3

Bin 1355: 15 of cap free
Amount of items: 2
Items: 
Size: 561488 Color: 2
Size: 438498 Color: 3

Bin 1356: 15 of cap free
Amount of items: 2
Items: 
Size: 561652 Color: 4
Size: 438334 Color: 2

Bin 1357: 15 of cap free
Amount of items: 2
Items: 
Size: 565386 Color: 4
Size: 434600 Color: 3

Bin 1358: 15 of cap free
Amount of items: 2
Items: 
Size: 566184 Color: 1
Size: 433802 Color: 2

Bin 1359: 15 of cap free
Amount of items: 2
Items: 
Size: 600206 Color: 2
Size: 399780 Color: 4

Bin 1360: 15 of cap free
Amount of items: 2
Items: 
Size: 608459 Color: 3
Size: 391527 Color: 4

Bin 1361: 15 of cap free
Amount of items: 3
Items: 
Size: 612598 Color: 0
Size: 193838 Color: 2
Size: 193550 Color: 2

Bin 1362: 15 of cap free
Amount of items: 2
Items: 
Size: 651234 Color: 4
Size: 348752 Color: 0

Bin 1363: 15 of cap free
Amount of items: 2
Items: 
Size: 671422 Color: 4
Size: 328564 Color: 2

Bin 1364: 15 of cap free
Amount of items: 2
Items: 
Size: 678018 Color: 2
Size: 321968 Color: 0

Bin 1365: 15 of cap free
Amount of items: 2
Items: 
Size: 706379 Color: 3
Size: 293607 Color: 2

Bin 1366: 15 of cap free
Amount of items: 3
Items: 
Size: 719834 Color: 3
Size: 140137 Color: 4
Size: 140015 Color: 4

Bin 1367: 15 of cap free
Amount of items: 2
Items: 
Size: 724662 Color: 2
Size: 275324 Color: 1

Bin 1368: 15 of cap free
Amount of items: 2
Items: 
Size: 735092 Color: 0
Size: 264894 Color: 2

Bin 1369: 15 of cap free
Amount of items: 2
Items: 
Size: 753246 Color: 4
Size: 246740 Color: 1

Bin 1370: 15 of cap free
Amount of items: 2
Items: 
Size: 757322 Color: 2
Size: 242664 Color: 0

Bin 1371: 15 of cap free
Amount of items: 2
Items: 
Size: 762778 Color: 4
Size: 237208 Color: 1

Bin 1372: 15 of cap free
Amount of items: 2
Items: 
Size: 765101 Color: 4
Size: 234885 Color: 0

Bin 1373: 15 of cap free
Amount of items: 2
Items: 
Size: 795548 Color: 3
Size: 204438 Color: 1

Bin 1374: 15 of cap free
Amount of items: 2
Items: 
Size: 573984 Color: 2
Size: 426002 Color: 1

Bin 1375: 15 of cap free
Amount of items: 3
Items: 
Size: 693935 Color: 2
Size: 157828 Color: 3
Size: 148223 Color: 2

Bin 1376: 15 of cap free
Amount of items: 3
Items: 
Size: 731676 Color: 2
Size: 167452 Color: 2
Size: 100858 Color: 3

Bin 1377: 15 of cap free
Amount of items: 3
Items: 
Size: 683509 Color: 1
Size: 159820 Color: 0
Size: 156657 Color: 0

Bin 1378: 15 of cap free
Amount of items: 2
Items: 
Size: 702647 Color: 0
Size: 297339 Color: 1

Bin 1379: 15 of cap free
Amount of items: 2
Items: 
Size: 788133 Color: 2
Size: 211853 Color: 3

Bin 1380: 15 of cap free
Amount of items: 2
Items: 
Size: 746269 Color: 3
Size: 253717 Color: 2

Bin 1381: 15 of cap free
Amount of items: 2
Items: 
Size: 754383 Color: 3
Size: 245603 Color: 4

Bin 1382: 15 of cap free
Amount of items: 2
Items: 
Size: 693408 Color: 1
Size: 306578 Color: 3

Bin 1383: 15 of cap free
Amount of items: 2
Items: 
Size: 689449 Color: 0
Size: 310537 Color: 3

Bin 1384: 15 of cap free
Amount of items: 2
Items: 
Size: 581604 Color: 0
Size: 418382 Color: 1

Bin 1385: 15 of cap free
Amount of items: 2
Items: 
Size: 748315 Color: 4
Size: 251671 Color: 2

Bin 1386: 15 of cap free
Amount of items: 3
Items: 
Size: 431216 Color: 4
Size: 312542 Color: 2
Size: 256228 Color: 4

Bin 1387: 15 of cap free
Amount of items: 3
Items: 
Size: 672236 Color: 2
Size: 164275 Color: 3
Size: 163475 Color: 2

Bin 1388: 15 of cap free
Amount of items: 3
Items: 
Size: 615330 Color: 0
Size: 194076 Color: 3
Size: 190580 Color: 1

Bin 1389: 15 of cap free
Amount of items: 2
Items: 
Size: 738993 Color: 2
Size: 260993 Color: 1

Bin 1390: 15 of cap free
Amount of items: 2
Items: 
Size: 758668 Color: 1
Size: 241318 Color: 4

Bin 1391: 15 of cap free
Amount of items: 3
Items: 
Size: 372168 Color: 0
Size: 333139 Color: 3
Size: 294679 Color: 0

Bin 1392: 15 of cap free
Amount of items: 3
Items: 
Size: 707136 Color: 0
Size: 146777 Color: 3
Size: 146073 Color: 1

Bin 1393: 15 of cap free
Amount of items: 2
Items: 
Size: 500941 Color: 0
Size: 499045 Color: 4

Bin 1394: 15 of cap free
Amount of items: 2
Items: 
Size: 752191 Color: 2
Size: 247795 Color: 3

Bin 1395: 15 of cap free
Amount of items: 2
Items: 
Size: 734905 Color: 2
Size: 265081 Color: 3

Bin 1396: 15 of cap free
Amount of items: 2
Items: 
Size: 563578 Color: 2
Size: 436408 Color: 3

Bin 1397: 15 of cap free
Amount of items: 2
Items: 
Size: 582768 Color: 3
Size: 417218 Color: 0

Bin 1398: 15 of cap free
Amount of items: 3
Items: 
Size: 428578 Color: 4
Size: 294402 Color: 4
Size: 277006 Color: 2

Bin 1399: 16 of cap free
Amount of items: 2
Items: 
Size: 506402 Color: 0
Size: 493583 Color: 1

Bin 1400: 16 of cap free
Amount of items: 2
Items: 
Size: 517222 Color: 0
Size: 482763 Color: 4

Bin 1401: 16 of cap free
Amount of items: 2
Items: 
Size: 529620 Color: 0
Size: 470365 Color: 1

Bin 1402: 16 of cap free
Amount of items: 2
Items: 
Size: 533138 Color: 2
Size: 466847 Color: 1

Bin 1403: 16 of cap free
Amount of items: 2
Items: 
Size: 617582 Color: 2
Size: 382403 Color: 4

Bin 1404: 16 of cap free
Amount of items: 2
Items: 
Size: 635941 Color: 3
Size: 364044 Color: 2

Bin 1405: 16 of cap free
Amount of items: 2
Items: 
Size: 672890 Color: 0
Size: 327095 Color: 2

Bin 1406: 16 of cap free
Amount of items: 2
Items: 
Size: 706447 Color: 4
Size: 293538 Color: 3

Bin 1407: 16 of cap free
Amount of items: 2
Items: 
Size: 786158 Color: 3
Size: 213827 Color: 4

Bin 1408: 16 of cap free
Amount of items: 2
Items: 
Size: 797997 Color: 1
Size: 201988 Color: 3

Bin 1409: 16 of cap free
Amount of items: 2
Items: 
Size: 781212 Color: 2
Size: 218773 Color: 0

Bin 1410: 16 of cap free
Amount of items: 3
Items: 
Size: 375919 Color: 2
Size: 312657 Color: 1
Size: 311409 Color: 3

Bin 1411: 16 of cap free
Amount of items: 3
Items: 
Size: 682581 Color: 4
Size: 158719 Color: 0
Size: 158685 Color: 1

Bin 1412: 16 of cap free
Amount of items: 2
Items: 
Size: 504664 Color: 1
Size: 495321 Color: 3

Bin 1413: 16 of cap free
Amount of items: 3
Items: 
Size: 603486 Color: 0
Size: 199088 Color: 2
Size: 197411 Color: 4

Bin 1414: 16 of cap free
Amount of items: 3
Items: 
Size: 370655 Color: 1
Size: 333666 Color: 0
Size: 295664 Color: 3

Bin 1415: 16 of cap free
Amount of items: 2
Items: 
Size: 603849 Color: 1
Size: 396136 Color: 2

Bin 1416: 16 of cap free
Amount of items: 3
Items: 
Size: 687159 Color: 2
Size: 156684 Color: 1
Size: 156142 Color: 3

Bin 1417: 16 of cap free
Amount of items: 3
Items: 
Size: 376061 Color: 2
Size: 329897 Color: 1
Size: 294027 Color: 1

Bin 1418: 16 of cap free
Amount of items: 2
Items: 
Size: 620951 Color: 2
Size: 379034 Color: 3

Bin 1419: 16 of cap free
Amount of items: 3
Items: 
Size: 663064 Color: 4
Size: 170046 Color: 1
Size: 166875 Color: 2

Bin 1420: 16 of cap free
Amount of items: 3
Items: 
Size: 654585 Color: 4
Size: 172872 Color: 2
Size: 172528 Color: 4

Bin 1421: 16 of cap free
Amount of items: 2
Items: 
Size: 568177 Color: 0
Size: 431808 Color: 3

Bin 1422: 16 of cap free
Amount of items: 2
Items: 
Size: 655355 Color: 0
Size: 344630 Color: 3

Bin 1423: 16 of cap free
Amount of items: 2
Items: 
Size: 568278 Color: 2
Size: 431707 Color: 4

Bin 1424: 16 of cap free
Amount of items: 2
Items: 
Size: 775737 Color: 2
Size: 224248 Color: 0

Bin 1425: 16 of cap free
Amount of items: 2
Items: 
Size: 575334 Color: 3
Size: 424651 Color: 1

Bin 1426: 16 of cap free
Amount of items: 2
Items: 
Size: 792251 Color: 2
Size: 207734 Color: 4

Bin 1427: 16 of cap free
Amount of items: 2
Items: 
Size: 556698 Color: 2
Size: 443287 Color: 3

Bin 1428: 16 of cap free
Amount of items: 3
Items: 
Size: 565264 Color: 4
Size: 222953 Color: 0
Size: 211768 Color: 3

Bin 1429: 16 of cap free
Amount of items: 2
Items: 
Size: 608527 Color: 0
Size: 391458 Color: 4

Bin 1430: 16 of cap free
Amount of items: 3
Items: 
Size: 591830 Color: 4
Size: 204255 Color: 2
Size: 203900 Color: 4

Bin 1431: 16 of cap free
Amount of items: 2
Items: 
Size: 717494 Color: 4
Size: 282491 Color: 2

Bin 1432: 16 of cap free
Amount of items: 3
Items: 
Size: 343081 Color: 3
Size: 340099 Color: 1
Size: 316805 Color: 0

Bin 1433: 17 of cap free
Amount of items: 2
Items: 
Size: 685625 Color: 1
Size: 314359 Color: 4

Bin 1434: 17 of cap free
Amount of items: 2
Items: 
Size: 508452 Color: 4
Size: 491532 Color: 0

Bin 1435: 17 of cap free
Amount of items: 2
Items: 
Size: 512653 Color: 2
Size: 487331 Color: 4

Bin 1436: 17 of cap free
Amount of items: 2
Items: 
Size: 516232 Color: 2
Size: 483752 Color: 0

Bin 1437: 17 of cap free
Amount of items: 2
Items: 
Size: 526829 Color: 4
Size: 473155 Color: 3

Bin 1438: 17 of cap free
Amount of items: 2
Items: 
Size: 543256 Color: 2
Size: 456728 Color: 3

Bin 1439: 17 of cap free
Amount of items: 2
Items: 
Size: 544622 Color: 3
Size: 455362 Color: 1

Bin 1440: 17 of cap free
Amount of items: 2
Items: 
Size: 547600 Color: 2
Size: 452384 Color: 3

Bin 1441: 17 of cap free
Amount of items: 2
Items: 
Size: 549145 Color: 2
Size: 450839 Color: 0

Bin 1442: 17 of cap free
Amount of items: 3
Items: 
Size: 558226 Color: 0
Size: 223663 Color: 1
Size: 218095 Color: 3

Bin 1443: 17 of cap free
Amount of items: 2
Items: 
Size: 562755 Color: 3
Size: 437229 Color: 1

Bin 1444: 17 of cap free
Amount of items: 2
Items: 
Size: 564379 Color: 2
Size: 435605 Color: 0

Bin 1445: 17 of cap free
Amount of items: 2
Items: 
Size: 566725 Color: 2
Size: 433259 Color: 4

Bin 1446: 17 of cap free
Amount of items: 2
Items: 
Size: 610690 Color: 4
Size: 389294 Color: 0

Bin 1447: 17 of cap free
Amount of items: 3
Items: 
Size: 636204 Color: 4
Size: 182637 Color: 1
Size: 181143 Color: 0

Bin 1448: 17 of cap free
Amount of items: 2
Items: 
Size: 662950 Color: 4
Size: 337034 Color: 0

Bin 1449: 17 of cap free
Amount of items: 2
Items: 
Size: 671909 Color: 2
Size: 328075 Color: 4

Bin 1450: 17 of cap free
Amount of items: 2
Items: 
Size: 672536 Color: 1
Size: 327448 Color: 3

Bin 1451: 17 of cap free
Amount of items: 2
Items: 
Size: 682355 Color: 3
Size: 317629 Color: 2

Bin 1452: 17 of cap free
Amount of items: 2
Items: 
Size: 712340 Color: 0
Size: 287644 Color: 4

Bin 1453: 17 of cap free
Amount of items: 2
Items: 
Size: 768403 Color: 1
Size: 231581 Color: 3

Bin 1454: 17 of cap free
Amount of items: 3
Items: 
Size: 670238 Color: 4
Size: 165219 Color: 2
Size: 164527 Color: 0

Bin 1455: 17 of cap free
Amount of items: 3
Items: 
Size: 701238 Color: 3
Size: 149537 Color: 1
Size: 149209 Color: 3

Bin 1456: 17 of cap free
Amount of items: 2
Items: 
Size: 799471 Color: 2
Size: 200513 Color: 1

Bin 1457: 17 of cap free
Amount of items: 3
Items: 
Size: 711588 Color: 0
Size: 144210 Color: 4
Size: 144186 Color: 3

Bin 1458: 17 of cap free
Amount of items: 2
Items: 
Size: 768240 Color: 3
Size: 231744 Color: 0

Bin 1459: 17 of cap free
Amount of items: 2
Items: 
Size: 688631 Color: 2
Size: 311353 Color: 4

Bin 1460: 17 of cap free
Amount of items: 3
Items: 
Size: 654114 Color: 0
Size: 173419 Color: 4
Size: 172451 Color: 4

Bin 1461: 17 of cap free
Amount of items: 2
Items: 
Size: 529107 Color: 2
Size: 470877 Color: 4

Bin 1462: 17 of cap free
Amount of items: 2
Items: 
Size: 662346 Color: 2
Size: 337638 Color: 3

Bin 1463: 17 of cap free
Amount of items: 2
Items: 
Size: 722652 Color: 4
Size: 277332 Color: 3

Bin 1464: 17 of cap free
Amount of items: 2
Items: 
Size: 784990 Color: 2
Size: 214994 Color: 1

Bin 1465: 17 of cap free
Amount of items: 2
Items: 
Size: 661361 Color: 0
Size: 338623 Color: 3

Bin 1466: 17 of cap free
Amount of items: 2
Items: 
Size: 699021 Color: 0
Size: 300963 Color: 3

Bin 1467: 17 of cap free
Amount of items: 2
Items: 
Size: 793727 Color: 1
Size: 206257 Color: 4

Bin 1468: 17 of cap free
Amount of items: 2
Items: 
Size: 567385 Color: 2
Size: 432599 Color: 4

Bin 1469: 17 of cap free
Amount of items: 2
Items: 
Size: 613168 Color: 3
Size: 386816 Color: 4

Bin 1470: 18 of cap free
Amount of items: 3
Items: 
Size: 690185 Color: 1
Size: 154986 Color: 0
Size: 154812 Color: 4

Bin 1471: 18 of cap free
Amount of items: 3
Items: 
Size: 652143 Color: 3
Size: 174059 Color: 1
Size: 173781 Color: 1

Bin 1472: 18 of cap free
Amount of items: 2
Items: 
Size: 501207 Color: 4
Size: 498776 Color: 3

Bin 1473: 18 of cap free
Amount of items: 2
Items: 
Size: 529937 Color: 3
Size: 470046 Color: 2

Bin 1474: 18 of cap free
Amount of items: 2
Items: 
Size: 535286 Color: 2
Size: 464697 Color: 4

Bin 1475: 18 of cap free
Amount of items: 2
Items: 
Size: 543928 Color: 4
Size: 456055 Color: 2

Bin 1476: 18 of cap free
Amount of items: 2
Items: 
Size: 545695 Color: 3
Size: 454288 Color: 0

Bin 1477: 18 of cap free
Amount of items: 2
Items: 
Size: 553144 Color: 4
Size: 446839 Color: 3

Bin 1478: 18 of cap free
Amount of items: 2
Items: 
Size: 622164 Color: 1
Size: 377819 Color: 0

Bin 1479: 18 of cap free
Amount of items: 2
Items: 
Size: 625137 Color: 0
Size: 374846 Color: 1

Bin 1480: 18 of cap free
Amount of items: 2
Items: 
Size: 663552 Color: 1
Size: 336431 Color: 0

Bin 1481: 18 of cap free
Amount of items: 2
Items: 
Size: 664614 Color: 1
Size: 335369 Color: 2

Bin 1482: 18 of cap free
Amount of items: 3
Items: 
Size: 684154 Color: 0
Size: 158166 Color: 1
Size: 157663 Color: 2

Bin 1483: 18 of cap free
Amount of items: 2
Items: 
Size: 697604 Color: 1
Size: 302379 Color: 4

Bin 1484: 18 of cap free
Amount of items: 2
Items: 
Size: 714586 Color: 3
Size: 285397 Color: 2

Bin 1485: 18 of cap free
Amount of items: 2
Items: 
Size: 737263 Color: 0
Size: 262720 Color: 1

Bin 1486: 18 of cap free
Amount of items: 2
Items: 
Size: 761405 Color: 4
Size: 238578 Color: 1

Bin 1487: 18 of cap free
Amount of items: 2
Items: 
Size: 778852 Color: 3
Size: 221131 Color: 4

Bin 1488: 18 of cap free
Amount of items: 2
Items: 
Size: 798875 Color: 4
Size: 201108 Color: 3

Bin 1489: 18 of cap free
Amount of items: 3
Items: 
Size: 729531 Color: 2
Size: 139839 Color: 1
Size: 130613 Color: 3

Bin 1490: 18 of cap free
Amount of items: 2
Items: 
Size: 740186 Color: 0
Size: 259797 Color: 1

Bin 1491: 18 of cap free
Amount of items: 2
Items: 
Size: 518422 Color: 4
Size: 481561 Color: 2

Bin 1492: 18 of cap free
Amount of items: 2
Items: 
Size: 767097 Color: 2
Size: 232886 Color: 4

Bin 1493: 18 of cap free
Amount of items: 3
Items: 
Size: 715225 Color: 0
Size: 142441 Color: 1
Size: 142317 Color: 3

Bin 1494: 18 of cap free
Amount of items: 2
Items: 
Size: 596811 Color: 2
Size: 403172 Color: 3

Bin 1495: 18 of cap free
Amount of items: 2
Items: 
Size: 712367 Color: 2
Size: 287616 Color: 1

Bin 1496: 18 of cap free
Amount of items: 2
Items: 
Size: 661064 Color: 0
Size: 338919 Color: 2

Bin 1497: 18 of cap free
Amount of items: 3
Items: 
Size: 646965 Color: 4
Size: 177031 Color: 2
Size: 175987 Color: 1

Bin 1498: 18 of cap free
Amount of items: 3
Items: 
Size: 635361 Color: 0
Size: 185443 Color: 3
Size: 179179 Color: 1

Bin 1499: 18 of cap free
Amount of items: 2
Items: 
Size: 500071 Color: 3
Size: 499912 Color: 0

Bin 1500: 18 of cap free
Amount of items: 3
Items: 
Size: 500450 Color: 0
Size: 253178 Color: 3
Size: 246355 Color: 0

Bin 1501: 18 of cap free
Amount of items: 2
Items: 
Size: 576528 Color: 0
Size: 423455 Color: 2

Bin 1502: 18 of cap free
Amount of items: 3
Items: 
Size: 368382 Color: 0
Size: 368116 Color: 3
Size: 263485 Color: 0

Bin 1503: 18 of cap free
Amount of items: 2
Items: 
Size: 782649 Color: 1
Size: 217334 Color: 2

Bin 1504: 18 of cap free
Amount of items: 3
Items: 
Size: 370111 Color: 3
Size: 368200 Color: 4
Size: 261672 Color: 2

Bin 1505: 18 of cap free
Amount of items: 3
Items: 
Size: 368065 Color: 2
Size: 353255 Color: 1
Size: 278663 Color: 1

Bin 1506: 19 of cap free
Amount of items: 3
Items: 
Size: 675439 Color: 3
Size: 162878 Color: 0
Size: 161665 Color: 1

Bin 1507: 19 of cap free
Amount of items: 2
Items: 
Size: 503838 Color: 4
Size: 496144 Color: 0

Bin 1508: 19 of cap free
Amount of items: 2
Items: 
Size: 544465 Color: 1
Size: 455517 Color: 3

Bin 1509: 19 of cap free
Amount of items: 2
Items: 
Size: 551095 Color: 0
Size: 448887 Color: 1

Bin 1510: 19 of cap free
Amount of items: 2
Items: 
Size: 555030 Color: 3
Size: 444952 Color: 1

Bin 1511: 19 of cap free
Amount of items: 2
Items: 
Size: 576053 Color: 1
Size: 423929 Color: 0

Bin 1512: 19 of cap free
Amount of items: 2
Items: 
Size: 606264 Color: 3
Size: 393718 Color: 1

Bin 1513: 19 of cap free
Amount of items: 2
Items: 
Size: 613896 Color: 3
Size: 386086 Color: 0

Bin 1514: 19 of cap free
Amount of items: 2
Items: 
Size: 627672 Color: 1
Size: 372310 Color: 0

Bin 1515: 19 of cap free
Amount of items: 2
Items: 
Size: 643722 Color: 1
Size: 356260 Color: 2

Bin 1516: 19 of cap free
Amount of items: 2
Items: 
Size: 707338 Color: 2
Size: 292644 Color: 0

Bin 1517: 19 of cap free
Amount of items: 2
Items: 
Size: 709431 Color: 3
Size: 290551 Color: 0

Bin 1518: 19 of cap free
Amount of items: 2
Items: 
Size: 738967 Color: 0
Size: 261015 Color: 2

Bin 1519: 19 of cap free
Amount of items: 3
Items: 
Size: 660886 Color: 3
Size: 169603 Color: 0
Size: 169493 Color: 2

Bin 1520: 19 of cap free
Amount of items: 3
Items: 
Size: 660766 Color: 1
Size: 170339 Color: 0
Size: 168877 Color: 4

Bin 1521: 19 of cap free
Amount of items: 2
Items: 
Size: 735249 Color: 2
Size: 264733 Color: 4

Bin 1522: 19 of cap free
Amount of items: 2
Items: 
Size: 539610 Color: 0
Size: 460372 Color: 3

Bin 1523: 19 of cap free
Amount of items: 2
Items: 
Size: 532637 Color: 2
Size: 467345 Color: 1

Bin 1524: 19 of cap free
Amount of items: 3
Items: 
Size: 373998 Color: 0
Size: 331369 Color: 1
Size: 294615 Color: 2

Bin 1525: 19 of cap free
Amount of items: 2
Items: 
Size: 699200 Color: 4
Size: 300782 Color: 2

Bin 1526: 19 of cap free
Amount of items: 3
Items: 
Size: 608063 Color: 0
Size: 210932 Color: 3
Size: 180987 Color: 1

Bin 1527: 19 of cap free
Amount of items: 3
Items: 
Size: 709174 Color: 3
Size: 146162 Color: 0
Size: 144646 Color: 2

Bin 1528: 19 of cap free
Amount of items: 2
Items: 
Size: 612256 Color: 1
Size: 387726 Color: 0

Bin 1529: 19 of cap free
Amount of items: 2
Items: 
Size: 653073 Color: 2
Size: 346909 Color: 3

Bin 1530: 19 of cap free
Amount of items: 3
Items: 
Size: 577056 Color: 4
Size: 215918 Color: 2
Size: 207008 Color: 2

Bin 1531: 19 of cap free
Amount of items: 2
Items: 
Size: 797137 Color: 2
Size: 202845 Color: 0

Bin 1532: 19 of cap free
Amount of items: 2
Items: 
Size: 518288 Color: 0
Size: 481694 Color: 4

Bin 1533: 20 of cap free
Amount of items: 2
Items: 
Size: 527288 Color: 2
Size: 472693 Color: 0

Bin 1534: 20 of cap free
Amount of items: 2
Items: 
Size: 532128 Color: 2
Size: 467853 Color: 1

Bin 1535: 20 of cap free
Amount of items: 2
Items: 
Size: 541778 Color: 1
Size: 458203 Color: 2

Bin 1536: 20 of cap free
Amount of items: 2
Items: 
Size: 588531 Color: 0
Size: 411450 Color: 1

Bin 1537: 20 of cap free
Amount of items: 2
Items: 
Size: 589885 Color: 1
Size: 410096 Color: 2

Bin 1538: 20 of cap free
Amount of items: 2
Items: 
Size: 598902 Color: 2
Size: 401079 Color: 0

Bin 1539: 20 of cap free
Amount of items: 2
Items: 
Size: 603290 Color: 2
Size: 396691 Color: 4

Bin 1540: 20 of cap free
Amount of items: 2
Items: 
Size: 614808 Color: 3
Size: 385173 Color: 2

Bin 1541: 20 of cap free
Amount of items: 2
Items: 
Size: 656696 Color: 4
Size: 343285 Color: 0

Bin 1542: 20 of cap free
Amount of items: 2
Items: 
Size: 681431 Color: 4
Size: 318550 Color: 3

Bin 1543: 20 of cap free
Amount of items: 2
Items: 
Size: 684422 Color: 1
Size: 315559 Color: 0

Bin 1544: 20 of cap free
Amount of items: 2
Items: 
Size: 701410 Color: 3
Size: 298571 Color: 2

Bin 1545: 20 of cap free
Amount of items: 3
Items: 
Size: 372276 Color: 1
Size: 333182 Color: 4
Size: 294523 Color: 2

Bin 1546: 20 of cap free
Amount of items: 3
Items: 
Size: 655926 Color: 3
Size: 172135 Color: 2
Size: 171920 Color: 0

Bin 1547: 20 of cap free
Amount of items: 2
Items: 
Size: 537711 Color: 3
Size: 462270 Color: 1

Bin 1548: 20 of cap free
Amount of items: 2
Items: 
Size: 562624 Color: 4
Size: 437357 Color: 1

Bin 1549: 20 of cap free
Amount of items: 2
Items: 
Size: 529269 Color: 1
Size: 470712 Color: 3

Bin 1550: 20 of cap free
Amount of items: 3
Items: 
Size: 622478 Color: 0
Size: 192918 Color: 3
Size: 184585 Color: 1

Bin 1551: 20 of cap free
Amount of items: 2
Items: 
Size: 668192 Color: 0
Size: 331789 Color: 4

Bin 1552: 20 of cap free
Amount of items: 3
Items: 
Size: 686989 Color: 2
Size: 157344 Color: 3
Size: 155648 Color: 1

Bin 1553: 20 of cap free
Amount of items: 2
Items: 
Size: 544725 Color: 3
Size: 455256 Color: 0

Bin 1554: 20 of cap free
Amount of items: 2
Items: 
Size: 553408 Color: 0
Size: 446573 Color: 1

Bin 1555: 20 of cap free
Amount of items: 2
Items: 
Size: 741907 Color: 2
Size: 258074 Color: 1

Bin 1556: 20 of cap free
Amount of items: 2
Items: 
Size: 609043 Color: 4
Size: 390938 Color: 3

Bin 1557: 20 of cap free
Amount of items: 2
Items: 
Size: 652804 Color: 1
Size: 347177 Color: 3

Bin 1558: 20 of cap free
Amount of items: 2
Items: 
Size: 749404 Color: 2
Size: 250577 Color: 0

Bin 1559: 20 of cap free
Amount of items: 2
Items: 
Size: 601357 Color: 0
Size: 398624 Color: 2

Bin 1560: 20 of cap free
Amount of items: 2
Items: 
Size: 795006 Color: 1
Size: 204975 Color: 0

Bin 1561: 20 of cap free
Amount of items: 3
Items: 
Size: 555793 Color: 4
Size: 224197 Color: 3
Size: 219991 Color: 4

Bin 1562: 20 of cap free
Amount of items: 3
Items: 
Size: 368676 Color: 0
Size: 353501 Color: 0
Size: 277804 Color: 2

Bin 1563: 21 of cap free
Amount of items: 2
Items: 
Size: 758718 Color: 4
Size: 241262 Color: 2

Bin 1564: 21 of cap free
Amount of items: 3
Items: 
Size: 626747 Color: 4
Size: 189709 Color: 3
Size: 183524 Color: 1

Bin 1565: 21 of cap free
Amount of items: 2
Items: 
Size: 732001 Color: 4
Size: 267979 Color: 0

Bin 1566: 21 of cap free
Amount of items: 2
Items: 
Size: 513757 Color: 3
Size: 486223 Color: 0

Bin 1567: 21 of cap free
Amount of items: 2
Items: 
Size: 591933 Color: 4
Size: 408047 Color: 1

Bin 1568: 21 of cap free
Amount of items: 3
Items: 
Size: 605018 Color: 0
Size: 197894 Color: 3
Size: 197068 Color: 2

Bin 1569: 21 of cap free
Amount of items: 2
Items: 
Size: 617536 Color: 0
Size: 382444 Color: 1

Bin 1570: 21 of cap free
Amount of items: 2
Items: 
Size: 624780 Color: 2
Size: 375200 Color: 1

Bin 1571: 21 of cap free
Amount of items: 3
Items: 
Size: 634355 Color: 4
Size: 185837 Color: 3
Size: 179788 Color: 1

Bin 1572: 21 of cap free
Amount of items: 2
Items: 
Size: 642147 Color: 2
Size: 357833 Color: 0

Bin 1573: 21 of cap free
Amount of items: 3
Items: 
Size: 644303 Color: 3
Size: 178701 Color: 2
Size: 176976 Color: 4

Bin 1574: 21 of cap free
Amount of items: 2
Items: 
Size: 666094 Color: 2
Size: 333886 Color: 3

Bin 1575: 21 of cap free
Amount of items: 2
Items: 
Size: 691333 Color: 2
Size: 308647 Color: 3

Bin 1576: 21 of cap free
Amount of items: 2
Items: 
Size: 707970 Color: 1
Size: 292010 Color: 2

Bin 1577: 21 of cap free
Amount of items: 2
Items: 
Size: 724224 Color: 1
Size: 275756 Color: 0

Bin 1578: 21 of cap free
Amount of items: 2
Items: 
Size: 741344 Color: 4
Size: 258636 Color: 0

Bin 1579: 21 of cap free
Amount of items: 2
Items: 
Size: 761706 Color: 1
Size: 238274 Color: 4

Bin 1580: 21 of cap free
Amount of items: 2
Items: 
Size: 786604 Color: 3
Size: 213376 Color: 0

Bin 1581: 21 of cap free
Amount of items: 2
Items: 
Size: 791695 Color: 4
Size: 208285 Color: 3

Bin 1582: 21 of cap free
Amount of items: 2
Items: 
Size: 705253 Color: 2
Size: 294727 Color: 1

Bin 1583: 21 of cap free
Amount of items: 2
Items: 
Size: 713070 Color: 3
Size: 286910 Color: 2

Bin 1584: 21 of cap free
Amount of items: 2
Items: 
Size: 531052 Color: 3
Size: 468928 Color: 2

Bin 1585: 21 of cap free
Amount of items: 2
Items: 
Size: 609075 Color: 0
Size: 390905 Color: 4

Bin 1586: 21 of cap free
Amount of items: 2
Items: 
Size: 616345 Color: 4
Size: 383635 Color: 1

Bin 1587: 21 of cap free
Amount of items: 2
Items: 
Size: 519548 Color: 4
Size: 480432 Color: 3

Bin 1588: 21 of cap free
Amount of items: 2
Items: 
Size: 640622 Color: 2
Size: 359358 Color: 3

Bin 1589: 21 of cap free
Amount of items: 3
Items: 
Size: 762896 Color: 2
Size: 119459 Color: 4
Size: 117625 Color: 0

Bin 1590: 21 of cap free
Amount of items: 3
Items: 
Size: 590780 Color: 2
Size: 205098 Color: 1
Size: 204102 Color: 1

Bin 1591: 21 of cap free
Amount of items: 2
Items: 
Size: 740480 Color: 1
Size: 259500 Color: 3

Bin 1592: 21 of cap free
Amount of items: 2
Items: 
Size: 563926 Color: 1
Size: 436054 Color: 4

Bin 1593: 21 of cap free
Amount of items: 3
Items: 
Size: 540546 Color: 1
Size: 229992 Color: 4
Size: 229442 Color: 3

Bin 1594: 21 of cap free
Amount of items: 3
Items: 
Size: 671190 Color: 0
Size: 164870 Color: 3
Size: 163920 Color: 4

Bin 1595: 21 of cap free
Amount of items: 2
Items: 
Size: 745754 Color: 0
Size: 254226 Color: 1

Bin 1596: 21 of cap free
Amount of items: 2
Items: 
Size: 685685 Color: 2
Size: 314295 Color: 0

Bin 1597: 21 of cap free
Amount of items: 3
Items: 
Size: 368153 Color: 2
Size: 367462 Color: 0
Size: 264365 Color: 4

Bin 1598: 21 of cap free
Amount of items: 3
Items: 
Size: 358873 Color: 1
Size: 342149 Color: 4
Size: 298958 Color: 0

Bin 1599: 22 of cap free
Amount of items: 3
Items: 
Size: 426286 Color: 1
Size: 316202 Color: 4
Size: 257491 Color: 1

Bin 1600: 22 of cap free
Amount of items: 3
Items: 
Size: 609007 Color: 3
Size: 195990 Color: 1
Size: 194982 Color: 2

Bin 1601: 22 of cap free
Amount of items: 3
Items: 
Size: 427713 Color: 3
Size: 299244 Color: 1
Size: 273022 Color: 4

Bin 1602: 22 of cap free
Amount of items: 2
Items: 
Size: 517245 Color: 3
Size: 482734 Color: 4

Bin 1603: 22 of cap free
Amount of items: 2
Items: 
Size: 560259 Color: 1
Size: 439720 Color: 2

Bin 1604: 22 of cap free
Amount of items: 2
Items: 
Size: 560765 Color: 4
Size: 439214 Color: 1

Bin 1605: 22 of cap free
Amount of items: 2
Items: 
Size: 562312 Color: 3
Size: 437667 Color: 0

Bin 1606: 22 of cap free
Amount of items: 2
Items: 
Size: 565109 Color: 4
Size: 434870 Color: 3

Bin 1607: 22 of cap free
Amount of items: 2
Items: 
Size: 575681 Color: 0
Size: 424298 Color: 2

Bin 1608: 22 of cap free
Amount of items: 3
Items: 
Size: 606119 Color: 3
Size: 196934 Color: 4
Size: 196926 Color: 0

Bin 1609: 22 of cap free
Amount of items: 2
Items: 
Size: 619263 Color: 0
Size: 380716 Color: 3

Bin 1610: 22 of cap free
Amount of items: 2
Items: 
Size: 643588 Color: 4
Size: 356391 Color: 0

Bin 1611: 22 of cap free
Amount of items: 2
Items: 
Size: 664945 Color: 2
Size: 335034 Color: 4

Bin 1612: 22 of cap free
Amount of items: 2
Items: 
Size: 671961 Color: 4
Size: 328018 Color: 2

Bin 1613: 22 of cap free
Amount of items: 2
Items: 
Size: 673860 Color: 1
Size: 326119 Color: 2

Bin 1614: 22 of cap free
Amount of items: 2
Items: 
Size: 675673 Color: 2
Size: 324306 Color: 0

Bin 1615: 22 of cap free
Amount of items: 2
Items: 
Size: 679228 Color: 0
Size: 320751 Color: 4

Bin 1616: 22 of cap free
Amount of items: 2
Items: 
Size: 696897 Color: 3
Size: 303082 Color: 4

Bin 1617: 22 of cap free
Amount of items: 2
Items: 
Size: 709646 Color: 0
Size: 290333 Color: 1

Bin 1618: 22 of cap free
Amount of items: 2
Items: 
Size: 761638 Color: 3
Size: 238341 Color: 4

Bin 1619: 22 of cap free
Amount of items: 2
Items: 
Size: 763196 Color: 3
Size: 236783 Color: 4

Bin 1620: 22 of cap free
Amount of items: 2
Items: 
Size: 784669 Color: 2
Size: 215310 Color: 4

Bin 1621: 22 of cap free
Amount of items: 2
Items: 
Size: 652998 Color: 0
Size: 346981 Color: 3

Bin 1622: 22 of cap free
Amount of items: 2
Items: 
Size: 528290 Color: 4
Size: 471689 Color: 2

Bin 1623: 22 of cap free
Amount of items: 2
Items: 
Size: 531258 Color: 4
Size: 468721 Color: 3

Bin 1624: 22 of cap free
Amount of items: 2
Items: 
Size: 762526 Color: 2
Size: 237453 Color: 1

Bin 1625: 22 of cap free
Amount of items: 2
Items: 
Size: 759839 Color: 2
Size: 240140 Color: 4

Bin 1626: 22 of cap free
Amount of items: 2
Items: 
Size: 508800 Color: 4
Size: 491179 Color: 2

Bin 1627: 22 of cap free
Amount of items: 3
Items: 
Size: 700131 Color: 2
Size: 150241 Color: 1
Size: 149607 Color: 4

Bin 1628: 22 of cap free
Amount of items: 2
Items: 
Size: 772073 Color: 2
Size: 227906 Color: 0

Bin 1629: 22 of cap free
Amount of items: 2
Items: 
Size: 502003 Color: 0
Size: 497976 Color: 3

Bin 1630: 22 of cap free
Amount of items: 2
Items: 
Size: 780033 Color: 2
Size: 219946 Color: 4

Bin 1631: 22 of cap free
Amount of items: 2
Items: 
Size: 533949 Color: 4
Size: 466030 Color: 3

Bin 1632: 22 of cap free
Amount of items: 3
Items: 
Size: 386457 Color: 4
Size: 340634 Color: 2
Size: 272888 Color: 0

Bin 1633: 22 of cap free
Amount of items: 2
Items: 
Size: 538261 Color: 3
Size: 461718 Color: 4

Bin 1634: 22 of cap free
Amount of items: 2
Items: 
Size: 559962 Color: 2
Size: 440017 Color: 3

Bin 1635: 22 of cap free
Amount of items: 3
Items: 
Size: 361736 Color: 4
Size: 343595 Color: 2
Size: 294648 Color: 2

Bin 1636: 23 of cap free
Amount of items: 3
Items: 
Size: 426505 Color: 1
Size: 311132 Color: 3
Size: 262341 Color: 3

Bin 1637: 23 of cap free
Amount of items: 3
Items: 
Size: 729687 Color: 1
Size: 135597 Color: 2
Size: 134694 Color: 2

Bin 1638: 23 of cap free
Amount of items: 3
Items: 
Size: 376158 Color: 1
Size: 324261 Color: 2
Size: 299559 Color: 3

Bin 1639: 23 of cap free
Amount of items: 2
Items: 
Size: 500842 Color: 2
Size: 499136 Color: 0

Bin 1640: 23 of cap free
Amount of items: 2
Items: 
Size: 502425 Color: 2
Size: 497553 Color: 1

Bin 1641: 23 of cap free
Amount of items: 2
Items: 
Size: 527234 Color: 0
Size: 472744 Color: 1

Bin 1642: 23 of cap free
Amount of items: 2
Items: 
Size: 541632 Color: 2
Size: 458346 Color: 4

Bin 1643: 23 of cap free
Amount of items: 2
Items: 
Size: 542439 Color: 4
Size: 457539 Color: 1

Bin 1644: 23 of cap free
Amount of items: 2
Items: 
Size: 562038 Color: 1
Size: 437940 Color: 3

Bin 1645: 23 of cap free
Amount of items: 2
Items: 
Size: 597731 Color: 0
Size: 402247 Color: 3

Bin 1646: 23 of cap free
Amount of items: 2
Items: 
Size: 623308 Color: 1
Size: 376670 Color: 4

Bin 1647: 23 of cap free
Amount of items: 2
Items: 
Size: 629371 Color: 4
Size: 370607 Color: 2

Bin 1648: 23 of cap free
Amount of items: 2
Items: 
Size: 632245 Color: 4
Size: 367733 Color: 2

Bin 1649: 23 of cap free
Amount of items: 2
Items: 
Size: 671272 Color: 1
Size: 328706 Color: 4

Bin 1650: 23 of cap free
Amount of items: 2
Items: 
Size: 679707 Color: 0
Size: 320271 Color: 4

Bin 1651: 23 of cap free
Amount of items: 2
Items: 
Size: 685600 Color: 1
Size: 314378 Color: 2

Bin 1652: 23 of cap free
Amount of items: 2
Items: 
Size: 713799 Color: 4
Size: 286179 Color: 2

Bin 1653: 23 of cap free
Amount of items: 2
Items: 
Size: 784317 Color: 0
Size: 215661 Color: 3

Bin 1654: 23 of cap free
Amount of items: 2
Items: 
Size: 526346 Color: 3
Size: 473632 Color: 4

Bin 1655: 23 of cap free
Amount of items: 2
Items: 
Size: 798125 Color: 2
Size: 201853 Color: 0

Bin 1656: 23 of cap free
Amount of items: 2
Items: 
Size: 553326 Color: 0
Size: 446652 Color: 4

Bin 1657: 23 of cap free
Amount of items: 2
Items: 
Size: 758466 Color: 0
Size: 241512 Color: 4

Bin 1658: 23 of cap free
Amount of items: 2
Items: 
Size: 603058 Color: 4
Size: 396920 Color: 0

Bin 1659: 23 of cap free
Amount of items: 3
Items: 
Size: 637914 Color: 1
Size: 183422 Color: 2
Size: 178642 Color: 4

Bin 1660: 23 of cap free
Amount of items: 2
Items: 
Size: 737438 Color: 0
Size: 262540 Color: 1

Bin 1661: 23 of cap free
Amount of items: 3
Items: 
Size: 591301 Color: 0
Size: 204501 Color: 1
Size: 204176 Color: 4

Bin 1662: 23 of cap free
Amount of items: 2
Items: 
Size: 651491 Color: 3
Size: 348487 Color: 4

Bin 1663: 23 of cap free
Amount of items: 3
Items: 
Size: 502888 Color: 3
Size: 251371 Color: 1
Size: 245719 Color: 1

Bin 1664: 24 of cap free
Amount of items: 3
Items: 
Size: 380994 Color: 1
Size: 342195 Color: 0
Size: 276788 Color: 2

Bin 1665: 24 of cap free
Amount of items: 2
Items: 
Size: 511067 Color: 1
Size: 488910 Color: 4

Bin 1666: 24 of cap free
Amount of items: 2
Items: 
Size: 529724 Color: 4
Size: 470253 Color: 3

Bin 1667: 24 of cap free
Amount of items: 2
Items: 
Size: 548040 Color: 0
Size: 451937 Color: 4

Bin 1668: 24 of cap free
Amount of items: 2
Items: 
Size: 583191 Color: 3
Size: 416786 Color: 1

Bin 1669: 24 of cap free
Amount of items: 3
Items: 
Size: 631836 Color: 3
Size: 184962 Color: 0
Size: 183179 Color: 0

Bin 1670: 24 of cap free
Amount of items: 2
Items: 
Size: 664418 Color: 0
Size: 335559 Color: 2

Bin 1671: 24 of cap free
Amount of items: 2
Items: 
Size: 666412 Color: 1
Size: 333565 Color: 0

Bin 1672: 24 of cap free
Amount of items: 2
Items: 
Size: 680497 Color: 2
Size: 319480 Color: 3

Bin 1673: 24 of cap free
Amount of items: 2
Items: 
Size: 701208 Color: 3
Size: 298769 Color: 0

Bin 1674: 24 of cap free
Amount of items: 2
Items: 
Size: 712678 Color: 3
Size: 287299 Color: 1

Bin 1675: 24 of cap free
Amount of items: 2
Items: 
Size: 739454 Color: 4
Size: 260523 Color: 2

Bin 1676: 24 of cap free
Amount of items: 2
Items: 
Size: 747555 Color: 3
Size: 252422 Color: 0

Bin 1677: 24 of cap free
Amount of items: 2
Items: 
Size: 762765 Color: 3
Size: 237212 Color: 4

Bin 1678: 24 of cap free
Amount of items: 2
Items: 
Size: 733921 Color: 2
Size: 266056 Color: 4

Bin 1679: 24 of cap free
Amount of items: 3
Items: 
Size: 387277 Color: 3
Size: 330389 Color: 1
Size: 282311 Color: 2

Bin 1680: 24 of cap free
Amount of items: 3
Items: 
Size: 491746 Color: 2
Size: 256099 Color: 2
Size: 252132 Color: 3

Bin 1681: 24 of cap free
Amount of items: 3
Items: 
Size: 569251 Color: 4
Size: 215522 Color: 1
Size: 215204 Color: 2

Bin 1682: 24 of cap free
Amount of items: 2
Items: 
Size: 774967 Color: 2
Size: 225010 Color: 4

Bin 1683: 24 of cap free
Amount of items: 2
Items: 
Size: 695295 Color: 3
Size: 304682 Color: 4

Bin 1684: 24 of cap free
Amount of items: 2
Items: 
Size: 721223 Color: 3
Size: 278754 Color: 0

Bin 1685: 24 of cap free
Amount of items: 2
Items: 
Size: 736929 Color: 3
Size: 263048 Color: 0

Bin 1686: 24 of cap free
Amount of items: 2
Items: 
Size: 536840 Color: 1
Size: 463137 Color: 3

Bin 1687: 24 of cap free
Amount of items: 2
Items: 
Size: 585373 Color: 2
Size: 414604 Color: 4

Bin 1688: 24 of cap free
Amount of items: 2
Items: 
Size: 725889 Color: 2
Size: 274088 Color: 0

Bin 1689: 24 of cap free
Amount of items: 2
Items: 
Size: 611863 Color: 0
Size: 388114 Color: 4

Bin 1690: 25 of cap free
Amount of items: 3
Items: 
Size: 431579 Color: 3
Size: 308932 Color: 1
Size: 259465 Color: 4

Bin 1691: 25 of cap free
Amount of items: 2
Items: 
Size: 510852 Color: 1
Size: 489124 Color: 0

Bin 1692: 25 of cap free
Amount of items: 2
Items: 
Size: 509614 Color: 0
Size: 490362 Color: 3

Bin 1693: 25 of cap free
Amount of items: 2
Items: 
Size: 525043 Color: 3
Size: 474933 Color: 2

Bin 1694: 25 of cap free
Amount of items: 2
Items: 
Size: 551699 Color: 1
Size: 448277 Color: 3

Bin 1695: 25 of cap free
Amount of items: 2
Items: 
Size: 554130 Color: 4
Size: 445846 Color: 1

Bin 1696: 25 of cap free
Amount of items: 2
Items: 
Size: 557307 Color: 4
Size: 442669 Color: 3

Bin 1697: 25 of cap free
Amount of items: 2
Items: 
Size: 561135 Color: 4
Size: 438841 Color: 1

Bin 1698: 25 of cap free
Amount of items: 2
Items: 
Size: 582258 Color: 1
Size: 417718 Color: 0

Bin 1699: 25 of cap free
Amount of items: 2
Items: 
Size: 593790 Color: 1
Size: 406186 Color: 2

Bin 1700: 25 of cap free
Amount of items: 2
Items: 
Size: 601949 Color: 1
Size: 398027 Color: 3

Bin 1701: 25 of cap free
Amount of items: 2
Items: 
Size: 610688 Color: 2
Size: 389288 Color: 1

Bin 1702: 25 of cap free
Amount of items: 2
Items: 
Size: 617723 Color: 3
Size: 382253 Color: 2

Bin 1703: 25 of cap free
Amount of items: 2
Items: 
Size: 632618 Color: 4
Size: 367358 Color: 0

Bin 1704: 25 of cap free
Amount of items: 2
Items: 
Size: 675389 Color: 2
Size: 324587 Color: 0

Bin 1705: 25 of cap free
Amount of items: 2
Items: 
Size: 686017 Color: 0
Size: 313959 Color: 4

Bin 1706: 25 of cap free
Amount of items: 2
Items: 
Size: 700000 Color: 4
Size: 299976 Color: 0

Bin 1707: 25 of cap free
Amount of items: 2
Items: 
Size: 790221 Color: 1
Size: 209755 Color: 4

Bin 1708: 25 of cap free
Amount of items: 2
Items: 
Size: 525652 Color: 1
Size: 474324 Color: 2

Bin 1709: 25 of cap free
Amount of items: 2
Items: 
Size: 682251 Color: 4
Size: 317725 Color: 3

Bin 1710: 25 of cap free
Amount of items: 2
Items: 
Size: 635252 Color: 3
Size: 364724 Color: 2

Bin 1711: 25 of cap free
Amount of items: 2
Items: 
Size: 543074 Color: 4
Size: 456902 Color: 1

Bin 1712: 25 of cap free
Amount of items: 2
Items: 
Size: 770238 Color: 1
Size: 229738 Color: 3

Bin 1713: 25 of cap free
Amount of items: 3
Items: 
Size: 710436 Color: 3
Size: 144846 Color: 2
Size: 144694 Color: 0

Bin 1714: 25 of cap free
Amount of items: 2
Items: 
Size: 556686 Color: 3
Size: 443290 Color: 0

Bin 1715: 25 of cap free
Amount of items: 3
Items: 
Size: 633430 Color: 3
Size: 186362 Color: 3
Size: 180184 Color: 0

Bin 1716: 25 of cap free
Amount of items: 2
Items: 
Size: 730798 Color: 2
Size: 269178 Color: 4

Bin 1717: 25 of cap free
Amount of items: 2
Items: 
Size: 742377 Color: 2
Size: 257599 Color: 3

Bin 1718: 25 of cap free
Amount of items: 2
Items: 
Size: 584950 Color: 0
Size: 415026 Color: 3

Bin 1719: 25 of cap free
Amount of items: 2
Items: 
Size: 765828 Color: 0
Size: 234148 Color: 2

Bin 1720: 25 of cap free
Amount of items: 2
Items: 
Size: 653912 Color: 3
Size: 346064 Color: 4

Bin 1721: 25 of cap free
Amount of items: 2
Items: 
Size: 767846 Color: 4
Size: 232130 Color: 2

Bin 1722: 26 of cap free
Amount of items: 3
Items: 
Size: 608917 Color: 0
Size: 196700 Color: 4
Size: 194358 Color: 3

Bin 1723: 26 of cap free
Amount of items: 2
Items: 
Size: 501459 Color: 4
Size: 498516 Color: 3

Bin 1724: 26 of cap free
Amount of items: 2
Items: 
Size: 532779 Color: 0
Size: 467196 Color: 4

Bin 1725: 26 of cap free
Amount of items: 2
Items: 
Size: 540200 Color: 2
Size: 459775 Color: 1

Bin 1726: 26 of cap free
Amount of items: 2
Items: 
Size: 547276 Color: 2
Size: 452699 Color: 4

Bin 1727: 26 of cap free
Amount of items: 2
Items: 
Size: 564290 Color: 0
Size: 435685 Color: 1

Bin 1728: 26 of cap free
Amount of items: 2
Items: 
Size: 579264 Color: 2
Size: 420711 Color: 4

Bin 1729: 26 of cap free
Amount of items: 2
Items: 
Size: 609593 Color: 3
Size: 390382 Color: 2

Bin 1730: 26 of cap free
Amount of items: 2
Items: 
Size: 645827 Color: 2
Size: 354148 Color: 3

Bin 1731: 26 of cap free
Amount of items: 2
Items: 
Size: 699550 Color: 2
Size: 300425 Color: 1

Bin 1732: 26 of cap free
Amount of items: 2
Items: 
Size: 706727 Color: 2
Size: 293248 Color: 3

Bin 1733: 26 of cap free
Amount of items: 2
Items: 
Size: 717502 Color: 2
Size: 282473 Color: 4

Bin 1734: 26 of cap free
Amount of items: 2
Items: 
Size: 725990 Color: 1
Size: 273985 Color: 0

Bin 1735: 26 of cap free
Amount of items: 2
Items: 
Size: 756782 Color: 3
Size: 243193 Color: 1

Bin 1736: 26 of cap free
Amount of items: 2
Items: 
Size: 757740 Color: 3
Size: 242235 Color: 1

Bin 1737: 26 of cap free
Amount of items: 2
Items: 
Size: 772844 Color: 0
Size: 227131 Color: 1

Bin 1738: 26 of cap free
Amount of items: 2
Items: 
Size: 753950 Color: 2
Size: 246025 Color: 3

Bin 1739: 26 of cap free
Amount of items: 2
Items: 
Size: 742301 Color: 0
Size: 257674 Color: 3

Bin 1740: 26 of cap free
Amount of items: 2
Items: 
Size: 708529 Color: 1
Size: 291446 Color: 3

Bin 1741: 26 of cap free
Amount of items: 2
Items: 
Size: 664302 Color: 1
Size: 335673 Color: 4

Bin 1742: 26 of cap free
Amount of items: 2
Items: 
Size: 640558 Color: 4
Size: 359417 Color: 1

Bin 1743: 26 of cap free
Amount of items: 2
Items: 
Size: 770156 Color: 0
Size: 229819 Color: 4

Bin 1744: 26 of cap free
Amount of items: 3
Items: 
Size: 358325 Color: 4
Size: 339116 Color: 2
Size: 302534 Color: 2

Bin 1745: 26 of cap free
Amount of items: 3
Items: 
Size: 338467 Color: 3
Size: 338377 Color: 3
Size: 323131 Color: 0

Bin 1746: 26 of cap free
Amount of items: 3
Items: 
Size: 338405 Color: 3
Size: 336170 Color: 3
Size: 325400 Color: 0

Bin 1747: 27 of cap free
Amount of items: 2
Items: 
Size: 505593 Color: 0
Size: 494381 Color: 1

Bin 1748: 27 of cap free
Amount of items: 2
Items: 
Size: 510281 Color: 2
Size: 489693 Color: 3

Bin 1749: 27 of cap free
Amount of items: 2
Items: 
Size: 515512 Color: 2
Size: 484462 Color: 0

Bin 1750: 27 of cap free
Amount of items: 2
Items: 
Size: 527133 Color: 0
Size: 472841 Color: 3

Bin 1751: 27 of cap free
Amount of items: 2
Items: 
Size: 532700 Color: 4
Size: 467274 Color: 2

Bin 1752: 27 of cap free
Amount of items: 2
Items: 
Size: 547501 Color: 0
Size: 452473 Color: 4

Bin 1753: 27 of cap free
Amount of items: 2
Items: 
Size: 579715 Color: 2
Size: 420259 Color: 4

Bin 1754: 27 of cap free
Amount of items: 2
Items: 
Size: 586698 Color: 0
Size: 413276 Color: 3

Bin 1755: 27 of cap free
Amount of items: 2
Items: 
Size: 597137 Color: 1
Size: 402837 Color: 2

Bin 1756: 27 of cap free
Amount of items: 2
Items: 
Size: 599556 Color: 1
Size: 400418 Color: 2

Bin 1757: 27 of cap free
Amount of items: 2
Items: 
Size: 599699 Color: 2
Size: 400275 Color: 4

Bin 1758: 27 of cap free
Amount of items: 2
Items: 
Size: 641567 Color: 2
Size: 358407 Color: 4

Bin 1759: 27 of cap free
Amount of items: 2
Items: 
Size: 700604 Color: 0
Size: 299370 Color: 1

Bin 1760: 27 of cap free
Amount of items: 3
Items: 
Size: 701921 Color: 4
Size: 149310 Color: 3
Size: 148743 Color: 0

Bin 1761: 27 of cap free
Amount of items: 2
Items: 
Size: 706305 Color: 1
Size: 293669 Color: 2

Bin 1762: 27 of cap free
Amount of items: 2
Items: 
Size: 793056 Color: 0
Size: 206918 Color: 2

Bin 1763: 27 of cap free
Amount of items: 3
Items: 
Size: 613284 Color: 2
Size: 194092 Color: 0
Size: 192598 Color: 3

Bin 1764: 27 of cap free
Amount of items: 3
Items: 
Size: 615861 Color: 4
Size: 195773 Color: 2
Size: 188340 Color: 3

Bin 1765: 27 of cap free
Amount of items: 3
Items: 
Size: 386219 Color: 1
Size: 352896 Color: 2
Size: 260859 Color: 4

Bin 1766: 27 of cap free
Amount of items: 2
Items: 
Size: 548825 Color: 4
Size: 451149 Color: 3

Bin 1767: 27 of cap free
Amount of items: 2
Items: 
Size: 532300 Color: 2
Size: 467674 Color: 4

Bin 1768: 27 of cap free
Amount of items: 2
Items: 
Size: 505309 Color: 1
Size: 494665 Color: 0

Bin 1769: 27 of cap free
Amount of items: 2
Items: 
Size: 792746 Color: 1
Size: 207228 Color: 2

Bin 1770: 27 of cap free
Amount of items: 2
Items: 
Size: 706867 Color: 4
Size: 293107 Color: 2

Bin 1771: 27 of cap free
Amount of items: 2
Items: 
Size: 666976 Color: 4
Size: 332998 Color: 1

Bin 1772: 27 of cap free
Amount of items: 2
Items: 
Size: 549377 Color: 1
Size: 450597 Color: 2

Bin 1773: 27 of cap free
Amount of items: 3
Items: 
Size: 365798 Color: 2
Size: 353081 Color: 0
Size: 281095 Color: 1

Bin 1774: 28 of cap free
Amount of items: 3
Items: 
Size: 606401 Color: 3
Size: 196808 Color: 3
Size: 196764 Color: 2

Bin 1775: 28 of cap free
Amount of items: 3
Items: 
Size: 369447 Color: 2
Size: 333040 Color: 0
Size: 297486 Color: 3

Bin 1776: 28 of cap free
Amount of items: 2
Items: 
Size: 522247 Color: 3
Size: 477726 Color: 4

Bin 1777: 28 of cap free
Amount of items: 2
Items: 
Size: 526300 Color: 2
Size: 473673 Color: 4

Bin 1778: 28 of cap free
Amount of items: 2
Items: 
Size: 527078 Color: 0
Size: 472895 Color: 4

Bin 1779: 28 of cap free
Amount of items: 2
Items: 
Size: 540050 Color: 0
Size: 459923 Color: 2

Bin 1780: 28 of cap free
Amount of items: 2
Items: 
Size: 548968 Color: 2
Size: 451005 Color: 1

Bin 1781: 28 of cap free
Amount of items: 2
Items: 
Size: 568703 Color: 4
Size: 431270 Color: 0

Bin 1782: 28 of cap free
Amount of items: 2
Items: 
Size: 571756 Color: 2
Size: 428217 Color: 0

Bin 1783: 28 of cap free
Amount of items: 2
Items: 
Size: 581988 Color: 3
Size: 417985 Color: 4

Bin 1784: 28 of cap free
Amount of items: 2
Items: 
Size: 590042 Color: 1
Size: 409931 Color: 4

Bin 1785: 28 of cap free
Amount of items: 3
Items: 
Size: 602452 Color: 0
Size: 199401 Color: 3
Size: 198120 Color: 2

Bin 1786: 28 of cap free
Amount of items: 2
Items: 
Size: 664532 Color: 0
Size: 335441 Color: 4

Bin 1787: 28 of cap free
Amount of items: 2
Items: 
Size: 676792 Color: 1
Size: 323181 Color: 4

Bin 1788: 28 of cap free
Amount of items: 2
Items: 
Size: 698050 Color: 0
Size: 301923 Color: 4

Bin 1789: 28 of cap free
Amount of items: 3
Items: 
Size: 702368 Color: 3
Size: 148810 Color: 2
Size: 148795 Color: 0

Bin 1790: 28 of cap free
Amount of items: 2
Items: 
Size: 702114 Color: 4
Size: 297859 Color: 1

Bin 1791: 28 of cap free
Amount of items: 2
Items: 
Size: 724641 Color: 1
Size: 275332 Color: 2

Bin 1792: 28 of cap free
Amount of items: 2
Items: 
Size: 729069 Color: 3
Size: 270904 Color: 1

Bin 1793: 28 of cap free
Amount of items: 2
Items: 
Size: 735478 Color: 3
Size: 264495 Color: 4

Bin 1794: 28 of cap free
Amount of items: 2
Items: 
Size: 744653 Color: 1
Size: 255320 Color: 0

Bin 1795: 28 of cap free
Amount of items: 2
Items: 
Size: 797420 Color: 3
Size: 202553 Color: 1

Bin 1796: 28 of cap free
Amount of items: 2
Items: 
Size: 729595 Color: 2
Size: 270378 Color: 0

Bin 1797: 28 of cap free
Amount of items: 2
Items: 
Size: 506333 Color: 3
Size: 493640 Color: 0

Bin 1798: 28 of cap free
Amount of items: 3
Items: 
Size: 364680 Color: 3
Size: 350030 Color: 0
Size: 285263 Color: 1

Bin 1799: 29 of cap free
Amount of items: 2
Items: 
Size: 623651 Color: 0
Size: 376321 Color: 2

Bin 1800: 29 of cap free
Amount of items: 3
Items: 
Size: 735445 Color: 2
Size: 137473 Color: 1
Size: 127054 Color: 2

Bin 1801: 29 of cap free
Amount of items: 3
Items: 
Size: 602380 Color: 0
Size: 199149 Color: 4
Size: 198443 Color: 3

Bin 1802: 29 of cap free
Amount of items: 2
Items: 
Size: 501617 Color: 1
Size: 498355 Color: 4

Bin 1803: 29 of cap free
Amount of items: 2
Items: 
Size: 507662 Color: 0
Size: 492310 Color: 4

Bin 1804: 29 of cap free
Amount of items: 2
Items: 
Size: 514919 Color: 4
Size: 485053 Color: 0

Bin 1805: 29 of cap free
Amount of items: 2
Items: 
Size: 523531 Color: 4
Size: 476441 Color: 0

Bin 1806: 29 of cap free
Amount of items: 2
Items: 
Size: 540476 Color: 1
Size: 459496 Color: 3

Bin 1807: 29 of cap free
Amount of items: 2
Items: 
Size: 570253 Color: 0
Size: 429719 Color: 4

Bin 1808: 29 of cap free
Amount of items: 2
Items: 
Size: 577697 Color: 1
Size: 422275 Color: 4

Bin 1809: 29 of cap free
Amount of items: 2
Items: 
Size: 594719 Color: 2
Size: 405253 Color: 4

Bin 1810: 29 of cap free
Amount of items: 2
Items: 
Size: 598002 Color: 0
Size: 401970 Color: 4

Bin 1811: 29 of cap free
Amount of items: 2
Items: 
Size: 627062 Color: 4
Size: 372910 Color: 1

Bin 1812: 29 of cap free
Amount of items: 2
Items: 
Size: 629969 Color: 4
Size: 370003 Color: 1

Bin 1813: 29 of cap free
Amount of items: 2
Items: 
Size: 642410 Color: 0
Size: 357562 Color: 1

Bin 1814: 29 of cap free
Amount of items: 2
Items: 
Size: 674246 Color: 3
Size: 325726 Color: 0

Bin 1815: 29 of cap free
Amount of items: 2
Items: 
Size: 692842 Color: 1
Size: 307130 Color: 2

Bin 1816: 29 of cap free
Amount of items: 2
Items: 
Size: 718796 Color: 4
Size: 281176 Color: 0

Bin 1817: 29 of cap free
Amount of items: 2
Items: 
Size: 721275 Color: 4
Size: 278697 Color: 1

Bin 1818: 29 of cap free
Amount of items: 2
Items: 
Size: 759744 Color: 1
Size: 240228 Color: 4

Bin 1819: 29 of cap free
Amount of items: 2
Items: 
Size: 760190 Color: 0
Size: 239782 Color: 2

Bin 1820: 29 of cap free
Amount of items: 2
Items: 
Size: 799003 Color: 4
Size: 200969 Color: 0

Bin 1821: 29 of cap free
Amount of items: 2
Items: 
Size: 793183 Color: 1
Size: 206789 Color: 2

Bin 1822: 29 of cap free
Amount of items: 2
Items: 
Size: 764577 Color: 3
Size: 235395 Color: 2

Bin 1823: 29 of cap free
Amount of items: 2
Items: 
Size: 558617 Color: 2
Size: 441355 Color: 0

Bin 1824: 29 of cap free
Amount of items: 2
Items: 
Size: 656037 Color: 2
Size: 343935 Color: 1

Bin 1825: 29 of cap free
Amount of items: 2
Items: 
Size: 716700 Color: 3
Size: 283272 Color: 1

Bin 1826: 29 of cap free
Amount of items: 2
Items: 
Size: 675283 Color: 3
Size: 324689 Color: 0

Bin 1827: 29 of cap free
Amount of items: 3
Items: 
Size: 514193 Color: 1
Size: 252207 Color: 4
Size: 233572 Color: 1

Bin 1828: 29 of cap free
Amount of items: 2
Items: 
Size: 795354 Color: 3
Size: 204618 Color: 1

Bin 1829: 29 of cap free
Amount of items: 2
Items: 
Size: 737091 Color: 1
Size: 262881 Color: 2

Bin 1830: 29 of cap free
Amount of items: 2
Items: 
Size: 749447 Color: 3
Size: 250525 Color: 0

Bin 1831: 29 of cap free
Amount of items: 3
Items: 
Size: 361760 Color: 1
Size: 349974 Color: 0
Size: 288238 Color: 4

Bin 1832: 30 of cap free
Amount of items: 3
Items: 
Size: 377835 Color: 1
Size: 313281 Color: 3
Size: 308855 Color: 4

Bin 1833: 30 of cap free
Amount of items: 2
Items: 
Size: 514372 Color: 3
Size: 485599 Color: 0

Bin 1834: 30 of cap free
Amount of items: 2
Items: 
Size: 520588 Color: 2
Size: 479383 Color: 0

Bin 1835: 30 of cap free
Amount of items: 2
Items: 
Size: 551164 Color: 3
Size: 448807 Color: 1

Bin 1836: 30 of cap free
Amount of items: 2
Items: 
Size: 567765 Color: 4
Size: 432206 Color: 2

Bin 1837: 30 of cap free
Amount of items: 2
Items: 
Size: 602669 Color: 2
Size: 397302 Color: 3

Bin 1838: 30 of cap free
Amount of items: 2
Items: 
Size: 611936 Color: 1
Size: 388035 Color: 4

Bin 1839: 30 of cap free
Amount of items: 2
Items: 
Size: 618807 Color: 4
Size: 381164 Color: 1

Bin 1840: 30 of cap free
Amount of items: 2
Items: 
Size: 630383 Color: 2
Size: 369588 Color: 0

Bin 1841: 30 of cap free
Amount of items: 2
Items: 
Size: 682876 Color: 2
Size: 317095 Color: 4

Bin 1842: 30 of cap free
Amount of items: 3
Items: 
Size: 697061 Color: 4
Size: 151728 Color: 1
Size: 151182 Color: 3

Bin 1843: 30 of cap free
Amount of items: 2
Items: 
Size: 723869 Color: 4
Size: 276102 Color: 2

Bin 1844: 30 of cap free
Amount of items: 2
Items: 
Size: 727369 Color: 2
Size: 272602 Color: 1

Bin 1845: 30 of cap free
Amount of items: 2
Items: 
Size: 760015 Color: 2
Size: 239956 Color: 0

Bin 1846: 30 of cap free
Amount of items: 3
Items: 
Size: 701763 Color: 2
Size: 149123 Color: 4
Size: 149085 Color: 3

Bin 1847: 30 of cap free
Amount of items: 2
Items: 
Size: 767361 Color: 4
Size: 232610 Color: 0

Bin 1848: 30 of cap free
Amount of items: 3
Items: 
Size: 732329 Color: 3
Size: 137000 Color: 2
Size: 130642 Color: 1

Bin 1849: 30 of cap free
Amount of items: 2
Items: 
Size: 676405 Color: 0
Size: 323566 Color: 3

Bin 1850: 30 of cap free
Amount of items: 2
Items: 
Size: 599483 Color: 3
Size: 400488 Color: 1

Bin 1851: 30 of cap free
Amount of items: 2
Items: 
Size: 542785 Color: 2
Size: 457186 Color: 0

Bin 1852: 30 of cap free
Amount of items: 2
Items: 
Size: 620018 Color: 1
Size: 379953 Color: 3

Bin 1853: 30 of cap free
Amount of items: 3
Items: 
Size: 640661 Color: 2
Size: 181688 Color: 3
Size: 177622 Color: 2

Bin 1854: 30 of cap free
Amount of items: 2
Items: 
Size: 680020 Color: 1
Size: 319951 Color: 3

Bin 1855: 30 of cap free
Amount of items: 2
Items: 
Size: 503794 Color: 1
Size: 496177 Color: 4

Bin 1856: 30 of cap free
Amount of items: 2
Items: 
Size: 757240 Color: 0
Size: 242731 Color: 2

Bin 1857: 30 of cap free
Amount of items: 3
Items: 
Size: 373598 Color: 2
Size: 331976 Color: 1
Size: 294397 Color: 4

Bin 1858: 30 of cap free
Amount of items: 3
Items: 
Size: 368379 Color: 2
Size: 316067 Color: 4
Size: 315525 Color: 4

Bin 1859: 30 of cap free
Amount of items: 2
Items: 
Size: 712311 Color: 4
Size: 287660 Color: 0

Bin 1860: 30 of cap free
Amount of items: 2
Items: 
Size: 733752 Color: 0
Size: 266219 Color: 4

Bin 1861: 30 of cap free
Amount of items: 3
Items: 
Size: 775882 Color: 3
Size: 117717 Color: 1
Size: 106372 Color: 3

Bin 1862: 31 of cap free
Amount of items: 2
Items: 
Size: 520809 Color: 2
Size: 479161 Color: 4

Bin 1863: 31 of cap free
Amount of items: 2
Items: 
Size: 525430 Color: 1
Size: 474540 Color: 3

Bin 1864: 31 of cap free
Amount of items: 2
Items: 
Size: 539476 Color: 3
Size: 460494 Color: 1

Bin 1865: 31 of cap free
Amount of items: 2
Items: 
Size: 596600 Color: 0
Size: 403370 Color: 2

Bin 1866: 31 of cap free
Amount of items: 2
Items: 
Size: 598963 Color: 1
Size: 401007 Color: 2

Bin 1867: 31 of cap free
Amount of items: 2
Items: 
Size: 612930 Color: 2
Size: 387040 Color: 4

Bin 1868: 31 of cap free
Amount of items: 2
Items: 
Size: 700899 Color: 3
Size: 299071 Color: 4

Bin 1869: 31 of cap free
Amount of items: 2
Items: 
Size: 703926 Color: 3
Size: 296044 Color: 1

Bin 1870: 31 of cap free
Amount of items: 2
Items: 
Size: 738739 Color: 0
Size: 261231 Color: 2

Bin 1871: 31 of cap free
Amount of items: 2
Items: 
Size: 742785 Color: 1
Size: 257185 Color: 3

Bin 1872: 31 of cap free
Amount of items: 2
Items: 
Size: 754001 Color: 3
Size: 245969 Color: 1

Bin 1873: 31 of cap free
Amount of items: 2
Items: 
Size: 761037 Color: 3
Size: 238933 Color: 1

Bin 1874: 31 of cap free
Amount of items: 2
Items: 
Size: 762115 Color: 2
Size: 237855 Color: 4

Bin 1875: 31 of cap free
Amount of items: 2
Items: 
Size: 695860 Color: 4
Size: 304110 Color: 1

Bin 1876: 31 of cap free
Amount of items: 3
Items: 
Size: 400647 Color: 2
Size: 329889 Color: 2
Size: 269434 Color: 1

Bin 1877: 31 of cap free
Amount of items: 2
Items: 
Size: 741387 Color: 0
Size: 258583 Color: 4

Bin 1878: 31 of cap free
Amount of items: 2
Items: 
Size: 601002 Color: 2
Size: 398968 Color: 0

Bin 1879: 31 of cap free
Amount of items: 2
Items: 
Size: 520370 Color: 1
Size: 479600 Color: 2

Bin 1880: 31 of cap free
Amount of items: 2
Items: 
Size: 649196 Color: 4
Size: 350774 Color: 2

Bin 1881: 31 of cap free
Amount of items: 2
Items: 
Size: 694924 Color: 0
Size: 305046 Color: 4

Bin 1882: 31 of cap free
Amount of items: 3
Items: 
Size: 505992 Color: 0
Size: 247270 Color: 4
Size: 246708 Color: 1

Bin 1883: 31 of cap free
Amount of items: 2
Items: 
Size: 699707 Color: 1
Size: 300263 Color: 0

Bin 1884: 31 of cap free
Amount of items: 2
Items: 
Size: 762980 Color: 0
Size: 236990 Color: 1

Bin 1885: 32 of cap free
Amount of items: 2
Items: 
Size: 517771 Color: 0
Size: 482198 Color: 2

Bin 1886: 32 of cap free
Amount of items: 2
Items: 
Size: 564503 Color: 0
Size: 435466 Color: 2

Bin 1887: 32 of cap free
Amount of items: 2
Items: 
Size: 565975 Color: 3
Size: 433994 Color: 0

Bin 1888: 32 of cap free
Amount of items: 2
Items: 
Size: 572799 Color: 2
Size: 427170 Color: 4

Bin 1889: 32 of cap free
Amount of items: 2
Items: 
Size: 580179 Color: 4
Size: 419790 Color: 3

Bin 1890: 32 of cap free
Amount of items: 2
Items: 
Size: 586287 Color: 2
Size: 413682 Color: 0

Bin 1891: 32 of cap free
Amount of items: 2
Items: 
Size: 589004 Color: 0
Size: 410965 Color: 4

Bin 1892: 32 of cap free
Amount of items: 2
Items: 
Size: 603694 Color: 1
Size: 396275 Color: 2

Bin 1893: 32 of cap free
Amount of items: 2
Items: 
Size: 616381 Color: 3
Size: 383588 Color: 2

Bin 1894: 32 of cap free
Amount of items: 2
Items: 
Size: 682703 Color: 4
Size: 317266 Color: 3

Bin 1895: 32 of cap free
Amount of items: 2
Items: 
Size: 706041 Color: 1
Size: 293928 Color: 2

Bin 1896: 32 of cap free
Amount of items: 2
Items: 
Size: 719684 Color: 4
Size: 280285 Color: 2

Bin 1897: 32 of cap free
Amount of items: 2
Items: 
Size: 747380 Color: 0
Size: 252589 Color: 3

Bin 1898: 32 of cap free
Amount of items: 3
Items: 
Size: 648457 Color: 3
Size: 176461 Color: 4
Size: 175051 Color: 3

Bin 1899: 32 of cap free
Amount of items: 2
Items: 
Size: 719679 Color: 0
Size: 280290 Color: 4

Bin 1900: 32 of cap free
Amount of items: 2
Items: 
Size: 557373 Color: 0
Size: 442596 Color: 4

Bin 1901: 32 of cap free
Amount of items: 2
Items: 
Size: 762164 Color: 2
Size: 237805 Color: 0

Bin 1902: 32 of cap free
Amount of items: 3
Items: 
Size: 657538 Color: 1
Size: 172018 Color: 0
Size: 170413 Color: 4

Bin 1903: 32 of cap free
Amount of items: 2
Items: 
Size: 743415 Color: 3
Size: 256554 Color: 1

Bin 1904: 32 of cap free
Amount of items: 2
Items: 
Size: 762245 Color: 2
Size: 237724 Color: 0

Bin 1905: 32 of cap free
Amount of items: 2
Items: 
Size: 622916 Color: 4
Size: 377053 Color: 1

Bin 1906: 32 of cap free
Amount of items: 2
Items: 
Size: 707503 Color: 4
Size: 292466 Color: 0

Bin 1907: 32 of cap free
Amount of items: 2
Items: 
Size: 654350 Color: 2
Size: 345619 Color: 0

Bin 1908: 33 of cap free
Amount of items: 2
Items: 
Size: 537707 Color: 2
Size: 462261 Color: 1

Bin 1909: 33 of cap free
Amount of items: 2
Items: 
Size: 724080 Color: 3
Size: 275888 Color: 1

Bin 1910: 33 of cap free
Amount of items: 2
Items: 
Size: 646017 Color: 4
Size: 353951 Color: 0

Bin 1911: 33 of cap free
Amount of items: 2
Items: 
Size: 530566 Color: 3
Size: 469402 Color: 4

Bin 1912: 33 of cap free
Amount of items: 2
Items: 
Size: 532350 Color: 0
Size: 467618 Color: 3

Bin 1913: 33 of cap free
Amount of items: 2
Items: 
Size: 537594 Color: 4
Size: 462374 Color: 3

Bin 1914: 33 of cap free
Amount of items: 2
Items: 
Size: 541156 Color: 2
Size: 458812 Color: 3

Bin 1915: 33 of cap free
Amount of items: 2
Items: 
Size: 567193 Color: 3
Size: 432775 Color: 1

Bin 1916: 33 of cap free
Amount of items: 2
Items: 
Size: 572682 Color: 1
Size: 427286 Color: 2

Bin 1917: 33 of cap free
Amount of items: 2
Items: 
Size: 592437 Color: 1
Size: 407531 Color: 2

Bin 1918: 33 of cap free
Amount of items: 2
Items: 
Size: 632356 Color: 0
Size: 367612 Color: 2

Bin 1919: 33 of cap free
Amount of items: 2
Items: 
Size: 678626 Color: 1
Size: 321342 Color: 0

Bin 1920: 33 of cap free
Amount of items: 3
Items: 
Size: 683848 Color: 3
Size: 158119 Color: 1
Size: 158001 Color: 2

Bin 1921: 33 of cap free
Amount of items: 2
Items: 
Size: 690942 Color: 1
Size: 309026 Color: 4

Bin 1922: 33 of cap free
Amount of items: 2
Items: 
Size: 722224 Color: 0
Size: 277744 Color: 4

Bin 1923: 33 of cap free
Amount of items: 2
Items: 
Size: 754716 Color: 0
Size: 245252 Color: 4

Bin 1924: 33 of cap free
Amount of items: 2
Items: 
Size: 777932 Color: 4
Size: 222036 Color: 0

Bin 1925: 33 of cap free
Amount of items: 2
Items: 
Size: 787112 Color: 0
Size: 212856 Color: 3

Bin 1926: 33 of cap free
Amount of items: 2
Items: 
Size: 788920 Color: 4
Size: 211048 Color: 0

Bin 1927: 33 of cap free
Amount of items: 3
Items: 
Size: 700912 Color: 4
Size: 150848 Color: 4
Size: 148208 Color: 1

Bin 1928: 33 of cap free
Amount of items: 2
Items: 
Size: 711520 Color: 0
Size: 288448 Color: 1

Bin 1929: 33 of cap free
Amount of items: 2
Items: 
Size: 635986 Color: 2
Size: 363982 Color: 3

Bin 1930: 33 of cap free
Amount of items: 2
Items: 
Size: 652936 Color: 2
Size: 347032 Color: 1

Bin 1931: 33 of cap free
Amount of items: 2
Items: 
Size: 501307 Color: 2
Size: 498661 Color: 3

Bin 1932: 33 of cap free
Amount of items: 2
Items: 
Size: 589131 Color: 3
Size: 410837 Color: 0

Bin 1933: 33 of cap free
Amount of items: 2
Items: 
Size: 769770 Color: 2
Size: 230198 Color: 0

Bin 1934: 33 of cap free
Amount of items: 2
Items: 
Size: 598509 Color: 1
Size: 401459 Color: 0

Bin 1935: 33 of cap free
Amount of items: 2
Items: 
Size: 625407 Color: 1
Size: 374561 Color: 4

Bin 1936: 33 of cap free
Amount of items: 2
Items: 
Size: 571394 Color: 3
Size: 428574 Color: 0

Bin 1937: 33 of cap free
Amount of items: 2
Items: 
Size: 774446 Color: 3
Size: 225522 Color: 2

Bin 1938: 33 of cap free
Amount of items: 2
Items: 
Size: 570598 Color: 2
Size: 429370 Color: 3

Bin 1939: 33 of cap free
Amount of items: 2
Items: 
Size: 692511 Color: 4
Size: 307457 Color: 1

Bin 1940: 33 of cap free
Amount of items: 2
Items: 
Size: 740948 Color: 4
Size: 259020 Color: 1

Bin 1941: 33 of cap free
Amount of items: 2
Items: 
Size: 552577 Color: 2
Size: 447391 Color: 3

Bin 1942: 33 of cap free
Amount of items: 2
Items: 
Size: 609610 Color: 2
Size: 390358 Color: 3

Bin 1943: 34 of cap free
Amount of items: 2
Items: 
Size: 531671 Color: 2
Size: 468296 Color: 4

Bin 1944: 34 of cap free
Amount of items: 2
Items: 
Size: 532049 Color: 4
Size: 467918 Color: 1

Bin 1945: 34 of cap free
Amount of items: 2
Items: 
Size: 559781 Color: 3
Size: 440186 Color: 2

Bin 1946: 34 of cap free
Amount of items: 2
Items: 
Size: 560614 Color: 3
Size: 439353 Color: 2

Bin 1947: 34 of cap free
Amount of items: 2
Items: 
Size: 580948 Color: 2
Size: 419019 Color: 3

Bin 1948: 34 of cap free
Amount of items: 2
Items: 
Size: 582918 Color: 4
Size: 417049 Color: 0

Bin 1949: 34 of cap free
Amount of items: 2
Items: 
Size: 605041 Color: 4
Size: 394926 Color: 2

Bin 1950: 34 of cap free
Amount of items: 2
Items: 
Size: 628418 Color: 4
Size: 371549 Color: 0

Bin 1951: 34 of cap free
Amount of items: 2
Items: 
Size: 646146 Color: 4
Size: 353821 Color: 0

Bin 1952: 34 of cap free
Amount of items: 2
Items: 
Size: 651689 Color: 1
Size: 348278 Color: 4

Bin 1953: 34 of cap free
Amount of items: 2
Items: 
Size: 672847 Color: 1
Size: 327120 Color: 4

Bin 1954: 34 of cap free
Amount of items: 2
Items: 
Size: 737497 Color: 3
Size: 262470 Color: 1

Bin 1955: 34 of cap free
Amount of items: 2
Items: 
Size: 571521 Color: 0
Size: 428446 Color: 4

Bin 1956: 34 of cap free
Amount of items: 3
Items: 
Size: 587916 Color: 0
Size: 206075 Color: 2
Size: 205976 Color: 2

Bin 1957: 34 of cap free
Amount of items: 3
Items: 
Size: 574463 Color: 1
Size: 216319 Color: 4
Size: 209185 Color: 4

Bin 1958: 34 of cap free
Amount of items: 2
Items: 
Size: 697261 Color: 2
Size: 302706 Color: 1

Bin 1959: 34 of cap free
Amount of items: 2
Items: 
Size: 669907 Color: 0
Size: 330060 Color: 2

Bin 1960: 34 of cap free
Amount of items: 2
Items: 
Size: 637579 Color: 4
Size: 362388 Color: 3

Bin 1961: 34 of cap free
Amount of items: 2
Items: 
Size: 759315 Color: 4
Size: 240652 Color: 0

Bin 1962: 34 of cap free
Amount of items: 2
Items: 
Size: 571849 Color: 3
Size: 428118 Color: 4

Bin 1963: 34 of cap free
Amount of items: 2
Items: 
Size: 576589 Color: 4
Size: 423378 Color: 1

Bin 1964: 34 of cap free
Amount of items: 2
Items: 
Size: 676326 Color: 2
Size: 323641 Color: 3

Bin 1965: 34 of cap free
Amount of items: 2
Items: 
Size: 584489 Color: 4
Size: 415478 Color: 0

Bin 1966: 35 of cap free
Amount of items: 2
Items: 
Size: 789956 Color: 0
Size: 210010 Color: 4

Bin 1967: 35 of cap free
Amount of items: 2
Items: 
Size: 690314 Color: 2
Size: 309652 Color: 1

Bin 1968: 35 of cap free
Amount of items: 3
Items: 
Size: 679888 Color: 0
Size: 160127 Color: 3
Size: 159951 Color: 3

Bin 1969: 35 of cap free
Amount of items: 2
Items: 
Size: 509044 Color: 4
Size: 490922 Color: 3

Bin 1970: 35 of cap free
Amount of items: 2
Items: 
Size: 510507 Color: 0
Size: 489459 Color: 3

Bin 1971: 35 of cap free
Amount of items: 2
Items: 
Size: 516643 Color: 0
Size: 483323 Color: 2

Bin 1972: 35 of cap free
Amount of items: 2
Items: 
Size: 537403 Color: 0
Size: 462563 Color: 2

Bin 1973: 35 of cap free
Amount of items: 2
Items: 
Size: 559140 Color: 4
Size: 440826 Color: 3

Bin 1974: 35 of cap free
Amount of items: 2
Items: 
Size: 560093 Color: 1
Size: 439873 Color: 3

Bin 1975: 35 of cap free
Amount of items: 2
Items: 
Size: 564823 Color: 4
Size: 435143 Color: 1

Bin 1976: 35 of cap free
Amount of items: 2
Items: 
Size: 582119 Color: 0
Size: 417847 Color: 3

Bin 1977: 35 of cap free
Amount of items: 2
Items: 
Size: 588142 Color: 1
Size: 411824 Color: 2

Bin 1978: 35 of cap free
Amount of items: 2
Items: 
Size: 641751 Color: 4
Size: 358215 Color: 0

Bin 1979: 35 of cap free
Amount of items: 2
Items: 
Size: 650060 Color: 4
Size: 349906 Color: 0

Bin 1980: 35 of cap free
Amount of items: 2
Items: 
Size: 652825 Color: 0
Size: 347141 Color: 4

Bin 1981: 35 of cap free
Amount of items: 2
Items: 
Size: 656091 Color: 2
Size: 343875 Color: 4

Bin 1982: 35 of cap free
Amount of items: 2
Items: 
Size: 660536 Color: 1
Size: 339430 Color: 0

Bin 1983: 35 of cap free
Amount of items: 2
Items: 
Size: 666889 Color: 2
Size: 333077 Color: 3

Bin 1984: 35 of cap free
Amount of items: 2
Items: 
Size: 680629 Color: 4
Size: 319337 Color: 0

Bin 1985: 35 of cap free
Amount of items: 2
Items: 
Size: 689774 Color: 1
Size: 310192 Color: 0

Bin 1986: 35 of cap free
Amount of items: 2
Items: 
Size: 703377 Color: 3
Size: 296589 Color: 2

Bin 1987: 35 of cap free
Amount of items: 2
Items: 
Size: 753138 Color: 1
Size: 246828 Color: 2

Bin 1988: 35 of cap free
Amount of items: 2
Items: 
Size: 781498 Color: 3
Size: 218468 Color: 0

Bin 1989: 35 of cap free
Amount of items: 3
Items: 
Size: 372551 Color: 2
Size: 314586 Color: 3
Size: 312829 Color: 3

Bin 1990: 35 of cap free
Amount of items: 2
Items: 
Size: 776368 Color: 4
Size: 223598 Color: 3

Bin 1991: 35 of cap free
Amount of items: 2
Items: 
Size: 508641 Color: 2
Size: 491325 Color: 4

Bin 1992: 35 of cap free
Amount of items: 2
Items: 
Size: 719918 Color: 3
Size: 280048 Color: 2

Bin 1993: 35 of cap free
Amount of items: 2
Items: 
Size: 698183 Color: 1
Size: 301783 Color: 3

Bin 1994: 35 of cap free
Amount of items: 2
Items: 
Size: 591876 Color: 3
Size: 408090 Color: 2

Bin 1995: 35 of cap free
Amount of items: 2
Items: 
Size: 627609 Color: 0
Size: 372357 Color: 3

Bin 1996: 35 of cap free
Amount of items: 2
Items: 
Size: 635790 Color: 2
Size: 364176 Color: 1

Bin 1997: 36 of cap free
Amount of items: 2
Items: 
Size: 506330 Color: 0
Size: 493635 Color: 3

Bin 1998: 36 of cap free
Amount of items: 2
Items: 
Size: 569065 Color: 3
Size: 430900 Color: 1

Bin 1999: 36 of cap free
Amount of items: 2
Items: 
Size: 573856 Color: 0
Size: 426109 Color: 4

Bin 2000: 36 of cap free
Amount of items: 2
Items: 
Size: 572497 Color: 4
Size: 427468 Color: 2

Bin 2001: 36 of cap free
Amount of items: 3
Items: 
Size: 602325 Color: 0
Size: 198892 Color: 3
Size: 198748 Color: 4

Bin 2002: 36 of cap free
Amount of items: 2
Items: 
Size: 628670 Color: 4
Size: 371295 Color: 2

Bin 2003: 36 of cap free
Amount of items: 2
Items: 
Size: 652502 Color: 4
Size: 347463 Color: 0

Bin 2004: 36 of cap free
Amount of items: 2
Items: 
Size: 702538 Color: 1
Size: 297427 Color: 2

Bin 2005: 36 of cap free
Amount of items: 2
Items: 
Size: 749855 Color: 0
Size: 250110 Color: 1

Bin 2006: 36 of cap free
Amount of items: 2
Items: 
Size: 669442 Color: 2
Size: 330523 Color: 3

Bin 2007: 36 of cap free
Amount of items: 2
Items: 
Size: 704388 Color: 4
Size: 295577 Color: 0

Bin 2008: 36 of cap free
Amount of items: 2
Items: 
Size: 689323 Color: 2
Size: 310642 Color: 0

Bin 2009: 36 of cap free
Amount of items: 2
Items: 
Size: 688488 Color: 3
Size: 311477 Color: 4

Bin 2010: 36 of cap free
Amount of items: 3
Items: 
Size: 710056 Color: 0
Size: 153345 Color: 4
Size: 136564 Color: 0

Bin 2011: 36 of cap free
Amount of items: 2
Items: 
Size: 696885 Color: 1
Size: 303080 Color: 3

Bin 2012: 36 of cap free
Amount of items: 2
Items: 
Size: 776889 Color: 4
Size: 223076 Color: 3

Bin 2013: 36 of cap free
Amount of items: 2
Items: 
Size: 631311 Color: 0
Size: 368654 Color: 2

Bin 2014: 36 of cap free
Amount of items: 2
Items: 
Size: 672754 Color: 3
Size: 327211 Color: 4

Bin 2015: 36 of cap free
Amount of items: 2
Items: 
Size: 617364 Color: 1
Size: 382601 Color: 0

Bin 2016: 36 of cap free
Amount of items: 2
Items: 
Size: 606443 Color: 3
Size: 393522 Color: 0

Bin 2017: 37 of cap free
Amount of items: 2
Items: 
Size: 595536 Color: 4
Size: 404428 Color: 3

Bin 2018: 37 of cap free
Amount of items: 2
Items: 
Size: 602862 Color: 2
Size: 397102 Color: 4

Bin 2019: 37 of cap free
Amount of items: 2
Items: 
Size: 621122 Color: 2
Size: 378842 Color: 4

Bin 2020: 37 of cap free
Amount of items: 2
Items: 
Size: 638419 Color: 1
Size: 361545 Color: 3

Bin 2021: 37 of cap free
Amount of items: 2
Items: 
Size: 677539 Color: 2
Size: 322425 Color: 4

Bin 2022: 37 of cap free
Amount of items: 2
Items: 
Size: 758568 Color: 3
Size: 241396 Color: 4

Bin 2023: 37 of cap free
Amount of items: 2
Items: 
Size: 735385 Color: 1
Size: 264579 Color: 3

Bin 2024: 37 of cap free
Amount of items: 2
Items: 
Size: 638622 Color: 0
Size: 361342 Color: 2

Bin 2025: 37 of cap free
Amount of items: 2
Items: 
Size: 514572 Color: 0
Size: 485392 Color: 4

Bin 2026: 37 of cap free
Amount of items: 2
Items: 
Size: 581227 Color: 3
Size: 418737 Color: 0

Bin 2027: 37 of cap free
Amount of items: 2
Items: 
Size: 680486 Color: 0
Size: 319478 Color: 3

Bin 2028: 37 of cap free
Amount of items: 3
Items: 
Size: 659395 Color: 4
Size: 170770 Color: 0
Size: 169799 Color: 2

Bin 2029: 37 of cap free
Amount of items: 2
Items: 
Size: 600737 Color: 3
Size: 399227 Color: 0

Bin 2030: 37 of cap free
Amount of items: 2
Items: 
Size: 798065 Color: 0
Size: 201899 Color: 1

Bin 2031: 37 of cap free
Amount of items: 2
Items: 
Size: 513712 Color: 2
Size: 486252 Color: 3

Bin 2032: 37 of cap free
Amount of items: 2
Items: 
Size: 641965 Color: 0
Size: 357999 Color: 3

Bin 2033: 37 of cap free
Amount of items: 2
Items: 
Size: 718281 Color: 1
Size: 281683 Color: 3

Bin 2034: 37 of cap free
Amount of items: 2
Items: 
Size: 575055 Color: 3
Size: 424909 Color: 0

Bin 2035: 37 of cap free
Amount of items: 2
Items: 
Size: 708325 Color: 2
Size: 291639 Color: 3

Bin 2036: 38 of cap free
Amount of items: 2
Items: 
Size: 528278 Color: 1
Size: 471685 Color: 0

Bin 2037: 38 of cap free
Amount of items: 2
Items: 
Size: 682239 Color: 4
Size: 317724 Color: 2

Bin 2038: 38 of cap free
Amount of items: 2
Items: 
Size: 525642 Color: 0
Size: 474321 Color: 4

Bin 2039: 38 of cap free
Amount of items: 2
Items: 
Size: 516474 Color: 0
Size: 483489 Color: 4

Bin 2040: 38 of cap free
Amount of items: 2
Items: 
Size: 571186 Color: 3
Size: 428777 Color: 1

Bin 2041: 38 of cap free
Amount of items: 2
Items: 
Size: 576877 Color: 3
Size: 423086 Color: 1

Bin 2042: 38 of cap free
Amount of items: 2
Items: 
Size: 582403 Color: 4
Size: 417560 Color: 2

Bin 2043: 38 of cap free
Amount of items: 2
Items: 
Size: 720661 Color: 1
Size: 279302 Color: 3

Bin 2044: 38 of cap free
Amount of items: 2
Items: 
Size: 714370 Color: 3
Size: 285593 Color: 2

Bin 2045: 38 of cap free
Amount of items: 2
Items: 
Size: 760895 Color: 1
Size: 239068 Color: 4

Bin 2046: 38 of cap free
Amount of items: 3
Items: 
Size: 660979 Color: 1
Size: 170937 Color: 3
Size: 168047 Color: 1

Bin 2047: 38 of cap free
Amount of items: 2
Items: 
Size: 666410 Color: 4
Size: 333553 Color: 2

Bin 2048: 38 of cap free
Amount of items: 2
Items: 
Size: 720803 Color: 1
Size: 279160 Color: 3

Bin 2049: 38 of cap free
Amount of items: 2
Items: 
Size: 654026 Color: 3
Size: 345937 Color: 4

Bin 2050: 38 of cap free
Amount of items: 2
Items: 
Size: 732958 Color: 0
Size: 267005 Color: 3

Bin 2051: 39 of cap free
Amount of items: 2
Items: 
Size: 714652 Color: 2
Size: 285310 Color: 1

Bin 2052: 39 of cap free
Amount of items: 2
Items: 
Size: 512764 Color: 4
Size: 487198 Color: 0

Bin 2053: 39 of cap free
Amount of items: 2
Items: 
Size: 534837 Color: 3
Size: 465125 Color: 2

Bin 2054: 39 of cap free
Amount of items: 2
Items: 
Size: 550114 Color: 1
Size: 449848 Color: 2

Bin 2055: 39 of cap free
Amount of items: 2
Items: 
Size: 570439 Color: 4
Size: 429523 Color: 2

Bin 2056: 39 of cap free
Amount of items: 2
Items: 
Size: 588231 Color: 0
Size: 411731 Color: 3

Bin 2057: 39 of cap free
Amount of items: 2
Items: 
Size: 594827 Color: 3
Size: 405135 Color: 1

Bin 2058: 39 of cap free
Amount of items: 2
Items: 
Size: 595209 Color: 0
Size: 404753 Color: 4

Bin 2059: 39 of cap free
Amount of items: 2
Items: 
Size: 640044 Color: 1
Size: 359918 Color: 4

Bin 2060: 39 of cap free
Amount of items: 2
Items: 
Size: 719374 Color: 4
Size: 280588 Color: 2

Bin 2061: 39 of cap free
Amount of items: 2
Items: 
Size: 750820 Color: 4
Size: 249142 Color: 1

Bin 2062: 39 of cap free
Amount of items: 2
Items: 
Size: 772583 Color: 0
Size: 227379 Color: 1

Bin 2063: 39 of cap free
Amount of items: 2
Items: 
Size: 504413 Color: 0
Size: 495549 Color: 4

Bin 2064: 39 of cap free
Amount of items: 2
Items: 
Size: 632658 Color: 1
Size: 367304 Color: 2

Bin 2065: 39 of cap free
Amount of items: 2
Items: 
Size: 788631 Color: 0
Size: 211331 Color: 2

Bin 2066: 39 of cap free
Amount of items: 2
Items: 
Size: 599978 Color: 0
Size: 399984 Color: 3

Bin 2067: 39 of cap free
Amount of items: 3
Items: 
Size: 596172 Color: 1
Size: 202145 Color: 0
Size: 201645 Color: 3

Bin 2068: 39 of cap free
Amount of items: 2
Items: 
Size: 734336 Color: 0
Size: 265626 Color: 2

Bin 2069: 40 of cap free
Amount of items: 3
Items: 
Size: 665023 Color: 2
Size: 168353 Color: 0
Size: 166585 Color: 1

Bin 2070: 40 of cap free
Amount of items: 2
Items: 
Size: 526009 Color: 0
Size: 473952 Color: 3

Bin 2071: 40 of cap free
Amount of items: 2
Items: 
Size: 531062 Color: 2
Size: 468899 Color: 0

Bin 2072: 40 of cap free
Amount of items: 2
Items: 
Size: 556480 Color: 0
Size: 443481 Color: 1

Bin 2073: 40 of cap free
Amount of items: 2
Items: 
Size: 583216 Color: 0
Size: 416745 Color: 3

Bin 2074: 40 of cap free
Amount of items: 2
Items: 
Size: 672353 Color: 2
Size: 327608 Color: 1

Bin 2075: 40 of cap free
Amount of items: 2
Items: 
Size: 685069 Color: 0
Size: 314892 Color: 4

Bin 2076: 40 of cap free
Amount of items: 2
Items: 
Size: 711306 Color: 3
Size: 288655 Color: 4

Bin 2077: 40 of cap free
Amount of items: 2
Items: 
Size: 720533 Color: 4
Size: 279428 Color: 1

Bin 2078: 40 of cap free
Amount of items: 2
Items: 
Size: 785733 Color: 1
Size: 214228 Color: 3

Bin 2079: 40 of cap free
Amount of items: 2
Items: 
Size: 794375 Color: 0
Size: 205586 Color: 1

Bin 2080: 40 of cap free
Amount of items: 2
Items: 
Size: 774828 Color: 0
Size: 225133 Color: 2

Bin 2081: 40 of cap free
Amount of items: 2
Items: 
Size: 744162 Color: 1
Size: 255799 Color: 3

Bin 2082: 40 of cap free
Amount of items: 2
Items: 
Size: 793107 Color: 2
Size: 206854 Color: 0

Bin 2083: 40 of cap free
Amount of items: 2
Items: 
Size: 754487 Color: 2
Size: 245474 Color: 0

Bin 2084: 40 of cap free
Amount of items: 2
Items: 
Size: 502668 Color: 4
Size: 497293 Color: 0

Bin 2085: 40 of cap free
Amount of items: 2
Items: 
Size: 522874 Color: 3
Size: 477087 Color: 1

Bin 2086: 40 of cap free
Amount of items: 3
Items: 
Size: 368464 Color: 4
Size: 354391 Color: 0
Size: 277106 Color: 2

Bin 2087: 41 of cap free
Amount of items: 2
Items: 
Size: 501164 Color: 3
Size: 498796 Color: 1

Bin 2088: 41 of cap free
Amount of items: 2
Items: 
Size: 527904 Color: 1
Size: 472056 Color: 0

Bin 2089: 41 of cap free
Amount of items: 3
Items: 
Size: 620888 Color: 3
Size: 192676 Color: 4
Size: 186396 Color: 1

Bin 2090: 41 of cap free
Amount of items: 2
Items: 
Size: 659481 Color: 4
Size: 340479 Color: 2

Bin 2091: 41 of cap free
Amount of items: 3
Items: 
Size: 676941 Color: 0
Size: 161883 Color: 3
Size: 161136 Color: 4

Bin 2092: 41 of cap free
Amount of items: 2
Items: 
Size: 681513 Color: 3
Size: 318447 Color: 0

Bin 2093: 41 of cap free
Amount of items: 2
Items: 
Size: 698737 Color: 0
Size: 301223 Color: 1

Bin 2094: 41 of cap free
Amount of items: 2
Items: 
Size: 589238 Color: 4
Size: 410722 Color: 0

Bin 2095: 41 of cap free
Amount of items: 3
Items: 
Size: 375301 Color: 4
Size: 320829 Color: 1
Size: 303830 Color: 0

Bin 2096: 41 of cap free
Amount of items: 2
Items: 
Size: 723225 Color: 0
Size: 276735 Color: 1

Bin 2097: 41 of cap free
Amount of items: 2
Items: 
Size: 733906 Color: 0
Size: 266054 Color: 4

Bin 2098: 41 of cap free
Amount of items: 2
Items: 
Size: 575624 Color: 4
Size: 424336 Color: 1

Bin 2099: 41 of cap free
Amount of items: 2
Items: 
Size: 503032 Color: 2
Size: 496928 Color: 1

Bin 2100: 41 of cap free
Amount of items: 2
Items: 
Size: 749976 Color: 4
Size: 249984 Color: 3

Bin 2101: 42 of cap free
Amount of items: 2
Items: 
Size: 683776 Color: 1
Size: 316183 Color: 2

Bin 2102: 42 of cap free
Amount of items: 2
Items: 
Size: 721126 Color: 0
Size: 278833 Color: 2

Bin 2103: 42 of cap free
Amount of items: 2
Items: 
Size: 717660 Color: 2
Size: 282299 Color: 0

Bin 2104: 42 of cap free
Amount of items: 2
Items: 
Size: 506247 Color: 4
Size: 493712 Color: 0

Bin 2105: 42 of cap free
Amount of items: 2
Items: 
Size: 522498 Color: 3
Size: 477461 Color: 0

Bin 2106: 42 of cap free
Amount of items: 2
Items: 
Size: 551207 Color: 2
Size: 448752 Color: 1

Bin 2107: 42 of cap free
Amount of items: 2
Items: 
Size: 562731 Color: 3
Size: 437228 Color: 1

Bin 2108: 42 of cap free
Amount of items: 2
Items: 
Size: 657259 Color: 1
Size: 342700 Color: 0

Bin 2109: 42 of cap free
Amount of items: 3
Items: 
Size: 690776 Color: 1
Size: 154703 Color: 4
Size: 154480 Color: 3

Bin 2110: 42 of cap free
Amount of items: 2
Items: 
Size: 695035 Color: 0
Size: 304924 Color: 2

Bin 2111: 42 of cap free
Amount of items: 2
Items: 
Size: 745306 Color: 1
Size: 254653 Color: 3

Bin 2112: 42 of cap free
Amount of items: 2
Items: 
Size: 782706 Color: 0
Size: 217253 Color: 1

Bin 2113: 42 of cap free
Amount of items: 2
Items: 
Size: 633603 Color: 1
Size: 366356 Color: 2

Bin 2114: 42 of cap free
Amount of items: 2
Items: 
Size: 573329 Color: 3
Size: 426630 Color: 0

Bin 2115: 42 of cap free
Amount of items: 2
Items: 
Size: 723923 Color: 3
Size: 276036 Color: 1

Bin 2116: 42 of cap free
Amount of items: 2
Items: 
Size: 745462 Color: 0
Size: 254497 Color: 2

Bin 2117: 42 of cap free
Amount of items: 2
Items: 
Size: 660824 Color: 2
Size: 339135 Color: 3

Bin 2118: 42 of cap free
Amount of items: 2
Items: 
Size: 607027 Color: 0
Size: 392932 Color: 4

Bin 2119: 43 of cap free
Amount of items: 2
Items: 
Size: 524039 Color: 1
Size: 475919 Color: 2

Bin 2120: 43 of cap free
Amount of items: 2
Items: 
Size: 627419 Color: 2
Size: 372539 Color: 0

Bin 2121: 43 of cap free
Amount of items: 2
Items: 
Size: 654877 Color: 1
Size: 345081 Color: 4

Bin 2122: 43 of cap free
Amount of items: 2
Items: 
Size: 666046 Color: 4
Size: 333912 Color: 0

Bin 2123: 43 of cap free
Amount of items: 2
Items: 
Size: 676263 Color: 0
Size: 323695 Color: 4

Bin 2124: 43 of cap free
Amount of items: 2
Items: 
Size: 733433 Color: 1
Size: 266525 Color: 0

Bin 2125: 43 of cap free
Amount of items: 2
Items: 
Size: 738456 Color: 0
Size: 261502 Color: 1

Bin 2126: 43 of cap free
Amount of items: 2
Items: 
Size: 781681 Color: 3
Size: 218277 Color: 1

Bin 2127: 43 of cap free
Amount of items: 2
Items: 
Size: 674956 Color: 3
Size: 325002 Color: 1

Bin 2128: 43 of cap free
Amount of items: 2
Items: 
Size: 650502 Color: 4
Size: 349456 Color: 3

Bin 2129: 43 of cap free
Amount of items: 2
Items: 
Size: 610682 Color: 3
Size: 389276 Color: 2

Bin 2130: 43 of cap free
Amount of items: 2
Items: 
Size: 731694 Color: 1
Size: 268264 Color: 0

Bin 2131: 44 of cap free
Amount of items: 2
Items: 
Size: 590380 Color: 4
Size: 409577 Color: 2

Bin 2132: 44 of cap free
Amount of items: 3
Items: 
Size: 425815 Color: 3
Size: 312475 Color: 2
Size: 261667 Color: 1

Bin 2133: 44 of cap free
Amount of items: 2
Items: 
Size: 509560 Color: 4
Size: 490397 Color: 1

Bin 2134: 44 of cap free
Amount of items: 2
Items: 
Size: 539253 Color: 3
Size: 460704 Color: 1

Bin 2135: 44 of cap free
Amount of items: 2
Items: 
Size: 556893 Color: 2
Size: 443064 Color: 3

Bin 2136: 44 of cap free
Amount of items: 2
Items: 
Size: 569174 Color: 4
Size: 430783 Color: 2

Bin 2137: 44 of cap free
Amount of items: 2
Items: 
Size: 633317 Color: 2
Size: 366640 Color: 4

Bin 2138: 44 of cap free
Amount of items: 2
Items: 
Size: 653109 Color: 1
Size: 346848 Color: 0

Bin 2139: 44 of cap free
Amount of items: 2
Items: 
Size: 655791 Color: 4
Size: 344166 Color: 2

Bin 2140: 44 of cap free
Amount of items: 2
Items: 
Size: 703068 Color: 2
Size: 296889 Color: 4

Bin 2141: 44 of cap free
Amount of items: 2
Items: 
Size: 778528 Color: 2
Size: 221429 Color: 3

Bin 2142: 44 of cap free
Amount of items: 2
Items: 
Size: 740068 Color: 1
Size: 259889 Color: 0

Bin 2143: 44 of cap free
Amount of items: 2
Items: 
Size: 541041 Color: 4
Size: 458916 Color: 1

Bin 2144: 44 of cap free
Amount of items: 3
Items: 
Size: 427054 Color: 2
Size: 296507 Color: 1
Size: 276396 Color: 1

Bin 2145: 44 of cap free
Amount of items: 2
Items: 
Size: 779864 Color: 3
Size: 220093 Color: 2

Bin 2146: 44 of cap free
Amount of items: 3
Items: 
Size: 608722 Color: 1
Size: 196420 Color: 0
Size: 194815 Color: 2

Bin 2147: 44 of cap free
Amount of items: 2
Items: 
Size: 601855 Color: 4
Size: 398102 Color: 2

Bin 2148: 44 of cap free
Amount of items: 2
Items: 
Size: 764860 Color: 3
Size: 235097 Color: 0

Bin 2149: 44 of cap free
Amount of items: 2
Items: 
Size: 607491 Color: 3
Size: 392466 Color: 0

Bin 2150: 44 of cap free
Amount of items: 2
Items: 
Size: 768840 Color: 4
Size: 231117 Color: 2

Bin 2151: 44 of cap free
Amount of items: 2
Items: 
Size: 795525 Color: 0
Size: 204432 Color: 2

Bin 2152: 44 of cap free
Amount of items: 3
Items: 
Size: 353195 Color: 1
Size: 324644 Color: 3
Size: 322118 Color: 3

Bin 2153: 44 of cap free
Amount of items: 3
Items: 
Size: 349391 Color: 1
Size: 327805 Color: 3
Size: 322761 Color: 3

Bin 2154: 45 of cap free
Amount of items: 2
Items: 
Size: 782850 Color: 3
Size: 217106 Color: 0

Bin 2155: 45 of cap free
Amount of items: 2
Items: 
Size: 658614 Color: 2
Size: 341342 Color: 4

Bin 2156: 45 of cap free
Amount of items: 2
Items: 
Size: 530820 Color: 2
Size: 469136 Color: 3

Bin 2157: 45 of cap free
Amount of items: 2
Items: 
Size: 529027 Color: 0
Size: 470929 Color: 1

Bin 2158: 45 of cap free
Amount of items: 2
Items: 
Size: 539677 Color: 0
Size: 460279 Color: 1

Bin 2159: 45 of cap free
Amount of items: 2
Items: 
Size: 559376 Color: 0
Size: 440580 Color: 4

Bin 2160: 45 of cap free
Amount of items: 2
Items: 
Size: 569993 Color: 3
Size: 429963 Color: 2

Bin 2161: 45 of cap free
Amount of items: 2
Items: 
Size: 573983 Color: 2
Size: 425973 Color: 1

Bin 2162: 45 of cap free
Amount of items: 2
Items: 
Size: 577240 Color: 0
Size: 422716 Color: 1

Bin 2163: 45 of cap free
Amount of items: 2
Items: 
Size: 585811 Color: 2
Size: 414145 Color: 4

Bin 2164: 45 of cap free
Amount of items: 2
Items: 
Size: 624447 Color: 1
Size: 375509 Color: 4

Bin 2165: 45 of cap free
Amount of items: 2
Items: 
Size: 677930 Color: 4
Size: 322026 Color: 2

Bin 2166: 45 of cap free
Amount of items: 2
Items: 
Size: 679433 Color: 2
Size: 320523 Color: 4

Bin 2167: 45 of cap free
Amount of items: 2
Items: 
Size: 683681 Color: 2
Size: 316275 Color: 4

Bin 2168: 45 of cap free
Amount of items: 2
Items: 
Size: 547672 Color: 4
Size: 452284 Color: 1

Bin 2169: 45 of cap free
Amount of items: 3
Items: 
Size: 647741 Color: 1
Size: 176741 Color: 3
Size: 175474 Color: 4

Bin 2170: 45 of cap free
Amount of items: 3
Items: 
Size: 668760 Color: 2
Size: 165837 Color: 3
Size: 165359 Color: 2

Bin 2171: 45 of cap free
Amount of items: 2
Items: 
Size: 597792 Color: 4
Size: 402164 Color: 3

Bin 2172: 45 of cap free
Amount of items: 2
Items: 
Size: 503896 Color: 0
Size: 496060 Color: 4

Bin 2173: 46 of cap free
Amount of items: 2
Items: 
Size: 777003 Color: 3
Size: 222952 Color: 0

Bin 2174: 46 of cap free
Amount of items: 2
Items: 
Size: 527900 Color: 0
Size: 472055 Color: 1

Bin 2175: 46 of cap free
Amount of items: 2
Items: 
Size: 539160 Color: 3
Size: 460795 Color: 4

Bin 2176: 46 of cap free
Amount of items: 2
Items: 
Size: 571296 Color: 4
Size: 428659 Color: 1

Bin 2177: 46 of cap free
Amount of items: 2
Items: 
Size: 634425 Color: 3
Size: 365530 Color: 1

Bin 2178: 46 of cap free
Amount of items: 2
Items: 
Size: 673699 Color: 4
Size: 326256 Color: 2

Bin 2179: 46 of cap free
Amount of items: 2
Items: 
Size: 710370 Color: 4
Size: 289585 Color: 1

Bin 2180: 46 of cap free
Amount of items: 2
Items: 
Size: 744301 Color: 3
Size: 255654 Color: 1

Bin 2181: 46 of cap free
Amount of items: 2
Items: 
Size: 774636 Color: 4
Size: 225319 Color: 0

Bin 2182: 46 of cap free
Amount of items: 2
Items: 
Size: 681267 Color: 3
Size: 318688 Color: 4

Bin 2183: 46 of cap free
Amount of items: 2
Items: 
Size: 751225 Color: 0
Size: 248730 Color: 2

Bin 2184: 46 of cap free
Amount of items: 2
Items: 
Size: 799318 Color: 4
Size: 200637 Color: 2

Bin 2185: 46 of cap free
Amount of items: 2
Items: 
Size: 796133 Color: 3
Size: 203822 Color: 2

Bin 2186: 47 of cap free
Amount of items: 2
Items: 
Size: 577794 Color: 3
Size: 422160 Color: 2

Bin 2187: 47 of cap free
Amount of items: 2
Items: 
Size: 725875 Color: 0
Size: 274079 Color: 4

Bin 2188: 47 of cap free
Amount of items: 2
Items: 
Size: 577937 Color: 3
Size: 422017 Color: 2

Bin 2189: 47 of cap free
Amount of items: 2
Items: 
Size: 676436 Color: 3
Size: 323518 Color: 4

Bin 2190: 47 of cap free
Amount of items: 2
Items: 
Size: 760367 Color: 1
Size: 239587 Color: 0

Bin 2191: 47 of cap free
Amount of items: 2
Items: 
Size: 606487 Color: 0
Size: 393467 Color: 2

Bin 2192: 47 of cap free
Amount of items: 2
Items: 
Size: 594386 Color: 0
Size: 405568 Color: 4

Bin 2193: 47 of cap free
Amount of items: 2
Items: 
Size: 673633 Color: 3
Size: 326321 Color: 0

Bin 2194: 47 of cap free
Amount of items: 2
Items: 
Size: 765479 Color: 4
Size: 234475 Color: 2

Bin 2195: 47 of cap free
Amount of items: 2
Items: 
Size: 794731 Color: 3
Size: 205223 Color: 4

Bin 2196: 47 of cap free
Amount of items: 2
Items: 
Size: 631052 Color: 3
Size: 368902 Color: 1

Bin 2197: 48 of cap free
Amount of items: 2
Items: 
Size: 517334 Color: 3
Size: 482619 Color: 1

Bin 2198: 48 of cap free
Amount of items: 2
Items: 
Size: 530450 Color: 3
Size: 469503 Color: 4

Bin 2199: 48 of cap free
Amount of items: 2
Items: 
Size: 531133 Color: 0
Size: 468820 Color: 3

Bin 2200: 48 of cap free
Amount of items: 2
Items: 
Size: 586015 Color: 2
Size: 413938 Color: 4

Bin 2201: 48 of cap free
Amount of items: 2
Items: 
Size: 642942 Color: 1
Size: 357011 Color: 4

Bin 2202: 48 of cap free
Amount of items: 2
Items: 
Size: 570865 Color: 4
Size: 429088 Color: 3

Bin 2203: 48 of cap free
Amount of items: 2
Items: 
Size: 781960 Color: 3
Size: 217993 Color: 2

Bin 2204: 48 of cap free
Amount of items: 2
Items: 
Size: 548673 Color: 2
Size: 451280 Color: 4

Bin 2205: 48 of cap free
Amount of items: 2
Items: 
Size: 728471 Color: 2
Size: 271482 Color: 3

Bin 2206: 48 of cap free
Amount of items: 3
Items: 
Size: 357715 Color: 2
Size: 321277 Color: 0
Size: 320961 Color: 1

Bin 2207: 48 of cap free
Amount of items: 2
Items: 
Size: 591594 Color: 2
Size: 408359 Color: 1

Bin 2208: 48 of cap free
Amount of items: 2
Items: 
Size: 600824 Color: 0
Size: 399129 Color: 1

Bin 2209: 49 of cap free
Amount of items: 2
Items: 
Size: 797233 Color: 2
Size: 202719 Color: 1

Bin 2210: 49 of cap free
Amount of items: 2
Items: 
Size: 520792 Color: 3
Size: 479160 Color: 4

Bin 2211: 49 of cap free
Amount of items: 2
Items: 
Size: 527770 Color: 3
Size: 472182 Color: 0

Bin 2212: 49 of cap free
Amount of items: 2
Items: 
Size: 561475 Color: 2
Size: 438477 Color: 3

Bin 2213: 49 of cap free
Amount of items: 2
Items: 
Size: 640859 Color: 0
Size: 359093 Color: 1

Bin 2214: 49 of cap free
Amount of items: 2
Items: 
Size: 650750 Color: 0
Size: 349202 Color: 1

Bin 2215: 49 of cap free
Amount of items: 2
Items: 
Size: 730595 Color: 4
Size: 269357 Color: 0

Bin 2216: 49 of cap free
Amount of items: 2
Items: 
Size: 759012 Color: 0
Size: 240940 Color: 1

Bin 2217: 49 of cap free
Amount of items: 2
Items: 
Size: 786342 Color: 2
Size: 213610 Color: 1

Bin 2218: 49 of cap free
Amount of items: 2
Items: 
Size: 668285 Color: 1
Size: 331667 Color: 2

Bin 2219: 49 of cap free
Amount of items: 2
Items: 
Size: 757901 Color: 4
Size: 242051 Color: 1

Bin 2220: 49 of cap free
Amount of items: 2
Items: 
Size: 751666 Color: 4
Size: 248286 Color: 1

Bin 2221: 49 of cap free
Amount of items: 2
Items: 
Size: 741157 Color: 1
Size: 258795 Color: 3

Bin 2222: 49 of cap free
Amount of items: 3
Items: 
Size: 747193 Color: 4
Size: 128336 Color: 2
Size: 124423 Color: 0

Bin 2223: 49 of cap free
Amount of items: 2
Items: 
Size: 545806 Color: 0
Size: 454146 Color: 3

Bin 2224: 50 of cap free
Amount of items: 2
Items: 
Size: 682558 Color: 3
Size: 317393 Color: 0

Bin 2225: 50 of cap free
Amount of items: 2
Items: 
Size: 550233 Color: 4
Size: 449718 Color: 2

Bin 2226: 50 of cap free
Amount of items: 2
Items: 
Size: 569884 Color: 1
Size: 430067 Color: 0

Bin 2227: 50 of cap free
Amount of items: 2
Items: 
Size: 581376 Color: 4
Size: 418575 Color: 2

Bin 2228: 50 of cap free
Amount of items: 2
Items: 
Size: 594239 Color: 3
Size: 405712 Color: 1

Bin 2229: 50 of cap free
Amount of items: 2
Items: 
Size: 658991 Color: 0
Size: 340960 Color: 1

Bin 2230: 50 of cap free
Amount of items: 2
Items: 
Size: 688713 Color: 2
Size: 311238 Color: 4

Bin 2231: 50 of cap free
Amount of items: 2
Items: 
Size: 763066 Color: 0
Size: 236885 Color: 3

Bin 2232: 50 of cap free
Amount of items: 2
Items: 
Size: 767159 Color: 1
Size: 232792 Color: 3

Bin 2233: 50 of cap free
Amount of items: 2
Items: 
Size: 798949 Color: 4
Size: 201002 Color: 0

Bin 2234: 50 of cap free
Amount of items: 3
Items: 
Size: 675497 Color: 0
Size: 162814 Color: 0
Size: 161640 Color: 1

Bin 2235: 50 of cap free
Amount of items: 2
Items: 
Size: 688991 Color: 2
Size: 310960 Color: 3

Bin 2236: 50 of cap free
Amount of items: 2
Items: 
Size: 571456 Color: 4
Size: 428495 Color: 0

Bin 2237: 50 of cap free
Amount of items: 2
Items: 
Size: 739674 Color: 0
Size: 260277 Color: 3

Bin 2238: 50 of cap free
Amount of items: 2
Items: 
Size: 728774 Color: 3
Size: 271177 Color: 4

Bin 2239: 50 of cap free
Amount of items: 2
Items: 
Size: 689137 Color: 4
Size: 310814 Color: 2

Bin 2240: 50 of cap free
Amount of items: 2
Items: 
Size: 501042 Color: 2
Size: 498909 Color: 1

Bin 2241: 51 of cap free
Amount of items: 2
Items: 
Size: 595290 Color: 0
Size: 404660 Color: 1

Bin 2242: 51 of cap free
Amount of items: 2
Items: 
Size: 523564 Color: 0
Size: 476386 Color: 1

Bin 2243: 51 of cap free
Amount of items: 2
Items: 
Size: 551575 Color: 3
Size: 448375 Color: 4

Bin 2244: 51 of cap free
Amount of items: 2
Items: 
Size: 556742 Color: 2
Size: 443208 Color: 0

Bin 2245: 51 of cap free
Amount of items: 2
Items: 
Size: 580700 Color: 3
Size: 419250 Color: 2

Bin 2246: 51 of cap free
Amount of items: 2
Items: 
Size: 657983 Color: 1
Size: 341967 Color: 3

Bin 2247: 51 of cap free
Amount of items: 2
Items: 
Size: 715374 Color: 0
Size: 284576 Color: 1

Bin 2248: 51 of cap free
Amount of items: 2
Items: 
Size: 759488 Color: 4
Size: 240462 Color: 1

Bin 2249: 51 of cap free
Amount of items: 2
Items: 
Size: 789463 Color: 4
Size: 210487 Color: 0

Bin 2250: 51 of cap free
Amount of items: 3
Items: 
Size: 593591 Color: 0
Size: 203453 Color: 4
Size: 202906 Color: 4

Bin 2251: 51 of cap free
Amount of items: 2
Items: 
Size: 656805 Color: 3
Size: 343145 Color: 2

Bin 2252: 51 of cap free
Amount of items: 2
Items: 
Size: 642306 Color: 0
Size: 357644 Color: 3

Bin 2253: 51 of cap free
Amount of items: 2
Items: 
Size: 659570 Color: 3
Size: 340380 Color: 0

Bin 2254: 51 of cap free
Amount of items: 2
Items: 
Size: 619383 Color: 4
Size: 380567 Color: 1

Bin 2255: 51 of cap free
Amount of items: 2
Items: 
Size: 710247 Color: 0
Size: 289703 Color: 3

Bin 2256: 51 of cap free
Amount of items: 2
Items: 
Size: 766209 Color: 0
Size: 233741 Color: 1

Bin 2257: 51 of cap free
Amount of items: 2
Items: 
Size: 691379 Color: 4
Size: 308571 Color: 3

Bin 2258: 52 of cap free
Amount of items: 2
Items: 
Size: 657007 Color: 3
Size: 342942 Color: 1

Bin 2259: 52 of cap free
Amount of items: 2
Items: 
Size: 792582 Color: 0
Size: 207367 Color: 3

Bin 2260: 52 of cap free
Amount of items: 2
Items: 
Size: 521638 Color: 4
Size: 478311 Color: 0

Bin 2261: 52 of cap free
Amount of items: 2
Items: 
Size: 650299 Color: 4
Size: 349650 Color: 1

Bin 2262: 52 of cap free
Amount of items: 2
Items: 
Size: 687839 Color: 3
Size: 312110 Color: 0

Bin 2263: 52 of cap free
Amount of items: 2
Items: 
Size: 700674 Color: 3
Size: 299275 Color: 1

Bin 2264: 52 of cap free
Amount of items: 2
Items: 
Size: 708681 Color: 2
Size: 291268 Color: 0

Bin 2265: 52 of cap free
Amount of items: 2
Items: 
Size: 744026 Color: 1
Size: 255923 Color: 2

Bin 2266: 52 of cap free
Amount of items: 2
Items: 
Size: 588305 Color: 2
Size: 411644 Color: 0

Bin 2267: 52 of cap free
Amount of items: 2
Items: 
Size: 709867 Color: 4
Size: 290082 Color: 3

Bin 2268: 52 of cap free
Amount of items: 2
Items: 
Size: 764570 Color: 3
Size: 235379 Color: 2

Bin 2269: 53 of cap free
Amount of items: 2
Items: 
Size: 779386 Color: 4
Size: 220562 Color: 0

Bin 2270: 53 of cap free
Amount of items: 2
Items: 
Size: 520107 Color: 4
Size: 479841 Color: 1

Bin 2271: 53 of cap free
Amount of items: 2
Items: 
Size: 516374 Color: 0
Size: 483574 Color: 3

Bin 2272: 53 of cap free
Amount of items: 2
Items: 
Size: 576161 Color: 3
Size: 423787 Color: 1

Bin 2273: 53 of cap free
Amount of items: 2
Items: 
Size: 631766 Color: 4
Size: 368182 Color: 3

Bin 2274: 53 of cap free
Amount of items: 2
Items: 
Size: 669163 Color: 0
Size: 330785 Color: 4

Bin 2275: 53 of cap free
Amount of items: 2
Items: 
Size: 715269 Color: 4
Size: 284679 Color: 2

Bin 2276: 53 of cap free
Amount of items: 2
Items: 
Size: 718011 Color: 3
Size: 281937 Color: 2

Bin 2277: 53 of cap free
Amount of items: 2
Items: 
Size: 665039 Color: 3
Size: 334909 Color: 2

Bin 2278: 53 of cap free
Amount of items: 2
Items: 
Size: 773140 Color: 4
Size: 226808 Color: 2

Bin 2279: 53 of cap free
Amount of items: 2
Items: 
Size: 674779 Color: 1
Size: 325169 Color: 3

Bin 2280: 53 of cap free
Amount of items: 2
Items: 
Size: 601087 Color: 1
Size: 398861 Color: 0

Bin 2281: 53 of cap free
Amount of items: 2
Items: 
Size: 792853 Color: 1
Size: 207095 Color: 2

Bin 2282: 54 of cap free
Amount of items: 2
Items: 
Size: 692748 Color: 4
Size: 307199 Color: 2

Bin 2283: 54 of cap free
Amount of items: 2
Items: 
Size: 616780 Color: 2
Size: 383167 Color: 3

Bin 2284: 54 of cap free
Amount of items: 2
Items: 
Size: 508481 Color: 4
Size: 491466 Color: 0

Bin 2285: 54 of cap free
Amount of items: 2
Items: 
Size: 516986 Color: 2
Size: 482961 Color: 0

Bin 2286: 54 of cap free
Amount of items: 2
Items: 
Size: 521462 Color: 0
Size: 478485 Color: 3

Bin 2287: 54 of cap free
Amount of items: 2
Items: 
Size: 559533 Color: 0
Size: 440414 Color: 4

Bin 2288: 54 of cap free
Amount of items: 2
Items: 
Size: 613794 Color: 3
Size: 386153 Color: 4

Bin 2289: 54 of cap free
Amount of items: 2
Items: 
Size: 622016 Color: 0
Size: 377931 Color: 2

Bin 2290: 54 of cap free
Amount of items: 2
Items: 
Size: 641754 Color: 0
Size: 358193 Color: 3

Bin 2291: 54 of cap free
Amount of items: 2
Items: 
Size: 735844 Color: 3
Size: 264103 Color: 4

Bin 2292: 54 of cap free
Amount of items: 2
Items: 
Size: 579069 Color: 0
Size: 420878 Color: 3

Bin 2293: 54 of cap free
Amount of items: 2
Items: 
Size: 716619 Color: 2
Size: 283328 Color: 1

Bin 2294: 54 of cap free
Amount of items: 3
Items: 
Size: 735180 Color: 4
Size: 139524 Color: 3
Size: 125243 Color: 3

Bin 2295: 54 of cap free
Amount of items: 2
Items: 
Size: 713344 Color: 3
Size: 286603 Color: 2

Bin 2296: 54 of cap free
Amount of items: 2
Items: 
Size: 507959 Color: 0
Size: 491988 Color: 2

Bin 2297: 54 of cap free
Amount of items: 2
Items: 
Size: 522666 Color: 2
Size: 477281 Color: 3

Bin 2298: 55 of cap free
Amount of items: 2
Items: 
Size: 666281 Color: 2
Size: 333665 Color: 4

Bin 2299: 55 of cap free
Amount of items: 2
Items: 
Size: 519213 Color: 0
Size: 480733 Color: 3

Bin 2300: 55 of cap free
Amount of items: 2
Items: 
Size: 523404 Color: 0
Size: 476542 Color: 3

Bin 2301: 55 of cap free
Amount of items: 2
Items: 
Size: 547870 Color: 4
Size: 452076 Color: 2

Bin 2302: 55 of cap free
Amount of items: 2
Items: 
Size: 562841 Color: 2
Size: 437105 Color: 1

Bin 2303: 55 of cap free
Amount of items: 2
Items: 
Size: 597105 Color: 4
Size: 402841 Color: 1

Bin 2304: 55 of cap free
Amount of items: 2
Items: 
Size: 612582 Color: 3
Size: 387364 Color: 1

Bin 2305: 55 of cap free
Amount of items: 2
Items: 
Size: 723671 Color: 4
Size: 276275 Color: 3

Bin 2306: 55 of cap free
Amount of items: 2
Items: 
Size: 748311 Color: 3
Size: 251635 Color: 4

Bin 2307: 55 of cap free
Amount of items: 2
Items: 
Size: 764501 Color: 1
Size: 235445 Color: 3

Bin 2308: 55 of cap free
Amount of items: 2
Items: 
Size: 782188 Color: 2
Size: 217758 Color: 4

Bin 2309: 55 of cap free
Amount of items: 2
Items: 
Size: 760682 Color: 1
Size: 239264 Color: 3

Bin 2310: 55 of cap free
Amount of items: 2
Items: 
Size: 697097 Color: 2
Size: 302849 Color: 4

Bin 2311: 55 of cap free
Amount of items: 2
Items: 
Size: 514561 Color: 2
Size: 485385 Color: 3

Bin 2312: 55 of cap free
Amount of items: 2
Items: 
Size: 778799 Color: 2
Size: 221147 Color: 3

Bin 2313: 55 of cap free
Amount of items: 2
Items: 
Size: 652974 Color: 0
Size: 346972 Color: 1

Bin 2314: 55 of cap free
Amount of items: 2
Items: 
Size: 590170 Color: 3
Size: 409776 Color: 0

Bin 2315: 55 of cap free
Amount of items: 2
Items: 
Size: 567489 Color: 2
Size: 432457 Color: 1

Bin 2316: 55 of cap free
Amount of items: 2
Items: 
Size: 753730 Color: 2
Size: 246216 Color: 3

Bin 2317: 56 of cap free
Amount of items: 2
Items: 
Size: 659774 Color: 0
Size: 340171 Color: 1

Bin 2318: 56 of cap free
Amount of items: 2
Items: 
Size: 625702 Color: 3
Size: 374243 Color: 1

Bin 2319: 56 of cap free
Amount of items: 2
Items: 
Size: 579400 Color: 4
Size: 420545 Color: 3

Bin 2320: 56 of cap free
Amount of items: 2
Items: 
Size: 591858 Color: 2
Size: 408087 Color: 3

Bin 2321: 56 of cap free
Amount of items: 2
Items: 
Size: 600159 Color: 3
Size: 399786 Color: 2

Bin 2322: 56 of cap free
Amount of items: 2
Items: 
Size: 641054 Color: 1
Size: 358891 Color: 2

Bin 2323: 56 of cap free
Amount of items: 2
Items: 
Size: 665584 Color: 0
Size: 334361 Color: 2

Bin 2324: 56 of cap free
Amount of items: 2
Items: 
Size: 685471 Color: 1
Size: 314474 Color: 0

Bin 2325: 56 of cap free
Amount of items: 2
Items: 
Size: 509996 Color: 4
Size: 489949 Color: 1

Bin 2326: 56 of cap free
Amount of items: 2
Items: 
Size: 573034 Color: 0
Size: 426911 Color: 1

Bin 2327: 56 of cap free
Amount of items: 2
Items: 
Size: 660132 Color: 1
Size: 339813 Color: 4

Bin 2328: 56 of cap free
Amount of items: 2
Items: 
Size: 519000 Color: 1
Size: 480945 Color: 4

Bin 2329: 56 of cap free
Amount of items: 2
Items: 
Size: 689477 Color: 1
Size: 310468 Color: 3

Bin 2330: 56 of cap free
Amount of items: 2
Items: 
Size: 616134 Color: 2
Size: 383811 Color: 4

Bin 2331: 56 of cap free
Amount of items: 2
Items: 
Size: 642051 Color: 1
Size: 357894 Color: 0

Bin 2332: 56 of cap free
Amount of items: 2
Items: 
Size: 625391 Color: 1
Size: 374554 Color: 4

Bin 2333: 56 of cap free
Amount of items: 2
Items: 
Size: 653161 Color: 2
Size: 346784 Color: 3

Bin 2334: 57 of cap free
Amount of items: 2
Items: 
Size: 638333 Color: 2
Size: 361611 Color: 1

Bin 2335: 57 of cap free
Amount of items: 2
Items: 
Size: 752008 Color: 4
Size: 247936 Color: 1

Bin 2336: 57 of cap free
Amount of items: 2
Items: 
Size: 609173 Color: 1
Size: 390771 Color: 4

Bin 2337: 57 of cap free
Amount of items: 2
Items: 
Size: 796010 Color: 3
Size: 203934 Color: 4

Bin 2338: 57 of cap free
Amount of items: 2
Items: 
Size: 601686 Color: 3
Size: 398258 Color: 4

Bin 2339: 57 of cap free
Amount of items: 2
Items: 
Size: 562033 Color: 3
Size: 437911 Color: 4

Bin 2340: 57 of cap free
Amount of items: 2
Items: 
Size: 556016 Color: 0
Size: 443928 Color: 1

Bin 2341: 57 of cap free
Amount of items: 2
Items: 
Size: 606745 Color: 4
Size: 393199 Color: 0

Bin 2342: 57 of cap free
Amount of items: 2
Items: 
Size: 603686 Color: 1
Size: 396258 Color: 2

Bin 2343: 57 of cap free
Amount of items: 2
Items: 
Size: 598995 Color: 2
Size: 400949 Color: 1

Bin 2344: 58 of cap free
Amount of items: 2
Items: 
Size: 726375 Color: 1
Size: 273568 Color: 0

Bin 2345: 58 of cap free
Amount of items: 2
Items: 
Size: 548603 Color: 4
Size: 451340 Color: 1

Bin 2346: 58 of cap free
Amount of items: 2
Items: 
Size: 588431 Color: 4
Size: 411512 Color: 3

Bin 2347: 58 of cap free
Amount of items: 2
Items: 
Size: 614740 Color: 1
Size: 385203 Color: 0

Bin 2348: 58 of cap free
Amount of items: 2
Items: 
Size: 637349 Color: 4
Size: 362594 Color: 2

Bin 2349: 58 of cap free
Amount of items: 2
Items: 
Size: 638687 Color: 3
Size: 361256 Color: 1

Bin 2350: 58 of cap free
Amount of items: 2
Items: 
Size: 691706 Color: 4
Size: 308237 Color: 2

Bin 2351: 58 of cap free
Amount of items: 2
Items: 
Size: 698871 Color: 4
Size: 301072 Color: 2

Bin 2352: 58 of cap free
Amount of items: 2
Items: 
Size: 699627 Color: 2
Size: 300316 Color: 4

Bin 2353: 58 of cap free
Amount of items: 2
Items: 
Size: 711799 Color: 0
Size: 288144 Color: 2

Bin 2354: 58 of cap free
Amount of items: 2
Items: 
Size: 712554 Color: 4
Size: 287389 Color: 1

Bin 2355: 58 of cap free
Amount of items: 2
Items: 
Size: 733589 Color: 2
Size: 266354 Color: 3

Bin 2356: 58 of cap free
Amount of items: 2
Items: 
Size: 718522 Color: 4
Size: 281421 Color: 2

Bin 2357: 58 of cap free
Amount of items: 2
Items: 
Size: 658227 Color: 1
Size: 341716 Color: 2

Bin 2358: 58 of cap free
Amount of items: 2
Items: 
Size: 595742 Color: 1
Size: 404201 Color: 0

Bin 2359: 58 of cap free
Amount of items: 2
Items: 
Size: 771483 Color: 4
Size: 228460 Color: 2

Bin 2360: 58 of cap free
Amount of items: 2
Items: 
Size: 545612 Color: 1
Size: 454331 Color: 4

Bin 2361: 58 of cap free
Amount of items: 3
Items: 
Size: 349967 Color: 2
Size: 343769 Color: 1
Size: 306207 Color: 3

Bin 2362: 59 of cap free
Amount of items: 2
Items: 
Size: 547664 Color: 2
Size: 452278 Color: 1

Bin 2363: 59 of cap free
Amount of items: 2
Items: 
Size: 522229 Color: 1
Size: 477713 Color: 2

Bin 2364: 59 of cap free
Amount of items: 2
Items: 
Size: 525548 Color: 3
Size: 474394 Color: 0

Bin 2365: 59 of cap free
Amount of items: 2
Items: 
Size: 529321 Color: 1
Size: 470621 Color: 4

Bin 2366: 59 of cap free
Amount of items: 2
Items: 
Size: 536157 Color: 3
Size: 463785 Color: 1

Bin 2367: 59 of cap free
Amount of items: 2
Items: 
Size: 615289 Color: 1
Size: 384653 Color: 4

Bin 2368: 59 of cap free
Amount of items: 2
Items: 
Size: 639452 Color: 2
Size: 360490 Color: 0

Bin 2369: 59 of cap free
Amount of items: 2
Items: 
Size: 647842 Color: 2
Size: 352100 Color: 1

Bin 2370: 59 of cap free
Amount of items: 2
Items: 
Size: 654642 Color: 2
Size: 345300 Color: 1

Bin 2371: 59 of cap free
Amount of items: 2
Items: 
Size: 672510 Color: 4
Size: 327432 Color: 0

Bin 2372: 59 of cap free
Amount of items: 2
Items: 
Size: 760057 Color: 3
Size: 239885 Color: 1

Bin 2373: 59 of cap free
Amount of items: 2
Items: 
Size: 585594 Color: 1
Size: 414348 Color: 0

Bin 2374: 59 of cap free
Amount of items: 2
Items: 
Size: 626626 Color: 4
Size: 373316 Color: 0

Bin 2375: 59 of cap free
Amount of items: 2
Items: 
Size: 526746 Color: 4
Size: 473196 Color: 2

Bin 2376: 59 of cap free
Amount of items: 2
Items: 
Size: 662846 Color: 4
Size: 337096 Color: 3

Bin 2377: 59 of cap free
Amount of items: 2
Items: 
Size: 628885 Color: 0
Size: 371057 Color: 3

Bin 2378: 59 of cap free
Amount of items: 2
Items: 
Size: 712009 Color: 3
Size: 287933 Color: 1

Bin 2379: 59 of cap free
Amount of items: 2
Items: 
Size: 659142 Color: 4
Size: 340800 Color: 3

Bin 2380: 60 of cap free
Amount of items: 3
Items: 
Size: 725220 Color: 2
Size: 137403 Color: 0
Size: 137318 Color: 1

Bin 2381: 60 of cap free
Amount of items: 2
Items: 
Size: 611606 Color: 4
Size: 388335 Color: 3

Bin 2382: 60 of cap free
Amount of items: 2
Items: 
Size: 513234 Color: 1
Size: 486707 Color: 4

Bin 2383: 60 of cap free
Amount of items: 2
Items: 
Size: 558002 Color: 1
Size: 441939 Color: 4

Bin 2384: 60 of cap free
Amount of items: 2
Items: 
Size: 647570 Color: 0
Size: 352371 Color: 4

Bin 2385: 60 of cap free
Amount of items: 2
Items: 
Size: 684739 Color: 1
Size: 315202 Color: 2

Bin 2386: 60 of cap free
Amount of items: 3
Items: 
Size: 711232 Color: 1
Size: 144564 Color: 2
Size: 144145 Color: 1

Bin 2387: 60 of cap free
Amount of items: 2
Items: 
Size: 756702 Color: 4
Size: 243239 Color: 1

Bin 2388: 60 of cap free
Amount of items: 2
Items: 
Size: 532499 Color: 3
Size: 467442 Color: 2

Bin 2389: 60 of cap free
Amount of items: 2
Items: 
Size: 565318 Color: 3
Size: 434623 Color: 1

Bin 2390: 60 of cap free
Amount of items: 2
Items: 
Size: 555285 Color: 2
Size: 444656 Color: 3

Bin 2391: 60 of cap free
Amount of items: 2
Items: 
Size: 567684 Color: 1
Size: 432257 Color: 4

Bin 2392: 60 of cap free
Amount of items: 2
Items: 
Size: 555609 Color: 0
Size: 444332 Color: 4

Bin 2393: 60 of cap free
Amount of items: 2
Items: 
Size: 751095 Color: 0
Size: 248846 Color: 1

Bin 2394: 61 of cap free
Amount of items: 2
Items: 
Size: 762299 Color: 2
Size: 237641 Color: 1

Bin 2395: 61 of cap free
Amount of items: 2
Items: 
Size: 770606 Color: 3
Size: 229334 Color: 2

Bin 2396: 61 of cap free
Amount of items: 2
Items: 
Size: 527890 Color: 0
Size: 472050 Color: 1

Bin 2397: 61 of cap free
Amount of items: 2
Items: 
Size: 557821 Color: 4
Size: 442119 Color: 1

Bin 2398: 61 of cap free
Amount of items: 2
Items: 
Size: 630099 Color: 0
Size: 369841 Color: 4

Bin 2399: 61 of cap free
Amount of items: 2
Items: 
Size: 698201 Color: 3
Size: 301739 Color: 2

Bin 2400: 61 of cap free
Amount of items: 2
Items: 
Size: 705708 Color: 2
Size: 294232 Color: 4

Bin 2401: 61 of cap free
Amount of items: 2
Items: 
Size: 736533 Color: 1
Size: 263407 Color: 0

Bin 2402: 61 of cap free
Amount of items: 2
Items: 
Size: 740414 Color: 4
Size: 259526 Color: 0

Bin 2403: 61 of cap free
Amount of items: 2
Items: 
Size: 717424 Color: 4
Size: 282516 Color: 1

Bin 2404: 61 of cap free
Amount of items: 3
Items: 
Size: 565472 Color: 0
Size: 222577 Color: 0
Size: 211891 Color: 2

Bin 2405: 61 of cap free
Amount of items: 2
Items: 
Size: 563295 Color: 2
Size: 436645 Color: 4

Bin 2406: 61 of cap free
Amount of items: 2
Items: 
Size: 728698 Color: 3
Size: 271242 Color: 0

Bin 2407: 61 of cap free
Amount of items: 2
Items: 
Size: 545683 Color: 1
Size: 454257 Color: 3

Bin 2408: 61 of cap free
Amount of items: 2
Items: 
Size: 644117 Color: 4
Size: 355823 Color: 3

Bin 2409: 61 of cap free
Amount of items: 2
Items: 
Size: 585050 Color: 2
Size: 414890 Color: 3

Bin 2410: 62 of cap free
Amount of items: 2
Items: 
Size: 675364 Color: 2
Size: 324575 Color: 0

Bin 2411: 62 of cap free
Amount of items: 2
Items: 
Size: 728975 Color: 4
Size: 270964 Color: 2

Bin 2412: 62 of cap free
Amount of items: 2
Items: 
Size: 519917 Color: 0
Size: 480022 Color: 4

Bin 2413: 62 of cap free
Amount of items: 2
Items: 
Size: 539840 Color: 4
Size: 460099 Color: 2

Bin 2414: 62 of cap free
Amount of items: 2
Items: 
Size: 570304 Color: 0
Size: 429635 Color: 4

Bin 2415: 62 of cap free
Amount of items: 2
Items: 
Size: 587028 Color: 2
Size: 412911 Color: 4

Bin 2416: 62 of cap free
Amount of items: 2
Items: 
Size: 592421 Color: 2
Size: 407518 Color: 3

Bin 2417: 62 of cap free
Amount of items: 2
Items: 
Size: 710447 Color: 0
Size: 289492 Color: 3

Bin 2418: 62 of cap free
Amount of items: 2
Items: 
Size: 730069 Color: 0
Size: 269870 Color: 4

Bin 2419: 62 of cap free
Amount of items: 2
Items: 
Size: 730506 Color: 1
Size: 269433 Color: 4

Bin 2420: 62 of cap free
Amount of items: 2
Items: 
Size: 760810 Color: 0
Size: 239129 Color: 2

Bin 2421: 62 of cap free
Amount of items: 3
Items: 
Size: 627909 Color: 1
Size: 189578 Color: 4
Size: 182452 Color: 1

Bin 2422: 62 of cap free
Amount of items: 2
Items: 
Size: 680733 Color: 2
Size: 319206 Color: 4

Bin 2423: 62 of cap free
Amount of items: 2
Items: 
Size: 641218 Color: 1
Size: 358721 Color: 4

Bin 2424: 62 of cap free
Amount of items: 2
Items: 
Size: 771867 Color: 0
Size: 228072 Color: 3

Bin 2425: 63 of cap free
Amount of items: 3
Items: 
Size: 689918 Color: 4
Size: 155337 Color: 0
Size: 154683 Color: 2

Bin 2426: 63 of cap free
Amount of items: 2
Items: 
Size: 654413 Color: 2
Size: 345525 Color: 1

Bin 2427: 63 of cap free
Amount of items: 2
Items: 
Size: 575484 Color: 4
Size: 424454 Color: 2

Bin 2428: 63 of cap free
Amount of items: 2
Items: 
Size: 515080 Color: 3
Size: 484858 Color: 0

Bin 2429: 63 of cap free
Amount of items: 2
Items: 
Size: 691046 Color: 1
Size: 308892 Color: 2

Bin 2430: 63 of cap free
Amount of items: 2
Items: 
Size: 762948 Color: 4
Size: 236990 Color: 3

Bin 2431: 63 of cap free
Amount of items: 2
Items: 
Size: 524575 Color: 1
Size: 475363 Color: 2

Bin 2432: 63 of cap free
Amount of items: 2
Items: 
Size: 545738 Color: 3
Size: 454200 Color: 0

Bin 2433: 63 of cap free
Amount of items: 2
Items: 
Size: 794182 Color: 4
Size: 205756 Color: 0

Bin 2434: 63 of cap free
Amount of items: 2
Items: 
Size: 598237 Color: 2
Size: 401701 Color: 1

Bin 2435: 63 of cap free
Amount of items: 2
Items: 
Size: 789223 Color: 2
Size: 210715 Color: 0

Bin 2436: 64 of cap free
Amount of items: 2
Items: 
Size: 718640 Color: 3
Size: 281297 Color: 0

Bin 2437: 64 of cap free
Amount of items: 2
Items: 
Size: 623898 Color: 0
Size: 376039 Color: 4

Bin 2438: 64 of cap free
Amount of items: 3
Items: 
Size: 676725 Color: 0
Size: 162494 Color: 3
Size: 160718 Color: 0

Bin 2439: 64 of cap free
Amount of items: 2
Items: 
Size: 521255 Color: 2
Size: 478682 Color: 1

Bin 2440: 64 of cap free
Amount of items: 2
Items: 
Size: 521348 Color: 4
Size: 478589 Color: 2

Bin 2441: 64 of cap free
Amount of items: 2
Items: 
Size: 577111 Color: 1
Size: 422826 Color: 2

Bin 2442: 64 of cap free
Amount of items: 2
Items: 
Size: 595444 Color: 2
Size: 404493 Color: 3

Bin 2443: 64 of cap free
Amount of items: 2
Items: 
Size: 634442 Color: 1
Size: 365495 Color: 0

Bin 2444: 64 of cap free
Amount of items: 2
Items: 
Size: 684939 Color: 1
Size: 314998 Color: 2

Bin 2445: 64 of cap free
Amount of items: 2
Items: 
Size: 789081 Color: 0
Size: 210856 Color: 3

Bin 2446: 64 of cap free
Amount of items: 2
Items: 
Size: 579979 Color: 0
Size: 419958 Color: 3

Bin 2447: 64 of cap free
Amount of items: 2
Items: 
Size: 583852 Color: 0
Size: 416085 Color: 2

Bin 2448: 64 of cap free
Amount of items: 2
Items: 
Size: 553890 Color: 0
Size: 446047 Color: 3

Bin 2449: 64 of cap free
Amount of items: 2
Items: 
Size: 743852 Color: 0
Size: 256085 Color: 1

Bin 2450: 64 of cap free
Amount of items: 2
Items: 
Size: 722425 Color: 3
Size: 277512 Color: 0

Bin 2451: 65 of cap free
Amount of items: 3
Items: 
Size: 639637 Color: 4
Size: 182198 Color: 4
Size: 178101 Color: 3

Bin 2452: 65 of cap free
Amount of items: 2
Items: 
Size: 634052 Color: 1
Size: 365884 Color: 4

Bin 2453: 65 of cap free
Amount of items: 2
Items: 
Size: 575798 Color: 1
Size: 424138 Color: 3

Bin 2454: 65 of cap free
Amount of items: 2
Items: 
Size: 684671 Color: 3
Size: 315265 Color: 4

Bin 2455: 65 of cap free
Amount of items: 2
Items: 
Size: 699529 Color: 2
Size: 300407 Color: 4

Bin 2456: 65 of cap free
Amount of items: 2
Items: 
Size: 772410 Color: 3
Size: 227526 Color: 2

Bin 2457: 65 of cap free
Amount of items: 2
Items: 
Size: 776478 Color: 4
Size: 223458 Color: 0

Bin 2458: 65 of cap free
Amount of items: 2
Items: 
Size: 620180 Color: 1
Size: 379756 Color: 4

Bin 2459: 65 of cap free
Amount of items: 2
Items: 
Size: 790603 Color: 2
Size: 209333 Color: 3

Bin 2460: 65 of cap free
Amount of items: 2
Items: 
Size: 766718 Color: 4
Size: 233218 Color: 1

Bin 2461: 65 of cap free
Amount of items: 2
Items: 
Size: 540256 Color: 1
Size: 459680 Color: 4

Bin 2462: 65 of cap free
Amount of items: 2
Items: 
Size: 786222 Color: 2
Size: 213714 Color: 1

Bin 2463: 65 of cap free
Amount of items: 2
Items: 
Size: 573828 Color: 2
Size: 426108 Color: 4

Bin 2464: 65 of cap free
Amount of items: 2
Items: 
Size: 561009 Color: 1
Size: 438927 Color: 2

Bin 2465: 65 of cap free
Amount of items: 3
Items: 
Size: 661872 Color: 2
Size: 186095 Color: 0
Size: 151969 Color: 4

Bin 2466: 65 of cap free
Amount of items: 2
Items: 
Size: 728225 Color: 4
Size: 271711 Color: 1

Bin 2467: 65 of cap free
Amount of items: 2
Items: 
Size: 526634 Color: 1
Size: 473302 Color: 2

Bin 2468: 66 of cap free
Amount of items: 3
Items: 
Size: 628078 Color: 3
Size: 206706 Color: 0
Size: 165151 Color: 2

Bin 2469: 66 of cap free
Amount of items: 2
Items: 
Size: 539583 Color: 2
Size: 460352 Color: 4

Bin 2470: 66 of cap free
Amount of items: 3
Items: 
Size: 392826 Color: 2
Size: 344979 Color: 0
Size: 262130 Color: 3

Bin 2471: 66 of cap free
Amount of items: 2
Items: 
Size: 528113 Color: 0
Size: 471822 Color: 3

Bin 2472: 66 of cap free
Amount of items: 2
Items: 
Size: 548895 Color: 2
Size: 451040 Color: 3

Bin 2473: 66 of cap free
Amount of items: 2
Items: 
Size: 554328 Color: 3
Size: 445607 Color: 1

Bin 2474: 66 of cap free
Amount of items: 2
Items: 
Size: 611990 Color: 2
Size: 387945 Color: 3

Bin 2475: 66 of cap free
Amount of items: 2
Items: 
Size: 623561 Color: 2
Size: 376374 Color: 0

Bin 2476: 66 of cap free
Amount of items: 2
Items: 
Size: 776405 Color: 0
Size: 223530 Color: 4

Bin 2477: 66 of cap free
Amount of items: 2
Items: 
Size: 738287 Color: 2
Size: 261648 Color: 0

Bin 2478: 66 of cap free
Amount of items: 2
Items: 
Size: 544447 Color: 4
Size: 455488 Color: 2

Bin 2479: 66 of cap free
Amount of items: 2
Items: 
Size: 711875 Color: 3
Size: 288060 Color: 0

Bin 2480: 67 of cap free
Amount of items: 2
Items: 
Size: 752370 Color: 3
Size: 247564 Color: 4

Bin 2481: 67 of cap free
Amount of items: 2
Items: 
Size: 740167 Color: 2
Size: 259767 Color: 4

Bin 2482: 67 of cap free
Amount of items: 2
Items: 
Size: 573660 Color: 1
Size: 426274 Color: 4

Bin 2483: 67 of cap free
Amount of items: 2
Items: 
Size: 587874 Color: 3
Size: 412060 Color: 0

Bin 2484: 67 of cap free
Amount of items: 2
Items: 
Size: 599196 Color: 2
Size: 400738 Color: 1

Bin 2485: 67 of cap free
Amount of items: 2
Items: 
Size: 618042 Color: 2
Size: 381892 Color: 1

Bin 2486: 67 of cap free
Amount of items: 2
Items: 
Size: 626871 Color: 0
Size: 373063 Color: 2

Bin 2487: 67 of cap free
Amount of items: 2
Items: 
Size: 646945 Color: 1
Size: 352989 Color: 4

Bin 2488: 67 of cap free
Amount of items: 2
Items: 
Size: 690667 Color: 4
Size: 309267 Color: 1

Bin 2489: 67 of cap free
Amount of items: 2
Items: 
Size: 747255 Color: 4
Size: 252679 Color: 3

Bin 2490: 67 of cap free
Amount of items: 2
Items: 
Size: 560486 Color: 2
Size: 439448 Color: 4

Bin 2491: 67 of cap free
Amount of items: 2
Items: 
Size: 594051 Color: 4
Size: 405883 Color: 3

Bin 2492: 67 of cap free
Amount of items: 2
Items: 
Size: 648619 Color: 2
Size: 351315 Color: 1

Bin 2493: 67 of cap free
Amount of items: 2
Items: 
Size: 667986 Color: 0
Size: 331948 Color: 3

Bin 2494: 67 of cap free
Amount of items: 2
Items: 
Size: 603963 Color: 2
Size: 395971 Color: 1

Bin 2495: 68 of cap free
Amount of items: 2
Items: 
Size: 512953 Color: 0
Size: 486980 Color: 2

Bin 2496: 68 of cap free
Amount of items: 2
Items: 
Size: 507287 Color: 1
Size: 492646 Color: 4

Bin 2497: 68 of cap free
Amount of items: 2
Items: 
Size: 532839 Color: 4
Size: 467094 Color: 1

Bin 2498: 68 of cap free
Amount of items: 2
Items: 
Size: 720927 Color: 4
Size: 279006 Color: 0

Bin 2499: 68 of cap free
Amount of items: 2
Items: 
Size: 780195 Color: 1
Size: 219738 Color: 2

Bin 2500: 68 of cap free
Amount of items: 2
Items: 
Size: 550746 Color: 4
Size: 449187 Color: 2

Bin 2501: 68 of cap free
Amount of items: 2
Items: 
Size: 590767 Color: 0
Size: 409166 Color: 1

Bin 2502: 68 of cap free
Amount of items: 2
Items: 
Size: 681996 Color: 2
Size: 317937 Color: 0

Bin 2503: 69 of cap free
Amount of items: 3
Items: 
Size: 629458 Color: 1
Size: 189279 Color: 3
Size: 181195 Color: 4

Bin 2504: 69 of cap free
Amount of items: 2
Items: 
Size: 555122 Color: 0
Size: 444810 Color: 4

Bin 2505: 69 of cap free
Amount of items: 2
Items: 
Size: 596764 Color: 1
Size: 403168 Color: 0

Bin 2506: 69 of cap free
Amount of items: 3
Items: 
Size: 699096 Color: 3
Size: 151097 Color: 2
Size: 149739 Color: 2

Bin 2507: 69 of cap free
Amount of items: 2
Items: 
Size: 503473 Color: 2
Size: 496459 Color: 4

Bin 2508: 69 of cap free
Amount of items: 2
Items: 
Size: 516726 Color: 0
Size: 483206 Color: 4

Bin 2509: 69 of cap free
Amount of items: 2
Items: 
Size: 522220 Color: 1
Size: 477712 Color: 2

Bin 2510: 69 of cap free
Amount of items: 2
Items: 
Size: 571727 Color: 0
Size: 428205 Color: 1

Bin 2511: 69 of cap free
Amount of items: 2
Items: 
Size: 618397 Color: 1
Size: 381535 Color: 4

Bin 2512: 69 of cap free
Amount of items: 2
Items: 
Size: 634336 Color: 2
Size: 365596 Color: 0

Bin 2513: 69 of cap free
Amount of items: 2
Items: 
Size: 763636 Color: 0
Size: 236296 Color: 3

Bin 2514: 69 of cap free
Amount of items: 2
Items: 
Size: 609129 Color: 4
Size: 390803 Color: 0

Bin 2515: 69 of cap free
Amount of items: 2
Items: 
Size: 785244 Color: 0
Size: 214688 Color: 3

Bin 2516: 69 of cap free
Amount of items: 2
Items: 
Size: 678340 Color: 1
Size: 321592 Color: 3

Bin 2517: 70 of cap free
Amount of items: 2
Items: 
Size: 561155 Color: 1
Size: 438776 Color: 3

Bin 2518: 70 of cap free
Amount of items: 2
Items: 
Size: 671598 Color: 1
Size: 328333 Color: 4

Bin 2519: 70 of cap free
Amount of items: 2
Items: 
Size: 583369 Color: 4
Size: 416562 Color: 0

Bin 2520: 70 of cap free
Amount of items: 2
Items: 
Size: 788848 Color: 4
Size: 211083 Color: 0

Bin 2521: 70 of cap free
Amount of items: 2
Items: 
Size: 555525 Color: 4
Size: 444406 Color: 3

Bin 2522: 70 of cap free
Amount of items: 2
Items: 
Size: 750450 Color: 0
Size: 249481 Color: 4

Bin 2523: 70 of cap free
Amount of items: 2
Items: 
Size: 544901 Color: 3
Size: 455030 Color: 4

Bin 2524: 71 of cap free
Amount of items: 2
Items: 
Size: 722001 Color: 3
Size: 277929 Color: 1

Bin 2525: 71 of cap free
Amount of items: 2
Items: 
Size: 534424 Color: 1
Size: 465506 Color: 3

Bin 2526: 71 of cap free
Amount of items: 2
Items: 
Size: 540819 Color: 4
Size: 459111 Color: 1

Bin 2527: 71 of cap free
Amount of items: 2
Items: 
Size: 554724 Color: 2
Size: 445206 Color: 1

Bin 2528: 71 of cap free
Amount of items: 2
Items: 
Size: 592153 Color: 4
Size: 407777 Color: 3

Bin 2529: 71 of cap free
Amount of items: 2
Items: 
Size: 640641 Color: 1
Size: 359289 Color: 2

Bin 2530: 71 of cap free
Amount of items: 2
Items: 
Size: 660358 Color: 0
Size: 339572 Color: 1

Bin 2531: 71 of cap free
Amount of items: 2
Items: 
Size: 700181 Color: 3
Size: 299749 Color: 2

Bin 2532: 71 of cap free
Amount of items: 2
Items: 
Size: 619806 Color: 4
Size: 380124 Color: 1

Bin 2533: 71 of cap free
Amount of items: 2
Items: 
Size: 515547 Color: 2
Size: 484383 Color: 0

Bin 2534: 71 of cap free
Amount of items: 2
Items: 
Size: 725487 Color: 0
Size: 274443 Color: 2

Bin 2535: 71 of cap free
Amount of items: 3
Items: 
Size: 374785 Color: 0
Size: 316325 Color: 1
Size: 308820 Color: 4

Bin 2536: 71 of cap free
Amount of items: 2
Items: 
Size: 578697 Color: 3
Size: 421233 Color: 0

Bin 2537: 71 of cap free
Amount of items: 2
Items: 
Size: 629778 Color: 0
Size: 370152 Color: 3

Bin 2538: 72 of cap free
Amount of items: 2
Items: 
Size: 543045 Color: 3
Size: 456884 Color: 0

Bin 2539: 72 of cap free
Amount of items: 2
Items: 
Size: 529864 Color: 2
Size: 470065 Color: 3

Bin 2540: 72 of cap free
Amount of items: 2
Items: 
Size: 535592 Color: 2
Size: 464337 Color: 3

Bin 2541: 72 of cap free
Amount of items: 2
Items: 
Size: 550218 Color: 0
Size: 449711 Color: 1

Bin 2542: 72 of cap free
Amount of items: 2
Items: 
Size: 776091 Color: 0
Size: 223838 Color: 1

Bin 2543: 72 of cap free
Amount of items: 2
Items: 
Size: 539231 Color: 4
Size: 460698 Color: 0

Bin 2544: 72 of cap free
Amount of items: 2
Items: 
Size: 569148 Color: 0
Size: 430781 Color: 3

Bin 2545: 72 of cap free
Amount of items: 2
Items: 
Size: 704231 Color: 3
Size: 295698 Color: 2

Bin 2546: 72 of cap free
Amount of items: 2
Items: 
Size: 745801 Color: 0
Size: 254128 Color: 1

Bin 2547: 72 of cap free
Amount of items: 2
Items: 
Size: 765586 Color: 4
Size: 234343 Color: 2

Bin 2548: 72 of cap free
Amount of items: 2
Items: 
Size: 654540 Color: 3
Size: 345389 Color: 2

Bin 2549: 73 of cap free
Amount of items: 2
Items: 
Size: 507736 Color: 1
Size: 492192 Color: 0

Bin 2550: 73 of cap free
Amount of items: 2
Items: 
Size: 703127 Color: 2
Size: 296801 Color: 3

Bin 2551: 73 of cap free
Amount of items: 2
Items: 
Size: 522764 Color: 2
Size: 477164 Color: 3

Bin 2552: 73 of cap free
Amount of items: 2
Items: 
Size: 679050 Color: 3
Size: 320878 Color: 2

Bin 2553: 73 of cap free
Amount of items: 2
Items: 
Size: 704183 Color: 0
Size: 295745 Color: 3

Bin 2554: 73 of cap free
Amount of items: 3
Items: 
Size: 558845 Color: 0
Size: 226931 Color: 0
Size: 214152 Color: 1

Bin 2555: 73 of cap free
Amount of items: 2
Items: 
Size: 789418 Color: 2
Size: 210510 Color: 4

Bin 2556: 73 of cap free
Amount of items: 2
Items: 
Size: 723897 Color: 2
Size: 276031 Color: 4

Bin 2557: 73 of cap free
Amount of items: 2
Items: 
Size: 704376 Color: 4
Size: 295552 Color: 2

Bin 2558: 73 of cap free
Amount of items: 2
Items: 
Size: 781168 Color: 3
Size: 218760 Color: 4

Bin 2559: 73 of cap free
Amount of items: 2
Items: 
Size: 708569 Color: 2
Size: 291359 Color: 3

Bin 2560: 73 of cap free
Amount of items: 2
Items: 
Size: 507388 Color: 2
Size: 492540 Color: 4

Bin 2561: 74 of cap free
Amount of items: 2
Items: 
Size: 788117 Color: 1
Size: 211810 Color: 4

Bin 2562: 74 of cap free
Amount of items: 2
Items: 
Size: 505799 Color: 3
Size: 494128 Color: 4

Bin 2563: 74 of cap free
Amount of items: 2
Items: 
Size: 507980 Color: 2
Size: 491947 Color: 0

Bin 2564: 74 of cap free
Amount of items: 2
Items: 
Size: 529078 Color: 4
Size: 470849 Color: 2

Bin 2565: 74 of cap free
Amount of items: 2
Items: 
Size: 568437 Color: 2
Size: 431490 Color: 3

Bin 2566: 74 of cap free
Amount of items: 2
Items: 
Size: 586596 Color: 0
Size: 413331 Color: 1

Bin 2567: 74 of cap free
Amount of items: 2
Items: 
Size: 738943 Color: 1
Size: 260984 Color: 0

Bin 2568: 74 of cap free
Amount of items: 2
Items: 
Size: 756500 Color: 2
Size: 243427 Color: 0

Bin 2569: 74 of cap free
Amount of items: 2
Items: 
Size: 766464 Color: 3
Size: 233463 Color: 1

Bin 2570: 74 of cap free
Amount of items: 2
Items: 
Size: 773404 Color: 1
Size: 226523 Color: 0

Bin 2571: 74 of cap free
Amount of items: 2
Items: 
Size: 572891 Color: 1
Size: 427036 Color: 2

Bin 2572: 74 of cap free
Amount of items: 2
Items: 
Size: 568100 Color: 4
Size: 431827 Color: 1

Bin 2573: 74 of cap free
Amount of items: 2
Items: 
Size: 704093 Color: 2
Size: 295834 Color: 4

Bin 2574: 75 of cap free
Amount of items: 2
Items: 
Size: 511447 Color: 3
Size: 488479 Color: 4

Bin 2575: 75 of cap free
Amount of items: 2
Items: 
Size: 654093 Color: 3
Size: 345833 Color: 0

Bin 2576: 75 of cap free
Amount of items: 2
Items: 
Size: 750983 Color: 1
Size: 248943 Color: 4

Bin 2577: 75 of cap free
Amount of items: 2
Items: 
Size: 696578 Color: 1
Size: 303348 Color: 3

Bin 2578: 76 of cap free
Amount of items: 2
Items: 
Size: 560310 Color: 0
Size: 439615 Color: 1

Bin 2579: 76 of cap free
Amount of items: 2
Items: 
Size: 523548 Color: 0
Size: 476377 Color: 3

Bin 2580: 76 of cap free
Amount of items: 2
Items: 
Size: 538123 Color: 0
Size: 461802 Color: 1

Bin 2581: 76 of cap free
Amount of items: 2
Items: 
Size: 551043 Color: 3
Size: 448882 Color: 0

Bin 2582: 76 of cap free
Amount of items: 2
Items: 
Size: 677088 Color: 2
Size: 322837 Color: 0

Bin 2583: 76 of cap free
Amount of items: 2
Items: 
Size: 687828 Color: 3
Size: 312097 Color: 1

Bin 2584: 76 of cap free
Amount of items: 2
Items: 
Size: 706757 Color: 2
Size: 293168 Color: 4

Bin 2585: 76 of cap free
Amount of items: 2
Items: 
Size: 734686 Color: 3
Size: 265239 Color: 0

Bin 2586: 76 of cap free
Amount of items: 2
Items: 
Size: 740562 Color: 1
Size: 259363 Color: 0

Bin 2587: 76 of cap free
Amount of items: 2
Items: 
Size: 748104 Color: 1
Size: 251821 Color: 3

Bin 2588: 76 of cap free
Amount of items: 2
Items: 
Size: 783564 Color: 1
Size: 216361 Color: 4

Bin 2589: 76 of cap free
Amount of items: 2
Items: 
Size: 697349 Color: 3
Size: 302576 Color: 2

Bin 2590: 76 of cap free
Amount of items: 2
Items: 
Size: 765123 Color: 3
Size: 234802 Color: 4

Bin 2591: 76 of cap free
Amount of items: 2
Items: 
Size: 513964 Color: 2
Size: 485961 Color: 4

Bin 2592: 76 of cap free
Amount of items: 2
Items: 
Size: 795234 Color: 4
Size: 204691 Color: 2

Bin 2593: 76 of cap free
Amount of items: 2
Items: 
Size: 672947 Color: 4
Size: 326978 Color: 3

Bin 2594: 77 of cap free
Amount of items: 2
Items: 
Size: 570302 Color: 1
Size: 429622 Color: 4

Bin 2595: 77 of cap free
Amount of items: 2
Items: 
Size: 607798 Color: 4
Size: 392126 Color: 0

Bin 2596: 77 of cap free
Amount of items: 2
Items: 
Size: 689746 Color: 2
Size: 310178 Color: 4

Bin 2597: 77 of cap free
Amount of items: 2
Items: 
Size: 794429 Color: 4
Size: 205495 Color: 2

Bin 2598: 77 of cap free
Amount of items: 2
Items: 
Size: 754364 Color: 3
Size: 245560 Color: 2

Bin 2599: 78 of cap free
Amount of items: 2
Items: 
Size: 667629 Color: 1
Size: 332294 Color: 2

Bin 2600: 78 of cap free
Amount of items: 2
Items: 
Size: 735089 Color: 4
Size: 264834 Color: 3

Bin 2601: 78 of cap free
Amount of items: 2
Items: 
Size: 669736 Color: 3
Size: 330187 Color: 1

Bin 2602: 78 of cap free
Amount of items: 2
Items: 
Size: 515784 Color: 0
Size: 484139 Color: 3

Bin 2603: 78 of cap free
Amount of items: 2
Items: 
Size: 531449 Color: 2
Size: 468474 Color: 1

Bin 2604: 78 of cap free
Amount of items: 2
Items: 
Size: 600293 Color: 4
Size: 399630 Color: 3

Bin 2605: 78 of cap free
Amount of items: 2
Items: 
Size: 601587 Color: 1
Size: 398336 Color: 4

Bin 2606: 78 of cap free
Amount of items: 2
Items: 
Size: 617045 Color: 3
Size: 382878 Color: 4

Bin 2607: 78 of cap free
Amount of items: 2
Items: 
Size: 633790 Color: 2
Size: 366133 Color: 1

Bin 2608: 78 of cap free
Amount of items: 2
Items: 
Size: 758507 Color: 2
Size: 241416 Color: 3

Bin 2609: 78 of cap free
Amount of items: 2
Items: 
Size: 787717 Color: 2
Size: 212206 Color: 4

Bin 2610: 78 of cap free
Amount of items: 2
Items: 
Size: 616671 Color: 1
Size: 383252 Color: 3

Bin 2611: 78 of cap free
Amount of items: 2
Items: 
Size: 717272 Color: 2
Size: 282651 Color: 1

Bin 2612: 78 of cap free
Amount of items: 2
Items: 
Size: 574120 Color: 1
Size: 425803 Color: 3

Bin 2613: 78 of cap free
Amount of items: 3
Items: 
Size: 706155 Color: 4
Size: 150971 Color: 2
Size: 142797 Color: 4

Bin 2614: 78 of cap free
Amount of items: 2
Items: 
Size: 788520 Color: 0
Size: 211403 Color: 4

Bin 2615: 78 of cap free
Amount of items: 2
Items: 
Size: 565797 Color: 2
Size: 434126 Color: 3

Bin 2616: 79 of cap free
Amount of items: 2
Items: 
Size: 670185 Color: 1
Size: 329737 Color: 4

Bin 2617: 79 of cap free
Amount of items: 2
Items: 
Size: 574362 Color: 2
Size: 425560 Color: 1

Bin 2618: 79 of cap free
Amount of items: 2
Items: 
Size: 599136 Color: 1
Size: 400786 Color: 2

Bin 2619: 79 of cap free
Amount of items: 2
Items: 
Size: 647385 Color: 1
Size: 352537 Color: 2

Bin 2620: 79 of cap free
Amount of items: 2
Items: 
Size: 649061 Color: 0
Size: 350861 Color: 4

Bin 2621: 79 of cap free
Amount of items: 2
Items: 
Size: 773042 Color: 0
Size: 226880 Color: 4

Bin 2622: 79 of cap free
Amount of items: 2
Items: 
Size: 564575 Color: 2
Size: 435347 Color: 4

Bin 2623: 79 of cap free
Amount of items: 2
Items: 
Size: 517434 Color: 2
Size: 482488 Color: 3

Bin 2624: 79 of cap free
Amount of items: 2
Items: 
Size: 507855 Color: 2
Size: 492067 Color: 0

Bin 2625: 79 of cap free
Amount of items: 2
Items: 
Size: 553602 Color: 4
Size: 446320 Color: 2

Bin 2626: 79 of cap free
Amount of items: 2
Items: 
Size: 609749 Color: 3
Size: 390173 Color: 4

Bin 2627: 80 of cap free
Amount of items: 2
Items: 
Size: 610309 Color: 2
Size: 389612 Color: 0

Bin 2628: 80 of cap free
Amount of items: 2
Items: 
Size: 691225 Color: 3
Size: 308696 Color: 0

Bin 2629: 80 of cap free
Amount of items: 2
Items: 
Size: 714459 Color: 2
Size: 285462 Color: 1

Bin 2630: 80 of cap free
Amount of items: 2
Items: 
Size: 738796 Color: 1
Size: 261125 Color: 3

Bin 2631: 80 of cap free
Amount of items: 2
Items: 
Size: 783159 Color: 2
Size: 216762 Color: 0

Bin 2632: 80 of cap free
Amount of items: 2
Items: 
Size: 679621 Color: 2
Size: 320300 Color: 1

Bin 2633: 80 of cap free
Amount of items: 2
Items: 
Size: 797704 Color: 1
Size: 202217 Color: 4

Bin 2634: 81 of cap free
Amount of items: 2
Items: 
Size: 500044 Color: 3
Size: 499876 Color: 4

Bin 2635: 81 of cap free
Amount of items: 2
Items: 
Size: 689127 Color: 1
Size: 310793 Color: 0

Bin 2636: 81 of cap free
Amount of items: 2
Items: 
Size: 749549 Color: 3
Size: 250371 Color: 0

Bin 2637: 81 of cap free
Amount of items: 2
Items: 
Size: 546417 Color: 1
Size: 453503 Color: 4

Bin 2638: 81 of cap free
Amount of items: 2
Items: 
Size: 595556 Color: 3
Size: 404364 Color: 0

Bin 2639: 81 of cap free
Amount of items: 2
Items: 
Size: 599053 Color: 3
Size: 400867 Color: 1

Bin 2640: 81 of cap free
Amount of items: 2
Items: 
Size: 611270 Color: 3
Size: 388650 Color: 0

Bin 2641: 81 of cap free
Amount of items: 2
Items: 
Size: 745362 Color: 4
Size: 254558 Color: 3

Bin 2642: 81 of cap free
Amount of items: 2
Items: 
Size: 785238 Color: 4
Size: 214682 Color: 3

Bin 2643: 81 of cap free
Amount of items: 2
Items: 
Size: 670843 Color: 4
Size: 329077 Color: 0

Bin 2644: 82 of cap free
Amount of items: 2
Items: 
Size: 767839 Color: 3
Size: 232080 Color: 1

Bin 2645: 82 of cap free
Amount of items: 2
Items: 
Size: 791520 Color: 3
Size: 208399 Color: 1

Bin 2646: 82 of cap free
Amount of items: 2
Items: 
Size: 660756 Color: 3
Size: 339163 Color: 2

Bin 2647: 82 of cap free
Amount of items: 2
Items: 
Size: 642211 Color: 1
Size: 357708 Color: 2

Bin 2648: 82 of cap free
Amount of items: 2
Items: 
Size: 609392 Color: 0
Size: 390527 Color: 2

Bin 2649: 82 of cap free
Amount of items: 2
Items: 
Size: 515366 Color: 2
Size: 484553 Color: 3

Bin 2650: 82 of cap free
Amount of items: 2
Items: 
Size: 608606 Color: 2
Size: 391313 Color: 4

Bin 2651: 82 of cap free
Amount of items: 2
Items: 
Size: 513246 Color: 3
Size: 486673 Color: 1

Bin 2652: 82 of cap free
Amount of items: 2
Items: 
Size: 693156 Color: 1
Size: 306763 Color: 2

Bin 2653: 82 of cap free
Amount of items: 3
Items: 
Size: 618483 Color: 3
Size: 191120 Color: 0
Size: 190316 Color: 3

Bin 2654: 82 of cap free
Amount of items: 2
Items: 
Size: 745565 Color: 0
Size: 254354 Color: 4

Bin 2655: 82 of cap free
Amount of items: 2
Items: 
Size: 590971 Color: 3
Size: 408948 Color: 1

Bin 2656: 82 of cap free
Amount of items: 2
Items: 
Size: 598808 Color: 3
Size: 401111 Color: 2

Bin 2657: 82 of cap free
Amount of items: 2
Items: 
Size: 598361 Color: 4
Size: 401558 Color: 1

Bin 2658: 83 of cap free
Amount of items: 3
Items: 
Size: 737780 Color: 1
Size: 131319 Color: 4
Size: 130819 Color: 4

Bin 2659: 83 of cap free
Amount of items: 2
Items: 
Size: 724995 Color: 3
Size: 274923 Color: 2

Bin 2660: 83 of cap free
Amount of items: 2
Items: 
Size: 559038 Color: 3
Size: 440880 Color: 0

Bin 2661: 83 of cap free
Amount of items: 2
Items: 
Size: 592702 Color: 0
Size: 407216 Color: 2

Bin 2662: 83 of cap free
Amount of items: 2
Items: 
Size: 741613 Color: 0
Size: 258305 Color: 4

Bin 2663: 83 of cap free
Amount of items: 2
Items: 
Size: 531009 Color: 3
Size: 468909 Color: 2

Bin 2664: 83 of cap free
Amount of items: 2
Items: 
Size: 624922 Color: 3
Size: 374996 Color: 4

Bin 2665: 83 of cap free
Amount of items: 2
Items: 
Size: 767252 Color: 2
Size: 232666 Color: 1

Bin 2666: 83 of cap free
Amount of items: 2
Items: 
Size: 625665 Color: 1
Size: 374253 Color: 3

Bin 2667: 83 of cap free
Amount of items: 2
Items: 
Size: 642939 Color: 4
Size: 356979 Color: 1

Bin 2668: 83 of cap free
Amount of items: 2
Items: 
Size: 794944 Color: 0
Size: 204974 Color: 2

Bin 2669: 84 of cap free
Amount of items: 2
Items: 
Size: 777920 Color: 0
Size: 221997 Color: 4

Bin 2670: 84 of cap free
Amount of items: 2
Items: 
Size: 510057 Color: 4
Size: 489860 Color: 3

Bin 2671: 84 of cap free
Amount of items: 2
Items: 
Size: 708858 Color: 4
Size: 291059 Color: 1

Bin 2672: 84 of cap free
Amount of items: 2
Items: 
Size: 778047 Color: 0
Size: 221870 Color: 1

Bin 2673: 84 of cap free
Amount of items: 2
Items: 
Size: 754213 Color: 0
Size: 245704 Color: 1

Bin 2674: 84 of cap free
Amount of items: 2
Items: 
Size: 576920 Color: 2
Size: 422997 Color: 0

Bin 2675: 84 of cap free
Amount of items: 2
Items: 
Size: 799448 Color: 1
Size: 200469 Color: 2

Bin 2676: 84 of cap free
Amount of items: 2
Items: 
Size: 549291 Color: 3
Size: 450626 Color: 2

Bin 2677: 85 of cap free
Amount of items: 2
Items: 
Size: 686461 Color: 3
Size: 313455 Color: 4

Bin 2678: 85 of cap free
Amount of items: 2
Items: 
Size: 579501 Color: 3
Size: 420415 Color: 4

Bin 2679: 85 of cap free
Amount of items: 2
Items: 
Size: 645449 Color: 1
Size: 354467 Color: 2

Bin 2680: 85 of cap free
Amount of items: 2
Items: 
Size: 731329 Color: 2
Size: 268587 Color: 1

Bin 2681: 85 of cap free
Amount of items: 2
Items: 
Size: 733208 Color: 3
Size: 266708 Color: 0

Bin 2682: 85 of cap free
Amount of items: 2
Items: 
Size: 718518 Color: 1
Size: 281398 Color: 2

Bin 2683: 85 of cap free
Amount of items: 2
Items: 
Size: 608915 Color: 3
Size: 391001 Color: 1

Bin 2684: 85 of cap free
Amount of items: 2
Items: 
Size: 652115 Color: 2
Size: 347801 Color: 3

Bin 2685: 85 of cap free
Amount of items: 2
Items: 
Size: 682404 Color: 0
Size: 317512 Color: 3

Bin 2686: 85 of cap free
Amount of items: 2
Items: 
Size: 767695 Color: 3
Size: 232221 Color: 1

Bin 2687: 86 of cap free
Amount of items: 2
Items: 
Size: 621430 Color: 2
Size: 378485 Color: 1

Bin 2688: 86 of cap free
Amount of items: 2
Items: 
Size: 560661 Color: 2
Size: 439254 Color: 4

Bin 2689: 86 of cap free
Amount of items: 2
Items: 
Size: 581592 Color: 1
Size: 418323 Color: 2

Bin 2690: 86 of cap free
Amount of items: 2
Items: 
Size: 662406 Color: 2
Size: 337509 Color: 0

Bin 2691: 86 of cap free
Amount of items: 2
Items: 
Size: 737479 Color: 3
Size: 262436 Color: 4

Bin 2692: 86 of cap free
Amount of items: 2
Items: 
Size: 613023 Color: 0
Size: 386892 Color: 3

Bin 2693: 86 of cap free
Amount of items: 2
Items: 
Size: 749594 Color: 0
Size: 250321 Color: 2

Bin 2694: 86 of cap free
Amount of items: 2
Items: 
Size: 732732 Color: 2
Size: 267183 Color: 3

Bin 2695: 86 of cap free
Amount of items: 2
Items: 
Size: 719858 Color: 2
Size: 280057 Color: 3

Bin 2696: 87 of cap free
Amount of items: 2
Items: 
Size: 555769 Color: 0
Size: 444145 Color: 1

Bin 2697: 87 of cap free
Amount of items: 3
Items: 
Size: 667084 Color: 0
Size: 167172 Color: 4
Size: 165658 Color: 4

Bin 2698: 87 of cap free
Amount of items: 2
Items: 
Size: 537978 Color: 2
Size: 461936 Color: 4

Bin 2699: 87 of cap free
Amount of items: 2
Items: 
Size: 582695 Color: 4
Size: 417219 Color: 3

Bin 2700: 87 of cap free
Amount of items: 2
Items: 
Size: 594819 Color: 4
Size: 405095 Color: 0

Bin 2701: 87 of cap free
Amount of items: 2
Items: 
Size: 614710 Color: 3
Size: 385204 Color: 1

Bin 2702: 87 of cap free
Amount of items: 2
Items: 
Size: 632501 Color: 0
Size: 367413 Color: 4

Bin 2703: 87 of cap free
Amount of items: 2
Items: 
Size: 678338 Color: 0
Size: 321576 Color: 4

Bin 2704: 87 of cap free
Amount of items: 2
Items: 
Size: 655779 Color: 3
Size: 344135 Color: 4

Bin 2705: 87 of cap free
Amount of items: 2
Items: 
Size: 650881 Color: 0
Size: 349033 Color: 1

Bin 2706: 87 of cap free
Amount of items: 2
Items: 
Size: 733569 Color: 3
Size: 266345 Color: 2

Bin 2707: 88 of cap free
Amount of items: 2
Items: 
Size: 702093 Color: 1
Size: 297820 Color: 4

Bin 2708: 88 of cap free
Amount of items: 2
Items: 
Size: 522071 Color: 3
Size: 477842 Color: 2

Bin 2709: 88 of cap free
Amount of items: 2
Items: 
Size: 772642 Color: 3
Size: 227271 Color: 4

Bin 2710: 89 of cap free
Amount of items: 2
Items: 
Size: 526299 Color: 3
Size: 473613 Color: 0

Bin 2711: 89 of cap free
Amount of items: 2
Items: 
Size: 665606 Color: 2
Size: 334306 Color: 3

Bin 2712: 89 of cap free
Amount of items: 2
Items: 
Size: 600492 Color: 2
Size: 399420 Color: 4

Bin 2713: 89 of cap free
Amount of items: 2
Items: 
Size: 524386 Color: 0
Size: 475526 Color: 4

Bin 2714: 89 of cap free
Amount of items: 2
Items: 
Size: 574948 Color: 3
Size: 424964 Color: 1

Bin 2715: 89 of cap free
Amount of items: 2
Items: 
Size: 594551 Color: 2
Size: 405361 Color: 4

Bin 2716: 89 of cap free
Amount of items: 2
Items: 
Size: 598775 Color: 0
Size: 401137 Color: 3

Bin 2717: 89 of cap free
Amount of items: 2
Items: 
Size: 622705 Color: 1
Size: 377207 Color: 4

Bin 2718: 89 of cap free
Amount of items: 2
Items: 
Size: 686580 Color: 0
Size: 313332 Color: 4

Bin 2719: 89 of cap free
Amount of items: 2
Items: 
Size: 681251 Color: 1
Size: 318661 Color: 2

Bin 2720: 89 of cap free
Amount of items: 2
Items: 
Size: 515936 Color: 2
Size: 483976 Color: 3

Bin 2721: 89 of cap free
Amount of items: 2
Items: 
Size: 560995 Color: 3
Size: 438917 Color: 2

Bin 2722: 89 of cap free
Amount of items: 2
Items: 
Size: 540292 Color: 4
Size: 459620 Color: 2

Bin 2723: 89 of cap free
Amount of items: 2
Items: 
Size: 698323 Color: 4
Size: 301589 Color: 0

Bin 2724: 90 of cap free
Amount of items: 2
Items: 
Size: 549030 Color: 0
Size: 450881 Color: 1

Bin 2725: 90 of cap free
Amount of items: 2
Items: 
Size: 563376 Color: 1
Size: 436535 Color: 2

Bin 2726: 90 of cap free
Amount of items: 3
Items: 
Size: 656292 Color: 1
Size: 172233 Color: 3
Size: 171386 Color: 1

Bin 2727: 90 of cap free
Amount of items: 2
Items: 
Size: 711065 Color: 0
Size: 288846 Color: 4

Bin 2728: 90 of cap free
Amount of items: 2
Items: 
Size: 679758 Color: 1
Size: 320153 Color: 3

Bin 2729: 91 of cap free
Amount of items: 2
Items: 
Size: 501441 Color: 2
Size: 498469 Color: 4

Bin 2730: 91 of cap free
Amount of items: 2
Items: 
Size: 503636 Color: 3
Size: 496274 Color: 2

Bin 2731: 91 of cap free
Amount of items: 2
Items: 
Size: 561775 Color: 0
Size: 438135 Color: 4

Bin 2732: 91 of cap free
Amount of items: 2
Items: 
Size: 578275 Color: 3
Size: 421635 Color: 1

Bin 2733: 91 of cap free
Amount of items: 2
Items: 
Size: 712522 Color: 0
Size: 287388 Color: 2

Bin 2734: 91 of cap free
Amount of items: 2
Items: 
Size: 773780 Color: 1
Size: 226130 Color: 0

Bin 2735: 91 of cap free
Amount of items: 2
Items: 
Size: 791920 Color: 0
Size: 207990 Color: 3

Bin 2736: 91 of cap free
Amount of items: 2
Items: 
Size: 677329 Color: 0
Size: 322581 Color: 2

Bin 2737: 92 of cap free
Amount of items: 3
Items: 
Size: 375168 Color: 0
Size: 372159 Color: 1
Size: 252582 Color: 4

Bin 2738: 92 of cap free
Amount of items: 2
Items: 
Size: 626184 Color: 0
Size: 373725 Color: 4

Bin 2739: 92 of cap free
Amount of items: 2
Items: 
Size: 647382 Color: 4
Size: 352527 Color: 2

Bin 2740: 92 of cap free
Amount of items: 2
Items: 
Size: 686245 Color: 4
Size: 313664 Color: 1

Bin 2741: 92 of cap free
Amount of items: 2
Items: 
Size: 698534 Color: 3
Size: 301375 Color: 0

Bin 2742: 92 of cap free
Amount of items: 2
Items: 
Size: 549610 Color: 1
Size: 450299 Color: 2

Bin 2743: 93 of cap free
Amount of items: 2
Items: 
Size: 721316 Color: 0
Size: 278592 Color: 4

Bin 2744: 93 of cap free
Amount of items: 2
Items: 
Size: 523991 Color: 4
Size: 475917 Color: 2

Bin 2745: 93 of cap free
Amount of items: 2
Items: 
Size: 525744 Color: 0
Size: 474164 Color: 4

Bin 2746: 93 of cap free
Amount of items: 2
Items: 
Size: 649577 Color: 1
Size: 350331 Color: 4

Bin 2747: 93 of cap free
Amount of items: 2
Items: 
Size: 548545 Color: 0
Size: 451363 Color: 4

Bin 2748: 93 of cap free
Amount of items: 2
Items: 
Size: 665753 Color: 2
Size: 334155 Color: 4

Bin 2749: 94 of cap free
Amount of items: 2
Items: 
Size: 643666 Color: 3
Size: 356241 Color: 0

Bin 2750: 94 of cap free
Amount of items: 2
Items: 
Size: 573024 Color: 4
Size: 426883 Color: 1

Bin 2751: 94 of cap free
Amount of items: 2
Items: 
Size: 544718 Color: 2
Size: 455189 Color: 3

Bin 2752: 94 of cap free
Amount of items: 2
Items: 
Size: 596754 Color: 1
Size: 403153 Color: 3

Bin 2753: 94 of cap free
Amount of items: 2
Items: 
Size: 755910 Color: 3
Size: 243997 Color: 4

Bin 2754: 94 of cap free
Amount of items: 2
Items: 
Size: 502496 Color: 0
Size: 497411 Color: 3

Bin 2755: 94 of cap free
Amount of items: 2
Items: 
Size: 739513 Color: 4
Size: 260394 Color: 2

Bin 2756: 94 of cap free
Amount of items: 2
Items: 
Size: 663060 Color: 1
Size: 336847 Color: 4

Bin 2757: 94 of cap free
Amount of items: 2
Items: 
Size: 701196 Color: 3
Size: 298711 Color: 0

Bin 2758: 94 of cap free
Amount of items: 2
Items: 
Size: 504087 Color: 4
Size: 495820 Color: 3

Bin 2759: 95 of cap free
Amount of items: 2
Items: 
Size: 608455 Color: 4
Size: 391451 Color: 0

Bin 2760: 95 of cap free
Amount of items: 2
Items: 
Size: 717602 Color: 0
Size: 282304 Color: 3

Bin 2761: 95 of cap free
Amount of items: 2
Items: 
Size: 723733 Color: 0
Size: 276173 Color: 4

Bin 2762: 95 of cap free
Amount of items: 2
Items: 
Size: 791352 Color: 4
Size: 208554 Color: 1

Bin 2763: 96 of cap free
Amount of items: 2
Items: 
Size: 765923 Color: 3
Size: 233982 Color: 1

Bin 2764: 96 of cap free
Amount of items: 2
Items: 
Size: 753597 Color: 4
Size: 246308 Color: 0

Bin 2765: 96 of cap free
Amount of items: 2
Items: 
Size: 527599 Color: 0
Size: 472306 Color: 4

Bin 2766: 96 of cap free
Amount of items: 2
Items: 
Size: 729987 Color: 4
Size: 269918 Color: 0

Bin 2767: 96 of cap free
Amount of items: 2
Items: 
Size: 636754 Color: 1
Size: 363151 Color: 2

Bin 2768: 96 of cap free
Amount of items: 2
Items: 
Size: 771993 Color: 4
Size: 227912 Color: 2

Bin 2769: 96 of cap free
Amount of items: 2
Items: 
Size: 533286 Color: 1
Size: 466619 Color: 4

Bin 2770: 96 of cap free
Amount of items: 2
Items: 
Size: 667407 Color: 2
Size: 332498 Color: 1

Bin 2771: 97 of cap free
Amount of items: 2
Items: 
Size: 722638 Color: 2
Size: 277266 Color: 4

Bin 2772: 97 of cap free
Amount of items: 2
Items: 
Size: 556083 Color: 4
Size: 443821 Color: 3

Bin 2773: 97 of cap free
Amount of items: 2
Items: 
Size: 586193 Color: 1
Size: 413711 Color: 2

Bin 2774: 97 of cap free
Amount of items: 2
Items: 
Size: 627452 Color: 0
Size: 372452 Color: 1

Bin 2775: 97 of cap free
Amount of items: 2
Items: 
Size: 669576 Color: 1
Size: 330328 Color: 2

Bin 2776: 97 of cap free
Amount of items: 2
Items: 
Size: 509265 Color: 4
Size: 490639 Color: 3

Bin 2777: 97 of cap free
Amount of items: 2
Items: 
Size: 798480 Color: 4
Size: 201424 Color: 0

Bin 2778: 97 of cap free
Amount of items: 2
Items: 
Size: 716993 Color: 4
Size: 282911 Color: 3

Bin 2779: 97 of cap free
Amount of items: 2
Items: 
Size: 503270 Color: 2
Size: 496634 Color: 4

Bin 2780: 97 of cap free
Amount of items: 2
Items: 
Size: 617220 Color: 0
Size: 382684 Color: 1

Bin 2781: 97 of cap free
Amount of items: 2
Items: 
Size: 570851 Color: 1
Size: 429053 Color: 2

Bin 2782: 98 of cap free
Amount of items: 2
Items: 
Size: 519557 Color: 3
Size: 480346 Color: 0

Bin 2783: 98 of cap free
Amount of items: 2
Items: 
Size: 521447 Color: 2
Size: 478456 Color: 3

Bin 2784: 98 of cap free
Amount of items: 2
Items: 
Size: 543584 Color: 3
Size: 456319 Color: 4

Bin 2785: 98 of cap free
Amount of items: 2
Items: 
Size: 615551 Color: 0
Size: 384352 Color: 3

Bin 2786: 98 of cap free
Amount of items: 2
Items: 
Size: 622988 Color: 1
Size: 376915 Color: 2

Bin 2787: 98 of cap free
Amount of items: 2
Items: 
Size: 658109 Color: 2
Size: 341794 Color: 4

Bin 2788: 98 of cap free
Amount of items: 2
Items: 
Size: 668215 Color: 0
Size: 331688 Color: 1

Bin 2789: 98 of cap free
Amount of items: 2
Items: 
Size: 706010 Color: 0
Size: 293893 Color: 4

Bin 2790: 98 of cap free
Amount of items: 2
Items: 
Size: 741444 Color: 4
Size: 258459 Color: 1

Bin 2791: 98 of cap free
Amount of items: 2
Items: 
Size: 750802 Color: 4
Size: 249101 Color: 0

Bin 2792: 98 of cap free
Amount of items: 2
Items: 
Size: 768475 Color: 0
Size: 231428 Color: 1

Bin 2793: 98 of cap free
Amount of items: 2
Items: 
Size: 529215 Color: 3
Size: 470688 Color: 0

Bin 2794: 98 of cap free
Amount of items: 2
Items: 
Size: 595894 Color: 3
Size: 404009 Color: 2

Bin 2795: 98 of cap free
Amount of items: 2
Items: 
Size: 720281 Color: 4
Size: 279622 Color: 2

Bin 2796: 99 of cap free
Amount of items: 3
Items: 
Size: 602523 Color: 3
Size: 198838 Color: 2
Size: 198541 Color: 4

Bin 2797: 99 of cap free
Amount of items: 2
Items: 
Size: 707849 Color: 1
Size: 292053 Color: 2

Bin 2798: 99 of cap free
Amount of items: 2
Items: 
Size: 792377 Color: 0
Size: 207525 Color: 1

Bin 2799: 99 of cap free
Amount of items: 2
Items: 
Size: 517981 Color: 2
Size: 481921 Color: 1

Bin 2800: 99 of cap free
Amount of items: 2
Items: 
Size: 717981 Color: 1
Size: 281921 Color: 0

Bin 2801: 99 of cap free
Amount of items: 2
Items: 
Size: 742230 Color: 4
Size: 257672 Color: 2

Bin 2802: 99 of cap free
Amount of items: 2
Items: 
Size: 661535 Color: 0
Size: 338367 Color: 1

Bin 2803: 100 of cap free
Amount of items: 2
Items: 
Size: 798934 Color: 4
Size: 200967 Color: 0

Bin 2804: 100 of cap free
Amount of items: 2
Items: 
Size: 668266 Color: 1
Size: 331635 Color: 3

Bin 2805: 100 of cap free
Amount of items: 2
Items: 
Size: 666767 Color: 0
Size: 333134 Color: 2

Bin 2806: 100 of cap free
Amount of items: 2
Items: 
Size: 505521 Color: 3
Size: 494380 Color: 4

Bin 2807: 100 of cap free
Amount of items: 2
Items: 
Size: 533036 Color: 0
Size: 466865 Color: 2

Bin 2808: 100 of cap free
Amount of items: 2
Items: 
Size: 567138 Color: 2
Size: 432763 Color: 0

Bin 2809: 100 of cap free
Amount of items: 2
Items: 
Size: 728856 Color: 4
Size: 271045 Color: 1

Bin 2810: 100 of cap free
Amount of items: 2
Items: 
Size: 757863 Color: 4
Size: 242038 Color: 0

Bin 2811: 100 of cap free
Amount of items: 2
Items: 
Size: 794424 Color: 3
Size: 205477 Color: 1

Bin 2812: 101 of cap free
Amount of items: 2
Items: 
Size: 728203 Color: 2
Size: 271697 Color: 0

Bin 2813: 101 of cap free
Amount of items: 2
Items: 
Size: 635734 Color: 0
Size: 364166 Color: 4

Bin 2814: 101 of cap free
Amount of items: 2
Items: 
Size: 790718 Color: 3
Size: 209182 Color: 2

Bin 2815: 101 of cap free
Amount of items: 2
Items: 
Size: 503019 Color: 1
Size: 496881 Color: 2

Bin 2816: 101 of cap free
Amount of items: 2
Items: 
Size: 545271 Color: 0
Size: 454629 Color: 1

Bin 2817: 101 of cap free
Amount of items: 2
Items: 
Size: 562526 Color: 3
Size: 437374 Color: 4

Bin 2818: 101 of cap free
Amount of items: 2
Items: 
Size: 588422 Color: 1
Size: 411478 Color: 2

Bin 2819: 101 of cap free
Amount of items: 2
Items: 
Size: 591678 Color: 3
Size: 408222 Color: 2

Bin 2820: 101 of cap free
Amount of items: 2
Items: 
Size: 593254 Color: 4
Size: 406646 Color: 3

Bin 2821: 101 of cap free
Amount of items: 2
Items: 
Size: 604092 Color: 3
Size: 395808 Color: 2

Bin 2822: 101 of cap free
Amount of items: 2
Items: 
Size: 681991 Color: 0
Size: 317909 Color: 1

Bin 2823: 101 of cap free
Amount of items: 2
Items: 
Size: 689274 Color: 3
Size: 310626 Color: 2

Bin 2824: 101 of cap free
Amount of items: 2
Items: 
Size: 672296 Color: 3
Size: 327604 Color: 0

Bin 2825: 101 of cap free
Amount of items: 2
Items: 
Size: 651668 Color: 2
Size: 348232 Color: 3

Bin 2826: 101 of cap free
Amount of items: 3
Items: 
Size: 339309 Color: 3
Size: 338687 Color: 2
Size: 321904 Color: 2

Bin 2827: 102 of cap free
Amount of items: 3
Items: 
Size: 615395 Color: 2
Size: 194042 Color: 3
Size: 190462 Color: 0

Bin 2828: 102 of cap free
Amount of items: 2
Items: 
Size: 732320 Color: 3
Size: 267579 Color: 0

Bin 2829: 102 of cap free
Amount of items: 2
Items: 
Size: 577740 Color: 0
Size: 422159 Color: 3

Bin 2830: 102 of cap free
Amount of items: 2
Items: 
Size: 754671 Color: 3
Size: 245228 Color: 0

Bin 2831: 102 of cap free
Amount of items: 2
Items: 
Size: 641177 Color: 4
Size: 358722 Color: 3

Bin 2832: 102 of cap free
Amount of items: 2
Items: 
Size: 524235 Color: 4
Size: 475664 Color: 0

Bin 2833: 102 of cap free
Amount of items: 2
Items: 
Size: 646326 Color: 0
Size: 353573 Color: 3

Bin 2834: 103 of cap free
Amount of items: 2
Items: 
Size: 527248 Color: 1
Size: 472650 Color: 0

Bin 2835: 103 of cap free
Amount of items: 2
Items: 
Size: 539055 Color: 3
Size: 460843 Color: 4

Bin 2836: 103 of cap free
Amount of items: 2
Items: 
Size: 545966 Color: 1
Size: 453932 Color: 0

Bin 2837: 103 of cap free
Amount of items: 2
Items: 
Size: 668747 Color: 0
Size: 331151 Color: 4

Bin 2838: 103 of cap free
Amount of items: 2
Items: 
Size: 757044 Color: 4
Size: 242854 Color: 1

Bin 2839: 103 of cap free
Amount of items: 2
Items: 
Size: 557444 Color: 0
Size: 442454 Color: 2

Bin 2840: 103 of cap free
Amount of items: 2
Items: 
Size: 607455 Color: 1
Size: 392443 Color: 2

Bin 2841: 103 of cap free
Amount of items: 2
Items: 
Size: 555731 Color: 2
Size: 444167 Color: 0

Bin 2842: 104 of cap free
Amount of items: 2
Items: 
Size: 759571 Color: 0
Size: 240326 Color: 2

Bin 2843: 104 of cap free
Amount of items: 2
Items: 
Size: 764863 Color: 0
Size: 235034 Color: 2

Bin 2844: 104 of cap free
Amount of items: 2
Items: 
Size: 551824 Color: 0
Size: 448073 Color: 2

Bin 2845: 104 of cap free
Amount of items: 2
Items: 
Size: 586544 Color: 1
Size: 413353 Color: 4

Bin 2846: 104 of cap free
Amount of items: 2
Items: 
Size: 542930 Color: 0
Size: 456967 Color: 3

Bin 2847: 104 of cap free
Amount of items: 2
Items: 
Size: 504583 Color: 0
Size: 495314 Color: 2

Bin 2848: 104 of cap free
Amount of items: 2
Items: 
Size: 547357 Color: 3
Size: 452540 Color: 2

Bin 2849: 105 of cap free
Amount of items: 2
Items: 
Size: 794549 Color: 0
Size: 205347 Color: 3

Bin 2850: 105 of cap free
Amount of items: 2
Items: 
Size: 721461 Color: 2
Size: 278435 Color: 4

Bin 2851: 105 of cap free
Amount of items: 2
Items: 
Size: 600127 Color: 1
Size: 399769 Color: 0

Bin 2852: 105 of cap free
Amount of items: 2
Items: 
Size: 601164 Color: 1
Size: 398732 Color: 2

Bin 2853: 105 of cap free
Amount of items: 2
Items: 
Size: 605109 Color: 2
Size: 394787 Color: 3

Bin 2854: 105 of cap free
Amount of items: 2
Items: 
Size: 637067 Color: 4
Size: 362829 Color: 0

Bin 2855: 105 of cap free
Amount of items: 2
Items: 
Size: 749279 Color: 0
Size: 250617 Color: 1

Bin 2856: 105 of cap free
Amount of items: 2
Items: 
Size: 584928 Color: 0
Size: 414968 Color: 2

Bin 2857: 105 of cap free
Amount of items: 2
Items: 
Size: 603536 Color: 1
Size: 396360 Color: 4

Bin 2858: 106 of cap free
Amount of items: 2
Items: 
Size: 748826 Color: 4
Size: 251069 Color: 0

Bin 2859: 106 of cap free
Amount of items: 2
Items: 
Size: 752558 Color: 2
Size: 247337 Color: 0

Bin 2860: 106 of cap free
Amount of items: 2
Items: 
Size: 556537 Color: 0
Size: 443358 Color: 4

Bin 2861: 106 of cap free
Amount of items: 2
Items: 
Size: 576375 Color: 3
Size: 423520 Color: 2

Bin 2862: 106 of cap free
Amount of items: 2
Items: 
Size: 629572 Color: 4
Size: 370323 Color: 1

Bin 2863: 106 of cap free
Amount of items: 2
Items: 
Size: 751444 Color: 4
Size: 248451 Color: 3

Bin 2864: 106 of cap free
Amount of items: 2
Items: 
Size: 755420 Color: 1
Size: 244475 Color: 3

Bin 2865: 106 of cap free
Amount of items: 2
Items: 
Size: 597445 Color: 3
Size: 402450 Color: 0

Bin 2866: 107 of cap free
Amount of items: 2
Items: 
Size: 670073 Color: 4
Size: 329821 Color: 0

Bin 2867: 107 of cap free
Amount of items: 2
Items: 
Size: 763795 Color: 4
Size: 236099 Color: 2

Bin 2868: 107 of cap free
Amount of items: 2
Items: 
Size: 514557 Color: 4
Size: 485337 Color: 2

Bin 2869: 107 of cap free
Amount of items: 2
Items: 
Size: 520908 Color: 1
Size: 478986 Color: 4

Bin 2870: 107 of cap free
Amount of items: 2
Items: 
Size: 654250 Color: 4
Size: 345644 Color: 2

Bin 2871: 107 of cap free
Amount of items: 2
Items: 
Size: 746611 Color: 3
Size: 253283 Color: 1

Bin 2872: 107 of cap free
Amount of items: 2
Items: 
Size: 678192 Color: 2
Size: 321702 Color: 3

Bin 2873: 107 of cap free
Amount of items: 2
Items: 
Size: 697659 Color: 2
Size: 302235 Color: 3

Bin 2874: 107 of cap free
Amount of items: 2
Items: 
Size: 615978 Color: 0
Size: 383916 Color: 4

Bin 2875: 108 of cap free
Amount of items: 2
Items: 
Size: 554415 Color: 2
Size: 445478 Color: 1

Bin 2876: 108 of cap free
Amount of items: 2
Items: 
Size: 531846 Color: 2
Size: 468047 Color: 3

Bin 2877: 108 of cap free
Amount of items: 2
Items: 
Size: 541487 Color: 3
Size: 458406 Color: 0

Bin 2878: 108 of cap free
Amount of items: 2
Items: 
Size: 500898 Color: 0
Size: 498995 Color: 3

Bin 2879: 109 of cap free
Amount of items: 2
Items: 
Size: 770488 Color: 2
Size: 229404 Color: 4

Bin 2880: 109 of cap free
Amount of items: 2
Items: 
Size: 771643 Color: 4
Size: 228249 Color: 3

Bin 2881: 109 of cap free
Amount of items: 2
Items: 
Size: 578857 Color: 2
Size: 421035 Color: 3

Bin 2882: 109 of cap free
Amount of items: 2
Items: 
Size: 660974 Color: 3
Size: 338918 Color: 0

Bin 2883: 109 of cap free
Amount of items: 2
Items: 
Size: 584223 Color: 2
Size: 415669 Color: 4

Bin 2884: 109 of cap free
Amount of items: 2
Items: 
Size: 590329 Color: 1
Size: 409563 Color: 0

Bin 2885: 109 of cap free
Amount of items: 2
Items: 
Size: 691549 Color: 2
Size: 308343 Color: 0

Bin 2886: 109 of cap free
Amount of items: 2
Items: 
Size: 719339 Color: 3
Size: 280553 Color: 2

Bin 2887: 109 of cap free
Amount of items: 2
Items: 
Size: 550592 Color: 1
Size: 449300 Color: 0

Bin 2888: 109 of cap free
Amount of items: 2
Items: 
Size: 693366 Color: 0
Size: 306526 Color: 4

Bin 2889: 109 of cap free
Amount of items: 2
Items: 
Size: 749930 Color: 2
Size: 249962 Color: 3

Bin 2890: 110 of cap free
Amount of items: 2
Items: 
Size: 709296 Color: 2
Size: 290595 Color: 1

Bin 2891: 110 of cap free
Amount of items: 2
Items: 
Size: 640217 Color: 2
Size: 359674 Color: 1

Bin 2892: 110 of cap free
Amount of items: 2
Items: 
Size: 609719 Color: 1
Size: 390172 Color: 2

Bin 2893: 111 of cap free
Amount of items: 3
Items: 
Size: 614423 Color: 0
Size: 193670 Color: 0
Size: 191797 Color: 2

Bin 2894: 111 of cap free
Amount of items: 2
Items: 
Size: 526120 Color: 2
Size: 473770 Color: 4

Bin 2895: 111 of cap free
Amount of items: 2
Items: 
Size: 726102 Color: 4
Size: 273788 Color: 1

Bin 2896: 111 of cap free
Amount of items: 3
Items: 
Size: 376301 Color: 2
Size: 323831 Color: 0
Size: 299758 Color: 3

Bin 2897: 111 of cap free
Amount of items: 2
Items: 
Size: 586294 Color: 1
Size: 413596 Color: 2

Bin 2898: 111 of cap free
Amount of items: 2
Items: 
Size: 736518 Color: 0
Size: 263372 Color: 2

Bin 2899: 111 of cap free
Amount of items: 2
Items: 
Size: 669731 Color: 1
Size: 330159 Color: 0

Bin 2900: 112 of cap free
Amount of items: 3
Items: 
Size: 680089 Color: 2
Size: 163201 Color: 4
Size: 156599 Color: 0

Bin 2901: 112 of cap free
Amount of items: 2
Items: 
Size: 691985 Color: 2
Size: 307904 Color: 1

Bin 2902: 112 of cap free
Amount of items: 2
Items: 
Size: 729259 Color: 0
Size: 270630 Color: 4

Bin 2903: 112 of cap free
Amount of items: 2
Items: 
Size: 571604 Color: 2
Size: 428285 Color: 3

Bin 2904: 112 of cap free
Amount of items: 2
Items: 
Size: 585312 Color: 1
Size: 414577 Color: 4

Bin 2905: 113 of cap free
Amount of items: 2
Items: 
Size: 527050 Color: 4
Size: 472838 Color: 3

Bin 2906: 113 of cap free
Amount of items: 2
Items: 
Size: 528381 Color: 0
Size: 471507 Color: 2

Bin 2907: 113 of cap free
Amount of items: 2
Items: 
Size: 549411 Color: 2
Size: 450477 Color: 3

Bin 2908: 113 of cap free
Amount of items: 2
Items: 
Size: 688279 Color: 3
Size: 311609 Color: 0

Bin 2909: 113 of cap free
Amount of items: 2
Items: 
Size: 697511 Color: 4
Size: 302377 Color: 1

Bin 2910: 113 of cap free
Amount of items: 2
Items: 
Size: 648363 Color: 4
Size: 351525 Color: 3

Bin 2911: 113 of cap free
Amount of items: 2
Items: 
Size: 555099 Color: 4
Size: 444789 Color: 0

Bin 2912: 114 of cap free
Amount of items: 2
Items: 
Size: 524786 Color: 0
Size: 475101 Color: 1

Bin 2913: 114 of cap free
Amount of items: 2
Items: 
Size: 534428 Color: 3
Size: 465459 Color: 2

Bin 2914: 114 of cap free
Amount of items: 2
Items: 
Size: 557159 Color: 2
Size: 442728 Color: 3

Bin 2915: 114 of cap free
Amount of items: 2
Items: 
Size: 607773 Color: 2
Size: 392114 Color: 1

Bin 2916: 114 of cap free
Amount of items: 2
Items: 
Size: 585777 Color: 3
Size: 414110 Color: 2

Bin 2917: 114 of cap free
Amount of items: 2
Items: 
Size: 536645 Color: 2
Size: 463242 Color: 0

Bin 2918: 115 of cap free
Amount of items: 2
Items: 
Size: 741456 Color: 1
Size: 258430 Color: 4

Bin 2919: 115 of cap free
Amount of items: 2
Items: 
Size: 515725 Color: 4
Size: 484161 Color: 0

Bin 2920: 115 of cap free
Amount of items: 2
Items: 
Size: 528073 Color: 0
Size: 471813 Color: 1

Bin 2921: 115 of cap free
Amount of items: 2
Items: 
Size: 599053 Color: 2
Size: 400833 Color: 0

Bin 2922: 115 of cap free
Amount of items: 2
Items: 
Size: 615972 Color: 2
Size: 383914 Color: 0

Bin 2923: 115 of cap free
Amount of items: 2
Items: 
Size: 681594 Color: 2
Size: 318292 Color: 0

Bin 2924: 115 of cap free
Amount of items: 2
Items: 
Size: 707729 Color: 1
Size: 292157 Color: 4

Bin 2925: 115 of cap free
Amount of items: 2
Items: 
Size: 770107 Color: 4
Size: 229779 Color: 0

Bin 2926: 115 of cap free
Amount of items: 2
Items: 
Size: 542747 Color: 0
Size: 457139 Color: 1

Bin 2927: 115 of cap free
Amount of items: 2
Items: 
Size: 609368 Color: 1
Size: 390518 Color: 3

Bin 2928: 115 of cap free
Amount of items: 2
Items: 
Size: 644987 Color: 4
Size: 354899 Color: 3

Bin 2929: 115 of cap free
Amount of items: 2
Items: 
Size: 630340 Color: 1
Size: 369546 Color: 4

Bin 2930: 116 of cap free
Amount of items: 2
Items: 
Size: 670215 Color: 2
Size: 329670 Color: 1

Bin 2931: 116 of cap free
Amount of items: 2
Items: 
Size: 549600 Color: 0
Size: 450285 Color: 4

Bin 2932: 116 of cap free
Amount of items: 2
Items: 
Size: 577874 Color: 1
Size: 422011 Color: 4

Bin 2933: 116 of cap free
Amount of items: 2
Items: 
Size: 642360 Color: 4
Size: 357525 Color: 1

Bin 2934: 116 of cap free
Amount of items: 2
Items: 
Size: 716319 Color: 1
Size: 283566 Color: 3

Bin 2935: 116 of cap free
Amount of items: 2
Items: 
Size: 545770 Color: 0
Size: 454115 Color: 4

Bin 2936: 117 of cap free
Amount of items: 2
Items: 
Size: 749245 Color: 2
Size: 250639 Color: 0

Bin 2937: 117 of cap free
Amount of items: 3
Items: 
Size: 478679 Color: 2
Size: 265588 Color: 0
Size: 255617 Color: 2

Bin 2938: 117 of cap free
Amount of items: 2
Items: 
Size: 511317 Color: 2
Size: 488567 Color: 4

Bin 2939: 117 of cap free
Amount of items: 3
Items: 
Size: 670499 Color: 0
Size: 168792 Color: 2
Size: 160593 Color: 2

Bin 2940: 118 of cap free
Amount of items: 2
Items: 
Size: 632106 Color: 4
Size: 367777 Color: 3

Bin 2941: 118 of cap free
Amount of items: 2
Items: 
Size: 546558 Color: 2
Size: 453325 Color: 4

Bin 2942: 119 of cap free
Amount of items: 2
Items: 
Size: 547170 Color: 4
Size: 452712 Color: 0

Bin 2943: 119 of cap free
Amount of items: 2
Items: 
Size: 587263 Color: 2
Size: 412619 Color: 1

Bin 2944: 119 of cap free
Amount of items: 2
Items: 
Size: 602665 Color: 2
Size: 397217 Color: 1

Bin 2945: 119 of cap free
Amount of items: 2
Items: 
Size: 610534 Color: 3
Size: 389348 Color: 2

Bin 2946: 119 of cap free
Amount of items: 2
Items: 
Size: 641490 Color: 2
Size: 358392 Color: 1

Bin 2947: 119 of cap free
Amount of items: 2
Items: 
Size: 680328 Color: 4
Size: 319554 Color: 1

Bin 2948: 119 of cap free
Amount of items: 2
Items: 
Size: 698677 Color: 3
Size: 301205 Color: 2

Bin 2949: 119 of cap free
Amount of items: 2
Items: 
Size: 758276 Color: 4
Size: 241606 Color: 3

Bin 2950: 120 of cap free
Amount of items: 2
Items: 
Size: 594806 Color: 4
Size: 405075 Color: 3

Bin 2951: 120 of cap free
Amount of items: 2
Items: 
Size: 647997 Color: 0
Size: 351884 Color: 2

Bin 2952: 120 of cap free
Amount of items: 2
Items: 
Size: 680012 Color: 4
Size: 319869 Color: 2

Bin 2953: 120 of cap free
Amount of items: 2
Items: 
Size: 790434 Color: 4
Size: 209447 Color: 0

Bin 2954: 120 of cap free
Amount of items: 2
Items: 
Size: 584826 Color: 2
Size: 415055 Color: 0

Bin 2955: 120 of cap free
Amount of items: 2
Items: 
Size: 582396 Color: 3
Size: 417485 Color: 1

Bin 2956: 120 of cap free
Amount of items: 2
Items: 
Size: 647857 Color: 1
Size: 352024 Color: 4

Bin 2957: 120 of cap free
Amount of items: 2
Items: 
Size: 649056 Color: 0
Size: 350825 Color: 1

Bin 2958: 121 of cap free
Amount of items: 2
Items: 
Size: 787216 Color: 1
Size: 212664 Color: 3

Bin 2959: 121 of cap free
Amount of items: 2
Items: 
Size: 792516 Color: 1
Size: 207364 Color: 0

Bin 2960: 121 of cap free
Amount of items: 2
Items: 
Size: 760458 Color: 4
Size: 239422 Color: 3

Bin 2961: 121 of cap free
Amount of items: 2
Items: 
Size: 775688 Color: 1
Size: 224192 Color: 2

Bin 2962: 121 of cap free
Amount of items: 2
Items: 
Size: 669431 Color: 2
Size: 330449 Color: 4

Bin 2963: 121 of cap free
Amount of items: 2
Items: 
Size: 583166 Color: 1
Size: 416714 Color: 3

Bin 2964: 121 of cap free
Amount of items: 2
Items: 
Size: 768081 Color: 3
Size: 231799 Color: 2

Bin 2965: 121 of cap free
Amount of items: 2
Items: 
Size: 600802 Color: 0
Size: 399078 Color: 3

Bin 2966: 121 of cap free
Amount of items: 2
Items: 
Size: 680995 Color: 0
Size: 318885 Color: 4

Bin 2967: 122 of cap free
Amount of items: 2
Items: 
Size: 548829 Color: 3
Size: 451050 Color: 4

Bin 2968: 122 of cap free
Amount of items: 2
Items: 
Size: 617015 Color: 2
Size: 382864 Color: 1

Bin 2969: 122 of cap free
Amount of items: 2
Items: 
Size: 630778 Color: 0
Size: 369101 Color: 4

Bin 2970: 122 of cap free
Amount of items: 2
Items: 
Size: 664729 Color: 2
Size: 335150 Color: 0

Bin 2971: 122 of cap free
Amount of items: 2
Items: 
Size: 677299 Color: 2
Size: 322580 Color: 0

Bin 2972: 122 of cap free
Amount of items: 2
Items: 
Size: 564976 Color: 3
Size: 434903 Color: 4

Bin 2973: 122 of cap free
Amount of items: 2
Items: 
Size: 713069 Color: 3
Size: 286810 Color: 4

Bin 2974: 122 of cap free
Amount of items: 2
Items: 
Size: 584772 Color: 0
Size: 415107 Color: 2

Bin 2975: 123 of cap free
Amount of items: 2
Items: 
Size: 665735 Color: 1
Size: 334143 Color: 4

Bin 2976: 123 of cap free
Amount of items: 2
Items: 
Size: 546520 Color: 4
Size: 453358 Color: 2

Bin 2977: 123 of cap free
Amount of items: 2
Items: 
Size: 565182 Color: 0
Size: 434696 Color: 2

Bin 2978: 123 of cap free
Amount of items: 2
Items: 
Size: 553367 Color: 2
Size: 446511 Color: 1

Bin 2979: 124 of cap free
Amount of items: 2
Items: 
Size: 737250 Color: 3
Size: 262627 Color: 2

Bin 2980: 124 of cap free
Amount of items: 2
Items: 
Size: 524229 Color: 1
Size: 475648 Color: 0

Bin 2981: 124 of cap free
Amount of items: 2
Items: 
Size: 574682 Color: 0
Size: 425195 Color: 4

Bin 2982: 124 of cap free
Amount of items: 2
Items: 
Size: 512743 Color: 1
Size: 487134 Color: 4

Bin 2983: 124 of cap free
Amount of items: 2
Items: 
Size: 739320 Color: 4
Size: 260557 Color: 2

Bin 2984: 125 of cap free
Amount of items: 2
Items: 
Size: 536466 Color: 2
Size: 463410 Color: 1

Bin 2985: 125 of cap free
Amount of items: 2
Items: 
Size: 590145 Color: 1
Size: 409731 Color: 4

Bin 2986: 125 of cap free
Amount of items: 2
Items: 
Size: 695043 Color: 2
Size: 304833 Color: 4

Bin 2987: 125 of cap free
Amount of items: 2
Items: 
Size: 544488 Color: 2
Size: 455388 Color: 3

Bin 2988: 125 of cap free
Amount of items: 2
Items: 
Size: 605958 Color: 3
Size: 393918 Color: 0

Bin 2989: 126 of cap free
Amount of items: 2
Items: 
Size: 651289 Color: 2
Size: 348586 Color: 3

Bin 2990: 126 of cap free
Amount of items: 2
Items: 
Size: 557170 Color: 3
Size: 442705 Color: 2

Bin 2991: 126 of cap free
Amount of items: 2
Items: 
Size: 609871 Color: 1
Size: 390004 Color: 0

Bin 2992: 126 of cap free
Amount of items: 2
Items: 
Size: 753050 Color: 3
Size: 246825 Color: 1

Bin 2993: 127 of cap free
Amount of items: 2
Items: 
Size: 587905 Color: 1
Size: 411969 Color: 3

Bin 2994: 127 of cap free
Amount of items: 2
Items: 
Size: 531716 Color: 3
Size: 468158 Color: 2

Bin 2995: 127 of cap free
Amount of items: 2
Items: 
Size: 572639 Color: 1
Size: 427235 Color: 4

Bin 2996: 127 of cap free
Amount of items: 2
Items: 
Size: 572087 Color: 1
Size: 427787 Color: 2

Bin 2997: 127 of cap free
Amount of items: 2
Items: 
Size: 772344 Color: 0
Size: 227530 Color: 3

Bin 2998: 128 of cap free
Amount of items: 2
Items: 
Size: 791661 Color: 2
Size: 208212 Color: 0

Bin 2999: 128 of cap free
Amount of items: 3
Items: 
Size: 702984 Color: 3
Size: 149353 Color: 0
Size: 147536 Color: 3

Bin 3000: 128 of cap free
Amount of items: 2
Items: 
Size: 569557 Color: 1
Size: 430316 Color: 4

Bin 3001: 128 of cap free
Amount of items: 2
Items: 
Size: 607914 Color: 3
Size: 391959 Color: 4

Bin 3002: 128 of cap free
Amount of items: 2
Items: 
Size: 623504 Color: 2
Size: 376369 Color: 3

Bin 3003: 128 of cap free
Amount of items: 2
Items: 
Size: 755747 Color: 1
Size: 244126 Color: 0

Bin 3004: 128 of cap free
Amount of items: 2
Items: 
Size: 760653 Color: 1
Size: 239220 Color: 2

Bin 3005: 128 of cap free
Amount of items: 3
Items: 
Size: 338687 Color: 1
Size: 338194 Color: 3
Size: 322992 Color: 3

Bin 3006: 129 of cap free
Amount of items: 2
Items: 
Size: 699120 Color: 2
Size: 300752 Color: 1

Bin 3007: 129 of cap free
Amount of items: 2
Items: 
Size: 616633 Color: 2
Size: 383239 Color: 0

Bin 3008: 129 of cap free
Amount of items: 2
Items: 
Size: 571089 Color: 1
Size: 428783 Color: 3

Bin 3009: 129 of cap free
Amount of items: 2
Items: 
Size: 757609 Color: 3
Size: 242263 Color: 1

Bin 3010: 129 of cap free
Amount of items: 2
Items: 
Size: 604632 Color: 4
Size: 395240 Color: 1

Bin 3011: 129 of cap free
Amount of items: 2
Items: 
Size: 781733 Color: 3
Size: 218139 Color: 1

Bin 3012: 130 of cap free
Amount of items: 2
Items: 
Size: 543392 Color: 2
Size: 456479 Color: 4

Bin 3013: 130 of cap free
Amount of items: 2
Items: 
Size: 629708 Color: 2
Size: 370163 Color: 0

Bin 3014: 130 of cap free
Amount of items: 2
Items: 
Size: 734500 Color: 0
Size: 265371 Color: 3

Bin 3015: 131 of cap free
Amount of items: 2
Items: 
Size: 519540 Color: 3
Size: 480330 Color: 4

Bin 3016: 131 of cap free
Amount of items: 2
Items: 
Size: 626184 Color: 4
Size: 373686 Color: 2

Bin 3017: 131 of cap free
Amount of items: 2
Items: 
Size: 747084 Color: 3
Size: 252786 Color: 4

Bin 3018: 131 of cap free
Amount of items: 2
Items: 
Size: 718508 Color: 0
Size: 281362 Color: 2

Bin 3019: 131 of cap free
Amount of items: 2
Items: 
Size: 550906 Color: 0
Size: 448964 Color: 2

Bin 3020: 131 of cap free
Amount of items: 2
Items: 
Size: 584554 Color: 1
Size: 415316 Color: 0

Bin 3021: 132 of cap free
Amount of items: 2
Items: 
Size: 589703 Color: 4
Size: 410166 Color: 0

Bin 3022: 132 of cap free
Amount of items: 2
Items: 
Size: 525149 Color: 3
Size: 474720 Color: 4

Bin 3023: 132 of cap free
Amount of items: 2
Items: 
Size: 542730 Color: 2
Size: 457139 Color: 0

Bin 3024: 133 of cap free
Amount of items: 2
Items: 
Size: 762445 Color: 2
Size: 237423 Color: 4

Bin 3025: 133 of cap free
Amount of items: 2
Items: 
Size: 643493 Color: 4
Size: 356375 Color: 3

Bin 3026: 133 of cap free
Amount of items: 2
Items: 
Size: 571440 Color: 0
Size: 428428 Color: 2

Bin 3027: 133 of cap free
Amount of items: 2
Items: 
Size: 638531 Color: 0
Size: 361337 Color: 3

Bin 3028: 133 of cap free
Amount of items: 2
Items: 
Size: 543371 Color: 0
Size: 456497 Color: 2

Bin 3029: 133 of cap free
Amount of items: 2
Items: 
Size: 551826 Color: 2
Size: 448042 Color: 4

Bin 3030: 133 of cap free
Amount of items: 2
Items: 
Size: 570090 Color: 3
Size: 429778 Color: 4

Bin 3031: 133 of cap free
Amount of items: 2
Items: 
Size: 663940 Color: 4
Size: 335928 Color: 3

Bin 3032: 133 of cap free
Amount of items: 2
Items: 
Size: 515460 Color: 4
Size: 484408 Color: 2

Bin 3033: 133 of cap free
Amount of items: 2
Items: 
Size: 595533 Color: 1
Size: 404335 Color: 4

Bin 3034: 133 of cap free
Amount of items: 2
Items: 
Size: 567670 Color: 3
Size: 432198 Color: 1

Bin 3035: 133 of cap free
Amount of items: 2
Items: 
Size: 519733 Color: 2
Size: 480135 Color: 1

Bin 3036: 134 of cap free
Amount of items: 2
Items: 
Size: 721971 Color: 4
Size: 277896 Color: 3

Bin 3037: 134 of cap free
Amount of items: 2
Items: 
Size: 644427 Color: 1
Size: 355440 Color: 3

Bin 3038: 135 of cap free
Amount of items: 2
Items: 
Size: 645645 Color: 4
Size: 354221 Color: 1

Bin 3039: 135 of cap free
Amount of items: 2
Items: 
Size: 511758 Color: 3
Size: 488108 Color: 1

Bin 3040: 135 of cap free
Amount of items: 2
Items: 
Size: 584215 Color: 4
Size: 415651 Color: 3

Bin 3041: 135 of cap free
Amount of items: 3
Items: 
Size: 739473 Color: 2
Size: 130960 Color: 3
Size: 129433 Color: 0

Bin 3042: 135 of cap free
Amount of items: 2
Items: 
Size: 798770 Color: 3
Size: 201096 Color: 4

Bin 3043: 135 of cap free
Amount of items: 2
Items: 
Size: 798699 Color: 0
Size: 201167 Color: 3

Bin 3044: 136 of cap free
Amount of items: 2
Items: 
Size: 645984 Color: 2
Size: 353881 Color: 1

Bin 3045: 136 of cap free
Amount of items: 2
Items: 
Size: 768721 Color: 0
Size: 231144 Color: 4

Bin 3046: 136 of cap free
Amount of items: 2
Items: 
Size: 724496 Color: 1
Size: 275369 Color: 0

Bin 3047: 136 of cap free
Amount of items: 2
Items: 
Size: 594915 Color: 3
Size: 404950 Color: 1

Bin 3048: 136 of cap free
Amount of items: 2
Items: 
Size: 656971 Color: 1
Size: 342894 Color: 0

Bin 3049: 136 of cap free
Amount of items: 2
Items: 
Size: 697910 Color: 3
Size: 301955 Color: 4

Bin 3050: 137 of cap free
Amount of items: 2
Items: 
Size: 517582 Color: 1
Size: 482282 Color: 0

Bin 3051: 137 of cap free
Amount of items: 3
Items: 
Size: 593016 Color: 3
Size: 203429 Color: 4
Size: 203419 Color: 2

Bin 3052: 138 of cap free
Amount of items: 2
Items: 
Size: 519284 Color: 4
Size: 480579 Color: 1

Bin 3053: 138 of cap free
Amount of items: 2
Items: 
Size: 686004 Color: 2
Size: 313859 Color: 1

Bin 3054: 138 of cap free
Amount of items: 2
Items: 
Size: 704829 Color: 2
Size: 295034 Color: 3

Bin 3055: 138 of cap free
Amount of items: 2
Items: 
Size: 538683 Color: 1
Size: 461180 Color: 3

Bin 3056: 139 of cap free
Amount of items: 2
Items: 
Size: 583860 Color: 2
Size: 416002 Color: 3

Bin 3057: 139 of cap free
Amount of items: 2
Items: 
Size: 713769 Color: 4
Size: 286093 Color: 1

Bin 3058: 139 of cap free
Amount of items: 2
Items: 
Size: 729256 Color: 2
Size: 270606 Color: 4

Bin 3059: 139 of cap free
Amount of items: 2
Items: 
Size: 686242 Color: 2
Size: 313620 Color: 1

Bin 3060: 139 of cap free
Amount of items: 2
Items: 
Size: 558209 Color: 1
Size: 441653 Color: 0

Bin 3061: 140 of cap free
Amount of items: 2
Items: 
Size: 775937 Color: 1
Size: 223924 Color: 3

Bin 3062: 140 of cap free
Amount of items: 3
Items: 
Size: 413320 Color: 2
Size: 316790 Color: 0
Size: 269751 Color: 0

Bin 3063: 140 of cap free
Amount of items: 2
Items: 
Size: 771919 Color: 3
Size: 227942 Color: 4

Bin 3064: 140 of cap free
Amount of items: 2
Items: 
Size: 750268 Color: 1
Size: 249593 Color: 3

Bin 3065: 140 of cap free
Amount of items: 2
Items: 
Size: 618581 Color: 3
Size: 381280 Color: 2

Bin 3066: 140 of cap free
Amount of items: 2
Items: 
Size: 665202 Color: 3
Size: 334659 Color: 1

Bin 3067: 141 of cap free
Amount of items: 2
Items: 
Size: 666521 Color: 0
Size: 333339 Color: 4

Bin 3068: 141 of cap free
Amount of items: 2
Items: 
Size: 795448 Color: 1
Size: 204412 Color: 3

Bin 3069: 141 of cap free
Amount of items: 2
Items: 
Size: 662068 Color: 3
Size: 337792 Color: 4

Bin 3070: 141 of cap free
Amount of items: 2
Items: 
Size: 580456 Color: 2
Size: 419404 Color: 0

Bin 3071: 141 of cap free
Amount of items: 2
Items: 
Size: 587415 Color: 3
Size: 412445 Color: 1

Bin 3072: 141 of cap free
Amount of items: 2
Items: 
Size: 763392 Color: 3
Size: 236468 Color: 0

Bin 3073: 141 of cap free
Amount of items: 2
Items: 
Size: 757033 Color: 3
Size: 242827 Color: 4

Bin 3074: 142 of cap free
Amount of items: 2
Items: 
Size: 694478 Color: 1
Size: 305381 Color: 3

Bin 3075: 142 of cap free
Amount of items: 2
Items: 
Size: 582873 Color: 0
Size: 416986 Color: 3

Bin 3076: 142 of cap free
Amount of items: 2
Items: 
Size: 759217 Color: 0
Size: 240642 Color: 1

Bin 3077: 142 of cap free
Amount of items: 2
Items: 
Size: 642050 Color: 3
Size: 357809 Color: 2

Bin 3078: 142 of cap free
Amount of items: 2
Items: 
Size: 518970 Color: 0
Size: 480889 Color: 1

Bin 3079: 143 of cap free
Amount of items: 2
Items: 
Size: 783373 Color: 0
Size: 216485 Color: 3

Bin 3080: 144 of cap free
Amount of items: 2
Items: 
Size: 552375 Color: 2
Size: 447482 Color: 4

Bin 3081: 144 of cap free
Amount of items: 2
Items: 
Size: 664996 Color: 2
Size: 334861 Color: 3

Bin 3082: 144 of cap free
Amount of items: 2
Items: 
Size: 701404 Color: 0
Size: 298453 Color: 3

Bin 3083: 144 of cap free
Amount of items: 2
Items: 
Size: 660088 Color: 1
Size: 339769 Color: 2

Bin 3084: 144 of cap free
Amount of items: 2
Items: 
Size: 643038 Color: 4
Size: 356819 Color: 1

Bin 3085: 144 of cap free
Amount of items: 2
Items: 
Size: 502837 Color: 0
Size: 497020 Color: 3

Bin 3086: 145 of cap free
Amount of items: 2
Items: 
Size: 599880 Color: 4
Size: 399976 Color: 3

Bin 3087: 145 of cap free
Amount of items: 2
Items: 
Size: 511958 Color: 2
Size: 487898 Color: 1

Bin 3088: 145 of cap free
Amount of items: 2
Items: 
Size: 587092 Color: 0
Size: 412764 Color: 2

Bin 3089: 145 of cap free
Amount of items: 2
Items: 
Size: 596097 Color: 3
Size: 403759 Color: 1

Bin 3090: 146 of cap free
Amount of items: 2
Items: 
Size: 513082 Color: 4
Size: 486773 Color: 0

Bin 3091: 146 of cap free
Amount of items: 2
Items: 
Size: 535880 Color: 3
Size: 463975 Color: 0

Bin 3092: 146 of cap free
Amount of items: 2
Items: 
Size: 514270 Color: 3
Size: 485585 Color: 1

Bin 3093: 147 of cap free
Amount of items: 2
Items: 
Size: 596068 Color: 0
Size: 403786 Color: 3

Bin 3094: 147 of cap free
Amount of items: 2
Items: 
Size: 504731 Color: 2
Size: 495123 Color: 3

Bin 3095: 147 of cap free
Amount of items: 2
Items: 
Size: 537605 Color: 3
Size: 462249 Color: 0

Bin 3096: 147 of cap free
Amount of items: 2
Items: 
Size: 620425 Color: 2
Size: 379429 Color: 1

Bin 3097: 147 of cap free
Amount of items: 2
Items: 
Size: 638526 Color: 1
Size: 361328 Color: 2

Bin 3098: 147 of cap free
Amount of items: 2
Items: 
Size: 665561 Color: 1
Size: 334293 Color: 3

Bin 3099: 147 of cap free
Amount of items: 2
Items: 
Size: 618315 Color: 4
Size: 381539 Color: 1

Bin 3100: 147 of cap free
Amount of items: 2
Items: 
Size: 762159 Color: 1
Size: 237695 Color: 2

Bin 3101: 148 of cap free
Amount of items: 2
Items: 
Size: 550072 Color: 4
Size: 449781 Color: 0

Bin 3102: 148 of cap free
Amount of items: 2
Items: 
Size: 640221 Color: 1
Size: 359632 Color: 0

Bin 3103: 148 of cap free
Amount of items: 2
Items: 
Size: 704669 Color: 4
Size: 295184 Color: 2

Bin 3104: 148 of cap free
Amount of items: 2
Items: 
Size: 760653 Color: 0
Size: 239200 Color: 4

Bin 3105: 148 of cap free
Amount of items: 2
Items: 
Size: 786603 Color: 2
Size: 213250 Color: 0

Bin 3106: 148 of cap free
Amount of items: 2
Items: 
Size: 790409 Color: 2
Size: 209444 Color: 1

Bin 3107: 148 of cap free
Amount of items: 2
Items: 
Size: 799967 Color: 0
Size: 199886 Color: 4

Bin 3108: 148 of cap free
Amount of items: 2
Items: 
Size: 541190 Color: 4
Size: 458663 Color: 0

Bin 3109: 148 of cap free
Amount of items: 2
Items: 
Size: 718933 Color: 0
Size: 280920 Color: 3

Bin 3110: 148 of cap free
Amount of items: 2
Items: 
Size: 635237 Color: 3
Size: 364616 Color: 2

Bin 3111: 149 of cap free
Amount of items: 2
Items: 
Size: 738392 Color: 0
Size: 261460 Color: 3

Bin 3112: 149 of cap free
Amount of items: 2
Items: 
Size: 633996 Color: 1
Size: 365856 Color: 0

Bin 3113: 149 of cap free
Amount of items: 2
Items: 
Size: 755469 Color: 3
Size: 244383 Color: 1

Bin 3114: 149 of cap free
Amount of items: 2
Items: 
Size: 523631 Color: 1
Size: 476221 Color: 4

Bin 3115: 150 of cap free
Amount of items: 2
Items: 
Size: 655306 Color: 1
Size: 344545 Color: 4

Bin 3116: 150 of cap free
Amount of items: 3
Items: 
Size: 705251 Color: 4
Size: 147356 Color: 2
Size: 147244 Color: 3

Bin 3117: 150 of cap free
Amount of items: 2
Items: 
Size: 705481 Color: 2
Size: 294370 Color: 4

Bin 3118: 150 of cap free
Amount of items: 2
Items: 
Size: 752688 Color: 4
Size: 247163 Color: 3

Bin 3119: 150 of cap free
Amount of items: 2
Items: 
Size: 506446 Color: 0
Size: 493405 Color: 2

Bin 3120: 150 of cap free
Amount of items: 2
Items: 
Size: 532035 Color: 4
Size: 467816 Color: 1

Bin 3121: 150 of cap free
Amount of items: 2
Items: 
Size: 560328 Color: 4
Size: 439523 Color: 0

Bin 3122: 150 of cap free
Amount of items: 2
Items: 
Size: 577053 Color: 0
Size: 422798 Color: 2

Bin 3123: 150 of cap free
Amount of items: 2
Items: 
Size: 682531 Color: 0
Size: 317320 Color: 4

Bin 3124: 150 of cap free
Amount of items: 2
Items: 
Size: 579891 Color: 4
Size: 419960 Color: 0

Bin 3125: 151 of cap free
Amount of items: 2
Items: 
Size: 575501 Color: 2
Size: 424349 Color: 4

Bin 3126: 151 of cap free
Amount of items: 2
Items: 
Size: 614937 Color: 4
Size: 384913 Color: 0

Bin 3127: 151 of cap free
Amount of items: 2
Items: 
Size: 666193 Color: 1
Size: 333657 Color: 0

Bin 3128: 151 of cap free
Amount of items: 2
Items: 
Size: 522591 Color: 2
Size: 477259 Color: 3

Bin 3129: 152 of cap free
Amount of items: 2
Items: 
Size: 691508 Color: 2
Size: 308341 Color: 0

Bin 3130: 152 of cap free
Amount of items: 2
Items: 
Size: 569514 Color: 4
Size: 430335 Color: 0

Bin 3131: 152 of cap free
Amount of items: 2
Items: 
Size: 734519 Color: 3
Size: 265330 Color: 4

Bin 3132: 152 of cap free
Amount of items: 2
Items: 
Size: 689527 Color: 3
Size: 310322 Color: 0

Bin 3133: 152 of cap free
Amount of items: 2
Items: 
Size: 790035 Color: 1
Size: 209814 Color: 2

Bin 3134: 152 of cap free
Amount of items: 2
Items: 
Size: 672463 Color: 0
Size: 327386 Color: 1

Bin 3135: 153 of cap free
Amount of items: 2
Items: 
Size: 705027 Color: 1
Size: 294821 Color: 2

Bin 3136: 153 of cap free
Amount of items: 2
Items: 
Size: 750790 Color: 2
Size: 249058 Color: 0

Bin 3137: 153 of cap free
Amount of items: 2
Items: 
Size: 545971 Color: 0
Size: 453877 Color: 1

Bin 3138: 153 of cap free
Amount of items: 2
Items: 
Size: 610822 Color: 4
Size: 389026 Color: 0

Bin 3139: 154 of cap free
Amount of items: 3
Items: 
Size: 494092 Color: 3
Size: 253674 Color: 0
Size: 252081 Color: 1

Bin 3140: 154 of cap free
Amount of items: 2
Items: 
Size: 699970 Color: 3
Size: 299877 Color: 2

Bin 3141: 154 of cap free
Amount of items: 2
Items: 
Size: 751553 Color: 1
Size: 248294 Color: 4

Bin 3142: 155 of cap free
Amount of items: 2
Items: 
Size: 792818 Color: 2
Size: 207028 Color: 4

Bin 3143: 155 of cap free
Amount of items: 2
Items: 
Size: 792511 Color: 0
Size: 207335 Color: 3

Bin 3144: 155 of cap free
Amount of items: 2
Items: 
Size: 595240 Color: 4
Size: 404606 Color: 0

Bin 3145: 155 of cap free
Amount of items: 2
Items: 
Size: 570385 Color: 4
Size: 429461 Color: 2

Bin 3146: 156 of cap free
Amount of items: 2
Items: 
Size: 784882 Color: 0
Size: 214963 Color: 1

Bin 3147: 156 of cap free
Amount of items: 2
Items: 
Size: 655720 Color: 1
Size: 344125 Color: 0

Bin 3148: 156 of cap free
Amount of items: 2
Items: 
Size: 745305 Color: 0
Size: 254540 Color: 2

Bin 3149: 157 of cap free
Amount of items: 2
Items: 
Size: 532455 Color: 4
Size: 467389 Color: 0

Bin 3150: 157 of cap free
Amount of items: 2
Items: 
Size: 632591 Color: 1
Size: 367253 Color: 3

Bin 3151: 157 of cap free
Amount of items: 2
Items: 
Size: 686814 Color: 0
Size: 313030 Color: 1

Bin 3152: 158 of cap free
Amount of items: 2
Items: 
Size: 545138 Color: 4
Size: 454705 Color: 0

Bin 3153: 158 of cap free
Amount of items: 2
Items: 
Size: 757956 Color: 0
Size: 241887 Color: 4

Bin 3154: 158 of cap free
Amount of items: 2
Items: 
Size: 601849 Color: 2
Size: 397994 Color: 1

Bin 3155: 158 of cap free
Amount of items: 2
Items: 
Size: 782518 Color: 4
Size: 217325 Color: 3

Bin 3156: 158 of cap free
Amount of items: 2
Items: 
Size: 656270 Color: 2
Size: 343573 Color: 3

Bin 3157: 159 of cap free
Amount of items: 2
Items: 
Size: 509987 Color: 0
Size: 489855 Color: 1

Bin 3158: 159 of cap free
Amount of items: 2
Items: 
Size: 701828 Color: 4
Size: 298014 Color: 1

Bin 3159: 159 of cap free
Amount of items: 2
Items: 
Size: 743516 Color: 0
Size: 256326 Color: 2

Bin 3160: 160 of cap free
Amount of items: 2
Items: 
Size: 516696 Color: 0
Size: 483145 Color: 2

Bin 3161: 160 of cap free
Amount of items: 2
Items: 
Size: 605633 Color: 1
Size: 394208 Color: 4

Bin 3162: 160 of cap free
Amount of items: 2
Items: 
Size: 664731 Color: 0
Size: 335110 Color: 1

Bin 3163: 160 of cap free
Amount of items: 2
Items: 
Size: 697391 Color: 2
Size: 302450 Color: 4

Bin 3164: 160 of cap free
Amount of items: 2
Items: 
Size: 791465 Color: 1
Size: 208376 Color: 4

Bin 3165: 160 of cap free
Amount of items: 2
Items: 
Size: 765083 Color: 0
Size: 234758 Color: 2

Bin 3166: 161 of cap free
Amount of items: 2
Items: 
Size: 749230 Color: 0
Size: 250610 Color: 2

Bin 3167: 161 of cap free
Amount of items: 2
Items: 
Size: 672479 Color: 1
Size: 327361 Color: 4

Bin 3168: 161 of cap free
Amount of items: 2
Items: 
Size: 727843 Color: 1
Size: 271997 Color: 2

Bin 3169: 162 of cap free
Amount of items: 2
Items: 
Size: 695185 Color: 4
Size: 304654 Color: 0

Bin 3170: 162 of cap free
Amount of items: 2
Items: 
Size: 556661 Color: 3
Size: 443178 Color: 2

Bin 3171: 162 of cap free
Amount of items: 2
Items: 
Size: 701008 Color: 2
Size: 298831 Color: 3

Bin 3172: 162 of cap free
Amount of items: 2
Items: 
Size: 652459 Color: 1
Size: 347380 Color: 0

Bin 3173: 163 of cap free
Amount of items: 2
Items: 
Size: 543357 Color: 0
Size: 456481 Color: 2

Bin 3174: 163 of cap free
Amount of items: 2
Items: 
Size: 597643 Color: 3
Size: 402195 Color: 4

Bin 3175: 164 of cap free
Amount of items: 2
Items: 
Size: 547809 Color: 2
Size: 452028 Color: 0

Bin 3176: 164 of cap free
Amount of items: 2
Items: 
Size: 503221 Color: 3
Size: 496616 Color: 4

Bin 3177: 164 of cap free
Amount of items: 2
Items: 
Size: 509199 Color: 0
Size: 490638 Color: 1

Bin 3178: 164 of cap free
Amount of items: 2
Items: 
Size: 577288 Color: 3
Size: 422549 Color: 4

Bin 3179: 164 of cap free
Amount of items: 2
Items: 
Size: 724519 Color: 0
Size: 275318 Color: 1

Bin 3180: 164 of cap free
Amount of items: 2
Items: 
Size: 756041 Color: 4
Size: 243796 Color: 3

Bin 3181: 164 of cap free
Amount of items: 2
Items: 
Size: 555690 Color: 3
Size: 444147 Color: 0

Bin 3182: 165 of cap free
Amount of items: 2
Items: 
Size: 560221 Color: 1
Size: 439615 Color: 4

Bin 3183: 165 of cap free
Amount of items: 2
Items: 
Size: 563398 Color: 2
Size: 436438 Color: 4

Bin 3184: 165 of cap free
Amount of items: 2
Items: 
Size: 583359 Color: 1
Size: 416477 Color: 2

Bin 3185: 165 of cap free
Amount of items: 2
Items: 
Size: 712481 Color: 1
Size: 287355 Color: 0

Bin 3186: 165 of cap free
Amount of items: 2
Items: 
Size: 773370 Color: 0
Size: 226466 Color: 3

Bin 3187: 165 of cap free
Amount of items: 2
Items: 
Size: 740569 Color: 0
Size: 259267 Color: 2

Bin 3188: 166 of cap free
Amount of items: 2
Items: 
Size: 550890 Color: 4
Size: 448945 Color: 3

Bin 3189: 166 of cap free
Amount of items: 2
Items: 
Size: 651280 Color: 1
Size: 348555 Color: 4

Bin 3190: 166 of cap free
Amount of items: 2
Items: 
Size: 699521 Color: 2
Size: 300314 Color: 0

Bin 3191: 166 of cap free
Amount of items: 2
Items: 
Size: 750412 Color: 2
Size: 249423 Color: 4

Bin 3192: 166 of cap free
Amount of items: 2
Items: 
Size: 518102 Color: 4
Size: 481733 Color: 0

Bin 3193: 166 of cap free
Amount of items: 2
Items: 
Size: 792509 Color: 3
Size: 207326 Color: 2

Bin 3194: 167 of cap free
Amount of items: 3
Items: 
Size: 728382 Color: 2
Size: 136194 Color: 2
Size: 135258 Color: 4

Bin 3195: 167 of cap free
Amount of items: 2
Items: 
Size: 793523 Color: 4
Size: 206311 Color: 1

Bin 3196: 167 of cap free
Amount of items: 2
Items: 
Size: 637462 Color: 4
Size: 362372 Color: 2

Bin 3197: 167 of cap free
Amount of items: 2
Items: 
Size: 531213 Color: 0
Size: 468621 Color: 1

Bin 3198: 167 of cap free
Amount of items: 2
Items: 
Size: 692745 Color: 1
Size: 307089 Color: 3

Bin 3199: 167 of cap free
Amount of items: 2
Items: 
Size: 704655 Color: 2
Size: 295179 Color: 0

Bin 3200: 167 of cap free
Amount of items: 2
Items: 
Size: 550543 Color: 2
Size: 449291 Color: 0

Bin 3201: 168 of cap free
Amount of items: 2
Items: 
Size: 716615 Color: 4
Size: 283218 Color: 2

Bin 3202: 168 of cap free
Amount of items: 2
Items: 
Size: 532024 Color: 3
Size: 467809 Color: 2

Bin 3203: 168 of cap free
Amount of items: 2
Items: 
Size: 639077 Color: 4
Size: 360756 Color: 1

Bin 3204: 168 of cap free
Amount of items: 2
Items: 
Size: 504041 Color: 2
Size: 495792 Color: 3

Bin 3205: 169 of cap free
Amount of items: 2
Items: 
Size: 774389 Color: 1
Size: 225443 Color: 2

Bin 3206: 169 of cap free
Amount of items: 2
Items: 
Size: 651967 Color: 0
Size: 347865 Color: 2

Bin 3207: 169 of cap free
Amount of items: 2
Items: 
Size: 530986 Color: 1
Size: 468846 Color: 0

Bin 3208: 169 of cap free
Amount of items: 2
Items: 
Size: 658691 Color: 3
Size: 341141 Color: 0

Bin 3209: 169 of cap free
Amount of items: 2
Items: 
Size: 597432 Color: 0
Size: 402400 Color: 4

Bin 3210: 169 of cap free
Amount of items: 2
Items: 
Size: 636868 Color: 2
Size: 362964 Color: 1

Bin 3211: 170 of cap free
Amount of items: 2
Items: 
Size: 725126 Color: 2
Size: 274705 Color: 0

Bin 3212: 170 of cap free
Amount of items: 2
Items: 
Size: 780371 Color: 0
Size: 219460 Color: 4

Bin 3213: 170 of cap free
Amount of items: 2
Items: 
Size: 795050 Color: 4
Size: 204781 Color: 0

Bin 3214: 170 of cap free
Amount of items: 2
Items: 
Size: 539219 Color: 3
Size: 460612 Color: 1

Bin 3215: 170 of cap free
Amount of items: 2
Items: 
Size: 602955 Color: 3
Size: 396876 Color: 0

Bin 3216: 171 of cap free
Amount of items: 2
Items: 
Size: 526068 Color: 1
Size: 473762 Color: 2

Bin 3217: 171 of cap free
Amount of items: 3
Items: 
Size: 623209 Color: 4
Size: 189971 Color: 3
Size: 186650 Color: 2

Bin 3218: 171 of cap free
Amount of items: 2
Items: 
Size: 676390 Color: 2
Size: 323440 Color: 0

Bin 3219: 171 of cap free
Amount of items: 2
Items: 
Size: 508530 Color: 0
Size: 491300 Color: 3

Bin 3220: 171 of cap free
Amount of items: 2
Items: 
Size: 576318 Color: 3
Size: 423512 Color: 2

Bin 3221: 171 of cap free
Amount of items: 2
Items: 
Size: 527839 Color: 2
Size: 471991 Color: 3

Bin 3222: 172 of cap free
Amount of items: 2
Items: 
Size: 624383 Color: 3
Size: 375446 Color: 0

Bin 3223: 172 of cap free
Amount of items: 3
Items: 
Size: 672912 Color: 2
Size: 163492 Color: 3
Size: 163425 Color: 3

Bin 3224: 172 of cap free
Amount of items: 2
Items: 
Size: 506427 Color: 0
Size: 493402 Color: 2

Bin 3225: 172 of cap free
Amount of items: 2
Items: 
Size: 517152 Color: 4
Size: 482677 Color: 2

Bin 3226: 172 of cap free
Amount of items: 2
Items: 
Size: 728363 Color: 4
Size: 271466 Color: 2

Bin 3227: 172 of cap free
Amount of items: 2
Items: 
Size: 512126 Color: 1
Size: 487703 Color: 2

Bin 3228: 174 of cap free
Amount of items: 2
Items: 
Size: 536891 Color: 4
Size: 462936 Color: 2

Bin 3229: 174 of cap free
Amount of items: 2
Items: 
Size: 566916 Color: 4
Size: 432911 Color: 0

Bin 3230: 174 of cap free
Amount of items: 2
Items: 
Size: 744494 Color: 2
Size: 255333 Color: 1

Bin 3231: 174 of cap free
Amount of items: 2
Items: 
Size: 761290 Color: 0
Size: 238537 Color: 4

Bin 3232: 175 of cap free
Amount of items: 2
Items: 
Size: 657400 Color: 0
Size: 342426 Color: 2

Bin 3233: 175 of cap free
Amount of items: 2
Items: 
Size: 505182 Color: 4
Size: 494644 Color: 3

Bin 3234: 175 of cap free
Amount of items: 2
Items: 
Size: 582347 Color: 3
Size: 417479 Color: 4

Bin 3235: 175 of cap free
Amount of items: 2
Items: 
Size: 604038 Color: 2
Size: 395788 Color: 3

Bin 3236: 175 of cap free
Amount of items: 2
Items: 
Size: 719475 Color: 3
Size: 280351 Color: 2

Bin 3237: 175 of cap free
Amount of items: 2
Items: 
Size: 759834 Color: 4
Size: 239992 Color: 2

Bin 3238: 176 of cap free
Amount of items: 2
Items: 
Size: 770956 Color: 0
Size: 228869 Color: 4

Bin 3239: 176 of cap free
Amount of items: 2
Items: 
Size: 619923 Color: 1
Size: 379902 Color: 2

Bin 3240: 176 of cap free
Amount of items: 2
Items: 
Size: 649529 Color: 2
Size: 350296 Color: 0

Bin 3241: 177 of cap free
Amount of items: 2
Items: 
Size: 798056 Color: 0
Size: 201768 Color: 1

Bin 3242: 177 of cap free
Amount of items: 2
Items: 
Size: 569497 Color: 0
Size: 430327 Color: 1

Bin 3243: 177 of cap free
Amount of items: 2
Items: 
Size: 663302 Color: 2
Size: 336522 Color: 4

Bin 3244: 178 of cap free
Amount of items: 2
Items: 
Size: 729231 Color: 2
Size: 270592 Color: 1

Bin 3245: 178 of cap free
Amount of items: 2
Items: 
Size: 605907 Color: 4
Size: 393916 Color: 2

Bin 3246: 178 of cap free
Amount of items: 2
Items: 
Size: 759871 Color: 2
Size: 239952 Color: 4

Bin 3247: 178 of cap free
Amount of items: 2
Items: 
Size: 764282 Color: 3
Size: 235541 Color: 4

Bin 3248: 178 of cap free
Amount of items: 2
Items: 
Size: 514508 Color: 2
Size: 485315 Color: 1

Bin 3249: 179 of cap free
Amount of items: 2
Items: 
Size: 542343 Color: 3
Size: 457479 Color: 2

Bin 3250: 179 of cap free
Amount of items: 2
Items: 
Size: 619923 Color: 2
Size: 379899 Color: 1

Bin 3251: 180 of cap free
Amount of items: 2
Items: 
Size: 542694 Color: 3
Size: 457127 Color: 1

Bin 3252: 181 of cap free
Amount of items: 2
Items: 
Size: 520277 Color: 3
Size: 479543 Color: 0

Bin 3253: 181 of cap free
Amount of items: 2
Items: 
Size: 620816 Color: 1
Size: 379004 Color: 0

Bin 3254: 181 of cap free
Amount of items: 2
Items: 
Size: 561922 Color: 2
Size: 437898 Color: 4

Bin 3255: 181 of cap free
Amount of items: 2
Items: 
Size: 514726 Color: 2
Size: 485094 Color: 3

Bin 3256: 181 of cap free
Amount of items: 2
Items: 
Size: 591197 Color: 3
Size: 408623 Color: 2

Bin 3257: 181 of cap free
Amount of items: 2
Items: 
Size: 621008 Color: 3
Size: 378812 Color: 0

Bin 3258: 182 of cap free
Amount of items: 2
Items: 
Size: 597062 Color: 4
Size: 402757 Color: 1

Bin 3259: 182 of cap free
Amount of items: 2
Items: 
Size: 538654 Color: 3
Size: 461165 Color: 2

Bin 3260: 182 of cap free
Amount of items: 2
Items: 
Size: 725481 Color: 3
Size: 274338 Color: 2

Bin 3261: 183 of cap free
Amount of items: 2
Items: 
Size: 510271 Color: 2
Size: 489547 Color: 1

Bin 3262: 183 of cap free
Amount of items: 2
Items: 
Size: 553780 Color: 3
Size: 446038 Color: 2

Bin 3263: 183 of cap free
Amount of items: 2
Items: 
Size: 579463 Color: 2
Size: 420355 Color: 4

Bin 3264: 183 of cap free
Amount of items: 2
Items: 
Size: 669104 Color: 2
Size: 330714 Color: 4

Bin 3265: 183 of cap free
Amount of items: 2
Items: 
Size: 797240 Color: 1
Size: 202578 Color: 3

Bin 3266: 184 of cap free
Amount of items: 2
Items: 
Size: 537387 Color: 3
Size: 462430 Color: 2

Bin 3267: 184 of cap free
Amount of items: 2
Items: 
Size: 543395 Color: 4
Size: 456422 Color: 0

Bin 3268: 184 of cap free
Amount of items: 2
Items: 
Size: 718929 Color: 4
Size: 280888 Color: 0

Bin 3269: 184 of cap free
Amount of items: 2
Items: 
Size: 572783 Color: 0
Size: 427034 Color: 3

Bin 3270: 185 of cap free
Amount of items: 3
Items: 
Size: 662782 Color: 4
Size: 170270 Color: 3
Size: 166764 Color: 3

Bin 3271: 185 of cap free
Amount of items: 2
Items: 
Size: 569119 Color: 1
Size: 430697 Color: 3

Bin 3272: 185 of cap free
Amount of items: 2
Items: 
Size: 593950 Color: 0
Size: 405866 Color: 1

Bin 3273: 185 of cap free
Amount of items: 2
Items: 
Size: 631671 Color: 2
Size: 368145 Color: 3

Bin 3274: 186 of cap free
Amount of items: 2
Items: 
Size: 542323 Color: 0
Size: 457492 Color: 3

Bin 3275: 186 of cap free
Amount of items: 2
Items: 
Size: 739277 Color: 1
Size: 260538 Color: 4

Bin 3276: 186 of cap free
Amount of items: 2
Items: 
Size: 541984 Color: 3
Size: 457831 Color: 0

Bin 3277: 186 of cap free
Amount of items: 2
Items: 
Size: 572074 Color: 3
Size: 427741 Color: 0

Bin 3278: 187 of cap free
Amount of items: 2
Items: 
Size: 730795 Color: 1
Size: 269019 Color: 0

Bin 3279: 187 of cap free
Amount of items: 2
Items: 
Size: 551569 Color: 3
Size: 448245 Color: 4

Bin 3280: 188 of cap free
Amount of items: 2
Items: 
Size: 504972 Color: 2
Size: 494841 Color: 4

Bin 3281: 188 of cap free
Amount of items: 2
Items: 
Size: 520874 Color: 3
Size: 478939 Color: 4

Bin 3282: 188 of cap free
Amount of items: 2
Items: 
Size: 597921 Color: 3
Size: 401892 Color: 1

Bin 3283: 188 of cap free
Amount of items: 2
Items: 
Size: 717869 Color: 2
Size: 281944 Color: 1

Bin 3284: 188 of cap free
Amount of items: 2
Items: 
Size: 582335 Color: 4
Size: 417478 Color: 0

Bin 3285: 189 of cap free
Amount of items: 2
Items: 
Size: 749190 Color: 3
Size: 250622 Color: 0

Bin 3286: 189 of cap free
Amount of items: 2
Items: 
Size: 626150 Color: 2
Size: 373662 Color: 0

Bin 3287: 189 of cap free
Amount of items: 2
Items: 
Size: 692690 Color: 2
Size: 307122 Color: 1

Bin 3288: 189 of cap free
Amount of items: 2
Items: 
Size: 622685 Color: 0
Size: 377127 Color: 1

Bin 3289: 189 of cap free
Amount of items: 2
Items: 
Size: 637534 Color: 2
Size: 362278 Color: 4

Bin 3290: 189 of cap free
Amount of items: 2
Items: 
Size: 642517 Color: 0
Size: 357295 Color: 4

Bin 3291: 190 of cap free
Amount of items: 2
Items: 
Size: 566715 Color: 3
Size: 433096 Color: 1

Bin 3292: 190 of cap free
Amount of items: 2
Items: 
Size: 518484 Color: 2
Size: 481327 Color: 0

Bin 3293: 190 of cap free
Amount of items: 2
Items: 
Size: 680716 Color: 3
Size: 319095 Color: 1

Bin 3294: 190 of cap free
Amount of items: 2
Items: 
Size: 675104 Color: 2
Size: 324707 Color: 3

Bin 3295: 191 of cap free
Amount of items: 2
Items: 
Size: 713052 Color: 4
Size: 286758 Color: 1

Bin 3296: 191 of cap free
Amount of items: 2
Items: 
Size: 640824 Color: 0
Size: 358986 Color: 1

Bin 3297: 191 of cap free
Amount of items: 2
Items: 
Size: 608205 Color: 2
Size: 391605 Color: 3

Bin 3298: 191 of cap free
Amount of items: 2
Items: 
Size: 785169 Color: 2
Size: 214641 Color: 4

Bin 3299: 191 of cap free
Amount of items: 2
Items: 
Size: 679753 Color: 4
Size: 320057 Color: 0

Bin 3300: 192 of cap free
Amount of items: 2
Items: 
Size: 572388 Color: 4
Size: 427421 Color: 1

Bin 3301: 192 of cap free
Amount of items: 2
Items: 
Size: 711504 Color: 1
Size: 288305 Color: 4

Bin 3302: 192 of cap free
Amount of items: 2
Items: 
Size: 619182 Color: 1
Size: 380627 Color: 3

Bin 3303: 192 of cap free
Amount of items: 2
Items: 
Size: 757855 Color: 2
Size: 241954 Color: 0

Bin 3304: 192 of cap free
Amount of items: 2
Items: 
Size: 710782 Color: 1
Size: 289027 Color: 4

Bin 3305: 192 of cap free
Amount of items: 2
Items: 
Size: 518271 Color: 4
Size: 481538 Color: 0

Bin 3306: 193 of cap free
Amount of items: 2
Items: 
Size: 710512 Color: 4
Size: 289296 Color: 2

Bin 3307: 193 of cap free
Amount of items: 2
Items: 
Size: 714642 Color: 3
Size: 285166 Color: 4

Bin 3308: 193 of cap free
Amount of items: 2
Items: 
Size: 585543 Color: 4
Size: 414265 Color: 3

Bin 3309: 193 of cap free
Amount of items: 2
Items: 
Size: 593577 Color: 0
Size: 406231 Color: 1

Bin 3310: 194 of cap free
Amount of items: 2
Items: 
Size: 760429 Color: 4
Size: 239378 Color: 1

Bin 3311: 194 of cap free
Amount of items: 2
Items: 
Size: 700364 Color: 3
Size: 299443 Color: 1

Bin 3312: 195 of cap free
Amount of items: 2
Items: 
Size: 672229 Color: 4
Size: 327577 Color: 3

Bin 3313: 195 of cap free
Amount of items: 2
Items: 
Size: 714059 Color: 2
Size: 285747 Color: 1

Bin 3314: 195 of cap free
Amount of items: 2
Items: 
Size: 537081 Color: 0
Size: 462725 Color: 2

Bin 3315: 197 of cap free
Amount of items: 2
Items: 
Size: 772326 Color: 2
Size: 227478 Color: 3

Bin 3316: 197 of cap free
Amount of items: 2
Items: 
Size: 525120 Color: 1
Size: 474684 Color: 4

Bin 3317: 197 of cap free
Amount of items: 2
Items: 
Size: 576077 Color: 0
Size: 423727 Color: 3

Bin 3318: 198 of cap free
Amount of items: 2
Items: 
Size: 594339 Color: 2
Size: 405464 Color: 4

Bin 3319: 198 of cap free
Amount of items: 2
Items: 
Size: 779054 Color: 0
Size: 220749 Color: 2

Bin 3320: 199 of cap free
Amount of items: 2
Items: 
Size: 583836 Color: 1
Size: 415966 Color: 4

Bin 3321: 199 of cap free
Amount of items: 2
Items: 
Size: 575207 Color: 0
Size: 424595 Color: 1

Bin 3322: 199 of cap free
Amount of items: 3
Items: 
Size: 703979 Color: 2
Size: 147970 Color: 4
Size: 147853 Color: 3

Bin 3323: 199 of cap free
Amount of items: 2
Items: 
Size: 781253 Color: 0
Size: 218549 Color: 2

Bin 3324: 200 of cap free
Amount of items: 2
Items: 
Size: 698263 Color: 4
Size: 301538 Color: 2

Bin 3325: 200 of cap free
Amount of items: 2
Items: 
Size: 757605 Color: 2
Size: 242196 Color: 1

Bin 3326: 200 of cap free
Amount of items: 2
Items: 
Size: 662010 Color: 0
Size: 337791 Color: 2

Bin 3327: 200 of cap free
Amount of items: 2
Items: 
Size: 566332 Color: 2
Size: 433469 Color: 4

Bin 3328: 200 of cap free
Amount of items: 2
Items: 
Size: 627354 Color: 1
Size: 372447 Color: 0

Bin 3329: 201 of cap free
Amount of items: 2
Items: 
Size: 563724 Color: 2
Size: 436076 Color: 4

Bin 3330: 202 of cap free
Amount of items: 2
Items: 
Size: 660430 Color: 4
Size: 339369 Color: 2

Bin 3331: 202 of cap free
Amount of items: 2
Items: 
Size: 544859 Color: 2
Size: 454940 Color: 4

Bin 3332: 202 of cap free
Amount of items: 2
Items: 
Size: 610570 Color: 2
Size: 389229 Color: 1

Bin 3333: 202 of cap free
Amount of items: 2
Items: 
Size: 706848 Color: 2
Size: 292951 Color: 0

Bin 3334: 202 of cap free
Amount of items: 2
Items: 
Size: 781610 Color: 0
Size: 218189 Color: 3

Bin 3335: 203 of cap free
Amount of items: 2
Items: 
Size: 708560 Color: 2
Size: 291238 Color: 0

Bin 3336: 204 of cap free
Amount of items: 2
Items: 
Size: 596514 Color: 4
Size: 403283 Color: 3

Bin 3337: 205 of cap free
Amount of items: 2
Items: 
Size: 574668 Color: 1
Size: 425128 Color: 2

Bin 3338: 206 of cap free
Amount of items: 2
Items: 
Size: 715046 Color: 3
Size: 284749 Color: 4

Bin 3339: 206 of cap free
Amount of items: 2
Items: 
Size: 634537 Color: 3
Size: 365258 Color: 0

Bin 3340: 207 of cap free
Amount of items: 2
Items: 
Size: 756981 Color: 0
Size: 242813 Color: 2

Bin 3341: 207 of cap free
Amount of items: 2
Items: 
Size: 719461 Color: 2
Size: 280333 Color: 0

Bin 3342: 207 of cap free
Amount of items: 2
Items: 
Size: 524442 Color: 4
Size: 475352 Color: 2

Bin 3343: 208 of cap free
Amount of items: 3
Items: 
Size: 703116 Color: 1
Size: 148340 Color: 4
Size: 148337 Color: 4

Bin 3344: 208 of cap free
Amount of items: 2
Items: 
Size: 770703 Color: 4
Size: 229090 Color: 1

Bin 3345: 208 of cap free
Amount of items: 2
Items: 
Size: 680702 Color: 0
Size: 319091 Color: 1

Bin 3346: 209 of cap free
Amount of items: 2
Items: 
Size: 562698 Color: 2
Size: 437094 Color: 1

Bin 3347: 209 of cap free
Amount of items: 2
Items: 
Size: 708284 Color: 4
Size: 291508 Color: 2

Bin 3348: 210 of cap free
Amount of items: 2
Items: 
Size: 763570 Color: 2
Size: 236221 Color: 0

Bin 3349: 210 of cap free
Amount of items: 2
Items: 
Size: 605649 Color: 4
Size: 394142 Color: 2

Bin 3350: 210 of cap free
Amount of items: 2
Items: 
Size: 536380 Color: 1
Size: 463411 Color: 2

Bin 3351: 211 of cap free
Amount of items: 2
Items: 
Size: 599550 Color: 0
Size: 400240 Color: 3

Bin 3352: 211 of cap free
Amount of items: 2
Items: 
Size: 535585 Color: 4
Size: 464205 Color: 1

Bin 3353: 212 of cap free
Amount of items: 2
Items: 
Size: 540419 Color: 0
Size: 459370 Color: 1

Bin 3354: 212 of cap free
Amount of items: 2
Items: 
Size: 637053 Color: 4
Size: 362736 Color: 1

Bin 3355: 212 of cap free
Amount of items: 2
Items: 
Size: 669342 Color: 0
Size: 330447 Color: 4

Bin 3356: 212 of cap free
Amount of items: 2
Items: 
Size: 687921 Color: 3
Size: 311868 Color: 2

Bin 3357: 213 of cap free
Amount of items: 2
Items: 
Size: 633368 Color: 1
Size: 366420 Color: 3

Bin 3358: 214 of cap free
Amount of items: 2
Items: 
Size: 777334 Color: 3
Size: 222453 Color: 0

Bin 3359: 214 of cap free
Amount of items: 2
Items: 
Size: 732703 Color: 0
Size: 267084 Color: 3

Bin 3360: 214 of cap free
Amount of items: 2
Items: 
Size: 585504 Color: 3
Size: 414283 Color: 0

Bin 3361: 215 of cap free
Amount of items: 2
Items: 
Size: 674227 Color: 2
Size: 325559 Color: 0

Bin 3362: 215 of cap free
Amount of items: 2
Items: 
Size: 507954 Color: 0
Size: 491832 Color: 2

Bin 3363: 215 of cap free
Amount of items: 2
Items: 
Size: 612187 Color: 4
Size: 387599 Color: 3

Bin 3364: 216 of cap free
Amount of items: 2
Items: 
Size: 521604 Color: 4
Size: 478181 Color: 2

Bin 3365: 216 of cap free
Amount of items: 2
Items: 
Size: 667923 Color: 1
Size: 331862 Color: 0

Bin 3366: 216 of cap free
Amount of items: 2
Items: 
Size: 755689 Color: 1
Size: 244096 Color: 0

Bin 3367: 216 of cap free
Amount of items: 2
Items: 
Size: 749523 Color: 3
Size: 250262 Color: 0

Bin 3368: 217 of cap free
Amount of items: 2
Items: 
Size: 621245 Color: 1
Size: 378539 Color: 2

Bin 3369: 217 of cap free
Amount of items: 2
Items: 
Size: 660709 Color: 2
Size: 339075 Color: 0

Bin 3370: 218 of cap free
Amount of items: 2
Items: 
Size: 728883 Color: 1
Size: 270900 Color: 4

Bin 3371: 218 of cap free
Amount of items: 2
Items: 
Size: 589052 Color: 3
Size: 410731 Color: 4

Bin 3372: 218 of cap free
Amount of items: 2
Items: 
Size: 504019 Color: 2
Size: 495764 Color: 3

Bin 3373: 218 of cap free
Amount of items: 2
Items: 
Size: 734435 Color: 1
Size: 265348 Color: 3

Bin 3374: 219 of cap free
Amount of items: 2
Items: 
Size: 697770 Color: 4
Size: 302012 Color: 3

Bin 3375: 219 of cap free
Amount of items: 2
Items: 
Size: 535192 Color: 1
Size: 464590 Color: 0

Bin 3376: 219 of cap free
Amount of items: 2
Items: 
Size: 766676 Color: 3
Size: 233106 Color: 4

Bin 3377: 219 of cap free
Amount of items: 2
Items: 
Size: 794125 Color: 4
Size: 205657 Color: 3

Bin 3378: 220 of cap free
Amount of items: 2
Items: 
Size: 696194 Color: 1
Size: 303587 Color: 4

Bin 3379: 220 of cap free
Amount of items: 2
Items: 
Size: 748745 Color: 0
Size: 251036 Color: 3

Bin 3380: 221 of cap free
Amount of items: 2
Items: 
Size: 761249 Color: 0
Size: 238531 Color: 4

Bin 3381: 221 of cap free
Amount of items: 2
Items: 
Size: 689298 Color: 2
Size: 310482 Color: 1

Bin 3382: 221 of cap free
Amount of items: 2
Items: 
Size: 541445 Color: 4
Size: 458335 Color: 3

Bin 3383: 221 of cap free
Amount of items: 2
Items: 
Size: 614348 Color: 3
Size: 385432 Color: 4

Bin 3384: 221 of cap free
Amount of items: 2
Items: 
Size: 512103 Color: 1
Size: 487677 Color: 3

Bin 3385: 222 of cap free
Amount of items: 2
Items: 
Size: 762838 Color: 2
Size: 236941 Color: 1

Bin 3386: 222 of cap free
Amount of items: 2
Items: 
Size: 676374 Color: 1
Size: 323405 Color: 3

Bin 3387: 222 of cap free
Amount of items: 2
Items: 
Size: 690541 Color: 4
Size: 309238 Color: 1

Bin 3388: 222 of cap free
Amount of items: 2
Items: 
Size: 696736 Color: 4
Size: 303043 Color: 1

Bin 3389: 222 of cap free
Amount of items: 2
Items: 
Size: 744249 Color: 1
Size: 255530 Color: 0

Bin 3390: 222 of cap free
Amount of items: 2
Items: 
Size: 616626 Color: 4
Size: 383153 Color: 2

Bin 3391: 222 of cap free
Amount of items: 2
Items: 
Size: 600453 Color: 1
Size: 399326 Color: 0

Bin 3392: 222 of cap free
Amount of items: 2
Items: 
Size: 655699 Color: 3
Size: 344080 Color: 0

Bin 3393: 224 of cap free
Amount of items: 2
Items: 
Size: 632936 Color: 4
Size: 366841 Color: 2

Bin 3394: 225 of cap free
Amount of items: 2
Items: 
Size: 594784 Color: 2
Size: 404992 Color: 3

Bin 3395: 225 of cap free
Amount of items: 2
Items: 
Size: 590408 Color: 0
Size: 409368 Color: 4

Bin 3396: 225 of cap free
Amount of items: 2
Items: 
Size: 754951 Color: 2
Size: 244825 Color: 0

Bin 3397: 225 of cap free
Amount of items: 2
Items: 
Size: 559523 Color: 2
Size: 440253 Color: 1

Bin 3398: 225 of cap free
Amount of items: 2
Items: 
Size: 742728 Color: 3
Size: 257048 Color: 0

Bin 3399: 226 of cap free
Amount of items: 2
Items: 
Size: 627328 Color: 0
Size: 372447 Color: 4

Bin 3400: 226 of cap free
Amount of items: 2
Items: 
Size: 639299 Color: 3
Size: 360476 Color: 2

Bin 3401: 226 of cap free
Amount of items: 2
Items: 
Size: 516328 Color: 1
Size: 483447 Color: 4

Bin 3402: 227 of cap free
Amount of items: 2
Items: 
Size: 609715 Color: 4
Size: 390059 Color: 1

Bin 3403: 227 of cap free
Amount of items: 2
Items: 
Size: 527172 Color: 4
Size: 472602 Color: 2

Bin 3404: 227 of cap free
Amount of items: 2
Items: 
Size: 761758 Color: 2
Size: 238016 Color: 1

Bin 3405: 227 of cap free
Amount of items: 2
Items: 
Size: 649531 Color: 0
Size: 350243 Color: 4

Bin 3406: 228 of cap free
Amount of items: 2
Items: 
Size: 716563 Color: 1
Size: 283210 Color: 3

Bin 3407: 229 of cap free
Amount of items: 2
Items: 
Size: 533170 Color: 1
Size: 466602 Color: 4

Bin 3408: 229 of cap free
Amount of items: 2
Items: 
Size: 601779 Color: 2
Size: 397993 Color: 4

Bin 3409: 229 of cap free
Amount of items: 2
Items: 
Size: 738796 Color: 4
Size: 260976 Color: 3

Bin 3410: 230 of cap free
Amount of items: 2
Items: 
Size: 792725 Color: 1
Size: 207046 Color: 2

Bin 3411: 230 of cap free
Amount of items: 2
Items: 
Size: 789380 Color: 2
Size: 210391 Color: 3

Bin 3412: 230 of cap free
Amount of items: 2
Items: 
Size: 568535 Color: 3
Size: 431236 Color: 4

Bin 3413: 230 of cap free
Amount of items: 2
Items: 
Size: 580101 Color: 0
Size: 419670 Color: 1

Bin 3414: 231 of cap free
Amount of items: 2
Items: 
Size: 737647 Color: 3
Size: 262123 Color: 4

Bin 3415: 232 of cap free
Amount of items: 2
Items: 
Size: 508905 Color: 3
Size: 490864 Color: 4

Bin 3416: 233 of cap free
Amount of items: 2
Items: 
Size: 519475 Color: 4
Size: 480293 Color: 2

Bin 3417: 233 of cap free
Amount of items: 2
Items: 
Size: 524438 Color: 4
Size: 475330 Color: 1

Bin 3418: 233 of cap free
Amount of items: 2
Items: 
Size: 595185 Color: 4
Size: 404583 Color: 1

Bin 3419: 233 of cap free
Amount of items: 2
Items: 
Size: 673557 Color: 3
Size: 326211 Color: 2

Bin 3420: 233 of cap free
Amount of items: 2
Items: 
Size: 793146 Color: 0
Size: 206622 Color: 4

Bin 3421: 233 of cap free
Amount of items: 2
Items: 
Size: 533166 Color: 0
Size: 466602 Color: 1

Bin 3422: 234 of cap free
Amount of items: 2
Items: 
Size: 761838 Color: 1
Size: 237929 Color: 4

Bin 3423: 234 of cap free
Amount of items: 2
Items: 
Size: 605861 Color: 1
Size: 393906 Color: 0

Bin 3424: 236 of cap free
Amount of items: 2
Items: 
Size: 709369 Color: 1
Size: 290396 Color: 3

Bin 3425: 236 of cap free
Amount of items: 2
Items: 
Size: 633709 Color: 4
Size: 366056 Color: 0

Bin 3426: 236 of cap free
Amount of items: 2
Items: 
Size: 515124 Color: 0
Size: 484641 Color: 1

Bin 3427: 236 of cap free
Amount of items: 2
Items: 
Size: 713038 Color: 3
Size: 286727 Color: 0

Bin 3428: 237 of cap free
Amount of items: 2
Items: 
Size: 592436 Color: 3
Size: 407328 Color: 1

Bin 3429: 238 of cap free
Amount of items: 2
Items: 
Size: 779039 Color: 0
Size: 220724 Color: 2

Bin 3430: 238 of cap free
Amount of items: 2
Items: 
Size: 771418 Color: 0
Size: 228345 Color: 4

Bin 3431: 238 of cap free
Amount of items: 2
Items: 
Size: 598329 Color: 1
Size: 401434 Color: 3

Bin 3432: 238 of cap free
Amount of items: 2
Items: 
Size: 634873 Color: 1
Size: 364890 Color: 4

Bin 3433: 239 of cap free
Amount of items: 2
Items: 
Size: 628021 Color: 3
Size: 371741 Color: 2

Bin 3434: 239 of cap free
Amount of items: 2
Items: 
Size: 697439 Color: 4
Size: 302323 Color: 2

Bin 3435: 239 of cap free
Amount of items: 2
Items: 
Size: 510785 Color: 2
Size: 488977 Color: 3

Bin 3436: 239 of cap free
Amount of items: 2
Items: 
Size: 554169 Color: 3
Size: 445593 Color: 4

Bin 3437: 239 of cap free
Amount of items: 2
Items: 
Size: 663926 Color: 2
Size: 335836 Color: 1

Bin 3438: 240 of cap free
Amount of items: 2
Items: 
Size: 550508 Color: 3
Size: 449253 Color: 4

Bin 3439: 240 of cap free
Amount of items: 2
Items: 
Size: 773006 Color: 0
Size: 226755 Color: 1

Bin 3440: 240 of cap free
Amount of items: 2
Items: 
Size: 787652 Color: 2
Size: 212109 Color: 0

Bin 3441: 241 of cap free
Amount of items: 2
Items: 
Size: 649001 Color: 2
Size: 350759 Color: 3

Bin 3442: 241 of cap free
Amount of items: 2
Items: 
Size: 570405 Color: 2
Size: 429355 Color: 3

Bin 3443: 241 of cap free
Amount of items: 2
Items: 
Size: 563259 Color: 4
Size: 436501 Color: 2

Bin 3444: 241 of cap free
Amount of items: 2
Items: 
Size: 578073 Color: 1
Size: 421687 Color: 3

Bin 3445: 241 of cap free
Amount of items: 2
Items: 
Size: 680348 Color: 1
Size: 319412 Color: 2

Bin 3446: 241 of cap free
Amount of items: 2
Items: 
Size: 710470 Color: 3
Size: 289290 Color: 4

Bin 3447: 241 of cap free
Amount of items: 2
Items: 
Size: 593500 Color: 3
Size: 406260 Color: 0

Bin 3448: 241 of cap free
Amount of items: 2
Items: 
Size: 727555 Color: 1
Size: 272205 Color: 3

Bin 3449: 242 of cap free
Amount of items: 2
Items: 
Size: 781581 Color: 1
Size: 218178 Color: 3

Bin 3450: 242 of cap free
Amount of items: 2
Items: 
Size: 690523 Color: 4
Size: 309236 Color: 0

Bin 3451: 242 of cap free
Amount of items: 2
Items: 
Size: 799949 Color: 4
Size: 199810 Color: 2

Bin 3452: 242 of cap free
Amount of items: 2
Items: 
Size: 635995 Color: 3
Size: 363764 Color: 0

Bin 3453: 242 of cap free
Amount of items: 2
Items: 
Size: 689884 Color: 3
Size: 309875 Color: 2

Bin 3454: 243 of cap free
Amount of items: 2
Items: 
Size: 747717 Color: 0
Size: 252041 Color: 2

Bin 3455: 244 of cap free
Amount of items: 2
Items: 
Size: 607453 Color: 4
Size: 392304 Color: 2

Bin 3456: 244 of cap free
Amount of items: 2
Items: 
Size: 718474 Color: 3
Size: 281283 Color: 1

Bin 3457: 245 of cap free
Amount of items: 2
Items: 
Size: 581467 Color: 3
Size: 418289 Color: 4

Bin 3458: 245 of cap free
Amount of items: 2
Items: 
Size: 648361 Color: 2
Size: 351395 Color: 4

Bin 3459: 246 of cap free
Amount of items: 2
Items: 
Size: 742702 Color: 2
Size: 257053 Color: 3

Bin 3460: 246 of cap free
Amount of items: 2
Items: 
Size: 527863 Color: 3
Size: 471892 Color: 0

Bin 3461: 246 of cap free
Amount of items: 2
Items: 
Size: 553582 Color: 4
Size: 446173 Color: 3

Bin 3462: 246 of cap free
Amount of items: 2
Items: 
Size: 662747 Color: 4
Size: 337008 Color: 3

Bin 3463: 246 of cap free
Amount of items: 2
Items: 
Size: 544848 Color: 0
Size: 454907 Color: 3

Bin 3464: 247 of cap free
Amount of items: 2
Items: 
Size: 606606 Color: 4
Size: 393148 Color: 2

Bin 3465: 247 of cap free
Amount of items: 2
Items: 
Size: 562016 Color: 4
Size: 437738 Color: 0

Bin 3466: 247 of cap free
Amount of items: 2
Items: 
Size: 599036 Color: 1
Size: 400718 Color: 3

Bin 3467: 247 of cap free
Amount of items: 2
Items: 
Size: 765414 Color: 0
Size: 234340 Color: 4

Bin 3468: 248 of cap free
Amount of items: 2
Items: 
Size: 785124 Color: 0
Size: 214629 Color: 3

Bin 3469: 249 of cap free
Amount of items: 3
Items: 
Size: 625379 Color: 4
Size: 187443 Color: 3
Size: 186930 Color: 1

Bin 3470: 249 of cap free
Amount of items: 2
Items: 
Size: 796372 Color: 0
Size: 203380 Color: 4

Bin 3471: 250 of cap free
Amount of items: 2
Items: 
Size: 564958 Color: 2
Size: 434793 Color: 0

Bin 3472: 250 of cap free
Amount of items: 2
Items: 
Size: 797660 Color: 3
Size: 202091 Color: 0

Bin 3473: 251 of cap free
Amount of items: 2
Items: 
Size: 758662 Color: 4
Size: 241088 Color: 2

Bin 3474: 251 of cap free
Amount of items: 2
Items: 
Size: 780361 Color: 1
Size: 219389 Color: 0

Bin 3475: 251 of cap free
Amount of items: 2
Items: 
Size: 789740 Color: 0
Size: 210010 Color: 4

Bin 3476: 251 of cap free
Amount of items: 2
Items: 
Size: 678335 Color: 4
Size: 321415 Color: 2

Bin 3477: 253 of cap free
Amount of items: 2
Items: 
Size: 581280 Color: 1
Size: 418468 Color: 3

Bin 3478: 253 of cap free
Amount of items: 2
Items: 
Size: 500425 Color: 4
Size: 499323 Color: 0

Bin 3479: 254 of cap free
Amount of items: 2
Items: 
Size: 787960 Color: 0
Size: 211787 Color: 3

Bin 3480: 254 of cap free
Amount of items: 2
Items: 
Size: 781209 Color: 4
Size: 218538 Color: 1

Bin 3481: 254 of cap free
Amount of items: 2
Items: 
Size: 602415 Color: 0
Size: 397332 Color: 2

Bin 3482: 255 of cap free
Amount of items: 2
Items: 
Size: 599399 Color: 3
Size: 400347 Color: 0

Bin 3483: 256 of cap free
Amount of items: 2
Items: 
Size: 569512 Color: 1
Size: 430233 Color: 4

Bin 3484: 259 of cap free
Amount of items: 2
Items: 
Size: 695653 Color: 0
Size: 304089 Color: 2

Bin 3485: 259 of cap free
Amount of items: 2
Items: 
Size: 652100 Color: 2
Size: 347642 Color: 4

Bin 3486: 259 of cap free
Amount of items: 2
Items: 
Size: 779353 Color: 0
Size: 220389 Color: 4

Bin 3487: 262 of cap free
Amount of items: 2
Items: 
Size: 726028 Color: 0
Size: 273711 Color: 4

Bin 3488: 262 of cap free
Amount of items: 2
Items: 
Size: 543325 Color: 0
Size: 456414 Color: 3

Bin 3489: 262 of cap free
Amount of items: 2
Items: 
Size: 573321 Color: 2
Size: 426418 Color: 4

Bin 3490: 263 of cap free
Amount of items: 2
Items: 
Size: 643108 Color: 1
Size: 356630 Color: 0

Bin 3491: 263 of cap free
Amount of items: 2
Items: 
Size: 504006 Color: 4
Size: 495732 Color: 0

Bin 3492: 263 of cap free
Amount of items: 2
Items: 
Size: 701753 Color: 0
Size: 297985 Color: 4

Bin 3493: 263 of cap free
Amount of items: 2
Items: 
Size: 520199 Color: 3
Size: 479539 Color: 4

Bin 3494: 264 of cap free
Amount of items: 2
Items: 
Size: 589994 Color: 4
Size: 409743 Color: 1

Bin 3495: 264 of cap free
Amount of items: 2
Items: 
Size: 509454 Color: 3
Size: 490283 Color: 2

Bin 3496: 264 of cap free
Amount of items: 2
Items: 
Size: 673327 Color: 2
Size: 326410 Color: 4

Bin 3497: 265 of cap free
Amount of items: 2
Items: 
Size: 539728 Color: 1
Size: 460008 Color: 4

Bin 3498: 265 of cap free
Amount of items: 2
Items: 
Size: 732660 Color: 0
Size: 267076 Color: 2

Bin 3499: 266 of cap free
Amount of items: 2
Items: 
Size: 576025 Color: 3
Size: 423710 Color: 2

Bin 3500: 266 of cap free
Amount of items: 2
Items: 
Size: 615283 Color: 3
Size: 384452 Color: 0

Bin 3501: 266 of cap free
Amount of items: 2
Items: 
Size: 777905 Color: 3
Size: 221830 Color: 4

Bin 3502: 266 of cap free
Amount of items: 2
Items: 
Size: 755387 Color: 3
Size: 244348 Color: 2

Bin 3503: 266 of cap free
Amount of items: 2
Items: 
Size: 740129 Color: 4
Size: 259606 Color: 1

Bin 3504: 267 of cap free
Amount of items: 2
Items: 
Size: 731974 Color: 0
Size: 267760 Color: 1

Bin 3505: 267 of cap free
Amount of items: 2
Items: 
Size: 640108 Color: 2
Size: 359626 Color: 0

Bin 3506: 268 of cap free
Amount of items: 2
Items: 
Size: 728318 Color: 4
Size: 271415 Color: 3

Bin 3507: 270 of cap free
Amount of items: 2
Items: 
Size: 553573 Color: 1
Size: 446158 Color: 3

Bin 3508: 271 of cap free
Amount of items: 2
Items: 
Size: 765900 Color: 2
Size: 233830 Color: 4

Bin 3509: 271 of cap free
Amount of items: 2
Items: 
Size: 654386 Color: 0
Size: 345344 Color: 2

Bin 3510: 272 of cap free
Amount of items: 2
Items: 
Size: 601779 Color: 1
Size: 397950 Color: 2

Bin 3511: 272 of cap free
Amount of items: 2
Items: 
Size: 755908 Color: 0
Size: 243821 Color: 4

Bin 3512: 273 of cap free
Amount of items: 2
Items: 
Size: 522579 Color: 0
Size: 477149 Color: 2

Bin 3513: 273 of cap free
Amount of items: 2
Items: 
Size: 535180 Color: 3
Size: 464548 Color: 4

Bin 3514: 274 of cap free
Amount of items: 2
Items: 
Size: 694842 Color: 4
Size: 304885 Color: 3

Bin 3515: 275 of cap free
Amount of items: 2
Items: 
Size: 680724 Color: 1
Size: 319002 Color: 0

Bin 3516: 275 of cap free
Amount of items: 2
Items: 
Size: 681523 Color: 0
Size: 318203 Color: 3

Bin 3517: 275 of cap free
Amount of items: 2
Items: 
Size: 583182 Color: 3
Size: 416544 Color: 1

Bin 3518: 275 of cap free
Amount of items: 2
Items: 
Size: 616220 Color: 3
Size: 383506 Color: 1

Bin 3519: 276 of cap free
Amount of items: 2
Items: 
Size: 752394 Color: 4
Size: 247331 Color: 0

Bin 3520: 277 of cap free
Amount of items: 2
Items: 
Size: 698988 Color: 4
Size: 300736 Color: 3

Bin 3521: 277 of cap free
Amount of items: 2
Items: 
Size: 713729 Color: 2
Size: 285995 Color: 4

Bin 3522: 277 of cap free
Amount of items: 2
Items: 
Size: 724485 Color: 0
Size: 275239 Color: 3

Bin 3523: 278 of cap free
Amount of items: 2
Items: 
Size: 515861 Color: 3
Size: 483862 Color: 2

Bin 3524: 278 of cap free
Amount of items: 2
Items: 
Size: 552852 Color: 3
Size: 446871 Color: 1

Bin 3525: 279 of cap free
Amount of items: 2
Items: 
Size: 650276 Color: 1
Size: 349446 Color: 2

Bin 3526: 280 of cap free
Amount of items: 2
Items: 
Size: 626420 Color: 1
Size: 373301 Color: 4

Bin 3527: 280 of cap free
Amount of items: 2
Items: 
Size: 616571 Color: 2
Size: 383150 Color: 1

Bin 3528: 281 of cap free
Amount of items: 2
Items: 
Size: 640563 Color: 1
Size: 359157 Color: 0

Bin 3529: 283 of cap free
Amount of items: 2
Items: 
Size: 620396 Color: 4
Size: 379322 Color: 2

Bin 3530: 284 of cap free
Amount of items: 2
Items: 
Size: 560970 Color: 0
Size: 438747 Color: 2

Bin 3531: 284 of cap free
Amount of items: 2
Items: 
Size: 714357 Color: 0
Size: 285360 Color: 4

Bin 3532: 285 of cap free
Amount of items: 2
Items: 
Size: 585452 Color: 1
Size: 414264 Color: 2

Bin 3533: 286 of cap free
Amount of items: 2
Items: 
Size: 660040 Color: 4
Size: 339675 Color: 1

Bin 3534: 286 of cap free
Amount of items: 2
Items: 
Size: 562374 Color: 2
Size: 437341 Color: 0

Bin 3535: 286 of cap free
Amount of items: 2
Items: 
Size: 756322 Color: 1
Size: 243393 Color: 0

Bin 3536: 287 of cap free
Amount of items: 2
Items: 
Size: 719985 Color: 1
Size: 279729 Color: 4

Bin 3537: 287 of cap free
Amount of items: 2
Items: 
Size: 528898 Color: 3
Size: 470816 Color: 0

Bin 3538: 287 of cap free
Amount of items: 2
Items: 
Size: 638483 Color: 2
Size: 361231 Color: 4

Bin 3539: 288 of cap free
Amount of items: 2
Items: 
Size: 676734 Color: 4
Size: 322979 Color: 0

Bin 3540: 288 of cap free
Amount of items: 2
Items: 
Size: 517135 Color: 1
Size: 482578 Color: 4

Bin 3541: 289 of cap free
Amount of items: 2
Items: 
Size: 687118 Color: 3
Size: 312594 Color: 1

Bin 3542: 289 of cap free
Amount of items: 2
Items: 
Size: 515117 Color: 0
Size: 484595 Color: 2

Bin 3543: 289 of cap free
Amount of items: 2
Items: 
Size: 793396 Color: 3
Size: 206316 Color: 4

Bin 3544: 290 of cap free
Amount of items: 2
Items: 
Size: 605567 Color: 1
Size: 394144 Color: 4

Bin 3545: 291 of cap free
Amount of items: 2
Items: 
Size: 787634 Color: 2
Size: 212076 Color: 4

Bin 3546: 292 of cap free
Amount of items: 2
Items: 
Size: 606585 Color: 1
Size: 393124 Color: 4

Bin 3547: 292 of cap free
Amount of items: 2
Items: 
Size: 503601 Color: 2
Size: 496108 Color: 1

Bin 3548: 293 of cap free
Amount of items: 2
Items: 
Size: 588997 Color: 4
Size: 410711 Color: 1

Bin 3549: 294 of cap free
Amount of items: 2
Items: 
Size: 686072 Color: 1
Size: 313635 Color: 2

Bin 3550: 294 of cap free
Amount of items: 2
Items: 
Size: 539391 Color: 3
Size: 460316 Color: 0

Bin 3551: 294 of cap free
Amount of items: 2
Items: 
Size: 570654 Color: 4
Size: 429053 Color: 3

Bin 3552: 294 of cap free
Amount of items: 2
Items: 
Size: 604610 Color: 3
Size: 395097 Color: 1

Bin 3553: 295 of cap free
Amount of items: 2
Items: 
Size: 604243 Color: 2
Size: 395463 Color: 0

Bin 3554: 296 of cap free
Amount of items: 2
Items: 
Size: 586946 Color: 4
Size: 412759 Color: 3

Bin 3555: 297 of cap free
Amount of items: 2
Items: 
Size: 587413 Color: 1
Size: 412291 Color: 2

Bin 3556: 297 of cap free
Amount of items: 2
Items: 
Size: 789326 Color: 2
Size: 210378 Color: 1

Bin 3557: 297 of cap free
Amount of items: 2
Items: 
Size: 536967 Color: 2
Size: 462737 Color: 0

Bin 3558: 297 of cap free
Amount of items: 2
Items: 
Size: 741000 Color: 0
Size: 258704 Color: 1

Bin 3559: 298 of cap free
Amount of items: 2
Items: 
Size: 667557 Color: 4
Size: 332146 Color: 3

Bin 3560: 298 of cap free
Amount of items: 2
Items: 
Size: 768627 Color: 3
Size: 231076 Color: 2

Bin 3561: 299 of cap free
Amount of items: 2
Items: 
Size: 755376 Color: 0
Size: 244326 Color: 1

Bin 3562: 299 of cap free
Amount of items: 2
Items: 
Size: 582296 Color: 3
Size: 417406 Color: 0

Bin 3563: 300 of cap free
Amount of items: 2
Items: 
Size: 591209 Color: 2
Size: 408492 Color: 3

Bin 3564: 301 of cap free
Amount of items: 2
Items: 
Size: 757835 Color: 3
Size: 241865 Color: 0

Bin 3565: 303 of cap free
Amount of items: 2
Items: 
Size: 729416 Color: 2
Size: 270282 Color: 1

Bin 3566: 303 of cap free
Amount of items: 2
Items: 
Size: 756948 Color: 4
Size: 242750 Color: 0

Bin 3567: 304 of cap free
Amount of items: 2
Items: 
Size: 501904 Color: 1
Size: 497793 Color: 2

Bin 3568: 304 of cap free
Amount of items: 2
Items: 
Size: 553863 Color: 2
Size: 445834 Color: 0

Bin 3569: 304 of cap free
Amount of items: 2
Items: 
Size: 763602 Color: 0
Size: 236095 Color: 3

Bin 3570: 305 of cap free
Amount of items: 2
Items: 
Size: 674175 Color: 3
Size: 325521 Color: 2

Bin 3571: 307 of cap free
Amount of items: 2
Items: 
Size: 671131 Color: 4
Size: 328563 Color: 3

Bin 3572: 308 of cap free
Amount of items: 2
Items: 
Size: 674738 Color: 2
Size: 324955 Color: 4

Bin 3573: 308 of cap free
Amount of items: 2
Items: 
Size: 508865 Color: 4
Size: 490828 Color: 2

Bin 3574: 308 of cap free
Amount of items: 2
Items: 
Size: 658735 Color: 0
Size: 340958 Color: 1

Bin 3575: 308 of cap free
Amount of items: 2
Items: 
Size: 618378 Color: 1
Size: 381315 Color: 3

Bin 3576: 308 of cap free
Amount of items: 2
Items: 
Size: 791013 Color: 3
Size: 208680 Color: 4

Bin 3577: 309 of cap free
Amount of items: 2
Items: 
Size: 542281 Color: 3
Size: 457411 Color: 4

Bin 3578: 310 of cap free
Amount of items: 2
Items: 
Size: 525020 Color: 4
Size: 474671 Color: 1

Bin 3579: 311 of cap free
Amount of items: 2
Items: 
Size: 525039 Color: 1
Size: 474651 Color: 2

Bin 3580: 311 of cap free
Amount of items: 2
Items: 
Size: 644591 Color: 2
Size: 355099 Color: 4

Bin 3581: 311 of cap free
Amount of items: 2
Items: 
Size: 694389 Color: 3
Size: 305301 Color: 1

Bin 3582: 312 of cap free
Amount of items: 2
Items: 
Size: 541035 Color: 0
Size: 458654 Color: 2

Bin 3583: 312 of cap free
Amount of items: 2
Items: 
Size: 566713 Color: 1
Size: 432976 Color: 4

Bin 3584: 312 of cap free
Amount of items: 2
Items: 
Size: 652918 Color: 3
Size: 346771 Color: 1

Bin 3585: 314 of cap free
Amount of items: 2
Items: 
Size: 549246 Color: 4
Size: 450441 Color: 2

Bin 3586: 314 of cap free
Amount of items: 2
Items: 
Size: 785115 Color: 3
Size: 214572 Color: 4

Bin 3587: 315 of cap free
Amount of items: 2
Items: 
Size: 741412 Color: 4
Size: 258274 Color: 2

Bin 3588: 316 of cap free
Amount of items: 2
Items: 
Size: 696718 Color: 2
Size: 302967 Color: 4

Bin 3589: 316 of cap free
Amount of items: 2
Items: 
Size: 717040 Color: 3
Size: 282645 Color: 1

Bin 3590: 316 of cap free
Amount of items: 2
Items: 
Size: 775170 Color: 2
Size: 224515 Color: 0

Bin 3591: 317 of cap free
Amount of items: 2
Items: 
Size: 685469 Color: 3
Size: 314215 Color: 4

Bin 3592: 317 of cap free
Amount of items: 2
Items: 
Size: 644294 Color: 4
Size: 355390 Color: 2

Bin 3593: 318 of cap free
Amount of items: 2
Items: 
Size: 534685 Color: 1
Size: 464998 Color: 2

Bin 3594: 318 of cap free
Amount of items: 2
Items: 
Size: 550040 Color: 4
Size: 449643 Color: 0

Bin 3595: 318 of cap free
Amount of items: 2
Items: 
Size: 702605 Color: 3
Size: 297078 Color: 4

Bin 3596: 318 of cap free
Amount of items: 2
Items: 
Size: 712659 Color: 1
Size: 287024 Color: 2

Bin 3597: 319 of cap free
Amount of items: 2
Items: 
Size: 774384 Color: 4
Size: 225298 Color: 2

Bin 3598: 320 of cap free
Amount of items: 2
Items: 
Size: 518855 Color: 2
Size: 480826 Color: 4

Bin 3599: 320 of cap free
Amount of items: 2
Items: 
Size: 541409 Color: 4
Size: 458272 Color: 2

Bin 3600: 321 of cap free
Amount of items: 2
Items: 
Size: 583731 Color: 0
Size: 415949 Color: 1

Bin 3601: 323 of cap free
Amount of items: 2
Items: 
Size: 656608 Color: 0
Size: 343070 Color: 2

Bin 3602: 323 of cap free
Amount of items: 2
Items: 
Size: 620396 Color: 1
Size: 379282 Color: 2

Bin 3603: 324 of cap free
Amount of items: 2
Items: 
Size: 502825 Color: 2
Size: 496852 Color: 1

Bin 3604: 324 of cap free
Amount of items: 2
Items: 
Size: 601762 Color: 3
Size: 397915 Color: 0

Bin 3605: 325 of cap free
Amount of items: 2
Items: 
Size: 726682 Color: 1
Size: 272994 Color: 2

Bin 3606: 326 of cap free
Amount of items: 2
Items: 
Size: 767072 Color: 3
Size: 232603 Color: 0

Bin 3607: 326 of cap free
Amount of items: 2
Items: 
Size: 614809 Color: 1
Size: 384866 Color: 3

Bin 3608: 327 of cap free
Amount of items: 2
Items: 
Size: 506741 Color: 4
Size: 492933 Color: 3

Bin 3609: 328 of cap free
Amount of items: 2
Items: 
Size: 522214 Color: 2
Size: 477459 Color: 4

Bin 3610: 332 of cap free
Amount of items: 2
Items: 
Size: 564215 Color: 3
Size: 435454 Color: 0

Bin 3611: 332 of cap free
Amount of items: 2
Items: 
Size: 702516 Color: 2
Size: 297153 Color: 3

Bin 3612: 333 of cap free
Amount of items: 2
Items: 
Size: 779312 Color: 4
Size: 220356 Color: 1

Bin 3613: 333 of cap free
Amount of items: 2
Items: 
Size: 625323 Color: 3
Size: 374345 Color: 0

Bin 3614: 333 of cap free
Amount of items: 2
Items: 
Size: 512568 Color: 2
Size: 487100 Color: 1

Bin 3615: 335 of cap free
Amount of items: 2
Items: 
Size: 552843 Color: 0
Size: 446823 Color: 3

Bin 3616: 337 of cap free
Amount of items: 2
Items: 
Size: 499846 Color: 4
Size: 499818 Color: 2

Bin 3617: 338 of cap free
Amount of items: 2
Items: 
Size: 788410 Color: 2
Size: 211253 Color: 1

Bin 3618: 338 of cap free
Amount of items: 2
Items: 
Size: 785566 Color: 1
Size: 214097 Color: 2

Bin 3619: 339 of cap free
Amount of items: 2
Items: 
Size: 711768 Color: 1
Size: 287894 Color: 4

Bin 3620: 339 of cap free
Amount of items: 2
Items: 
Size: 701712 Color: 1
Size: 297950 Color: 2

Bin 3621: 339 of cap free
Amount of items: 2
Items: 
Size: 742615 Color: 4
Size: 257047 Color: 1

Bin 3622: 339 of cap free
Amount of items: 2
Items: 
Size: 799878 Color: 3
Size: 199784 Color: 4

Bin 3623: 340 of cap free
Amount of items: 2
Items: 
Size: 687812 Color: 3
Size: 311849 Color: 0

Bin 3624: 342 of cap free
Amount of items: 2
Items: 
Size: 602282 Color: 1
Size: 397377 Color: 0

Bin 3625: 343 of cap free
Amount of items: 2
Items: 
Size: 593480 Color: 3
Size: 406178 Color: 4

Bin 3626: 344 of cap free
Amount of items: 2
Items: 
Size: 528332 Color: 3
Size: 471325 Color: 2

Bin 3627: 345 of cap free
Amount of items: 2
Items: 
Size: 665529 Color: 4
Size: 334127 Color: 1

Bin 3628: 345 of cap free
Amount of items: 2
Items: 
Size: 623043 Color: 2
Size: 376613 Color: 4

Bin 3629: 346 of cap free
Amount of items: 2
Items: 
Size: 568965 Color: 3
Size: 430690 Color: 0

Bin 3630: 346 of cap free
Amount of items: 2
Items: 
Size: 566306 Color: 2
Size: 433349 Color: 3

Bin 3631: 348 of cap free
Amount of items: 3
Items: 
Size: 688473 Color: 3
Size: 155608 Color: 0
Size: 155572 Color: 2

Bin 3632: 348 of cap free
Amount of items: 2
Items: 
Size: 669299 Color: 2
Size: 330354 Color: 3

Bin 3633: 348 of cap free
Amount of items: 2
Items: 
Size: 516347 Color: 4
Size: 483306 Color: 1

Bin 3634: 349 of cap free
Amount of items: 2
Items: 
Size: 652909 Color: 1
Size: 346743 Color: 2

Bin 3635: 350 of cap free
Amount of items: 2
Items: 
Size: 685463 Color: 3
Size: 314188 Color: 4

Bin 3636: 350 of cap free
Amount of items: 2
Items: 
Size: 619099 Color: 0
Size: 380552 Color: 4

Bin 3637: 351 of cap free
Amount of items: 2
Items: 
Size: 673413 Color: 4
Size: 326237 Color: 3

Bin 3638: 352 of cap free
Amount of items: 2
Items: 
Size: 725111 Color: 0
Size: 274538 Color: 4

Bin 3639: 352 of cap free
Amount of items: 2
Items: 
Size: 735708 Color: 0
Size: 263941 Color: 2

Bin 3640: 352 of cap free
Amount of items: 2
Items: 
Size: 672448 Color: 4
Size: 327201 Color: 1

Bin 3641: 354 of cap free
Amount of items: 2
Items: 
Size: 660046 Color: 1
Size: 339601 Color: 0

Bin 3642: 354 of cap free
Amount of items: 2
Items: 
Size: 648999 Color: 2
Size: 350648 Color: 4

Bin 3643: 354 of cap free
Amount of items: 2
Items: 
Size: 756306 Color: 4
Size: 243341 Color: 0

Bin 3644: 354 of cap free
Amount of items: 2
Items: 
Size: 705747 Color: 4
Size: 293900 Color: 0

Bin 3645: 355 of cap free
Amount of items: 2
Items: 
Size: 749436 Color: 0
Size: 250210 Color: 2

Bin 3646: 356 of cap free
Amount of items: 2
Items: 
Size: 774347 Color: 3
Size: 225298 Color: 4

Bin 3647: 356 of cap free
Amount of items: 2
Items: 
Size: 759247 Color: 1
Size: 240398 Color: 0

Bin 3648: 359 of cap free
Amount of items: 2
Items: 
Size: 583731 Color: 0
Size: 415911 Color: 4

Bin 3649: 359 of cap free
Amount of items: 2
Items: 
Size: 718369 Color: 4
Size: 281273 Color: 1

Bin 3650: 360 of cap free
Amount of items: 2
Items: 
Size: 764995 Color: 4
Size: 234646 Color: 1

Bin 3651: 360 of cap free
Amount of items: 2
Items: 
Size: 731200 Color: 0
Size: 268441 Color: 4

Bin 3652: 360 of cap free
Amount of items: 2
Items: 
Size: 506780 Color: 0
Size: 492861 Color: 4

Bin 3653: 360 of cap free
Amount of items: 2
Items: 
Size: 620362 Color: 0
Size: 379279 Color: 4

Bin 3654: 362 of cap free
Amount of items: 2
Items: 
Size: 614249 Color: 1
Size: 385390 Color: 3

Bin 3655: 362 of cap free
Amount of items: 2
Items: 
Size: 742600 Color: 4
Size: 257039 Color: 3

Bin 3656: 363 of cap free
Amount of items: 2
Items: 
Size: 532661 Color: 2
Size: 466977 Color: 3

Bin 3657: 364 of cap free
Amount of items: 2
Items: 
Size: 563233 Color: 4
Size: 436404 Color: 0

Bin 3658: 365 of cap free
Amount of items: 2
Items: 
Size: 601152 Color: 0
Size: 398484 Color: 4

Bin 3659: 366 of cap free
Amount of items: 2
Items: 
Size: 511314 Color: 1
Size: 488321 Color: 2

Bin 3660: 366 of cap free
Amount of items: 2
Items: 
Size: 701680 Color: 3
Size: 297955 Color: 1

Bin 3661: 368 of cap free
Amount of items: 2
Items: 
Size: 501672 Color: 2
Size: 497961 Color: 0

Bin 3662: 368 of cap free
Amount of items: 2
Items: 
Size: 773767 Color: 4
Size: 225866 Color: 1

Bin 3663: 369 of cap free
Amount of items: 2
Items: 
Size: 566298 Color: 2
Size: 433334 Color: 3

Bin 3664: 370 of cap free
Amount of items: 2
Items: 
Size: 762465 Color: 4
Size: 237166 Color: 1

Bin 3665: 372 of cap free
Amount of items: 2
Items: 
Size: 771253 Color: 4
Size: 228376 Color: 0

Bin 3666: 372 of cap free
Amount of items: 2
Items: 
Size: 781573 Color: 0
Size: 218056 Color: 2

Bin 3667: 374 of cap free
Amount of items: 2
Items: 
Size: 563622 Color: 2
Size: 436005 Color: 0

Bin 3668: 374 of cap free
Amount of items: 2
Items: 
Size: 624651 Color: 1
Size: 374976 Color: 2

Bin 3669: 375 of cap free
Amount of items: 2
Items: 
Size: 781095 Color: 2
Size: 218531 Color: 0

Bin 3670: 375 of cap free
Amount of items: 2
Items: 
Size: 596951 Color: 0
Size: 402675 Color: 3

Bin 3671: 375 of cap free
Amount of items: 2
Items: 
Size: 792695 Color: 4
Size: 206931 Color: 0

Bin 3672: 375 of cap free
Amount of items: 2
Items: 
Size: 628507 Color: 1
Size: 371119 Color: 4

Bin 3673: 376 of cap free
Amount of items: 2
Items: 
Size: 616611 Color: 1
Size: 383014 Color: 0

Bin 3674: 376 of cap free
Amount of items: 2
Items: 
Size: 703963 Color: 2
Size: 295662 Color: 1

Bin 3675: 377 of cap free
Amount of items: 2
Items: 
Size: 557348 Color: 4
Size: 442276 Color: 2

Bin 3676: 378 of cap free
Amount of items: 2
Items: 
Size: 706921 Color: 0
Size: 292702 Color: 4

Bin 3677: 378 of cap free
Amount of items: 2
Items: 
Size: 547104 Color: 2
Size: 452519 Color: 3

Bin 3678: 381 of cap free
Amount of items: 3
Items: 
Size: 685050 Color: 3
Size: 157484 Color: 2
Size: 157086 Color: 4

Bin 3679: 382 of cap free
Amount of items: 2
Items: 
Size: 775867 Color: 2
Size: 223752 Color: 1

Bin 3680: 383 of cap free
Amount of items: 2
Items: 
Size: 765816 Color: 1
Size: 233802 Color: 2

Bin 3681: 384 of cap free
Amount of items: 2
Items: 
Size: 694297 Color: 1
Size: 305320 Color: 0

Bin 3682: 384 of cap free
Amount of items: 2
Items: 
Size: 556437 Color: 0
Size: 443180 Color: 3

Bin 3683: 385 of cap free
Amount of items: 2
Items: 
Size: 788368 Color: 2
Size: 211248 Color: 3

Bin 3684: 387 of cap free
Amount of items: 2
Items: 
Size: 624520 Color: 0
Size: 375094 Color: 1

Bin 3685: 387 of cap free
Amount of items: 2
Items: 
Size: 504509 Color: 0
Size: 495105 Color: 4

Bin 3686: 387 of cap free
Amount of items: 2
Items: 
Size: 745271 Color: 3
Size: 254343 Color: 0

Bin 3687: 387 of cap free
Amount of items: 2
Items: 
Size: 736260 Color: 3
Size: 263354 Color: 4

Bin 3688: 387 of cap free
Amount of items: 2
Items: 
Size: 733305 Color: 0
Size: 266309 Color: 1

Bin 3689: 387 of cap free
Amount of items: 2
Items: 
Size: 512633 Color: 1
Size: 486981 Color: 0

Bin 3690: 389 of cap free
Amount of items: 2
Items: 
Size: 560938 Color: 4
Size: 438674 Color: 1

Bin 3691: 389 of cap free
Amount of items: 2
Items: 
Size: 619885 Color: 4
Size: 379727 Color: 3

Bin 3692: 390 of cap free
Amount of items: 2
Items: 
Size: 724377 Color: 2
Size: 275234 Color: 3

Bin 3693: 390 of cap free
Amount of items: 2
Items: 
Size: 658511 Color: 4
Size: 341100 Color: 0

Bin 3694: 391 of cap free
Amount of items: 2
Items: 
Size: 544432 Color: 0
Size: 455178 Color: 3

Bin 3695: 392 of cap free
Amount of items: 2
Items: 
Size: 726535 Color: 0
Size: 273074 Color: 1

Bin 3696: 392 of cap free
Amount of items: 2
Items: 
Size: 503155 Color: 0
Size: 496454 Color: 1

Bin 3697: 392 of cap free
Amount of items: 2
Items: 
Size: 517131 Color: 4
Size: 482478 Color: 2

Bin 3698: 392 of cap free
Amount of items: 2
Items: 
Size: 551396 Color: 3
Size: 448213 Color: 4

Bin 3699: 394 of cap free
Amount of items: 2
Items: 
Size: 619107 Color: 4
Size: 380500 Color: 3

Bin 3700: 397 of cap free
Amount of items: 2
Items: 
Size: 548741 Color: 4
Size: 450863 Color: 3

Bin 3701: 398 of cap free
Amount of items: 2
Items: 
Size: 762444 Color: 3
Size: 237159 Color: 2

Bin 3702: 398 of cap free
Amount of items: 2
Items: 
Size: 566684 Color: 3
Size: 432919 Color: 4

Bin 3703: 398 of cap free
Amount of items: 2
Items: 
Size: 715856 Color: 3
Size: 283747 Color: 4

Bin 3704: 398 of cap free
Amount of items: 2
Items: 
Size: 734028 Color: 0
Size: 265575 Color: 2

Bin 3705: 398 of cap free
Amount of items: 2
Items: 
Size: 516326 Color: 1
Size: 483277 Color: 0

Bin 3706: 399 of cap free
Amount of items: 2
Items: 
Size: 656749 Color: 2
Size: 342853 Color: 3

Bin 3707: 399 of cap free
Amount of items: 2
Items: 
Size: 561890 Color: 3
Size: 437712 Color: 2

Bin 3708: 400 of cap free
Amount of items: 2
Items: 
Size: 732535 Color: 2
Size: 267066 Color: 1

Bin 3709: 403 of cap free
Amount of items: 2
Items: 
Size: 665478 Color: 1
Size: 334120 Color: 4

Bin 3710: 403 of cap free
Amount of items: 2
Items: 
Size: 699936 Color: 3
Size: 299662 Color: 2

Bin 3711: 403 of cap free
Amount of items: 2
Items: 
Size: 533763 Color: 4
Size: 465835 Color: 1

Bin 3712: 404 of cap free
Amount of items: 2
Items: 
Size: 586346 Color: 2
Size: 413251 Color: 0

Bin 3713: 405 of cap free
Amount of items: 2
Items: 
Size: 528795 Color: 2
Size: 470801 Color: 1

Bin 3714: 406 of cap free
Amount of items: 2
Items: 
Size: 578572 Color: 3
Size: 421023 Color: 4

Bin 3715: 407 of cap free
Amount of items: 2
Items: 
Size: 770406 Color: 0
Size: 229188 Color: 4

Bin 3716: 407 of cap free
Amount of items: 2
Items: 
Size: 690253 Color: 1
Size: 309341 Color: 4

Bin 3717: 409 of cap free
Amount of items: 2
Items: 
Size: 620803 Color: 0
Size: 378789 Color: 1

Bin 3718: 409 of cap free
Amount of items: 2
Items: 
Size: 593459 Color: 0
Size: 406133 Color: 2

Bin 3719: 411 of cap free
Amount of items: 2
Items: 
Size: 689243 Color: 0
Size: 310347 Color: 3

Bin 3720: 411 of cap free
Amount of items: 2
Items: 
Size: 676670 Color: 1
Size: 322920 Color: 2

Bin 3721: 412 of cap free
Amount of items: 2
Items: 
Size: 646723 Color: 2
Size: 352866 Color: 0

Bin 3722: 413 of cap free
Amount of items: 2
Items: 
Size: 544413 Color: 4
Size: 455175 Color: 1

Bin 3723: 414 of cap free
Amount of items: 2
Items: 
Size: 508763 Color: 3
Size: 490824 Color: 2

Bin 3724: 415 of cap free
Amount of items: 2
Items: 
Size: 624619 Color: 1
Size: 374967 Color: 2

Bin 3725: 415 of cap free
Amount of items: 2
Items: 
Size: 781075 Color: 3
Size: 218511 Color: 4

Bin 3726: 415 of cap free
Amount of items: 2
Items: 
Size: 613266 Color: 1
Size: 386320 Color: 2

Bin 3727: 416 of cap free
Amount of items: 2
Items: 
Size: 584812 Color: 2
Size: 414773 Color: 3

Bin 3728: 416 of cap free
Amount of items: 2
Items: 
Size: 655094 Color: 0
Size: 344491 Color: 1

Bin 3729: 417 of cap free
Amount of items: 2
Items: 
Size: 633233 Color: 1
Size: 366351 Color: 3

Bin 3730: 417 of cap free
Amount of items: 2
Items: 
Size: 532616 Color: 0
Size: 466968 Color: 2

Bin 3731: 417 of cap free
Amount of items: 2
Items: 
Size: 600914 Color: 3
Size: 398670 Color: 0

Bin 3732: 418 of cap free
Amount of items: 2
Items: 
Size: 668179 Color: 4
Size: 331404 Color: 0

Bin 3733: 418 of cap free
Amount of items: 2
Items: 
Size: 634327 Color: 4
Size: 365256 Color: 2

Bin 3734: 418 of cap free
Amount of items: 2
Items: 
Size: 651955 Color: 4
Size: 347628 Color: 3

Bin 3735: 420 of cap free
Amount of items: 2
Items: 
Size: 607706 Color: 0
Size: 391875 Color: 2

Bin 3736: 420 of cap free
Amount of items: 2
Items: 
Size: 641441 Color: 4
Size: 358140 Color: 3

Bin 3737: 421 of cap free
Amount of items: 2
Items: 
Size: 590707 Color: 4
Size: 408873 Color: 2

Bin 3738: 421 of cap free
Amount of items: 2
Items: 
Size: 628553 Color: 4
Size: 371027 Color: 2

Bin 3739: 423 of cap free
Amount of items: 2
Items: 
Size: 538549 Color: 3
Size: 461029 Color: 1

Bin 3740: 424 of cap free
Amount of items: 2
Items: 
Size: 764978 Color: 1
Size: 234599 Color: 2

Bin 3741: 425 of cap free
Amount of items: 2
Items: 
Size: 588955 Color: 4
Size: 410621 Color: 3

Bin 3742: 427 of cap free
Amount of items: 2
Items: 
Size: 680190 Color: 3
Size: 319384 Color: 2

Bin 3743: 428 of cap free
Amount of items: 2
Items: 
Size: 701635 Color: 4
Size: 297938 Color: 2

Bin 3744: 429 of cap free
Amount of items: 2
Items: 
Size: 748683 Color: 4
Size: 250889 Color: 1

Bin 3745: 430 of cap free
Amount of items: 2
Items: 
Size: 501400 Color: 1
Size: 498171 Color: 2

Bin 3746: 430 of cap free
Amount of items: 2
Items: 
Size: 596936 Color: 1
Size: 402635 Color: 2

Bin 3747: 432 of cap free
Amount of items: 2
Items: 
Size: 793366 Color: 4
Size: 206203 Color: 1

Bin 3748: 432 of cap free
Amount of items: 2
Items: 
Size: 702527 Color: 3
Size: 297042 Color: 0

Bin 3749: 435 of cap free
Amount of items: 2
Items: 
Size: 563142 Color: 1
Size: 436424 Color: 4

Bin 3750: 435 of cap free
Amount of items: 2
Items: 
Size: 598161 Color: 1
Size: 401405 Color: 3

Bin 3751: 436 of cap free
Amount of items: 2
Items: 
Size: 745720 Color: 1
Size: 253845 Color: 0

Bin 3752: 436 of cap free
Amount of items: 2
Items: 
Size: 709345 Color: 1
Size: 290220 Color: 3

Bin 3753: 439 of cap free
Amount of items: 2
Items: 
Size: 559458 Color: 0
Size: 440104 Color: 2

Bin 3754: 440 of cap free
Amount of items: 2
Items: 
Size: 750231 Color: 1
Size: 249330 Color: 3

Bin 3755: 442 of cap free
Amount of items: 2
Items: 
Size: 719302 Color: 2
Size: 280257 Color: 4

Bin 3756: 442 of cap free
Amount of items: 2
Items: 
Size: 742600 Color: 0
Size: 256959 Color: 2

Bin 3757: 442 of cap free
Amount of items: 2
Items: 
Size: 716931 Color: 0
Size: 282628 Color: 3

Bin 3758: 443 of cap free
Amount of items: 2
Items: 
Size: 657239 Color: 0
Size: 342319 Color: 3

Bin 3759: 444 of cap free
Amount of items: 2
Items: 
Size: 596473 Color: 2
Size: 403084 Color: 1

Bin 3760: 446 of cap free
Amount of items: 2
Items: 
Size: 645378 Color: 1
Size: 354177 Color: 2

Bin 3761: 448 of cap free
Amount of items: 2
Items: 
Size: 744748 Color: 2
Size: 254805 Color: 4

Bin 3762: 448 of cap free
Amount of items: 2
Items: 
Size: 508731 Color: 3
Size: 490822 Color: 2

Bin 3763: 448 of cap free
Amount of items: 2
Items: 
Size: 514275 Color: 1
Size: 485278 Color: 2

Bin 3764: 449 of cap free
Amount of items: 2
Items: 
Size: 678277 Color: 3
Size: 321275 Color: 1

Bin 3765: 449 of cap free
Amount of items: 2
Items: 
Size: 566654 Color: 3
Size: 432898 Color: 2

Bin 3766: 449 of cap free
Amount of items: 2
Items: 
Size: 587412 Color: 1
Size: 412140 Color: 4

Bin 3767: 449 of cap free
Amount of items: 2
Items: 
Size: 560870 Color: 0
Size: 438682 Color: 4

Bin 3768: 449 of cap free
Amount of items: 2
Items: 
Size: 549953 Color: 0
Size: 449599 Color: 3

Bin 3769: 449 of cap free
Amount of items: 2
Items: 
Size: 579328 Color: 1
Size: 420224 Color: 3

Bin 3770: 451 of cap free
Amount of items: 2
Items: 
Size: 787544 Color: 4
Size: 212006 Color: 0

Bin 3771: 451 of cap free
Amount of items: 2
Items: 
Size: 662681 Color: 0
Size: 336869 Color: 1

Bin 3772: 451 of cap free
Amount of items: 2
Items: 
Size: 647550 Color: 4
Size: 352000 Color: 0

Bin 3773: 454 of cap free
Amount of items: 2
Items: 
Size: 743236 Color: 2
Size: 256311 Color: 3

Bin 3774: 454 of cap free
Amount of items: 2
Items: 
Size: 571918 Color: 3
Size: 427629 Color: 4

Bin 3775: 455 of cap free
Amount of items: 2
Items: 
Size: 684378 Color: 0
Size: 315168 Color: 1

Bin 3776: 455 of cap free
Amount of items: 2
Items: 
Size: 598148 Color: 1
Size: 401398 Color: 0

Bin 3777: 455 of cap free
Amount of items: 2
Items: 
Size: 633222 Color: 1
Size: 366324 Color: 4

Bin 3778: 457 of cap free
Amount of items: 2
Items: 
Size: 596909 Color: 1
Size: 402635 Color: 0

Bin 3779: 458 of cap free
Amount of items: 2
Items: 
Size: 664730 Color: 0
Size: 334813 Color: 3

Bin 3780: 460 of cap free
Amount of items: 2
Items: 
Size: 642041 Color: 4
Size: 357500 Color: 3

Bin 3781: 463 of cap free
Amount of items: 2
Items: 
Size: 721864 Color: 2
Size: 277674 Color: 3

Bin 3782: 463 of cap free
Amount of items: 2
Items: 
Size: 718265 Color: 0
Size: 281273 Color: 3

Bin 3783: 463 of cap free
Amount of items: 2
Items: 
Size: 558150 Color: 4
Size: 441388 Color: 1

Bin 3784: 464 of cap free
Amount of items: 2
Items: 
Size: 547739 Color: 2
Size: 451798 Color: 3

Bin 3785: 465 of cap free
Amount of items: 2
Items: 
Size: 535584 Color: 4
Size: 463952 Color: 0

Bin 3786: 469 of cap free
Amount of items: 2
Items: 
Size: 648996 Color: 0
Size: 350536 Color: 1

Bin 3787: 469 of cap free
Amount of items: 2
Items: 
Size: 582151 Color: 3
Size: 417381 Color: 4

Bin 3788: 469 of cap free
Amount of items: 2
Items: 
Size: 635904 Color: 0
Size: 363628 Color: 4

Bin 3789: 469 of cap free
Amount of items: 2
Items: 
Size: 569409 Color: 4
Size: 430123 Color: 3

Bin 3790: 472 of cap free
Amount of items: 2
Items: 
Size: 669557 Color: 3
Size: 329972 Color: 0

Bin 3791: 472 of cap free
Amount of items: 2
Items: 
Size: 590691 Color: 0
Size: 408838 Color: 4

Bin 3792: 472 of cap free
Amount of items: 2
Items: 
Size: 639986 Color: 3
Size: 359543 Color: 4

Bin 3793: 473 of cap free
Amount of items: 2
Items: 
Size: 659553 Color: 1
Size: 339975 Color: 4

Bin 3794: 473 of cap free
Amount of items: 2
Items: 
Size: 518736 Color: 1
Size: 480792 Color: 3

Bin 3795: 476 of cap free
Amount of items: 2
Items: 
Size: 665413 Color: 4
Size: 334112 Color: 1

Bin 3796: 476 of cap free
Amount of items: 2
Items: 
Size: 614216 Color: 3
Size: 385309 Color: 1

Bin 3797: 477 of cap free
Amount of items: 2
Items: 
Size: 673326 Color: 2
Size: 326198 Color: 0

Bin 3798: 478 of cap free
Amount of items: 2
Items: 
Size: 732534 Color: 4
Size: 266989 Color: 0

Bin 3799: 479 of cap free
Amount of items: 2
Items: 
Size: 741300 Color: 0
Size: 258222 Color: 1

Bin 3800: 481 of cap free
Amount of items: 2
Items: 
Size: 536818 Color: 4
Size: 462702 Color: 2

Bin 3801: 482 of cap free
Amount of items: 2
Items: 
Size: 578373 Color: 4
Size: 421146 Color: 3

Bin 3802: 483 of cap free
Amount of items: 2
Items: 
Size: 696004 Color: 4
Size: 303514 Color: 0

Bin 3803: 483 of cap free
Amount of items: 2
Items: 
Size: 583163 Color: 0
Size: 416355 Color: 2

Bin 3804: 485 of cap free
Amount of items: 2
Items: 
Size: 767014 Color: 0
Size: 232502 Color: 2

Bin 3805: 485 of cap free
Amount of items: 2
Items: 
Size: 508899 Color: 2
Size: 490617 Color: 4

Bin 3806: 486 of cap free
Amount of items: 2
Items: 
Size: 693028 Color: 2
Size: 306487 Color: 0

Bin 3807: 487 of cap free
Amount of items: 2
Items: 
Size: 758436 Color: 2
Size: 241078 Color: 4

Bin 3808: 488 of cap free
Amount of items: 2
Items: 
Size: 739260 Color: 4
Size: 260253 Color: 2

Bin 3809: 488 of cap free
Amount of items: 2
Items: 
Size: 534062 Color: 1
Size: 465451 Color: 3

Bin 3810: 492 of cap free
Amount of items: 2
Items: 
Size: 613495 Color: 2
Size: 386014 Color: 0

Bin 3811: 493 of cap free
Amount of items: 2
Items: 
Size: 652877 Color: 3
Size: 346631 Color: 1

Bin 3812: 493 of cap free
Amount of items: 2
Items: 
Size: 708505 Color: 3
Size: 291003 Color: 0

Bin 3813: 494 of cap free
Amount of items: 2
Items: 
Size: 713719 Color: 3
Size: 285788 Color: 2

Bin 3814: 497 of cap free
Amount of items: 2
Items: 
Size: 648337 Color: 0
Size: 351167 Color: 3

Bin 3815: 501 of cap free
Amount of items: 2
Items: 
Size: 547717 Color: 1
Size: 451783 Color: 3

Bin 3816: 501 of cap free
Amount of items: 2
Items: 
Size: 652869 Color: 3
Size: 346631 Color: 4

Bin 3817: 502 of cap free
Amount of items: 2
Items: 
Size: 537333 Color: 4
Size: 462166 Color: 3

Bin 3818: 505 of cap free
Amount of items: 2
Items: 
Size: 588945 Color: 4
Size: 410551 Color: 1

Bin 3819: 507 of cap free
Amount of items: 2
Items: 
Size: 729931 Color: 3
Size: 269563 Color: 0

Bin 3820: 507 of cap free
Amount of items: 2
Items: 
Size: 786543 Color: 0
Size: 212951 Color: 3

Bin 3821: 509 of cap free
Amount of items: 2
Items: 
Size: 576014 Color: 1
Size: 423478 Color: 0

Bin 3822: 513 of cap free
Amount of items: 2
Items: 
Size: 635873 Color: 1
Size: 363615 Color: 4

Bin 3823: 513 of cap free
Amount of items: 2
Items: 
Size: 782788 Color: 2
Size: 216700 Color: 0

Bin 3824: 515 of cap free
Amount of items: 2
Items: 
Size: 678858 Color: 0
Size: 320628 Color: 1

Bin 3825: 515 of cap free
Amount of items: 2
Items: 
Size: 716285 Color: 4
Size: 283201 Color: 1

Bin 3826: 517 of cap free
Amount of items: 2
Items: 
Size: 693028 Color: 3
Size: 306456 Color: 2

Bin 3827: 520 of cap free
Amount of items: 2
Items: 
Size: 718210 Color: 0
Size: 281271 Color: 4

Bin 3828: 520 of cap free
Amount of items: 2
Items: 
Size: 637297 Color: 0
Size: 362184 Color: 2

Bin 3829: 521 of cap free
Amount of items: 2
Items: 
Size: 556317 Color: 4
Size: 443163 Color: 0

Bin 3830: 522 of cap free
Amount of items: 2
Items: 
Size: 635866 Color: 2
Size: 363613 Color: 0

Bin 3831: 525 of cap free
Amount of items: 2
Items: 
Size: 726056 Color: 2
Size: 273420 Color: 0

Bin 3832: 526 of cap free
Amount of items: 2
Items: 
Size: 666149 Color: 4
Size: 333326 Color: 0

Bin 3833: 527 of cap free
Amount of items: 2
Items: 
Size: 611854 Color: 3
Size: 387620 Color: 0

Bin 3834: 528 of cap free
Amount of items: 2
Items: 
Size: 524965 Color: 1
Size: 474508 Color: 2

Bin 3835: 529 of cap free
Amount of items: 2
Items: 
Size: 683837 Color: 1
Size: 315635 Color: 4

Bin 3836: 531 of cap free
Amount of items: 2
Items: 
Size: 698971 Color: 1
Size: 300499 Color: 4

Bin 3837: 532 of cap free
Amount of items: 2
Items: 
Size: 682986 Color: 0
Size: 316483 Color: 1

Bin 3838: 535 of cap free
Amount of items: 2
Items: 
Size: 745564 Color: 0
Size: 253902 Color: 1

Bin 3839: 536 of cap free
Amount of items: 2
Items: 
Size: 767683 Color: 0
Size: 231782 Color: 4

Bin 3840: 536 of cap free
Amount of items: 2
Items: 
Size: 524981 Color: 2
Size: 474484 Color: 4

Bin 3841: 538 of cap free
Amount of items: 2
Items: 
Size: 558123 Color: 1
Size: 441340 Color: 4

Bin 3842: 540 of cap free
Amount of items: 2
Items: 
Size: 791131 Color: 4
Size: 208330 Color: 2

Bin 3843: 543 of cap free
Amount of items: 2
Items: 
Size: 768395 Color: 4
Size: 231063 Color: 0

Bin 3844: 544 of cap free
Amount of items: 2
Items: 
Size: 558811 Color: 3
Size: 440646 Color: 1

Bin 3845: 545 of cap free
Amount of items: 2
Items: 
Size: 549000 Color: 1
Size: 450456 Color: 4

Bin 3846: 547 of cap free
Amount of items: 2
Items: 
Size: 581163 Color: 4
Size: 418291 Color: 3

Bin 3847: 548 of cap free
Amount of items: 2
Items: 
Size: 756853 Color: 4
Size: 242600 Color: 3

Bin 3848: 549 of cap free
Amount of items: 2
Items: 
Size: 608711 Color: 4
Size: 390741 Color: 3

Bin 3849: 549 of cap free
Amount of items: 2
Items: 
Size: 579292 Color: 1
Size: 420160 Color: 2

Bin 3850: 550 of cap free
Amount of items: 2
Items: 
Size: 766370 Color: 3
Size: 233081 Color: 0

Bin 3851: 550 of cap free
Amount of items: 2
Items: 
Size: 773743 Color: 3
Size: 225708 Color: 1

Bin 3852: 550 of cap free
Amount of items: 2
Items: 
Size: 527731 Color: 4
Size: 471720 Color: 1

Bin 3853: 551 of cap free
Amount of items: 2
Items: 
Size: 768452 Color: 1
Size: 230998 Color: 4

Bin 3854: 553 of cap free
Amount of items: 2
Items: 
Size: 588852 Color: 1
Size: 410596 Color: 2

Bin 3855: 554 of cap free
Amount of items: 2
Items: 
Size: 592399 Color: 4
Size: 407048 Color: 2

Bin 3856: 554 of cap free
Amount of items: 2
Items: 
Size: 710197 Color: 3
Size: 289250 Color: 0

Bin 3857: 555 of cap free
Amount of items: 2
Items: 
Size: 731034 Color: 4
Size: 268412 Color: 0

Bin 3858: 555 of cap free
Amount of items: 2
Items: 
Size: 692983 Color: 2
Size: 306463 Color: 3

Bin 3859: 557 of cap free
Amount of items: 2
Items: 
Size: 506601 Color: 4
Size: 492843 Color: 2

Bin 3860: 560 of cap free
Amount of items: 2
Items: 
Size: 712870 Color: 2
Size: 286571 Color: 1

Bin 3861: 560 of cap free
Amount of items: 2
Items: 
Size: 751391 Color: 3
Size: 248050 Color: 4

Bin 3862: 560 of cap free
Amount of items: 2
Items: 
Size: 652719 Color: 4
Size: 346722 Color: 3

Bin 3863: 562 of cap free
Amount of items: 2
Items: 
Size: 555457 Color: 3
Size: 443982 Color: 0

Bin 3864: 562 of cap free
Amount of items: 2
Items: 
Size: 778253 Color: 3
Size: 221186 Color: 2

Bin 3865: 564 of cap free
Amount of items: 2
Items: 
Size: 651883 Color: 4
Size: 347554 Color: 2

Bin 3866: 565 of cap free
Amount of items: 2
Items: 
Size: 612858 Color: 2
Size: 386578 Color: 1

Bin 3867: 572 of cap free
Amount of items: 2
Items: 
Size: 556286 Color: 2
Size: 443143 Color: 0

Bin 3868: 572 of cap free
Amount of items: 2
Items: 
Size: 523308 Color: 0
Size: 476121 Color: 4

Bin 3869: 574 of cap free
Amount of items: 2
Items: 
Size: 763363 Color: 2
Size: 236064 Color: 4

Bin 3870: 575 of cap free
Amount of items: 2
Items: 
Size: 776160 Color: 1
Size: 223266 Color: 4

Bin 3871: 575 of cap free
Amount of items: 2
Items: 
Size: 648931 Color: 4
Size: 350495 Color: 2

Bin 3872: 577 of cap free
Amount of items: 2
Items: 
Size: 782184 Color: 3
Size: 217240 Color: 1

Bin 3873: 577 of cap free
Amount of items: 2
Items: 
Size: 627150 Color: 0
Size: 372274 Color: 1

Bin 3874: 579 of cap free
Amount of items: 2
Items: 
Size: 516969 Color: 1
Size: 482453 Color: 4

Bin 3875: 580 of cap free
Amount of items: 2
Items: 
Size: 692388 Color: 2
Size: 307033 Color: 1

Bin 3876: 580 of cap free
Amount of items: 2
Items: 
Size: 523355 Color: 4
Size: 476066 Color: 1

Bin 3877: 581 of cap free
Amount of items: 2
Items: 
Size: 643355 Color: 0
Size: 356065 Color: 2

Bin 3878: 582 of cap free
Amount of items: 2
Items: 
Size: 516958 Color: 0
Size: 482461 Color: 1

Bin 3879: 582 of cap free
Amount of items: 2
Items: 
Size: 635845 Color: 2
Size: 363574 Color: 3

Bin 3880: 582 of cap free
Amount of items: 2
Items: 
Size: 613188 Color: 1
Size: 386231 Color: 2

Bin 3881: 584 of cap free
Amount of items: 2
Items: 
Size: 789664 Color: 2
Size: 209753 Color: 4

Bin 3882: 585 of cap free
Amount of items: 2
Items: 
Size: 545548 Color: 3
Size: 453868 Color: 2

Bin 3883: 585 of cap free
Amount of items: 2
Items: 
Size: 558081 Color: 0
Size: 441335 Color: 3

Bin 3884: 586 of cap free
Amount of items: 2
Items: 
Size: 592375 Color: 3
Size: 407040 Color: 0

Bin 3885: 586 of cap free
Amount of items: 2
Items: 
Size: 679391 Color: 4
Size: 320024 Color: 0

Bin 3886: 588 of cap free
Amount of items: 2
Items: 
Size: 520583 Color: 3
Size: 478830 Color: 4

Bin 3887: 589 of cap free
Amount of items: 2
Items: 
Size: 618313 Color: 2
Size: 381099 Color: 1

Bin 3888: 591 of cap free
Amount of items: 2
Items: 
Size: 518844 Color: 3
Size: 480566 Color: 0

Bin 3889: 596 of cap free
Amount of items: 2
Items: 
Size: 709236 Color: 2
Size: 290169 Color: 0

Bin 3890: 601 of cap free
Amount of items: 2
Items: 
Size: 676568 Color: 3
Size: 322832 Color: 2

Bin 3891: 604 of cap free
Amount of items: 2
Items: 
Size: 752307 Color: 3
Size: 247090 Color: 4

Bin 3892: 605 of cap free
Amount of items: 2
Items: 
Size: 786441 Color: 4
Size: 212955 Color: 0

Bin 3893: 609 of cap free
Amount of items: 2
Items: 
Size: 499718 Color: 2
Size: 499674 Color: 1

Bin 3894: 611 of cap free
Amount of items: 2
Items: 
Size: 639908 Color: 0
Size: 359482 Color: 1

Bin 3895: 612 of cap free
Amount of items: 2
Items: 
Size: 664579 Color: 0
Size: 334810 Color: 1

Bin 3896: 613 of cap free
Amount of items: 2
Items: 
Size: 592364 Color: 1
Size: 407024 Color: 4

Bin 3897: 614 of cap free
Amount of items: 2
Items: 
Size: 626158 Color: 0
Size: 373229 Color: 3

Bin 3898: 614 of cap free
Amount of items: 2
Items: 
Size: 786438 Color: 3
Size: 212949 Color: 1

Bin 3899: 614 of cap free
Amount of items: 2
Items: 
Size: 600924 Color: 0
Size: 398463 Color: 3

Bin 3900: 618 of cap free
Amount of items: 2
Items: 
Size: 675435 Color: 3
Size: 323948 Color: 1

Bin 3901: 619 of cap free
Amount of items: 2
Items: 
Size: 575049 Color: 0
Size: 424333 Color: 2

Bin 3902: 619 of cap free
Amount of items: 2
Items: 
Size: 608905 Color: 0
Size: 390477 Color: 4

Bin 3903: 621 of cap free
Amount of items: 2
Items: 
Size: 750060 Color: 1
Size: 249320 Color: 4

Bin 3904: 622 of cap free
Amount of items: 2
Items: 
Size: 753044 Color: 3
Size: 246335 Color: 4

Bin 3905: 625 of cap free
Amount of items: 2
Items: 
Size: 564789 Color: 3
Size: 434587 Color: 1

Bin 3906: 627 of cap free
Amount of items: 2
Items: 
Size: 579311 Color: 2
Size: 420063 Color: 4

Bin 3907: 629 of cap free
Amount of items: 2
Items: 
Size: 799648 Color: 4
Size: 199724 Color: 0

Bin 3908: 637 of cap free
Amount of items: 2
Items: 
Size: 548534 Color: 4
Size: 450830 Color: 3

Bin 3909: 639 of cap free
Amount of items: 2
Items: 
Size: 662634 Color: 0
Size: 336728 Color: 2

Bin 3910: 639 of cap free
Amount of items: 2
Items: 
Size: 639888 Color: 2
Size: 359474 Color: 1

Bin 3911: 640 of cap free
Amount of items: 2
Items: 
Size: 703873 Color: 0
Size: 295488 Color: 4

Bin 3912: 640 of cap free
Amount of items: 2
Items: 
Size: 610426 Color: 4
Size: 388935 Color: 3

Bin 3913: 641 of cap free
Amount of items: 2
Items: 
Size: 751273 Color: 1
Size: 248087 Color: 3

Bin 3914: 641 of cap free
Amount of items: 2
Items: 
Size: 611793 Color: 3
Size: 387567 Color: 4

Bin 3915: 644 of cap free
Amount of items: 2
Items: 
Size: 618301 Color: 3
Size: 381056 Color: 0

Bin 3916: 650 of cap free
Amount of items: 2
Items: 
Size: 564774 Color: 1
Size: 434577 Color: 4

Bin 3917: 650 of cap free
Amount of items: 2
Items: 
Size: 715853 Color: 3
Size: 283498 Color: 4

Bin 3918: 655 of cap free
Amount of items: 2
Items: 
Size: 508730 Color: 0
Size: 490616 Color: 4

Bin 3919: 655 of cap free
Amount of items: 2
Items: 
Size: 692981 Color: 4
Size: 306365 Color: 1

Bin 3920: 659 of cap free
Amount of items: 2
Items: 
Size: 603904 Color: 4
Size: 395438 Color: 3

Bin 3921: 660 of cap free
Amount of items: 2
Items: 
Size: 654213 Color: 4
Size: 345128 Color: 0

Bin 3922: 660 of cap free
Amount of items: 2
Items: 
Size: 757519 Color: 3
Size: 241822 Color: 2

Bin 3923: 660 of cap free
Amount of items: 2
Items: 
Size: 765584 Color: 4
Size: 233757 Color: 0

Bin 3924: 662 of cap free
Amount of items: 2
Items: 
Size: 575901 Color: 0
Size: 423438 Color: 4

Bin 3925: 662 of cap free
Amount of items: 2
Items: 
Size: 579186 Color: 4
Size: 420153 Color: 2

Bin 3926: 665 of cap free
Amount of items: 2
Items: 
Size: 681456 Color: 1
Size: 317880 Color: 2

Bin 3927: 669 of cap free
Amount of items: 2
Items: 
Size: 709130 Color: 3
Size: 290202 Color: 2

Bin 3928: 670 of cap free
Amount of items: 2
Items: 
Size: 528755 Color: 1
Size: 470576 Color: 2

Bin 3929: 672 of cap free
Amount of items: 2
Items: 
Size: 760225 Color: 3
Size: 239104 Color: 1

Bin 3930: 672 of cap free
Amount of items: 2
Items: 
Size: 531212 Color: 1
Size: 468117 Color: 2

Bin 3931: 672 of cap free
Amount of items: 2
Items: 
Size: 737886 Color: 1
Size: 261443 Color: 0

Bin 3932: 673 of cap free
Amount of items: 2
Items: 
Size: 595145 Color: 1
Size: 404183 Color: 0

Bin 3933: 674 of cap free
Amount of items: 2
Items: 
Size: 584596 Color: 0
Size: 414731 Color: 4

Bin 3934: 678 of cap free
Amount of items: 2
Items: 
Size: 556202 Color: 2
Size: 443121 Color: 3

Bin 3935: 686 of cap free
Amount of items: 2
Items: 
Size: 542210 Color: 3
Size: 457105 Color: 4

Bin 3936: 686 of cap free
Amount of items: 2
Items: 
Size: 630682 Color: 1
Size: 368633 Color: 4

Bin 3937: 689 of cap free
Amount of items: 2
Items: 
Size: 581914 Color: 2
Size: 417398 Color: 3

Bin 3938: 689 of cap free
Amount of items: 2
Items: 
Size: 558673 Color: 2
Size: 440639 Color: 4

Bin 3939: 694 of cap free
Amount of items: 2
Items: 
Size: 783805 Color: 2
Size: 215502 Color: 0

Bin 3940: 696 of cap free
Amount of items: 2
Items: 
Size: 778229 Color: 1
Size: 221076 Color: 4

Bin 3941: 700 of cap free
Amount of items: 2
Items: 
Size: 593842 Color: 2
Size: 405459 Color: 3

Bin 3942: 701 of cap free
Amount of items: 2
Items: 
Size: 639855 Color: 1
Size: 359445 Color: 4

Bin 3943: 703 of cap free
Amount of items: 2
Items: 
Size: 581015 Color: 2
Size: 418283 Color: 3

Bin 3944: 708 of cap free
Amount of items: 2
Items: 
Size: 707175 Color: 4
Size: 292118 Color: 1

Bin 3945: 709 of cap free
Amount of items: 2
Items: 
Size: 650859 Color: 4
Size: 348433 Color: 3

Bin 3946: 710 of cap free
Amount of items: 2
Items: 
Size: 661340 Color: 1
Size: 337951 Color: 0

Bin 3947: 714 of cap free
Amount of items: 2
Items: 
Size: 721023 Color: 0
Size: 278264 Color: 4

Bin 3948: 717 of cap free
Amount of items: 2
Items: 
Size: 703864 Color: 1
Size: 295420 Color: 3

Bin 3949: 718 of cap free
Amount of items: 2
Items: 
Size: 652681 Color: 0
Size: 346602 Color: 3

Bin 3950: 719 of cap free
Amount of items: 2
Items: 
Size: 769632 Color: 4
Size: 229650 Color: 3

Bin 3951: 724 of cap free
Amount of items: 2
Items: 
Size: 799635 Color: 4
Size: 199642 Color: 2

Bin 3952: 725 of cap free
Amount of items: 2
Items: 
Size: 768286 Color: 3
Size: 230990 Color: 2

Bin 3953: 732 of cap free
Amount of items: 2
Items: 
Size: 618242 Color: 3
Size: 381027 Color: 1

Bin 3954: 737 of cap free
Amount of items: 2
Items: 
Size: 740040 Color: 4
Size: 259224 Color: 0

Bin 3955: 737 of cap free
Amount of items: 2
Items: 
Size: 630566 Color: 2
Size: 368698 Color: 1

Bin 3956: 738 of cap free
Amount of items: 2
Items: 
Size: 788338 Color: 3
Size: 210925 Color: 4

Bin 3957: 739 of cap free
Amount of items: 2
Items: 
Size: 763344 Color: 4
Size: 235918 Color: 2

Bin 3958: 741 of cap free
Amount of items: 2
Items: 
Size: 786321 Color: 4
Size: 212939 Color: 3

Bin 3959: 742 of cap free
Amount of items: 2
Items: 
Size: 681391 Color: 2
Size: 317868 Color: 4

Bin 3960: 743 of cap free
Amount of items: 2
Items: 
Size: 710030 Color: 1
Size: 289228 Color: 4

Bin 3961: 744 of cap free
Amount of items: 2
Items: 
Size: 545412 Color: 4
Size: 453845 Color: 0

Bin 3962: 745 of cap free
Amount of items: 2
Items: 
Size: 759405 Color: 0
Size: 239851 Color: 1

Bin 3963: 745 of cap free
Amount of items: 2
Items: 
Size: 533854 Color: 1
Size: 465402 Color: 3

Bin 3964: 747 of cap free
Amount of items: 2
Items: 
Size: 626567 Color: 4
Size: 372687 Color: 0

Bin 3965: 751 of cap free
Amount of items: 2
Items: 
Size: 603823 Color: 0
Size: 395427 Color: 3

Bin 3966: 754 of cap free
Amount of items: 2
Items: 
Size: 765569 Color: 1
Size: 233678 Color: 0

Bin 3967: 754 of cap free
Amount of items: 2
Items: 
Size: 643233 Color: 0
Size: 356014 Color: 4

Bin 3968: 755 of cap free
Amount of items: 2
Items: 
Size: 733426 Color: 1
Size: 265820 Color: 0

Bin 3969: 756 of cap free
Amount of items: 2
Items: 
Size: 571827 Color: 3
Size: 427418 Color: 2

Bin 3970: 764 of cap free
Amount of items: 2
Items: 
Size: 595109 Color: 3
Size: 404128 Color: 0

Bin 3971: 768 of cap free
Amount of items: 2
Items: 
Size: 561905 Color: 4
Size: 437328 Color: 3

Bin 3972: 771 of cap free
Amount of items: 2
Items: 
Size: 581884 Color: 3
Size: 417346 Color: 0

Bin 3973: 773 of cap free
Amount of items: 2
Items: 
Size: 760220 Color: 3
Size: 239008 Color: 4

Bin 3974: 773 of cap free
Amount of items: 2
Items: 
Size: 605577 Color: 4
Size: 393651 Color: 2

Bin 3975: 776 of cap free
Amount of items: 2
Items: 
Size: 706626 Color: 3
Size: 292599 Color: 4

Bin 3976: 781 of cap free
Amount of items: 2
Items: 
Size: 739148 Color: 2
Size: 260072 Color: 4

Bin 3977: 781 of cap free
Amount of items: 2
Items: 
Size: 534795 Color: 2
Size: 464425 Color: 0

Bin 3978: 784 of cap free
Amount of items: 2
Items: 
Size: 628392 Color: 4
Size: 370825 Color: 2

Bin 3979: 784 of cap free
Amount of items: 2
Items: 
Size: 545434 Color: 0
Size: 453783 Color: 2

Bin 3980: 785 of cap free
Amount of items: 2
Items: 
Size: 616204 Color: 1
Size: 383012 Color: 0

Bin 3981: 788 of cap free
Amount of items: 2
Items: 
Size: 560807 Color: 3
Size: 438406 Color: 2

Bin 3982: 789 of cap free
Amount of items: 2
Items: 
Size: 611743 Color: 3
Size: 387469 Color: 4

Bin 3983: 793 of cap free
Amount of items: 2
Items: 
Size: 574928 Color: 0
Size: 424280 Color: 1

Bin 3984: 794 of cap free
Amount of items: 2
Items: 
Size: 618220 Color: 2
Size: 380987 Color: 4

Bin 3985: 799 of cap free
Amount of items: 2
Items: 
Size: 604544 Color: 3
Size: 394658 Color: 4

Bin 3986: 805 of cap free
Amount of items: 2
Items: 
Size: 774233 Color: 1
Size: 224963 Color: 3

Bin 3987: 806 of cap free
Amount of items: 2
Items: 
Size: 648074 Color: 2
Size: 351121 Color: 1

Bin 3988: 808 of cap free
Amount of items: 2
Items: 
Size: 564739 Color: 2
Size: 434454 Color: 0

Bin 3989: 816 of cap free
Amount of items: 2
Items: 
Size: 637047 Color: 4
Size: 362138 Color: 1

Bin 3990: 819 of cap free
Amount of items: 2
Items: 
Size: 799549 Color: 2
Size: 199633 Color: 3

Bin 3991: 819 of cap free
Amount of items: 2
Items: 
Size: 703763 Color: 4
Size: 295419 Color: 1

Bin 3992: 823 of cap free
Amount of items: 2
Items: 
Size: 651874 Color: 4
Size: 347304 Color: 0

Bin 3993: 824 of cap free
Amount of items: 2
Items: 
Size: 690058 Color: 2
Size: 309119 Color: 1

Bin 3994: 833 of cap free
Amount of items: 2
Items: 
Size: 681404 Color: 4
Size: 317764 Color: 0

Bin 3995: 842 of cap free
Amount of items: 2
Items: 
Size: 799540 Color: 1
Size: 199619 Color: 4

Bin 3996: 843 of cap free
Amount of items: 2
Items: 
Size: 709057 Color: 3
Size: 290101 Color: 4

Bin 3997: 846 of cap free
Amount of items: 2
Items: 
Size: 733832 Color: 0
Size: 265323 Color: 2

Bin 3998: 846 of cap free
Amount of items: 2
Items: 
Size: 641165 Color: 4
Size: 357990 Color: 0

Bin 3999: 862 of cap free
Amount of items: 2
Items: 
Size: 676313 Color: 0
Size: 322826 Color: 1

Bin 4000: 865 of cap free
Amount of items: 2
Items: 
Size: 600714 Color: 3
Size: 398422 Color: 2

Bin 4001: 865 of cap free
Amount of items: 2
Items: 
Size: 686732 Color: 2
Size: 312404 Color: 3

Bin 4002: 865 of cap free
Amount of items: 2
Items: 
Size: 692976 Color: 1
Size: 306160 Color: 0

Bin 4003: 865 of cap free
Amount of items: 2
Items: 
Size: 608703 Color: 2
Size: 390433 Color: 3

Bin 4004: 873 of cap free
Amount of items: 2
Items: 
Size: 644112 Color: 4
Size: 355016 Color: 0

Bin 4005: 875 of cap free
Amount of items: 2
Items: 
Size: 553534 Color: 0
Size: 445592 Color: 1

Bin 4006: 878 of cap free
Amount of items: 2
Items: 
Size: 648971 Color: 2
Size: 350152 Color: 3

Bin 4007: 878 of cap free
Amount of items: 2
Items: 
Size: 523258 Color: 2
Size: 475865 Color: 1

Bin 4008: 880 of cap free
Amount of items: 2
Items: 
Size: 716031 Color: 4
Size: 283090 Color: 0

Bin 4009: 880 of cap free
Amount of items: 2
Items: 
Size: 499671 Color: 0
Size: 499450 Color: 4

Bin 4010: 881 of cap free
Amount of items: 2
Items: 
Size: 652592 Color: 0
Size: 346528 Color: 3

Bin 4011: 882 of cap free
Amount of items: 2
Items: 
Size: 650688 Color: 1
Size: 348431 Color: 3

Bin 4012: 895 of cap free
Amount of items: 2
Items: 
Size: 528538 Color: 2
Size: 470568 Color: 3

Bin 4013: 899 of cap free
Amount of items: 2
Items: 
Size: 685247 Color: 2
Size: 313855 Color: 3

Bin 4014: 899 of cap free
Amount of items: 2
Items: 
Size: 593459 Color: 3
Size: 405643 Color: 2

Bin 4015: 899 of cap free
Amount of items: 2
Items: 
Size: 575748 Color: 2
Size: 423354 Color: 1

Bin 4016: 901 of cap free
Amount of items: 2
Items: 
Size: 568001 Color: 0
Size: 431099 Color: 2

Bin 4017: 902 of cap free
Amount of items: 2
Items: 
Size: 549925 Color: 4
Size: 449174 Color: 1

Bin 4018: 903 of cap free
Amount of items: 2
Items: 
Size: 755292 Color: 2
Size: 243806 Color: 4

Bin 4019: 904 of cap free
Amount of items: 2
Items: 
Size: 760177 Color: 4
Size: 238920 Color: 0

Bin 4020: 909 of cap free
Amount of items: 2
Items: 
Size: 751206 Color: 4
Size: 247886 Color: 2

Bin 4021: 913 of cap free
Amount of items: 2
Items: 
Size: 564709 Color: 1
Size: 434379 Color: 3

Bin 4022: 914 of cap free
Amount of items: 2
Items: 
Size: 595102 Color: 0
Size: 403985 Color: 4

Bin 4023: 918 of cap free
Amount of items: 2
Items: 
Size: 720937 Color: 0
Size: 278146 Color: 1

Bin 4024: 919 of cap free
Amount of items: 2
Items: 
Size: 662369 Color: 2
Size: 336713 Color: 0

Bin 4025: 924 of cap free
Amount of items: 2
Items: 
Size: 652703 Color: 4
Size: 346374 Color: 0

Bin 4026: 924 of cap free
Amount of items: 2
Items: 
Size: 523262 Color: 1
Size: 475815 Color: 4

Bin 4027: 932 of cap free
Amount of items: 2
Items: 
Size: 786151 Color: 0
Size: 212918 Color: 4

Bin 4028: 956 of cap free
Amount of items: 2
Items: 
Size: 569002 Color: 4
Size: 430043 Color: 3

Bin 4029: 957 of cap free
Amount of items: 2
Items: 
Size: 580890 Color: 2
Size: 418154 Color: 3

Bin 4030: 958 of cap free
Amount of items: 2
Items: 
Size: 689868 Color: 3
Size: 309175 Color: 2

Bin 4031: 971 of cap free
Amount of items: 2
Items: 
Size: 718183 Color: 2
Size: 280847 Color: 0

Bin 4032: 973 of cap free
Amount of items: 2
Items: 
Size: 786147 Color: 1
Size: 212881 Color: 0

Bin 4033: 975 of cap free
Amount of items: 2
Items: 
Size: 555969 Color: 0
Size: 443057 Color: 2

Bin 4034: 976 of cap free
Amount of items: 2
Items: 
Size: 794710 Color: 3
Size: 204315 Color: 0

Bin 4035: 983 of cap free
Amount of items: 2
Items: 
Size: 662629 Color: 0
Size: 336389 Color: 2

Bin 4036: 984 of cap free
Amount of items: 2
Items: 
Size: 746722 Color: 3
Size: 252295 Color: 2

Bin 4037: 984 of cap free
Amount of items: 2
Items: 
Size: 564736 Color: 3
Size: 434281 Color: 0

Bin 4038: 986 of cap free
Amount of items: 2
Items: 
Size: 610186 Color: 3
Size: 388829 Color: 1

Bin 4039: 988 of cap free
Amount of items: 2
Items: 
Size: 581854 Color: 3
Size: 417159 Color: 4

Bin 4040: 991 of cap free
Amount of items: 2
Items: 
Size: 739029 Color: 4
Size: 259981 Color: 0

Bin 4041: 1000 of cap free
Amount of items: 2
Items: 
Size: 544401 Color: 2
Size: 454600 Color: 4

Bin 4042: 1005 of cap free
Amount of items: 2
Items: 
Size: 698645 Color: 4
Size: 300351 Color: 2

Bin 4043: 1008 of cap free
Amount of items: 2
Items: 
Size: 799408 Color: 2
Size: 199585 Color: 4

Bin 4044: 1011 of cap free
Amount of items: 2
Items: 
Size: 605382 Color: 2
Size: 393608 Color: 4

Bin 4045: 1026 of cap free
Amount of items: 2
Items: 
Size: 560609 Color: 1
Size: 438366 Color: 3

Bin 4046: 1036 of cap free
Amount of items: 2
Items: 
Size: 689858 Color: 4
Size: 309107 Color: 3

Bin 4047: 1050 of cap free
Amount of items: 2
Items: 
Size: 799395 Color: 1
Size: 199556 Color: 0

Bin 4048: 1056 of cap free
Amount of items: 2
Items: 
Size: 530900 Color: 3
Size: 468045 Color: 2

Bin 4049: 1070 of cap free
Amount of items: 2
Items: 
Size: 548498 Color: 1
Size: 450433 Color: 3

Bin 4050: 1074 of cap free
Amount of items: 2
Items: 
Size: 523123 Color: 0
Size: 475804 Color: 4

Bin 4051: 1078 of cap free
Amount of items: 2
Items: 
Size: 610301 Color: 1
Size: 388622 Color: 4

Bin 4052: 1081 of cap free
Amount of items: 2
Items: 
Size: 753424 Color: 4
Size: 245496 Color: 2

Bin 4053: 1081 of cap free
Amount of items: 2
Items: 
Size: 615915 Color: 4
Size: 383005 Color: 2

Bin 4054: 1082 of cap free
Amount of items: 2
Items: 
Size: 632854 Color: 0
Size: 366065 Color: 4

Bin 4055: 1087 of cap free
Amount of items: 2
Items: 
Size: 662627 Color: 0
Size: 336287 Color: 2

Bin 4056: 1090 of cap free
Amount of items: 2
Items: 
Size: 579004 Color: 4
Size: 419907 Color: 0

Bin 4057: 1091 of cap free
Amount of items: 2
Items: 
Size: 610296 Color: 1
Size: 388614 Color: 0

Bin 4058: 1095 of cap free
Amount of items: 2
Items: 
Size: 726028 Color: 0
Size: 272878 Color: 2

Bin 4059: 1096 of cap free
Amount of items: 2
Items: 
Size: 742579 Color: 0
Size: 256326 Color: 2

Bin 4060: 1097 of cap free
Amount of items: 2
Items: 
Size: 632860 Color: 4
Size: 366044 Color: 2

Bin 4061: 1102 of cap free
Amount of items: 2
Items: 
Size: 642912 Color: 2
Size: 355987 Color: 0

Bin 4062: 1105 of cap free
Amount of items: 2
Items: 
Size: 709040 Color: 2
Size: 289856 Color: 1

Bin 4063: 1117 of cap free
Amount of items: 2
Items: 
Size: 574603 Color: 4
Size: 424281 Color: 0

Bin 4064: 1120 of cap free
Amount of items: 2
Items: 
Size: 740785 Color: 2
Size: 258096 Color: 1

Bin 4065: 1124 of cap free
Amount of items: 2
Items: 
Size: 790566 Color: 4
Size: 208311 Color: 2

Bin 4066: 1128 of cap free
Amount of items: 2
Items: 
Size: 671814 Color: 0
Size: 327059 Color: 2

Bin 4067: 1130 of cap free
Amount of items: 2
Items: 
Size: 566172 Color: 3
Size: 432699 Color: 4

Bin 4068: 1134 of cap free
Amount of items: 2
Items: 
Size: 712310 Color: 2
Size: 286557 Color: 4

Bin 4069: 1143 of cap free
Amount of items: 2
Items: 
Size: 553488 Color: 1
Size: 445370 Color: 2

Bin 4070: 1147 of cap free
Amount of items: 2
Items: 
Size: 748648 Color: 2
Size: 250206 Color: 0

Bin 4071: 1147 of cap free
Amount of items: 2
Items: 
Size: 615857 Color: 0
Size: 382997 Color: 3

Bin 4072: 1157 of cap free
Amount of items: 2
Items: 
Size: 636745 Color: 3
Size: 362099 Color: 0

Bin 4073: 1164 of cap free
Amount of items: 2
Items: 
Size: 506315 Color: 4
Size: 492522 Color: 3

Bin 4074: 1167 of cap free
Amount of items: 2
Items: 
Size: 541935 Color: 2
Size: 456899 Color: 3

Bin 4075: 1173 of cap free
Amount of items: 2
Items: 
Size: 566163 Color: 1
Size: 432665 Color: 2

Bin 4076: 1180 of cap free
Amount of items: 2
Items: 
Size: 564555 Color: 4
Size: 434266 Color: 1

Bin 4077: 1203 of cap free
Amount of items: 2
Items: 
Size: 738821 Color: 3
Size: 259977 Color: 1

Bin 4078: 1206 of cap free
Amount of items: 2
Items: 
Size: 607241 Color: 2
Size: 391554 Color: 0

Bin 4079: 1209 of cap free
Amount of items: 2
Items: 
Size: 748600 Color: 2
Size: 250192 Color: 1

Bin 4080: 1210 of cap free
Amount of items: 2
Items: 
Size: 783752 Color: 2
Size: 215039 Color: 0

Bin 4081: 1212 of cap free
Amount of items: 2
Items: 
Size: 508567 Color: 3
Size: 490222 Color: 1

Bin 4082: 1215 of cap free
Amount of items: 2
Items: 
Size: 562790 Color: 1
Size: 435996 Color: 2

Bin 4083: 1224 of cap free
Amount of items: 2
Items: 
Size: 712301 Color: 1
Size: 286476 Color: 4

Bin 4084: 1227 of cap free
Amount of items: 2
Items: 
Size: 514486 Color: 2
Size: 484288 Color: 0

Bin 4085: 1231 of cap free
Amount of items: 2
Items: 
Size: 639809 Color: 2
Size: 358961 Color: 0

Bin 4086: 1236 of cap free
Amount of items: 2
Items: 
Size: 755044 Color: 0
Size: 243721 Color: 2

Bin 4087: 1237 of cap free
Amount of items: 2
Items: 
Size: 636676 Color: 4
Size: 362088 Color: 3

Bin 4088: 1247 of cap free
Amount of items: 2
Items: 
Size: 698523 Color: 0
Size: 300231 Color: 3

Bin 4089: 1251 of cap free
Amount of items: 2
Items: 
Size: 595075 Color: 3
Size: 403675 Color: 1

Bin 4090: 1277 of cap free
Amount of items: 2
Items: 
Size: 508477 Color: 2
Size: 490247 Color: 3

Bin 4091: 1283 of cap free
Amount of items: 2
Items: 
Size: 688417 Color: 3
Size: 310301 Color: 4

Bin 4092: 1295 of cap free
Amount of items: 2
Items: 
Size: 712266 Color: 3
Size: 286440 Color: 2

Bin 4093: 1296 of cap free
Amount of items: 2
Items: 
Size: 775571 Color: 2
Size: 223134 Color: 3

Bin 4094: 1298 of cap free
Amount of items: 2
Items: 
Size: 777729 Color: 3
Size: 220974 Color: 4

Bin 4095: 1298 of cap free
Amount of items: 2
Items: 
Size: 593362 Color: 0
Size: 405341 Color: 2

Bin 4096: 1307 of cap free
Amount of items: 2
Items: 
Size: 765691 Color: 0
Size: 233003 Color: 3

Bin 4097: 1310 of cap free
Amount of items: 2
Items: 
Size: 742538 Color: 4
Size: 256153 Color: 0

Bin 4098: 1315 of cap free
Amount of items: 2
Items: 
Size: 694169 Color: 0
Size: 304517 Color: 1

Bin 4099: 1315 of cap free
Amount of items: 2
Items: 
Size: 639726 Color: 1
Size: 358960 Color: 0

Bin 4100: 1320 of cap free
Amount of items: 2
Items: 
Size: 748574 Color: 1
Size: 250107 Color: 3

Bin 4101: 1328 of cap free
Amount of items: 2
Items: 
Size: 553324 Color: 4
Size: 445349 Color: 1

Bin 4102: 1329 of cap free
Amount of items: 2
Items: 
Size: 786101 Color: 1
Size: 212571 Color: 3

Bin 4103: 1330 of cap free
Amount of items: 2
Items: 
Size: 698431 Color: 2
Size: 300240 Color: 0

Bin 4104: 1340 of cap free
Amount of items: 2
Items: 
Size: 566036 Color: 4
Size: 432625 Color: 2

Bin 4105: 1343 of cap free
Amount of items: 2
Items: 
Size: 562703 Color: 1
Size: 435955 Color: 0

Bin 4106: 1344 of cap free
Amount of items: 2
Items: 
Size: 596251 Color: 1
Size: 402406 Color: 0

Bin 4107: 1346 of cap free
Amount of items: 2
Items: 
Size: 665376 Color: 0
Size: 333279 Color: 2

Bin 4108: 1356 of cap free
Amount of items: 2
Items: 
Size: 656166 Color: 1
Size: 342479 Color: 0

Bin 4109: 1359 of cap free
Amount of items: 2
Items: 
Size: 584385 Color: 1
Size: 414257 Color: 0

Bin 4110: 1364 of cap free
Amount of items: 2
Items: 
Size: 503192 Color: 1
Size: 495445 Color: 0

Bin 4111: 1373 of cap free
Amount of items: 2
Items: 
Size: 506216 Color: 4
Size: 492412 Color: 0

Bin 4112: 1380 of cap free
Amount of items: 2
Items: 
Size: 650226 Color: 2
Size: 348395 Color: 3

Bin 4113: 1380 of cap free
Amount of items: 2
Items: 
Size: 748535 Color: 1
Size: 250086 Color: 3

Bin 4114: 1381 of cap free
Amount of items: 2
Items: 
Size: 508398 Color: 0
Size: 490222 Color: 4

Bin 4115: 1387 of cap free
Amount of items: 2
Items: 
Size: 584540 Color: 0
Size: 414074 Color: 3

Bin 4116: 1389 of cap free
Amount of items: 2
Items: 
Size: 639661 Color: 1
Size: 358951 Color: 4

Bin 4117: 1396 of cap free
Amount of items: 2
Items: 
Size: 742519 Color: 1
Size: 256086 Color: 0

Bin 4118: 1399 of cap free
Amount of items: 2
Items: 
Size: 548456 Color: 4
Size: 450146 Color: 2

Bin 4119: 1402 of cap free
Amount of items: 2
Items: 
Size: 553266 Color: 1
Size: 445333 Color: 0

Bin 4120: 1403 of cap free
Amount of items: 2
Items: 
Size: 775582 Color: 1
Size: 223016 Color: 2

Bin 4121: 1403 of cap free
Amount of items: 2
Items: 
Size: 607192 Color: 0
Size: 391406 Color: 1

Bin 4122: 1404 of cap free
Amount of items: 2
Items: 
Size: 748498 Color: 2
Size: 250099 Color: 1

Bin 4123: 1404 of cap free
Amount of items: 2
Items: 
Size: 610020 Color: 0
Size: 388577 Color: 4

Bin 4124: 1431 of cap free
Amount of items: 2
Items: 
Size: 720901 Color: 0
Size: 277669 Color: 3

Bin 4125: 1436 of cap free
Amount of items: 2
Items: 
Size: 530759 Color: 4
Size: 467806 Color: 2

Bin 4126: 1440 of cap free
Amount of items: 2
Items: 
Size: 692566 Color: 1
Size: 305995 Color: 4

Bin 4127: 1446 of cap free
Amount of items: 2
Items: 
Size: 540401 Color: 0
Size: 458154 Color: 4

Bin 4128: 1450 of cap free
Amount of items: 2
Items: 
Size: 590078 Color: 0
Size: 408473 Color: 4

Bin 4129: 1453 of cap free
Amount of items: 2
Items: 
Size: 717753 Color: 2
Size: 280795 Color: 0

Bin 4130: 1455 of cap free
Amount of items: 2
Items: 
Size: 725767 Color: 0
Size: 272779 Color: 3

Bin 4131: 1460 of cap free
Amount of items: 2
Items: 
Size: 676056 Color: 0
Size: 322485 Color: 1

Bin 4132: 1462 of cap free
Amount of items: 2
Items: 
Size: 779290 Color: 4
Size: 219249 Color: 2

Bin 4133: 1476 of cap free
Amount of items: 2
Items: 
Size: 530538 Color: 0
Size: 467987 Color: 4

Bin 4134: 1495 of cap free
Amount of items: 2
Items: 
Size: 720838 Color: 3
Size: 277668 Color: 4

Bin 4135: 1500 of cap free
Amount of items: 2
Items: 
Size: 566160 Color: 2
Size: 432341 Color: 3

Bin 4136: 1505 of cap free
Amount of items: 2
Items: 
Size: 536283 Color: 3
Size: 462213 Color: 4

Bin 4137: 1512 of cap free
Amount of items: 2
Items: 
Size: 799235 Color: 0
Size: 199254 Color: 2

Bin 4138: 1515 of cap free
Amount of items: 2
Items: 
Size: 728753 Color: 0
Size: 269733 Color: 3

Bin 4139: 1517 of cap free
Amount of items: 2
Items: 
Size: 624376 Color: 4
Size: 374108 Color: 2

Bin 4140: 1521 of cap free
Amount of items: 2
Items: 
Size: 785988 Color: 2
Size: 212492 Color: 3

Bin 4141: 1526 of cap free
Amount of items: 2
Items: 
Size: 715846 Color: 4
Size: 282629 Color: 0

Bin 4142: 1536 of cap free
Amount of items: 2
Items: 
Size: 615833 Color: 2
Size: 382632 Color: 1

Bin 4143: 1541 of cap free
Amount of items: 2
Items: 
Size: 548326 Color: 0
Size: 450134 Color: 1

Bin 4144: 1548 of cap free
Amount of items: 2
Items: 
Size: 594702 Color: 0
Size: 403751 Color: 3

Bin 4145: 1550 of cap free
Amount of items: 2
Items: 
Size: 650070 Color: 0
Size: 348381 Color: 4

Bin 4146: 1561 of cap free
Amount of items: 2
Items: 
Size: 533613 Color: 4
Size: 464827 Color: 2

Bin 4147: 1579 of cap free
Amount of items: 2
Items: 
Size: 728196 Color: 3
Size: 270226 Color: 4

Bin 4148: 1580 of cap free
Amount of items: 2
Items: 
Size: 590469 Color: 4
Size: 407952 Color: 2

Bin 4149: 1597 of cap free
Amount of items: 2
Items: 
Size: 565883 Color: 4
Size: 432521 Color: 2

Bin 4150: 1597 of cap free
Amount of items: 2
Items: 
Size: 499638 Color: 0
Size: 498766 Color: 2

Bin 4151: 1602 of cap free
Amount of items: 2
Items: 
Size: 548268 Color: 1
Size: 450131 Color: 4

Bin 4152: 1602 of cap free
Amount of items: 2
Items: 
Size: 600704 Color: 2
Size: 397695 Color: 1

Bin 4153: 1615 of cap free
Amount of items: 2
Items: 
Size: 755229 Color: 4
Size: 243157 Color: 0

Bin 4154: 1628 of cap free
Amount of items: 2
Items: 
Size: 584367 Color: 0
Size: 414006 Color: 1

Bin 4155: 1628 of cap free
Amount of items: 2
Items: 
Size: 655987 Color: 4
Size: 342386 Color: 0

Bin 4156: 1628 of cap free
Amount of items: 2
Items: 
Size: 738637 Color: 0
Size: 259736 Color: 4

Bin 4157: 1646 of cap free
Amount of items: 2
Items: 
Size: 711983 Color: 4
Size: 286372 Color: 3

Bin 4158: 1647 of cap free
Amount of items: 2
Items: 
Size: 740420 Color: 0
Size: 257934 Color: 1

Bin 4159: 1652 of cap free
Amount of items: 2
Items: 
Size: 557789 Color: 2
Size: 440560 Color: 0

Bin 4160: 1674 of cap free
Amount of items: 2
Items: 
Size: 642895 Color: 0
Size: 355432 Color: 4

Bin 4161: 1695 of cap free
Amount of items: 2
Items: 
Size: 617916 Color: 4
Size: 380390 Color: 0

Bin 4162: 1698 of cap free
Amount of items: 2
Items: 
Size: 540037 Color: 3
Size: 458266 Color: 0

Bin 4163: 1719 of cap free
Amount of items: 2
Items: 
Size: 692441 Color: 1
Size: 305841 Color: 3

Bin 4164: 1719 of cap free
Amount of items: 2
Items: 
Size: 530501 Color: 4
Size: 467781 Color: 3

Bin 4165: 1722 of cap free
Amount of items: 2
Items: 
Size: 799203 Color: 1
Size: 199076 Color: 3

Bin 4166: 1729 of cap free
Amount of items: 2
Items: 
Size: 530510 Color: 3
Size: 467762 Color: 4

Bin 4167: 1734 of cap free
Amount of items: 2
Items: 
Size: 617768 Color: 0
Size: 380499 Color: 4

Bin 4168: 1735 of cap free
Amount of items: 2
Items: 
Size: 589810 Color: 0
Size: 408456 Color: 4

Bin 4169: 1801 of cap free
Amount of items: 2
Items: 
Size: 609711 Color: 4
Size: 388489 Color: 0

Bin 4170: 1802 of cap free
Amount of items: 2
Items: 
Size: 720791 Color: 2
Size: 277408 Color: 3

Bin 4171: 1811 of cap free
Amount of items: 2
Items: 
Size: 514241 Color: 4
Size: 483949 Color: 3

Bin 4172: 1812 of cap free
Amount of items: 2
Items: 
Size: 715749 Color: 4
Size: 282440 Color: 1

Bin 4173: 1813 of cap free
Amount of items: 2
Items: 
Size: 574875 Color: 0
Size: 423313 Color: 2

Bin 4174: 1828 of cap free
Amount of items: 2
Items: 
Size: 578539 Color: 3
Size: 419634 Color: 1

Bin 4175: 1837 of cap free
Amount of items: 2
Items: 
Size: 656131 Color: 0
Size: 342033 Color: 3

Bin 4176: 1844 of cap free
Amount of items: 2
Items: 
Size: 552839 Color: 4
Size: 445318 Color: 3

Bin 4177: 1853 of cap free
Amount of items: 2
Items: 
Size: 581164 Color: 3
Size: 416984 Color: 0

Bin 4178: 1863 of cap free
Amount of items: 2
Items: 
Size: 580760 Color: 1
Size: 417378 Color: 3

Bin 4179: 1884 of cap free
Amount of items: 2
Items: 
Size: 662587 Color: 0
Size: 335530 Color: 3

Bin 4180: 1892 of cap free
Amount of items: 2
Items: 
Size: 720736 Color: 3
Size: 277373 Color: 4

Bin 4181: 1901 of cap free
Amount of items: 2
Items: 
Size: 503002 Color: 1
Size: 495098 Color: 3

Bin 4182: 1906 of cap free
Amount of items: 2
Items: 
Size: 527688 Color: 4
Size: 470407 Color: 1

Bin 4183: 1917 of cap free
Amount of items: 2
Items: 
Size: 544407 Color: 3
Size: 453677 Color: 2

Bin 4184: 1929 of cap free
Amount of items: 2
Items: 
Size: 555023 Color: 4
Size: 443049 Color: 1

Bin 4185: 1953 of cap free
Amount of items: 2
Items: 
Size: 701581 Color: 1
Size: 296467 Color: 0

Bin 4186: 1963 of cap free
Amount of items: 2
Items: 
Size: 514264 Color: 3
Size: 483774 Color: 1

Bin 4187: 1965 of cap free
Amount of items: 2
Items: 
Size: 777668 Color: 0
Size: 220368 Color: 4

Bin 4188: 1970 of cap free
Amount of items: 2
Items: 
Size: 581132 Color: 3
Size: 416899 Color: 2

Bin 4189: 1973 of cap free
Amount of items: 2
Items: 
Size: 552824 Color: 0
Size: 445204 Color: 4

Bin 4190: 2002 of cap free
Amount of items: 2
Items: 
Size: 530284 Color: 2
Size: 467715 Color: 4

Bin 4191: 2012 of cap free
Amount of items: 2
Items: 
Size: 544288 Color: 2
Size: 453701 Color: 1

Bin 4192: 2018 of cap free
Amount of items: 2
Items: 
Size: 533559 Color: 4
Size: 464424 Color: 1

Bin 4193: 2021 of cap free
Amount of items: 2
Items: 
Size: 552782 Color: 4
Size: 445198 Color: 1

Bin 4194: 2026 of cap free
Amount of items: 2
Items: 
Size: 578422 Color: 3
Size: 419553 Color: 1

Bin 4195: 2030 of cap free
Amount of items: 2
Items: 
Size: 727760 Color: 3
Size: 270211 Color: 1

Bin 4196: 2034 of cap free
Amount of items: 2
Items: 
Size: 527575 Color: 2
Size: 470392 Color: 4

Bin 4197: 2035 of cap free
Amount of items: 2
Items: 
Size: 580648 Color: 0
Size: 417318 Color: 3

Bin 4198: 2058 of cap free
Amount of items: 2
Items: 
Size: 554944 Color: 4
Size: 442999 Color: 1

Bin 4199: 2084 of cap free
Amount of items: 2
Items: 
Size: 560584 Color: 3
Size: 437333 Color: 4

Bin 4200: 2086 of cap free
Amount of items: 2
Items: 
Size: 552781 Color: 4
Size: 445134 Color: 0

Bin 4201: 2087 of cap free
Amount of items: 2
Items: 
Size: 600608 Color: 0
Size: 397306 Color: 2

Bin 4202: 2092 of cap free
Amount of items: 2
Items: 
Size: 565768 Color: 0
Size: 432141 Color: 1

Bin 4203: 2123 of cap free
Amount of items: 2
Items: 
Size: 785949 Color: 3
Size: 211929 Color: 0

Bin 4204: 2136 of cap free
Amount of items: 2
Items: 
Size: 502819 Color: 0
Size: 495046 Color: 2

Bin 4205: 2140 of cap free
Amount of items: 2
Items: 
Size: 514179 Color: 3
Size: 483682 Color: 4

Bin 4206: 2149 of cap free
Amount of items: 2
Items: 
Size: 530270 Color: 2
Size: 467582 Color: 0

Bin 4207: 2152 of cap free
Amount of items: 2
Items: 
Size: 651657 Color: 4
Size: 346192 Color: 2

Bin 4208: 2160 of cap free
Amount of items: 2
Items: 
Size: 557769 Color: 2
Size: 440072 Color: 3

Bin 4209: 2163 of cap free
Amount of items: 2
Items: 
Size: 499096 Color: 0
Size: 498742 Color: 1

Bin 4210: 2190 of cap free
Amount of items: 2
Items: 
Size: 527522 Color: 1
Size: 470289 Color: 2

Bin 4211: 2190 of cap free
Amount of items: 2
Items: 
Size: 617714 Color: 0
Size: 380097 Color: 4

Bin 4212: 2193 of cap free
Amount of items: 2
Items: 
Size: 565762 Color: 1
Size: 432046 Color: 4

Bin 4213: 2198 of cap free
Amount of items: 2
Items: 
Size: 527520 Color: 0
Size: 470283 Color: 2

Bin 4214: 2205 of cap free
Amount of items: 2
Items: 
Size: 544212 Color: 4
Size: 453584 Color: 1

Bin 4215: 2215 of cap free
Amount of items: 2
Items: 
Size: 565741 Color: 4
Size: 432045 Color: 0

Bin 4216: 2225 of cap free
Amount of items: 2
Items: 
Size: 502737 Color: 4
Size: 495039 Color: 1

Bin 4217: 2239 of cap free
Amount of items: 2
Items: 
Size: 720734 Color: 0
Size: 277028 Color: 1

Bin 4218: 2261 of cap free
Amount of items: 2
Items: 
Size: 642850 Color: 0
Size: 354890 Color: 2

Bin 4219: 2281 of cap free
Amount of items: 2
Items: 
Size: 574539 Color: 0
Size: 423181 Color: 1

Bin 4220: 2295 of cap free
Amount of items: 2
Items: 
Size: 635704 Color: 2
Size: 362002 Color: 1

Bin 4221: 2315 of cap free
Amount of items: 2
Items: 
Size: 647505 Color: 3
Size: 350181 Color: 2

Bin 4222: 2320 of cap free
Amount of items: 2
Items: 
Size: 764731 Color: 4
Size: 232950 Color: 0

Bin 4223: 2324 of cap free
Amount of items: 2
Items: 
Size: 527446 Color: 3
Size: 470231 Color: 2

Bin 4224: 2385 of cap free
Amount of items: 2
Items: 
Size: 679853 Color: 0
Size: 317763 Color: 2

Bin 4225: 2398 of cap free
Amount of items: 2
Items: 
Size: 527423 Color: 0
Size: 470180 Color: 2

Bin 4226: 2399 of cap free
Amount of items: 2
Items: 
Size: 624004 Color: 4
Size: 373598 Color: 0

Bin 4227: 2414 of cap free
Amount of items: 2
Items: 
Size: 574596 Color: 1
Size: 422991 Color: 0

Bin 4228: 2416 of cap free
Amount of items: 2
Items: 
Size: 635699 Color: 1
Size: 361886 Color: 0

Bin 4229: 2422 of cap free
Amount of items: 2
Items: 
Size: 754990 Color: 0
Size: 242589 Color: 4

Bin 4230: 2439 of cap free
Amount of items: 2
Items: 
Size: 727888 Color: 2
Size: 269674 Color: 3

Bin 4231: 2454 of cap free
Amount of items: 2
Items: 
Size: 671556 Color: 1
Size: 325991 Color: 4

Bin 4232: 2455 of cap free
Amount of items: 2
Items: 
Size: 781082 Color: 4
Size: 216464 Color: 0

Bin 4233: 2475 of cap free
Amount of items: 2
Items: 
Size: 635694 Color: 0
Size: 361832 Color: 1

Bin 4234: 2494 of cap free
Amount of items: 2
Items: 
Size: 552722 Color: 1
Size: 444785 Color: 4

Bin 4235: 2514 of cap free
Amount of items: 2
Items: 
Size: 703729 Color: 3
Size: 293758 Color: 0

Bin 4236: 2518 of cap free
Amount of items: 2
Items: 
Size: 533544 Color: 3
Size: 463939 Color: 4

Bin 4237: 2542 of cap free
Amount of items: 2
Items: 
Size: 731661 Color: 2
Size: 265798 Color: 0

Bin 4238: 2548 of cap free
Amount of items: 2
Items: 
Size: 711058 Color: 2
Size: 286395 Color: 4

Bin 4239: 2569 of cap free
Amount of items: 2
Items: 
Size: 600375 Color: 0
Size: 397057 Color: 2

Bin 4240: 2576 of cap free
Amount of items: 2
Items: 
Size: 533496 Color: 3
Size: 463929 Color: 2

Bin 4241: 2605 of cap free
Amount of items: 2
Items: 
Size: 671512 Color: 1
Size: 325884 Color: 3

Bin 4242: 2608 of cap free
Amount of items: 2
Items: 
Size: 574309 Color: 3
Size: 423084 Color: 1

Bin 4243: 2610 of cap free
Amount of items: 2
Items: 
Size: 697737 Color: 3
Size: 299654 Color: 1

Bin 4244: 2612 of cap free
Amount of items: 2
Items: 
Size: 655687 Color: 1
Size: 341702 Color: 3

Bin 4245: 2612 of cap free
Amount of items: 2
Items: 
Size: 589634 Color: 2
Size: 407755 Color: 4

Bin 4246: 2630 of cap free
Amount of items: 2
Items: 
Size: 635574 Color: 1
Size: 361797 Color: 0

Bin 4247: 2647 of cap free
Amount of items: 2
Items: 
Size: 533485 Color: 3
Size: 463869 Color: 1

Bin 4248: 2649 of cap free
Amount of items: 2
Items: 
Size: 711973 Color: 4
Size: 285379 Color: 0

Bin 4249: 2655 of cap free
Amount of items: 2
Items: 
Size: 498722 Color: 4
Size: 498624 Color: 0

Bin 4250: 2663 of cap free
Amount of items: 2
Items: 
Size: 533462 Color: 2
Size: 463876 Color: 3

Bin 4251: 2666 of cap free
Amount of items: 2
Items: 
Size: 502797 Color: 1
Size: 494538 Color: 0

Bin 4252: 2670 of cap free
Amount of items: 2
Items: 
Size: 764852 Color: 0
Size: 232479 Color: 2

Bin 4253: 2686 of cap free
Amount of items: 2
Items: 
Size: 655630 Color: 1
Size: 341685 Color: 2

Bin 4254: 2700 of cap free
Amount of items: 2
Items: 
Size: 635573 Color: 2
Size: 361728 Color: 1

Bin 4255: 2702 of cap free
Amount of items: 2
Items: 
Size: 502789 Color: 0
Size: 494510 Color: 4

Bin 4256: 2708 of cap free
Amount of items: 2
Items: 
Size: 589614 Color: 4
Size: 407679 Color: 1

Bin 4257: 2711 of cap free
Amount of items: 2
Items: 
Size: 708268 Color: 1
Size: 289022 Color: 4

Bin 4258: 2718 of cap free
Amount of items: 2
Items: 
Size: 754750 Color: 0
Size: 242533 Color: 4

Bin 4259: 2731 of cap free
Amount of items: 2
Items: 
Size: 617572 Color: 0
Size: 379698 Color: 4

Bin 4260: 2751 of cap free
Amount of items: 2
Items: 
Size: 548030 Color: 3
Size: 449220 Color: 4

Bin 4261: 2759 of cap free
Amount of items: 2
Items: 
Size: 720610 Color: 0
Size: 276632 Color: 1

Bin 4262: 2764 of cap free
Amount of items: 2
Items: 
Size: 745231 Color: 3
Size: 252006 Color: 2

Bin 4263: 2769 of cap free
Amount of items: 2
Items: 
Size: 589555 Color: 1
Size: 407677 Color: 4

Bin 4264: 2770 of cap free
Amount of items: 2
Items: 
Size: 577867 Color: 2
Size: 419364 Color: 1

Bin 4265: 2772 of cap free
Amount of items: 2
Items: 
Size: 514120 Color: 0
Size: 483109 Color: 1

Bin 4266: 2779 of cap free
Amount of items: 2
Items: 
Size: 580312 Color: 1
Size: 416910 Color: 3

Bin 4267: 2784 of cap free
Amount of items: 2
Items: 
Size: 514167 Color: 2
Size: 483050 Color: 0

Bin 4268: 2785 of cap free
Amount of items: 2
Items: 
Size: 708241 Color: 0
Size: 288975 Color: 4

Bin 4269: 2815 of cap free
Amount of items: 2
Items: 
Size: 581011 Color: 3
Size: 416175 Color: 0

Bin 4270: 2824 of cap free
Amount of items: 2
Items: 
Size: 764648 Color: 1
Size: 232529 Color: 0

Bin 4271: 2830 of cap free
Amount of items: 2
Items: 
Size: 631393 Color: 1
Size: 365778 Color: 4

Bin 4272: 2848 of cap free
Amount of items: 2
Items: 
Size: 659368 Color: 2
Size: 337785 Color: 0

Bin 4273: 2856 of cap free
Amount of items: 2
Items: 
Size: 600299 Color: 3
Size: 396846 Color: 1

Bin 4274: 2895 of cap free
Amount of items: 2
Items: 
Size: 679410 Color: 0
Size: 317696 Color: 3

Bin 4275: 2908 of cap free
Amount of items: 2
Items: 
Size: 764679 Color: 0
Size: 232414 Color: 2

Bin 4276: 2917 of cap free
Amount of items: 2
Items: 
Size: 502766 Color: 1
Size: 494318 Color: 4

Bin 4277: 2925 of cap free
Amount of items: 2
Items: 
Size: 552344 Color: 0
Size: 444732 Color: 4

Bin 4278: 2934 of cap free
Amount of items: 2
Items: 
Size: 530220 Color: 3
Size: 466847 Color: 4

Bin 4279: 2935 of cap free
Amount of items: 2
Items: 
Size: 608581 Color: 1
Size: 388485 Color: 0

Bin 4280: 2952 of cap free
Amount of items: 2
Items: 
Size: 548239 Color: 4
Size: 448810 Color: 3

Bin 4281: 2969 of cap free
Amount of items: 2
Items: 
Size: 708194 Color: 3
Size: 288838 Color: 2

Bin 4282: 2983 of cap free
Amount of items: 2
Items: 
Size: 764635 Color: 1
Size: 232383 Color: 3

Bin 4283: 3001 of cap free
Amount of items: 2
Items: 
Size: 708175 Color: 1
Size: 288825 Color: 2

Bin 4284: 3012 of cap free
Amount of items: 2
Items: 
Size: 711947 Color: 4
Size: 285042 Color: 2

Bin 4285: 3040 of cap free
Amount of items: 2
Items: 
Size: 799150 Color: 1
Size: 197811 Color: 2

Bin 4286: 3043 of cap free
Amount of items: 2
Items: 
Size: 708142 Color: 4
Size: 288816 Color: 2

Bin 4287: 3046 of cap free
Amount of items: 2
Items: 
Size: 530116 Color: 0
Size: 466839 Color: 4

Bin 4288: 3103 of cap free
Amount of items: 2
Items: 
Size: 574200 Color: 1
Size: 422698 Color: 3

Bin 4289: 3110 of cap free
Amount of items: 2
Items: 
Size: 720380 Color: 0
Size: 276511 Color: 3

Bin 4290: 3113 of cap free
Amount of items: 2
Items: 
Size: 671500 Color: 0
Size: 325388 Color: 1

Bin 4291: 3113 of cap free
Amount of items: 2
Items: 
Size: 599852 Color: 4
Size: 397036 Color: 3

Bin 4292: 3123 of cap free
Amount of items: 2
Items: 
Size: 751064 Color: 3
Size: 245814 Color: 4

Bin 4293: 3125 of cap free
Amount of items: 2
Items: 
Size: 547684 Color: 1
Size: 449192 Color: 4

Bin 4294: 3220 of cap free
Amount of items: 2
Items: 
Size: 697135 Color: 4
Size: 299646 Color: 1

Bin 4295: 3294 of cap free
Amount of items: 2
Items: 
Size: 692257 Color: 1
Size: 304450 Color: 2

Bin 4296: 3299 of cap free
Amount of items: 2
Items: 
Size: 502663 Color: 1
Size: 494039 Color: 4

Bin 4297: 3318 of cap free
Amount of items: 2
Items: 
Size: 498582 Color: 1
Size: 498101 Color: 2

Bin 4298: 3372 of cap free
Amount of items: 2
Items: 
Size: 599795 Color: 0
Size: 396834 Color: 2

Bin 4299: 3405 of cap free
Amount of items: 2
Items: 
Size: 502636 Color: 1
Size: 493960 Color: 3

Bin 4300: 3463 of cap free
Amount of items: 2
Items: 
Size: 547656 Color: 3
Size: 448882 Color: 0

Bin 4301: 3468 of cap free
Amount of items: 2
Items: 
Size: 589541 Color: 1
Size: 406992 Color: 0

Bin 4302: 3482 of cap free
Amount of items: 2
Items: 
Size: 599738 Color: 3
Size: 396781 Color: 4

Bin 4303: 3523 of cap free
Amount of items: 2
Items: 
Size: 514031 Color: 4
Size: 482447 Color: 1

Bin 4304: 3531 of cap free
Amount of items: 2
Items: 
Size: 574080 Color: 1
Size: 422390 Color: 3

Bin 4305: 3551 of cap free
Amount of items: 2
Items: 
Size: 502615 Color: 0
Size: 493835 Color: 1

Bin 4306: 3576 of cap free
Amount of items: 2
Items: 
Size: 679182 Color: 1
Size: 317243 Color: 3

Bin 4307: 3586 of cap free
Amount of items: 2
Items: 
Size: 764049 Color: 2
Size: 232366 Color: 4

Bin 4308: 3622 of cap free
Amount of items: 2
Items: 
Size: 727441 Color: 3
Size: 268938 Color: 4

Bin 4309: 3626 of cap free
Amount of items: 2
Items: 
Size: 699910 Color: 1
Size: 296465 Color: 0

Bin 4310: 3646 of cap free
Amount of items: 2
Items: 
Size: 711745 Color: 4
Size: 284610 Color: 0

Bin 4311: 3683 of cap free
Amount of items: 2
Items: 
Size: 599766 Color: 4
Size: 396552 Color: 0

Bin 4312: 3697 of cap free
Amount of items: 2
Items: 
Size: 547632 Color: 1
Size: 448672 Color: 3

Bin 4313: 3745 of cap free
Amount of items: 2
Items: 
Size: 502391 Color: 4
Size: 493865 Color: 0

Bin 4314: 3775 of cap free
Amount of items: 2
Items: 
Size: 670949 Color: 1
Size: 325277 Color: 3

Bin 4315: 3815 of cap free
Amount of items: 2
Items: 
Size: 617413 Color: 4
Size: 378773 Color: 2

Bin 4316: 3853 of cap free
Amount of items: 2
Items: 
Size: 502408 Color: 2
Size: 493740 Color: 4

Bin 4317: 3937 of cap free
Amount of items: 2
Items: 
Size: 547542 Color: 1
Size: 448522 Color: 3

Bin 4318: 3956 of cap free
Amount of items: 2
Items: 
Size: 617417 Color: 2
Size: 378628 Color: 1

Bin 4319: 3978 of cap free
Amount of items: 2
Items: 
Size: 691974 Color: 4
Size: 304049 Color: 2

Bin 4320: 3997 of cap free
Amount of items: 2
Items: 
Size: 513868 Color: 3
Size: 482136 Color: 4

Bin 4321: 4013 of cap free
Amount of items: 2
Items: 
Size: 655061 Color: 0
Size: 340927 Color: 4

Bin 4322: 4058 of cap free
Amount of items: 2
Items: 
Size: 670831 Color: 0
Size: 325112 Color: 2

Bin 4323: 4126 of cap free
Amount of items: 2
Items: 
Size: 692387 Color: 2
Size: 303488 Color: 3

Bin 4324: 4228 of cap free
Amount of items: 2
Items: 
Size: 588929 Color: 2
Size: 406844 Color: 4

Bin 4325: 4280 of cap free
Amount of items: 2
Items: 
Size: 670783 Color: 2
Size: 324938 Color: 3

Bin 4326: 4319 of cap free
Amount of items: 2
Items: 
Size: 552702 Color: 4
Size: 442980 Color: 3

Bin 4327: 4343 of cap free
Amount of items: 2
Items: 
Size: 547464 Color: 3
Size: 448194 Color: 0

Bin 4328: 4364 of cap free
Amount of items: 2
Items: 
Size: 763307 Color: 2
Size: 232330 Color: 4

Bin 4329: 4388 of cap free
Amount of items: 2
Items: 
Size: 552676 Color: 4
Size: 442937 Color: 1

Bin 4330: 4521 of cap free
Amount of items: 2
Items: 
Size: 763301 Color: 3
Size: 232179 Color: 0

Bin 4331: 4570 of cap free
Amount of items: 2
Items: 
Size: 718923 Color: 0
Size: 276508 Color: 3

Bin 4332: 4593 of cap free
Amount of items: 2
Items: 
Size: 670767 Color: 0
Size: 324641 Color: 1

Bin 4333: 4609 of cap free
Amount of items: 2
Items: 
Size: 546953 Color: 2
Size: 448439 Color: 3

Bin 4334: 4611 of cap free
Amount of items: 2
Items: 
Size: 647308 Color: 1
Size: 348082 Color: 4

Bin 4335: 4627 of cap free
Amount of items: 2
Items: 
Size: 763296 Color: 3
Size: 232078 Color: 0

Bin 4336: 4664 of cap free
Amount of items: 2
Items: 
Size: 725711 Color: 2
Size: 269626 Color: 3

Bin 4337: 4698 of cap free
Amount of items: 2
Items: 
Size: 533424 Color: 0
Size: 461879 Color: 3

Bin 4338: 4753 of cap free
Amount of items: 2
Items: 
Size: 513989 Color: 4
Size: 481259 Color: 1

Bin 4339: 4775 of cap free
Amount of items: 2
Items: 
Size: 533404 Color: 4
Size: 461822 Color: 0

Bin 4340: 4887 of cap free
Amount of items: 2
Items: 
Size: 533416 Color: 0
Size: 461698 Color: 4

Bin 4341: 4923 of cap free
Amount of items: 2
Items: 
Size: 677927 Color: 3
Size: 317151 Color: 1

Bin 4342: 4933 of cap free
Amount of items: 2
Items: 
Size: 696699 Color: 2
Size: 298369 Color: 1

Bin 4343: 5008 of cap free
Amount of items: 3
Items: 
Size: 736793 Color: 1
Size: 129917 Color: 0
Size: 128283 Color: 0

Bin 4344: 5019 of cap free
Amount of items: 2
Items: 
Size: 725705 Color: 2
Size: 269277 Color: 3

Bin 4345: 5054 of cap free
Amount of items: 2
Items: 
Size: 547101 Color: 3
Size: 447846 Color: 1

Bin 4346: 5075 of cap free
Amount of items: 2
Items: 
Size: 763148 Color: 1
Size: 231778 Color: 4

Bin 4347: 5109 of cap free
Amount of items: 2
Items: 
Size: 662263 Color: 1
Size: 332629 Color: 3

Bin 4348: 5139 of cap free
Amount of items: 2
Items: 
Size: 547038 Color: 3
Size: 447824 Color: 0

Bin 4349: 5153 of cap free
Amount of items: 2
Items: 
Size: 799102 Color: 2
Size: 195746 Color: 4

Bin 4350: 5219 of cap free
Amount of items: 2
Items: 
Size: 763066 Color: 3
Size: 231716 Color: 2

Bin 4351: 5249 of cap free
Amount of items: 2
Items: 
Size: 546843 Color: 2
Size: 447909 Color: 3

Bin 4352: 5332 of cap free
Amount of items: 2
Items: 
Size: 502270 Color: 1
Size: 492399 Color: 4

Bin 4353: 5397 of cap free
Amount of items: 2
Items: 
Size: 502206 Color: 2
Size: 492398 Color: 4

Bin 4354: 5430 of cap free
Amount of items: 2
Items: 
Size: 696655 Color: 4
Size: 297916 Color: 1

Bin 4355: 5566 of cap free
Amount of items: 2
Items: 
Size: 502109 Color: 2
Size: 492326 Color: 0

Bin 4356: 5818 of cap free
Amount of items: 2
Items: 
Size: 615774 Color: 1
Size: 378409 Color: 3

Bin 4357: 5826 of cap free
Amount of items: 2
Items: 
Size: 580101 Color: 1
Size: 414074 Color: 0

Bin 4358: 5835 of cap free
Amount of items: 2
Items: 
Size: 677891 Color: 2
Size: 316275 Color: 3

Bin 4359: 5854 of cap free
Amount of items: 2
Items: 
Size: 513866 Color: 2
Size: 480281 Color: 0

Bin 4360: 5933 of cap free
Amount of items: 2
Items: 
Size: 513798 Color: 2
Size: 480270 Color: 4

Bin 4361: 5963 of cap free
Amount of items: 2
Items: 
Size: 623993 Color: 0
Size: 370045 Color: 1

Bin 4362: 6105 of cap free
Amount of items: 2
Items: 
Size: 615750 Color: 3
Size: 378146 Color: 0

Bin 4363: 6367 of cap free
Amount of items: 2
Items: 
Size: 513623 Color: 2
Size: 480011 Color: 4

Bin 4364: 6480 of cap free
Amount of items: 2
Items: 
Size: 513610 Color: 2
Size: 479911 Color: 0

Bin 4365: 6609 of cap free
Amount of items: 2
Items: 
Size: 647246 Color: 1
Size: 346146 Color: 4

Bin 4366: 6776 of cap free
Amount of items: 2
Items: 
Size: 513559 Color: 0
Size: 479666 Color: 3

Bin 4367: 6838 of cap free
Amount of items: 2
Items: 
Size: 564422 Color: 0
Size: 428741 Color: 4

Bin 4368: 6975 of cap free
Amount of items: 2
Items: 
Size: 513540 Color: 0
Size: 479486 Color: 2

Bin 4369: 7145 of cap free
Amount of items: 2
Items: 
Size: 638870 Color: 0
Size: 353986 Color: 2

Bin 4370: 7176 of cap free
Amount of items: 2
Items: 
Size: 513375 Color: 3
Size: 479450 Color: 1

Bin 4371: 7265 of cap free
Amount of items: 2
Items: 
Size: 761823 Color: 1
Size: 230913 Color: 0

Bin 4372: 7431 of cap free
Amount of items: 2
Items: 
Size: 589500 Color: 4
Size: 403070 Color: 1

Bin 4373: 7612 of cap free
Amount of items: 2
Items: 
Size: 552340 Color: 0
Size: 440049 Color: 1

Bin 4374: 7632 of cap free
Amount of items: 2
Items: 
Size: 564088 Color: 3
Size: 428281 Color: 4

Bin 4375: 7637 of cap free
Amount of items: 2
Items: 
Size: 761543 Color: 2
Size: 230821 Color: 3

Bin 4376: 7863 of cap free
Amount of items: 2
Items: 
Size: 614159 Color: 3
Size: 377979 Color: 0

Bin 4377: 7969 of cap free
Amount of items: 2
Items: 
Size: 706370 Color: 3
Size: 285662 Color: 4

Bin 4378: 8030 of cap free
Amount of items: 2
Items: 
Size: 682855 Color: 3
Size: 309116 Color: 4

Bin 4379: 8058 of cap free
Amount of items: 2
Items: 
Size: 513387 Color: 0
Size: 478556 Color: 3

Bin 4380: 8069 of cap free
Amount of items: 2
Items: 
Size: 761186 Color: 2
Size: 230746 Color: 4

Bin 4381: 8134 of cap free
Amount of items: 2
Items: 
Size: 530198 Color: 4
Size: 461669 Color: 1

Bin 4382: 8268 of cap free
Amount of items: 2
Items: 
Size: 530073 Color: 4
Size: 461660 Color: 1

Bin 4383: 8426 of cap free
Amount of items: 2
Items: 
Size: 513375 Color: 2
Size: 478200 Color: 4

Bin 4384: 8503 of cap free
Amount of items: 2
Items: 
Size: 589466 Color: 4
Size: 402032 Color: 3

Bin 4385: 8553 of cap free
Amount of items: 2
Items: 
Size: 761184 Color: 0
Size: 230264 Color: 4

Bin 4386: 8587 of cap free
Amount of items: 2
Items: 
Size: 646704 Color: 4
Size: 344710 Color: 0

Bin 4387: 8638 of cap free
Amount of items: 2
Items: 
Size: 742114 Color: 0
Size: 249249 Color: 2

Bin 4388: 9035 of cap free
Amount of items: 2
Items: 
Size: 588842 Color: 0
Size: 402124 Color: 4

Bin 4389: 9047 of cap free
Amount of items: 2
Items: 
Size: 588793 Color: 3
Size: 402161 Color: 0

Bin 4390: 9315 of cap free
Amount of items: 2
Items: 
Size: 588783 Color: 2
Size: 401903 Color: 3

Bin 4391: 9404 of cap free
Amount of items: 2
Items: 
Size: 760157 Color: 3
Size: 230440 Color: 0

Bin 4392: 9506 of cap free
Amount of items: 2
Items: 
Size: 588691 Color: 3
Size: 401804 Color: 1

Bin 4393: 9672 of cap free
Amount of items: 2
Items: 
Size: 697049 Color: 1
Size: 293280 Color: 3

Bin 4394: 10025 of cap free
Amount of items: 2
Items: 
Size: 588628 Color: 0
Size: 401348 Color: 1

Bin 4395: 10078 of cap free
Amount of items: 2
Items: 
Size: 724958 Color: 1
Size: 264965 Color: 2

Bin 4396: 10139 of cap free
Amount of items: 2
Items: 
Size: 588584 Color: 3
Size: 401278 Color: 4

Bin 4397: 10218 of cap free
Amount of items: 2
Items: 
Size: 724952 Color: 4
Size: 264831 Color: 2

Bin 4398: 10497 of cap free
Amount of items: 2
Items: 
Size: 588388 Color: 1
Size: 401116 Color: 3

Bin 4399: 10756 of cap free
Amount of items: 2
Items: 
Size: 561844 Color: 4
Size: 427401 Color: 0

Bin 4400: 10873 of cap free
Amount of items: 2
Items: 
Size: 724297 Color: 3
Size: 264831 Color: 1

Bin 4401: 11064 of cap free
Amount of items: 2
Items: 
Size: 527388 Color: 2
Size: 461549 Color: 3

Bin 4402: 11072 of cap free
Amount of items: 2
Items: 
Size: 742350 Color: 2
Size: 246579 Color: 3

Bin 4403: 11098 of cap free
Amount of items: 2
Items: 
Size: 587855 Color: 4
Size: 401048 Color: 1

Bin 4404: 11474 of cap free
Amount of items: 2
Items: 
Size: 527002 Color: 3
Size: 461525 Color: 1

Bin 4405: 11928 of cap free
Amount of items: 2
Items: 
Size: 526609 Color: 0
Size: 461464 Color: 3

Bin 4406: 12244 of cap free
Amount of items: 2
Items: 
Size: 799038 Color: 1
Size: 188719 Color: 3

Bin 4407: 13076 of cap free
Amount of items: 2
Items: 
Size: 526596 Color: 4
Size: 460329 Color: 3

Bin 4408: 13108 of cap free
Amount of items: 2
Items: 
Size: 546898 Color: 3
Size: 439995 Color: 4

Bin 4409: 13190 of cap free
Amount of items: 2
Items: 
Size: 546829 Color: 4
Size: 439982 Color: 0

Bin 4410: 13309 of cap free
Amount of items: 2
Items: 
Size: 546830 Color: 1
Size: 439862 Color: 4

Bin 4411: 13393 of cap free
Amount of items: 2
Items: 
Size: 546759 Color: 2
Size: 439849 Color: 1

Bin 4412: 13570 of cap free
Amount of items: 2
Items: 
Size: 526594 Color: 0
Size: 459837 Color: 3

Bin 4413: 13935 of cap free
Amount of items: 2
Items: 
Size: 798691 Color: 1
Size: 187375 Color: 2

Bin 4414: 14084 of cap free
Amount of items: 2
Items: 
Size: 798609 Color: 0
Size: 187308 Color: 1

Bin 4415: 15545 of cap free
Amount of items: 2
Items: 
Size: 492281 Color: 3
Size: 492175 Color: 2

Bin 4416: 16250 of cap free
Amount of items: 2
Items: 
Size: 587408 Color: 3
Size: 396343 Color: 0

Bin 4417: 16354 of cap free
Amount of items: 2
Items: 
Size: 491830 Color: 4
Size: 491817 Color: 0

Bin 4418: 16458 of cap free
Amount of items: 2
Items: 
Size: 642806 Color: 2
Size: 340737 Color: 3

Bin 4419: 16559 of cap free
Amount of items: 2
Items: 
Size: 491798 Color: 4
Size: 491644 Color: 3

Bin 4420: 16667 of cap free
Amount of items: 2
Items: 
Size: 526458 Color: 4
Size: 456876 Color: 2

Bin 4421: 16712 of cap free
Amount of items: 2
Items: 
Size: 526407 Color: 3
Size: 456882 Color: 4

Bin 4422: 16729 of cap free
Amount of items: 2
Items: 
Size: 526436 Color: 4
Size: 456836 Color: 1

Bin 4423: 16972 of cap free
Amount of items: 2
Items: 
Size: 588042 Color: 1
Size: 394987 Color: 3

Bin 4424: 17917 of cap free
Amount of items: 2
Items: 
Size: 525688 Color: 0
Size: 456396 Color: 3

Bin 4425: 17988 of cap free
Amount of items: 2
Items: 
Size: 525862 Color: 3
Size: 456151 Color: 0

Bin 4426: 17990 of cap free
Amount of items: 2
Items: 
Size: 525683 Color: 2
Size: 456328 Color: 3

Bin 4427: 18275 of cap free
Amount of items: 2
Items: 
Size: 491634 Color: 3
Size: 490092 Color: 0

Bin 4428: 19057 of cap free
Amount of items: 2
Items: 
Size: 696515 Color: 2
Size: 284429 Color: 4

Bin 4429: 19171 of cap free
Amount of items: 2
Items: 
Size: 587391 Color: 2
Size: 393439 Color: 4

Bin 4430: 19327 of cap free
Amount of items: 2
Items: 
Size: 587246 Color: 1
Size: 393428 Color: 0

Bin 4431: 19672 of cap free
Amount of items: 2
Items: 
Size: 524255 Color: 0
Size: 456074 Color: 2

Bin 4432: 19689 of cap free
Amount of items: 2
Items: 
Size: 524167 Color: 4
Size: 456145 Color: 0

Bin 4433: 19718 of cap free
Amount of items: 2
Items: 
Size: 524223 Color: 2
Size: 456060 Color: 4

Bin 4434: 19877 of cap free
Amount of items: 2
Items: 
Size: 587027 Color: 3
Size: 393097 Color: 0

Bin 4435: 20034 of cap free
Amount of items: 2
Items: 
Size: 586896 Color: 0
Size: 393071 Color: 2

Bin 4436: 20421 of cap free
Amount of items: 2
Items: 
Size: 586770 Color: 3
Size: 392810 Color: 4

Bin 4437: 20476 of cap free
Amount of items: 2
Items: 
Size: 489841 Color: 3
Size: 489684 Color: 2

Bin 4438: 20757 of cap free
Amount of items: 2
Items: 
Size: 489731 Color: 3
Size: 489513 Color: 1

Bin 4439: 20812 of cap free
Amount of items: 2
Items: 
Size: 798601 Color: 1
Size: 180588 Color: 4

Bin 4440: 21039 of cap free
Amount of items: 2
Items: 
Size: 523092 Color: 3
Size: 455870 Color: 4

Bin 4441: 21632 of cap free
Amount of items: 2
Items: 
Size: 489417 Color: 2
Size: 488952 Color: 3

Bin 4442: 22110 of cap free
Amount of items: 2
Items: 
Size: 798478 Color: 3
Size: 179413 Color: 4

Bin 4443: 22395 of cap free
Amount of items: 2
Items: 
Size: 488882 Color: 4
Size: 488724 Color: 1

Bin 4444: 22423 of cap free
Amount of items: 2
Items: 
Size: 488870 Color: 4
Size: 488708 Color: 0

Bin 4445: 23439 of cap free
Amount of items: 2
Items: 
Size: 523012 Color: 4
Size: 453550 Color: 1

Bin 4446: 23616 of cap free
Amount of items: 2
Items: 
Size: 523062 Color: 1
Size: 453323 Color: 3

Bin 4447: 23760 of cap free
Amount of items: 2
Items: 
Size: 635535 Color: 0
Size: 340706 Color: 3

Bin 4448: 23884 of cap free
Amount of items: 2
Items: 
Size: 522857 Color: 2
Size: 453260 Color: 1

Bin 4449: 23961 of cap free
Amount of items: 2
Items: 
Size: 635354 Color: 2
Size: 340686 Color: 1

Bin 4450: 24079 of cap free
Amount of items: 2
Items: 
Size: 488269 Color: 2
Size: 487653 Color: 1

Bin 4451: 24139 of cap free
Amount of items: 2
Items: 
Size: 488265 Color: 2
Size: 487597 Color: 4

Bin 4452: 24159 of cap free
Amount of items: 2
Items: 
Size: 522560 Color: 0
Size: 453282 Color: 2

Bin 4453: 24198 of cap free
Amount of items: 2
Items: 
Size: 522544 Color: 4
Size: 453259 Color: 3

Bin 4454: 24204 of cap free
Amount of items: 2
Items: 
Size: 798265 Color: 3
Size: 177532 Color: 1

Bin 4455: 24857 of cap free
Amount of items: 2
Items: 
Size: 586814 Color: 0
Size: 388330 Color: 3

Bin 4456: 24879 of cap free
Amount of items: 2
Items: 
Size: 798236 Color: 0
Size: 176886 Color: 4

Bin 4457: 25106 of cap free
Amount of items: 2
Items: 
Size: 487451 Color: 1
Size: 487444 Color: 0

Bin 4458: 26546 of cap free
Amount of items: 2
Items: 
Size: 546743 Color: 0
Size: 426712 Color: 4

Bin 4459: 32434 of cap free
Amount of items: 2
Items: 
Size: 798076 Color: 1
Size: 169491 Color: 2

Bin 4460: 34920 of cap free
Amount of items: 2
Items: 
Size: 577728 Color: 1
Size: 387353 Color: 0

Bin 4461: 37611 of cap free
Amount of items: 2
Items: 
Size: 584132 Color: 0
Size: 378258 Color: 3

Bin 4462: 38109 of cap free
Amount of items: 2
Items: 
Size: 522453 Color: 4
Size: 439439 Color: 0

Bin 4463: 40030 of cap free
Amount of items: 2
Items: 
Size: 574120 Color: 3
Size: 385851 Color: 0

Bin 4464: 48055 of cap free
Amount of items: 2
Items: 
Size: 622658 Color: 2
Size: 329288 Color: 1

Bin 4465: 48473 of cap free
Amount of items: 2
Items: 
Size: 573904 Color: 4
Size: 377624 Color: 3

Bin 4466: 51159 of cap free
Amount of items: 2
Items: 
Size: 522426 Color: 4
Size: 426416 Color: 2

Bin 4467: 51585 of cap free
Amount of items: 2
Items: 
Size: 522041 Color: 1
Size: 426375 Color: 3

Bin 4468: 51767 of cap free
Amount of items: 2
Items: 
Size: 521992 Color: 2
Size: 426242 Color: 4

Bin 4469: 52263 of cap free
Amount of items: 2
Items: 
Size: 797566 Color: 2
Size: 150172 Color: 1

Bin 4470: 56740 of cap free
Amount of items: 2
Items: 
Size: 573895 Color: 4
Size: 369366 Color: 2

Bin 4471: 202466 of cap free
Amount of items: 1
Items: 
Size: 797535 Color: 0

Bin 4472: 202491 of cap free
Amount of items: 1
Items: 
Size: 797510 Color: 1

Bin 4473: 202804 of cap free
Amount of items: 1
Items: 
Size: 797197 Color: 3

Bin 4474: 202966 of cap free
Amount of items: 1
Items: 
Size: 797035 Color: 4

Bin 4475: 203119 of cap free
Amount of items: 1
Items: 
Size: 796882 Color: 3

Bin 4476: 203169 of cap free
Amount of items: 1
Items: 
Size: 796832 Color: 1

Bin 4477: 203327 of cap free
Amount of items: 1
Items: 
Size: 796674 Color: 0

Bin 4478: 203349 of cap free
Amount of items: 1
Items: 
Size: 796652 Color: 3

Bin 4479: 203357 of cap free
Amount of items: 1
Items: 
Size: 796644 Color: 0

Bin 4480: 203703 of cap free
Amount of items: 1
Items: 
Size: 796298 Color: 0

Bin 4481: 204051 of cap free
Amount of items: 1
Items: 
Size: 795950 Color: 3

Bin 4482: 204071 of cap free
Amount of items: 1
Items: 
Size: 795930 Color: 2

Bin 4483: 205466 of cap free
Amount of items: 1
Items: 
Size: 794535 Color: 4

Bin 4484: 205468 of cap free
Amount of items: 1
Items: 
Size: 794533 Color: 2

Bin 4485: 205583 of cap free
Amount of items: 1
Items: 
Size: 794418 Color: 3

Bin 4486: 205668 of cap free
Amount of items: 1
Items: 
Size: 794333 Color: 3

Bin 4487: 205890 of cap free
Amount of items: 1
Items: 
Size: 794111 Color: 0

Bin 4488: 206015 of cap free
Amount of items: 1
Items: 
Size: 793986 Color: 1

Bin 4489: 206171 of cap free
Amount of items: 1
Items: 
Size: 793830 Color: 0

Bin 4490: 207716 of cap free
Amount of items: 1
Items: 
Size: 792285 Color: 3

Bin 4491: 207820 of cap free
Amount of items: 1
Items: 
Size: 792181 Color: 4

Bin 4492: 208009 of cap free
Amount of items: 1
Items: 
Size: 791992 Color: 3

Bin 4493: 208142 of cap free
Amount of items: 1
Items: 
Size: 791859 Color: 3

Bin 4494: 208152 of cap free
Amount of items: 1
Items: 
Size: 791849 Color: 0

Bin 4495: 208176 of cap free
Amount of items: 1
Items: 
Size: 791825 Color: 4

Bin 4496: 208204 of cap free
Amount of items: 1
Items: 
Size: 791797 Color: 3

Bin 4497: 208224 of cap free
Amount of items: 1
Items: 
Size: 791777 Color: 0

Bin 4498: 209286 of cap free
Amount of items: 1
Items: 
Size: 790715 Color: 2

Bin 4499: 209665 of cap free
Amount of items: 1
Items: 
Size: 790336 Color: 2

Bin 4500: 209694 of cap free
Amount of items: 1
Items: 
Size: 790307 Color: 1

Bin 4501: 210374 of cap free
Amount of items: 1
Items: 
Size: 789627 Color: 1

Bin 4502: 210924 of cap free
Amount of items: 1
Items: 
Size: 789077 Color: 1

Bin 4503: 211664 of cap free
Amount of items: 1
Items: 
Size: 788337 Color: 1

Bin 4504: 214021 of cap free
Amount of items: 1
Items: 
Size: 785980 Color: 0

Bin 4505: 214081 of cap free
Amount of items: 1
Items: 
Size: 785920 Color: 2

Bin 4506: 214977 of cap free
Amount of items: 1
Items: 
Size: 785024 Color: 1

Bin 4507: 216201 of cap free
Amount of items: 1
Items: 
Size: 783800 Color: 0

Bin 4508: 219076 of cap free
Amount of items: 1
Items: 
Size: 780925 Color: 3

Bin 4509: 219139 of cap free
Amount of items: 1
Items: 
Size: 780862 Color: 1

Bin 4510: 219149 of cap free
Amount of items: 1
Items: 
Size: 780852 Color: 1

Bin 4511: 219212 of cap free
Amount of items: 1
Items: 
Size: 780789 Color: 4

Bin 4512: 222769 of cap free
Amount of items: 1
Items: 
Size: 777232 Color: 0

Bin 4513: 222784 of cap free
Amount of items: 1
Items: 
Size: 777217 Color: 0

Bin 4514: 224434 of cap free
Amount of items: 1
Items: 
Size: 775567 Color: 1

Bin 4515: 224905 of cap free
Amount of items: 1
Items: 
Size: 775096 Color: 2

Bin 4516: 225210 of cap free
Amount of items: 1
Items: 
Size: 774791 Color: 3

Bin 4517: 226058 of cap free
Amount of items: 1
Items: 
Size: 773943 Color: 1

Bin 4518: 226326 of cap free
Amount of items: 1
Items: 
Size: 773675 Color: 2

Bin 4519: 226663 of cap free
Amount of items: 1
Items: 
Size: 773338 Color: 4

Bin 4520: 227048 of cap free
Amount of items: 1
Items: 
Size: 772953 Color: 0

Bin 4521: 227220 of cap free
Amount of items: 1
Items: 
Size: 772781 Color: 3

Bin 4522: 227357 of cap free
Amount of items: 1
Items: 
Size: 772644 Color: 4

Bin 4523: 227746 of cap free
Amount of items: 1
Items: 
Size: 772255 Color: 4

Bin 4524: 227824 of cap free
Amount of items: 1
Items: 
Size: 772177 Color: 4

Bin 4525: 239861 of cap free
Amount of items: 1
Items: 
Size: 760140 Color: 1

Bin 4526: 240204 of cap free
Amount of items: 1
Items: 
Size: 759797 Color: 1

Bin 4527: 240826 of cap free
Amount of items: 1
Items: 
Size: 759175 Color: 2

Bin 4528: 240936 of cap free
Amount of items: 1
Items: 
Size: 759065 Color: 3

Bin 4529: 241802 of cap free
Amount of items: 1
Items: 
Size: 758199 Color: 4

Bin 4530: 258162 of cap free
Amount of items: 1
Items: 
Size: 741839 Color: 1

Total size: 4513873771
Total free space: 16130759

