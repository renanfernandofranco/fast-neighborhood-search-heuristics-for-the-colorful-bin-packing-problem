Capicity Bin: 8000
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 17
Items: 
Size: 664 Color: 12
Size: 664 Color: 2
Size: 632 Color: 19
Size: 536 Color: 18
Size: 536 Color: 4
Size: 512 Color: 13
Size: 508 Color: 14
Size: 500 Color: 9
Size: 472 Color: 16
Size: 448 Color: 9
Size: 448 Color: 0
Size: 440 Color: 14
Size: 432 Color: 19
Size: 416 Color: 10
Size: 332 Color: 13
Size: 236 Color: 16
Size: 224 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5108 Color: 5
Size: 2716 Color: 2
Size: 176 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5558 Color: 0
Size: 2102 Color: 19
Size: 340 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5581 Color: 14
Size: 2017 Color: 14
Size: 402 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5629 Color: 11
Size: 1977 Color: 0
Size: 394 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5886 Color: 3
Size: 1474 Color: 15
Size: 640 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5963 Color: 17
Size: 1699 Color: 7
Size: 338 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 16
Size: 1540 Color: 14
Size: 304 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 14
Size: 1436 Color: 0
Size: 392 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6258 Color: 8
Size: 1454 Color: 12
Size: 288 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 1
Size: 983 Color: 4
Size: 632 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 12
Size: 884 Color: 18
Size: 724 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 19
Size: 1305 Color: 13
Size: 260 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6430 Color: 16
Size: 1202 Color: 6
Size: 368 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6446 Color: 7
Size: 1298 Color: 8
Size: 256 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6468 Color: 18
Size: 830 Color: 14
Size: 702 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6523 Color: 19
Size: 1231 Color: 15
Size: 246 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 3
Size: 1310 Color: 0
Size: 140 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 15
Size: 1195 Color: 0
Size: 238 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 6
Size: 1208 Color: 8
Size: 220 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6579 Color: 4
Size: 1155 Color: 13
Size: 266 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 19
Size: 1294 Color: 0
Size: 112 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 7
Size: 1156 Color: 13
Size: 224 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 19
Size: 1149 Color: 3
Size: 228 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6674 Color: 1
Size: 1020 Color: 15
Size: 306 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6679 Color: 12
Size: 1061 Color: 8
Size: 260 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6682 Color: 4
Size: 1154 Color: 19
Size: 164 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6765 Color: 14
Size: 883 Color: 5
Size: 352 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 8
Size: 716 Color: 12
Size: 512 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6821 Color: 8
Size: 991 Color: 14
Size: 188 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6831 Color: 18
Size: 1007 Color: 3
Size: 162 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6839 Color: 10
Size: 969 Color: 0
Size: 192 Color: 19

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6915 Color: 18
Size: 905 Color: 0
Size: 180 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 0
Size: 818 Color: 2
Size: 204 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 19
Size: 810 Color: 10
Size: 168 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 3
Size: 504 Color: 1
Size: 484 Color: 16

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 9
Size: 828 Color: 11
Size: 152 Color: 11

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7030 Color: 9
Size: 802 Color: 19
Size: 168 Color: 18

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7118 Color: 8
Size: 592 Color: 5
Size: 290 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 19
Size: 548 Color: 10
Size: 304 Color: 18

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5620 Color: 17
Size: 2247 Color: 7
Size: 132 Color: 7

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 6
Size: 2193 Color: 14
Size: 160 Color: 17

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5908 Color: 10
Size: 1739 Color: 6
Size: 352 Color: 2

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 5915 Color: 7
Size: 2084 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6003 Color: 19
Size: 1804 Color: 4
Size: 192 Color: 3

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6652 Color: 14
Size: 1347 Color: 8

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6923 Color: 17
Size: 756 Color: 19
Size: 320 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6971 Color: 6
Size: 576 Color: 12
Size: 452 Color: 15

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 10
Size: 859 Color: 9

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 4426 Color: 16
Size: 3228 Color: 12
Size: 344 Color: 14

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 4524 Color: 15
Size: 3274 Color: 18
Size: 200 Color: 10

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 4945 Color: 9
Size: 2883 Color: 19
Size: 170 Color: 12

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 5436 Color: 9
Size: 2412 Color: 15
Size: 150 Color: 1

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 5508 Color: 13
Size: 2490 Color: 4

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5625 Color: 10
Size: 1957 Color: 19
Size: 416 Color: 15

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 5834 Color: 4
Size: 2164 Color: 13

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6234 Color: 13
Size: 1764 Color: 1

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6269 Color: 2
Size: 1481 Color: 19
Size: 248 Color: 8

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 6395 Color: 13
Size: 1603 Color: 8

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 6615 Color: 15
Size: 1339 Color: 4
Size: 44 Color: 8

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 6746 Color: 17
Size: 1252 Color: 7

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 6802 Color: 10
Size: 1196 Color: 6

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 7074 Color: 18
Size: 924 Color: 2

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 6193 Color: 12
Size: 1524 Color: 19
Size: 280 Color: 2

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 7158 Color: 0
Size: 839 Color: 4

Bin 66: 4 of cap free
Amount of items: 6
Items: 
Size: 4002 Color: 2
Size: 1162 Color: 9
Size: 1101 Color: 11
Size: 931 Color: 12
Size: 664 Color: 10
Size: 136 Color: 10

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 5014 Color: 17
Size: 2982 Color: 3

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 5892 Color: 15
Size: 2104 Color: 4

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 9
Size: 1146 Color: 12
Size: 288 Color: 19

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6634 Color: 15
Size: 1362 Color: 13

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 7006 Color: 14
Size: 990 Color: 2

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 7042 Color: 7
Size: 954 Color: 9

Bin 73: 5 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 18
Size: 3331 Color: 8
Size: 516 Color: 19

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 5270 Color: 10
Size: 2429 Color: 2
Size: 296 Color: 15

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 5480 Color: 18
Size: 2169 Color: 4
Size: 346 Color: 10

Bin 76: 6 of cap free
Amount of items: 5
Items: 
Size: 4004 Color: 19
Size: 1512 Color: 16
Size: 1284 Color: 11
Size: 1028 Color: 2
Size: 166 Color: 9

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 4005 Color: 12
Size: 3709 Color: 2
Size: 280 Color: 19

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 4660 Color: 8
Size: 3334 Color: 18

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6380 Color: 5
Size: 1614 Color: 7

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6941 Color: 15
Size: 1053 Color: 13

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6948 Color: 7
Size: 1046 Color: 17

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 7100 Color: 2
Size: 894 Color: 11

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 4574 Color: 5
Size: 3332 Color: 6
Size: 86 Color: 6

Bin 84: 8 of cap free
Amount of items: 5
Items: 
Size: 4711 Color: 8
Size: 1981 Color: 5
Size: 1124 Color: 19
Size: 88 Color: 1
Size: 88 Color: 0

Bin 85: 8 of cap free
Amount of items: 3
Items: 
Size: 4884 Color: 12
Size: 2540 Color: 5
Size: 568 Color: 13

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 4
Size: 2012 Color: 18
Size: 576 Color: 7

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 5482 Color: 1
Size: 2510 Color: 7

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 5954 Color: 12
Size: 2038 Color: 5

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 6814 Color: 4
Size: 1178 Color: 1

Bin 90: 9 of cap free
Amount of items: 7
Items: 
Size: 4001 Color: 14
Size: 854 Color: 0
Size: 820 Color: 17
Size: 738 Color: 9
Size: 674 Color: 2
Size: 480 Color: 12
Size: 424 Color: 5

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 5305 Color: 4
Size: 2686 Color: 15

Bin 92: 9 of cap free
Amount of items: 3
Items: 
Size: 5653 Color: 9
Size: 1962 Color: 10
Size: 376 Color: 16

Bin 93: 9 of cap free
Amount of items: 2
Items: 
Size: 6780 Color: 17
Size: 1211 Color: 14

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 4778 Color: 1
Size: 3212 Color: 15

Bin 95: 10 of cap free
Amount of items: 3
Items: 
Size: 4956 Color: 3
Size: 2858 Color: 10
Size: 176 Color: 12

Bin 96: 10 of cap free
Amount of items: 3
Items: 
Size: 6042 Color: 3
Size: 1706 Color: 19
Size: 242 Color: 18

Bin 97: 10 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 16
Size: 1490 Color: 10

Bin 98: 10 of cap free
Amount of items: 2
Items: 
Size: 6547 Color: 1
Size: 1443 Color: 4

Bin 99: 11 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 13
Size: 1331 Color: 3
Size: 48 Color: 15

Bin 100: 11 of cap free
Amount of items: 2
Items: 
Size: 6883 Color: 16
Size: 1106 Color: 2

Bin 101: 12 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 3
Size: 3311 Color: 16
Size: 136 Color: 2

Bin 102: 12 of cap free
Amount of items: 2
Items: 
Size: 6737 Color: 2
Size: 1251 Color: 18

Bin 103: 12 of cap free
Amount of items: 2
Items: 
Size: 6900 Color: 15
Size: 1088 Color: 8

Bin 104: 13 of cap free
Amount of items: 3
Items: 
Size: 4902 Color: 11
Size: 2741 Color: 15
Size: 344 Color: 18

Bin 105: 13 of cap free
Amount of items: 2
Items: 
Size: 5087 Color: 1
Size: 2900 Color: 16

Bin 106: 13 of cap free
Amount of items: 2
Items: 
Size: 6225 Color: 7
Size: 1762 Color: 3

Bin 107: 13 of cap free
Amount of items: 2
Items: 
Size: 6995 Color: 16
Size: 992 Color: 14

Bin 108: 15 of cap free
Amount of items: 3
Items: 
Size: 4497 Color: 0
Size: 3260 Color: 18
Size: 228 Color: 19

Bin 109: 16 of cap free
Amount of items: 2
Items: 
Size: 5844 Color: 11
Size: 2140 Color: 17

Bin 110: 18 of cap free
Amount of items: 31
Items: 
Size: 408 Color: 10
Size: 404 Color: 17
Size: 394 Color: 14
Size: 390 Color: 6
Size: 372 Color: 0
Size: 360 Color: 8
Size: 324 Color: 2
Size: 296 Color: 13
Size: 294 Color: 7
Size: 292 Color: 8
Size: 268 Color: 4
Size: 266 Color: 0
Size: 264 Color: 12
Size: 248 Color: 19
Size: 232 Color: 17
Size: 230 Color: 11
Size: 228 Color: 0
Size: 224 Color: 4
Size: 220 Color: 1
Size: 216 Color: 12
Size: 212 Color: 3
Size: 210 Color: 6
Size: 208 Color: 6
Size: 204 Color: 10
Size: 200 Color: 16
Size: 196 Color: 15
Size: 196 Color: 14
Size: 186 Color: 13
Size: 176 Color: 5
Size: 144 Color: 5
Size: 120 Color: 11

Bin 111: 18 of cap free
Amount of items: 2
Items: 
Size: 6568 Color: 7
Size: 1414 Color: 15

Bin 112: 18 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 17
Size: 1356 Color: 4

Bin 113: 19 of cap free
Amount of items: 2
Items: 
Size: 5399 Color: 15
Size: 2582 Color: 10

Bin 114: 29 of cap free
Amount of items: 2
Items: 
Size: 6306 Color: 9
Size: 1665 Color: 1

Bin 115: 31 of cap free
Amount of items: 3
Items: 
Size: 4207 Color: 8
Size: 3330 Color: 4
Size: 432 Color: 10

Bin 116: 32 of cap free
Amount of items: 2
Items: 
Size: 7194 Color: 12
Size: 774 Color: 10

Bin 117: 33 of cap free
Amount of items: 2
Items: 
Size: 5765 Color: 13
Size: 2202 Color: 4

Bin 118: 33 of cap free
Amount of items: 2
Items: 
Size: 6727 Color: 16
Size: 1240 Color: 0

Bin 119: 34 of cap free
Amount of items: 2
Items: 
Size: 4990 Color: 14
Size: 2976 Color: 0

Bin 120: 38 of cap free
Amount of items: 2
Items: 
Size: 5358 Color: 17
Size: 2604 Color: 14

Bin 121: 38 of cap free
Amount of items: 2
Items: 
Size: 6214 Color: 17
Size: 1748 Color: 9

Bin 122: 39 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 8
Size: 1031 Color: 17

Bin 123: 40 of cap free
Amount of items: 2
Items: 
Size: 6858 Color: 16
Size: 1102 Color: 14

Bin 124: 41 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 1
Size: 2547 Color: 2
Size: 664 Color: 8

Bin 125: 41 of cap free
Amount of items: 2
Items: 
Size: 6153 Color: 13
Size: 1806 Color: 18

Bin 126: 42 of cap free
Amount of items: 2
Items: 
Size: 4006 Color: 13
Size: 3952 Color: 16

Bin 127: 56 of cap free
Amount of items: 2
Items: 
Size: 6403 Color: 9
Size: 1541 Color: 2

Bin 128: 58 of cap free
Amount of items: 2
Items: 
Size: 4430 Color: 18
Size: 3512 Color: 19

Bin 129: 65 of cap free
Amount of items: 2
Items: 
Size: 5657 Color: 16
Size: 2278 Color: 3

Bin 130: 71 of cap free
Amount of items: 2
Items: 
Size: 6066 Color: 4
Size: 1863 Color: 5

Bin 131: 75 of cap free
Amount of items: 3
Items: 
Size: 4268 Color: 16
Size: 3161 Color: 13
Size: 496 Color: 2

Bin 132: 82 of cap free
Amount of items: 2
Items: 
Size: 6284 Color: 7
Size: 1634 Color: 16

Bin 133: 6638 of cap free
Amount of items: 9
Items: 
Size: 188 Color: 14
Size: 170 Color: 13
Size: 160 Color: 10
Size: 160 Color: 4
Size: 160 Color: 3
Size: 156 Color: 1
Size: 144 Color: 9
Size: 112 Color: 11
Size: 112 Color: 5

Total size: 1056000
Total free space: 8000

