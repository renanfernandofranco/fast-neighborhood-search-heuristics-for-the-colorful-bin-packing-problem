Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 421592 Color: 3
Size: 314127 Color: 4
Size: 264282 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 472655 Color: 0
Size: 275466 Color: 3
Size: 251880 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 433061 Color: 4
Size: 297031 Color: 1
Size: 269909 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 435916 Color: 3
Size: 311899 Color: 4
Size: 252186 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 454746 Color: 3
Size: 288340 Color: 0
Size: 256915 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 378606 Color: 3
Size: 367106 Color: 2
Size: 254289 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 409656 Color: 2
Size: 299835 Color: 1
Size: 290510 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 383186 Color: 4
Size: 342413 Color: 0
Size: 274402 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 430643 Color: 2
Size: 300507 Color: 0
Size: 268851 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 424297 Color: 1
Size: 289258 Color: 0
Size: 286446 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 458848 Color: 0
Size: 277028 Color: 0
Size: 264125 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 427754 Color: 1
Size: 294116 Color: 2
Size: 278131 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 497362 Color: 0
Size: 251452 Color: 2
Size: 251187 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 427398 Color: 1
Size: 321701 Color: 3
Size: 250902 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 485790 Color: 4
Size: 262867 Color: 1
Size: 251344 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 443074 Color: 1
Size: 289126 Color: 1
Size: 267801 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 446317 Color: 1
Size: 291957 Color: 3
Size: 261727 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 423965 Color: 3
Size: 310421 Color: 0
Size: 265615 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 429115 Color: 1
Size: 320013 Color: 1
Size: 250873 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 473743 Color: 1
Size: 275213 Color: 0
Size: 251045 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 447905 Color: 1
Size: 279134 Color: 4
Size: 272962 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 446998 Color: 4
Size: 301915 Color: 1
Size: 251088 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 464397 Color: 3
Size: 285123 Color: 1
Size: 250481 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 446913 Color: 1
Size: 284419 Color: 4
Size: 268669 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 416496 Color: 1
Size: 332007 Color: 2
Size: 251498 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 473085 Color: 0
Size: 276137 Color: 4
Size: 250779 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 352351 Color: 3
Size: 338708 Color: 2
Size: 308942 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 395370 Color: 3
Size: 352571 Color: 0
Size: 252060 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 439450 Color: 1
Size: 296850 Color: 0
Size: 263701 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 398457 Color: 2
Size: 346135 Color: 1
Size: 255409 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 362560 Color: 1
Size: 318991 Color: 1
Size: 318450 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 451061 Color: 3
Size: 298890 Color: 2
Size: 250050 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 439136 Color: 2
Size: 294220 Color: 4
Size: 266645 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 445243 Color: 4
Size: 282185 Color: 0
Size: 272573 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 410177 Color: 2
Size: 307893 Color: 4
Size: 281931 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 386404 Color: 2
Size: 318603 Color: 3
Size: 294994 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 416640 Color: 2
Size: 318414 Color: 1
Size: 264947 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 447787 Color: 1
Size: 282973 Color: 1
Size: 269241 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 406459 Color: 4
Size: 307978 Color: 4
Size: 285564 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 490635 Color: 4
Size: 255905 Color: 2
Size: 253461 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 387563 Color: 1
Size: 343394 Color: 0
Size: 269044 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 453489 Color: 2
Size: 273888 Color: 1
Size: 272624 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 408298 Color: 2
Size: 304554 Color: 3
Size: 287149 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 439416 Color: 1
Size: 291786 Color: 4
Size: 268799 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 446741 Color: 1
Size: 283350 Color: 2
Size: 269910 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 480001 Color: 3
Size: 262463 Color: 1
Size: 257537 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 491748 Color: 4
Size: 255165 Color: 1
Size: 253088 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 367107 Color: 2
Size: 363873 Color: 2
Size: 269021 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 490427 Color: 4
Size: 256034 Color: 0
Size: 253540 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 364126 Color: 0
Size: 341299 Color: 3
Size: 294576 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 443393 Color: 1
Size: 289221 Color: 2
Size: 267387 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 357952 Color: 3
Size: 350387 Color: 0
Size: 291662 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 396946 Color: 2
Size: 351118 Color: 0
Size: 251937 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 459551 Color: 1
Size: 276961 Color: 1
Size: 263489 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 435315 Color: 3
Size: 302711 Color: 4
Size: 261975 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 450521 Color: 1
Size: 282373 Color: 3
Size: 267107 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 457378 Color: 3
Size: 282034 Color: 2
Size: 260589 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 478247 Color: 0
Size: 271581 Color: 4
Size: 250173 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 495810 Color: 3
Size: 252851 Color: 3
Size: 251340 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 426331 Color: 0
Size: 315967 Color: 4
Size: 257703 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 387860 Color: 1
Size: 318825 Color: 0
Size: 293316 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 497456 Color: 2
Size: 252361 Color: 3
Size: 250184 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 488742 Color: 1
Size: 256430 Color: 4
Size: 254829 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 441385 Color: 0
Size: 300022 Color: 0
Size: 258594 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 451600 Color: 1
Size: 279877 Color: 4
Size: 268524 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 455444 Color: 0
Size: 275407 Color: 2
Size: 269150 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 449615 Color: 1
Size: 283203 Color: 1
Size: 267183 Color: 4

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 381755 Color: 3
Size: 362698 Color: 3
Size: 255548 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 420431 Color: 4
Size: 327842 Color: 1
Size: 251728 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 387346 Color: 0
Size: 352750 Color: 3
Size: 259905 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 474705 Color: 4
Size: 264299 Color: 3
Size: 260997 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 385585 Color: 2
Size: 358876 Color: 4
Size: 255540 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 419837 Color: 4
Size: 328133 Color: 3
Size: 252031 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 366019 Color: 1
Size: 333092 Color: 3
Size: 300890 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 455528 Color: 0
Size: 274945 Color: 4
Size: 269528 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 393971 Color: 4
Size: 349620 Color: 3
Size: 256410 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 431669 Color: 2
Size: 308918 Color: 4
Size: 259414 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 399654 Color: 0
Size: 332770 Color: 1
Size: 267577 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 385513 Color: 3
Size: 330988 Color: 4
Size: 283500 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 412409 Color: 1
Size: 297695 Color: 2
Size: 289897 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 417517 Color: 1
Size: 291693 Color: 2
Size: 290791 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 444898 Color: 0
Size: 292514 Color: 3
Size: 262589 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 415912 Color: 2
Size: 313605 Color: 0
Size: 270484 Color: 4

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 446565 Color: 1
Size: 287378 Color: 2
Size: 266058 Color: 3

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 394398 Color: 1
Size: 349787 Color: 0
Size: 255816 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 497135 Color: 4
Size: 251572 Color: 3
Size: 251294 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 467278 Color: 0
Size: 267369 Color: 4
Size: 265354 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 392961 Color: 3
Size: 306778 Color: 4
Size: 300262 Color: 4

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 491215 Color: 1
Size: 258655 Color: 2
Size: 250131 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 471970 Color: 2
Size: 276199 Color: 3
Size: 251832 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 357875 Color: 0
Size: 355214 Color: 2
Size: 286912 Color: 4

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 453534 Color: 4
Size: 291197 Color: 1
Size: 255270 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 366349 Color: 1
Size: 347249 Color: 4
Size: 286403 Color: 3

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 358520 Color: 3
Size: 323798 Color: 0
Size: 317683 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 484655 Color: 3
Size: 258679 Color: 2
Size: 256667 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 416673 Color: 2
Size: 305454 Color: 1
Size: 277874 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 432010 Color: 0
Size: 299023 Color: 2
Size: 268968 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 458804 Color: 0
Size: 271077 Color: 2
Size: 270120 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 363582 Color: 3
Size: 360258 Color: 3
Size: 276161 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 416083 Color: 4
Size: 293090 Color: 1
Size: 290828 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 468197 Color: 2
Size: 272443 Color: 3
Size: 259361 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 490327 Color: 4
Size: 256547 Color: 1
Size: 253127 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 431252 Color: 4
Size: 308351 Color: 3
Size: 260398 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 455599 Color: 2
Size: 273936 Color: 3
Size: 270466 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 393311 Color: 4
Size: 342375 Color: 2
Size: 264315 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 370262 Color: 1
Size: 346116 Color: 4
Size: 283623 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 453703 Color: 1
Size: 274365 Color: 0
Size: 271933 Color: 2

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 392147 Color: 2
Size: 329935 Color: 0
Size: 277919 Color: 2

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 496419 Color: 3
Size: 253271 Color: 0
Size: 250311 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 372430 Color: 4
Size: 321587 Color: 0
Size: 305984 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 463993 Color: 1
Size: 277429 Color: 2
Size: 258579 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 480953 Color: 3
Size: 268032 Color: 0
Size: 251016 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 381195 Color: 3
Size: 321848 Color: 2
Size: 296958 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 437801 Color: 2
Size: 284618 Color: 1
Size: 277582 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 468765 Color: 1
Size: 279014 Color: 0
Size: 252222 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 406266 Color: 4
Size: 322059 Color: 0
Size: 271676 Color: 2

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 380174 Color: 4
Size: 336782 Color: 0
Size: 283045 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 403603 Color: 2
Size: 319944 Color: 3
Size: 276454 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 492807 Color: 4
Size: 254494 Color: 3
Size: 252700 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 495549 Color: 1
Size: 253052 Color: 0
Size: 251400 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 354818 Color: 2
Size: 345472 Color: 3
Size: 299711 Color: 3

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 471110 Color: 0
Size: 266751 Color: 2
Size: 262140 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 490241 Color: 2
Size: 257094 Color: 0
Size: 252666 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 385538 Color: 2
Size: 307955 Color: 3
Size: 306508 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 460032 Color: 4
Size: 275360 Color: 2
Size: 264609 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 360757 Color: 0
Size: 356565 Color: 3
Size: 282679 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 493620 Color: 2
Size: 253885 Color: 4
Size: 252496 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 395326 Color: 1
Size: 346034 Color: 3
Size: 258641 Color: 2

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 462270 Color: 0
Size: 277571 Color: 4
Size: 260160 Color: 2

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 416095 Color: 2
Size: 313257 Color: 0
Size: 270649 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 392960 Color: 2
Size: 323975 Color: 4
Size: 283066 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 419293 Color: 1
Size: 319848 Color: 1
Size: 260860 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 377121 Color: 2
Size: 348425 Color: 2
Size: 274455 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 379701 Color: 2
Size: 353094 Color: 1
Size: 267206 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 444696 Color: 1
Size: 294438 Color: 2
Size: 260867 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 485570 Color: 0
Size: 258169 Color: 2
Size: 256262 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 464442 Color: 0
Size: 269250 Color: 3
Size: 266309 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 375149 Color: 2
Size: 370922 Color: 2
Size: 253930 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 494565 Color: 2
Size: 252958 Color: 1
Size: 252478 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 398821 Color: 3
Size: 309388 Color: 0
Size: 291792 Color: 3

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 381553 Color: 3
Size: 309965 Color: 4
Size: 308483 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 412107 Color: 2
Size: 320125 Color: 3
Size: 267769 Color: 4

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 495148 Color: 0
Size: 253532 Color: 4
Size: 251321 Color: 4

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 438895 Color: 4
Size: 282721 Color: 1
Size: 278385 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 486042 Color: 2
Size: 257271 Color: 3
Size: 256688 Color: 4

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 415311 Color: 0
Size: 304451 Color: 2
Size: 280239 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 459576 Color: 3
Size: 287991 Color: 2
Size: 252434 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 396509 Color: 0
Size: 306160 Color: 0
Size: 297332 Color: 2

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 394799 Color: 4
Size: 321997 Color: 2
Size: 283205 Color: 3

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 467396 Color: 4
Size: 275889 Color: 2
Size: 256716 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 377030 Color: 0
Size: 316146 Color: 1
Size: 306825 Color: 2

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 434813 Color: 2
Size: 286818 Color: 1
Size: 278370 Color: 4

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 448770 Color: 4
Size: 286949 Color: 3
Size: 264282 Color: 3

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 377014 Color: 2
Size: 345805 Color: 1
Size: 277182 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 425159 Color: 1
Size: 311086 Color: 2
Size: 263756 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 426889 Color: 4
Size: 286678 Color: 0
Size: 286434 Color: 2

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 459367 Color: 2
Size: 279674 Color: 4
Size: 260960 Color: 4

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 445637 Color: 2
Size: 295763 Color: 1
Size: 258601 Color: 4

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 484398 Color: 3
Size: 258924 Color: 0
Size: 256679 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 410651 Color: 2
Size: 299835 Color: 1
Size: 289515 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 470266 Color: 2
Size: 273953 Color: 0
Size: 255782 Color: 2

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 406252 Color: 3
Size: 312658 Color: 4
Size: 281091 Color: 4

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 469855 Color: 1
Size: 276796 Color: 3
Size: 253350 Color: 4

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 447109 Color: 3
Size: 296033 Color: 4
Size: 256859 Color: 2

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 455668 Color: 4
Size: 287020 Color: 2
Size: 257313 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 474194 Color: 3
Size: 264168 Color: 2
Size: 261639 Color: 2

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 402723 Color: 2
Size: 333194 Color: 0
Size: 264084 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 464804 Color: 0
Size: 272844 Color: 3
Size: 262353 Color: 4

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 368547 Color: 3
Size: 358024 Color: 0
Size: 273430 Color: 4

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 450569 Color: 3
Size: 295762 Color: 4
Size: 253670 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 470538 Color: 4
Size: 267621 Color: 1
Size: 261842 Color: 4

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 483534 Color: 0
Size: 261790 Color: 3
Size: 254677 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 464393 Color: 2
Size: 276008 Color: 4
Size: 259600 Color: 4

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 446130 Color: 2
Size: 278288 Color: 4
Size: 275583 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 461161 Color: 0
Size: 273238 Color: 4
Size: 265602 Color: 2

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 414025 Color: 0
Size: 295065 Color: 4
Size: 290911 Color: 2

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 478787 Color: 1
Size: 269119 Color: 2
Size: 252095 Color: 3

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 412388 Color: 3
Size: 297436 Color: 0
Size: 290177 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 380221 Color: 1
Size: 340188 Color: 2
Size: 279592 Color: 4

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 346080 Color: 2
Size: 338326 Color: 2
Size: 315595 Color: 4

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 402561 Color: 3
Size: 317062 Color: 3
Size: 280378 Color: 4

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 484941 Color: 2
Size: 264092 Color: 3
Size: 250968 Color: 4

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 427616 Color: 0
Size: 302977 Color: 4
Size: 269408 Color: 2

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 446118 Color: 3
Size: 301799 Color: 2
Size: 252084 Color: 3

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 383161 Color: 3
Size: 342726 Color: 2
Size: 274114 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 495451 Color: 1
Size: 252623 Color: 1
Size: 251927 Color: 2

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 394430 Color: 3
Size: 330046 Color: 2
Size: 275525 Color: 3

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 393614 Color: 1
Size: 332019 Color: 0
Size: 274368 Color: 2

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 405032 Color: 3
Size: 314441 Color: 2
Size: 280528 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 440538 Color: 4
Size: 297046 Color: 1
Size: 262417 Color: 3

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 453652 Color: 2
Size: 282347 Color: 3
Size: 264002 Color: 4

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 440803 Color: 1
Size: 279626 Color: 0
Size: 279572 Color: 2

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 486766 Color: 4
Size: 257402 Color: 0
Size: 255833 Color: 2

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 373858 Color: 3
Size: 352864 Color: 2
Size: 273279 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 401670 Color: 0
Size: 307076 Color: 0
Size: 291255 Color: 4

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 384332 Color: 0
Size: 315909 Color: 4
Size: 299760 Color: 3

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 490163 Color: 1
Size: 257155 Color: 3
Size: 252683 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 438170 Color: 4
Size: 283652 Color: 1
Size: 278179 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 426721 Color: 3
Size: 317606 Color: 1
Size: 255674 Color: 4

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 452554 Color: 2
Size: 280577 Color: 4
Size: 266870 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 454263 Color: 4
Size: 295220 Color: 2
Size: 250518 Color: 3

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 489504 Color: 1
Size: 260166 Color: 2
Size: 250331 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 462298 Color: 2
Size: 285533 Color: 0
Size: 252170 Color: 3

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 365995 Color: 3
Size: 326621 Color: 1
Size: 307385 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 442320 Color: 3
Size: 304326 Color: 2
Size: 253355 Color: 2

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 441274 Color: 0
Size: 285947 Color: 3
Size: 272780 Color: 2

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 414432 Color: 2
Size: 323098 Color: 0
Size: 262471 Color: 3

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 377327 Color: 2
Size: 343102 Color: 0
Size: 279572 Color: 4

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 388454 Color: 1
Size: 347367 Color: 0
Size: 264180 Color: 2

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 423901 Color: 2
Size: 294080 Color: 2
Size: 282020 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 435025 Color: 0
Size: 294663 Color: 3
Size: 270313 Color: 4

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 401280 Color: 0
Size: 337926 Color: 0
Size: 260795 Color: 2

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 481268 Color: 4
Size: 266072 Color: 4
Size: 252661 Color: 2

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 479275 Color: 1
Size: 266655 Color: 3
Size: 254071 Color: 4

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 395572 Color: 2
Size: 345431 Color: 1
Size: 258998 Color: 4

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 377281 Color: 1
Size: 368413 Color: 3
Size: 254307 Color: 2

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 418551 Color: 0
Size: 298130 Color: 4
Size: 283320 Color: 4

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 485196 Color: 0
Size: 263317 Color: 1
Size: 251488 Color: 3

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 377979 Color: 4
Size: 325271 Color: 2
Size: 296751 Color: 4

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 385418 Color: 3
Size: 359861 Color: 1
Size: 254722 Color: 4

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 379971 Color: 3
Size: 345809 Color: 4
Size: 274221 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 400502 Color: 4
Size: 315851 Color: 4
Size: 283648 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 366307 Color: 1
Size: 361762 Color: 4
Size: 271932 Color: 2

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 415863 Color: 3
Size: 306320 Color: 0
Size: 277818 Color: 4

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 382263 Color: 1
Size: 321012 Color: 0
Size: 296726 Color: 3

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 442911 Color: 3
Size: 295416 Color: 3
Size: 261674 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 373731 Color: 1
Size: 313356 Color: 2
Size: 312914 Color: 2

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 381905 Color: 3
Size: 350060 Color: 0
Size: 268036 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 378225 Color: 4
Size: 371236 Color: 1
Size: 250540 Color: 2

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 483432 Color: 3
Size: 260362 Color: 4
Size: 256207 Color: 3

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 360703 Color: 2
Size: 349517 Color: 1
Size: 289781 Color: 3

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 361114 Color: 4
Size: 333620 Color: 2
Size: 305267 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 367538 Color: 4
Size: 352111 Color: 0
Size: 280352 Color: 3

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 344754 Color: 4
Size: 340012 Color: 0
Size: 315235 Color: 4

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 392643 Color: 1
Size: 349217 Color: 0
Size: 258141 Color: 4

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 366762 Color: 0
Size: 336773 Color: 2
Size: 296466 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 395640 Color: 2
Size: 302345 Color: 2
Size: 302016 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 359410 Color: 1
Size: 358698 Color: 4
Size: 281893 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 499022 Color: 3
Size: 250774 Color: 1
Size: 250205 Color: 4

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 420585 Color: 3
Size: 328832 Color: 4
Size: 250584 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 398008 Color: 2
Size: 328209 Color: 0
Size: 273784 Color: 3

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 356547 Color: 1
Size: 348511 Color: 0
Size: 294943 Color: 2

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 403413 Color: 4
Size: 341146 Color: 4
Size: 255442 Color: 3

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 411629 Color: 4
Size: 313363 Color: 0
Size: 275009 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 367802 Color: 4
Size: 350356 Color: 0
Size: 281843 Color: 2

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 372140 Color: 0
Size: 358601 Color: 4
Size: 269260 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 372720 Color: 0
Size: 320209 Color: 4
Size: 307072 Color: 2

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 372270 Color: 1
Size: 351420 Color: 4
Size: 276311 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 375707 Color: 0
Size: 320552 Color: 2
Size: 303742 Color: 3

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 377589 Color: 3
Size: 339187 Color: 2
Size: 283225 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 380257 Color: 0
Size: 338467 Color: 2
Size: 281277 Color: 4

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 380698 Color: 3
Size: 334415 Color: 4
Size: 284888 Color: 2

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 381929 Color: 4
Size: 353315 Color: 3
Size: 264757 Color: 3

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 385353 Color: 2
Size: 318180 Color: 0
Size: 296468 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 386233 Color: 4
Size: 331600 Color: 3
Size: 282168 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 388333 Color: 3
Size: 305852 Color: 4
Size: 305816 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 388564 Color: 1
Size: 312768 Color: 0
Size: 298669 Color: 3

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 392537 Color: 2
Size: 318619 Color: 1
Size: 288845 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 393536 Color: 2
Size: 327799 Color: 3
Size: 278666 Color: 3

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 395049 Color: 2
Size: 352110 Color: 1
Size: 252842 Color: 3

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 396200 Color: 4
Size: 338072 Color: 2
Size: 265729 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 396372 Color: 0
Size: 316518 Color: 3
Size: 287111 Color: 3

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 396941 Color: 2
Size: 326815 Color: 4
Size: 276245 Color: 4

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 397378 Color: 2
Size: 320681 Color: 4
Size: 281942 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 398992 Color: 3
Size: 339000 Color: 4
Size: 262009 Color: 2

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 400159 Color: 2
Size: 312280 Color: 0
Size: 287562 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 400518 Color: 3
Size: 334915 Color: 2
Size: 264568 Color: 3

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 401075 Color: 3
Size: 318819 Color: 1
Size: 280107 Color: 4

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 403391 Color: 2
Size: 346336 Color: 1
Size: 250274 Color: 4

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 403348 Color: 1
Size: 339313 Color: 0
Size: 257340 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 407016 Color: 0
Size: 304883 Color: 3
Size: 288102 Color: 4

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 407743 Color: 2
Size: 296485 Color: 1
Size: 295773 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 409718 Color: 2
Size: 338318 Color: 1
Size: 251965 Color: 3

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 412035 Color: 3
Size: 312315 Color: 3
Size: 275651 Color: 2

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 413114 Color: 0
Size: 329538 Color: 2
Size: 257349 Color: 4

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 413179 Color: 0
Size: 336097 Color: 2
Size: 250725 Color: 4

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 413824 Color: 0
Size: 302553 Color: 1
Size: 283624 Color: 3

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 414956 Color: 2
Size: 295526 Color: 4
Size: 289519 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 415377 Color: 0
Size: 317864 Color: 1
Size: 266760 Color: 2

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 416071 Color: 2
Size: 323750 Color: 3
Size: 260180 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 416039 Color: 3
Size: 325440 Color: 4
Size: 258522 Color: 2

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 416170 Color: 4
Size: 332705 Color: 2
Size: 251126 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 416885 Color: 0
Size: 322160 Color: 3
Size: 260956 Color: 3

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 418485 Color: 2
Size: 330030 Color: 4
Size: 251486 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 418568 Color: 2
Size: 291256 Color: 1
Size: 290177 Color: 4

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 418367 Color: 3
Size: 326690 Color: 1
Size: 254944 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 419506 Color: 1
Size: 291914 Color: 2
Size: 288581 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 419582 Color: 2
Size: 303288 Color: 3
Size: 277131 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 420369 Color: 3
Size: 294856 Color: 3
Size: 284776 Color: 2

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 420420 Color: 4
Size: 327791 Color: 2
Size: 251790 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 421033 Color: 0
Size: 298962 Color: 0
Size: 280006 Color: 2

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 421387 Color: 0
Size: 299320 Color: 1
Size: 279294 Color: 3

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 422586 Color: 1
Size: 326321 Color: 3
Size: 251094 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 424439 Color: 3
Size: 290041 Color: 2
Size: 285521 Color: 3

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 424728 Color: 1
Size: 322857 Color: 1
Size: 252416 Color: 2

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 424951 Color: 2
Size: 296418 Color: 3
Size: 278632 Color: 4

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 424793 Color: 3
Size: 317554 Color: 4
Size: 257654 Color: 3

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 425371 Color: 4
Size: 304562 Color: 1
Size: 270068 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 425891 Color: 0
Size: 302748 Color: 2
Size: 271362 Color: 1

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 426284 Color: 3
Size: 293393 Color: 0
Size: 280324 Color: 2

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 427196 Color: 3
Size: 303073 Color: 4
Size: 269732 Color: 2

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 427881 Color: 0
Size: 315751 Color: 1
Size: 256369 Color: 3

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 428850 Color: 2
Size: 320240 Color: 1
Size: 250911 Color: 4

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 429342 Color: 0
Size: 319740 Color: 0
Size: 250919 Color: 2

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 429615 Color: 3
Size: 310312 Color: 4
Size: 260074 Color: 4

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 430924 Color: 3
Size: 303866 Color: 4
Size: 265211 Color: 2

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 431090 Color: 4
Size: 299995 Color: 0
Size: 268916 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 432186 Color: 3
Size: 309411 Color: 3
Size: 258404 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 432797 Color: 3
Size: 293967 Color: 1
Size: 273237 Color: 2

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 433662 Color: 2
Size: 297020 Color: 0
Size: 269319 Color: 3

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 434010 Color: 3
Size: 308470 Color: 3
Size: 257521 Color: 2

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 435410 Color: 0
Size: 285476 Color: 2
Size: 279115 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 435616 Color: 4
Size: 292548 Color: 3
Size: 271837 Color: 2

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 436305 Color: 1
Size: 305326 Color: 3
Size: 258370 Color: 2

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 436451 Color: 4
Size: 290590 Color: 1
Size: 272960 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 437224 Color: 0
Size: 304196 Color: 1
Size: 258581 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 438417 Color: 2
Size: 308757 Color: 4
Size: 252827 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 439782 Color: 1
Size: 283942 Color: 3
Size: 276277 Color: 2

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 439805 Color: 1
Size: 294447 Color: 3
Size: 265749 Color: 3

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 441113 Color: 1
Size: 306476 Color: 4
Size: 252412 Color: 4

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 442167 Color: 2
Size: 298598 Color: 4
Size: 259236 Color: 4

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 443250 Color: 0
Size: 285326 Color: 2
Size: 271425 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 443619 Color: 3
Size: 297355 Color: 4
Size: 259027 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 444143 Color: 2
Size: 292698 Color: 3
Size: 263160 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 444093 Color: 3
Size: 285518 Color: 4
Size: 270390 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 446187 Color: 0
Size: 302109 Color: 1
Size: 251705 Color: 2

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 446755 Color: 3
Size: 296868 Color: 4
Size: 256378 Color: 3

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 446857 Color: 4
Size: 293901 Color: 4
Size: 259243 Color: 2

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 447192 Color: 0
Size: 281904 Color: 4
Size: 270905 Color: 2

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 448782 Color: 2
Size: 287079 Color: 3
Size: 264140 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 448814 Color: 0
Size: 277901 Color: 3
Size: 273286 Color: 2

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 448916 Color: 3
Size: 282177 Color: 2
Size: 268908 Color: 3

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 449651 Color: 4
Size: 289199 Color: 0
Size: 261151 Color: 2

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 450229 Color: 1
Size: 284452 Color: 0
Size: 265320 Color: 4

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 450653 Color: 0
Size: 285294 Color: 4
Size: 264054 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 451302 Color: 0
Size: 274872 Color: 3
Size: 273827 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 451524 Color: 1
Size: 296522 Color: 4
Size: 251955 Color: 2

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 453962 Color: 0
Size: 285043 Color: 4
Size: 260996 Color: 2

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 454023 Color: 1
Size: 292446 Color: 4
Size: 253532 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 455297 Color: 3
Size: 283246 Color: 0
Size: 261458 Color: 4

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 455405 Color: 0
Size: 290015 Color: 2
Size: 254581 Color: 3

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 456513 Color: 0
Size: 290529 Color: 2
Size: 252959 Color: 4

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 456551 Color: 1
Size: 291210 Color: 2
Size: 252240 Color: 2

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 456673 Color: 3
Size: 293224 Color: 1
Size: 250104 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 457503 Color: 2
Size: 279052 Color: 1
Size: 263446 Color: 3

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 459552 Color: 2
Size: 277374 Color: 1
Size: 263075 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 460519 Color: 4
Size: 288572 Color: 0
Size: 250910 Color: 1

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 461416 Color: 1
Size: 281045 Color: 4
Size: 257540 Color: 3

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 461688 Color: 3
Size: 273844 Color: 0
Size: 264469 Color: 2

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 461822 Color: 3
Size: 277185 Color: 4
Size: 260994 Color: 4

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 463499 Color: 4
Size: 277336 Color: 4
Size: 259166 Color: 3

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 463971 Color: 2
Size: 280310 Color: 1
Size: 255720 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 466658 Color: 4
Size: 274907 Color: 2
Size: 258436 Color: 1

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 467326 Color: 2
Size: 278333 Color: 4
Size: 254342 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 468078 Color: 1
Size: 270211 Color: 4
Size: 261712 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 468397 Color: 1
Size: 273222 Color: 2
Size: 258382 Color: 3

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 469624 Color: 2
Size: 274170 Color: 3
Size: 256207 Color: 4

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 470454 Color: 4
Size: 272840 Color: 0
Size: 256707 Color: 1

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 471228 Color: 1
Size: 274208 Color: 3
Size: 254565 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 473290 Color: 2
Size: 276687 Color: 0
Size: 250024 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 475481 Color: 2
Size: 270057 Color: 4
Size: 254463 Color: 3

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 476280 Color: 0
Size: 267019 Color: 2
Size: 256702 Color: 4

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 479010 Color: 0
Size: 263674 Color: 0
Size: 257317 Color: 2

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 479949 Color: 4
Size: 267478 Color: 2
Size: 252574 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 480867 Color: 1
Size: 262607 Color: 0
Size: 256527 Color: 2

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 482024 Color: 2
Size: 263043 Color: 4
Size: 254934 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 481867 Color: 0
Size: 267705 Color: 3
Size: 250429 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 483258 Color: 2
Size: 261993 Color: 3
Size: 254750 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 482883 Color: 1
Size: 260374 Color: 0
Size: 256744 Color: 4

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 483306 Color: 0
Size: 265345 Color: 2
Size: 251350 Color: 1

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 483942 Color: 1
Size: 259915 Color: 2
Size: 256144 Color: 3

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 484149 Color: 0
Size: 261191 Color: 2
Size: 254661 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 485612 Color: 1
Size: 257306 Color: 1
Size: 257083 Color: 2

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 486688 Color: 4
Size: 257143 Color: 4
Size: 256170 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 487105 Color: 4
Size: 257505 Color: 1
Size: 255391 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 487109 Color: 3
Size: 258717 Color: 2
Size: 254175 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 488997 Color: 2
Size: 257925 Color: 1
Size: 253079 Color: 1

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 490468 Color: 0
Size: 256701 Color: 3
Size: 252832 Color: 3

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 491391 Color: 4
Size: 257770 Color: 4
Size: 250840 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 492461 Color: 2
Size: 256707 Color: 2
Size: 250833 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 492511 Color: 2
Size: 256898 Color: 0
Size: 250592 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 493623 Color: 2
Size: 255685 Color: 4
Size: 250693 Color: 3

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 493364 Color: 1
Size: 256370 Color: 0
Size: 250267 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 493952 Color: 2
Size: 254953 Color: 3
Size: 251096 Color: 3

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 494512 Color: 2
Size: 255243 Color: 0
Size: 250246 Color: 3

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 495024 Color: 1
Size: 254528 Color: 3
Size: 250449 Color: 2

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 496939 Color: 4
Size: 252853 Color: 2
Size: 250209 Color: 1

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 423287 Color: 4
Size: 289247 Color: 2
Size: 287466 Color: 0

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 424214 Color: 4
Size: 323300 Color: 0
Size: 252486 Color: 2

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 406170 Color: 0
Size: 305225 Color: 2
Size: 288605 Color: 4

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 415203 Color: 4
Size: 293336 Color: 1
Size: 291461 Color: 1

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 350743 Color: 2
Size: 341970 Color: 2
Size: 307287 Color: 1

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 418104 Color: 2
Size: 330245 Color: 3
Size: 251651 Color: 0

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 394256 Color: 2
Size: 346014 Color: 3
Size: 259730 Color: 1

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 372181 Color: 3
Size: 327025 Color: 0
Size: 300794 Color: 0

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 370563 Color: 4
Size: 359319 Color: 4
Size: 270118 Color: 0

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 402302 Color: 1
Size: 345551 Color: 3
Size: 252147 Color: 1

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 430496 Color: 1
Size: 307451 Color: 0
Size: 262053 Color: 2

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 470556 Color: 0
Size: 270879 Color: 3
Size: 258565 Color: 2

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 403732 Color: 2
Size: 318175 Color: 1
Size: 278093 Color: 3

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 397291 Color: 3
Size: 316523 Color: 3
Size: 286186 Color: 2

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 428009 Color: 3
Size: 313523 Color: 4
Size: 258468 Color: 2

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 400799 Color: 3
Size: 339583 Color: 2
Size: 259618 Color: 2

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 441891 Color: 0
Size: 281696 Color: 2
Size: 276413 Color: 1

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 473343 Color: 3
Size: 266486 Color: 3
Size: 260171 Color: 2

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 480642 Color: 2
Size: 262825 Color: 0
Size: 256533 Color: 1

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 451068 Color: 0
Size: 292935 Color: 4
Size: 255997 Color: 4

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 401917 Color: 2
Size: 343977 Color: 3
Size: 254106 Color: 3

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 413183 Color: 3
Size: 307736 Color: 4
Size: 279081 Color: 1

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 444583 Color: 4
Size: 280747 Color: 2
Size: 274670 Color: 0

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 393084 Color: 4
Size: 350855 Color: 2
Size: 256061 Color: 0

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 382922 Color: 3
Size: 360853 Color: 1
Size: 256225 Color: 2

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 426155 Color: 4
Size: 312291 Color: 0
Size: 261554 Color: 3

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 356780 Color: 2
Size: 350287 Color: 1
Size: 292933 Color: 0

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 494891 Color: 4
Size: 254043 Color: 1
Size: 251066 Color: 2

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 379980 Color: 3
Size: 359051 Color: 4
Size: 260969 Color: 1

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 411130 Color: 2
Size: 323264 Color: 4
Size: 265606 Color: 4

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 423903 Color: 1
Size: 291380 Color: 2
Size: 284717 Color: 3

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 447836 Color: 0
Size: 293406 Color: 2
Size: 258758 Color: 3

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 400086 Color: 2
Size: 306277 Color: 3
Size: 293637 Color: 4

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 460747 Color: 4
Size: 286756 Color: 2
Size: 252497 Color: 3

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 377988 Color: 3
Size: 355472 Color: 4
Size: 266540 Color: 3

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 442875 Color: 1
Size: 302199 Color: 0
Size: 254926 Color: 4

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 419537 Color: 2
Size: 316057 Color: 4
Size: 264406 Color: 1

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 379595 Color: 3
Size: 355237 Color: 0
Size: 265168 Color: 4

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 441482 Color: 3
Size: 283324 Color: 4
Size: 275194 Color: 1

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 384778 Color: 1
Size: 312602 Color: 1
Size: 302620 Color: 3

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 377536 Color: 3
Size: 352972 Color: 4
Size: 269492 Color: 0

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 401090 Color: 4
Size: 337459 Color: 2
Size: 261451 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 400519 Color: 2
Size: 327969 Color: 1
Size: 271512 Color: 3

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 381899 Color: 3
Size: 359707 Color: 2
Size: 258394 Color: 1

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 362130 Color: 1
Size: 323136 Color: 3
Size: 314734 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 395384 Color: 4
Size: 348755 Color: 4
Size: 255861 Color: 2

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 366236 Color: 1
Size: 350136 Color: 3
Size: 283628 Color: 0

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 369942 Color: 2
Size: 316126 Color: 2
Size: 313932 Color: 0

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 378225 Color: 2
Size: 318446 Color: 1
Size: 303329 Color: 3

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 379561 Color: 3
Size: 324005 Color: 3
Size: 296434 Color: 1

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 381072 Color: 2
Size: 344088 Color: 0
Size: 274840 Color: 4

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 382427 Color: 3
Size: 357983 Color: 0
Size: 259590 Color: 4

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 389615 Color: 1
Size: 312176 Color: 3
Size: 298209 Color: 1

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 392340 Color: 3
Size: 307638 Color: 3
Size: 300022 Color: 2

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 392437 Color: 3
Size: 317519 Color: 4
Size: 290044 Color: 1

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 393112 Color: 0
Size: 348058 Color: 3
Size: 258830 Color: 1

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 395000 Color: 2
Size: 304077 Color: 1
Size: 300923 Color: 4

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 394860 Color: 0
Size: 339832 Color: 4
Size: 265308 Color: 4

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 398089 Color: 3
Size: 318644 Color: 2
Size: 283267 Color: 1

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 399627 Color: 1
Size: 320321 Color: 1
Size: 280052 Color: 3

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 403595 Color: 3
Size: 304829 Color: 1
Size: 291576 Color: 1

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 405542 Color: 3
Size: 307686 Color: 3
Size: 286772 Color: 4

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 405607 Color: 3
Size: 321553 Color: 2
Size: 272840 Color: 0

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 408049 Color: 1
Size: 333378 Color: 3
Size: 258573 Color: 2

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 409532 Color: 0
Size: 309333 Color: 0
Size: 281135 Color: 1

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 413084 Color: 1
Size: 322646 Color: 3
Size: 264270 Color: 0

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 414280 Color: 1
Size: 313121 Color: 1
Size: 272599 Color: 0

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 416477 Color: 3
Size: 320129 Color: 4
Size: 263394 Color: 4

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 420237 Color: 1
Size: 292796 Color: 4
Size: 286967 Color: 2

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 420290 Color: 4
Size: 318571 Color: 1
Size: 261139 Color: 0

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 424781 Color: 3
Size: 324494 Color: 1
Size: 250725 Color: 1

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 435101 Color: 0
Size: 288469 Color: 2
Size: 276430 Color: 4

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 435334 Color: 4
Size: 287584 Color: 4
Size: 277082 Color: 0

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 437936 Color: 2
Size: 287360 Color: 3
Size: 274704 Color: 3

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 440320 Color: 3
Size: 292871 Color: 3
Size: 266809 Color: 2

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 440910 Color: 3
Size: 304773 Color: 2
Size: 254317 Color: 1

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 441338 Color: 2
Size: 280307 Color: 1
Size: 278355 Color: 0

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 457794 Color: 4
Size: 275462 Color: 3
Size: 266744 Color: 0

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 459430 Color: 4
Size: 276206 Color: 2
Size: 264364 Color: 0

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 473454 Color: 3
Size: 269466 Color: 0
Size: 257080 Color: 1

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 475040 Color: 3
Size: 274572 Color: 2
Size: 250388 Color: 1

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 477010 Color: 1
Size: 269286 Color: 2
Size: 253704 Color: 4

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 479294 Color: 4
Size: 268466 Color: 0
Size: 252240 Color: 1

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 400403 Color: 4
Size: 345785 Color: 1
Size: 253812 Color: 3

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 492973 Color: 0
Size: 254194 Color: 0
Size: 252833 Color: 2

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 493993 Color: 4
Size: 254932 Color: 0
Size: 251075 Color: 0

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 496902 Color: 4
Size: 251625 Color: 2
Size: 251473 Color: 1

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 382027 Color: 1
Size: 319663 Color: 4
Size: 298310 Color: 2

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 408431 Color: 2
Size: 302416 Color: 2
Size: 289153 Color: 3

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 339093 Color: 1
Size: 333192 Color: 2
Size: 327715 Color: 1

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 339016 Color: 3
Size: 334596 Color: 4
Size: 326388 Color: 4

Bin 479: 1 of cap free
Amount of items: 3
Items: 
Size: 386345 Color: 2
Size: 308341 Color: 4
Size: 305314 Color: 3

Bin 480: 1 of cap free
Amount of items: 3
Items: 
Size: 401835 Color: 0
Size: 329153 Color: 2
Size: 269012 Color: 2

Bin 481: 1 of cap free
Amount of items: 3
Items: 
Size: 393720 Color: 3
Size: 350939 Color: 3
Size: 255341 Color: 0

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 373928 Color: 0
Size: 342697 Color: 3
Size: 283374 Color: 4

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 408824 Color: 1
Size: 300365 Color: 2
Size: 290810 Color: 3

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 389382 Color: 4
Size: 333015 Color: 0
Size: 277602 Color: 2

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 361691 Color: 2
Size: 335565 Color: 0
Size: 302743 Color: 4

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 422777 Color: 2
Size: 317377 Color: 1
Size: 259845 Color: 0

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 413537 Color: 3
Size: 310891 Color: 3
Size: 275571 Color: 2

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 373351 Color: 3
Size: 338680 Color: 0
Size: 287968 Color: 2

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 385519 Color: 4
Size: 354868 Color: 3
Size: 259612 Color: 0

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 409181 Color: 4
Size: 313201 Color: 3
Size: 277617 Color: 1

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 360365 Color: 1
Size: 322886 Color: 3
Size: 316748 Color: 0

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 391980 Color: 0
Size: 352839 Color: 4
Size: 255180 Color: 4

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 378718 Color: 1
Size: 351680 Color: 4
Size: 269601 Color: 2

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 400233 Color: 4
Size: 328332 Color: 3
Size: 271434 Color: 0

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 376521 Color: 2
Size: 312357 Color: 2
Size: 311121 Color: 3

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 428480 Color: 0
Size: 300391 Color: 2
Size: 271128 Color: 0

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 431101 Color: 0
Size: 295609 Color: 2
Size: 273289 Color: 3

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 424264 Color: 0
Size: 300685 Color: 4
Size: 275050 Color: 4

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 335046 Color: 4
Size: 333381 Color: 1
Size: 331572 Color: 2

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 381831 Color: 0
Size: 331658 Color: 1
Size: 286510 Color: 0

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 409444 Color: 1
Size: 333067 Color: 0
Size: 257488 Color: 3

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 380000 Color: 0
Size: 325883 Color: 0
Size: 294116 Color: 2

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 413444 Color: 1
Size: 328429 Color: 1
Size: 258126 Color: 2

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 370494 Color: 2
Size: 326866 Color: 2
Size: 302639 Color: 4

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 363969 Color: 0
Size: 319709 Color: 2
Size: 316321 Color: 4

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 418051 Color: 4
Size: 292897 Color: 2
Size: 289051 Color: 1

Bin 507: 2 of cap free
Amount of items: 3
Items: 
Size: 366326 Color: 0
Size: 356681 Color: 1
Size: 276992 Color: 2

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 367951 Color: 0
Size: 365285 Color: 4
Size: 266763 Color: 3

Bin 509: 2 of cap free
Amount of items: 3
Items: 
Size: 373017 Color: 3
Size: 317726 Color: 2
Size: 309256 Color: 2

Bin 510: 2 of cap free
Amount of items: 3
Items: 
Size: 383471 Color: 4
Size: 327088 Color: 2
Size: 289440 Color: 3

Bin 511: 2 of cap free
Amount of items: 3
Items: 
Size: 386258 Color: 4
Size: 339765 Color: 2
Size: 273976 Color: 1

Bin 512: 2 of cap free
Amount of items: 3
Items: 
Size: 379197 Color: 3
Size: 319025 Color: 2
Size: 301777 Color: 2

Bin 513: 2 of cap free
Amount of items: 3
Items: 
Size: 394350 Color: 2
Size: 325893 Color: 0
Size: 279756 Color: 0

Bin 514: 2 of cap free
Amount of items: 3
Items: 
Size: 395668 Color: 0
Size: 343549 Color: 3
Size: 260782 Color: 2

Bin 515: 2 of cap free
Amount of items: 3
Items: 
Size: 396987 Color: 1
Size: 341465 Color: 4
Size: 261547 Color: 4

Bin 516: 2 of cap free
Amount of items: 3
Items: 
Size: 382003 Color: 1
Size: 361903 Color: 4
Size: 256093 Color: 4

Bin 517: 2 of cap free
Amount of items: 3
Items: 
Size: 402764 Color: 3
Size: 344201 Color: 1
Size: 253034 Color: 1

Bin 518: 2 of cap free
Amount of items: 3
Items: 
Size: 410192 Color: 0
Size: 318109 Color: 1
Size: 271698 Color: 3

Bin 519: 2 of cap free
Amount of items: 3
Items: 
Size: 418080 Color: 0
Size: 321666 Color: 2
Size: 260253 Color: 0

Bin 520: 2 of cap free
Amount of items: 3
Items: 
Size: 426394 Color: 2
Size: 291856 Color: 3
Size: 281749 Color: 3

Bin 521: 2 of cap free
Amount of items: 3
Items: 
Size: 428310 Color: 2
Size: 299355 Color: 4
Size: 272334 Color: 1

Bin 522: 2 of cap free
Amount of items: 3
Items: 
Size: 440421 Color: 1
Size: 288269 Color: 2
Size: 271309 Color: 3

Bin 523: 2 of cap free
Amount of items: 3
Items: 
Size: 450142 Color: 3
Size: 285024 Color: 2
Size: 264833 Color: 4

Bin 524: 2 of cap free
Amount of items: 3
Items: 
Size: 366386 Color: 4
Size: 338482 Color: 4
Size: 295131 Color: 3

Bin 525: 2 of cap free
Amount of items: 3
Items: 
Size: 424863 Color: 4
Size: 315937 Color: 2
Size: 259199 Color: 2

Bin 526: 2 of cap free
Amount of items: 3
Items: 
Size: 436555 Color: 1
Size: 291111 Color: 1
Size: 272333 Color: 2

Bin 527: 2 of cap free
Amount of items: 3
Items: 
Size: 364212 Color: 0
Size: 344509 Color: 0
Size: 291278 Color: 4

Bin 528: 2 of cap free
Amount of items: 3
Items: 
Size: 431423 Color: 4
Size: 300059 Color: 1
Size: 268517 Color: 4

Bin 529: 2 of cap free
Amount of items: 3
Items: 
Size: 358453 Color: 1
Size: 356773 Color: 1
Size: 284773 Color: 0

Bin 530: 2 of cap free
Amount of items: 3
Items: 
Size: 398668 Color: 3
Size: 342014 Color: 2
Size: 259317 Color: 2

Bin 531: 2 of cap free
Amount of items: 3
Items: 
Size: 343625 Color: 3
Size: 331354 Color: 3
Size: 325020 Color: 4

Bin 532: 3 of cap free
Amount of items: 3
Items: 
Size: 482291 Color: 1
Size: 261689 Color: 0
Size: 256018 Color: 0

Bin 533: 3 of cap free
Amount of items: 3
Items: 
Size: 361831 Color: 2
Size: 325729 Color: 4
Size: 312438 Color: 2

Bin 534: 3 of cap free
Amount of items: 3
Items: 
Size: 365143 Color: 3
Size: 327709 Color: 3
Size: 307146 Color: 2

Bin 535: 3 of cap free
Amount of items: 3
Items: 
Size: 468822 Color: 3
Size: 267706 Color: 0
Size: 263470 Color: 2

Bin 536: 3 of cap free
Amount of items: 3
Items: 
Size: 364646 Color: 3
Size: 355294 Color: 0
Size: 280058 Color: 3

Bin 537: 3 of cap free
Amount of items: 3
Items: 
Size: 354522 Color: 2
Size: 331386 Color: 0
Size: 314090 Color: 2

Bin 538: 3 of cap free
Amount of items: 3
Items: 
Size: 441465 Color: 1
Size: 288604 Color: 2
Size: 269929 Color: 4

Bin 539: 3 of cap free
Amount of items: 3
Items: 
Size: 366831 Color: 1
Size: 355593 Color: 2
Size: 277574 Color: 4

Bin 540: 3 of cap free
Amount of items: 3
Items: 
Size: 423614 Color: 3
Size: 294900 Color: 1
Size: 281484 Color: 1

Bin 541: 3 of cap free
Amount of items: 3
Items: 
Size: 387502 Color: 1
Size: 354283 Color: 0
Size: 258213 Color: 1

Bin 542: 3 of cap free
Amount of items: 3
Items: 
Size: 377360 Color: 1
Size: 370250 Color: 4
Size: 252388 Color: 3

Bin 543: 3 of cap free
Amount of items: 3
Items: 
Size: 361715 Color: 0
Size: 324498 Color: 1
Size: 313785 Color: 1

Bin 544: 3 of cap free
Amount of items: 3
Items: 
Size: 390759 Color: 1
Size: 319203 Color: 2
Size: 290036 Color: 3

Bin 545: 3 of cap free
Amount of items: 3
Items: 
Size: 369005 Color: 3
Size: 319413 Color: 4
Size: 311580 Color: 2

Bin 546: 3 of cap free
Amount of items: 3
Items: 
Size: 380902 Color: 1
Size: 364157 Color: 3
Size: 254939 Color: 1

Bin 547: 3 of cap free
Amount of items: 3
Items: 
Size: 381722 Color: 3
Size: 336720 Color: 0
Size: 281556 Color: 2

Bin 548: 3 of cap free
Amount of items: 3
Items: 
Size: 397259 Color: 3
Size: 310248 Color: 3
Size: 292491 Color: 4

Bin 549: 3 of cap free
Amount of items: 3
Items: 
Size: 368677 Color: 3
Size: 348768 Color: 2
Size: 282553 Color: 2

Bin 550: 3 of cap free
Amount of items: 3
Items: 
Size: 366682 Color: 3
Size: 341294 Color: 1
Size: 292022 Color: 2

Bin 551: 3 of cap free
Amount of items: 3
Items: 
Size: 348101 Color: 0
Size: 332106 Color: 3
Size: 319791 Color: 3

Bin 552: 4 of cap free
Amount of items: 3
Items: 
Size: 360396 Color: 2
Size: 354800 Color: 2
Size: 284801 Color: 1

Bin 553: 4 of cap free
Amount of items: 3
Items: 
Size: 383179 Color: 2
Size: 359696 Color: 3
Size: 257122 Color: 2

Bin 554: 4 of cap free
Amount of items: 3
Items: 
Size: 345013 Color: 0
Size: 343971 Color: 1
Size: 311013 Color: 2

Bin 555: 4 of cap free
Amount of items: 3
Items: 
Size: 378314 Color: 2
Size: 355949 Color: 4
Size: 265734 Color: 2

Bin 556: 4 of cap free
Amount of items: 3
Items: 
Size: 379827 Color: 3
Size: 349429 Color: 1
Size: 270741 Color: 0

Bin 557: 4 of cap free
Amount of items: 3
Items: 
Size: 408859 Color: 1
Size: 332903 Color: 0
Size: 258235 Color: 2

Bin 558: 4 of cap free
Amount of items: 3
Items: 
Size: 378826 Color: 1
Size: 356223 Color: 3
Size: 264948 Color: 1

Bin 559: 4 of cap free
Amount of items: 3
Items: 
Size: 365584 Color: 4
Size: 349416 Color: 0
Size: 284997 Color: 0

Bin 560: 4 of cap free
Amount of items: 3
Items: 
Size: 367564 Color: 2
Size: 361896 Color: 4
Size: 270537 Color: 4

Bin 561: 4 of cap free
Amount of items: 3
Items: 
Size: 374258 Color: 1
Size: 350871 Color: 1
Size: 274868 Color: 4

Bin 562: 4 of cap free
Amount of items: 3
Items: 
Size: 448400 Color: 3
Size: 287876 Color: 2
Size: 263721 Color: 1

Bin 563: 4 of cap free
Amount of items: 3
Items: 
Size: 366545 Color: 1
Size: 357280 Color: 2
Size: 276172 Color: 4

Bin 564: 5 of cap free
Amount of items: 3
Items: 
Size: 404291 Color: 4
Size: 309237 Color: 2
Size: 286468 Color: 1

Bin 565: 5 of cap free
Amount of items: 3
Items: 
Size: 340098 Color: 4
Size: 338872 Color: 4
Size: 321026 Color: 2

Bin 566: 5 of cap free
Amount of items: 3
Items: 
Size: 436083 Color: 1
Size: 291418 Color: 1
Size: 272495 Color: 2

Bin 567: 5 of cap free
Amount of items: 3
Items: 
Size: 383123 Color: 2
Size: 364724 Color: 1
Size: 252149 Color: 4

Bin 568: 5 of cap free
Amount of items: 3
Items: 
Size: 390850 Color: 3
Size: 310019 Color: 4
Size: 299127 Color: 3

Bin 569: 5 of cap free
Amount of items: 3
Items: 
Size: 382129 Color: 1
Size: 315713 Color: 2
Size: 302154 Color: 2

Bin 570: 5 of cap free
Amount of items: 3
Items: 
Size: 370110 Color: 4
Size: 364681 Color: 1
Size: 265205 Color: 3

Bin 571: 5 of cap free
Amount of items: 3
Items: 
Size: 428398 Color: 3
Size: 299706 Color: 4
Size: 271892 Color: 0

Bin 572: 7 of cap free
Amount of items: 3
Items: 
Size: 359792 Color: 2
Size: 342971 Color: 0
Size: 297231 Color: 2

Bin 573: 7 of cap free
Amount of items: 3
Items: 
Size: 420052 Color: 0
Size: 303640 Color: 2
Size: 276302 Color: 3

Bin 574: 8 of cap free
Amount of items: 3
Items: 
Size: 386376 Color: 2
Size: 320687 Color: 1
Size: 292930 Color: 0

Bin 575: 8 of cap free
Amount of items: 3
Items: 
Size: 377616 Color: 2
Size: 370289 Color: 2
Size: 252088 Color: 4

Bin 576: 8 of cap free
Amount of items: 3
Items: 
Size: 353899 Color: 3
Size: 340823 Color: 0
Size: 305271 Color: 1

Bin 577: 8 of cap free
Amount of items: 3
Items: 
Size: 354072 Color: 0
Size: 340005 Color: 1
Size: 305916 Color: 0

Bin 578: 9 of cap free
Amount of items: 3
Items: 
Size: 377001 Color: 2
Size: 314829 Color: 4
Size: 308162 Color: 0

Bin 579: 9 of cap free
Amount of items: 3
Items: 
Size: 371079 Color: 3
Size: 349547 Color: 3
Size: 279366 Color: 4

Bin 580: 9 of cap free
Amount of items: 3
Items: 
Size: 362404 Color: 2
Size: 347127 Color: 2
Size: 290461 Color: 0

Bin 581: 9 of cap free
Amount of items: 3
Items: 
Size: 343573 Color: 2
Size: 330106 Color: 4
Size: 326313 Color: 4

Bin 582: 9 of cap free
Amount of items: 3
Items: 
Size: 356655 Color: 3
Size: 348277 Color: 2
Size: 295060 Color: 2

Bin 583: 9 of cap free
Amount of items: 3
Items: 
Size: 381696 Color: 0
Size: 338741 Color: 2
Size: 279555 Color: 1

Bin 584: 9 of cap free
Amount of items: 3
Items: 
Size: 366687 Color: 1
Size: 319551 Color: 3
Size: 313754 Color: 2

Bin 585: 10 of cap free
Amount of items: 3
Items: 
Size: 389390 Color: 1
Size: 340182 Color: 2
Size: 270419 Color: 4

Bin 586: 10 of cap free
Amount of items: 3
Items: 
Size: 371211 Color: 4
Size: 329385 Color: 3
Size: 299395 Color: 4

Bin 587: 10 of cap free
Amount of items: 3
Items: 
Size: 374822 Color: 3
Size: 334800 Color: 0
Size: 290369 Color: 1

Bin 588: 11 of cap free
Amount of items: 3
Items: 
Size: 366096 Color: 1
Size: 318641 Color: 2
Size: 315253 Color: 0

Bin 589: 11 of cap free
Amount of items: 3
Items: 
Size: 397049 Color: 4
Size: 349280 Color: 1
Size: 253661 Color: 1

Bin 590: 11 of cap free
Amount of items: 3
Items: 
Size: 376296 Color: 0
Size: 366631 Color: 3
Size: 257063 Color: 4

Bin 591: 12 of cap free
Amount of items: 3
Items: 
Size: 362270 Color: 2
Size: 328182 Color: 0
Size: 309537 Color: 0

Bin 592: 12 of cap free
Amount of items: 3
Items: 
Size: 381463 Color: 1
Size: 325821 Color: 3
Size: 292705 Color: 3

Bin 593: 13 of cap free
Amount of items: 3
Items: 
Size: 357552 Color: 2
Size: 335604 Color: 1
Size: 306832 Color: 0

Bin 594: 13 of cap free
Amount of items: 3
Items: 
Size: 352376 Color: 4
Size: 325684 Color: 3
Size: 321928 Color: 0

Bin 595: 13 of cap free
Amount of items: 3
Items: 
Size: 365600 Color: 1
Size: 356598 Color: 3
Size: 277790 Color: 1

Bin 596: 13 of cap free
Amount of items: 3
Items: 
Size: 350373 Color: 0
Size: 347419 Color: 1
Size: 302196 Color: 2

Bin 597: 13 of cap free
Amount of items: 3
Items: 
Size: 385519 Color: 3
Size: 308719 Color: 1
Size: 305750 Color: 3

Bin 598: 15 of cap free
Amount of items: 3
Items: 
Size: 365291 Color: 4
Size: 322585 Color: 2
Size: 312110 Color: 2

Bin 599: 15 of cap free
Amount of items: 3
Items: 
Size: 410221 Color: 2
Size: 310554 Color: 1
Size: 279211 Color: 0

Bin 600: 15 of cap free
Amount of items: 3
Items: 
Size: 376404 Color: 2
Size: 352775 Color: 4
Size: 270807 Color: 0

Bin 601: 15 of cap free
Amount of items: 3
Items: 
Size: 374220 Color: 1
Size: 313049 Color: 1
Size: 312717 Color: 2

Bin 602: 16 of cap free
Amount of items: 3
Items: 
Size: 456335 Color: 4
Size: 289732 Color: 0
Size: 253918 Color: 2

Bin 603: 16 of cap free
Amount of items: 3
Items: 
Size: 492463 Color: 1
Size: 255894 Color: 1
Size: 251628 Color: 2

Bin 604: 16 of cap free
Amount of items: 3
Items: 
Size: 496872 Color: 0
Size: 252158 Color: 0
Size: 250955 Color: 1

Bin 605: 16 of cap free
Amount of items: 3
Items: 
Size: 492008 Color: 1
Size: 254037 Color: 4
Size: 253940 Color: 2

Bin 606: 17 of cap free
Amount of items: 3
Items: 
Size: 415156 Color: 0
Size: 322878 Color: 1
Size: 261950 Color: 2

Bin 607: 17 of cap free
Amount of items: 3
Items: 
Size: 495300 Color: 3
Size: 253802 Color: 2
Size: 250882 Color: 2

Bin 608: 17 of cap free
Amount of items: 3
Items: 
Size: 407984 Color: 3
Size: 305631 Color: 0
Size: 286369 Color: 0

Bin 609: 17 of cap free
Amount of items: 3
Items: 
Size: 361460 Color: 2
Size: 341613 Color: 1
Size: 296911 Color: 4

Bin 610: 18 of cap free
Amount of items: 3
Items: 
Size: 384124 Color: 4
Size: 318308 Color: 2
Size: 297551 Color: 0

Bin 611: 19 of cap free
Amount of items: 3
Items: 
Size: 340619 Color: 3
Size: 337524 Color: 4
Size: 321839 Color: 2

Bin 612: 19 of cap free
Amount of items: 3
Items: 
Size: 380928 Color: 0
Size: 329285 Color: 2
Size: 289769 Color: 0

Bin 613: 23 of cap free
Amount of items: 3
Items: 
Size: 416938 Color: 3
Size: 325249 Color: 2
Size: 257791 Color: 3

Bin 614: 23 of cap free
Amount of items: 3
Items: 
Size: 357812 Color: 0
Size: 356022 Color: 1
Size: 286144 Color: 3

Bin 615: 23 of cap free
Amount of items: 3
Items: 
Size: 361598 Color: 2
Size: 324459 Color: 3
Size: 313921 Color: 2

Bin 616: 24 of cap free
Amount of items: 3
Items: 
Size: 370320 Color: 3
Size: 328536 Color: 2
Size: 301121 Color: 1

Bin 617: 26 of cap free
Amount of items: 3
Items: 
Size: 347667 Color: 1
Size: 331005 Color: 4
Size: 321303 Color: 4

Bin 618: 27 of cap free
Amount of items: 3
Items: 
Size: 359520 Color: 0
Size: 325640 Color: 1
Size: 314814 Color: 2

Bin 619: 29 of cap free
Amount of items: 3
Items: 
Size: 395553 Color: 4
Size: 303082 Color: 2
Size: 301337 Color: 3

Bin 620: 30 of cap free
Amount of items: 3
Items: 
Size: 380550 Color: 1
Size: 318181 Color: 4
Size: 301240 Color: 0

Bin 621: 30 of cap free
Amount of items: 3
Items: 
Size: 378369 Color: 4
Size: 310978 Color: 1
Size: 310624 Color: 4

Bin 622: 31 of cap free
Amount of items: 3
Items: 
Size: 397736 Color: 4
Size: 323870 Color: 0
Size: 278364 Color: 2

Bin 623: 31 of cap free
Amount of items: 3
Items: 
Size: 381358 Color: 1
Size: 317563 Color: 1
Size: 301049 Color: 3

Bin 624: 33 of cap free
Amount of items: 3
Items: 
Size: 349751 Color: 0
Size: 331351 Color: 4
Size: 318866 Color: 2

Bin 625: 34 of cap free
Amount of items: 3
Items: 
Size: 388549 Color: 1
Size: 338638 Color: 2
Size: 272780 Color: 2

Bin 626: 36 of cap free
Amount of items: 3
Items: 
Size: 386809 Color: 2
Size: 355727 Color: 4
Size: 257429 Color: 4

Bin 627: 40 of cap free
Amount of items: 3
Items: 
Size: 447772 Color: 4
Size: 281877 Color: 2
Size: 270312 Color: 4

Bin 628: 42 of cap free
Amount of items: 3
Items: 
Size: 411487 Color: 4
Size: 301503 Color: 1
Size: 286969 Color: 4

Bin 629: 51 of cap free
Amount of items: 3
Items: 
Size: 442985 Color: 1
Size: 286365 Color: 3
Size: 270600 Color: 0

Bin 630: 51 of cap free
Amount of items: 3
Items: 
Size: 374679 Color: 0
Size: 374477 Color: 0
Size: 250794 Color: 1

Bin 631: 56 of cap free
Amount of items: 3
Items: 
Size: 418161 Color: 1
Size: 302581 Color: 3
Size: 279203 Color: 3

Bin 632: 64 of cap free
Amount of items: 3
Items: 
Size: 354545 Color: 1
Size: 335421 Color: 4
Size: 309971 Color: 3

Bin 633: 69 of cap free
Amount of items: 3
Items: 
Size: 356654 Color: 2
Size: 349867 Color: 3
Size: 293411 Color: 4

Bin 634: 70 of cap free
Amount of items: 3
Items: 
Size: 377721 Color: 1
Size: 371093 Color: 0
Size: 251117 Color: 3

Bin 635: 73 of cap free
Amount of items: 3
Items: 
Size: 486085 Color: 4
Size: 260526 Color: 0
Size: 253317 Color: 4

Bin 636: 77 of cap free
Amount of items: 3
Items: 
Size: 360379 Color: 1
Size: 335788 Color: 2
Size: 303757 Color: 1

Bin 637: 86 of cap free
Amount of items: 3
Items: 
Size: 358855 Color: 4
Size: 337692 Color: 1
Size: 303368 Color: 4

Bin 638: 88 of cap free
Amount of items: 3
Items: 
Size: 462553 Color: 4
Size: 270468 Color: 2
Size: 266892 Color: 3

Bin 639: 93 of cap free
Amount of items: 3
Items: 
Size: 486788 Color: 3
Size: 257642 Color: 0
Size: 255478 Color: 3

Bin 640: 105 of cap free
Amount of items: 3
Items: 
Size: 392723 Color: 2
Size: 327678 Color: 2
Size: 279495 Color: 4

Bin 641: 122 of cap free
Amount of items: 3
Items: 
Size: 410835 Color: 0
Size: 295513 Color: 4
Size: 293531 Color: 1

Bin 642: 138 of cap free
Amount of items: 3
Items: 
Size: 390586 Color: 0
Size: 313595 Color: 0
Size: 295682 Color: 4

Bin 643: 147 of cap free
Amount of items: 3
Items: 
Size: 361855 Color: 0
Size: 356022 Color: 2
Size: 281977 Color: 4

Bin 644: 172 of cap free
Amount of items: 3
Items: 
Size: 421765 Color: 1
Size: 291703 Color: 3
Size: 286361 Color: 3

Bin 645: 196 of cap free
Amount of items: 3
Items: 
Size: 358503 Color: 1
Size: 336662 Color: 0
Size: 304640 Color: 3

Bin 646: 202 of cap free
Amount of items: 3
Items: 
Size: 384103 Color: 3
Size: 363539 Color: 2
Size: 252157 Color: 2

Bin 647: 203 of cap free
Amount of items: 3
Items: 
Size: 367274 Color: 2
Size: 354671 Color: 4
Size: 277853 Color: 1

Bin 648: 294 of cap free
Amount of items: 3
Items: 
Size: 444529 Color: 2
Size: 280685 Color: 4
Size: 274493 Color: 3

Bin 649: 379 of cap free
Amount of items: 3
Items: 
Size: 415529 Color: 3
Size: 309085 Color: 0
Size: 275008 Color: 4

Bin 650: 461 of cap free
Amount of items: 3
Items: 
Size: 493229 Color: 4
Size: 256110 Color: 3
Size: 250201 Color: 0

Bin 651: 472 of cap free
Amount of items: 3
Items: 
Size: 373007 Color: 1
Size: 331057 Color: 0
Size: 295465 Color: 2

Bin 652: 486 of cap free
Amount of items: 3
Items: 
Size: 364146 Color: 2
Size: 350899 Color: 0
Size: 284470 Color: 0

Bin 653: 570 of cap free
Amount of items: 3
Items: 
Size: 475452 Color: 0
Size: 269276 Color: 4
Size: 254703 Color: 4

Bin 654: 591 of cap free
Amount of items: 3
Items: 
Size: 368669 Color: 0
Size: 347789 Color: 2
Size: 282952 Color: 2

Bin 655: 615 of cap free
Amount of items: 3
Items: 
Size: 413052 Color: 4
Size: 327330 Color: 4
Size: 259004 Color: 2

Bin 656: 752 of cap free
Amount of items: 3
Items: 
Size: 351509 Color: 0
Size: 333899 Color: 3
Size: 313841 Color: 1

Bin 657: 754 of cap free
Amount of items: 3
Items: 
Size: 468208 Color: 1
Size: 267455 Color: 3
Size: 263584 Color: 1

Bin 658: 814 of cap free
Amount of items: 3
Items: 
Size: 379145 Color: 3
Size: 341363 Color: 4
Size: 278679 Color: 2

Bin 659: 899 of cap free
Amount of items: 3
Items: 
Size: 397341 Color: 0
Size: 324362 Color: 3
Size: 277399 Color: 4

Bin 660: 1630 of cap free
Amount of items: 2
Items: 
Size: 499688 Color: 1
Size: 498683 Color: 3

Bin 661: 1966 of cap free
Amount of items: 3
Items: 
Size: 408641 Color: 1
Size: 322209 Color: 4
Size: 267185 Color: 3

Bin 662: 3910 of cap free
Amount of items: 2
Items: 
Size: 498150 Color: 4
Size: 497941 Color: 3

Bin 663: 22028 of cap free
Amount of items: 3
Items: 
Size: 354119 Color: 1
Size: 328570 Color: 0
Size: 295284 Color: 4

Bin 664: 99140 of cap free
Amount of items: 3
Items: 
Size: 321385 Color: 4
Size: 293232 Color: 0
Size: 286244 Color: 0

Bin 665: 179150 of cap free
Amount of items: 3
Items: 
Size: 284241 Color: 1
Size: 269995 Color: 2
Size: 266615 Color: 4

Bin 666: 206397 of cap free
Amount of items: 3
Items: 
Size: 265934 Color: 3
Size: 265872 Color: 2
Size: 261798 Color: 4

Bin 667: 226578 of cap free
Amount of items: 3
Items: 
Size: 261551 Color: 3
Size: 260965 Color: 3
Size: 250907 Color: 1

Bin 668: 248719 of cap free
Amount of items: 2
Items: 
Size: 497869 Color: 1
Size: 253413 Color: 0

Total size: 667000667
Total free space: 1000001

