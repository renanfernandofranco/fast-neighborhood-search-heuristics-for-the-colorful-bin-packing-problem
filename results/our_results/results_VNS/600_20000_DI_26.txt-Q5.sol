Capicity Bin: 16032
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 22
Items: 
Size: 1188 Color: 0
Size: 1104 Color: 1
Size: 1000 Color: 1
Size: 880 Color: 0
Size: 864 Color: 1
Size: 818 Color: 3
Size: 816 Color: 3
Size: 768 Color: 1
Size: 758 Color: 3
Size: 752 Color: 3
Size: 724 Color: 0
Size: 682 Color: 2
Size: 676 Color: 3
Size: 672 Color: 1
Size: 640 Color: 4
Size: 584 Color: 4
Size: 582 Color: 0
Size: 554 Color: 2
Size: 548 Color: 4
Size: 498 Color: 4
Size: 476 Color: 3
Size: 448 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8034 Color: 0
Size: 6666 Color: 2
Size: 1332 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9714 Color: 3
Size: 5672 Color: 1
Size: 646 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10036 Color: 3
Size: 5788 Color: 3
Size: 208 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10732 Color: 1
Size: 3884 Color: 2
Size: 1416 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11075 Color: 2
Size: 4131 Color: 3
Size: 826 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11480 Color: 0
Size: 4152 Color: 4
Size: 400 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11507 Color: 1
Size: 4219 Color: 0
Size: 306 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11539 Color: 1
Size: 3989 Color: 0
Size: 504 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11738 Color: 0
Size: 3800 Color: 2
Size: 494 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11939 Color: 0
Size: 3771 Color: 3
Size: 322 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 3
Size: 3288 Color: 0
Size: 496 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 2
Size: 2035 Color: 3
Size: 1601 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12482 Color: 1
Size: 3082 Color: 2
Size: 468 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 4
Size: 2891 Color: 3
Size: 576 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 0
Size: 2264 Color: 1
Size: 992 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12800 Color: 0
Size: 2856 Color: 1
Size: 376 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12855 Color: 0
Size: 1773 Color: 1
Size: 1404 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13166 Color: 4
Size: 2290 Color: 0
Size: 576 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13220 Color: 1
Size: 1660 Color: 0
Size: 1152 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13260 Color: 0
Size: 2008 Color: 2
Size: 764 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13327 Color: 2
Size: 2493 Color: 0
Size: 212 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13356 Color: 4
Size: 2236 Color: 0
Size: 440 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 0
Size: 1724 Color: 2
Size: 872 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13562 Color: 0
Size: 2082 Color: 1
Size: 388 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13578 Color: 0
Size: 1942 Color: 1
Size: 512 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13612 Color: 0
Size: 2092 Color: 3
Size: 328 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13626 Color: 0
Size: 1560 Color: 4
Size: 846 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13637 Color: 3
Size: 1997 Color: 3
Size: 398 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 0
Size: 1400 Color: 2
Size: 912 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13702 Color: 3
Size: 2006 Color: 1
Size: 324 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13780 Color: 0
Size: 1428 Color: 1
Size: 824 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13765 Color: 1
Size: 1917 Color: 1
Size: 350 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 1
Size: 1442 Color: 3
Size: 712 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13833 Color: 4
Size: 1833 Color: 1
Size: 366 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 1
Size: 1248 Color: 3
Size: 892 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13894 Color: 1
Size: 1604 Color: 4
Size: 534 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 0
Size: 1608 Color: 1
Size: 528 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13910 Color: 0
Size: 1788 Color: 1
Size: 334 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13948 Color: 3
Size: 1740 Color: 1
Size: 344 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 1
Size: 1334 Color: 4
Size: 754 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14025 Color: 1
Size: 1771 Color: 2
Size: 236 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 3
Size: 1280 Color: 0
Size: 760 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14053 Color: 2
Size: 1611 Color: 3
Size: 368 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14095 Color: 2
Size: 1497 Color: 3
Size: 440 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14107 Color: 4
Size: 1605 Color: 3
Size: 320 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14111 Color: 0
Size: 1537 Color: 0
Size: 384 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 1
Size: 1556 Color: 3
Size: 356 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 4
Size: 1542 Color: 0
Size: 322 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14172 Color: 3
Size: 1332 Color: 1
Size: 528 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 1
Size: 1152 Color: 2
Size: 664 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14233 Color: 2
Size: 1501 Color: 3
Size: 298 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14302 Color: 1
Size: 1458 Color: 4
Size: 272 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14290 Color: 4
Size: 1422 Color: 0
Size: 320 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14326 Color: 3
Size: 1370 Color: 0
Size: 336 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 1
Size: 1332 Color: 4
Size: 356 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 0
Size: 1188 Color: 2
Size: 488 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 4
Size: 1328 Color: 2
Size: 344 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 2
Size: 1332 Color: 4
Size: 310 Color: 1

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 8024 Color: 3
Size: 6673 Color: 1
Size: 1334 Color: 2

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 8893 Color: 3
Size: 6674 Color: 1
Size: 464 Color: 1

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 9613 Color: 3
Size: 6146 Color: 4
Size: 272 Color: 0

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 9783 Color: 0
Size: 5904 Color: 0
Size: 344 Color: 4

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 10245 Color: 0
Size: 5498 Color: 1
Size: 288 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11031 Color: 4
Size: 4388 Color: 0
Size: 612 Color: 1

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11086 Color: 1
Size: 4641 Color: 0
Size: 304 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11428 Color: 1
Size: 4151 Color: 4
Size: 452 Color: 0

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 11442 Color: 3
Size: 4231 Color: 1
Size: 358 Color: 1

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 11475 Color: 4
Size: 3804 Color: 1
Size: 752 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12153 Color: 0
Size: 2184 Color: 2
Size: 1694 Color: 4

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12357 Color: 4
Size: 3338 Color: 0
Size: 336 Color: 3

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12636 Color: 1
Size: 3063 Color: 0
Size: 332 Color: 3

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 12852 Color: 4
Size: 2747 Color: 0
Size: 432 Color: 2

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13082 Color: 1
Size: 2521 Color: 0
Size: 428 Color: 3

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13213 Color: 3
Size: 2316 Color: 4
Size: 502 Color: 2

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13283 Color: 3
Size: 2160 Color: 1
Size: 588 Color: 0

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13311 Color: 2
Size: 1890 Color: 1
Size: 830 Color: 0

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 2
Size: 2649 Color: 1

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 13455 Color: 0
Size: 1456 Color: 1
Size: 1120 Color: 2

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 13513 Color: 0
Size: 1686 Color: 3
Size: 832 Color: 2

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 13529 Color: 0
Size: 2206 Color: 1
Size: 296 Color: 1

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 13623 Color: 1
Size: 2020 Color: 0
Size: 388 Color: 1

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 13733 Color: 4
Size: 1334 Color: 1
Size: 964 Color: 0

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 14037 Color: 0
Size: 1770 Color: 1
Size: 224 Color: 4

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 14189 Color: 2
Size: 1626 Color: 1
Size: 216 Color: 4

Bin 86: 1 of cap free
Amount of items: 2
Items: 
Size: 14358 Color: 3
Size: 1673 Color: 0

Bin 87: 2 of cap free
Amount of items: 7
Items: 
Size: 8021 Color: 4
Size: 1953 Color: 1
Size: 1753 Color: 1
Size: 1553 Color: 2
Size: 1040 Color: 3
Size: 880 Color: 4
Size: 830 Color: 2

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 10662 Color: 3
Size: 4856 Color: 0
Size: 512 Color: 0

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 11932 Color: 1
Size: 2528 Color: 4
Size: 1570 Color: 0

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 12680 Color: 0
Size: 1896 Color: 1
Size: 1454 Color: 3

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 13041 Color: 2
Size: 2773 Color: 0
Size: 216 Color: 2

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 13144 Color: 4
Size: 2516 Color: 1
Size: 370 Color: 0

Bin 93: 2 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 3
Size: 2390 Color: 4

Bin 94: 2 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 3
Size: 2264 Color: 4

Bin 95: 2 of cap free
Amount of items: 2
Items: 
Size: 13929 Color: 4
Size: 2101 Color: 0

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 10298 Color: 3
Size: 5351 Color: 0
Size: 380 Color: 4

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 10820 Color: 4
Size: 5209 Color: 0

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 3
Size: 3385 Color: 1
Size: 204 Color: 4

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 12524 Color: 4
Size: 3207 Color: 0
Size: 298 Color: 2

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 12732 Color: 2
Size: 2229 Color: 4
Size: 1068 Color: 0

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 12910 Color: 0
Size: 2447 Color: 3
Size: 672 Color: 2

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 13009 Color: 1
Size: 2924 Color: 2
Size: 96 Color: 1

Bin 103: 4 of cap free
Amount of items: 38
Items: 
Size: 672 Color: 0
Size: 640 Color: 3
Size: 560 Color: 0
Size: 544 Color: 2
Size: 528 Color: 3
Size: 520 Color: 2
Size: 488 Color: 0
Size: 480 Color: 0
Size: 464 Color: 3
Size: 458 Color: 3
Size: 456 Color: 2
Size: 450 Color: 3
Size: 444 Color: 1
Size: 440 Color: 2
Size: 432 Color: 2
Size: 418 Color: 4
Size: 418 Color: 4
Size: 416 Color: 2
Size: 408 Color: 1
Size: 406 Color: 4
Size: 400 Color: 4
Size: 400 Color: 4
Size: 392 Color: 2
Size: 390 Color: 2
Size: 388 Color: 4
Size: 384 Color: 0
Size: 382 Color: 3
Size: 360 Color: 1
Size: 354 Color: 0
Size: 352 Color: 4
Size: 352 Color: 2
Size: 352 Color: 0
Size: 352 Color: 0
Size: 320 Color: 4
Size: 312 Color: 4
Size: 304 Color: 2
Size: 304 Color: 1
Size: 288 Color: 3

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 2
Size: 4348 Color: 3
Size: 616 Color: 1

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 11971 Color: 2
Size: 2675 Color: 3
Size: 1382 Color: 0

Bin 106: 4 of cap free
Amount of items: 3
Items: 
Size: 12331 Color: 1
Size: 3085 Color: 2
Size: 612 Color: 0

Bin 107: 4 of cap free
Amount of items: 2
Items: 
Size: 13020 Color: 0
Size: 3008 Color: 1

Bin 108: 4 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 3
Size: 1984 Color: 0

Bin 109: 4 of cap free
Amount of items: 2
Items: 
Size: 14324 Color: 3
Size: 1704 Color: 0

Bin 110: 5 of cap free
Amount of items: 3
Items: 
Size: 10968 Color: 0
Size: 4099 Color: 3
Size: 960 Color: 2

Bin 111: 5 of cap free
Amount of items: 3
Items: 
Size: 11043 Color: 3
Size: 4744 Color: 1
Size: 240 Color: 1

Bin 112: 5 of cap free
Amount of items: 2
Items: 
Size: 12705 Color: 2
Size: 3322 Color: 4

Bin 113: 5 of cap free
Amount of items: 2
Items: 
Size: 13065 Color: 2
Size: 2962 Color: 1

Bin 114: 5 of cap free
Amount of items: 2
Items: 
Size: 14099 Color: 0
Size: 1928 Color: 4

Bin 115: 6 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 0
Size: 4420 Color: 0
Size: 834 Color: 3

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 10923 Color: 3
Size: 4159 Color: 4
Size: 944 Color: 0

Bin 117: 6 of cap free
Amount of items: 2
Items: 
Size: 14228 Color: 3
Size: 1798 Color: 0

Bin 118: 7 of cap free
Amount of items: 3
Items: 
Size: 8528 Color: 1
Size: 6681 Color: 3
Size: 816 Color: 1

Bin 119: 7 of cap free
Amount of items: 2
Items: 
Size: 13861 Color: 2
Size: 2164 Color: 4

Bin 120: 8 of cap free
Amount of items: 3
Items: 
Size: 9208 Color: 4
Size: 6248 Color: 1
Size: 568 Color: 1

Bin 121: 8 of cap free
Amount of items: 3
Items: 
Size: 11127 Color: 1
Size: 4179 Color: 0
Size: 718 Color: 2

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 12180 Color: 4
Size: 3844 Color: 2

Bin 123: 9 of cap free
Amount of items: 5
Items: 
Size: 8026 Color: 2
Size: 4411 Color: 0
Size: 2348 Color: 3
Size: 782 Color: 4
Size: 456 Color: 4

Bin 124: 9 of cap free
Amount of items: 2
Items: 
Size: 11019 Color: 0
Size: 5004 Color: 3

Bin 125: 9 of cap free
Amount of items: 2
Items: 
Size: 13732 Color: 2
Size: 2291 Color: 1

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 12810 Color: 1
Size: 3212 Color: 3

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 13873 Color: 2
Size: 2149 Color: 4

Bin 128: 10 of cap free
Amount of items: 2
Items: 
Size: 14169 Color: 3
Size: 1853 Color: 2

Bin 129: 11 of cap free
Amount of items: 2
Items: 
Size: 14237 Color: 2
Size: 1784 Color: 3

Bin 130: 12 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 0
Size: 1782 Color: 4
Size: 176 Color: 2

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 13809 Color: 2
Size: 2210 Color: 4

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 13964 Color: 2
Size: 2055 Color: 0

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 14288 Color: 2
Size: 1731 Color: 0

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 8898 Color: 1
Size: 7120 Color: 4

Bin 135: 14 of cap free
Amount of items: 3
Items: 
Size: 12185 Color: 3
Size: 3233 Color: 1
Size: 600 Color: 0

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 1
Size: 2602 Color: 4

Bin 137: 15 of cap free
Amount of items: 3
Items: 
Size: 8018 Color: 3
Size: 6671 Color: 2
Size: 1328 Color: 4

Bin 138: 15 of cap free
Amount of items: 3
Items: 
Size: 9095 Color: 1
Size: 6682 Color: 4
Size: 240 Color: 3

Bin 139: 15 of cap free
Amount of items: 2
Items: 
Size: 11115 Color: 4
Size: 4902 Color: 0

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 3
Size: 4089 Color: 4

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 2
Size: 2473 Color: 1

Bin 142: 16 of cap free
Amount of items: 3
Items: 
Size: 10344 Color: 1
Size: 5464 Color: 0
Size: 208 Color: 2

Bin 143: 16 of cap free
Amount of items: 3
Items: 
Size: 12306 Color: 4
Size: 3582 Color: 3
Size: 128 Color: 4

Bin 144: 16 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 1
Size: 3400 Color: 2

Bin 145: 17 of cap free
Amount of items: 3
Items: 
Size: 10213 Color: 0
Size: 5266 Color: 2
Size: 536 Color: 3

Bin 146: 17 of cap free
Amount of items: 2
Items: 
Size: 14108 Color: 3
Size: 1907 Color: 4

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 13097 Color: 2
Size: 2917 Color: 1

Bin 148: 18 of cap free
Amount of items: 2
Items: 
Size: 13286 Color: 4
Size: 2728 Color: 3

Bin 149: 19 of cap free
Amount of items: 2
Items: 
Size: 12602 Color: 4
Size: 3411 Color: 1

Bin 150: 20 of cap free
Amount of items: 3
Items: 
Size: 9480 Color: 3
Size: 5780 Color: 1
Size: 752 Color: 4

Bin 151: 20 of cap free
Amount of items: 2
Items: 
Size: 13689 Color: 3
Size: 2323 Color: 1

Bin 152: 21 of cap free
Amount of items: 3
Items: 
Size: 10362 Color: 1
Size: 3380 Color: 3
Size: 2269 Color: 0

Bin 153: 21 of cap free
Amount of items: 2
Items: 
Size: 13359 Color: 2
Size: 2652 Color: 4

Bin 154: 21 of cap free
Amount of items: 2
Items: 
Size: 14002 Color: 0
Size: 2009 Color: 4

Bin 155: 23 of cap free
Amount of items: 2
Items: 
Size: 12737 Color: 4
Size: 3272 Color: 1

Bin 156: 24 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 1
Size: 6676 Color: 1
Size: 1052 Color: 4

Bin 157: 25 of cap free
Amount of items: 3
Items: 
Size: 8029 Color: 2
Size: 7786 Color: 0
Size: 192 Color: 0

Bin 158: 26 of cap free
Amount of items: 2
Items: 
Size: 13320 Color: 4
Size: 2686 Color: 1

Bin 159: 28 of cap free
Amount of items: 2
Items: 
Size: 11980 Color: 1
Size: 4024 Color: 3

Bin 160: 29 of cap free
Amount of items: 2
Items: 
Size: 13957 Color: 3
Size: 2046 Color: 4

Bin 161: 29 of cap free
Amount of items: 2
Items: 
Size: 14340 Color: 3
Size: 1663 Color: 4

Bin 162: 30 of cap free
Amount of items: 8
Items: 
Size: 8020 Color: 1
Size: 1528 Color: 0
Size: 1508 Color: 0
Size: 1328 Color: 0
Size: 1200 Color: 1
Size: 928 Color: 3
Size: 850 Color: 4
Size: 640 Color: 3

Bin 163: 31 of cap free
Amount of items: 2
Items: 
Size: 13245 Color: 2
Size: 2756 Color: 4

Bin 164: 32 of cap free
Amount of items: 2
Items: 
Size: 13745 Color: 1
Size: 2255 Color: 2

Bin 165: 33 of cap free
Amount of items: 2
Items: 
Size: 10463 Color: 4
Size: 5536 Color: 1

Bin 166: 33 of cap free
Amount of items: 2
Items: 
Size: 10955 Color: 3
Size: 5044 Color: 4

Bin 167: 33 of cap free
Amount of items: 2
Items: 
Size: 13591 Color: 3
Size: 2408 Color: 2

Bin 168: 35 of cap free
Amount of items: 2
Items: 
Size: 10216 Color: 1
Size: 5781 Color: 0

Bin 169: 35 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 4
Size: 1811 Color: 2

Bin 170: 36 of cap free
Amount of items: 3
Items: 
Size: 12046 Color: 3
Size: 2862 Color: 2
Size: 1088 Color: 0

Bin 171: 37 of cap free
Amount of items: 2
Items: 
Size: 11736 Color: 4
Size: 4259 Color: 3

Bin 172: 38 of cap free
Amount of items: 2
Items: 
Size: 9522 Color: 2
Size: 6472 Color: 0

Bin 173: 39 of cap free
Amount of items: 2
Items: 
Size: 14378 Color: 0
Size: 1615 Color: 3

Bin 174: 40 of cap free
Amount of items: 3
Items: 
Size: 8042 Color: 1
Size: 5946 Color: 1
Size: 2004 Color: 2

Bin 175: 40 of cap free
Amount of items: 2
Items: 
Size: 13905 Color: 3
Size: 2087 Color: 0

Bin 176: 44 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 2
Size: 3036 Color: 4

Bin 177: 45 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 1
Size: 2351 Color: 3

Bin 178: 49 of cap free
Amount of items: 2
Items: 
Size: 13521 Color: 1
Size: 2462 Color: 4

Bin 179: 50 of cap free
Amount of items: 2
Items: 
Size: 9824 Color: 1
Size: 6158 Color: 3

Bin 180: 63 of cap free
Amount of items: 3
Items: 
Size: 9100 Color: 3
Size: 6677 Color: 0
Size: 192 Color: 2

Bin 181: 66 of cap free
Amount of items: 2
Items: 
Size: 14082 Color: 1
Size: 1884 Color: 0

Bin 182: 67 of cap free
Amount of items: 2
Items: 
Size: 12533 Color: 4
Size: 3432 Color: 3

Bin 183: 68 of cap free
Amount of items: 3
Items: 
Size: 9092 Color: 4
Size: 6680 Color: 0
Size: 192 Color: 4

Bin 184: 69 of cap free
Amount of items: 2
Items: 
Size: 12338 Color: 4
Size: 3625 Color: 3

Bin 185: 78 of cap free
Amount of items: 5
Items: 
Size: 8025 Color: 2
Size: 3268 Color: 2
Size: 2093 Color: 1
Size: 1412 Color: 3
Size: 1156 Color: 3

Bin 186: 78 of cap free
Amount of items: 2
Items: 
Size: 11476 Color: 1
Size: 4478 Color: 4

Bin 187: 78 of cap free
Amount of items: 2
Items: 
Size: 13386 Color: 3
Size: 2568 Color: 1

Bin 188: 81 of cap free
Amount of items: 2
Items: 
Size: 14150 Color: 4
Size: 1801 Color: 3

Bin 189: 84 of cap free
Amount of items: 2
Items: 
Size: 8028 Color: 4
Size: 7920 Color: 2

Bin 190: 93 of cap free
Amount of items: 2
Items: 
Size: 9988 Color: 0
Size: 5951 Color: 2

Bin 191: 94 of cap free
Amount of items: 3
Items: 
Size: 8017 Color: 3
Size: 4122 Color: 2
Size: 3799 Color: 4

Bin 192: 97 of cap free
Amount of items: 2
Items: 
Size: 12823 Color: 4
Size: 3112 Color: 2

Bin 193: 102 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 2
Size: 3826 Color: 1

Bin 194: 108 of cap free
Amount of items: 2
Items: 
Size: 9240 Color: 0
Size: 6684 Color: 1

Bin 195: 117 of cap free
Amount of items: 2
Items: 
Size: 11683 Color: 3
Size: 4232 Color: 4

Bin 196: 158 of cap free
Amount of items: 2
Items: 
Size: 9618 Color: 1
Size: 6256 Color: 0

Bin 197: 158 of cap free
Amount of items: 2
Items: 
Size: 11051 Color: 3
Size: 4823 Color: 4

Bin 198: 190 of cap free
Amount of items: 2
Items: 
Size: 10741 Color: 2
Size: 5101 Color: 4

Bin 199: 12668 of cap free
Amount of items: 12
Items: 
Size: 304 Color: 0
Size: 288 Color: 2
Size: 288 Color: 2
Size: 284 Color: 4
Size: 284 Color: 3
Size: 280 Color: 0
Size: 280 Color: 0
Size: 272 Color: 4
Size: 272 Color: 3
Size: 272 Color: 3
Size: 272 Color: 1
Size: 268 Color: 4

Total size: 3174336
Total free space: 16032

