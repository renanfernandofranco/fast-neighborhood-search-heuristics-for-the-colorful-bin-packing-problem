Capicity Bin: 1000001
Lower Bound: 227

Bins used: 229
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 774946 Color: 1
Size: 114977 Color: 0
Size: 110078 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 768653 Color: 1
Size: 116060 Color: 0
Size: 115288 Color: 1

Bin 3: 3 of cap free
Amount of items: 3
Items: 
Size: 711242 Color: 1
Size: 170712 Color: 0
Size: 118044 Color: 0

Bin 4: 3 of cap free
Amount of items: 3
Items: 
Size: 636564 Color: 1
Size: 211211 Color: 0
Size: 152223 Color: 0

Bin 5: 5 of cap free
Amount of items: 3
Items: 
Size: 768621 Color: 0
Size: 125992 Color: 0
Size: 105383 Color: 1

Bin 6: 5 of cap free
Amount of items: 2
Items: 
Size: 700811 Color: 0
Size: 299185 Color: 1

Bin 7: 6 of cap free
Amount of items: 3
Items: 
Size: 653526 Color: 0
Size: 198669 Color: 0
Size: 147800 Color: 1

Bin 8: 6 of cap free
Amount of items: 3
Items: 
Size: 551850 Color: 1
Size: 265052 Color: 0
Size: 183093 Color: 0

Bin 9: 8 of cap free
Amount of items: 3
Items: 
Size: 397646 Color: 0
Size: 319074 Color: 1
Size: 283273 Color: 1

Bin 10: 8 of cap free
Amount of items: 3
Items: 
Size: 693998 Color: 1
Size: 165275 Color: 1
Size: 140720 Color: 0

Bin 11: 8 of cap free
Amount of items: 3
Items: 
Size: 690126 Color: 0
Size: 158766 Color: 1
Size: 151101 Color: 1

Bin 12: 9 of cap free
Amount of items: 3
Items: 
Size: 652889 Color: 0
Size: 186403 Color: 0
Size: 160700 Color: 1

Bin 13: 11 of cap free
Amount of items: 3
Items: 
Size: 537955 Color: 1
Size: 302717 Color: 0
Size: 159318 Color: 1

Bin 14: 15 of cap free
Amount of items: 3
Items: 
Size: 527169 Color: 0
Size: 299520 Color: 0
Size: 173297 Color: 1

Bin 15: 15 of cap free
Amount of items: 3
Items: 
Size: 602922 Color: 1
Size: 216948 Color: 1
Size: 180116 Color: 0

Bin 16: 18 of cap free
Amount of items: 3
Items: 
Size: 613403 Color: 1
Size: 204050 Color: 0
Size: 182530 Color: 1

Bin 17: 19 of cap free
Amount of items: 3
Items: 
Size: 534169 Color: 0
Size: 318827 Color: 1
Size: 146986 Color: 1

Bin 18: 27 of cap free
Amount of items: 3
Items: 
Size: 663759 Color: 0
Size: 211558 Color: 1
Size: 124657 Color: 1

Bin 19: 31 of cap free
Amount of items: 3
Items: 
Size: 758214 Color: 0
Size: 130799 Color: 0
Size: 110957 Color: 1

Bin 20: 32 of cap free
Amount of items: 2
Items: 
Size: 510213 Color: 0
Size: 489756 Color: 1

Bin 21: 33 of cap free
Amount of items: 2
Items: 
Size: 709180 Color: 0
Size: 290788 Color: 1

Bin 22: 34 of cap free
Amount of items: 3
Items: 
Size: 671640 Color: 0
Size: 187849 Color: 1
Size: 140478 Color: 0

Bin 23: 40 of cap free
Amount of items: 3
Items: 
Size: 658350 Color: 0
Size: 191844 Color: 0
Size: 149767 Color: 1

Bin 24: 41 of cap free
Amount of items: 3
Items: 
Size: 753912 Color: 1
Size: 125301 Color: 0
Size: 120747 Color: 1

Bin 25: 42 of cap free
Amount of items: 3
Items: 
Size: 620919 Color: 1
Size: 215425 Color: 0
Size: 163615 Color: 1

Bin 26: 43 of cap free
Amount of items: 3
Items: 
Size: 606821 Color: 1
Size: 291756 Color: 0
Size: 101381 Color: 0

Bin 27: 44 of cap free
Amount of items: 2
Items: 
Size: 748023 Color: 1
Size: 251934 Color: 0

Bin 28: 60 of cap free
Amount of items: 3
Items: 
Size: 678950 Color: 1
Size: 161792 Color: 0
Size: 159199 Color: 1

Bin 29: 67 of cap free
Amount of items: 3
Items: 
Size: 646996 Color: 1
Size: 234696 Color: 0
Size: 118242 Color: 0

Bin 30: 67 of cap free
Amount of items: 2
Items: 
Size: 722149 Color: 0
Size: 277785 Color: 1

Bin 31: 76 of cap free
Amount of items: 2
Items: 
Size: 523099 Color: 1
Size: 476826 Color: 0

Bin 32: 80 of cap free
Amount of items: 3
Items: 
Size: 702006 Color: 1
Size: 167985 Color: 0
Size: 129930 Color: 1

Bin 33: 98 of cap free
Amount of items: 3
Items: 
Size: 525726 Color: 0
Size: 276227 Color: 1
Size: 197950 Color: 0

Bin 34: 100 of cap free
Amount of items: 3
Items: 
Size: 704957 Color: 0
Size: 168266 Color: 1
Size: 126678 Color: 0

Bin 35: 100 of cap free
Amount of items: 2
Items: 
Size: 637915 Color: 0
Size: 361986 Color: 1

Bin 36: 106 of cap free
Amount of items: 3
Items: 
Size: 639505 Color: 0
Size: 242979 Color: 0
Size: 117411 Color: 1

Bin 37: 108 of cap free
Amount of items: 3
Items: 
Size: 655436 Color: 1
Size: 179585 Color: 1
Size: 164872 Color: 0

Bin 38: 110 of cap free
Amount of items: 2
Items: 
Size: 560715 Color: 1
Size: 439176 Color: 0

Bin 39: 110 of cap free
Amount of items: 2
Items: 
Size: 741666 Color: 1
Size: 258225 Color: 0

Bin 40: 126 of cap free
Amount of items: 3
Items: 
Size: 639271 Color: 0
Size: 234661 Color: 0
Size: 125943 Color: 1

Bin 41: 134 of cap free
Amount of items: 3
Items: 
Size: 656898 Color: 0
Size: 206626 Color: 0
Size: 136343 Color: 1

Bin 42: 139 of cap free
Amount of items: 2
Items: 
Size: 743153 Color: 1
Size: 256709 Color: 0

Bin 43: 140 of cap free
Amount of items: 3
Items: 
Size: 718133 Color: 0
Size: 172827 Color: 0
Size: 108901 Color: 1

Bin 44: 149 of cap free
Amount of items: 3
Items: 
Size: 686401 Color: 0
Size: 183346 Color: 1
Size: 130105 Color: 1

Bin 45: 150 of cap free
Amount of items: 3
Items: 
Size: 398534 Color: 0
Size: 312369 Color: 0
Size: 288948 Color: 1

Bin 46: 155 of cap free
Amount of items: 3
Items: 
Size: 703489 Color: 0
Size: 173017 Color: 0
Size: 123340 Color: 1

Bin 47: 161 of cap free
Amount of items: 3
Items: 
Size: 679062 Color: 0
Size: 182524 Color: 0
Size: 138254 Color: 1

Bin 48: 161 of cap free
Amount of items: 2
Items: 
Size: 723332 Color: 1
Size: 276508 Color: 0

Bin 49: 162 of cap free
Amount of items: 2
Items: 
Size: 795835 Color: 1
Size: 204004 Color: 0

Bin 50: 183 of cap free
Amount of items: 3
Items: 
Size: 520978 Color: 0
Size: 274879 Color: 1
Size: 203961 Color: 1

Bin 51: 200 of cap free
Amount of items: 2
Items: 
Size: 704099 Color: 0
Size: 295702 Color: 1

Bin 52: 205 of cap free
Amount of items: 2
Items: 
Size: 611132 Color: 1
Size: 388664 Color: 0

Bin 53: 224 of cap free
Amount of items: 2
Items: 
Size: 618250 Color: 1
Size: 381527 Color: 0

Bin 54: 226 of cap free
Amount of items: 3
Items: 
Size: 636457 Color: 1
Size: 218338 Color: 0
Size: 144980 Color: 1

Bin 55: 264 of cap free
Amount of items: 2
Items: 
Size: 696097 Color: 1
Size: 303640 Color: 0

Bin 56: 268 of cap free
Amount of items: 3
Items: 
Size: 602688 Color: 1
Size: 211020 Color: 0
Size: 186025 Color: 0

Bin 57: 276 of cap free
Amount of items: 2
Items: 
Size: 740292 Color: 0
Size: 259433 Color: 1

Bin 58: 292 of cap free
Amount of items: 3
Items: 
Size: 401598 Color: 0
Size: 308788 Color: 0
Size: 289323 Color: 1

Bin 59: 293 of cap free
Amount of items: 3
Items: 
Size: 694524 Color: 0
Size: 200362 Color: 1
Size: 104822 Color: 0

Bin 60: 294 of cap free
Amount of items: 2
Items: 
Size: 768380 Color: 0
Size: 231327 Color: 1

Bin 61: 308 of cap free
Amount of items: 2
Items: 
Size: 622110 Color: 1
Size: 377583 Color: 0

Bin 62: 346 of cap free
Amount of items: 2
Items: 
Size: 642247 Color: 1
Size: 357408 Color: 0

Bin 63: 356 of cap free
Amount of items: 2
Items: 
Size: 512168 Color: 1
Size: 487477 Color: 0

Bin 64: 382 of cap free
Amount of items: 3
Items: 
Size: 681175 Color: 0
Size: 169368 Color: 0
Size: 149076 Color: 1

Bin 65: 382 of cap free
Amount of items: 2
Items: 
Size: 547309 Color: 0
Size: 452310 Color: 1

Bin 66: 396 of cap free
Amount of items: 2
Items: 
Size: 756805 Color: 0
Size: 242800 Color: 1

Bin 67: 416 of cap free
Amount of items: 2
Items: 
Size: 576713 Color: 0
Size: 422872 Color: 1

Bin 68: 417 of cap free
Amount of items: 2
Items: 
Size: 530423 Color: 0
Size: 469161 Color: 1

Bin 69: 426 of cap free
Amount of items: 2
Items: 
Size: 592527 Color: 0
Size: 407048 Color: 1

Bin 70: 437 of cap free
Amount of items: 3
Items: 
Size: 559602 Color: 0
Size: 262089 Color: 0
Size: 177873 Color: 1

Bin 71: 442 of cap free
Amount of items: 2
Items: 
Size: 537540 Color: 0
Size: 462019 Color: 1

Bin 72: 453 of cap free
Amount of items: 2
Items: 
Size: 780194 Color: 0
Size: 219354 Color: 1

Bin 73: 488 of cap free
Amount of items: 2
Items: 
Size: 776805 Color: 0
Size: 222708 Color: 1

Bin 74: 488 of cap free
Amount of items: 2
Items: 
Size: 503155 Color: 1
Size: 496358 Color: 0

Bin 75: 489 of cap free
Amount of items: 2
Items: 
Size: 775874 Color: 0
Size: 223638 Color: 1

Bin 76: 511 of cap free
Amount of items: 2
Items: 
Size: 767333 Color: 0
Size: 232157 Color: 1

Bin 77: 517 of cap free
Amount of items: 2
Items: 
Size: 634425 Color: 0
Size: 365059 Color: 1

Bin 78: 521 of cap free
Amount of items: 2
Items: 
Size: 552800 Color: 0
Size: 446680 Color: 1

Bin 79: 526 of cap free
Amount of items: 2
Items: 
Size: 700115 Color: 0
Size: 299360 Color: 1

Bin 80: 536 of cap free
Amount of items: 2
Items: 
Size: 747685 Color: 1
Size: 251780 Color: 0

Bin 81: 556 of cap free
Amount of items: 3
Items: 
Size: 700095 Color: 1
Size: 187102 Color: 0
Size: 112248 Color: 0

Bin 82: 651 of cap free
Amount of items: 2
Items: 
Size: 693244 Color: 0
Size: 306106 Color: 1

Bin 83: 652 of cap free
Amount of items: 2
Items: 
Size: 626593 Color: 1
Size: 372756 Color: 0

Bin 84: 659 of cap free
Amount of items: 2
Items: 
Size: 537049 Color: 1
Size: 462293 Color: 0

Bin 85: 701 of cap free
Amount of items: 3
Items: 
Size: 533286 Color: 0
Size: 296689 Color: 1
Size: 169325 Color: 0

Bin 86: 702 of cap free
Amount of items: 2
Items: 
Size: 752790 Color: 1
Size: 246509 Color: 0

Bin 87: 739 of cap free
Amount of items: 2
Items: 
Size: 624437 Color: 1
Size: 374825 Color: 0

Bin 88: 755 of cap free
Amount of items: 2
Items: 
Size: 500064 Color: 1
Size: 499182 Color: 0

Bin 89: 827 of cap free
Amount of items: 3
Items: 
Size: 628371 Color: 1
Size: 192084 Color: 0
Size: 178719 Color: 0

Bin 90: 845 of cap free
Amount of items: 2
Items: 
Size: 691275 Color: 1
Size: 307881 Color: 0

Bin 91: 874 of cap free
Amount of items: 2
Items: 
Size: 684858 Color: 0
Size: 314269 Color: 1

Bin 92: 879 of cap free
Amount of items: 2
Items: 
Size: 552645 Color: 1
Size: 446477 Color: 0

Bin 93: 941 of cap free
Amount of items: 2
Items: 
Size: 540176 Color: 1
Size: 458884 Color: 0

Bin 94: 967 of cap free
Amount of items: 2
Items: 
Size: 762020 Color: 0
Size: 237014 Color: 1

Bin 95: 968 of cap free
Amount of items: 2
Items: 
Size: 666385 Color: 0
Size: 332648 Color: 1

Bin 96: 986 of cap free
Amount of items: 2
Items: 
Size: 673911 Color: 0
Size: 325104 Color: 1

Bin 97: 996 of cap free
Amount of items: 2
Items: 
Size: 721967 Color: 0
Size: 277038 Color: 1

Bin 98: 1005 of cap free
Amount of items: 2
Items: 
Size: 798954 Color: 0
Size: 200042 Color: 1

Bin 99: 1062 of cap free
Amount of items: 2
Items: 
Size: 511387 Color: 0
Size: 487552 Color: 1

Bin 100: 1105 of cap free
Amount of items: 2
Items: 
Size: 559548 Color: 1
Size: 439348 Color: 0

Bin 101: 1127 of cap free
Amount of items: 2
Items: 
Size: 571816 Color: 1
Size: 427058 Color: 0

Bin 102: 1141 of cap free
Amount of items: 2
Items: 
Size: 508357 Color: 1
Size: 490503 Color: 0

Bin 103: 1227 of cap free
Amount of items: 2
Items: 
Size: 582915 Color: 1
Size: 415859 Color: 0

Bin 104: 1252 of cap free
Amount of items: 2
Items: 
Size: 771154 Color: 1
Size: 227595 Color: 0

Bin 105: 1300 of cap free
Amount of items: 2
Items: 
Size: 671015 Color: 1
Size: 327686 Color: 0

Bin 106: 1350 of cap free
Amount of items: 2
Items: 
Size: 721177 Color: 1
Size: 277474 Color: 0

Bin 107: 1396 of cap free
Amount of items: 2
Items: 
Size: 644387 Color: 1
Size: 354218 Color: 0

Bin 108: 1411 of cap free
Amount of items: 2
Items: 
Size: 676602 Color: 0
Size: 321988 Color: 1

Bin 109: 1420 of cap free
Amount of items: 2
Items: 
Size: 559534 Color: 1
Size: 439047 Color: 0

Bin 110: 1438 of cap free
Amount of items: 2
Items: 
Size: 626410 Color: 1
Size: 372153 Color: 0

Bin 111: 1444 of cap free
Amount of items: 2
Items: 
Size: 771860 Color: 0
Size: 226697 Color: 1

Bin 112: 1455 of cap free
Amount of items: 2
Items: 
Size: 590045 Color: 1
Size: 408501 Color: 0

Bin 113: 1475 of cap free
Amount of items: 2
Items: 
Size: 752179 Color: 0
Size: 246347 Color: 1

Bin 114: 1499 of cap free
Amount of items: 2
Items: 
Size: 747633 Color: 0
Size: 250869 Color: 1

Bin 115: 1513 of cap free
Amount of items: 2
Items: 
Size: 711844 Color: 1
Size: 286644 Color: 0

Bin 116: 1514 of cap free
Amount of items: 2
Items: 
Size: 684000 Color: 1
Size: 314487 Color: 0

Bin 117: 1520 of cap free
Amount of items: 2
Items: 
Size: 563191 Color: 1
Size: 435290 Color: 0

Bin 118: 1538 of cap free
Amount of items: 2
Items: 
Size: 695304 Color: 1
Size: 303159 Color: 0

Bin 119: 1591 of cap free
Amount of items: 2
Items: 
Size: 718958 Color: 1
Size: 279452 Color: 0

Bin 120: 1632 of cap free
Amount of items: 2
Items: 
Size: 747380 Color: 1
Size: 250989 Color: 0

Bin 121: 1818 of cap free
Amount of items: 2
Items: 
Size: 768190 Color: 1
Size: 229993 Color: 0

Bin 122: 1821 of cap free
Amount of items: 2
Items: 
Size: 548488 Color: 1
Size: 449692 Color: 0

Bin 123: 1897 of cap free
Amount of items: 2
Items: 
Size: 639409 Color: 1
Size: 358695 Color: 0

Bin 124: 1952 of cap free
Amount of items: 2
Items: 
Size: 508191 Color: 1
Size: 489858 Color: 0

Bin 125: 1954 of cap free
Amount of items: 2
Items: 
Size: 622107 Color: 1
Size: 375940 Color: 0

Bin 126: 2104 of cap free
Amount of items: 2
Items: 
Size: 790640 Color: 1
Size: 207257 Color: 0

Bin 127: 2164 of cap free
Amount of items: 2
Items: 
Size: 607001 Color: 1
Size: 390836 Color: 0

Bin 128: 2175 of cap free
Amount of items: 2
Items: 
Size: 598055 Color: 0
Size: 399771 Color: 1

Bin 129: 2293 of cap free
Amount of items: 2
Items: 
Size: 609276 Color: 0
Size: 388432 Color: 1

Bin 130: 2395 of cap free
Amount of items: 2
Items: 
Size: 730285 Color: 0
Size: 267321 Color: 1

Bin 131: 2422 of cap free
Amount of items: 2
Items: 
Size: 579616 Color: 1
Size: 417963 Color: 0

Bin 132: 2507 of cap free
Amount of items: 2
Items: 
Size: 731032 Color: 1
Size: 266462 Color: 0

Bin 133: 2558 of cap free
Amount of items: 2
Items: 
Size: 567128 Color: 1
Size: 430315 Color: 0

Bin 134: 2613 of cap free
Amount of items: 2
Items: 
Size: 506088 Color: 0
Size: 491300 Color: 1

Bin 135: 2661 of cap free
Amount of items: 3
Items: 
Size: 705763 Color: 0
Size: 148785 Color: 0
Size: 142792 Color: 1

Bin 136: 2680 of cap free
Amount of items: 2
Items: 
Size: 755283 Color: 0
Size: 242038 Color: 1

Bin 137: 2868 of cap free
Amount of items: 2
Items: 
Size: 747617 Color: 0
Size: 249516 Color: 1

Bin 138: 2883 of cap free
Amount of items: 2
Items: 
Size: 707273 Color: 1
Size: 289845 Color: 0

Bin 139: 2893 of cap free
Amount of items: 2
Items: 
Size: 784596 Color: 0
Size: 212512 Color: 1

Bin 140: 2897 of cap free
Amount of items: 2
Items: 
Size: 541760 Color: 0
Size: 455344 Color: 1

Bin 141: 3034 of cap free
Amount of items: 2
Items: 
Size: 793043 Color: 1
Size: 203924 Color: 0

Bin 142: 3074 of cap free
Amount of items: 2
Items: 
Size: 516170 Color: 0
Size: 480757 Color: 1

Bin 143: 3084 of cap free
Amount of items: 2
Items: 
Size: 660652 Color: 0
Size: 336265 Color: 1

Bin 144: 3087 of cap free
Amount of items: 2
Items: 
Size: 530661 Color: 1
Size: 466253 Color: 0

Bin 145: 3148 of cap free
Amount of items: 2
Items: 
Size: 720645 Color: 1
Size: 276208 Color: 0

Bin 146: 3180 of cap free
Amount of items: 2
Items: 
Size: 603050 Color: 1
Size: 393771 Color: 0

Bin 147: 3186 of cap free
Amount of items: 2
Items: 
Size: 555957 Color: 0
Size: 440858 Color: 1

Bin 148: 3470 of cap free
Amount of items: 2
Items: 
Size: 504626 Color: 1
Size: 491905 Color: 0

Bin 149: 3477 of cap free
Amount of items: 2
Items: 
Size: 581577 Color: 0
Size: 414947 Color: 1

Bin 150: 3509 of cap free
Amount of items: 2
Items: 
Size: 672910 Color: 0
Size: 323582 Color: 1

Bin 151: 3654 of cap free
Amount of items: 2
Items: 
Size: 665443 Color: 0
Size: 330904 Color: 1

Bin 152: 3741 of cap free
Amount of items: 2
Items: 
Size: 582050 Color: 1
Size: 414210 Color: 0

Bin 153: 3752 of cap free
Amount of items: 2
Items: 
Size: 565758 Color: 0
Size: 430491 Color: 1

Bin 154: 3791 of cap free
Amount of items: 2
Items: 
Size: 551554 Color: 0
Size: 444656 Color: 1

Bin 155: 3805 of cap free
Amount of items: 2
Items: 
Size: 534770 Color: 0
Size: 461426 Color: 1

Bin 156: 3814 of cap free
Amount of items: 2
Items: 
Size: 673409 Color: 1
Size: 322778 Color: 0

Bin 157: 3883 of cap free
Amount of items: 2
Items: 
Size: 574961 Color: 0
Size: 421157 Color: 1

Bin 158: 3990 of cap free
Amount of items: 2
Items: 
Size: 624025 Color: 0
Size: 371986 Color: 1

Bin 159: 4190 of cap free
Amount of items: 2
Items: 
Size: 633597 Color: 1
Size: 362214 Color: 0

Bin 160: 4228 of cap free
Amount of items: 2
Items: 
Size: 511007 Color: 0
Size: 484766 Color: 1

Bin 161: 4259 of cap free
Amount of items: 2
Items: 
Size: 714975 Color: 0
Size: 280767 Color: 1

Bin 162: 4318 of cap free
Amount of items: 2
Items: 
Size: 746439 Color: 0
Size: 249244 Color: 1

Bin 163: 4334 of cap free
Amount of items: 3
Items: 
Size: 728477 Color: 0
Size: 163571 Color: 0
Size: 103619 Color: 1

Bin 164: 4351 of cap free
Amount of items: 2
Items: 
Size: 788403 Color: 0
Size: 207247 Color: 1

Bin 165: 4521 of cap free
Amount of items: 2
Items: 
Size: 732865 Color: 0
Size: 262615 Color: 1

Bin 166: 4653 of cap free
Amount of items: 2
Items: 
Size: 551078 Color: 0
Size: 444270 Color: 1

Bin 167: 4797 of cap free
Amount of items: 2
Items: 
Size: 638257 Color: 1
Size: 356947 Color: 0

Bin 168: 4805 of cap free
Amount of items: 2
Items: 
Size: 595926 Color: 1
Size: 399270 Color: 0

Bin 169: 4811 of cap free
Amount of items: 2
Items: 
Size: 520853 Color: 0
Size: 474337 Color: 1

Bin 170: 4831 of cap free
Amount of items: 2
Items: 
Size: 631643 Color: 0
Size: 363527 Color: 1

Bin 171: 4873 of cap free
Amount of items: 2
Items: 
Size: 527457 Color: 0
Size: 467671 Color: 1

Bin 172: 4906 of cap free
Amount of items: 2
Items: 
Size: 511056 Color: 1
Size: 484039 Color: 0

Bin 173: 4942 of cap free
Amount of items: 2
Items: 
Size: 570115 Color: 1
Size: 424944 Color: 0

Bin 174: 4960 of cap free
Amount of items: 2
Items: 
Size: 665279 Color: 0
Size: 329762 Color: 1

Bin 175: 5115 of cap free
Amount of items: 2
Items: 
Size: 673266 Color: 1
Size: 321620 Color: 0

Bin 176: 5249 of cap free
Amount of items: 2
Items: 
Size: 623259 Color: 0
Size: 371493 Color: 1

Bin 177: 5394 of cap free
Amount of items: 2
Items: 
Size: 644973 Color: 0
Size: 349634 Color: 1

Bin 178: 5669 of cap free
Amount of items: 2
Items: 
Size: 770703 Color: 0
Size: 223629 Color: 1

Bin 179: 5833 of cap free
Amount of items: 2
Items: 
Size: 536462 Color: 1
Size: 457706 Color: 0

Bin 180: 6177 of cap free
Amount of items: 2
Items: 
Size: 616366 Color: 0
Size: 377458 Color: 1

Bin 181: 6351 of cap free
Amount of items: 2
Items: 
Size: 656250 Color: 1
Size: 337400 Color: 0

Bin 182: 6509 of cap free
Amount of items: 2
Items: 
Size: 564455 Color: 0
Size: 429037 Color: 1

Bin 183: 6710 of cap free
Amount of items: 2
Items: 
Size: 608036 Color: 0
Size: 385255 Color: 1

Bin 184: 6919 of cap free
Amount of items: 2
Items: 
Size: 699967 Color: 1
Size: 293115 Color: 0

Bin 185: 6985 of cap free
Amount of items: 2
Items: 
Size: 519953 Color: 1
Size: 473063 Color: 0

Bin 186: 7135 of cap free
Amount of items: 2
Items: 
Size: 739200 Color: 0
Size: 253666 Color: 1

Bin 187: 7639 of cap free
Amount of items: 2
Items: 
Size: 766321 Color: 1
Size: 226041 Color: 0

Bin 188: 8627 of cap free
Amount of items: 2
Items: 
Size: 715671 Color: 1
Size: 275703 Color: 0

Bin 189: 9604 of cap free
Amount of items: 2
Items: 
Size: 534666 Color: 1
Size: 455731 Color: 0

Bin 190: 10184 of cap free
Amount of items: 2
Items: 
Size: 510061 Color: 0
Size: 479756 Color: 1

Bin 191: 10310 of cap free
Amount of items: 3
Items: 
Size: 728001 Color: 0
Size: 133278 Color: 0
Size: 128412 Color: 1

Bin 192: 10385 of cap free
Amount of items: 2
Items: 
Size: 699888 Color: 1
Size: 289728 Color: 0

Bin 193: 10848 of cap free
Amount of items: 2
Items: 
Size: 551012 Color: 0
Size: 438141 Color: 1

Bin 194: 12232 of cap free
Amount of items: 2
Items: 
Size: 550883 Color: 0
Size: 436886 Color: 1

Bin 195: 12255 of cap free
Amount of items: 2
Items: 
Size: 654517 Color: 1
Size: 333229 Color: 0

Bin 196: 12539 of cap free
Amount of items: 2
Items: 
Size: 725985 Color: 0
Size: 261477 Color: 1

Bin 197: 12723 of cap free
Amount of items: 2
Items: 
Size: 606985 Color: 1
Size: 380293 Color: 0

Bin 198: 13703 of cap free
Amount of items: 2
Items: 
Size: 632125 Color: 1
Size: 354173 Color: 0

Bin 199: 14706 of cap free
Amount of items: 2
Items: 
Size: 798914 Color: 0
Size: 186381 Color: 1

Bin 200: 15579 of cap free
Amount of items: 2
Items: 
Size: 576547 Color: 1
Size: 407875 Color: 0

Bin 201: 15807 of cap free
Amount of items: 2
Items: 
Size: 529717 Color: 1
Size: 454477 Color: 0

Bin 202: 15898 of cap free
Amount of items: 2
Items: 
Size: 738839 Color: 0
Size: 245264 Color: 1

Bin 203: 16487 of cap free
Amount of items: 2
Items: 
Size: 650386 Color: 1
Size: 333128 Color: 0

Bin 204: 17330 of cap free
Amount of items: 2
Items: 
Size: 704068 Color: 0
Size: 278603 Color: 1

Bin 205: 17708 of cap free
Amount of items: 2
Items: 
Size: 547470 Color: 1
Size: 434823 Color: 0

Bin 206: 18046 of cap free
Amount of items: 2
Items: 
Size: 778978 Color: 0
Size: 202977 Color: 1

Bin 207: 18052 of cap free
Amount of items: 2
Items: 
Size: 703664 Color: 0
Size: 278285 Color: 1

Bin 208: 21496 of cap free
Amount of items: 2
Items: 
Size: 649290 Color: 1
Size: 329215 Color: 0

Bin 209: 22932 of cap free
Amount of items: 2
Items: 
Size: 606454 Color: 0
Size: 370615 Color: 1

Bin 210: 25945 of cap free
Amount of items: 2
Items: 
Size: 786120 Color: 1
Size: 187936 Color: 0

Bin 211: 27733 of cap free
Amount of items: 2
Items: 
Size: 563850 Color: 0
Size: 408418 Color: 1

Bin 212: 31229 of cap free
Amount of items: 2
Items: 
Size: 563223 Color: 0
Size: 405549 Color: 1

Bin 213: 37929 of cap free
Amount of items: 2
Items: 
Size: 528727 Color: 1
Size: 433345 Color: 0

Bin 214: 39962 of cap free
Amount of items: 2
Items: 
Size: 725035 Color: 1
Size: 235004 Color: 0

Bin 215: 41962 of cap free
Amount of items: 2
Items: 
Size: 785950 Color: 1
Size: 172089 Color: 0

Bin 216: 44454 of cap free
Amount of items: 2
Items: 
Size: 550560 Color: 0
Size: 404987 Color: 1

Bin 217: 51582 of cap free
Amount of items: 2
Items: 
Size: 518442 Color: 1
Size: 429977 Color: 0

Bin 218: 53863 of cap free
Amount of items: 2
Items: 
Size: 768982 Color: 0
Size: 177156 Color: 1

Bin 219: 56067 of cap free
Amount of items: 2
Items: 
Size: 547035 Color: 0
Size: 396899 Color: 1

Bin 220: 64529 of cap free
Amount of items: 2
Items: 
Size: 511050 Color: 1
Size: 424422 Color: 0

Bin 221: 65813 of cap free
Amount of items: 2
Items: 
Size: 540188 Color: 0
Size: 394000 Color: 1

Bin 222: 72001 of cap free
Amount of items: 2
Items: 
Size: 534255 Color: 0
Size: 393745 Color: 1

Bin 223: 215686 of cap free
Amount of items: 1
Items: 
Size: 784315 Color: 1

Bin 224: 221238 of cap free
Amount of items: 1
Items: 
Size: 778763 Color: 1

Bin 225: 221630 of cap free
Amount of items: 1
Items: 
Size: 778371 Color: 1

Bin 226: 233761 of cap free
Amount of items: 1
Items: 
Size: 766240 Color: 1

Bin 227: 234262 of cap free
Amount of items: 1
Items: 
Size: 765739 Color: 0

Bin 228: 236459 of cap free
Amount of items: 1
Items: 
Size: 763542 Color: 0

Bin 229: 274185 of cap free
Amount of items: 1
Items: 
Size: 725816 Color: 0

Total size: 226107050
Total free space: 2893179

